.class public Lorg/apache/tools/ant/taskdefs/Java;
.super Lorg/apache/tools/ant/Task;
.source "Java.java"


# instance fields
.field private cmdl:Lorg/apache/tools/ant/types/CommandlineJava;

.field private dir:Ljava/io/File;

.field private env:Lorg/apache/tools/ant/types/Environment;

.field private error:Ljava/io/File;

.field private failOnError:Z

.field private fork:Z

.field private incompatibleWithSpawn:Z

.field private input:Ljava/io/File;

.field private inputString:Ljava/lang/String;

.field private newEnvironment:Z

.field private output:Ljava/io/File;

.field private perm:Lorg/apache/tools/ant/types/Permissions;

.field protected redirector:Lorg/apache/tools/ant/taskdefs/Redirector;

.field protected redirectorElement:Lorg/apache/tools/ant/types/RedirectorElement;

.field private resultProperty:Ljava/lang/String;

.field private spawn:Z

.field private timeout:Ljava/lang/Long;


# direct methods
.method public constructor <init>()V
    .locals 3

    const/4 v2, 0x0

    const/4 v1, 0x0

    invoke-direct {p0}, Lorg/apache/tools/ant/Task;-><init>()V

    new-instance v0, Lorg/apache/tools/ant/types/CommandlineJava;

    invoke-direct {v0}, Lorg/apache/tools/ant/types/CommandlineJava;-><init>()V

    iput-object v0, p0, Lorg/apache/tools/ant/taskdefs/Java;->cmdl:Lorg/apache/tools/ant/types/CommandlineJava;

    new-instance v0, Lorg/apache/tools/ant/types/Environment;

    invoke-direct {v0}, Lorg/apache/tools/ant/types/Environment;-><init>()V

    iput-object v0, p0, Lorg/apache/tools/ant/taskdefs/Java;->env:Lorg/apache/tools/ant/types/Environment;

    iput-boolean v1, p0, Lorg/apache/tools/ant/taskdefs/Java;->fork:Z

    iput-boolean v1, p0, Lorg/apache/tools/ant/taskdefs/Java;->newEnvironment:Z

    iput-object v2, p0, Lorg/apache/tools/ant/taskdefs/Java;->dir:Ljava/io/File;

    iput-boolean v1, p0, Lorg/apache/tools/ant/taskdefs/Java;->failOnError:Z

    iput-object v2, p0, Lorg/apache/tools/ant/taskdefs/Java;->timeout:Ljava/lang/Long;

    new-instance v0, Lorg/apache/tools/ant/taskdefs/Redirector;

    invoke-direct {v0, p0}, Lorg/apache/tools/ant/taskdefs/Redirector;-><init>(Lorg/apache/tools/ant/Task;)V

    iput-object v0, p0, Lorg/apache/tools/ant/taskdefs/Java;->redirector:Lorg/apache/tools/ant/taskdefs/Redirector;

    iput-object v2, p0, Lorg/apache/tools/ant/taskdefs/Java;->perm:Lorg/apache/tools/ant/types/Permissions;

    iput-boolean v1, p0, Lorg/apache/tools/ant/taskdefs/Java;->spawn:Z

    iput-boolean v1, p0, Lorg/apache/tools/ant/taskdefs/Java;->incompatibleWithSpawn:Z

    return-void
.end method

.method public constructor <init>(Lorg/apache/tools/ant/Task;)V
    .locals 3
    .param p1    # Lorg/apache/tools/ant/Task;

    const/4 v2, 0x0

    const/4 v1, 0x0

    invoke-direct {p0}, Lorg/apache/tools/ant/Task;-><init>()V

    new-instance v0, Lorg/apache/tools/ant/types/CommandlineJava;

    invoke-direct {v0}, Lorg/apache/tools/ant/types/CommandlineJava;-><init>()V

    iput-object v0, p0, Lorg/apache/tools/ant/taskdefs/Java;->cmdl:Lorg/apache/tools/ant/types/CommandlineJava;

    new-instance v0, Lorg/apache/tools/ant/types/Environment;

    invoke-direct {v0}, Lorg/apache/tools/ant/types/Environment;-><init>()V

    iput-object v0, p0, Lorg/apache/tools/ant/taskdefs/Java;->env:Lorg/apache/tools/ant/types/Environment;

    iput-boolean v1, p0, Lorg/apache/tools/ant/taskdefs/Java;->fork:Z

    iput-boolean v1, p0, Lorg/apache/tools/ant/taskdefs/Java;->newEnvironment:Z

    iput-object v2, p0, Lorg/apache/tools/ant/taskdefs/Java;->dir:Ljava/io/File;

    iput-boolean v1, p0, Lorg/apache/tools/ant/taskdefs/Java;->failOnError:Z

    iput-object v2, p0, Lorg/apache/tools/ant/taskdefs/Java;->timeout:Ljava/lang/Long;

    new-instance v0, Lorg/apache/tools/ant/taskdefs/Redirector;

    invoke-direct {v0, p0}, Lorg/apache/tools/ant/taskdefs/Redirector;-><init>(Lorg/apache/tools/ant/Task;)V

    iput-object v0, p0, Lorg/apache/tools/ant/taskdefs/Java;->redirector:Lorg/apache/tools/ant/taskdefs/Redirector;

    iput-object v2, p0, Lorg/apache/tools/ant/taskdefs/Java;->perm:Lorg/apache/tools/ant/types/Permissions;

    iput-boolean v1, p0, Lorg/apache/tools/ant/taskdefs/Java;->spawn:Z

    iput-boolean v1, p0, Lorg/apache/tools/ant/taskdefs/Java;->incompatibleWithSpawn:Z

    invoke-virtual {p0, p1}, Lorg/apache/tools/ant/taskdefs/Java;->bindToOwner(Lorg/apache/tools/ant/Task;)V

    return-void
.end method

.method private fork([Ljava/lang/String;)I
    .locals 5
    .param p1    # [Ljava/lang/String;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lorg/apache/tools/ant/BuildException;
        }
    .end annotation

    new-instance v1, Lorg/apache/tools/ant/taskdefs/Execute;

    iget-object v3, p0, Lorg/apache/tools/ant/taskdefs/Java;->redirector:Lorg/apache/tools/ant/taskdefs/Redirector;

    invoke-virtual {v3}, Lorg/apache/tools/ant/taskdefs/Redirector;->createHandler()Lorg/apache/tools/ant/taskdefs/ExecuteStreamHandler;

    move-result-object v3

    invoke-virtual {p0}, Lorg/apache/tools/ant/taskdefs/Java;->createWatchdog()Lorg/apache/tools/ant/taskdefs/ExecuteWatchdog;

    move-result-object v4

    invoke-direct {v1, v3, v4}, Lorg/apache/tools/ant/taskdefs/Execute;-><init>(Lorg/apache/tools/ant/taskdefs/ExecuteStreamHandler;Lorg/apache/tools/ant/taskdefs/ExecuteWatchdog;)V

    invoke-direct {p0, v1, p1}, Lorg/apache/tools/ant/taskdefs/Java;->setupExecutable(Lorg/apache/tools/ant/taskdefs/Execute;[Ljava/lang/String;)V

    :try_start_0
    invoke-virtual {v1}, Lorg/apache/tools/ant/taskdefs/Execute;->execute()I

    move-result v2

    iget-object v3, p0, Lorg/apache/tools/ant/taskdefs/Java;->redirector:Lorg/apache/tools/ant/taskdefs/Redirector;

    invoke-virtual {v3}, Lorg/apache/tools/ant/taskdefs/Redirector;->complete()V

    invoke-virtual {v1}, Lorg/apache/tools/ant/taskdefs/Execute;->killedProcess()Z

    move-result v3

    if-eqz v3, :cond_0

    new-instance v3, Lorg/apache/tools/ant/BuildException;

    const-string v4, "Timeout: killed the sub-process"

    invoke-direct {v3, v4}, Lorg/apache/tools/ant/BuildException;-><init>(Ljava/lang/String;)V

    throw v3
    :try_end_0
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_0

    :catch_0
    move-exception v0

    new-instance v3, Lorg/apache/tools/ant/BuildException;

    invoke-virtual {p0}, Lorg/apache/tools/ant/taskdefs/Java;->getLocation()Lorg/apache/tools/ant/Location;

    move-result-object v4

    invoke-direct {v3, v0, v4}, Lorg/apache/tools/ant/BuildException;-><init>(Ljava/lang/Throwable;Lorg/apache/tools/ant/Location;)V

    throw v3

    :cond_0
    return v2
.end method

.method private log(Ljava/lang/Throwable;)V
    .locals 4
    .param p1    # Ljava/lang/Throwable;

    new-instance v0, Ljava/io/StringWriter;

    invoke-direct {v0}, Ljava/io/StringWriter;-><init>()V

    new-instance v1, Ljava/io/PrintWriter;

    invoke-direct {v1, v0}, Ljava/io/PrintWriter;-><init>(Ljava/io/Writer;)V

    invoke-virtual {p1, v1}, Ljava/lang/Throwable;->printStackTrace(Ljava/io/PrintWriter;)V

    invoke-virtual {v1}, Ljava/io/PrintWriter;->close()V

    invoke-virtual {v0}, Ljava/io/StringWriter;->toString()Ljava/lang/String;

    move-result-object v2

    const/4 v3, 0x0

    invoke-virtual {p0, v2, v3}, Lorg/apache/tools/ant/taskdefs/Java;->log(Ljava/lang/String;I)V

    return-void
.end method

.method private run(Lorg/apache/tools/ant/types/CommandlineJava;)V
    .locals 4
    .param p1    # Lorg/apache/tools/ant/types/CommandlineJava;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lorg/apache/tools/ant/BuildException;
        }
    .end annotation

    :try_start_0
    new-instance v1, Lorg/apache/tools/ant/taskdefs/ExecuteJava;

    invoke-direct {v1}, Lorg/apache/tools/ant/taskdefs/ExecuteJava;-><init>()V

    invoke-virtual {p1}, Lorg/apache/tools/ant/types/CommandlineJava;->getJavaCommand()Lorg/apache/tools/ant/types/Commandline;

    move-result-object v2

    invoke-virtual {v1, v2}, Lorg/apache/tools/ant/taskdefs/ExecuteJava;->setJavaCommand(Lorg/apache/tools/ant/types/Commandline;)V

    invoke-virtual {p1}, Lorg/apache/tools/ant/types/CommandlineJava;->getClasspath()Lorg/apache/tools/ant/types/Path;

    move-result-object v2

    invoke-virtual {v1, v2}, Lorg/apache/tools/ant/taskdefs/ExecuteJava;->setClasspath(Lorg/apache/tools/ant/types/Path;)V

    invoke-virtual {p1}, Lorg/apache/tools/ant/types/CommandlineJava;->getSystemProperties()Lorg/apache/tools/ant/types/CommandlineJava$SysProperties;

    move-result-object v2

    invoke-virtual {v1, v2}, Lorg/apache/tools/ant/taskdefs/ExecuteJava;->setSystemProperties(Lorg/apache/tools/ant/types/CommandlineJava$SysProperties;)V

    iget-object v2, p0, Lorg/apache/tools/ant/taskdefs/Java;->perm:Lorg/apache/tools/ant/types/Permissions;

    invoke-virtual {v1, v2}, Lorg/apache/tools/ant/taskdefs/ExecuteJava;->setPermissions(Lorg/apache/tools/ant/types/Permissions;)V

    iget-object v2, p0, Lorg/apache/tools/ant/taskdefs/Java;->timeout:Ljava/lang/Long;

    invoke-virtual {v1, v2}, Lorg/apache/tools/ant/taskdefs/ExecuteJava;->setTimeout(Ljava/lang/Long;)V

    iget-object v2, p0, Lorg/apache/tools/ant/taskdefs/Java;->redirector:Lorg/apache/tools/ant/taskdefs/Redirector;

    invoke-virtual {v2}, Lorg/apache/tools/ant/taskdefs/Redirector;->createStreams()V

    invoke-virtual {p0}, Lorg/apache/tools/ant/taskdefs/Java;->getProject()Lorg/apache/tools/ant/Project;

    move-result-object v2

    invoke-virtual {v1, v2}, Lorg/apache/tools/ant/taskdefs/ExecuteJava;->execute(Lorg/apache/tools/ant/Project;)V

    iget-object v2, p0, Lorg/apache/tools/ant/taskdefs/Java;->redirector:Lorg/apache/tools/ant/taskdefs/Redirector;

    invoke-virtual {v2}, Lorg/apache/tools/ant/taskdefs/Redirector;->complete()V

    invoke-virtual {v1}, Lorg/apache/tools/ant/taskdefs/ExecuteJava;->killedProcess()Z

    move-result v2

    if-eqz v2, :cond_0

    new-instance v2, Lorg/apache/tools/ant/BuildException;

    const-string v3, "Timeout: killed the sub-process"

    invoke-direct {v2, v3}, Lorg/apache/tools/ant/BuildException;-><init>(Ljava/lang/String;)V

    throw v2
    :try_end_0
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_0

    :catch_0
    move-exception v0

    new-instance v2, Lorg/apache/tools/ant/BuildException;

    invoke-direct {v2, v0}, Lorg/apache/tools/ant/BuildException;-><init>(Ljava/lang/Throwable;)V

    throw v2

    :cond_0
    return-void
.end method

.method private setupCommandLine(Lorg/apache/tools/ant/taskdefs/Execute;[Ljava/lang/String;)V
    .locals 1
    .param p1    # Lorg/apache/tools/ant/taskdefs/Execute;
    .param p2    # [Ljava/lang/String;

    const-string v0, "openvms"

    invoke-static {v0}, Lorg/apache/tools/ant/taskdefs/condition/Os;->isFamily(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-direct {p0, p1, p2}, Lorg/apache/tools/ant/taskdefs/Java;->setupCommandLineForVMS(Lorg/apache/tools/ant/taskdefs/Execute;[Ljava/lang/String;)V

    :goto_0
    return-void

    :cond_0
    invoke-virtual {p1, p2}, Lorg/apache/tools/ant/taskdefs/Execute;->setCommandline([Ljava/lang/String;)V

    goto :goto_0
.end method

.method private setupCommandLineForVMS(Lorg/apache/tools/ant/taskdefs/Execute;[Ljava/lang/String;)V
    .locals 0
    .param p1    # Lorg/apache/tools/ant/taskdefs/Execute;
    .param p2    # [Ljava/lang/String;

    invoke-static {p1, p2}, Lorg/apache/tools/ant/taskdefs/ExecuteJava;->setupCommandLineForVMS(Lorg/apache/tools/ant/taskdefs/Execute;[Ljava/lang/String;)V

    return-void
.end method

.method private setupEnvironment(Lorg/apache/tools/ant/taskdefs/Execute;)V
    .locals 4
    .param p1    # Lorg/apache/tools/ant/taskdefs/Execute;

    iget-object v2, p0, Lorg/apache/tools/ant/taskdefs/Java;->env:Lorg/apache/tools/ant/types/Environment;

    invoke-virtual {v2}, Lorg/apache/tools/ant/types/Environment;->getVariables()[Ljava/lang/String;

    move-result-object v0

    if-eqz v0, :cond_0

    const/4 v1, 0x0

    :goto_0
    array-length v2, v0

    if-ge v1, v2, :cond_0

    new-instance v2, Ljava/lang/StringBuffer;

    invoke-direct {v2}, Ljava/lang/StringBuffer;-><init>()V

    const-string v3, "Setting environment variable: "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    move-result-object v2

    aget-object v3, v0, v1

    invoke-virtual {v2, v3}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuffer;->toString()Ljava/lang/String;

    move-result-object v2

    const/4 v3, 0x3

    invoke-virtual {p0, v2, v3}, Lorg/apache/tools/ant/taskdefs/Java;->log(Ljava/lang/String;I)V

    add-int/lit8 v1, v1, 0x1

    goto :goto_0

    :cond_0
    iget-boolean v2, p0, Lorg/apache/tools/ant/taskdefs/Java;->newEnvironment:Z

    invoke-virtual {p1, v2}, Lorg/apache/tools/ant/taskdefs/Execute;->setNewenvironment(Z)V

    invoke-virtual {p1, v0}, Lorg/apache/tools/ant/taskdefs/Execute;->setEnvironment([Ljava/lang/String;)V

    return-void
.end method

.method private setupExecutable(Lorg/apache/tools/ant/taskdefs/Execute;[Ljava/lang/String;)V
    .locals 1
    .param p1    # Lorg/apache/tools/ant/taskdefs/Execute;
    .param p2    # [Ljava/lang/String;

    invoke-virtual {p0}, Lorg/apache/tools/ant/taskdefs/Java;->getProject()Lorg/apache/tools/ant/Project;

    move-result-object v0

    invoke-virtual {p1, v0}, Lorg/apache/tools/ant/taskdefs/Execute;->setAntRun(Lorg/apache/tools/ant/Project;)V

    invoke-direct {p0, p1}, Lorg/apache/tools/ant/taskdefs/Java;->setupWorkingDir(Lorg/apache/tools/ant/taskdefs/Execute;)V

    invoke-direct {p0, p1}, Lorg/apache/tools/ant/taskdefs/Java;->setupEnvironment(Lorg/apache/tools/ant/taskdefs/Execute;)V

    invoke-direct {p0, p1, p2}, Lorg/apache/tools/ant/taskdefs/Java;->setupCommandLine(Lorg/apache/tools/ant/taskdefs/Execute;[Ljava/lang/String;)V

    return-void
.end method

.method private setupWorkingDir(Lorg/apache/tools/ant/taskdefs/Execute;)V
    .locals 3
    .param p1    # Lorg/apache/tools/ant/taskdefs/Execute;

    iget-object v0, p0, Lorg/apache/tools/ant/taskdefs/Java;->dir:Ljava/io/File;

    if-nez v0, :cond_1

    invoke-virtual {p0}, Lorg/apache/tools/ant/taskdefs/Java;->getProject()Lorg/apache/tools/ant/Project;

    move-result-object v0

    invoke-virtual {v0}, Lorg/apache/tools/ant/Project;->getBaseDir()Ljava/io/File;

    move-result-object v0

    iput-object v0, p0, Lorg/apache/tools/ant/taskdefs/Java;->dir:Ljava/io/File;

    :cond_0
    iget-object v0, p0, Lorg/apache/tools/ant/taskdefs/Java;->dir:Ljava/io/File;

    invoke-virtual {p1, v0}, Lorg/apache/tools/ant/taskdefs/Execute;->setWorkingDirectory(Ljava/io/File;)V

    return-void

    :cond_1
    iget-object v0, p0, Lorg/apache/tools/ant/taskdefs/Java;->dir:Ljava/io/File;

    invoke-virtual {v0}, Ljava/io/File;->exists()Z

    move-result v0

    if-eqz v0, :cond_2

    iget-object v0, p0, Lorg/apache/tools/ant/taskdefs/Java;->dir:Ljava/io/File;

    invoke-virtual {v0}, Ljava/io/File;->isDirectory()Z

    move-result v0

    if-nez v0, :cond_0

    :cond_2
    new-instance v0, Lorg/apache/tools/ant/BuildException;

    new-instance v1, Ljava/lang/StringBuffer;

    invoke-direct {v1}, Ljava/lang/StringBuffer;-><init>()V

    iget-object v2, p0, Lorg/apache/tools/ant/taskdefs/Java;->dir:Ljava/io/File;

    invoke-virtual {v2}, Ljava/io/File;->getAbsolutePath()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    move-result-object v1

    const-string v2, " is not a valid directory"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuffer;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {p0}, Lorg/apache/tools/ant/taskdefs/Java;->getLocation()Lorg/apache/tools/ant/Location;

    move-result-object v2

    invoke-direct {v0, v1, v2}, Lorg/apache/tools/ant/BuildException;-><init>(Ljava/lang/String;Lorg/apache/tools/ant/Location;)V

    throw v0
.end method

.method private spawn([Ljava/lang/String;)V
    .locals 4
    .param p1    # [Ljava/lang/String;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lorg/apache/tools/ant/BuildException;
        }
    .end annotation

    new-instance v1, Lorg/apache/tools/ant/taskdefs/Execute;

    invoke-direct {v1}, Lorg/apache/tools/ant/taskdefs/Execute;-><init>()V

    invoke-direct {p0, v1, p1}, Lorg/apache/tools/ant/taskdefs/Java;->setupExecutable(Lorg/apache/tools/ant/taskdefs/Execute;[Ljava/lang/String;)V

    :try_start_0
    invoke-virtual {v1}, Lorg/apache/tools/ant/taskdefs/Execute;->spawn()V
    :try_end_0
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_0

    return-void

    :catch_0
    move-exception v0

    new-instance v2, Lorg/apache/tools/ant/BuildException;

    invoke-virtual {p0}, Lorg/apache/tools/ant/taskdefs/Java;->getLocation()Lorg/apache/tools/ant/Location;

    move-result-object v3

    invoke-direct {v2, v0, v3}, Lorg/apache/tools/ant/BuildException;-><init>(Ljava/lang/Throwable;Lorg/apache/tools/ant/Location;)V

    throw v2
.end method


# virtual methods
.method public addAssertions(Lorg/apache/tools/ant/types/Assertions;)V
    .locals 2
    .param p1    # Lorg/apache/tools/ant/types/Assertions;

    invoke-virtual {p0}, Lorg/apache/tools/ant/taskdefs/Java;->getCommandLine()Lorg/apache/tools/ant/types/CommandlineJava;

    move-result-object v0

    invoke-virtual {v0}, Lorg/apache/tools/ant/types/CommandlineJava;->getAssertions()Lorg/apache/tools/ant/types/Assertions;

    move-result-object v0

    if-eqz v0, :cond_0

    new-instance v0, Lorg/apache/tools/ant/BuildException;

    const-string v1, "Only one assertion declaration is allowed"

    invoke-direct {v0, v1}, Lorg/apache/tools/ant/BuildException;-><init>(Ljava/lang/String;)V

    throw v0

    :cond_0
    invoke-virtual {p0}, Lorg/apache/tools/ant/taskdefs/Java;->getCommandLine()Lorg/apache/tools/ant/types/CommandlineJava;

    move-result-object v0

    invoke-virtual {v0, p1}, Lorg/apache/tools/ant/types/CommandlineJava;->setAssertions(Lorg/apache/tools/ant/types/Assertions;)V

    return-void
.end method

.method public addConfiguredRedirector(Lorg/apache/tools/ant/types/RedirectorElement;)V
    .locals 2
    .param p1    # Lorg/apache/tools/ant/types/RedirectorElement;

    iget-object v0, p0, Lorg/apache/tools/ant/taskdefs/Java;->redirectorElement:Lorg/apache/tools/ant/types/RedirectorElement;

    if-eqz v0, :cond_0

    new-instance v0, Lorg/apache/tools/ant/BuildException;

    const-string v1, "cannot have > 1 nested redirectors"

    invoke-direct {v0, v1}, Lorg/apache/tools/ant/BuildException;-><init>(Ljava/lang/String;)V

    throw v0

    :cond_0
    iput-object p1, p0, Lorg/apache/tools/ant/taskdefs/Java;->redirectorElement:Lorg/apache/tools/ant/types/RedirectorElement;

    const/4 v0, 0x1

    iput-boolean v0, p0, Lorg/apache/tools/ant/taskdefs/Java;->incompatibleWithSpawn:Z

    return-void
.end method

.method public addEnv(Lorg/apache/tools/ant/types/Environment$Variable;)V
    .locals 1
    .param p1    # Lorg/apache/tools/ant/types/Environment$Variable;

    iget-object v0, p0, Lorg/apache/tools/ant/taskdefs/Java;->env:Lorg/apache/tools/ant/types/Environment;

    invoke-virtual {v0, p1}, Lorg/apache/tools/ant/types/Environment;->addVariable(Lorg/apache/tools/ant/types/Environment$Variable;)V

    return-void
.end method

.method public addSysproperty(Lorg/apache/tools/ant/types/Environment$Variable;)V
    .locals 1
    .param p1    # Lorg/apache/tools/ant/types/Environment$Variable;

    invoke-virtual {p0}, Lorg/apache/tools/ant/taskdefs/Java;->getCommandLine()Lorg/apache/tools/ant/types/CommandlineJava;

    move-result-object v0

    invoke-virtual {v0, p1}, Lorg/apache/tools/ant/types/CommandlineJava;->addSysproperty(Lorg/apache/tools/ant/types/Environment$Variable;)V

    return-void
.end method

.method public addSyspropertyset(Lorg/apache/tools/ant/types/PropertySet;)V
    .locals 1
    .param p1    # Lorg/apache/tools/ant/types/PropertySet;

    invoke-virtual {p0}, Lorg/apache/tools/ant/taskdefs/Java;->getCommandLine()Lorg/apache/tools/ant/types/CommandlineJava;

    move-result-object v0

    invoke-virtual {v0, p1}, Lorg/apache/tools/ant/types/CommandlineJava;->addSyspropertyset(Lorg/apache/tools/ant/types/PropertySet;)V

    return-void
.end method

.method public clearArgs()V
    .locals 1

    invoke-virtual {p0}, Lorg/apache/tools/ant/taskdefs/Java;->getCommandLine()Lorg/apache/tools/ant/types/CommandlineJava;

    move-result-object v0

    invoke-virtual {v0}, Lorg/apache/tools/ant/types/CommandlineJava;->clearJavaArgs()V

    return-void
.end method

.method public createArg()Lorg/apache/tools/ant/types/Commandline$Argument;
    .locals 1

    invoke-virtual {p0}, Lorg/apache/tools/ant/taskdefs/Java;->getCommandLine()Lorg/apache/tools/ant/types/CommandlineJava;

    move-result-object v0

    invoke-virtual {v0}, Lorg/apache/tools/ant/types/CommandlineJava;->createArgument()Lorg/apache/tools/ant/types/Commandline$Argument;

    move-result-object v0

    return-object v0
.end method

.method public createBootclasspath()Lorg/apache/tools/ant/types/Path;
    .locals 2

    invoke-virtual {p0}, Lorg/apache/tools/ant/taskdefs/Java;->getCommandLine()Lorg/apache/tools/ant/types/CommandlineJava;

    move-result-object v0

    invoke-virtual {p0}, Lorg/apache/tools/ant/taskdefs/Java;->getProject()Lorg/apache/tools/ant/Project;

    move-result-object v1

    invoke-virtual {v0, v1}, Lorg/apache/tools/ant/types/CommandlineJava;->createBootclasspath(Lorg/apache/tools/ant/Project;)Lorg/apache/tools/ant/types/Path;

    move-result-object v0

    invoke-virtual {v0}, Lorg/apache/tools/ant/types/Path;->createPath()Lorg/apache/tools/ant/types/Path;

    move-result-object v0

    return-object v0
.end method

.method public createClasspath()Lorg/apache/tools/ant/types/Path;
    .locals 2

    invoke-virtual {p0}, Lorg/apache/tools/ant/taskdefs/Java;->getCommandLine()Lorg/apache/tools/ant/types/CommandlineJava;

    move-result-object v0

    invoke-virtual {p0}, Lorg/apache/tools/ant/taskdefs/Java;->getProject()Lorg/apache/tools/ant/Project;

    move-result-object v1

    invoke-virtual {v0, v1}, Lorg/apache/tools/ant/types/CommandlineJava;->createClasspath(Lorg/apache/tools/ant/Project;)Lorg/apache/tools/ant/types/Path;

    move-result-object v0

    invoke-virtual {v0}, Lorg/apache/tools/ant/types/Path;->createPath()Lorg/apache/tools/ant/types/Path;

    move-result-object v0

    return-object v0
.end method

.method public createJvmarg()Lorg/apache/tools/ant/types/Commandline$Argument;
    .locals 1

    invoke-virtual {p0}, Lorg/apache/tools/ant/taskdefs/Java;->getCommandLine()Lorg/apache/tools/ant/types/CommandlineJava;

    move-result-object v0

    invoke-virtual {v0}, Lorg/apache/tools/ant/types/CommandlineJava;->createVmArgument()Lorg/apache/tools/ant/types/Commandline$Argument;

    move-result-object v0

    return-object v0
.end method

.method public createPermissions()Lorg/apache/tools/ant/types/Permissions;
    .locals 1

    iget-object v0, p0, Lorg/apache/tools/ant/taskdefs/Java;->perm:Lorg/apache/tools/ant/types/Permissions;

    if-nez v0, :cond_0

    new-instance v0, Lorg/apache/tools/ant/types/Permissions;

    invoke-direct {v0}, Lorg/apache/tools/ant/types/Permissions;-><init>()V

    :goto_0
    iput-object v0, p0, Lorg/apache/tools/ant/taskdefs/Java;->perm:Lorg/apache/tools/ant/types/Permissions;

    iget-object v0, p0, Lorg/apache/tools/ant/taskdefs/Java;->perm:Lorg/apache/tools/ant/types/Permissions;

    return-object v0

    :cond_0
    iget-object v0, p0, Lorg/apache/tools/ant/taskdefs/Java;->perm:Lorg/apache/tools/ant/types/Permissions;

    goto :goto_0
.end method

.method protected createWatchdog()Lorg/apache/tools/ant/taskdefs/ExecuteWatchdog;
    .locals 3
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lorg/apache/tools/ant/BuildException;
        }
    .end annotation

    iget-object v0, p0, Lorg/apache/tools/ant/taskdefs/Java;->timeout:Ljava/lang/Long;

    if-nez v0, :cond_0

    const/4 v0, 0x0

    :goto_0
    return-object v0

    :cond_0
    new-instance v0, Lorg/apache/tools/ant/taskdefs/ExecuteWatchdog;

    iget-object v1, p0, Lorg/apache/tools/ant/taskdefs/Java;->timeout:Ljava/lang/Long;

    invoke-virtual {v1}, Ljava/lang/Long;->longValue()J

    move-result-wide v1

    invoke-direct {v0, v1, v2}, Lorg/apache/tools/ant/taskdefs/ExecuteWatchdog;-><init>(J)V

    goto :goto_0
.end method

.method public execute()V
    .locals 6
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lorg/apache/tools/ant/BuildException;
        }
    .end annotation

    iget-object v1, p0, Lorg/apache/tools/ant/taskdefs/Java;->dir:Ljava/io/File;

    iget-object v2, p0, Lorg/apache/tools/ant/taskdefs/Java;->perm:Lorg/apache/tools/ant/types/Permissions;

    const/4 v0, -0x1

    :try_start_0
    invoke-virtual {p0}, Lorg/apache/tools/ant/taskdefs/Java;->executeJava()I

    move-result v0

    if-eqz v0, :cond_1

    iget-boolean v3, p0, Lorg/apache/tools/ant/taskdefs/Java;->failOnError:Z

    if-eqz v3, :cond_0

    new-instance v3, Lorg/apache/tools/ant/ExitStatusException;

    new-instance v4, Ljava/lang/StringBuffer;

    invoke-direct {v4}, Ljava/lang/StringBuffer;-><init>()V

    const-string v5, "Java returned: "

    invoke-virtual {v4, v5}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    move-result-object v4

    invoke-virtual {v4, v0}, Ljava/lang/StringBuffer;->append(I)Ljava/lang/StringBuffer;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuffer;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {p0}, Lorg/apache/tools/ant/taskdefs/Java;->getLocation()Lorg/apache/tools/ant/Location;

    move-result-object v5

    invoke-direct {v3, v4, v0, v5}, Lorg/apache/tools/ant/ExitStatusException;-><init>(Ljava/lang/String;ILorg/apache/tools/ant/Location;)V

    throw v3
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    :catchall_0
    move-exception v3

    iput-object v1, p0, Lorg/apache/tools/ant/taskdefs/Java;->dir:Ljava/io/File;

    iput-object v2, p0, Lorg/apache/tools/ant/taskdefs/Java;->perm:Lorg/apache/tools/ant/types/Permissions;

    throw v3

    :cond_0
    :try_start_1
    new-instance v3, Ljava/lang/StringBuffer;

    invoke-direct {v3}, Ljava/lang/StringBuffer;-><init>()V

    const-string v4, "Java Result: "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    move-result-object v3

    invoke-virtual {v3, v0}, Ljava/lang/StringBuffer;->append(I)Ljava/lang/StringBuffer;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuffer;->toString()Ljava/lang/String;

    move-result-object v3

    const/4 v4, 0x0

    invoke-virtual {p0, v3, v4}, Lorg/apache/tools/ant/taskdefs/Java;->log(Ljava/lang/String;I)V

    :cond_1
    invoke-virtual {p0, v0}, Lorg/apache/tools/ant/taskdefs/Java;->maybeSetResultPropertyValue(I)V
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    iput-object v1, p0, Lorg/apache/tools/ant/taskdefs/Java;->dir:Ljava/io/File;

    iput-object v2, p0, Lorg/apache/tools/ant/taskdefs/Java;->perm:Lorg/apache/tools/ant/types/Permissions;

    return-void
.end method

.method public executeJava()I
    .locals 8
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lorg/apache/tools/ant/BuildException;
        }
    .end annotation

    const/4 v7, 0x3

    const/4 v6, 0x1

    const/4 v4, 0x0

    invoke-virtual {p0}, Lorg/apache/tools/ant/taskdefs/Java;->getCommandLine()Lorg/apache/tools/ant/types/CommandlineJava;

    move-result-object v5

    invoke-virtual {v5}, Lorg/apache/tools/ant/types/CommandlineJava;->getClassname()Ljava/lang/String;

    move-result-object v0

    if-nez v0, :cond_0

    invoke-virtual {p0}, Lorg/apache/tools/ant/taskdefs/Java;->getCommandLine()Lorg/apache/tools/ant/types/CommandlineJava;

    move-result-object v5

    invoke-virtual {v5}, Lorg/apache/tools/ant/types/CommandlineJava;->getJar()Ljava/lang/String;

    move-result-object v5

    if-nez v5, :cond_0

    new-instance v4, Lorg/apache/tools/ant/BuildException;

    const-string v5, "Classname must not be null."

    invoke-direct {v4, v5}, Lorg/apache/tools/ant/BuildException;-><init>(Ljava/lang/String;)V

    throw v4

    :cond_0
    iget-boolean v5, p0, Lorg/apache/tools/ant/taskdefs/Java;->fork:Z

    if-nez v5, :cond_1

    invoke-virtual {p0}, Lorg/apache/tools/ant/taskdefs/Java;->getCommandLine()Lorg/apache/tools/ant/types/CommandlineJava;

    move-result-object v5

    invoke-virtual {v5}, Lorg/apache/tools/ant/types/CommandlineJava;->getJar()Ljava/lang/String;

    move-result-object v5

    if-eqz v5, :cond_1

    new-instance v4, Lorg/apache/tools/ant/BuildException;

    const-string v5, "Cannot execute a jar in non-forked mode. Please set fork=\'true\'. "

    invoke-direct {v4, v5}, Lorg/apache/tools/ant/BuildException;-><init>(Ljava/lang/String;)V

    throw v4

    :cond_1
    iget-boolean v5, p0, Lorg/apache/tools/ant/taskdefs/Java;->spawn:Z

    if-eqz v5, :cond_2

    iget-boolean v5, p0, Lorg/apache/tools/ant/taskdefs/Java;->fork:Z

    if-nez v5, :cond_2

    new-instance v4, Lorg/apache/tools/ant/BuildException;

    const-string v5, "Cannot spawn a java process in non-forked mode. Please set fork=\'true\'. "

    invoke-direct {v4, v5}, Lorg/apache/tools/ant/BuildException;-><init>(Ljava/lang/String;)V

    throw v4

    :cond_2
    invoke-virtual {p0}, Lorg/apache/tools/ant/taskdefs/Java;->getCommandLine()Lorg/apache/tools/ant/types/CommandlineJava;

    move-result-object v5

    invoke-virtual {v5}, Lorg/apache/tools/ant/types/CommandlineJava;->getClasspath()Lorg/apache/tools/ant/types/Path;

    move-result-object v5

    if-eqz v5, :cond_3

    invoke-virtual {p0}, Lorg/apache/tools/ant/taskdefs/Java;->getCommandLine()Lorg/apache/tools/ant/types/CommandlineJava;

    move-result-object v5

    invoke-virtual {v5}, Lorg/apache/tools/ant/types/CommandlineJava;->getJar()Ljava/lang/String;

    move-result-object v5

    if-eqz v5, :cond_3

    const-string v5, "When using \'jar\' attribute classpath-settings are ignored. See the manual for more information."

    invoke-virtual {p0, v5, v7}, Lorg/apache/tools/ant/taskdefs/Java;->log(Ljava/lang/String;I)V

    :cond_3
    iget-boolean v5, p0, Lorg/apache/tools/ant/taskdefs/Java;->spawn:Z

    if-eqz v5, :cond_4

    iget-boolean v5, p0, Lorg/apache/tools/ant/taskdefs/Java;->incompatibleWithSpawn:Z

    if-eqz v5, :cond_4

    invoke-virtual {p0}, Lorg/apache/tools/ant/taskdefs/Java;->getProject()Lorg/apache/tools/ant/Project;

    move-result-object v5

    const-string v6, "spawn does not allow attributes related to input, output, error, result"

    invoke-virtual {v5, v6, v4}, Lorg/apache/tools/ant/Project;->log(Ljava/lang/String;I)V

    invoke-virtual {p0}, Lorg/apache/tools/ant/taskdefs/Java;->getProject()Lorg/apache/tools/ant/Project;

    move-result-object v5

    const-string v6, "spawn also does not allow timeout"

    invoke-virtual {v5, v6, v4}, Lorg/apache/tools/ant/Project;->log(Ljava/lang/String;I)V

    invoke-virtual {p0}, Lorg/apache/tools/ant/taskdefs/Java;->getProject()Lorg/apache/tools/ant/Project;

    move-result-object v5

    const-string v6, "finally, spawn is not compatible with a nested I/O <redirector>"

    invoke-virtual {v5, v6, v4}, Lorg/apache/tools/ant/Project;->log(Ljava/lang/String;I)V

    new-instance v4, Lorg/apache/tools/ant/BuildException;

    const-string v5, "You have used an attribute or nested element which is not compatible with spawn"

    invoke-direct {v4, v5}, Lorg/apache/tools/ant/BuildException;-><init>(Ljava/lang/String;)V

    throw v4

    :cond_4
    invoke-virtual {p0}, Lorg/apache/tools/ant/taskdefs/Java;->getCommandLine()Lorg/apache/tools/ant/types/CommandlineJava;

    move-result-object v5

    invoke-virtual {v5}, Lorg/apache/tools/ant/types/CommandlineJava;->getAssertions()Lorg/apache/tools/ant/types/Assertions;

    move-result-object v5

    if-eqz v5, :cond_5

    iget-boolean v5, p0, Lorg/apache/tools/ant/taskdefs/Java;->fork:Z

    if-nez v5, :cond_5

    const-string v5, "Assertion statements are currently ignored in non-forked mode"

    invoke-virtual {p0, v5}, Lorg/apache/tools/ant/taskdefs/Java;->log(Ljava/lang/String;)V

    :cond_5
    iget-boolean v5, p0, Lorg/apache/tools/ant/taskdefs/Java;->fork:Z

    if-eqz v5, :cond_7

    iget-object v5, p0, Lorg/apache/tools/ant/taskdefs/Java;->perm:Lorg/apache/tools/ant/types/Permissions;

    if-eqz v5, :cond_6

    const-string v5, "Permissions can not be set this way in forked mode."

    invoke-virtual {p0, v5, v6}, Lorg/apache/tools/ant/taskdefs/Java;->log(Ljava/lang/String;I)V

    :cond_6
    invoke-virtual {p0}, Lorg/apache/tools/ant/taskdefs/Java;->getCommandLine()Lorg/apache/tools/ant/types/CommandlineJava;

    move-result-object v5

    invoke-virtual {v5}, Lorg/apache/tools/ant/types/CommandlineJava;->describeCommand()Ljava/lang/String;

    move-result-object v5

    invoke-virtual {p0, v5, v7}, Lorg/apache/tools/ant/taskdefs/Java;->log(Ljava/lang/String;I)V

    :goto_0
    invoke-virtual {p0}, Lorg/apache/tools/ant/taskdefs/Java;->setupRedirector()V

    :try_start_0
    iget-boolean v5, p0, Lorg/apache/tools/ant/taskdefs/Java;->fork:Z

    if-eqz v5, :cond_10

    iget-boolean v5, p0, Lorg/apache/tools/ant/taskdefs/Java;->spawn:Z

    if-nez v5, :cond_e

    invoke-virtual {p0}, Lorg/apache/tools/ant/taskdefs/Java;->getCommandLine()Lorg/apache/tools/ant/types/CommandlineJava;

    move-result-object v5

    invoke-virtual {v5}, Lorg/apache/tools/ant/types/CommandlineJava;->getCommandline()[Ljava/lang/String;

    move-result-object v5

    invoke-direct {p0, v5}, Lorg/apache/tools/ant/taskdefs/Java;->fork([Ljava/lang/String;)I
    :try_end_0
    .catch Lorg/apache/tools/ant/BuildException; {:try_start_0 .. :try_end_0} :catch_0
    .catch Ljava/lang/ThreadDeath; {:try_start_0 .. :try_end_0} :catch_2
    .catch Ljava/lang/Throwable; {:try_start_0 .. :try_end_0} :catch_3

    move-result v4

    :goto_1
    return v4

    :cond_7
    invoke-virtual {p0}, Lorg/apache/tools/ant/taskdefs/Java;->getCommandLine()Lorg/apache/tools/ant/types/CommandlineJava;

    move-result-object v5

    invoke-virtual {v5}, Lorg/apache/tools/ant/types/CommandlineJava;->getVmCommand()Lorg/apache/tools/ant/types/Commandline;

    move-result-object v5

    invoke-virtual {v5}, Lorg/apache/tools/ant/types/Commandline;->size()I

    move-result v5

    if-le v5, v6, :cond_8

    const-string v5, "JVM args ignored when same JVM is used."

    invoke-virtual {p0, v5, v6}, Lorg/apache/tools/ant/taskdefs/Java;->log(Ljava/lang/String;I)V

    :cond_8
    iget-object v5, p0, Lorg/apache/tools/ant/taskdefs/Java;->dir:Ljava/io/File;

    if-eqz v5, :cond_9

    const-string v5, "Working directory ignored when same JVM is used."

    invoke-virtual {p0, v5, v6}, Lorg/apache/tools/ant/taskdefs/Java;->log(Ljava/lang/String;I)V

    :cond_9
    iget-boolean v5, p0, Lorg/apache/tools/ant/taskdefs/Java;->newEnvironment:Z

    if-nez v5, :cond_a

    iget-object v5, p0, Lorg/apache/tools/ant/taskdefs/Java;->env:Lorg/apache/tools/ant/types/Environment;

    invoke-virtual {v5}, Lorg/apache/tools/ant/types/Environment;->getVariables()[Ljava/lang/String;

    move-result-object v5

    if-eqz v5, :cond_b

    :cond_a
    const-string v5, "Changes to environment variables are ignored when same JVM is used."

    invoke-virtual {p0, v5, v6}, Lorg/apache/tools/ant/taskdefs/Java;->log(Ljava/lang/String;I)V

    :cond_b
    invoke-virtual {p0}, Lorg/apache/tools/ant/taskdefs/Java;->getCommandLine()Lorg/apache/tools/ant/types/CommandlineJava;

    move-result-object v5

    invoke-virtual {v5}, Lorg/apache/tools/ant/types/CommandlineJava;->getBootclasspath()Lorg/apache/tools/ant/types/Path;

    move-result-object v5

    if-eqz v5, :cond_c

    const-string v5, "bootclasspath ignored when same JVM is used."

    invoke-virtual {p0, v5, v6}, Lorg/apache/tools/ant/taskdefs/Java;->log(Ljava/lang/String;I)V

    :cond_c
    iget-object v5, p0, Lorg/apache/tools/ant/taskdefs/Java;->perm:Lorg/apache/tools/ant/types/Permissions;

    if-nez v5, :cond_d

    new-instance v5, Lorg/apache/tools/ant/types/Permissions;

    invoke-direct {v5, v6}, Lorg/apache/tools/ant/types/Permissions;-><init>(Z)V

    iput-object v5, p0, Lorg/apache/tools/ant/taskdefs/Java;->perm:Lorg/apache/tools/ant/types/Permissions;

    new-instance v5, Ljava/lang/StringBuffer;

    invoke-direct {v5}, Ljava/lang/StringBuffer;-><init>()V

    const-string v6, "running "

    invoke-virtual {v5, v6}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    move-result-object v5

    invoke-virtual {p0}, Lorg/apache/tools/ant/taskdefs/Java;->getCommandLine()Lorg/apache/tools/ant/types/CommandlineJava;

    move-result-object v6

    invoke-virtual {v6}, Lorg/apache/tools/ant/types/CommandlineJava;->getClassname()Ljava/lang/String;

    move-result-object v6

    invoke-virtual {v5, v6}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    move-result-object v5

    const-string v6, " with default permissions (exit forbidden)"

    invoke-virtual {v5, v6}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/StringBuffer;->toString()Ljava/lang/String;

    move-result-object v5

    invoke-virtual {p0, v5, v7}, Lorg/apache/tools/ant/taskdefs/Java;->log(Ljava/lang/String;I)V

    :cond_d
    new-instance v5, Ljava/lang/StringBuffer;

    invoke-direct {v5}, Ljava/lang/StringBuffer;-><init>()V

    const-string v6, "Running in same VM "

    invoke-virtual {v5, v6}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    move-result-object v5

    invoke-virtual {p0}, Lorg/apache/tools/ant/taskdefs/Java;->getCommandLine()Lorg/apache/tools/ant/types/CommandlineJava;

    move-result-object v6

    invoke-virtual {v6}, Lorg/apache/tools/ant/types/CommandlineJava;->describeJavaCommand()Ljava/lang/String;

    move-result-object v6

    invoke-virtual {v5, v6}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/StringBuffer;->toString()Ljava/lang/String;

    move-result-object v5

    invoke-virtual {p0, v5, v7}, Lorg/apache/tools/ant/taskdefs/Java;->log(Ljava/lang/String;I)V

    goto/16 :goto_0

    :cond_e
    :try_start_1
    invoke-virtual {p0}, Lorg/apache/tools/ant/taskdefs/Java;->getCommandLine()Lorg/apache/tools/ant/types/CommandlineJava;

    move-result-object v5

    invoke-virtual {v5}, Lorg/apache/tools/ant/types/CommandlineJava;->getCommandline()[Ljava/lang/String;

    move-result-object v5

    invoke-direct {p0, v5}, Lorg/apache/tools/ant/taskdefs/Java;->spawn([Ljava/lang/String;)V
    :try_end_1
    .catch Lorg/apache/tools/ant/BuildException; {:try_start_1 .. :try_end_1} :catch_0
    .catch Ljava/lang/ThreadDeath; {:try_start_1 .. :try_end_1} :catch_2
    .catch Ljava/lang/Throwable; {:try_start_1 .. :try_end_1} :catch_3

    goto/16 :goto_1

    :catch_0
    move-exception v1

    invoke-virtual {v1}, Lorg/apache/tools/ant/BuildException;->getLocation()Lorg/apache/tools/ant/Location;

    move-result-object v5

    if-nez v5, :cond_f

    invoke-virtual {p0}, Lorg/apache/tools/ant/taskdefs/Java;->getLocation()Lorg/apache/tools/ant/Location;

    move-result-object v5

    if-eqz v5, :cond_f

    invoke-virtual {p0}, Lorg/apache/tools/ant/taskdefs/Java;->getLocation()Lorg/apache/tools/ant/Location;

    move-result-object v5

    invoke-virtual {v1, v5}, Lorg/apache/tools/ant/BuildException;->setLocation(Lorg/apache/tools/ant/Location;)V

    :cond_f
    iget-boolean v5, p0, Lorg/apache/tools/ant/taskdefs/Java;->failOnError:Z

    if-eqz v5, :cond_11

    throw v1

    :cond_10
    :try_start_2
    invoke-virtual {p0}, Lorg/apache/tools/ant/taskdefs/Java;->getCommandLine()Lorg/apache/tools/ant/types/CommandlineJava;

    move-result-object v5

    invoke-direct {p0, v5}, Lorg/apache/tools/ant/taskdefs/Java;->run(Lorg/apache/tools/ant/types/CommandlineJava;)V
    :try_end_2
    .catch Lorg/apache/tools/ant/ExitException; {:try_start_2 .. :try_end_2} :catch_1
    .catch Lorg/apache/tools/ant/BuildException; {:try_start_2 .. :try_end_2} :catch_0
    .catch Ljava/lang/ThreadDeath; {:try_start_2 .. :try_end_2} :catch_2
    .catch Ljava/lang/Throwable; {:try_start_2 .. :try_end_2} :catch_3

    goto/16 :goto_1

    :catch_1
    move-exception v2

    :try_start_3
    invoke-virtual {v2}, Lorg/apache/tools/ant/ExitException;->getStatus()I
    :try_end_3
    .catch Lorg/apache/tools/ant/BuildException; {:try_start_3 .. :try_end_3} :catch_0
    .catch Ljava/lang/ThreadDeath; {:try_start_3 .. :try_end_3} :catch_2
    .catch Ljava/lang/Throwable; {:try_start_3 .. :try_end_3} :catch_3

    move-result v4

    goto/16 :goto_1

    :cond_11
    invoke-direct {p0, v1}, Lorg/apache/tools/ant/taskdefs/Java;->log(Ljava/lang/Throwable;)V

    goto/16 :goto_1

    :catch_2
    move-exception v3

    throw v3

    :catch_3
    move-exception v3

    iget-boolean v5, p0, Lorg/apache/tools/ant/taskdefs/Java;->failOnError:Z

    if-eqz v5, :cond_12

    new-instance v4, Lorg/apache/tools/ant/BuildException;

    invoke-virtual {p0}, Lorg/apache/tools/ant/taskdefs/Java;->getLocation()Lorg/apache/tools/ant/Location;

    move-result-object v5

    invoke-direct {v4, v3, v5}, Lorg/apache/tools/ant/BuildException;-><init>(Ljava/lang/Throwable;Lorg/apache/tools/ant/Location;)V

    throw v4

    :cond_12
    invoke-direct {p0, v3}, Lorg/apache/tools/ant/taskdefs/Java;->log(Ljava/lang/Throwable;)V

    goto/16 :goto_1
.end method

.method public getCommandLine()Lorg/apache/tools/ant/types/CommandlineJava;
    .locals 1

    iget-object v0, p0, Lorg/apache/tools/ant/taskdefs/Java;->cmdl:Lorg/apache/tools/ant/types/CommandlineJava;

    return-object v0
.end method

.method public getSysProperties()Lorg/apache/tools/ant/types/CommandlineJava$SysProperties;
    .locals 1

    invoke-virtual {p0}, Lorg/apache/tools/ant/taskdefs/Java;->getCommandLine()Lorg/apache/tools/ant/types/CommandlineJava;

    move-result-object v0

    invoke-virtual {v0}, Lorg/apache/tools/ant/types/CommandlineJava;->getSystemProperties()Lorg/apache/tools/ant/types/CommandlineJava$SysProperties;

    move-result-object v0

    return-object v0
.end method

.method protected handleErrorFlush(Ljava/lang/String;)V
    .locals 1
    .param p1    # Ljava/lang/String;

    iget-object v0, p0, Lorg/apache/tools/ant/taskdefs/Java;->redirector:Lorg/apache/tools/ant/taskdefs/Redirector;

    invoke-virtual {v0}, Lorg/apache/tools/ant/taskdefs/Redirector;->getErrorStream()Ljava/io/OutputStream;

    move-result-object v0

    if-eqz v0, :cond_0

    iget-object v0, p0, Lorg/apache/tools/ant/taskdefs/Java;->redirector:Lorg/apache/tools/ant/taskdefs/Redirector;

    invoke-virtual {v0, p1}, Lorg/apache/tools/ant/taskdefs/Redirector;->handleErrorFlush(Ljava/lang/String;)V

    :goto_0
    return-void

    :cond_0
    invoke-super {p0, p1}, Lorg/apache/tools/ant/Task;->handleErrorOutput(Ljava/lang/String;)V

    goto :goto_0
.end method

.method protected handleErrorOutput(Ljava/lang/String;)V
    .locals 1
    .param p1    # Ljava/lang/String;

    iget-object v0, p0, Lorg/apache/tools/ant/taskdefs/Java;->redirector:Lorg/apache/tools/ant/taskdefs/Redirector;

    invoke-virtual {v0}, Lorg/apache/tools/ant/taskdefs/Redirector;->getErrorStream()Ljava/io/OutputStream;

    move-result-object v0

    if-eqz v0, :cond_0

    iget-object v0, p0, Lorg/apache/tools/ant/taskdefs/Java;->redirector:Lorg/apache/tools/ant/taskdefs/Redirector;

    invoke-virtual {v0, p1}, Lorg/apache/tools/ant/taskdefs/Redirector;->handleErrorOutput(Ljava/lang/String;)V

    :goto_0
    return-void

    :cond_0
    invoke-super {p0, p1}, Lorg/apache/tools/ant/Task;->handleErrorOutput(Ljava/lang/String;)V

    goto :goto_0
.end method

.method protected handleFlush(Ljava/lang/String;)V
    .locals 1
    .param p1    # Ljava/lang/String;

    iget-object v0, p0, Lorg/apache/tools/ant/taskdefs/Java;->redirector:Lorg/apache/tools/ant/taskdefs/Redirector;

    invoke-virtual {v0}, Lorg/apache/tools/ant/taskdefs/Redirector;->getOutputStream()Ljava/io/OutputStream;

    move-result-object v0

    if-eqz v0, :cond_0

    iget-object v0, p0, Lorg/apache/tools/ant/taskdefs/Java;->redirector:Lorg/apache/tools/ant/taskdefs/Redirector;

    invoke-virtual {v0, p1}, Lorg/apache/tools/ant/taskdefs/Redirector;->handleFlush(Ljava/lang/String;)V

    :goto_0
    return-void

    :cond_0
    invoke-super {p0, p1}, Lorg/apache/tools/ant/Task;->handleFlush(Ljava/lang/String;)V

    goto :goto_0
.end method

.method public handleInput([BII)I
    .locals 1
    .param p1    # [B
    .param p2    # I
    .param p3    # I
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    iget-object v0, p0, Lorg/apache/tools/ant/taskdefs/Java;->redirector:Lorg/apache/tools/ant/taskdefs/Redirector;

    invoke-virtual {v0, p1, p2, p3}, Lorg/apache/tools/ant/taskdefs/Redirector;->handleInput([BII)I

    move-result v0

    return v0
.end method

.method protected handleOutput(Ljava/lang/String;)V
    .locals 1
    .param p1    # Ljava/lang/String;

    iget-object v0, p0, Lorg/apache/tools/ant/taskdefs/Java;->redirector:Lorg/apache/tools/ant/taskdefs/Redirector;

    invoke-virtual {v0}, Lorg/apache/tools/ant/taskdefs/Redirector;->getOutputStream()Ljava/io/OutputStream;

    move-result-object v0

    if-eqz v0, :cond_0

    iget-object v0, p0, Lorg/apache/tools/ant/taskdefs/Java;->redirector:Lorg/apache/tools/ant/taskdefs/Redirector;

    invoke-virtual {v0, p1}, Lorg/apache/tools/ant/taskdefs/Redirector;->handleOutput(Ljava/lang/String;)V

    :goto_0
    return-void

    :cond_0
    invoke-super {p0, p1}, Lorg/apache/tools/ant/Task;->handleOutput(Ljava/lang/String;)V

    goto :goto_0
.end method

.method protected maybeSetResultPropertyValue(I)V
    .locals 3
    .param p1    # I

    invoke-static {p1}, Ljava/lang/Integer;->toString(I)Ljava/lang/String;

    move-result-object v0

    iget-object v1, p0, Lorg/apache/tools/ant/taskdefs/Java;->resultProperty:Ljava/lang/String;

    if-eqz v1, :cond_0

    invoke-virtual {p0}, Lorg/apache/tools/ant/taskdefs/Java;->getProject()Lorg/apache/tools/ant/Project;

    move-result-object v1

    iget-object v2, p0, Lorg/apache/tools/ant/taskdefs/Java;->resultProperty:Ljava/lang/String;

    invoke-virtual {v1, v2, v0}, Lorg/apache/tools/ant/Project;->setNewProperty(Ljava/lang/String;Ljava/lang/String;)V

    :cond_0
    return-void
.end method

.method protected run(Ljava/lang/String;Ljava/util/Vector;)V
    .locals 4
    .param p1    # Ljava/lang/String;
    .param p2    # Ljava/util/Vector;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lorg/apache/tools/ant/BuildException;
        }
    .end annotation

    new-instance v0, Lorg/apache/tools/ant/types/CommandlineJava;

    invoke-direct {v0}, Lorg/apache/tools/ant/types/CommandlineJava;-><init>()V

    invoke-virtual {v0, p1}, Lorg/apache/tools/ant/types/CommandlineJava;->setClassname(Ljava/lang/String;)V

    const/4 v1, 0x0

    :goto_0
    invoke-virtual {p2}, Ljava/util/Vector;->size()I

    move-result v2

    if-ge v1, v2, :cond_0

    invoke-virtual {v0}, Lorg/apache/tools/ant/types/CommandlineJava;->createArgument()Lorg/apache/tools/ant/types/Commandline$Argument;

    move-result-object v3

    invoke-virtual {p2, v1}, Ljava/util/Vector;->elementAt(I)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Ljava/lang/String;

    invoke-virtual {v3, v2}, Lorg/apache/tools/ant/types/Commandline$Argument;->setValue(Ljava/lang/String;)V

    add-int/lit8 v1, v1, 0x1

    goto :goto_0

    :cond_0
    invoke-direct {p0, v0}, Lorg/apache/tools/ant/taskdefs/Java;->run(Lorg/apache/tools/ant/types/CommandlineJava;)V

    return-void
.end method

.method public setAppend(Z)V
    .locals 1
    .param p1    # Z

    iget-object v0, p0, Lorg/apache/tools/ant/taskdefs/Java;->redirector:Lorg/apache/tools/ant/taskdefs/Redirector;

    invoke-virtual {v0, p1}, Lorg/apache/tools/ant/taskdefs/Redirector;->setAppend(Z)V

    const/4 v0, 0x1

    iput-boolean v0, p0, Lorg/apache/tools/ant/taskdefs/Java;->incompatibleWithSpawn:Z

    return-void
.end method

.method public setArgs(Ljava/lang/String;)V
    .locals 2
    .param p1    # Ljava/lang/String;

    const-string v0, "The args attribute is deprecated. Please use nested arg elements."

    const/4 v1, 0x1

    invoke-virtual {p0, v0, v1}, Lorg/apache/tools/ant/taskdefs/Java;->log(Ljava/lang/String;I)V

    invoke-virtual {p0}, Lorg/apache/tools/ant/taskdefs/Java;->getCommandLine()Lorg/apache/tools/ant/types/CommandlineJava;

    move-result-object v0

    invoke-virtual {v0}, Lorg/apache/tools/ant/types/CommandlineJava;->createArgument()Lorg/apache/tools/ant/types/Commandline$Argument;

    move-result-object v0

    invoke-virtual {v0, p1}, Lorg/apache/tools/ant/types/Commandline$Argument;->setLine(Ljava/lang/String;)V

    return-void
.end method

.method public setClassname(Ljava/lang/String;)V
    .locals 2
    .param p1    # Ljava/lang/String;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lorg/apache/tools/ant/BuildException;
        }
    .end annotation

    invoke-virtual {p0}, Lorg/apache/tools/ant/taskdefs/Java;->getCommandLine()Lorg/apache/tools/ant/types/CommandlineJava;

    move-result-object v0

    invoke-virtual {v0}, Lorg/apache/tools/ant/types/CommandlineJava;->getJar()Ljava/lang/String;

    move-result-object v0

    if-eqz v0, :cond_0

    new-instance v0, Lorg/apache/tools/ant/BuildException;

    const-string v1, "Cannot use \'jar\' and \'classname\' attributes in same command"

    invoke-direct {v0, v1}, Lorg/apache/tools/ant/BuildException;-><init>(Ljava/lang/String;)V

    throw v0

    :cond_0
    invoke-virtual {p0}, Lorg/apache/tools/ant/taskdefs/Java;->getCommandLine()Lorg/apache/tools/ant/types/CommandlineJava;

    move-result-object v0

    invoke-virtual {v0, p1}, Lorg/apache/tools/ant/types/CommandlineJava;->setClassname(Ljava/lang/String;)V

    return-void
.end method

.method public setClasspath(Lorg/apache/tools/ant/types/Path;)V
    .locals 1
    .param p1    # Lorg/apache/tools/ant/types/Path;

    invoke-virtual {p0}, Lorg/apache/tools/ant/taskdefs/Java;->createClasspath()Lorg/apache/tools/ant/types/Path;

    move-result-object v0

    invoke-virtual {v0, p1}, Lorg/apache/tools/ant/types/Path;->append(Lorg/apache/tools/ant/types/Path;)V

    return-void
.end method

.method public setClasspathRef(Lorg/apache/tools/ant/types/Reference;)V
    .locals 1
    .param p1    # Lorg/apache/tools/ant/types/Reference;

    invoke-virtual {p0}, Lorg/apache/tools/ant/taskdefs/Java;->createClasspath()Lorg/apache/tools/ant/types/Path;

    move-result-object v0

    invoke-virtual {v0, p1}, Lorg/apache/tools/ant/types/Path;->setRefid(Lorg/apache/tools/ant/types/Reference;)V

    return-void
.end method

.method public setCloneVm(Z)V
    .locals 1
    .param p1    # Z

    invoke-virtual {p0}, Lorg/apache/tools/ant/taskdefs/Java;->getCommandLine()Lorg/apache/tools/ant/types/CommandlineJava;

    move-result-object v0

    invoke-virtual {v0, p1}, Lorg/apache/tools/ant/types/CommandlineJava;->setCloneVm(Z)V

    return-void
.end method

.method public setDir(Ljava/io/File;)V
    .locals 0
    .param p1    # Ljava/io/File;

    iput-object p1, p0, Lorg/apache/tools/ant/taskdefs/Java;->dir:Ljava/io/File;

    return-void
.end method

.method public setError(Ljava/io/File;)V
    .locals 1
    .param p1    # Ljava/io/File;

    iput-object p1, p0, Lorg/apache/tools/ant/taskdefs/Java;->error:Ljava/io/File;

    const/4 v0, 0x1

    iput-boolean v0, p0, Lorg/apache/tools/ant/taskdefs/Java;->incompatibleWithSpawn:Z

    return-void
.end method

.method public setErrorProperty(Ljava/lang/String;)V
    .locals 1
    .param p1    # Ljava/lang/String;

    iget-object v0, p0, Lorg/apache/tools/ant/taskdefs/Java;->redirector:Lorg/apache/tools/ant/taskdefs/Redirector;

    invoke-virtual {v0, p1}, Lorg/apache/tools/ant/taskdefs/Redirector;->setErrorProperty(Ljava/lang/String;)V

    const/4 v0, 0x1

    iput-boolean v0, p0, Lorg/apache/tools/ant/taskdefs/Java;->incompatibleWithSpawn:Z

    return-void
.end method

.method public setFailonerror(Z)V
    .locals 1
    .param p1    # Z

    iput-boolean p1, p0, Lorg/apache/tools/ant/taskdefs/Java;->failOnError:Z

    iget-boolean v0, p0, Lorg/apache/tools/ant/taskdefs/Java;->incompatibleWithSpawn:Z

    or-int/2addr v0, p1

    iput-boolean v0, p0, Lorg/apache/tools/ant/taskdefs/Java;->incompatibleWithSpawn:Z

    return-void
.end method

.method public setFork(Z)V
    .locals 0
    .param p1    # Z

    iput-boolean p1, p0, Lorg/apache/tools/ant/taskdefs/Java;->fork:Z

    return-void
.end method

.method public setInput(Ljava/io/File;)V
    .locals 2
    .param p1    # Ljava/io/File;

    iget-object v0, p0, Lorg/apache/tools/ant/taskdefs/Java;->inputString:Ljava/lang/String;

    if-eqz v0, :cond_0

    new-instance v0, Lorg/apache/tools/ant/BuildException;

    const-string v1, "The \"input\" and \"inputstring\" attributes cannot both be specified"

    invoke-direct {v0, v1}, Lorg/apache/tools/ant/BuildException;-><init>(Ljava/lang/String;)V

    throw v0

    :cond_0
    iput-object p1, p0, Lorg/apache/tools/ant/taskdefs/Java;->input:Ljava/io/File;

    const/4 v0, 0x1

    iput-boolean v0, p0, Lorg/apache/tools/ant/taskdefs/Java;->incompatibleWithSpawn:Z

    return-void
.end method

.method public setInputString(Ljava/lang/String;)V
    .locals 2
    .param p1    # Ljava/lang/String;

    iget-object v0, p0, Lorg/apache/tools/ant/taskdefs/Java;->input:Ljava/io/File;

    if-eqz v0, :cond_0

    new-instance v0, Lorg/apache/tools/ant/BuildException;

    const-string v1, "The \"input\" and \"inputstring\" attributes cannot both be specified"

    invoke-direct {v0, v1}, Lorg/apache/tools/ant/BuildException;-><init>(Ljava/lang/String;)V

    throw v0

    :cond_0
    iput-object p1, p0, Lorg/apache/tools/ant/taskdefs/Java;->inputString:Ljava/lang/String;

    const/4 v0, 0x1

    iput-boolean v0, p0, Lorg/apache/tools/ant/taskdefs/Java;->incompatibleWithSpawn:Z

    return-void
.end method

.method public setJVMVersion(Ljava/lang/String;)V
    .locals 1
    .param p1    # Ljava/lang/String;

    invoke-virtual {p0}, Lorg/apache/tools/ant/taskdefs/Java;->getCommandLine()Lorg/apache/tools/ant/types/CommandlineJava;

    move-result-object v0

    invoke-virtual {v0, p1}, Lorg/apache/tools/ant/types/CommandlineJava;->setVmversion(Ljava/lang/String;)V

    return-void
.end method

.method public setJar(Ljava/io/File;)V
    .locals 2
    .param p1    # Ljava/io/File;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lorg/apache/tools/ant/BuildException;
        }
    .end annotation

    invoke-virtual {p0}, Lorg/apache/tools/ant/taskdefs/Java;->getCommandLine()Lorg/apache/tools/ant/types/CommandlineJava;

    move-result-object v0

    invoke-virtual {v0}, Lorg/apache/tools/ant/types/CommandlineJava;->getClassname()Ljava/lang/String;

    move-result-object v0

    if-eqz v0, :cond_0

    new-instance v0, Lorg/apache/tools/ant/BuildException;

    const-string v1, "Cannot use \'jar\' and \'classname\' attributes in same command."

    invoke-direct {v0, v1}, Lorg/apache/tools/ant/BuildException;-><init>(Ljava/lang/String;)V

    throw v0

    :cond_0
    invoke-virtual {p0}, Lorg/apache/tools/ant/taskdefs/Java;->getCommandLine()Lorg/apache/tools/ant/types/CommandlineJava;

    move-result-object v0

    invoke-virtual {p1}, Ljava/io/File;->getAbsolutePath()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Lorg/apache/tools/ant/types/CommandlineJava;->setJar(Ljava/lang/String;)V

    return-void
.end method

.method public setJvm(Ljava/lang/String;)V
    .locals 1
    .param p1    # Ljava/lang/String;

    invoke-virtual {p0}, Lorg/apache/tools/ant/taskdefs/Java;->getCommandLine()Lorg/apache/tools/ant/types/CommandlineJava;

    move-result-object v0

    invoke-virtual {v0, p1}, Lorg/apache/tools/ant/types/CommandlineJava;->setVm(Ljava/lang/String;)V

    return-void
.end method

.method public setJvmargs(Ljava/lang/String;)V
    .locals 2
    .param p1    # Ljava/lang/String;

    const-string v0, "The jvmargs attribute is deprecated. Please use nested jvmarg elements."

    const/4 v1, 0x1

    invoke-virtual {p0, v0, v1}, Lorg/apache/tools/ant/taskdefs/Java;->log(Ljava/lang/String;I)V

    invoke-virtual {p0}, Lorg/apache/tools/ant/taskdefs/Java;->getCommandLine()Lorg/apache/tools/ant/types/CommandlineJava;

    move-result-object v0

    invoke-virtual {v0}, Lorg/apache/tools/ant/types/CommandlineJava;->createVmArgument()Lorg/apache/tools/ant/types/Commandline$Argument;

    move-result-object v0

    invoke-virtual {v0, p1}, Lorg/apache/tools/ant/types/Commandline$Argument;->setLine(Ljava/lang/String;)V

    return-void
.end method

.method public setLogError(Z)V
    .locals 1
    .param p1    # Z

    iget-object v0, p0, Lorg/apache/tools/ant/taskdefs/Java;->redirector:Lorg/apache/tools/ant/taskdefs/Redirector;

    invoke-virtual {v0, p1}, Lorg/apache/tools/ant/taskdefs/Redirector;->setLogError(Z)V

    iget-boolean v0, p0, Lorg/apache/tools/ant/taskdefs/Java;->incompatibleWithSpawn:Z

    or-int/2addr v0, p1

    iput-boolean v0, p0, Lorg/apache/tools/ant/taskdefs/Java;->incompatibleWithSpawn:Z

    return-void
.end method

.method public setMaxmemory(Ljava/lang/String;)V
    .locals 1
    .param p1    # Ljava/lang/String;

    invoke-virtual {p0}, Lorg/apache/tools/ant/taskdefs/Java;->getCommandLine()Lorg/apache/tools/ant/types/CommandlineJava;

    move-result-object v0

    invoke-virtual {v0, p1}, Lorg/apache/tools/ant/types/CommandlineJava;->setMaxmemory(Ljava/lang/String;)V

    return-void
.end method

.method public setNewenvironment(Z)V
    .locals 0
    .param p1    # Z

    iput-boolean p1, p0, Lorg/apache/tools/ant/taskdefs/Java;->newEnvironment:Z

    return-void
.end method

.method public setOutput(Ljava/io/File;)V
    .locals 1
    .param p1    # Ljava/io/File;

    iput-object p1, p0, Lorg/apache/tools/ant/taskdefs/Java;->output:Ljava/io/File;

    const/4 v0, 0x1

    iput-boolean v0, p0, Lorg/apache/tools/ant/taskdefs/Java;->incompatibleWithSpawn:Z

    return-void
.end method

.method public setOutputproperty(Ljava/lang/String;)V
    .locals 1
    .param p1    # Ljava/lang/String;

    iget-object v0, p0, Lorg/apache/tools/ant/taskdefs/Java;->redirector:Lorg/apache/tools/ant/taskdefs/Redirector;

    invoke-virtual {v0, p1}, Lorg/apache/tools/ant/taskdefs/Redirector;->setOutputProperty(Ljava/lang/String;)V

    const/4 v0, 0x1

    iput-boolean v0, p0, Lorg/apache/tools/ant/taskdefs/Java;->incompatibleWithSpawn:Z

    return-void
.end method

.method public setResultProperty(Ljava/lang/String;)V
    .locals 1
    .param p1    # Ljava/lang/String;

    iput-object p1, p0, Lorg/apache/tools/ant/taskdefs/Java;->resultProperty:Ljava/lang/String;

    const/4 v0, 0x1

    iput-boolean v0, p0, Lorg/apache/tools/ant/taskdefs/Java;->incompatibleWithSpawn:Z

    return-void
.end method

.method public setSpawn(Z)V
    .locals 0
    .param p1    # Z

    iput-boolean p1, p0, Lorg/apache/tools/ant/taskdefs/Java;->spawn:Z

    return-void
.end method

.method public setTimeout(Ljava/lang/Long;)V
    .locals 2
    .param p1    # Ljava/lang/Long;

    iput-object p1, p0, Lorg/apache/tools/ant/taskdefs/Java;->timeout:Ljava/lang/Long;

    iget-boolean v1, p0, Lorg/apache/tools/ant/taskdefs/Java;->incompatibleWithSpawn:Z

    iget-object v0, p0, Lorg/apache/tools/ant/taskdefs/Java;->timeout:Ljava/lang/Long;

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    or-int/2addr v0, v1

    iput-boolean v0, p0, Lorg/apache/tools/ant/taskdefs/Java;->incompatibleWithSpawn:Z

    return-void

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method protected setupRedirector()V
    .locals 3

    iget-object v0, p0, Lorg/apache/tools/ant/taskdefs/Java;->redirector:Lorg/apache/tools/ant/taskdefs/Redirector;

    iget-object v1, p0, Lorg/apache/tools/ant/taskdefs/Java;->input:Ljava/io/File;

    invoke-virtual {v0, v1}, Lorg/apache/tools/ant/taskdefs/Redirector;->setInput(Ljava/io/File;)V

    iget-object v0, p0, Lorg/apache/tools/ant/taskdefs/Java;->redirector:Lorg/apache/tools/ant/taskdefs/Redirector;

    iget-object v1, p0, Lorg/apache/tools/ant/taskdefs/Java;->inputString:Ljava/lang/String;

    invoke-virtual {v0, v1}, Lorg/apache/tools/ant/taskdefs/Redirector;->setInputString(Ljava/lang/String;)V

    iget-object v0, p0, Lorg/apache/tools/ant/taskdefs/Java;->redirector:Lorg/apache/tools/ant/taskdefs/Redirector;

    iget-object v1, p0, Lorg/apache/tools/ant/taskdefs/Java;->output:Ljava/io/File;

    invoke-virtual {v0, v1}, Lorg/apache/tools/ant/taskdefs/Redirector;->setOutput(Ljava/io/File;)V

    iget-object v0, p0, Lorg/apache/tools/ant/taskdefs/Java;->redirector:Lorg/apache/tools/ant/taskdefs/Redirector;

    iget-object v1, p0, Lorg/apache/tools/ant/taskdefs/Java;->error:Ljava/io/File;

    invoke-virtual {v0, v1}, Lorg/apache/tools/ant/taskdefs/Redirector;->setError(Ljava/io/File;)V

    iget-object v0, p0, Lorg/apache/tools/ant/taskdefs/Java;->redirectorElement:Lorg/apache/tools/ant/types/RedirectorElement;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lorg/apache/tools/ant/taskdefs/Java;->redirectorElement:Lorg/apache/tools/ant/types/RedirectorElement;

    iget-object v1, p0, Lorg/apache/tools/ant/taskdefs/Java;->redirector:Lorg/apache/tools/ant/taskdefs/Redirector;

    invoke-virtual {v0, v1}, Lorg/apache/tools/ant/types/RedirectorElement;->configure(Lorg/apache/tools/ant/taskdefs/Redirector;)V

    :cond_0
    iget-boolean v0, p0, Lorg/apache/tools/ant/taskdefs/Java;->spawn:Z

    if-nez v0, :cond_1

    iget-object v0, p0, Lorg/apache/tools/ant/taskdefs/Java;->input:Ljava/io/File;

    if-nez v0, :cond_1

    iget-object v0, p0, Lorg/apache/tools/ant/taskdefs/Java;->inputString:Ljava/lang/String;

    if-nez v0, :cond_1

    iget-object v0, p0, Lorg/apache/tools/ant/taskdefs/Java;->redirector:Lorg/apache/tools/ant/taskdefs/Redirector;

    new-instance v1, Lorg/apache/tools/ant/util/KeepAliveInputStream;

    invoke-virtual {p0}, Lorg/apache/tools/ant/taskdefs/Java;->getProject()Lorg/apache/tools/ant/Project;

    move-result-object v2

    invoke-virtual {v2}, Lorg/apache/tools/ant/Project;->getDefaultInputStream()Ljava/io/InputStream;

    move-result-object v2

    invoke-direct {v1, v2}, Lorg/apache/tools/ant/util/KeepAliveInputStream;-><init>(Ljava/io/InputStream;)V

    invoke-virtual {v0, v1}, Lorg/apache/tools/ant/taskdefs/Redirector;->setInputStream(Ljava/io/InputStream;)V

    :cond_1
    return-void
.end method
