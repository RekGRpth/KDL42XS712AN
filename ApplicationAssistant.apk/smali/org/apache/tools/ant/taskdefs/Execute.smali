.class public Lorg/apache/tools/ant/taskdefs/Execute;
.super Ljava/lang/Object;
.source "Execute.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lorg/apache/tools/ant/taskdefs/Execute$VmsCommandLauncher;,
        Lorg/apache/tools/ant/taskdefs/Execute$PerlScriptCommandLauncher;,
        Lorg/apache/tools/ant/taskdefs/Execute$ScriptCommandLauncher;,
        Lorg/apache/tools/ant/taskdefs/Execute$MacCommandLauncher;,
        Lorg/apache/tools/ant/taskdefs/Execute$WinNTCommandLauncher;,
        Lorg/apache/tools/ant/taskdefs/Execute$OS2CommandLauncher;,
        Lorg/apache/tools/ant/taskdefs/Execute$CommandLauncherProxy;,
        Lorg/apache/tools/ant/taskdefs/Execute$Java13CommandLauncher;,
        Lorg/apache/tools/ant/taskdefs/Execute$CommandLauncher;
    }
.end annotation


# static fields
.field private static final FILE_UTILS:Lorg/apache/tools/ant/util/FileUtils;

.field public static final INVALID:I = 0x7fffffff

.field private static antWorkingDirectory:Ljava/lang/String;

.field static array$Ljava$lang$String:Ljava/lang/Class;

.field static class$java$io$File:Ljava/lang/Class;

.field static class$java$lang$Runtime:Ljava/lang/Class;

.field private static environmentCaseInSensitive:Z

.field private static procEnvironment:Ljava/util/Vector;

.field private static processDestroyer:Lorg/apache/tools/ant/taskdefs/ProcessDestroyer;

.field private static shellLauncher:Lorg/apache/tools/ant/taskdefs/Execute$CommandLauncher;

.field private static vmLauncher:Lorg/apache/tools/ant/taskdefs/Execute$CommandLauncher;


# instance fields
.field private cmdl:[Ljava/lang/String;

.field private env:[Ljava/lang/String;

.field private exitValue:I

.field private newEnvironment:Z

.field private project:Lorg/apache/tools/ant/Project;

.field private spawn:Z

.field private streamHandler:Lorg/apache/tools/ant/taskdefs/ExecuteStreamHandler;

.field private useVMLauncher:Z

.field private watchdog:Lorg/apache/tools/ant/taskdefs/ExecuteWatchdog;

.field private workingDirectory:Ljava/io/File;


# direct methods
.method static constructor <clinit>()V
    .locals 5

    const/4 v4, 0x0

    invoke-static {}, Lorg/apache/tools/ant/util/FileUtils;->getFileUtils()Lorg/apache/tools/ant/util/FileUtils;

    move-result-object v1

    sput-object v1, Lorg/apache/tools/ant/taskdefs/Execute;->FILE_UTILS:Lorg/apache/tools/ant/util/FileUtils;

    const-string v1, "user.dir"

    invoke-static {v1}, Ljava/lang/System;->getProperty(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    sput-object v1, Lorg/apache/tools/ant/taskdefs/Execute;->antWorkingDirectory:Ljava/lang/String;

    sput-object v4, Lorg/apache/tools/ant/taskdefs/Execute;->vmLauncher:Lorg/apache/tools/ant/taskdefs/Execute$CommandLauncher;

    sput-object v4, Lorg/apache/tools/ant/taskdefs/Execute;->shellLauncher:Lorg/apache/tools/ant/taskdefs/Execute$CommandLauncher;

    sput-object v4, Lorg/apache/tools/ant/taskdefs/Execute;->procEnvironment:Ljava/util/Vector;

    new-instance v1, Lorg/apache/tools/ant/taskdefs/ProcessDestroyer;

    invoke-direct {v1}, Lorg/apache/tools/ant/taskdefs/ProcessDestroyer;-><init>()V

    sput-object v1, Lorg/apache/tools/ant/taskdefs/Execute;->processDestroyer:Lorg/apache/tools/ant/taskdefs/ProcessDestroyer;

    const/4 v1, 0x0

    sput-boolean v1, Lorg/apache/tools/ant/taskdefs/Execute;->environmentCaseInSensitive:Z

    :try_start_0
    const-string v1, "os/2"

    invoke-static {v1}, Lorg/apache/tools/ant/taskdefs/condition/Os;->isFamily(Ljava/lang/String;)Z

    move-result v1

    if-nez v1, :cond_0

    new-instance v1, Lorg/apache/tools/ant/taskdefs/Execute$Java13CommandLauncher;

    invoke-direct {v1}, Lorg/apache/tools/ant/taskdefs/Execute$Java13CommandLauncher;-><init>()V

    sput-object v1, Lorg/apache/tools/ant/taskdefs/Execute;->vmLauncher:Lorg/apache/tools/ant/taskdefs/Execute$CommandLauncher;
    :try_end_0
    .catch Ljava/lang/NoSuchMethodException; {:try_start_0 .. :try_end_0} :catch_1

    :cond_0
    :goto_0
    const-string v1, "mac"

    invoke-static {v1}, Lorg/apache/tools/ant/taskdefs/condition/Os;->isFamily(Ljava/lang/String;)Z

    move-result v1

    if-eqz v1, :cond_1

    const-string v1, "unix"

    invoke-static {v1}, Lorg/apache/tools/ant/taskdefs/condition/Os;->isFamily(Ljava/lang/String;)Z

    move-result v1

    if-nez v1, :cond_1

    new-instance v1, Lorg/apache/tools/ant/taskdefs/Execute$MacCommandLauncher;

    new-instance v2, Lorg/apache/tools/ant/taskdefs/Execute$CommandLauncher;

    invoke-direct {v2, v4}, Lorg/apache/tools/ant/taskdefs/Execute$CommandLauncher;-><init>(Lorg/apache/tools/ant/taskdefs/Execute$1;)V

    invoke-direct {v1, v2}, Lorg/apache/tools/ant/taskdefs/Execute$MacCommandLauncher;-><init>(Lorg/apache/tools/ant/taskdefs/Execute$CommandLauncher;)V

    sput-object v1, Lorg/apache/tools/ant/taskdefs/Execute;->shellLauncher:Lorg/apache/tools/ant/taskdefs/Execute$CommandLauncher;

    :goto_1
    return-void

    :cond_1
    const-string v1, "os/2"

    invoke-static {v1}, Lorg/apache/tools/ant/taskdefs/condition/Os;->isFamily(Ljava/lang/String;)Z

    move-result v1

    if-eqz v1, :cond_2

    new-instance v1, Lorg/apache/tools/ant/taskdefs/Execute$OS2CommandLauncher;

    new-instance v2, Lorg/apache/tools/ant/taskdefs/Execute$CommandLauncher;

    invoke-direct {v2, v4}, Lorg/apache/tools/ant/taskdefs/Execute$CommandLauncher;-><init>(Lorg/apache/tools/ant/taskdefs/Execute$1;)V

    invoke-direct {v1, v2}, Lorg/apache/tools/ant/taskdefs/Execute$OS2CommandLauncher;-><init>(Lorg/apache/tools/ant/taskdefs/Execute$CommandLauncher;)V

    sput-object v1, Lorg/apache/tools/ant/taskdefs/Execute;->shellLauncher:Lorg/apache/tools/ant/taskdefs/Execute$CommandLauncher;

    goto :goto_1

    :cond_2
    const-string v1, "windows"

    invoke-static {v1}, Lorg/apache/tools/ant/taskdefs/condition/Os;->isFamily(Ljava/lang/String;)Z

    move-result v1

    if-eqz v1, :cond_4

    const/4 v1, 0x1

    sput-boolean v1, Lorg/apache/tools/ant/taskdefs/Execute;->environmentCaseInSensitive:Z

    new-instance v0, Lorg/apache/tools/ant/taskdefs/Execute$CommandLauncher;

    invoke-direct {v0, v4}, Lorg/apache/tools/ant/taskdefs/Execute$CommandLauncher;-><init>(Lorg/apache/tools/ant/taskdefs/Execute$1;)V

    const-string v1, "win9x"

    invoke-static {v1}, Lorg/apache/tools/ant/taskdefs/condition/Os;->isFamily(Ljava/lang/String;)Z

    move-result v1

    if-nez v1, :cond_3

    new-instance v1, Lorg/apache/tools/ant/taskdefs/Execute$WinNTCommandLauncher;

    invoke-direct {v1, v0}, Lorg/apache/tools/ant/taskdefs/Execute$WinNTCommandLauncher;-><init>(Lorg/apache/tools/ant/taskdefs/Execute$CommandLauncher;)V

    sput-object v1, Lorg/apache/tools/ant/taskdefs/Execute;->shellLauncher:Lorg/apache/tools/ant/taskdefs/Execute$CommandLauncher;

    goto :goto_1

    :cond_3
    new-instance v1, Lorg/apache/tools/ant/taskdefs/Execute$ScriptCommandLauncher;

    const-string v2, "bin/antRun.bat"

    invoke-direct {v1, v2, v0}, Lorg/apache/tools/ant/taskdefs/Execute$ScriptCommandLauncher;-><init>(Ljava/lang/String;Lorg/apache/tools/ant/taskdefs/Execute$CommandLauncher;)V

    sput-object v1, Lorg/apache/tools/ant/taskdefs/Execute;->shellLauncher:Lorg/apache/tools/ant/taskdefs/Execute$CommandLauncher;

    goto :goto_1

    :cond_4
    const-string v1, "netware"

    invoke-static {v1}, Lorg/apache/tools/ant/taskdefs/condition/Os;->isFamily(Ljava/lang/String;)Z

    move-result v1

    if-eqz v1, :cond_5

    new-instance v0, Lorg/apache/tools/ant/taskdefs/Execute$CommandLauncher;

    invoke-direct {v0, v4}, Lorg/apache/tools/ant/taskdefs/Execute$CommandLauncher;-><init>(Lorg/apache/tools/ant/taskdefs/Execute$1;)V

    new-instance v1, Lorg/apache/tools/ant/taskdefs/Execute$PerlScriptCommandLauncher;

    const-string v2, "bin/antRun.pl"

    invoke-direct {v1, v2, v0}, Lorg/apache/tools/ant/taskdefs/Execute$PerlScriptCommandLauncher;-><init>(Ljava/lang/String;Lorg/apache/tools/ant/taskdefs/Execute$CommandLauncher;)V

    sput-object v1, Lorg/apache/tools/ant/taskdefs/Execute;->shellLauncher:Lorg/apache/tools/ant/taskdefs/Execute$CommandLauncher;

    goto :goto_1

    :cond_5
    const-string v1, "openvms"

    invoke-static {v1}, Lorg/apache/tools/ant/taskdefs/condition/Os;->isFamily(Ljava/lang/String;)Z

    move-result v1

    if-eqz v1, :cond_6

    :try_start_1
    new-instance v1, Lorg/apache/tools/ant/taskdefs/Execute$VmsCommandLauncher;

    invoke-direct {v1}, Lorg/apache/tools/ant/taskdefs/Execute$VmsCommandLauncher;-><init>()V

    sput-object v1, Lorg/apache/tools/ant/taskdefs/Execute;->shellLauncher:Lorg/apache/tools/ant/taskdefs/Execute$CommandLauncher;
    :try_end_1
    .catch Ljava/lang/NoSuchMethodException; {:try_start_1 .. :try_end_1} :catch_0

    goto :goto_1

    :catch_0
    move-exception v1

    goto :goto_1

    :cond_6
    new-instance v1, Lorg/apache/tools/ant/taskdefs/Execute$ScriptCommandLauncher;

    const-string v2, "bin/antRun"

    new-instance v3, Lorg/apache/tools/ant/taskdefs/Execute$CommandLauncher;

    invoke-direct {v3, v4}, Lorg/apache/tools/ant/taskdefs/Execute$CommandLauncher;-><init>(Lorg/apache/tools/ant/taskdefs/Execute$1;)V

    invoke-direct {v1, v2, v3}, Lorg/apache/tools/ant/taskdefs/Execute$ScriptCommandLauncher;-><init>(Ljava/lang/String;Lorg/apache/tools/ant/taskdefs/Execute$CommandLauncher;)V

    sput-object v1, Lorg/apache/tools/ant/taskdefs/Execute;->shellLauncher:Lorg/apache/tools/ant/taskdefs/Execute$CommandLauncher;

    goto :goto_1

    :catch_1
    move-exception v1

    goto/16 :goto_0
.end method

.method public constructor <init>()V
    .locals 2

    new-instance v0, Lorg/apache/tools/ant/taskdefs/PumpStreamHandler;

    invoke-direct {v0}, Lorg/apache/tools/ant/taskdefs/PumpStreamHandler;-><init>()V

    const/4 v1, 0x0

    invoke-direct {p0, v0, v1}, Lorg/apache/tools/ant/taskdefs/Execute;-><init>(Lorg/apache/tools/ant/taskdefs/ExecuteStreamHandler;Lorg/apache/tools/ant/taskdefs/ExecuteWatchdog;)V

    return-void
.end method

.method public constructor <init>(Lorg/apache/tools/ant/taskdefs/ExecuteStreamHandler;)V
    .locals 1
    .param p1    # Lorg/apache/tools/ant/taskdefs/ExecuteStreamHandler;

    const/4 v0, 0x0

    invoke-direct {p0, p1, v0}, Lorg/apache/tools/ant/taskdefs/Execute;-><init>(Lorg/apache/tools/ant/taskdefs/ExecuteStreamHandler;Lorg/apache/tools/ant/taskdefs/ExecuteWatchdog;)V

    return-void
.end method

.method public constructor <init>(Lorg/apache/tools/ant/taskdefs/ExecuteStreamHandler;Lorg/apache/tools/ant/taskdefs/ExecuteWatchdog;)V
    .locals 3
    .param p1    # Lorg/apache/tools/ant/taskdefs/ExecuteStreamHandler;
    .param p2    # Lorg/apache/tools/ant/taskdefs/ExecuteWatchdog;

    const/4 v2, 0x0

    const/4 v1, 0x0

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    iput-object v1, p0, Lorg/apache/tools/ant/taskdefs/Execute;->cmdl:[Ljava/lang/String;

    iput-object v1, p0, Lorg/apache/tools/ant/taskdefs/Execute;->env:[Ljava/lang/String;

    const v0, 0x7fffffff

    iput v0, p0, Lorg/apache/tools/ant/taskdefs/Execute;->exitValue:I

    iput-object v1, p0, Lorg/apache/tools/ant/taskdefs/Execute;->workingDirectory:Ljava/io/File;

    iput-object v1, p0, Lorg/apache/tools/ant/taskdefs/Execute;->project:Lorg/apache/tools/ant/Project;

    iput-boolean v2, p0, Lorg/apache/tools/ant/taskdefs/Execute;->newEnvironment:Z

    iput-boolean v2, p0, Lorg/apache/tools/ant/taskdefs/Execute;->spawn:Z

    const/4 v0, 0x1

    iput-boolean v0, p0, Lorg/apache/tools/ant/taskdefs/Execute;->useVMLauncher:Z

    invoke-virtual {p0, p1}, Lorg/apache/tools/ant/taskdefs/Execute;->setStreamHandler(Lorg/apache/tools/ant/taskdefs/ExecuteStreamHandler;)V

    iput-object p2, p0, Lorg/apache/tools/ant/taskdefs/Execute;->watchdog:Lorg/apache/tools/ant/taskdefs/ExecuteWatchdog;

    const-string v0, "openvms"

    invoke-static {v0}, Lorg/apache/tools/ant/taskdefs/condition/Os;->isFamily(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_0

    iput-boolean v2, p0, Lorg/apache/tools/ant/taskdefs/Execute;->useVMLauncher:Z

    :cond_0
    return-void
.end method

.method static access$100()Ljava/lang/String;
    .locals 1

    sget-object v0, Lorg/apache/tools/ant/taskdefs/Execute;->antWorkingDirectory:Ljava/lang/String;

    return-object v0
.end method

.method static access$200()Lorg/apache/tools/ant/util/FileUtils;
    .locals 1

    sget-object v0, Lorg/apache/tools/ant/taskdefs/Execute;->FILE_UTILS:Lorg/apache/tools/ant/util/FileUtils;

    return-object v0
.end method

.method private static addVMSLogicals(Ljava/util/Vector;Ljava/io/BufferedReader;)Ljava/util/Vector;
    .locals 11
    .param p0    # Ljava/util/Vector;
    .param p1    # Ljava/io/BufferedReader;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    new-instance v6, Ljava/util/HashMap;

    invoke-direct {v6}, Ljava/util/HashMap;-><init>()V

    const/4 v3, 0x0

    const/4 v4, 0x0

    const/4 v2, 0x0

    :cond_0
    :goto_0
    invoke-virtual {p1}, Ljava/io/BufferedReader;->readLine()Ljava/lang/String;

    move-result-object v2

    if-eqz v2, :cond_4

    const-string v8, "\t="

    invoke-virtual {v2, v8}, Ljava/lang/String;->startsWith(Ljava/lang/String;)Z

    move-result v8

    if-eqz v8, :cond_1

    if-eqz v3, :cond_0

    new-instance v8, Ljava/lang/StringBuffer;

    invoke-direct {v8}, Ljava/lang/StringBuffer;-><init>()V

    invoke-virtual {v8, v4}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    move-result-object v8

    const-string v9, ","

    invoke-virtual {v8, v9}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    move-result-object v8

    const/4 v9, 0x4

    invoke-virtual {v2}, Ljava/lang/String;->length()I

    move-result v10

    add-int/lit8 v10, v10, -0x1

    invoke-virtual {v2, v9, v10}, Ljava/lang/String;->substring(II)Ljava/lang/String;

    move-result-object v9

    invoke-virtual {v8, v9}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    move-result-object v8

    invoke-virtual {v8}, Ljava/lang/StringBuffer;->toString()Ljava/lang/String;

    move-result-object v4

    goto :goto_0

    :cond_1
    const-string v8, "  \""

    invoke-virtual {v2, v8}, Ljava/lang/String;->startsWith(Ljava/lang/String;)Z

    move-result v8

    if-eqz v8, :cond_0

    if-eqz v3, :cond_2

    invoke-virtual {v6, v3, v4}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    :cond_2
    const/16 v8, 0x3d

    invoke-virtual {v2, v8}, Ljava/lang/String;->indexOf(I)I

    move-result v0

    const/4 v8, 0x3

    add-int/lit8 v9, v0, -0x2

    invoke-virtual {v2, v8, v9}, Ljava/lang/String;->substring(II)Ljava/lang/String;

    move-result-object v7

    invoke-virtual {v6, v7}, Ljava/util/HashMap;->containsKey(Ljava/lang/Object;)Z

    move-result v8

    if-eqz v8, :cond_3

    const/4 v3, 0x0

    goto :goto_0

    :cond_3
    move-object v3, v7

    add-int/lit8 v8, v0, 0x3

    invoke-virtual {v2}, Ljava/lang/String;->length()I

    move-result v9

    add-int/lit8 v9, v9, -0x1

    invoke-virtual {v2, v8, v9}, Ljava/lang/String;->substring(II)Ljava/lang/String;

    move-result-object v4

    goto :goto_0

    :cond_4
    if-eqz v3, :cond_5

    invoke-virtual {v6, v3, v4}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    :cond_5
    invoke-virtual {v6}, Ljava/util/HashMap;->keySet()Ljava/util/Set;

    move-result-object v8

    invoke-interface {v8}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :goto_1
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v8

    if-eqz v8, :cond_6

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v5

    check-cast v5, Ljava/lang/String;

    new-instance v8, Ljava/lang/StringBuffer;

    invoke-direct {v8}, Ljava/lang/StringBuffer;-><init>()V

    invoke-virtual {v8, v5}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    move-result-object v8

    const-string v9, "="

    invoke-virtual {v8, v9}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    move-result-object v8

    invoke-virtual {v6, v5}, Ljava/util/HashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v9

    invoke-virtual {v8, v9}, Ljava/lang/StringBuffer;->append(Ljava/lang/Object;)Ljava/lang/StringBuffer;

    move-result-object v8

    invoke-virtual {v8}, Ljava/lang/StringBuffer;->toString()Ljava/lang/String;

    move-result-object v8

    invoke-virtual {p0, v8}, Ljava/util/Vector;->add(Ljava/lang/Object;)Z

    goto :goto_1

    :cond_6
    return-object p0
.end method

.method static class$(Ljava/lang/String;)Ljava/lang/Class;
    .locals 3
    .param p0    # Ljava/lang/String;

    :try_start_0
    invoke-static {p0}, Ljava/lang/Class;->forName(Ljava/lang/String;)Ljava/lang/Class;
    :try_end_0
    .catch Ljava/lang/ClassNotFoundException; {:try_start_0 .. :try_end_0} :catch_0

    move-result-object v1

    return-object v1

    :catch_0
    move-exception v0

    new-instance v1, Ljava/lang/NoClassDefFoundError;

    invoke-virtual {v0}, Ljava/lang/ClassNotFoundException;->getMessage()Ljava/lang/String;

    move-result-object v2

    invoke-direct {v1, v2}, Ljava/lang/NoClassDefFoundError;-><init>(Ljava/lang/String;)V

    throw v1
.end method

.method public static closeStreams(Ljava/lang/Process;)V
    .locals 1
    .param p0    # Ljava/lang/Process;

    invoke-virtual {p0}, Ljava/lang/Process;->getInputStream()Ljava/io/InputStream;

    move-result-object v0

    invoke-static {v0}, Lorg/apache/tools/ant/util/FileUtils;->close(Ljava/io/InputStream;)V

    invoke-virtual {p0}, Ljava/lang/Process;->getOutputStream()Ljava/io/OutputStream;

    move-result-object v0

    invoke-static {v0}, Lorg/apache/tools/ant/util/FileUtils;->close(Ljava/io/OutputStream;)V

    invoke-virtual {p0}, Ljava/lang/Process;->getErrorStream()Ljava/io/InputStream;

    move-result-object v0

    invoke-static {v0}, Lorg/apache/tools/ant/util/FileUtils;->close(Ljava/io/InputStream;)V

    return-void
.end method

.method private static getProcEnvCommand()[Ljava/lang/String;
    .locals 6

    const/4 v5, 0x3

    const/4 v4, 0x2

    const/4 v2, 0x1

    const/4 v3, 0x0

    const-string v1, "os/2"

    invoke-static {v1}, Lorg/apache/tools/ant/taskdefs/condition/Os;->isFamily(Ljava/lang/String;)Z

    move-result v1

    if-eqz v1, :cond_0

    new-array v0, v5, [Ljava/lang/String;

    const-string v1, "cmd"

    aput-object v1, v0, v3

    const-string v1, "/c"

    aput-object v1, v0, v2

    const-string v1, "set"

    aput-object v1, v0, v4

    :goto_0
    return-object v0

    :cond_0
    const-string v1, "windows"

    invoke-static {v1}, Lorg/apache/tools/ant/taskdefs/condition/Os;->isFamily(Ljava/lang/String;)Z

    move-result v1

    if-eqz v1, :cond_2

    const-string v1, "win9x"

    invoke-static {v1}, Lorg/apache/tools/ant/taskdefs/condition/Os;->isFamily(Ljava/lang/String;)Z

    move-result v1

    if-eqz v1, :cond_1

    new-array v0, v5, [Ljava/lang/String;

    const-string v1, "command.com"

    aput-object v1, v0, v3

    const-string v1, "/c"

    aput-object v1, v0, v2

    const-string v1, "set"

    aput-object v1, v0, v4

    goto :goto_0

    :cond_1
    new-array v0, v5, [Ljava/lang/String;

    const-string v1, "cmd"

    aput-object v1, v0, v3

    const-string v1, "/c"

    aput-object v1, v0, v2

    const-string v1, "set"

    aput-object v1, v0, v4

    goto :goto_0

    :cond_2
    const-string v1, "z/os"

    invoke-static {v1}, Lorg/apache/tools/ant/taskdefs/condition/Os;->isFamily(Ljava/lang/String;)Z

    move-result v1

    if-nez v1, :cond_3

    const-string v1, "unix"

    invoke-static {v1}, Lorg/apache/tools/ant/taskdefs/condition/Os;->isFamily(Ljava/lang/String;)Z

    move-result v1

    if-eqz v1, :cond_6

    :cond_3
    new-array v0, v2, [Ljava/lang/String;

    new-instance v1, Ljava/io/File;

    const-string v2, "/bin/env"

    invoke-direct {v1, v2}, Ljava/io/File;-><init>(Ljava/lang/String;)V

    invoke-virtual {v1}, Ljava/io/File;->canRead()Z

    move-result v1

    if-eqz v1, :cond_4

    const-string v1, "/bin/env"

    aput-object v1, v0, v3

    goto :goto_0

    :cond_4
    new-instance v1, Ljava/io/File;

    const-string v2, "/usr/bin/env"

    invoke-direct {v1, v2}, Ljava/io/File;-><init>(Ljava/lang/String;)V

    invoke-virtual {v1}, Ljava/io/File;->canRead()Z

    move-result v1

    if-eqz v1, :cond_5

    const-string v1, "/usr/bin/env"

    aput-object v1, v0, v3

    goto :goto_0

    :cond_5
    const-string v1, "env"

    aput-object v1, v0, v3

    goto :goto_0

    :cond_6
    const-string v1, "netware"

    invoke-static {v1}, Lorg/apache/tools/ant/taskdefs/condition/Os;->isFamily(Ljava/lang/String;)Z

    move-result v1

    if-nez v1, :cond_7

    const-string v1, "os/400"

    invoke-static {v1}, Lorg/apache/tools/ant/taskdefs/condition/Os;->isFamily(Ljava/lang/String;)Z

    move-result v1

    if-eqz v1, :cond_8

    :cond_7
    new-array v0, v2, [Ljava/lang/String;

    const-string v1, "env"

    aput-object v1, v0, v3

    goto :goto_0

    :cond_8
    const-string v1, "openvms"

    invoke-static {v1}, Lorg/apache/tools/ant/taskdefs/condition/Os;->isFamily(Ljava/lang/String;)Z

    move-result v1

    if-eqz v1, :cond_9

    new-array v0, v4, [Ljava/lang/String;

    const-string v1, "show"

    aput-object v1, v0, v3

    const-string v1, "logical"

    aput-object v1, v0, v2

    goto/16 :goto_0

    :cond_9
    const/4 v0, 0x0

    goto/16 :goto_0
.end method

.method public static declared-synchronized getProcEnvironment()Ljava/util/Vector;
    .locals 11

    const-class v9, Lorg/apache/tools/ant/taskdefs/Execute;

    monitor-enter v9

    :try_start_0
    sget-object v8, Lorg/apache/tools/ant/taskdefs/Execute;->procEnvironment:Ljava/util/Vector;

    if-eqz v8, :cond_0

    sget-object v8, Lorg/apache/tools/ant/taskdefs/Execute;->procEnvironment:Ljava/util/Vector;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    :goto_0
    monitor-exit v9

    return-object v8

    :cond_0
    :try_start_1
    new-instance v8, Ljava/util/Vector;

    invoke-direct {v8}, Ljava/util/Vector;-><init>()V

    sput-object v8, Lorg/apache/tools/ant/taskdefs/Execute;->procEnvironment:Ljava/util/Vector;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    :try_start_2
    new-instance v5, Ljava/io/ByteArrayOutputStream;

    invoke-direct {v5}, Ljava/io/ByteArrayOutputStream;-><init>()V

    new-instance v1, Lorg/apache/tools/ant/taskdefs/Execute;

    new-instance v8, Lorg/apache/tools/ant/taskdefs/PumpStreamHandler;

    invoke-direct {v8, v5}, Lorg/apache/tools/ant/taskdefs/PumpStreamHandler;-><init>(Ljava/io/OutputStream;)V

    invoke-direct {v1, v8}, Lorg/apache/tools/ant/taskdefs/Execute;-><init>(Lorg/apache/tools/ant/taskdefs/ExecuteStreamHandler;)V

    invoke-static {}, Lorg/apache/tools/ant/taskdefs/Execute;->getProcEnvCommand()[Ljava/lang/String;

    move-result-object v8

    invoke-virtual {v1, v8}, Lorg/apache/tools/ant/taskdefs/Execute;->setCommandline([Ljava/lang/String;)V

    const/4 v8, 0x1

    invoke-virtual {v1, v8}, Lorg/apache/tools/ant/taskdefs/Execute;->setNewenvironment(Z)V

    invoke-virtual {v1}, Lorg/apache/tools/ant/taskdefs/Execute;->execute()I

    move-result v6

    if-eqz v6, :cond_1

    :cond_1
    new-instance v2, Ljava/io/BufferedReader;

    new-instance v8, Ljava/io/StringReader;

    invoke-static {v5}, Lorg/apache/tools/ant/taskdefs/Execute;->toString(Ljava/io/ByteArrayOutputStream;)Ljava/lang/String;

    move-result-object v10

    invoke-direct {v8, v10}, Ljava/io/StringReader;-><init>(Ljava/lang/String;)V

    invoke-direct {v2, v8}, Ljava/io/BufferedReader;-><init>(Ljava/io/Reader;)V

    const-string v8, "openvms"

    invoke-static {v8}, Lorg/apache/tools/ant/taskdefs/condition/Os;->isFamily(Ljava/lang/String;)Z

    move-result v8

    if-eqz v8, :cond_2

    sget-object v8, Lorg/apache/tools/ant/taskdefs/Execute;->procEnvironment:Ljava/util/Vector;

    invoke-static {v8, v2}, Lorg/apache/tools/ant/taskdefs/Execute;->addVMSLogicals(Ljava/util/Vector;Ljava/io/BufferedReader;)Ljava/util/Vector;

    move-result-object v8

    sput-object v8, Lorg/apache/tools/ant/taskdefs/Execute;->procEnvironment:Ljava/util/Vector;

    sget-object v8, Lorg/apache/tools/ant/taskdefs/Execute;->procEnvironment:Ljava/util/Vector;

    goto :goto_0

    :cond_2
    const/4 v7, 0x0

    sget-object v4, Lorg/apache/tools/ant/util/StringUtils;->LINE_SEP:Ljava/lang/String;

    :goto_1
    invoke-virtual {v2}, Ljava/io/BufferedReader;->readLine()Ljava/lang/String;

    move-result-object v3

    if-eqz v3, :cond_6

    const/16 v8, 0x3d

    invoke-virtual {v3, v8}, Ljava/lang/String;->indexOf(I)I

    move-result v8

    const/4 v10, -0x1

    if-ne v8, v10, :cond_4

    if-nez v7, :cond_3

    new-instance v8, Ljava/lang/StringBuffer;

    invoke-direct {v8}, Ljava/lang/StringBuffer;-><init>()V

    invoke-virtual {v8, v4}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    move-result-object v8

    invoke-virtual {v8, v3}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    move-result-object v8

    invoke-virtual {v8}, Ljava/lang/StringBuffer;->toString()Ljava/lang/String;

    move-result-object v7

    goto :goto_1

    :cond_3
    new-instance v8, Ljava/lang/StringBuffer;

    invoke-direct {v8}, Ljava/lang/StringBuffer;-><init>()V

    invoke-virtual {v8, v7}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    move-result-object v8

    invoke-virtual {v8, v4}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    move-result-object v8

    invoke-virtual {v8, v3}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    move-result-object v8

    invoke-virtual {v8}, Ljava/lang/StringBuffer;->toString()Ljava/lang/String;

    move-result-object v7

    goto :goto_1

    :cond_4
    if-eqz v7, :cond_5

    sget-object v8, Lorg/apache/tools/ant/taskdefs/Execute;->procEnvironment:Ljava/util/Vector;

    invoke-virtual {v8, v7}, Ljava/util/Vector;->addElement(Ljava/lang/Object;)V

    :cond_5
    move-object v7, v3

    goto :goto_1

    :cond_6
    if-eqz v7, :cond_7

    sget-object v8, Lorg/apache/tools/ant/taskdefs/Execute;->procEnvironment:Ljava/util/Vector;

    invoke-virtual {v8, v7}, Ljava/util/Vector;->addElement(Ljava/lang/Object;)V
    :try_end_2
    .catch Ljava/io/IOException; {:try_start_2 .. :try_end_2} :catch_0
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    :cond_7
    :goto_2
    :try_start_3
    sget-object v8, Lorg/apache/tools/ant/taskdefs/Execute;->procEnvironment:Ljava/util/Vector;

    goto/16 :goto_0

    :catch_0
    move-exception v0

    invoke-virtual {v0}, Ljava/io/IOException;->printStackTrace()V
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_0

    goto :goto_2

    :catchall_0
    move-exception v8

    monitor-exit v9

    throw v8
.end method

.method public static isFailure(I)Z
    .locals 3
    .param p0    # I

    const/4 v0, 0x1

    const/4 v1, 0x0

    const-string v2, "openvms"

    invoke-static {v2}, Lorg/apache/tools/ant/taskdefs/condition/Os;->isFamily(Ljava/lang/String;)Z

    move-result v2

    if-eqz v2, :cond_2

    rem-int/lit8 v2, p0, 0x2

    if-nez v2, :cond_1

    :cond_0
    :goto_0
    return v0

    :cond_1
    move v0, v1

    goto :goto_0

    :cond_2
    if-nez p0, :cond_0

    move v0, v1

    goto :goto_0
.end method

.method public static launch(Lorg/apache/tools/ant/Project;[Ljava/lang/String;[Ljava/lang/String;Ljava/io/File;Z)Ljava/lang/Process;
    .locals 4
    .param p0    # Lorg/apache/tools/ant/Project;
    .param p1    # [Ljava/lang/String;
    .param p2    # [Ljava/lang/String;
    .param p3    # Ljava/io/File;
    .param p4    # Z
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    if-eqz p3, :cond_0

    invoke-virtual {p3}, Ljava/io/File;->exists()Z

    move-result v1

    if-nez v1, :cond_0

    new-instance v1, Lorg/apache/tools/ant/BuildException;

    new-instance v2, Ljava/lang/StringBuffer;

    invoke-direct {v2}, Ljava/lang/StringBuffer;-><init>()V

    invoke-virtual {v2, p3}, Ljava/lang/StringBuffer;->append(Ljava/lang/Object;)Ljava/lang/StringBuffer;

    move-result-object v2

    const-string v3, " doesn\'t exist."

    invoke-virtual {v2, v3}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuffer;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-direct {v1, v2}, Lorg/apache/tools/ant/BuildException;-><init>(Ljava/lang/String;)V

    throw v1

    :cond_0
    if-eqz p4, :cond_1

    sget-object v1, Lorg/apache/tools/ant/taskdefs/Execute;->vmLauncher:Lorg/apache/tools/ant/taskdefs/Execute$CommandLauncher;

    if-eqz v1, :cond_1

    sget-object v0, Lorg/apache/tools/ant/taskdefs/Execute;->vmLauncher:Lorg/apache/tools/ant/taskdefs/Execute$CommandLauncher;

    :goto_0
    invoke-virtual {v0, p0, p1, p2, p3}, Lorg/apache/tools/ant/taskdefs/Execute$CommandLauncher;->exec(Lorg/apache/tools/ant/Project;[Ljava/lang/String;[Ljava/lang/String;Ljava/io/File;)Ljava/lang/Process;

    move-result-object v1

    return-object v1

    :cond_1
    sget-object v0, Lorg/apache/tools/ant/taskdefs/Execute;->shellLauncher:Lorg/apache/tools/ant/taskdefs/Execute$CommandLauncher;

    goto :goto_0
.end method

.method private patchEnvironment()[Ljava/lang/String;
    .locals 11

    const/4 v10, 0x0

    const-string v8, "openvms"

    invoke-static {v8}, Lorg/apache/tools/ant/taskdefs/condition/Os;->isFamily(Ljava/lang/String;)Z

    move-result v8

    if-eqz v8, :cond_0

    iget-object v8, p0, Lorg/apache/tools/ant/taskdefs/Execute;->env:[Ljava/lang/String;

    :goto_0
    return-object v8

    :cond_0
    invoke-static {}, Lorg/apache/tools/ant/taskdefs/Execute;->getProcEnvironment()Ljava/util/Vector;

    move-result-object v8

    invoke-virtual {v8}, Ljava/util/Vector;->clone()Ljava/lang/Object;

    move-result-object v5

    check-cast v5, Ljava/util/Vector;

    const/4 v1, 0x0

    :goto_1
    iget-object v8, p0, Lorg/apache/tools/ant/taskdefs/Execute;->env:[Ljava/lang/String;

    array-length v8, v8

    if-ge v1, v8, :cond_5

    iget-object v8, p0, Lorg/apache/tools/ant/taskdefs/Execute;->env:[Ljava/lang/String;

    aget-object v4, v8, v1

    const/16 v8, 0x3d

    invoke-virtual {v4, v8}, Ljava/lang/String;->indexOf(I)I

    move-result v8

    add-int/lit8 v8, v8, 0x1

    invoke-virtual {v4, v10, v8}, Ljava/lang/String;->substring(II)Ljava/lang/String;

    move-result-object v3

    sget-boolean v8, Lorg/apache/tools/ant/taskdefs/Execute;->environmentCaseInSensitive:Z

    if-eqz v8, :cond_1

    invoke-virtual {v3}, Ljava/lang/String;->toLowerCase()Ljava/lang/String;

    move-result-object v3

    :cond_1
    invoke-virtual {v5}, Ljava/util/Vector;->size()I

    move-result v7

    const/4 v2, 0x0

    :goto_2
    if-ge v2, v7, :cond_2

    invoke-virtual {v5, v2}, Ljava/util/Vector;->elementAt(I)Ljava/lang/Object;

    move-result-object v6

    check-cast v6, Ljava/lang/String;

    sget-boolean v8, Lorg/apache/tools/ant/taskdefs/Execute;->environmentCaseInSensitive:Z

    if-eqz v8, :cond_3

    invoke-virtual {v6}, Ljava/lang/String;->toLowerCase()Ljava/lang/String;

    move-result-object v0

    :goto_3
    invoke-virtual {v0, v3}, Ljava/lang/String;->startsWith(Ljava/lang/String;)Z

    move-result v8

    if-eqz v8, :cond_4

    invoke-virtual {v5, v2}, Ljava/util/Vector;->removeElementAt(I)V

    sget-boolean v8, Lorg/apache/tools/ant/taskdefs/Execute;->environmentCaseInSensitive:Z

    if-eqz v8, :cond_2

    new-instance v8, Ljava/lang/StringBuffer;

    invoke-direct {v8}, Ljava/lang/StringBuffer;-><init>()V

    invoke-virtual {v3}, Ljava/lang/String;->length()I

    move-result v9

    invoke-virtual {v6, v10, v9}, Ljava/lang/String;->substring(II)Ljava/lang/String;

    move-result-object v9

    invoke-virtual {v8, v9}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    move-result-object v8

    invoke-virtual {v3}, Ljava/lang/String;->length()I

    move-result v9

    invoke-virtual {v4, v9}, Ljava/lang/String;->substring(I)Ljava/lang/String;

    move-result-object v9

    invoke-virtual {v8, v9}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    move-result-object v8

    invoke-virtual {v8}, Ljava/lang/StringBuffer;->toString()Ljava/lang/String;

    move-result-object v4

    :cond_2
    invoke-virtual {v5, v4}, Ljava/util/Vector;->addElement(Ljava/lang/Object;)V

    add-int/lit8 v1, v1, 0x1

    goto :goto_1

    :cond_3
    move-object v0, v6

    goto :goto_3

    :cond_4
    add-int/lit8 v2, v2, 0x1

    goto :goto_2

    :cond_5
    invoke-virtual {v5}, Ljava/util/Vector;->size()I

    move-result v8

    new-array v8, v8, [Ljava/lang/String;

    invoke-virtual {v5, v8}, Ljava/util/Vector;->toArray([Ljava/lang/Object;)[Ljava/lang/Object;

    move-result-object v8

    check-cast v8, [Ljava/lang/String;

    check-cast v8, [Ljava/lang/String;

    goto/16 :goto_0
.end method

.method public static runCommand(Lorg/apache/tools/ant/Task;[Ljava/lang/String;)V
    .locals 7
    .param p0    # Lorg/apache/tools/ant/Task;
    .param p1    # [Ljava/lang/String;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lorg/apache/tools/ant/BuildException;
        }
    .end annotation

    const/4 v6, 0x0

    :try_start_0
    invoke-static {p1}, Lorg/apache/tools/ant/types/Commandline;->describeCommand([Ljava/lang/String;)Ljava/lang/String;

    move-result-object v3

    const/4 v4, 0x3

    invoke-virtual {p0, v3, v4}, Lorg/apache/tools/ant/Task;->log(Ljava/lang/String;I)V

    new-instance v1, Lorg/apache/tools/ant/taskdefs/Execute;

    new-instance v3, Lorg/apache/tools/ant/taskdefs/LogStreamHandler;

    const/4 v4, 0x2

    const/4 v5, 0x0

    invoke-direct {v3, p0, v4, v5}, Lorg/apache/tools/ant/taskdefs/LogStreamHandler;-><init>(Lorg/apache/tools/ant/Task;II)V

    invoke-direct {v1, v3}, Lorg/apache/tools/ant/taskdefs/Execute;-><init>(Lorg/apache/tools/ant/taskdefs/ExecuteStreamHandler;)V

    invoke-virtual {p0}, Lorg/apache/tools/ant/Task;->getProject()Lorg/apache/tools/ant/Project;

    move-result-object v3

    invoke-virtual {v1, v3}, Lorg/apache/tools/ant/taskdefs/Execute;->setAntRun(Lorg/apache/tools/ant/Project;)V

    invoke-virtual {v1, p1}, Lorg/apache/tools/ant/taskdefs/Execute;->setCommandline([Ljava/lang/String;)V

    invoke-virtual {v1}, Lorg/apache/tools/ant/taskdefs/Execute;->execute()I

    move-result v2

    invoke-static {v2}, Lorg/apache/tools/ant/taskdefs/Execute;->isFailure(I)Z

    move-result v3

    if-eqz v3, :cond_0

    new-instance v3, Lorg/apache/tools/ant/BuildException;

    new-instance v4, Ljava/lang/StringBuffer;

    invoke-direct {v4}, Ljava/lang/StringBuffer;-><init>()V

    const/4 v5, 0x0

    aget-object v5, p1, v5

    invoke-virtual {v4, v5}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    move-result-object v4

    const-string v5, " failed with return code "

    invoke-virtual {v4, v5}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    move-result-object v4

    invoke-virtual {v4, v2}, Ljava/lang/StringBuffer;->append(I)Ljava/lang/StringBuffer;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuffer;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {p0}, Lorg/apache/tools/ant/Task;->getLocation()Lorg/apache/tools/ant/Location;

    move-result-object v5

    invoke-direct {v3, v4, v5}, Lorg/apache/tools/ant/BuildException;-><init>(Ljava/lang/String;Lorg/apache/tools/ant/Location;)V

    throw v3
    :try_end_0
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_0

    :catch_0
    move-exception v0

    new-instance v3, Lorg/apache/tools/ant/BuildException;

    new-instance v4, Ljava/lang/StringBuffer;

    invoke-direct {v4}, Ljava/lang/StringBuffer;-><init>()V

    const-string v5, "Could not launch "

    invoke-virtual {v4, v5}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    move-result-object v4

    aget-object v5, p1, v6

    invoke-virtual {v4, v5}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    move-result-object v4

    const-string v5, ": "

    invoke-virtual {v4, v5}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    move-result-object v4

    invoke-virtual {v4, v0}, Ljava/lang/StringBuffer;->append(Ljava/lang/Object;)Ljava/lang/StringBuffer;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuffer;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {p0}, Lorg/apache/tools/ant/Task;->getLocation()Lorg/apache/tools/ant/Location;

    move-result-object v5

    invoke-direct {v3, v4, v5}, Lorg/apache/tools/ant/BuildException;-><init>(Ljava/lang/String;Lorg/apache/tools/ant/Location;)V

    throw v3

    :cond_0
    return-void
.end method

.method public static toString(Ljava/io/ByteArrayOutputStream;)Ljava/lang/String;
    .locals 1
    .param p0    # Ljava/io/ByteArrayOutputStream;

    const-string v0, "z/os"

    invoke-static {v0}, Lorg/apache/tools/ant/taskdefs/condition/Os;->isFamily(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_0

    :try_start_0
    const-string v0, "Cp1047"

    invoke-virtual {p0, v0}, Ljava/io/ByteArrayOutputStream;->toString(Ljava/lang/String;)Ljava/lang/String;
    :try_end_0
    .catch Ljava/io/UnsupportedEncodingException; {:try_start_0 .. :try_end_0} :catch_1

    move-result-object v0

    :goto_0
    return-object v0

    :cond_0
    const-string v0, "os/400"

    invoke-static {v0}, Lorg/apache/tools/ant/taskdefs/condition/Os;->isFamily(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_1

    :try_start_1
    const-string v0, "Cp500"

    invoke-virtual {p0, v0}, Ljava/io/ByteArrayOutputStream;->toString(Ljava/lang/String;)Ljava/lang/String;
    :try_end_1
    .catch Ljava/io/UnsupportedEncodingException; {:try_start_1 .. :try_end_1} :catch_0

    move-result-object v0

    goto :goto_0

    :catch_0
    move-exception v0

    :cond_1
    :goto_1
    invoke-virtual {p0}, Ljava/io/ByteArrayOutputStream;->toString()Ljava/lang/String;

    move-result-object v0

    goto :goto_0

    :catch_1
    move-exception v0

    goto :goto_1
.end method


# virtual methods
.method public execute()I
    .locals 8
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    iget-object v3, p0, Lorg/apache/tools/ant/taskdefs/Execute;->workingDirectory:Ljava/io/File;

    if-eqz v3, :cond_0

    iget-object v3, p0, Lorg/apache/tools/ant/taskdefs/Execute;->workingDirectory:Ljava/io/File;

    invoke-virtual {v3}, Ljava/io/File;->exists()Z

    move-result v3

    if-nez v3, :cond_0

    new-instance v3, Lorg/apache/tools/ant/BuildException;

    new-instance v4, Ljava/lang/StringBuffer;

    invoke-direct {v4}, Ljava/lang/StringBuffer;-><init>()V

    iget-object v5, p0, Lorg/apache/tools/ant/taskdefs/Execute;->workingDirectory:Ljava/io/File;

    invoke-virtual {v4, v5}, Ljava/lang/StringBuffer;->append(Ljava/lang/Object;)Ljava/lang/StringBuffer;

    move-result-object v4

    const-string v5, " doesn\'t exist."

    invoke-virtual {v4, v5}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuffer;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-direct {v3, v4}, Lorg/apache/tools/ant/BuildException;-><init>(Ljava/lang/String;)V

    throw v3

    :cond_0
    iget-object v3, p0, Lorg/apache/tools/ant/taskdefs/Execute;->project:Lorg/apache/tools/ant/Project;

    invoke-virtual {p0}, Lorg/apache/tools/ant/taskdefs/Execute;->getCommandline()[Ljava/lang/String;

    move-result-object v4

    invoke-virtual {p0}, Lorg/apache/tools/ant/taskdefs/Execute;->getEnvironment()[Ljava/lang/String;

    move-result-object v5

    iget-object v6, p0, Lorg/apache/tools/ant/taskdefs/Execute;->workingDirectory:Ljava/io/File;

    iget-boolean v7, p0, Lorg/apache/tools/ant/taskdefs/Execute;->useVMLauncher:Z

    invoke-static {v3, v4, v5, v6, v7}, Lorg/apache/tools/ant/taskdefs/Execute;->launch(Lorg/apache/tools/ant/Project;[Ljava/lang/String;[Ljava/lang/String;Ljava/io/File;Z)Ljava/lang/Process;

    move-result-object v1

    :try_start_0
    iget-object v3, p0, Lorg/apache/tools/ant/taskdefs/Execute;->streamHandler:Lorg/apache/tools/ant/taskdefs/ExecuteStreamHandler;

    invoke-virtual {v1}, Ljava/lang/Process;->getOutputStream()Ljava/io/OutputStream;

    move-result-object v4

    invoke-interface {v3, v4}, Lorg/apache/tools/ant/taskdefs/ExecuteStreamHandler;->setProcessInputStream(Ljava/io/OutputStream;)V

    iget-object v3, p0, Lorg/apache/tools/ant/taskdefs/Execute;->streamHandler:Lorg/apache/tools/ant/taskdefs/ExecuteStreamHandler;

    invoke-virtual {v1}, Ljava/lang/Process;->getInputStream()Ljava/io/InputStream;

    move-result-object v4

    invoke-interface {v3, v4}, Lorg/apache/tools/ant/taskdefs/ExecuteStreamHandler;->setProcessOutputStream(Ljava/io/InputStream;)V

    iget-object v3, p0, Lorg/apache/tools/ant/taskdefs/Execute;->streamHandler:Lorg/apache/tools/ant/taskdefs/ExecuteStreamHandler;

    invoke-virtual {v1}, Ljava/lang/Process;->getErrorStream()Ljava/io/InputStream;

    move-result-object v4

    invoke-interface {v3, v4}, Lorg/apache/tools/ant/taskdefs/ExecuteStreamHandler;->setProcessErrorStream(Ljava/io/InputStream;)V
    :try_end_0
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_0

    iget-object v3, p0, Lorg/apache/tools/ant/taskdefs/Execute;->streamHandler:Lorg/apache/tools/ant/taskdefs/ExecuteStreamHandler;

    invoke-interface {v3}, Lorg/apache/tools/ant/taskdefs/ExecuteStreamHandler;->start()V

    :try_start_1
    sget-object v3, Lorg/apache/tools/ant/taskdefs/Execute;->processDestroyer:Lorg/apache/tools/ant/taskdefs/ProcessDestroyer;

    invoke-virtual {v3, v1}, Lorg/apache/tools/ant/taskdefs/ProcessDestroyer;->add(Ljava/lang/Process;)Z

    iget-object v3, p0, Lorg/apache/tools/ant/taskdefs/Execute;->watchdog:Lorg/apache/tools/ant/taskdefs/ExecuteWatchdog;

    if-eqz v3, :cond_1

    iget-object v3, p0, Lorg/apache/tools/ant/taskdefs/Execute;->watchdog:Lorg/apache/tools/ant/taskdefs/ExecuteWatchdog;

    invoke-virtual {v3, v1}, Lorg/apache/tools/ant/taskdefs/ExecuteWatchdog;->start(Ljava/lang/Process;)V

    :cond_1
    invoke-virtual {p0, v1}, Lorg/apache/tools/ant/taskdefs/Execute;->waitFor(Ljava/lang/Process;)V

    iget-object v3, p0, Lorg/apache/tools/ant/taskdefs/Execute;->watchdog:Lorg/apache/tools/ant/taskdefs/ExecuteWatchdog;

    if-eqz v3, :cond_2

    iget-object v3, p0, Lorg/apache/tools/ant/taskdefs/Execute;->watchdog:Lorg/apache/tools/ant/taskdefs/ExecuteWatchdog;

    invoke-virtual {v3}, Lorg/apache/tools/ant/taskdefs/ExecuteWatchdog;->stop()V

    :cond_2
    iget-object v3, p0, Lorg/apache/tools/ant/taskdefs/Execute;->streamHandler:Lorg/apache/tools/ant/taskdefs/ExecuteStreamHandler;

    invoke-interface {v3}, Lorg/apache/tools/ant/taskdefs/ExecuteStreamHandler;->stop()V

    invoke-static {v1}, Lorg/apache/tools/ant/taskdefs/Execute;->closeStreams(Ljava/lang/Process;)V

    iget-object v3, p0, Lorg/apache/tools/ant/taskdefs/Execute;->watchdog:Lorg/apache/tools/ant/taskdefs/ExecuteWatchdog;

    if-eqz v3, :cond_3

    iget-object v3, p0, Lorg/apache/tools/ant/taskdefs/Execute;->watchdog:Lorg/apache/tools/ant/taskdefs/ExecuteWatchdog;

    invoke-virtual {v3}, Lorg/apache/tools/ant/taskdefs/ExecuteWatchdog;->checkException()V

    :cond_3
    invoke-virtual {p0}, Lorg/apache/tools/ant/taskdefs/Execute;->getExitValue()I
    :try_end_1
    .catch Ljava/lang/ThreadDeath; {:try_start_1 .. :try_end_1} :catch_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    move-result v3

    sget-object v4, Lorg/apache/tools/ant/taskdefs/Execute;->processDestroyer:Lorg/apache/tools/ant/taskdefs/ProcessDestroyer;

    invoke-virtual {v4, v1}, Lorg/apache/tools/ant/taskdefs/ProcessDestroyer;->remove(Ljava/lang/Process;)Z

    return v3

    :catch_0
    move-exception v0

    invoke-virtual {v1}, Ljava/lang/Process;->destroy()V

    throw v0

    :catch_1
    move-exception v2

    :try_start_2
    invoke-virtual {v1}, Ljava/lang/Process;->destroy()V

    throw v2
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    :catchall_0
    move-exception v3

    sget-object v4, Lorg/apache/tools/ant/taskdefs/Execute;->processDestroyer:Lorg/apache/tools/ant/taskdefs/ProcessDestroyer;

    invoke-virtual {v4, v1}, Lorg/apache/tools/ant/taskdefs/ProcessDestroyer;->remove(Ljava/lang/Process;)Z

    throw v3
.end method

.method public getCommandline()[Ljava/lang/String;
    .locals 1

    iget-object v0, p0, Lorg/apache/tools/ant/taskdefs/Execute;->cmdl:[Ljava/lang/String;

    return-object v0
.end method

.method public getEnvironment()[Ljava/lang/String;
    .locals 1

    iget-object v0, p0, Lorg/apache/tools/ant/taskdefs/Execute;->env:[Ljava/lang/String;

    if-eqz v0, :cond_0

    iget-boolean v0, p0, Lorg/apache/tools/ant/taskdefs/Execute;->newEnvironment:Z

    if-eqz v0, :cond_1

    :cond_0
    iget-object v0, p0, Lorg/apache/tools/ant/taskdefs/Execute;->env:[Ljava/lang/String;

    :goto_0
    return-object v0

    :cond_1
    invoke-direct {p0}, Lorg/apache/tools/ant/taskdefs/Execute;->patchEnvironment()[Ljava/lang/String;

    move-result-object v0

    goto :goto_0
.end method

.method public getExitValue()I
    .locals 1

    iget v0, p0, Lorg/apache/tools/ant/taskdefs/Execute;->exitValue:I

    return v0
.end method

.method public getWorkingDirectory()Ljava/io/File;
    .locals 2

    iget-object v0, p0, Lorg/apache/tools/ant/taskdefs/Execute;->workingDirectory:Ljava/io/File;

    if-nez v0, :cond_0

    new-instance v0, Ljava/io/File;

    sget-object v1, Lorg/apache/tools/ant/taskdefs/Execute;->antWorkingDirectory:Ljava/lang/String;

    invoke-direct {v0, v1}, Ljava/io/File;-><init>(Ljava/lang/String;)V

    :goto_0
    return-object v0

    :cond_0
    iget-object v0, p0, Lorg/apache/tools/ant/taskdefs/Execute;->workingDirectory:Ljava/io/File;

    goto :goto_0
.end method

.method public isFailure()Z
    .locals 1

    invoke-virtual {p0}, Lorg/apache/tools/ant/taskdefs/Execute;->getExitValue()I

    move-result v0

    invoke-static {v0}, Lorg/apache/tools/ant/taskdefs/Execute;->isFailure(I)Z

    move-result v0

    return v0
.end method

.method public killedProcess()Z
    .locals 1

    iget-object v0, p0, Lorg/apache/tools/ant/taskdefs/Execute;->watchdog:Lorg/apache/tools/ant/taskdefs/ExecuteWatchdog;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lorg/apache/tools/ant/taskdefs/Execute;->watchdog:Lorg/apache/tools/ant/taskdefs/ExecuteWatchdog;

    invoke-virtual {v0}, Lorg/apache/tools/ant/taskdefs/ExecuteWatchdog;->killedProcess()Z

    move-result v0

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public setAntRun(Lorg/apache/tools/ant/Project;)V
    .locals 0
    .param p1    # Lorg/apache/tools/ant/Project;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lorg/apache/tools/ant/BuildException;
        }
    .end annotation

    iput-object p1, p0, Lorg/apache/tools/ant/taskdefs/Execute;->project:Lorg/apache/tools/ant/Project;

    return-void
.end method

.method public setCommandline([Ljava/lang/String;)V
    .locals 0
    .param p1    # [Ljava/lang/String;

    iput-object p1, p0, Lorg/apache/tools/ant/taskdefs/Execute;->cmdl:[Ljava/lang/String;

    return-void
.end method

.method public setEnvironment([Ljava/lang/String;)V
    .locals 0
    .param p1    # [Ljava/lang/String;

    iput-object p1, p0, Lorg/apache/tools/ant/taskdefs/Execute;->env:[Ljava/lang/String;

    return-void
.end method

.method protected setExitValue(I)V
    .locals 0
    .param p1    # I

    iput p1, p0, Lorg/apache/tools/ant/taskdefs/Execute;->exitValue:I

    return-void
.end method

.method public setNewenvironment(Z)V
    .locals 0
    .param p1    # Z

    iput-boolean p1, p0, Lorg/apache/tools/ant/taskdefs/Execute;->newEnvironment:Z

    return-void
.end method

.method public setSpawn(Z)V
    .locals 0
    .param p1    # Z

    iput-boolean p1, p0, Lorg/apache/tools/ant/taskdefs/Execute;->spawn:Z

    return-void
.end method

.method public setStreamHandler(Lorg/apache/tools/ant/taskdefs/ExecuteStreamHandler;)V
    .locals 0
    .param p1    # Lorg/apache/tools/ant/taskdefs/ExecuteStreamHandler;

    iput-object p1, p0, Lorg/apache/tools/ant/taskdefs/Execute;->streamHandler:Lorg/apache/tools/ant/taskdefs/ExecuteStreamHandler;

    return-void
.end method

.method public setVMLauncher(Z)V
    .locals 0
    .param p1    # Z

    iput-boolean p1, p0, Lorg/apache/tools/ant/taskdefs/Execute;->useVMLauncher:Z

    return-void
.end method

.method public setWorkingDirectory(Ljava/io/File;)V
    .locals 2
    .param p1    # Ljava/io/File;

    if-eqz p1, :cond_0

    invoke-virtual {p1}, Ljava/io/File;->getAbsolutePath()Ljava/lang/String;

    move-result-object v0

    sget-object v1, Lorg/apache/tools/ant/taskdefs/Execute;->antWorkingDirectory:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_1

    :cond_0
    const/4 p1, 0x0

    :cond_1
    iput-object p1, p0, Lorg/apache/tools/ant/taskdefs/Execute;->workingDirectory:Ljava/io/File;

    return-void
.end method

.method public spawn()V
    .locals 10
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    const/4 v9, 0x3

    iget-object v4, p0, Lorg/apache/tools/ant/taskdefs/Execute;->workingDirectory:Ljava/io/File;

    if-eqz v4, :cond_0

    iget-object v4, p0, Lorg/apache/tools/ant/taskdefs/Execute;->workingDirectory:Ljava/io/File;

    invoke-virtual {v4}, Ljava/io/File;->exists()Z

    move-result v4

    if-nez v4, :cond_0

    new-instance v4, Lorg/apache/tools/ant/BuildException;

    new-instance v5, Ljava/lang/StringBuffer;

    invoke-direct {v5}, Ljava/lang/StringBuffer;-><init>()V

    iget-object v6, p0, Lorg/apache/tools/ant/taskdefs/Execute;->workingDirectory:Ljava/io/File;

    invoke-virtual {v5, v6}, Ljava/lang/StringBuffer;->append(Ljava/lang/Object;)Ljava/lang/StringBuffer;

    move-result-object v5

    const-string v6, " doesn\'t exist."

    invoke-virtual {v5, v6}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/StringBuffer;->toString()Ljava/lang/String;

    move-result-object v5

    invoke-direct {v4, v5}, Lorg/apache/tools/ant/BuildException;-><init>(Ljava/lang/String;)V

    throw v4

    :cond_0
    iget-object v4, p0, Lorg/apache/tools/ant/taskdefs/Execute;->project:Lorg/apache/tools/ant/Project;

    invoke-virtual {p0}, Lorg/apache/tools/ant/taskdefs/Execute;->getCommandline()[Ljava/lang/String;

    move-result-object v5

    invoke-virtual {p0}, Lorg/apache/tools/ant/taskdefs/Execute;->getEnvironment()[Ljava/lang/String;

    move-result-object v6

    iget-object v7, p0, Lorg/apache/tools/ant/taskdefs/Execute;->workingDirectory:Ljava/io/File;

    iget-boolean v8, p0, Lorg/apache/tools/ant/taskdefs/Execute;->useVMLauncher:Z

    invoke-static {v4, v5, v6, v7, v8}, Lorg/apache/tools/ant/taskdefs/Execute;->launch(Lorg/apache/tools/ant/Project;[Ljava/lang/String;[Ljava/lang/String;Ljava/io/File;Z)Ljava/lang/Process;

    move-result-object v3

    const-string v4, "windows"

    invoke-static {v4}, Lorg/apache/tools/ant/taskdefs/condition/Os;->isFamily(Ljava/lang/String;)Z

    move-result v4

    if-eqz v4, :cond_1

    const-wide/16 v4, 0x3e8

    :try_start_0
    invoke-static {v4, v5}, Ljava/lang/Thread;->sleep(J)V
    :try_end_0
    .catch Ljava/lang/InterruptedException; {:try_start_0 .. :try_end_0} :catch_0

    :cond_1
    :goto_0
    new-instance v0, Lorg/apache/tools/ant/taskdefs/Execute$1;

    invoke-direct {v0, p0}, Lorg/apache/tools/ant/taskdefs/Execute$1;-><init>(Lorg/apache/tools/ant/taskdefs/Execute;)V

    new-instance v2, Lorg/apache/tools/ant/taskdefs/PumpStreamHandler;

    invoke-direct {v2, v0}, Lorg/apache/tools/ant/taskdefs/PumpStreamHandler;-><init>(Ljava/io/OutputStream;)V

    invoke-virtual {v3}, Ljava/lang/Process;->getErrorStream()Ljava/io/InputStream;

    move-result-object v4

    invoke-interface {v2, v4}, Lorg/apache/tools/ant/taskdefs/ExecuteStreamHandler;->setProcessErrorStream(Ljava/io/InputStream;)V

    invoke-virtual {v3}, Ljava/lang/Process;->getInputStream()Ljava/io/InputStream;

    move-result-object v4

    invoke-interface {v2, v4}, Lorg/apache/tools/ant/taskdefs/ExecuteStreamHandler;->setProcessOutputStream(Ljava/io/InputStream;)V

    invoke-interface {v2}, Lorg/apache/tools/ant/taskdefs/ExecuteStreamHandler;->start()V

    invoke-virtual {v3}, Ljava/lang/Process;->getOutputStream()Ljava/io/OutputStream;

    move-result-object v4

    invoke-virtual {v4}, Ljava/io/OutputStream;->close()V

    iget-object v4, p0, Lorg/apache/tools/ant/taskdefs/Execute;->project:Lorg/apache/tools/ant/Project;

    new-instance v5, Ljava/lang/StringBuffer;

    invoke-direct {v5}, Ljava/lang/StringBuffer;-><init>()V

    const-string v6, "spawned process "

    invoke-virtual {v5, v6}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    move-result-object v5

    invoke-virtual {v3}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object v6

    invoke-virtual {v5, v6}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/StringBuffer;->toString()Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v4, v5, v9}, Lorg/apache/tools/ant/Project;->log(Ljava/lang/String;I)V

    return-void

    :catch_0
    move-exception v1

    iget-object v4, p0, Lorg/apache/tools/ant/taskdefs/Execute;->project:Lorg/apache/tools/ant/Project;

    const-string v5, "interruption in the sleep after having spawned a process"

    invoke-virtual {v4, v5, v9}, Lorg/apache/tools/ant/Project;->log(Ljava/lang/String;I)V

    goto :goto_0
.end method

.method protected waitFor(Ljava/lang/Process;)V
    .locals 2
    .param p1    # Ljava/lang/Process;

    :try_start_0
    invoke-virtual {p1}, Ljava/lang/Process;->waitFor()I

    invoke-virtual {p1}, Ljava/lang/Process;->exitValue()I

    move-result v1

    invoke-virtual {p0, v1}, Lorg/apache/tools/ant/taskdefs/Execute;->setExitValue(I)V
    :try_end_0
    .catch Ljava/lang/InterruptedException; {:try_start_0 .. :try_end_0} :catch_0

    :goto_0
    return-void

    :catch_0
    move-exception v0

    invoke-virtual {p1}, Ljava/lang/Process;->destroy()V

    goto :goto_0
.end method
