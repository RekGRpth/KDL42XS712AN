.class public Lorg/apache/tools/ant/taskdefs/ImportTask;
.super Lorg/apache/tools/ant/Task;
.source "ImportTask.java"


# static fields
.field private static final FILE_UTILS:Lorg/apache/tools/ant/util/FileUtils;


# instance fields
.field private file:Ljava/lang/String;

.field private optional:Z


# direct methods
.method static constructor <clinit>()V
    .locals 1

    invoke-static {}, Lorg/apache/tools/ant/util/FileUtils;->getFileUtils()Lorg/apache/tools/ant/util/FileUtils;

    move-result-object v0

    sput-object v0, Lorg/apache/tools/ant/taskdefs/ImportTask;->FILE_UTILS:Lorg/apache/tools/ant/util/FileUtils;

    return-void
.end method

.method public constructor <init>()V
    .locals 0

    invoke-direct {p0}, Lorg/apache/tools/ant/Task;-><init>()V

    return-void
.end method


# virtual methods
.method public execute()V
    .locals 11

    const/4 v10, 0x3

    iget-object v7, p0, Lorg/apache/tools/ant/taskdefs/ImportTask;->file:Ljava/lang/String;

    if-nez v7, :cond_0

    new-instance v7, Lorg/apache/tools/ant/BuildException;

    const-string v8, "import requires file attribute"

    invoke-direct {v7, v8}, Lorg/apache/tools/ant/BuildException;-><init>(Ljava/lang/String;)V

    throw v7

    :cond_0
    invoke-virtual {p0}, Lorg/apache/tools/ant/taskdefs/ImportTask;->getOwningTarget()Lorg/apache/tools/ant/Target;

    move-result-object v7

    if-eqz v7, :cond_1

    const-string v7, ""

    invoke-virtual {p0}, Lorg/apache/tools/ant/taskdefs/ImportTask;->getOwningTarget()Lorg/apache/tools/ant/Target;

    move-result-object v8

    invoke-virtual {v8}, Lorg/apache/tools/ant/Target;->getName()Ljava/lang/String;

    move-result-object v8

    invoke-virtual {v7, v8}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v7

    if-nez v7, :cond_2

    :cond_1
    new-instance v7, Lorg/apache/tools/ant/BuildException;

    const-string v8, "import only allowed as a top-level task"

    invoke-direct {v7, v8}, Lorg/apache/tools/ant/BuildException;-><init>(Ljava/lang/String;)V

    throw v7

    :cond_2
    invoke-virtual {p0}, Lorg/apache/tools/ant/taskdefs/ImportTask;->getProject()Lorg/apache/tools/ant/Project;

    move-result-object v7

    const-string v8, "ant.projectHelper"

    invoke-virtual {v7, v8}, Lorg/apache/tools/ant/Project;->getReference(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Lorg/apache/tools/ant/ProjectHelper;

    if-nez v3, :cond_3

    new-instance v7, Lorg/apache/tools/ant/BuildException;

    const-string v8, "import requires support in ProjectHelper"

    invoke-direct {v7, v8}, Lorg/apache/tools/ant/BuildException;-><init>(Ljava/lang/String;)V

    throw v7

    :cond_3
    invoke-virtual {v3}, Lorg/apache/tools/ant/ProjectHelper;->getImportStack()Ljava/util/Vector;

    move-result-object v4

    invoke-virtual {v4}, Ljava/util/Vector;->size()I

    move-result v7

    if-nez v7, :cond_4

    new-instance v7, Lorg/apache/tools/ant/BuildException;

    const-string v8, "import requires support in ProjectHelper"

    invoke-direct {v7, v8}, Lorg/apache/tools/ant/BuildException;-><init>(Ljava/lang/String;)V

    throw v7

    :cond_4
    invoke-virtual {p0}, Lorg/apache/tools/ant/taskdefs/ImportTask;->getLocation()Lorg/apache/tools/ant/Location;

    move-result-object v7

    if-eqz v7, :cond_5

    invoke-virtual {p0}, Lorg/apache/tools/ant/taskdefs/ImportTask;->getLocation()Lorg/apache/tools/ant/Location;

    move-result-object v7

    invoke-virtual {v7}, Lorg/apache/tools/ant/Location;->getFileName()Ljava/lang/String;

    move-result-object v7

    if-nez v7, :cond_6

    :cond_5
    new-instance v7, Lorg/apache/tools/ant/BuildException;

    const-string v8, "Unable to get location of import task"

    invoke-direct {v7, v8}, Lorg/apache/tools/ant/BuildException;-><init>(Ljava/lang/String;)V

    throw v7

    :cond_6
    new-instance v7, Ljava/io/File;

    invoke-virtual {p0}, Lorg/apache/tools/ant/taskdefs/ImportTask;->getLocation()Lorg/apache/tools/ant/Location;

    move-result-object v8

    invoke-virtual {v8}, Lorg/apache/tools/ant/Location;->getFileName()Ljava/lang/String;

    move-result-object v8

    invoke-direct {v7, v8}, Ljava/io/File;-><init>(Ljava/lang/String;)V

    invoke-virtual {v7}, Ljava/io/File;->getAbsoluteFile()Ljava/io/File;

    move-result-object v0

    new-instance v1, Ljava/io/File;

    invoke-virtual {v0}, Ljava/io/File;->getParent()Ljava/lang/String;

    move-result-object v7

    invoke-direct {v1, v7}, Ljava/io/File;-><init>(Ljava/lang/String;)V

    sget-object v7, Lorg/apache/tools/ant/taskdefs/ImportTask;->FILE_UTILS:Lorg/apache/tools/ant/util/FileUtils;

    iget-object v8, p0, Lorg/apache/tools/ant/taskdefs/ImportTask;->file:Ljava/lang/String;

    invoke-virtual {v7, v1, v8}, Lorg/apache/tools/ant/util/FileUtils;->resolveFile(Ljava/io/File;Ljava/lang/String;)Ljava/io/File;

    move-result-object v5

    invoke-virtual {p0}, Lorg/apache/tools/ant/taskdefs/ImportTask;->getProject()Lorg/apache/tools/ant/Project;

    move-result-object v7

    new-instance v8, Ljava/lang/StringBuffer;

    invoke-direct {v8}, Ljava/lang/StringBuffer;-><init>()V

    const-string v9, "Importing file "

    invoke-virtual {v8, v9}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    move-result-object v8

    invoke-virtual {v8, v5}, Ljava/lang/StringBuffer;->append(Ljava/lang/Object;)Ljava/lang/StringBuffer;

    move-result-object v8

    const-string v9, " from "

    invoke-virtual {v8, v9}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    move-result-object v8

    invoke-virtual {v0}, Ljava/io/File;->getAbsolutePath()Ljava/lang/String;

    move-result-object v9

    invoke-virtual {v8, v9}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    move-result-object v8

    invoke-virtual {v8}, Ljava/lang/StringBuffer;->toString()Ljava/lang/String;

    move-result-object v8

    invoke-virtual {v7, v8, v10}, Lorg/apache/tools/ant/Project;->log(Ljava/lang/String;I)V

    invoke-virtual {v5}, Ljava/io/File;->exists()Z

    move-result v7

    if-nez v7, :cond_8

    new-instance v7, Ljava/lang/StringBuffer;

    invoke-direct {v7}, Ljava/lang/StringBuffer;-><init>()V

    const-string v8, "Cannot find "

    invoke-virtual {v7, v8}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    move-result-object v7

    iget-object v8, p0, Lorg/apache/tools/ant/taskdefs/ImportTask;->file:Ljava/lang/String;

    invoke-virtual {v7, v8}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    move-result-object v7

    const-string v8, " imported from "

    invoke-virtual {v7, v8}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    move-result-object v7

    invoke-virtual {v0}, Ljava/io/File;->getAbsolutePath()Ljava/lang/String;

    move-result-object v8

    invoke-virtual {v7, v8}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    move-result-object v7

    invoke-virtual {v7}, Ljava/lang/StringBuffer;->toString()Ljava/lang/String;

    move-result-object v6

    iget-boolean v7, p0, Lorg/apache/tools/ant/taskdefs/ImportTask;->optional:Z

    if-eqz v7, :cond_7

    invoke-virtual {p0}, Lorg/apache/tools/ant/taskdefs/ImportTask;->getProject()Lorg/apache/tools/ant/Project;

    move-result-object v7

    invoke-virtual {v7, v6, v10}, Lorg/apache/tools/ant/Project;->log(Ljava/lang/String;I)V

    :goto_0
    return-void

    :cond_7
    new-instance v7, Lorg/apache/tools/ant/BuildException;

    invoke-direct {v7, v6}, Lorg/apache/tools/ant/BuildException;-><init>(Ljava/lang/String;)V

    throw v7

    :cond_8
    invoke-virtual {v4, v5}, Ljava/util/Vector;->contains(Ljava/lang/Object;)Z

    move-result v7

    if-eqz v7, :cond_9

    invoke-virtual {p0}, Lorg/apache/tools/ant/taskdefs/ImportTask;->getProject()Lorg/apache/tools/ant/Project;

    move-result-object v7

    new-instance v8, Ljava/lang/StringBuffer;

    invoke-direct {v8}, Ljava/lang/StringBuffer;-><init>()V

    const-string v9, "Skipped already imported file:\n   "

    invoke-virtual {v8, v9}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    move-result-object v8

    invoke-virtual {v8, v5}, Ljava/lang/StringBuffer;->append(Ljava/lang/Object;)Ljava/lang/StringBuffer;

    move-result-object v8

    const-string v9, "\n"

    invoke-virtual {v8, v9}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    move-result-object v8

    invoke-virtual {v8}, Ljava/lang/StringBuffer;->toString()Ljava/lang/String;

    move-result-object v8

    invoke-virtual {v7, v8, v10}, Lorg/apache/tools/ant/Project;->log(Ljava/lang/String;I)V

    goto :goto_0

    :cond_9
    :try_start_0
    invoke-virtual {p0}, Lorg/apache/tools/ant/taskdefs/ImportTask;->getProject()Lorg/apache/tools/ant/Project;

    move-result-object v7

    invoke-virtual {v3, v7, v5}, Lorg/apache/tools/ant/ProjectHelper;->parse(Lorg/apache/tools/ant/Project;Ljava/lang/Object;)V
    :try_end_0
    .catch Lorg/apache/tools/ant/BuildException; {:try_start_0 .. :try_end_0} :catch_0

    goto :goto_0

    :catch_0
    move-exception v2

    invoke-virtual {p0}, Lorg/apache/tools/ant/taskdefs/ImportTask;->getLocation()Lorg/apache/tools/ant/Location;

    move-result-object v7

    invoke-static {v2, v7}, Lorg/apache/tools/ant/ProjectHelper;->addLocationToBuildException(Lorg/apache/tools/ant/BuildException;Lorg/apache/tools/ant/Location;)Lorg/apache/tools/ant/BuildException;

    move-result-object v7

    throw v7
.end method

.method public setFile(Ljava/lang/String;)V
    .locals 0
    .param p1    # Ljava/lang/String;

    iput-object p1, p0, Lorg/apache/tools/ant/taskdefs/ImportTask;->file:Ljava/lang/String;

    return-void
.end method

.method public setOptional(Z)V
    .locals 0
    .param p1    # Z

    iput-boolean p1, p0, Lorg/apache/tools/ant/taskdefs/ImportTask;->optional:Z

    return-void
.end method
