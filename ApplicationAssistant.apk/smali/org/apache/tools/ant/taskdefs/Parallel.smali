.class public Lorg/apache/tools/ant/taskdefs/Parallel;
.super Lorg/apache/tools/ant/Task;
.source "Parallel.java"

# interfaces
.implements Lorg/apache/tools/ant/TaskContainer;


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lorg/apache/tools/ant/taskdefs/Parallel$TaskRunnable;,
        Lorg/apache/tools/ant/taskdefs/Parallel$TaskList;
    }
.end annotation


# static fields
.field static class$java$lang$Runtime:Ljava/lang/Class;


# instance fields
.field private daemonTasks:Lorg/apache/tools/ant/taskdefs/Parallel$TaskList;

.field private exceptionMessage:Ljava/lang/StringBuffer;

.field private failOnAny:Z

.field private firstException:Ljava/lang/Throwable;

.field private firstLocation:Lorg/apache/tools/ant/Location;

.field private nestedTasks:Ljava/util/Vector;

.field private numExceptions:I

.field private numThreads:I

.field private numThreadsPerProcessor:I

.field private final semaphore:Ljava/lang/Object;

.field private volatile stillRunning:Z

.field private timedOut:Z

.field private timeout:J


# direct methods
.method public constructor <init>()V
    .locals 2

    const/4 v1, 0x0

    invoke-direct {p0}, Lorg/apache/tools/ant/Task;-><init>()V

    new-instance v0, Ljava/util/Vector;

    invoke-direct {v0}, Ljava/util/Vector;-><init>()V

    iput-object v0, p0, Lorg/apache/tools/ant/taskdefs/Parallel;->nestedTasks:Ljava/util/Vector;

    new-instance v0, Ljava/lang/Object;

    invoke-direct {v0}, Ljava/lang/Object;-><init>()V

    iput-object v0, p0, Lorg/apache/tools/ant/taskdefs/Parallel;->semaphore:Ljava/lang/Object;

    iput v1, p0, Lorg/apache/tools/ant/taskdefs/Parallel;->numThreads:I

    iput v1, p0, Lorg/apache/tools/ant/taskdefs/Parallel;->numThreadsPerProcessor:I

    iput v1, p0, Lorg/apache/tools/ant/taskdefs/Parallel;->numExceptions:I

    return-void
.end method

.method static access$100(Lorg/apache/tools/ant/taskdefs/Parallel;)J
    .locals 2
    .param p0    # Lorg/apache/tools/ant/taskdefs/Parallel;

    iget-wide v0, p0, Lorg/apache/tools/ant/taskdefs/Parallel;->timeout:J

    return-wide v0
.end method

.method static access$200(Lorg/apache/tools/ant/taskdefs/Parallel;)Ljava/lang/Object;
    .locals 1
    .param p0    # Lorg/apache/tools/ant/taskdefs/Parallel;

    iget-object v0, p0, Lorg/apache/tools/ant/taskdefs/Parallel;->semaphore:Ljava/lang/Object;

    return-object v0
.end method

.method static access$302(Lorg/apache/tools/ant/taskdefs/Parallel;Z)Z
    .locals 0
    .param p0    # Lorg/apache/tools/ant/taskdefs/Parallel;
    .param p1    # Z

    iput-boolean p1, p0, Lorg/apache/tools/ant/taskdefs/Parallel;->stillRunning:Z

    return p1
.end method

.method static access$402(Lorg/apache/tools/ant/taskdefs/Parallel;Z)Z
    .locals 0
    .param p0    # Lorg/apache/tools/ant/taskdefs/Parallel;
    .param p1    # Z

    iput-boolean p1, p0, Lorg/apache/tools/ant/taskdefs/Parallel;->timedOut:Z

    return p1
.end method

.method static access$500(Lorg/apache/tools/ant/taskdefs/Parallel;)Z
    .locals 1
    .param p0    # Lorg/apache/tools/ant/taskdefs/Parallel;

    iget-boolean v0, p0, Lorg/apache/tools/ant/taskdefs/Parallel;->failOnAny:Z

    return v0
.end method

.method static class$(Ljava/lang/String;)Ljava/lang/Class;
    .locals 3
    .param p0    # Ljava/lang/String;

    :try_start_0
    invoke-static {p0}, Ljava/lang/Class;->forName(Ljava/lang/String;)Ljava/lang/Class;
    :try_end_0
    .catch Ljava/lang/ClassNotFoundException; {:try_start_0 .. :try_end_0} :catch_0

    move-result-object v1

    return-object v1

    :catch_0
    move-exception v0

    new-instance v1, Ljava/lang/NoClassDefFoundError;

    invoke-virtual {v0}, Ljava/lang/ClassNotFoundException;->getMessage()Ljava/lang/String;

    move-result-object v2

    invoke-direct {v1, v2}, Ljava/lang/NoClassDefFoundError;-><init>(Ljava/lang/String;)V

    throw v1
.end method

.method private getNumProcessors()I
    .locals 8

    const/4 v5, 0x0

    const/4 v6, 0x0

    :try_start_0
    new-array v3, v6, [Ljava/lang/Class;

    sget-object v6, Lorg/apache/tools/ant/taskdefs/Parallel;->class$java$lang$Runtime:Ljava/lang/Class;

    if-nez v6, :cond_0

    const-string v6, "java.lang.Runtime"

    invoke-static {v6}, Lorg/apache/tools/ant/taskdefs/Parallel;->class$(Ljava/lang/String;)Ljava/lang/Class;

    move-result-object v6

    sput-object v6, Lorg/apache/tools/ant/taskdefs/Parallel;->class$java$lang$Runtime:Ljava/lang/Class;

    :goto_0
    const-string v7, "availableProcessors"

    invoke-virtual {v6, v7, v3}, Ljava/lang/Class;->getMethod(Ljava/lang/String;[Ljava/lang/Class;)Ljava/lang/reflect/Method;

    move-result-object v1

    const/4 v6, 0x0

    new-array v0, v6, [Ljava/lang/Object;

    invoke-static {}, Ljava/lang/Runtime;->getRuntime()Ljava/lang/Runtime;

    move-result-object v6

    invoke-virtual {v1, v6, v0}, Ljava/lang/reflect/Method;->invoke(Ljava/lang/Object;[Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v4

    check-cast v4, Ljava/lang/Integer;

    invoke-virtual {v4}, Ljava/lang/Integer;->intValue()I

    move-result v5

    :goto_1
    return v5

    :cond_0
    sget-object v6, Lorg/apache/tools/ant/taskdefs/Parallel;->class$java$lang$Runtime:Ljava/lang/Class;
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    goto :goto_0

    :catch_0
    move-exception v2

    goto :goto_1
.end method

.method private processExceptions([Lorg/apache/tools/ant/taskdefs/Parallel$TaskRunnable;)V
    .locals 4
    .param p1    # [Lorg/apache/tools/ant/taskdefs/Parallel$TaskRunnable;

    if-nez p1, :cond_1

    :cond_0
    return-void

    :cond_1
    const/4 v0, 0x0

    :goto_0
    array-length v2, p1

    if-ge v0, v2, :cond_0

    aget-object v2, p1, v0

    invoke-virtual {v2}, Lorg/apache/tools/ant/taskdefs/Parallel$TaskRunnable;->getException()Ljava/lang/Throwable;

    move-result-object v1

    if-eqz v1, :cond_4

    iget v2, p0, Lorg/apache/tools/ant/taskdefs/Parallel;->numExceptions:I

    add-int/lit8 v2, v2, 0x1

    iput v2, p0, Lorg/apache/tools/ant/taskdefs/Parallel;->numExceptions:I

    iget-object v2, p0, Lorg/apache/tools/ant/taskdefs/Parallel;->firstException:Ljava/lang/Throwable;

    if-nez v2, :cond_2

    iput-object v1, p0, Lorg/apache/tools/ant/taskdefs/Parallel;->firstException:Ljava/lang/Throwable;

    :cond_2
    instance-of v2, v1, Lorg/apache/tools/ant/BuildException;

    if-eqz v2, :cond_3

    iget-object v2, p0, Lorg/apache/tools/ant/taskdefs/Parallel;->firstLocation:Lorg/apache/tools/ant/Location;

    sget-object v3, Lorg/apache/tools/ant/Location;->UNKNOWN_LOCATION:Lorg/apache/tools/ant/Location;

    if-ne v2, v3, :cond_3

    move-object v2, v1

    check-cast v2, Lorg/apache/tools/ant/BuildException;

    invoke-virtual {v2}, Lorg/apache/tools/ant/BuildException;->getLocation()Lorg/apache/tools/ant/Location;

    move-result-object v2

    iput-object v2, p0, Lorg/apache/tools/ant/taskdefs/Parallel;->firstLocation:Lorg/apache/tools/ant/Location;

    :cond_3
    iget-object v2, p0, Lorg/apache/tools/ant/taskdefs/Parallel;->exceptionMessage:Ljava/lang/StringBuffer;

    sget-object v3, Lorg/apache/tools/ant/util/StringUtils;->LINE_SEP:Ljava/lang/String;

    invoke-virtual {v2, v3}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    iget-object v2, p0, Lorg/apache/tools/ant/taskdefs/Parallel;->exceptionMessage:Ljava/lang/StringBuffer;

    invoke-virtual {v1}, Ljava/lang/Throwable;->getMessage()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    :cond_4
    add-int/lit8 v0, v0, 0x1

    goto :goto_0
.end method

.method private spinThreads()V
    .locals 23
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lorg/apache/tools/ant/BuildException;
        }
    .end annotation

    move-object/from16 v0, p0

    iget-object v0, v0, Lorg/apache/tools/ant/taskdefs/Parallel;->nestedTasks:Ljava/util/Vector;

    move-object/from16 v17, v0

    invoke-virtual/range {v17 .. v17}, Ljava/util/Vector;->size()I

    move-result v10

    new-array v11, v10, [Lorg/apache/tools/ant/taskdefs/Parallel$TaskRunnable;

    const/16 v17, 0x1

    move/from16 v0, v17

    move-object/from16 v1, p0

    iput-boolean v0, v1, Lorg/apache/tools/ant/taskdefs/Parallel;->stillRunning:Z

    const/16 v17, 0x0

    move/from16 v0, v17

    move-object/from16 v1, p0

    iput-boolean v0, v1, Lorg/apache/tools/ant/taskdefs/Parallel;->timedOut:Z

    const/4 v14, 0x0

    move-object/from16 v0, p0

    iget-object v0, v0, Lorg/apache/tools/ant/taskdefs/Parallel;->nestedTasks:Ljava/util/Vector;

    move-object/from16 v17, v0

    invoke-virtual/range {v17 .. v17}, Ljava/util/Vector;->elements()Ljava/util/Enumeration;

    move-result-object v5

    :goto_0
    invoke-interface {v5}, Ljava/util/Enumeration;->hasMoreElements()Z

    move-result v17

    if-eqz v17, :cond_0

    invoke-interface {v5}, Ljava/util/Enumeration;->nextElement()Ljava/lang/Object;

    move-result-object v9

    check-cast v9, Lorg/apache/tools/ant/Task;

    new-instance v17, Lorg/apache/tools/ant/taskdefs/Parallel$TaskRunnable;

    move-object/from16 v0, v17

    move-object/from16 v1, p0

    invoke-direct {v0, v1, v9}, Lorg/apache/tools/ant/taskdefs/Parallel$TaskRunnable;-><init>(Lorg/apache/tools/ant/taskdefs/Parallel;Lorg/apache/tools/ant/Task;)V

    aput-object v17, v11, v14

    add-int/lit8 v14, v14, 0x1

    goto :goto_0

    :cond_0
    move-object/from16 v0, p0

    iget v0, v0, Lorg/apache/tools/ant/taskdefs/Parallel;->numThreads:I

    move/from16 v17, v0

    move/from16 v0, v17

    if-ge v10, v0, :cond_2

    move v8, v10

    :goto_1
    new-array v12, v8, [Lorg/apache/tools/ant/taskdefs/Parallel$TaskRunnable;

    const/4 v14, 0x0

    new-instance v6, Ljava/lang/ThreadGroup;

    const-string v17, "parallel"

    move-object/from16 v0, v17

    invoke-direct {v6, v0}, Ljava/lang/ThreadGroup;-><init>(Ljava/lang/String;)V

    const/4 v4, 0x0

    move-object/from16 v0, p0

    iget-object v0, v0, Lorg/apache/tools/ant/taskdefs/Parallel;->daemonTasks:Lorg/apache/tools/ant/taskdefs/Parallel$TaskList;

    move-object/from16 v17, v0

    if-eqz v17, :cond_1

    move-object/from16 v0, p0

    iget-object v0, v0, Lorg/apache/tools/ant/taskdefs/Parallel;->daemonTasks:Lorg/apache/tools/ant/taskdefs/Parallel$TaskList;

    move-object/from16 v17, v0

    invoke-static/range {v17 .. v17}, Lorg/apache/tools/ant/taskdefs/Parallel$TaskList;->access$000(Lorg/apache/tools/ant/taskdefs/Parallel$TaskList;)Ljava/util/List;

    move-result-object v17

    invoke-interface/range {v17 .. v17}, Ljava/util/List;->size()I

    move-result v17

    if-eqz v17, :cond_1

    move-object/from16 v0, p0

    iget-object v0, v0, Lorg/apache/tools/ant/taskdefs/Parallel;->daemonTasks:Lorg/apache/tools/ant/taskdefs/Parallel$TaskList;

    move-object/from16 v17, v0

    invoke-static/range {v17 .. v17}, Lorg/apache/tools/ant/taskdefs/Parallel$TaskList;->access$000(Lorg/apache/tools/ant/taskdefs/Parallel$TaskList;)Ljava/util/List;

    move-result-object v17

    invoke-interface/range {v17 .. v17}, Ljava/util/List;->size()I

    move-result v17

    move/from16 v0, v17

    new-array v4, v0, [Lorg/apache/tools/ant/taskdefs/Parallel$TaskRunnable;

    :cond_1
    move-object/from16 v0, p0

    iget-object v0, v0, Lorg/apache/tools/ant/taskdefs/Parallel;->semaphore:Ljava/lang/Object;

    move-object/from16 v18, v0

    monitor-enter v18

    :try_start_0
    monitor-exit v18
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    move-object/from16 v0, p0

    iget-object v0, v0, Lorg/apache/tools/ant/taskdefs/Parallel;->semaphore:Ljava/lang/Object;

    move-object/from16 v18, v0

    monitor-enter v18

    if-eqz v4, :cond_3

    const/4 v7, 0x0

    :goto_2
    :try_start_1
    array-length v0, v4

    move/from16 v17, v0

    move/from16 v0, v17

    if-ge v7, v0, :cond_3

    new-instance v19, Lorg/apache/tools/ant/taskdefs/Parallel$TaskRunnable;

    move-object/from16 v0, p0

    iget-object v0, v0, Lorg/apache/tools/ant/taskdefs/Parallel;->daemonTasks:Lorg/apache/tools/ant/taskdefs/Parallel$TaskList;

    move-object/from16 v17, v0

    invoke-static/range {v17 .. v17}, Lorg/apache/tools/ant/taskdefs/Parallel$TaskList;->access$000(Lorg/apache/tools/ant/taskdefs/Parallel$TaskList;)Ljava/util/List;

    move-result-object v17

    move-object/from16 v0, v17

    invoke-interface {v0, v7}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v17

    check-cast v17, Lorg/apache/tools/ant/Task;

    move-object/from16 v0, v19

    move-object/from16 v1, p0

    move-object/from16 v2, v17

    invoke-direct {v0, v1, v2}, Lorg/apache/tools/ant/taskdefs/Parallel$TaskRunnable;-><init>(Lorg/apache/tools/ant/taskdefs/Parallel;Lorg/apache/tools/ant/Task;)V

    aput-object v19, v4, v7

    new-instance v3, Ljava/lang/Thread;

    aget-object v17, v4, v7

    move-object/from16 v0, v17

    invoke-direct {v3, v6, v0}, Ljava/lang/Thread;-><init>(Ljava/lang/ThreadGroup;Ljava/lang/Runnable;)V

    const/16 v17, 0x1

    move/from16 v0, v17

    invoke-virtual {v3, v0}, Ljava/lang/Thread;->setDaemon(Z)V

    invoke-virtual {v3}, Ljava/lang/Thread;->start()V
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_2

    add-int/lit8 v7, v7, 0x1

    goto :goto_2

    :cond_2
    move-object/from16 v0, p0

    iget v8, v0, Lorg/apache/tools/ant/taskdefs/Parallel;->numThreads:I

    goto/16 :goto_1

    :catchall_0
    move-exception v17

    :try_start_2
    monitor-exit v18
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    throw v17

    :cond_3
    const/4 v7, 0x0

    move v15, v14

    :goto_3
    if-ge v7, v8, :cond_4

    add-int/lit8 v14, v15, 0x1

    :try_start_3
    aget-object v17, v11, v15

    aput-object v17, v12, v7

    new-instance v13, Ljava/lang/Thread;

    aget-object v17, v12, v7

    move-object/from16 v0, v17

    invoke-direct {v13, v6, v0}, Ljava/lang/Thread;-><init>(Ljava/lang/ThreadGroup;Ljava/lang/Runnable;)V

    invoke-virtual {v13}, Ljava/lang/Thread;->start()V
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_2

    add-int/lit8 v7, v7, 0x1

    move v15, v14

    goto :goto_3

    :cond_4
    :try_start_4
    move-object/from16 v0, p0

    iget-wide v0, v0, Lorg/apache/tools/ant/taskdefs/Parallel;->timeout:J

    move-wide/from16 v19, v0

    const-wide/16 v21, 0x0

    cmp-long v17, v19, v21

    if-eqz v17, :cond_5

    new-instance v16, Lorg/apache/tools/ant/taskdefs/Parallel$1;

    move-object/from16 v0, v16

    move-object/from16 v1, p0

    invoke-direct {v0, v1}, Lorg/apache/tools/ant/taskdefs/Parallel$1;-><init>(Lorg/apache/tools/ant/taskdefs/Parallel;)V

    invoke-virtual/range {v16 .. v16}, Ljava/lang/Thread;->start()V

    :cond_5
    :goto_4
    if-ge v15, v10, :cond_9

    move-object/from16 v0, p0

    iget-boolean v0, v0, Lorg/apache/tools/ant/taskdefs/Parallel;->stillRunning:Z

    move/from16 v17, v0

    if-eqz v17, :cond_9

    const/4 v7, 0x0

    :goto_5
    if-ge v7, v8, :cond_8

    aget-object v17, v12, v7

    if-eqz v17, :cond_6

    aget-object v17, v12, v7

    invoke-virtual/range {v17 .. v17}, Lorg/apache/tools/ant/taskdefs/Parallel$TaskRunnable;->isFinished()Z
    :try_end_4
    .catchall {:try_start_4 .. :try_end_4} :catchall_1

    move-result v17

    if-eqz v17, :cond_7

    :cond_6
    add-int/lit8 v14, v15, 0x1

    :try_start_5
    aget-object v17, v11, v15

    aput-object v17, v12, v7

    new-instance v13, Ljava/lang/Thread;

    aget-object v17, v12, v7

    move-object/from16 v0, v17

    invoke-direct {v13, v6, v0}, Ljava/lang/Thread;-><init>(Ljava/lang/ThreadGroup;Ljava/lang/Runnable;)V

    invoke-virtual {v13}, Ljava/lang/Thread;->start()V
    :try_end_5
    .catchall {:try_start_5 .. :try_end_5} :catchall_2

    move v15, v14

    goto :goto_4

    :cond_7
    add-int/lit8 v7, v7, 0x1

    goto :goto_5

    :cond_8
    :try_start_6
    move-object/from16 v0, p0

    iget-object v0, v0, Lorg/apache/tools/ant/taskdefs/Parallel;->semaphore:Ljava/lang/Object;

    move-object/from16 v17, v0

    invoke-virtual/range {v17 .. v17}, Ljava/lang/Object;->wait()V
    :try_end_6
    .catch Ljava/lang/InterruptedException; {:try_start_6 .. :try_end_6} :catch_0
    .catchall {:try_start_6 .. :try_end_6} :catchall_1

    goto :goto_4

    :catch_0
    move-exception v17

    goto :goto_4

    :cond_9
    :goto_6
    :try_start_7
    move-object/from16 v0, p0

    iget-boolean v0, v0, Lorg/apache/tools/ant/taskdefs/Parallel;->stillRunning:Z

    move/from16 v17, v0

    if-eqz v17, :cond_c

    const/4 v7, 0x0

    :goto_7
    if-ge v7, v8, :cond_b

    aget-object v17, v12, v7

    if-eqz v17, :cond_a

    aget-object v17, v12, v7

    invoke-virtual/range {v17 .. v17}, Lorg/apache/tools/ant/taskdefs/Parallel$TaskRunnable;->isFinished()Z
    :try_end_7
    .catchall {:try_start_7 .. :try_end_7} :catchall_1

    move-result v17

    if-nez v17, :cond_a

    :try_start_8
    move-object/from16 v0, p0

    iget-object v0, v0, Lorg/apache/tools/ant/taskdefs/Parallel;->semaphore:Ljava/lang/Object;

    move-object/from16 v17, v0

    invoke-virtual/range {v17 .. v17}, Ljava/lang/Object;->wait()V
    :try_end_8
    .catch Ljava/lang/InterruptedException; {:try_start_8 .. :try_end_8} :catch_1
    .catchall {:try_start_8 .. :try_end_8} :catchall_1

    goto :goto_6

    :catch_1
    move-exception v17

    goto :goto_6

    :cond_a
    add-int/lit8 v7, v7, 0x1

    goto :goto_7

    :cond_b
    const/16 v17, 0x0

    :try_start_9
    move/from16 v0, v17

    move-object/from16 v1, p0

    iput-boolean v0, v1, Lorg/apache/tools/ant/taskdefs/Parallel;->stillRunning:Z
    :try_end_9
    .catchall {:try_start_9 .. :try_end_9} :catchall_1

    goto :goto_6

    :catchall_1
    move-exception v17

    move v14, v15

    :goto_8
    :try_start_a
    monitor-exit v18
    :try_end_a
    .catchall {:try_start_a .. :try_end_a} :catchall_2

    throw v17

    :cond_c
    :try_start_b
    monitor-exit v18
    :try_end_b
    .catchall {:try_start_b .. :try_end_b} :catchall_1

    move-object/from16 v0, p0

    iget-boolean v0, v0, Lorg/apache/tools/ant/taskdefs/Parallel;->timedOut:Z

    move/from16 v17, v0

    if-eqz v17, :cond_d

    new-instance v17, Lorg/apache/tools/ant/BuildException;

    const-string v18, "Parallel execution timed out"

    invoke-direct/range {v17 .. v18}, Lorg/apache/tools/ant/BuildException;-><init>(Ljava/lang/String;)V

    throw v17

    :cond_d
    new-instance v17, Ljava/lang/StringBuffer;

    invoke-direct/range {v17 .. v17}, Ljava/lang/StringBuffer;-><init>()V

    move-object/from16 v0, v17

    move-object/from16 v1, p0

    iput-object v0, v1, Lorg/apache/tools/ant/taskdefs/Parallel;->exceptionMessage:Ljava/lang/StringBuffer;

    const/16 v17, 0x0

    move/from16 v0, v17

    move-object/from16 v1, p0

    iput v0, v1, Lorg/apache/tools/ant/taskdefs/Parallel;->numExceptions:I

    const/16 v17, 0x0

    move-object/from16 v0, v17

    move-object/from16 v1, p0

    iput-object v0, v1, Lorg/apache/tools/ant/taskdefs/Parallel;->firstException:Ljava/lang/Throwable;

    sget-object v17, Lorg/apache/tools/ant/Location;->UNKNOWN_LOCATION:Lorg/apache/tools/ant/Location;

    move-object/from16 v0, v17

    move-object/from16 v1, p0

    iput-object v0, v1, Lorg/apache/tools/ant/taskdefs/Parallel;->firstLocation:Lorg/apache/tools/ant/Location;

    move-object/from16 v0, p0

    invoke-direct {v0, v4}, Lorg/apache/tools/ant/taskdefs/Parallel;->processExceptions([Lorg/apache/tools/ant/taskdefs/Parallel$TaskRunnable;)V

    move-object/from16 v0, p0

    invoke-direct {v0, v11}, Lorg/apache/tools/ant/taskdefs/Parallel;->processExceptions([Lorg/apache/tools/ant/taskdefs/Parallel$TaskRunnable;)V

    move-object/from16 v0, p0

    iget v0, v0, Lorg/apache/tools/ant/taskdefs/Parallel;->numExceptions:I

    move/from16 v17, v0

    const/16 v18, 0x1

    move/from16 v0, v17

    move/from16 v1, v18

    if-ne v0, v1, :cond_f

    move-object/from16 v0, p0

    iget-object v0, v0, Lorg/apache/tools/ant/taskdefs/Parallel;->firstException:Ljava/lang/Throwable;

    move-object/from16 v17, v0

    move-object/from16 v0, v17

    instance-of v0, v0, Lorg/apache/tools/ant/BuildException;

    move/from16 v17, v0

    if-eqz v17, :cond_e

    move-object/from16 v0, p0

    iget-object v0, v0, Lorg/apache/tools/ant/taskdefs/Parallel;->firstException:Ljava/lang/Throwable;

    move-object/from16 v17, v0

    check-cast v17, Lorg/apache/tools/ant/BuildException;

    throw v17

    :cond_e
    new-instance v17, Lorg/apache/tools/ant/BuildException;

    move-object/from16 v0, p0

    iget-object v0, v0, Lorg/apache/tools/ant/taskdefs/Parallel;->firstException:Ljava/lang/Throwable;

    move-object/from16 v18, v0

    invoke-direct/range {v17 .. v18}, Lorg/apache/tools/ant/BuildException;-><init>(Ljava/lang/Throwable;)V

    throw v17

    :cond_f
    move-object/from16 v0, p0

    iget v0, v0, Lorg/apache/tools/ant/taskdefs/Parallel;->numExceptions:I

    move/from16 v17, v0

    const/16 v18, 0x1

    move/from16 v0, v17

    move/from16 v1, v18

    if-le v0, v1, :cond_10

    new-instance v17, Lorg/apache/tools/ant/BuildException;

    move-object/from16 v0, p0

    iget-object v0, v0, Lorg/apache/tools/ant/taskdefs/Parallel;->exceptionMessage:Ljava/lang/StringBuffer;

    move-object/from16 v18, v0

    invoke-virtual/range {v18 .. v18}, Ljava/lang/StringBuffer;->toString()Ljava/lang/String;

    move-result-object v18

    move-object/from16 v0, p0

    iget-object v0, v0, Lorg/apache/tools/ant/taskdefs/Parallel;->firstLocation:Lorg/apache/tools/ant/Location;

    move-object/from16 v19, v0

    invoke-direct/range {v17 .. v19}, Lorg/apache/tools/ant/BuildException;-><init>(Ljava/lang/String;Lorg/apache/tools/ant/Location;)V

    throw v17

    :cond_10
    return-void

    :catchall_2
    move-exception v17

    goto/16 :goto_8
.end method

.method private updateThreadCounts()V
    .locals 2

    iget v1, p0, Lorg/apache/tools/ant/taskdefs/Parallel;->numThreadsPerProcessor:I

    if-eqz v1, :cond_0

    invoke-direct {p0}, Lorg/apache/tools/ant/taskdefs/Parallel;->getNumProcessors()I

    move-result v0

    if-eqz v0, :cond_0

    iget v1, p0, Lorg/apache/tools/ant/taskdefs/Parallel;->numThreadsPerProcessor:I

    mul-int/2addr v1, v0

    iput v1, p0, Lorg/apache/tools/ant/taskdefs/Parallel;->numThreads:I

    :cond_0
    return-void
.end method


# virtual methods
.method public addDaemons(Lorg/apache/tools/ant/taskdefs/Parallel$TaskList;)V
    .locals 2
    .param p1    # Lorg/apache/tools/ant/taskdefs/Parallel$TaskList;

    iget-object v0, p0, Lorg/apache/tools/ant/taskdefs/Parallel;->daemonTasks:Lorg/apache/tools/ant/taskdefs/Parallel$TaskList;

    if-eqz v0, :cond_0

    new-instance v0, Lorg/apache/tools/ant/BuildException;

    const-string v1, "Only one daemon group is supported"

    invoke-direct {v0, v1}, Lorg/apache/tools/ant/BuildException;-><init>(Ljava/lang/String;)V

    throw v0

    :cond_0
    iput-object p1, p0, Lorg/apache/tools/ant/taskdefs/Parallel;->daemonTasks:Lorg/apache/tools/ant/taskdefs/Parallel$TaskList;

    return-void
.end method

.method public addTask(Lorg/apache/tools/ant/Task;)V
    .locals 1
    .param p1    # Lorg/apache/tools/ant/Task;

    iget-object v0, p0, Lorg/apache/tools/ant/taskdefs/Parallel;->nestedTasks:Ljava/util/Vector;

    invoke-virtual {v0, p1}, Ljava/util/Vector;->addElement(Ljava/lang/Object;)V

    return-void
.end method

.method public execute()V
    .locals 1
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lorg/apache/tools/ant/BuildException;
        }
    .end annotation

    invoke-direct {p0}, Lorg/apache/tools/ant/taskdefs/Parallel;->updateThreadCounts()V

    iget v0, p0, Lorg/apache/tools/ant/taskdefs/Parallel;->numThreads:I

    if-nez v0, :cond_0

    iget-object v0, p0, Lorg/apache/tools/ant/taskdefs/Parallel;->nestedTasks:Ljava/util/Vector;

    invoke-virtual {v0}, Ljava/util/Vector;->size()I

    move-result v0

    iput v0, p0, Lorg/apache/tools/ant/taskdefs/Parallel;->numThreads:I

    :cond_0
    invoke-direct {p0}, Lorg/apache/tools/ant/taskdefs/Parallel;->spinThreads()V

    return-void
.end method

.method public setFailOnAny(Z)V
    .locals 0
    .param p1    # Z

    iput-boolean p1, p0, Lorg/apache/tools/ant/taskdefs/Parallel;->failOnAny:Z

    return-void
.end method

.method public setPollInterval(I)V
    .locals 0
    .param p1    # I

    return-void
.end method

.method public setThreadCount(I)V
    .locals 0
    .param p1    # I

    iput p1, p0, Lorg/apache/tools/ant/taskdefs/Parallel;->numThreads:I

    return-void
.end method

.method public setThreadsPerProcessor(I)V
    .locals 0
    .param p1    # I

    iput p1, p0, Lorg/apache/tools/ant/taskdefs/Parallel;->numThreadsPerProcessor:I

    return-void
.end method

.method public setTimeout(J)V
    .locals 0
    .param p1    # J

    iput-wide p1, p0, Lorg/apache/tools/ant/taskdefs/Parallel;->timeout:J

    return-void
.end method
