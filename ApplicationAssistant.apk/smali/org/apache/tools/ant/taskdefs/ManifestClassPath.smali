.class public Lorg/apache/tools/ant/taskdefs/ManifestClassPath;
.super Lorg/apache/tools/ant/Task;
.source "ManifestClassPath.java"


# instance fields
.field private dir:Ljava/io/File;

.field private maxParentLevels:I

.field private name:Ljava/lang/String;

.field private path:Lorg/apache/tools/ant/types/Path;


# direct methods
.method public constructor <init>()V
    .locals 1

    invoke-direct {p0}, Lorg/apache/tools/ant/Task;-><init>()V

    const/4 v0, 0x2

    iput v0, p0, Lorg/apache/tools/ant/taskdefs/ManifestClassPath;->maxParentLevels:I

    return-void
.end method


# virtual methods
.method public addClassPath(Lorg/apache/tools/ant/types/Path;)V
    .locals 0
    .param p1    # Lorg/apache/tools/ant/types/Path;

    iput-object p1, p0, Lorg/apache/tools/ant/taskdefs/ManifestClassPath;->path:Lorg/apache/tools/ant/types/Path;

    return-void
.end method

.method public execute()V
    .locals 18

    move-object/from16 v0, p0

    iget-object v15, v0, Lorg/apache/tools/ant/taskdefs/ManifestClassPath;->name:Ljava/lang/String;

    if-nez v15, :cond_0

    new-instance v15, Lorg/apache/tools/ant/BuildException;

    const-string v16, "Missing \'property\' attribute!"

    invoke-direct/range {v15 .. v16}, Lorg/apache/tools/ant/BuildException;-><init>(Ljava/lang/String;)V

    throw v15

    :cond_0
    move-object/from16 v0, p0

    iget-object v15, v0, Lorg/apache/tools/ant/taskdefs/ManifestClassPath;->dir:Ljava/io/File;

    if-nez v15, :cond_1

    new-instance v15, Lorg/apache/tools/ant/BuildException;

    const-string v16, "Missing \'jarfile\' attribute!"

    invoke-direct/range {v15 .. v16}, Lorg/apache/tools/ant/BuildException;-><init>(Ljava/lang/String;)V

    throw v15

    :cond_1
    invoke-virtual/range {p0 .. p0}, Lorg/apache/tools/ant/taskdefs/ManifestClassPath;->getProject()Lorg/apache/tools/ant/Project;

    move-result-object v15

    move-object/from16 v0, p0

    iget-object v0, v0, Lorg/apache/tools/ant/taskdefs/ManifestClassPath;->name:Ljava/lang/String;

    move-object/from16 v16, v0

    invoke-virtual/range {v15 .. v16}, Lorg/apache/tools/ant/Project;->getProperty(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v15

    if-eqz v15, :cond_2

    new-instance v15, Lorg/apache/tools/ant/BuildException;

    new-instance v16, Ljava/lang/StringBuffer;

    invoke-direct/range {v16 .. v16}, Ljava/lang/StringBuffer;-><init>()V

    const-string v17, "Property \'"

    invoke-virtual/range {v16 .. v17}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    move-result-object v16

    move-object/from16 v0, p0

    iget-object v0, v0, Lorg/apache/tools/ant/taskdefs/ManifestClassPath;->name:Ljava/lang/String;

    move-object/from16 v17, v0

    invoke-virtual/range {v16 .. v17}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    move-result-object v16

    const-string v17, "\' already set!"

    invoke-virtual/range {v16 .. v17}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    move-result-object v16

    invoke-virtual/range {v16 .. v16}, Ljava/lang/StringBuffer;->toString()Ljava/lang/String;

    move-result-object v16

    invoke-direct/range {v15 .. v16}, Lorg/apache/tools/ant/BuildException;-><init>(Ljava/lang/String;)V

    throw v15

    :cond_2
    move-object/from16 v0, p0

    iget-object v15, v0, Lorg/apache/tools/ant/taskdefs/ManifestClassPath;->path:Lorg/apache/tools/ant/types/Path;

    if-nez v15, :cond_3

    new-instance v15, Lorg/apache/tools/ant/BuildException;

    const-string v16, "Missing nested <classpath>!"

    invoke-direct/range {v15 .. v16}, Lorg/apache/tools/ant/BuildException;-><init>(Ljava/lang/String;)V

    throw v15

    :cond_3
    invoke-static {}, Lorg/apache/tools/ant/util/FileUtils;->getFileUtils()Lorg/apache/tools/ant/util/FileUtils;

    move-result-object v8

    move-object/from16 v0, p0

    iget-object v15, v0, Lorg/apache/tools/ant/taskdefs/ManifestClassPath;->dir:Ljava/io/File;

    invoke-virtual {v15}, Ljava/io/File;->getAbsolutePath()Ljava/lang/String;

    move-result-object v15

    invoke-virtual {v8, v15}, Lorg/apache/tools/ant/util/FileUtils;->normalize(Ljava/lang/String;)Ljava/io/File;

    move-result-object v15

    move-object/from16 v0, p0

    iput-object v15, v0, Lorg/apache/tools/ant/taskdefs/ManifestClassPath;->dir:Ljava/io/File;

    move-object/from16 v0, p0

    iget-object v2, v0, Lorg/apache/tools/ant/taskdefs/ManifestClassPath;->dir:Ljava/io/File;

    move-object/from16 v0, p0

    iget v15, v0, Lorg/apache/tools/ant/taskdefs/ManifestClassPath;->maxParentLevels:I

    add-int/lit8 v15, v15, 0x1

    new-array v4, v15, [Ljava/lang/String;

    const/4 v10, 0x0

    :goto_0
    move-object/from16 v0, p0

    iget v15, v0, Lorg/apache/tools/ant/taskdefs/ManifestClassPath;->maxParentLevels:I

    add-int/lit8 v15, v15, 0x1

    if-ge v10, v15, :cond_4

    new-instance v15, Ljava/lang/StringBuffer;

    invoke-direct {v15}, Ljava/lang/StringBuffer;-><init>()V

    invoke-virtual {v2}, Ljava/io/File;->getAbsolutePath()Ljava/lang/String;

    move-result-object v16

    invoke-virtual/range {v15 .. v16}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    move-result-object v15

    sget-char v16, Ljava/io/File;->separatorChar:C

    invoke-virtual/range {v15 .. v16}, Ljava/lang/StringBuffer;->append(C)Ljava/lang/StringBuffer;

    move-result-object v15

    invoke-virtual {v15}, Ljava/lang/StringBuffer;->toString()Ljava/lang/String;

    move-result-object v15

    aput-object v15, v4, v10

    invoke-virtual {v2}, Ljava/io/File;->getParentFile()Ljava/io/File;

    move-result-object v2

    if-nez v2, :cond_5

    add-int/lit8 v15, v10, 0x1

    move-object/from16 v0, p0

    iput v15, v0, Lorg/apache/tools/ant/taskdefs/ManifestClassPath;->maxParentLevels:I

    :cond_4
    move-object/from16 v0, p0

    iget-object v15, v0, Lorg/apache/tools/ant/taskdefs/ManifestClassPath;->path:Lorg/apache/tools/ant/types/Path;

    invoke-virtual {v15}, Lorg/apache/tools/ant/types/Path;->list()[Ljava/lang/String;

    move-result-object v6

    new-instance v1, Ljava/lang/StringBuffer;

    invoke-direct {v1}, Ljava/lang/StringBuffer;-><init>()V

    new-instance v5, Ljava/lang/StringBuffer;

    invoke-direct {v5}, Ljava/lang/StringBuffer;-><init>()V

    const/4 v10, 0x0

    :goto_1
    array-length v15, v6

    if-ge v10, v15, :cond_c

    new-instance v13, Ljava/io/File;

    aget-object v15, v6, v10

    invoke-direct {v13, v15}, Ljava/io/File;-><init>(Ljava/lang/String;)V

    invoke-virtual {v13}, Ljava/io/File;->getAbsolutePath()Ljava/lang/String;

    move-result-object v15

    invoke-virtual {v8, v15}, Lorg/apache/tools/ant/util/FileUtils;->normalize(Ljava/lang/String;)Ljava/io/File;

    move-result-object v13

    invoke-virtual {v13}, Ljava/io/File;->getAbsolutePath()Ljava/lang/String;

    move-result-object v9

    const/4 v14, 0x0

    const/4 v11, 0x0

    :goto_2
    move-object/from16 v0, p0

    iget v15, v0, Lorg/apache/tools/ant/taskdefs/ManifestClassPath;->maxParentLevels:I

    if-gt v11, v15, :cond_8

    aget-object v3, v4, v11

    invoke-virtual {v9, v3}, Ljava/lang/String;->startsWith(Ljava/lang/String;)Z

    move-result v15

    if-nez v15, :cond_6

    add-int/lit8 v11, v11, 0x1

    goto :goto_2

    :cond_5
    add-int/lit8 v10, v10, 0x1

    goto :goto_0

    :cond_6
    const/4 v15, 0x0

    invoke-virtual {v5, v15}, Ljava/lang/StringBuffer;->setLength(I)V

    const/4 v12, 0x0

    :goto_3
    if-ge v12, v11, :cond_7

    const-string v15, ".."

    invoke-virtual {v5, v15}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    sget-char v15, Ljava/io/File;->separatorChar:C

    invoke-virtual {v5, v15}, Ljava/lang/StringBuffer;->append(C)Ljava/lang/StringBuffer;

    add-int/lit8 v12, v12, 0x1

    goto :goto_3

    :cond_7
    invoke-virtual {v3}, Ljava/lang/String;->length()I

    move-result v15

    invoke-virtual {v9, v15}, Ljava/lang/String;->substring(I)Ljava/lang/String;

    move-result-object v15

    invoke-virtual {v5, v15}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    invoke-virtual {v5}, Ljava/lang/StringBuffer;->toString()Ljava/lang/String;

    move-result-object v14

    :cond_8
    if-nez v14, :cond_9

    new-instance v15, Lorg/apache/tools/ant/BuildException;

    new-instance v16, Ljava/lang/StringBuffer;

    invoke-direct/range {v16 .. v16}, Ljava/lang/StringBuffer;-><init>()V

    const-string v17, "No suitable relative path from "

    invoke-virtual/range {v16 .. v17}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    move-result-object v16

    move-object/from16 v0, p0

    iget-object v0, v0, Lorg/apache/tools/ant/taskdefs/ManifestClassPath;->dir:Ljava/io/File;

    move-object/from16 v17, v0

    invoke-virtual/range {v16 .. v17}, Ljava/lang/StringBuffer;->append(Ljava/lang/Object;)Ljava/lang/StringBuffer;

    move-result-object v16

    const-string v17, " to "

    invoke-virtual/range {v16 .. v17}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    move-result-object v16

    move-object/from16 v0, v16

    invoke-virtual {v0, v9}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    move-result-object v16

    invoke-virtual/range {v16 .. v16}, Ljava/lang/StringBuffer;->toString()Ljava/lang/String;

    move-result-object v16

    invoke-direct/range {v15 .. v16}, Lorg/apache/tools/ant/BuildException;-><init>(Ljava/lang/String;)V

    throw v15

    :cond_9
    sget-char v15, Ljava/io/File;->separatorChar:C

    const/16 v16, 0x2f

    move/from16 v0, v16

    if-eq v15, v0, :cond_a

    sget-char v15, Ljava/io/File;->separatorChar:C

    const/16 v16, 0x2f

    invoke-virtual/range {v14 .. v16}, Ljava/lang/String;->replace(CC)Ljava/lang/String;

    move-result-object v14

    :cond_a
    invoke-virtual {v13}, Ljava/io/File;->isDirectory()Z

    move-result v15

    if-eqz v15, :cond_b

    new-instance v15, Ljava/lang/StringBuffer;

    invoke-direct {v15}, Ljava/lang/StringBuffer;-><init>()V

    invoke-virtual {v15, v14}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    move-result-object v15

    const/16 v16, 0x2f

    invoke-virtual/range {v15 .. v16}, Ljava/lang/StringBuffer;->append(C)Ljava/lang/StringBuffer;

    move-result-object v15

    invoke-virtual {v15}, Ljava/lang/StringBuffer;->toString()Ljava/lang/String;

    move-result-object v14

    :cond_b
    :try_start_0
    invoke-static {v14}, Lorg/apache/tools/ant/launch/Locator;->encodeURI(Ljava/lang/String;)Ljava/lang/String;
    :try_end_0
    .catch Ljava/io/UnsupportedEncodingException; {:try_start_0 .. :try_end_0} :catch_0

    move-result-object v14

    invoke-virtual {v1, v14}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    const/16 v15, 0x20

    invoke-virtual {v1, v15}, Ljava/lang/StringBuffer;->append(C)Ljava/lang/StringBuffer;

    add-int/lit8 v10, v10, 0x1

    goto/16 :goto_1

    :catch_0
    move-exception v7

    new-instance v15, Lorg/apache/tools/ant/BuildException;

    invoke-direct {v15, v7}, Lorg/apache/tools/ant/BuildException;-><init>(Ljava/lang/Throwable;)V

    throw v15

    :cond_c
    invoke-virtual/range {p0 .. p0}, Lorg/apache/tools/ant/taskdefs/ManifestClassPath;->getProject()Lorg/apache/tools/ant/Project;

    move-result-object v15

    move-object/from16 v0, p0

    iget-object v0, v0, Lorg/apache/tools/ant/taskdefs/ManifestClassPath;->name:Ljava/lang/String;

    move-object/from16 v16, v0

    invoke-virtual {v1}, Ljava/lang/StringBuffer;->toString()Ljava/lang/String;

    move-result-object v17

    invoke-virtual/range {v17 .. v17}, Ljava/lang/String;->trim()Ljava/lang/String;

    move-result-object v17

    invoke-virtual/range {v15 .. v17}, Lorg/apache/tools/ant/Project;->setNewProperty(Ljava/lang/String;Ljava/lang/String;)V

    return-void
.end method

.method public setJarFile(Ljava/io/File;)V
    .locals 4
    .param p1    # Ljava/io/File;

    invoke-virtual {p1}, Ljava/io/File;->getParentFile()Ljava/io/File;

    move-result-object v0

    invoke-virtual {v0}, Ljava/io/File;->isDirectory()Z

    move-result v1

    if-nez v1, :cond_0

    new-instance v1, Lorg/apache/tools/ant/BuildException;

    new-instance v2, Ljava/lang/StringBuffer;

    invoke-direct {v2}, Ljava/lang/StringBuffer;-><init>()V

    const-string v3, "Jar\'s directory not found: "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    move-result-object v2

    invoke-virtual {v2, v0}, Ljava/lang/StringBuffer;->append(Ljava/lang/Object;)Ljava/lang/StringBuffer;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuffer;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-direct {v1, v2}, Lorg/apache/tools/ant/BuildException;-><init>(Ljava/lang/String;)V

    throw v1

    :cond_0
    iput-object v0, p0, Lorg/apache/tools/ant/taskdefs/ManifestClassPath;->dir:Ljava/io/File;

    return-void
.end method

.method public setMaxParentLevels(I)V
    .locals 0
    .param p1    # I

    iput p1, p0, Lorg/apache/tools/ant/taskdefs/ManifestClassPath;->maxParentLevels:I

    return-void
.end method

.method public setProperty(Ljava/lang/String;)V
    .locals 0
    .param p1    # Ljava/lang/String;

    iput-object p1, p0, Lorg/apache/tools/ant/taskdefs/ManifestClassPath;->name:Ljava/lang/String;

    return-void
.end method
