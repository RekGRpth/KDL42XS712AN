.class public Lorg/apache/tools/ant/taskdefs/SignJar;
.super Lorg/apache/tools/ant/taskdefs/AbstractJarSignerTask;
.source "SignJar.java"


# static fields
.field public static final ERROR_BAD_MAP:Ljava/lang/String; = "Cannot map source file to anything sensible: "

.field public static final ERROR_MAPPER_WITHOUT_DEST:Ljava/lang/String; = "The destDir attribute is required if a mapper is set"

.field public static final ERROR_NO_ALIAS:Ljava/lang/String; = "alias attribute must be set"

.field public static final ERROR_NO_STOREPASS:Ljava/lang/String; = "storepass attribute must be set"

.field public static final ERROR_SIGNEDJAR_AND_PATHS:Ljava/lang/String; = "You cannot specify the signed JAR when using paths or filesets"

.field public static final ERROR_TODIR_AND_SIGNEDJAR:Ljava/lang/String; = "\'destdir\' and \'signedjar\' cannot both be set"

.field public static final ERROR_TOO_MANY_MAPPERS:Ljava/lang/String; = "Too many mappers"

.field private static final FILE_UTILS:Lorg/apache/tools/ant/util/FileUtils;


# instance fields
.field protected destDir:Ljava/io/File;

.field protected internalsf:Z

.field protected lazy:Z

.field private mapper:Lorg/apache/tools/ant/util/FileNameMapper;

.field private preserveLastModified:Z

.field protected sectionsonly:Z

.field protected sigfile:Ljava/lang/String;

.field protected signedjar:Ljava/io/File;

.field protected tsacert:Ljava/lang/String;

.field protected tsaurl:Ljava/lang/String;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    invoke-static {}, Lorg/apache/tools/ant/util/FileUtils;->getFileUtils()Lorg/apache/tools/ant/util/FileUtils;

    move-result-object v0

    sput-object v0, Lorg/apache/tools/ant/taskdefs/SignJar;->FILE_UTILS:Lorg/apache/tools/ant/util/FileUtils;

    return-void
.end method

.method public constructor <init>()V
    .locals 0

    invoke-direct {p0}, Lorg/apache/tools/ant/taskdefs/AbstractJarSignerTask;-><init>()V

    return-void
.end method

.method private addTimestampAuthorityCommands(Lorg/apache/tools/ant/taskdefs/ExecTask;)V
    .locals 1
    .param p1    # Lorg/apache/tools/ant/taskdefs/ExecTask;

    iget-object v0, p0, Lorg/apache/tools/ant/taskdefs/SignJar;->tsaurl:Ljava/lang/String;

    if-eqz v0, :cond_0

    const-string v0, "-tsa"

    invoke-virtual {p0, p1, v0}, Lorg/apache/tools/ant/taskdefs/SignJar;->addValue(Lorg/apache/tools/ant/taskdefs/ExecTask;Ljava/lang/String;)V

    iget-object v0, p0, Lorg/apache/tools/ant/taskdefs/SignJar;->tsaurl:Ljava/lang/String;

    invoke-virtual {p0, p1, v0}, Lorg/apache/tools/ant/taskdefs/SignJar;->addValue(Lorg/apache/tools/ant/taskdefs/ExecTask;Ljava/lang/String;)V

    :cond_0
    iget-object v0, p0, Lorg/apache/tools/ant/taskdefs/SignJar;->tsacert:Ljava/lang/String;

    if-eqz v0, :cond_1

    const-string v0, "-tsacert"

    invoke-virtual {p0, p1, v0}, Lorg/apache/tools/ant/taskdefs/SignJar;->addValue(Lorg/apache/tools/ant/taskdefs/ExecTask;Ljava/lang/String;)V

    iget-object v0, p0, Lorg/apache/tools/ant/taskdefs/SignJar;->tsacert:Ljava/lang/String;

    invoke-virtual {p0, p1, v0}, Lorg/apache/tools/ant/taskdefs/SignJar;->addValue(Lorg/apache/tools/ant/taskdefs/ExecTask;Ljava/lang/String;)V

    :cond_1
    return-void
.end method

.method private signOneJar(Ljava/io/File;Ljava/io/File;)V
    .locals 7
    .param p1    # Ljava/io/File;
    .param p2    # Ljava/io/File;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lorg/apache/tools/ant/BuildException;
        }
    .end annotation

    move-object v3, p2

    if-nez v3, :cond_0

    move-object v3, p1

    :cond_0
    invoke-virtual {p0, p1, v3}, Lorg/apache/tools/ant/taskdefs/SignJar;->isUpToDate(Ljava/io/File;Ljava/io/File;)Z

    move-result v5

    if-eqz v5, :cond_2

    :cond_1
    :goto_0
    return-void

    :cond_2
    invoke-virtual {p1}, Ljava/io/File;->lastModified()J

    move-result-wide v1

    invoke-virtual {p0}, Lorg/apache/tools/ant/taskdefs/SignJar;->createJarSigner()Lorg/apache/tools/ant/taskdefs/ExecTask;

    move-result-object v0

    invoke-virtual {p0, v0}, Lorg/apache/tools/ant/taskdefs/SignJar;->setCommonOptions(Lorg/apache/tools/ant/taskdefs/ExecTask;)V

    invoke-virtual {p0, v0}, Lorg/apache/tools/ant/taskdefs/SignJar;->bindToKeystore(Lorg/apache/tools/ant/taskdefs/ExecTask;)V

    iget-object v5, p0, Lorg/apache/tools/ant/taskdefs/SignJar;->sigfile:Ljava/lang/String;

    if-eqz v5, :cond_3

    const-string v5, "-sigfile"

    invoke-virtual {p0, v0, v5}, Lorg/apache/tools/ant/taskdefs/SignJar;->addValue(Lorg/apache/tools/ant/taskdefs/ExecTask;Ljava/lang/String;)V

    iget-object v4, p0, Lorg/apache/tools/ant/taskdefs/SignJar;->sigfile:Ljava/lang/String;

    invoke-virtual {p0, v0, v4}, Lorg/apache/tools/ant/taskdefs/SignJar;->addValue(Lorg/apache/tools/ant/taskdefs/ExecTask;Ljava/lang/String;)V

    :cond_3
    if-eqz v3, :cond_4

    invoke-virtual {p1, v3}, Ljava/io/File;->equals(Ljava/lang/Object;)Z

    move-result v5

    if-nez v5, :cond_4

    const-string v5, "-signedjar"

    invoke-virtual {p0, v0, v5}, Lorg/apache/tools/ant/taskdefs/SignJar;->addValue(Lorg/apache/tools/ant/taskdefs/ExecTask;Ljava/lang/String;)V

    invoke-virtual {v3}, Ljava/io/File;->getPath()Ljava/lang/String;

    move-result-object v5

    invoke-virtual {p0, v0, v5}, Lorg/apache/tools/ant/taskdefs/SignJar;->addValue(Lorg/apache/tools/ant/taskdefs/ExecTask;Ljava/lang/String;)V

    :cond_4
    iget-boolean v5, p0, Lorg/apache/tools/ant/taskdefs/SignJar;->internalsf:Z

    if-eqz v5, :cond_5

    const-string v5, "-internalsf"

    invoke-virtual {p0, v0, v5}, Lorg/apache/tools/ant/taskdefs/SignJar;->addValue(Lorg/apache/tools/ant/taskdefs/ExecTask;Ljava/lang/String;)V

    :cond_5
    iget-boolean v5, p0, Lorg/apache/tools/ant/taskdefs/SignJar;->sectionsonly:Z

    if-eqz v5, :cond_6

    const-string v5, "-sectionsonly"

    invoke-virtual {p0, v0, v5}, Lorg/apache/tools/ant/taskdefs/SignJar;->addValue(Lorg/apache/tools/ant/taskdefs/ExecTask;Ljava/lang/String;)V

    :cond_6
    invoke-direct {p0, v0}, Lorg/apache/tools/ant/taskdefs/SignJar;->addTimestampAuthorityCommands(Lorg/apache/tools/ant/taskdefs/ExecTask;)V

    invoke-virtual {p1}, Ljava/io/File;->getPath()Ljava/lang/String;

    move-result-object v5

    invoke-virtual {p0, v0, v5}, Lorg/apache/tools/ant/taskdefs/SignJar;->addValue(Lorg/apache/tools/ant/taskdefs/ExecTask;Ljava/lang/String;)V

    iget-object v5, p0, Lorg/apache/tools/ant/taskdefs/SignJar;->alias:Ljava/lang/String;

    invoke-virtual {p0, v0, v5}, Lorg/apache/tools/ant/taskdefs/SignJar;->addValue(Lorg/apache/tools/ant/taskdefs/ExecTask;Ljava/lang/String;)V

    new-instance v5, Ljava/lang/StringBuffer;

    invoke-direct {v5}, Ljava/lang/StringBuffer;-><init>()V

    const-string v6, "Signing JAR: "

    invoke-virtual {v5, v6}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    move-result-object v5

    invoke-virtual {p1}, Ljava/io/File;->getAbsolutePath()Ljava/lang/String;

    move-result-object v6

    invoke-virtual {v5, v6}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    move-result-object v5

    const-string v6, " to "

    invoke-virtual {v5, v6}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    move-result-object v5

    invoke-virtual {v3}, Ljava/io/File;->getAbsolutePath()Ljava/lang/String;

    move-result-object v6

    invoke-virtual {v5, v6}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    move-result-object v5

    const-string v6, " as "

    invoke-virtual {v5, v6}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    move-result-object v5

    iget-object v6, p0, Lorg/apache/tools/ant/taskdefs/SignJar;->alias:Ljava/lang/String;

    invoke-virtual {v5, v6}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/StringBuffer;->toString()Ljava/lang/String;

    move-result-object v5

    invoke-virtual {p0, v5}, Lorg/apache/tools/ant/taskdefs/SignJar;->log(Ljava/lang/String;)V

    invoke-virtual {v0}, Lorg/apache/tools/ant/taskdefs/ExecTask;->execute()V

    iget-boolean v5, p0, Lorg/apache/tools/ant/taskdefs/SignJar;->preserveLastModified:Z

    if-eqz v5, :cond_1

    invoke-virtual {v3, v1, v2}, Ljava/io/File;->setLastModified(J)Z

    goto/16 :goto_0
.end method


# virtual methods
.method public add(Lorg/apache/tools/ant/util/FileNameMapper;)V
    .locals 2
    .param p1    # Lorg/apache/tools/ant/util/FileNameMapper;

    iget-object v0, p0, Lorg/apache/tools/ant/taskdefs/SignJar;->mapper:Lorg/apache/tools/ant/util/FileNameMapper;

    if-eqz v0, :cond_0

    new-instance v0, Lorg/apache/tools/ant/BuildException;

    const-string v1, "Too many mappers"

    invoke-direct {v0, v1}, Lorg/apache/tools/ant/BuildException;-><init>(Ljava/lang/String;)V

    throw v0

    :cond_0
    iput-object p1, p0, Lorg/apache/tools/ant/taskdefs/SignJar;->mapper:Lorg/apache/tools/ant/util/FileNameMapper;

    return-void
.end method

.method public execute()V
    .locals 14
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lorg/apache/tools/ant/BuildException;
        }
    .end annotation

    const/4 v11, 0x1

    const/4 v12, 0x0

    iget-object v13, p0, Lorg/apache/tools/ant/taskdefs/SignJar;->jar:Ljava/io/File;

    if-eqz v13, :cond_0

    move v5, v11

    :goto_0
    iget-object v13, p0, Lorg/apache/tools/ant/taskdefs/SignJar;->signedjar:Ljava/io/File;

    if-eqz v13, :cond_1

    move v7, v11

    :goto_1
    iget-object v13, p0, Lorg/apache/tools/ant/taskdefs/SignJar;->destDir:Ljava/io/File;

    if-eqz v13, :cond_2

    move v4, v11

    :goto_2
    iget-object v13, p0, Lorg/apache/tools/ant/taskdefs/SignJar;->mapper:Lorg/apache/tools/ant/util/FileNameMapper;

    if-eqz v13, :cond_3

    move v6, v11

    :goto_3
    if-nez v5, :cond_4

    invoke-virtual {p0}, Lorg/apache/tools/ant/taskdefs/SignJar;->hasResources()Z

    move-result v12

    if-nez v12, :cond_4

    new-instance v11, Lorg/apache/tools/ant/BuildException;

    const-string v12, "jar must be set through jar attribute or nested filesets"

    invoke-direct {v11, v12}, Lorg/apache/tools/ant/BuildException;-><init>(Ljava/lang/String;)V

    throw v11

    :cond_0
    move v5, v12

    goto :goto_0

    :cond_1
    move v7, v12

    goto :goto_1

    :cond_2
    move v4, v12

    goto :goto_2

    :cond_3
    move v6, v12

    goto :goto_3

    :cond_4
    iget-object v12, p0, Lorg/apache/tools/ant/taskdefs/SignJar;->alias:Ljava/lang/String;

    if-nez v12, :cond_5

    new-instance v11, Lorg/apache/tools/ant/BuildException;

    const-string v12, "alias attribute must be set"

    invoke-direct {v11, v12}, Lorg/apache/tools/ant/BuildException;-><init>(Ljava/lang/String;)V

    throw v11

    :cond_5
    iget-object v12, p0, Lorg/apache/tools/ant/taskdefs/SignJar;->storepass:Ljava/lang/String;

    if-nez v12, :cond_6

    new-instance v11, Lorg/apache/tools/ant/BuildException;

    const-string v12, "storepass attribute must be set"

    invoke-direct {v11, v12}, Lorg/apache/tools/ant/BuildException;-><init>(Ljava/lang/String;)V

    throw v11

    :cond_6
    if-eqz v4, :cond_7

    if-eqz v7, :cond_7

    new-instance v11, Lorg/apache/tools/ant/BuildException;

    const-string v12, "\'destdir\' and \'signedjar\' cannot both be set"

    invoke-direct {v11, v12}, Lorg/apache/tools/ant/BuildException;-><init>(Ljava/lang/String;)V

    throw v11

    :cond_7
    invoke-virtual {p0}, Lorg/apache/tools/ant/taskdefs/SignJar;->hasResources()Z

    move-result v12

    if-eqz v12, :cond_8

    if-eqz v7, :cond_8

    new-instance v11, Lorg/apache/tools/ant/BuildException;

    const-string v12, "You cannot specify the signed JAR when using paths or filesets"

    invoke-direct {v11, v12}, Lorg/apache/tools/ant/BuildException;-><init>(Ljava/lang/String;)V

    throw v11

    :cond_8
    if-nez v4, :cond_9

    if-eqz v6, :cond_9

    new-instance v11, Lorg/apache/tools/ant/BuildException;

    const-string v12, "The destDir attribute is required if a mapper is set"

    invoke-direct {v11, v12}, Lorg/apache/tools/ant/BuildException;-><init>(Ljava/lang/String;)V

    throw v11

    :cond_9
    invoke-virtual {p0}, Lorg/apache/tools/ant/taskdefs/SignJar;->beginExecution()V

    if-eqz v5, :cond_a

    if-eqz v7, :cond_a

    :try_start_0
    iget-object v11, p0, Lorg/apache/tools/ant/taskdefs/SignJar;->jar:Ljava/io/File;

    iget-object v12, p0, Lorg/apache/tools/ant/taskdefs/SignJar;->signedjar:Ljava/io/File;

    invoke-direct {p0, v11, v12}, Lorg/apache/tools/ant/taskdefs/SignJar;->signOneJar(Ljava/io/File;Ljava/io/File;)V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    invoke-virtual {p0}, Lorg/apache/tools/ant/taskdefs/SignJar;->endExecution()V

    :goto_4
    return-void

    :cond_a
    :try_start_1
    invoke-virtual {p0}, Lorg/apache/tools/ant/taskdefs/SignJar;->createUnifiedSourcePath()Lorg/apache/tools/ant/types/Path;

    move-result-object v9

    if-eqz v6, :cond_c

    iget-object v2, p0, Lorg/apache/tools/ant/taskdefs/SignJar;->mapper:Lorg/apache/tools/ant/util/FileNameMapper;

    :goto_5
    invoke-virtual {v9}, Lorg/apache/tools/ant/types/Path;->iterator()Ljava/util/Iterator;

    move-result-object v8

    :goto_6
    invoke-interface {v8}, Ljava/util/Iterator;->hasNext()Z

    move-result v12

    if-eqz v12, :cond_f

    invoke-interface {v8}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Lorg/apache/tools/ant/types/resources/FileResource;

    if-eqz v4, :cond_d

    iget-object v10, p0, Lorg/apache/tools/ant/taskdefs/SignJar;->destDir:Ljava/io/File;

    :goto_7
    invoke-virtual {v3}, Lorg/apache/tools/ant/types/resources/FileResource;->getName()Ljava/lang/String;

    move-result-object v12

    invoke-interface {v2, v12}, Lorg/apache/tools/ant/util/FileNameMapper;->mapFileName(Ljava/lang/String;)[Ljava/lang/String;

    move-result-object v1

    if-eqz v1, :cond_b

    array-length v12, v1

    if-eq v12, v11, :cond_e

    :cond_b
    new-instance v11, Lorg/apache/tools/ant/BuildException;

    new-instance v12, Ljava/lang/StringBuffer;

    invoke-direct {v12}, Ljava/lang/StringBuffer;-><init>()V

    const-string v13, "Cannot map source file to anything sensible: "

    invoke-virtual {v12, v13}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    move-result-object v12

    invoke-virtual {v3}, Lorg/apache/tools/ant/types/resources/FileResource;->getFile()Ljava/io/File;

    move-result-object v13

    invoke-virtual {v12, v13}, Ljava/lang/StringBuffer;->append(Ljava/lang/Object;)Ljava/lang/StringBuffer;

    move-result-object v12

    invoke-virtual {v12}, Ljava/lang/StringBuffer;->toString()Ljava/lang/String;

    move-result-object v12

    invoke-direct {v11, v12}, Lorg/apache/tools/ant/BuildException;-><init>(Ljava/lang/String;)V

    throw v11
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    :catchall_0
    move-exception v11

    invoke-virtual {p0}, Lorg/apache/tools/ant/taskdefs/SignJar;->endExecution()V

    throw v11

    :cond_c
    :try_start_2
    new-instance v2, Lorg/apache/tools/ant/util/IdentityMapper;

    invoke-direct {v2}, Lorg/apache/tools/ant/util/IdentityMapper;-><init>()V

    goto :goto_5

    :cond_d
    invoke-virtual {v3}, Lorg/apache/tools/ant/types/resources/FileResource;->getBaseDir()Ljava/io/File;

    move-result-object v10

    goto :goto_7

    :cond_e
    new-instance v0, Ljava/io/File;

    const/4 v12, 0x0

    aget-object v12, v1, v12

    invoke-direct {v0, v10, v12}, Ljava/io/File;-><init>(Ljava/io/File;Ljava/lang/String;)V

    invoke-virtual {v3}, Lorg/apache/tools/ant/types/resources/FileResource;->getFile()Ljava/io/File;

    move-result-object v12

    invoke-direct {p0, v12, v0}, Lorg/apache/tools/ant/taskdefs/SignJar;->signOneJar(Ljava/io/File;Ljava/io/File;)V
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    goto :goto_6

    :cond_f
    invoke-virtual {p0}, Lorg/apache/tools/ant/taskdefs/SignJar;->endExecution()V

    goto :goto_4
.end method

.method public getMapper()Lorg/apache/tools/ant/util/FileNameMapper;
    .locals 1

    iget-object v0, p0, Lorg/apache/tools/ant/taskdefs/SignJar;->mapper:Lorg/apache/tools/ant/util/FileNameMapper;

    return-object v0
.end method

.method public getTsacert()Ljava/lang/String;
    .locals 1

    iget-object v0, p0, Lorg/apache/tools/ant/taskdefs/SignJar;->tsacert:Ljava/lang/String;

    return-object v0
.end method

.method public getTsaurl()Ljava/lang/String;
    .locals 1

    iget-object v0, p0, Lorg/apache/tools/ant/taskdefs/SignJar;->tsaurl:Ljava/lang/String;

    return-object v0
.end method

.method protected isSigned(Ljava/io/File;)Z
    .locals 3
    .param p1    # Ljava/io/File;

    :try_start_0
    iget-object v1, p0, Lorg/apache/tools/ant/taskdefs/SignJar;->alias:Ljava/lang/String;

    invoke-static {p1, v1}, Lorg/apache/tools/ant/taskdefs/condition/IsSigned;->isSigned(Ljava/io/File;Ljava/lang/String;)Z
    :try_end_0
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_0

    move-result v1

    :goto_0
    return v1

    :catch_0
    move-exception v0

    invoke-virtual {v0}, Ljava/io/IOException;->toString()Ljava/lang/String;

    move-result-object v1

    const/4 v2, 0x3

    invoke-virtual {p0, v1, v2}, Lorg/apache/tools/ant/taskdefs/SignJar;->log(Ljava/lang/String;I)V

    const/4 v1, 0x0

    goto :goto_0
.end method

.method protected isUpToDate(Ljava/io/File;Ljava/io/File;)Z
    .locals 3
    .param p1    # Ljava/io/File;
    .param p2    # Ljava/io/File;

    const/4 v1, 0x0

    if-eqz p1, :cond_0

    invoke-virtual {p1}, Ljava/io/File;->exists()Z

    move-result v2

    if-nez v2, :cond_1

    :cond_0
    :goto_0
    return v1

    :cond_1
    move-object v0, p2

    if-nez v0, :cond_2

    move-object v0, p1

    :cond_2
    invoke-virtual {p1, v0}, Ljava/io/File;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_3

    iget-boolean v2, p0, Lorg/apache/tools/ant/taskdefs/SignJar;->lazy:Z

    if-eqz v2, :cond_0

    invoke-virtual {p0, p1}, Lorg/apache/tools/ant/taskdefs/SignJar;->isSigned(Ljava/io/File;)Z

    move-result v1

    goto :goto_0

    :cond_3
    sget-object v1, Lorg/apache/tools/ant/taskdefs/SignJar;->FILE_UTILS:Lorg/apache/tools/ant/util/FileUtils;

    invoke-virtual {v1, p1, v0}, Lorg/apache/tools/ant/util/FileUtils;->isUpToDate(Ljava/io/File;Ljava/io/File;)Z

    move-result v1

    goto :goto_0
.end method

.method public setDestDir(Ljava/io/File;)V
    .locals 0
    .param p1    # Ljava/io/File;

    iput-object p1, p0, Lorg/apache/tools/ant/taskdefs/SignJar;->destDir:Ljava/io/File;

    return-void
.end method

.method public setInternalsf(Z)V
    .locals 0
    .param p1    # Z

    iput-boolean p1, p0, Lorg/apache/tools/ant/taskdefs/SignJar;->internalsf:Z

    return-void
.end method

.method public setLazy(Z)V
    .locals 0
    .param p1    # Z

    iput-boolean p1, p0, Lorg/apache/tools/ant/taskdefs/SignJar;->lazy:Z

    return-void
.end method

.method public setPreserveLastModified(Z)V
    .locals 0
    .param p1    # Z

    iput-boolean p1, p0, Lorg/apache/tools/ant/taskdefs/SignJar;->preserveLastModified:Z

    return-void
.end method

.method public setSectionsonly(Z)V
    .locals 0
    .param p1    # Z

    iput-boolean p1, p0, Lorg/apache/tools/ant/taskdefs/SignJar;->sectionsonly:Z

    return-void
.end method

.method public setSigfile(Ljava/lang/String;)V
    .locals 0
    .param p1    # Ljava/lang/String;

    iput-object p1, p0, Lorg/apache/tools/ant/taskdefs/SignJar;->sigfile:Ljava/lang/String;

    return-void
.end method

.method public setSignedjar(Ljava/io/File;)V
    .locals 0
    .param p1    # Ljava/io/File;

    iput-object p1, p0, Lorg/apache/tools/ant/taskdefs/SignJar;->signedjar:Ljava/io/File;

    return-void
.end method

.method public setTsacert(Ljava/lang/String;)V
    .locals 0
    .param p1    # Ljava/lang/String;

    iput-object p1, p0, Lorg/apache/tools/ant/taskdefs/SignJar;->tsacert:Ljava/lang/String;

    return-void
.end method

.method public setTsaurl(Ljava/lang/String;)V
    .locals 0
    .param p1    # Ljava/lang/String;

    iput-object p1, p0, Lorg/apache/tools/ant/taskdefs/SignJar;->tsaurl:Ljava/lang/String;

    return-void
.end method
