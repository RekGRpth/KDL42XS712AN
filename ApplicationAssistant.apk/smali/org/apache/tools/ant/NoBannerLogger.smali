.class public Lorg/apache/tools/ant/NoBannerLogger;
.super Lorg/apache/tools/ant/DefaultLogger;
.source "NoBannerLogger.java"


# instance fields
.field protected targetName:Ljava/lang/String;


# direct methods
.method public constructor <init>()V
    .locals 0

    invoke-direct {p0}, Lorg/apache/tools/ant/DefaultLogger;-><init>()V

    return-void
.end method


# virtual methods
.method public messageLogged(Lorg/apache/tools/ant/BuildEvent;)V
    .locals 3
    .param p1    # Lorg/apache/tools/ant/BuildEvent;

    invoke-virtual {p1}, Lorg/apache/tools/ant/BuildEvent;->getPriority()I

    move-result v0

    iget v1, p0, Lorg/apache/tools/ant/NoBannerLogger;->msgOutputLevel:I

    if-gt v0, v1, :cond_0

    invoke-virtual {p1}, Lorg/apache/tools/ant/BuildEvent;->getMessage()Ljava/lang/String;

    move-result-object v0

    if-eqz v0, :cond_0

    const-string v0, ""

    invoke-virtual {p1}, Lorg/apache/tools/ant/BuildEvent;->getMessage()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/String;->trim()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_1

    :cond_0
    :goto_0
    return-void

    :cond_1
    iget-object v0, p0, Lorg/apache/tools/ant/NoBannerLogger;->targetName:Ljava/lang/String;

    if-eqz v0, :cond_2

    iget-object v0, p0, Lorg/apache/tools/ant/NoBannerLogger;->out:Ljava/io/PrintStream;

    new-instance v1, Ljava/lang/StringBuffer;

    invoke-direct {v1}, Ljava/lang/StringBuffer;-><init>()V

    sget-object v2, Lorg/apache/tools/ant/util/StringUtils;->LINE_SEP:Ljava/lang/String;

    invoke-virtual {v1, v2}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    move-result-object v1

    iget-object v2, p0, Lorg/apache/tools/ant/NoBannerLogger;->targetName:Ljava/lang/String;

    invoke-virtual {v1, v2}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    move-result-object v1

    const-string v2, ":"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuffer;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/io/PrintStream;->println(Ljava/lang/String;)V

    const/4 v0, 0x0

    iput-object v0, p0, Lorg/apache/tools/ant/NoBannerLogger;->targetName:Ljava/lang/String;

    :cond_2
    invoke-super {p0, p1}, Lorg/apache/tools/ant/DefaultLogger;->messageLogged(Lorg/apache/tools/ant/BuildEvent;)V

    goto :goto_0
.end method

.method public targetFinished(Lorg/apache/tools/ant/BuildEvent;)V
    .locals 1
    .param p1    # Lorg/apache/tools/ant/BuildEvent;

    const/4 v0, 0x0

    iput-object v0, p0, Lorg/apache/tools/ant/NoBannerLogger;->targetName:Ljava/lang/String;

    return-void
.end method

.method public targetStarted(Lorg/apache/tools/ant/BuildEvent;)V
    .locals 1
    .param p1    # Lorg/apache/tools/ant/BuildEvent;

    invoke-virtual {p1}, Lorg/apache/tools/ant/BuildEvent;->getTarget()Lorg/apache/tools/ant/Target;

    move-result-object v0

    invoke-virtual {v0}, Lorg/apache/tools/ant/Target;->getName()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lorg/apache/tools/ant/NoBannerLogger;->targetName:Ljava/lang/String;

    return-void
.end method
