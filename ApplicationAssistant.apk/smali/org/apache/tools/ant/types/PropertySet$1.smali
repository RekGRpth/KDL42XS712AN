.class Lorg/apache/tools/ant/types/PropertySet$1;
.super Ljava/lang/Object;
.source "PropertySet.java"

# interfaces
.implements Ljava/util/Iterator;


# instance fields
.field private final this$0:Lorg/apache/tools/ant/types/PropertySet;

.field private final val$e:Ljava/util/Enumeration;


# direct methods
.method constructor <init>(Lorg/apache/tools/ant/types/PropertySet;Ljava/util/Enumeration;)V
    .locals 0

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    iput-object p1, p0, Lorg/apache/tools/ant/types/PropertySet$1;->this$0:Lorg/apache/tools/ant/types/PropertySet;

    iput-object p2, p0, Lorg/apache/tools/ant/types/PropertySet$1;->val$e:Ljava/util/Enumeration;

    return-void
.end method


# virtual methods
.method public hasNext()Z
    .locals 1

    iget-object v0, p0, Lorg/apache/tools/ant/types/PropertySet$1;->val$e:Ljava/util/Enumeration;

    invoke-interface {v0}, Ljava/util/Enumeration;->hasMoreElements()Z

    move-result v0

    return v0
.end method

.method public next()Ljava/lang/Object;
    .locals 3

    new-instance v1, Lorg/apache/tools/ant/types/resources/PropertyResource;

    iget-object v0, p0, Lorg/apache/tools/ant/types/PropertySet$1;->this$0:Lorg/apache/tools/ant/types/PropertySet;

    invoke-virtual {v0}, Lorg/apache/tools/ant/types/PropertySet;->getProject()Lorg/apache/tools/ant/Project;

    move-result-object v2

    iget-object v0, p0, Lorg/apache/tools/ant/types/PropertySet$1;->val$e:Ljava/util/Enumeration;

    invoke-interface {v0}, Ljava/util/Enumeration;->nextElement()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/String;

    invoke-direct {v1, v2, v0}, Lorg/apache/tools/ant/types/resources/PropertyResource;-><init>(Lorg/apache/tools/ant/Project;Ljava/lang/String;)V

    return-object v1
.end method

.method public remove()V
    .locals 1

    new-instance v0, Ljava/lang/UnsupportedOperationException;

    invoke-direct {v0}, Ljava/lang/UnsupportedOperationException;-><init>()V

    throw v0
.end method
