.class public Lorg/apache/tools/ant/types/ZipFileSet;
.super Lorg/apache/tools/ant/types/ArchiveFileSet;
.source "ZipFileSet.java"


# instance fields
.field private encoding:Ljava/lang/String;


# direct methods
.method public constructor <init>()V
    .locals 1

    invoke-direct {p0}, Lorg/apache/tools/ant/types/ArchiveFileSet;-><init>()V

    const/4 v0, 0x0

    iput-object v0, p0, Lorg/apache/tools/ant/types/ZipFileSet;->encoding:Ljava/lang/String;

    return-void
.end method

.method protected constructor <init>(Lorg/apache/tools/ant/types/FileSet;)V
    .locals 1
    .param p1    # Lorg/apache/tools/ant/types/FileSet;

    invoke-direct {p0, p1}, Lorg/apache/tools/ant/types/ArchiveFileSet;-><init>(Lorg/apache/tools/ant/types/FileSet;)V

    const/4 v0, 0x0

    iput-object v0, p0, Lorg/apache/tools/ant/types/ZipFileSet;->encoding:Ljava/lang/String;

    return-void
.end method

.method protected constructor <init>(Lorg/apache/tools/ant/types/ZipFileSet;)V
    .locals 1
    .param p1    # Lorg/apache/tools/ant/types/ZipFileSet;

    invoke-direct {p0, p1}, Lorg/apache/tools/ant/types/ArchiveFileSet;-><init>(Lorg/apache/tools/ant/types/ArchiveFileSet;)V

    const/4 v0, 0x0

    iput-object v0, p0, Lorg/apache/tools/ant/types/ZipFileSet;->encoding:Ljava/lang/String;

    iget-object v0, p1, Lorg/apache/tools/ant/types/ZipFileSet;->encoding:Ljava/lang/String;

    iput-object v0, p0, Lorg/apache/tools/ant/types/ZipFileSet;->encoding:Ljava/lang/String;

    return-void
.end method

.method private checkZipFileSetAttributesAllowed()V
    .locals 2

    invoke-virtual {p0}, Lorg/apache/tools/ant/types/ZipFileSet;->getProject()Lorg/apache/tools/ant/Project;

    move-result-object v0

    if-eqz v0, :cond_0

    invoke-virtual {p0}, Lorg/apache/tools/ant/types/ZipFileSet;->isReference()Z

    move-result v0

    if-eqz v0, :cond_1

    invoke-virtual {p0}, Lorg/apache/tools/ant/types/ZipFileSet;->getRefid()Lorg/apache/tools/ant/types/Reference;

    move-result-object v0

    invoke-virtual {p0}, Lorg/apache/tools/ant/types/ZipFileSet;->getProject()Lorg/apache/tools/ant/Project;

    move-result-object v1

    invoke-virtual {v0, v1}, Lorg/apache/tools/ant/types/Reference;->getReferencedObject(Lorg/apache/tools/ant/Project;)Ljava/lang/Object;

    move-result-object v0

    instance-of v0, v0, Lorg/apache/tools/ant/types/ZipFileSet;

    if-eqz v0, :cond_1

    :cond_0
    invoke-virtual {p0}, Lorg/apache/tools/ant/types/ZipFileSet;->checkAttributesAllowed()V

    :cond_1
    return-void
.end method


# virtual methods
.method public clone()Ljava/lang/Object;
    .locals 1

    invoke-virtual {p0}, Lorg/apache/tools/ant/types/ZipFileSet;->isReference()Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-virtual {p0}, Lorg/apache/tools/ant/types/ZipFileSet;->getProject()Lorg/apache/tools/ant/Project;

    move-result-object v0

    invoke-virtual {p0, v0}, Lorg/apache/tools/ant/types/ZipFileSet;->getRef(Lorg/apache/tools/ant/Project;)Lorg/apache/tools/ant/types/AbstractFileSet;

    move-result-object v0

    check-cast v0, Lorg/apache/tools/ant/types/ZipFileSet;

    invoke-virtual {v0}, Lorg/apache/tools/ant/types/ZipFileSet;->clone()Ljava/lang/Object;

    move-result-object v0

    :goto_0
    return-object v0

    :cond_0
    invoke-super {p0}, Lorg/apache/tools/ant/types/ArchiveFileSet;->clone()Ljava/lang/Object;

    move-result-object v0

    goto :goto_0
.end method

.method public getEncoding()Ljava/lang/String;
    .locals 2

    invoke-virtual {p0}, Lorg/apache/tools/ant/types/ZipFileSet;->isReference()Z

    move-result v1

    if-eqz v1, :cond_1

    invoke-virtual {p0}, Lorg/apache/tools/ant/types/ZipFileSet;->getProject()Lorg/apache/tools/ant/Project;

    move-result-object v1

    invoke-virtual {p0, v1}, Lorg/apache/tools/ant/types/ZipFileSet;->getRef(Lorg/apache/tools/ant/Project;)Lorg/apache/tools/ant/types/AbstractFileSet;

    move-result-object v0

    instance-of v1, v0, Lorg/apache/tools/ant/types/ZipFileSet;

    if-eqz v1, :cond_0

    check-cast v0, Lorg/apache/tools/ant/types/ZipFileSet;

    invoke-virtual {v0}, Lorg/apache/tools/ant/types/ZipFileSet;->getEncoding()Ljava/lang/String;

    move-result-object v1

    :goto_0
    return-object v1

    :cond_0
    const/4 v1, 0x0

    goto :goto_0

    :cond_1
    iget-object v1, p0, Lorg/apache/tools/ant/types/ZipFileSet;->encoding:Ljava/lang/String;

    goto :goto_0
.end method

.method protected getRef(Lorg/apache/tools/ant/Project;)Lorg/apache/tools/ant/types/AbstractFileSet;
    .locals 5
    .param p1    # Lorg/apache/tools/ant/Project;

    invoke-virtual {p0, p1}, Lorg/apache/tools/ant/types/ZipFileSet;->dieOnCircularReference(Lorg/apache/tools/ant/Project;)V

    invoke-virtual {p0}, Lorg/apache/tools/ant/types/ZipFileSet;->getRefid()Lorg/apache/tools/ant/types/Reference;

    move-result-object v3

    invoke-virtual {v3, p1}, Lorg/apache/tools/ant/types/Reference;->getReferencedObject(Lorg/apache/tools/ant/Project;)Ljava/lang/Object;

    move-result-object v1

    instance-of v3, v1, Lorg/apache/tools/ant/types/ZipFileSet;

    if-eqz v3, :cond_0

    check-cast v1, Lorg/apache/tools/ant/types/AbstractFileSet;

    :goto_0
    return-object v1

    :cond_0
    instance-of v3, v1, Lorg/apache/tools/ant/types/FileSet;

    if-eqz v3, :cond_1

    new-instance v2, Lorg/apache/tools/ant/types/ZipFileSet;

    check-cast v1, Lorg/apache/tools/ant/types/FileSet;

    invoke-direct {v2, v1}, Lorg/apache/tools/ant/types/ZipFileSet;-><init>(Lorg/apache/tools/ant/types/FileSet;)V

    invoke-virtual {p0, v2}, Lorg/apache/tools/ant/types/ZipFileSet;->configureFileSet(Lorg/apache/tools/ant/types/ArchiveFileSet;)V

    move-object v1, v2

    goto :goto_0

    :cond_1
    new-instance v3, Ljava/lang/StringBuffer;

    invoke-direct {v3}, Ljava/lang/StringBuffer;-><init>()V

    invoke-virtual {p0}, Lorg/apache/tools/ant/types/ZipFileSet;->getRefid()Lorg/apache/tools/ant/types/Reference;

    move-result-object v4

    invoke-virtual {v4}, Lorg/apache/tools/ant/types/Reference;->getRefId()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v3, v4}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    move-result-object v3

    const-string v4, " doesn\'t denote a zipfileset or a fileset"

    invoke-virtual {v3, v4}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuffer;->toString()Ljava/lang/String;

    move-result-object v0

    new-instance v3, Lorg/apache/tools/ant/BuildException;

    invoke-direct {v3, v0}, Lorg/apache/tools/ant/BuildException;-><init>(Ljava/lang/String;)V

    throw v3
.end method

.method protected newArchiveScanner()Lorg/apache/tools/ant/types/ArchiveScanner;
    .locals 2

    new-instance v0, Lorg/apache/tools/ant/types/ZipScanner;

    invoke-direct {v0}, Lorg/apache/tools/ant/types/ZipScanner;-><init>()V

    iget-object v1, p0, Lorg/apache/tools/ant/types/ZipFileSet;->encoding:Ljava/lang/String;

    invoke-virtual {v0, v1}, Lorg/apache/tools/ant/types/ZipScanner;->setEncoding(Ljava/lang/String;)V

    return-object v0
.end method

.method public setEncoding(Ljava/lang/String;)V
    .locals 0
    .param p1    # Ljava/lang/String;

    invoke-direct {p0}, Lorg/apache/tools/ant/types/ZipFileSet;->checkZipFileSetAttributesAllowed()V

    iput-object p1, p0, Lorg/apache/tools/ant/types/ZipFileSet;->encoding:Ljava/lang/String;

    return-void
.end method
