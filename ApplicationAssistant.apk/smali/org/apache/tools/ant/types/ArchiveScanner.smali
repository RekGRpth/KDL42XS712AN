.class public abstract Lorg/apache/tools/ant/types/ArchiveScanner;
.super Lorg/apache/tools/ant/DirectoryScanner;
.source "ArchiveScanner.java"


# instance fields
.field private dirEntries:Ljava/util/TreeMap;

.field private encoding:Ljava/lang/String;

.field private fileEntries:Ljava/util/TreeMap;

.field private lastScannedResource:Lorg/apache/tools/ant/types/Resource;

.field private matchDirEntries:Ljava/util/TreeMap;

.field private matchFileEntries:Ljava/util/TreeMap;

.field private src:Lorg/apache/tools/ant/types/Resource;

.field protected srcFile:Ljava/io/File;


# direct methods
.method public constructor <init>()V
    .locals 1

    invoke-direct {p0}, Lorg/apache/tools/ant/DirectoryScanner;-><init>()V

    new-instance v0, Ljava/util/TreeMap;

    invoke-direct {v0}, Ljava/util/TreeMap;-><init>()V

    iput-object v0, p0, Lorg/apache/tools/ant/types/ArchiveScanner;->fileEntries:Ljava/util/TreeMap;

    new-instance v0, Ljava/util/TreeMap;

    invoke-direct {v0}, Ljava/util/TreeMap;-><init>()V

    iput-object v0, p0, Lorg/apache/tools/ant/types/ArchiveScanner;->dirEntries:Ljava/util/TreeMap;

    new-instance v0, Ljava/util/TreeMap;

    invoke-direct {v0}, Ljava/util/TreeMap;-><init>()V

    iput-object v0, p0, Lorg/apache/tools/ant/types/ArchiveScanner;->matchFileEntries:Ljava/util/TreeMap;

    new-instance v0, Ljava/util/TreeMap;

    invoke-direct {v0}, Ljava/util/TreeMap;-><init>()V

    iput-object v0, p0, Lorg/apache/tools/ant/types/ArchiveScanner;->matchDirEntries:Ljava/util/TreeMap;

    return-void
.end method

.method private scanme()V
    .locals 8

    new-instance v7, Lorg/apache/tools/ant/types/Resource;

    iget-object v0, p0, Lorg/apache/tools/ant/types/ArchiveScanner;->src:Lorg/apache/tools/ant/types/Resource;

    invoke-virtual {v0}, Lorg/apache/tools/ant/types/Resource;->getName()Ljava/lang/String;

    move-result-object v0

    iget-object v1, p0, Lorg/apache/tools/ant/types/ArchiveScanner;->src:Lorg/apache/tools/ant/types/Resource;

    invoke-virtual {v1}, Lorg/apache/tools/ant/types/Resource;->isExists()Z

    move-result v1

    iget-object v2, p0, Lorg/apache/tools/ant/types/ArchiveScanner;->src:Lorg/apache/tools/ant/types/Resource;

    invoke-virtual {v2}, Lorg/apache/tools/ant/types/Resource;->getLastModified()J

    move-result-wide v2

    invoke-direct {v7, v0, v1, v2, v3}, Lorg/apache/tools/ant/types/Resource;-><init>(Ljava/lang/String;ZJ)V

    iget-object v0, p0, Lorg/apache/tools/ant/types/ArchiveScanner;->lastScannedResource:Lorg/apache/tools/ant/types/Resource;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lorg/apache/tools/ant/types/ArchiveScanner;->lastScannedResource:Lorg/apache/tools/ant/types/Resource;

    invoke-virtual {v0}, Lorg/apache/tools/ant/types/Resource;->getName()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v7}, Lorg/apache/tools/ant/types/Resource;->getName()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    iget-object v0, p0, Lorg/apache/tools/ant/types/ArchiveScanner;->lastScannedResource:Lorg/apache/tools/ant/types/Resource;

    invoke-virtual {v0}, Lorg/apache/tools/ant/types/Resource;->getLastModified()J

    move-result-wide v0

    invoke-virtual {v7}, Lorg/apache/tools/ant/types/Resource;->getLastModified()J

    move-result-wide v2

    cmp-long v0, v0, v2

    if-nez v0, :cond_0

    :goto_0
    return-void

    :cond_0
    invoke-virtual {p0}, Lorg/apache/tools/ant/types/ArchiveScanner;->init()V

    iget-object v0, p0, Lorg/apache/tools/ant/types/ArchiveScanner;->fileEntries:Ljava/util/TreeMap;

    invoke-virtual {v0}, Ljava/util/TreeMap;->clear()V

    iget-object v0, p0, Lorg/apache/tools/ant/types/ArchiveScanner;->dirEntries:Ljava/util/TreeMap;

    invoke-virtual {v0}, Ljava/util/TreeMap;->clear()V

    iget-object v0, p0, Lorg/apache/tools/ant/types/ArchiveScanner;->matchFileEntries:Ljava/util/TreeMap;

    invoke-virtual {v0}, Ljava/util/TreeMap;->clear()V

    iget-object v0, p0, Lorg/apache/tools/ant/types/ArchiveScanner;->matchDirEntries:Ljava/util/TreeMap;

    invoke-virtual {v0}, Ljava/util/TreeMap;->clear()V

    iget-object v1, p0, Lorg/apache/tools/ant/types/ArchiveScanner;->src:Lorg/apache/tools/ant/types/Resource;

    iget-object v2, p0, Lorg/apache/tools/ant/types/ArchiveScanner;->encoding:Ljava/lang/String;

    iget-object v3, p0, Lorg/apache/tools/ant/types/ArchiveScanner;->fileEntries:Ljava/util/TreeMap;

    iget-object v4, p0, Lorg/apache/tools/ant/types/ArchiveScanner;->matchFileEntries:Ljava/util/TreeMap;

    iget-object v5, p0, Lorg/apache/tools/ant/types/ArchiveScanner;->dirEntries:Ljava/util/TreeMap;

    iget-object v6, p0, Lorg/apache/tools/ant/types/ArchiveScanner;->matchDirEntries:Ljava/util/TreeMap;

    move-object v0, p0

    invoke-virtual/range {v0 .. v6}, Lorg/apache/tools/ant/types/ArchiveScanner;->fillMapsFromArchive(Lorg/apache/tools/ant/types/Resource;Ljava/lang/String;Ljava/util/Map;Ljava/util/Map;Ljava/util/Map;Ljava/util/Map;)V

    iput-object v7, p0, Lorg/apache/tools/ant/types/ArchiveScanner;->lastScannedResource:Lorg/apache/tools/ant/types/Resource;

    goto :goto_0
.end method

.method protected static final trimSeparator(Ljava/lang/String;)Ljava/lang/String;
    .locals 2
    .param p0    # Ljava/lang/String;

    const-string v0, "/"

    invoke-virtual {p0, v0}, Ljava/lang/String;->endsWith(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_0

    const/4 v0, 0x0

    invoke-virtual {p0}, Ljava/lang/String;->length()I

    move-result v1

    add-int/lit8 v1, v1, -0x1

    invoke-virtual {p0, v0, v1}, Ljava/lang/String;->substring(II)Ljava/lang/String;

    move-result-object p0

    :cond_0
    return-object p0
.end method


# virtual methods
.method protected abstract fillMapsFromArchive(Lorg/apache/tools/ant/types/Resource;Ljava/lang/String;Ljava/util/Map;Ljava/util/Map;Ljava/util/Map;Ljava/util/Map;)V
.end method

.method public getIncludedDirectories()[Ljava/lang/String;
    .locals 2

    iget-object v1, p0, Lorg/apache/tools/ant/types/ArchiveScanner;->src:Lorg/apache/tools/ant/types/Resource;

    if-nez v1, :cond_0

    invoke-super {p0}, Lorg/apache/tools/ant/DirectoryScanner;->getIncludedDirectories()[Ljava/lang/String;

    move-result-object v1

    :goto_0
    return-object v1

    :cond_0
    invoke-direct {p0}, Lorg/apache/tools/ant/types/ArchiveScanner;->scanme()V

    iget-object v1, p0, Lorg/apache/tools/ant/types/ArchiveScanner;->matchDirEntries:Ljava/util/TreeMap;

    invoke-virtual {v1}, Ljava/util/TreeMap;->keySet()Ljava/util/Set;

    move-result-object v0

    invoke-interface {v0}, Ljava/util/Set;->size()I

    move-result v1

    new-array v1, v1, [Ljava/lang/String;

    invoke-interface {v0, v1}, Ljava/util/Set;->toArray([Ljava/lang/Object;)[Ljava/lang/Object;

    move-result-object v1

    check-cast v1, [Ljava/lang/String;

    check-cast v1, [Ljava/lang/String;

    goto :goto_0
.end method

.method public getIncludedDirsCount()I
    .locals 1

    iget-object v0, p0, Lorg/apache/tools/ant/types/ArchiveScanner;->src:Lorg/apache/tools/ant/types/Resource;

    if-nez v0, :cond_0

    invoke-super {p0}, Lorg/apache/tools/ant/DirectoryScanner;->getIncludedDirsCount()I

    move-result v0

    :goto_0
    return v0

    :cond_0
    invoke-direct {p0}, Lorg/apache/tools/ant/types/ArchiveScanner;->scanme()V

    iget-object v0, p0, Lorg/apache/tools/ant/types/ArchiveScanner;->matchDirEntries:Ljava/util/TreeMap;

    invoke-virtual {v0}, Ljava/util/TreeMap;->size()I

    move-result v0

    goto :goto_0
.end method

.method public getIncludedFiles()[Ljava/lang/String;
    .locals 2

    iget-object v1, p0, Lorg/apache/tools/ant/types/ArchiveScanner;->src:Lorg/apache/tools/ant/types/Resource;

    if-nez v1, :cond_0

    invoke-super {p0}, Lorg/apache/tools/ant/DirectoryScanner;->getIncludedFiles()[Ljava/lang/String;

    move-result-object v1

    :goto_0
    return-object v1

    :cond_0
    invoke-direct {p0}, Lorg/apache/tools/ant/types/ArchiveScanner;->scanme()V

    iget-object v1, p0, Lorg/apache/tools/ant/types/ArchiveScanner;->matchFileEntries:Ljava/util/TreeMap;

    invoke-virtual {v1}, Ljava/util/TreeMap;->keySet()Ljava/util/Set;

    move-result-object v0

    invoke-interface {v0}, Ljava/util/Set;->size()I

    move-result v1

    new-array v1, v1, [Ljava/lang/String;

    invoke-interface {v0, v1}, Ljava/util/Set;->toArray([Ljava/lang/Object;)[Ljava/lang/Object;

    move-result-object v1

    check-cast v1, [Ljava/lang/String;

    check-cast v1, [Ljava/lang/String;

    goto :goto_0
.end method

.method public getIncludedFilesCount()I
    .locals 1

    iget-object v0, p0, Lorg/apache/tools/ant/types/ArchiveScanner;->src:Lorg/apache/tools/ant/types/Resource;

    if-nez v0, :cond_0

    invoke-super {p0}, Lorg/apache/tools/ant/DirectoryScanner;->getIncludedFilesCount()I

    move-result v0

    :goto_0
    return v0

    :cond_0
    invoke-direct {p0}, Lorg/apache/tools/ant/types/ArchiveScanner;->scanme()V

    iget-object v0, p0, Lorg/apache/tools/ant/types/ArchiveScanner;->matchFileEntries:Ljava/util/TreeMap;

    invoke-virtual {v0}, Ljava/util/TreeMap;->size()I

    move-result v0

    goto :goto_0
.end method

.method public getResource(Ljava/lang/String;)Lorg/apache/tools/ant/types/Resource;
    .locals 6
    .param p1    # Ljava/lang/String;

    const/4 v2, 0x1

    iget-object v0, p0, Lorg/apache/tools/ant/types/ArchiveScanner;->src:Lorg/apache/tools/ant/types/Resource;

    if-nez v0, :cond_0

    invoke-super {p0, p1}, Lorg/apache/tools/ant/DirectoryScanner;->getResource(Ljava/lang/String;)Lorg/apache/tools/ant/types/Resource;

    move-result-object v0

    :goto_0
    return-object v0

    :cond_0
    const-string v0, ""

    invoke-virtual {p1, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_1

    new-instance v0, Lorg/apache/tools/ant/types/Resource;

    const-string v1, ""

    const-wide v3, 0x7fffffffffffffffL

    move v5, v2

    invoke-direct/range {v0 .. v5}, Lorg/apache/tools/ant/types/Resource;-><init>(Ljava/lang/String;ZJZ)V

    goto :goto_0

    :cond_1
    invoke-direct {p0}, Lorg/apache/tools/ant/types/ArchiveScanner;->scanme()V

    iget-object v0, p0, Lorg/apache/tools/ant/types/ArchiveScanner;->fileEntries:Ljava/util/TreeMap;

    invoke-virtual {v0, p1}, Ljava/util/TreeMap;->containsKey(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_2

    iget-object v0, p0, Lorg/apache/tools/ant/types/ArchiveScanner;->fileEntries:Ljava/util/TreeMap;

    invoke-virtual {v0, p1}, Ljava/util/TreeMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lorg/apache/tools/ant/types/Resource;

    goto :goto_0

    :cond_2
    invoke-static {p1}, Lorg/apache/tools/ant/types/ArchiveScanner;->trimSeparator(Ljava/lang/String;)Ljava/lang/String;

    move-result-object p1

    iget-object v0, p0, Lorg/apache/tools/ant/types/ArchiveScanner;->dirEntries:Ljava/util/TreeMap;

    invoke-virtual {v0, p1}, Ljava/util/TreeMap;->containsKey(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_3

    iget-object v0, p0, Lorg/apache/tools/ant/types/ArchiveScanner;->dirEntries:Ljava/util/TreeMap;

    invoke-virtual {v0, p1}, Ljava/util/TreeMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lorg/apache/tools/ant/types/Resource;

    goto :goto_0

    :cond_3
    new-instance v0, Lorg/apache/tools/ant/types/Resource;

    invoke-direct {v0, p1}, Lorg/apache/tools/ant/types/Resource;-><init>(Ljava/lang/String;)V

    goto :goto_0
.end method

.method getResourceDirectories()Ljava/util/Iterator;
    .locals 3

    iget-object v0, p0, Lorg/apache/tools/ant/types/ArchiveScanner;->src:Lorg/apache/tools/ant/types/Resource;

    if-nez v0, :cond_0

    new-instance v0, Lorg/apache/tools/ant/types/resources/FileResourceIterator;

    invoke-virtual {p0}, Lorg/apache/tools/ant/types/ArchiveScanner;->getBasedir()Ljava/io/File;

    move-result-object v1

    invoke-virtual {p0}, Lorg/apache/tools/ant/types/ArchiveScanner;->getIncludedDirectories()[Ljava/lang/String;

    move-result-object v2

    invoke-direct {v0, v1, v2}, Lorg/apache/tools/ant/types/resources/FileResourceIterator;-><init>(Ljava/io/File;[Ljava/lang/String;)V

    :goto_0
    return-object v0

    :cond_0
    invoke-direct {p0}, Lorg/apache/tools/ant/types/ArchiveScanner;->scanme()V

    iget-object v0, p0, Lorg/apache/tools/ant/types/ArchiveScanner;->matchDirEntries:Ljava/util/TreeMap;

    invoke-virtual {v0}, Ljava/util/TreeMap;->values()Ljava/util/Collection;

    move-result-object v0

    invoke-interface {v0}, Ljava/util/Collection;->iterator()Ljava/util/Iterator;

    move-result-object v0

    goto :goto_0
.end method

.method getResourceFiles()Ljava/util/Iterator;
    .locals 3

    iget-object v0, p0, Lorg/apache/tools/ant/types/ArchiveScanner;->src:Lorg/apache/tools/ant/types/Resource;

    if-nez v0, :cond_0

    new-instance v0, Lorg/apache/tools/ant/types/resources/FileResourceIterator;

    invoke-virtual {p0}, Lorg/apache/tools/ant/types/ArchiveScanner;->getBasedir()Ljava/io/File;

    move-result-object v1

    invoke-virtual {p0}, Lorg/apache/tools/ant/types/ArchiveScanner;->getIncludedFiles()[Ljava/lang/String;

    move-result-object v2

    invoke-direct {v0, v1, v2}, Lorg/apache/tools/ant/types/resources/FileResourceIterator;-><init>(Ljava/io/File;[Ljava/lang/String;)V

    :goto_0
    return-object v0

    :cond_0
    invoke-direct {p0}, Lorg/apache/tools/ant/types/ArchiveScanner;->scanme()V

    iget-object v0, p0, Lorg/apache/tools/ant/types/ArchiveScanner;->matchFileEntries:Ljava/util/TreeMap;

    invoke-virtual {v0}, Ljava/util/TreeMap;->values()Ljava/util/Collection;

    move-result-object v0

    invoke-interface {v0}, Ljava/util/Collection;->iterator()Ljava/util/Iterator;

    move-result-object v0

    goto :goto_0
.end method

.method public init()V
    .locals 3

    const/4 v2, 0x0

    iget-object v0, p0, Lorg/apache/tools/ant/types/ArchiveScanner;->includes:[Ljava/lang/String;

    if-nez v0, :cond_0

    const/4 v0, 0x1

    new-array v0, v0, [Ljava/lang/String;

    iput-object v0, p0, Lorg/apache/tools/ant/types/ArchiveScanner;->includes:[Ljava/lang/String;

    iget-object v0, p0, Lorg/apache/tools/ant/types/ArchiveScanner;->includes:[Ljava/lang/String;

    const-string v1, "**"

    aput-object v1, v0, v2

    :cond_0
    iget-object v0, p0, Lorg/apache/tools/ant/types/ArchiveScanner;->excludes:[Ljava/lang/String;

    if-nez v0, :cond_1

    new-array v0, v2, [Ljava/lang/String;

    iput-object v0, p0, Lorg/apache/tools/ant/types/ArchiveScanner;->excludes:[Ljava/lang/String;

    :cond_1
    return-void
.end method

.method public match(Ljava/lang/String;)Z
    .locals 4
    .param p1    # Ljava/lang/String;

    const/16 v1, 0x2f

    sget-char v2, Ljava/io/File;->separatorChar:C

    invoke-virtual {p1, v1, v2}, Ljava/lang/String;->replace(CC)Ljava/lang/String;

    move-result-object v1

    const/16 v2, 0x5c

    sget-char v3, Ljava/io/File;->separatorChar:C

    invoke-virtual {v1, v2, v3}, Ljava/lang/String;->replace(CC)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p0, v0}, Lorg/apache/tools/ant/types/ArchiveScanner;->isIncluded(Ljava/lang/String;)Z

    move-result v1

    if-eqz v1, :cond_0

    invoke-virtual {p0, v0}, Lorg/apache/tools/ant/types/ArchiveScanner;->isExcluded(Ljava/lang/String;)Z

    move-result v1

    if-nez v1, :cond_0

    const/4 v1, 0x1

    :goto_0
    return v1

    :cond_0
    const/4 v1, 0x0

    goto :goto_0
.end method

.method public scan()V
    .locals 1

    iget-object v0, p0, Lorg/apache/tools/ant/types/ArchiveScanner;->src:Lorg/apache/tools/ant/types/Resource;

    if-nez v0, :cond_0

    :goto_0
    return-void

    :cond_0
    invoke-super {p0}, Lorg/apache/tools/ant/DirectoryScanner;->scan()V

    goto :goto_0
.end method

.method public setEncoding(Ljava/lang/String;)V
    .locals 0
    .param p1    # Ljava/lang/String;

    iput-object p1, p0, Lorg/apache/tools/ant/types/ArchiveScanner;->encoding:Ljava/lang/String;

    return-void
.end method

.method public setSrc(Ljava/io/File;)V
    .locals 1
    .param p1    # Ljava/io/File;

    new-instance v0, Lorg/apache/tools/ant/types/resources/FileResource;

    invoke-direct {v0, p1}, Lorg/apache/tools/ant/types/resources/FileResource;-><init>(Ljava/io/File;)V

    invoke-virtual {p0, v0}, Lorg/apache/tools/ant/types/ArchiveScanner;->setSrc(Lorg/apache/tools/ant/types/Resource;)V

    return-void
.end method

.method public setSrc(Lorg/apache/tools/ant/types/Resource;)V
    .locals 1
    .param p1    # Lorg/apache/tools/ant/types/Resource;

    iput-object p1, p0, Lorg/apache/tools/ant/types/ArchiveScanner;->src:Lorg/apache/tools/ant/types/Resource;

    instance-of v0, p1, Lorg/apache/tools/ant/types/resources/FileResource;

    if-eqz v0, :cond_0

    check-cast p1, Lorg/apache/tools/ant/types/resources/FileResource;

    invoke-virtual {p1}, Lorg/apache/tools/ant/types/resources/FileResource;->getFile()Ljava/io/File;

    move-result-object v0

    iput-object v0, p0, Lorg/apache/tools/ant/types/ArchiveScanner;->srcFile:Ljava/io/File;

    :cond_0
    return-void
.end method
