.class public Lorg/apache/tools/ant/types/Path$PathElement;
.super Ljava/lang/Object;
.source "Path.java"

# interfaces
.implements Lorg/apache/tools/ant/types/ResourceCollection;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lorg/apache/tools/ant/types/Path;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x1
    name = "PathElement"
.end annotation


# instance fields
.field private parts:[Ljava/lang/String;

.field private final this$0:Lorg/apache/tools/ant/types/Path;


# direct methods
.method public constructor <init>(Lorg/apache/tools/ant/types/Path;)V
    .locals 0

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    iput-object p1, p0, Lorg/apache/tools/ant/types/Path$PathElement;->this$0:Lorg/apache/tools/ant/types/Path;

    return-void
.end method


# virtual methods
.method public getParts()[Ljava/lang/String;
    .locals 1

    iget-object v0, p0, Lorg/apache/tools/ant/types/Path$PathElement;->parts:[Ljava/lang/String;

    return-object v0
.end method

.method public isFilesystemOnly()Z
    .locals 1

    const/4 v0, 0x1

    return v0
.end method

.method public iterator()Ljava/util/Iterator;
    .locals 3

    new-instance v0, Lorg/apache/tools/ant/types/resources/FileResourceIterator;

    const/4 v1, 0x0

    iget-object v2, p0, Lorg/apache/tools/ant/types/Path$PathElement;->parts:[Ljava/lang/String;

    invoke-direct {v0, v1, v2}, Lorg/apache/tools/ant/types/resources/FileResourceIterator;-><init>(Ljava/io/File;[Ljava/lang/String;)V

    return-object v0
.end method

.method public setLocation(Ljava/io/File;)V
    .locals 3
    .param p1    # Ljava/io/File;

    const/4 v0, 0x1

    new-array v0, v0, [Ljava/lang/String;

    const/4 v1, 0x0

    invoke-virtual {p1}, Ljava/io/File;->getAbsolutePath()Ljava/lang/String;

    move-result-object v2

    invoke-static {v2}, Lorg/apache/tools/ant/types/Path;->translateFile(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v2

    aput-object v2, v0, v1

    iput-object v0, p0, Lorg/apache/tools/ant/types/Path$PathElement;->parts:[Ljava/lang/String;

    return-void
.end method

.method public setPath(Ljava/lang/String;)V
    .locals 1
    .param p1    # Ljava/lang/String;

    iget-object v0, p0, Lorg/apache/tools/ant/types/Path$PathElement;->this$0:Lorg/apache/tools/ant/types/Path;

    invoke-virtual {v0}, Lorg/apache/tools/ant/types/Path;->getProject()Lorg/apache/tools/ant/Project;

    move-result-object v0

    invoke-static {v0, p1}, Lorg/apache/tools/ant/types/Path;->translatePath(Lorg/apache/tools/ant/Project;Ljava/lang/String;)[Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lorg/apache/tools/ant/types/Path$PathElement;->parts:[Ljava/lang/String;

    return-void
.end method

.method public size()I
    .locals 1

    iget-object v0, p0, Lorg/apache/tools/ant/types/Path$PathElement;->parts:[Ljava/lang/String;

    if-nez v0, :cond_0

    const/4 v0, 0x0

    :goto_0
    return v0

    :cond_0
    iget-object v0, p0, Lorg/apache/tools/ant/types/Path$PathElement;->parts:[Ljava/lang/String;

    array-length v0, v0

    goto :goto_0
.end method
