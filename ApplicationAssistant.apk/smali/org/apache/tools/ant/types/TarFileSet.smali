.class public Lorg/apache/tools/ant/types/TarFileSet;
.super Lorg/apache/tools/ant/types/ArchiveFileSet;
.source "TarFileSet.java"


# instance fields
.field private gid:I

.field private groupIdSet:Z

.field private groupName:Ljava/lang/String;

.field private groupNameSet:Z

.field private uid:I

.field private userIdSet:Z

.field private userName:Ljava/lang/String;

.field private userNameSet:Z


# direct methods
.method public constructor <init>()V
    .locals 1

    invoke-direct {p0}, Lorg/apache/tools/ant/types/ArchiveFileSet;-><init>()V

    const-string v0, ""

    iput-object v0, p0, Lorg/apache/tools/ant/types/TarFileSet;->userName:Ljava/lang/String;

    const-string v0, ""

    iput-object v0, p0, Lorg/apache/tools/ant/types/TarFileSet;->groupName:Ljava/lang/String;

    return-void
.end method

.method protected constructor <init>(Lorg/apache/tools/ant/types/FileSet;)V
    .locals 1
    .param p1    # Lorg/apache/tools/ant/types/FileSet;

    invoke-direct {p0, p1}, Lorg/apache/tools/ant/types/ArchiveFileSet;-><init>(Lorg/apache/tools/ant/types/FileSet;)V

    const-string v0, ""

    iput-object v0, p0, Lorg/apache/tools/ant/types/TarFileSet;->userName:Ljava/lang/String;

    const-string v0, ""

    iput-object v0, p0, Lorg/apache/tools/ant/types/TarFileSet;->groupName:Ljava/lang/String;

    return-void
.end method

.method protected constructor <init>(Lorg/apache/tools/ant/types/TarFileSet;)V
    .locals 1
    .param p1    # Lorg/apache/tools/ant/types/TarFileSet;

    invoke-direct {p0, p1}, Lorg/apache/tools/ant/types/ArchiveFileSet;-><init>(Lorg/apache/tools/ant/types/ArchiveFileSet;)V

    const-string v0, ""

    iput-object v0, p0, Lorg/apache/tools/ant/types/TarFileSet;->userName:Ljava/lang/String;

    const-string v0, ""

    iput-object v0, p0, Lorg/apache/tools/ant/types/TarFileSet;->groupName:Ljava/lang/String;

    return-void
.end method

.method private checkTarFileSetAttributesAllowed()V
    .locals 2

    invoke-virtual {p0}, Lorg/apache/tools/ant/types/TarFileSet;->getProject()Lorg/apache/tools/ant/Project;

    move-result-object v0

    if-eqz v0, :cond_0

    invoke-virtual {p0}, Lorg/apache/tools/ant/types/TarFileSet;->isReference()Z

    move-result v0

    if-eqz v0, :cond_1

    invoke-virtual {p0}, Lorg/apache/tools/ant/types/TarFileSet;->getRefid()Lorg/apache/tools/ant/types/Reference;

    move-result-object v0

    invoke-virtual {p0}, Lorg/apache/tools/ant/types/TarFileSet;->getProject()Lorg/apache/tools/ant/Project;

    move-result-object v1

    invoke-virtual {v0, v1}, Lorg/apache/tools/ant/types/Reference;->getReferencedObject(Lorg/apache/tools/ant/Project;)Ljava/lang/Object;

    move-result-object v0

    instance-of v0, v0, Lorg/apache/tools/ant/types/TarFileSet;

    if-eqz v0, :cond_1

    :cond_0
    invoke-virtual {p0}, Lorg/apache/tools/ant/types/TarFileSet;->checkAttributesAllowed()V

    :cond_1
    return-void
.end method


# virtual methods
.method public clone()Ljava/lang/Object;
    .locals 1

    invoke-virtual {p0}, Lorg/apache/tools/ant/types/TarFileSet;->isReference()Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-virtual {p0}, Lorg/apache/tools/ant/types/TarFileSet;->getProject()Lorg/apache/tools/ant/Project;

    move-result-object v0

    invoke-virtual {p0, v0}, Lorg/apache/tools/ant/types/TarFileSet;->getRef(Lorg/apache/tools/ant/Project;)Lorg/apache/tools/ant/types/AbstractFileSet;

    move-result-object v0

    check-cast v0, Lorg/apache/tools/ant/types/TarFileSet;

    invoke-virtual {v0}, Lorg/apache/tools/ant/types/TarFileSet;->clone()Ljava/lang/Object;

    move-result-object v0

    :goto_0
    return-object v0

    :cond_0
    invoke-super {p0}, Lorg/apache/tools/ant/types/ArchiveFileSet;->clone()Ljava/lang/Object;

    move-result-object v0

    goto :goto_0
.end method

.method protected configureFileSet(Lorg/apache/tools/ant/types/ArchiveFileSet;)V
    .locals 2
    .param p1    # Lorg/apache/tools/ant/types/ArchiveFileSet;

    invoke-super {p0, p1}, Lorg/apache/tools/ant/types/ArchiveFileSet;->configureFileSet(Lorg/apache/tools/ant/types/ArchiveFileSet;)V

    instance-of v1, p1, Lorg/apache/tools/ant/types/TarFileSet;

    if-eqz v1, :cond_0

    move-object v0, p1

    check-cast v0, Lorg/apache/tools/ant/types/TarFileSet;

    iget-object v1, p0, Lorg/apache/tools/ant/types/TarFileSet;->userName:Ljava/lang/String;

    invoke-virtual {v0, v1}, Lorg/apache/tools/ant/types/TarFileSet;->setUserName(Ljava/lang/String;)V

    iget-object v1, p0, Lorg/apache/tools/ant/types/TarFileSet;->groupName:Ljava/lang/String;

    invoke-virtual {v0, v1}, Lorg/apache/tools/ant/types/TarFileSet;->setGroup(Ljava/lang/String;)V

    iget v1, p0, Lorg/apache/tools/ant/types/TarFileSet;->uid:I

    invoke-virtual {v0, v1}, Lorg/apache/tools/ant/types/TarFileSet;->setUid(I)V

    iget v1, p0, Lorg/apache/tools/ant/types/TarFileSet;->gid:I

    invoke-virtual {v0, v1}, Lorg/apache/tools/ant/types/TarFileSet;->setGid(I)V

    :cond_0
    return-void
.end method

.method public getGid()I
    .locals 1

    invoke-virtual {p0}, Lorg/apache/tools/ant/types/TarFileSet;->isReference()Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-virtual {p0}, Lorg/apache/tools/ant/types/TarFileSet;->getCheckedRef()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lorg/apache/tools/ant/types/TarFileSet;

    invoke-virtual {v0}, Lorg/apache/tools/ant/types/TarFileSet;->getGid()I

    move-result v0

    :goto_0
    return v0

    :cond_0
    iget v0, p0, Lorg/apache/tools/ant/types/TarFileSet;->gid:I

    goto :goto_0
.end method

.method public getGroup()Ljava/lang/String;
    .locals 1

    invoke-virtual {p0}, Lorg/apache/tools/ant/types/TarFileSet;->isReference()Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-virtual {p0}, Lorg/apache/tools/ant/types/TarFileSet;->getCheckedRef()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lorg/apache/tools/ant/types/TarFileSet;

    invoke-virtual {v0}, Lorg/apache/tools/ant/types/TarFileSet;->getGroup()Ljava/lang/String;

    move-result-object v0

    :goto_0
    return-object v0

    :cond_0
    iget-object v0, p0, Lorg/apache/tools/ant/types/TarFileSet;->groupName:Ljava/lang/String;

    goto :goto_0
.end method

.method protected getRef(Lorg/apache/tools/ant/Project;)Lorg/apache/tools/ant/types/AbstractFileSet;
    .locals 5
    .param p1    # Lorg/apache/tools/ant/Project;

    invoke-virtual {p0, p1}, Lorg/apache/tools/ant/types/TarFileSet;->dieOnCircularReference(Lorg/apache/tools/ant/Project;)V

    invoke-virtual {p0}, Lorg/apache/tools/ant/types/TarFileSet;->getRefid()Lorg/apache/tools/ant/types/Reference;

    move-result-object v3

    invoke-virtual {v3, p1}, Lorg/apache/tools/ant/types/Reference;->getReferencedObject(Lorg/apache/tools/ant/Project;)Ljava/lang/Object;

    move-result-object v1

    instance-of v3, v1, Lorg/apache/tools/ant/types/TarFileSet;

    if-eqz v3, :cond_0

    check-cast v1, Lorg/apache/tools/ant/types/AbstractFileSet;

    :goto_0
    return-object v1

    :cond_0
    instance-of v3, v1, Lorg/apache/tools/ant/types/FileSet;

    if-eqz v3, :cond_1

    new-instance v2, Lorg/apache/tools/ant/types/TarFileSet;

    check-cast v1, Lorg/apache/tools/ant/types/FileSet;

    invoke-direct {v2, v1}, Lorg/apache/tools/ant/types/TarFileSet;-><init>(Lorg/apache/tools/ant/types/FileSet;)V

    invoke-virtual {p0, v2}, Lorg/apache/tools/ant/types/TarFileSet;->configureFileSet(Lorg/apache/tools/ant/types/ArchiveFileSet;)V

    move-object v1, v2

    goto :goto_0

    :cond_1
    new-instance v3, Ljava/lang/StringBuffer;

    invoke-direct {v3}, Ljava/lang/StringBuffer;-><init>()V

    invoke-virtual {p0}, Lorg/apache/tools/ant/types/TarFileSet;->getRefid()Lorg/apache/tools/ant/types/Reference;

    move-result-object v4

    invoke-virtual {v4}, Lorg/apache/tools/ant/types/Reference;->getRefId()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v3, v4}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    move-result-object v3

    const-string v4, " doesn\'t denote a tarfileset or a fileset"

    invoke-virtual {v3, v4}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuffer;->toString()Ljava/lang/String;

    move-result-object v0

    new-instance v3, Lorg/apache/tools/ant/BuildException;

    invoke-direct {v3, v0}, Lorg/apache/tools/ant/BuildException;-><init>(Ljava/lang/String;)V

    throw v3
.end method

.method public getUid()I
    .locals 1

    invoke-virtual {p0}, Lorg/apache/tools/ant/types/TarFileSet;->isReference()Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-virtual {p0}, Lorg/apache/tools/ant/types/TarFileSet;->getCheckedRef()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lorg/apache/tools/ant/types/TarFileSet;

    invoke-virtual {v0}, Lorg/apache/tools/ant/types/TarFileSet;->getUid()I

    move-result v0

    :goto_0
    return v0

    :cond_0
    iget v0, p0, Lorg/apache/tools/ant/types/TarFileSet;->uid:I

    goto :goto_0
.end method

.method public getUserName()Ljava/lang/String;
    .locals 1

    invoke-virtual {p0}, Lorg/apache/tools/ant/types/TarFileSet;->isReference()Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-virtual {p0}, Lorg/apache/tools/ant/types/TarFileSet;->getCheckedRef()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lorg/apache/tools/ant/types/TarFileSet;

    invoke-virtual {v0}, Lorg/apache/tools/ant/types/TarFileSet;->getUserName()Ljava/lang/String;

    move-result-object v0

    :goto_0
    return-object v0

    :cond_0
    iget-object v0, p0, Lorg/apache/tools/ant/types/TarFileSet;->userName:Ljava/lang/String;

    goto :goto_0
.end method

.method public hasGroupBeenSet()Z
    .locals 1

    iget-boolean v0, p0, Lorg/apache/tools/ant/types/TarFileSet;->groupNameSet:Z

    return v0
.end method

.method public hasGroupIdBeenSet()Z
    .locals 1

    iget-boolean v0, p0, Lorg/apache/tools/ant/types/TarFileSet;->groupIdSet:Z

    return v0
.end method

.method public hasUserIdBeenSet()Z
    .locals 1

    iget-boolean v0, p0, Lorg/apache/tools/ant/types/TarFileSet;->userIdSet:Z

    return v0
.end method

.method public hasUserNameBeenSet()Z
    .locals 1

    iget-boolean v0, p0, Lorg/apache/tools/ant/types/TarFileSet;->userNameSet:Z

    return v0
.end method

.method protected newArchiveScanner()Lorg/apache/tools/ant/types/ArchiveScanner;
    .locals 1

    new-instance v0, Lorg/apache/tools/ant/types/TarScanner;

    invoke-direct {v0}, Lorg/apache/tools/ant/types/TarScanner;-><init>()V

    return-object v0
.end method

.method public setGid(I)V
    .locals 1
    .param p1    # I

    invoke-direct {p0}, Lorg/apache/tools/ant/types/TarFileSet;->checkTarFileSetAttributesAllowed()V

    const/4 v0, 0x1

    iput-boolean v0, p0, Lorg/apache/tools/ant/types/TarFileSet;->groupIdSet:Z

    iput p1, p0, Lorg/apache/tools/ant/types/TarFileSet;->gid:I

    return-void
.end method

.method public setGroup(Ljava/lang/String;)V
    .locals 1
    .param p1    # Ljava/lang/String;

    invoke-direct {p0}, Lorg/apache/tools/ant/types/TarFileSet;->checkTarFileSetAttributesAllowed()V

    const/4 v0, 0x1

    iput-boolean v0, p0, Lorg/apache/tools/ant/types/TarFileSet;->groupNameSet:Z

    iput-object p1, p0, Lorg/apache/tools/ant/types/TarFileSet;->groupName:Ljava/lang/String;

    return-void
.end method

.method public setRefid(Lorg/apache/tools/ant/types/Reference;)V
    .locals 1
    .param p1    # Lorg/apache/tools/ant/types/Reference;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lorg/apache/tools/ant/BuildException;
        }
    .end annotation

    iget-boolean v0, p0, Lorg/apache/tools/ant/types/TarFileSet;->userNameSet:Z

    if-nez v0, :cond_0

    iget-boolean v0, p0, Lorg/apache/tools/ant/types/TarFileSet;->userIdSet:Z

    if-nez v0, :cond_0

    iget-boolean v0, p0, Lorg/apache/tools/ant/types/TarFileSet;->groupNameSet:Z

    if-nez v0, :cond_0

    iget-boolean v0, p0, Lorg/apache/tools/ant/types/TarFileSet;->groupIdSet:Z

    if-eqz v0, :cond_1

    :cond_0
    invoke-virtual {p0}, Lorg/apache/tools/ant/types/TarFileSet;->tooManyAttributes()Lorg/apache/tools/ant/BuildException;

    move-result-object v0

    throw v0

    :cond_1
    invoke-super {p0, p1}, Lorg/apache/tools/ant/types/ArchiveFileSet;->setRefid(Lorg/apache/tools/ant/types/Reference;)V

    return-void
.end method

.method public setUid(I)V
    .locals 1
    .param p1    # I

    invoke-direct {p0}, Lorg/apache/tools/ant/types/TarFileSet;->checkTarFileSetAttributesAllowed()V

    const/4 v0, 0x1

    iput-boolean v0, p0, Lorg/apache/tools/ant/types/TarFileSet;->userIdSet:Z

    iput p1, p0, Lorg/apache/tools/ant/types/TarFileSet;->uid:I

    return-void
.end method

.method public setUserName(Ljava/lang/String;)V
    .locals 1
    .param p1    # Ljava/lang/String;

    invoke-direct {p0}, Lorg/apache/tools/ant/types/TarFileSet;->checkTarFileSetAttributesAllowed()V

    const/4 v0, 0x1

    iput-boolean v0, p0, Lorg/apache/tools/ant/types/TarFileSet;->userNameSet:Z

    iput-object p1, p0, Lorg/apache/tools/ant/types/TarFileSet;->userName:Ljava/lang/String;

    return-void
.end method
