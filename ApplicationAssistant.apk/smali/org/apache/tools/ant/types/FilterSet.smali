.class public Lorg/apache/tools/ant/types/FilterSet;
.super Lorg/apache/tools/ant/types/DataType;
.source "FilterSet.java"

# interfaces
.implements Ljava/lang/Cloneable;


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lorg/apache/tools/ant/types/FilterSet$OnMissing;,
        Lorg/apache/tools/ant/types/FilterSet$FiltersFile;,
        Lorg/apache/tools/ant/types/FilterSet$Filter;
    }
.end annotation


# static fields
.field public static final DEFAULT_TOKEN_END:Ljava/lang/String; = "@"

.field public static final DEFAULT_TOKEN_START:Ljava/lang/String; = "@"

.field static class$org$apache$tools$ant$types$FilterSet:Ljava/lang/Class;


# instance fields
.field private duplicateToken:Z

.field private endOfToken:Ljava/lang/String;

.field private filterHash:Ljava/util/Hashtable;

.field private filters:Ljava/util/Vector;

.field private filtersFiles:Ljava/util/Vector;

.field private onMissingFiltersFile:Lorg/apache/tools/ant/types/FilterSet$OnMissing;

.field private passedTokens:Ljava/util/Vector;

.field private readingFiles:Z

.field private recurse:Z

.field private recurseDepth:I

.field private startOfToken:Ljava/lang/String;


# direct methods
.method public constructor <init>()V
    .locals 2

    const/4 v1, 0x0

    invoke-direct {p0}, Lorg/apache/tools/ant/types/DataType;-><init>()V

    const-string v0, "@"

    iput-object v0, p0, Lorg/apache/tools/ant/types/FilterSet;->startOfToken:Ljava/lang/String;

    const-string v0, "@"

    iput-object v0, p0, Lorg/apache/tools/ant/types/FilterSet;->endOfToken:Ljava/lang/String;

    iput-boolean v1, p0, Lorg/apache/tools/ant/types/FilterSet;->duplicateToken:Z

    const/4 v0, 0x1

    iput-boolean v0, p0, Lorg/apache/tools/ant/types/FilterSet;->recurse:Z

    const/4 v0, 0x0

    iput-object v0, p0, Lorg/apache/tools/ant/types/FilterSet;->filterHash:Ljava/util/Hashtable;

    new-instance v0, Ljava/util/Vector;

    invoke-direct {v0}, Ljava/util/Vector;-><init>()V

    iput-object v0, p0, Lorg/apache/tools/ant/types/FilterSet;->filtersFiles:Ljava/util/Vector;

    sget-object v0, Lorg/apache/tools/ant/types/FilterSet$OnMissing;->FAIL:Lorg/apache/tools/ant/types/FilterSet$OnMissing;

    iput-object v0, p0, Lorg/apache/tools/ant/types/FilterSet;->onMissingFiltersFile:Lorg/apache/tools/ant/types/FilterSet$OnMissing;

    iput-boolean v1, p0, Lorg/apache/tools/ant/types/FilterSet;->readingFiles:Z

    iput v1, p0, Lorg/apache/tools/ant/types/FilterSet;->recurseDepth:I

    new-instance v0, Ljava/util/Vector;

    invoke-direct {v0}, Ljava/util/Vector;-><init>()V

    iput-object v0, p0, Lorg/apache/tools/ant/types/FilterSet;->filters:Ljava/util/Vector;

    return-void
.end method

.method protected constructor <init>(Lorg/apache/tools/ant/types/FilterSet;)V
    .locals 2
    .param p1    # Lorg/apache/tools/ant/types/FilterSet;

    const/4 v1, 0x0

    invoke-direct {p0}, Lorg/apache/tools/ant/types/DataType;-><init>()V

    const-string v0, "@"

    iput-object v0, p0, Lorg/apache/tools/ant/types/FilterSet;->startOfToken:Ljava/lang/String;

    const-string v0, "@"

    iput-object v0, p0, Lorg/apache/tools/ant/types/FilterSet;->endOfToken:Ljava/lang/String;

    iput-boolean v1, p0, Lorg/apache/tools/ant/types/FilterSet;->duplicateToken:Z

    const/4 v0, 0x1

    iput-boolean v0, p0, Lorg/apache/tools/ant/types/FilterSet;->recurse:Z

    const/4 v0, 0x0

    iput-object v0, p0, Lorg/apache/tools/ant/types/FilterSet;->filterHash:Ljava/util/Hashtable;

    new-instance v0, Ljava/util/Vector;

    invoke-direct {v0}, Ljava/util/Vector;-><init>()V

    iput-object v0, p0, Lorg/apache/tools/ant/types/FilterSet;->filtersFiles:Ljava/util/Vector;

    sget-object v0, Lorg/apache/tools/ant/types/FilterSet$OnMissing;->FAIL:Lorg/apache/tools/ant/types/FilterSet$OnMissing;

    iput-object v0, p0, Lorg/apache/tools/ant/types/FilterSet;->onMissingFiltersFile:Lorg/apache/tools/ant/types/FilterSet$OnMissing;

    iput-boolean v1, p0, Lorg/apache/tools/ant/types/FilterSet;->readingFiles:Z

    iput v1, p0, Lorg/apache/tools/ant/types/FilterSet;->recurseDepth:I

    new-instance v0, Ljava/util/Vector;

    invoke-direct {v0}, Ljava/util/Vector;-><init>()V

    iput-object v0, p0, Lorg/apache/tools/ant/types/FilterSet;->filters:Ljava/util/Vector;

    invoke-virtual {p1}, Lorg/apache/tools/ant/types/FilterSet;->getFilters()Ljava/util/Vector;

    move-result-object v0

    invoke-virtual {v0}, Ljava/util/Vector;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/util/Vector;

    iput-object v0, p0, Lorg/apache/tools/ant/types/FilterSet;->filters:Ljava/util/Vector;

    return-void
.end method

.method static access$000(Lorg/apache/tools/ant/types/FilterSet;)Ljava/util/Vector;
    .locals 1
    .param p0    # Lorg/apache/tools/ant/types/FilterSet;

    iget-object v0, p0, Lorg/apache/tools/ant/types/FilterSet;->filtersFiles:Ljava/util/Vector;

    return-object v0
.end method

.method static class$(Ljava/lang/String;)Ljava/lang/Class;
    .locals 3
    .param p0    # Ljava/lang/String;

    :try_start_0
    invoke-static {p0}, Ljava/lang/Class;->forName(Ljava/lang/String;)Ljava/lang/Class;
    :try_end_0
    .catch Ljava/lang/ClassNotFoundException; {:try_start_0 .. :try_end_0} :catch_0

    move-result-object v1

    return-object v1

    :catch_0
    move-exception v0

    new-instance v1, Ljava/lang/NoClassDefFoundError;

    invoke-virtual {v0}, Ljava/lang/ClassNotFoundException;->getMessage()Ljava/lang/String;

    move-result-object v2

    invoke-direct {v1, v2}, Ljava/lang/NoClassDefFoundError;-><init>(Ljava/lang/String;)V

    throw v1
.end method

.method private handleMissingFile(Ljava/lang/String;)V
    .locals 2
    .param p1    # Ljava/lang/String;

    iget-object v0, p0, Lorg/apache/tools/ant/types/FilterSet;->onMissingFiltersFile:Lorg/apache/tools/ant/types/FilterSet$OnMissing;

    invoke-virtual {v0}, Lorg/apache/tools/ant/types/FilterSet$OnMissing;->getIndex()I

    move-result v0

    packed-switch v0, :pswitch_data_0

    new-instance v0, Lorg/apache/tools/ant/BuildException;

    const-string v1, "Invalid value for onMissingFiltersFile"

    invoke-direct {v0, v1}, Lorg/apache/tools/ant/BuildException;-><init>(Ljava/lang/String;)V

    throw v0

    :pswitch_0
    new-instance v0, Lorg/apache/tools/ant/BuildException;

    invoke-direct {v0, p1}, Lorg/apache/tools/ant/BuildException;-><init>(Ljava/lang/String;)V

    throw v0

    :pswitch_1
    const/4 v0, 0x1

    invoke-virtual {p0, p1, v0}, Lorg/apache/tools/ant/types/FilterSet;->log(Ljava/lang/String;I)V

    :pswitch_2
    return-void

    :pswitch_data_0
    .packed-switch 0x0
        :pswitch_0
        :pswitch_1
        :pswitch_2
    .end packed-switch
.end method

.method private declared-synchronized iReplaceTokens(Ljava/lang/String;)Ljava/lang/String;
    .locals 13
    .param p1    # Ljava/lang/String;

    const/4 v12, -0x1

    monitor-enter p0

    :try_start_0
    invoke-virtual {p0}, Lorg/apache/tools/ant/types/FilterSet;->getBeginToken()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {p0}, Lorg/apache/tools/ant/types/FilterSet;->getEndToken()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {p1, v1}, Ljava/lang/String;->indexOf(Ljava/lang/String;)I

    move-result v6

    if-le v6, v12, :cond_1

    invoke-virtual {p0}, Lorg/apache/tools/ant/types/FilterSet;->getFilterHash()Ljava/util/Hashtable;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    move-result-object v8

    :try_start_1
    new-instance v0, Ljava/lang/StringBuffer;

    invoke-direct {v0}, Ljava/lang/StringBuffer;-><init>()V

    const/4 v5, 0x0

    const/4 v7, 0x0

    const/4 v9, 0x0

    :goto_0
    if-le v6, v12, :cond_0

    invoke-virtual {v1}, Ljava/lang/String;->length()I

    move-result v10

    add-int/2addr v10, v6

    add-int/lit8 v10, v10, 0x1

    invoke-virtual {p1, v4, v10}, Ljava/lang/String;->indexOf(Ljava/lang/String;I)I

    move-result v3

    if-ne v3, v12, :cond_2

    :cond_0
    invoke-virtual {p1, v5}, Ljava/lang/String;->substring(I)Ljava/lang/String;

    move-result-object v10

    invoke-virtual {v0, v10}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    invoke-virtual {v0}, Ljava/lang/StringBuffer;->toString()Ljava/lang/String;
    :try_end_1
    .catch Ljava/lang/StringIndexOutOfBoundsException; {:try_start_1 .. :try_end_1} :catch_0
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    move-result-object p1

    :cond_1
    :goto_1
    monitor-exit p0

    return-object p1

    :cond_2
    :try_start_2
    invoke-virtual {v1}, Ljava/lang/String;->length()I

    move-result v10

    add-int/2addr v10, v6

    invoke-virtual {p1, v10, v3}, Ljava/lang/String;->substring(II)Ljava/lang/String;

    move-result-object v7

    invoke-virtual {p1, v5, v6}, Ljava/lang/String;->substring(II)Ljava/lang/String;

    move-result-object v10

    invoke-virtual {v0, v10}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    invoke-virtual {v8, v7}, Ljava/util/Hashtable;->containsKey(Ljava/lang/Object;)Z

    move-result v10

    if-eqz v10, :cond_4

    invoke-virtual {v8, v7}, Ljava/util/Hashtable;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v9

    check-cast v9, Ljava/lang/String;

    iget-boolean v10, p0, Lorg/apache/tools/ant/types/FilterSet;->recurse:Z

    if-eqz v10, :cond_3

    invoke-virtual {v9, v7}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v10

    if-nez v10, :cond_3

    invoke-direct {p0, v9, v7}, Lorg/apache/tools/ant/types/FilterSet;->replaceTokens(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v9

    :cond_3
    new-instance v10, Ljava/lang/StringBuffer;

    invoke-direct {v10}, Ljava/lang/StringBuffer;-><init>()V

    const-string v11, "Replacing: "

    invoke-virtual {v10, v11}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    move-result-object v10

    invoke-virtual {v10, v1}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    move-result-object v10

    invoke-virtual {v10, v7}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    move-result-object v10

    invoke-virtual {v10, v4}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    move-result-object v10

    const-string v11, " -> "

    invoke-virtual {v10, v11}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    move-result-object v10

    invoke-virtual {v10, v9}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    move-result-object v10

    invoke-virtual {v10}, Ljava/lang/StringBuffer;->toString()Ljava/lang/String;

    move-result-object v10

    const/4 v11, 0x3

    invoke-virtual {p0, v10, v11}, Lorg/apache/tools/ant/types/FilterSet;->log(Ljava/lang/String;I)V

    invoke-virtual {v0, v9}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    invoke-virtual {v1}, Ljava/lang/String;->length()I

    move-result v10

    add-int/2addr v10, v6

    invoke-virtual {v7}, Ljava/lang/String;->length()I

    move-result v11

    add-int/2addr v10, v11

    invoke-virtual {v4}, Ljava/lang/String;->length()I

    move-result v11

    add-int v5, v10, v11

    :goto_2
    invoke-virtual {p1, v1, v5}, Ljava/lang/String;->indexOf(Ljava/lang/String;I)I

    move-result v6

    goto/16 :goto_0

    :cond_4
    invoke-virtual {v0, v1}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    invoke-virtual {v1}, Ljava/lang/String;->length()I
    :try_end_2
    .catch Ljava/lang/StringIndexOutOfBoundsException; {:try_start_2 .. :try_end_2} :catch_0
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    move-result v10

    add-int v5, v6, v10

    goto :goto_2

    :catch_0
    move-exception v2

    goto :goto_1

    :catchall_0
    move-exception v10

    monitor-exit p0

    throw v10
.end method

.method private declared-synchronized replaceTokens(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;
    .locals 6
    .param p1    # Ljava/lang/String;
    .param p2    # Ljava/lang/String;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lorg/apache/tools/ant/BuildException;
        }
    .end annotation

    const/4 v5, 0x1

    monitor-enter p0

    :try_start_0
    invoke-virtual {p0}, Lorg/apache/tools/ant/types/FilterSet;->getBeginToken()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p0}, Lorg/apache/tools/ant/types/FilterSet;->getEndToken()Ljava/lang/String;

    move-result-object v1

    iget v3, p0, Lorg/apache/tools/ant/types/FilterSet;->recurseDepth:I

    if-nez v3, :cond_0

    new-instance v3, Ljava/util/Vector;

    invoke-direct {v3}, Ljava/util/Vector;-><init>()V

    iput-object v3, p0, Lorg/apache/tools/ant/types/FilterSet;->passedTokens:Ljava/util/Vector;

    :cond_0
    iget v3, p0, Lorg/apache/tools/ant/types/FilterSet;->recurseDepth:I

    add-int/lit8 v3, v3, 0x1

    iput v3, p0, Lorg/apache/tools/ant/types/FilterSet;->recurseDepth:I

    iget-object v3, p0, Lorg/apache/tools/ant/types/FilterSet;->passedTokens:Ljava/util/Vector;

    invoke-virtual {v3, p2}, Ljava/util/Vector;->contains(Ljava/lang/Object;)Z

    move-result v3

    if-eqz v3, :cond_1

    iget-boolean v3, p0, Lorg/apache/tools/ant/types/FilterSet;->duplicateToken:Z

    if-nez v3, :cond_1

    const/4 v3, 0x1

    iput-boolean v3, p0, Lorg/apache/tools/ant/types/FilterSet;->duplicateToken:Z

    sget-object v3, Ljava/lang/System;->out:Ljava/io/PrintStream;

    new-instance v4, Ljava/lang/StringBuffer;

    invoke-direct {v4}, Ljava/lang/StringBuffer;-><init>()V

    const-string v5, "Infinite loop in tokens. Currently known tokens : "

    invoke-virtual {v4, v5}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    move-result-object v4

    iget-object v5, p0, Lorg/apache/tools/ant/types/FilterSet;->passedTokens:Ljava/util/Vector;

    invoke-virtual {v5}, Ljava/util/Vector;->toString()Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v4, v5}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    move-result-object v4

    const-string v5, "\nProblem token : "

    invoke-virtual {v4, v5}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    move-result-object v4

    invoke-virtual {v4, v0}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    move-result-object v4

    invoke-virtual {v4, p2}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    move-result-object v4

    invoke-virtual {v4, v1}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    move-result-object v4

    const-string v5, " called from "

    invoke-virtual {v4, v5}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    move-result-object v4

    invoke-virtual {v4, v0}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    move-result-object v4

    iget-object v5, p0, Lorg/apache/tools/ant/types/FilterSet;->passedTokens:Ljava/util/Vector;

    invoke-virtual {v5}, Ljava/util/Vector;->lastElement()Ljava/lang/Object;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v4, v5}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    move-result-object v4

    invoke-virtual {v4, v1}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuffer;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v3, v4}, Ljava/io/PrintStream;->println(Ljava/lang/String;)V

    iget v3, p0, Lorg/apache/tools/ant/types/FilterSet;->recurseDepth:I

    add-int/lit8 v3, v3, -0x1

    iput v3, p0, Lorg/apache/tools/ant/types/FilterSet;->recurseDepth:I
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    :goto_0
    monitor-exit p0

    return-object p2

    :cond_1
    :try_start_1
    iget-object v3, p0, Lorg/apache/tools/ant/types/FilterSet;->passedTokens:Ljava/util/Vector;

    invoke-virtual {v3, p2}, Ljava/util/Vector;->addElement(Ljava/lang/Object;)V

    invoke-direct {p0, p1}, Lorg/apache/tools/ant/types/FilterSet;->iReplaceTokens(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v2, v0}, Ljava/lang/String;->indexOf(Ljava/lang/String;)I

    move-result v3

    const/4 v4, -0x1

    if-ne v3, v4, :cond_3

    iget-boolean v3, p0, Lorg/apache/tools/ant/types/FilterSet;->duplicateToken:Z

    if-nez v3, :cond_3

    iget v3, p0, Lorg/apache/tools/ant/types/FilterSet;->recurseDepth:I

    if-ne v3, v5, :cond_3

    const/4 v3, 0x0

    iput-object v3, p0, Lorg/apache/tools/ant/types/FilterSet;->passedTokens:Ljava/util/Vector;

    :cond_2
    :goto_1
    iget v3, p0, Lorg/apache/tools/ant/types/FilterSet;->recurseDepth:I

    add-int/lit8 v3, v3, -0x1

    iput v3, p0, Lorg/apache/tools/ant/types/FilterSet;->recurseDepth:I

    move-object p2, v2

    goto :goto_0

    :cond_3
    iget-boolean v3, p0, Lorg/apache/tools/ant/types/FilterSet;->duplicateToken:Z

    if-eqz v3, :cond_2

    iget-object v3, p0, Lorg/apache/tools/ant/types/FilterSet;->passedTokens:Ljava/util/Vector;

    invoke-virtual {v3}, Ljava/util/Vector;->size()I

    move-result v3

    if-lez v3, :cond_2

    iget-object v3, p0, Lorg/apache/tools/ant/types/FilterSet;->passedTokens:Ljava/util/Vector;

    iget-object v4, p0, Lorg/apache/tools/ant/types/FilterSet;->passedTokens:Ljava/util/Vector;

    invoke-virtual {v4}, Ljava/util/Vector;->size()I

    move-result v4

    add-int/lit8 v4, v4, -0x1

    invoke-virtual {v3, v4}, Ljava/util/Vector;->remove(I)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Ljava/lang/String;

    iget-object v3, p0, Lorg/apache/tools/ant/types/FilterSet;->passedTokens:Ljava/util/Vector;

    invoke-virtual {v3}, Ljava/util/Vector;->size()I

    move-result v3

    if-nez v3, :cond_2

    new-instance v3, Ljava/lang/StringBuffer;

    invoke-direct {v3}, Ljava/lang/StringBuffer;-><init>()V

    invoke-virtual {v3, v0}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    move-result-object v3

    invoke-virtual {v3, v2}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    move-result-object v3

    invoke-virtual {v3, v1}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuffer;->toString()Ljava/lang/String;

    move-result-object v2

    const/4 v3, 0x0

    iput-boolean v3, p0, Lorg/apache/tools/ant/types/FilterSet;->duplicateToken:Z
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    goto :goto_1

    :catchall_0
    move-exception v3

    monitor-exit p0

    throw v3
.end method


# virtual methods
.method public declared-synchronized addConfiguredFilterSet(Lorg/apache/tools/ant/types/FilterSet;)V
    .locals 2
    .param p1    # Lorg/apache/tools/ant/types/FilterSet;

    monitor-enter p0

    :try_start_0
    invoke-virtual {p0}, Lorg/apache/tools/ant/types/FilterSet;->isReference()Z

    move-result v1

    if-eqz v1, :cond_0

    invoke-virtual {p0}, Lorg/apache/tools/ant/types/FilterSet;->noChildrenAllowed()Lorg/apache/tools/ant/BuildException;

    move-result-object v1

    throw v1
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    :catchall_0
    move-exception v1

    monitor-exit p0

    throw v1

    :cond_0
    :try_start_1
    invoke-virtual {p1}, Lorg/apache/tools/ant/types/FilterSet;->getFilters()Ljava/util/Vector;

    move-result-object v1

    invoke-virtual {v1}, Ljava/util/Vector;->elements()Ljava/util/Enumeration;

    move-result-object v0

    :goto_0
    invoke-interface {v0}, Ljava/util/Enumeration;->hasMoreElements()Z

    move-result v1

    if-eqz v1, :cond_1

    invoke-interface {v0}, Ljava/util/Enumeration;->nextElement()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lorg/apache/tools/ant/types/FilterSet$Filter;

    invoke-virtual {p0, v1}, Lorg/apache/tools/ant/types/FilterSet;->addFilter(Lorg/apache/tools/ant/types/FilterSet$Filter;)V
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    goto :goto_0

    :cond_1
    monitor-exit p0

    return-void
.end method

.method public declared-synchronized addFilter(Ljava/lang/String;Ljava/lang/String;)V
    .locals 1
    .param p1    # Ljava/lang/String;
    .param p2    # Ljava/lang/String;

    monitor-enter p0

    :try_start_0
    invoke-virtual {p0}, Lorg/apache/tools/ant/types/FilterSet;->isReference()Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-virtual {p0}, Lorg/apache/tools/ant/types/FilterSet;->noChildrenAllowed()Lorg/apache/tools/ant/BuildException;

    move-result-object v0

    throw v0
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0

    :cond_0
    :try_start_1
    new-instance v0, Lorg/apache/tools/ant/types/FilterSet$Filter;

    invoke-direct {v0, p1, p2}, Lorg/apache/tools/ant/types/FilterSet$Filter;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    invoke-virtual {p0, v0}, Lorg/apache/tools/ant/types/FilterSet;->addFilter(Lorg/apache/tools/ant/types/FilterSet$Filter;)V
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    monitor-exit p0

    return-void
.end method

.method public declared-synchronized addFilter(Lorg/apache/tools/ant/types/FilterSet$Filter;)V
    .locals 1
    .param p1    # Lorg/apache/tools/ant/types/FilterSet$Filter;

    monitor-enter p0

    :try_start_0
    invoke-virtual {p0}, Lorg/apache/tools/ant/types/FilterSet;->isReference()Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-virtual {p0}, Lorg/apache/tools/ant/types/FilterSet;->noChildrenAllowed()Lorg/apache/tools/ant/BuildException;

    move-result-object v0

    throw v0
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0

    :cond_0
    :try_start_1
    iget-object v0, p0, Lorg/apache/tools/ant/types/FilterSet;->filters:Ljava/util/Vector;

    invoke-virtual {v0, p1}, Ljava/util/Vector;->addElement(Ljava/lang/Object;)V

    const/4 v0, 0x0

    iput-object v0, p0, Lorg/apache/tools/ant/types/FilterSet;->filterHash:Ljava/util/Hashtable;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    monitor-exit p0

    return-void
.end method

.method public declared-synchronized clone()Ljava/lang/Object;
    .locals 3
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lorg/apache/tools/ant/BuildException;
        }
    .end annotation

    monitor-enter p0

    :try_start_0
    invoke-virtual {p0}, Lorg/apache/tools/ant/types/FilterSet;->isReference()Z

    move-result v2

    if-eqz v2, :cond_0

    invoke-virtual {p0}, Lorg/apache/tools/ant/types/FilterSet;->getRef()Lorg/apache/tools/ant/types/FilterSet;

    move-result-object v2

    invoke-virtual {v2}, Lorg/apache/tools/ant/types/FilterSet;->clone()Ljava/lang/Object;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    move-result-object v1

    :goto_0
    monitor-exit p0

    return-object v1

    :cond_0
    :try_start_1
    invoke-super {p0}, Lorg/apache/tools/ant/types/DataType;->clone()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lorg/apache/tools/ant/types/FilterSet;

    invoke-virtual {p0}, Lorg/apache/tools/ant/types/FilterSet;->getFilters()Ljava/util/Vector;

    move-result-object v2

    invoke-virtual {v2}, Ljava/util/Vector;->clone()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Ljava/util/Vector;

    iput-object v2, v1, Lorg/apache/tools/ant/types/FilterSet;->filters:Ljava/util/Vector;

    invoke-virtual {p0}, Lorg/apache/tools/ant/types/FilterSet;->getProject()Lorg/apache/tools/ant/Project;

    move-result-object v2

    invoke-virtual {v1, v2}, Lorg/apache/tools/ant/types/FilterSet;->setProject(Lorg/apache/tools/ant/Project;)V
    :try_end_1
    .catch Ljava/lang/CloneNotSupportedException; {:try_start_1 .. :try_end_1} :catch_0
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    goto :goto_0

    :catch_0
    move-exception v0

    :try_start_2
    new-instance v2, Lorg/apache/tools/ant/BuildException;

    invoke-direct {v2, v0}, Lorg/apache/tools/ant/BuildException;-><init>(Ljava/lang/Throwable;)V

    throw v2
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    :catchall_0
    move-exception v2

    monitor-exit p0

    throw v2
.end method

.method public createFiltersfile()Lorg/apache/tools/ant/types/FilterSet$FiltersFile;
    .locals 1

    invoke-virtual {p0}, Lorg/apache/tools/ant/types/FilterSet;->isReference()Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-virtual {p0}, Lorg/apache/tools/ant/types/FilterSet;->noChildrenAllowed()Lorg/apache/tools/ant/BuildException;

    move-result-object v0

    throw v0

    :cond_0
    new-instance v0, Lorg/apache/tools/ant/types/FilterSet$FiltersFile;

    invoke-direct {v0, p0}, Lorg/apache/tools/ant/types/FilterSet$FiltersFile;-><init>(Lorg/apache/tools/ant/types/FilterSet;)V

    return-object v0
.end method

.method public getBeginToken()Ljava/lang/String;
    .locals 1

    invoke-virtual {p0}, Lorg/apache/tools/ant/types/FilterSet;->isReference()Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-virtual {p0}, Lorg/apache/tools/ant/types/FilterSet;->getRef()Lorg/apache/tools/ant/types/FilterSet;

    move-result-object v0

    invoke-virtual {v0}, Lorg/apache/tools/ant/types/FilterSet;->getBeginToken()Ljava/lang/String;

    move-result-object v0

    :goto_0
    return-object v0

    :cond_0
    iget-object v0, p0, Lorg/apache/tools/ant/types/FilterSet;->startOfToken:Ljava/lang/String;

    goto :goto_0
.end method

.method public getEndToken()Ljava/lang/String;
    .locals 1

    invoke-virtual {p0}, Lorg/apache/tools/ant/types/FilterSet;->isReference()Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-virtual {p0}, Lorg/apache/tools/ant/types/FilterSet;->getRef()Lorg/apache/tools/ant/types/FilterSet;

    move-result-object v0

    invoke-virtual {v0}, Lorg/apache/tools/ant/types/FilterSet;->getEndToken()Ljava/lang/String;

    move-result-object v0

    :goto_0
    return-object v0

    :cond_0
    iget-object v0, p0, Lorg/apache/tools/ant/types/FilterSet;->endOfToken:Ljava/lang/String;

    goto :goto_0
.end method

.method public declared-synchronized getFilterHash()Ljava/util/Hashtable;
    .locals 5

    monitor-enter p0

    :try_start_0
    iget-object v2, p0, Lorg/apache/tools/ant/types/FilterSet;->filterHash:Ljava/util/Hashtable;

    if-nez v2, :cond_0

    new-instance v2, Ljava/util/Hashtable;

    invoke-virtual {p0}, Lorg/apache/tools/ant/types/FilterSet;->getFilters()Ljava/util/Vector;

    move-result-object v3

    invoke-virtual {v3}, Ljava/util/Vector;->size()I

    move-result v3

    invoke-direct {v2, v3}, Ljava/util/Hashtable;-><init>(I)V

    iput-object v2, p0, Lorg/apache/tools/ant/types/FilterSet;->filterHash:Ljava/util/Hashtable;

    invoke-virtual {p0}, Lorg/apache/tools/ant/types/FilterSet;->getFilters()Ljava/util/Vector;

    move-result-object v2

    invoke-virtual {v2}, Ljava/util/Vector;->elements()Ljava/util/Enumeration;

    move-result-object v0

    :goto_0
    invoke-interface {v0}, Ljava/util/Enumeration;->hasMoreElements()Z

    move-result v2

    if-eqz v2, :cond_0

    invoke-interface {v0}, Ljava/util/Enumeration;->nextElement()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lorg/apache/tools/ant/types/FilterSet$Filter;

    iget-object v2, p0, Lorg/apache/tools/ant/types/FilterSet;->filterHash:Ljava/util/Hashtable;

    invoke-virtual {v1}, Lorg/apache/tools/ant/types/FilterSet$Filter;->getToken()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v1}, Lorg/apache/tools/ant/types/FilterSet$Filter;->getValue()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v2, v3, v4}, Ljava/util/Hashtable;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    goto :goto_0

    :catchall_0
    move-exception v2

    monitor-exit p0

    throw v2

    :cond_0
    :try_start_1
    iget-object v2, p0, Lorg/apache/tools/ant/types/FilterSet;->filterHash:Ljava/util/Hashtable;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    monitor-exit p0

    return-object v2
.end method

.method protected declared-synchronized getFilters()Ljava/util/Vector;
    .locals 3

    monitor-enter p0

    :try_start_0
    invoke-virtual {p0}, Lorg/apache/tools/ant/types/FilterSet;->isReference()Z

    move-result v2

    if-eqz v2, :cond_0

    invoke-virtual {p0}, Lorg/apache/tools/ant/types/FilterSet;->getRef()Lorg/apache/tools/ant/types/FilterSet;

    move-result-object v2

    invoke-virtual {v2}, Lorg/apache/tools/ant/types/FilterSet;->getFilters()Ljava/util/Vector;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    move-result-object v2

    :goto_0
    monitor-exit p0

    return-object v2

    :cond_0
    :try_start_1
    iget-boolean v2, p0, Lorg/apache/tools/ant/types/FilterSet;->readingFiles:Z

    if-nez v2, :cond_2

    const/4 v2, 0x1

    iput-boolean v2, p0, Lorg/apache/tools/ant/types/FilterSet;->readingFiles:Z

    const/4 v0, 0x0

    iget-object v2, p0, Lorg/apache/tools/ant/types/FilterSet;->filtersFiles:Ljava/util/Vector;

    invoke-virtual {v2}, Ljava/util/Vector;->size()I

    move-result v1

    :goto_1
    if-ge v0, v1, :cond_1

    iget-object v2, p0, Lorg/apache/tools/ant/types/FilterSet;->filtersFiles:Ljava/util/Vector;

    invoke-virtual {v2, v0}, Ljava/util/Vector;->get(I)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Ljava/io/File;

    invoke-virtual {p0, v2}, Lorg/apache/tools/ant/types/FilterSet;->readFiltersFromFile(Ljava/io/File;)V

    add-int/lit8 v0, v0, 0x1

    goto :goto_1

    :cond_1
    iget-object v2, p0, Lorg/apache/tools/ant/types/FilterSet;->filtersFiles:Ljava/util/Vector;

    invoke-virtual {v2}, Ljava/util/Vector;->clear()V

    const/4 v2, 0x0

    iput-boolean v2, p0, Lorg/apache/tools/ant/types/FilterSet;->readingFiles:Z

    :cond_2
    iget-object v2, p0, Lorg/apache/tools/ant/types/FilterSet;->filters:Ljava/util/Vector;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    goto :goto_0

    :catchall_0
    move-exception v2

    monitor-exit p0

    throw v2
.end method

.method public getOnMissingFiltersFile()Lorg/apache/tools/ant/types/FilterSet$OnMissing;
    .locals 1

    iget-object v0, p0, Lorg/apache/tools/ant/types/FilterSet;->onMissingFiltersFile:Lorg/apache/tools/ant/types/FilterSet$OnMissing;

    return-object v0
.end method

.method protected getRef()Lorg/apache/tools/ant/types/FilterSet;
    .locals 2

    sget-object v0, Lorg/apache/tools/ant/types/FilterSet;->class$org$apache$tools$ant$types$FilterSet:Ljava/lang/Class;

    if-nez v0, :cond_0

    const-string v0, "org.apache.tools.ant.types.FilterSet"

    invoke-static {v0}, Lorg/apache/tools/ant/types/FilterSet;->class$(Ljava/lang/String;)Ljava/lang/Class;

    move-result-object v0

    sput-object v0, Lorg/apache/tools/ant/types/FilterSet;->class$org$apache$tools$ant$types$FilterSet:Ljava/lang/Class;

    :goto_0
    const-string v1, "filterset"

    invoke-virtual {p0, v0, v1}, Lorg/apache/tools/ant/types/FilterSet;->getCheckedRef(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lorg/apache/tools/ant/types/FilterSet;

    return-object v0

    :cond_0
    sget-object v0, Lorg/apache/tools/ant/types/FilterSet;->class$org$apache$tools$ant$types$FilterSet:Ljava/lang/Class;

    goto :goto_0
.end method

.method public declared-synchronized hasFilters()Z
    .locals 1

    monitor-enter p0

    :try_start_0
    invoke-virtual {p0}, Lorg/apache/tools/ant/types/FilterSet;->getFilters()Ljava/util/Vector;

    move-result-object v0

    invoke-virtual {v0}, Ljava/util/Vector;->size()I
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    move-result v0

    if-lez v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    monitor-exit p0

    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0

    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method public isRecurse()Z
    .locals 1

    iget-boolean v0, p0, Lorg/apache/tools/ant/types/FilterSet;->recurse:Z

    return v0
.end method

.method public declared-synchronized readFiltersFromFile(Ljava/io/File;)V
    .locals 11
    .param p1    # Ljava/io/File;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lorg/apache/tools/ant/BuildException;
        }
    .end annotation

    monitor-enter p0

    :try_start_0
    invoke-virtual {p0}, Lorg/apache/tools/ant/types/FilterSet;->isReference()Z

    move-result v8

    if-eqz v8, :cond_0

    invoke-virtual {p0}, Lorg/apache/tools/ant/types/FilterSet;->tooManyAttributes()Lorg/apache/tools/ant/BuildException;

    move-result-object v8

    throw v8
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    :catchall_0
    move-exception v8

    monitor-exit p0

    throw v8

    :cond_0
    :try_start_1
    invoke-virtual {p1}, Ljava/io/File;->exists()Z

    move-result v8

    if-nez v8, :cond_1

    new-instance v8, Ljava/lang/StringBuffer;

    invoke-direct {v8}, Ljava/lang/StringBuffer;-><init>()V

    const-string v9, "Could not read filters from file "

    invoke-virtual {v8, v9}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    move-result-object v8

    invoke-virtual {v8, p1}, Ljava/lang/StringBuffer;->append(Ljava/lang/Object;)Ljava/lang/StringBuffer;

    move-result-object v8

    const-string v9, " as it doesn\'t exist."

    invoke-virtual {v8, v9}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    move-result-object v8

    invoke-virtual {v8}, Ljava/lang/StringBuffer;->toString()Ljava/lang/String;

    move-result-object v8

    invoke-direct {p0, v8}, Lorg/apache/tools/ant/types/FilterSet;->handleMissingFile(Ljava/lang/String;)V

    :cond_1
    invoke-virtual {p1}, Ljava/io/File;->isFile()Z

    move-result v8

    if-eqz v8, :cond_3

    new-instance v8, Ljava/lang/StringBuffer;

    invoke-direct {v8}, Ljava/lang/StringBuffer;-><init>()V

    const-string v9, "Reading filters from "

    invoke-virtual {v8, v9}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    move-result-object v8

    invoke-virtual {v8, p1}, Ljava/lang/StringBuffer;->append(Ljava/lang/Object;)Ljava/lang/StringBuffer;

    move-result-object v8

    invoke-virtual {v8}, Ljava/lang/StringBuffer;->toString()Ljava/lang/String;

    move-result-object v8

    const/4 v9, 0x3

    invoke-virtual {p0, v8, v9}, Lorg/apache/tools/ant/types/FilterSet;->log(Ljava/lang/String;I)V
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    const/4 v3, 0x0

    :try_start_2
    new-instance v5, Ljava/util/Properties;

    invoke-direct {v5}, Ljava/util/Properties;-><init>()V

    new-instance v4, Ljava/io/FileInputStream;

    invoke-direct {v4, p1}, Ljava/io/FileInputStream;-><init>(Ljava/io/File;)V
    :try_end_2
    .catch Ljava/lang/Exception; {:try_start_2 .. :try_end_2} :catch_1
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    :try_start_3
    invoke-virtual {v5, v4}, Ljava/util/Properties;->load(Ljava/io/InputStream;)V

    invoke-virtual {v5}, Ljava/util/Properties;->propertyNames()Ljava/util/Enumeration;

    move-result-object v0

    invoke-virtual {p0}, Lorg/apache/tools/ant/types/FilterSet;->getFilters()Ljava/util/Vector;

    move-result-object v2

    :goto_0
    invoke-interface {v0}, Ljava/util/Enumeration;->hasMoreElements()Z

    move-result v8

    if-eqz v8, :cond_2

    invoke-interface {v0}, Ljava/util/Enumeration;->nextElement()Ljava/lang/Object;

    move-result-object v6

    check-cast v6, Ljava/lang/String;

    invoke-virtual {v5, v6}, Ljava/util/Properties;->getProperty(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v7

    new-instance v8, Lorg/apache/tools/ant/types/FilterSet$Filter;

    invoke-direct {v8, v6, v7}, Lorg/apache/tools/ant/types/FilterSet$Filter;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    invoke-virtual {v2, v8}, Ljava/util/Vector;->addElement(Ljava/lang/Object;)V
    :try_end_3
    .catch Ljava/lang/Exception; {:try_start_3 .. :try_end_3} :catch_0
    .catchall {:try_start_3 .. :try_end_3} :catchall_2

    goto :goto_0

    :catch_0
    move-exception v1

    move-object v3, v4

    :goto_1
    :try_start_4
    new-instance v8, Lorg/apache/tools/ant/BuildException;

    new-instance v9, Ljava/lang/StringBuffer;

    invoke-direct {v9}, Ljava/lang/StringBuffer;-><init>()V

    const-string v10, "Could not read filters from file: "

    invoke-virtual {v9, v10}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    move-result-object v9

    invoke-virtual {v9, p1}, Ljava/lang/StringBuffer;->append(Ljava/lang/Object;)Ljava/lang/StringBuffer;

    move-result-object v9

    invoke-virtual {v9}, Ljava/lang/StringBuffer;->toString()Ljava/lang/String;

    move-result-object v9

    invoke-direct {v8, v9}, Lorg/apache/tools/ant/BuildException;-><init>(Ljava/lang/String;)V

    throw v8
    :try_end_4
    .catchall {:try_start_4 .. :try_end_4} :catchall_1

    :catchall_1
    move-exception v8

    :goto_2
    :try_start_5
    invoke-static {v3}, Lorg/apache/tools/ant/util/FileUtils;->close(Ljava/io/InputStream;)V

    throw v8

    :cond_2
    invoke-static {v4}, Lorg/apache/tools/ant/util/FileUtils;->close(Ljava/io/InputStream;)V

    :goto_3
    const/4 v8, 0x0

    iput-object v8, p0, Lorg/apache/tools/ant/types/FilterSet;->filterHash:Ljava/util/Hashtable;
    :try_end_5
    .catchall {:try_start_5 .. :try_end_5} :catchall_0

    monitor-exit p0

    return-void

    :cond_3
    :try_start_6
    new-instance v8, Ljava/lang/StringBuffer;

    invoke-direct {v8}, Ljava/lang/StringBuffer;-><init>()V

    const-string v9, "Must specify a file rather than a directory in the filtersfile attribute:"

    invoke-virtual {v8, v9}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    move-result-object v8

    invoke-virtual {v8, p1}, Ljava/lang/StringBuffer;->append(Ljava/lang/Object;)Ljava/lang/StringBuffer;

    move-result-object v8

    invoke-virtual {v8}, Ljava/lang/StringBuffer;->toString()Ljava/lang/String;

    move-result-object v8

    invoke-direct {p0, v8}, Lorg/apache/tools/ant/types/FilterSet;->handleMissingFile(Ljava/lang/String;)V
    :try_end_6
    .catchall {:try_start_6 .. :try_end_6} :catchall_0

    goto :goto_3

    :catchall_2
    move-exception v8

    move-object v3, v4

    goto :goto_2

    :catch_1
    move-exception v1

    goto :goto_1
.end method

.method public declared-synchronized replaceTokens(Ljava/lang/String;)Ljava/lang/String;
    .locals 1
    .param p1    # Ljava/lang/String;

    monitor-enter p0

    :try_start_0
    invoke-direct {p0, p1}, Lorg/apache/tools/ant/types/FilterSet;->iReplaceTokens(Ljava/lang/String;)Ljava/lang/String;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    move-result-object v0

    monitor-exit p0

    return-object v0

    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method public setBeginToken(Ljava/lang/String;)V
    .locals 2
    .param p1    # Ljava/lang/String;

    invoke-virtual {p0}, Lorg/apache/tools/ant/types/FilterSet;->isReference()Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-virtual {p0}, Lorg/apache/tools/ant/types/FilterSet;->tooManyAttributes()Lorg/apache/tools/ant/BuildException;

    move-result-object v0

    throw v0

    :cond_0
    if-eqz p1, :cond_1

    const-string v0, ""

    invoke-virtual {v0, p1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_2

    :cond_1
    new-instance v0, Lorg/apache/tools/ant/BuildException;

    const-string v1, "beginToken must not be empty"

    invoke-direct {v0, v1}, Lorg/apache/tools/ant/BuildException;-><init>(Ljava/lang/String;)V

    throw v0

    :cond_2
    iput-object p1, p0, Lorg/apache/tools/ant/types/FilterSet;->startOfToken:Ljava/lang/String;

    return-void
.end method

.method public setEndToken(Ljava/lang/String;)V
    .locals 2
    .param p1    # Ljava/lang/String;

    invoke-virtual {p0}, Lorg/apache/tools/ant/types/FilterSet;->isReference()Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-virtual {p0}, Lorg/apache/tools/ant/types/FilterSet;->tooManyAttributes()Lorg/apache/tools/ant/BuildException;

    move-result-object v0

    throw v0

    :cond_0
    if-eqz p1, :cond_1

    const-string v0, ""

    invoke-virtual {v0, p1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_2

    :cond_1
    new-instance v0, Lorg/apache/tools/ant/BuildException;

    const-string v1, "endToken must not be empty"

    invoke-direct {v0, v1}, Lorg/apache/tools/ant/BuildException;-><init>(Ljava/lang/String;)V

    throw v0

    :cond_2
    iput-object p1, p0, Lorg/apache/tools/ant/types/FilterSet;->endOfToken:Ljava/lang/String;

    return-void
.end method

.method public setFiltersfile(Ljava/io/File;)V
    .locals 1
    .param p1    # Ljava/io/File;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lorg/apache/tools/ant/BuildException;
        }
    .end annotation

    invoke-virtual {p0}, Lorg/apache/tools/ant/types/FilterSet;->isReference()Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-virtual {p0}, Lorg/apache/tools/ant/types/FilterSet;->tooManyAttributes()Lorg/apache/tools/ant/BuildException;

    move-result-object v0

    throw v0

    :cond_0
    iget-object v0, p0, Lorg/apache/tools/ant/types/FilterSet;->filtersFiles:Ljava/util/Vector;

    invoke-virtual {v0, p1}, Ljava/util/Vector;->add(Ljava/lang/Object;)Z

    return-void
.end method

.method public setOnMissingFiltersFile(Lorg/apache/tools/ant/types/FilterSet$OnMissing;)V
    .locals 0
    .param p1    # Lorg/apache/tools/ant/types/FilterSet$OnMissing;

    iput-object p1, p0, Lorg/apache/tools/ant/types/FilterSet;->onMissingFiltersFile:Lorg/apache/tools/ant/types/FilterSet$OnMissing;

    return-void
.end method

.method public setRecurse(Z)V
    .locals 0
    .param p1    # Z

    iput-boolean p1, p0, Lorg/apache/tools/ant/types/FilterSet;->recurse:Z

    return-void
.end method
