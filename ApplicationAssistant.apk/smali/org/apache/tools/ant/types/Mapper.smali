.class public Lorg/apache/tools/ant/types/Mapper;
.super Lorg/apache/tools/ant/types/DataType;
.source "Mapper.java"

# interfaces
.implements Ljava/lang/Cloneable;


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lorg/apache/tools/ant/types/Mapper$MapperType;
    }
.end annotation


# instance fields
.field protected classname:Ljava/lang/String;

.field protected classpath:Lorg/apache/tools/ant/types/Path;

.field private container:Lorg/apache/tools/ant/util/ContainerMapper;

.field protected from:Ljava/lang/String;

.field protected to:Ljava/lang/String;

.field protected type:Lorg/apache/tools/ant/types/Mapper$MapperType;


# direct methods
.method public constructor <init>(Lorg/apache/tools/ant/Project;)V
    .locals 1
    .param p1    # Lorg/apache/tools/ant/Project;

    const/4 v0, 0x0

    invoke-direct {p0}, Lorg/apache/tools/ant/types/DataType;-><init>()V

    iput-object v0, p0, Lorg/apache/tools/ant/types/Mapper;->type:Lorg/apache/tools/ant/types/Mapper$MapperType;

    iput-object v0, p0, Lorg/apache/tools/ant/types/Mapper;->classname:Ljava/lang/String;

    iput-object v0, p0, Lorg/apache/tools/ant/types/Mapper;->classpath:Lorg/apache/tools/ant/types/Path;

    iput-object v0, p0, Lorg/apache/tools/ant/types/Mapper;->from:Ljava/lang/String;

    iput-object v0, p0, Lorg/apache/tools/ant/types/Mapper;->to:Ljava/lang/String;

    iput-object v0, p0, Lorg/apache/tools/ant/types/Mapper;->container:Lorg/apache/tools/ant/util/ContainerMapper;

    invoke-virtual {p0, p1}, Lorg/apache/tools/ant/types/Mapper;->setProject(Lorg/apache/tools/ant/Project;)V

    return-void
.end method


# virtual methods
.method public add(Lorg/apache/tools/ant/util/FileNameMapper;)V
    .locals 4
    .param p1    # Lorg/apache/tools/ant/util/FileNameMapper;

    invoke-virtual {p0}, Lorg/apache/tools/ant/types/Mapper;->isReference()Z

    move-result v1

    if-eqz v1, :cond_0

    invoke-virtual {p0}, Lorg/apache/tools/ant/types/Mapper;->noChildrenAllowed()Lorg/apache/tools/ant/BuildException;

    move-result-object v1

    throw v1

    :cond_0
    iget-object v1, p0, Lorg/apache/tools/ant/types/Mapper;->container:Lorg/apache/tools/ant/util/ContainerMapper;

    if-nez v1, :cond_1

    iget-object v1, p0, Lorg/apache/tools/ant/types/Mapper;->type:Lorg/apache/tools/ant/types/Mapper$MapperType;

    if-nez v1, :cond_2

    iget-object v1, p0, Lorg/apache/tools/ant/types/Mapper;->classname:Ljava/lang/String;

    if-nez v1, :cond_2

    new-instance v1, Lorg/apache/tools/ant/util/CompositeMapper;

    invoke-direct {v1}, Lorg/apache/tools/ant/util/CompositeMapper;-><init>()V

    iput-object v1, p0, Lorg/apache/tools/ant/types/Mapper;->container:Lorg/apache/tools/ant/util/ContainerMapper;

    :cond_1
    :goto_0
    iget-object v1, p0, Lorg/apache/tools/ant/types/Mapper;->container:Lorg/apache/tools/ant/util/ContainerMapper;

    invoke-virtual {v1, p1}, Lorg/apache/tools/ant/util/ContainerMapper;->add(Lorg/apache/tools/ant/util/FileNameMapper;)V

    return-void

    :cond_2
    invoke-virtual {p0}, Lorg/apache/tools/ant/types/Mapper;->getImplementation()Lorg/apache/tools/ant/util/FileNameMapper;

    move-result-object v0

    instance-of v1, v0, Lorg/apache/tools/ant/util/ContainerMapper;

    if-eqz v1, :cond_3

    check-cast v0, Lorg/apache/tools/ant/util/ContainerMapper;

    iput-object v0, p0, Lorg/apache/tools/ant/types/Mapper;->container:Lorg/apache/tools/ant/util/ContainerMapper;

    goto :goto_0

    :cond_3
    new-instance v1, Lorg/apache/tools/ant/BuildException;

    new-instance v2, Ljava/lang/StringBuffer;

    invoke-direct {v2}, Ljava/lang/StringBuffer;-><init>()V

    invoke-static {v0}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    move-result-object v2

    const-string v3, " mapper implementation does not support nested mappers!"

    invoke-virtual {v2, v3}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuffer;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-direct {v1, v2}, Lorg/apache/tools/ant/BuildException;-><init>(Ljava/lang/String;)V

    throw v1
.end method

.method public addConfigured(Lorg/apache/tools/ant/util/FileNameMapper;)V
    .locals 0
    .param p1    # Lorg/apache/tools/ant/util/FileNameMapper;

    invoke-virtual {p0, p1}, Lorg/apache/tools/ant/types/Mapper;->add(Lorg/apache/tools/ant/util/FileNameMapper;)V

    return-void
.end method

.method public addConfiguredMapper(Lorg/apache/tools/ant/types/Mapper;)V
    .locals 1
    .param p1    # Lorg/apache/tools/ant/types/Mapper;

    invoke-virtual {p1}, Lorg/apache/tools/ant/types/Mapper;->getImplementation()Lorg/apache/tools/ant/util/FileNameMapper;

    move-result-object v0

    invoke-virtual {p0, v0}, Lorg/apache/tools/ant/types/Mapper;->add(Lorg/apache/tools/ant/util/FileNameMapper;)V

    return-void
.end method

.method public createClasspath()Lorg/apache/tools/ant/types/Path;
    .locals 2

    invoke-virtual {p0}, Lorg/apache/tools/ant/types/Mapper;->isReference()Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-virtual {p0}, Lorg/apache/tools/ant/types/Mapper;->noChildrenAllowed()Lorg/apache/tools/ant/BuildException;

    move-result-object v0

    throw v0

    :cond_0
    iget-object v0, p0, Lorg/apache/tools/ant/types/Mapper;->classpath:Lorg/apache/tools/ant/types/Path;

    if-nez v0, :cond_1

    new-instance v0, Lorg/apache/tools/ant/types/Path;

    invoke-virtual {p0}, Lorg/apache/tools/ant/types/Mapper;->getProject()Lorg/apache/tools/ant/Project;

    move-result-object v1

    invoke-direct {v0, v1}, Lorg/apache/tools/ant/types/Path;-><init>(Lorg/apache/tools/ant/Project;)V

    iput-object v0, p0, Lorg/apache/tools/ant/types/Mapper;->classpath:Lorg/apache/tools/ant/types/Path;

    :cond_1
    iget-object v0, p0, Lorg/apache/tools/ant/types/Mapper;->classpath:Lorg/apache/tools/ant/types/Path;

    invoke-virtual {v0}, Lorg/apache/tools/ant/types/Path;->createPath()Lorg/apache/tools/ant/types/Path;

    move-result-object v0

    return-object v0
.end method

.method public getImplementation()Lorg/apache/tools/ant/util/FileNameMapper;
    .locals 7
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lorg/apache/tools/ant/BuildException;
        }
    .end annotation

    invoke-virtual {p0}, Lorg/apache/tools/ant/types/Mapper;->isReference()Z

    move-result v5

    if-eqz v5, :cond_0

    invoke-virtual {p0}, Lorg/apache/tools/ant/types/Mapper;->getRef()Lorg/apache/tools/ant/types/Mapper;

    move-result-object v5

    invoke-virtual {v5}, Lorg/apache/tools/ant/types/Mapper;->getImplementation()Lorg/apache/tools/ant/util/FileNameMapper;

    move-result-object v2

    :goto_0
    return-object v2

    :cond_0
    iget-object v5, p0, Lorg/apache/tools/ant/types/Mapper;->type:Lorg/apache/tools/ant/types/Mapper$MapperType;

    if-nez v5, :cond_1

    iget-object v5, p0, Lorg/apache/tools/ant/types/Mapper;->classname:Ljava/lang/String;

    if-nez v5, :cond_1

    iget-object v5, p0, Lorg/apache/tools/ant/types/Mapper;->container:Lorg/apache/tools/ant/util/ContainerMapper;

    if-nez v5, :cond_1

    new-instance v5, Lorg/apache/tools/ant/BuildException;

    const-string v6, "nested mapper or one of the attributes type or classname is required"

    invoke-direct {v5, v6}, Lorg/apache/tools/ant/BuildException;-><init>(Ljava/lang/String;)V

    throw v5

    :cond_1
    iget-object v5, p0, Lorg/apache/tools/ant/types/Mapper;->container:Lorg/apache/tools/ant/util/ContainerMapper;

    if-eqz v5, :cond_2

    iget-object v2, p0, Lorg/apache/tools/ant/types/Mapper;->container:Lorg/apache/tools/ant/util/ContainerMapper;

    goto :goto_0

    :cond_2
    iget-object v5, p0, Lorg/apache/tools/ant/types/Mapper;->type:Lorg/apache/tools/ant/types/Mapper$MapperType;

    if-eqz v5, :cond_3

    iget-object v5, p0, Lorg/apache/tools/ant/types/Mapper;->classname:Ljava/lang/String;

    if-eqz v5, :cond_3

    new-instance v5, Lorg/apache/tools/ant/BuildException;

    const-string v6, "must not specify both type and classname attribute"

    invoke-direct {v5, v6}, Lorg/apache/tools/ant/BuildException;-><init>(Ljava/lang/String;)V

    throw v5

    :cond_3
    :try_start_0
    invoke-virtual {p0}, Lorg/apache/tools/ant/types/Mapper;->getImplementationClass()Ljava/lang/Class;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/Class;->newInstance()Ljava/lang/Object;

    move-result-object v5

    check-cast v5, Lorg/apache/tools/ant/util/FileNameMapper;

    move-object v0, v5

    check-cast v0, Lorg/apache/tools/ant/util/FileNameMapper;

    move-object v2, v0

    invoke-virtual {p0}, Lorg/apache/tools/ant/types/Mapper;->getProject()Lorg/apache/tools/ant/Project;

    move-result-object v3

    if-eqz v3, :cond_4

    invoke-virtual {v3, v2}, Lorg/apache/tools/ant/Project;->setProjectReference(Ljava/lang/Object;)V

    :cond_4
    iget-object v5, p0, Lorg/apache/tools/ant/types/Mapper;->from:Ljava/lang/String;

    invoke-interface {v2, v5}, Lorg/apache/tools/ant/util/FileNameMapper;->setFrom(Ljava/lang/String;)V

    iget-object v5, p0, Lorg/apache/tools/ant/types/Mapper;->to:Ljava/lang/String;

    invoke-interface {v2, v5}, Lorg/apache/tools/ant/util/FileNameMapper;->setTo(Ljava/lang/String;)V
    :try_end_0
    .catch Lorg/apache/tools/ant/BuildException; {:try_start_0 .. :try_end_0} :catch_0
    .catch Ljava/lang/Throwable; {:try_start_0 .. :try_end_0} :catch_1

    goto :goto_0

    :catch_0
    move-exception v1

    throw v1

    :catch_1
    move-exception v4

    new-instance v5, Lorg/apache/tools/ant/BuildException;

    invoke-direct {v5, v4}, Lorg/apache/tools/ant/BuildException;-><init>(Ljava/lang/Throwable;)V

    throw v5
.end method

.method protected getImplementationClass()Ljava/lang/Class;
    .locals 4
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/lang/ClassNotFoundException;
        }
    .end annotation

    iget-object v0, p0, Lorg/apache/tools/ant/types/Mapper;->classname:Ljava/lang/String;

    iget-object v2, p0, Lorg/apache/tools/ant/types/Mapper;->type:Lorg/apache/tools/ant/types/Mapper$MapperType;

    if-eqz v2, :cond_0

    iget-object v2, p0, Lorg/apache/tools/ant/types/Mapper;->type:Lorg/apache/tools/ant/types/Mapper$MapperType;

    invoke-virtual {v2}, Lorg/apache/tools/ant/types/Mapper$MapperType;->getImplementation()Ljava/lang/String;

    move-result-object v0

    :cond_0
    iget-object v2, p0, Lorg/apache/tools/ant/types/Mapper;->classpath:Lorg/apache/tools/ant/types/Path;

    if-nez v2, :cond_1

    invoke-virtual {p0}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/Class;->getClassLoader()Ljava/lang/ClassLoader;

    move-result-object v1

    :goto_0
    const/4 v2, 0x1

    invoke-static {v0, v2, v1}, Ljava/lang/Class;->forName(Ljava/lang/String;ZLjava/lang/ClassLoader;)Ljava/lang/Class;

    move-result-object v2

    return-object v2

    :cond_1
    invoke-virtual {p0}, Lorg/apache/tools/ant/types/Mapper;->getProject()Lorg/apache/tools/ant/Project;

    move-result-object v2

    iget-object v3, p0, Lorg/apache/tools/ant/types/Mapper;->classpath:Lorg/apache/tools/ant/types/Path;

    invoke-virtual {v2, v3}, Lorg/apache/tools/ant/Project;->createClassLoader(Lorg/apache/tools/ant/types/Path;)Lorg/apache/tools/ant/AntClassLoader;

    move-result-object v1

    goto :goto_0
.end method

.method protected getRef()Lorg/apache/tools/ant/types/Mapper;
    .locals 1

    invoke-virtual {p0}, Lorg/apache/tools/ant/types/Mapper;->getCheckedRef()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lorg/apache/tools/ant/types/Mapper;

    return-object v0
.end method

.method public setClassname(Ljava/lang/String;)V
    .locals 1
    .param p1    # Ljava/lang/String;

    invoke-virtual {p0}, Lorg/apache/tools/ant/types/Mapper;->isReference()Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-virtual {p0}, Lorg/apache/tools/ant/types/Mapper;->tooManyAttributes()Lorg/apache/tools/ant/BuildException;

    move-result-object v0

    throw v0

    :cond_0
    iput-object p1, p0, Lorg/apache/tools/ant/types/Mapper;->classname:Ljava/lang/String;

    return-void
.end method

.method public setClasspath(Lorg/apache/tools/ant/types/Path;)V
    .locals 1
    .param p1    # Lorg/apache/tools/ant/types/Path;

    invoke-virtual {p0}, Lorg/apache/tools/ant/types/Mapper;->isReference()Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-virtual {p0}, Lorg/apache/tools/ant/types/Mapper;->tooManyAttributes()Lorg/apache/tools/ant/BuildException;

    move-result-object v0

    throw v0

    :cond_0
    iget-object v0, p0, Lorg/apache/tools/ant/types/Mapper;->classpath:Lorg/apache/tools/ant/types/Path;

    if-nez v0, :cond_1

    iput-object p1, p0, Lorg/apache/tools/ant/types/Mapper;->classpath:Lorg/apache/tools/ant/types/Path;

    :goto_0
    return-void

    :cond_1
    iget-object v0, p0, Lorg/apache/tools/ant/types/Mapper;->classpath:Lorg/apache/tools/ant/types/Path;

    invoke-virtual {v0, p1}, Lorg/apache/tools/ant/types/Path;->append(Lorg/apache/tools/ant/types/Path;)V

    goto :goto_0
.end method

.method public setClasspathRef(Lorg/apache/tools/ant/types/Reference;)V
    .locals 1
    .param p1    # Lorg/apache/tools/ant/types/Reference;

    invoke-virtual {p0}, Lorg/apache/tools/ant/types/Mapper;->isReference()Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-virtual {p0}, Lorg/apache/tools/ant/types/Mapper;->tooManyAttributes()Lorg/apache/tools/ant/BuildException;

    move-result-object v0

    throw v0

    :cond_0
    invoke-virtual {p0}, Lorg/apache/tools/ant/types/Mapper;->createClasspath()Lorg/apache/tools/ant/types/Path;

    move-result-object v0

    invoke-virtual {v0, p1}, Lorg/apache/tools/ant/types/Path;->setRefid(Lorg/apache/tools/ant/types/Reference;)V

    return-void
.end method

.method public setFrom(Ljava/lang/String;)V
    .locals 1
    .param p1    # Ljava/lang/String;

    invoke-virtual {p0}, Lorg/apache/tools/ant/types/Mapper;->isReference()Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-virtual {p0}, Lorg/apache/tools/ant/types/Mapper;->tooManyAttributes()Lorg/apache/tools/ant/BuildException;

    move-result-object v0

    throw v0

    :cond_0
    iput-object p1, p0, Lorg/apache/tools/ant/types/Mapper;->from:Ljava/lang/String;

    return-void
.end method

.method public setRefid(Lorg/apache/tools/ant/types/Reference;)V
    .locals 1
    .param p1    # Lorg/apache/tools/ant/types/Reference;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lorg/apache/tools/ant/BuildException;
        }
    .end annotation

    iget-object v0, p0, Lorg/apache/tools/ant/types/Mapper;->type:Lorg/apache/tools/ant/types/Mapper$MapperType;

    if-nez v0, :cond_0

    iget-object v0, p0, Lorg/apache/tools/ant/types/Mapper;->from:Ljava/lang/String;

    if-nez v0, :cond_0

    iget-object v0, p0, Lorg/apache/tools/ant/types/Mapper;->to:Ljava/lang/String;

    if-eqz v0, :cond_1

    :cond_0
    invoke-virtual {p0}, Lorg/apache/tools/ant/types/Mapper;->tooManyAttributes()Lorg/apache/tools/ant/BuildException;

    move-result-object v0

    throw v0

    :cond_1
    invoke-super {p0, p1}, Lorg/apache/tools/ant/types/DataType;->setRefid(Lorg/apache/tools/ant/types/Reference;)V

    return-void
.end method

.method public setTo(Ljava/lang/String;)V
    .locals 1
    .param p1    # Ljava/lang/String;

    invoke-virtual {p0}, Lorg/apache/tools/ant/types/Mapper;->isReference()Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-virtual {p0}, Lorg/apache/tools/ant/types/Mapper;->tooManyAttributes()Lorg/apache/tools/ant/BuildException;

    move-result-object v0

    throw v0

    :cond_0
    iput-object p1, p0, Lorg/apache/tools/ant/types/Mapper;->to:Ljava/lang/String;

    return-void
.end method

.method public setType(Lorg/apache/tools/ant/types/Mapper$MapperType;)V
    .locals 1
    .param p1    # Lorg/apache/tools/ant/types/Mapper$MapperType;

    invoke-virtual {p0}, Lorg/apache/tools/ant/types/Mapper;->isReference()Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-virtual {p0}, Lorg/apache/tools/ant/types/Mapper;->tooManyAttributes()Lorg/apache/tools/ant/BuildException;

    move-result-object v0

    throw v0

    :cond_0
    iput-object p1, p0, Lorg/apache/tools/ant/types/Mapper;->type:Lorg/apache/tools/ant/types/Mapper$MapperType;

    return-void
.end method
