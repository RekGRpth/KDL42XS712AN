.class public Lorg/apache/tools/ant/types/FileList;
.super Lorg/apache/tools/ant/types/DataType;
.source "FileList.java"

# interfaces
.implements Lorg/apache/tools/ant/types/ResourceCollection;


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lorg/apache/tools/ant/types/FileList$FileName;
    }
.end annotation


# instance fields
.field private dir:Ljava/io/File;

.field private filenames:Ljava/util/Vector;


# direct methods
.method public constructor <init>()V
    .locals 1

    invoke-direct {p0}, Lorg/apache/tools/ant/types/DataType;-><init>()V

    new-instance v0, Ljava/util/Vector;

    invoke-direct {v0}, Ljava/util/Vector;-><init>()V

    iput-object v0, p0, Lorg/apache/tools/ant/types/FileList;->filenames:Ljava/util/Vector;

    return-void
.end method

.method protected constructor <init>(Lorg/apache/tools/ant/types/FileList;)V
    .locals 1
    .param p1    # Lorg/apache/tools/ant/types/FileList;

    invoke-direct {p0}, Lorg/apache/tools/ant/types/DataType;-><init>()V

    new-instance v0, Ljava/util/Vector;

    invoke-direct {v0}, Ljava/util/Vector;-><init>()V

    iput-object v0, p0, Lorg/apache/tools/ant/types/FileList;->filenames:Ljava/util/Vector;

    iget-object v0, p1, Lorg/apache/tools/ant/types/FileList;->dir:Ljava/io/File;

    iput-object v0, p0, Lorg/apache/tools/ant/types/FileList;->dir:Ljava/io/File;

    iget-object v0, p1, Lorg/apache/tools/ant/types/FileList;->filenames:Ljava/util/Vector;

    iput-object v0, p0, Lorg/apache/tools/ant/types/FileList;->filenames:Ljava/util/Vector;

    invoke-virtual {p1}, Lorg/apache/tools/ant/types/FileList;->getProject()Lorg/apache/tools/ant/Project;

    move-result-object v0

    invoke-virtual {p0, v0}, Lorg/apache/tools/ant/types/FileList;->setProject(Lorg/apache/tools/ant/Project;)V

    return-void
.end method


# virtual methods
.method public addConfiguredFile(Lorg/apache/tools/ant/types/FileList$FileName;)V
    .locals 2
    .param p1    # Lorg/apache/tools/ant/types/FileList$FileName;

    invoke-virtual {p1}, Lorg/apache/tools/ant/types/FileList$FileName;->getName()Ljava/lang/String;

    move-result-object v0

    if-nez v0, :cond_0

    new-instance v0, Lorg/apache/tools/ant/BuildException;

    const-string v1, "No name specified in nested file element"

    invoke-direct {v0, v1}, Lorg/apache/tools/ant/BuildException;-><init>(Ljava/lang/String;)V

    throw v0

    :cond_0
    iget-object v0, p0, Lorg/apache/tools/ant/types/FileList;->filenames:Ljava/util/Vector;

    invoke-virtual {p1}, Lorg/apache/tools/ant/types/FileList$FileName;->getName()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/util/Vector;->addElement(Ljava/lang/Object;)V

    return-void
.end method

.method public getDir(Lorg/apache/tools/ant/Project;)Ljava/io/File;
    .locals 1
    .param p1    # Lorg/apache/tools/ant/Project;

    invoke-virtual {p0}, Lorg/apache/tools/ant/types/FileList;->isReference()Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-virtual {p0, p1}, Lorg/apache/tools/ant/types/FileList;->getRef(Lorg/apache/tools/ant/Project;)Lorg/apache/tools/ant/types/FileList;

    move-result-object v0

    invoke-virtual {v0, p1}, Lorg/apache/tools/ant/types/FileList;->getDir(Lorg/apache/tools/ant/Project;)Ljava/io/File;

    move-result-object v0

    :goto_0
    return-object v0

    :cond_0
    iget-object v0, p0, Lorg/apache/tools/ant/types/FileList;->dir:Ljava/io/File;

    goto :goto_0
.end method

.method public getFiles(Lorg/apache/tools/ant/Project;)[Ljava/lang/String;
    .locals 3
    .param p1    # Lorg/apache/tools/ant/Project;

    invoke-virtual {p0}, Lorg/apache/tools/ant/types/FileList;->isReference()Z

    move-result v1

    if-eqz v1, :cond_0

    invoke-virtual {p0, p1}, Lorg/apache/tools/ant/types/FileList;->getRef(Lorg/apache/tools/ant/Project;)Lorg/apache/tools/ant/types/FileList;

    move-result-object v1

    invoke-virtual {v1, p1}, Lorg/apache/tools/ant/types/FileList;->getFiles(Lorg/apache/tools/ant/Project;)[Ljava/lang/String;

    move-result-object v0

    :goto_0
    return-object v0

    :cond_0
    iget-object v1, p0, Lorg/apache/tools/ant/types/FileList;->dir:Ljava/io/File;

    if-nez v1, :cond_1

    new-instance v1, Lorg/apache/tools/ant/BuildException;

    const-string v2, "No directory specified for filelist."

    invoke-direct {v1, v2}, Lorg/apache/tools/ant/BuildException;-><init>(Ljava/lang/String;)V

    throw v1

    :cond_1
    iget-object v1, p0, Lorg/apache/tools/ant/types/FileList;->filenames:Ljava/util/Vector;

    invoke-virtual {v1}, Ljava/util/Vector;->size()I

    move-result v1

    if-nez v1, :cond_2

    new-instance v1, Lorg/apache/tools/ant/BuildException;

    const-string v2, "No files specified for filelist."

    invoke-direct {v1, v2}, Lorg/apache/tools/ant/BuildException;-><init>(Ljava/lang/String;)V

    throw v1

    :cond_2
    iget-object v1, p0, Lorg/apache/tools/ant/types/FileList;->filenames:Ljava/util/Vector;

    invoke-virtual {v1}, Ljava/util/Vector;->size()I

    move-result v1

    new-array v0, v1, [Ljava/lang/String;

    iget-object v1, p0, Lorg/apache/tools/ant/types/FileList;->filenames:Ljava/util/Vector;

    invoke-virtual {v1, v0}, Ljava/util/Vector;->copyInto([Ljava/lang/Object;)V

    goto :goto_0
.end method

.method protected getRef(Lorg/apache/tools/ant/Project;)Lorg/apache/tools/ant/types/FileList;
    .locals 1
    .param p1    # Lorg/apache/tools/ant/Project;

    invoke-virtual {p0, p1}, Lorg/apache/tools/ant/types/FileList;->getCheckedRef(Lorg/apache/tools/ant/Project;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lorg/apache/tools/ant/types/FileList;

    return-object v0
.end method

.method public isFilesystemOnly()Z
    .locals 1

    const/4 v0, 0x1

    return v0
.end method

.method public iterator()Ljava/util/Iterator;
    .locals 4

    invoke-virtual {p0}, Lorg/apache/tools/ant/types/FileList;->isReference()Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-virtual {p0}, Lorg/apache/tools/ant/types/FileList;->getProject()Lorg/apache/tools/ant/Project;

    move-result-object v0

    invoke-virtual {p0, v0}, Lorg/apache/tools/ant/types/FileList;->getRef(Lorg/apache/tools/ant/Project;)Lorg/apache/tools/ant/types/FileList;

    move-result-object v0

    invoke-virtual {v0}, Lorg/apache/tools/ant/types/FileList;->iterator()Ljava/util/Iterator;

    move-result-object v0

    :goto_0
    return-object v0

    :cond_0
    new-instance v1, Lorg/apache/tools/ant/types/resources/FileResourceIterator;

    iget-object v2, p0, Lorg/apache/tools/ant/types/FileList;->dir:Ljava/io/File;

    iget-object v0, p0, Lorg/apache/tools/ant/types/FileList;->filenames:Ljava/util/Vector;

    iget-object v3, p0, Lorg/apache/tools/ant/types/FileList;->filenames:Ljava/util/Vector;

    invoke-virtual {v3}, Ljava/util/Vector;->size()I

    move-result v3

    new-array v3, v3, [Ljava/lang/String;

    invoke-virtual {v0, v3}, Ljava/util/Vector;->toArray([Ljava/lang/Object;)[Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [Ljava/lang/String;

    check-cast v0, [Ljava/lang/String;

    invoke-direct {v1, v2, v0}, Lorg/apache/tools/ant/types/resources/FileResourceIterator;-><init>(Ljava/io/File;[Ljava/lang/String;)V

    move-object v0, v1

    goto :goto_0
.end method

.method public setDir(Ljava/io/File;)V
    .locals 0
    .param p1    # Ljava/io/File;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lorg/apache/tools/ant/BuildException;
        }
    .end annotation

    invoke-virtual {p0}, Lorg/apache/tools/ant/types/FileList;->checkAttributesAllowed()V

    iput-object p1, p0, Lorg/apache/tools/ant/types/FileList;->dir:Ljava/io/File;

    return-void
.end method

.method public setFiles(Ljava/lang/String;)V
    .locals 3
    .param p1    # Ljava/lang/String;

    invoke-virtual {p0}, Lorg/apache/tools/ant/types/FileList;->checkAttributesAllowed()V

    if-eqz p1, :cond_0

    invoke-virtual {p1}, Ljava/lang/String;->length()I

    move-result v1

    if-lez v1, :cond_0

    new-instance v0, Ljava/util/StringTokenizer;

    const-string v1, ", \t\n\r\u000c"

    const/4 v2, 0x0

    invoke-direct {v0, p1, v1, v2}, Ljava/util/StringTokenizer;-><init>(Ljava/lang/String;Ljava/lang/String;Z)V

    :goto_0
    invoke-virtual {v0}, Ljava/util/StringTokenizer;->hasMoreTokens()Z

    move-result v1

    if-eqz v1, :cond_0

    iget-object v1, p0, Lorg/apache/tools/ant/types/FileList;->filenames:Ljava/util/Vector;

    invoke-virtual {v0}, Ljava/util/StringTokenizer;->nextToken()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/util/Vector;->addElement(Ljava/lang/Object;)V

    goto :goto_0

    :cond_0
    return-void
.end method

.method public setRefid(Lorg/apache/tools/ant/types/Reference;)V
    .locals 1
    .param p1    # Lorg/apache/tools/ant/types/Reference;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lorg/apache/tools/ant/BuildException;
        }
    .end annotation

    iget-object v0, p0, Lorg/apache/tools/ant/types/FileList;->dir:Ljava/io/File;

    if-nez v0, :cond_0

    iget-object v0, p0, Lorg/apache/tools/ant/types/FileList;->filenames:Ljava/util/Vector;

    invoke-virtual {v0}, Ljava/util/Vector;->size()I

    move-result v0

    if-eqz v0, :cond_1

    :cond_0
    invoke-virtual {p0}, Lorg/apache/tools/ant/types/FileList;->tooManyAttributes()Lorg/apache/tools/ant/BuildException;

    move-result-object v0

    throw v0

    :cond_1
    invoke-super {p0, p1}, Lorg/apache/tools/ant/types/DataType;->setRefid(Lorg/apache/tools/ant/types/Reference;)V

    return-void
.end method

.method public size()I
    .locals 1

    invoke-virtual {p0}, Lorg/apache/tools/ant/types/FileList;->isReference()Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-virtual {p0}, Lorg/apache/tools/ant/types/FileList;->getProject()Lorg/apache/tools/ant/Project;

    move-result-object v0

    invoke-virtual {p0, v0}, Lorg/apache/tools/ant/types/FileList;->getRef(Lorg/apache/tools/ant/Project;)Lorg/apache/tools/ant/types/FileList;

    move-result-object v0

    invoke-virtual {v0}, Lorg/apache/tools/ant/types/FileList;->size()I

    move-result v0

    :goto_0
    return v0

    :cond_0
    iget-object v0, p0, Lorg/apache/tools/ant/types/FileList;->filenames:Ljava/util/Vector;

    invoke-virtual {v0}, Ljava/util/Vector;->size()I

    move-result v0

    goto :goto_0
.end method
