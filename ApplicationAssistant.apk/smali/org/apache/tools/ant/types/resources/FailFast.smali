.class Lorg/apache/tools/ant/types/resources/FailFast;
.super Ljava/lang/Object;
.source "FailFast.java"

# interfaces
.implements Ljava/util/Iterator;


# static fields
.field private static final MAP:Ljava/util/WeakHashMap;


# instance fields
.field private parent:Ljava/lang/Object;

.field private wrapped:Ljava/util/Iterator;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    new-instance v0, Ljava/util/WeakHashMap;

    invoke-direct {v0}, Ljava/util/WeakHashMap;-><init>()V

    sput-object v0, Lorg/apache/tools/ant/types/resources/FailFast;->MAP:Ljava/util/WeakHashMap;

    return-void
.end method

.method constructor <init>(Ljava/lang/Object;Ljava/util/Iterator;)V
    .locals 2
    .param p1    # Ljava/lang/Object;
    .param p2    # Ljava/util/Iterator;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    if-nez p1, :cond_0

    new-instance v0, Ljava/lang/IllegalArgumentException;

    const-string v1, "parent object is null"

    invoke-direct {v0, v1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v0

    :cond_0
    if-nez p2, :cond_1

    new-instance v0, Ljava/lang/IllegalArgumentException;

    const-string v1, "cannot wrap null iterator"

    invoke-direct {v0, v1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v0

    :cond_1
    iput-object p1, p0, Lorg/apache/tools/ant/types/resources/FailFast;->parent:Ljava/lang/Object;

    invoke-interface {p2}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_2

    iput-object p2, p0, Lorg/apache/tools/ant/types/resources/FailFast;->wrapped:Ljava/util/Iterator;

    invoke-static {p0}, Lorg/apache/tools/ant/types/resources/FailFast;->add(Lorg/apache/tools/ant/types/resources/FailFast;)V

    :cond_2
    return-void
.end method

.method private static declared-synchronized add(Lorg/apache/tools/ant/types/resources/FailFast;)V
    .locals 5
    .param p0    # Lorg/apache/tools/ant/types/resources/FailFast;

    const-class v3, Lorg/apache/tools/ant/types/resources/FailFast;

    monitor-enter v3

    :try_start_0
    sget-object v2, Lorg/apache/tools/ant/types/resources/FailFast;->MAP:Ljava/util/WeakHashMap;

    iget-object v4, p0, Lorg/apache/tools/ant/types/resources/FailFast;->parent:Ljava/lang/Object;

    invoke-virtual {v2, v4}, Ljava/util/WeakHashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Ljava/util/Set;

    move-object v0, v2

    check-cast v0, Ljava/util/Set;

    move-object v1, v0

    if-nez v1, :cond_0

    new-instance v1, Ljava/util/HashSet;

    invoke-direct {v1}, Ljava/util/HashSet;-><init>()V

    sget-object v2, Lorg/apache/tools/ant/types/resources/FailFast;->MAP:Ljava/util/WeakHashMap;

    iget-object v4, p0, Lorg/apache/tools/ant/types/resources/FailFast;->parent:Ljava/lang/Object;

    invoke-virtual {v2, v4, v1}, Ljava/util/WeakHashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    :cond_0
    invoke-interface {v1, p0}, Ljava/util/Set;->add(Ljava/lang/Object;)Z
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    monitor-exit v3

    return-void

    :catchall_0
    move-exception v2

    monitor-exit v3

    throw v2
.end method

.method private static declared-synchronized failFast(Lorg/apache/tools/ant/types/resources/FailFast;)V
    .locals 5
    .param p0    # Lorg/apache/tools/ant/types/resources/FailFast;

    const-class v3, Lorg/apache/tools/ant/types/resources/FailFast;

    monitor-enter v3

    :try_start_0
    sget-object v2, Lorg/apache/tools/ant/types/resources/FailFast;->MAP:Ljava/util/WeakHashMap;

    iget-object v4, p0, Lorg/apache/tools/ant/types/resources/FailFast;->parent:Ljava/lang/Object;

    invoke-virtual {v2, v4}, Ljava/util/WeakHashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Ljava/util/Set;

    move-object v0, v2

    check-cast v0, Ljava/util/Set;

    move-object v1, v0

    invoke-interface {v1, p0}, Ljava/util/Set;->contains(Ljava/lang/Object;)Z

    move-result v2

    if-nez v2, :cond_0

    new-instance v2, Ljava/util/ConcurrentModificationException;

    invoke-direct {v2}, Ljava/util/ConcurrentModificationException;-><init>()V

    throw v2
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    :catchall_0
    move-exception v2

    monitor-exit v3

    throw v2

    :cond_0
    monitor-exit v3

    return-void
.end method

.method static declared-synchronized invalidate(Ljava/lang/Object;)V
    .locals 4
    .param p0    # Ljava/lang/Object;

    const-class v3, Lorg/apache/tools/ant/types/resources/FailFast;

    monitor-enter v3

    :try_start_0
    sget-object v2, Lorg/apache/tools/ant/types/resources/FailFast;->MAP:Ljava/util/WeakHashMap;

    invoke-virtual {v2, p0}, Ljava/util/WeakHashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Ljava/util/Set;

    move-object v0, v2

    check-cast v0, Ljava/util/Set;

    move-object v1, v0

    if-eqz v1, :cond_0

    invoke-interface {v1}, Ljava/util/Set;->clear()V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    :cond_0
    monitor-exit v3

    return-void

    :catchall_0
    move-exception v2

    monitor-exit v3

    throw v2
.end method

.method private static declared-synchronized remove(Lorg/apache/tools/ant/types/resources/FailFast;)V
    .locals 5
    .param p0    # Lorg/apache/tools/ant/types/resources/FailFast;

    const-class v3, Lorg/apache/tools/ant/types/resources/FailFast;

    monitor-enter v3

    :try_start_0
    sget-object v2, Lorg/apache/tools/ant/types/resources/FailFast;->MAP:Ljava/util/WeakHashMap;

    iget-object v4, p0, Lorg/apache/tools/ant/types/resources/FailFast;->parent:Ljava/lang/Object;

    invoke-virtual {v2, v4}, Ljava/util/WeakHashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Ljava/util/Set;

    move-object v0, v2

    check-cast v0, Ljava/util/Set;

    move-object v1, v0

    if-eqz v1, :cond_0

    invoke-interface {v1, p0}, Ljava/util/Set;->remove(Ljava/lang/Object;)Z
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    :cond_0
    monitor-exit v3

    return-void

    :catchall_0
    move-exception v2

    monitor-exit v3

    throw v2
.end method


# virtual methods
.method public hasNext()Z
    .locals 1

    iget-object v0, p0, Lorg/apache/tools/ant/types/resources/FailFast;->wrapped:Ljava/util/Iterator;

    if-nez v0, :cond_0

    const/4 v0, 0x0

    :goto_0
    return v0

    :cond_0
    invoke-static {p0}, Lorg/apache/tools/ant/types/resources/FailFast;->failFast(Lorg/apache/tools/ant/types/resources/FailFast;)V

    iget-object v0, p0, Lorg/apache/tools/ant/types/resources/FailFast;->wrapped:Ljava/util/Iterator;

    invoke-interface {v0}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    goto :goto_0
.end method

.method public next()Ljava/lang/Object;
    .locals 3

    const/4 v2, 0x0

    iget-object v0, p0, Lorg/apache/tools/ant/types/resources/FailFast;->wrapped:Ljava/util/Iterator;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lorg/apache/tools/ant/types/resources/FailFast;->wrapped:Ljava/util/Iterator;

    invoke-interface {v0}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-nez v0, :cond_1

    :cond_0
    new-instance v0, Ljava/util/NoSuchElementException;

    invoke-direct {v0}, Ljava/util/NoSuchElementException;-><init>()V

    throw v0

    :cond_1
    invoke-static {p0}, Lorg/apache/tools/ant/types/resources/FailFast;->failFast(Lorg/apache/tools/ant/types/resources/FailFast;)V

    :try_start_0
    iget-object v0, p0, Lorg/apache/tools/ant/types/resources/FailFast;->wrapped:Ljava/util/Iterator;

    invoke-interface {v0}, Ljava/util/Iterator;->next()Ljava/lang/Object;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    move-result-object v0

    iget-object v1, p0, Lorg/apache/tools/ant/types/resources/FailFast;->wrapped:Ljava/util/Iterator;

    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v1

    if-nez v1, :cond_2

    iput-object v2, p0, Lorg/apache/tools/ant/types/resources/FailFast;->wrapped:Ljava/util/Iterator;

    invoke-static {p0}, Lorg/apache/tools/ant/types/resources/FailFast;->remove(Lorg/apache/tools/ant/types/resources/FailFast;)V

    :cond_2
    return-object v0

    :catchall_0
    move-exception v0

    iget-object v1, p0, Lorg/apache/tools/ant/types/resources/FailFast;->wrapped:Ljava/util/Iterator;

    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v1

    if-nez v1, :cond_3

    iput-object v2, p0, Lorg/apache/tools/ant/types/resources/FailFast;->wrapped:Ljava/util/Iterator;

    invoke-static {p0}, Lorg/apache/tools/ant/types/resources/FailFast;->remove(Lorg/apache/tools/ant/types/resources/FailFast;)V

    :cond_3
    throw v0
.end method

.method public remove()V
    .locals 1

    new-instance v0, Ljava/lang/UnsupportedOperationException;

    invoke-direct {v0}, Ljava/lang/UnsupportedOperationException;-><init>()V

    throw v0
.end method
