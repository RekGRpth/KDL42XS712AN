.class public Lorg/apache/tools/ant/types/resources/selectors/Name;
.super Ljava/lang/Object;
.source "Name.java"

# interfaces
.implements Lorg/apache/tools/ant/types/resources/selectors/ResourceSelector;


# instance fields
.field private cs:Z

.field private pattern:Ljava/lang/String;


# direct methods
.method public constructor <init>()V
    .locals 1

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    const/4 v0, 0x1

    iput-boolean v0, p0, Lorg/apache/tools/ant/types/resources/selectors/Name;->cs:Z

    return-void
.end method


# virtual methods
.method public getName()Ljava/lang/String;
    .locals 1

    iget-object v0, p0, Lorg/apache/tools/ant/types/resources/selectors/Name;->pattern:Ljava/lang/String;

    return-object v0
.end method

.method public isCaseSensitive()Z
    .locals 1

    iget-boolean v0, p0, Lorg/apache/tools/ant/types/resources/selectors/Name;->cs:Z

    return v0
.end method

.method public isSelected(Lorg/apache/tools/ant/types/Resource;)Z
    .locals 4
    .param p1    # Lorg/apache/tools/ant/types/Resource;

    invoke-virtual {p1}, Lorg/apache/tools/ant/types/Resource;->getName()Ljava/lang/String;

    move-result-object v0

    iget-object v2, p0, Lorg/apache/tools/ant/types/resources/selectors/Name;->pattern:Ljava/lang/String;

    iget-boolean v3, p0, Lorg/apache/tools/ant/types/resources/selectors/Name;->cs:Z

    invoke-static {v2, v0, v3}, Lorg/apache/tools/ant/types/selectors/SelectorUtils;->match(Ljava/lang/String;Ljava/lang/String;Z)Z

    move-result v2

    if-eqz v2, :cond_0

    const/4 v2, 0x1

    :goto_0
    return v2

    :cond_0
    invoke-virtual {p1}, Lorg/apache/tools/ant/types/Resource;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v1, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_1

    const/4 v2, 0x0

    goto :goto_0

    :cond_1
    iget-object v2, p0, Lorg/apache/tools/ant/types/resources/selectors/Name;->pattern:Ljava/lang/String;

    iget-boolean v3, p0, Lorg/apache/tools/ant/types/resources/selectors/Name;->cs:Z

    invoke-static {v2, v1, v3}, Lorg/apache/tools/ant/types/selectors/SelectorUtils;->match(Ljava/lang/String;Ljava/lang/String;Z)Z

    move-result v2

    goto :goto_0
.end method

.method public setCaseSensitive(Z)V
    .locals 0
    .param p1    # Z

    iput-boolean p1, p0, Lorg/apache/tools/ant/types/resources/selectors/Name;->cs:Z

    return-void
.end method

.method public setName(Ljava/lang/String;)V
    .locals 0
    .param p1    # Ljava/lang/String;

    iput-object p1, p0, Lorg/apache/tools/ant/types/resources/selectors/Name;->pattern:Ljava/lang/String;

    return-void
.end method
