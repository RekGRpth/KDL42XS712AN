.class public Lorg/apache/tools/ant/types/resources/Tokens;
.super Lorg/apache/tools/ant/types/resources/BaseResourceCollectionWrapper;
.source "Tokens.java"


# instance fields
.field private encoding:Ljava/lang/String;

.field private tokenizer:Lorg/apache/tools/ant/util/Tokenizer;


# direct methods
.method public constructor <init>()V
    .locals 0

    invoke-direct {p0}, Lorg/apache/tools/ant/types/resources/BaseResourceCollectionWrapper;-><init>()V

    return-void
.end method


# virtual methods
.method public declared-synchronized add(Lorg/apache/tools/ant/util/Tokenizer;)V
    .locals 2
    .param p1    # Lorg/apache/tools/ant/util/Tokenizer;

    monitor-enter p0

    :try_start_0
    invoke-virtual {p0}, Lorg/apache/tools/ant/types/resources/Tokens;->isReference()Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-virtual {p0}, Lorg/apache/tools/ant/types/resources/Tokens;->noChildrenAllowed()Lorg/apache/tools/ant/BuildException;

    move-result-object v0

    throw v0
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0

    :cond_0
    :try_start_1
    iget-object v0, p0, Lorg/apache/tools/ant/types/resources/Tokens;->tokenizer:Lorg/apache/tools/ant/util/Tokenizer;

    if-eqz v0, :cond_1

    new-instance v0, Lorg/apache/tools/ant/BuildException;

    const-string v1, "Only one nested tokenizer allowed."

    invoke-direct {v0, v1}, Lorg/apache/tools/ant/BuildException;-><init>(Ljava/lang/String;)V

    throw v0

    :cond_1
    iput-object p1, p0, Lorg/apache/tools/ant/types/resources/Tokens;->tokenizer:Lorg/apache/tools/ant/util/Tokenizer;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    monitor-exit p0

    return-void
.end method

.method protected declared-synchronized dieOnCircularReference(Ljava/util/Stack;Lorg/apache/tools/ant/Project;)V
    .locals 1
    .param p1    # Ljava/util/Stack;
    .param p2    # Lorg/apache/tools/ant/Project;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lorg/apache/tools/ant/BuildException;
        }
    .end annotation

    monitor-enter p0

    :try_start_0
    invoke-virtual {p0}, Lorg/apache/tools/ant/types/resources/Tokens;->isChecked()Z
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    move-result v0

    if-eqz v0, :cond_0

    :goto_0
    monitor-exit p0

    return-void

    :cond_0
    :try_start_1
    invoke-virtual {p0}, Lorg/apache/tools/ant/types/resources/Tokens;->isReference()Z

    move-result v0

    if-eqz v0, :cond_1

    invoke-super {p0, p1, p2}, Lorg/apache/tools/ant/types/resources/BaseResourceCollectionWrapper;->dieOnCircularReference(Ljava/util/Stack;Lorg/apache/tools/ant/Project;)V
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    goto :goto_0

    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0

    :cond_1
    :try_start_2
    iget-object v0, p0, Lorg/apache/tools/ant/types/resources/Tokens;->tokenizer:Lorg/apache/tools/ant/util/Tokenizer;

    instance-of v0, v0, Lorg/apache/tools/ant/types/DataType;

    if-eqz v0, :cond_2

    iget-object v0, p0, Lorg/apache/tools/ant/types/resources/Tokens;->tokenizer:Lorg/apache/tools/ant/util/Tokenizer;

    invoke-virtual {p1, v0}, Ljava/util/Stack;->push(Ljava/lang/Object;)Ljava/lang/Object;

    iget-object v0, p0, Lorg/apache/tools/ant/types/resources/Tokens;->tokenizer:Lorg/apache/tools/ant/util/Tokenizer;

    check-cast v0, Lorg/apache/tools/ant/types/DataType;

    invoke-static {v0, p1, p2}, Lorg/apache/tools/ant/types/resources/Tokens;->invokeCircularReferenceCheck(Lorg/apache/tools/ant/types/DataType;Ljava/util/Stack;Lorg/apache/tools/ant/Project;)V

    :cond_2
    const/4 v0, 0x1

    invoke-virtual {p0, v0}, Lorg/apache/tools/ant/types/resources/Tokens;->setChecked(Z)V
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    goto :goto_0
.end method

.method protected declared-synchronized getCollection()Ljava/util/Collection;
    .locals 8

    monitor-enter p0

    :try_start_0
    invoke-virtual {p0}, Lorg/apache/tools/ant/types/resources/Tokens;->getResourceCollection()Lorg/apache/tools/ant/types/ResourceCollection;

    move-result-object v2

    invoke-interface {v2}, Lorg/apache/tools/ant/types/ResourceCollection;->size()I

    move-result v6

    if-nez v6, :cond_1

    sget-object v4, Ljava/util/Collections;->EMPTY_SET:Ljava/util/Set;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    :cond_0
    monitor-exit p0

    return-object v4

    :cond_1
    :try_start_1
    iget-object v6, p0, Lorg/apache/tools/ant/types/resources/Tokens;->tokenizer:Lorg/apache/tools/ant/util/Tokenizer;

    if-nez v6, :cond_2

    new-instance v6, Lorg/apache/tools/ant/util/LineTokenizer;

    invoke-direct {v6}, Lorg/apache/tools/ant/util/LineTokenizer;-><init>()V

    iput-object v6, p0, Lorg/apache/tools/ant/types/resources/Tokens;->tokenizer:Lorg/apache/tools/ant/util/Tokenizer;

    :cond_2
    new-instance v0, Lorg/apache/tools/ant/util/ConcatResourceInputStream;

    invoke-direct {v0, v2}, Lorg/apache/tools/ant/util/ConcatResourceInputStream;-><init>(Lorg/apache/tools/ant/types/ResourceCollection;)V

    invoke-virtual {v0, p0}, Lorg/apache/tools/ant/util/ConcatResourceInputStream;->setManagingComponent(Lorg/apache/tools/ant/ProjectComponent;)V

    const/4 v3, 0x0

    iget-object v6, p0, Lorg/apache/tools/ant/types/resources/Tokens;->encoding:Ljava/lang/String;

    if-nez v6, :cond_3

    new-instance v3, Ljava/io/InputStreamReader;

    invoke-direct {v3, v0}, Ljava/io/InputStreamReader;-><init>(Ljava/io/InputStream;)V

    :goto_0
    new-instance v4, Ljava/util/ArrayList;

    invoke-direct {v4}, Ljava/util/ArrayList;-><init>()V
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    :try_start_2
    iget-object v6, p0, Lorg/apache/tools/ant/types/resources/Tokens;->tokenizer:Lorg/apache/tools/ant/util/Tokenizer;

    invoke-interface {v6, v3}, Lorg/apache/tools/ant/util/Tokenizer;->getToken(Ljava/io/Reader;)Ljava/lang/String;

    move-result-object v5

    :goto_1
    if-eqz v5, :cond_0

    new-instance v6, Lorg/apache/tools/ant/types/resources/StringResource;

    invoke-direct {v6, v5}, Lorg/apache/tools/ant/types/resources/StringResource;-><init>(Ljava/lang/String;)V

    invoke-virtual {v4, v6}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    iget-object v6, p0, Lorg/apache/tools/ant/types/resources/Tokens;->tokenizer:Lorg/apache/tools/ant/util/Tokenizer;

    invoke-interface {v6, v3}, Lorg/apache/tools/ant/util/Tokenizer;->getToken(Ljava/io/Reader;)Ljava/lang/String;
    :try_end_2
    .catch Ljava/io/IOException; {:try_start_2 .. :try_end_2} :catch_1
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    move-result-object v5

    goto :goto_1

    :cond_3
    :try_start_3
    new-instance v3, Ljava/io/InputStreamReader;

    iget-object v6, p0, Lorg/apache/tools/ant/types/resources/Tokens;->encoding:Ljava/lang/String;

    invoke-direct {v3, v0, v6}, Ljava/io/InputStreamReader;-><init>(Ljava/io/InputStream;Ljava/lang/String;)V
    :try_end_3
    .catch Ljava/io/UnsupportedEncodingException; {:try_start_3 .. :try_end_3} :catch_0
    .catchall {:try_start_3 .. :try_end_3} :catchall_0

    goto :goto_0

    :catch_0
    move-exception v1

    :try_start_4
    new-instance v6, Lorg/apache/tools/ant/BuildException;

    invoke-direct {v6, v1}, Lorg/apache/tools/ant/BuildException;-><init>(Ljava/lang/Throwable;)V

    throw v6
    :try_end_4
    .catchall {:try_start_4 .. :try_end_4} :catchall_0

    :catchall_0
    move-exception v6

    monitor-exit p0

    throw v6

    :catch_1
    move-exception v1

    :try_start_5
    new-instance v6, Lorg/apache/tools/ant/BuildException;

    const-string v7, "Error reading tokens"

    invoke-direct {v6, v7, v1}, Lorg/apache/tools/ant/BuildException;-><init>(Ljava/lang/String;Ljava/lang/Throwable;)V

    throw v6
    :try_end_5
    .catchall {:try_start_5 .. :try_end_5} :catchall_0
.end method

.method public declared-synchronized setEncoding(Ljava/lang/String;)V
    .locals 1
    .param p1    # Ljava/lang/String;

    monitor-enter p0

    :try_start_0
    iput-object p1, p0, Lorg/apache/tools/ant/types/resources/Tokens;->encoding:Ljava/lang/String;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    monitor-exit p0

    return-void

    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method
