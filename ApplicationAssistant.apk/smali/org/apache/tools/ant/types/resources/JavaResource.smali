.class public Lorg/apache/tools/ant/types/resources/JavaResource;
.super Lorg/apache/tools/ant/types/Resource;
.source "JavaResource.java"


# static fields
.field static class$org$apache$tools$ant$types$resources$JavaResource:Ljava/lang/Class;


# instance fields
.field private classpath:Lorg/apache/tools/ant/types/Path;

.field private loader:Lorg/apache/tools/ant/types/Reference;


# direct methods
.method public constructor <init>()V
    .locals 0

    invoke-direct {p0}, Lorg/apache/tools/ant/types/Resource;-><init>()V

    return-void
.end method

.method public constructor <init>(Ljava/lang/String;Lorg/apache/tools/ant/types/Path;)V
    .locals 0
    .param p1    # Ljava/lang/String;
    .param p2    # Lorg/apache/tools/ant/types/Path;

    invoke-direct {p0}, Lorg/apache/tools/ant/types/Resource;-><init>()V

    invoke-virtual {p0, p1}, Lorg/apache/tools/ant/types/resources/JavaResource;->setName(Ljava/lang/String;)V

    iput-object p2, p0, Lorg/apache/tools/ant/types/resources/JavaResource;->classpath:Lorg/apache/tools/ant/types/Path;

    return-void
.end method

.method static class$(Ljava/lang/String;)Ljava/lang/Class;
    .locals 3
    .param p0    # Ljava/lang/String;

    :try_start_0
    invoke-static {p0}, Ljava/lang/Class;->forName(Ljava/lang/String;)Ljava/lang/Class;
    :try_end_0
    .catch Ljava/lang/ClassNotFoundException; {:try_start_0 .. :try_end_0} :catch_0

    move-result-object v1

    return-object v1

    :catch_0
    move-exception v0

    new-instance v1, Ljava/lang/NoClassDefFoundError;

    invoke-virtual {v0}, Ljava/lang/ClassNotFoundException;->getMessage()Ljava/lang/String;

    move-result-object v2

    invoke-direct {v1, v2}, Ljava/lang/NoClassDefFoundError;-><init>(Ljava/lang/String;)V

    throw v1
.end method


# virtual methods
.method public compareTo(Ljava/lang/Object;)I
    .locals 7
    .param p1    # Ljava/lang/Object;

    const/4 v4, 0x1

    const/4 v3, -0x1

    invoke-virtual {p0}, Lorg/apache/tools/ant/types/resources/JavaResource;->isReference()Z

    move-result v5

    if-eqz v5, :cond_1

    invoke-virtual {p0}, Lorg/apache/tools/ant/types/resources/JavaResource;->getCheckedRef()Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Ljava/lang/Comparable;

    invoke-interface {v3, p1}, Ljava/lang/Comparable;->compareTo(Ljava/lang/Object;)I

    move-result v3

    :cond_0
    :goto_0
    return v3

    :cond_1
    invoke-virtual {p1}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v5

    invoke-virtual {p0}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v6

    invoke-virtual {v5, v6}, Ljava/lang/Object;->equals(Ljava/lang/Object;)Z

    move-result v5

    if-eqz v5, :cond_7

    move-object v1, p1

    check-cast v1, Lorg/apache/tools/ant/types/resources/JavaResource;

    invoke-virtual {p0}, Lorg/apache/tools/ant/types/resources/JavaResource;->getName()Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v1}, Lorg/apache/tools/ant/types/resources/JavaResource;->getName()Ljava/lang/String;

    move-result-object v6

    invoke-virtual {v5, v6}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v5

    if-nez v5, :cond_2

    invoke-virtual {p0}, Lorg/apache/tools/ant/types/resources/JavaResource;->getName()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v1}, Lorg/apache/tools/ant/types/resources/JavaResource;->getName()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v3, v4}, Ljava/lang/String;->compareTo(Ljava/lang/String;)I

    move-result v3

    goto :goto_0

    :cond_2
    iget-object v5, p0, Lorg/apache/tools/ant/types/resources/JavaResource;->loader:Lorg/apache/tools/ant/types/Reference;

    iget-object v6, v1, Lorg/apache/tools/ant/types/resources/JavaResource;->loader:Lorg/apache/tools/ant/types/Reference;

    if-eq v5, v6, :cond_4

    iget-object v5, p0, Lorg/apache/tools/ant/types/resources/JavaResource;->loader:Lorg/apache/tools/ant/types/Reference;

    if-eqz v5, :cond_0

    iget-object v3, v1, Lorg/apache/tools/ant/types/resources/JavaResource;->loader:Lorg/apache/tools/ant/types/Reference;

    if-nez v3, :cond_3

    move v3, v4

    goto :goto_0

    :cond_3
    iget-object v3, p0, Lorg/apache/tools/ant/types/resources/JavaResource;->loader:Lorg/apache/tools/ant/types/Reference;

    invoke-virtual {v3}, Lorg/apache/tools/ant/types/Reference;->getRefId()Ljava/lang/String;

    move-result-object v3

    iget-object v4, v1, Lorg/apache/tools/ant/types/resources/JavaResource;->loader:Lorg/apache/tools/ant/types/Reference;

    invoke-virtual {v4}, Lorg/apache/tools/ant/types/Reference;->getRefId()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v3, v4}, Ljava/lang/String;->compareTo(Ljava/lang/String;)I

    move-result v3

    goto :goto_0

    :cond_4
    invoke-virtual {p0}, Lorg/apache/tools/ant/types/resources/JavaResource;->getClasspath()Lorg/apache/tools/ant/types/Path;

    move-result-object v2

    invoke-virtual {v1}, Lorg/apache/tools/ant/types/resources/JavaResource;->getClasspath()Lorg/apache/tools/ant/types/Path;

    move-result-object v0

    if-eq v2, v0, :cond_6

    if-eqz v2, :cond_0

    if-nez v0, :cond_5

    move v3, v4

    goto :goto_0

    :cond_5
    invoke-virtual {v2}, Lorg/apache/tools/ant/types/Path;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v0}, Lorg/apache/tools/ant/types/Path;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v3, v4}, Ljava/lang/String;->compareTo(Ljava/lang/String;)I

    move-result v3

    goto :goto_0

    :cond_6
    const/4 v3, 0x0

    goto :goto_0

    :cond_7
    invoke-super {p0, p1}, Lorg/apache/tools/ant/types/Resource;->compareTo(Ljava/lang/Object;)I

    move-result v3

    goto :goto_0
.end method

.method public createClasspath()Lorg/apache/tools/ant/types/Path;
    .locals 2

    invoke-virtual {p0}, Lorg/apache/tools/ant/types/resources/JavaResource;->checkChildrenAllowed()V

    iget-object v0, p0, Lorg/apache/tools/ant/types/resources/JavaResource;->classpath:Lorg/apache/tools/ant/types/Path;

    if-nez v0, :cond_0

    new-instance v0, Lorg/apache/tools/ant/types/Path;

    invoke-virtual {p0}, Lorg/apache/tools/ant/types/resources/JavaResource;->getProject()Lorg/apache/tools/ant/Project;

    move-result-object v1

    invoke-direct {v0, v1}, Lorg/apache/tools/ant/types/Path;-><init>(Lorg/apache/tools/ant/Project;)V

    iput-object v0, p0, Lorg/apache/tools/ant/types/resources/JavaResource;->classpath:Lorg/apache/tools/ant/types/Path;

    :cond_0
    iget-object v0, p0, Lorg/apache/tools/ant/types/resources/JavaResource;->classpath:Lorg/apache/tools/ant/types/Path;

    invoke-virtual {v0}, Lorg/apache/tools/ant/types/Path;->createPath()Lorg/apache/tools/ant/types/Path;

    move-result-object v0

    return-object v0
.end method

.method public getClasspath()Lorg/apache/tools/ant/types/Path;
    .locals 1

    invoke-virtual {p0}, Lorg/apache/tools/ant/types/resources/JavaResource;->isReference()Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-virtual {p0}, Lorg/apache/tools/ant/types/resources/JavaResource;->getCheckedRef()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lorg/apache/tools/ant/types/resources/JavaResource;

    invoke-virtual {v0}, Lorg/apache/tools/ant/types/resources/JavaResource;->getClasspath()Lorg/apache/tools/ant/types/Path;

    move-result-object v0

    :goto_0
    return-object v0

    :cond_0
    iget-object v0, p0, Lorg/apache/tools/ant/types/resources/JavaResource;->classpath:Lorg/apache/tools/ant/types/Path;

    goto :goto_0
.end method

.method public getInputStream()Ljava/io/InputStream;
    .locals 3
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    invoke-virtual {p0}, Lorg/apache/tools/ant/types/resources/JavaResource;->isReference()Z

    move-result v1

    if-eqz v1, :cond_0

    invoke-virtual {p0}, Lorg/apache/tools/ant/types/resources/JavaResource;->getCheckedRef()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lorg/apache/tools/ant/types/Resource;

    invoke-virtual {v1}, Lorg/apache/tools/ant/types/Resource;->getInputStream()Ljava/io/InputStream;

    move-result-object v1

    :goto_0
    return-object v1

    :cond_0
    const/4 v0, 0x0

    iget-object v1, p0, Lorg/apache/tools/ant/types/resources/JavaResource;->loader:Lorg/apache/tools/ant/types/Reference;

    if-eqz v1, :cond_1

    iget-object v1, p0, Lorg/apache/tools/ant/types/resources/JavaResource;->loader:Lorg/apache/tools/ant/types/Reference;

    invoke-virtual {v1}, Lorg/apache/tools/ant/types/Reference;->getReferencedObject()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/ClassLoader;

    :cond_1
    if-nez v0, :cond_2

    invoke-virtual {p0}, Lorg/apache/tools/ant/types/resources/JavaResource;->getClasspath()Lorg/apache/tools/ant/types/Path;

    move-result-object v1

    if-eqz v1, :cond_3

    invoke-virtual {p0}, Lorg/apache/tools/ant/types/resources/JavaResource;->getProject()Lorg/apache/tools/ant/Project;

    move-result-object v1

    iget-object v2, p0, Lorg/apache/tools/ant/types/resources/JavaResource;->classpath:Lorg/apache/tools/ant/types/Path;

    invoke-virtual {v1, v2}, Lorg/apache/tools/ant/Project;->createClassLoader(Lorg/apache/tools/ant/types/Path;)Lorg/apache/tools/ant/AntClassLoader;

    move-result-object v0

    :goto_1
    iget-object v1, p0, Lorg/apache/tools/ant/types/resources/JavaResource;->loader:Lorg/apache/tools/ant/types/Reference;

    if-eqz v1, :cond_2

    if-eqz v0, :cond_2

    invoke-virtual {p0}, Lorg/apache/tools/ant/types/resources/JavaResource;->getProject()Lorg/apache/tools/ant/Project;

    move-result-object v1

    iget-object v2, p0, Lorg/apache/tools/ant/types/resources/JavaResource;->loader:Lorg/apache/tools/ant/types/Reference;

    invoke-virtual {v2}, Lorg/apache/tools/ant/types/Reference;->getRefId()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2, v0}, Lorg/apache/tools/ant/Project;->addReference(Ljava/lang/String;Ljava/lang/Object;)V

    :cond_2
    if-nez v0, :cond_5

    invoke-virtual {p0}, Lorg/apache/tools/ant/types/resources/JavaResource;->getName()Ljava/lang/String;

    move-result-object v1

    invoke-static {v1}, Ljava/lang/ClassLoader;->getSystemResourceAsStream(Ljava/lang/String;)Ljava/io/InputStream;

    move-result-object v1

    goto :goto_0

    :cond_3
    sget-object v1, Lorg/apache/tools/ant/types/resources/JavaResource;->class$org$apache$tools$ant$types$resources$JavaResource:Ljava/lang/Class;

    if-nez v1, :cond_4

    const-string v1, "org.apache.tools.ant.types.resources.JavaResource"

    invoke-static {v1}, Lorg/apache/tools/ant/types/resources/JavaResource;->class$(Ljava/lang/String;)Ljava/lang/Class;

    move-result-object v1

    sput-object v1, Lorg/apache/tools/ant/types/resources/JavaResource;->class$org$apache$tools$ant$types$resources$JavaResource:Ljava/lang/Class;

    :goto_2
    invoke-virtual {v1}, Ljava/lang/Class;->getClassLoader()Ljava/lang/ClassLoader;

    move-result-object v0

    goto :goto_1

    :cond_4
    sget-object v1, Lorg/apache/tools/ant/types/resources/JavaResource;->class$org$apache$tools$ant$types$resources$JavaResource:Ljava/lang/Class;

    goto :goto_2

    :cond_5
    invoke-virtual {p0}, Lorg/apache/tools/ant/types/resources/JavaResource;->getName()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/ClassLoader;->getResourceAsStream(Ljava/lang/String;)Ljava/io/InputStream;

    move-result-object v1

    goto :goto_0
.end method

.method public isExists()Z
    .locals 4

    const/4 v3, 0x0

    const/4 v1, 0x0

    :try_start_0
    invoke-virtual {p0}, Lorg/apache/tools/ant/types/resources/JavaResource;->isReference()Z

    move-result v2

    if-eqz v2, :cond_0

    invoke-virtual {p0}, Lorg/apache/tools/ant/types/resources/JavaResource;->getCheckedRef()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lorg/apache/tools/ant/types/Resource;

    invoke-virtual {v2}, Lorg/apache/tools/ant/types/Resource;->isExists()Z
    :try_end_0
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    move-result v2

    :goto_0
    invoke-static {v1}, Lorg/apache/tools/ant/util/FileUtils;->close(Ljava/io/InputStream;)V

    :goto_1
    return v2

    :cond_0
    :try_start_1
    invoke-virtual {p0}, Lorg/apache/tools/ant/types/resources/JavaResource;->getInputStream()Ljava/io/InputStream;
    :try_end_1
    .catch Ljava/io/IOException; {:try_start_1 .. :try_end_1} :catch_0
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    move-result-object v1

    if-eqz v1, :cond_1

    const/4 v2, 0x1

    goto :goto_0

    :cond_1
    move v2, v3

    goto :goto_0

    :catch_0
    move-exception v0

    invoke-static {v1}, Lorg/apache/tools/ant/util/FileUtils;->close(Ljava/io/InputStream;)V

    move v2, v3

    goto :goto_1

    :catchall_0
    move-exception v2

    invoke-static {v1}, Lorg/apache/tools/ant/util/FileUtils;->close(Ljava/io/InputStream;)V

    throw v2
.end method

.method public setClasspath(Lorg/apache/tools/ant/types/Path;)V
    .locals 1
    .param p1    # Lorg/apache/tools/ant/types/Path;

    invoke-virtual {p0}, Lorg/apache/tools/ant/types/resources/JavaResource;->checkAttributesAllowed()V

    iget-object v0, p0, Lorg/apache/tools/ant/types/resources/JavaResource;->classpath:Lorg/apache/tools/ant/types/Path;

    if-nez v0, :cond_0

    iput-object p1, p0, Lorg/apache/tools/ant/types/resources/JavaResource;->classpath:Lorg/apache/tools/ant/types/Path;

    :goto_0
    return-void

    :cond_0
    iget-object v0, p0, Lorg/apache/tools/ant/types/resources/JavaResource;->classpath:Lorg/apache/tools/ant/types/Path;

    invoke-virtual {v0, p1}, Lorg/apache/tools/ant/types/Path;->append(Lorg/apache/tools/ant/types/Path;)V

    goto :goto_0
.end method

.method public setClasspathRef(Lorg/apache/tools/ant/types/Reference;)V
    .locals 1
    .param p1    # Lorg/apache/tools/ant/types/Reference;

    invoke-virtual {p0}, Lorg/apache/tools/ant/types/resources/JavaResource;->checkAttributesAllowed()V

    invoke-virtual {p0}, Lorg/apache/tools/ant/types/resources/JavaResource;->createClasspath()Lorg/apache/tools/ant/types/Path;

    move-result-object v0

    invoke-virtual {v0, p1}, Lorg/apache/tools/ant/types/Path;->setRefid(Lorg/apache/tools/ant/types/Reference;)V

    return-void
.end method

.method public setLoaderRef(Lorg/apache/tools/ant/types/Reference;)V
    .locals 0
    .param p1    # Lorg/apache/tools/ant/types/Reference;

    invoke-virtual {p0}, Lorg/apache/tools/ant/types/resources/JavaResource;->checkAttributesAllowed()V

    iput-object p1, p0, Lorg/apache/tools/ant/types/resources/JavaResource;->loader:Lorg/apache/tools/ant/types/Reference;

    return-void
.end method

.method public setRefid(Lorg/apache/tools/ant/types/Reference;)V
    .locals 1
    .param p1    # Lorg/apache/tools/ant/types/Reference;

    iget-object v0, p0, Lorg/apache/tools/ant/types/resources/JavaResource;->loader:Lorg/apache/tools/ant/types/Reference;

    if-nez v0, :cond_0

    iget-object v0, p0, Lorg/apache/tools/ant/types/resources/JavaResource;->classpath:Lorg/apache/tools/ant/types/Path;

    if-eqz v0, :cond_1

    :cond_0
    invoke-virtual {p0}, Lorg/apache/tools/ant/types/resources/JavaResource;->tooManyAttributes()Lorg/apache/tools/ant/BuildException;

    move-result-object v0

    throw v0

    :cond_1
    invoke-super {p0, p1}, Lorg/apache/tools/ant/types/Resource;->setRefid(Lorg/apache/tools/ant/types/Reference;)V

    return-void
.end method
