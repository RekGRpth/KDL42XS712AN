.class public Lorg/apache/tools/ant/types/resources/First;
.super Lorg/apache/tools/ant/types/resources/BaseResourceCollectionWrapper;
.source "First.java"


# static fields
.field private static final BAD_COUNT:Ljava/lang/String; = "count of first resources should be set to an int >= 0"


# instance fields
.field private count:I


# direct methods
.method public constructor <init>()V
    .locals 1

    invoke-direct {p0}, Lorg/apache/tools/ant/types/resources/BaseResourceCollectionWrapper;-><init>()V

    const/4 v0, 0x1

    iput v0, p0, Lorg/apache/tools/ant/types/resources/First;->count:I

    return-void
.end method


# virtual methods
.method protected getCollection()Ljava/util/Collection;
    .locals 6

    invoke-virtual {p0}, Lorg/apache/tools/ant/types/resources/First;->getCount()I

    move-result v1

    if-gez v1, :cond_0

    new-instance v4, Lorg/apache/tools/ant/BuildException;

    const-string v5, "count of first resources should be set to an int >= 0"

    invoke-direct {v4, v5}, Lorg/apache/tools/ant/BuildException;-><init>(Ljava/lang/String;)V

    throw v4

    :cond_0
    invoke-virtual {p0}, Lorg/apache/tools/ant/types/resources/First;->getResourceCollection()Lorg/apache/tools/ant/types/ResourceCollection;

    move-result-object v4

    invoke-interface {v4}, Lorg/apache/tools/ant/types/ResourceCollection;->iterator()Ljava/util/Iterator;

    move-result-object v3

    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0, v1}, Ljava/util/ArrayList;-><init>(I)V

    const/4 v2, 0x0

    :goto_0
    if-ge v2, v1, :cond_1

    invoke-interface {v3}, Ljava/util/Iterator;->hasNext()Z

    move-result v4

    if-eqz v4, :cond_1

    invoke-interface {v3}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v4

    invoke-virtual {v0, v4}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    add-int/lit8 v2, v2, 0x1

    goto :goto_0

    :cond_1
    return-object v0
.end method

.method public declared-synchronized getCount()I
    .locals 1

    monitor-enter p0

    :try_start_0
    iget v0, p0, Lorg/apache/tools/ant/types/resources/First;->count:I
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    monitor-exit p0

    return v0

    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method public declared-synchronized setCount(I)V
    .locals 1
    .param p1    # I

    monitor-enter p0

    :try_start_0
    iput p1, p0, Lorg/apache/tools/ant/types/resources/First;->count:I
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    monitor-exit p0

    return-void

    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method
