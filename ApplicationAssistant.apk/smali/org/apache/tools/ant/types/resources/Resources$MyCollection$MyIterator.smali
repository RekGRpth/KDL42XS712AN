.class Lorg/apache/tools/ant/types/resources/Resources$MyCollection$MyIterator;
.super Ljava/lang/Object;
.source "Resources.java"

# interfaces
.implements Ljava/util/Iterator;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lorg/apache/tools/ant/types/resources/Resources$MyCollection;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x2
    name = "MyIterator"
.end annotation


# instance fields
.field private rci:Ljava/util/Iterator;

.field private ri:Ljava/util/Iterator;

.field private final this$1:Lorg/apache/tools/ant/types/resources/Resources$MyCollection;


# direct methods
.method private constructor <init>(Lorg/apache/tools/ant/types/resources/Resources$MyCollection;)V
    .locals 1

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    iput-object p1, p0, Lorg/apache/tools/ant/types/resources/Resources$MyCollection$MyIterator;->this$1:Lorg/apache/tools/ant/types/resources/Resources$MyCollection;

    iget-object v0, p0, Lorg/apache/tools/ant/types/resources/Resources$MyCollection$MyIterator;->this$1:Lorg/apache/tools/ant/types/resources/Resources$MyCollection;

    invoke-static {v0}, Lorg/apache/tools/ant/types/resources/Resources$MyCollection;->access$200(Lorg/apache/tools/ant/types/resources/Resources$MyCollection;)Lorg/apache/tools/ant/types/resources/Resources;

    move-result-object v0

    invoke-static {v0}, Lorg/apache/tools/ant/types/resources/Resources;->access$000(Lorg/apache/tools/ant/types/resources/Resources;)Ljava/util/List;

    move-result-object v0

    invoke-interface {v0}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v0

    iput-object v0, p0, Lorg/apache/tools/ant/types/resources/Resources$MyCollection$MyIterator;->rci:Ljava/util/Iterator;

    const/4 v0, 0x0

    iput-object v0, p0, Lorg/apache/tools/ant/types/resources/Resources$MyCollection$MyIterator;->ri:Ljava/util/Iterator;

    return-void
.end method

.method constructor <init>(Lorg/apache/tools/ant/types/resources/Resources$MyCollection;Lorg/apache/tools/ant/types/resources/Resources$1;)V
    .locals 0
    .param p1    # Lorg/apache/tools/ant/types/resources/Resources$MyCollection;
    .param p2    # Lorg/apache/tools/ant/types/resources/Resources$1;

    invoke-direct {p0, p1}, Lorg/apache/tools/ant/types/resources/Resources$MyCollection$MyIterator;-><init>(Lorg/apache/tools/ant/types/resources/Resources$MyCollection;)V

    return-void
.end method


# virtual methods
.method public hasNext()Z
    .locals 2

    iget-object v1, p0, Lorg/apache/tools/ant/types/resources/Resources$MyCollection$MyIterator;->ri:Ljava/util/Iterator;

    if-eqz v1, :cond_0

    iget-object v1, p0, Lorg/apache/tools/ant/types/resources/Resources$MyCollection$MyIterator;->ri:Ljava/util/Iterator;

    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v1

    if-eqz v1, :cond_0

    const/4 v0, 0x1

    :goto_0
    if-nez v0, :cond_1

    iget-object v1, p0, Lorg/apache/tools/ant/types/resources/Resources$MyCollection$MyIterator;->rci:Ljava/util/Iterator;

    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v1

    if-eqz v1, :cond_1

    iget-object v1, p0, Lorg/apache/tools/ant/types/resources/Resources$MyCollection$MyIterator;->rci:Ljava/util/Iterator;

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lorg/apache/tools/ant/types/ResourceCollection;

    invoke-interface {v1}, Lorg/apache/tools/ant/types/ResourceCollection;->iterator()Ljava/util/Iterator;

    move-result-object v1

    iput-object v1, p0, Lorg/apache/tools/ant/types/resources/Resources$MyCollection$MyIterator;->ri:Ljava/util/Iterator;

    iget-object v1, p0, Lorg/apache/tools/ant/types/resources/Resources$MyCollection$MyIterator;->ri:Ljava/util/Iterator;

    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    goto :goto_0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0

    :cond_1
    return v0
.end method

.method public next()Ljava/lang/Object;
    .locals 1

    invoke-virtual {p0}, Lorg/apache/tools/ant/types/resources/Resources$MyCollection$MyIterator;->hasNext()Z

    move-result v0

    if-nez v0, :cond_0

    new-instance v0, Ljava/util/NoSuchElementException;

    invoke-direct {v0}, Ljava/util/NoSuchElementException;-><init>()V

    throw v0

    :cond_0
    iget-object v0, p0, Lorg/apache/tools/ant/types/resources/Resources$MyCollection$MyIterator;->ri:Ljava/util/Iterator;

    invoke-interface {v0}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    return-object v0
.end method

.method public remove()V
    .locals 1

    new-instance v0, Ljava/lang/UnsupportedOperationException;

    invoke-direct {v0}, Ljava/lang/UnsupportedOperationException;-><init>()V

    throw v0
.end method
