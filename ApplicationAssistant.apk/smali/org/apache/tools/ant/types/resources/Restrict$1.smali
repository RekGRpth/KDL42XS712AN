.class Lorg/apache/tools/ant/types/resources/Restrict$1;
.super Lorg/apache/tools/ant/types/resources/BaseResourceCollectionWrapper;
.source "Restrict.java"


# instance fields
.field private final this$0:Lorg/apache/tools/ant/types/resources/Restrict;


# direct methods
.method constructor <init>(Lorg/apache/tools/ant/types/resources/Restrict;)V
    .locals 0

    invoke-direct {p0}, Lorg/apache/tools/ant/types/resources/BaseResourceCollectionWrapper;-><init>()V

    iput-object p1, p0, Lorg/apache/tools/ant/types/resources/Restrict$1;->this$0:Lorg/apache/tools/ant/types/resources/Restrict;

    return-void
.end method


# virtual methods
.method protected getCollection()Ljava/util/Collection;
    .locals 5

    new-instance v2, Ljava/util/ArrayList;

    invoke-direct {v2}, Ljava/util/ArrayList;-><init>()V

    iget-object v4, p0, Lorg/apache/tools/ant/types/resources/Restrict$1;->this$0:Lorg/apache/tools/ant/types/resources/Restrict;

    invoke-static {v4}, Lorg/apache/tools/ant/types/resources/Restrict;->access$000(Lorg/apache/tools/ant/types/resources/Restrict;)Lorg/apache/tools/ant/types/resources/BaseResourceCollectionWrapper;

    move-result-object v4

    invoke-virtual {v4}, Lorg/apache/tools/ant/types/resources/BaseResourceCollectionWrapper;->getResourceCollection()Lorg/apache/tools/ant/types/ResourceCollection;

    move-result-object v4

    invoke-interface {v4}, Lorg/apache/tools/ant/types/ResourceCollection;->iterator()Ljava/util/Iterator;

    move-result-object v3

    :goto_0
    invoke-interface {v3}, Ljava/util/Iterator;->hasNext()Z

    move-result v4

    if-eqz v4, :cond_2

    invoke-interface {v3}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lorg/apache/tools/ant/types/Resource;

    iget-object v4, p0, Lorg/apache/tools/ant/types/resources/Restrict$1;->this$0:Lorg/apache/tools/ant/types/resources/Restrict;

    invoke-virtual {v4}, Lorg/apache/tools/ant/types/resources/Restrict;->getSelectors()Ljava/util/Iterator;

    move-result-object v0

    :cond_0
    invoke-interface {v0}, Ljava/util/Iterator;->hasNext()Z

    move-result v4

    if-eqz v4, :cond_1

    invoke-interface {v0}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v4

    check-cast v4, Lorg/apache/tools/ant/types/resources/selectors/ResourceSelector;

    check-cast v4, Lorg/apache/tools/ant/types/resources/selectors/ResourceSelector;

    invoke-interface {v4, v1}, Lorg/apache/tools/ant/types/resources/selectors/ResourceSelector;->isSelected(Lorg/apache/tools/ant/types/Resource;)Z

    move-result v4

    if-nez v4, :cond_0

    goto :goto_0

    :cond_1
    invoke-virtual {v2, v1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    goto :goto_0

    :cond_2
    return-object v2
.end method
