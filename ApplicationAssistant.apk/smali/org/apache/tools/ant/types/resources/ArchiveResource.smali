.class public abstract Lorg/apache/tools/ant/types/resources/ArchiveResource;
.super Lorg/apache/tools/ant/types/Resource;
.source "ArchiveResource.java"


# static fields
.field private static final NULL_ARCHIVE:I


# instance fields
.field private archive:Lorg/apache/tools/ant/types/Resource;

.field private haveEntry:Z

.field private mode:I

.field private modeSet:Z


# direct methods
.method static constructor <clinit>()V
    .locals 1

    const-string v0, "null archive"

    invoke-virtual {v0}, Ljava/lang/String;->getBytes()[B

    move-result-object v0

    invoke-static {v0}, Lorg/apache/tools/ant/types/Resource;->getMagicNumber([B)I

    move-result v0

    sput v0, Lorg/apache/tools/ant/types/resources/ArchiveResource;->NULL_ARCHIVE:I

    return-void
.end method

.method public constructor <init>()V
    .locals 1

    const/4 v0, 0x0

    invoke-direct {p0}, Lorg/apache/tools/ant/types/Resource;-><init>()V

    iput-boolean v0, p0, Lorg/apache/tools/ant/types/resources/ArchiveResource;->haveEntry:Z

    iput-boolean v0, p0, Lorg/apache/tools/ant/types/resources/ArchiveResource;->modeSet:Z

    iput v0, p0, Lorg/apache/tools/ant/types/resources/ArchiveResource;->mode:I

    return-void
.end method

.method public constructor <init>(Ljava/io/File;)V
    .locals 1
    .param p1    # Ljava/io/File;

    const/4 v0, 0x0

    invoke-direct {p0, p1, v0}, Lorg/apache/tools/ant/types/resources/ArchiveResource;-><init>(Ljava/io/File;Z)V

    return-void
.end method

.method public constructor <init>(Ljava/io/File;Z)V
    .locals 1
    .param p1    # Ljava/io/File;
    .param p2    # Z

    const/4 v0, 0x0

    invoke-direct {p0}, Lorg/apache/tools/ant/types/Resource;-><init>()V

    iput-boolean v0, p0, Lorg/apache/tools/ant/types/resources/ArchiveResource;->haveEntry:Z

    iput-boolean v0, p0, Lorg/apache/tools/ant/types/resources/ArchiveResource;->modeSet:Z

    iput v0, p0, Lorg/apache/tools/ant/types/resources/ArchiveResource;->mode:I

    invoke-virtual {p0, p1}, Lorg/apache/tools/ant/types/resources/ArchiveResource;->setArchive(Ljava/io/File;)V

    iput-boolean p2, p0, Lorg/apache/tools/ant/types/resources/ArchiveResource;->haveEntry:Z

    return-void
.end method

.method public constructor <init>(Lorg/apache/tools/ant/types/Resource;Z)V
    .locals 1
    .param p1    # Lorg/apache/tools/ant/types/Resource;
    .param p2    # Z

    const/4 v0, 0x0

    invoke-direct {p0}, Lorg/apache/tools/ant/types/Resource;-><init>()V

    iput-boolean v0, p0, Lorg/apache/tools/ant/types/resources/ArchiveResource;->haveEntry:Z

    iput-boolean v0, p0, Lorg/apache/tools/ant/types/resources/ArchiveResource;->modeSet:Z

    iput v0, p0, Lorg/apache/tools/ant/types/resources/ArchiveResource;->mode:I

    invoke-virtual {p0, p1}, Lorg/apache/tools/ant/types/resources/ArchiveResource;->addConfigured(Lorg/apache/tools/ant/types/ResourceCollection;)V

    iput-boolean p2, p0, Lorg/apache/tools/ant/types/resources/ArchiveResource;->haveEntry:Z

    return-void
.end method

.method private declared-synchronized checkEntry()V
    .locals 5
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lorg/apache/tools/ant/BuildException;
        }
    .end annotation

    monitor-enter p0

    :try_start_0
    iget-boolean v2, p0, Lorg/apache/tools/ant/types/resources/ArchiveResource;->haveEntry:Z
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    if-eqz v2, :cond_0

    :goto_0
    monitor-exit p0

    return-void

    :cond_0
    :try_start_1
    invoke-virtual {p0}, Lorg/apache/tools/ant/types/resources/ArchiveResource;->getName()Ljava/lang/String;

    move-result-object v0

    if-nez v0, :cond_1

    new-instance v2, Lorg/apache/tools/ant/BuildException;

    const-string v3, "entry name not set"

    invoke-direct {v2, v3}, Lorg/apache/tools/ant/BuildException;-><init>(Ljava/lang/String;)V

    throw v2
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    :catchall_0
    move-exception v2

    monitor-exit p0

    throw v2

    :cond_1
    :try_start_2
    invoke-virtual {p0}, Lorg/apache/tools/ant/types/resources/ArchiveResource;->getArchive()Lorg/apache/tools/ant/types/Resource;

    move-result-object v1

    if-nez v1, :cond_2

    new-instance v2, Lorg/apache/tools/ant/BuildException;

    const-string v3, "archive attribute not set"

    invoke-direct {v2, v3}, Lorg/apache/tools/ant/BuildException;-><init>(Ljava/lang/String;)V

    throw v2

    :cond_2
    invoke-virtual {v1}, Lorg/apache/tools/ant/types/Resource;->isExists()Z

    move-result v2

    if-nez v2, :cond_3

    new-instance v2, Lorg/apache/tools/ant/BuildException;

    new-instance v3, Ljava/lang/StringBuffer;

    invoke-direct {v3}, Ljava/lang/StringBuffer;-><init>()V

    invoke-virtual {v1}, Lorg/apache/tools/ant/types/Resource;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v3, v4}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    move-result-object v3

    const-string v4, " does not exist."

    invoke-virtual {v3, v4}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuffer;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-direct {v2, v3}, Lorg/apache/tools/ant/BuildException;-><init>(Ljava/lang/String;)V

    throw v2

    :cond_3
    invoke-virtual {v1}, Lorg/apache/tools/ant/types/Resource;->isDirectory()Z

    move-result v2

    if-eqz v2, :cond_4

    new-instance v2, Lorg/apache/tools/ant/BuildException;

    new-instance v3, Ljava/lang/StringBuffer;

    invoke-direct {v3}, Ljava/lang/StringBuffer;-><init>()V

    invoke-virtual {v3, v1}, Ljava/lang/StringBuffer;->append(Ljava/lang/Object;)Ljava/lang/StringBuffer;

    move-result-object v3

    const-string v4, " denotes a directory."

    invoke-virtual {v3, v4}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuffer;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-direct {v2, v3}, Lorg/apache/tools/ant/BuildException;-><init>(Ljava/lang/String;)V

    throw v2

    :cond_4
    invoke-virtual {p0}, Lorg/apache/tools/ant/types/resources/ArchiveResource;->fetchEntry()V

    const/4 v2, 0x1

    iput-boolean v2, p0, Lorg/apache/tools/ant/types/resources/ArchiveResource;->haveEntry:Z
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    goto :goto_0
.end method


# virtual methods
.method public addConfigured(Lorg/apache/tools/ant/types/ResourceCollection;)V
    .locals 2
    .param p1    # Lorg/apache/tools/ant/types/ResourceCollection;

    invoke-virtual {p0}, Lorg/apache/tools/ant/types/resources/ArchiveResource;->checkChildrenAllowed()V

    iget-object v0, p0, Lorg/apache/tools/ant/types/resources/ArchiveResource;->archive:Lorg/apache/tools/ant/types/Resource;

    if-eqz v0, :cond_0

    new-instance v0, Lorg/apache/tools/ant/BuildException;

    const-string v1, "you must not specify more than one archive"

    invoke-direct {v0, v1}, Lorg/apache/tools/ant/BuildException;-><init>(Ljava/lang/String;)V

    throw v0

    :cond_0
    invoke-interface {p1}, Lorg/apache/tools/ant/types/ResourceCollection;->size()I

    move-result v0

    const/4 v1, 0x1

    if-eq v0, v1, :cond_1

    new-instance v0, Lorg/apache/tools/ant/BuildException;

    const-string v1, "only single argument resource collections are supported as archives"

    invoke-direct {v0, v1}, Lorg/apache/tools/ant/BuildException;-><init>(Ljava/lang/String;)V

    throw v0

    :cond_1
    invoke-interface {p1}, Lorg/apache/tools/ant/types/ResourceCollection;->iterator()Ljava/util/Iterator;

    move-result-object v0

    invoke-interface {v0}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lorg/apache/tools/ant/types/Resource;

    iput-object v0, p0, Lorg/apache/tools/ant/types/resources/ArchiveResource;->archive:Lorg/apache/tools/ant/types/Resource;

    return-void
.end method

.method public compareTo(Ljava/lang/Object;)I
    .locals 1
    .param p1    # Ljava/lang/Object;

    invoke-virtual {p0, p1}, Lorg/apache/tools/ant/types/resources/ArchiveResource;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    const/4 v0, 0x0

    :goto_0
    return v0

    :cond_0
    invoke-super {p0, p1}, Lorg/apache/tools/ant/types/Resource;->compareTo(Ljava/lang/Object;)I

    move-result v0

    goto :goto_0
.end method

.method public equals(Ljava/lang/Object;)Z
    .locals 5
    .param p1    # Ljava/lang/Object;

    const/4 v1, 0x1

    const/4 v2, 0x0

    if-ne p0, p1, :cond_1

    :cond_0
    :goto_0
    return v1

    :cond_1
    invoke-virtual {p0}, Lorg/apache/tools/ant/types/resources/ArchiveResource;->isReference()Z

    move-result v3

    if-eqz v3, :cond_2

    invoke-virtual {p0}, Lorg/apache/tools/ant/types/resources/ArchiveResource;->getCheckedRef()Ljava/lang/Object;

    move-result-object v1

    invoke-virtual {v1, p1}, Ljava/lang/Object;->equals(Ljava/lang/Object;)Z

    move-result v1

    goto :goto_0

    :cond_2
    invoke-virtual {p1}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v3

    invoke-virtual {p0}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v4

    invoke-virtual {v3, v4}, Ljava/lang/Object;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-nez v3, :cond_3

    move v1, v2

    goto :goto_0

    :cond_3
    move-object v0, p1

    check-cast v0, Lorg/apache/tools/ant/types/resources/ArchiveResource;

    invoke-virtual {p0}, Lorg/apache/tools/ant/types/resources/ArchiveResource;->getArchive()Lorg/apache/tools/ant/types/Resource;

    move-result-object v3

    invoke-virtual {v0}, Lorg/apache/tools/ant/types/resources/ArchiveResource;->getArchive()Lorg/apache/tools/ant/types/Resource;

    move-result-object v4

    invoke-virtual {v3, v4}, Lorg/apache/tools/ant/types/Resource;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-eqz v3, :cond_4

    invoke-virtual {p0}, Lorg/apache/tools/ant/types/resources/ArchiveResource;->getName()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v0}, Lorg/apache/tools/ant/types/resources/ArchiveResource;->getName()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v3, v4}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-nez v3, :cond_0

    :cond_4
    move v1, v2

    goto :goto_0
.end method

.method protected abstract fetchEntry()V
.end method

.method public getArchive()Lorg/apache/tools/ant/types/Resource;
    .locals 1

    invoke-virtual {p0}, Lorg/apache/tools/ant/types/resources/ArchiveResource;->isReference()Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-virtual {p0}, Lorg/apache/tools/ant/types/resources/ArchiveResource;->getCheckedRef()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lorg/apache/tools/ant/types/resources/ArchiveResource;

    invoke-virtual {v0}, Lorg/apache/tools/ant/types/resources/ArchiveResource;->getArchive()Lorg/apache/tools/ant/types/Resource;

    move-result-object v0

    :goto_0
    return-object v0

    :cond_0
    iget-object v0, p0, Lorg/apache/tools/ant/types/resources/ArchiveResource;->archive:Lorg/apache/tools/ant/types/Resource;

    goto :goto_0
.end method

.method public getLastModified()J
    .locals 2

    invoke-virtual {p0}, Lorg/apache/tools/ant/types/resources/ArchiveResource;->isReference()Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-virtual {p0}, Lorg/apache/tools/ant/types/resources/ArchiveResource;->getCheckedRef()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lorg/apache/tools/ant/types/Resource;

    invoke-virtual {v0}, Lorg/apache/tools/ant/types/Resource;->getLastModified()J

    move-result-wide v0

    :goto_0
    return-wide v0

    :cond_0
    invoke-direct {p0}, Lorg/apache/tools/ant/types/resources/ArchiveResource;->checkEntry()V

    invoke-super {p0}, Lorg/apache/tools/ant/types/Resource;->getLastModified()J

    move-result-wide v0

    goto :goto_0
.end method

.method public getMode()I
    .locals 1

    invoke-virtual {p0}, Lorg/apache/tools/ant/types/resources/ArchiveResource;->isReference()Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-virtual {p0}, Lorg/apache/tools/ant/types/resources/ArchiveResource;->getCheckedRef()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lorg/apache/tools/ant/types/resources/ArchiveResource;

    invoke-virtual {v0}, Lorg/apache/tools/ant/types/resources/ArchiveResource;->getMode()I

    move-result v0

    :goto_0
    return v0

    :cond_0
    invoke-direct {p0}, Lorg/apache/tools/ant/types/resources/ArchiveResource;->checkEntry()V

    iget v0, p0, Lorg/apache/tools/ant/types/resources/ArchiveResource;->mode:I

    goto :goto_0
.end method

.method public getSize()J
    .locals 2

    invoke-virtual {p0}, Lorg/apache/tools/ant/types/resources/ArchiveResource;->isReference()Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-virtual {p0}, Lorg/apache/tools/ant/types/resources/ArchiveResource;->getCheckedRef()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lorg/apache/tools/ant/types/Resource;

    invoke-virtual {v0}, Lorg/apache/tools/ant/types/Resource;->getSize()J

    move-result-wide v0

    :goto_0
    return-wide v0

    :cond_0
    invoke-direct {p0}, Lorg/apache/tools/ant/types/resources/ArchiveResource;->checkEntry()V

    invoke-super {p0}, Lorg/apache/tools/ant/types/Resource;->getSize()J

    move-result-wide v0

    goto :goto_0
.end method

.method public hashCode()I
    .locals 2

    invoke-super {p0}, Lorg/apache/tools/ant/types/Resource;->hashCode()I

    move-result v1

    invoke-virtual {p0}, Lorg/apache/tools/ant/types/resources/ArchiveResource;->getArchive()Lorg/apache/tools/ant/types/Resource;

    move-result-object v0

    if-nez v0, :cond_0

    sget v0, Lorg/apache/tools/ant/types/resources/ArchiveResource;->NULL_ARCHIVE:I

    :goto_0
    mul-int/2addr v0, v1

    return v0

    :cond_0
    invoke-virtual {p0}, Lorg/apache/tools/ant/types/resources/ArchiveResource;->getArchive()Lorg/apache/tools/ant/types/Resource;

    move-result-object v0

    invoke-virtual {v0}, Lorg/apache/tools/ant/types/Resource;->hashCode()I

    move-result v0

    goto :goto_0
.end method

.method public isDirectory()Z
    .locals 1

    invoke-virtual {p0}, Lorg/apache/tools/ant/types/resources/ArchiveResource;->isReference()Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-virtual {p0}, Lorg/apache/tools/ant/types/resources/ArchiveResource;->getCheckedRef()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lorg/apache/tools/ant/types/Resource;

    invoke-virtual {v0}, Lorg/apache/tools/ant/types/Resource;->isDirectory()Z

    move-result v0

    :goto_0
    return v0

    :cond_0
    invoke-direct {p0}, Lorg/apache/tools/ant/types/resources/ArchiveResource;->checkEntry()V

    invoke-super {p0}, Lorg/apache/tools/ant/types/Resource;->isDirectory()Z

    move-result v0

    goto :goto_0
.end method

.method public isExists()Z
    .locals 1

    invoke-virtual {p0}, Lorg/apache/tools/ant/types/resources/ArchiveResource;->isReference()Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-virtual {p0}, Lorg/apache/tools/ant/types/resources/ArchiveResource;->getCheckedRef()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lorg/apache/tools/ant/types/Resource;

    invoke-virtual {v0}, Lorg/apache/tools/ant/types/Resource;->isExists()Z

    move-result v0

    :goto_0
    return v0

    :cond_0
    invoke-direct {p0}, Lorg/apache/tools/ant/types/resources/ArchiveResource;->checkEntry()V

    invoke-super {p0}, Lorg/apache/tools/ant/types/Resource;->isExists()Z

    move-result v0

    goto :goto_0
.end method

.method public setArchive(Ljava/io/File;)V
    .locals 1
    .param p1    # Ljava/io/File;

    invoke-virtual {p0}, Lorg/apache/tools/ant/types/resources/ArchiveResource;->checkAttributesAllowed()V

    new-instance v0, Lorg/apache/tools/ant/types/resources/FileResource;

    invoke-direct {v0, p1}, Lorg/apache/tools/ant/types/resources/FileResource;-><init>(Ljava/io/File;)V

    iput-object v0, p0, Lorg/apache/tools/ant/types/resources/ArchiveResource;->archive:Lorg/apache/tools/ant/types/Resource;

    return-void
.end method

.method public setMode(I)V
    .locals 1
    .param p1    # I

    invoke-virtual {p0}, Lorg/apache/tools/ant/types/resources/ArchiveResource;->checkAttributesAllowed()V

    iput p1, p0, Lorg/apache/tools/ant/types/resources/ArchiveResource;->mode:I

    const/4 v0, 0x1

    iput-boolean v0, p0, Lorg/apache/tools/ant/types/resources/ArchiveResource;->modeSet:Z

    return-void
.end method

.method public setRefid(Lorg/apache/tools/ant/types/Reference;)V
    .locals 1
    .param p1    # Lorg/apache/tools/ant/types/Reference;

    iget-object v0, p0, Lorg/apache/tools/ant/types/resources/ArchiveResource;->archive:Lorg/apache/tools/ant/types/Resource;

    if-nez v0, :cond_0

    iget-boolean v0, p0, Lorg/apache/tools/ant/types/resources/ArchiveResource;->modeSet:Z

    if-eqz v0, :cond_1

    :cond_0
    invoke-virtual {p0}, Lorg/apache/tools/ant/types/resources/ArchiveResource;->tooManyAttributes()Lorg/apache/tools/ant/BuildException;

    move-result-object v0

    throw v0

    :cond_1
    invoke-super {p0, p1}, Lorg/apache/tools/ant/types/Resource;->setRefid(Lorg/apache/tools/ant/types/Reference;)V

    return-void
.end method

.method public toString()Ljava/lang/String;
    .locals 2

    invoke-virtual {p0}, Lorg/apache/tools/ant/types/resources/ArchiveResource;->isReference()Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-virtual {p0}, Lorg/apache/tools/ant/types/resources/ArchiveResource;->getCheckedRef()Ljava/lang/Object;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object v0

    :goto_0
    return-object v0

    :cond_0
    new-instance v0, Ljava/lang/StringBuffer;

    invoke-direct {v0}, Ljava/lang/StringBuffer;-><init>()V

    invoke-virtual {p0}, Lorg/apache/tools/ant/types/resources/ArchiveResource;->getArchive()Lorg/apache/tools/ant/types/Resource;

    move-result-object v1

    invoke-virtual {v1}, Lorg/apache/tools/ant/types/Resource;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    move-result-object v0

    const/16 v1, 0x3a

    invoke-virtual {v0, v1}, Ljava/lang/StringBuffer;->append(C)Ljava/lang/StringBuffer;

    move-result-object v0

    invoke-virtual {p0}, Lorg/apache/tools/ant/types/resources/ArchiveResource;->getName()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuffer;->toString()Ljava/lang/String;

    move-result-object v0

    goto :goto_0
.end method
