.class public abstract Lorg/apache/tools/ant/types/resources/CompressedResource;
.super Lorg/apache/tools/ant/types/Resource;
.source "CompressedResource.java"


# instance fields
.field private resource:Lorg/apache/tools/ant/types/Resource;


# direct methods
.method public constructor <init>()V
    .locals 0

    invoke-direct {p0}, Lorg/apache/tools/ant/types/Resource;-><init>()V

    return-void
.end method

.method public constructor <init>(Lorg/apache/tools/ant/types/ResourceCollection;)V
    .locals 0
    .param p1    # Lorg/apache/tools/ant/types/ResourceCollection;

    invoke-direct {p0}, Lorg/apache/tools/ant/types/Resource;-><init>()V

    invoke-virtual {p0, p1}, Lorg/apache/tools/ant/types/resources/CompressedResource;->addConfigured(Lorg/apache/tools/ant/types/ResourceCollection;)V

    return-void
.end method

.method private getResource()Lorg/apache/tools/ant/types/Resource;
    .locals 2

    invoke-virtual {p0}, Lorg/apache/tools/ant/types/resources/CompressedResource;->isReference()Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-virtual {p0}, Lorg/apache/tools/ant/types/resources/CompressedResource;->getCheckedRef()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lorg/apache/tools/ant/types/Resource;

    :goto_0
    return-object v0

    :cond_0
    iget-object v0, p0, Lorg/apache/tools/ant/types/resources/CompressedResource;->resource:Lorg/apache/tools/ant/types/Resource;

    if-nez v0, :cond_1

    new-instance v0, Lorg/apache/tools/ant/BuildException;

    const-string v1, "no resource specified"

    invoke-direct {v0, v1}, Lorg/apache/tools/ant/BuildException;-><init>(Ljava/lang/String;)V

    throw v0

    :cond_1
    iget-object v0, p0, Lorg/apache/tools/ant/types/resources/CompressedResource;->resource:Lorg/apache/tools/ant/types/Resource;

    goto :goto_0
.end method


# virtual methods
.method public addConfigured(Lorg/apache/tools/ant/types/ResourceCollection;)V
    .locals 2
    .param p1    # Lorg/apache/tools/ant/types/ResourceCollection;

    invoke-virtual {p0}, Lorg/apache/tools/ant/types/resources/CompressedResource;->checkChildrenAllowed()V

    iget-object v0, p0, Lorg/apache/tools/ant/types/resources/CompressedResource;->resource:Lorg/apache/tools/ant/types/Resource;

    if-eqz v0, :cond_0

    new-instance v0, Lorg/apache/tools/ant/BuildException;

    const-string v1, "you must not specify more than one resource"

    invoke-direct {v0, v1}, Lorg/apache/tools/ant/BuildException;-><init>(Ljava/lang/String;)V

    throw v0

    :cond_0
    invoke-interface {p1}, Lorg/apache/tools/ant/types/ResourceCollection;->size()I

    move-result v0

    const/4 v1, 0x1

    if-eq v0, v1, :cond_1

    new-instance v0, Lorg/apache/tools/ant/BuildException;

    const-string v1, "only single argument resource collections are supported"

    invoke-direct {v0, v1}, Lorg/apache/tools/ant/BuildException;-><init>(Ljava/lang/String;)V

    throw v0

    :cond_1
    invoke-interface {p1}, Lorg/apache/tools/ant/types/ResourceCollection;->iterator()Ljava/util/Iterator;

    move-result-object v0

    invoke-interface {v0}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lorg/apache/tools/ant/types/Resource;

    iput-object v0, p0, Lorg/apache/tools/ant/types/resources/CompressedResource;->resource:Lorg/apache/tools/ant/types/Resource;

    return-void
.end method

.method public compareTo(Ljava/lang/Object;)I
    .locals 2
    .param p1    # Ljava/lang/Object;

    if-ne p1, p0, :cond_0

    const/4 v0, 0x0

    :goto_0
    return v0

    :cond_0
    instance-of v0, p1, Lorg/apache/tools/ant/types/resources/CompressedResource;

    if-eqz v0, :cond_1

    invoke-direct {p0}, Lorg/apache/tools/ant/types/resources/CompressedResource;->getResource()Lorg/apache/tools/ant/types/Resource;

    move-result-object v0

    check-cast p1, Lorg/apache/tools/ant/types/resources/CompressedResource;

    invoke-direct {p1}, Lorg/apache/tools/ant/types/resources/CompressedResource;->getResource()Lorg/apache/tools/ant/types/Resource;

    move-result-object v1

    invoke-virtual {v0, v1}, Lorg/apache/tools/ant/types/Resource;->compareTo(Ljava/lang/Object;)I

    move-result v0

    goto :goto_0

    :cond_1
    invoke-direct {p0}, Lorg/apache/tools/ant/types/resources/CompressedResource;->getResource()Lorg/apache/tools/ant/types/Resource;

    move-result-object v0

    invoke-virtual {v0, p1}, Lorg/apache/tools/ant/types/Resource;->compareTo(Ljava/lang/Object;)I

    move-result v0

    goto :goto_0
.end method

.method protected abstract getCompressionName()Ljava/lang/String;
.end method

.method public getInputStream()Ljava/io/InputStream;
    .locals 2
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    invoke-direct {p0}, Lorg/apache/tools/ant/types/resources/CompressedResource;->getResource()Lorg/apache/tools/ant/types/Resource;

    move-result-object v1

    invoke-virtual {v1}, Lorg/apache/tools/ant/types/Resource;->getInputStream()Ljava/io/InputStream;

    move-result-object v0

    if-eqz v0, :cond_0

    invoke-virtual {p0, v0}, Lorg/apache/tools/ant/types/resources/CompressedResource;->wrapStream(Ljava/io/InputStream;)Ljava/io/InputStream;

    move-result-object v0

    :cond_0
    return-object v0
.end method

.method public getLastModified()J
    .locals 2

    invoke-direct {p0}, Lorg/apache/tools/ant/types/resources/CompressedResource;->getResource()Lorg/apache/tools/ant/types/Resource;

    move-result-object v0

    invoke-virtual {v0}, Lorg/apache/tools/ant/types/Resource;->getLastModified()J

    move-result-wide v0

    return-wide v0
.end method

.method public getName()Ljava/lang/String;
    .locals 1

    invoke-direct {p0}, Lorg/apache/tools/ant/types/resources/CompressedResource;->getResource()Lorg/apache/tools/ant/types/Resource;

    move-result-object v0

    invoke-virtual {v0}, Lorg/apache/tools/ant/types/Resource;->getName()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public getOutputStream()Ljava/io/OutputStream;
    .locals 2
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    invoke-direct {p0}, Lorg/apache/tools/ant/types/resources/CompressedResource;->getResource()Lorg/apache/tools/ant/types/Resource;

    move-result-object v1

    invoke-virtual {v1}, Lorg/apache/tools/ant/types/Resource;->getOutputStream()Ljava/io/OutputStream;

    move-result-object v0

    if-eqz v0, :cond_0

    invoke-virtual {p0, v0}, Lorg/apache/tools/ant/types/resources/CompressedResource;->wrapStream(Ljava/io/OutputStream;)Ljava/io/OutputStream;

    move-result-object v0

    :cond_0
    return-object v0
.end method

.method public getSize()J
    .locals 8

    invoke-virtual {p0}, Lorg/apache/tools/ant/types/resources/CompressedResource;->isExists()Z

    move-result v5

    if-eqz v5, :cond_1

    const/4 v2, 0x0

    :try_start_0
    invoke-virtual {p0}, Lorg/apache/tools/ant/types/resources/CompressedResource;->getInputStream()Ljava/io/InputStream;

    move-result-object v2

    const/16 v5, 0x2000

    new-array v0, v5, [B

    const/4 v4, 0x0

    :goto_0
    const/4 v5, 0x0

    array-length v6, v0

    invoke-virtual {v2, v0, v5, v6}, Ljava/io/InputStream;->read([BII)I
    :try_end_0
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    move-result v3

    if-lez v3, :cond_0

    add-int/2addr v4, v3

    goto :goto_0

    :cond_0
    int-to-long v5, v4

    invoke-static {v2}, Lorg/apache/tools/ant/util/FileUtils;->close(Ljava/io/InputStream;)V

    :goto_1
    return-wide v5

    :catch_0
    move-exception v1

    :try_start_1
    new-instance v5, Lorg/apache/tools/ant/BuildException;

    new-instance v6, Ljava/lang/StringBuffer;

    invoke-direct {v6}, Ljava/lang/StringBuffer;-><init>()V

    const-string v7, "caught exception while reading "

    invoke-virtual {v6, v7}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    move-result-object v6

    invoke-virtual {p0}, Lorg/apache/tools/ant/types/resources/CompressedResource;->getName()Ljava/lang/String;

    move-result-object v7

    invoke-virtual {v6, v7}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    move-result-object v6

    invoke-virtual {v6}, Ljava/lang/StringBuffer;->toString()Ljava/lang/String;

    move-result-object v6

    invoke-direct {v5, v6, v1}, Lorg/apache/tools/ant/BuildException;-><init>(Ljava/lang/String;Ljava/lang/Throwable;)V

    throw v5
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    :catchall_0
    move-exception v5

    invoke-static {v2}, Lorg/apache/tools/ant/util/FileUtils;->close(Ljava/io/InputStream;)V

    throw v5

    :cond_1
    const-wide/16 v5, 0x0

    goto :goto_1
.end method

.method public hashCode()I
    .locals 1

    invoke-direct {p0}, Lorg/apache/tools/ant/types/resources/CompressedResource;->getResource()Lorg/apache/tools/ant/types/Resource;

    move-result-object v0

    invoke-virtual {v0}, Lorg/apache/tools/ant/types/Resource;->hashCode()I

    move-result v0

    return v0
.end method

.method public isDirectory()Z
    .locals 1

    invoke-direct {p0}, Lorg/apache/tools/ant/types/resources/CompressedResource;->getResource()Lorg/apache/tools/ant/types/Resource;

    move-result-object v0

    invoke-virtual {v0}, Lorg/apache/tools/ant/types/Resource;->isDirectory()Z

    move-result v0

    return v0
.end method

.method public isExists()Z
    .locals 1

    invoke-direct {p0}, Lorg/apache/tools/ant/types/resources/CompressedResource;->getResource()Lorg/apache/tools/ant/types/Resource;

    move-result-object v0

    invoke-virtual {v0}, Lorg/apache/tools/ant/types/Resource;->isExists()Z

    move-result v0

    return v0
.end method

.method public isFilesystemOnly()Z
    .locals 1

    const/4 v0, 0x0

    return v0
.end method

.method public setDirectory(Z)V
    .locals 2
    .param p1    # Z
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lorg/apache/tools/ant/BuildException;
        }
    .end annotation

    new-instance v0, Lorg/apache/tools/ant/BuildException;

    const-string v1, "you can\'t change the directory state of a  compressed resource"

    invoke-direct {v0, v1}, Lorg/apache/tools/ant/BuildException;-><init>(Ljava/lang/String;)V

    throw v0
.end method

.method public setExists(Z)V
    .locals 2
    .param p1    # Z

    new-instance v0, Lorg/apache/tools/ant/BuildException;

    const-string v1, "you can\'t change the exists state of a  compressed resource"

    invoke-direct {v0, v1}, Lorg/apache/tools/ant/BuildException;-><init>(Ljava/lang/String;)V

    throw v0
.end method

.method public setLastModified(J)V
    .locals 2
    .param p1    # J
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lorg/apache/tools/ant/BuildException;
        }
    .end annotation

    new-instance v0, Lorg/apache/tools/ant/BuildException;

    const-string v1, "you can\'t change the timestamp of a  compressed resource"

    invoke-direct {v0, v1}, Lorg/apache/tools/ant/BuildException;-><init>(Ljava/lang/String;)V

    throw v0
.end method

.method public setName(Ljava/lang/String;)V
    .locals 2
    .param p1    # Ljava/lang/String;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lorg/apache/tools/ant/BuildException;
        }
    .end annotation

    new-instance v0, Lorg/apache/tools/ant/BuildException;

    const-string v1, "you can\'t change the name of a compressed resource"

    invoke-direct {v0, v1}, Lorg/apache/tools/ant/BuildException;-><init>(Ljava/lang/String;)V

    throw v0
.end method

.method public setRefid(Lorg/apache/tools/ant/types/Reference;)V
    .locals 1
    .param p1    # Lorg/apache/tools/ant/types/Reference;

    iget-object v0, p0, Lorg/apache/tools/ant/types/resources/CompressedResource;->resource:Lorg/apache/tools/ant/types/Resource;

    if-eqz v0, :cond_0

    invoke-virtual {p0}, Lorg/apache/tools/ant/types/resources/CompressedResource;->noChildrenAllowed()Lorg/apache/tools/ant/BuildException;

    move-result-object v0

    throw v0

    :cond_0
    invoke-super {p0, p1}, Lorg/apache/tools/ant/types/Resource;->setRefid(Lorg/apache/tools/ant/types/Reference;)V

    return-void
.end method

.method public setSize(J)V
    .locals 2
    .param p1    # J
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lorg/apache/tools/ant/BuildException;
        }
    .end annotation

    new-instance v0, Lorg/apache/tools/ant/BuildException;

    const-string v1, "you can\'t change the size of a  compressed resource"

    invoke-direct {v0, v1}, Lorg/apache/tools/ant/BuildException;-><init>(Ljava/lang/String;)V

    throw v0
.end method

.method public toString()Ljava/lang/String;
    .locals 2

    new-instance v0, Ljava/lang/StringBuffer;

    invoke-direct {v0}, Ljava/lang/StringBuffer;-><init>()V

    invoke-virtual {p0}, Lorg/apache/tools/ant/types/resources/CompressedResource;->getCompressionName()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    move-result-object v0

    const-string v1, " compressed "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    move-result-object v0

    invoke-direct {p0}, Lorg/apache/tools/ant/types/resources/CompressedResource;->getResource()Lorg/apache/tools/ant/types/Resource;

    move-result-object v1

    invoke-virtual {v1}, Lorg/apache/tools/ant/types/Resource;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuffer;->toString()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method protected abstract wrapStream(Ljava/io/InputStream;)Ljava/io/InputStream;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation
.end method

.method protected abstract wrapStream(Ljava/io/OutputStream;)Ljava/io/OutputStream;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation
.end method
