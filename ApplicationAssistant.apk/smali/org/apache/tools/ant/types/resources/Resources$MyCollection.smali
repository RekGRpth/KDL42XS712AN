.class Lorg/apache/tools/ant/types/resources/Resources$MyCollection;
.super Ljava/util/AbstractCollection;
.source "Resources.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lorg/apache/tools/ant/types/resources/Resources;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x2
    name = "MyCollection"
.end annotation

.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lorg/apache/tools/ant/types/resources/Resources$MyCollection$MyIterator;
    }
.end annotation


# instance fields
.field private size:I

.field private final this$0:Lorg/apache/tools/ant/types/resources/Resources;


# direct methods
.method constructor <init>(Lorg/apache/tools/ant/types/resources/Resources;)V
    .locals 3

    invoke-direct {p0}, Ljava/util/AbstractCollection;-><init>()V

    iput-object p1, p0, Lorg/apache/tools/ant/types/resources/Resources$MyCollection;->this$0:Lorg/apache/tools/ant/types/resources/Resources;

    const/4 v1, 0x0

    iput v1, p0, Lorg/apache/tools/ant/types/resources/Resources$MyCollection;->size:I

    invoke-static {p1}, Lorg/apache/tools/ant/types/resources/Resources;->access$000(Lorg/apache/tools/ant/types/resources/Resources;)Ljava/util/List;

    move-result-object v1

    invoke-interface {v1}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v0

    :goto_0
    invoke-interface {v0}, Ljava/util/Iterator;->hasNext()Z

    move-result v1

    if-eqz v1, :cond_0

    iget v2, p0, Lorg/apache/tools/ant/types/resources/Resources$MyCollection;->size:I

    invoke-interface {v0}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lorg/apache/tools/ant/types/ResourceCollection;

    invoke-interface {v1}, Lorg/apache/tools/ant/types/ResourceCollection;->size()I

    move-result v1

    add-int/2addr v1, v2

    iput v1, p0, Lorg/apache/tools/ant/types/resources/Resources$MyCollection;->size:I

    goto :goto_0

    :cond_0
    return-void
.end method

.method static access$200(Lorg/apache/tools/ant/types/resources/Resources$MyCollection;)Lorg/apache/tools/ant/types/resources/Resources;
    .locals 1
    .param p0    # Lorg/apache/tools/ant/types/resources/Resources$MyCollection;

    iget-object v0, p0, Lorg/apache/tools/ant/types/resources/Resources$MyCollection;->this$0:Lorg/apache/tools/ant/types/resources/Resources;

    return-object v0
.end method


# virtual methods
.method public iterator()Ljava/util/Iterator;
    .locals 2

    new-instance v0, Lorg/apache/tools/ant/types/resources/Resources$MyCollection$MyIterator;

    const/4 v1, 0x0

    invoke-direct {v0, p0, v1}, Lorg/apache/tools/ant/types/resources/Resources$MyCollection$MyIterator;-><init>(Lorg/apache/tools/ant/types/resources/Resources$MyCollection;Lorg/apache/tools/ant/types/resources/Resources$1;)V

    return-object v0
.end method

.method public size()I
    .locals 1

    iget v0, p0, Lorg/apache/tools/ant/types/resources/Resources$MyCollection;->size:I

    return v0
.end method
