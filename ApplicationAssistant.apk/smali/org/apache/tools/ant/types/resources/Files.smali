.class public Lorg/apache/tools/ant/types/resources/Files;
.super Lorg/apache/tools/ant/types/selectors/AbstractSelectorContainer;
.source "Files.java"

# interfaces
.implements Ljava/lang/Cloneable;
.implements Lorg/apache/tools/ant/types/ResourceCollection;


# static fields
.field private static final EMPTY_ITERATOR:Ljava/util/Iterator;


# instance fields
.field private additionalPatterns:Ljava/util/Vector;

.field private caseSensitive:Z

.field private defaultPatterns:Lorg/apache/tools/ant/types/PatternSet;

.field private ds:Lorg/apache/tools/ant/DirectoryScanner;

.field private followSymlinks:Z

.field private selectors:Ljava/util/Vector;

.field private useDefaultExcludes:Z


# direct methods
.method static constructor <clinit>()V
    .locals 1

    sget-object v0, Ljava/util/Collections;->EMPTY_SET:Ljava/util/Set;

    invoke-interface {v0}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    move-result-object v0

    sput-object v0, Lorg/apache/tools/ant/types/resources/Files;->EMPTY_ITERATOR:Ljava/util/Iterator;

    return-void
.end method

.method public constructor <init>()V
    .locals 2

    const/4 v1, 0x1

    invoke-direct {p0}, Lorg/apache/tools/ant/types/selectors/AbstractSelectorContainer;-><init>()V

    new-instance v0, Lorg/apache/tools/ant/types/PatternSet;

    invoke-direct {v0}, Lorg/apache/tools/ant/types/PatternSet;-><init>()V

    iput-object v0, p0, Lorg/apache/tools/ant/types/resources/Files;->defaultPatterns:Lorg/apache/tools/ant/types/PatternSet;

    new-instance v0, Ljava/util/Vector;

    invoke-direct {v0}, Ljava/util/Vector;-><init>()V

    iput-object v0, p0, Lorg/apache/tools/ant/types/resources/Files;->additionalPatterns:Ljava/util/Vector;

    new-instance v0, Ljava/util/Vector;

    invoke-direct {v0}, Ljava/util/Vector;-><init>()V

    iput-object v0, p0, Lorg/apache/tools/ant/types/resources/Files;->selectors:Ljava/util/Vector;

    iput-boolean v1, p0, Lorg/apache/tools/ant/types/resources/Files;->useDefaultExcludes:Z

    iput-boolean v1, p0, Lorg/apache/tools/ant/types/resources/Files;->caseSensitive:Z

    iput-boolean v1, p0, Lorg/apache/tools/ant/types/resources/Files;->followSymlinks:Z

    const/4 v0, 0x0

    iput-object v0, p0, Lorg/apache/tools/ant/types/resources/Files;->ds:Lorg/apache/tools/ant/DirectoryScanner;

    return-void
.end method

.method protected constructor <init>(Lorg/apache/tools/ant/types/resources/Files;)V
    .locals 2
    .param p1    # Lorg/apache/tools/ant/types/resources/Files;

    const/4 v1, 0x1

    invoke-direct {p0}, Lorg/apache/tools/ant/types/selectors/AbstractSelectorContainer;-><init>()V

    new-instance v0, Lorg/apache/tools/ant/types/PatternSet;

    invoke-direct {v0}, Lorg/apache/tools/ant/types/PatternSet;-><init>()V

    iput-object v0, p0, Lorg/apache/tools/ant/types/resources/Files;->defaultPatterns:Lorg/apache/tools/ant/types/PatternSet;

    new-instance v0, Ljava/util/Vector;

    invoke-direct {v0}, Ljava/util/Vector;-><init>()V

    iput-object v0, p0, Lorg/apache/tools/ant/types/resources/Files;->additionalPatterns:Ljava/util/Vector;

    new-instance v0, Ljava/util/Vector;

    invoke-direct {v0}, Ljava/util/Vector;-><init>()V

    iput-object v0, p0, Lorg/apache/tools/ant/types/resources/Files;->selectors:Ljava/util/Vector;

    iput-boolean v1, p0, Lorg/apache/tools/ant/types/resources/Files;->useDefaultExcludes:Z

    iput-boolean v1, p0, Lorg/apache/tools/ant/types/resources/Files;->caseSensitive:Z

    iput-boolean v1, p0, Lorg/apache/tools/ant/types/resources/Files;->followSymlinks:Z

    const/4 v0, 0x0

    iput-object v0, p0, Lorg/apache/tools/ant/types/resources/Files;->ds:Lorg/apache/tools/ant/DirectoryScanner;

    iget-object v0, p1, Lorg/apache/tools/ant/types/resources/Files;->defaultPatterns:Lorg/apache/tools/ant/types/PatternSet;

    iput-object v0, p0, Lorg/apache/tools/ant/types/resources/Files;->defaultPatterns:Lorg/apache/tools/ant/types/PatternSet;

    iget-object v0, p1, Lorg/apache/tools/ant/types/resources/Files;->additionalPatterns:Ljava/util/Vector;

    iput-object v0, p0, Lorg/apache/tools/ant/types/resources/Files;->additionalPatterns:Ljava/util/Vector;

    iget-object v0, p1, Lorg/apache/tools/ant/types/resources/Files;->selectors:Ljava/util/Vector;

    iput-object v0, p0, Lorg/apache/tools/ant/types/resources/Files;->selectors:Ljava/util/Vector;

    iget-boolean v0, p1, Lorg/apache/tools/ant/types/resources/Files;->useDefaultExcludes:Z

    iput-boolean v0, p0, Lorg/apache/tools/ant/types/resources/Files;->useDefaultExcludes:Z

    iget-boolean v0, p1, Lorg/apache/tools/ant/types/resources/Files;->caseSensitive:Z

    iput-boolean v0, p0, Lorg/apache/tools/ant/types/resources/Files;->caseSensitive:Z

    iget-boolean v0, p1, Lorg/apache/tools/ant/types/resources/Files;->followSymlinks:Z

    iput-boolean v0, p0, Lorg/apache/tools/ant/types/resources/Files;->followSymlinks:Z

    iget-object v0, p1, Lorg/apache/tools/ant/types/resources/Files;->ds:Lorg/apache/tools/ant/DirectoryScanner;

    iput-object v0, p0, Lorg/apache/tools/ant/types/resources/Files;->ds:Lorg/apache/tools/ant/DirectoryScanner;

    invoke-virtual {p1}, Lorg/apache/tools/ant/types/resources/Files;->getProject()Lorg/apache/tools/ant/Project;

    move-result-object v0

    invoke-virtual {p0, v0}, Lorg/apache/tools/ant/types/resources/Files;->setProject(Lorg/apache/tools/ant/Project;)V

    return-void
.end method

.method private declared-synchronized ensureDirectoryScannerSetup()V
    .locals 3

    monitor-enter p0

    :try_start_0
    iget-object v1, p0, Lorg/apache/tools/ant/types/resources/Files;->ds:Lorg/apache/tools/ant/DirectoryScanner;

    if-nez v1, :cond_1

    new-instance v1, Lorg/apache/tools/ant/DirectoryScanner;

    invoke-direct {v1}, Lorg/apache/tools/ant/DirectoryScanner;-><init>()V

    iput-object v1, p0, Lorg/apache/tools/ant/types/resources/Files;->ds:Lorg/apache/tools/ant/DirectoryScanner;

    invoke-virtual {p0}, Lorg/apache/tools/ant/types/resources/Files;->getProject()Lorg/apache/tools/ant/Project;

    move-result-object v1

    invoke-virtual {p0, v1}, Lorg/apache/tools/ant/types/resources/Files;->mergePatterns(Lorg/apache/tools/ant/Project;)Lorg/apache/tools/ant/types/PatternSet;

    move-result-object v0

    iget-object v1, p0, Lorg/apache/tools/ant/types/resources/Files;->ds:Lorg/apache/tools/ant/DirectoryScanner;

    invoke-virtual {p0}, Lorg/apache/tools/ant/types/resources/Files;->getProject()Lorg/apache/tools/ant/Project;

    move-result-object v2

    invoke-virtual {v0, v2}, Lorg/apache/tools/ant/types/PatternSet;->getIncludePatterns(Lorg/apache/tools/ant/Project;)[Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Lorg/apache/tools/ant/DirectoryScanner;->setIncludes([Ljava/lang/String;)V

    iget-object v1, p0, Lorg/apache/tools/ant/types/resources/Files;->ds:Lorg/apache/tools/ant/DirectoryScanner;

    invoke-virtual {p0}, Lorg/apache/tools/ant/types/resources/Files;->getProject()Lorg/apache/tools/ant/Project;

    move-result-object v2

    invoke-virtual {v0, v2}, Lorg/apache/tools/ant/types/PatternSet;->getExcludePatterns(Lorg/apache/tools/ant/Project;)[Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Lorg/apache/tools/ant/DirectoryScanner;->setExcludes([Ljava/lang/String;)V

    iget-object v1, p0, Lorg/apache/tools/ant/types/resources/Files;->ds:Lorg/apache/tools/ant/DirectoryScanner;

    invoke-virtual {p0}, Lorg/apache/tools/ant/types/resources/Files;->getProject()Lorg/apache/tools/ant/Project;

    move-result-object v2

    invoke-virtual {p0, v2}, Lorg/apache/tools/ant/types/resources/Files;->getSelectors(Lorg/apache/tools/ant/Project;)[Lorg/apache/tools/ant/types/selectors/FileSelector;

    move-result-object v2

    invoke-virtual {v1, v2}, Lorg/apache/tools/ant/DirectoryScanner;->setSelectors([Lorg/apache/tools/ant/types/selectors/FileSelector;)V

    iget-boolean v1, p0, Lorg/apache/tools/ant/types/resources/Files;->useDefaultExcludes:Z

    if-eqz v1, :cond_0

    iget-object v1, p0, Lorg/apache/tools/ant/types/resources/Files;->ds:Lorg/apache/tools/ant/DirectoryScanner;

    invoke-virtual {v1}, Lorg/apache/tools/ant/DirectoryScanner;->addDefaultExcludes()V

    :cond_0
    iget-object v1, p0, Lorg/apache/tools/ant/types/resources/Files;->ds:Lorg/apache/tools/ant/DirectoryScanner;

    iget-boolean v2, p0, Lorg/apache/tools/ant/types/resources/Files;->caseSensitive:Z

    invoke-virtual {v1, v2}, Lorg/apache/tools/ant/DirectoryScanner;->setCaseSensitive(Z)V

    iget-object v1, p0, Lorg/apache/tools/ant/types/resources/Files;->ds:Lorg/apache/tools/ant/DirectoryScanner;

    iget-boolean v2, p0, Lorg/apache/tools/ant/types/resources/Files;->followSymlinks:Z

    invoke-virtual {v1, v2}, Lorg/apache/tools/ant/DirectoryScanner;->setFollowSymlinks(Z)V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    :cond_1
    monitor-exit p0

    return-void

    :catchall_0
    move-exception v1

    monitor-exit p0

    throw v1
.end method

.method private hasPatterns(Lorg/apache/tools/ant/types/PatternSet;)Z
    .locals 1
    .param p1    # Lorg/apache/tools/ant/types/PatternSet;

    invoke-virtual {p0}, Lorg/apache/tools/ant/types/resources/Files;->getProject()Lorg/apache/tools/ant/Project;

    move-result-object v0

    invoke-virtual {p1, v0}, Lorg/apache/tools/ant/types/PatternSet;->getIncludePatterns(Lorg/apache/tools/ant/Project;)[Ljava/lang/String;

    move-result-object v0

    array-length v0, v0

    if-gtz v0, :cond_0

    invoke-virtual {p0}, Lorg/apache/tools/ant/types/resources/Files;->getProject()Lorg/apache/tools/ant/Project;

    move-result-object v0

    invoke-virtual {p1, v0}, Lorg/apache/tools/ant/types/PatternSet;->getExcludePatterns(Lorg/apache/tools/ant/Project;)[Ljava/lang/String;

    move-result-object v0

    array-length v0, v0

    if-lez v0, :cond_1

    :cond_0
    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_1
    const/4 v0, 0x0

    goto :goto_0
.end method


# virtual methods
.method public declared-synchronized appendExcludes([Ljava/lang/String;)V
    .locals 3
    .param p1    # [Ljava/lang/String;

    monitor-enter p0

    :try_start_0
    invoke-virtual {p0}, Lorg/apache/tools/ant/types/resources/Files;->checkAttributesAllowed()V

    if-eqz p1, :cond_1

    const/4 v0, 0x0

    :goto_0
    array-length v1, p1

    if-ge v0, v1, :cond_0

    iget-object v1, p0, Lorg/apache/tools/ant/types/resources/Files;->defaultPatterns:Lorg/apache/tools/ant/types/PatternSet;

    invoke-virtual {v1}, Lorg/apache/tools/ant/types/PatternSet;->createExclude()Lorg/apache/tools/ant/types/PatternSet$NameEntry;

    move-result-object v1

    aget-object v2, p1, v0

    invoke-virtual {v1, v2}, Lorg/apache/tools/ant/types/PatternSet$NameEntry;->setName(Ljava/lang/String;)V

    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    :cond_0
    const/4 v1, 0x0

    iput-object v1, p0, Lorg/apache/tools/ant/types/resources/Files;->ds:Lorg/apache/tools/ant/DirectoryScanner;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    :cond_1
    monitor-exit p0

    return-void

    :catchall_0
    move-exception v1

    monitor-exit p0

    throw v1
.end method

.method public declared-synchronized appendIncludes([Ljava/lang/String;)V
    .locals 3
    .param p1    # [Ljava/lang/String;

    monitor-enter p0

    :try_start_0
    invoke-virtual {p0}, Lorg/apache/tools/ant/types/resources/Files;->checkAttributesAllowed()V

    if-eqz p1, :cond_1

    const/4 v0, 0x0

    :goto_0
    array-length v1, p1

    if-ge v0, v1, :cond_0

    iget-object v1, p0, Lorg/apache/tools/ant/types/resources/Files;->defaultPatterns:Lorg/apache/tools/ant/types/PatternSet;

    invoke-virtual {v1}, Lorg/apache/tools/ant/types/PatternSet;->createInclude()Lorg/apache/tools/ant/types/PatternSet$NameEntry;

    move-result-object v1

    aget-object v2, p1, v0

    invoke-virtual {v1, v2}, Lorg/apache/tools/ant/types/PatternSet$NameEntry;->setName(Ljava/lang/String;)V

    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    :cond_0
    const/4 v1, 0x0

    iput-object v1, p0, Lorg/apache/tools/ant/types/resources/Files;->ds:Lorg/apache/tools/ant/DirectoryScanner;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    :cond_1
    monitor-exit p0

    return-void

    :catchall_0
    move-exception v1

    monitor-exit p0

    throw v1
.end method

.method public declared-synchronized appendSelector(Lorg/apache/tools/ant/types/selectors/FileSelector;)V
    .locals 1
    .param p1    # Lorg/apache/tools/ant/types/selectors/FileSelector;

    monitor-enter p0

    :try_start_0
    invoke-virtual {p0}, Lorg/apache/tools/ant/types/resources/Files;->isReference()Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-virtual {p0}, Lorg/apache/tools/ant/types/resources/Files;->noChildrenAllowed()Lorg/apache/tools/ant/BuildException;

    move-result-object v0

    throw v0
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0

    :cond_0
    :try_start_1
    invoke-super {p0, p1}, Lorg/apache/tools/ant/types/selectors/AbstractSelectorContainer;->appendSelector(Lorg/apache/tools/ant/types/selectors/FileSelector;)V

    const/4 v0, 0x0

    iput-object v0, p0, Lorg/apache/tools/ant/types/resources/Files;->ds:Lorg/apache/tools/ant/DirectoryScanner;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    monitor-exit p0

    return-void
.end method

.method public declared-synchronized clone()Ljava/lang/Object;
    .locals 6

    monitor-enter p0

    :try_start_0
    invoke-virtual {p0}, Lorg/apache/tools/ant/types/resources/Files;->isReference()Z

    move-result v4

    if-eqz v4, :cond_0

    invoke-virtual {p0}, Lorg/apache/tools/ant/types/resources/Files;->getRef()Lorg/apache/tools/ant/types/resources/Files;

    move-result-object v4

    invoke-virtual {v4}, Lorg/apache/tools/ant/types/resources/Files;->clone()Ljava/lang/Object;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    move-result-object v1

    :goto_0
    monitor-exit p0

    return-object v1

    :cond_0
    :try_start_1
    invoke-super {p0}, Lorg/apache/tools/ant/types/selectors/AbstractSelectorContainer;->clone()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lorg/apache/tools/ant/types/resources/Files;

    iget-object v4, p0, Lorg/apache/tools/ant/types/resources/Files;->defaultPatterns:Lorg/apache/tools/ant/types/PatternSet;

    invoke-virtual {v4}, Lorg/apache/tools/ant/types/PatternSet;->clone()Ljava/lang/Object;

    move-result-object v4

    check-cast v4, Lorg/apache/tools/ant/types/PatternSet;

    iput-object v4, v1, Lorg/apache/tools/ant/types/resources/Files;->defaultPatterns:Lorg/apache/tools/ant/types/PatternSet;

    new-instance v4, Ljava/util/Vector;

    iget-object v5, p0, Lorg/apache/tools/ant/types/resources/Files;->additionalPatterns:Ljava/util/Vector;

    invoke-virtual {v5}, Ljava/util/Vector;->size()I

    move-result v5

    invoke-direct {v4, v5}, Ljava/util/Vector;-><init>(I)V

    iput-object v4, v1, Lorg/apache/tools/ant/types/resources/Files;->additionalPatterns:Ljava/util/Vector;

    iget-object v4, p0, Lorg/apache/tools/ant/types/resources/Files;->additionalPatterns:Ljava/util/Vector;

    invoke-virtual {v4}, Ljava/util/Vector;->iterator()Ljava/util/Iterator;

    move-result-object v2

    :goto_1
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    move-result v4

    if-eqz v4, :cond_1

    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Lorg/apache/tools/ant/types/PatternSet;

    iget-object v4, v1, Lorg/apache/tools/ant/types/resources/Files;->additionalPatterns:Ljava/util/Vector;

    invoke-virtual {v3}, Lorg/apache/tools/ant/types/PatternSet;->clone()Ljava/lang/Object;

    move-result-object v5

    invoke-virtual {v4, v5}, Ljava/util/Vector;->add(Ljava/lang/Object;)Z
    :try_end_1
    .catch Ljava/lang/CloneNotSupportedException; {:try_start_1 .. :try_end_1} :catch_0
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    goto :goto_1

    :catch_0
    move-exception v0

    :try_start_2
    new-instance v4, Lorg/apache/tools/ant/BuildException;

    invoke-direct {v4, v0}, Lorg/apache/tools/ant/BuildException;-><init>(Ljava/lang/Throwable;)V

    throw v4
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    :catchall_0
    move-exception v4

    monitor-exit p0

    throw v4

    :cond_1
    :try_start_3
    new-instance v4, Ljava/util/Vector;

    iget-object v5, p0, Lorg/apache/tools/ant/types/resources/Files;->selectors:Ljava/util/Vector;

    invoke-direct {v4, v5}, Ljava/util/Vector;-><init>(Ljava/util/Collection;)V

    iput-object v4, v1, Lorg/apache/tools/ant/types/resources/Files;->selectors:Ljava/util/Vector;
    :try_end_3
    .catch Ljava/lang/CloneNotSupportedException; {:try_start_3 .. :try_end_3} :catch_0
    .catchall {:try_start_3 .. :try_end_3} :catchall_0

    goto :goto_0
.end method

.method public declared-synchronized createExclude()Lorg/apache/tools/ant/types/PatternSet$NameEntry;
    .locals 1

    monitor-enter p0

    :try_start_0
    invoke-virtual {p0}, Lorg/apache/tools/ant/types/resources/Files;->isReference()Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-virtual {p0}, Lorg/apache/tools/ant/types/resources/Files;->noChildrenAllowed()Lorg/apache/tools/ant/BuildException;

    move-result-object v0

    throw v0
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0

    :cond_0
    const/4 v0, 0x0

    :try_start_1
    iput-object v0, p0, Lorg/apache/tools/ant/types/resources/Files;->ds:Lorg/apache/tools/ant/DirectoryScanner;

    iget-object v0, p0, Lorg/apache/tools/ant/types/resources/Files;->defaultPatterns:Lorg/apache/tools/ant/types/PatternSet;

    invoke-virtual {v0}, Lorg/apache/tools/ant/types/PatternSet;->createExclude()Lorg/apache/tools/ant/types/PatternSet$NameEntry;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    move-result-object v0

    monitor-exit p0

    return-object v0
.end method

.method public declared-synchronized createExcludesFile()Lorg/apache/tools/ant/types/PatternSet$NameEntry;
    .locals 1

    monitor-enter p0

    :try_start_0
    invoke-virtual {p0}, Lorg/apache/tools/ant/types/resources/Files;->isReference()Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-virtual {p0}, Lorg/apache/tools/ant/types/resources/Files;->noChildrenAllowed()Lorg/apache/tools/ant/BuildException;

    move-result-object v0

    throw v0
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0

    :cond_0
    const/4 v0, 0x0

    :try_start_1
    iput-object v0, p0, Lorg/apache/tools/ant/types/resources/Files;->ds:Lorg/apache/tools/ant/DirectoryScanner;

    iget-object v0, p0, Lorg/apache/tools/ant/types/resources/Files;->defaultPatterns:Lorg/apache/tools/ant/types/PatternSet;

    invoke-virtual {v0}, Lorg/apache/tools/ant/types/PatternSet;->createExcludesFile()Lorg/apache/tools/ant/types/PatternSet$NameEntry;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    move-result-object v0

    monitor-exit p0

    return-object v0
.end method

.method public declared-synchronized createInclude()Lorg/apache/tools/ant/types/PatternSet$NameEntry;
    .locals 1

    monitor-enter p0

    :try_start_0
    invoke-virtual {p0}, Lorg/apache/tools/ant/types/resources/Files;->isReference()Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-virtual {p0}, Lorg/apache/tools/ant/types/resources/Files;->noChildrenAllowed()Lorg/apache/tools/ant/BuildException;

    move-result-object v0

    throw v0
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0

    :cond_0
    const/4 v0, 0x0

    :try_start_1
    iput-object v0, p0, Lorg/apache/tools/ant/types/resources/Files;->ds:Lorg/apache/tools/ant/DirectoryScanner;

    iget-object v0, p0, Lorg/apache/tools/ant/types/resources/Files;->defaultPatterns:Lorg/apache/tools/ant/types/PatternSet;

    invoke-virtual {v0}, Lorg/apache/tools/ant/types/PatternSet;->createInclude()Lorg/apache/tools/ant/types/PatternSet$NameEntry;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    move-result-object v0

    monitor-exit p0

    return-object v0
.end method

.method public declared-synchronized createIncludesFile()Lorg/apache/tools/ant/types/PatternSet$NameEntry;
    .locals 1

    monitor-enter p0

    :try_start_0
    invoke-virtual {p0}, Lorg/apache/tools/ant/types/resources/Files;->isReference()Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-virtual {p0}, Lorg/apache/tools/ant/types/resources/Files;->noChildrenAllowed()Lorg/apache/tools/ant/BuildException;

    move-result-object v0

    throw v0
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0

    :cond_0
    const/4 v0, 0x0

    :try_start_1
    iput-object v0, p0, Lorg/apache/tools/ant/types/resources/Files;->ds:Lorg/apache/tools/ant/DirectoryScanner;

    iget-object v0, p0, Lorg/apache/tools/ant/types/resources/Files;->defaultPatterns:Lorg/apache/tools/ant/types/PatternSet;

    invoke-virtual {v0}, Lorg/apache/tools/ant/types/PatternSet;->createIncludesFile()Lorg/apache/tools/ant/types/PatternSet$NameEntry;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    move-result-object v0

    monitor-exit p0

    return-object v0
.end method

.method public declared-synchronized createPatternSet()Lorg/apache/tools/ant/types/PatternSet;
    .locals 2

    monitor-enter p0

    :try_start_0
    invoke-virtual {p0}, Lorg/apache/tools/ant/types/resources/Files;->isReference()Z

    move-result v1

    if-eqz v1, :cond_0

    invoke-virtual {p0}, Lorg/apache/tools/ant/types/resources/Files;->noChildrenAllowed()Lorg/apache/tools/ant/BuildException;

    move-result-object v1

    throw v1
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    :catchall_0
    move-exception v1

    monitor-exit p0

    throw v1

    :cond_0
    :try_start_1
    new-instance v0, Lorg/apache/tools/ant/types/PatternSet;

    invoke-direct {v0}, Lorg/apache/tools/ant/types/PatternSet;-><init>()V

    iget-object v1, p0, Lorg/apache/tools/ant/types/resources/Files;->additionalPatterns:Ljava/util/Vector;

    invoke-virtual {v1, v0}, Ljava/util/Vector;->addElement(Ljava/lang/Object;)V

    const/4 v1, 0x0

    iput-object v1, p0, Lorg/apache/tools/ant/types/resources/Files;->ds:Lorg/apache/tools/ant/DirectoryScanner;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    monitor-exit p0

    return-object v0
.end method

.method public declared-synchronized getDefaultexcludes()Z
    .locals 1

    monitor-enter p0

    :try_start_0
    invoke-virtual {p0}, Lorg/apache/tools/ant/types/resources/Files;->isReference()Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-virtual {p0}, Lorg/apache/tools/ant/types/resources/Files;->getRef()Lorg/apache/tools/ant/types/resources/Files;

    move-result-object v0

    invoke-virtual {v0}, Lorg/apache/tools/ant/types/resources/Files;->getDefaultexcludes()Z
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    move-result v0

    :goto_0
    monitor-exit p0

    return v0

    :cond_0
    :try_start_1
    iget-boolean v0, p0, Lorg/apache/tools/ant/types/resources/Files;->useDefaultExcludes:Z
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    goto :goto_0

    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method protected getRef()Lorg/apache/tools/ant/types/resources/Files;
    .locals 1

    invoke-virtual {p0}, Lorg/apache/tools/ant/types/resources/Files;->getCheckedRef()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lorg/apache/tools/ant/types/resources/Files;

    return-object v0
.end method

.method public declared-synchronized hasPatterns()Z
    .locals 3

    const/4 v2, 0x1

    monitor-enter p0

    :try_start_0
    invoke-virtual {p0}, Lorg/apache/tools/ant/types/resources/Files;->isReference()Z

    move-result v1

    if-eqz v1, :cond_0

    invoke-virtual {p0}, Lorg/apache/tools/ant/types/resources/Files;->getRef()Lorg/apache/tools/ant/types/resources/Files;

    move-result-object v1

    invoke-virtual {v1}, Lorg/apache/tools/ant/types/resources/Files;->hasPatterns()Z
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    move-result v1

    :goto_0
    monitor-exit p0

    return v1

    :cond_0
    :try_start_1
    iget-object v1, p0, Lorg/apache/tools/ant/types/resources/Files;->defaultPatterns:Lorg/apache/tools/ant/types/PatternSet;

    invoke-direct {p0, v1}, Lorg/apache/tools/ant/types/resources/Files;->hasPatterns(Lorg/apache/tools/ant/types/PatternSet;)Z

    move-result v1

    if-eqz v1, :cond_1

    move v1, v2

    goto :goto_0

    :cond_1
    iget-object v1, p0, Lorg/apache/tools/ant/types/resources/Files;->additionalPatterns:Ljava/util/Vector;

    invoke-virtual {v1}, Ljava/util/Vector;->iterator()Ljava/util/Iterator;

    move-result-object v0

    :cond_2
    invoke-interface {v0}, Ljava/util/Iterator;->hasNext()Z

    move-result v1

    if-eqz v1, :cond_3

    invoke-interface {v0}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lorg/apache/tools/ant/types/PatternSet;

    invoke-direct {p0, v1}, Lorg/apache/tools/ant/types/resources/Files;->hasPatterns(Lorg/apache/tools/ant/types/PatternSet;)Z
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    move-result v1

    if-eqz v1, :cond_2

    move v1, v2

    goto :goto_0

    :cond_3
    const/4 v1, 0x0

    goto :goto_0

    :catchall_0
    move-exception v1

    monitor-exit p0

    throw v1
.end method

.method public declared-synchronized isCaseSensitive()Z
    .locals 1

    monitor-enter p0

    :try_start_0
    invoke-virtual {p0}, Lorg/apache/tools/ant/types/resources/Files;->isReference()Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-virtual {p0}, Lorg/apache/tools/ant/types/resources/Files;->getRef()Lorg/apache/tools/ant/types/resources/Files;

    move-result-object v0

    invoke-virtual {v0}, Lorg/apache/tools/ant/types/resources/Files;->isCaseSensitive()Z
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    move-result v0

    :goto_0
    monitor-exit p0

    return v0

    :cond_0
    :try_start_1
    iget-boolean v0, p0, Lorg/apache/tools/ant/types/resources/Files;->caseSensitive:Z
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    goto :goto_0

    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method public isFilesystemOnly()Z
    .locals 1

    const/4 v0, 0x1

    return v0
.end method

.method public declared-synchronized isFollowSymlinks()Z
    .locals 1

    monitor-enter p0

    :try_start_0
    invoke-virtual {p0}, Lorg/apache/tools/ant/types/resources/Files;->isReference()Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-virtual {p0}, Lorg/apache/tools/ant/types/resources/Files;->getRef()Lorg/apache/tools/ant/types/resources/Files;

    move-result-object v0

    invoke-virtual {v0}, Lorg/apache/tools/ant/types/resources/Files;->isFollowSymlinks()Z
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    move-result v0

    :goto_0
    monitor-exit p0

    return v0

    :cond_0
    :try_start_1
    iget-boolean v0, p0, Lorg/apache/tools/ant/types/resources/Files;->followSymlinks:Z
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    goto :goto_0

    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method public declared-synchronized iterator()Ljava/util/Iterator;
    .locals 4

    monitor-enter p0

    :try_start_0
    invoke-virtual {p0}, Lorg/apache/tools/ant/types/resources/Files;->isReference()Z

    move-result v3

    if-eqz v3, :cond_1

    invoke-virtual {p0}, Lorg/apache/tools/ant/types/resources/Files;->getRef()Lorg/apache/tools/ant/types/resources/Files;

    move-result-object v3

    invoke-virtual {v3}, Lorg/apache/tools/ant/types/resources/Files;->iterator()Ljava/util/Iterator;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    move-result-object v2

    :cond_0
    :goto_0
    monitor-exit p0

    return-object v2

    :cond_1
    :try_start_1
    invoke-direct {p0}, Lorg/apache/tools/ant/types/resources/Files;->ensureDirectoryScannerSetup()V

    iget-object v3, p0, Lorg/apache/tools/ant/types/resources/Files;->ds:Lorg/apache/tools/ant/DirectoryScanner;

    invoke-virtual {v3}, Lorg/apache/tools/ant/DirectoryScanner;->scan()V

    iget-object v3, p0, Lorg/apache/tools/ant/types/resources/Files;->ds:Lorg/apache/tools/ant/DirectoryScanner;

    invoke-virtual {v3}, Lorg/apache/tools/ant/DirectoryScanner;->getIncludedFilesCount()I

    move-result v1

    iget-object v3, p0, Lorg/apache/tools/ant/types/resources/Files;->ds:Lorg/apache/tools/ant/DirectoryScanner;

    invoke-virtual {v3}, Lorg/apache/tools/ant/DirectoryScanner;->getIncludedDirsCount()I

    move-result v0

    add-int v3, v1, v0

    if-nez v3, :cond_2

    sget-object v2, Lorg/apache/tools/ant/types/resources/Files;->EMPTY_ITERATOR:Ljava/util/Iterator;

    goto :goto_0

    :cond_2
    new-instance v2, Lorg/apache/tools/ant/types/resources/FileResourceIterator;

    invoke-direct {v2}, Lorg/apache/tools/ant/types/resources/FileResourceIterator;-><init>()V

    if-lez v1, :cond_3

    iget-object v3, p0, Lorg/apache/tools/ant/types/resources/Files;->ds:Lorg/apache/tools/ant/DirectoryScanner;

    invoke-virtual {v3}, Lorg/apache/tools/ant/DirectoryScanner;->getIncludedFiles()[Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Lorg/apache/tools/ant/types/resources/FileResourceIterator;->addFiles([Ljava/lang/String;)V

    :cond_3
    if-lez v0, :cond_0

    iget-object v3, p0, Lorg/apache/tools/ant/types/resources/Files;->ds:Lorg/apache/tools/ant/DirectoryScanner;

    invoke-virtual {v3}, Lorg/apache/tools/ant/DirectoryScanner;->getIncludedDirectories()[Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Lorg/apache/tools/ant/types/resources/FileResourceIterator;->addFiles([Ljava/lang/String;)V
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    goto :goto_0

    :catchall_0
    move-exception v3

    monitor-exit p0

    throw v3
.end method

.method public mergeExcludes(Lorg/apache/tools/ant/Project;)[Ljava/lang/String;
    .locals 1
    .param p1    # Lorg/apache/tools/ant/Project;

    invoke-virtual {p0, p1}, Lorg/apache/tools/ant/types/resources/Files;->mergePatterns(Lorg/apache/tools/ant/Project;)Lorg/apache/tools/ant/types/PatternSet;

    move-result-object v0

    invoke-virtual {v0, p1}, Lorg/apache/tools/ant/types/PatternSet;->getExcludePatterns(Lorg/apache/tools/ant/Project;)[Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public mergeIncludes(Lorg/apache/tools/ant/Project;)[Ljava/lang/String;
    .locals 1
    .param p1    # Lorg/apache/tools/ant/Project;

    invoke-virtual {p0, p1}, Lorg/apache/tools/ant/types/resources/Files;->mergePatterns(Lorg/apache/tools/ant/Project;)Lorg/apache/tools/ant/types/PatternSet;

    move-result-object v0

    invoke-virtual {v0, p1}, Lorg/apache/tools/ant/types/PatternSet;->getIncludePatterns(Lorg/apache/tools/ant/Project;)[Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public declared-synchronized mergePatterns(Lorg/apache/tools/ant/Project;)Lorg/apache/tools/ant/types/PatternSet;
    .locals 5
    .param p1    # Lorg/apache/tools/ant/Project;

    monitor-enter p0

    :try_start_0
    invoke-virtual {p0}, Lorg/apache/tools/ant/types/resources/Files;->isReference()Z

    move-result v4

    if-eqz v4, :cond_1

    invoke-virtual {p0}, Lorg/apache/tools/ant/types/resources/Files;->getRef()Lorg/apache/tools/ant/types/resources/Files;

    move-result-object v4

    invoke-virtual {v4, p1}, Lorg/apache/tools/ant/types/resources/Files;->mergePatterns(Lorg/apache/tools/ant/Project;)Lorg/apache/tools/ant/types/PatternSet;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    move-result-object v3

    :cond_0
    monitor-exit p0

    return-object v3

    :cond_1
    :try_start_1
    new-instance v3, Lorg/apache/tools/ant/types/PatternSet;

    invoke-direct {v3}, Lorg/apache/tools/ant/types/PatternSet;-><init>()V

    iget-object v4, p0, Lorg/apache/tools/ant/types/resources/Files;->defaultPatterns:Lorg/apache/tools/ant/types/PatternSet;

    invoke-virtual {v3, v4, p1}, Lorg/apache/tools/ant/types/PatternSet;->append(Lorg/apache/tools/ant/types/PatternSet;Lorg/apache/tools/ant/Project;)V

    iget-object v4, p0, Lorg/apache/tools/ant/types/resources/Files;->additionalPatterns:Ljava/util/Vector;

    invoke-virtual {v4}, Ljava/util/Vector;->size()I

    move-result v0

    const/4 v1, 0x0

    :goto_0
    if-ge v1, v0, :cond_0

    iget-object v4, p0, Lorg/apache/tools/ant/types/resources/Files;->additionalPatterns:Ljava/util/Vector;

    invoke-virtual {v4, v1}, Ljava/util/Vector;->elementAt(I)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lorg/apache/tools/ant/types/PatternSet;

    invoke-virtual {v3, v2, p1}, Lorg/apache/tools/ant/types/PatternSet;->append(Lorg/apache/tools/ant/types/PatternSet;Lorg/apache/tools/ant/Project;)V
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    add-int/lit8 v1, v1, 0x1

    goto :goto_0

    :catchall_0
    move-exception v4

    monitor-exit p0

    throw v4
.end method

.method public declared-synchronized setCaseSensitive(Z)V
    .locals 1
    .param p1    # Z

    monitor-enter p0

    :try_start_0
    invoke-virtual {p0}, Lorg/apache/tools/ant/types/resources/Files;->checkAttributesAllowed()V

    iput-boolean p1, p0, Lorg/apache/tools/ant/types/resources/Files;->caseSensitive:Z

    const/4 v0, 0x0

    iput-object v0, p0, Lorg/apache/tools/ant/types/resources/Files;->ds:Lorg/apache/tools/ant/DirectoryScanner;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    monitor-exit p0

    return-void

    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method public declared-synchronized setDefaultexcludes(Z)V
    .locals 1
    .param p1    # Z

    monitor-enter p0

    :try_start_0
    invoke-virtual {p0}, Lorg/apache/tools/ant/types/resources/Files;->checkAttributesAllowed()V

    iput-boolean p1, p0, Lorg/apache/tools/ant/types/resources/Files;->useDefaultExcludes:Z

    const/4 v0, 0x0

    iput-object v0, p0, Lorg/apache/tools/ant/types/resources/Files;->ds:Lorg/apache/tools/ant/DirectoryScanner;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    monitor-exit p0

    return-void

    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method public declared-synchronized setExcludes(Ljava/lang/String;)V
    .locals 1
    .param p1    # Ljava/lang/String;

    monitor-enter p0

    :try_start_0
    invoke-virtual {p0}, Lorg/apache/tools/ant/types/resources/Files;->checkAttributesAllowed()V

    iget-object v0, p0, Lorg/apache/tools/ant/types/resources/Files;->defaultPatterns:Lorg/apache/tools/ant/types/PatternSet;

    invoke-virtual {v0, p1}, Lorg/apache/tools/ant/types/PatternSet;->setExcludes(Ljava/lang/String;)V

    const/4 v0, 0x0

    iput-object v0, p0, Lorg/apache/tools/ant/types/resources/Files;->ds:Lorg/apache/tools/ant/DirectoryScanner;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    monitor-exit p0

    return-void

    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method public declared-synchronized setExcludesfile(Ljava/io/File;)V
    .locals 1
    .param p1    # Ljava/io/File;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lorg/apache/tools/ant/BuildException;
        }
    .end annotation

    monitor-enter p0

    :try_start_0
    invoke-virtual {p0}, Lorg/apache/tools/ant/types/resources/Files;->checkAttributesAllowed()V

    iget-object v0, p0, Lorg/apache/tools/ant/types/resources/Files;->defaultPatterns:Lorg/apache/tools/ant/types/PatternSet;

    invoke-virtual {v0, p1}, Lorg/apache/tools/ant/types/PatternSet;->setExcludesfile(Ljava/io/File;)V

    const/4 v0, 0x0

    iput-object v0, p0, Lorg/apache/tools/ant/types/resources/Files;->ds:Lorg/apache/tools/ant/DirectoryScanner;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    monitor-exit p0

    return-void

    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method public declared-synchronized setFollowSymlinks(Z)V
    .locals 1
    .param p1    # Z

    monitor-enter p0

    :try_start_0
    invoke-virtual {p0}, Lorg/apache/tools/ant/types/resources/Files;->checkAttributesAllowed()V

    iput-boolean p1, p0, Lorg/apache/tools/ant/types/resources/Files;->followSymlinks:Z

    const/4 v0, 0x0

    iput-object v0, p0, Lorg/apache/tools/ant/types/resources/Files;->ds:Lorg/apache/tools/ant/DirectoryScanner;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    monitor-exit p0

    return-void

    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method public declared-synchronized setIncludes(Ljava/lang/String;)V
    .locals 1
    .param p1    # Ljava/lang/String;

    monitor-enter p0

    :try_start_0
    invoke-virtual {p0}, Lorg/apache/tools/ant/types/resources/Files;->checkAttributesAllowed()V

    iget-object v0, p0, Lorg/apache/tools/ant/types/resources/Files;->defaultPatterns:Lorg/apache/tools/ant/types/PatternSet;

    invoke-virtual {v0, p1}, Lorg/apache/tools/ant/types/PatternSet;->setIncludes(Ljava/lang/String;)V

    const/4 v0, 0x0

    iput-object v0, p0, Lorg/apache/tools/ant/types/resources/Files;->ds:Lorg/apache/tools/ant/DirectoryScanner;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    monitor-exit p0

    return-void

    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method public declared-synchronized setIncludesfile(Ljava/io/File;)V
    .locals 1
    .param p1    # Ljava/io/File;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lorg/apache/tools/ant/BuildException;
        }
    .end annotation

    monitor-enter p0

    :try_start_0
    invoke-virtual {p0}, Lorg/apache/tools/ant/types/resources/Files;->checkAttributesAllowed()V

    iget-object v0, p0, Lorg/apache/tools/ant/types/resources/Files;->defaultPatterns:Lorg/apache/tools/ant/types/PatternSet;

    invoke-virtual {v0, p1}, Lorg/apache/tools/ant/types/PatternSet;->setIncludesfile(Ljava/io/File;)V

    const/4 v0, 0x0

    iput-object v0, p0, Lorg/apache/tools/ant/types/resources/Files;->ds:Lorg/apache/tools/ant/DirectoryScanner;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    monitor-exit p0

    return-void

    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method public setRefid(Lorg/apache/tools/ant/types/Reference;)V
    .locals 1
    .param p1    # Lorg/apache/tools/ant/types/Reference;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lorg/apache/tools/ant/BuildException;
        }
    .end annotation

    iget-object v0, p0, Lorg/apache/tools/ant/types/resources/Files;->defaultPatterns:Lorg/apache/tools/ant/types/PatternSet;

    invoke-direct {p0, v0}, Lorg/apache/tools/ant/types/resources/Files;->hasPatterns(Lorg/apache/tools/ant/types/PatternSet;)Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-virtual {p0}, Lorg/apache/tools/ant/types/resources/Files;->tooManyAttributes()Lorg/apache/tools/ant/BuildException;

    move-result-object v0

    throw v0

    :cond_0
    iget-object v0, p0, Lorg/apache/tools/ant/types/resources/Files;->additionalPatterns:Ljava/util/Vector;

    invoke-virtual {v0}, Ljava/util/Vector;->isEmpty()Z

    move-result v0

    if-nez v0, :cond_1

    invoke-virtual {p0}, Lorg/apache/tools/ant/types/resources/Files;->noChildrenAllowed()Lorg/apache/tools/ant/BuildException;

    move-result-object v0

    throw v0

    :cond_1
    iget-object v0, p0, Lorg/apache/tools/ant/types/resources/Files;->selectors:Ljava/util/Vector;

    invoke-virtual {v0}, Ljava/util/Vector;->isEmpty()Z

    move-result v0

    if-nez v0, :cond_2

    invoke-virtual {p0}, Lorg/apache/tools/ant/types/resources/Files;->noChildrenAllowed()Lorg/apache/tools/ant/BuildException;

    move-result-object v0

    throw v0

    :cond_2
    invoke-super {p0, p1}, Lorg/apache/tools/ant/types/selectors/AbstractSelectorContainer;->setRefid(Lorg/apache/tools/ant/types/Reference;)V

    return-void
.end method

.method public declared-synchronized size()I
    .locals 2

    monitor-enter p0

    :try_start_0
    invoke-virtual {p0}, Lorg/apache/tools/ant/types/resources/Files;->isReference()Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-virtual {p0}, Lorg/apache/tools/ant/types/resources/Files;->getRef()Lorg/apache/tools/ant/types/resources/Files;

    move-result-object v0

    invoke-virtual {v0}, Lorg/apache/tools/ant/types/resources/Files;->size()I
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    move-result v0

    :goto_0
    monitor-exit p0

    return v0

    :cond_0
    :try_start_1
    invoke-direct {p0}, Lorg/apache/tools/ant/types/resources/Files;->ensureDirectoryScannerSetup()V

    iget-object v0, p0, Lorg/apache/tools/ant/types/resources/Files;->ds:Lorg/apache/tools/ant/DirectoryScanner;

    invoke-virtual {v0}, Lorg/apache/tools/ant/DirectoryScanner;->scan()V

    iget-object v0, p0, Lorg/apache/tools/ant/types/resources/Files;->ds:Lorg/apache/tools/ant/DirectoryScanner;

    invoke-virtual {v0}, Lorg/apache/tools/ant/DirectoryScanner;->getIncludedFilesCount()I

    move-result v0

    iget-object v1, p0, Lorg/apache/tools/ant/types/resources/Files;->ds:Lorg/apache/tools/ant/DirectoryScanner;

    invoke-virtual {v1}, Lorg/apache/tools/ant/DirectoryScanner;->getIncludedDirsCount()I
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    move-result v1

    add-int/2addr v0, v1

    goto :goto_0

    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method public toString()Ljava/lang/String;
    .locals 3

    invoke-virtual {p0}, Lorg/apache/tools/ant/types/resources/Files;->isReference()Z

    move-result v2

    if-eqz v2, :cond_0

    invoke-virtual {p0}, Lorg/apache/tools/ant/types/resources/Files;->getRef()Lorg/apache/tools/ant/types/resources/Files;

    move-result-object v2

    invoke-virtual {v2}, Lorg/apache/tools/ant/types/resources/Files;->toString()Ljava/lang/String;

    move-result-object v2

    :goto_0
    return-object v2

    :cond_0
    invoke-virtual {p0}, Lorg/apache/tools/ant/types/resources/Files;->iterator()Ljava/util/Iterator;

    move-result-object v0

    invoke-interface {v0}, Ljava/util/Iterator;->hasNext()Z

    move-result v2

    if-nez v2, :cond_1

    const-string v2, ""

    goto :goto_0

    :cond_1
    new-instance v1, Ljava/lang/StringBuffer;

    invoke-direct {v1}, Ljava/lang/StringBuffer;-><init>()V

    :goto_1
    invoke-interface {v0}, Ljava/util/Iterator;->hasNext()Z

    move-result v2

    if-eqz v2, :cond_3

    invoke-virtual {v1}, Ljava/lang/StringBuffer;->length()I

    move-result v2

    if-lez v2, :cond_2

    sget-char v2, Ljava/io/File;->pathSeparatorChar:C

    invoke-virtual {v1, v2}, Ljava/lang/StringBuffer;->append(C)Ljava/lang/StringBuffer;

    :cond_2
    invoke-interface {v0}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuffer;->append(Ljava/lang/Object;)Ljava/lang/StringBuffer;

    goto :goto_1

    :cond_3
    invoke-virtual {v1}, Ljava/lang/StringBuffer;->toString()Ljava/lang/String;

    move-result-object v2

    goto :goto_0
.end method
