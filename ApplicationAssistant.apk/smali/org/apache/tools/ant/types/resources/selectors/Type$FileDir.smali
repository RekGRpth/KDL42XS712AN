.class public Lorg/apache/tools/ant/types/resources/selectors/Type$FileDir;
.super Lorg/apache/tools/ant/types/EnumeratedAttribute;
.source "Type.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lorg/apache/tools/ant/types/resources/selectors/Type;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x9
    name = "FileDir"
.end annotation


# static fields
.field private static final VALUES:[Ljava/lang/String;


# direct methods
.method static constructor <clinit>()V
    .locals 3

    const/4 v0, 0x2

    new-array v0, v0, [Ljava/lang/String;

    const/4 v1, 0x0

    const-string v2, "file"

    aput-object v2, v0, v1

    const/4 v1, 0x1

    const-string v2, "dir"

    aput-object v2, v0, v1

    sput-object v0, Lorg/apache/tools/ant/types/resources/selectors/Type$FileDir;->VALUES:[Ljava/lang/String;

    return-void
.end method

.method public constructor <init>()V
    .locals 0

    invoke-direct {p0}, Lorg/apache/tools/ant/types/EnumeratedAttribute;-><init>()V

    return-void
.end method

.method public constructor <init>(Ljava/lang/String;)V
    .locals 0
    .param p1    # Ljava/lang/String;

    invoke-direct {p0}, Lorg/apache/tools/ant/types/EnumeratedAttribute;-><init>()V

    invoke-virtual {p0, p1}, Lorg/apache/tools/ant/types/resources/selectors/Type$FileDir;->setValue(Ljava/lang/String;)V

    return-void
.end method


# virtual methods
.method public getValues()[Ljava/lang/String;
    .locals 1

    sget-object v0, Lorg/apache/tools/ant/types/resources/selectors/Type$FileDir;->VALUES:[Ljava/lang/String;

    return-object v0
.end method
