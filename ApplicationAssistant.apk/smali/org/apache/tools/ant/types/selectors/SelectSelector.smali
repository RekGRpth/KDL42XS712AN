.class public Lorg/apache/tools/ant/types/selectors/SelectSelector;
.super Lorg/apache/tools/ant/types/selectors/BaseSelectorContainer;
.source "SelectSelector.java"


# instance fields
.field private ifProperty:Ljava/lang/String;

.field private unlessProperty:Ljava/lang/String;


# direct methods
.method public constructor <init>()V
    .locals 0

    invoke-direct {p0}, Lorg/apache/tools/ant/types/selectors/BaseSelectorContainer;-><init>()V

    return-void
.end method

.method private getRef()Lorg/apache/tools/ant/types/selectors/SelectSelector;
    .locals 3

    invoke-virtual {p0}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v1

    const-string v2, "SelectSelector"

    invoke-virtual {p0, v1, v2}, Lorg/apache/tools/ant/types/selectors/SelectSelector;->getCheckedRef(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lorg/apache/tools/ant/types/selectors/SelectSelector;

    return-object v0
.end method


# virtual methods
.method public appendSelector(Lorg/apache/tools/ant/types/selectors/FileSelector;)V
    .locals 1
    .param p1    # Lorg/apache/tools/ant/types/selectors/FileSelector;

    invoke-virtual {p0}, Lorg/apache/tools/ant/types/selectors/SelectSelector;->isReference()Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-virtual {p0}, Lorg/apache/tools/ant/types/selectors/SelectSelector;->noChildrenAllowed()Lorg/apache/tools/ant/BuildException;

    move-result-object v0

    throw v0

    :cond_0
    invoke-super {p0, p1}, Lorg/apache/tools/ant/types/selectors/BaseSelectorContainer;->appendSelector(Lorg/apache/tools/ant/types/selectors/FileSelector;)V

    return-void
.end method

.method public getSelectors(Lorg/apache/tools/ant/Project;)[Lorg/apache/tools/ant/types/selectors/FileSelector;
    .locals 1
    .param p1    # Lorg/apache/tools/ant/Project;

    invoke-virtual {p0}, Lorg/apache/tools/ant/types/selectors/SelectSelector;->isReference()Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-direct {p0}, Lorg/apache/tools/ant/types/selectors/SelectSelector;->getRef()Lorg/apache/tools/ant/types/selectors/SelectSelector;

    move-result-object v0

    invoke-virtual {v0, p1}, Lorg/apache/tools/ant/types/selectors/SelectSelector;->getSelectors(Lorg/apache/tools/ant/Project;)[Lorg/apache/tools/ant/types/selectors/FileSelector;

    move-result-object v0

    :goto_0
    return-object v0

    :cond_0
    invoke-super {p0, p1}, Lorg/apache/tools/ant/types/selectors/BaseSelectorContainer;->getSelectors(Lorg/apache/tools/ant/Project;)[Lorg/apache/tools/ant/types/selectors/FileSelector;

    move-result-object v0

    goto :goto_0
.end method

.method public hasSelectors()Z
    .locals 1

    invoke-virtual {p0}, Lorg/apache/tools/ant/types/selectors/SelectSelector;->isReference()Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-direct {p0}, Lorg/apache/tools/ant/types/selectors/SelectSelector;->getRef()Lorg/apache/tools/ant/types/selectors/SelectSelector;

    move-result-object v0

    invoke-virtual {v0}, Lorg/apache/tools/ant/types/selectors/SelectSelector;->hasSelectors()Z

    move-result v0

    :goto_0
    return v0

    :cond_0
    invoke-super {p0}, Lorg/apache/tools/ant/types/selectors/BaseSelectorContainer;->hasSelectors()Z

    move-result v0

    goto :goto_0
.end method

.method public isSelected(Ljava/io/File;Ljava/lang/String;Ljava/io/File;)Z
    .locals 3
    .param p1    # Ljava/io/File;
    .param p2    # Ljava/lang/String;
    .param p3    # Ljava/io/File;

    invoke-virtual {p0}, Lorg/apache/tools/ant/types/selectors/SelectSelector;->validate()V

    invoke-virtual {p0}, Lorg/apache/tools/ant/types/selectors/SelectSelector;->passesConditions()Z

    move-result v2

    if-nez v2, :cond_0

    const/4 v2, 0x0

    :goto_0
    return v2

    :cond_0
    invoke-virtual {p0}, Lorg/apache/tools/ant/types/selectors/SelectSelector;->selectorElements()Ljava/util/Enumeration;

    move-result-object v0

    invoke-interface {v0}, Ljava/util/Enumeration;->hasMoreElements()Z

    move-result v2

    if-nez v2, :cond_1

    const/4 v2, 0x1

    goto :goto_0

    :cond_1
    invoke-interface {v0}, Ljava/util/Enumeration;->nextElement()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lorg/apache/tools/ant/types/selectors/FileSelector;

    invoke-interface {v1, p1, p2, p3}, Lorg/apache/tools/ant/types/selectors/FileSelector;->isSelected(Ljava/io/File;Ljava/lang/String;Ljava/io/File;)Z

    move-result v2

    goto :goto_0
.end method

.method public passesConditions()Z
    .locals 3

    const/4 v0, 0x0

    iget-object v1, p0, Lorg/apache/tools/ant/types/selectors/SelectSelector;->ifProperty:Ljava/lang/String;

    if-eqz v1, :cond_1

    invoke-virtual {p0}, Lorg/apache/tools/ant/types/selectors/SelectSelector;->getProject()Lorg/apache/tools/ant/Project;

    move-result-object v1

    iget-object v2, p0, Lorg/apache/tools/ant/types/selectors/SelectSelector;->ifProperty:Ljava/lang/String;

    invoke-virtual {v1, v2}, Lorg/apache/tools/ant/Project;->getProperty(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    if-nez v1, :cond_1

    :cond_0
    :goto_0
    return v0

    :cond_1
    iget-object v1, p0, Lorg/apache/tools/ant/types/selectors/SelectSelector;->unlessProperty:Ljava/lang/String;

    if-eqz v1, :cond_2

    invoke-virtual {p0}, Lorg/apache/tools/ant/types/selectors/SelectSelector;->getProject()Lorg/apache/tools/ant/Project;

    move-result-object v1

    iget-object v2, p0, Lorg/apache/tools/ant/types/selectors/SelectSelector;->unlessProperty:Ljava/lang/String;

    invoke-virtual {v1, v2}, Lorg/apache/tools/ant/Project;->getProperty(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    if-nez v1, :cond_0

    :cond_2
    const/4 v0, 0x1

    goto :goto_0
.end method

.method public selectorCount()I
    .locals 1

    invoke-virtual {p0}, Lorg/apache/tools/ant/types/selectors/SelectSelector;->isReference()Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-direct {p0}, Lorg/apache/tools/ant/types/selectors/SelectSelector;->getRef()Lorg/apache/tools/ant/types/selectors/SelectSelector;

    move-result-object v0

    invoke-virtual {v0}, Lorg/apache/tools/ant/types/selectors/SelectSelector;->selectorCount()I

    move-result v0

    :goto_0
    return v0

    :cond_0
    invoke-super {p0}, Lorg/apache/tools/ant/types/selectors/BaseSelectorContainer;->selectorCount()I

    move-result v0

    goto :goto_0
.end method

.method public selectorElements()Ljava/util/Enumeration;
    .locals 1

    invoke-virtual {p0}, Lorg/apache/tools/ant/types/selectors/SelectSelector;->isReference()Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-direct {p0}, Lorg/apache/tools/ant/types/selectors/SelectSelector;->getRef()Lorg/apache/tools/ant/types/selectors/SelectSelector;

    move-result-object v0

    invoke-virtual {v0}, Lorg/apache/tools/ant/types/selectors/SelectSelector;->selectorElements()Ljava/util/Enumeration;

    move-result-object v0

    :goto_0
    return-object v0

    :cond_0
    invoke-super {p0}, Lorg/apache/tools/ant/types/selectors/BaseSelectorContainer;->selectorElements()Ljava/util/Enumeration;

    move-result-object v0

    goto :goto_0
.end method

.method public setIf(Ljava/lang/String;)V
    .locals 0
    .param p1    # Ljava/lang/String;

    iput-object p1, p0, Lorg/apache/tools/ant/types/selectors/SelectSelector;->ifProperty:Ljava/lang/String;

    return-void
.end method

.method public setUnless(Ljava/lang/String;)V
    .locals 0
    .param p1    # Ljava/lang/String;

    iput-object p1, p0, Lorg/apache/tools/ant/types/selectors/SelectSelector;->unlessProperty:Ljava/lang/String;

    return-void
.end method

.method public toString()Ljava/lang/String;
    .locals 2

    new-instance v0, Ljava/lang/StringBuffer;

    invoke-direct {v0}, Ljava/lang/StringBuffer;-><init>()V

    invoke-virtual {p0}, Lorg/apache/tools/ant/types/selectors/SelectSelector;->hasSelectors()Z

    move-result v1

    if-eqz v1, :cond_2

    const-string v1, "{select"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    iget-object v1, p0, Lorg/apache/tools/ant/types/selectors/SelectSelector;->ifProperty:Ljava/lang/String;

    if-eqz v1, :cond_0

    const-string v1, " if: "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    iget-object v1, p0, Lorg/apache/tools/ant/types/selectors/SelectSelector;->ifProperty:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    :cond_0
    iget-object v1, p0, Lorg/apache/tools/ant/types/selectors/SelectSelector;->unlessProperty:Ljava/lang/String;

    if-eqz v1, :cond_1

    const-string v1, " unless: "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    iget-object v1, p0, Lorg/apache/tools/ant/types/selectors/SelectSelector;->unlessProperty:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    :cond_1
    const-string v1, " "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    invoke-super {p0}, Lorg/apache/tools/ant/types/selectors/BaseSelectorContainer;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    const-string v1, "}"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    :cond_2
    invoke-virtual {v0}, Ljava/lang/StringBuffer;->toString()Ljava/lang/String;

    move-result-object v1

    return-object v1
.end method

.method public verifySettings()V
    .locals 2

    invoke-virtual {p0}, Lorg/apache/tools/ant/types/selectors/SelectSelector;->selectorCount()I

    move-result v0

    if-ltz v0, :cond_0

    const/4 v1, 0x1

    if-le v0, v1, :cond_1

    :cond_0
    const-string v1, "Only one selector is allowed within the <selector> tag"

    invoke-virtual {p0, v1}, Lorg/apache/tools/ant/types/selectors/SelectSelector;->setError(Ljava/lang/String;)V

    :cond_1
    return-void
.end method
