.class public final Lorg/apache/tools/ant/types/selectors/SelectorUtils;
.super Ljava/lang/Object;
.source "SelectorUtils.java"


# static fields
.field private static final FILE_UTILS:Lorg/apache/tools/ant/util/FileUtils;

.field private static instance:Lorg/apache/tools/ant/types/selectors/SelectorUtils;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    new-instance v0, Lorg/apache/tools/ant/types/selectors/SelectorUtils;

    invoke-direct {v0}, Lorg/apache/tools/ant/types/selectors/SelectorUtils;-><init>()V

    sput-object v0, Lorg/apache/tools/ant/types/selectors/SelectorUtils;->instance:Lorg/apache/tools/ant/types/selectors/SelectorUtils;

    invoke-static {}, Lorg/apache/tools/ant/util/FileUtils;->getFileUtils()Lorg/apache/tools/ant/util/FileUtils;

    move-result-object v0

    sput-object v0, Lorg/apache/tools/ant/types/selectors/SelectorUtils;->FILE_UTILS:Lorg/apache/tools/ant/util/FileUtils;

    return-void
.end method

.method private constructor <init>()V
    .locals 0

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method public static getInstance()Lorg/apache/tools/ant/types/selectors/SelectorUtils;
    .locals 1

    sget-object v0, Lorg/apache/tools/ant/types/selectors/SelectorUtils;->instance:Lorg/apache/tools/ant/types/selectors/SelectorUtils;

    return-object v0
.end method

.method public static hasWildcards(Ljava/lang/String;)Z
    .locals 2
    .param p0    # Ljava/lang/String;

    const/4 v1, -0x1

    const/16 v0, 0x2a

    invoke-virtual {p0, v0}, Ljava/lang/String;->indexOf(I)I

    move-result v0

    if-ne v0, v1, :cond_0

    const/16 v0, 0x3f

    invoke-virtual {p0, v0}, Ljava/lang/String;->indexOf(I)I

    move-result v0

    if-eq v0, v1, :cond_1

    :cond_0
    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_1
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public static isOutOfDate(Ljava/io/File;Ljava/io/File;I)Z
    .locals 6
    .param p0    # Ljava/io/File;
    .param p1    # Ljava/io/File;
    .param p2    # I

    const/4 v1, 0x1

    const/4 v0, 0x0

    invoke-virtual {p0}, Ljava/io/File;->exists()Z

    move-result v2

    if-nez v2, :cond_1

    :cond_0
    :goto_0
    return v0

    :cond_1
    invoke-virtual {p1}, Ljava/io/File;->exists()Z

    move-result v2

    if-nez v2, :cond_2

    move v0, v1

    goto :goto_0

    :cond_2
    invoke-virtual {p0}, Ljava/io/File;->lastModified()J

    move-result-wide v2

    int-to-long v4, p2

    sub-long/2addr v2, v4

    invoke-virtual {p1}, Ljava/io/File;->lastModified()J

    move-result-wide v4

    cmp-long v2, v2, v4

    if-lez v2, :cond_0

    move v0, v1

    goto :goto_0
.end method

.method public static isOutOfDate(Lorg/apache/tools/ant/types/Resource;Lorg/apache/tools/ant/types/Resource;I)Z
    .locals 2
    .param p0    # Lorg/apache/tools/ant/types/Resource;
    .param p1    # Lorg/apache/tools/ant/types/Resource;
    .param p2    # I

    int-to-long v0, p2

    invoke-static {p0, p1, v0, v1}, Lorg/apache/tools/ant/types/selectors/SelectorUtils;->isOutOfDate(Lorg/apache/tools/ant/types/Resource;Lorg/apache/tools/ant/types/Resource;J)Z

    move-result v0

    return v0
.end method

.method public static isOutOfDate(Lorg/apache/tools/ant/types/Resource;Lorg/apache/tools/ant/types/Resource;J)Z
    .locals 6
    .param p0    # Lorg/apache/tools/ant/types/Resource;
    .param p1    # Lorg/apache/tools/ant/types/Resource;
    .param p2    # J

    const/4 v1, 0x1

    const/4 v0, 0x0

    invoke-virtual {p0}, Lorg/apache/tools/ant/types/Resource;->isExists()Z

    move-result v2

    if-nez v2, :cond_1

    :cond_0
    :goto_0
    return v0

    :cond_1
    invoke-virtual {p1}, Lorg/apache/tools/ant/types/Resource;->isExists()Z

    move-result v2

    if-nez v2, :cond_2

    move v0, v1

    goto :goto_0

    :cond_2
    invoke-virtual {p0}, Lorg/apache/tools/ant/types/Resource;->getLastModified()J

    move-result-wide v2

    sub-long/2addr v2, p2

    invoke-virtual {p1}, Lorg/apache/tools/ant/types/Resource;->getLastModified()J

    move-result-wide v4

    cmp-long v2, v2, v4

    if-lez v2, :cond_0

    move v0, v1

    goto :goto_0
.end method

.method public static match(Ljava/lang/String;Ljava/lang/String;)Z
    .locals 1
    .param p0    # Ljava/lang/String;
    .param p1    # Ljava/lang/String;

    const/4 v0, 0x1

    invoke-static {p0, p1, v0}, Lorg/apache/tools/ant/types/selectors/SelectorUtils;->match(Ljava/lang/String;Ljava/lang/String;Z)Z

    move-result v0

    return v0
.end method

.method public static match(Ljava/lang/String;Ljava/lang/String;Z)Z
    .locals 16
    .param p0    # Ljava/lang/String;
    .param p1    # Ljava/lang/String;
    .param p2    # Z

    invoke-virtual/range {p0 .. p0}, Ljava/lang/String;->toCharArray()[C

    move-result-object v5

    invoke-virtual/range {p1 .. p1}, Ljava/lang/String;->toCharArray()[C

    move-result-object v10

    const/4 v7, 0x0

    array-length v14, v5

    add-int/lit8 v6, v14, -0x1

    const/4 v12, 0x0

    array-length v14, v10

    add-int/lit8 v11, v14, -0x1

    const/4 v1, 0x0

    const/4 v3, 0x0

    :goto_0
    array-length v14, v5

    if-ge v3, v14, :cond_0

    aget-char v14, v5, v3

    const/16 v15, 0x2a

    if-ne v14, v15, :cond_1

    const/4 v1, 0x1

    :cond_0
    if-nez v1, :cond_6

    if-eq v6, v11, :cond_2

    const/4 v14, 0x0

    :goto_1
    return v14

    :cond_1
    add-int/lit8 v3, v3, 0x1

    goto :goto_0

    :cond_2
    const/4 v3, 0x0

    :goto_2
    if-gt v3, v6, :cond_5

    aget-char v0, v5, v3

    const/16 v14, 0x3f

    if-eq v0, v14, :cond_4

    if-eqz p2, :cond_3

    aget-char v14, v10, v3

    if-eq v0, v14, :cond_3

    const/4 v14, 0x0

    goto :goto_1

    :cond_3
    if-nez p2, :cond_4

    invoke-static {v0}, Ljava/lang/Character;->toUpperCase(C)C

    move-result v14

    aget-char v15, v10, v3

    invoke-static {v15}, Ljava/lang/Character;->toUpperCase(C)C

    move-result v15

    if-eq v14, v15, :cond_4

    const/4 v14, 0x0

    goto :goto_1

    :cond_4
    add-int/lit8 v3, v3, 0x1

    goto :goto_2

    :cond_5
    const/4 v14, 0x1

    goto :goto_1

    :cond_6
    if-nez v6, :cond_8

    const/4 v14, 0x1

    goto :goto_1

    :cond_7
    add-int/lit8 v7, v7, 0x1

    add-int/lit8 v12, v12, 0x1

    :cond_8
    aget-char v0, v5, v7

    const/16 v14, 0x2a

    if-eq v0, v14, :cond_a

    if-gt v12, v11, :cond_a

    const/16 v14, 0x3f

    if-eq v0, v14, :cond_7

    if-eqz p2, :cond_9

    aget-char v14, v10, v12

    if-eq v0, v14, :cond_9

    const/4 v14, 0x0

    goto :goto_1

    :cond_9
    if-nez p2, :cond_7

    invoke-static {v0}, Ljava/lang/Character;->toUpperCase(C)C

    move-result v14

    aget-char v15, v10, v12

    invoke-static {v15}, Ljava/lang/Character;->toUpperCase(C)C

    move-result v15

    if-eq v14, v15, :cond_7

    const/4 v14, 0x0

    goto :goto_1

    :cond_a
    if-le v12, v11, :cond_e

    move v3, v7

    :goto_3
    if-gt v3, v6, :cond_c

    aget-char v14, v5, v3

    const/16 v15, 0x2a

    if-eq v14, v15, :cond_b

    const/4 v14, 0x0

    goto :goto_1

    :cond_b
    add-int/lit8 v3, v3, 0x1

    goto :goto_3

    :cond_c
    const/4 v14, 0x1

    goto :goto_1

    :cond_d
    add-int/lit8 v6, v6, -0x1

    add-int/lit8 v11, v11, -0x1

    :cond_e
    aget-char v0, v5, v6

    const/16 v14, 0x2a

    if-eq v0, v14, :cond_10

    if-gt v12, v11, :cond_10

    const/16 v14, 0x3f

    if-eq v0, v14, :cond_d

    if-eqz p2, :cond_f

    aget-char v14, v10, v11

    if-eq v0, v14, :cond_f

    const/4 v14, 0x0

    goto :goto_1

    :cond_f
    if-nez p2, :cond_d

    invoke-static {v0}, Ljava/lang/Character;->toUpperCase(C)C

    move-result v14

    aget-char v15, v10, v11

    invoke-static {v15}, Ljava/lang/Character;->toUpperCase(C)C

    move-result v15

    if-eq v14, v15, :cond_d

    const/4 v14, 0x0

    goto/16 :goto_1

    :cond_10
    if-le v12, v11, :cond_13

    move v3, v7

    :goto_4
    if-gt v3, v6, :cond_12

    aget-char v14, v5, v3

    const/16 v15, 0x2a

    if-eq v14, v15, :cond_11

    const/4 v14, 0x0

    goto/16 :goto_1

    :cond_11
    add-int/lit8 v3, v3, 0x1

    goto :goto_4

    :cond_12
    const/4 v14, 0x1

    goto/16 :goto_1

    :cond_13
    :goto_5
    if-eq v7, v6, :cond_1d

    if-gt v12, v11, :cond_1d

    const/4 v8, -0x1

    add-int/lit8 v3, v7, 0x1

    :goto_6
    if-gt v3, v6, :cond_14

    aget-char v14, v5, v3

    const/16 v15, 0x2a

    if-ne v14, v15, :cond_15

    move v8, v3

    :cond_14
    add-int/lit8 v14, v7, 0x1

    if-ne v8, v14, :cond_16

    add-int/lit8 v7, v7, 0x1

    goto :goto_5

    :cond_15
    add-int/lit8 v3, v3, 0x1

    goto :goto_6

    :cond_16
    sub-int v14, v8, v7

    add-int/lit8 v9, v14, -0x1

    sub-int v14, v11, v12

    add-int/lit8 v13, v14, 0x1

    const/4 v2, -0x1

    const/4 v3, 0x0

    :goto_7
    sub-int v14, v13, v9

    if-gt v3, v14, :cond_1b

    const/4 v4, 0x0

    :goto_8
    if-ge v4, v9, :cond_1a

    add-int v14, v7, v4

    add-int/lit8 v14, v14, 0x1

    aget-char v0, v5, v14

    const/16 v14, 0x3f

    if-eq v0, v14, :cond_19

    if-eqz p2, :cond_18

    add-int v14, v12, v3

    add-int/2addr v14, v4

    aget-char v14, v10, v14

    if-eq v0, v14, :cond_18

    :cond_17
    add-int/lit8 v3, v3, 0x1

    goto :goto_7

    :cond_18
    if-nez p2, :cond_19

    invoke-static {v0}, Ljava/lang/Character;->toUpperCase(C)C

    move-result v14

    add-int v15, v12, v3

    add-int/2addr v15, v4

    aget-char v15, v10, v15

    invoke-static {v15}, Ljava/lang/Character;->toUpperCase(C)C

    move-result v15

    if-ne v14, v15, :cond_17

    :cond_19
    add-int/lit8 v4, v4, 0x1

    goto :goto_8

    :cond_1a
    add-int v2, v12, v3

    :cond_1b
    const/4 v14, -0x1

    if-ne v2, v14, :cond_1c

    const/4 v14, 0x0

    goto/16 :goto_1

    :cond_1c
    move v7, v8

    add-int v12, v2, v9

    goto :goto_5

    :cond_1d
    move v3, v7

    :goto_9
    if-gt v3, v6, :cond_1f

    aget-char v14, v5, v3

    const/16 v15, 0x2a

    if-eq v14, v15, :cond_1e

    const/4 v14, 0x0

    goto/16 :goto_1

    :cond_1e
    add-int/lit8 v3, v3, 0x1

    goto :goto_9

    :cond_1f
    const/4 v14, 0x1

    goto/16 :goto_1
.end method

.method public static matchPath(Ljava/lang/String;Ljava/lang/String;)Z
    .locals 1
    .param p0    # Ljava/lang/String;
    .param p1    # Ljava/lang/String;

    const/4 v0, 0x1

    invoke-static {p0, p1, v0}, Lorg/apache/tools/ant/types/selectors/SelectorUtils;->matchPath(Ljava/lang/String;Ljava/lang/String;Z)Z

    move-result v0

    return v0
.end method

.method public static matchPath(Ljava/lang/String;Ljava/lang/String;Z)Z
    .locals 19
    .param p0    # Ljava/lang/String;
    .param p1    # Ljava/lang/String;
    .param p2    # Z

    invoke-static/range {p0 .. p0}, Lorg/apache/tools/ant/types/selectors/SelectorUtils;->tokenizePathAsArray(Ljava/lang/String;)[Ljava/lang/String;

    move-result-object v6

    invoke-static/range {p1 .. p1}, Lorg/apache/tools/ant/types/selectors/SelectorUtils;->tokenizePathAsArray(Ljava/lang/String;)[Ljava/lang/String;

    move-result-object v11

    const/4 v8, 0x0

    array-length v0, v6

    move/from16 v17, v0

    add-int/lit8 v7, v17, -0x1

    const/4 v13, 0x0

    array-length v0, v11

    move/from16 v17, v0

    add-int/lit8 v12, v17, -0x1

    :goto_0
    if-gt v8, v7, :cond_0

    if-gt v13, v12, :cond_0

    aget-object v5, v6, v8

    const-string v17, "**"

    move-object/from16 v0, v17

    invoke-virtual {v5, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v17

    if-eqz v17, :cond_1

    :cond_0
    if-le v13, v12, :cond_5

    move v3, v8

    :goto_1
    if-gt v3, v7, :cond_4

    aget-object v17, v6, v3

    const-string v18, "**"

    invoke-virtual/range {v17 .. v18}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v17

    if-nez v17, :cond_3

    const/4 v6, 0x0

    const/4 v11, 0x0

    const/16 v17, 0x0

    :goto_2
    return v17

    :cond_1
    aget-object v17, v11, v13

    move-object/from16 v0, v17

    move/from16 v1, p2

    invoke-static {v5, v0, v1}, Lorg/apache/tools/ant/types/selectors/SelectorUtils;->match(Ljava/lang/String;Ljava/lang/String;Z)Z

    move-result v17

    if-nez v17, :cond_2

    const/4 v6, 0x0

    const/4 v11, 0x0

    const/16 v17, 0x0

    goto :goto_2

    :cond_2
    add-int/lit8 v8, v8, 0x1

    add-int/lit8 v13, v13, 0x1

    goto :goto_0

    :cond_3
    add-int/lit8 v3, v3, 0x1

    goto :goto_1

    :cond_4
    const/16 v17, 0x1

    goto :goto_2

    :cond_5
    if-le v8, v7, :cond_7

    const/4 v6, 0x0

    const/4 v11, 0x0

    const/16 v17, 0x0

    goto :goto_2

    :cond_6
    add-int/lit8 v7, v7, -0x1

    add-int/lit8 v12, v12, -0x1

    :cond_7
    if-gt v8, v7, :cond_8

    if-gt v13, v12, :cond_8

    aget-object v5, v6, v7

    const-string v17, "**"

    move-object/from16 v0, v17

    invoke-virtual {v5, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v17

    if-eqz v17, :cond_9

    :cond_8
    if-le v13, v12, :cond_c

    move v3, v8

    :goto_3
    if-gt v3, v7, :cond_b

    aget-object v17, v6, v3

    const-string v18, "**"

    invoke-virtual/range {v17 .. v18}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v17

    if-nez v17, :cond_a

    const/4 v6, 0x0

    const/4 v11, 0x0

    const/16 v17, 0x0

    goto :goto_2

    :cond_9
    aget-object v17, v11, v12

    move-object/from16 v0, v17

    move/from16 v1, p2

    invoke-static {v5, v0, v1}, Lorg/apache/tools/ant/types/selectors/SelectorUtils;->match(Ljava/lang/String;Ljava/lang/String;Z)Z

    move-result v17

    if-nez v17, :cond_6

    const/4 v6, 0x0

    const/4 v11, 0x0

    const/16 v17, 0x0

    goto :goto_2

    :cond_a
    add-int/lit8 v3, v3, 0x1

    goto :goto_3

    :cond_b
    const/16 v17, 0x1

    goto :goto_2

    :cond_c
    :goto_4
    if-eq v8, v7, :cond_14

    if-gt v13, v12, :cond_14

    const/4 v9, -0x1

    add-int/lit8 v3, v8, 0x1

    :goto_5
    if-gt v3, v7, :cond_d

    aget-object v17, v6, v3

    const-string v18, "**"

    invoke-virtual/range {v17 .. v18}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v17

    if-eqz v17, :cond_e

    move v9, v3

    :cond_d
    add-int/lit8 v17, v8, 0x1

    move/from16 v0, v17

    if-ne v9, v0, :cond_f

    add-int/lit8 v8, v8, 0x1

    goto :goto_4

    :cond_e
    add-int/lit8 v3, v3, 0x1

    goto :goto_5

    :cond_f
    sub-int v17, v9, v8

    add-int/lit8 v10, v17, -0x1

    sub-int v17, v12, v13

    add-int/lit8 v14, v17, 0x1

    const/4 v2, -0x1

    const/4 v3, 0x0

    :goto_6
    sub-int v17, v14, v10

    move/from16 v0, v17

    if-gt v3, v0, :cond_12

    const/4 v4, 0x0

    :goto_7
    if-ge v4, v10, :cond_11

    add-int v17, v8, v4

    add-int/lit8 v17, v17, 0x1

    aget-object v15, v6, v17

    add-int v17, v13, v3

    add-int v17, v17, v4

    aget-object v16, v11, v17

    move-object/from16 v0, v16

    move/from16 v1, p2

    invoke-static {v15, v0, v1}, Lorg/apache/tools/ant/types/selectors/SelectorUtils;->match(Ljava/lang/String;Ljava/lang/String;Z)Z

    move-result v17

    if-nez v17, :cond_10

    add-int/lit8 v3, v3, 0x1

    goto :goto_6

    :cond_10
    add-int/lit8 v4, v4, 0x1

    goto :goto_7

    :cond_11
    add-int v2, v13, v3

    :cond_12
    const/16 v17, -0x1

    move/from16 v0, v17

    if-ne v2, v0, :cond_13

    const/4 v6, 0x0

    const/4 v11, 0x0

    const/16 v17, 0x0

    goto/16 :goto_2

    :cond_13
    move v8, v9

    add-int v13, v2, v10

    goto :goto_4

    :cond_14
    move v3, v8

    :goto_8
    if-gt v3, v7, :cond_16

    aget-object v17, v6, v3

    const-string v18, "**"

    invoke-virtual/range {v17 .. v18}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v17

    if-nez v17, :cond_15

    const/4 v6, 0x0

    const/4 v11, 0x0

    const/16 v17, 0x0

    goto/16 :goto_2

    :cond_15
    add-int/lit8 v3, v3, 0x1

    goto :goto_8

    :cond_16
    const/16 v17, 0x1

    goto/16 :goto_2
.end method

.method public static matchPatternStart(Ljava/lang/String;Ljava/lang/String;)Z
    .locals 1
    .param p0    # Ljava/lang/String;
    .param p1    # Ljava/lang/String;

    const/4 v0, 0x1

    invoke-static {p0, p1, v0}, Lorg/apache/tools/ant/types/selectors/SelectorUtils;->matchPatternStart(Ljava/lang/String;Ljava/lang/String;Z)Z

    move-result v0

    return v0
.end method

.method public static matchPatternStart(Ljava/lang/String;Ljava/lang/String;Z)Z
    .locals 11
    .param p0    # Ljava/lang/String;
    .param p1    # Ljava/lang/String;
    .param p2    # Z

    const/4 v8, 0x1

    const/4 v7, 0x0

    sget-object v9, Ljava/io/File;->separator:Ljava/lang/String;

    invoke-virtual {p1, v9}, Ljava/lang/String;->startsWith(Ljava/lang/String;)Z

    move-result v9

    sget-object v10, Ljava/io/File;->separator:Ljava/lang/String;

    invoke-virtual {p0, v10}, Ljava/lang/String;->startsWith(Ljava/lang/String;)Z

    move-result v10

    if-eq v9, v10, :cond_1

    :cond_0
    :goto_0
    return v7

    :cond_1
    invoke-static {p0}, Lorg/apache/tools/ant/types/selectors/SelectorUtils;->tokenizePathAsArray(Ljava/lang/String;)[Ljava/lang/String;

    move-result-object v1

    invoke-static {p1}, Lorg/apache/tools/ant/types/selectors/SelectorUtils;->tokenizePathAsArray(Ljava/lang/String;)[Ljava/lang/String;

    move-result-object v4

    const/4 v3, 0x0

    array-length v9, v1

    add-int/lit8 v2, v9, -0x1

    const/4 v6, 0x0

    array-length v9, v4

    add-int/lit8 v5, v9, -0x1

    :goto_1
    if-gt v3, v2, :cond_2

    if-gt v6, v5, :cond_2

    aget-object v0, v1, v3

    const-string v9, "**"

    invoke-virtual {v0, v9}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v9

    if-eqz v9, :cond_3

    :cond_2
    if-le v6, v5, :cond_4

    move v7, v8

    goto :goto_0

    :cond_3
    aget-object v9, v4, v6

    invoke-static {v0, v9, p2}, Lorg/apache/tools/ant/types/selectors/SelectorUtils;->match(Ljava/lang/String;Ljava/lang/String;Z)Z

    move-result v9

    if-eqz v9, :cond_0

    add-int/lit8 v3, v3, 0x1

    add-int/lit8 v6, v6, 0x1

    goto :goto_1

    :cond_4
    if-gt v3, v2, :cond_0

    move v7, v8

    goto :goto_0
.end method

.method public static removeWhitespace(Ljava/lang/String;)Ljava/lang/String;
    .locals 3
    .param p0    # Ljava/lang/String;

    new-instance v0, Ljava/lang/StringBuffer;

    invoke-direct {v0}, Ljava/lang/StringBuffer;-><init>()V

    if-eqz p0, :cond_0

    new-instance v1, Ljava/util/StringTokenizer;

    invoke-direct {v1, p0}, Ljava/util/StringTokenizer;-><init>(Ljava/lang/String;)V

    :goto_0
    invoke-virtual {v1}, Ljava/util/StringTokenizer;->hasMoreTokens()Z

    move-result v2

    if-eqz v2, :cond_0

    invoke-virtual {v1}, Ljava/util/StringTokenizer;->nextToken()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0, v2}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    goto :goto_0

    :cond_0
    invoke-virtual {v0}, Ljava/lang/StringBuffer;->toString()Ljava/lang/String;

    move-result-object v2

    return-object v2
.end method

.method public static rtrimWildcardTokens(Ljava/lang/String;)Ljava/lang/String;
    .locals 5
    .param p0    # Ljava/lang/String;

    invoke-static {p0}, Lorg/apache/tools/ant/types/selectors/SelectorUtils;->tokenizePathAsArray(Ljava/lang/String;)[Ljava/lang/String;

    move-result-object v2

    new-instance v1, Ljava/lang/StringBuffer;

    invoke-direct {v1}, Ljava/lang/StringBuffer;-><init>()V

    const/4 v0, 0x0

    :goto_0
    array-length v3, v2

    if-ge v0, v3, :cond_0

    aget-object v3, v2, v0

    invoke-static {v3}, Lorg/apache/tools/ant/types/selectors/SelectorUtils;->hasWildcards(Ljava/lang/String;)Z

    move-result v3

    if-eqz v3, :cond_1

    :cond_0
    invoke-virtual {v1}, Ljava/lang/StringBuffer;->toString()Ljava/lang/String;

    move-result-object v3

    return-object v3

    :cond_1
    if-lez v0, :cond_2

    invoke-virtual {v1}, Ljava/lang/StringBuffer;->length()I

    move-result v3

    add-int/lit8 v3, v3, -0x1

    invoke-virtual {v1, v3}, Ljava/lang/StringBuffer;->charAt(I)C

    move-result v3

    sget-char v4, Ljava/io/File;->separatorChar:C

    if-eq v3, v4, :cond_2

    sget-object v3, Ljava/io/File;->separator:Ljava/lang/String;

    invoke-virtual {v1, v3}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    :cond_2
    aget-object v3, v2, v0

    invoke-virtual {v1, v3}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    add-int/lit8 v0, v0, 0x1

    goto :goto_0
.end method

.method public static tokenizePath(Ljava/lang/String;)Ljava/util/Vector;
    .locals 1
    .param p0    # Ljava/lang/String;

    sget-object v0, Ljava/io/File;->separator:Ljava/lang/String;

    invoke-static {p0, v0}, Lorg/apache/tools/ant/types/selectors/SelectorUtils;->tokenizePath(Ljava/lang/String;Ljava/lang/String;)Ljava/util/Vector;

    move-result-object v0

    return-object v0
.end method

.method public static tokenizePath(Ljava/lang/String;Ljava/lang/String;)Ljava/util/Vector;
    .locals 4
    .param p0    # Ljava/lang/String;
    .param p1    # Ljava/lang/String;

    new-instance v0, Ljava/util/Vector;

    invoke-direct {v0}, Ljava/util/Vector;-><init>()V

    invoke-static {p0}, Lorg/apache/tools/ant/util/FileUtils;->isAbsolutePath(Ljava/lang/String;)Z

    move-result v3

    if-eqz v3, :cond_0

    sget-object v3, Lorg/apache/tools/ant/types/selectors/SelectorUtils;->FILE_UTILS:Lorg/apache/tools/ant/util/FileUtils;

    invoke-virtual {v3, p0}, Lorg/apache/tools/ant/util/FileUtils;->dissect(Ljava/lang/String;)[Ljava/lang/String;

    move-result-object v1

    const/4 v3, 0x0

    aget-object v3, v1, v3

    invoke-virtual {v0, v3}, Ljava/util/Vector;->add(Ljava/lang/Object;)Z

    const/4 v3, 0x1

    aget-object p0, v1, v3

    :cond_0
    new-instance v2, Ljava/util/StringTokenizer;

    invoke-direct {v2, p0, p1}, Ljava/util/StringTokenizer;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    :goto_0
    invoke-virtual {v2}, Ljava/util/StringTokenizer;->hasMoreTokens()Z

    move-result v3

    if-eqz v3, :cond_1

    invoke-virtual {v2}, Ljava/util/StringTokenizer;->nextToken()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v0, v3}, Ljava/util/Vector;->addElement(Ljava/lang/Object;)V

    goto :goto_0

    :cond_1
    return-object v0
.end method

.method private static tokenizePathAsArray(Ljava/lang/String;)[Ljava/lang/String;
    .locals 13
    .param p0    # Ljava/lang/String;

    const/4 v10, 0x1

    const/4 v11, 0x0

    const/4 v5, 0x0

    invoke-static {p0}, Lorg/apache/tools/ant/util/FileUtils;->isAbsolutePath(Ljava/lang/String;)Z

    move-result v12

    if-eqz v12, :cond_0

    sget-object v12, Lorg/apache/tools/ant/types/selectors/SelectorUtils;->FILE_UTILS:Lorg/apache/tools/ant/util/FileUtils;

    invoke-virtual {v12, p0}, Lorg/apache/tools/ant/util/FileUtils;->dissect(Ljava/lang/String;)[Ljava/lang/String;

    move-result-object v6

    aget-object v5, v6, v11

    aget-object p0, v6, v10

    :cond_0
    sget-char v7, Ljava/io/File;->separatorChar:C

    const/4 v8, 0x0

    invoke-virtual {p0}, Ljava/lang/String;->length()I

    move-result v3

    const/4 v0, 0x0

    const/4 v4, 0x0

    :goto_0
    if-ge v4, v3, :cond_3

    invoke-virtual {p0, v4}, Ljava/lang/String;->charAt(I)C

    move-result v12

    if-ne v12, v7, :cond_2

    if-eq v4, v8, :cond_1

    add-int/lit8 v0, v0, 0x1

    :cond_1
    add-int/lit8 v8, v4, 0x1

    :cond_2
    add-int/lit8 v4, v4, 0x1

    goto :goto_0

    :cond_3
    if-eq v3, v8, :cond_4

    add-int/lit8 v0, v0, 0x1

    :cond_4
    if-nez v5, :cond_5

    move v10, v11

    :cond_5
    add-int/2addr v10, v0

    new-array v2, v10, [Ljava/lang/String;

    if-eqz v5, :cond_6

    aput-object v5, v2, v11

    const/4 v0, 0x1

    :goto_1
    const/4 v8, 0x0

    const/4 v4, 0x0

    move v1, v0

    :goto_2
    if-ge v4, v3, :cond_7

    invoke-virtual {p0, v4}, Ljava/lang/String;->charAt(I)C

    move-result v10

    if-ne v10, v7, :cond_a

    if-eq v4, v8, :cond_9

    invoke-virtual {p0, v8, v4}, Ljava/lang/String;->substring(II)Ljava/lang/String;

    move-result-object v9

    add-int/lit8 v0, v1, 0x1

    aput-object v9, v2, v1

    :goto_3
    add-int/lit8 v8, v4, 0x1

    :goto_4
    add-int/lit8 v4, v4, 0x1

    move v1, v0

    goto :goto_2

    :cond_6
    const/4 v0, 0x0

    goto :goto_1

    :cond_7
    if-eq v3, v8, :cond_8

    invoke-virtual {p0, v8}, Ljava/lang/String;->substring(I)Ljava/lang/String;

    move-result-object v9

    aput-object v9, v2, v1

    :cond_8
    return-object v2

    :cond_9
    move v0, v1

    goto :goto_3

    :cond_a
    move v0, v1

    goto :goto_4
.end method
