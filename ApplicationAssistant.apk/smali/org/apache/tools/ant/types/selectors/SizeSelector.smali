.class public Lorg/apache/tools/ant/types/selectors/SizeSelector;
.super Lorg/apache/tools/ant/types/selectors/BaseExtendSelector;
.source "SizeSelector.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lorg/apache/tools/ant/types/selectors/SizeSelector$SizeComparisons;,
        Lorg/apache/tools/ant/types/selectors/SizeSelector$ByteUnits;
    }
.end annotation


# static fields
.field public static final SIZE_KEY:Ljava/lang/String; = "value"

.field public static final UNITS_KEY:Ljava/lang/String; = "units"

.field public static final WHEN_KEY:Ljava/lang/String; = "when"


# instance fields
.field private multiplier:J

.field private size:J

.field private sizelimit:J

.field private when:Lorg/apache/tools/ant/types/Comparison;


# direct methods
.method public constructor <init>()V
    .locals 4

    const-wide/16 v2, -0x1

    invoke-direct {p0}, Lorg/apache/tools/ant/types/selectors/BaseExtendSelector;-><init>()V

    iput-wide v2, p0, Lorg/apache/tools/ant/types/selectors/SizeSelector;->size:J

    const-wide/16 v0, 0x1

    iput-wide v0, p0, Lorg/apache/tools/ant/types/selectors/SizeSelector;->multiplier:J

    iput-wide v2, p0, Lorg/apache/tools/ant/types/selectors/SizeSelector;->sizelimit:J

    sget-object v0, Lorg/apache/tools/ant/types/Comparison;->EQUAL:Lorg/apache/tools/ant/types/Comparison;

    iput-object v0, p0, Lorg/apache/tools/ant/types/selectors/SizeSelector;->when:Lorg/apache/tools/ant/types/Comparison;

    return-void
.end method


# virtual methods
.method public isSelected(Ljava/io/File;Ljava/lang/String;Ljava/io/File;)Z
    .locals 6
    .param p1    # Ljava/io/File;
    .param p2    # Ljava/lang/String;
    .param p3    # Ljava/io/File;

    invoke-virtual {p0}, Lorg/apache/tools/ant/types/selectors/SizeSelector;->validate()V

    invoke-virtual {p3}, Ljava/io/File;->isDirectory()Z

    move-result v2

    if-eqz v2, :cond_0

    const/4 v2, 0x1

    :goto_0
    return v2

    :cond_0
    invoke-virtual {p3}, Ljava/io/File;->length()J

    move-result-wide v2

    iget-wide v4, p0, Lorg/apache/tools/ant/types/selectors/SizeSelector;->sizelimit:J

    sub-long v0, v2, v4

    iget-object v3, p0, Lorg/apache/tools/ant/types/selectors/SizeSelector;->when:Lorg/apache/tools/ant/types/Comparison;

    const-wide/16 v4, 0x0

    cmp-long v2, v0, v4

    if-nez v2, :cond_1

    const/4 v2, 0x0

    :goto_1
    invoke-virtual {v3, v2}, Lorg/apache/tools/ant/types/Comparison;->evaluate(I)Z

    move-result v2

    goto :goto_0

    :cond_1
    invoke-static {v0, v1}, Ljava/lang/Math;->abs(J)J

    move-result-wide v4

    div-long v4, v0, v4

    long-to-int v2, v4

    goto :goto_1
.end method

.method public setParameters([Lorg/apache/tools/ant/types/Parameter;)V
    .locals 7
    .param p1    # [Lorg/apache/tools/ant/types/Parameter;

    invoke-super {p0, p1}, Lorg/apache/tools/ant/types/selectors/BaseExtendSelector;->setParameters([Lorg/apache/tools/ant/types/Parameter;)V

    if-eqz p1, :cond_3

    const/4 v0, 0x0

    :goto_0
    array-length v5, p1

    if-ge v0, v5, :cond_3

    aget-object v5, p1, v0

    invoke-virtual {v5}, Lorg/apache/tools/ant/types/Parameter;->getName()Ljava/lang/String;

    move-result-object v2

    const-string v5, "value"

    invoke-virtual {v5, v2}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v5

    if-eqz v5, :cond_0

    :try_start_0
    new-instance v5, Ljava/lang/Long;

    aget-object v6, p1, v0

    invoke-virtual {v6}, Lorg/apache/tools/ant/types/Parameter;->getValue()Ljava/lang/String;

    move-result-object v6

    invoke-direct {v5, v6}, Ljava/lang/Long;-><init>(Ljava/lang/String;)V

    invoke-virtual {v5}, Ljava/lang/Long;->longValue()J

    move-result-wide v5

    invoke-virtual {p0, v5, v6}, Lorg/apache/tools/ant/types/selectors/SizeSelector;->setValue(J)V
    :try_end_0
    .catch Ljava/lang/NumberFormatException; {:try_start_0 .. :try_end_0} :catch_0

    :goto_1
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    :catch_0
    move-exception v1

    new-instance v5, Ljava/lang/StringBuffer;

    invoke-direct {v5}, Ljava/lang/StringBuffer;-><init>()V

    const-string v6, "Invalid size setting "

    invoke-virtual {v5, v6}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    move-result-object v5

    aget-object v6, p1, v0

    invoke-virtual {v6}, Lorg/apache/tools/ant/types/Parameter;->getValue()Ljava/lang/String;

    move-result-object v6

    invoke-virtual {v5, v6}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/StringBuffer;->toString()Ljava/lang/String;

    move-result-object v5

    invoke-virtual {p0, v5}, Lorg/apache/tools/ant/types/selectors/SizeSelector;->setError(Ljava/lang/String;)V

    goto :goto_1

    :cond_0
    const-string v5, "units"

    invoke-virtual {v5, v2}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v5

    if-eqz v5, :cond_1

    new-instance v4, Lorg/apache/tools/ant/types/selectors/SizeSelector$ByteUnits;

    invoke-direct {v4}, Lorg/apache/tools/ant/types/selectors/SizeSelector$ByteUnits;-><init>()V

    aget-object v5, p1, v0

    invoke-virtual {v5}, Lorg/apache/tools/ant/types/Parameter;->getValue()Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v4, v5}, Lorg/apache/tools/ant/types/selectors/SizeSelector$ByteUnits;->setValue(Ljava/lang/String;)V

    invoke-virtual {p0, v4}, Lorg/apache/tools/ant/types/selectors/SizeSelector;->setUnits(Lorg/apache/tools/ant/types/selectors/SizeSelector$ByteUnits;)V

    goto :goto_1

    :cond_1
    const-string v5, "when"

    invoke-virtual {v5, v2}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v5

    if-eqz v5, :cond_2

    new-instance v3, Lorg/apache/tools/ant/types/selectors/SizeSelector$SizeComparisons;

    invoke-direct {v3}, Lorg/apache/tools/ant/types/selectors/SizeSelector$SizeComparisons;-><init>()V

    aget-object v5, p1, v0

    invoke-virtual {v5}, Lorg/apache/tools/ant/types/Parameter;->getValue()Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v3, v5}, Lorg/apache/tools/ant/types/selectors/SizeSelector$SizeComparisons;->setValue(Ljava/lang/String;)V

    invoke-virtual {p0, v3}, Lorg/apache/tools/ant/types/selectors/SizeSelector;->setWhen(Lorg/apache/tools/ant/types/selectors/SizeSelector$SizeComparisons;)V

    goto :goto_1

    :cond_2
    new-instance v5, Ljava/lang/StringBuffer;

    invoke-direct {v5}, Ljava/lang/StringBuffer;-><init>()V

    const-string v6, "Invalid parameter "

    invoke-virtual {v5, v6}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    move-result-object v5

    invoke-virtual {v5, v2}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/StringBuffer;->toString()Ljava/lang/String;

    move-result-object v5

    invoke-virtual {p0, v5}, Lorg/apache/tools/ant/types/selectors/SizeSelector;->setError(Ljava/lang/String;)V

    goto :goto_1

    :cond_3
    return-void
.end method

.method public setUnits(Lorg/apache/tools/ant/types/selectors/SizeSelector$ByteUnits;)V
    .locals 5
    .param p1    # Lorg/apache/tools/ant/types/selectors/SizeSelector$ByteUnits;

    const-wide/16 v3, 0x0

    invoke-virtual {p1}, Lorg/apache/tools/ant/types/selectors/SizeSelector$ByteUnits;->getIndex()I

    move-result v0

    iput-wide v3, p0, Lorg/apache/tools/ant/types/selectors/SizeSelector;->multiplier:J

    const/4 v1, -0x1

    if-le v0, v1, :cond_2

    const/4 v1, 0x4

    if-ge v0, v1, :cond_2

    const-wide/16 v1, 0x3e8

    iput-wide v1, p0, Lorg/apache/tools/ant/types/selectors/SizeSelector;->multiplier:J

    :cond_0
    :goto_0
    iget-wide v1, p0, Lorg/apache/tools/ant/types/selectors/SizeSelector;->multiplier:J

    cmp-long v1, v1, v3

    if-lez v1, :cond_1

    iget-wide v1, p0, Lorg/apache/tools/ant/types/selectors/SizeSelector;->size:J

    const-wide/16 v3, -0x1

    cmp-long v1, v1, v3

    if-lez v1, :cond_1

    iget-wide v1, p0, Lorg/apache/tools/ant/types/selectors/SizeSelector;->size:J

    iget-wide v3, p0, Lorg/apache/tools/ant/types/selectors/SizeSelector;->multiplier:J

    mul-long/2addr v1, v3

    iput-wide v1, p0, Lorg/apache/tools/ant/types/selectors/SizeSelector;->sizelimit:J

    :cond_1
    return-void

    :cond_2
    const/4 v1, 0x3

    if-le v0, v1, :cond_3

    const/16 v1, 0x9

    if-ge v0, v1, :cond_3

    const-wide/16 v1, 0x400

    iput-wide v1, p0, Lorg/apache/tools/ant/types/selectors/SizeSelector;->multiplier:J

    goto :goto_0

    :cond_3
    const/16 v1, 0x8

    if-le v0, v1, :cond_4

    const/16 v1, 0xd

    if-ge v0, v1, :cond_4

    const-wide/32 v1, 0xf4240

    iput-wide v1, p0, Lorg/apache/tools/ant/types/selectors/SizeSelector;->multiplier:J

    goto :goto_0

    :cond_4
    const/16 v1, 0xc

    if-le v0, v1, :cond_5

    const/16 v1, 0x12

    if-ge v0, v1, :cond_5

    const-wide/32 v1, 0x100000

    iput-wide v1, p0, Lorg/apache/tools/ant/types/selectors/SizeSelector;->multiplier:J

    goto :goto_0

    :cond_5
    const/16 v1, 0x11

    if-le v0, v1, :cond_6

    const/16 v1, 0x16

    if-ge v0, v1, :cond_6

    const-wide/32 v1, 0x3b9aca00

    iput-wide v1, p0, Lorg/apache/tools/ant/types/selectors/SizeSelector;->multiplier:J

    goto :goto_0

    :cond_6
    const/16 v1, 0x15

    if-le v0, v1, :cond_7

    const/16 v1, 0x1b

    if-ge v0, v1, :cond_7

    const-wide/32 v1, 0x40000000

    iput-wide v1, p0, Lorg/apache/tools/ant/types/selectors/SizeSelector;->multiplier:J

    goto :goto_0

    :cond_7
    const/16 v1, 0x1a

    if-le v0, v1, :cond_8

    const/16 v1, 0x1f

    if-ge v0, v1, :cond_8

    const-wide v1, 0xe8d4a51000L

    iput-wide v1, p0, Lorg/apache/tools/ant/types/selectors/SizeSelector;->multiplier:J

    goto :goto_0

    :cond_8
    const/16 v1, 0x1e

    if-le v0, v1, :cond_0

    const/16 v1, 0x24

    if-ge v0, v1, :cond_0

    const-wide v1, 0x10000000000L

    iput-wide v1, p0, Lorg/apache/tools/ant/types/selectors/SizeSelector;->multiplier:J

    goto :goto_0
.end method

.method public setValue(J)V
    .locals 4
    .param p1    # J

    iput-wide p1, p0, Lorg/apache/tools/ant/types/selectors/SizeSelector;->size:J

    iget-wide v0, p0, Lorg/apache/tools/ant/types/selectors/SizeSelector;->multiplier:J

    const-wide/16 v2, 0x0

    cmp-long v0, v0, v2

    if-eqz v0, :cond_0

    const-wide/16 v0, -0x1

    cmp-long v0, p1, v0

    if-lez v0, :cond_0

    iget-wide v0, p0, Lorg/apache/tools/ant/types/selectors/SizeSelector;->multiplier:J

    mul-long/2addr v0, p1

    iput-wide v0, p0, Lorg/apache/tools/ant/types/selectors/SizeSelector;->sizelimit:J

    :cond_0
    return-void
.end method

.method public setWhen(Lorg/apache/tools/ant/types/selectors/SizeSelector$SizeComparisons;)V
    .locals 0
    .param p1    # Lorg/apache/tools/ant/types/selectors/SizeSelector$SizeComparisons;

    iput-object p1, p0, Lorg/apache/tools/ant/types/selectors/SizeSelector;->when:Lorg/apache/tools/ant/types/Comparison;

    return-void
.end method

.method public toString()Ljava/lang/String;
    .locals 3

    new-instance v0, Ljava/lang/StringBuffer;

    const-string v1, "{sizeselector value: "

    invoke-direct {v0, v1}, Ljava/lang/StringBuffer;-><init>(Ljava/lang/String;)V

    iget-wide v1, p0, Lorg/apache/tools/ant/types/selectors/SizeSelector;->sizelimit:J

    invoke-virtual {v0, v1, v2}, Ljava/lang/StringBuffer;->append(J)Ljava/lang/StringBuffer;

    const-string v1, "compare: "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    move-result-object v1

    iget-object v2, p0, Lorg/apache/tools/ant/types/selectors/SizeSelector;->when:Lorg/apache/tools/ant/types/Comparison;

    invoke-virtual {v2}, Lorg/apache/tools/ant/types/Comparison;->getValue()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    const-string v1, "}"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    invoke-virtual {v0}, Ljava/lang/StringBuffer;->toString()Ljava/lang/String;

    move-result-object v1

    return-object v1
.end method

.method public verifySettings()V
    .locals 6

    const-wide/16 v4, 0x0

    iget-wide v0, p0, Lorg/apache/tools/ant/types/selectors/SizeSelector;->size:J

    cmp-long v0, v0, v4

    if-gez v0, :cond_1

    const-string v0, "The value attribute is required, and must be positive"

    invoke-virtual {p0, v0}, Lorg/apache/tools/ant/types/selectors/SizeSelector;->setError(Ljava/lang/String;)V

    :cond_0
    :goto_0
    return-void

    :cond_1
    iget-wide v0, p0, Lorg/apache/tools/ant/types/selectors/SizeSelector;->multiplier:J

    const-wide/16 v2, 0x1

    cmp-long v0, v0, v2

    if-gez v0, :cond_2

    const-string v0, "Invalid Units supplied, must be K,Ki,M,Mi,G,Gi,T,or Ti"

    invoke-virtual {p0, v0}, Lorg/apache/tools/ant/types/selectors/SizeSelector;->setError(Ljava/lang/String;)V

    goto :goto_0

    :cond_2
    iget-wide v0, p0, Lorg/apache/tools/ant/types/selectors/SizeSelector;->sizelimit:J

    cmp-long v0, v0, v4

    if-gez v0, :cond_0

    const-string v0, "Internal error: Code is not setting sizelimit correctly"

    invoke-virtual {p0, v0}, Lorg/apache/tools/ant/types/selectors/SizeSelector;->setError(Ljava/lang/String;)V

    goto :goto_0
.end method
