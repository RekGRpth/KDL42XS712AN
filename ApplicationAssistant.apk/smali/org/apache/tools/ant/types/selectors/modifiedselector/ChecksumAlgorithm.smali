.class public Lorg/apache/tools/ant/types/selectors/modifiedselector/ChecksumAlgorithm;
.super Ljava/lang/Object;
.source "ChecksumAlgorithm.java"

# interfaces
.implements Lorg/apache/tools/ant/types/selectors/modifiedselector/Algorithm;


# instance fields
.field private algorithm:Ljava/lang/String;

.field private checksum:Ljava/util/zip/Checksum;


# direct methods
.method public constructor <init>()V
    .locals 1

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    const-string v0, "CRC"

    iput-object v0, p0, Lorg/apache/tools/ant/types/selectors/modifiedselector/ChecksumAlgorithm;->algorithm:Ljava/lang/String;

    const/4 v0, 0x0

    iput-object v0, p0, Lorg/apache/tools/ant/types/selectors/modifiedselector/ChecksumAlgorithm;->checksum:Ljava/util/zip/Checksum;

    return-void
.end method


# virtual methods
.method public getValue(Ljava/io/File;)Ljava/lang/String;
    .locals 7
    .param p1    # Ljava/io/File;

    invoke-virtual {p0}, Lorg/apache/tools/ant/types/selectors/modifiedselector/ChecksumAlgorithm;->initChecksum()V

    const/4 v4, 0x0

    :try_start_0
    invoke-virtual {p1}, Ljava/io/File;->canRead()Z

    move-result v5

    if-eqz v5, :cond_1

    iget-object v5, p0, Lorg/apache/tools/ant/types/selectors/modifiedselector/ChecksumAlgorithm;->checksum:Ljava/util/zip/Checksum;

    invoke-interface {v5}, Ljava/util/zip/Checksum;->reset()V

    new-instance v2, Ljava/io/FileInputStream;

    invoke-direct {v2, p1}, Ljava/io/FileInputStream;-><init>(Ljava/io/File;)V

    new-instance v0, Ljava/util/zip/CheckedInputStream;

    iget-object v5, p0, Lorg/apache/tools/ant/types/selectors/modifiedselector/ChecksumAlgorithm;->checksum:Ljava/util/zip/Checksum;

    invoke-direct {v0, v2, v5}, Ljava/util/zip/CheckedInputStream;-><init>(Ljava/io/InputStream;Ljava/util/zip/Checksum;)V

    new-instance v3, Ljava/io/BufferedInputStream;

    invoke-direct {v3, v0}, Ljava/io/BufferedInputStream;-><init>(Ljava/io/InputStream;)V

    :cond_0
    invoke-virtual {v3}, Ljava/io/BufferedInputStream;->read()I

    move-result v5

    const/4 v6, -0x1

    if-ne v5, v6, :cond_0

    invoke-virtual {v0}, Ljava/util/zip/CheckedInputStream;->getChecksum()Ljava/util/zip/Checksum;

    move-result-object v5

    invoke-interface {v5}, Ljava/util/zip/Checksum;->getValue()J

    move-result-wide v5

    invoke-static {v5, v6}, Ljava/lang/Long;->toString(J)Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v3}, Ljava/io/BufferedInputStream;->close()V
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    :cond_1
    :goto_0
    return-object v4

    :catch_0
    move-exception v1

    const/4 v4, 0x0

    goto :goto_0
.end method

.method public initChecksum()V
    .locals 2

    iget-object v0, p0, Lorg/apache/tools/ant/types/selectors/modifiedselector/ChecksumAlgorithm;->checksum:Ljava/util/zip/Checksum;

    if-eqz v0, :cond_0

    :goto_0
    return-void

    :cond_0
    const-string v0, "CRC"

    iget-object v1, p0, Lorg/apache/tools/ant/types/selectors/modifiedselector/ChecksumAlgorithm;->algorithm:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_1

    new-instance v0, Ljava/util/zip/CRC32;

    invoke-direct {v0}, Ljava/util/zip/CRC32;-><init>()V

    iput-object v0, p0, Lorg/apache/tools/ant/types/selectors/modifiedselector/ChecksumAlgorithm;->checksum:Ljava/util/zip/Checksum;

    goto :goto_0

    :cond_1
    const-string v0, "ADLER"

    iget-object v1, p0, Lorg/apache/tools/ant/types/selectors/modifiedselector/ChecksumAlgorithm;->algorithm:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_2

    new-instance v0, Ljava/util/zip/Adler32;

    invoke-direct {v0}, Ljava/util/zip/Adler32;-><init>()V

    iput-object v0, p0, Lorg/apache/tools/ant/types/selectors/modifiedselector/ChecksumAlgorithm;->checksum:Ljava/util/zip/Checksum;

    goto :goto_0

    :cond_2
    new-instance v0, Lorg/apache/tools/ant/BuildException;

    new-instance v1, Ljava/security/NoSuchAlgorithmException;

    invoke-direct {v1}, Ljava/security/NoSuchAlgorithmException;-><init>()V

    invoke-direct {v0, v1}, Lorg/apache/tools/ant/BuildException;-><init>(Ljava/lang/Throwable;)V

    throw v0
.end method

.method public isValid()Z
    .locals 2

    const-string v0, "CRC"

    iget-object v1, p0, Lorg/apache/tools/ant/types/selectors/modifiedselector/ChecksumAlgorithm;->algorithm:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v0

    if-nez v0, :cond_0

    const-string v0, "ADLER"

    iget-object v1, p0, Lorg/apache/tools/ant/types/selectors/modifiedselector/ChecksumAlgorithm;->algorithm:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_1

    :cond_0
    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_1
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public setAlgorithm(Ljava/lang/String;)V
    .locals 0
    .param p1    # Ljava/lang/String;

    iput-object p1, p0, Lorg/apache/tools/ant/types/selectors/modifiedselector/ChecksumAlgorithm;->algorithm:Ljava/lang/String;

    return-void
.end method

.method public toString()Ljava/lang/String;
    .locals 3

    new-instance v0, Ljava/lang/StringBuffer;

    invoke-direct {v0}, Ljava/lang/StringBuffer;-><init>()V

    const-string v1, "<ChecksumAlgorithm:"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    const-string v1, "algorithm="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    move-result-object v1

    iget-object v2, p0, Lorg/apache/tools/ant/types/selectors/modifiedselector/ChecksumAlgorithm;->algorithm:Ljava/lang/String;

    invoke-virtual {v1, v2}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    const-string v1, ">"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    invoke-virtual {v0}, Ljava/lang/StringBuffer;->toString()Ljava/lang/String;

    move-result-object v1

    return-object v1
.end method
