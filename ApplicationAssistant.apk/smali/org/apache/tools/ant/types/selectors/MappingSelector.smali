.class public abstract Lorg/apache/tools/ant/types/selectors/MappingSelector;
.super Lorg/apache/tools/ant/types/selectors/BaseSelector;
.source "MappingSelector.java"


# static fields
.field private static final FILE_UTILS:Lorg/apache/tools/ant/util/FileUtils;


# instance fields
.field protected granularity:I

.field protected map:Lorg/apache/tools/ant/util/FileNameMapper;

.field protected mapperElement:Lorg/apache/tools/ant/types/Mapper;

.field protected targetdir:Ljava/io/File;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    invoke-static {}, Lorg/apache/tools/ant/util/FileUtils;->getFileUtils()Lorg/apache/tools/ant/util/FileUtils;

    move-result-object v0

    sput-object v0, Lorg/apache/tools/ant/types/selectors/MappingSelector;->FILE_UTILS:Lorg/apache/tools/ant/util/FileUtils;

    return-void
.end method

.method public constructor <init>()V
    .locals 2

    const/4 v0, 0x0

    invoke-direct {p0}, Lorg/apache/tools/ant/types/selectors/BaseSelector;-><init>()V

    iput-object v0, p0, Lorg/apache/tools/ant/types/selectors/MappingSelector;->targetdir:Ljava/io/File;

    iput-object v0, p0, Lorg/apache/tools/ant/types/selectors/MappingSelector;->mapperElement:Lorg/apache/tools/ant/types/Mapper;

    iput-object v0, p0, Lorg/apache/tools/ant/types/selectors/MappingSelector;->map:Lorg/apache/tools/ant/util/FileNameMapper;

    const/4 v0, 0x0

    iput v0, p0, Lorg/apache/tools/ant/types/selectors/MappingSelector;->granularity:I

    sget-object v0, Lorg/apache/tools/ant/types/selectors/MappingSelector;->FILE_UTILS:Lorg/apache/tools/ant/util/FileUtils;

    invoke-virtual {v0}, Lorg/apache/tools/ant/util/FileUtils;->getFileTimestampGranularity()J

    move-result-wide v0

    long-to-int v0, v0

    iput v0, p0, Lorg/apache/tools/ant/types/selectors/MappingSelector;->granularity:I

    return-void
.end method


# virtual methods
.method public createMapper()Lorg/apache/tools/ant/types/Mapper;
    .locals 2
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lorg/apache/tools/ant/BuildException;
        }
    .end annotation

    iget-object v0, p0, Lorg/apache/tools/ant/types/selectors/MappingSelector;->mapperElement:Lorg/apache/tools/ant/types/Mapper;

    if-eqz v0, :cond_0

    new-instance v0, Lorg/apache/tools/ant/BuildException;

    const-string v1, "Cannot define more than one mapper"

    invoke-direct {v0, v1}, Lorg/apache/tools/ant/BuildException;-><init>(Ljava/lang/String;)V

    throw v0

    :cond_0
    new-instance v0, Lorg/apache/tools/ant/types/Mapper;

    invoke-virtual {p0}, Lorg/apache/tools/ant/types/selectors/MappingSelector;->getProject()Lorg/apache/tools/ant/Project;

    move-result-object v1

    invoke-direct {v0, v1}, Lorg/apache/tools/ant/types/Mapper;-><init>(Lorg/apache/tools/ant/Project;)V

    iput-object v0, p0, Lorg/apache/tools/ant/types/selectors/MappingSelector;->mapperElement:Lorg/apache/tools/ant/types/Mapper;

    iget-object v0, p0, Lorg/apache/tools/ant/types/selectors/MappingSelector;->mapperElement:Lorg/apache/tools/ant/types/Mapper;

    return-object v0
.end method

.method public isSelected(Ljava/io/File;Ljava/lang/String;Ljava/io/File;)Z
    .locals 7
    .param p1    # Ljava/io/File;
    .param p2    # Ljava/lang/String;
    .param p3    # Ljava/io/File;

    const/4 v3, 0x0

    invoke-virtual {p0}, Lorg/apache/tools/ant/types/selectors/MappingSelector;->validate()V

    iget-object v4, p0, Lorg/apache/tools/ant/types/selectors/MappingSelector;->map:Lorg/apache/tools/ant/util/FileNameMapper;

    invoke-interface {v4, p2}, Lorg/apache/tools/ant/util/FileNameMapper;->mapFileName(Ljava/lang/String;)[Ljava/lang/String;

    move-result-object v1

    if-nez v1, :cond_0

    :goto_0
    return v3

    :cond_0
    array-length v4, v1

    const/4 v5, 0x1

    if-ne v4, v5, :cond_1

    aget-object v4, v1, v3

    if-nez v4, :cond_2

    :cond_1
    new-instance v4, Lorg/apache/tools/ant/BuildException;

    new-instance v5, Ljava/lang/StringBuffer;

    invoke-direct {v5}, Ljava/lang/StringBuffer;-><init>()V

    const-string v6, "Invalid destination file results for "

    invoke-virtual {v5, v6}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    move-result-object v5

    iget-object v6, p0, Lorg/apache/tools/ant/types/selectors/MappingSelector;->targetdir:Ljava/io/File;

    invoke-virtual {v6}, Ljava/io/File;->getName()Ljava/lang/String;

    move-result-object v6

    invoke-virtual {v5, v6}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    move-result-object v5

    const-string v6, " with filename "

    invoke-virtual {v5, v6}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    move-result-object v5

    invoke-virtual {v5, p2}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/StringBuffer;->toString()Ljava/lang/String;

    move-result-object v5

    invoke-direct {v4, v5}, Lorg/apache/tools/ant/BuildException;-><init>(Ljava/lang/String;)V

    throw v4

    :cond_2
    aget-object v2, v1, v3

    new-instance v0, Ljava/io/File;

    iget-object v4, p0, Lorg/apache/tools/ant/types/selectors/MappingSelector;->targetdir:Ljava/io/File;

    invoke-direct {v0, v4, v2}, Ljava/io/File;-><init>(Ljava/io/File;Ljava/lang/String;)V

    invoke-virtual {p0, p3, v0}, Lorg/apache/tools/ant/types/selectors/MappingSelector;->selectionTest(Ljava/io/File;Ljava/io/File;)Z

    move-result v3

    goto :goto_0
.end method

.method protected abstract selectionTest(Ljava/io/File;Ljava/io/File;)Z
.end method

.method public setGranularity(I)V
    .locals 0
    .param p1    # I

    iput p1, p0, Lorg/apache/tools/ant/types/selectors/MappingSelector;->granularity:I

    return-void
.end method

.method public setTargetdir(Ljava/io/File;)V
    .locals 0
    .param p1    # Ljava/io/File;

    iput-object p1, p0, Lorg/apache/tools/ant/types/selectors/MappingSelector;->targetdir:Ljava/io/File;

    return-void
.end method

.method public verifySettings()V
    .locals 1

    iget-object v0, p0, Lorg/apache/tools/ant/types/selectors/MappingSelector;->targetdir:Ljava/io/File;

    if-nez v0, :cond_0

    const-string v0, "The targetdir attribute is required."

    invoke-virtual {p0, v0}, Lorg/apache/tools/ant/types/selectors/MappingSelector;->setError(Ljava/lang/String;)V

    :cond_0
    iget-object v0, p0, Lorg/apache/tools/ant/types/selectors/MappingSelector;->mapperElement:Lorg/apache/tools/ant/types/Mapper;

    if-nez v0, :cond_2

    new-instance v0, Lorg/apache/tools/ant/util/IdentityMapper;

    invoke-direct {v0}, Lorg/apache/tools/ant/util/IdentityMapper;-><init>()V

    iput-object v0, p0, Lorg/apache/tools/ant/types/selectors/MappingSelector;->map:Lorg/apache/tools/ant/util/FileNameMapper;

    :goto_0
    iget-object v0, p0, Lorg/apache/tools/ant/types/selectors/MappingSelector;->map:Lorg/apache/tools/ant/util/FileNameMapper;

    if-nez v0, :cond_1

    const-string v0, "Could not set <mapper> element."

    invoke-virtual {p0, v0}, Lorg/apache/tools/ant/types/selectors/MappingSelector;->setError(Ljava/lang/String;)V

    :cond_1
    return-void

    :cond_2
    iget-object v0, p0, Lorg/apache/tools/ant/types/selectors/MappingSelector;->mapperElement:Lorg/apache/tools/ant/types/Mapper;

    invoke-virtual {v0}, Lorg/apache/tools/ant/types/Mapper;->getImplementation()Lorg/apache/tools/ant/util/FileNameMapper;

    move-result-object v0

    iput-object v0, p0, Lorg/apache/tools/ant/types/selectors/MappingSelector;->map:Lorg/apache/tools/ant/util/FileNameMapper;

    goto :goto_0
.end method
