.class public Lorg/apache/tools/ant/types/selectors/DateSelector;
.super Lorg/apache/tools/ant/types/selectors/BaseExtendSelector;
.source "DateSelector.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lorg/apache/tools/ant/types/selectors/DateSelector$TimeComparisons;
    }
.end annotation


# static fields
.field public static final CHECKDIRS_KEY:Ljava/lang/String; = "checkdirs"

.field public static final DATETIME_KEY:Ljava/lang/String; = "datetime"

.field private static final FILE_UTILS:Lorg/apache/tools/ant/util/FileUtils;

.field public static final GRANULARITY_KEY:Ljava/lang/String; = "granularity"

.field public static final MILLIS_KEY:Ljava/lang/String; = "millis"

.field public static final PATTERN_KEY:Ljava/lang/String; = "pattern"

.field public static final WHEN_KEY:Ljava/lang/String; = "when"


# instance fields
.field private dateTime:Ljava/lang/String;

.field private granularity:J

.field private includeDirs:Z

.field private millis:J

.field private pattern:Ljava/lang/String;

.field private when:Lorg/apache/tools/ant/types/TimeComparison;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    invoke-static {}, Lorg/apache/tools/ant/util/FileUtils;->getFileUtils()Lorg/apache/tools/ant/util/FileUtils;

    move-result-object v0

    sput-object v0, Lorg/apache/tools/ant/types/selectors/DateSelector;->FILE_UTILS:Lorg/apache/tools/ant/util/FileUtils;

    return-void
.end method

.method public constructor <init>()V
    .locals 2

    invoke-direct {p0}, Lorg/apache/tools/ant/types/selectors/BaseExtendSelector;-><init>()V

    const-wide/16 v0, -0x1

    iput-wide v0, p0, Lorg/apache/tools/ant/types/selectors/DateSelector;->millis:J

    const/4 v0, 0x0

    iput-object v0, p0, Lorg/apache/tools/ant/types/selectors/DateSelector;->dateTime:Ljava/lang/String;

    const/4 v0, 0x0

    iput-boolean v0, p0, Lorg/apache/tools/ant/types/selectors/DateSelector;->includeDirs:Z

    const-wide/16 v0, 0x0

    iput-wide v0, p0, Lorg/apache/tools/ant/types/selectors/DateSelector;->granularity:J

    sget-object v0, Lorg/apache/tools/ant/types/TimeComparison;->EQUAL:Lorg/apache/tools/ant/types/TimeComparison;

    iput-object v0, p0, Lorg/apache/tools/ant/types/selectors/DateSelector;->when:Lorg/apache/tools/ant/types/TimeComparison;

    sget-object v0, Lorg/apache/tools/ant/types/selectors/DateSelector;->FILE_UTILS:Lorg/apache/tools/ant/util/FileUtils;

    invoke-virtual {v0}, Lorg/apache/tools/ant/util/FileUtils;->getFileTimestampGranularity()J

    move-result-wide v0

    iput-wide v0, p0, Lorg/apache/tools/ant/types/selectors/DateSelector;->granularity:J

    return-void
.end method


# virtual methods
.method public getMillis()J
    .locals 2

    iget-object v0, p0, Lorg/apache/tools/ant/types/selectors/DateSelector;->dateTime:Ljava/lang/String;

    if-eqz v0, :cond_0

    invoke-virtual {p0}, Lorg/apache/tools/ant/types/selectors/DateSelector;->validate()V

    :cond_0
    iget-wide v0, p0, Lorg/apache/tools/ant/types/selectors/DateSelector;->millis:J

    return-wide v0
.end method

.method public isSelected(Ljava/io/File;Ljava/lang/String;Ljava/io/File;)Z
    .locals 7
    .param p1    # Ljava/io/File;
    .param p2    # Ljava/lang/String;
    .param p3    # Ljava/io/File;

    invoke-virtual {p0}, Lorg/apache/tools/ant/types/selectors/DateSelector;->validate()V

    invoke-virtual {p3}, Ljava/io/File;->isDirectory()Z

    move-result v0

    if-eqz v0, :cond_0

    iget-boolean v0, p0, Lorg/apache/tools/ant/types/selectors/DateSelector;->includeDirs:Z

    if-eqz v0, :cond_1

    :cond_0
    iget-object v0, p0, Lorg/apache/tools/ant/types/selectors/DateSelector;->when:Lorg/apache/tools/ant/types/TimeComparison;

    invoke-virtual {p3}, Ljava/io/File;->lastModified()J

    move-result-wide v1

    iget-wide v3, p0, Lorg/apache/tools/ant/types/selectors/DateSelector;->millis:J

    iget-wide v5, p0, Lorg/apache/tools/ant/types/selectors/DateSelector;->granularity:J

    invoke-virtual/range {v0 .. v6}, Lorg/apache/tools/ant/types/TimeComparison;->evaluate(JJJ)Z

    move-result v0

    if-eqz v0, :cond_2

    :cond_1
    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_2
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public setCheckdirs(Z)V
    .locals 0
    .param p1    # Z

    iput-boolean p1, p0, Lorg/apache/tools/ant/types/selectors/DateSelector;->includeDirs:Z

    return-void
.end method

.method public setDatetime(Ljava/lang/String;)V
    .locals 2
    .param p1    # Ljava/lang/String;

    iput-object p1, p0, Lorg/apache/tools/ant/types/selectors/DateSelector;->dateTime:Ljava/lang/String;

    const-wide/16 v0, -0x1

    iput-wide v0, p0, Lorg/apache/tools/ant/types/selectors/DateSelector;->millis:J

    return-void
.end method

.method public setGranularity(I)V
    .locals 2
    .param p1    # I

    int-to-long v0, p1

    iput-wide v0, p0, Lorg/apache/tools/ant/types/selectors/DateSelector;->granularity:J

    return-void
.end method

.method public setMillis(J)V
    .locals 0
    .param p1    # J

    iput-wide p1, p0, Lorg/apache/tools/ant/types/selectors/DateSelector;->millis:J

    return-void
.end method

.method public setParameters([Lorg/apache/tools/ant/types/Parameter;)V
    .locals 5
    .param p1    # [Lorg/apache/tools/ant/types/Parameter;

    invoke-super {p0, p1}, Lorg/apache/tools/ant/types/selectors/BaseExtendSelector;->setParameters([Lorg/apache/tools/ant/types/Parameter;)V

    if-eqz p1, :cond_6

    const/4 v0, 0x0

    :goto_0
    array-length v3, p1

    if-ge v0, v3, :cond_6

    aget-object v3, p1, v0

    invoke-virtual {v3}, Lorg/apache/tools/ant/types/Parameter;->getName()Ljava/lang/String;

    move-result-object v2

    const-string v3, "millis"

    invoke-virtual {v3, v2}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v3

    if-eqz v3, :cond_0

    :try_start_0
    new-instance v3, Ljava/lang/Long;

    aget-object v4, p1, v0

    invoke-virtual {v4}, Lorg/apache/tools/ant/types/Parameter;->getValue()Ljava/lang/String;

    move-result-object v4

    invoke-direct {v3, v4}, Ljava/lang/Long;-><init>(Ljava/lang/String;)V

    invoke-virtual {v3}, Ljava/lang/Long;->longValue()J

    move-result-wide v3

    invoke-virtual {p0, v3, v4}, Lorg/apache/tools/ant/types/selectors/DateSelector;->setMillis(J)V
    :try_end_0
    .catch Ljava/lang/NumberFormatException; {:try_start_0 .. :try_end_0} :catch_0

    :goto_1
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    :catch_0
    move-exception v1

    new-instance v3, Ljava/lang/StringBuffer;

    invoke-direct {v3}, Ljava/lang/StringBuffer;-><init>()V

    const-string v4, "Invalid millisecond setting "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    move-result-object v3

    aget-object v4, p1, v0

    invoke-virtual {v4}, Lorg/apache/tools/ant/types/Parameter;->getValue()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v3, v4}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuffer;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {p0, v3}, Lorg/apache/tools/ant/types/selectors/DateSelector;->setError(Ljava/lang/String;)V

    goto :goto_1

    :cond_0
    const-string v3, "datetime"

    invoke-virtual {v3, v2}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v3

    if-eqz v3, :cond_1

    aget-object v3, p1, v0

    invoke-virtual {v3}, Lorg/apache/tools/ant/types/Parameter;->getValue()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {p0, v3}, Lorg/apache/tools/ant/types/selectors/DateSelector;->setDatetime(Ljava/lang/String;)V

    goto :goto_1

    :cond_1
    const-string v3, "checkdirs"

    invoke-virtual {v3, v2}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v3

    if-eqz v3, :cond_2

    aget-object v3, p1, v0

    invoke-virtual {v3}, Lorg/apache/tools/ant/types/Parameter;->getValue()Ljava/lang/String;

    move-result-object v3

    invoke-static {v3}, Lorg/apache/tools/ant/Project;->toBoolean(Ljava/lang/String;)Z

    move-result v3

    invoke-virtual {p0, v3}, Lorg/apache/tools/ant/types/selectors/DateSelector;->setCheckdirs(Z)V

    goto :goto_1

    :cond_2
    const-string v3, "granularity"

    invoke-virtual {v3, v2}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v3

    if-eqz v3, :cond_3

    :try_start_1
    new-instance v3, Ljava/lang/Integer;

    aget-object v4, p1, v0

    invoke-virtual {v4}, Lorg/apache/tools/ant/types/Parameter;->getValue()Ljava/lang/String;

    move-result-object v4

    invoke-direct {v3, v4}, Ljava/lang/Integer;-><init>(Ljava/lang/String;)V

    invoke-virtual {v3}, Ljava/lang/Integer;->intValue()I

    move-result v3

    invoke-virtual {p0, v3}, Lorg/apache/tools/ant/types/selectors/DateSelector;->setGranularity(I)V
    :try_end_1
    .catch Ljava/lang/NumberFormatException; {:try_start_1 .. :try_end_1} :catch_1

    goto :goto_1

    :catch_1
    move-exception v1

    new-instance v3, Ljava/lang/StringBuffer;

    invoke-direct {v3}, Ljava/lang/StringBuffer;-><init>()V

    const-string v4, "Invalid granularity setting "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    move-result-object v3

    aget-object v4, p1, v0

    invoke-virtual {v4}, Lorg/apache/tools/ant/types/Parameter;->getValue()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v3, v4}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuffer;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {p0, v3}, Lorg/apache/tools/ant/types/selectors/DateSelector;->setError(Ljava/lang/String;)V

    goto/16 :goto_1

    :cond_3
    const-string v3, "when"

    invoke-virtual {v3, v2}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v3

    if-eqz v3, :cond_4

    new-instance v3, Lorg/apache/tools/ant/types/TimeComparison;

    aget-object v4, p1, v0

    invoke-virtual {v4}, Lorg/apache/tools/ant/types/Parameter;->getValue()Ljava/lang/String;

    move-result-object v4

    invoke-direct {v3, v4}, Lorg/apache/tools/ant/types/TimeComparison;-><init>(Ljava/lang/String;)V

    invoke-virtual {p0, v3}, Lorg/apache/tools/ant/types/selectors/DateSelector;->setWhen(Lorg/apache/tools/ant/types/TimeComparison;)V

    goto/16 :goto_1

    :cond_4
    const-string v3, "pattern"

    invoke-virtual {v3, v2}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v3

    if-eqz v3, :cond_5

    aget-object v3, p1, v0

    invoke-virtual {v3}, Lorg/apache/tools/ant/types/Parameter;->getValue()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {p0, v3}, Lorg/apache/tools/ant/types/selectors/DateSelector;->setPattern(Ljava/lang/String;)V

    goto/16 :goto_1

    :cond_5
    new-instance v3, Ljava/lang/StringBuffer;

    invoke-direct {v3}, Ljava/lang/StringBuffer;-><init>()V

    const-string v4, "Invalid parameter "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    move-result-object v3

    invoke-virtual {v3, v2}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuffer;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {p0, v3}, Lorg/apache/tools/ant/types/selectors/DateSelector;->setError(Ljava/lang/String;)V

    goto/16 :goto_1

    :cond_6
    return-void
.end method

.method public setPattern(Ljava/lang/String;)V
    .locals 0
    .param p1    # Ljava/lang/String;

    iput-object p1, p0, Lorg/apache/tools/ant/types/selectors/DateSelector;->pattern:Ljava/lang/String;

    return-void
.end method

.method public setWhen(Lorg/apache/tools/ant/types/TimeComparison;)V
    .locals 0
    .param p1    # Lorg/apache/tools/ant/types/TimeComparison;

    iput-object p1, p0, Lorg/apache/tools/ant/types/selectors/DateSelector;->when:Lorg/apache/tools/ant/types/TimeComparison;

    return-void
.end method

.method public setWhen(Lorg/apache/tools/ant/types/selectors/DateSelector$TimeComparisons;)V
    .locals 0
    .param p1    # Lorg/apache/tools/ant/types/selectors/DateSelector$TimeComparisons;

    invoke-virtual {p0, p1}, Lorg/apache/tools/ant/types/selectors/DateSelector;->setWhen(Lorg/apache/tools/ant/types/TimeComparison;)V

    return-void
.end method

.method public toString()Ljava/lang/String;
    .locals 3

    new-instance v0, Ljava/lang/StringBuffer;

    const-string v1, "{dateselector date: "

    invoke-direct {v0, v1}, Ljava/lang/StringBuffer;-><init>(Ljava/lang/String;)V

    iget-object v1, p0, Lorg/apache/tools/ant/types/selectors/DateSelector;->dateTime:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    const-string v1, " compare: "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    move-result-object v1

    iget-object v2, p0, Lorg/apache/tools/ant/types/selectors/DateSelector;->when:Lorg/apache/tools/ant/types/TimeComparison;

    invoke-virtual {v2}, Lorg/apache/tools/ant/types/TimeComparison;->getValue()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    const-string v1, " granularity: "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    iget-wide v1, p0, Lorg/apache/tools/ant/types/selectors/DateSelector;->granularity:J

    invoke-virtual {v0, v1, v2}, Ljava/lang/StringBuffer;->append(J)Ljava/lang/StringBuffer;

    iget-object v1, p0, Lorg/apache/tools/ant/types/selectors/DateSelector;->pattern:Ljava/lang/String;

    if-eqz v1, :cond_0

    const-string v1, " pattern: "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    move-result-object v1

    iget-object v2, p0, Lorg/apache/tools/ant/types/selectors/DateSelector;->pattern:Ljava/lang/String;

    invoke-virtual {v1, v2}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    :cond_0
    const-string v1, "}"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    invoke-virtual {v0}, Ljava/lang/StringBuffer;->toString()Ljava/lang/String;

    move-result-object v1

    return-object v1
.end method

.method public verifySettings()V
    .locals 7

    const/4 v6, 0x3

    const-wide/16 v4, 0x0

    iget-object v2, p0, Lorg/apache/tools/ant/types/selectors/DateSelector;->dateTime:Ljava/lang/String;

    if-nez v2, :cond_1

    iget-wide v2, p0, Lorg/apache/tools/ant/types/selectors/DateSelector;->millis:J

    cmp-long v2, v2, v4

    if-gez v2, :cond_1

    const-string v2, "You must provide a datetime or the number of milliseconds."

    invoke-virtual {p0, v2}, Lorg/apache/tools/ant/types/selectors/DateSelector;->setError(Ljava/lang/String;)V

    :cond_0
    :goto_0
    return-void

    :cond_1
    iget-wide v2, p0, Lorg/apache/tools/ant/types/selectors/DateSelector;->millis:J

    cmp-long v2, v2, v4

    if-gez v2, :cond_0

    iget-object v2, p0, Lorg/apache/tools/ant/types/selectors/DateSelector;->dateTime:Ljava/lang/String;

    if-eqz v2, :cond_0

    iget-object v2, p0, Lorg/apache/tools/ant/types/selectors/DateSelector;->pattern:Ljava/lang/String;

    if-nez v2, :cond_2

    sget-object v2, Ljava/util/Locale;->US:Ljava/util/Locale;

    invoke-static {v6, v6, v2}, Ljava/text/DateFormat;->getDateTimeInstance(IILjava/util/Locale;)Ljava/text/DateFormat;

    move-result-object v0

    :goto_1
    :try_start_0
    iget-object v2, p0, Lorg/apache/tools/ant/types/selectors/DateSelector;->dateTime:Ljava/lang/String;

    invoke-virtual {v0, v2}, Ljava/text/DateFormat;->parse(Ljava/lang/String;)Ljava/util/Date;

    move-result-object v2

    invoke-virtual {v2}, Ljava/util/Date;->getTime()J

    move-result-wide v2

    invoke-virtual {p0, v2, v3}, Lorg/apache/tools/ant/types/selectors/DateSelector;->setMillis(J)V

    iget-wide v2, p0, Lorg/apache/tools/ant/types/selectors/DateSelector;->millis:J

    cmp-long v2, v2, v4

    if-gez v2, :cond_0

    new-instance v2, Ljava/lang/StringBuffer;

    invoke-direct {v2}, Ljava/lang/StringBuffer;-><init>()V

    const-string v3, "Date of "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    move-result-object v2

    iget-object v3, p0, Lorg/apache/tools/ant/types/selectors/DateSelector;->dateTime:Ljava/lang/String;

    invoke-virtual {v2, v3}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    move-result-object v2

    const-string v3, " results in negative milliseconds value"

    invoke-virtual {v2, v3}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    move-result-object v2

    const-string v3, " relative to epoch (January 1, 1970, 00:00:00 GMT)."

    invoke-virtual {v2, v3}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuffer;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {p0, v2}, Lorg/apache/tools/ant/types/selectors/DateSelector;->setError(Ljava/lang/String;)V
    :try_end_0
    .catch Ljava/text/ParseException; {:try_start_0 .. :try_end_0} :catch_0

    goto :goto_0

    :catch_0
    move-exception v1

    new-instance v2, Ljava/lang/StringBuffer;

    invoke-direct {v2}, Ljava/lang/StringBuffer;-><init>()V

    const-string v3, "Date of "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    move-result-object v2

    iget-object v3, p0, Lorg/apache/tools/ant/types/selectors/DateSelector;->dateTime:Ljava/lang/String;

    invoke-virtual {v2, v3}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    move-result-object v2

    const-string v3, " Cannot be parsed correctly. It should be in"

    invoke-virtual {v2, v3}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    move-result-object v3

    iget-object v2, p0, Lorg/apache/tools/ant/types/selectors/DateSelector;->pattern:Ljava/lang/String;

    if-nez v2, :cond_3

    const-string v2, " MM/DD/YYYY HH:MM AM_PM"

    :goto_2
    invoke-virtual {v3, v2}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    move-result-object v2

    const-string v3, " format."

    invoke-virtual {v2, v3}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuffer;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {p0, v2}, Lorg/apache/tools/ant/types/selectors/DateSelector;->setError(Ljava/lang/String;)V

    goto :goto_0

    :cond_2
    new-instance v0, Ljava/text/SimpleDateFormat;

    iget-object v2, p0, Lorg/apache/tools/ant/types/selectors/DateSelector;->pattern:Ljava/lang/String;

    invoke-direct {v0, v2}, Ljava/text/SimpleDateFormat;-><init>(Ljava/lang/String;)V

    goto :goto_1

    :cond_3
    iget-object v2, p0, Lorg/apache/tools/ant/types/selectors/DateSelector;->pattern:Ljava/lang/String;

    goto :goto_2
.end method
