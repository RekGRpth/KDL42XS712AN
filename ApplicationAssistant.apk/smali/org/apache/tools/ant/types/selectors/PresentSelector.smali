.class public Lorg/apache/tools/ant/types/selectors/PresentSelector;
.super Lorg/apache/tools/ant/types/selectors/BaseSelector;
.source "PresentSelector.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lorg/apache/tools/ant/types/selectors/PresentSelector$FilePresence;
    }
.end annotation


# instance fields
.field private destmustexist:Z

.field private map:Lorg/apache/tools/ant/util/FileNameMapper;

.field private mapperElement:Lorg/apache/tools/ant/types/Mapper;

.field private targetdir:Ljava/io/File;


# direct methods
.method public constructor <init>()V
    .locals 1

    const/4 v0, 0x0

    invoke-direct {p0}, Lorg/apache/tools/ant/types/selectors/BaseSelector;-><init>()V

    iput-object v0, p0, Lorg/apache/tools/ant/types/selectors/PresentSelector;->targetdir:Ljava/io/File;

    iput-object v0, p0, Lorg/apache/tools/ant/types/selectors/PresentSelector;->mapperElement:Lorg/apache/tools/ant/types/Mapper;

    iput-object v0, p0, Lorg/apache/tools/ant/types/selectors/PresentSelector;->map:Lorg/apache/tools/ant/util/FileNameMapper;

    const/4 v0, 0x1

    iput-boolean v0, p0, Lorg/apache/tools/ant/types/selectors/PresentSelector;->destmustexist:Z

    return-void
.end method


# virtual methods
.method public createMapper()Lorg/apache/tools/ant/types/Mapper;
    .locals 2
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lorg/apache/tools/ant/BuildException;
        }
    .end annotation

    iget-object v0, p0, Lorg/apache/tools/ant/types/selectors/PresentSelector;->mapperElement:Lorg/apache/tools/ant/types/Mapper;

    if-eqz v0, :cond_0

    new-instance v0, Lorg/apache/tools/ant/BuildException;

    const-string v1, "Cannot define more than one mapper"

    invoke-direct {v0, v1}, Lorg/apache/tools/ant/BuildException;-><init>(Ljava/lang/String;)V

    throw v0

    :cond_0
    new-instance v0, Lorg/apache/tools/ant/types/Mapper;

    invoke-virtual {p0}, Lorg/apache/tools/ant/types/selectors/PresentSelector;->getProject()Lorg/apache/tools/ant/Project;

    move-result-object v1

    invoke-direct {v0, v1}, Lorg/apache/tools/ant/types/Mapper;-><init>(Lorg/apache/tools/ant/Project;)V

    iput-object v0, p0, Lorg/apache/tools/ant/types/selectors/PresentSelector;->mapperElement:Lorg/apache/tools/ant/types/Mapper;

    iget-object v0, p0, Lorg/apache/tools/ant/types/selectors/PresentSelector;->mapperElement:Lorg/apache/tools/ant/types/Mapper;

    return-object v0
.end method

.method public isSelected(Ljava/io/File;Ljava/lang/String;Ljava/io/File;)Z
    .locals 7
    .param p1    # Ljava/io/File;
    .param p2    # Ljava/lang/String;
    .param p3    # Ljava/io/File;

    const/4 v3, 0x1

    const/4 v4, 0x0

    invoke-virtual {p0}, Lorg/apache/tools/ant/types/selectors/PresentSelector;->validate()V

    iget-object v5, p0, Lorg/apache/tools/ant/types/selectors/PresentSelector;->map:Lorg/apache/tools/ant/util/FileNameMapper;

    invoke-interface {v5, p2}, Lorg/apache/tools/ant/util/FileNameMapper;->mapFileName(Ljava/lang/String;)[Ljava/lang/String;

    move-result-object v1

    if-nez v1, :cond_0

    :goto_0
    return v4

    :cond_0
    array-length v5, v1

    if-ne v5, v3, :cond_1

    aget-object v5, v1, v4

    if-nez v5, :cond_2

    :cond_1
    new-instance v3, Lorg/apache/tools/ant/BuildException;

    new-instance v4, Ljava/lang/StringBuffer;

    invoke-direct {v4}, Ljava/lang/StringBuffer;-><init>()V

    const-string v5, "Invalid destination file results for "

    invoke-virtual {v4, v5}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    move-result-object v4

    iget-object v5, p0, Lorg/apache/tools/ant/types/selectors/PresentSelector;->targetdir:Ljava/io/File;

    invoke-virtual {v4, v5}, Ljava/lang/StringBuffer;->append(Ljava/lang/Object;)Ljava/lang/StringBuffer;

    move-result-object v4

    const-string v5, " with filename "

    invoke-virtual {v4, v5}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    move-result-object v4

    invoke-virtual {v4, p2}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuffer;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-direct {v3, v4}, Lorg/apache/tools/ant/BuildException;-><init>(Ljava/lang/String;)V

    throw v3

    :cond_2
    aget-object v2, v1, v4

    new-instance v0, Ljava/io/File;

    iget-object v5, p0, Lorg/apache/tools/ant/types/selectors/PresentSelector;->targetdir:Ljava/io/File;

    invoke-direct {v0, v5, v2}, Ljava/io/File;-><init>(Ljava/io/File;Ljava/lang/String;)V

    invoke-virtual {v0}, Ljava/io/File;->exists()Z

    move-result v5

    iget-boolean v6, p0, Lorg/apache/tools/ant/types/selectors/PresentSelector;->destmustexist:Z

    if-ne v5, v6, :cond_3

    :goto_1
    move v4, v3

    goto :goto_0

    :cond_3
    move v3, v4

    goto :goto_1
.end method

.method public setPresent(Lorg/apache/tools/ant/types/selectors/PresentSelector$FilePresence;)V
    .locals 1
    .param p1    # Lorg/apache/tools/ant/types/selectors/PresentSelector$FilePresence;

    invoke-virtual {p1}, Lorg/apache/tools/ant/types/selectors/PresentSelector$FilePresence;->getIndex()I

    move-result v0

    if-nez v0, :cond_0

    const/4 v0, 0x0

    iput-boolean v0, p0, Lorg/apache/tools/ant/types/selectors/PresentSelector;->destmustexist:Z

    :cond_0
    return-void
.end method

.method public setTargetdir(Ljava/io/File;)V
    .locals 0
    .param p1    # Ljava/io/File;

    iput-object p1, p0, Lorg/apache/tools/ant/types/selectors/PresentSelector;->targetdir:Ljava/io/File;

    return-void
.end method

.method public toString()Ljava/lang/String;
    .locals 2

    new-instance v0, Ljava/lang/StringBuffer;

    const-string v1, "{presentselector targetdir: "

    invoke-direct {v0, v1}, Ljava/lang/StringBuffer;-><init>(Ljava/lang/String;)V

    iget-object v1, p0, Lorg/apache/tools/ant/types/selectors/PresentSelector;->targetdir:Ljava/io/File;

    if-nez v1, :cond_1

    const-string v1, "NOT YET SET"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    :goto_0
    const-string v1, " present: "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    iget-boolean v1, p0, Lorg/apache/tools/ant/types/selectors/PresentSelector;->destmustexist:Z

    if-eqz v1, :cond_2

    const-string v1, "both"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    :goto_1
    iget-object v1, p0, Lorg/apache/tools/ant/types/selectors/PresentSelector;->map:Lorg/apache/tools/ant/util/FileNameMapper;

    if-eqz v1, :cond_3

    iget-object v1, p0, Lorg/apache/tools/ant/types/selectors/PresentSelector;->map:Lorg/apache/tools/ant/util/FileNameMapper;

    invoke-virtual {v1}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    :cond_0
    :goto_2
    const-string v1, "}"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    invoke-virtual {v0}, Ljava/lang/StringBuffer;->toString()Ljava/lang/String;

    move-result-object v1

    return-object v1

    :cond_1
    iget-object v1, p0, Lorg/apache/tools/ant/types/selectors/PresentSelector;->targetdir:Ljava/io/File;

    invoke-virtual {v1}, Ljava/io/File;->getName()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    goto :goto_0

    :cond_2
    const-string v1, "srconly"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    goto :goto_1

    :cond_3
    iget-object v1, p0, Lorg/apache/tools/ant/types/selectors/PresentSelector;->mapperElement:Lorg/apache/tools/ant/types/Mapper;

    if-eqz v1, :cond_0

    iget-object v1, p0, Lorg/apache/tools/ant/types/selectors/PresentSelector;->mapperElement:Lorg/apache/tools/ant/types/Mapper;

    invoke-virtual {v1}, Lorg/apache/tools/ant/types/Mapper;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    goto :goto_2
.end method

.method public verifySettings()V
    .locals 1

    iget-object v0, p0, Lorg/apache/tools/ant/types/selectors/PresentSelector;->targetdir:Ljava/io/File;

    if-nez v0, :cond_0

    const-string v0, "The targetdir attribute is required."

    invoke-virtual {p0, v0}, Lorg/apache/tools/ant/types/selectors/PresentSelector;->setError(Ljava/lang/String;)V

    :cond_0
    iget-object v0, p0, Lorg/apache/tools/ant/types/selectors/PresentSelector;->mapperElement:Lorg/apache/tools/ant/types/Mapper;

    if-nez v0, :cond_2

    new-instance v0, Lorg/apache/tools/ant/util/IdentityMapper;

    invoke-direct {v0}, Lorg/apache/tools/ant/util/IdentityMapper;-><init>()V

    iput-object v0, p0, Lorg/apache/tools/ant/types/selectors/PresentSelector;->map:Lorg/apache/tools/ant/util/FileNameMapper;

    :goto_0
    iget-object v0, p0, Lorg/apache/tools/ant/types/selectors/PresentSelector;->map:Lorg/apache/tools/ant/util/FileNameMapper;

    if-nez v0, :cond_1

    const-string v0, "Could not set <mapper> element."

    invoke-virtual {p0, v0}, Lorg/apache/tools/ant/types/selectors/PresentSelector;->setError(Ljava/lang/String;)V

    :cond_1
    return-void

    :cond_2
    iget-object v0, p0, Lorg/apache/tools/ant/types/selectors/PresentSelector;->mapperElement:Lorg/apache/tools/ant/types/Mapper;

    invoke-virtual {v0}, Lorg/apache/tools/ant/types/Mapper;->getImplementation()Lorg/apache/tools/ant/util/FileNameMapper;

    move-result-object v0

    iput-object v0, p0, Lorg/apache/tools/ant/types/selectors/PresentSelector;->map:Lorg/apache/tools/ant/util/FileNameMapper;

    goto :goto_0
.end method
