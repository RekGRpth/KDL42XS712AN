.class public Lorg/apache/tools/ant/types/Commandline$Marker;
.super Ljava/lang/Object;
.source "Commandline.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lorg/apache/tools/ant/types/Commandline;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x1
    name = "Marker"
.end annotation


# instance fields
.field private position:I

.field private realPos:I

.field private final this$0:Lorg/apache/tools/ant/types/Commandline;


# direct methods
.method constructor <init>(Lorg/apache/tools/ant/types/Commandline;I)V
    .locals 1
    .param p2    # I

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    iput-object p1, p0, Lorg/apache/tools/ant/types/Commandline$Marker;->this$0:Lorg/apache/tools/ant/types/Commandline;

    const/4 v0, -0x1

    iput v0, p0, Lorg/apache/tools/ant/types/Commandline$Marker;->realPos:I

    iput p2, p0, Lorg/apache/tools/ant/types/Commandline$Marker;->position:I

    return-void
.end method


# virtual methods
.method public getPosition()I
    .locals 4

    iget v2, p0, Lorg/apache/tools/ant/types/Commandline$Marker;->realPos:I

    const/4 v3, -0x1

    if-ne v2, v3, :cond_1

    iget-object v2, p0, Lorg/apache/tools/ant/types/Commandline$Marker;->this$0:Lorg/apache/tools/ant/types/Commandline;

    invoke-static {v2}, Lorg/apache/tools/ant/types/Commandline;->access$000(Lorg/apache/tools/ant/types/Commandline;)Ljava/lang/String;

    move-result-object v2

    if-nez v2, :cond_0

    const/4 v2, 0x0

    :goto_0
    iput v2, p0, Lorg/apache/tools/ant/types/Commandline$Marker;->realPos:I

    const/4 v1, 0x0

    :goto_1
    iget v2, p0, Lorg/apache/tools/ant/types/Commandline$Marker;->position:I

    if-ge v1, v2, :cond_1

    iget-object v2, p0, Lorg/apache/tools/ant/types/Commandline$Marker;->this$0:Lorg/apache/tools/ant/types/Commandline;

    invoke-static {v2}, Lorg/apache/tools/ant/types/Commandline;->access$100(Lorg/apache/tools/ant/types/Commandline;)Ljava/util/Vector;

    move-result-object v2

    invoke-virtual {v2, v1}, Ljava/util/Vector;->elementAt(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lorg/apache/tools/ant/types/Commandline$Argument;

    iget v2, p0, Lorg/apache/tools/ant/types/Commandline$Marker;->realPos:I

    invoke-virtual {v0}, Lorg/apache/tools/ant/types/Commandline$Argument;->getParts()[Ljava/lang/String;

    move-result-object v3

    array-length v3, v3

    add-int/2addr v2, v3

    iput v2, p0, Lorg/apache/tools/ant/types/Commandline$Marker;->realPos:I

    add-int/lit8 v1, v1, 0x1

    goto :goto_1

    :cond_0
    const/4 v2, 0x1

    goto :goto_0

    :cond_1
    iget v2, p0, Lorg/apache/tools/ant/types/Commandline$Marker;->realPos:I

    return v2
.end method
