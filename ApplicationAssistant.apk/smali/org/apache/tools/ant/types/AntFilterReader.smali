.class public final Lorg/apache/tools/ant/types/AntFilterReader;
.super Lorg/apache/tools/ant/types/DataType;
.source "AntFilterReader.java"

# interfaces
.implements Ljava/lang/Cloneable;


# instance fields
.field private className:Ljava/lang/String;

.field private classpath:Lorg/apache/tools/ant/types/Path;

.field private final parameters:Ljava/util/Vector;


# direct methods
.method public constructor <init>()V
    .locals 1

    invoke-direct {p0}, Lorg/apache/tools/ant/types/DataType;-><init>()V

    new-instance v0, Ljava/util/Vector;

    invoke-direct {v0}, Ljava/util/Vector;-><init>()V

    iput-object v0, p0, Lorg/apache/tools/ant/types/AntFilterReader;->parameters:Ljava/util/Vector;

    return-void
.end method


# virtual methods
.method public addParam(Lorg/apache/tools/ant/types/Parameter;)V
    .locals 1
    .param p1    # Lorg/apache/tools/ant/types/Parameter;

    iget-object v0, p0, Lorg/apache/tools/ant/types/AntFilterReader;->parameters:Ljava/util/Vector;

    invoke-virtual {v0, p1}, Ljava/util/Vector;->addElement(Ljava/lang/Object;)V

    return-void
.end method

.method public createClasspath()Lorg/apache/tools/ant/types/Path;
    .locals 2

    invoke-virtual {p0}, Lorg/apache/tools/ant/types/AntFilterReader;->isReference()Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-virtual {p0}, Lorg/apache/tools/ant/types/AntFilterReader;->noChildrenAllowed()Lorg/apache/tools/ant/BuildException;

    move-result-object v0

    throw v0

    :cond_0
    iget-object v0, p0, Lorg/apache/tools/ant/types/AntFilterReader;->classpath:Lorg/apache/tools/ant/types/Path;

    if-nez v0, :cond_1

    new-instance v0, Lorg/apache/tools/ant/types/Path;

    invoke-virtual {p0}, Lorg/apache/tools/ant/types/AntFilterReader;->getProject()Lorg/apache/tools/ant/Project;

    move-result-object v1

    invoke-direct {v0, v1}, Lorg/apache/tools/ant/types/Path;-><init>(Lorg/apache/tools/ant/Project;)V

    iput-object v0, p0, Lorg/apache/tools/ant/types/AntFilterReader;->classpath:Lorg/apache/tools/ant/types/Path;

    :cond_1
    iget-object v0, p0, Lorg/apache/tools/ant/types/AntFilterReader;->classpath:Lorg/apache/tools/ant/types/Path;

    invoke-virtual {v0}, Lorg/apache/tools/ant/types/Path;->createPath()Lorg/apache/tools/ant/types/Path;

    move-result-object v0

    return-object v0
.end method

.method public getClassName()Ljava/lang/String;
    .locals 1

    iget-object v0, p0, Lorg/apache/tools/ant/types/AntFilterReader;->className:Ljava/lang/String;

    return-object v0
.end method

.method public getClasspath()Lorg/apache/tools/ant/types/Path;
    .locals 1

    iget-object v0, p0, Lorg/apache/tools/ant/types/AntFilterReader;->classpath:Lorg/apache/tools/ant/types/Path;

    return-object v0
.end method

.method public getParams()[Lorg/apache/tools/ant/types/Parameter;
    .locals 2

    iget-object v1, p0, Lorg/apache/tools/ant/types/AntFilterReader;->parameters:Ljava/util/Vector;

    invoke-virtual {v1}, Ljava/util/Vector;->size()I

    move-result v1

    new-array v0, v1, [Lorg/apache/tools/ant/types/Parameter;

    iget-object v1, p0, Lorg/apache/tools/ant/types/AntFilterReader;->parameters:Ljava/util/Vector;

    invoke-virtual {v1, v0}, Ljava/util/Vector;->copyInto([Ljava/lang/Object;)V

    return-object v0
.end method

.method public setClassName(Ljava/lang/String;)V
    .locals 0
    .param p1    # Ljava/lang/String;

    iput-object p1, p0, Lorg/apache/tools/ant/types/AntFilterReader;->className:Ljava/lang/String;

    return-void
.end method

.method public setClasspath(Lorg/apache/tools/ant/types/Path;)V
    .locals 1
    .param p1    # Lorg/apache/tools/ant/types/Path;

    invoke-virtual {p0}, Lorg/apache/tools/ant/types/AntFilterReader;->isReference()Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-virtual {p0}, Lorg/apache/tools/ant/types/AntFilterReader;->tooManyAttributes()Lorg/apache/tools/ant/BuildException;

    move-result-object v0

    throw v0

    :cond_0
    iget-object v0, p0, Lorg/apache/tools/ant/types/AntFilterReader;->classpath:Lorg/apache/tools/ant/types/Path;

    if-nez v0, :cond_1

    iput-object p1, p0, Lorg/apache/tools/ant/types/AntFilterReader;->classpath:Lorg/apache/tools/ant/types/Path;

    :goto_0
    return-void

    :cond_1
    iget-object v0, p0, Lorg/apache/tools/ant/types/AntFilterReader;->classpath:Lorg/apache/tools/ant/types/Path;

    invoke-virtual {v0, p1}, Lorg/apache/tools/ant/types/Path;->append(Lorg/apache/tools/ant/types/Path;)V

    goto :goto_0
.end method

.method public setClasspathRef(Lorg/apache/tools/ant/types/Reference;)V
    .locals 1
    .param p1    # Lorg/apache/tools/ant/types/Reference;

    invoke-virtual {p0}, Lorg/apache/tools/ant/types/AntFilterReader;->isReference()Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-virtual {p0}, Lorg/apache/tools/ant/types/AntFilterReader;->tooManyAttributes()Lorg/apache/tools/ant/BuildException;

    move-result-object v0

    throw v0

    :cond_0
    invoke-virtual {p0}, Lorg/apache/tools/ant/types/AntFilterReader;->createClasspath()Lorg/apache/tools/ant/types/Path;

    move-result-object v0

    invoke-virtual {v0, p1}, Lorg/apache/tools/ant/types/Path;->setRefid(Lorg/apache/tools/ant/types/Reference;)V

    return-void
.end method

.method public setRefid(Lorg/apache/tools/ant/types/Reference;)V
    .locals 7
    .param p1    # Lorg/apache/tools/ant/types/Reference;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lorg/apache/tools/ant/BuildException;
        }
    .end annotation

    iget-object v5, p0, Lorg/apache/tools/ant/types/AntFilterReader;->parameters:Ljava/util/Vector;

    invoke-virtual {v5}, Ljava/util/Vector;->isEmpty()Z

    move-result v5

    if-eqz v5, :cond_0

    iget-object v5, p0, Lorg/apache/tools/ant/types/AntFilterReader;->className:Ljava/lang/String;

    if-nez v5, :cond_0

    iget-object v5, p0, Lorg/apache/tools/ant/types/AntFilterReader;->classpath:Lorg/apache/tools/ant/types/Path;

    if-eqz v5, :cond_1

    :cond_0
    invoke-virtual {p0}, Lorg/apache/tools/ant/types/AntFilterReader;->tooManyAttributes()Lorg/apache/tools/ant/BuildException;

    move-result-object v5

    throw v5

    :cond_1
    invoke-virtual {p0}, Lorg/apache/tools/ant/types/AntFilterReader;->getProject()Lorg/apache/tools/ant/Project;

    move-result-object v5

    invoke-virtual {p1, v5}, Lorg/apache/tools/ant/types/Reference;->getReferencedObject(Lorg/apache/tools/ant/Project;)Ljava/lang/Object;

    move-result-object v3

    instance-of v5, v3, Lorg/apache/tools/ant/types/AntFilterReader;

    if-eqz v5, :cond_2

    move-object v0, v3

    check-cast v0, Lorg/apache/tools/ant/types/AntFilterReader;

    invoke-virtual {v0}, Lorg/apache/tools/ant/types/AntFilterReader;->getClassName()Ljava/lang/String;

    move-result-object v5

    invoke-virtual {p0, v5}, Lorg/apache/tools/ant/types/AntFilterReader;->setClassName(Ljava/lang/String;)V

    invoke-virtual {v0}, Lorg/apache/tools/ant/types/AntFilterReader;->getClasspath()Lorg/apache/tools/ant/types/Path;

    move-result-object v5

    invoke-virtual {p0, v5}, Lorg/apache/tools/ant/types/AntFilterReader;->setClasspath(Lorg/apache/tools/ant/types/Path;)V

    invoke-virtual {v0}, Lorg/apache/tools/ant/types/AntFilterReader;->getParams()[Lorg/apache/tools/ant/types/Parameter;

    move-result-object v4

    if-eqz v4, :cond_3

    const/4 v1, 0x0

    :goto_0
    array-length v5, v4

    if-ge v1, v5, :cond_3

    aget-object v5, v4, v1

    invoke-virtual {p0, v5}, Lorg/apache/tools/ant/types/AntFilterReader;->addParam(Lorg/apache/tools/ant/types/Parameter;)V

    add-int/lit8 v1, v1, 0x1

    goto :goto_0

    :cond_2
    new-instance v5, Ljava/lang/StringBuffer;

    invoke-direct {v5}, Ljava/lang/StringBuffer;-><init>()V

    invoke-virtual {p1}, Lorg/apache/tools/ant/types/Reference;->getRefId()Ljava/lang/String;

    move-result-object v6

    invoke-virtual {v5, v6}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    move-result-object v5

    const-string v6, " doesn\'t refer to a FilterReader"

    invoke-virtual {v5, v6}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/StringBuffer;->toString()Ljava/lang/String;

    move-result-object v2

    new-instance v5, Lorg/apache/tools/ant/BuildException;

    invoke-direct {v5, v2}, Lorg/apache/tools/ant/BuildException;-><init>(Ljava/lang/String;)V

    throw v5

    :cond_3
    invoke-super {p0, p1}, Lorg/apache/tools/ant/types/DataType;->setRefid(Lorg/apache/tools/ant/types/Reference;)V

    return-void
.end method
