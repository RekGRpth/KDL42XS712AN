.class public Lorg/apache/tools/ant/types/spi/Service;
.super Lorg/apache/tools/ant/ProjectComponent;
.source "Service.java"


# instance fields
.field private providerList:Ljava/util/List;

.field private type:Ljava/lang/String;


# direct methods
.method public constructor <init>()V
    .locals 1

    invoke-direct {p0}, Lorg/apache/tools/ant/ProjectComponent;-><init>()V

    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, Lorg/apache/tools/ant/types/spi/Service;->providerList:Ljava/util/List;

    return-void
.end method


# virtual methods
.method public addConfiguredProvider(Lorg/apache/tools/ant/types/spi/Provider;)V
    .locals 1
    .param p1    # Lorg/apache/tools/ant/types/spi/Provider;

    invoke-virtual {p1}, Lorg/apache/tools/ant/types/spi/Provider;->check()V

    iget-object v0, p0, Lorg/apache/tools/ant/types/spi/Service;->providerList:Ljava/util/List;

    invoke-interface {v0, p1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    return-void
.end method

.method public check()V
    .locals 3

    iget-object v0, p0, Lorg/apache/tools/ant/types/spi/Service;->type:Ljava/lang/String;

    if-nez v0, :cond_0

    new-instance v0, Lorg/apache/tools/ant/BuildException;

    const-string v1, "type attribute must be set for service element"

    invoke-virtual {p0}, Lorg/apache/tools/ant/types/spi/Service;->getLocation()Lorg/apache/tools/ant/Location;

    move-result-object v2

    invoke-direct {v0, v1, v2}, Lorg/apache/tools/ant/BuildException;-><init>(Ljava/lang/String;Lorg/apache/tools/ant/Location;)V

    throw v0

    :cond_0
    iget-object v0, p0, Lorg/apache/tools/ant/types/spi/Service;->type:Ljava/lang/String;

    invoke-virtual {v0}, Ljava/lang/String;->length()I

    move-result v0

    if-nez v0, :cond_1

    new-instance v0, Lorg/apache/tools/ant/BuildException;

    const-string v1, "Invalid empty type classname"

    invoke-virtual {p0}, Lorg/apache/tools/ant/types/spi/Service;->getLocation()Lorg/apache/tools/ant/Location;

    move-result-object v2

    invoke-direct {v0, v1, v2}, Lorg/apache/tools/ant/BuildException;-><init>(Ljava/lang/String;Lorg/apache/tools/ant/Location;)V

    throw v0

    :cond_1
    iget-object v0, p0, Lorg/apache/tools/ant/types/spi/Service;->providerList:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->size()I

    move-result v0

    if-nez v0, :cond_2

    new-instance v0, Lorg/apache/tools/ant/BuildException;

    const-string v1, "provider attribute or nested provider element must be set!"

    invoke-virtual {p0}, Lorg/apache/tools/ant/types/spi/Service;->getLocation()Lorg/apache/tools/ant/Location;

    move-result-object v2

    invoke-direct {v0, v1, v2}, Lorg/apache/tools/ant/BuildException;-><init>(Ljava/lang/String;Lorg/apache/tools/ant/Location;)V

    throw v0

    :cond_2
    return-void
.end method

.method public getAsStream()Ljava/io/InputStream;
    .locals 6
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    new-instance v0, Ljava/io/ByteArrayOutputStream;

    invoke-direct {v0}, Ljava/io/ByteArrayOutputStream;-><init>()V

    new-instance v3, Ljava/io/OutputStreamWriter;

    const-string v4, "UTF-8"

    invoke-direct {v3, v0, v4}, Ljava/io/OutputStreamWriter;-><init>(Ljava/io/OutputStream;Ljava/lang/String;)V

    iget-object v4, p0, Lorg/apache/tools/ant/types/spi/Service;->providerList:Ljava/util/List;

    invoke-interface {v4}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v2

    :goto_0
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    move-result v4

    if-eqz v4, :cond_0

    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lorg/apache/tools/ant/types/spi/Provider;

    invoke-virtual {v1}, Lorg/apache/tools/ant/types/spi/Provider;->getClassName()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v3, v4}, Ljava/io/Writer;->write(Ljava/lang/String;)V

    const-string v4, "\n"

    invoke-virtual {v3, v4}, Ljava/io/Writer;->write(Ljava/lang/String;)V

    goto :goto_0

    :cond_0
    invoke-virtual {v3}, Ljava/io/Writer;->close()V

    new-instance v4, Ljava/io/ByteArrayInputStream;

    invoke-virtual {v0}, Ljava/io/ByteArrayOutputStream;->toByteArray()[B

    move-result-object v5

    invoke-direct {v4, v5}, Ljava/io/ByteArrayInputStream;-><init>([B)V

    return-object v4
.end method

.method public getType()Ljava/lang/String;
    .locals 1

    iget-object v0, p0, Lorg/apache/tools/ant/types/spi/Service;->type:Ljava/lang/String;

    return-object v0
.end method

.method public setProvider(Ljava/lang/String;)V
    .locals 2
    .param p1    # Ljava/lang/String;

    new-instance v0, Lorg/apache/tools/ant/types/spi/Provider;

    invoke-direct {v0}, Lorg/apache/tools/ant/types/spi/Provider;-><init>()V

    invoke-virtual {v0, p1}, Lorg/apache/tools/ant/types/spi/Provider;->setClassName(Ljava/lang/String;)V

    iget-object v1, p0, Lorg/apache/tools/ant/types/spi/Service;->providerList:Ljava/util/List;

    invoke-interface {v1, v0}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    return-void
.end method

.method public setType(Ljava/lang/String;)V
    .locals 0
    .param p1    # Ljava/lang/String;

    iput-object p1, p0, Lorg/apache/tools/ant/types/spi/Service;->type:Ljava/lang/String;

    return-void
.end method
