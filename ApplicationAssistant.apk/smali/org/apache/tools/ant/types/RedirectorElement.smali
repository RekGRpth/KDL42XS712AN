.class public Lorg/apache/tools/ant/types/RedirectorElement;
.super Lorg/apache/tools/ant/types/DataType;
.source "RedirectorElement.java"


# static fields
.field static class$org$apache$tools$ant$util$MergingMapper:Ljava/lang/Class;


# instance fields
.field private alwaysLog:Ljava/lang/Boolean;

.field private append:Ljava/lang/Boolean;

.field private createEmptyFiles:Ljava/lang/Boolean;

.field private errorEncoding:Ljava/lang/String;

.field private errorFilterChains:Ljava/util/Vector;

.field private errorMapper:Lorg/apache/tools/ant/types/Mapper;

.field private errorProperty:Ljava/lang/String;

.field private inputEncoding:Ljava/lang/String;

.field private inputFilterChains:Ljava/util/Vector;

.field private inputMapper:Lorg/apache/tools/ant/types/Mapper;

.field private inputString:Ljava/lang/String;

.field private logError:Ljava/lang/Boolean;

.field private logInputString:Ljava/lang/Boolean;

.field private outputEncoding:Ljava/lang/String;

.field private outputFilterChains:Ljava/util/Vector;

.field private outputMapper:Lorg/apache/tools/ant/types/Mapper;

.field private outputProperty:Ljava/lang/String;

.field private usingError:Z

.field private usingInput:Z

.field private usingOutput:Z


# direct methods
.method public constructor <init>()V
    .locals 1

    const/4 v0, 0x0

    invoke-direct {p0}, Lorg/apache/tools/ant/types/DataType;-><init>()V

    iput-boolean v0, p0, Lorg/apache/tools/ant/types/RedirectorElement;->usingInput:Z

    iput-boolean v0, p0, Lorg/apache/tools/ant/types/RedirectorElement;->usingOutput:Z

    iput-boolean v0, p0, Lorg/apache/tools/ant/types/RedirectorElement;->usingError:Z

    new-instance v0, Ljava/util/Vector;

    invoke-direct {v0}, Ljava/util/Vector;-><init>()V

    iput-object v0, p0, Lorg/apache/tools/ant/types/RedirectorElement;->inputFilterChains:Ljava/util/Vector;

    new-instance v0, Ljava/util/Vector;

    invoke-direct {v0}, Ljava/util/Vector;-><init>()V

    iput-object v0, p0, Lorg/apache/tools/ant/types/RedirectorElement;->outputFilterChains:Ljava/util/Vector;

    new-instance v0, Ljava/util/Vector;

    invoke-direct {v0}, Ljava/util/Vector;-><init>()V

    iput-object v0, p0, Lorg/apache/tools/ant/types/RedirectorElement;->errorFilterChains:Ljava/util/Vector;

    return-void
.end method

.method static class$(Ljava/lang/String;)Ljava/lang/Class;
    .locals 3
    .param p0    # Ljava/lang/String;

    :try_start_0
    invoke-static {p0}, Ljava/lang/Class;->forName(Ljava/lang/String;)Ljava/lang/Class;
    :try_end_0
    .catch Ljava/lang/ClassNotFoundException; {:try_start_0 .. :try_end_0} :catch_0

    move-result-object v1

    return-object v1

    :catch_0
    move-exception v0

    new-instance v1, Ljava/lang/NoClassDefFoundError;

    invoke-virtual {v0}, Ljava/lang/ClassNotFoundException;->getMessage()Ljava/lang/String;

    move-result-object v2

    invoke-direct {v1, v2}, Ljava/lang/NoClassDefFoundError;-><init>(Ljava/lang/String;)V

    throw v1
.end method

.method private getRef()Lorg/apache/tools/ant/types/RedirectorElement;
    .locals 1

    invoke-virtual {p0}, Lorg/apache/tools/ant/types/RedirectorElement;->getCheckedRef()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lorg/apache/tools/ant/types/RedirectorElement;

    return-object v0
.end method


# virtual methods
.method public addConfiguredErrorMapper(Lorg/apache/tools/ant/types/Mapper;)V
    .locals 2
    .param p1    # Lorg/apache/tools/ant/types/Mapper;

    invoke-virtual {p0}, Lorg/apache/tools/ant/types/RedirectorElement;->isReference()Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-virtual {p0}, Lorg/apache/tools/ant/types/RedirectorElement;->noChildrenAllowed()Lorg/apache/tools/ant/BuildException;

    move-result-object v0

    throw v0

    :cond_0
    iget-object v0, p0, Lorg/apache/tools/ant/types/RedirectorElement;->errorMapper:Lorg/apache/tools/ant/types/Mapper;

    if-eqz v0, :cond_2

    iget-boolean v0, p0, Lorg/apache/tools/ant/types/RedirectorElement;->usingError:Z

    if-eqz v0, :cond_1

    new-instance v0, Lorg/apache/tools/ant/BuildException;

    const-string v1, "attribute \"error\" cannot coexist with a nested <errormapper>"

    invoke-direct {v0, v1}, Lorg/apache/tools/ant/BuildException;-><init>(Ljava/lang/String;)V

    throw v0

    :cond_1
    new-instance v0, Lorg/apache/tools/ant/BuildException;

    const-string v1, "Cannot have > 1 <errormapper>"

    invoke-direct {v0, v1}, Lorg/apache/tools/ant/BuildException;-><init>(Ljava/lang/String;)V

    throw v0

    :cond_2
    iput-object p1, p0, Lorg/apache/tools/ant/types/RedirectorElement;->errorMapper:Lorg/apache/tools/ant/types/Mapper;

    return-void
.end method

.method public addConfiguredInputMapper(Lorg/apache/tools/ant/types/Mapper;)V
    .locals 2
    .param p1    # Lorg/apache/tools/ant/types/Mapper;

    invoke-virtual {p0}, Lorg/apache/tools/ant/types/RedirectorElement;->isReference()Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-virtual {p0}, Lorg/apache/tools/ant/types/RedirectorElement;->noChildrenAllowed()Lorg/apache/tools/ant/BuildException;

    move-result-object v0

    throw v0

    :cond_0
    iget-object v0, p0, Lorg/apache/tools/ant/types/RedirectorElement;->inputMapper:Lorg/apache/tools/ant/types/Mapper;

    if-eqz v0, :cond_2

    iget-boolean v0, p0, Lorg/apache/tools/ant/types/RedirectorElement;->usingInput:Z

    if-eqz v0, :cond_1

    new-instance v0, Lorg/apache/tools/ant/BuildException;

    const-string v1, "attribute \"input\" cannot coexist with a nested <inputmapper>"

    invoke-direct {v0, v1}, Lorg/apache/tools/ant/BuildException;-><init>(Ljava/lang/String;)V

    throw v0

    :cond_1
    new-instance v0, Lorg/apache/tools/ant/BuildException;

    const-string v1, "Cannot have > 1 <inputmapper>"

    invoke-direct {v0, v1}, Lorg/apache/tools/ant/BuildException;-><init>(Ljava/lang/String;)V

    throw v0

    :cond_2
    iput-object p1, p0, Lorg/apache/tools/ant/types/RedirectorElement;->inputMapper:Lorg/apache/tools/ant/types/Mapper;

    return-void
.end method

.method public addConfiguredOutputMapper(Lorg/apache/tools/ant/types/Mapper;)V
    .locals 2
    .param p1    # Lorg/apache/tools/ant/types/Mapper;

    invoke-virtual {p0}, Lorg/apache/tools/ant/types/RedirectorElement;->isReference()Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-virtual {p0}, Lorg/apache/tools/ant/types/RedirectorElement;->noChildrenAllowed()Lorg/apache/tools/ant/BuildException;

    move-result-object v0

    throw v0

    :cond_0
    iget-object v0, p0, Lorg/apache/tools/ant/types/RedirectorElement;->outputMapper:Lorg/apache/tools/ant/types/Mapper;

    if-eqz v0, :cond_2

    iget-boolean v0, p0, Lorg/apache/tools/ant/types/RedirectorElement;->usingOutput:Z

    if-eqz v0, :cond_1

    new-instance v0, Lorg/apache/tools/ant/BuildException;

    const-string v1, "attribute \"output\" cannot coexist with a nested <outputmapper>"

    invoke-direct {v0, v1}, Lorg/apache/tools/ant/BuildException;-><init>(Ljava/lang/String;)V

    throw v0

    :cond_1
    new-instance v0, Lorg/apache/tools/ant/BuildException;

    const-string v1, "Cannot have > 1 <outputmapper>"

    invoke-direct {v0, v1}, Lorg/apache/tools/ant/BuildException;-><init>(Ljava/lang/String;)V

    throw v0

    :cond_2
    iput-object p1, p0, Lorg/apache/tools/ant/types/RedirectorElement;->outputMapper:Lorg/apache/tools/ant/types/Mapper;

    return-void
.end method

.method public configure(Lorg/apache/tools/ant/taskdefs/Redirector;)V
    .locals 1
    .param p1    # Lorg/apache/tools/ant/taskdefs/Redirector;

    const/4 v0, 0x0

    invoke-virtual {p0, p1, v0}, Lorg/apache/tools/ant/types/RedirectorElement;->configure(Lorg/apache/tools/ant/taskdefs/Redirector;Ljava/lang/String;)V

    return-void
.end method

.method public configure(Lorg/apache/tools/ant/taskdefs/Redirector;Ljava/lang/String;)V
    .locals 5
    .param p1    # Lorg/apache/tools/ant/taskdefs/Redirector;
    .param p2    # Ljava/lang/String;

    invoke-virtual {p0}, Lorg/apache/tools/ant/types/RedirectorElement;->isReference()Z

    move-result v4

    if-eqz v4, :cond_1

    invoke-direct {p0}, Lorg/apache/tools/ant/types/RedirectorElement;->getRef()Lorg/apache/tools/ant/types/RedirectorElement;

    move-result-object v4

    invoke-virtual {v4, p1, p2}, Lorg/apache/tools/ant/types/RedirectorElement;->configure(Lorg/apache/tools/ant/taskdefs/Redirector;Ljava/lang/String;)V

    :cond_0
    :goto_0
    return-void

    :cond_1
    iget-object v4, p0, Lorg/apache/tools/ant/types/RedirectorElement;->alwaysLog:Ljava/lang/Boolean;

    if-eqz v4, :cond_2

    iget-object v4, p0, Lorg/apache/tools/ant/types/RedirectorElement;->alwaysLog:Ljava/lang/Boolean;

    invoke-virtual {v4}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v4

    invoke-virtual {p1, v4}, Lorg/apache/tools/ant/taskdefs/Redirector;->setAlwaysLog(Z)V

    :cond_2
    iget-object v4, p0, Lorg/apache/tools/ant/types/RedirectorElement;->logError:Ljava/lang/Boolean;

    if-eqz v4, :cond_3

    iget-object v4, p0, Lorg/apache/tools/ant/types/RedirectorElement;->logError:Ljava/lang/Boolean;

    invoke-virtual {v4}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v4

    invoke-virtual {p1, v4}, Lorg/apache/tools/ant/taskdefs/Redirector;->setLogError(Z)V

    :cond_3
    iget-object v4, p0, Lorg/apache/tools/ant/types/RedirectorElement;->append:Ljava/lang/Boolean;

    if-eqz v4, :cond_4

    iget-object v4, p0, Lorg/apache/tools/ant/types/RedirectorElement;->append:Ljava/lang/Boolean;

    invoke-virtual {v4}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v4

    invoke-virtual {p1, v4}, Lorg/apache/tools/ant/taskdefs/Redirector;->setAppend(Z)V

    :cond_4
    iget-object v4, p0, Lorg/apache/tools/ant/types/RedirectorElement;->createEmptyFiles:Ljava/lang/Boolean;

    if-eqz v4, :cond_5

    iget-object v4, p0, Lorg/apache/tools/ant/types/RedirectorElement;->createEmptyFiles:Ljava/lang/Boolean;

    invoke-virtual {v4}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v4

    invoke-virtual {p1, v4}, Lorg/apache/tools/ant/taskdefs/Redirector;->setCreateEmptyFiles(Z)V

    :cond_5
    iget-object v4, p0, Lorg/apache/tools/ant/types/RedirectorElement;->outputProperty:Ljava/lang/String;

    if-eqz v4, :cond_6

    iget-object v4, p0, Lorg/apache/tools/ant/types/RedirectorElement;->outputProperty:Ljava/lang/String;

    invoke-virtual {p1, v4}, Lorg/apache/tools/ant/taskdefs/Redirector;->setOutputProperty(Ljava/lang/String;)V

    :cond_6
    iget-object v4, p0, Lorg/apache/tools/ant/types/RedirectorElement;->errorProperty:Ljava/lang/String;

    if-eqz v4, :cond_7

    iget-object v4, p0, Lorg/apache/tools/ant/types/RedirectorElement;->errorProperty:Ljava/lang/String;

    invoke-virtual {p1, v4}, Lorg/apache/tools/ant/taskdefs/Redirector;->setErrorProperty(Ljava/lang/String;)V

    :cond_7
    iget-object v4, p0, Lorg/apache/tools/ant/types/RedirectorElement;->inputString:Ljava/lang/String;

    if-eqz v4, :cond_8

    iget-object v4, p0, Lorg/apache/tools/ant/types/RedirectorElement;->inputString:Ljava/lang/String;

    invoke-virtual {p1, v4}, Lorg/apache/tools/ant/taskdefs/Redirector;->setInputString(Ljava/lang/String;)V

    :cond_8
    iget-object v4, p0, Lorg/apache/tools/ant/types/RedirectorElement;->logInputString:Ljava/lang/Boolean;

    if-eqz v4, :cond_9

    iget-object v4, p0, Lorg/apache/tools/ant/types/RedirectorElement;->logInputString:Ljava/lang/Boolean;

    invoke-virtual {v4}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v4

    invoke-virtual {p1, v4}, Lorg/apache/tools/ant/taskdefs/Redirector;->setLogInputString(Z)V

    :cond_9
    iget-object v4, p0, Lorg/apache/tools/ant/types/RedirectorElement;->inputMapper:Lorg/apache/tools/ant/types/Mapper;

    if-eqz v4, :cond_b

    const/4 v2, 0x0

    :try_start_0
    iget-object v4, p0, Lorg/apache/tools/ant/types/RedirectorElement;->inputMapper:Lorg/apache/tools/ant/types/Mapper;

    invoke-virtual {v4}, Lorg/apache/tools/ant/types/Mapper;->getImplementation()Lorg/apache/tools/ant/util/FileNameMapper;

    move-result-object v4

    invoke-interface {v4, p2}, Lorg/apache/tools/ant/util/FileNameMapper;->mapFileName(Ljava/lang/String;)[Ljava/lang/String;
    :try_end_0
    .catch Ljava/lang/NullPointerException; {:try_start_0 .. :try_end_0} :catch_0

    move-result-object v2

    :cond_a
    if-eqz v2, :cond_b

    array-length v4, v2

    if-lez v4, :cond_b

    invoke-virtual {p0, v2}, Lorg/apache/tools/ant/types/RedirectorElement;->toFileArray([Ljava/lang/String;)[Ljava/io/File;

    move-result-object v4

    invoke-virtual {p1, v4}, Lorg/apache/tools/ant/taskdefs/Redirector;->setInput([Ljava/io/File;)V

    :cond_b
    iget-object v4, p0, Lorg/apache/tools/ant/types/RedirectorElement;->outputMapper:Lorg/apache/tools/ant/types/Mapper;

    if-eqz v4, :cond_d

    const/4 v3, 0x0

    :try_start_1
    iget-object v4, p0, Lorg/apache/tools/ant/types/RedirectorElement;->outputMapper:Lorg/apache/tools/ant/types/Mapper;

    invoke-virtual {v4}, Lorg/apache/tools/ant/types/Mapper;->getImplementation()Lorg/apache/tools/ant/util/FileNameMapper;

    move-result-object v4

    invoke-interface {v4, p2}, Lorg/apache/tools/ant/util/FileNameMapper;->mapFileName(Ljava/lang/String;)[Ljava/lang/String;
    :try_end_1
    .catch Ljava/lang/NullPointerException; {:try_start_1 .. :try_end_1} :catch_1

    move-result-object v3

    :cond_c
    if-eqz v3, :cond_d

    array-length v4, v3

    if-lez v4, :cond_d

    invoke-virtual {p0, v3}, Lorg/apache/tools/ant/types/RedirectorElement;->toFileArray([Ljava/lang/String;)[Ljava/io/File;

    move-result-object v4

    invoke-virtual {p1, v4}, Lorg/apache/tools/ant/taskdefs/Redirector;->setOutput([Ljava/io/File;)V

    :cond_d
    iget-object v4, p0, Lorg/apache/tools/ant/types/RedirectorElement;->errorMapper:Lorg/apache/tools/ant/types/Mapper;

    if-eqz v4, :cond_f

    const/4 v1, 0x0

    :try_start_2
    iget-object v4, p0, Lorg/apache/tools/ant/types/RedirectorElement;->errorMapper:Lorg/apache/tools/ant/types/Mapper;

    invoke-virtual {v4}, Lorg/apache/tools/ant/types/Mapper;->getImplementation()Lorg/apache/tools/ant/util/FileNameMapper;

    move-result-object v4

    invoke-interface {v4, p2}, Lorg/apache/tools/ant/util/FileNameMapper;->mapFileName(Ljava/lang/String;)[Ljava/lang/String;
    :try_end_2
    .catch Ljava/lang/NullPointerException; {:try_start_2 .. :try_end_2} :catch_2

    move-result-object v1

    :cond_e
    if-eqz v1, :cond_f

    array-length v4, v1

    if-lez v4, :cond_f

    invoke-virtual {p0, v1}, Lorg/apache/tools/ant/types/RedirectorElement;->toFileArray([Ljava/lang/String;)[Ljava/io/File;

    move-result-object v4

    invoke-virtual {p1, v4}, Lorg/apache/tools/ant/taskdefs/Redirector;->setError([Ljava/io/File;)V

    :cond_f
    iget-object v4, p0, Lorg/apache/tools/ant/types/RedirectorElement;->inputFilterChains:Ljava/util/Vector;

    invoke-virtual {v4}, Ljava/util/Vector;->size()I

    move-result v4

    if-lez v4, :cond_10

    iget-object v4, p0, Lorg/apache/tools/ant/types/RedirectorElement;->inputFilterChains:Ljava/util/Vector;

    invoke-virtual {p1, v4}, Lorg/apache/tools/ant/taskdefs/Redirector;->setInputFilterChains(Ljava/util/Vector;)V

    :cond_10
    iget-object v4, p0, Lorg/apache/tools/ant/types/RedirectorElement;->outputFilterChains:Ljava/util/Vector;

    invoke-virtual {v4}, Ljava/util/Vector;->size()I

    move-result v4

    if-lez v4, :cond_11

    iget-object v4, p0, Lorg/apache/tools/ant/types/RedirectorElement;->outputFilterChains:Ljava/util/Vector;

    invoke-virtual {p1, v4}, Lorg/apache/tools/ant/taskdefs/Redirector;->setOutputFilterChains(Ljava/util/Vector;)V

    :cond_11
    iget-object v4, p0, Lorg/apache/tools/ant/types/RedirectorElement;->errorFilterChains:Ljava/util/Vector;

    invoke-virtual {v4}, Ljava/util/Vector;->size()I

    move-result v4

    if-lez v4, :cond_12

    iget-object v4, p0, Lorg/apache/tools/ant/types/RedirectorElement;->errorFilterChains:Ljava/util/Vector;

    invoke-virtual {p1, v4}, Lorg/apache/tools/ant/taskdefs/Redirector;->setErrorFilterChains(Ljava/util/Vector;)V

    :cond_12
    iget-object v4, p0, Lorg/apache/tools/ant/types/RedirectorElement;->inputEncoding:Ljava/lang/String;

    if-eqz v4, :cond_13

    iget-object v4, p0, Lorg/apache/tools/ant/types/RedirectorElement;->inputEncoding:Ljava/lang/String;

    invoke-virtual {p1, v4}, Lorg/apache/tools/ant/taskdefs/Redirector;->setInputEncoding(Ljava/lang/String;)V

    :cond_13
    iget-object v4, p0, Lorg/apache/tools/ant/types/RedirectorElement;->outputEncoding:Ljava/lang/String;

    if-eqz v4, :cond_14

    iget-object v4, p0, Lorg/apache/tools/ant/types/RedirectorElement;->outputEncoding:Ljava/lang/String;

    invoke-virtual {p1, v4}, Lorg/apache/tools/ant/taskdefs/Redirector;->setOutputEncoding(Ljava/lang/String;)V

    :cond_14
    iget-object v4, p0, Lorg/apache/tools/ant/types/RedirectorElement;->errorEncoding:Ljava/lang/String;

    if-eqz v4, :cond_0

    iget-object v4, p0, Lorg/apache/tools/ant/types/RedirectorElement;->errorEncoding:Ljava/lang/String;

    invoke-virtual {p1, v4}, Lorg/apache/tools/ant/taskdefs/Redirector;->setErrorEncoding(Ljava/lang/String;)V

    goto/16 :goto_0

    :catch_0
    move-exception v0

    if-eqz p2, :cond_a

    throw v0

    :catch_1
    move-exception v0

    if-eqz p2, :cond_c

    throw v0

    :catch_2
    move-exception v0

    if-eqz p2, :cond_e

    throw v0
.end method

.method public createErrorFilterChain()Lorg/apache/tools/ant/types/FilterChain;
    .locals 2

    invoke-virtual {p0}, Lorg/apache/tools/ant/types/RedirectorElement;->isReference()Z

    move-result v1

    if-eqz v1, :cond_0

    invoke-virtual {p0}, Lorg/apache/tools/ant/types/RedirectorElement;->noChildrenAllowed()Lorg/apache/tools/ant/BuildException;

    move-result-object v1

    throw v1

    :cond_0
    new-instance v0, Lorg/apache/tools/ant/types/FilterChain;

    invoke-direct {v0}, Lorg/apache/tools/ant/types/FilterChain;-><init>()V

    invoke-virtual {p0}, Lorg/apache/tools/ant/types/RedirectorElement;->getProject()Lorg/apache/tools/ant/Project;

    move-result-object v1

    invoke-virtual {v0, v1}, Lorg/apache/tools/ant/types/FilterChain;->setProject(Lorg/apache/tools/ant/Project;)V

    iget-object v1, p0, Lorg/apache/tools/ant/types/RedirectorElement;->errorFilterChains:Ljava/util/Vector;

    invoke-virtual {v1, v0}, Ljava/util/Vector;->add(Ljava/lang/Object;)Z

    return-object v0
.end method

.method public createInputFilterChain()Lorg/apache/tools/ant/types/FilterChain;
    .locals 2

    invoke-virtual {p0}, Lorg/apache/tools/ant/types/RedirectorElement;->isReference()Z

    move-result v1

    if-eqz v1, :cond_0

    invoke-virtual {p0}, Lorg/apache/tools/ant/types/RedirectorElement;->noChildrenAllowed()Lorg/apache/tools/ant/BuildException;

    move-result-object v1

    throw v1

    :cond_0
    new-instance v0, Lorg/apache/tools/ant/types/FilterChain;

    invoke-direct {v0}, Lorg/apache/tools/ant/types/FilterChain;-><init>()V

    invoke-virtual {p0}, Lorg/apache/tools/ant/types/RedirectorElement;->getProject()Lorg/apache/tools/ant/Project;

    move-result-object v1

    invoke-virtual {v0, v1}, Lorg/apache/tools/ant/types/FilterChain;->setProject(Lorg/apache/tools/ant/Project;)V

    iget-object v1, p0, Lorg/apache/tools/ant/types/RedirectorElement;->inputFilterChains:Ljava/util/Vector;

    invoke-virtual {v1, v0}, Ljava/util/Vector;->add(Ljava/lang/Object;)Z

    return-object v0
.end method

.method protected createMergeMapper(Ljava/io/File;)Lorg/apache/tools/ant/types/Mapper;
    .locals 2
    .param p1    # Ljava/io/File;

    new-instance v0, Lorg/apache/tools/ant/types/Mapper;

    invoke-virtual {p0}, Lorg/apache/tools/ant/types/RedirectorElement;->getProject()Lorg/apache/tools/ant/Project;

    move-result-object v1

    invoke-direct {v0, v1}, Lorg/apache/tools/ant/types/Mapper;-><init>(Lorg/apache/tools/ant/Project;)V

    sget-object v1, Lorg/apache/tools/ant/types/RedirectorElement;->class$org$apache$tools$ant$util$MergingMapper:Ljava/lang/Class;

    if-nez v1, :cond_0

    const-string v1, "org.apache.tools.ant.util.MergingMapper"

    invoke-static {v1}, Lorg/apache/tools/ant/types/RedirectorElement;->class$(Ljava/lang/String;)Ljava/lang/Class;

    move-result-object v1

    sput-object v1, Lorg/apache/tools/ant/types/RedirectorElement;->class$org$apache$tools$ant$util$MergingMapper:Ljava/lang/Class;

    :goto_0
    invoke-virtual {v1}, Ljava/lang/Class;->getName()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Lorg/apache/tools/ant/types/Mapper;->setClassname(Ljava/lang/String;)V

    invoke-virtual {p1}, Ljava/io/File;->getAbsolutePath()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Lorg/apache/tools/ant/types/Mapper;->setTo(Ljava/lang/String;)V

    return-object v0

    :cond_0
    sget-object v1, Lorg/apache/tools/ant/types/RedirectorElement;->class$org$apache$tools$ant$util$MergingMapper:Ljava/lang/Class;

    goto :goto_0
.end method

.method public createOutputFilterChain()Lorg/apache/tools/ant/types/FilterChain;
    .locals 2

    invoke-virtual {p0}, Lorg/apache/tools/ant/types/RedirectorElement;->isReference()Z

    move-result v1

    if-eqz v1, :cond_0

    invoke-virtual {p0}, Lorg/apache/tools/ant/types/RedirectorElement;->noChildrenAllowed()Lorg/apache/tools/ant/BuildException;

    move-result-object v1

    throw v1

    :cond_0
    new-instance v0, Lorg/apache/tools/ant/types/FilterChain;

    invoke-direct {v0}, Lorg/apache/tools/ant/types/FilterChain;-><init>()V

    invoke-virtual {p0}, Lorg/apache/tools/ant/types/RedirectorElement;->getProject()Lorg/apache/tools/ant/Project;

    move-result-object v1

    invoke-virtual {v0, v1}, Lorg/apache/tools/ant/types/FilterChain;->setProject(Lorg/apache/tools/ant/Project;)V

    iget-object v1, p0, Lorg/apache/tools/ant/types/RedirectorElement;->outputFilterChains:Ljava/util/Vector;

    invoke-virtual {v1, v0}, Ljava/util/Vector;->add(Ljava/lang/Object;)Z

    return-object v0
.end method

.method protected dieOnCircularReference(Ljava/util/Stack;Lorg/apache/tools/ant/Project;)V
    .locals 10
    .param p1    # Ljava/util/Stack;
    .param p2    # Lorg/apache/tools/ant/Project;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lorg/apache/tools/ant/BuildException;
        }
    .end annotation

    const/4 v9, 0x3

    const/4 v8, 0x2

    const/4 v7, 0x0

    const/4 v6, 0x1

    invoke-virtual {p0}, Lorg/apache/tools/ant/types/RedirectorElement;->isChecked()Z

    move-result v5

    if-eqz v5, :cond_0

    :goto_0
    return-void

    :cond_0
    invoke-virtual {p0}, Lorg/apache/tools/ant/types/RedirectorElement;->isReference()Z

    move-result v5

    if-eqz v5, :cond_1

    invoke-super {p0, p1, p2}, Lorg/apache/tools/ant/types/DataType;->dieOnCircularReference(Ljava/util/Stack;Lorg/apache/tools/ant/Project;)V

    goto :goto_0

    :cond_1
    new-array v3, v9, [Lorg/apache/tools/ant/types/Mapper;

    iget-object v5, p0, Lorg/apache/tools/ant/types/RedirectorElement;->inputMapper:Lorg/apache/tools/ant/types/Mapper;

    aput-object v5, v3, v7

    iget-object v5, p0, Lorg/apache/tools/ant/types/RedirectorElement;->outputMapper:Lorg/apache/tools/ant/types/Mapper;

    aput-object v5, v3, v6

    iget-object v5, p0, Lorg/apache/tools/ant/types/RedirectorElement;->errorMapper:Lorg/apache/tools/ant/types/Mapper;

    aput-object v5, v3, v8

    const/4 v2, 0x0

    :goto_1
    array-length v5, v3

    if-ge v2, v5, :cond_3

    aget-object v5, v3, v2

    if-eqz v5, :cond_2

    aget-object v5, v3, v2

    invoke-virtual {p1, v5}, Ljava/util/Stack;->push(Ljava/lang/Object;)Ljava/lang/Object;

    aget-object v5, v3, v2

    invoke-virtual {v5, p1, p2}, Lorg/apache/tools/ant/types/Mapper;->dieOnCircularReference(Ljava/util/Stack;Lorg/apache/tools/ant/Project;)V

    invoke-virtual {p1}, Ljava/util/Stack;->pop()Ljava/lang/Object;

    :cond_2
    add-int/lit8 v2, v2, 0x1

    goto :goto_1

    :cond_3
    new-array v4, v9, [Ljava/util/Vector;

    iget-object v5, p0, Lorg/apache/tools/ant/types/RedirectorElement;->inputFilterChains:Ljava/util/Vector;

    aput-object v5, v4, v7

    iget-object v5, p0, Lorg/apache/tools/ant/types/RedirectorElement;->outputFilterChains:Ljava/util/Vector;

    aput-object v5, v4, v6

    iget-object v5, p0, Lorg/apache/tools/ant/types/RedirectorElement;->errorFilterChains:Ljava/util/Vector;

    aput-object v5, v4, v8

    const/4 v2, 0x0

    :goto_2
    array-length v5, v4

    if-ge v2, v5, :cond_5

    aget-object v5, v4, v2

    if-eqz v5, :cond_4

    aget-object v5, v4, v2

    invoke-virtual {v5}, Ljava/util/Vector;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :goto_3
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v5

    if-eqz v5, :cond_4

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lorg/apache/tools/ant/types/FilterChain;

    invoke-virtual {p1, v0}, Ljava/util/Stack;->push(Ljava/lang/Object;)Ljava/lang/Object;

    invoke-virtual {v0, p1, p2}, Lorg/apache/tools/ant/types/FilterChain;->dieOnCircularReference(Ljava/util/Stack;Lorg/apache/tools/ant/Project;)V

    invoke-virtual {p1}, Ljava/util/Stack;->pop()Ljava/lang/Object;

    goto :goto_3

    :cond_4
    add-int/lit8 v2, v2, 0x1

    goto :goto_2

    :cond_5
    invoke-virtual {p0, v6}, Lorg/apache/tools/ant/types/RedirectorElement;->setChecked(Z)V

    goto :goto_0
.end method

.method public setAlwaysLog(Z)V
    .locals 1
    .param p1    # Z

    invoke-virtual {p0}, Lorg/apache/tools/ant/types/RedirectorElement;->isReference()Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-virtual {p0}, Lorg/apache/tools/ant/types/RedirectorElement;->tooManyAttributes()Lorg/apache/tools/ant/BuildException;

    move-result-object v0

    throw v0

    :cond_0
    if-eqz p1, :cond_1

    sget-object v0, Ljava/lang/Boolean;->TRUE:Ljava/lang/Boolean;

    :goto_0
    iput-object v0, p0, Lorg/apache/tools/ant/types/RedirectorElement;->alwaysLog:Ljava/lang/Boolean;

    return-void

    :cond_1
    sget-object v0, Ljava/lang/Boolean;->FALSE:Ljava/lang/Boolean;

    goto :goto_0
.end method

.method public setAppend(Z)V
    .locals 1
    .param p1    # Z

    invoke-virtual {p0}, Lorg/apache/tools/ant/types/RedirectorElement;->isReference()Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-virtual {p0}, Lorg/apache/tools/ant/types/RedirectorElement;->tooManyAttributes()Lorg/apache/tools/ant/BuildException;

    move-result-object v0

    throw v0

    :cond_0
    if-eqz p1, :cond_1

    sget-object v0, Ljava/lang/Boolean;->TRUE:Ljava/lang/Boolean;

    :goto_0
    iput-object v0, p0, Lorg/apache/tools/ant/types/RedirectorElement;->append:Ljava/lang/Boolean;

    return-void

    :cond_1
    sget-object v0, Ljava/lang/Boolean;->FALSE:Ljava/lang/Boolean;

    goto :goto_0
.end method

.method public setCreateEmptyFiles(Z)V
    .locals 1
    .param p1    # Z

    invoke-virtual {p0}, Lorg/apache/tools/ant/types/RedirectorElement;->isReference()Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-virtual {p0}, Lorg/apache/tools/ant/types/RedirectorElement;->tooManyAttributes()Lorg/apache/tools/ant/BuildException;

    move-result-object v0

    throw v0

    :cond_0
    if-eqz p1, :cond_1

    sget-object v0, Ljava/lang/Boolean;->TRUE:Ljava/lang/Boolean;

    :goto_0
    iput-object v0, p0, Lorg/apache/tools/ant/types/RedirectorElement;->createEmptyFiles:Ljava/lang/Boolean;

    return-void

    :cond_1
    sget-object v0, Ljava/lang/Boolean;->FALSE:Ljava/lang/Boolean;

    goto :goto_0
.end method

.method public setError(Ljava/io/File;)V
    .locals 2
    .param p1    # Ljava/io/File;

    invoke-virtual {p0}, Lorg/apache/tools/ant/types/RedirectorElement;->isReference()Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-virtual {p0}, Lorg/apache/tools/ant/types/RedirectorElement;->tooManyAttributes()Lorg/apache/tools/ant/BuildException;

    move-result-object v0

    throw v0

    :cond_0
    if-nez p1, :cond_1

    new-instance v0, Ljava/lang/IllegalArgumentException;

    const-string v1, "error file specified as null"

    invoke-direct {v0, v1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v0

    :cond_1
    const/4 v0, 0x1

    iput-boolean v0, p0, Lorg/apache/tools/ant/types/RedirectorElement;->usingError:Z

    invoke-virtual {p0, p1}, Lorg/apache/tools/ant/types/RedirectorElement;->createMergeMapper(Ljava/io/File;)Lorg/apache/tools/ant/types/Mapper;

    move-result-object v0

    iput-object v0, p0, Lorg/apache/tools/ant/types/RedirectorElement;->errorMapper:Lorg/apache/tools/ant/types/Mapper;

    return-void
.end method

.method public setErrorEncoding(Ljava/lang/String;)V
    .locals 1
    .param p1    # Ljava/lang/String;

    invoke-virtual {p0}, Lorg/apache/tools/ant/types/RedirectorElement;->isReference()Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-virtual {p0}, Lorg/apache/tools/ant/types/RedirectorElement;->tooManyAttributes()Lorg/apache/tools/ant/BuildException;

    move-result-object v0

    throw v0

    :cond_0
    iput-object p1, p0, Lorg/apache/tools/ant/types/RedirectorElement;->errorEncoding:Ljava/lang/String;

    return-void
.end method

.method public setErrorProperty(Ljava/lang/String;)V
    .locals 1
    .param p1    # Ljava/lang/String;

    invoke-virtual {p0}, Lorg/apache/tools/ant/types/RedirectorElement;->isReference()Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-virtual {p0}, Lorg/apache/tools/ant/types/RedirectorElement;->tooManyAttributes()Lorg/apache/tools/ant/BuildException;

    move-result-object v0

    throw v0

    :cond_0
    iput-object p1, p0, Lorg/apache/tools/ant/types/RedirectorElement;->errorProperty:Ljava/lang/String;

    return-void
.end method

.method public setInput(Ljava/io/File;)V
    .locals 2
    .param p1    # Ljava/io/File;

    invoke-virtual {p0}, Lorg/apache/tools/ant/types/RedirectorElement;->isReference()Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-virtual {p0}, Lorg/apache/tools/ant/types/RedirectorElement;->tooManyAttributes()Lorg/apache/tools/ant/BuildException;

    move-result-object v0

    throw v0

    :cond_0
    iget-object v0, p0, Lorg/apache/tools/ant/types/RedirectorElement;->inputString:Ljava/lang/String;

    if-eqz v0, :cond_1

    new-instance v0, Lorg/apache/tools/ant/BuildException;

    const-string v1, "The \"input\" and \"inputstring\" attributes cannot both be specified"

    invoke-direct {v0, v1}, Lorg/apache/tools/ant/BuildException;-><init>(Ljava/lang/String;)V

    throw v0

    :cond_1
    const/4 v0, 0x1

    iput-boolean v0, p0, Lorg/apache/tools/ant/types/RedirectorElement;->usingInput:Z

    invoke-virtual {p0, p1}, Lorg/apache/tools/ant/types/RedirectorElement;->createMergeMapper(Ljava/io/File;)Lorg/apache/tools/ant/types/Mapper;

    move-result-object v0

    iput-object v0, p0, Lorg/apache/tools/ant/types/RedirectorElement;->inputMapper:Lorg/apache/tools/ant/types/Mapper;

    return-void
.end method

.method public setInputEncoding(Ljava/lang/String;)V
    .locals 1
    .param p1    # Ljava/lang/String;

    invoke-virtual {p0}, Lorg/apache/tools/ant/types/RedirectorElement;->isReference()Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-virtual {p0}, Lorg/apache/tools/ant/types/RedirectorElement;->tooManyAttributes()Lorg/apache/tools/ant/BuildException;

    move-result-object v0

    throw v0

    :cond_0
    iput-object p1, p0, Lorg/apache/tools/ant/types/RedirectorElement;->inputEncoding:Ljava/lang/String;

    return-void
.end method

.method public setInputString(Ljava/lang/String;)V
    .locals 2
    .param p1    # Ljava/lang/String;

    invoke-virtual {p0}, Lorg/apache/tools/ant/types/RedirectorElement;->isReference()Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-virtual {p0}, Lorg/apache/tools/ant/types/RedirectorElement;->tooManyAttributes()Lorg/apache/tools/ant/BuildException;

    move-result-object v0

    throw v0

    :cond_0
    iget-boolean v0, p0, Lorg/apache/tools/ant/types/RedirectorElement;->usingInput:Z

    if-eqz v0, :cond_1

    new-instance v0, Lorg/apache/tools/ant/BuildException;

    const-string v1, "The \"input\" and \"inputstring\" attributes cannot both be specified"

    invoke-direct {v0, v1}, Lorg/apache/tools/ant/BuildException;-><init>(Ljava/lang/String;)V

    throw v0

    :cond_1
    iput-object p1, p0, Lorg/apache/tools/ant/types/RedirectorElement;->inputString:Ljava/lang/String;

    return-void
.end method

.method public setLogError(Z)V
    .locals 1
    .param p1    # Z

    invoke-virtual {p0}, Lorg/apache/tools/ant/types/RedirectorElement;->isReference()Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-virtual {p0}, Lorg/apache/tools/ant/types/RedirectorElement;->tooManyAttributes()Lorg/apache/tools/ant/BuildException;

    move-result-object v0

    throw v0

    :cond_0
    if-eqz p1, :cond_1

    sget-object v0, Ljava/lang/Boolean;->TRUE:Ljava/lang/Boolean;

    :goto_0
    iput-object v0, p0, Lorg/apache/tools/ant/types/RedirectorElement;->logError:Ljava/lang/Boolean;

    return-void

    :cond_1
    sget-object v0, Ljava/lang/Boolean;->FALSE:Ljava/lang/Boolean;

    goto :goto_0
.end method

.method public setLogInputString(Z)V
    .locals 1
    .param p1    # Z

    invoke-virtual {p0}, Lorg/apache/tools/ant/types/RedirectorElement;->isReference()Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-virtual {p0}, Lorg/apache/tools/ant/types/RedirectorElement;->tooManyAttributes()Lorg/apache/tools/ant/BuildException;

    move-result-object v0

    throw v0

    :cond_0
    if-eqz p1, :cond_1

    sget-object v0, Ljava/lang/Boolean;->TRUE:Ljava/lang/Boolean;

    :goto_0
    iput-object v0, p0, Lorg/apache/tools/ant/types/RedirectorElement;->logInputString:Ljava/lang/Boolean;

    return-void

    :cond_1
    sget-object v0, Ljava/lang/Boolean;->FALSE:Ljava/lang/Boolean;

    goto :goto_0
.end method

.method public setOutput(Ljava/io/File;)V
    .locals 2
    .param p1    # Ljava/io/File;

    invoke-virtual {p0}, Lorg/apache/tools/ant/types/RedirectorElement;->isReference()Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-virtual {p0}, Lorg/apache/tools/ant/types/RedirectorElement;->tooManyAttributes()Lorg/apache/tools/ant/BuildException;

    move-result-object v0

    throw v0

    :cond_0
    if-nez p1, :cond_1

    new-instance v0, Ljava/lang/IllegalArgumentException;

    const-string v1, "output file specified as null"

    invoke-direct {v0, v1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v0

    :cond_1
    const/4 v0, 0x1

    iput-boolean v0, p0, Lorg/apache/tools/ant/types/RedirectorElement;->usingOutput:Z

    invoke-virtual {p0, p1}, Lorg/apache/tools/ant/types/RedirectorElement;->createMergeMapper(Ljava/io/File;)Lorg/apache/tools/ant/types/Mapper;

    move-result-object v0

    iput-object v0, p0, Lorg/apache/tools/ant/types/RedirectorElement;->outputMapper:Lorg/apache/tools/ant/types/Mapper;

    return-void
.end method

.method public setOutputEncoding(Ljava/lang/String;)V
    .locals 1
    .param p1    # Ljava/lang/String;

    invoke-virtual {p0}, Lorg/apache/tools/ant/types/RedirectorElement;->isReference()Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-virtual {p0}, Lorg/apache/tools/ant/types/RedirectorElement;->tooManyAttributes()Lorg/apache/tools/ant/BuildException;

    move-result-object v0

    throw v0

    :cond_0
    iput-object p1, p0, Lorg/apache/tools/ant/types/RedirectorElement;->outputEncoding:Ljava/lang/String;

    return-void
.end method

.method public setOutputProperty(Ljava/lang/String;)V
    .locals 1
    .param p1    # Ljava/lang/String;

    invoke-virtual {p0}, Lorg/apache/tools/ant/types/RedirectorElement;->isReference()Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-virtual {p0}, Lorg/apache/tools/ant/types/RedirectorElement;->tooManyAttributes()Lorg/apache/tools/ant/BuildException;

    move-result-object v0

    throw v0

    :cond_0
    iput-object p1, p0, Lorg/apache/tools/ant/types/RedirectorElement;->outputProperty:Ljava/lang/String;

    return-void
.end method

.method public setRefid(Lorg/apache/tools/ant/types/Reference;)V
    .locals 1
    .param p1    # Lorg/apache/tools/ant/types/Reference;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lorg/apache/tools/ant/BuildException;
        }
    .end annotation

    iget-boolean v0, p0, Lorg/apache/tools/ant/types/RedirectorElement;->usingInput:Z

    if-nez v0, :cond_0

    iget-boolean v0, p0, Lorg/apache/tools/ant/types/RedirectorElement;->usingOutput:Z

    if-nez v0, :cond_0

    iget-boolean v0, p0, Lorg/apache/tools/ant/types/RedirectorElement;->usingError:Z

    if-nez v0, :cond_0

    iget-object v0, p0, Lorg/apache/tools/ant/types/RedirectorElement;->inputString:Ljava/lang/String;

    if-nez v0, :cond_0

    iget-object v0, p0, Lorg/apache/tools/ant/types/RedirectorElement;->logError:Ljava/lang/Boolean;

    if-nez v0, :cond_0

    iget-object v0, p0, Lorg/apache/tools/ant/types/RedirectorElement;->append:Ljava/lang/Boolean;

    if-nez v0, :cond_0

    iget-object v0, p0, Lorg/apache/tools/ant/types/RedirectorElement;->createEmptyFiles:Ljava/lang/Boolean;

    if-nez v0, :cond_0

    iget-object v0, p0, Lorg/apache/tools/ant/types/RedirectorElement;->inputEncoding:Ljava/lang/String;

    if-nez v0, :cond_0

    iget-object v0, p0, Lorg/apache/tools/ant/types/RedirectorElement;->outputEncoding:Ljava/lang/String;

    if-nez v0, :cond_0

    iget-object v0, p0, Lorg/apache/tools/ant/types/RedirectorElement;->errorEncoding:Ljava/lang/String;

    if-nez v0, :cond_0

    iget-object v0, p0, Lorg/apache/tools/ant/types/RedirectorElement;->outputProperty:Ljava/lang/String;

    if-nez v0, :cond_0

    iget-object v0, p0, Lorg/apache/tools/ant/types/RedirectorElement;->errorProperty:Ljava/lang/String;

    if-nez v0, :cond_0

    iget-object v0, p0, Lorg/apache/tools/ant/types/RedirectorElement;->logInputString:Ljava/lang/Boolean;

    if-eqz v0, :cond_1

    :cond_0
    invoke-virtual {p0}, Lorg/apache/tools/ant/types/RedirectorElement;->tooManyAttributes()Lorg/apache/tools/ant/BuildException;

    move-result-object v0

    throw v0

    :cond_1
    invoke-super {p0, p1}, Lorg/apache/tools/ant/types/DataType;->setRefid(Lorg/apache/tools/ant/types/Reference;)V

    return-void
.end method

.method protected toFileArray([Ljava/lang/String;)[Ljava/io/File;
    .locals 4
    .param p1    # [Ljava/lang/String;

    if-nez p1, :cond_0

    const/4 v2, 0x0

    :goto_0
    return-object v2

    :cond_0
    new-instance v1, Ljava/util/ArrayList;

    array-length v2, p1

    invoke-direct {v1, v2}, Ljava/util/ArrayList;-><init>(I)V

    const/4 v0, 0x0

    :goto_1
    array-length v2, p1

    if-ge v0, v2, :cond_2

    aget-object v2, p1, v0

    if-eqz v2, :cond_1

    invoke-virtual {p0}, Lorg/apache/tools/ant/types/RedirectorElement;->getProject()Lorg/apache/tools/ant/Project;

    move-result-object v2

    aget-object v3, p1, v0

    invoke-virtual {v2, v3}, Lorg/apache/tools/ant/Project;->resolveFile(Ljava/lang/String;)Ljava/io/File;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    :cond_1
    add-int/lit8 v0, v0, 0x1

    goto :goto_1

    :cond_2
    invoke-virtual {v1}, Ljava/util/ArrayList;->size()I

    move-result v2

    new-array v2, v2, [Ljava/io/File;

    invoke-virtual {v1, v2}, Ljava/util/ArrayList;->toArray([Ljava/lang/Object;)[Ljava/lang/Object;

    move-result-object v2

    check-cast v2, [Ljava/io/File;

    check-cast v2, [Ljava/io/File;

    goto :goto_0
.end method
