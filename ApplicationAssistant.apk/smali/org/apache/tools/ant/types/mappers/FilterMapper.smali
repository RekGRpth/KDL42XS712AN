.class public Lorg/apache/tools/ant/types/mappers/FilterMapper;
.super Lorg/apache/tools/ant/types/FilterChain;
.source "FilterMapper.java"

# interfaces
.implements Lorg/apache/tools/ant/util/FileNameMapper;


# direct methods
.method public constructor <init>()V
    .locals 0

    invoke-direct {p0}, Lorg/apache/tools/ant/types/FilterChain;-><init>()V

    return-void
.end method


# virtual methods
.method public mapFileName(Ljava/lang/String;)[Ljava/lang/String;
    .locals 7
    .param p1    # Ljava/lang/String;

    :try_start_0
    new-instance v4, Ljava/io/StringReader;

    invoke-direct {v4, p1}, Ljava/io/StringReader;-><init>(Ljava/lang/String;)V

    new-instance v2, Lorg/apache/tools/ant/filters/util/ChainReaderHelper;

    invoke-direct {v2}, Lorg/apache/tools/ant/filters/util/ChainReaderHelper;-><init>()V

    const/16 v5, 0x2000

    invoke-virtual {v2, v5}, Lorg/apache/tools/ant/filters/util/ChainReaderHelper;->setBufferSize(I)V

    invoke-virtual {v2, v4}, Lorg/apache/tools/ant/filters/util/ChainReaderHelper;->setPrimaryReader(Ljava/io/Reader;)V

    invoke-virtual {p0}, Lorg/apache/tools/ant/types/mappers/FilterMapper;->getProject()Lorg/apache/tools/ant/Project;

    move-result-object v5

    invoke-virtual {v2, v5}, Lorg/apache/tools/ant/filters/util/ChainReaderHelper;->setProject(Lorg/apache/tools/ant/Project;)V

    new-instance v1, Ljava/util/Vector;

    invoke-direct {v1}, Ljava/util/Vector;-><init>()V

    invoke-virtual {v1, p0}, Ljava/util/Vector;->add(Ljava/lang/Object;)Z

    invoke-virtual {v2, v1}, Lorg/apache/tools/ant/filters/util/ChainReaderHelper;->setFilterChains(Ljava/util/Vector;)V

    invoke-virtual {v2}, Lorg/apache/tools/ant/filters/util/ChainReaderHelper;->getAssembledReader()Ljava/io/Reader;

    move-result-object v5

    invoke-static {v5}, Lorg/apache/tools/ant/util/FileUtils;->readFully(Ljava/io/Reader;)Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/String;->length()I

    move-result v5

    if-nez v5, :cond_0

    const/4 v5, 0x0

    :goto_0
    return-object v5

    :cond_0
    const/4 v5, 0x1

    new-array v5, v5, [Ljava/lang/String;

    const/4 v6, 0x0

    aput-object v3, v5, v6
    :try_end_0
    .catch Lorg/apache/tools/ant/BuildException; {:try_start_0 .. :try_end_0} :catch_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_1

    goto :goto_0

    :catch_0
    move-exception v0

    throw v0

    :catch_1
    move-exception v0

    new-instance v5, Lorg/apache/tools/ant/BuildException;

    invoke-direct {v5, v0}, Lorg/apache/tools/ant/BuildException;-><init>(Ljava/lang/Throwable;)V

    throw v5
.end method

.method public setFrom(Ljava/lang/String;)V
    .locals 3
    .param p1    # Ljava/lang/String;

    new-instance v0, Lorg/apache/tools/ant/UnsupportedAttributeException;

    const-string v1, "filtermapper doesn\'t support the \"from\" attribute."

    const-string v2, "from"

    invoke-direct {v0, v1, v2}, Lorg/apache/tools/ant/UnsupportedAttributeException;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    throw v0
.end method

.method public setTo(Ljava/lang/String;)V
    .locals 3
    .param p1    # Ljava/lang/String;

    new-instance v0, Lorg/apache/tools/ant/UnsupportedAttributeException;

    const-string v1, "filtermapper doesn\'t support the \"to\" attribute."

    const-string v2, "to"

    invoke-direct {v0, v1, v2}, Lorg/apache/tools/ant/UnsupportedAttributeException;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    throw v0
.end method
