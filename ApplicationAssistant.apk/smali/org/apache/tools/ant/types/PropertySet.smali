.class public Lorg/apache/tools/ant/types/PropertySet;
.super Lorg/apache/tools/ant/types/DataType;
.source "PropertySet.java"

# interfaces
.implements Lorg/apache/tools/ant/types/ResourceCollection;


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lorg/apache/tools/ant/types/PropertySet$BuiltinPropertySetName;,
        Lorg/apache/tools/ant/types/PropertySet$PropertyRef;
    }
.end annotation


# static fields
.field static class$org$apache$tools$ant$types$PropertySet:Ljava/lang/Class;


# instance fields
.field private cachedNames:Ljava/util/Set;

.field private dynamic:Z

.field private mapper:Lorg/apache/tools/ant/types/Mapper;

.field private negate:Z

.field private noAttributeSet:Z

.field private ptyRefs:Ljava/util/Vector;

.field private setRefs:Ljava/util/Vector;


# direct methods
.method public constructor <init>()V
    .locals 2

    const/4 v1, 0x1

    invoke-direct {p0}, Lorg/apache/tools/ant/types/DataType;-><init>()V

    iput-boolean v1, p0, Lorg/apache/tools/ant/types/PropertySet;->dynamic:Z

    const/4 v0, 0x0

    iput-boolean v0, p0, Lorg/apache/tools/ant/types/PropertySet;->negate:Z

    new-instance v0, Ljava/util/Vector;

    invoke-direct {v0}, Ljava/util/Vector;-><init>()V

    iput-object v0, p0, Lorg/apache/tools/ant/types/PropertySet;->ptyRefs:Ljava/util/Vector;

    new-instance v0, Ljava/util/Vector;

    invoke-direct {v0}, Ljava/util/Vector;-><init>()V

    iput-object v0, p0, Lorg/apache/tools/ant/types/PropertySet;->setRefs:Ljava/util/Vector;

    iput-boolean v1, p0, Lorg/apache/tools/ant/types/PropertySet;->noAttributeSet:Z

    return-void
.end method

.method private addPropertyNames(Ljava/util/Set;Ljava/util/Hashtable;)V
    .locals 8
    .param p1    # Ljava/util/Set;
    .param p2    # Ljava/util/Hashtable;

    iget-object v6, p0, Lorg/apache/tools/ant/types/PropertySet;->ptyRefs:Ljava/util/Vector;

    invoke-virtual {v6}, Ljava/util/Vector;->elements()Ljava/util/Enumeration;

    move-result-object v0

    :cond_0
    :goto_0
    invoke-interface {v0}, Ljava/util/Enumeration;->hasMoreElements()Z

    move-result v6

    if-eqz v6, :cond_a

    invoke-interface {v0}, Ljava/util/Enumeration;->nextElement()Ljava/lang/Object;

    move-result-object v5

    check-cast v5, Lorg/apache/tools/ant/types/PropertySet$PropertyRef;

    invoke-static {v5}, Lorg/apache/tools/ant/types/PropertySet$PropertyRef;->access$000(Lorg/apache/tools/ant/types/PropertySet$PropertyRef;)Ljava/lang/String;

    move-result-object v6

    if-eqz v6, :cond_1

    invoke-static {v5}, Lorg/apache/tools/ant/types/PropertySet$PropertyRef;->access$000(Lorg/apache/tools/ant/types/PropertySet$PropertyRef;)Ljava/lang/String;

    move-result-object v6

    invoke-virtual {p2, v6}, Ljava/util/Hashtable;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v6

    if-eqz v6, :cond_0

    invoke-static {v5}, Lorg/apache/tools/ant/types/PropertySet$PropertyRef;->access$000(Lorg/apache/tools/ant/types/PropertySet$PropertyRef;)Ljava/lang/String;

    move-result-object v6

    invoke-interface {p1, v6}, Ljava/util/Set;->add(Ljava/lang/Object;)Z

    goto :goto_0

    :cond_1
    invoke-static {v5}, Lorg/apache/tools/ant/types/PropertySet$PropertyRef;->access$100(Lorg/apache/tools/ant/types/PropertySet$PropertyRef;)Ljava/lang/String;

    move-result-object v6

    if-eqz v6, :cond_3

    invoke-virtual {p2}, Ljava/util/Hashtable;->keys()Ljava/util/Enumeration;

    move-result-object v4

    :cond_2
    :goto_1
    invoke-interface {v4}, Ljava/util/Enumeration;->hasMoreElements()Z

    move-result v6

    if-eqz v6, :cond_0

    invoke-interface {v4}, Ljava/util/Enumeration;->nextElement()Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Ljava/lang/String;

    invoke-static {v5}, Lorg/apache/tools/ant/types/PropertySet$PropertyRef;->access$100(Lorg/apache/tools/ant/types/PropertySet$PropertyRef;)Ljava/lang/String;

    move-result-object v6

    invoke-virtual {v3, v6}, Ljava/lang/String;->startsWith(Ljava/lang/String;)Z

    move-result v6

    if-eqz v6, :cond_2

    invoke-interface {p1, v3}, Ljava/util/Set;->add(Ljava/lang/Object;)Z

    goto :goto_1

    :cond_3
    invoke-static {v5}, Lorg/apache/tools/ant/types/PropertySet$PropertyRef;->access$200(Lorg/apache/tools/ant/types/PropertySet$PropertyRef;)Ljava/lang/String;

    move-result-object v6

    if-eqz v6, :cond_5

    new-instance v1, Lorg/apache/tools/ant/util/regexp/RegexpMatcherFactory;

    invoke-direct {v1}, Lorg/apache/tools/ant/util/regexp/RegexpMatcherFactory;-><init>()V

    invoke-virtual {v1}, Lorg/apache/tools/ant/util/regexp/RegexpMatcherFactory;->newRegexpMatcher()Lorg/apache/tools/ant/util/regexp/RegexpMatcher;

    move-result-object v2

    invoke-static {v5}, Lorg/apache/tools/ant/types/PropertySet$PropertyRef;->access$200(Lorg/apache/tools/ant/types/PropertySet$PropertyRef;)Ljava/lang/String;

    move-result-object v6

    invoke-interface {v2, v6}, Lorg/apache/tools/ant/util/regexp/RegexpMatcher;->setPattern(Ljava/lang/String;)V

    invoke-virtual {p2}, Ljava/util/Hashtable;->keys()Ljava/util/Enumeration;

    move-result-object v4

    :cond_4
    :goto_2
    invoke-interface {v4}, Ljava/util/Enumeration;->hasMoreElements()Z

    move-result v6

    if-eqz v6, :cond_0

    invoke-interface {v4}, Ljava/util/Enumeration;->nextElement()Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Ljava/lang/String;

    invoke-interface {v2, v3}, Lorg/apache/tools/ant/util/regexp/RegexpMatcher;->matches(Ljava/lang/String;)Z

    move-result v6

    if-eqz v6, :cond_4

    invoke-interface {p1, v3}, Ljava/util/Set;->add(Ljava/lang/Object;)Z

    goto :goto_2

    :cond_5
    invoke-static {v5}, Lorg/apache/tools/ant/types/PropertySet$PropertyRef;->access$300(Lorg/apache/tools/ant/types/PropertySet$PropertyRef;)Ljava/lang/String;

    move-result-object v6

    if-eqz v6, :cond_9

    invoke-static {v5}, Lorg/apache/tools/ant/types/PropertySet$PropertyRef;->access$300(Lorg/apache/tools/ant/types/PropertySet$PropertyRef;)Ljava/lang/String;

    move-result-object v6

    const-string v7, "all"

    invoke-virtual {v6, v7}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v6

    if-eqz v6, :cond_6

    invoke-virtual {p2}, Ljava/util/Hashtable;->keySet()Ljava/util/Set;

    move-result-object v6

    invoke-interface {p1, v6}, Ljava/util/Set;->addAll(Ljava/util/Collection;)Z

    goto/16 :goto_0

    :cond_6
    invoke-static {v5}, Lorg/apache/tools/ant/types/PropertySet$PropertyRef;->access$300(Lorg/apache/tools/ant/types/PropertySet$PropertyRef;)Ljava/lang/String;

    move-result-object v6

    const-string v7, "system"

    invoke-virtual {v6, v7}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v6

    if-eqz v6, :cond_7

    invoke-static {}, Ljava/lang/System;->getProperties()Ljava/util/Properties;

    move-result-object v6

    invoke-virtual {v6}, Ljava/util/Properties;->keySet()Ljava/util/Set;

    move-result-object v6

    invoke-interface {p1, v6}, Ljava/util/Set;->addAll(Ljava/util/Collection;)Z

    goto/16 :goto_0

    :cond_7
    invoke-static {v5}, Lorg/apache/tools/ant/types/PropertySet$PropertyRef;->access$300(Lorg/apache/tools/ant/types/PropertySet$PropertyRef;)Ljava/lang/String;

    move-result-object v6

    const-string v7, "commandline"

    invoke-virtual {v6, v7}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v6

    if-eqz v6, :cond_8

    invoke-virtual {p0}, Lorg/apache/tools/ant/types/PropertySet;->getProject()Lorg/apache/tools/ant/Project;

    move-result-object v6

    invoke-virtual {v6}, Lorg/apache/tools/ant/Project;->getUserProperties()Ljava/util/Hashtable;

    move-result-object v6

    invoke-virtual {v6}, Ljava/util/Hashtable;->keySet()Ljava/util/Set;

    move-result-object v6

    invoke-interface {p1, v6}, Ljava/util/Set;->addAll(Ljava/util/Collection;)Z

    goto/16 :goto_0

    :cond_8
    new-instance v6, Lorg/apache/tools/ant/BuildException;

    const-string v7, "Impossible: Invalid builtin attribute!"

    invoke-direct {v6, v7}, Lorg/apache/tools/ant/BuildException;-><init>(Ljava/lang/String;)V

    throw v6

    :cond_9
    new-instance v6, Lorg/apache/tools/ant/BuildException;

    const-string v7, "Impossible: Invalid PropertyRef!"

    invoke-direct {v6, v7}, Lorg/apache/tools/ant/BuildException;-><init>(Ljava/lang/String;)V

    throw v6

    :cond_a
    return-void
.end method

.method static class$(Ljava/lang/String;)Ljava/lang/Class;
    .locals 3
    .param p0    # Ljava/lang/String;

    :try_start_0
    invoke-static {p0}, Ljava/lang/Class;->forName(Ljava/lang/String;)Ljava/lang/Class;
    :try_end_0
    .catch Ljava/lang/ClassNotFoundException; {:try_start_0 .. :try_end_0} :catch_0

    move-result-object v1

    return-object v1

    :catch_0
    move-exception v0

    new-instance v1, Ljava/lang/NoClassDefFoundError;

    invoke-virtual {v0}, Ljava/lang/ClassNotFoundException;->getMessage()Ljava/lang/String;

    move-result-object v2

    invoke-direct {v1, v2}, Ljava/lang/NoClassDefFoundError;-><init>(Ljava/lang/String;)V

    throw v1
.end method

.method private getAllSystemProperties()Ljava/util/Hashtable;
    .locals 4

    new-instance v2, Ljava/util/Hashtable;

    invoke-direct {v2}, Ljava/util/Hashtable;-><init>()V

    invoke-static {}, Ljava/lang/System;->getProperties()Ljava/util/Properties;

    move-result-object v3

    invoke-virtual {v3}, Ljava/util/Properties;->propertyNames()Ljava/util/Enumeration;

    move-result-object v0

    :goto_0
    invoke-interface {v0}, Ljava/util/Enumeration;->hasMoreElements()Z

    move-result v3

    if-eqz v3, :cond_0

    invoke-interface {v0}, Ljava/util/Enumeration;->nextElement()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Ljava/lang/String;

    invoke-static {}, Ljava/lang/System;->getProperties()Ljava/util/Properties;

    move-result-object v3

    invoke-virtual {v3, v1}, Ljava/util/Properties;->getProperty(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v1, v3}, Ljava/util/Hashtable;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    goto :goto_0

    :cond_0
    return-object v2
.end method


# virtual methods
.method public add(Lorg/apache/tools/ant/util/FileNameMapper;)V
    .locals 1
    .param p1    # Lorg/apache/tools/ant/util/FileNameMapper;

    invoke-virtual {p0}, Lorg/apache/tools/ant/types/PropertySet;->createMapper()Lorg/apache/tools/ant/types/Mapper;

    move-result-object v0

    invoke-virtual {v0, p1}, Lorg/apache/tools/ant/types/Mapper;->add(Lorg/apache/tools/ant/util/FileNameMapper;)V

    return-void
.end method

.method public addPropertyref(Lorg/apache/tools/ant/types/PropertySet$PropertyRef;)V
    .locals 1
    .param p1    # Lorg/apache/tools/ant/types/PropertySet$PropertyRef;

    invoke-virtual {p0}, Lorg/apache/tools/ant/types/PropertySet;->assertNotReference()V

    iget-object v0, p0, Lorg/apache/tools/ant/types/PropertySet;->ptyRefs:Ljava/util/Vector;

    invoke-virtual {v0, p1}, Ljava/util/Vector;->addElement(Ljava/lang/Object;)V

    return-void
.end method

.method public addPropertyset(Lorg/apache/tools/ant/types/PropertySet;)V
    .locals 1
    .param p1    # Lorg/apache/tools/ant/types/PropertySet;

    invoke-virtual {p0}, Lorg/apache/tools/ant/types/PropertySet;->assertNotReference()V

    iget-object v0, p0, Lorg/apache/tools/ant/types/PropertySet;->setRefs:Ljava/util/Vector;

    invoke-virtual {v0, p1}, Ljava/util/Vector;->addElement(Ljava/lang/Object;)V

    return-void
.end method

.method public appendBuiltin(Lorg/apache/tools/ant/types/PropertySet$BuiltinPropertySetName;)V
    .locals 1
    .param p1    # Lorg/apache/tools/ant/types/PropertySet$BuiltinPropertySetName;

    new-instance v0, Lorg/apache/tools/ant/types/PropertySet$PropertyRef;

    invoke-direct {v0}, Lorg/apache/tools/ant/types/PropertySet$PropertyRef;-><init>()V

    invoke-virtual {v0, p1}, Lorg/apache/tools/ant/types/PropertySet$PropertyRef;->setBuiltin(Lorg/apache/tools/ant/types/PropertySet$BuiltinPropertySetName;)V

    invoke-virtual {p0, v0}, Lorg/apache/tools/ant/types/PropertySet;->addPropertyref(Lorg/apache/tools/ant/types/PropertySet$PropertyRef;)V

    return-void
.end method

.method public appendName(Ljava/lang/String;)V
    .locals 1
    .param p1    # Ljava/lang/String;

    new-instance v0, Lorg/apache/tools/ant/types/PropertySet$PropertyRef;

    invoke-direct {v0}, Lorg/apache/tools/ant/types/PropertySet$PropertyRef;-><init>()V

    invoke-virtual {v0, p1}, Lorg/apache/tools/ant/types/PropertySet$PropertyRef;->setName(Ljava/lang/String;)V

    invoke-virtual {p0, v0}, Lorg/apache/tools/ant/types/PropertySet;->addPropertyref(Lorg/apache/tools/ant/types/PropertySet$PropertyRef;)V

    return-void
.end method

.method public appendPrefix(Ljava/lang/String;)V
    .locals 1
    .param p1    # Ljava/lang/String;

    new-instance v0, Lorg/apache/tools/ant/types/PropertySet$PropertyRef;

    invoke-direct {v0}, Lorg/apache/tools/ant/types/PropertySet$PropertyRef;-><init>()V

    invoke-virtual {v0, p1}, Lorg/apache/tools/ant/types/PropertySet$PropertyRef;->setPrefix(Ljava/lang/String;)V

    invoke-virtual {p0, v0}, Lorg/apache/tools/ant/types/PropertySet;->addPropertyref(Lorg/apache/tools/ant/types/PropertySet$PropertyRef;)V

    return-void
.end method

.method public appendRegex(Ljava/lang/String;)V
    .locals 1
    .param p1    # Ljava/lang/String;

    new-instance v0, Lorg/apache/tools/ant/types/PropertySet$PropertyRef;

    invoke-direct {v0}, Lorg/apache/tools/ant/types/PropertySet$PropertyRef;-><init>()V

    invoke-virtual {v0, p1}, Lorg/apache/tools/ant/types/PropertySet$PropertyRef;->setRegex(Ljava/lang/String;)V

    invoke-virtual {p0, v0}, Lorg/apache/tools/ant/types/PropertySet;->addPropertyref(Lorg/apache/tools/ant/types/PropertySet$PropertyRef;)V

    return-void
.end method

.method protected final assertNotReference()V
    .locals 1

    invoke-virtual {p0}, Lorg/apache/tools/ant/types/PropertySet;->isReference()Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-virtual {p0}, Lorg/apache/tools/ant/types/PropertySet;->tooManyAttributes()Lorg/apache/tools/ant/BuildException;

    move-result-object v0

    throw v0

    :cond_0
    const/4 v0, 0x0

    iput-boolean v0, p0, Lorg/apache/tools/ant/types/PropertySet;->noAttributeSet:Z

    return-void
.end method

.method public createMapper()Lorg/apache/tools/ant/types/Mapper;
    .locals 2

    invoke-virtual {p0}, Lorg/apache/tools/ant/types/PropertySet;->assertNotReference()V

    iget-object v0, p0, Lorg/apache/tools/ant/types/PropertySet;->mapper:Lorg/apache/tools/ant/types/Mapper;

    if-eqz v0, :cond_0

    new-instance v0, Lorg/apache/tools/ant/BuildException;

    const-string v1, "Too many <mapper>s!"

    invoke-direct {v0, v1}, Lorg/apache/tools/ant/BuildException;-><init>(Ljava/lang/String;)V

    throw v0

    :cond_0
    new-instance v0, Lorg/apache/tools/ant/types/Mapper;

    invoke-virtual {p0}, Lorg/apache/tools/ant/types/PropertySet;->getProject()Lorg/apache/tools/ant/Project;

    move-result-object v1

    invoke-direct {v0, v1}, Lorg/apache/tools/ant/types/Mapper;-><init>(Lorg/apache/tools/ant/Project;)V

    iput-object v0, p0, Lorg/apache/tools/ant/types/PropertySet;->mapper:Lorg/apache/tools/ant/types/Mapper;

    iget-object v0, p0, Lorg/apache/tools/ant/types/PropertySet;->mapper:Lorg/apache/tools/ant/types/Mapper;

    return-object v0
.end method

.method public getDynamic()Z
    .locals 1

    invoke-virtual {p0}, Lorg/apache/tools/ant/types/PropertySet;->isReference()Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-virtual {p0}, Lorg/apache/tools/ant/types/PropertySet;->getRef()Lorg/apache/tools/ant/types/PropertySet;

    move-result-object v0

    iget-boolean v0, v0, Lorg/apache/tools/ant/types/PropertySet;->dynamic:Z

    :goto_0
    return v0

    :cond_0
    iget-boolean v0, p0, Lorg/apache/tools/ant/types/PropertySet;->dynamic:Z

    goto :goto_0
.end method

.method public getMapper()Lorg/apache/tools/ant/types/Mapper;
    .locals 1

    invoke-virtual {p0}, Lorg/apache/tools/ant/types/PropertySet;->isReference()Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-virtual {p0}, Lorg/apache/tools/ant/types/PropertySet;->getRef()Lorg/apache/tools/ant/types/PropertySet;

    move-result-object v0

    iget-object v0, v0, Lorg/apache/tools/ant/types/PropertySet;->mapper:Lorg/apache/tools/ant/types/Mapper;

    :goto_0
    return-object v0

    :cond_0
    iget-object v0, p0, Lorg/apache/tools/ant/types/PropertySet;->mapper:Lorg/apache/tools/ant/types/Mapper;

    goto :goto_0
.end method

.method public getProperties()Ljava/util/Properties;
    .locals 14

    invoke-virtual {p0}, Lorg/apache/tools/ant/types/PropertySet;->isReference()Z

    move-result v13

    if-eqz v13, :cond_1

    invoke-virtual {p0}, Lorg/apache/tools/ant/types/PropertySet;->getRef()Lorg/apache/tools/ant/types/PropertySet;

    move-result-object v13

    invoke-virtual {v13}, Lorg/apache/tools/ant/types/PropertySet;->getProperties()Ljava/util/Properties;

    move-result-object v9

    :cond_0
    return-object v9

    :cond_1
    const/4 v6, 0x0

    invoke-virtual {p0}, Lorg/apache/tools/ant/types/PropertySet;->getProject()Lorg/apache/tools/ant/Project;

    move-result-object v8

    if-nez v8, :cond_2

    invoke-direct {p0}, Lorg/apache/tools/ant/types/PropertySet;->getAllSystemProperties()Ljava/util/Hashtable;

    move-result-object v10

    :goto_0
    iget-object v13, p0, Lorg/apache/tools/ant/types/PropertySet;->setRefs:Ljava/util/Vector;

    invoke-virtual {v13}, Ljava/util/Vector;->elements()Ljava/util/Enumeration;

    move-result-object v1

    :goto_1
    invoke-interface {v1}, Ljava/util/Enumeration;->hasMoreElements()Z

    move-result v13

    if-eqz v13, :cond_3

    invoke-interface {v1}, Ljava/util/Enumeration;->nextElement()Ljava/lang/Object;

    move-result-object v11

    check-cast v11, Lorg/apache/tools/ant/types/PropertySet;

    invoke-virtual {v11}, Lorg/apache/tools/ant/types/PropertySet;->getProperties()Ljava/util/Properties;

    move-result-object v13

    invoke-virtual {v10, v13}, Ljava/util/Hashtable;->putAll(Ljava/util/Map;)V

    goto :goto_1

    :cond_2
    invoke-virtual {v8}, Lorg/apache/tools/ant/Project;->getProperties()Ljava/util/Hashtable;

    move-result-object v10

    goto :goto_0

    :cond_3
    invoke-virtual {p0}, Lorg/apache/tools/ant/types/PropertySet;->getDynamic()Z

    move-result v13

    if-nez v13, :cond_4

    iget-object v13, p0, Lorg/apache/tools/ant/types/PropertySet;->cachedNames:Ljava/util/Set;

    if-nez v13, :cond_b

    :cond_4
    new-instance v6, Ljava/util/HashSet;

    invoke-direct {v6}, Ljava/util/HashSet;-><init>()V

    invoke-direct {p0, v6, v10}, Lorg/apache/tools/ant/types/PropertySet;->addPropertyNames(Ljava/util/Set;Ljava/util/Hashtable;)V

    iget-object v13, p0, Lorg/apache/tools/ant/types/PropertySet;->setRefs:Ljava/util/Vector;

    invoke-virtual {v13}, Ljava/util/Vector;->elements()Ljava/util/Enumeration;

    move-result-object v1

    :goto_2
    invoke-interface {v1}, Ljava/util/Enumeration;->hasMoreElements()Z

    move-result v13

    if-eqz v13, :cond_5

    invoke-interface {v1}, Ljava/util/Enumeration;->nextElement()Ljava/lang/Object;

    move-result-object v11

    check-cast v11, Lorg/apache/tools/ant/types/PropertySet;

    invoke-virtual {v11}, Lorg/apache/tools/ant/types/PropertySet;->getProperties()Ljava/util/Properties;

    move-result-object v13

    invoke-virtual {v13}, Ljava/util/Properties;->keySet()Ljava/util/Set;

    move-result-object v13

    invoke-interface {v6, v13}, Ljava/util/Set;->addAll(Ljava/util/Collection;)Z

    goto :goto_2

    :cond_5
    iget-boolean v13, p0, Lorg/apache/tools/ant/types/PropertySet;->negate:Z

    if-eqz v13, :cond_6

    new-instance v0, Ljava/util/HashSet;

    invoke-virtual {v10}, Ljava/util/Hashtable;->keySet()Ljava/util/Set;

    move-result-object v13

    invoke-direct {v0, v13}, Ljava/util/HashSet;-><init>(Ljava/util/Collection;)V

    invoke-virtual {v0, v6}, Ljava/util/HashSet;->removeAll(Ljava/util/Collection;)Z

    move-object v6, v0

    :cond_6
    invoke-virtual {p0}, Lorg/apache/tools/ant/types/PropertySet;->getDynamic()Z

    move-result v13

    if-nez v13, :cond_7

    iput-object v6, p0, Lorg/apache/tools/ant/types/PropertySet;->cachedNames:Ljava/util/Set;

    :cond_7
    :goto_3
    const/4 v3, 0x0

    invoke-virtual {p0}, Lorg/apache/tools/ant/types/PropertySet;->getMapper()Lorg/apache/tools/ant/types/Mapper;

    move-result-object v4

    if-eqz v4, :cond_8

    invoke-virtual {v4}, Lorg/apache/tools/ant/types/Mapper;->getImplementation()Lorg/apache/tools/ant/util/FileNameMapper;

    move-result-object v3

    :cond_8
    new-instance v9, Ljava/util/Properties;

    invoke-direct {v9}, Ljava/util/Properties;-><init>()V

    invoke-interface {v6}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    move-result-object v2

    :cond_9
    :goto_4
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    move-result v13

    if-eqz v13, :cond_0

    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v5

    check-cast v5, Ljava/lang/String;

    invoke-virtual {v10, v5}, Ljava/util/Hashtable;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v12

    check-cast v12, Ljava/lang/String;

    if-eqz v12, :cond_9

    if-eqz v3, :cond_a

    invoke-interface {v3, v5}, Lorg/apache/tools/ant/util/FileNameMapper;->mapFileName(Ljava/lang/String;)[Ljava/lang/String;

    move-result-object v7

    if-eqz v7, :cond_a

    const/4 v13, 0x0

    aget-object v5, v7, v13

    :cond_a
    invoke-virtual {v9, v5, v12}, Ljava/util/Properties;->setProperty(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/Object;

    goto :goto_4

    :cond_b
    iget-object v6, p0, Lorg/apache/tools/ant/types/PropertySet;->cachedNames:Ljava/util/Set;

    goto :goto_3
.end method

.method protected getRef()Lorg/apache/tools/ant/types/PropertySet;
    .locals 2

    sget-object v0, Lorg/apache/tools/ant/types/PropertySet;->class$org$apache$tools$ant$types$PropertySet:Ljava/lang/Class;

    if-nez v0, :cond_0

    const-string v0, "org.apache.tools.ant.types.PropertySet"

    invoke-static {v0}, Lorg/apache/tools/ant/types/PropertySet;->class$(Ljava/lang/String;)Ljava/lang/Class;

    move-result-object v0

    sput-object v0, Lorg/apache/tools/ant/types/PropertySet;->class$org$apache$tools$ant$types$PropertySet:Ljava/lang/Class;

    :goto_0
    const-string v1, "propertyset"

    invoke-virtual {p0, v0, v1}, Lorg/apache/tools/ant/types/PropertySet;->getCheckedRef(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lorg/apache/tools/ant/types/PropertySet;

    return-object v0

    :cond_0
    sget-object v0, Lorg/apache/tools/ant/types/PropertySet;->class$org$apache$tools$ant$types$PropertySet:Ljava/lang/Class;

    goto :goto_0
.end method

.method public isFilesystemOnly()Z
    .locals 1

    invoke-virtual {p0}, Lorg/apache/tools/ant/types/PropertySet;->isReference()Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-virtual {p0}, Lorg/apache/tools/ant/types/PropertySet;->getRef()Lorg/apache/tools/ant/types/PropertySet;

    move-result-object v0

    invoke-virtual {v0}, Lorg/apache/tools/ant/types/PropertySet;->isFilesystemOnly()Z

    move-result v0

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public iterator()Ljava/util/Iterator;
    .locals 2

    invoke-virtual {p0}, Lorg/apache/tools/ant/types/PropertySet;->getProperties()Ljava/util/Properties;

    move-result-object v1

    invoke-virtual {v1}, Ljava/util/Properties;->propertyNames()Ljava/util/Enumeration;

    move-result-object v0

    new-instance v1, Lorg/apache/tools/ant/types/PropertySet$1;

    invoke-direct {v1, p0, v0}, Lorg/apache/tools/ant/types/PropertySet$1;-><init>(Lorg/apache/tools/ant/types/PropertySet;Ljava/util/Enumeration;)V

    return-object v1
.end method

.method public setDynamic(Z)V
    .locals 0
    .param p1    # Z

    invoke-virtual {p0}, Lorg/apache/tools/ant/types/PropertySet;->assertNotReference()V

    iput-boolean p1, p0, Lorg/apache/tools/ant/types/PropertySet;->dynamic:Z

    return-void
.end method

.method public setMapper(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V
    .locals 2
    .param p1    # Ljava/lang/String;
    .param p2    # Ljava/lang/String;
    .param p3    # Ljava/lang/String;

    invoke-virtual {p0}, Lorg/apache/tools/ant/types/PropertySet;->createMapper()Lorg/apache/tools/ant/types/Mapper;

    move-result-object v0

    new-instance v1, Lorg/apache/tools/ant/types/Mapper$MapperType;

    invoke-direct {v1}, Lorg/apache/tools/ant/types/Mapper$MapperType;-><init>()V

    invoke-virtual {v1, p1}, Lorg/apache/tools/ant/types/Mapper$MapperType;->setValue(Ljava/lang/String;)V

    invoke-virtual {v0, v1}, Lorg/apache/tools/ant/types/Mapper;->setType(Lorg/apache/tools/ant/types/Mapper$MapperType;)V

    invoke-virtual {v0, p2}, Lorg/apache/tools/ant/types/Mapper;->setFrom(Ljava/lang/String;)V

    invoke-virtual {v0, p3}, Lorg/apache/tools/ant/types/Mapper;->setTo(Ljava/lang/String;)V

    return-void
.end method

.method public setNegate(Z)V
    .locals 0
    .param p1    # Z

    invoke-virtual {p0}, Lorg/apache/tools/ant/types/PropertySet;->assertNotReference()V

    iput-boolean p1, p0, Lorg/apache/tools/ant/types/PropertySet;->negate:Z

    return-void
.end method

.method public final setRefid(Lorg/apache/tools/ant/types/Reference;)V
    .locals 1
    .param p1    # Lorg/apache/tools/ant/types/Reference;

    iget-boolean v0, p0, Lorg/apache/tools/ant/types/PropertySet;->noAttributeSet:Z

    if-nez v0, :cond_0

    invoke-virtual {p0}, Lorg/apache/tools/ant/types/PropertySet;->tooManyAttributes()Lorg/apache/tools/ant/BuildException;

    move-result-object v0

    throw v0

    :cond_0
    invoke-super {p0, p1}, Lorg/apache/tools/ant/types/DataType;->setRefid(Lorg/apache/tools/ant/types/Reference;)V

    return-void
.end method

.method public size()I
    .locals 1

    invoke-virtual {p0}, Lorg/apache/tools/ant/types/PropertySet;->isReference()Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-virtual {p0}, Lorg/apache/tools/ant/types/PropertySet;->getRef()Lorg/apache/tools/ant/types/PropertySet;

    move-result-object v0

    invoke-virtual {v0}, Lorg/apache/tools/ant/types/PropertySet;->size()I

    move-result v0

    :goto_0
    return v0

    :cond_0
    invoke-virtual {p0}, Lorg/apache/tools/ant/types/PropertySet;->getProperties()Ljava/util/Properties;

    move-result-object v0

    invoke-virtual {v0}, Ljava/util/Properties;->size()I

    move-result v0

    goto :goto_0
.end method

.method public toString()Ljava/lang/String;
    .locals 5

    new-instance v0, Ljava/lang/StringBuffer;

    invoke-direct {v0}, Ljava/lang/StringBuffer;-><init>()V

    new-instance v3, Ljava/util/TreeMap;

    invoke-virtual {p0}, Lorg/apache/tools/ant/types/PropertySet;->getProperties()Ljava/util/Properties;

    move-result-object v4

    invoke-direct {v3, v4}, Ljava/util/TreeMap;-><init>(Ljava/util/Map;)V

    invoke-virtual {v3}, Ljava/util/TreeMap;->entrySet()Ljava/util/Set;

    move-result-object v4

    invoke-interface {v4}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    move-result-object v2

    :goto_0
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    move-result v4

    if-eqz v4, :cond_1

    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Ljava/util/Map$Entry;

    invoke-virtual {v0}, Ljava/lang/StringBuffer;->length()I

    move-result v4

    if-eqz v4, :cond_0

    const-string v4, ", "

    invoke-virtual {v0, v4}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    :cond_0
    invoke-interface {v1}, Ljava/util/Map$Entry;->getKey()Ljava/lang/Object;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v0, v4}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    const-string v4, "="

    invoke-virtual {v0, v4}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    invoke-interface {v1}, Ljava/util/Map$Entry;->getValue()Ljava/lang/Object;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v0, v4}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    goto :goto_0

    :cond_1
    invoke-virtual {v0}, Ljava/lang/StringBuffer;->toString()Ljava/lang/String;

    move-result-object v4

    return-object v4
.end method
