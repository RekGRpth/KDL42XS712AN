.class public Lorg/apache/tools/ant/types/PropertySet$PropertyRef;
.super Ljava/lang/Object;
.source "PropertySet.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lorg/apache/tools/ant/types/PropertySet;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x9
    name = "PropertyRef"
.end annotation


# instance fields
.field private builtin:Ljava/lang/String;

.field private count:I

.field private name:Ljava/lang/String;

.field private prefix:Ljava/lang/String;

.field private regex:Ljava/lang/String;


# direct methods
.method public constructor <init>()V
    .locals 0

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method static access$000(Lorg/apache/tools/ant/types/PropertySet$PropertyRef;)Ljava/lang/String;
    .locals 1
    .param p0    # Lorg/apache/tools/ant/types/PropertySet$PropertyRef;

    iget-object v0, p0, Lorg/apache/tools/ant/types/PropertySet$PropertyRef;->name:Ljava/lang/String;

    return-object v0
.end method

.method static access$100(Lorg/apache/tools/ant/types/PropertySet$PropertyRef;)Ljava/lang/String;
    .locals 1
    .param p0    # Lorg/apache/tools/ant/types/PropertySet$PropertyRef;

    iget-object v0, p0, Lorg/apache/tools/ant/types/PropertySet$PropertyRef;->prefix:Ljava/lang/String;

    return-object v0
.end method

.method static access$200(Lorg/apache/tools/ant/types/PropertySet$PropertyRef;)Ljava/lang/String;
    .locals 1
    .param p0    # Lorg/apache/tools/ant/types/PropertySet$PropertyRef;

    iget-object v0, p0, Lorg/apache/tools/ant/types/PropertySet$PropertyRef;->regex:Ljava/lang/String;

    return-object v0
.end method

.method static access$300(Lorg/apache/tools/ant/types/PropertySet$PropertyRef;)Ljava/lang/String;
    .locals 1
    .param p0    # Lorg/apache/tools/ant/types/PropertySet$PropertyRef;

    iget-object v0, p0, Lorg/apache/tools/ant/types/PropertySet$PropertyRef;->builtin:Ljava/lang/String;

    return-object v0
.end method

.method private assertValid(Ljava/lang/String;Ljava/lang/String;)V
    .locals 3
    .param p1    # Ljava/lang/String;
    .param p2    # Ljava/lang/String;

    const/4 v1, 0x1

    if-eqz p2, :cond_0

    invoke-virtual {p2}, Ljava/lang/String;->length()I

    move-result v0

    if-ge v0, v1, :cond_1

    :cond_0
    new-instance v0, Lorg/apache/tools/ant/BuildException;

    new-instance v1, Ljava/lang/StringBuffer;

    invoke-direct {v1}, Ljava/lang/StringBuffer;-><init>()V

    const-string v2, "Invalid attribute: "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    move-result-object v1

    invoke-virtual {v1, p1}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuffer;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Lorg/apache/tools/ant/BuildException;-><init>(Ljava/lang/String;)V

    throw v0

    :cond_1
    iget v0, p0, Lorg/apache/tools/ant/types/PropertySet$PropertyRef;->count:I

    add-int/lit8 v0, v0, 0x1

    iput v0, p0, Lorg/apache/tools/ant/types/PropertySet$PropertyRef;->count:I

    if-eq v0, v1, :cond_2

    new-instance v0, Lorg/apache/tools/ant/BuildException;

    const-string v1, "Attributes name, regex, and prefix are mutually exclusive"

    invoke-direct {v0, v1}, Lorg/apache/tools/ant/BuildException;-><init>(Ljava/lang/String;)V

    throw v0

    :cond_2
    return-void
.end method


# virtual methods
.method public setBuiltin(Lorg/apache/tools/ant/types/PropertySet$BuiltinPropertySetName;)V
    .locals 2
    .param p1    # Lorg/apache/tools/ant/types/PropertySet$BuiltinPropertySetName;

    invoke-virtual {p1}, Lorg/apache/tools/ant/types/PropertySet$BuiltinPropertySetName;->getValue()Ljava/lang/String;

    move-result-object v0

    const-string v1, "builtin"

    invoke-direct {p0, v1, v0}, Lorg/apache/tools/ant/types/PropertySet$PropertyRef;->assertValid(Ljava/lang/String;Ljava/lang/String;)V

    iput-object v0, p0, Lorg/apache/tools/ant/types/PropertySet$PropertyRef;->builtin:Ljava/lang/String;

    return-void
.end method

.method public setName(Ljava/lang/String;)V
    .locals 1
    .param p1    # Ljava/lang/String;

    const-string v0, "name"

    invoke-direct {p0, v0, p1}, Lorg/apache/tools/ant/types/PropertySet$PropertyRef;->assertValid(Ljava/lang/String;Ljava/lang/String;)V

    iput-object p1, p0, Lorg/apache/tools/ant/types/PropertySet$PropertyRef;->name:Ljava/lang/String;

    return-void
.end method

.method public setPrefix(Ljava/lang/String;)V
    .locals 1
    .param p1    # Ljava/lang/String;

    const-string v0, "prefix"

    invoke-direct {p0, v0, p1}, Lorg/apache/tools/ant/types/PropertySet$PropertyRef;->assertValid(Ljava/lang/String;Ljava/lang/String;)V

    iput-object p1, p0, Lorg/apache/tools/ant/types/PropertySet$PropertyRef;->prefix:Ljava/lang/String;

    return-void
.end method

.method public setRegex(Ljava/lang/String;)V
    .locals 1
    .param p1    # Ljava/lang/String;

    const-string v0, "regex"

    invoke-direct {p0, v0, p1}, Lorg/apache/tools/ant/types/PropertySet$PropertyRef;->assertValid(Ljava/lang/String;Ljava/lang/String;)V

    iput-object p1, p0, Lorg/apache/tools/ant/types/PropertySet$PropertyRef;->regex:Ljava/lang/String;

    return-void
.end method

.method public toString()Ljava/lang/String;
    .locals 2

    new-instance v0, Ljava/lang/StringBuffer;

    invoke-direct {v0}, Ljava/lang/StringBuffer;-><init>()V

    const-string v1, "name="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    move-result-object v0

    iget-object v1, p0, Lorg/apache/tools/ant/types/PropertySet$PropertyRef;->name:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    move-result-object v0

    const-string v1, ", regex="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    move-result-object v0

    iget-object v1, p0, Lorg/apache/tools/ant/types/PropertySet$PropertyRef;->regex:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    move-result-object v0

    const-string v1, ", prefix="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    move-result-object v0

    iget-object v1, p0, Lorg/apache/tools/ant/types/PropertySet$PropertyRef;->prefix:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    move-result-object v0

    const-string v1, ", builtin="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    move-result-object v0

    iget-object v1, p0, Lorg/apache/tools/ant/types/PropertySet$PropertyRef;->builtin:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuffer;->toString()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method
