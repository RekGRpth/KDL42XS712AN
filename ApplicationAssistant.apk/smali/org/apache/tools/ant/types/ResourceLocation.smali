.class public Lorg/apache/tools/ant/types/ResourceLocation;
.super Ljava/lang/Object;
.source "ResourceLocation.java"


# instance fields
.field private base:Ljava/net/URL;

.field private location:Ljava/lang/String;

.field private publicId:Ljava/lang/String;


# direct methods
.method public constructor <init>()V
    .locals 1

    const/4 v0, 0x0

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    iput-object v0, p0, Lorg/apache/tools/ant/types/ResourceLocation;->publicId:Ljava/lang/String;

    iput-object v0, p0, Lorg/apache/tools/ant/types/ResourceLocation;->location:Ljava/lang/String;

    iput-object v0, p0, Lorg/apache/tools/ant/types/ResourceLocation;->base:Ljava/net/URL;

    return-void
.end method


# virtual methods
.method public getBase()Ljava/net/URL;
    .locals 1

    iget-object v0, p0, Lorg/apache/tools/ant/types/ResourceLocation;->base:Ljava/net/URL;

    return-object v0
.end method

.method public getLocation()Ljava/lang/String;
    .locals 1

    iget-object v0, p0, Lorg/apache/tools/ant/types/ResourceLocation;->location:Ljava/lang/String;

    return-object v0
.end method

.method public getPublicId()Ljava/lang/String;
    .locals 1

    iget-object v0, p0, Lorg/apache/tools/ant/types/ResourceLocation;->publicId:Ljava/lang/String;

    return-object v0
.end method

.method public setBase(Ljava/net/URL;)V
    .locals 0
    .param p1    # Ljava/net/URL;

    iput-object p1, p0, Lorg/apache/tools/ant/types/ResourceLocation;->base:Ljava/net/URL;

    return-void
.end method

.method public setLocation(Ljava/lang/String;)V
    .locals 0
    .param p1    # Ljava/lang/String;

    iput-object p1, p0, Lorg/apache/tools/ant/types/ResourceLocation;->location:Ljava/lang/String;

    return-void
.end method

.method public setPublicId(Ljava/lang/String;)V
    .locals 0
    .param p1    # Ljava/lang/String;

    iput-object p1, p0, Lorg/apache/tools/ant/types/ResourceLocation;->publicId:Ljava/lang/String;

    return-void
.end method
