.class public Lorg/apache/tools/ant/util/StringTokenizer;
.super Lorg/apache/tools/ant/ProjectComponent;
.source "StringTokenizer.java"

# interfaces
.implements Lorg/apache/tools/ant/util/Tokenizer;


# instance fields
.field private delims:[C

.field private delimsAreTokens:Z

.field private includeDelims:Z

.field private intraString:Ljava/lang/String;

.field private pushed:I

.field private suppressDelims:Z


# direct methods
.method public constructor <init>()V
    .locals 2

    const/4 v1, 0x0

    invoke-direct {p0}, Lorg/apache/tools/ant/ProjectComponent;-><init>()V

    const-string v0, ""

    iput-object v0, p0, Lorg/apache/tools/ant/util/StringTokenizer;->intraString:Ljava/lang/String;

    const/4 v0, -0x2

    iput v0, p0, Lorg/apache/tools/ant/util/StringTokenizer;->pushed:I

    const/4 v0, 0x0

    iput-object v0, p0, Lorg/apache/tools/ant/util/StringTokenizer;->delims:[C

    iput-boolean v1, p0, Lorg/apache/tools/ant/util/StringTokenizer;->delimsAreTokens:Z

    iput-boolean v1, p0, Lorg/apache/tools/ant/util/StringTokenizer;->suppressDelims:Z

    iput-boolean v1, p0, Lorg/apache/tools/ant/util/StringTokenizer;->includeDelims:Z

    return-void
.end method

.method private isDelim(C)Z
    .locals 2
    .param p1    # C

    iget-object v1, p0, Lorg/apache/tools/ant/util/StringTokenizer;->delims:[C

    if-nez v1, :cond_0

    invoke-static {p1}, Ljava/lang/Character;->isWhitespace(C)Z

    move-result v1

    :goto_0
    return v1

    :cond_0
    const/4 v0, 0x0

    :goto_1
    iget-object v1, p0, Lorg/apache/tools/ant/util/StringTokenizer;->delims:[C

    array-length v1, v1

    if-ge v0, v1, :cond_2

    iget-object v1, p0, Lorg/apache/tools/ant/util/StringTokenizer;->delims:[C

    aget-char v1, v1, v0

    if-ne v1, p1, :cond_1

    const/4 v1, 0x1

    goto :goto_0

    :cond_1
    add-int/lit8 v0, v0, 0x1

    goto :goto_1

    :cond_2
    const/4 v1, 0x0

    goto :goto_0
.end method


# virtual methods
.method public getPostToken()Ljava/lang/String;
    .locals 1

    iget-boolean v0, p0, Lorg/apache/tools/ant/util/StringTokenizer;->suppressDelims:Z

    if-nez v0, :cond_0

    iget-boolean v0, p0, Lorg/apache/tools/ant/util/StringTokenizer;->includeDelims:Z

    if-eqz v0, :cond_1

    :cond_0
    const-string v0, ""

    :goto_0
    return-object v0

    :cond_1
    iget-object v0, p0, Lorg/apache/tools/ant/util/StringTokenizer;->intraString:Ljava/lang/String;

    goto :goto_0
.end method

.method public getToken(Ljava/io/Reader;)Ljava/lang/String;
    .locals 9
    .param p1    # Ljava/io/Reader;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    const/4 v8, -0x1

    const/4 v7, -0x2

    const/4 v1, -0x1

    iget v6, p0, Lorg/apache/tools/ant/util/StringTokenizer;->pushed:I

    if-eq v6, v7, :cond_0

    iget v1, p0, Lorg/apache/tools/ant/util/StringTokenizer;->pushed:I

    iput v7, p0, Lorg/apache/tools/ant/util/StringTokenizer;->pushed:I

    :goto_0
    if-ne v1, v8, :cond_1

    const/4 v6, 0x0

    :goto_1
    return-object v6

    :cond_0
    invoke-virtual {p1}, Ljava/io/Reader;->read()I

    move-result v1

    goto :goto_0

    :cond_1
    const/4 v2, 0x1

    const-string v6, ""

    iput-object v6, p0, Lorg/apache/tools/ant/util/StringTokenizer;->intraString:Ljava/lang/String;

    new-instance v5, Ljava/lang/StringBuffer;

    invoke-direct {v5}, Ljava/lang/StringBuffer;-><init>()V

    new-instance v4, Ljava/lang/StringBuffer;

    invoke-direct {v4}, Ljava/lang/StringBuffer;-><init>()V

    :goto_2
    if-eq v1, v8, :cond_2

    int-to-char v0, v1

    invoke-direct {p0, v0}, Lorg/apache/tools/ant/util/StringTokenizer;->isDelim(C)Z

    move-result v3

    if-eqz v2, :cond_7

    if-eqz v3, :cond_6

    iget-boolean v6, p0, Lorg/apache/tools/ant/util/StringTokenizer;->delimsAreTokens:Z

    if-eqz v6, :cond_5

    invoke-virtual {v5}, Ljava/lang/StringBuffer;->length()I

    move-result v6

    if-nez v6, :cond_4

    invoke-virtual {v5, v0}, Ljava/lang/StringBuffer;->append(C)Ljava/lang/StringBuffer;

    :cond_2
    :goto_3
    invoke-virtual {v4}, Ljava/lang/StringBuffer;->toString()Ljava/lang/String;

    move-result-object v6

    iput-object v6, p0, Lorg/apache/tools/ant/util/StringTokenizer;->intraString:Ljava/lang/String;

    iget-boolean v6, p0, Lorg/apache/tools/ant/util/StringTokenizer;->includeDelims:Z

    if-eqz v6, :cond_3

    iget-object v6, p0, Lorg/apache/tools/ant/util/StringTokenizer;->intraString:Ljava/lang/String;

    invoke-virtual {v5, v6}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    :cond_3
    invoke-virtual {v5}, Ljava/lang/StringBuffer;->toString()Ljava/lang/String;

    move-result-object v6

    goto :goto_1

    :cond_4
    iput v1, p0, Lorg/apache/tools/ant/util/StringTokenizer;->pushed:I

    goto :goto_3

    :cond_5
    invoke-virtual {v4, v0}, Ljava/lang/StringBuffer;->append(C)Ljava/lang/StringBuffer;

    const/4 v2, 0x0

    :goto_4
    invoke-virtual {p1}, Ljava/io/Reader;->read()I

    move-result v1

    goto :goto_2

    :cond_6
    invoke-virtual {v5, v0}, Ljava/lang/StringBuffer;->append(C)Ljava/lang/StringBuffer;

    goto :goto_4

    :cond_7
    if-eqz v3, :cond_8

    invoke-virtual {v4, v0}, Ljava/lang/StringBuffer;->append(C)Ljava/lang/StringBuffer;

    goto :goto_4

    :cond_8
    iput v1, p0, Lorg/apache/tools/ant/util/StringTokenizer;->pushed:I

    goto :goto_3
.end method

.method public setDelims(Ljava/lang/String;)V
    .locals 1
    .param p1    # Ljava/lang/String;

    invoke-static {p1}, Lorg/apache/tools/ant/util/StringUtils;->resolveBackSlash(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/String;->toCharArray()[C

    move-result-object v0

    iput-object v0, p0, Lorg/apache/tools/ant/util/StringTokenizer;->delims:[C

    return-void
.end method

.method public setDelimsAreTokens(Z)V
    .locals 0
    .param p1    # Z

    iput-boolean p1, p0, Lorg/apache/tools/ant/util/StringTokenizer;->delimsAreTokens:Z

    return-void
.end method

.method public setIncludeDelims(Z)V
    .locals 0
    .param p1    # Z

    iput-boolean p1, p0, Lorg/apache/tools/ant/util/StringTokenizer;->includeDelims:Z

    return-void
.end method

.method public setSuppressDelims(Z)V
    .locals 0
    .param p1    # Z

    iput-boolean p1, p0, Lorg/apache/tools/ant/util/StringTokenizer;->suppressDelims:Z

    return-void
.end method
