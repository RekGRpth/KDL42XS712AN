.class public final Lorg/apache/tools/ant/util/JavaEnvUtils;
.super Ljava/lang/Object;
.source "JavaEnvUtils.java"


# static fields
.field private static final FILE_UTILS:Lorg/apache/tools/ant/util/FileUtils;

.field private static final IS_AIX:Z

.field private static final IS_DOS:Z

.field private static final IS_NETWARE:Z

.field public static final JAVA_1_0:Ljava/lang/String; = "1.0"

.field public static final JAVA_1_1:Ljava/lang/String; = "1.1"

.field public static final JAVA_1_2:Ljava/lang/String; = "1.2"

.field public static final JAVA_1_3:Ljava/lang/String; = "1.3"

.field public static final JAVA_1_4:Ljava/lang/String; = "1.4"

.field public static final JAVA_1_5:Ljava/lang/String; = "1.5"

.field public static final JAVA_1_6:Ljava/lang/String; = "1.6"

.field private static final JAVA_HOME:Ljava/lang/String;

.field private static javaVersion:Ljava/lang/String;

.field private static javaVersionNumber:I

.field private static jrePackages:Ljava/util/Vector;

.field private static kaffeDetected:Z


# direct methods
.method static constructor <clinit>()V
    .locals 1

    const-string v0, "dos"

    invoke-static {v0}, Lorg/apache/tools/ant/taskdefs/condition/Os;->isFamily(Ljava/lang/String;)Z

    move-result v0

    sput-boolean v0, Lorg/apache/tools/ant/util/JavaEnvUtils;->IS_DOS:Z

    const-string v0, "netware"

    invoke-static {v0}, Lorg/apache/tools/ant/taskdefs/condition/Os;->isName(Ljava/lang/String;)Z

    move-result v0

    sput-boolean v0, Lorg/apache/tools/ant/util/JavaEnvUtils;->IS_NETWARE:Z

    const-string v0, "aix"

    invoke-static {v0}, Lorg/apache/tools/ant/taskdefs/condition/Os;->isName(Ljava/lang/String;)Z

    move-result v0

    sput-boolean v0, Lorg/apache/tools/ant/util/JavaEnvUtils;->IS_AIX:Z

    const-string v0, "java.home"

    invoke-static {v0}, Ljava/lang/System;->getProperty(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    sput-object v0, Lorg/apache/tools/ant/util/JavaEnvUtils;->JAVA_HOME:Ljava/lang/String;

    invoke-static {}, Lorg/apache/tools/ant/util/FileUtils;->getFileUtils()Lorg/apache/tools/ant/util/FileUtils;

    move-result-object v0

    sput-object v0, Lorg/apache/tools/ant/util/JavaEnvUtils;->FILE_UTILS:Lorg/apache/tools/ant/util/FileUtils;

    :try_start_0
    const-string v0, "1.0"

    sput-object v0, Lorg/apache/tools/ant/util/JavaEnvUtils;->javaVersion:Ljava/lang/String;

    const/16 v0, 0xa

    sput v0, Lorg/apache/tools/ant/util/JavaEnvUtils;->javaVersionNumber:I

    const-string v0, "java.lang.Void"

    invoke-static {v0}, Ljava/lang/Class;->forName(Ljava/lang/String;)Ljava/lang/Class;

    const-string v0, "1.1"

    sput-object v0, Lorg/apache/tools/ant/util/JavaEnvUtils;->javaVersion:Ljava/lang/String;

    sget v0, Lorg/apache/tools/ant/util/JavaEnvUtils;->javaVersionNumber:I

    add-int/lit8 v0, v0, 0x1

    sput v0, Lorg/apache/tools/ant/util/JavaEnvUtils;->javaVersionNumber:I

    const-string v0, "java.lang.ThreadLocal"

    invoke-static {v0}, Ljava/lang/Class;->forName(Ljava/lang/String;)Ljava/lang/Class;

    const-string v0, "1.2"

    sput-object v0, Lorg/apache/tools/ant/util/JavaEnvUtils;->javaVersion:Ljava/lang/String;

    sget v0, Lorg/apache/tools/ant/util/JavaEnvUtils;->javaVersionNumber:I

    add-int/lit8 v0, v0, 0x1

    sput v0, Lorg/apache/tools/ant/util/JavaEnvUtils;->javaVersionNumber:I

    const-string v0, "java.lang.StrictMath"

    invoke-static {v0}, Ljava/lang/Class;->forName(Ljava/lang/String;)Ljava/lang/Class;

    const-string v0, "1.3"

    sput-object v0, Lorg/apache/tools/ant/util/JavaEnvUtils;->javaVersion:Ljava/lang/String;

    sget v0, Lorg/apache/tools/ant/util/JavaEnvUtils;->javaVersionNumber:I

    add-int/lit8 v0, v0, 0x1

    sput v0, Lorg/apache/tools/ant/util/JavaEnvUtils;->javaVersionNumber:I

    const-string v0, "java.lang.CharSequence"

    invoke-static {v0}, Ljava/lang/Class;->forName(Ljava/lang/String;)Ljava/lang/Class;

    const-string v0, "1.4"

    sput-object v0, Lorg/apache/tools/ant/util/JavaEnvUtils;->javaVersion:Ljava/lang/String;

    sget v0, Lorg/apache/tools/ant/util/JavaEnvUtils;->javaVersionNumber:I

    add-int/lit8 v0, v0, 0x1

    sput v0, Lorg/apache/tools/ant/util/JavaEnvUtils;->javaVersionNumber:I

    const-string v0, "java.net.Proxy"

    invoke-static {v0}, Ljava/lang/Class;->forName(Ljava/lang/String;)Ljava/lang/Class;

    const-string v0, "1.5"

    sput-object v0, Lorg/apache/tools/ant/util/JavaEnvUtils;->javaVersion:Ljava/lang/String;

    sget v0, Lorg/apache/tools/ant/util/JavaEnvUtils;->javaVersionNumber:I

    add-int/lit8 v0, v0, 0x1

    sput v0, Lorg/apache/tools/ant/util/JavaEnvUtils;->javaVersionNumber:I

    const-string v0, "java.util.ServiceLoader"

    invoke-static {v0}, Ljava/lang/Class;->forName(Ljava/lang/String;)Ljava/lang/Class;

    const-string v0, "1.6"

    sput-object v0, Lorg/apache/tools/ant/util/JavaEnvUtils;->javaVersion:Ljava/lang/String;

    sget v0, Lorg/apache/tools/ant/util/JavaEnvUtils;->javaVersionNumber:I

    add-int/lit8 v0, v0, 0x1

    sput v0, Lorg/apache/tools/ant/util/JavaEnvUtils;->javaVersionNumber:I
    :try_end_0
    .catch Ljava/lang/Throwable; {:try_start_0 .. :try_end_0} :catch_1

    :goto_0
    const/4 v0, 0x0

    sput-boolean v0, Lorg/apache/tools/ant/util/JavaEnvUtils;->kaffeDetected:Z

    :try_start_1
    const-string v0, "kaffe.util.NotImplemented"

    invoke-static {v0}, Ljava/lang/Class;->forName(Ljava/lang/String;)Ljava/lang/Class;

    const/4 v0, 0x1

    sput-boolean v0, Lorg/apache/tools/ant/util/JavaEnvUtils;->kaffeDetected:Z
    :try_end_1
    .catch Ljava/lang/Throwable; {:try_start_1 .. :try_end_1} :catch_0

    :goto_1
    return-void

    :catch_0
    move-exception v0

    goto :goto_1

    :catch_1
    move-exception v0

    goto :goto_0
.end method

.method private constructor <init>()V
    .locals 0

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method private static addExtension(Ljava/lang/String;)Ljava/lang/String;
    .locals 2
    .param p0    # Ljava/lang/String;

    new-instance v0, Ljava/lang/StringBuffer;

    invoke-direct {v0}, Ljava/lang/StringBuffer;-><init>()V

    invoke-virtual {v0, p0}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    move-result-object v1

    sget-boolean v0, Lorg/apache/tools/ant/util/JavaEnvUtils;->IS_DOS:Z

    if-eqz v0, :cond_0

    const-string v0, ".exe"

    :goto_0
    invoke-virtual {v1, v0}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuffer;->toString()Ljava/lang/String;

    move-result-object v0

    return-object v0

    :cond_0
    const-string v0, ""

    goto :goto_0
.end method

.method private static buildJrePackages()V
    .locals 2

    new-instance v0, Ljava/util/Vector;

    invoke-direct {v0}, Ljava/util/Vector;-><init>()V

    sput-object v0, Lorg/apache/tools/ant/util/JavaEnvUtils;->jrePackages:Ljava/util/Vector;

    sget v0, Lorg/apache/tools/ant/util/JavaEnvUtils;->javaVersionNumber:I

    packed-switch v0, :pswitch_data_0

    :goto_0
    sget-object v0, Lorg/apache/tools/ant/util/JavaEnvUtils;->jrePackages:Ljava/util/Vector;

    const-string v1, "sun"

    invoke-virtual {v0, v1}, Ljava/util/Vector;->addElement(Ljava/lang/Object;)V

    sget-object v0, Lorg/apache/tools/ant/util/JavaEnvUtils;->jrePackages:Ljava/util/Vector;

    const-string v1, "java"

    invoke-virtual {v0, v1}, Ljava/util/Vector;->addElement(Ljava/lang/Object;)V

    sget-object v0, Lorg/apache/tools/ant/util/JavaEnvUtils;->jrePackages:Ljava/util/Vector;

    const-string v1, "javax"

    invoke-virtual {v0, v1}, Ljava/util/Vector;->addElement(Ljava/lang/Object;)V

    return-void

    :pswitch_0
    sget-object v0, Lorg/apache/tools/ant/util/JavaEnvUtils;->jrePackages:Ljava/util/Vector;

    const-string v1, "com.sun.org.apache"

    invoke-virtual {v0, v1}, Ljava/util/Vector;->addElement(Ljava/lang/Object;)V

    :pswitch_1
    sget v0, Lorg/apache/tools/ant/util/JavaEnvUtils;->javaVersionNumber:I

    const/16 v1, 0xe

    if-ne v0, v1, :cond_0

    sget-object v0, Lorg/apache/tools/ant/util/JavaEnvUtils;->jrePackages:Ljava/util/Vector;

    const-string v1, "org.apache.crimson"

    invoke-virtual {v0, v1}, Ljava/util/Vector;->addElement(Ljava/lang/Object;)V

    sget-object v0, Lorg/apache/tools/ant/util/JavaEnvUtils;->jrePackages:Ljava/util/Vector;

    const-string v1, "org.apache.xalan"

    invoke-virtual {v0, v1}, Ljava/util/Vector;->addElement(Ljava/lang/Object;)V

    sget-object v0, Lorg/apache/tools/ant/util/JavaEnvUtils;->jrePackages:Ljava/util/Vector;

    const-string v1, "org.apache.xml"

    invoke-virtual {v0, v1}, Ljava/util/Vector;->addElement(Ljava/lang/Object;)V

    sget-object v0, Lorg/apache/tools/ant/util/JavaEnvUtils;->jrePackages:Ljava/util/Vector;

    const-string v1, "org.apache.xpath"

    invoke-virtual {v0, v1}, Ljava/util/Vector;->addElement(Ljava/lang/Object;)V

    :cond_0
    sget-object v0, Lorg/apache/tools/ant/util/JavaEnvUtils;->jrePackages:Ljava/util/Vector;

    const-string v1, "org.ietf.jgss"

    invoke-virtual {v0, v1}, Ljava/util/Vector;->addElement(Ljava/lang/Object;)V

    sget-object v0, Lorg/apache/tools/ant/util/JavaEnvUtils;->jrePackages:Ljava/util/Vector;

    const-string v1, "org.w3c.dom"

    invoke-virtual {v0, v1}, Ljava/util/Vector;->addElement(Ljava/lang/Object;)V

    sget-object v0, Lorg/apache/tools/ant/util/JavaEnvUtils;->jrePackages:Ljava/util/Vector;

    const-string v1, "org.xml.sax"

    invoke-virtual {v0, v1}, Ljava/util/Vector;->addElement(Ljava/lang/Object;)V

    :pswitch_2
    sget-object v0, Lorg/apache/tools/ant/util/JavaEnvUtils;->jrePackages:Ljava/util/Vector;

    const-string v1, "org.omg"

    invoke-virtual {v0, v1}, Ljava/util/Vector;->addElement(Ljava/lang/Object;)V

    sget-object v0, Lorg/apache/tools/ant/util/JavaEnvUtils;->jrePackages:Ljava/util/Vector;

    const-string v1, "com.sun.corba"

    invoke-virtual {v0, v1}, Ljava/util/Vector;->addElement(Ljava/lang/Object;)V

    sget-object v0, Lorg/apache/tools/ant/util/JavaEnvUtils;->jrePackages:Ljava/util/Vector;

    const-string v1, "com.sun.jndi"

    invoke-virtual {v0, v1}, Ljava/util/Vector;->addElement(Ljava/lang/Object;)V

    sget-object v0, Lorg/apache/tools/ant/util/JavaEnvUtils;->jrePackages:Ljava/util/Vector;

    const-string v1, "com.sun.media"

    invoke-virtual {v0, v1}, Ljava/util/Vector;->addElement(Ljava/lang/Object;)V

    sget-object v0, Lorg/apache/tools/ant/util/JavaEnvUtils;->jrePackages:Ljava/util/Vector;

    const-string v1, "com.sun.naming"

    invoke-virtual {v0, v1}, Ljava/util/Vector;->addElement(Ljava/lang/Object;)V

    sget-object v0, Lorg/apache/tools/ant/util/JavaEnvUtils;->jrePackages:Ljava/util/Vector;

    const-string v1, "com.sun.org.omg"

    invoke-virtual {v0, v1}, Ljava/util/Vector;->addElement(Ljava/lang/Object;)V

    sget-object v0, Lorg/apache/tools/ant/util/JavaEnvUtils;->jrePackages:Ljava/util/Vector;

    const-string v1, "com.sun.rmi"

    invoke-virtual {v0, v1}, Ljava/util/Vector;->addElement(Ljava/lang/Object;)V

    sget-object v0, Lorg/apache/tools/ant/util/JavaEnvUtils;->jrePackages:Ljava/util/Vector;

    const-string v1, "sunw.io"

    invoke-virtual {v0, v1}, Ljava/util/Vector;->addElement(Ljava/lang/Object;)V

    sget-object v0, Lorg/apache/tools/ant/util/JavaEnvUtils;->jrePackages:Ljava/util/Vector;

    const-string v1, "sunw.util"

    invoke-virtual {v0, v1}, Ljava/util/Vector;->addElement(Ljava/lang/Object;)V

    :pswitch_3
    sget-object v0, Lorg/apache/tools/ant/util/JavaEnvUtils;->jrePackages:Ljava/util/Vector;

    const-string v1, "com.sun.java"

    invoke-virtual {v0, v1}, Ljava/util/Vector;->addElement(Ljava/lang/Object;)V

    sget-object v0, Lorg/apache/tools/ant/util/JavaEnvUtils;->jrePackages:Ljava/util/Vector;

    const-string v1, "com.sun.image"

    invoke-virtual {v0, v1}, Ljava/util/Vector;->addElement(Ljava/lang/Object;)V

    goto/16 :goto_0

    nop

    :pswitch_data_0
    .packed-switch 0xc
        :pswitch_3
        :pswitch_2
        :pswitch_1
        :pswitch_0
        :pswitch_0
    .end packed-switch
.end method

.method public static createVmsJavaOptionFile([Ljava/lang/String;)Ljava/io/File;
    .locals 8
    .param p0    # [Ljava/lang/String;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    sget-object v4, Lorg/apache/tools/ant/util/JavaEnvUtils;->FILE_UTILS:Lorg/apache/tools/ant/util/FileUtils;

    const-string v5, "ANT"

    const-string v6, ".JAVA_OPTS"

    const/4 v7, 0x0

    invoke-virtual {v4, v5, v6, v7}, Lorg/apache/tools/ant/util/FileUtils;->createTempFile(Ljava/lang/String;Ljava/lang/String;Ljava/io/File;)Ljava/io/File;

    move-result-object v3

    const/4 v1, 0x0

    :try_start_0
    new-instance v2, Ljava/io/PrintWriter;

    new-instance v4, Ljava/io/BufferedWriter;

    new-instance v5, Ljava/io/FileWriter;

    invoke-direct {v5, v3}, Ljava/io/FileWriter;-><init>(Ljava/io/File;)V

    invoke-direct {v4, v5}, Ljava/io/BufferedWriter;-><init>(Ljava/io/Writer;)V

    invoke-direct {v2, v4}, Ljava/io/PrintWriter;-><init>(Ljava/io/Writer;)V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    const/4 v0, 0x0

    :goto_0
    :try_start_1
    array-length v4, p0

    if-ge v0, v4, :cond_0

    aget-object v4, p0, v0

    invoke-virtual {v2, v4}, Ljava/io/PrintWriter;->println(Ljava/lang/String;)V
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_1

    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    :cond_0
    invoke-static {v2}, Lorg/apache/tools/ant/util/FileUtils;->close(Ljava/io/Writer;)V

    return-object v3

    :catchall_0
    move-exception v4

    :goto_1
    invoke-static {v1}, Lorg/apache/tools/ant/util/FileUtils;->close(Ljava/io/Writer;)V

    throw v4

    :catchall_1
    move-exception v4

    move-object v1, v2

    goto :goto_1
.end method

.method private static findInDir(Ljava/lang/String;Ljava/lang/String;)Ljava/io/File;
    .locals 3
    .param p0    # Ljava/lang/String;
    .param p1    # Ljava/lang/String;

    sget-object v2, Lorg/apache/tools/ant/util/JavaEnvUtils;->FILE_UTILS:Lorg/apache/tools/ant/util/FileUtils;

    invoke-virtual {v2, p0}, Lorg/apache/tools/ant/util/FileUtils;->normalize(Ljava/lang/String;)Ljava/io/File;

    move-result-object v0

    const/4 v1, 0x0

    invoke-virtual {v0}, Ljava/io/File;->exists()Z

    move-result v2

    if-eqz v2, :cond_0

    new-instance v1, Ljava/io/File;

    invoke-static {p1}, Lorg/apache/tools/ant/util/JavaEnvUtils;->addExtension(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v2

    invoke-direct {v1, v0, v2}, Ljava/io/File;-><init>(Ljava/io/File;Ljava/lang/String;)V

    invoke-virtual {v1}, Ljava/io/File;->exists()Z

    move-result v2

    if-nez v2, :cond_0

    const/4 v1, 0x0

    :cond_0
    return-object v1
.end method

.method public static getJavaHome()Ljava/lang/String;
    .locals 1

    sget-object v0, Lorg/apache/tools/ant/util/JavaEnvUtils;->JAVA_HOME:Ljava/lang/String;

    return-object v0
.end method

.method public static getJavaVersion()Ljava/lang/String;
    .locals 1

    sget-object v0, Lorg/apache/tools/ant/util/JavaEnvUtils;->javaVersion:Ljava/lang/String;

    return-object v0
.end method

.method public static getJavaVersionNumber()I
    .locals 1

    sget v0, Lorg/apache/tools/ant/util/JavaEnvUtils;->javaVersionNumber:I

    return v0
.end method

.method public static getJdkExecutable(Ljava/lang/String;)Ljava/lang/String;
    .locals 3
    .param p0    # Ljava/lang/String;

    sget-boolean v1, Lorg/apache/tools/ant/util/JavaEnvUtils;->IS_NETWARE:Z

    if-eqz v1, :cond_0

    :goto_0
    return-object p0

    :cond_0
    const/4 v0, 0x0

    sget-boolean v1, Lorg/apache/tools/ant/util/JavaEnvUtils;->IS_AIX:Z

    if-eqz v1, :cond_1

    new-instance v1, Ljava/lang/StringBuffer;

    invoke-direct {v1}, Ljava/lang/StringBuffer;-><init>()V

    sget-object v2, Lorg/apache/tools/ant/util/JavaEnvUtils;->JAVA_HOME:Ljava/lang/String;

    invoke-virtual {v1, v2}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    move-result-object v1

    const-string v2, "/../sh"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuffer;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v1, p0}, Lorg/apache/tools/ant/util/JavaEnvUtils;->findInDir(Ljava/lang/String;Ljava/lang/String;)Ljava/io/File;

    move-result-object v0

    :cond_1
    if-nez v0, :cond_2

    new-instance v1, Ljava/lang/StringBuffer;

    invoke-direct {v1}, Ljava/lang/StringBuffer;-><init>()V

    sget-object v2, Lorg/apache/tools/ant/util/JavaEnvUtils;->JAVA_HOME:Ljava/lang/String;

    invoke-virtual {v1, v2}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    move-result-object v1

    const-string v2, "/../bin"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuffer;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v1, p0}, Lorg/apache/tools/ant/util/JavaEnvUtils;->findInDir(Ljava/lang/String;Ljava/lang/String;)Ljava/io/File;

    move-result-object v0

    :cond_2
    if-eqz v0, :cond_3

    invoke-virtual {v0}, Ljava/io/File;->getAbsolutePath()Ljava/lang/String;

    move-result-object p0

    goto :goto_0

    :cond_3
    invoke-static {p0}, Lorg/apache/tools/ant/util/JavaEnvUtils;->getJreExecutable(Ljava/lang/String;)Ljava/lang/String;

    move-result-object p0

    goto :goto_0
.end method

.method public static getJreExecutable(Ljava/lang/String;)Ljava/lang/String;
    .locals 3
    .param p0    # Ljava/lang/String;

    sget-boolean v1, Lorg/apache/tools/ant/util/JavaEnvUtils;->IS_NETWARE:Z

    if-eqz v1, :cond_0

    :goto_0
    return-object p0

    :cond_0
    const/4 v0, 0x0

    sget-boolean v1, Lorg/apache/tools/ant/util/JavaEnvUtils;->IS_AIX:Z

    if-eqz v1, :cond_1

    new-instance v1, Ljava/lang/StringBuffer;

    invoke-direct {v1}, Ljava/lang/StringBuffer;-><init>()V

    sget-object v2, Lorg/apache/tools/ant/util/JavaEnvUtils;->JAVA_HOME:Ljava/lang/String;

    invoke-virtual {v1, v2}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    move-result-object v1

    const-string v2, "/sh"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuffer;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v1, p0}, Lorg/apache/tools/ant/util/JavaEnvUtils;->findInDir(Ljava/lang/String;Ljava/lang/String;)Ljava/io/File;

    move-result-object v0

    :cond_1
    if-nez v0, :cond_2

    new-instance v1, Ljava/lang/StringBuffer;

    invoke-direct {v1}, Ljava/lang/StringBuffer;-><init>()V

    sget-object v2, Lorg/apache/tools/ant/util/JavaEnvUtils;->JAVA_HOME:Ljava/lang/String;

    invoke-virtual {v1, v2}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    move-result-object v1

    const-string v2, "/bin"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuffer;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v1, p0}, Lorg/apache/tools/ant/util/JavaEnvUtils;->findInDir(Ljava/lang/String;Ljava/lang/String;)Ljava/io/File;

    move-result-object v0

    :cond_2
    if-eqz v0, :cond_3

    invoke-virtual {v0}, Ljava/io/File;->getAbsolutePath()Ljava/lang/String;

    move-result-object p0

    goto :goto_0

    :cond_3
    invoke-static {p0}, Lorg/apache/tools/ant/util/JavaEnvUtils;->addExtension(Ljava/lang/String;)Ljava/lang/String;

    move-result-object p0

    goto :goto_0
.end method

.method public static getJrePackageTestCases()Ljava/util/Vector;
    .locals 3

    new-instance v0, Ljava/util/Vector;

    invoke-direct {v0}, Ljava/util/Vector;-><init>()V

    const-string v1, "java.lang.Object"

    invoke-virtual {v0, v1}, Ljava/util/Vector;->addElement(Ljava/lang/Object;)V

    sget v1, Lorg/apache/tools/ant/util/JavaEnvUtils;->javaVersionNumber:I

    packed-switch v1, :pswitch_data_0

    :goto_0
    const-string v1, "sun.reflect.SerializationConstructorAccessorImpl"

    invoke-virtual {v0, v1}, Ljava/util/Vector;->addElement(Ljava/lang/Object;)V

    const-string v1, "sun.net.www.http.HttpClient"

    invoke-virtual {v0, v1}, Ljava/util/Vector;->addElement(Ljava/lang/Object;)V

    const-string v1, "sun.audio.AudioPlayer"

    invoke-virtual {v0, v1}, Ljava/util/Vector;->addElement(Ljava/lang/Object;)V

    return-object v0

    :pswitch_0
    const-string v1, "com.sun.org.apache.xerces.internal.jaxp.datatype.DatatypeFactoryImpl "

    invoke-virtual {v0, v1}, Ljava/util/Vector;->addElement(Ljava/lang/Object;)V

    :pswitch_1
    const-string v1, "sun.audio.AudioPlayer"

    invoke-virtual {v0, v1}, Ljava/util/Vector;->addElement(Ljava/lang/Object;)V

    sget v1, Lorg/apache/tools/ant/util/JavaEnvUtils;->javaVersionNumber:I

    const/16 v2, 0xe

    if-ne v1, v2, :cond_0

    const-string v1, "org.apache.crimson.parser.ContentModel"

    invoke-virtual {v0, v1}, Ljava/util/Vector;->addElement(Ljava/lang/Object;)V

    const-string v1, "org.apache.xalan.processor.ProcessorImport"

    invoke-virtual {v0, v1}, Ljava/util/Vector;->addElement(Ljava/lang/Object;)V

    const-string v1, "org.apache.xml.utils.URI"

    invoke-virtual {v0, v1}, Ljava/util/Vector;->addElement(Ljava/lang/Object;)V

    const-string v1, "org.apache.xpath.XPathFactory"

    invoke-virtual {v0, v1}, Ljava/util/Vector;->addElement(Ljava/lang/Object;)V

    :cond_0
    const-string v1, "org.ietf.jgss.Oid"

    invoke-virtual {v0, v1}, Ljava/util/Vector;->addElement(Ljava/lang/Object;)V

    const-string v1, "org.w3c.dom.Attr"

    invoke-virtual {v0, v1}, Ljava/util/Vector;->addElement(Ljava/lang/Object;)V

    const-string v1, "org.xml.sax.XMLReader"

    invoke-virtual {v0, v1}, Ljava/util/Vector;->addElement(Ljava/lang/Object;)V

    :pswitch_2
    const-string v1, "org.omg.CORBA.Any"

    invoke-virtual {v0, v1}, Ljava/util/Vector;->addElement(Ljava/lang/Object;)V

    const-string v1, "com.sun.corba.se.internal.corba.AnyImpl"

    invoke-virtual {v0, v1}, Ljava/util/Vector;->addElement(Ljava/lang/Object;)V

    const-string v1, "com.sun.jndi.ldap.LdapURL"

    invoke-virtual {v0, v1}, Ljava/util/Vector;->addElement(Ljava/lang/Object;)V

    const-string v1, "com.sun.media.sound.Printer"

    invoke-virtual {v0, v1}, Ljava/util/Vector;->addElement(Ljava/lang/Object;)V

    const-string v1, "com.sun.naming.internal.VersionHelper"

    invoke-virtual {v0, v1}, Ljava/util/Vector;->addElement(Ljava/lang/Object;)V

    const-string v1, "com.sun.org.omg.CORBA.Initializer"

    invoke-virtual {v0, v1}, Ljava/util/Vector;->addElement(Ljava/lang/Object;)V

    const-string v1, "sunw.io.Serializable"

    invoke-virtual {v0, v1}, Ljava/util/Vector;->addElement(Ljava/lang/Object;)V

    const-string v1, "sunw.util.EventListener"

    invoke-virtual {v0, v1}, Ljava/util/Vector;->addElement(Ljava/lang/Object;)V

    :pswitch_3
    const-string v1, "javax.accessibility.Accessible"

    invoke-virtual {v0, v1}, Ljava/util/Vector;->addElement(Ljava/lang/Object;)V

    const-string v1, "sun.misc.BASE64Encoder"

    invoke-virtual {v0, v1}, Ljava/util/Vector;->addElement(Ljava/lang/Object;)V

    const-string v1, "com.sun.image.codec.jpeg.JPEGCodec"

    invoke-virtual {v0, v1}, Ljava/util/Vector;->addElement(Ljava/lang/Object;)V

    goto :goto_0

    :pswitch_data_0
    .packed-switch 0xc
        :pswitch_3
        :pswitch_2
        :pswitch_1
        :pswitch_0
        :pswitch_0
    .end packed-switch
.end method

.method public static getJrePackages()Ljava/util/Vector;
    .locals 1

    sget-object v0, Lorg/apache/tools/ant/util/JavaEnvUtils;->jrePackages:Ljava/util/Vector;

    if-nez v0, :cond_0

    invoke-static {}, Lorg/apache/tools/ant/util/JavaEnvUtils;->buildJrePackages()V

    :cond_0
    sget-object v0, Lorg/apache/tools/ant/util/JavaEnvUtils;->jrePackages:Ljava/util/Vector;

    return-object v0
.end method

.method public static isAtLeastJavaVersion(Ljava/lang/String;)Z
    .locals 1
    .param p0    # Ljava/lang/String;

    sget-object v0, Lorg/apache/tools/ant/util/JavaEnvUtils;->javaVersion:Ljava/lang/String;

    invoke-virtual {v0, p0}, Ljava/lang/String;->compareTo(Ljava/lang/String;)I

    move-result v0

    if-ltz v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public static isJavaVersion(Ljava/lang/String;)Z
    .locals 1
    .param p0    # Ljava/lang/String;

    sget-object v0, Lorg/apache/tools/ant/util/JavaEnvUtils;->javaVersion:Ljava/lang/String;

    invoke-virtual {v0, p0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    return v0
.end method

.method public static isKaffe()Z
    .locals 1

    sget-boolean v0, Lorg/apache/tools/ant/util/JavaEnvUtils;->kaffeDetected:Z

    return v0
.end method
