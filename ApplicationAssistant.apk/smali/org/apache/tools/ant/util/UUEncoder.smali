.class public Lorg/apache/tools/ant/util/UUEncoder;
.super Ljava/lang/Object;
.source "UUEncoder.java"


# static fields
.field protected static final DEFAULT_MODE:I = 0x284

.field private static final MAX_CHARS_PER_LINE:I = 0x2d


# instance fields
.field private name:Ljava/lang/String;

.field private out:Ljava/io/OutputStream;


# direct methods
.method public constructor <init>(Ljava/lang/String;)V
    .locals 0
    .param p1    # Ljava/lang/String;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    iput-object p1, p0, Lorg/apache/tools/ant/util/UUEncoder;->name:Ljava/lang/String;

    return-void
.end method

.method private encodeBegin()V
    .locals 2
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    new-instance v0, Ljava/lang/StringBuffer;

    invoke-direct {v0}, Ljava/lang/StringBuffer;-><init>()V

    const-string v1, "begin 644 "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    move-result-object v0

    iget-object v1, p0, Lorg/apache/tools/ant/util/UUEncoder;->name:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    move-result-object v0

    const-string v1, "\n"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuffer;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-direct {p0, v0}, Lorg/apache/tools/ant/util/UUEncoder;->encodeString(Ljava/lang/String;)V

    return-void
.end method

.method private encodeEnd()V
    .locals 1
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    const-string v0, " \nend\n"

    invoke-direct {p0, v0}, Lorg/apache/tools/ant/util/UUEncoder;->encodeString(Ljava/lang/String;)V

    return-void
.end method

.method private encodeLine([BIILjava/io/OutputStream;)V
    .locals 11
    .param p1    # [B
    .param p2    # I
    .param p3    # I
    .param p4    # Ljava/io/OutputStream;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    and-int/lit8 v9, p3, 0x3f

    add-int/lit8 v9, v9, 0x20

    int-to-byte v9, v9

    invoke-virtual {p4, v9}, Ljava/io/OutputStream;->write(I)V

    const/4 v7, 0x0

    move v8, v7

    :goto_0
    if-ge v8, p3, :cond_1

    const/4 v1, 0x1

    const/4 v2, 0x1

    add-int/lit8 v7, v8, 0x1

    add-int v9, p2, v8

    aget-byte v0, p1, v9

    if-ge v7, p3, :cond_0

    add-int/lit8 v8, v7, 0x1

    add-int v9, p2, v7

    aget-byte v1, p1, v9

    if-ge v8, p3, :cond_2

    add-int/lit8 v7, v8, 0x1

    add-int v9, p2, v8

    aget-byte v2, p1, v9

    :cond_0
    :goto_1
    ushr-int/lit8 v9, v0, 0x2

    and-int/lit8 v9, v9, 0x3f

    add-int/lit8 v9, v9, 0x20

    int-to-byte v3, v9

    shl-int/lit8 v9, v0, 0x4

    and-int/lit8 v9, v9, 0x30

    ushr-int/lit8 v10, v1, 0x4

    and-int/lit8 v10, v10, 0xf

    or-int/2addr v9, v10

    add-int/lit8 v9, v9, 0x20

    int-to-byte v4, v9

    shl-int/lit8 v9, v1, 0x2

    and-int/lit8 v9, v9, 0x3c

    ushr-int/lit8 v10, v2, 0x6

    and-int/lit8 v10, v10, 0x3

    or-int/2addr v9, v10

    add-int/lit8 v9, v9, 0x20

    int-to-byte v5, v9

    and-int/lit8 v9, v2, 0x3f

    add-int/lit8 v9, v9, 0x20

    int-to-byte v6, v9

    invoke-virtual {p4, v3}, Ljava/io/OutputStream;->write(I)V

    invoke-virtual {p4, v4}, Ljava/io/OutputStream;->write(I)V

    invoke-virtual {p4, v5}, Ljava/io/OutputStream;->write(I)V

    invoke-virtual {p4, v6}, Ljava/io/OutputStream;->write(I)V

    move v8, v7

    goto :goto_0

    :cond_1
    const/16 v9, 0xa

    invoke-virtual {p4, v9}, Ljava/io/OutputStream;->write(I)V

    return-void

    :cond_2
    move v7, v8

    goto :goto_1
.end method

.method private encodeString(Ljava/lang/String;)V
    .locals 2
    .param p1    # Ljava/lang/String;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    new-instance v0, Ljava/io/PrintStream;

    iget-object v1, p0, Lorg/apache/tools/ant/util/UUEncoder;->out:Ljava/io/OutputStream;

    invoke-direct {v0, v1}, Ljava/io/PrintStream;-><init>(Ljava/io/OutputStream;)V

    invoke-virtual {v0, p1}, Ljava/io/PrintStream;->print(Ljava/lang/String;)V

    invoke-virtual {v0}, Ljava/io/PrintStream;->flush()V

    return-void
.end method


# virtual methods
.method public encode(Ljava/io/InputStream;Ljava/io/OutputStream;)V
    .locals 7
    .param p1    # Ljava/io/InputStream;
    .param p2    # Ljava/io/OutputStream;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    const/16 v4, 0x2d

    iput-object p2, p0, Lorg/apache/tools/ant/util/UUEncoder;->out:Ljava/io/OutputStream;

    invoke-direct {p0}, Lorg/apache/tools/ant/util/UUEncoder;->encodeBegin()V

    const/16 v5, 0x1194

    new-array v0, v5, [B

    :cond_0
    const/4 v5, 0x0

    array-length v6, v0

    invoke-virtual {p1, v0, v5, v6}, Ljava/io/InputStream;->read([BII)I

    move-result v1

    const/4 v5, -0x1

    if-eq v1, v5, :cond_2

    const/4 v3, 0x0

    :goto_0
    if-lez v1, :cond_0

    if-le v1, v4, :cond_1

    move v2, v4

    :goto_1
    invoke-direct {p0, v0, v3, v2, p2}, Lorg/apache/tools/ant/util/UUEncoder;->encodeLine([BIILjava/io/OutputStream;)V

    add-int/2addr v3, v2

    sub-int/2addr v1, v2

    goto :goto_0

    :cond_1
    move v2, v1

    goto :goto_1

    :cond_2
    invoke-virtual {p2}, Ljava/io/OutputStream;->flush()V

    invoke-direct {p0}, Lorg/apache/tools/ant/util/UUEncoder;->encodeEnd()V

    return-void
.end method
