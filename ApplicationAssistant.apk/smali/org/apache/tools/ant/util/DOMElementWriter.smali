.class public Lorg/apache/tools/ant/util/DOMElementWriter;
.super Ljava/lang/Object;
.source "DOMElementWriter.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lorg/apache/tools/ant/util/DOMElementWriter$XmlNamespacePolicy;
    }
.end annotation


# static fields
.field private static final NS:Ljava/lang/String; = "ns"

.field private static lSep:Ljava/lang/String;


# instance fields
.field protected knownEntities:[Ljava/lang/String;

.field private namespacePolicy:Lorg/apache/tools/ant/util/DOMElementWriter$XmlNamespacePolicy;

.field private nextPrefix:I

.field private nsPrefixMap:Ljava/util/HashMap;

.field private nsURIByElement:Ljava/util/HashMap;

.field private xmlDeclaration:Z


# direct methods
.method static constructor <clinit>()V
    .locals 1

    const-string v0, "line.separator"

    invoke-static {v0}, Ljava/lang/System;->getProperty(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    sput-object v0, Lorg/apache/tools/ant/util/DOMElementWriter;->lSep:Ljava/lang/String;

    return-void
.end method

.method public constructor <init>()V
    .locals 4

    const/4 v3, 0x1

    const/4 v2, 0x0

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    iput-boolean v3, p0, Lorg/apache/tools/ant/util/DOMElementWriter;->xmlDeclaration:Z

    sget-object v0, Lorg/apache/tools/ant/util/DOMElementWriter$XmlNamespacePolicy;->IGNORE:Lorg/apache/tools/ant/util/DOMElementWriter$XmlNamespacePolicy;

    iput-object v0, p0, Lorg/apache/tools/ant/util/DOMElementWriter;->namespacePolicy:Lorg/apache/tools/ant/util/DOMElementWriter$XmlNamespacePolicy;

    new-instance v0, Ljava/util/HashMap;

    invoke-direct {v0}, Ljava/util/HashMap;-><init>()V

    iput-object v0, p0, Lorg/apache/tools/ant/util/DOMElementWriter;->nsPrefixMap:Ljava/util/HashMap;

    iput v2, p0, Lorg/apache/tools/ant/util/DOMElementWriter;->nextPrefix:I

    new-instance v0, Ljava/util/HashMap;

    invoke-direct {v0}, Ljava/util/HashMap;-><init>()V

    iput-object v0, p0, Lorg/apache/tools/ant/util/DOMElementWriter;->nsURIByElement:Ljava/util/HashMap;

    const/4 v0, 0x5

    new-array v0, v0, [Ljava/lang/String;

    const-string v1, "gt"

    aput-object v1, v0, v2

    const-string v1, "amp"

    aput-object v1, v0, v3

    const/4 v1, 0x2

    const-string v2, "lt"

    aput-object v2, v0, v1

    const/4 v1, 0x3

    const-string v2, "apos"

    aput-object v2, v0, v1

    const/4 v1, 0x4

    const-string v2, "quot"

    aput-object v2, v0, v1

    iput-object v0, p0, Lorg/apache/tools/ant/util/DOMElementWriter;->knownEntities:[Ljava/lang/String;

    return-void
.end method

.method public constructor <init>(Z)V
    .locals 1
    .param p1    # Z

    sget-object v0, Lorg/apache/tools/ant/util/DOMElementWriter$XmlNamespacePolicy;->IGNORE:Lorg/apache/tools/ant/util/DOMElementWriter$XmlNamespacePolicy;

    invoke-direct {p0, p1, v0}, Lorg/apache/tools/ant/util/DOMElementWriter;-><init>(ZLorg/apache/tools/ant/util/DOMElementWriter$XmlNamespacePolicy;)V

    return-void
.end method

.method public constructor <init>(ZLorg/apache/tools/ant/util/DOMElementWriter$XmlNamespacePolicy;)V
    .locals 4
    .param p1    # Z
    .param p2    # Lorg/apache/tools/ant/util/DOMElementWriter$XmlNamespacePolicy;

    const/4 v3, 0x1

    const/4 v2, 0x0

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    iput-boolean v3, p0, Lorg/apache/tools/ant/util/DOMElementWriter;->xmlDeclaration:Z

    sget-object v0, Lorg/apache/tools/ant/util/DOMElementWriter$XmlNamespacePolicy;->IGNORE:Lorg/apache/tools/ant/util/DOMElementWriter$XmlNamespacePolicy;

    iput-object v0, p0, Lorg/apache/tools/ant/util/DOMElementWriter;->namespacePolicy:Lorg/apache/tools/ant/util/DOMElementWriter$XmlNamespacePolicy;

    new-instance v0, Ljava/util/HashMap;

    invoke-direct {v0}, Ljava/util/HashMap;-><init>()V

    iput-object v0, p0, Lorg/apache/tools/ant/util/DOMElementWriter;->nsPrefixMap:Ljava/util/HashMap;

    iput v2, p0, Lorg/apache/tools/ant/util/DOMElementWriter;->nextPrefix:I

    new-instance v0, Ljava/util/HashMap;

    invoke-direct {v0}, Ljava/util/HashMap;-><init>()V

    iput-object v0, p0, Lorg/apache/tools/ant/util/DOMElementWriter;->nsURIByElement:Ljava/util/HashMap;

    const/4 v0, 0x5

    new-array v0, v0, [Ljava/lang/String;

    const-string v1, "gt"

    aput-object v1, v0, v2

    const-string v1, "amp"

    aput-object v1, v0, v3

    const/4 v1, 0x2

    const-string v2, "lt"

    aput-object v2, v0, v1

    const/4 v1, 0x3

    const-string v2, "apos"

    aput-object v2, v0, v1

    const/4 v1, 0x4

    const-string v2, "quot"

    aput-object v2, v0, v1

    iput-object v0, p0, Lorg/apache/tools/ant/util/DOMElementWriter;->knownEntities:[Ljava/lang/String;

    iput-boolean p1, p0, Lorg/apache/tools/ant/util/DOMElementWriter;->xmlDeclaration:Z

    iput-object p2, p0, Lorg/apache/tools/ant/util/DOMElementWriter;->namespacePolicy:Lorg/apache/tools/ant/util/DOMElementWriter$XmlNamespacePolicy;

    return-void
.end method

.method private addNSDefinition(Lorg/w3c/dom/Element;Ljava/lang/String;)V
    .locals 2
    .param p1    # Lorg/w3c/dom/Element;
    .param p2    # Ljava/lang/String;

    iget-object v1, p0, Lorg/apache/tools/ant/util/DOMElementWriter;->nsURIByElement:Ljava/util/HashMap;

    invoke-virtual {v1, p1}, Ljava/util/HashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/util/ArrayList;

    if-nez v0, :cond_0

    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iget-object v1, p0, Lorg/apache/tools/ant/util/DOMElementWriter;->nsURIByElement:Ljava/util/HashMap;

    invoke-virtual {v1, p1, v0}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    :cond_0
    invoke-virtual {v0, p2}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    return-void
.end method

.method private static getNamespaceURI(Lorg/w3c/dom/Node;)Ljava/lang/String;
    .locals 1
    .param p0    # Lorg/w3c/dom/Node;

    invoke-interface {p0}, Lorg/w3c/dom/Node;->getNamespaceURI()Ljava/lang/String;

    move-result-object v0

    if-nez v0, :cond_0

    const-string v0, ""

    :cond_0
    return-object v0
.end method

.method private removeNSDefinitions(Lorg/w3c/dom/Element;)V
    .locals 4
    .param p1    # Lorg/w3c/dom/Element;

    iget-object v2, p0, Lorg/apache/tools/ant/util/DOMElementWriter;->nsURIByElement:Ljava/util/HashMap;

    invoke-virtual {v2, p1}, Ljava/util/HashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/util/ArrayList;

    if-eqz v0, :cond_1

    invoke-virtual {v0}, Ljava/util/ArrayList;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :goto_0
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v2

    if-eqz v2, :cond_0

    iget-object v2, p0, Lorg/apache/tools/ant/util/DOMElementWriter;->nsPrefixMap:Ljava/util/HashMap;

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v3

    invoke-virtual {v2, v3}, Ljava/util/HashMap;->remove(Ljava/lang/Object;)Ljava/lang/Object;

    goto :goto_0

    :cond_0
    iget-object v2, p0, Lorg/apache/tools/ant/util/DOMElementWriter;->nsURIByElement:Ljava/util/HashMap;

    invoke-virtual {v2, p1}, Ljava/util/HashMap;->remove(Ljava/lang/Object;)Ljava/lang/Object;

    :cond_1
    return-void
.end method


# virtual methods
.method public closeElement(Lorg/w3c/dom/Element;Ljava/io/Writer;ILjava/lang/String;Z)V
    .locals 4
    .param p1    # Lorg/w3c/dom/Element;
    .param p2    # Ljava/io/Writer;
    .param p3    # I
    .param p4    # Ljava/lang/String;
    .param p5    # Z
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    if-eqz p5, :cond_0

    const/4 v0, 0x0

    :goto_0
    if-ge v0, p3, :cond_0

    invoke-virtual {p2, p4}, Ljava/io/Writer;->write(Ljava/lang/String;)V

    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    :cond_0
    const-string v3, "</"

    invoke-virtual {p2, v3}, Ljava/io/Writer;->write(Ljava/lang/String;)V

    iget-object v3, p0, Lorg/apache/tools/ant/util/DOMElementWriter;->namespacePolicy:Lorg/apache/tools/ant/util/DOMElementWriter$XmlNamespacePolicy;

    invoke-static {v3}, Lorg/apache/tools/ant/util/DOMElementWriter$XmlNamespacePolicy;->access$000(Lorg/apache/tools/ant/util/DOMElementWriter$XmlNamespacePolicy;)Z

    move-result v3

    if-eqz v3, :cond_2

    invoke-static {p1}, Lorg/apache/tools/ant/util/DOMElementWriter;->getNamespaceURI(Lorg/w3c/dom/Node;)Ljava/lang/String;

    move-result-object v2

    iget-object v3, p0, Lorg/apache/tools/ant/util/DOMElementWriter;->nsPrefixMap:Ljava/util/HashMap;

    invoke-virtual {v3, v2}, Ljava/util/HashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Ljava/lang/String;

    if-eqz v1, :cond_1

    const-string v3, ""

    invoke-virtual {v3, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-nez v3, :cond_1

    invoke-virtual {p2, v1}, Ljava/io/Writer;->write(Ljava/lang/String;)V

    const-string v3, ":"

    invoke-virtual {p2, v3}, Ljava/io/Writer;->write(Ljava/lang/String;)V

    :cond_1
    invoke-direct {p0, p1}, Lorg/apache/tools/ant/util/DOMElementWriter;->removeNSDefinitions(Lorg/w3c/dom/Element;)V

    :cond_2
    invoke-interface {p1}, Lorg/w3c/dom/Element;->getTagName()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {p2, v3}, Ljava/io/Writer;->write(Ljava/lang/String;)V

    const-string v3, ">"

    invoke-virtual {p2, v3}, Ljava/io/Writer;->write(Ljava/lang/String;)V

    sget-object v3, Lorg/apache/tools/ant/util/DOMElementWriter;->lSep:Ljava/lang/String;

    invoke-virtual {p2, v3}, Ljava/io/Writer;->write(Ljava/lang/String;)V

    invoke-virtual {p2}, Ljava/io/Writer;->flush()V

    return-void
.end method

.method public encode(Ljava/lang/String;)Ljava/lang/String;
    .locals 6
    .param p1    # Ljava/lang/String;

    new-instance v4, Ljava/lang/StringBuffer;

    invoke-direct {v4}, Ljava/lang/StringBuffer;-><init>()V

    invoke-virtual {p1}, Ljava/lang/String;->length()I

    move-result v2

    const/4 v1, 0x0

    :goto_0
    if-ge v1, v2, :cond_3

    invoke-virtual {p1, v1}, Ljava/lang/String;->charAt(I)C

    move-result v0

    sparse-switch v0, :sswitch_data_0

    invoke-virtual {p0, v0}, Lorg/apache/tools/ant/util/DOMElementWriter;->isLegalCharacter(C)Z

    move-result v5

    if-eqz v5, :cond_0

    invoke-virtual {v4, v0}, Ljava/lang/StringBuffer;->append(C)Ljava/lang/StringBuffer;

    :cond_0
    :goto_1
    add-int/lit8 v1, v1, 0x1

    goto :goto_0

    :sswitch_0
    const-string v5, "&lt;"

    invoke-virtual {v4, v5}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    goto :goto_1

    :sswitch_1
    const-string v5, "&gt;"

    invoke-virtual {v4, v5}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    goto :goto_1

    :sswitch_2
    const-string v5, "&apos;"

    invoke-virtual {v4, v5}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    goto :goto_1

    :sswitch_3
    const-string v5, "&quot;"

    invoke-virtual {v4, v5}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    goto :goto_1

    :sswitch_4
    const-string v5, ";"

    invoke-virtual {p1, v5, v1}, Ljava/lang/String;->indexOf(Ljava/lang/String;I)I

    move-result v3

    if-ltz v3, :cond_1

    add-int/lit8 v5, v3, 0x1

    invoke-virtual {p1, v1, v5}, Ljava/lang/String;->substring(II)Ljava/lang/String;

    move-result-object v5

    invoke-virtual {p0, v5}, Lorg/apache/tools/ant/util/DOMElementWriter;->isReference(Ljava/lang/String;)Z

    move-result v5

    if-nez v5, :cond_2

    :cond_1
    const-string v5, "&amp;"

    invoke-virtual {v4, v5}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    goto :goto_1

    :cond_2
    const/16 v5, 0x26

    invoke-virtual {v4, v5}, Ljava/lang/StringBuffer;->append(C)Ljava/lang/StringBuffer;

    goto :goto_1

    :cond_3
    const/4 v5, 0x0

    invoke-virtual {v4, v5}, Ljava/lang/StringBuffer;->substring(I)Ljava/lang/String;

    move-result-object v5

    return-object v5

    nop

    :sswitch_data_0
    .sparse-switch
        0x22 -> :sswitch_3
        0x26 -> :sswitch_4
        0x27 -> :sswitch_2
        0x3c -> :sswitch_0
        0x3e -> :sswitch_1
    .end sparse-switch
.end method

.method public encodedata(Ljava/lang/String;)Ljava/lang/String;
    .locals 9
    .param p1    # Ljava/lang/String;

    const/4 v8, 0x0

    new-instance v5, Ljava/lang/StringBuffer;

    invoke-direct {v5}, Ljava/lang/StringBuffer;-><init>()V

    invoke-virtual {p1}, Ljava/lang/String;->length()I

    move-result v3

    const/4 v2, 0x0

    :goto_0
    if-ge v2, v3, :cond_1

    invoke-virtual {p1, v2}, Ljava/lang/String;->charAt(I)C

    move-result v0

    invoke-virtual {p0, v0}, Lorg/apache/tools/ant/util/DOMElementWriter;->isLegalCharacter(C)Z

    move-result v6

    if-eqz v6, :cond_0

    invoke-virtual {v5, v0}, Ljava/lang/StringBuffer;->append(C)Ljava/lang/StringBuffer;

    :cond_0
    add-int/lit8 v2, v2, 0x1

    goto :goto_0

    :cond_1
    invoke-virtual {v5, v8}, Ljava/lang/StringBuffer;->substring(I)Ljava/lang/String;

    move-result-object v4

    const-string v6, "]]>"

    invoke-virtual {v4, v6}, Ljava/lang/String;->indexOf(Ljava/lang/String;)I

    move-result v1

    :goto_1
    const/4 v6, -0x1

    if-eq v1, v6, :cond_2

    invoke-virtual {v5, v1}, Ljava/lang/StringBuffer;->setLength(I)V

    const-string v6, "&#x5d;&#x5d;&gt;"

    invoke-virtual {v5, v6}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    move-result-object v6

    add-int/lit8 v7, v1, 0x3

    invoke-virtual {v4, v7}, Ljava/lang/String;->substring(I)Ljava/lang/String;

    move-result-object v7

    invoke-virtual {v6, v7}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    invoke-virtual {v5, v8}, Ljava/lang/StringBuffer;->substring(I)Ljava/lang/String;

    move-result-object v4

    const-string v6, "]]>"

    invoke-virtual {v4, v6}, Ljava/lang/String;->indexOf(Ljava/lang/String;)I

    move-result v1

    goto :goto_1

    :cond_2
    return-object v4
.end method

.method public isLegalCharacter(C)Z
    .locals 3
    .param p1    # C

    const/4 v1, 0x1

    const/4 v0, 0x0

    const/16 v2, 0x9

    if-eq p1, v2, :cond_0

    const/16 v2, 0xa

    if-eq p1, v2, :cond_0

    const/16 v2, 0xd

    if-ne p1, v2, :cond_2

    :cond_0
    move v0, v1

    :cond_1
    :goto_0
    return v0

    :cond_2
    const/16 v2, 0x20

    if-lt p1, v2, :cond_1

    const v2, 0xd7ff

    if-gt p1, v2, :cond_3

    move v0, v1

    goto :goto_0

    :cond_3
    const v2, 0xe000

    if-lt p1, v2, :cond_1

    const v2, 0xfffd

    if-gt p1, v2, :cond_1

    move v0, v1

    goto :goto_0
.end method

.method public isReference(Ljava/lang/String;)Z
    .locals 8
    .param p1    # Ljava/lang/String;

    const/4 v7, 0x2

    const/4 v3, 0x1

    const/4 v4, 0x0

    invoke-virtual {p1, v4}, Ljava/lang/String;->charAt(I)C

    move-result v5

    const/16 v6, 0x26

    if-ne v5, v6, :cond_0

    const-string v5, ";"

    invoke-virtual {p1, v5}, Ljava/lang/String;->endsWith(Ljava/lang/String;)Z

    move-result v5

    if-nez v5, :cond_2

    :cond_0
    move v3, v4

    :cond_1
    :goto_0
    return v3

    :cond_2
    invoke-virtual {p1, v3}, Ljava/lang/String;->charAt(I)C

    move-result v5

    const/16 v6, 0x23

    if-ne v5, v6, :cond_4

    invoke-virtual {p1, v7}, Ljava/lang/String;->charAt(I)C

    move-result v5

    const/16 v6, 0x78

    if-ne v5, v6, :cond_3

    const/4 v5, 0x3

    :try_start_0
    invoke-virtual {p1}, Ljava/lang/String;->length()I

    move-result v6

    add-int/lit8 v6, v6, -0x1

    invoke-virtual {p1, v5, v6}, Ljava/lang/String;->substring(II)Ljava/lang/String;

    move-result-object v5

    const/16 v6, 0x10

    invoke-static {v5, v6}, Ljava/lang/Integer;->parseInt(Ljava/lang/String;I)I
    :try_end_0
    .catch Ljava/lang/NumberFormatException; {:try_start_0 .. :try_end_0} :catch_0

    goto :goto_0

    :catch_0
    move-exception v2

    move v3, v4

    goto :goto_0

    :cond_3
    const/4 v5, 0x2

    :try_start_1
    invoke-virtual {p1}, Ljava/lang/String;->length()I

    move-result v6

    add-int/lit8 v6, v6, -0x1

    invoke-virtual {p1, v5, v6}, Ljava/lang/String;->substring(II)Ljava/lang/String;

    move-result-object v5

    invoke-static {v5}, Ljava/lang/Integer;->parseInt(Ljava/lang/String;)I
    :try_end_1
    .catch Ljava/lang/NumberFormatException; {:try_start_1 .. :try_end_1} :catch_1

    goto :goto_0

    :catch_1
    move-exception v2

    move v3, v4

    goto :goto_0

    :cond_4
    invoke-virtual {p1}, Ljava/lang/String;->length()I

    move-result v5

    add-int/lit8 v5, v5, -0x1

    invoke-virtual {p1, v3, v5}, Ljava/lang/String;->substring(II)Ljava/lang/String;

    move-result-object v1

    const/4 v0, 0x0

    :goto_1
    iget-object v5, p0, Lorg/apache/tools/ant/util/DOMElementWriter;->knownEntities:[Ljava/lang/String;

    array-length v5, v5

    if-ge v0, v5, :cond_5

    iget-object v5, p0, Lorg/apache/tools/ant/util/DOMElementWriter;->knownEntities:[Ljava/lang/String;

    aget-object v5, v5, v0

    invoke-virtual {v1, v5}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v5

    if-nez v5, :cond_1

    add-int/lit8 v0, v0, 0x1

    goto :goto_1

    :cond_5
    move v3, v4

    goto :goto_0
.end method

.method public openElement(Lorg/w3c/dom/Element;Ljava/io/Writer;ILjava/lang/String;)V
    .locals 6
    .param p1    # Lorg/w3c/dom/Element;
    .param p2    # Ljava/io/Writer;
    .param p3    # I
    .param p4    # Ljava/lang/String;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    const/4 v5, 0x1

    move-object v0, p0

    move-object v1, p1

    move-object v2, p2

    move v3, p3

    move-object v4, p4

    invoke-virtual/range {v0 .. v5}, Lorg/apache/tools/ant/util/DOMElementWriter;->openElement(Lorg/w3c/dom/Element;Ljava/io/Writer;ILjava/lang/String;Z)V

    return-void
.end method

.method public openElement(Lorg/w3c/dom/Element;Ljava/io/Writer;ILjava/lang/String;Z)V
    .locals 10
    .param p1    # Lorg/w3c/dom/Element;
    .param p2    # Ljava/io/Writer;
    .param p3    # I
    .param p4    # Ljava/lang/String;
    .param p5    # Z
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    const/4 v3, 0x0

    :goto_0
    if-ge v3, p3, :cond_0

    invoke-virtual {p2, p4}, Ljava/io/Writer;->write(Ljava/lang/String;)V

    add-int/lit8 v3, v3, 0x1

    goto :goto_0

    :cond_0
    const-string v7, "<"

    invoke-virtual {p2, v7}, Ljava/io/Writer;->write(Ljava/lang/String;)V

    iget-object v7, p0, Lorg/apache/tools/ant/util/DOMElementWriter;->namespacePolicy:Lorg/apache/tools/ant/util/DOMElementWriter$XmlNamespacePolicy;

    invoke-static {v7}, Lorg/apache/tools/ant/util/DOMElementWriter$XmlNamespacePolicy;->access$000(Lorg/apache/tools/ant/util/DOMElementWriter$XmlNamespacePolicy;)Z

    move-result v7

    if-eqz v7, :cond_2

    invoke-static {p1}, Lorg/apache/tools/ant/util/DOMElementWriter;->getNamespaceURI(Lorg/w3c/dom/Node;)Ljava/lang/String;

    move-result-object v6

    iget-object v7, p0, Lorg/apache/tools/ant/util/DOMElementWriter;->nsPrefixMap:Ljava/util/HashMap;

    invoke-virtual {v7, v6}, Ljava/util/HashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v5

    check-cast v5, Ljava/lang/String;

    if-nez v5, :cond_1

    iget-object v7, p0, Lorg/apache/tools/ant/util/DOMElementWriter;->nsPrefixMap:Ljava/util/HashMap;

    invoke-virtual {v7}, Ljava/util/HashMap;->isEmpty()Z

    move-result v7

    if-eqz v7, :cond_5

    const-string v5, ""

    :goto_1
    iget-object v7, p0, Lorg/apache/tools/ant/util/DOMElementWriter;->nsPrefixMap:Ljava/util/HashMap;

    invoke-virtual {v7, v6, v5}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    invoke-direct {p0, p1, v6}, Lorg/apache/tools/ant/util/DOMElementWriter;->addNSDefinition(Lorg/w3c/dom/Element;Ljava/lang/String;)V

    :cond_1
    const-string v7, ""

    invoke-virtual {v7, v5}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v7

    if-nez v7, :cond_2

    invoke-virtual {p2, v5}, Ljava/io/Writer;->write(Ljava/lang/String;)V

    const-string v7, ":"

    invoke-virtual {p2, v7}, Ljava/io/Writer;->write(Ljava/lang/String;)V

    :cond_2
    invoke-interface {p1}, Lorg/w3c/dom/Element;->getTagName()Ljava/lang/String;

    move-result-object v7

    invoke-virtual {p2, v7}, Ljava/io/Writer;->write(Ljava/lang/String;)V

    invoke-interface {p1}, Lorg/w3c/dom/Element;->getAttributes()Lorg/w3c/dom/NamedNodeMap;

    move-result-object v2

    const/4 v3, 0x0

    :goto_2
    invoke-interface {v2}, Lorg/w3c/dom/NamedNodeMap;->getLength()I

    move-result v7

    if-ge v3, v7, :cond_6

    invoke-interface {v2, v3}, Lorg/w3c/dom/NamedNodeMap;->item(I)Lorg/w3c/dom/Node;

    move-result-object v1

    check-cast v1, Lorg/w3c/dom/Attr;

    const-string v7, " "

    invoke-virtual {p2, v7}, Ljava/io/Writer;->write(Ljava/lang/String;)V

    iget-object v7, p0, Lorg/apache/tools/ant/util/DOMElementWriter;->namespacePolicy:Lorg/apache/tools/ant/util/DOMElementWriter$XmlNamespacePolicy;

    invoke-static {v7}, Lorg/apache/tools/ant/util/DOMElementWriter$XmlNamespacePolicy;->access$100(Lorg/apache/tools/ant/util/DOMElementWriter$XmlNamespacePolicy;)Z

    move-result v7

    if-eqz v7, :cond_4

    invoke-static {v1}, Lorg/apache/tools/ant/util/DOMElementWriter;->getNamespaceURI(Lorg/w3c/dom/Node;)Ljava/lang/String;

    move-result-object v6

    iget-object v7, p0, Lorg/apache/tools/ant/util/DOMElementWriter;->nsPrefixMap:Ljava/util/HashMap;

    invoke-virtual {v7, v6}, Ljava/util/HashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v5

    check-cast v5, Ljava/lang/String;

    if-nez v5, :cond_3

    new-instance v7, Ljava/lang/StringBuffer;

    invoke-direct {v7}, Ljava/lang/StringBuffer;-><init>()V

    const-string v8, "ns"

    invoke-virtual {v7, v8}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    move-result-object v7

    iget v8, p0, Lorg/apache/tools/ant/util/DOMElementWriter;->nextPrefix:I

    add-int/lit8 v9, v8, 0x1

    iput v9, p0, Lorg/apache/tools/ant/util/DOMElementWriter;->nextPrefix:I

    invoke-virtual {v7, v8}, Ljava/lang/StringBuffer;->append(I)Ljava/lang/StringBuffer;

    move-result-object v7

    invoke-virtual {v7}, Ljava/lang/StringBuffer;->toString()Ljava/lang/String;

    move-result-object v5

    iget-object v7, p0, Lorg/apache/tools/ant/util/DOMElementWriter;->nsPrefixMap:Ljava/util/HashMap;

    invoke-virtual {v7, v6, v5}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    invoke-direct {p0, p1, v6}, Lorg/apache/tools/ant/util/DOMElementWriter;->addNSDefinition(Lorg/w3c/dom/Element;Ljava/lang/String;)V

    :cond_3
    invoke-virtual {p2, v5}, Ljava/io/Writer;->write(Ljava/lang/String;)V

    const-string v7, ":"

    invoke-virtual {p2, v7}, Ljava/io/Writer;->write(Ljava/lang/String;)V

    :cond_4
    invoke-interface {v1}, Lorg/w3c/dom/Attr;->getName()Ljava/lang/String;

    move-result-object v7

    invoke-virtual {p2, v7}, Ljava/io/Writer;->write(Ljava/lang/String;)V

    const-string v7, "=\""

    invoke-virtual {p2, v7}, Ljava/io/Writer;->write(Ljava/lang/String;)V

    invoke-interface {v1}, Lorg/w3c/dom/Attr;->getValue()Ljava/lang/String;

    move-result-object v7

    invoke-virtual {p0, v7}, Lorg/apache/tools/ant/util/DOMElementWriter;->encode(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v7

    invoke-virtual {p2, v7}, Ljava/io/Writer;->write(Ljava/lang/String;)V

    const-string v7, "\""

    invoke-virtual {p2, v7}, Ljava/io/Writer;->write(Ljava/lang/String;)V

    add-int/lit8 v3, v3, 0x1

    goto :goto_2

    :cond_5
    new-instance v7, Ljava/lang/StringBuffer;

    invoke-direct {v7}, Ljava/lang/StringBuffer;-><init>()V

    const-string v8, "ns"

    invoke-virtual {v7, v8}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    move-result-object v7

    iget v8, p0, Lorg/apache/tools/ant/util/DOMElementWriter;->nextPrefix:I

    add-int/lit8 v9, v8, 0x1

    iput v9, p0, Lorg/apache/tools/ant/util/DOMElementWriter;->nextPrefix:I

    invoke-virtual {v7, v8}, Ljava/lang/StringBuffer;->append(I)Ljava/lang/StringBuffer;

    move-result-object v7

    invoke-virtual {v7}, Ljava/lang/StringBuffer;->toString()Ljava/lang/String;

    move-result-object v5

    goto/16 :goto_1

    :cond_6
    iget-object v7, p0, Lorg/apache/tools/ant/util/DOMElementWriter;->nsURIByElement:Ljava/util/HashMap;

    invoke-virtual {v7, p1}, Ljava/util/HashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/util/ArrayList;

    if-eqz v0, :cond_8

    invoke-virtual {v0}, Ljava/util/ArrayList;->iterator()Ljava/util/Iterator;

    move-result-object v4

    :goto_3
    invoke-interface {v4}, Ljava/util/Iterator;->hasNext()Z

    move-result v7

    if-eqz v7, :cond_8

    invoke-interface {v4}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v6

    check-cast v6, Ljava/lang/String;

    iget-object v7, p0, Lorg/apache/tools/ant/util/DOMElementWriter;->nsPrefixMap:Ljava/util/HashMap;

    invoke-virtual {v7, v6}, Ljava/util/HashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v5

    check-cast v5, Ljava/lang/String;

    const-string v7, " xmlns"

    invoke-virtual {p2, v7}, Ljava/io/Writer;->write(Ljava/lang/String;)V

    const-string v7, ""

    invoke-virtual {v7, v5}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v7

    if-nez v7, :cond_7

    const-string v7, ":"

    invoke-virtual {p2, v7}, Ljava/io/Writer;->write(Ljava/lang/String;)V

    invoke-virtual {p2, v5}, Ljava/io/Writer;->write(Ljava/lang/String;)V

    :cond_7
    const-string v7, "=\""

    invoke-virtual {p2, v7}, Ljava/io/Writer;->write(Ljava/lang/String;)V

    invoke-virtual {p2, v6}, Ljava/io/Writer;->write(Ljava/lang/String;)V

    const-string v7, "\""

    invoke-virtual {p2, v7}, Ljava/io/Writer;->write(Ljava/lang/String;)V

    goto :goto_3

    :cond_8
    if-eqz p5, :cond_9

    const-string v7, ">"

    invoke-virtual {p2, v7}, Ljava/io/Writer;->write(Ljava/lang/String;)V

    :goto_4
    return-void

    :cond_9
    invoke-direct {p0, p1}, Lorg/apache/tools/ant/util/DOMElementWriter;->removeNSDefinitions(Lorg/w3c/dom/Element;)V

    const-string v7, " />"

    invoke-virtual {p2, v7}, Ljava/io/Writer;->write(Ljava/lang/String;)V

    sget-object v7, Lorg/apache/tools/ant/util/DOMElementWriter;->lSep:Ljava/lang/String;

    invoke-virtual {p2, v7}, Ljava/io/Writer;->write(Ljava/lang/String;)V

    invoke-virtual {p2}, Ljava/io/Writer;->flush()V

    goto :goto_4
.end method

.method public write(Lorg/w3c/dom/Element;Ljava/io/OutputStream;)V
    .locals 3
    .param p1    # Lorg/w3c/dom/Element;
    .param p2    # Ljava/io/OutputStream;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    new-instance v0, Ljava/io/OutputStreamWriter;

    const-string v1, "UTF8"

    invoke-direct {v0, p2, v1}, Ljava/io/OutputStreamWriter;-><init>(Ljava/io/OutputStream;Ljava/lang/String;)V

    invoke-virtual {p0, v0}, Lorg/apache/tools/ant/util/DOMElementWriter;->writeXMLDeclaration(Ljava/io/Writer;)V

    const/4 v1, 0x0

    const-string v2, "  "

    invoke-virtual {p0, p1, v0, v1, v2}, Lorg/apache/tools/ant/util/DOMElementWriter;->write(Lorg/w3c/dom/Element;Ljava/io/Writer;ILjava/lang/String;)V

    invoke-virtual {v0}, Ljava/io/Writer;->flush()V

    return-void
.end method

.method public write(Lorg/w3c/dom/Element;Ljava/io/Writer;ILjava/lang/String;)V
    .locals 19
    .param p1    # Lorg/w3c/dom/Element;
    .param p2    # Ljava/io/Writer;
    .param p3    # I
    .param p4    # Ljava/lang/String;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    invoke-interface/range {p1 .. p1}, Lorg/w3c/dom/Element;->getChildNodes()Lorg/w3c/dom/NodeList;

    move-result-object v16

    invoke-interface/range {v16 .. v16}, Lorg/w3c/dom/NodeList;->getLength()I

    move-result v3

    if-lez v3, :cond_0

    const/4 v8, 0x1

    :goto_0
    const/4 v14, 0x0

    move-object/from16 v3, p0

    move-object/from16 v4, p1

    move-object/from16 v5, p2

    move/from16 v6, p3

    move-object/from16 v7, p4

    invoke-virtual/range {v3 .. v8}, Lorg/apache/tools/ant/util/DOMElementWriter;->openElement(Lorg/w3c/dom/Element;Ljava/io/Writer;ILjava/lang/String;Z)V

    if-eqz v8, :cond_4

    const/16 v18, 0x0

    :goto_1
    invoke-interface/range {v16 .. v16}, Lorg/w3c/dom/NodeList;->getLength()I

    move-result v3

    move/from16 v0, v18

    if-ge v0, v3, :cond_3

    move-object/from16 v0, v16

    move/from16 v1, v18

    invoke-interface {v0, v1}, Lorg/w3c/dom/NodeList;->item(I)Lorg/w3c/dom/Node;

    move-result-object v15

    invoke-interface {v15}, Lorg/w3c/dom/Node;->getNodeType()S

    move-result v3

    packed-switch v3, :pswitch_data_0

    :goto_2
    :pswitch_0
    add-int/lit8 v18, v18, 0x1

    goto :goto_1

    :cond_0
    const/4 v8, 0x0

    goto :goto_0

    :pswitch_1
    const/4 v14, 0x1

    if-nez v18, :cond_1

    sget-object v3, Lorg/apache/tools/ant/util/DOMElementWriter;->lSep:Ljava/lang/String;

    move-object/from16 v0, p2

    invoke-virtual {v0, v3}, Ljava/io/Writer;->write(Ljava/lang/String;)V

    :cond_1
    check-cast v15, Lorg/w3c/dom/Element;

    add-int/lit8 v3, p3, 0x1

    move-object/from16 v0, p0

    move-object/from16 v1, p2

    move-object/from16 v2, p4

    invoke-virtual {v0, v15, v1, v3, v2}, Lorg/apache/tools/ant/util/DOMElementWriter;->write(Lorg/w3c/dom/Element;Ljava/io/Writer;ILjava/lang/String;)V

    goto :goto_2

    :pswitch_2
    invoke-interface {v15}, Lorg/w3c/dom/Node;->getNodeValue()Ljava/lang/String;

    move-result-object v3

    move-object/from16 v0, p0

    invoke-virtual {v0, v3}, Lorg/apache/tools/ant/util/DOMElementWriter;->encode(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v3

    move-object/from16 v0, p2

    invoke-virtual {v0, v3}, Ljava/io/Writer;->write(Ljava/lang/String;)V

    goto :goto_2

    :pswitch_3
    const-string v3, "<!--"

    move-object/from16 v0, p2

    invoke-virtual {v0, v3}, Ljava/io/Writer;->write(Ljava/lang/String;)V

    invoke-interface {v15}, Lorg/w3c/dom/Node;->getNodeValue()Ljava/lang/String;

    move-result-object v3

    move-object/from16 v0, p0

    invoke-virtual {v0, v3}, Lorg/apache/tools/ant/util/DOMElementWriter;->encode(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v3

    move-object/from16 v0, p2

    invoke-virtual {v0, v3}, Ljava/io/Writer;->write(Ljava/lang/String;)V

    const-string v3, "-->"

    move-object/from16 v0, p2

    invoke-virtual {v0, v3}, Ljava/io/Writer;->write(Ljava/lang/String;)V

    goto :goto_2

    :pswitch_4
    const-string v3, "<![CDATA["

    move-object/from16 v0, p2

    invoke-virtual {v0, v3}, Ljava/io/Writer;->write(Ljava/lang/String;)V

    check-cast v15, Lorg/w3c/dom/Text;

    invoke-interface {v15}, Lorg/w3c/dom/Text;->getData()Ljava/lang/String;

    move-result-object v3

    move-object/from16 v0, p0

    invoke-virtual {v0, v3}, Lorg/apache/tools/ant/util/DOMElementWriter;->encodedata(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v3

    move-object/from16 v0, p2

    invoke-virtual {v0, v3}, Ljava/io/Writer;->write(Ljava/lang/String;)V

    const-string v3, "]]>"

    move-object/from16 v0, p2

    invoke-virtual {v0, v3}, Ljava/io/Writer;->write(Ljava/lang/String;)V

    goto :goto_2

    :pswitch_5
    const/16 v3, 0x26

    move-object/from16 v0, p2

    invoke-virtual {v0, v3}, Ljava/io/Writer;->write(I)V

    invoke-interface {v15}, Lorg/w3c/dom/Node;->getNodeName()Ljava/lang/String;

    move-result-object v3

    move-object/from16 v0, p2

    invoke-virtual {v0, v3}, Ljava/io/Writer;->write(Ljava/lang/String;)V

    const/16 v3, 0x3b

    move-object/from16 v0, p2

    invoke-virtual {v0, v3}, Ljava/io/Writer;->write(I)V

    goto/16 :goto_2

    :pswitch_6
    const-string v3, "<?"

    move-object/from16 v0, p2

    invoke-virtual {v0, v3}, Ljava/io/Writer;->write(Ljava/lang/String;)V

    invoke-interface {v15}, Lorg/w3c/dom/Node;->getNodeName()Ljava/lang/String;

    move-result-object v3

    move-object/from16 v0, p2

    invoke-virtual {v0, v3}, Ljava/io/Writer;->write(Ljava/lang/String;)V

    invoke-interface {v15}, Lorg/w3c/dom/Node;->getNodeValue()Ljava/lang/String;

    move-result-object v17

    if-eqz v17, :cond_2

    invoke-virtual/range {v17 .. v17}, Ljava/lang/String;->length()I

    move-result v3

    if-lez v3, :cond_2

    const/16 v3, 0x20

    move-object/from16 v0, p2

    invoke-virtual {v0, v3}, Ljava/io/Writer;->write(I)V

    move-object/from16 v0, p2

    move-object/from16 v1, v17

    invoke-virtual {v0, v1}, Ljava/io/Writer;->write(Ljava/lang/String;)V

    :cond_2
    const-string v3, "?>"

    move-object/from16 v0, p2

    invoke-virtual {v0, v3}, Ljava/io/Writer;->write(Ljava/lang/String;)V

    goto/16 :goto_2

    :cond_3
    move-object/from16 v9, p0

    move-object/from16 v10, p1

    move-object/from16 v11, p2

    move/from16 v12, p3

    move-object/from16 v13, p4

    invoke-virtual/range {v9 .. v14}, Lorg/apache/tools/ant/util/DOMElementWriter;->closeElement(Lorg/w3c/dom/Element;Ljava/io/Writer;ILjava/lang/String;Z)V

    :cond_4
    return-void

    nop

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_1
        :pswitch_0
        :pswitch_2
        :pswitch_4
        :pswitch_5
        :pswitch_0
        :pswitch_6
        :pswitch_3
    .end packed-switch
.end method

.method public writeXMLDeclaration(Ljava/io/Writer;)V
    .locals 1
    .param p1    # Ljava/io/Writer;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    iget-boolean v0, p0, Lorg/apache/tools/ant/util/DOMElementWriter;->xmlDeclaration:Z

    if-eqz v0, :cond_0

    const-string v0, "<?xml version=\"1.0\" encoding=\"UTF-8\"?>\n"

    invoke-virtual {p1, v0}, Ljava/io/Writer;->write(Ljava/lang/String;)V

    :cond_0
    return-void
.end method
