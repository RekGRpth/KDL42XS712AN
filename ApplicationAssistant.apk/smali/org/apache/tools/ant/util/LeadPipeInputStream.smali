.class public Lorg/apache/tools/ant/util/LeadPipeInputStream;
.super Ljava/io/PipedInputStream;
.source "LeadPipeInputStream.java"


# instance fields
.field private managingPc:Lorg/apache/tools/ant/ProjectComponent;


# direct methods
.method public constructor <init>()V
    .locals 0

    invoke-direct {p0}, Ljava/io/PipedInputStream;-><init>()V

    return-void
.end method

.method public constructor <init>(I)V
    .locals 0
    .param p1    # I

    invoke-direct {p0}, Ljava/io/PipedInputStream;-><init>()V

    invoke-virtual {p0, p1}, Lorg/apache/tools/ant/util/LeadPipeInputStream;->setBufferSize(I)V

    return-void
.end method

.method public constructor <init>(Ljava/io/PipedOutputStream;)V
    .locals 0
    .param p1    # Ljava/io/PipedOutputStream;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    invoke-direct {p0, p1}, Ljava/io/PipedInputStream;-><init>(Ljava/io/PipedOutputStream;)V

    return-void
.end method

.method public constructor <init>(Ljava/io/PipedOutputStream;I)V
    .locals 0
    .param p1    # Ljava/io/PipedOutputStream;
    .param p2    # I
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    invoke-direct {p0, p1}, Ljava/io/PipedInputStream;-><init>(Ljava/io/PipedOutputStream;)V

    invoke-virtual {p0, p2}, Lorg/apache/tools/ant/util/LeadPipeInputStream;->setBufferSize(I)V

    return-void
.end method


# virtual methods
.method public log(Ljava/lang/String;I)V
    .locals 1
    .param p1    # Ljava/lang/String;
    .param p2    # I

    iget-object v0, p0, Lorg/apache/tools/ant/util/LeadPipeInputStream;->managingPc:Lorg/apache/tools/ant/ProjectComponent;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lorg/apache/tools/ant/util/LeadPipeInputStream;->managingPc:Lorg/apache/tools/ant/ProjectComponent;

    invoke-virtual {v0, p1, p2}, Lorg/apache/tools/ant/ProjectComponent;->log(Ljava/lang/String;I)V

    :goto_0
    return-void

    :cond_0
    const/4 v0, 0x1

    if-le p2, v0, :cond_1

    sget-object v0, Ljava/lang/System;->out:Ljava/io/PrintStream;

    invoke-virtual {v0, p1}, Ljava/io/PrintStream;->println(Ljava/lang/String;)V

    goto :goto_0

    :cond_1
    sget-object v0, Ljava/lang/System;->err:Ljava/io/PrintStream;

    invoke-virtual {v0, p1}, Ljava/io/PrintStream;->println(Ljava/lang/String;)V

    goto :goto_0
.end method

.method public declared-synchronized read()I
    .locals 5
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    monitor-enter p0

    const/4 v1, -0x1

    :try_start_0
    invoke-super {p0}, Ljava/io/PipedInputStream;->read()I
    :try_end_0
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    move-result v1

    :cond_0
    :goto_0
    monitor-exit p0

    return v1

    :catch_0
    move-exception v0

    :try_start_1
    const-string v2, "write end dead"

    invoke-virtual {v0}, Ljava/io/IOException;->getMessage()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v2

    if-eqz v2, :cond_1

    iget v2, p0, Ljava/io/PipedInputStream;->in:I

    if-lez v2, :cond_0

    iget v2, p0, Ljava/io/PipedInputStream;->out:I

    iget-object v3, p0, Ljava/io/PipedInputStream;->buffer:[B

    array-length v3, v3

    if-ge v2, v3, :cond_0

    iget v2, p0, Ljava/io/PipedInputStream;->out:I

    iget v3, p0, Ljava/io/PipedInputStream;->in:I

    if-le v2, v3, :cond_0

    iget-object v2, p0, Ljava/io/PipedInputStream;->buffer:[B

    iget v3, p0, Ljava/io/PipedInputStream;->out:I

    add-int/lit8 v4, v3, 0x1

    iput v4, p0, Ljava/io/PipedInputStream;->out:I

    aget-byte v2, v2, v3

    and-int/lit16 v1, v2, 0xff

    goto :goto_0

    :cond_1
    new-instance v2, Ljava/lang/StringBuffer;

    invoke-direct {v2}, Ljava/lang/StringBuffer;-><init>()V

    const-string v3, "error at LeadPipeInputStream.read():  "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    move-result-object v2

    invoke-virtual {v0}, Ljava/io/IOException;->getMessage()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuffer;->toString()Ljava/lang/String;

    move-result-object v2

    const/4 v3, 0x2

    invoke-virtual {p0, v2, v3}, Lorg/apache/tools/ant/util/LeadPipeInputStream;->log(Ljava/lang/String;I)V
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    goto :goto_0

    :catchall_0
    move-exception v2

    monitor-exit p0

    throw v2
.end method

.method public declared-synchronized setBufferSize(I)V
    .locals 7
    .param p1    # I

    monitor-enter p0

    :try_start_0
    iget-object v2, p0, Lorg/apache/tools/ant/util/LeadPipeInputStream;->buffer:[B

    array-length v2, v2

    if-le p1, v2, :cond_1

    new-array v0, p1, [B

    iget v2, p0, Lorg/apache/tools/ant/util/LeadPipeInputStream;->in:I

    if-ltz v2, :cond_0

    iget v2, p0, Lorg/apache/tools/ant/util/LeadPipeInputStream;->in:I

    iget v3, p0, Lorg/apache/tools/ant/util/LeadPipeInputStream;->out:I

    if-le v2, v3, :cond_2

    iget-object v2, p0, Lorg/apache/tools/ant/util/LeadPipeInputStream;->buffer:[B

    iget v3, p0, Lorg/apache/tools/ant/util/LeadPipeInputStream;->out:I

    iget v4, p0, Lorg/apache/tools/ant/util/LeadPipeInputStream;->out:I

    iget v5, p0, Lorg/apache/tools/ant/util/LeadPipeInputStream;->in:I

    iget v6, p0, Lorg/apache/tools/ant/util/LeadPipeInputStream;->out:I

    sub-int/2addr v5, v6

    invoke-static {v2, v3, v0, v4, v5}, Ljava/lang/System;->arraycopy(Ljava/lang/Object;ILjava/lang/Object;II)V

    :cond_0
    :goto_0
    iput-object v0, p0, Lorg/apache/tools/ant/util/LeadPipeInputStream;->buffer:[B
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    :cond_1
    monitor-exit p0

    return-void

    :cond_2
    :try_start_1
    iget-object v2, p0, Lorg/apache/tools/ant/util/LeadPipeInputStream;->buffer:[B

    array-length v2, v2

    iget v3, p0, Lorg/apache/tools/ant/util/LeadPipeInputStream;->out:I

    sub-int v1, v2, v3

    iget-object v2, p0, Lorg/apache/tools/ant/util/LeadPipeInputStream;->buffer:[B

    iget v3, p0, Lorg/apache/tools/ant/util/LeadPipeInputStream;->out:I

    const/4 v4, 0x0

    invoke-static {v2, v3, v0, v4, v1}, Ljava/lang/System;->arraycopy(Ljava/lang/Object;ILjava/lang/Object;II)V

    iget-object v2, p0, Lorg/apache/tools/ant/util/LeadPipeInputStream;->buffer:[B

    const/4 v3, 0x0

    iget v4, p0, Lorg/apache/tools/ant/util/LeadPipeInputStream;->in:I

    invoke-static {v2, v3, v0, v1, v4}, Ljava/lang/System;->arraycopy(Ljava/lang/Object;ILjava/lang/Object;II)V

    iget v2, p0, Lorg/apache/tools/ant/util/LeadPipeInputStream;->in:I

    add-int/2addr v2, v1

    iput v2, p0, Lorg/apache/tools/ant/util/LeadPipeInputStream;->in:I

    const/4 v2, 0x0

    iput v2, p0, Lorg/apache/tools/ant/util/LeadPipeInputStream;->out:I
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    goto :goto_0

    :catchall_0
    move-exception v2

    monitor-exit p0

    throw v2
.end method

.method public setManagingComponent(Lorg/apache/tools/ant/ProjectComponent;)V
    .locals 0
    .param p1    # Lorg/apache/tools/ant/ProjectComponent;

    iput-object p1, p0, Lorg/apache/tools/ant/util/LeadPipeInputStream;->managingPc:Lorg/apache/tools/ant/ProjectComponent;

    return-void
.end method

.method public setManagingTask(Lorg/apache/tools/ant/Task;)V
    .locals 0
    .param p1    # Lorg/apache/tools/ant/Task;

    invoke-virtual {p0, p1}, Lorg/apache/tools/ant/util/LeadPipeInputStream;->setManagingComponent(Lorg/apache/tools/ant/ProjectComponent;)V

    return-void
.end method
