.class public Lorg/apache/tools/ant/util/ReaderInputStream;
.super Ljava/io/InputStream;
.source "ReaderInputStream.java"


# instance fields
.field private begin:I

.field private encoding:Ljava/lang/String;

.field private in:Ljava/io/Reader;

.field private slack:[B


# direct methods
.method public constructor <init>(Ljava/io/Reader;)V
    .locals 1
    .param p1    # Ljava/io/Reader;

    invoke-direct {p0}, Ljava/io/InputStream;-><init>()V

    const-string v0, "file.encoding"

    invoke-static {v0}, Ljava/lang/System;->getProperty(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lorg/apache/tools/ant/util/ReaderInputStream;->encoding:Ljava/lang/String;

    iput-object p1, p0, Lorg/apache/tools/ant/util/ReaderInputStream;->in:Ljava/io/Reader;

    return-void
.end method

.method public constructor <init>(Ljava/io/Reader;Ljava/lang/String;)V
    .locals 2
    .param p1    # Ljava/io/Reader;
    .param p2    # Ljava/lang/String;

    invoke-direct {p0, p1}, Lorg/apache/tools/ant/util/ReaderInputStream;-><init>(Ljava/io/Reader;)V

    if-nez p2, :cond_0

    new-instance v0, Ljava/lang/IllegalArgumentException;

    const-string v1, "encoding must not be null"

    invoke-direct {v0, v1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v0

    :cond_0
    iput-object p2, p0, Lorg/apache/tools/ant/util/ReaderInputStream;->encoding:Ljava/lang/String;

    return-void
.end method


# virtual methods
.method public declared-synchronized available()I
    .locals 2
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    monitor-enter p0

    :try_start_0
    iget-object v0, p0, Lorg/apache/tools/ant/util/ReaderInputStream;->in:Ljava/io/Reader;

    if-nez v0, :cond_0

    new-instance v0, Ljava/io/IOException;

    const-string v1, "Stream Closed"

    invoke-direct {v0, v1}, Ljava/io/IOException;-><init>(Ljava/lang/String;)V

    throw v0
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0

    :cond_0
    :try_start_1
    iget-object v0, p0, Lorg/apache/tools/ant/util/ReaderInputStream;->slack:[B

    if-eqz v0, :cond_1

    iget-object v0, p0, Lorg/apache/tools/ant/util/ReaderInputStream;->slack:[B

    array-length v0, v0

    iget v1, p0, Lorg/apache/tools/ant/util/ReaderInputStream;->begin:I
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    sub-int/2addr v0, v1

    :goto_0
    monitor-exit p0

    return v0

    :cond_1
    :try_start_2
    iget-object v0, p0, Lorg/apache/tools/ant/util/ReaderInputStream;->in:Ljava/io/Reader;

    invoke-virtual {v0}, Ljava/io/Reader;->ready()Z
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    move-result v0

    if-eqz v0, :cond_2

    const/4 v0, 0x1

    goto :goto_0

    :cond_2
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public declared-synchronized close()V
    .locals 1
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    monitor-enter p0

    :try_start_0
    iget-object v0, p0, Lorg/apache/tools/ant/util/ReaderInputStream;->in:Ljava/io/Reader;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lorg/apache/tools/ant/util/ReaderInputStream;->in:Ljava/io/Reader;

    invoke-virtual {v0}, Ljava/io/Reader;->close()V

    const/4 v0, 0x0

    iput-object v0, p0, Lorg/apache/tools/ant/util/ReaderInputStream;->slack:[B

    const/4 v0, 0x0

    iput-object v0, p0, Lorg/apache/tools/ant/util/ReaderInputStream;->in:Ljava/io/Reader;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    :cond_0
    monitor-exit p0

    return-void

    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method public declared-synchronized mark(I)V
    .locals 3
    .param p1    # I

    monitor-enter p0

    :try_start_0
    iget-object v1, p0, Lorg/apache/tools/ant/util/ReaderInputStream;->in:Ljava/io/Reader;

    invoke-virtual {v1, p1}, Ljava/io/Reader;->mark(I)V
    :try_end_0
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    monitor-exit p0

    return-void

    :catch_0
    move-exception v0

    :try_start_1
    new-instance v1, Ljava/lang/RuntimeException;

    invoke-virtual {v0}, Ljava/io/IOException;->getMessage()Ljava/lang/String;

    move-result-object v2

    invoke-direct {v1, v2}, Ljava/lang/RuntimeException;-><init>(Ljava/lang/String;)V

    throw v1
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    :catchall_0
    move-exception v1

    monitor-exit p0

    throw v1
.end method

.method public markSupported()Z
    .locals 1

    const/4 v0, 0x0

    return v0
.end method

.method public declared-synchronized read()I
    .locals 4
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    monitor-enter p0

    :try_start_0
    iget-object v2, p0, Lorg/apache/tools/ant/util/ReaderInputStream;->in:Ljava/io/Reader;

    if-nez v2, :cond_0

    new-instance v2, Ljava/io/IOException;

    const-string v3, "Stream Closed"

    invoke-direct {v2, v3}, Ljava/io/IOException;-><init>(Ljava/lang/String;)V

    throw v2
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    :catchall_0
    move-exception v2

    monitor-exit p0

    throw v2

    :cond_0
    :try_start_1
    iget-object v2, p0, Lorg/apache/tools/ant/util/ReaderInputStream;->slack:[B

    if-eqz v2, :cond_2

    iget v2, p0, Lorg/apache/tools/ant/util/ReaderInputStream;->begin:I

    iget-object v3, p0, Lorg/apache/tools/ant/util/ReaderInputStream;->slack:[B

    array-length v3, v3

    if-ge v2, v3, :cond_2

    iget-object v2, p0, Lorg/apache/tools/ant/util/ReaderInputStream;->slack:[B

    iget v3, p0, Lorg/apache/tools/ant/util/ReaderInputStream;->begin:I

    aget-byte v1, v2, v3

    iget v2, p0, Lorg/apache/tools/ant/util/ReaderInputStream;->begin:I

    add-int/lit8 v2, v2, 0x1

    iput v2, p0, Lorg/apache/tools/ant/util/ReaderInputStream;->begin:I

    iget-object v3, p0, Lorg/apache/tools/ant/util/ReaderInputStream;->slack:[B

    array-length v3, v3

    if-ne v2, v3, :cond_1

    const/4 v2, 0x0

    iput-object v2, p0, Lorg/apache/tools/ant/util/ReaderInputStream;->slack:[B
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    :cond_1
    :goto_0
    and-int/lit16 v2, v1, 0xff

    :goto_1
    monitor-exit p0

    return v2

    :cond_2
    const/4 v2, 0x1

    :try_start_2
    new-array v0, v2, [B

    const/4 v2, 0x0

    const/4 v3, 0x1

    invoke-virtual {p0, v0, v2, v3}, Lorg/apache/tools/ant/util/ReaderInputStream;->read([BII)I

    move-result v2

    if-gtz v2, :cond_3

    const/4 v2, -0x1

    goto :goto_1

    :cond_3
    const/4 v2, 0x0

    aget-byte v1, v0, v2
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    goto :goto_0
.end method

.method public declared-synchronized read([BII)I
    .locals 5
    .param p1    # [B
    .param p2    # I
    .param p3    # I
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    const/4 v3, -0x1

    const/4 v2, 0x0

    monitor-enter p0

    :try_start_0
    iget-object v4, p0, Lorg/apache/tools/ant/util/ReaderInputStream;->in:Ljava/io/Reader;

    if-nez v4, :cond_0

    new-instance v2, Ljava/io/IOException;

    const-string v3, "Stream Closed"

    invoke-direct {v2, v3}, Ljava/io/IOException;-><init>(Ljava/lang/String;)V

    throw v2
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    :catchall_0
    move-exception v2

    monitor-exit p0

    throw v2

    :cond_0
    if-nez p3, :cond_2

    :goto_0
    monitor-exit p0

    return v2

    :cond_1
    if-lez v1, :cond_2

    :try_start_1
    new-instance v2, Ljava/lang/String;

    const/4 v4, 0x0

    invoke-direct {v2, v0, v4, v1}, Ljava/lang/String;-><init>([CII)V

    iget-object v4, p0, Lorg/apache/tools/ant/util/ReaderInputStream;->encoding:Ljava/lang/String;

    invoke-virtual {v2, v4}, Ljava/lang/String;->getBytes(Ljava/lang/String;)[B

    move-result-object v2

    iput-object v2, p0, Lorg/apache/tools/ant/util/ReaderInputStream;->slack:[B

    const/4 v2, 0x0

    iput v2, p0, Lorg/apache/tools/ant/util/ReaderInputStream;->begin:I

    :cond_2
    iget-object v2, p0, Lorg/apache/tools/ant/util/ReaderInputStream;->slack:[B

    if-nez v2, :cond_3

    new-array v0, p3, [C

    iget-object v2, p0, Lorg/apache/tools/ant/util/ReaderInputStream;->in:Ljava/io/Reader;

    invoke-virtual {v2, v0}, Ljava/io/Reader;->read([C)I

    move-result v1

    if-ne v1, v3, :cond_1

    move v2, v3

    goto :goto_0

    :cond_3
    iget-object v2, p0, Lorg/apache/tools/ant/util/ReaderInputStream;->slack:[B

    array-length v2, v2

    iget v3, p0, Lorg/apache/tools/ant/util/ReaderInputStream;->begin:I

    sub-int/2addr v2, v3

    if-le p3, v2, :cond_4

    iget-object v2, p0, Lorg/apache/tools/ant/util/ReaderInputStream;->slack:[B

    array-length v2, v2

    iget v3, p0, Lorg/apache/tools/ant/util/ReaderInputStream;->begin:I

    sub-int p3, v2, v3

    :cond_4
    iget-object v2, p0, Lorg/apache/tools/ant/util/ReaderInputStream;->slack:[B

    iget v3, p0, Lorg/apache/tools/ant/util/ReaderInputStream;->begin:I

    invoke-static {v2, v3, p1, p2, p3}, Ljava/lang/System;->arraycopy(Ljava/lang/Object;ILjava/lang/Object;II)V

    iget v2, p0, Lorg/apache/tools/ant/util/ReaderInputStream;->begin:I

    add-int/2addr v2, p3

    iput v2, p0, Lorg/apache/tools/ant/util/ReaderInputStream;->begin:I

    iget-object v3, p0, Lorg/apache/tools/ant/util/ReaderInputStream;->slack:[B

    array-length v3, v3

    if-lt v2, v3, :cond_5

    const/4 v2, 0x0

    iput-object v2, p0, Lorg/apache/tools/ant/util/ReaderInputStream;->slack:[B
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    :cond_5
    move v2, p3

    goto :goto_0
.end method

.method public declared-synchronized reset()V
    .locals 2
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    monitor-enter p0

    :try_start_0
    iget-object v0, p0, Lorg/apache/tools/ant/util/ReaderInputStream;->in:Ljava/io/Reader;

    if-nez v0, :cond_0

    new-instance v0, Ljava/io/IOException;

    const-string v1, "Stream Closed"

    invoke-direct {v0, v1}, Ljava/io/IOException;-><init>(Ljava/lang/String;)V

    throw v0
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0

    :cond_0
    const/4 v0, 0x0

    :try_start_1
    iput-object v0, p0, Lorg/apache/tools/ant/util/ReaderInputStream;->slack:[B

    iget-object v0, p0, Lorg/apache/tools/ant/util/ReaderInputStream;->in:Ljava/io/Reader;

    invoke-virtual {v0}, Ljava/io/Reader;->reset()V
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    monitor-exit p0

    return-void
.end method
