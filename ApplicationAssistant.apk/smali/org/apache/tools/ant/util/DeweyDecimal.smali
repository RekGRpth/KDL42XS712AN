.class public Lorg/apache/tools/ant/util/DeweyDecimal;
.super Ljava/lang/Object;
.source "DeweyDecimal.java"


# instance fields
.field private components:[I


# direct methods
.method public constructor <init>(Ljava/lang/String;)V
    .locals 6
    .param p1    # Ljava/lang/String;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/lang/NumberFormatException;
        }
    .end annotation

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    new-instance v3, Ljava/util/StringTokenizer;

    const-string v4, "."

    const/4 v5, 0x1

    invoke-direct {v3, p1, v4, v5}, Ljava/util/StringTokenizer;-><init>(Ljava/lang/String;Ljava/lang/String;Z)V

    invoke-virtual {v3}, Ljava/util/StringTokenizer;->countTokens()I

    move-result v2

    add-int/lit8 v4, v2, 0x1

    div-int/lit8 v4, v4, 0x2

    new-array v4, v4, [I

    iput-object v4, p0, Lorg/apache/tools/ant/util/DeweyDecimal;->components:[I

    const/4 v1, 0x0

    :goto_0
    iget-object v4, p0, Lorg/apache/tools/ant/util/DeweyDecimal;->components:[I

    array-length v4, v4

    if-ge v1, v4, :cond_2

    invoke-virtual {v3}, Ljava/util/StringTokenizer;->nextToken()Ljava/lang/String;

    move-result-object v0

    const-string v4, ""

    invoke-virtual {v0, v4}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v4

    if-eqz v4, :cond_0

    new-instance v4, Ljava/lang/NumberFormatException;

    const-string v5, "Empty component in string"

    invoke-direct {v4, v5}, Ljava/lang/NumberFormatException;-><init>(Ljava/lang/String;)V

    throw v4

    :cond_0
    iget-object v4, p0, Lorg/apache/tools/ant/util/DeweyDecimal;->components:[I

    invoke-static {v0}, Ljava/lang/Integer;->parseInt(Ljava/lang/String;)I

    move-result v5

    aput v5, v4, v1

    invoke-virtual {v3}, Ljava/util/StringTokenizer;->hasMoreTokens()Z

    move-result v4

    if-eqz v4, :cond_1

    invoke-virtual {v3}, Ljava/util/StringTokenizer;->nextToken()Ljava/lang/String;

    invoke-virtual {v3}, Ljava/util/StringTokenizer;->hasMoreTokens()Z

    move-result v4

    if-nez v4, :cond_1

    new-instance v4, Ljava/lang/NumberFormatException;

    const-string v5, "DeweyDecimal ended in a \'.\'"

    invoke-direct {v4, v5}, Ljava/lang/NumberFormatException;-><init>(Ljava/lang/String;)V

    throw v4

    :cond_1
    add-int/lit8 v1, v1, 0x1

    goto :goto_0

    :cond_2
    return-void
.end method

.method public constructor <init>([I)V
    .locals 3
    .param p1    # [I

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    array-length v1, p1

    new-array v1, v1, [I

    iput-object v1, p0, Lorg/apache/tools/ant/util/DeweyDecimal;->components:[I

    const/4 v0, 0x0

    :goto_0
    array-length v1, p1

    if-ge v0, v1, :cond_0

    iget-object v1, p0, Lorg/apache/tools/ant/util/DeweyDecimal;->components:[I

    aget v2, p1, v0

    aput v2, v1, v0

    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    :cond_0
    return-void
.end method


# virtual methods
.method public get(I)I
    .locals 1
    .param p1    # I

    iget-object v0, p0, Lorg/apache/tools/ant/util/DeweyDecimal;->components:[I

    aget v0, v0, p1

    return v0
.end method

.method public getSize()I
    .locals 1

    iget-object v0, p0, Lorg/apache/tools/ant/util/DeweyDecimal;->components:[I

    array-length v0, v0

    return v0
.end method

.method public isEqual(Lorg/apache/tools/ant/util/DeweyDecimal;)Z
    .locals 7
    .param p1    # Lorg/apache/tools/ant/util/DeweyDecimal;

    const/4 v4, 0x0

    iget-object v5, p1, Lorg/apache/tools/ant/util/DeweyDecimal;->components:[I

    array-length v5, v5

    iget-object v6, p0, Lorg/apache/tools/ant/util/DeweyDecimal;->components:[I

    array-length v6, v6

    invoke-static {v5, v6}, Ljava/lang/Math;->max(II)I

    move-result v3

    const/4 v2, 0x0

    :goto_0
    if-ge v2, v3, :cond_3

    iget-object v5, p0, Lorg/apache/tools/ant/util/DeweyDecimal;->components:[I

    array-length v5, v5

    if-ge v2, v5, :cond_0

    iget-object v5, p0, Lorg/apache/tools/ant/util/DeweyDecimal;->components:[I

    aget v0, v5, v2

    :goto_1
    iget-object v5, p1, Lorg/apache/tools/ant/util/DeweyDecimal;->components:[I

    array-length v5, v5

    if-ge v2, v5, :cond_1

    iget-object v5, p1, Lorg/apache/tools/ant/util/DeweyDecimal;->components:[I

    aget v1, v5, v2

    :goto_2
    if-eq v1, v0, :cond_2

    :goto_3
    return v4

    :cond_0
    move v0, v4

    goto :goto_1

    :cond_1
    move v1, v4

    goto :goto_2

    :cond_2
    add-int/lit8 v2, v2, 0x1

    goto :goto_0

    :cond_3
    const/4 v4, 0x1

    goto :goto_3
.end method

.method public isGreaterThan(Lorg/apache/tools/ant/util/DeweyDecimal;)Z
    .locals 7
    .param p1    # Lorg/apache/tools/ant/util/DeweyDecimal;

    const/4 v4, 0x0

    iget-object v5, p1, Lorg/apache/tools/ant/util/DeweyDecimal;->components:[I

    array-length v5, v5

    iget-object v6, p0, Lorg/apache/tools/ant/util/DeweyDecimal;->components:[I

    array-length v6, v6

    invoke-static {v5, v6}, Ljava/lang/Math;->max(II)I

    move-result v3

    const/4 v2, 0x0

    :goto_0
    if-ge v2, v3, :cond_0

    iget-object v5, p0, Lorg/apache/tools/ant/util/DeweyDecimal;->components:[I

    array-length v5, v5

    if-ge v2, v5, :cond_1

    iget-object v5, p0, Lorg/apache/tools/ant/util/DeweyDecimal;->components:[I

    aget v0, v5, v2

    :goto_1
    iget-object v5, p1, Lorg/apache/tools/ant/util/DeweyDecimal;->components:[I

    array-length v5, v5

    if-ge v2, v5, :cond_2

    iget-object v5, p1, Lorg/apache/tools/ant/util/DeweyDecimal;->components:[I

    aget v1, v5, v2

    :goto_2
    if-le v1, v0, :cond_3

    :cond_0
    :goto_3
    return v4

    :cond_1
    move v0, v4

    goto :goto_1

    :cond_2
    move v1, v4

    goto :goto_2

    :cond_3
    if-ge v1, v0, :cond_4

    const/4 v4, 0x1

    goto :goto_3

    :cond_4
    add-int/lit8 v2, v2, 0x1

    goto :goto_0
.end method

.method public isGreaterThanOrEqual(Lorg/apache/tools/ant/util/DeweyDecimal;)Z
    .locals 8
    .param p1    # Lorg/apache/tools/ant/util/DeweyDecimal;

    const/4 v5, 0x1

    const/4 v4, 0x0

    iget-object v6, p1, Lorg/apache/tools/ant/util/DeweyDecimal;->components:[I

    array-length v6, v6

    iget-object v7, p0, Lorg/apache/tools/ant/util/DeweyDecimal;->components:[I

    array-length v7, v7

    invoke-static {v6, v7}, Ljava/lang/Math;->max(II)I

    move-result v3

    const/4 v2, 0x0

    :goto_0
    if-ge v2, v3, :cond_4

    iget-object v6, p0, Lorg/apache/tools/ant/util/DeweyDecimal;->components:[I

    array-length v6, v6

    if-ge v2, v6, :cond_0

    iget-object v6, p0, Lorg/apache/tools/ant/util/DeweyDecimal;->components:[I

    aget v0, v6, v2

    :goto_1
    iget-object v6, p1, Lorg/apache/tools/ant/util/DeweyDecimal;->components:[I

    array-length v6, v6

    if-ge v2, v6, :cond_1

    iget-object v6, p1, Lorg/apache/tools/ant/util/DeweyDecimal;->components:[I

    aget v1, v6, v2

    :goto_2
    if-le v1, v0, :cond_2

    :goto_3
    return v4

    :cond_0
    move v0, v4

    goto :goto_1

    :cond_1
    move v1, v4

    goto :goto_2

    :cond_2
    if-ge v1, v0, :cond_3

    move v4, v5

    goto :goto_3

    :cond_3
    add-int/lit8 v2, v2, 0x1

    goto :goto_0

    :cond_4
    move v4, v5

    goto :goto_3
.end method

.method public isLessThan(Lorg/apache/tools/ant/util/DeweyDecimal;)Z
    .locals 1
    .param p1    # Lorg/apache/tools/ant/util/DeweyDecimal;

    invoke-virtual {p0, p1}, Lorg/apache/tools/ant/util/DeweyDecimal;->isGreaterThanOrEqual(Lorg/apache/tools/ant/util/DeweyDecimal;)Z

    move-result v0

    if-nez v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public isLessThanOrEqual(Lorg/apache/tools/ant/util/DeweyDecimal;)Z
    .locals 1
    .param p1    # Lorg/apache/tools/ant/util/DeweyDecimal;

    invoke-virtual {p0, p1}, Lorg/apache/tools/ant/util/DeweyDecimal;->isGreaterThan(Lorg/apache/tools/ant/util/DeweyDecimal;)Z

    move-result v0

    if-nez v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public toString()Ljava/lang/String;
    .locals 3

    new-instance v1, Ljava/lang/StringBuffer;

    invoke-direct {v1}, Ljava/lang/StringBuffer;-><init>()V

    const/4 v0, 0x0

    :goto_0
    iget-object v2, p0, Lorg/apache/tools/ant/util/DeweyDecimal;->components:[I

    array-length v2, v2

    if-ge v0, v2, :cond_1

    if-eqz v0, :cond_0

    const/16 v2, 0x2e

    invoke-virtual {v1, v2}, Ljava/lang/StringBuffer;->append(C)Ljava/lang/StringBuffer;

    :cond_0
    iget-object v2, p0, Lorg/apache/tools/ant/util/DeweyDecimal;->components:[I

    aget v2, v2, v0

    invoke-virtual {v1, v2}, Ljava/lang/StringBuffer;->append(I)Ljava/lang/StringBuffer;

    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    :cond_1
    invoke-virtual {v1}, Ljava/lang/StringBuffer;->toString()Ljava/lang/String;

    move-result-object v2

    return-object v2
.end method
