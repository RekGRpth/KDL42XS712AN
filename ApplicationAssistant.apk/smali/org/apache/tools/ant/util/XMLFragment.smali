.class public Lorg/apache/tools/ant/util/XMLFragment;
.super Lorg/apache/tools/ant/ProjectComponent;
.source "XMLFragment.java"

# interfaces
.implements Lorg/apache/tools/ant/DynamicElementNS;


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lorg/apache/tools/ant/util/XMLFragment$Child;
    }
.end annotation


# instance fields
.field private doc:Lorg/w3c/dom/Document;

.field private fragment:Lorg/w3c/dom/DocumentFragment;


# direct methods
.method public constructor <init>()V
    .locals 1

    invoke-direct {p0}, Lorg/apache/tools/ant/ProjectComponent;-><init>()V

    invoke-static {}, Lorg/apache/tools/ant/util/JAXPUtils;->getDocumentBuilder()Ljavax/xml/parsers/DocumentBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljavax/xml/parsers/DocumentBuilder;->newDocument()Lorg/w3c/dom/Document;

    move-result-object v0

    iput-object v0, p0, Lorg/apache/tools/ant/util/XMLFragment;->doc:Lorg/w3c/dom/Document;

    iget-object v0, p0, Lorg/apache/tools/ant/util/XMLFragment;->doc:Lorg/w3c/dom/Document;

    invoke-interface {v0}, Lorg/w3c/dom/Document;->createDocumentFragment()Lorg/w3c/dom/DocumentFragment;

    move-result-object v0

    iput-object v0, p0, Lorg/apache/tools/ant/util/XMLFragment;->fragment:Lorg/w3c/dom/DocumentFragment;

    return-void
.end method

.method static access$000(Lorg/apache/tools/ant/util/XMLFragment;Lorg/w3c/dom/Node;Ljava/lang/String;)V
    .locals 0
    .param p0    # Lorg/apache/tools/ant/util/XMLFragment;
    .param p1    # Lorg/w3c/dom/Node;
    .param p2    # Ljava/lang/String;

    invoke-direct {p0, p1, p2}, Lorg/apache/tools/ant/util/XMLFragment;->addText(Lorg/w3c/dom/Node;Ljava/lang/String;)V

    return-void
.end method

.method static access$100(Lorg/apache/tools/ant/util/XMLFragment;)Lorg/w3c/dom/Document;
    .locals 1
    .param p0    # Lorg/apache/tools/ant/util/XMLFragment;

    iget-object v0, p0, Lorg/apache/tools/ant/util/XMLFragment;->doc:Lorg/w3c/dom/Document;

    return-object v0
.end method

.method private addText(Lorg/w3c/dom/Node;Ljava/lang/String;)V
    .locals 3
    .param p1    # Lorg/w3c/dom/Node;
    .param p2    # Ljava/lang/String;

    invoke-virtual {p0}, Lorg/apache/tools/ant/util/XMLFragment;->getProject()Lorg/apache/tools/ant/Project;

    move-result-object v1

    invoke-virtual {v1, p2}, Lorg/apache/tools/ant/Project;->replaceProperties(Ljava/lang/String;)Ljava/lang/String;

    move-result-object p2

    if-eqz p2, :cond_0

    invoke-virtual {p2}, Ljava/lang/String;->trim()Ljava/lang/String;

    move-result-object v1

    const-string v2, ""

    invoke-virtual {v1, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-nez v1, :cond_0

    iget-object v1, p0, Lorg/apache/tools/ant/util/XMLFragment;->doc:Lorg/w3c/dom/Document;

    invoke-virtual {p2}, Ljava/lang/String;->trim()Ljava/lang/String;

    move-result-object v2

    invoke-interface {v1, v2}, Lorg/w3c/dom/Document;->createTextNode(Ljava/lang/String;)Lorg/w3c/dom/Text;

    move-result-object v0

    invoke-interface {p1, v0}, Lorg/w3c/dom/Node;->appendChild(Lorg/w3c/dom/Node;)Lorg/w3c/dom/Node;

    :cond_0
    return-void
.end method


# virtual methods
.method public addText(Ljava/lang/String;)V
    .locals 1
    .param p1    # Ljava/lang/String;

    iget-object v0, p0, Lorg/apache/tools/ant/util/XMLFragment;->fragment:Lorg/w3c/dom/DocumentFragment;

    invoke-direct {p0, v0, p1}, Lorg/apache/tools/ant/util/XMLFragment;->addText(Lorg/w3c/dom/Node;Ljava/lang/String;)V

    return-void
.end method

.method public createDynamicElement(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)Ljava/lang/Object;
    .locals 2
    .param p1    # Ljava/lang/String;
    .param p2    # Ljava/lang/String;
    .param p3    # Ljava/lang/String;

    const/4 v0, 0x0

    const-string v1, ""

    invoke-virtual {p1, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_0

    iget-object v1, p0, Lorg/apache/tools/ant/util/XMLFragment;->doc:Lorg/w3c/dom/Document;

    invoke-interface {v1, p2}, Lorg/w3c/dom/Document;->createElement(Ljava/lang/String;)Lorg/w3c/dom/Element;

    move-result-object v0

    :goto_0
    iget-object v1, p0, Lorg/apache/tools/ant/util/XMLFragment;->fragment:Lorg/w3c/dom/DocumentFragment;

    invoke-interface {v1, v0}, Lorg/w3c/dom/DocumentFragment;->appendChild(Lorg/w3c/dom/Node;)Lorg/w3c/dom/Node;

    new-instance v1, Lorg/apache/tools/ant/util/XMLFragment$Child;

    invoke-direct {v1, p0, v0}, Lorg/apache/tools/ant/util/XMLFragment$Child;-><init>(Lorg/apache/tools/ant/util/XMLFragment;Lorg/w3c/dom/Element;)V

    return-object v1

    :cond_0
    iget-object v1, p0, Lorg/apache/tools/ant/util/XMLFragment;->doc:Lorg/w3c/dom/Document;

    invoke-interface {v1, p1, p3}, Lorg/w3c/dom/Document;->createElementNS(Ljava/lang/String;Ljava/lang/String;)Lorg/w3c/dom/Element;

    move-result-object v0

    goto :goto_0
.end method

.method public getFragment()Lorg/w3c/dom/DocumentFragment;
    .locals 1

    iget-object v0, p0, Lorg/apache/tools/ant/util/XMLFragment;->fragment:Lorg/w3c/dom/DocumentFragment;

    return-object v0
.end method
