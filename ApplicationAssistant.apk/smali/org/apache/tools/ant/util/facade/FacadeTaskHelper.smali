.class public Lorg/apache/tools/ant/util/facade/FacadeTaskHelper;
.super Ljava/lang/Object;
.source "FacadeTaskHelper.java"


# instance fields
.field private args:Ljava/util/Vector;

.field private defaultValue:Ljava/lang/String;

.field private magicValue:Ljava/lang/String;

.field private userChoice:Ljava/lang/String;


# direct methods
.method public constructor <init>(Ljava/lang/String;)V
    .locals 1
    .param p1    # Ljava/lang/String;

    const/4 v0, 0x0

    invoke-direct {p0, p1, v0}, Lorg/apache/tools/ant/util/facade/FacadeTaskHelper;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    return-void
.end method

.method public constructor <init>(Ljava/lang/String;Ljava/lang/String;)V
    .locals 1
    .param p1    # Ljava/lang/String;
    .param p2    # Ljava/lang/String;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    new-instance v0, Ljava/util/Vector;

    invoke-direct {v0}, Ljava/util/Vector;-><init>()V

    iput-object v0, p0, Lorg/apache/tools/ant/util/facade/FacadeTaskHelper;->args:Ljava/util/Vector;

    iput-object p1, p0, Lorg/apache/tools/ant/util/facade/FacadeTaskHelper;->defaultValue:Ljava/lang/String;

    iput-object p2, p0, Lorg/apache/tools/ant/util/facade/FacadeTaskHelper;->magicValue:Ljava/lang/String;

    return-void
.end method


# virtual methods
.method public addImplementationArgument(Lorg/apache/tools/ant/util/facade/ImplementationSpecificArgument;)V
    .locals 1
    .param p1    # Lorg/apache/tools/ant/util/facade/ImplementationSpecificArgument;

    iget-object v0, p0, Lorg/apache/tools/ant/util/facade/FacadeTaskHelper;->args:Ljava/util/Vector;

    invoke-virtual {v0, p1}, Ljava/util/Vector;->addElement(Ljava/lang/Object;)V

    return-void
.end method

.method public getArgs()[Ljava/lang/String;
    .locals 7

    new-instance v5, Ljava/util/Vector;

    iget-object v6, p0, Lorg/apache/tools/ant/util/facade/FacadeTaskHelper;->args:Ljava/util/Vector;

    invoke-virtual {v6}, Ljava/util/Vector;->size()I

    move-result v6

    invoke-direct {v5, v6}, Ljava/util/Vector;-><init>(I)V

    iget-object v6, p0, Lorg/apache/tools/ant/util/facade/FacadeTaskHelper;->args:Ljava/util/Vector;

    invoke-virtual {v6}, Ljava/util/Vector;->elements()Ljava/util/Enumeration;

    move-result-object v2

    :cond_0
    invoke-interface {v2}, Ljava/util/Enumeration;->hasMoreElements()Z

    move-result v6

    if-eqz v6, :cond_1

    invoke-interface {v2}, Ljava/util/Enumeration;->nextElement()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lorg/apache/tools/ant/util/facade/ImplementationSpecificArgument;

    invoke-virtual {p0}, Lorg/apache/tools/ant/util/facade/FacadeTaskHelper;->getImplementation()Ljava/lang/String;

    move-result-object v6

    invoke-virtual {v0, v6}, Lorg/apache/tools/ant/util/facade/ImplementationSpecificArgument;->getParts(Ljava/lang/String;)[Ljava/lang/String;

    move-result-object v1

    const/4 v3, 0x0

    :goto_0
    array-length v6, v1

    if-ge v3, v6, :cond_0

    aget-object v6, v1, v3

    invoke-virtual {v5, v6}, Ljava/util/Vector;->addElement(Ljava/lang/Object;)V

    add-int/lit8 v3, v3, 0x1

    goto :goto_0

    :cond_1
    invoke-virtual {v5}, Ljava/util/Vector;->size()I

    move-result v6

    new-array v4, v6, [Ljava/lang/String;

    invoke-virtual {v5, v4}, Ljava/util/Vector;->copyInto([Ljava/lang/Object;)V

    return-object v4
.end method

.method public getExplicitChoice()Ljava/lang/String;
    .locals 1

    iget-object v0, p0, Lorg/apache/tools/ant/util/facade/FacadeTaskHelper;->userChoice:Ljava/lang/String;

    return-object v0
.end method

.method public getImplementation()Ljava/lang/String;
    .locals 1

    iget-object v0, p0, Lorg/apache/tools/ant/util/facade/FacadeTaskHelper;->userChoice:Ljava/lang/String;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lorg/apache/tools/ant/util/facade/FacadeTaskHelper;->userChoice:Ljava/lang/String;

    :goto_0
    return-object v0

    :cond_0
    iget-object v0, p0, Lorg/apache/tools/ant/util/facade/FacadeTaskHelper;->magicValue:Ljava/lang/String;

    if-eqz v0, :cond_1

    iget-object v0, p0, Lorg/apache/tools/ant/util/facade/FacadeTaskHelper;->magicValue:Ljava/lang/String;

    goto :goto_0

    :cond_1
    iget-object v0, p0, Lorg/apache/tools/ant/util/facade/FacadeTaskHelper;->defaultValue:Ljava/lang/String;

    goto :goto_0
.end method

.method public hasBeenSet()Z
    .locals 1

    iget-object v0, p0, Lorg/apache/tools/ant/util/facade/FacadeTaskHelper;->userChoice:Ljava/lang/String;

    if-nez v0, :cond_0

    iget-object v0, p0, Lorg/apache/tools/ant/util/facade/FacadeTaskHelper;->magicValue:Ljava/lang/String;

    if-eqz v0, :cond_1

    :cond_0
    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_1
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public setImplementation(Ljava/lang/String;)V
    .locals 0
    .param p1    # Ljava/lang/String;

    iput-object p1, p0, Lorg/apache/tools/ant/util/facade/FacadeTaskHelper;->userChoice:Ljava/lang/String;

    return-void
.end method

.method public setMagicValue(Ljava/lang/String;)V
    .locals 0
    .param p1    # Ljava/lang/String;

    iput-object p1, p0, Lorg/apache/tools/ant/util/facade/FacadeTaskHelper;->magicValue:Ljava/lang/String;

    return-void
.end method
