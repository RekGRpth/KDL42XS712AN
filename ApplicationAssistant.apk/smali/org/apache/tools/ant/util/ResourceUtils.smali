.class public Lorg/apache/tools/ant/util/ResourceUtils;
.super Ljava/lang/Object;
.source "ResourceUtils.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lorg/apache/tools/ant/util/ResourceUtils$1;,
        Lorg/apache/tools/ant/util/ResourceUtils$Outdated;
    }
.end annotation


# static fields
.field private static final FILE_UTILS:Lorg/apache/tools/ant/util/FileUtils;

.field private static final NOT_EXISTS:Lorg/apache/tools/ant/types/resources/selectors/ResourceSelector;


# direct methods
.method static constructor <clinit>()V
    .locals 2

    invoke-static {}, Lorg/apache/tools/ant/util/FileUtils;->getFileUtils()Lorg/apache/tools/ant/util/FileUtils;

    move-result-object v0

    sput-object v0, Lorg/apache/tools/ant/util/ResourceUtils;->FILE_UTILS:Lorg/apache/tools/ant/util/FileUtils;

    new-instance v0, Lorg/apache/tools/ant/types/resources/selectors/Not;

    new-instance v1, Lorg/apache/tools/ant/types/resources/selectors/Exists;

    invoke-direct {v1}, Lorg/apache/tools/ant/types/resources/selectors/Exists;-><init>()V

    invoke-direct {v0, v1}, Lorg/apache/tools/ant/types/resources/selectors/Not;-><init>(Lorg/apache/tools/ant/types/resources/selectors/ResourceSelector;)V

    sput-object v0, Lorg/apache/tools/ant/util/ResourceUtils;->NOT_EXISTS:Lorg/apache/tools/ant/types/resources/selectors/ResourceSelector;

    return-void
.end method

.method public constructor <init>()V
    .locals 0

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method private static binaryCompare(Lorg/apache/tools/ant/types/Resource;Lorg/apache/tools/ant/types/Resource;)I
    .locals 8
    .param p0    # Lorg/apache/tools/ant/types/Resource;
    .param p1    # Lorg/apache/tools/ant/types/Resource;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    const/4 v6, -0x1

    const/4 v2, 0x0

    const/4 v4, 0x0

    :try_start_0
    new-instance v3, Ljava/io/BufferedInputStream;

    invoke-virtual {p0}, Lorg/apache/tools/ant/types/Resource;->getInputStream()Ljava/io/InputStream;

    move-result-object v7

    invoke-direct {v3, v7}, Ljava/io/BufferedInputStream;-><init>(Ljava/io/InputStream;)V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    :try_start_1
    new-instance v5, Ljava/io/BufferedInputStream;

    invoke-virtual {p1}, Lorg/apache/tools/ant/types/Resource;->getInputStream()Ljava/io/InputStream;

    move-result-object v7

    invoke-direct {v5, v7}, Ljava/io/BufferedInputStream;-><init>(Ljava/io/InputStream;)V
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_1

    :try_start_2
    invoke-virtual {v3}, Ljava/io/InputStream;->read()I

    move-result v0

    :goto_0
    if-eq v0, v6, :cond_2

    invoke-virtual {v5}, Ljava/io/InputStream;->read()I
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_2

    move-result v1

    if-eq v0, v1, :cond_1

    if-le v0, v1, :cond_0

    const/4 v6, 0x1

    :cond_0
    invoke-static {v3}, Lorg/apache/tools/ant/util/FileUtils;->close(Ljava/io/InputStream;)V

    invoke-static {v5}, Lorg/apache/tools/ant/util/FileUtils;->close(Ljava/io/InputStream;)V

    :goto_1
    return v6

    :cond_1
    :try_start_3
    invoke-virtual {v3}, Ljava/io/InputStream;->read()I

    move-result v0

    goto :goto_0

    :cond_2
    invoke-virtual {v5}, Ljava/io/InputStream;->read()I
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_2

    move-result v7

    if-ne v7, v6, :cond_3

    const/4 v6, 0x0

    :cond_3
    invoke-static {v3}, Lorg/apache/tools/ant/util/FileUtils;->close(Ljava/io/InputStream;)V

    invoke-static {v5}, Lorg/apache/tools/ant/util/FileUtils;->close(Ljava/io/InputStream;)V

    goto :goto_1

    :catchall_0
    move-exception v6

    :goto_2
    invoke-static {v2}, Lorg/apache/tools/ant/util/FileUtils;->close(Ljava/io/InputStream;)V

    invoke-static {v4}, Lorg/apache/tools/ant/util/FileUtils;->close(Ljava/io/InputStream;)V

    throw v6

    :catchall_1
    move-exception v6

    move-object v2, v3

    goto :goto_2

    :catchall_2
    move-exception v6

    move-object v4, v5

    move-object v2, v3

    goto :goto_2
.end method

.method public static compareContent(Lorg/apache/tools/ant/types/Resource;Lorg/apache/tools/ant/types/Resource;Z)I
    .locals 8
    .param p0    # Lorg/apache/tools/ant/types/Resource;
    .param p1    # Lorg/apache/tools/ant/types/Resource;
    .param p2    # Z
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    const/4 v4, 0x1

    const/4 v5, -0x1

    const/4 v6, 0x0

    invoke-virtual {p0, p1}, Lorg/apache/tools/ant/types/Resource;->equals(Ljava/lang/Object;)Z

    move-result v7

    if-eqz v7, :cond_1

    move v4, v6

    :cond_0
    :goto_0
    return v4

    :cond_1
    invoke-virtual {p0}, Lorg/apache/tools/ant/types/Resource;->isExists()Z

    move-result v2

    invoke-virtual {p1}, Lorg/apache/tools/ant/types/Resource;->isExists()Z

    move-result v3

    if-nez v2, :cond_2

    if-nez v3, :cond_2

    move v4, v6

    goto :goto_0

    :cond_2
    if-eq v2, v3, :cond_3

    if-nez v2, :cond_0

    move v4, v5

    goto :goto_0

    :cond_3
    invoke-virtual {p0}, Lorg/apache/tools/ant/types/Resource;->isDirectory()Z

    move-result v0

    invoke-virtual {p1}, Lorg/apache/tools/ant/types/Resource;->isDirectory()Z

    move-result v1

    if-eqz v0, :cond_4

    if-eqz v1, :cond_4

    move v4, v6

    goto :goto_0

    :cond_4
    if-nez v0, :cond_5

    if-eqz v1, :cond_7

    :cond_5
    if-eqz v0, :cond_6

    :goto_1
    move v4, v5

    goto :goto_0

    :cond_6
    move v5, v4

    goto :goto_1

    :cond_7
    if-eqz p2, :cond_8

    invoke-static {p0, p1}, Lorg/apache/tools/ant/util/ResourceUtils;->textCompare(Lorg/apache/tools/ant/types/Resource;Lorg/apache/tools/ant/types/Resource;)I

    move-result v4

    goto :goto_0

    :cond_8
    invoke-static {p0, p1}, Lorg/apache/tools/ant/util/ResourceUtils;->binaryCompare(Lorg/apache/tools/ant/types/Resource;Lorg/apache/tools/ant/types/Resource;)I

    move-result v4

    goto :goto_0
.end method

.method public static contentEquals(Lorg/apache/tools/ant/types/Resource;Lorg/apache/tools/ant/types/Resource;Z)Z
    .locals 6
    .param p0    # Lorg/apache/tools/ant/types/Resource;
    .param p1    # Lorg/apache/tools/ant/types/Resource;
    .param p2    # Z
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    const/4 v0, 0x1

    const/4 v1, 0x0

    invoke-virtual {p0}, Lorg/apache/tools/ant/types/Resource;->isExists()Z

    move-result v2

    invoke-virtual {p1}, Lorg/apache/tools/ant/types/Resource;->isExists()Z

    move-result v3

    if-eq v2, v3, :cond_1

    :cond_0
    :goto_0
    return v1

    :cond_1
    invoke-virtual {p0}, Lorg/apache/tools/ant/types/Resource;->isExists()Z

    move-result v2

    if-nez v2, :cond_2

    move v1, v0

    goto :goto_0

    :cond_2
    invoke-virtual {p0}, Lorg/apache/tools/ant/types/Resource;->isDirectory()Z

    move-result v2

    if-nez v2, :cond_0

    invoke-virtual {p1}, Lorg/apache/tools/ant/types/Resource;->isDirectory()Z

    move-result v2

    if-nez v2, :cond_0

    invoke-virtual {p0, p1}, Lorg/apache/tools/ant/types/Resource;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_3

    move v1, v0

    goto :goto_0

    :cond_3
    if-nez p2, :cond_4

    invoke-virtual {p0}, Lorg/apache/tools/ant/types/Resource;->getSize()J

    move-result-wide v2

    invoke-virtual {p1}, Lorg/apache/tools/ant/types/Resource;->getSize()J

    move-result-wide v4

    cmp-long v2, v2, v4

    if-nez v2, :cond_0

    :cond_4
    invoke-static {p0, p1, p2}, Lorg/apache/tools/ant/util/ResourceUtils;->compareContent(Lorg/apache/tools/ant/types/Resource;Lorg/apache/tools/ant/types/Resource;Z)I

    move-result v2

    if-nez v2, :cond_5

    :goto_1
    move v1, v0

    goto :goto_0

    :cond_5
    move v0, v1

    goto :goto_1
.end method

.method public static copyResource(Lorg/apache/tools/ant/types/Resource;Lorg/apache/tools/ant/types/Resource;)V
    .locals 1
    .param p0    # Lorg/apache/tools/ant/types/Resource;
    .param p1    # Lorg/apache/tools/ant/types/Resource;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    const/4 v0, 0x0

    invoke-static {p0, p1, v0}, Lorg/apache/tools/ant/util/ResourceUtils;->copyResource(Lorg/apache/tools/ant/types/Resource;Lorg/apache/tools/ant/types/Resource;Lorg/apache/tools/ant/Project;)V

    return-void
.end method

.method public static copyResource(Lorg/apache/tools/ant/types/Resource;Lorg/apache/tools/ant/types/Resource;Lorg/apache/tools/ant/Project;)V
    .locals 9
    .param p0    # Lorg/apache/tools/ant/types/Resource;
    .param p1    # Lorg/apache/tools/ant/types/Resource;
    .param p2    # Lorg/apache/tools/ant/Project;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    const/4 v4, 0x0

    const/4 v2, 0x0

    move-object v0, p0

    move-object v1, p1

    move-object v3, v2

    move v5, v4

    move-object v6, v2

    move-object v7, v2

    move-object v8, p2

    invoke-static/range {v0 .. v8}, Lorg/apache/tools/ant/util/ResourceUtils;->copyResource(Lorg/apache/tools/ant/types/Resource;Lorg/apache/tools/ant/types/Resource;Lorg/apache/tools/ant/types/FilterSetCollection;Ljava/util/Vector;ZZLjava/lang/String;Ljava/lang/String;Lorg/apache/tools/ant/Project;)V

    return-void
.end method

.method public static copyResource(Lorg/apache/tools/ant/types/Resource;Lorg/apache/tools/ant/types/Resource;Lorg/apache/tools/ant/types/FilterSetCollection;Ljava/util/Vector;ZZLjava/lang/String;Ljava/lang/String;Lorg/apache/tools/ant/Project;)V
    .locals 24
    .param p0    # Lorg/apache/tools/ant/types/Resource;
    .param p1    # Lorg/apache/tools/ant/types/Resource;
    .param p2    # Lorg/apache/tools/ant/types/FilterSetCollection;
    .param p3    # Ljava/util/Vector;
    .param p4    # Z
    .param p5    # Z
    .param p6    # Ljava/lang/String;
    .param p7    # Ljava/lang/String;
    .param p8    # Lorg/apache/tools/ant/Project;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    if-nez p4, :cond_1

    invoke-virtual/range {p0 .. p0}, Lorg/apache/tools/ant/types/Resource;->getLastModified()J

    move-result-wide v20

    invoke-virtual/range {p1 .. p1}, Lorg/apache/tools/ant/types/Resource;->isExists()Z

    move-result v22

    if-eqz v22, :cond_1

    const-wide/16 v22, 0x0

    cmp-long v22, v20, v22

    if-eqz v22, :cond_1

    invoke-virtual/range {p1 .. p1}, Lorg/apache/tools/ant/types/Resource;->getLastModified()J

    move-result-wide v22

    cmp-long v22, v22, v20

    if-lez v22, :cond_1

    :cond_0
    :goto_0
    return-void

    :cond_1
    if-eqz p2, :cond_2

    invoke-virtual/range {p2 .. p2}, Lorg/apache/tools/ant/types/FilterSetCollection;->hasFilters()Z

    move-result v22

    if-eqz v22, :cond_2

    const/4 v7, 0x1

    :goto_1
    if-eqz p3, :cond_3

    invoke-virtual/range {p3 .. p3}, Ljava/util/Vector;->size()I

    move-result v22

    if-lez v22, :cond_3

    const/4 v6, 0x1

    :goto_2
    if-eqz v7, :cond_8

    const/4 v8, 0x0

    const/16 v17, 0x0

    const/4 v10, 0x0

    if-nez p6, :cond_4

    :try_start_0
    new-instance v11, Ljava/io/InputStreamReader;

    invoke-virtual/range {p0 .. p0}, Lorg/apache/tools/ant/types/Resource;->getInputStream()Ljava/io/InputStream;

    move-result-object v22

    move-object/from16 v0, v22

    invoke-direct {v11, v0}, Ljava/io/InputStreamReader;-><init>(Ljava/io/InputStream;)V

    move-object v10, v11

    :goto_3
    new-instance v9, Ljava/io/BufferedReader;

    invoke-direct {v9, v10}, Ljava/io/BufferedReader;-><init>(Ljava/io/Reader;)V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_6

    const/16 v16, 0x0

    if-nez p7, :cond_5

    :try_start_1
    new-instance v16, Ljava/io/OutputStreamWriter;

    invoke-virtual/range {p1 .. p1}, Lorg/apache/tools/ant/types/Resource;->getOutputStream()Ljava/io/OutputStream;

    move-result-object v22

    move-object/from16 v0, v16

    move-object/from16 v1, v22

    invoke-direct {v0, v1}, Ljava/io/OutputStreamWriter;-><init>(Ljava/io/OutputStream;)V

    :goto_4
    new-instance v18, Ljava/io/BufferedWriter;

    move-object/from16 v0, v18

    move-object/from16 v1, v16

    invoke-direct {v0, v1}, Ljava/io/BufferedWriter;-><init>(Ljava/io/Writer;)V
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_7

    if-eqz v6, :cond_11

    :try_start_2
    new-instance v5, Lorg/apache/tools/ant/filters/util/ChainReaderHelper;

    invoke-direct {v5}, Lorg/apache/tools/ant/filters/util/ChainReaderHelper;-><init>()V

    const/16 v22, 0x2000

    move/from16 v0, v22

    invoke-virtual {v5, v0}, Lorg/apache/tools/ant/filters/util/ChainReaderHelper;->setBufferSize(I)V

    invoke-virtual {v5, v9}, Lorg/apache/tools/ant/filters/util/ChainReaderHelper;->setPrimaryReader(Ljava/io/Reader;)V

    move-object/from16 v0, p3

    invoke-virtual {v5, v0}, Lorg/apache/tools/ant/filters/util/ChainReaderHelper;->setFilterChains(Ljava/util/Vector;)V

    move-object/from16 v0, p8

    invoke-virtual {v5, v0}, Lorg/apache/tools/ant/filters/util/ChainReaderHelper;->setProject(Lorg/apache/tools/ant/Project;)V

    invoke-virtual {v5}, Lorg/apache/tools/ant/filters/util/ChainReaderHelper;->getAssembledReader()Ljava/io/Reader;

    move-result-object v19

    new-instance v8, Ljava/io/BufferedReader;

    move-object/from16 v0, v19

    invoke-direct {v8, v0}, Ljava/io/BufferedReader;-><init>(Ljava/io/Reader;)V
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_8

    :goto_5
    :try_start_3
    new-instance v13, Lorg/apache/tools/ant/util/LineTokenizer;

    invoke-direct {v13}, Lorg/apache/tools/ant/util/LineTokenizer;-><init>()V

    const/16 v22, 0x1

    move/from16 v0, v22

    invoke-virtual {v13, v0}, Lorg/apache/tools/ant/util/LineTokenizer;->setIncludeDelims(Z)V

    const/4 v15, 0x0

    invoke-virtual {v13, v8}, Lorg/apache/tools/ant/util/LineTokenizer;->getToken(Ljava/io/Reader;)Ljava/lang/String;

    move-result-object v12

    :goto_6
    if-eqz v12, :cond_7

    invoke-virtual {v12}, Ljava/lang/String;->length()I

    move-result v22

    if-nez v22, :cond_6

    invoke-virtual/range {v18 .. v18}, Ljava/io/BufferedWriter;->newLine()V

    :goto_7
    invoke-virtual {v13, v8}, Lorg/apache/tools/ant/util/LineTokenizer;->getToken(Ljava/io/Reader;)Ljava/lang/String;
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_0

    move-result-object v12

    goto :goto_6

    :cond_2
    const/4 v7, 0x0

    goto/16 :goto_1

    :cond_3
    const/4 v6, 0x0

    goto :goto_2

    :cond_4
    :try_start_4
    new-instance v11, Ljava/io/InputStreamReader;

    invoke-virtual/range {p0 .. p0}, Lorg/apache/tools/ant/types/Resource;->getInputStream()Ljava/io/InputStream;

    move-result-object v22

    move-object/from16 v0, v22

    move-object/from16 v1, p6

    invoke-direct {v11, v0, v1}, Ljava/io/InputStreamReader;-><init>(Ljava/io/InputStream;Ljava/lang/String;)V
    :try_end_4
    .catchall {:try_start_4 .. :try_end_4} :catchall_6

    move-object v10, v11

    goto :goto_3

    :cond_5
    :try_start_5
    new-instance v16, Ljava/io/OutputStreamWriter;

    invoke-virtual/range {p1 .. p1}, Lorg/apache/tools/ant/types/Resource;->getOutputStream()Ljava/io/OutputStream;

    move-result-object v22

    move-object/from16 v0, v16

    move-object/from16 v1, v22

    move-object/from16 v2, p7

    invoke-direct {v0, v1, v2}, Ljava/io/OutputStreamWriter;-><init>(Ljava/io/OutputStream;Ljava/lang/String;)V
    :try_end_5
    .catchall {:try_start_5 .. :try_end_5} :catchall_7

    goto :goto_4

    :cond_6
    :try_start_6
    move-object/from16 v0, p2

    invoke-virtual {v0, v12}, Lorg/apache/tools/ant/types/FilterSetCollection;->replaceTokens(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v15

    move-object/from16 v0, v18

    invoke-virtual {v0, v15}, Ljava/io/BufferedWriter;->write(Ljava/lang/String;)V
    :try_end_6
    .catchall {:try_start_6 .. :try_end_6} :catchall_0

    goto :goto_7

    :catchall_0
    move-exception v22

    move-object/from16 v17, v18

    :goto_8
    invoke-static/range {v17 .. v17}, Lorg/apache/tools/ant/util/FileUtils;->close(Ljava/io/Writer;)V

    invoke-static {v8}, Lorg/apache/tools/ant/util/FileUtils;->close(Ljava/io/Reader;)V

    throw v22

    :cond_7
    invoke-static/range {v18 .. v18}, Lorg/apache/tools/ant/util/FileUtils;->close(Ljava/io/Writer;)V

    invoke-static {v8}, Lorg/apache/tools/ant/util/FileUtils;->close(Ljava/io/Reader;)V

    :goto_9
    if-eqz p5, :cond_0

    move-object/from16 v0, p1

    instance-of v0, v0, Lorg/apache/tools/ant/types/resources/Touchable;

    move/from16 v22, v0

    if-eqz v22, :cond_0

    check-cast p1, Lorg/apache/tools/ant/types/resources/Touchable;

    invoke-virtual/range {p0 .. p0}, Lorg/apache/tools/ant/types/Resource;->getLastModified()J

    move-result-wide v22

    move-object/from16 v0, p1

    move-wide/from16 v1, v22

    invoke-static {v0, v1, v2}, Lorg/apache/tools/ant/util/ResourceUtils;->setLastModified(Lorg/apache/tools/ant/types/resources/Touchable;J)V

    goto/16 :goto_0

    :cond_8
    if-nez v6, :cond_a

    if-eqz p6, :cond_9

    invoke-virtual/range {p6 .. p7}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v22

    if-eqz v22, :cond_a

    :cond_9
    if-nez p6, :cond_e

    if-eqz p7, :cond_e

    :cond_a
    const/4 v8, 0x0

    const/16 v17, 0x0

    const/4 v10, 0x0

    if-nez p6, :cond_b

    :try_start_7
    new-instance v11, Ljava/io/InputStreamReader;

    invoke-virtual/range {p0 .. p0}, Lorg/apache/tools/ant/types/Resource;->getInputStream()Ljava/io/InputStream;

    move-result-object v22

    move-object/from16 v0, v22

    invoke-direct {v11, v0}, Ljava/io/InputStreamReader;-><init>(Ljava/io/InputStream;)V

    move-object v10, v11

    :goto_a
    new-instance v9, Ljava/io/BufferedReader;

    invoke-direct {v9, v10}, Ljava/io/BufferedReader;-><init>(Ljava/io/Reader;)V
    :try_end_7
    .catchall {:try_start_7 .. :try_end_7} :catchall_3

    const/16 v16, 0x0

    if-nez p7, :cond_c

    :try_start_8
    new-instance v16, Ljava/io/OutputStreamWriter;

    invoke-virtual/range {p1 .. p1}, Lorg/apache/tools/ant/types/Resource;->getOutputStream()Ljava/io/OutputStream;

    move-result-object v22

    move-object/from16 v0, v16

    move-object/from16 v1, v22

    invoke-direct {v0, v1}, Ljava/io/OutputStreamWriter;-><init>(Ljava/io/OutputStream;)V

    :goto_b
    new-instance v18, Ljava/io/BufferedWriter;

    move-object/from16 v0, v18

    move-object/from16 v1, v16

    invoke-direct {v0, v1}, Ljava/io/BufferedWriter;-><init>(Ljava/io/Writer;)V
    :try_end_8
    .catchall {:try_start_8 .. :try_end_8} :catchall_4

    if-eqz v6, :cond_10

    :try_start_9
    new-instance v5, Lorg/apache/tools/ant/filters/util/ChainReaderHelper;

    invoke-direct {v5}, Lorg/apache/tools/ant/filters/util/ChainReaderHelper;-><init>()V

    const/16 v22, 0x2000

    move/from16 v0, v22

    invoke-virtual {v5, v0}, Lorg/apache/tools/ant/filters/util/ChainReaderHelper;->setBufferSize(I)V

    invoke-virtual {v5, v9}, Lorg/apache/tools/ant/filters/util/ChainReaderHelper;->setPrimaryReader(Ljava/io/Reader;)V

    move-object/from16 v0, p3

    invoke-virtual {v5, v0}, Lorg/apache/tools/ant/filters/util/ChainReaderHelper;->setFilterChains(Ljava/util/Vector;)V

    move-object/from16 v0, p8

    invoke-virtual {v5, v0}, Lorg/apache/tools/ant/filters/util/ChainReaderHelper;->setProject(Lorg/apache/tools/ant/Project;)V

    invoke-virtual {v5}, Lorg/apache/tools/ant/filters/util/ChainReaderHelper;->getAssembledReader()Ljava/io/Reader;

    move-result-object v19

    new-instance v8, Ljava/io/BufferedReader;

    move-object/from16 v0, v19

    invoke-direct {v8, v0}, Ljava/io/BufferedReader;-><init>(Ljava/io/Reader;)V
    :try_end_9
    .catchall {:try_start_9 .. :try_end_9} :catchall_5

    :goto_c
    const/16 v22, 0x2000

    :try_start_a
    move/from16 v0, v22

    new-array v3, v0, [C

    :goto_d
    const/16 v22, 0x0

    array-length v0, v3

    move/from16 v23, v0

    move/from16 v0, v22

    move/from16 v1, v23

    invoke-virtual {v8, v3, v0, v1}, Ljava/io/BufferedReader;->read([CII)I
    :try_end_a
    .catchall {:try_start_a .. :try_end_a} :catchall_1

    move-result v14

    const/16 v22, -0x1

    move/from16 v0, v22

    if-ne v14, v0, :cond_d

    invoke-static/range {v18 .. v18}, Lorg/apache/tools/ant/util/FileUtils;->close(Ljava/io/Writer;)V

    invoke-static {v8}, Lorg/apache/tools/ant/util/FileUtils;->close(Ljava/io/Reader;)V

    goto/16 :goto_9

    :cond_b
    :try_start_b
    new-instance v11, Ljava/io/InputStreamReader;

    invoke-virtual/range {p0 .. p0}, Lorg/apache/tools/ant/types/Resource;->getInputStream()Ljava/io/InputStream;

    move-result-object v22

    move-object/from16 v0, v22

    move-object/from16 v1, p6

    invoke-direct {v11, v0, v1}, Ljava/io/InputStreamReader;-><init>(Ljava/io/InputStream;Ljava/lang/String;)V
    :try_end_b
    .catchall {:try_start_b .. :try_end_b} :catchall_3

    move-object v10, v11

    goto :goto_a

    :cond_c
    :try_start_c
    new-instance v16, Ljava/io/OutputStreamWriter;

    invoke-virtual/range {p1 .. p1}, Lorg/apache/tools/ant/types/Resource;->getOutputStream()Ljava/io/OutputStream;

    move-result-object v22

    move-object/from16 v0, v16

    move-object/from16 v1, v22

    move-object/from16 v2, p7

    invoke-direct {v0, v1, v2}, Ljava/io/OutputStreamWriter;-><init>(Ljava/io/OutputStream;Ljava/lang/String;)V
    :try_end_c
    .catchall {:try_start_c .. :try_end_c} :catchall_4

    goto :goto_b

    :cond_d
    const/16 v22, 0x0

    :try_start_d
    move-object/from16 v0, v18

    move/from16 v1, v22

    invoke-virtual {v0, v3, v1, v14}, Ljava/io/BufferedWriter;->write([CII)V
    :try_end_d
    .catchall {:try_start_d .. :try_end_d} :catchall_1

    goto :goto_d

    :catchall_1
    move-exception v22

    move-object/from16 v17, v18

    :goto_e
    invoke-static/range {v17 .. v17}, Lorg/apache/tools/ant/util/FileUtils;->close(Ljava/io/Writer;)V

    invoke-static {v8}, Lorg/apache/tools/ant/util/FileUtils;->close(Ljava/io/Reader;)V

    throw v22

    :cond_e
    const/4 v8, 0x0

    const/16 v17, 0x0

    :try_start_e
    invoke-virtual/range {p0 .. p0}, Lorg/apache/tools/ant/types/Resource;->getInputStream()Ljava/io/InputStream;

    move-result-object v8

    invoke-virtual/range {p1 .. p1}, Lorg/apache/tools/ant/types/Resource;->getOutputStream()Ljava/io/OutputStream;

    move-result-object v17

    const/16 v22, 0x2000

    move/from16 v0, v22

    new-array v3, v0, [B

    const/4 v4, 0x0

    :cond_f
    const/16 v22, 0x0

    move-object/from16 v0, v17

    move/from16 v1, v22

    invoke-virtual {v0, v3, v1, v4}, Ljava/io/OutputStream;->write([BII)V

    const/16 v22, 0x0

    array-length v0, v3

    move/from16 v23, v0

    move/from16 v0, v22

    move/from16 v1, v23

    invoke-virtual {v8, v3, v0, v1}, Ljava/io/InputStream;->read([BII)I
    :try_end_e
    .catchall {:try_start_e .. :try_end_e} :catchall_2

    move-result v4

    const/16 v22, -0x1

    move/from16 v0, v22

    if-ne v4, v0, :cond_f

    invoke-static/range {v17 .. v17}, Lorg/apache/tools/ant/util/FileUtils;->close(Ljava/io/OutputStream;)V

    invoke-static {v8}, Lorg/apache/tools/ant/util/FileUtils;->close(Ljava/io/InputStream;)V

    goto/16 :goto_9

    :catchall_2
    move-exception v22

    invoke-static/range {v17 .. v17}, Lorg/apache/tools/ant/util/FileUtils;->close(Ljava/io/OutputStream;)V

    invoke-static {v8}, Lorg/apache/tools/ant/util/FileUtils;->close(Ljava/io/InputStream;)V

    throw v22

    :catchall_3
    move-exception v22

    goto :goto_e

    :catchall_4
    move-exception v22

    move-object v8, v9

    goto :goto_e

    :catchall_5
    move-exception v22

    move-object/from16 v17, v18

    move-object v8, v9

    goto :goto_e

    :catchall_6
    move-exception v22

    goto/16 :goto_8

    :catchall_7
    move-exception v22

    move-object v8, v9

    goto/16 :goto_8

    :catchall_8
    move-exception v22

    move-object/from16 v17, v18

    move-object v8, v9

    goto/16 :goto_8

    :cond_10
    move-object v8, v9

    goto/16 :goto_c

    :cond_11
    move-object v8, v9

    goto/16 :goto_5
.end method

.method private static logFuture(Lorg/apache/tools/ant/ProjectComponent;Lorg/apache/tools/ant/types/ResourceCollection;J)V
    .locals 7
    .param p0    # Lorg/apache/tools/ant/ProjectComponent;
    .param p1    # Lorg/apache/tools/ant/types/ResourceCollection;
    .param p2    # J

    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v5

    add-long v2, v5, p2

    new-instance v4, Lorg/apache/tools/ant/types/resources/selectors/Date;

    invoke-direct {v4}, Lorg/apache/tools/ant/types/resources/selectors/Date;-><init>()V

    invoke-virtual {v4, v2, v3}, Lorg/apache/tools/ant/types/resources/selectors/Date;->setMillis(J)V

    sget-object v5, Lorg/apache/tools/ant/types/TimeComparison;->AFTER:Lorg/apache/tools/ant/types/TimeComparison;

    invoke-virtual {v4, v5}, Lorg/apache/tools/ant/types/resources/selectors/Date;->setWhen(Lorg/apache/tools/ant/types/TimeComparison;)V

    new-instance v0, Lorg/apache/tools/ant/types/resources/Restrict;

    invoke-direct {v0}, Lorg/apache/tools/ant/types/resources/Restrict;-><init>()V

    invoke-virtual {v0, v4}, Lorg/apache/tools/ant/types/resources/Restrict;->add(Lorg/apache/tools/ant/types/resources/selectors/ResourceSelector;)V

    invoke-virtual {v0, p1}, Lorg/apache/tools/ant/types/resources/Restrict;->add(Lorg/apache/tools/ant/types/ResourceCollection;)V

    invoke-virtual {v0}, Lorg/apache/tools/ant/types/resources/Restrict;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :goto_0
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v5

    if-eqz v5, :cond_0

    new-instance v5, Ljava/lang/StringBuffer;

    invoke-direct {v5}, Ljava/lang/StringBuffer;-><init>()V

    const-string v6, "Warning: "

    invoke-virtual {v5, v6}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    move-result-object v6

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v5

    check-cast v5, Lorg/apache/tools/ant/types/Resource;

    invoke-virtual {v5}, Lorg/apache/tools/ant/types/Resource;->getName()Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v6, v5}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    move-result-object v5

    const-string v6, " modified in the future."

    invoke-virtual {v5, v6}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/StringBuffer;->toString()Ljava/lang/String;

    move-result-object v5

    const/4 v6, 0x1

    invoke-virtual {p0, v5, v6}, Lorg/apache/tools/ant/ProjectComponent;->log(Ljava/lang/String;I)V

    goto :goto_0

    :cond_0
    return-void
.end method

.method public static selectOutOfDateSources(Lorg/apache/tools/ant/ProjectComponent;Lorg/apache/tools/ant/types/ResourceCollection;Lorg/apache/tools/ant/util/FileNameMapper;Lorg/apache/tools/ant/types/ResourceFactory;J)Lorg/apache/tools/ant/types/ResourceCollection;
    .locals 22
    .param p0    # Lorg/apache/tools/ant/ProjectComponent;
    .param p1    # Lorg/apache/tools/ant/types/ResourceCollection;
    .param p2    # Lorg/apache/tools/ant/util/FileNameMapper;
    .param p3    # Lorg/apache/tools/ant/types/ResourceFactory;
    .param p4    # J

    invoke-interface/range {p1 .. p1}, Lorg/apache/tools/ant/types/ResourceCollection;->size()I

    move-result v14

    if-nez v14, :cond_1

    const-string v14, "No sources found."

    const/4 v15, 0x3

    move-object/from16 v0, p0

    invoke-virtual {v0, v14, v15}, Lorg/apache/tools/ant/ProjectComponent;->log(Ljava/lang/String;I)V

    sget-object v8, Lorg/apache/tools/ant/types/resources/Resources;->NONE:Lorg/apache/tools/ant/types/ResourceCollection;

    :cond_0
    return-object v8

    :cond_1
    invoke-static/range {p1 .. p1}, Lorg/apache/tools/ant/types/resources/Union;->getInstance(Lorg/apache/tools/ant/types/ResourceCollection;)Lorg/apache/tools/ant/types/resources/Union;

    move-result-object p1

    move-object/from16 v0, p0

    move-object/from16 v1, p1

    move-wide/from16 v2, p4

    invoke-static {v0, v1, v2, v3}, Lorg/apache/tools/ant/util/ResourceUtils;->logFuture(Lorg/apache/tools/ant/ProjectComponent;Lorg/apache/tools/ant/types/ResourceCollection;J)V

    new-instance v8, Lorg/apache/tools/ant/types/resources/Union;

    invoke-direct {v8}, Lorg/apache/tools/ant/types/resources/Union;-><init>()V

    invoke-interface/range {p1 .. p1}, Lorg/apache/tools/ant/types/ResourceCollection;->iterator()Ljava/util/Iterator;

    move-result-object v6

    :goto_0
    invoke-interface {v6}, Ljava/util/Iterator;->hasNext()Z

    move-result v14

    if-eqz v14, :cond_0

    invoke-interface {v6}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v9

    check-cast v9, Lorg/apache/tools/ant/types/Resource;

    invoke-virtual {v9}, Lorg/apache/tools/ant/types/Resource;->getName()Ljava/lang/String;

    move-result-object v10

    if-nez v10, :cond_3

    :goto_1
    const/4 v13, 0x0

    :try_start_0
    move-object/from16 v0, p2

    invoke-interface {v0, v10}, Lorg/apache/tools/ant/util/FileNameMapper;->mapFileName(Ljava/lang/String;)[Ljava/lang/String;
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    move-result-object v13

    :goto_2
    if-eqz v13, :cond_2

    array-length v14, v13

    if-nez v14, :cond_4

    :cond_2
    new-instance v14, Ljava/lang/StringBuffer;

    invoke-direct {v14}, Ljava/lang/StringBuffer;-><init>()V

    invoke-virtual {v14, v9}, Ljava/lang/StringBuffer;->append(Ljava/lang/Object;)Ljava/lang/StringBuffer;

    move-result-object v14

    const-string v15, " skipped - don\'t know how to handle it"

    invoke-virtual {v14, v15}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    move-result-object v14

    invoke-virtual {v14}, Ljava/lang/StringBuffer;->toString()Ljava/lang/String;

    move-result-object v14

    const/4 v15, 0x3

    move-object/from16 v0, p0

    invoke-virtual {v0, v14, v15}, Lorg/apache/tools/ant/ProjectComponent;->log(Ljava/lang/String;I)V

    goto :goto_0

    :cond_3
    const/16 v14, 0x2f

    sget-char v15, Ljava/io/File;->separatorChar:C

    invoke-virtual {v10, v14, v15}, Ljava/lang/String;->replace(CC)Ljava/lang/String;

    move-result-object v10

    goto :goto_1

    :catch_0
    move-exception v4

    new-instance v14, Ljava/lang/StringBuffer;

    invoke-direct {v14}, Ljava/lang/StringBuffer;-><init>()V

    const-string v15, "Caught "

    invoke-virtual {v14, v15}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    move-result-object v14

    invoke-virtual {v14, v4}, Ljava/lang/StringBuffer;->append(Ljava/lang/Object;)Ljava/lang/StringBuffer;

    move-result-object v14

    const-string v15, " mapping resource "

    invoke-virtual {v14, v15}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    move-result-object v14

    invoke-virtual {v14, v9}, Ljava/lang/StringBuffer;->append(Ljava/lang/Object;)Ljava/lang/StringBuffer;

    move-result-object v14

    invoke-virtual {v14}, Ljava/lang/StringBuffer;->toString()Ljava/lang/String;

    move-result-object v14

    const/4 v15, 0x3

    move-object/from16 v0, p0

    invoke-virtual {v0, v14, v15}, Lorg/apache/tools/ant/ProjectComponent;->log(Ljava/lang/String;I)V

    goto :goto_2

    :cond_4
    new-instance v12, Lorg/apache/tools/ant/types/resources/Union;

    invoke-direct {v12}, Lorg/apache/tools/ant/types/resources/Union;-><init>()V

    const/4 v5, 0x0

    :goto_3
    array-length v14, v13

    if-ge v5, v14, :cond_5

    aget-object v14, v13, v5

    sget-char v15, Ljava/io/File;->separatorChar:C

    const/16 v16, 0x2f

    invoke-virtual/range {v14 .. v16}, Ljava/lang/String;->replace(CC)Ljava/lang/String;

    move-result-object v14

    move-object/from16 v0, p3

    invoke-interface {v0, v14}, Lorg/apache/tools/ant/types/ResourceFactory;->getResource(Ljava/lang/String;)Lorg/apache/tools/ant/types/Resource;

    move-result-object v14

    invoke-virtual {v12, v14}, Lorg/apache/tools/ant/types/resources/Union;->add(Lorg/apache/tools/ant/types/ResourceCollection;)V

    add-int/lit8 v5, v5, 0x1

    goto :goto_3

    :cond_5
    new-instance v7, Lorg/apache/tools/ant/types/resources/Restrict;

    invoke-direct {v7}, Lorg/apache/tools/ant/types/resources/Restrict;-><init>()V

    new-instance v14, Lorg/apache/tools/ant/types/resources/selectors/And;

    const/4 v15, 0x2

    new-array v15, v15, [Lorg/apache/tools/ant/types/resources/selectors/ResourceSelector;

    const/16 v16, 0x0

    sget-object v17, Lorg/apache/tools/ant/types/resources/selectors/Type;->FILE:Lorg/apache/tools/ant/types/resources/selectors/Type;

    aput-object v17, v15, v16

    const/16 v16, 0x1

    new-instance v17, Lorg/apache/tools/ant/types/resources/selectors/Or;

    const/16 v18, 0x2

    move/from16 v0, v18

    new-array v0, v0, [Lorg/apache/tools/ant/types/resources/selectors/ResourceSelector;

    move-object/from16 v18, v0

    const/16 v19, 0x0

    sget-object v20, Lorg/apache/tools/ant/util/ResourceUtils;->NOT_EXISTS:Lorg/apache/tools/ant/types/resources/selectors/ResourceSelector;

    aput-object v20, v18, v19

    const/16 v19, 0x1

    new-instance v20, Lorg/apache/tools/ant/util/ResourceUtils$Outdated;

    const/16 v21, 0x0

    move-object/from16 v0, v20

    move-wide/from16 v1, p4

    move-object/from16 v3, v21

    invoke-direct {v0, v9, v1, v2, v3}, Lorg/apache/tools/ant/util/ResourceUtils$Outdated;-><init>(Lorg/apache/tools/ant/types/Resource;JLorg/apache/tools/ant/util/ResourceUtils$1;)V

    aput-object v20, v18, v19

    invoke-direct/range {v17 .. v18}, Lorg/apache/tools/ant/types/resources/selectors/Or;-><init>([Lorg/apache/tools/ant/types/resources/selectors/ResourceSelector;)V

    aput-object v17, v15, v16

    invoke-direct {v14, v15}, Lorg/apache/tools/ant/types/resources/selectors/And;-><init>([Lorg/apache/tools/ant/types/resources/selectors/ResourceSelector;)V

    invoke-virtual {v7, v14}, Lorg/apache/tools/ant/types/resources/Restrict;->add(Lorg/apache/tools/ant/types/resources/selectors/ResourceSelector;)V

    invoke-virtual {v7, v12}, Lorg/apache/tools/ant/types/resources/Restrict;->add(Lorg/apache/tools/ant/types/ResourceCollection;)V

    invoke-virtual {v7}, Lorg/apache/tools/ant/types/resources/Restrict;->size()I

    move-result v14

    if-lez v14, :cond_7

    invoke-virtual {v8, v9}, Lorg/apache/tools/ant/types/resources/Union;->add(Lorg/apache/tools/ant/types/ResourceCollection;)V

    invoke-virtual {v7}, Lorg/apache/tools/ant/types/resources/Restrict;->iterator()Ljava/util/Iterator;

    move-result-object v14

    invoke-interface {v14}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v14

    check-cast v14, Lorg/apache/tools/ant/types/Resource;

    move-object v11, v14

    check-cast v11, Lorg/apache/tools/ant/types/Resource;

    new-instance v14, Ljava/lang/StringBuffer;

    invoke-direct {v14}, Ljava/lang/StringBuffer;-><init>()V

    invoke-virtual {v9}, Lorg/apache/tools/ant/types/Resource;->getName()Ljava/lang/String;

    move-result-object v15

    invoke-virtual {v14, v15}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    move-result-object v14

    const-string v15, " added as "

    invoke-virtual {v14, v15}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    move-result-object v14

    invoke-virtual {v11}, Lorg/apache/tools/ant/types/Resource;->getName()Ljava/lang/String;

    move-result-object v15

    invoke-virtual {v14, v15}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    move-result-object v15

    invoke-virtual {v11}, Lorg/apache/tools/ant/types/Resource;->isExists()Z

    move-result v14

    if-eqz v14, :cond_6

    const-string v14, " is outdated."

    :goto_4
    invoke-virtual {v15, v14}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    move-result-object v14

    invoke-virtual {v14}, Ljava/lang/StringBuffer;->toString()Ljava/lang/String;

    move-result-object v14

    const/4 v15, 0x3

    move-object/from16 v0, p0

    invoke-virtual {v0, v14, v15}, Lorg/apache/tools/ant/ProjectComponent;->log(Ljava/lang/String;I)V

    goto/16 :goto_0

    :cond_6
    const-string v14, " doesn\'t exist."

    goto :goto_4

    :cond_7
    new-instance v14, Ljava/lang/StringBuffer;

    invoke-direct {v14}, Ljava/lang/StringBuffer;-><init>()V

    invoke-virtual {v9}, Lorg/apache/tools/ant/types/Resource;->getName()Ljava/lang/String;

    move-result-object v15

    invoke-virtual {v14, v15}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    move-result-object v14

    const-string v15, " omitted as "

    invoke-virtual {v14, v15}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    move-result-object v14

    invoke-virtual {v12}, Lorg/apache/tools/ant/types/resources/Union;->toString()Ljava/lang/String;

    move-result-object v15

    invoke-virtual {v14, v15}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    move-result-object v15

    invoke-virtual {v12}, Lorg/apache/tools/ant/types/resources/Union;->size()I

    move-result v14

    const/16 v16, 0x1

    move/from16 v0, v16

    if-ne v14, v0, :cond_8

    const-string v14, " is"

    :goto_5
    invoke-virtual {v15, v14}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    move-result-object v14

    const-string v15, " up to date."

    invoke-virtual {v14, v15}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    move-result-object v14

    invoke-virtual {v14}, Ljava/lang/StringBuffer;->toString()Ljava/lang/String;

    move-result-object v14

    const/4 v15, 0x3

    move-object/from16 v0, p0

    invoke-virtual {v0, v14, v15}, Lorg/apache/tools/ant/ProjectComponent;->log(Ljava/lang/String;I)V

    goto/16 :goto_0

    :cond_8
    const-string v14, " are "

    goto :goto_5
.end method

.method public static selectOutOfDateSources(Lorg/apache/tools/ant/ProjectComponent;[Lorg/apache/tools/ant/types/Resource;Lorg/apache/tools/ant/util/FileNameMapper;Lorg/apache/tools/ant/types/ResourceFactory;)[Lorg/apache/tools/ant/types/Resource;
    .locals 6
    .param p0    # Lorg/apache/tools/ant/ProjectComponent;
    .param p1    # [Lorg/apache/tools/ant/types/Resource;
    .param p2    # Lorg/apache/tools/ant/util/FileNameMapper;
    .param p3    # Lorg/apache/tools/ant/types/ResourceFactory;

    sget-object v0, Lorg/apache/tools/ant/util/ResourceUtils;->FILE_UTILS:Lorg/apache/tools/ant/util/FileUtils;

    invoke-virtual {v0}, Lorg/apache/tools/ant/util/FileUtils;->getFileTimestampGranularity()J

    move-result-wide v4

    move-object v0, p0

    move-object v1, p1

    move-object v2, p2

    move-object v3, p3

    invoke-static/range {v0 .. v5}, Lorg/apache/tools/ant/util/ResourceUtils;->selectOutOfDateSources(Lorg/apache/tools/ant/ProjectComponent;[Lorg/apache/tools/ant/types/Resource;Lorg/apache/tools/ant/util/FileNameMapper;Lorg/apache/tools/ant/types/ResourceFactory;J)[Lorg/apache/tools/ant/types/Resource;

    move-result-object v0

    return-object v0
.end method

.method public static selectOutOfDateSources(Lorg/apache/tools/ant/ProjectComponent;[Lorg/apache/tools/ant/types/Resource;Lorg/apache/tools/ant/util/FileNameMapper;Lorg/apache/tools/ant/types/ResourceFactory;J)[Lorg/apache/tools/ant/types/Resource;
    .locals 7
    .param p0    # Lorg/apache/tools/ant/ProjectComponent;
    .param p1    # [Lorg/apache/tools/ant/types/Resource;
    .param p2    # Lorg/apache/tools/ant/util/FileNameMapper;
    .param p3    # Lorg/apache/tools/ant/types/ResourceFactory;
    .param p4    # J

    new-instance v1, Lorg/apache/tools/ant/types/resources/Union;

    invoke-direct {v1}, Lorg/apache/tools/ant/types/resources/Union;-><init>()V

    invoke-static {p1}, Ljava/util/Arrays;->asList([Ljava/lang/Object;)Ljava/util/List;

    move-result-object v0

    invoke-virtual {v1, v0}, Lorg/apache/tools/ant/types/resources/Union;->addAll(Ljava/util/Collection;)V

    move-object v0, p0

    move-object v2, p2

    move-object v3, p3

    move-wide v4, p4

    invoke-static/range {v0 .. v5}, Lorg/apache/tools/ant/util/ResourceUtils;->selectOutOfDateSources(Lorg/apache/tools/ant/ProjectComponent;Lorg/apache/tools/ant/types/ResourceCollection;Lorg/apache/tools/ant/util/FileNameMapper;Lorg/apache/tools/ant/types/ResourceFactory;J)Lorg/apache/tools/ant/types/ResourceCollection;

    move-result-object v6

    invoke-interface {v6}, Lorg/apache/tools/ant/types/ResourceCollection;->size()I

    move-result v0

    if-nez v0, :cond_0

    const/4 v0, 0x0

    new-array v0, v0, [Lorg/apache/tools/ant/types/Resource;

    :goto_0
    return-object v0

    :cond_0
    check-cast v6, Lorg/apache/tools/ant/types/resources/Union;

    invoke-virtual {v6}, Lorg/apache/tools/ant/types/resources/Union;->listResources()[Lorg/apache/tools/ant/types/Resource;

    move-result-object v0

    goto :goto_0
.end method

.method public static setLastModified(Lorg/apache/tools/ant/types/resources/Touchable;J)V
    .locals 2
    .param p0    # Lorg/apache/tools/ant/types/resources/Touchable;
    .param p1    # J

    const-wide/16 v0, 0x0

    cmp-long v0, p1, v0

    if-gez v0, :cond_0

    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide p1

    :cond_0
    invoke-interface {p0, p1, p2}, Lorg/apache/tools/ant/types/resources/Touchable;->touch(J)V

    return-void
.end method

.method private static textCompare(Lorg/apache/tools/ant/types/Resource;Lorg/apache/tools/ant/types/Resource;)I
    .locals 8
    .param p0    # Lorg/apache/tools/ant/types/Resource;
    .param p1    # Lorg/apache/tools/ant/types/Resource;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    const/4 v2, 0x0

    const/4 v4, 0x0

    :try_start_0
    new-instance v3, Ljava/io/BufferedReader;

    new-instance v6, Ljava/io/InputStreamReader;

    invoke-virtual {p0}, Lorg/apache/tools/ant/types/Resource;->getInputStream()Ljava/io/InputStream;

    move-result-object v7

    invoke-direct {v6, v7}, Ljava/io/InputStreamReader;-><init>(Ljava/io/InputStream;)V

    invoke-direct {v3, v6}, Ljava/io/BufferedReader;-><init>(Ljava/io/Reader;)V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    :try_start_1
    new-instance v5, Ljava/io/BufferedReader;

    new-instance v6, Ljava/io/InputStreamReader;

    invoke-virtual {p1}, Lorg/apache/tools/ant/types/Resource;->getInputStream()Ljava/io/InputStream;

    move-result-object v7

    invoke-direct {v6, v7}, Ljava/io/InputStreamReader;-><init>(Ljava/io/InputStream;)V

    invoke-direct {v5, v6}, Ljava/io/BufferedReader;-><init>(Ljava/io/Reader;)V
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_1

    :try_start_2
    invoke-virtual {v3}, Ljava/io/BufferedReader;->readLine()Ljava/lang/String;

    move-result-object v1

    :goto_0
    if-eqz v1, :cond_1

    invoke-virtual {v5}, Ljava/io/BufferedReader;->readLine()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v1, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v6

    if-nez v6, :cond_0

    invoke-virtual {v1, v0}, Ljava/lang/String;->compareTo(Ljava/lang/String;)I
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_2

    move-result v6

    invoke-static {v3}, Lorg/apache/tools/ant/util/FileUtils;->close(Ljava/io/Reader;)V

    invoke-static {v5}, Lorg/apache/tools/ant/util/FileUtils;->close(Ljava/io/Reader;)V

    :goto_1
    return v6

    :cond_0
    :try_start_3
    invoke-virtual {v3}, Ljava/io/BufferedReader;->readLine()Ljava/lang/String;

    move-result-object v1

    goto :goto_0

    :cond_1
    invoke-virtual {v5}, Ljava/io/BufferedReader;->readLine()Ljava/lang/String;
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_2

    move-result-object v6

    if-nez v6, :cond_2

    const/4 v6, 0x0

    :goto_2
    invoke-static {v3}, Lorg/apache/tools/ant/util/FileUtils;->close(Ljava/io/Reader;)V

    invoke-static {v5}, Lorg/apache/tools/ant/util/FileUtils;->close(Ljava/io/Reader;)V

    goto :goto_1

    :cond_2
    const/4 v6, -0x1

    goto :goto_2

    :catchall_0
    move-exception v6

    :goto_3
    invoke-static {v2}, Lorg/apache/tools/ant/util/FileUtils;->close(Ljava/io/Reader;)V

    invoke-static {v4}, Lorg/apache/tools/ant/util/FileUtils;->close(Ljava/io/Reader;)V

    throw v6

    :catchall_1
    move-exception v6

    move-object v2, v3

    goto :goto_3

    :catchall_2
    move-exception v6

    move-object v4, v5

    move-object v2, v3

    goto :goto_3
.end method
