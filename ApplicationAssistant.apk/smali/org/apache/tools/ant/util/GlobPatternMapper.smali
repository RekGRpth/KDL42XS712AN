.class public Lorg/apache/tools/ant/util/GlobPatternMapper;
.super Ljava/lang/Object;
.source "GlobPatternMapper.java"

# interfaces
.implements Lorg/apache/tools/ant/util/FileNameMapper;


# instance fields
.field private caseSensitive:Z

.field protected fromPostfix:Ljava/lang/String;

.field protected fromPrefix:Ljava/lang/String;

.field private handleDirSep:Z

.field protected postfixLength:I

.field protected prefixLength:I

.field protected toPostfix:Ljava/lang/String;

.field protected toPrefix:Ljava/lang/String;


# direct methods
.method public constructor <init>()V
    .locals 1

    const/4 v0, 0x0

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    iput-object v0, p0, Lorg/apache/tools/ant/util/GlobPatternMapper;->fromPrefix:Ljava/lang/String;

    iput-object v0, p0, Lorg/apache/tools/ant/util/GlobPatternMapper;->fromPostfix:Ljava/lang/String;

    iput-object v0, p0, Lorg/apache/tools/ant/util/GlobPatternMapper;->toPrefix:Ljava/lang/String;

    iput-object v0, p0, Lorg/apache/tools/ant/util/GlobPatternMapper;->toPostfix:Ljava/lang/String;

    const/4 v0, 0x0

    iput-boolean v0, p0, Lorg/apache/tools/ant/util/GlobPatternMapper;->handleDirSep:Z

    const/4 v0, 0x1

    iput-boolean v0, p0, Lorg/apache/tools/ant/util/GlobPatternMapper;->caseSensitive:Z

    return-void
.end method

.method private modifyName(Ljava/lang/String;)Ljava/lang/String;
    .locals 3
    .param p1    # Ljava/lang/String;

    const/16 v2, 0x5c

    iget-boolean v0, p0, Lorg/apache/tools/ant/util/GlobPatternMapper;->caseSensitive:Z

    if-nez v0, :cond_0

    invoke-virtual {p1}, Ljava/lang/String;->toLowerCase()Ljava/lang/String;

    move-result-object p1

    :cond_0
    iget-boolean v0, p0, Lorg/apache/tools/ant/util/GlobPatternMapper;->handleDirSep:Z

    if-eqz v0, :cond_1

    invoke-virtual {p1, v2}, Ljava/lang/String;->indexOf(I)I

    move-result v0

    const/4 v1, -0x1

    if-eq v0, v1, :cond_1

    const/16 v0, 0x2f

    invoke-virtual {p1, v2, v0}, Ljava/lang/String;->replace(CC)Ljava/lang/String;

    move-result-object p1

    :cond_1
    return-object p1
.end method


# virtual methods
.method protected extractVariablePart(Ljava/lang/String;)Ljava/lang/String;
    .locals 3
    .param p1    # Ljava/lang/String;

    iget v0, p0, Lorg/apache/tools/ant/util/GlobPatternMapper;->prefixLength:I

    invoke-virtual {p1}, Ljava/lang/String;->length()I

    move-result v1

    iget v2, p0, Lorg/apache/tools/ant/util/GlobPatternMapper;->postfixLength:I

    sub-int/2addr v1, v2

    invoke-virtual {p1, v0, v1}, Ljava/lang/String;->substring(II)Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public mapFileName(Ljava/lang/String;)[Ljava/lang/String;
    .locals 4
    .param p1    # Ljava/lang/String;

    iget-object v0, p0, Lorg/apache/tools/ant/util/GlobPatternMapper;->fromPrefix:Ljava/lang/String;

    if-eqz v0, :cond_0

    invoke-direct {p0, p1}, Lorg/apache/tools/ant/util/GlobPatternMapper;->modifyName(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    iget-object v1, p0, Lorg/apache/tools/ant/util/GlobPatternMapper;->fromPrefix:Ljava/lang/String;

    invoke-direct {p0, v1}, Lorg/apache/tools/ant/util/GlobPatternMapper;->modifyName(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/String;->startsWith(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-direct {p0, p1}, Lorg/apache/tools/ant/util/GlobPatternMapper;->modifyName(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    iget-object v1, p0, Lorg/apache/tools/ant/util/GlobPatternMapper;->fromPostfix:Ljava/lang/String;

    invoke-direct {p0, v1}, Lorg/apache/tools/ant/util/GlobPatternMapper;->modifyName(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/String;->endsWith(Ljava/lang/String;)Z

    move-result v0

    if-nez v0, :cond_1

    :cond_0
    const/4 v0, 0x0

    :goto_0
    return-object v0

    :cond_1
    const/4 v0, 0x1

    new-array v0, v0, [Ljava/lang/String;

    const/4 v1, 0x0

    new-instance v2, Ljava/lang/StringBuffer;

    invoke-direct {v2}, Ljava/lang/StringBuffer;-><init>()V

    iget-object v3, p0, Lorg/apache/tools/ant/util/GlobPatternMapper;->toPrefix:Ljava/lang/String;

    invoke-virtual {v2, v3}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    move-result-object v2

    invoke-virtual {p0, p1}, Lorg/apache/tools/ant/util/GlobPatternMapper;->extractVariablePart(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    move-result-object v2

    iget-object v3, p0, Lorg/apache/tools/ant/util/GlobPatternMapper;->toPostfix:Ljava/lang/String;

    invoke-virtual {v2, v3}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuffer;->toString()Ljava/lang/String;

    move-result-object v2

    aput-object v2, v0, v1

    goto :goto_0
.end method

.method public setCaseSensitive(Z)V
    .locals 0
    .param p1    # Z

    iput-boolean p1, p0, Lorg/apache/tools/ant/util/GlobPatternMapper;->caseSensitive:Z

    return-void
.end method

.method public setFrom(Ljava/lang/String;)V
    .locals 2
    .param p1    # Ljava/lang/String;

    const-string v1, "*"

    invoke-virtual {p1, v1}, Ljava/lang/String;->lastIndexOf(Ljava/lang/String;)I

    move-result v0

    const/4 v1, -0x1

    if-ne v0, v1, :cond_0

    iput-object p1, p0, Lorg/apache/tools/ant/util/GlobPatternMapper;->fromPrefix:Ljava/lang/String;

    const-string v1, ""

    iput-object v1, p0, Lorg/apache/tools/ant/util/GlobPatternMapper;->fromPostfix:Ljava/lang/String;

    :goto_0
    iget-object v1, p0, Lorg/apache/tools/ant/util/GlobPatternMapper;->fromPrefix:Ljava/lang/String;

    invoke-virtual {v1}, Ljava/lang/String;->length()I

    move-result v1

    iput v1, p0, Lorg/apache/tools/ant/util/GlobPatternMapper;->prefixLength:I

    iget-object v1, p0, Lorg/apache/tools/ant/util/GlobPatternMapper;->fromPostfix:Ljava/lang/String;

    invoke-virtual {v1}, Ljava/lang/String;->length()I

    move-result v1

    iput v1, p0, Lorg/apache/tools/ant/util/GlobPatternMapper;->postfixLength:I

    return-void

    :cond_0
    const/4 v1, 0x0

    invoke-virtual {p1, v1, v0}, Ljava/lang/String;->substring(II)Ljava/lang/String;

    move-result-object v1

    iput-object v1, p0, Lorg/apache/tools/ant/util/GlobPatternMapper;->fromPrefix:Ljava/lang/String;

    add-int/lit8 v1, v0, 0x1

    invoke-virtual {p1, v1}, Ljava/lang/String;->substring(I)Ljava/lang/String;

    move-result-object v1

    iput-object v1, p0, Lorg/apache/tools/ant/util/GlobPatternMapper;->fromPostfix:Ljava/lang/String;

    goto :goto_0
.end method

.method public setHandleDirSep(Z)V
    .locals 0
    .param p1    # Z

    iput-boolean p1, p0, Lorg/apache/tools/ant/util/GlobPatternMapper;->handleDirSep:Z

    return-void
.end method

.method public setTo(Ljava/lang/String;)V
    .locals 2
    .param p1    # Ljava/lang/String;

    const-string v1, "*"

    invoke-virtual {p1, v1}, Ljava/lang/String;->lastIndexOf(Ljava/lang/String;)I

    move-result v0

    const/4 v1, -0x1

    if-ne v0, v1, :cond_0

    iput-object p1, p0, Lorg/apache/tools/ant/util/GlobPatternMapper;->toPrefix:Ljava/lang/String;

    const-string v1, ""

    iput-object v1, p0, Lorg/apache/tools/ant/util/GlobPatternMapper;->toPostfix:Ljava/lang/String;

    :goto_0
    return-void

    :cond_0
    const/4 v1, 0x0

    invoke-virtual {p1, v1, v0}, Ljava/lang/String;->substring(II)Ljava/lang/String;

    move-result-object v1

    iput-object v1, p0, Lorg/apache/tools/ant/util/GlobPatternMapper;->toPrefix:Ljava/lang/String;

    add-int/lit8 v1, v0, 0x1

    invoke-virtual {p1, v1}, Ljava/lang/String;->substring(I)Ljava/lang/String;

    move-result-object v1

    iput-object v1, p0, Lorg/apache/tools/ant/util/GlobPatternMapper;->toPostfix:Ljava/lang/String;

    goto :goto_0
.end method
