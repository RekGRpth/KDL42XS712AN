.class final Lorg/apache/tools/ant/util/ResourceUtils$Outdated;
.super Ljava/lang/Object;
.source "ResourceUtils.java"

# interfaces
.implements Lorg/apache/tools/ant/types/resources/selectors/ResourceSelector;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lorg/apache/tools/ant/util/ResourceUtils;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x1a
    name = "Outdated"
.end annotation


# instance fields
.field private control:Lorg/apache/tools/ant/types/Resource;

.field private granularity:J


# direct methods
.method private constructor <init>(Lorg/apache/tools/ant/types/Resource;J)V
    .locals 0
    .param p1    # Lorg/apache/tools/ant/types/Resource;
    .param p2    # J

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    iput-object p1, p0, Lorg/apache/tools/ant/util/ResourceUtils$Outdated;->control:Lorg/apache/tools/ant/types/Resource;

    iput-wide p2, p0, Lorg/apache/tools/ant/util/ResourceUtils$Outdated;->granularity:J

    return-void
.end method

.method constructor <init>(Lorg/apache/tools/ant/types/Resource;JLorg/apache/tools/ant/util/ResourceUtils$1;)V
    .locals 0
    .param p1    # Lorg/apache/tools/ant/types/Resource;
    .param p2    # J
    .param p4    # Lorg/apache/tools/ant/util/ResourceUtils$1;

    invoke-direct {p0, p1, p2, p3}, Lorg/apache/tools/ant/util/ResourceUtils$Outdated;-><init>(Lorg/apache/tools/ant/types/Resource;J)V

    return-void
.end method


# virtual methods
.method public isSelected(Lorg/apache/tools/ant/types/Resource;)Z
    .locals 3
    .param p1    # Lorg/apache/tools/ant/types/Resource;

    iget-object v0, p0, Lorg/apache/tools/ant/util/ResourceUtils$Outdated;->control:Lorg/apache/tools/ant/types/Resource;

    iget-wide v1, p0, Lorg/apache/tools/ant/util/ResourceUtils$Outdated;->granularity:J

    invoke-static {v0, p1, v1, v2}, Lorg/apache/tools/ant/types/selectors/SelectorUtils;->isOutOfDate(Lorg/apache/tools/ant/types/Resource;Lorg/apache/tools/ant/types/Resource;J)Z

    move-result v0

    return v0
.end method
