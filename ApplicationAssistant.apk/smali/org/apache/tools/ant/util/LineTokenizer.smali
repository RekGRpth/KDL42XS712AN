.class public Lorg/apache/tools/ant/util/LineTokenizer;
.super Lorg/apache/tools/ant/ProjectComponent;
.source "LineTokenizer.java"

# interfaces
.implements Lorg/apache/tools/ant/util/Tokenizer;


# instance fields
.field private includeDelims:Z

.field private lineEnd:Ljava/lang/String;

.field private pushed:I


# direct methods
.method public constructor <init>()V
    .locals 1

    invoke-direct {p0}, Lorg/apache/tools/ant/ProjectComponent;-><init>()V

    const-string v0, ""

    iput-object v0, p0, Lorg/apache/tools/ant/util/LineTokenizer;->lineEnd:Ljava/lang/String;

    const/4 v0, -0x2

    iput v0, p0, Lorg/apache/tools/ant/util/LineTokenizer;->pushed:I

    const/4 v0, 0x0

    iput-boolean v0, p0, Lorg/apache/tools/ant/util/LineTokenizer;->includeDelims:Z

    return-void
.end method


# virtual methods
.method public getPostToken()Ljava/lang/String;
    .locals 1

    iget-boolean v0, p0, Lorg/apache/tools/ant/util/LineTokenizer;->includeDelims:Z

    if-eqz v0, :cond_0

    const-string v0, ""

    :goto_0
    return-object v0

    :cond_0
    iget-object v0, p0, Lorg/apache/tools/ant/util/LineTokenizer;->lineEnd:Ljava/lang/String;

    goto :goto_0
.end method

.method public getToken(Ljava/io/Reader;)Ljava/lang/String;
    .locals 7
    .param p1    # Ljava/io/Reader;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    const/16 v6, 0xa

    const/4 v5, -0x2

    const/4 v4, -0x1

    const/4 v0, -0x1

    iget v3, p0, Lorg/apache/tools/ant/util/LineTokenizer;->pushed:I

    if-eq v3, v5, :cond_0

    iget v0, p0, Lorg/apache/tools/ant/util/LineTokenizer;->pushed:I

    iput v5, p0, Lorg/apache/tools/ant/util/LineTokenizer;->pushed:I

    :goto_0
    if-ne v0, v4, :cond_1

    const/4 v3, 0x0

    :goto_1
    return-object v3

    :cond_0
    invoke-virtual {p1}, Ljava/io/Reader;->read()I

    move-result v0

    goto :goto_0

    :cond_1
    const-string v3, ""

    iput-object v3, p0, Lorg/apache/tools/ant/util/LineTokenizer;->lineEnd:Ljava/lang/String;

    new-instance v1, Ljava/lang/StringBuffer;

    invoke-direct {v1}, Ljava/lang/StringBuffer;-><init>()V

    const/4 v2, 0x0

    :goto_2
    if-eq v0, v4, :cond_3

    if-nez v2, :cond_7

    const/16 v3, 0xd

    if-ne v0, v3, :cond_2

    const/4 v2, 0x1

    :goto_3
    invoke-virtual {p1}, Ljava/io/Reader;->read()I

    move-result v0

    goto :goto_2

    :cond_2
    if-ne v0, v6, :cond_6

    const-string v3, "\n"

    iput-object v3, p0, Lorg/apache/tools/ant/util/LineTokenizer;->lineEnd:Ljava/lang/String;

    :cond_3
    :goto_4
    if-ne v0, v4, :cond_4

    const/4 v3, 0x1

    if-ne v2, v3, :cond_4

    const-string v3, "\r"

    iput-object v3, p0, Lorg/apache/tools/ant/util/LineTokenizer;->lineEnd:Ljava/lang/String;

    :cond_4
    iget-boolean v3, p0, Lorg/apache/tools/ant/util/LineTokenizer;->includeDelims:Z

    if-eqz v3, :cond_5

    iget-object v3, p0, Lorg/apache/tools/ant/util/LineTokenizer;->lineEnd:Ljava/lang/String;

    invoke-virtual {v1, v3}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    :cond_5
    invoke-virtual {v1}, Ljava/lang/StringBuffer;->toString()Ljava/lang/String;

    move-result-object v3

    goto :goto_1

    :cond_6
    int-to-char v3, v0

    invoke-virtual {v1, v3}, Ljava/lang/StringBuffer;->append(C)Ljava/lang/StringBuffer;

    goto :goto_3

    :cond_7
    const/4 v2, 0x0

    if-ne v0, v6, :cond_8

    const-string v3, "\r\n"

    iput-object v3, p0, Lorg/apache/tools/ant/util/LineTokenizer;->lineEnd:Ljava/lang/String;

    goto :goto_4

    :cond_8
    iput v0, p0, Lorg/apache/tools/ant/util/LineTokenizer;->pushed:I

    const-string v3, "\r"

    iput-object v3, p0, Lorg/apache/tools/ant/util/LineTokenizer;->lineEnd:Ljava/lang/String;

    goto :goto_4
.end method

.method public setIncludeDelims(Z)V
    .locals 0
    .param p1    # Z

    iput-boolean p1, p0, Lorg/apache/tools/ant/util/LineTokenizer;->includeDelims:Z

    return-void
.end method
