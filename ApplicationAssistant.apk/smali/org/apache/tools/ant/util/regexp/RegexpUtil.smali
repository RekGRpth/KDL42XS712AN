.class public Lorg/apache/tools/ant/util/regexp/RegexpUtil;
.super Ljava/lang/Object;
.source "RegexpUtil.java"


# direct methods
.method public constructor <init>()V
    .locals 0

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method public static hasFlag(II)Z
    .locals 1
    .param p0    # I
    .param p1    # I

    and-int v0, p0, p1

    if-lez v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public static removeFlag(II)I
    .locals 1
    .param p0    # I
    .param p1    # I

    rsub-int/lit8 v0, p1, -0x1

    and-int/2addr v0, p0

    return v0
.end method
