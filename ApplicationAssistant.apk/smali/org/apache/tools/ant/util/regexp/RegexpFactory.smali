.class public Lorg/apache/tools/ant/util/regexp/RegexpFactory;
.super Lorg/apache/tools/ant/util/regexp/RegexpMatcherFactory;
.source "RegexpFactory.java"


# direct methods
.method public constructor <init>()V
    .locals 0

    invoke-direct {p0}, Lorg/apache/tools/ant/util/regexp/RegexpMatcherFactory;-><init>()V

    return-void
.end method


# virtual methods
.method protected createRegexpInstance(Ljava/lang/String;)Lorg/apache/tools/ant/util/regexp/Regexp;
    .locals 4
    .param p1    # Ljava/lang/String;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lorg/apache/tools/ant/BuildException;
        }
    .end annotation

    invoke-virtual {p0, p1}, Lorg/apache/tools/ant/util/regexp/RegexpFactory;->createInstance(Ljava/lang/String;)Lorg/apache/tools/ant/util/regexp/RegexpMatcher;

    move-result-object v0

    instance-of v1, v0, Lorg/apache/tools/ant/util/regexp/Regexp;

    if-eqz v1, :cond_0

    check-cast v0, Lorg/apache/tools/ant/util/regexp/Regexp;

    return-object v0

    :cond_0
    new-instance v1, Lorg/apache/tools/ant/BuildException;

    new-instance v2, Ljava/lang/StringBuffer;

    invoke-direct {v2}, Ljava/lang/StringBuffer;-><init>()V

    invoke-virtual {v2, p1}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    move-result-object v2

    const-string v3, " doesn\'t implement the Regexp interface"

    invoke-virtual {v2, v3}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuffer;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-direct {v1, v2}, Lorg/apache/tools/ant/BuildException;-><init>(Ljava/lang/String;)V

    throw v1
.end method

.method public newRegexp()Lorg/apache/tools/ant/util/regexp/Regexp;
    .locals 1
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lorg/apache/tools/ant/BuildException;
        }
    .end annotation

    const/4 v0, 0x0

    invoke-virtual {p0, v0}, Lorg/apache/tools/ant/util/regexp/RegexpFactory;->newRegexp(Lorg/apache/tools/ant/Project;)Lorg/apache/tools/ant/util/regexp/Regexp;

    move-result-object v0

    return-object v0
.end method

.method public newRegexp(Lorg/apache/tools/ant/Project;)Lorg/apache/tools/ant/util/regexp/Regexp;
    .locals 7
    .param p1    # Lorg/apache/tools/ant/Project;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lorg/apache/tools/ant/BuildException;
        }
    .end annotation

    const/4 v4, 0x1

    const/4 v2, 0x0

    if-nez p1, :cond_0

    const-string v3, "ant.regexp.regexpimpl"

    invoke-static {v3}, Ljava/lang/System;->getProperty(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v2

    :goto_0
    if-eqz v2, :cond_1

    invoke-virtual {p0, v2}, Lorg/apache/tools/ant/util/regexp/RegexpFactory;->createRegexpInstance(Ljava/lang/String;)Lorg/apache/tools/ant/util/regexp/Regexp;

    move-result-object v3

    :goto_1
    return-object v3

    :cond_0
    const-string v3, "ant.regexp.regexpimpl"

    invoke-virtual {p1, v3}, Lorg/apache/tools/ant/Project;->getProperty(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v2

    goto :goto_0

    :cond_1
    const/4 v1, 0x0

    :try_start_0
    const-string v3, "java.util.regex.Matcher"

    invoke-virtual {p0, v3}, Lorg/apache/tools/ant/util/regexp/RegexpFactory;->testAvailability(Ljava/lang/String;)V

    const-string v3, "org.apache.tools.ant.util.regexp.Jdk14RegexpRegexp"

    invoke-virtual {p0, v3}, Lorg/apache/tools/ant/util/regexp/RegexpFactory;->createRegexpInstance(Ljava/lang/String;)Lorg/apache/tools/ant/util/regexp/Regexp;
    :try_end_0
    .catch Lorg/apache/tools/ant/BuildException; {:try_start_0 .. :try_end_0} :catch_0

    move-result-object v3

    goto :goto_1

    :catch_0
    move-exception v0

    invoke-static {}, Lorg/apache/tools/ant/util/JavaEnvUtils;->getJavaVersionNumber()I

    move-result v3

    const/16 v5, 0xe

    if-ge v3, v5, :cond_2

    move v3, v4

    :goto_2
    invoke-static {v1, v0, v3}, Lorg/apache/tools/ant/util/regexp/RegexpFactory;->orCause(Ljava/lang/Throwable;Lorg/apache/tools/ant/BuildException;Z)Ljava/lang/Throwable;

    move-result-object v1

    :try_start_1
    const-string v3, "org.apache.oro.text.regex.Pattern"

    invoke-virtual {p0, v3}, Lorg/apache/tools/ant/util/regexp/RegexpFactory;->testAvailability(Ljava/lang/String;)V

    const-string v3, "org.apache.tools.ant.util.regexp.JakartaOroRegexp"

    invoke-virtual {p0, v3}, Lorg/apache/tools/ant/util/regexp/RegexpFactory;->createRegexpInstance(Ljava/lang/String;)Lorg/apache/tools/ant/util/regexp/Regexp;
    :try_end_1
    .catch Lorg/apache/tools/ant/BuildException; {:try_start_1 .. :try_end_1} :catch_1

    move-result-object v3

    goto :goto_1

    :cond_2
    const/4 v3, 0x0

    goto :goto_2

    :catch_1
    move-exception v0

    invoke-static {v1, v0, v4}, Lorg/apache/tools/ant/util/regexp/RegexpFactory;->orCause(Ljava/lang/Throwable;Lorg/apache/tools/ant/BuildException;Z)Ljava/lang/Throwable;

    move-result-object v1

    :try_start_2
    const-string v3, "org.apache.regexp.RE"

    invoke-virtual {p0, v3}, Lorg/apache/tools/ant/util/regexp/RegexpFactory;->testAvailability(Ljava/lang/String;)V

    const-string v3, "org.apache.tools.ant.util.regexp.JakartaRegexpRegexp"

    invoke-virtual {p0, v3}, Lorg/apache/tools/ant/util/regexp/RegexpFactory;->createRegexpInstance(Ljava/lang/String;)Lorg/apache/tools/ant/util/regexp/Regexp;
    :try_end_2
    .catch Lorg/apache/tools/ant/BuildException; {:try_start_2 .. :try_end_2} :catch_2

    move-result-object v3

    goto :goto_1

    :catch_2
    move-exception v0

    invoke-static {v1, v0, v4}, Lorg/apache/tools/ant/util/regexp/RegexpFactory;->orCause(Ljava/lang/Throwable;Lorg/apache/tools/ant/BuildException;Z)Ljava/lang/Throwable;

    move-result-object v1

    new-instance v4, Lorg/apache/tools/ant/BuildException;

    new-instance v3, Ljava/lang/StringBuffer;

    invoke-direct {v3}, Ljava/lang/StringBuffer;-><init>()V

    const-string v5, "No supported regular expression matcher found"

    invoke-virtual {v3, v5}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    move-result-object v5

    if-eqz v1, :cond_3

    new-instance v3, Ljava/lang/StringBuffer;

    invoke-direct {v3}, Ljava/lang/StringBuffer;-><init>()V

    const-string v6, ": "

    invoke-virtual {v3, v6}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    move-result-object v3

    invoke-virtual {v3, v1}, Ljava/lang/StringBuffer;->append(Ljava/lang/Object;)Ljava/lang/StringBuffer;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuffer;->toString()Ljava/lang/String;

    move-result-object v3

    :goto_3
    invoke-virtual {v5, v3}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuffer;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-direct {v4, v3, v1}, Lorg/apache/tools/ant/BuildException;-><init>(Ljava/lang/String;Ljava/lang/Throwable;)V

    throw v4

    :cond_3
    const-string v3, ""

    goto :goto_3
.end method
