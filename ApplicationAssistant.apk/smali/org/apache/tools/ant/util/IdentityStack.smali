.class public Lorg/apache/tools/ant/util/IdentityStack;
.super Ljava/util/Stack;
.source "IdentityStack.java"


# direct methods
.method public constructor <init>()V
    .locals 0

    invoke-direct {p0}, Ljava/util/Stack;-><init>()V

    return-void
.end method

.method public constructor <init>(Ljava/lang/Object;)V
    .locals 0
    .param p1    # Ljava/lang/Object;

    invoke-direct {p0}, Ljava/util/Stack;-><init>()V

    invoke-virtual {p0, p1}, Lorg/apache/tools/ant/util/IdentityStack;->push(Ljava/lang/Object;)Ljava/lang/Object;

    return-void
.end method

.method public static getInstance(Ljava/util/Stack;)Lorg/apache/tools/ant/util/IdentityStack;
    .locals 2
    .param p0    # Ljava/util/Stack;

    instance-of v1, p0, Lorg/apache/tools/ant/util/IdentityStack;

    if-eqz v1, :cond_0

    check-cast p0, Lorg/apache/tools/ant/util/IdentityStack;

    :goto_0
    return-object p0

    :cond_0
    new-instance v0, Lorg/apache/tools/ant/util/IdentityStack;

    invoke-direct {v0}, Lorg/apache/tools/ant/util/IdentityStack;-><init>()V

    if-eqz p0, :cond_1

    invoke-virtual {v0, p0}, Lorg/apache/tools/ant/util/IdentityStack;->addAll(Ljava/util/Collection;)Z

    :cond_1
    move-object p0, v0

    goto :goto_0
.end method


# virtual methods
.method public declared-synchronized contains(Ljava/lang/Object;)Z
    .locals 1
    .param p1    # Ljava/lang/Object;

    monitor-enter p0

    :try_start_0
    invoke-virtual {p0, p1}, Lorg/apache/tools/ant/util/IdentityStack;->indexOf(Ljava/lang/Object;)I
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    move-result v0

    if-ltz v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    monitor-exit p0

    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0

    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method public declared-synchronized indexOf(Ljava/lang/Object;I)I
    .locals 2
    .param p1    # Ljava/lang/Object;
    .param p2    # I

    monitor-enter p0

    move v0, p2

    :goto_0
    :try_start_0
    invoke-virtual {p0}, Lorg/apache/tools/ant/util/IdentityStack;->size()I

    move-result v1

    if-ge v0, v1, :cond_1

    invoke-virtual {p0, v0}, Lorg/apache/tools/ant/util/IdentityStack;->get(I)Ljava/lang/Object;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    move-result-object v1

    if-ne v1, p1, :cond_0

    :goto_1
    monitor-exit p0

    return v0

    :cond_0
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    :cond_1
    const/4 v0, -0x1

    goto :goto_1

    :catchall_0
    move-exception v1

    monitor-exit p0

    throw v1
.end method

.method public declared-synchronized lastIndexOf(Ljava/lang/Object;I)I
    .locals 2
    .param p1    # Ljava/lang/Object;
    .param p2    # I

    monitor-enter p0

    move v0, p2

    :goto_0
    if-ltz v0, :cond_1

    :try_start_0
    invoke-virtual {p0, v0}, Lorg/apache/tools/ant/util/IdentityStack;->get(I)Ljava/lang/Object;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    move-result-object v1

    if-ne v1, p1, :cond_0

    :goto_1
    monitor-exit p0

    return v0

    :cond_0
    add-int/lit8 v0, v0, -0x1

    goto :goto_0

    :cond_1
    const/4 v0, -0x1

    goto :goto_1

    :catchall_0
    move-exception v1

    monitor-exit p0

    throw v1
.end method
