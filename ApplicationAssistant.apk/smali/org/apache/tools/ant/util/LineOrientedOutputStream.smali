.class public abstract Lorg/apache/tools/ant/util/LineOrientedOutputStream;
.super Ljava/io/OutputStream;
.source "LineOrientedOutputStream.java"


# static fields
.field private static final CR:I = 0xd

.field private static final INTIAL_SIZE:I = 0x84

.field private static final LF:I = 0xa


# instance fields
.field private buffer:Ljava/io/ByteArrayOutputStream;

.field private skip:Z


# direct methods
.method public constructor <init>()V
    .locals 2

    invoke-direct {p0}, Ljava/io/OutputStream;-><init>()V

    new-instance v0, Ljava/io/ByteArrayOutputStream;

    const/16 v1, 0x84

    invoke-direct {v0, v1}, Ljava/io/ByteArrayOutputStream;-><init>(I)V

    iput-object v0, p0, Lorg/apache/tools/ant/util/LineOrientedOutputStream;->buffer:Ljava/io/ByteArrayOutputStream;

    const/4 v0, 0x0

    iput-boolean v0, p0, Lorg/apache/tools/ant/util/LineOrientedOutputStream;->skip:Z

    return-void
.end method


# virtual methods
.method public final close()V
    .locals 1
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    iget-object v0, p0, Lorg/apache/tools/ant/util/LineOrientedOutputStream;->buffer:Ljava/io/ByteArrayOutputStream;

    invoke-virtual {v0}, Ljava/io/ByteArrayOutputStream;->size()I

    move-result v0

    if-lez v0, :cond_0

    invoke-virtual {p0}, Lorg/apache/tools/ant/util/LineOrientedOutputStream;->processBuffer()V

    :cond_0
    invoke-super {p0}, Ljava/io/OutputStream;->close()V

    return-void
.end method

.method public final flush()V
    .locals 1
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    iget-object v0, p0, Lorg/apache/tools/ant/util/LineOrientedOutputStream;->buffer:Ljava/io/ByteArrayOutputStream;

    invoke-virtual {v0}, Ljava/io/ByteArrayOutputStream;->size()I

    move-result v0

    if-lez v0, :cond_0

    invoke-virtual {p0}, Lorg/apache/tools/ant/util/LineOrientedOutputStream;->processBuffer()V

    :cond_0
    return-void
.end method

.method protected processBuffer()V
    .locals 2
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    :try_start_0
    iget-object v0, p0, Lorg/apache/tools/ant/util/LineOrientedOutputStream;->buffer:Ljava/io/ByteArrayOutputStream;

    invoke-virtual {v0}, Ljava/io/ByteArrayOutputStream;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p0, v0}, Lorg/apache/tools/ant/util/LineOrientedOutputStream;->processLine(Ljava/lang/String;)V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    iget-object v0, p0, Lorg/apache/tools/ant/util/LineOrientedOutputStream;->buffer:Ljava/io/ByteArrayOutputStream;

    invoke-virtual {v0}, Ljava/io/ByteArrayOutputStream;->reset()V

    return-void

    :catchall_0
    move-exception v0

    iget-object v1, p0, Lorg/apache/tools/ant/util/LineOrientedOutputStream;->buffer:Ljava/io/ByteArrayOutputStream;

    invoke-virtual {v1}, Ljava/io/ByteArrayOutputStream;->reset()V

    throw v0
.end method

.method protected abstract processLine(Ljava/lang/String;)V
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation
.end method

.method public final write(I)V
    .locals 3
    .param p1    # I
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    const/16 v2, 0xd

    int-to-byte v0, p1

    const/16 v1, 0xa

    if-eq v0, v1, :cond_0

    if-ne v0, v2, :cond_2

    :cond_0
    iget-boolean v1, p0, Lorg/apache/tools/ant/util/LineOrientedOutputStream;->skip:Z

    if-nez v1, :cond_1

    invoke-virtual {p0}, Lorg/apache/tools/ant/util/LineOrientedOutputStream;->processBuffer()V

    :cond_1
    :goto_0
    if-ne v0, v2, :cond_3

    const/4 v1, 0x1

    :goto_1
    iput-boolean v1, p0, Lorg/apache/tools/ant/util/LineOrientedOutputStream;->skip:Z

    return-void

    :cond_2
    iget-object v1, p0, Lorg/apache/tools/ant/util/LineOrientedOutputStream;->buffer:Ljava/io/ByteArrayOutputStream;

    invoke-virtual {v1, p1}, Ljava/io/ByteArrayOutputStream;->write(I)V

    goto :goto_0

    :cond_3
    const/4 v1, 0x0

    goto :goto_1
.end method

.method public final write([BII)V
    .locals 7
    .param p1    # [B
    .param p2    # I
    .param p3    # I
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    const/16 v6, 0xd

    const/16 v5, 0xa

    move v2, p2

    move v1, v2

    move v3, p3

    :goto_0
    if-lez v3, :cond_4

    :goto_1
    if-lez v3, :cond_0

    aget-byte v4, p1, v2

    if-eq v4, v5, :cond_0

    aget-byte v4, p1, v2

    if-eq v4, v6, :cond_0

    add-int/lit8 v2, v2, 0x1

    add-int/lit8 v3, v3, -0x1

    goto :goto_1

    :cond_0
    sub-int v0, v2, v1

    if-lez v0, :cond_1

    iget-object v4, p0, Lorg/apache/tools/ant/util/LineOrientedOutputStream;->buffer:Ljava/io/ByteArrayOutputStream;

    invoke-virtual {v4, p1, v1, v0}, Ljava/io/ByteArrayOutputStream;->write([BII)V

    :cond_1
    :goto_2
    if-lez v3, :cond_3

    aget-byte v4, p1, v2

    if-eq v4, v5, :cond_2

    aget-byte v4, p1, v2

    if-ne v4, v6, :cond_3

    :cond_2
    aget-byte v4, p1, v2

    invoke-virtual {p0, v4}, Lorg/apache/tools/ant/util/LineOrientedOutputStream;->write(I)V

    add-int/lit8 v2, v2, 0x1

    add-int/lit8 v3, v3, -0x1

    goto :goto_2

    :cond_3
    move v1, v2

    goto :goto_0

    :cond_4
    return-void
.end method
