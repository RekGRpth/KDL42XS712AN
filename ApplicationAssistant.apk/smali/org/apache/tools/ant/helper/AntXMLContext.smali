.class public Lorg/apache/tools/ant/helper/AntXMLContext;
.super Ljava/lang/Object;
.source "AntXMLContext.java"


# instance fields
.field private buildFile:Ljava/io/File;

.field private buildFileParent:Ljava/io/File;

.field private currentProjectName:Ljava/lang/String;

.field private currentTarget:Lorg/apache/tools/ant/Target;

.field private currentTargets:Ljava/util/Map;

.field private ignoreProjectTag:Z

.field private implicitTarget:Lorg/apache/tools/ant/Target;

.field private locator:Lorg/xml/sax/Locator;

.field private prefixMapping:Ljava/util/Map;

.field private project:Lorg/apache/tools/ant/Project;

.field private targetVector:Ljava/util/Vector;

.field private wStack:Ljava/util/Vector;


# direct methods
.method public constructor <init>(Lorg/apache/tools/ant/Project;)V
    .locals 2
    .param p1    # Lorg/apache/tools/ant/Project;

    const/4 v1, 0x0

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    new-instance v0, Ljava/util/Vector;

    invoke-direct {v0}, Ljava/util/Vector;-><init>()V

    iput-object v0, p0, Lorg/apache/tools/ant/helper/AntXMLContext;->targetVector:Ljava/util/Vector;

    new-instance v0, Lorg/apache/tools/ant/Target;

    invoke-direct {v0}, Lorg/apache/tools/ant/Target;-><init>()V

    iput-object v0, p0, Lorg/apache/tools/ant/helper/AntXMLContext;->implicitTarget:Lorg/apache/tools/ant/Target;

    iput-object v1, p0, Lorg/apache/tools/ant/helper/AntXMLContext;->currentTarget:Lorg/apache/tools/ant/Target;

    new-instance v0, Ljava/util/Vector;

    invoke-direct {v0}, Ljava/util/Vector;-><init>()V

    iput-object v0, p0, Lorg/apache/tools/ant/helper/AntXMLContext;->wStack:Ljava/util/Vector;

    const/4 v0, 0x0

    iput-boolean v0, p0, Lorg/apache/tools/ant/helper/AntXMLContext;->ignoreProjectTag:Z

    new-instance v0, Ljava/util/HashMap;

    invoke-direct {v0}, Ljava/util/HashMap;-><init>()V

    iput-object v0, p0, Lorg/apache/tools/ant/helper/AntXMLContext;->prefixMapping:Ljava/util/Map;

    iput-object v1, p0, Lorg/apache/tools/ant/helper/AntXMLContext;->currentTargets:Ljava/util/Map;

    iput-object p1, p0, Lorg/apache/tools/ant/helper/AntXMLContext;->project:Lorg/apache/tools/ant/Project;

    iget-object v0, p0, Lorg/apache/tools/ant/helper/AntXMLContext;->implicitTarget:Lorg/apache/tools/ant/Target;

    invoke-virtual {v0, p1}, Lorg/apache/tools/ant/Target;->setProject(Lorg/apache/tools/ant/Project;)V

    iget-object v0, p0, Lorg/apache/tools/ant/helper/AntXMLContext;->implicitTarget:Lorg/apache/tools/ant/Target;

    const-string v1, ""

    invoke-virtual {v0, v1}, Lorg/apache/tools/ant/Target;->setName(Ljava/lang/String;)V

    iget-object v0, p0, Lorg/apache/tools/ant/helper/AntXMLContext;->targetVector:Ljava/util/Vector;

    iget-object v1, p0, Lorg/apache/tools/ant/helper/AntXMLContext;->implicitTarget:Lorg/apache/tools/ant/Target;

    invoke-virtual {v0, v1}, Ljava/util/Vector;->addElement(Ljava/lang/Object;)V

    return-void
.end method


# virtual methods
.method public addTarget(Lorg/apache/tools/ant/Target;)V
    .locals 1
    .param p1    # Lorg/apache/tools/ant/Target;

    iget-object v0, p0, Lorg/apache/tools/ant/helper/AntXMLContext;->targetVector:Ljava/util/Vector;

    invoke-virtual {v0, p1}, Ljava/util/Vector;->addElement(Ljava/lang/Object;)V

    iput-object p1, p0, Lorg/apache/tools/ant/helper/AntXMLContext;->currentTarget:Lorg/apache/tools/ant/Target;

    return-void
.end method

.method public configureId(Ljava/lang/Object;Lorg/xml/sax/Attributes;)V
    .locals 2
    .param p1    # Ljava/lang/Object;
    .param p2    # Lorg/xml/sax/Attributes;

    const-string v1, "id"

    invoke-interface {p2, v1}, Lorg/xml/sax/Attributes;->getValue(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    if-eqz v0, :cond_0

    iget-object v1, p0, Lorg/apache/tools/ant/helper/AntXMLContext;->project:Lorg/apache/tools/ant/Project;

    invoke-virtual {v1, v0, p1}, Lorg/apache/tools/ant/Project;->addIdReference(Ljava/lang/String;Ljava/lang/Object;)V

    :cond_0
    return-void
.end method

.method public currentWrapper()Lorg/apache/tools/ant/RuntimeConfigurable;
    .locals 2

    iget-object v0, p0, Lorg/apache/tools/ant/helper/AntXMLContext;->wStack:Ljava/util/Vector;

    invoke-virtual {v0}, Ljava/util/Vector;->size()I

    move-result v0

    const/4 v1, 0x1

    if-ge v0, v1, :cond_0

    const/4 v0, 0x0

    :goto_0
    return-object v0

    :cond_0
    iget-object v0, p0, Lorg/apache/tools/ant/helper/AntXMLContext;->wStack:Ljava/util/Vector;

    iget-object v1, p0, Lorg/apache/tools/ant/helper/AntXMLContext;->wStack:Ljava/util/Vector;

    invoke-virtual {v1}, Ljava/util/Vector;->size()I

    move-result v1

    add-int/lit8 v1, v1, -0x1

    invoke-virtual {v0, v1}, Ljava/util/Vector;->elementAt(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lorg/apache/tools/ant/RuntimeConfigurable;

    goto :goto_0
.end method

.method public endPrefixMapping(Ljava/lang/String;)V
    .locals 2
    .param p1    # Ljava/lang/String;

    iget-object v1, p0, Lorg/apache/tools/ant/helper/AntXMLContext;->prefixMapping:Ljava/util/Map;

    invoke-interface {v1, p1}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/util/List;

    if-eqz v0, :cond_0

    invoke-interface {v0}, Ljava/util/List;->size()I

    move-result v1

    if-nez v1, :cond_1

    :cond_0
    :goto_0
    return-void

    :cond_1
    invoke-interface {v0}, Ljava/util/List;->size()I

    move-result v1

    add-int/lit8 v1, v1, -0x1

    invoke-interface {v0, v1}, Ljava/util/List;->remove(I)Ljava/lang/Object;

    goto :goto_0
.end method

.method public getBuildFile()Ljava/io/File;
    .locals 1

    iget-object v0, p0, Lorg/apache/tools/ant/helper/AntXMLContext;->buildFile:Ljava/io/File;

    return-object v0
.end method

.method public getBuildFileParent()Ljava/io/File;
    .locals 1

    iget-object v0, p0, Lorg/apache/tools/ant/helper/AntXMLContext;->buildFileParent:Ljava/io/File;

    return-object v0
.end method

.method public getCurrentProjectName()Ljava/lang/String;
    .locals 1

    iget-object v0, p0, Lorg/apache/tools/ant/helper/AntXMLContext;->currentProjectName:Ljava/lang/String;

    return-object v0
.end method

.method public getCurrentTarget()Lorg/apache/tools/ant/Target;
    .locals 1

    iget-object v0, p0, Lorg/apache/tools/ant/helper/AntXMLContext;->currentTarget:Lorg/apache/tools/ant/Target;

    return-object v0
.end method

.method public getCurrentTargets()Ljava/util/Map;
    .locals 1

    iget-object v0, p0, Lorg/apache/tools/ant/helper/AntXMLContext;->currentTargets:Ljava/util/Map;

    return-object v0
.end method

.method public getImplicitTarget()Lorg/apache/tools/ant/Target;
    .locals 1

    iget-object v0, p0, Lorg/apache/tools/ant/helper/AntXMLContext;->implicitTarget:Lorg/apache/tools/ant/Target;

    return-object v0
.end method

.method public getLocator()Lorg/xml/sax/Locator;
    .locals 1

    iget-object v0, p0, Lorg/apache/tools/ant/helper/AntXMLContext;->locator:Lorg/xml/sax/Locator;

    return-object v0
.end method

.method public getPrefixMapping(Ljava/lang/String;)Ljava/lang/String;
    .locals 2
    .param p1    # Ljava/lang/String;

    iget-object v1, p0, Lorg/apache/tools/ant/helper/AntXMLContext;->prefixMapping:Ljava/util/Map;

    invoke-interface {v1, p1}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/util/List;

    if-eqz v0, :cond_0

    invoke-interface {v0}, Ljava/util/List;->size()I

    move-result v1

    if-nez v1, :cond_1

    :cond_0
    const/4 v1, 0x0

    :goto_0
    return-object v1

    :cond_1
    invoke-interface {v0}, Ljava/util/List;->size()I

    move-result v1

    add-int/lit8 v1, v1, -0x1

    invoke-interface {v0, v1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Ljava/lang/String;

    goto :goto_0
.end method

.method public getProject()Lorg/apache/tools/ant/Project;
    .locals 1

    iget-object v0, p0, Lorg/apache/tools/ant/helper/AntXMLContext;->project:Lorg/apache/tools/ant/Project;

    return-object v0
.end method

.method public getTargets()Ljava/util/Vector;
    .locals 1

    iget-object v0, p0, Lorg/apache/tools/ant/helper/AntXMLContext;->targetVector:Ljava/util/Vector;

    return-object v0
.end method

.method public getWrapperStack()Ljava/util/Vector;
    .locals 1

    iget-object v0, p0, Lorg/apache/tools/ant/helper/AntXMLContext;->wStack:Ljava/util/Vector;

    return-object v0
.end method

.method public isIgnoringProjectTag()Z
    .locals 1

    iget-boolean v0, p0, Lorg/apache/tools/ant/helper/AntXMLContext;->ignoreProjectTag:Z

    return v0
.end method

.method public parentWrapper()Lorg/apache/tools/ant/RuntimeConfigurable;
    .locals 2

    iget-object v0, p0, Lorg/apache/tools/ant/helper/AntXMLContext;->wStack:Ljava/util/Vector;

    invoke-virtual {v0}, Ljava/util/Vector;->size()I

    move-result v0

    const/4 v1, 0x2

    if-ge v0, v1, :cond_0

    const/4 v0, 0x0

    :goto_0
    return-object v0

    :cond_0
    iget-object v0, p0, Lorg/apache/tools/ant/helper/AntXMLContext;->wStack:Ljava/util/Vector;

    iget-object v1, p0, Lorg/apache/tools/ant/helper/AntXMLContext;->wStack:Ljava/util/Vector;

    invoke-virtual {v1}, Ljava/util/Vector;->size()I

    move-result v1

    add-int/lit8 v1, v1, -0x2

    invoke-virtual {v0, v1}, Ljava/util/Vector;->elementAt(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lorg/apache/tools/ant/RuntimeConfigurable;

    goto :goto_0
.end method

.method public popWrapper()V
    .locals 2

    iget-object v0, p0, Lorg/apache/tools/ant/helper/AntXMLContext;->wStack:Ljava/util/Vector;

    invoke-virtual {v0}, Ljava/util/Vector;->size()I

    move-result v0

    if-lez v0, :cond_0

    iget-object v0, p0, Lorg/apache/tools/ant/helper/AntXMLContext;->wStack:Ljava/util/Vector;

    iget-object v1, p0, Lorg/apache/tools/ant/helper/AntXMLContext;->wStack:Ljava/util/Vector;

    invoke-virtual {v1}, Ljava/util/Vector;->size()I

    move-result v1

    add-int/lit8 v1, v1, -0x1

    invoke-virtual {v0, v1}, Ljava/util/Vector;->removeElementAt(I)V

    :cond_0
    return-void
.end method

.method public pushWrapper(Lorg/apache/tools/ant/RuntimeConfigurable;)V
    .locals 1
    .param p1    # Lorg/apache/tools/ant/RuntimeConfigurable;

    iget-object v0, p0, Lorg/apache/tools/ant/helper/AntXMLContext;->wStack:Ljava/util/Vector;

    invoke-virtual {v0, p1}, Ljava/util/Vector;->addElement(Ljava/lang/Object;)V

    return-void
.end method

.method public setBuildFile(Ljava/io/File;)V
    .locals 3
    .param p1    # Ljava/io/File;

    iput-object p1, p0, Lorg/apache/tools/ant/helper/AntXMLContext;->buildFile:Ljava/io/File;

    new-instance v0, Ljava/io/File;

    invoke-virtual {p1}, Ljava/io/File;->getParent()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/io/File;-><init>(Ljava/lang/String;)V

    iput-object v0, p0, Lorg/apache/tools/ant/helper/AntXMLContext;->buildFileParent:Ljava/io/File;

    iget-object v0, p0, Lorg/apache/tools/ant/helper/AntXMLContext;->implicitTarget:Lorg/apache/tools/ant/Target;

    new-instance v1, Lorg/apache/tools/ant/Location;

    invoke-virtual {p1}, Ljava/io/File;->getAbsolutePath()Ljava/lang/String;

    move-result-object v2

    invoke-direct {v1, v2}, Lorg/apache/tools/ant/Location;-><init>(Ljava/lang/String;)V

    invoke-virtual {v0, v1}, Lorg/apache/tools/ant/Target;->setLocation(Lorg/apache/tools/ant/Location;)V

    return-void
.end method

.method public setCurrentProjectName(Ljava/lang/String;)V
    .locals 0
    .param p1    # Ljava/lang/String;

    iput-object p1, p0, Lorg/apache/tools/ant/helper/AntXMLContext;->currentProjectName:Ljava/lang/String;

    return-void
.end method

.method public setCurrentTarget(Lorg/apache/tools/ant/Target;)V
    .locals 0
    .param p1    # Lorg/apache/tools/ant/Target;

    iput-object p1, p0, Lorg/apache/tools/ant/helper/AntXMLContext;->currentTarget:Lorg/apache/tools/ant/Target;

    return-void
.end method

.method public setCurrentTargets(Ljava/util/Map;)V
    .locals 0
    .param p1    # Ljava/util/Map;

    iput-object p1, p0, Lorg/apache/tools/ant/helper/AntXMLContext;->currentTargets:Ljava/util/Map;

    return-void
.end method

.method public setIgnoreProjectTag(Z)V
    .locals 0
    .param p1    # Z

    iput-boolean p1, p0, Lorg/apache/tools/ant/helper/AntXMLContext;->ignoreProjectTag:Z

    return-void
.end method

.method public setImplicitTarget(Lorg/apache/tools/ant/Target;)V
    .locals 0
    .param p1    # Lorg/apache/tools/ant/Target;

    iput-object p1, p0, Lorg/apache/tools/ant/helper/AntXMLContext;->implicitTarget:Lorg/apache/tools/ant/Target;

    return-void
.end method

.method public setLocator(Lorg/xml/sax/Locator;)V
    .locals 0
    .param p1    # Lorg/xml/sax/Locator;

    iput-object p1, p0, Lorg/apache/tools/ant/helper/AntXMLContext;->locator:Lorg/xml/sax/Locator;

    return-void
.end method

.method public startPrefixMapping(Ljava/lang/String;Ljava/lang/String;)V
    .locals 2
    .param p1    # Ljava/lang/String;
    .param p2    # Ljava/lang/String;

    iget-object v1, p0, Lorg/apache/tools/ant/helper/AntXMLContext;->prefixMapping:Ljava/util/Map;

    invoke-interface {v1, p1}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/util/List;

    if-nez v0, :cond_0

    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iget-object v1, p0, Lorg/apache/tools/ant/helper/AntXMLContext;->prefixMapping:Ljava/util/Map;

    invoke-interface {v1, p1, v0}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    :cond_0
    invoke-interface {v0, p2}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    return-void
.end method
