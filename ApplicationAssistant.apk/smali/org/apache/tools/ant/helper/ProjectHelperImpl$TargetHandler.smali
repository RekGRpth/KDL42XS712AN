.class Lorg/apache/tools/ant/helper/ProjectHelperImpl$TargetHandler;
.super Lorg/apache/tools/ant/helper/ProjectHelperImpl$AbstractHandler;
.source "ProjectHelperImpl.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lorg/apache/tools/ant/helper/ProjectHelperImpl;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x8
    name = "TargetHandler"
.end annotation


# instance fields
.field private target:Lorg/apache/tools/ant/Target;


# direct methods
.method public constructor <init>(Lorg/apache/tools/ant/helper/ProjectHelperImpl;Lorg/xml/sax/DocumentHandler;)V
    .locals 0
    .param p1    # Lorg/apache/tools/ant/helper/ProjectHelperImpl;
    .param p2    # Lorg/xml/sax/DocumentHandler;

    invoke-direct {p0, p1, p2}, Lorg/apache/tools/ant/helper/ProjectHelperImpl$AbstractHandler;-><init>(Lorg/apache/tools/ant/helper/ProjectHelperImpl;Lorg/xml/sax/DocumentHandler;)V

    return-void
.end method


# virtual methods
.method public init(Ljava/lang/String;Lorg/xml/sax/AttributeList;)V
    .locals 13
    .param p1    # Ljava/lang/String;
    .param p2    # Lorg/xml/sax/AttributeList;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lorg/xml/sax/SAXParseException;
        }
    .end annotation

    const/4 v6, 0x0

    const-string v0, ""

    const/4 v4, 0x0

    const/4 v7, 0x0

    const/4 v3, 0x0

    const/4 v1, 0x0

    const/4 v2, 0x0

    :goto_0
    invoke-interface {p2}, Lorg/xml/sax/AttributeList;->getLength()I

    move-result v9

    if-ge v2, v9, :cond_7

    invoke-interface {p2, v2}, Lorg/xml/sax/AttributeList;->getName(I)Ljava/lang/String;

    move-result-object v5

    invoke-interface {p2, v2}, Lorg/xml/sax/AttributeList;->getValue(I)Ljava/lang/String;

    move-result-object v8

    const-string v9, "name"

    invoke-virtual {v5, v9}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v9

    if-eqz v9, :cond_0

    move-object v6, v8

    const-string v9, ""

    invoke-virtual {v6, v9}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v9

    if-eqz v9, :cond_1

    new-instance v9, Lorg/apache/tools/ant/BuildException;

    const-string v10, "name attribute must not be empty"

    new-instance v11, Lorg/apache/tools/ant/Location;

    iget-object v12, p0, Lorg/apache/tools/ant/helper/ProjectHelperImpl$TargetHandler;->helperImpl:Lorg/apache/tools/ant/helper/ProjectHelperImpl;

    invoke-static {v12}, Lorg/apache/tools/ant/helper/ProjectHelperImpl;->access$100(Lorg/apache/tools/ant/helper/ProjectHelperImpl;)Lorg/xml/sax/Locator;

    move-result-object v12

    invoke-direct {v11, v12}, Lorg/apache/tools/ant/Location;-><init>(Lorg/xml/sax/Locator;)V

    invoke-direct {v9, v10, v11}, Lorg/apache/tools/ant/BuildException;-><init>(Ljava/lang/String;Lorg/apache/tools/ant/Location;)V

    throw v9

    :cond_0
    const-string v9, "depends"

    invoke-virtual {v5, v9}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v9

    if-eqz v9, :cond_2

    move-object v0, v8

    :cond_1
    :goto_1
    add-int/lit8 v2, v2, 0x1

    goto :goto_0

    :cond_2
    const-string v9, "if"

    invoke-virtual {v5, v9}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v9

    if-eqz v9, :cond_3

    move-object v4, v8

    goto :goto_1

    :cond_3
    const-string v9, "unless"

    invoke-virtual {v5, v9}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v9

    if-eqz v9, :cond_4

    move-object v7, v8

    goto :goto_1

    :cond_4
    const-string v9, "id"

    invoke-virtual {v5, v9}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v9

    if-eqz v9, :cond_5

    move-object v3, v8

    goto :goto_1

    :cond_5
    const-string v9, "description"

    invoke-virtual {v5, v9}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v9

    if-eqz v9, :cond_6

    move-object v1, v8

    goto :goto_1

    :cond_6
    new-instance v9, Lorg/xml/sax/SAXParseException;

    new-instance v10, Ljava/lang/StringBuffer;

    invoke-direct {v10}, Ljava/lang/StringBuffer;-><init>()V

    const-string v11, "Unexpected attribute \""

    invoke-virtual {v10, v11}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    move-result-object v10

    invoke-virtual {v10, v5}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    move-result-object v10

    const-string v11, "\""

    invoke-virtual {v10, v11}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    move-result-object v10

    invoke-virtual {v10}, Ljava/lang/StringBuffer;->toString()Ljava/lang/String;

    move-result-object v10

    iget-object v11, p0, Lorg/apache/tools/ant/helper/ProjectHelperImpl$TargetHandler;->helperImpl:Lorg/apache/tools/ant/helper/ProjectHelperImpl;

    invoke-static {v11}, Lorg/apache/tools/ant/helper/ProjectHelperImpl;->access$100(Lorg/apache/tools/ant/helper/ProjectHelperImpl;)Lorg/xml/sax/Locator;

    move-result-object v11

    invoke-direct {v9, v10, v11}, Lorg/xml/sax/SAXParseException;-><init>(Ljava/lang/String;Lorg/xml/sax/Locator;)V

    throw v9

    :cond_7
    if-nez v6, :cond_8

    new-instance v9, Lorg/xml/sax/SAXParseException;

    const-string v10, "target element appears without a name attribute"

    iget-object v11, p0, Lorg/apache/tools/ant/helper/ProjectHelperImpl$TargetHandler;->helperImpl:Lorg/apache/tools/ant/helper/ProjectHelperImpl;

    invoke-static {v11}, Lorg/apache/tools/ant/helper/ProjectHelperImpl;->access$100(Lorg/apache/tools/ant/helper/ProjectHelperImpl;)Lorg/xml/sax/Locator;

    move-result-object v11

    invoke-direct {v9, v10, v11}, Lorg/xml/sax/SAXParseException;-><init>(Ljava/lang/String;Lorg/xml/sax/Locator;)V

    throw v9

    :cond_8
    new-instance v9, Lorg/apache/tools/ant/Target;

    invoke-direct {v9}, Lorg/apache/tools/ant/Target;-><init>()V

    iput-object v9, p0, Lorg/apache/tools/ant/helper/ProjectHelperImpl$TargetHandler;->target:Lorg/apache/tools/ant/Target;

    iget-object v9, p0, Lorg/apache/tools/ant/helper/ProjectHelperImpl$TargetHandler;->target:Lorg/apache/tools/ant/Target;

    const-string v10, ""

    invoke-virtual {v9, v10}, Lorg/apache/tools/ant/Target;->addDependency(Ljava/lang/String;)V

    iget-object v9, p0, Lorg/apache/tools/ant/helper/ProjectHelperImpl$TargetHandler;->target:Lorg/apache/tools/ant/Target;

    invoke-virtual {v9, v6}, Lorg/apache/tools/ant/Target;->setName(Ljava/lang/String;)V

    iget-object v9, p0, Lorg/apache/tools/ant/helper/ProjectHelperImpl$TargetHandler;->target:Lorg/apache/tools/ant/Target;

    invoke-virtual {v9, v4}, Lorg/apache/tools/ant/Target;->setIf(Ljava/lang/String;)V

    iget-object v9, p0, Lorg/apache/tools/ant/helper/ProjectHelperImpl$TargetHandler;->target:Lorg/apache/tools/ant/Target;

    invoke-virtual {v9, v7}, Lorg/apache/tools/ant/Target;->setUnless(Ljava/lang/String;)V

    iget-object v9, p0, Lorg/apache/tools/ant/helper/ProjectHelperImpl$TargetHandler;->target:Lorg/apache/tools/ant/Target;

    invoke-virtual {v9, v1}, Lorg/apache/tools/ant/Target;->setDescription(Ljava/lang/String;)V

    iget-object v9, p0, Lorg/apache/tools/ant/helper/ProjectHelperImpl$TargetHandler;->helperImpl:Lorg/apache/tools/ant/helper/ProjectHelperImpl;

    invoke-static {v9}, Lorg/apache/tools/ant/helper/ProjectHelperImpl;->access$200(Lorg/apache/tools/ant/helper/ProjectHelperImpl;)Lorg/apache/tools/ant/Project;

    move-result-object v9

    iget-object v10, p0, Lorg/apache/tools/ant/helper/ProjectHelperImpl$TargetHandler;->target:Lorg/apache/tools/ant/Target;

    invoke-virtual {v9, v6, v10}, Lorg/apache/tools/ant/Project;->addTarget(Ljava/lang/String;Lorg/apache/tools/ant/Target;)V

    if-eqz v3, :cond_9

    const-string v9, ""

    invoke-virtual {v3, v9}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v9

    if-nez v9, :cond_9

    iget-object v9, p0, Lorg/apache/tools/ant/helper/ProjectHelperImpl$TargetHandler;->helperImpl:Lorg/apache/tools/ant/helper/ProjectHelperImpl;

    invoke-static {v9}, Lorg/apache/tools/ant/helper/ProjectHelperImpl;->access$200(Lorg/apache/tools/ant/helper/ProjectHelperImpl;)Lorg/apache/tools/ant/Project;

    move-result-object v9

    iget-object v10, p0, Lorg/apache/tools/ant/helper/ProjectHelperImpl$TargetHandler;->target:Lorg/apache/tools/ant/Target;

    invoke-virtual {v9, v3, v10}, Lorg/apache/tools/ant/Project;->addReference(Ljava/lang/String;Ljava/lang/Object;)V

    :cond_9
    invoke-virtual {v0}, Ljava/lang/String;->length()I

    move-result v9

    if-lez v9, :cond_a

    iget-object v9, p0, Lorg/apache/tools/ant/helper/ProjectHelperImpl$TargetHandler;->target:Lorg/apache/tools/ant/Target;

    invoke-virtual {v9, v0}, Lorg/apache/tools/ant/Target;->setDepends(Ljava/lang/String;)V

    :cond_a
    return-void
.end method

.method public startElement(Ljava/lang/String;Lorg/xml/sax/AttributeList;)V
    .locals 2
    .param p1    # Ljava/lang/String;
    .param p2    # Lorg/xml/sax/AttributeList;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lorg/xml/sax/SAXParseException;
        }
    .end annotation

    iget-object v0, p0, Lorg/apache/tools/ant/helper/ProjectHelperImpl$TargetHandler;->helperImpl:Lorg/apache/tools/ant/helper/ProjectHelperImpl;

    iget-object v1, p0, Lorg/apache/tools/ant/helper/ProjectHelperImpl$TargetHandler;->target:Lorg/apache/tools/ant/Target;

    invoke-static {v0, p0, v1, p1, p2}, Lorg/apache/tools/ant/helper/ProjectHelperImpl;->access$700(Lorg/apache/tools/ant/helper/ProjectHelperImpl;Lorg/xml/sax/DocumentHandler;Lorg/apache/tools/ant/Target;Ljava/lang/String;Lorg/xml/sax/AttributeList;)V

    return-void
.end method
