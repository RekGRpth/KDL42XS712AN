.class Lorg/apache/tools/ant/helper/ProjectHelperImpl$NestedElementHandler;
.super Lorg/apache/tools/ant/helper/ProjectHelperImpl$AbstractHandler;
.source "ProjectHelperImpl.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lorg/apache/tools/ant/helper/ProjectHelperImpl;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x8
    name = "NestedElementHandler"
.end annotation


# instance fields
.field private child:Ljava/lang/Object;

.field private childWrapper:Lorg/apache/tools/ant/RuntimeConfigurable;

.field private parent:Ljava/lang/Object;

.field private parentWrapper:Lorg/apache/tools/ant/RuntimeConfigurable;

.field private target:Lorg/apache/tools/ant/Target;


# direct methods
.method public constructor <init>(Lorg/apache/tools/ant/helper/ProjectHelperImpl;Lorg/xml/sax/DocumentHandler;Ljava/lang/Object;Lorg/apache/tools/ant/RuntimeConfigurable;Lorg/apache/tools/ant/Target;)V
    .locals 1
    .param p1    # Lorg/apache/tools/ant/helper/ProjectHelperImpl;
    .param p2    # Lorg/xml/sax/DocumentHandler;
    .param p3    # Ljava/lang/Object;
    .param p4    # Lorg/apache/tools/ant/RuntimeConfigurable;
    .param p5    # Lorg/apache/tools/ant/Target;

    invoke-direct {p0, p1, p2}, Lorg/apache/tools/ant/helper/ProjectHelperImpl$AbstractHandler;-><init>(Lorg/apache/tools/ant/helper/ProjectHelperImpl;Lorg/xml/sax/DocumentHandler;)V

    const/4 v0, 0x0

    iput-object v0, p0, Lorg/apache/tools/ant/helper/ProjectHelperImpl$NestedElementHandler;->childWrapper:Lorg/apache/tools/ant/RuntimeConfigurable;

    instance-of v0, p3, Lorg/apache/tools/ant/TypeAdapter;

    if-eqz v0, :cond_0

    check-cast p3, Lorg/apache/tools/ant/TypeAdapter;

    invoke-interface {p3}, Lorg/apache/tools/ant/TypeAdapter;->getProxy()Ljava/lang/Object;

    move-result-object v0

    iput-object v0, p0, Lorg/apache/tools/ant/helper/ProjectHelperImpl$NestedElementHandler;->parent:Ljava/lang/Object;

    :goto_0
    iput-object p4, p0, Lorg/apache/tools/ant/helper/ProjectHelperImpl$NestedElementHandler;->parentWrapper:Lorg/apache/tools/ant/RuntimeConfigurable;

    iput-object p5, p0, Lorg/apache/tools/ant/helper/ProjectHelperImpl$NestedElementHandler;->target:Lorg/apache/tools/ant/Target;

    return-void

    :cond_0
    iput-object p3, p0, Lorg/apache/tools/ant/helper/ProjectHelperImpl$NestedElementHandler;->parent:Ljava/lang/Object;

    goto :goto_0
.end method


# virtual methods
.method public characters([CII)V
    .locals 1
    .param p1    # [C
    .param p2    # I
    .param p3    # I

    iget-object v0, p0, Lorg/apache/tools/ant/helper/ProjectHelperImpl$NestedElementHandler;->childWrapper:Lorg/apache/tools/ant/RuntimeConfigurable;

    invoke-virtual {v0, p1, p2, p3}, Lorg/apache/tools/ant/RuntimeConfigurable;->addText([CII)V

    return-void
.end method

.method public init(Ljava/lang/String;Lorg/xml/sax/AttributeList;)V
    .locals 8
    .param p1    # Ljava/lang/String;
    .param p2    # Lorg/xml/sax/AttributeList;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lorg/xml/sax/SAXParseException;
        }
    .end annotation

    iget-object v5, p0, Lorg/apache/tools/ant/helper/ProjectHelperImpl$NestedElementHandler;->parent:Ljava/lang/Object;

    invoke-virtual {v5}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v3

    iget-object v5, p0, Lorg/apache/tools/ant/helper/ProjectHelperImpl$NestedElementHandler;->helperImpl:Lorg/apache/tools/ant/helper/ProjectHelperImpl;

    invoke-static {v5}, Lorg/apache/tools/ant/helper/ProjectHelperImpl;->access$200(Lorg/apache/tools/ant/helper/ProjectHelperImpl;)Lorg/apache/tools/ant/Project;

    move-result-object v5

    invoke-static {v5, v3}, Lorg/apache/tools/ant/IntrospectionHelper;->getHelper(Lorg/apache/tools/ant/Project;Ljava/lang/Class;)Lorg/apache/tools/ant/IntrospectionHelper;

    move-result-object v2

    :try_start_0
    sget-object v5, Ljava/util/Locale;->US:Ljava/util/Locale;

    invoke-virtual {p1, v5}, Ljava/lang/String;->toLowerCase(Ljava/util/Locale;)Ljava/lang/String;

    move-result-object v0

    iget-object v5, p0, Lorg/apache/tools/ant/helper/ProjectHelperImpl$NestedElementHandler;->parent:Ljava/lang/Object;

    instance-of v5, v5, Lorg/apache/tools/ant/UnknownElement;

    if-eqz v5, :cond_0

    new-instance v4, Lorg/apache/tools/ant/UnknownElement;

    invoke-direct {v4, v0}, Lorg/apache/tools/ant/UnknownElement;-><init>(Ljava/lang/String;)V

    iget-object v5, p0, Lorg/apache/tools/ant/helper/ProjectHelperImpl$NestedElementHandler;->helperImpl:Lorg/apache/tools/ant/helper/ProjectHelperImpl;

    invoke-static {v5}, Lorg/apache/tools/ant/helper/ProjectHelperImpl;->access$200(Lorg/apache/tools/ant/helper/ProjectHelperImpl;)Lorg/apache/tools/ant/Project;

    move-result-object v5

    invoke-virtual {v4, v5}, Lorg/apache/tools/ant/UnknownElement;->setProject(Lorg/apache/tools/ant/Project;)V

    iget-object v5, p0, Lorg/apache/tools/ant/helper/ProjectHelperImpl$NestedElementHandler;->parent:Ljava/lang/Object;

    check-cast v5, Lorg/apache/tools/ant/UnknownElement;

    invoke-virtual {v5, v4}, Lorg/apache/tools/ant/UnknownElement;->addChild(Lorg/apache/tools/ant/UnknownElement;)V

    iput-object v4, p0, Lorg/apache/tools/ant/helper/ProjectHelperImpl$NestedElementHandler;->child:Ljava/lang/Object;

    :goto_0
    iget-object v5, p0, Lorg/apache/tools/ant/helper/ProjectHelperImpl$NestedElementHandler;->helperImpl:Lorg/apache/tools/ant/helper/ProjectHelperImpl;

    iget-object v6, p0, Lorg/apache/tools/ant/helper/ProjectHelperImpl$NestedElementHandler;->child:Ljava/lang/Object;

    invoke-static {v5, v6, p2}, Lorg/apache/tools/ant/helper/ProjectHelperImpl;->access$800(Lorg/apache/tools/ant/helper/ProjectHelperImpl;Ljava/lang/Object;Lorg/xml/sax/AttributeList;)V

    new-instance v5, Lorg/apache/tools/ant/RuntimeConfigurable;

    iget-object v6, p0, Lorg/apache/tools/ant/helper/ProjectHelperImpl$NestedElementHandler;->child:Ljava/lang/Object;

    invoke-direct {v5, v6, p1}, Lorg/apache/tools/ant/RuntimeConfigurable;-><init>(Ljava/lang/Object;Ljava/lang/String;)V

    iput-object v5, p0, Lorg/apache/tools/ant/helper/ProjectHelperImpl$NestedElementHandler;->childWrapper:Lorg/apache/tools/ant/RuntimeConfigurable;

    iget-object v5, p0, Lorg/apache/tools/ant/helper/ProjectHelperImpl$NestedElementHandler;->childWrapper:Lorg/apache/tools/ant/RuntimeConfigurable;

    invoke-virtual {v5, p2}, Lorg/apache/tools/ant/RuntimeConfigurable;->setAttributes(Lorg/xml/sax/AttributeList;)V

    iget-object v5, p0, Lorg/apache/tools/ant/helper/ProjectHelperImpl$NestedElementHandler;->parentWrapper:Lorg/apache/tools/ant/RuntimeConfigurable;

    iget-object v6, p0, Lorg/apache/tools/ant/helper/ProjectHelperImpl$NestedElementHandler;->childWrapper:Lorg/apache/tools/ant/RuntimeConfigurable;

    invoke-virtual {v5, v6}, Lorg/apache/tools/ant/RuntimeConfigurable;->addChild(Lorg/apache/tools/ant/RuntimeConfigurable;)V

    return-void

    :cond_0
    iget-object v5, p0, Lorg/apache/tools/ant/helper/ProjectHelperImpl$NestedElementHandler;->helperImpl:Lorg/apache/tools/ant/helper/ProjectHelperImpl;

    invoke-static {v5}, Lorg/apache/tools/ant/helper/ProjectHelperImpl;->access$200(Lorg/apache/tools/ant/helper/ProjectHelperImpl;)Lorg/apache/tools/ant/Project;

    move-result-object v5

    iget-object v6, p0, Lorg/apache/tools/ant/helper/ProjectHelperImpl$NestedElementHandler;->parent:Ljava/lang/Object;

    invoke-virtual {v2, v5, v6, v0}, Lorg/apache/tools/ant/IntrospectionHelper;->createElement(Lorg/apache/tools/ant/Project;Ljava/lang/Object;Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v5

    iput-object v5, p0, Lorg/apache/tools/ant/helper/ProjectHelperImpl$NestedElementHandler;->child:Ljava/lang/Object;
    :try_end_0
    .catch Lorg/apache/tools/ant/BuildException; {:try_start_0 .. :try_end_0} :catch_0

    goto :goto_0

    :catch_0
    move-exception v1

    new-instance v5, Lorg/xml/sax/SAXParseException;

    invoke-virtual {v1}, Lorg/apache/tools/ant/BuildException;->getMessage()Ljava/lang/String;

    move-result-object v6

    iget-object v7, p0, Lorg/apache/tools/ant/helper/ProjectHelperImpl$NestedElementHandler;->helperImpl:Lorg/apache/tools/ant/helper/ProjectHelperImpl;

    invoke-static {v7}, Lorg/apache/tools/ant/helper/ProjectHelperImpl;->access$100(Lorg/apache/tools/ant/helper/ProjectHelperImpl;)Lorg/xml/sax/Locator;

    move-result-object v7

    invoke-direct {v5, v6, v7, v1}, Lorg/xml/sax/SAXParseException;-><init>(Ljava/lang/String;Lorg/xml/sax/Locator;Ljava/lang/Exception;)V

    throw v5
.end method

.method public startElement(Ljava/lang/String;Lorg/xml/sax/AttributeList;)V
    .locals 6
    .param p1    # Ljava/lang/String;
    .param p2    # Lorg/xml/sax/AttributeList;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lorg/xml/sax/SAXParseException;
        }
    .end annotation

    iget-object v0, p0, Lorg/apache/tools/ant/helper/ProjectHelperImpl$NestedElementHandler;->child:Ljava/lang/Object;

    instance-of v0, v0, Lorg/apache/tools/ant/TaskContainer;

    if-eqz v0, :cond_0

    new-instance v0, Lorg/apache/tools/ant/helper/ProjectHelperImpl$TaskHandler;

    iget-object v1, p0, Lorg/apache/tools/ant/helper/ProjectHelperImpl$NestedElementHandler;->helperImpl:Lorg/apache/tools/ant/helper/ProjectHelperImpl;

    iget-object v3, p0, Lorg/apache/tools/ant/helper/ProjectHelperImpl$NestedElementHandler;->child:Ljava/lang/Object;

    check-cast v3, Lorg/apache/tools/ant/TaskContainer;

    iget-object v4, p0, Lorg/apache/tools/ant/helper/ProjectHelperImpl$NestedElementHandler;->childWrapper:Lorg/apache/tools/ant/RuntimeConfigurable;

    iget-object v5, p0, Lorg/apache/tools/ant/helper/ProjectHelperImpl$NestedElementHandler;->target:Lorg/apache/tools/ant/Target;

    move-object v2, p0

    invoke-direct/range {v0 .. v5}, Lorg/apache/tools/ant/helper/ProjectHelperImpl$TaskHandler;-><init>(Lorg/apache/tools/ant/helper/ProjectHelperImpl;Lorg/xml/sax/DocumentHandler;Lorg/apache/tools/ant/TaskContainer;Lorg/apache/tools/ant/RuntimeConfigurable;Lorg/apache/tools/ant/Target;)V

    invoke-virtual {v0, p1, p2}, Lorg/apache/tools/ant/helper/ProjectHelperImpl$TaskHandler;->init(Ljava/lang/String;Lorg/xml/sax/AttributeList;)V

    :goto_0
    return-void

    :cond_0
    new-instance v0, Lorg/apache/tools/ant/helper/ProjectHelperImpl$NestedElementHandler;

    iget-object v1, p0, Lorg/apache/tools/ant/helper/ProjectHelperImpl$NestedElementHandler;->helperImpl:Lorg/apache/tools/ant/helper/ProjectHelperImpl;

    iget-object v3, p0, Lorg/apache/tools/ant/helper/ProjectHelperImpl$NestedElementHandler;->child:Ljava/lang/Object;

    iget-object v4, p0, Lorg/apache/tools/ant/helper/ProjectHelperImpl$NestedElementHandler;->childWrapper:Lorg/apache/tools/ant/RuntimeConfigurable;

    iget-object v5, p0, Lorg/apache/tools/ant/helper/ProjectHelperImpl$NestedElementHandler;->target:Lorg/apache/tools/ant/Target;

    move-object v2, p0

    invoke-direct/range {v0 .. v5}, Lorg/apache/tools/ant/helper/ProjectHelperImpl$NestedElementHandler;-><init>(Lorg/apache/tools/ant/helper/ProjectHelperImpl;Lorg/xml/sax/DocumentHandler;Ljava/lang/Object;Lorg/apache/tools/ant/RuntimeConfigurable;Lorg/apache/tools/ant/Target;)V

    invoke-virtual {v0, p1, p2}, Lorg/apache/tools/ant/helper/ProjectHelperImpl$NestedElementHandler;->init(Ljava/lang/String;Lorg/xml/sax/AttributeList;)V

    goto :goto_0
.end method
