.class abstract Lorg/apache/tools/ant/IntrospectionHelper$AttributeSetter;
.super Ljava/lang/Object;
.source "IntrospectionHelper.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lorg/apache/tools/ant/IntrospectionHelper;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x40a
    name = "AttributeSetter"
.end annotation


# instance fields
.field private method:Ljava/lang/reflect/Method;


# direct methods
.method constructor <init>(Ljava/lang/reflect/Method;)V
    .locals 0
    .param p1    # Ljava/lang/reflect/Method;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    iput-object p1, p0, Lorg/apache/tools/ant/IntrospectionHelper$AttributeSetter;->method:Ljava/lang/reflect/Method;

    return-void
.end method

.method static access$200(Lorg/apache/tools/ant/IntrospectionHelper$AttributeSetter;)Ljava/lang/reflect/Method;
    .locals 1
    .param p0    # Lorg/apache/tools/ant/IntrospectionHelper$AttributeSetter;

    iget-object v0, p0, Lorg/apache/tools/ant/IntrospectionHelper$AttributeSetter;->method:Ljava/lang/reflect/Method;

    return-object v0
.end method


# virtual methods
.method abstract set(Lorg/apache/tools/ant/Project;Ljava/lang/Object;Ljava/lang/String;)V
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/lang/reflect/InvocationTargetException;,
            Ljava/lang/IllegalAccessException;,
            Lorg/apache/tools/ant/BuildException;
        }
    .end annotation
.end method
