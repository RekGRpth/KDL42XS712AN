.class public final Lorg/apache/tools/ant/MagicNames;
.super Ljava/lang/Object;
.source "MagicNames.java"


# static fields
.field public static final ANTLIB_PREFIX:Ljava/lang/String; = "antlib:"

.field public static final ANT_EXECUTOR_CLASSNAME:Ljava/lang/String; = "ant.executor.class"

.field public static final ANT_EXECUTOR_REFERENCE:Ljava/lang/String; = "ant.executor"

.field public static final ANT_FILE:Ljava/lang/String; = "ant.file"

.field public static final ANT_HOME:Ljava/lang/String; = "ant.home"

.field public static final ANT_JAVA_VERSION:Ljava/lang/String; = "ant.java.version"

.field public static final ANT_LIB:Ljava/lang/String; = "ant.core.lib"

.field public static final ANT_VERSION:Ljava/lang/String; = "ant.version"

.field public static final BUILD_JAVAC_SOURCE:Ljava/lang/String; = "ant.build.javac.source"

.field public static final BUILD_JAVAC_TARGET:Ljava/lang/String; = "ant.build.javac.target"

.field public static final BUILD_SYSCLASSPATH:Ljava/lang/String; = "build.sysclasspath"

.field public static final PROJECT_BASEDIR:Ljava/lang/String; = "basedir"

.field public static final REFID_CLASSPATH_LOADER_PREFIX:Ljava/lang/String; = "ant.loader."

.field public static final REFID_CLASSPATH_REUSE_LOADER:Ljava/lang/String; = "ant.reuse.loader"

.field public static final REFID_PROPERTY_HELPER:Ljava/lang/String; = "ant.PropertyHelper"

.field public static final REGEXP_IMPL:Ljava/lang/String; = "ant.regexp.regexpimpl"

.field public static final REPOSITORY_DIR_PROPERTY:Ljava/lang/String; = "ant.maven.repository.dir"

.field public static final REPOSITORY_URL_PROPERTY:Ljava/lang/String; = "ant.maven.repository.url"

.field public static final SCRIPT_REPOSITORY:Ljava/lang/String; = "org.apache.ant.scriptrepo"

.field public static final SYSTEM_LOADER_REF:Ljava/lang/String; = "ant.coreLoader"

.field public static final TASKDEF_PROPERTIES_RESOURCE:Ljava/lang/String; = "/org/apache/tools/ant/taskdefs/defaults.properties"

.field public static final TYPEDEFS_PROPERTIES_RESOURCE:Ljava/lang/String; = "/org/apache/tools/ant/types/defaults.properties"


# direct methods
.method private constructor <init>()V
    .locals 0

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method
