.class public final Lorg/apache/tools/ant/listener/AnsiColorLogger;
.super Lorg/apache/tools/ant/DefaultLogger;
.source "AnsiColorLogger.java"


# static fields
.field private static final ATTR_DIM:I = 0x2

.field private static final END_COLOR:Ljava/lang/String; = "\u001b[m"

.field private static final FG_BLUE:I = 0x22

.field private static final FG_CYAN:I = 0x24

.field private static final FG_GREEN:I = 0x20

.field private static final FG_MAGENTA:I = 0x23

.field private static final FG_RED:I = 0x1f

.field private static final PREFIX:Ljava/lang/String; = "\u001b["

.field private static final SEPARATOR:C = ';'

.field private static final SUFFIX:Ljava/lang/String; = "m"


# instance fields
.field private colorsSet:Z

.field private debugColor:Ljava/lang/String;

.field private errColor:Ljava/lang/String;

.field private infoColor:Ljava/lang/String;

.field private verboseColor:Ljava/lang/String;

.field private warnColor:Ljava/lang/String;


# direct methods
.method public constructor <init>()V
    .locals 1

    invoke-direct {p0}, Lorg/apache/tools/ant/DefaultLogger;-><init>()V

    const-string v0, "\u001b[2;31m"

    iput-object v0, p0, Lorg/apache/tools/ant/listener/AnsiColorLogger;->errColor:Ljava/lang/String;

    const-string v0, "\u001b[2;35m"

    iput-object v0, p0, Lorg/apache/tools/ant/listener/AnsiColorLogger;->warnColor:Ljava/lang/String;

    const-string v0, "\u001b[2;36m"

    iput-object v0, p0, Lorg/apache/tools/ant/listener/AnsiColorLogger;->infoColor:Ljava/lang/String;

    const-string v0, "\u001b[2;32m"

    iput-object v0, p0, Lorg/apache/tools/ant/listener/AnsiColorLogger;->verboseColor:Ljava/lang/String;

    const-string v0, "\u001b[2;34m"

    iput-object v0, p0, Lorg/apache/tools/ant/listener/AnsiColorLogger;->debugColor:Ljava/lang/String;

    const/4 v0, 0x0

    iput-boolean v0, p0, Lorg/apache/tools/ant/listener/AnsiColorLogger;->colorsSet:Z

    return-void
.end method

.method private setColors()V
    .locals 12

    const-string v10, "ant.logger.defaults"

    invoke-static {v10}, Ljava/lang/System;->getProperty(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v7

    const-string v6, "/org/apache/tools/ant/listener/defaults.properties"

    const/4 v2, 0x0

    :try_start_0
    new-instance v5, Ljava/util/Properties;

    invoke-direct {v5}, Ljava/util/Properties;-><init>()V

    if-eqz v7, :cond_7

    new-instance v3, Ljava/io/FileInputStream;

    invoke-direct {v3, v7}, Ljava/io/FileInputStream;-><init>(Ljava/lang/String;)V

    move-object v2, v3

    :goto_0
    if-eqz v2, :cond_0

    invoke-virtual {v5, v2}, Ljava/util/Properties;->load(Ljava/io/InputStream;)V

    :cond_0
    const-string v10, "AnsiColorLogger.ERROR_COLOR"

    invoke-virtual {v5, v10}, Ljava/util/Properties;->getProperty(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    const-string v10, "AnsiColorLogger.WARNING_COLOR"

    invoke-virtual {v5, v10}, Ljava/util/Properties;->getProperty(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v9

    const-string v10, "AnsiColorLogger.INFO_COLOR"

    invoke-virtual {v5, v10}, Ljava/util/Properties;->getProperty(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v4

    const-string v10, "AnsiColorLogger.VERBOSE_COLOR"

    invoke-virtual {v5, v10}, Ljava/util/Properties;->getProperty(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v8

    const-string v10, "AnsiColorLogger.DEBUG_COLOR"

    invoke-virtual {v5, v10}, Ljava/util/Properties;->getProperty(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    if-eqz v1, :cond_1

    new-instance v10, Ljava/lang/StringBuffer;

    invoke-direct {v10}, Ljava/lang/StringBuffer;-><init>()V

    const-string v11, "\u001b["

    invoke-virtual {v10, v11}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    move-result-object v10

    invoke-virtual {v10, v1}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    move-result-object v10

    const-string v11, "m"

    invoke-virtual {v10, v11}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    move-result-object v10

    invoke-virtual {v10}, Ljava/lang/StringBuffer;->toString()Ljava/lang/String;

    move-result-object v10

    iput-object v10, p0, Lorg/apache/tools/ant/listener/AnsiColorLogger;->errColor:Ljava/lang/String;

    :cond_1
    if-eqz v9, :cond_2

    new-instance v10, Ljava/lang/StringBuffer;

    invoke-direct {v10}, Ljava/lang/StringBuffer;-><init>()V

    const-string v11, "\u001b["

    invoke-virtual {v10, v11}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    move-result-object v10

    invoke-virtual {v10, v9}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    move-result-object v10

    const-string v11, "m"

    invoke-virtual {v10, v11}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    move-result-object v10

    invoke-virtual {v10}, Ljava/lang/StringBuffer;->toString()Ljava/lang/String;

    move-result-object v10

    iput-object v10, p0, Lorg/apache/tools/ant/listener/AnsiColorLogger;->warnColor:Ljava/lang/String;

    :cond_2
    if-eqz v4, :cond_3

    new-instance v10, Ljava/lang/StringBuffer;

    invoke-direct {v10}, Ljava/lang/StringBuffer;-><init>()V

    const-string v11, "\u001b["

    invoke-virtual {v10, v11}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    move-result-object v10

    invoke-virtual {v10, v4}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    move-result-object v10

    const-string v11, "m"

    invoke-virtual {v10, v11}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    move-result-object v10

    invoke-virtual {v10}, Ljava/lang/StringBuffer;->toString()Ljava/lang/String;

    move-result-object v10

    iput-object v10, p0, Lorg/apache/tools/ant/listener/AnsiColorLogger;->infoColor:Ljava/lang/String;

    :cond_3
    if-eqz v8, :cond_4

    new-instance v10, Ljava/lang/StringBuffer;

    invoke-direct {v10}, Ljava/lang/StringBuffer;-><init>()V

    const-string v11, "\u001b["

    invoke-virtual {v10, v11}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    move-result-object v10

    invoke-virtual {v10, v8}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    move-result-object v10

    const-string v11, "m"

    invoke-virtual {v10, v11}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    move-result-object v10

    invoke-virtual {v10}, Ljava/lang/StringBuffer;->toString()Ljava/lang/String;

    move-result-object v10

    iput-object v10, p0, Lorg/apache/tools/ant/listener/AnsiColorLogger;->verboseColor:Ljava/lang/String;

    :cond_4
    if-eqz v0, :cond_5

    new-instance v10, Ljava/lang/StringBuffer;

    invoke-direct {v10}, Ljava/lang/StringBuffer;-><init>()V

    const-string v11, "\u001b["

    invoke-virtual {v10, v11}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    move-result-object v10

    invoke-virtual {v10, v0}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    move-result-object v10

    const-string v11, "m"

    invoke-virtual {v10, v11}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    move-result-object v10

    invoke-virtual {v10}, Ljava/lang/StringBuffer;->toString()Ljava/lang/String;

    move-result-object v10

    iput-object v10, p0, Lorg/apache/tools/ant/listener/AnsiColorLogger;->debugColor:Ljava/lang/String;
    :try_end_0
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    :cond_5
    if-eqz v2, :cond_6

    :try_start_1
    invoke-virtual {v2}, Ljava/io/InputStream;->close()V
    :try_end_1
    .catch Ljava/io/IOException; {:try_start_1 .. :try_end_1} :catch_2

    :cond_6
    :goto_1
    return-void

    :cond_7
    :try_start_2
    invoke-virtual {p0}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v10

    invoke-virtual {v10, v6}, Ljava/lang/Class;->getResourceAsStream(Ljava/lang/String;)Ljava/io/InputStream;
    :try_end_2
    .catch Ljava/io/IOException; {:try_start_2 .. :try_end_2} :catch_0
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    move-result-object v2

    goto/16 :goto_0

    :catch_0
    move-exception v10

    if-eqz v2, :cond_6

    :try_start_3
    invoke-virtual {v2}, Ljava/io/InputStream;->close()V
    :try_end_3
    .catch Ljava/io/IOException; {:try_start_3 .. :try_end_3} :catch_1

    goto :goto_1

    :catch_1
    move-exception v10

    goto :goto_1

    :catchall_0
    move-exception v10

    if-eqz v2, :cond_8

    :try_start_4
    invoke-virtual {v2}, Ljava/io/InputStream;->close()V
    :try_end_4
    .catch Ljava/io/IOException; {:try_start_4 .. :try_end_4} :catch_3

    :cond_8
    :goto_2
    throw v10

    :catch_2
    move-exception v10

    goto :goto_1

    :catch_3
    move-exception v11

    goto :goto_2
.end method


# virtual methods
.method protected printMessage(Ljava/lang/String;Ljava/io/PrintStream;I)V
    .locals 4
    .param p1    # Ljava/lang/String;
    .param p2    # Ljava/io/PrintStream;
    .param p3    # I

    const/4 v3, 0x0

    if-eqz p1, :cond_1

    if-eqz p2, :cond_1

    iget-boolean v2, p0, Lorg/apache/tools/ant/listener/AnsiColorLogger;->colorsSet:Z

    if-nez v2, :cond_0

    invoke-direct {p0}, Lorg/apache/tools/ant/listener/AnsiColorLogger;->setColors()V

    const/4 v2, 0x1

    iput-boolean v2, p0, Lorg/apache/tools/ant/listener/AnsiColorLogger;->colorsSet:Z

    :cond_0
    new-instance v0, Ljava/lang/StringBuffer;

    invoke-direct {v0, p1}, Ljava/lang/StringBuffer;-><init>(Ljava/lang/String;)V

    packed-switch p3, :pswitch_data_0

    iget-object v2, p0, Lorg/apache/tools/ant/listener/AnsiColorLogger;->debugColor:Ljava/lang/String;

    invoke-virtual {v0, v3, v2}, Ljava/lang/StringBuffer;->insert(ILjava/lang/String;)Ljava/lang/StringBuffer;

    const-string v2, "\u001b[m"

    invoke-virtual {v0, v2}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    :goto_0
    invoke-virtual {v0}, Ljava/lang/StringBuffer;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {p2, v1}, Ljava/io/PrintStream;->println(Ljava/lang/String;)V

    :cond_1
    return-void

    :pswitch_0
    iget-object v2, p0, Lorg/apache/tools/ant/listener/AnsiColorLogger;->errColor:Ljava/lang/String;

    invoke-virtual {v0, v3, v2}, Ljava/lang/StringBuffer;->insert(ILjava/lang/String;)Ljava/lang/StringBuffer;

    const-string v2, "\u001b[m"

    invoke-virtual {v0, v2}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    goto :goto_0

    :pswitch_1
    iget-object v2, p0, Lorg/apache/tools/ant/listener/AnsiColorLogger;->warnColor:Ljava/lang/String;

    invoke-virtual {v0, v3, v2}, Ljava/lang/StringBuffer;->insert(ILjava/lang/String;)Ljava/lang/StringBuffer;

    const-string v2, "\u001b[m"

    invoke-virtual {v0, v2}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    goto :goto_0

    :pswitch_2
    iget-object v2, p0, Lorg/apache/tools/ant/listener/AnsiColorLogger;->infoColor:Ljava/lang/String;

    invoke-virtual {v0, v3, v2}, Ljava/lang/StringBuffer;->insert(ILjava/lang/String;)Ljava/lang/StringBuffer;

    const-string v2, "\u001b[m"

    invoke-virtual {v0, v2}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    goto :goto_0

    :pswitch_3
    iget-object v2, p0, Lorg/apache/tools/ant/listener/AnsiColorLogger;->verboseColor:Ljava/lang/String;

    invoke-virtual {v0, v3, v2}, Ljava/lang/StringBuffer;->insert(ILjava/lang/String;)Ljava/lang/StringBuffer;

    const-string v2, "\u001b[m"

    invoke-virtual {v0, v2}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    goto :goto_0

    nop

    :pswitch_data_0
    .packed-switch 0x0
        :pswitch_0
        :pswitch_1
        :pswitch_2
        :pswitch_3
    .end packed-switch
.end method
