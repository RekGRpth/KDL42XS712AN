.class public Lorg/apache/tools/ant/Target;
.super Ljava/lang/Object;
.source "Target.java"

# interfaces
.implements Lorg/apache/tools/ant/TaskContainer;


# instance fields
.field private children:Ljava/util/List;

.field private dependencies:Ljava/util/List;

.field private description:Ljava/lang/String;

.field private ifCondition:Ljava/lang/String;

.field private location:Lorg/apache/tools/ant/Location;

.field private name:Ljava/lang/String;

.field private project:Lorg/apache/tools/ant/Project;

.field private unlessCondition:Ljava/lang/String;


# direct methods
.method public constructor <init>()V
    .locals 2

    const/4 v1, 0x0

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    const-string v0, ""

    iput-object v0, p0, Lorg/apache/tools/ant/Target;->ifCondition:Ljava/lang/String;

    const-string v0, ""

    iput-object v0, p0, Lorg/apache/tools/ant/Target;->unlessCondition:Ljava/lang/String;

    iput-object v1, p0, Lorg/apache/tools/ant/Target;->dependencies:Ljava/util/List;

    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, Lorg/apache/tools/ant/Target;->children:Ljava/util/List;

    sget-object v0, Lorg/apache/tools/ant/Location;->UNKNOWN_LOCATION:Lorg/apache/tools/ant/Location;

    iput-object v0, p0, Lorg/apache/tools/ant/Target;->location:Lorg/apache/tools/ant/Location;

    iput-object v1, p0, Lorg/apache/tools/ant/Target;->description:Ljava/lang/String;

    return-void
.end method

.method public constructor <init>(Lorg/apache/tools/ant/Target;)V
    .locals 2
    .param p1    # Lorg/apache/tools/ant/Target;

    const/4 v1, 0x0

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    const-string v0, ""

    iput-object v0, p0, Lorg/apache/tools/ant/Target;->ifCondition:Ljava/lang/String;

    const-string v0, ""

    iput-object v0, p0, Lorg/apache/tools/ant/Target;->unlessCondition:Ljava/lang/String;

    iput-object v1, p0, Lorg/apache/tools/ant/Target;->dependencies:Ljava/util/List;

    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, Lorg/apache/tools/ant/Target;->children:Ljava/util/List;

    sget-object v0, Lorg/apache/tools/ant/Location;->UNKNOWN_LOCATION:Lorg/apache/tools/ant/Location;

    iput-object v0, p0, Lorg/apache/tools/ant/Target;->location:Lorg/apache/tools/ant/Location;

    iput-object v1, p0, Lorg/apache/tools/ant/Target;->description:Ljava/lang/String;

    iget-object v0, p1, Lorg/apache/tools/ant/Target;->name:Ljava/lang/String;

    iput-object v0, p0, Lorg/apache/tools/ant/Target;->name:Ljava/lang/String;

    iget-object v0, p1, Lorg/apache/tools/ant/Target;->ifCondition:Ljava/lang/String;

    iput-object v0, p0, Lorg/apache/tools/ant/Target;->ifCondition:Ljava/lang/String;

    iget-object v0, p1, Lorg/apache/tools/ant/Target;->unlessCondition:Ljava/lang/String;

    iput-object v0, p0, Lorg/apache/tools/ant/Target;->unlessCondition:Ljava/lang/String;

    iget-object v0, p1, Lorg/apache/tools/ant/Target;->dependencies:Ljava/util/List;

    iput-object v0, p0, Lorg/apache/tools/ant/Target;->dependencies:Ljava/util/List;

    iget-object v0, p1, Lorg/apache/tools/ant/Target;->location:Lorg/apache/tools/ant/Location;

    iput-object v0, p0, Lorg/apache/tools/ant/Target;->location:Lorg/apache/tools/ant/Location;

    iget-object v0, p1, Lorg/apache/tools/ant/Target;->project:Lorg/apache/tools/ant/Project;

    iput-object v0, p0, Lorg/apache/tools/ant/Target;->project:Lorg/apache/tools/ant/Project;

    iget-object v0, p1, Lorg/apache/tools/ant/Target;->description:Ljava/lang/String;

    iput-object v0, p0, Lorg/apache/tools/ant/Target;->description:Ljava/lang/String;

    iget-object v0, p1, Lorg/apache/tools/ant/Target;->children:Ljava/util/List;

    iput-object v0, p0, Lorg/apache/tools/ant/Target;->children:Ljava/util/List;

    return-void
.end method

.method private testIfCondition()Z
    .locals 4

    const/4 v1, 0x1

    const-string v2, ""

    iget-object v3, p0, Lorg/apache/tools/ant/Target;->ifCondition:Ljava/lang/String;

    invoke-virtual {v2, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_1

    :cond_0
    :goto_0
    return v1

    :cond_1
    iget-object v2, p0, Lorg/apache/tools/ant/Target;->project:Lorg/apache/tools/ant/Project;

    iget-object v3, p0, Lorg/apache/tools/ant/Target;->ifCondition:Ljava/lang/String;

    invoke-virtual {v2, v3}, Lorg/apache/tools/ant/Project;->replaceProperties(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    iget-object v2, p0, Lorg/apache/tools/ant/Target;->project:Lorg/apache/tools/ant/Project;

    invoke-virtual {v2, v0}, Lorg/apache/tools/ant/Project;->getProperty(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v2

    if-nez v2, :cond_0

    const/4 v1, 0x0

    goto :goto_0
.end method

.method private testUnlessCondition()Z
    .locals 4

    const/4 v1, 0x1

    const-string v2, ""

    iget-object v3, p0, Lorg/apache/tools/ant/Target;->unlessCondition:Ljava/lang/String;

    invoke-virtual {v2, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_1

    :cond_0
    :goto_0
    return v1

    :cond_1
    iget-object v2, p0, Lorg/apache/tools/ant/Target;->project:Lorg/apache/tools/ant/Project;

    iget-object v3, p0, Lorg/apache/tools/ant/Target;->unlessCondition:Ljava/lang/String;

    invoke-virtual {v2, v3}, Lorg/apache/tools/ant/Project;->replaceProperties(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    iget-object v2, p0, Lorg/apache/tools/ant/Target;->project:Lorg/apache/tools/ant/Project;

    invoke-virtual {v2, v0}, Lorg/apache/tools/ant/Project;->getProperty(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v2

    if-eqz v2, :cond_0

    const/4 v1, 0x0

    goto :goto_0
.end method


# virtual methods
.method public addDataType(Lorg/apache/tools/ant/RuntimeConfigurable;)V
    .locals 1
    .param p1    # Lorg/apache/tools/ant/RuntimeConfigurable;

    iget-object v0, p0, Lorg/apache/tools/ant/Target;->children:Ljava/util/List;

    invoke-interface {v0, p1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    return-void
.end method

.method public addDependency(Ljava/lang/String;)V
    .locals 2
    .param p1    # Ljava/lang/String;

    iget-object v0, p0, Lorg/apache/tools/ant/Target;->dependencies:Ljava/util/List;

    if-nez v0, :cond_0

    new-instance v0, Ljava/util/ArrayList;

    const/4 v1, 0x2

    invoke-direct {v0, v1}, Ljava/util/ArrayList;-><init>(I)V

    iput-object v0, p0, Lorg/apache/tools/ant/Target;->dependencies:Ljava/util/List;

    :cond_0
    iget-object v0, p0, Lorg/apache/tools/ant/Target;->dependencies:Ljava/util/List;

    invoke-interface {v0, p1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    return-void
.end method

.method public addTask(Lorg/apache/tools/ant/Task;)V
    .locals 1
    .param p1    # Lorg/apache/tools/ant/Task;

    iget-object v0, p0, Lorg/apache/tools/ant/Target;->children:Ljava/util/List;

    invoke-interface {v0, p1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    return-void
.end method

.method public dependsOn(Ljava/lang/String;)Z
    .locals 5
    .param p1    # Ljava/lang/String;

    const/4 v2, 0x0

    invoke-virtual {p0}, Lorg/apache/tools/ant/Target;->getProject()Lorg/apache/tools/ant/Project;

    move-result-object v0

    if-nez v0, :cond_1

    const/4 v1, 0x0

    :goto_0
    if-eqz v0, :cond_0

    invoke-virtual {p0}, Lorg/apache/tools/ant/Target;->getName()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v0, v3, v1, v2}, Lorg/apache/tools/ant/Project;->topoSort(Ljava/lang/String;Ljava/util/Hashtable;Z)Ljava/util/Vector;

    move-result-object v3

    invoke-virtual {v1, p1}, Ljava/util/Hashtable;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v4

    invoke-virtual {v3, v4}, Ljava/util/Vector;->contains(Ljava/lang/Object;)Z

    move-result v3

    if-eqz v3, :cond_0

    const/4 v2, 0x1

    :cond_0
    return v2

    :cond_1
    invoke-virtual {v0}, Lorg/apache/tools/ant/Project;->getTargets()Ljava/util/Hashtable;

    move-result-object v1

    goto :goto_0
.end method

.method public execute()V
    .locals 9
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lorg/apache/tools/ant/BuildException;
        }
    .end annotation

    const/4 v8, 0x3

    invoke-direct {p0}, Lorg/apache/tools/ant/Target;->testIfCondition()Z

    move-result v4

    if-eqz v4, :cond_1

    invoke-direct {p0}, Lorg/apache/tools/ant/Target;->testUnlessCondition()Z

    move-result v4

    if-eqz v4, :cond_1

    const/4 v3, 0x0

    :goto_0
    iget-object v4, p0, Lorg/apache/tools/ant/Target;->children:Ljava/util/List;

    invoke-interface {v4}, Ljava/util/List;->size()I

    move-result v4

    if-ge v3, v4, :cond_2

    iget-object v4, p0, Lorg/apache/tools/ant/Target;->children:Ljava/util/List;

    invoke-interface {v4, v3}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    instance-of v4, v0, Lorg/apache/tools/ant/Task;

    if-eqz v4, :cond_0

    move-object v2, v0

    check-cast v2, Lorg/apache/tools/ant/Task;

    invoke-virtual {v2}, Lorg/apache/tools/ant/Task;->perform()V

    :goto_1
    add-int/lit8 v3, v3, 0x1

    goto :goto_0

    :cond_0
    move-object v1, v0

    check-cast v1, Lorg/apache/tools/ant/RuntimeConfigurable;

    iget-object v4, p0, Lorg/apache/tools/ant/Target;->project:Lorg/apache/tools/ant/Project;

    invoke-virtual {v1, v4}, Lorg/apache/tools/ant/RuntimeConfigurable;->maybeConfigure(Lorg/apache/tools/ant/Project;)V

    goto :goto_1

    :cond_1
    invoke-direct {p0}, Lorg/apache/tools/ant/Target;->testIfCondition()Z

    move-result v4

    if-nez v4, :cond_3

    iget-object v4, p0, Lorg/apache/tools/ant/Target;->project:Lorg/apache/tools/ant/Project;

    new-instance v5, Ljava/lang/StringBuffer;

    invoke-direct {v5}, Ljava/lang/StringBuffer;-><init>()V

    const-string v6, "Skipped because property \'"

    invoke-virtual {v5, v6}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    move-result-object v5

    iget-object v6, p0, Lorg/apache/tools/ant/Target;->project:Lorg/apache/tools/ant/Project;

    iget-object v7, p0, Lorg/apache/tools/ant/Target;->ifCondition:Ljava/lang/String;

    invoke-virtual {v6, v7}, Lorg/apache/tools/ant/Project;->replaceProperties(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v6

    invoke-virtual {v5, v6}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    move-result-object v5

    const-string v6, "\' not set."

    invoke-virtual {v5, v6}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/StringBuffer;->toString()Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v4, p0, v5, v8}, Lorg/apache/tools/ant/Project;->log(Lorg/apache/tools/ant/Target;Ljava/lang/String;I)V

    :cond_2
    :goto_2
    return-void

    :cond_3
    iget-object v4, p0, Lorg/apache/tools/ant/Target;->project:Lorg/apache/tools/ant/Project;

    new-instance v5, Ljava/lang/StringBuffer;

    invoke-direct {v5}, Ljava/lang/StringBuffer;-><init>()V

    const-string v6, "Skipped because property \'"

    invoke-virtual {v5, v6}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    move-result-object v5

    iget-object v6, p0, Lorg/apache/tools/ant/Target;->project:Lorg/apache/tools/ant/Project;

    iget-object v7, p0, Lorg/apache/tools/ant/Target;->unlessCondition:Ljava/lang/String;

    invoke-virtual {v6, v7}, Lorg/apache/tools/ant/Project;->replaceProperties(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v6

    invoke-virtual {v5, v6}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    move-result-object v5

    const-string v6, "\' set."

    invoke-virtual {v5, v6}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/StringBuffer;->toString()Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v4, p0, v5, v8}, Lorg/apache/tools/ant/Project;->log(Lorg/apache/tools/ant/Target;Ljava/lang/String;I)V

    goto :goto_2
.end method

.method public getDependencies()Ljava/util/Enumeration;
    .locals 1

    iget-object v0, p0, Lorg/apache/tools/ant/Target;->dependencies:Ljava/util/List;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lorg/apache/tools/ant/Target;->dependencies:Ljava/util/List;

    invoke-static {v0}, Ljava/util/Collections;->enumeration(Ljava/util/Collection;)Ljava/util/Enumeration;

    move-result-object v0

    :goto_0
    return-object v0

    :cond_0
    new-instance v0, Lorg/apache/tools/ant/util/CollectionUtils$EmptyEnumeration;

    invoke-direct {v0}, Lorg/apache/tools/ant/util/CollectionUtils$EmptyEnumeration;-><init>()V

    goto :goto_0
.end method

.method public getDescription()Ljava/lang/String;
    .locals 1

    iget-object v0, p0, Lorg/apache/tools/ant/Target;->description:Ljava/lang/String;

    return-object v0
.end method

.method public getIf()Ljava/lang/String;
    .locals 2

    const-string v0, ""

    iget-object v1, p0, Lorg/apache/tools/ant/Target;->ifCondition:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    const/4 v0, 0x0

    :goto_0
    return-object v0

    :cond_0
    iget-object v0, p0, Lorg/apache/tools/ant/Target;->ifCondition:Ljava/lang/String;

    goto :goto_0
.end method

.method public getLocation()Lorg/apache/tools/ant/Location;
    .locals 1

    iget-object v0, p0, Lorg/apache/tools/ant/Target;->location:Lorg/apache/tools/ant/Location;

    return-object v0
.end method

.method public getName()Ljava/lang/String;
    .locals 1

    iget-object v0, p0, Lorg/apache/tools/ant/Target;->name:Ljava/lang/String;

    return-object v0
.end method

.method public getProject()Lorg/apache/tools/ant/Project;
    .locals 1

    iget-object v0, p0, Lorg/apache/tools/ant/Target;->project:Lorg/apache/tools/ant/Project;

    return-object v0
.end method

.method public getTasks()[Lorg/apache/tools/ant/Task;
    .locals 4

    new-instance v2, Ljava/util/ArrayList;

    iget-object v3, p0, Lorg/apache/tools/ant/Target;->children:Ljava/util/List;

    invoke-interface {v3}, Ljava/util/List;->size()I

    move-result v3

    invoke-direct {v2, v3}, Ljava/util/ArrayList;-><init>(I)V

    iget-object v3, p0, Lorg/apache/tools/ant/Target;->children:Ljava/util/List;

    invoke-interface {v3}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v0

    :cond_0
    :goto_0
    invoke-interface {v0}, Ljava/util/Iterator;->hasNext()Z

    move-result v3

    if-eqz v3, :cond_1

    invoke-interface {v0}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v1

    instance-of v3, v1, Lorg/apache/tools/ant/Task;

    if-eqz v3, :cond_0

    invoke-interface {v2, v1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    goto :goto_0

    :cond_1
    invoke-interface {v2}, Ljava/util/List;->size()I

    move-result v3

    new-array v3, v3, [Lorg/apache/tools/ant/Task;

    invoke-interface {v2, v3}, Ljava/util/List;->toArray([Ljava/lang/Object;)[Ljava/lang/Object;

    move-result-object v3

    check-cast v3, [Lorg/apache/tools/ant/Task;

    check-cast v3, [Lorg/apache/tools/ant/Task;

    return-object v3
.end method

.method public getUnless()Ljava/lang/String;
    .locals 2

    const-string v0, ""

    iget-object v1, p0, Lorg/apache/tools/ant/Target;->unlessCondition:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    const/4 v0, 0x0

    :goto_0
    return-object v0

    :cond_0
    iget-object v0, p0, Lorg/apache/tools/ant/Target;->unlessCondition:Ljava/lang/String;

    goto :goto_0
.end method

.method public final performTasks()V
    .locals 4

    const/4 v1, 0x0

    iget-object v2, p0, Lorg/apache/tools/ant/Target;->project:Lorg/apache/tools/ant/Project;

    invoke-virtual {v2, p0}, Lorg/apache/tools/ant/Project;->fireTargetStarted(Lorg/apache/tools/ant/Target;)V

    :try_start_0
    invoke-virtual {p0}, Lorg/apache/tools/ant/Target;->execute()V
    :try_end_0
    .catch Ljava/lang/RuntimeException; {:try_start_0 .. :try_end_0} :catch_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    iget-object v2, p0, Lorg/apache/tools/ant/Target;->project:Lorg/apache/tools/ant/Project;

    invoke-virtual {v2, p0, v1}, Lorg/apache/tools/ant/Project;->fireTargetFinished(Lorg/apache/tools/ant/Target;Ljava/lang/Throwable;)V

    return-void

    :catch_0
    move-exception v0

    move-object v1, v0

    :try_start_1
    throw v0
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    :catchall_0
    move-exception v2

    iget-object v3, p0, Lorg/apache/tools/ant/Target;->project:Lorg/apache/tools/ant/Project;

    invoke-virtual {v3, p0, v1}, Lorg/apache/tools/ant/Project;->fireTargetFinished(Lorg/apache/tools/ant/Target;Ljava/lang/Throwable;)V

    throw v2
.end method

.method replaceChild(Lorg/apache/tools/ant/Task;Lorg/apache/tools/ant/RuntimeConfigurable;)V
    .locals 2
    .param p1    # Lorg/apache/tools/ant/Task;
    .param p2    # Lorg/apache/tools/ant/RuntimeConfigurable;

    :goto_0
    iget-object v1, p0, Lorg/apache/tools/ant/Target;->children:Ljava/util/List;

    invoke-interface {v1, p1}, Ljava/util/List;->indexOf(Ljava/lang/Object;)I

    move-result v0

    if-ltz v0, :cond_0

    iget-object v1, p0, Lorg/apache/tools/ant/Target;->children:Ljava/util/List;

    invoke-interface {v1, v0, p2}, Ljava/util/List;->set(ILjava/lang/Object;)Ljava/lang/Object;

    goto :goto_0

    :cond_0
    return-void
.end method

.method replaceChild(Lorg/apache/tools/ant/Task;Lorg/apache/tools/ant/Task;)V
    .locals 2
    .param p1    # Lorg/apache/tools/ant/Task;
    .param p2    # Lorg/apache/tools/ant/Task;

    :goto_0
    iget-object v1, p0, Lorg/apache/tools/ant/Target;->children:Ljava/util/List;

    invoke-interface {v1, p1}, Ljava/util/List;->indexOf(Ljava/lang/Object;)I

    move-result v0

    if-ltz v0, :cond_0

    iget-object v1, p0, Lorg/apache/tools/ant/Target;->children:Ljava/util/List;

    invoke-interface {v1, v0, p2}, Ljava/util/List;->set(ILjava/lang/Object;)Ljava/lang/Object;

    goto :goto_0

    :cond_0
    return-void
.end method

.method public setDepends(Ljava/lang/String;)V
    .locals 5
    .param p1    # Ljava/lang/String;

    invoke-virtual {p1}, Ljava/lang/String;->length()I

    move-result v2

    if-lez v2, :cond_4

    new-instance v0, Ljava/util/StringTokenizer;

    const-string v2, ","

    const/4 v3, 0x1

    invoke-direct {v0, p1, v2, v3}, Ljava/util/StringTokenizer;-><init>(Ljava/lang/String;Ljava/lang/String;Z)V

    :cond_0
    invoke-virtual {v0}, Ljava/util/StringTokenizer;->hasMoreTokens()Z

    move-result v2

    if-eqz v2, :cond_4

    invoke-virtual {v0}, Ljava/util/StringTokenizer;->nextToken()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/String;->trim()Ljava/lang/String;

    move-result-object v1

    const-string v2, ""

    invoke-virtual {v2, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-nez v2, :cond_1

    const-string v2, ","

    invoke-virtual {v2, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_2

    :cond_1
    new-instance v2, Lorg/apache/tools/ant/BuildException;

    new-instance v3, Ljava/lang/StringBuffer;

    invoke-direct {v3}, Ljava/lang/StringBuffer;-><init>()V

    const-string v4, "Syntax Error: depends attribute of target \""

    invoke-virtual {v3, v4}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    move-result-object v3

    invoke-virtual {p0}, Lorg/apache/tools/ant/Target;->getName()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v3, v4}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    move-result-object v3

    const-string v4, "\" has an empty string as dependency."

    invoke-virtual {v3, v4}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuffer;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-direct {v2, v3}, Lorg/apache/tools/ant/BuildException;-><init>(Ljava/lang/String;)V

    throw v2

    :cond_2
    invoke-virtual {p0, v1}, Lorg/apache/tools/ant/Target;->addDependency(Ljava/lang/String;)V

    invoke-virtual {v0}, Ljava/util/StringTokenizer;->hasMoreTokens()Z

    move-result v2

    if-eqz v2, :cond_0

    invoke-virtual {v0}, Ljava/util/StringTokenizer;->nextToken()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0}, Ljava/util/StringTokenizer;->hasMoreTokens()Z

    move-result v2

    if-eqz v2, :cond_3

    const-string v2, ","

    invoke-virtual {v2, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-nez v2, :cond_0

    :cond_3
    new-instance v2, Lorg/apache/tools/ant/BuildException;

    new-instance v3, Ljava/lang/StringBuffer;

    invoke-direct {v3}, Ljava/lang/StringBuffer;-><init>()V

    const-string v4, "Syntax Error: Depend attribute for target \""

    invoke-virtual {v3, v4}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    move-result-object v3

    invoke-virtual {p0}, Lorg/apache/tools/ant/Target;->getName()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v3, v4}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    move-result-object v3

    const-string v4, "\" ends with a , character"

    invoke-virtual {v3, v4}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuffer;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-direct {v2, v3}, Lorg/apache/tools/ant/BuildException;-><init>(Ljava/lang/String;)V

    throw v2

    :cond_4
    return-void
.end method

.method public setDescription(Ljava/lang/String;)V
    .locals 0
    .param p1    # Ljava/lang/String;

    iput-object p1, p0, Lorg/apache/tools/ant/Target;->description:Ljava/lang/String;

    return-void
.end method

.method public setIf(Ljava/lang/String;)V
    .locals 0
    .param p1    # Ljava/lang/String;

    if-nez p1, :cond_0

    const-string p1, ""

    :cond_0
    iput-object p1, p0, Lorg/apache/tools/ant/Target;->ifCondition:Ljava/lang/String;

    return-void
.end method

.method public setLocation(Lorg/apache/tools/ant/Location;)V
    .locals 0
    .param p1    # Lorg/apache/tools/ant/Location;

    iput-object p1, p0, Lorg/apache/tools/ant/Target;->location:Lorg/apache/tools/ant/Location;

    return-void
.end method

.method public setName(Ljava/lang/String;)V
    .locals 0
    .param p1    # Ljava/lang/String;

    iput-object p1, p0, Lorg/apache/tools/ant/Target;->name:Ljava/lang/String;

    return-void
.end method

.method public setProject(Lorg/apache/tools/ant/Project;)V
    .locals 0
    .param p1    # Lorg/apache/tools/ant/Project;

    iput-object p1, p0, Lorg/apache/tools/ant/Target;->project:Lorg/apache/tools/ant/Project;

    return-void
.end method

.method public setUnless(Ljava/lang/String;)V
    .locals 0
    .param p1    # Ljava/lang/String;

    if-nez p1, :cond_0

    const-string p1, ""

    :cond_0
    iput-object p1, p0, Lorg/apache/tools/ant/Target;->unlessCondition:Ljava/lang/String;

    return-void
.end method

.method public toString()Ljava/lang/String;
    .locals 1

    iget-object v0, p0, Lorg/apache/tools/ant/Target;->name:Ljava/lang/String;

    return-object v0
.end method
