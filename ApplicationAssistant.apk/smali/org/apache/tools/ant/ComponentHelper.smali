.class public Lorg/apache/tools/ant/ComponentHelper;
.super Ljava/lang/Object;
.source "ComponentHelper.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lorg/apache/tools/ant/ComponentHelper$AntTypeTable;
    }
.end annotation


# static fields
.field private static final ANT_PROPERTY_TASK:Ljava/lang/String; = "property"

.field private static final BUILD_SYSCLASSPATH_ONLY:Ljava/lang/String; = "only"

.field public static final COMPONENT_HELPER_REFERENCE:Ljava/lang/String; = "ant.ComponentHelper"

.field private static final ERROR_NO_TASK_LIST_LOAD:Ljava/lang/String; = "Can\'t load default task list"

.field private static final ERROR_NO_TYPE_LIST_LOAD:Ljava/lang/String; = "Can\'t load default type list"

.field static class$org$apache$tools$ant$ComponentHelper:Ljava/lang/Class;

.field static class$org$apache$tools$ant$Project:Ljava/lang/Class;

.field static class$org$apache$tools$ant$Task:Ljava/lang/Class;

.field static class$org$apache$tools$ant$TaskAdapter:Ljava/lang/Class;

.field static class$org$apache$tools$ant$taskdefs$Property:Ljava/lang/Class;

.field private static defaultDefinitions:[Ljava/util/Properties;


# instance fields
.field private antLibCurrentUri:Ljava/lang/String;

.field private antLibStack:Ljava/util/Stack;

.field private antTypeTable:Lorg/apache/tools/ant/ComponentHelper$AntTypeTable;

.field private checkedNamespaces:Ljava/util/Set;

.field private next:Lorg/apache/tools/ant/ComponentHelper;

.field private project:Lorg/apache/tools/ant/Project;

.field private rebuildTaskClassDefinitions:Z

.field private rebuildTypeClassDefinitions:Z

.field private taskClassDefinitions:Ljava/util/Hashtable;

.field private typeClassDefinitions:Ljava/util/Hashtable;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    const/4 v0, 0x2

    new-array v0, v0, [Ljava/util/Properties;

    sput-object v0, Lorg/apache/tools/ant/ComponentHelper;->defaultDefinitions:[Ljava/util/Properties;

    return-void
.end method

.method protected constructor <init>()V
    .locals 2

    const/4 v1, 0x1

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    new-instance v0, Ljava/util/Hashtable;

    invoke-direct {v0}, Ljava/util/Hashtable;-><init>()V

    iput-object v0, p0, Lorg/apache/tools/ant/ComponentHelper;->taskClassDefinitions:Ljava/util/Hashtable;

    iput-boolean v1, p0, Lorg/apache/tools/ant/ComponentHelper;->rebuildTaskClassDefinitions:Z

    new-instance v0, Ljava/util/Hashtable;

    invoke-direct {v0}, Ljava/util/Hashtable;-><init>()V

    iput-object v0, p0, Lorg/apache/tools/ant/ComponentHelper;->typeClassDefinitions:Ljava/util/Hashtable;

    iput-boolean v1, p0, Lorg/apache/tools/ant/ComponentHelper;->rebuildTypeClassDefinitions:Z

    new-instance v0, Ljava/util/HashSet;

    invoke-direct {v0}, Ljava/util/HashSet;-><init>()V

    iput-object v0, p0, Lorg/apache/tools/ant/ComponentHelper;->checkedNamespaces:Ljava/util/Set;

    new-instance v0, Ljava/util/Stack;

    invoke-direct {v0}, Ljava/util/Stack;-><init>()V

    iput-object v0, p0, Lorg/apache/tools/ant/ComponentHelper;->antLibStack:Ljava/util/Stack;

    const/4 v0, 0x0

    iput-object v0, p0, Lorg/apache/tools/ant/ComponentHelper;->antLibCurrentUri:Ljava/lang/String;

    return-void
.end method

.method private declared-synchronized checkNamespace(Ljava/lang/String;)V
    .locals 4
    .param p1    # Ljava/lang/String;

    monitor-enter p0

    :try_start_0
    invoke-static {p1}, Lorg/apache/tools/ant/ProjectHelper;->extractUriFromComponentName(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    const-string v2, ""

    invoke-virtual {v2, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_0

    const-string v1, "antlib:org.apache.tools.ant"

    :cond_0
    const-string v2, "antlib:"

    invoke-virtual {v1, v2}, Ljava/lang/String;->startsWith(Ljava/lang/String;)Z
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    move-result v2

    if-nez v2, :cond_2

    :cond_1
    :goto_0
    monitor-exit p0

    return-void

    :cond_2
    :try_start_1
    iget-object v2, p0, Lorg/apache/tools/ant/ComponentHelper;->checkedNamespaces:Ljava/util/Set;

    invoke-interface {v2, v1}, Ljava/util/Set;->contains(Ljava/lang/Object;)Z

    move-result v2

    if-nez v2, :cond_1

    iget-object v2, p0, Lorg/apache/tools/ant/ComponentHelper;->checkedNamespaces:Ljava/util/Set;

    invoke-interface {v2, v1}, Ljava/util/Set;->add(Ljava/lang/Object;)Z

    new-instance v0, Lorg/apache/tools/ant/taskdefs/Typedef;

    invoke-direct {v0}, Lorg/apache/tools/ant/taskdefs/Typedef;-><init>()V

    iget-object v2, p0, Lorg/apache/tools/ant/ComponentHelper;->project:Lorg/apache/tools/ant/Project;

    invoke-virtual {v0, v2}, Lorg/apache/tools/ant/taskdefs/Typedef;->setProject(Lorg/apache/tools/ant/Project;)V

    invoke-virtual {v0}, Lorg/apache/tools/ant/taskdefs/Typedef;->init()V

    invoke-virtual {v0, v1}, Lorg/apache/tools/ant/taskdefs/Typedef;->setURI(Ljava/lang/String;)V

    invoke-virtual {v0, v1}, Lorg/apache/tools/ant/taskdefs/Typedef;->setTaskName(Ljava/lang/String;)V

    invoke-static {v1}, Lorg/apache/tools/ant/taskdefs/Definer;->makeResourceFromURI(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0, v2}, Lorg/apache/tools/ant/taskdefs/Typedef;->setResource(Ljava/lang/String;)V

    new-instance v2, Lorg/apache/tools/ant/taskdefs/Definer$OnError;

    const-string v3, "ignore"

    invoke-direct {v2, v3}, Lorg/apache/tools/ant/taskdefs/Definer$OnError;-><init>(Ljava/lang/String;)V

    invoke-virtual {v0, v2}, Lorg/apache/tools/ant/taskdefs/Typedef;->setOnError(Lorg/apache/tools/ant/taskdefs/Definer$OnError;)V

    invoke-virtual {v0}, Lorg/apache/tools/ant/taskdefs/Typedef;->execute()V
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    goto :goto_0

    :catchall_0
    move-exception v2

    monitor-exit p0

    throw v2
.end method

.method static class$(Ljava/lang/String;)Ljava/lang/Class;
    .locals 3
    .param p0    # Ljava/lang/String;

    :try_start_0
    invoke-static {p0}, Ljava/lang/Class;->forName(Ljava/lang/String;)Ljava/lang/Class;
    :try_end_0
    .catch Ljava/lang/ClassNotFoundException; {:try_start_0 .. :try_end_0} :catch_0

    move-result-object v1

    return-object v1

    :catch_0
    move-exception v0

    new-instance v1, Ljava/lang/NoClassDefFoundError;

    invoke-virtual {v0}, Ljava/lang/ClassNotFoundException;->getMessage()Ljava/lang/String;

    move-result-object v2

    invoke-direct {v1, v2}, Ljava/lang/NoClassDefFoundError;-><init>(Ljava/lang/String;)V

    throw v1
.end method

.method private createNewTask(Ljava/lang/String;)Lorg/apache/tools/ant/Task;
    .locals 6
    .param p1    # Ljava/lang/String;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lorg/apache/tools/ant/BuildException;
        }
    .end annotation

    const/4 v2, 0x0

    invoke-virtual {p0, p1}, Lorg/apache/tools/ant/ComponentHelper;->getComponentClass(Ljava/lang/String;)Ljava/lang/Class;

    move-result-object v0

    if-eqz v0, :cond_0

    sget-object v3, Lorg/apache/tools/ant/ComponentHelper;->class$org$apache$tools$ant$Task:Ljava/lang/Class;

    if-nez v3, :cond_1

    const-string v3, "org.apache.tools.ant.Task"

    invoke-static {v3}, Lorg/apache/tools/ant/ComponentHelper;->class$(Ljava/lang/String;)Ljava/lang/Class;

    move-result-object v3

    sput-object v3, Lorg/apache/tools/ant/ComponentHelper;->class$org$apache$tools$ant$Task:Ljava/lang/Class;

    :goto_0
    invoke-virtual {v3, v0}, Ljava/lang/Class;->isAssignableFrom(Ljava/lang/Class;)Z

    move-result v3

    if-nez v3, :cond_2

    :cond_0
    :goto_1
    return-object v2

    :cond_1
    sget-object v3, Lorg/apache/tools/ant/ComponentHelper;->class$org$apache$tools$ant$Task:Ljava/lang/Class;

    goto :goto_0

    :cond_2
    invoke-virtual {p0, p1}, Lorg/apache/tools/ant/ComponentHelper;->createComponent(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v1

    if-eqz v1, :cond_0

    instance-of v3, v1, Lorg/apache/tools/ant/Task;

    if-nez v3, :cond_3

    new-instance v3, Lorg/apache/tools/ant/BuildException;

    new-instance v4, Ljava/lang/StringBuffer;

    invoke-direct {v4}, Ljava/lang/StringBuffer;-><init>()V

    const-string v5, "Expected a Task from \'"

    invoke-virtual {v4, v5}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    move-result-object v4

    invoke-virtual {v4, p1}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    move-result-object v4

    const-string v5, "\' but got an instance of "

    invoke-virtual {v4, v5}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    move-result-object v4

    invoke-virtual {v1}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/Class;->getName()Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v4, v5}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    move-result-object v4

    const-string v5, " instead"

    invoke-virtual {v4, v5}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuffer;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-direct {v3, v4}, Lorg/apache/tools/ant/BuildException;-><init>(Ljava/lang/String;)V

    throw v3

    :cond_3
    move-object v2, v1

    check-cast v2, Lorg/apache/tools/ant/Task;

    invoke-virtual {v2, p1}, Lorg/apache/tools/ant/Task;->setTaskType(Ljava/lang/String;)V

    invoke-virtual {v2, p1}, Lorg/apache/tools/ant/Task;->setTaskName(Ljava/lang/String;)V

    iget-object v3, p0, Lorg/apache/tools/ant/ComponentHelper;->project:Lorg/apache/tools/ant/Project;

    new-instance v4, Ljava/lang/StringBuffer;

    invoke-direct {v4}, Ljava/lang/StringBuffer;-><init>()V

    const-string v5, "   +Task: "

    invoke-virtual {v4, v5}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    move-result-object v4

    invoke-virtual {v4, p1}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuffer;->toString()Ljava/lang/String;

    move-result-object v4

    const/4 v5, 0x4

    invoke-virtual {v3, v4, v5}, Lorg/apache/tools/ant/Project;->log(Ljava/lang/String;I)V

    goto :goto_1
.end method

.method private getClassLoader(Ljava/lang/ClassLoader;)Ljava/lang/ClassLoader;
    .locals 3
    .param p1    # Ljava/lang/ClassLoader;

    iget-object v1, p0, Lorg/apache/tools/ant/ComponentHelper;->project:Lorg/apache/tools/ant/Project;

    const-string v2, "build.sysclasspath"

    invoke-virtual {v1, v2}, Lorg/apache/tools/ant/Project;->getProperty(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    iget-object v1, p0, Lorg/apache/tools/ant/ComponentHelper;->project:Lorg/apache/tools/ant/Project;

    invoke-virtual {v1}, Lorg/apache/tools/ant/Project;->getCoreLoader()Ljava/lang/ClassLoader;

    move-result-object v1

    if-eqz v1, :cond_0

    const-string v1, "only"

    invoke-virtual {v1, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-nez v1, :cond_0

    iget-object v1, p0, Lorg/apache/tools/ant/ComponentHelper;->project:Lorg/apache/tools/ant/Project;

    invoke-virtual {v1}, Lorg/apache/tools/ant/Project;->getCoreLoader()Ljava/lang/ClassLoader;

    move-result-object p1

    :cond_0
    return-object p1
.end method

.method public static getComponentHelper(Lorg/apache/tools/ant/Project;)Lorg/apache/tools/ant/ComponentHelper;
    .locals 2
    .param p0    # Lorg/apache/tools/ant/Project;

    if-nez p0, :cond_1

    const/4 v0, 0x0

    :cond_0
    :goto_0
    return-object v0

    :cond_1
    const-string v1, "ant.ComponentHelper"

    invoke-virtual {p0, v1}, Lorg/apache/tools/ant/Project;->getReference(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lorg/apache/tools/ant/ComponentHelper;

    if-nez v0, :cond_0

    new-instance v0, Lorg/apache/tools/ant/ComponentHelper;

    invoke-direct {v0}, Lorg/apache/tools/ant/ComponentHelper;-><init>()V

    invoke-virtual {v0, p0}, Lorg/apache/tools/ant/ComponentHelper;->setProject(Lorg/apache/tools/ant/Project;)V

    const-string v1, "ant.ComponentHelper"

    invoke-virtual {p0, v1, v0}, Lorg/apache/tools/ant/Project;->addReference(Ljava/lang/String;Ljava/lang/Object;)V

    goto :goto_0
.end method

.method private static declared-synchronized getDefaultDefinitions(Z)Ljava/util/Properties;
    .locals 8
    .param p0    # Z
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lorg/apache/tools/ant/BuildException;
        }
    .end annotation

    const-class v7, Lorg/apache/tools/ant/ComponentHelper;

    monitor-enter v7

    if-eqz p0, :cond_0

    const/4 v2, 0x1

    :goto_0
    :try_start_0
    sget-object v6, Lorg/apache/tools/ant/ComponentHelper;->defaultDefinitions:[Ljava/util/Properties;

    aget-object v6, v6, v2

    if-nez v6, :cond_5

    if-eqz p0, :cond_1

    const-string v5, "/org/apache/tools/ant/types/defaults.properties"

    :goto_1
    if-eqz p0, :cond_2

    const-string v1, "Can\'t load default type list"
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    :goto_2
    const/4 v3, 0x0

    :try_start_1
    sget-object v6, Lorg/apache/tools/ant/ComponentHelper;->class$org$apache$tools$ant$ComponentHelper:Ljava/lang/Class;

    if-nez v6, :cond_3

    const-string v6, "org.apache.tools.ant.ComponentHelper"

    invoke-static {v6}, Lorg/apache/tools/ant/ComponentHelper;->class$(Ljava/lang/String;)Ljava/lang/Class;

    move-result-object v6

    sput-object v6, Lorg/apache/tools/ant/ComponentHelper;->class$org$apache$tools$ant$ComponentHelper:Ljava/lang/Class;

    :goto_3
    invoke-virtual {v6, v5}, Ljava/lang/Class;->getResourceAsStream(Ljava/lang/String;)Ljava/io/InputStream;

    move-result-object v3

    if-nez v3, :cond_4

    new-instance v6, Lorg/apache/tools/ant/BuildException;

    invoke-direct {v6, v1}, Lorg/apache/tools/ant/BuildException;-><init>(Ljava/lang/String;)V

    throw v6
    :try_end_1
    .catch Ljava/io/IOException; {:try_start_1 .. :try_end_1} :catch_0
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    :catch_0
    move-exception v0

    :try_start_2
    new-instance v6, Lorg/apache/tools/ant/BuildException;

    invoke-direct {v6, v1, v0}, Lorg/apache/tools/ant/BuildException;-><init>(Ljava/lang/String;Ljava/lang/Throwable;)V

    throw v6
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    :catchall_0
    move-exception v6

    :try_start_3
    invoke-static {v3}, Lorg/apache/tools/ant/util/FileUtils;->close(Ljava/io/InputStream;)V

    throw v6
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_1

    :catchall_1
    move-exception v6

    monitor-exit v7

    throw v6

    :cond_0
    const/4 v2, 0x0

    goto :goto_0

    :cond_1
    :try_start_4
    const-string v5, "/org/apache/tools/ant/taskdefs/defaults.properties"

    goto :goto_1

    :cond_2
    const-string v1, "Can\'t load default task list"
    :try_end_4
    .catchall {:try_start_4 .. :try_end_4} :catchall_1

    goto :goto_2

    :cond_3
    :try_start_5
    sget-object v6, Lorg/apache/tools/ant/ComponentHelper;->class$org$apache$tools$ant$ComponentHelper:Ljava/lang/Class;

    goto :goto_3

    :cond_4
    new-instance v4, Ljava/util/Properties;

    invoke-direct {v4}, Ljava/util/Properties;-><init>()V

    invoke-virtual {v4, v3}, Ljava/util/Properties;->load(Ljava/io/InputStream;)V

    sget-object v6, Lorg/apache/tools/ant/ComponentHelper;->defaultDefinitions:[Ljava/util/Properties;

    aput-object v4, v6, v2
    :try_end_5
    .catch Ljava/io/IOException; {:try_start_5 .. :try_end_5} :catch_0
    .catchall {:try_start_5 .. :try_end_5} :catchall_0

    :try_start_6
    invoke-static {v3}, Lorg/apache/tools/ant/util/FileUtils;->close(Ljava/io/InputStream;)V

    :cond_5
    sget-object v6, Lorg/apache/tools/ant/ComponentHelper;->defaultDefinitions:[Ljava/util/Properties;

    aget-object v6, v6, v2
    :try_end_6
    .catchall {:try_start_6 .. :try_end_6} :catchall_1

    monitor-exit v7

    return-object v6
.end method

.method public static getElementName(Lorg/apache/tools/ant/Project;Ljava/lang/Object;Z)Ljava/lang/String;
    .locals 1
    .param p0    # Lorg/apache/tools/ant/Project;
    .param p1    # Ljava/lang/Object;
    .param p2    # Z

    if-nez p0, :cond_0

    invoke-static {p1}, Lorg/apache/tools/ant/ComponentHelper;->getProject(Ljava/lang/Object;)Lorg/apache/tools/ant/Project;

    move-result-object p0

    :cond_0
    if-nez p0, :cond_1

    invoke-virtual {p1}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v0

    invoke-static {v0, p2}, Lorg/apache/tools/ant/ComponentHelper;->getUnmappedElementName(Ljava/lang/Class;Z)Ljava/lang/String;

    move-result-object v0

    :goto_0
    return-object v0

    :cond_1
    invoke-static {p0}, Lorg/apache/tools/ant/ComponentHelper;->getComponentHelper(Lorg/apache/tools/ant/Project;)Lorg/apache/tools/ant/ComponentHelper;

    move-result-object v0

    invoke-virtual {v0, p1, p2}, Lorg/apache/tools/ant/ComponentHelper;->getElementName(Ljava/lang/Object;Z)Ljava/lang/String;

    move-result-object v0

    goto :goto_0
.end method

.method private static getProject(Ljava/lang/Object;)Lorg/apache/tools/ant/Project;
    .locals 5
    .param p0    # Ljava/lang/Object;

    const/4 v2, 0x0

    instance-of v1, p0, Lorg/apache/tools/ant/ProjectComponent;

    if-eqz v1, :cond_0

    check-cast p0, Lorg/apache/tools/ant/ProjectComponent;

    invoke-virtual {p0}, Lorg/apache/tools/ant/ProjectComponent;->getProject()Lorg/apache/tools/ant/Project;

    move-result-object v1

    :goto_0
    return-object v1

    :cond_0
    :try_start_0
    invoke-virtual {p0}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v3

    const-string v4, "getProject"

    const/4 v1, 0x0

    check-cast v1, [Ljava/lang/Class;

    invoke-virtual {v3, v4, v1}, Ljava/lang/Class;->getMethod(Ljava/lang/String;[Ljava/lang/Class;)Ljava/lang/reflect/Method;

    move-result-object v0

    sget-object v1, Lorg/apache/tools/ant/ComponentHelper;->class$org$apache$tools$ant$Project:Ljava/lang/Class;

    if-nez v1, :cond_1

    const-string v1, "org.apache.tools.ant.Project"

    invoke-static {v1}, Lorg/apache/tools/ant/ComponentHelper;->class$(Ljava/lang/String;)Ljava/lang/Class;

    move-result-object v1

    sput-object v1, Lorg/apache/tools/ant/ComponentHelper;->class$org$apache$tools$ant$Project:Ljava/lang/Class;

    :goto_1
    invoke-virtual {v0}, Ljava/lang/reflect/Method;->getReturnType()Ljava/lang/Class;

    move-result-object v3

    if-ne v1, v3, :cond_2

    const/4 v1, 0x0

    check-cast v1, [Ljava/lang/Object;

    invoke-virtual {v0, p0, v1}, Ljava/lang/reflect/Method;->invoke(Ljava/lang/Object;[Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lorg/apache/tools/ant/Project;

    goto :goto_0

    :cond_1
    sget-object v1, Lorg/apache/tools/ant/ComponentHelper;->class$org$apache$tools$ant$Project:Ljava/lang/Class;
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    goto :goto_1

    :catch_0
    move-exception v1

    :cond_2
    move-object v1, v2

    goto :goto_0
.end method

.method private static getUnmappedElementName(Ljava/lang/Class;Z)Ljava/lang/String;
    .locals 2
    .param p0    # Ljava/lang/Class;
    .param p1    # Z

    if-eqz p1, :cond_0

    invoke-virtual {p0}, Ljava/lang/Class;->getName()Ljava/lang/String;

    move-result-object v0

    const/16 v1, 0x2e

    invoke-virtual {v0, v1}, Ljava/lang/String;->lastIndexOf(I)I

    move-result v1

    add-int/lit8 v1, v1, 0x1

    invoke-virtual {v0, v1}, Ljava/lang/String;->substring(I)Ljava/lang/String;

    move-result-object v1

    :goto_0
    return-object v1

    :cond_0
    invoke-virtual {p0}, Ljava/lang/Class;->toString()Ljava/lang/String;

    move-result-object v1

    goto :goto_0
.end method

.method private initTasks()V
    .locals 7

    const/4 v6, 0x0

    invoke-direct {p0, v6}, Lorg/apache/tools/ant/ComponentHelper;->getClassLoader(Ljava/lang/ClassLoader;)Ljava/lang/ClassLoader;

    move-result-object v0

    const/4 v6, 0x0

    invoke-static {v6}, Lorg/apache/tools/ant/ComponentHelper;->getDefaultDefinitions(Z)Ljava/util/Properties;

    move-result-object v5

    invoke-virtual {v5}, Ljava/util/Properties;->propertyNames()Ljava/util/Enumeration;

    move-result-object v3

    :goto_0
    invoke-interface {v3}, Ljava/util/Enumeration;->hasMoreElements()Z

    move-result v6

    if-eqz v6, :cond_2

    invoke-interface {v3}, Ljava/util/Enumeration;->nextElement()Ljava/lang/Object;

    move-result-object v4

    check-cast v4, Ljava/lang/String;

    invoke-virtual {v5, v4}, Ljava/util/Properties;->getProperty(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    new-instance v2, Lorg/apache/tools/ant/AntTypeDefinition;

    invoke-direct {v2}, Lorg/apache/tools/ant/AntTypeDefinition;-><init>()V

    invoke-virtual {v2, v4}, Lorg/apache/tools/ant/AntTypeDefinition;->setName(Ljava/lang/String;)V

    invoke-virtual {v2, v1}, Lorg/apache/tools/ant/AntTypeDefinition;->setClassName(Ljava/lang/String;)V

    invoke-virtual {v2, v0}, Lorg/apache/tools/ant/AntTypeDefinition;->setClassLoader(Ljava/lang/ClassLoader;)V

    sget-object v6, Lorg/apache/tools/ant/ComponentHelper;->class$org$apache$tools$ant$Task:Ljava/lang/Class;

    if-nez v6, :cond_0

    const-string v6, "org.apache.tools.ant.Task"

    invoke-static {v6}, Lorg/apache/tools/ant/ComponentHelper;->class$(Ljava/lang/String;)Ljava/lang/Class;

    move-result-object v6

    sput-object v6, Lorg/apache/tools/ant/ComponentHelper;->class$org$apache$tools$ant$Task:Ljava/lang/Class;

    :goto_1
    invoke-virtual {v2, v6}, Lorg/apache/tools/ant/AntTypeDefinition;->setAdaptToClass(Ljava/lang/Class;)V

    sget-object v6, Lorg/apache/tools/ant/ComponentHelper;->class$org$apache$tools$ant$TaskAdapter:Ljava/lang/Class;

    if-nez v6, :cond_1

    const-string v6, "org.apache.tools.ant.TaskAdapter"

    invoke-static {v6}, Lorg/apache/tools/ant/ComponentHelper;->class$(Ljava/lang/String;)Ljava/lang/Class;

    move-result-object v6

    sput-object v6, Lorg/apache/tools/ant/ComponentHelper;->class$org$apache$tools$ant$TaskAdapter:Ljava/lang/Class;

    :goto_2
    invoke-virtual {v2, v6}, Lorg/apache/tools/ant/AntTypeDefinition;->setAdapterClass(Ljava/lang/Class;)V

    iget-object v6, p0, Lorg/apache/tools/ant/ComponentHelper;->antTypeTable:Lorg/apache/tools/ant/ComponentHelper$AntTypeTable;

    invoke-virtual {v6, v4, v2}, Lorg/apache/tools/ant/ComponentHelper$AntTypeTable;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    goto :goto_0

    :cond_0
    sget-object v6, Lorg/apache/tools/ant/ComponentHelper;->class$org$apache$tools$ant$Task:Ljava/lang/Class;

    goto :goto_1

    :cond_1
    sget-object v6, Lorg/apache/tools/ant/ComponentHelper;->class$org$apache$tools$ant$TaskAdapter:Ljava/lang/Class;

    goto :goto_2

    :cond_2
    return-void
.end method

.method private initTypes()V
    .locals 7

    const/4 v6, 0x0

    invoke-direct {p0, v6}, Lorg/apache/tools/ant/ComponentHelper;->getClassLoader(Ljava/lang/ClassLoader;)Ljava/lang/ClassLoader;

    move-result-object v0

    const/4 v6, 0x1

    invoke-static {v6}, Lorg/apache/tools/ant/ComponentHelper;->getDefaultDefinitions(Z)Ljava/util/Properties;

    move-result-object v5

    invoke-virtual {v5}, Ljava/util/Properties;->propertyNames()Ljava/util/Enumeration;

    move-result-object v3

    :goto_0
    invoke-interface {v3}, Ljava/util/Enumeration;->hasMoreElements()Z

    move-result v6

    if-eqz v6, :cond_0

    invoke-interface {v3}, Ljava/util/Enumeration;->nextElement()Ljava/lang/Object;

    move-result-object v4

    check-cast v4, Ljava/lang/String;

    invoke-virtual {v5, v4}, Ljava/util/Properties;->getProperty(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    new-instance v2, Lorg/apache/tools/ant/AntTypeDefinition;

    invoke-direct {v2}, Lorg/apache/tools/ant/AntTypeDefinition;-><init>()V

    invoke-virtual {v2, v4}, Lorg/apache/tools/ant/AntTypeDefinition;->setName(Ljava/lang/String;)V

    invoke-virtual {v2, v1}, Lorg/apache/tools/ant/AntTypeDefinition;->setClassName(Ljava/lang/String;)V

    invoke-virtual {v2, v0}, Lorg/apache/tools/ant/AntTypeDefinition;->setClassLoader(Ljava/lang/ClassLoader;)V

    iget-object v6, p0, Lorg/apache/tools/ant/ComponentHelper;->antTypeTable:Lorg/apache/tools/ant/ComponentHelper$AntTypeTable;

    invoke-virtual {v6, v4, v2}, Lorg/apache/tools/ant/ComponentHelper$AntTypeTable;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    goto :goto_0

    :cond_0
    return-void
.end method

.method private sameDefinition(Lorg/apache/tools/ant/AntTypeDefinition;Lorg/apache/tools/ant/AntTypeDefinition;)Z
    .locals 5
    .param p1    # Lorg/apache/tools/ant/AntTypeDefinition;
    .param p2    # Lorg/apache/tools/ant/AntTypeDefinition;

    const/4 v2, 0x1

    const/4 v3, 0x0

    invoke-direct {p0, p1}, Lorg/apache/tools/ant/ComponentHelper;->validDefinition(Lorg/apache/tools/ant/AntTypeDefinition;)Z

    move-result v0

    invoke-direct {p0, p2}, Lorg/apache/tools/ant/ComponentHelper;->validDefinition(Lorg/apache/tools/ant/AntTypeDefinition;)Z

    move-result v4

    if-ne v0, v4, :cond_1

    move v1, v2

    :goto_0
    if-eqz v1, :cond_2

    if-eqz v0, :cond_0

    iget-object v4, p0, Lorg/apache/tools/ant/ComponentHelper;->project:Lorg/apache/tools/ant/Project;

    invoke-virtual {p1, p2, v4}, Lorg/apache/tools/ant/AntTypeDefinition;->sameDefinition(Lorg/apache/tools/ant/AntTypeDefinition;Lorg/apache/tools/ant/Project;)Z

    move-result v4

    if-eqz v4, :cond_2

    :cond_0
    :goto_1
    return v2

    :cond_1
    move v1, v3

    goto :goto_0

    :cond_2
    move v2, v3

    goto :goto_1
.end method

.method private updateDataTypeDefinition(Lorg/apache/tools/ant/AntTypeDefinition;)V
    .locals 9
    .param p1    # Lorg/apache/tools/ant/AntTypeDefinition;

    const/4 v4, 0x1

    invoke-virtual {p1}, Lorg/apache/tools/ant/AntTypeDefinition;->getName()Ljava/lang/String;

    move-result-object v1

    iget-object v6, p0, Lorg/apache/tools/ant/ComponentHelper;->antTypeTable:Lorg/apache/tools/ant/ComponentHelper$AntTypeTable;

    monitor-enter v6

    const/4 v5, 0x1

    :try_start_0
    iput-boolean v5, p0, Lorg/apache/tools/ant/ComponentHelper;->rebuildTaskClassDefinitions:Z

    const/4 v5, 0x1

    iput-boolean v5, p0, Lorg/apache/tools/ant/ComponentHelper;->rebuildTypeClassDefinitions:Z

    iget-object v5, p0, Lorg/apache/tools/ant/ComponentHelper;->antTypeTable:Lorg/apache/tools/ant/ComponentHelper$AntTypeTable;

    invoke-virtual {v5, v1}, Lorg/apache/tools/ant/ComponentHelper$AntTypeTable;->getDefinition(Ljava/lang/String;)Lorg/apache/tools/ant/AntTypeDefinition;

    move-result-object v2

    if-eqz v2, :cond_2

    invoke-direct {p0, p1, v2}, Lorg/apache/tools/ant/ComponentHelper;->sameDefinition(Lorg/apache/tools/ant/AntTypeDefinition;Lorg/apache/tools/ant/AntTypeDefinition;)Z

    move-result v5

    if-eqz v5, :cond_0

    monitor-exit v6

    :goto_0
    return-void

    :cond_0
    iget-object v5, p0, Lorg/apache/tools/ant/ComponentHelper;->antTypeTable:Lorg/apache/tools/ant/ComponentHelper$AntTypeTable;

    invoke-virtual {v5, v1}, Lorg/apache/tools/ant/ComponentHelper$AntTypeTable;->getExposedClass(Ljava/lang/String;)Ljava/lang/Class;

    move-result-object v3

    if-eqz v3, :cond_4

    sget-object v5, Lorg/apache/tools/ant/ComponentHelper;->class$org$apache$tools$ant$Task:Ljava/lang/Class;

    if-nez v5, :cond_3

    const-string v5, "org.apache.tools.ant.Task"

    invoke-static {v5}, Lorg/apache/tools/ant/ComponentHelper;->class$(Ljava/lang/String;)Ljava/lang/Class;

    move-result-object v5

    sput-object v5, Lorg/apache/tools/ant/ComponentHelper;->class$org$apache$tools$ant$Task:Ljava/lang/Class;

    :goto_1
    invoke-virtual {v5, v3}, Ljava/lang/Class;->isAssignableFrom(Ljava/lang/Class;)Z

    move-result v5

    if-eqz v5, :cond_4

    move v0, v4

    :goto_2
    iget-object v7, p0, Lorg/apache/tools/ant/ComponentHelper;->project:Lorg/apache/tools/ant/Project;

    new-instance v5, Ljava/lang/StringBuffer;

    invoke-direct {v5}, Ljava/lang/StringBuffer;-><init>()V

    const-string v8, "Trying to override old definition of "

    invoke-virtual {v5, v8}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    move-result-object v8

    if-eqz v0, :cond_5

    const-string v5, "task "

    :goto_3
    invoke-virtual {v8, v5}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    move-result-object v5

    invoke-virtual {v5, v1}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/StringBuffer;->toString()Ljava/lang/String;

    move-result-object v5

    iget-object v8, p0, Lorg/apache/tools/ant/ComponentHelper;->project:Lorg/apache/tools/ant/Project;

    invoke-virtual {p1, v2, v8}, Lorg/apache/tools/ant/AntTypeDefinition;->similarDefinition(Lorg/apache/tools/ant/AntTypeDefinition;Lorg/apache/tools/ant/Project;)Z

    move-result v8

    if-eqz v8, :cond_1

    const/4 v4, 0x3

    :cond_1
    invoke-virtual {v7, v5, v4}, Lorg/apache/tools/ant/Project;->log(Ljava/lang/String;I)V

    :cond_2
    iget-object v4, p0, Lorg/apache/tools/ant/ComponentHelper;->project:Lorg/apache/tools/ant/Project;

    new-instance v5, Ljava/lang/StringBuffer;

    invoke-direct {v5}, Ljava/lang/StringBuffer;-><init>()V

    const-string v7, " +Datatype "

    invoke-virtual {v5, v7}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    move-result-object v5

    invoke-virtual {v5, v1}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    move-result-object v5

    const-string v7, " "

    invoke-virtual {v5, v7}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    move-result-object v5

    invoke-virtual {p1}, Lorg/apache/tools/ant/AntTypeDefinition;->getClassName()Ljava/lang/String;

    move-result-object v7

    invoke-virtual {v5, v7}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/StringBuffer;->toString()Ljava/lang/String;

    move-result-object v5

    const/4 v7, 0x4

    invoke-virtual {v4, v5, v7}, Lorg/apache/tools/ant/Project;->log(Ljava/lang/String;I)V

    iget-object v4, p0, Lorg/apache/tools/ant/ComponentHelper;->antTypeTable:Lorg/apache/tools/ant/ComponentHelper$AntTypeTable;

    invoke-virtual {v4, v1, p1}, Lorg/apache/tools/ant/ComponentHelper$AntTypeTable;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    monitor-exit v6

    goto :goto_0

    :catchall_0
    move-exception v4

    monitor-exit v6
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    throw v4

    :cond_3
    :try_start_1
    sget-object v5, Lorg/apache/tools/ant/ComponentHelper;->class$org$apache$tools$ant$Task:Ljava/lang/Class;

    goto :goto_1

    :cond_4
    const/4 v0, 0x0

    goto :goto_2

    :cond_5
    const-string v5, "datatype "
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    goto :goto_3
.end method

.method private validDefinition(Lorg/apache/tools/ant/AntTypeDefinition;)Z
    .locals 1
    .param p1    # Lorg/apache/tools/ant/AntTypeDefinition;

    iget-object v0, p0, Lorg/apache/tools/ant/ComponentHelper;->project:Lorg/apache/tools/ant/Project;

    invoke-virtual {p1, v0}, Lorg/apache/tools/ant/AntTypeDefinition;->getTypeClass(Lorg/apache/tools/ant/Project;)Ljava/lang/Class;

    move-result-object v0

    if-eqz v0, :cond_0

    iget-object v0, p0, Lorg/apache/tools/ant/ComponentHelper;->project:Lorg/apache/tools/ant/Project;

    invoke-virtual {p1, v0}, Lorg/apache/tools/ant/AntTypeDefinition;->getExposedClass(Lorg/apache/tools/ant/Project;)Ljava/lang/Class;

    move-result-object v0

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method


# virtual methods
.method public addDataTypeDefinition(Ljava/lang/String;Ljava/lang/Class;)V
    .locals 4
    .param p1    # Ljava/lang/String;
    .param p2    # Ljava/lang/Class;

    new-instance v0, Lorg/apache/tools/ant/AntTypeDefinition;

    invoke-direct {v0}, Lorg/apache/tools/ant/AntTypeDefinition;-><init>()V

    invoke-virtual {v0, p1}, Lorg/apache/tools/ant/AntTypeDefinition;->setName(Ljava/lang/String;)V

    invoke-virtual {v0, p2}, Lorg/apache/tools/ant/AntTypeDefinition;->setClass(Ljava/lang/Class;)V

    invoke-direct {p0, v0}, Lorg/apache/tools/ant/ComponentHelper;->updateDataTypeDefinition(Lorg/apache/tools/ant/AntTypeDefinition;)V

    iget-object v1, p0, Lorg/apache/tools/ant/ComponentHelper;->project:Lorg/apache/tools/ant/Project;

    new-instance v2, Ljava/lang/StringBuffer;

    invoke-direct {v2}, Ljava/lang/StringBuffer;-><init>()V

    const-string v3, " +User datatype: "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    move-result-object v2

    invoke-virtual {v2, p1}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    move-result-object v2

    const-string v3, "     "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    move-result-object v2

    invoke-virtual {p2}, Ljava/lang/Class;->getName()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuffer;->toString()Ljava/lang/String;

    move-result-object v2

    const/4 v3, 0x4

    invoke-virtual {v1, v2, v3}, Lorg/apache/tools/ant/Project;->log(Ljava/lang/String;I)V

    return-void
.end method

.method public addDataTypeDefinition(Lorg/apache/tools/ant/AntTypeDefinition;)V
    .locals 0
    .param p1    # Lorg/apache/tools/ant/AntTypeDefinition;

    invoke-direct {p0, p1}, Lorg/apache/tools/ant/ComponentHelper;->updateDataTypeDefinition(Lorg/apache/tools/ant/AntTypeDefinition;)V

    return-void
.end method

.method public addTaskDefinition(Ljava/lang/String;Ljava/lang/Class;)V
    .locals 2
    .param p1    # Ljava/lang/String;
    .param p2    # Ljava/lang/Class;

    invoke-virtual {p0, p2}, Lorg/apache/tools/ant/ComponentHelper;->checkTaskClass(Ljava/lang/Class;)V

    new-instance v0, Lorg/apache/tools/ant/AntTypeDefinition;

    invoke-direct {v0}, Lorg/apache/tools/ant/AntTypeDefinition;-><init>()V

    invoke-virtual {v0, p1}, Lorg/apache/tools/ant/AntTypeDefinition;->setName(Ljava/lang/String;)V

    invoke-virtual {p2}, Ljava/lang/Class;->getClassLoader()Ljava/lang/ClassLoader;

    move-result-object v1

    invoke-virtual {v0, v1}, Lorg/apache/tools/ant/AntTypeDefinition;->setClassLoader(Ljava/lang/ClassLoader;)V

    invoke-virtual {v0, p2}, Lorg/apache/tools/ant/AntTypeDefinition;->setClass(Ljava/lang/Class;)V

    sget-object v1, Lorg/apache/tools/ant/ComponentHelper;->class$org$apache$tools$ant$TaskAdapter:Ljava/lang/Class;

    if-nez v1, :cond_0

    const-string v1, "org.apache.tools.ant.TaskAdapter"

    invoke-static {v1}, Lorg/apache/tools/ant/ComponentHelper;->class$(Ljava/lang/String;)Ljava/lang/Class;

    move-result-object v1

    sput-object v1, Lorg/apache/tools/ant/ComponentHelper;->class$org$apache$tools$ant$TaskAdapter:Ljava/lang/Class;

    :goto_0
    invoke-virtual {v0, v1}, Lorg/apache/tools/ant/AntTypeDefinition;->setAdapterClass(Ljava/lang/Class;)V

    invoke-virtual {p2}, Ljava/lang/Class;->getName()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Lorg/apache/tools/ant/AntTypeDefinition;->setClassName(Ljava/lang/String;)V

    sget-object v1, Lorg/apache/tools/ant/ComponentHelper;->class$org$apache$tools$ant$Task:Ljava/lang/Class;

    if-nez v1, :cond_1

    const-string v1, "org.apache.tools.ant.Task"

    invoke-static {v1}, Lorg/apache/tools/ant/ComponentHelper;->class$(Ljava/lang/String;)Ljava/lang/Class;

    move-result-object v1

    sput-object v1, Lorg/apache/tools/ant/ComponentHelper;->class$org$apache$tools$ant$Task:Ljava/lang/Class;

    :goto_1
    invoke-virtual {v0, v1}, Lorg/apache/tools/ant/AntTypeDefinition;->setAdaptToClass(Ljava/lang/Class;)V

    invoke-direct {p0, v0}, Lorg/apache/tools/ant/ComponentHelper;->updateDataTypeDefinition(Lorg/apache/tools/ant/AntTypeDefinition;)V

    return-void

    :cond_0
    sget-object v1, Lorg/apache/tools/ant/ComponentHelper;->class$org$apache$tools$ant$TaskAdapter:Ljava/lang/Class;

    goto :goto_0

    :cond_1
    sget-object v1, Lorg/apache/tools/ant/ComponentHelper;->class$org$apache$tools$ant$Task:Ljava/lang/Class;

    goto :goto_1
.end method

.method public checkTaskClass(Ljava/lang/Class;)V
    .locals 5
    .param p1    # Ljava/lang/Class;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lorg/apache/tools/ant/BuildException;
        }
    .end annotation

    const/4 v4, 0x0

    invoke-virtual {p1}, Ljava/lang/Class;->getModifiers()I

    move-result v2

    invoke-static {v2}, Ljava/lang/reflect/Modifier;->isPublic(I)Z

    move-result v2

    if-nez v2, :cond_0

    new-instance v2, Ljava/lang/StringBuffer;

    invoke-direct {v2}, Ljava/lang/StringBuffer;-><init>()V

    invoke-virtual {v2, p1}, Ljava/lang/StringBuffer;->append(Ljava/lang/Object;)Ljava/lang/StringBuffer;

    move-result-object v2

    const-string v3, " is not public"

    invoke-virtual {v2, v3}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuffer;->toString()Ljava/lang/String;

    move-result-object v1

    iget-object v2, p0, Lorg/apache/tools/ant/ComponentHelper;->project:Lorg/apache/tools/ant/Project;

    invoke-virtual {v2, v1, v4}, Lorg/apache/tools/ant/Project;->log(Ljava/lang/String;I)V

    new-instance v2, Lorg/apache/tools/ant/BuildException;

    invoke-direct {v2, v1}, Lorg/apache/tools/ant/BuildException;-><init>(Ljava/lang/String;)V

    throw v2

    :cond_0
    invoke-virtual {p1}, Ljava/lang/Class;->getModifiers()I

    move-result v2

    invoke-static {v2}, Ljava/lang/reflect/Modifier;->isAbstract(I)Z

    move-result v2

    if-eqz v2, :cond_1

    new-instance v2, Ljava/lang/StringBuffer;

    invoke-direct {v2}, Ljava/lang/StringBuffer;-><init>()V

    invoke-virtual {v2, p1}, Ljava/lang/StringBuffer;->append(Ljava/lang/Object;)Ljava/lang/StringBuffer;

    move-result-object v2

    const-string v3, " is abstract"

    invoke-virtual {v2, v3}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuffer;->toString()Ljava/lang/String;

    move-result-object v1

    iget-object v2, p0, Lorg/apache/tools/ant/ComponentHelper;->project:Lorg/apache/tools/ant/Project;

    invoke-virtual {v2, v1, v4}, Lorg/apache/tools/ant/Project;->log(Ljava/lang/String;I)V

    new-instance v2, Lorg/apache/tools/ant/BuildException;

    invoke-direct {v2, v1}, Lorg/apache/tools/ant/BuildException;-><init>(Ljava/lang/String;)V

    throw v2

    :cond_1
    const/4 v2, 0x0

    :try_start_0
    check-cast v2, [Ljava/lang/Class;

    invoke-virtual {p1, v2}, Ljava/lang/Class;->getConstructor([Ljava/lang/Class;)Ljava/lang/reflect/Constructor;
    :try_end_0
    .catch Ljava/lang/NoSuchMethodException; {:try_start_0 .. :try_end_0} :catch_0

    sget-object v2, Lorg/apache/tools/ant/ComponentHelper;->class$org$apache$tools$ant$Task:Ljava/lang/Class;

    if-nez v2, :cond_3

    const-string v2, "org.apache.tools.ant.Task"

    invoke-static {v2}, Lorg/apache/tools/ant/ComponentHelper;->class$(Ljava/lang/String;)Ljava/lang/Class;

    move-result-object v2

    sput-object v2, Lorg/apache/tools/ant/ComponentHelper;->class$org$apache$tools$ant$Task:Ljava/lang/Class;

    :goto_0
    invoke-virtual {v2, p1}, Ljava/lang/Class;->isAssignableFrom(Ljava/lang/Class;)Z

    move-result v2

    if-nez v2, :cond_2

    iget-object v2, p0, Lorg/apache/tools/ant/ComponentHelper;->project:Lorg/apache/tools/ant/Project;

    invoke-static {p1, v2}, Lorg/apache/tools/ant/TaskAdapter;->checkTaskClass(Ljava/lang/Class;Lorg/apache/tools/ant/Project;)V

    :cond_2
    return-void

    :catch_0
    move-exception v0

    new-instance v2, Ljava/lang/StringBuffer;

    invoke-direct {v2}, Ljava/lang/StringBuffer;-><init>()V

    const-string v3, "No public no-arg constructor in "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    move-result-object v2

    invoke-virtual {v2, p1}, Ljava/lang/StringBuffer;->append(Ljava/lang/Object;)Ljava/lang/StringBuffer;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuffer;->toString()Ljava/lang/String;

    move-result-object v1

    iget-object v2, p0, Lorg/apache/tools/ant/ComponentHelper;->project:Lorg/apache/tools/ant/Project;

    invoke-virtual {v2, v1, v4}, Lorg/apache/tools/ant/Project;->log(Ljava/lang/String;I)V

    new-instance v2, Lorg/apache/tools/ant/BuildException;

    invoke-direct {v2, v1}, Lorg/apache/tools/ant/BuildException;-><init>(Ljava/lang/String;)V

    throw v2

    :cond_3
    sget-object v2, Lorg/apache/tools/ant/ComponentHelper;->class$org$apache$tools$ant$Task:Ljava/lang/Class;

    goto :goto_0
.end method

.method public createComponent(Ljava/lang/String;)Ljava/lang/Object;
    .locals 2
    .param p1    # Ljava/lang/String;

    invoke-virtual {p0, p1}, Lorg/apache/tools/ant/ComponentHelper;->getDefinition(Ljava/lang/String;)Lorg/apache/tools/ant/AntTypeDefinition;

    move-result-object v0

    if-nez v0, :cond_0

    const/4 v1, 0x0

    :goto_0
    return-object v1

    :cond_0
    iget-object v1, p0, Lorg/apache/tools/ant/ComponentHelper;->project:Lorg/apache/tools/ant/Project;

    invoke-virtual {v0, v1}, Lorg/apache/tools/ant/AntTypeDefinition;->create(Lorg/apache/tools/ant/Project;)Ljava/lang/Object;

    move-result-object v1

    goto :goto_0
.end method

.method public createComponent(Lorg/apache/tools/ant/UnknownElement;Ljava/lang/String;Ljava/lang/String;)Ljava/lang/Object;
    .locals 3
    .param p1    # Lorg/apache/tools/ant/UnknownElement;
    .param p2    # Ljava/lang/String;
    .param p3    # Ljava/lang/String;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lorg/apache/tools/ant/BuildException;
        }
    .end annotation

    invoke-virtual {p0, p3}, Lorg/apache/tools/ant/ComponentHelper;->createComponent(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v0

    instance-of v2, v0, Lorg/apache/tools/ant/Task;

    if-eqz v2, :cond_0

    move-object v1, v0

    check-cast v1, Lorg/apache/tools/ant/Task;

    invoke-virtual {p1}, Lorg/apache/tools/ant/UnknownElement;->getLocation()Lorg/apache/tools/ant/Location;

    move-result-object v2

    invoke-virtual {v1, v2}, Lorg/apache/tools/ant/Task;->setLocation(Lorg/apache/tools/ant/Location;)V

    invoke-virtual {v1, p3}, Lorg/apache/tools/ant/Task;->setTaskType(Ljava/lang/String;)V

    invoke-virtual {p1}, Lorg/apache/tools/ant/UnknownElement;->getTaskName()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Lorg/apache/tools/ant/Task;->setTaskName(Ljava/lang/String;)V

    invoke-virtual {p1}, Lorg/apache/tools/ant/UnknownElement;->getOwningTarget()Lorg/apache/tools/ant/Target;

    move-result-object v2

    invoke-virtual {v1, v2}, Lorg/apache/tools/ant/Task;->setOwningTarget(Lorg/apache/tools/ant/Target;)V

    invoke-virtual {v1}, Lorg/apache/tools/ant/Task;->init()V

    :cond_0
    return-object v0
.end method

.method public createDataType(Ljava/lang/String;)Ljava/lang/Object;
    .locals 1
    .param p1    # Ljava/lang/String;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lorg/apache/tools/ant/BuildException;
        }
    .end annotation

    invoke-virtual {p0, p1}, Lorg/apache/tools/ant/ComponentHelper;->createComponent(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v0

    return-object v0
.end method

.method public createTask(Ljava/lang/String;)Lorg/apache/tools/ant/Task;
    .locals 3
    .param p1    # Ljava/lang/String;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lorg/apache/tools/ant/BuildException;
        }
    .end annotation

    invoke-direct {p0, p1}, Lorg/apache/tools/ant/ComponentHelper;->createNewTask(Ljava/lang/String;)Lorg/apache/tools/ant/Task;

    move-result-object v0

    if-nez v0, :cond_0

    const-string v1, "property"

    invoke-virtual {p1, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_0

    const-string v2, "property"

    sget-object v1, Lorg/apache/tools/ant/ComponentHelper;->class$org$apache$tools$ant$taskdefs$Property:Ljava/lang/Class;

    if-nez v1, :cond_1

    const-string v1, "org.apache.tools.ant.taskdefs.Property"

    invoke-static {v1}, Lorg/apache/tools/ant/ComponentHelper;->class$(Ljava/lang/String;)Ljava/lang/Class;

    move-result-object v1

    sput-object v1, Lorg/apache/tools/ant/ComponentHelper;->class$org$apache$tools$ant$taskdefs$Property:Ljava/lang/Class;

    :goto_0
    invoke-virtual {p0, v2, v1}, Lorg/apache/tools/ant/ComponentHelper;->addTaskDefinition(Ljava/lang/String;Ljava/lang/Class;)V

    invoke-direct {p0, p1}, Lorg/apache/tools/ant/ComponentHelper;->createNewTask(Ljava/lang/String;)Lorg/apache/tools/ant/Task;

    move-result-object v0

    :cond_0
    return-object v0

    :cond_1
    sget-object v1, Lorg/apache/tools/ant/ComponentHelper;->class$org$apache$tools$ant$taskdefs$Property:Ljava/lang/Class;

    goto :goto_0
.end method

.method public diagnoseCreationFailure(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;
    .locals 28
    .param p1    # Ljava/lang/String;
    .param p2    # Ljava/lang/String;

    new-instance v13, Ljava/io/StringWriter;

    invoke-direct {v13}, Ljava/io/StringWriter;-><init>()V

    new-instance v22, Ljava/io/PrintWriter;

    move-object/from16 v0, v22

    invoke-direct {v0, v13}, Ljava/io/PrintWriter;-><init>(Ljava/io/Writer;)V

    new-instance v26, Ljava/lang/StringBuffer;

    invoke-direct/range {v26 .. v26}, Ljava/lang/StringBuffer;-><init>()V

    const-string v27, "Problem: failed to create "

    invoke-virtual/range {v26 .. v27}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    move-result-object v26

    move-object/from16 v0, v26

    move-object/from16 v1, p2

    invoke-virtual {v0, v1}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    move-result-object v26

    const-string v27, " "

    invoke-virtual/range {v26 .. v27}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    move-result-object v26

    move-object/from16 v0, v26

    move-object/from16 v1, p1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    move-result-object v26

    invoke-virtual/range {v26 .. v26}, Ljava/lang/StringBuffer;->toString()Ljava/lang/String;

    move-result-object v26

    move-object/from16 v0, v22

    move-object/from16 v1, v26

    invoke-virtual {v0, v1}, Ljava/io/PrintWriter;->println(Ljava/lang/String;)V

    const/16 v19, 0x0

    const/16 v17, 0x0

    const/4 v9, 0x0

    const-string v26, "user.home"

    invoke-static/range {v26 .. v26}, Ljava/lang/System;->getProperty(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v15

    new-instance v18, Ljava/io/File;

    sget-object v26, Lorg/apache/tools/ant/launch/Launcher;->USER_LIBDIR:Ljava/lang/String;

    move-object/from16 v0, v18

    move-object/from16 v1, v26

    invoke-direct {v0, v15, v1}, Ljava/io/File;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    const/16 v23, 0x0

    const-string v26, "ant.home"

    invoke-static/range {v26 .. v26}, Ljava/lang/System;->getProperty(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v5

    if-eqz v5, :cond_1

    new-instance v3, Ljava/io/File;

    const-string v26, "lib"

    move-object/from16 v0, v26

    invoke-direct {v3, v5, v0}, Ljava/io/File;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    invoke-virtual {v3}, Ljava/io/File;->getAbsolutePath()Ljava/lang/String;

    move-result-object v2

    :goto_0
    new-instance v11, Ljava/lang/StringBuffer;

    invoke-direct {v11}, Ljava/lang/StringBuffer;-><init>()V

    const-string v25, "        -"

    const-string v26, "        -"

    move-object/from16 v0, v26

    invoke-virtual {v11, v0}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    invoke-virtual {v11, v2}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    const/16 v26, 0xa

    move/from16 v0, v26

    invoke-virtual {v11, v0}, Ljava/lang/StringBuffer;->append(C)Ljava/lang/StringBuffer;

    if-eqz v23, :cond_2

    const-string v26, "        -"

    move-object/from16 v0, v26

    invoke-virtual {v11, v0}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    const-string v26, "the IDE Ant configuration dialogs"

    move-object/from16 v0, v26

    invoke-virtual {v11, v0}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    :goto_1
    invoke-virtual {v11}, Ljava/lang/StringBuffer;->toString()Ljava/lang/String;

    move-result-object v10

    invoke-virtual/range {p0 .. p1}, Lorg/apache/tools/ant/ComponentHelper;->getDefinition(Ljava/lang/String;)Lorg/apache/tools/ant/AntTypeDefinition;

    move-result-object v8

    if-nez v8, :cond_4

    const-string v26, "antlib:"

    move-object/from16 v0, p1

    move-object/from16 v1, v26

    invoke-virtual {v0, v1}, Ljava/lang/String;->indexOf(Ljava/lang/String;)I

    move-result v26

    if-nez v26, :cond_3

    const/16 v16, 0x1

    :goto_2
    const-string v26, "Cause: The name is undefined."

    move-object/from16 v0, v22

    move-object/from16 v1, v26

    invoke-virtual {v0, v1}, Ljava/io/PrintWriter;->println(Ljava/lang/String;)V

    const-string v26, "Action: Check the spelling."

    move-object/from16 v0, v22

    move-object/from16 v1, v26

    invoke-virtual {v0, v1}, Ljava/io/PrintWriter;->println(Ljava/lang/String;)V

    const-string v26, "Action: Check that any custom tasks/types have been declared."

    move-object/from16 v0, v22

    move-object/from16 v1, v26

    invoke-virtual {v0, v1}, Ljava/io/PrintWriter;->println(Ljava/lang/String;)V

    const-string v26, "Action: Check that any <presetdef>/<macrodef> declarations have taken place."

    move-object/from16 v0, v22

    move-object/from16 v1, v26

    invoke-virtual {v0, v1}, Ljava/io/PrintWriter;->println(Ljava/lang/String;)V

    if-eqz v16, :cond_0

    invoke-virtual/range {v22 .. v22}, Ljava/io/PrintWriter;->println()V

    const-string v26, "This appears to be an antlib declaration. "

    move-object/from16 v0, v22

    move-object/from16 v1, v26

    invoke-virtual {v0, v1}, Ljava/io/PrintWriter;->println(Ljava/lang/String;)V

    const-string v26, "Action: Check that the implementing library exists in one of:"

    move-object/from16 v0, v22

    move-object/from16 v1, v26

    invoke-virtual {v0, v1}, Ljava/io/PrintWriter;->println(Ljava/lang/String;)V

    move-object/from16 v0, v22

    invoke-virtual {v0, v10}, Ljava/io/PrintWriter;->println(Ljava/lang/String;)V

    :cond_0
    const/4 v9, 0x1

    :goto_3
    invoke-virtual/range {v22 .. v22}, Ljava/io/PrintWriter;->flush()V

    invoke-virtual/range {v22 .. v22}, Ljava/io/PrintWriter;->close()V

    invoke-virtual {v13}, Ljava/io/StringWriter;->toString()Ljava/lang/String;

    move-result-object v26

    return-object v26

    :cond_1
    const/16 v23, 0x1

    new-instance v26, Ljava/lang/StringBuffer;

    invoke-direct/range {v26 .. v26}, Ljava/lang/StringBuffer;-><init>()V

    const-string v27, "ANT_HOME"

    invoke-virtual/range {v26 .. v27}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    move-result-object v26

    sget-char v27, Ljava/io/File;->separatorChar:C

    invoke-virtual/range {v26 .. v27}, Ljava/lang/StringBuffer;->append(C)Ljava/lang/StringBuffer;

    move-result-object v26

    const-string v27, "lib"

    invoke-virtual/range {v26 .. v27}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    move-result-object v26

    invoke-virtual/range {v26 .. v26}, Ljava/lang/StringBuffer;->toString()Ljava/lang/String;

    move-result-object v2

    goto/16 :goto_0

    :cond_2
    const-string v26, "        -"

    move-object/from16 v0, v26

    invoke-virtual {v11, v0}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    move-object/from16 v0, v18

    invoke-virtual {v11, v0}, Ljava/lang/StringBuffer;->append(Ljava/lang/Object;)Ljava/lang/StringBuffer;

    const/16 v26, 0xa

    move/from16 v0, v26

    invoke-virtual {v11, v0}, Ljava/lang/StringBuffer;->append(C)Ljava/lang/StringBuffer;

    const-string v26, "        -"

    move-object/from16 v0, v26

    invoke-virtual {v11, v0}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    const-string v26, "a directory added on the command line with the -lib argument"

    move-object/from16 v0, v26

    invoke-virtual {v11, v0}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    goto/16 :goto_1

    :cond_3
    const/16 v16, 0x0

    goto/16 :goto_2

    :cond_4
    invoke-virtual {v8}, Lorg/apache/tools/ant/AntTypeDefinition;->getClassName()Ljava/lang/String;

    move-result-object v6

    const-string v26, "org.apache.tools.ant."

    move-object/from16 v0, v26

    invoke-virtual {v6, v0}, Ljava/lang/String;->startsWith(Ljava/lang/String;)Z

    move-result v4

    const-string v26, "org.apache.tools.ant.taskdefs.optional"

    move-object/from16 v0, v26

    invoke-virtual {v6, v0}, Ljava/lang/String;->startsWith(Ljava/lang/String;)Z

    move-result v21

    const-string v26, "org.apache.tools.ant.types.optional"

    move-object/from16 v0, v26

    invoke-virtual {v6, v0}, Ljava/lang/String;->startsWith(Ljava/lang/String;)Z

    move-result v26

    or-int v21, v21, v26

    const/4 v7, 0x0

    :try_start_0
    invoke-virtual {v8}, Lorg/apache/tools/ant/AntTypeDefinition;->innerGetTypeClass()Ljava/lang/Class;
    :try_end_0
    .catch Ljava/lang/ClassNotFoundException; {:try_start_0 .. :try_end_0} :catch_0
    .catch Ljava/lang/NoClassDefFoundError; {:try_start_0 .. :try_end_0} :catch_1

    move-result-object v7

    :goto_4
    if-eqz v7, :cond_5

    :try_start_1
    move-object/from16 v0, p0

    iget-object v0, v0, Lorg/apache/tools/ant/ComponentHelper;->project:Lorg/apache/tools/ant/Project;

    move-object/from16 v26, v0

    move-object/from16 v0, v26

    invoke-virtual {v8, v7, v0}, Lorg/apache/tools/ant/AntTypeDefinition;->innerCreateAndSet(Ljava/lang/Class;Lorg/apache/tools/ant/Project;)Ljava/lang/Object;

    const-string v26, "The component could be instantiated."

    move-object/from16 v0, v22

    move-object/from16 v1, v26

    invoke-virtual {v0, v1}, Ljava/io/PrintWriter;->println(Ljava/lang/String;)V
    :try_end_1
    .catch Ljava/lang/NoSuchMethodException; {:try_start_1 .. :try_end_1} :catch_2
    .catch Ljava/lang/InstantiationException; {:try_start_1 .. :try_end_1} :catch_3
    .catch Ljava/lang/IllegalAccessException; {:try_start_1 .. :try_end_1} :catch_4
    .catch Ljava/lang/reflect/InvocationTargetException; {:try_start_1 .. :try_end_1} :catch_5
    .catch Ljava/lang/NoClassDefFoundError; {:try_start_1 .. :try_end_1} :catch_6

    :cond_5
    :goto_5
    invoke-virtual/range {v22 .. v22}, Ljava/io/PrintWriter;->println()V

    const-string v26, "Do not panic, this is a common problem."

    move-object/from16 v0, v22

    move-object/from16 v1, v26

    invoke-virtual {v0, v1}, Ljava/io/PrintWriter;->println(Ljava/lang/String;)V

    if-eqz v9, :cond_6

    const-string v26, "It may just be a typographical error in the build file or the task/type declaration."

    move-object/from16 v0, v22

    move-object/from16 v1, v26

    invoke-virtual {v0, v1}, Ljava/io/PrintWriter;->println(Ljava/lang/String;)V

    :cond_6
    if-eqz v17, :cond_7

    const-string v26, "The commonest cause is a missing JAR."

    move-object/from16 v0, v22

    move-object/from16 v1, v26

    invoke-virtual {v0, v1}, Ljava/io/PrintWriter;->println(Ljava/lang/String;)V

    :cond_7
    if-eqz v19, :cond_b

    const-string v26, "This is quite a low level problem, which may need consultation with the author of the task."

    move-object/from16 v0, v22

    move-object/from16 v1, v26

    invoke-virtual {v0, v1}, Ljava/io/PrintWriter;->println(Ljava/lang/String;)V

    if-eqz v4, :cond_a

    const-string v26, "This may be the Ant team. Please file a defect or contact the developer team."

    move-object/from16 v0, v22

    move-object/from16 v1, v26

    invoke-virtual {v0, v1}, Ljava/io/PrintWriter;->println(Ljava/lang/String;)V

    goto/16 :goto_3

    :catch_0
    move-exception v12

    new-instance v26, Ljava/lang/StringBuffer;

    invoke-direct/range {v26 .. v26}, Ljava/lang/StringBuffer;-><init>()V

    const-string v27, "Cause: the class "

    invoke-virtual/range {v26 .. v27}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    move-result-object v26

    move-object/from16 v0, v26

    invoke-virtual {v0, v6}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    move-result-object v26

    const-string v27, " was not found."

    invoke-virtual/range {v26 .. v27}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    move-result-object v26

    invoke-virtual/range {v26 .. v26}, Ljava/lang/StringBuffer;->toString()Ljava/lang/String;

    move-result-object v26

    move-object/from16 v0, v22

    move-object/from16 v1, v26

    invoke-virtual {v0, v1}, Ljava/io/PrintWriter;->println(Ljava/lang/String;)V

    const/16 v17, 0x1

    if-eqz v21, :cond_8

    const-string v26, "        This looks like one of Ant\'s optional components."

    move-object/from16 v0, v22

    move-object/from16 v1, v26

    invoke-virtual {v0, v1}, Ljava/io/PrintWriter;->println(Ljava/lang/String;)V

    const-string v26, "Action: Check that the appropriate optional JAR exists in"

    move-object/from16 v0, v22

    move-object/from16 v1, v26

    invoke-virtual {v0, v1}, Ljava/io/PrintWriter;->println(Ljava/lang/String;)V

    move-object/from16 v0, v22

    invoke-virtual {v0, v10}, Ljava/io/PrintWriter;->println(Ljava/lang/String;)V

    goto/16 :goto_4

    :cond_8
    const-string v26, "Action: Check that the component has been correctly declared"

    move-object/from16 v0, v22

    move-object/from16 v1, v26

    invoke-virtual {v0, v1}, Ljava/io/PrintWriter;->println(Ljava/lang/String;)V

    const-string v26, "        and that the implementing JAR is in one of:"

    move-object/from16 v0, v22

    move-object/from16 v1, v26

    invoke-virtual {v0, v1}, Ljava/io/PrintWriter;->println(Ljava/lang/String;)V

    move-object/from16 v0, v22

    invoke-virtual {v0, v10}, Ljava/io/PrintWriter;->println(Ljava/lang/String;)V

    const/4 v9, 0x1

    goto/16 :goto_4

    :catch_1
    move-exception v20

    const/16 v17, 0x1

    new-instance v26, Ljava/lang/StringBuffer;

    invoke-direct/range {v26 .. v26}, Ljava/lang/StringBuffer;-><init>()V

    const-string v27, "Cause: Could not load a dependent class "

    invoke-virtual/range {v26 .. v27}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    move-result-object v26

    invoke-virtual/range {v20 .. v20}, Ljava/lang/NoClassDefFoundError;->getMessage()Ljava/lang/String;

    move-result-object v27

    invoke-virtual/range {v26 .. v27}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    move-result-object v26

    invoke-virtual/range {v26 .. v26}, Ljava/lang/StringBuffer;->toString()Ljava/lang/String;

    move-result-object v26

    move-object/from16 v0, v22

    move-object/from16 v1, v26

    invoke-virtual {v0, v1}, Ljava/io/PrintWriter;->println(Ljava/lang/String;)V

    if-eqz v21, :cond_9

    const-string v26, "       It is not enough to have Ant\'s optional JARs"

    move-object/from16 v0, v22

    move-object/from16 v1, v26

    invoke-virtual {v0, v1}, Ljava/io/PrintWriter;->println(Ljava/lang/String;)V

    const-string v26, "       you need the JAR files that the optional tasks depend upon."

    move-object/from16 v0, v22

    move-object/from16 v1, v26

    invoke-virtual {v0, v1}, Ljava/io/PrintWriter;->println(Ljava/lang/String;)V

    const-string v26, "       Ant\'s optional task dependencies are listed in the manual."

    move-object/from16 v0, v22

    move-object/from16 v1, v26

    invoke-virtual {v0, v1}, Ljava/io/PrintWriter;->println(Ljava/lang/String;)V

    :goto_6
    const-string v26, "Action: Determine what extra JAR files are needed, and place them in one of:"

    move-object/from16 v0, v22

    move-object/from16 v1, v26

    invoke-virtual {v0, v1}, Ljava/io/PrintWriter;->println(Ljava/lang/String;)V

    move-object/from16 v0, v22

    invoke-virtual {v0, v10}, Ljava/io/PrintWriter;->println(Ljava/lang/String;)V

    goto/16 :goto_4

    :cond_9
    const-string v26, "       This class may be in a separate JAR that is not installed."

    move-object/from16 v0, v22

    move-object/from16 v1, v26

    invoke-virtual {v0, v1}, Ljava/io/PrintWriter;->println(Ljava/lang/String;)V

    goto :goto_6

    :catch_2
    move-exception v12

    const/16 v19, 0x1

    new-instance v26, Ljava/lang/StringBuffer;

    invoke-direct/range {v26 .. v26}, Ljava/lang/StringBuffer;-><init>()V

    const-string v27, "Cause: The class "

    invoke-virtual/range {v26 .. v27}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    move-result-object v26

    move-object/from16 v0, v26

    invoke-virtual {v0, v6}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    move-result-object v26

    const-string v27, " has no compatible constructor."

    invoke-virtual/range {v26 .. v27}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    move-result-object v26

    invoke-virtual/range {v26 .. v26}, Ljava/lang/StringBuffer;->toString()Ljava/lang/String;

    move-result-object v26

    move-object/from16 v0, v22

    move-object/from16 v1, v26

    invoke-virtual {v0, v1}, Ljava/io/PrintWriter;->println(Ljava/lang/String;)V

    goto/16 :goto_5

    :catch_3
    move-exception v12

    const/16 v19, 0x1

    new-instance v26, Ljava/lang/StringBuffer;

    invoke-direct/range {v26 .. v26}, Ljava/lang/StringBuffer;-><init>()V

    const-string v27, "Cause: The class "

    invoke-virtual/range {v26 .. v27}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    move-result-object v26

    move-object/from16 v0, v26

    invoke-virtual {v0, v6}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    move-result-object v26

    const-string v27, " is abstract and cannot be instantiated."

    invoke-virtual/range {v26 .. v27}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    move-result-object v26

    invoke-virtual/range {v26 .. v26}, Ljava/lang/StringBuffer;->toString()Ljava/lang/String;

    move-result-object v26

    move-object/from16 v0, v22

    move-object/from16 v1, v26

    invoke-virtual {v0, v1}, Ljava/io/PrintWriter;->println(Ljava/lang/String;)V

    goto/16 :goto_5

    :catch_4
    move-exception v12

    const/16 v19, 0x1

    new-instance v26, Ljava/lang/StringBuffer;

    invoke-direct/range {v26 .. v26}, Ljava/lang/StringBuffer;-><init>()V

    const-string v27, "Cause: The constructor for "

    invoke-virtual/range {v26 .. v27}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    move-result-object v26

    move-object/from16 v0, v26

    invoke-virtual {v0, v6}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    move-result-object v26

    const-string v27, " is private and cannot be invoked."

    invoke-virtual/range {v26 .. v27}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    move-result-object v26

    invoke-virtual/range {v26 .. v26}, Ljava/lang/StringBuffer;->toString()Ljava/lang/String;

    move-result-object v26

    move-object/from16 v0, v22

    move-object/from16 v1, v26

    invoke-virtual {v0, v1}, Ljava/io/PrintWriter;->println(Ljava/lang/String;)V

    goto/16 :goto_5

    :catch_5
    move-exception v14

    const/16 v19, 0x1

    invoke-virtual {v14}, Ljava/lang/reflect/InvocationTargetException;->getTargetException()Ljava/lang/Throwable;

    move-result-object v24

    const-string v26, "Cause: The constructor threw the exception"

    move-object/from16 v0, v22

    move-object/from16 v1, v26

    invoke-virtual {v0, v1}, Ljava/io/PrintWriter;->println(Ljava/lang/String;)V

    invoke-virtual/range {v24 .. v24}, Ljava/lang/Throwable;->toString()Ljava/lang/String;

    move-result-object v26

    move-object/from16 v0, v22

    move-object/from16 v1, v26

    invoke-virtual {v0, v1}, Ljava/io/PrintWriter;->println(Ljava/lang/String;)V

    move-object/from16 v0, v24

    move-object/from16 v1, v22

    invoke-virtual {v0, v1}, Ljava/lang/Throwable;->printStackTrace(Ljava/io/PrintWriter;)V

    goto/16 :goto_5

    :catch_6
    move-exception v20

    const/16 v17, 0x1

    new-instance v26, Ljava/lang/StringBuffer;

    invoke-direct/range {v26 .. v26}, Ljava/lang/StringBuffer;-><init>()V

    const-string v27, "Cause:  A class needed by class "

    invoke-virtual/range {v26 .. v27}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    move-result-object v26

    move-object/from16 v0, v26

    invoke-virtual {v0, v6}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    move-result-object v26

    const-string v27, " cannot be found: "

    invoke-virtual/range {v26 .. v27}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    move-result-object v26

    invoke-virtual/range {v26 .. v26}, Ljava/lang/StringBuffer;->toString()Ljava/lang/String;

    move-result-object v26

    move-object/from16 v0, v22

    move-object/from16 v1, v26

    invoke-virtual {v0, v1}, Ljava/io/PrintWriter;->println(Ljava/lang/String;)V

    new-instance v26, Ljava/lang/StringBuffer;

    invoke-direct/range {v26 .. v26}, Ljava/lang/StringBuffer;-><init>()V

    const-string v27, "       "

    invoke-virtual/range {v26 .. v27}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    move-result-object v26

    invoke-virtual/range {v20 .. v20}, Ljava/lang/NoClassDefFoundError;->getMessage()Ljava/lang/String;

    move-result-object v27

    invoke-virtual/range {v26 .. v27}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    move-result-object v26

    invoke-virtual/range {v26 .. v26}, Ljava/lang/StringBuffer;->toString()Ljava/lang/String;

    move-result-object v26

    move-object/from16 v0, v22

    move-object/from16 v1, v26

    invoke-virtual {v0, v1}, Ljava/io/PrintWriter;->println(Ljava/lang/String;)V

    const-string v26, "Action: Determine what extra JAR files are needed, and place them in:"

    move-object/from16 v0, v22

    move-object/from16 v1, v26

    invoke-virtual {v0, v1}, Ljava/io/PrintWriter;->println(Ljava/lang/String;)V

    move-object/from16 v0, v22

    invoke-virtual {v0, v10}, Ljava/io/PrintWriter;->println(Ljava/lang/String;)V

    goto/16 :goto_5

    :cond_a
    const-string v26, "This does not appear to be a task bundled with Ant."

    move-object/from16 v0, v22

    move-object/from16 v1, v26

    invoke-virtual {v0, v1}, Ljava/io/PrintWriter;->println(Ljava/lang/String;)V

    new-instance v26, Ljava/lang/StringBuffer;

    invoke-direct/range {v26 .. v26}, Ljava/lang/StringBuffer;-><init>()V

    const-string v27, "Please take it up with the supplier of the third-party "

    invoke-virtual/range {v26 .. v27}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    move-result-object v26

    move-object/from16 v0, v26

    move-object/from16 v1, p2

    invoke-virtual {v0, v1}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    move-result-object v26

    const-string v27, "."

    invoke-virtual/range {v26 .. v27}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    move-result-object v26

    invoke-virtual/range {v26 .. v26}, Ljava/lang/StringBuffer;->toString()Ljava/lang/String;

    move-result-object v26

    move-object/from16 v0, v22

    move-object/from16 v1, v26

    invoke-virtual {v0, v1}, Ljava/io/PrintWriter;->println(Ljava/lang/String;)V

    const-string v26, "If you have written it yourself, you probably have a bug to fix."

    move-object/from16 v0, v22

    move-object/from16 v1, v26

    invoke-virtual {v0, v1}, Ljava/io/PrintWriter;->println(Ljava/lang/String;)V

    goto/16 :goto_3

    :cond_b
    invoke-virtual/range {v22 .. v22}, Ljava/io/PrintWriter;->println()V

    const-string v26, "This is not a bug; it is a configuration problem"

    move-object/from16 v0, v22

    move-object/from16 v1, v26

    invoke-virtual {v0, v1}, Ljava/io/PrintWriter;->println(Ljava/lang/String;)V

    goto/16 :goto_3
.end method

.method public enterAntLib(Ljava/lang/String;)V
    .locals 1
    .param p1    # Ljava/lang/String;

    iput-object p1, p0, Lorg/apache/tools/ant/ComponentHelper;->antLibCurrentUri:Ljava/lang/String;

    iget-object v0, p0, Lorg/apache/tools/ant/ComponentHelper;->antLibStack:Ljava/util/Stack;

    invoke-virtual {v0, p1}, Ljava/util/Stack;->push(Ljava/lang/Object;)Ljava/lang/Object;

    return-void
.end method

.method public exitAntLib()V
    .locals 1

    iget-object v0, p0, Lorg/apache/tools/ant/ComponentHelper;->antLibStack:Ljava/util/Stack;

    invoke-virtual {v0}, Ljava/util/Stack;->pop()Ljava/lang/Object;

    iget-object v0, p0, Lorg/apache/tools/ant/ComponentHelper;->antLibStack:Ljava/util/Stack;

    invoke-virtual {v0}, Ljava/util/Stack;->size()I

    move-result v0

    if-nez v0, :cond_0

    const/4 v0, 0x0

    :goto_0
    iput-object v0, p0, Lorg/apache/tools/ant/ComponentHelper;->antLibCurrentUri:Ljava/lang/String;

    return-void

    :cond_0
    iget-object v0, p0, Lorg/apache/tools/ant/ComponentHelper;->antLibStack:Ljava/util/Stack;

    invoke-virtual {v0}, Ljava/util/Stack;->peek()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/String;

    goto :goto_0
.end method

.method public getAntTypeTable()Ljava/util/Hashtable;
    .locals 1

    iget-object v0, p0, Lorg/apache/tools/ant/ComponentHelper;->antTypeTable:Lorg/apache/tools/ant/ComponentHelper$AntTypeTable;

    return-object v0
.end method

.method public getComponentClass(Ljava/lang/String;)Ljava/lang/Class;
    .locals 2
    .param p1    # Ljava/lang/String;

    invoke-virtual {p0, p1}, Lorg/apache/tools/ant/ComponentHelper;->getDefinition(Ljava/lang/String;)Lorg/apache/tools/ant/AntTypeDefinition;

    move-result-object v0

    if-nez v0, :cond_0

    const/4 v1, 0x0

    :goto_0
    return-object v1

    :cond_0
    iget-object v1, p0, Lorg/apache/tools/ant/ComponentHelper;->project:Lorg/apache/tools/ant/Project;

    invoke-virtual {v0, v1}, Lorg/apache/tools/ant/AntTypeDefinition;->getExposedClass(Lorg/apache/tools/ant/Project;)Ljava/lang/Class;

    move-result-object v1

    goto :goto_0
.end method

.method public getCurrentAntlibUri()Ljava/lang/String;
    .locals 1

    iget-object v0, p0, Lorg/apache/tools/ant/ComponentHelper;->antLibCurrentUri:Ljava/lang/String;

    return-object v0
.end method

.method public getDataTypeDefinitions()Ljava/util/Hashtable;
    .locals 7

    iget-object v4, p0, Lorg/apache/tools/ant/ComponentHelper;->typeClassDefinitions:Ljava/util/Hashtable;

    monitor-enter v4

    :try_start_0
    iget-object v5, p0, Lorg/apache/tools/ant/ComponentHelper;->antTypeTable:Lorg/apache/tools/ant/ComponentHelper$AntTypeTable;

    monitor-enter v5
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    :try_start_1
    iget-boolean v3, p0, Lorg/apache/tools/ant/ComponentHelper;->rebuildTypeClassDefinitions:Z

    if-eqz v3, :cond_3

    iget-object v3, p0, Lorg/apache/tools/ant/ComponentHelper;->typeClassDefinitions:Ljava/util/Hashtable;

    invoke-virtual {v3}, Ljava/util/Hashtable;->clear()V

    iget-object v3, p0, Lorg/apache/tools/ant/ComponentHelper;->antTypeTable:Lorg/apache/tools/ant/ComponentHelper$AntTypeTable;

    invoke-virtual {v3}, Lorg/apache/tools/ant/ComponentHelper$AntTypeTable;->keySet()Ljava/util/Set;

    move-result-object v3

    invoke-interface {v3}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :cond_0
    :goto_0
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v3

    if-eqz v3, :cond_2

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Ljava/lang/String;

    iget-object v3, p0, Lorg/apache/tools/ant/ComponentHelper;->antTypeTable:Lorg/apache/tools/ant/ComponentHelper$AntTypeTable;

    invoke-virtual {v3, v2}, Lorg/apache/tools/ant/ComponentHelper$AntTypeTable;->getExposedClass(Ljava/lang/String;)Ljava/lang/Class;

    move-result-object v0

    if-eqz v0, :cond_0

    sget-object v3, Lorg/apache/tools/ant/ComponentHelper;->class$org$apache$tools$ant$Task:Ljava/lang/Class;

    if-nez v3, :cond_1

    const-string v3, "org.apache.tools.ant.Task"

    invoke-static {v3}, Lorg/apache/tools/ant/ComponentHelper;->class$(Ljava/lang/String;)Ljava/lang/Class;

    move-result-object v3

    sput-object v3, Lorg/apache/tools/ant/ComponentHelper;->class$org$apache$tools$ant$Task:Ljava/lang/Class;

    :goto_1
    invoke-virtual {v3, v0}, Ljava/lang/Class;->isAssignableFrom(Ljava/lang/Class;)Z

    move-result v3

    if-nez v3, :cond_0

    iget-object v3, p0, Lorg/apache/tools/ant/ComponentHelper;->typeClassDefinitions:Ljava/util/Hashtable;

    iget-object v6, p0, Lorg/apache/tools/ant/ComponentHelper;->antTypeTable:Lorg/apache/tools/ant/ComponentHelper$AntTypeTable;

    invoke-virtual {v6, v2}, Lorg/apache/tools/ant/ComponentHelper$AntTypeTable;->getTypeClass(Ljava/lang/String;)Ljava/lang/Class;

    move-result-object v6

    invoke-virtual {v3, v2, v6}, Ljava/util/Hashtable;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    goto :goto_0

    :catchall_0
    move-exception v3

    monitor-exit v5
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    :try_start_2
    throw v3

    :catchall_1
    move-exception v3

    monitor-exit v4
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    throw v3

    :cond_1
    :try_start_3
    sget-object v3, Lorg/apache/tools/ant/ComponentHelper;->class$org$apache$tools$ant$Task:Ljava/lang/Class;

    goto :goto_1

    :cond_2
    const/4 v3, 0x0

    iput-boolean v3, p0, Lorg/apache/tools/ant/ComponentHelper;->rebuildTypeClassDefinitions:Z

    :cond_3
    monitor-exit v5
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_0

    :try_start_4
    monitor-exit v4
    :try_end_4
    .catchall {:try_start_4 .. :try_end_4} :catchall_1

    iget-object v3, p0, Lorg/apache/tools/ant/ComponentHelper;->typeClassDefinitions:Ljava/util/Hashtable;

    return-object v3
.end method

.method public getDefinition(Ljava/lang/String;)Lorg/apache/tools/ant/AntTypeDefinition;
    .locals 1
    .param p1    # Ljava/lang/String;

    invoke-direct {p0, p1}, Lorg/apache/tools/ant/ComponentHelper;->checkNamespace(Ljava/lang/String;)V

    iget-object v0, p0, Lorg/apache/tools/ant/ComponentHelper;->antTypeTable:Lorg/apache/tools/ant/ComponentHelper$AntTypeTable;

    invoke-virtual {v0, p1}, Lorg/apache/tools/ant/ComponentHelper$AntTypeTable;->getDefinition(Ljava/lang/String;)Lorg/apache/tools/ant/AntTypeDefinition;

    move-result-object v0

    return-object v0
.end method

.method public getElementName(Ljava/lang/Object;)Ljava/lang/String;
    .locals 1
    .param p1    # Ljava/lang/Object;

    const/4 v0, 0x0

    invoke-virtual {p0, p1, v0}, Lorg/apache/tools/ant/ComponentHelper;->getElementName(Ljava/lang/Object;Z)Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public getElementName(Ljava/lang/Object;Z)Ljava/lang/String;
    .locals 7
    .param p1    # Ljava/lang/Object;
    .param p2    # Z

    invoke-virtual {p1}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/Class;->getName()Ljava/lang/String;

    move-result-object v2

    iget-object v5, p0, Lorg/apache/tools/ant/ComponentHelper;->antTypeTable:Lorg/apache/tools/ant/ComponentHelper$AntTypeTable;

    invoke-virtual {v5}, Lorg/apache/tools/ant/ComponentHelper$AntTypeTable;->values()Ljava/util/Collection;

    move-result-object v5

    invoke-interface {v5}, Ljava/util/Collection;->iterator()Ljava/util/Iterator;

    move-result-object v3

    :cond_0
    invoke-interface {v3}, Ljava/util/Iterator;->hasNext()Z

    move-result v5

    if-eqz v5, :cond_2

    invoke-interface {v3}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lorg/apache/tools/ant/AntTypeDefinition;

    invoke-virtual {v0}, Lorg/apache/tools/ant/AntTypeDefinition;->getClassName()Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v2, v5}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v5

    if-eqz v5, :cond_0

    iget-object v5, p0, Lorg/apache/tools/ant/ComponentHelper;->project:Lorg/apache/tools/ant/Project;

    invoke-virtual {v0, v5}, Lorg/apache/tools/ant/AntTypeDefinition;->getExposedClass(Lorg/apache/tools/ant/Project;)Ljava/lang/Class;

    move-result-object v5

    if-ne v1, v5, :cond_0

    invoke-virtual {v0}, Lorg/apache/tools/ant/AntTypeDefinition;->getName()Ljava/lang/String;

    move-result-object v4

    if-eqz p2, :cond_1

    :goto_0
    return-object v4

    :cond_1
    new-instance v5, Ljava/lang/StringBuffer;

    invoke-direct {v5}, Ljava/lang/StringBuffer;-><init>()V

    const-string v6, "The <"

    invoke-virtual {v5, v6}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    move-result-object v5

    invoke-virtual {v5, v4}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    move-result-object v5

    const-string v6, "> type"

    invoke-virtual {v5, v6}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/StringBuffer;->toString()Ljava/lang/String;

    move-result-object v4

    goto :goto_0

    :cond_2
    invoke-virtual {p1}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v5

    invoke-static {v5, p2}, Lorg/apache/tools/ant/ComponentHelper;->getUnmappedElementName(Ljava/lang/Class;Z)Ljava/lang/String;

    move-result-object v4

    goto :goto_0
.end method

.method public getNext()Lorg/apache/tools/ant/ComponentHelper;
    .locals 1

    iget-object v0, p0, Lorg/apache/tools/ant/ComponentHelper;->next:Lorg/apache/tools/ant/ComponentHelper;

    return-object v0
.end method

.method public getTaskDefinitions()Ljava/util/Hashtable;
    .locals 7

    iget-object v4, p0, Lorg/apache/tools/ant/ComponentHelper;->taskClassDefinitions:Ljava/util/Hashtable;

    monitor-enter v4

    :try_start_0
    iget-object v5, p0, Lorg/apache/tools/ant/ComponentHelper;->antTypeTable:Lorg/apache/tools/ant/ComponentHelper$AntTypeTable;

    monitor-enter v5
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    :try_start_1
    iget-boolean v3, p0, Lorg/apache/tools/ant/ComponentHelper;->rebuildTaskClassDefinitions:Z

    if-eqz v3, :cond_3

    iget-object v3, p0, Lorg/apache/tools/ant/ComponentHelper;->taskClassDefinitions:Ljava/util/Hashtable;

    invoke-virtual {v3}, Ljava/util/Hashtable;->clear()V

    iget-object v3, p0, Lorg/apache/tools/ant/ComponentHelper;->antTypeTable:Lorg/apache/tools/ant/ComponentHelper$AntTypeTable;

    invoke-virtual {v3}, Lorg/apache/tools/ant/ComponentHelper$AntTypeTable;->keySet()Ljava/util/Set;

    move-result-object v3

    invoke-interface {v3}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :cond_0
    :goto_0
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v3

    if-eqz v3, :cond_2

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Ljava/lang/String;

    iget-object v3, p0, Lorg/apache/tools/ant/ComponentHelper;->antTypeTable:Lorg/apache/tools/ant/ComponentHelper$AntTypeTable;

    invoke-virtual {v3, v2}, Lorg/apache/tools/ant/ComponentHelper$AntTypeTable;->getExposedClass(Ljava/lang/String;)Ljava/lang/Class;

    move-result-object v0

    if-eqz v0, :cond_0

    sget-object v3, Lorg/apache/tools/ant/ComponentHelper;->class$org$apache$tools$ant$Task:Ljava/lang/Class;

    if-nez v3, :cond_1

    const-string v3, "org.apache.tools.ant.Task"

    invoke-static {v3}, Lorg/apache/tools/ant/ComponentHelper;->class$(Ljava/lang/String;)Ljava/lang/Class;

    move-result-object v3

    sput-object v3, Lorg/apache/tools/ant/ComponentHelper;->class$org$apache$tools$ant$Task:Ljava/lang/Class;

    :goto_1
    invoke-virtual {v3, v0}, Ljava/lang/Class;->isAssignableFrom(Ljava/lang/Class;)Z

    move-result v3

    if-eqz v3, :cond_0

    iget-object v3, p0, Lorg/apache/tools/ant/ComponentHelper;->taskClassDefinitions:Ljava/util/Hashtable;

    iget-object v6, p0, Lorg/apache/tools/ant/ComponentHelper;->antTypeTable:Lorg/apache/tools/ant/ComponentHelper$AntTypeTable;

    invoke-virtual {v6, v2}, Lorg/apache/tools/ant/ComponentHelper$AntTypeTable;->getTypeClass(Ljava/lang/String;)Ljava/lang/Class;

    move-result-object v6

    invoke-virtual {v3, v2, v6}, Ljava/util/Hashtable;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    goto :goto_0

    :catchall_0
    move-exception v3

    monitor-exit v5
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    :try_start_2
    throw v3

    :catchall_1
    move-exception v3

    monitor-exit v4
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    throw v3

    :cond_1
    :try_start_3
    sget-object v3, Lorg/apache/tools/ant/ComponentHelper;->class$org$apache$tools$ant$Task:Ljava/lang/Class;

    goto :goto_1

    :cond_2
    const/4 v3, 0x0

    iput-boolean v3, p0, Lorg/apache/tools/ant/ComponentHelper;->rebuildTaskClassDefinitions:Z

    :cond_3
    monitor-exit v5
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_0

    :try_start_4
    monitor-exit v4
    :try_end_4
    .catchall {:try_start_4 .. :try_end_4} :catchall_1

    iget-object v3, p0, Lorg/apache/tools/ant/ComponentHelper;->taskClassDefinitions:Ljava/util/Hashtable;

    return-object v3
.end method

.method public initDefaultDefinitions()V
    .locals 0

    invoke-direct {p0}, Lorg/apache/tools/ant/ComponentHelper;->initTasks()V

    invoke-direct {p0}, Lorg/apache/tools/ant/ComponentHelper;->initTypes()V

    return-void
.end method

.method public initSubProject(Lorg/apache/tools/ant/ComponentHelper;)V
    .locals 5
    .param p1    # Lorg/apache/tools/ant/ComponentHelper;

    iget-object v2, p1, Lorg/apache/tools/ant/ComponentHelper;->antTypeTable:Lorg/apache/tools/ant/ComponentHelper$AntTypeTable;

    invoke-virtual {v2}, Lorg/apache/tools/ant/ComponentHelper$AntTypeTable;->values()Ljava/util/Collection;

    move-result-object v3

    invoke-interface {v3}, Ljava/util/Collection;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :goto_0
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v3

    if-eqz v3, :cond_0

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lorg/apache/tools/ant/AntTypeDefinition;

    iget-object v3, p0, Lorg/apache/tools/ant/ComponentHelper;->antTypeTable:Lorg/apache/tools/ant/ComponentHelper$AntTypeTable;

    invoke-virtual {v0}, Lorg/apache/tools/ant/AntTypeDefinition;->getName()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v3, v4, v0}, Lorg/apache/tools/ant/ComponentHelper$AntTypeTable;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    goto :goto_0

    :cond_0
    iget-object v3, p1, Lorg/apache/tools/ant/ComponentHelper;->checkedNamespaces:Ljava/util/Set;

    invoke-interface {v3}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :goto_1
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v3

    if-eqz v3, :cond_1

    iget-object v3, p0, Lorg/apache/tools/ant/ComponentHelper;->checkedNamespaces:Ljava/util/Set;

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v4

    invoke-interface {v3, v4}, Ljava/util/Set;->add(Ljava/lang/Object;)Z

    goto :goto_1

    :cond_1
    return-void
.end method

.method public setNext(Lorg/apache/tools/ant/ComponentHelper;)V
    .locals 0
    .param p1    # Lorg/apache/tools/ant/ComponentHelper;

    iput-object p1, p0, Lorg/apache/tools/ant/ComponentHelper;->next:Lorg/apache/tools/ant/ComponentHelper;

    return-void
.end method

.method public setProject(Lorg/apache/tools/ant/Project;)V
    .locals 1
    .param p1    # Lorg/apache/tools/ant/Project;

    iput-object p1, p0, Lorg/apache/tools/ant/ComponentHelper;->project:Lorg/apache/tools/ant/Project;

    new-instance v0, Lorg/apache/tools/ant/ComponentHelper$AntTypeTable;

    invoke-direct {v0, p1}, Lorg/apache/tools/ant/ComponentHelper$AntTypeTable;-><init>(Lorg/apache/tools/ant/Project;)V

    iput-object v0, p0, Lorg/apache/tools/ant/ComponentHelper;->antTypeTable:Lorg/apache/tools/ant/ComponentHelper$AntTypeTable;

    return-void
.end method
