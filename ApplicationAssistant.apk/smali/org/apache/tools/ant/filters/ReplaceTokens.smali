.class public final Lorg/apache/tools/ant/filters/ReplaceTokens;
.super Lorg/apache/tools/ant/filters/BaseParamFilterReader;
.source "ReplaceTokens.java"

# interfaces
.implements Lorg/apache/tools/ant/filters/ChainableReader;


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lorg/apache/tools/ant/filters/ReplaceTokens$Token;
    }
.end annotation


# static fields
.field private static final DEFAULT_BEGIN_TOKEN:C = '@'

.field private static final DEFAULT_END_TOKEN:C = '@'


# instance fields
.field private beginToken:C

.field private endToken:C

.field private hash:Ljava/util/Hashtable;

.field private queueIndex:I

.field private queuedData:Ljava/lang/String;

.field private replaceData:Ljava/lang/String;

.field private replaceIndex:I


# direct methods
.method public constructor <init>()V
    .locals 3

    const/4 v2, 0x0

    const/16 v1, 0x40

    const/4 v0, -0x1

    invoke-direct {p0}, Lorg/apache/tools/ant/filters/BaseParamFilterReader;-><init>()V

    iput-object v2, p0, Lorg/apache/tools/ant/filters/ReplaceTokens;->queuedData:Ljava/lang/String;

    iput-object v2, p0, Lorg/apache/tools/ant/filters/ReplaceTokens;->replaceData:Ljava/lang/String;

    iput v0, p0, Lorg/apache/tools/ant/filters/ReplaceTokens;->replaceIndex:I

    iput v0, p0, Lorg/apache/tools/ant/filters/ReplaceTokens;->queueIndex:I

    new-instance v0, Ljava/util/Hashtable;

    invoke-direct {v0}, Ljava/util/Hashtable;-><init>()V

    iput-object v0, p0, Lorg/apache/tools/ant/filters/ReplaceTokens;->hash:Ljava/util/Hashtable;

    iput-char v1, p0, Lorg/apache/tools/ant/filters/ReplaceTokens;->beginToken:C

    iput-char v1, p0, Lorg/apache/tools/ant/filters/ReplaceTokens;->endToken:C

    return-void
.end method

.method public constructor <init>(Ljava/io/Reader;)V
    .locals 3
    .param p1    # Ljava/io/Reader;

    const/4 v2, 0x0

    const/16 v1, 0x40

    const/4 v0, -0x1

    invoke-direct {p0, p1}, Lorg/apache/tools/ant/filters/BaseParamFilterReader;-><init>(Ljava/io/Reader;)V

    iput-object v2, p0, Lorg/apache/tools/ant/filters/ReplaceTokens;->queuedData:Ljava/lang/String;

    iput-object v2, p0, Lorg/apache/tools/ant/filters/ReplaceTokens;->replaceData:Ljava/lang/String;

    iput v0, p0, Lorg/apache/tools/ant/filters/ReplaceTokens;->replaceIndex:I

    iput v0, p0, Lorg/apache/tools/ant/filters/ReplaceTokens;->queueIndex:I

    new-instance v0, Ljava/util/Hashtable;

    invoke-direct {v0}, Ljava/util/Hashtable;-><init>()V

    iput-object v0, p0, Lorg/apache/tools/ant/filters/ReplaceTokens;->hash:Ljava/util/Hashtable;

    iput-char v1, p0, Lorg/apache/tools/ant/filters/ReplaceTokens;->beginToken:C

    iput-char v1, p0, Lorg/apache/tools/ant/filters/ReplaceTokens;->endToken:C

    return-void
.end method

.method private getBeginToken()C
    .locals 1

    iget-char v0, p0, Lorg/apache/tools/ant/filters/ReplaceTokens;->beginToken:C

    return v0
.end method

.method private getEndToken()C
    .locals 1

    iget-char v0, p0, Lorg/apache/tools/ant/filters/ReplaceTokens;->endToken:C

    return v0
.end method

.method private getNextChar()I
    .locals 5
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    const/4 v4, -0x1

    iget v1, p0, Lorg/apache/tools/ant/filters/ReplaceTokens;->queueIndex:I

    if-eq v1, v4, :cond_1

    iget-object v1, p0, Lorg/apache/tools/ant/filters/ReplaceTokens;->queuedData:Ljava/lang/String;

    iget v2, p0, Lorg/apache/tools/ant/filters/ReplaceTokens;->queueIndex:I

    add-int/lit8 v3, v2, 0x1

    iput v3, p0, Lorg/apache/tools/ant/filters/ReplaceTokens;->queueIndex:I

    invoke-virtual {v1, v2}, Ljava/lang/String;->charAt(I)C

    move-result v0

    iget v1, p0, Lorg/apache/tools/ant/filters/ReplaceTokens;->queueIndex:I

    iget-object v2, p0, Lorg/apache/tools/ant/filters/ReplaceTokens;->queuedData:Ljava/lang/String;

    invoke-virtual {v2}, Ljava/lang/String;->length()I

    move-result v2

    if-lt v1, v2, :cond_0

    iput v4, p0, Lorg/apache/tools/ant/filters/ReplaceTokens;->queueIndex:I

    :cond_0
    :goto_0
    return v0

    :cond_1
    iget-object v1, p0, Lorg/apache/tools/ant/filters/ReplaceTokens;->in:Ljava/io/Reader;

    invoke-virtual {v1}, Ljava/io/Reader;->read()I

    move-result v0

    goto :goto_0
.end method

.method private getPropertiesFromFile(Ljava/lang/String;)Ljava/util/Properties;
    .locals 5
    .param p1    # Ljava/lang/String;

    const/4 v0, 0x0

    new-instance v3, Ljava/util/Properties;

    invoke-direct {v3}, Ljava/util/Properties;-><init>()V

    :try_start_0
    new-instance v1, Ljava/io/FileInputStream;

    invoke-direct {v1, p1}, Ljava/io/FileInputStream;-><init>(Ljava/lang/String;)V
    :try_end_0
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    :try_start_1
    invoke-virtual {v3, v1}, Ljava/util/Properties;->load(Ljava/io/InputStream;)V
    :try_end_1
    .catch Ljava/io/IOException; {:try_start_1 .. :try_end_1} :catch_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_1

    invoke-static {v1}, Lorg/apache/tools/ant/util/FileUtils;->close(Ljava/io/InputStream;)V

    move-object v0, v1

    :goto_0
    return-object v3

    :catch_0
    move-exception v2

    :goto_1
    :try_start_2
    invoke-virtual {v2}, Ljava/io/IOException;->printStackTrace()V
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    invoke-static {v0}, Lorg/apache/tools/ant/util/FileUtils;->close(Ljava/io/InputStream;)V

    goto :goto_0

    :catchall_0
    move-exception v4

    :goto_2
    invoke-static {v0}, Lorg/apache/tools/ant/util/FileUtils;->close(Ljava/io/InputStream;)V

    throw v4

    :catchall_1
    move-exception v4

    move-object v0, v1

    goto :goto_2

    :catch_1
    move-exception v2

    move-object v0, v1

    goto :goto_1
.end method

.method private getTokens()Ljava/util/Hashtable;
    .locals 1

    iget-object v0, p0, Lorg/apache/tools/ant/filters/ReplaceTokens;->hash:Ljava/util/Hashtable;

    return-object v0
.end method

.method private initialize()V
    .locals 10

    const/4 v9, 0x0

    invoke-virtual {p0}, Lorg/apache/tools/ant/filters/ReplaceTokens;->getParameters()[Lorg/apache/tools/ant/types/Parameter;

    move-result-object v4

    if-eqz v4, :cond_6

    const/4 v1, 0x0

    :goto_0
    array-length v8, v4

    if-ge v1, v8, :cond_6

    aget-object v8, v4, v1

    if-eqz v8, :cond_1

    aget-object v8, v4, v1

    invoke-virtual {v8}, Lorg/apache/tools/ant/types/Parameter;->getType()Ljava/lang/String;

    move-result-object v6

    const-string v8, "tokenchar"

    invoke-virtual {v8, v6}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v8

    if-eqz v8, :cond_4

    aget-object v8, v4, v1

    invoke-virtual {v8}, Lorg/apache/tools/ant/types/Parameter;->getName()Ljava/lang/String;

    move-result-object v3

    aget-object v8, v4, v1

    invoke-virtual {v8}, Lorg/apache/tools/ant/types/Parameter;->getValue()Ljava/lang/String;

    move-result-object v7

    const-string v8, "begintoken"

    invoke-virtual {v8, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v8

    if-eqz v8, :cond_2

    invoke-virtual {v7}, Ljava/lang/String;->length()I

    move-result v8

    if-nez v8, :cond_0

    new-instance v8, Lorg/apache/tools/ant/BuildException;

    const-string v9, "Begin token cannot be empty"

    invoke-direct {v8, v9}, Lorg/apache/tools/ant/BuildException;-><init>(Ljava/lang/String;)V

    throw v8

    :cond_0
    aget-object v8, v4, v1

    invoke-virtual {v8}, Lorg/apache/tools/ant/types/Parameter;->getValue()Ljava/lang/String;

    move-result-object v8

    invoke-virtual {v8, v9}, Ljava/lang/String;->charAt(I)C

    move-result v8

    iput-char v8, p0, Lorg/apache/tools/ant/filters/ReplaceTokens;->beginToken:C

    :cond_1
    :goto_1
    add-int/lit8 v1, v1, 0x1

    goto :goto_0

    :cond_2
    const-string v8, "endtoken"

    invoke-virtual {v8, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v8

    if-eqz v8, :cond_1

    invoke-virtual {v7}, Ljava/lang/String;->length()I

    move-result v8

    if-nez v8, :cond_3

    new-instance v8, Lorg/apache/tools/ant/BuildException;

    const-string v9, "End token cannot be empty"

    invoke-direct {v8, v9}, Lorg/apache/tools/ant/BuildException;-><init>(Ljava/lang/String;)V

    throw v8

    :cond_3
    aget-object v8, v4, v1

    invoke-virtual {v8}, Lorg/apache/tools/ant/types/Parameter;->getValue()Ljava/lang/String;

    move-result-object v8

    invoke-virtual {v8, v9}, Ljava/lang/String;->charAt(I)C

    move-result v8

    iput-char v8, p0, Lorg/apache/tools/ant/filters/ReplaceTokens;->endToken:C

    goto :goto_1

    :cond_4
    const-string v8, "token"

    invoke-virtual {v8, v6}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v8

    if-eqz v8, :cond_5

    aget-object v8, v4, v1

    invoke-virtual {v8}, Lorg/apache/tools/ant/types/Parameter;->getName()Ljava/lang/String;

    move-result-object v3

    aget-object v8, v4, v1

    invoke-virtual {v8}, Lorg/apache/tools/ant/types/Parameter;->getValue()Ljava/lang/String;

    move-result-object v7

    iget-object v8, p0, Lorg/apache/tools/ant/filters/ReplaceTokens;->hash:Ljava/util/Hashtable;

    invoke-virtual {v8, v3, v7}, Ljava/util/Hashtable;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    goto :goto_1

    :cond_5
    const-string v8, "propertiesfile"

    invoke-virtual {v8, v6}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v8

    if-eqz v8, :cond_1

    aget-object v8, v4, v1

    invoke-virtual {v8}, Lorg/apache/tools/ant/types/Parameter;->getValue()Ljava/lang/String;

    move-result-object v8

    invoke-direct {p0, v8}, Lorg/apache/tools/ant/filters/ReplaceTokens;->getPropertiesFromFile(Ljava/lang/String;)Ljava/util/Properties;

    move-result-object v5

    invoke-virtual {v5}, Ljava/util/Properties;->keys()Ljava/util/Enumeration;

    move-result-object v0

    :goto_2
    invoke-interface {v0}, Ljava/util/Enumeration;->hasMoreElements()Z

    move-result v8

    if-eqz v8, :cond_1

    invoke-interface {v0}, Ljava/util/Enumeration;->nextElement()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Ljava/lang/String;

    invoke-virtual {v5, v2}, Ljava/util/Properties;->getProperty(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v7

    iget-object v8, p0, Lorg/apache/tools/ant/filters/ReplaceTokens;->hash:Ljava/util/Hashtable;

    invoke-virtual {v8, v2, v7}, Ljava/util/Hashtable;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    goto :goto_2

    :cond_6
    return-void
.end method

.method private setTokens(Ljava/util/Hashtable;)V
    .locals 0
    .param p1    # Ljava/util/Hashtable;

    iput-object p1, p0, Lorg/apache/tools/ant/filters/ReplaceTokens;->hash:Ljava/util/Hashtable;

    return-void
.end method


# virtual methods
.method public addConfiguredToken(Lorg/apache/tools/ant/filters/ReplaceTokens$Token;)V
    .locals 3
    .param p1    # Lorg/apache/tools/ant/filters/ReplaceTokens$Token;

    iget-object v0, p0, Lorg/apache/tools/ant/filters/ReplaceTokens;->hash:Ljava/util/Hashtable;

    invoke-virtual {p1}, Lorg/apache/tools/ant/filters/ReplaceTokens$Token;->getKey()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {p1}, Lorg/apache/tools/ant/filters/ReplaceTokens$Token;->getValue()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Ljava/util/Hashtable;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    return-void
.end method

.method public chain(Ljava/io/Reader;)Ljava/io/Reader;
    .locals 2
    .param p1    # Ljava/io/Reader;

    new-instance v0, Lorg/apache/tools/ant/filters/ReplaceTokens;

    invoke-direct {v0, p1}, Lorg/apache/tools/ant/filters/ReplaceTokens;-><init>(Ljava/io/Reader;)V

    invoke-direct {p0}, Lorg/apache/tools/ant/filters/ReplaceTokens;->getBeginToken()C

    move-result v1

    invoke-virtual {v0, v1}, Lorg/apache/tools/ant/filters/ReplaceTokens;->setBeginToken(C)V

    invoke-direct {p0}, Lorg/apache/tools/ant/filters/ReplaceTokens;->getEndToken()C

    move-result v1

    invoke-virtual {v0, v1}, Lorg/apache/tools/ant/filters/ReplaceTokens;->setEndToken(C)V

    invoke-direct {p0}, Lorg/apache/tools/ant/filters/ReplaceTokens;->getTokens()Ljava/util/Hashtable;

    move-result-object v1

    invoke-direct {v0, v1}, Lorg/apache/tools/ant/filters/ReplaceTokens;->setTokens(Ljava/util/Hashtable;)V

    const/4 v1, 0x1

    invoke-virtual {v0, v1}, Lorg/apache/tools/ant/filters/ReplaceTokens;->setInitialized(Z)V

    return-object v0
.end method

.method public read()I
    .locals 9
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    const/4 v8, 0x0

    const/4 v7, -0x1

    invoke-virtual {p0}, Lorg/apache/tools/ant/filters/ReplaceTokens;->getInitialized()Z

    move-result v4

    if-nez v4, :cond_0

    invoke-direct {p0}, Lorg/apache/tools/ant/filters/ReplaceTokens;->initialize()V

    const/4 v4, 0x1

    invoke-virtual {p0, v4}, Lorg/apache/tools/ant/filters/ReplaceTokens;->setInitialized(Z)V

    :cond_0
    iget v4, p0, Lorg/apache/tools/ant/filters/ReplaceTokens;->replaceIndex:I

    if-eq v4, v7, :cond_2

    iget-object v4, p0, Lorg/apache/tools/ant/filters/ReplaceTokens;->replaceData:Ljava/lang/String;

    iget v5, p0, Lorg/apache/tools/ant/filters/ReplaceTokens;->replaceIndex:I

    add-int/lit8 v6, v5, 0x1

    iput v6, p0, Lorg/apache/tools/ant/filters/ReplaceTokens;->replaceIndex:I

    invoke-virtual {v4, v5}, Ljava/lang/String;->charAt(I)C

    move-result v0

    iget v4, p0, Lorg/apache/tools/ant/filters/ReplaceTokens;->replaceIndex:I

    iget-object v5, p0, Lorg/apache/tools/ant/filters/ReplaceTokens;->replaceData:Ljava/lang/String;

    invoke-virtual {v5}, Ljava/lang/String;->length()I

    move-result v5

    if-lt v4, v5, :cond_1

    iput v7, p0, Lorg/apache/tools/ant/filters/ReplaceTokens;->replaceIndex:I

    :cond_1
    move v4, v0

    :goto_0
    return v4

    :cond_2
    invoke-direct {p0}, Lorg/apache/tools/ant/filters/ReplaceTokens;->getNextChar()I

    move-result v0

    iget-char v4, p0, Lorg/apache/tools/ant/filters/ReplaceTokens;->beginToken:C

    if-ne v0, v4, :cond_c

    new-instance v1, Ljava/lang/StringBuffer;

    const-string v4, ""

    invoke-direct {v1, v4}, Ljava/lang/StringBuffer;-><init>(Ljava/lang/String;)V

    :cond_3
    invoke-direct {p0}, Lorg/apache/tools/ant/filters/ReplaceTokens;->getNextChar()I

    move-result v0

    if-eq v0, v7, :cond_4

    int-to-char v4, v0

    invoke-virtual {v1, v4}, Ljava/lang/StringBuffer;->append(C)Ljava/lang/StringBuffer;

    iget-char v4, p0, Lorg/apache/tools/ant/filters/ReplaceTokens;->endToken:C

    if-ne v0, v4, :cond_3

    :cond_4
    if-ne v0, v7, :cond_7

    iget-object v4, p0, Lorg/apache/tools/ant/filters/ReplaceTokens;->queuedData:Ljava/lang/String;

    if-eqz v4, :cond_5

    iget v4, p0, Lorg/apache/tools/ant/filters/ReplaceTokens;->queueIndex:I

    if-ne v4, v7, :cond_6

    :cond_5
    invoke-virtual {v1}, Ljava/lang/StringBuffer;->toString()Ljava/lang/String;

    move-result-object v4

    iput-object v4, p0, Lorg/apache/tools/ant/filters/ReplaceTokens;->queuedData:Ljava/lang/String;

    :goto_1
    iput v8, p0, Lorg/apache/tools/ant/filters/ReplaceTokens;->queueIndex:I

    iget-char v4, p0, Lorg/apache/tools/ant/filters/ReplaceTokens;->beginToken:C

    goto :goto_0

    :cond_6
    new-instance v4, Ljava/lang/StringBuffer;

    invoke-direct {v4}, Ljava/lang/StringBuffer;-><init>()V

    invoke-virtual {v1}, Ljava/lang/StringBuffer;->toString()Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v4, v5}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    move-result-object v4

    iget-object v5, p0, Lorg/apache/tools/ant/filters/ReplaceTokens;->queuedData:Ljava/lang/String;

    iget v6, p0, Lorg/apache/tools/ant/filters/ReplaceTokens;->queueIndex:I

    invoke-virtual {v5, v6}, Ljava/lang/String;->substring(I)Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v4, v5}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuffer;->toString()Ljava/lang/String;

    move-result-object v4

    iput-object v4, p0, Lorg/apache/tools/ant/filters/ReplaceTokens;->queuedData:Ljava/lang/String;

    goto :goto_1

    :cond_7
    invoke-virtual {v1}, Ljava/lang/StringBuffer;->length()I

    move-result v4

    add-int/lit8 v4, v4, -0x1

    invoke-virtual {v1, v4}, Ljava/lang/StringBuffer;->setLength(I)V

    iget-object v4, p0, Lorg/apache/tools/ant/filters/ReplaceTokens;->hash:Ljava/util/Hashtable;

    invoke-virtual {v1}, Ljava/lang/StringBuffer;->toString()Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v4, v5}, Ljava/util/Hashtable;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Ljava/lang/String;

    if-eqz v3, :cond_9

    invoke-virtual {v3}, Ljava/lang/String;->length()I

    move-result v4

    if-lez v4, :cond_8

    iput-object v3, p0, Lorg/apache/tools/ant/filters/ReplaceTokens;->replaceData:Ljava/lang/String;

    iput v8, p0, Lorg/apache/tools/ant/filters/ReplaceTokens;->replaceIndex:I

    :cond_8
    invoke-virtual {p0}, Lorg/apache/tools/ant/filters/ReplaceTokens;->read()I

    move-result v4

    goto :goto_0

    :cond_9
    new-instance v4, Ljava/lang/StringBuffer;

    invoke-direct {v4}, Ljava/lang/StringBuffer;-><init>()V

    invoke-virtual {v1}, Ljava/lang/StringBuffer;->toString()Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v4, v5}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    move-result-object v4

    iget-char v5, p0, Lorg/apache/tools/ant/filters/ReplaceTokens;->endToken:C

    invoke-virtual {v4, v5}, Ljava/lang/StringBuffer;->append(C)Ljava/lang/StringBuffer;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuffer;->toString()Ljava/lang/String;

    move-result-object v2

    iget-object v4, p0, Lorg/apache/tools/ant/filters/ReplaceTokens;->queuedData:Ljava/lang/String;

    if-eqz v4, :cond_a

    iget v4, p0, Lorg/apache/tools/ant/filters/ReplaceTokens;->queueIndex:I

    if-ne v4, v7, :cond_b

    :cond_a
    iput-object v2, p0, Lorg/apache/tools/ant/filters/ReplaceTokens;->queuedData:Ljava/lang/String;

    :goto_2
    iput v8, p0, Lorg/apache/tools/ant/filters/ReplaceTokens;->queueIndex:I

    iget-char v4, p0, Lorg/apache/tools/ant/filters/ReplaceTokens;->beginToken:C

    goto/16 :goto_0

    :cond_b
    new-instance v4, Ljava/lang/StringBuffer;

    invoke-direct {v4}, Ljava/lang/StringBuffer;-><init>()V

    invoke-virtual {v4, v2}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    move-result-object v4

    iget-object v5, p0, Lorg/apache/tools/ant/filters/ReplaceTokens;->queuedData:Ljava/lang/String;

    iget v6, p0, Lorg/apache/tools/ant/filters/ReplaceTokens;->queueIndex:I

    invoke-virtual {v5, v6}, Ljava/lang/String;->substring(I)Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v4, v5}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuffer;->toString()Ljava/lang/String;

    move-result-object v4

    iput-object v4, p0, Lorg/apache/tools/ant/filters/ReplaceTokens;->queuedData:Ljava/lang/String;

    goto :goto_2

    :cond_c
    move v4, v0

    goto/16 :goto_0
.end method

.method public setBeginToken(C)V
    .locals 0
    .param p1    # C

    iput-char p1, p0, Lorg/apache/tools/ant/filters/ReplaceTokens;->beginToken:C

    return-void
.end method

.method public setEndToken(C)V
    .locals 0
    .param p1    # C

    iput-char p1, p0, Lorg/apache/tools/ant/filters/ReplaceTokens;->endToken:C

    return-void
.end method
