.class public final Lorg/apache/tools/ant/filters/ExpandProperties;
.super Lorg/apache/tools/ant/filters/BaseFilterReader;
.source "ExpandProperties.java"

# interfaces
.implements Lorg/apache/tools/ant/filters/ChainableReader;


# instance fields
.field private queuedData:Ljava/lang/String;


# direct methods
.method public constructor <init>()V
    .locals 1

    invoke-direct {p0}, Lorg/apache/tools/ant/filters/BaseFilterReader;-><init>()V

    const/4 v0, 0x0

    iput-object v0, p0, Lorg/apache/tools/ant/filters/ExpandProperties;->queuedData:Ljava/lang/String;

    return-void
.end method

.method public constructor <init>(Ljava/io/Reader;)V
    .locals 1
    .param p1    # Ljava/io/Reader;

    invoke-direct {p0, p1}, Lorg/apache/tools/ant/filters/BaseFilterReader;-><init>(Ljava/io/Reader;)V

    const/4 v0, 0x0

    iput-object v0, p0, Lorg/apache/tools/ant/filters/ExpandProperties;->queuedData:Ljava/lang/String;

    return-void
.end method


# virtual methods
.method public chain(Ljava/io/Reader;)Ljava/io/Reader;
    .locals 2
    .param p1    # Ljava/io/Reader;

    new-instance v0, Lorg/apache/tools/ant/filters/ExpandProperties;

    invoke-direct {v0, p1}, Lorg/apache/tools/ant/filters/ExpandProperties;-><init>(Ljava/io/Reader;)V

    invoke-virtual {p0}, Lorg/apache/tools/ant/filters/ExpandProperties;->getProject()Lorg/apache/tools/ant/Project;

    move-result-object v1

    invoke-virtual {v0, v1}, Lorg/apache/tools/ant/filters/ExpandProperties;->setProject(Lorg/apache/tools/ant/Project;)V

    return-object v0
.end method

.method public read()I
    .locals 5
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    const/4 v4, 0x0

    const/4 v0, -0x1

    iget-object v2, p0, Lorg/apache/tools/ant/filters/ExpandProperties;->queuedData:Ljava/lang/String;

    if-eqz v2, :cond_0

    iget-object v2, p0, Lorg/apache/tools/ant/filters/ExpandProperties;->queuedData:Ljava/lang/String;

    invoke-virtual {v2}, Ljava/lang/String;->length()I

    move-result v2

    if-nez v2, :cond_0

    iput-object v4, p0, Lorg/apache/tools/ant/filters/ExpandProperties;->queuedData:Ljava/lang/String;

    :cond_0
    iget-object v2, p0, Lorg/apache/tools/ant/filters/ExpandProperties;->queuedData:Ljava/lang/String;

    if-eqz v2, :cond_2

    iget-object v2, p0, Lorg/apache/tools/ant/filters/ExpandProperties;->queuedData:Ljava/lang/String;

    const/4 v3, 0x0

    invoke-virtual {v2, v3}, Ljava/lang/String;->charAt(I)C

    move-result v0

    iget-object v2, p0, Lorg/apache/tools/ant/filters/ExpandProperties;->queuedData:Ljava/lang/String;

    const/4 v3, 0x1

    invoke-virtual {v2, v3}, Ljava/lang/String;->substring(I)Ljava/lang/String;

    move-result-object v2

    iput-object v2, p0, Lorg/apache/tools/ant/filters/ExpandProperties;->queuedData:Ljava/lang/String;

    iget-object v2, p0, Lorg/apache/tools/ant/filters/ExpandProperties;->queuedData:Ljava/lang/String;

    invoke-virtual {v2}, Ljava/lang/String;->length()I

    move-result v2

    if-nez v2, :cond_1

    iput-object v4, p0, Lorg/apache/tools/ant/filters/ExpandProperties;->queuedData:Ljava/lang/String;

    :cond_1
    :goto_0
    move v2, v0

    :goto_1
    return v2

    :cond_2
    invoke-virtual {p0}, Lorg/apache/tools/ant/filters/ExpandProperties;->readFully()Ljava/lang/String;

    move-result-object v2

    iput-object v2, p0, Lorg/apache/tools/ant/filters/ExpandProperties;->queuedData:Ljava/lang/String;

    iget-object v2, p0, Lorg/apache/tools/ant/filters/ExpandProperties;->queuedData:Ljava/lang/String;

    if-nez v2, :cond_3

    const/4 v0, -0x1

    goto :goto_0

    :cond_3
    invoke-virtual {p0}, Lorg/apache/tools/ant/filters/ExpandProperties;->getProject()Lorg/apache/tools/ant/Project;

    move-result-object v1

    iget-object v2, p0, Lorg/apache/tools/ant/filters/ExpandProperties;->queuedData:Ljava/lang/String;

    invoke-virtual {v1, v2}, Lorg/apache/tools/ant/Project;->replaceProperties(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v2

    iput-object v2, p0, Lorg/apache/tools/ant/filters/ExpandProperties;->queuedData:Ljava/lang/String;

    invoke-virtual {p0}, Lorg/apache/tools/ant/filters/ExpandProperties;->read()I

    move-result v2

    goto :goto_1
.end method
