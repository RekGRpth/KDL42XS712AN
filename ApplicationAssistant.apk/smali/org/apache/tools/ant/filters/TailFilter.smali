.class public final Lorg/apache/tools/ant/filters/TailFilter;
.super Lorg/apache/tools/ant/filters/BaseParamFilterReader;
.source "TailFilter.java"

# interfaces
.implements Lorg/apache/tools/ant/filters/ChainableReader;


# static fields
.field private static final DEFAULT_NUM_LINES:I = 0xa

.field private static final LINES_KEY:Ljava/lang/String; = "lines"

.field private static final SKIP_KEY:Ljava/lang/String; = "skip"


# instance fields
.field private completedReadAhead:Z

.field private line:Ljava/lang/String;

.field private lineList:Ljava/util/LinkedList;

.field private linePos:I

.field private lineTokenizer:Lorg/apache/tools/ant/util/LineTokenizer;

.field private lines:J

.field private skip:J


# direct methods
.method public constructor <init>()V
    .locals 4

    const/4 v3, 0x0

    const/4 v2, 0x0

    invoke-direct {p0}, Lorg/apache/tools/ant/filters/BaseParamFilterReader;-><init>()V

    const-wide/16 v0, 0xa

    iput-wide v0, p0, Lorg/apache/tools/ant/filters/TailFilter;->lines:J

    const-wide/16 v0, 0x0

    iput-wide v0, p0, Lorg/apache/tools/ant/filters/TailFilter;->skip:J

    iput-boolean v2, p0, Lorg/apache/tools/ant/filters/TailFilter;->completedReadAhead:Z

    iput-object v3, p0, Lorg/apache/tools/ant/filters/TailFilter;->lineTokenizer:Lorg/apache/tools/ant/util/LineTokenizer;

    iput-object v3, p0, Lorg/apache/tools/ant/filters/TailFilter;->line:Ljava/lang/String;

    iput v2, p0, Lorg/apache/tools/ant/filters/TailFilter;->linePos:I

    new-instance v0, Ljava/util/LinkedList;

    invoke-direct {v0}, Ljava/util/LinkedList;-><init>()V

    iput-object v0, p0, Lorg/apache/tools/ant/filters/TailFilter;->lineList:Ljava/util/LinkedList;

    return-void
.end method

.method public constructor <init>(Ljava/io/Reader;)V
    .locals 4
    .param p1    # Ljava/io/Reader;

    const/4 v3, 0x0

    const/4 v2, 0x0

    invoke-direct {p0, p1}, Lorg/apache/tools/ant/filters/BaseParamFilterReader;-><init>(Ljava/io/Reader;)V

    const-wide/16 v0, 0xa

    iput-wide v0, p0, Lorg/apache/tools/ant/filters/TailFilter;->lines:J

    const-wide/16 v0, 0x0

    iput-wide v0, p0, Lorg/apache/tools/ant/filters/TailFilter;->skip:J

    iput-boolean v2, p0, Lorg/apache/tools/ant/filters/TailFilter;->completedReadAhead:Z

    iput-object v3, p0, Lorg/apache/tools/ant/filters/TailFilter;->lineTokenizer:Lorg/apache/tools/ant/util/LineTokenizer;

    iput-object v3, p0, Lorg/apache/tools/ant/filters/TailFilter;->line:Ljava/lang/String;

    iput v2, p0, Lorg/apache/tools/ant/filters/TailFilter;->linePos:I

    new-instance v0, Ljava/util/LinkedList;

    invoke-direct {v0}, Ljava/util/LinkedList;-><init>()V

    iput-object v0, p0, Lorg/apache/tools/ant/filters/TailFilter;->lineList:Ljava/util/LinkedList;

    new-instance v0, Lorg/apache/tools/ant/util/LineTokenizer;

    invoke-direct {v0}, Lorg/apache/tools/ant/util/LineTokenizer;-><init>()V

    iput-object v0, p0, Lorg/apache/tools/ant/filters/TailFilter;->lineTokenizer:Lorg/apache/tools/ant/util/LineTokenizer;

    iget-object v0, p0, Lorg/apache/tools/ant/filters/TailFilter;->lineTokenizer:Lorg/apache/tools/ant/util/LineTokenizer;

    const/4 v1, 0x1

    invoke-virtual {v0, v1}, Lorg/apache/tools/ant/util/LineTokenizer;->setIncludeDelims(Z)V

    return-void
.end method

.method private getLines()J
    .locals 2

    iget-wide v0, p0, Lorg/apache/tools/ant/filters/TailFilter;->lines:J

    return-wide v0
.end method

.method private getSkip()J
    .locals 2

    iget-wide v0, p0, Lorg/apache/tools/ant/filters/TailFilter;->skip:J

    return-wide v0
.end method

.method private initialize()V
    .locals 4

    invoke-virtual {p0}, Lorg/apache/tools/ant/filters/TailFilter;->getParameters()[Lorg/apache/tools/ant/types/Parameter;

    move-result-object v1

    if-eqz v1, :cond_2

    const/4 v0, 0x0

    :goto_0
    array-length v2, v1

    if-ge v0, v2, :cond_2

    const-string v2, "lines"

    aget-object v3, v1, v0

    invoke-virtual {v3}, Lorg/apache/tools/ant/types/Parameter;->getName()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_1

    new-instance v2, Ljava/lang/Long;

    aget-object v3, v1, v0

    invoke-virtual {v3}, Lorg/apache/tools/ant/types/Parameter;->getValue()Ljava/lang/String;

    move-result-object v3

    invoke-direct {v2, v3}, Ljava/lang/Long;-><init>(Ljava/lang/String;)V

    invoke-virtual {v2}, Ljava/lang/Long;->longValue()J

    move-result-wide v2

    invoke-virtual {p0, v2, v3}, Lorg/apache/tools/ant/filters/TailFilter;->setLines(J)V

    :cond_0
    :goto_1
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    :cond_1
    const-string v2, "skip"

    aget-object v3, v1, v0

    invoke-virtual {v3}, Lorg/apache/tools/ant/types/Parameter;->getName()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_0

    new-instance v2, Ljava/lang/Long;

    aget-object v3, v1, v0

    invoke-virtual {v3}, Lorg/apache/tools/ant/types/Parameter;->getValue()Ljava/lang/String;

    move-result-object v3

    invoke-direct {v2, v3}, Ljava/lang/Long;-><init>(Ljava/lang/String;)V

    invoke-virtual {v2}, Ljava/lang/Long;->longValue()J

    move-result-wide v2

    iput-wide v2, p0, Lorg/apache/tools/ant/filters/TailFilter;->skip:J

    goto :goto_1

    :cond_2
    return-void
.end method

.method private tailFilter(Ljava/lang/String;)Ljava/lang/String;
    .locals 9
    .param p1    # Ljava/lang/String;

    const-wide/16 v7, -0x1

    const-wide/16 v3, 0x0

    iget-boolean v5, p0, Lorg/apache/tools/ant/filters/TailFilter;->completedReadAhead:Z

    if-nez v5, :cond_5

    if-eqz p1, :cond_3

    iget-object v5, p0, Lorg/apache/tools/ant/filters/TailFilter;->lineList:Ljava/util/LinkedList;

    invoke-virtual {v5, p1}, Ljava/util/LinkedList;->add(Ljava/lang/Object;)Z

    iget-wide v5, p0, Lorg/apache/tools/ant/filters/TailFilter;->lines:J

    cmp-long v5, v5, v7

    if-nez v5, :cond_0

    iget-object v3, p0, Lorg/apache/tools/ant/filters/TailFilter;->lineList:Ljava/util/LinkedList;

    invoke-virtual {v3}, Ljava/util/LinkedList;->size()I

    move-result v3

    int-to-long v3, v3

    iget-wide v5, p0, Lorg/apache/tools/ant/filters/TailFilter;->skip:J

    cmp-long v3, v3, v5

    if-lez v3, :cond_2

    iget-object v3, p0, Lorg/apache/tools/ant/filters/TailFilter;->lineList:Ljava/util/LinkedList;

    invoke-virtual {v3}, Ljava/util/LinkedList;->removeFirst()Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Ljava/lang/String;

    :goto_0
    return-object v3

    :cond_0
    iget-wide v5, p0, Lorg/apache/tools/ant/filters/TailFilter;->lines:J

    iget-wide v7, p0, Lorg/apache/tools/ant/filters/TailFilter;->skip:J

    cmp-long v7, v7, v3

    if-lez v7, :cond_1

    iget-wide v3, p0, Lorg/apache/tools/ant/filters/TailFilter;->skip:J

    :cond_1
    add-long v1, v5, v3

    iget-object v3, p0, Lorg/apache/tools/ant/filters/TailFilter;->lineList:Ljava/util/LinkedList;

    invoke-virtual {v3}, Ljava/util/LinkedList;->size()I

    move-result v3

    int-to-long v3, v3

    cmp-long v3, v1, v3

    if-gez v3, :cond_2

    iget-object v3, p0, Lorg/apache/tools/ant/filters/TailFilter;->lineList:Ljava/util/LinkedList;

    invoke-virtual {v3}, Ljava/util/LinkedList;->removeFirst()Ljava/lang/Object;

    :cond_2
    const-string v3, ""

    goto :goto_0

    :cond_3
    const/4 v5, 0x1

    iput-boolean v5, p0, Lorg/apache/tools/ant/filters/TailFilter;->completedReadAhead:Z

    iget-wide v5, p0, Lorg/apache/tools/ant/filters/TailFilter;->skip:J

    cmp-long v3, v5, v3

    if-lez v3, :cond_4

    const/4 v0, 0x0

    :goto_1
    int-to-long v3, v0

    iget-wide v5, p0, Lorg/apache/tools/ant/filters/TailFilter;->skip:J

    cmp-long v3, v3, v5

    if-gez v3, :cond_4

    iget-object v3, p0, Lorg/apache/tools/ant/filters/TailFilter;->lineList:Ljava/util/LinkedList;

    invoke-virtual {v3}, Ljava/util/LinkedList;->removeLast()Ljava/lang/Object;

    add-int/lit8 v0, v0, 0x1

    goto :goto_1

    :cond_4
    iget-wide v3, p0, Lorg/apache/tools/ant/filters/TailFilter;->lines:J

    cmp-long v3, v3, v7

    if-lez v3, :cond_5

    :goto_2
    iget-object v3, p0, Lorg/apache/tools/ant/filters/TailFilter;->lineList:Ljava/util/LinkedList;

    invoke-virtual {v3}, Ljava/util/LinkedList;->size()I

    move-result v3

    int-to-long v3, v3

    iget-wide v5, p0, Lorg/apache/tools/ant/filters/TailFilter;->lines:J

    cmp-long v3, v3, v5

    if-lez v3, :cond_5

    iget-object v3, p0, Lorg/apache/tools/ant/filters/TailFilter;->lineList:Ljava/util/LinkedList;

    invoke-virtual {v3}, Ljava/util/LinkedList;->removeFirst()Ljava/lang/Object;

    goto :goto_2

    :cond_5
    iget-object v3, p0, Lorg/apache/tools/ant/filters/TailFilter;->lineList:Ljava/util/LinkedList;

    invoke-virtual {v3}, Ljava/util/LinkedList;->size()I

    move-result v3

    if-lez v3, :cond_6

    iget-object v3, p0, Lorg/apache/tools/ant/filters/TailFilter;->lineList:Ljava/util/LinkedList;

    invoke-virtual {v3}, Ljava/util/LinkedList;->removeFirst()Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Ljava/lang/String;

    goto :goto_0

    :cond_6
    const/4 v3, 0x0

    goto :goto_0
.end method


# virtual methods
.method public chain(Ljava/io/Reader;)Ljava/io/Reader;
    .locals 3
    .param p1    # Ljava/io/Reader;

    new-instance v0, Lorg/apache/tools/ant/filters/TailFilter;

    invoke-direct {v0, p1}, Lorg/apache/tools/ant/filters/TailFilter;-><init>(Ljava/io/Reader;)V

    invoke-direct {p0}, Lorg/apache/tools/ant/filters/TailFilter;->getLines()J

    move-result-wide v1

    invoke-virtual {v0, v1, v2}, Lorg/apache/tools/ant/filters/TailFilter;->setLines(J)V

    invoke-direct {p0}, Lorg/apache/tools/ant/filters/TailFilter;->getSkip()J

    move-result-wide v1

    invoke-virtual {v0, v1, v2}, Lorg/apache/tools/ant/filters/TailFilter;->setSkip(J)V

    const/4 v1, 0x1

    invoke-virtual {v0, v1}, Lorg/apache/tools/ant/filters/TailFilter;->setInitialized(Z)V

    return-object v0
.end method

.method public read()I
    .locals 3
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    invoke-virtual {p0}, Lorg/apache/tools/ant/filters/TailFilter;->getInitialized()Z

    move-result v1

    if-nez v1, :cond_0

    invoke-direct {p0}, Lorg/apache/tools/ant/filters/TailFilter;->initialize()V

    const/4 v1, 0x1

    invoke-virtual {p0, v1}, Lorg/apache/tools/ant/filters/TailFilter;->setInitialized(Z)V

    :cond_0
    :goto_0
    iget-object v1, p0, Lorg/apache/tools/ant/filters/TailFilter;->line:Ljava/lang/String;

    if-eqz v1, :cond_1

    iget-object v1, p0, Lorg/apache/tools/ant/filters/TailFilter;->line:Ljava/lang/String;

    invoke-virtual {v1}, Ljava/lang/String;->length()I

    move-result v1

    if-nez v1, :cond_4

    :cond_1
    iget-object v1, p0, Lorg/apache/tools/ant/filters/TailFilter;->lineTokenizer:Lorg/apache/tools/ant/util/LineTokenizer;

    iget-object v2, p0, Lorg/apache/tools/ant/filters/TailFilter;->in:Ljava/io/Reader;

    invoke-virtual {v1, v2}, Lorg/apache/tools/ant/util/LineTokenizer;->getToken(Ljava/io/Reader;)Ljava/lang/String;

    move-result-object v1

    iput-object v1, p0, Lorg/apache/tools/ant/filters/TailFilter;->line:Ljava/lang/String;

    iget-object v1, p0, Lorg/apache/tools/ant/filters/TailFilter;->line:Ljava/lang/String;

    invoke-direct {p0, v1}, Lorg/apache/tools/ant/filters/TailFilter;->tailFilter(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    iput-object v1, p0, Lorg/apache/tools/ant/filters/TailFilter;->line:Ljava/lang/String;

    iget-object v1, p0, Lorg/apache/tools/ant/filters/TailFilter;->line:Ljava/lang/String;

    if-nez v1, :cond_3

    const/4 v0, -0x1

    :cond_2
    :goto_1
    return v0

    :cond_3
    const/4 v1, 0x0

    iput v1, p0, Lorg/apache/tools/ant/filters/TailFilter;->linePos:I

    goto :goto_0

    :cond_4
    iget-object v1, p0, Lorg/apache/tools/ant/filters/TailFilter;->line:Ljava/lang/String;

    iget v2, p0, Lorg/apache/tools/ant/filters/TailFilter;->linePos:I

    invoke-virtual {v1, v2}, Ljava/lang/String;->charAt(I)C

    move-result v0

    iget v1, p0, Lorg/apache/tools/ant/filters/TailFilter;->linePos:I

    add-int/lit8 v1, v1, 0x1

    iput v1, p0, Lorg/apache/tools/ant/filters/TailFilter;->linePos:I

    iget v1, p0, Lorg/apache/tools/ant/filters/TailFilter;->linePos:I

    iget-object v2, p0, Lorg/apache/tools/ant/filters/TailFilter;->line:Ljava/lang/String;

    invoke-virtual {v2}, Ljava/lang/String;->length()I

    move-result v2

    if-ne v1, v2, :cond_2

    const/4 v1, 0x0

    iput-object v1, p0, Lorg/apache/tools/ant/filters/TailFilter;->line:Ljava/lang/String;

    goto :goto_1
.end method

.method public setLines(J)V
    .locals 0
    .param p1    # J

    iput-wide p1, p0, Lorg/apache/tools/ant/filters/TailFilter;->lines:J

    return-void
.end method

.method public setSkip(J)V
    .locals 0
    .param p1    # J

    iput-wide p1, p0, Lorg/apache/tools/ant/filters/TailFilter;->skip:J

    return-void
.end method
