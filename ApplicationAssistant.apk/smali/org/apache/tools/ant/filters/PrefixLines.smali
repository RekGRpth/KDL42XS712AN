.class public final Lorg/apache/tools/ant/filters/PrefixLines;
.super Lorg/apache/tools/ant/filters/BaseParamFilterReader;
.source "PrefixLines.java"

# interfaces
.implements Lorg/apache/tools/ant/filters/ChainableReader;


# static fields
.field private static final PREFIX_KEY:Ljava/lang/String; = "prefix"


# instance fields
.field private prefix:Ljava/lang/String;

.field private queuedData:Ljava/lang/String;


# direct methods
.method public constructor <init>()V
    .locals 1

    const/4 v0, 0x0

    invoke-direct {p0}, Lorg/apache/tools/ant/filters/BaseParamFilterReader;-><init>()V

    iput-object v0, p0, Lorg/apache/tools/ant/filters/PrefixLines;->prefix:Ljava/lang/String;

    iput-object v0, p0, Lorg/apache/tools/ant/filters/PrefixLines;->queuedData:Ljava/lang/String;

    return-void
.end method

.method public constructor <init>(Ljava/io/Reader;)V
    .locals 1
    .param p1    # Ljava/io/Reader;

    const/4 v0, 0x0

    invoke-direct {p0, p1}, Lorg/apache/tools/ant/filters/BaseParamFilterReader;-><init>(Ljava/io/Reader;)V

    iput-object v0, p0, Lorg/apache/tools/ant/filters/PrefixLines;->prefix:Ljava/lang/String;

    iput-object v0, p0, Lorg/apache/tools/ant/filters/PrefixLines;->queuedData:Ljava/lang/String;

    return-void
.end method

.method private getPrefix()Ljava/lang/String;
    .locals 1

    iget-object v0, p0, Lorg/apache/tools/ant/filters/PrefixLines;->prefix:Ljava/lang/String;

    return-object v0
.end method

.method private initialize()V
    .locals 4

    invoke-virtual {p0}, Lorg/apache/tools/ant/filters/PrefixLines;->getParameters()[Lorg/apache/tools/ant/types/Parameter;

    move-result-object v1

    if-eqz v1, :cond_0

    const/4 v0, 0x0

    :goto_0
    array-length v2, v1

    if-ge v0, v2, :cond_0

    const-string v2, "prefix"

    aget-object v3, v1, v0

    invoke-virtual {v3}, Lorg/apache/tools/ant/types/Parameter;->getName()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_1

    aget-object v2, v1, v0

    invoke-virtual {v2}, Lorg/apache/tools/ant/types/Parameter;->getValue()Ljava/lang/String;

    move-result-object v2

    iput-object v2, p0, Lorg/apache/tools/ant/filters/PrefixLines;->prefix:Ljava/lang/String;

    :cond_0
    return-void

    :cond_1
    add-int/lit8 v0, v0, 0x1

    goto :goto_0
.end method


# virtual methods
.method public chain(Ljava/io/Reader;)Ljava/io/Reader;
    .locals 2
    .param p1    # Ljava/io/Reader;

    new-instance v0, Lorg/apache/tools/ant/filters/PrefixLines;

    invoke-direct {v0, p1}, Lorg/apache/tools/ant/filters/PrefixLines;-><init>(Ljava/io/Reader;)V

    invoke-direct {p0}, Lorg/apache/tools/ant/filters/PrefixLines;->getPrefix()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Lorg/apache/tools/ant/filters/PrefixLines;->setPrefix(Ljava/lang/String;)V

    const/4 v1, 0x1

    invoke-virtual {v0, v1}, Lorg/apache/tools/ant/filters/PrefixLines;->setInitialized(Z)V

    return-object v0
.end method

.method public read()I
    .locals 5
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    const/4 v4, 0x0

    const/4 v3, 0x1

    invoke-virtual {p0}, Lorg/apache/tools/ant/filters/PrefixLines;->getInitialized()Z

    move-result v1

    if-nez v1, :cond_0

    invoke-direct {p0}, Lorg/apache/tools/ant/filters/PrefixLines;->initialize()V

    invoke-virtual {p0, v3}, Lorg/apache/tools/ant/filters/PrefixLines;->setInitialized(Z)V

    :cond_0
    const/4 v0, -0x1

    iget-object v1, p0, Lorg/apache/tools/ant/filters/PrefixLines;->queuedData:Ljava/lang/String;

    if-eqz v1, :cond_1

    iget-object v1, p0, Lorg/apache/tools/ant/filters/PrefixLines;->queuedData:Ljava/lang/String;

    invoke-virtual {v1}, Ljava/lang/String;->length()I

    move-result v1

    if-nez v1, :cond_1

    iput-object v4, p0, Lorg/apache/tools/ant/filters/PrefixLines;->queuedData:Ljava/lang/String;

    :cond_1
    iget-object v1, p0, Lorg/apache/tools/ant/filters/PrefixLines;->queuedData:Ljava/lang/String;

    if-eqz v1, :cond_3

    iget-object v1, p0, Lorg/apache/tools/ant/filters/PrefixLines;->queuedData:Ljava/lang/String;

    const/4 v2, 0x0

    invoke-virtual {v1, v2}, Ljava/lang/String;->charAt(I)C

    move-result v0

    iget-object v1, p0, Lorg/apache/tools/ant/filters/PrefixLines;->queuedData:Ljava/lang/String;

    invoke-virtual {v1, v3}, Ljava/lang/String;->substring(I)Ljava/lang/String;

    move-result-object v1

    iput-object v1, p0, Lorg/apache/tools/ant/filters/PrefixLines;->queuedData:Ljava/lang/String;

    iget-object v1, p0, Lorg/apache/tools/ant/filters/PrefixLines;->queuedData:Ljava/lang/String;

    invoke-virtual {v1}, Ljava/lang/String;->length()I

    move-result v1

    if-nez v1, :cond_2

    iput-object v4, p0, Lorg/apache/tools/ant/filters/PrefixLines;->queuedData:Ljava/lang/String;

    :cond_2
    :goto_0
    move v1, v0

    :goto_1
    return v1

    :cond_3
    invoke-virtual {p0}, Lorg/apache/tools/ant/filters/PrefixLines;->readLine()Ljava/lang/String;

    move-result-object v1

    iput-object v1, p0, Lorg/apache/tools/ant/filters/PrefixLines;->queuedData:Ljava/lang/String;

    iget-object v1, p0, Lorg/apache/tools/ant/filters/PrefixLines;->queuedData:Ljava/lang/String;

    if-nez v1, :cond_4

    const/4 v0, -0x1

    goto :goto_0

    :cond_4
    iget-object v1, p0, Lorg/apache/tools/ant/filters/PrefixLines;->prefix:Ljava/lang/String;

    if-eqz v1, :cond_5

    new-instance v1, Ljava/lang/StringBuffer;

    invoke-direct {v1}, Ljava/lang/StringBuffer;-><init>()V

    iget-object v2, p0, Lorg/apache/tools/ant/filters/PrefixLines;->prefix:Ljava/lang/String;

    invoke-virtual {v1, v2}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    move-result-object v1

    iget-object v2, p0, Lorg/apache/tools/ant/filters/PrefixLines;->queuedData:Ljava/lang/String;

    invoke-virtual {v1, v2}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuffer;->toString()Ljava/lang/String;

    move-result-object v1

    iput-object v1, p0, Lorg/apache/tools/ant/filters/PrefixLines;->queuedData:Ljava/lang/String;

    :cond_5
    invoke-virtual {p0}, Lorg/apache/tools/ant/filters/PrefixLines;->read()I

    move-result v1

    goto :goto_1
.end method

.method public setPrefix(Ljava/lang/String;)V
    .locals 0
    .param p1    # Ljava/lang/String;

    iput-object p1, p0, Lorg/apache/tools/ant/filters/PrefixLines;->prefix:Ljava/lang/String;

    return-void
.end method
