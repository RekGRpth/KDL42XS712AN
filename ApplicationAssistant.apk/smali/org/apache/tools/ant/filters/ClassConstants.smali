.class public final Lorg/apache/tools/ant/filters/ClassConstants;
.super Lorg/apache/tools/ant/filters/BaseFilterReader;
.source "ClassConstants.java"

# interfaces
.implements Lorg/apache/tools/ant/filters/ChainableReader;


# static fields
.field private static final JAVA_CLASS_HELPER:Ljava/lang/String; = "org.apache.tools.ant.filters.util.JavaClassHelper"

.field static array$B:Ljava/lang/Class;


# instance fields
.field private queuedData:Ljava/lang/String;


# direct methods
.method public constructor <init>()V
    .locals 1

    invoke-direct {p0}, Lorg/apache/tools/ant/filters/BaseFilterReader;-><init>()V

    const/4 v0, 0x0

    iput-object v0, p0, Lorg/apache/tools/ant/filters/ClassConstants;->queuedData:Ljava/lang/String;

    return-void
.end method

.method public constructor <init>(Ljava/io/Reader;)V
    .locals 1
    .param p1    # Ljava/io/Reader;

    invoke-direct {p0, p1}, Lorg/apache/tools/ant/filters/BaseFilterReader;-><init>(Ljava/io/Reader;)V

    const/4 v0, 0x0

    iput-object v0, p0, Lorg/apache/tools/ant/filters/ClassConstants;->queuedData:Ljava/lang/String;

    return-void
.end method

.method static class$(Ljava/lang/String;)Ljava/lang/Class;
    .locals 3
    .param p0    # Ljava/lang/String;

    :try_start_0
    invoke-static {p0}, Ljava/lang/Class;->forName(Ljava/lang/String;)Ljava/lang/Class;
    :try_end_0
    .catch Ljava/lang/ClassNotFoundException; {:try_start_0 .. :try_end_0} :catch_0

    move-result-object v1

    return-object v1

    :catch_0
    move-exception v0

    new-instance v1, Ljava/lang/NoClassDefFoundError;

    invoke-virtual {v0}, Ljava/lang/ClassNotFoundException;->getMessage()Ljava/lang/String;

    move-result-object v2

    invoke-direct {v1, v2}, Ljava/lang/NoClassDefFoundError;-><init>(Ljava/lang/String;)V

    throw v1
.end method


# virtual methods
.method public chain(Ljava/io/Reader;)Ljava/io/Reader;
    .locals 1
    .param p1    # Ljava/io/Reader;

    new-instance v0, Lorg/apache/tools/ant/filters/ClassConstants;

    invoke-direct {v0, p1}, Lorg/apache/tools/ant/filters/ClassConstants;-><init>(Ljava/io/Reader;)V

    return-object v0
.end method

.method public read()I
    .locals 14
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    const/4 v13, 0x0

    const/4 v12, 0x1

    const/4 v11, 0x0

    const/4 v2, -0x1

    iget-object v10, p0, Lorg/apache/tools/ant/filters/ClassConstants;->queuedData:Ljava/lang/String;

    if-eqz v10, :cond_0

    iget-object v10, p0, Lorg/apache/tools/ant/filters/ClassConstants;->queuedData:Ljava/lang/String;

    invoke-virtual {v10}, Ljava/lang/String;->length()I

    move-result v10

    if-nez v10, :cond_0

    iput-object v13, p0, Lorg/apache/tools/ant/filters/ClassConstants;->queuedData:Ljava/lang/String;

    :cond_0
    iget-object v10, p0, Lorg/apache/tools/ant/filters/ClassConstants;->queuedData:Ljava/lang/String;

    if-eqz v10, :cond_2

    iget-object v10, p0, Lorg/apache/tools/ant/filters/ClassConstants;->queuedData:Ljava/lang/String;

    invoke-virtual {v10, v11}, Ljava/lang/String;->charAt(I)C

    move-result v2

    iget-object v10, p0, Lorg/apache/tools/ant/filters/ClassConstants;->queuedData:Ljava/lang/String;

    invoke-virtual {v10, v12}, Ljava/lang/String;->substring(I)Ljava/lang/String;

    move-result-object v10

    iput-object v10, p0, Lorg/apache/tools/ant/filters/ClassConstants;->queuedData:Ljava/lang/String;

    iget-object v10, p0, Lorg/apache/tools/ant/filters/ClassConstants;->queuedData:Ljava/lang/String;

    invoke-virtual {v10}, Ljava/lang/String;->length()I

    move-result v10

    if-nez v10, :cond_1

    iput-object v13, p0, Lorg/apache/tools/ant/filters/ClassConstants;->queuedData:Ljava/lang/String;

    :cond_1
    :goto_0
    move v10, v2

    :goto_1
    return v10

    :cond_2
    invoke-virtual {p0}, Lorg/apache/tools/ant/filters/ClassConstants;->readFully()Ljava/lang/String;

    move-result-object v3

    if-nez v3, :cond_3

    const/4 v2, -0x1

    goto :goto_0

    :cond_3
    const-string v10, "ISO-8859-1"

    invoke-virtual {v3, v10}, Ljava/lang/String;->getBytes(Ljava/lang/String;)[B

    move-result-object v1

    :try_start_0
    const-string v10, "org.apache.tools.ant.filters.util.JavaClassHelper"

    invoke-static {v10}, Ljava/lang/Class;->forName(Ljava/lang/String;)Ljava/lang/Class;

    move-result-object v6

    if-eqz v6, :cond_1

    const/4 v10, 0x1

    new-array v7, v10, [Ljava/lang/Class;

    const/4 v11, 0x0

    sget-object v10, Lorg/apache/tools/ant/filters/ClassConstants;->array$B:Ljava/lang/Class;

    if-nez v10, :cond_4

    const-string v10, "[B"

    invoke-static {v10}, Lorg/apache/tools/ant/filters/ClassConstants;->class$(Ljava/lang/String;)Ljava/lang/Class;

    move-result-object v10

    sput-object v10, Lorg/apache/tools/ant/filters/ClassConstants;->array$B:Ljava/lang/Class;

    :goto_2
    aput-object v10, v7, v11

    const-string v10, "getConstants"

    invoke-virtual {v6, v10, v7}, Ljava/lang/Class;->getMethod(Ljava/lang/String;[Ljava/lang/Class;)Ljava/lang/reflect/Method;

    move-result-object v5

    const/4 v10, 0x1

    new-array v0, v10, [Ljava/lang/Object;

    const/4 v10, 0x0

    aput-object v1, v0, v10

    const/4 v10, 0x0

    invoke-virtual {v5, v10, v0}, Ljava/lang/reflect/Method;->invoke(Ljava/lang/Object;[Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v8

    check-cast v8, Ljava/lang/StringBuffer;

    invoke-virtual {v8}, Ljava/lang/StringBuffer;->length()I

    move-result v10

    if-lez v10, :cond_1

    invoke-virtual {v8}, Ljava/lang/StringBuffer;->toString()Ljava/lang/String;

    move-result-object v10

    iput-object v10, p0, Lorg/apache/tools/ant/filters/ClassConstants;->queuedData:Ljava/lang/String;

    invoke-virtual {p0}, Lorg/apache/tools/ant/filters/ClassConstants;->read()I

    move-result v10

    goto :goto_1

    :cond_4
    sget-object v10, Lorg/apache/tools/ant/filters/ClassConstants;->array$B:Ljava/lang/Class;
    :try_end_0
    .catch Ljava/lang/NoClassDefFoundError; {:try_start_0 .. :try_end_0} :catch_0
    .catch Ljava/lang/RuntimeException; {:try_start_0 .. :try_end_0} :catch_1
    .catch Ljava/lang/reflect/InvocationTargetException; {:try_start_0 .. :try_end_0} :catch_2
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_3

    goto :goto_2

    :catch_0
    move-exception v4

    throw v4

    :catch_1
    move-exception v4

    throw v4

    :catch_2
    move-exception v4

    invoke-virtual {v4}, Ljava/lang/reflect/InvocationTargetException;->getTargetException()Ljava/lang/Throwable;

    move-result-object v9

    instance-of v10, v9, Ljava/lang/NoClassDefFoundError;

    if-eqz v10, :cond_5

    check-cast v9, Ljava/lang/NoClassDefFoundError;

    throw v9

    :cond_5
    instance-of v10, v9, Ljava/lang/RuntimeException;

    if-eqz v10, :cond_6

    check-cast v9, Ljava/lang/RuntimeException;

    throw v9

    :cond_6
    new-instance v10, Lorg/apache/tools/ant/BuildException;

    invoke-direct {v10, v9}, Lorg/apache/tools/ant/BuildException;-><init>(Ljava/lang/Throwable;)V

    throw v10

    :catch_3
    move-exception v4

    new-instance v10, Lorg/apache/tools/ant/BuildException;

    invoke-direct {v10, v4}, Lorg/apache/tools/ant/BuildException;-><init>(Ljava/lang/Throwable;)V

    throw v10
.end method
