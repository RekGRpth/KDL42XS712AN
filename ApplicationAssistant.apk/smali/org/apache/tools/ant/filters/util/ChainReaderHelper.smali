.class public final Lorg/apache/tools/ant/filters/util/ChainReaderHelper;
.super Ljava/lang/Object;
.source "ChainReaderHelper.java"


# static fields
.field private static final DEFAULT_BUFFER_SIZE:I = 0x2000

.field static class$java$io$FilterReader:Ljava/lang/Class;

.field static class$java$io$Reader:Ljava/lang/Class;

.field static class$org$apache$tools$ant$types$Parameterizable:Ljava/lang/Class;


# instance fields
.field public bufferSize:I

.field public filterChains:Ljava/util/Vector;

.field public primaryReader:Ljava/io/Reader;

.field private project:Lorg/apache/tools/ant/Project;


# direct methods
.method public constructor <init>()V
    .locals 1

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    const/16 v0, 0x2000

    iput v0, p0, Lorg/apache/tools/ant/filters/util/ChainReaderHelper;->bufferSize:I

    new-instance v0, Ljava/util/Vector;

    invoke-direct {v0}, Ljava/util/Vector;-><init>()V

    iput-object v0, p0, Lorg/apache/tools/ant/filters/util/ChainReaderHelper;->filterChains:Ljava/util/Vector;

    const/4 v0, 0x0

    iput-object v0, p0, Lorg/apache/tools/ant/filters/util/ChainReaderHelper;->project:Lorg/apache/tools/ant/Project;

    return-void
.end method

.method static class$(Ljava/lang/String;)Ljava/lang/Class;
    .locals 3
    .param p0    # Ljava/lang/String;

    :try_start_0
    invoke-static {p0}, Ljava/lang/Class;->forName(Ljava/lang/String;)Ljava/lang/Class;
    :try_end_0
    .catch Ljava/lang/ClassNotFoundException; {:try_start_0 .. :try_end_0} :catch_0

    move-result-object v1

    return-object v1

    :catch_0
    move-exception v0

    new-instance v1, Ljava/lang/NoClassDefFoundError;

    invoke-virtual {v0}, Ljava/lang/ClassNotFoundException;->getMessage()Ljava/lang/String;

    move-result-object v2

    invoke-direct {v1, v2}, Ljava/lang/NoClassDefFoundError;-><init>(Ljava/lang/String;)V

    throw v1
.end method

.method private setProjectOnObject(Ljava/lang/Object;)V
    .locals 1
    .param p1    # Ljava/lang/Object;

    iget-object v0, p0, Lorg/apache/tools/ant/filters/util/ChainReaderHelper;->project:Lorg/apache/tools/ant/Project;

    if-nez v0, :cond_0

    :goto_0
    return-void

    :cond_0
    instance-of v0, p1, Lorg/apache/tools/ant/filters/BaseFilterReader;

    if-eqz v0, :cond_1

    check-cast p1, Lorg/apache/tools/ant/filters/BaseFilterReader;

    iget-object v0, p0, Lorg/apache/tools/ant/filters/util/ChainReaderHelper;->project:Lorg/apache/tools/ant/Project;

    invoke-virtual {p1, v0}, Lorg/apache/tools/ant/filters/BaseFilterReader;->setProject(Lorg/apache/tools/ant/Project;)V

    goto :goto_0

    :cond_1
    iget-object v0, p0, Lorg/apache/tools/ant/filters/util/ChainReaderHelper;->project:Lorg/apache/tools/ant/Project;

    invoke-virtual {v0, p1}, Lorg/apache/tools/ant/Project;->setProjectReference(Ljava/lang/Object;)V

    goto :goto_0
.end method


# virtual methods
.method public getAssembledReader()Ljava/io/Reader;
    .locals 30
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lorg/apache/tools/ant/BuildException;
        }
    .end annotation

    move-object/from16 v0, p0

    iget-object v0, v0, Lorg/apache/tools/ant/filters/util/ChainReaderHelper;->primaryReader:Ljava/io/Reader;

    move-object/from16 v27, v0

    if-nez v27, :cond_0

    new-instance v27, Lorg/apache/tools/ant/BuildException;

    const-string v28, "primaryReader must not be null."

    invoke-direct/range {v27 .. v28}, Lorg/apache/tools/ant/BuildException;-><init>(Ljava/lang/String;)V

    throw v27

    :cond_0
    move-object/from16 v0, p0

    iget-object v0, v0, Lorg/apache/tools/ant/filters/util/ChainReaderHelper;->primaryReader:Ljava/io/Reader;

    move-object/from16 v18, v0

    move-object/from16 v0, p0

    iget-object v0, v0, Lorg/apache/tools/ant/filters/util/ChainReaderHelper;->filterChains:Ljava/util/Vector;

    move-object/from16 v27, v0

    invoke-virtual/range {v27 .. v27}, Ljava/util/Vector;->size()I

    move-result v11

    new-instance v14, Ljava/util/Vector;

    invoke-direct {v14}, Ljava/util/Vector;-><init>()V

    const/4 v15, 0x0

    :goto_0
    if-ge v15, v11, :cond_2

    move-object/from16 v0, p0

    iget-object v0, v0, Lorg/apache/tools/ant/filters/util/ChainReaderHelper;->filterChains:Ljava/util/Vector;

    move-object/from16 v27, v0

    move-object/from16 v0, v27

    invoke-virtual {v0, v15}, Ljava/util/Vector;->elementAt(I)Ljava/lang/Object;

    move-result-object v12

    check-cast v12, Lorg/apache/tools/ant/types/FilterChain;

    invoke-virtual {v12}, Lorg/apache/tools/ant/types/FilterChain;->getFilterReaders()Ljava/util/Vector;

    move-result-object v10

    invoke-virtual {v10}, Ljava/util/Vector;->size()I

    move-result v25

    const/16 v20, 0x0

    :goto_1
    move/from16 v0, v20

    move/from16 v1, v25

    if-ge v0, v1, :cond_1

    move/from16 v0, v20

    invoke-virtual {v10, v0}, Ljava/util/Vector;->elementAt(I)Ljava/lang/Object;

    move-result-object v27

    move-object/from16 v0, v27

    invoke-virtual {v14, v0}, Ljava/util/Vector;->addElement(Ljava/lang/Object;)V

    add-int/lit8 v20, v20, 0x1

    goto :goto_1

    :cond_1
    add-int/lit8 v15, v15, 0x1

    goto :goto_0

    :cond_2
    invoke-virtual {v14}, Ljava/util/Vector;->size()I

    move-result v13

    if-lez v13, :cond_d

    const/4 v15, 0x0

    :goto_2
    if-ge v15, v13, :cond_d

    invoke-virtual {v14, v15}, Ljava/util/Vector;->elementAt(I)Ljava/lang/Object;

    move-result-object v21

    move-object/from16 v0, v21

    instance-of v0, v0, Lorg/apache/tools/ant/types/AntFilterReader;

    move/from16 v27, v0

    if-eqz v27, :cond_c

    invoke-virtual {v14, v15}, Ljava/util/Vector;->elementAt(I)Ljava/lang/Object;

    move-result-object v9

    check-cast v9, Lorg/apache/tools/ant/types/AntFilterReader;

    invoke-virtual {v9}, Lorg/apache/tools/ant/types/AntFilterReader;->getClassName()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v9}, Lorg/apache/tools/ant/types/AntFilterReader;->getClasspath()Lorg/apache/tools/ant/types/Path;

    move-result-object v4

    invoke-virtual {v9}, Lorg/apache/tools/ant/types/AntFilterReader;->getProject()Lorg/apache/tools/ant/Project;

    move-result-object v23

    if-eqz v3, :cond_a

    const/4 v5, 0x0

    if-nez v4, :cond_3

    :try_start_0
    invoke-static {v3}, Ljava/lang/Class;->forName(Ljava/lang/String;)Ljava/lang/Class;

    move-result-object v5

    :goto_3
    if-eqz v5, :cond_a

    sget-object v27, Lorg/apache/tools/ant/filters/util/ChainReaderHelper;->class$java$io$FilterReader:Ljava/lang/Class;

    if-nez v27, :cond_4

    const-string v27, "java.io.FilterReader"

    invoke-static/range {v27 .. v27}, Lorg/apache/tools/ant/filters/util/ChainReaderHelper;->class$(Ljava/lang/String;)Ljava/lang/Class;

    move-result-object v27

    sput-object v27, Lorg/apache/tools/ant/filters/util/ChainReaderHelper;->class$java$io$FilterReader:Ljava/lang/Class;

    :goto_4
    move-object/from16 v0, v27

    invoke-virtual {v0, v5}, Ljava/lang/Class;->isAssignableFrom(Ljava/lang/Class;)Z

    move-result v27

    if-nez v27, :cond_5

    new-instance v27, Lorg/apache/tools/ant/BuildException;

    new-instance v28, Ljava/lang/StringBuffer;

    invoke-direct/range {v28 .. v28}, Ljava/lang/StringBuffer;-><init>()V

    move-object/from16 v0, v28

    invoke-virtual {v0, v3}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    move-result-object v28

    const-string v29, " does not extend java.io.FilterReader"

    invoke-virtual/range {v28 .. v29}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    move-result-object v28

    invoke-virtual/range {v28 .. v28}, Ljava/lang/StringBuffer;->toString()Ljava/lang/String;

    move-result-object v28

    invoke-direct/range {v27 .. v28}, Lorg/apache/tools/ant/BuildException;-><init>(Ljava/lang/String;)V

    throw v27
    :try_end_0
    .catch Ljava/lang/ClassNotFoundException; {:try_start_0 .. :try_end_0} :catch_0
    .catch Ljava/lang/InstantiationException; {:try_start_0 .. :try_end_0} :catch_1
    .catch Ljava/lang/IllegalAccessException; {:try_start_0 .. :try_end_0} :catch_2
    .catch Ljava/lang/reflect/InvocationTargetException; {:try_start_0 .. :try_end_0} :catch_3

    :catch_0
    move-exception v6

    new-instance v27, Lorg/apache/tools/ant/BuildException;

    move-object/from16 v0, v27

    invoke-direct {v0, v6}, Lorg/apache/tools/ant/BuildException;-><init>(Ljava/lang/Throwable;)V

    throw v27

    :cond_3
    :try_start_1
    move-object/from16 v0, v23

    invoke-virtual {v0, v4}, Lorg/apache/tools/ant/Project;->createClassLoader(Lorg/apache/tools/ant/types/Path;)Lorg/apache/tools/ant/AntClassLoader;

    move-result-object v2

    const/16 v27, 0x1

    move/from16 v0, v27

    invoke-static {v3, v0, v2}, Ljava/lang/Class;->forName(Ljava/lang/String;ZLjava/lang/ClassLoader;)Ljava/lang/Class;

    move-result-object v5

    goto :goto_3

    :cond_4
    sget-object v27, Lorg/apache/tools/ant/filters/util/ChainReaderHelper;->class$java$io$FilterReader:Ljava/lang/Class;

    goto :goto_4

    :cond_5
    invoke-virtual {v5}, Ljava/lang/Class;->getConstructors()[Ljava/lang/reflect/Constructor;

    move-result-object v8

    const/16 v20, 0x0

    const/4 v7, 0x0

    :goto_5
    array-length v0, v8

    move/from16 v27, v0

    move/from16 v0, v20

    move/from16 v1, v27

    if-ge v0, v1, :cond_6

    aget-object v27, v8, v20

    invoke-virtual/range {v27 .. v27}, Ljava/lang/reflect/Constructor;->getParameterTypes()[Ljava/lang/Class;

    move-result-object v26

    move-object/from16 v0, v26

    array-length v0, v0

    move/from16 v27, v0

    const/16 v28, 0x1

    move/from16 v0, v27

    move/from16 v1, v28

    if-ne v0, v1, :cond_8

    const/16 v27, 0x0

    aget-object v28, v26, v27

    sget-object v27, Lorg/apache/tools/ant/filters/util/ChainReaderHelper;->class$java$io$Reader:Ljava/lang/Class;

    if-nez v27, :cond_7

    const-string v27, "java.io.Reader"

    invoke-static/range {v27 .. v27}, Lorg/apache/tools/ant/filters/util/ChainReaderHelper;->class$(Ljava/lang/String;)Ljava/lang/Class;

    move-result-object v27

    sput-object v27, Lorg/apache/tools/ant/filters/util/ChainReaderHelper;->class$java$io$Reader:Ljava/lang/Class;

    :goto_6
    move-object/from16 v0, v28

    move-object/from16 v1, v27

    invoke-virtual {v0, v1}, Ljava/lang/Class;->isAssignableFrom(Ljava/lang/Class;)Z

    move-result v27

    if-eqz v27, :cond_8

    const/4 v7, 0x1

    :cond_6
    if-nez v7, :cond_9

    new-instance v27, Lorg/apache/tools/ant/BuildException;

    new-instance v28, Ljava/lang/StringBuffer;

    invoke-direct/range {v28 .. v28}, Ljava/lang/StringBuffer;-><init>()V

    move-object/from16 v0, v28

    invoke-virtual {v0, v3}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    move-result-object v28

    const-string v29, " does not define a public constructor"

    invoke-virtual/range {v28 .. v29}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    move-result-object v28

    const-string v29, " that takes in a Reader as its "

    invoke-virtual/range {v28 .. v29}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    move-result-object v28

    const-string v29, "single argument."

    invoke-virtual/range {v28 .. v29}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    move-result-object v28

    invoke-virtual/range {v28 .. v28}, Ljava/lang/StringBuffer;->toString()Ljava/lang/String;

    move-result-object v28

    invoke-direct/range {v27 .. v28}, Lorg/apache/tools/ant/BuildException;-><init>(Ljava/lang/String;)V

    throw v27
    :try_end_1
    .catch Ljava/lang/ClassNotFoundException; {:try_start_1 .. :try_end_1} :catch_0
    .catch Ljava/lang/InstantiationException; {:try_start_1 .. :try_end_1} :catch_1
    .catch Ljava/lang/IllegalAccessException; {:try_start_1 .. :try_end_1} :catch_2
    .catch Ljava/lang/reflect/InvocationTargetException; {:try_start_1 .. :try_end_1} :catch_3

    :catch_1
    move-exception v17

    new-instance v27, Lorg/apache/tools/ant/BuildException;

    move-object/from16 v0, v27

    move-object/from16 v1, v17

    invoke-direct {v0, v1}, Lorg/apache/tools/ant/BuildException;-><init>(Ljava/lang/Throwable;)V

    throw v27

    :cond_7
    :try_start_2
    sget-object v27, Lorg/apache/tools/ant/filters/util/ChainReaderHelper;->class$java$io$Reader:Ljava/lang/Class;

    goto :goto_6

    :cond_8
    add-int/lit8 v20, v20, 0x1

    goto :goto_5

    :cond_9
    const/16 v27, 0x1

    move/from16 v0, v27

    new-array v0, v0, [Ljava/io/Reader;

    move-object/from16 v24, v0

    const/16 v27, 0x0

    aput-object v18, v24, v27

    aget-object v27, v8, v20

    check-cast v24, [Ljava/lang/Object;

    move-object/from16 v0, v27

    move-object/from16 v1, v24

    invoke-virtual {v0, v1}, Ljava/lang/reflect/Constructor;->newInstance([Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v27

    move-object/from16 v0, v27

    check-cast v0, Ljava/io/Reader;

    move-object/from16 v18, v0

    move-object/from16 v0, p0

    move-object/from16 v1, v18

    invoke-direct {v0, v1}, Lorg/apache/tools/ant/filters/util/ChainReaderHelper;->setProjectOnObject(Ljava/lang/Object;)V

    sget-object v27, Lorg/apache/tools/ant/filters/util/ChainReaderHelper;->class$org$apache$tools$ant$types$Parameterizable:Ljava/lang/Class;

    if-nez v27, :cond_b

    const-string v27, "org.apache.tools.ant.types.Parameterizable"

    invoke-static/range {v27 .. v27}, Lorg/apache/tools/ant/filters/util/ChainReaderHelper;->class$(Ljava/lang/String;)Ljava/lang/Class;

    move-result-object v27

    sput-object v27, Lorg/apache/tools/ant/filters/util/ChainReaderHelper;->class$org$apache$tools$ant$types$Parameterizable:Ljava/lang/Class;

    :goto_7
    move-object/from16 v0, v27

    invoke-virtual {v0, v5}, Ljava/lang/Class;->isAssignableFrom(Ljava/lang/Class;)Z

    move-result v27

    if-eqz v27, :cond_a

    invoke-virtual {v9}, Lorg/apache/tools/ant/types/AntFilterReader;->getParams()[Lorg/apache/tools/ant/types/Parameter;

    move-result-object v22

    move-object/from16 v0, v18

    check-cast v0, Lorg/apache/tools/ant/types/Parameterizable;

    move-object/from16 v27, v0

    move-object/from16 v0, v27

    move-object/from16 v1, v22

    invoke-interface {v0, v1}, Lorg/apache/tools/ant/types/Parameterizable;->setParameters([Lorg/apache/tools/ant/types/Parameter;)V

    :cond_a
    :goto_8
    add-int/lit8 v15, v15, 0x1

    goto/16 :goto_2

    :cond_b
    sget-object v27, Lorg/apache/tools/ant/filters/util/ChainReaderHelper;->class$org$apache$tools$ant$types$Parameterizable:Ljava/lang/Class;
    :try_end_2
    .catch Ljava/lang/ClassNotFoundException; {:try_start_2 .. :try_end_2} :catch_0
    .catch Ljava/lang/InstantiationException; {:try_start_2 .. :try_end_2} :catch_1
    .catch Ljava/lang/IllegalAccessException; {:try_start_2 .. :try_end_2} :catch_2
    .catch Ljava/lang/reflect/InvocationTargetException; {:try_start_2 .. :try_end_2} :catch_3

    goto :goto_7

    :catch_2
    move-exception v16

    new-instance v27, Lorg/apache/tools/ant/BuildException;

    move-object/from16 v0, v27

    move-object/from16 v1, v16

    invoke-direct {v0, v1}, Lorg/apache/tools/ant/BuildException;-><init>(Ljava/lang/Throwable;)V

    throw v27

    :catch_3
    move-exception v19

    new-instance v27, Lorg/apache/tools/ant/BuildException;

    move-object/from16 v0, v27

    move-object/from16 v1, v19

    invoke-direct {v0, v1}, Lorg/apache/tools/ant/BuildException;-><init>(Ljava/lang/Throwable;)V

    throw v27

    :cond_c
    move-object/from16 v0, v21

    instance-of v0, v0, Lorg/apache/tools/ant/filters/ChainableReader;

    move/from16 v27, v0

    if-eqz v27, :cond_a

    move-object/from16 v0, p0

    move-object/from16 v1, v21

    invoke-direct {v0, v1}, Lorg/apache/tools/ant/filters/util/ChainReaderHelper;->setProjectOnObject(Ljava/lang/Object;)V

    check-cast v21, Lorg/apache/tools/ant/filters/ChainableReader;

    move-object/from16 v0, v21

    move-object/from16 v1, v18

    invoke-interface {v0, v1}, Lorg/apache/tools/ant/filters/ChainableReader;->chain(Ljava/io/Reader;)Ljava/io/Reader;

    move-result-object v18

    move-object/from16 v0, p0

    move-object/from16 v1, v18

    invoke-direct {v0, v1}, Lorg/apache/tools/ant/filters/util/ChainReaderHelper;->setProjectOnObject(Ljava/lang/Object;)V

    goto :goto_8

    :cond_d
    return-object v18
.end method

.method public getProject()Lorg/apache/tools/ant/Project;
    .locals 1

    iget-object v0, p0, Lorg/apache/tools/ant/filters/util/ChainReaderHelper;->project:Lorg/apache/tools/ant/Project;

    return-object v0
.end method

.method public readFully(Ljava/io/Reader;)Ljava/lang/String;
    .locals 1
    .param p1    # Ljava/io/Reader;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    iget v0, p0, Lorg/apache/tools/ant/filters/util/ChainReaderHelper;->bufferSize:I

    invoke-static {p1, v0}, Lorg/apache/tools/ant/util/FileUtils;->readFully(Ljava/io/Reader;I)Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public setBufferSize(I)V
    .locals 0
    .param p1    # I

    iput p1, p0, Lorg/apache/tools/ant/filters/util/ChainReaderHelper;->bufferSize:I

    return-void
.end method

.method public setFilterChains(Ljava/util/Vector;)V
    .locals 0
    .param p1    # Ljava/util/Vector;

    iput-object p1, p0, Lorg/apache/tools/ant/filters/util/ChainReaderHelper;->filterChains:Ljava/util/Vector;

    return-void
.end method

.method public setPrimaryReader(Ljava/io/Reader;)V
    .locals 0
    .param p1    # Ljava/io/Reader;

    iput-object p1, p0, Lorg/apache/tools/ant/filters/util/ChainReaderHelper;->primaryReader:Ljava/io/Reader;

    return-void
.end method

.method public setProject(Lorg/apache/tools/ant/Project;)V
    .locals 0
    .param p1    # Lorg/apache/tools/ant/Project;

    iput-object p1, p0, Lorg/apache/tools/ant/filters/util/ChainReaderHelper;->project:Lorg/apache/tools/ant/Project;

    return-void
.end method
