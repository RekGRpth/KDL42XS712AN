.class public final Lorg/apache/tools/ant/filters/LineContains;
.super Lorg/apache/tools/ant/filters/BaseParamFilterReader;
.source "LineContains.java"

# interfaces
.implements Lorg/apache/tools/ant/filters/ChainableReader;


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lorg/apache/tools/ant/filters/LineContains$Contains;
    }
.end annotation


# static fields
.field private static final CONTAINS_KEY:Ljava/lang/String; = "contains"

.field private static final NEGATE_KEY:Ljava/lang/String; = "negate"


# instance fields
.field private contains:Ljava/util/Vector;

.field private line:Ljava/lang/String;

.field private negate:Z


# direct methods
.method public constructor <init>()V
    .locals 1

    invoke-direct {p0}, Lorg/apache/tools/ant/filters/BaseParamFilterReader;-><init>()V

    new-instance v0, Ljava/util/Vector;

    invoke-direct {v0}, Ljava/util/Vector;-><init>()V

    iput-object v0, p0, Lorg/apache/tools/ant/filters/LineContains;->contains:Ljava/util/Vector;

    const/4 v0, 0x0

    iput-object v0, p0, Lorg/apache/tools/ant/filters/LineContains;->line:Ljava/lang/String;

    const/4 v0, 0x0

    iput-boolean v0, p0, Lorg/apache/tools/ant/filters/LineContains;->negate:Z

    return-void
.end method

.method public constructor <init>(Ljava/io/Reader;)V
    .locals 1
    .param p1    # Ljava/io/Reader;

    invoke-direct {p0, p1}, Lorg/apache/tools/ant/filters/BaseParamFilterReader;-><init>(Ljava/io/Reader;)V

    new-instance v0, Ljava/util/Vector;

    invoke-direct {v0}, Ljava/util/Vector;-><init>()V

    iput-object v0, p0, Lorg/apache/tools/ant/filters/LineContains;->contains:Ljava/util/Vector;

    const/4 v0, 0x0

    iput-object v0, p0, Lorg/apache/tools/ant/filters/LineContains;->line:Ljava/lang/String;

    const/4 v0, 0x0

    iput-boolean v0, p0, Lorg/apache/tools/ant/filters/LineContains;->negate:Z

    return-void
.end method

.method private getContains()Ljava/util/Vector;
    .locals 1

    iget-object v0, p0, Lorg/apache/tools/ant/filters/LineContains;->contains:Ljava/util/Vector;

    return-object v0
.end method

.method private initialize()V
    .locals 4

    invoke-virtual {p0}, Lorg/apache/tools/ant/filters/LineContains;->getParameters()[Lorg/apache/tools/ant/types/Parameter;

    move-result-object v1

    if-eqz v1, :cond_2

    const/4 v0, 0x0

    :goto_0
    array-length v2, v1

    if-ge v0, v2, :cond_2

    const-string v2, "contains"

    aget-object v3, v1, v0

    invoke-virtual {v3}, Lorg/apache/tools/ant/types/Parameter;->getType()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_1

    iget-object v2, p0, Lorg/apache/tools/ant/filters/LineContains;->contains:Ljava/util/Vector;

    aget-object v3, v1, v0

    invoke-virtual {v3}, Lorg/apache/tools/ant/types/Parameter;->getValue()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Ljava/util/Vector;->addElement(Ljava/lang/Object;)V

    :cond_0
    :goto_1
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    :cond_1
    const-string v2, "negate"

    aget-object v3, v1, v0

    invoke-virtual {v3}, Lorg/apache/tools/ant/types/Parameter;->getType()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_0

    aget-object v2, v1, v0

    invoke-virtual {v2}, Lorg/apache/tools/ant/types/Parameter;->getValue()Ljava/lang/String;

    move-result-object v2

    invoke-static {v2}, Lorg/apache/tools/ant/Project;->toBoolean(Ljava/lang/String;)Z

    move-result v2

    invoke-virtual {p0, v2}, Lorg/apache/tools/ant/filters/LineContains;->setNegate(Z)V

    goto :goto_1

    :cond_2
    return-void
.end method

.method private setContains(Ljava/util/Vector;)V
    .locals 0
    .param p1    # Ljava/util/Vector;

    iput-object p1, p0, Lorg/apache/tools/ant/filters/LineContains;->contains:Ljava/util/Vector;

    return-void
.end method


# virtual methods
.method public addConfiguredContains(Lorg/apache/tools/ant/filters/LineContains$Contains;)V
    .locals 2
    .param p1    # Lorg/apache/tools/ant/filters/LineContains$Contains;

    iget-object v0, p0, Lorg/apache/tools/ant/filters/LineContains;->contains:Ljava/util/Vector;

    invoke-virtual {p1}, Lorg/apache/tools/ant/filters/LineContains$Contains;->getValue()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/util/Vector;->addElement(Ljava/lang/Object;)V

    return-void
.end method

.method public chain(Ljava/io/Reader;)Ljava/io/Reader;
    .locals 2
    .param p1    # Ljava/io/Reader;

    new-instance v0, Lorg/apache/tools/ant/filters/LineContains;

    invoke-direct {v0, p1}, Lorg/apache/tools/ant/filters/LineContains;-><init>(Ljava/io/Reader;)V

    invoke-direct {p0}, Lorg/apache/tools/ant/filters/LineContains;->getContains()Ljava/util/Vector;

    move-result-object v1

    invoke-direct {v0, v1}, Lorg/apache/tools/ant/filters/LineContains;->setContains(Ljava/util/Vector;)V

    invoke-virtual {p0}, Lorg/apache/tools/ant/filters/LineContains;->isNegated()Z

    move-result v1

    invoke-virtual {v0, v1}, Lorg/apache/tools/ant/filters/LineContains;->setNegate(Z)V

    return-object v0
.end method

.method public isNegated()Z
    .locals 1

    iget-boolean v0, p0, Lorg/apache/tools/ant/filters/LineContains;->negate:Z

    return v0
.end method

.method public read()I
    .locals 8
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    const/4 v6, 0x0

    const/4 v5, 0x1

    invoke-virtual {p0}, Lorg/apache/tools/ant/filters/LineContains;->getInitialized()Z

    move-result v7

    if-nez v7, :cond_0

    invoke-direct {p0}, Lorg/apache/tools/ant/filters/LineContains;->initialize()V

    invoke-virtual {p0, v5}, Lorg/apache/tools/ant/filters/LineContains;->setInitialized(Z)V

    :cond_0
    const/4 v0, -0x1

    iget-object v7, p0, Lorg/apache/tools/ant/filters/LineContains;->line:Ljava/lang/String;

    if-eqz v7, :cond_3

    iget-object v7, p0, Lorg/apache/tools/ant/filters/LineContains;->line:Ljava/lang/String;

    invoke-virtual {v7, v6}, Ljava/lang/String;->charAt(I)C

    move-result v0

    iget-object v6, p0, Lorg/apache/tools/ant/filters/LineContains;->line:Ljava/lang/String;

    invoke-virtual {v6}, Ljava/lang/String;->length()I

    move-result v6

    if-ne v6, v5, :cond_2

    const/4 v5, 0x0

    iput-object v5, p0, Lorg/apache/tools/ant/filters/LineContains;->line:Ljava/lang/String;

    :cond_1
    :goto_0
    move v5, v0

    :goto_1
    return v5

    :cond_2
    iget-object v6, p0, Lorg/apache/tools/ant/filters/LineContains;->line:Ljava/lang/String;

    invoke-virtual {v6, v5}, Ljava/lang/String;->substring(I)Ljava/lang/String;

    move-result-object v5

    iput-object v5, p0, Lorg/apache/tools/ant/filters/LineContains;->line:Ljava/lang/String;

    goto :goto_0

    :cond_3
    iget-object v7, p0, Lorg/apache/tools/ant/filters/LineContains;->contains:Ljava/util/Vector;

    invoke-virtual {v7}, Ljava/util/Vector;->size()I

    move-result v1

    invoke-virtual {p0}, Lorg/apache/tools/ant/filters/LineContains;->readLine()Ljava/lang/String;

    move-result-object v7

    iput-object v7, p0, Lorg/apache/tools/ant/filters/LineContains;->line:Ljava/lang/String;

    :goto_2
    iget-object v7, p0, Lorg/apache/tools/ant/filters/LineContains;->line:Ljava/lang/String;

    if-eqz v7, :cond_6

    const/4 v4, 0x1

    const/4 v3, 0x0

    :goto_3
    if-eqz v4, :cond_5

    if-ge v3, v1, :cond_5

    iget-object v7, p0, Lorg/apache/tools/ant/filters/LineContains;->contains:Ljava/util/Vector;

    invoke-virtual {v7, v3}, Ljava/util/Vector;->elementAt(I)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Ljava/lang/String;

    iget-object v7, p0, Lorg/apache/tools/ant/filters/LineContains;->line:Ljava/lang/String;

    invoke-virtual {v7, v2}, Ljava/lang/String;->indexOf(Ljava/lang/String;)I

    move-result v7

    if-ltz v7, :cond_4

    move v4, v5

    :goto_4
    add-int/lit8 v3, v3, 0x1

    goto :goto_3

    :cond_4
    move v4, v6

    goto :goto_4

    :cond_5
    invoke-virtual {p0}, Lorg/apache/tools/ant/filters/LineContains;->isNegated()Z

    move-result v7

    xor-int/2addr v7, v4

    if-eqz v7, :cond_7

    :cond_6
    iget-object v5, p0, Lorg/apache/tools/ant/filters/LineContains;->line:Ljava/lang/String;

    if-eqz v5, :cond_1

    invoke-virtual {p0}, Lorg/apache/tools/ant/filters/LineContains;->read()I

    move-result v5

    goto :goto_1

    :cond_7
    invoke-virtual {p0}, Lorg/apache/tools/ant/filters/LineContains;->readLine()Ljava/lang/String;

    move-result-object v7

    iput-object v7, p0, Lorg/apache/tools/ant/filters/LineContains;->line:Ljava/lang/String;

    goto :goto_2
.end method

.method public setNegate(Z)V
    .locals 0
    .param p1    # Z

    iput-boolean p1, p0, Lorg/apache/tools/ant/filters/LineContains;->negate:Z

    return-void
.end method
