.class public Lorg/apache/tools/ant/filters/EscapeUnicode;
.super Lorg/apache/tools/ant/filters/BaseParamFilterReader;
.source "EscapeUnicode.java"

# interfaces
.implements Lorg/apache/tools/ant/filters/ChainableReader;


# instance fields
.field private unicodeBuf:Ljava/lang/StringBuffer;


# direct methods
.method public constructor <init>()V
    .locals 1

    invoke-direct {p0}, Lorg/apache/tools/ant/filters/BaseParamFilterReader;-><init>()V

    new-instance v0, Ljava/lang/StringBuffer;

    invoke-direct {v0}, Ljava/lang/StringBuffer;-><init>()V

    iput-object v0, p0, Lorg/apache/tools/ant/filters/EscapeUnicode;->unicodeBuf:Ljava/lang/StringBuffer;

    return-void
.end method

.method public constructor <init>(Ljava/io/Reader;)V
    .locals 1
    .param p1    # Ljava/io/Reader;

    invoke-direct {p0, p1}, Lorg/apache/tools/ant/filters/BaseParamFilterReader;-><init>(Ljava/io/Reader;)V

    new-instance v0, Ljava/lang/StringBuffer;

    invoke-direct {v0}, Ljava/lang/StringBuffer;-><init>()V

    iput-object v0, p0, Lorg/apache/tools/ant/filters/EscapeUnicode;->unicodeBuf:Ljava/lang/StringBuffer;

    return-void
.end method

.method private initialize()V
    .locals 0

    return-void
.end method


# virtual methods
.method public final chain(Ljava/io/Reader;)Ljava/io/Reader;
    .locals 2
    .param p1    # Ljava/io/Reader;

    new-instance v0, Lorg/apache/tools/ant/filters/EscapeUnicode;

    invoke-direct {v0, p1}, Lorg/apache/tools/ant/filters/EscapeUnicode;-><init>(Ljava/io/Reader;)V

    const/4 v1, 0x1

    invoke-virtual {v0, v1}, Lorg/apache/tools/ant/filters/EscapeUnicode;->setInitialized(Z)V

    return-object v0
.end method

.method public final read()I
    .locals 7
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    const/4 v5, 0x0

    invoke-virtual {p0}, Lorg/apache/tools/ant/filters/EscapeUnicode;->getInitialized()Z

    move-result v4

    if-nez v4, :cond_0

    invoke-direct {p0}, Lorg/apache/tools/ant/filters/EscapeUnicode;->initialize()V

    const/4 v4, 0x1

    invoke-virtual {p0, v4}, Lorg/apache/tools/ant/filters/EscapeUnicode;->setInitialized(Z)V

    :cond_0
    const/4 v1, -0x1

    iget-object v4, p0, Lorg/apache/tools/ant/filters/EscapeUnicode;->unicodeBuf:Ljava/lang/StringBuffer;

    invoke-virtual {v4}, Ljava/lang/StringBuffer;->length()I

    move-result v4

    if-nez v4, :cond_3

    iget-object v4, p0, Lorg/apache/tools/ant/filters/EscapeUnicode;->in:Ljava/io/Reader;

    invoke-virtual {v4}, Ljava/io/Reader;->read()I

    move-result v1

    const/4 v4, -0x1

    if-eq v1, v4, :cond_2

    int-to-char v0, v1

    const/16 v4, 0x80

    if-lt v0, v4, :cond_2

    new-instance v4, Ljava/lang/StringBuffer;

    const-string v5, "u0000"

    invoke-direct {v4, v5}, Ljava/lang/StringBuffer;-><init>(Ljava/lang/String;)V

    iput-object v4, p0, Lorg/apache/tools/ant/filters/EscapeUnicode;->unicodeBuf:Ljava/lang/StringBuffer;

    invoke-static {v1}, Ljava/lang/Integer;->toHexString(I)Ljava/lang/String;

    move-result-object v3

    const/4 v2, 0x0

    :goto_0
    invoke-virtual {v3}, Ljava/lang/String;->length()I

    move-result v4

    if-ge v2, v4, :cond_1

    iget-object v4, p0, Lorg/apache/tools/ant/filters/EscapeUnicode;->unicodeBuf:Ljava/lang/StringBuffer;

    iget-object v5, p0, Lorg/apache/tools/ant/filters/EscapeUnicode;->unicodeBuf:Ljava/lang/StringBuffer;

    invoke-virtual {v5}, Ljava/lang/StringBuffer;->length()I

    move-result v5

    invoke-virtual {v3}, Ljava/lang/String;->length()I

    move-result v6

    sub-int/2addr v5, v6

    add-int/2addr v5, v2

    invoke-virtual {v3, v2}, Ljava/lang/String;->charAt(I)C

    move-result v6

    invoke-virtual {v4, v5, v6}, Ljava/lang/StringBuffer;->setCharAt(IC)V

    add-int/lit8 v2, v2, 0x1

    goto :goto_0

    :cond_1
    const/16 v1, 0x5c

    :cond_2
    :goto_1
    return v1

    :cond_3
    iget-object v4, p0, Lorg/apache/tools/ant/filters/EscapeUnicode;->unicodeBuf:Ljava/lang/StringBuffer;

    invoke-virtual {v4, v5}, Ljava/lang/StringBuffer;->charAt(I)C

    move-result v1

    iget-object v4, p0, Lorg/apache/tools/ant/filters/EscapeUnicode;->unicodeBuf:Ljava/lang/StringBuffer;

    invoke-virtual {v4, v5}, Ljava/lang/StringBuffer;->deleteCharAt(I)Ljava/lang/StringBuffer;

    goto :goto_1
.end method
