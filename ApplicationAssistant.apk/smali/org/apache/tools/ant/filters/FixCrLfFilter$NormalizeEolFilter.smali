.class Lorg/apache/tools/ant/filters/FixCrLfFilter$NormalizeEolFilter;
.super Lorg/apache/tools/ant/filters/FixCrLfFilter$SimpleFilterReader;
.source "FixCrLfFilter.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lorg/apache/tools/ant/filters/FixCrLfFilter;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0xa
    name = "NormalizeEolFilter"
.end annotation


# instance fields
.field private eol:[C

.field private fixLast:Z

.field private normalizedEOL:I

.field private previousWasEOL:Z


# direct methods
.method public constructor <init>(Ljava/io/Reader;Ljava/lang/String;Z)V
    .locals 1
    .param p1    # Ljava/io/Reader;
    .param p2    # Ljava/lang/String;
    .param p3    # Z

    invoke-direct {p0, p1}, Lorg/apache/tools/ant/filters/FixCrLfFilter$SimpleFilterReader;-><init>(Ljava/io/Reader;)V

    const/4 v0, 0x0

    iput v0, p0, Lorg/apache/tools/ant/filters/FixCrLfFilter$NormalizeEolFilter;->normalizedEOL:I

    const/4 v0, 0x0

    iput-object v0, p0, Lorg/apache/tools/ant/filters/FixCrLfFilter$NormalizeEolFilter;->eol:[C

    invoke-virtual {p2}, Ljava/lang/String;->toCharArray()[C

    move-result-object v0

    iput-object v0, p0, Lorg/apache/tools/ant/filters/FixCrLfFilter$NormalizeEolFilter;->eol:[C

    iput-boolean p3, p0, Lorg/apache/tools/ant/filters/FixCrLfFilter$NormalizeEolFilter;->fixLast:Z

    return-void
.end method


# virtual methods
.method public read()I
    .locals 10
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    const/16 v9, 0xd

    const/16 v8, 0xa

    invoke-super {p0}, Lorg/apache/tools/ant/filters/FixCrLfFilter$SimpleFilterReader;->read()I

    move-result v6

    iget v7, p0, Lorg/apache/tools/ant/filters/FixCrLfFilter$NormalizeEolFilter;->normalizedEOL:I

    if-nez v7, :cond_8

    const/4 v4, 0x0

    const/4 v0, 0x0

    sparse-switch v6, :sswitch_data_0

    :cond_0
    :goto_0
    if-lez v4, :cond_7

    move v5, v4

    :goto_1
    add-int/lit8 v4, v5, -0x1

    if-lez v5, :cond_5

    iget-object v7, p0, Lorg/apache/tools/ant/filters/FixCrLfFilter$NormalizeEolFilter;->eol:[C

    invoke-virtual {p0, v7}, Lorg/apache/tools/ant/filters/FixCrLfFilter$NormalizeEolFilter;->push([C)V

    iget v7, p0, Lorg/apache/tools/ant/filters/FixCrLfFilter$NormalizeEolFilter;->normalizedEOL:I

    iget-object v8, p0, Lorg/apache/tools/ant/filters/FixCrLfFilter$NormalizeEolFilter;->eol:[C

    array-length v8, v8

    add-int/2addr v7, v8

    iput v7, p0, Lorg/apache/tools/ant/filters/FixCrLfFilter$NormalizeEolFilter;->normalizedEOL:I

    move v5, v4

    goto :goto_1

    :sswitch_0
    invoke-super {p0}, Lorg/apache/tools/ant/filters/FixCrLfFilter$SimpleFilterReader;->read()I

    move-result v1

    const/4 v7, -0x1

    if-ne v1, v7, :cond_1

    const/4 v0, 0x1

    iget-boolean v7, p0, Lorg/apache/tools/ant/filters/FixCrLfFilter$NormalizeEolFilter;->fixLast:Z

    if-eqz v7, :cond_0

    iget-boolean v7, p0, Lorg/apache/tools/ant/filters/FixCrLfFilter$NormalizeEolFilter;->previousWasEOL:Z

    if-nez v7, :cond_0

    const/4 v4, 0x1

    invoke-virtual {p0, v6}, Lorg/apache/tools/ant/filters/FixCrLfFilter$NormalizeEolFilter;->push(I)V

    goto :goto_0

    :cond_1
    invoke-virtual {p0, v1}, Lorg/apache/tools/ant/filters/FixCrLfFilter$NormalizeEolFilter;->push(I)V

    goto :goto_0

    :sswitch_1
    const/4 v0, 0x1

    iget-boolean v7, p0, Lorg/apache/tools/ant/filters/FixCrLfFilter$NormalizeEolFilter;->fixLast:Z

    if-eqz v7, :cond_0

    iget-boolean v7, p0, Lorg/apache/tools/ant/filters/FixCrLfFilter$NormalizeEolFilter;->previousWasEOL:Z

    if-nez v7, :cond_0

    const/4 v4, 0x1

    goto :goto_0

    :sswitch_2
    const/4 v4, 0x1

    goto :goto_0

    :sswitch_3
    const/4 v4, 0x1

    invoke-super {p0}, Lorg/apache/tools/ant/filters/FixCrLfFilter$SimpleFilterReader;->read()I

    move-result v2

    invoke-super {p0}, Lorg/apache/tools/ant/filters/FixCrLfFilter$SimpleFilterReader;->read()I

    move-result v3

    if-ne v2, v9, :cond_2

    if-eq v3, v8, :cond_0

    :cond_2
    if-ne v2, v9, :cond_3

    const/4 v4, 0x2

    invoke-virtual {p0, v3}, Lorg/apache/tools/ant/filters/FixCrLfFilter$NormalizeEolFilter;->push(I)V

    goto :goto_0

    :cond_3
    if-ne v2, v8, :cond_4

    invoke-virtual {p0, v3}, Lorg/apache/tools/ant/filters/FixCrLfFilter$NormalizeEolFilter;->push(I)V

    goto :goto_0

    :cond_4
    invoke-virtual {p0, v3}, Lorg/apache/tools/ant/filters/FixCrLfFilter$NormalizeEolFilter;->push(I)V

    invoke-virtual {p0, v2}, Lorg/apache/tools/ant/filters/FixCrLfFilter$NormalizeEolFilter;->push(I)V

    goto :goto_0

    :cond_5
    const/4 v7, 0x1

    iput-boolean v7, p0, Lorg/apache/tools/ant/filters/FixCrLfFilter$NormalizeEolFilter;->previousWasEOL:Z

    invoke-virtual {p0}, Lorg/apache/tools/ant/filters/FixCrLfFilter$NormalizeEolFilter;->read()I

    move-result v6

    :cond_6
    :goto_2
    return v6

    :cond_7
    if-nez v0, :cond_6

    const/4 v7, 0x0

    iput-boolean v7, p0, Lorg/apache/tools/ant/filters/FixCrLfFilter$NormalizeEolFilter;->previousWasEOL:Z

    goto :goto_2

    :cond_8
    iget v7, p0, Lorg/apache/tools/ant/filters/FixCrLfFilter$NormalizeEolFilter;->normalizedEOL:I

    add-int/lit8 v7, v7, -0x1

    iput v7, p0, Lorg/apache/tools/ant/filters/FixCrLfFilter$NormalizeEolFilter;->normalizedEOL:I

    goto :goto_2

    nop

    :sswitch_data_0
    .sparse-switch
        -0x1 -> :sswitch_1
        0xa -> :sswitch_2
        0xd -> :sswitch_3
        0x1a -> :sswitch_0
    .end sparse-switch
.end method
