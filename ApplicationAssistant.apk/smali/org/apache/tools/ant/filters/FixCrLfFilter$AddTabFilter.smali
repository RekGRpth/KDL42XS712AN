.class Lorg/apache/tools/ant/filters/FixCrLfFilter$AddTabFilter;
.super Lorg/apache/tools/ant/filters/FixCrLfFilter$SimpleFilterReader;
.source "FixCrLfFilter.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lorg/apache/tools/ant/filters/FixCrLfFilter;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0xa
    name = "AddTabFilter"
.end annotation


# instance fields
.field private columnNumber:I

.field private tabLength:I


# direct methods
.method public constructor <init>(Ljava/io/Reader;I)V
    .locals 1
    .param p1    # Ljava/io/Reader;
    .param p2    # I

    const/4 v0, 0x0

    invoke-direct {p0, p1}, Lorg/apache/tools/ant/filters/FixCrLfFilter$SimpleFilterReader;-><init>(Ljava/io/Reader;)V

    iput v0, p0, Lorg/apache/tools/ant/filters/FixCrLfFilter$AddTabFilter;->columnNumber:I

    iput v0, p0, Lorg/apache/tools/ant/filters/FixCrLfFilter$AddTabFilter;->tabLength:I

    iput p2, p0, Lorg/apache/tools/ant/filters/FixCrLfFilter$AddTabFilter;->tabLength:I

    return-void
.end method


# virtual methods
.method public read()I
    .locals 8
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    invoke-super {p0}, Lorg/apache/tools/ant/filters/FixCrLfFilter$SimpleFilterReader;->read()I

    move-result v0

    sparse-switch v0, :sswitch_data_0

    iget v6, p0, Lorg/apache/tools/ant/filters/FixCrLfFilter$AddTabFilter;->columnNumber:I

    add-int/lit8 v6, v6, 0x1

    iput v6, p0, Lorg/apache/tools/ant/filters/FixCrLfFilter$AddTabFilter;->columnNumber:I

    :cond_0
    :goto_0
    return v0

    :sswitch_0
    const/4 v6, 0x0

    iput v6, p0, Lorg/apache/tools/ant/filters/FixCrLfFilter$AddTabFilter;->columnNumber:I

    goto :goto_0

    :sswitch_1
    iget v6, p0, Lorg/apache/tools/ant/filters/FixCrLfFilter$AddTabFilter;->columnNumber:I

    add-int/lit8 v6, v6, 0x1

    iput v6, p0, Lorg/apache/tools/ant/filters/FixCrLfFilter$AddTabFilter;->columnNumber:I

    invoke-virtual {p0}, Lorg/apache/tools/ant/filters/FixCrLfFilter$AddTabFilter;->editsBlocked()Z

    move-result v6

    if-nez v6, :cond_0

    iget v6, p0, Lorg/apache/tools/ant/filters/FixCrLfFilter$AddTabFilter;->columnNumber:I

    iget v7, p0, Lorg/apache/tools/ant/filters/FixCrLfFilter$AddTabFilter;->tabLength:I

    add-int/2addr v6, v7

    add-int/lit8 v6, v6, -0x1

    iget v7, p0, Lorg/apache/tools/ant/filters/FixCrLfFilter$AddTabFilter;->tabLength:I

    div-int/2addr v6, v7

    iget v7, p0, Lorg/apache/tools/ant/filters/FixCrLfFilter$AddTabFilter;->tabLength:I

    mul-int v1, v6, v7

    const/4 v2, 0x1

    const/4 v4, 0x0

    :goto_1
    invoke-super {p0}, Lorg/apache/tools/ant/filters/FixCrLfFilter$SimpleFilterReader;->read()I

    move-result v0

    const/4 v6, -0x1

    if-eq v0, v6, :cond_4

    sparse-switch v0, :sswitch_data_1

    invoke-virtual {p0, v0}, Lorg/apache/tools/ant/filters/FixCrLfFilter$AddTabFilter;->push(I)V

    move v3, v2

    :goto_2
    add-int/lit8 v2, v3, -0x1

    if-lez v3, :cond_3

    const/16 v6, 0x20

    invoke-virtual {p0, v6}, Lorg/apache/tools/ant/filters/FixCrLfFilter$AddTabFilter;->push(C)V

    iget v6, p0, Lorg/apache/tools/ant/filters/FixCrLfFilter$AddTabFilter;->columnNumber:I

    add-int/lit8 v6, v6, -0x1

    iput v6, p0, Lorg/apache/tools/ant/filters/FixCrLfFilter$AddTabFilter;->columnNumber:I

    move v3, v2

    goto :goto_2

    :sswitch_2
    iget v6, p0, Lorg/apache/tools/ant/filters/FixCrLfFilter$AddTabFilter;->columnNumber:I

    add-int/lit8 v6, v6, 0x1

    iput v6, p0, Lorg/apache/tools/ant/filters/FixCrLfFilter$AddTabFilter;->columnNumber:I

    if-ne v6, v1, :cond_1

    add-int/lit8 v4, v4, 0x1

    const/4 v2, 0x0

    iget v6, p0, Lorg/apache/tools/ant/filters/FixCrLfFilter$AddTabFilter;->tabLength:I

    add-int/2addr v1, v6

    goto :goto_1

    :cond_1
    add-int/lit8 v2, v2, 0x1

    goto :goto_1

    :sswitch_3
    iput v1, p0, Lorg/apache/tools/ant/filters/FixCrLfFilter$AddTabFilter;->columnNumber:I

    add-int/lit8 v4, v4, 0x1

    const/4 v2, 0x0

    iget v6, p0, Lorg/apache/tools/ant/filters/FixCrLfFilter$AddTabFilter;->tabLength:I

    add-int/2addr v1, v6

    goto :goto_1

    :goto_3
    add-int/lit8 v4, v5, -0x1

    if-lez v5, :cond_2

    const/16 v6, 0x9

    invoke-virtual {p0, v6}, Lorg/apache/tools/ant/filters/FixCrLfFilter$AddTabFilter;->push(C)V

    iget v6, p0, Lorg/apache/tools/ant/filters/FixCrLfFilter$AddTabFilter;->columnNumber:I

    iget v7, p0, Lorg/apache/tools/ant/filters/FixCrLfFilter$AddTabFilter;->tabLength:I

    sub-int/2addr v6, v7

    iput v6, p0, Lorg/apache/tools/ant/filters/FixCrLfFilter$AddTabFilter;->columnNumber:I

    move v5, v4

    goto :goto_3

    :cond_2
    invoke-super {p0}, Lorg/apache/tools/ant/filters/FixCrLfFilter$SimpleFilterReader;->read()I

    move-result v0

    sparse-switch v0, :sswitch_data_2

    goto :goto_0

    :sswitch_4
    iget v6, p0, Lorg/apache/tools/ant/filters/FixCrLfFilter$AddTabFilter;->columnNumber:I

    iget v7, p0, Lorg/apache/tools/ant/filters/FixCrLfFilter$AddTabFilter;->tabLength:I

    add-int/2addr v6, v7

    iput v6, p0, Lorg/apache/tools/ant/filters/FixCrLfFilter$AddTabFilter;->columnNumber:I

    goto :goto_0

    :sswitch_5
    iget v6, p0, Lorg/apache/tools/ant/filters/FixCrLfFilter$AddTabFilter;->columnNumber:I

    add-int/lit8 v6, v6, 0x1

    iput v6, p0, Lorg/apache/tools/ant/filters/FixCrLfFilter$AddTabFilter;->columnNumber:I

    goto/16 :goto_0

    :sswitch_6
    iget v6, p0, Lorg/apache/tools/ant/filters/FixCrLfFilter$AddTabFilter;->columnNumber:I

    iget v7, p0, Lorg/apache/tools/ant/filters/FixCrLfFilter$AddTabFilter;->tabLength:I

    add-int/2addr v6, v7

    add-int/lit8 v6, v6, -0x1

    iget v7, p0, Lorg/apache/tools/ant/filters/FixCrLfFilter$AddTabFilter;->tabLength:I

    div-int/2addr v6, v7

    iget v7, p0, Lorg/apache/tools/ant/filters/FixCrLfFilter$AddTabFilter;->tabLength:I

    mul-int/2addr v6, v7

    iput v6, p0, Lorg/apache/tools/ant/filters/FixCrLfFilter$AddTabFilter;->columnNumber:I

    goto/16 :goto_0

    :cond_3
    move v5, v4

    goto :goto_3

    :cond_4
    move v3, v2

    goto :goto_2

    nop

    :sswitch_data_0
    .sparse-switch
        0x9 -> :sswitch_6
        0xa -> :sswitch_0
        0xd -> :sswitch_0
        0x20 -> :sswitch_1
    .end sparse-switch

    :sswitch_data_1
    .sparse-switch
        0x9 -> :sswitch_3
        0x20 -> :sswitch_2
    .end sparse-switch

    :sswitch_data_2
    .sparse-switch
        0x9 -> :sswitch_4
        0x20 -> :sswitch_5
    .end sparse-switch
.end method
