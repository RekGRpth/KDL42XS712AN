.class public Lorg/apache/tools/ant/DefaultLogger;
.super Ljava/lang/Object;
.source "DefaultLogger.java"

# interfaces
.implements Lorg/apache/tools/ant/BuildLogger;


# static fields
.field public static final LEFT_COLUMN_SIZE:I = 0xc

.field protected static final lSep:Ljava/lang/String;


# instance fields
.field protected emacsMode:Z

.field protected err:Ljava/io/PrintStream;

.field protected msgOutputLevel:I

.field protected out:Ljava/io/PrintStream;

.field private startTime:J


# direct methods
.method static constructor <clinit>()V
    .locals 1

    sget-object v0, Lorg/apache/tools/ant/util/StringUtils;->LINE_SEP:Ljava/lang/String;

    sput-object v0, Lorg/apache/tools/ant/DefaultLogger;->lSep:Ljava/lang/String;

    return-void
.end method

.method public constructor <init>()V
    .locals 3

    const/4 v2, 0x0

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    iput v2, p0, Lorg/apache/tools/ant/DefaultLogger;->msgOutputLevel:I

    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v0

    iput-wide v0, p0, Lorg/apache/tools/ant/DefaultLogger;->startTime:J

    iput-boolean v2, p0, Lorg/apache/tools/ant/DefaultLogger;->emacsMode:Z

    return-void
.end method

.method protected static formatTime(J)Ljava/lang/String;
    .locals 1
    .param p0    # J

    invoke-static {p0, p1}, Lorg/apache/tools/ant/util/DateUtils;->formatElapsedTime(J)Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method


# virtual methods
.method public buildFinished(Lorg/apache/tools/ant/BuildEvent;)V
    .locals 8
    .param p1    # Lorg/apache/tools/ant/BuildEvent;

    const/4 v7, 0x3

    invoke-virtual {p1}, Lorg/apache/tools/ant/BuildEvent;->getException()Ljava/lang/Throwable;

    move-result-object v0

    new-instance v1, Ljava/lang/StringBuffer;

    invoke-direct {v1}, Ljava/lang/StringBuffer;-><init>()V

    if-nez v0, :cond_0

    sget-object v3, Lorg/apache/tools/ant/util/StringUtils;->LINE_SEP:Ljava/lang/String;

    invoke-virtual {v1, v3}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    invoke-virtual {p0}, Lorg/apache/tools/ant/DefaultLogger;->getBuildSuccessfulMessage()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v1, v3}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    :goto_0
    sget-object v3, Lorg/apache/tools/ant/util/StringUtils;->LINE_SEP:Ljava/lang/String;

    invoke-virtual {v1, v3}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    const-string v3, "Total time: "

    invoke-virtual {v1, v3}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v3

    iget-wide v5, p0, Lorg/apache/tools/ant/DefaultLogger;->startTime:J

    sub-long/2addr v3, v5

    invoke-static {v3, v4}, Lorg/apache/tools/ant/DefaultLogger;->formatTime(J)Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v1, v3}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    invoke-virtual {v1}, Ljava/lang/StringBuffer;->toString()Ljava/lang/String;

    move-result-object v2

    if-nez v0, :cond_3

    iget-object v3, p0, Lorg/apache/tools/ant/DefaultLogger;->out:Ljava/io/PrintStream;

    invoke-virtual {p0, v2, v3, v7}, Lorg/apache/tools/ant/DefaultLogger;->printMessage(Ljava/lang/String;Ljava/io/PrintStream;I)V

    :goto_1
    invoke-virtual {p0, v2}, Lorg/apache/tools/ant/DefaultLogger;->log(Ljava/lang/String;)V

    return-void

    :cond_0
    sget-object v3, Lorg/apache/tools/ant/util/StringUtils;->LINE_SEP:Ljava/lang/String;

    invoke-virtual {v1, v3}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    invoke-virtual {p0}, Lorg/apache/tools/ant/DefaultLogger;->getBuildFailedMessage()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v1, v3}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    sget-object v3, Lorg/apache/tools/ant/util/StringUtils;->LINE_SEP:Ljava/lang/String;

    invoke-virtual {v1, v3}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    iget v3, p0, Lorg/apache/tools/ant/DefaultLogger;->msgOutputLevel:I

    if-le v7, v3, :cond_1

    instance-of v3, v0, Lorg/apache/tools/ant/BuildException;

    if-nez v3, :cond_2

    :cond_1
    invoke-static {v0}, Lorg/apache/tools/ant/util/StringUtils;->getStackTrace(Ljava/lang/Throwable;)Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v1, v3}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    goto :goto_0

    :cond_2
    invoke-virtual {v0}, Ljava/lang/Throwable;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v1, v3}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    move-result-object v3

    sget-object v4, Lorg/apache/tools/ant/DefaultLogger;->lSep:Ljava/lang/String;

    invoke-virtual {v3, v4}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    goto :goto_0

    :cond_3
    iget-object v3, p0, Lorg/apache/tools/ant/DefaultLogger;->err:Ljava/io/PrintStream;

    const/4 v4, 0x0

    invoke-virtual {p0, v2, v3, v4}, Lorg/apache/tools/ant/DefaultLogger;->printMessage(Ljava/lang/String;Ljava/io/PrintStream;I)V

    goto :goto_1
.end method

.method public buildStarted(Lorg/apache/tools/ant/BuildEvent;)V
    .locals 2
    .param p1    # Lorg/apache/tools/ant/BuildEvent;

    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v0

    iput-wide v0, p0, Lorg/apache/tools/ant/DefaultLogger;->startTime:J

    return-void
.end method

.method protected getBuildFailedMessage()Ljava/lang/String;
    .locals 1

    const-string v0, "BUILD FAILED"

    return-object v0
.end method

.method protected getBuildSuccessfulMessage()Ljava/lang/String;
    .locals 1

    const-string v0, "BUILD SUCCESSFUL"

    return-object v0
.end method

.method protected log(Ljava/lang/String;)V
    .locals 0
    .param p1    # Ljava/lang/String;

    return-void
.end method

.method public messageLogged(Lorg/apache/tools/ant/BuildEvent;)V
    .locals 15
    .param p1    # Lorg/apache/tools/ant/BuildEvent;

    invoke-virtual/range {p1 .. p1}, Lorg/apache/tools/ant/BuildEvent;->getPriority()I

    move-result v9

    iget v13, p0, Lorg/apache/tools/ant/DefaultLogger;->msgOutputLevel:I

    if-gt v9, v13, :cond_3

    new-instance v6, Ljava/lang/StringBuffer;

    invoke-direct {v6}, Ljava/lang/StringBuffer;-><init>()V

    invoke-virtual/range {p1 .. p1}, Lorg/apache/tools/ant/BuildEvent;->getTask()Lorg/apache/tools/ant/Task;

    move-result-object v13

    if-eqz v13, :cond_6

    iget-boolean v13, p0, Lorg/apache/tools/ant/DefaultLogger;->emacsMode:Z

    if-nez v13, :cond_6

    invoke-virtual/range {p1 .. p1}, Lorg/apache/tools/ant/BuildEvent;->getTask()Lorg/apache/tools/ant/Task;

    move-result-object v13

    invoke-virtual {v13}, Lorg/apache/tools/ant/Task;->getTaskName()Ljava/lang/String;

    move-result-object v8

    new-instance v13, Ljava/lang/StringBuffer;

    invoke-direct {v13}, Ljava/lang/StringBuffer;-><init>()V

    const-string v14, "["

    invoke-virtual {v13, v14}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    move-result-object v13

    invoke-virtual {v13, v8}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    move-result-object v13

    const-string v14, "] "

    invoke-virtual {v13, v14}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    move-result-object v13

    invoke-virtual {v13}, Ljava/lang/StringBuffer;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/String;->length()I

    move-result v13

    rsub-int/lit8 v11, v13, 0xc

    new-instance v12, Ljava/lang/StringBuffer;

    invoke-direct {v12}, Ljava/lang/StringBuffer;-><init>()V

    const/4 v3, 0x0

    :goto_0
    if-ge v3, v11, :cond_0

    const-string v13, " "

    invoke-virtual {v12, v13}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    add-int/lit8 v3, v3, 0x1

    goto :goto_0

    :cond_0
    invoke-virtual {v12, v4}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    invoke-virtual {v12}, Ljava/lang/StringBuffer;->toString()Ljava/lang/String;

    move-result-object v4

    :try_start_0
    new-instance v10, Ljava/io/BufferedReader;

    new-instance v13, Ljava/io/StringReader;

    invoke-virtual/range {p1 .. p1}, Lorg/apache/tools/ant/BuildEvent;->getMessage()Ljava/lang/String;

    move-result-object v14

    invoke-direct {v13, v14}, Ljava/io/StringReader;-><init>(Ljava/lang/String;)V

    invoke-direct {v10, v13}, Ljava/io/BufferedReader;-><init>(Ljava/io/Reader;)V

    invoke-virtual {v10}, Ljava/io/BufferedReader;->readLine()Ljava/lang/String;

    move-result-object v5

    const/4 v2, 0x1

    :cond_1
    if-eqz v2, :cond_4

    if-nez v5, :cond_5

    invoke-virtual {v6, v4}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;
    :try_end_0
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_0

    :goto_1
    invoke-virtual/range {p1 .. p1}, Lorg/apache/tools/ant/BuildEvent;->getException()Ljava/lang/Throwable;

    move-result-object v1

    const/4 v13, 0x4

    iget v14, p0, Lorg/apache/tools/ant/DefaultLogger;->msgOutputLevel:I

    if-gt v13, v14, :cond_2

    if-eqz v1, :cond_2

    invoke-static {v1}, Lorg/apache/tools/ant/util/StringUtils;->getStackTrace(Ljava/lang/Throwable;)Ljava/lang/String;

    move-result-object v13

    invoke-virtual {v6, v13}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    :cond_2
    invoke-virtual {v6}, Ljava/lang/StringBuffer;->toString()Ljava/lang/String;

    move-result-object v7

    if-eqz v9, :cond_7

    iget-object v13, p0, Lorg/apache/tools/ant/DefaultLogger;->out:Ljava/io/PrintStream;

    invoke-virtual {p0, v7, v13, v9}, Lorg/apache/tools/ant/DefaultLogger;->printMessage(Ljava/lang/String;Ljava/io/PrintStream;I)V

    :goto_2
    invoke-virtual {p0, v7}, Lorg/apache/tools/ant/DefaultLogger;->log(Ljava/lang/String;)V

    :cond_3
    return-void

    :cond_4
    :try_start_1
    sget-object v13, Lorg/apache/tools/ant/util/StringUtils;->LINE_SEP:Ljava/lang/String;

    invoke-virtual {v6, v13}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    :cond_5
    const/4 v2, 0x0

    invoke-virtual {v6, v4}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    move-result-object v13

    invoke-virtual {v13, v5}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    invoke-virtual {v10}, Ljava/io/BufferedReader;->readLine()Ljava/lang/String;
    :try_end_1
    .catch Ljava/io/IOException; {:try_start_1 .. :try_end_1} :catch_0

    move-result-object v5

    if-nez v5, :cond_1

    goto :goto_1

    :catch_0
    move-exception v0

    invoke-virtual {v6, v4}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    move-result-object v13

    invoke-virtual/range {p1 .. p1}, Lorg/apache/tools/ant/BuildEvent;->getMessage()Ljava/lang/String;

    move-result-object v14

    invoke-virtual {v13, v14}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    goto :goto_1

    :cond_6
    invoke-virtual/range {p1 .. p1}, Lorg/apache/tools/ant/BuildEvent;->getMessage()Ljava/lang/String;

    move-result-object v13

    invoke-virtual {v6, v13}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    goto :goto_1

    :cond_7
    iget-object v13, p0, Lorg/apache/tools/ant/DefaultLogger;->err:Ljava/io/PrintStream;

    invoke-virtual {p0, v7, v13, v9}, Lorg/apache/tools/ant/DefaultLogger;->printMessage(Ljava/lang/String;Ljava/io/PrintStream;I)V

    goto :goto_2
.end method

.method protected printMessage(Ljava/lang/String;Ljava/io/PrintStream;I)V
    .locals 0
    .param p1    # Ljava/lang/String;
    .param p2    # Ljava/io/PrintStream;
    .param p3    # I

    invoke-virtual {p2, p1}, Ljava/io/PrintStream;->println(Ljava/lang/String;)V

    return-void
.end method

.method public setEmacsMode(Z)V
    .locals 0
    .param p1    # Z

    iput-boolean p1, p0, Lorg/apache/tools/ant/DefaultLogger;->emacsMode:Z

    return-void
.end method

.method public setErrorPrintStream(Ljava/io/PrintStream;)V
    .locals 2
    .param p1    # Ljava/io/PrintStream;

    new-instance v0, Ljava/io/PrintStream;

    const/4 v1, 0x1

    invoke-direct {v0, p1, v1}, Ljava/io/PrintStream;-><init>(Ljava/io/OutputStream;Z)V

    iput-object v0, p0, Lorg/apache/tools/ant/DefaultLogger;->err:Ljava/io/PrintStream;

    return-void
.end method

.method public setMessageOutputLevel(I)V
    .locals 0
    .param p1    # I

    iput p1, p0, Lorg/apache/tools/ant/DefaultLogger;->msgOutputLevel:I

    return-void
.end method

.method public setOutputPrintStream(Ljava/io/PrintStream;)V
    .locals 2
    .param p1    # Ljava/io/PrintStream;

    new-instance v0, Ljava/io/PrintStream;

    const/4 v1, 0x1

    invoke-direct {v0, p1, v1}, Ljava/io/PrintStream;-><init>(Ljava/io/OutputStream;Z)V

    iput-object v0, p0, Lorg/apache/tools/ant/DefaultLogger;->out:Ljava/io/PrintStream;

    return-void
.end method

.method public targetFinished(Lorg/apache/tools/ant/BuildEvent;)V
    .locals 0
    .param p1    # Lorg/apache/tools/ant/BuildEvent;

    return-void
.end method

.method public targetStarted(Lorg/apache/tools/ant/BuildEvent;)V
    .locals 3
    .param p1    # Lorg/apache/tools/ant/BuildEvent;

    const/4 v1, 0x2

    iget v2, p0, Lorg/apache/tools/ant/DefaultLogger;->msgOutputLevel:I

    if-gt v1, v2, :cond_0

    invoke-virtual {p1}, Lorg/apache/tools/ant/BuildEvent;->getTarget()Lorg/apache/tools/ant/Target;

    move-result-object v1

    invoke-virtual {v1}, Lorg/apache/tools/ant/Target;->getName()Ljava/lang/String;

    move-result-object v1

    const-string v2, ""

    invoke-virtual {v1, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-nez v1, :cond_0

    new-instance v1, Ljava/lang/StringBuffer;

    invoke-direct {v1}, Ljava/lang/StringBuffer;-><init>()V

    sget-object v2, Lorg/apache/tools/ant/util/StringUtils;->LINE_SEP:Ljava/lang/String;

    invoke-virtual {v1, v2}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    move-result-object v1

    invoke-virtual {p1}, Lorg/apache/tools/ant/BuildEvent;->getTarget()Lorg/apache/tools/ant/Target;

    move-result-object v2

    invoke-virtual {v2}, Lorg/apache/tools/ant/Target;->getName()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    move-result-object v1

    const-string v2, ":"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuffer;->toString()Ljava/lang/String;

    move-result-object v0

    iget-object v1, p0, Lorg/apache/tools/ant/DefaultLogger;->out:Ljava/io/PrintStream;

    invoke-virtual {p1}, Lorg/apache/tools/ant/BuildEvent;->getPriority()I

    move-result v2

    invoke-virtual {p0, v0, v1, v2}, Lorg/apache/tools/ant/DefaultLogger;->printMessage(Ljava/lang/String;Ljava/io/PrintStream;I)V

    invoke-virtual {p0, v0}, Lorg/apache/tools/ant/DefaultLogger;->log(Ljava/lang/String;)V

    :cond_0
    return-void
.end method

.method public taskFinished(Lorg/apache/tools/ant/BuildEvent;)V
    .locals 0
    .param p1    # Lorg/apache/tools/ant/BuildEvent;

    return-void
.end method

.method public taskStarted(Lorg/apache/tools/ant/BuildEvent;)V
    .locals 0
    .param p1    # Lorg/apache/tools/ant/BuildEvent;

    return-void
.end method
