.class public final Lorg/apache/tools/ant/IntrospectionHelper;
.super Ljava/lang/Object;
.source "IntrospectionHelper.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lorg/apache/tools/ant/IntrospectionHelper$AttributeSetter;,
        Lorg/apache/tools/ant/IntrospectionHelper$AddNestedCreator;,
        Lorg/apache/tools/ant/IntrospectionHelper$CreateNestedCreator;,
        Lorg/apache/tools/ant/IntrospectionHelper$NestedCreator;,
        Lorg/apache/tools/ant/IntrospectionHelper$Creator;
    }
.end annotation


# static fields
.field private static final ELLIPSIS:Ljava/lang/String; = "..."

.field private static final EMPTY_MAP:Ljava/util/Map;

.field private static final HELPERS:Ljava/util/Map;

.field private static final MAX_REPORT_NESTED_TEXT:I = 0x14

.field private static final PRIMITIVE_TYPE_MAP:Ljava/util/Map;

.field static class$java$io$File:Ljava/lang/Class;

.field static class$java$lang$Boolean:Ljava/lang/Class;

.field static class$java$lang$Byte:Ljava/lang/Class;

.field static class$java$lang$Character:Ljava/lang/Class;

.field static class$java$lang$Class:Ljava/lang/Class;

.field static class$java$lang$Double:Ljava/lang/Class;

.field static class$java$lang$Float:Ljava/lang/Class;

.field static class$java$lang$Integer:Ljava/lang/Class;

.field static class$java$lang$Long:Ljava/lang/Class;

.field static class$java$lang$Short:Ljava/lang/Class;

.field static class$java$lang$String:Ljava/lang/Class;

.field static class$org$apache$tools$ant$DynamicElement:Ljava/lang/Class;

.field static class$org$apache$tools$ant$DynamicElementNS:Ljava/lang/Class;

.field static class$org$apache$tools$ant$Location:Ljava/lang/Class;

.field static class$org$apache$tools$ant$Project:Ljava/lang/Class;

.field static class$org$apache$tools$ant$ProjectComponent:Ljava/lang/Class;

.field static class$org$apache$tools$ant$Task:Ljava/lang/Class;

.field static class$org$apache$tools$ant$TaskContainer:Ljava/lang/Class;

.field static class$org$apache$tools$ant$types$EnumeratedAttribute:Ljava/lang/Class;


# instance fields
.field private addText:Ljava/lang/reflect/Method;

.field private addTypeMethods:Ljava/util/List;

.field private attributeSetters:Ljava/util/Hashtable;

.field private attributeTypes:Ljava/util/Hashtable;

.field private bean:Ljava/lang/Class;

.field private nestedCreators:Ljava/util/Hashtable;

.field private nestedTypes:Ljava/util/Hashtable;


# direct methods
.method static constructor <clinit>()V
    .locals 10

    const/4 v9, 0x3

    const/4 v8, 0x2

    const/4 v7, 0x1

    const/16 v6, 0x8

    const/4 v5, 0x0

    new-instance v3, Ljava/util/HashMap;

    invoke-direct {v3, v5}, Ljava/util/HashMap;-><init>(I)V

    invoke-static {v3}, Ljava/util/Collections;->unmodifiableMap(Ljava/util/Map;)Ljava/util/Map;

    move-result-object v3

    sput-object v3, Lorg/apache/tools/ant/IntrospectionHelper;->EMPTY_MAP:Ljava/util/Map;

    new-instance v3, Ljava/util/Hashtable;

    invoke-direct {v3}, Ljava/util/Hashtable;-><init>()V

    sput-object v3, Lorg/apache/tools/ant/IntrospectionHelper;->HELPERS:Ljava/util/Map;

    new-instance v3, Ljava/util/HashMap;

    invoke-direct {v3, v6}, Ljava/util/HashMap;-><init>(I)V

    sput-object v3, Lorg/apache/tools/ant/IntrospectionHelper;->PRIMITIVE_TYPE_MAP:Ljava/util/Map;

    new-array v1, v6, [Ljava/lang/Class;

    sget-object v3, Ljava/lang/Boolean;->TYPE:Ljava/lang/Class;

    aput-object v3, v1, v5

    sget-object v3, Ljava/lang/Byte;->TYPE:Ljava/lang/Class;

    aput-object v3, v1, v7

    sget-object v3, Ljava/lang/Character;->TYPE:Ljava/lang/Class;

    aput-object v3, v1, v8

    sget-object v3, Ljava/lang/Short;->TYPE:Ljava/lang/Class;

    aput-object v3, v1, v9

    const/4 v3, 0x4

    sget-object v4, Ljava/lang/Integer;->TYPE:Ljava/lang/Class;

    aput-object v4, v1, v3

    const/4 v3, 0x5

    sget-object v4, Ljava/lang/Long;->TYPE:Ljava/lang/Class;

    aput-object v4, v1, v3

    const/4 v3, 0x6

    sget-object v4, Ljava/lang/Float;->TYPE:Ljava/lang/Class;

    aput-object v4, v1, v3

    const/4 v3, 0x7

    sget-object v4, Ljava/lang/Double;->TYPE:Ljava/lang/Class;

    aput-object v4, v1, v3

    new-array v2, v6, [Ljava/lang/Class;

    sget-object v3, Lorg/apache/tools/ant/IntrospectionHelper;->class$java$lang$Boolean:Ljava/lang/Class;

    if-nez v3, :cond_0

    const-string v3, "java.lang.Boolean"

    invoke-static {v3}, Lorg/apache/tools/ant/IntrospectionHelper;->class$(Ljava/lang/String;)Ljava/lang/Class;

    move-result-object v3

    sput-object v3, Lorg/apache/tools/ant/IntrospectionHelper;->class$java$lang$Boolean:Ljava/lang/Class;

    :goto_0
    aput-object v3, v2, v5

    sget-object v3, Lorg/apache/tools/ant/IntrospectionHelper;->class$java$lang$Byte:Ljava/lang/Class;

    if-nez v3, :cond_1

    const-string v3, "java.lang.Byte"

    invoke-static {v3}, Lorg/apache/tools/ant/IntrospectionHelper;->class$(Ljava/lang/String;)Ljava/lang/Class;

    move-result-object v3

    sput-object v3, Lorg/apache/tools/ant/IntrospectionHelper;->class$java$lang$Byte:Ljava/lang/Class;

    :goto_1
    aput-object v3, v2, v7

    sget-object v3, Lorg/apache/tools/ant/IntrospectionHelper;->class$java$lang$Character:Ljava/lang/Class;

    if-nez v3, :cond_2

    const-string v3, "java.lang.Character"

    invoke-static {v3}, Lorg/apache/tools/ant/IntrospectionHelper;->class$(Ljava/lang/String;)Ljava/lang/Class;

    move-result-object v3

    sput-object v3, Lorg/apache/tools/ant/IntrospectionHelper;->class$java$lang$Character:Ljava/lang/Class;

    :goto_2
    aput-object v3, v2, v8

    sget-object v3, Lorg/apache/tools/ant/IntrospectionHelper;->class$java$lang$Short:Ljava/lang/Class;

    if-nez v3, :cond_3

    const-string v3, "java.lang.Short"

    invoke-static {v3}, Lorg/apache/tools/ant/IntrospectionHelper;->class$(Ljava/lang/String;)Ljava/lang/Class;

    move-result-object v3

    sput-object v3, Lorg/apache/tools/ant/IntrospectionHelper;->class$java$lang$Short:Ljava/lang/Class;

    :goto_3
    aput-object v3, v2, v9

    const/4 v4, 0x4

    sget-object v3, Lorg/apache/tools/ant/IntrospectionHelper;->class$java$lang$Integer:Ljava/lang/Class;

    if-nez v3, :cond_4

    const-string v3, "java.lang.Integer"

    invoke-static {v3}, Lorg/apache/tools/ant/IntrospectionHelper;->class$(Ljava/lang/String;)Ljava/lang/Class;

    move-result-object v3

    sput-object v3, Lorg/apache/tools/ant/IntrospectionHelper;->class$java$lang$Integer:Ljava/lang/Class;

    :goto_4
    aput-object v3, v2, v4

    const/4 v4, 0x5

    sget-object v3, Lorg/apache/tools/ant/IntrospectionHelper;->class$java$lang$Long:Ljava/lang/Class;

    if-nez v3, :cond_5

    const-string v3, "java.lang.Long"

    invoke-static {v3}, Lorg/apache/tools/ant/IntrospectionHelper;->class$(Ljava/lang/String;)Ljava/lang/Class;

    move-result-object v3

    sput-object v3, Lorg/apache/tools/ant/IntrospectionHelper;->class$java$lang$Long:Ljava/lang/Class;

    :goto_5
    aput-object v3, v2, v4

    const/4 v4, 0x6

    sget-object v3, Lorg/apache/tools/ant/IntrospectionHelper;->class$java$lang$Float:Ljava/lang/Class;

    if-nez v3, :cond_6

    const-string v3, "java.lang.Float"

    invoke-static {v3}, Lorg/apache/tools/ant/IntrospectionHelper;->class$(Ljava/lang/String;)Ljava/lang/Class;

    move-result-object v3

    sput-object v3, Lorg/apache/tools/ant/IntrospectionHelper;->class$java$lang$Float:Ljava/lang/Class;

    :goto_6
    aput-object v3, v2, v4

    const/4 v4, 0x7

    sget-object v3, Lorg/apache/tools/ant/IntrospectionHelper;->class$java$lang$Double:Ljava/lang/Class;

    if-nez v3, :cond_7

    const-string v3, "java.lang.Double"

    invoke-static {v3}, Lorg/apache/tools/ant/IntrospectionHelper;->class$(Ljava/lang/String;)Ljava/lang/Class;

    move-result-object v3

    sput-object v3, Lorg/apache/tools/ant/IntrospectionHelper;->class$java$lang$Double:Ljava/lang/Class;

    :goto_7
    aput-object v3, v2, v4

    const/4 v0, 0x0

    :goto_8
    array-length v3, v1

    if-ge v0, v3, :cond_8

    sget-object v3, Lorg/apache/tools/ant/IntrospectionHelper;->PRIMITIVE_TYPE_MAP:Ljava/util/Map;

    aget-object v4, v1, v0

    aget-object v5, v2, v0

    invoke-interface {v3, v4, v5}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    add-int/lit8 v0, v0, 0x1

    goto :goto_8

    :cond_0
    sget-object v3, Lorg/apache/tools/ant/IntrospectionHelper;->class$java$lang$Boolean:Ljava/lang/Class;

    goto :goto_0

    :cond_1
    sget-object v3, Lorg/apache/tools/ant/IntrospectionHelper;->class$java$lang$Byte:Ljava/lang/Class;

    goto :goto_1

    :cond_2
    sget-object v3, Lorg/apache/tools/ant/IntrospectionHelper;->class$java$lang$Character:Ljava/lang/Class;

    goto :goto_2

    :cond_3
    sget-object v3, Lorg/apache/tools/ant/IntrospectionHelper;->class$java$lang$Short:Ljava/lang/Class;

    goto :goto_3

    :cond_4
    sget-object v3, Lorg/apache/tools/ant/IntrospectionHelper;->class$java$lang$Integer:Ljava/lang/Class;

    goto :goto_4

    :cond_5
    sget-object v3, Lorg/apache/tools/ant/IntrospectionHelper;->class$java$lang$Long:Ljava/lang/Class;

    goto :goto_5

    :cond_6
    sget-object v3, Lorg/apache/tools/ant/IntrospectionHelper;->class$java$lang$Float:Ljava/lang/Class;

    goto :goto_6

    :cond_7
    sget-object v3, Lorg/apache/tools/ant/IntrospectionHelper;->class$java$lang$Double:Ljava/lang/Class;

    goto :goto_7

    :cond_8
    return-void
.end method

.method private constructor <init>(Ljava/lang/Class;)V
    .locals 14
    .param p1    # Ljava/lang/Class;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    new-instance v10, Ljava/util/Hashtable;

    invoke-direct {v10}, Ljava/util/Hashtable;-><init>()V

    iput-object v10, p0, Lorg/apache/tools/ant/IntrospectionHelper;->attributeTypes:Ljava/util/Hashtable;

    new-instance v10, Ljava/util/Hashtable;

    invoke-direct {v10}, Ljava/util/Hashtable;-><init>()V

    iput-object v10, p0, Lorg/apache/tools/ant/IntrospectionHelper;->attributeSetters:Ljava/util/Hashtable;

    new-instance v10, Ljava/util/Hashtable;

    invoke-direct {v10}, Ljava/util/Hashtable;-><init>()V

    iput-object v10, p0, Lorg/apache/tools/ant/IntrospectionHelper;->nestedTypes:Ljava/util/Hashtable;

    new-instance v10, Ljava/util/Hashtable;

    invoke-direct {v10}, Ljava/util/Hashtable;-><init>()V

    iput-object v10, p0, Lorg/apache/tools/ant/IntrospectionHelper;->nestedCreators:Ljava/util/Hashtable;

    new-instance v10, Ljava/util/ArrayList;

    invoke-direct {v10}, Ljava/util/ArrayList;-><init>()V

    iput-object v10, p0, Lorg/apache/tools/ant/IntrospectionHelper;->addTypeMethods:Ljava/util/List;

    const/4 v10, 0x0

    iput-object v10, p0, Lorg/apache/tools/ant/IntrospectionHelper;->addText:Ljava/lang/reflect/Method;

    iput-object p1, p0, Lorg/apache/tools/ant/IntrospectionHelper;->bean:Ljava/lang/Class;

    invoke-virtual {p1}, Ljava/lang/Class;->getMethods()[Ljava/lang/reflect/Method;

    move-result-object v6

    const/4 v4, 0x0

    :goto_0
    array-length v10, v6

    if-ge v4, v10, :cond_12

    aget-object v5, v6, v4

    invoke-virtual {v5}, Ljava/lang/reflect/Method;->getName()Ljava/lang/String;

    move-result-object v7

    invoke-virtual {v5}, Ljava/lang/reflect/Method;->getReturnType()Ljava/lang/Class;

    move-result-object v9

    invoke-virtual {v5}, Ljava/lang/reflect/Method;->getParameterTypes()[Ljava/lang/Class;

    move-result-object v0

    array-length v10, v0

    const/4 v11, 0x1

    if-ne v10, v11, :cond_2

    sget-object v10, Ljava/lang/Void;->TYPE:Ljava/lang/Class;

    invoke-virtual {v10, v9}, Ljava/lang/Object;->equals(Ljava/lang/Object;)Z

    move-result v10

    if-eqz v10, :cond_2

    const-string v10, "add"

    invoke-virtual {v10, v7}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v10

    if-nez v10, :cond_0

    const-string v10, "addConfigured"

    invoke-virtual {v10, v7}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v10

    if-eqz v10, :cond_2

    :cond_0
    invoke-direct {p0, v5}, Lorg/apache/tools/ant/IntrospectionHelper;->insertAddTypeMethod(Ljava/lang/reflect/Method;)V

    :cond_1
    :goto_1
    add-int/lit8 v4, v4, 0x1

    goto :goto_0

    :cond_2
    sget-object v10, Lorg/apache/tools/ant/IntrospectionHelper;->class$org$apache$tools$ant$ProjectComponent:Ljava/lang/Class;

    if-nez v10, :cond_5

    const-string v10, "org.apache.tools.ant.ProjectComponent"

    invoke-static {v10}, Lorg/apache/tools/ant/IntrospectionHelper;->class$(Ljava/lang/String;)Ljava/lang/Class;

    move-result-object v10

    sput-object v10, Lorg/apache/tools/ant/IntrospectionHelper;->class$org$apache$tools$ant$ProjectComponent:Ljava/lang/Class;

    :goto_2
    invoke-virtual {v10, p1}, Ljava/lang/Class;->isAssignableFrom(Ljava/lang/Class;)Z

    move-result v10

    if-eqz v10, :cond_3

    array-length v10, v0

    const/4 v11, 0x1

    if-ne v10, v11, :cond_3

    const/4 v10, 0x0

    aget-object v10, v0, v10

    invoke-direct {p0, v7, v10}, Lorg/apache/tools/ant/IntrospectionHelper;->isHiddenSetMethod(Ljava/lang/String;Ljava/lang/Class;)Z

    move-result v10

    if-nez v10, :cond_1

    :cond_3
    invoke-virtual {p0}, Lorg/apache/tools/ant/IntrospectionHelper;->isContainer()Z

    move-result v10

    if-eqz v10, :cond_4

    array-length v10, v0

    const/4 v11, 0x1

    if-ne v10, v11, :cond_4

    const-string v10, "addTask"

    invoke-virtual {v10, v7}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v10

    if-eqz v10, :cond_4

    sget-object v10, Lorg/apache/tools/ant/IntrospectionHelper;->class$org$apache$tools$ant$Task:Ljava/lang/Class;

    if-nez v10, :cond_6

    const-string v10, "org.apache.tools.ant.Task"

    invoke-static {v10}, Lorg/apache/tools/ant/IntrospectionHelper;->class$(Ljava/lang/String;)Ljava/lang/Class;

    move-result-object v10

    sput-object v10, Lorg/apache/tools/ant/IntrospectionHelper;->class$org$apache$tools$ant$Task:Ljava/lang/Class;

    :goto_3
    const/4 v11, 0x0

    aget-object v11, v0, v11

    invoke-virtual {v10, v11}, Ljava/lang/Object;->equals(Ljava/lang/Object;)Z

    move-result v10

    if-nez v10, :cond_1

    :cond_4
    const-string v10, "addText"

    invoke-virtual {v10, v7}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v10

    if-eqz v10, :cond_8

    sget-object v10, Ljava/lang/Void;->TYPE:Ljava/lang/Class;

    invoke-virtual {v10, v9}, Ljava/lang/Object;->equals(Ljava/lang/Object;)Z

    move-result v10

    if-eqz v10, :cond_8

    array-length v10, v0

    const/4 v11, 0x1

    if-ne v10, v11, :cond_8

    sget-object v10, Lorg/apache/tools/ant/IntrospectionHelper;->class$java$lang$String:Ljava/lang/Class;

    if-nez v10, :cond_7

    const-string v10, "java.lang.String"

    invoke-static {v10}, Lorg/apache/tools/ant/IntrospectionHelper;->class$(Ljava/lang/String;)Ljava/lang/Class;

    move-result-object v10

    sput-object v10, Lorg/apache/tools/ant/IntrospectionHelper;->class$java$lang$String:Ljava/lang/Class;

    :goto_4
    const/4 v11, 0x0

    aget-object v11, v0, v11

    invoke-virtual {v10, v11}, Ljava/lang/Object;->equals(Ljava/lang/Object;)Z

    move-result v10

    if-eqz v10, :cond_8

    aget-object v10, v6, v4

    iput-object v10, p0, Lorg/apache/tools/ant/IntrospectionHelper;->addText:Ljava/lang/reflect/Method;

    goto :goto_1

    :cond_5
    sget-object v10, Lorg/apache/tools/ant/IntrospectionHelper;->class$org$apache$tools$ant$ProjectComponent:Ljava/lang/Class;

    goto :goto_2

    :cond_6
    sget-object v10, Lorg/apache/tools/ant/IntrospectionHelper;->class$org$apache$tools$ant$Task:Ljava/lang/Class;

    goto :goto_3

    :cond_7
    sget-object v10, Lorg/apache/tools/ant/IntrospectionHelper;->class$java$lang$String:Ljava/lang/Class;

    goto :goto_4

    :cond_8
    const-string v10, "set"

    invoke-virtual {v7, v10}, Ljava/lang/String;->startsWith(Ljava/lang/String;)Z

    move-result v10

    if-eqz v10, :cond_b

    sget-object v10, Ljava/lang/Void;->TYPE:Ljava/lang/Class;

    invoke-virtual {v10, v9}, Ljava/lang/Object;->equals(Ljava/lang/Object;)Z

    move-result v10

    if-eqz v10, :cond_b

    array-length v10, v0

    const/4 v11, 0x1

    if-ne v10, v11, :cond_b

    const/4 v10, 0x0

    aget-object v10, v0, v10

    invoke-virtual {v10}, Ljava/lang/Class;->isArray()Z

    move-result v10

    if-nez v10, :cond_b

    const-string v10, "set"

    invoke-direct {p0, v7, v10}, Lorg/apache/tools/ant/IntrospectionHelper;->getPropertyName(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v8

    iget-object v10, p0, Lorg/apache/tools/ant/IntrospectionHelper;->attributeSetters:Ljava/util/Hashtable;

    invoke-virtual {v10, v8}, Ljava/util/Hashtable;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v10

    if-eqz v10, :cond_9

    sget-object v10, Lorg/apache/tools/ant/IntrospectionHelper;->class$java$lang$String:Ljava/lang/Class;

    if-nez v10, :cond_a

    const-string v10, "java.lang.String"

    invoke-static {v10}, Lorg/apache/tools/ant/IntrospectionHelper;->class$(Ljava/lang/String;)Ljava/lang/Class;

    move-result-object v10

    sput-object v10, Lorg/apache/tools/ant/IntrospectionHelper;->class$java$lang$String:Ljava/lang/Class;

    :goto_5
    const/4 v11, 0x0

    aget-object v11, v0, v11

    invoke-virtual {v10, v11}, Ljava/lang/Object;->equals(Ljava/lang/Object;)Z

    move-result v10

    if-nez v10, :cond_1

    :cond_9
    const/4 v10, 0x0

    aget-object v10, v0, v10

    invoke-direct {p0, v5, v10, v8}, Lorg/apache/tools/ant/IntrospectionHelper;->createAttributeSetter(Ljava/lang/reflect/Method;Ljava/lang/Class;Ljava/lang/String;)Lorg/apache/tools/ant/IntrospectionHelper$AttributeSetter;

    move-result-object v1

    if-eqz v1, :cond_1

    iget-object v10, p0, Lorg/apache/tools/ant/IntrospectionHelper;->attributeTypes:Ljava/util/Hashtable;

    const/4 v11, 0x0

    aget-object v11, v0, v11

    invoke-virtual {v10, v8, v11}, Ljava/util/Hashtable;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    iget-object v10, p0, Lorg/apache/tools/ant/IntrospectionHelper;->attributeSetters:Ljava/util/Hashtable;

    invoke-virtual {v10, v8, v1}, Ljava/util/Hashtable;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    goto/16 :goto_1

    :cond_a
    sget-object v10, Lorg/apache/tools/ant/IntrospectionHelper;->class$java$lang$String:Ljava/lang/Class;

    goto :goto_5

    :cond_b
    const-string v10, "create"

    invoke-virtual {v7, v10}, Ljava/lang/String;->startsWith(Ljava/lang/String;)Z

    move-result v10

    if-eqz v10, :cond_c

    invoke-virtual {v9}, Ljava/lang/Class;->isArray()Z

    move-result v10

    if-nez v10, :cond_c

    invoke-virtual {v9}, Ljava/lang/Class;->isPrimitive()Z

    move-result v10

    if-nez v10, :cond_c

    array-length v10, v0

    if-nez v10, :cond_c

    const-string v10, "create"

    invoke-direct {p0, v7, v10}, Lorg/apache/tools/ant/IntrospectionHelper;->getPropertyName(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v8

    iget-object v10, p0, Lorg/apache/tools/ant/IntrospectionHelper;->nestedCreators:Ljava/util/Hashtable;

    invoke-virtual {v10, v8}, Ljava/util/Hashtable;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v10

    if-nez v10, :cond_1

    iget-object v10, p0, Lorg/apache/tools/ant/IntrospectionHelper;->nestedTypes:Ljava/util/Hashtable;

    invoke-virtual {v10, v8, v9}, Ljava/util/Hashtable;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    iget-object v10, p0, Lorg/apache/tools/ant/IntrospectionHelper;->nestedCreators:Ljava/util/Hashtable;

    new-instance v11, Lorg/apache/tools/ant/IntrospectionHelper$CreateNestedCreator;

    invoke-direct {v11, p0, v5}, Lorg/apache/tools/ant/IntrospectionHelper$CreateNestedCreator;-><init>(Lorg/apache/tools/ant/IntrospectionHelper;Ljava/lang/reflect/Method;)V

    invoke-virtual {v10, v8, v11}, Ljava/util/Hashtable;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    goto/16 :goto_1

    :cond_c
    const-string v10, "addConfigured"

    invoke-virtual {v7, v10}, Ljava/lang/String;->startsWith(Ljava/lang/String;)Z

    move-result v10

    if-eqz v10, :cond_f

    sget-object v10, Ljava/lang/Void;->TYPE:Ljava/lang/Class;

    invoke-virtual {v10, v9}, Ljava/lang/Object;->equals(Ljava/lang/Object;)Z

    move-result v10

    if-eqz v10, :cond_f

    array-length v10, v0

    const/4 v11, 0x1

    if-ne v10, v11, :cond_f

    sget-object v10, Lorg/apache/tools/ant/IntrospectionHelper;->class$java$lang$String:Ljava/lang/Class;

    if-nez v10, :cond_d

    const-string v10, "java.lang.String"

    invoke-static {v10}, Lorg/apache/tools/ant/IntrospectionHelper;->class$(Ljava/lang/String;)Ljava/lang/Class;

    move-result-object v10

    sput-object v10, Lorg/apache/tools/ant/IntrospectionHelper;->class$java$lang$String:Ljava/lang/Class;

    :goto_6
    const/4 v11, 0x0

    aget-object v11, v0, v11

    invoke-virtual {v10, v11}, Ljava/lang/Object;->equals(Ljava/lang/Object;)Z

    move-result v10

    if-nez v10, :cond_f

    const/4 v10, 0x0

    aget-object v10, v0, v10

    invoke-virtual {v10}, Ljava/lang/Class;->isArray()Z

    move-result v10

    if-nez v10, :cond_f

    const/4 v10, 0x0

    aget-object v10, v0, v10

    invoke-virtual {v10}, Ljava/lang/Class;->isPrimitive()Z

    move-result v10

    if-nez v10, :cond_f

    const/4 v2, 0x0

    const/4 v10, 0x0

    :try_start_0
    aget-object v10, v0, v10

    const/4 v11, 0x0

    new-array v11, v11, [Ljava/lang/Class;

    invoke-virtual {v10, v11}, Ljava/lang/Class;->getConstructor([Ljava/lang/Class;)Ljava/lang/reflect/Constructor;
    :try_end_0
    .catch Ljava/lang/NoSuchMethodException; {:try_start_0 .. :try_end_0} :catch_1

    move-result-object v2

    :goto_7
    :try_start_1
    const-string v10, "addConfigured"

    invoke-direct {p0, v7, v10}, Lorg/apache/tools/ant/IntrospectionHelper;->getPropertyName(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v8

    iget-object v10, p0, Lorg/apache/tools/ant/IntrospectionHelper;->nestedTypes:Ljava/util/Hashtable;

    const/4 v11, 0x0

    aget-object v11, v0, v11

    invoke-virtual {v10, v8, v11}, Ljava/util/Hashtable;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    iget-object v10, p0, Lorg/apache/tools/ant/IntrospectionHelper;->nestedCreators:Ljava/util/Hashtable;

    new-instance v11, Lorg/apache/tools/ant/IntrospectionHelper$AddNestedCreator;

    const/4 v12, 0x2

    invoke-direct {v11, p0, v5, v2, v12}, Lorg/apache/tools/ant/IntrospectionHelper$AddNestedCreator;-><init>(Lorg/apache/tools/ant/IntrospectionHelper;Ljava/lang/reflect/Method;Ljava/lang/reflect/Constructor;I)V

    invoke-virtual {v10, v8, v11}, Ljava/util/Hashtable;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;
    :try_end_1
    .catch Ljava/lang/NoSuchMethodException; {:try_start_1 .. :try_end_1} :catch_0

    goto/16 :goto_1

    :catch_0
    move-exception v10

    goto/16 :goto_1

    :cond_d
    sget-object v10, Lorg/apache/tools/ant/IntrospectionHelper;->class$java$lang$String:Ljava/lang/Class;

    goto :goto_6

    :catch_1
    move-exception v3

    const/4 v10, 0x0

    :try_start_2
    aget-object v11, v0, v10

    const/4 v10, 0x1

    new-array v12, v10, [Ljava/lang/Class;

    const/4 v13, 0x0

    sget-object v10, Lorg/apache/tools/ant/IntrospectionHelper;->class$org$apache$tools$ant$Project:Ljava/lang/Class;

    if-nez v10, :cond_e

    const-string v10, "org.apache.tools.ant.Project"

    invoke-static {v10}, Lorg/apache/tools/ant/IntrospectionHelper;->class$(Ljava/lang/String;)Ljava/lang/Class;

    move-result-object v10

    sput-object v10, Lorg/apache/tools/ant/IntrospectionHelper;->class$org$apache$tools$ant$Project:Ljava/lang/Class;

    :goto_8
    aput-object v10, v12, v13

    invoke-virtual {v11, v12}, Ljava/lang/Class;->getConstructor([Ljava/lang/Class;)Ljava/lang/reflect/Constructor;

    move-result-object v2

    goto :goto_7

    :cond_e
    sget-object v10, Lorg/apache/tools/ant/IntrospectionHelper;->class$org$apache$tools$ant$Project:Ljava/lang/Class;
    :try_end_2
    .catch Ljava/lang/NoSuchMethodException; {:try_start_2 .. :try_end_2} :catch_0

    goto :goto_8

    :cond_f
    const-string v10, "add"

    invoke-virtual {v7, v10}, Ljava/lang/String;->startsWith(Ljava/lang/String;)Z

    move-result v10

    if-eqz v10, :cond_1

    sget-object v10, Ljava/lang/Void;->TYPE:Ljava/lang/Class;

    invoke-virtual {v10, v9}, Ljava/lang/Object;->equals(Ljava/lang/Object;)Z

    move-result v10

    if-eqz v10, :cond_1

    array-length v10, v0

    const/4 v11, 0x1

    if-ne v10, v11, :cond_1

    sget-object v10, Lorg/apache/tools/ant/IntrospectionHelper;->class$java$lang$String:Ljava/lang/Class;

    if-nez v10, :cond_10

    const-string v10, "java.lang.String"

    invoke-static {v10}, Lorg/apache/tools/ant/IntrospectionHelper;->class$(Ljava/lang/String;)Ljava/lang/Class;

    move-result-object v10

    sput-object v10, Lorg/apache/tools/ant/IntrospectionHelper;->class$java$lang$String:Ljava/lang/Class;

    :goto_9
    const/4 v11, 0x0

    aget-object v11, v0, v11

    invoke-virtual {v10, v11}, Ljava/lang/Object;->equals(Ljava/lang/Object;)Z

    move-result v10

    if-nez v10, :cond_1

    const/4 v10, 0x0

    aget-object v10, v0, v10

    invoke-virtual {v10}, Ljava/lang/Class;->isArray()Z

    move-result v10

    if-nez v10, :cond_1

    const/4 v10, 0x0

    aget-object v10, v0, v10

    invoke-virtual {v10}, Ljava/lang/Class;->isPrimitive()Z

    move-result v10

    if-nez v10, :cond_1

    const/4 v2, 0x0

    const/4 v10, 0x0

    :try_start_3
    aget-object v10, v0, v10

    const/4 v11, 0x0

    new-array v11, v11, [Ljava/lang/Class;

    invoke-virtual {v10, v11}, Ljava/lang/Class;->getConstructor([Ljava/lang/Class;)Ljava/lang/reflect/Constructor;
    :try_end_3
    .catch Ljava/lang/NoSuchMethodException; {:try_start_3 .. :try_end_3} :catch_3

    move-result-object v2

    :goto_a
    :try_start_4
    const-string v10, "add"

    invoke-direct {p0, v7, v10}, Lorg/apache/tools/ant/IntrospectionHelper;->getPropertyName(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v8

    iget-object v10, p0, Lorg/apache/tools/ant/IntrospectionHelper;->nestedTypes:Ljava/util/Hashtable;

    invoke-virtual {v10, v8}, Ljava/util/Hashtable;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v10

    if-nez v10, :cond_1

    iget-object v10, p0, Lorg/apache/tools/ant/IntrospectionHelper;->nestedTypes:Ljava/util/Hashtable;

    const/4 v11, 0x0

    aget-object v11, v0, v11

    invoke-virtual {v10, v8, v11}, Ljava/util/Hashtable;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    iget-object v10, p0, Lorg/apache/tools/ant/IntrospectionHelper;->nestedCreators:Ljava/util/Hashtable;

    new-instance v11, Lorg/apache/tools/ant/IntrospectionHelper$AddNestedCreator;

    const/4 v12, 0x1

    invoke-direct {v11, p0, v5, v2, v12}, Lorg/apache/tools/ant/IntrospectionHelper$AddNestedCreator;-><init>(Lorg/apache/tools/ant/IntrospectionHelper;Ljava/lang/reflect/Method;Ljava/lang/reflect/Constructor;I)V

    invoke-virtual {v10, v8, v11}, Ljava/util/Hashtable;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;
    :try_end_4
    .catch Ljava/lang/NoSuchMethodException; {:try_start_4 .. :try_end_4} :catch_2

    goto/16 :goto_1

    :catch_2
    move-exception v10

    goto/16 :goto_1

    :cond_10
    sget-object v10, Lorg/apache/tools/ant/IntrospectionHelper;->class$java$lang$String:Ljava/lang/Class;

    goto :goto_9

    :catch_3
    move-exception v3

    const/4 v10, 0x0

    :try_start_5
    aget-object v11, v0, v10

    const/4 v10, 0x1

    new-array v12, v10, [Ljava/lang/Class;

    const/4 v13, 0x0

    sget-object v10, Lorg/apache/tools/ant/IntrospectionHelper;->class$org$apache$tools$ant$Project:Ljava/lang/Class;

    if-nez v10, :cond_11

    const-string v10, "org.apache.tools.ant.Project"

    invoke-static {v10}, Lorg/apache/tools/ant/IntrospectionHelper;->class$(Ljava/lang/String;)Ljava/lang/Class;

    move-result-object v10

    sput-object v10, Lorg/apache/tools/ant/IntrospectionHelper;->class$org$apache$tools$ant$Project:Ljava/lang/Class;

    :goto_b
    aput-object v10, v12, v13

    invoke-virtual {v11, v12}, Ljava/lang/Class;->getConstructor([Ljava/lang/Class;)Ljava/lang/reflect/Constructor;

    move-result-object v2

    goto :goto_a

    :cond_11
    sget-object v10, Lorg/apache/tools/ant/IntrospectionHelper;->class$org$apache$tools$ant$Project:Ljava/lang/Class;
    :try_end_5
    .catch Ljava/lang/NoSuchMethodException; {:try_start_5 .. :try_end_5} :catch_2

    goto :goto_b

    :cond_12
    return-void
.end method

.method static class$(Ljava/lang/String;)Ljava/lang/Class;
    .locals 3
    .param p0    # Ljava/lang/String;

    :try_start_0
    invoke-static {p0}, Ljava/lang/Class;->forName(Ljava/lang/String;)Ljava/lang/Class;
    :try_end_0
    .catch Ljava/lang/ClassNotFoundException; {:try_start_0 .. :try_end_0} :catch_0

    move-result-object v1

    return-object v1

    :catch_0
    move-exception v0

    new-instance v1, Ljava/lang/NoClassDefFoundError;

    invoke-virtual {v0}, Ljava/lang/ClassNotFoundException;->getMessage()Ljava/lang/String;

    move-result-object v2

    invoke-direct {v1, v2}, Ljava/lang/NoClassDefFoundError;-><init>(Ljava/lang/String;)V

    throw v1
.end method

.method public static clearCache()V
    .locals 1

    sget-object v0, Lorg/apache/tools/ant/IntrospectionHelper;->HELPERS:Ljava/util/Map;

    invoke-interface {v0}, Ljava/util/Map;->clear()V

    return-void
.end method

.method private condenseText(Ljava/lang/String;)Ljava/lang/String;
    .locals 4
    .param p1    # Ljava/lang/String;

    invoke-virtual {p1}, Ljava/lang/String;->length()I

    move-result v1

    const/16 v2, 0x14

    if-gt v1, v2, :cond_0

    :goto_0
    return-object p1

    :cond_0
    const-string v1, "..."

    invoke-virtual {v1}, Ljava/lang/String;->length()I

    move-result v1

    rsub-int/lit8 v1, v1, 0x14

    div-int/lit8 v0, v1, 0x2

    new-instance v1, Ljava/lang/StringBuffer;

    invoke-direct {v1, p1}, Ljava/lang/StringBuffer;-><init>(Ljava/lang/String;)V

    invoke-virtual {p1}, Ljava/lang/String;->length()I

    move-result v2

    sub-int/2addr v2, v0

    const-string v3, "..."

    invoke-virtual {v1, v0, v2, v3}, Ljava/lang/StringBuffer;->replace(IILjava/lang/String;)Ljava/lang/StringBuffer;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuffer;->toString()Ljava/lang/String;

    move-result-object p1

    goto :goto_0
.end method

.method private createAddTypeCreator(Lorg/apache/tools/ant/Project;Ljava/lang/Object;Ljava/lang/String;)Lorg/apache/tools/ant/IntrospectionHelper$NestedCreator;
    .locals 9
    .param p1    # Lorg/apache/tools/ant/Project;
    .param p2    # Ljava/lang/Object;
    .param p3    # Ljava/lang/String;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lorg/apache/tools/ant/BuildException;
        }
    .end annotation

    const/4 v7, 0x0

    iget-object v8, p0, Lorg/apache/tools/ant/IntrospectionHelper;->addTypeMethods:Ljava/util/List;

    invoke-interface {v8}, Ljava/util/List;->size()I

    move-result v8

    if-nez v8, :cond_1

    :cond_0
    :goto_0
    return-object v7

    :cond_1
    invoke-static {p1}, Lorg/apache/tools/ant/ComponentHelper;->getComponentHelper(Lorg/apache/tools/ant/Project;)Lorg/apache/tools/ant/ComponentHelper;

    move-result-object v3

    const/4 v1, 0x0

    const/4 v0, 0x0

    invoke-virtual {v3, p3}, Lorg/apache/tools/ant/ComponentHelper;->getComponentClass(Ljava/lang/String;)Ljava/lang/Class;

    move-result-object v2

    if-eqz v2, :cond_0

    iget-object v8, p0, Lorg/apache/tools/ant/IntrospectionHelper;->addTypeMethods:Ljava/util/List;

    invoke-direct {p0, v2, v8}, Lorg/apache/tools/ant/IntrospectionHelper;->findMatchingMethod(Ljava/lang/Class;Ljava/util/List;)Ljava/lang/reflect/Method;

    move-result-object v0

    if-eqz v0, :cond_0

    invoke-virtual {v3, p3}, Lorg/apache/tools/ant/ComponentHelper;->createComponent(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v1

    if-eqz v1, :cond_0

    move-object v5, v1

    instance-of v7, v1, Lorg/apache/tools/ant/taskdefs/PreSetDef$PreSetDefinition;

    if-eqz v7, :cond_2

    move-object v7, v1

    check-cast v7, Lorg/apache/tools/ant/taskdefs/PreSetDef$PreSetDefinition;

    invoke-virtual {v7, p1}, Lorg/apache/tools/ant/taskdefs/PreSetDef$PreSetDefinition;->createObject(Lorg/apache/tools/ant/Project;)Ljava/lang/Object;

    move-result-object v5

    :cond_2
    move-object v4, v1

    move-object v6, v5

    new-instance v7, Lorg/apache/tools/ant/IntrospectionHelper$11;

    invoke-direct {v7, p0, v0, v6, v4}, Lorg/apache/tools/ant/IntrospectionHelper$11;-><init>(Lorg/apache/tools/ant/IntrospectionHelper;Ljava/lang/reflect/Method;Ljava/lang/Object;Ljava/lang/Object;)V

    goto :goto_0
.end method

.method private createAttributeSetter(Ljava/lang/reflect/Method;Ljava/lang/Class;Ljava/lang/String;)Lorg/apache/tools/ant/IntrospectionHelper$AttributeSetter;
    .locals 11
    .param p1    # Ljava/lang/reflect/Method;
    .param p2    # Ljava/lang/Class;
    .param p3    # Ljava/lang/String;

    sget-object v0, Lorg/apache/tools/ant/IntrospectionHelper;->PRIMITIVE_TYPE_MAP:Ljava/util/Map;

    invoke-interface {v0, p2}, Ljava/util/Map;->containsKey(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    sget-object v0, Lorg/apache/tools/ant/IntrospectionHelper;->PRIMITIVE_TYPE_MAP:Ljava/util/Map;

    invoke-interface {v0, p2}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/Class;

    move-object v10, v0

    :goto_0
    sget-object v0, Lorg/apache/tools/ant/IntrospectionHelper;->class$java$lang$String:Ljava/lang/Class;

    if-nez v0, :cond_1

    const-string v0, "java.lang.String"

    invoke-static {v0}, Lorg/apache/tools/ant/IntrospectionHelper;->class$(Ljava/lang/String;)Ljava/lang/Class;

    move-result-object v0

    sput-object v0, Lorg/apache/tools/ant/IntrospectionHelper;->class$java$lang$String:Ljava/lang/Class;

    :goto_1
    invoke-virtual {v0, v10}, Ljava/lang/Object;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_2

    new-instance v0, Lorg/apache/tools/ant/IntrospectionHelper$3;

    invoke-direct {v0, p0, p1, p1}, Lorg/apache/tools/ant/IntrospectionHelper$3;-><init>(Lorg/apache/tools/ant/IntrospectionHelper;Ljava/lang/reflect/Method;Ljava/lang/reflect/Method;)V

    :goto_2
    return-object v0

    :cond_0
    move-object v10, p2

    goto :goto_0

    :cond_1
    sget-object v0, Lorg/apache/tools/ant/IntrospectionHelper;->class$java$lang$String:Ljava/lang/Class;

    goto :goto_1

    :cond_2
    sget-object v0, Lorg/apache/tools/ant/IntrospectionHelper;->class$java$lang$Character:Ljava/lang/Class;

    if-nez v0, :cond_3

    const-string v0, "java.lang.Character"

    invoke-static {v0}, Lorg/apache/tools/ant/IntrospectionHelper;->class$(Ljava/lang/String;)Ljava/lang/Class;

    move-result-object v0

    sput-object v0, Lorg/apache/tools/ant/IntrospectionHelper;->class$java$lang$Character:Ljava/lang/Class;

    :goto_3
    invoke-virtual {v0, v10}, Ljava/lang/Object;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_4

    new-instance v0, Lorg/apache/tools/ant/IntrospectionHelper$4;

    invoke-direct {v0, p0, p1, p3, p1}, Lorg/apache/tools/ant/IntrospectionHelper$4;-><init>(Lorg/apache/tools/ant/IntrospectionHelper;Ljava/lang/reflect/Method;Ljava/lang/String;Ljava/lang/reflect/Method;)V

    goto :goto_2

    :cond_3
    sget-object v0, Lorg/apache/tools/ant/IntrospectionHelper;->class$java$lang$Character:Ljava/lang/Class;

    goto :goto_3

    :cond_4
    sget-object v0, Lorg/apache/tools/ant/IntrospectionHelper;->class$java$lang$Boolean:Ljava/lang/Class;

    if-nez v0, :cond_5

    const-string v0, "java.lang.Boolean"

    invoke-static {v0}, Lorg/apache/tools/ant/IntrospectionHelper;->class$(Ljava/lang/String;)Ljava/lang/Class;

    move-result-object v0

    sput-object v0, Lorg/apache/tools/ant/IntrospectionHelper;->class$java$lang$Boolean:Ljava/lang/Class;

    :goto_4
    invoke-virtual {v0, v10}, Ljava/lang/Object;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_6

    new-instance v0, Lorg/apache/tools/ant/IntrospectionHelper$5;

    invoke-direct {v0, p0, p1, p1}, Lorg/apache/tools/ant/IntrospectionHelper$5;-><init>(Lorg/apache/tools/ant/IntrospectionHelper;Ljava/lang/reflect/Method;Ljava/lang/reflect/Method;)V

    goto :goto_2

    :cond_5
    sget-object v0, Lorg/apache/tools/ant/IntrospectionHelper;->class$java$lang$Boolean:Ljava/lang/Class;

    goto :goto_4

    :cond_6
    sget-object v0, Lorg/apache/tools/ant/IntrospectionHelper;->class$java$lang$Class:Ljava/lang/Class;

    if-nez v0, :cond_7

    const-string v0, "java.lang.Class"

    invoke-static {v0}, Lorg/apache/tools/ant/IntrospectionHelper;->class$(Ljava/lang/String;)Ljava/lang/Class;

    move-result-object v0

    sput-object v0, Lorg/apache/tools/ant/IntrospectionHelper;->class$java$lang$Class:Ljava/lang/Class;

    :goto_5
    invoke-virtual {v0, v10}, Ljava/lang/Object;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_8

    new-instance v0, Lorg/apache/tools/ant/IntrospectionHelper$6;

    invoke-direct {v0, p0, p1, p1}, Lorg/apache/tools/ant/IntrospectionHelper$6;-><init>(Lorg/apache/tools/ant/IntrospectionHelper;Ljava/lang/reflect/Method;Ljava/lang/reflect/Method;)V

    goto :goto_2

    :cond_7
    sget-object v0, Lorg/apache/tools/ant/IntrospectionHelper;->class$java$lang$Class:Ljava/lang/Class;

    goto :goto_5

    :cond_8
    sget-object v0, Lorg/apache/tools/ant/IntrospectionHelper;->class$java$io$File:Ljava/lang/Class;

    if-nez v0, :cond_9

    const-string v0, "java.io.File"

    invoke-static {v0}, Lorg/apache/tools/ant/IntrospectionHelper;->class$(Ljava/lang/String;)Ljava/lang/Class;

    move-result-object v0

    sput-object v0, Lorg/apache/tools/ant/IntrospectionHelper;->class$java$io$File:Ljava/lang/Class;

    :goto_6
    invoke-virtual {v0, v10}, Ljava/lang/Object;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_a

    new-instance v0, Lorg/apache/tools/ant/IntrospectionHelper$7;

    invoke-direct {v0, p0, p1, p1}, Lorg/apache/tools/ant/IntrospectionHelper$7;-><init>(Lorg/apache/tools/ant/IntrospectionHelper;Ljava/lang/reflect/Method;Ljava/lang/reflect/Method;)V

    goto :goto_2

    :cond_9
    sget-object v0, Lorg/apache/tools/ant/IntrospectionHelper;->class$java$io$File:Ljava/lang/Class;

    goto :goto_6

    :cond_a
    sget-object v0, Lorg/apache/tools/ant/IntrospectionHelper;->class$org$apache$tools$ant$types$EnumeratedAttribute:Ljava/lang/Class;

    if-nez v0, :cond_b

    const-string v0, "org.apache.tools.ant.types.EnumeratedAttribute"

    invoke-static {v0}, Lorg/apache/tools/ant/IntrospectionHelper;->class$(Ljava/lang/String;)Ljava/lang/Class;

    move-result-object v0

    sput-object v0, Lorg/apache/tools/ant/IntrospectionHelper;->class$org$apache$tools$ant$types$EnumeratedAttribute:Ljava/lang/Class;

    :goto_7
    invoke-virtual {v0, v10}, Ljava/lang/Class;->isAssignableFrom(Ljava/lang/Class;)Z

    move-result v0

    if-eqz v0, :cond_c

    new-instance v0, Lorg/apache/tools/ant/IntrospectionHelper$8;

    invoke-direct {v0, p0, p1, v10, p1}, Lorg/apache/tools/ant/IntrospectionHelper$8;-><init>(Lorg/apache/tools/ant/IntrospectionHelper;Ljava/lang/reflect/Method;Ljava/lang/Class;Ljava/lang/reflect/Method;)V

    goto/16 :goto_2

    :cond_b
    sget-object v0, Lorg/apache/tools/ant/IntrospectionHelper;->class$org$apache$tools$ant$types$EnumeratedAttribute:Ljava/lang/Class;

    goto :goto_7

    :cond_c
    invoke-virtual {v10}, Ljava/lang/Class;->getSuperclass()Ljava/lang/Class;

    move-result-object v0

    if-eqz v0, :cond_d

    invoke-virtual {v10}, Ljava/lang/Class;->getSuperclass()Ljava/lang/Class;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/Class;->getName()Ljava/lang/String;

    move-result-object v0

    const-string v1, "java.lang.Enum"

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_d

    new-instance v0, Lorg/apache/tools/ant/IntrospectionHelper$9;

    invoke-direct {v0, p0, p1, p1, v10}, Lorg/apache/tools/ant/IntrospectionHelper$9;-><init>(Lorg/apache/tools/ant/IntrospectionHelper;Ljava/lang/reflect/Method;Ljava/lang/reflect/Method;Ljava/lang/Class;)V

    goto/16 :goto_2

    :cond_d
    const/4 v0, 0x2

    :try_start_0
    new-array v1, v0, [Ljava/lang/Class;

    const/4 v2, 0x0

    sget-object v0, Lorg/apache/tools/ant/IntrospectionHelper;->class$org$apache$tools$ant$Project:Ljava/lang/Class;

    if-nez v0, :cond_e

    const-string v0, "org.apache.tools.ant.Project"

    invoke-static {v0}, Lorg/apache/tools/ant/IntrospectionHelper;->class$(Ljava/lang/String;)Ljava/lang/Class;

    move-result-object v0

    sput-object v0, Lorg/apache/tools/ant/IntrospectionHelper;->class$org$apache$tools$ant$Project:Ljava/lang/Class;

    :goto_8
    aput-object v0, v1, v2

    const/4 v2, 0x1

    sget-object v0, Lorg/apache/tools/ant/IntrospectionHelper;->class$java$lang$String:Ljava/lang/Class;

    if-nez v0, :cond_f

    const-string v0, "java.lang.String"

    invoke-static {v0}, Lorg/apache/tools/ant/IntrospectionHelper;->class$(Ljava/lang/String;)Ljava/lang/Class;

    move-result-object v0

    sput-object v0, Lorg/apache/tools/ant/IntrospectionHelper;->class$java$lang$String:Ljava/lang/Class;

    :goto_9
    aput-object v0, v1, v2

    invoke-virtual {v10, v1}, Ljava/lang/Class;->getConstructor([Ljava/lang/Class;)Ljava/lang/reflect/Constructor;
    :try_end_0
    .catch Ljava/lang/NoSuchMethodException; {:try_start_0 .. :try_end_0} :catch_0

    move-result-object v6

    const/4 v7, 0x1

    :goto_a
    move v3, v7

    move-object v4, v6

    new-instance v0, Lorg/apache/tools/ant/IntrospectionHelper$10;

    move-object v1, p0

    move-object v2, p1

    move-object v5, p1

    invoke-direct/range {v0 .. v5}, Lorg/apache/tools/ant/IntrospectionHelper$10;-><init>(Lorg/apache/tools/ant/IntrospectionHelper;Ljava/lang/reflect/Method;ZLjava/lang/reflect/Constructor;Ljava/lang/reflect/Method;)V

    goto/16 :goto_2

    :cond_e
    :try_start_1
    sget-object v0, Lorg/apache/tools/ant/IntrospectionHelper;->class$org$apache$tools$ant$Project:Ljava/lang/Class;

    goto :goto_8

    :cond_f
    sget-object v0, Lorg/apache/tools/ant/IntrospectionHelper;->class$java$lang$String:Ljava/lang/Class;
    :try_end_1
    .catch Ljava/lang/NoSuchMethodException; {:try_start_1 .. :try_end_1} :catch_0

    goto :goto_9

    :catch_0
    move-exception v8

    const/4 v0, 0x1

    :try_start_2
    new-array v1, v0, [Ljava/lang/Class;

    const/4 v2, 0x0

    sget-object v0, Lorg/apache/tools/ant/IntrospectionHelper;->class$java$lang$String:Ljava/lang/Class;

    if-nez v0, :cond_10

    const-string v0, "java.lang.String"

    invoke-static {v0}, Lorg/apache/tools/ant/IntrospectionHelper;->class$(Ljava/lang/String;)Ljava/lang/Class;

    move-result-object v0

    sput-object v0, Lorg/apache/tools/ant/IntrospectionHelper;->class$java$lang$String:Ljava/lang/Class;

    :goto_b
    aput-object v0, v1, v2

    invoke-virtual {v10, v1}, Ljava/lang/Class;->getConstructor([Ljava/lang/Class;)Ljava/lang/reflect/Constructor;

    move-result-object v6

    const/4 v7, 0x0

    goto :goto_a

    :cond_10
    sget-object v0, Lorg/apache/tools/ant/IntrospectionHelper;->class$java$lang$String:Ljava/lang/Class;
    :try_end_2
    .catch Ljava/lang/NoSuchMethodException; {:try_start_2 .. :try_end_2} :catch_1

    goto :goto_b

    :catch_1
    move-exception v9

    const/4 v0, 0x0

    goto/16 :goto_2
.end method

.method private findMatchingMethod(Ljava/lang/Class;Ljava/util/List;)Ljava/lang/reflect/Method;
    .locals 8
    .param p1    # Ljava/lang/Class;
    .param p2    # Ljava/util/List;

    const/4 v1, 0x0

    const/4 v2, 0x0

    const/4 v0, 0x0

    :goto_0
    invoke-interface {p2}, Ljava/util/List;->size()I

    move-result v5

    if-ge v0, v5, :cond_2

    invoke-interface {p2, v0}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Ljava/lang/reflect/Method;

    invoke-virtual {v3}, Ljava/lang/reflect/Method;->getParameterTypes()[Ljava/lang/Class;

    move-result-object v5

    const/4 v6, 0x0

    aget-object v4, v5, v6

    invoke-virtual {v4, p1}, Ljava/lang/Class;->isAssignableFrom(Ljava/lang/Class;)Z

    move-result v5

    if-eqz v5, :cond_0

    if-nez v1, :cond_1

    move-object v1, v4

    move-object v2, v3

    :cond_0
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    :cond_1
    invoke-virtual {v4, v1}, Ljava/lang/Class;->isAssignableFrom(Ljava/lang/Class;)Z

    move-result v5

    if-nez v5, :cond_0

    new-instance v5, Lorg/apache/tools/ant/BuildException;

    new-instance v6, Ljava/lang/StringBuffer;

    invoke-direct {v6}, Ljava/lang/StringBuffer;-><init>()V

    const-string v7, "ambiguous: types "

    invoke-virtual {v6, v7}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    move-result-object v6

    invoke-virtual {v1}, Ljava/lang/Class;->getName()Ljava/lang/String;

    move-result-object v7

    invoke-virtual {v6, v7}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    move-result-object v6

    const-string v7, " and "

    invoke-virtual {v6, v7}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    move-result-object v6

    invoke-virtual {v4}, Ljava/lang/Class;->getName()Ljava/lang/String;

    move-result-object v7

    invoke-virtual {v6, v7}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    move-result-object v6

    const-string v7, " match "

    invoke-virtual {v6, v7}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    move-result-object v6

    invoke-virtual {p1}, Ljava/lang/Class;->getName()Ljava/lang/String;

    move-result-object v7

    invoke-virtual {v6, v7}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    move-result-object v6

    invoke-virtual {v6}, Ljava/lang/StringBuffer;->toString()Ljava/lang/String;

    move-result-object v6

    invoke-direct {v5, v6}, Lorg/apache/tools/ant/BuildException;-><init>(Ljava/lang/String;)V

    throw v5

    :cond_2
    return-object v2
.end method

.method public static declared-synchronized getHelper(Ljava/lang/Class;)Lorg/apache/tools/ant/IntrospectionHelper;
    .locals 2
    .param p0    # Ljava/lang/Class;

    const-class v0, Lorg/apache/tools/ant/IntrospectionHelper;

    monitor-enter v0

    const/4 v1, 0x0

    :try_start_0
    invoke-static {v1, p0}, Lorg/apache/tools/ant/IntrospectionHelper;->getHelper(Lorg/apache/tools/ant/Project;Ljava/lang/Class;)Lorg/apache/tools/ant/IntrospectionHelper;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    move-result-object v1

    monitor-exit v0

    return-object v1

    :catchall_0
    move-exception v1

    monitor-exit v0

    throw v1
.end method

.method public static getHelper(Lorg/apache/tools/ant/Project;Ljava/lang/Class;)Lorg/apache/tools/ant/IntrospectionHelper;
    .locals 3
    .param p0    # Lorg/apache/tools/ant/Project;
    .param p1    # Ljava/lang/Class;

    sget-object v1, Lorg/apache/tools/ant/IntrospectionHelper;->HELPERS:Ljava/util/Map;

    invoke-virtual {p1}, Ljava/lang/Class;->getName()Ljava/lang/String;

    move-result-object v2

    invoke-interface {v1, v2}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lorg/apache/tools/ant/IntrospectionHelper;

    if-eqz v0, :cond_0

    iget-object v1, v0, Lorg/apache/tools/ant/IntrospectionHelper;->bean:Ljava/lang/Class;

    if-eq v1, p1, :cond_1

    :cond_0
    new-instance v0, Lorg/apache/tools/ant/IntrospectionHelper;

    invoke-direct {v0, p1}, Lorg/apache/tools/ant/IntrospectionHelper;-><init>(Ljava/lang/Class;)V

    if-eqz p0, :cond_1

    sget-object v1, Lorg/apache/tools/ant/IntrospectionHelper;->HELPERS:Ljava/util/Map;

    invoke-virtual {p1}, Ljava/lang/Class;->getName()Ljava/lang/String;

    move-result-object v2

    invoke-interface {v1, v2, v0}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    :cond_1
    return-object v0
.end method

.method private getNestedCreator(Lorg/apache/tools/ant/Project;Ljava/lang/String;Ljava/lang/Object;Ljava/lang/String;Lorg/apache/tools/ant/UnknownElement;)Lorg/apache/tools/ant/IntrospectionHelper$NestedCreator;
    .locals 9
    .param p1    # Lorg/apache/tools/ant/Project;
    .param p2    # Ljava/lang/String;
    .param p3    # Ljava/lang/Object;
    .param p4    # Ljava/lang/String;
    .param p5    # Lorg/apache/tools/ant/UnknownElement;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lorg/apache/tools/ant/BuildException;
        }
    .end annotation

    const/4 v8, 0x0

    invoke-static {p4}, Lorg/apache/tools/ant/ProjectHelper;->extractUriFromComponentName(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v5

    invoke-static {p4}, Lorg/apache/tools/ant/ProjectHelper;->extractNameFromComponentName(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    const-string v6, "antlib:org.apache.tools.ant"

    invoke-virtual {v5, v6}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v6

    if-eqz v6, :cond_0

    const-string v5, ""

    :cond_0
    const-string v6, "antlib:org.apache.tools.ant"

    invoke-virtual {p2, v6}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v6

    if-eqz v6, :cond_1

    const-string p2, ""

    :cond_1
    const/4 v2, 0x0

    invoke-virtual {v5, p2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v6

    if-nez v6, :cond_2

    const-string v6, ""

    invoke-virtual {v5, v6}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v6

    if-eqz v6, :cond_3

    :cond_2
    iget-object v6, p0, Lorg/apache/tools/ant/IntrospectionHelper;->nestedCreators:Ljava/util/Hashtable;

    sget-object v7, Ljava/util/Locale;->US:Ljava/util/Locale;

    invoke-virtual {v1, v7}, Ljava/lang/String;->toLowerCase(Ljava/util/Locale;)Ljava/lang/String;

    move-result-object v7

    invoke-virtual {v6, v7}, Ljava/util/Hashtable;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lorg/apache/tools/ant/IntrospectionHelper$NestedCreator;

    :cond_3
    if-nez v2, :cond_4

    invoke-direct {p0, p1, p3, p4}, Lorg/apache/tools/ant/IntrospectionHelper;->createAddTypeCreator(Lorg/apache/tools/ant/Project;Ljava/lang/Object;Ljava/lang/String;)Lorg/apache/tools/ant/IntrospectionHelper$NestedCreator;

    move-result-object v2

    :cond_4
    if-nez v2, :cond_5

    instance-of v6, p3, Lorg/apache/tools/ant/DynamicElementNS;

    if-eqz v6, :cond_5

    move-object v0, p3

    check-cast v0, Lorg/apache/tools/ant/DynamicElementNS;

    if-nez p5, :cond_8

    move-object v4, v1

    :goto_0
    if-nez p5, :cond_9

    const-string v6, ""

    :goto_1
    invoke-interface {v0, v6, v1, v4}, Lorg/apache/tools/ant/DynamicElementNS;->createDynamicElement(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v3

    if-eqz v3, :cond_5

    new-instance v2, Lorg/apache/tools/ant/IntrospectionHelper$1;

    invoke-direct {v2, p0, v8, v3}, Lorg/apache/tools/ant/IntrospectionHelper$1;-><init>(Lorg/apache/tools/ant/IntrospectionHelper;Ljava/lang/reflect/Method;Ljava/lang/Object;)V

    :cond_5
    if-nez v2, :cond_6

    instance-of v6, p3, Lorg/apache/tools/ant/DynamicElement;

    if-eqz v6, :cond_6

    move-object v0, p3

    check-cast v0, Lorg/apache/tools/ant/DynamicElement;

    sget-object v6, Ljava/util/Locale;->US:Ljava/util/Locale;

    invoke-virtual {v1, v6}, Ljava/lang/String;->toLowerCase(Ljava/util/Locale;)Ljava/lang/String;

    move-result-object v6

    invoke-interface {v0, v6}, Lorg/apache/tools/ant/DynamicElement;->createDynamicElement(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v3

    if-eqz v3, :cond_6

    new-instance v2, Lorg/apache/tools/ant/IntrospectionHelper$2;

    invoke-direct {v2, p0, v8, v3}, Lorg/apache/tools/ant/IntrospectionHelper$2;-><init>(Lorg/apache/tools/ant/IntrospectionHelper;Ljava/lang/reflect/Method;Ljava/lang/Object;)V

    :cond_6
    if-nez v2, :cond_7

    invoke-virtual {p0, p1, p3, p4}, Lorg/apache/tools/ant/IntrospectionHelper;->throwNotSupported(Lorg/apache/tools/ant/Project;Ljava/lang/Object;Ljava/lang/String;)V

    :cond_7
    return-object v2

    :cond_8
    invoke-virtual {p5}, Lorg/apache/tools/ant/UnknownElement;->getQName()Ljava/lang/String;

    move-result-object v4

    goto :goto_0

    :cond_9
    invoke-virtual {p5}, Lorg/apache/tools/ant/UnknownElement;->getNamespace()Ljava/lang/String;

    move-result-object v6

    goto :goto_1
.end method

.method private getPropertyName(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;
    .locals 2
    .param p1    # Ljava/lang/String;
    .param p2    # Ljava/lang/String;

    invoke-virtual {p2}, Ljava/lang/String;->length()I

    move-result v0

    invoke-virtual {p1, v0}, Ljava/lang/String;->substring(I)Ljava/lang/String;

    move-result-object v0

    sget-object v1, Ljava/util/Locale;->US:Ljava/util/Locale;

    invoke-virtual {v0, v1}, Ljava/lang/String;->toLowerCase(Ljava/util/Locale;)Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method private insertAddTypeMethod(Ljava/lang/reflect/Method;)V
    .locals 5
    .param p1    # Ljava/lang/reflect/Method;

    const/4 v4, 0x0

    invoke-virtual {p1}, Ljava/lang/reflect/Method;->getParameterTypes()[Ljava/lang/Class;

    move-result-object v3

    aget-object v0, v3, v4

    const/4 v1, 0x0

    :goto_0
    iget-object v3, p0, Lorg/apache/tools/ant/IntrospectionHelper;->addTypeMethods:Ljava/util/List;

    invoke-interface {v3}, Ljava/util/List;->size()I

    move-result v3

    if-ge v1, v3, :cond_3

    iget-object v3, p0, Lorg/apache/tools/ant/IntrospectionHelper;->addTypeMethods:Ljava/util/List;

    invoke-interface {v3, v1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Ljava/lang/reflect/Method;

    invoke-virtual {v2}, Ljava/lang/reflect/Method;->getParameterTypes()[Ljava/lang/Class;

    move-result-object v3

    aget-object v3, v3, v4

    invoke-virtual {v3, v0}, Ljava/lang/Object;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-eqz v3, :cond_1

    invoke-virtual {p1}, Ljava/lang/reflect/Method;->getName()Ljava/lang/String;

    move-result-object v3

    const-string v4, "addConfigured"

    invoke-virtual {v3, v4}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-eqz v3, :cond_0

    iget-object v3, p0, Lorg/apache/tools/ant/IntrospectionHelper;->addTypeMethods:Ljava/util/List;

    invoke-interface {v3, v1, p1}, Ljava/util/List;->set(ILjava/lang/Object;)Ljava/lang/Object;

    :cond_0
    :goto_1
    return-void

    :cond_1
    invoke-virtual {v2}, Ljava/lang/reflect/Method;->getParameterTypes()[Ljava/lang/Class;

    move-result-object v3

    aget-object v3, v3, v4

    invoke-virtual {v3, v0}, Ljava/lang/Class;->isAssignableFrom(Ljava/lang/Class;)Z

    move-result v3

    if-eqz v3, :cond_2

    iget-object v3, p0, Lorg/apache/tools/ant/IntrospectionHelper;->addTypeMethods:Ljava/util/List;

    invoke-interface {v3, v1, p1}, Ljava/util/List;->add(ILjava/lang/Object;)V

    goto :goto_1

    :cond_2
    add-int/lit8 v1, v1, 0x1

    goto :goto_0

    :cond_3
    iget-object v3, p0, Lorg/apache/tools/ant/IntrospectionHelper;->addTypeMethods:Ljava/util/List;

    invoke-interface {v3, p1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    goto :goto_1
.end method

.method private isHiddenSetMethod(Ljava/lang/String;Ljava/lang/Class;)Z
    .locals 2
    .param p1    # Ljava/lang/String;
    .param p2    # Ljava/lang/Class;

    const/4 v1, 0x1

    const-string v0, "setLocation"

    invoke-virtual {v0, p1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_1

    sget-object v0, Lorg/apache/tools/ant/IntrospectionHelper;->class$org$apache$tools$ant$Location:Ljava/lang/Class;

    if-nez v0, :cond_0

    const-string v0, "org.apache.tools.ant.Location"

    invoke-static {v0}, Lorg/apache/tools/ant/IntrospectionHelper;->class$(Ljava/lang/String;)Ljava/lang/Class;

    move-result-object v0

    sput-object v0, Lorg/apache/tools/ant/IntrospectionHelper;->class$org$apache$tools$ant$Location:Ljava/lang/Class;

    :goto_0
    invoke-virtual {v0, p2}, Ljava/lang/Object;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_1

    move v0, v1

    :goto_1
    return v0

    :cond_0
    sget-object v0, Lorg/apache/tools/ant/IntrospectionHelper;->class$org$apache$tools$ant$Location:Ljava/lang/Class;

    goto :goto_0

    :cond_1
    const-string v0, "setTaskType"

    invoke-virtual {v0, p1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_3

    sget-object v0, Lorg/apache/tools/ant/IntrospectionHelper;->class$java$lang$String:Ljava/lang/Class;

    if-nez v0, :cond_2

    const-string v0, "java.lang.String"

    invoke-static {v0}, Lorg/apache/tools/ant/IntrospectionHelper;->class$(Ljava/lang/String;)Ljava/lang/Class;

    move-result-object v0

    sput-object v0, Lorg/apache/tools/ant/IntrospectionHelper;->class$java$lang$String:Ljava/lang/Class;

    :goto_2
    invoke-virtual {v0, p2}, Ljava/lang/Object;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_3

    move v0, v1

    goto :goto_1

    :cond_2
    sget-object v0, Lorg/apache/tools/ant/IntrospectionHelper;->class$java$lang$String:Ljava/lang/Class;

    goto :goto_2

    :cond_3
    const/4 v0, 0x0

    goto :goto_1
.end method


# virtual methods
.method public addText(Lorg/apache/tools/ant/Project;Ljava/lang/Object;Ljava/lang/String;)V
    .locals 7
    .param p1    # Lorg/apache/tools/ant/Project;
    .param p2    # Ljava/lang/Object;
    .param p3    # Ljava/lang/String;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lorg/apache/tools/ant/BuildException;
        }
    .end annotation

    iget-object v4, p0, Lorg/apache/tools/ant/IntrospectionHelper;->addText:Ljava/lang/reflect/Method;

    if-nez v4, :cond_1

    invoke-virtual {p3}, Ljava/lang/String;->trim()Ljava/lang/String;

    move-result-object p3

    invoke-virtual {p3}, Ljava/lang/String;->length()I

    move-result v4

    if-nez v4, :cond_0

    :goto_0
    return-void

    :cond_0
    new-instance v4, Ljava/lang/StringBuffer;

    invoke-direct {v4}, Ljava/lang/StringBuffer;-><init>()V

    invoke-virtual {p1, p2}, Lorg/apache/tools/ant/Project;->getElementName(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v4, v5}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    move-result-object v4

    const-string v5, " doesn\'t support nested text data (\""

    invoke-virtual {v4, v5}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    move-result-object v4

    invoke-direct {p0, p3}, Lorg/apache/tools/ant/IntrospectionHelper;->condenseText(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v4, v5}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    move-result-object v4

    const-string v5, "\")."

    invoke-virtual {v4, v5}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuffer;->toString()Ljava/lang/String;

    move-result-object v2

    new-instance v4, Lorg/apache/tools/ant/BuildException;

    invoke-direct {v4, v2}, Lorg/apache/tools/ant/BuildException;-><init>(Ljava/lang/String;)V

    throw v4

    :cond_1
    :try_start_0
    iget-object v4, p0, Lorg/apache/tools/ant/IntrospectionHelper;->addText:Ljava/lang/reflect/Method;

    const/4 v5, 0x1

    new-array v5, v5, [Ljava/lang/Object;

    const/4 v6, 0x0

    aput-object p3, v5, v6

    invoke-virtual {v4, p2, v5}, Ljava/lang/reflect/Method;->invoke(Ljava/lang/Object;[Ljava/lang/Object;)Ljava/lang/Object;
    :try_end_0
    .catch Ljava/lang/IllegalAccessException; {:try_start_0 .. :try_end_0} :catch_0
    .catch Ljava/lang/reflect/InvocationTargetException; {:try_start_0 .. :try_end_0} :catch_1

    goto :goto_0

    :catch_0
    move-exception v0

    new-instance v4, Lorg/apache/tools/ant/BuildException;

    invoke-direct {v4, v0}, Lorg/apache/tools/ant/BuildException;-><init>(Ljava/lang/Throwable;)V

    throw v4

    :catch_1
    move-exception v1

    invoke-virtual {v1}, Ljava/lang/reflect/InvocationTargetException;->getTargetException()Ljava/lang/Throwable;

    move-result-object v3

    instance-of v4, v3, Lorg/apache/tools/ant/BuildException;

    if-eqz v4, :cond_2

    check-cast v3, Lorg/apache/tools/ant/BuildException;

    throw v3

    :cond_2
    new-instance v4, Lorg/apache/tools/ant/BuildException;

    invoke-direct {v4, v3}, Lorg/apache/tools/ant/BuildException;-><init>(Ljava/lang/Throwable;)V

    throw v4
.end method

.method public createElement(Lorg/apache/tools/ant/Project;Ljava/lang/Object;Ljava/lang/String;)Ljava/lang/Object;
    .locals 12
    .param p1    # Lorg/apache/tools/ant/Project;
    .param p2    # Ljava/lang/Object;
    .param p3    # Ljava/lang/String;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lorg/apache/tools/ant/BuildException;
        }
    .end annotation

    const/4 v5, 0x0

    const-string v2, ""

    move-object v0, p0

    move-object v1, p1

    move-object v3, p2

    move-object v4, p3

    invoke-direct/range {v0 .. v5}, Lorg/apache/tools/ant/IntrospectionHelper;->getNestedCreator(Lorg/apache/tools/ant/Project;Ljava/lang/String;Ljava/lang/Object;Ljava/lang/String;Lorg/apache/tools/ant/UnknownElement;)Lorg/apache/tools/ant/IntrospectionHelper$NestedCreator;

    move-result-object v9

    const/4 v0, 0x0

    :try_start_0
    invoke-virtual {v9, p1, p2, v0}, Lorg/apache/tools/ant/IntrospectionHelper$NestedCreator;->create(Lorg/apache/tools/ant/Project;Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v10

    if-eqz p1, :cond_0

    invoke-virtual {p1, v10}, Lorg/apache/tools/ant/Project;->setProjectReference(Ljava/lang/Object;)V
    :try_end_0
    .catch Ljava/lang/IllegalAccessException; {:try_start_0 .. :try_end_0} :catch_0
    .catch Ljava/lang/InstantiationException; {:try_start_0 .. :try_end_0} :catch_1
    .catch Ljava/lang/reflect/InvocationTargetException; {:try_start_0 .. :try_end_0} :catch_2

    :cond_0
    return-object v10

    :catch_0
    move-exception v6

    new-instance v0, Lorg/apache/tools/ant/BuildException;

    invoke-direct {v0, v6}, Lorg/apache/tools/ant/BuildException;-><init>(Ljava/lang/Throwable;)V

    throw v0

    :catch_1
    move-exception v7

    new-instance v0, Lorg/apache/tools/ant/BuildException;

    invoke-direct {v0, v7}, Lorg/apache/tools/ant/BuildException;-><init>(Ljava/lang/Throwable;)V

    throw v0

    :catch_2
    move-exception v8

    invoke-virtual {v8}, Ljava/lang/reflect/InvocationTargetException;->getTargetException()Ljava/lang/Throwable;

    move-result-object v11

    instance-of v0, v11, Lorg/apache/tools/ant/BuildException;

    if-eqz v0, :cond_1

    check-cast v11, Lorg/apache/tools/ant/BuildException;

    throw v11

    :cond_1
    new-instance v0, Lorg/apache/tools/ant/BuildException;

    invoke-direct {v0, v11}, Lorg/apache/tools/ant/BuildException;-><init>(Ljava/lang/Throwable;)V

    throw v0
.end method

.method public getAddTextMethod()Ljava/lang/reflect/Method;
    .locals 3
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lorg/apache/tools/ant/BuildException;
        }
    .end annotation

    invoke-virtual {p0}, Lorg/apache/tools/ant/IntrospectionHelper;->supportsCharacters()Z

    move-result v0

    if-nez v0, :cond_0

    new-instance v0, Lorg/apache/tools/ant/BuildException;

    new-instance v1, Ljava/lang/StringBuffer;

    invoke-direct {v1}, Ljava/lang/StringBuffer;-><init>()V

    const-string v2, "Class "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    move-result-object v1

    iget-object v2, p0, Lorg/apache/tools/ant/IntrospectionHelper;->bean:Ljava/lang/Class;

    invoke-virtual {v2}, Ljava/lang/Class;->getName()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    move-result-object v1

    const-string v2, " doesn\'t support nested text data."

    invoke-virtual {v1, v2}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuffer;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Lorg/apache/tools/ant/BuildException;-><init>(Ljava/lang/String;)V

    throw v0

    :cond_0
    iget-object v0, p0, Lorg/apache/tools/ant/IntrospectionHelper;->addText:Ljava/lang/reflect/Method;

    return-object v0
.end method

.method public getAttributeMap()Ljava/util/Map;
    .locals 2

    iget-object v0, p0, Lorg/apache/tools/ant/IntrospectionHelper;->attributeTypes:Ljava/util/Hashtable;

    invoke-virtual {v0}, Ljava/util/Hashtable;->size()I

    move-result v0

    const/4 v1, 0x1

    if-ge v0, v1, :cond_0

    sget-object v0, Lorg/apache/tools/ant/IntrospectionHelper;->EMPTY_MAP:Ljava/util/Map;

    :goto_0
    return-object v0

    :cond_0
    iget-object v0, p0, Lorg/apache/tools/ant/IntrospectionHelper;->attributeTypes:Ljava/util/Hashtable;

    invoke-static {v0}, Ljava/util/Collections;->unmodifiableMap(Ljava/util/Map;)Ljava/util/Map;

    move-result-object v0

    goto :goto_0
.end method

.method public getAttributeMethod(Ljava/lang/String;)Ljava/lang/reflect/Method;
    .locals 4
    .param p1    # Ljava/lang/String;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lorg/apache/tools/ant/BuildException;
        }
    .end annotation

    iget-object v1, p0, Lorg/apache/tools/ant/IntrospectionHelper;->attributeSetters:Ljava/util/Hashtable;

    invoke-virtual {v1, p1}, Ljava/util/Hashtable;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    if-nez v0, :cond_0

    new-instance v1, Lorg/apache/tools/ant/UnsupportedAttributeException;

    new-instance v2, Ljava/lang/StringBuffer;

    invoke-direct {v2}, Ljava/lang/StringBuffer;-><init>()V

    const-string v3, "Class "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    move-result-object v2

    iget-object v3, p0, Lorg/apache/tools/ant/IntrospectionHelper;->bean:Ljava/lang/Class;

    invoke-virtual {v3}, Ljava/lang/Class;->getName()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    move-result-object v2

    const-string v3, " doesn\'t support the \""

    invoke-virtual {v2, v3}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    move-result-object v2

    invoke-virtual {v2, p1}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    move-result-object v2

    const-string v3, "\" attribute."

    invoke-virtual {v2, v3}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuffer;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-direct {v1, v2, p1}, Lorg/apache/tools/ant/UnsupportedAttributeException;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    throw v1

    :cond_0
    check-cast v0, Lorg/apache/tools/ant/IntrospectionHelper$AttributeSetter;

    invoke-static {v0}, Lorg/apache/tools/ant/IntrospectionHelper$AttributeSetter;->access$200(Lorg/apache/tools/ant/IntrospectionHelper$AttributeSetter;)Ljava/lang/reflect/Method;

    move-result-object v1

    return-object v1
.end method

.method public getAttributeType(Ljava/lang/String;)Ljava/lang/Class;
    .locals 4
    .param p1    # Ljava/lang/String;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lorg/apache/tools/ant/BuildException;
        }
    .end annotation

    iget-object v1, p0, Lorg/apache/tools/ant/IntrospectionHelper;->attributeTypes:Ljava/util/Hashtable;

    invoke-virtual {v1, p1}, Ljava/util/Hashtable;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/Class;

    if-nez v0, :cond_0

    new-instance v1, Lorg/apache/tools/ant/UnsupportedAttributeException;

    new-instance v2, Ljava/lang/StringBuffer;

    invoke-direct {v2}, Ljava/lang/StringBuffer;-><init>()V

    const-string v3, "Class "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    move-result-object v2

    iget-object v3, p0, Lorg/apache/tools/ant/IntrospectionHelper;->bean:Ljava/lang/Class;

    invoke-virtual {v3}, Ljava/lang/Class;->getName()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    move-result-object v2

    const-string v3, " doesn\'t support the \""

    invoke-virtual {v2, v3}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    move-result-object v2

    invoke-virtual {v2, p1}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    move-result-object v2

    const-string v3, "\" attribute."

    invoke-virtual {v2, v3}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuffer;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-direct {v1, v2, p1}, Lorg/apache/tools/ant/UnsupportedAttributeException;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    throw v1

    :cond_0
    return-object v0
.end method

.method public getAttributes()Ljava/util/Enumeration;
    .locals 1

    iget-object v0, p0, Lorg/apache/tools/ant/IntrospectionHelper;->attributeSetters:Ljava/util/Hashtable;

    invoke-virtual {v0}, Ljava/util/Hashtable;->keys()Ljava/util/Enumeration;

    move-result-object v0

    return-object v0
.end method

.method public getElementCreator(Lorg/apache/tools/ant/Project;Ljava/lang/String;Ljava/lang/Object;Ljava/lang/String;Lorg/apache/tools/ant/UnknownElement;)Lorg/apache/tools/ant/IntrospectionHelper$Creator;
    .locals 3
    .param p1    # Lorg/apache/tools/ant/Project;
    .param p2    # Ljava/lang/String;
    .param p3    # Ljava/lang/Object;
    .param p4    # Ljava/lang/String;
    .param p5    # Lorg/apache/tools/ant/UnknownElement;

    invoke-direct/range {p0 .. p5}, Lorg/apache/tools/ant/IntrospectionHelper;->getNestedCreator(Lorg/apache/tools/ant/Project;Ljava/lang/String;Ljava/lang/Object;Ljava/lang/String;Lorg/apache/tools/ant/UnknownElement;)Lorg/apache/tools/ant/IntrospectionHelper$NestedCreator;

    move-result-object v0

    new-instance v1, Lorg/apache/tools/ant/IntrospectionHelper$Creator;

    const/4 v2, 0x0

    invoke-direct {v1, p1, p3, v0, v2}, Lorg/apache/tools/ant/IntrospectionHelper$Creator;-><init>(Lorg/apache/tools/ant/Project;Ljava/lang/Object;Lorg/apache/tools/ant/IntrospectionHelper$NestedCreator;Lorg/apache/tools/ant/IntrospectionHelper$1;)V

    return-object v1
.end method

.method public getElementMethod(Ljava/lang/String;)Ljava/lang/reflect/Method;
    .locals 4
    .param p1    # Ljava/lang/String;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lorg/apache/tools/ant/BuildException;
        }
    .end annotation

    iget-object v1, p0, Lorg/apache/tools/ant/IntrospectionHelper;->nestedCreators:Ljava/util/Hashtable;

    invoke-virtual {v1, p1}, Ljava/util/Hashtable;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    if-nez v0, :cond_0

    new-instance v1, Lorg/apache/tools/ant/UnsupportedElementException;

    new-instance v2, Ljava/lang/StringBuffer;

    invoke-direct {v2}, Ljava/lang/StringBuffer;-><init>()V

    const-string v3, "Class "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    move-result-object v2

    iget-object v3, p0, Lorg/apache/tools/ant/IntrospectionHelper;->bean:Ljava/lang/Class;

    invoke-virtual {v3}, Ljava/lang/Class;->getName()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    move-result-object v2

    const-string v3, " doesn\'t support the nested \""

    invoke-virtual {v2, v3}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    move-result-object v2

    invoke-virtual {v2, p1}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    move-result-object v2

    const-string v3, "\" element."

    invoke-virtual {v2, v3}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuffer;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-direct {v1, v2, p1}, Lorg/apache/tools/ant/UnsupportedElementException;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    throw v1

    :cond_0
    check-cast v0, Lorg/apache/tools/ant/IntrospectionHelper$NestedCreator;

    invoke-static {v0}, Lorg/apache/tools/ant/IntrospectionHelper$NestedCreator;->access$100(Lorg/apache/tools/ant/IntrospectionHelper$NestedCreator;)Ljava/lang/reflect/Method;

    move-result-object v1

    return-object v1
.end method

.method protected getElementName(Lorg/apache/tools/ant/Project;Ljava/lang/Object;)Ljava/lang/String;
    .locals 1
    .param p1    # Lorg/apache/tools/ant/Project;
    .param p2    # Ljava/lang/Object;

    invoke-virtual {p1, p2}, Lorg/apache/tools/ant/Project;->getElementName(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public getElementType(Ljava/lang/String;)Ljava/lang/Class;
    .locals 4
    .param p1    # Ljava/lang/String;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lorg/apache/tools/ant/BuildException;
        }
    .end annotation

    iget-object v1, p0, Lorg/apache/tools/ant/IntrospectionHelper;->nestedTypes:Ljava/util/Hashtable;

    invoke-virtual {v1, p1}, Ljava/util/Hashtable;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/Class;

    if-nez v0, :cond_0

    new-instance v1, Lorg/apache/tools/ant/UnsupportedElementException;

    new-instance v2, Ljava/lang/StringBuffer;

    invoke-direct {v2}, Ljava/lang/StringBuffer;-><init>()V

    const-string v3, "Class "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    move-result-object v2

    iget-object v3, p0, Lorg/apache/tools/ant/IntrospectionHelper;->bean:Ljava/lang/Class;

    invoke-virtual {v3}, Ljava/lang/Class;->getName()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    move-result-object v2

    const-string v3, " doesn\'t support the nested \""

    invoke-virtual {v2, v3}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    move-result-object v2

    invoke-virtual {v2, p1}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    move-result-object v2

    const-string v3, "\" element."

    invoke-virtual {v2, v3}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuffer;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-direct {v1, v2, p1}, Lorg/apache/tools/ant/UnsupportedElementException;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    throw v1

    :cond_0
    return-object v0
.end method

.method public getExtensionPoints()Ljava/util/List;
    .locals 2

    iget-object v0, p0, Lorg/apache/tools/ant/IntrospectionHelper;->addTypeMethods:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->size()I

    move-result v0

    const/4 v1, 0x1

    if-ge v0, v1, :cond_0

    sget-object v0, Ljava/util/Collections;->EMPTY_LIST:Ljava/util/List;

    :goto_0
    return-object v0

    :cond_0
    iget-object v0, p0, Lorg/apache/tools/ant/IntrospectionHelper;->addTypeMethods:Ljava/util/List;

    invoke-static {v0}, Ljava/util/Collections;->unmodifiableList(Ljava/util/List;)Ljava/util/List;

    move-result-object v0

    goto :goto_0
.end method

.method public getNestedElementMap()Ljava/util/Map;
    .locals 2

    iget-object v0, p0, Lorg/apache/tools/ant/IntrospectionHelper;->nestedTypes:Ljava/util/Hashtable;

    invoke-virtual {v0}, Ljava/util/Hashtable;->size()I

    move-result v0

    const/4 v1, 0x1

    if-ge v0, v1, :cond_0

    sget-object v0, Lorg/apache/tools/ant/IntrospectionHelper;->EMPTY_MAP:Ljava/util/Map;

    :goto_0
    return-object v0

    :cond_0
    iget-object v0, p0, Lorg/apache/tools/ant/IntrospectionHelper;->nestedTypes:Ljava/util/Hashtable;

    invoke-static {v0}, Ljava/util/Collections;->unmodifiableMap(Ljava/util/Map;)Ljava/util/Map;

    move-result-object v0

    goto :goto_0
.end method

.method public getNestedElements()Ljava/util/Enumeration;
    .locals 1

    iget-object v0, p0, Lorg/apache/tools/ant/IntrospectionHelper;->nestedTypes:Ljava/util/Hashtable;

    invoke-virtual {v0}, Ljava/util/Hashtable;->keys()Ljava/util/Enumeration;

    move-result-object v0

    return-object v0
.end method

.method public isContainer()Z
    .locals 2

    sget-object v0, Lorg/apache/tools/ant/IntrospectionHelper;->class$org$apache$tools$ant$TaskContainer:Ljava/lang/Class;

    if-nez v0, :cond_0

    const-string v0, "org.apache.tools.ant.TaskContainer"

    invoke-static {v0}, Lorg/apache/tools/ant/IntrospectionHelper;->class$(Ljava/lang/String;)Ljava/lang/Class;

    move-result-object v0

    sput-object v0, Lorg/apache/tools/ant/IntrospectionHelper;->class$org$apache$tools$ant$TaskContainer:Ljava/lang/Class;

    :goto_0
    iget-object v1, p0, Lorg/apache/tools/ant/IntrospectionHelper;->bean:Ljava/lang/Class;

    invoke-virtual {v0, v1}, Ljava/lang/Class;->isAssignableFrom(Ljava/lang/Class;)Z

    move-result v0

    return v0

    :cond_0
    sget-object v0, Lorg/apache/tools/ant/IntrospectionHelper;->class$org$apache$tools$ant$TaskContainer:Ljava/lang/Class;

    goto :goto_0
.end method

.method public isDynamic()Z
    .locals 2

    sget-object v0, Lorg/apache/tools/ant/IntrospectionHelper;->class$org$apache$tools$ant$DynamicElement:Ljava/lang/Class;

    if-nez v0, :cond_1

    const-string v0, "org.apache.tools.ant.DynamicElement"

    invoke-static {v0}, Lorg/apache/tools/ant/IntrospectionHelper;->class$(Ljava/lang/String;)Ljava/lang/Class;

    move-result-object v0

    sput-object v0, Lorg/apache/tools/ant/IntrospectionHelper;->class$org$apache$tools$ant$DynamicElement:Ljava/lang/Class;

    :goto_0
    iget-object v1, p0, Lorg/apache/tools/ant/IntrospectionHelper;->bean:Ljava/lang/Class;

    invoke-virtual {v0, v1}, Ljava/lang/Class;->isAssignableFrom(Ljava/lang/Class;)Z

    move-result v0

    if-nez v0, :cond_0

    sget-object v0, Lorg/apache/tools/ant/IntrospectionHelper;->class$org$apache$tools$ant$DynamicElementNS:Ljava/lang/Class;

    if-nez v0, :cond_2

    const-string v0, "org.apache.tools.ant.DynamicElementNS"

    invoke-static {v0}, Lorg/apache/tools/ant/IntrospectionHelper;->class$(Ljava/lang/String;)Ljava/lang/Class;

    move-result-object v0

    sput-object v0, Lorg/apache/tools/ant/IntrospectionHelper;->class$org$apache$tools$ant$DynamicElementNS:Ljava/lang/Class;

    :goto_1
    iget-object v1, p0, Lorg/apache/tools/ant/IntrospectionHelper;->bean:Ljava/lang/Class;

    invoke-virtual {v0, v1}, Ljava/lang/Class;->isAssignableFrom(Ljava/lang/Class;)Z

    move-result v0

    if-eqz v0, :cond_3

    :cond_0
    const/4 v0, 0x1

    :goto_2
    return v0

    :cond_1
    sget-object v0, Lorg/apache/tools/ant/IntrospectionHelper;->class$org$apache$tools$ant$DynamicElement:Ljava/lang/Class;

    goto :goto_0

    :cond_2
    sget-object v0, Lorg/apache/tools/ant/IntrospectionHelper;->class$org$apache$tools$ant$DynamicElementNS:Ljava/lang/Class;

    goto :goto_1

    :cond_3
    const/4 v0, 0x0

    goto :goto_2
.end method

.method public setAttribute(Lorg/apache/tools/ant/Project;Ljava/lang/Object;Ljava/lang/String;Ljava/lang/String;)V
    .locals 13
    .param p1    # Lorg/apache/tools/ant/Project;
    .param p2    # Ljava/lang/Object;
    .param p3    # Ljava/lang/String;
    .param p4    # Ljava/lang/String;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lorg/apache/tools/ant/BuildException;
        }
    .end annotation

    iget-object v11, p0, Lorg/apache/tools/ant/IntrospectionHelper;->attributeSetters:Ljava/util/Hashtable;

    sget-object v12, Ljava/util/Locale;->US:Ljava/util/Locale;

    move-object/from16 v0, p3

    invoke-virtual {v0, v12}, Ljava/lang/String;->toLowerCase(Ljava/util/Locale;)Ljava/lang/String;

    move-result-object v12

    invoke-virtual {v11, v12}, Ljava/util/Hashtable;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lorg/apache/tools/ant/IntrospectionHelper$AttributeSetter;

    if-nez v1, :cond_4

    instance-of v11, p2, Lorg/apache/tools/ant/DynamicAttributeNS;

    if-eqz v11, :cond_2

    move-object v2, p2

    check-cast v2, Lorg/apache/tools/ant/DynamicAttributeNS;

    invoke-static/range {p3 .. p3}, Lorg/apache/tools/ant/ProjectHelper;->extractUriFromComponentName(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v10

    invoke-static {v10}, Lorg/apache/tools/ant/ProjectHelper;->extractUriFromComponentName(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v9

    invoke-static/range {p3 .. p3}, Lorg/apache/tools/ant/ProjectHelper;->extractNameFromComponentName(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v5

    const-string v11, ""

    invoke-virtual {v11, v9}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v11

    if-eqz v11, :cond_1

    move-object v7, v5

    :goto_0
    move-object/from16 v0, p4

    invoke-interface {v2, v9, v5, v7, v0}, Lorg/apache/tools/ant/DynamicAttributeNS;->setDynamicAttribute(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    :cond_0
    :goto_1
    return-void

    :cond_1
    new-instance v11, Ljava/lang/StringBuffer;

    invoke-direct {v11}, Ljava/lang/StringBuffer;-><init>()V

    invoke-virtual {v11, v9}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    move-result-object v11

    const-string v12, ":"

    invoke-virtual {v11, v12}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    move-result-object v11

    invoke-virtual {v11, v5}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    move-result-object v11

    invoke-virtual {v11}, Ljava/lang/StringBuffer;->toString()Ljava/lang/String;

    move-result-object v7

    goto :goto_0

    :cond_2
    instance-of v11, p2, Lorg/apache/tools/ant/DynamicAttribute;

    if-eqz v11, :cond_3

    move-object v2, p2

    check-cast v2, Lorg/apache/tools/ant/DynamicAttribute;

    sget-object v11, Ljava/util/Locale;->US:Ljava/util/Locale;

    move-object/from16 v0, p3

    invoke-virtual {v0, v11}, Ljava/lang/String;->toLowerCase(Ljava/util/Locale;)Ljava/lang/String;

    move-result-object v11

    move-object/from16 v0, p4

    invoke-interface {v2, v11, v0}, Lorg/apache/tools/ant/DynamicAttribute;->setDynamicAttribute(Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_1

    :cond_3
    const/16 v11, 0x3a

    move-object/from16 v0, p3

    invoke-virtual {v0, v11}, Ljava/lang/String;->indexOf(I)I

    move-result v11

    const/4 v12, -0x1

    if-ne v11, v12, :cond_0

    new-instance v11, Ljava/lang/StringBuffer;

    invoke-direct {v11}, Ljava/lang/StringBuffer;-><init>()V

    invoke-virtual {p0, p1, p2}, Lorg/apache/tools/ant/IntrospectionHelper;->getElementName(Lorg/apache/tools/ant/Project;Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v12

    invoke-virtual {v11, v12}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    move-result-object v11

    const-string v12, " doesn\'t support the \""

    invoke-virtual {v11, v12}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    move-result-object v11

    move-object/from16 v0, p3

    invoke-virtual {v11, v0}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    move-result-object v11

    const-string v12, "\" attribute."

    invoke-virtual {v11, v12}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    move-result-object v11

    invoke-virtual {v11}, Ljava/lang/StringBuffer;->toString()Ljava/lang/String;

    move-result-object v6

    new-instance v11, Lorg/apache/tools/ant/UnsupportedAttributeException;

    move-object/from16 v0, p3

    invoke-direct {v11, v6, v0}, Lorg/apache/tools/ant/UnsupportedAttributeException;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    throw v11

    :cond_4
    :try_start_0
    move-object/from16 v0, p4

    invoke-virtual {v1, p1, p2, v0}, Lorg/apache/tools/ant/IntrospectionHelper$AttributeSetter;->set(Lorg/apache/tools/ant/Project;Ljava/lang/Object;Ljava/lang/String;)V
    :try_end_0
    .catch Ljava/lang/IllegalAccessException; {:try_start_0 .. :try_end_0} :catch_0
    .catch Ljava/lang/reflect/InvocationTargetException; {:try_start_0 .. :try_end_0} :catch_1

    goto :goto_1

    :catch_0
    move-exception v3

    new-instance v11, Lorg/apache/tools/ant/BuildException;

    invoke-direct {v11, v3}, Lorg/apache/tools/ant/BuildException;-><init>(Ljava/lang/Throwable;)V

    throw v11

    :catch_1
    move-exception v4

    invoke-virtual {v4}, Ljava/lang/reflect/InvocationTargetException;->getTargetException()Ljava/lang/Throwable;

    move-result-object v8

    instance-of v11, v8, Lorg/apache/tools/ant/BuildException;

    if-eqz v11, :cond_5

    check-cast v8, Lorg/apache/tools/ant/BuildException;

    throw v8

    :cond_5
    new-instance v11, Lorg/apache/tools/ant/BuildException;

    invoke-direct {v11, v8}, Lorg/apache/tools/ant/BuildException;-><init>(Ljava/lang/Throwable;)V

    throw v11
.end method

.method public storeElement(Lorg/apache/tools/ant/Project;Ljava/lang/Object;Ljava/lang/Object;Ljava/lang/String;)V
    .locals 7
    .param p1    # Lorg/apache/tools/ant/Project;
    .param p2    # Ljava/lang/Object;
    .param p3    # Ljava/lang/Object;
    .param p4    # Ljava/lang/String;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lorg/apache/tools/ant/BuildException;
        }
    .end annotation

    if-nez p4, :cond_1

    :cond_0
    :goto_0
    return-void

    :cond_1
    iget-object v5, p0, Lorg/apache/tools/ant/IntrospectionHelper;->nestedCreators:Ljava/util/Hashtable;

    sget-object v6, Ljava/util/Locale;->US:Ljava/util/Locale;

    invoke-virtual {p4, v6}, Ljava/lang/String;->toLowerCase(Ljava/util/Locale;)Ljava/lang/String;

    move-result-object v6

    invoke-virtual {v5, v6}, Ljava/util/Hashtable;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Lorg/apache/tools/ant/IntrospectionHelper$NestedCreator;

    if-eqz v3, :cond_0

    :try_start_0
    invoke-virtual {v3, p2, p3}, Lorg/apache/tools/ant/IntrospectionHelper$NestedCreator;->store(Ljava/lang/Object;Ljava/lang/Object;)V
    :try_end_0
    .catch Ljava/lang/IllegalAccessException; {:try_start_0 .. :try_end_0} :catch_0
    .catch Ljava/lang/InstantiationException; {:try_start_0 .. :try_end_0} :catch_1
    .catch Ljava/lang/reflect/InvocationTargetException; {:try_start_0 .. :try_end_0} :catch_2

    goto :goto_0

    :catch_0
    move-exception v0

    new-instance v5, Lorg/apache/tools/ant/BuildException;

    invoke-direct {v5, v0}, Lorg/apache/tools/ant/BuildException;-><init>(Ljava/lang/Throwable;)V

    throw v5

    :catch_1
    move-exception v1

    new-instance v5, Lorg/apache/tools/ant/BuildException;

    invoke-direct {v5, v1}, Lorg/apache/tools/ant/BuildException;-><init>(Ljava/lang/Throwable;)V

    throw v5

    :catch_2
    move-exception v2

    invoke-virtual {v2}, Ljava/lang/reflect/InvocationTargetException;->getTargetException()Ljava/lang/Throwable;

    move-result-object v4

    instance-of v5, v4, Lorg/apache/tools/ant/BuildException;

    if-eqz v5, :cond_2

    check-cast v4, Lorg/apache/tools/ant/BuildException;

    throw v4

    :cond_2
    new-instance v5, Lorg/apache/tools/ant/BuildException;

    invoke-direct {v5, v4}, Lorg/apache/tools/ant/BuildException;-><init>(Ljava/lang/Throwable;)V

    throw v5
.end method

.method public supportsCharacters()Z
    .locals 1

    iget-object v0, p0, Lorg/apache/tools/ant/IntrospectionHelper;->addText:Ljava/lang/reflect/Method;

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public supportsNestedElement(Ljava/lang/String;)Z
    .locals 2
    .param p1    # Ljava/lang/String;

    iget-object v0, p0, Lorg/apache/tools/ant/IntrospectionHelper;->nestedCreators:Ljava/util/Hashtable;

    sget-object v1, Ljava/util/Locale;->US:Ljava/util/Locale;

    invoke-virtual {p1, v1}, Ljava/lang/String;->toLowerCase(Ljava/util/Locale;)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/util/Hashtable;->containsKey(Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_0

    invoke-virtual {p0}, Lorg/apache/tools/ant/IntrospectionHelper;->isDynamic()Z

    move-result v0

    if-nez v0, :cond_0

    iget-object v0, p0, Lorg/apache/tools/ant/IntrospectionHelper;->addTypeMethods:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->size()I

    move-result v0

    if-eqz v0, :cond_1

    :cond_0
    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_1
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public supportsNestedElement(Ljava/lang/String;Ljava/lang/String;)Z
    .locals 4
    .param p1    # Ljava/lang/String;
    .param p2    # Ljava/lang/String;

    const-string v2, "antlib:org.apache.tools.ant"

    invoke-virtual {p1, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_0

    const-string p1, ""

    :cond_0
    invoke-static {p2}, Lorg/apache/tools/ant/ProjectHelper;->extractUriFromComponentName(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    const-string v2, "antlib:org.apache.tools.ant"

    invoke-virtual {v1, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_1

    const-string v1, ""

    :cond_1
    invoke-static {p2}, Lorg/apache/tools/ant/ProjectHelper;->extractNameFromComponentName(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    iget-object v2, p0, Lorg/apache/tools/ant/IntrospectionHelper;->nestedCreators:Ljava/util/Hashtable;

    sget-object v3, Ljava/util/Locale;->US:Ljava/util/Locale;

    invoke-virtual {v0, v3}, Ljava/lang/String;->toLowerCase(Ljava/util/Locale;)Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Ljava/util/Hashtable;->containsKey(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_2

    invoke-virtual {v1, p1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-nez v2, :cond_3

    const-string v2, ""

    invoke-virtual {v2, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-nez v2, :cond_3

    :cond_2
    invoke-virtual {p0}, Lorg/apache/tools/ant/IntrospectionHelper;->isDynamic()Z

    move-result v2

    if-nez v2, :cond_3

    iget-object v2, p0, Lorg/apache/tools/ant/IntrospectionHelper;->addTypeMethods:Ljava/util/List;

    invoke-interface {v2}, Ljava/util/List;->size()I

    move-result v2

    if-eqz v2, :cond_4

    :cond_3
    const/4 v2, 0x1

    :goto_0
    return v2

    :cond_4
    const/4 v2, 0x0

    goto :goto_0
.end method

.method public throwNotSupported(Lorg/apache/tools/ant/Project;Ljava/lang/Object;Ljava/lang/String;)V
    .locals 3
    .param p1    # Lorg/apache/tools/ant/Project;
    .param p2    # Ljava/lang/Object;
    .param p3    # Ljava/lang/String;

    new-instance v1, Ljava/lang/StringBuffer;

    invoke-direct {v1}, Ljava/lang/StringBuffer;-><init>()V

    invoke-virtual {p1, p2}, Lorg/apache/tools/ant/Project;->getElementName(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    move-result-object v1

    const-string v2, " doesn\'t support the nested \""

    invoke-virtual {v1, v2}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    move-result-object v1

    invoke-virtual {v1, p3}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    move-result-object v1

    const-string v2, "\" element."

    invoke-virtual {v1, v2}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuffer;->toString()Ljava/lang/String;

    move-result-object v0

    new-instance v1, Lorg/apache/tools/ant/UnsupportedElementException;

    invoke-direct {v1, v0, p3}, Lorg/apache/tools/ant/UnsupportedElementException;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    throw v1
.end method
