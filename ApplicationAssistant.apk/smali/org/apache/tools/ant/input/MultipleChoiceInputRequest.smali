.class public Lorg/apache/tools/ant/input/MultipleChoiceInputRequest;
.super Lorg/apache/tools/ant/input/InputRequest;
.source "MultipleChoiceInputRequest.java"


# instance fields
.field private choices:Ljava/util/Vector;


# direct methods
.method public constructor <init>(Ljava/lang/String;Ljava/util/Vector;)V
    .locals 2
    .param p1    # Ljava/lang/String;
    .param p2    # Ljava/util/Vector;

    invoke-direct {p0, p1}, Lorg/apache/tools/ant/input/InputRequest;-><init>(Ljava/lang/String;)V

    new-instance v0, Ljava/util/Vector;

    invoke-direct {v0}, Ljava/util/Vector;-><init>()V

    iput-object v0, p0, Lorg/apache/tools/ant/input/MultipleChoiceInputRequest;->choices:Ljava/util/Vector;

    if-nez p2, :cond_0

    new-instance v0, Ljava/lang/IllegalArgumentException;

    const-string v1, "choices must not be null"

    invoke-direct {v0, v1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v0

    :cond_0
    iput-object p2, p0, Lorg/apache/tools/ant/input/MultipleChoiceInputRequest;->choices:Ljava/util/Vector;

    return-void
.end method


# virtual methods
.method public getChoices()Ljava/util/Vector;
    .locals 1

    iget-object v0, p0, Lorg/apache/tools/ant/input/MultipleChoiceInputRequest;->choices:Ljava/util/Vector;

    return-object v0
.end method

.method public isInputValid()Z
    .locals 2

    iget-object v0, p0, Lorg/apache/tools/ant/input/MultipleChoiceInputRequest;->choices:Ljava/util/Vector;

    invoke-virtual {p0}, Lorg/apache/tools/ant/input/MultipleChoiceInputRequest;->getInput()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/util/Vector;->contains(Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_0

    const-string v0, ""

    invoke-virtual {p0}, Lorg/apache/tools/ant/input/MultipleChoiceInputRequest;->getInput()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_1

    invoke-virtual {p0}, Lorg/apache/tools/ant/input/MultipleChoiceInputRequest;->getDefaultValue()Ljava/lang/String;

    move-result-object v0

    if-eqz v0, :cond_1

    :cond_0
    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_1
    const/4 v0, 0x0

    goto :goto_0
.end method
