.class public Lorg/apache/tools/ant/XmlLogger;
.super Ljava/lang/Object;
.source "XmlLogger.java"

# interfaces
.implements Lorg/apache/tools/ant/BuildLogger;


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lorg/apache/tools/ant/XmlLogger$1;,
        Lorg/apache/tools/ant/XmlLogger$TimedElement;
    }
.end annotation


# static fields
.field private static final BUILD_TAG:Ljava/lang/String; = "build"

.field private static final ERROR_ATTR:Ljava/lang/String; = "error"

.field private static final LOCATION_ATTR:Ljava/lang/String; = "location"

.field private static final MESSAGE_TAG:Ljava/lang/String; = "message"

.field private static final NAME_ATTR:Ljava/lang/String; = "name"

.field private static final PRIORITY_ATTR:Ljava/lang/String; = "priority"

.field private static final STACKTRACE_TAG:Ljava/lang/String; = "stacktrace"

.field private static final TARGET_TAG:Ljava/lang/String; = "target"

.field private static final TASK_TAG:Ljava/lang/String; = "task"

.field private static final TIME_ATTR:Ljava/lang/String; = "time"

.field private static builder:Ljavax/xml/parsers/DocumentBuilder;


# instance fields
.field private buildElement:Lorg/apache/tools/ant/XmlLogger$TimedElement;

.field private doc:Lorg/w3c/dom/Document;

.field private msgOutputLevel:I

.field private outStream:Ljava/io/PrintStream;

.field private targets:Ljava/util/Hashtable;

.field private tasks:Ljava/util/Hashtable;

.field private threadStacks:Ljava/util/Hashtable;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    invoke-static {}, Lorg/apache/tools/ant/XmlLogger;->getDocumentBuilder()Ljavax/xml/parsers/DocumentBuilder;

    move-result-object v0

    sput-object v0, Lorg/apache/tools/ant/XmlLogger;->builder:Ljavax/xml/parsers/DocumentBuilder;

    return-void
.end method

.method public constructor <init>()V
    .locals 1

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    const/4 v0, 0x4

    iput v0, p0, Lorg/apache/tools/ant/XmlLogger;->msgOutputLevel:I

    sget-object v0, Lorg/apache/tools/ant/XmlLogger;->builder:Ljavax/xml/parsers/DocumentBuilder;

    invoke-virtual {v0}, Ljavax/xml/parsers/DocumentBuilder;->newDocument()Lorg/w3c/dom/Document;

    move-result-object v0

    iput-object v0, p0, Lorg/apache/tools/ant/XmlLogger;->doc:Lorg/w3c/dom/Document;

    new-instance v0, Ljava/util/Hashtable;

    invoke-direct {v0}, Ljava/util/Hashtable;-><init>()V

    iput-object v0, p0, Lorg/apache/tools/ant/XmlLogger;->tasks:Ljava/util/Hashtable;

    new-instance v0, Ljava/util/Hashtable;

    invoke-direct {v0}, Ljava/util/Hashtable;-><init>()V

    iput-object v0, p0, Lorg/apache/tools/ant/XmlLogger;->targets:Ljava/util/Hashtable;

    new-instance v0, Ljava/util/Hashtable;

    invoke-direct {v0}, Ljava/util/Hashtable;-><init>()V

    iput-object v0, p0, Lorg/apache/tools/ant/XmlLogger;->threadStacks:Ljava/util/Hashtable;

    const/4 v0, 0x0

    iput-object v0, p0, Lorg/apache/tools/ant/XmlLogger;->buildElement:Lorg/apache/tools/ant/XmlLogger$TimedElement;

    return-void
.end method

.method private static getDocumentBuilder()Ljavax/xml/parsers/DocumentBuilder;
    .locals 2

    :try_start_0
    invoke-static {}, Ljavax/xml/parsers/DocumentBuilderFactory;->newInstance()Ljavax/xml/parsers/DocumentBuilderFactory;

    move-result-object v1

    invoke-virtual {v1}, Ljavax/xml/parsers/DocumentBuilderFactory;->newDocumentBuilder()Ljavax/xml/parsers/DocumentBuilder;
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    move-result-object v1

    return-object v1

    :catch_0
    move-exception v0

    new-instance v1, Ljava/lang/ExceptionInInitializerError;

    invoke-direct {v1, v0}, Ljava/lang/ExceptionInInitializerError;-><init>(Ljava/lang/Throwable;)V

    throw v1
.end method

.method private getStack()Ljava/util/Stack;
    .locals 3

    iget-object v1, p0, Lorg/apache/tools/ant/XmlLogger;->threadStacks:Ljava/util/Hashtable;

    invoke-static {}, Ljava/lang/Thread;->currentThread()Ljava/lang/Thread;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/util/Hashtable;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/util/Stack;

    if-nez v0, :cond_0

    new-instance v0, Ljava/util/Stack;

    invoke-direct {v0}, Ljava/util/Stack;-><init>()V

    iget-object v1, p0, Lorg/apache/tools/ant/XmlLogger;->threadStacks:Ljava/util/Hashtable;

    invoke-static {}, Ljava/lang/Thread;->currentThread()Ljava/lang/Thread;

    move-result-object v2

    invoke-virtual {v1, v2, v0}, Ljava/util/Hashtable;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    :cond_0
    return-object v0
.end method

.method private getTaskElement(Lorg/apache/tools/ant/Task;)Lorg/apache/tools/ant/XmlLogger$TimedElement;
    .locals 4
    .param p1    # Lorg/apache/tools/ant/Task;

    iget-object v3, p0, Lorg/apache/tools/ant/XmlLogger;->tasks:Ljava/util/Hashtable;

    invoke-virtual {v3, p1}, Ljava/util/Hashtable;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lorg/apache/tools/ant/XmlLogger$TimedElement;

    if-eqz v1, :cond_0

    :goto_0
    return-object v1

    :cond_0
    iget-object v3, p0, Lorg/apache/tools/ant/XmlLogger;->tasks:Ljava/util/Hashtable;

    invoke-virtual {v3}, Ljava/util/Hashtable;->keys()Ljava/util/Enumeration;

    move-result-object v0

    :cond_1
    invoke-interface {v0}, Ljava/util/Enumeration;->hasMoreElements()Z

    move-result v3

    if-eqz v3, :cond_2

    invoke-interface {v0}, Ljava/util/Enumeration;->nextElement()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lorg/apache/tools/ant/Task;

    instance-of v3, v2, Lorg/apache/tools/ant/UnknownElement;

    if-eqz v3, :cond_1

    move-object v3, v2

    check-cast v3, Lorg/apache/tools/ant/UnknownElement;

    invoke-virtual {v3}, Lorg/apache/tools/ant/UnknownElement;->getTask()Lorg/apache/tools/ant/Task;

    move-result-object v3

    if-ne v3, p1, :cond_1

    iget-object v3, p0, Lorg/apache/tools/ant/XmlLogger;->tasks:Ljava/util/Hashtable;

    invoke-virtual {v3, v2}, Ljava/util/Hashtable;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Lorg/apache/tools/ant/XmlLogger$TimedElement;

    move-object v1, v3

    goto :goto_0

    :cond_2
    const/4 v1, 0x0

    goto :goto_0
.end method


# virtual methods
.method public buildFinished(Lorg/apache/tools/ant/BuildEvent;)V
    .locals 15
    .param p1    # Lorg/apache/tools/ant/BuildEvent;

    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v11

    iget-object v13, p0, Lorg/apache/tools/ant/XmlLogger;->buildElement:Lorg/apache/tools/ant/XmlLogger$TimedElement;

    invoke-static {v13}, Lorg/apache/tools/ant/XmlLogger$TimedElement;->access$100(Lorg/apache/tools/ant/XmlLogger$TimedElement;)J

    move-result-wide v13

    sub-long v8, v11, v13

    iget-object v11, p0, Lorg/apache/tools/ant/XmlLogger;->buildElement:Lorg/apache/tools/ant/XmlLogger$TimedElement;

    invoke-static {v11}, Lorg/apache/tools/ant/XmlLogger$TimedElement;->access$200(Lorg/apache/tools/ant/XmlLogger$TimedElement;)Lorg/w3c/dom/Element;

    move-result-object v11

    const-string v12, "time"

    invoke-static {v8, v9}, Lorg/apache/tools/ant/DefaultLogger;->formatTime(J)Ljava/lang/String;

    move-result-object v13

    invoke-interface {v11, v12, v13}, Lorg/w3c/dom/Element;->setAttribute(Ljava/lang/String;Ljava/lang/String;)V

    invoke-virtual/range {p1 .. p1}, Lorg/apache/tools/ant/BuildEvent;->getException()Ljava/lang/Throwable;

    move-result-object v11

    if-eqz v11, :cond_0

    iget-object v11, p0, Lorg/apache/tools/ant/XmlLogger;->buildElement:Lorg/apache/tools/ant/XmlLogger$TimedElement;

    invoke-static {v11}, Lorg/apache/tools/ant/XmlLogger$TimedElement;->access$200(Lorg/apache/tools/ant/XmlLogger$TimedElement;)Lorg/w3c/dom/Element;

    move-result-object v11

    const-string v12, "error"

    invoke-virtual/range {p1 .. p1}, Lorg/apache/tools/ant/BuildEvent;->getException()Ljava/lang/Throwable;

    move-result-object v13

    invoke-virtual {v13}, Ljava/lang/Throwable;->toString()Ljava/lang/String;

    move-result-object v13

    invoke-interface {v11, v12, v13}, Lorg/w3c/dom/Element;->setAttribute(Ljava/lang/String;Ljava/lang/String;)V

    invoke-virtual/range {p1 .. p1}, Lorg/apache/tools/ant/BuildEvent;->getException()Ljava/lang/Throwable;

    move-result-object v7

    iget-object v11, p0, Lorg/apache/tools/ant/XmlLogger;->doc:Lorg/w3c/dom/Document;

    invoke-static {v7}, Lorg/apache/tools/ant/util/StringUtils;->getStackTrace(Ljava/lang/Throwable;)Ljava/lang/String;

    move-result-object v12

    invoke-interface {v11, v12}, Lorg/w3c/dom/Document;->createCDATASection(Ljava/lang/String;)Lorg/w3c/dom/CDATASection;

    move-result-object v0

    iget-object v11, p0, Lorg/apache/tools/ant/XmlLogger;->doc:Lorg/w3c/dom/Document;

    const-string v12, "stacktrace"

    invoke-interface {v11, v12}, Lorg/w3c/dom/Document;->createElement(Ljava/lang/String;)Lorg/w3c/dom/Element;

    move-result-object v5

    invoke-interface {v5, v0}, Lorg/w3c/dom/Element;->appendChild(Lorg/w3c/dom/Node;)Lorg/w3c/dom/Node;

    iget-object v11, p0, Lorg/apache/tools/ant/XmlLogger;->buildElement:Lorg/apache/tools/ant/XmlLogger$TimedElement;

    invoke-static {v11}, Lorg/apache/tools/ant/XmlLogger$TimedElement;->access$200(Lorg/apache/tools/ant/XmlLogger$TimedElement;)Lorg/w3c/dom/Element;

    move-result-object v11

    invoke-interface {v11, v5}, Lorg/w3c/dom/Element;->appendChild(Lorg/w3c/dom/Node;)Lorg/w3c/dom/Node;

    :cond_0
    invoke-virtual/range {p1 .. p1}, Lorg/apache/tools/ant/BuildEvent;->getProject()Lorg/apache/tools/ant/Project;

    move-result-object v11

    const-string v12, "XmlLogger.file"

    invoke-virtual {v11, v12}, Lorg/apache/tools/ant/Project;->getProperty(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v4

    if-nez v4, :cond_1

    const-string v4, "log.xml"

    :cond_1
    invoke-virtual/range {p1 .. p1}, Lorg/apache/tools/ant/BuildEvent;->getProject()Lorg/apache/tools/ant/Project;

    move-result-object v11

    const-string v12, "ant.XmlLogger.stylesheet.uri"

    invoke-virtual {v11, v12}, Lorg/apache/tools/ant/Project;->getProperty(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v10

    if-nez v10, :cond_2

    const-string v10, "log.xsl"

    :cond_2
    const/4 v2, 0x0

    :try_start_0
    iget-object v6, p0, Lorg/apache/tools/ant/XmlLogger;->outStream:Ljava/io/PrintStream;

    if-nez v6, :cond_3

    new-instance v6, Ljava/io/FileOutputStream;

    invoke-direct {v6, v4}, Ljava/io/FileOutputStream;-><init>(Ljava/lang/String;)V

    :cond_3
    new-instance v3, Ljava/io/OutputStreamWriter;

    const-string v11, "UTF8"

    invoke-direct {v3, v6, v11}, Ljava/io/OutputStreamWriter;-><init>(Ljava/io/OutputStream;Ljava/lang/String;)V
    :try_end_0
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    :try_start_1
    const-string v11, "<?xml version=\"1.0\" encoding=\"UTF-8\"?>\n"

    invoke-virtual {v3, v11}, Ljava/io/Writer;->write(Ljava/lang/String;)V

    invoke-virtual {v10}, Ljava/lang/String;->length()I

    move-result v11

    if-lez v11, :cond_4

    new-instance v11, Ljava/lang/StringBuffer;

    invoke-direct {v11}, Ljava/lang/StringBuffer;-><init>()V

    const-string v12, "<?xml-stylesheet type=\"text/xsl\" href=\""

    invoke-virtual {v11, v12}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    move-result-object v11

    invoke-virtual {v11, v10}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    move-result-object v11

    const-string v12, "\"?>\n\n"

    invoke-virtual {v11, v12}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    move-result-object v11

    invoke-virtual {v11}, Ljava/lang/StringBuffer;->toString()Ljava/lang/String;

    move-result-object v11

    invoke-virtual {v3, v11}, Ljava/io/Writer;->write(Ljava/lang/String;)V

    :cond_4
    new-instance v11, Lorg/apache/tools/ant/util/DOMElementWriter;

    invoke-direct {v11}, Lorg/apache/tools/ant/util/DOMElementWriter;-><init>()V

    iget-object v12, p0, Lorg/apache/tools/ant/XmlLogger;->buildElement:Lorg/apache/tools/ant/XmlLogger$TimedElement;

    invoke-static {v12}, Lorg/apache/tools/ant/XmlLogger$TimedElement;->access$200(Lorg/apache/tools/ant/XmlLogger$TimedElement;)Lorg/w3c/dom/Element;

    move-result-object v12

    const/4 v13, 0x0

    const-string v14, "\t"

    invoke-virtual {v11, v12, v3, v13, v14}, Lorg/apache/tools/ant/util/DOMElementWriter;->write(Lorg/w3c/dom/Element;Ljava/io/Writer;ILjava/lang/String;)V

    invoke-virtual {v3}, Ljava/io/Writer;->flush()V
    :try_end_1
    .catch Ljava/io/IOException; {:try_start_1 .. :try_end_1} :catch_3
    .catchall {:try_start_1 .. :try_end_1} :catchall_1

    if-eqz v3, :cond_5

    :try_start_2
    invoke-virtual {v3}, Ljava/io/Writer;->close()V
    :try_end_2
    .catch Ljava/io/IOException; {:try_start_2 .. :try_end_2} :catch_1

    :cond_5
    :goto_0
    const/4 v11, 0x0

    iput-object v11, p0, Lorg/apache/tools/ant/XmlLogger;->buildElement:Lorg/apache/tools/ant/XmlLogger$TimedElement;

    return-void

    :catch_0
    move-exception v1

    :goto_1
    :try_start_3
    new-instance v11, Lorg/apache/tools/ant/BuildException;

    const-string v12, "Unable to write log file"

    invoke-direct {v11, v12, v1}, Lorg/apache/tools/ant/BuildException;-><init>(Ljava/lang/String;Ljava/lang/Throwable;)V

    throw v11
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_0

    :catchall_0
    move-exception v11

    :goto_2
    if-eqz v2, :cond_6

    :try_start_4
    invoke-virtual {v2}, Ljava/io/Writer;->close()V
    :try_end_4
    .catch Ljava/io/IOException; {:try_start_4 .. :try_end_4} :catch_2

    :cond_6
    :goto_3
    throw v11

    :catch_1
    move-exception v11

    goto :goto_0

    :catch_2
    move-exception v12

    goto :goto_3

    :catchall_1
    move-exception v11

    move-object v2, v3

    goto :goto_2

    :catch_3
    move-exception v1

    move-object v2, v3

    goto :goto_1
.end method

.method public buildStarted(Lorg/apache/tools/ant/BuildEvent;)V
    .locals 3
    .param p1    # Lorg/apache/tools/ant/BuildEvent;

    new-instance v0, Lorg/apache/tools/ant/XmlLogger$TimedElement;

    const/4 v1, 0x0

    invoke-direct {v0, v1}, Lorg/apache/tools/ant/XmlLogger$TimedElement;-><init>(Lorg/apache/tools/ant/XmlLogger$1;)V

    iput-object v0, p0, Lorg/apache/tools/ant/XmlLogger;->buildElement:Lorg/apache/tools/ant/XmlLogger$TimedElement;

    iget-object v0, p0, Lorg/apache/tools/ant/XmlLogger;->buildElement:Lorg/apache/tools/ant/XmlLogger$TimedElement;

    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v1

    invoke-static {v0, v1, v2}, Lorg/apache/tools/ant/XmlLogger$TimedElement;->access$102(Lorg/apache/tools/ant/XmlLogger$TimedElement;J)J

    iget-object v0, p0, Lorg/apache/tools/ant/XmlLogger;->buildElement:Lorg/apache/tools/ant/XmlLogger$TimedElement;

    iget-object v1, p0, Lorg/apache/tools/ant/XmlLogger;->doc:Lorg/w3c/dom/Document;

    const-string v2, "build"

    invoke-interface {v1, v2}, Lorg/w3c/dom/Document;->createElement(Ljava/lang/String;)Lorg/w3c/dom/Element;

    move-result-object v1

    invoke-static {v0, v1}, Lorg/apache/tools/ant/XmlLogger$TimedElement;->access$202(Lorg/apache/tools/ant/XmlLogger$TimedElement;Lorg/w3c/dom/Element;)Lorg/w3c/dom/Element;

    return-void
.end method

.method public messageLogged(Lorg/apache/tools/ant/BuildEvent;)V
    .locals 12
    .param p1    # Lorg/apache/tools/ant/BuildEvent;

    invoke-virtual {p1}, Lorg/apache/tools/ant/BuildEvent;->getPriority()I

    move-result v6

    iget v10, p0, Lorg/apache/tools/ant/XmlLogger;->msgOutputLevel:I

    if-le v6, v10, :cond_0

    :goto_0
    return-void

    :cond_0
    iget-object v10, p0, Lorg/apache/tools/ant/XmlLogger;->doc:Lorg/w3c/dom/Document;

    const-string v11, "message"

    invoke-interface {v10, v11}, Lorg/w3c/dom/Document;->createElement(Ljava/lang/String;)Lorg/w3c/dom/Element;

    move-result-object v2

    const-string v4, "debug"

    invoke-virtual {p1}, Lorg/apache/tools/ant/BuildEvent;->getPriority()I

    move-result v10

    packed-switch v10, :pswitch_data_0

    const-string v4, "debug"

    :goto_1
    const-string v10, "priority"

    invoke-interface {v2, v10, v4}, Lorg/w3c/dom/Element;->setAttribute(Ljava/lang/String;Ljava/lang/String;)V

    invoke-virtual {p1}, Lorg/apache/tools/ant/BuildEvent;->getException()Ljava/lang/Throwable;

    move-result-object v1

    const/4 v10, 0x4

    iget v11, p0, Lorg/apache/tools/ant/XmlLogger;->msgOutputLevel:I

    if-gt v10, v11, :cond_1

    if-eqz v1, :cond_1

    iget-object v10, p0, Lorg/apache/tools/ant/XmlLogger;->doc:Lorg/w3c/dom/Document;

    invoke-static {v1}, Lorg/apache/tools/ant/util/StringUtils;->getStackTrace(Ljava/lang/Throwable;)Ljava/lang/String;

    move-result-object v11

    invoke-interface {v10, v11}, Lorg/w3c/dom/Document;->createCDATASection(Ljava/lang/String;)Lorg/w3c/dom/CDATASection;

    move-result-object v0

    iget-object v10, p0, Lorg/apache/tools/ant/XmlLogger;->doc:Lorg/w3c/dom/Document;

    const-string v11, "stacktrace"

    invoke-interface {v10, v11}, Lorg/w3c/dom/Document;->createElement(Ljava/lang/String;)Lorg/w3c/dom/Element;

    move-result-object v7

    invoke-interface {v7, v0}, Lorg/w3c/dom/Element;->appendChild(Lorg/w3c/dom/Node;)Lorg/w3c/dom/Node;

    iget-object v10, p0, Lorg/apache/tools/ant/XmlLogger;->buildElement:Lorg/apache/tools/ant/XmlLogger$TimedElement;

    invoke-static {v10}, Lorg/apache/tools/ant/XmlLogger$TimedElement;->access$200(Lorg/apache/tools/ant/XmlLogger$TimedElement;)Lorg/w3c/dom/Element;

    move-result-object v10

    invoke-interface {v10, v7}, Lorg/w3c/dom/Element;->appendChild(Lorg/w3c/dom/Node;)Lorg/w3c/dom/Node;

    :cond_1
    iget-object v10, p0, Lorg/apache/tools/ant/XmlLogger;->doc:Lorg/w3c/dom/Document;

    invoke-virtual {p1}, Lorg/apache/tools/ant/BuildEvent;->getMessage()Ljava/lang/String;

    move-result-object v11

    invoke-interface {v10, v11}, Lorg/w3c/dom/Document;->createCDATASection(Ljava/lang/String;)Lorg/w3c/dom/CDATASection;

    move-result-object v3

    invoke-interface {v2, v3}, Lorg/w3c/dom/Element;->appendChild(Lorg/w3c/dom/Node;)Lorg/w3c/dom/Node;

    const/4 v5, 0x0

    invoke-virtual {p1}, Lorg/apache/tools/ant/BuildEvent;->getTask()Lorg/apache/tools/ant/Task;

    move-result-object v9

    invoke-virtual {p1}, Lorg/apache/tools/ant/BuildEvent;->getTarget()Lorg/apache/tools/ant/Target;

    move-result-object v8

    if-eqz v9, :cond_2

    invoke-direct {p0, v9}, Lorg/apache/tools/ant/XmlLogger;->getTaskElement(Lorg/apache/tools/ant/Task;)Lorg/apache/tools/ant/XmlLogger$TimedElement;

    move-result-object v5

    :cond_2
    if-nez v5, :cond_3

    if-eqz v8, :cond_3

    iget-object v10, p0, Lorg/apache/tools/ant/XmlLogger;->targets:Ljava/util/Hashtable;

    invoke-virtual {v10, v8}, Ljava/util/Hashtable;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v5

    check-cast v5, Lorg/apache/tools/ant/XmlLogger$TimedElement;

    :cond_3
    if-eqz v5, :cond_4

    invoke-static {v5}, Lorg/apache/tools/ant/XmlLogger$TimedElement;->access$200(Lorg/apache/tools/ant/XmlLogger$TimedElement;)Lorg/w3c/dom/Element;

    move-result-object v10

    invoke-interface {v10, v2}, Lorg/w3c/dom/Element;->appendChild(Lorg/w3c/dom/Node;)Lorg/w3c/dom/Node;

    goto :goto_0

    :pswitch_0
    const-string v4, "error"

    goto :goto_1

    :pswitch_1
    const-string v4, "warn"

    goto :goto_1

    :pswitch_2
    const-string v4, "info"

    goto :goto_1

    :cond_4
    iget-object v10, p0, Lorg/apache/tools/ant/XmlLogger;->buildElement:Lorg/apache/tools/ant/XmlLogger$TimedElement;

    invoke-static {v10}, Lorg/apache/tools/ant/XmlLogger$TimedElement;->access$200(Lorg/apache/tools/ant/XmlLogger$TimedElement;)Lorg/w3c/dom/Element;

    move-result-object v10

    invoke-interface {v10, v2}, Lorg/w3c/dom/Element;->appendChild(Lorg/w3c/dom/Node;)Lorg/w3c/dom/Node;

    goto/16 :goto_0

    :pswitch_data_0
    .packed-switch 0x0
        :pswitch_0
        :pswitch_1
        :pswitch_2
    .end packed-switch
.end method

.method public setEmacsMode(Z)V
    .locals 0
    .param p1    # Z

    return-void
.end method

.method public setErrorPrintStream(Ljava/io/PrintStream;)V
    .locals 0
    .param p1    # Ljava/io/PrintStream;

    return-void
.end method

.method public setMessageOutputLevel(I)V
    .locals 0
    .param p1    # I

    iput p1, p0, Lorg/apache/tools/ant/XmlLogger;->msgOutputLevel:I

    return-void
.end method

.method public setOutputPrintStream(Ljava/io/PrintStream;)V
    .locals 2
    .param p1    # Ljava/io/PrintStream;

    new-instance v0, Ljava/io/PrintStream;

    const/4 v1, 0x1

    invoke-direct {v0, p1, v1}, Ljava/io/PrintStream;-><init>(Ljava/io/OutputStream;Z)V

    iput-object v0, p0, Lorg/apache/tools/ant/XmlLogger;->outStream:Ljava/io/PrintStream;

    return-void
.end method

.method public targetFinished(Lorg/apache/tools/ant/BuildEvent;)V
    .locals 11
    .param p1    # Lorg/apache/tools/ant/BuildEvent;

    invoke-virtual {p1}, Lorg/apache/tools/ant/BuildEvent;->getTarget()Lorg/apache/tools/ant/Target;

    move-result-object v2

    iget-object v7, p0, Lorg/apache/tools/ant/XmlLogger;->targets:Ljava/util/Hashtable;

    invoke-virtual {v7, v2}, Ljava/util/Hashtable;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Lorg/apache/tools/ant/XmlLogger$TimedElement;

    if-eqz v3, :cond_2

    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v7

    invoke-static {v3}, Lorg/apache/tools/ant/XmlLogger$TimedElement;->access$100(Lorg/apache/tools/ant/XmlLogger$TimedElement;)J

    move-result-wide v9

    sub-long v5, v7, v9

    invoke-static {v3}, Lorg/apache/tools/ant/XmlLogger$TimedElement;->access$200(Lorg/apache/tools/ant/XmlLogger$TimedElement;)Lorg/w3c/dom/Element;

    move-result-object v7

    const-string v8, "time"

    invoke-static {v5, v6}, Lorg/apache/tools/ant/DefaultLogger;->formatTime(J)Ljava/lang/String;

    move-result-object v9

    invoke-interface {v7, v8, v9}, Lorg/w3c/dom/Element;->setAttribute(Ljava/lang/String;Ljava/lang/String;)V

    const/4 v0, 0x0

    invoke-direct {p0}, Lorg/apache/tools/ant/XmlLogger;->getStack()Ljava/util/Stack;

    move-result-object v4

    invoke-virtual {v4}, Ljava/util/Stack;->empty()Z

    move-result v7

    if-nez v7, :cond_1

    invoke-virtual {v4}, Ljava/util/Stack;->pop()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lorg/apache/tools/ant/XmlLogger$TimedElement;

    if-eq v1, v3, :cond_0

    new-instance v7, Ljava/lang/RuntimeException;

    new-instance v8, Ljava/lang/StringBuffer;

    invoke-direct {v8}, Ljava/lang/StringBuffer;-><init>()V

    const-string v9, "Mismatch - popped element = "

    invoke-virtual {v8, v9}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    move-result-object v8

    invoke-virtual {v8, v1}, Ljava/lang/StringBuffer;->append(Ljava/lang/Object;)Ljava/lang/StringBuffer;

    move-result-object v8

    const-string v9, " finished target element = "

    invoke-virtual {v8, v9}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    move-result-object v8

    invoke-virtual {v8, v3}, Ljava/lang/StringBuffer;->append(Ljava/lang/Object;)Ljava/lang/StringBuffer;

    move-result-object v8

    invoke-virtual {v8}, Ljava/lang/StringBuffer;->toString()Ljava/lang/String;

    move-result-object v8

    invoke-direct {v7, v8}, Ljava/lang/RuntimeException;-><init>(Ljava/lang/String;)V

    throw v7

    :cond_0
    invoke-virtual {v4}, Ljava/util/Stack;->empty()Z

    move-result v7

    if-nez v7, :cond_1

    invoke-virtual {v4}, Ljava/util/Stack;->peek()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lorg/apache/tools/ant/XmlLogger$TimedElement;

    :cond_1
    if-nez v0, :cond_3

    iget-object v7, p0, Lorg/apache/tools/ant/XmlLogger;->buildElement:Lorg/apache/tools/ant/XmlLogger$TimedElement;

    invoke-static {v7}, Lorg/apache/tools/ant/XmlLogger$TimedElement;->access$200(Lorg/apache/tools/ant/XmlLogger$TimedElement;)Lorg/w3c/dom/Element;

    move-result-object v7

    invoke-static {v3}, Lorg/apache/tools/ant/XmlLogger$TimedElement;->access$200(Lorg/apache/tools/ant/XmlLogger$TimedElement;)Lorg/w3c/dom/Element;

    move-result-object v8

    invoke-interface {v7, v8}, Lorg/w3c/dom/Element;->appendChild(Lorg/w3c/dom/Node;)Lorg/w3c/dom/Node;

    :cond_2
    :goto_0
    iget-object v7, p0, Lorg/apache/tools/ant/XmlLogger;->targets:Ljava/util/Hashtable;

    invoke-virtual {v7, v2}, Ljava/util/Hashtable;->remove(Ljava/lang/Object;)Ljava/lang/Object;

    return-void

    :cond_3
    invoke-static {v0}, Lorg/apache/tools/ant/XmlLogger$TimedElement;->access$200(Lorg/apache/tools/ant/XmlLogger$TimedElement;)Lorg/w3c/dom/Element;

    move-result-object v7

    invoke-static {v3}, Lorg/apache/tools/ant/XmlLogger$TimedElement;->access$200(Lorg/apache/tools/ant/XmlLogger$TimedElement;)Lorg/w3c/dom/Element;

    move-result-object v8

    invoke-interface {v7, v8}, Lorg/w3c/dom/Element;->appendChild(Lorg/w3c/dom/Node;)Lorg/w3c/dom/Node;

    goto :goto_0
.end method

.method public targetStarted(Lorg/apache/tools/ant/BuildEvent;)V
    .locals 5
    .param p1    # Lorg/apache/tools/ant/BuildEvent;

    invoke-virtual {p1}, Lorg/apache/tools/ant/BuildEvent;->getTarget()Lorg/apache/tools/ant/Target;

    move-result-object v0

    new-instance v1, Lorg/apache/tools/ant/XmlLogger$TimedElement;

    const/4 v2, 0x0

    invoke-direct {v1, v2}, Lorg/apache/tools/ant/XmlLogger$TimedElement;-><init>(Lorg/apache/tools/ant/XmlLogger$1;)V

    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v2

    invoke-static {v1, v2, v3}, Lorg/apache/tools/ant/XmlLogger$TimedElement;->access$102(Lorg/apache/tools/ant/XmlLogger$TimedElement;J)J

    iget-object v2, p0, Lorg/apache/tools/ant/XmlLogger;->doc:Lorg/w3c/dom/Document;

    const-string v3, "target"

    invoke-interface {v2, v3}, Lorg/w3c/dom/Document;->createElement(Ljava/lang/String;)Lorg/w3c/dom/Element;

    move-result-object v2

    invoke-static {v1, v2}, Lorg/apache/tools/ant/XmlLogger$TimedElement;->access$202(Lorg/apache/tools/ant/XmlLogger$TimedElement;Lorg/w3c/dom/Element;)Lorg/w3c/dom/Element;

    invoke-static {v1}, Lorg/apache/tools/ant/XmlLogger$TimedElement;->access$200(Lorg/apache/tools/ant/XmlLogger$TimedElement;)Lorg/w3c/dom/Element;

    move-result-object v2

    const-string v3, "name"

    invoke-virtual {v0}, Lorg/apache/tools/ant/Target;->getName()Ljava/lang/String;

    move-result-object v4

    invoke-interface {v2, v3, v4}, Lorg/w3c/dom/Element;->setAttribute(Ljava/lang/String;Ljava/lang/String;)V

    iget-object v2, p0, Lorg/apache/tools/ant/XmlLogger;->targets:Ljava/util/Hashtable;

    invoke-virtual {v2, v0, v1}, Ljava/util/Hashtable;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    invoke-direct {p0}, Lorg/apache/tools/ant/XmlLogger;->getStack()Ljava/util/Stack;

    move-result-object v2

    invoke-virtual {v2, v1}, Ljava/util/Stack;->push(Ljava/lang/Object;)Ljava/lang/Object;

    return-void
.end method

.method public taskFinished(Lorg/apache/tools/ant/BuildEvent;)V
    .locals 12
    .param p1    # Lorg/apache/tools/ant/BuildEvent;

    invoke-virtual {p1}, Lorg/apache/tools/ant/BuildEvent;->getTask()Lorg/apache/tools/ant/Task;

    move-result-object v3

    iget-object v8, p0, Lorg/apache/tools/ant/XmlLogger;->tasks:Ljava/util/Hashtable;

    invoke-virtual {v8, v3}, Ljava/util/Hashtable;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v4

    check-cast v4, Lorg/apache/tools/ant/XmlLogger$TimedElement;

    if-eqz v4, :cond_3

    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v8

    invoke-static {v4}, Lorg/apache/tools/ant/XmlLogger$TimedElement;->access$100(Lorg/apache/tools/ant/XmlLogger$TimedElement;)J

    move-result-wide v10

    sub-long v6, v8, v10

    invoke-static {v4}, Lorg/apache/tools/ant/XmlLogger$TimedElement;->access$200(Lorg/apache/tools/ant/XmlLogger$TimedElement;)Lorg/w3c/dom/Element;

    move-result-object v8

    const-string v9, "time"

    invoke-static {v6, v7}, Lorg/apache/tools/ant/DefaultLogger;->formatTime(J)Ljava/lang/String;

    move-result-object v10

    invoke-interface {v8, v9, v10}, Lorg/w3c/dom/Element;->setAttribute(Ljava/lang/String;Ljava/lang/String;)V

    invoke-virtual {v3}, Lorg/apache/tools/ant/Task;->getOwningTarget()Lorg/apache/tools/ant/Target;

    move-result-object v1

    const/4 v2, 0x0

    if-eqz v1, :cond_0

    iget-object v8, p0, Lorg/apache/tools/ant/XmlLogger;->targets:Ljava/util/Hashtable;

    invoke-virtual {v8, v1}, Ljava/util/Hashtable;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lorg/apache/tools/ant/XmlLogger$TimedElement;

    :cond_0
    if-nez v2, :cond_1

    iget-object v8, p0, Lorg/apache/tools/ant/XmlLogger;->buildElement:Lorg/apache/tools/ant/XmlLogger$TimedElement;

    invoke-static {v8}, Lorg/apache/tools/ant/XmlLogger$TimedElement;->access$200(Lorg/apache/tools/ant/XmlLogger$TimedElement;)Lorg/w3c/dom/Element;

    move-result-object v8

    invoke-static {v4}, Lorg/apache/tools/ant/XmlLogger$TimedElement;->access$200(Lorg/apache/tools/ant/XmlLogger$TimedElement;)Lorg/w3c/dom/Element;

    move-result-object v9

    invoke-interface {v8, v9}, Lorg/w3c/dom/Element;->appendChild(Lorg/w3c/dom/Node;)Lorg/w3c/dom/Node;

    :goto_0
    invoke-direct {p0}, Lorg/apache/tools/ant/XmlLogger;->getStack()Ljava/util/Stack;

    move-result-object v5

    invoke-virtual {v5}, Ljava/util/Stack;->empty()Z

    move-result v8

    if-nez v8, :cond_2

    invoke-virtual {v5}, Ljava/util/Stack;->pop()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lorg/apache/tools/ant/XmlLogger$TimedElement;

    if-eq v0, v4, :cond_2

    new-instance v8, Ljava/lang/RuntimeException;

    new-instance v9, Ljava/lang/StringBuffer;

    invoke-direct {v9}, Ljava/lang/StringBuffer;-><init>()V

    const-string v10, "Mismatch - popped element = "

    invoke-virtual {v9, v10}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    move-result-object v9

    invoke-virtual {v9, v0}, Ljava/lang/StringBuffer;->append(Ljava/lang/Object;)Ljava/lang/StringBuffer;

    move-result-object v9

    const-string v10, " finished task element = "

    invoke-virtual {v9, v10}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    move-result-object v9

    invoke-virtual {v9, v4}, Ljava/lang/StringBuffer;->append(Ljava/lang/Object;)Ljava/lang/StringBuffer;

    move-result-object v9

    invoke-virtual {v9}, Ljava/lang/StringBuffer;->toString()Ljava/lang/String;

    move-result-object v9

    invoke-direct {v8, v9}, Ljava/lang/RuntimeException;-><init>(Ljava/lang/String;)V

    throw v8

    :cond_1
    invoke-static {v2}, Lorg/apache/tools/ant/XmlLogger$TimedElement;->access$200(Lorg/apache/tools/ant/XmlLogger$TimedElement;)Lorg/w3c/dom/Element;

    move-result-object v8

    invoke-static {v4}, Lorg/apache/tools/ant/XmlLogger$TimedElement;->access$200(Lorg/apache/tools/ant/XmlLogger$TimedElement;)Lorg/w3c/dom/Element;

    move-result-object v9

    invoke-interface {v8, v9}, Lorg/w3c/dom/Element;->appendChild(Lorg/w3c/dom/Node;)Lorg/w3c/dom/Node;

    goto :goto_0

    :cond_2
    iget-object v8, p0, Lorg/apache/tools/ant/XmlLogger;->tasks:Ljava/util/Hashtable;

    invoke-virtual {v8, v3}, Ljava/util/Hashtable;->remove(Ljava/lang/Object;)Ljava/lang/Object;

    return-void

    :cond_3
    new-instance v8, Ljava/lang/RuntimeException;

    new-instance v9, Ljava/lang/StringBuffer;

    invoke-direct {v9}, Ljava/lang/StringBuffer;-><init>()V

    const-string v10, "Unknown task "

    invoke-virtual {v9, v10}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    move-result-object v9

    invoke-virtual {v9, v3}, Ljava/lang/StringBuffer;->append(Ljava/lang/Object;)Ljava/lang/StringBuffer;

    move-result-object v9

    const-string v10, " not in "

    invoke-virtual {v9, v10}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    move-result-object v9

    iget-object v10, p0, Lorg/apache/tools/ant/XmlLogger;->tasks:Ljava/util/Hashtable;

    invoke-virtual {v9, v10}, Ljava/lang/StringBuffer;->append(Ljava/lang/Object;)Ljava/lang/StringBuffer;

    move-result-object v9

    invoke-virtual {v9}, Ljava/lang/StringBuffer;->toString()Ljava/lang/String;

    move-result-object v9

    invoke-direct {v8, v9}, Ljava/lang/RuntimeException;-><init>(Ljava/lang/String;)V

    throw v8
.end method

.method public taskStarted(Lorg/apache/tools/ant/BuildEvent;)V
    .locals 6
    .param p1    # Lorg/apache/tools/ant/BuildEvent;

    new-instance v2, Lorg/apache/tools/ant/XmlLogger$TimedElement;

    const/4 v3, 0x0

    invoke-direct {v2, v3}, Lorg/apache/tools/ant/XmlLogger$TimedElement;-><init>(Lorg/apache/tools/ant/XmlLogger$1;)V

    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v3

    invoke-static {v2, v3, v4}, Lorg/apache/tools/ant/XmlLogger$TimedElement;->access$102(Lorg/apache/tools/ant/XmlLogger$TimedElement;J)J

    iget-object v3, p0, Lorg/apache/tools/ant/XmlLogger;->doc:Lorg/w3c/dom/Document;

    const-string v4, "task"

    invoke-interface {v3, v4}, Lorg/w3c/dom/Document;->createElement(Ljava/lang/String;)Lorg/w3c/dom/Element;

    move-result-object v3

    invoke-static {v2, v3}, Lorg/apache/tools/ant/XmlLogger$TimedElement;->access$202(Lorg/apache/tools/ant/XmlLogger$TimedElement;Lorg/w3c/dom/Element;)Lorg/w3c/dom/Element;

    invoke-virtual {p1}, Lorg/apache/tools/ant/BuildEvent;->getTask()Lorg/apache/tools/ant/Task;

    move-result-object v1

    invoke-virtual {p1}, Lorg/apache/tools/ant/BuildEvent;->getTask()Lorg/apache/tools/ant/Task;

    move-result-object v3

    invoke-virtual {v3}, Lorg/apache/tools/ant/Task;->getTaskName()Ljava/lang/String;

    move-result-object v0

    if-nez v0, :cond_0

    const-string v0, ""

    :cond_0
    invoke-static {v2}, Lorg/apache/tools/ant/XmlLogger$TimedElement;->access$200(Lorg/apache/tools/ant/XmlLogger$TimedElement;)Lorg/w3c/dom/Element;

    move-result-object v3

    const-string v4, "name"

    invoke-interface {v3, v4, v0}, Lorg/w3c/dom/Element;->setAttribute(Ljava/lang/String;Ljava/lang/String;)V

    invoke-static {v2}, Lorg/apache/tools/ant/XmlLogger$TimedElement;->access$200(Lorg/apache/tools/ant/XmlLogger$TimedElement;)Lorg/w3c/dom/Element;

    move-result-object v3

    const-string v4, "location"

    invoke-virtual {p1}, Lorg/apache/tools/ant/BuildEvent;->getTask()Lorg/apache/tools/ant/Task;

    move-result-object v5

    invoke-virtual {v5}, Lorg/apache/tools/ant/Task;->getLocation()Lorg/apache/tools/ant/Location;

    move-result-object v5

    invoke-virtual {v5}, Lorg/apache/tools/ant/Location;->toString()Ljava/lang/String;

    move-result-object v5

    invoke-interface {v3, v4, v5}, Lorg/w3c/dom/Element;->setAttribute(Ljava/lang/String;Ljava/lang/String;)V

    iget-object v3, p0, Lorg/apache/tools/ant/XmlLogger;->tasks:Ljava/util/Hashtable;

    invoke-virtual {v3, v1, v2}, Ljava/util/Hashtable;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    invoke-direct {p0}, Lorg/apache/tools/ant/XmlLogger;->getStack()Ljava/util/Stack;

    move-result-object v3

    invoke-virtual {v3, v2}, Ljava/util/Stack;->push(Ljava/lang/Object;)Ljava/lang/Object;

    return-void
.end method
