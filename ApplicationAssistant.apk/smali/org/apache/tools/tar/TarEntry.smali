.class public Lorg/apache/tools/tar/TarEntry;
.super Ljava/lang/Object;
.source "TarEntry.java"

# interfaces
.implements Lorg/apache/tools/tar/TarConstants;


# static fields
.field public static final DEFAULT_DIR_MODE:I = 0x41ed

.field public static final DEFAULT_FILE_MODE:I = 0x81a4

.field public static final MAX_NAMELEN:I = 0x1f

.field public static final MILLIS_PER_SECOND:I = 0x3e8


# instance fields
.field private devMajor:I

.field private devMinor:I

.field private file:Ljava/io/File;

.field private groupId:I

.field private groupName:Ljava/lang/StringBuffer;

.field private linkFlag:B

.field private linkName:Ljava/lang/StringBuffer;

.field private magic:Ljava/lang/StringBuffer;

.field private modTime:J

.field private mode:I

.field private name:Ljava/lang/StringBuffer;

.field private size:J

.field private userId:I

.field private userName:Ljava/lang/StringBuffer;


# direct methods
.method private constructor <init>()V
    .locals 5

    const/16 v4, 0x1f

    const/4 v3, 0x0

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    new-instance v1, Ljava/lang/StringBuffer;

    const-string v2, "ustar"

    invoke-direct {v1, v2}, Ljava/lang/StringBuffer;-><init>(Ljava/lang/String;)V

    iput-object v1, p0, Lorg/apache/tools/tar/TarEntry;->magic:Ljava/lang/StringBuffer;

    new-instance v1, Ljava/lang/StringBuffer;

    invoke-direct {v1}, Ljava/lang/StringBuffer;-><init>()V

    iput-object v1, p0, Lorg/apache/tools/tar/TarEntry;->name:Ljava/lang/StringBuffer;

    new-instance v1, Ljava/lang/StringBuffer;

    invoke-direct {v1}, Ljava/lang/StringBuffer;-><init>()V

    iput-object v1, p0, Lorg/apache/tools/tar/TarEntry;->linkName:Ljava/lang/StringBuffer;

    const-string v1, "user.name"

    const-string v2, ""

    invoke-static {v1, v2}, Ljava/lang/System;->getProperty(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/String;->length()I

    move-result v1

    if-le v1, v4, :cond_0

    invoke-virtual {v0, v3, v4}, Ljava/lang/String;->substring(II)Ljava/lang/String;

    move-result-object v0

    :cond_0
    iput v3, p0, Lorg/apache/tools/tar/TarEntry;->userId:I

    iput v3, p0, Lorg/apache/tools/tar/TarEntry;->groupId:I

    new-instance v1, Ljava/lang/StringBuffer;

    invoke-direct {v1, v0}, Ljava/lang/StringBuffer;-><init>(Ljava/lang/String;)V

    iput-object v1, p0, Lorg/apache/tools/tar/TarEntry;->userName:Ljava/lang/StringBuffer;

    new-instance v1, Ljava/lang/StringBuffer;

    const-string v2, ""

    invoke-direct {v1, v2}, Ljava/lang/StringBuffer;-><init>(Ljava/lang/String;)V

    iput-object v1, p0, Lorg/apache/tools/tar/TarEntry;->groupName:Ljava/lang/StringBuffer;

    const/4 v1, 0x0

    iput-object v1, p0, Lorg/apache/tools/tar/TarEntry;->file:Ljava/io/File;

    return-void
.end method

.method public constructor <init>(Ljava/io/File;)V
    .locals 12
    .param p1    # Ljava/io/File;

    const/16 v11, 0x2f

    const/4 v10, 0x2

    const/4 v8, 0x1

    const/4 v7, -0x1

    const/4 v9, 0x0

    invoke-direct {p0}, Lorg/apache/tools/tar/TarEntry;-><init>()V

    iput-object p1, p0, Lorg/apache/tools/tar/TarEntry;->file:Ljava/io/File;

    invoke-virtual {p1}, Ljava/io/File;->getPath()Ljava/lang/String;

    move-result-object v3

    const-string v5, "os.name"

    invoke-static {v5}, Ljava/lang/System;->getProperty(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v5

    sget-object v6, Ljava/util/Locale;->US:Ljava/util/Locale;

    invoke-virtual {v5, v6}, Ljava/lang/String;->toLowerCase(Ljava/util/Locale;)Ljava/lang/String;

    move-result-object v4

    if-eqz v4, :cond_2

    const-string v5, "windows"

    invoke-virtual {v4, v5}, Ljava/lang/String;->startsWith(Ljava/lang/String;)Z

    move-result v5

    if-eqz v5, :cond_3

    invoke-virtual {v3}, Ljava/lang/String;->length()I

    move-result v5

    if-le v5, v10, :cond_2

    invoke-virtual {v3, v9}, Ljava/lang/String;->charAt(I)C

    move-result v0

    invoke-virtual {v3, v8}, Ljava/lang/String;->charAt(I)C

    move-result v1

    const/16 v5, 0x3a

    if-ne v1, v5, :cond_2

    const/16 v5, 0x61

    if-lt v0, v5, :cond_0

    const/16 v5, 0x7a

    if-le v0, v5, :cond_1

    :cond_0
    const/16 v5, 0x41

    if-lt v0, v5, :cond_2

    const/16 v5, 0x5a

    if-gt v0, v5, :cond_2

    :cond_1
    invoke-virtual {v3, v10}, Ljava/lang/String;->substring(I)Ljava/lang/String;

    move-result-object v3

    :cond_2
    :goto_0
    sget-char v5, Ljava/io/File;->separatorChar:C

    invoke-virtual {v3, v5, v11}, Ljava/lang/String;->replace(CC)Ljava/lang/String;

    move-result-object v3

    :goto_1
    const-string v5, "/"

    invoke-virtual {v3, v5}, Ljava/lang/String;->startsWith(Ljava/lang/String;)Z

    move-result v5

    if-eqz v5, :cond_4

    invoke-virtual {v3, v8}, Ljava/lang/String;->substring(I)Ljava/lang/String;

    move-result-object v3

    goto :goto_1

    :cond_3
    const-string v5, "netware"

    invoke-virtual {v4, v5}, Ljava/lang/String;->indexOf(Ljava/lang/String;)I

    move-result v5

    if-le v5, v7, :cond_2

    const/16 v5, 0x3a

    invoke-virtual {v3, v5}, Ljava/lang/String;->indexOf(I)I

    move-result v2

    if-eq v2, v7, :cond_2

    add-int/lit8 v5, v2, 0x1

    invoke-virtual {v3, v5}, Ljava/lang/String;->substring(I)Ljava/lang/String;

    move-result-object v3

    goto :goto_0

    :cond_4
    new-instance v5, Ljava/lang/StringBuffer;

    const-string v6, ""

    invoke-direct {v5, v6}, Ljava/lang/StringBuffer;-><init>(Ljava/lang/String;)V

    iput-object v5, p0, Lorg/apache/tools/tar/TarEntry;->linkName:Ljava/lang/StringBuffer;

    new-instance v5, Ljava/lang/StringBuffer;

    invoke-direct {v5, v3}, Ljava/lang/StringBuffer;-><init>(Ljava/lang/String;)V

    iput-object v5, p0, Lorg/apache/tools/tar/TarEntry;->name:Ljava/lang/StringBuffer;

    invoke-virtual {p1}, Ljava/io/File;->isDirectory()Z

    move-result v5

    if-eqz v5, :cond_6

    const/16 v5, 0x41ed

    iput v5, p0, Lorg/apache/tools/tar/TarEntry;->mode:I

    const/16 v5, 0x35

    iput-byte v5, p0, Lorg/apache/tools/tar/TarEntry;->linkFlag:B

    iget-object v5, p0, Lorg/apache/tools/tar/TarEntry;->name:Ljava/lang/StringBuffer;

    iget-object v6, p0, Lorg/apache/tools/tar/TarEntry;->name:Ljava/lang/StringBuffer;

    invoke-virtual {v6}, Ljava/lang/StringBuffer;->length()I

    move-result v6

    add-int/lit8 v6, v6, -0x1

    invoke-virtual {v5, v6}, Ljava/lang/StringBuffer;->charAt(I)C

    move-result v5

    if-eq v5, v11, :cond_5

    iget-object v5, p0, Lorg/apache/tools/tar/TarEntry;->name:Ljava/lang/StringBuffer;

    const-string v6, "/"

    invoke-virtual {v5, v6}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    :cond_5
    :goto_2
    invoke-virtual {p1}, Ljava/io/File;->length()J

    move-result-wide v5

    iput-wide v5, p0, Lorg/apache/tools/tar/TarEntry;->size:J

    invoke-virtual {p1}, Ljava/io/File;->lastModified()J

    move-result-wide v5

    const-wide/16 v7, 0x3e8

    div-long/2addr v5, v7

    iput-wide v5, p0, Lorg/apache/tools/tar/TarEntry;->modTime:J

    iput v9, p0, Lorg/apache/tools/tar/TarEntry;->devMajor:I

    iput v9, p0, Lorg/apache/tools/tar/TarEntry;->devMinor:I

    return-void

    :cond_6
    const v5, 0x81a4

    iput v5, p0, Lorg/apache/tools/tar/TarEntry;->mode:I

    const/16 v5, 0x30

    iput-byte v5, p0, Lorg/apache/tools/tar/TarEntry;->linkFlag:B

    goto :goto_2
.end method

.method public constructor <init>(Ljava/lang/String;)V
    .locals 6
    .param p1    # Ljava/lang/String;

    const/4 v5, 0x0

    invoke-direct {p0}, Lorg/apache/tools/tar/TarEntry;-><init>()V

    const-string v1, "/"

    invoke-virtual {p1, v1}, Ljava/lang/String;->endsWith(Ljava/lang/String;)Z

    move-result v0

    iput v5, p0, Lorg/apache/tools/tar/TarEntry;->devMajor:I

    iput v5, p0, Lorg/apache/tools/tar/TarEntry;->devMinor:I

    new-instance v1, Ljava/lang/StringBuffer;

    invoke-direct {v1, p1}, Ljava/lang/StringBuffer;-><init>(Ljava/lang/String;)V

    iput-object v1, p0, Lorg/apache/tools/tar/TarEntry;->name:Ljava/lang/StringBuffer;

    if-eqz v0, :cond_0

    const/16 v1, 0x41ed

    :goto_0
    iput v1, p0, Lorg/apache/tools/tar/TarEntry;->mode:I

    if-eqz v0, :cond_1

    const/16 v1, 0x35

    :goto_1
    iput-byte v1, p0, Lorg/apache/tools/tar/TarEntry;->linkFlag:B

    iput v5, p0, Lorg/apache/tools/tar/TarEntry;->userId:I

    iput v5, p0, Lorg/apache/tools/tar/TarEntry;->groupId:I

    const-wide/16 v1, 0x0

    iput-wide v1, p0, Lorg/apache/tools/tar/TarEntry;->size:J

    new-instance v1, Ljava/util/Date;

    invoke-direct {v1}, Ljava/util/Date;-><init>()V

    invoke-virtual {v1}, Ljava/util/Date;->getTime()J

    move-result-wide v1

    const-wide/16 v3, 0x3e8

    div-long/2addr v1, v3

    iput-wide v1, p0, Lorg/apache/tools/tar/TarEntry;->modTime:J

    new-instance v1, Ljava/lang/StringBuffer;

    const-string v2, ""

    invoke-direct {v1, v2}, Ljava/lang/StringBuffer;-><init>(Ljava/lang/String;)V

    iput-object v1, p0, Lorg/apache/tools/tar/TarEntry;->linkName:Ljava/lang/StringBuffer;

    new-instance v1, Ljava/lang/StringBuffer;

    const-string v2, ""

    invoke-direct {v1, v2}, Ljava/lang/StringBuffer;-><init>(Ljava/lang/String;)V

    iput-object v1, p0, Lorg/apache/tools/tar/TarEntry;->userName:Ljava/lang/StringBuffer;

    new-instance v1, Ljava/lang/StringBuffer;

    const-string v2, ""

    invoke-direct {v1, v2}, Ljava/lang/StringBuffer;-><init>(Ljava/lang/String;)V

    iput-object v1, p0, Lorg/apache/tools/tar/TarEntry;->groupName:Ljava/lang/StringBuffer;

    iput v5, p0, Lorg/apache/tools/tar/TarEntry;->devMajor:I

    iput v5, p0, Lorg/apache/tools/tar/TarEntry;->devMinor:I

    return-void

    :cond_0
    const v1, 0x81a4

    goto :goto_0

    :cond_1
    const/16 v1, 0x30

    goto :goto_1
.end method

.method public constructor <init>(Ljava/lang/String;B)V
    .locals 0
    .param p1    # Ljava/lang/String;
    .param p2    # B

    invoke-direct {p0, p1}, Lorg/apache/tools/tar/TarEntry;-><init>(Ljava/lang/String;)V

    iput-byte p2, p0, Lorg/apache/tools/tar/TarEntry;->linkFlag:B

    return-void
.end method

.method public constructor <init>([B)V
    .locals 0
    .param p1    # [B

    invoke-direct {p0}, Lorg/apache/tools/tar/TarEntry;-><init>()V

    invoke-virtual {p0, p1}, Lorg/apache/tools/tar/TarEntry;->parseTarHeader([B)V

    return-void
.end method


# virtual methods
.method public equals(Ljava/lang/Object;)Z
    .locals 2
    .param p1    # Ljava/lang/Object;

    if-eqz p1, :cond_0

    invoke-virtual {p0}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v0

    invoke-virtual {p1}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v1

    if-eq v0, v1, :cond_1

    :cond_0
    const/4 v0, 0x0

    :goto_0
    return v0

    :cond_1
    check-cast p1, Lorg/apache/tools/tar/TarEntry;

    invoke-virtual {p0, p1}, Lorg/apache/tools/tar/TarEntry;->equals(Lorg/apache/tools/tar/TarEntry;)Z

    move-result v0

    goto :goto_0
.end method

.method public equals(Lorg/apache/tools/tar/TarEntry;)Z
    .locals 2
    .param p1    # Lorg/apache/tools/tar/TarEntry;

    invoke-virtual {p0}, Lorg/apache/tools/tar/TarEntry;->getName()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p1}, Lorg/apache/tools/tar/TarEntry;->getName()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    return v0
.end method

.method public getDirectoryEntries()[Lorg/apache/tools/tar/TarEntry;
    .locals 7

    iget-object v3, p0, Lorg/apache/tools/tar/TarEntry;->file:Ljava/io/File;

    if-eqz v3, :cond_0

    iget-object v3, p0, Lorg/apache/tools/tar/TarEntry;->file:Ljava/io/File;

    invoke-virtual {v3}, Ljava/io/File;->isDirectory()Z

    move-result v3

    if-nez v3, :cond_2

    :cond_0
    const/4 v3, 0x0

    new-array v2, v3, [Lorg/apache/tools/tar/TarEntry;

    :cond_1
    return-object v2

    :cond_2
    iget-object v3, p0, Lorg/apache/tools/tar/TarEntry;->file:Ljava/io/File;

    invoke-virtual {v3}, Ljava/io/File;->list()[Ljava/lang/String;

    move-result-object v1

    array-length v3, v1

    new-array v2, v3, [Lorg/apache/tools/tar/TarEntry;

    const/4 v0, 0x0

    :goto_0
    array-length v3, v1

    if-ge v0, v3, :cond_1

    new-instance v3, Lorg/apache/tools/tar/TarEntry;

    new-instance v4, Ljava/io/File;

    iget-object v5, p0, Lorg/apache/tools/tar/TarEntry;->file:Ljava/io/File;

    aget-object v6, v1, v0

    invoke-direct {v4, v5, v6}, Ljava/io/File;-><init>(Ljava/io/File;Ljava/lang/String;)V

    invoke-direct {v3, v4}, Lorg/apache/tools/tar/TarEntry;-><init>(Ljava/io/File;)V

    aput-object v3, v2, v0

    add-int/lit8 v0, v0, 0x1

    goto :goto_0
.end method

.method public getFile()Ljava/io/File;
    .locals 1

    iget-object v0, p0, Lorg/apache/tools/tar/TarEntry;->file:Ljava/io/File;

    return-object v0
.end method

.method public getGroupId()I
    .locals 1

    iget v0, p0, Lorg/apache/tools/tar/TarEntry;->groupId:I

    return v0
.end method

.method public getGroupName()Ljava/lang/String;
    .locals 1

    iget-object v0, p0, Lorg/apache/tools/tar/TarEntry;->groupName:Ljava/lang/StringBuffer;

    invoke-virtual {v0}, Ljava/lang/StringBuffer;->toString()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public getLinkName()Ljava/lang/String;
    .locals 1

    iget-object v0, p0, Lorg/apache/tools/tar/TarEntry;->linkName:Ljava/lang/StringBuffer;

    invoke-virtual {v0}, Ljava/lang/StringBuffer;->toString()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public getModTime()Ljava/util/Date;
    .locals 5

    new-instance v0, Ljava/util/Date;

    iget-wide v1, p0, Lorg/apache/tools/tar/TarEntry;->modTime:J

    const-wide/16 v3, 0x3e8

    mul-long/2addr v1, v3

    invoke-direct {v0, v1, v2}, Ljava/util/Date;-><init>(J)V

    return-object v0
.end method

.method public getMode()I
    .locals 1

    iget v0, p0, Lorg/apache/tools/tar/TarEntry;->mode:I

    return v0
.end method

.method public getName()Ljava/lang/String;
    .locals 1

    iget-object v0, p0, Lorg/apache/tools/tar/TarEntry;->name:Ljava/lang/StringBuffer;

    invoke-virtual {v0}, Ljava/lang/StringBuffer;->toString()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public getSize()J
    .locals 2

    iget-wide v0, p0, Lorg/apache/tools/tar/TarEntry;->size:J

    return-wide v0
.end method

.method public getUserId()I
    .locals 1

    iget v0, p0, Lorg/apache/tools/tar/TarEntry;->userId:I

    return v0
.end method

.method public getUserName()Ljava/lang/String;
    .locals 1

    iget-object v0, p0, Lorg/apache/tools/tar/TarEntry;->userName:Ljava/lang/StringBuffer;

    invoke-virtual {v0}, Ljava/lang/StringBuffer;->toString()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public hashCode()I
    .locals 1

    invoke-virtual {p0}, Lorg/apache/tools/tar/TarEntry;->getName()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/String;->hashCode()I

    move-result v0

    return v0
.end method

.method public isDescendent(Lorg/apache/tools/tar/TarEntry;)Z
    .locals 2
    .param p1    # Lorg/apache/tools/tar/TarEntry;

    invoke-virtual {p1}, Lorg/apache/tools/tar/TarEntry;->getName()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p0}, Lorg/apache/tools/tar/TarEntry;->getName()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/String;->startsWith(Ljava/lang/String;)Z

    move-result v0

    return v0
.end method

.method public isDirectory()Z
    .locals 3

    const/4 v0, 0x1

    iget-object v1, p0, Lorg/apache/tools/tar/TarEntry;->file:Ljava/io/File;

    if-eqz v1, :cond_1

    iget-object v0, p0, Lorg/apache/tools/tar/TarEntry;->file:Ljava/io/File;

    invoke-virtual {v0}, Ljava/io/File;->isDirectory()Z

    move-result v0

    :cond_0
    :goto_0
    return v0

    :cond_1
    iget-byte v1, p0, Lorg/apache/tools/tar/TarEntry;->linkFlag:B

    const/16 v2, 0x35

    if-eq v1, v2, :cond_0

    invoke-virtual {p0}, Lorg/apache/tools/tar/TarEntry;->getName()Ljava/lang/String;

    move-result-object v1

    const-string v2, "/"

    invoke-virtual {v1, v2}, Ljava/lang/String;->endsWith(Ljava/lang/String;)Z

    move-result v1

    if-nez v1, :cond_0

    const/4 v0, 0x0

    goto :goto_0
.end method

.method public isGNULongNameEntry()Z
    .locals 2

    iget-byte v0, p0, Lorg/apache/tools/tar/TarEntry;->linkFlag:B

    const/16 v1, 0x4c

    if-ne v0, v1, :cond_0

    iget-object v0, p0, Lorg/apache/tools/tar/TarEntry;->name:Ljava/lang/StringBuffer;

    invoke-virtual {v0}, Ljava/lang/StringBuffer;->toString()Ljava/lang/String;

    move-result-object v0

    const-string v1, "././@LongLink"

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public parseTarHeader([B)V
    .locals 8
    .param p1    # [B

    const/16 v7, 0x64

    const/16 v6, 0x20

    const/16 v5, 0xc

    const/16 v4, 0x8

    const/4 v0, 0x0

    invoke-static {p1, v0, v7}, Lorg/apache/tools/tar/TarUtils;->parseName([BII)Ljava/lang/StringBuffer;

    move-result-object v2

    iput-object v2, p0, Lorg/apache/tools/tar/TarEntry;->name:Ljava/lang/StringBuffer;

    add-int/lit8 v0, v0, 0x64

    invoke-static {p1, v0, v4}, Lorg/apache/tools/tar/TarUtils;->parseOctal([BII)J

    move-result-wide v2

    long-to-int v2, v2

    iput v2, p0, Lorg/apache/tools/tar/TarEntry;->mode:I

    add-int/lit8 v0, v0, 0x8

    invoke-static {p1, v0, v4}, Lorg/apache/tools/tar/TarUtils;->parseOctal([BII)J

    move-result-wide v2

    long-to-int v2, v2

    iput v2, p0, Lorg/apache/tools/tar/TarEntry;->userId:I

    add-int/lit8 v0, v0, 0x8

    invoke-static {p1, v0, v4}, Lorg/apache/tools/tar/TarUtils;->parseOctal([BII)J

    move-result-wide v2

    long-to-int v2, v2

    iput v2, p0, Lorg/apache/tools/tar/TarEntry;->groupId:I

    add-int/lit8 v0, v0, 0x8

    invoke-static {p1, v0, v5}, Lorg/apache/tools/tar/TarUtils;->parseOctal([BII)J

    move-result-wide v2

    iput-wide v2, p0, Lorg/apache/tools/tar/TarEntry;->size:J

    add-int/lit8 v0, v0, 0xc

    invoke-static {p1, v0, v5}, Lorg/apache/tools/tar/TarUtils;->parseOctal([BII)J

    move-result-wide v2

    iput-wide v2, p0, Lorg/apache/tools/tar/TarEntry;->modTime:J

    add-int/lit8 v0, v0, 0xc

    add-int/lit8 v0, v0, 0x8

    add-int/lit8 v1, v0, 0x1

    aget-byte v2, p1, v0

    iput-byte v2, p0, Lorg/apache/tools/tar/TarEntry;->linkFlag:B

    invoke-static {p1, v1, v7}, Lorg/apache/tools/tar/TarUtils;->parseName([BII)Ljava/lang/StringBuffer;

    move-result-object v2

    iput-object v2, p0, Lorg/apache/tools/tar/TarEntry;->linkName:Ljava/lang/StringBuffer;

    add-int/lit8 v0, v1, 0x64

    invoke-static {p1, v0, v4}, Lorg/apache/tools/tar/TarUtils;->parseName([BII)Ljava/lang/StringBuffer;

    move-result-object v2

    iput-object v2, p0, Lorg/apache/tools/tar/TarEntry;->magic:Ljava/lang/StringBuffer;

    add-int/lit8 v0, v0, 0x8

    invoke-static {p1, v0, v6}, Lorg/apache/tools/tar/TarUtils;->parseName([BII)Ljava/lang/StringBuffer;

    move-result-object v2

    iput-object v2, p0, Lorg/apache/tools/tar/TarEntry;->userName:Ljava/lang/StringBuffer;

    add-int/lit8 v0, v0, 0x20

    invoke-static {p1, v0, v6}, Lorg/apache/tools/tar/TarUtils;->parseName([BII)Ljava/lang/StringBuffer;

    move-result-object v2

    iput-object v2, p0, Lorg/apache/tools/tar/TarEntry;->groupName:Ljava/lang/StringBuffer;

    add-int/lit8 v0, v0, 0x20

    invoke-static {p1, v0, v4}, Lorg/apache/tools/tar/TarUtils;->parseOctal([BII)J

    move-result-wide v2

    long-to-int v2, v2

    iput v2, p0, Lorg/apache/tools/tar/TarEntry;->devMajor:I

    add-int/lit8 v0, v0, 0x8

    invoke-static {p1, v0, v4}, Lorg/apache/tools/tar/TarUtils;->parseOctal([BII)J

    move-result-wide v2

    long-to-int v2, v2

    iput v2, p0, Lorg/apache/tools/tar/TarEntry;->devMinor:I

    return-void
.end method

.method public setGroupId(I)V
    .locals 0
    .param p1    # I

    iput p1, p0, Lorg/apache/tools/tar/TarEntry;->groupId:I

    return-void
.end method

.method public setGroupName(Ljava/lang/String;)V
    .locals 1
    .param p1    # Ljava/lang/String;

    new-instance v0, Ljava/lang/StringBuffer;

    invoke-direct {v0, p1}, Ljava/lang/StringBuffer;-><init>(Ljava/lang/String;)V

    iput-object v0, p0, Lorg/apache/tools/tar/TarEntry;->groupName:Ljava/lang/StringBuffer;

    return-void
.end method

.method public setIds(II)V
    .locals 0
    .param p1    # I
    .param p2    # I

    invoke-virtual {p0, p1}, Lorg/apache/tools/tar/TarEntry;->setUserId(I)V

    invoke-virtual {p0, p2}, Lorg/apache/tools/tar/TarEntry;->setGroupId(I)V

    return-void
.end method

.method public setModTime(J)V
    .locals 2
    .param p1    # J

    const-wide/16 v0, 0x3e8

    div-long v0, p1, v0

    iput-wide v0, p0, Lorg/apache/tools/tar/TarEntry;->modTime:J

    return-void
.end method

.method public setModTime(Ljava/util/Date;)V
    .locals 4
    .param p1    # Ljava/util/Date;

    invoke-virtual {p1}, Ljava/util/Date;->getTime()J

    move-result-wide v0

    const-wide/16 v2, 0x3e8

    div-long/2addr v0, v2

    iput-wide v0, p0, Lorg/apache/tools/tar/TarEntry;->modTime:J

    return-void
.end method

.method public setMode(I)V
    .locals 0
    .param p1    # I

    iput p1, p0, Lorg/apache/tools/tar/TarEntry;->mode:I

    return-void
.end method

.method public setName(Ljava/lang/String;)V
    .locals 1
    .param p1    # Ljava/lang/String;

    new-instance v0, Ljava/lang/StringBuffer;

    invoke-direct {v0, p1}, Ljava/lang/StringBuffer;-><init>(Ljava/lang/String;)V

    iput-object v0, p0, Lorg/apache/tools/tar/TarEntry;->name:Ljava/lang/StringBuffer;

    return-void
.end method

.method public setNames(Ljava/lang/String;Ljava/lang/String;)V
    .locals 0
    .param p1    # Ljava/lang/String;
    .param p2    # Ljava/lang/String;

    invoke-virtual {p0, p1}, Lorg/apache/tools/tar/TarEntry;->setUserName(Ljava/lang/String;)V

    invoke-virtual {p0, p2}, Lorg/apache/tools/tar/TarEntry;->setGroupName(Ljava/lang/String;)V

    return-void
.end method

.method public setSize(J)V
    .locals 0
    .param p1    # J

    iput-wide p1, p0, Lorg/apache/tools/tar/TarEntry;->size:J

    return-void
.end method

.method public setUserId(I)V
    .locals 0
    .param p1    # I

    iput p1, p0, Lorg/apache/tools/tar/TarEntry;->userId:I

    return-void
.end method

.method public setUserName(Ljava/lang/String;)V
    .locals 1
    .param p1    # Ljava/lang/String;

    new-instance v0, Ljava/lang/StringBuffer;

    invoke-direct {v0, p1}, Ljava/lang/StringBuffer;-><init>(Ljava/lang/String;)V

    iput-object v0, p0, Lorg/apache/tools/tar/TarEntry;->userName:Ljava/lang/StringBuffer;

    return-void
.end method

.method public writeEntryHeader([B)V
    .locals 12
    .param p1    # [B

    const/16 v11, 0x64

    const/16 v10, 0xc

    const/16 v9, 0x20

    const/16 v8, 0x8

    const/4 v4, 0x0

    iget-object v6, p0, Lorg/apache/tools/tar/TarEntry;->name:Ljava/lang/StringBuffer;

    invoke-static {v6, p1, v4, v11}, Lorg/apache/tools/tar/TarUtils;->getNameBytes(Ljava/lang/StringBuffer;[BII)I

    move-result v4

    iget v6, p0, Lorg/apache/tools/tar/TarEntry;->mode:I

    int-to-long v6, v6

    invoke-static {v6, v7, p1, v4, v8}, Lorg/apache/tools/tar/TarUtils;->getOctalBytes(J[BII)I

    move-result v4

    iget v6, p0, Lorg/apache/tools/tar/TarEntry;->userId:I

    int-to-long v6, v6

    invoke-static {v6, v7, p1, v4, v8}, Lorg/apache/tools/tar/TarUtils;->getOctalBytes(J[BII)I

    move-result v4

    iget v6, p0, Lorg/apache/tools/tar/TarEntry;->groupId:I

    int-to-long v6, v6

    invoke-static {v6, v7, p1, v4, v8}, Lorg/apache/tools/tar/TarUtils;->getOctalBytes(J[BII)I

    move-result v4

    iget-wide v6, p0, Lorg/apache/tools/tar/TarEntry;->size:J

    invoke-static {v6, v7, p1, v4, v10}, Lorg/apache/tools/tar/TarUtils;->getLongOctalBytes(J[BII)I

    move-result v4

    iget-wide v6, p0, Lorg/apache/tools/tar/TarEntry;->modTime:J

    invoke-static {v6, v7, p1, v4, v10}, Lorg/apache/tools/tar/TarUtils;->getLongOctalBytes(J[BII)I

    move-result v4

    move v3, v4

    const/4 v0, 0x0

    move v5, v4

    :goto_0
    if-ge v0, v8, :cond_0

    add-int/lit8 v4, v5, 0x1

    aput-byte v9, p1, v5

    add-int/lit8 v0, v0, 0x1

    move v5, v4

    goto :goto_0

    :cond_0
    add-int/lit8 v4, v5, 0x1

    iget-byte v6, p0, Lorg/apache/tools/tar/TarEntry;->linkFlag:B

    aput-byte v6, p1, v5

    iget-object v6, p0, Lorg/apache/tools/tar/TarEntry;->linkName:Ljava/lang/StringBuffer;

    invoke-static {v6, p1, v4, v11}, Lorg/apache/tools/tar/TarUtils;->getNameBytes(Ljava/lang/StringBuffer;[BII)I

    move-result v4

    iget-object v6, p0, Lorg/apache/tools/tar/TarEntry;->magic:Ljava/lang/StringBuffer;

    invoke-static {v6, p1, v4, v8}, Lorg/apache/tools/tar/TarUtils;->getNameBytes(Ljava/lang/StringBuffer;[BII)I

    move-result v4

    iget-object v6, p0, Lorg/apache/tools/tar/TarEntry;->userName:Ljava/lang/StringBuffer;

    invoke-static {v6, p1, v4, v9}, Lorg/apache/tools/tar/TarUtils;->getNameBytes(Ljava/lang/StringBuffer;[BII)I

    move-result v4

    iget-object v6, p0, Lorg/apache/tools/tar/TarEntry;->groupName:Ljava/lang/StringBuffer;

    invoke-static {v6, p1, v4, v9}, Lorg/apache/tools/tar/TarUtils;->getNameBytes(Ljava/lang/StringBuffer;[BII)I

    move-result v4

    iget v6, p0, Lorg/apache/tools/tar/TarEntry;->devMajor:I

    int-to-long v6, v6

    invoke-static {v6, v7, p1, v4, v8}, Lorg/apache/tools/tar/TarUtils;->getOctalBytes(J[BII)I

    move-result v4

    iget v6, p0, Lorg/apache/tools/tar/TarEntry;->devMinor:I

    int-to-long v6, v6

    invoke-static {v6, v7, p1, v4, v8}, Lorg/apache/tools/tar/TarUtils;->getOctalBytes(J[BII)I

    move-result v4

    :goto_1
    array-length v6, p1

    if-ge v4, v6, :cond_1

    add-int/lit8 v5, v4, 0x1

    const/4 v6, 0x0

    aput-byte v6, p1, v4

    move v4, v5

    goto :goto_1

    :cond_1
    invoke-static {p1}, Lorg/apache/tools/tar/TarUtils;->computeCheckSum([B)J

    move-result-wide v1

    invoke-static {v1, v2, p1, v3, v8}, Lorg/apache/tools/tar/TarUtils;->getCheckSumOctalBytes(J[BII)I

    return-void
.end method
