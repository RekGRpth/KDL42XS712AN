.class public Lorg/apache/tools/tar/TarBuffer;
.super Ljava/lang/Object;
.source "TarBuffer.java"


# static fields
.field public static final DEFAULT_BLKSIZE:I = 0x2800

.field public static final DEFAULT_RCDSIZE:I = 0x200


# instance fields
.field private blockBuffer:[B

.field private blockSize:I

.field private currBlkIdx:I

.field private currRecIdx:I

.field private debug:Z

.field private inStream:Ljava/io/InputStream;

.field private outStream:Ljava/io/OutputStream;

.field private recordSize:I

.field private recsPerBlock:I


# direct methods
.method public constructor <init>(Ljava/io/InputStream;)V
    .locals 1
    .param p1    # Ljava/io/InputStream;

    const/16 v0, 0x2800

    invoke-direct {p0, p1, v0}, Lorg/apache/tools/tar/TarBuffer;-><init>(Ljava/io/InputStream;I)V

    return-void
.end method

.method public constructor <init>(Ljava/io/InputStream;I)V
    .locals 1
    .param p1    # Ljava/io/InputStream;
    .param p2    # I

    const/16 v0, 0x200

    invoke-direct {p0, p1, p2, v0}, Lorg/apache/tools/tar/TarBuffer;-><init>(Ljava/io/InputStream;II)V

    return-void
.end method

.method public constructor <init>(Ljava/io/InputStream;II)V
    .locals 1
    .param p1    # Ljava/io/InputStream;
    .param p2    # I
    .param p3    # I

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    iput-object p1, p0, Lorg/apache/tools/tar/TarBuffer;->inStream:Ljava/io/InputStream;

    const/4 v0, 0x0

    iput-object v0, p0, Lorg/apache/tools/tar/TarBuffer;->outStream:Ljava/io/OutputStream;

    invoke-direct {p0, p2, p3}, Lorg/apache/tools/tar/TarBuffer;->initialize(II)V

    return-void
.end method

.method public constructor <init>(Ljava/io/OutputStream;)V
    .locals 1
    .param p1    # Ljava/io/OutputStream;

    const/16 v0, 0x2800

    invoke-direct {p0, p1, v0}, Lorg/apache/tools/tar/TarBuffer;-><init>(Ljava/io/OutputStream;I)V

    return-void
.end method

.method public constructor <init>(Ljava/io/OutputStream;I)V
    .locals 1
    .param p1    # Ljava/io/OutputStream;
    .param p2    # I

    const/16 v0, 0x200

    invoke-direct {p0, p1, p2, v0}, Lorg/apache/tools/tar/TarBuffer;-><init>(Ljava/io/OutputStream;II)V

    return-void
.end method

.method public constructor <init>(Ljava/io/OutputStream;II)V
    .locals 1
    .param p1    # Ljava/io/OutputStream;
    .param p2    # I
    .param p3    # I

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    const/4 v0, 0x0

    iput-object v0, p0, Lorg/apache/tools/tar/TarBuffer;->inStream:Ljava/io/InputStream;

    iput-object p1, p0, Lorg/apache/tools/tar/TarBuffer;->outStream:Ljava/io/OutputStream;

    invoke-direct {p0, p2, p3}, Lorg/apache/tools/tar/TarBuffer;->initialize(II)V

    return-void
.end method

.method private flushBlock()V
    .locals 2
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    iget-boolean v0, p0, Lorg/apache/tools/tar/TarBuffer;->debug:Z

    if-eqz v0, :cond_0

    sget-object v0, Ljava/lang/System;->err:Ljava/io/PrintStream;

    const-string v1, "TarBuffer.flushBlock() called."

    invoke-virtual {v0, v1}, Ljava/io/PrintStream;->println(Ljava/lang/String;)V

    :cond_0
    iget-object v0, p0, Lorg/apache/tools/tar/TarBuffer;->outStream:Ljava/io/OutputStream;

    if-nez v0, :cond_1

    new-instance v0, Ljava/io/IOException;

    const-string v1, "writing to an input buffer"

    invoke-direct {v0, v1}, Ljava/io/IOException;-><init>(Ljava/lang/String;)V

    throw v0

    :cond_1
    iget v0, p0, Lorg/apache/tools/tar/TarBuffer;->currRecIdx:I

    if-lez v0, :cond_2

    invoke-direct {p0}, Lorg/apache/tools/tar/TarBuffer;->writeBlock()V

    :cond_2
    return-void
.end method

.method private initialize(II)V
    .locals 3
    .param p1    # I
    .param p2    # I

    const/4 v2, 0x0

    iput-boolean v2, p0, Lorg/apache/tools/tar/TarBuffer;->debug:Z

    iput p1, p0, Lorg/apache/tools/tar/TarBuffer;->blockSize:I

    iput p2, p0, Lorg/apache/tools/tar/TarBuffer;->recordSize:I

    iget v0, p0, Lorg/apache/tools/tar/TarBuffer;->blockSize:I

    iget v1, p0, Lorg/apache/tools/tar/TarBuffer;->recordSize:I

    div-int/2addr v0, v1

    iput v0, p0, Lorg/apache/tools/tar/TarBuffer;->recsPerBlock:I

    iget v0, p0, Lorg/apache/tools/tar/TarBuffer;->blockSize:I

    new-array v0, v0, [B

    iput-object v0, p0, Lorg/apache/tools/tar/TarBuffer;->blockBuffer:[B

    iget-object v0, p0, Lorg/apache/tools/tar/TarBuffer;->inStream:Ljava/io/InputStream;

    if-eqz v0, :cond_0

    const/4 v0, -0x1

    iput v0, p0, Lorg/apache/tools/tar/TarBuffer;->currBlkIdx:I

    iget v0, p0, Lorg/apache/tools/tar/TarBuffer;->recsPerBlock:I

    iput v0, p0, Lorg/apache/tools/tar/TarBuffer;->currRecIdx:I

    :goto_0
    return-void

    :cond_0
    iput v2, p0, Lorg/apache/tools/tar/TarBuffer;->currBlkIdx:I

    iput v2, p0, Lorg/apache/tools/tar/TarBuffer;->currRecIdx:I

    goto :goto_0
.end method

.method private readBlock()Z
    .locals 8
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    const/4 v4, 0x0

    iget-boolean v5, p0, Lorg/apache/tools/tar/TarBuffer;->debug:Z

    if-eqz v5, :cond_0

    sget-object v5, Ljava/lang/System;->err:Ljava/io/PrintStream;

    new-instance v6, Ljava/lang/StringBuffer;

    invoke-direct {v6}, Ljava/lang/StringBuffer;-><init>()V

    const-string v7, "ReadBlock: blkIdx = "

    invoke-virtual {v6, v7}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    move-result-object v6

    iget v7, p0, Lorg/apache/tools/tar/TarBuffer;->currBlkIdx:I

    invoke-virtual {v6, v7}, Ljava/lang/StringBuffer;->append(I)Ljava/lang/StringBuffer;

    move-result-object v6

    invoke-virtual {v6}, Ljava/lang/StringBuffer;->toString()Ljava/lang/String;

    move-result-object v6

    invoke-virtual {v5, v6}, Ljava/io/PrintStream;->println(Ljava/lang/String;)V

    :cond_0
    iget-object v5, p0, Lorg/apache/tools/tar/TarBuffer;->inStream:Ljava/io/InputStream;

    if-nez v5, :cond_1

    new-instance v4, Ljava/io/IOException;

    const-string v5, "reading from an output buffer"

    invoke-direct {v4, v5}, Ljava/io/IOException;-><init>(Ljava/lang/String;)V

    throw v4

    :cond_1
    iput v4, p0, Lorg/apache/tools/tar/TarBuffer;->currRecIdx:I

    const/4 v3, 0x0

    iget v0, p0, Lorg/apache/tools/tar/TarBuffer;->blockSize:I

    :cond_2
    :goto_0
    if-lez v0, :cond_4

    iget-object v5, p0, Lorg/apache/tools/tar/TarBuffer;->inStream:Ljava/io/InputStream;

    iget-object v6, p0, Lorg/apache/tools/tar/TarBuffer;->blockBuffer:[B

    invoke-virtual {v5, v6, v3, v0}, Ljava/io/InputStream;->read([BII)I

    move-result v5

    int-to-long v1, v5

    const-wide/16 v5, -0x1

    cmp-long v5, v1, v5

    if-nez v5, :cond_5

    if-nez v3, :cond_3

    :goto_1
    return v4

    :cond_3
    iget-object v5, p0, Lorg/apache/tools/tar/TarBuffer;->blockBuffer:[B

    add-int v6, v3, v0

    invoke-static {v5, v3, v6, v4}, Ljava/util/Arrays;->fill([BIIB)V

    :cond_4
    iget v4, p0, Lorg/apache/tools/tar/TarBuffer;->currBlkIdx:I

    add-int/lit8 v4, v4, 0x1

    iput v4, p0, Lorg/apache/tools/tar/TarBuffer;->currBlkIdx:I

    const/4 v4, 0x1

    goto :goto_1

    :cond_5
    int-to-long v5, v3

    add-long/2addr v5, v1

    long-to-int v3, v5

    int-to-long v5, v0

    sub-long/2addr v5, v1

    long-to-int v0, v5

    iget v5, p0, Lorg/apache/tools/tar/TarBuffer;->blockSize:I

    int-to-long v5, v5

    cmp-long v5, v1, v5

    if-eqz v5, :cond_2

    iget-boolean v5, p0, Lorg/apache/tools/tar/TarBuffer;->debug:Z

    if-eqz v5, :cond_2

    sget-object v5, Ljava/lang/System;->err:Ljava/io/PrintStream;

    new-instance v6, Ljava/lang/StringBuffer;

    invoke-direct {v6}, Ljava/lang/StringBuffer;-><init>()V

    const-string v7, "ReadBlock: INCOMPLETE READ "

    invoke-virtual {v6, v7}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    move-result-object v6

    invoke-virtual {v6, v1, v2}, Ljava/lang/StringBuffer;->append(J)Ljava/lang/StringBuffer;

    move-result-object v6

    const-string v7, " of "

    invoke-virtual {v6, v7}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    move-result-object v6

    iget v7, p0, Lorg/apache/tools/tar/TarBuffer;->blockSize:I

    invoke-virtual {v6, v7}, Ljava/lang/StringBuffer;->append(I)Ljava/lang/StringBuffer;

    move-result-object v6

    const-string v7, " bytes read."

    invoke-virtual {v6, v7}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    move-result-object v6

    invoke-virtual {v6}, Ljava/lang/StringBuffer;->toString()Ljava/lang/String;

    move-result-object v6

    invoke-virtual {v5, v6}, Ljava/io/PrintStream;->println(Ljava/lang/String;)V

    goto :goto_0
.end method

.method private writeBlock()V
    .locals 4
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    const/4 v3, 0x0

    iget-boolean v0, p0, Lorg/apache/tools/tar/TarBuffer;->debug:Z

    if-eqz v0, :cond_0

    sget-object v0, Ljava/lang/System;->err:Ljava/io/PrintStream;

    new-instance v1, Ljava/lang/StringBuffer;

    invoke-direct {v1}, Ljava/lang/StringBuffer;-><init>()V

    const-string v2, "WriteBlock: blkIdx = "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    move-result-object v1

    iget v2, p0, Lorg/apache/tools/tar/TarBuffer;->currBlkIdx:I

    invoke-virtual {v1, v2}, Ljava/lang/StringBuffer;->append(I)Ljava/lang/StringBuffer;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuffer;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/io/PrintStream;->println(Ljava/lang/String;)V

    :cond_0
    iget-object v0, p0, Lorg/apache/tools/tar/TarBuffer;->outStream:Ljava/io/OutputStream;

    if-nez v0, :cond_1

    new-instance v0, Ljava/io/IOException;

    const-string v1, "writing to an input buffer"

    invoke-direct {v0, v1}, Ljava/io/IOException;-><init>(Ljava/lang/String;)V

    throw v0

    :cond_1
    iget-object v0, p0, Lorg/apache/tools/tar/TarBuffer;->outStream:Ljava/io/OutputStream;

    iget-object v1, p0, Lorg/apache/tools/tar/TarBuffer;->blockBuffer:[B

    iget v2, p0, Lorg/apache/tools/tar/TarBuffer;->blockSize:I

    invoke-virtual {v0, v1, v3, v2}, Ljava/io/OutputStream;->write([BII)V

    iget-object v0, p0, Lorg/apache/tools/tar/TarBuffer;->outStream:Ljava/io/OutputStream;

    invoke-virtual {v0}, Ljava/io/OutputStream;->flush()V

    iput v3, p0, Lorg/apache/tools/tar/TarBuffer;->currRecIdx:I

    iget v0, p0, Lorg/apache/tools/tar/TarBuffer;->currBlkIdx:I

    add-int/lit8 v0, v0, 0x1

    iput v0, p0, Lorg/apache/tools/tar/TarBuffer;->currBlkIdx:I

    return-void
.end method


# virtual methods
.method public close()V
    .locals 3
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    const/4 v2, 0x0

    iget-boolean v0, p0, Lorg/apache/tools/tar/TarBuffer;->debug:Z

    if-eqz v0, :cond_0

    sget-object v0, Ljava/lang/System;->err:Ljava/io/PrintStream;

    const-string v1, "TarBuffer.closeBuffer()."

    invoke-virtual {v0, v1}, Ljava/io/PrintStream;->println(Ljava/lang/String;)V

    :cond_0
    iget-object v0, p0, Lorg/apache/tools/tar/TarBuffer;->outStream:Ljava/io/OutputStream;

    if-eqz v0, :cond_2

    invoke-direct {p0}, Lorg/apache/tools/tar/TarBuffer;->flushBlock()V

    iget-object v0, p0, Lorg/apache/tools/tar/TarBuffer;->outStream:Ljava/io/OutputStream;

    sget-object v1, Ljava/lang/System;->out:Ljava/io/PrintStream;

    if-eq v0, v1, :cond_1

    iget-object v0, p0, Lorg/apache/tools/tar/TarBuffer;->outStream:Ljava/io/OutputStream;

    sget-object v1, Ljava/lang/System;->err:Ljava/io/PrintStream;

    if-eq v0, v1, :cond_1

    iget-object v0, p0, Lorg/apache/tools/tar/TarBuffer;->outStream:Ljava/io/OutputStream;

    invoke-virtual {v0}, Ljava/io/OutputStream;->close()V

    iput-object v2, p0, Lorg/apache/tools/tar/TarBuffer;->outStream:Ljava/io/OutputStream;

    :cond_1
    :goto_0
    return-void

    :cond_2
    iget-object v0, p0, Lorg/apache/tools/tar/TarBuffer;->inStream:Ljava/io/InputStream;

    if-eqz v0, :cond_1

    iget-object v0, p0, Lorg/apache/tools/tar/TarBuffer;->inStream:Ljava/io/InputStream;

    sget-object v1, Ljava/lang/System;->in:Ljava/io/InputStream;

    if-eq v0, v1, :cond_1

    iget-object v0, p0, Lorg/apache/tools/tar/TarBuffer;->inStream:Ljava/io/InputStream;

    invoke-virtual {v0}, Ljava/io/InputStream;->close()V

    iput-object v2, p0, Lorg/apache/tools/tar/TarBuffer;->inStream:Ljava/io/InputStream;

    goto :goto_0
.end method

.method public getBlockSize()I
    .locals 1

    iget v0, p0, Lorg/apache/tools/tar/TarBuffer;->blockSize:I

    return v0
.end method

.method public getCurrentBlockNum()I
    .locals 1

    iget v0, p0, Lorg/apache/tools/tar/TarBuffer;->currBlkIdx:I

    return v0
.end method

.method public getCurrentRecordNum()I
    .locals 1

    iget v0, p0, Lorg/apache/tools/tar/TarBuffer;->currRecIdx:I

    add-int/lit8 v0, v0, -0x1

    return v0
.end method

.method public getRecordSize()I
    .locals 1

    iget v0, p0, Lorg/apache/tools/tar/TarBuffer;->recordSize:I

    return v0
.end method

.method public isEOFRecord([B)Z
    .locals 3
    .param p1    # [B

    const/4 v0, 0x0

    invoke-virtual {p0}, Lorg/apache/tools/tar/TarBuffer;->getRecordSize()I

    move-result v1

    :goto_0
    if-ge v0, v1, :cond_1

    aget-byte v2, p1, v0

    if-eqz v2, :cond_0

    const/4 v2, 0x0

    :goto_1
    return v2

    :cond_0
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    :cond_1
    const/4 v2, 0x1

    goto :goto_1
.end method

.method public readRecord()[B
    .locals 5
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    iget-boolean v1, p0, Lorg/apache/tools/tar/TarBuffer;->debug:Z

    if-eqz v1, :cond_0

    sget-object v1, Ljava/lang/System;->err:Ljava/io/PrintStream;

    new-instance v2, Ljava/lang/StringBuffer;

    invoke-direct {v2}, Ljava/lang/StringBuffer;-><init>()V

    const-string v3, "ReadRecord: recIdx = "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    move-result-object v2

    iget v3, p0, Lorg/apache/tools/tar/TarBuffer;->currRecIdx:I

    invoke-virtual {v2, v3}, Ljava/lang/StringBuffer;->append(I)Ljava/lang/StringBuffer;

    move-result-object v2

    const-string v3, " blkIdx = "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    move-result-object v2

    iget v3, p0, Lorg/apache/tools/tar/TarBuffer;->currBlkIdx:I

    invoke-virtual {v2, v3}, Ljava/lang/StringBuffer;->append(I)Ljava/lang/StringBuffer;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuffer;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/io/PrintStream;->println(Ljava/lang/String;)V

    :cond_0
    iget-object v1, p0, Lorg/apache/tools/tar/TarBuffer;->inStream:Ljava/io/InputStream;

    if-nez v1, :cond_1

    new-instance v1, Ljava/io/IOException;

    const-string v2, "reading from an output buffer"

    invoke-direct {v1, v2}, Ljava/io/IOException;-><init>(Ljava/lang/String;)V

    throw v1

    :cond_1
    iget v1, p0, Lorg/apache/tools/tar/TarBuffer;->currRecIdx:I

    iget v2, p0, Lorg/apache/tools/tar/TarBuffer;->recsPerBlock:I

    if-lt v1, v2, :cond_2

    invoke-direct {p0}, Lorg/apache/tools/tar/TarBuffer;->readBlock()Z

    move-result v1

    if-nez v1, :cond_2

    const/4 v0, 0x0

    :goto_0
    return-object v0

    :cond_2
    iget v1, p0, Lorg/apache/tools/tar/TarBuffer;->recordSize:I

    new-array v0, v1, [B

    iget-object v1, p0, Lorg/apache/tools/tar/TarBuffer;->blockBuffer:[B

    iget v2, p0, Lorg/apache/tools/tar/TarBuffer;->currRecIdx:I

    iget v3, p0, Lorg/apache/tools/tar/TarBuffer;->recordSize:I

    mul-int/2addr v2, v3

    const/4 v3, 0x0

    iget v4, p0, Lorg/apache/tools/tar/TarBuffer;->recordSize:I

    invoke-static {v1, v2, v0, v3, v4}, Ljava/lang/System;->arraycopy(Ljava/lang/Object;ILjava/lang/Object;II)V

    iget v1, p0, Lorg/apache/tools/tar/TarBuffer;->currRecIdx:I

    add-int/lit8 v1, v1, 0x1

    iput v1, p0, Lorg/apache/tools/tar/TarBuffer;->currRecIdx:I

    goto :goto_0
.end method

.method public setDebug(Z)V
    .locals 0
    .param p1    # Z

    iput-boolean p1, p0, Lorg/apache/tools/tar/TarBuffer;->debug:Z

    return-void
.end method

.method public skipRecord()V
    .locals 3
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    iget-boolean v0, p0, Lorg/apache/tools/tar/TarBuffer;->debug:Z

    if-eqz v0, :cond_0

    sget-object v0, Ljava/lang/System;->err:Ljava/io/PrintStream;

    new-instance v1, Ljava/lang/StringBuffer;

    invoke-direct {v1}, Ljava/lang/StringBuffer;-><init>()V

    const-string v2, "SkipRecord: recIdx = "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    move-result-object v1

    iget v2, p0, Lorg/apache/tools/tar/TarBuffer;->currRecIdx:I

    invoke-virtual {v1, v2}, Ljava/lang/StringBuffer;->append(I)Ljava/lang/StringBuffer;

    move-result-object v1

    const-string v2, " blkIdx = "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    move-result-object v1

    iget v2, p0, Lorg/apache/tools/tar/TarBuffer;->currBlkIdx:I

    invoke-virtual {v1, v2}, Ljava/lang/StringBuffer;->append(I)Ljava/lang/StringBuffer;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuffer;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/io/PrintStream;->println(Ljava/lang/String;)V

    :cond_0
    iget-object v0, p0, Lorg/apache/tools/tar/TarBuffer;->inStream:Ljava/io/InputStream;

    if-nez v0, :cond_1

    new-instance v0, Ljava/io/IOException;

    const-string v1, "reading (via skip) from an output buffer"

    invoke-direct {v0, v1}, Ljava/io/IOException;-><init>(Ljava/lang/String;)V

    throw v0

    :cond_1
    iget v0, p0, Lorg/apache/tools/tar/TarBuffer;->currRecIdx:I

    iget v1, p0, Lorg/apache/tools/tar/TarBuffer;->recsPerBlock:I

    if-lt v0, v1, :cond_2

    invoke-direct {p0}, Lorg/apache/tools/tar/TarBuffer;->readBlock()Z

    move-result v0

    if-nez v0, :cond_2

    :goto_0
    return-void

    :cond_2
    iget v0, p0, Lorg/apache/tools/tar/TarBuffer;->currRecIdx:I

    add-int/lit8 v0, v0, 0x1

    iput v0, p0, Lorg/apache/tools/tar/TarBuffer;->currRecIdx:I

    goto :goto_0
.end method

.method public writeRecord([B)V
    .locals 4
    .param p1    # [B
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    iget-boolean v0, p0, Lorg/apache/tools/tar/TarBuffer;->debug:Z

    if-eqz v0, :cond_0

    sget-object v0, Ljava/lang/System;->err:Ljava/io/PrintStream;

    new-instance v1, Ljava/lang/StringBuffer;

    invoke-direct {v1}, Ljava/lang/StringBuffer;-><init>()V

    const-string v2, "WriteRecord: recIdx = "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    move-result-object v1

    iget v2, p0, Lorg/apache/tools/tar/TarBuffer;->currRecIdx:I

    invoke-virtual {v1, v2}, Ljava/lang/StringBuffer;->append(I)Ljava/lang/StringBuffer;

    move-result-object v1

    const-string v2, " blkIdx = "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    move-result-object v1

    iget v2, p0, Lorg/apache/tools/tar/TarBuffer;->currBlkIdx:I

    invoke-virtual {v1, v2}, Ljava/lang/StringBuffer;->append(I)Ljava/lang/StringBuffer;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuffer;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/io/PrintStream;->println(Ljava/lang/String;)V

    :cond_0
    iget-object v0, p0, Lorg/apache/tools/tar/TarBuffer;->outStream:Ljava/io/OutputStream;

    if-nez v0, :cond_1

    new-instance v0, Ljava/io/IOException;

    const-string v1, "writing to an input buffer"

    invoke-direct {v0, v1}, Ljava/io/IOException;-><init>(Ljava/lang/String;)V

    throw v0

    :cond_1
    array-length v0, p1

    iget v1, p0, Lorg/apache/tools/tar/TarBuffer;->recordSize:I

    if-eq v0, v1, :cond_2

    new-instance v0, Ljava/io/IOException;

    new-instance v1, Ljava/lang/StringBuffer;

    invoke-direct {v1}, Ljava/lang/StringBuffer;-><init>()V

    const-string v2, "record to write has length \'"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    move-result-object v1

    array-length v2, p1

    invoke-virtual {v1, v2}, Ljava/lang/StringBuffer;->append(I)Ljava/lang/StringBuffer;

    move-result-object v1

    const-string v2, "\' which is not the record size of \'"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    move-result-object v1

    iget v2, p0, Lorg/apache/tools/tar/TarBuffer;->recordSize:I

    invoke-virtual {v1, v2}, Ljava/lang/StringBuffer;->append(I)Ljava/lang/StringBuffer;

    move-result-object v1

    const-string v2, "\'"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuffer;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/io/IOException;-><init>(Ljava/lang/String;)V

    throw v0

    :cond_2
    iget v0, p0, Lorg/apache/tools/tar/TarBuffer;->currRecIdx:I

    iget v1, p0, Lorg/apache/tools/tar/TarBuffer;->recsPerBlock:I

    if-lt v0, v1, :cond_3

    invoke-direct {p0}, Lorg/apache/tools/tar/TarBuffer;->writeBlock()V

    :cond_3
    const/4 v0, 0x0

    iget-object v1, p0, Lorg/apache/tools/tar/TarBuffer;->blockBuffer:[B

    iget v2, p0, Lorg/apache/tools/tar/TarBuffer;->currRecIdx:I

    iget v3, p0, Lorg/apache/tools/tar/TarBuffer;->recordSize:I

    mul-int/2addr v2, v3

    iget v3, p0, Lorg/apache/tools/tar/TarBuffer;->recordSize:I

    invoke-static {p1, v0, v1, v2, v3}, Ljava/lang/System;->arraycopy(Ljava/lang/Object;ILjava/lang/Object;II)V

    iget v0, p0, Lorg/apache/tools/tar/TarBuffer;->currRecIdx:I

    add-int/lit8 v0, v0, 0x1

    iput v0, p0, Lorg/apache/tools/tar/TarBuffer;->currRecIdx:I

    return-void
.end method

.method public writeRecord([BI)V
    .locals 3
    .param p1    # [B
    .param p2    # I
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    iget-boolean v0, p0, Lorg/apache/tools/tar/TarBuffer;->debug:Z

    if-eqz v0, :cond_0

    sget-object v0, Ljava/lang/System;->err:Ljava/io/PrintStream;

    new-instance v1, Ljava/lang/StringBuffer;

    invoke-direct {v1}, Ljava/lang/StringBuffer;-><init>()V

    const-string v2, "WriteRecord: recIdx = "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    move-result-object v1

    iget v2, p0, Lorg/apache/tools/tar/TarBuffer;->currRecIdx:I

    invoke-virtual {v1, v2}, Ljava/lang/StringBuffer;->append(I)Ljava/lang/StringBuffer;

    move-result-object v1

    const-string v2, " blkIdx = "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    move-result-object v1

    iget v2, p0, Lorg/apache/tools/tar/TarBuffer;->currBlkIdx:I

    invoke-virtual {v1, v2}, Ljava/lang/StringBuffer;->append(I)Ljava/lang/StringBuffer;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuffer;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/io/PrintStream;->println(Ljava/lang/String;)V

    :cond_0
    iget-object v0, p0, Lorg/apache/tools/tar/TarBuffer;->outStream:Ljava/io/OutputStream;

    if-nez v0, :cond_1

    new-instance v0, Ljava/io/IOException;

    const-string v1, "writing to an input buffer"

    invoke-direct {v0, v1}, Ljava/io/IOException;-><init>(Ljava/lang/String;)V

    throw v0

    :cond_1
    iget v0, p0, Lorg/apache/tools/tar/TarBuffer;->recordSize:I

    add-int/2addr v0, p2

    array-length v1, p1

    if-le v0, v1, :cond_2

    new-instance v0, Ljava/io/IOException;

    new-instance v1, Ljava/lang/StringBuffer;

    invoke-direct {v1}, Ljava/lang/StringBuffer;-><init>()V

    const-string v2, "record has length \'"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    move-result-object v1

    array-length v2, p1

    invoke-virtual {v1, v2}, Ljava/lang/StringBuffer;->append(I)Ljava/lang/StringBuffer;

    move-result-object v1

    const-string v2, "\' with offset \'"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    move-result-object v1

    invoke-virtual {v1, p2}, Ljava/lang/StringBuffer;->append(I)Ljava/lang/StringBuffer;

    move-result-object v1

    const-string v2, "\' which is less than the record size of \'"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    move-result-object v1

    iget v2, p0, Lorg/apache/tools/tar/TarBuffer;->recordSize:I

    invoke-virtual {v1, v2}, Ljava/lang/StringBuffer;->append(I)Ljava/lang/StringBuffer;

    move-result-object v1

    const-string v2, "\'"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuffer;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/io/IOException;-><init>(Ljava/lang/String;)V

    throw v0

    :cond_2
    iget v0, p0, Lorg/apache/tools/tar/TarBuffer;->currRecIdx:I

    iget v1, p0, Lorg/apache/tools/tar/TarBuffer;->recsPerBlock:I

    if-lt v0, v1, :cond_3

    invoke-direct {p0}, Lorg/apache/tools/tar/TarBuffer;->writeBlock()V

    :cond_3
    iget-object v0, p0, Lorg/apache/tools/tar/TarBuffer;->blockBuffer:[B

    iget v1, p0, Lorg/apache/tools/tar/TarBuffer;->currRecIdx:I

    iget v2, p0, Lorg/apache/tools/tar/TarBuffer;->recordSize:I

    mul-int/2addr v1, v2

    iget v2, p0, Lorg/apache/tools/tar/TarBuffer;->recordSize:I

    invoke-static {p1, p2, v0, v1, v2}, Ljava/lang/System;->arraycopy(Ljava/lang/Object;ILjava/lang/Object;II)V

    iget v0, p0, Lorg/apache/tools/tar/TarBuffer;->currRecIdx:I

    add-int/lit8 v0, v0, 0x1

    iput v0, p0, Lorg/apache/tools/tar/TarBuffer;->currRecIdx:I

    return-void
.end method
