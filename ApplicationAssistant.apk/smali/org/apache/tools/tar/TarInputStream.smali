.class public Lorg/apache/tools/tar/TarInputStream;
.super Ljava/io/FilterInputStream;
.source "TarInputStream.java"


# instance fields
.field protected buffer:Lorg/apache/tools/tar/TarBuffer;

.field protected currEntry:Lorg/apache/tools/tar/TarEntry;

.field protected debug:Z

.field protected entryOffset:J

.field protected entrySize:J

.field protected hasHitEOF:Z

.field protected oneBuf:[B

.field protected readBuf:[B


# direct methods
.method public constructor <init>(Ljava/io/InputStream;)V
    .locals 2
    .param p1    # Ljava/io/InputStream;

    const/16 v0, 0x2800

    const/16 v1, 0x200

    invoke-direct {p0, p1, v0, v1}, Lorg/apache/tools/tar/TarInputStream;-><init>(Ljava/io/InputStream;II)V

    return-void
.end method

.method public constructor <init>(Ljava/io/InputStream;I)V
    .locals 1
    .param p1    # Ljava/io/InputStream;
    .param p2    # I

    const/16 v0, 0x200

    invoke-direct {p0, p1, p2, v0}, Lorg/apache/tools/tar/TarInputStream;-><init>(Ljava/io/InputStream;II)V

    return-void
.end method

.method public constructor <init>(Ljava/io/InputStream;II)V
    .locals 2
    .param p1    # Ljava/io/InputStream;
    .param p2    # I
    .param p3    # I

    const/4 v1, 0x0

    invoke-direct {p0, p1}, Ljava/io/FilterInputStream;-><init>(Ljava/io/InputStream;)V

    new-instance v0, Lorg/apache/tools/tar/TarBuffer;

    invoke-direct {v0, p1, p2, p3}, Lorg/apache/tools/tar/TarBuffer;-><init>(Ljava/io/InputStream;II)V

    iput-object v0, p0, Lorg/apache/tools/tar/TarInputStream;->buffer:Lorg/apache/tools/tar/TarBuffer;

    const/4 v0, 0x0

    iput-object v0, p0, Lorg/apache/tools/tar/TarInputStream;->readBuf:[B

    const/4 v0, 0x1

    new-array v0, v0, [B

    iput-object v0, p0, Lorg/apache/tools/tar/TarInputStream;->oneBuf:[B

    iput-boolean v1, p0, Lorg/apache/tools/tar/TarInputStream;->debug:Z

    iput-boolean v1, p0, Lorg/apache/tools/tar/TarInputStream;->hasHitEOF:Z

    return-void
.end method


# virtual methods
.method public available()I
    .locals 4
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    iget-wide v0, p0, Lorg/apache/tools/tar/TarInputStream;->entrySize:J

    iget-wide v2, p0, Lorg/apache/tools/tar/TarInputStream;->entryOffset:J

    sub-long/2addr v0, v2

    const-wide/32 v2, 0x7fffffff

    cmp-long v0, v0, v2

    if-lez v0, :cond_0

    const v0, 0x7fffffff

    :goto_0
    return v0

    :cond_0
    iget-wide v0, p0, Lorg/apache/tools/tar/TarInputStream;->entrySize:J

    iget-wide v2, p0, Lorg/apache/tools/tar/TarInputStream;->entryOffset:J

    sub-long/2addr v0, v2

    long-to-int v0, v0

    goto :goto_0
.end method

.method public close()V
    .locals 1
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    iget-object v0, p0, Lorg/apache/tools/tar/TarInputStream;->buffer:Lorg/apache/tools/tar/TarBuffer;

    invoke-virtual {v0}, Lorg/apache/tools/tar/TarBuffer;->close()V

    return-void
.end method

.method public copyEntryContents(Ljava/io/OutputStream;)V
    .locals 4
    .param p1    # Ljava/io/OutputStream;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    const/4 v3, 0x0

    const v2, 0x8000

    new-array v0, v2, [B

    :goto_0
    array-length v2, v0

    invoke-virtual {p0, v0, v3, v2}, Lorg/apache/tools/tar/TarInputStream;->read([BII)I

    move-result v1

    const/4 v2, -0x1

    if-ne v1, v2, :cond_0

    return-void

    :cond_0
    invoke-virtual {p1, v0, v3, v1}, Ljava/io/OutputStream;->write([BII)V

    goto :goto_0
.end method

.method public getNextEntry()Lorg/apache/tools/tar/TarEntry;
    .locals 14
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    const-wide/16 v12, 0x0

    const/4 v11, 0x1

    const/4 v6, 0x0

    iget-boolean v7, p0, Lorg/apache/tools/tar/TarInputStream;->hasHitEOF:Z

    if-eqz v7, :cond_1

    :cond_0
    :goto_0
    return-object v6

    :cond_1
    iget-object v7, p0, Lorg/apache/tools/tar/TarInputStream;->currEntry:Lorg/apache/tools/tar/TarEntry;

    if-eqz v7, :cond_4

    iget-wide v7, p0, Lorg/apache/tools/tar/TarInputStream;->entrySize:J

    iget-wide v9, p0, Lorg/apache/tools/tar/TarInputStream;->entryOffset:J

    sub-long v4, v7, v9

    iget-boolean v7, p0, Lorg/apache/tools/tar/TarInputStream;->debug:Z

    if-eqz v7, :cond_2

    sget-object v7, Ljava/lang/System;->err:Ljava/io/PrintStream;

    new-instance v8, Ljava/lang/StringBuffer;

    invoke-direct {v8}, Ljava/lang/StringBuffer;-><init>()V

    const-string v9, "TarInputStream: SKIP currENTRY \'"

    invoke-virtual {v8, v9}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    move-result-object v8

    iget-object v9, p0, Lorg/apache/tools/tar/TarInputStream;->currEntry:Lorg/apache/tools/tar/TarEntry;

    invoke-virtual {v9}, Lorg/apache/tools/tar/TarEntry;->getName()Ljava/lang/String;

    move-result-object v9

    invoke-virtual {v8, v9}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    move-result-object v8

    const-string v9, "\' SZ "

    invoke-virtual {v8, v9}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    move-result-object v8

    iget-wide v9, p0, Lorg/apache/tools/tar/TarInputStream;->entrySize:J

    invoke-virtual {v8, v9, v10}, Ljava/lang/StringBuffer;->append(J)Ljava/lang/StringBuffer;

    move-result-object v8

    const-string v9, " OFF "

    invoke-virtual {v8, v9}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    move-result-object v8

    iget-wide v9, p0, Lorg/apache/tools/tar/TarInputStream;->entryOffset:J

    invoke-virtual {v8, v9, v10}, Ljava/lang/StringBuffer;->append(J)Ljava/lang/StringBuffer;

    move-result-object v8

    const-string v9, "  skipping "

    invoke-virtual {v8, v9}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    move-result-object v8

    invoke-virtual {v8, v4, v5}, Ljava/lang/StringBuffer;->append(J)Ljava/lang/StringBuffer;

    move-result-object v8

    const-string v9, " bytes"

    invoke-virtual {v8, v9}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    move-result-object v8

    invoke-virtual {v8}, Ljava/lang/StringBuffer;->toString()Ljava/lang/String;

    move-result-object v8

    invoke-virtual {v7, v8}, Ljava/io/PrintStream;->println(Ljava/lang/String;)V

    :cond_2
    cmp-long v7, v4, v12

    if-lez v7, :cond_3

    invoke-virtual {p0, v4, v5}, Lorg/apache/tools/tar/TarInputStream;->skip(J)J

    :cond_3
    iput-object v6, p0, Lorg/apache/tools/tar/TarInputStream;->readBuf:[B

    :cond_4
    iget-object v7, p0, Lorg/apache/tools/tar/TarInputStream;->buffer:Lorg/apache/tools/tar/TarBuffer;

    invoke-virtual {v7}, Lorg/apache/tools/tar/TarBuffer;->readRecord()[B

    move-result-object v1

    if-nez v1, :cond_7

    iget-boolean v7, p0, Lorg/apache/tools/tar/TarInputStream;->debug:Z

    if-eqz v7, :cond_5

    sget-object v7, Ljava/lang/System;->err:Ljava/io/PrintStream;

    const-string v8, "READ NULL RECORD"

    invoke-virtual {v7, v8}, Ljava/io/PrintStream;->println(Ljava/lang/String;)V

    :cond_5
    iput-boolean v11, p0, Lorg/apache/tools/tar/TarInputStream;->hasHitEOF:Z

    :cond_6
    :goto_1
    iget-boolean v7, p0, Lorg/apache/tools/tar/TarInputStream;->hasHitEOF:Z

    if-eqz v7, :cond_9

    iput-object v6, p0, Lorg/apache/tools/tar/TarInputStream;->currEntry:Lorg/apache/tools/tar/TarEntry;

    :goto_2
    iget-object v7, p0, Lorg/apache/tools/tar/TarInputStream;->currEntry:Lorg/apache/tools/tar/TarEntry;

    if-eqz v7, :cond_d

    iget-object v7, p0, Lorg/apache/tools/tar/TarInputStream;->currEntry:Lorg/apache/tools/tar/TarEntry;

    invoke-virtual {v7}, Lorg/apache/tools/tar/TarEntry;->isGNULongNameEntry()Z

    move-result v7

    if-eqz v7, :cond_d

    new-instance v3, Ljava/lang/StringBuffer;

    invoke-direct {v3}, Ljava/lang/StringBuffer;-><init>()V

    const/16 v7, 0x100

    new-array v0, v7, [B

    const/4 v2, 0x0

    :goto_3
    invoke-virtual {p0, v0}, Lorg/apache/tools/tar/TarInputStream;->read([B)I

    move-result v2

    if-ltz v2, :cond_b

    new-instance v7, Ljava/lang/String;

    const/4 v8, 0x0

    invoke-direct {v7, v0, v8, v2}, Ljava/lang/String;-><init>([BII)V

    invoke-virtual {v3, v7}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    goto :goto_3

    :cond_7
    iget-object v7, p0, Lorg/apache/tools/tar/TarInputStream;->buffer:Lorg/apache/tools/tar/TarBuffer;

    invoke-virtual {v7, v1}, Lorg/apache/tools/tar/TarBuffer;->isEOFRecord([B)Z

    move-result v7

    if-eqz v7, :cond_6

    iget-boolean v7, p0, Lorg/apache/tools/tar/TarInputStream;->debug:Z

    if-eqz v7, :cond_8

    sget-object v7, Ljava/lang/System;->err:Ljava/io/PrintStream;

    const-string v8, "READ EOF RECORD"

    invoke-virtual {v7, v8}, Ljava/io/PrintStream;->println(Ljava/lang/String;)V

    :cond_8
    iput-boolean v11, p0, Lorg/apache/tools/tar/TarInputStream;->hasHitEOF:Z

    goto :goto_1

    :cond_9
    new-instance v7, Lorg/apache/tools/tar/TarEntry;

    invoke-direct {v7, v1}, Lorg/apache/tools/tar/TarEntry;-><init>([B)V

    iput-object v7, p0, Lorg/apache/tools/tar/TarInputStream;->currEntry:Lorg/apache/tools/tar/TarEntry;

    iget-boolean v7, p0, Lorg/apache/tools/tar/TarInputStream;->debug:Z

    if-eqz v7, :cond_a

    sget-object v7, Ljava/lang/System;->err:Ljava/io/PrintStream;

    new-instance v8, Ljava/lang/StringBuffer;

    invoke-direct {v8}, Ljava/lang/StringBuffer;-><init>()V

    const-string v9, "TarInputStream: SET CURRENTRY \'"

    invoke-virtual {v8, v9}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    move-result-object v8

    iget-object v9, p0, Lorg/apache/tools/tar/TarInputStream;->currEntry:Lorg/apache/tools/tar/TarEntry;

    invoke-virtual {v9}, Lorg/apache/tools/tar/TarEntry;->getName()Ljava/lang/String;

    move-result-object v9

    invoke-virtual {v8, v9}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    move-result-object v8

    const-string v9, "\' size = "

    invoke-virtual {v8, v9}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    move-result-object v8

    iget-object v9, p0, Lorg/apache/tools/tar/TarInputStream;->currEntry:Lorg/apache/tools/tar/TarEntry;

    invoke-virtual {v9}, Lorg/apache/tools/tar/TarEntry;->getSize()J

    move-result-wide v9

    invoke-virtual {v8, v9, v10}, Ljava/lang/StringBuffer;->append(J)Ljava/lang/StringBuffer;

    move-result-object v8

    invoke-virtual {v8}, Ljava/lang/StringBuffer;->toString()Ljava/lang/String;

    move-result-object v8

    invoke-virtual {v7, v8}, Ljava/io/PrintStream;->println(Ljava/lang/String;)V

    :cond_a
    iput-wide v12, p0, Lorg/apache/tools/tar/TarInputStream;->entryOffset:J

    iget-object v7, p0, Lorg/apache/tools/tar/TarInputStream;->currEntry:Lorg/apache/tools/tar/TarEntry;

    invoke-virtual {v7}, Lorg/apache/tools/tar/TarEntry;->getSize()J

    move-result-wide v7

    iput-wide v7, p0, Lorg/apache/tools/tar/TarInputStream;->entrySize:J

    goto :goto_2

    :cond_b
    invoke-virtual {p0}, Lorg/apache/tools/tar/TarInputStream;->getNextEntry()Lorg/apache/tools/tar/TarEntry;

    iget-object v7, p0, Lorg/apache/tools/tar/TarInputStream;->currEntry:Lorg/apache/tools/tar/TarEntry;

    if-eqz v7, :cond_0

    invoke-virtual {v3}, Ljava/lang/StringBuffer;->length()I

    move-result v6

    if-lez v6, :cond_c

    invoke-virtual {v3}, Ljava/lang/StringBuffer;->length()I

    move-result v6

    add-int/lit8 v6, v6, -0x1

    invoke-virtual {v3, v6}, Ljava/lang/StringBuffer;->charAt(I)C

    move-result v6

    if-nez v6, :cond_c

    invoke-virtual {v3}, Ljava/lang/StringBuffer;->length()I

    move-result v6

    add-int/lit8 v6, v6, -0x1

    invoke-virtual {v3, v6}, Ljava/lang/StringBuffer;->deleteCharAt(I)Ljava/lang/StringBuffer;

    :cond_c
    iget-object v6, p0, Lorg/apache/tools/tar/TarInputStream;->currEntry:Lorg/apache/tools/tar/TarEntry;

    invoke-virtual {v3}, Ljava/lang/StringBuffer;->toString()Ljava/lang/String;

    move-result-object v7

    invoke-virtual {v6, v7}, Lorg/apache/tools/tar/TarEntry;->setName(Ljava/lang/String;)V

    :cond_d
    iget-object v6, p0, Lorg/apache/tools/tar/TarInputStream;->currEntry:Lorg/apache/tools/tar/TarEntry;

    goto/16 :goto_0
.end method

.method public getRecordSize()I
    .locals 1

    iget-object v0, p0, Lorg/apache/tools/tar/TarInputStream;->buffer:Lorg/apache/tools/tar/TarBuffer;

    invoke-virtual {v0}, Lorg/apache/tools/tar/TarBuffer;->getRecordSize()I

    move-result v0

    return v0
.end method

.method public mark(I)V
    .locals 0
    .param p1    # I

    return-void
.end method

.method public markSupported()Z
    .locals 1

    const/4 v0, 0x0

    return v0
.end method

.method public read()I
    .locals 5
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    const/4 v4, 0x0

    const/4 v1, -0x1

    iget-object v2, p0, Lorg/apache/tools/tar/TarInputStream;->oneBuf:[B

    const/4 v3, 0x1

    invoke-virtual {p0, v2, v4, v3}, Lorg/apache/tools/tar/TarInputStream;->read([BII)I

    move-result v0

    if-ne v0, v1, :cond_0

    :goto_0
    return v1

    :cond_0
    iget-object v1, p0, Lorg/apache/tools/tar/TarInputStream;->oneBuf:[B

    aget-byte v1, v1, v4

    and-int/lit16 v1, v1, 0xff

    goto :goto_0
.end method

.method public read([BII)I
    .locals 11
    .param p1    # [B
    .param p2    # I
    .param p3    # I
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    const/4 v10, 0x0

    const/4 v5, 0x0

    iget-wide v6, p0, Lorg/apache/tools/tar/TarInputStream;->entryOffset:J

    iget-wide v8, p0, Lorg/apache/tools/tar/TarInputStream;->entrySize:J

    cmp-long v6, v6, v8

    if-ltz v6, :cond_0

    const/4 v6, -0x1

    :goto_0
    return v6

    :cond_0
    int-to-long v6, p3

    iget-wide v8, p0, Lorg/apache/tools/tar/TarInputStream;->entryOffset:J

    add-long/2addr v6, v8

    iget-wide v8, p0, Lorg/apache/tools/tar/TarInputStream;->entrySize:J

    cmp-long v6, v6, v8

    if-lez v6, :cond_1

    iget-wide v6, p0, Lorg/apache/tools/tar/TarInputStream;->entrySize:J

    iget-wide v8, p0, Lorg/apache/tools/tar/TarInputStream;->entryOffset:J

    sub-long/2addr v6, v8

    long-to-int p3, v6

    :cond_1
    iget-object v6, p0, Lorg/apache/tools/tar/TarInputStream;->readBuf:[B

    if-eqz v6, :cond_2

    iget-object v6, p0, Lorg/apache/tools/tar/TarInputStream;->readBuf:[B

    array-length v6, v6

    if-le p3, v6, :cond_3

    iget-object v6, p0, Lorg/apache/tools/tar/TarInputStream;->readBuf:[B

    array-length v4, v6

    :goto_1
    iget-object v6, p0, Lorg/apache/tools/tar/TarInputStream;->readBuf:[B

    invoke-static {v6, v10, p1, p2, v4}, Ljava/lang/System;->arraycopy(Ljava/lang/Object;ILjava/lang/Object;II)V

    iget-object v6, p0, Lorg/apache/tools/tar/TarInputStream;->readBuf:[B

    array-length v6, v6

    if-lt v4, v6, :cond_4

    const/4 v6, 0x0

    iput-object v6, p0, Lorg/apache/tools/tar/TarInputStream;->readBuf:[B

    :goto_2
    add-int/2addr v5, v4

    sub-int/2addr p3, v4

    add-int/2addr p2, v4

    :cond_2
    :goto_3
    if-lez p3, :cond_7

    iget-object v6, p0, Lorg/apache/tools/tar/TarInputStream;->buffer:Lorg/apache/tools/tar/TarBuffer;

    invoke-virtual {v6}, Lorg/apache/tools/tar/TarBuffer;->readRecord()[B

    move-result-object v2

    if-nez v2, :cond_5

    new-instance v6, Ljava/io/IOException;

    new-instance v7, Ljava/lang/StringBuffer;

    invoke-direct {v7}, Ljava/lang/StringBuffer;-><init>()V

    const-string v8, "unexpected EOF with "

    invoke-virtual {v7, v8}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    move-result-object v7

    invoke-virtual {v7, p3}, Ljava/lang/StringBuffer;->append(I)Ljava/lang/StringBuffer;

    move-result-object v7

    const-string v8, " bytes unread"

    invoke-virtual {v7, v8}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    move-result-object v7

    invoke-virtual {v7}, Ljava/lang/StringBuffer;->toString()Ljava/lang/String;

    move-result-object v7

    invoke-direct {v6, v7}, Ljava/io/IOException;-><init>(Ljava/lang/String;)V

    throw v6

    :cond_3
    move v4, p3

    goto :goto_1

    :cond_4
    iget-object v6, p0, Lorg/apache/tools/tar/TarInputStream;->readBuf:[B

    array-length v6, v6

    sub-int v1, v6, v4

    new-array v0, v1, [B

    iget-object v6, p0, Lorg/apache/tools/tar/TarInputStream;->readBuf:[B

    invoke-static {v6, v4, v0, v10, v1}, Ljava/lang/System;->arraycopy(Ljava/lang/Object;ILjava/lang/Object;II)V

    iput-object v0, p0, Lorg/apache/tools/tar/TarInputStream;->readBuf:[B

    goto :goto_2

    :cond_5
    move v4, p3

    array-length v3, v2

    if-le v3, v4, :cond_6

    invoke-static {v2, v10, p1, p2, v4}, Ljava/lang/System;->arraycopy(Ljava/lang/Object;ILjava/lang/Object;II)V

    sub-int v6, v3, v4

    new-array v6, v6, [B

    iput-object v6, p0, Lorg/apache/tools/tar/TarInputStream;->readBuf:[B

    iget-object v6, p0, Lorg/apache/tools/tar/TarInputStream;->readBuf:[B

    sub-int v7, v3, v4

    invoke-static {v2, v4, v6, v10, v7}, Ljava/lang/System;->arraycopy(Ljava/lang/Object;ILjava/lang/Object;II)V

    :goto_4
    add-int/2addr v5, v4

    sub-int/2addr p3, v4

    add-int/2addr p2, v4

    goto :goto_3

    :cond_6
    move v4, v3

    invoke-static {v2, v10, p1, p2, v3}, Ljava/lang/System;->arraycopy(Ljava/lang/Object;ILjava/lang/Object;II)V

    goto :goto_4

    :cond_7
    iget-wide v6, p0, Lorg/apache/tools/tar/TarInputStream;->entryOffset:J

    int-to-long v8, v5

    add-long/2addr v6, v8

    iput-wide v6, p0, Lorg/apache/tools/tar/TarInputStream;->entryOffset:J

    move v6, v5

    goto/16 :goto_0
.end method

.method public reset()V
    .locals 0

    return-void
.end method

.method public setDebug(Z)V
    .locals 1
    .param p1    # Z

    iput-boolean p1, p0, Lorg/apache/tools/tar/TarInputStream;->debug:Z

    iget-object v0, p0, Lorg/apache/tools/tar/TarInputStream;->buffer:Lorg/apache/tools/tar/TarBuffer;

    invoke-virtual {v0, p1}, Lorg/apache/tools/tar/TarBuffer;->setDebug(Z)V

    return-void
.end method

.method public skip(J)J
    .locals 7
    .param p1    # J
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    const/16 v5, 0x2000

    new-array v4, v5, [B

    move-wide v2, p1

    :goto_0
    const-wide/16 v5, 0x0

    cmp-long v5, v2, v5

    if-lez v5, :cond_0

    array-length v5, v4

    int-to-long v5, v5

    cmp-long v5, v2, v5

    if-lez v5, :cond_1

    array-length v5, v4

    int-to-long v5, v5

    :goto_1
    long-to-int v1, v5

    const/4 v5, 0x0

    invoke-virtual {p0, v4, v5, v1}, Lorg/apache/tools/tar/TarInputStream;->read([BII)I

    move-result v0

    const/4 v5, -0x1

    if-ne v0, v5, :cond_2

    :cond_0
    sub-long v5, p1, v2

    return-wide v5

    :cond_1
    move-wide v5, v2

    goto :goto_1

    :cond_2
    int-to-long v5, v0

    sub-long/2addr v2, v5

    goto :goto_0
.end method
