.class public Lorg/apache/tools/bzip2/CBZip2OutputStream;
.super Ljava/io/OutputStream;
.source "CBZip2OutputStream.java"

# interfaces
.implements Lorg/apache/tools/bzip2/BZip2Constants;


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lorg/apache/tools/bzip2/CBZip2OutputStream$Data;
    }
.end annotation


# static fields
.field protected static final CLEARMASK:I = -0x200001

.field protected static final DEPTH_THRESH:I = 0xa

.field protected static final GREATER_ICOST:I = 0xf

.field private static final INCS:[I

.field protected static final LESSER_ICOST:I = 0x0

.field public static final MAX_BLOCKSIZE:I = 0x9

.field public static final MIN_BLOCKSIZE:I = 0x1

.field protected static final QSORT_STACK_SIZE:I = 0x3e8

.field protected static final SETMASK:I = 0x200000

.field protected static final SMALL_THRESH:I = 0x14

.field protected static final WORK_FACTOR:I = 0x1e


# instance fields
.field private allowableBlockSize:I

.field private blockCRC:I

.field private blockRandomised:Z

.field private final blockSize100k:I

.field private bsBuff:I

.field private bsLive:I

.field private combinedCRC:I

.field private final crc:Lorg/apache/tools/bzip2/CRC;

.field private currentChar:I

.field private data:Lorg/apache/tools/bzip2/CBZip2OutputStream$Data;

.field private firstAttempt:Z

.field private last:I

.field private nInUse:I

.field private nMTF:I

.field private origPtr:I

.field private out:Ljava/io/OutputStream;

.field private runLength:I

.field private workDone:I

.field private workLimit:I


# direct methods
.method static constructor <clinit>()V
    .locals 1

    const/16 v0, 0xe

    new-array v0, v0, [I

    fill-array-data v0, :array_0

    sput-object v0, Lorg/apache/tools/bzip2/CBZip2OutputStream;->INCS:[I

    return-void

    :array_0
    .array-data 4
        0x1
        0x4
        0xd
        0x28
        0x79
        0x16c
        0x445
        0xcd0
        0x2671
        0x7354
        0x159fd
        0x40df8
        0xc29e9
        0x247dbc
    .end array-data
.end method

.method public constructor <init>(Ljava/io/OutputStream;)V
    .locals 1
    .param p1    # Ljava/io/OutputStream;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    const/16 v0, 0x9

    invoke-direct {p0, p1, v0}, Lorg/apache/tools/bzip2/CBZip2OutputStream;-><init>(Ljava/io/OutputStream;I)V

    return-void
.end method

.method public constructor <init>(Ljava/io/OutputStream;I)V
    .locals 3
    .param p1    # Ljava/io/OutputStream;
    .param p2    # I
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    invoke-direct {p0}, Ljava/io/OutputStream;-><init>()V

    new-instance v0, Lorg/apache/tools/bzip2/CRC;

    invoke-direct {v0}, Lorg/apache/tools/bzip2/CRC;-><init>()V

    iput-object v0, p0, Lorg/apache/tools/bzip2/CBZip2OutputStream;->crc:Lorg/apache/tools/bzip2/CRC;

    const/4 v0, -0x1

    iput v0, p0, Lorg/apache/tools/bzip2/CBZip2OutputStream;->currentChar:I

    const/4 v0, 0x0

    iput v0, p0, Lorg/apache/tools/bzip2/CBZip2OutputStream;->runLength:I

    const/4 v0, 0x1

    if-ge p2, v0, :cond_0

    new-instance v0, Ljava/lang/IllegalArgumentException;

    new-instance v1, Ljava/lang/StringBuffer;

    invoke-direct {v1}, Ljava/lang/StringBuffer;-><init>()V

    const-string v2, "blockSize("

    invoke-virtual {v1, v2}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    move-result-object v1

    invoke-virtual {v1, p2}, Ljava/lang/StringBuffer;->append(I)Ljava/lang/StringBuffer;

    move-result-object v1

    const-string v2, ") < 1"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuffer;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v0

    :cond_0
    const/16 v0, 0x9

    if-le p2, v0, :cond_1

    new-instance v0, Ljava/lang/IllegalArgumentException;

    new-instance v1, Ljava/lang/StringBuffer;

    invoke-direct {v1}, Ljava/lang/StringBuffer;-><init>()V

    const-string v2, "blockSize("

    invoke-virtual {v1, v2}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    move-result-object v1

    invoke-virtual {v1, p2}, Ljava/lang/StringBuffer;->append(I)Ljava/lang/StringBuffer;

    move-result-object v1

    const-string v2, ") > 9"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuffer;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v0

    :cond_1
    iput p2, p0, Lorg/apache/tools/bzip2/CBZip2OutputStream;->blockSize100k:I

    iput-object p1, p0, Lorg/apache/tools/bzip2/CBZip2OutputStream;->out:Ljava/io/OutputStream;

    invoke-direct {p0}, Lorg/apache/tools/bzip2/CBZip2OutputStream;->init()V

    return-void
.end method

.method private blockSort()V
    .locals 6

    const/4 v5, 0x0

    iget v3, p0, Lorg/apache/tools/bzip2/CBZip2OutputStream;->last:I

    mul-int/lit8 v3, v3, 0x1e

    iput v3, p0, Lorg/apache/tools/bzip2/CBZip2OutputStream;->workLimit:I

    iput v5, p0, Lorg/apache/tools/bzip2/CBZip2OutputStream;->workDone:I

    iput-boolean v5, p0, Lorg/apache/tools/bzip2/CBZip2OutputStream;->blockRandomised:Z

    const/4 v3, 0x1

    iput-boolean v3, p0, Lorg/apache/tools/bzip2/CBZip2OutputStream;->firstAttempt:Z

    invoke-direct {p0}, Lorg/apache/tools/bzip2/CBZip2OutputStream;->mainSort()V

    iget-boolean v3, p0, Lorg/apache/tools/bzip2/CBZip2OutputStream;->firstAttempt:Z

    if-eqz v3, :cond_0

    iget v3, p0, Lorg/apache/tools/bzip2/CBZip2OutputStream;->workDone:I

    iget v4, p0, Lorg/apache/tools/bzip2/CBZip2OutputStream;->workLimit:I

    if-le v3, v4, :cond_0

    invoke-direct {p0}, Lorg/apache/tools/bzip2/CBZip2OutputStream;->randomiseBlock()V

    iput v5, p0, Lorg/apache/tools/bzip2/CBZip2OutputStream;->workDone:I

    iput v5, p0, Lorg/apache/tools/bzip2/CBZip2OutputStream;->workLimit:I

    iput-boolean v5, p0, Lorg/apache/tools/bzip2/CBZip2OutputStream;->firstAttempt:Z

    invoke-direct {p0}, Lorg/apache/tools/bzip2/CBZip2OutputStream;->mainSort()V

    :cond_0
    iget-object v3, p0, Lorg/apache/tools/bzip2/CBZip2OutputStream;->data:Lorg/apache/tools/bzip2/CBZip2OutputStream$Data;

    iget-object v0, v3, Lorg/apache/tools/bzip2/CBZip2OutputStream$Data;->fmap:[I

    const/4 v3, -0x1

    iput v3, p0, Lorg/apache/tools/bzip2/CBZip2OutputStream;->origPtr:I

    const/4 v1, 0x0

    iget v2, p0, Lorg/apache/tools/bzip2/CBZip2OutputStream;->last:I

    :goto_0
    if-gt v1, v2, :cond_1

    aget v3, v0, v1

    if-nez v3, :cond_2

    iput v1, p0, Lorg/apache/tools/bzip2/CBZip2OutputStream;->origPtr:I

    :cond_1
    return-void

    :cond_2
    add-int/lit8 v1, v1, 0x1

    goto :goto_0
.end method

.method private bsFinishedWithStream()V
    .locals 2
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    :goto_0
    iget v1, p0, Lorg/apache/tools/bzip2/CBZip2OutputStream;->bsLive:I

    if-lez v1, :cond_0

    iget v1, p0, Lorg/apache/tools/bzip2/CBZip2OutputStream;->bsBuff:I

    shr-int/lit8 v0, v1, 0x18

    iget-object v1, p0, Lorg/apache/tools/bzip2/CBZip2OutputStream;->out:Ljava/io/OutputStream;

    invoke-virtual {v1, v0}, Ljava/io/OutputStream;->write(I)V

    iget v1, p0, Lorg/apache/tools/bzip2/CBZip2OutputStream;->bsBuff:I

    shl-int/lit8 v1, v1, 0x8

    iput v1, p0, Lorg/apache/tools/bzip2/CBZip2OutputStream;->bsBuff:I

    iget v1, p0, Lorg/apache/tools/bzip2/CBZip2OutputStream;->bsLive:I

    add-int/lit8 v1, v1, -0x8

    iput v1, p0, Lorg/apache/tools/bzip2/CBZip2OutputStream;->bsLive:I

    goto :goto_0

    :cond_0
    return-void
.end method

.method private bsPutInt(I)V
    .locals 2
    .param p1    # I
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    const/16 v1, 0x8

    shr-int/lit8 v0, p1, 0x18

    and-int/lit16 v0, v0, 0xff

    invoke-direct {p0, v1, v0}, Lorg/apache/tools/bzip2/CBZip2OutputStream;->bsW(II)V

    shr-int/lit8 v0, p1, 0x10

    and-int/lit16 v0, v0, 0xff

    invoke-direct {p0, v1, v0}, Lorg/apache/tools/bzip2/CBZip2OutputStream;->bsW(II)V

    shr-int/lit8 v0, p1, 0x8

    and-int/lit16 v0, v0, 0xff

    invoke-direct {p0, v1, v0}, Lorg/apache/tools/bzip2/CBZip2OutputStream;->bsW(II)V

    and-int/lit16 v0, p1, 0xff

    invoke-direct {p0, v1, v0}, Lorg/apache/tools/bzip2/CBZip2OutputStream;->bsW(II)V

    return-void
.end method

.method private bsPutUByte(I)V
    .locals 1
    .param p1    # I
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    const/16 v0, 0x8

    invoke-direct {p0, v0, p1}, Lorg/apache/tools/bzip2/CBZip2OutputStream;->bsW(II)V

    return-void
.end method

.method private bsW(II)V
    .locals 4
    .param p1    # I
    .param p2    # I
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    iget-object v2, p0, Lorg/apache/tools/bzip2/CBZip2OutputStream;->out:Ljava/io/OutputStream;

    iget v1, p0, Lorg/apache/tools/bzip2/CBZip2OutputStream;->bsLive:I

    iget v0, p0, Lorg/apache/tools/bzip2/CBZip2OutputStream;->bsBuff:I

    :goto_0
    const/16 v3, 0x8

    if-lt v1, v3, :cond_0

    shr-int/lit8 v3, v0, 0x18

    invoke-virtual {v2, v3}, Ljava/io/OutputStream;->write(I)V

    shl-int/lit8 v0, v0, 0x8

    add-int/lit8 v1, v1, -0x8

    goto :goto_0

    :cond_0
    rsub-int/lit8 v3, v1, 0x20

    sub-int/2addr v3, p1

    shl-int v3, p2, v3

    or-int/2addr v3, v0

    iput v3, p0, Lorg/apache/tools/bzip2/CBZip2OutputStream;->bsBuff:I

    add-int v3, v1, p1

    iput v3, p0, Lorg/apache/tools/bzip2/CBZip2OutputStream;->bsLive:I

    return-void
.end method

.method public static chooseBlockSize(J)I
    .locals 4
    .param p0    # J

    const-wide/16 v0, 0x0

    cmp-long v0, p0, v0

    if-lez v0, :cond_0

    const-wide/32 v0, 0x203a0

    div-long v0, p0, v0

    const-wide/16 v2, 0x1

    add-long/2addr v0, v2

    const-wide/16 v2, 0x9

    invoke-static {v0, v1, v2, v3}, Ljava/lang/Math;->min(JJ)J

    move-result-wide v0

    long-to-int v0, v0

    :goto_0
    return v0

    :cond_0
    const/16 v0, 0x9

    goto :goto_0
.end method

.method private endBlock()V
    .locals 4
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    const/16 v3, 0x59

    const/4 v2, 0x1

    iget-object v0, p0, Lorg/apache/tools/bzip2/CBZip2OutputStream;->crc:Lorg/apache/tools/bzip2/CRC;

    invoke-virtual {v0}, Lorg/apache/tools/bzip2/CRC;->getFinalCRC()I

    move-result v0

    iput v0, p0, Lorg/apache/tools/bzip2/CBZip2OutputStream;->blockCRC:I

    iget v0, p0, Lorg/apache/tools/bzip2/CBZip2OutputStream;->combinedCRC:I

    shl-int/lit8 v0, v0, 0x1

    iget v1, p0, Lorg/apache/tools/bzip2/CBZip2OutputStream;->combinedCRC:I

    ushr-int/lit8 v1, v1, 0x1f

    or-int/2addr v0, v1

    iput v0, p0, Lorg/apache/tools/bzip2/CBZip2OutputStream;->combinedCRC:I

    iget v0, p0, Lorg/apache/tools/bzip2/CBZip2OutputStream;->combinedCRC:I

    iget v1, p0, Lorg/apache/tools/bzip2/CBZip2OutputStream;->blockCRC:I

    xor-int/2addr v0, v1

    iput v0, p0, Lorg/apache/tools/bzip2/CBZip2OutputStream;->combinedCRC:I

    iget v0, p0, Lorg/apache/tools/bzip2/CBZip2OutputStream;->last:I

    const/4 v1, -0x1

    if-ne v0, v1, :cond_0

    :goto_0
    return-void

    :cond_0
    invoke-direct {p0}, Lorg/apache/tools/bzip2/CBZip2OutputStream;->blockSort()V

    const/16 v0, 0x31

    invoke-direct {p0, v0}, Lorg/apache/tools/bzip2/CBZip2OutputStream;->bsPutUByte(I)V

    const/16 v0, 0x41

    invoke-direct {p0, v0}, Lorg/apache/tools/bzip2/CBZip2OutputStream;->bsPutUByte(I)V

    invoke-direct {p0, v3}, Lorg/apache/tools/bzip2/CBZip2OutputStream;->bsPutUByte(I)V

    const/16 v0, 0x26

    invoke-direct {p0, v0}, Lorg/apache/tools/bzip2/CBZip2OutputStream;->bsPutUByte(I)V

    const/16 v0, 0x53

    invoke-direct {p0, v0}, Lorg/apache/tools/bzip2/CBZip2OutputStream;->bsPutUByte(I)V

    invoke-direct {p0, v3}, Lorg/apache/tools/bzip2/CBZip2OutputStream;->bsPutUByte(I)V

    iget v0, p0, Lorg/apache/tools/bzip2/CBZip2OutputStream;->blockCRC:I

    invoke-direct {p0, v0}, Lorg/apache/tools/bzip2/CBZip2OutputStream;->bsPutInt(I)V

    iget-boolean v0, p0, Lorg/apache/tools/bzip2/CBZip2OutputStream;->blockRandomised:Z

    if-eqz v0, :cond_1

    invoke-direct {p0, v2, v2}, Lorg/apache/tools/bzip2/CBZip2OutputStream;->bsW(II)V

    :goto_1
    invoke-direct {p0}, Lorg/apache/tools/bzip2/CBZip2OutputStream;->moveToFrontCodeAndSend()V

    goto :goto_0

    :cond_1
    const/4 v0, 0x0

    invoke-direct {p0, v2, v0}, Lorg/apache/tools/bzip2/CBZip2OutputStream;->bsW(II)V

    goto :goto_1
.end method

.method private endCompression()V
    .locals 1
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    const/16 v0, 0x17

    invoke-direct {p0, v0}, Lorg/apache/tools/bzip2/CBZip2OutputStream;->bsPutUByte(I)V

    const/16 v0, 0x72

    invoke-direct {p0, v0}, Lorg/apache/tools/bzip2/CBZip2OutputStream;->bsPutUByte(I)V

    const/16 v0, 0x45

    invoke-direct {p0, v0}, Lorg/apache/tools/bzip2/CBZip2OutputStream;->bsPutUByte(I)V

    const/16 v0, 0x38

    invoke-direct {p0, v0}, Lorg/apache/tools/bzip2/CBZip2OutputStream;->bsPutUByte(I)V

    const/16 v0, 0x50

    invoke-direct {p0, v0}, Lorg/apache/tools/bzip2/CBZip2OutputStream;->bsPutUByte(I)V

    const/16 v0, 0x90

    invoke-direct {p0, v0}, Lorg/apache/tools/bzip2/CBZip2OutputStream;->bsPutUByte(I)V

    iget v0, p0, Lorg/apache/tools/bzip2/CBZip2OutputStream;->combinedCRC:I

    invoke-direct {p0, v0}, Lorg/apache/tools/bzip2/CBZip2OutputStream;->bsPutInt(I)V

    invoke-direct {p0}, Lorg/apache/tools/bzip2/CBZip2OutputStream;->bsFinishedWithStream()V

    return-void
.end method

.method private generateMTFValues()V
    .locals 22

    move-object/from16 v0, p0

    iget v9, v0, Lorg/apache/tools/bzip2/CBZip2OutputStream;->last:I

    move-object/from16 v0, p0

    iget-object v3, v0, Lorg/apache/tools/bzip2/CBZip2OutputStream;->data:Lorg/apache/tools/bzip2/CBZip2OutputStream$Data;

    iget-object v7, v3, Lorg/apache/tools/bzip2/CBZip2OutputStream$Data;->inUse:[Z

    iget-object v2, v3, Lorg/apache/tools/bzip2/CBZip2OutputStream$Data;->block:[B

    iget-object v5, v3, Lorg/apache/tools/bzip2/CBZip2OutputStream$Data;->fmap:[I

    iget-object v13, v3, Lorg/apache/tools/bzip2/CBZip2OutputStream$Data;->sfmap:[C

    iget-object v11, v3, Lorg/apache/tools/bzip2/CBZip2OutputStream$Data;->mtfFreq:[I

    iget-object v0, v3, Lorg/apache/tools/bzip2/CBZip2OutputStream$Data;->unseqToSeq:[B

    move-object/from16 v16, v0

    iget-object v0, v3, Lorg/apache/tools/bzip2/CBZip2OutputStream$Data;->generateMTFValues_yy:[B

    move-object/from16 v18, v0

    const/4 v12, 0x0

    const/4 v6, 0x0

    :goto_0
    const/16 v20, 0x100

    move/from16 v0, v20

    if-ge v6, v0, :cond_1

    aget-boolean v20, v7, v6

    if-eqz v20, :cond_0

    int-to-byte v0, v12

    move/from16 v20, v0

    aput-byte v20, v16, v6

    add-int/lit8 v12, v12, 0x1

    :cond_0
    add-int/lit8 v6, v6, 0x1

    goto :goto_0

    :cond_1
    move-object/from16 v0, p0

    iput v12, v0, Lorg/apache/tools/bzip2/CBZip2OutputStream;->nInUse:I

    add-int/lit8 v4, v12, 0x1

    move v6, v4

    :goto_1
    if-ltz v6, :cond_2

    const/16 v20, 0x0

    aput v20, v11, v6

    add-int/lit8 v6, v6, -0x1

    goto :goto_1

    :cond_2
    move v6, v12

    :goto_2
    add-int/lit8 v6, v6, -0x1

    if-ltz v6, :cond_3

    int-to-byte v0, v6

    move/from16 v20, v0

    aput-byte v20, v18, v6

    goto :goto_2

    :cond_3
    const/16 v17, 0x0

    const/16 v19, 0x0

    const/4 v6, 0x0

    :goto_3
    if-gt v6, v9, :cond_9

    aget v20, v5, v6

    aget-byte v20, v2, v20

    move/from16 v0, v20

    and-int/lit16 v0, v0, 0xff

    move/from16 v20, v0

    aget-byte v10, v16, v20

    const/16 v20, 0x0

    aget-byte v14, v18, v20

    const/4 v8, 0x0

    :goto_4
    if-eq v10, v14, :cond_4

    add-int/lit8 v8, v8, 0x1

    move v15, v14

    aget-byte v14, v18, v8

    aput-byte v15, v18, v8

    goto :goto_4

    :cond_4
    const/16 v20, 0x0

    aput-byte v14, v18, v20

    if-nez v8, :cond_5

    add-int/lit8 v19, v19, 0x1

    :goto_5
    add-int/lit8 v6, v6, 0x1

    goto :goto_3

    :cond_5
    if-lez v19, :cond_8

    add-int/lit8 v19, v19, -0x1

    :goto_6
    and-int/lit8 v20, v19, 0x1

    if-nez v20, :cond_6

    const/16 v20, 0x0

    aput-char v20, v13, v17

    add-int/lit8 v17, v17, 0x1

    const/16 v20, 0x0

    aget v21, v11, v20

    add-int/lit8 v21, v21, 0x1

    aput v21, v11, v20

    :goto_7
    const/16 v20, 0x2

    move/from16 v0, v19

    move/from16 v1, v20

    if-lt v0, v1, :cond_7

    add-int/lit8 v20, v19, -0x2

    shr-int/lit8 v19, v20, 0x1

    goto :goto_6

    :cond_6
    const/16 v20, 0x1

    aput-char v20, v13, v17

    add-int/lit8 v17, v17, 0x1

    const/16 v20, 0x1

    aget v21, v11, v20

    add-int/lit8 v21, v21, 0x1

    aput v21, v11, v20

    goto :goto_7

    :cond_7
    const/16 v19, 0x0

    :cond_8
    add-int/lit8 v20, v8, 0x1

    move/from16 v0, v20

    int-to-char v0, v0

    move/from16 v20, v0

    aput-char v20, v13, v17

    add-int/lit8 v17, v17, 0x1

    add-int/lit8 v20, v8, 0x1

    aget v21, v11, v20

    add-int/lit8 v21, v21, 0x1

    aput v21, v11, v20

    goto :goto_5

    :cond_9
    if-lez v19, :cond_b

    add-int/lit8 v19, v19, -0x1

    :goto_8
    and-int/lit8 v20, v19, 0x1

    if-nez v20, :cond_a

    const/16 v20, 0x0

    aput-char v20, v13, v17

    add-int/lit8 v17, v17, 0x1

    const/16 v20, 0x0

    aget v21, v11, v20

    add-int/lit8 v21, v21, 0x1

    aput v21, v11, v20

    :goto_9
    const/16 v20, 0x2

    move/from16 v0, v19

    move/from16 v1, v20

    if-lt v0, v1, :cond_b

    add-int/lit8 v20, v19, -0x2

    shr-int/lit8 v19, v20, 0x1

    goto :goto_8

    :cond_a
    const/16 v20, 0x1

    aput-char v20, v13, v17

    add-int/lit8 v17, v17, 0x1

    const/16 v20, 0x1

    aget v21, v11, v20

    add-int/lit8 v21, v21, 0x1

    aput v21, v11, v20

    goto :goto_9

    :cond_b
    int-to-char v0, v4

    move/from16 v20, v0

    aput-char v20, v13, v17

    aget v20, v11, v4

    add-int/lit8 v20, v20, 0x1

    aput v20, v11, v4

    add-int/lit8 v20, v17, 0x1

    move/from16 v0, v20

    move-object/from16 v1, p0

    iput v0, v1, Lorg/apache/tools/bzip2/CBZip2OutputStream;->nMTF:I

    return-void
.end method

.method private static hbAssignCodes([I[BIII)V
    .locals 4
    .param p0    # [I
    .param p1    # [B
    .param p2    # I
    .param p3    # I
    .param p4    # I

    const/4 v2, 0x0

    move v1, p2

    :goto_0
    if-gt v1, p3, :cond_2

    const/4 v0, 0x0

    :goto_1
    if-ge v0, p4, :cond_1

    aget-byte v3, p1, v0

    and-int/lit16 v3, v3, 0xff

    if-ne v3, v1, :cond_0

    aput v2, p0, v0

    add-int/lit8 v2, v2, 0x1

    :cond_0
    add-int/lit8 v0, v0, 0x1

    goto :goto_1

    :cond_1
    shl-int/lit8 v2, v2, 0x1

    add-int/lit8 v1, v1, 0x1

    goto :goto_0

    :cond_2
    return-void
.end method

.method private static hbMakeCodeLengths([B[ILorg/apache/tools/bzip2/CBZip2OutputStream$Data;II)V
    .locals 23
    .param p0    # [B
    .param p1    # [I
    .param p2    # Lorg/apache/tools/bzip2/CBZip2OutputStream$Data;
    .param p3    # I
    .param p4    # I

    move-object/from16 v0, p2

    iget-object v2, v0, Lorg/apache/tools/bzip2/CBZip2OutputStream$Data;->heap:[I

    move-object/from16 v0, p2

    iget-object v14, v0, Lorg/apache/tools/bzip2/CBZip2OutputStream$Data;->weight:[I

    move-object/from16 v0, p2

    iget-object v10, v0, Lorg/apache/tools/bzip2/CBZip2OutputStream$Data;->parent:[I

    move/from16 v3, p3

    :goto_0
    add-int/lit8 v3, v3, -0x1

    if-ltz v3, :cond_1

    add-int/lit8 v21, v3, 0x1

    aget v20, p1, v3

    if-nez v20, :cond_0

    const/16 v20, 0x1

    :goto_1
    shl-int/lit8 v20, v20, 0x8

    aput v20, v14, v21

    goto :goto_0

    :cond_0
    aget v20, p1, v3

    goto :goto_1

    :cond_1
    const/4 v13, 0x1

    :cond_2
    if-eqz v13, :cond_11

    const/4 v13, 0x0

    move/from16 v9, p3

    const/4 v8, 0x0

    const/16 v20, 0x0

    const/16 v21, 0x0

    aput v21, v2, v20

    const/16 v20, 0x0

    const/16 v21, 0x0

    aput v21, v14, v20

    const/16 v20, 0x0

    const/16 v21, -0x2

    aput v21, v10, v20

    const/4 v3, 0x1

    :goto_2
    move/from16 v0, p3

    if-gt v3, v0, :cond_5

    const/16 v20, -0x1

    aput v20, v10, v3

    add-int/lit8 v8, v8, 0x1

    aput v3, v2, v8

    move/from16 v19, v8

    aget v12, v2, v19

    :goto_3
    aget v20, v14, v12

    shr-int/lit8 v21, v19, 0x1

    aget v21, v2, v21

    aget v21, v14, v21

    move/from16 v0, v20

    move/from16 v1, v21

    if-ge v0, v1, :cond_3

    shr-int/lit8 v20, v19, 0x1

    aget v20, v2, v20

    aput v20, v2, v19

    shr-int/lit8 v19, v19, 0x1

    goto :goto_3

    :cond_3
    aput v12, v2, v19

    add-int/lit8 v3, v3, 0x1

    goto :goto_2

    :cond_4
    aput v12, v2, v19

    :cond_5
    const/16 v20, 0x1

    move/from16 v0, v20

    if-le v8, v0, :cond_d

    const/16 v20, 0x1

    aget v6, v2, v20

    const/16 v20, 0x1

    aget v21, v2, v8

    aput v21, v2, v20

    add-int/lit8 v8, v8, -0x1

    const/16 v18, 0x0

    const/16 v19, 0x1

    const/16 v20, 0x1

    aget v12, v2, v20

    :goto_4
    shl-int/lit8 v18, v19, 0x1

    move/from16 v0, v18

    if-le v0, v8, :cond_8

    :cond_6
    aput v12, v2, v19

    const/16 v20, 0x1

    aget v7, v2, v20

    const/16 v20, 0x1

    aget v21, v2, v8

    aput v21, v2, v20

    add-int/lit8 v8, v8, -0x1

    const/16 v18, 0x0

    const/16 v19, 0x1

    const/16 v20, 0x1

    aget v12, v2, v20

    :goto_5
    shl-int/lit8 v18, v19, 0x1

    move/from16 v0, v18

    if-le v0, v8, :cond_a

    :cond_7
    aput v12, v2, v19

    add-int/lit8 v9, v9, 0x1

    aput v9, v10, v7

    aput v9, v10, v6

    aget v15, v14, v6

    aget v16, v14, v7

    and-int/lit16 v0, v15, -0x100

    move/from16 v20, v0

    move/from16 v0, v16

    and-int/lit16 v0, v0, -0x100

    move/from16 v21, v0

    add-int v21, v21, v20

    and-int/lit16 v0, v15, 0xff

    move/from16 v20, v0

    move/from16 v0, v16

    and-int/lit16 v0, v0, 0xff

    move/from16 v22, v0

    move/from16 v0, v20

    move/from16 v1, v22

    if-le v0, v1, :cond_c

    and-int/lit16 v0, v15, 0xff

    move/from16 v20, v0

    :goto_6
    add-int/lit8 v20, v20, 0x1

    or-int v20, v20, v21

    aput v20, v14, v9

    const/16 v20, -0x1

    aput v20, v10, v9

    add-int/lit8 v8, v8, 0x1

    aput v9, v2, v8

    const/4 v12, 0x0

    move/from16 v19, v8

    aget v12, v2, v19

    aget v17, v14, v12

    :goto_7
    shr-int/lit8 v20, v19, 0x1

    aget v20, v2, v20

    aget v20, v14, v20

    move/from16 v0, v17

    move/from16 v1, v20

    if-ge v0, v1, :cond_4

    shr-int/lit8 v20, v19, 0x1

    aget v20, v2, v20

    aput v20, v2, v19

    shr-int/lit8 v19, v19, 0x1

    goto :goto_7

    :cond_8
    move/from16 v0, v18

    if-ge v0, v8, :cond_9

    add-int/lit8 v20, v18, 0x1

    aget v20, v2, v20

    aget v20, v14, v20

    aget v21, v2, v18

    aget v21, v14, v21

    move/from16 v0, v20

    move/from16 v1, v21

    if-ge v0, v1, :cond_9

    add-int/lit8 v18, v18, 0x1

    :cond_9
    aget v20, v14, v12

    aget v21, v2, v18

    aget v21, v14, v21

    move/from16 v0, v20

    move/from16 v1, v21

    if-lt v0, v1, :cond_6

    aget v20, v2, v18

    aput v20, v2, v19

    move/from16 v19, v18

    goto/16 :goto_4

    :cond_a
    move/from16 v0, v18

    if-ge v0, v8, :cond_b

    add-int/lit8 v20, v18, 0x1

    aget v20, v2, v20

    aget v20, v14, v20

    aget v21, v2, v18

    aget v21, v14, v21

    move/from16 v0, v20

    move/from16 v1, v21

    if-ge v0, v1, :cond_b

    add-int/lit8 v18, v18, 0x1

    :cond_b
    aget v20, v14, v12

    aget v21, v2, v18

    aget v21, v14, v21

    move/from16 v0, v20

    move/from16 v1, v21

    if-lt v0, v1, :cond_7

    aget v20, v2, v18

    aput v20, v2, v19

    move/from16 v19, v18

    goto/16 :goto_5

    :cond_c
    move/from16 v0, v16

    and-int/lit16 v0, v0, 0xff

    move/from16 v20, v0

    goto/16 :goto_6

    :cond_d
    const/4 v3, 0x1

    :goto_8
    move/from16 v0, p3

    if-gt v3, v0, :cond_10

    const/4 v4, 0x0

    move v5, v3

    :goto_9
    aget v11, v10, v5

    if-ltz v11, :cond_e

    move v5, v11

    add-int/lit8 v4, v4, 0x1

    goto :goto_9

    :cond_e
    add-int/lit8 v20, v3, -0x1

    int-to-byte v0, v4

    move/from16 v21, v0

    aput-byte v21, p0, v20

    move/from16 v0, p4

    if-le v4, v0, :cond_f

    const/4 v13, 0x1

    :cond_f
    add-int/lit8 v3, v3, 0x1

    goto :goto_8

    :cond_10
    if-eqz v13, :cond_2

    const/4 v3, 0x1

    :goto_a
    move/from16 v0, p3

    if-ge v3, v0, :cond_2

    aget v20, v14, v3

    shr-int/lit8 v4, v20, 0x8

    shr-int/lit8 v20, v4, 0x1

    add-int/lit8 v4, v20, 0x1

    shl-int/lit8 v20, v4, 0x8

    aput v20, v14, v3

    add-int/lit8 v3, v3, 0x1

    goto :goto_a

    :cond_11
    return-void
.end method

.method protected static hbMakeCodeLengths([C[III)V
    .locals 23
    .param p0    # [C
    .param p1    # [I
    .param p2    # I
    .param p3    # I

    const/16 v20, 0x204

    move/from16 v0, v20

    new-array v2, v0, [I

    const/16 v20, 0x204

    move/from16 v0, v20

    new-array v14, v0, [I

    const/16 v20, 0x204

    move/from16 v0, v20

    new-array v10, v0, [I

    move/from16 v3, p2

    :goto_0
    add-int/lit8 v3, v3, -0x1

    if-ltz v3, :cond_1

    add-int/lit8 v21, v3, 0x1

    aget v20, p1, v3

    if-nez v20, :cond_0

    const/16 v20, 0x1

    :goto_1
    shl-int/lit8 v20, v20, 0x8

    aput v20, v14, v21

    goto :goto_0

    :cond_0
    aget v20, p1, v3

    goto :goto_1

    :cond_1
    const/4 v13, 0x1

    :cond_2
    if-eqz v13, :cond_11

    const/4 v13, 0x0

    move/from16 v9, p2

    const/4 v8, 0x0

    const/16 v20, 0x0

    const/16 v21, 0x0

    aput v21, v2, v20

    const/16 v20, 0x0

    const/16 v21, 0x0

    aput v21, v14, v20

    const/16 v20, 0x0

    const/16 v21, -0x2

    aput v21, v10, v20

    const/4 v3, 0x1

    :goto_2
    move/from16 v0, p2

    if-gt v3, v0, :cond_5

    const/16 v20, -0x1

    aput v20, v10, v3

    add-int/lit8 v8, v8, 0x1

    aput v3, v2, v8

    move/from16 v19, v8

    aget v12, v2, v19

    :goto_3
    aget v20, v14, v12

    shr-int/lit8 v21, v19, 0x1

    aget v21, v2, v21

    aget v21, v14, v21

    move/from16 v0, v20

    move/from16 v1, v21

    if-ge v0, v1, :cond_3

    shr-int/lit8 v20, v19, 0x1

    aget v20, v2, v20

    aput v20, v2, v19

    shr-int/lit8 v19, v19, 0x1

    goto :goto_3

    :cond_3
    aput v12, v2, v19

    add-int/lit8 v3, v3, 0x1

    goto :goto_2

    :cond_4
    aput v12, v2, v19

    :cond_5
    const/16 v20, 0x1

    move/from16 v0, v20

    if-le v8, v0, :cond_d

    const/16 v20, 0x1

    aget v6, v2, v20

    const/16 v20, 0x1

    aget v21, v2, v8

    aput v21, v2, v20

    add-int/lit8 v8, v8, -0x1

    const/16 v18, 0x0

    const/16 v19, 0x1

    const/16 v20, 0x1

    aget v12, v2, v20

    :goto_4
    shl-int/lit8 v18, v19, 0x1

    move/from16 v0, v18

    if-le v0, v8, :cond_8

    :cond_6
    aput v12, v2, v19

    const/16 v20, 0x1

    aget v7, v2, v20

    const/16 v20, 0x1

    aget v21, v2, v8

    aput v21, v2, v20

    add-int/lit8 v8, v8, -0x1

    const/16 v18, 0x0

    const/16 v19, 0x1

    const/16 v20, 0x1

    aget v12, v2, v20

    :goto_5
    shl-int/lit8 v18, v19, 0x1

    move/from16 v0, v18

    if-le v0, v8, :cond_a

    :cond_7
    aput v12, v2, v19

    add-int/lit8 v9, v9, 0x1

    aput v9, v10, v7

    aput v9, v10, v6

    aget v15, v14, v6

    aget v16, v14, v7

    and-int/lit16 v0, v15, -0x100

    move/from16 v20, v0

    move/from16 v0, v16

    and-int/lit16 v0, v0, -0x100

    move/from16 v21, v0

    add-int v21, v21, v20

    and-int/lit16 v0, v15, 0xff

    move/from16 v20, v0

    move/from16 v0, v16

    and-int/lit16 v0, v0, 0xff

    move/from16 v22, v0

    move/from16 v0, v20

    move/from16 v1, v22

    if-le v0, v1, :cond_c

    and-int/lit16 v0, v15, 0xff

    move/from16 v20, v0

    :goto_6
    add-int/lit8 v20, v20, 0x1

    or-int v20, v20, v21

    aput v20, v14, v9

    const/16 v20, -0x1

    aput v20, v10, v9

    add-int/lit8 v8, v8, 0x1

    aput v9, v2, v8

    const/4 v12, 0x0

    move/from16 v19, v8

    aget v12, v2, v19

    aget v17, v14, v12

    :goto_7
    shr-int/lit8 v20, v19, 0x1

    aget v20, v2, v20

    aget v20, v14, v20

    move/from16 v0, v17

    move/from16 v1, v20

    if-ge v0, v1, :cond_4

    shr-int/lit8 v20, v19, 0x1

    aget v20, v2, v20

    aput v20, v2, v19

    shr-int/lit8 v19, v19, 0x1

    goto :goto_7

    :cond_8
    move/from16 v0, v18

    if-ge v0, v8, :cond_9

    add-int/lit8 v20, v18, 0x1

    aget v20, v2, v20

    aget v20, v14, v20

    aget v21, v2, v18

    aget v21, v14, v21

    move/from16 v0, v20

    move/from16 v1, v21

    if-ge v0, v1, :cond_9

    add-int/lit8 v18, v18, 0x1

    :cond_9
    aget v20, v14, v12

    aget v21, v2, v18

    aget v21, v14, v21

    move/from16 v0, v20

    move/from16 v1, v21

    if-lt v0, v1, :cond_6

    aget v20, v2, v18

    aput v20, v2, v19

    move/from16 v19, v18

    goto/16 :goto_4

    :cond_a
    move/from16 v0, v18

    if-ge v0, v8, :cond_b

    add-int/lit8 v20, v18, 0x1

    aget v20, v2, v20

    aget v20, v14, v20

    aget v21, v2, v18

    aget v21, v14, v21

    move/from16 v0, v20

    move/from16 v1, v21

    if-ge v0, v1, :cond_b

    add-int/lit8 v18, v18, 0x1

    :cond_b
    aget v20, v14, v12

    aget v21, v2, v18

    aget v21, v14, v21

    move/from16 v0, v20

    move/from16 v1, v21

    if-lt v0, v1, :cond_7

    aget v20, v2, v18

    aput v20, v2, v19

    move/from16 v19, v18

    goto/16 :goto_5

    :cond_c
    move/from16 v0, v16

    and-int/lit16 v0, v0, 0xff

    move/from16 v20, v0

    goto/16 :goto_6

    :cond_d
    const/4 v3, 0x1

    :goto_8
    move/from16 v0, p2

    if-gt v3, v0, :cond_10

    const/4 v4, 0x0

    move v5, v3

    :goto_9
    aget v11, v10, v5

    if-ltz v11, :cond_e

    move v5, v11

    add-int/lit8 v4, v4, 0x1

    goto :goto_9

    :cond_e
    add-int/lit8 v20, v3, -0x1

    int-to-char v0, v4

    move/from16 v21, v0

    aput-char v21, p0, v20

    move/from16 v0, p3

    if-le v4, v0, :cond_f

    const/4 v13, 0x1

    :cond_f
    add-int/lit8 v3, v3, 0x1

    goto :goto_8

    :cond_10
    if-eqz v13, :cond_2

    const/4 v3, 0x1

    :goto_a
    move/from16 v0, p2

    if-ge v3, v0, :cond_2

    aget v20, v14, v3

    shr-int/lit8 v4, v20, 0x8

    shr-int/lit8 v20, v4, 0x1

    add-int/lit8 v4, v20, 0x1

    shl-int/lit8 v20, v4, 0x8

    aput v20, v14, v3

    add-int/lit8 v3, v3, 0x1

    goto :goto_a

    :cond_11
    return-void
.end method

.method private init()V
    .locals 2
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    new-instance v0, Lorg/apache/tools/bzip2/CBZip2OutputStream$Data;

    iget v1, p0, Lorg/apache/tools/bzip2/CBZip2OutputStream;->blockSize100k:I

    invoke-direct {v0, v1}, Lorg/apache/tools/bzip2/CBZip2OutputStream$Data;-><init>(I)V

    iput-object v0, p0, Lorg/apache/tools/bzip2/CBZip2OutputStream;->data:Lorg/apache/tools/bzip2/CBZip2OutputStream$Data;

    const/16 v0, 0x68

    invoke-direct {p0, v0}, Lorg/apache/tools/bzip2/CBZip2OutputStream;->bsPutUByte(I)V

    iget v0, p0, Lorg/apache/tools/bzip2/CBZip2OutputStream;->blockSize100k:I

    add-int/lit8 v0, v0, 0x30

    invoke-direct {p0, v0}, Lorg/apache/tools/bzip2/CBZip2OutputStream;->bsPutUByte(I)V

    const/4 v0, 0x0

    iput v0, p0, Lorg/apache/tools/bzip2/CBZip2OutputStream;->combinedCRC:I

    invoke-direct {p0}, Lorg/apache/tools/bzip2/CBZip2OutputStream;->initBlock()V

    return-void
.end method

.method private initBlock()V
    .locals 4

    iget-object v2, p0, Lorg/apache/tools/bzip2/CBZip2OutputStream;->crc:Lorg/apache/tools/bzip2/CRC;

    invoke-virtual {v2}, Lorg/apache/tools/bzip2/CRC;->initialiseCRC()V

    const/4 v2, -0x1

    iput v2, p0, Lorg/apache/tools/bzip2/CBZip2OutputStream;->last:I

    iget-object v2, p0, Lorg/apache/tools/bzip2/CBZip2OutputStream;->data:Lorg/apache/tools/bzip2/CBZip2OutputStream$Data;

    iget-object v1, v2, Lorg/apache/tools/bzip2/CBZip2OutputStream$Data;->inUse:[Z

    const/16 v0, 0x100

    :goto_0
    add-int/lit8 v0, v0, -0x1

    if-ltz v0, :cond_0

    const/4 v2, 0x0

    aput-boolean v2, v1, v0

    goto :goto_0

    :cond_0
    iget v2, p0, Lorg/apache/tools/bzip2/CBZip2OutputStream;->blockSize100k:I

    const v3, 0x186a0

    mul-int/2addr v2, v3

    add-int/lit8 v2, v2, -0x14

    iput v2, p0, Lorg/apache/tools/bzip2/CBZip2OutputStream;->allowableBlockSize:I

    return-void
.end method

.method private mainQSort3(Lorg/apache/tools/bzip2/CBZip2OutputStream$Data;III)V
    .locals 27
    .param p1    # Lorg/apache/tools/bzip2/CBZip2OutputStream$Data;
    .param p2    # I
    .param p3    # I
    .param p4    # I

    move-object/from16 v0, p1

    iget-object v0, v0, Lorg/apache/tools/bzip2/CBZip2OutputStream$Data;->stack_ll:[I

    move-object/from16 v18, v0

    move-object/from16 v0, p1

    iget-object v0, v0, Lorg/apache/tools/bzip2/CBZip2OutputStream$Data;->stack_hh:[I

    move-object/from16 v17, v0

    move-object/from16 v0, p1

    iget-object v0, v0, Lorg/apache/tools/bzip2/CBZip2OutputStream$Data;->stack_dd:[I

    move-object/from16 v16, v0

    move-object/from16 v0, p1

    iget-object v5, v0, Lorg/apache/tools/bzip2/CBZip2OutputStream$Data;->fmap:[I

    move-object/from16 v0, p1

    iget-object v2, v0, Lorg/apache/tools/bzip2/CBZip2OutputStream$Data;->block:[B

    const/16 v24, 0x0

    aput p2, v18, v24

    const/16 v24, 0x0

    aput p3, v17, v24

    const/16 v24, 0x0

    aput p4, v16, v24

    const/4 v15, 0x1

    :cond_0
    :goto_0
    add-int/lit8 v15, v15, -0x1

    if-ltz v15, :cond_2

    aget v9, v18, v15

    aget v8, v17, v15

    aget v3, v16, v15

    sub-int v24, v8, v9

    const/16 v25, 0x14

    move/from16 v0, v24

    move/from16 v1, v25

    if-lt v0, v1, :cond_1

    const/16 v24, 0xa

    move/from16 v0, v24

    if-le v3, v0, :cond_3

    :cond_1
    move-object/from16 v0, p0

    move-object/from16 v1, p1

    invoke-direct {v0, v1, v9, v8, v3}, Lorg/apache/tools/bzip2/CBZip2OutputStream;->mainSimpleSort(Lorg/apache/tools/bzip2/CBZip2OutputStream$Data;III)Z

    move-result v24

    if-eqz v24, :cond_0

    :cond_2
    return-void

    :cond_3
    add-int/lit8 v4, v3, 0x1

    aget v24, v5, v9

    add-int v24, v24, v4

    aget-byte v24, v2, v24

    aget v25, v5, v8

    add-int v25, v25, v4

    aget-byte v25, v2, v25

    add-int v26, v9, v8

    shr-int/lit8 v26, v26, 0x1

    aget v26, v5, v26

    add-int v26, v26, v4

    aget-byte v26, v2, v26

    invoke-static/range {v24 .. v26}, Lorg/apache/tools/bzip2/CBZip2OutputStream;->med3(BBB)B

    move-result v24

    move/from16 v0, v24

    and-int/lit16 v13, v0, 0xff

    move/from16 v22, v9

    move/from16 v20, v8

    move v10, v9

    move v6, v8

    move v11, v10

    move/from16 v23, v22

    :goto_1
    move/from16 v0, v23

    move/from16 v1, v20

    if-gt v0, v1, :cond_b

    aget v24, v5, v23

    add-int v24, v24, v4

    aget-byte v24, v2, v24

    move/from16 v0, v24

    and-int/lit16 v0, v0, 0xff

    move/from16 v24, v0

    sub-int v14, v24, v13

    if-nez v14, :cond_4

    aget v19, v5, v23

    add-int/lit8 v22, v23, 0x1

    aget v24, v5, v11

    aput v24, v5, v23

    add-int/lit8 v10, v11, 0x1

    aput v19, v5, v11

    :goto_2
    move v11, v10

    move/from16 v23, v22

    goto :goto_1

    :cond_4
    if-gez v14, :cond_b

    add-int/lit8 v22, v23, 0x1

    move v10, v11

    goto :goto_2

    :goto_3
    move/from16 v0, v23

    move/from16 v1, v21

    if-gt v0, v1, :cond_6

    aget v24, v5, v21

    add-int v24, v24, v4

    aget-byte v24, v2, v24

    move/from16 v0, v24

    and-int/lit16 v0, v0, 0xff

    move/from16 v24, v0

    sub-int v14, v24, v13

    if-nez v14, :cond_5

    aget v19, v5, v21

    add-int/lit8 v20, v21, -0x1

    aget v24, v5, v7

    aput v24, v5, v21

    add-int/lit8 v6, v7, -0x1

    aput v19, v5, v7

    :goto_4
    move v7, v6

    move/from16 v21, v20

    goto :goto_3

    :cond_5
    if-lez v14, :cond_6

    add-int/lit8 v20, v21, -0x1

    move v6, v7

    goto :goto_4

    :cond_6
    move/from16 v0, v23

    move/from16 v1, v21

    if-gt v0, v1, :cond_7

    aget v19, v5, v23

    add-int/lit8 v22, v23, 0x1

    aget v24, v5, v21

    aput v24, v5, v23

    add-int/lit8 v20, v21, -0x1

    aput v19, v5, v21

    move v6, v7

    move/from16 v23, v22

    goto :goto_1

    :cond_7
    if-ge v7, v11, :cond_8

    aput v9, v18, v15

    aput v8, v17, v15

    aput v4, v16, v15

    add-int/lit8 v15, v15, 0x1

    goto/16 :goto_0

    :cond_8
    sub-int v24, v11, v9

    sub-int v25, v23, v11

    move/from16 v0, v24

    move/from16 v1, v25

    if-ge v0, v1, :cond_9

    sub-int v14, v11, v9

    :goto_5
    sub-int v24, v23, v14

    move/from16 v0, v24

    invoke-static {v5, v9, v0, v14}, Lorg/apache/tools/bzip2/CBZip2OutputStream;->vswap([IIII)V

    sub-int v24, v8, v7

    sub-int v25, v7, v21

    move/from16 v0, v24

    move/from16 v1, v25

    if-ge v0, v1, :cond_a

    sub-int v12, v8, v7

    :goto_6
    sub-int v24, v8, v12

    add-int/lit8 v24, v24, 0x1

    move/from16 v0, v23

    move/from16 v1, v24

    invoke-static {v5, v0, v1, v12}, Lorg/apache/tools/bzip2/CBZip2OutputStream;->vswap([IIII)V

    add-int v24, v9, v23

    sub-int v24, v24, v11

    add-int/lit8 v14, v24, -0x1

    sub-int v24, v7, v21

    sub-int v24, v8, v24

    add-int/lit8 v12, v24, 0x1

    aput v9, v18, v15

    aput v14, v17, v15

    aput v3, v16, v15

    add-int/lit8 v15, v15, 0x1

    add-int/lit8 v24, v14, 0x1

    aput v24, v18, v15

    add-int/lit8 v24, v12, -0x1

    aput v24, v17, v15

    aput v4, v16, v15

    add-int/lit8 v15, v15, 0x1

    aput v12, v18, v15

    aput v8, v17, v15

    aput v3, v16, v15

    add-int/lit8 v15, v15, 0x1

    goto/16 :goto_0

    :cond_9
    sub-int v14, v23, v11

    goto :goto_5

    :cond_a
    sub-int v12, v7, v21

    goto :goto_6

    :cond_b
    move v7, v6

    move/from16 v21, v20

    goto/16 :goto_3
.end method

.method private mainSimpleSort(Lorg/apache/tools/bzip2/CBZip2OutputStream$Data;III)Z
    .locals 26
    .param p1    # Lorg/apache/tools/bzip2/CBZip2OutputStream$Data;
    .param p2    # I
    .param p3    # I
    .param p4    # I

    sub-int v24, p3, p2

    add-int/lit8 v3, v24, 0x1

    const/16 v24, 0x2

    move/from16 v0, v24

    if-ge v3, v0, :cond_1

    move-object/from16 v0, p0

    iget-boolean v0, v0, Lorg/apache/tools/bzip2/CBZip2OutputStream;->firstAttempt:Z

    move/from16 v24, v0

    if-eqz v24, :cond_0

    move-object/from16 v0, p0

    iget v0, v0, Lorg/apache/tools/bzip2/CBZip2OutputStream;->workDone:I

    move/from16 v24, v0

    move-object/from16 v0, p0

    iget v0, v0, Lorg/apache/tools/bzip2/CBZip2OutputStream;->workLimit:I

    move/from16 v25, v0

    move/from16 v0, v24

    move/from16 v1, v25

    if-le v0, v1, :cond_0

    const/16 v24, 0x1

    :goto_0
    return v24

    :cond_0
    const/16 v24, 0x0

    goto :goto_0

    :cond_1
    const/4 v8, 0x0

    :goto_1
    sget-object v24, Lorg/apache/tools/bzip2/CBZip2OutputStream;->INCS:[I

    aget v24, v24, v8

    move/from16 v0, v24

    if-ge v0, v3, :cond_2

    add-int/lit8 v8, v8, 0x1

    goto :goto_1

    :cond_2
    move-object/from16 v0, p1

    iget-object v6, v0, Lorg/apache/tools/bzip2/CBZip2OutputStream$Data;->fmap:[I

    move-object/from16 v0, p1

    iget-object v0, v0, Lorg/apache/tools/bzip2/CBZip2OutputStream$Data;->quadrant:[C

    move-object/from16 v18, v0

    move-object/from16 v0, p1

    iget-object v4, v0, Lorg/apache/tools/bzip2/CBZip2OutputStream$Data;->block:[B

    move-object/from16 v0, p0

    iget v15, v0, Lorg/apache/tools/bzip2/CBZip2OutputStream;->last:I

    add-int/lit8 v14, v15, 0x1

    move-object/from16 v0, p0

    iget-boolean v5, v0, Lorg/apache/tools/bzip2/CBZip2OutputStream;->firstAttempt:Z

    move-object/from16 v0, p0

    iget v0, v0, Lorg/apache/tools/bzip2/CBZip2OutputStream;->workLimit:I

    move/from16 v22, v0

    move-object/from16 v0, p0

    iget v0, v0, Lorg/apache/tools/bzip2/CBZip2OutputStream;->workDone:I

    move/from16 v21, v0

    :cond_3
    add-int/lit8 v8, v8, -0x1

    if-ltz v8, :cond_19

    sget-object v24, Lorg/apache/tools/bzip2/CBZip2OutputStream;->INCS:[I

    aget v7, v24, v8

    add-int v24, p2, v7

    add-int/lit8 v16, v24, -0x1

    add-int v9, p2, v7

    :cond_4
    move/from16 v0, p3

    if-gt v9, v0, :cond_3

    const/4 v13, 0x3

    :goto_2
    move/from16 v0, p3

    if-gt v9, v0, :cond_18

    add-int/lit8 v13, v13, -0x1

    if-ltz v13, :cond_18

    aget v19, v6, v9

    add-int v20, v19, p4

    move v12, v9

    const/16 v17, 0x0

    const/4 v2, 0x0

    :goto_3
    if-eqz v17, :cond_6

    aput v2, v6, v12

    sub-int/2addr v12, v7

    move/from16 v0, v16

    if-gt v12, v0, :cond_7

    :cond_5
    aput v19, v6, v12

    add-int/lit8 v9, v9, 0x1

    goto :goto_2

    :cond_6
    const/16 v17, 0x1

    :cond_7
    sub-int v24, v12, v7

    aget v2, v6, v24

    add-int v10, v2, p4

    move/from16 v11, v20

    add-int/lit8 v24, v10, 0x1

    aget-byte v24, v4, v24

    add-int/lit8 v25, v11, 0x1

    aget-byte v25, v4, v25

    move/from16 v0, v24

    move/from16 v1, v25

    if-ne v0, v1, :cond_17

    add-int/lit8 v24, v10, 0x2

    aget-byte v24, v4, v24

    add-int/lit8 v25, v11, 0x2

    aget-byte v25, v4, v25

    move/from16 v0, v24

    move/from16 v1, v25

    if-ne v0, v1, :cond_16

    add-int/lit8 v24, v10, 0x3

    aget-byte v24, v4, v24

    add-int/lit8 v25, v11, 0x3

    aget-byte v25, v4, v25

    move/from16 v0, v24

    move/from16 v1, v25

    if-ne v0, v1, :cond_15

    add-int/lit8 v24, v10, 0x4

    aget-byte v24, v4, v24

    add-int/lit8 v25, v11, 0x4

    aget-byte v25, v4, v25

    move/from16 v0, v24

    move/from16 v1, v25

    if-ne v0, v1, :cond_14

    add-int/lit8 v24, v10, 0x5

    aget-byte v24, v4, v24

    add-int/lit8 v25, v11, 0x5

    aget-byte v25, v4, v25

    move/from16 v0, v24

    move/from16 v1, v25

    if-ne v0, v1, :cond_13

    add-int/lit8 v10, v10, 0x6

    aget-byte v24, v4, v10

    add-int/lit8 v11, v11, 0x6

    aget-byte v25, v4, v11

    move/from16 v0, v24

    move/from16 v1, v25

    if-ne v0, v1, :cond_12

    move/from16 v23, v15

    :goto_4
    if-lez v23, :cond_5

    add-int/lit8 v23, v23, -0x4

    add-int/lit8 v24, v10, 0x1

    aget-byte v24, v4, v24

    add-int/lit8 v25, v11, 0x1

    aget-byte v25, v4, v25

    move/from16 v0, v24

    move/from16 v1, v25

    if-ne v0, v1, :cond_11

    aget-char v24, v18, v10

    aget-char v25, v18, v11

    move/from16 v0, v24

    move/from16 v1, v25

    if-ne v0, v1, :cond_10

    add-int/lit8 v24, v10, 0x2

    aget-byte v24, v4, v24

    add-int/lit8 v25, v11, 0x2

    aget-byte v25, v4, v25

    move/from16 v0, v24

    move/from16 v1, v25

    if-ne v0, v1, :cond_f

    add-int/lit8 v24, v10, 0x1

    aget-char v24, v18, v24

    add-int/lit8 v25, v11, 0x1

    aget-char v25, v18, v25

    move/from16 v0, v24

    move/from16 v1, v25

    if-ne v0, v1, :cond_e

    add-int/lit8 v24, v10, 0x3

    aget-byte v24, v4, v24

    add-int/lit8 v25, v11, 0x3

    aget-byte v25, v4, v25

    move/from16 v0, v24

    move/from16 v1, v25

    if-ne v0, v1, :cond_d

    add-int/lit8 v24, v10, 0x2

    aget-char v24, v18, v24

    add-int/lit8 v25, v11, 0x2

    aget-char v25, v18, v25

    move/from16 v0, v24

    move/from16 v1, v25

    if-ne v0, v1, :cond_c

    add-int/lit8 v24, v10, 0x4

    aget-byte v24, v4, v24

    add-int/lit8 v25, v11, 0x4

    aget-byte v25, v4, v25

    move/from16 v0, v24

    move/from16 v1, v25

    if-ne v0, v1, :cond_b

    add-int/lit8 v24, v10, 0x3

    aget-char v24, v18, v24

    add-int/lit8 v25, v11, 0x3

    aget-char v25, v18, v25

    move/from16 v0, v24

    move/from16 v1, v25

    if-ne v0, v1, :cond_a

    add-int/lit8 v10, v10, 0x4

    if-lt v10, v14, :cond_8

    sub-int/2addr v10, v14

    :cond_8
    add-int/lit8 v11, v11, 0x4

    if-lt v11, v14, :cond_9

    sub-int/2addr v11, v14

    :cond_9
    add-int/lit8 v21, v21, 0x1

    goto :goto_4

    :cond_a
    add-int/lit8 v24, v10, 0x3

    aget-char v24, v18, v24

    add-int/lit8 v25, v11, 0x3

    aget-char v25, v18, v25

    move/from16 v0, v24

    move/from16 v1, v25

    if-le v0, v1, :cond_5

    goto/16 :goto_3

    :cond_b
    add-int/lit8 v24, v10, 0x4

    aget-byte v24, v4, v24

    move/from16 v0, v24

    and-int/lit16 v0, v0, 0xff

    move/from16 v24, v0

    add-int/lit8 v25, v11, 0x4

    aget-byte v25, v4, v25

    move/from16 v0, v25

    and-int/lit16 v0, v0, 0xff

    move/from16 v25, v0

    move/from16 v0, v24

    move/from16 v1, v25

    if-le v0, v1, :cond_5

    goto/16 :goto_3

    :cond_c
    add-int/lit8 v24, v10, 0x2

    aget-char v24, v18, v24

    add-int/lit8 v25, v11, 0x2

    aget-char v25, v18, v25

    move/from16 v0, v24

    move/from16 v1, v25

    if-le v0, v1, :cond_5

    goto/16 :goto_3

    :cond_d
    add-int/lit8 v24, v10, 0x3

    aget-byte v24, v4, v24

    move/from16 v0, v24

    and-int/lit16 v0, v0, 0xff

    move/from16 v24, v0

    add-int/lit8 v25, v11, 0x3

    aget-byte v25, v4, v25

    move/from16 v0, v25

    and-int/lit16 v0, v0, 0xff

    move/from16 v25, v0

    move/from16 v0, v24

    move/from16 v1, v25

    if-le v0, v1, :cond_5

    goto/16 :goto_3

    :cond_e
    add-int/lit8 v24, v10, 0x1

    aget-char v24, v18, v24

    add-int/lit8 v25, v11, 0x1

    aget-char v25, v18, v25

    move/from16 v0, v24

    move/from16 v1, v25

    if-le v0, v1, :cond_5

    goto/16 :goto_3

    :cond_f
    add-int/lit8 v24, v10, 0x2

    aget-byte v24, v4, v24

    move/from16 v0, v24

    and-int/lit16 v0, v0, 0xff

    move/from16 v24, v0

    add-int/lit8 v25, v11, 0x2

    aget-byte v25, v4, v25

    move/from16 v0, v25

    and-int/lit16 v0, v0, 0xff

    move/from16 v25, v0

    move/from16 v0, v24

    move/from16 v1, v25

    if-le v0, v1, :cond_5

    goto/16 :goto_3

    :cond_10
    aget-char v24, v18, v10

    aget-char v25, v18, v11

    move/from16 v0, v24

    move/from16 v1, v25

    if-le v0, v1, :cond_5

    goto/16 :goto_3

    :cond_11
    add-int/lit8 v24, v10, 0x1

    aget-byte v24, v4, v24

    move/from16 v0, v24

    and-int/lit16 v0, v0, 0xff

    move/from16 v24, v0

    add-int/lit8 v25, v11, 0x1

    aget-byte v25, v4, v25

    move/from16 v0, v25

    and-int/lit16 v0, v0, 0xff

    move/from16 v25, v0

    move/from16 v0, v24

    move/from16 v1, v25

    if-le v0, v1, :cond_5

    goto/16 :goto_3

    :cond_12
    aget-byte v24, v4, v10

    move/from16 v0, v24

    and-int/lit16 v0, v0, 0xff

    move/from16 v24, v0

    aget-byte v25, v4, v11

    move/from16 v0, v25

    and-int/lit16 v0, v0, 0xff

    move/from16 v25, v0

    move/from16 v0, v24

    move/from16 v1, v25

    if-le v0, v1, :cond_5

    goto/16 :goto_3

    :cond_13
    add-int/lit8 v24, v10, 0x5

    aget-byte v24, v4, v24

    move/from16 v0, v24

    and-int/lit16 v0, v0, 0xff

    move/from16 v24, v0

    add-int/lit8 v25, v11, 0x5

    aget-byte v25, v4, v25

    move/from16 v0, v25

    and-int/lit16 v0, v0, 0xff

    move/from16 v25, v0

    move/from16 v0, v24

    move/from16 v1, v25

    if-le v0, v1, :cond_5

    goto/16 :goto_3

    :cond_14
    add-int/lit8 v24, v10, 0x4

    aget-byte v24, v4, v24

    move/from16 v0, v24

    and-int/lit16 v0, v0, 0xff

    move/from16 v24, v0

    add-int/lit8 v25, v11, 0x4

    aget-byte v25, v4, v25

    move/from16 v0, v25

    and-int/lit16 v0, v0, 0xff

    move/from16 v25, v0

    move/from16 v0, v24

    move/from16 v1, v25

    if-le v0, v1, :cond_5

    goto/16 :goto_3

    :cond_15
    add-int/lit8 v24, v10, 0x3

    aget-byte v24, v4, v24

    move/from16 v0, v24

    and-int/lit16 v0, v0, 0xff

    move/from16 v24, v0

    add-int/lit8 v25, v11, 0x3

    aget-byte v25, v4, v25

    move/from16 v0, v25

    and-int/lit16 v0, v0, 0xff

    move/from16 v25, v0

    move/from16 v0, v24

    move/from16 v1, v25

    if-le v0, v1, :cond_5

    goto/16 :goto_3

    :cond_16
    add-int/lit8 v24, v10, 0x2

    aget-byte v24, v4, v24

    move/from16 v0, v24

    and-int/lit16 v0, v0, 0xff

    move/from16 v24, v0

    add-int/lit8 v25, v11, 0x2

    aget-byte v25, v4, v25

    move/from16 v0, v25

    and-int/lit16 v0, v0, 0xff

    move/from16 v25, v0

    move/from16 v0, v24

    move/from16 v1, v25

    if-le v0, v1, :cond_5

    goto/16 :goto_3

    :cond_17
    add-int/lit8 v24, v10, 0x1

    aget-byte v24, v4, v24

    move/from16 v0, v24

    and-int/lit16 v0, v0, 0xff

    move/from16 v24, v0

    add-int/lit8 v25, v11, 0x1

    aget-byte v25, v4, v25

    move/from16 v0, v25

    and-int/lit16 v0, v0, 0xff

    move/from16 v25, v0

    move/from16 v0, v24

    move/from16 v1, v25

    if-le v0, v1, :cond_5

    goto/16 :goto_3

    :cond_18
    if-eqz v5, :cond_4

    move/from16 v0, p3

    if-gt v9, v0, :cond_4

    move/from16 v0, v21

    move/from16 v1, v22

    if-le v0, v1, :cond_4

    :cond_19
    move/from16 v0, v21

    move-object/from16 v1, p0

    iput v0, v1, Lorg/apache/tools/bzip2/CBZip2OutputStream;->workDone:I

    if-eqz v5, :cond_1a

    move/from16 v0, v21

    move/from16 v1, v22

    if-le v0, v1, :cond_1a

    const/16 v24, 0x1

    goto/16 :goto_0

    :cond_1a
    const/16 v24, 0x0

    goto/16 :goto_0
.end method

.method private mainSort()V
    .locals 39

    move-object/from16 v0, p0

    iget-object v14, v0, Lorg/apache/tools/bzip2/CBZip2OutputStream;->data:Lorg/apache/tools/bzip2/CBZip2OutputStream$Data;

    iget-object v0, v14, Lorg/apache/tools/bzip2/CBZip2OutputStream$Data;->mainSort_runningOrder:[I

    move-object/from16 v30, v0

    iget-object v13, v14, Lorg/apache/tools/bzip2/CBZip2OutputStream$Data;->mainSort_copy:[I

    iget-object v9, v14, Lorg/apache/tools/bzip2/CBZip2OutputStream$Data;->mainSort_bigDone:[Z

    iget-object v0, v14, Lorg/apache/tools/bzip2/CBZip2OutputStream$Data;->ftab:[I

    move-object/from16 v18, v0

    iget-object v10, v14, Lorg/apache/tools/bzip2/CBZip2OutputStream$Data;->block:[B

    iget-object v0, v14, Lorg/apache/tools/bzip2/CBZip2OutputStream$Data;->fmap:[I

    move-object/from16 v16, v0

    iget-object v0, v14, Lorg/apache/tools/bzip2/CBZip2OutputStream$Data;->quadrant:[C

    move-object/from16 v28, v0

    move-object/from16 v0, p0

    iget v0, v0, Lorg/apache/tools/bzip2/CBZip2OutputStream;->last:I

    move/from16 v25, v0

    move-object/from16 v0, p0

    iget v0, v0, Lorg/apache/tools/bzip2/CBZip2OutputStream;->workLimit:I

    move/from16 v35, v0

    move-object/from16 v0, p0

    iget-boolean v15, v0, Lorg/apache/tools/bzip2/CBZip2OutputStream;->firstAttempt:Z

    const v23, 0x10001

    :goto_0
    add-int/lit8 v23, v23, -0x1

    if-ltz v23, :cond_0

    const/16 v36, 0x0

    aput v36, v18, v23

    goto :goto_0

    :cond_0
    const/16 v23, 0x0

    :goto_1
    const/16 v36, 0x14

    move/from16 v0, v23

    move/from16 v1, v36

    if-ge v0, v1, :cond_1

    add-int v36, v25, v23

    add-int/lit8 v36, v36, 0x2

    add-int/lit8 v37, v25, 0x1

    rem-int v37, v23, v37

    add-int/lit8 v37, v37, 0x1

    aget-byte v37, v10, v37

    aput-byte v37, v10, v36

    add-int/lit8 v23, v23, 0x1

    goto :goto_1

    :cond_1
    add-int/lit8 v23, v25, 0x14

    :goto_2
    add-int/lit8 v23, v23, -0x1

    if-ltz v23, :cond_2

    const/16 v36, 0x0

    aput-char v36, v28, v23

    goto :goto_2

    :cond_2
    const/16 v36, 0x0

    add-int/lit8 v37, v25, 0x1

    aget-byte v37, v10, v37

    aput-byte v37, v10, v36

    const/16 v36, 0x0

    aget-byte v36, v10, v36

    move/from16 v0, v36

    and-int/lit16 v11, v0, 0xff

    const/16 v23, 0x0

    :goto_3
    move/from16 v0, v23

    move/from16 v1, v25

    if-gt v0, v1, :cond_3

    add-int/lit8 v36, v23, 0x1

    aget-byte v36, v10, v36

    move/from16 v0, v36

    and-int/lit16 v12, v0, 0xff

    shl-int/lit8 v36, v11, 0x8

    add-int v36, v36, v12

    aget v37, v18, v36

    add-int/lit8 v37, v37, 0x1

    aput v37, v18, v36

    move v11, v12

    add-int/lit8 v23, v23, 0x1

    goto :goto_3

    :cond_3
    const/16 v23, 0x1

    :goto_4
    const/high16 v36, 0x10000

    move/from16 v0, v23

    move/from16 v1, v36

    if-gt v0, v1, :cond_4

    aget v36, v18, v23

    add-int/lit8 v37, v23, -0x1

    aget v37, v18, v37

    add-int v36, v36, v37

    aput v36, v18, v23

    add-int/lit8 v23, v23, 0x1

    goto :goto_4

    :cond_4
    const/16 v36, 0x1

    aget-byte v36, v10, v36

    move/from16 v0, v36

    and-int/lit16 v11, v0, 0xff

    const/16 v23, 0x0

    :goto_5
    move/from16 v0, v23

    move/from16 v1, v25

    if-ge v0, v1, :cond_5

    add-int/lit8 v36, v23, 0x2

    aget-byte v36, v10, v36

    move/from16 v0, v36

    and-int/lit16 v12, v0, 0xff

    shl-int/lit8 v36, v11, 0x8

    add-int v36, v36, v12

    aget v37, v18, v36

    add-int/lit8 v37, v37, -0x1

    aput v37, v18, v36

    aput v23, v16, v37

    move v11, v12

    add-int/lit8 v23, v23, 0x1

    goto :goto_5

    :cond_5
    add-int/lit8 v36, v25, 0x1

    aget-byte v36, v10, v36

    move/from16 v0, v36

    and-int/lit16 v0, v0, 0xff

    move/from16 v36, v0

    shl-int/lit8 v36, v36, 0x8

    const/16 v37, 0x1

    aget-byte v37, v10, v37

    move/from16 v0, v37

    and-int/lit16 v0, v0, 0xff

    move/from16 v37, v0

    add-int v36, v36, v37

    aget v37, v18, v36

    add-int/lit8 v37, v37, -0x1

    aput v37, v18, v36

    aput v25, v16, v37

    const/16 v23, 0x100

    :goto_6
    add-int/lit8 v23, v23, -0x1

    if-ltz v23, :cond_6

    const/16 v36, 0x0

    aput-boolean v36, v9, v23

    aput v23, v30, v23

    goto :goto_6

    :cond_6
    const/16 v20, 0x16c

    :cond_7
    const/16 v36, 0x1

    move/from16 v0, v20

    move/from16 v1, v36

    if-eq v0, v1, :cond_a

    div-int/lit8 v20, v20, 0x3

    move/from16 v23, v20

    :goto_7
    const/16 v36, 0xff

    move/from16 v0, v23

    move/from16 v1, v36

    if-gt v0, v1, :cond_7

    aget v34, v30, v23

    add-int/lit8 v36, v34, 0x1

    shl-int/lit8 v36, v36, 0x8

    aget v36, v18, v36

    shl-int/lit8 v37, v34, 0x8

    aget v37, v18, v37

    sub-int v4, v36, v37

    add-int/lit8 v6, v20, -0x1

    move/from16 v24, v23

    sub-int v36, v24, v20

    aget v29, v30, v36

    :goto_8
    add-int/lit8 v36, v29, 0x1

    shl-int/lit8 v36, v36, 0x8

    aget v36, v18, v36

    shl-int/lit8 v37, v29, 0x8

    aget v37, v18, v37

    sub-int v36, v36, v37

    move/from16 v0, v36

    if-le v0, v4, :cond_8

    aput v29, v30, v24

    sub-int v24, v24, v20

    move/from16 v0, v24

    if-gt v0, v6, :cond_9

    :cond_8
    aput v34, v30, v24

    add-int/lit8 v23, v23, 0x1

    goto :goto_7

    :cond_9
    sub-int v36, v24, v20

    aget v29, v30, v36

    goto :goto_8

    :cond_a
    const/16 v23, 0x0

    :goto_9
    const/16 v36, 0xff

    move/from16 v0, v23

    move/from16 v1, v36

    if-gt v0, v1, :cond_b

    aget v33, v30, v23

    const/16 v24, 0x0

    :goto_a
    const/16 v36, 0xff

    move/from16 v0, v24

    move/from16 v1, v36

    if-gt v0, v1, :cond_e

    shl-int/lit8 v36, v33, 0x8

    add-int v31, v36, v24

    aget v19, v18, v31

    const/high16 v36, 0x200000

    and-int v36, v36, v19

    const/high16 v37, 0x200000

    move/from16 v0, v36

    move/from16 v1, v37

    if-eq v0, v1, :cond_d

    const v36, -0x200001

    and-int v26, v19, v36

    add-int/lit8 v36, v31, 0x1

    aget v36, v18, v36

    const v37, -0x200001

    and-int v36, v36, v37

    add-int/lit8 v21, v36, -0x1

    move/from16 v0, v21

    move/from16 v1, v26

    if-le v0, v1, :cond_c

    const/16 v36, 0x2

    move-object/from16 v0, p0

    move/from16 v1, v26

    move/from16 v2, v21

    move/from16 v3, v36

    invoke-direct {v0, v14, v1, v2, v3}, Lorg/apache/tools/bzip2/CBZip2OutputStream;->mainQSort3(Lorg/apache/tools/bzip2/CBZip2OutputStream$Data;III)V

    if-eqz v15, :cond_c

    move-object/from16 v0, p0

    iget v0, v0, Lorg/apache/tools/bzip2/CBZip2OutputStream;->workDone:I

    move/from16 v36, v0

    move/from16 v0, v36

    move/from16 v1, v35

    if-le v0, v1, :cond_c

    :cond_b
    return-void

    :cond_c
    const/high16 v36, 0x200000

    or-int v36, v36, v19

    aput v36, v18, v31

    :cond_d
    add-int/lit8 v24, v24, 0x1

    goto :goto_a

    :cond_e
    const/16 v24, 0x0

    :goto_b
    const/16 v36, 0xff

    move/from16 v0, v24

    move/from16 v1, v36

    if-gt v0, v1, :cond_f

    shl-int/lit8 v36, v24, 0x8

    add-int v36, v36, v33

    aget v36, v18, v36

    const v37, -0x200001

    and-int v36, v36, v37

    aput v36, v13, v24

    add-int/lit8 v24, v24, 0x1

    goto :goto_b

    :cond_f
    shl-int/lit8 v36, v33, 0x8

    aget v36, v18, v36

    const v37, -0x200001

    and-int v24, v36, v37

    add-int/lit8 v36, v33, 0x1

    shl-int/lit8 v36, v36, 0x8

    aget v36, v18, v36

    const v37, -0x200001

    and-int v22, v36, v37

    :goto_c
    move/from16 v0, v24

    move/from16 v1, v22

    if-ge v0, v1, :cond_12

    aget v17, v16, v24

    aget-byte v36, v10, v17

    move/from16 v0, v36

    and-int/lit16 v11, v0, 0xff

    aget-boolean v36, v9, v11

    if-nez v36, :cond_10

    aget v37, v13, v11

    if-nez v17, :cond_11

    move/from16 v36, v25

    :goto_d
    aput v36, v16, v37

    aget v36, v13, v11

    add-int/lit8 v36, v36, 0x1

    aput v36, v13, v11

    :cond_10
    add-int/lit8 v24, v24, 0x1

    goto :goto_c

    :cond_11
    add-int/lit8 v36, v17, -0x1

    goto :goto_d

    :cond_12
    const/16 v24, 0x100

    :goto_e
    add-int/lit8 v24, v24, -0x1

    if-ltz v24, :cond_13

    shl-int/lit8 v36, v24, 0x8

    add-int v36, v36, v33

    aget v37, v18, v36

    const/high16 v38, 0x200000

    or-int v37, v37, v38

    aput v37, v18, v36

    goto :goto_e

    :cond_13
    const/16 v36, 0x1

    aput-boolean v36, v9, v33

    const/16 v36, 0xff

    move/from16 v0, v23

    move/from16 v1, v36

    if-ge v0, v1, :cond_16

    shl-int/lit8 v36, v33, 0x8

    aget v36, v18, v36

    const v37, -0x200001

    and-int v8, v36, v37

    add-int/lit8 v36, v33, 0x1

    shl-int/lit8 v36, v36, 0x8

    aget v36, v18, v36

    const v37, -0x200001

    and-int v36, v36, v37

    sub-int v7, v36, v8

    const/16 v32, 0x0

    :goto_f
    shr-int v36, v7, v32

    const v37, 0xfffe

    move/from16 v0, v36

    move/from16 v1, v37

    if-le v0, v1, :cond_14

    add-int/lit8 v32, v32, 0x1

    goto :goto_f

    :cond_14
    const/16 v24, 0x0

    :goto_10
    move/from16 v0, v24

    if-ge v0, v7, :cond_16

    add-int v36, v8, v24

    aget v5, v16, v36

    shr-int v36, v24, v32

    move/from16 v0, v36

    int-to-char v0, v0

    move/from16 v27, v0

    aput-char v27, v28, v5

    const/16 v36, 0x14

    move/from16 v0, v36

    if-ge v5, v0, :cond_15

    add-int v36, v5, v25

    add-int/lit8 v36, v36, 0x1

    aput-char v27, v28, v36

    :cond_15
    add-int/lit8 v24, v24, 0x1

    goto :goto_10

    :cond_16
    add-int/lit8 v23, v23, 0x1

    goto/16 :goto_9
.end method

.method private static med3(BBB)B
    .locals 0
    .param p0    # B
    .param p1    # B
    .param p2    # B

    if-ge p0, p1, :cond_3

    if-ge p1, p2, :cond_1

    :cond_0
    :goto_0
    return p1

    :cond_1
    if-ge p0, p2, :cond_2

    move p1, p2

    goto :goto_0

    :cond_2
    move p1, p0

    goto :goto_0

    :cond_3
    if-gt p1, p2, :cond_0

    if-le p0, p2, :cond_4

    move p1, p2

    goto :goto_0

    :cond_4
    move p1, p0

    goto :goto_0
.end method

.method private moveToFrontCodeAndSend()V
    .locals 2
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    const/16 v0, 0x18

    iget v1, p0, Lorg/apache/tools/bzip2/CBZip2OutputStream;->origPtr:I

    invoke-direct {p0, v0, v1}, Lorg/apache/tools/bzip2/CBZip2OutputStream;->bsW(II)V

    invoke-direct {p0}, Lorg/apache/tools/bzip2/CBZip2OutputStream;->generateMTFValues()V

    invoke-direct {p0}, Lorg/apache/tools/bzip2/CBZip2OutputStream;->sendMTFValues()V

    return-void
.end method

.method private randomiseBlock()V
    .locals 11

    const/4 v9, 0x0

    const/4 v8, 0x1

    iget-object v7, p0, Lorg/apache/tools/bzip2/CBZip2OutputStream;->data:Lorg/apache/tools/bzip2/CBZip2OutputStream$Data;

    iget-object v2, v7, Lorg/apache/tools/bzip2/CBZip2OutputStream$Data;->inUse:[Z

    iget-object v7, p0, Lorg/apache/tools/bzip2/CBZip2OutputStream;->data:Lorg/apache/tools/bzip2/CBZip2OutputStream$Data;

    iget-object v0, v7, Lorg/apache/tools/bzip2/CBZip2OutputStream$Data;->block:[B

    iget v4, p0, Lorg/apache/tools/bzip2/CBZip2OutputStream;->last:I

    const/16 v1, 0x100

    :goto_0
    add-int/lit8 v1, v1, -0x1

    if-ltz v1, :cond_0

    aput-boolean v9, v2, v1

    goto :goto_0

    :cond_0
    const/4 v5, 0x0

    const/4 v6, 0x0

    const/4 v1, 0x0

    const/4 v3, 0x1

    :goto_1
    if-gt v1, v4, :cond_3

    if-nez v5, :cond_1

    sget-object v7, Lorg/apache/tools/bzip2/BZip2Constants;->rNums:[I

    aget v7, v7, v6

    int-to-char v5, v7

    add-int/lit8 v6, v6, 0x1

    const/16 v7, 0x200

    if-ne v6, v7, :cond_1

    const/4 v6, 0x0

    :cond_1
    add-int/lit8 v5, v5, -0x1

    aget-byte v10, v0, v3

    if-ne v5, v8, :cond_2

    move v7, v8

    :goto_2
    xor-int/2addr v7, v10

    int-to-byte v7, v7

    aput-byte v7, v0, v3

    aget-byte v7, v0, v3

    and-int/lit16 v7, v7, 0xff

    aput-boolean v8, v2, v7

    move v1, v3

    add-int/lit8 v3, v3, 0x1

    goto :goto_1

    :cond_2
    move v7, v9

    goto :goto_2

    :cond_3
    iput-boolean v8, p0, Lorg/apache/tools/bzip2/CBZip2OutputStream;->blockRandomised:Z

    return-void
.end method

.method private sendMTFValues()V
    .locals 9
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    iget-object v7, p0, Lorg/apache/tools/bzip2/CBZip2OutputStream;->data:Lorg/apache/tools/bzip2/CBZip2OutputStream$Data;

    iget-object v1, v7, Lorg/apache/tools/bzip2/CBZip2OutputStream$Data;->sendMTFValues_len:[[B

    iget v7, p0, Lorg/apache/tools/bzip2/CBZip2OutputStream;->nInUse:I

    add-int/lit8 v0, v7, 0x2

    const/4 v5, 0x6

    :cond_0
    add-int/lit8 v5, v5, -0x1

    if-ltz v5, :cond_1

    aget-object v2, v1, v5

    move v6, v0

    :goto_0
    add-int/lit8 v6, v6, -0x1

    if-ltz v6, :cond_0

    const/16 v7, 0xf

    aput-byte v7, v2, v6

    goto :goto_0

    :cond_1
    iget v7, p0, Lorg/apache/tools/bzip2/CBZip2OutputStream;->nMTF:I

    const/16 v8, 0xc8

    if-ge v7, v8, :cond_2

    const/4 v3, 0x2

    :goto_1
    invoke-direct {p0, v3, v0}, Lorg/apache/tools/bzip2/CBZip2OutputStream;->sendMTFValues0(II)V

    invoke-direct {p0, v3, v0}, Lorg/apache/tools/bzip2/CBZip2OutputStream;->sendMTFValues1(II)I

    move-result v4

    invoke-direct {p0, v3, v4}, Lorg/apache/tools/bzip2/CBZip2OutputStream;->sendMTFValues2(II)V

    invoke-direct {p0, v3, v0}, Lorg/apache/tools/bzip2/CBZip2OutputStream;->sendMTFValues3(II)V

    invoke-direct {p0}, Lorg/apache/tools/bzip2/CBZip2OutputStream;->sendMTFValues4()V

    invoke-direct {p0, v3, v4}, Lorg/apache/tools/bzip2/CBZip2OutputStream;->sendMTFValues5(II)V

    invoke-direct {p0, v3, v0}, Lorg/apache/tools/bzip2/CBZip2OutputStream;->sendMTFValues6(II)V

    invoke-direct {p0, v4}, Lorg/apache/tools/bzip2/CBZip2OutputStream;->sendMTFValues7(I)V

    return-void

    :cond_2
    iget v7, p0, Lorg/apache/tools/bzip2/CBZip2OutputStream;->nMTF:I

    const/16 v8, 0x258

    if-ge v7, v8, :cond_3

    const/4 v3, 0x3

    goto :goto_1

    :cond_3
    iget v7, p0, Lorg/apache/tools/bzip2/CBZip2OutputStream;->nMTF:I

    const/16 v8, 0x4b0

    if-ge v7, v8, :cond_4

    const/4 v3, 0x4

    goto :goto_1

    :cond_4
    iget v7, p0, Lorg/apache/tools/bzip2/CBZip2OutputStream;->nMTF:I

    const/16 v8, 0x960

    if-ge v7, v8, :cond_5

    const/4 v3, 0x5

    goto :goto_1

    :cond_5
    const/4 v3, 0x6

    goto :goto_1
.end method

.method private sendMTFValues0(II)V
    .locals 13
    .param p1    # I
    .param p2    # I

    iget-object v12, p0, Lorg/apache/tools/bzip2/CBZip2OutputStream;->data:Lorg/apache/tools/bzip2/CBZip2OutputStream$Data;

    iget-object v5, v12, Lorg/apache/tools/bzip2/CBZip2OutputStream$Data;->sendMTFValues_len:[[B

    iget-object v12, p0, Lorg/apache/tools/bzip2/CBZip2OutputStream;->data:Lorg/apache/tools/bzip2/CBZip2OutputStream$Data;

    iget-object v7, v12, Lorg/apache/tools/bzip2/CBZip2OutputStream$Data;->mtfFreq:[I

    iget v9, p0, Lorg/apache/tools/bzip2/CBZip2OutputStream;->nMTF:I

    const/4 v4, 0x0

    move v8, p1

    :goto_0
    if-lez v8, :cond_3

    div-int v10, v9, v8

    add-int/lit8 v2, v4, -0x1

    const/4 v1, 0x0

    add-int/lit8 v0, p2, -0x1

    move v3, v2

    :goto_1
    if-ge v1, v10, :cond_0

    if-ge v3, v0, :cond_0

    add-int/lit8 v2, v3, 0x1

    aget v12, v7, v2

    add-int/2addr v1, v12

    move v3, v2

    goto :goto_1

    :cond_0
    if-le v3, v4, :cond_4

    if-eq v8, p1, :cond_4

    const/4 v12, 0x1

    if-eq v8, v12, :cond_4

    sub-int v12, p1, v8

    and-int/lit8 v12, v12, 0x1

    if-eqz v12, :cond_4

    add-int/lit8 v2, v3, -0x1

    aget v12, v7, v3

    sub-int/2addr v1, v12

    :goto_2
    add-int/lit8 v12, v8, -0x1

    aget-object v6, v5, v12

    move v11, p2

    :goto_3
    add-int/lit8 v11, v11, -0x1

    if-ltz v11, :cond_2

    if-lt v11, v4, :cond_1

    if-gt v11, v2, :cond_1

    const/4 v12, 0x0

    aput-byte v12, v6, v11

    goto :goto_3

    :cond_1
    const/16 v12, 0xf

    aput-byte v12, v6, v11

    goto :goto_3

    :cond_2
    add-int/lit8 v4, v2, 0x1

    sub-int/2addr v9, v1

    add-int/lit8 v8, v8, -0x1

    goto :goto_0

    :cond_3
    return-void

    :cond_4
    move v2, v3

    goto :goto_2
.end method

.method private sendMTFValues1(II)I
    .locals 41
    .param p1    # I
    .param p2    # I

    move-object/from16 v0, p0

    iget-object v15, v0, Lorg/apache/tools/bzip2/CBZip2OutputStream;->data:Lorg/apache/tools/bzip2/CBZip2OutputStream$Data;

    iget-object v0, v15, Lorg/apache/tools/bzip2/CBZip2OutputStream$Data;->sendMTFValues_rfreq:[[I

    move-object/from16 v31, v0

    iget-object v0, v15, Lorg/apache/tools/bzip2/CBZip2OutputStream$Data;->sendMTFValues_fave:[I

    move-object/from16 v16, v0

    iget-object v7, v15, Lorg/apache/tools/bzip2/CBZip2OutputStream$Data;->sendMTFValues_cost:[S

    iget-object v0, v15, Lorg/apache/tools/bzip2/CBZip2OutputStream$Data;->sfmap:[C

    move-object/from16 v35, v0

    iget-object v0, v15, Lorg/apache/tools/bzip2/CBZip2OutputStream$Data;->selector:[B

    move-object/from16 v34, v0

    iget-object v0, v15, Lorg/apache/tools/bzip2/CBZip2OutputStream$Data;->sendMTFValues_len:[[B

    move-object/from16 v22, v0

    const/16 v37, 0x0

    aget-object v23, v22, v37

    const/16 v37, 0x1

    aget-object v24, v22, v37

    const/16 v37, 0x2

    aget-object v25, v22, v37

    const/16 v37, 0x3

    aget-object v26, v22, v37

    const/16 v37, 0x4

    aget-object v27, v22, v37

    const/16 v37, 0x5

    aget-object v28, v22, v37

    move-object/from16 v0, p0

    iget v0, v0, Lorg/apache/tools/bzip2/CBZip2OutputStream;->nMTF:I

    move/from16 v29, v0

    const/16 v30, 0x0

    const/16 v21, 0x0

    :goto_0
    const/16 v37, 0x4

    move/from16 v0, v21

    move/from16 v1, v37

    if-ge v0, v1, :cond_c

    move/from16 v36, p1

    :cond_0
    add-int/lit8 v36, v36, -0x1

    if-ltz v36, :cond_1

    const/16 v37, 0x0

    aput v37, v16, v36

    aget-object v33, v31, v36

    move/from16 v19, p2

    :goto_1
    add-int/lit8 v19, v19, -0x1

    if-ltz v19, :cond_0

    const/16 v37, 0x0

    aput v37, v33, v19

    goto :goto_1

    :cond_1
    const/16 v30, 0x0

    const/16 v18, 0x0

    :goto_2
    move-object/from16 v0, p0

    iget v0, v0, Lorg/apache/tools/bzip2/CBZip2OutputStream;->nMTF:I

    move/from16 v37, v0

    move/from16 v0, v18

    move/from16 v1, v37

    if-ge v0, v1, :cond_a

    add-int/lit8 v37, v18, 0x32

    add-int/lit8 v37, v37, -0x1

    add-int/lit8 v38, v29, -0x1

    invoke-static/range {v37 .. v38}, Ljava/lang/Math;->min(II)I

    move-result v17

    const/16 v37, 0x6

    move/from16 v0, p1

    move/from16 v1, v37

    if-ne v0, v1, :cond_5

    const/4 v8, 0x0

    const/4 v9, 0x0

    const/4 v10, 0x0

    const/4 v11, 0x0

    const/4 v12, 0x0

    const/4 v13, 0x0

    move/from16 v19, v18

    :goto_3
    move/from16 v0, v19

    move/from16 v1, v17

    if-gt v0, v1, :cond_2

    aget-char v20, v35, v19

    aget-byte v37, v23, v20

    move/from16 v0, v37

    and-int/lit16 v0, v0, 0xff

    move/from16 v37, v0

    add-int v37, v37, v8

    move/from16 v0, v37

    int-to-short v8, v0

    aget-byte v37, v24, v20

    move/from16 v0, v37

    and-int/lit16 v0, v0, 0xff

    move/from16 v37, v0

    add-int v37, v37, v9

    move/from16 v0, v37

    int-to-short v9, v0

    aget-byte v37, v25, v20

    move/from16 v0, v37

    and-int/lit16 v0, v0, 0xff

    move/from16 v37, v0

    add-int v37, v37, v10

    move/from16 v0, v37

    int-to-short v10, v0

    aget-byte v37, v26, v20

    move/from16 v0, v37

    and-int/lit16 v0, v0, 0xff

    move/from16 v37, v0

    add-int v37, v37, v11

    move/from16 v0, v37

    int-to-short v11, v0

    aget-byte v37, v27, v20

    move/from16 v0, v37

    and-int/lit16 v0, v0, 0xff

    move/from16 v37, v0

    add-int v37, v37, v12

    move/from16 v0, v37

    int-to-short v12, v0

    aget-byte v37, v28, v20

    move/from16 v0, v37

    and-int/lit16 v0, v0, 0xff

    move/from16 v37, v0

    add-int v37, v37, v13

    move/from16 v0, v37

    int-to-short v13, v0

    add-int/lit8 v19, v19, 0x1

    goto :goto_3

    :cond_2
    const/16 v37, 0x0

    aput-short v8, v7, v37

    const/16 v37, 0x1

    aput-short v9, v7, v37

    const/16 v37, 0x2

    aput-short v10, v7, v37

    const/16 v37, 0x3

    aput-short v11, v7, v37

    const/16 v37, 0x4

    aput-short v12, v7, v37

    const/16 v37, 0x5

    aput-short v13, v7, v37

    :cond_3
    const/4 v6, -0x1

    move/from16 v36, p1

    const v5, 0x3b9ac9ff

    :cond_4
    :goto_4
    add-int/lit8 v36, v36, -0x1

    if-ltz v36, :cond_8

    aget-short v14, v7, v36

    if-ge v14, v5, :cond_4

    move v5, v14

    move/from16 v6, v36

    goto :goto_4

    :cond_5
    move/from16 v36, p1

    :goto_5
    add-int/lit8 v36, v36, -0x1

    if-ltz v36, :cond_6

    const/16 v37, 0x0

    aput-short v37, v7, v36

    goto :goto_5

    :cond_6
    move/from16 v19, v18

    :goto_6
    move/from16 v0, v19

    move/from16 v1, v17

    if-gt v0, v1, :cond_3

    aget-char v20, v35, v19

    move/from16 v36, p1

    :goto_7
    add-int/lit8 v36, v36, -0x1

    if-ltz v36, :cond_7

    aget-short v37, v7, v36

    aget-object v38, v22, v36

    aget-byte v38, v38, v20

    move/from16 v0, v38

    and-int/lit16 v0, v0, 0xff

    move/from16 v38, v0

    add-int v37, v37, v38

    move/from16 v0, v37

    int-to-short v0, v0

    move/from16 v37, v0

    aput-short v37, v7, v36

    goto :goto_7

    :cond_7
    add-int/lit8 v19, v19, 0x1

    goto :goto_6

    :cond_8
    aget v37, v16, v6

    add-int/lit8 v37, v37, 0x1

    aput v37, v16, v6

    int-to-byte v0, v6

    move/from16 v37, v0

    aput-byte v37, v34, v30

    add-int/lit8 v30, v30, 0x1

    aget-object v32, v31, v6

    move/from16 v19, v18

    :goto_8
    move/from16 v0, v19

    move/from16 v1, v17

    if-gt v0, v1, :cond_9

    aget-char v37, v35, v19

    aget v38, v32, v37

    add-int/lit8 v38, v38, 0x1

    aput v38, v32, v37

    add-int/lit8 v19, v19, 0x1

    goto :goto_8

    :cond_9
    add-int/lit8 v18, v17, 0x1

    goto/16 :goto_2

    :cond_a
    const/16 v36, 0x0

    :goto_9
    move/from16 v0, v36

    move/from16 v1, p1

    if-ge v0, v1, :cond_b

    aget-object v37, v22, v36

    aget-object v38, v31, v36

    move-object/from16 v0, p0

    iget-object v0, v0, Lorg/apache/tools/bzip2/CBZip2OutputStream;->data:Lorg/apache/tools/bzip2/CBZip2OutputStream$Data;

    move-object/from16 v39, v0

    const/16 v40, 0x14

    move-object/from16 v0, v37

    move-object/from16 v1, v38

    move-object/from16 v2, v39

    move/from16 v3, p2

    move/from16 v4, v40

    invoke-static {v0, v1, v2, v3, v4}, Lorg/apache/tools/bzip2/CBZip2OutputStream;->hbMakeCodeLengths([B[ILorg/apache/tools/bzip2/CBZip2OutputStream$Data;II)V

    add-int/lit8 v36, v36, 0x1

    goto :goto_9

    :cond_b
    add-int/lit8 v21, v21, 0x1

    goto/16 :goto_0

    :cond_c
    return v30
.end method

.method private sendMTFValues2(II)V
    .locals 10
    .param p1    # I
    .param p2    # I

    const/4 v9, 0x0

    iget-object v0, p0, Lorg/apache/tools/bzip2/CBZip2OutputStream;->data:Lorg/apache/tools/bzip2/CBZip2OutputStream$Data;

    iget-object v4, v0, Lorg/apache/tools/bzip2/CBZip2OutputStream$Data;->sendMTFValues2_pos:[B

    move v1, p1

    :goto_0
    add-int/lit8 v1, v1, -0x1

    if-ltz v1, :cond_0

    int-to-byte v7, v1

    aput-byte v7, v4, v1

    goto :goto_0

    :cond_0
    const/4 v1, 0x0

    :goto_1
    if-ge v1, p2, :cond_2

    iget-object v7, v0, Lorg/apache/tools/bzip2/CBZip2OutputStream$Data;->selector:[B

    aget-byte v3, v7, v1

    aget-byte v5, v4, v9

    const/4 v2, 0x0

    :goto_2
    if-eq v3, v5, :cond_1

    add-int/lit8 v2, v2, 0x1

    move v6, v5

    aget-byte v5, v4, v2

    aput-byte v6, v4, v2

    goto :goto_2

    :cond_1
    aput-byte v5, v4, v9

    iget-object v7, v0, Lorg/apache/tools/bzip2/CBZip2OutputStream$Data;->selectorMtf:[B

    int-to-byte v8, v2

    aput-byte v8, v7, v1

    add-int/lit8 v1, v1, 0x1

    goto :goto_1

    :cond_2
    return-void
.end method

.method private sendMTFValues3(II)V
    .locals 10
    .param p1    # I
    .param p2    # I

    iget-object v8, p0, Lorg/apache/tools/bzip2/CBZip2OutputStream;->data:Lorg/apache/tools/bzip2/CBZip2OutputStream$Data;

    iget-object v0, v8, Lorg/apache/tools/bzip2/CBZip2OutputStream$Data;->sendMTFValues_code:[[I

    iget-object v8, p0, Lorg/apache/tools/bzip2/CBZip2OutputStream;->data:Lorg/apache/tools/bzip2/CBZip2OutputStream$Data;

    iget-object v3, v8, Lorg/apache/tools/bzip2/CBZip2OutputStream$Data;->sendMTFValues_len:[[B

    const/4 v7, 0x0

    :goto_0
    if-ge v7, p1, :cond_3

    const/16 v6, 0x20

    const/4 v5, 0x0

    aget-object v4, v3, v7

    move v1, p2

    :cond_0
    :goto_1
    add-int/lit8 v1, v1, -0x1

    if-ltz v1, :cond_2

    aget-byte v8, v4, v1

    and-int/lit16 v2, v8, 0xff

    if-le v2, v5, :cond_1

    move v5, v2

    :cond_1
    if-ge v2, v6, :cond_0

    move v6, v2

    goto :goto_1

    :cond_2
    aget-object v8, v0, v7

    aget-object v9, v3, v7

    invoke-static {v8, v9, v6, v5, p2}, Lorg/apache/tools/bzip2/CBZip2OutputStream;->hbAssignCodes([I[BIII)V

    add-int/lit8 v7, v7, 0x1

    goto :goto_0

    :cond_3
    return-void
.end method

.method private sendMTFValues4()V
    .locals 12
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    const/4 v10, 0x0

    const/16 v11, 0x10

    const/4 v9, 0x1

    iget-object v8, p0, Lorg/apache/tools/bzip2/CBZip2OutputStream;->data:Lorg/apache/tools/bzip2/CBZip2OutputStream$Data;

    iget-object v4, v8, Lorg/apache/tools/bzip2/CBZip2OutputStream$Data;->inUse:[Z

    iget-object v8, p0, Lorg/apache/tools/bzip2/CBZip2OutputStream;->data:Lorg/apache/tools/bzip2/CBZip2OutputStream$Data;

    iget-object v5, v8, Lorg/apache/tools/bzip2/CBZip2OutputStream$Data;->sentMTFValues4_inUse16:[Z

    const/16 v2, 0x10

    :cond_0
    add-int/lit8 v2, v2, -0x1

    if-ltz v2, :cond_2

    aput-boolean v10, v5, v2

    mul-int/lit8 v3, v2, 0x10

    const/16 v6, 0x10

    :cond_1
    :goto_0
    add-int/lit8 v6, v6, -0x1

    if-ltz v6, :cond_0

    add-int v8, v3, v6

    aget-boolean v8, v4, v8

    if-eqz v8, :cond_1

    aput-boolean v9, v5, v2

    goto :goto_0

    :cond_2
    const/4 v2, 0x0

    :goto_1
    if-ge v2, v11, :cond_4

    aget-boolean v8, v5, v2

    if-eqz v8, :cond_3

    move v8, v9

    :goto_2
    invoke-direct {p0, v9, v8}, Lorg/apache/tools/bzip2/CBZip2OutputStream;->bsW(II)V

    add-int/lit8 v2, v2, 0x1

    goto :goto_1

    :cond_3
    move v8, v10

    goto :goto_2

    :cond_4
    iget-object v7, p0, Lorg/apache/tools/bzip2/CBZip2OutputStream;->out:Ljava/io/OutputStream;

    iget v1, p0, Lorg/apache/tools/bzip2/CBZip2OutputStream;->bsLive:I

    iget v0, p0, Lorg/apache/tools/bzip2/CBZip2OutputStream;->bsBuff:I

    const/4 v2, 0x0

    :goto_3
    if-ge v2, v11, :cond_8

    aget-boolean v8, v5, v2

    if-eqz v8, :cond_7

    mul-int/lit8 v3, v2, 0x10

    const/4 v6, 0x0

    :goto_4
    if-ge v6, v11, :cond_7

    :goto_5
    const/16 v8, 0x8

    if-lt v1, v8, :cond_5

    shr-int/lit8 v8, v0, 0x18

    invoke-virtual {v7, v8}, Ljava/io/OutputStream;->write(I)V

    shl-int/lit8 v0, v0, 0x8

    add-int/lit8 v1, v1, -0x8

    goto :goto_5

    :cond_5
    add-int v8, v3, v6

    aget-boolean v8, v4, v8

    if-eqz v8, :cond_6

    rsub-int/lit8 v8, v1, 0x20

    add-int/lit8 v8, v8, -0x1

    shl-int v8, v9, v8

    or-int/2addr v0, v8

    :cond_6
    add-int/lit8 v1, v1, 0x1

    add-int/lit8 v6, v6, 0x1

    goto :goto_4

    :cond_7
    add-int/lit8 v2, v2, 0x1

    goto :goto_3

    :cond_8
    iput v0, p0, Lorg/apache/tools/bzip2/CBZip2OutputStream;->bsBuff:I

    iput v1, p0, Lorg/apache/tools/bzip2/CBZip2OutputStream;->bsLive:I

    return-void
.end method

.method private sendMTFValues5(II)V
    .locals 10
    .param p1    # I
    .param p2    # I
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    const/16 v9, 0x8

    const/4 v7, 0x3

    invoke-direct {p0, v7, p1}, Lorg/apache/tools/bzip2/CBZip2OutputStream;->bsW(II)V

    const/16 v7, 0xf

    invoke-direct {p0, v7, p2}, Lorg/apache/tools/bzip2/CBZip2OutputStream;->bsW(II)V

    iget-object v5, p0, Lorg/apache/tools/bzip2/CBZip2OutputStream;->out:Ljava/io/OutputStream;

    iget-object v7, p0, Lorg/apache/tools/bzip2/CBZip2OutputStream;->data:Lorg/apache/tools/bzip2/CBZip2OutputStream$Data;

    iget-object v6, v7, Lorg/apache/tools/bzip2/CBZip2OutputStream$Data;->selectorMtf:[B

    iget v1, p0, Lorg/apache/tools/bzip2/CBZip2OutputStream;->bsLive:I

    iget v0, p0, Lorg/apache/tools/bzip2/CBZip2OutputStream;->bsBuff:I

    const/4 v3, 0x0

    :goto_0
    if-ge v3, p2, :cond_3

    const/4 v4, 0x0

    aget-byte v7, v6, v3

    and-int/lit16 v2, v7, 0xff

    :goto_1
    if-ge v4, v2, :cond_1

    :goto_2
    if-lt v1, v9, :cond_0

    shr-int/lit8 v7, v0, 0x18

    invoke-virtual {v5, v7}, Ljava/io/OutputStream;->write(I)V

    shl-int/lit8 v0, v0, 0x8

    add-int/lit8 v1, v1, -0x8

    goto :goto_2

    :cond_0
    const/4 v7, 0x1

    rsub-int/lit8 v8, v1, 0x20

    add-int/lit8 v8, v8, -0x1

    shl-int/2addr v7, v8

    or-int/2addr v0, v7

    add-int/lit8 v1, v1, 0x1

    add-int/lit8 v4, v4, 0x1

    goto :goto_1

    :cond_1
    :goto_3
    if-lt v1, v9, :cond_2

    shr-int/lit8 v7, v0, 0x18

    invoke-virtual {v5, v7}, Ljava/io/OutputStream;->write(I)V

    shl-int/lit8 v0, v0, 0x8

    add-int/lit8 v1, v1, -0x8

    goto :goto_3

    :cond_2
    add-int/lit8 v1, v1, 0x1

    add-int/lit8 v3, v3, 0x1

    goto :goto_0

    :cond_3
    iput v0, p0, Lorg/apache/tools/bzip2/CBZip2OutputStream;->bsBuff:I

    iput v1, p0, Lorg/apache/tools/bzip2/CBZip2OutputStream;->bsLive:I

    return-void
.end method

.method private sendMTFValues6(II)V
    .locals 12
    .param p1    # I
    .param p2    # I
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    const/16 v11, 0x8

    iget-object v9, p0, Lorg/apache/tools/bzip2/CBZip2OutputStream;->data:Lorg/apache/tools/bzip2/CBZip2OutputStream$Data;

    iget-object v4, v9, Lorg/apache/tools/bzip2/CBZip2OutputStream$Data;->sendMTFValues_len:[[B

    iget-object v7, p0, Lorg/apache/tools/bzip2/CBZip2OutputStream;->out:Ljava/io/OutputStream;

    iget v1, p0, Lorg/apache/tools/bzip2/CBZip2OutputStream;->bsLive:I

    iget v0, p0, Lorg/apache/tools/bzip2/CBZip2OutputStream;->bsBuff:I

    const/4 v8, 0x0

    :goto_0
    if-ge v8, p1, :cond_7

    aget-object v5, v4, v8

    const/4 v9, 0x0

    aget-byte v9, v5, v9

    and-int/lit16 v2, v9, 0xff

    :goto_1
    if-lt v1, v11, :cond_0

    shr-int/lit8 v9, v0, 0x18

    invoke-virtual {v7, v9}, Ljava/io/OutputStream;->write(I)V

    shl-int/lit8 v0, v0, 0x8

    add-int/lit8 v1, v1, -0x8

    goto :goto_1

    :cond_0
    rsub-int/lit8 v9, v1, 0x20

    add-int/lit8 v9, v9, -0x5

    shl-int v9, v2, v9

    or-int/2addr v0, v9

    add-int/lit8 v1, v1, 0x5

    const/4 v3, 0x0

    :goto_2
    if-ge v3, p2, :cond_6

    aget-byte v9, v5, v3

    and-int/lit16 v6, v9, 0xff

    :goto_3
    if-ge v2, v6, :cond_3

    :goto_4
    if-lt v1, v11, :cond_1

    shr-int/lit8 v9, v0, 0x18

    invoke-virtual {v7, v9}, Ljava/io/OutputStream;->write(I)V

    shl-int/lit8 v0, v0, 0x8

    add-int/lit8 v1, v1, -0x8

    goto :goto_4

    :cond_1
    const/4 v9, 0x2

    rsub-int/lit8 v10, v1, 0x20

    add-int/lit8 v10, v10, -0x2

    shl-int/2addr v9, v10

    or-int/2addr v0, v9

    add-int/lit8 v1, v1, 0x2

    add-int/lit8 v2, v2, 0x1

    goto :goto_3

    :cond_2
    const/4 v9, 0x3

    rsub-int/lit8 v10, v1, 0x20

    add-int/lit8 v10, v10, -0x2

    shl-int/2addr v9, v10

    or-int/2addr v0, v9

    add-int/lit8 v1, v1, 0x2

    add-int/lit8 v2, v2, -0x1

    :cond_3
    if-le v2, v6, :cond_4

    :goto_5
    if-lt v1, v11, :cond_2

    shr-int/lit8 v9, v0, 0x18

    invoke-virtual {v7, v9}, Ljava/io/OutputStream;->write(I)V

    shl-int/lit8 v0, v0, 0x8

    add-int/lit8 v1, v1, -0x8

    goto :goto_5

    :cond_4
    :goto_6
    if-lt v1, v11, :cond_5

    shr-int/lit8 v9, v0, 0x18

    invoke-virtual {v7, v9}, Ljava/io/OutputStream;->write(I)V

    shl-int/lit8 v0, v0, 0x8

    add-int/lit8 v1, v1, -0x8

    goto :goto_6

    :cond_5
    add-int/lit8 v1, v1, 0x1

    add-int/lit8 v3, v3, 0x1

    goto :goto_2

    :cond_6
    add-int/lit8 v8, v8, 0x1

    goto :goto_0

    :cond_7
    iput v0, p0, Lorg/apache/tools/bzip2/CBZip2OutputStream;->bsBuff:I

    iput v1, p0, Lorg/apache/tools/bzip2/CBZip2OutputStream;->bsLive:I

    return-void
.end method

.method private sendMTFValues7(I)V
    .locals 20
    .param p1    # I
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    move-object/from16 v0, p0

    iget-object v5, v0, Lorg/apache/tools/bzip2/CBZip2OutputStream;->data:Lorg/apache/tools/bzip2/CBZip2OutputStream$Data;

    iget-object v8, v5, Lorg/apache/tools/bzip2/CBZip2OutputStream$Data;->sendMTFValues_len:[[B

    iget-object v3, v5, Lorg/apache/tools/bzip2/CBZip2OutputStream$Data;->sendMTFValues_code:[[I

    move-object/from16 v0, p0

    iget-object v12, v0, Lorg/apache/tools/bzip2/CBZip2OutputStream;->out:Ljava/io/OutputStream;

    iget-object v14, v5, Lorg/apache/tools/bzip2/CBZip2OutputStream$Data;->selector:[B

    iget-object v0, v5, Lorg/apache/tools/bzip2/CBZip2OutputStream$Data;->sfmap:[C

    move-object/from16 v16, v0

    move-object/from16 v0, p0

    iget v11, v0, Lorg/apache/tools/bzip2/CBZip2OutputStream;->nMTF:I

    const/4 v13, 0x0

    move-object/from16 v0, p0

    iget v2, v0, Lorg/apache/tools/bzip2/CBZip2OutputStream;->bsLive:I

    move-object/from16 v0, p0

    iget v1, v0, Lorg/apache/tools/bzip2/CBZip2OutputStream;->bsBuff:I

    const/4 v7, 0x0

    :goto_0
    if-ge v7, v11, :cond_2

    add-int/lit8 v18, v7, 0x32

    add-int/lit8 v18, v18, -0x1

    add-int/lit8 v19, v11, -0x1

    invoke-static/range {v18 .. v19}, Ljava/lang/Math;->min(II)I

    move-result v6

    aget-byte v18, v14, v13

    move/from16 v0, v18

    and-int/lit16 v15, v0, 0xff

    aget-object v4, v3, v15

    aget-object v9, v8, v15

    :goto_1
    if-gt v7, v6, :cond_1

    aget-char v17, v16, v7

    :goto_2
    const/16 v18, 0x8

    move/from16 v0, v18

    if-lt v2, v0, :cond_0

    shr-int/lit8 v18, v1, 0x18

    move/from16 v0, v18

    invoke-virtual {v12, v0}, Ljava/io/OutputStream;->write(I)V

    shl-int/lit8 v1, v1, 0x8

    add-int/lit8 v2, v2, -0x8

    goto :goto_2

    :cond_0
    aget-byte v18, v9, v17

    move/from16 v0, v18

    and-int/lit16 v10, v0, 0xff

    aget v18, v4, v17

    rsub-int/lit8 v19, v2, 0x20

    sub-int v19, v19, v10

    shl-int v18, v18, v19

    or-int v1, v1, v18

    add-int/2addr v2, v10

    add-int/lit8 v7, v7, 0x1

    goto :goto_1

    :cond_1
    add-int/lit8 v7, v6, 0x1

    add-int/lit8 v13, v13, 0x1

    goto :goto_0

    :cond_2
    move-object/from16 v0, p0

    iput v1, v0, Lorg/apache/tools/bzip2/CBZip2OutputStream;->bsBuff:I

    move-object/from16 v0, p0

    iput v2, v0, Lorg/apache/tools/bzip2/CBZip2OutputStream;->bsLive:I

    return-void
.end method

.method private static vswap([IIII)V
    .locals 4
    .param p0    # [I
    .param p1    # I
    .param p2    # I
    .param p3    # I

    add-int/2addr p3, p1

    move v1, p2

    move v0, p1

    :goto_0
    if-ge v0, p3, :cond_0

    aget v2, p0, v0

    add-int/lit8 p1, v0, 0x1

    aget v3, p0, v1

    aput v3, p0, v0

    add-int/lit8 p2, v1, 0x1

    aput v2, p0, v1

    move v1, p2

    move v0, p1

    goto :goto_0

    :cond_0
    return-void
.end method

.method private write0(I)V
    .locals 3
    .param p1    # I
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    const/4 v2, -0x1

    iget v0, p0, Lorg/apache/tools/bzip2/CBZip2OutputStream;->currentChar:I

    if-eq v0, v2, :cond_2

    and-int/lit16 p1, p1, 0xff

    iget v0, p0, Lorg/apache/tools/bzip2/CBZip2OutputStream;->currentChar:I

    if-ne v0, p1, :cond_1

    iget v0, p0, Lorg/apache/tools/bzip2/CBZip2OutputStream;->runLength:I

    add-int/lit8 v0, v0, 0x1

    iput v0, p0, Lorg/apache/tools/bzip2/CBZip2OutputStream;->runLength:I

    const/16 v1, 0xfe

    if-le v0, v1, :cond_0

    invoke-direct {p0}, Lorg/apache/tools/bzip2/CBZip2OutputStream;->writeRun()V

    iput v2, p0, Lorg/apache/tools/bzip2/CBZip2OutputStream;->currentChar:I

    const/4 v0, 0x0

    iput v0, p0, Lorg/apache/tools/bzip2/CBZip2OutputStream;->runLength:I

    :cond_0
    :goto_0
    return-void

    :cond_1
    invoke-direct {p0}, Lorg/apache/tools/bzip2/CBZip2OutputStream;->writeRun()V

    const/4 v0, 0x1

    iput v0, p0, Lorg/apache/tools/bzip2/CBZip2OutputStream;->runLength:I

    iput p1, p0, Lorg/apache/tools/bzip2/CBZip2OutputStream;->currentChar:I

    goto :goto_0

    :cond_2
    and-int/lit16 v0, p1, 0xff

    iput v0, p0, Lorg/apache/tools/bzip2/CBZip2OutputStream;->currentChar:I

    iget v0, p0, Lorg/apache/tools/bzip2/CBZip2OutputStream;->runLength:I

    add-int/lit8 v0, v0, 0x1

    iput v0, p0, Lorg/apache/tools/bzip2/CBZip2OutputStream;->runLength:I

    goto :goto_0
.end method

.method private writeRun()V
    .locals 8
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    const/4 v7, 0x1

    iget v4, p0, Lorg/apache/tools/bzip2/CBZip2OutputStream;->last:I

    iget v6, p0, Lorg/apache/tools/bzip2/CBZip2OutputStream;->allowableBlockSize:I

    if-ge v4, v6, :cond_0

    iget v2, p0, Lorg/apache/tools/bzip2/CBZip2OutputStream;->currentChar:I

    iget-object v3, p0, Lorg/apache/tools/bzip2/CBZip2OutputStream;->data:Lorg/apache/tools/bzip2/CBZip2OutputStream$Data;

    iget-object v6, v3, Lorg/apache/tools/bzip2/CBZip2OutputStream$Data;->inUse:[Z

    aput-boolean v7, v6, v2

    int-to-byte v1, v2

    iget v5, p0, Lorg/apache/tools/bzip2/CBZip2OutputStream;->runLength:I

    iget-object v6, p0, Lorg/apache/tools/bzip2/CBZip2OutputStream;->crc:Lorg/apache/tools/bzip2/CRC;

    invoke-virtual {v6, v2, v5}, Lorg/apache/tools/bzip2/CRC;->updateCRC(II)V

    packed-switch v5, :pswitch_data_0

    add-int/lit8 v5, v5, -0x4

    iget-object v6, v3, Lorg/apache/tools/bzip2/CBZip2OutputStream$Data;->inUse:[Z

    aput-boolean v7, v6, v5

    iget-object v0, v3, Lorg/apache/tools/bzip2/CBZip2OutputStream$Data;->block:[B

    add-int/lit8 v6, v4, 0x2

    aput-byte v1, v0, v6

    add-int/lit8 v6, v4, 0x3

    aput-byte v1, v0, v6

    add-int/lit8 v6, v4, 0x4

    aput-byte v1, v0, v6

    add-int/lit8 v6, v4, 0x5

    aput-byte v1, v0, v6

    add-int/lit8 v6, v4, 0x6

    int-to-byte v7, v5

    aput-byte v7, v0, v6

    add-int/lit8 v6, v4, 0x5

    iput v6, p0, Lorg/apache/tools/bzip2/CBZip2OutputStream;->last:I

    :goto_0
    return-void

    :pswitch_0
    iget-object v6, v3, Lorg/apache/tools/bzip2/CBZip2OutputStream$Data;->block:[B

    add-int/lit8 v7, v4, 0x2

    aput-byte v1, v6, v7

    add-int/lit8 v6, v4, 0x1

    iput v6, p0, Lorg/apache/tools/bzip2/CBZip2OutputStream;->last:I

    goto :goto_0

    :pswitch_1
    iget-object v6, v3, Lorg/apache/tools/bzip2/CBZip2OutputStream$Data;->block:[B

    add-int/lit8 v7, v4, 0x2

    aput-byte v1, v6, v7

    iget-object v6, v3, Lorg/apache/tools/bzip2/CBZip2OutputStream$Data;->block:[B

    add-int/lit8 v7, v4, 0x3

    aput-byte v1, v6, v7

    add-int/lit8 v6, v4, 0x2

    iput v6, p0, Lorg/apache/tools/bzip2/CBZip2OutputStream;->last:I

    goto :goto_0

    :pswitch_2
    iget-object v0, v3, Lorg/apache/tools/bzip2/CBZip2OutputStream$Data;->block:[B

    add-int/lit8 v6, v4, 0x2

    aput-byte v1, v0, v6

    add-int/lit8 v6, v4, 0x3

    aput-byte v1, v0, v6

    add-int/lit8 v6, v4, 0x4

    aput-byte v1, v0, v6

    add-int/lit8 v6, v4, 0x3

    iput v6, p0, Lorg/apache/tools/bzip2/CBZip2OutputStream;->last:I

    goto :goto_0

    :cond_0
    invoke-direct {p0}, Lorg/apache/tools/bzip2/CBZip2OutputStream;->endBlock()V

    invoke-direct {p0}, Lorg/apache/tools/bzip2/CBZip2OutputStream;->initBlock()V

    invoke-direct {p0}, Lorg/apache/tools/bzip2/CBZip2OutputStream;->writeRun()V

    goto :goto_0

    nop

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
        :pswitch_1
        :pswitch_2
    .end packed-switch
.end method


# virtual methods
.method public close()V
    .locals 3
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    const/4 v2, 0x0

    iget-object v0, p0, Lorg/apache/tools/bzip2/CBZip2OutputStream;->out:Ljava/io/OutputStream;

    if-eqz v0, :cond_1

    :try_start_0
    iget v1, p0, Lorg/apache/tools/bzip2/CBZip2OutputStream;->runLength:I

    if-lez v1, :cond_0

    invoke-direct {p0}, Lorg/apache/tools/bzip2/CBZip2OutputStream;->writeRun()V

    :cond_0
    const/4 v1, -0x1

    iput v1, p0, Lorg/apache/tools/bzip2/CBZip2OutputStream;->currentChar:I

    invoke-direct {p0}, Lorg/apache/tools/bzip2/CBZip2OutputStream;->endBlock()V

    invoke-direct {p0}, Lorg/apache/tools/bzip2/CBZip2OutputStream;->endCompression()V

    invoke-virtual {v0}, Ljava/io/OutputStream;->close()V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    iput-object v2, p0, Lorg/apache/tools/bzip2/CBZip2OutputStream;->out:Ljava/io/OutputStream;

    iput-object v2, p0, Lorg/apache/tools/bzip2/CBZip2OutputStream;->data:Lorg/apache/tools/bzip2/CBZip2OutputStream$Data;

    :cond_1
    return-void

    :catchall_0
    move-exception v1

    iput-object v2, p0, Lorg/apache/tools/bzip2/CBZip2OutputStream;->out:Ljava/io/OutputStream;

    iput-object v2, p0, Lorg/apache/tools/bzip2/CBZip2OutputStream;->data:Lorg/apache/tools/bzip2/CBZip2OutputStream$Data;

    throw v1
.end method

.method protected finalize()V
    .locals 0
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/lang/Throwable;
        }
    .end annotation

    invoke-virtual {p0}, Lorg/apache/tools/bzip2/CBZip2OutputStream;->close()V

    invoke-super {p0}, Ljava/lang/Object;->finalize()V

    return-void
.end method

.method public flush()V
    .locals 1
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    iget-object v0, p0, Lorg/apache/tools/bzip2/CBZip2OutputStream;->out:Ljava/io/OutputStream;

    if-eqz v0, :cond_0

    invoke-virtual {v0}, Ljava/io/OutputStream;->flush()V

    :cond_0
    return-void
.end method

.method public final getBlockSize()I
    .locals 1

    iget v0, p0, Lorg/apache/tools/bzip2/CBZip2OutputStream;->blockSize100k:I

    return v0
.end method

.method public write(I)V
    .locals 2
    .param p1    # I
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    iget-object v0, p0, Lorg/apache/tools/bzip2/CBZip2OutputStream;->out:Ljava/io/OutputStream;

    if-eqz v0, :cond_0

    invoke-direct {p0, p1}, Lorg/apache/tools/bzip2/CBZip2OutputStream;->write0(I)V

    return-void

    :cond_0
    new-instance v0, Ljava/io/IOException;

    const-string v1, "closed"

    invoke-direct {v0, v1}, Ljava/io/IOException;-><init>(Ljava/lang/String;)V

    throw v0
.end method

.method public write([BII)V
    .locals 5
    .param p1    # [B
    .param p2    # I
    .param p3    # I
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    if-gez p2, :cond_0

    new-instance v2, Ljava/lang/IndexOutOfBoundsException;

    new-instance v3, Ljava/lang/StringBuffer;

    invoke-direct {v3}, Ljava/lang/StringBuffer;-><init>()V

    const-string v4, "offs("

    invoke-virtual {v3, v4}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    move-result-object v3

    invoke-virtual {v3, p2}, Ljava/lang/StringBuffer;->append(I)Ljava/lang/StringBuffer;

    move-result-object v3

    const-string v4, ") < 0."

    invoke-virtual {v3, v4}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuffer;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-direct {v2, v3}, Ljava/lang/IndexOutOfBoundsException;-><init>(Ljava/lang/String;)V

    throw v2

    :cond_0
    if-gez p3, :cond_1

    new-instance v2, Ljava/lang/IndexOutOfBoundsException;

    new-instance v3, Ljava/lang/StringBuffer;

    invoke-direct {v3}, Ljava/lang/StringBuffer;-><init>()V

    const-string v4, "len("

    invoke-virtual {v3, v4}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    move-result-object v3

    invoke-virtual {v3, p3}, Ljava/lang/StringBuffer;->append(I)Ljava/lang/StringBuffer;

    move-result-object v3

    const-string v4, ") < 0."

    invoke-virtual {v3, v4}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuffer;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-direct {v2, v3}, Ljava/lang/IndexOutOfBoundsException;-><init>(Ljava/lang/String;)V

    throw v2

    :cond_1
    add-int v2, p2, p3

    array-length v3, p1

    if-le v2, v3, :cond_2

    new-instance v2, Ljava/lang/IndexOutOfBoundsException;

    new-instance v3, Ljava/lang/StringBuffer;

    invoke-direct {v3}, Ljava/lang/StringBuffer;-><init>()V

    const-string v4, "offs("

    invoke-virtual {v3, v4}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    move-result-object v3

    invoke-virtual {v3, p2}, Ljava/lang/StringBuffer;->append(I)Ljava/lang/StringBuffer;

    move-result-object v3

    const-string v4, ") + len("

    invoke-virtual {v3, v4}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    move-result-object v3

    invoke-virtual {v3, p3}, Ljava/lang/StringBuffer;->append(I)Ljava/lang/StringBuffer;

    move-result-object v3

    const-string v4, ") > buf.length("

    invoke-virtual {v3, v4}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    move-result-object v3

    array-length v4, p1

    invoke-virtual {v3, v4}, Ljava/lang/StringBuffer;->append(I)Ljava/lang/StringBuffer;

    move-result-object v3

    const-string v4, ")."

    invoke-virtual {v3, v4}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuffer;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-direct {v2, v3}, Ljava/lang/IndexOutOfBoundsException;-><init>(Ljava/lang/String;)V

    throw v2

    :cond_2
    iget-object v2, p0, Lorg/apache/tools/bzip2/CBZip2OutputStream;->out:Ljava/io/OutputStream;

    if-nez v2, :cond_3

    new-instance v2, Ljava/io/IOException;

    const-string v3, "stream closed"

    invoke-direct {v2, v3}, Ljava/io/IOException;-><init>(Ljava/lang/String;)V

    throw v2

    :cond_3
    add-int v0, p2, p3

    move v1, p2

    :goto_0
    if-ge v1, v0, :cond_4

    add-int/lit8 p2, v1, 0x1

    aget-byte v2, p1, v1

    invoke-direct {p0, v2}, Lorg/apache/tools/bzip2/CBZip2OutputStream;->write0(I)V

    move v1, p2

    goto :goto_0

    :cond_4
    return-void
.end method
