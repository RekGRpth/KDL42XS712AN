.class Lcom/konka/appassistant/ApplicationInfoView$1;
.super Ljava/lang/Object;
.source "ApplicationInfoView.java"

# interfaces
.implements Landroid/view/View$OnKeyListener;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/konka/appassistant/ApplicationInfoView;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/konka/appassistant/ApplicationInfoView;


# direct methods
.method constructor <init>(Lcom/konka/appassistant/ApplicationInfoView;)V
    .locals 0

    iput-object p1, p0, Lcom/konka/appassistant/ApplicationInfoView$1;->this$0:Lcom/konka/appassistant/ApplicationInfoView;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onKey(Landroid/view/View;ILandroid/view/KeyEvent;)Z
    .locals 2
    .param p1    # Landroid/view/View;
    .param p2    # I
    .param p3    # Landroid/view/KeyEvent;

    invoke-virtual {p3}, Landroid/view/KeyEvent;->getAction()I

    move-result v0

    if-nez v0, :cond_0

    packed-switch p2, :pswitch_data_0

    :cond_0
    :goto_0
    const/4 v0, 0x0

    return v0

    :pswitch_0
    iget-object v0, p0, Lcom/konka/appassistant/ApplicationInfoView$1;->this$0:Lcom/konka/appassistant/ApplicationInfoView;

    # getter for: Lcom/konka/appassistant/ApplicationInfoView;->mOkButton:Landroid/widget/Button;
    invoke-static {v0}, Lcom/konka/appassistant/ApplicationInfoView;->access$0(Lcom/konka/appassistant/ApplicationInfoView;)Landroid/widget/Button;

    move-result-object v0

    if-eq p1, v0, :cond_1

    iget-object v0, p0, Lcom/konka/appassistant/ApplicationInfoView$1;->this$0:Lcom/konka/appassistant/ApplicationInfoView;

    # getter for: Lcom/konka/appassistant/ApplicationInfoView;->mZipInstallResult:Landroid/widget/TextView;
    invoke-static {v0}, Lcom/konka/appassistant/ApplicationInfoView;->access$1(Lcom/konka/appassistant/ApplicationInfoView;)Landroid/widget/TextView;

    move-result-object v0

    if-ne p1, v0, :cond_0

    :cond_1
    iget-object v0, p0, Lcom/konka/appassistant/ApplicationInfoView$1;->this$0:Lcom/konka/appassistant/ApplicationInfoView;

    # getter for: Lcom/konka/appassistant/ApplicationInfoView;->mContext:Landroid/content/Context;
    invoke-static {v0}, Lcom/konka/appassistant/ApplicationInfoView;->access$2(Lcom/konka/appassistant/ApplicationInfoView;)Landroid/content/Context;

    move-result-object v0

    instance-of v0, v0, Lcom/konka/appassistant/AppAssistantActivity;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/konka/appassistant/ApplicationInfoView$1;->this$0:Lcom/konka/appassistant/ApplicationInfoView;

    # getter for: Lcom/konka/appassistant/ApplicationInfoView;->mContext:Landroid/content/Context;
    invoke-static {v0}, Lcom/konka/appassistant/ApplicationInfoView;->access$2(Lcom/konka/appassistant/ApplicationInfoView;)Landroid/content/Context;

    move-result-object v0

    check-cast v0, Lcom/konka/appassistant/AppAssistantActivity;

    iget-object v1, p0, Lcom/konka/appassistant/ApplicationInfoView$1;->this$0:Lcom/konka/appassistant/ApplicationInfoView;

    # getter for: Lcom/konka/appassistant/ApplicationInfoView;->mViewType:Lcom/konka/appassistant/ApplicationInfoView$ViewType;
    invoke-static {v1}, Lcom/konka/appassistant/ApplicationInfoView;->access$3(Lcom/konka/appassistant/ApplicationInfoView;)Lcom/konka/appassistant/ApplicationInfoView$ViewType;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/konka/appassistant/AppAssistantActivity;->requestFocus(Lcom/konka/appassistant/ApplicationInfoView$ViewType;)V

    iget-object v0, p0, Lcom/konka/appassistant/ApplicationInfoView$1;->this$0:Lcom/konka/appassistant/ApplicationInfoView;

    # getter for: Lcom/konka/appassistant/ApplicationInfoView;->mChildNum:I
    invoke-static {v0}, Lcom/konka/appassistant/ApplicationInfoView;->access$4(Lcom/konka/appassistant/ApplicationInfoView;)I

    move-result v0

    sput v0, Lcom/konka/appassistant/AppAssistantActivity;->mLastFocusedItemNum:I

    goto :goto_0

    nop

    :pswitch_data_0
    .packed-switch 0x15
        :pswitch_0
    .end packed-switch
.end method
