.class Lcom/konka/appassistant/AppAssistantActivity$6;
.super Ljava/lang/Object;
.source "AppAssistantActivity.java"

# interfaces
.implements Ljava/util/Comparator;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/konka/appassistant/AppAssistantActivity;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Object;",
        "Ljava/util/Comparator",
        "<",
        "Lcom/konka/appassistant/AppAssistantActivity$AppInfo;",
        ">;"
    }
.end annotation


# instance fields
.field final synthetic this$0:Lcom/konka/appassistant/AppAssistantActivity;


# direct methods
.method constructor <init>(Lcom/konka/appassistant/AppAssistantActivity;)V
    .locals 0

    iput-object p1, p0, Lcom/konka/appassistant/AppAssistantActivity$6;->this$0:Lcom/konka/appassistant/AppAssistantActivity;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public compare(Lcom/konka/appassistant/AppAssistantActivity$AppInfo;Lcom/konka/appassistant/AppAssistantActivity$AppInfo;)I
    .locals 12
    .param p1    # Lcom/konka/appassistant/AppAssistantActivity$AppInfo;
    .param p2    # Lcom/konka/appassistant/AppAssistantActivity$AppInfo;

    const/4 v8, 0x1

    const/4 v7, -0x1

    const/4 v6, 0x0

    const/4 v3, 0x0

    const/4 v4, 0x0

    :try_start_0
    iget-object v9, p0, Lcom/konka/appassistant/AppAssistantActivity$6;->this$0:Lcom/konka/appassistant/AppAssistantActivity;

    # getter for: Lcom/konka/appassistant/AppAssistantActivity;->pm:Landroid/content/pm/PackageManager;
    invoke-static {v9}, Lcom/konka/appassistant/AppAssistantActivity;->access$19(Lcom/konka/appassistant/AppAssistantActivity;)Landroid/content/pm/PackageManager;

    move-result-object v9

    iget-object v10, p1, Lcom/konka/appassistant/AppAssistantActivity$AppInfo;->info:Landroid/content/pm/ApplicationInfo;

    iget-object v10, v10, Landroid/content/pm/ApplicationInfo;->packageName:Ljava/lang/String;

    const/4 v11, 0x0

    invoke-virtual {v9, v10, v11}, Landroid/content/pm/PackageManager;->getPackageInfo(Ljava/lang/String;I)Landroid/content/pm/PackageInfo;

    move-result-object v3

    iget-object v9, p0, Lcom/konka/appassistant/AppAssistantActivity$6;->this$0:Lcom/konka/appassistant/AppAssistantActivity;

    # getter for: Lcom/konka/appassistant/AppAssistantActivity;->pm:Landroid/content/pm/PackageManager;
    invoke-static {v9}, Lcom/konka/appassistant/AppAssistantActivity;->access$19(Lcom/konka/appassistant/AppAssistantActivity;)Landroid/content/pm/PackageManager;

    move-result-object v9

    iget-object v10, p2, Lcom/konka/appassistant/AppAssistantActivity$AppInfo;->info:Landroid/content/pm/ApplicationInfo;

    iget-object v10, v10, Landroid/content/pm/ApplicationInfo;->packageName:Ljava/lang/String;

    const/4 v11, 0x0

    invoke-virtual {v9, v10, v11}, Landroid/content/pm/PackageManager;->getPackageInfo(Ljava/lang/String;I)Landroid/content/pm/PackageInfo;
    :try_end_0
    .catch Landroid/content/pm/PackageManager$NameNotFoundException; {:try_start_0 .. :try_end_0} :catch_0

    move-result-object v4

    :goto_0
    if-nez v3, :cond_1

    if-nez v4, :cond_1

    move v5, v6

    :cond_0
    :goto_1
    return v5

    :catch_0
    move-exception v2

    invoke-virtual {v2}, Landroid/content/pm/PackageManager$NameNotFoundException;->printStackTrace()V

    goto :goto_0

    :cond_1
    if-nez v3, :cond_2

    move v5, v7

    goto :goto_1

    :cond_2
    if-nez v4, :cond_3

    move v5, v8

    goto :goto_1

    :cond_3
    iget-object v9, v3, Landroid/content/pm/PackageInfo;->applicationInfo:Landroid/content/pm/ApplicationInfo;

    iget-object v10, p0, Lcom/konka/appassistant/AppAssistantActivity$6;->this$0:Lcom/konka/appassistant/AppAssistantActivity;

    # getter for: Lcom/konka/appassistant/AppAssistantActivity;->pm:Landroid/content/pm/PackageManager;
    invoke-static {v10}, Lcom/konka/appassistant/AppAssistantActivity;->access$19(Lcom/konka/appassistant/AppAssistantActivity;)Landroid/content/pm/PackageManager;

    move-result-object v10

    invoke-virtual {v9, v10}, Landroid/content/pm/ApplicationInfo;->loadLabel(Landroid/content/pm/PackageManager;)Ljava/lang/CharSequence;

    move-result-object v0

    check-cast v0, Ljava/lang/String;

    iget-object v9, v4, Landroid/content/pm/PackageInfo;->applicationInfo:Landroid/content/pm/ApplicationInfo;

    iget-object v10, p0, Lcom/konka/appassistant/AppAssistantActivity$6;->this$0:Lcom/konka/appassistant/AppAssistantActivity;

    # getter for: Lcom/konka/appassistant/AppAssistantActivity;->pm:Landroid/content/pm/PackageManager;
    invoke-static {v10}, Lcom/konka/appassistant/AppAssistantActivity;->access$19(Lcom/konka/appassistant/AppAssistantActivity;)Landroid/content/pm/PackageManager;

    move-result-object v10

    invoke-virtual {v9, v10}, Landroid/content/pm/ApplicationInfo;->loadLabel(Landroid/content/pm/PackageManager;)Ljava/lang/CharSequence;

    move-result-object v1

    check-cast v1, Ljava/lang/String;

    # getter for: Lcom/konka/appassistant/AppAssistantActivity;->sCollator:Ljava/text/Collator;
    invoke-static {}, Lcom/konka/appassistant/AppAssistantActivity;->access$20()Ljava/text/Collator;

    move-result-object v9

    invoke-virtual {v9, v0, v1}, Ljava/text/Collator;->compare(Ljava/lang/String;Ljava/lang/String;)I

    move-result v5

    if-nez v5, :cond_0

    iget-object v9, v3, Landroid/content/pm/PackageInfo;->versionName:Ljava/lang/String;

    if-nez v9, :cond_4

    iget-object v9, v4, Landroid/content/pm/PackageInfo;->versionName:Ljava/lang/String;

    if-nez v9, :cond_4

    move v5, v6

    goto :goto_1

    :cond_4
    iget-object v6, v3, Landroid/content/pm/PackageInfo;->versionName:Ljava/lang/String;

    if-nez v6, :cond_5

    move v5, v7

    goto :goto_1

    :cond_5
    iget-object v6, v4, Landroid/content/pm/PackageInfo;->versionName:Ljava/lang/String;

    if-nez v6, :cond_6

    move v5, v8

    goto :goto_1

    :cond_6
    # getter for: Lcom/konka/appassistant/AppAssistantActivity;->sCollator:Ljava/text/Collator;
    invoke-static {}, Lcom/konka/appassistant/AppAssistantActivity;->access$20()Ljava/text/Collator;

    move-result-object v6

    iget-object v7, v3, Landroid/content/pm/PackageInfo;->versionName:Ljava/lang/String;

    iget-object v8, v4, Landroid/content/pm/PackageInfo;->versionName:Ljava/lang/String;

    invoke-virtual {v6, v7, v8}, Ljava/text/Collator;->compare(Ljava/lang/String;Ljava/lang/String;)I

    move-result v5

    goto :goto_1
.end method

.method public bridge synthetic compare(Ljava/lang/Object;Ljava/lang/Object;)I
    .locals 1

    check-cast p1, Lcom/konka/appassistant/AppAssistantActivity$AppInfo;

    check-cast p2, Lcom/konka/appassistant/AppAssistantActivity$AppInfo;

    invoke-virtual {p0, p1, p2}, Lcom/konka/appassistant/AppAssistantActivity$6;->compare(Lcom/konka/appassistant/AppAssistantActivity$AppInfo;Lcom/konka/appassistant/AppAssistantActivity$AppInfo;)I

    move-result v0

    return v0
.end method
