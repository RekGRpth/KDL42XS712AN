.class Lcom/konka/appassistant/ApplicationInfoView$3;
.super Ljava/lang/Object;
.source "ApplicationInfoView.java"

# interfaces
.implements Landroid/view/View$OnClickListener;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/konka/appassistant/ApplicationInfoView;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# static fields
.field private static synthetic $SWITCH_TABLE$com$konka$appassistant$ApplicationInfoView$ViewType:[I


# instance fields
.field final synthetic this$0:Lcom/konka/appassistant/ApplicationInfoView;


# direct methods
.method static synthetic $SWITCH_TABLE$com$konka$appassistant$ApplicationInfoView$ViewType()[I
    .locals 3

    sget-object v0, Lcom/konka/appassistant/ApplicationInfoView$3;->$SWITCH_TABLE$com$konka$appassistant$ApplicationInfoView$ViewType:[I

    if-eqz v0, :cond_0

    :goto_0
    return-object v0

    :cond_0
    invoke-static {}, Lcom/konka/appassistant/ApplicationInfoView$ViewType;->values()[Lcom/konka/appassistant/ApplicationInfoView$ViewType;

    move-result-object v0

    array-length v0, v0

    new-array v0, v0, [I

    :try_start_0
    sget-object v1, Lcom/konka/appassistant/ApplicationInfoView$ViewType;->INSTALL_VIEW:Lcom/konka/appassistant/ApplicationInfoView$ViewType;

    invoke-virtual {v1}, Lcom/konka/appassistant/ApplicationInfoView$ViewType;->ordinal()I

    move-result v1

    const/4 v2, 0x1

    aput v2, v0, v1
    :try_end_0
    .catch Ljava/lang/NoSuchFieldError; {:try_start_0 .. :try_end_0} :catch_2

    :goto_1
    :try_start_1
    sget-object v1, Lcom/konka/appassistant/ApplicationInfoView$ViewType;->UNINSTALL_VIEW:Lcom/konka/appassistant/ApplicationInfoView$ViewType;

    invoke-virtual {v1}, Lcom/konka/appassistant/ApplicationInfoView$ViewType;->ordinal()I

    move-result v1

    const/4 v2, 0x2

    aput v2, v0, v1
    :try_end_1
    .catch Ljava/lang/NoSuchFieldError; {:try_start_1 .. :try_end_1} :catch_1

    :goto_2
    :try_start_2
    sget-object v1, Lcom/konka/appassistant/ApplicationInfoView$ViewType;->ZIP_INSTALL_VIEW:Lcom/konka/appassistant/ApplicationInfoView$ViewType;

    invoke-virtual {v1}, Lcom/konka/appassistant/ApplicationInfoView$ViewType;->ordinal()I

    move-result v1

    const/4 v2, 0x3

    aput v2, v0, v1
    :try_end_2
    .catch Ljava/lang/NoSuchFieldError; {:try_start_2 .. :try_end_2} :catch_0

    :goto_3
    sput-object v0, Lcom/konka/appassistant/ApplicationInfoView$3;->$SWITCH_TABLE$com$konka$appassistant$ApplicationInfoView$ViewType:[I

    goto :goto_0

    :catch_0
    move-exception v1

    goto :goto_3

    :catch_1
    move-exception v1

    goto :goto_2

    :catch_2
    move-exception v1

    goto :goto_1
.end method

.method constructor <init>(Lcom/konka/appassistant/ApplicationInfoView;)V
    .locals 0

    iput-object p1, p0, Lcom/konka/appassistant/ApplicationInfoView$3;->this$0:Lcom/konka/appassistant/ApplicationInfoView;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onClick(Landroid/view/View;)V
    .locals 2
    .param p1    # Landroid/view/View;

    invoke-static {}, Lcom/konka/appassistant/ApplicationInfoView$3;->$SWITCH_TABLE$com$konka$appassistant$ApplicationInfoView$ViewType()[I

    move-result-object v0

    iget-object v1, p0, Lcom/konka/appassistant/ApplicationInfoView$3;->this$0:Lcom/konka/appassistant/ApplicationInfoView;

    # getter for: Lcom/konka/appassistant/ApplicationInfoView;->mViewType:Lcom/konka/appassistant/ApplicationInfoView$ViewType;
    invoke-static {v1}, Lcom/konka/appassistant/ApplicationInfoView;->access$3(Lcom/konka/appassistant/ApplicationInfoView;)Lcom/konka/appassistant/ApplicationInfoView$ViewType;

    move-result-object v1

    invoke-virtual {v1}, Lcom/konka/appassistant/ApplicationInfoView$ViewType;->ordinal()I

    move-result v1

    aget v0, v0, v1

    packed-switch v0, :pswitch_data_0

    :goto_0
    :pswitch_0
    return-void

    :pswitch_1
    iget-object v0, p0, Lcom/konka/appassistant/ApplicationInfoView$3;->this$0:Lcom/konka/appassistant/ApplicationInfoView;

    # invokes: Lcom/konka/appassistant/ApplicationInfoView;->OpenDialog()V
    invoke-static {v0}, Lcom/konka/appassistant/ApplicationInfoView;->access$7(Lcom/konka/appassistant/ApplicationInfoView;)V

    goto :goto_0

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_1
        :pswitch_0
        :pswitch_0
    .end packed-switch
.end method
