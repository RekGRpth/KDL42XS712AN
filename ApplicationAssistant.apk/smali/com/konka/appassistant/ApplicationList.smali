.class public Lcom/konka/appassistant/ApplicationList;
.super Ljava/lang/Object;
.source "ApplicationList.java"


# static fields
.field private static synthetic $SWITCH_TABLE$com$konka$appassistant$ApplicationInfoView$ViewType:[I = null

.field static final TAG:Ljava/lang/String; = "AppAssistant"


# instance fields
.field private ChildNum:I

.field private TopOfPage:I

.field private mAppListView:Landroid/widget/LinearLayout;

.field private mApplicationList:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Lcom/konka/appassistant/ApplicationInfoView;",
            ">;"
        }
    .end annotation
.end field

.field private mContext:Landroid/content/Context;

.field private mInstallAppList:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Landroid/net/Uri;",
            ">;"
        }
    .end annotation
.end field

.field private mUninstallAppList:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Lcom/konka/appassistant/AppAssistantActivity$AppInfo;",
            ">;"
        }
    .end annotation
.end field

.field private mViewType:Lcom/konka/appassistant/ApplicationInfoView$ViewType;


# direct methods
.method static synthetic $SWITCH_TABLE$com$konka$appassistant$ApplicationInfoView$ViewType()[I
    .locals 3

    sget-object v0, Lcom/konka/appassistant/ApplicationList;->$SWITCH_TABLE$com$konka$appassistant$ApplicationInfoView$ViewType:[I

    if-eqz v0, :cond_0

    :goto_0
    return-object v0

    :cond_0
    invoke-static {}, Lcom/konka/appassistant/ApplicationInfoView$ViewType;->values()[Lcom/konka/appassistant/ApplicationInfoView$ViewType;

    move-result-object v0

    array-length v0, v0

    new-array v0, v0, [I

    :try_start_0
    sget-object v1, Lcom/konka/appassistant/ApplicationInfoView$ViewType;->INSTALL_VIEW:Lcom/konka/appassistant/ApplicationInfoView$ViewType;

    invoke-virtual {v1}, Lcom/konka/appassistant/ApplicationInfoView$ViewType;->ordinal()I

    move-result v1

    const/4 v2, 0x1

    aput v2, v0, v1
    :try_end_0
    .catch Ljava/lang/NoSuchFieldError; {:try_start_0 .. :try_end_0} :catch_2

    :goto_1
    :try_start_1
    sget-object v1, Lcom/konka/appassistant/ApplicationInfoView$ViewType;->UNINSTALL_VIEW:Lcom/konka/appassistant/ApplicationInfoView$ViewType;

    invoke-virtual {v1}, Lcom/konka/appassistant/ApplicationInfoView$ViewType;->ordinal()I

    move-result v1

    const/4 v2, 0x2

    aput v2, v0, v1
    :try_end_1
    .catch Ljava/lang/NoSuchFieldError; {:try_start_1 .. :try_end_1} :catch_1

    :goto_2
    :try_start_2
    sget-object v1, Lcom/konka/appassistant/ApplicationInfoView$ViewType;->ZIP_INSTALL_VIEW:Lcom/konka/appassistant/ApplicationInfoView$ViewType;

    invoke-virtual {v1}, Lcom/konka/appassistant/ApplicationInfoView$ViewType;->ordinal()I

    move-result v1

    const/4 v2, 0x3

    aput v2, v0, v1
    :try_end_2
    .catch Ljava/lang/NoSuchFieldError; {:try_start_2 .. :try_end_2} :catch_0

    :goto_3
    sput-object v0, Lcom/konka/appassistant/ApplicationList;->$SWITCH_TABLE$com$konka$appassistant$ApplicationInfoView$ViewType:[I

    goto :goto_0

    :catch_0
    move-exception v1

    goto :goto_3

    :catch_1
    move-exception v1

    goto :goto_2

    :catch_2
    move-exception v1

    goto :goto_1
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/widget/LinearLayout;)V
    .locals 3
    .param p1    # Landroid/content/Context;
    .param p2    # Landroid/widget/LinearLayout;

    const/4 v2, 0x0

    const/4 v1, 0x0

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, Lcom/konka/appassistant/ApplicationList;->mApplicationList:Ljava/util/List;

    iput-object v2, p0, Lcom/konka/appassistant/ApplicationList;->mUninstallAppList:Ljava/util/List;

    iput-object v2, p0, Lcom/konka/appassistant/ApplicationList;->mInstallAppList:Ljava/util/List;

    sget-object v0, Lcom/konka/appassistant/ApplicationInfoView$ViewType;->INSTALL_VIEW:Lcom/konka/appassistant/ApplicationInfoView$ViewType;

    iput-object v0, p0, Lcom/konka/appassistant/ApplicationList;->mViewType:Lcom/konka/appassistant/ApplicationInfoView$ViewType;

    iput-object v2, p0, Lcom/konka/appassistant/ApplicationList;->mAppListView:Landroid/widget/LinearLayout;

    iput v1, p0, Lcom/konka/appassistant/ApplicationList;->TopOfPage:I

    iput v1, p0, Lcom/konka/appassistant/ApplicationList;->ChildNum:I

    iput-object p1, p0, Lcom/konka/appassistant/ApplicationList;->mContext:Landroid/content/Context;

    iput-object p2, p0, Lcom/konka/appassistant/ApplicationList;->mAppListView:Landroid/widget/LinearLayout;

    iput v1, p0, Lcom/konka/appassistant/ApplicationList;->ChildNum:I

    return-void
.end method

.method private AddViewForViewType(Landroid/net/Uri;Lcom/konka/appassistant/AppAssistantActivity$AppInfo;)V
    .locals 10
    .param p1    # Landroid/net/Uri;
    .param p2    # Lcom/konka/appassistant/AppAssistantActivity$AppInfo;

    const/4 v7, 0x0

    const/4 v0, 0x0

    invoke-static {}, Lcom/konka/appassistant/ApplicationList;->$SWITCH_TABLE$com$konka$appassistant$ApplicationInfoView$ViewType()[I

    move-result-object v1

    iget-object v2, p0, Lcom/konka/appassistant/ApplicationList;->mViewType:Lcom/konka/appassistant/ApplicationInfoView$ViewType;

    invoke-virtual {v2}, Lcom/konka/appassistant/ApplicationInfoView$ViewType;->ordinal()I

    move-result v2

    aget v1, v1, v2

    packed-switch v1, :pswitch_data_0

    :cond_0
    :goto_0
    return-void

    :pswitch_0
    iget-object v1, p0, Lcom/konka/appassistant/ApplicationList;->mInstallAppList:Ljava/util/List;

    if-eqz v1, :cond_0

    new-instance v0, Lcom/konka/appassistant/ApplicationInfoView;

    iget-object v1, p0, Lcom/konka/appassistant/ApplicationList;->mContext:Landroid/content/Context;

    sget-object v2, Lcom/konka/appassistant/ApplicationInfoView$ViewType;->INSTALL_VIEW:Lcom/konka/appassistant/ApplicationInfoView$ViewType;

    iget v3, p0, Lcom/konka/appassistant/ApplicationList;->ChildNum:I

    add-int/lit8 v4, v3, 0x1

    iput v4, p0, Lcom/konka/appassistant/ApplicationList;->ChildNum:I

    invoke-direct {v0, v1, p1, v2, v3}, Lcom/konka/appassistant/ApplicationInfoView;-><init>(Landroid/content/Context;Landroid/net/Uri;Lcom/konka/appassistant/ApplicationInfoView$ViewType;I)V

    iget-object v1, p0, Lcom/konka/appassistant/ApplicationList;->mApplicationList:Ljava/util/List;

    invoke-interface {v1, v0}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    invoke-virtual {v0}, Lcom/konka/appassistant/ApplicationInfoView;->getAppView()Landroid/view/View;

    move-result-object v7

    if-eqz v7, :cond_0

    iget-object v1, p0, Lcom/konka/appassistant/ApplicationList;->mAppListView:Landroid/widget/LinearLayout;

    invoke-virtual {v1, v7}, Landroid/widget/LinearLayout;->addView(Landroid/view/View;)V

    goto :goto_0

    :pswitch_1
    iget-object v1, p0, Lcom/konka/appassistant/ApplicationList;->mUninstallAppList:Ljava/util/List;

    if-eqz v1, :cond_0

    new-instance v0, Lcom/konka/appassistant/ApplicationInfoView;

    iget-object v1, p0, Lcom/konka/appassistant/ApplicationList;->mContext:Landroid/content/Context;

    iget-object v2, p2, Lcom/konka/appassistant/AppAssistantActivity$AppInfo;->info:Landroid/content/pm/ApplicationInfo;

    sget-object v3, Lcom/konka/appassistant/ApplicationInfoView$ViewType;->UNINSTALL_VIEW:Lcom/konka/appassistant/ApplicationInfoView$ViewType;

    iget-wide v4, p2, Lcom/konka/appassistant/AppAssistantActivity$AppInfo;->appsize:J

    iget v6, p0, Lcom/konka/appassistant/ApplicationList;->ChildNum:I

    add-int/lit8 v9, v6, 0x1

    iput v9, p0, Lcom/konka/appassistant/ApplicationList;->ChildNum:I

    invoke-direct/range {v0 .. v6}, Lcom/konka/appassistant/ApplicationInfoView;-><init>(Landroid/content/Context;Landroid/content/pm/ApplicationInfo;Lcom/konka/appassistant/ApplicationInfoView$ViewType;JI)V

    iget-object v1, p0, Lcom/konka/appassistant/ApplicationList;->mApplicationList:Ljava/util/List;

    invoke-interface {v1, v0}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    invoke-virtual {v0}, Lcom/konka/appassistant/ApplicationInfoView;->getAppView()Landroid/view/View;

    move-result-object v7

    if-eqz v7, :cond_0

    iget-object v1, p0, Lcom/konka/appassistant/ApplicationList;->mAppListView:Landroid/widget/LinearLayout;

    invoke-virtual {v1, v7}, Landroid/widget/LinearLayout;->addView(Landroid/view/View;)V

    goto :goto_0

    :pswitch_2
    iget-object v1, p0, Lcom/konka/appassistant/ApplicationList;->mInstallAppList:Ljava/util/List;

    if-eqz v1, :cond_0

    new-instance v0, Lcom/konka/appassistant/ApplicationInfoView;

    iget-object v1, p0, Lcom/konka/appassistant/ApplicationList;->mContext:Landroid/content/Context;

    sget-object v2, Lcom/konka/appassistant/ApplicationInfoView$ViewType;->ZIP_INSTALL_VIEW:Lcom/konka/appassistant/ApplicationInfoView$ViewType;

    iget v3, p0, Lcom/konka/appassistant/ApplicationList;->ChildNum:I

    add-int/lit8 v4, v3, 0x1

    iput v4, p0, Lcom/konka/appassistant/ApplicationList;->ChildNum:I

    invoke-direct {v0, v1, p1, v2, v3}, Lcom/konka/appassistant/ApplicationInfoView;-><init>(Landroid/content/Context;Landroid/net/Uri;Lcom/konka/appassistant/ApplicationInfoView$ViewType;I)V

    iget-object v1, p0, Lcom/konka/appassistant/ApplicationList;->mApplicationList:Ljava/util/List;

    invoke-interface {v1, v0}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    invoke-virtual {v0}, Lcom/konka/appassistant/ApplicationInfoView;->getAppView()Landroid/view/View;

    move-result-object v7

    if-eqz v7, :cond_0

    iget-object v1, p0, Lcom/konka/appassistant/ApplicationList;->mAppListView:Landroid/widget/LinearLayout;

    invoke-virtual {v1, v7}, Landroid/widget/LinearLayout;->addView(Landroid/view/View;)V

    invoke-static {}, Lcom/konka/appassistant/ApplicationList;->getViewNumPerPage()I

    move-result v8

    iget-object v1, p0, Lcom/konka/appassistant/ApplicationList;->mApplicationList:Ljava/util/List;

    invoke-interface {v1}, Ljava/util/List;->size()I

    move-result v1

    if-le v1, v8, :cond_0

    iget-object v1, p0, Lcom/konka/appassistant/ApplicationList;->mContext:Landroid/content/Context;

    check-cast v1, Lcom/konka/appassistant/AppAssistantActivity;

    invoke-virtual {v1}, Lcom/konka/appassistant/AppAssistantActivity;->sendScrollMsg()V

    goto/16 :goto_0

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
        :pswitch_1
        :pswitch_2
    .end packed-switch
.end method

.method private getFocusNum()I
    .locals 3

    const/4 v2, -0x1

    iget-object v1, p0, Lcom/konka/appassistant/ApplicationList;->mApplicationList:Ljava/util/List;

    if-eqz v1, :cond_0

    iget-object v1, p0, Lcom/konka/appassistant/ApplicationList;->mApplicationList:Ljava/util/List;

    invoke-interface {v1}, Ljava/util/List;->size()I

    move-result v1

    if-nez v1, :cond_2

    :cond_0
    move v0, v2

    :cond_1
    :goto_0
    return v0

    :cond_2
    const/4 v0, 0x0

    :goto_1
    iget-object v1, p0, Lcom/konka/appassistant/ApplicationList;->mApplicationList:Ljava/util/List;

    invoke-interface {v1}, Ljava/util/List;->size()I

    move-result v1

    if-lt v0, v1, :cond_3

    move v0, v2

    goto :goto_0

    :cond_3
    iget-object v1, p0, Lcom/konka/appassistant/ApplicationList;->mApplicationList:Ljava/util/List;

    invoke-interface {v1, v0}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/konka/appassistant/ApplicationInfoView;

    invoke-virtual {v1}, Lcom/konka/appassistant/ApplicationInfoView;->IsFocused()I

    move-result v1

    if-nez v1, :cond_1

    add-int/lit8 v0, v0, 0x1

    goto :goto_1
.end method

.method public static getViewNumPerPage()I
    .locals 2

    invoke-static {}, Lcom/konka/appassistant/AppAssistantActivity;->getScrollViewHeight()I

    move-result v0

    if-eqz v0, :cond_0

    invoke-static {}, Lcom/konka/appassistant/ApplicationInfoView;->getViewHeight()I

    move-result v0

    if-nez v0, :cond_1

    :cond_0
    const/4 v0, 0x0

    :goto_0
    return v0

    :cond_1
    invoke-static {}, Lcom/konka/appassistant/AppAssistantActivity;->getScrollViewHeight()I

    move-result v0

    invoke-static {}, Lcom/konka/appassistant/ApplicationInfoView;->getViewHeight()I

    move-result v1

    div-int/2addr v0, v1

    goto :goto_0
.end method


# virtual methods
.method public AddView(Lcom/konka/appassistant/ApplicationInfoView$ViewType;Landroid/net/Uri;Lcom/konka/appassistant/AppAssistantActivity$AppInfo;)V
    .locals 2
    .param p1    # Lcom/konka/appassistant/ApplicationInfoView$ViewType;
    .param p2    # Landroid/net/Uri;
    .param p3    # Lcom/konka/appassistant/AppAssistantActivity$AppInfo;

    iput-object p1, p0, Lcom/konka/appassistant/ApplicationList;->mViewType:Lcom/konka/appassistant/ApplicationInfoView$ViewType;

    if-eqz p2, :cond_1

    iget-object v1, p0, Lcom/konka/appassistant/ApplicationList;->mInstallAppList:Ljava/util/List;

    if-nez v1, :cond_0

    new-instance v1, Ljava/util/ArrayList;

    invoke-direct {v1}, Ljava/util/ArrayList;-><init>()V

    iput-object v1, p0, Lcom/konka/appassistant/ApplicationList;->mInstallAppList:Ljava/util/List;

    :cond_0
    invoke-static {p2}, Lcom/konka/appassistant/PackageUtil;->getApplicationInfo(Landroid/net/Uri;)Landroid/content/pm/ApplicationInfo;

    move-result-object v0

    if-eqz v0, :cond_1

    iget-object v1, v0, Landroid/content/pm/ApplicationInfo;->packageName:Ljava/lang/String;

    if-eqz v1, :cond_1

    iget-object v1, p0, Lcom/konka/appassistant/ApplicationList;->mInstallAppList:Ljava/util/List;

    invoke-interface {v1, p2}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    :cond_1
    if-eqz p3, :cond_3

    iget-object v1, p0, Lcom/konka/appassistant/ApplicationList;->mUninstallAppList:Ljava/util/List;

    if-nez v1, :cond_2

    new-instance v1, Ljava/util/ArrayList;

    invoke-direct {v1}, Ljava/util/ArrayList;-><init>()V

    iput-object v1, p0, Lcom/konka/appassistant/ApplicationList;->mUninstallAppList:Ljava/util/List;

    :cond_2
    iget-object v1, p0, Lcom/konka/appassistant/ApplicationList;->mUninstallAppList:Ljava/util/List;

    invoke-interface {v1, p3}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    :cond_3
    invoke-direct {p0, p2, p3}, Lcom/konka/appassistant/ApplicationList;->AddViewForViewType(Landroid/net/Uri;Lcom/konka/appassistant/AppAssistantActivity$AppInfo;)V

    return-void
.end method

.method public FocusOnFirst()V
    .locals 2

    iget-object v0, p0, Lcom/konka/appassistant/ApplicationList;->mApplicationList:Ljava/util/List;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/konka/appassistant/ApplicationList;->mApplicationList:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->size()I

    move-result v0

    if-eqz v0, :cond_0

    invoke-static {}, Lcom/konka/appassistant/ApplicationList;->$SWITCH_TABLE$com$konka$appassistant$ApplicationInfoView$ViewType()[I

    move-result-object v0

    iget-object v1, p0, Lcom/konka/appassistant/ApplicationList;->mViewType:Lcom/konka/appassistant/ApplicationInfoView$ViewType;

    invoke-virtual {v1}, Lcom/konka/appassistant/ApplicationInfoView$ViewType;->ordinal()I

    move-result v1

    aget v0, v0, v1

    packed-switch v0, :pswitch_data_0

    :cond_0
    :goto_0
    return-void

    :pswitch_0
    iget-object v0, p0, Lcom/konka/appassistant/ApplicationList;->mApplicationList:Ljava/util/List;

    iget v1, p0, Lcom/konka/appassistant/ApplicationList;->TopOfPage:I

    invoke-interface {v0, v1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/konka/appassistant/ApplicationInfoView;

    const/4 v1, 0x1

    invoke-virtual {v0, v1}, Lcom/konka/appassistant/ApplicationInfoView;->setFocused(I)V

    goto :goto_0

    :pswitch_1
    iget-object v0, p0, Lcom/konka/appassistant/ApplicationList;->mApplicationList:Ljava/util/List;

    iget v1, p0, Lcom/konka/appassistant/ApplicationList;->TopOfPage:I

    invoke-interface {v0, v1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/konka/appassistant/ApplicationInfoView;

    const/4 v1, 0x3

    invoke-virtual {v0, v1}, Lcom/konka/appassistant/ApplicationInfoView;->setFocused(I)V

    goto :goto_0

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
        :pswitch_0
        :pswitch_1
    .end packed-switch
.end method

.method public FocusOnTopofPage()V
    .locals 2

    iget-object v0, p0, Lcom/konka/appassistant/ApplicationList;->mApplicationList:Ljava/util/List;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/konka/appassistant/ApplicationList;->mApplicationList:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->size()I

    move-result v0

    if-lez v0, :cond_0

    iget-object v0, p0, Lcom/konka/appassistant/ApplicationList;->mApplicationList:Ljava/util/List;

    iget v1, p0, Lcom/konka/appassistant/ApplicationList;->TopOfPage:I

    invoke-interface {v0, v1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/konka/appassistant/ApplicationInfoView;

    const/4 v1, 0x1

    invoke-virtual {v0, v1}, Lcom/konka/appassistant/ApplicationInfoView;->setFocused(I)V

    :cond_0
    return-void
.end method

.method public OnKeyUp()I
    .locals 4

    const/4 v3, -0x1

    invoke-direct {p0}, Lcom/konka/appassistant/ApplicationList;->getFocusNum()I

    move-result v1

    if-ne v3, v1, :cond_0

    move v2, v3

    :goto_0
    return v2

    :cond_0
    iget-object v2, p0, Lcom/konka/appassistant/ApplicationList;->mApplicationList:Ljava/util/List;

    invoke-interface {v2, v1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lcom/konka/appassistant/ApplicationInfoView;

    invoke-virtual {v2}, Lcom/konka/appassistant/ApplicationInfoView;->IsFocused()I

    move-result v0

    if-nez v1, :cond_1

    move v2, v3

    goto :goto_0

    :cond_1
    add-int/lit8 v2, v1, -0x1

    iget v3, p0, Lcom/konka/appassistant/ApplicationList;->TopOfPage:I

    if-lt v2, v3, :cond_3

    add-int/lit8 v2, v1, -0x1

    if-ltz v2, :cond_2

    iget-object v2, p0, Lcom/konka/appassistant/ApplicationList;->mApplicationList:Ljava/util/List;

    add-int/lit8 v3, v1, -0x1

    invoke-interface {v2, v3}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lcom/konka/appassistant/ApplicationInfoView;

    invoke-virtual {v2, v0}, Lcom/konka/appassistant/ApplicationInfoView;->setFocused(I)V

    :cond_2
    iget v2, p0, Lcom/konka/appassistant/ApplicationList;->TopOfPage:I

    invoke-static {}, Lcom/konka/appassistant/ApplicationInfoView;->getViewHeight()I

    move-result v3

    mul-int/2addr v2, v3

    goto :goto_0

    :cond_3
    add-int/lit8 v2, v1, -0x1

    if-ltz v2, :cond_4

    iget-object v2, p0, Lcom/konka/appassistant/ApplicationList;->mApplicationList:Ljava/util/List;

    add-int/lit8 v3, v1, -0x1

    invoke-interface {v2, v3}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lcom/konka/appassistant/ApplicationInfoView;

    invoke-virtual {v2, v0}, Lcom/konka/appassistant/ApplicationInfoView;->setFocused(I)V

    :cond_4
    iget v2, p0, Lcom/konka/appassistant/ApplicationList;->TopOfPage:I

    add-int/lit8 v2, v2, -0x1

    iput v2, p0, Lcom/konka/appassistant/ApplicationList;->TopOfPage:I

    iget v2, p0, Lcom/konka/appassistant/ApplicationList;->TopOfPage:I

    invoke-static {}, Lcom/konka/appassistant/ApplicationInfoView;->getViewHeight()I

    move-result v3

    mul-int/2addr v2, v3

    goto :goto_0
.end method

.method public OnkeyDown()I
    .locals 5

    const/4 v3, -0x1

    invoke-direct {p0}, Lcom/konka/appassistant/ApplicationList;->getFocusNum()I

    move-result v1

    invoke-static {}, Lcom/konka/appassistant/ApplicationList;->getViewNumPerPage()I

    move-result v2

    if-ne v3, v1, :cond_0

    :goto_0
    return v3

    :cond_0
    iget-object v3, p0, Lcom/konka/appassistant/ApplicationList;->mApplicationList:Ljava/util/List;

    invoke-interface {v3, v1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Lcom/konka/appassistant/ApplicationInfoView;

    invoke-virtual {v3}, Lcom/konka/appassistant/ApplicationInfoView;->IsFocused()I

    move-result v0

    add-int/lit8 v3, v1, 0x1

    iget v4, p0, Lcom/konka/appassistant/ApplicationList;->TopOfPage:I

    add-int/2addr v4, v2

    if-lt v3, v4, :cond_2

    add-int/lit8 v3, v1, 0x1

    iget-object v4, p0, Lcom/konka/appassistant/ApplicationList;->mApplicationList:Ljava/util/List;

    invoke-interface {v4}, Ljava/util/List;->size()I

    move-result v4

    if-ge v3, v4, :cond_1

    iget-object v3, p0, Lcom/konka/appassistant/ApplicationList;->mApplicationList:Ljava/util/List;

    add-int/lit8 v4, v1, 0x1

    invoke-interface {v3, v4}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Lcom/konka/appassistant/ApplicationInfoView;

    invoke-virtual {v3, v0}, Lcom/konka/appassistant/ApplicationInfoView;->setFocused(I)V

    :cond_1
    iget v3, p0, Lcom/konka/appassistant/ApplicationList;->TopOfPage:I

    add-int/lit8 v3, v3, 0x1

    iput v3, p0, Lcom/konka/appassistant/ApplicationList;->TopOfPage:I

    iget v3, p0, Lcom/konka/appassistant/ApplicationList;->TopOfPage:I

    invoke-static {}, Lcom/konka/appassistant/ApplicationInfoView;->getViewHeight()I

    move-result v4

    mul-int/2addr v3, v4

    goto :goto_0

    :cond_2
    add-int/lit8 v3, v1, 0x1

    iget-object v4, p0, Lcom/konka/appassistant/ApplicationList;->mApplicationList:Ljava/util/List;

    invoke-interface {v4}, Ljava/util/List;->size()I

    move-result v4

    if-ge v3, v4, :cond_3

    iget-object v3, p0, Lcom/konka/appassistant/ApplicationList;->mApplicationList:Ljava/util/List;

    add-int/lit8 v4, v1, 0x1

    invoke-interface {v3, v4}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Lcom/konka/appassistant/ApplicationInfoView;

    invoke-virtual {v3, v0}, Lcom/konka/appassistant/ApplicationInfoView;->setFocused(I)V

    :cond_3
    iget v3, p0, Lcom/konka/appassistant/ApplicationList;->TopOfPage:I

    invoke-static {}, Lcom/konka/appassistant/ApplicationInfoView;->getViewHeight()I

    move-result v4

    mul-int/2addr v3, v4

    goto :goto_0
.end method

.method public PageDown()I
    .locals 6

    invoke-direct {p0}, Lcom/konka/appassistant/ApplicationList;->getFocusNum()I

    move-result v1

    invoke-static {}, Lcom/konka/appassistant/ApplicationList;->getViewNumPerPage()I

    move-result v2

    iget-object v4, p0, Lcom/konka/appassistant/ApplicationList;->mApplicationList:Ljava/util/List;

    invoke-interface {v4, v1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v4

    check-cast v4, Lcom/konka/appassistant/ApplicationInfoView;

    invoke-virtual {v4}, Lcom/konka/appassistant/ApplicationInfoView;->IsFocused()I

    move-result v0

    add-int v4, v1, v2

    iget-object v5, p0, Lcom/konka/appassistant/ApplicationList;->mApplicationList:Ljava/util/List;

    invoke-interface {v5}, Ljava/util/List;->size()I

    move-result v5

    if-ge v4, v5, :cond_0

    add-int v4, v1, v2

    div-int v3, v4, v2

    iget-object v4, p0, Lcom/konka/appassistant/ApplicationList;->mApplicationList:Ljava/util/List;

    add-int v5, v1, v2

    invoke-interface {v4, v5}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v4

    check-cast v4, Lcom/konka/appassistant/ApplicationInfoView;

    invoke-virtual {v4, v0}, Lcom/konka/appassistant/ApplicationInfoView;->setFocused(I)V

    mul-int v4, v3, v2

    iput v4, p0, Lcom/konka/appassistant/ApplicationList;->TopOfPage:I

    mul-int v4, v3, v2

    invoke-static {}, Lcom/konka/appassistant/ApplicationInfoView;->getViewHeight()I

    move-result v5

    mul-int/2addr v4, v5

    :goto_0
    return v4

    :cond_0
    iget-object v4, p0, Lcom/konka/appassistant/ApplicationList;->mApplicationList:Ljava/util/List;

    iget-object v5, p0, Lcom/konka/appassistant/ApplicationList;->mApplicationList:Ljava/util/List;

    invoke-interface {v5}, Ljava/util/List;->size()I

    move-result v5

    add-int/lit8 v5, v5, -0x1

    invoke-interface {v4, v5}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v4

    check-cast v4, Lcom/konka/appassistant/ApplicationInfoView;

    invoke-virtual {v4, v0}, Lcom/konka/appassistant/ApplicationInfoView;->setFocused(I)V

    iget-object v4, p0, Lcom/konka/appassistant/ApplicationList;->mApplicationList:Ljava/util/List;

    invoke-interface {v4}, Ljava/util/List;->size()I

    move-result v4

    sub-int/2addr v4, v2

    iput v4, p0, Lcom/konka/appassistant/ApplicationList;->TopOfPage:I

    iget-object v4, p0, Lcom/konka/appassistant/ApplicationList;->mApplicationList:Ljava/util/List;

    invoke-interface {v4}, Ljava/util/List;->size()I

    move-result v4

    add-int/lit8 v4, v4, -0x1

    invoke-static {}, Lcom/konka/appassistant/ApplicationInfoView;->getViewHeight()I

    move-result v5

    mul-int/2addr v4, v5

    goto :goto_0
.end method

.method public PageUp()I
    .locals 6

    const/4 v5, 0x0

    invoke-direct {p0}, Lcom/konka/appassistant/ApplicationList;->getFocusNum()I

    move-result v1

    invoke-static {}, Lcom/konka/appassistant/ApplicationList;->getViewNumPerPage()I

    move-result v2

    iget-object v4, p0, Lcom/konka/appassistant/ApplicationList;->mApplicationList:Ljava/util/List;

    invoke-interface {v4, v1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v4

    check-cast v4, Lcom/konka/appassistant/ApplicationInfoView;

    invoke-virtual {v4}, Lcom/konka/appassistant/ApplicationInfoView;->IsFocused()I

    move-result v0

    if-nez v1, :cond_0

    const/4 v4, -0x1

    :goto_0
    return v4

    :cond_0
    if-lt v1, v2, :cond_1

    sub-int v4, v1, v2

    div-int v3, v4, v2

    iget-object v4, p0, Lcom/konka/appassistant/ApplicationList;->mApplicationList:Ljava/util/List;

    sub-int v5, v1, v2

    invoke-interface {v4, v5}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v4

    check-cast v4, Lcom/konka/appassistant/ApplicationInfoView;

    invoke-virtual {v4, v0}, Lcom/konka/appassistant/ApplicationInfoView;->setFocused(I)V

    mul-int v4, v3, v2

    iput v4, p0, Lcom/konka/appassistant/ApplicationList;->TopOfPage:I

    mul-int v4, v3, v2

    invoke-static {}, Lcom/konka/appassistant/ApplicationInfoView;->getViewHeight()I

    move-result v5

    mul-int/2addr v4, v5

    goto :goto_0

    :cond_1
    iput v5, p0, Lcom/konka/appassistant/ApplicationList;->TopOfPage:I

    iget-object v4, p0, Lcom/konka/appassistant/ApplicationList;->mApplicationList:Ljava/util/List;

    invoke-interface {v4, v5}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v4

    check-cast v4, Lcom/konka/appassistant/ApplicationInfoView;

    invoke-virtual {v4, v0}, Lcom/konka/appassistant/ApplicationInfoView;->setFocused(I)V

    move v4, v5

    goto :goto_0
.end method

.method public RemoveView(I)V
    .locals 6
    .param p1    # I

    const/4 v4, 0x0

    const/4 v5, 0x2

    if-ltz p1, :cond_0

    iget-object v3, p0, Lcom/konka/appassistant/ApplicationList;->mInstallAppList:Ljava/util/List;

    if-eqz v3, :cond_0

    iget-object v3, p0, Lcom/konka/appassistant/ApplicationList;->mApplicationList:Ljava/util/List;

    if-eqz v3, :cond_0

    iget-object v3, p0, Lcom/konka/appassistant/ApplicationList;->mApplicationList:Ljava/util/List;

    invoke-interface {v3}, Ljava/util/List;->size()I

    move-result v3

    if-lt p1, v3, :cond_1

    :cond_0
    :goto_0
    return-void

    :cond_1
    iget-object v3, p0, Lcom/konka/appassistant/ApplicationList;->mApplicationList:Ljava/util/List;

    invoke-interface {v3, p1}, Ljava/util/List;->remove(I)Ljava/lang/Object;

    iget-object v3, p0, Lcom/konka/appassistant/ApplicationList;->mAppListView:Landroid/widget/LinearLayout;

    invoke-virtual {v3, p1}, Landroid/widget/LinearLayout;->removeViewAt(I)V

    iget v3, p0, Lcom/konka/appassistant/ApplicationList;->TopOfPage:I

    if-ne p1, v3, :cond_2

    const/4 v3, 0x1

    :goto_1
    invoke-static {v3}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v1

    move v0, p1

    :goto_2
    iget-object v3, p0, Lcom/konka/appassistant/ApplicationList;->mApplicationList:Ljava/util/List;

    invoke-interface {v3}, Ljava/util/List;->size()I

    move-result v3

    if-lt v0, v3, :cond_3

    iget-object v3, p0, Lcom/konka/appassistant/ApplicationList;->mApplicationList:Ljava/util/List;

    invoke-interface {v3}, Ljava/util/List;->size()I

    move-result v3

    if-lez v3, :cond_0

    add-int/lit8 v3, p1, -0x1

    if-ltz v3, :cond_5

    invoke-virtual {v1}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v3

    if-eqz v3, :cond_4

    iget-object v3, p0, Lcom/konka/appassistant/ApplicationList;->mApplicationList:Ljava/util/List;

    invoke-interface {v3}, Ljava/util/List;->size()I

    move-result v3

    if-ge p1, v3, :cond_4

    iget-object v3, p0, Lcom/konka/appassistant/ApplicationList;->mApplicationList:Ljava/util/List;

    invoke-interface {v3, p1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Lcom/konka/appassistant/ApplicationInfoView;

    invoke-virtual {v3, v5}, Lcom/konka/appassistant/ApplicationInfoView;->setFocused(I)V

    goto :goto_0

    :cond_2
    move v3, v4

    goto :goto_1

    :cond_3
    iget-object v3, p0, Lcom/konka/appassistant/ApplicationList;->mApplicationList:Ljava/util/List;

    invoke-interface {v3, v0}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lcom/konka/appassistant/ApplicationInfoView;

    invoke-virtual {v2}, Lcom/konka/appassistant/ApplicationInfoView;->getChildNum()I

    move-result v3

    add-int/lit8 v3, v3, -0x1

    invoke-virtual {v2, v3}, Lcom/konka/appassistant/ApplicationInfoView;->setChildNum(I)V

    add-int/lit8 v0, v0, 0x1

    goto :goto_2

    :cond_4
    iget-object v3, p0, Lcom/konka/appassistant/ApplicationList;->mApplicationList:Ljava/util/List;

    add-int/lit8 v4, p1, -0x1

    invoke-interface {v3, v4}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Lcom/konka/appassistant/ApplicationInfoView;

    invoke-virtual {v3, v5}, Lcom/konka/appassistant/ApplicationInfoView;->setFocused(I)V

    goto :goto_0

    :cond_5
    iget-object v3, p0, Lcom/konka/appassistant/ApplicationList;->mApplicationList:Ljava/util/List;

    invoke-interface {v3, v4}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Lcom/konka/appassistant/ApplicationInfoView;

    invoke-virtual {v3, v5}, Lcom/konka/appassistant/ApplicationInfoView;->setFocused(I)V

    goto :goto_0
.end method

.method public ShowZipApkInstallResult(Ljava/lang/String;ILjava/lang/String;)V
    .locals 4
    .param p1    # Ljava/lang/String;
    .param p2    # I
    .param p3    # Ljava/lang/String;

    iget-object v2, p0, Lcom/konka/appassistant/ApplicationList;->mViewType:Lcom/konka/appassistant/ApplicationInfoView$ViewType;

    sget-object v3, Lcom/konka/appassistant/ApplicationInfoView$ViewType;->ZIP_INSTALL_VIEW:Lcom/konka/appassistant/ApplicationInfoView$ViewType;

    if-eq v2, v3, :cond_1

    :cond_0
    :goto_0
    return-void

    :cond_1
    const/4 v0, 0x0

    :goto_1
    iget-object v2, p0, Lcom/konka/appassistant/ApplicationList;->mApplicationList:Ljava/util/List;

    invoke-interface {v2}, Ljava/util/List;->size()I

    move-result v2

    if-ge v0, v2, :cond_0

    iget-object v2, p0, Lcom/konka/appassistant/ApplicationList;->mApplicationList:Ljava/util/List;

    invoke-interface {v2, v0}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lcom/konka/appassistant/ApplicationInfoView;

    invoke-virtual {v2}, Lcom/konka/appassistant/ApplicationInfoView;->getFilename()Ljava/lang/String;

    move-result-object v1

    if-eqz v1, :cond_2

    if-eqz p1, :cond_2

    invoke-virtual {p1, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_2

    iget-object v2, p0, Lcom/konka/appassistant/ApplicationList;->mApplicationList:Ljava/util/List;

    invoke-interface {v2, v0}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lcom/konka/appassistant/ApplicationInfoView;

    sget-object v3, Lcom/konka/appassistant/ApplicationInfoView$ViewType;->ZIP_INSTALL_VIEW:Lcom/konka/appassistant/ApplicationInfoView$ViewType;

    invoke-virtual {v2, v3}, Lcom/konka/appassistant/ApplicationInfoView;->setViewType(Lcom/konka/appassistant/ApplicationInfoView$ViewType;)V

    iget-object v2, p0, Lcom/konka/appassistant/ApplicationList;->mApplicationList:Ljava/util/List;

    invoke-interface {v2, v0}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lcom/konka/appassistant/ApplicationInfoView;

    invoke-virtual {v2, p2, p3}, Lcom/konka/appassistant/ApplicationInfoView;->ShowZipApkInstallResult(ILjava/lang/String;)V

    goto :goto_0

    :cond_2
    add-int/lit8 v0, v0, 0x1

    goto :goto_1
.end method

.method public addToApplicationList(Lcom/konka/appassistant/ApplicationInfoView;)V
    .locals 1
    .param p1    # Lcom/konka/appassistant/ApplicationInfoView;

    iget-object v0, p0, Lcom/konka/appassistant/ApplicationList;->mApplicationList:Ljava/util/List;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/konka/appassistant/ApplicationList;->mApplicationList:Ljava/util/List;

    invoke-interface {v0, p1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    :cond_0
    return-void
.end method

.method public getApplicationList()Ljava/util/List;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/List",
            "<",
            "Lcom/konka/appassistant/ApplicationInfoView;",
            ">;"
        }
    .end annotation

    iget-object v0, p0, Lcom/konka/appassistant/ApplicationList;->mApplicationList:Ljava/util/List;

    return-object v0
.end method

.method public getFirstView()Landroid/view/View;
    .locals 2

    iget-object v0, p0, Lcom/konka/appassistant/ApplicationList;->mAppListView:Landroid/widget/LinearLayout;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/konka/appassistant/ApplicationList;->mAppListView:Landroid/widget/LinearLayout;

    invoke-virtual {v0}, Landroid/widget/LinearLayout;->getChildCount()I

    move-result v0

    if-nez v0, :cond_1

    :cond_0
    const/4 v0, 0x0

    :goto_0
    return-object v0

    :cond_1
    iget-object v0, p0, Lcom/konka/appassistant/ApplicationList;->mAppListView:Landroid/widget/LinearLayout;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Landroid/widget/LinearLayout;->getChildAt(I)Landroid/view/View;

    move-result-object v0

    goto :goto_0
.end method

.method public getTopPageNum()I
    .locals 1

    iget v0, p0, Lcom/konka/appassistant/ApplicationList;->TopOfPage:I

    return v0
.end method

.method public initTopOfPage()V
    .locals 1

    const/4 v0, 0x0

    iput v0, p0, Lcom/konka/appassistant/ApplicationList;->TopOfPage:I

    return-void
.end method

.method public initView()V
    .locals 8

    iget-object v1, p0, Lcom/konka/appassistant/ApplicationList;->mAppListView:Landroid/widget/LinearLayout;

    if-nez v1, :cond_1

    :cond_0
    :goto_0
    :pswitch_0
    return-void

    :cond_1
    const/4 v6, 0x0

    :goto_1
    iget-object v1, p0, Lcom/konka/appassistant/ApplicationList;->mAppListView:Landroid/widget/LinearLayout;

    invoke-virtual {v1}, Landroid/widget/LinearLayout;->getChildCount()I

    move-result v1

    if-lt v6, v1, :cond_3

    const/4 v1, 0x0

    iput v1, p0, Lcom/konka/appassistant/ApplicationList;->ChildNum:I

    iget-object v1, p0, Lcom/konka/appassistant/ApplicationList;->mApplicationList:Ljava/util/List;

    invoke-interface {v1}, Ljava/util/List;->clear()V

    invoke-virtual {p0}, Lcom/konka/appassistant/ApplicationList;->initTopOfPage()V

    invoke-static {}, Lcom/konka/appassistant/ApplicationList;->$SWITCH_TABLE$com$konka$appassistant$ApplicationInfoView$ViewType()[I

    move-result-object v1

    iget-object v2, p0, Lcom/konka/appassistant/ApplicationList;->mViewType:Lcom/konka/appassistant/ApplicationInfoView$ViewType;

    invoke-virtual {v2}, Lcom/konka/appassistant/ApplicationInfoView$ViewType;->ordinal()I

    move-result v2

    aget v1, v1, v2

    packed-switch v1, :pswitch_data_0

    goto :goto_0

    :pswitch_1
    iget-object v1, p0, Lcom/konka/appassistant/ApplicationList;->mInstallAppList:Ljava/util/List;

    if-eqz v1, :cond_0

    const/4 v6, 0x0

    :goto_2
    iget-object v1, p0, Lcom/konka/appassistant/ApplicationList;->mInstallAppList:Ljava/util/List;

    invoke-interface {v1}, Ljava/util/List;->size()I

    move-result v1

    if-ge v6, v1, :cond_0

    new-instance v0, Lcom/konka/appassistant/ApplicationInfoView;

    iget-object v2, p0, Lcom/konka/appassistant/ApplicationList;->mContext:Landroid/content/Context;

    iget-object v1, p0, Lcom/konka/appassistant/ApplicationList;->mInstallAppList:Ljava/util/List;

    invoke-interface {v1, v6}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Landroid/net/Uri;

    iget-object v3, p0, Lcom/konka/appassistant/ApplicationList;->mViewType:Lcom/konka/appassistant/ApplicationInfoView$ViewType;

    invoke-direct {v0, v2, v1, v3, v6}, Lcom/konka/appassistant/ApplicationInfoView;-><init>(Landroid/content/Context;Landroid/net/Uri;Lcom/konka/appassistant/ApplicationInfoView$ViewType;I)V

    invoke-virtual {v0}, Lcom/konka/appassistant/ApplicationInfoView;->getAppView()Landroid/view/View;

    move-result-object v7

    if-eqz v7, :cond_2

    iget-object v1, p0, Lcom/konka/appassistant/ApplicationList;->mAppListView:Landroid/widget/LinearLayout;

    invoke-virtual {v1, v7}, Landroid/widget/LinearLayout;->addView(Landroid/view/View;)V

    iget-object v1, p0, Lcom/konka/appassistant/ApplicationList;->mApplicationList:Ljava/util/List;

    invoke-interface {v1, v0}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    :cond_2
    add-int/lit8 v6, v6, 0x1

    goto :goto_2

    :cond_3
    iget-object v1, p0, Lcom/konka/appassistant/ApplicationList;->mAppListView:Landroid/widget/LinearLayout;

    invoke-virtual {v1, v6}, Landroid/widget/LinearLayout;->removeViewAt(I)V

    add-int/lit8 v6, v6, 0x1

    goto :goto_1

    :pswitch_2
    iget-object v1, p0, Lcom/konka/appassistant/ApplicationList;->mUninstallAppList:Ljava/util/List;

    if-eqz v1, :cond_0

    const/4 v6, 0x0

    :goto_3
    iget-object v1, p0, Lcom/konka/appassistant/ApplicationList;->mUninstallAppList:Ljava/util/List;

    invoke-interface {v1}, Ljava/util/List;->size()I

    move-result v1

    if-ge v6, v1, :cond_0

    new-instance v0, Lcom/konka/appassistant/ApplicationInfoView;

    iget-object v1, p0, Lcom/konka/appassistant/ApplicationList;->mContext:Landroid/content/Context;

    iget-object v2, p0, Lcom/konka/appassistant/ApplicationList;->mUninstallAppList:Ljava/util/List;

    invoke-interface {v2, v6}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lcom/konka/appassistant/AppAssistantActivity$AppInfo;

    iget-object v2, v2, Lcom/konka/appassistant/AppAssistantActivity$AppInfo;->info:Landroid/content/pm/ApplicationInfo;

    iget-object v3, p0, Lcom/konka/appassistant/ApplicationList;->mViewType:Lcom/konka/appassistant/ApplicationInfoView$ViewType;

    iget-object v4, p0, Lcom/konka/appassistant/ApplicationList;->mUninstallAppList:Ljava/util/List;

    invoke-interface {v4, v6}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v4

    check-cast v4, Lcom/konka/appassistant/AppAssistantActivity$AppInfo;

    iget-wide v4, v4, Lcom/konka/appassistant/AppAssistantActivity$AppInfo;->appsize:J

    invoke-direct/range {v0 .. v6}, Lcom/konka/appassistant/ApplicationInfoView;-><init>(Landroid/content/Context;Landroid/content/pm/ApplicationInfo;Lcom/konka/appassistant/ApplicationInfoView$ViewType;JI)V

    invoke-virtual {v0}, Lcom/konka/appassistant/ApplicationInfoView;->getAppView()Landroid/view/View;

    move-result-object v7

    if-eqz v7, :cond_4

    iget-object v1, p0, Lcom/konka/appassistant/ApplicationList;->mAppListView:Landroid/widget/LinearLayout;

    invoke-virtual {v1, v7}, Landroid/widget/LinearLayout;->addView(Landroid/view/View;)V

    iget-object v1, p0, Lcom/konka/appassistant/ApplicationList;->mApplicationList:Ljava/util/List;

    invoke-interface {v1, v0}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    :cond_4
    add-int/lit8 v6, v6, 0x1

    goto :goto_3

    nop

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_1
        :pswitch_2
        :pswitch_0
    .end packed-switch
.end method

.method public refreshView(Lcom/konka/appassistant/ApplicationInfoView$ViewType;Ljava/util/List;Ljava/util/List;)V
    .locals 0
    .param p1    # Lcom/konka/appassistant/ApplicationInfoView$ViewType;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/konka/appassistant/ApplicationInfoView$ViewType;",
            "Ljava/util/List",
            "<",
            "Landroid/net/Uri;",
            ">;",
            "Ljava/util/List",
            "<",
            "Lcom/konka/appassistant/AppAssistantActivity$AppInfo;",
            ">;)V"
        }
    .end annotation

    iput-object p1, p0, Lcom/konka/appassistant/ApplicationList;->mViewType:Lcom/konka/appassistant/ApplicationInfoView$ViewType;

    iput-object p2, p0, Lcom/konka/appassistant/ApplicationList;->mInstallAppList:Ljava/util/List;

    iput-object p3, p0, Lcom/konka/appassistant/ApplicationList;->mUninstallAppList:Ljava/util/List;

    invoke-virtual {p0}, Lcom/konka/appassistant/ApplicationList;->initView()V

    return-void
.end method

.method public setInstallList(Ljava/util/List;)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List",
            "<",
            "Landroid/net/Uri;",
            ">;)V"
        }
    .end annotation

    iput-object p1, p0, Lcom/konka/appassistant/ApplicationList;->mInstallAppList:Ljava/util/List;

    return-void
.end method

.method public setTopPageNum(I)V
    .locals 0
    .param p1    # I

    iput p1, p0, Lcom/konka/appassistant/ApplicationList;->TopOfPage:I

    return-void
.end method

.method public setUninstallList(Ljava/util/List;)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List",
            "<",
            "Lcom/konka/appassistant/AppAssistantActivity$AppInfo;",
            ">;)V"
        }
    .end annotation

    iput-object p1, p0, Lcom/konka/appassistant/ApplicationList;->mUninstallAppList:Ljava/util/List;

    return-void
.end method

.method public setViewType(Lcom/konka/appassistant/ApplicationInfoView$ViewType;)V
    .locals 0
    .param p1    # Lcom/konka/appassistant/ApplicationInfoView$ViewType;

    iput-object p1, p0, Lcom/konka/appassistant/ApplicationList;->mViewType:Lcom/konka/appassistant/ApplicationInfoView$ViewType;

    return-void
.end method
