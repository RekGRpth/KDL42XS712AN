.class public Lcom/konka/appassistant/AppAssistantActivity;
.super Landroid/app/Activity;
.source "AppAssistantActivity.java"

# interfaces
.implements Landroid/view/View$OnKeyListener;


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/konka/appassistant/AppAssistantActivity$AppInfo;,
        Lcom/konka/appassistant/AppAssistantActivity$PkgSizeObserver;,
        Lcom/konka/appassistant/AppAssistantActivity$ZipInstallInfo;
    }
.end annotation


# static fields
.field private static synthetic $SWITCH_TABLE$com$konka$appassistant$ApplicationInfoView$ViewType:[I = null

.field private static AllAppList:Ljava/util/List; = null
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Landroid/content/pm/ApplicationInfo;",
            ">;"
        }
    .end annotation
.end field

.field static final HANDLER_FINISH:I = 0x1

.field static final HANDLER_START:I = 0x0

.field static final HANDLER_ZIP_INSTALL_ADD_VIEW:I = 0x68

.field static final HANDLER_ZIP_INSTALL_FIRST_FOUND:I = 0x66

.field static final HANDLER_ZIP_INSTALL_FOUND:I = 0x64

.field static final HANDLER_ZIP_INSTALL_NOTFOUND:I = 0x65

.field static final HANDLER_ZIP_INSTALL_SCROLL_TO_END:I = 0x67

.field private static ScrollViewHeight:I = 0x0

.field static final TAG:Ljava/lang/String; = "AppAssistant"

.field public static mLastFocusedItemNum:I

.field private static final sCollator:Ljava/text/Collator;


# instance fields
.field private InitFlag:Ljava/lang/Boolean;

.field private IsThreadRun:Ljava/lang/Boolean;

.field private cachesize:J

.field private codesize:J

.field private datasize:J

.field private mApplicationList:Lcom/konka/appassistant/ApplicationList;

.field private mDetail:Lcom/konka/appassistant/OneKeyInstallProcess$InstallStateDetail;

.field private mDialog:Landroid/app/AlertDialog;

.field private mInstallComparator:Ljava/util/Comparator;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Comparator",
            "<",
            "Landroid/net/Uri;",
            ">;"
        }
    .end annotation
.end field

.field private mInstallList:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Landroid/net/Uri;",
            ">;"
        }
    .end annotation
.end field

.field private mInstallProcessListener:Lcom/konka/appassistant/InstallProcessListener;

.field private mInstall_button:Landroid/widget/TextView;

.field public final mMainHandler:Landroid/os/Handler;

.field private mOnClickListener:Landroid/view/View$OnClickListener;

.field private mOnFocusChangeListener:Landroid/view/View$OnFocusChangeListener;

.field private mOneKeyInstallProcess:Lcom/konka/appassistant/OneKeyInstallProcess;

.field private mProgressBar:Landroid/widget/ProgressBar;

.field private mProgressBarContainer:Landroid/widget/LinearLayout;

.field private final mReceiver:Landroid/content/BroadcastReceiver;

.field private mRefreshButton:Landroid/widget/Button;

.field private mRefreshFail:Landroid/widget/TextView;

.field private mRefreshFailContainer:Landroid/widget/LinearLayout;

.field private mRefreshOnClickListener:Landroid/view/View$OnClickListener;

.field private mResultList:Landroid/widget/LinearLayout;

.field private mResultNum:Landroid/widget/TextView;

.field private mScrollView:Landroid/widget/ScrollView;

.field private mStorage:Lcom/konka/appassistant/Storage;

.field private mTrustList:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation
.end field

.field private mUninstallComparator:Ljava/util/Comparator;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Comparator",
            "<",
            "Lcom/konka/appassistant/AppAssistantActivity$AppInfo;",
            ">;"
        }
    .end annotation
.end field

.field private mUninstallList:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Lcom/konka/appassistant/AppAssistantActivity$AppInfo;",
            ">;"
        }
    .end annotation
.end field

.field private mUninstall_button:Landroid/widget/TextView;

.field private mViewType:Lcom/konka/appassistant/ApplicationInfoView$ViewType;

.field private mZipAppInfo:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Lcom/konka/appassistant/ApplicationInfoView;",
            ">;"
        }
    .end annotation
.end field

.field private mZipInstallList:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Lcom/konka/appassistant/AppAssistantActivity$ZipInstallInfo;",
            ">;"
        }
    .end annotation
.end field

.field private mZipInstallNum:I

.field private mZipInstallStatus:Ljava/lang/String;

.field private mZipInstall_button:Landroid/widget/TextView;

.field private mZipInstalling:Ljava/lang/Boolean;

.field private pm:Landroid/content/pm/PackageManager;

.field private totalsize:J

.field private uninstallAppMun:I


# direct methods
.method static synthetic $SWITCH_TABLE$com$konka$appassistant$ApplicationInfoView$ViewType()[I
    .locals 3

    sget-object v0, Lcom/konka/appassistant/AppAssistantActivity;->$SWITCH_TABLE$com$konka$appassistant$ApplicationInfoView$ViewType:[I

    if-eqz v0, :cond_0

    :goto_0
    return-object v0

    :cond_0
    invoke-static {}, Lcom/konka/appassistant/ApplicationInfoView$ViewType;->values()[Lcom/konka/appassistant/ApplicationInfoView$ViewType;

    move-result-object v0

    array-length v0, v0

    new-array v0, v0, [I

    :try_start_0
    sget-object v1, Lcom/konka/appassistant/ApplicationInfoView$ViewType;->INSTALL_VIEW:Lcom/konka/appassistant/ApplicationInfoView$ViewType;

    invoke-virtual {v1}, Lcom/konka/appassistant/ApplicationInfoView$ViewType;->ordinal()I

    move-result v1

    const/4 v2, 0x1

    aput v2, v0, v1
    :try_end_0
    .catch Ljava/lang/NoSuchFieldError; {:try_start_0 .. :try_end_0} :catch_2

    :goto_1
    :try_start_1
    sget-object v1, Lcom/konka/appassistant/ApplicationInfoView$ViewType;->UNINSTALL_VIEW:Lcom/konka/appassistant/ApplicationInfoView$ViewType;

    invoke-virtual {v1}, Lcom/konka/appassistant/ApplicationInfoView$ViewType;->ordinal()I

    move-result v1

    const/4 v2, 0x2

    aput v2, v0, v1
    :try_end_1
    .catch Ljava/lang/NoSuchFieldError; {:try_start_1 .. :try_end_1} :catch_1

    :goto_2
    :try_start_2
    sget-object v1, Lcom/konka/appassistant/ApplicationInfoView$ViewType;->ZIP_INSTALL_VIEW:Lcom/konka/appassistant/ApplicationInfoView$ViewType;

    invoke-virtual {v1}, Lcom/konka/appassistant/ApplicationInfoView$ViewType;->ordinal()I

    move-result v1

    const/4 v2, 0x3

    aput v2, v0, v1
    :try_end_2
    .catch Ljava/lang/NoSuchFieldError; {:try_start_2 .. :try_end_2} :catch_0

    :goto_3
    sput-object v0, Lcom/konka/appassistant/AppAssistantActivity;->$SWITCH_TABLE$com$konka$appassistant$ApplicationInfoView$ViewType:[I

    goto :goto_0

    :catch_0
    move-exception v1

    goto :goto_3

    :catch_1
    move-exception v1

    goto :goto_2

    :catch_2
    move-exception v1

    goto :goto_1
.end method

.method static constructor <clinit>()V
    .locals 2

    const/4 v1, 0x0

    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    sput-object v0, Lcom/konka/appassistant/AppAssistantActivity;->AllAppList:Ljava/util/List;

    sput v1, Lcom/konka/appassistant/AppAssistantActivity;->mLastFocusedItemNum:I

    sput v1, Lcom/konka/appassistant/AppAssistantActivity;->ScrollViewHeight:I

    invoke-static {}, Ljava/text/Collator;->getInstance()Ljava/text/Collator;

    move-result-object v0

    sput-object v0, Lcom/konka/appassistant/AppAssistantActivity;->sCollator:Ljava/text/Collator;

    return-void
.end method

.method public constructor <init>()V
    .locals 3

    const/4 v2, 0x0

    const/4 v1, 0x0

    invoke-direct {p0}, Landroid/app/Activity;-><init>()V

    new-instance v0, Lcom/konka/appassistant/Storage;

    invoke-direct {v0}, Lcom/konka/appassistant/Storage;-><init>()V

    iput-object v0, p0, Lcom/konka/appassistant/AppAssistantActivity;->mStorage:Lcom/konka/appassistant/Storage;

    iput-object v2, p0, Lcom/konka/appassistant/AppAssistantActivity;->mTrustList:Ljava/util/List;

    invoke-static {v1}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v0

    iput-object v0, p0, Lcom/konka/appassistant/AppAssistantActivity;->IsThreadRun:Ljava/lang/Boolean;

    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, Lcom/konka/appassistant/AppAssistantActivity;->mZipInstallList:Ljava/util/List;

    iput-object v2, p0, Lcom/konka/appassistant/AppAssistantActivity;->mZipInstallStatus:Ljava/lang/String;

    iput v1, p0, Lcom/konka/appassistant/AppAssistantActivity;->uninstallAppMun:I

    sget-object v0, Lcom/konka/appassistant/ApplicationInfoView$ViewType;->INSTALL_VIEW:Lcom/konka/appassistant/ApplicationInfoView$ViewType;

    iput-object v0, p0, Lcom/konka/appassistant/AppAssistantActivity;->mViewType:Lcom/konka/appassistant/ApplicationInfoView$ViewType;

    iput-object v2, p0, Lcom/konka/appassistant/AppAssistantActivity;->mResultList:Landroid/widget/LinearLayout;

    iput-object v2, p0, Lcom/konka/appassistant/AppAssistantActivity;->mApplicationList:Lcom/konka/appassistant/ApplicationList;

    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, Lcom/konka/appassistant/AppAssistantActivity;->mUninstallList:Ljava/util/List;

    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, Lcom/konka/appassistant/AppAssistantActivity;->mInstallList:Ljava/util/List;

    invoke-static {v1}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v0

    iput-object v0, p0, Lcom/konka/appassistant/AppAssistantActivity;->mZipInstalling:Ljava/lang/Boolean;

    iput v1, p0, Lcom/konka/appassistant/AppAssistantActivity;->mZipInstallNum:I

    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, Lcom/konka/appassistant/AppAssistantActivity;->mZipAppInfo:Ljava/util/List;

    invoke-static {}, Lcom/konka/appassistant/OneKeyInstallProcess;->getInstance()Lcom/konka/appassistant/OneKeyInstallProcess;

    move-result-object v0

    iput-object v0, p0, Lcom/konka/appassistant/AppAssistantActivity;->mOneKeyInstallProcess:Lcom/konka/appassistant/OneKeyInstallProcess;

    new-instance v0, Lcom/konka/appassistant/AppAssistantActivity$1;

    invoke-direct {v0, p0}, Lcom/konka/appassistant/AppAssistantActivity$1;-><init>(Lcom/konka/appassistant/AppAssistantActivity;)V

    iput-object v0, p0, Lcom/konka/appassistant/AppAssistantActivity;->mInstallProcessListener:Lcom/konka/appassistant/InstallProcessListener;

    const/4 v0, 0x1

    invoke-static {v0}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v0

    iput-object v0, p0, Lcom/konka/appassistant/AppAssistantActivity;->InitFlag:Ljava/lang/Boolean;

    new-instance v0, Lcom/konka/appassistant/AppAssistantActivity$2;

    invoke-direct {v0, p0}, Lcom/konka/appassistant/AppAssistantActivity$2;-><init>(Lcom/konka/appassistant/AppAssistantActivity;)V

    iput-object v0, p0, Lcom/konka/appassistant/AppAssistantActivity;->mRefreshOnClickListener:Landroid/view/View$OnClickListener;

    new-instance v0, Lcom/konka/appassistant/AppAssistantActivity$3;

    invoke-direct {v0, p0}, Lcom/konka/appassistant/AppAssistantActivity$3;-><init>(Lcom/konka/appassistant/AppAssistantActivity;)V

    iput-object v0, p0, Lcom/konka/appassistant/AppAssistantActivity;->mOnFocusChangeListener:Landroid/view/View$OnFocusChangeListener;

    new-instance v0, Lcom/konka/appassistant/AppAssistantActivity$4;

    invoke-direct {v0, p0}, Lcom/konka/appassistant/AppAssistantActivity$4;-><init>(Lcom/konka/appassistant/AppAssistantActivity;)V

    iput-object v0, p0, Lcom/konka/appassistant/AppAssistantActivity;->mOnClickListener:Landroid/view/View$OnClickListener;

    new-instance v0, Lcom/konka/appassistant/AppAssistantActivity$5;

    invoke-direct {v0, p0}, Lcom/konka/appassistant/AppAssistantActivity$5;-><init>(Lcom/konka/appassistant/AppAssistantActivity;)V

    iput-object v0, p0, Lcom/konka/appassistant/AppAssistantActivity;->mInstallComparator:Ljava/util/Comparator;

    new-instance v0, Lcom/konka/appassistant/AppAssistantActivity$6;

    invoke-direct {v0, p0}, Lcom/konka/appassistant/AppAssistantActivity$6;-><init>(Lcom/konka/appassistant/AppAssistantActivity;)V

    iput-object v0, p0, Lcom/konka/appassistant/AppAssistantActivity;->mUninstallComparator:Ljava/util/Comparator;

    new-instance v0, Lcom/konka/appassistant/AppAssistantActivity$7;

    invoke-direct {v0, p0}, Lcom/konka/appassistant/AppAssistantActivity$7;-><init>(Lcom/konka/appassistant/AppAssistantActivity;)V

    iput-object v0, p0, Lcom/konka/appassistant/AppAssistantActivity;->mMainHandler:Landroid/os/Handler;

    new-instance v0, Lcom/konka/appassistant/AppAssistantActivity$8;

    invoke-direct {v0, p0}, Lcom/konka/appassistant/AppAssistantActivity$8;-><init>(Lcom/konka/appassistant/AppAssistantActivity;)V

    iput-object v0, p0, Lcom/konka/appassistant/AppAssistantActivity;->mReceiver:Landroid/content/BroadcastReceiver;

    return-void
.end method

.method private ChangeZipInstallStatus(Ljava/lang/String;ILjava/lang/String;)V
    .locals 2
    .param p1    # Ljava/lang/String;
    .param p2    # I
    .param p3    # Ljava/lang/String;

    iget-object v1, p0, Lcom/konka/appassistant/AppAssistantActivity;->mZipInstallList:Ljava/util/List;

    if-nez v1, :cond_1

    :cond_0
    return-void

    :cond_1
    const/4 v0, 0x0

    :goto_0
    iget-object v1, p0, Lcom/konka/appassistant/AppAssistantActivity;->mZipInstallList:Ljava/util/List;

    invoke-interface {v1}, Ljava/util/List;->size()I

    move-result v1

    if-ge v0, v1, :cond_0

    iget-object v1, p0, Lcom/konka/appassistant/AppAssistantActivity;->mZipInstallList:Ljava/util/List;

    invoke-interface {v1, v0}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/konka/appassistant/AppAssistantActivity$ZipInstallInfo;

    iget-object v1, v1, Lcom/konka/appassistant/AppAssistantActivity$ZipInstallInfo;->PackageUri:Landroid/net/Uri;

    invoke-virtual {v1}, Landroid/net/Uri;->getPath()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {p1, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_2

    iget-object v1, p0, Lcom/konka/appassistant/AppAssistantActivity;->mZipInstallList:Ljava/util/List;

    invoke-interface {v1, v0}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/konka/appassistant/AppAssistantActivity$ZipInstallInfo;

    iput p2, v1, Lcom/konka/appassistant/AppAssistantActivity$ZipInstallInfo;->InstallResult:I

    iget-object v1, p0, Lcom/konka/appassistant/AppAssistantActivity;->mZipInstallList:Ljava/util/List;

    invoke-interface {v1, v0}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/konka/appassistant/AppAssistantActivity$ZipInstallInfo;

    iput-object p3, v1, Lcom/konka/appassistant/AppAssistantActivity$ZipInstallInfo;->error:Ljava/lang/String;

    :cond_2
    add-int/lit8 v0, v0, 0x1

    goto :goto_0
.end method

.method public static IsAppInstalled(Ljava/lang/String;)Ljava/lang/Boolean;
    .locals 2
    .param p0    # Ljava/lang/String;

    sget-object v1, Lcom/konka/appassistant/AppAssistantActivity;->AllAppList:Ljava/util/List;

    if-eqz v1, :cond_0

    sget-object v1, Lcom/konka/appassistant/AppAssistantActivity;->AllAppList:Ljava/util/List;

    invoke-interface {v1}, Ljava/util/List;->size()I

    move-result v1

    if-eqz v1, :cond_0

    const/4 v0, 0x0

    :goto_0
    sget-object v1, Lcom/konka/appassistant/AppAssistantActivity;->AllAppList:Ljava/util/List;

    invoke-interface {v1}, Ljava/util/List;->size()I

    move-result v1

    if-lt v0, v1, :cond_1

    :cond_0
    const/4 v1, 0x0

    invoke-static {v1}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v1

    :goto_1
    return-object v1

    :cond_1
    sget-object v1, Lcom/konka/appassistant/AppAssistantActivity;->AllAppList:Ljava/util/List;

    invoke-interface {v1, v0}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Landroid/content/pm/ApplicationInfo;

    iget-object v1, v1, Landroid/content/pm/ApplicationInfo;->packageName:Ljava/lang/String;

    invoke-virtual {p0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_2

    const/4 v1, 0x1

    invoke-static {v1}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v1

    goto :goto_1

    :cond_2
    add-int/lit8 v0, v0, 0x1

    goto :goto_0
.end method

.method private IsTabButtonFocused()Ljava/lang/Boolean;
    .locals 1

    iget-object v0, p0, Lcom/konka/appassistant/AppAssistantActivity;->mInstall_button:Landroid/widget/TextView;

    invoke-virtual {v0}, Landroid/widget/TextView;->isFocused()Z

    move-result v0

    if-nez v0, :cond_0

    iget-object v0, p0, Lcom/konka/appassistant/AppAssistantActivity;->mUninstall_button:Landroid/widget/TextView;

    invoke-virtual {v0}, Landroid/widget/TextView;->isFocused()Z

    move-result v0

    if-nez v0, :cond_0

    iget-object v0, p0, Lcom/konka/appassistant/AppAssistantActivity;->mZipInstall_button:Landroid/widget/TextView;

    invoke-virtual {v0}, Landroid/widget/TextView;->isFocused()Z

    move-result v0

    if-nez v0, :cond_0

    const/4 v0, 0x0

    invoke-static {v0}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v0

    :goto_0
    return-object v0

    :cond_0
    const/4 v0, 0x1

    invoke-static {v0}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v0

    goto :goto_0
.end method

.method private IsTrusted(Ljava/lang/String;)Ljava/lang/Boolean;
    .locals 4
    .param p1    # Ljava/lang/String;

    const/4 v3, 0x0

    iget-object v1, p0, Lcom/konka/appassistant/AppAssistantActivity;->mTrustList:Ljava/util/List;

    if-nez v1, :cond_0

    invoke-static {v3}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v1

    :goto_0
    return-object v1

    :cond_0
    const/4 v0, 0x0

    :goto_1
    iget-object v1, p0, Lcom/konka/appassistant/AppAssistantActivity;->mTrustList:Ljava/util/List;

    invoke-interface {v1}, Ljava/util/List;->size()I

    move-result v1

    if-lt v0, v1, :cond_1

    invoke-static {v3}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v1

    goto :goto_0

    :cond_1
    const-string v2, "1"

    iget-object v1, p0, Lcom/konka/appassistant/AppAssistantActivity;->mTrustList:Ljava/util/List;

    invoke-interface {v1, v0}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Ljava/lang/String;

    invoke-static {v2, v1}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    iget-object v1, p0, Lcom/konka/appassistant/AppAssistantActivity;->mTrustList:Ljava/util/List;

    invoke-interface {v1, v0}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v1

    invoke-virtual {p1, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_2

    const/4 v1, 0x1

    invoke-static {v1}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v1

    goto :goto_0

    :cond_2
    add-int/lit8 v0, v0, 0x1

    goto :goto_1
.end method

.method private OpenDialog()V
    .locals 3

    new-instance v0, Landroid/app/AlertDialog$Builder;

    invoke-direct {v0, p0}, Landroid/app/AlertDialog$Builder;-><init>(Landroid/content/Context;)V

    const v1, 0x7f060021    # com.konka.appassistant.R.string.sureornot

    invoke-virtual {p0, v1}, Lcom/konka/appassistant/AppAssistantActivity;->getString(I)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/app/AlertDialog$Builder;->setTitle(Ljava/lang/CharSequence;)Landroid/app/AlertDialog$Builder;

    const v1, 0x7f060023    # com.konka.appassistant.R.string.m_yes

    invoke-virtual {p0, v1}, Lcom/konka/appassistant/AppAssistantActivity;->getString(I)Ljava/lang/String;

    move-result-object v1

    new-instance v2, Lcom/konka/appassistant/AppAssistantActivity$11;

    invoke-direct {v2, p0}, Lcom/konka/appassistant/AppAssistantActivity$11;-><init>(Lcom/konka/appassistant/AppAssistantActivity;)V

    invoke-virtual {v0, v1, v2}, Landroid/app/AlertDialog$Builder;->setPositiveButton(Ljava/lang/CharSequence;Landroid/content/DialogInterface$OnClickListener;)Landroid/app/AlertDialog$Builder;

    const v1, 0x7f060024    # com.konka.appassistant.R.string.m_return

    invoke-virtual {p0, v1}, Lcom/konka/appassistant/AppAssistantActivity;->getString(I)Ljava/lang/String;

    move-result-object v1

    new-instance v2, Lcom/konka/appassistant/AppAssistantActivity$12;

    invoke-direct {v2, p0}, Lcom/konka/appassistant/AppAssistantActivity$12;-><init>(Lcom/konka/appassistant/AppAssistantActivity;)V

    invoke-virtual {v0, v1, v2}, Landroid/app/AlertDialog$Builder;->setNegativeButton(Ljava/lang/CharSequence;Landroid/content/DialogInterface$OnClickListener;)Landroid/app/AlertDialog$Builder;

    invoke-virtual {v0}, Landroid/app/AlertDialog$Builder;->create()Landroid/app/AlertDialog;

    move-result-object v1

    iput-object v1, p0, Lcom/konka/appassistant/AppAssistantActivity;->mDialog:Landroid/app/AlertDialog;

    iget-object v1, p0, Lcom/konka/appassistant/AppAssistantActivity;->mDialog:Landroid/app/AlertDialog;

    invoke-virtual {v1}, Landroid/app/AlertDialog;->show()V

    return-void
.end method

.method static synthetic access$0(Lcom/konka/appassistant/AppAssistantActivity;)Lcom/konka/appassistant/ApplicationInfoView$ViewType;
    .locals 1

    iget-object v0, p0, Lcom/konka/appassistant/AppAssistantActivity;->mViewType:Lcom/konka/appassistant/ApplicationInfoView$ViewType;

    return-object v0
.end method

.method static synthetic access$1(Lcom/konka/appassistant/AppAssistantActivity;)Ljava/lang/Boolean;
    .locals 1

    iget-object v0, p0, Lcom/konka/appassistant/AppAssistantActivity;->IsThreadRun:Ljava/lang/Boolean;

    return-object v0
.end method

.method static synthetic access$10(Lcom/konka/appassistant/AppAssistantActivity;)V
    .locals 0

    invoke-direct {p0}, Lcom/konka/appassistant/AppAssistantActivity;->tabButtonInit()V

    return-void
.end method

.method static synthetic access$11(Lcom/konka/appassistant/AppAssistantActivity;)Landroid/widget/TextView;
    .locals 1

    iget-object v0, p0, Lcom/konka/appassistant/AppAssistantActivity;->mInstall_button:Landroid/widget/TextView;

    return-object v0
.end method

.method static synthetic access$12(Lcom/konka/appassistant/AppAssistantActivity;Lcom/konka/appassistant/ApplicationInfoView$ViewType;)V
    .locals 0

    iput-object p1, p0, Lcom/konka/appassistant/AppAssistantActivity;->mViewType:Lcom/konka/appassistant/ApplicationInfoView$ViewType;

    return-void
.end method

.method static synthetic access$13(Lcom/konka/appassistant/AppAssistantActivity;)Landroid/widget/TextView;
    .locals 1

    iget-object v0, p0, Lcom/konka/appassistant/AppAssistantActivity;->mUninstall_button:Landroid/widget/TextView;

    return-object v0
.end method

.method static synthetic access$14(Lcom/konka/appassistant/AppAssistantActivity;)Landroid/widget/TextView;
    .locals 1

    iget-object v0, p0, Lcom/konka/appassistant/AppAssistantActivity;->mZipInstall_button:Landroid/widget/TextView;

    return-object v0
.end method

.method static synthetic access$15(Lcom/konka/appassistant/AppAssistantActivity;)Ljava/lang/Boolean;
    .locals 1

    invoke-direct {p0}, Lcom/konka/appassistant/AppAssistantActivity;->IsTabButtonFocused()Ljava/lang/Boolean;

    move-result-object v0

    return-object v0
.end method

.method static synthetic access$16(Lcom/konka/appassistant/AppAssistantActivity;)Ljava/util/List;
    .locals 1

    iget-object v0, p0, Lcom/konka/appassistant/AppAssistantActivity;->mUninstallList:Ljava/util/List;

    return-object v0
.end method

.method static synthetic access$17(Lcom/konka/appassistant/AppAssistantActivity;)Ljava/util/List;
    .locals 1

    iget-object v0, p0, Lcom/konka/appassistant/AppAssistantActivity;->mInstallList:Ljava/util/List;

    return-object v0
.end method

.method static synthetic access$18(Lcom/konka/appassistant/AppAssistantActivity;Ljava/util/List;)V
    .locals 0

    iput-object p1, p0, Lcom/konka/appassistant/AppAssistantActivity;->mInstallList:Ljava/util/List;

    return-void
.end method

.method static synthetic access$19(Lcom/konka/appassistant/AppAssistantActivity;)Landroid/content/pm/PackageManager;
    .locals 1

    iget-object v0, p0, Lcom/konka/appassistant/AppAssistantActivity;->pm:Landroid/content/pm/PackageManager;

    return-object v0
.end method

.method static synthetic access$2(Lcom/konka/appassistant/AppAssistantActivity;)Landroid/widget/LinearLayout;
    .locals 1

    iget-object v0, p0, Lcom/konka/appassistant/AppAssistantActivity;->mProgressBarContainer:Landroid/widget/LinearLayout;

    return-object v0
.end method

.method static synthetic access$20()Ljava/text/Collator;
    .locals 1

    sget-object v0, Lcom/konka/appassistant/AppAssistantActivity;->sCollator:Ljava/text/Collator;

    return-object v0
.end method

.method static synthetic access$21(Lcom/konka/appassistant/AppAssistantActivity;Lcom/konka/appassistant/OneKeyInstallProcess$InstallStateDetail;)V
    .locals 0

    iput-object p1, p0, Lcom/konka/appassistant/AppAssistantActivity;->mDetail:Lcom/konka/appassistant/OneKeyInstallProcess$InstallStateDetail;

    return-void
.end method

.method static synthetic access$22(Lcom/konka/appassistant/AppAssistantActivity;)V
    .locals 0

    invoke-direct {p0}, Lcom/konka/appassistant/AppAssistantActivity;->showViewList()V

    return-void
.end method

.method static synthetic access$23(Lcom/konka/appassistant/AppAssistantActivity;)Lcom/konka/appassistant/OneKeyInstallProcess$InstallStateDetail;
    .locals 1

    iget-object v0, p0, Lcom/konka/appassistant/AppAssistantActivity;->mDetail:Lcom/konka/appassistant/OneKeyInstallProcess$InstallStateDetail;

    return-object v0
.end method

.method static synthetic access$24(Lcom/konka/appassistant/AppAssistantActivity;Ljava/lang/String;)V
    .locals 0

    iput-object p1, p0, Lcom/konka/appassistant/AppAssistantActivity;->mZipInstallStatus:Ljava/lang/String;

    return-void
.end method

.method static synthetic access$25(Lcom/konka/appassistant/AppAssistantActivity;)Landroid/widget/ScrollView;
    .locals 1

    iget-object v0, p0, Lcom/konka/appassistant/AppAssistantActivity;->mScrollView:Landroid/widget/ScrollView;

    return-object v0
.end method

.method static synthetic access$26(Lcom/konka/appassistant/AppAssistantActivity;)Landroid/widget/ProgressBar;
    .locals 1

    iget-object v0, p0, Lcom/konka/appassistant/AppAssistantActivity;->mProgressBar:Landroid/widget/ProgressBar;

    return-object v0
.end method

.method static synthetic access$27(Lcom/konka/appassistant/AppAssistantActivity;)Landroid/widget/TextView;
    .locals 1

    iget-object v0, p0, Lcom/konka/appassistant/AppAssistantActivity;->mResultNum:Landroid/widget/TextView;

    return-object v0
.end method

.method static synthetic access$28(Lcom/konka/appassistant/AppAssistantActivity;)Ljava/lang/String;
    .locals 1

    iget-object v0, p0, Lcom/konka/appassistant/AppAssistantActivity;->mZipInstallStatus:Ljava/lang/String;

    return-object v0
.end method

.method static synthetic access$29(Lcom/konka/appassistant/AppAssistantActivity;)Landroid/widget/LinearLayout;
    .locals 1

    iget-object v0, p0, Lcom/konka/appassistant/AppAssistantActivity;->mRefreshFailContainer:Landroid/widget/LinearLayout;

    return-object v0
.end method

.method static synthetic access$3(Lcom/konka/appassistant/AppAssistantActivity;)Ljava/lang/Boolean;
    .locals 1

    iget-object v0, p0, Lcom/konka/appassistant/AppAssistantActivity;->mZipInstalling:Ljava/lang/Boolean;

    return-object v0
.end method

.method static synthetic access$30(Lcom/konka/appassistant/AppAssistantActivity;)Landroid/widget/TextView;
    .locals 1

    iget-object v0, p0, Lcom/konka/appassistant/AppAssistantActivity;->mRefreshFail:Landroid/widget/TextView;

    return-object v0
.end method

.method static synthetic access$31(Lcom/konka/appassistant/AppAssistantActivity;)Lcom/konka/appassistant/ApplicationList;
    .locals 1

    iget-object v0, p0, Lcom/konka/appassistant/AppAssistantActivity;->mApplicationList:Lcom/konka/appassistant/ApplicationList;

    return-object v0
.end method

.method static synthetic access$32(Lcom/konka/appassistant/AppAssistantActivity;I)I
    .locals 1

    invoke-direct {p0, p1}, Lcom/konka/appassistant/AppAssistantActivity;->getExplanationFromErrorCode(I)I

    move-result v0

    return v0
.end method

.method static synthetic access$33(Lcom/konka/appassistant/AppAssistantActivity;Ljava/lang/String;ILjava/lang/String;)V
    .locals 0

    invoke-direct {p0, p1, p2, p3}, Lcom/konka/appassistant/AppAssistantActivity;->ChangeZipInstallStatus(Ljava/lang/String;ILjava/lang/String;)V

    return-void
.end method

.method static synthetic access$34(Lcom/konka/appassistant/AppAssistantActivity;)I
    .locals 1

    iget v0, p0, Lcom/konka/appassistant/AppAssistantActivity;->mZipInstallNum:I

    return v0
.end method

.method static synthetic access$35(Lcom/konka/appassistant/AppAssistantActivity;J)V
    .locals 0

    iput-wide p1, p0, Lcom/konka/appassistant/AppAssistantActivity;->cachesize:J

    return-void
.end method

.method static synthetic access$36(Lcom/konka/appassistant/AppAssistantActivity;J)V
    .locals 0

    iput-wide p1, p0, Lcom/konka/appassistant/AppAssistantActivity;->datasize:J

    return-void
.end method

.method static synthetic access$37(Lcom/konka/appassistant/AppAssistantActivity;J)V
    .locals 0

    iput-wide p1, p0, Lcom/konka/appassistant/AppAssistantActivity;->codesize:J

    return-void
.end method

.method static synthetic access$38(Lcom/konka/appassistant/AppAssistantActivity;)J
    .locals 2

    iget-wide v0, p0, Lcom/konka/appassistant/AppAssistantActivity;->cachesize:J

    return-wide v0
.end method

.method static synthetic access$39(Lcom/konka/appassistant/AppAssistantActivity;)J
    .locals 2

    iget-wide v0, p0, Lcom/konka/appassistant/AppAssistantActivity;->datasize:J

    return-wide v0
.end method

.method static synthetic access$4(Lcom/konka/appassistant/AppAssistantActivity;I)V
    .locals 0

    iput p1, p0, Lcom/konka/appassistant/AppAssistantActivity;->mZipInstallNum:I

    return-void
.end method

.method static synthetic access$40(Lcom/konka/appassistant/AppAssistantActivity;)J
    .locals 2

    iget-wide v0, p0, Lcom/konka/appassistant/AppAssistantActivity;->codesize:J

    return-wide v0
.end method

.method static synthetic access$41(Lcom/konka/appassistant/AppAssistantActivity;J)V
    .locals 0

    iput-wide p1, p0, Lcom/konka/appassistant/AppAssistantActivity;->totalsize:J

    return-void
.end method

.method static synthetic access$42(Lcom/konka/appassistant/AppAssistantActivity;)J
    .locals 2

    iget-wide v0, p0, Lcom/konka/appassistant/AppAssistantActivity;->totalsize:J

    return-wide v0
.end method

.method static synthetic access$43(Lcom/konka/appassistant/AppAssistantActivity;Ljava/lang/String;J)V
    .locals 0

    invoke-direct {p0, p1, p2, p3}, Lcom/konka/appassistant/AppAssistantActivity;->setAppSize(Ljava/lang/String;J)V

    return-void
.end method

.method static synthetic access$45(Lcom/konka/appassistant/AppAssistantActivity;Ljava/lang/Boolean;)V
    .locals 0

    iput-object p1, p0, Lcom/konka/appassistant/AppAssistantActivity;->IsThreadRun:Ljava/lang/Boolean;

    return-void
.end method

.method static synthetic access$46(Lcom/konka/appassistant/AppAssistantActivity;)Ljava/util/Comparator;
    .locals 1

    iget-object v0, p0, Lcom/konka/appassistant/AppAssistantActivity;->mInstallComparator:Ljava/util/Comparator;

    return-object v0
.end method

.method static synthetic access$5(Lcom/konka/appassistant/AppAssistantActivity;)Ljava/util/List;
    .locals 1

    iget-object v0, p0, Lcom/konka/appassistant/AppAssistantActivity;->mZipInstallList:Ljava/util/List;

    return-object v0
.end method

.method static synthetic access$6(Lcom/konka/appassistant/AppAssistantActivity;)Ljava/util/List;
    .locals 1

    iget-object v0, p0, Lcom/konka/appassistant/AppAssistantActivity;->mZipAppInfo:Ljava/util/List;

    return-object v0
.end method

.method static synthetic access$7(Lcom/konka/appassistant/AppAssistantActivity;)V
    .locals 0

    invoke-direct {p0}, Lcom/konka/appassistant/AppAssistantActivity;->refreshView()V

    return-void
.end method

.method static synthetic access$8(Lcom/konka/appassistant/AppAssistantActivity;)Lcom/konka/appassistant/OneKeyInstallProcess;
    .locals 1

    iget-object v0, p0, Lcom/konka/appassistant/AppAssistantActivity;->mOneKeyInstallProcess:Lcom/konka/appassistant/OneKeyInstallProcess;

    return-object v0
.end method

.method static synthetic access$9(Lcom/konka/appassistant/AppAssistantActivity;Ljava/lang/Boolean;)V
    .locals 0

    iput-object p1, p0, Lcom/konka/appassistant/AppAssistantActivity;->mZipInstalling:Ljava/lang/Boolean;

    return-void
.end method

.method private getExplanationFromErrorCode(I)I
    .locals 1
    .param p1    # I

    sparse-switch p1, :sswitch_data_0

    const v0, 0x7f060010    # com.konka.appassistant.R.string.app_assistant_zipinstall_failed

    :goto_0
    return v0

    :sswitch_0
    const v0, 0x7f06001d    # com.konka.appassistant.R.string.install_failed_invalid_apk

    goto :goto_0

    :sswitch_1
    const v0, 0x7f06001e    # com.konka.appassistant.R.string.install_failed_inconsistent_certificates

    goto :goto_0

    :sswitch_2
    const v0, 0x7f06001f    # com.konka.appassistant.R.string.install_failed_older_sdk

    goto :goto_0

    :sswitch_3
    const v0, 0x7f060020    # com.konka.appassistant.R.string.install_failed_cpu_abi_incompatible

    goto :goto_0

    :sswitch_4
    const v0, 0x7f06001c    # com.konka.appassistant.R.string.install_failed_storage_not_enough

    goto :goto_0

    nop

    :sswitch_data_0
    .sparse-switch
        -0x68 -> :sswitch_1
        -0x10 -> :sswitch_3
        -0xc -> :sswitch_2
        -0x4 -> :sswitch_4
        -0x2 -> :sswitch_0
    .end sparse-switch
.end method

.method public static getScrollViewHeight()I
    .locals 1

    sget v0, Lcom/konka/appassistant/AppAssistantActivity;->ScrollViewHeight:I

    return v0
.end method

.method private initInstallBroadcastReceiver()V
    .locals 2

    new-instance v0, Landroid/content/IntentFilter;

    invoke-direct {v0}, Landroid/content/IntentFilter;-><init>()V

    const-string v1, "android.intent.action.MEDIA_MOUNTED"

    invoke-virtual {v0, v1}, Landroid/content/IntentFilter;->addAction(Ljava/lang/String;)V

    const-string v1, "android.intent.action.MEDIA_REMOVED"

    invoke-virtual {v0, v1}, Landroid/content/IntentFilter;->addAction(Ljava/lang/String;)V

    const-string v1, "android.intent.action.MEDIA_BAD_REMOVAL"

    invoke-virtual {v0, v1}, Landroid/content/IntentFilter;->addAction(Ljava/lang/String;)V

    const-string v1, "android.intent.action.MEDIA_UNMOUNTED"

    invoke-virtual {v0, v1}, Landroid/content/IntentFilter;->addAction(Ljava/lang/String;)V

    const-string v1, "file"

    invoke-virtual {v0, v1}, Landroid/content/IntentFilter;->addDataScheme(Ljava/lang/String;)V

    iget-object v1, p0, Lcom/konka/appassistant/AppAssistantActivity;->mReceiver:Landroid/content/BroadcastReceiver;

    invoke-virtual {p0, v1, v0}, Lcom/konka/appassistant/AppAssistantActivity;->registerReceiver(Landroid/content/BroadcastReceiver;Landroid/content/IntentFilter;)Landroid/content/Intent;

    return-void
.end method

.method private initTrustedList()Ljava/util/List;
    .locals 12
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/List",
            "<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation

    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    :try_start_0
    new-instance v2, Ljava/io/RandomAccessFile;

    const-string v8, "/data/misc/konka/konka_mguard_trusted.dat"

    const-string v9, "r"

    invoke-direct {v2, v8, v9}, Ljava/io/RandomAccessFile;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    invoke-virtual {v2}, Ljava/io/RandomAccessFile;->length()J

    move-result-wide v5

    const-wide/16 v3, 0x0

    :goto_0
    cmp-long v8, v3, v5

    if-ltz v8, :cond_0

    :goto_1
    return-object v0

    :cond_0
    invoke-virtual {v2}, Ljava/io/RandomAccessFile;->readLine()Ljava/lang/String;

    move-result-object v7

    invoke-virtual {v7}, Ljava/lang/String;->length()I

    move-result v8

    int-to-long v8, v8

    add-long/2addr v8, v3

    const-wide/16 v10, 0x1

    add-long v3, v8, v10

    invoke-virtual {v2, v3, v4}, Ljava/io/RandomAccessFile;->seek(J)V

    invoke-virtual {v0, v7}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z
    :try_end_0
    .catch Ljava/io/FileNotFoundException; {:try_start_0 .. :try_end_0} :catch_0
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_1

    goto :goto_0

    :catch_0
    move-exception v1

    invoke-virtual {v1}, Ljava/io/FileNotFoundException;->printStackTrace()V

    goto :goto_1

    :catch_1
    move-exception v1

    invoke-virtual {v1}, Ljava/io/IOException;->printStackTrace()V

    goto :goto_1
.end method

.method private refreshView()V
    .locals 11

    const/4 v10, 0x0

    const/4 v8, 0x1

    invoke-direct {p0}, Lcom/konka/appassistant/AppAssistantActivity;->reset()V

    invoke-static {}, Lcom/konka/appassistant/AppAssistantActivity;->$SWITCH_TABLE$com$konka$appassistant$ApplicationInfoView$ViewType()[I

    move-result-object v6

    iget-object v7, p0, Lcom/konka/appassistant/AppAssistantActivity;->mViewType:Lcom/konka/appassistant/ApplicationInfoView$ViewType;

    invoke-virtual {v7}, Lcom/konka/appassistant/ApplicationInfoView$ViewType;->ordinal()I

    move-result v7

    aget v6, v6, v7

    packed-switch v6, :pswitch_data_0

    :cond_0
    :goto_0
    return-void

    :pswitch_0
    iget-object v6, p0, Lcom/konka/appassistant/AppAssistantActivity;->mInstallList:Ljava/util/List;

    if-eqz v6, :cond_1

    iget-object v6, p0, Lcom/konka/appassistant/AppAssistantActivity;->mInstallList:Ljava/util/List;

    invoke-interface {v6}, Ljava/util/List;->size()I

    move-result v6

    if-nez v6, :cond_1

    new-instance v6, Ljava/lang/Thread;

    new-instance v7, Lcom/konka/appassistant/AppAssistantActivity$10;

    invoke-direct {v7, p0}, Lcom/konka/appassistant/AppAssistantActivity$10;-><init>(Lcom/konka/appassistant/AppAssistantActivity;)V

    invoke-direct {v6, v7}, Ljava/lang/Thread;-><init>(Ljava/lang/Runnable;)V

    invoke-virtual {v6}, Ljava/lang/Thread;->start()V

    goto :goto_0

    :cond_1
    iget-object v6, p0, Lcom/konka/appassistant/AppAssistantActivity;->mMainHandler:Landroid/os/Handler;

    invoke-virtual {v6, v8}, Landroid/os/Handler;->sendEmptyMessage(I)Z

    goto :goto_0

    :pswitch_1
    iget-object v6, p0, Lcom/konka/appassistant/AppAssistantActivity;->mUninstallList:Ljava/util/List;

    if-eqz v6, :cond_5

    iget-object v6, p0, Lcom/konka/appassistant/AppAssistantActivity;->mUninstallList:Ljava/util/List;

    invoke-interface {v6}, Ljava/util/List;->size()I

    move-result v6

    if-nez v6, :cond_5

    sget-object v6, Lcom/konka/appassistant/AppAssistantActivity;->AllAppList:Ljava/util/List;

    if-eqz v6, :cond_0

    const/4 v2, 0x0

    :goto_1
    sget-object v6, Lcom/konka/appassistant/AppAssistantActivity;->AllAppList:Ljava/util/List;

    invoke-interface {v6}, Ljava/util/List;->size()I

    move-result v6

    if-lt v2, v6, :cond_3

    iget-object v6, p0, Lcom/konka/appassistant/AppAssistantActivity;->mUninstallList:Ljava/util/List;

    iget-object v7, p0, Lcom/konka/appassistant/AppAssistantActivity;->mUninstallComparator:Ljava/util/Comparator;

    invoke-static {v6, v7}, Ljava/util/Collections;->sort(Ljava/util/List;Ljava/util/Comparator;)V

    iget-object v6, p0, Lcom/konka/appassistant/AppAssistantActivity;->mUninstallList:Ljava/util/List;

    invoke-interface {v6}, Ljava/util/List;->size()I

    move-result v6

    if-nez v6, :cond_2

    iget-object v6, p0, Lcom/konka/appassistant/AppAssistantActivity;->mMainHandler:Landroid/os/Handler;

    invoke-virtual {v6, v8}, Landroid/os/Handler;->sendEmptyMessage(I)Z

    :cond_2
    const/4 v2, 0x0

    :goto_2
    iget-object v6, p0, Lcom/konka/appassistant/AppAssistantActivity;->mUninstallList:Ljava/util/List;

    invoke-interface {v6}, Ljava/util/List;->size()I

    move-result v6

    if-ge v2, v6, :cond_0

    :try_start_0
    iget-object v6, p0, Lcom/konka/appassistant/AppAssistantActivity;->mUninstallList:Ljava/util/List;

    invoke-interface {v6, v2}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v6

    check-cast v6, Lcom/konka/appassistant/AppAssistantActivity$AppInfo;

    iget-object v6, v6, Lcom/konka/appassistant/AppAssistantActivity$AppInfo;->info:Landroid/content/pm/ApplicationInfo;

    iget-object v6, v6, Landroid/content/pm/ApplicationInfo;->packageName:Ljava/lang/String;

    invoke-virtual {p0, v6}, Lcom/konka/appassistant/AppAssistantActivity;->queryPacakgeSize(Ljava/lang/String;)V
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    :goto_3
    iget-object v6, p0, Lcom/konka/appassistant/AppAssistantActivity;->mUninstallList:Ljava/util/List;

    invoke-interface {v6, v2}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v6

    check-cast v6, Lcom/konka/appassistant/AppAssistantActivity$AppInfo;

    iget-wide v7, p0, Lcom/konka/appassistant/AppAssistantActivity;->totalsize:J

    iput-wide v7, v6, Lcom/konka/appassistant/AppAssistantActivity$AppInfo;->appsize:J

    add-int/lit8 v2, v2, 0x1

    goto :goto_2

    :cond_3
    sget-object v6, Lcom/konka/appassistant/AppAssistantActivity;->AllAppList:Ljava/util/List;

    invoke-interface {v6, v2}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v6

    check-cast v6, Landroid/content/pm/ApplicationInfo;

    iget v6, v6, Landroid/content/pm/ApplicationInfo;->flags:I

    and-int/lit8 v6, v6, 0x1

    if-nez v6, :cond_4

    new-instance v0, Lcom/konka/appassistant/AppAssistantActivity$AppInfo;

    invoke-direct {v0, p0}, Lcom/konka/appassistant/AppAssistantActivity$AppInfo;-><init>(Lcom/konka/appassistant/AppAssistantActivity;)V

    sget-object v6, Lcom/konka/appassistant/AppAssistantActivity;->AllAppList:Ljava/util/List;

    invoke-interface {v6, v2}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v6

    check-cast v6, Landroid/content/pm/ApplicationInfo;

    iput-object v6, v0, Lcom/konka/appassistant/AppAssistantActivity$AppInfo;->info:Landroid/content/pm/ApplicationInfo;

    iget-object v6, v0, Lcom/konka/appassistant/AppAssistantActivity$AppInfo;->info:Landroid/content/pm/ApplicationInfo;

    iget-object v6, v6, Landroid/content/pm/ApplicationInfo;->packageName:Ljava/lang/String;

    invoke-direct {p0, v6}, Lcom/konka/appassistant/AppAssistantActivity;->IsTrusted(Ljava/lang/String;)Ljava/lang/Boolean;

    move-result-object v6

    invoke-virtual {v6}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v6

    if-nez v6, :cond_4

    iget-object v6, p0, Lcom/konka/appassistant/AppAssistantActivity;->mUninstallList:Ljava/util/List;

    invoke-interface {v6, v0}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    :cond_4
    add-int/lit8 v2, v2, 0x1

    goto :goto_1

    :catch_0
    move-exception v1

    invoke-virtual {v1}, Ljava/lang/Exception;->printStackTrace()V

    goto :goto_3

    :cond_5
    iget-object v6, p0, Lcom/konka/appassistant/AppAssistantActivity;->mMainHandler:Landroid/os/Handler;

    invoke-virtual {v6, v8}, Landroid/os/Handler;->sendEmptyMessage(I)Z

    goto/16 :goto_0

    :pswitch_2
    iget-object v6, p0, Lcom/konka/appassistant/AppAssistantActivity;->mProgressBarContainer:Landroid/widget/LinearLayout;

    const/16 v7, 0x8

    invoke-virtual {v6, v7}, Landroid/widget/LinearLayout;->setVisibility(I)V

    iget-object v6, p0, Lcom/konka/appassistant/AppAssistantActivity;->mScrollView:Landroid/widget/ScrollView;

    invoke-virtual {v6, v10}, Landroid/widget/ScrollView;->setVisibility(I)V

    iget-object v6, p0, Lcom/konka/appassistant/AppAssistantActivity;->mRefreshButton:Landroid/widget/Button;

    const v7, 0x7f06000d    # com.konka.appassistant.R.string.app_assistant_zipinstall_startinstall

    invoke-virtual {p0, v7}, Lcom/konka/appassistant/AppAssistantActivity;->getString(I)Ljava/lang/String;

    move-result-object v7

    invoke-virtual {v6, v7}, Landroid/widget/Button;->setText(Ljava/lang/CharSequence;)V

    iget-object v6, p0, Lcom/konka/appassistant/AppAssistantActivity;->mApplicationList:Lcom/konka/appassistant/ApplicationList;

    iget-object v7, p0, Lcom/konka/appassistant/AppAssistantActivity;->mViewType:Lcom/konka/appassistant/ApplicationInfoView$ViewType;

    invoke-virtual {v6, v7}, Lcom/konka/appassistant/ApplicationList;->setViewType(Lcom/konka/appassistant/ApplicationInfoView$ViewType;)V

    iget-object v6, p0, Lcom/konka/appassistant/AppAssistantActivity;->mApplicationList:Lcom/konka/appassistant/ApplicationList;

    invoke-virtual {v6}, Lcom/konka/appassistant/ApplicationList;->initView()V

    iget-object v6, p0, Lcom/konka/appassistant/AppAssistantActivity;->mZipInstalling:Ljava/lang/Boolean;

    invoke-virtual {v6}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v6

    if-nez v6, :cond_8

    const/4 v2, 0x0

    :goto_4
    iget-object v6, p0, Lcom/konka/appassistant/AppAssistantActivity;->mZipAppInfo:Ljava/util/List;

    invoke-interface {v6}, Ljava/util/List;->size()I

    move-result v6

    if-lt v2, v6, :cond_7

    :cond_6
    iget-object v6, p0, Lcom/konka/appassistant/AppAssistantActivity;->mResultNum:Landroid/widget/TextView;

    invoke-virtual {v6, v10}, Landroid/widget/TextView;->setVisibility(I)V

    iget-object v6, p0, Lcom/konka/appassistant/AppAssistantActivity;->mResultNum:Landroid/widget/TextView;

    iget-object v7, p0, Lcom/konka/appassistant/AppAssistantActivity;->mZipInstallStatus:Ljava/lang/String;

    invoke-virtual {v6, v7}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    goto/16 :goto_0

    :cond_7
    iget-object v6, p0, Lcom/konka/appassistant/AppAssistantActivity;->mZipAppInfo:Ljava/util/List;

    invoke-interface {v6, v2}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Lcom/konka/appassistant/ApplicationInfoView;

    iget-object v6, p0, Lcom/konka/appassistant/AppAssistantActivity;->mApplicationList:Lcom/konka/appassistant/ApplicationList;

    invoke-virtual {v6, v3}, Lcom/konka/appassistant/ApplicationList;->addToApplicationList(Lcom/konka/appassistant/ApplicationInfoView;)V

    iget-object v6, p0, Lcom/konka/appassistant/AppAssistantActivity;->mZipInstallList:Ljava/util/List;

    invoke-interface {v6, v2}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v6

    check-cast v6, Lcom/konka/appassistant/AppAssistantActivity$ZipInstallInfo;

    iget v7, v6, Lcom/konka/appassistant/AppAssistantActivity$ZipInstallInfo;->InstallResult:I

    iget-object v6, p0, Lcom/konka/appassistant/AppAssistantActivity;->mZipInstallList:Ljava/util/List;

    invoke-interface {v6, v2}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v6

    check-cast v6, Lcom/konka/appassistant/AppAssistantActivity$ZipInstallInfo;

    iget-object v6, v6, Lcom/konka/appassistant/AppAssistantActivity$ZipInstallInfo;->error:Ljava/lang/String;

    invoke-virtual {v3, v7, v6}, Lcom/konka/appassistant/ApplicationInfoView;->ShowZipApkInstallResult(ILjava/lang/String;)V

    invoke-virtual {v3}, Lcom/konka/appassistant/ApplicationInfoView;->getAppView()Landroid/view/View;

    move-result-object v5

    iget-object v6, p0, Lcom/konka/appassistant/AppAssistantActivity;->mResultList:Landroid/widget/LinearLayout;

    invoke-virtual {v6, v5}, Landroid/widget/LinearLayout;->addView(Landroid/view/View;)V

    add-int/lit8 v2, v2, 0x1

    goto :goto_4

    :cond_8
    iget-object v6, p0, Lcom/konka/appassistant/AppAssistantActivity;->mZipInstallList:Ljava/util/List;

    invoke-interface {v6}, Ljava/util/List;->size()I

    move-result v6

    if-eqz v6, :cond_6

    const/4 v2, 0x0

    :goto_5
    iget-object v6, p0, Lcom/konka/appassistant/AppAssistantActivity;->mZipInstallList:Ljava/util/List;

    invoke-interface {v6}, Ljava/util/List;->size()I

    move-result v6

    if-ge v2, v6, :cond_6

    iget-object v6, p0, Lcom/konka/appassistant/AppAssistantActivity;->mZipInstallList:Ljava/util/List;

    invoke-interface {v6, v2}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v6

    check-cast v6, Lcom/konka/appassistant/AppAssistantActivity$ZipInstallInfo;

    iget-object v4, v6, Lcom/konka/appassistant/AppAssistantActivity$ZipInstallInfo;->PackageUri:Landroid/net/Uri;

    iget-object v6, p0, Lcom/konka/appassistant/AppAssistantActivity;->mInstallList:Ljava/util/List;

    invoke-interface {v6, v4}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    iget-object v6, p0, Lcom/konka/appassistant/AppAssistantActivity;->mApplicationList:Lcom/konka/appassistant/ApplicationList;

    sget-object v7, Lcom/konka/appassistant/ApplicationInfoView$ViewType;->ZIP_INSTALL_VIEW:Lcom/konka/appassistant/ApplicationInfoView$ViewType;

    const/4 v8, 0x0

    invoke-virtual {v6, v7, v4, v8}, Lcom/konka/appassistant/ApplicationList;->AddView(Lcom/konka/appassistant/ApplicationInfoView$ViewType;Landroid/net/Uri;Lcom/konka/appassistant/AppAssistantActivity$AppInfo;)V

    iget-object v7, p0, Lcom/konka/appassistant/AppAssistantActivity;->mApplicationList:Lcom/konka/appassistant/ApplicationList;

    invoke-virtual {v4}, Landroid/net/Uri;->getPath()Ljava/lang/String;

    move-result-object v8

    iget-object v6, p0, Lcom/konka/appassistant/AppAssistantActivity;->mZipInstallList:Ljava/util/List;

    invoke-interface {v6, v2}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v6

    check-cast v6, Lcom/konka/appassistant/AppAssistantActivity$ZipInstallInfo;

    iget v9, v6, Lcom/konka/appassistant/AppAssistantActivity$ZipInstallInfo;->InstallResult:I

    iget-object v6, p0, Lcom/konka/appassistant/AppAssistantActivity;->mZipInstallList:Ljava/util/List;

    invoke-interface {v6, v2}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v6

    check-cast v6, Lcom/konka/appassistant/AppAssistantActivity$ZipInstallInfo;

    iget-object v6, v6, Lcom/konka/appassistant/AppAssistantActivity$ZipInstallInfo;->error:Ljava/lang/String;

    invoke-virtual {v7, v8, v9, v6}, Lcom/konka/appassistant/ApplicationList;->ShowZipApkInstallResult(Ljava/lang/String;ILjava/lang/String;)V

    add-int/lit8 v2, v2, 0x1

    goto :goto_5

    nop

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
        :pswitch_1
        :pswitch_2
    .end packed-switch
.end method

.method private reset()V
    .locals 4

    const/16 v3, 0x8

    const/4 v2, 0x0

    iget-object v0, p0, Lcom/konka/appassistant/AppAssistantActivity;->mResultList:Landroid/widget/LinearLayout;

    invoke-virtual {v0}, Landroid/widget/LinearLayout;->removeAllViews()V

    iput v2, p0, Lcom/konka/appassistant/AppAssistantActivity;->uninstallAppMun:I

    iget-object v0, p0, Lcom/konka/appassistant/AppAssistantActivity;->mResultNum:Landroid/widget/TextView;

    const/4 v1, 0x4

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setVisibility(I)V

    iget-object v0, p0, Lcom/konka/appassistant/AppAssistantActivity;->mProgressBar:Landroid/widget/ProgressBar;

    invoke-virtual {v0, v2}, Landroid/widget/ProgressBar;->setVisibility(I)V

    iget-object v0, p0, Lcom/konka/appassistant/AppAssistantActivity;->mRefreshFailContainer:Landroid/widget/LinearLayout;

    invoke-virtual {v0, v3}, Landroid/widget/LinearLayout;->setVisibility(I)V

    iget-object v0, p0, Lcom/konka/appassistant/AppAssistantActivity;->mScrollView:Landroid/widget/ScrollView;

    invoke-virtual {v0, v3}, Landroid/widget/ScrollView;->setVisibility(I)V

    iget-object v0, p0, Lcom/konka/appassistant/AppAssistantActivity;->mScrollView:Landroid/widget/ScrollView;

    invoke-virtual {v0, v2, v2}, Landroid/widget/ScrollView;->scrollTo(II)V

    iget-object v0, p0, Lcom/konka/appassistant/AppAssistantActivity;->mApplicationList:Lcom/konka/appassistant/ApplicationList;

    invoke-virtual {v0}, Lcom/konka/appassistant/ApplicationList;->initTopOfPage()V

    iget-object v0, p0, Lcom/konka/appassistant/AppAssistantActivity;->mProgressBarContainer:Landroid/widget/LinearLayout;

    invoke-virtual {v0, v2}, Landroid/widget/LinearLayout;->setVisibility(I)V

    iget-object v0, p0, Lcom/konka/appassistant/AppAssistantActivity;->mRefreshButton:Landroid/widget/Button;

    const v1, 0x7f06000c    # com.konka.appassistant.R.string.app_assistant_second_menu_refresh

    invoke-virtual {p0, v1}, Lcom/konka/appassistant/AppAssistantActivity;->getString(I)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/widget/Button;->setText(Ljava/lang/CharSequence;)V

    sput v2, Lcom/konka/appassistant/AppAssistantActivity;->mLastFocusedItemNum:I

    iget-object v0, p0, Lcom/konka/appassistant/AppAssistantActivity;->pm:Landroid/content/pm/PackageManager;

    const/16 v1, 0x2000

    invoke-virtual {v0, v1}, Landroid/content/pm/PackageManager;->getInstalledApplications(I)Ljava/util/List;

    move-result-object v0

    sput-object v0, Lcom/konka/appassistant/AppAssistantActivity;->AllAppList:Ljava/util/List;

    return-void
.end method

.method private setAppSize(Ljava/lang/String;J)V
    .locals 3
    .param p1    # Ljava/lang/String;
    .param p2    # J

    const/4 v0, 0x0

    :goto_0
    iget-object v1, p0, Lcom/konka/appassistant/AppAssistantActivity;->mUninstallList:Ljava/util/List;

    invoke-interface {v1}, Ljava/util/List;->size()I

    move-result v1

    if-lt v0, v1, :cond_1

    :goto_1
    iget v1, p0, Lcom/konka/appassistant/AppAssistantActivity;->uninstallAppMun:I

    iget-object v2, p0, Lcom/konka/appassistant/AppAssistantActivity;->mUninstallList:Ljava/util/List;

    invoke-interface {v2}, Ljava/util/List;->size()I

    move-result v2

    if-ne v1, v2, :cond_0

    iget-object v1, p0, Lcom/konka/appassistant/AppAssistantActivity;->mViewType:Lcom/konka/appassistant/ApplicationInfoView$ViewType;

    sget-object v2, Lcom/konka/appassistant/ApplicationInfoView$ViewType;->UNINSTALL_VIEW:Lcom/konka/appassistant/ApplicationInfoView$ViewType;

    if-ne v1, v2, :cond_0

    iget-object v1, p0, Lcom/konka/appassistant/AppAssistantActivity;->mMainHandler:Landroid/os/Handler;

    const/4 v2, 0x1

    invoke-virtual {v1, v2}, Landroid/os/Handler;->sendEmptyMessage(I)Z

    :cond_0
    return-void

    :cond_1
    iget-object v1, p0, Lcom/konka/appassistant/AppAssistantActivity;->mUninstallList:Ljava/util/List;

    invoke-interface {v1, v0}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/konka/appassistant/AppAssistantActivity$AppInfo;

    iget-object v1, v1, Lcom/konka/appassistant/AppAssistantActivity$AppInfo;->info:Landroid/content/pm/ApplicationInfo;

    iget-object v1, v1, Landroid/content/pm/ApplicationInfo;->packageName:Ljava/lang/String;

    invoke-virtual {v1, p1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_2

    iget-object v1, p0, Lcom/konka/appassistant/AppAssistantActivity;->mUninstallList:Ljava/util/List;

    invoke-interface {v1, v0}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/konka/appassistant/AppAssistantActivity$AppInfo;

    iput-wide p2, v1, Lcom/konka/appassistant/AppAssistantActivity$AppInfo;->appsize:J

    iget v1, p0, Lcom/konka/appassistant/AppAssistantActivity;->uninstallAppMun:I

    add-int/lit8 v1, v1, 0x1

    iput v1, p0, Lcom/konka/appassistant/AppAssistantActivity;->uninstallAppMun:I

    goto :goto_1

    :cond_2
    add-int/lit8 v0, v0, 0x1

    goto :goto_0
.end method

.method private showViewList()V
    .locals 7

    const v6, 0x7f060009    # com.konka.appassistant.R.string.app_assistant_second_menu_result_fail

    const/4 v5, 0x1

    const/4 v4, 0x0

    iget-object v0, p0, Lcom/konka/appassistant/AppAssistantActivity;->mResultNum:Landroid/widget/TextView;

    invoke-virtual {v0, v4}, Landroid/widget/TextView;->setVisibility(I)V

    iget-object v0, p0, Lcom/konka/appassistant/AppAssistantActivity;->mProgressBarContainer:Landroid/widget/LinearLayout;

    const/16 v1, 0x8

    invoke-virtual {v0, v1}, Landroid/widget/LinearLayout;->setVisibility(I)V

    iget-object v0, p0, Lcom/konka/appassistant/AppAssistantActivity;->mApplicationList:Lcom/konka/appassistant/ApplicationList;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/konka/appassistant/AppAssistantActivity;->mApplicationList:Lcom/konka/appassistant/ApplicationList;

    iget-object v1, p0, Lcom/konka/appassistant/AppAssistantActivity;->mViewType:Lcom/konka/appassistant/ApplicationInfoView$ViewType;

    iget-object v2, p0, Lcom/konka/appassistant/AppAssistantActivity;->mInstallList:Ljava/util/List;

    iget-object v3, p0, Lcom/konka/appassistant/AppAssistantActivity;->mUninstallList:Ljava/util/List;

    invoke-virtual {v0, v1, v2, v3}, Lcom/konka/appassistant/ApplicationList;->refreshView(Lcom/konka/appassistant/ApplicationInfoView$ViewType;Ljava/util/List;Ljava/util/List;)V

    :cond_0
    invoke-static {}, Lcom/konka/appassistant/AppAssistantActivity;->$SWITCH_TABLE$com$konka$appassistant$ApplicationInfoView$ViewType()[I

    move-result-object v0

    iget-object v1, p0, Lcom/konka/appassistant/AppAssistantActivity;->mViewType:Lcom/konka/appassistant/ApplicationInfoView$ViewType;

    invoke-virtual {v1}, Lcom/konka/appassistant/ApplicationInfoView$ViewType;->ordinal()I

    move-result v1

    aget v0, v0, v1

    packed-switch v0, :pswitch_data_0

    :goto_0
    :pswitch_0
    return-void

    :pswitch_1
    iget-object v0, p0, Lcom/konka/appassistant/AppAssistantActivity;->mResultNum:Landroid/widget/TextView;

    const v1, 0x7f060006    # com.konka.appassistant.R.string.app_assistant_second_menu_app_num_install

    invoke-virtual {p0, v1}, Lcom/konka/appassistant/AppAssistantActivity;->getString(I)Ljava/lang/String;

    move-result-object v1

    new-array v2, v5, [Ljava/lang/Object;

    iget-object v3, p0, Lcom/konka/appassistant/AppAssistantActivity;->mInstallList:Ljava/util/List;

    invoke-interface {v3}, Ljava/util/List;->size()I

    move-result v3

    invoke-static {v3}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v3

    aput-object v3, v2, v4

    invoke-static {v1, v2}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    iget-object v0, p0, Lcom/konka/appassistant/AppAssistantActivity;->mInstallList:Ljava/util/List;

    if-nez v0, :cond_1

    iget-object v0, p0, Lcom/konka/appassistant/AppAssistantActivity;->mRefreshFailContainer:Landroid/widget/LinearLayout;

    invoke-virtual {v0, v4}, Landroid/widget/LinearLayout;->setVisibility(I)V

    iget-object v0, p0, Lcom/konka/appassistant/AppAssistantActivity;->mRefreshFail:Landroid/widget/TextView;

    invoke-virtual {p0, v6}, Lcom/konka/appassistant/AppAssistantActivity;->getString(I)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    goto :goto_0

    :cond_1
    iget-object v0, p0, Lcom/konka/appassistant/AppAssistantActivity;->mInstallList:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->size()I

    move-result v0

    if-nez v0, :cond_3

    iget-object v0, p0, Lcom/konka/appassistant/AppAssistantActivity;->mRefreshFailContainer:Landroid/widget/LinearLayout;

    invoke-virtual {v0, v4}, Landroid/widget/LinearLayout;->setVisibility(I)V

    iget-object v0, p0, Lcom/konka/appassistant/AppAssistantActivity;->mStorage:Lcom/konka/appassistant/Storage;

    invoke-virtual {v0}, Lcom/konka/appassistant/Storage;->isSDMounted()Z

    move-result v0

    if-nez v0, :cond_2

    iget-object v0, p0, Lcom/konka/appassistant/AppAssistantActivity;->mRefreshFail:Landroid/widget/TextView;

    invoke-virtual {p0, v6}, Lcom/konka/appassistant/AppAssistantActivity;->getString(I)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    goto :goto_0

    :cond_2
    iget-object v0, p0, Lcom/konka/appassistant/AppAssistantActivity;->mRefreshFail:Landroid/widget/TextView;

    const v1, 0x7f06000a    # com.konka.appassistant.R.string.app_assistant_second_menu_result_no_apk

    invoke-virtual {p0, v1}, Lcom/konka/appassistant/AppAssistantActivity;->getString(I)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    goto :goto_0

    :cond_3
    iget-object v0, p0, Lcom/konka/appassistant/AppAssistantActivity;->mScrollView:Landroid/widget/ScrollView;

    invoke-virtual {v0, v4}, Landroid/widget/ScrollView;->setVisibility(I)V

    goto :goto_0

    :pswitch_2
    iget-object v0, p0, Lcom/konka/appassistant/AppAssistantActivity;->mResultNum:Landroid/widget/TextView;

    const v1, 0x7f060007    # com.konka.appassistant.R.string.app_assistant_second_menu_app_num_uninstall

    invoke-virtual {p0, v1}, Lcom/konka/appassistant/AppAssistantActivity;->getString(I)Ljava/lang/String;

    move-result-object v1

    new-array v2, v5, [Ljava/lang/Object;

    iget-object v3, p0, Lcom/konka/appassistant/AppAssistantActivity;->mUninstallList:Ljava/util/List;

    invoke-interface {v3}, Ljava/util/List;->size()I

    move-result v3

    invoke-static {v3}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v3

    aput-object v3, v2, v4

    invoke-static {v1, v2}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    iget-object v0, p0, Lcom/konka/appassistant/AppAssistantActivity;->mUninstallList:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->size()I

    move-result v0

    if-eqz v0, :cond_4

    iget-object v0, p0, Lcom/konka/appassistant/AppAssistantActivity;->mScrollView:Landroid/widget/ScrollView;

    invoke-virtual {v0, v4}, Landroid/widget/ScrollView;->setVisibility(I)V

    goto/16 :goto_0

    :cond_4
    iget-object v0, p0, Lcom/konka/appassistant/AppAssistantActivity;->mRefreshFailContainer:Landroid/widget/LinearLayout;

    invoke-virtual {v0, v4}, Landroid/widget/LinearLayout;->setVisibility(I)V

    iget-object v0, p0, Lcom/konka/appassistant/AppAssistantActivity;->mRefreshFail:Landroid/widget/TextView;

    const v1, 0x7f06000b    # com.konka.appassistant.R.string.app_assistant_second_menu_result_no_apk_uninstall

    invoke-virtual {p0, v1}, Lcom/konka/appassistant/AppAssistantActivity;->getString(I)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    goto/16 :goto_0

    nop

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_1
        :pswitch_2
        :pswitch_0
    .end packed-switch
.end method

.method private tabButtonInit()V
    .locals 6

    const v5, 0x7f050009    # com.konka.appassistant.R.dimen.app_assistant_button_icon_paddingleft

    const v4, 0x7f02000f    # com.konka.appassistant.R.drawable.tab_button

    const/4 v3, -0x1

    const/4 v2, 0x0

    iget-object v0, p0, Lcom/konka/appassistant/AppAssistantActivity;->mInstall_button:Landroid/widget/TextView;

    invoke-virtual {v0, v4}, Landroid/widget/TextView;->setBackgroundResource(I)V

    iget-object v0, p0, Lcom/konka/appassistant/AppAssistantActivity;->mInstall_button:Landroid/widget/TextView;

    invoke-virtual {v0, v3}, Landroid/widget/TextView;->setTextColor(I)V

    iget-object v0, p0, Lcom/konka/appassistant/AppAssistantActivity;->mInstall_button:Landroid/widget/TextView;

    invoke-virtual {p0}, Lcom/konka/appassistant/AppAssistantActivity;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    invoke-virtual {v1, v5}, Landroid/content/res/Resources;->getDimension(I)F

    move-result v1

    float-to-int v1, v1

    invoke-virtual {v0, v1, v2, v2, v2}, Landroid/widget/TextView;->setPadding(IIII)V

    iget-object v0, p0, Lcom/konka/appassistant/AppAssistantActivity;->mUninstall_button:Landroid/widget/TextView;

    invoke-virtual {v0, v4}, Landroid/widget/TextView;->setBackgroundResource(I)V

    iget-object v0, p0, Lcom/konka/appassistant/AppAssistantActivity;->mUninstall_button:Landroid/widget/TextView;

    invoke-virtual {v0, v3}, Landroid/widget/TextView;->setTextColor(I)V

    iget-object v0, p0, Lcom/konka/appassistant/AppAssistantActivity;->mUninstall_button:Landroid/widget/TextView;

    invoke-virtual {p0}, Lcom/konka/appassistant/AppAssistantActivity;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    invoke-virtual {v1, v5}, Landroid/content/res/Resources;->getDimension(I)F

    move-result v1

    float-to-int v1, v1

    invoke-virtual {v0, v1, v2, v2, v2}, Landroid/widget/TextView;->setPadding(IIII)V

    iget-object v0, p0, Lcom/konka/appassistant/AppAssistantActivity;->mZipInstall_button:Landroid/widget/TextView;

    invoke-virtual {v0, v4}, Landroid/widget/TextView;->setBackgroundResource(I)V

    iget-object v0, p0, Lcom/konka/appassistant/AppAssistantActivity;->mZipInstall_button:Landroid/widget/TextView;

    invoke-virtual {v0, v3}, Landroid/widget/TextView;->setTextColor(I)V

    iget-object v0, p0, Lcom/konka/appassistant/AppAssistantActivity;->mZipInstall_button:Landroid/widget/TextView;

    invoke-virtual {p0}, Lcom/konka/appassistant/AppAssistantActivity;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    invoke-virtual {v1, v5}, Landroid/content/res/Resources;->getDimension(I)F

    move-result v1

    float-to-int v1, v1

    invoke-virtual {v0, v1, v2, v2, v2}, Landroid/widget/TextView;->setPadding(IIII)V

    return-void
.end method


# virtual methods
.method public Refresh()V
    .locals 2

    iget-object v0, p0, Lcom/konka/appassistant/AppAssistantActivity;->mViewType:Lcom/konka/appassistant/ApplicationInfoView$ViewType;

    invoke-virtual {p0, v0}, Lcom/konka/appassistant/AppAssistantActivity;->requestFocus(Lcom/konka/appassistant/ApplicationInfoView$ViewType;)V

    iget-object v0, p0, Lcom/konka/appassistant/AppAssistantActivity;->mUninstallList:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->clear()V

    iget-object v0, p0, Lcom/konka/appassistant/AppAssistantActivity;->mInstallList:Ljava/util/List;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/konka/appassistant/AppAssistantActivity;->mInstallList:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->clear()V

    :goto_0
    iget-object v0, p0, Lcom/konka/appassistant/AppAssistantActivity;->mMainHandler:Landroid/os/Handler;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Landroid/os/Handler;->sendEmptyMessage(I)Z

    return-void

    :cond_0
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, Lcom/konka/appassistant/AppAssistantActivity;->mInstallList:Ljava/util/List;

    goto :goto_0
.end method

.method public ScrollToEnd()V
    .locals 3

    iget-object v1, p0, Lcom/konka/appassistant/AppAssistantActivity;->mScrollView:Landroid/widget/ScrollView;

    if-eqz v1, :cond_0

    iget-object v1, p0, Lcom/konka/appassistant/AppAssistantActivity;->mResultList:Landroid/widget/LinearLayout;

    if-eqz v1, :cond_0

    iget-object v1, p0, Lcom/konka/appassistant/AppAssistantActivity;->mResultList:Landroid/widget/LinearLayout;

    invoke-virtual {v1}, Landroid/widget/LinearLayout;->getMeasuredHeight()I

    move-result v1

    iget-object v2, p0, Lcom/konka/appassistant/AppAssistantActivity;->mScrollView:Landroid/widget/ScrollView;

    invoke-virtual {v2}, Landroid/widget/ScrollView;->getHeight()I

    move-result v2

    sub-int v0, v1, v2

    iget-object v1, p0, Lcom/konka/appassistant/AppAssistantActivity;->mScrollView:Landroid/widget/ScrollView;

    const/4 v2, 0x0

    invoke-virtual {v1, v2, v0}, Landroid/widget/ScrollView;->smoothScrollTo(II)V

    if-lez v0, :cond_0

    iget-object v1, p0, Lcom/konka/appassistant/AppAssistantActivity;->mApplicationList:Lcom/konka/appassistant/ApplicationList;

    iget-object v2, p0, Lcom/konka/appassistant/AppAssistantActivity;->mApplicationList:Lcom/konka/appassistant/ApplicationList;

    invoke-virtual {v2}, Lcom/konka/appassistant/ApplicationList;->getTopPageNum()I

    move-result v2

    add-int/lit8 v2, v2, 0x1

    invoke-virtual {v1, v2}, Lcom/konka/appassistant/ApplicationList;->setTopPageNum(I)V

    :cond_0
    return-void
.end method

.method public dispatchKeyEvent(Landroid/view/KeyEvent;)Z
    .locals 5
    .param p1    # Landroid/view/KeyEvent;

    const/4 v3, -0x1

    const/4 v4, 0x0

    const/4 v1, 0x1

    invoke-virtual {p1}, Landroid/view/KeyEvent;->getAction()I

    move-result v2

    if-nez v2, :cond_0

    invoke-virtual {p1}, Landroid/view/KeyEvent;->getKeyCode()I

    move-result v2

    sparse-switch v2, :sswitch_data_0

    :cond_0
    invoke-super {p0, p1}, Landroid/app/Activity;->dispatchKeyEvent(Landroid/view/KeyEvent;)Z

    move-result v1

    :cond_1
    :goto_0
    return v1

    :sswitch_0
    invoke-direct {p0}, Lcom/konka/appassistant/AppAssistantActivity;->IsTabButtonFocused()Ljava/lang/Boolean;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v2

    if-nez v2, :cond_0

    iget-object v2, p0, Lcom/konka/appassistant/AppAssistantActivity;->mRefreshButton:Landroid/widget/Button;

    invoke-virtual {v2}, Landroid/widget/Button;->isFocused()Z

    move-result v2

    if-nez v2, :cond_1

    iget-object v2, p0, Lcom/konka/appassistant/AppAssistantActivity;->mApplicationList:Lcom/konka/appassistant/ApplicationList;

    invoke-virtual {v2}, Lcom/konka/appassistant/ApplicationList;->PageUp()I

    move-result v0

    if-eq v3, v0, :cond_1

    iget-object v2, p0, Lcom/konka/appassistant/AppAssistantActivity;->mScrollView:Landroid/widget/ScrollView;

    invoke-virtual {v2, v4, v0}, Landroid/widget/ScrollView;->smoothScrollTo(II)V

    goto :goto_0

    :sswitch_1
    invoke-direct {p0}, Lcom/konka/appassistant/AppAssistantActivity;->IsTabButtonFocused()Ljava/lang/Boolean;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v2

    if-nez v2, :cond_0

    iget-object v2, p0, Lcom/konka/appassistant/AppAssistantActivity;->mRefreshButton:Landroid/widget/Button;

    invoke-virtual {v2}, Landroid/widget/Button;->isFocused()Z

    move-result v2

    if-eqz v2, :cond_2

    iget-object v2, p0, Lcom/konka/appassistant/AppAssistantActivity;->mViewType:Lcom/konka/appassistant/ApplicationInfoView$ViewType;

    sget-object v3, Lcom/konka/appassistant/ApplicationInfoView$ViewType;->ZIP_INSTALL_VIEW:Lcom/konka/appassistant/ApplicationInfoView$ViewType;

    if-ne v2, v3, :cond_1

    iget-object v2, p0, Lcom/konka/appassistant/AppAssistantActivity;->mApplicationList:Lcom/konka/appassistant/ApplicationList;

    invoke-virtual {v2}, Lcom/konka/appassistant/ApplicationList;->FocusOnTopofPage()V

    goto :goto_0

    :cond_2
    iget-object v2, p0, Lcom/konka/appassistant/AppAssistantActivity;->mScrollView:Landroid/widget/ScrollView;

    iget-object v3, p0, Lcom/konka/appassistant/AppAssistantActivity;->mApplicationList:Lcom/konka/appassistant/ApplicationList;

    invoke-virtual {v3}, Lcom/konka/appassistant/ApplicationList;->PageDown()I

    move-result v3

    invoke-virtual {v2, v4, v3}, Landroid/widget/ScrollView;->smoothScrollTo(II)V

    goto :goto_0

    :sswitch_2
    invoke-direct {p0}, Lcom/konka/appassistant/AppAssistantActivity;->IsTabButtonFocused()Ljava/lang/Boolean;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v2

    if-nez v2, :cond_0

    iget-object v2, p0, Lcom/konka/appassistant/AppAssistantActivity;->mRefreshButton:Landroid/widget/Button;

    invoke-virtual {v2}, Landroid/widget/Button;->isFocused()Z

    move-result v2

    if-nez v2, :cond_0

    iget-object v2, p0, Lcom/konka/appassistant/AppAssistantActivity;->mApplicationList:Lcom/konka/appassistant/ApplicationList;

    invoke-virtual {v2}, Lcom/konka/appassistant/ApplicationList;->OnKeyUp()I

    move-result v0

    if-eq v3, v0, :cond_3

    iget-object v2, p0, Lcom/konka/appassistant/AppAssistantActivity;->mScrollView:Landroid/widget/ScrollView;

    invoke-virtual {v2, v4, v0}, Landroid/widget/ScrollView;->smoothScrollTo(II)V

    goto :goto_0

    :cond_3
    iget-object v2, p0, Lcom/konka/appassistant/AppAssistantActivity;->mRefreshButton:Landroid/widget/Button;

    invoke-virtual {v2}, Landroid/widget/Button;->requestFocus()Z

    goto :goto_0

    :sswitch_3
    invoke-direct {p0}, Lcom/konka/appassistant/AppAssistantActivity;->IsTabButtonFocused()Ljava/lang/Boolean;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v2

    if-nez v2, :cond_0

    iget-object v2, p0, Lcom/konka/appassistant/AppAssistantActivity;->mRefreshButton:Landroid/widget/Button;

    invoke-virtual {v2}, Landroid/widget/Button;->isFocused()Z

    move-result v2

    if-nez v2, :cond_0

    iget-object v2, p0, Lcom/konka/appassistant/AppAssistantActivity;->mApplicationList:Lcom/konka/appassistant/ApplicationList;

    invoke-virtual {v2}, Lcom/konka/appassistant/ApplicationList;->OnkeyDown()I

    move-result v0

    if-eq v3, v0, :cond_1

    iget-object v2, p0, Lcom/konka/appassistant/AppAssistantActivity;->mScrollView:Landroid/widget/ScrollView;

    invoke-virtual {v2, v4, v0}, Landroid/widget/ScrollView;->smoothScrollTo(II)V

    goto/16 :goto_0

    :sswitch_4
    invoke-direct {p0}, Lcom/konka/appassistant/AppAssistantActivity;->OpenDialog()V

    goto/16 :goto_0

    nop

    :sswitch_data_0
    .sparse-switch
        0x4 -> :sswitch_4
        0x13 -> :sswitch_2
        0x14 -> :sswitch_3
        0x5c -> :sswitch_0
        0x5d -> :sswitch_1
    .end sparse-switch
.end method

.method public onCreate(Landroid/os/Bundle;)V
    .locals 6
    .param p1    # Landroid/os/Bundle;

    const/4 v5, 0x0

    invoke-super {p0, p1}, Landroid/app/Activity;->onCreate(Landroid/os/Bundle;)V

    const/high16 v2, 0x7f030000    # com.konka.appassistant.R.layout.application_assistant

    invoke-virtual {p0, v2}, Lcom/konka/appassistant/AppAssistantActivity;->setContentView(I)V

    invoke-virtual {p0}, Lcom/konka/appassistant/AppAssistantActivity;->getPackageManager()Landroid/content/pm/PackageManager;

    move-result-object v2

    iput-object v2, p0, Lcom/konka/appassistant/AppAssistantActivity;->pm:Landroid/content/pm/PackageManager;

    invoke-direct {p0}, Lcom/konka/appassistant/AppAssistantActivity;->initTrustedList()Ljava/util/List;

    move-result-object v2

    iput-object v2, p0, Lcom/konka/appassistant/AppAssistantActivity;->mTrustList:Ljava/util/List;

    const v2, 0x7f070011    # com.konka.appassistant.R.id.app_assistant_second_menu_scrollview_content

    invoke-virtual {p0, v2}, Lcom/konka/appassistant/AppAssistantActivity;->findViewById(I)Landroid/view/View;

    move-result-object v2

    check-cast v2, Landroid/widget/LinearLayout;

    iput-object v2, p0, Lcom/konka/appassistant/AppAssistantActivity;->mResultList:Landroid/widget/LinearLayout;

    iget-object v2, p0, Lcom/konka/appassistant/AppAssistantActivity;->mResultList:Landroid/widget/LinearLayout;

    invoke-virtual {v2, p0}, Landroid/widget/LinearLayout;->setOnKeyListener(Landroid/view/View$OnKeyListener;)V

    new-instance v2, Lcom/konka/appassistant/ApplicationList;

    iget-object v3, p0, Lcom/konka/appassistant/AppAssistantActivity;->mResultList:Landroid/widget/LinearLayout;

    invoke-direct {v2, p0, v3}, Lcom/konka/appassistant/ApplicationList;-><init>(Landroid/content/Context;Landroid/widget/LinearLayout;)V

    iput-object v2, p0, Lcom/konka/appassistant/AppAssistantActivity;->mApplicationList:Lcom/konka/appassistant/ApplicationList;

    const v2, 0x7f070005    # com.konka.appassistant.R.id.app_assistant_button_install

    invoke-virtual {p0, v2}, Lcom/konka/appassistant/AppAssistantActivity;->findViewById(I)Landroid/view/View;

    move-result-object v2

    check-cast v2, Landroid/widget/TextView;

    iput-object v2, p0, Lcom/konka/appassistant/AppAssistantActivity;->mInstall_button:Landroid/widget/TextView;

    iget-object v2, p0, Lcom/konka/appassistant/AppAssistantActivity;->mInstall_button:Landroid/widget/TextView;

    invoke-virtual {v2, p0}, Landroid/widget/TextView;->setOnKeyListener(Landroid/view/View$OnKeyListener;)V

    iget-object v2, p0, Lcom/konka/appassistant/AppAssistantActivity;->mInstall_button:Landroid/widget/TextView;

    iget-object v3, p0, Lcom/konka/appassistant/AppAssistantActivity;->mOnFocusChangeListener:Landroid/view/View$OnFocusChangeListener;

    invoke-virtual {v2, v3}, Landroid/widget/TextView;->setOnFocusChangeListener(Landroid/view/View$OnFocusChangeListener;)V

    iget-object v2, p0, Lcom/konka/appassistant/AppAssistantActivity;->mInstall_button:Landroid/widget/TextView;

    iget-object v3, p0, Lcom/konka/appassistant/AppAssistantActivity;->mOnClickListener:Landroid/view/View$OnClickListener;

    invoke-virtual {v2, v3}, Landroid/widget/TextView;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    iget-object v2, p0, Lcom/konka/appassistant/AppAssistantActivity;->mInstall_button:Landroid/widget/TextView;

    invoke-virtual {p0}, Lcom/konka/appassistant/AppAssistantActivity;->getResources()Landroid/content/res/Resources;

    move-result-object v3

    const v4, 0x7f020004    # com.konka.appassistant.R.drawable.icon_install

    invoke-virtual {v3, v4}, Landroid/content/res/Resources;->getDrawable(I)Landroid/graphics/drawable/Drawable;

    move-result-object v3

    invoke-virtual {v2, v3, v5, v5, v5}, Landroid/widget/TextView;->setCompoundDrawablesWithIntrinsicBounds(Landroid/graphics/drawable/Drawable;Landroid/graphics/drawable/Drawable;Landroid/graphics/drawable/Drawable;Landroid/graphics/drawable/Drawable;)V

    iget-object v2, p0, Lcom/konka/appassistant/AppAssistantActivity;->mInstall_button:Landroid/widget/TextView;

    new-instance v3, Ljava/lang/StringBuilder;

    const-string v4, "    "

    invoke-direct {v3, v4}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    const v4, 0x7f060003    # com.konka.appassistant.R.string.app_assistant_button_install

    invoke-virtual {p0, v4}, Lcom/konka/appassistant/AppAssistantActivity;->getString(I)Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    const v2, 0x7f070006    # com.konka.appassistant.R.id.app_assistant_button_uninstall

    invoke-virtual {p0, v2}, Lcom/konka/appassistant/AppAssistantActivity;->findViewById(I)Landroid/view/View;

    move-result-object v2

    check-cast v2, Landroid/widget/TextView;

    iput-object v2, p0, Lcom/konka/appassistant/AppAssistantActivity;->mUninstall_button:Landroid/widget/TextView;

    iget-object v2, p0, Lcom/konka/appassistant/AppAssistantActivity;->mUninstall_button:Landroid/widget/TextView;

    invoke-virtual {v2, p0}, Landroid/widget/TextView;->setOnKeyListener(Landroid/view/View$OnKeyListener;)V

    iget-object v2, p0, Lcom/konka/appassistant/AppAssistantActivity;->mUninstall_button:Landroid/widget/TextView;

    iget-object v3, p0, Lcom/konka/appassistant/AppAssistantActivity;->mOnFocusChangeListener:Landroid/view/View$OnFocusChangeListener;

    invoke-virtual {v2, v3}, Landroid/widget/TextView;->setOnFocusChangeListener(Landroid/view/View$OnFocusChangeListener;)V

    iget-object v2, p0, Lcom/konka/appassistant/AppAssistantActivity;->mUninstall_button:Landroid/widget/TextView;

    iget-object v3, p0, Lcom/konka/appassistant/AppAssistantActivity;->mOnClickListener:Landroid/view/View$OnClickListener;

    invoke-virtual {v2, v3}, Landroid/widget/TextView;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    iget-object v2, p0, Lcom/konka/appassistant/AppAssistantActivity;->mUninstall_button:Landroid/widget/TextView;

    invoke-virtual {p0}, Lcom/konka/appassistant/AppAssistantActivity;->getResources()Landroid/content/res/Resources;

    move-result-object v3

    const v4, 0x7f020005    # com.konka.appassistant.R.drawable.icon_uninstall

    invoke-virtual {v3, v4}, Landroid/content/res/Resources;->getDrawable(I)Landroid/graphics/drawable/Drawable;

    move-result-object v3

    invoke-virtual {v2, v3, v5, v5, v5}, Landroid/widget/TextView;->setCompoundDrawablesWithIntrinsicBounds(Landroid/graphics/drawable/Drawable;Landroid/graphics/drawable/Drawable;Landroid/graphics/drawable/Drawable;Landroid/graphics/drawable/Drawable;)V

    iget-object v2, p0, Lcom/konka/appassistant/AppAssistantActivity;->mUninstall_button:Landroid/widget/TextView;

    new-instance v3, Ljava/lang/StringBuilder;

    const-string v4, "    "

    invoke-direct {v3, v4}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    const v4, 0x7f060004    # com.konka.appassistant.R.string.app_assistant_button_uninstall

    invoke-virtual {p0, v4}, Lcom/konka/appassistant/AppAssistantActivity;->getString(I)Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    const v2, 0x7f070004    # com.konka.appassistant.R.id.app_assistant_button_zipinstall

    invoke-virtual {p0, v2}, Lcom/konka/appassistant/AppAssistantActivity;->findViewById(I)Landroid/view/View;

    move-result-object v2

    check-cast v2, Landroid/widget/TextView;

    iput-object v2, p0, Lcom/konka/appassistant/AppAssistantActivity;->mZipInstall_button:Landroid/widget/TextView;

    iget-object v2, p0, Lcom/konka/appassistant/AppAssistantActivity;->mZipInstall_button:Landroid/widget/TextView;

    invoke-virtual {v2, p0}, Landroid/widget/TextView;->setOnKeyListener(Landroid/view/View$OnKeyListener;)V

    iget-object v2, p0, Lcom/konka/appassistant/AppAssistantActivity;->mZipInstall_button:Landroid/widget/TextView;

    iget-object v3, p0, Lcom/konka/appassistant/AppAssistantActivity;->mOnFocusChangeListener:Landroid/view/View$OnFocusChangeListener;

    invoke-virtual {v2, v3}, Landroid/widget/TextView;->setOnFocusChangeListener(Landroid/view/View$OnFocusChangeListener;)V

    iget-object v2, p0, Lcom/konka/appassistant/AppAssistantActivity;->mZipInstall_button:Landroid/widget/TextView;

    iget-object v3, p0, Lcom/konka/appassistant/AppAssistantActivity;->mOnClickListener:Landroid/view/View$OnClickListener;

    invoke-virtual {v2, v3}, Landroid/widget/TextView;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    iget-object v2, p0, Lcom/konka/appassistant/AppAssistantActivity;->mZipInstall_button:Landroid/widget/TextView;

    invoke-virtual {p0}, Lcom/konka/appassistant/AppAssistantActivity;->getResources()Landroid/content/res/Resources;

    move-result-object v3

    const v4, 0x7f020006    # com.konka.appassistant.R.drawable.icon_zip

    invoke-virtual {v3, v4}, Landroid/content/res/Resources;->getDrawable(I)Landroid/graphics/drawable/Drawable;

    move-result-object v3

    invoke-virtual {v2, v3, v5, v5, v5}, Landroid/widget/TextView;->setCompoundDrawablesWithIntrinsicBounds(Landroid/graphics/drawable/Drawable;Landroid/graphics/drawable/Drawable;Landroid/graphics/drawable/Drawable;Landroid/graphics/drawable/Drawable;)V

    iget-object v2, p0, Lcom/konka/appassistant/AppAssistantActivity;->mZipInstall_button:Landroid/widget/TextView;

    new-instance v3, Ljava/lang/StringBuilder;

    const-string v4, "    "

    invoke-direct {v3, v4}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    const v4, 0x7f060005    # com.konka.appassistant.R.string.app_assistant_button_zipinstall

    invoke-virtual {p0, v4}, Lcom/konka/appassistant/AppAssistantActivity;->getString(I)Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    const v2, 0x7f070010    # com.konka.appassistant.R.id.app_assistant_second_menu_scrollview

    invoke-virtual {p0, v2}, Lcom/konka/appassistant/AppAssistantActivity;->findViewById(I)Landroid/view/View;

    move-result-object v2

    check-cast v2, Landroid/widget/ScrollView;

    iput-object v2, p0, Lcom/konka/appassistant/AppAssistantActivity;->mScrollView:Landroid/widget/ScrollView;

    invoke-virtual {p0}, Lcom/konka/appassistant/AppAssistantActivity;->getResources()Landroid/content/res/Resources;

    move-result-object v2

    const v3, 0x7f050006    # com.konka.appassistant.R.dimen.app_assistant_second_menu_scrollview_height

    invoke-virtual {v2, v3}, Landroid/content/res/Resources;->getDimension(I)F

    move-result v2

    float-to-int v2, v2

    sput v2, Lcom/konka/appassistant/AppAssistantActivity;->ScrollViewHeight:I

    const v2, 0x7f07000c    # com.konka.appassistant.R.id.app_assistant_second_menu_progressbar_container

    invoke-virtual {p0, v2}, Lcom/konka/appassistant/AppAssistantActivity;->findViewById(I)Landroid/view/View;

    move-result-object v2

    check-cast v2, Landroid/widget/LinearLayout;

    iput-object v2, p0, Lcom/konka/appassistant/AppAssistantActivity;->mProgressBarContainer:Landroid/widget/LinearLayout;

    const v2, 0x7f07000d    # com.konka.appassistant.R.id.app_assistant_second_menu_progressbar

    invoke-virtual {p0, v2}, Lcom/konka/appassistant/AppAssistantActivity;->findViewById(I)Landroid/view/View;

    move-result-object v2

    check-cast v2, Landroid/widget/ProgressBar;

    iput-object v2, p0, Lcom/konka/appassistant/AppAssistantActivity;->mProgressBar:Landroid/widget/ProgressBar;

    iget-object v2, p0, Lcom/konka/appassistant/AppAssistantActivity;->mProgressBar:Landroid/widget/ProgressBar;

    const/4 v3, 0x1

    invoke-virtual {v2, v3}, Landroid/widget/ProgressBar;->setIndeterminate(Z)V

    const v2, 0x7f07000a    # com.konka.appassistant.R.id.app_assistant_second_menu_app_num

    invoke-virtual {p0, v2}, Lcom/konka/appassistant/AppAssistantActivity;->findViewById(I)Landroid/view/View;

    move-result-object v2

    check-cast v2, Landroid/widget/TextView;

    iput-object v2, p0, Lcom/konka/appassistant/AppAssistantActivity;->mResultNum:Landroid/widget/TextView;

    const v2, 0x7f07000b    # com.konka.appassistant.R.id.app_assistant_second_menu_app_refresh

    invoke-virtual {p0, v2}, Lcom/konka/appassistant/AppAssistantActivity;->findViewById(I)Landroid/view/View;

    move-result-object v2

    check-cast v2, Landroid/widget/Button;

    iput-object v2, p0, Lcom/konka/appassistant/AppAssistantActivity;->mRefreshButton:Landroid/widget/Button;

    iget-object v2, p0, Lcom/konka/appassistant/AppAssistantActivity;->mRefreshButton:Landroid/widget/Button;

    iget-object v3, p0, Lcom/konka/appassistant/AppAssistantActivity;->mRefreshOnClickListener:Landroid/view/View$OnClickListener;

    invoke-virtual {v2, v3}, Landroid/widget/Button;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    iget-object v2, p0, Lcom/konka/appassistant/AppAssistantActivity;->mRefreshButton:Landroid/widget/Button;

    invoke-virtual {v2, p0}, Landroid/widget/Button;->setOnKeyListener(Landroid/view/View$OnKeyListener;)V

    const v2, 0x7f07000f    # com.konka.appassistant.R.id.app_assistant_second_menu_result_fail_content

    invoke-virtual {p0, v2}, Lcom/konka/appassistant/AppAssistantActivity;->findViewById(I)Landroid/view/View;

    move-result-object v2

    check-cast v2, Landroid/widget/TextView;

    iput-object v2, p0, Lcom/konka/appassistant/AppAssistantActivity;->mRefreshFail:Landroid/widget/TextView;

    const v2, 0x7f07000e    # com.konka.appassistant.R.id.app_assistant_second_menu_result_fail

    invoke-virtual {p0, v2}, Lcom/konka/appassistant/AppAssistantActivity;->findViewById(I)Landroid/view/View;

    move-result-object v2

    check-cast v2, Landroid/widget/LinearLayout;

    iput-object v2, p0, Lcom/konka/appassistant/AppAssistantActivity;->mRefreshFailContainer:Landroid/widget/LinearLayout;

    :try_start_0
    iget-object v2, p0, Lcom/konka/appassistant/AppAssistantActivity;->pm:Landroid/content/pm/PackageManager;

    invoke-virtual {p0}, Lcom/konka/appassistant/AppAssistantActivity;->getPackageName()Ljava/lang/String;

    move-result-object v3

    const/4 v4, 0x0

    invoke-virtual {v2, v3, v4}, Landroid/content/pm/PackageManager;->getPackageInfo(Ljava/lang/String;I)Landroid/content/pm/PackageInfo;

    move-result-object v1

    iget-object v2, v1, Landroid/content/pm/PackageInfo;->versionName:Ljava/lang/String;

    if-eqz v2, :cond_0

    const v2, 0x7f070007    # com.konka.appassistant.R.id.app_assistant_version

    invoke-virtual {p0, v2}, Lcom/konka/appassistant/AppAssistantActivity;->findViewById(I)Landroid/view/View;

    move-result-object v2

    check-cast v2, Landroid/widget/TextView;

    iget-object v3, v1, Landroid/content/pm/PackageInfo;->versionName:Ljava/lang/String;

    invoke-virtual {v2, v3}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V
    :try_end_0
    .catch Landroid/content/pm/PackageManager$NameNotFoundException; {:try_start_0 .. :try_end_0} :catch_0

    :cond_0
    :goto_0
    iget-object v2, p0, Lcom/konka/appassistant/AppAssistantActivity;->mOneKeyInstallProcess:Lcom/konka/appassistant/OneKeyInstallProcess;

    invoke-virtual {v2, p0}, Lcom/konka/appassistant/OneKeyInstallProcess;->setContext(Landroid/content/Context;)V

    iget-object v2, p0, Lcom/konka/appassistant/AppAssistantActivity;->mOneKeyInstallProcess:Lcom/konka/appassistant/OneKeyInstallProcess;

    iget-object v3, p0, Lcom/konka/appassistant/AppAssistantActivity;->mInstallProcessListener:Lcom/konka/appassistant/InstallProcessListener;

    invoke-virtual {v2, v3}, Lcom/konka/appassistant/OneKeyInstallProcess;->setProcessListenser(Lcom/konka/appassistant/InstallProcessListener;)V

    return-void

    :catch_0
    move-exception v0

    invoke-virtual {v0}, Landroid/content/pm/PackageManager$NameNotFoundException;->printStackTrace()V

    goto :goto_0
.end method

.method protected onDestroy()V
    .locals 1

    iget-object v0, p0, Lcom/konka/appassistant/AppAssistantActivity;->mOneKeyInstallProcess:Lcom/konka/appassistant/OneKeyInstallProcess;

    invoke-virtual {v0}, Lcom/konka/appassistant/OneKeyInstallProcess;->stopInstallThread()V

    invoke-super {p0}, Landroid/app/Activity;->onDestroy()V

    const/4 v0, 0x0

    invoke-static {v0}, Ljava/lang/System;->exit(I)V

    return-void
.end method

.method public onKey(Landroid/view/View;ILandroid/view/KeyEvent;)Z
    .locals 3
    .param p1    # Landroid/view/View;
    .param p2    # I
    .param p3    # Landroid/view/KeyEvent;

    const/4 v1, 0x1

    invoke-virtual {p3}, Landroid/view/KeyEvent;->getAction()I

    move-result v0

    if-nez v0, :cond_0

    sparse-switch p2, :sswitch_data_0

    :cond_0
    :goto_0
    const/4 v0, 0x0

    :goto_1
    return v0

    :sswitch_0
    iget-object v0, p0, Lcom/konka/appassistant/AppAssistantActivity;->mInstall_button:Landroid/widget/TextView;

    if-eq p1, v0, :cond_1

    iget-object v0, p0, Lcom/konka/appassistant/AppAssistantActivity;->mUninstall_button:Landroid/widget/TextView;

    if-eq p1, v0, :cond_1

    iget-object v0, p0, Lcom/konka/appassistant/AppAssistantActivity;->mZipInstall_button:Landroid/widget/TextView;

    if-ne p1, v0, :cond_0

    :cond_1
    iget-object v0, p0, Lcom/konka/appassistant/AppAssistantActivity;->mScrollView:Landroid/widget/ScrollView;

    invoke-virtual {v0}, Landroid/widget/ScrollView;->getVisibility()I

    move-result v0

    if-nez v0, :cond_4

    iget-object v0, p0, Lcom/konka/appassistant/AppAssistantActivity;->mResultList:Landroid/widget/LinearLayout;

    invoke-virtual {v0}, Landroid/widget/LinearLayout;->getChildCount()I

    move-result v0

    if-eqz v0, :cond_2

    iget-object v0, p0, Lcom/konka/appassistant/AppAssistantActivity;->mViewType:Lcom/konka/appassistant/ApplicationInfoView$ViewType;

    sget-object v2, Lcom/konka/appassistant/ApplicationInfoView$ViewType;->ZIP_INSTALL_VIEW:Lcom/konka/appassistant/ApplicationInfoView$ViewType;

    if-ne v0, v2, :cond_3

    :cond_2
    iget-object v0, p0, Lcom/konka/appassistant/AppAssistantActivity;->mRefreshButton:Landroid/widget/Button;

    invoke-virtual {v0}, Landroid/widget/Button;->requestFocus()Z

    goto :goto_0

    :cond_3
    iget-object v0, p0, Lcom/konka/appassistant/AppAssistantActivity;->mApplicationList:Lcom/konka/appassistant/ApplicationList;

    invoke-virtual {v0}, Lcom/konka/appassistant/ApplicationList;->getApplicationList()Ljava/util/List;

    move-result-object v0

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/konka/appassistant/AppAssistantActivity;->mApplicationList:Lcom/konka/appassistant/ApplicationList;

    invoke-virtual {v0}, Lcom/konka/appassistant/ApplicationList;->getApplicationList()Ljava/util/List;

    move-result-object v0

    sget v2, Lcom/konka/appassistant/AppAssistantActivity;->mLastFocusedItemNum:I

    invoke-interface {v0, v2}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/konka/appassistant/ApplicationInfoView;

    invoke-virtual {v0, v1}, Lcom/konka/appassistant/ApplicationInfoView;->setFocused(I)V

    move v0, v1

    goto :goto_1

    :cond_4
    iget-object v0, p0, Lcom/konka/appassistant/AppAssistantActivity;->mRefreshButton:Landroid/widget/Button;

    invoke-virtual {v0}, Landroid/widget/Button;->requestFocus()Z

    goto :goto_0

    :sswitch_1
    iget-object v0, p0, Lcom/konka/appassistant/AppAssistantActivity;->mRefreshButton:Landroid/widget/Button;

    if-ne p1, v0, :cond_0

    iget-object v0, p0, Lcom/konka/appassistant/AppAssistantActivity;->mViewType:Lcom/konka/appassistant/ApplicationInfoView$ViewType;

    invoke-virtual {p0, v0}, Lcom/konka/appassistant/AppAssistantActivity;->requestFocus(Lcom/konka/appassistant/ApplicationInfoView$ViewType;)V

    goto :goto_0

    :sswitch_2
    iget-object v0, p0, Lcom/konka/appassistant/AppAssistantActivity;->mRefreshButton:Landroid/widget/Button;

    if-ne p1, v0, :cond_6

    iget-object v0, p0, Lcom/konka/appassistant/AppAssistantActivity;->mScrollView:Landroid/widget/ScrollView;

    invoke-virtual {v0}, Landroid/widget/ScrollView;->getVisibility()I

    move-result v0

    if-nez v0, :cond_5

    iget-object v0, p0, Lcom/konka/appassistant/AppAssistantActivity;->mResultList:Landroid/widget/LinearLayout;

    invoke-virtual {v0}, Landroid/widget/LinearLayout;->getChildCount()I

    move-result v0

    if-nez v0, :cond_0

    :cond_5
    move v0, v1

    goto :goto_1

    :cond_6
    iget-object v0, p0, Lcom/konka/appassistant/AppAssistantActivity;->mUninstall_button:Landroid/widget/TextView;

    if-ne p1, v0, :cond_0

    move v0, v1

    goto :goto_1

    :sswitch_3
    iget-object v0, p0, Lcom/konka/appassistant/AppAssistantActivity;->mInstall_button:Landroid/widget/TextView;

    if-eq p1, v0, :cond_7

    iget-object v0, p0, Lcom/konka/appassistant/AppAssistantActivity;->mUninstall_button:Landroid/widget/TextView;

    if-eq p1, v0, :cond_7

    iget-object v0, p0, Lcom/konka/appassistant/AppAssistantActivity;->mZipInstall_button:Landroid/widget/TextView;

    if-ne p1, v0, :cond_0

    :cond_7
    move v0, v1

    goto :goto_1

    nop

    :sswitch_data_0
    .sparse-switch
        0x14 -> :sswitch_2
        0x15 -> :sswitch_1
        0x16 -> :sswitch_0
        0x42 -> :sswitch_3
    .end sparse-switch
.end method

.method protected onResume()V
    .locals 2

    iget-object v0, p0, Lcom/konka/appassistant/AppAssistantActivity;->mViewType:Lcom/konka/appassistant/ApplicationInfoView$ViewType;

    invoke-virtual {p0, v0}, Lcom/konka/appassistant/AppAssistantActivity;->requestFocus(Lcom/konka/appassistant/ApplicationInfoView$ViewType;)V

    iget-object v0, p0, Lcom/konka/appassistant/AppAssistantActivity;->mUninstallList:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->clear()V

    iget-object v0, p0, Lcom/konka/appassistant/AppAssistantActivity;->mMainHandler:Landroid/os/Handler;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Landroid/os/Handler;->sendEmptyMessage(I)Z

    invoke-super {p0}, Landroid/app/Activity;->onResume()V

    return-void
.end method

.method protected onStart()V
    .locals 2

    new-instance v0, Ljava/lang/Thread;

    new-instance v1, Lcom/konka/appassistant/AppAssistantActivity$9;

    invoke-direct {v1, p0}, Lcom/konka/appassistant/AppAssistantActivity$9;-><init>(Lcom/konka/appassistant/AppAssistantActivity;)V

    invoke-direct {v0, v1}, Ljava/lang/Thread;-><init>(Ljava/lang/Runnable;)V

    invoke-virtual {v0}, Ljava/lang/Thread;->start()V

    invoke-direct {p0}, Lcom/konka/appassistant/AppAssistantActivity;->initInstallBroadcastReceiver()V

    invoke-super {p0}, Landroid/app/Activity;->onStart()V

    return-void
.end method

.method protected onStop()V
    .locals 1

    iget-object v0, p0, Lcom/konka/appassistant/AppAssistantActivity;->mReceiver:Landroid/content/BroadcastReceiver;

    invoke-virtual {p0, v0}, Lcom/konka/appassistant/AppAssistantActivity;->unregisterReceiver(Landroid/content/BroadcastReceiver;)V

    invoke-super {p0}, Landroid/app/Activity;->onStop()V

    return-void
.end method

.method public queryPacakgeSize(Ljava/lang/String;)V
    .locals 8
    .param p1    # Ljava/lang/String;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/lang/Exception;
        }
    .end annotation

    if-eqz p1, :cond_0

    :try_start_0
    sget v2, Landroid/os/Build$VERSION;->SDK_INT:I

    const/4 v1, 0x0

    packed-switch v2, :pswitch_data_0

    :pswitch_0
    const/16 v3, 0x11

    if-le v2, v3, :cond_0

    iget-object v3, p0, Lcom/konka/appassistant/AppAssistantActivity;->pm:Landroid/content/pm/PackageManager;

    invoke-virtual {v3}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v3

    const-string v4, "getPackageSizeInfo"

    const/4 v5, 0x3

    new-array v5, v5, [Ljava/lang/Class;

    const/4 v6, 0x0

    const-class v7, Ljava/lang/String;

    aput-object v7, v5, v6

    const/4 v6, 0x1

    sget-object v7, Ljava/lang/Integer;->TYPE:Ljava/lang/Class;

    aput-object v7, v5, v6

    const/4 v6, 0x2

    const-class v7, Landroid/content/pm/IPackageStatsObserver;

    aput-object v7, v5, v6

    invoke-virtual {v3, v4, v5}, Ljava/lang/Class;->getDeclaredMethod(Ljava/lang/String;[Ljava/lang/Class;)Ljava/lang/reflect/Method;

    move-result-object v1

    iget-object v3, p0, Lcom/konka/appassistant/AppAssistantActivity;->pm:Landroid/content/pm/PackageManager;

    const/4 v4, 0x3

    new-array v4, v4, [Ljava/lang/Object;

    const/4 v5, 0x0

    aput-object p1, v4, v5

    const/4 v5, 0x1

    invoke-static {}, Landroid/os/Process;->myUid()I

    move-result v6

    const v7, 0x186a0

    div-int/2addr v6, v7

    invoke-static {v6}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v6

    aput-object v6, v4, v5

    const/4 v5, 0x2

    new-instance v6, Lcom/konka/appassistant/AppAssistantActivity$PkgSizeObserver;

    invoke-direct {v6, p0}, Lcom/konka/appassistant/AppAssistantActivity$PkgSizeObserver;-><init>(Lcom/konka/appassistant/AppAssistantActivity;)V

    aput-object v6, v4, v5

    invoke-virtual {v1, v3, v4}, Ljava/lang/reflect/Method;->invoke(Ljava/lang/Object;[Ljava/lang/Object;)Ljava/lang/Object;

    :cond_0
    :goto_0
    return-void

    :pswitch_1
    iget-object v3, p0, Lcom/konka/appassistant/AppAssistantActivity;->pm:Landroid/content/pm/PackageManager;

    invoke-virtual {v3}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v3

    const-string v4, "getPackageSizeInfo"

    const/4 v5, 0x2

    new-array v5, v5, [Ljava/lang/Class;

    const/4 v6, 0x0

    const-class v7, Ljava/lang/String;

    aput-object v7, v5, v6

    const/4 v6, 0x1

    const-class v7, Landroid/content/pm/IPackageStatsObserver;

    aput-object v7, v5, v6

    invoke-virtual {v3, v4, v5}, Ljava/lang/Class;->getDeclaredMethod(Ljava/lang/String;[Ljava/lang/Class;)Ljava/lang/reflect/Method;

    move-result-object v1

    iget-object v3, p0, Lcom/konka/appassistant/AppAssistantActivity;->pm:Landroid/content/pm/PackageManager;

    const/4 v4, 0x2

    new-array v4, v4, [Ljava/lang/Object;

    const/4 v5, 0x0

    aput-object p1, v4, v5

    const/4 v5, 0x1

    new-instance v6, Lcom/konka/appassistant/AppAssistantActivity$PkgSizeObserver;

    invoke-direct {v6, p0}, Lcom/konka/appassistant/AppAssistantActivity$PkgSizeObserver;-><init>(Lcom/konka/appassistant/AppAssistantActivity;)V

    aput-object v6, v4, v5

    invoke-virtual {v1, v3, v4}, Ljava/lang/reflect/Method;->invoke(Ljava/lang/Object;[Ljava/lang/Object;)Ljava/lang/Object;
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    goto :goto_0

    :catch_0
    move-exception v0

    const-string v3, "AppAssistant"

    const-string v4, "NoSuchMethodException"

    invoke-static {v3, v4}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    invoke-virtual {v0}, Ljava/lang/Exception;->printStackTrace()V

    throw v0

    :pswitch_2
    :try_start_1
    iget-object v3, p0, Lcom/konka/appassistant/AppAssistantActivity;->pm:Landroid/content/pm/PackageManager;

    invoke-virtual {v3}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v3

    const-string v4, "getPackageSizeInfo"

    const/4 v5, 0x3

    new-array v5, v5, [Ljava/lang/Class;

    const/4 v6, 0x0

    const-class v7, Ljava/lang/String;

    aput-object v7, v5, v6

    const/4 v6, 0x1

    sget-object v7, Ljava/lang/Integer;->TYPE:Ljava/lang/Class;

    aput-object v7, v5, v6

    const/4 v6, 0x2

    const-class v7, Landroid/content/pm/IPackageStatsObserver;

    aput-object v7, v5, v6

    invoke-virtual {v3, v4, v5}, Ljava/lang/Class;->getDeclaredMethod(Ljava/lang/String;[Ljava/lang/Class;)Ljava/lang/reflect/Method;

    move-result-object v1

    iget-object v3, p0, Lcom/konka/appassistant/AppAssistantActivity;->pm:Landroid/content/pm/PackageManager;

    const/4 v4, 0x3

    new-array v4, v4, [Ljava/lang/Object;

    const/4 v5, 0x0

    aput-object p1, v4, v5

    const/4 v5, 0x1

    invoke-static {}, Landroid/os/Process;->myUid()I

    move-result v6

    const v7, 0x186a0

    div-int/2addr v6, v7

    invoke-static {v6}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v6

    aput-object v6, v4, v5

    const/4 v5, 0x2

    new-instance v6, Lcom/konka/appassistant/AppAssistantActivity$PkgSizeObserver;

    invoke-direct {v6, p0}, Lcom/konka/appassistant/AppAssistantActivity$PkgSizeObserver;-><init>(Lcom/konka/appassistant/AppAssistantActivity;)V

    aput-object v6, v4, v5

    invoke-virtual {v1, v3, v4}, Ljava/lang/reflect/Method;->invoke(Ljava/lang/Object;[Ljava/lang/Object;)Ljava/lang/Object;
    :try_end_1
    .catch Ljava/lang/Exception; {:try_start_1 .. :try_end_1} :catch_0

    goto :goto_0

    nop

    :pswitch_data_0
    .packed-switch 0xf
        :pswitch_1
        :pswitch_0
        :pswitch_2
    .end packed-switch
.end method

.method public removeInstallList(I)V
    .locals 6
    .param p1    # I

    const v5, 0x7f060009    # com.konka.appassistant.R.string.app_assistant_second_menu_result_fail

    const/4 v4, 0x0

    if-ltz p1, :cond_0

    iget-object v0, p0, Lcom/konka/appassistant/AppAssistantActivity;->mInstallList:Ljava/util/List;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/konka/appassistant/AppAssistantActivity;->mInstallList:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->size()I

    move-result v0

    if-lt p1, v0, :cond_1

    :cond_0
    :goto_0
    return-void

    :cond_1
    iget-object v0, p0, Lcom/konka/appassistant/AppAssistantActivity;->mInstallList:Ljava/util/List;

    invoke-interface {v0, p1}, Ljava/util/List;->remove(I)Ljava/lang/Object;

    sget-object v0, Lcom/konka/appassistant/ApplicationInfoView$ViewType;->INSTALL_VIEW:Lcom/konka/appassistant/ApplicationInfoView$ViewType;

    iput-object v0, p0, Lcom/konka/appassistant/AppAssistantActivity;->mViewType:Lcom/konka/appassistant/ApplicationInfoView$ViewType;

    iget-object v0, p0, Lcom/konka/appassistant/AppAssistantActivity;->mRefreshButton:Landroid/widget/Button;

    invoke-virtual {v0}, Landroid/widget/Button;->requestFocus()Z

    add-int/lit8 v0, p1, -0x1

    sput v0, Lcom/konka/appassistant/AppAssistantActivity;->mLastFocusedItemNum:I

    sget v0, Lcom/konka/appassistant/AppAssistantActivity;->mLastFocusedItemNum:I

    if-gez v0, :cond_2

    sput v4, Lcom/konka/appassistant/AppAssistantActivity;->mLastFocusedItemNum:I

    :cond_2
    iget-object v0, p0, Lcom/konka/appassistant/AppAssistantActivity;->mResultNum:Landroid/widget/TextView;

    invoke-virtual {v0, v4}, Landroid/widget/TextView;->setVisibility(I)V

    iget-object v0, p0, Lcom/konka/appassistant/AppAssistantActivity;->mProgressBarContainer:Landroid/widget/LinearLayout;

    const/16 v1, 0x8

    invoke-virtual {v0, v1}, Landroid/widget/LinearLayout;->setVisibility(I)V

    iget-object v0, p0, Lcom/konka/appassistant/AppAssistantActivity;->mApplicationList:Lcom/konka/appassistant/ApplicationList;

    if-eqz v0, :cond_3

    iget-object v0, p0, Lcom/konka/appassistant/AppAssistantActivity;->mApplicationList:Lcom/konka/appassistant/ApplicationList;

    invoke-virtual {v0, p1}, Lcom/konka/appassistant/ApplicationList;->RemoveView(I)V

    :cond_3
    iget-object v0, p0, Lcom/konka/appassistant/AppAssistantActivity;->mResultNum:Landroid/widget/TextView;

    const v1, 0x7f060006    # com.konka.appassistant.R.string.app_assistant_second_menu_app_num_install

    invoke-virtual {p0, v1}, Lcom/konka/appassistant/AppAssistantActivity;->getString(I)Ljava/lang/String;

    move-result-object v1

    const/4 v2, 0x1

    new-array v2, v2, [Ljava/lang/Object;

    iget-object v3, p0, Lcom/konka/appassistant/AppAssistantActivity;->mInstallList:Ljava/util/List;

    invoke-interface {v3}, Ljava/util/List;->size()I

    move-result v3

    invoke-static {v3}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v3

    aput-object v3, v2, v4

    invoke-static {v1, v2}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    iget-object v0, p0, Lcom/konka/appassistant/AppAssistantActivity;->mInstallList:Ljava/util/List;

    if-nez v0, :cond_4

    iget-object v0, p0, Lcom/konka/appassistant/AppAssistantActivity;->mRefreshFailContainer:Landroid/widget/LinearLayout;

    invoke-virtual {v0, v4}, Landroid/widget/LinearLayout;->setVisibility(I)V

    iget-object v0, p0, Lcom/konka/appassistant/AppAssistantActivity;->mRefreshFail:Landroid/widget/TextView;

    invoke-virtual {p0, v5}, Lcom/konka/appassistant/AppAssistantActivity;->getString(I)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    goto :goto_0

    :cond_4
    iget-object v0, p0, Lcom/konka/appassistant/AppAssistantActivity;->mInstallList:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->size()I

    move-result v0

    if-nez v0, :cond_6

    iget-object v0, p0, Lcom/konka/appassistant/AppAssistantActivity;->mRefreshFailContainer:Landroid/widget/LinearLayout;

    invoke-virtual {v0, v4}, Landroid/widget/LinearLayout;->setVisibility(I)V

    iget-object v0, p0, Lcom/konka/appassistant/AppAssistantActivity;->mStorage:Lcom/konka/appassistant/Storage;

    invoke-virtual {v0}, Lcom/konka/appassistant/Storage;->isSDMounted()Z

    move-result v0

    if-nez v0, :cond_5

    iget-object v0, p0, Lcom/konka/appassistant/AppAssistantActivity;->mRefreshFail:Landroid/widget/TextView;

    invoke-virtual {p0, v5}, Lcom/konka/appassistant/AppAssistantActivity;->getString(I)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    goto :goto_0

    :cond_5
    iget-object v0, p0, Lcom/konka/appassistant/AppAssistantActivity;->mRefreshFail:Landroid/widget/TextView;

    const v1, 0x7f06000a    # com.konka.appassistant.R.string.app_assistant_second_menu_result_no_apk

    invoke-virtual {p0, v1}, Lcom/konka/appassistant/AppAssistantActivity;->getString(I)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    goto/16 :goto_0

    :cond_6
    iget-object v0, p0, Lcom/konka/appassistant/AppAssistantActivity;->mScrollView:Landroid/widget/ScrollView;

    invoke-virtual {v0, v4}, Landroid/widget/ScrollView;->setVisibility(I)V

    goto/16 :goto_0
.end method

.method public requestFocus(Lcom/konka/appassistant/ApplicationInfoView$ViewType;)V
    .locals 2
    .param p1    # Lcom/konka/appassistant/ApplicationInfoView$ViewType;

    invoke-static {}, Lcom/konka/appassistant/AppAssistantActivity;->$SWITCH_TABLE$com$konka$appassistant$ApplicationInfoView$ViewType()[I

    move-result-object v0

    invoke-virtual {p1}, Lcom/konka/appassistant/ApplicationInfoView$ViewType;->ordinal()I

    move-result v1

    aget v0, v0, v1

    packed-switch v0, :pswitch_data_0

    :goto_0
    return-void

    :pswitch_0
    iget-object v0, p0, Lcom/konka/appassistant/AppAssistantActivity;->mInstall_button:Landroid/widget/TextView;

    invoke-virtual {v0}, Landroid/widget/TextView;->requestFocus()Z

    goto :goto_0

    :pswitch_1
    iget-object v0, p0, Lcom/konka/appassistant/AppAssistantActivity;->mUninstall_button:Landroid/widget/TextView;

    invoke-virtual {v0}, Landroid/widget/TextView;->requestFocus()Z

    goto :goto_0

    :pswitch_2
    iget-object v0, p0, Lcom/konka/appassistant/AppAssistantActivity;->mZipInstall_button:Landroid/widget/TextView;

    invoke-virtual {v0}, Landroid/widget/TextView;->requestFocus()Z

    goto :goto_0

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
        :pswitch_1
        :pswitch_2
    .end packed-switch
.end method

.method public sendScrollMsg()V
    .locals 2

    iget-object v0, p0, Lcom/konka/appassistant/AppAssistantActivity;->mMainHandler:Landroid/os/Handler;

    const/16 v1, 0x67

    invoke-virtual {v0, v1}, Landroid/os/Handler;->sendEmptyMessage(I)Z

    return-void
.end method
