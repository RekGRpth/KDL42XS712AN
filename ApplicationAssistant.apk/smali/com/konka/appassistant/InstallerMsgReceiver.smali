.class public Lcom/konka/appassistant/InstallerMsgReceiver;
.super Landroid/content/BroadcastReceiver;
.source "InstallerMsgReceiver.java"


# direct methods
.method public constructor <init>()V
    .locals 0

    invoke-direct {p0}, Landroid/content/BroadcastReceiver;-><init>()V

    return-void
.end method


# virtual methods
.method public onReceive(Landroid/content/Context;Landroid/content/Intent;)V
    .locals 5
    .param p1    # Landroid/content/Context;
    .param p2    # Landroid/content/Intent;

    const-string v3, "ttttt"

    const-string v4, "InstallerMsgReceiver:onReceive"

    invoke-static {v3, v4}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    invoke-virtual {p2}, Landroid/content/Intent;->getAction()Ljava/lang/String;

    move-result-object v3

    const-string v4, "com.konka.ACTION.SILENT_INSTALL_COMPLETE"

    invoke-virtual {v3, v4}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-eqz v3, :cond_0

    const-string v3, "ttttt"

    const-string v4, "com.konka.ACTION.SILENT_INSTALL_COMPLETE"

    invoke-static {v3, v4}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    const-string v3, "SILENT_INSTALL_RESULT"

    const/4 v4, 0x1

    invoke-virtual {p2, v3, v4}, Landroid/content/Intent;->getIntExtra(Ljava/lang/String;I)I

    move-result v2

    const-string v3, "FILE_NAME"

    invoke-virtual {p2, v3}, Landroid/content/Intent;->getStringExtra(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    const-string v3, "PACKAGE_NAME"

    invoke-virtual {p2, v3}, Landroid/content/Intent;->getStringExtra(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    invoke-static {}, Lcom/konka/appassistant/OneKeyInstallProcess;->getInstance()Lcom/konka/appassistant/OneKeyInstallProcess;

    move-result-object v3

    invoke-virtual {v3, v2, v0, v1}, Lcom/konka/appassistant/OneKeyInstallProcess;->onInstallOneApkFinished(ILjava/lang/String;Ljava/lang/String;)V

    :cond_0
    return-void
.end method
