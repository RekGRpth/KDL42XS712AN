.class public Lcom/konka/appassistant/AppAssistantActivity$PkgSizeObserver;
.super Landroid/content/pm/IPackageStatsObserver$Stub;
.source "AppAssistantActivity.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/konka/appassistant/AppAssistantActivity;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x1
    name = "PkgSizeObserver"
.end annotation


# instance fields
.field final synthetic this$0:Lcom/konka/appassistant/AppAssistantActivity;


# direct methods
.method public constructor <init>(Lcom/konka/appassistant/AppAssistantActivity;)V
    .locals 0

    iput-object p1, p0, Lcom/konka/appassistant/AppAssistantActivity$PkgSizeObserver;->this$0:Lcom/konka/appassistant/AppAssistantActivity;

    invoke-direct {p0}, Landroid/content/pm/IPackageStatsObserver$Stub;-><init>()V

    return-void
.end method


# virtual methods
.method public onGetStatsCompleted(Landroid/content/pm/PackageStats;Z)V
    .locals 5
    .param p1    # Landroid/content/pm/PackageStats;
    .param p2    # Z
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Landroid/os/RemoteException;
        }
    .end annotation

    iget-object v0, p0, Lcom/konka/appassistant/AppAssistantActivity$PkgSizeObserver;->this$0:Lcom/konka/appassistant/AppAssistantActivity;

    iget-wide v1, p1, Landroid/content/pm/PackageStats;->cacheSize:J

    invoke-static {v0, v1, v2}, Lcom/konka/appassistant/AppAssistantActivity;->access$35(Lcom/konka/appassistant/AppAssistantActivity;J)V

    iget-object v0, p0, Lcom/konka/appassistant/AppAssistantActivity$PkgSizeObserver;->this$0:Lcom/konka/appassistant/AppAssistantActivity;

    iget-wide v1, p1, Landroid/content/pm/PackageStats;->dataSize:J

    invoke-static {v0, v1, v2}, Lcom/konka/appassistant/AppAssistantActivity;->access$36(Lcom/konka/appassistant/AppAssistantActivity;J)V

    iget-object v0, p0, Lcom/konka/appassistant/AppAssistantActivity$PkgSizeObserver;->this$0:Lcom/konka/appassistant/AppAssistantActivity;

    iget-wide v1, p1, Landroid/content/pm/PackageStats;->codeSize:J

    invoke-static {v0, v1, v2}, Lcom/konka/appassistant/AppAssistantActivity;->access$37(Lcom/konka/appassistant/AppAssistantActivity;J)V

    iget-object v0, p0, Lcom/konka/appassistant/AppAssistantActivity$PkgSizeObserver;->this$0:Lcom/konka/appassistant/AppAssistantActivity;

    iget-object v1, p0, Lcom/konka/appassistant/AppAssistantActivity$PkgSizeObserver;->this$0:Lcom/konka/appassistant/AppAssistantActivity;

    # getter for: Lcom/konka/appassistant/AppAssistantActivity;->cachesize:J
    invoke-static {v1}, Lcom/konka/appassistant/AppAssistantActivity;->access$38(Lcom/konka/appassistant/AppAssistantActivity;)J

    move-result-wide v1

    iget-object v3, p0, Lcom/konka/appassistant/AppAssistantActivity$PkgSizeObserver;->this$0:Lcom/konka/appassistant/AppAssistantActivity;

    # getter for: Lcom/konka/appassistant/AppAssistantActivity;->datasize:J
    invoke-static {v3}, Lcom/konka/appassistant/AppAssistantActivity;->access$39(Lcom/konka/appassistant/AppAssistantActivity;)J

    move-result-wide v3

    add-long/2addr v1, v3

    iget-object v3, p0, Lcom/konka/appassistant/AppAssistantActivity$PkgSizeObserver;->this$0:Lcom/konka/appassistant/AppAssistantActivity;

    # getter for: Lcom/konka/appassistant/AppAssistantActivity;->codesize:J
    invoke-static {v3}, Lcom/konka/appassistant/AppAssistantActivity;->access$40(Lcom/konka/appassistant/AppAssistantActivity;)J

    move-result-wide v3

    add-long/2addr v1, v3

    invoke-static {v0, v1, v2}, Lcom/konka/appassistant/AppAssistantActivity;->access$41(Lcom/konka/appassistant/AppAssistantActivity;J)V

    iget-object v0, p0, Lcom/konka/appassistant/AppAssistantActivity$PkgSizeObserver;->this$0:Lcom/konka/appassistant/AppAssistantActivity;

    iget-object v1, p1, Landroid/content/pm/PackageStats;->packageName:Ljava/lang/String;

    iget-object v2, p0, Lcom/konka/appassistant/AppAssistantActivity$PkgSizeObserver;->this$0:Lcom/konka/appassistant/AppAssistantActivity;

    # getter for: Lcom/konka/appassistant/AppAssistantActivity;->totalsize:J
    invoke-static {v2}, Lcom/konka/appassistant/AppAssistantActivity;->access$42(Lcom/konka/appassistant/AppAssistantActivity;)J

    move-result-wide v2

    # invokes: Lcom/konka/appassistant/AppAssistantActivity;->setAppSize(Ljava/lang/String;J)V
    invoke-static {v0, v1, v2, v3}, Lcom/konka/appassistant/AppAssistantActivity;->access$43(Lcom/konka/appassistant/AppAssistantActivity;Ljava/lang/String;J)V

    return-void
.end method
