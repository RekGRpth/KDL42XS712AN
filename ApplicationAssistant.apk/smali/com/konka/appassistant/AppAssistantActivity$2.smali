.class Lcom/konka/appassistant/AppAssistantActivity$2;
.super Ljava/lang/Object;
.source "AppAssistantActivity.java"

# interfaces
.implements Landroid/view/View$OnClickListener;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/konka/appassistant/AppAssistantActivity;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/konka/appassistant/AppAssistantActivity;


# direct methods
.method constructor <init>(Lcom/konka/appassistant/AppAssistantActivity;)V
    .locals 0

    iput-object p1, p0, Lcom/konka/appassistant/AppAssistantActivity$2;->this$0:Lcom/konka/appassistant/AppAssistantActivity;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onClick(Landroid/view/View;)V
    .locals 2
    .param p1    # Landroid/view/View;

    iget-object v0, p0, Lcom/konka/appassistant/AppAssistantActivity$2;->this$0:Lcom/konka/appassistant/AppAssistantActivity;

    # getter for: Lcom/konka/appassistant/AppAssistantActivity;->mViewType:Lcom/konka/appassistant/ApplicationInfoView$ViewType;
    invoke-static {v0}, Lcom/konka/appassistant/AppAssistantActivity;->access$0(Lcom/konka/appassistant/AppAssistantActivity;)Lcom/konka/appassistant/ApplicationInfoView$ViewType;

    move-result-object v0

    sget-object v1, Lcom/konka/appassistant/ApplicationInfoView$ViewType;->ZIP_INSTALL_VIEW:Lcom/konka/appassistant/ApplicationInfoView$ViewType;

    if-eq v0, v1, :cond_2

    iget-object v0, p0, Lcom/konka/appassistant/AppAssistantActivity$2;->this$0:Lcom/konka/appassistant/AppAssistantActivity;

    # getter for: Lcom/konka/appassistant/AppAssistantActivity;->IsThreadRun:Ljava/lang/Boolean;
    invoke-static {v0}, Lcom/konka/appassistant/AppAssistantActivity;->access$1(Lcom/konka/appassistant/AppAssistantActivity;)Ljava/lang/Boolean;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v0

    if-nez v0, :cond_0

    iget-object v0, p0, Lcom/konka/appassistant/AppAssistantActivity$2;->this$0:Lcom/konka/appassistant/AppAssistantActivity;

    # getter for: Lcom/konka/appassistant/AppAssistantActivity;->mProgressBarContainer:Landroid/widget/LinearLayout;
    invoke-static {v0}, Lcom/konka/appassistant/AppAssistantActivity;->access$2(Lcom/konka/appassistant/AppAssistantActivity;)Landroid/widget/LinearLayout;

    move-result-object v0

    invoke-virtual {v0}, Landroid/widget/LinearLayout;->getVisibility()I

    move-result v0

    const/16 v1, 0x8

    if-ne v0, v1, :cond_0

    iget-object v0, p0, Lcom/konka/appassistant/AppAssistantActivity$2;->this$0:Lcom/konka/appassistant/AppAssistantActivity;

    invoke-virtual {v0}, Lcom/konka/appassistant/AppAssistantActivity;->Refresh()V

    :cond_0
    invoke-virtual {p1}, Landroid/view/View;->requestFocus()Z

    :cond_1
    :goto_0
    return-void

    :cond_2
    iget-object v0, p0, Lcom/konka/appassistant/AppAssistantActivity$2;->this$0:Lcom/konka/appassistant/AppAssistantActivity;

    # getter for: Lcom/konka/appassistant/AppAssistantActivity;->mZipInstalling:Ljava/lang/Boolean;
    invoke-static {v0}, Lcom/konka/appassistant/AppAssistantActivity;->access$3(Lcom/konka/appassistant/AppAssistantActivity;)Ljava/lang/Boolean;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v0

    if-nez v0, :cond_1

    iget-object v0, p0, Lcom/konka/appassistant/AppAssistantActivity$2;->this$0:Lcom/konka/appassistant/AppAssistantActivity;

    const/4 v1, 0x0

    invoke-static {v0, v1}, Lcom/konka/appassistant/AppAssistantActivity;->access$4(Lcom/konka/appassistant/AppAssistantActivity;I)V

    iget-object v0, p0, Lcom/konka/appassistant/AppAssistantActivity$2;->this$0:Lcom/konka/appassistant/AppAssistantActivity;

    # getter for: Lcom/konka/appassistant/AppAssistantActivity;->mZipInstallList:Ljava/util/List;
    invoke-static {v0}, Lcom/konka/appassistant/AppAssistantActivity;->access$5(Lcom/konka/appassistant/AppAssistantActivity;)Ljava/util/List;

    move-result-object v0

    invoke-interface {v0}, Ljava/util/List;->clear()V

    iget-object v0, p0, Lcom/konka/appassistant/AppAssistantActivity$2;->this$0:Lcom/konka/appassistant/AppAssistantActivity;

    # getter for: Lcom/konka/appassistant/AppAssistantActivity;->mZipAppInfo:Ljava/util/List;
    invoke-static {v0}, Lcom/konka/appassistant/AppAssistantActivity;->access$6(Lcom/konka/appassistant/AppAssistantActivity;)Ljava/util/List;

    move-result-object v0

    invoke-interface {v0}, Ljava/util/List;->clear()V

    iget-object v0, p0, Lcom/konka/appassistant/AppAssistantActivity$2;->this$0:Lcom/konka/appassistant/AppAssistantActivity;

    # invokes: Lcom/konka/appassistant/AppAssistantActivity;->refreshView()V
    invoke-static {v0}, Lcom/konka/appassistant/AppAssistantActivity;->access$7(Lcom/konka/appassistant/AppAssistantActivity;)V

    iget-object v0, p0, Lcom/konka/appassistant/AppAssistantActivity$2;->this$0:Lcom/konka/appassistant/AppAssistantActivity;

    # getter for: Lcom/konka/appassistant/AppAssistantActivity;->mOneKeyInstallProcess:Lcom/konka/appassistant/OneKeyInstallProcess;
    invoke-static {v0}, Lcom/konka/appassistant/AppAssistantActivity;->access$8(Lcom/konka/appassistant/AppAssistantActivity;)Lcom/konka/appassistant/OneKeyInstallProcess;

    move-result-object v0

    invoke-virtual {v0}, Lcom/konka/appassistant/OneKeyInstallProcess;->startOneKeyInstallThread()V

    iget-object v0, p0, Lcom/konka/appassistant/AppAssistantActivity$2;->this$0:Lcom/konka/appassistant/AppAssistantActivity;

    const/4 v1, 0x1

    invoke-static {v1}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/konka/appassistant/AppAssistantActivity;->access$9(Lcom/konka/appassistant/AppAssistantActivity;Ljava/lang/Boolean;)V

    goto :goto_0
.end method
