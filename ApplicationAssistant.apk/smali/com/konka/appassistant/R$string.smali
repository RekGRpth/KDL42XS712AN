.class public final Lcom/konka/appassistant/R$string;
.super Ljava/lang/Object;
.source "R.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/konka/appassistant/R;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x19
    name = "string"
.end annotation


# static fields
.field public static final app_assistant_button_install:I = 0x7f060003

.field public static final app_assistant_button_uninstall:I = 0x7f060004

.field public static final app_assistant_button_zipinstall:I = 0x7f060005

.field public static final app_assistant_second_menu_app_num_install:I = 0x7f060006

.field public static final app_assistant_second_menu_app_num_uninstall:I = 0x7f060007

.field public static final app_assistant_second_menu_app_num_zipinstall:I = 0x7f060008

.field public static final app_assistant_second_menu_refresh:I = 0x7f06000c

.field public static final app_assistant_second_menu_result_fail:I = 0x7f060009

.field public static final app_assistant_second_menu_result_no_apk:I = 0x7f06000a

.field public static final app_assistant_second_menu_result_no_apk_uninstall:I = 0x7f06000b

.field public static final app_assistant_title:I = 0x7f060001

.field public static final app_assistant_version_number:I = 0x7f060002

.field public static final app_assistant_zipinstall_failed:I = 0x7f060010

.field public static final app_assistant_zipinstall_install_status:I = 0x7f060014

.field public static final app_assistant_zipinstall_installing:I = 0x7f06000e

.field public static final app_assistant_zipinstall_startinstall:I = 0x7f06000d

.field public static final app_assistant_zipinstall_succeed:I = 0x7f06000f

.field public static final app_assistant_zipinstall_zip_failed:I = 0x7f060013

.field public static final app_assistant_zipinstall_zip_status:I = 0x7f060011

.field public static final app_assistant_zipinstall_zip_succeed:I = 0x7f060012

.field public static final app_info_button_delete:I = 0x7f060017

.field public static final app_info_button_install:I = 0x7f060015

.field public static final app_info_button_uninstall:I = 0x7f060016

.field public static final app_info_size:I = 0x7f060019

.field public static final app_info_status_installed:I = 0x7f06001a

.field public static final app_info_status_not_installed:I = 0x7f06001b

.field public static final app_info_version_num:I = 0x7f060018

.field public static final app_name:I = 0x7f060000

.field public static final delete_sureornot:I = 0x7f060022

.field public static final install_failed_cpu_abi_incompatible:I = 0x7f060020

.field public static final install_failed_inconsistent_certificates:I = 0x7f06001e

.field public static final install_failed_invalid_apk:I = 0x7f06001d

.field public static final install_failed_older_sdk:I = 0x7f06001f

.field public static final install_failed_storage_not_enough:I = 0x7f06001c

.field public static final m_return:I = 0x7f060024

.field public static final m_yes:I = 0x7f060023

.field public static final sureornot:I = 0x7f060021


# direct methods
.method public constructor <init>()V
    .locals 0

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method
