.class Lcom/konka/appassistant/AppAssistantActivity$3;
.super Ljava/lang/Object;
.source "AppAssistantActivity.java"

# interfaces
.implements Landroid/view/View$OnFocusChangeListener;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/konka/appassistant/AppAssistantActivity;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/konka/appassistant/AppAssistantActivity;


# direct methods
.method constructor <init>(Lcom/konka/appassistant/AppAssistantActivity;)V
    .locals 0

    iput-object p1, p0, Lcom/konka/appassistant/AppAssistantActivity$3;->this$0:Lcom/konka/appassistant/AppAssistantActivity;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onFocusChange(Landroid/view/View;Z)V
    .locals 5
    .param p1    # Landroid/view/View;
    .param p2    # Z

    const v1, 0x7f02000f    # com.konka.appassistant.R.drawable.tab_button

    const v4, 0x7f020002    # com.konka.appassistant.R.drawable.button_uns_choice

    const/4 v3, 0x0

    const/4 v2, -0x1

    iget-object v0, p0, Lcom/konka/appassistant/AppAssistantActivity$3;->this$0:Lcom/konka/appassistant/AppAssistantActivity;

    # invokes: Lcom/konka/appassistant/AppAssistantActivity;->tabButtonInit()V
    invoke-static {v0}, Lcom/konka/appassistant/AppAssistantActivity;->access$10(Lcom/konka/appassistant/AppAssistantActivity;)V

    if-eqz p2, :cond_3

    iget-object v0, p0, Lcom/konka/appassistant/AppAssistantActivity$3;->this$0:Lcom/konka/appassistant/AppAssistantActivity;

    # getter for: Lcom/konka/appassistant/AppAssistantActivity;->mInstall_button:Landroid/widget/TextView;
    invoke-static {v0}, Lcom/konka/appassistant/AppAssistantActivity;->access$11(Lcom/konka/appassistant/AppAssistantActivity;)Landroid/widget/TextView;

    move-result-object v0

    if-ne p1, v0, :cond_1

    iget-object v0, p0, Lcom/konka/appassistant/AppAssistantActivity$3;->this$0:Lcom/konka/appassistant/AppAssistantActivity;

    # getter for: Lcom/konka/appassistant/AppAssistantActivity;->mInstall_button:Landroid/widget/TextView;
    invoke-static {v0}, Lcom/konka/appassistant/AppAssistantActivity;->access$11(Lcom/konka/appassistant/AppAssistantActivity;)Landroid/widget/TextView;

    move-result-object v0

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setBackgroundResource(I)V

    iget-object v0, p0, Lcom/konka/appassistant/AppAssistantActivity$3;->this$0:Lcom/konka/appassistant/AppAssistantActivity;

    # getter for: Lcom/konka/appassistant/AppAssistantActivity;->mInstall_button:Landroid/widget/TextView;
    invoke-static {v0}, Lcom/konka/appassistant/AppAssistantActivity;->access$11(Lcom/konka/appassistant/AppAssistantActivity;)Landroid/widget/TextView;

    move-result-object v0

    invoke-virtual {v0, v2}, Landroid/widget/TextView;->setTextColor(I)V

    iget-object v0, p0, Lcom/konka/appassistant/AppAssistantActivity$3;->this$0:Lcom/konka/appassistant/AppAssistantActivity;

    # getter for: Lcom/konka/appassistant/AppAssistantActivity;->mViewType:Lcom/konka/appassistant/ApplicationInfoView$ViewType;
    invoke-static {v0}, Lcom/konka/appassistant/AppAssistantActivity;->access$0(Lcom/konka/appassistant/AppAssistantActivity;)Lcom/konka/appassistant/ApplicationInfoView$ViewType;

    move-result-object v0

    sget-object v1, Lcom/konka/appassistant/ApplicationInfoView$ViewType;->INSTALL_VIEW:Lcom/konka/appassistant/ApplicationInfoView$ViewType;

    if-eq v0, v1, :cond_0

    iget-object v0, p0, Lcom/konka/appassistant/AppAssistantActivity$3;->this$0:Lcom/konka/appassistant/AppAssistantActivity;

    sget-object v1, Lcom/konka/appassistant/ApplicationInfoView$ViewType;->INSTALL_VIEW:Lcom/konka/appassistant/ApplicationInfoView$ViewType;

    invoke-static {v0, v1}, Lcom/konka/appassistant/AppAssistantActivity;->access$12(Lcom/konka/appassistant/AppAssistantActivity;Lcom/konka/appassistant/ApplicationInfoView$ViewType;)V

    iget-object v0, p0, Lcom/konka/appassistant/AppAssistantActivity$3;->this$0:Lcom/konka/appassistant/AppAssistantActivity;

    iget-object v0, v0, Lcom/konka/appassistant/AppAssistantActivity;->mMainHandler:Landroid/os/Handler;

    invoke-virtual {v0, v3}, Landroid/os/Handler;->sendEmptyMessage(I)Z

    :cond_0
    :goto_0
    iget-object v0, p0, Lcom/konka/appassistant/AppAssistantActivity$3;->this$0:Lcom/konka/appassistant/AppAssistantActivity;

    invoke-virtual {v0}, Lcom/konka/appassistant/AppAssistantActivity;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    const v1, 0x7f050009    # com.konka.appassistant.R.dimen.app_assistant_button_icon_paddingleft

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getDimension(I)F

    move-result v0

    float-to-int v0, v0

    invoke-virtual {p1, v0, v3, v3, v3}, Landroid/view/View;->setPadding(IIII)V

    return-void

    :cond_1
    iget-object v0, p0, Lcom/konka/appassistant/AppAssistantActivity$3;->this$0:Lcom/konka/appassistant/AppAssistantActivity;

    # getter for: Lcom/konka/appassistant/AppAssistantActivity;->mUninstall_button:Landroid/widget/TextView;
    invoke-static {v0}, Lcom/konka/appassistant/AppAssistantActivity;->access$13(Lcom/konka/appassistant/AppAssistantActivity;)Landroid/widget/TextView;

    move-result-object v0

    if-ne p1, v0, :cond_2

    iget-object v0, p0, Lcom/konka/appassistant/AppAssistantActivity$3;->this$0:Lcom/konka/appassistant/AppAssistantActivity;

    # getter for: Lcom/konka/appassistant/AppAssistantActivity;->mUninstall_button:Landroid/widget/TextView;
    invoke-static {v0}, Lcom/konka/appassistant/AppAssistantActivity;->access$13(Lcom/konka/appassistant/AppAssistantActivity;)Landroid/widget/TextView;

    move-result-object v0

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setBackgroundResource(I)V

    iget-object v0, p0, Lcom/konka/appassistant/AppAssistantActivity$3;->this$0:Lcom/konka/appassistant/AppAssistantActivity;

    # getter for: Lcom/konka/appassistant/AppAssistantActivity;->mUninstall_button:Landroid/widget/TextView;
    invoke-static {v0}, Lcom/konka/appassistant/AppAssistantActivity;->access$13(Lcom/konka/appassistant/AppAssistantActivity;)Landroid/widget/TextView;

    move-result-object v0

    invoke-virtual {v0, v2}, Landroid/widget/TextView;->setTextColor(I)V

    iget-object v0, p0, Lcom/konka/appassistant/AppAssistantActivity$3;->this$0:Lcom/konka/appassistant/AppAssistantActivity;

    # getter for: Lcom/konka/appassistant/AppAssistantActivity;->mViewType:Lcom/konka/appassistant/ApplicationInfoView$ViewType;
    invoke-static {v0}, Lcom/konka/appassistant/AppAssistantActivity;->access$0(Lcom/konka/appassistant/AppAssistantActivity;)Lcom/konka/appassistant/ApplicationInfoView$ViewType;

    move-result-object v0

    sget-object v1, Lcom/konka/appassistant/ApplicationInfoView$ViewType;->UNINSTALL_VIEW:Lcom/konka/appassistant/ApplicationInfoView$ViewType;

    if-eq v0, v1, :cond_0

    iget-object v0, p0, Lcom/konka/appassistant/AppAssistantActivity$3;->this$0:Lcom/konka/appassistant/AppAssistantActivity;

    sget-object v1, Lcom/konka/appassistant/ApplicationInfoView$ViewType;->UNINSTALL_VIEW:Lcom/konka/appassistant/ApplicationInfoView$ViewType;

    invoke-static {v0, v1}, Lcom/konka/appassistant/AppAssistantActivity;->access$12(Lcom/konka/appassistant/AppAssistantActivity;Lcom/konka/appassistant/ApplicationInfoView$ViewType;)V

    iget-object v0, p0, Lcom/konka/appassistant/AppAssistantActivity$3;->this$0:Lcom/konka/appassistant/AppAssistantActivity;

    iget-object v0, v0, Lcom/konka/appassistant/AppAssistantActivity;->mMainHandler:Landroid/os/Handler;

    invoke-virtual {v0, v3}, Landroid/os/Handler;->sendEmptyMessage(I)Z

    goto :goto_0

    :cond_2
    iget-object v0, p0, Lcom/konka/appassistant/AppAssistantActivity$3;->this$0:Lcom/konka/appassistant/AppAssistantActivity;

    # getter for: Lcom/konka/appassistant/AppAssistantActivity;->mZipInstall_button:Landroid/widget/TextView;
    invoke-static {v0}, Lcom/konka/appassistant/AppAssistantActivity;->access$14(Lcom/konka/appassistant/AppAssistantActivity;)Landroid/widget/TextView;

    move-result-object v0

    if-ne p1, v0, :cond_0

    iget-object v0, p0, Lcom/konka/appassistant/AppAssistantActivity$3;->this$0:Lcom/konka/appassistant/AppAssistantActivity;

    # getter for: Lcom/konka/appassistant/AppAssistantActivity;->mZipInstall_button:Landroid/widget/TextView;
    invoke-static {v0}, Lcom/konka/appassistant/AppAssistantActivity;->access$14(Lcom/konka/appassistant/AppAssistantActivity;)Landroid/widget/TextView;

    move-result-object v0

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setBackgroundResource(I)V

    iget-object v0, p0, Lcom/konka/appassistant/AppAssistantActivity$3;->this$0:Lcom/konka/appassistant/AppAssistantActivity;

    # getter for: Lcom/konka/appassistant/AppAssistantActivity;->mZipInstall_button:Landroid/widget/TextView;
    invoke-static {v0}, Lcom/konka/appassistant/AppAssistantActivity;->access$14(Lcom/konka/appassistant/AppAssistantActivity;)Landroid/widget/TextView;

    move-result-object v0

    invoke-virtual {v0, v2}, Landroid/widget/TextView;->setTextColor(I)V

    iget-object v0, p0, Lcom/konka/appassistant/AppAssistantActivity$3;->this$0:Lcom/konka/appassistant/AppAssistantActivity;

    # getter for: Lcom/konka/appassistant/AppAssistantActivity;->mViewType:Lcom/konka/appassistant/ApplicationInfoView$ViewType;
    invoke-static {v0}, Lcom/konka/appassistant/AppAssistantActivity;->access$0(Lcom/konka/appassistant/AppAssistantActivity;)Lcom/konka/appassistant/ApplicationInfoView$ViewType;

    move-result-object v0

    sget-object v1, Lcom/konka/appassistant/ApplicationInfoView$ViewType;->ZIP_INSTALL_VIEW:Lcom/konka/appassistant/ApplicationInfoView$ViewType;

    if-eq v0, v1, :cond_0

    iget-object v0, p0, Lcom/konka/appassistant/AppAssistantActivity$3;->this$0:Lcom/konka/appassistant/AppAssistantActivity;

    sget-object v1, Lcom/konka/appassistant/ApplicationInfoView$ViewType;->ZIP_INSTALL_VIEW:Lcom/konka/appassistant/ApplicationInfoView$ViewType;

    invoke-static {v0, v1}, Lcom/konka/appassistant/AppAssistantActivity;->access$12(Lcom/konka/appassistant/AppAssistantActivity;Lcom/konka/appassistant/ApplicationInfoView$ViewType;)V

    iget-object v0, p0, Lcom/konka/appassistant/AppAssistantActivity$3;->this$0:Lcom/konka/appassistant/AppAssistantActivity;

    iget-object v0, v0, Lcom/konka/appassistant/AppAssistantActivity;->mMainHandler:Landroid/os/Handler;

    invoke-virtual {v0, v3}, Landroid/os/Handler;->sendEmptyMessage(I)Z

    goto :goto_0

    :cond_3
    iget-object v0, p0, Lcom/konka/appassistant/AppAssistantActivity$3;->this$0:Lcom/konka/appassistant/AppAssistantActivity;

    # getter for: Lcom/konka/appassistant/AppAssistantActivity;->mInstall_button:Landroid/widget/TextView;
    invoke-static {v0}, Lcom/konka/appassistant/AppAssistantActivity;->access$11(Lcom/konka/appassistant/AppAssistantActivity;)Landroid/widget/TextView;

    move-result-object v0

    if-ne p1, v0, :cond_4

    iget-object v0, p0, Lcom/konka/appassistant/AppAssistantActivity$3;->this$0:Lcom/konka/appassistant/AppAssistantActivity;

    # getter for: Lcom/konka/appassistant/AppAssistantActivity;->mInstall_button:Landroid/widget/TextView;
    invoke-static {v0}, Lcom/konka/appassistant/AppAssistantActivity;->access$11(Lcom/konka/appassistant/AppAssistantActivity;)Landroid/widget/TextView;

    move-result-object v0

    invoke-virtual {v0, v2}, Landroid/widget/TextView;->setTextColor(I)V

    iget-object v0, p0, Lcom/konka/appassistant/AppAssistantActivity$3;->this$0:Lcom/konka/appassistant/AppAssistantActivity;

    # invokes: Lcom/konka/appassistant/AppAssistantActivity;->IsTabButtonFocused()Ljava/lang/Boolean;
    invoke-static {v0}, Lcom/konka/appassistant/AppAssistantActivity;->access$15(Lcom/konka/appassistant/AppAssistantActivity;)Ljava/lang/Boolean;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v0

    if-nez v0, :cond_0

    iget-object v0, p0, Lcom/konka/appassistant/AppAssistantActivity$3;->this$0:Lcom/konka/appassistant/AppAssistantActivity;

    # getter for: Lcom/konka/appassistant/AppAssistantActivity;->mInstall_button:Landroid/widget/TextView;
    invoke-static {v0}, Lcom/konka/appassistant/AppAssistantActivity;->access$11(Lcom/konka/appassistant/AppAssistantActivity;)Landroid/widget/TextView;

    move-result-object v0

    invoke-virtual {v0, v2}, Landroid/widget/TextView;->setTextColor(I)V

    iget-object v0, p0, Lcom/konka/appassistant/AppAssistantActivity$3;->this$0:Lcom/konka/appassistant/AppAssistantActivity;

    # getter for: Lcom/konka/appassistant/AppAssistantActivity;->mInstall_button:Landroid/widget/TextView;
    invoke-static {v0}, Lcom/konka/appassistant/AppAssistantActivity;->access$11(Lcom/konka/appassistant/AppAssistantActivity;)Landroid/widget/TextView;

    move-result-object v0

    invoke-virtual {v0, v4}, Landroid/widget/TextView;->setBackgroundResource(I)V

    goto/16 :goto_0

    :cond_4
    iget-object v0, p0, Lcom/konka/appassistant/AppAssistantActivity$3;->this$0:Lcom/konka/appassistant/AppAssistantActivity;

    # getter for: Lcom/konka/appassistant/AppAssistantActivity;->mUninstall_button:Landroid/widget/TextView;
    invoke-static {v0}, Lcom/konka/appassistant/AppAssistantActivity;->access$13(Lcom/konka/appassistant/AppAssistantActivity;)Landroid/widget/TextView;

    move-result-object v0

    if-ne p1, v0, :cond_5

    iget-object v0, p0, Lcom/konka/appassistant/AppAssistantActivity$3;->this$0:Lcom/konka/appassistant/AppAssistantActivity;

    # getter for: Lcom/konka/appassistant/AppAssistantActivity;->mUninstall_button:Landroid/widget/TextView;
    invoke-static {v0}, Lcom/konka/appassistant/AppAssistantActivity;->access$13(Lcom/konka/appassistant/AppAssistantActivity;)Landroid/widget/TextView;

    move-result-object v0

    invoke-virtual {v0, v2}, Landroid/widget/TextView;->setTextColor(I)V

    iget-object v0, p0, Lcom/konka/appassistant/AppAssistantActivity$3;->this$0:Lcom/konka/appassistant/AppAssistantActivity;

    # invokes: Lcom/konka/appassistant/AppAssistantActivity;->IsTabButtonFocused()Ljava/lang/Boolean;
    invoke-static {v0}, Lcom/konka/appassistant/AppAssistantActivity;->access$15(Lcom/konka/appassistant/AppAssistantActivity;)Ljava/lang/Boolean;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v0

    if-nez v0, :cond_0

    iget-object v0, p0, Lcom/konka/appassistant/AppAssistantActivity$3;->this$0:Lcom/konka/appassistant/AppAssistantActivity;

    # getter for: Lcom/konka/appassistant/AppAssistantActivity;->mUninstall_button:Landroid/widget/TextView;
    invoke-static {v0}, Lcom/konka/appassistant/AppAssistantActivity;->access$13(Lcom/konka/appassistant/AppAssistantActivity;)Landroid/widget/TextView;

    move-result-object v0

    invoke-virtual {v0, v2}, Landroid/widget/TextView;->setTextColor(I)V

    iget-object v0, p0, Lcom/konka/appassistant/AppAssistantActivity$3;->this$0:Lcom/konka/appassistant/AppAssistantActivity;

    # getter for: Lcom/konka/appassistant/AppAssistantActivity;->mUninstall_button:Landroid/widget/TextView;
    invoke-static {v0}, Lcom/konka/appassistant/AppAssistantActivity;->access$13(Lcom/konka/appassistant/AppAssistantActivity;)Landroid/widget/TextView;

    move-result-object v0

    invoke-virtual {v0, v4}, Landroid/widget/TextView;->setBackgroundResource(I)V

    goto/16 :goto_0

    :cond_5
    iget-object v0, p0, Lcom/konka/appassistant/AppAssistantActivity$3;->this$0:Lcom/konka/appassistant/AppAssistantActivity;

    # getter for: Lcom/konka/appassistant/AppAssistantActivity;->mZipInstall_button:Landroid/widget/TextView;
    invoke-static {v0}, Lcom/konka/appassistant/AppAssistantActivity;->access$14(Lcom/konka/appassistant/AppAssistantActivity;)Landroid/widget/TextView;

    move-result-object v0

    if-ne p1, v0, :cond_0

    iget-object v0, p0, Lcom/konka/appassistant/AppAssistantActivity$3;->this$0:Lcom/konka/appassistant/AppAssistantActivity;

    # getter for: Lcom/konka/appassistant/AppAssistantActivity;->mZipInstall_button:Landroid/widget/TextView;
    invoke-static {v0}, Lcom/konka/appassistant/AppAssistantActivity;->access$14(Lcom/konka/appassistant/AppAssistantActivity;)Landroid/widget/TextView;

    move-result-object v0

    invoke-virtual {v0, v2}, Landroid/widget/TextView;->setTextColor(I)V

    iget-object v0, p0, Lcom/konka/appassistant/AppAssistantActivity$3;->this$0:Lcom/konka/appassistant/AppAssistantActivity;

    # getter for: Lcom/konka/appassistant/AppAssistantActivity;->mUninstallList:Ljava/util/List;
    invoke-static {v0}, Lcom/konka/appassistant/AppAssistantActivity;->access$16(Lcom/konka/appassistant/AppAssistantActivity;)Ljava/util/List;

    move-result-object v0

    invoke-interface {v0}, Ljava/util/List;->clear()V

    iget-object v0, p0, Lcom/konka/appassistant/AppAssistantActivity$3;->this$0:Lcom/konka/appassistant/AppAssistantActivity;

    # getter for: Lcom/konka/appassistant/AppAssistantActivity;->mInstallList:Ljava/util/List;
    invoke-static {v0}, Lcom/konka/appassistant/AppAssistantActivity;->access$17(Lcom/konka/appassistant/AppAssistantActivity;)Ljava/util/List;

    move-result-object v0

    if-eqz v0, :cond_6

    iget-object v0, p0, Lcom/konka/appassistant/AppAssistantActivity$3;->this$0:Lcom/konka/appassistant/AppAssistantActivity;

    # getter for: Lcom/konka/appassistant/AppAssistantActivity;->mInstallList:Ljava/util/List;
    invoke-static {v0}, Lcom/konka/appassistant/AppAssistantActivity;->access$17(Lcom/konka/appassistant/AppAssistantActivity;)Ljava/util/List;

    move-result-object v0

    invoke-interface {v0}, Ljava/util/List;->clear()V

    :goto_1
    iget-object v0, p0, Lcom/konka/appassistant/AppAssistantActivity$3;->this$0:Lcom/konka/appassistant/AppAssistantActivity;

    # invokes: Lcom/konka/appassistant/AppAssistantActivity;->IsTabButtonFocused()Ljava/lang/Boolean;
    invoke-static {v0}, Lcom/konka/appassistant/AppAssistantActivity;->access$15(Lcom/konka/appassistant/AppAssistantActivity;)Ljava/lang/Boolean;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v0

    if-nez v0, :cond_0

    iget-object v0, p0, Lcom/konka/appassistant/AppAssistantActivity$3;->this$0:Lcom/konka/appassistant/AppAssistantActivity;

    # getter for: Lcom/konka/appassistant/AppAssistantActivity;->mZipInstall_button:Landroid/widget/TextView;
    invoke-static {v0}, Lcom/konka/appassistant/AppAssistantActivity;->access$14(Lcom/konka/appassistant/AppAssistantActivity;)Landroid/widget/TextView;

    move-result-object v0

    invoke-virtual {v0, v2}, Landroid/widget/TextView;->setTextColor(I)V

    iget-object v0, p0, Lcom/konka/appassistant/AppAssistantActivity$3;->this$0:Lcom/konka/appassistant/AppAssistantActivity;

    # getter for: Lcom/konka/appassistant/AppAssistantActivity;->mZipInstall_button:Landroid/widget/TextView;
    invoke-static {v0}, Lcom/konka/appassistant/AppAssistantActivity;->access$14(Lcom/konka/appassistant/AppAssistantActivity;)Landroid/widget/TextView;

    move-result-object v0

    invoke-virtual {v0, v4}, Landroid/widget/TextView;->setBackgroundResource(I)V

    goto/16 :goto_0

    :cond_6
    iget-object v0, p0, Lcom/konka/appassistant/AppAssistantActivity$3;->this$0:Lcom/konka/appassistant/AppAssistantActivity;

    new-instance v1, Ljava/util/ArrayList;

    invoke-direct {v1}, Ljava/util/ArrayList;-><init>()V

    invoke-static {v0, v1}, Lcom/konka/appassistant/AppAssistantActivity;->access$18(Lcom/konka/appassistant/AppAssistantActivity;Ljava/util/List;)V

    goto :goto_1
.end method
