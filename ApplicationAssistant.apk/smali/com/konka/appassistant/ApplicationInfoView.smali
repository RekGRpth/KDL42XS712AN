.class public Lcom/konka/appassistant/ApplicationInfoView;
.super Ljava/lang/Object;
.source "ApplicationInfoView.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/konka/appassistant/ApplicationInfoView$ViewType;
    }
.end annotation


# static fields
.field private static synthetic $SWITCH_TABLE$com$konka$appassistant$ApplicationInfoView$ViewType:[I = null

.field static final TAG:Ljava/lang/String; = "AppAssistant"

.field private static mViewHeight:I


# instance fields
.field private IsInstalled:Ljava/lang/Boolean;

.field private as:Lcom/konka/appassistant/PackageUtil$AppSnippet;

.field private mAppIcon:Landroid/widget/ImageView;

.field private mAppInfo:Landroid/content/pm/ApplicationInfo;

.field private mAppLable:Landroid/widget/TextView;

.field private mAppSize:Landroid/widget/TextView;

.field private mAppView:Landroid/view/View;

.field private mChildNum:I

.field private mContext:Landroid/content/Context;

.field private mDeleteButton:Landroid/widget/Button;

.field private mDeletelistener:Landroid/view/View$OnClickListener;

.field private mInstallStatus:Landroid/widget/TextView;

.field private mOkButton:Landroid/widget/Button;

.field private mOklistener:Landroid/view/View$OnClickListener;

.field private mOnFocusChangeListener:Landroid/view/View$OnFocusChangeListener;

.field private mOnkKeyListener:Landroid/view/View$OnKeyListener;

.field private mPackageUri:Landroid/net/Uri;

.field private mVersionName:Ljava/lang/String;

.field private mVersionNum:Landroid/widget/TextView;

.field private mViewType:Lcom/konka/appassistant/ApplicationInfoView$ViewType;

.field private mZipInstallResult:Landroid/widget/TextView;

.field private pm:Landroid/content/pm/PackageManager;

.field private totalsize:J


# direct methods
.method static synthetic $SWITCH_TABLE$com$konka$appassistant$ApplicationInfoView$ViewType()[I
    .locals 3

    sget-object v0, Lcom/konka/appassistant/ApplicationInfoView;->$SWITCH_TABLE$com$konka$appassistant$ApplicationInfoView$ViewType:[I

    if-eqz v0, :cond_0

    :goto_0
    return-object v0

    :cond_0
    invoke-static {}, Lcom/konka/appassistant/ApplicationInfoView$ViewType;->values()[Lcom/konka/appassistant/ApplicationInfoView$ViewType;

    move-result-object v0

    array-length v0, v0

    new-array v0, v0, [I

    :try_start_0
    sget-object v1, Lcom/konka/appassistant/ApplicationInfoView$ViewType;->INSTALL_VIEW:Lcom/konka/appassistant/ApplicationInfoView$ViewType;

    invoke-virtual {v1}, Lcom/konka/appassistant/ApplicationInfoView$ViewType;->ordinal()I

    move-result v1

    const/4 v2, 0x1

    aput v2, v0, v1
    :try_end_0
    .catch Ljava/lang/NoSuchFieldError; {:try_start_0 .. :try_end_0} :catch_2

    :goto_1
    :try_start_1
    sget-object v1, Lcom/konka/appassistant/ApplicationInfoView$ViewType;->UNINSTALL_VIEW:Lcom/konka/appassistant/ApplicationInfoView$ViewType;

    invoke-virtual {v1}, Lcom/konka/appassistant/ApplicationInfoView$ViewType;->ordinal()I

    move-result v1

    const/4 v2, 0x2

    aput v2, v0, v1
    :try_end_1
    .catch Ljava/lang/NoSuchFieldError; {:try_start_1 .. :try_end_1} :catch_1

    :goto_2
    :try_start_2
    sget-object v1, Lcom/konka/appassistant/ApplicationInfoView$ViewType;->ZIP_INSTALL_VIEW:Lcom/konka/appassistant/ApplicationInfoView$ViewType;

    invoke-virtual {v1}, Lcom/konka/appassistant/ApplicationInfoView$ViewType;->ordinal()I

    move-result v1

    const/4 v2, 0x3

    aput v2, v0, v1
    :try_end_2
    .catch Ljava/lang/NoSuchFieldError; {:try_start_2 .. :try_end_2} :catch_0

    :goto_3
    sput-object v0, Lcom/konka/appassistant/ApplicationInfoView;->$SWITCH_TABLE$com$konka$appassistant$ApplicationInfoView$ViewType:[I

    goto :goto_0

    :catch_0
    move-exception v1

    goto :goto_3

    :catch_1
    move-exception v1

    goto :goto_2

    :catch_2
    move-exception v1

    goto :goto_1
.end method

.method static constructor <clinit>()V
    .locals 1

    const/4 v0, 0x0

    sput v0, Lcom/konka/appassistant/ApplicationInfoView;->mViewHeight:I

    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/content/pm/ApplicationInfo;Lcom/konka/appassistant/ApplicationInfoView$ViewType;JI)V
    .locals 3
    .param p1    # Landroid/content/Context;
    .param p2    # Landroid/content/pm/ApplicationInfo;
    .param p3    # Lcom/konka/appassistant/ApplicationInfoView$ViewType;
    .param p4    # J
    .param p6    # I

    const/4 v2, 0x0

    const/4 v1, 0x0

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    iput-object v1, p0, Lcom/konka/appassistant/ApplicationInfoView;->mContext:Landroid/content/Context;

    sget-object v0, Lcom/konka/appassistant/ApplicationInfoView$ViewType;->INSTALL_VIEW:Lcom/konka/appassistant/ApplicationInfoView$ViewType;

    iput-object v0, p0, Lcom/konka/appassistant/ApplicationInfoView;->mViewType:Lcom/konka/appassistant/ApplicationInfoView$ViewType;

    iput-object v1, p0, Lcom/konka/appassistant/ApplicationInfoView;->mAppInfo:Landroid/content/pm/ApplicationInfo;

    iput-object v1, p0, Lcom/konka/appassistant/ApplicationInfoView;->mPackageUri:Landroid/net/Uri;

    iput-object v1, p0, Lcom/konka/appassistant/ApplicationInfoView;->mAppView:Landroid/view/View;

    iput-object v1, p0, Lcom/konka/appassistant/ApplicationInfoView;->mAppIcon:Landroid/widget/ImageView;

    iput-object v1, p0, Lcom/konka/appassistant/ApplicationInfoView;->mAppLable:Landroid/widget/TextView;

    iput-object v1, p0, Lcom/konka/appassistant/ApplicationInfoView;->mAppSize:Landroid/widget/TextView;

    iput-object v1, p0, Lcom/konka/appassistant/ApplicationInfoView;->mOkButton:Landroid/widget/Button;

    iput-object v1, p0, Lcom/konka/appassistant/ApplicationInfoView;->mDeleteButton:Landroid/widget/Button;

    iput-object v1, p0, Lcom/konka/appassistant/ApplicationInfoView;->mInstallStatus:Landroid/widget/TextView;

    iput-object v1, p0, Lcom/konka/appassistant/ApplicationInfoView;->mZipInstallResult:Landroid/widget/TextView;

    iput-object v1, p0, Lcom/konka/appassistant/ApplicationInfoView;->mVersionNum:Landroid/widget/TextView;

    iput-object v1, p0, Lcom/konka/appassistant/ApplicationInfoView;->as:Lcom/konka/appassistant/PackageUtil$AppSnippet;

    const-string v0, ""

    iput-object v0, p0, Lcom/konka/appassistant/ApplicationInfoView;->mVersionName:Ljava/lang/String;

    iput v2, p0, Lcom/konka/appassistant/ApplicationInfoView;->mChildNum:I

    invoke-static {v2}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v0

    iput-object v0, p0, Lcom/konka/appassistant/ApplicationInfoView;->IsInstalled:Ljava/lang/Boolean;

    new-instance v0, Lcom/konka/appassistant/ApplicationInfoView$1;

    invoke-direct {v0, p0}, Lcom/konka/appassistant/ApplicationInfoView$1;-><init>(Lcom/konka/appassistant/ApplicationInfoView;)V

    iput-object v0, p0, Lcom/konka/appassistant/ApplicationInfoView;->mOnkKeyListener:Landroid/view/View$OnKeyListener;

    new-instance v0, Lcom/konka/appassistant/ApplicationInfoView$2;

    invoke-direct {v0, p0}, Lcom/konka/appassistant/ApplicationInfoView$2;-><init>(Lcom/konka/appassistant/ApplicationInfoView;)V

    iput-object v0, p0, Lcom/konka/appassistant/ApplicationInfoView;->mOklistener:Landroid/view/View$OnClickListener;

    new-instance v0, Lcom/konka/appassistant/ApplicationInfoView$3;

    invoke-direct {v0, p0}, Lcom/konka/appassistant/ApplicationInfoView$3;-><init>(Lcom/konka/appassistant/ApplicationInfoView;)V

    iput-object v0, p0, Lcom/konka/appassistant/ApplicationInfoView;->mDeletelistener:Landroid/view/View$OnClickListener;

    new-instance v0, Lcom/konka/appassistant/ApplicationInfoView$4;

    invoke-direct {v0, p0}, Lcom/konka/appassistant/ApplicationInfoView$4;-><init>(Lcom/konka/appassistant/ApplicationInfoView;)V

    iput-object v0, p0, Lcom/konka/appassistant/ApplicationInfoView;->mOnFocusChangeListener:Landroid/view/View$OnFocusChangeListener;

    iput-object p1, p0, Lcom/konka/appassistant/ApplicationInfoView;->mContext:Landroid/content/Context;

    iput-object p2, p0, Lcom/konka/appassistant/ApplicationInfoView;->mAppInfo:Landroid/content/pm/ApplicationInfo;

    iget-object v0, p0, Lcom/konka/appassistant/ApplicationInfoView;->mAppInfo:Landroid/content/pm/ApplicationInfo;

    invoke-static {v0}, Lcom/konka/appassistant/PackageUtil;->getUri(Landroid/content/pm/ApplicationInfo;)Landroid/net/Uri;

    move-result-object v0

    iput-object v0, p0, Lcom/konka/appassistant/ApplicationInfoView;->mPackageUri:Landroid/net/Uri;

    iput-object p3, p0, Lcom/konka/appassistant/ApplicationInfoView;->mViewType:Lcom/konka/appassistant/ApplicationInfoView$ViewType;

    iput-wide p4, p0, Lcom/konka/appassistant/ApplicationInfoView;->totalsize:J

    iput p6, p0, Lcom/konka/appassistant/ApplicationInfoView;->mChildNum:I

    invoke-direct {p0}, Lcom/konka/appassistant/ApplicationInfoView;->init()V

    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/net/Uri;Lcom/konka/appassistant/ApplicationInfoView$ViewType;I)V
    .locals 3
    .param p1    # Landroid/content/Context;
    .param p2    # Landroid/net/Uri;
    .param p3    # Lcom/konka/appassistant/ApplicationInfoView$ViewType;
    .param p4    # I

    const/4 v2, 0x0

    const/4 v1, 0x0

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    iput-object v1, p0, Lcom/konka/appassistant/ApplicationInfoView;->mContext:Landroid/content/Context;

    sget-object v0, Lcom/konka/appassistant/ApplicationInfoView$ViewType;->INSTALL_VIEW:Lcom/konka/appassistant/ApplicationInfoView$ViewType;

    iput-object v0, p0, Lcom/konka/appassistant/ApplicationInfoView;->mViewType:Lcom/konka/appassistant/ApplicationInfoView$ViewType;

    iput-object v1, p0, Lcom/konka/appassistant/ApplicationInfoView;->mAppInfo:Landroid/content/pm/ApplicationInfo;

    iput-object v1, p0, Lcom/konka/appassistant/ApplicationInfoView;->mPackageUri:Landroid/net/Uri;

    iput-object v1, p0, Lcom/konka/appassistant/ApplicationInfoView;->mAppView:Landroid/view/View;

    iput-object v1, p0, Lcom/konka/appassistant/ApplicationInfoView;->mAppIcon:Landroid/widget/ImageView;

    iput-object v1, p0, Lcom/konka/appassistant/ApplicationInfoView;->mAppLable:Landroid/widget/TextView;

    iput-object v1, p0, Lcom/konka/appassistant/ApplicationInfoView;->mAppSize:Landroid/widget/TextView;

    iput-object v1, p0, Lcom/konka/appassistant/ApplicationInfoView;->mOkButton:Landroid/widget/Button;

    iput-object v1, p0, Lcom/konka/appassistant/ApplicationInfoView;->mDeleteButton:Landroid/widget/Button;

    iput-object v1, p0, Lcom/konka/appassistant/ApplicationInfoView;->mInstallStatus:Landroid/widget/TextView;

    iput-object v1, p0, Lcom/konka/appassistant/ApplicationInfoView;->mZipInstallResult:Landroid/widget/TextView;

    iput-object v1, p0, Lcom/konka/appassistant/ApplicationInfoView;->mVersionNum:Landroid/widget/TextView;

    iput-object v1, p0, Lcom/konka/appassistant/ApplicationInfoView;->as:Lcom/konka/appassistant/PackageUtil$AppSnippet;

    const-string v0, ""

    iput-object v0, p0, Lcom/konka/appassistant/ApplicationInfoView;->mVersionName:Ljava/lang/String;

    iput v2, p0, Lcom/konka/appassistant/ApplicationInfoView;->mChildNum:I

    invoke-static {v2}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v0

    iput-object v0, p0, Lcom/konka/appassistant/ApplicationInfoView;->IsInstalled:Ljava/lang/Boolean;

    new-instance v0, Lcom/konka/appassistant/ApplicationInfoView$1;

    invoke-direct {v0, p0}, Lcom/konka/appassistant/ApplicationInfoView$1;-><init>(Lcom/konka/appassistant/ApplicationInfoView;)V

    iput-object v0, p0, Lcom/konka/appassistant/ApplicationInfoView;->mOnkKeyListener:Landroid/view/View$OnKeyListener;

    new-instance v0, Lcom/konka/appassistant/ApplicationInfoView$2;

    invoke-direct {v0, p0}, Lcom/konka/appassistant/ApplicationInfoView$2;-><init>(Lcom/konka/appassistant/ApplicationInfoView;)V

    iput-object v0, p0, Lcom/konka/appassistant/ApplicationInfoView;->mOklistener:Landroid/view/View$OnClickListener;

    new-instance v0, Lcom/konka/appassistant/ApplicationInfoView$3;

    invoke-direct {v0, p0}, Lcom/konka/appassistant/ApplicationInfoView$3;-><init>(Lcom/konka/appassistant/ApplicationInfoView;)V

    iput-object v0, p0, Lcom/konka/appassistant/ApplicationInfoView;->mDeletelistener:Landroid/view/View$OnClickListener;

    new-instance v0, Lcom/konka/appassistant/ApplicationInfoView$4;

    invoke-direct {v0, p0}, Lcom/konka/appassistant/ApplicationInfoView$4;-><init>(Lcom/konka/appassistant/ApplicationInfoView;)V

    iput-object v0, p0, Lcom/konka/appassistant/ApplicationInfoView;->mOnFocusChangeListener:Landroid/view/View$OnFocusChangeListener;

    iput-object p1, p0, Lcom/konka/appassistant/ApplicationInfoView;->mContext:Landroid/content/Context;

    iput-object p2, p0, Lcom/konka/appassistant/ApplicationInfoView;->mPackageUri:Landroid/net/Uri;

    iget-object v0, p0, Lcom/konka/appassistant/ApplicationInfoView;->mPackageUri:Landroid/net/Uri;

    invoke-static {v0}, Lcom/konka/appassistant/PackageUtil;->getApplicationInfo(Landroid/net/Uri;)Landroid/content/pm/ApplicationInfo;

    move-result-object v0

    iput-object v0, p0, Lcom/konka/appassistant/ApplicationInfoView;->mAppInfo:Landroid/content/pm/ApplicationInfo;

    iput-object p3, p0, Lcom/konka/appassistant/ApplicationInfoView;->mViewType:Lcom/konka/appassistant/ApplicationInfoView$ViewType;

    const-wide/16 v0, 0x0

    iput-wide v0, p0, Lcom/konka/appassistant/ApplicationInfoView;->totalsize:J

    iput p4, p0, Lcom/konka/appassistant/ApplicationInfoView;->mChildNum:I

    iget-object v0, p0, Lcom/konka/appassistant/ApplicationInfoView;->mAppInfo:Landroid/content/pm/ApplicationInfo;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/konka/appassistant/ApplicationInfoView;->mAppInfo:Landroid/content/pm/ApplicationInfo;

    iget-object v0, v0, Landroid/content/pm/ApplicationInfo;->packageName:Ljava/lang/String;

    invoke-static {v0}, Lcom/konka/appassistant/AppAssistantActivity;->IsAppInstalled(Ljava/lang/String;)Ljava/lang/Boolean;

    move-result-object v0

    iput-object v0, p0, Lcom/konka/appassistant/ApplicationInfoView;->IsInstalled:Ljava/lang/Boolean;

    :cond_0
    invoke-direct {p0}, Lcom/konka/appassistant/ApplicationInfoView;->init()V

    return-void
.end method

.method private OpenDialog()V
    .locals 5

    new-instance v0, Landroid/app/AlertDialog$Builder;

    iget-object v2, p0, Lcom/konka/appassistant/ApplicationInfoView;->mContext:Landroid/content/Context;

    invoke-direct {v0, v2}, Landroid/app/AlertDialog$Builder;-><init>(Landroid/content/Context;)V

    iget-object v2, p0, Lcom/konka/appassistant/ApplicationInfoView;->mContext:Landroid/content/Context;

    const v3, 0x7f060022    # com.konka.appassistant.R.string.delete_sureornot

    invoke-virtual {v2, v3}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0, v2}, Landroid/app/AlertDialog$Builder;->setTitle(Ljava/lang/CharSequence;)Landroid/app/AlertDialog$Builder;

    iget-object v2, p0, Lcom/konka/appassistant/ApplicationInfoView;->mContext:Landroid/content/Context;

    const v3, 0x7f060023    # com.konka.appassistant.R.string.m_yes

    invoke-virtual {v2, v3}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v2

    new-instance v3, Lcom/konka/appassistant/ApplicationInfoView$5;

    invoke-direct {v3, p0}, Lcom/konka/appassistant/ApplicationInfoView$5;-><init>(Lcom/konka/appassistant/ApplicationInfoView;)V

    invoke-virtual {v0, v2, v3}, Landroid/app/AlertDialog$Builder;->setPositiveButton(Ljava/lang/CharSequence;Landroid/content/DialogInterface$OnClickListener;)Landroid/app/AlertDialog$Builder;

    iget-object v2, p0, Lcom/konka/appassistant/ApplicationInfoView;->mContext:Landroid/content/Context;

    const v3, 0x7f060024    # com.konka.appassistant.R.string.m_return

    invoke-virtual {v2, v3}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v2

    new-instance v3, Lcom/konka/appassistant/ApplicationInfoView$6;

    invoke-direct {v3, p0}, Lcom/konka/appassistant/ApplicationInfoView$6;-><init>(Lcom/konka/appassistant/ApplicationInfoView;)V

    invoke-virtual {v0, v2, v3}, Landroid/app/AlertDialog$Builder;->setNegativeButton(Ljava/lang/CharSequence;Landroid/content/DialogInterface$OnClickListener;)Landroid/app/AlertDialog$Builder;

    invoke-virtual {v0}, Landroid/app/AlertDialog$Builder;->create()Landroid/app/AlertDialog;

    move-result-object v1

    const-string v2, "1111"

    new-instance v3, Ljava/lang/StringBuilder;

    const-string v4, "mChildNum = "

    invoke-direct {v3, v4}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    iget v4, p0, Lcom/konka/appassistant/ApplicationInfoView;->mChildNum:I

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v2, v3}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    invoke-virtual {v1}, Landroid/app/AlertDialog;->show()V

    return-void
.end method

.method static synthetic access$0(Lcom/konka/appassistant/ApplicationInfoView;)Landroid/widget/Button;
    .locals 1

    iget-object v0, p0, Lcom/konka/appassistant/ApplicationInfoView;->mOkButton:Landroid/widget/Button;

    return-object v0
.end method

.method static synthetic access$1(Lcom/konka/appassistant/ApplicationInfoView;)Landroid/widget/TextView;
    .locals 1

    iget-object v0, p0, Lcom/konka/appassistant/ApplicationInfoView;->mZipInstallResult:Landroid/widget/TextView;

    return-object v0
.end method

.method static synthetic access$10(Lcom/konka/appassistant/ApplicationInfoView;)Landroid/widget/TextView;
    .locals 1

    iget-object v0, p0, Lcom/konka/appassistant/ApplicationInfoView;->mVersionNum:Landroid/widget/TextView;

    return-object v0
.end method

.method static synthetic access$11(Lcom/konka/appassistant/ApplicationInfoView;)Landroid/widget/TextView;
    .locals 1

    iget-object v0, p0, Lcom/konka/appassistant/ApplicationInfoView;->mInstallStatus:Landroid/widget/TextView;

    return-object v0
.end method

.method static synthetic access$12(Lcom/konka/appassistant/ApplicationInfoView;)Landroid/view/View;
    .locals 1

    iget-object v0, p0, Lcom/konka/appassistant/ApplicationInfoView;->mAppView:Landroid/view/View;

    return-object v0
.end method

.method static synthetic access$13(Lcom/konka/appassistant/ApplicationInfoView;)Landroid/widget/Button;
    .locals 1

    iget-object v0, p0, Lcom/konka/appassistant/ApplicationInfoView;->mDeleteButton:Landroid/widget/Button;

    return-object v0
.end method

.method static synthetic access$2(Lcom/konka/appassistant/ApplicationInfoView;)Landroid/content/Context;
    .locals 1

    iget-object v0, p0, Lcom/konka/appassistant/ApplicationInfoView;->mContext:Landroid/content/Context;

    return-object v0
.end method

.method static synthetic access$3(Lcom/konka/appassistant/ApplicationInfoView;)Lcom/konka/appassistant/ApplicationInfoView$ViewType;
    .locals 1

    iget-object v0, p0, Lcom/konka/appassistant/ApplicationInfoView;->mViewType:Lcom/konka/appassistant/ApplicationInfoView$ViewType;

    return-object v0
.end method

.method static synthetic access$4(Lcom/konka/appassistant/ApplicationInfoView;)I
    .locals 1

    iget v0, p0, Lcom/konka/appassistant/ApplicationInfoView;->mChildNum:I

    return v0
.end method

.method static synthetic access$5(Lcom/konka/appassistant/ApplicationInfoView;)Landroid/net/Uri;
    .locals 1

    iget-object v0, p0, Lcom/konka/appassistant/ApplicationInfoView;->mPackageUri:Landroid/net/Uri;

    return-object v0
.end method

.method static synthetic access$6(Lcom/konka/appassistant/ApplicationInfoView;)Landroid/content/pm/ApplicationInfo;
    .locals 1

    iget-object v0, p0, Lcom/konka/appassistant/ApplicationInfoView;->mAppInfo:Landroid/content/pm/ApplicationInfo;

    return-object v0
.end method

.method static synthetic access$7(Lcom/konka/appassistant/ApplicationInfoView;)V
    .locals 0

    invoke-direct {p0}, Lcom/konka/appassistant/ApplicationInfoView;->OpenDialog()V

    return-void
.end method

.method static synthetic access$8(Lcom/konka/appassistant/ApplicationInfoView;)Landroid/widget/TextView;
    .locals 1

    iget-object v0, p0, Lcom/konka/appassistant/ApplicationInfoView;->mAppLable:Landroid/widget/TextView;

    return-object v0
.end method

.method static synthetic access$9(Lcom/konka/appassistant/ApplicationInfoView;)Landroid/widget/TextView;
    .locals 1

    iget-object v0, p0, Lcom/konka/appassistant/ApplicationInfoView;->mAppSize:Landroid/widget/TextView;

    return-object v0
.end method

.method public static getViewHeight()I
    .locals 1

    sget v0, Lcom/konka/appassistant/ApplicationInfoView;->mViewHeight:I

    return v0
.end method

.method private init()V
    .locals 8

    const v7, 0x7f05000a    # com.konka.appassistant.R.dimen.appinfo_view_height

    iget-object v4, p0, Lcom/konka/appassistant/ApplicationInfoView;->mContext:Landroid/content/Context;

    invoke-virtual {v4}, Landroid/content/Context;->getPackageManager()Landroid/content/pm/PackageManager;

    move-result-object v4

    iput-object v4, p0, Lcom/konka/appassistant/ApplicationInfoView;->pm:Landroid/content/pm/PackageManager;

    iget-object v4, p0, Lcom/konka/appassistant/ApplicationInfoView;->mContext:Landroid/content/Context;

    const-string v5, "layout_inflater"

    invoke-virtual {v4, v5}, Landroid/content/Context;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v4

    check-cast v4, Landroid/view/LayoutInflater;

    const v5, 0x7f030001    # com.konka.appassistant.R.layout.applicationinfo_view

    const/4 v6, 0x0

    invoke-virtual {v4, v5, v6}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;)Landroid/view/View;

    move-result-object v4

    iput-object v4, p0, Lcom/konka/appassistant/ApplicationInfoView;->mAppView:Landroid/view/View;

    new-instance v2, Landroid/view/ViewGroup$LayoutParams;

    const/4 v4, -0x1

    iget-object v5, p0, Lcom/konka/appassistant/ApplicationInfoView;->mContext:Landroid/content/Context;

    invoke-virtual {v5}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v5

    invoke-virtual {v5, v7}, Landroid/content/res/Resources;->getDimension(I)F

    move-result v5

    float-to-int v5, v5

    invoke-direct {v2, v4, v5}, Landroid/view/ViewGroup$LayoutParams;-><init>(II)V

    iget-object v4, p0, Lcom/konka/appassistant/ApplicationInfoView;->mAppView:Landroid/view/View;

    invoke-virtual {v4, v2}, Landroid/view/View;->setLayoutParams(Landroid/view/ViewGroup$LayoutParams;)V

    iget-object v4, p0, Lcom/konka/appassistant/ApplicationInfoView;->mContext:Landroid/content/Context;

    invoke-virtual {v4}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v4

    invoke-virtual {v4, v7}, Landroid/content/res/Resources;->getDimension(I)F

    move-result v4

    float-to-int v4, v4

    sput v4, Lcom/konka/appassistant/ApplicationInfoView;->mViewHeight:I

    iget-object v4, p0, Lcom/konka/appassistant/ApplicationInfoView;->mAppView:Landroid/view/View;

    if-eqz v4, :cond_1

    iget-object v4, p0, Lcom/konka/appassistant/ApplicationInfoView;->mViewType:Lcom/konka/appassistant/ApplicationInfoView$ViewType;

    sget-object v5, Lcom/konka/appassistant/ApplicationInfoView$ViewType;->UNINSTALL_VIEW:Lcom/konka/appassistant/ApplicationInfoView$ViewType;

    if-ne v4, v5, :cond_2

    :try_start_0
    iget-object v4, p0, Lcom/konka/appassistant/ApplicationInfoView;->mContext:Landroid/content/Context;

    invoke-virtual {v4}, Landroid/content/Context;->getPackageManager()Landroid/content/pm/PackageManager;

    move-result-object v4

    iget-object v5, p0, Lcom/konka/appassistant/ApplicationInfoView;->mAppInfo:Landroid/content/pm/ApplicationInfo;

    iget-object v5, v5, Landroid/content/pm/ApplicationInfo;->packageName:Ljava/lang/String;

    const/4 v6, 0x0

    invoke-virtual {v4, v5, v6}, Landroid/content/pm/PackageManager;->getPackageInfo(Ljava/lang/String;I)Landroid/content/pm/PackageInfo;

    move-result-object v1

    iget-object v4, v1, Landroid/content/pm/PackageInfo;->versionName:Ljava/lang/String;

    iput-object v4, p0, Lcom/konka/appassistant/ApplicationInfoView;->mVersionName:Ljava/lang/String;
    :try_end_0
    .catch Landroid/content/pm/PackageManager$NameNotFoundException; {:try_start_0 .. :try_end_0} :catch_0

    :cond_0
    :goto_0
    iget-object v4, p0, Lcom/konka/appassistant/ApplicationInfoView;->mAppView:Landroid/view/View;

    const v5, 0x7f070014    # com.konka.appassistant.R.id.appVersion

    invoke-virtual {v4, v5}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v4

    check-cast v4, Landroid/widget/TextView;

    iput-object v4, p0, Lcom/konka/appassistant/ApplicationInfoView;->mVersionNum:Landroid/widget/TextView;

    iget-object v4, p0, Lcom/konka/appassistant/ApplicationInfoView;->mVersionNum:Landroid/widget/TextView;

    new-instance v5, Ljava/lang/StringBuilder;

    iget-object v6, p0, Lcom/konka/appassistant/ApplicationInfoView;->mContext:Landroid/content/Context;

    const v7, 0x7f060018    # com.konka.appassistant.R.string.app_info_version_num

    invoke-virtual {v6, v7}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v6

    invoke-static {v6}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v6

    invoke-direct {v5, v6}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    iget-object v6, p0, Lcom/konka/appassistant/ApplicationInfoView;->mVersionName:Ljava/lang/String;

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v4, v5}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    iget-object v4, p0, Lcom/konka/appassistant/ApplicationInfoView;->mContext:Landroid/content/Context;

    iget-object v5, p0, Lcom/konka/appassistant/ApplicationInfoView;->mAppInfo:Landroid/content/pm/ApplicationInfo;

    iget-object v6, p0, Lcom/konka/appassistant/ApplicationInfoView;->mPackageUri:Landroid/net/Uri;

    invoke-static {v4, v5, v6}, Lcom/konka/appassistant/PackageUtil;->getAppSnippet(Landroid/content/Context;Landroid/content/pm/ApplicationInfo;Landroid/net/Uri;)Lcom/konka/appassistant/PackageUtil$AppSnippet;

    move-result-object v4

    iput-object v4, p0, Lcom/konka/appassistant/ApplicationInfoView;->as:Lcom/konka/appassistant/PackageUtil$AppSnippet;

    iget-object v4, p0, Lcom/konka/appassistant/ApplicationInfoView;->as:Lcom/konka/appassistant/PackageUtil$AppSnippet;

    if-nez v4, :cond_3

    :cond_1
    :goto_1
    return-void

    :catch_0
    move-exception v0

    invoke-virtual {v0}, Landroid/content/pm/PackageManager$NameNotFoundException;->printStackTrace()V

    goto :goto_0

    :cond_2
    iget-object v4, p0, Lcom/konka/appassistant/ApplicationInfoView;->mPackageUri:Landroid/net/Uri;

    invoke-static {v4}, Lcom/konka/appassistant/PackageUtil;->getPackageInfo(Landroid/net/Uri;)Landroid/content/pm/PackageParser$Package;

    move-result-object v3

    if-eqz v3, :cond_0

    iget-object v4, v3, Landroid/content/pm/PackageParser$Package;->mVersionName:Ljava/lang/String;

    iput-object v4, p0, Lcom/konka/appassistant/ApplicationInfoView;->mVersionName:Ljava/lang/String;

    goto :goto_0

    :cond_3
    iget-object v4, p0, Lcom/konka/appassistant/ApplicationInfoView;->mAppView:Landroid/view/View;

    const v5, 0x7f070012    # com.konka.appassistant.R.id.appIcon

    invoke-virtual {v4, v5}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v4

    check-cast v4, Landroid/widget/ImageView;

    iput-object v4, p0, Lcom/konka/appassistant/ApplicationInfoView;->mAppIcon:Landroid/widget/ImageView;

    iget-object v4, p0, Lcom/konka/appassistant/ApplicationInfoView;->as:Lcom/konka/appassistant/PackageUtil$AppSnippet;

    iget-object v4, v4, Lcom/konka/appassistant/PackageUtil$AppSnippet;->icon:Landroid/graphics/drawable/Drawable;

    if-eqz v4, :cond_4

    iget-object v4, p0, Lcom/konka/appassistant/ApplicationInfoView;->mAppIcon:Landroid/widget/ImageView;

    iget-object v5, p0, Lcom/konka/appassistant/ApplicationInfoView;->as:Lcom/konka/appassistant/PackageUtil$AppSnippet;

    iget-object v5, v5, Lcom/konka/appassistant/PackageUtil$AppSnippet;->icon:Landroid/graphics/drawable/Drawable;

    invoke-virtual {v4, v5}, Landroid/widget/ImageView;->setBackgroundDrawable(Landroid/graphics/drawable/Drawable;)V

    :goto_2
    iget-object v4, p0, Lcom/konka/appassistant/ApplicationInfoView;->mAppView:Landroid/view/View;

    const v5, 0x7f070013    # com.konka.appassistant.R.id.appLable

    invoke-virtual {v4, v5}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v4

    check-cast v4, Landroid/widget/TextView;

    iput-object v4, p0, Lcom/konka/appassistant/ApplicationInfoView;->mAppLable:Landroid/widget/TextView;

    iget-object v4, p0, Lcom/konka/appassistant/ApplicationInfoView;->as:Lcom/konka/appassistant/PackageUtil$AppSnippet;

    iget-object v4, v4, Lcom/konka/appassistant/PackageUtil$AppSnippet;->label:Ljava/lang/CharSequence;

    if-eqz v4, :cond_5

    iget-object v4, p0, Lcom/konka/appassistant/ApplicationInfoView;->mAppLable:Landroid/widget/TextView;

    iget-object v5, p0, Lcom/konka/appassistant/ApplicationInfoView;->as:Lcom/konka/appassistant/PackageUtil$AppSnippet;

    iget-object v5, v5, Lcom/konka/appassistant/PackageUtil$AppSnippet;->label:Ljava/lang/CharSequence;

    invoke-virtual {v4, v5}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    :goto_3
    iget-object v4, p0, Lcom/konka/appassistant/ApplicationInfoView;->mAppView:Landroid/view/View;

    const v5, 0x7f070016    # com.konka.appassistant.R.id.appSize

    invoke-virtual {v4, v5}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v4

    check-cast v4, Landroid/widget/TextView;

    iput-object v4, p0, Lcom/konka/appassistant/ApplicationInfoView;->mAppSize:Landroid/widget/TextView;

    invoke-direct {p0}, Lcom/konka/appassistant/ApplicationInfoView;->setAppSize()V

    iget-object v4, p0, Lcom/konka/appassistant/ApplicationInfoView;->mAppView:Landroid/view/View;

    const v5, 0x7f070019    # com.konka.appassistant.R.id.ok_button

    invoke-virtual {v4, v5}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v4

    check-cast v4, Landroid/widget/Button;

    iput-object v4, p0, Lcom/konka/appassistant/ApplicationInfoView;->mOkButton:Landroid/widget/Button;

    iget-object v4, p0, Lcom/konka/appassistant/ApplicationInfoView;->mOkButton:Landroid/widget/Button;

    iget-object v5, p0, Lcom/konka/appassistant/ApplicationInfoView;->mOnkKeyListener:Landroid/view/View$OnKeyListener;

    invoke-virtual {v4, v5}, Landroid/widget/Button;->setOnKeyListener(Landroid/view/View$OnKeyListener;)V

    iget-object v4, p0, Lcom/konka/appassistant/ApplicationInfoView;->mOkButton:Landroid/widget/Button;

    iget-object v5, p0, Lcom/konka/appassistant/ApplicationInfoView;->mOklistener:Landroid/view/View$OnClickListener;

    invoke-virtual {v4, v5}, Landroid/widget/Button;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    iget-object v4, p0, Lcom/konka/appassistant/ApplicationInfoView;->mOkButton:Landroid/widget/Button;

    iget-object v5, p0, Lcom/konka/appassistant/ApplicationInfoView;->mOnFocusChangeListener:Landroid/view/View$OnFocusChangeListener;

    invoke-virtual {v4, v5}, Landroid/widget/Button;->setOnFocusChangeListener(Landroid/view/View$OnFocusChangeListener;)V

    iget-object v4, p0, Lcom/konka/appassistant/ApplicationInfoView;->mAppView:Landroid/view/View;

    const v5, 0x7f07001a    # com.konka.appassistant.R.id.delete_button

    invoke-virtual {v4, v5}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v4

    check-cast v4, Landroid/widget/Button;

    iput-object v4, p0, Lcom/konka/appassistant/ApplicationInfoView;->mDeleteButton:Landroid/widget/Button;

    iget-object v4, p0, Lcom/konka/appassistant/ApplicationInfoView;->mDeleteButton:Landroid/widget/Button;

    iget-object v5, p0, Lcom/konka/appassistant/ApplicationInfoView;->mDeletelistener:Landroid/view/View$OnClickListener;

    invoke-virtual {v4, v5}, Landroid/widget/Button;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    iget-object v4, p0, Lcom/konka/appassistant/ApplicationInfoView;->mDeleteButton:Landroid/widget/Button;

    iget-object v5, p0, Lcom/konka/appassistant/ApplicationInfoView;->mOnFocusChangeListener:Landroid/view/View$OnFocusChangeListener;

    invoke-virtual {v4, v5}, Landroid/widget/Button;->setOnFocusChangeListener(Landroid/view/View$OnFocusChangeListener;)V

    iget-object v4, p0, Lcom/konka/appassistant/ApplicationInfoView;->mAppView:Landroid/view/View;

    const v5, 0x7f070018    # com.konka.appassistant.R.id.zip_install_result

    invoke-virtual {v4, v5}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v4

    check-cast v4, Landroid/widget/TextView;

    iput-object v4, p0, Lcom/konka/appassistant/ApplicationInfoView;->mZipInstallResult:Landroid/widget/TextView;

    iget-object v4, p0, Lcom/konka/appassistant/ApplicationInfoView;->mZipInstallResult:Landroid/widget/TextView;

    iget-object v5, p0, Lcom/konka/appassistant/ApplicationInfoView;->mOnFocusChangeListener:Landroid/view/View$OnFocusChangeListener;

    invoke-virtual {v4, v5}, Landroid/widget/TextView;->setOnFocusChangeListener(Landroid/view/View$OnFocusChangeListener;)V

    iget-object v4, p0, Lcom/konka/appassistant/ApplicationInfoView;->mZipInstallResult:Landroid/widget/TextView;

    iget-object v5, p0, Lcom/konka/appassistant/ApplicationInfoView;->mOnkKeyListener:Landroid/view/View$OnKeyListener;

    invoke-virtual {v4, v5}, Landroid/widget/TextView;->setOnKeyListener(Landroid/view/View$OnKeyListener;)V

    iget-object v4, p0, Lcom/konka/appassistant/ApplicationInfoView;->mAppView:Landroid/view/View;

    const v5, 0x7f070015    # com.konka.appassistant.R.id.appinstallStatus

    invoke-virtual {v4, v5}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v4

    check-cast v4, Landroid/widget/TextView;

    iput-object v4, p0, Lcom/konka/appassistant/ApplicationInfoView;->mInstallStatus:Landroid/widget/TextView;

    invoke-direct {p0}, Lcom/konka/appassistant/ApplicationInfoView;->initElement()V

    goto/16 :goto_1

    :cond_4
    iget-object v4, p0, Lcom/konka/appassistant/ApplicationInfoView;->mAppIcon:Landroid/widget/ImageView;

    iget-object v5, p0, Lcom/konka/appassistant/ApplicationInfoView;->mAppInfo:Landroid/content/pm/ApplicationInfo;

    iget-object v6, p0, Lcom/konka/appassistant/ApplicationInfoView;->pm:Landroid/content/pm/PackageManager;

    invoke-virtual {v5, v6}, Landroid/content/pm/ApplicationInfo;->loadIcon(Landroid/content/pm/PackageManager;)Landroid/graphics/drawable/Drawable;

    move-result-object v5

    invoke-virtual {v4, v5}, Landroid/widget/ImageView;->setBackgroundDrawable(Landroid/graphics/drawable/Drawable;)V

    goto/16 :goto_2

    :cond_5
    iget-object v4, p0, Lcom/konka/appassistant/ApplicationInfoView;->mAppLable:Landroid/widget/TextView;

    iget-object v5, p0, Lcom/konka/appassistant/ApplicationInfoView;->mAppInfo:Landroid/content/pm/ApplicationInfo;

    iget-object v6, p0, Lcom/konka/appassistant/ApplicationInfoView;->pm:Landroid/content/pm/PackageManager;

    invoke-virtual {v5, v6}, Landroid/content/pm/ApplicationInfo;->loadLabel(Landroid/content/pm/PackageManager;)Ljava/lang/CharSequence;

    move-result-object v5

    invoke-virtual {v4, v5}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    goto/16 :goto_3
.end method

.method private initElement()V
    .locals 5

    const/4 v4, 0x4

    const/16 v2, 0x8

    const/4 v3, 0x0

    invoke-static {}, Lcom/konka/appassistant/ApplicationInfoView;->$SWITCH_TABLE$com$konka$appassistant$ApplicationInfoView$ViewType()[I

    move-result-object v0

    iget-object v1, p0, Lcom/konka/appassistant/ApplicationInfoView;->mViewType:Lcom/konka/appassistant/ApplicationInfoView$ViewType;

    invoke-virtual {v1}, Lcom/konka/appassistant/ApplicationInfoView$ViewType;->ordinal()I

    move-result v1

    aget v0, v0, v1

    packed-switch v0, :pswitch_data_0

    :goto_0
    return-void

    :pswitch_0
    iget-object v0, p0, Lcom/konka/appassistant/ApplicationInfoView;->mZipInstallResult:Landroid/widget/TextView;

    invoke-virtual {v0, v2}, Landroid/widget/TextView;->setVisibility(I)V

    iget-object v0, p0, Lcom/konka/appassistant/ApplicationInfoView;->mOkButton:Landroid/widget/Button;

    invoke-virtual {v0, v3}, Landroid/widget/Button;->setVisibility(I)V

    iget-object v0, p0, Lcom/konka/appassistant/ApplicationInfoView;->mDeleteButton:Landroid/widget/Button;

    invoke-virtual {v0, v3}, Landroid/widget/Button;->setVisibility(I)V

    iget-object v0, p0, Lcom/konka/appassistant/ApplicationInfoView;->mOkButton:Landroid/widget/Button;

    iget-object v1, p0, Lcom/konka/appassistant/ApplicationInfoView;->mContext:Landroid/content/Context;

    const v2, 0x7f060015    # com.konka.appassistant.R.string.app_info_button_install

    invoke-virtual {v1, v2}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/widget/Button;->setText(Ljava/lang/CharSequence;)V

    iget-object v0, p0, Lcom/konka/appassistant/ApplicationInfoView;->mDeleteButton:Landroid/widget/Button;

    iget-object v1, p0, Lcom/konka/appassistant/ApplicationInfoView;->mContext:Landroid/content/Context;

    const v2, 0x7f060017    # com.konka.appassistant.R.string.app_info_button_delete

    invoke-virtual {v1, v2}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/widget/Button;->setText(Ljava/lang/CharSequence;)V

    iget-object v0, p0, Lcom/konka/appassistant/ApplicationInfoView;->mInstallStatus:Landroid/widget/TextView;

    invoke-virtual {v0, v3}, Landroid/widget/TextView;->setVisibility(I)V

    iget-object v0, p0, Lcom/konka/appassistant/ApplicationInfoView;->IsInstalled:Ljava/lang/Boolean;

    invoke-virtual {v0}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v0

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/konka/appassistant/ApplicationInfoView;->mInstallStatus:Landroid/widget/TextView;

    iget-object v1, p0, Lcom/konka/appassistant/ApplicationInfoView;->mContext:Landroid/content/Context;

    const v2, 0x7f06001a    # com.konka.appassistant.R.string.app_info_status_installed

    invoke-virtual {v1, v2}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    goto :goto_0

    :cond_0
    iget-object v0, p0, Lcom/konka/appassistant/ApplicationInfoView;->mInstallStatus:Landroid/widget/TextView;

    iget-object v1, p0, Lcom/konka/appassistant/ApplicationInfoView;->mContext:Landroid/content/Context;

    const v2, 0x7f06001b    # com.konka.appassistant.R.string.app_info_status_not_installed

    invoke-virtual {v1, v2}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    goto :goto_0

    :pswitch_1
    iget-object v0, p0, Lcom/konka/appassistant/ApplicationInfoView;->mZipInstallResult:Landroid/widget/TextView;

    invoke-virtual {v0, v2}, Landroid/widget/TextView;->setVisibility(I)V

    iget-object v0, p0, Lcom/konka/appassistant/ApplicationInfoView;->mOkButton:Landroid/widget/Button;

    invoke-virtual {v0, v3}, Landroid/widget/Button;->setVisibility(I)V

    iget-object v0, p0, Lcom/konka/appassistant/ApplicationInfoView;->mDeleteButton:Landroid/widget/Button;

    invoke-virtual {v0, v2}, Landroid/widget/Button;->setVisibility(I)V

    iget-object v0, p0, Lcom/konka/appassistant/ApplicationInfoView;->mOkButton:Landroid/widget/Button;

    iget-object v1, p0, Lcom/konka/appassistant/ApplicationInfoView;->mContext:Landroid/content/Context;

    const v2, 0x7f060016    # com.konka.appassistant.R.string.app_info_button_uninstall

    invoke-virtual {v1, v2}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/widget/Button;->setText(Ljava/lang/CharSequence;)V

    iget-object v0, p0, Lcom/konka/appassistant/ApplicationInfoView;->mInstallStatus:Landroid/widget/TextView;

    invoke-virtual {v0, v4}, Landroid/widget/TextView;->setVisibility(I)V

    goto :goto_0

    :pswitch_2
    iget-object v0, p0, Lcom/konka/appassistant/ApplicationInfoView;->mZipInstallResult:Landroid/widget/TextView;

    invoke-virtual {v0, v3}, Landroid/widget/TextView;->setVisibility(I)V

    iget-object v0, p0, Lcom/konka/appassistant/ApplicationInfoView;->mOkButton:Landroid/widget/Button;

    invoke-virtual {v0, v2}, Landroid/widget/Button;->setVisibility(I)V

    iget-object v0, p0, Lcom/konka/appassistant/ApplicationInfoView;->mDeleteButton:Landroid/widget/Button;

    invoke-virtual {v0, v2}, Landroid/widget/Button;->setVisibility(I)V

    iget-object v0, p0, Lcom/konka/appassistant/ApplicationInfoView;->mInstallStatus:Landroid/widget/TextView;

    invoke-virtual {v0, v4}, Landroid/widget/TextView;->setVisibility(I)V

    goto/16 :goto_0

    nop

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
        :pswitch_1
        :pswitch_2
    .end packed-switch
.end method

.method private setAppSize()V
    .locals 4

    const-wide/16 v0, 0x0

    iget-object v2, p0, Lcom/konka/appassistant/ApplicationInfoView;->mPackageUri:Landroid/net/Uri;

    invoke-virtual {v2}, Landroid/net/Uri;->getPath()Ljava/lang/String;

    move-result-object v2

    if-eqz v2, :cond_0

    new-instance v2, Ljava/io/File;

    iget-object v3, p0, Lcom/konka/appassistant/ApplicationInfoView;->mPackageUri:Landroid/net/Uri;

    invoke-virtual {v3}, Landroid/net/Uri;->getPath()Ljava/lang/String;

    move-result-object v3

    invoke-direct {v2, v3}, Ljava/io/File;-><init>(Ljava/lang/String;)V

    invoke-virtual {v2}, Ljava/io/File;->length()J

    move-result-wide v2

    long-to-int v2, v2

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/Integer;->intValue()I

    move-result v2

    int-to-long v0, v2

    long-to-float v2, v0

    invoke-virtual {p0, v2}, Lcom/konka/appassistant/ApplicationInfoView;->setAppSize(F)V

    :goto_0
    return-void

    :cond_0
    iget-wide v2, p0, Lcom/konka/appassistant/ApplicationInfoView;->totalsize:J

    long-to-float v2, v2

    invoke-virtual {p0, v2}, Lcom/konka/appassistant/ApplicationInfoView;->setAppSize(F)V

    goto :goto_0
.end method


# virtual methods
.method public IsFocused()I
    .locals 1

    iget-object v0, p0, Lcom/konka/appassistant/ApplicationInfoView;->mOkButton:Landroid/widget/Button;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/konka/appassistant/ApplicationInfoView;->mOkButton:Landroid/widget/Button;

    invoke-virtual {v0}, Landroid/widget/Button;->isFocused()Z

    move-result v0

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    iget-object v0, p0, Lcom/konka/appassistant/ApplicationInfoView;->mDeleteButton:Landroid/widget/Button;

    if-eqz v0, :cond_1

    iget-object v0, p0, Lcom/konka/appassistant/ApplicationInfoView;->mDeleteButton:Landroid/widget/Button;

    invoke-virtual {v0}, Landroid/widget/Button;->isFocused()Z

    move-result v0

    if-eqz v0, :cond_1

    const/4 v0, 0x2

    goto :goto_0

    :cond_1
    iget-object v0, p0, Lcom/konka/appassistant/ApplicationInfoView;->mZipInstallResult:Landroid/widget/TextView;

    if-eqz v0, :cond_2

    iget-object v0, p0, Lcom/konka/appassistant/ApplicationInfoView;->mZipInstallResult:Landroid/widget/TextView;

    invoke-virtual {v0}, Landroid/widget/TextView;->isFocused()Z

    move-result v0

    if-eqz v0, :cond_2

    const/4 v0, 0x3

    goto :goto_0

    :cond_2
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public ShowZipApkInstallResult(ILjava/lang/String;)V
    .locals 3
    .param p1    # I
    .param p2    # Ljava/lang/String;

    iget-object v0, p0, Lcom/konka/appassistant/ApplicationInfoView;->mViewType:Lcom/konka/appassistant/ApplicationInfoView$ViewType;

    sget-object v1, Lcom/konka/appassistant/ApplicationInfoView$ViewType;->ZIP_INSTALL_VIEW:Lcom/konka/appassistant/ApplicationInfoView$ViewType;

    if-eq v0, v1, :cond_1

    :cond_0
    :goto_0
    return-void

    :cond_1
    packed-switch p1, :pswitch_data_0

    goto :goto_0

    :pswitch_0
    iget-object v0, p0, Lcom/konka/appassistant/ApplicationInfoView;->mZipInstallResult:Landroid/widget/TextView;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/konka/appassistant/ApplicationInfoView;->mZipInstallResult:Landroid/widget/TextView;

    invoke-virtual {v0, p2}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    iget-object v0, p0, Lcom/konka/appassistant/ApplicationInfoView;->mZipInstallResult:Landroid/widget/TextView;

    const v1, -0x15feff

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setTextColor(I)V

    goto :goto_0

    :pswitch_1
    iget-object v0, p0, Lcom/konka/appassistant/ApplicationInfoView;->mZipInstallResult:Landroid/widget/TextView;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/konka/appassistant/ApplicationInfoView;->mZipInstallResult:Landroid/widget/TextView;

    iget-object v1, p0, Lcom/konka/appassistant/ApplicationInfoView;->mContext:Landroid/content/Context;

    const v2, 0x7f06000f    # com.konka.appassistant.R.string.app_assistant_zipinstall_succeed

    invoke-virtual {v1, v2}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    iget-object v0, p0, Lcom/konka/appassistant/ApplicationInfoView;->mZipInstallResult:Landroid/widget/TextView;

    const v1, -0x782f00

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setTextColor(I)V

    goto :goto_0

    :pswitch_data_0
    .packed-switch 0x6
        :pswitch_0
        :pswitch_1
    .end packed-switch
.end method

.method public getAppView()Landroid/view/View;
    .locals 1

    iget-object v0, p0, Lcom/konka/appassistant/ApplicationInfoView;->as:Lcom/konka/appassistant/PackageUtil$AppSnippet;

    if-nez v0, :cond_0

    const/4 v0, 0x0

    :goto_0
    return-object v0

    :cond_0
    iget-object v0, p0, Lcom/konka/appassistant/ApplicationInfoView;->mAppView:Landroid/view/View;

    goto :goto_0
.end method

.method public getChildNum()I
    .locals 1

    iget v0, p0, Lcom/konka/appassistant/ApplicationInfoView;->mChildNum:I

    return v0
.end method

.method public getFilename()Ljava/lang/String;
    .locals 1

    iget-object v0, p0, Lcom/konka/appassistant/ApplicationInfoView;->mPackageUri:Landroid/net/Uri;

    if-nez v0, :cond_0

    const/4 v0, 0x0

    :goto_0
    return-object v0

    :cond_0
    iget-object v0, p0, Lcom/konka/appassistant/ApplicationInfoView;->mPackageUri:Landroid/net/Uri;

    invoke-virtual {v0}, Landroid/net/Uri;->getPath()Ljava/lang/String;

    move-result-object v0

    goto :goto_0
.end method

.method public getViewType()Lcom/konka/appassistant/ApplicationInfoView$ViewType;
    .locals 1

    iget-object v0, p0, Lcom/konka/appassistant/ApplicationInfoView;->mViewType:Lcom/konka/appassistant/ApplicationInfoView$ViewType;

    return-object v0
.end method

.method public setAppSize(F)V
    .locals 7
    .param p1    # F

    const/high16 v5, 0x44800000    # 1024.0f

    const/high16 v4, 0x41200000    # 10.0f

    const v6, 0x7f060019    # com.konka.appassistant.R.string.app_info_size

    const/4 v0, 0x0

    :goto_0
    cmpl-float v3, p1, v5

    if-lez v3, :cond_0

    const/4 v3, 0x3

    if-lt v0, v3, :cond_1

    :cond_0
    mul-float v3, p1, v4

    float-to-int v3, v3

    int-to-long v1, v3

    long-to-float v3, v1

    div-float p1, v3, v4

    if-nez v0, :cond_2

    iget-object v3, p0, Lcom/konka/appassistant/ApplicationInfoView;->mAppSize:Landroid/widget/TextView;

    new-instance v4, Ljava/lang/StringBuilder;

    iget-object v5, p0, Lcom/konka/appassistant/ApplicationInfoView;->mContext:Landroid/content/Context;

    invoke-virtual {v5, v6}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v5

    invoke-static {v5}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v5

    invoke-direct {v4, v5}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-static {p1}, Ljava/lang/String;->valueOf(F)Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    const-string v5, "B"

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v3, v4}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    :goto_1
    return-void

    :cond_1
    div-float/2addr p1, v5

    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    :cond_2
    const/4 v3, 0x1

    if-ne v3, v0, :cond_3

    iget-object v3, p0, Lcom/konka/appassistant/ApplicationInfoView;->mAppSize:Landroid/widget/TextView;

    new-instance v4, Ljava/lang/StringBuilder;

    iget-object v5, p0, Lcom/konka/appassistant/ApplicationInfoView;->mContext:Landroid/content/Context;

    invoke-virtual {v5, v6}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v5

    invoke-static {v5}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v5

    invoke-direct {v4, v5}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-static {p1}, Ljava/lang/String;->valueOf(F)Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    const-string v5, "K"

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v3, v4}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    goto :goto_1

    :cond_3
    const/4 v3, 0x2

    if-ne v3, v0, :cond_4

    iget-object v3, p0, Lcom/konka/appassistant/ApplicationInfoView;->mAppSize:Landroid/widget/TextView;

    new-instance v4, Ljava/lang/StringBuilder;

    iget-object v5, p0, Lcom/konka/appassistant/ApplicationInfoView;->mContext:Landroid/content/Context;

    invoke-virtual {v5, v6}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v5

    invoke-static {v5}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v5

    invoke-direct {v4, v5}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-static {p1}, Ljava/lang/String;->valueOf(F)Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    const-string v5, "M"

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v3, v4}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    goto :goto_1

    :cond_4
    iget-object v3, p0, Lcom/konka/appassistant/ApplicationInfoView;->mAppSize:Landroid/widget/TextView;

    new-instance v4, Ljava/lang/StringBuilder;

    iget-object v5, p0, Lcom/konka/appassistant/ApplicationInfoView;->mContext:Landroid/content/Context;

    invoke-virtual {v5, v6}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v5

    invoke-static {v5}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v5

    invoke-direct {v4, v5}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-static {p1}, Ljava/lang/String;->valueOf(F)Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    const-string v5, "G"

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v3, v4}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    goto :goto_1
.end method

.method public setChildNum(I)V
    .locals 0
    .param p1    # I

    iput p1, p0, Lcom/konka/appassistant/ApplicationInfoView;->mChildNum:I

    return-void
.end method

.method public setFocused(I)V
    .locals 1
    .param p1    # I

    const/4 v0, 0x1

    if-ne v0, p1, :cond_1

    iget-object v0, p0, Lcom/konka/appassistant/ApplicationInfoView;->mOkButton:Landroid/widget/Button;

    invoke-virtual {v0}, Landroid/widget/Button;->requestFocus()Z

    :cond_0
    :goto_0
    return-void

    :cond_1
    const/4 v0, 0x2

    if-ne v0, p1, :cond_2

    iget-object v0, p0, Lcom/konka/appassistant/ApplicationInfoView;->mDeleteButton:Landroid/widget/Button;

    invoke-virtual {v0}, Landroid/widget/Button;->requestFocus()Z

    goto :goto_0

    :cond_2
    const/4 v0, 0x3

    if-ne v0, p1, :cond_0

    iget-object v0, p0, Lcom/konka/appassistant/ApplicationInfoView;->mZipInstallResult:Landroid/widget/TextView;

    invoke-virtual {v0}, Landroid/widget/TextView;->requestFocus()Z

    goto :goto_0
.end method

.method public setViewType(Lcom/konka/appassistant/ApplicationInfoView$ViewType;)V
    .locals 0
    .param p1    # Lcom/konka/appassistant/ApplicationInfoView$ViewType;

    iput-object p1, p0, Lcom/konka/appassistant/ApplicationInfoView;->mViewType:Lcom/konka/appassistant/ApplicationInfoView$ViewType;

    return-void
.end method
