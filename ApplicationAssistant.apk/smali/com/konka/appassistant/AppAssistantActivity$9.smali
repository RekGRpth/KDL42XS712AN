.class Lcom/konka/appassistant/AppAssistantActivity$9;
.super Ljava/lang/Object;
.source "AppAssistantActivity.java"

# interfaces
.implements Ljava/lang/Runnable;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/konka/appassistant/AppAssistantActivity;->onStart()V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/konka/appassistant/AppAssistantActivity;


# direct methods
.method constructor <init>(Lcom/konka/appassistant/AppAssistantActivity;)V
    .locals 0

    iput-object p1, p0, Lcom/konka/appassistant/AppAssistantActivity$9;->this$0:Lcom/konka/appassistant/AppAssistantActivity;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public run()V
    .locals 8

    iget-object v3, p0, Lcom/konka/appassistant/AppAssistantActivity$9;->this$0:Lcom/konka/appassistant/AppAssistantActivity;

    # getter for: Lcom/konka/appassistant/AppAssistantActivity;->mOneKeyInstallProcess:Lcom/konka/appassistant/OneKeyInstallProcess;
    invoke-static {v3}, Lcom/konka/appassistant/AppAssistantActivity;->access$8(Lcom/konka/appassistant/AppAssistantActivity;)Lcom/konka/appassistant/OneKeyInstallProcess;

    move-result-object v3

    invoke-virtual {v3}, Lcom/konka/appassistant/OneKeyInstallProcess;->IsZipPackageExist()Ljava/lang/Boolean;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v3

    if-eqz v3, :cond_1

    iget-object v3, p0, Lcom/konka/appassistant/AppAssistantActivity$9;->this$0:Lcom/konka/appassistant/AppAssistantActivity;

    # getter for: Lcom/konka/appassistant/AppAssistantActivity;->mOneKeyInstallProcess:Lcom/konka/appassistant/OneKeyInstallProcess;
    invoke-static {v3}, Lcom/konka/appassistant/AppAssistantActivity;->access$8(Lcom/konka/appassistant/AppAssistantActivity;)Lcom/konka/appassistant/OneKeyInstallProcess;

    move-result-object v3

    invoke-virtual {v3}, Lcom/konka/appassistant/OneKeyInstallProcess;->getkkapkFile()Ljava/lang/String;

    move-result-object v1

    if-eqz v1, :cond_0

    const/4 v3, 0x0

    const/4 v4, 0x1

    :try_start_0
    invoke-static {v1, v3, v4}, Lcom/konka/appassistant/ZipApi;->apacheGetZipFileList(Ljava/lang/String;ZZ)Ljava/util/List;

    move-result-object v2

    if-eqz v2, :cond_0

    iget-object v3, p0, Lcom/konka/appassistant/AppAssistantActivity$9;->this$0:Lcom/konka/appassistant/AppAssistantActivity;

    iget-object v4, p0, Lcom/konka/appassistant/AppAssistantActivity$9;->this$0:Lcom/konka/appassistant/AppAssistantActivity;

    const v5, 0x7f060006    # com.konka.appassistant.R.string.app_assistant_second_menu_app_num_install

    invoke-virtual {v4, v5}, Lcom/konka/appassistant/AppAssistantActivity;->getString(I)Ljava/lang/String;

    move-result-object v4

    const/4 v5, 0x1

    new-array v5, v5, [Ljava/lang/Object;

    const/4 v6, 0x0

    invoke-interface {v2}, Ljava/util/List;->size()I

    move-result v7

    invoke-static {v7}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v7

    aput-object v7, v5, v6

    invoke-static {v4, v5}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v4

    invoke-static {v3, v4}, Lcom/konka/appassistant/AppAssistantActivity;->access$24(Lcom/konka/appassistant/AppAssistantActivity;Ljava/lang/String;)V
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    :cond_0
    :goto_0
    iget-object v3, p0, Lcom/konka/appassistant/AppAssistantActivity$9;->this$0:Lcom/konka/appassistant/AppAssistantActivity;

    iget-object v3, v3, Lcom/konka/appassistant/AppAssistantActivity;->mMainHandler:Landroid/os/Handler;

    const/16 v4, 0x66

    invoke-virtual {v3, v4}, Landroid/os/Handler;->sendEmptyMessage(I)Z

    :goto_1
    return-void

    :catch_0
    move-exception v0

    invoke-virtual {v0}, Ljava/lang/Exception;->printStackTrace()V

    goto :goto_0

    :cond_1
    iget-object v3, p0, Lcom/konka/appassistant/AppAssistantActivity$9;->this$0:Lcom/konka/appassistant/AppAssistantActivity;

    iget-object v3, v3, Lcom/konka/appassistant/AppAssistantActivity;->mMainHandler:Landroid/os/Handler;

    const/16 v4, 0x65

    invoke-virtual {v3, v4}, Landroid/os/Handler;->sendEmptyMessage(I)Z

    goto :goto_1
.end method
