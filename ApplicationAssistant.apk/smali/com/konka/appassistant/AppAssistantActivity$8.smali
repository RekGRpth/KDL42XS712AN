.class Lcom/konka/appassistant/AppAssistantActivity$8;
.super Landroid/content/BroadcastReceiver;
.source "AppAssistantActivity.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/konka/appassistant/AppAssistantActivity;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/konka/appassistant/AppAssistantActivity;


# direct methods
.method constructor <init>(Lcom/konka/appassistant/AppAssistantActivity;)V
    .locals 0

    iput-object p1, p0, Lcom/konka/appassistant/AppAssistantActivity$8;->this$0:Lcom/konka/appassistant/AppAssistantActivity;

    invoke-direct {p0}, Landroid/content/BroadcastReceiver;-><init>()V

    return-void
.end method

.method static synthetic access$0(Lcom/konka/appassistant/AppAssistantActivity$8;)Lcom/konka/appassistant/AppAssistantActivity;
    .locals 1

    iget-object v0, p0, Lcom/konka/appassistant/AppAssistantActivity$8;->this$0:Lcom/konka/appassistant/AppAssistantActivity;

    return-object v0
.end method


# virtual methods
.method public onReceive(Landroid/content/Context;Landroid/content/Intent;)V
    .locals 3
    .param p1    # Landroid/content/Context;
    .param p2    # Landroid/content/Intent;

    invoke-virtual {p2}, Landroid/content/Intent;->getAction()Ljava/lang/String;

    move-result-object v0

    const-string v1, "android.intent.action.MEDIA_MOUNTED"

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-nez v1, :cond_0

    const-string v1, "android.intent.action.MEDIA_UNMOUNTED"

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_1

    :cond_0
    new-instance v1, Ljava/lang/Thread;

    new-instance v2, Lcom/konka/appassistant/AppAssistantActivity$8$1;

    invoke-direct {v2, p0}, Lcom/konka/appassistant/AppAssistantActivity$8$1;-><init>(Lcom/konka/appassistant/AppAssistantActivity$8;)V

    invoke-direct {v1, v2}, Ljava/lang/Thread;-><init>(Ljava/lang/Runnable;)V

    invoke-virtual {v1}, Ljava/lang/Thread;->start()V

    iget-object v1, p0, Lcom/konka/appassistant/AppAssistantActivity$8;->this$0:Lcom/konka/appassistant/AppAssistantActivity;

    # getter for: Lcom/konka/appassistant/AppAssistantActivity;->mViewType:Lcom/konka/appassistant/ApplicationInfoView$ViewType;
    invoke-static {v1}, Lcom/konka/appassistant/AppAssistantActivity;->access$0(Lcom/konka/appassistant/AppAssistantActivity;)Lcom/konka/appassistant/ApplicationInfoView$ViewType;

    move-result-object v1

    sget-object v2, Lcom/konka/appassistant/ApplicationInfoView$ViewType;->INSTALL_VIEW:Lcom/konka/appassistant/ApplicationInfoView$ViewType;

    if-ne v1, v2, :cond_1

    const-string v1, "android.intent.action.MEDIA_UNMOUNTED"

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_1

    iget-object v1, p0, Lcom/konka/appassistant/AppAssistantActivity$8;->this$0:Lcom/konka/appassistant/AppAssistantActivity;

    invoke-virtual {v1}, Lcom/konka/appassistant/AppAssistantActivity;->Refresh()V

    :cond_1
    return-void
.end method
