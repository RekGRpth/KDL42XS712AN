.class public Lcom/konka/appassistant/OneKeyInstallProcess;
.super Ljava/lang/Object;
.source "OneKeyInstallProcess.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/konka/appassistant/OneKeyInstallProcess$InstallStateDetail;,
        Lcom/konka/appassistant/OneKeyInstallProcess$SystemPackageAddedMsgReveicer;
    }
.end annotation


# static fields
.field private static DEBUG:Z = false

.field public static final INSTALL_STATE_CLEANING_TMP_FILES:I = 0x8

.field public static final INSTALL_STATE_DECOMPRESSING:I = 0x2

.field public static final INSTALL_STATE_DECOMPRESS_FAILED:I = 0x3

.field public static final INSTALL_STATE_DECOMPRESS_SUCCESSED:I = 0x4

.field public static final INSTALL_STATE_FINISHED:I = 0xa

.field public static final INSTALL_STATE_INSTALLING_APK:I = 0x5

.field public static final INSTALL_STATE_INSTALL_ONE_APK_FAILED:I = 0x6

.field public static final INSTALL_STATE_INSTALL_ONE_APK_SUCCESSED:I = 0x7

.field private static TAG:Ljava/lang/String;

.field private static uniqueInstance:Lcom/konka/appassistant/OneKeyInstallProcess;


# instance fields
.field mAllApkFileList:Ljava/util/ArrayList;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/ArrayList",
            "<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation
.end field

.field mContext:Landroid/content/Context;

.field mCurInstallIndex:I

.field mCurPackageInfo:Landroid/content/pm/PackageParser$Package;

.field mInstallIsCanceled:Z

.field mInstallerMsgReceiver:Lcom/konka/appassistant/InstallerMsgReceiver;

.field mKkApkUnzip:Lcom/konka/appassistant/KKApkUnzip;

.field mProcessListener:Lcom/konka/appassistant/InstallProcessListener;

.field mSystemPackageAddedReceiver:Lcom/konka/appassistant/OneKeyInstallProcess$SystemPackageAddedMsgReveicer;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    const-string v0, "OneKeyInstallProcess"

    sput-object v0, Lcom/konka/appassistant/OneKeyInstallProcess;->TAG:Ljava/lang/String;

    const/4 v0, 0x0

    sput-boolean v0, Lcom/konka/appassistant/OneKeyInstallProcess;->DEBUG:Z

    return-void
.end method

.method private constructor <init>()V
    .locals 1

    const/4 v0, 0x0

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    iput-object v0, p0, Lcom/konka/appassistant/OneKeyInstallProcess;->mInstallerMsgReceiver:Lcom/konka/appassistant/InstallerMsgReceiver;

    iput-object v0, p0, Lcom/konka/appassistant/OneKeyInstallProcess;->mSystemPackageAddedReceiver:Lcom/konka/appassistant/OneKeyInstallProcess$SystemPackageAddedMsgReveicer;

    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/konka/appassistant/OneKeyInstallProcess;->mInstallIsCanceled:Z

    return-void
.end method

.method static synthetic access$0()Z
    .locals 1

    sget-boolean v0, Lcom/konka/appassistant/OneKeyInstallProcess;->DEBUG:Z

    return v0
.end method

.method static synthetic access$1()Ljava/lang/String;
    .locals 1

    sget-object v0, Lcom/konka/appassistant/OneKeyInstallProcess;->TAG:Ljava/lang/String;

    return-object v0
.end method

.method static synthetic access$2(Lcom/konka/appassistant/OneKeyInstallProcess;Lcom/konka/appassistant/OneKeyInstallProcess$InstallStateDetail;)V
    .locals 0

    invoke-direct {p0, p1}, Lcom/konka/appassistant/OneKeyInstallProcess;->notifyListenser(Lcom/konka/appassistant/OneKeyInstallProcess$InstallStateDetail;)V

    return-void
.end method

.method static synthetic access$3(Lcom/konka/appassistant/OneKeyInstallProcess;)V
    .locals 0

    invoke-direct {p0}, Lcom/konka/appassistant/OneKeyInstallProcess;->oneKeyInstallFinished()V

    return-void
.end method

.method static synthetic access$4(Lcom/konka/appassistant/OneKeyInstallProcess;)V
    .locals 0

    invoke-direct {p0}, Lcom/konka/appassistant/OneKeyInstallProcess;->beginInstallApks()V

    return-void
.end method

.method private beginInstallApks()V
    .locals 4

    iget-object v2, p0, Lcom/konka/appassistant/OneKeyInstallProcess;->mAllApkFileList:Ljava/util/ArrayList;

    if-eqz v2, :cond_0

    iget-object v2, p0, Lcom/konka/appassistant/OneKeyInstallProcess;->mAllApkFileList:Ljava/util/ArrayList;

    invoke-virtual {v2}, Ljava/util/ArrayList;->size()I

    move-result v2

    if-nez v2, :cond_1

    :cond_0
    new-instance v1, Lcom/konka/appassistant/OneKeyInstallProcess$InstallStateDetail;

    invoke-direct {v1}, Lcom/konka/appassistant/OneKeyInstallProcess$InstallStateDetail;-><init>()V

    const/16 v2, 0xa

    iput v2, v1, Lcom/konka/appassistant/OneKeyInstallProcess$InstallStateDetail;->mCurProcessState:I

    invoke-direct {p0, v1}, Lcom/konka/appassistant/OneKeyInstallProcess;->notifyListenser(Lcom/konka/appassistant/OneKeyInstallProcess$InstallStateDetail;)V

    :goto_0
    return-void

    :cond_1
    sget-boolean v2, Lcom/konka/appassistant/OneKeyInstallProcess;->DEBUG:Z

    if-eqz v2, :cond_2

    sget-object v2, Lcom/konka/appassistant/OneKeyInstallProcess;->TAG:Ljava/lang/String;

    const-string v3, "=======beginInstallApks()========"

    invoke-static {v2, v3}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    :cond_2
    invoke-direct {p0}, Lcom/konka/appassistant/OneKeyInstallProcess;->registerInstallMsgReceiver()V

    const/4 v2, 0x0

    iput v2, p0, Lcom/konka/appassistant/OneKeyInstallProcess;->mCurInstallIndex:I

    iget-object v2, p0, Lcom/konka/appassistant/OneKeyInstallProcess;->mAllApkFileList:Ljava/util/ArrayList;

    iget v3, p0, Lcom/konka/appassistant/OneKeyInstallProcess;->mCurInstallIndex:I

    invoke-virtual {v2, v3}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/String;

    invoke-direct {p0, v0}, Lcom/konka/appassistant/OneKeyInstallProcess;->installOneApk(Ljava/lang/String;)V

    goto :goto_0
.end method

.method private endInstallApks()V
    .locals 3

    invoke-direct {p0}, Lcom/konka/appassistant/OneKeyInstallProcess;->unregisterInstallMsgReceiver()V

    sget-boolean v1, Lcom/konka/appassistant/OneKeyInstallProcess;->DEBUG:Z

    if-eqz v1, :cond_0

    sget-object v1, Lcom/konka/appassistant/OneKeyInstallProcess;->TAG:Ljava/lang/String;

    const-string v2, " ==========clean unzip apks======"

    invoke-static {v1, v2}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    :cond_0
    new-instance v0, Lcom/konka/appassistant/OneKeyInstallProcess$InstallStateDetail;

    invoke-direct {v0}, Lcom/konka/appassistant/OneKeyInstallProcess$InstallStateDetail;-><init>()V

    const/16 v1, 0x8

    iput v1, v0, Lcom/konka/appassistant/OneKeyInstallProcess$InstallStateDetail;->mCurProcessState:I

    invoke-direct {p0, v0}, Lcom/konka/appassistant/OneKeyInstallProcess;->notifyListenser(Lcom/konka/appassistant/OneKeyInstallProcess$InstallStateDetail;)V

    iget-object v1, p0, Lcom/konka/appassistant/OneKeyInstallProcess;->mKkApkUnzip:Lcom/konka/appassistant/KKApkUnzip;

    invoke-virtual {v1}, Lcom/konka/appassistant/KKApkUnzip;->clearUnzipApks()V

    invoke-direct {p0}, Lcom/konka/appassistant/OneKeyInstallProcess;->oneKeyInstallFinished()V

    return-void
.end method

.method public static getInstance()Lcom/konka/appassistant/OneKeyInstallProcess;
    .locals 2

    sget-object v0, Lcom/konka/appassistant/OneKeyInstallProcess;->uniqueInstance:Lcom/konka/appassistant/OneKeyInstallProcess;

    if-nez v0, :cond_0

    const-class v1, Lcom/konka/appassistant/OneKeyInstallProcess;

    monitor-enter v1

    :try_start_0
    new-instance v0, Lcom/konka/appassistant/OneKeyInstallProcess;

    invoke-direct {v0}, Lcom/konka/appassistant/OneKeyInstallProcess;-><init>()V

    sput-object v0, Lcom/konka/appassistant/OneKeyInstallProcess;->uniqueInstance:Lcom/konka/appassistant/OneKeyInstallProcess;

    monitor-exit v1
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    :cond_0
    sget-object v0, Lcom/konka/appassistant/OneKeyInstallProcess;->uniqueInstance:Lcom/konka/appassistant/OneKeyInstallProcess;

    return-object v0

    :catchall_0
    move-exception v0

    :try_start_1
    monitor-exit v1
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    throw v0
.end method

.method private installOneApk(Ljava/lang/String;)V
    .locals 7
    .param p1    # Ljava/lang/String;

    new-instance v1, Lcom/konka/appassistant/OneKeyInstallProcess$InstallStateDetail;

    invoke-direct {v1}, Lcom/konka/appassistant/OneKeyInstallProcess$InstallStateDetail;-><init>()V

    const/4 v4, 0x5

    iput v4, v1, Lcom/konka/appassistant/OneKeyInstallProcess$InstallStateDetail;->mCurProcessState:I

    iput-object p1, v1, Lcom/konka/appassistant/OneKeyInstallProcess$InstallStateDetail;->mCurApkPath:Ljava/lang/String;

    iget v4, p0, Lcom/konka/appassistant/OneKeyInstallProcess;->mCurInstallIndex:I

    add-int/lit8 v4, v4, 0x1

    iput v4, v1, Lcom/konka/appassistant/OneKeyInstallProcess$InstallStateDetail;->mCurrentIndex:I

    iget-object v4, p0, Lcom/konka/appassistant/OneKeyInstallProcess;->mAllApkFileList:Ljava/util/ArrayList;

    invoke-virtual {v4}, Ljava/util/ArrayList;->size()I

    move-result v4

    iput v4, v1, Lcom/konka/appassistant/OneKeyInstallProcess$InstallStateDetail;->mTotalApkNum:I

    invoke-direct {p0, v1}, Lcom/konka/appassistant/OneKeyInstallProcess;->notifyListenser(Lcom/konka/appassistant/OneKeyInstallProcess$InstallStateDetail;)V

    new-instance v4, Ljava/io/File;

    invoke-direct {v4, p1}, Ljava/io/File;-><init>(Ljava/lang/String;)V

    invoke-static {v4}, Landroid/net/Uri;->fromFile(Ljava/io/File;)Landroid/net/Uri;

    move-result-object v0

    invoke-static {v0}, Lcom/konka/appassistant/PackageUtil;->getPackageInfo(Landroid/net/Uri;)Landroid/content/pm/PackageParser$Package;

    move-result-object v3

    :goto_0
    if-eqz v3, :cond_1

    iput-object v3, p0, Lcom/konka/appassistant/OneKeyInstallProcess;->mCurPackageInfo:Landroid/content/pm/PackageParser$Package;

    sget-boolean v4, Lcom/konka/appassistant/OneKeyInstallProcess;->DEBUG:Z

    if-eqz v4, :cond_0

    sget-object v4, Lcom/konka/appassistant/OneKeyInstallProcess;->TAG:Ljava/lang/String;

    new-instance v5, Ljava/lang/StringBuilder;

    const-string v6, "InstallOneApk:Path = "

    invoke-direct {v5, v6}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v5, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    invoke-static {v4, v5}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    :cond_0
    new-instance v2, Landroid/content/Intent;

    invoke-direct {v2}, Landroid/content/Intent;-><init>()V

    const-string v4, "com.konka.ACTION.SILENT_INSTALL"

    invoke-virtual {v2, v4}, Landroid/content/Intent;->setAction(Ljava/lang/String;)Landroid/content/Intent;

    const-string v4, "FILE_NAME"

    invoke-virtual {v2, v4, p1}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    const-string v4, "REMOVE_APK_AFTER_INSTALL"

    const/4 v5, 0x0

    invoke-virtual {v2, v4, v5}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Z)Landroid/content/Intent;

    iget-object v4, p0, Lcom/konka/appassistant/OneKeyInstallProcess;->mContext:Landroid/content/Context;

    invoke-virtual {v4, v2}, Landroid/content/Context;->sendBroadcast(Landroid/content/Intent;)V

    :goto_1
    return-void

    :cond_1
    sget-object v4, Lcom/konka/appassistant/OneKeyInstallProcess;->TAG:Ljava/lang/String;

    new-instance v5, Ljava/lang/StringBuilder;

    const-string v6, "get packageInfo is null!! apk path = "

    invoke-direct {v5, v6}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v5, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    invoke-static {v4, v5}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    iget v4, p0, Lcom/konka/appassistant/OneKeyInstallProcess;->mCurInstallIndex:I

    add-int/lit8 v4, v4, 0x1

    iput v4, p0, Lcom/konka/appassistant/OneKeyInstallProcess;->mCurInstallIndex:I

    iget v4, p0, Lcom/konka/appassistant/OneKeyInstallProcess;->mCurInstallIndex:I

    iget-object v5, p0, Lcom/konka/appassistant/OneKeyInstallProcess;->mAllApkFileList:Ljava/util/ArrayList;

    invoke-virtual {v5}, Ljava/util/ArrayList;->size()I

    move-result v5

    if-lt v4, v5, :cond_2

    invoke-direct {p0}, Lcom/konka/appassistant/OneKeyInstallProcess;->endInstallApks()V

    goto :goto_1

    :cond_2
    iget-object v4, p0, Lcom/konka/appassistant/OneKeyInstallProcess;->mAllApkFileList:Ljava/util/ArrayList;

    iget v5, p0, Lcom/konka/appassistant/OneKeyInstallProcess;->mCurInstallIndex:I

    invoke-virtual {v4, v5}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object p1

    check-cast p1, Ljava/lang/String;

    new-instance v4, Ljava/io/File;

    invoke-direct {v4, p1}, Ljava/io/File;-><init>(Ljava/lang/String;)V

    invoke-static {v4}, Landroid/net/Uri;->fromFile(Ljava/io/File;)Landroid/net/Uri;

    move-result-object v0

    invoke-static {v0}, Lcom/konka/appassistant/PackageUtil;->getPackageInfo(Landroid/net/Uri;)Landroid/content/pm/PackageParser$Package;

    move-result-object v3

    goto :goto_0
.end method

.method private notifyListenser(Lcom/konka/appassistant/OneKeyInstallProcess$InstallStateDetail;)V
    .locals 1
    .param p1    # Lcom/konka/appassistant/OneKeyInstallProcess$InstallStateDetail;

    iget-object v0, p0, Lcom/konka/appassistant/OneKeyInstallProcess;->mProcessListener:Lcom/konka/appassistant/InstallProcessListener;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/konka/appassistant/OneKeyInstallProcess;->mProcessListener:Lcom/konka/appassistant/InstallProcessListener;

    invoke-interface {v0, p1}, Lcom/konka/appassistant/InstallProcessListener;->onProcseeChanged(Lcom/konka/appassistant/OneKeyInstallProcess$InstallStateDetail;)V

    :cond_0
    return-void
.end method

.method private oneKeyInstallFinished()V
    .locals 2

    new-instance v0, Lcom/konka/appassistant/OneKeyInstallProcess$InstallStateDetail;

    invoke-direct {v0}, Lcom/konka/appassistant/OneKeyInstallProcess$InstallStateDetail;-><init>()V

    const/16 v1, 0xa

    iput v1, v0, Lcom/konka/appassistant/OneKeyInstallProcess$InstallStateDetail;->mCurProcessState:I

    invoke-direct {p0, v0}, Lcom/konka/appassistant/OneKeyInstallProcess;->notifyListenser(Lcom/konka/appassistant/OneKeyInstallProcess$InstallStateDetail;)V

    return-void
.end method

.method private registerInstallMsgReceiver()V
    .locals 4

    sget-boolean v2, Lcom/konka/appassistant/OneKeyInstallProcess;->DEBUG:Z

    if-eqz v2, :cond_0

    sget-object v2, Lcom/konka/appassistant/OneKeyInstallProcess;->TAG:Ljava/lang/String;

    const-string v3, "====registerReceiver()===="

    invoke-static {v2, v3}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    :cond_0
    iget-object v2, p0, Lcom/konka/appassistant/OneKeyInstallProcess;->mInstallerMsgReceiver:Lcom/konka/appassistant/InstallerMsgReceiver;

    if-nez v2, :cond_1

    new-instance v2, Lcom/konka/appassistant/InstallerMsgReceiver;

    invoke-direct {v2}, Lcom/konka/appassistant/InstallerMsgReceiver;-><init>()V

    iput-object v2, p0, Lcom/konka/appassistant/OneKeyInstallProcess;->mInstallerMsgReceiver:Lcom/konka/appassistant/InstallerMsgReceiver;

    :cond_1
    new-instance v0, Landroid/content/IntentFilter;

    const-string v2, "com.konka.ACTION.SILENT_INSTALL_COMPLETE"

    invoke-direct {v0, v2}, Landroid/content/IntentFilter;-><init>(Ljava/lang/String;)V

    iget-object v2, p0, Lcom/konka/appassistant/OneKeyInstallProcess;->mContext:Landroid/content/Context;

    iget-object v3, p0, Lcom/konka/appassistant/OneKeyInstallProcess;->mInstallerMsgReceiver:Lcom/konka/appassistant/InstallerMsgReceiver;

    invoke-virtual {v2, v3, v0}, Landroid/content/Context;->registerReceiver(Landroid/content/BroadcastReceiver;Landroid/content/IntentFilter;)Landroid/content/Intent;

    iget-object v2, p0, Lcom/konka/appassistant/OneKeyInstallProcess;->mSystemPackageAddedReceiver:Lcom/konka/appassistant/OneKeyInstallProcess$SystemPackageAddedMsgReveicer;

    if-nez v2, :cond_2

    new-instance v2, Lcom/konka/appassistant/OneKeyInstallProcess$SystemPackageAddedMsgReveicer;

    invoke-direct {v2, p0}, Lcom/konka/appassistant/OneKeyInstallProcess$SystemPackageAddedMsgReveicer;-><init>(Lcom/konka/appassistant/OneKeyInstallProcess;)V

    iput-object v2, p0, Lcom/konka/appassistant/OneKeyInstallProcess;->mSystemPackageAddedReceiver:Lcom/konka/appassistant/OneKeyInstallProcess$SystemPackageAddedMsgReveicer;

    :cond_2
    new-instance v1, Landroid/content/IntentFilter;

    const-string v2, "android.intent.action.PACKAGE_ADDED"

    invoke-direct {v1, v2}, Landroid/content/IntentFilter;-><init>(Ljava/lang/String;)V

    const-string v2, "package"

    invoke-virtual {v1, v2}, Landroid/content/IntentFilter;->addDataScheme(Ljava/lang/String;)V

    iget-object v2, p0, Lcom/konka/appassistant/OneKeyInstallProcess;->mContext:Landroid/content/Context;

    iget-object v3, p0, Lcom/konka/appassistant/OneKeyInstallProcess;->mSystemPackageAddedReceiver:Lcom/konka/appassistant/OneKeyInstallProcess$SystemPackageAddedMsgReveicer;

    invoke-virtual {v2, v3, v1}, Landroid/content/Context;->registerReceiver(Landroid/content/BroadcastReceiver;Landroid/content/IntentFilter;)Landroid/content/Intent;

    return-void
.end method

.method private unregisterInstallMsgReceiver()V
    .locals 3

    const/4 v2, 0x0

    sget-boolean v0, Lcom/konka/appassistant/OneKeyInstallProcess;->DEBUG:Z

    if-eqz v0, :cond_0

    sget-object v0, Lcom/konka/appassistant/OneKeyInstallProcess;->TAG:Ljava/lang/String;

    const-string v1, "====unregisterReceiver()===="

    invoke-static {v0, v1}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    :cond_0
    iget-object v0, p0, Lcom/konka/appassistant/OneKeyInstallProcess;->mInstallerMsgReceiver:Lcom/konka/appassistant/InstallerMsgReceiver;

    if-eqz v0, :cond_1

    iget-object v0, p0, Lcom/konka/appassistant/OneKeyInstallProcess;->mContext:Landroid/content/Context;

    iget-object v1, p0, Lcom/konka/appassistant/OneKeyInstallProcess;->mInstallerMsgReceiver:Lcom/konka/appassistant/InstallerMsgReceiver;

    invoke-virtual {v0, v1}, Landroid/content/Context;->unregisterReceiver(Landroid/content/BroadcastReceiver;)V

    iput-object v2, p0, Lcom/konka/appassistant/OneKeyInstallProcess;->mInstallerMsgReceiver:Lcom/konka/appassistant/InstallerMsgReceiver;

    :cond_1
    iget-object v0, p0, Lcom/konka/appassistant/OneKeyInstallProcess;->mSystemPackageAddedReceiver:Lcom/konka/appassistant/OneKeyInstallProcess$SystemPackageAddedMsgReveicer;

    if-eqz v0, :cond_2

    iget-object v0, p0, Lcom/konka/appassistant/OneKeyInstallProcess;->mContext:Landroid/content/Context;

    iget-object v1, p0, Lcom/konka/appassistant/OneKeyInstallProcess;->mSystemPackageAddedReceiver:Lcom/konka/appassistant/OneKeyInstallProcess$SystemPackageAddedMsgReveicer;

    invoke-virtual {v0, v1}, Landroid/content/Context;->unregisterReceiver(Landroid/content/BroadcastReceiver;)V

    iput-object v2, p0, Lcom/konka/appassistant/OneKeyInstallProcess;->mSystemPackageAddedReceiver:Lcom/konka/appassistant/OneKeyInstallProcess$SystemPackageAddedMsgReveicer;

    :cond_2
    return-void
.end method


# virtual methods
.method public IsZipPackageExist()Ljava/lang/Boolean;
    .locals 1

    iget-object v0, p0, Lcom/konka/appassistant/OneKeyInstallProcess;->mKkApkUnzip:Lcom/konka/appassistant/KKApkUnzip;

    invoke-virtual {v0}, Lcom/konka/appassistant/KKApkUnzip;->kkapkFileExists()Z

    move-result v0

    invoke-static {v0}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v0

    return-object v0
.end method

.method public getkkapkFile()Ljava/lang/String;
    .locals 1

    iget-object v0, p0, Lcom/konka/appassistant/OneKeyInstallProcess;->mKkApkUnzip:Lcom/konka/appassistant/KKApkUnzip;

    invoke-virtual {v0}, Lcom/konka/appassistant/KKApkUnzip;->getkkapkFile()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public onInstallOneApkFinished(ILjava/lang/String;Ljava/lang/String;)V
    .locals 4
    .param p1    # I
    .param p2    # Ljava/lang/String;
    .param p3    # Ljava/lang/String;

    sget-boolean v1, Lcom/konka/appassistant/OneKeyInstallProcess;->DEBUG:Z

    if-eqz v1, :cond_0

    sget-object v1, Lcom/konka/appassistant/OneKeyInstallProcess;->TAG:Ljava/lang/String;

    new-instance v2, Ljava/lang/StringBuilder;

    const-string v3, "onInstallOneApkFinished:result code = "

    invoke-direct {v2, v3}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v2, p1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, ",apk path = "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, p2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    :cond_0
    iget-object v2, p0, Lcom/konka/appassistant/OneKeyInstallProcess;->mContext:Landroid/content/Context;

    monitor-enter v2

    :try_start_0
    iget-object v1, p0, Lcom/konka/appassistant/OneKeyInstallProcess;->mAllApkFileList:Ljava/util/ArrayList;

    iget v3, p0, Lcom/konka/appassistant/OneKeyInstallProcess;->mCurInstallIndex:I

    invoke-virtual {v1, v3}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Ljava/lang/String;

    invoke-virtual {v1, p2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-nez v1, :cond_1

    const-string v1, "ttttt"

    const-string v3, "apk path is not current apk path is being installed,return!!"

    invoke-static {v1, v3}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    monitor-exit v2

    :goto_0
    return-void

    :cond_1
    new-instance v0, Lcom/konka/appassistant/OneKeyInstallProcess$InstallStateDetail;

    invoke-direct {v0}, Lcom/konka/appassistant/OneKeyInstallProcess$InstallStateDetail;-><init>()V

    const/4 v1, 0x1

    if-eq p1, v1, :cond_2

    const/4 v1, 0x6

    iput v1, v0, Lcom/konka/appassistant/OneKeyInstallProcess$InstallStateDetail;->mCurProcessState:I

    iput p1, v0, Lcom/konka/appassistant/OneKeyInstallProcess$InstallStateDetail;->mErrorCode:I

    :goto_1
    iput-object p2, v0, Lcom/konka/appassistant/OneKeyInstallProcess$InstallStateDetail;->mCurApkPath:Ljava/lang/String;

    iget v1, p0, Lcom/konka/appassistant/OneKeyInstallProcess;->mCurInstallIndex:I

    add-int/lit8 v1, v1, 0x1

    iput v1, v0, Lcom/konka/appassistant/OneKeyInstallProcess$InstallStateDetail;->mCurrentIndex:I

    iget-object v1, p0, Lcom/konka/appassistant/OneKeyInstallProcess;->mAllApkFileList:Ljava/util/ArrayList;

    invoke-virtual {v1}, Ljava/util/ArrayList;->size()I

    move-result v1

    iput v1, v0, Lcom/konka/appassistant/OneKeyInstallProcess$InstallStateDetail;->mTotalApkNum:I

    invoke-direct {p0, v0}, Lcom/konka/appassistant/OneKeyInstallProcess;->notifyListenser(Lcom/konka/appassistant/OneKeyInstallProcess$InstallStateDetail;)V

    monitor-exit v2
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    iget v1, p0, Lcom/konka/appassistant/OneKeyInstallProcess;->mCurInstallIndex:I

    iget-object v2, p0, Lcom/konka/appassistant/OneKeyInstallProcess;->mAllApkFileList:Ljava/util/ArrayList;

    invoke-virtual {v2}, Ljava/util/ArrayList;->size()I

    move-result v2

    add-int/lit8 v2, v2, -0x1

    if-ne v1, v2, :cond_3

    invoke-direct {p0}, Lcom/konka/appassistant/OneKeyInstallProcess;->endInstallApks()V

    goto :goto_0

    :cond_2
    const/4 v1, 0x7

    :try_start_1
    iput v1, v0, Lcom/konka/appassistant/OneKeyInstallProcess$InstallStateDetail;->mCurProcessState:I

    goto :goto_1

    :catchall_0
    move-exception v1

    monitor-exit v2
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    throw v1

    :cond_3
    iget-boolean v1, p0, Lcom/konka/appassistant/OneKeyInstallProcess;->mInstallIsCanceled:Z

    if-nez v1, :cond_4

    iget v1, p0, Lcom/konka/appassistant/OneKeyInstallProcess;->mCurInstallIndex:I

    add-int/lit8 v1, v1, 0x1

    iput v1, p0, Lcom/konka/appassistant/OneKeyInstallProcess;->mCurInstallIndex:I

    iget-object v1, p0, Lcom/konka/appassistant/OneKeyInstallProcess;->mAllApkFileList:Ljava/util/ArrayList;

    iget v2, p0, Lcom/konka/appassistant/OneKeyInstallProcess;->mCurInstallIndex:I

    invoke-virtual {v1, v2}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Ljava/lang/String;

    invoke-direct {p0, v1}, Lcom/konka/appassistant/OneKeyInstallProcess;->installOneApk(Ljava/lang/String;)V

    goto :goto_0

    :cond_4
    invoke-direct {p0}, Lcom/konka/appassistant/OneKeyInstallProcess;->endInstallApks()V

    goto :goto_0
.end method

.method public onUnzipCall(ILjava/lang/String;)V
    .locals 4
    .param p1    # I
    .param p2    # Ljava/lang/String;

    sget-object v1, Lcom/konka/appassistant/OneKeyInstallProcess;->TAG:Ljava/lang/String;

    new-instance v2, Ljava/lang/StringBuilder;

    const-string v3, "curIndex = "

    invoke-direct {v2, v3}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v2, p1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, "unzipApkFile"

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, p2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    new-instance v0, Lcom/konka/appassistant/OneKeyInstallProcess$InstallStateDetail;

    invoke-direct {v0}, Lcom/konka/appassistant/OneKeyInstallProcess$InstallStateDetail;-><init>()V

    const/4 v1, 0x2

    iput v1, v0, Lcom/konka/appassistant/OneKeyInstallProcess$InstallStateDetail;->mCurProcessState:I

    iput p1, v0, Lcom/konka/appassistant/OneKeyInstallProcess$InstallStateDetail;->mCurrentIndex:I

    iput-object p2, v0, Lcom/konka/appassistant/OneKeyInstallProcess$InstallStateDetail;->mCurApkPath:Ljava/lang/String;

    iget-object v1, p0, Lcom/konka/appassistant/OneKeyInstallProcess;->mProcessListener:Lcom/konka/appassistant/InstallProcessListener;

    if-eqz v1, :cond_0

    iget-object v1, p0, Lcom/konka/appassistant/OneKeyInstallProcess;->mProcessListener:Lcom/konka/appassistant/InstallProcessListener;

    invoke-interface {v1, v0}, Lcom/konka/appassistant/InstallProcessListener;->onProcseeChanged(Lcom/konka/appassistant/OneKeyInstallProcess$InstallStateDetail;)V

    :cond_0
    return-void
.end method

.method public setContext(Landroid/content/Context;)V
    .locals 1
    .param p1    # Landroid/content/Context;

    iput-object p1, p0, Lcom/konka/appassistant/OneKeyInstallProcess;->mContext:Landroid/content/Context;

    new-instance v0, Lcom/konka/appassistant/KKApkUnzip;

    invoke-direct {v0, p1}, Lcom/konka/appassistant/KKApkUnzip;-><init>(Landroid/content/Context;)V

    iput-object v0, p0, Lcom/konka/appassistant/OneKeyInstallProcess;->mKkApkUnzip:Lcom/konka/appassistant/KKApkUnzip;

    return-void
.end method

.method public setProcessListenser(Lcom/konka/appassistant/InstallProcessListener;)V
    .locals 0
    .param p1    # Lcom/konka/appassistant/InstallProcessListener;

    iput-object p1, p0, Lcom/konka/appassistant/OneKeyInstallProcess;->mProcessListener:Lcom/konka/appassistant/InstallProcessListener;

    return-void
.end method

.method public startOneKeyInstallThread()V
    .locals 2

    new-instance v1, Lcom/konka/appassistant/OneKeyInstallProcess$1;

    invoke-direct {v1, p0}, Lcom/konka/appassistant/OneKeyInstallProcess$1;-><init>(Lcom/konka/appassistant/OneKeyInstallProcess;)V

    new-instance v0, Ljava/lang/Thread;

    invoke-direct {v0, v1}, Ljava/lang/Thread;-><init>(Ljava/lang/Runnable;)V

    invoke-virtual {v0}, Ljava/lang/Thread;->start()V

    return-void
.end method

.method public stopInstallThread()V
    .locals 1

    invoke-direct {p0}, Lcom/konka/appassistant/OneKeyInstallProcess;->unregisterInstallMsgReceiver()V

    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/konka/appassistant/OneKeyInstallProcess;->mInstallIsCanceled:Z

    return-void
.end method
