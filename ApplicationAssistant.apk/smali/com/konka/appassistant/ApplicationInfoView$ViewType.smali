.class public final enum Lcom/konka/appassistant/ApplicationInfoView$ViewType;
.super Ljava/lang/Enum;
.source "ApplicationInfoView.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/konka/appassistant/ApplicationInfoView;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x4019
    name = "ViewType"
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Enum",
        "<",
        "Lcom/konka/appassistant/ApplicationInfoView$ViewType;",
        ">;"
    }
.end annotation


# static fields
.field private static final synthetic ENUM$VALUES:[Lcom/konka/appassistant/ApplicationInfoView$ViewType;

.field public static final enum INSTALL_VIEW:Lcom/konka/appassistant/ApplicationInfoView$ViewType;

.field public static final enum UNINSTALL_VIEW:Lcom/konka/appassistant/ApplicationInfoView$ViewType;

.field public static final enum ZIP_INSTALL_VIEW:Lcom/konka/appassistant/ApplicationInfoView$ViewType;


# direct methods
.method static constructor <clinit>()V
    .locals 5

    const/4 v4, 0x2

    const/4 v3, 0x1

    const/4 v2, 0x0

    new-instance v0, Lcom/konka/appassistant/ApplicationInfoView$ViewType;

    const-string v1, "INSTALL_VIEW"

    invoke-direct {v0, v1, v2}, Lcom/konka/appassistant/ApplicationInfoView$ViewType;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/konka/appassistant/ApplicationInfoView$ViewType;->INSTALL_VIEW:Lcom/konka/appassistant/ApplicationInfoView$ViewType;

    new-instance v0, Lcom/konka/appassistant/ApplicationInfoView$ViewType;

    const-string v1, "UNINSTALL_VIEW"

    invoke-direct {v0, v1, v3}, Lcom/konka/appassistant/ApplicationInfoView$ViewType;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/konka/appassistant/ApplicationInfoView$ViewType;->UNINSTALL_VIEW:Lcom/konka/appassistant/ApplicationInfoView$ViewType;

    new-instance v0, Lcom/konka/appassistant/ApplicationInfoView$ViewType;

    const-string v1, "ZIP_INSTALL_VIEW"

    invoke-direct {v0, v1, v4}, Lcom/konka/appassistant/ApplicationInfoView$ViewType;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/konka/appassistant/ApplicationInfoView$ViewType;->ZIP_INSTALL_VIEW:Lcom/konka/appassistant/ApplicationInfoView$ViewType;

    const/4 v0, 0x3

    new-array v0, v0, [Lcom/konka/appassistant/ApplicationInfoView$ViewType;

    sget-object v1, Lcom/konka/appassistant/ApplicationInfoView$ViewType;->INSTALL_VIEW:Lcom/konka/appassistant/ApplicationInfoView$ViewType;

    aput-object v1, v0, v2

    sget-object v1, Lcom/konka/appassistant/ApplicationInfoView$ViewType;->UNINSTALL_VIEW:Lcom/konka/appassistant/ApplicationInfoView$ViewType;

    aput-object v1, v0, v3

    sget-object v1, Lcom/konka/appassistant/ApplicationInfoView$ViewType;->ZIP_INSTALL_VIEW:Lcom/konka/appassistant/ApplicationInfoView$ViewType;

    aput-object v1, v0, v4

    sput-object v0, Lcom/konka/appassistant/ApplicationInfoView$ViewType;->ENUM$VALUES:[Lcom/konka/appassistant/ApplicationInfoView$ViewType;

    return-void
.end method

.method private constructor <init>(Ljava/lang/String;I)V
    .locals 0

    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    return-void
.end method

.method public static valueOf(Ljava/lang/String;)Lcom/konka/appassistant/ApplicationInfoView$ViewType;
    .locals 1

    const-class v0, Lcom/konka/appassistant/ApplicationInfoView$ViewType;

    invoke-static {v0, p0}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;

    move-result-object v0

    check-cast v0, Lcom/konka/appassistant/ApplicationInfoView$ViewType;

    return-object v0
.end method

.method public static values()[Lcom/konka/appassistant/ApplicationInfoView$ViewType;
    .locals 4

    const/4 v3, 0x0

    sget-object v0, Lcom/konka/appassistant/ApplicationInfoView$ViewType;->ENUM$VALUES:[Lcom/konka/appassistant/ApplicationInfoView$ViewType;

    array-length v1, v0

    new-array v2, v1, [Lcom/konka/appassistant/ApplicationInfoView$ViewType;

    invoke-static {v0, v3, v2, v3, v1}, Ljava/lang/System;->arraycopy(Ljava/lang/Object;ILjava/lang/Object;II)V

    return-object v2
.end method
