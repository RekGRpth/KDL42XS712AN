.class Lcom/konka/appassistant/OneKeyInstallProcess$1;
.super Ljava/lang/Object;
.source "OneKeyInstallProcess.java"

# interfaces
.implements Ljava/lang/Runnable;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/konka/appassistant/OneKeyInstallProcess;->startOneKeyInstallThread()V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/konka/appassistant/OneKeyInstallProcess;


# direct methods
.method constructor <init>(Lcom/konka/appassistant/OneKeyInstallProcess;)V
    .locals 0

    iput-object p1, p0, Lcom/konka/appassistant/OneKeyInstallProcess$1;->this$0:Lcom/konka/appassistant/OneKeyInstallProcess;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public run()V
    .locals 4

    const/4 v3, 0x3

    new-instance v0, Lcom/konka/appassistant/OneKeyInstallProcess$InstallStateDetail;

    invoke-direct {v0}, Lcom/konka/appassistant/OneKeyInstallProcess$InstallStateDetail;-><init>()V

    # getter for: Lcom/konka/appassistant/OneKeyInstallProcess;->DEBUG:Z
    invoke-static {}, Lcom/konka/appassistant/OneKeyInstallProcess;->access$0()Z

    move-result v1

    if-eqz v1, :cond_0

    # getter for: Lcom/konka/appassistant/OneKeyInstallProcess;->TAG:Ljava/lang/String;
    invoke-static {}, Lcom/konka/appassistant/OneKeyInstallProcess;->access$1()Ljava/lang/String;

    move-result-object v1

    const-string v2, "startOneKeyInstallThread():"

    invoke-static {v1, v2}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    :cond_0
    const/4 v1, 0x2

    iput v1, v0, Lcom/konka/appassistant/OneKeyInstallProcess$InstallStateDetail;->mCurProcessState:I

    iget-object v1, p0, Lcom/konka/appassistant/OneKeyInstallProcess$1;->this$0:Lcom/konka/appassistant/OneKeyInstallProcess;

    # invokes: Lcom/konka/appassistant/OneKeyInstallProcess;->notifyListenser(Lcom/konka/appassistant/OneKeyInstallProcess$InstallStateDetail;)V
    invoke-static {v1, v0}, Lcom/konka/appassistant/OneKeyInstallProcess;->access$2(Lcom/konka/appassistant/OneKeyInstallProcess;Lcom/konka/appassistant/OneKeyInstallProcess$InstallStateDetail;)V

    iget-object v1, p0, Lcom/konka/appassistant/OneKeyInstallProcess$1;->this$0:Lcom/konka/appassistant/OneKeyInstallProcess;

    iget-object v1, v1, Lcom/konka/appassistant/OneKeyInstallProcess;->mKkApkUnzip:Lcom/konka/appassistant/KKApkUnzip;

    invoke-virtual {v1}, Lcom/konka/appassistant/KKApkUnzip;->kkapkFileExists()Z

    move-result v1

    if-nez v1, :cond_1

    new-instance v0, Lcom/konka/appassistant/OneKeyInstallProcess$InstallStateDetail;

    invoke-direct {v0}, Lcom/konka/appassistant/OneKeyInstallProcess$InstallStateDetail;-><init>()V

    iput v3, v0, Lcom/konka/appassistant/OneKeyInstallProcess$InstallStateDetail;->mCurProcessState:I

    const/4 v1, 0x1

    iput v1, v0, Lcom/konka/appassistant/OneKeyInstallProcess$InstallStateDetail;->mErrorCode:I

    iget-object v1, p0, Lcom/konka/appassistant/OneKeyInstallProcess$1;->this$0:Lcom/konka/appassistant/OneKeyInstallProcess;

    # invokes: Lcom/konka/appassistant/OneKeyInstallProcess;->notifyListenser(Lcom/konka/appassistant/OneKeyInstallProcess$InstallStateDetail;)V
    invoke-static {v1, v0}, Lcom/konka/appassistant/OneKeyInstallProcess;->access$2(Lcom/konka/appassistant/OneKeyInstallProcess;Lcom/konka/appassistant/OneKeyInstallProcess$InstallStateDetail;)V

    iget-object v1, p0, Lcom/konka/appassistant/OneKeyInstallProcess$1;->this$0:Lcom/konka/appassistant/OneKeyInstallProcess;

    # invokes: Lcom/konka/appassistant/OneKeyInstallProcess;->oneKeyInstallFinished()V
    invoke-static {v1}, Lcom/konka/appassistant/OneKeyInstallProcess;->access$3(Lcom/konka/appassistant/OneKeyInstallProcess;)V

    :cond_1
    iget-object v1, p0, Lcom/konka/appassistant/OneKeyInstallProcess$1;->this$0:Lcom/konka/appassistant/OneKeyInstallProcess;

    iget-boolean v1, v1, Lcom/konka/appassistant/OneKeyInstallProcess;->mInstallIsCanceled:Z

    if-nez v1, :cond_2

    iget-object v1, p0, Lcom/konka/appassistant/OneKeyInstallProcess$1;->this$0:Lcom/konka/appassistant/OneKeyInstallProcess;

    iget-object v2, p0, Lcom/konka/appassistant/OneKeyInstallProcess$1;->this$0:Lcom/konka/appassistant/OneKeyInstallProcess;

    iget-object v2, v2, Lcom/konka/appassistant/OneKeyInstallProcess;->mKkApkUnzip:Lcom/konka/appassistant/KKApkUnzip;

    invoke-virtual {v2}, Lcom/konka/appassistant/KKApkUnzip;->unzipKkapkFiles()Ljava/util/ArrayList;

    move-result-object v2

    iput-object v2, v1, Lcom/konka/appassistant/OneKeyInstallProcess;->mAllApkFileList:Ljava/util/ArrayList;

    :cond_2
    iget-object v1, p0, Lcom/konka/appassistant/OneKeyInstallProcess$1;->this$0:Lcom/konka/appassistant/OneKeyInstallProcess;

    iget-object v1, v1, Lcom/konka/appassistant/OneKeyInstallProcess;->mAllApkFileList:Ljava/util/ArrayList;

    if-nez v1, :cond_3

    new-instance v0, Lcom/konka/appassistant/OneKeyInstallProcess$InstallStateDetail;

    invoke-direct {v0}, Lcom/konka/appassistant/OneKeyInstallProcess$InstallStateDetail;-><init>()V

    iput v3, v0, Lcom/konka/appassistant/OneKeyInstallProcess$InstallStateDetail;->mCurProcessState:I

    iget-object v1, p0, Lcom/konka/appassistant/OneKeyInstallProcess$1;->this$0:Lcom/konka/appassistant/OneKeyInstallProcess;

    iget-object v1, v1, Lcom/konka/appassistant/OneKeyInstallProcess;->mKkApkUnzip:Lcom/konka/appassistant/KKApkUnzip;

    invoke-virtual {v1}, Lcom/konka/appassistant/KKApkUnzip;->getErrorCode()I

    move-result v1

    iput v1, v0, Lcom/konka/appassistant/OneKeyInstallProcess$InstallStateDetail;->mErrorCode:I

    iget-object v1, p0, Lcom/konka/appassistant/OneKeyInstallProcess$1;->this$0:Lcom/konka/appassistant/OneKeyInstallProcess;

    # invokes: Lcom/konka/appassistant/OneKeyInstallProcess;->notifyListenser(Lcom/konka/appassistant/OneKeyInstallProcess$InstallStateDetail;)V
    invoke-static {v1, v0}, Lcom/konka/appassistant/OneKeyInstallProcess;->access$2(Lcom/konka/appassistant/OneKeyInstallProcess;Lcom/konka/appassistant/OneKeyInstallProcess$InstallStateDetail;)V

    iget-object v1, p0, Lcom/konka/appassistant/OneKeyInstallProcess$1;->this$0:Lcom/konka/appassistant/OneKeyInstallProcess;

    # invokes: Lcom/konka/appassistant/OneKeyInstallProcess;->oneKeyInstallFinished()V
    invoke-static {v1}, Lcom/konka/appassistant/OneKeyInstallProcess;->access$3(Lcom/konka/appassistant/OneKeyInstallProcess;)V

    :goto_0
    return-void

    :cond_3
    new-instance v0, Lcom/konka/appassistant/OneKeyInstallProcess$InstallStateDetail;

    invoke-direct {v0}, Lcom/konka/appassistant/OneKeyInstallProcess$InstallStateDetail;-><init>()V

    const/4 v1, 0x4

    iput v1, v0, Lcom/konka/appassistant/OneKeyInstallProcess$InstallStateDetail;->mCurProcessState:I

    iget-object v1, p0, Lcom/konka/appassistant/OneKeyInstallProcess$1;->this$0:Lcom/konka/appassistant/OneKeyInstallProcess;

    # invokes: Lcom/konka/appassistant/OneKeyInstallProcess;->notifyListenser(Lcom/konka/appassistant/OneKeyInstallProcess$InstallStateDetail;)V
    invoke-static {v1, v0}, Lcom/konka/appassistant/OneKeyInstallProcess;->access$2(Lcom/konka/appassistant/OneKeyInstallProcess;Lcom/konka/appassistant/OneKeyInstallProcess$InstallStateDetail;)V

    iget-object v1, p0, Lcom/konka/appassistant/OneKeyInstallProcess$1;->this$0:Lcom/konka/appassistant/OneKeyInstallProcess;

    iget-boolean v1, v1, Lcom/konka/appassistant/OneKeyInstallProcess;->mInstallIsCanceled:Z

    if-nez v1, :cond_4

    iget-object v1, p0, Lcom/konka/appassistant/OneKeyInstallProcess$1;->this$0:Lcom/konka/appassistant/OneKeyInstallProcess;

    # invokes: Lcom/konka/appassistant/OneKeyInstallProcess;->beginInstallApks()V
    invoke-static {v1}, Lcom/konka/appassistant/OneKeyInstallProcess;->access$4(Lcom/konka/appassistant/OneKeyInstallProcess;)V

    goto :goto_0

    :cond_4
    iget-object v1, p0, Lcom/konka/appassistant/OneKeyInstallProcess$1;->this$0:Lcom/konka/appassistant/OneKeyInstallProcess;

    iget-object v1, v1, Lcom/konka/appassistant/OneKeyInstallProcess;->mKkApkUnzip:Lcom/konka/appassistant/KKApkUnzip;

    invoke-virtual {v1}, Lcom/konka/appassistant/KKApkUnzip;->clearUnzipApks()V

    iget-object v1, p0, Lcom/konka/appassistant/OneKeyInstallProcess$1;->this$0:Lcom/konka/appassistant/OneKeyInstallProcess;

    # invokes: Lcom/konka/appassistant/OneKeyInstallProcess;->oneKeyInstallFinished()V
    invoke-static {v1}, Lcom/konka/appassistant/OneKeyInstallProcess;->access$3(Lcom/konka/appassistant/OneKeyInstallProcess;)V

    goto :goto_0
.end method
