.class public Lcom/konka/help/ImageLoader;
.super Landroid/os/AsyncTask;
.source "ImageLoader.java"


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Landroid/os/AsyncTask",
        "<",
        "Ljava/lang/Void;",
        "Ljava/lang/Void;",
        "Ljava/lang/Void;",
        ">;"
    }
.end annotation


# instance fields
.field private activity:Landroid/app/Activity;

.field private bitmap:Landroid/graphics/Bitmap;

.field private index:I

.field private photoDrawableId:[I

.field private photoSwitcher:Landroid/widget/ImageSwitcher;


# direct methods
.method constructor <init>(Landroid/app/Activity;Landroid/widget/ImageSwitcher;[II)V
    .locals 0
    .param p1    # Landroid/app/Activity;
    .param p2    # Landroid/widget/ImageSwitcher;
    .param p3    # [I
    .param p4    # I

    invoke-direct {p0}, Landroid/os/AsyncTask;-><init>()V

    iput-object p1, p0, Lcom/konka/help/ImageLoader;->activity:Landroid/app/Activity;

    iput-object p2, p0, Lcom/konka/help/ImageLoader;->photoSwitcher:Landroid/widget/ImageSwitcher;

    iput-object p3, p0, Lcom/konka/help/ImageLoader;->photoDrawableId:[I

    iput p4, p0, Lcom/konka/help/ImageLoader;->index:I

    return-void
.end method


# virtual methods
.method protected bridge varargs synthetic doInBackground([Ljava/lang/Object;)Ljava/lang/Object;
    .locals 1

    check-cast p1, [Ljava/lang/Void;

    invoke-virtual {p0, p1}, Lcom/konka/help/ImageLoader;->doInBackground([Ljava/lang/Void;)Ljava/lang/Void;

    move-result-object v0

    return-object v0
.end method

.method protected varargs doInBackground([Ljava/lang/Void;)Ljava/lang/Void;
    .locals 3
    .param p1    # [Ljava/lang/Void;

    iget-object v0, p0, Lcom/konka/help/ImageLoader;->activity:Landroid/app/Activity;

    invoke-virtual {v0}, Landroid/app/Activity;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    iget-object v1, p0, Lcom/konka/help/ImageLoader;->photoDrawableId:[I

    iget v2, p0, Lcom/konka/help/ImageLoader;->index:I

    aget v1, v1, v2

    invoke-static {v0, v1}, Landroid/graphics/BitmapFactory;->decodeResource(Landroid/content/res/Resources;I)Landroid/graphics/Bitmap;

    move-result-object v0

    iput-object v0, p0, Lcom/konka/help/ImageLoader;->bitmap:Landroid/graphics/Bitmap;

    const/4 v0, 0x0

    new-array v0, v0, [Ljava/lang/Void;

    invoke-virtual {p0, v0}, Lcom/konka/help/ImageLoader;->publishProgress([Ljava/lang/Object;)V

    const/4 v0, 0x0

    return-object v0
.end method

.method public bridge varargs synthetic onProgressUpdate([Ljava/lang/Object;)V
    .locals 0

    check-cast p1, [Ljava/lang/Void;

    invoke-virtual {p0, p1}, Lcom/konka/help/ImageLoader;->onProgressUpdate([Ljava/lang/Void;)V

    return-void
.end method

.method public varargs onProgressUpdate([Ljava/lang/Void;)V
    .locals 3
    .param p1    # [Ljava/lang/Void;

    new-instance v0, Landroid/graphics/drawable/BitmapDrawable;

    iget-object v1, p0, Lcom/konka/help/ImageLoader;->activity:Landroid/app/Activity;

    invoke-virtual {v1}, Landroid/app/Activity;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    iget-object v2, p0, Lcom/konka/help/ImageLoader;->bitmap:Landroid/graphics/Bitmap;

    invoke-direct {v0, v1, v2}, Landroid/graphics/drawable/BitmapDrawable;-><init>(Landroid/content/res/Resources;Landroid/graphics/Bitmap;)V

    iget-object v1, p0, Lcom/konka/help/ImageLoader;->photoSwitcher:Landroid/widget/ImageSwitcher;

    invoke-virtual {v1, v0}, Landroid/widget/ImageSwitcher;->setImageDrawable(Landroid/graphics/drawable/Drawable;)V

    return-void
.end method
