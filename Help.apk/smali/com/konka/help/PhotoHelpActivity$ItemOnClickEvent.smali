.class Lcom/konka/help/PhotoHelpActivity$ItemOnClickEvent;
.super Ljava/lang/Object;
.source "PhotoHelpActivity.java"

# interfaces
.implements Landroid/view/View$OnClickListener;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/konka/help/PhotoHelpActivity;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = "ItemOnClickEvent"
.end annotation


# instance fields
.field final synthetic this$0:Lcom/konka/help/PhotoHelpActivity;


# direct methods
.method constructor <init>(Lcom/konka/help/PhotoHelpActivity;)V
    .locals 0

    iput-object p1, p0, Lcom/konka/help/PhotoHelpActivity$ItemOnClickEvent;->this$0:Lcom/konka/help/PhotoHelpActivity;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onClick(Landroid/view/View;)V
    .locals 5
    .param p1    # Landroid/view/View;

    const/16 v4, 0x11

    const/16 v3, 0x8

    const/4 v2, 0x0

    const/4 v1, 0x1

    invoke-virtual {p1}, Landroid/view/View;->getId()I

    move-result v0

    packed-switch v0, :pswitch_data_0

    :goto_0
    return-void

    :pswitch_0
    iget-object v0, p0, Lcom/konka/help/PhotoHelpActivity$ItemOnClickEvent;->this$0:Lcom/konka/help/PhotoHelpActivity;

    const/4 v1, -0x1

    invoke-virtual {v0, v1}, Lcom/konka/help/PhotoHelpActivity;->moveNextOrPrevious(I)V

    goto :goto_0

    :pswitch_1
    iget-object v0, p0, Lcom/konka/help/PhotoHelpActivity$ItemOnClickEvent;->this$0:Lcom/konka/help/PhotoHelpActivity;

    invoke-static {v0, v1}, Lcom/konka/help/PhotoHelpActivity;->access$1(Lcom/konka/help/PhotoHelpActivity;Z)V

    iget-object v0, p0, Lcom/konka/help/PhotoHelpActivity$ItemOnClickEvent;->this$0:Lcom/konka/help/PhotoHelpActivity;

    iget-object v0, v0, Lcom/konka/help/PhotoHelpActivity;->handler:Landroid/os/Handler;

    invoke-virtual {v0, v1}, Landroid/os/Handler;->removeMessages(I)V

    iget-object v0, p0, Lcom/konka/help/PhotoHelpActivity$ItemOnClickEvent;->this$0:Lcom/konka/help/PhotoHelpActivity;

    iget-object v0, v0, Lcom/konka/help/PhotoHelpActivity;->handler:Landroid/os/Handler;

    invoke-virtual {v0, v1}, Landroid/os/Handler;->sendEmptyMessage(I)Z

    iget-object v0, p0, Lcom/konka/help/PhotoHelpActivity$ItemOnClickEvent;->this$0:Lcom/konka/help/PhotoHelpActivity;

    iget-object v0, v0, Lcom/konka/help/PhotoHelpActivity;->btn_play:Landroid/widget/Button;

    invoke-virtual {v0, v3}, Landroid/widget/Button;->setVisibility(I)V

    iget-object v0, p0, Lcom/konka/help/PhotoHelpActivity$ItemOnClickEvent;->this$0:Lcom/konka/help/PhotoHelpActivity;

    iget-object v0, v0, Lcom/konka/help/PhotoHelpActivity;->btn_pause:Landroid/widget/Button;

    invoke-virtual {v0, v2}, Landroid/widget/Button;->setVisibility(I)V

    iget-object v0, p0, Lcom/konka/help/PhotoHelpActivity$ItemOnClickEvent;->this$0:Lcom/konka/help/PhotoHelpActivity;

    iget-object v0, v0, Lcom/konka/help/PhotoHelpActivity;->btn_pause:Landroid/widget/Button;

    invoke-virtual {v0, v1}, Landroid/widget/Button;->setFocusable(Z)V

    iget-object v0, p0, Lcom/konka/help/PhotoHelpActivity$ItemOnClickEvent;->this$0:Lcom/konka/help/PhotoHelpActivity;

    iget-object v0, v0, Lcom/konka/help/PhotoHelpActivity;->btn_pause:Landroid/widget/Button;

    invoke-virtual {v0}, Landroid/widget/Button;->requestFocus()Z

    iget-object v0, p0, Lcom/konka/help/PhotoHelpActivity$ItemOnClickEvent;->this$0:Lcom/konka/help/PhotoHelpActivity;

    iget-object v0, v0, Lcom/konka/help/PhotoHelpActivity;->handler:Landroid/os/Handler;

    invoke-virtual {v0, v4}, Landroid/os/Handler;->removeMessages(I)V

    iget-object v0, p0, Lcom/konka/help/PhotoHelpActivity$ItemOnClickEvent;->this$0:Lcom/konka/help/PhotoHelpActivity;

    invoke-virtual {v0}, Lcom/konka/help/PhotoHelpActivity;->hideControlDelay()V

    goto :goto_0

    :pswitch_2
    iget-object v0, p0, Lcom/konka/help/PhotoHelpActivity$ItemOnClickEvent;->this$0:Lcom/konka/help/PhotoHelpActivity;

    invoke-static {v0, v2}, Lcom/konka/help/PhotoHelpActivity;->access$1(Lcom/konka/help/PhotoHelpActivity;Z)V

    iget-object v0, p0, Lcom/konka/help/PhotoHelpActivity$ItemOnClickEvent;->this$0:Lcom/konka/help/PhotoHelpActivity;

    iget-object v0, v0, Lcom/konka/help/PhotoHelpActivity;->handler:Landroid/os/Handler;

    invoke-virtual {v0, v1}, Landroid/os/Handler;->removeMessages(I)V

    iget-object v0, p0, Lcom/konka/help/PhotoHelpActivity$ItemOnClickEvent;->this$0:Lcom/konka/help/PhotoHelpActivity;

    iget-object v0, v0, Lcom/konka/help/PhotoHelpActivity;->btn_pause:Landroid/widget/Button;

    invoke-virtual {v0, v3}, Landroid/widget/Button;->setVisibility(I)V

    iget-object v0, p0, Lcom/konka/help/PhotoHelpActivity$ItemOnClickEvent;->this$0:Lcom/konka/help/PhotoHelpActivity;

    iget-object v0, v0, Lcom/konka/help/PhotoHelpActivity;->btn_play:Landroid/widget/Button;

    invoke-virtual {v0, v2}, Landroid/widget/Button;->setVisibility(I)V

    iget-object v0, p0, Lcom/konka/help/PhotoHelpActivity$ItemOnClickEvent;->this$0:Lcom/konka/help/PhotoHelpActivity;

    iget-object v0, v0, Lcom/konka/help/PhotoHelpActivity;->btn_play:Landroid/widget/Button;

    invoke-virtual {v0, v1}, Landroid/widget/Button;->setFocusable(Z)V

    iget-object v0, p0, Lcom/konka/help/PhotoHelpActivity$ItemOnClickEvent;->this$0:Lcom/konka/help/PhotoHelpActivity;

    iget-object v0, v0, Lcom/konka/help/PhotoHelpActivity;->btn_play:Landroid/widget/Button;

    invoke-virtual {v0}, Landroid/widget/Button;->requestFocus()Z

    iget-object v0, p0, Lcom/konka/help/PhotoHelpActivity$ItemOnClickEvent;->this$0:Lcom/konka/help/PhotoHelpActivity;

    iget-object v0, v0, Lcom/konka/help/PhotoHelpActivity;->handler:Landroid/os/Handler;

    invoke-virtual {v0, v4}, Landroid/os/Handler;->removeMessages(I)V

    goto :goto_0

    :pswitch_3
    iget-object v0, p0, Lcom/konka/help/PhotoHelpActivity$ItemOnClickEvent;->this$0:Lcom/konka/help/PhotoHelpActivity;

    invoke-virtual {v0, v1}, Lcom/konka/help/PhotoHelpActivity;->moveNextOrPrevious(I)V

    goto :goto_0

    nop

    :pswitch_data_0
    .packed-switch 0x7f070003
        :pswitch_0    # com.konka.help.R.id.btn_back
        :pswitch_1    # com.konka.help.R.id.btn_play
        :pswitch_2    # com.konka.help.R.id.btn_pause
        :pswitch_3    # com.konka.help.R.id.btn_next
    .end packed-switch
.end method
