.class public Lcom/konka/help/PhotoHelpActivity;
.super Landroid/app/Activity;
.source "PhotoHelpActivity.java"

# interfaces
.implements Landroid/widget/ViewSwitcher$ViewFactory;


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/konka/help/PhotoHelpActivity$ItemOnClickEvent;
    }
.end annotation


# static fields
.field private static final DEFAULT_TIMEOUT:I = 0x3a98

.field public static final HIDE_PLAYER_CONTROL:I = 0x11

.field public static final HIDE_STATE_DRAWABLE:I = 0x12

.field protected static photoDrawableId:[I


# instance fields
.field protected final PPT_PLAYER:I

.field protected btn_back:Landroid/widget/Button;

.field protected btn_next:Landroid/widget/Button;

.field protected btn_pause:Landroid/widget/Button;

.field protected btn_play:Landroid/widget/Button;

.field protected control_layout:Landroid/widget/RelativeLayout;

.field private currentPhotoNum:I

.field protected handler:Landroid/os/Handler;

.field private isAutoPlaying:Z

.field private isControllerShow:Z

.field protected mAlphaIn:Landroid/view/animation/Animation;

.field protected mAlphaOut:Landroid/view/animation/Animation;

.field protected mPicControlInAnim:Landroid/view/animation/Animation;

.field protected mPicControlOutAnim:Landroid/view/animation/Animation;

.field photoSwitcher:Landroid/widget/ImageSwitcher;

.field protected playStateDrawable:[I

.field protected stateDrawable:Landroid/widget/ImageView;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    const/16 v0, 0xd

    new-array v0, v0, [I

    fill-array-data v0, :array_0

    sput-object v0, Lcom/konka/help/PhotoHelpActivity;->photoDrawableId:[I

    return-void

    :array_0
    .array-data 4
        0x7f020018    # com.konka.help.R.drawable.menu
        0x7f02000b    # com.konka.help.R.drawable.help1
        0x7f02000f    # com.konka.help.R.drawable.help2
        0x7f020010    # com.konka.help.R.drawable.help3
        0x7f020011    # com.konka.help.R.drawable.help4
        0x7f020012    # com.konka.help.R.drawable.help5
        0x7f020013    # com.konka.help.R.drawable.help6
        0x7f020014    # com.konka.help.R.drawable.help7
        0x7f020015    # com.konka.help.R.drawable.help8
        0x7f020016    # com.konka.help.R.drawable.help9
        0x7f02000c    # com.konka.help.R.drawable.help10
        0x7f02000d    # com.konka.help.R.drawable.help11
        0x7f02000e    # com.konka.help.R.drawable.help12
    .end array-data
.end method

.method public constructor <init>()V
    .locals 2

    const/4 v1, 0x0

    invoke-direct {p0}, Landroid/app/Activity;-><init>()V

    const/4 v0, 0x1

    iput v0, p0, Lcom/konka/help/PhotoHelpActivity;->PPT_PLAYER:I

    iput-boolean v1, p0, Lcom/konka/help/PhotoHelpActivity;->isAutoPlaying:Z

    iput-boolean v1, p0, Lcom/konka/help/PhotoHelpActivity;->isControllerShow:Z

    iput v1, p0, Lcom/konka/help/PhotoHelpActivity;->currentPhotoNum:I

    const/4 v0, 0x4

    new-array v0, v0, [I

    fill-array-data v0, :array_0

    iput-object v0, p0, Lcom/konka/help/PhotoHelpActivity;->playStateDrawable:[I

    new-instance v0, Lcom/konka/help/PhotoHelpActivity$1;

    invoke-direct {v0, p0}, Lcom/konka/help/PhotoHelpActivity$1;-><init>(Lcom/konka/help/PhotoHelpActivity;)V

    iput-object v0, p0, Lcom/konka/help/PhotoHelpActivity;->handler:Landroid/os/Handler;

    return-void

    nop

    :array_0
    .array-data 4
        0x7f02001c    # com.konka.help.R.drawable.playstate1
        0x7f02001d    # com.konka.help.R.drawable.playstate2
        0x7f02001e    # com.konka.help.R.drawable.playstate3
        0x7f02001f    # com.konka.help.R.drawable.playstate4
    .end array-data
.end method

.method static synthetic access$0(Lcom/konka/help/PhotoHelpActivity;)Z
    .locals 1

    iget-boolean v0, p0, Lcom/konka/help/PhotoHelpActivity;->isAutoPlaying:Z

    return v0
.end method

.method static synthetic access$1(Lcom/konka/help/PhotoHelpActivity;Z)V
    .locals 0

    iput-boolean p1, p0, Lcom/konka/help/PhotoHelpActivity;->isAutoPlaying:Z

    return-void
.end method

.method private showController()V
    .locals 2

    iget-object v0, p0, Lcom/konka/help/PhotoHelpActivity;->control_layout:Landroid/widget/RelativeLayout;

    iget-object v1, p0, Lcom/konka/help/PhotoHelpActivity;->mPicControlInAnim:Landroid/view/animation/Animation;

    invoke-virtual {v0, v1}, Landroid/widget/RelativeLayout;->startAnimation(Landroid/view/animation/Animation;)V

    iget-object v0, p0, Lcom/konka/help/PhotoHelpActivity;->control_layout:Landroid/widget/RelativeLayout;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Landroid/widget/RelativeLayout;->setVisibility(I)V

    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/konka/help/PhotoHelpActivity;->isControllerShow:Z

    return-void
.end method


# virtual methods
.method public findView()V
    .locals 2

    const/high16 v0, 0x7f070000    # com.konka.help.R.id.photo_switcher

    invoke-virtual {p0, v0}, Lcom/konka/help/PhotoHelpActivity;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/ImageSwitcher;

    iput-object v0, p0, Lcom/konka/help/PhotoHelpActivity;->photoSwitcher:Landroid/widget/ImageSwitcher;

    const v0, 0x7f070002    # com.konka.help.R.id.control

    invoke-virtual {p0, v0}, Lcom/konka/help/PhotoHelpActivity;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/RelativeLayout;

    iput-object v0, p0, Lcom/konka/help/PhotoHelpActivity;->control_layout:Landroid/widget/RelativeLayout;

    const v0, 0x7f070003    # com.konka.help.R.id.btn_back

    invoke-virtual {p0, v0}, Lcom/konka/help/PhotoHelpActivity;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/Button;

    iput-object v0, p0, Lcom/konka/help/PhotoHelpActivity;->btn_back:Landroid/widget/Button;

    const v0, 0x7f070004    # com.konka.help.R.id.btn_play

    invoke-virtual {p0, v0}, Lcom/konka/help/PhotoHelpActivity;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/Button;

    iput-object v0, p0, Lcom/konka/help/PhotoHelpActivity;->btn_play:Landroid/widget/Button;

    const v0, 0x7f070006    # com.konka.help.R.id.btn_next

    invoke-virtual {p0, v0}, Lcom/konka/help/PhotoHelpActivity;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/Button;

    iput-object v0, p0, Lcom/konka/help/PhotoHelpActivity;->btn_next:Landroid/widget/Button;

    const v0, 0x7f070005    # com.konka.help.R.id.btn_pause

    invoke-virtual {p0, v0}, Lcom/konka/help/PhotoHelpActivity;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/Button;

    iput-object v0, p0, Lcom/konka/help/PhotoHelpActivity;->btn_pause:Landroid/widget/Button;

    const v0, 0x7f070001    # com.konka.help.R.id.state_image

    invoke-virtual {p0, v0}, Lcom/konka/help/PhotoHelpActivity;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/ImageView;

    iput-object v0, p0, Lcom/konka/help/PhotoHelpActivity;->stateDrawable:Landroid/widget/ImageView;

    iget-object v0, p0, Lcom/konka/help/PhotoHelpActivity;->btn_back:Landroid/widget/Button;

    new-instance v1, Lcom/konka/help/PhotoHelpActivity$ItemOnClickEvent;

    invoke-direct {v1, p0}, Lcom/konka/help/PhotoHelpActivity$ItemOnClickEvent;-><init>(Lcom/konka/help/PhotoHelpActivity;)V

    invoke-virtual {v0, v1}, Landroid/widget/Button;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    iget-object v0, p0, Lcom/konka/help/PhotoHelpActivity;->btn_play:Landroid/widget/Button;

    new-instance v1, Lcom/konka/help/PhotoHelpActivity$ItemOnClickEvent;

    invoke-direct {v1, p0}, Lcom/konka/help/PhotoHelpActivity$ItemOnClickEvent;-><init>(Lcom/konka/help/PhotoHelpActivity;)V

    invoke-virtual {v0, v1}, Landroid/widget/Button;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    iget-object v0, p0, Lcom/konka/help/PhotoHelpActivity;->btn_pause:Landroid/widget/Button;

    new-instance v1, Lcom/konka/help/PhotoHelpActivity$ItemOnClickEvent;

    invoke-direct {v1, p0}, Lcom/konka/help/PhotoHelpActivity$ItemOnClickEvent;-><init>(Lcom/konka/help/PhotoHelpActivity;)V

    invoke-virtual {v0, v1}, Landroid/widget/Button;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    iget-object v0, p0, Lcom/konka/help/PhotoHelpActivity;->btn_next:Landroid/widget/Button;

    new-instance v1, Lcom/konka/help/PhotoHelpActivity$ItemOnClickEvent;

    invoke-direct {v1, p0}, Lcom/konka/help/PhotoHelpActivity$ItemOnClickEvent;-><init>(Lcom/konka/help/PhotoHelpActivity;)V

    invoke-virtual {v0, v1}, Landroid/widget/Button;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    const/high16 v0, 0x7f040000    # com.konka.help.R.anim.alpha_in

    invoke-static {p0, v0}, Landroid/view/animation/AnimationUtils;->loadAnimation(Landroid/content/Context;I)Landroid/view/animation/Animation;

    move-result-object v0

    iput-object v0, p0, Lcom/konka/help/PhotoHelpActivity;->mAlphaIn:Landroid/view/animation/Animation;

    const v0, 0x7f040001    # com.konka.help.R.anim.alpha_out

    invoke-static {p0, v0}, Landroid/view/animation/AnimationUtils;->loadAnimation(Landroid/content/Context;I)Landroid/view/animation/Animation;

    move-result-object v0

    iput-object v0, p0, Lcom/konka/help/PhotoHelpActivity;->mAlphaOut:Landroid/view/animation/Animation;

    const v0, 0x7f040002    # com.konka.help.R.anim.pic_control_in

    invoke-static {p0, v0}, Landroid/view/animation/AnimationUtils;->loadAnimation(Landroid/content/Context;I)Landroid/view/animation/Animation;

    move-result-object v0

    iput-object v0, p0, Lcom/konka/help/PhotoHelpActivity;->mPicControlInAnim:Landroid/view/animation/Animation;

    const v0, 0x7f040003    # com.konka.help.R.anim.pic_control_out

    invoke-static {p0, v0}, Landroid/view/animation/AnimationUtils;->loadAnimation(Landroid/content/Context;I)Landroid/view/animation/Animation;

    move-result-object v0

    iput-object v0, p0, Lcom/konka/help/PhotoHelpActivity;->mPicControlOutAnim:Landroid/view/animation/Animation;

    iget-object v0, p0, Lcom/konka/help/PhotoHelpActivity;->photoSwitcher:Landroid/widget/ImageSwitcher;

    iget-object v1, p0, Lcom/konka/help/PhotoHelpActivity;->mAlphaIn:Landroid/view/animation/Animation;

    invoke-virtual {v0, v1}, Landroid/widget/ImageSwitcher;->setInAnimation(Landroid/view/animation/Animation;)V

    iget-object v0, p0, Lcom/konka/help/PhotoHelpActivity;->photoSwitcher:Landroid/widget/ImageSwitcher;

    iget-object v1, p0, Lcom/konka/help/PhotoHelpActivity;->mAlphaOut:Landroid/view/animation/Animation;

    invoke-virtual {v0, v1}, Landroid/widget/ImageSwitcher;->setOutAnimation(Landroid/view/animation/Animation;)V

    return-void
.end method

.method public hideControlDelay()V
    .locals 4

    iget-object v0, p0, Lcom/konka/help/PhotoHelpActivity;->handler:Landroid/os/Handler;

    const/16 v1, 0x11

    const-wide/16 v2, 0x3a98

    invoke-virtual {v0, v1, v2, v3}, Landroid/os/Handler;->sendEmptyMessageDelayed(IJ)Z

    return-void
.end method

.method public hideController()V
    .locals 2

    iget-object v0, p0, Lcom/konka/help/PhotoHelpActivity;->control_layout:Landroid/widget/RelativeLayout;

    iget-object v1, p0, Lcom/konka/help/PhotoHelpActivity;->mPicControlOutAnim:Landroid/view/animation/Animation;

    invoke-virtual {v0, v1}, Landroid/widget/RelativeLayout;->startAnimation(Landroid/view/animation/Animation;)V

    iget-object v0, p0, Lcom/konka/help/PhotoHelpActivity;->control_layout:Landroid/widget/RelativeLayout;

    const/16 v1, 0x8

    invoke-virtual {v0, v1}, Landroid/widget/RelativeLayout;->setVisibility(I)V

    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/konka/help/PhotoHelpActivity;->isControllerShow:Z

    return-void
.end method

.method public makeView()Landroid/view/View;
    .locals 3

    const/4 v2, -0x1

    new-instance v0, Landroid/widget/ImageView;

    invoke-direct {v0, p0}, Landroid/widget/ImageView;-><init>(Landroid/content/Context;)V

    new-instance v1, Landroid/widget/FrameLayout$LayoutParams;

    invoke-direct {v1, v2, v2}, Landroid/widget/FrameLayout$LayoutParams;-><init>(II)V

    invoke-virtual {v0, v1}, Landroid/widget/ImageView;->setLayoutParams(Landroid/view/ViewGroup$LayoutParams;)V

    return-object v0
.end method

.method protected moveNextOrPrevious(I)V
    .locals 5
    .param p1    # I

    const/4 v4, 0x0

    iget v0, p0, Lcom/konka/help/PhotoHelpActivity;->currentPhotoNum:I

    add-int/2addr v0, p1

    iput v0, p0, Lcom/konka/help/PhotoHelpActivity;->currentPhotoNum:I

    iget v0, p0, Lcom/konka/help/PhotoHelpActivity;->currentPhotoNum:I

    const/4 v1, -0x1

    if-gt v0, v1, :cond_1

    sget-object v0, Lcom/konka/help/PhotoHelpActivity;->photoDrawableId:[I

    array-length v0, v0

    add-int/lit8 v0, v0, -0x1

    iput v0, p0, Lcom/konka/help/PhotoHelpActivity;->currentPhotoNum:I

    :cond_0
    :goto_0
    new-instance v0, Lcom/konka/help/ImageLoader;

    iget-object v1, p0, Lcom/konka/help/PhotoHelpActivity;->photoSwitcher:Landroid/widget/ImageSwitcher;

    sget-object v2, Lcom/konka/help/PhotoHelpActivity;->photoDrawableId:[I

    iget v3, p0, Lcom/konka/help/PhotoHelpActivity;->currentPhotoNum:I

    invoke-direct {v0, p0, v1, v2, v3}, Lcom/konka/help/ImageLoader;-><init>(Landroid/app/Activity;Landroid/widget/ImageSwitcher;[II)V

    new-array v1, v4, [Ljava/lang/Void;

    invoke-virtual {v0, v1}, Lcom/konka/help/ImageLoader;->execute([Ljava/lang/Object;)Landroid/os/AsyncTask;

    return-void

    :cond_1
    iget v0, p0, Lcom/konka/help/PhotoHelpActivity;->currentPhotoNum:I

    sget-object v1, Lcom/konka/help/PhotoHelpActivity;->photoDrawableId:[I

    array-length v1, v1

    if-lt v0, v1, :cond_0

    iput v4, p0, Lcom/konka/help/PhotoHelpActivity;->currentPhotoNum:I

    goto :goto_0
.end method

.method public onCreate(Landroid/os/Bundle;)V
    .locals 4
    .param p1    # Landroid/os/Bundle;

    invoke-super {p0, p1}, Landroid/app/Activity;->onCreate(Landroid/os/Bundle;)V

    const/4 v0, 0x1

    invoke-virtual {p0, v0}, Lcom/konka/help/PhotoHelpActivity;->requestWindowFeature(I)Z

    const/high16 v0, 0x7f030000    # com.konka.help.R.layout.main

    invoke-virtual {p0, v0}, Lcom/konka/help/PhotoHelpActivity;->setContentView(I)V

    invoke-virtual {p0}, Lcom/konka/help/PhotoHelpActivity;->findView()V

    iget-object v0, p0, Lcom/konka/help/PhotoHelpActivity;->photoSwitcher:Landroid/widget/ImageSwitcher;

    invoke-virtual {v0, p0}, Landroid/widget/ImageSwitcher;->setFactory(Landroid/widget/ViewSwitcher$ViewFactory;)V

    new-instance v0, Lcom/konka/help/ImageLoader;

    iget-object v1, p0, Lcom/konka/help/PhotoHelpActivity;->photoSwitcher:Landroid/widget/ImageSwitcher;

    sget-object v2, Lcom/konka/help/PhotoHelpActivity;->photoDrawableId:[I

    iget v3, p0, Lcom/konka/help/PhotoHelpActivity;->currentPhotoNum:I

    invoke-direct {v0, p0, v1, v2, v3}, Lcom/konka/help/ImageLoader;-><init>(Landroid/app/Activity;Landroid/widget/ImageSwitcher;[II)V

    const/4 v1, 0x0

    new-array v1, v1, [Ljava/lang/Void;

    invoke-virtual {v0, v1}, Lcom/konka/help/ImageLoader;->execute([Ljava/lang/Object;)Landroid/os/AsyncTask;

    return-void
.end method

.method public onKeyDown(ILandroid/view/KeyEvent;)Z
    .locals 4
    .param p1    # I
    .param p2    # Landroid/view/KeyEvent;

    const/16 v3, 0x8

    const/4 v0, 0x0

    const/4 v1, 0x1

    invoke-virtual {p2}, Landroid/view/KeyEvent;->getKeyCode()I

    move-result v2

    sparse-switch v2, :sswitch_data_0

    :cond_0
    :goto_0
    invoke-super {p0, p1, p2}, Landroid/app/Activity;->onKeyDown(ILandroid/view/KeyEvent;)Z

    move-result v0

    return v0

    :sswitch_0
    iget-boolean v0, p0, Lcom/konka/help/PhotoHelpActivity;->isControllerShow:Z

    if-nez v0, :cond_0

    const/4 v0, -0x1

    invoke-virtual {p0, v0}, Lcom/konka/help/PhotoHelpActivity;->moveNextOrPrevious(I)V

    goto :goto_0

    :sswitch_1
    iget-boolean v0, p0, Lcom/konka/help/PhotoHelpActivity;->isControllerShow:Z

    if-nez v0, :cond_0

    invoke-virtual {p0, v1}, Lcom/konka/help/PhotoHelpActivity;->moveNextOrPrevious(I)V

    goto :goto_0

    :sswitch_2
    iget-boolean v2, p0, Lcom/konka/help/PhotoHelpActivity;->isAutoPlaying:Z

    if-eqz v2, :cond_1

    :goto_1
    iput-boolean v0, p0, Lcom/konka/help/PhotoHelpActivity;->isAutoPlaying:Z

    iget-boolean v0, p0, Lcom/konka/help/PhotoHelpActivity;->isAutoPlaying:Z

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/konka/help/PhotoHelpActivity;->handler:Landroid/os/Handler;

    invoke-virtual {v0, v1}, Landroid/os/Handler;->removeMessages(I)V

    iget-object v0, p0, Lcom/konka/help/PhotoHelpActivity;->handler:Landroid/os/Handler;

    invoke-virtual {v0, v1}, Landroid/os/Handler;->sendEmptyMessage(I)Z

    goto :goto_0

    :cond_1
    move v0, v1

    goto :goto_1

    :sswitch_3
    iget-boolean v2, p0, Lcom/konka/help/PhotoHelpActivity;->isControllerShow:Z

    if-eqz v2, :cond_2

    iget-object v0, p0, Lcom/konka/help/PhotoHelpActivity;->handler:Landroid/os/Handler;

    const/16 v1, 0x11

    invoke-virtual {v0, v1}, Landroid/os/Handler;->removeMessages(I)V

    invoke-virtual {p0}, Lcom/konka/help/PhotoHelpActivity;->hideController()V

    :goto_2
    iget-boolean v0, p0, Lcom/konka/help/PhotoHelpActivity;->isControllerShow:Z

    if-eqz v0, :cond_0

    invoke-virtual {p0}, Lcom/konka/help/PhotoHelpActivity;->hideControlDelay()V

    goto :goto_0

    :cond_2
    invoke-direct {p0}, Lcom/konka/help/PhotoHelpActivity;->showController()V

    iget-boolean v2, p0, Lcom/konka/help/PhotoHelpActivity;->isAutoPlaying:Z

    if-eqz v2, :cond_3

    iget-object v2, p0, Lcom/konka/help/PhotoHelpActivity;->btn_play:Landroid/widget/Button;

    invoke-virtual {v2, v3}, Landroid/widget/Button;->setVisibility(I)V

    iget-object v2, p0, Lcom/konka/help/PhotoHelpActivity;->btn_pause:Landroid/widget/Button;

    invoke-virtual {v2, v0}, Landroid/widget/Button;->setVisibility(I)V

    iget-object v0, p0, Lcom/konka/help/PhotoHelpActivity;->btn_pause:Landroid/widget/Button;

    invoke-virtual {v0, v1}, Landroid/widget/Button;->setFocusable(Z)V

    iget-object v0, p0, Lcom/konka/help/PhotoHelpActivity;->btn_pause:Landroid/widget/Button;

    invoke-virtual {v0}, Landroid/widget/Button;->requestFocus()Z

    goto :goto_2

    :cond_3
    iget-object v2, p0, Lcom/konka/help/PhotoHelpActivity;->btn_pause:Landroid/widget/Button;

    invoke-virtual {v2, v3}, Landroid/widget/Button;->setVisibility(I)V

    iget-object v2, p0, Lcom/konka/help/PhotoHelpActivity;->btn_play:Landroid/widget/Button;

    invoke-virtual {v2, v0}, Landroid/widget/Button;->setVisibility(I)V

    iget-object v0, p0, Lcom/konka/help/PhotoHelpActivity;->btn_play:Landroid/widget/Button;

    invoke-virtual {v0, v1}, Landroid/widget/Button;->setFocusable(Z)V

    iget-object v0, p0, Lcom/konka/help/PhotoHelpActivity;->btn_play:Landroid/widget/Button;

    invoke-virtual {v0}, Landroid/widget/Button;->requestFocus()Z

    goto :goto_2

    nop

    :sswitch_data_0
    .sparse-switch
        0x15 -> :sswitch_0
        0x16 -> :sswitch_1
        0x42 -> :sswitch_2
        0x52 -> :sswitch_3
    .end sparse-switch
.end method

.method public setStateDrawable(I)V
    .locals 4
    .param p1    # I

    const/16 v3, 0x12

    iget-boolean v0, p0, Lcom/konka/help/PhotoHelpActivity;->isControllerShow:Z

    if-nez v0, :cond_0

    iget-boolean v0, p0, Lcom/konka/help/PhotoHelpActivity;->isAutoPlaying:Z

    if-eqz v0, :cond_1

    iget-object v0, p0, Lcom/konka/help/PhotoHelpActivity;->stateDrawable:Landroid/widget/ImageView;

    invoke-virtual {v0, p1}, Landroid/widget/ImageView;->setImageResource(I)V

    :cond_0
    :goto_0
    return-void

    :cond_1
    iget-object v0, p0, Lcom/konka/help/PhotoHelpActivity;->stateDrawable:Landroid/widget/ImageView;

    invoke-virtual {v0, p1}, Landroid/widget/ImageView;->setImageResource(I)V

    iget-object v0, p0, Lcom/konka/help/PhotoHelpActivity;->handler:Landroid/os/Handler;

    invoke-virtual {v0, v3}, Landroid/os/Handler;->removeMessages(I)V

    iget-object v0, p0, Lcom/konka/help/PhotoHelpActivity;->handler:Landroid/os/Handler;

    const-wide/16 v1, 0x1388

    invoke-virtual {v0, v3, v1, v2}, Landroid/os/Handler;->sendEmptyMessageDelayed(IJ)Z

    goto :goto_0
.end method
