.class public final Lcom/konka/epg/R$string;
.super Ljava/lang/Object;
.source "R.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/konka/epg/R;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x19
    name = "string"
.end annotation


# static fields
.field public static final epg_App_Name_Str:I = 0x7f080000

.field public static final epg_Bookmenu_ChannelId_Str:I = 0x7f080002

.field public static final epg_Bookmenu_ChannelName_Str:I = 0x7f080003

.field public static final epg_Bookmenu_EventName_Str:I = 0x7f080004

.field public static final epg_Bookmenu_Title_Str:I = 0x7f080001

.field public static final epg_Mainmenu_BookManager_Str:I = 0x7f080006

.field public static final epg_Mainmenu_EventInfo_Str:I = 0x7f08000c

.field public static final epg_Mainmenu_RecordBook_Str:I = 0x7f080007

.field public static final epg_Mainmenu_SS_No_Module:I = 0x7f080009

.field public static final epg_Mainmenu_SS_Scrambled:I = 0x7f08000a

.field public static final epg_Mainmenu_Signal_Unlock:I = 0x7f080008

.field public static final epg_Mainmenu_SwitchSource_Str:I = 0x7f080005

.field public static final epg_Popwin_Book_Content_Str:I = 0x7f080013

.field public static final epg_Popwin_Book_Title_Str:I = 0x7f080012

.field public static final epg_Popwin_Eventinfo_Title_Str:I = 0x7f080011

.field public static final epg_Popwin_Exit_Content_Str:I = 0x7f08000f

.field public static final epg_Popwin_No_Str:I = 0x7f08000e

.field public static final epg_Popwin_Search_Title_Str:I = 0x7f080010

.field public static final epg_Popwin_Yes_Str:I = 0x7f08000d

.field public static final epg_Toast_BookFail_AtHand_Str:I = 0x7f080019

.field public static final epg_Toast_BookFail_Cancel_Str:I = 0x7f080017

.field public static final epg_Toast_BookFail_Exception_Str:I = 0x7f08001a

.field public static final epg_Toast_BookFail_Exist_Str:I = 0x7f080015

.field public static final epg_Toast_BookFail_Full_Str:I = 0x7f080016

.field public static final epg_Toast_BookFail_OverLay_Str:I = 0x7f080021

.field public static final epg_Toast_BookFail_TimeOut_Str:I = 0x7f080022

.field public static final epg_Toast_BookSuc_Str:I = 0x7f08001c

.field public static final epg_Toast_CantBook_Str:I = 0x7f080014

.field public static final epg_Toast_ListEmpty_Str:I = 0x7f080018

.field public static final epg_Toast_NoSignal_Str:I = 0x7f08001e

.field public static final epg_Toast_No_ThisChannel_Str:I = 0x7f080020

.field public static final epg_Toast_Please_Input_Str:I = 0x7f08001f

.field public static final epg_Toast_UnBookSuc_Str:I = 0x7f08001d

.field public static final epg_bookMenu_Return_Str:I = 0x7f08000b

.field public static final epg_popwin_evnetinfo_null_str:I = 0x7f08001b

.field public static final epg_record_date_str:I = 0x7f080036

.field public static final epg_record_hour_str:I = 0x7f080038

.field public static final epg_record_minute_str:I = 0x7f080039

.field public static final epg_record_month_str:I = 0x7f080037

.field public static final epg_reminder_channel_str:I = 0x7f080024

.field public static final epg_reminder_daily_str:I = 0x7f08002b

.field public static final epg_reminder_date_str:I = 0x7f080026

.field public static final epg_reminder_hint_back:I = 0x7f08002e

.field public static final epg_reminder_hint_ok:I = 0x7f08002d

.field public static final epg_reminder_hour_str:I = 0x7f080028

.field public static final epg_reminder_minute_str:I = 0x7f080025

.field public static final epg_reminder_mode_str:I = 0x7f080029

.field public static final epg_reminder_month_str:I = 0x7f080027

.field public static final epg_reminder_once_str:I = 0x7f08002a

.field public static final epg_reminder_title_str:I = 0x7f080023

.field public static final epg_reminder_weekly_str:I = 0x7f08002c

.field public static final epg_textview_record_chaneel_name_str:I = 0x7f080035

.field public static final epg_textview_record_context_str:I = 0x7f080033

.field public static final epg_textview_record_end_time_str:I = 0x7f08002f

.field public static final epg_textview_record_mode_str:I = 0x7f080034

.field public static final epg_textview_record_sbutitle_str:I = 0x7f080032

.field public static final epg_textview_record_start_time_str:I = 0x7f080030

.field public static final epg_textview_record_title_str:I = 0x7f080031


# direct methods
.method public constructor <init>()V
    .locals 0

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method
