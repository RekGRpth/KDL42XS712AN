.class public Lcom/konka/epg/ui/EpgMainMenuButtomText;
.super Ljava/lang/Object;
.source "EpgMainMenuButtomText.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/konka/epg/ui/EpgMainMenuButtomText$OnBtmTextTouched;
    }
.end annotation


# instance fields
.field private activity:Landroid/app/Activity;

.field private epgMainMenuViewHolder:Lcom/konka/epg/ui/EpgMainMenuViewHolder;

.field private epgSrv:Lcom/konka/epg/service/EpgServiceProvider;

.field private m_BookEventText:Landroid/widget/TextView;

.field private m_BookManagerText:Landroid/widget/TextView;

.field private m_EventInfoText:Landroid/widget/TextView;

.field private m_RecordBookText:Landroid/widget/TextView;

.field private m_SwitchListText:Landroid/widget/TextView;

.field private m_VersionInfoText:Landroid/widget/TextView;

.field private strBookEventArray:[Ljava/lang/String;

.field private strSwitchListArray:[Ljava/lang/String;


# direct methods
.method public constructor <init>(Landroid/app/Activity;Lcom/konka/epg/ui/EpgMainMenuViewHolder;Lcom/konka/epg/service/EpgServiceProvider;)V
    .locals 1
    .param p1    # Landroid/app/Activity;
    .param p2    # Lcom/konka/epg/ui/EpgMainMenuViewHolder;
    .param p3    # Lcom/konka/epg/service/EpgServiceProvider;

    const/4 v0, 0x0

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    iput-object v0, p0, Lcom/konka/epg/ui/EpgMainMenuButtomText;->epgMainMenuViewHolder:Lcom/konka/epg/ui/EpgMainMenuViewHolder;

    iput-object v0, p0, Lcom/konka/epg/ui/EpgMainMenuButtomText;->activity:Landroid/app/Activity;

    iput-object v0, p0, Lcom/konka/epg/ui/EpgMainMenuButtomText;->epgSrv:Lcom/konka/epg/service/EpgServiceProvider;

    iput-object v0, p0, Lcom/konka/epg/ui/EpgMainMenuButtomText;->m_BookManagerText:Landroid/widget/TextView;

    iput-object v0, p0, Lcom/konka/epg/ui/EpgMainMenuButtomText;->m_RecordBookText:Landroid/widget/TextView;

    iput-object v0, p0, Lcom/konka/epg/ui/EpgMainMenuButtomText;->m_SwitchListText:Landroid/widget/TextView;

    iput-object v0, p0, Lcom/konka/epg/ui/EpgMainMenuButtomText;->m_BookEventText:Landroid/widget/TextView;

    iput-object v0, p0, Lcom/konka/epg/ui/EpgMainMenuButtomText;->m_EventInfoText:Landroid/widget/TextView;

    iput-object v0, p0, Lcom/konka/epg/ui/EpgMainMenuButtomText;->m_VersionInfoText:Landroid/widget/TextView;

    iput-object v0, p0, Lcom/konka/epg/ui/EpgMainMenuButtomText;->strSwitchListArray:[Ljava/lang/String;

    iput-object v0, p0, Lcom/konka/epg/ui/EpgMainMenuButtomText;->strBookEventArray:[Ljava/lang/String;

    iput-object p1, p0, Lcom/konka/epg/ui/EpgMainMenuButtomText;->activity:Landroid/app/Activity;

    iput-object p2, p0, Lcom/konka/epg/ui/EpgMainMenuButtomText;->epgMainMenuViewHolder:Lcom/konka/epg/ui/EpgMainMenuViewHolder;

    iput-object p3, p0, Lcom/konka/epg/ui/EpgMainMenuButtomText;->epgSrv:Lcom/konka/epg/service/EpgServiceProvider;

    invoke-direct {p0}, Lcom/konka/epg/ui/EpgMainMenuButtomText;->findView()V

    invoke-direct {p0}, Lcom/konka/epg/ui/EpgMainMenuButtomText;->initView()V

    invoke-direct {p0}, Lcom/konka/epg/ui/EpgMainMenuButtomText;->setListeners()V

    return-void
.end method

.method static synthetic access$0(Lcom/konka/epg/ui/EpgMainMenuButtomText;)Lcom/konka/epg/ui/EpgMainMenuViewHolder;
    .locals 1

    iget-object v0, p0, Lcom/konka/epg/ui/EpgMainMenuButtomText;->epgMainMenuViewHolder:Lcom/konka/epg/ui/EpgMainMenuViewHolder;

    return-object v0
.end method

.method static synthetic access$1(Lcom/konka/epg/ui/EpgMainMenuButtomText;)Landroid/app/Activity;
    .locals 1

    iget-object v0, p0, Lcom/konka/epg/ui/EpgMainMenuButtomText;->activity:Landroid/app/Activity;

    return-object v0
.end method

.method static synthetic access$2(Lcom/konka/epg/ui/EpgMainMenuButtomText;)Lcom/konka/epg/service/EpgServiceProvider;
    .locals 1

    iget-object v0, p0, Lcom/konka/epg/ui/EpgMainMenuButtomText;->epgSrv:Lcom/konka/epg/service/EpgServiceProvider;

    return-object v0
.end method

.method private findView()V
    .locals 2

    iget-object v0, p0, Lcom/konka/epg/ui/EpgMainMenuButtomText;->activity:Landroid/app/Activity;

    const v1, 0x7f0a002e    # com.konka.epg.R.id.epg_mainmenu_bookmanager_text

    invoke-virtual {v0, v1}, Landroid/app/Activity;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    iput-object v0, p0, Lcom/konka/epg/ui/EpgMainMenuButtomText;->m_BookManagerText:Landroid/widget/TextView;

    iget-object v0, p0, Lcom/konka/epg/ui/EpgMainMenuButtomText;->activity:Landroid/app/Activity;

    const v1, 0x7f0a002f    # com.konka.epg.R.id.epg_mainmenu_record_text

    invoke-virtual {v0, v1}, Landroid/app/Activity;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    iput-object v0, p0, Lcom/konka/epg/ui/EpgMainMenuButtomText;->m_RecordBookText:Landroid/widget/TextView;

    iget-object v0, p0, Lcom/konka/epg/ui/EpgMainMenuButtomText;->activity:Landroid/app/Activity;

    const v1, 0x7f0a0032    # com.konka.epg.R.id.epg_mainmenu_switchlist_text

    invoke-virtual {v0, v1}, Landroid/app/Activity;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    iput-object v0, p0, Lcom/konka/epg/ui/EpgMainMenuButtomText;->m_SwitchListText:Landroid/widget/TextView;

    iget-object v0, p0, Lcom/konka/epg/ui/EpgMainMenuButtomText;->activity:Landroid/app/Activity;

    const v1, 0x7f0a0033    # com.konka.epg.R.id.epg_mainmenu_bookevent_text

    invoke-virtual {v0, v1}, Landroid/app/Activity;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    iput-object v0, p0, Lcom/konka/epg/ui/EpgMainMenuButtomText;->m_BookEventText:Landroid/widget/TextView;

    iget-object v0, p0, Lcom/konka/epg/ui/EpgMainMenuButtomText;->activity:Landroid/app/Activity;

    const v1, 0x7f0a0034    # com.konka.epg.R.id.epg_mainmenu_eventinfo_text

    invoke-virtual {v0, v1}, Landroid/app/Activity;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    iput-object v0, p0, Lcom/konka/epg/ui/EpgMainMenuButtomText;->m_EventInfoText:Landroid/widget/TextView;

    iget-object v0, p0, Lcom/konka/epg/ui/EpgMainMenuButtomText;->activity:Landroid/app/Activity;

    const v1, 0x7f0a0035    # com.konka.epg.R.id.epg_mainmenu_versioninfo_text

    invoke-virtual {v0, v1}, Landroid/app/Activity;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    iput-object v0, p0, Lcom/konka/epg/ui/EpgMainMenuButtomText;->m_VersionInfoText:Landroid/widget/TextView;

    return-void
.end method

.method private initView()V
    .locals 6

    const/4 v3, 0x0

    iget-object v1, p0, Lcom/konka/epg/ui/EpgMainMenuButtomText;->activity:Landroid/app/Activity;

    invoke-virtual {v1}, Landroid/app/Activity;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    const/high16 v2, 0x7f090000    # com.konka.epg.R.array.epg_Mainmenu_SwitchList_Str

    invoke-virtual {v1, v2}, Landroid/content/res/Resources;->getStringArray(I)[Ljava/lang/String;

    move-result-object v1

    iput-object v1, p0, Lcom/konka/epg/ui/EpgMainMenuButtomText;->strSwitchListArray:[Ljava/lang/String;

    iget-object v1, p0, Lcom/konka/epg/ui/EpgMainMenuButtomText;->activity:Landroid/app/Activity;

    invoke-virtual {v1}, Landroid/app/Activity;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    const v2, 0x7f090001    # com.konka.epg.R.array.epg_Mainmenu_BookEvent_Str

    invoke-virtual {v1, v2}, Landroid/content/res/Resources;->getStringArray(I)[Ljava/lang/String;

    move-result-object v1

    iput-object v1, p0, Lcom/konka/epg/ui/EpgMainMenuButtomText;->strBookEventArray:[Ljava/lang/String;

    iget-object v1, p0, Lcom/konka/epg/ui/EpgMainMenuButtomText;->m_BookManagerText:Landroid/widget/TextView;

    const v2, 0x7f080006    # com.konka.epg.R.string.epg_Mainmenu_BookManager_Str

    invoke-virtual {v1, v2}, Landroid/widget/TextView;->setText(I)V

    iget-object v1, p0, Lcom/konka/epg/ui/EpgMainMenuButtomText;->m_RecordBookText:Landroid/widget/TextView;

    const v2, 0x7f080007    # com.konka.epg.R.string.epg_Mainmenu_RecordBook_Str

    invoke-virtual {v1, v2}, Landroid/widget/TextView;->setText(I)V

    iget-object v1, p0, Lcom/konka/epg/ui/EpgMainMenuButtomText;->m_SwitchListText:Landroid/widget/TextView;

    iget-object v2, p0, Lcom/konka/epg/ui/EpgMainMenuButtomText;->strSwitchListArray:[Ljava/lang/String;

    aget-object v2, v2, v3

    invoke-virtual {v1, v2}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    iget-object v1, p0, Lcom/konka/epg/ui/EpgMainMenuButtomText;->m_BookEventText:Landroid/widget/TextView;

    iget-object v2, p0, Lcom/konka/epg/ui/EpgMainMenuButtomText;->strBookEventArray:[Ljava/lang/String;

    const/4 v3, 0x2

    aget-object v2, v2, v3

    invoke-virtual {v1, v2}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    iget-object v1, p0, Lcom/konka/epg/ui/EpgMainMenuButtomText;->m_EventInfoText:Landroid/widget/TextView;

    const v2, 0x7f08000c    # com.konka.epg.R.string.epg_Mainmenu_EventInfo_Str

    invoke-virtual {v1, v2}, Landroid/widget/TextView;->setText(I)V

    iget-object v1, p0, Lcom/konka/epg/ui/EpgMainMenuButtomText;->m_EventInfoText:Landroid/widget/TextView;

    const/4 v2, 0x4

    invoke-virtual {v1, v2}, Landroid/widget/TextView;->setVisibility(I)V

    :try_start_0
    iget-object v1, p0, Lcom/konka/epg/ui/EpgMainMenuButtomText;->m_VersionInfoText:Landroid/widget/TextView;

    new-instance v2, Ljava/lang/StringBuilder;

    const-string v3, "V"

    invoke-direct {v2, v3}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    iget-object v3, p0, Lcom/konka/epg/ui/EpgMainMenuButtomText;->activity:Landroid/app/Activity;

    invoke-virtual {v3}, Landroid/app/Activity;->getPackageManager()Landroid/content/pm/PackageManager;

    move-result-object v3

    iget-object v4, p0, Lcom/konka/epg/ui/EpgMainMenuButtomText;->activity:Landroid/app/Activity;

    invoke-virtual {v4}, Landroid/app/Activity;->getPackageName()Ljava/lang/String;

    move-result-object v4

    const/4 v5, 0x0

    invoke-virtual {v3, v4, v5}, Landroid/content/pm/PackageManager;->getPackageInfo(Ljava/lang/String;I)Landroid/content/pm/PackageInfo;

    move-result-object v3

    iget-object v3, v3, Landroid/content/pm/PackageInfo;->versionName:Ljava/lang/String;

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, " Build 7587"

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V
    :try_end_0
    .catch Landroid/content/pm/PackageManager$NameNotFoundException; {:try_start_0 .. :try_end_0} :catch_0

    :goto_0
    return-void

    :catch_0
    move-exception v0

    invoke-virtual {v0}, Landroid/content/pm/PackageManager$NameNotFoundException;->printStackTrace()V

    goto :goto_0
.end method

.method private setListeners()V
    .locals 2

    new-instance v0, Lcom/konka/epg/ui/EpgMainMenuButtomText$OnBtmTextTouched;

    const/4 v1, 0x0

    invoke-direct {v0, p0, v1}, Lcom/konka/epg/ui/EpgMainMenuButtomText$OnBtmTextTouched;-><init>(Lcom/konka/epg/ui/EpgMainMenuButtomText;Lcom/konka/epg/ui/EpgMainMenuButtomText$OnBtmTextTouched;)V

    iget-object v1, p0, Lcom/konka/epg/ui/EpgMainMenuButtomText;->m_BookManagerText:Landroid/widget/TextView;

    invoke-virtual {v1, v0}, Landroid/widget/TextView;->setOnTouchListener(Landroid/view/View$OnTouchListener;)V

    iget-object v1, p0, Lcom/konka/epg/ui/EpgMainMenuButtomText;->m_RecordBookText:Landroid/widget/TextView;

    invoke-virtual {v1, v0}, Landroid/widget/TextView;->setOnTouchListener(Landroid/view/View$OnTouchListener;)V

    iget-object v1, p0, Lcom/konka/epg/ui/EpgMainMenuButtomText;->m_SwitchListText:Landroid/widget/TextView;

    invoke-virtual {v1, v0}, Landroid/widget/TextView;->setOnTouchListener(Landroid/view/View$OnTouchListener;)V

    iget-object v1, p0, Lcom/konka/epg/ui/EpgMainMenuButtomText;->m_BookEventText:Landroid/widget/TextView;

    invoke-virtual {v1, v0}, Landroid/widget/TextView;->setOnTouchListener(Landroid/view/View$OnTouchListener;)V

    iget-object v1, p0, Lcom/konka/epg/ui/EpgMainMenuButtomText;->m_EventInfoText:Landroid/widget/TextView;

    invoke-virtual {v1, v0}, Landroid/widget/TextView;->setOnTouchListener(Landroid/view/View$OnTouchListener;)V

    return-void
.end method


# virtual methods
.method public getM_BookEventStr()[Ljava/lang/String;
    .locals 1

    iget-object v0, p0, Lcom/konka/epg/ui/EpgMainMenuButtomText;->strBookEventArray:[Ljava/lang/String;

    return-object v0
.end method

.method public getM_BookEventText()Landroid/widget/TextView;
    .locals 1

    iget-object v0, p0, Lcom/konka/epg/ui/EpgMainMenuButtomText;->m_BookEventText:Landroid/widget/TextView;

    return-object v0
.end method

.method public getM_BookManagerText()Landroid/widget/TextView;
    .locals 1

    iget-object v0, p0, Lcom/konka/epg/ui/EpgMainMenuButtomText;->m_BookManagerText:Landroid/widget/TextView;

    return-object v0
.end method

.method public getM_EventInfoText()Landroid/widget/TextView;
    .locals 1

    iget-object v0, p0, Lcom/konka/epg/ui/EpgMainMenuButtomText;->m_EventInfoText:Landroid/widget/TextView;

    return-object v0
.end method

.method public getM_SwitchListText()Landroid/widget/TextView;
    .locals 1

    iget-object v0, p0, Lcom/konka/epg/ui/EpgMainMenuButtomText;->m_SwitchListText:Landroid/widget/TextView;

    return-object v0
.end method

.method public getm_RecordBookText()Landroid/widget/TextView;
    .locals 1

    iget-object v0, p0, Lcom/konka/epg/ui/EpgMainMenuButtomText;->m_RecordBookText:Landroid/widget/TextView;

    return-object v0
.end method

.method public setVisibility(I)V
    .locals 4
    .param p1    # I

    const/4 v1, 0x4

    const/4 v3, 0x0

    if-nez p1, :cond_1

    iget-object v0, p0, Lcom/konka/epg/ui/EpgMainMenuButtomText;->m_EventInfoText:Landroid/widget/TextView;

    invoke-virtual {v0, v3}, Landroid/widget/TextView;->setVisibility(I)V

    iget-object v0, p0, Lcom/konka/epg/ui/EpgMainMenuButtomText;->m_SwitchListText:Landroid/widget/TextView;

    iget-object v1, p0, Lcom/konka/epg/ui/EpgMainMenuButtomText;->strSwitchListArray:[Ljava/lang/String;

    const/4 v2, 0x1

    aget-object v1, v1, v2

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    iget-object v0, p0, Lcom/konka/epg/ui/EpgMainMenuButtomText;->m_BookEventText:Landroid/widget/TextView;

    iget-object v1, p0, Lcom/konka/epg/ui/EpgMainMenuButtomText;->strBookEventArray:[Ljava/lang/String;

    aget-object v1, v1, v3

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    :cond_0
    :goto_0
    return-void

    :cond_1
    if-ne p1, v1, :cond_0

    iget-object v0, p0, Lcom/konka/epg/ui/EpgMainMenuButtomText;->m_EventInfoText:Landroid/widget/TextView;

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setVisibility(I)V

    iget-object v0, p0, Lcom/konka/epg/ui/EpgMainMenuButtomText;->m_SwitchListText:Landroid/widget/TextView;

    iget-object v1, p0, Lcom/konka/epg/ui/EpgMainMenuButtomText;->strSwitchListArray:[Ljava/lang/String;

    aget-object v1, v1, v3

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    iget-object v0, p0, Lcom/konka/epg/ui/EpgMainMenuButtomText;->m_BookEventText:Landroid/widget/TextView;

    iget-object v1, p0, Lcom/konka/epg/ui/EpgMainMenuButtomText;->strBookEventArray:[Ljava/lang/String;

    const/4 v2, 0x2

    aget-object v1, v1, v2

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    goto :goto_0
.end method
