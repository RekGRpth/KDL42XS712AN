.class Lcom/konka/epg/ui/EpgMainMenuViewHolder$OnClick;
.super Ljava/lang/Object;
.source "EpgMainMenuViewHolder.java"

# interfaces
.implements Landroid/view/View$OnClickListener;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/konka/epg/ui/EpgMainMenuViewHolder;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x2
    name = "OnClick"
.end annotation


# instance fields
.field final synthetic this$0:Lcom/konka/epg/ui/EpgMainMenuViewHolder;


# direct methods
.method private constructor <init>(Lcom/konka/epg/ui/EpgMainMenuViewHolder;)V
    .locals 0

    iput-object p1, p0, Lcom/konka/epg/ui/EpgMainMenuViewHolder$OnClick;->this$0:Lcom/konka/epg/ui/EpgMainMenuViewHolder;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method synthetic constructor <init>(Lcom/konka/epg/ui/EpgMainMenuViewHolder;Lcom/konka/epg/ui/EpgMainMenuViewHolder$OnClick;)V
    .locals 0

    invoke-direct {p0, p1}, Lcom/konka/epg/ui/EpgMainMenuViewHolder$OnClick;-><init>(Lcom/konka/epg/ui/EpgMainMenuViewHolder;)V

    return-void
.end method


# virtual methods
.method public onClick(Landroid/view/View;)V
    .locals 2
    .param p1    # Landroid/view/View;

    invoke-virtual {p1}, Landroid/view/View;->getId()I

    move-result v0

    packed-switch v0, :pswitch_data_0

    :goto_0
    return-void

    :pswitch_0
    iget-object v0, p0, Lcom/konka/epg/ui/EpgMainMenuViewHolder$OnClick;->this$0:Lcom/konka/epg/ui/EpgMainMenuViewHolder;

    sget-object v1, Lcom/konka/epg/ui/EpgMainMenuActivity;->currentServiceType:Lcom/mstar/android/tvapi/common/vo/EnumServiceType;

    invoke-virtual {v0, v1}, Lcom/konka/epg/ui/EpgMainMenuViewHolder;->refreshByService(Lcom/mstar/android/tvapi/common/vo/EnumServiceType;)V

    iget-object v0, p0, Lcom/konka/epg/ui/EpgMainMenuViewHolder$OnClick;->this$0:Lcom/konka/epg/ui/EpgMainMenuViewHolder;

    invoke-virtual {v0}, Lcom/konka/epg/ui/EpgMainMenuViewHolder;->getEpgMainMenuButtomText()Lcom/konka/epg/ui/EpgMainMenuButtomText;

    move-result-object v0

    const/4 v1, 0x4

    invoke-virtual {v0, v1}, Lcom/konka/epg/ui/EpgMainMenuButtomText;->setVisibility(I)V

    goto :goto_0

    :pswitch_data_0
    .packed-switch 0x7f0a0012
        :pswitch_0    # com.konka.epg.R.id.epg_mainmenu_source_text
    .end packed-switch
.end method
