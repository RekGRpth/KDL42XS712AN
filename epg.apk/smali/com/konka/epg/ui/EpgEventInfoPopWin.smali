.class public Lcom/konka/epg/ui/EpgEventInfoPopWin;
.super Lcom/konka/epg/ui/EpgPopWinParent;
.source "EpgEventInfoPopWin.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/konka/epg/ui/EpgEventInfoPopWin$PopWinKeyClickListener;
    }
.end annotation


# instance fields
.field private m_EventName:Landroid/widget/TextView;

.field private strDescrib:Ljava/lang/String;


# direct methods
.method protected constructor <init>(Landroid/view/View;Landroid/content/Context;Lcom/konka/epg/ui/EpgMainMenuViewHolder;Lcom/konka/epg/service/EpgServiceProvider;)V
    .locals 2
    .param p1    # Landroid/view/View;
    .param p2    # Landroid/content/Context;
    .param p3    # Lcom/konka/epg/ui/EpgMainMenuViewHolder;
    .param p4    # Lcom/konka/epg/service/EpgServiceProvider;

    const/4 v0, 0x0

    invoke-direct {p0, p1, p2, p3, p4}, Lcom/konka/epg/ui/EpgPopWinParent;-><init>(Landroid/view/View;Landroid/content/Context;Lcom/konka/epg/ui/EpgMainMenuViewHolder;Lcom/konka/epg/service/EpgServiceProvider;)V

    iput-object v0, p0, Lcom/konka/epg/ui/EpgEventInfoPopWin;->strDescrib:Ljava/lang/String;

    iput-object v0, p0, Lcom/konka/epg/ui/EpgEventInfoPopWin;->m_EventName:Landroid/widget/TextView;

    const v0, 0x7f030006    # com.konka.epg.R.layout.epg_popwin_eventinfo

    invoke-super {p0, v0}, Lcom/konka/epg/ui/EpgPopWinParent;->initEpgPopWin(I)V

    iget-object v0, p0, Lcom/konka/epg/ui/EpgEventInfoPopWin;->m_TitleText:Landroid/widget/TextView;

    const v1, 0x7f080011    # com.konka.epg.R.string.epg_Popwin_Eventinfo_Title_Str

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setText(I)V

    return-void
.end method


# virtual methods
.method protected findView()V
    .locals 2

    iget-object v0, p0, Lcom/konka/epg/ui/EpgEventInfoPopWin;->m_ContentView:Landroid/view/View;

    const v1, 0x7f0a003f    # com.konka.epg.R.id.epg_popwin_eventinfo_title

    invoke-virtual {v0, v1}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    iput-object v0, p0, Lcom/konka/epg/ui/EpgEventInfoPopWin;->m_TitleText:Landroid/widget/TextView;

    iget-object v0, p0, Lcom/konka/epg/ui/EpgEventInfoPopWin;->m_ContentView:Landroid/view/View;

    const v1, 0x7f0a0042    # com.konka.epg.R.id.epg_popwin_eventinfo_content

    invoke-virtual {v0, v1}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    iput-object v0, p0, Lcom/konka/epg/ui/EpgEventInfoPopWin;->m_ContentText:Landroid/widget/TextView;

    iget-object v0, p0, Lcom/konka/epg/ui/EpgEventInfoPopWin;->m_ContentView:Landroid/view/View;

    const v1, 0x7f0a003e    # com.konka.epg.R.id.epg_popwin_cancel_btn

    invoke-virtual {v0, v1}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/Button;

    iput-object v0, p0, Lcom/konka/epg/ui/EpgEventInfoPopWin;->m_CancelBtn:Landroid/widget/Button;

    iget-object v0, p0, Lcom/konka/epg/ui/EpgEventInfoPopWin;->m_ContentView:Landroid/view/View;

    const v1, 0x7f0a0040    # com.konka.epg.R.id.epg_popwin_evetinfo_eventtitle

    invoke-virtual {v0, v1}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    iput-object v0, p0, Lcom/konka/epg/ui/EpgEventInfoPopWin;->m_EventName:Landroid/widget/TextView;

    return-void
.end method

.method protected initEpgPopWin(I)V
    .locals 3
    .param p1    # I

    iget-object v0, p0, Lcom/konka/epg/ui/EpgEventInfoPopWin;->epgMainMenuViewHolder:Lcom/konka/epg/ui/EpgMainMenuViewHolder;

    invoke-virtual {v0}, Lcom/konka/epg/ui/EpgMainMenuViewHolder;->getM_WeekEventAdapterList()Ljava/util/ArrayList;

    move-result-object v0

    iget-object v1, p0, Lcom/konka/epg/ui/EpgEventInfoPopWin;->epgMainMenuViewHolder:Lcom/konka/epg/ui/EpgMainMenuViewHolder;

    invoke-virtual {v1}, Lcom/konka/epg/ui/EpgMainMenuViewHolder;->getCurrentDayId()I

    move-result v1

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/konka/epg/adapter/EventListAdapter;

    invoke-virtual {v0}, Lcom/konka/epg/adapter/EventListAdapter;->getCount()I

    move-result v0

    if-lez v0, :cond_0

    iget-object v1, p0, Lcom/konka/epg/ui/EpgEventInfoPopWin;->epgServiceProvider:Lcom/konka/epg/service/EpgServiceProvider;

    iget-object v0, p0, Lcom/konka/epg/ui/EpgEventInfoPopWin;->epgMainMenuViewHolder:Lcom/konka/epg/ui/EpgMainMenuViewHolder;

    invoke-virtual {v0}, Lcom/konka/epg/ui/EpgMainMenuViewHolder;->getM_WeekEventAdapterList()Ljava/util/ArrayList;

    move-result-object v0

    iget-object v2, p0, Lcom/konka/epg/ui/EpgEventInfoPopWin;->epgMainMenuViewHolder:Lcom/konka/epg/ui/EpgMainMenuViewHolder;

    invoke-virtual {v2}, Lcom/konka/epg/ui/EpgMainMenuViewHolder;->getCurrentDayId()I

    move-result v2

    invoke-virtual {v0, v2}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/konka/epg/adapter/EventListAdapter;

    iget-object v2, p0, Lcom/konka/epg/ui/EpgEventInfoPopWin;->epgMainMenuViewHolder:Lcom/konka/epg/ui/EpgMainMenuViewHolder;

    invoke-virtual {v2}, Lcom/konka/epg/ui/EpgMainMenuViewHolder;->getPosInEventList()I

    move-result v2

    invoke-virtual {v0, v2}, Lcom/konka/epg/adapter/EventListAdapter;->getItem(I)Lcom/konka/kkimplements/tv/vo/KonkaEventInfo;

    move-result-object v0

    iget v0, v0, Lcom/konka/kkimplements/tv/vo/KonkaEventInfo;->_mStartTime:I

    invoke-interface {v1, v0}, Lcom/konka/epg/service/EpgServiceProvider;->getEventDescribe(I)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/konka/epg/ui/EpgEventInfoPopWin;->strDescrib:Ljava/lang/String;

    iget-object v0, p0, Lcom/konka/epg/ui/EpgEventInfoPopWin;->strDescrib:Ljava/lang/String;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/konka/epg/ui/EpgEventInfoPopWin;->strDescrib:Ljava/lang/String;

    invoke-virtual {v0}, Ljava/lang/String;->trim()Ljava/lang/String;

    :cond_0
    return-void
.end method

.method protected setListener()V
    .locals 2

    new-instance v0, Lcom/konka/epg/ui/EpgEventInfoPopWin$PopWinKeyClickListener;

    const/4 v1, 0x0

    invoke-direct {v0, p0, v1}, Lcom/konka/epg/ui/EpgEventInfoPopWin$PopWinKeyClickListener;-><init>(Lcom/konka/epg/ui/EpgEventInfoPopWin;Lcom/konka/epg/ui/EpgEventInfoPopWin$PopWinKeyClickListener;)V

    iget-object v1, p0, Lcom/konka/epg/ui/EpgEventInfoPopWin;->m_CancelBtn:Landroid/widget/Button;

    invoke-virtual {v1, v0}, Landroid/widget/Button;->setOnKeyListener(Landroid/view/View$OnKeyListener;)V

    iget-object v1, p0, Lcom/konka/epg/ui/EpgEventInfoPopWin;->m_ContentText:Landroid/widget/TextView;

    invoke-virtual {v1, v0}, Landroid/widget/TextView;->setOnKeyListener(Landroid/view/View$OnKeyListener;)V

    return-void
.end method

.method protected showEpgPopWin(II)V
    .locals 6
    .param p1    # I
    .param p2    # I

    invoke-super {p0, p1, p2}, Lcom/konka/epg/ui/EpgPopWinParent;->showEpgPopWin(II)V

    iget-object v1, p0, Lcom/konka/epg/ui/EpgEventInfoPopWin;->m_CancelBtn:Landroid/widget/Button;

    invoke-virtual {v1}, Landroid/widget/Button;->requestFocus()Z

    :try_start_0
    iget-object v2, p0, Lcom/konka/epg/ui/EpgEventInfoPopWin;->m_EventName:Landroid/widget/TextView;

    iget-object v1, p0, Lcom/konka/epg/ui/EpgEventInfoPopWin;->epgMainMenuViewHolder:Lcom/konka/epg/ui/EpgMainMenuViewHolder;

    invoke-virtual {v1}, Lcom/konka/epg/ui/EpgMainMenuViewHolder;->getM_EventList()Landroid/widget/ListView;

    move-result-object v1

    iget-object v3, p0, Lcom/konka/epg/ui/EpgEventInfoPopWin;->epgMainMenuViewHolder:Lcom/konka/epg/ui/EpgMainMenuViewHolder;

    invoke-virtual {v3}, Lcom/konka/epg/ui/EpgMainMenuViewHolder;->getM_EventList()Landroid/widget/ListView;

    move-result-object v3

    invoke-virtual {v3}, Landroid/widget/ListView;->getSelectedItemPosition()I

    move-result v3

    invoke-virtual {v1, v3}, Landroid/widget/ListView;->getItemAtPosition(I)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/konka/kkimplements/tv/vo/KonkaEventInfo;

    iget-object v1, v1, Lcom/konka/kkimplements/tv/vo/KonkaEventInfo;->_mEventName:Ljava/lang/String;

    invoke-virtual {v2, v1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    :goto_0
    iget-object v1, p0, Lcom/konka/epg/ui/EpgEventInfoPopWin;->strDescrib:Ljava/lang/String;

    if-eqz v1, :cond_0

    iget-object v1, p0, Lcom/konka/epg/ui/EpgEventInfoPopWin;->strDescrib:Ljava/lang/String;

    invoke-virtual {v1}, Ljava/lang/String;->length()I

    move-result v1

    const/4 v2, 0x2

    if-ge v1, v2, :cond_1

    :cond_0
    iget-object v1, p0, Lcom/konka/epg/ui/EpgEventInfoPopWin;->m_ContentText:Landroid/widget/TextView;

    const v2, 0x7f08001b    # com.konka.epg.R.string.epg_popwin_evnetinfo_null_str

    invoke-virtual {v1, v2}, Landroid/widget/TextView;->setText(I)V

    :goto_1
    return-void

    :catch_0
    move-exception v0

    invoke-virtual {v0}, Ljava/lang/Exception;->printStackTrace()V

    goto :goto_0

    :cond_1
    iget-object v1, p0, Lcom/konka/epg/ui/EpgEventInfoPopWin;->strDescrib:Ljava/lang/String;

    iget-object v2, p0, Lcom/konka/epg/ui/EpgEventInfoPopWin;->strDescrib:Ljava/lang/String;

    invoke-virtual {v2}, Ljava/lang/String;->length()I

    move-result v2

    add-int/lit8 v2, v2, -0x2

    iget-object v3, p0, Lcom/konka/epg/ui/EpgEventInfoPopWin;->strDescrib:Ljava/lang/String;

    invoke-virtual {v3}, Ljava/lang/String;->length()I

    move-result v3

    add-int/lit8 v3, v3, -0x1

    invoke-virtual {v1, v2, v3}, Ljava/lang/String;->substring(II)Ljava/lang/String;

    move-result-object v1

    const-string v2, "\u3002"

    invoke-virtual {v1, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_2

    iget-object v1, p0, Lcom/konka/epg/ui/EpgEventInfoPopWin;->m_ContentText:Landroid/widget/TextView;

    new-instance v2, Ljava/lang/StringBuilder;

    const-string v3, "\t\t "

    invoke-direct {v2, v3}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    iget-object v3, p0, Lcom/konka/epg/ui/EpgEventInfoPopWin;->strDescrib:Ljava/lang/String;

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    goto :goto_1

    :cond_2
    iget-object v1, p0, Lcom/konka/epg/ui/EpgEventInfoPopWin;->m_ContentText:Landroid/widget/TextView;

    new-instance v2, Ljava/lang/StringBuilder;

    const-string v3, "\t\t "

    invoke-direct {v2, v3}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    iget-object v3, p0, Lcom/konka/epg/ui/EpgEventInfoPopWin;->strDescrib:Ljava/lang/String;

    const/4 v4, 0x0

    iget-object v5, p0, Lcom/konka/epg/ui/EpgEventInfoPopWin;->strDescrib:Ljava/lang/String;

    invoke-virtual {v5}, Ljava/lang/String;->length()I

    move-result v5

    add-int/lit8 v5, v5, -0x1

    invoke-virtual {v3, v4, v5}, Ljava/lang/String;->substring(II)Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, "......"

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    goto :goto_1
.end method
