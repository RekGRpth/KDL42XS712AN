.class public Lcom/konka/epg/ui/EpgBookMenuActivity;
.super Landroid/app/Activity;
.source "EpgBookMenuActivity.java"


# instance fields
.field private bOnStart:Z

.field private epgBookMenuViewHolder:Lcom/konka/epg/ui/EpgBookMenuViewHolder;


# direct methods
.method public constructor <init>()V
    .locals 1

    invoke-direct {p0}, Landroid/app/Activity;-><init>()V

    const/4 v0, 0x0

    iput-object v0, p0, Lcom/konka/epg/ui/EpgBookMenuActivity;->epgBookMenuViewHolder:Lcom/konka/epg/ui/EpgBookMenuViewHolder;

    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/konka/epg/ui/EpgBookMenuActivity;->bOnStart:Z

    return-void
.end method


# virtual methods
.method protected onCreate(Landroid/os/Bundle;)V
    .locals 1
    .param p1    # Landroid/os/Bundle;

    invoke-super {p0, p1}, Landroid/app/Activity;->onCreate(Landroid/os/Bundle;)V

    const-string v0, "=================book menu on create"

    invoke-static {v0}, Lcom/konka/epg/util/EpgDebugPrint;->Debug(Ljava/lang/String;)V

    const/high16 v0, 0x7f030000    # com.konka.epg.R.layout.epg_book_menu

    invoke-virtual {p0, v0}, Lcom/konka/epg/ui/EpgBookMenuActivity;->setContentView(I)V

    return-void
.end method

.method protected onDestroy()V
    .locals 1

    invoke-super {p0}, Landroid/app/Activity;->onDestroy()V

    const-string v0, "=================book menu on destroy"

    invoke-static {v0}, Lcom/konka/epg/util/EpgDebugPrint;->Debug(Ljava/lang/String;)V

    const/4 v0, 0x0

    iput-object v0, p0, Lcom/konka/epg/ui/EpgBookMenuActivity;->epgBookMenuViewHolder:Lcom/konka/epg/ui/EpgBookMenuViewHolder;

    return-void
.end method

.method public onKeyDown(ILandroid/view/KeyEvent;)Z
    .locals 2
    .param p1    # I
    .param p2    # Landroid/view/KeyEvent;

    const/4 v1, 0x0

    const/4 v0, 0x4

    if-ne p1, v0, :cond_1

    iget-object v0, p0, Lcom/konka/epg/ui/EpgBookMenuActivity;->epgBookMenuViewHolder:Lcom/konka/epg/ui/EpgBookMenuViewHolder;

    invoke-virtual {v0, v1}, Lcom/konka/epg/ui/EpgBookMenuViewHolder;->setAct(Z)V

    :cond_0
    invoke-super {p0, p1, p2}, Landroid/app/Activity;->onKeyDown(ILandroid/view/KeyEvent;)Z

    move-result v0

    :goto_0
    return v0

    :cond_1
    const/16 v0, 0xba

    if-ne p1, v0, :cond_0

    iget-object v0, p0, Lcom/konka/epg/ui/EpgBookMenuActivity;->epgBookMenuViewHolder:Lcom/konka/epg/ui/EpgBookMenuViewHolder;

    invoke-virtual {v0, v1}, Lcom/konka/epg/ui/EpgBookMenuViewHolder;->setAct(Z)V

    invoke-virtual {p0}, Lcom/konka/epg/ui/EpgBookMenuActivity;->finish()V

    const/4 v0, 0x1

    goto :goto_0
.end method

.method protected onResume()V
    .locals 1

    invoke-super {p0}, Landroid/app/Activity;->onResume()V

    const-string v0, "=================book menu on resume"

    invoke-static {v0}, Lcom/konka/epg/util/EpgDebugPrint;->Debug(Ljava/lang/String;)V

    iget-boolean v0, p0, Lcom/konka/epg/ui/EpgBookMenuActivity;->bOnStart:Z

    if-nez v0, :cond_0

    iget-object v0, p0, Lcom/konka/epg/ui/EpgBookMenuActivity;->epgBookMenuViewHolder:Lcom/konka/epg/ui/EpgBookMenuViewHolder;

    invoke-virtual {v0}, Lcom/konka/epg/ui/EpgBookMenuViewHolder;->updateBookMenu()V

    :cond_0
    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/konka/epg/ui/EpgBookMenuActivity;->bOnStart:Z

    return-void
.end method

.method protected onStart()V
    .locals 2

    invoke-super {p0}, Landroid/app/Activity;->onStart()V

    const-string v0, "=================book menu on start"

    invoke-static {v0}, Lcom/konka/epg/util/EpgDebugPrint;->Debug(Ljava/lang/String;)V

    iget-object v0, p0, Lcom/konka/epg/ui/EpgBookMenuActivity;->epgBookMenuViewHolder:Lcom/konka/epg/ui/EpgBookMenuViewHolder;

    if-nez v0, :cond_0

    new-instance v0, Lcom/konka/epg/ui/EpgBookMenuViewHolder;

    invoke-direct {v0, p0}, Lcom/konka/epg/ui/EpgBookMenuViewHolder;-><init>(Landroid/app/Activity;)V

    iput-object v0, p0, Lcom/konka/epg/ui/EpgBookMenuActivity;->epgBookMenuViewHolder:Lcom/konka/epg/ui/EpgBookMenuViewHolder;

    const-string v0, "=================book menu new viewholder"

    invoke-static {v0}, Lcom/konka/epg/util/EpgDebugPrint;->Debug(Ljava/lang/String;)V

    :cond_0
    iget-object v0, p0, Lcom/konka/epg/ui/EpgBookMenuActivity;->epgBookMenuViewHolder:Lcom/konka/epg/ui/EpgBookMenuViewHolder;

    invoke-virtual {v0}, Lcom/konka/epg/ui/EpgBookMenuViewHolder;->initView()V

    iget-object v0, p0, Lcom/konka/epg/ui/EpgBookMenuActivity;->epgBookMenuViewHolder:Lcom/konka/epg/ui/EpgBookMenuViewHolder;

    invoke-virtual {v0}, Lcom/konka/epg/ui/EpgBookMenuViewHolder;->startTimeText()V

    iget-object v0, p0, Lcom/konka/epg/ui/EpgBookMenuActivity;->epgBookMenuViewHolder:Lcom/konka/epg/ui/EpgBookMenuViewHolder;

    invoke-virtual {v0}, Lcom/konka/epg/ui/EpgBookMenuViewHolder;->getM_BookList()Landroid/widget/ListView;

    move-result-object v0

    iget-object v1, p0, Lcom/konka/epg/ui/EpgBookMenuActivity;->epgBookMenuViewHolder:Lcom/konka/epg/ui/EpgBookMenuViewHolder;

    invoke-virtual {v1}, Lcom/konka/epg/ui/EpgBookMenuViewHolder;->getM_BookListAdapter()Lcom/konka/epg/adapter/BookListAdapter;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/widget/ListView;->setAdapter(Landroid/widget/ListAdapter;)V

    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/konka/epg/ui/EpgBookMenuActivity;->bOnStart:Z

    return-void
.end method

.method protected onStop()V
    .locals 1

    invoke-super {p0}, Landroid/app/Activity;->onStop()V

    const-string v0, "=================book menu on stop"

    invoke-static {v0}, Lcom/konka/epg/util/EpgDebugPrint;->Debug(Ljava/lang/String;)V

    return-void
.end method
