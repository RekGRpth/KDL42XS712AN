.class public Lcom/konka/epg/ui/EpgMainMenuActivity$EpgHandler;
.super Landroid/os/Handler;
.source "EpgMainMenuActivity.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/konka/epg/ui/EpgMainMenuActivity;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x1
    name = "EpgHandler"
.end annotation


# instance fields
.field final synthetic this$0:Lcom/konka/epg/ui/EpgMainMenuActivity;


# direct methods
.method public constructor <init>(Lcom/konka/epg/ui/EpgMainMenuActivity;)V
    .locals 0

    iput-object p1, p0, Lcom/konka/epg/ui/EpgMainMenuActivity$EpgHandler;->this$0:Lcom/konka/epg/ui/EpgMainMenuActivity;

    invoke-direct {p0}, Landroid/os/Handler;-><init>()V

    return-void
.end method


# virtual methods
.method public handleMessage(Landroid/os/Message;)V
    .locals 5
    .param p1    # Landroid/os/Message;

    const/4 v0, 0x0

    invoke-super {p0, p1}, Landroid/os/Handler;->handleMessage(Landroid/os/Message;)V

    iget-object v3, p0, Lcom/konka/epg/ui/EpgMainMenuActivity$EpgHandler;->this$0:Lcom/konka/epg/ui/EpgMainMenuActivity;

    # getter for: Lcom/konka/epg/ui/EpgMainMenuActivity;->epgViewHolder:Lcom/konka/epg/ui/EpgMainMenuViewHolder;
    invoke-static {v3}, Lcom/konka/epg/ui/EpgMainMenuActivity;->access$1(Lcom/konka/epg/ui/EpgMainMenuActivity;)Lcom/konka/epg/ui/EpgMainMenuViewHolder;

    move-result-object v3

    iget-object v4, p0, Lcom/konka/epg/ui/EpgMainMenuActivity$EpgHandler;->this$0:Lcom/konka/epg/ui/EpgMainMenuActivity;

    # invokes: Lcom/konka/epg/ui/EpgMainMenuActivity;->switchMsgToScreenMode(Landroid/os/Message;)I
    invoke-static {v4, p1}, Lcom/konka/epg/ui/EpgMainMenuActivity;->access$2(Lcom/konka/epg/ui/EpgMainMenuActivity;Landroid/os/Message;)I

    move-result v4

    invoke-virtual {v3, v4}, Lcom/konka/epg/ui/EpgMainMenuViewHolder;->setCurEpgStatus(I)V

    sget-object v3, Lcom/konka/epg/ui/EpgMainMenuActivity;->currentMainFocus:Lcom/konka/epg/ui/EpgMainMenuActivity$EnumFocusPosition;

    sget-object v4, Lcom/konka/epg/ui/EpgMainMenuActivity$EnumFocusPosition;->E_FCS_POS_POPUP_WIN:Lcom/konka/epg/ui/EpgMainMenuActivity$EnumFocusPosition;

    if-eq v3, v4, :cond_0

    iget v3, p1, Landroid/os/Message;->what:I

    sparse-switch v3, :sswitch_data_0

    :cond_0
    :goto_0
    return-void

    :sswitch_0
    iget-object v3, p0, Lcom/konka/epg/ui/EpgMainMenuActivity$EpgHandler;->this$0:Lcom/konka/epg/ui/EpgMainMenuActivity;

    # getter for: Lcom/konka/epg/ui/EpgMainMenuActivity;->epgViewHolder:Lcom/konka/epg/ui/EpgMainMenuViewHolder;
    invoke-static {v3}, Lcom/konka/epg/ui/EpgMainMenuActivity;->access$1(Lcom/konka/epg/ui/EpgMainMenuActivity;)Lcom/konka/epg/ui/EpgMainMenuViewHolder;

    move-result-object v3

    invoke-virtual {v3}, Lcom/konka/epg/ui/EpgMainMenuViewHolder;->refreshSysTime()V

    iget-object v3, p0, Lcom/konka/epg/ui/EpgMainMenuActivity$EpgHandler;->this$0:Lcom/konka/epg/ui/EpgMainMenuActivity;

    # getter for: Lcom/konka/epg/ui/EpgMainMenuActivity;->epgViewHolder:Lcom/konka/epg/ui/EpgMainMenuViewHolder;
    invoke-static {v3}, Lcom/konka/epg/ui/EpgMainMenuActivity;->access$1(Lcom/konka/epg/ui/EpgMainMenuActivity;)Lcom/konka/epg/ui/EpgMainMenuViewHolder;

    move-result-object v3

    invoke-virtual {v3}, Lcom/konka/epg/ui/EpgMainMenuViewHolder;->getEpgMainMenuDayText()Lcom/konka/epg/ui/EpgMainMenuDayText;

    move-result-object v3

    invoke-virtual {v3}, Lcom/konka/epg/ui/EpgMainMenuDayText;->getFirstDayIdOfWeek()I

    move-result v3

    iget-object v4, p0, Lcom/konka/epg/ui/EpgMainMenuActivity$EpgHandler;->this$0:Lcom/konka/epg/ui/EpgMainMenuActivity;

    # getter for: Lcom/konka/epg/ui/EpgMainMenuActivity;->epgSrv:Lcom/konka/epg/service/EpgSrvProImp;
    invoke-static {v4}, Lcom/konka/epg/ui/EpgMainMenuActivity;->access$3(Lcom/konka/epg/ui/EpgMainMenuActivity;)Lcom/konka/epg/service/EpgSrvProImp;

    move-result-object v4

    invoke-virtual {v4}, Lcom/konka/epg/service/EpgSrvProImp;->getTimeInfo()Landroid/text/format/Time;

    move-result-object v4

    iget v4, v4, Landroid/text/format/Time;->weekDay:I

    if-ne v3, v4, :cond_1

    const/4 v0, 0x1

    :cond_1
    if-nez v0, :cond_0

    iget-object v3, p0, Lcom/konka/epg/ui/EpgMainMenuActivity$EpgHandler;->this$0:Lcom/konka/epg/ui/EpgMainMenuActivity;

    # getter for: Lcom/konka/epg/ui/EpgMainMenuActivity;->epgViewHolder:Lcom/konka/epg/ui/EpgMainMenuViewHolder;
    invoke-static {v3}, Lcom/konka/epg/ui/EpgMainMenuActivity;->access$1(Lcom/konka/epg/ui/EpgMainMenuActivity;)Lcom/konka/epg/ui/EpgMainMenuViewHolder;

    move-result-object v3

    invoke-virtual {v3}, Lcom/konka/epg/ui/EpgMainMenuViewHolder;->getEpgMainMenuDayText()Lcom/konka/epg/ui/EpgMainMenuDayText;

    move-result-object v3

    invoke-virtual {v3}, Lcom/konka/epg/ui/EpgMainMenuDayText;->refreshDayText()V

    iget-object v3, p0, Lcom/konka/epg/ui/EpgMainMenuActivity$EpgHandler;->this$0:Lcom/konka/epg/ui/EpgMainMenuActivity;

    # getter for: Lcom/konka/epg/ui/EpgMainMenuActivity;->epgViewHolder:Lcom/konka/epg/ui/EpgMainMenuViewHolder;
    invoke-static {v3}, Lcom/konka/epg/ui/EpgMainMenuActivity;->access$1(Lcom/konka/epg/ui/EpgMainMenuActivity;)Lcom/konka/epg/ui/EpgMainMenuViewHolder;

    move-result-object v3

    sget-object v4, Lcom/konka/epg/ui/EpgMainMenuActivity;->currentServiceType:Lcom/mstar/android/tvapi/common/vo/EnumServiceType;

    invoke-virtual {v3, v4}, Lcom/konka/epg/ui/EpgMainMenuViewHolder;->refreshEventListByChannel(Lcom/mstar/android/tvapi/common/vo/EnumServiceType;)V

    sget-object v3, Lcom/konka/epg/ui/EpgMainMenuActivity;->currentMainFocus:Lcom/konka/epg/ui/EpgMainMenuActivity$EnumFocusPosition;

    sget-object v4, Lcom/konka/epg/ui/EpgMainMenuActivity$EnumFocusPosition;->E_FCS_POS_EVENT_LIST:Lcom/konka/epg/ui/EpgMainMenuActivity$EnumFocusPosition;

    if-ne v3, v4, :cond_0

    iget-object v3, p0, Lcom/konka/epg/ui/EpgMainMenuActivity$EpgHandler;->this$0:Lcom/konka/epg/ui/EpgMainMenuActivity;

    # getter for: Lcom/konka/epg/ui/EpgMainMenuActivity;->epgViewHolder:Lcom/konka/epg/ui/EpgMainMenuViewHolder;
    invoke-static {v3}, Lcom/konka/epg/ui/EpgMainMenuActivity;->access$1(Lcom/konka/epg/ui/EpgMainMenuActivity;)Lcom/konka/epg/ui/EpgMainMenuViewHolder;

    move-result-object v3

    invoke-virtual {v3}, Lcom/konka/epg/ui/EpgMainMenuViewHolder;->getEpgMainMenuDayText()Lcom/konka/epg/ui/EpgMainMenuDayText;

    move-result-object v3

    sget-object v4, Lcom/konka/epg/ui/EpgMainMenuActivity$EnumFocusPosition;->E_FCS_POS_NONE_DAY:Lcom/konka/epg/ui/EpgMainMenuActivity$EnumFocusPosition;

    invoke-virtual {v3, v4}, Lcom/konka/epg/ui/EpgMainMenuDayText;->changeSevenTextFocus(Lcom/konka/epg/ui/EpgMainMenuActivity$EnumFocusPosition;)Lcom/konka/epg/ui/EpgMainMenuActivity$EnumFocusPosition;

    move-result-object v3

    sput-object v3, Lcom/konka/epg/ui/EpgMainMenuActivity;->currentDayFocus:Lcom/konka/epg/ui/EpgMainMenuActivity$EnumFocusPosition;

    goto :goto_0

    :sswitch_1
    invoke-virtual {p1}, Landroid/os/Message;->getData()Landroid/os/Bundle;

    move-result-object v1

    sget-object v3, Lcom/konka/epg/ui/EpgMainMenuActivity;->currentMainFocus:Lcom/konka/epg/ui/EpgMainMenuActivity$EnumFocusPosition;

    sget-object v4, Lcom/konka/epg/ui/EpgMainMenuActivity$EnumFocusPosition;->E_FCS_POS_POPUP_WIN:Lcom/konka/epg/ui/EpgMainMenuActivity$EnumFocusPosition;

    if-eq v3, v4, :cond_0

    iget-object v3, p0, Lcom/konka/epg/ui/EpgMainMenuActivity$EpgHandler;->this$0:Lcom/konka/epg/ui/EpgMainMenuActivity;

    # getter for: Lcom/konka/epg/ui/EpgMainMenuActivity;->epgViewHolder:Lcom/konka/epg/ui/EpgMainMenuViewHolder;
    invoke-static {v3}, Lcom/konka/epg/ui/EpgMainMenuActivity;->access$1(Lcom/konka/epg/ui/EpgMainMenuActivity;)Lcom/konka/epg/ui/EpgMainMenuViewHolder;

    move-result-object v3

    invoke-virtual {v3}, Lcom/konka/epg/ui/EpgMainMenuViewHolder;->getPosInChannelList()I

    move-result v3

    const-string v4, "channelId"

    invoke-virtual {v1, v4}, Landroid/os/Bundle;->getInt(Ljava/lang/String;)I

    move-result v4

    if-ne v3, v4, :cond_0

    iget-object v3, p0, Lcom/konka/epg/ui/EpgMainMenuActivity$EpgHandler;->this$0:Lcom/konka/epg/ui/EpgMainMenuActivity;

    # getter for: Lcom/konka/epg/ui/EpgMainMenuActivity;->epgViewHolder:Lcom/konka/epg/ui/EpgMainMenuViewHolder;
    invoke-static {v3}, Lcom/konka/epg/ui/EpgMainMenuActivity;->access$1(Lcom/konka/epg/ui/EpgMainMenuActivity;)Lcom/konka/epg/ui/EpgMainMenuViewHolder;

    move-result-object v3

    sget-object v4, Lcom/konka/epg/ui/EpgMainMenuActivity;->currentServiceType:Lcom/mstar/android/tvapi/common/vo/EnumServiceType;

    invoke-virtual {v3, v4}, Lcom/konka/epg/ui/EpgMainMenuViewHolder;->refreshEventListByChannel(Lcom/mstar/android/tvapi/common/vo/EnumServiceType;)V

    const-string v3, "========\u300b\u8bbe\u7f6e\u9891\u9053\u3001\u6293\u53d6\u8282\u76ee"

    invoke-static {v3}, Lcom/konka/epg/util/EpgDebugPrint;->Debug(Ljava/lang/String;)V

    new-instance v2, Landroid/content/Intent;

    const-string v3, "com.konka.tv.hotkey.service.UNFREEZE"

    invoke-direct {v2, v3}, Landroid/content/Intent;-><init>(Ljava/lang/String;)V

    iget-object v3, p0, Lcom/konka/epg/ui/EpgMainMenuActivity$EpgHandler;->this$0:Lcom/konka/epg/ui/EpgMainMenuActivity;

    invoke-virtual {v3, v2}, Lcom/konka/epg/ui/EpgMainMenuActivity;->sendBroadcast(Landroid/content/Intent;)V

    goto/16 :goto_0

    :sswitch_2
    iget-object v3, p0, Lcom/konka/epg/ui/EpgMainMenuActivity$EpgHandler;->this$0:Lcom/konka/epg/ui/EpgMainMenuActivity;

    # getter for: Lcom/konka/epg/ui/EpgMainMenuActivity;->epgSrv:Lcom/konka/epg/service/EpgSrvProImp;
    invoke-static {v3}, Lcom/konka/epg/ui/EpgMainMenuActivity;->access$3(Lcom/konka/epg/ui/EpgMainMenuActivity;)Lcom/konka/epg/service/EpgSrvProImp;

    move-result-object v3

    invoke-virtual {v3}, Lcom/konka/epg/service/EpgSrvProImp;->scaleWindow()V

    iget-object v3, p0, Lcom/konka/epg/ui/EpgMainMenuActivity$EpgHandler;->this$0:Lcom/konka/epg/ui/EpgMainMenuActivity;

    # getter for: Lcom/konka/epg/ui/EpgMainMenuActivity;->epgViewHolder:Lcom/konka/epg/ui/EpgMainMenuViewHolder;
    invoke-static {v3}, Lcom/konka/epg/ui/EpgMainMenuActivity;->access$1(Lcom/konka/epg/ui/EpgMainMenuActivity;)Lcom/konka/epg/ui/EpgMainMenuViewHolder;

    move-result-object v3

    invoke-virtual {v3}, Lcom/konka/epg/ui/EpgMainMenuViewHolder;->getM_EpgTypeView()Landroid/view/View;

    move-result-object v3

    invoke-virtual {v3, v0}, Landroid/view/View;->setBackgroundColor(I)V

    goto/16 :goto_0

    :sswitch_3
    invoke-virtual {p1}, Landroid/os/Message;->getData()Landroid/os/Bundle;

    move-result-object v1

    const-string v3, "SwitchTimes"

    invoke-virtual {v1, v3}, Landroid/os/Bundle;->getInt(Ljava/lang/String;)I

    move-result v3

    sget v4, Lcom/konka/epg/ui/EpgMainMenuActivity;->iSwitchServiceTimes:I

    if-ne v3, v4, :cond_0

    const-string v3, "ServiceType"

    invoke-virtual {v1, v3}, Landroid/os/Bundle;->getInt(Ljava/lang/String;)I

    move-result v3

    sget-object v4, Lcom/konka/epg/ui/EpgMainMenuActivity;->currentServiceType:Lcom/mstar/android/tvapi/common/vo/EnumServiceType;

    invoke-virtual {v4}, Lcom/mstar/android/tvapi/common/vo/EnumServiceType;->ordinal()I

    move-result v4

    if-ne v3, v4, :cond_0

    iget-object v3, p0, Lcom/konka/epg/ui/EpgMainMenuActivity$EpgHandler;->this$0:Lcom/konka/epg/ui/EpgMainMenuActivity;

    # getter for: Lcom/konka/epg/ui/EpgMainMenuActivity;->epgViewHolder:Lcom/konka/epg/ui/EpgMainMenuViewHolder;
    invoke-static {v3}, Lcom/konka/epg/ui/EpgMainMenuActivity;->access$1(Lcom/konka/epg/ui/EpgMainMenuActivity;)Lcom/konka/epg/ui/EpgMainMenuViewHolder;

    move-result-object v3

    sget-object v4, Lcom/konka/epg/ui/EpgMainMenuActivity;->currentServiceType:Lcom/mstar/android/tvapi/common/vo/EnumServiceType;

    invoke-virtual {v3, v4}, Lcom/konka/epg/ui/EpgMainMenuViewHolder;->refreshByService(Lcom/mstar/android/tvapi/common/vo/EnumServiceType;)V

    goto/16 :goto_0

    nop

    :sswitch_data_0
    .sparse-switch
        0x1000 -> :sswitch_0
        0x1007 -> :sswitch_1
        0x1008 -> :sswitch_2
        0x1009 -> :sswitch_3
    .end sparse-switch
.end method
