.class Lcom/konka/epg/ui/EpgRecordMenuActivity$1;
.super Ljava/lang/Object;
.source "EpgRecordMenuActivity.java"

# interfaces
.implements Landroid/view/View$OnClickListener;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/konka/epg/ui/EpgRecordMenuActivity;->onCreate(Landroid/os/Bundle;)V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/konka/epg/ui/EpgRecordMenuActivity;


# direct methods
.method constructor <init>(Lcom/konka/epg/ui/EpgRecordMenuActivity;)V
    .locals 0

    iput-object p1, p0, Lcom/konka/epg/ui/EpgRecordMenuActivity$1;->this$0:Lcom/konka/epg/ui/EpgRecordMenuActivity;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onClick(Landroid/view/View;)V
    .locals 11
    .param p1    # Landroid/view/View;

    sget-object v7, Ljava/lang/System;->out:Ljava/io/PrintStream;

    const-string v8, "\n###########addEpgEvent recorder click\n"

    invoke-virtual {v7, v8}, Ljava/io/PrintStream;->println(Ljava/lang/String;)V

    :try_start_0
    new-instance v0, Landroid/text/format/Time;

    invoke-direct {v0}, Landroid/text/format/Time;-><init>()V

    invoke-virtual {v0}, Landroid/text/format/Time;->setToNow()V

    # getter for: Lcom/konka/epg/ui/EpgRecordMenuActivity;->eventTimerInfo:Lcom/mstar/android/tvapi/common/vo/EpgEventTimerInfo;
    invoke-static {}, Lcom/konka/epg/ui/EpgRecordMenuActivity;->access$0()Lcom/mstar/android/tvapi/common/vo/EpgEventTimerInfo;

    move-result-object v7

    iget v8, v7, Lcom/mstar/android/tvapi/common/vo/EpgEventTimerInfo;->startTime:I

    iget-object v9, p0, Lcom/konka/epg/ui/EpgRecordMenuActivity$1;->this$0:Lcom/konka/epg/ui/EpgRecordMenuActivity;

    # getter for: Lcom/konka/epg/ui/EpgRecordMenuActivity;->offsetTimeInS:J
    invoke-static {v9}, Lcom/konka/epg/ui/EpgRecordMenuActivity;->access$1(Lcom/konka/epg/ui/EpgRecordMenuActivity;)J

    move-result-wide v9

    long-to-int v9, v9

    sub-int/2addr v8, v9

    iput v8, v7, Lcom/mstar/android/tvapi/common/vo/EpgEventTimerInfo;->startTime:I

    # getter for: Lcom/konka/epg/ui/EpgRecordMenuActivity;->eventTimerInfo:Lcom/mstar/android/tvapi/common/vo/EpgEventTimerInfo;
    invoke-static {}, Lcom/konka/epg/ui/EpgRecordMenuActivity;->access$0()Lcom/mstar/android/tvapi/common/vo/EpgEventTimerInfo;

    move-result-object v7

    iget v7, v7, Lcom/mstar/android/tvapi/common/vo/EpgEventTimerInfo;->startTime:I

    int-to-long v7, v7

    const-wide/16 v9, 0x3e8

    mul-long/2addr v7, v9

    const/4 v9, 0x1

    invoke-virtual {v0, v9}, Landroid/text/format/Time;->toMillis(Z)J

    move-result-wide v9

    cmp-long v7, v7, v9

    if-lez v7, :cond_1

    const/4 v3, 0x0

    # getter for: Lcom/konka/epg/ui/EpgRecordMenuActivity;->ed:Lcom/konka/kkinterface/tv/EpgDesk;
    invoke-static {}, Lcom/konka/epg/ui/EpgRecordMenuActivity;->access$2()Lcom/konka/kkinterface/tv/EpgDesk;

    move-result-object v7

    # getter for: Lcom/konka/epg/ui/EpgRecordMenuActivity;->eventTimerInfo:Lcom/mstar/android/tvapi/common/vo/EpgEventTimerInfo;
    invoke-static {}, Lcom/konka/epg/ui/EpgRecordMenuActivity;->access$0()Lcom/mstar/android/tvapi/common/vo/EpgEventTimerInfo;

    move-result-object v8

    invoke-interface {v7, v8}, Lcom/konka/kkinterface/tv/EpgDesk;->addEpgEvent(Lcom/mstar/android/tvapi/common/vo/EpgEventTimerInfo;)Lcom/mstar/android/tvapi/dtv/vo/EnumEpgTimerCheck;

    move-result-object v5

    sget-object v7, Ljava/lang/System;->out:Ljava/io/PrintStream;

    new-instance v8, Ljava/lang/StringBuilder;

    const-string v9, "\n ########### eventTimer startTime:"

    invoke-direct {v8, v9}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    # getter for: Lcom/konka/epg/ui/EpgRecordMenuActivity;->eventTimerInfo:Lcom/mstar/android/tvapi/common/vo/EpgEventTimerInfo;
    invoke-static {}, Lcom/konka/epg/ui/EpgRecordMenuActivity;->access$0()Lcom/mstar/android/tvapi/common/vo/EpgEventTimerInfo;

    move-result-object v9

    iget v9, v9, Lcom/mstar/android/tvapi/common/vo/EpgEventTimerInfo;->startTime:I

    invoke-virtual {v8, v9}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v8

    const-string v9, ",durationTime:"

    invoke-virtual {v8, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v8

    # getter for: Lcom/konka/epg/ui/EpgRecordMenuActivity;->eventTimerInfo:Lcom/mstar/android/tvapi/common/vo/EpgEventTimerInfo;
    invoke-static {}, Lcom/konka/epg/ui/EpgRecordMenuActivity;->access$0()Lcom/mstar/android/tvapi/common/vo/EpgEventTimerInfo;

    move-result-object v9

    iget v9, v9, Lcom/mstar/android/tvapi/common/vo/EpgEventTimerInfo;->durationTime:I

    invoke-virtual {v8, v9}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v8

    invoke-virtual {v8}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v8

    invoke-virtual {v7, v8}, Ljava/io/PrintStream;->println(Ljava/lang/String;)V

    iget-object v7, p0, Lcom/konka/epg/ui/EpgRecordMenuActivity$1;->this$0:Lcom/konka/epg/ui/EpgRecordMenuActivity;

    invoke-static {v7}, Lcom/konka/epg/service/EpgSrvProImp;->getEpgMgrInstance(Landroid/app/Activity;)Lcom/konka/epg/service/EpgSrvProImp;

    move-result-object v2

    invoke-virtual {v5}, Lcom/mstar/android/tvapi/dtv/vo/EnumEpgTimerCheck;->ordinal()I

    move-result v7

    sget-object v8, Lcom/mstar/android/tvapi/dtv/vo/EnumEpgTimerCheck;->E_SUCCESS:Lcom/mstar/android/tvapi/dtv/vo/EnumEpgTimerCheck;

    invoke-virtual {v8}, Lcom/mstar/android/tvapi/dtv/vo/EnumEpgTimerCheck;->ordinal()I

    move-result v8

    if-ne v7, v8, :cond_0

    invoke-virtual {v2}, Lcom/konka/epg/service/EpgSrvProImp;->getM_WeekEventAdapterList()Ljava/util/ArrayList;

    move-result-object v7

    iget-object v8, p0, Lcom/konka/epg/ui/EpgRecordMenuActivity$1;->this$0:Lcom/konka/epg/ui/EpgRecordMenuActivity;

    # getter for: Lcom/konka/epg/ui/EpgRecordMenuActivity;->eventDayIdx:I
    invoke-static {v8}, Lcom/konka/epg/ui/EpgRecordMenuActivity;->access$3(Lcom/konka/epg/ui/EpgRecordMenuActivity;)I

    move-result v8

    invoke-virtual {v7, v8}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v7

    check-cast v7, Lcom/konka/epg/adapter/EventListAdapter;

    iget-object v8, p0, Lcom/konka/epg/ui/EpgRecordMenuActivity$1;->this$0:Lcom/konka/epg/ui/EpgRecordMenuActivity;

    # getter for: Lcom/konka/epg/ui/EpgRecordMenuActivity;->eventIdx:I
    invoke-static {v8}, Lcom/konka/epg/ui/EpgRecordMenuActivity;->access$4(Lcom/konka/epg/ui/EpgRecordMenuActivity;)I

    move-result v8

    invoke-virtual {v7, v8}, Lcom/konka/epg/adapter/EventListAdapter;->getItem(I)Lcom/konka/kkimplements/tv/vo/KonkaEventInfo;

    move-result-object v7

    iget-object v3, v7, Lcom/konka/kkimplements/tv/vo/KonkaEventInfo;->_mEventName:Ljava/lang/String;

    sget-object v7, Ljava/lang/System;->out:Ljava/io/PrintStream;

    new-instance v8, Ljava/lang/StringBuilder;

    const-string v9, "\n########### eventname \n"

    invoke-direct {v8, v9}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v8, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v8

    const-string v9, "servicename "

    invoke-virtual {v8, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v8

    iget-object v9, p0, Lcom/konka/epg/ui/EpgRecordMenuActivity$1;->this$0:Lcom/konka/epg/ui/EpgRecordMenuActivity;

    # getter for: Lcom/konka/epg/ui/EpgRecordMenuActivity;->curProgInfo:Lcom/mstar/android/tvapi/common/vo/ProgramInfo;
    invoke-static {v9}, Lcom/konka/epg/ui/EpgRecordMenuActivity;->access$5(Lcom/konka/epg/ui/EpgRecordMenuActivity;)Lcom/mstar/android/tvapi/common/vo/ProgramInfo;

    move-result-object v9

    iget-object v9, v9, Lcom/mstar/android/tvapi/common/vo/ProgramInfo;->serviceName:Ljava/lang/String;

    invoke-virtual {v8, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v8

    invoke-virtual {v8}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v8

    invoke-virtual {v7, v8}, Ljava/io/PrintStream;->println(Ljava/lang/String;)V

    # getter for: Lcom/konka/epg/ui/EpgRecordMenuActivity;->ed:Lcom/konka/kkinterface/tv/EpgDesk;
    invoke-static {}, Lcom/konka/epg/ui/EpgRecordMenuActivity;->access$2()Lcom/konka/kkinterface/tv/EpgDesk;

    move-result-object v7

    iget-object v8, p0, Lcom/konka/epg/ui/EpgRecordMenuActivity$1;->this$0:Lcom/konka/epg/ui/EpgRecordMenuActivity;

    # getter for: Lcom/konka/epg/ui/EpgRecordMenuActivity;->curProgInfo:Lcom/mstar/android/tvapi/common/vo/ProgramInfo;
    invoke-static {v8}, Lcom/konka/epg/ui/EpgRecordMenuActivity;->access$5(Lcom/konka/epg/ui/EpgRecordMenuActivity;)Lcom/mstar/android/tvapi/common/vo/ProgramInfo;

    move-result-object v8

    iget-object v8, v8, Lcom/mstar/android/tvapi/common/vo/ProgramInfo;->serviceName:Ljava/lang/String;

    # getter for: Lcom/konka/epg/ui/EpgRecordMenuActivity;->eventTimerInfo:Lcom/mstar/android/tvapi/common/vo/EpgEventTimerInfo;
    invoke-static {}, Lcom/konka/epg/ui/EpgRecordMenuActivity;->access$0()Lcom/mstar/android/tvapi/common/vo/EpgEventTimerInfo;

    move-result-object v9

    iget v9, v9, Lcom/mstar/android/tvapi/common/vo/EpgEventTimerInfo;->startTime:I

    invoke-interface {v7, v8, v3, v9}, Lcom/konka/kkinterface/tv/EpgDesk;->UpdateEpgTimer(Ljava/lang/String;Ljava/lang/String;I)V

    :cond_0
    iget-object v7, p0, Lcom/konka/epg/ui/EpgRecordMenuActivity$1;->this$0:Lcom/konka/epg/ui/EpgRecordMenuActivity;

    iget-object v8, p0, Lcom/konka/epg/ui/EpgRecordMenuActivity$1;->this$0:Lcom/konka/epg/ui/EpgRecordMenuActivity;

    # getter for: Lcom/konka/epg/ui/EpgRecordMenuActivity;->bookToast:[Ljava/lang/String;
    invoke-static {v8}, Lcom/konka/epg/ui/EpgRecordMenuActivity;->access$6(Lcom/konka/epg/ui/EpgRecordMenuActivity;)[Ljava/lang/String;

    move-result-object v8

    invoke-virtual {v5}, Lcom/mstar/android/tvapi/dtv/vo/EnumEpgTimerCheck;->ordinal()I

    move-result v9

    aget-object v8, v8, v9

    const/16 v9, 0x1e

    invoke-static {v7, v8, v9}, Landroid/widget/Toast;->makeText(Landroid/content/Context;Ljava/lang/CharSequence;I)Landroid/widget/Toast;

    move-result-object v6

    invoke-virtual {v6}, Landroid/widget/Toast;->show()V
    :try_end_0
    .catch Lcom/mstar/android/tvapi/common/exception/TvCommonException; {:try_start_0 .. :try_end_0} :catch_0

    :goto_0
    new-instance v4, Landroid/content/Intent;

    iget-object v7, p0, Lcom/konka/epg/ui/EpgRecordMenuActivity$1;->this$0:Lcom/konka/epg/ui/EpgRecordMenuActivity;

    const-class v8, Lcom/konka/epg/ui/EpgMainMenuActivity;

    invoke-direct {v4, v7, v8}, Landroid/content/Intent;-><init>(Landroid/content/Context;Ljava/lang/Class;)V

    iget-object v7, p0, Lcom/konka/epg/ui/EpgRecordMenuActivity$1;->this$0:Lcom/konka/epg/ui/EpgRecordMenuActivity;

    const-class v8, Lcom/konka/epg/ui/EpgMainMenuActivity;

    invoke-virtual {v4, v7, v8}, Landroid/content/Intent;->setClass(Landroid/content/Context;Ljava/lang/Class;)Landroid/content/Intent;

    const-string v7, "FocusIndex"

    iget-object v8, p0, Lcom/konka/epg/ui/EpgRecordMenuActivity$1;->this$0:Lcom/konka/epg/ui/EpgRecordMenuActivity;

    # getter for: Lcom/konka/epg/ui/EpgRecordMenuActivity;->eventIdx:I
    invoke-static {v8}, Lcom/konka/epg/ui/EpgRecordMenuActivity;->access$4(Lcom/konka/epg/ui/EpgRecordMenuActivity;)I

    move-result v8

    invoke-virtual {v4, v7, v8}, Landroid/content/Intent;->putExtra(Ljava/lang/String;I)Landroid/content/Intent;

    iget-object v7, p0, Lcom/konka/epg/ui/EpgRecordMenuActivity$1;->this$0:Lcom/konka/epg/ui/EpgRecordMenuActivity;

    invoke-virtual {v7, v4}, Lcom/konka/epg/ui/EpgRecordMenuActivity;->startActivity(Landroid/content/Intent;)V

    iget-object v7, p0, Lcom/konka/epg/ui/EpgRecordMenuActivity$1;->this$0:Lcom/konka/epg/ui/EpgRecordMenuActivity;

    invoke-virtual {v7}, Lcom/konka/epg/ui/EpgRecordMenuActivity;->finish()V

    return-void

    :cond_1
    :try_start_1
    iget-object v7, p0, Lcom/konka/epg/ui/EpgRecordMenuActivity$1;->this$0:Lcom/konka/epg/ui/EpgRecordMenuActivity;

    iget-object v8, p0, Lcom/konka/epg/ui/EpgRecordMenuActivity$1;->this$0:Lcom/konka/epg/ui/EpgRecordMenuActivity;

    # getter for: Lcom/konka/epg/ui/EpgRecordMenuActivity;->bookToast:[Ljava/lang/String;
    invoke-static {v8}, Lcom/konka/epg/ui/EpgRecordMenuActivity;->access$6(Lcom/konka/epg/ui/EpgRecordMenuActivity;)[Ljava/lang/String;

    move-result-object v8

    const/4 v9, 0x2

    aget-object v8, v8, v9

    const/16 v9, 0x1e

    invoke-static {v7, v8, v9}, Landroid/widget/Toast;->makeText(Landroid/content/Context;Ljava/lang/CharSequence;I)Landroid/widget/Toast;

    move-result-object v6

    invoke-virtual {v6}, Landroid/widget/Toast;->show()V
    :try_end_1
    .catch Lcom/mstar/android/tvapi/common/exception/TvCommonException; {:try_start_1 .. :try_end_1} :catch_0

    goto :goto_0

    :catch_0
    move-exception v1

    invoke-virtual {v1}, Lcom/mstar/android/tvapi/common/exception/TvCommonException;->printStackTrace()V

    goto :goto_0
.end method
