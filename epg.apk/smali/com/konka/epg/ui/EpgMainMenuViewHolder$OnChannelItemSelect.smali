.class Lcom/konka/epg/ui/EpgMainMenuViewHolder$OnChannelItemSelect;
.super Ljava/lang/Object;
.source "EpgMainMenuViewHolder.java"

# interfaces
.implements Landroid/widget/AdapterView$OnItemSelectedListener;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/konka/epg/ui/EpgMainMenuViewHolder;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x2
    name = "OnChannelItemSelect"
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Object;",
        "Landroid/widget/AdapterView$OnItemSelectedListener;"
    }
.end annotation


# instance fields
.field final synthetic this$0:Lcom/konka/epg/ui/EpgMainMenuViewHolder;


# direct methods
.method private constructor <init>(Lcom/konka/epg/ui/EpgMainMenuViewHolder;)V
    .locals 0

    iput-object p1, p0, Lcom/konka/epg/ui/EpgMainMenuViewHolder$OnChannelItemSelect;->this$0:Lcom/konka/epg/ui/EpgMainMenuViewHolder;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method synthetic constructor <init>(Lcom/konka/epg/ui/EpgMainMenuViewHolder;Lcom/konka/epg/ui/EpgMainMenuViewHolder$OnChannelItemSelect;)V
    .locals 0

    invoke-direct {p0, p1}, Lcom/konka/epg/ui/EpgMainMenuViewHolder$OnChannelItemSelect;-><init>(Lcom/konka/epg/ui/EpgMainMenuViewHolder;)V

    return-void
.end method


# virtual methods
.method public onItemSelected(Landroid/widget/AdapterView;Landroid/view/View;IJ)V
    .locals 3
    .param p2    # Landroid/view/View;
    .param p3    # I
    .param p4    # J
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Landroid/widget/AdapterView",
            "<*>;",
            "Landroid/view/View;",
            "IJ)V"
        }
    .end annotation

    iget-object v1, p0, Lcom/konka/epg/ui/EpgMainMenuViewHolder$OnChannelItemSelect;->this$0:Lcom/konka/epg/ui/EpgMainMenuViewHolder;

    invoke-static {v1, p3}, Lcom/konka/epg/ui/EpgMainMenuViewHolder;->access$0(Lcom/konka/epg/ui/EpgMainMenuViewHolder;I)V

    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "\u5f53\u524d\u4f4d\u7f6e\uff1a"

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    iget-object v2, p0, Lcom/konka/epg/ui/EpgMainMenuViewHolder$OnChannelItemSelect;->this$0:Lcom/konka/epg/ui/EpgMainMenuViewHolder;

    # getter for: Lcom/konka/epg/ui/EpgMainMenuViewHolder;->iPosInChannelList:I
    invoke-static {v2}, Lcom/konka/epg/ui/EpgMainMenuViewHolder;->access$1(Lcom/konka/epg/ui/EpgMainMenuViewHolder;)I

    move-result v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v1}, Lcom/konka/epg/util/EpgDebugPrint;->Debug(Ljava/lang/String;)V

    iget-object v1, p0, Lcom/konka/epg/ui/EpgMainMenuViewHolder$OnChannelItemSelect;->this$0:Lcom/konka/epg/ui/EpgMainMenuViewHolder;

    const/4 v2, 0x0

    invoke-static {v1, v2}, Lcom/konka/epg/ui/EpgMainMenuViewHolder;->access$2(Lcom/konka/epg/ui/EpgMainMenuViewHolder;I)V

    sget-object v1, Lcom/konka/epg/ui/EpgMainMenuActivity$EnumFocusPosition;->E_FCS_POS_NONE_DAY:Lcom/konka/epg/ui/EpgMainMenuActivity$EnumFocusPosition;

    sput-object v1, Lcom/konka/epg/ui/EpgMainMenuActivity;->currentDayFocus:Lcom/konka/epg/ui/EpgMainMenuActivity$EnumFocusPosition;

    const/4 v0, 0x0

    :goto_0
    const/4 v1, 0x7

    if-lt v0, v1, :cond_1

    iget-object v1, p0, Lcom/konka/epg/ui/EpgMainMenuViewHolder$OnChannelItemSelect;->this$0:Lcom/konka/epg/ui/EpgMainMenuViewHolder;

    # getter for: Lcom/konka/epg/ui/EpgMainMenuViewHolder;->m_EventList:Landroid/widget/ListView;
    invoke-static {v1}, Lcom/konka/epg/ui/EpgMainMenuViewHolder;->access$3(Lcom/konka/epg/ui/EpgMainMenuViewHolder;)Landroid/widget/ListView;

    move-result-object v1

    invoke-virtual {v1}, Landroid/widget/ListView;->getAdapter()Landroid/widget/ListAdapter;

    move-result-object v1

    if-eqz v1, :cond_0

    iget-object v1, p0, Lcom/konka/epg/ui/EpgMainMenuViewHolder$OnChannelItemSelect;->this$0:Lcom/konka/epg/ui/EpgMainMenuViewHolder;

    # getter for: Lcom/konka/epg/ui/EpgMainMenuViewHolder;->m_EventList:Landroid/widget/ListView;
    invoke-static {v1}, Lcom/konka/epg/ui/EpgMainMenuViewHolder;->access$3(Lcom/konka/epg/ui/EpgMainMenuViewHolder;)Landroid/widget/ListView;

    move-result-object v1

    invoke-virtual {v1}, Landroid/widget/ListView;->getAdapter()Landroid/widget/ListAdapter;

    move-result-object v1

    check-cast v1, Lcom/konka/epg/adapter/EventListAdapter;

    invoke-virtual {v1}, Lcom/konka/epg/adapter/EventListAdapter;->notifyDataSetChanged()V

    :cond_0
    iget-object v1, p0, Lcom/konka/epg/ui/EpgMainMenuViewHolder$OnChannelItemSelect;->this$0:Lcom/konka/epg/ui/EpgMainMenuViewHolder;

    sget-object v2, Lcom/konka/epg/ui/EpgMainMenuActivity;->currentServiceType:Lcom/mstar/android/tvapi/common/vo/EnumServiceType;

    invoke-virtual {v1, v2}, Lcom/konka/epg/ui/EpgMainMenuViewHolder;->delayProgSel(Lcom/mstar/android/tvapi/common/vo/EnumServiceType;)V

    return-void

    :cond_1
    iget-object v1, p0, Lcom/konka/epg/ui/EpgMainMenuViewHolder$OnChannelItemSelect;->this$0:Lcom/konka/epg/ui/EpgMainMenuViewHolder;

    invoke-virtual {v1}, Lcom/konka/epg/ui/EpgMainMenuViewHolder;->getM_WeekEventAdapterList()Ljava/util/ArrayList;

    move-result-object v1

    invoke-virtual {v1, v0}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/konka/epg/adapter/EventListAdapter;

    invoke-virtual {v1}, Lcom/konka/epg/adapter/EventListAdapter;->delAllItem()V

    add-int/lit8 v0, v0, 0x1

    goto :goto_0
.end method

.method public onNothingSelected(Landroid/widget/AdapterView;)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Landroid/widget/AdapterView",
            "<*>;)V"
        }
    .end annotation

    return-void
.end method
