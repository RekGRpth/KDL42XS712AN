.class Lcom/konka/epg/ui/EpgMainMenuViewHolder$OnFocus;
.super Ljava/lang/Object;
.source "EpgMainMenuViewHolder.java"

# interfaces
.implements Landroid/view/View$OnFocusChangeListener;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/konka/epg/ui/EpgMainMenuViewHolder;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x2
    name = "OnFocus"
.end annotation


# instance fields
.field final synthetic this$0:Lcom/konka/epg/ui/EpgMainMenuViewHolder;


# direct methods
.method private constructor <init>(Lcom/konka/epg/ui/EpgMainMenuViewHolder;)V
    .locals 0

    iput-object p1, p0, Lcom/konka/epg/ui/EpgMainMenuViewHolder$OnFocus;->this$0:Lcom/konka/epg/ui/EpgMainMenuViewHolder;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method synthetic constructor <init>(Lcom/konka/epg/ui/EpgMainMenuViewHolder;Lcom/konka/epg/ui/EpgMainMenuViewHolder$OnFocus;)V
    .locals 0

    invoke-direct {p0, p1}, Lcom/konka/epg/ui/EpgMainMenuViewHolder$OnFocus;-><init>(Lcom/konka/epg/ui/EpgMainMenuViewHolder;)V

    return-void
.end method


# virtual methods
.method public onFocusChange(Landroid/view/View;Z)V
    .locals 2
    .param p1    # Landroid/view/View;
    .param p2    # Z

    const/4 v1, 0x0

    invoke-virtual {p1}, Landroid/view/View;->getId()I

    move-result v0

    sparse-switch v0, :sswitch_data_0

    :goto_0
    return-void

    :sswitch_0
    iget-object v0, p0, Lcom/konka/epg/ui/EpgMainMenuViewHolder$OnFocus;->this$0:Lcom/konka/epg/ui/EpgMainMenuViewHolder;

    # getter for: Lcom/konka/epg/ui/EpgMainMenuViewHolder;->m_DtvChannelAdapter:Lcom/konka/epg/adapter/ChannelListAdapter;
    invoke-static {v0}, Lcom/konka/epg/ui/EpgMainMenuViewHolder;->access$9(Lcom/konka/epg/ui/EpgMainMenuViewHolder;)Lcom/konka/epg/adapter/ChannelListAdapter;

    move-result-object v0

    invoke-virtual {v0, p2}, Lcom/konka/epg/adapter/ChannelListAdapter;->setIsFocus(Z)V

    iget-object v0, p0, Lcom/konka/epg/ui/EpgMainMenuViewHolder$OnFocus;->this$0:Lcom/konka/epg/ui/EpgMainMenuViewHolder;

    # getter for: Lcom/konka/epg/ui/EpgMainMenuViewHolder;->m_RadioChannelAdapter:Lcom/konka/epg/adapter/ChannelListAdapter;
    invoke-static {v0}, Lcom/konka/epg/ui/EpgMainMenuViewHolder;->access$10(Lcom/konka/epg/ui/EpgMainMenuViewHolder;)Lcom/konka/epg/adapter/ChannelListAdapter;

    move-result-object v0

    invoke-virtual {v0, p2}, Lcom/konka/epg/adapter/ChannelListAdapter;->setIsFocus(Z)V

    iget-object v0, p0, Lcom/konka/epg/ui/EpgMainMenuViewHolder$OnFocus;->this$0:Lcom/konka/epg/ui/EpgMainMenuViewHolder;

    # getter for: Lcom/konka/epg/ui/EpgMainMenuViewHolder;->m_DtvChannelAdapter:Lcom/konka/epg/adapter/ChannelListAdapter;
    invoke-static {v0}, Lcom/konka/epg/ui/EpgMainMenuViewHolder;->access$9(Lcom/konka/epg/ui/EpgMainMenuViewHolder;)Lcom/konka/epg/adapter/ChannelListAdapter;

    move-result-object v0

    invoke-virtual {v0}, Lcom/konka/epg/adapter/ChannelListAdapter;->notifyDataSetChanged()V

    iget-object v0, p0, Lcom/konka/epg/ui/EpgMainMenuViewHolder$OnFocus;->this$0:Lcom/konka/epg/ui/EpgMainMenuViewHolder;

    # getter for: Lcom/konka/epg/ui/EpgMainMenuViewHolder;->m_RadioChannelAdapter:Lcom/konka/epg/adapter/ChannelListAdapter;
    invoke-static {v0}, Lcom/konka/epg/ui/EpgMainMenuViewHolder;->access$10(Lcom/konka/epg/ui/EpgMainMenuViewHolder;)Lcom/konka/epg/adapter/ChannelListAdapter;

    move-result-object v0

    invoke-virtual {v0}, Lcom/konka/epg/adapter/ChannelListAdapter;->notifyDataSetChanged()V

    goto :goto_0

    :sswitch_1
    iget-object v0, p0, Lcom/konka/epg/ui/EpgMainMenuViewHolder$OnFocus;->this$0:Lcom/konka/epg/ui/EpgMainMenuViewHolder;

    # getter for: Lcom/konka/epg/ui/EpgMainMenuViewHolder;->m_WeekEventAdapterList:Ljava/util/ArrayList;
    invoke-static {v0}, Lcom/konka/epg/ui/EpgMainMenuViewHolder;->access$7(Lcom/konka/epg/ui/EpgMainMenuViewHolder;)Ljava/util/ArrayList;

    move-result-object v0

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/konka/epg/adapter/EventListAdapter;

    invoke-virtual {v0, p2}, Lcom/konka/epg/adapter/EventListAdapter;->setIsInList(Z)V

    iget-object v0, p0, Lcom/konka/epg/ui/EpgMainMenuViewHolder$OnFocus;->this$0:Lcom/konka/epg/ui/EpgMainMenuViewHolder;

    # getter for: Lcom/konka/epg/ui/EpgMainMenuViewHolder;->m_WeekEventAdapterList:Ljava/util/ArrayList;
    invoke-static {v0}, Lcom/konka/epg/ui/EpgMainMenuViewHolder;->access$7(Lcom/konka/epg/ui/EpgMainMenuViewHolder;)Ljava/util/ArrayList;

    move-result-object v0

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/konka/epg/adapter/EventListAdapter;

    invoke-virtual {v0}, Lcom/konka/epg/adapter/EventListAdapter;->notifyDataSetChanged()V

    goto :goto_0

    nop

    :sswitch_data_0
    .sparse-switch
        0x7f0a0013 -> :sswitch_0    # com.konka.epg.R.id.epg_mainmenu_channel_list
        0x7f0a002d -> :sswitch_1    # com.konka.epg.R.id.epg_mainmenu_event_list
    .end sparse-switch
.end method
