.class Lcom/konka/epg/ui/EpgBookMenuViewHolder$2;
.super Ljava/lang/Object;
.source "EpgBookMenuViewHolder.java"

# interfaces
.implements Landroid/widget/AdapterView$OnItemClickListener;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/konka/epg/ui/EpgBookMenuViewHolder;->setListener()V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Object;",
        "Landroid/widget/AdapterView$OnItemClickListener;"
    }
.end annotation


# instance fields
.field final synthetic this$0:Lcom/konka/epg/ui/EpgBookMenuViewHolder;


# direct methods
.method constructor <init>(Lcom/konka/epg/ui/EpgBookMenuViewHolder;)V
    .locals 0

    iput-object p1, p0, Lcom/konka/epg/ui/EpgBookMenuViewHolder$2;->this$0:Lcom/konka/epg/ui/EpgBookMenuViewHolder;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onItemClick(Landroid/widget/AdapterView;Landroid/view/View;IJ)V
    .locals 5
    .param p2    # Landroid/view/View;
    .param p3    # I
    .param p4    # J
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Landroid/widget/AdapterView",
            "<*>;",
            "Landroid/view/View;",
            "IJ)V"
        }
    .end annotation

    const/4 v4, 0x0

    iget-object v0, p0, Lcom/konka/epg/ui/EpgBookMenuViewHolder$2;->this$0:Lcom/konka/epg/ui/EpgBookMenuViewHolder;

    # getter for: Lcom/konka/epg/ui/EpgBookMenuViewHolder;->m_EpgDelPopWin:Lcom/konka/epg/ui/EpgDelPopWin;
    invoke-static {v0}, Lcom/konka/epg/ui/EpgBookMenuViewHolder;->access$4(Lcom/konka/epg/ui/EpgBookMenuViewHolder;)Lcom/konka/epg/ui/EpgDelPopWin;

    move-result-object v0

    const v1, 0x7f030005    # com.konka.epg.R.layout.epg_popwin_del

    iget-object v2, p0, Lcom/konka/epg/ui/EpgBookMenuViewHolder$2;->this$0:Lcom/konka/epg/ui/EpgBookMenuViewHolder;

    # getter for: Lcom/konka/epg/ui/EpgBookMenuViewHolder;->epgDesk:Lcom/konka/kkinterface/tv/EpgDesk;
    invoke-static {v2}, Lcom/konka/epg/ui/EpgBookMenuViewHolder;->access$1(Lcom/konka/epg/ui/EpgBookMenuViewHolder;)Lcom/konka/kkinterface/tv/EpgDesk;

    move-result-object v2

    iget-object v3, p0, Lcom/konka/epg/ui/EpgBookMenuViewHolder$2;->this$0:Lcom/konka/epg/ui/EpgBookMenuViewHolder;

    # getter for: Lcom/konka/epg/ui/EpgBookMenuViewHolder;->m_BookListAdapter:Lcom/konka/epg/adapter/BookListAdapter;
    invoke-static {v3}, Lcom/konka/epg/ui/EpgBookMenuViewHolder;->access$5(Lcom/konka/epg/ui/EpgBookMenuViewHolder;)Lcom/konka/epg/adapter/BookListAdapter;

    move-result-object v3

    invoke-virtual {v0, v1, v2, v3, p3}, Lcom/konka/epg/ui/EpgDelPopWin;->initEpgPopWin(ILcom/konka/kkinterface/tv/EpgDesk;Lcom/konka/epg/adapter/BookListAdapter;I)V

    iget-object v0, p0, Lcom/konka/epg/ui/EpgBookMenuViewHolder$2;->this$0:Lcom/konka/epg/ui/EpgBookMenuViewHolder;

    # getter for: Lcom/konka/epg/ui/EpgBookMenuViewHolder;->m_EpgDelPopWin:Lcom/konka/epg/ui/EpgDelPopWin;
    invoke-static {v0}, Lcom/konka/epg/ui/EpgBookMenuViewHolder;->access$4(Lcom/konka/epg/ui/EpgBookMenuViewHolder;)Lcom/konka/epg/ui/EpgDelPopWin;

    move-result-object v0

    invoke-virtual {v0, v4, v4}, Lcom/konka/epg/ui/EpgDelPopWin;->showEpgPopWin(II)V

    return-void
.end method
