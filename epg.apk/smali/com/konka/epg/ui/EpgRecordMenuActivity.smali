.class public Lcom/konka/epg/ui/EpgRecordMenuActivity;
.super Landroid/app/Activity;
.source "EpgRecordMenuActivity.java"


# static fields
.field private static cd:Lcom/konka/kkinterface/tv/ChannelDesk;

.field private static ed:Lcom/konka/kkinterface/tv/EpgDesk;

.field private static eventTimerInfo:Lcom/mstar/android/tvapi/common/vo/EpgEventTimerInfo;

.field private static userProgramIndex:I


# instance fields
.field private final EPG_EVENT_NONE:I

.field private final EPG_EVENT_RECORDER:I

.field private final EPG_EVENT_REMIDER:I

.field private final EPG_REPEAT_DAILY:I

.field private final EPG_REPEAT_ONCE:I

.field private final EPG_REPEAT_WEEKLY:I

.field private bookToast:[Ljava/lang/String;

.field private channelValue:Landroid/widget/TextView;

.field private curProgInfo:Lcom/mstar/android/tvapi/common/vo/ProgramInfo;

.field private endDate:Landroid/widget/LinearLayout;

.field private endDateValue:Landroid/widget/TextView;

.field private endHour:Landroid/widget/LinearLayout;

.field private endHourValue:Landroid/widget/TextView;

.field private endMinute:Landroid/widget/LinearLayout;

.field private endMinuteValue:Landroid/widget/TextView;

.field private endMonth:Landroid/widget/LinearLayout;

.field private endMonthValue:Landroid/widget/TextView;

.field private eventBaseTime:I

.field private eventDayIdx:I

.field private eventIdx:I

.field private eventInfoList:Ljava/util/ArrayList;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/ArrayList",
            "<",
            "Lcom/mstar/android/tvapi/dtv/vo/EpgEventInfo;",
            ">;"
        }
    .end annotation
.end field

.field private modeValue:Landroid/widget/TextView;

.field private offsetTimeInS:J

.field private recordChannel:Landroid/widget/LinearLayout;

.field private recordMode:Landroid/widget/LinearLayout;

.field private startDate:Landroid/widget/LinearLayout;

.field private startDateValue:Landroid/widget/TextView;

.field private startHour:Landroid/widget/LinearLayout;

.field private startHourValue:Landroid/widget/TextView;

.field private startMinute:Landroid/widget/LinearLayout;

.field private startMinuteValue:Landroid/widget/TextView;

.field private startMonth:Landroid/widget/LinearLayout;

.field private startMonthValue:Landroid/widget/TextView;


# direct methods
.method static constructor <clinit>()V
    .locals 2

    const/4 v1, 0x0

    new-instance v0, Lcom/mstar/android/tvapi/common/vo/EpgEventTimerInfo;

    invoke-direct {v0}, Lcom/mstar/android/tvapi/common/vo/EpgEventTimerInfo;-><init>()V

    sput-object v0, Lcom/konka/epg/ui/EpgRecordMenuActivity;->eventTimerInfo:Lcom/mstar/android/tvapi/common/vo/EpgEventTimerInfo;

    sput-object v1, Lcom/konka/epg/ui/EpgRecordMenuActivity;->cd:Lcom/konka/kkinterface/tv/ChannelDesk;

    sput-object v1, Lcom/konka/epg/ui/EpgRecordMenuActivity;->ed:Lcom/konka/kkinterface/tv/EpgDesk;

    const/4 v0, 0x0

    sput v0, Lcom/konka/epg/ui/EpgRecordMenuActivity;->userProgramIndex:I

    return-void
.end method

.method public constructor <init>()V
    .locals 3

    const/4 v0, 0x0

    const/4 v2, 0x0

    invoke-direct {p0}, Landroid/app/Activity;-><init>()V

    iput v0, p0, Lcom/konka/epg/ui/EpgRecordMenuActivity;->eventBaseTime:I

    iput v0, p0, Lcom/konka/epg/ui/EpgRecordMenuActivity;->eventIdx:I

    iput v0, p0, Lcom/konka/epg/ui/EpgRecordMenuActivity;->eventDayIdx:I

    iput v0, p0, Lcom/konka/epg/ui/EpgRecordMenuActivity;->EPG_EVENT_NONE:I

    const/4 v0, 0x1

    iput v0, p0, Lcom/konka/epg/ui/EpgRecordMenuActivity;->EPG_EVENT_REMIDER:I

    const/4 v0, 0x2

    iput v0, p0, Lcom/konka/epg/ui/EpgRecordMenuActivity;->EPG_EVENT_RECORDER:I

    const/16 v0, 0x81

    iput v0, p0, Lcom/konka/epg/ui/EpgRecordMenuActivity;->EPG_REPEAT_ONCE:I

    const/16 v0, 0x7f

    iput v0, p0, Lcom/konka/epg/ui/EpgRecordMenuActivity;->EPG_REPEAT_DAILY:I

    const/16 v0, 0x82

    iput v0, p0, Lcom/konka/epg/ui/EpgRecordMenuActivity;->EPG_REPEAT_WEEKLY:I

    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, Lcom/konka/epg/ui/EpgRecordMenuActivity;->eventInfoList:Ljava/util/ArrayList;

    iput-object v2, p0, Lcom/konka/epg/ui/EpgRecordMenuActivity;->channelValue:Landroid/widget/TextView;

    iput-object v2, p0, Lcom/konka/epg/ui/EpgRecordMenuActivity;->startMinuteValue:Landroid/widget/TextView;

    iput-object v2, p0, Lcom/konka/epg/ui/EpgRecordMenuActivity;->startHourValue:Landroid/widget/TextView;

    iput-object v2, p0, Lcom/konka/epg/ui/EpgRecordMenuActivity;->startMonthValue:Landroid/widget/TextView;

    iput-object v2, p0, Lcom/konka/epg/ui/EpgRecordMenuActivity;->startDateValue:Landroid/widget/TextView;

    iput-object v2, p0, Lcom/konka/epg/ui/EpgRecordMenuActivity;->endMinuteValue:Landroid/widget/TextView;

    iput-object v2, p0, Lcom/konka/epg/ui/EpgRecordMenuActivity;->endHourValue:Landroid/widget/TextView;

    iput-object v2, p0, Lcom/konka/epg/ui/EpgRecordMenuActivity;->endMonthValue:Landroid/widget/TextView;

    iput-object v2, p0, Lcom/konka/epg/ui/EpgRecordMenuActivity;->endDateValue:Landroid/widget/TextView;

    iput-object v2, p0, Lcom/konka/epg/ui/EpgRecordMenuActivity;->modeValue:Landroid/widget/TextView;

    iput-object v2, p0, Lcom/konka/epg/ui/EpgRecordMenuActivity;->recordChannel:Landroid/widget/LinearLayout;

    iput-object v2, p0, Lcom/konka/epg/ui/EpgRecordMenuActivity;->startMinute:Landroid/widget/LinearLayout;

    iput-object v2, p0, Lcom/konka/epg/ui/EpgRecordMenuActivity;->startHour:Landroid/widget/LinearLayout;

    iput-object v2, p0, Lcom/konka/epg/ui/EpgRecordMenuActivity;->startMonth:Landroid/widget/LinearLayout;

    iput-object v2, p0, Lcom/konka/epg/ui/EpgRecordMenuActivity;->startDate:Landroid/widget/LinearLayout;

    iput-object v2, p0, Lcom/konka/epg/ui/EpgRecordMenuActivity;->endMinute:Landroid/widget/LinearLayout;

    iput-object v2, p0, Lcom/konka/epg/ui/EpgRecordMenuActivity;->endHour:Landroid/widget/LinearLayout;

    iput-object v2, p0, Lcom/konka/epg/ui/EpgRecordMenuActivity;->endMonth:Landroid/widget/LinearLayout;

    iput-object v2, p0, Lcom/konka/epg/ui/EpgRecordMenuActivity;->endDate:Landroid/widget/LinearLayout;

    iput-object v2, p0, Lcom/konka/epg/ui/EpgRecordMenuActivity;->recordMode:Landroid/widget/LinearLayout;

    const-wide/16 v0, 0x0

    iput-wide v0, p0, Lcom/konka/epg/ui/EpgRecordMenuActivity;->offsetTimeInS:J

    iput-object v2, p0, Lcom/konka/epg/ui/EpgRecordMenuActivity;->curProgInfo:Lcom/mstar/android/tvapi/common/vo/ProgramInfo;

    return-void
.end method

.method static synthetic access$0()Lcom/mstar/android/tvapi/common/vo/EpgEventTimerInfo;
    .locals 1

    sget-object v0, Lcom/konka/epg/ui/EpgRecordMenuActivity;->eventTimerInfo:Lcom/mstar/android/tvapi/common/vo/EpgEventTimerInfo;

    return-object v0
.end method

.method static synthetic access$1(Lcom/konka/epg/ui/EpgRecordMenuActivity;)J
    .locals 2

    iget-wide v0, p0, Lcom/konka/epg/ui/EpgRecordMenuActivity;->offsetTimeInS:J

    return-wide v0
.end method

.method static synthetic access$2()Lcom/konka/kkinterface/tv/EpgDesk;
    .locals 1

    sget-object v0, Lcom/konka/epg/ui/EpgRecordMenuActivity;->ed:Lcom/konka/kkinterface/tv/EpgDesk;

    return-object v0
.end method

.method static synthetic access$3(Lcom/konka/epg/ui/EpgRecordMenuActivity;)I
    .locals 1

    iget v0, p0, Lcom/konka/epg/ui/EpgRecordMenuActivity;->eventDayIdx:I

    return v0
.end method

.method static synthetic access$4(Lcom/konka/epg/ui/EpgRecordMenuActivity;)I
    .locals 1

    iget v0, p0, Lcom/konka/epg/ui/EpgRecordMenuActivity;->eventIdx:I

    return v0
.end method

.method static synthetic access$5(Lcom/konka/epg/ui/EpgRecordMenuActivity;)Lcom/mstar/android/tvapi/common/vo/ProgramInfo;
    .locals 1

    iget-object v0, p0, Lcom/konka/epg/ui/EpgRecordMenuActivity;->curProgInfo:Lcom/mstar/android/tvapi/common/vo/ProgramInfo;

    return-object v0
.end method

.method static synthetic access$6(Lcom/konka/epg/ui/EpgRecordMenuActivity;)[Ljava/lang/String;
    .locals 1

    iget-object v0, p0, Lcom/konka/epg/ui/EpgRecordMenuActivity;->bookToast:[Ljava/lang/String;

    return-object v0
.end method

.method private clearFocus()V
    .locals 2

    const/high16 v1, 0x7f020000    # com.konka.epg.R.drawable.alpha_img

    iget-object v0, p0, Lcom/konka/epg/ui/EpgRecordMenuActivity;->startMinuteValue:Landroid/widget/TextView;

    invoke-virtual {v0}, Landroid/widget/TextView;->clearFocus()V

    iget-object v0, p0, Lcom/konka/epg/ui/EpgRecordMenuActivity;->startHourValue:Landroid/widget/TextView;

    invoke-virtual {v0}, Landroid/widget/TextView;->clearFocus()V

    iget-object v0, p0, Lcom/konka/epg/ui/EpgRecordMenuActivity;->endMinuteValue:Landroid/widget/TextView;

    invoke-virtual {v0}, Landroid/widget/TextView;->clearFocus()V

    iget-object v0, p0, Lcom/konka/epg/ui/EpgRecordMenuActivity;->endHourValue:Landroid/widget/TextView;

    invoke-virtual {v0}, Landroid/widget/TextView;->clearFocus()V

    iget-object v0, p0, Lcom/konka/epg/ui/EpgRecordMenuActivity;->modeValue:Landroid/widget/TextView;

    invoke-virtual {v0}, Landroid/widget/TextView;->clearFocus()V

    iget-object v0, p0, Lcom/konka/epg/ui/EpgRecordMenuActivity;->startMinute:Landroid/widget/LinearLayout;

    invoke-virtual {v0, v1}, Landroid/widget/LinearLayout;->setBackgroundResource(I)V

    iget-object v0, p0, Lcom/konka/epg/ui/EpgRecordMenuActivity;->startHour:Landroid/widget/LinearLayout;

    invoke-virtual {v0, v1}, Landroid/widget/LinearLayout;->setBackgroundResource(I)V

    iget-object v0, p0, Lcom/konka/epg/ui/EpgRecordMenuActivity;->endMinute:Landroid/widget/LinearLayout;

    invoke-virtual {v0, v1}, Landroid/widget/LinearLayout;->setBackgroundResource(I)V

    iget-object v0, p0, Lcom/konka/epg/ui/EpgRecordMenuActivity;->endHour:Landroid/widget/LinearLayout;

    invoke-virtual {v0, v1}, Landroid/widget/LinearLayout;->setBackgroundResource(I)V

    iget-object v0, p0, Lcom/konka/epg/ui/EpgRecordMenuActivity;->recordMode:Landroid/widget/LinearLayout;

    invoke-virtual {v0, v1}, Landroid/widget/LinearLayout;->setBackgroundResource(I)V

    return-void
.end method


# virtual methods
.method public changeFocusDown()V
    .locals 2

    const v1, 0x7f020028    # com.konka.epg.R.drawable.programme_epg_img_focus

    iget-object v0, p0, Lcom/konka/epg/ui/EpgRecordMenuActivity;->startMinuteValue:Landroid/widget/TextView;

    invoke-virtual {v0}, Landroid/widget/TextView;->isFocused()Z

    move-result v0

    if-eqz v0, :cond_1

    invoke-direct {p0}, Lcom/konka/epg/ui/EpgRecordMenuActivity;->clearFocus()V

    iget-object v0, p0, Lcom/konka/epg/ui/EpgRecordMenuActivity;->startHourValue:Landroid/widget/TextView;

    invoke-virtual {v0}, Landroid/widget/TextView;->requestFocus()Z

    iget-object v0, p0, Lcom/konka/epg/ui/EpgRecordMenuActivity;->startHour:Landroid/widget/LinearLayout;

    invoke-virtual {v0, v1}, Landroid/widget/LinearLayout;->setBackgroundResource(I)V

    :cond_0
    :goto_0
    return-void

    :cond_1
    iget-object v0, p0, Lcom/konka/epg/ui/EpgRecordMenuActivity;->startHourValue:Landroid/widget/TextView;

    invoke-virtual {v0}, Landroid/widget/TextView;->isFocused()Z

    move-result v0

    if-eqz v0, :cond_2

    invoke-direct {p0}, Lcom/konka/epg/ui/EpgRecordMenuActivity;->clearFocus()V

    iget-object v0, p0, Lcom/konka/epg/ui/EpgRecordMenuActivity;->endMinuteValue:Landroid/widget/TextView;

    invoke-virtual {v0}, Landroid/widget/TextView;->requestFocus()Z

    iget-object v0, p0, Lcom/konka/epg/ui/EpgRecordMenuActivity;->endMinute:Landroid/widget/LinearLayout;

    invoke-virtual {v0, v1}, Landroid/widget/LinearLayout;->setBackgroundResource(I)V

    goto :goto_0

    :cond_2
    iget-object v0, p0, Lcom/konka/epg/ui/EpgRecordMenuActivity;->endMinuteValue:Landroid/widget/TextView;

    invoke-virtual {v0}, Landroid/widget/TextView;->isFocused()Z

    move-result v0

    if-eqz v0, :cond_3

    invoke-direct {p0}, Lcom/konka/epg/ui/EpgRecordMenuActivity;->clearFocus()V

    iget-object v0, p0, Lcom/konka/epg/ui/EpgRecordMenuActivity;->endHourValue:Landroid/widget/TextView;

    invoke-virtual {v0}, Landroid/widget/TextView;->requestFocus()Z

    iget-object v0, p0, Lcom/konka/epg/ui/EpgRecordMenuActivity;->endHour:Landroid/widget/LinearLayout;

    invoke-virtual {v0, v1}, Landroid/widget/LinearLayout;->setBackgroundResource(I)V

    goto :goto_0

    :cond_3
    iget-object v0, p0, Lcom/konka/epg/ui/EpgRecordMenuActivity;->endHourValue:Landroid/widget/TextView;

    invoke-virtual {v0}, Landroid/widget/TextView;->isFocused()Z

    move-result v0

    if-eqz v0, :cond_4

    invoke-direct {p0}, Lcom/konka/epg/ui/EpgRecordMenuActivity;->clearFocus()V

    iget-object v0, p0, Lcom/konka/epg/ui/EpgRecordMenuActivity;->modeValue:Landroid/widget/TextView;

    invoke-virtual {v0}, Landroid/widget/TextView;->requestFocus()Z

    iget-object v0, p0, Lcom/konka/epg/ui/EpgRecordMenuActivity;->recordMode:Landroid/widget/LinearLayout;

    invoke-virtual {v0, v1}, Landroid/widget/LinearLayout;->setBackgroundResource(I)V

    goto :goto_0

    :cond_4
    iget-object v0, p0, Lcom/konka/epg/ui/EpgRecordMenuActivity;->modeValue:Landroid/widget/TextView;

    invoke-virtual {v0}, Landroid/widget/TextView;->isFocused()Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-direct {p0}, Lcom/konka/epg/ui/EpgRecordMenuActivity;->clearFocus()V

    iget-object v0, p0, Lcom/konka/epg/ui/EpgRecordMenuActivity;->startMinuteValue:Landroid/widget/TextView;

    invoke-virtual {v0}, Landroid/widget/TextView;->requestFocus()Z

    iget-object v0, p0, Lcom/konka/epg/ui/EpgRecordMenuActivity;->startMinute:Landroid/widget/LinearLayout;

    invoke-virtual {v0, v1}, Landroid/widget/LinearLayout;->setBackgroundResource(I)V

    goto :goto_0
.end method

.method public changeFocusUp()V
    .locals 2

    const v1, 0x7f020028    # com.konka.epg.R.drawable.programme_epg_img_focus

    iget-object v0, p0, Lcom/konka/epg/ui/EpgRecordMenuActivity;->startMinuteValue:Landroid/widget/TextView;

    invoke-virtual {v0}, Landroid/widget/TextView;->isFocused()Z

    move-result v0

    if-eqz v0, :cond_1

    invoke-direct {p0}, Lcom/konka/epg/ui/EpgRecordMenuActivity;->clearFocus()V

    iget-object v0, p0, Lcom/konka/epg/ui/EpgRecordMenuActivity;->modeValue:Landroid/widget/TextView;

    invoke-virtual {v0}, Landroid/widget/TextView;->requestFocus()Z

    iget-object v0, p0, Lcom/konka/epg/ui/EpgRecordMenuActivity;->recordMode:Landroid/widget/LinearLayout;

    invoke-virtual {v0, v1}, Landroid/widget/LinearLayout;->setBackgroundResource(I)V

    :cond_0
    :goto_0
    return-void

    :cond_1
    iget-object v0, p0, Lcom/konka/epg/ui/EpgRecordMenuActivity;->startHourValue:Landroid/widget/TextView;

    invoke-virtual {v0}, Landroid/widget/TextView;->isFocused()Z

    move-result v0

    if-eqz v0, :cond_2

    invoke-direct {p0}, Lcom/konka/epg/ui/EpgRecordMenuActivity;->clearFocus()V

    iget-object v0, p0, Lcom/konka/epg/ui/EpgRecordMenuActivity;->startMinuteValue:Landroid/widget/TextView;

    invoke-virtual {v0}, Landroid/widget/TextView;->requestFocus()Z

    iget-object v0, p0, Lcom/konka/epg/ui/EpgRecordMenuActivity;->startMinute:Landroid/widget/LinearLayout;

    invoke-virtual {v0, v1}, Landroid/widget/LinearLayout;->setBackgroundResource(I)V

    goto :goto_0

    :cond_2
    iget-object v0, p0, Lcom/konka/epg/ui/EpgRecordMenuActivity;->endMinuteValue:Landroid/widget/TextView;

    invoke-virtual {v0}, Landroid/widget/TextView;->isFocused()Z

    move-result v0

    if-eqz v0, :cond_3

    invoke-direct {p0}, Lcom/konka/epg/ui/EpgRecordMenuActivity;->clearFocus()V

    iget-object v0, p0, Lcom/konka/epg/ui/EpgRecordMenuActivity;->startHourValue:Landroid/widget/TextView;

    invoke-virtual {v0}, Landroid/widget/TextView;->requestFocus()Z

    iget-object v0, p0, Lcom/konka/epg/ui/EpgRecordMenuActivity;->startHour:Landroid/widget/LinearLayout;

    invoke-virtual {v0, v1}, Landroid/widget/LinearLayout;->setBackgroundResource(I)V

    goto :goto_0

    :cond_3
    iget-object v0, p0, Lcom/konka/epg/ui/EpgRecordMenuActivity;->endHourValue:Landroid/widget/TextView;

    invoke-virtual {v0}, Landroid/widget/TextView;->isFocused()Z

    move-result v0

    if-eqz v0, :cond_4

    invoke-direct {p0}, Lcom/konka/epg/ui/EpgRecordMenuActivity;->clearFocus()V

    iget-object v0, p0, Lcom/konka/epg/ui/EpgRecordMenuActivity;->endMinuteValue:Landroid/widget/TextView;

    invoke-virtual {v0}, Landroid/widget/TextView;->requestFocus()Z

    iget-object v0, p0, Lcom/konka/epg/ui/EpgRecordMenuActivity;->endMinute:Landroid/widget/LinearLayout;

    invoke-virtual {v0, v1}, Landroid/widget/LinearLayout;->setBackgroundResource(I)V

    goto :goto_0

    :cond_4
    iget-object v0, p0, Lcom/konka/epg/ui/EpgRecordMenuActivity;->modeValue:Landroid/widget/TextView;

    invoke-virtual {v0}, Landroid/widget/TextView;->isFocused()Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-direct {p0}, Lcom/konka/epg/ui/EpgRecordMenuActivity;->clearFocus()V

    iget-object v0, p0, Lcom/konka/epg/ui/EpgRecordMenuActivity;->endHourValue:Landroid/widget/TextView;

    invoke-virtual {v0}, Landroid/widget/TextView;->requestFocus()Z

    iget-object v0, p0, Lcom/konka/epg/ui/EpgRecordMenuActivity;->endHour:Landroid/widget/LinearLayout;

    invoke-virtual {v0, v1}, Landroid/widget/LinearLayout;->setBackgroundResource(I)V

    goto :goto_0
.end method

.method public changeRecordEndHour(Z)V
    .locals 8
    .param p1    # Z

    const-wide/16 v6, 0x3e8

    sget-object v3, Lcom/konka/epg/ui/EpgRecordMenuActivity;->eventTimerInfo:Lcom/mstar/android/tvapi/common/vo/EpgEventTimerInfo;

    iget v3, v3, Lcom/mstar/android/tvapi/common/vo/EpgEventTimerInfo;->startTime:I

    sget-object v4, Lcom/konka/epg/ui/EpgRecordMenuActivity;->eventTimerInfo:Lcom/mstar/android/tvapi/common/vo/EpgEventTimerInfo;

    iget v4, v4, Lcom/mstar/android/tvapi/common/vo/EpgEventTimerInfo;->durationTime:I

    add-int v2, v3, v4

    new-instance v0, Landroid/text/format/Time;

    invoke-direct {v0}, Landroid/text/format/Time;-><init>()V

    int-to-long v3, v2

    mul-long/2addr v3, v6

    invoke-virtual {v0, v3, v4}, Landroid/text/format/Time;->set(J)V

    iget v2, v0, Landroid/text/format/Time;->hour:I

    if-eqz p1, :cond_1

    if-lez v2, :cond_0

    add-int/lit8 v2, v2, -0x1

    :goto_0
    iput v2, v0, Landroid/text/format/Time;->hour:I

    sget-object v3, Lcom/konka/epg/ui/EpgRecordMenuActivity;->eventTimerInfo:Lcom/mstar/android/tvapi/common/vo/EpgEventTimerInfo;

    const/4 v4, 0x0

    invoke-virtual {v0, v4}, Landroid/text/format/Time;->toMillis(Z)J

    move-result-wide v4

    div-long/2addr v4, v6

    long-to-int v4, v4

    sget-object v5, Lcom/konka/epg/ui/EpgRecordMenuActivity;->eventTimerInfo:Lcom/mstar/android/tvapi/common/vo/EpgEventTimerInfo;

    iget v5, v5, Lcom/mstar/android/tvapi/common/vo/EpgEventTimerInfo;->startTime:I

    sub-int/2addr v4, v5

    iput v4, v3, Lcom/mstar/android/tvapi/common/vo/EpgEventTimerInfo;->durationTime:I

    const-string v3, "changeRecordEndHour"

    new-instance v4, Ljava/lang/StringBuilder;

    const-string v5, "========>>> eventTimerInfo startTime["

    invoke-direct {v4, v5}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    sget-object v5, Lcom/konka/epg/ui/EpgRecordMenuActivity;->eventTimerInfo:Lcom/mstar/android/tvapi/common/vo/EpgEventTimerInfo;

    iget v5, v5, Lcom/mstar/android/tvapi/common/vo/EpgEventTimerInfo;->startTime:I

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v4

    const-string v5, "],durationTime["

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    sget-object v5, Lcom/konka/epg/ui/EpgRecordMenuActivity;->eventTimerInfo:Lcom/mstar/android/tvapi/common/vo/EpgEventTimerInfo;

    iget v5, v5, Lcom/mstar/android/tvapi/common/vo/EpgEventTimerInfo;->durationTime:I

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v4

    const-string v5, "]"

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-static {v3, v4}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-static {v2}, Ljava/lang/String;->valueOf(I)Ljava/lang/String;

    move-result-object v4

    invoke-direct {v3, v4}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    iget-object v3, p0, Lcom/konka/epg/ui/EpgRecordMenuActivity;->endHourValue:Landroid/widget/TextView;

    invoke-virtual {v3, v1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    iget-object v3, p0, Lcom/konka/epg/ui/EpgRecordMenuActivity;->endHourValue:Landroid/widget/TextView;

    invoke-virtual {v3}, Landroid/widget/TextView;->invalidate()V

    return-void

    :cond_0
    const/16 v2, 0x17

    goto :goto_0

    :cond_1
    const/16 v3, 0x17

    if-ge v2, v3, :cond_2

    add-int/lit8 v2, v2, 0x1

    goto :goto_0

    :cond_2
    const/4 v2, 0x0

    goto :goto_0
.end method

.method public changeRecordEndMin(Z)V
    .locals 8
    .param p1    # Z

    const-wide/16 v6, 0x3e8

    sget-object v3, Lcom/konka/epg/ui/EpgRecordMenuActivity;->eventTimerInfo:Lcom/mstar/android/tvapi/common/vo/EpgEventTimerInfo;

    iget v3, v3, Lcom/mstar/android/tvapi/common/vo/EpgEventTimerInfo;->startTime:I

    sget-object v4, Lcom/konka/epg/ui/EpgRecordMenuActivity;->eventTimerInfo:Lcom/mstar/android/tvapi/common/vo/EpgEventTimerInfo;

    iget v4, v4, Lcom/mstar/android/tvapi/common/vo/EpgEventTimerInfo;->durationTime:I

    add-int v2, v3, v4

    new-instance v0, Landroid/text/format/Time;

    invoke-direct {v0}, Landroid/text/format/Time;-><init>()V

    int-to-long v3, v2

    mul-long/2addr v3, v6

    invoke-virtual {v0, v3, v4}, Landroid/text/format/Time;->set(J)V

    iget v2, v0, Landroid/text/format/Time;->minute:I

    if-eqz p1, :cond_1

    if-lez v2, :cond_0

    add-int/lit8 v2, v2, -0x1

    :goto_0
    iput v2, v0, Landroid/text/format/Time;->minute:I

    sget-object v3, Lcom/konka/epg/ui/EpgRecordMenuActivity;->eventTimerInfo:Lcom/mstar/android/tvapi/common/vo/EpgEventTimerInfo;

    const/4 v4, 0x0

    invoke-virtual {v0, v4}, Landroid/text/format/Time;->toMillis(Z)J

    move-result-wide v4

    div-long/2addr v4, v6

    long-to-int v4, v4

    sget-object v5, Lcom/konka/epg/ui/EpgRecordMenuActivity;->eventTimerInfo:Lcom/mstar/android/tvapi/common/vo/EpgEventTimerInfo;

    iget v5, v5, Lcom/mstar/android/tvapi/common/vo/EpgEventTimerInfo;->startTime:I

    sub-int/2addr v4, v5

    iput v4, v3, Lcom/mstar/android/tvapi/common/vo/EpgEventTimerInfo;->durationTime:I

    const-string v3, "changeRecordEndMin"

    new-instance v4, Ljava/lang/StringBuilder;

    const-string v5, "========>>> eventTimerInfo startTime["

    invoke-direct {v4, v5}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    sget-object v5, Lcom/konka/epg/ui/EpgRecordMenuActivity;->eventTimerInfo:Lcom/mstar/android/tvapi/common/vo/EpgEventTimerInfo;

    iget v5, v5, Lcom/mstar/android/tvapi/common/vo/EpgEventTimerInfo;->startTime:I

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v4

    const-string v5, "],durationTime["

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    sget-object v5, Lcom/konka/epg/ui/EpgRecordMenuActivity;->eventTimerInfo:Lcom/mstar/android/tvapi/common/vo/EpgEventTimerInfo;

    iget v5, v5, Lcom/mstar/android/tvapi/common/vo/EpgEventTimerInfo;->durationTime:I

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v4

    const-string v5, "]"

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-static {v3, v4}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-static {v2}, Ljava/lang/String;->valueOf(I)Ljava/lang/String;

    move-result-object v4

    invoke-direct {v3, v4}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    iget-object v3, p0, Lcom/konka/epg/ui/EpgRecordMenuActivity;->endMinuteValue:Landroid/widget/TextView;

    invoke-virtual {v3, v1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    iget-object v3, p0, Lcom/konka/epg/ui/EpgRecordMenuActivity;->endMinuteValue:Landroid/widget/TextView;

    invoke-virtual {v3}, Landroid/widget/TextView;->invalidate()V

    return-void

    :cond_0
    const/16 v2, 0x3b

    goto :goto_0

    :cond_1
    const/16 v3, 0x3b

    if-ge v2, v3, :cond_2

    add-int/lit8 v2, v2, 0x1

    goto :goto_0

    :cond_2
    const/4 v2, 0x0

    goto :goto_0
.end method

.method public changeRecordMode(Z)V
    .locals 5
    .param p1    # Z

    const v4, 0x7f08002a    # com.konka.epg.R.string.epg_reminder_once_str

    const/16 v3, 0x82

    const/16 v2, 0x7f

    const/16 v1, 0x81

    if-eqz p1, :cond_0

    sget-object v0, Lcom/konka/epg/ui/EpgRecordMenuActivity;->eventTimerInfo:Lcom/mstar/android/tvapi/common/vo/EpgEventTimerInfo;

    iget-short v0, v0, Lcom/mstar/android/tvapi/common/vo/EpgEventTimerInfo;->enRepeatMode:S

    packed-switch v0, :pswitch_data_0

    :pswitch_0
    sget-object v0, Lcom/konka/epg/ui/EpgRecordMenuActivity;->eventTimerInfo:Lcom/mstar/android/tvapi/common/vo/EpgEventTimerInfo;

    iput-short v1, v0, Lcom/mstar/android/tvapi/common/vo/EpgEventTimerInfo;->enRepeatMode:S

    :goto_0
    sget-object v0, Lcom/konka/epg/ui/EpgRecordMenuActivity;->eventTimerInfo:Lcom/mstar/android/tvapi/common/vo/EpgEventTimerInfo;

    iget-short v0, v0, Lcom/mstar/android/tvapi/common/vo/EpgEventTimerInfo;->enRepeatMode:S

    packed-switch v0, :pswitch_data_1

    :pswitch_1
    iget-object v0, p0, Lcom/konka/epg/ui/EpgRecordMenuActivity;->modeValue:Landroid/widget/TextView;

    invoke-virtual {v0, v4}, Landroid/widget/TextView;->setText(I)V

    :goto_1
    iget-object v0, p0, Lcom/konka/epg/ui/EpgRecordMenuActivity;->modeValue:Landroid/widget/TextView;

    invoke-virtual {v0}, Landroid/widget/TextView;->invalidate()V

    return-void

    :pswitch_2
    sget-object v0, Lcom/konka/epg/ui/EpgRecordMenuActivity;->eventTimerInfo:Lcom/mstar/android/tvapi/common/vo/EpgEventTimerInfo;

    iput-short v2, v0, Lcom/mstar/android/tvapi/common/vo/EpgEventTimerInfo;->enRepeatMode:S

    goto :goto_0

    :pswitch_3
    sget-object v0, Lcom/konka/epg/ui/EpgRecordMenuActivity;->eventTimerInfo:Lcom/mstar/android/tvapi/common/vo/EpgEventTimerInfo;

    iput-short v3, v0, Lcom/mstar/android/tvapi/common/vo/EpgEventTimerInfo;->enRepeatMode:S

    goto :goto_0

    :pswitch_4
    sget-object v0, Lcom/konka/epg/ui/EpgRecordMenuActivity;->eventTimerInfo:Lcom/mstar/android/tvapi/common/vo/EpgEventTimerInfo;

    iput-short v1, v0, Lcom/mstar/android/tvapi/common/vo/EpgEventTimerInfo;->enRepeatMode:S

    goto :goto_0

    :cond_0
    sget-object v0, Lcom/konka/epg/ui/EpgRecordMenuActivity;->eventTimerInfo:Lcom/mstar/android/tvapi/common/vo/EpgEventTimerInfo;

    iget-short v0, v0, Lcom/mstar/android/tvapi/common/vo/EpgEventTimerInfo;->enRepeatMode:S

    packed-switch v0, :pswitch_data_2

    :pswitch_5
    sget-object v0, Lcom/konka/epg/ui/EpgRecordMenuActivity;->eventTimerInfo:Lcom/mstar/android/tvapi/common/vo/EpgEventTimerInfo;

    iput-short v1, v0, Lcom/mstar/android/tvapi/common/vo/EpgEventTimerInfo;->enRepeatMode:S

    goto :goto_0

    :pswitch_6
    sget-object v0, Lcom/konka/epg/ui/EpgRecordMenuActivity;->eventTimerInfo:Lcom/mstar/android/tvapi/common/vo/EpgEventTimerInfo;

    iput-short v3, v0, Lcom/mstar/android/tvapi/common/vo/EpgEventTimerInfo;->enRepeatMode:S

    goto :goto_0

    :pswitch_7
    sget-object v0, Lcom/konka/epg/ui/EpgRecordMenuActivity;->eventTimerInfo:Lcom/mstar/android/tvapi/common/vo/EpgEventTimerInfo;

    iput-short v2, v0, Lcom/mstar/android/tvapi/common/vo/EpgEventTimerInfo;->enRepeatMode:S

    goto :goto_0

    :pswitch_8
    sget-object v0, Lcom/konka/epg/ui/EpgRecordMenuActivity;->eventTimerInfo:Lcom/mstar/android/tvapi/common/vo/EpgEventTimerInfo;

    iput-short v1, v0, Lcom/mstar/android/tvapi/common/vo/EpgEventTimerInfo;->enRepeatMode:S

    goto :goto_0

    :pswitch_9
    iget-object v0, p0, Lcom/konka/epg/ui/EpgRecordMenuActivity;->modeValue:Landroid/widget/TextView;

    invoke-virtual {v0, v4}, Landroid/widget/TextView;->setText(I)V

    goto :goto_1

    :pswitch_a
    iget-object v0, p0, Lcom/konka/epg/ui/EpgRecordMenuActivity;->modeValue:Landroid/widget/TextView;

    const v1, 0x7f08002b    # com.konka.epg.R.string.epg_reminder_daily_str

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setText(I)V

    goto :goto_1

    :pswitch_b
    iget-object v0, p0, Lcom/konka/epg/ui/EpgRecordMenuActivity;->modeValue:Landroid/widget/TextView;

    const v1, 0x7f08002c    # com.konka.epg.R.string.epg_reminder_weekly_str

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setText(I)V

    goto :goto_1

    :pswitch_data_0
    .packed-switch 0x7f
        :pswitch_3
        :pswitch_0
        :pswitch_2
        :pswitch_4
    .end packed-switch

    :pswitch_data_1
    .packed-switch 0x7f
        :pswitch_a
        :pswitch_1
        :pswitch_9
        :pswitch_b
    .end packed-switch

    :pswitch_data_2
    .packed-switch 0x7f
        :pswitch_8
        :pswitch_5
        :pswitch_6
        :pswitch_7
    .end packed-switch
.end method

.method public changeRecordProgramNumber(Z)V
    .locals 7
    .param p1    # Z

    invoke-virtual {p0}, Lcom/konka/epg/ui/EpgRecordMenuActivity;->getApplication()Landroid/app/Application;

    move-result-object v0

    check-cast v0, Lcom/konka/epg/TVRootApp;

    invoke-virtual {v0}, Lcom/konka/epg/TVRootApp;->getTvDeskProvider()Lcom/konka/kkinterface/tv/TvDeskProvider;

    move-result-object v2

    new-instance v1, Lcom/mstar/android/tvapi/common/vo/ProgramInfo;

    invoke-direct {v1}, Lcom/mstar/android/tvapi/common/vo/ProgramInfo;-><init>()V

    const/4 v3, 0x0

    invoke-interface {v2}, Lcom/konka/kkinterface/tv/TvDeskProvider;->getChannelManagerInstance()Lcom/konka/kkinterface/tv/ChannelDesk;

    move-result-object v4

    sput-object v4, Lcom/konka/epg/ui/EpgRecordMenuActivity;->cd:Lcom/konka/kkinterface/tv/ChannelDesk;

    invoke-interface {v2}, Lcom/konka/kkinterface/tv/TvDeskProvider;->getEpgManagerInstance()Lcom/konka/kkinterface/tv/EpgDesk;

    move-result-object v4

    sput-object v4, Lcom/konka/epg/ui/EpgRecordMenuActivity;->ed:Lcom/konka/kkinterface/tv/EpgDesk;

    if-eqz p1, :cond_1

    sget v4, Lcom/konka/epg/ui/EpgRecordMenuActivity;->userProgramIndex:I

    if-lez v4, :cond_0

    sget v4, Lcom/konka/epg/ui/EpgRecordMenuActivity;->userProgramIndex:I

    add-int/lit8 v4, v4, -0x1

    sput v4, Lcom/konka/epg/ui/EpgRecordMenuActivity;->userProgramIndex:I

    :goto_0
    sget-object v4, Lcom/konka/epg/ui/EpgRecordMenuActivity;->cd:Lcom/konka/kkinterface/tv/ChannelDesk;

    sget v5, Lcom/konka/epg/ui/EpgRecordMenuActivity;->userProgramIndex:I

    invoke-interface {v4, v5}, Lcom/konka/kkinterface/tv/ChannelDesk;->getProgramInfoByIndex(I)Lcom/mstar/android/tvapi/common/vo/ProgramInfo;

    move-result-object v1

    sget-object v4, Lcom/konka/epg/ui/EpgRecordMenuActivity;->eventTimerInfo:Lcom/mstar/android/tvapi/common/vo/EpgEventTimerInfo;

    iget v5, v1, Lcom/mstar/android/tvapi/common/vo/ProgramInfo;->number:I

    iput v5, v4, Lcom/mstar/android/tvapi/common/vo/EpgEventTimerInfo;->serviceNumber:I

    new-instance v4, Ljava/lang/StringBuilder;

    const-string v5, "CH "

    invoke-direct {v4, v5}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    iget v5, v1, Lcom/mstar/android/tvapi/common/vo/ProgramInfo;->number:I

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v4

    const-string v5, " "

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    iget-object v5, v1, Lcom/mstar/android/tvapi/common/vo/ProgramInfo;->serviceName:Ljava/lang/String;

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    iget-object v4, p0, Lcom/konka/epg/ui/EpgRecordMenuActivity;->channelValue:Landroid/widget/TextView;

    invoke-virtual {v4, v3}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    iget-object v4, p0, Lcom/konka/epg/ui/EpgRecordMenuActivity;->channelValue:Landroid/widget/TextView;

    invoke-virtual {v4}, Landroid/widget/TextView;->invalidate()V

    return-void

    :cond_0
    sget-object v4, Lcom/konka/epg/ui/EpgRecordMenuActivity;->cd:Lcom/konka/kkinterface/tv/ChannelDesk;

    sget-object v5, Lcom/mstar/android/tvapi/common/vo/EnumProgramCountType;->E_COUNT_DTV:Lcom/mstar/android/tvapi/common/vo/EnumProgramCountType;

    invoke-interface {v4, v5}, Lcom/konka/kkinterface/tv/ChannelDesk;->getProgramCount(Lcom/mstar/android/tvapi/common/vo/EnumProgramCountType;)I

    move-result v4

    add-int/lit8 v4, v4, -0x1

    sput v4, Lcom/konka/epg/ui/EpgRecordMenuActivity;->userProgramIndex:I

    goto :goto_0

    :cond_1
    sget v4, Lcom/konka/epg/ui/EpgRecordMenuActivity;->userProgramIndex:I

    sget-object v5, Lcom/konka/epg/ui/EpgRecordMenuActivity;->cd:Lcom/konka/kkinterface/tv/ChannelDesk;

    sget-object v6, Lcom/mstar/android/tvapi/common/vo/EnumProgramCountType;->E_COUNT_DTV:Lcom/mstar/android/tvapi/common/vo/EnumProgramCountType;

    invoke-interface {v5, v6}, Lcom/konka/kkinterface/tv/ChannelDesk;->getProgramCount(Lcom/mstar/android/tvapi/common/vo/EnumProgramCountType;)I

    move-result v5

    add-int/lit8 v5, v5, -0x1

    if-ge v4, v5, :cond_2

    sget v4, Lcom/konka/epg/ui/EpgRecordMenuActivity;->userProgramIndex:I

    add-int/lit8 v4, v4, 0x1

    sput v4, Lcom/konka/epg/ui/EpgRecordMenuActivity;->userProgramIndex:I

    goto :goto_0

    :cond_2
    const/4 v4, 0x0

    sput v4, Lcom/konka/epg/ui/EpgRecordMenuActivity;->userProgramIndex:I

    goto :goto_0
.end method

.method public changeRecordStartHour(Z)V
    .locals 9
    .param p1    # Z

    const-wide/16 v7, 0x3e8

    sget-object v4, Lcom/konka/epg/ui/EpgRecordMenuActivity;->eventTimerInfo:Lcom/mstar/android/tvapi/common/vo/EpgEventTimerInfo;

    iget v3, v4, Lcom/mstar/android/tvapi/common/vo/EpgEventTimerInfo;->startTime:I

    sget-object v4, Lcom/konka/epg/ui/EpgRecordMenuActivity;->eventTimerInfo:Lcom/mstar/android/tvapi/common/vo/EpgEventTimerInfo;

    iget v0, v4, Lcom/mstar/android/tvapi/common/vo/EpgEventTimerInfo;->startTime:I

    new-instance v1, Landroid/text/format/Time;

    invoke-direct {v1}, Landroid/text/format/Time;-><init>()V

    int-to-long v4, v3

    mul-long/2addr v4, v7

    invoke-virtual {v1, v4, v5}, Landroid/text/format/Time;->set(J)V

    iget v3, v1, Landroid/text/format/Time;->hour:I

    if-eqz p1, :cond_1

    if-lez v3, :cond_0

    add-int/lit8 v3, v3, -0x1

    :goto_0
    iput v3, v1, Landroid/text/format/Time;->hour:I

    sget-object v4, Lcom/konka/epg/ui/EpgRecordMenuActivity;->eventTimerInfo:Lcom/mstar/android/tvapi/common/vo/EpgEventTimerInfo;

    const/4 v5, 0x0

    invoke-virtual {v1, v5}, Landroid/text/format/Time;->toMillis(Z)J

    move-result-wide v5

    div-long/2addr v5, v7

    long-to-int v5, v5

    iput v5, v4, Lcom/mstar/android/tvapi/common/vo/EpgEventTimerInfo;->startTime:I

    sget-object v4, Lcom/konka/epg/ui/EpgRecordMenuActivity;->eventTimerInfo:Lcom/mstar/android/tvapi/common/vo/EpgEventTimerInfo;

    iget v5, v4, Lcom/mstar/android/tvapi/common/vo/EpgEventTimerInfo;->durationTime:I

    sget-object v6, Lcom/konka/epg/ui/EpgRecordMenuActivity;->eventTimerInfo:Lcom/mstar/android/tvapi/common/vo/EpgEventTimerInfo;

    iget v6, v6, Lcom/mstar/android/tvapi/common/vo/EpgEventTimerInfo;->startTime:I

    sub-int/2addr v6, v0

    sub-int/2addr v5, v6

    iput v5, v4, Lcom/mstar/android/tvapi/common/vo/EpgEventTimerInfo;->durationTime:I

    new-instance v4, Ljava/lang/StringBuilder;

    invoke-static {v3}, Ljava/lang/String;->valueOf(I)Ljava/lang/String;

    move-result-object v5

    invoke-direct {v4, v5}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    iget-object v4, p0, Lcom/konka/epg/ui/EpgRecordMenuActivity;->startHourValue:Landroid/widget/TextView;

    invoke-virtual {v4, v2}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    iget-object v4, p0, Lcom/konka/epg/ui/EpgRecordMenuActivity;->startHourValue:Landroid/widget/TextView;

    invoke-virtual {v4}, Landroid/widget/TextView;->invalidate()V

    return-void

    :cond_0
    const/16 v3, 0x17

    goto :goto_0

    :cond_1
    const/16 v4, 0x17

    if-ge v3, v4, :cond_2

    add-int/lit8 v3, v3, 0x1

    goto :goto_0

    :cond_2
    const/4 v3, 0x0

    goto :goto_0
.end method

.method public changeRecordStartMin(Z)V
    .locals 9
    .param p1    # Z

    const-wide/16 v7, 0x3e8

    sget-object v4, Lcom/konka/epg/ui/EpgRecordMenuActivity;->eventTimerInfo:Lcom/mstar/android/tvapi/common/vo/EpgEventTimerInfo;

    iget v3, v4, Lcom/mstar/android/tvapi/common/vo/EpgEventTimerInfo;->startTime:I

    sget-object v4, Lcom/konka/epg/ui/EpgRecordMenuActivity;->eventTimerInfo:Lcom/mstar/android/tvapi/common/vo/EpgEventTimerInfo;

    iget v0, v4, Lcom/mstar/android/tvapi/common/vo/EpgEventTimerInfo;->startTime:I

    new-instance v1, Landroid/text/format/Time;

    invoke-direct {v1}, Landroid/text/format/Time;-><init>()V

    int-to-long v4, v3

    mul-long/2addr v4, v7

    invoke-virtual {v1, v4, v5}, Landroid/text/format/Time;->set(J)V

    iget v3, v1, Landroid/text/format/Time;->minute:I

    if-eqz p1, :cond_1

    if-lez v3, :cond_0

    add-int/lit8 v3, v3, -0x1

    :goto_0
    iput v3, v1, Landroid/text/format/Time;->minute:I

    sget-object v4, Lcom/konka/epg/ui/EpgRecordMenuActivity;->eventTimerInfo:Lcom/mstar/android/tvapi/common/vo/EpgEventTimerInfo;

    const/4 v5, 0x0

    invoke-virtual {v1, v5}, Landroid/text/format/Time;->toMillis(Z)J

    move-result-wide v5

    div-long/2addr v5, v7

    long-to-int v5, v5

    iput v5, v4, Lcom/mstar/android/tvapi/common/vo/EpgEventTimerInfo;->startTime:I

    sget-object v4, Lcom/konka/epg/ui/EpgRecordMenuActivity;->eventTimerInfo:Lcom/mstar/android/tvapi/common/vo/EpgEventTimerInfo;

    iget v5, v4, Lcom/mstar/android/tvapi/common/vo/EpgEventTimerInfo;->durationTime:I

    sget-object v6, Lcom/konka/epg/ui/EpgRecordMenuActivity;->eventTimerInfo:Lcom/mstar/android/tvapi/common/vo/EpgEventTimerInfo;

    iget v6, v6, Lcom/mstar/android/tvapi/common/vo/EpgEventTimerInfo;->startTime:I

    sub-int/2addr v6, v0

    sub-int/2addr v5, v6

    iput v5, v4, Lcom/mstar/android/tvapi/common/vo/EpgEventTimerInfo;->durationTime:I

    new-instance v4, Ljava/lang/StringBuilder;

    invoke-static {v3}, Ljava/lang/String;->valueOf(I)Ljava/lang/String;

    move-result-object v5

    invoke-direct {v4, v5}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    iget-object v4, p0, Lcom/konka/epg/ui/EpgRecordMenuActivity;->startMinuteValue:Landroid/widget/TextView;

    invoke-virtual {v4, v2}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    iget-object v4, p0, Lcom/konka/epg/ui/EpgRecordMenuActivity;->startMinuteValue:Landroid/widget/TextView;

    invoke-virtual {v4}, Landroid/widget/TextView;->invalidate()V

    return-void

    :cond_0
    const/16 v3, 0x3b

    goto :goto_0

    :cond_1
    const/16 v4, 0x3b

    if-ge v3, v4, :cond_2

    add-int/lit8 v3, v3, 0x1

    goto :goto_0

    :cond_2
    const/4 v3, 0x0

    goto :goto_0
.end method

.method protected onCreate(Landroid/os/Bundle;)V
    .locals 21
    .param p1    # Landroid/os/Bundle;

    invoke-super/range {p0 .. p1}, Landroid/app/Activity;->onCreate(Landroid/os/Bundle;)V

    const v17, 0x7f03000a    # com.konka.epg.R.layout.epg_record_menu

    move-object/from16 v0, p0

    move/from16 v1, v17

    invoke-virtual {v0, v1}, Lcom/konka/epg/ui/EpgRecordMenuActivity;->setContentView(I)V

    invoke-virtual/range {p0 .. p0}, Lcom/konka/epg/ui/EpgRecordMenuActivity;->getApplication()Landroid/app/Application;

    move-result-object v4

    check-cast v4, Lcom/konka/epg/TVRootApp;

    invoke-virtual {v4}, Lcom/konka/epg/TVRootApp;->getTvDeskProvider()Lcom/konka/kkinterface/tv/TvDeskProvider;

    move-result-object v14

    invoke-interface {v14}, Lcom/konka/kkinterface/tv/TvDeskProvider;->getChannelManagerInstance()Lcom/konka/kkinterface/tv/ChannelDesk;

    move-result-object v17

    sput-object v17, Lcom/konka/epg/ui/EpgRecordMenuActivity;->cd:Lcom/konka/kkinterface/tv/ChannelDesk;

    invoke-interface {v14}, Lcom/konka/kkinterface/tv/TvDeskProvider;->getEpgManagerInstance()Lcom/konka/kkinterface/tv/EpgDesk;

    move-result-object v17

    sput-object v17, Lcom/konka/epg/ui/EpgRecordMenuActivity;->ed:Lcom/konka/kkinterface/tv/EpgDesk;

    new-instance v17, Lcom/mstar/android/tvapi/common/vo/ProgramInfo;

    invoke-direct/range {v17 .. v17}, Lcom/mstar/android/tvapi/common/vo/ProgramInfo;-><init>()V

    move-object/from16 v0, v17

    move-object/from16 v1, p0

    iput-object v0, v1, Lcom/konka/epg/ui/EpgRecordMenuActivity;->curProgInfo:Lcom/mstar/android/tvapi/common/vo/ProgramInfo;

    new-instance v9, Lcom/mstar/android/tvapi/dtv/vo/EpgEventInfo;

    invoke-direct {v9}, Lcom/mstar/android/tvapi/dtv/vo/EpgEventInfo;-><init>()V

    new-instance v17, Lcom/mstar/android/tvapi/common/vo/EpgEventTimerInfo;

    invoke-direct/range {v17 .. v17}, Lcom/mstar/android/tvapi/common/vo/EpgEventTimerInfo;-><init>()V

    sput-object v17, Lcom/konka/epg/ui/EpgRecordMenuActivity;->eventTimerInfo:Lcom/mstar/android/tvapi/common/vo/EpgEventTimerInfo;

    invoke-virtual/range {p0 .. p0}, Lcom/konka/epg/ui/EpgRecordMenuActivity;->getIntent()Landroid/content/Intent;

    move-result-object v17

    if-eqz v17, :cond_0

    invoke-virtual/range {p0 .. p0}, Lcom/konka/epg/ui/EpgRecordMenuActivity;->getIntent()Landroid/content/Intent;

    move-result-object v17

    invoke-virtual/range {v17 .. v17}, Landroid/content/Intent;->getExtras()Landroid/os/Bundle;

    move-result-object v17

    if-eqz v17, :cond_0

    invoke-virtual/range {p0 .. p0}, Lcom/konka/epg/ui/EpgRecordMenuActivity;->getIntent()Landroid/content/Intent;

    move-result-object v17

    const-string v18, "EventBaseTime"

    const/16 v19, 0x0

    invoke-virtual/range {v17 .. v19}, Landroid/content/Intent;->getIntExtra(Ljava/lang/String;I)I

    move-result v17

    move/from16 v0, v17

    move-object/from16 v1, p0

    iput v0, v1, Lcom/konka/epg/ui/EpgRecordMenuActivity;->eventBaseTime:I

    invoke-virtual/range {p0 .. p0}, Lcom/konka/epg/ui/EpgRecordMenuActivity;->getIntent()Landroid/content/Intent;

    move-result-object v17

    const-string v18, "FocusIndex"

    const/16 v19, 0x0

    invoke-virtual/range {v17 .. v19}, Landroid/content/Intent;->getIntExtra(Ljava/lang/String;I)I

    move-result v17

    move/from16 v0, v17

    move-object/from16 v1, p0

    iput v0, v1, Lcom/konka/epg/ui/EpgRecordMenuActivity;->eventIdx:I

    invoke-virtual/range {p0 .. p0}, Lcom/konka/epg/ui/EpgRecordMenuActivity;->getIntent()Landroid/content/Intent;

    move-result-object v17

    const-string v18, "CurrentDayId"

    const/16 v19, 0x0

    invoke-virtual/range {v17 .. v19}, Landroid/content/Intent;->getIntExtra(Ljava/lang/String;I)I

    move-result v17

    move/from16 v0, v17

    move-object/from16 v1, p0

    iput v0, v1, Lcom/konka/epg/ui/EpgRecordMenuActivity;->eventDayIdx:I

    :cond_0
    sget-object v17, Lcom/konka/epg/ui/EpgRecordMenuActivity;->cd:Lcom/konka/kkinterface/tv/ChannelDesk;

    invoke-interface/range {v17 .. v17}, Lcom/konka/kkinterface/tv/ChannelDesk;->getCurrProgramInfo()Lcom/mstar/android/tvapi/common/vo/ProgramInfo;

    move-result-object v17

    move-object/from16 v0, v17

    move-object/from16 v1, p0

    iput-object v0, v1, Lcom/konka/epg/ui/EpgRecordMenuActivity;->curProgInfo:Lcom/mstar/android/tvapi/common/vo/ProgramInfo;

    sget-object v17, Lcom/konka/epg/ui/EpgRecordMenuActivity;->cd:Lcom/konka/kkinterface/tv/ChannelDesk;

    sget-object v18, Lcom/mstar/android/tvapi/common/vo/EnumProgramCountType;->E_COUNT_DTV:Lcom/mstar/android/tvapi/common/vo/EnumProgramCountType;

    invoke-interface/range {v17 .. v18}, Lcom/konka/kkinterface/tv/ChannelDesk;->getProgramCount(Lcom/mstar/android/tvapi/common/vo/EnumProgramCountType;)I

    move-result v12

    const/4 v10, 0x0

    :goto_0
    if-lt v10, v12, :cond_1

    :goto_1
    const v17, 0x7f0a0048    # com.konka.epg.R.id.record_channel_value

    move-object/from16 v0, p0

    move/from16 v1, v17

    invoke-virtual {v0, v1}, Lcom/konka/epg/ui/EpgRecordMenuActivity;->findViewById(I)Landroid/view/View;

    move-result-object v17

    check-cast v17, Landroid/widget/TextView;

    move-object/from16 v0, v17

    move-object/from16 v1, p0

    iput-object v0, v1, Lcom/konka/epg/ui/EpgRecordMenuActivity;->channelValue:Landroid/widget/TextView;

    const v17, 0x7f0a004b    # com.konka.epg.R.id.record_start_minute_value

    move-object/from16 v0, p0

    move/from16 v1, v17

    invoke-virtual {v0, v1}, Lcom/konka/epg/ui/EpgRecordMenuActivity;->findViewById(I)Landroid/view/View;

    move-result-object v17

    check-cast v17, Landroid/widget/TextView;

    move-object/from16 v0, v17

    move-object/from16 v1, p0

    iput-object v0, v1, Lcom/konka/epg/ui/EpgRecordMenuActivity;->startMinuteValue:Landroid/widget/TextView;

    const v17, 0x7f0a004e    # com.konka.epg.R.id.record_start_hour_value

    move-object/from16 v0, p0

    move/from16 v1, v17

    invoke-virtual {v0, v1}, Lcom/konka/epg/ui/EpgRecordMenuActivity;->findViewById(I)Landroid/view/View;

    move-result-object v17

    check-cast v17, Landroid/widget/TextView;

    move-object/from16 v0, v17

    move-object/from16 v1, p0

    iput-object v0, v1, Lcom/konka/epg/ui/EpgRecordMenuActivity;->startHourValue:Landroid/widget/TextView;

    const v17, 0x7f0a0051    # com.konka.epg.R.id.record_start_month_value

    move-object/from16 v0, p0

    move/from16 v1, v17

    invoke-virtual {v0, v1}, Lcom/konka/epg/ui/EpgRecordMenuActivity;->findViewById(I)Landroid/view/View;

    move-result-object v17

    check-cast v17, Landroid/widget/TextView;

    move-object/from16 v0, v17

    move-object/from16 v1, p0

    iput-object v0, v1, Lcom/konka/epg/ui/EpgRecordMenuActivity;->startMonthValue:Landroid/widget/TextView;

    const v17, 0x7f0a0054    # com.konka.epg.R.id.record_start_date_value

    move-object/from16 v0, p0

    move/from16 v1, v17

    invoke-virtual {v0, v1}, Lcom/konka/epg/ui/EpgRecordMenuActivity;->findViewById(I)Landroid/view/View;

    move-result-object v17

    check-cast v17, Landroid/widget/TextView;

    move-object/from16 v0, v17

    move-object/from16 v1, p0

    iput-object v0, v1, Lcom/konka/epg/ui/EpgRecordMenuActivity;->startDateValue:Landroid/widget/TextView;

    const v17, 0x7f0a0057    # com.konka.epg.R.id.record_end_minute_value

    move-object/from16 v0, p0

    move/from16 v1, v17

    invoke-virtual {v0, v1}, Lcom/konka/epg/ui/EpgRecordMenuActivity;->findViewById(I)Landroid/view/View;

    move-result-object v17

    check-cast v17, Landroid/widget/TextView;

    move-object/from16 v0, v17

    move-object/from16 v1, p0

    iput-object v0, v1, Lcom/konka/epg/ui/EpgRecordMenuActivity;->endMinuteValue:Landroid/widget/TextView;

    const v17, 0x7f0a005a    # com.konka.epg.R.id.record_end_hour_value

    move-object/from16 v0, p0

    move/from16 v1, v17

    invoke-virtual {v0, v1}, Lcom/konka/epg/ui/EpgRecordMenuActivity;->findViewById(I)Landroid/view/View;

    move-result-object v17

    check-cast v17, Landroid/widget/TextView;

    move-object/from16 v0, v17

    move-object/from16 v1, p0

    iput-object v0, v1, Lcom/konka/epg/ui/EpgRecordMenuActivity;->endHourValue:Landroid/widget/TextView;

    const v17, 0x7f0a005d    # com.konka.epg.R.id.record_end_month_value

    move-object/from16 v0, p0

    move/from16 v1, v17

    invoke-virtual {v0, v1}, Lcom/konka/epg/ui/EpgRecordMenuActivity;->findViewById(I)Landroid/view/View;

    move-result-object v17

    check-cast v17, Landroid/widget/TextView;

    move-object/from16 v0, v17

    move-object/from16 v1, p0

    iput-object v0, v1, Lcom/konka/epg/ui/EpgRecordMenuActivity;->endMonthValue:Landroid/widget/TextView;

    const v17, 0x7f0a0060    # com.konka.epg.R.id.record_end_date_value

    move-object/from16 v0, p0

    move/from16 v1, v17

    invoke-virtual {v0, v1}, Lcom/konka/epg/ui/EpgRecordMenuActivity;->findViewById(I)Landroid/view/View;

    move-result-object v17

    check-cast v17, Landroid/widget/TextView;

    move-object/from16 v0, v17

    move-object/from16 v1, p0

    iput-object v0, v1, Lcom/konka/epg/ui/EpgRecordMenuActivity;->endDateValue:Landroid/widget/TextView;

    const v17, 0x7f0a0062    # com.konka.epg.R.id.record_mode_value

    move-object/from16 v0, p0

    move/from16 v1, v17

    invoke-virtual {v0, v1}, Lcom/konka/epg/ui/EpgRecordMenuActivity;->findViewById(I)Landroid/view/View;

    move-result-object v17

    check-cast v17, Landroid/widget/TextView;

    move-object/from16 v0, v17

    move-object/from16 v1, p0

    iput-object v0, v1, Lcom/konka/epg/ui/EpgRecordMenuActivity;->modeValue:Landroid/widget/TextView;

    const v17, 0x7f0a0046    # com.konka.epg.R.id.record_service_name

    move-object/from16 v0, p0

    move/from16 v1, v17

    invoke-virtual {v0, v1}, Lcom/konka/epg/ui/EpgRecordMenuActivity;->findViewById(I)Landroid/view/View;

    move-result-object v17

    check-cast v17, Landroid/widget/LinearLayout;

    move-object/from16 v0, v17

    move-object/from16 v1, p0

    iput-object v0, v1, Lcom/konka/epg/ui/EpgRecordMenuActivity;->recordChannel:Landroid/widget/LinearLayout;

    const v17, 0x7f0a0049    # com.konka.epg.R.id.record_start_time_minute

    move-object/from16 v0, p0

    move/from16 v1, v17

    invoke-virtual {v0, v1}, Lcom/konka/epg/ui/EpgRecordMenuActivity;->findViewById(I)Landroid/view/View;

    move-result-object v17

    check-cast v17, Landroid/widget/LinearLayout;

    move-object/from16 v0, v17

    move-object/from16 v1, p0

    iput-object v0, v1, Lcom/konka/epg/ui/EpgRecordMenuActivity;->startMinute:Landroid/widget/LinearLayout;

    const v17, 0x7f0a004c    # com.konka.epg.R.id.record_start_time_hour

    move-object/from16 v0, p0

    move/from16 v1, v17

    invoke-virtual {v0, v1}, Lcom/konka/epg/ui/EpgRecordMenuActivity;->findViewById(I)Landroid/view/View;

    move-result-object v17

    check-cast v17, Landroid/widget/LinearLayout;

    move-object/from16 v0, v17

    move-object/from16 v1, p0

    iput-object v0, v1, Lcom/konka/epg/ui/EpgRecordMenuActivity;->startHour:Landroid/widget/LinearLayout;

    const v17, 0x7f0a004f    # com.konka.epg.R.id.record_start_time_month

    move-object/from16 v0, p0

    move/from16 v1, v17

    invoke-virtual {v0, v1}, Lcom/konka/epg/ui/EpgRecordMenuActivity;->findViewById(I)Landroid/view/View;

    move-result-object v17

    check-cast v17, Landroid/widget/LinearLayout;

    move-object/from16 v0, v17

    move-object/from16 v1, p0

    iput-object v0, v1, Lcom/konka/epg/ui/EpgRecordMenuActivity;->startMonth:Landroid/widget/LinearLayout;

    const v17, 0x7f0a0052    # com.konka.epg.R.id.record_start_time_date

    move-object/from16 v0, p0

    move/from16 v1, v17

    invoke-virtual {v0, v1}, Lcom/konka/epg/ui/EpgRecordMenuActivity;->findViewById(I)Landroid/view/View;

    move-result-object v17

    check-cast v17, Landroid/widget/LinearLayout;

    move-object/from16 v0, v17

    move-object/from16 v1, p0

    iput-object v0, v1, Lcom/konka/epg/ui/EpgRecordMenuActivity;->startDate:Landroid/widget/LinearLayout;

    const v17, 0x7f0a0055    # com.konka.epg.R.id.record_end_time_minute

    move-object/from16 v0, p0

    move/from16 v1, v17

    invoke-virtual {v0, v1}, Lcom/konka/epg/ui/EpgRecordMenuActivity;->findViewById(I)Landroid/view/View;

    move-result-object v17

    check-cast v17, Landroid/widget/LinearLayout;

    move-object/from16 v0, v17

    move-object/from16 v1, p0

    iput-object v0, v1, Lcom/konka/epg/ui/EpgRecordMenuActivity;->endMinute:Landroid/widget/LinearLayout;

    const v17, 0x7f0a0058    # com.konka.epg.R.id.record_end_time_hour

    move-object/from16 v0, p0

    move/from16 v1, v17

    invoke-virtual {v0, v1}, Lcom/konka/epg/ui/EpgRecordMenuActivity;->findViewById(I)Landroid/view/View;

    move-result-object v17

    check-cast v17, Landroid/widget/LinearLayout;

    move-object/from16 v0, v17

    move-object/from16 v1, p0

    iput-object v0, v1, Lcom/konka/epg/ui/EpgRecordMenuActivity;->endHour:Landroid/widget/LinearLayout;

    const v17, 0x7f0a005b    # com.konka.epg.R.id.record_end_time_month

    move-object/from16 v0, p0

    move/from16 v1, v17

    invoke-virtual {v0, v1}, Lcom/konka/epg/ui/EpgRecordMenuActivity;->findViewById(I)Landroid/view/View;

    move-result-object v17

    check-cast v17, Landroid/widget/LinearLayout;

    move-object/from16 v0, v17

    move-object/from16 v1, p0

    iput-object v0, v1, Lcom/konka/epg/ui/EpgRecordMenuActivity;->endMonth:Landroid/widget/LinearLayout;

    const v17, 0x7f0a005e    # com.konka.epg.R.id.record_end_time_date

    move-object/from16 v0, p0

    move/from16 v1, v17

    invoke-virtual {v0, v1}, Lcom/konka/epg/ui/EpgRecordMenuActivity;->findViewById(I)Landroid/view/View;

    move-result-object v17

    check-cast v17, Landroid/widget/LinearLayout;

    move-object/from16 v0, v17

    move-object/from16 v1, p0

    iput-object v0, v1, Lcom/konka/epg/ui/EpgRecordMenuActivity;->endDate:Landroid/widget/LinearLayout;

    const v17, 0x7f0a0061    # com.konka.epg.R.id.record_mode_value_layout

    move-object/from16 v0, p0

    move/from16 v1, v17

    invoke-virtual {v0, v1}, Lcom/konka/epg/ui/EpgRecordMenuActivity;->findViewById(I)Landroid/view/View;

    move-result-object v17

    check-cast v17, Landroid/widget/LinearLayout;

    move-object/from16 v0, v17

    move-object/from16 v1, p0

    iput-object v0, v1, Lcom/konka/epg/ui/EpgRecordMenuActivity;->recordMode:Landroid/widget/LinearLayout;

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/konka/epg/ui/EpgRecordMenuActivity;->startMinute:Landroid/widget/LinearLayout;

    move-object/from16 v17, v0

    const v18, 0x7f020028    # com.konka.epg.R.drawable.programme_epg_img_focus

    invoke-virtual/range {v17 .. v18}, Landroid/widget/LinearLayout;->setBackgroundResource(I)V

    invoke-virtual/range {p0 .. p0}, Lcom/konka/epg/ui/EpgRecordMenuActivity;->getResources()Landroid/content/res/Resources;

    move-result-object v17

    const v18, 0x7f090004    # com.konka.epg.R.array.epg_book_toast

    invoke-virtual/range {v17 .. v18}, Landroid/content/res/Resources;->getStringArray(I)[Ljava/lang/String;

    move-result-object v17

    move-object/from16 v0, v17

    move-object/from16 v1, p0

    iput-object v0, v1, Lcom/konka/epg/ui/EpgRecordMenuActivity;->bookToast:[Ljava/lang/String;

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/konka/epg/ui/EpgRecordMenuActivity;->channelValue:Landroid/widget/TextView;

    move-object/from16 v17, v0

    const/16 v18, 0x0

    invoke-virtual/range {v17 .. v18}, Landroid/widget/TextView;->setFocusable(Z)V

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/konka/epg/ui/EpgRecordMenuActivity;->startMonthValue:Landroid/widget/TextView;

    move-object/from16 v17, v0

    const/16 v18, 0x0

    invoke-virtual/range {v17 .. v18}, Landroid/widget/TextView;->setFocusable(Z)V

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/konka/epg/ui/EpgRecordMenuActivity;->startDateValue:Landroid/widget/TextView;

    move-object/from16 v17, v0

    const/16 v18, 0x0

    invoke-virtual/range {v17 .. v18}, Landroid/widget/TextView;->setFocusable(Z)V

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/konka/epg/ui/EpgRecordMenuActivity;->endMonthValue:Landroid/widget/TextView;

    move-object/from16 v17, v0

    const/16 v18, 0x0

    invoke-virtual/range {v17 .. v18}, Landroid/widget/TextView;->setFocusable(Z)V

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/konka/epg/ui/EpgRecordMenuActivity;->endDateValue:Landroid/widget/TextView;

    move-object/from16 v17, v0

    const/16 v18, 0x0

    invoke-virtual/range {v17 .. v18}, Landroid/widget/TextView;->setFocusable(Z)V

    new-instance v17, Ljava/lang/StringBuilder;

    const-string v18, "CH "

    invoke-direct/range {v17 .. v18}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/konka/epg/ui/EpgRecordMenuActivity;->curProgInfo:Lcom/mstar/android/tvapi/common/vo/ProgramInfo;

    move-object/from16 v18, v0

    move-object/from16 v0, v18

    iget v0, v0, Lcom/mstar/android/tvapi/common/vo/ProgramInfo;->number:I

    move/from16 v18, v0

    invoke-virtual/range {v17 .. v18}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v17

    const-string v18, " "

    invoke-virtual/range {v17 .. v18}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v17

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/konka/epg/ui/EpgRecordMenuActivity;->curProgInfo:Lcom/mstar/android/tvapi/common/vo/ProgramInfo;

    move-object/from16 v18, v0

    move-object/from16 v0, v18

    iget-object v0, v0, Lcom/mstar/android/tvapi/common/vo/ProgramInfo;->serviceName:Ljava/lang/String;

    move-object/from16 v18, v0

    invoke-virtual/range {v17 .. v18}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v17

    invoke-virtual/range {v17 .. v17}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v13

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/konka/epg/ui/EpgRecordMenuActivity;->channelValue:Landroid/widget/TextView;

    move-object/from16 v17, v0

    move-object/from16 v0, v17

    invoke-virtual {v0, v13}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    new-instance v5, Landroid/text/format/Time;

    invoke-direct {v5}, Landroid/text/format/Time;-><init>()V

    invoke-virtual {v5}, Landroid/text/format/Time;->setToNow()V

    const/16 v17, 0x1

    move/from16 v0, v17

    invoke-virtual {v5, v0}, Landroid/text/format/Time;->toMillis(Z)J

    move-object/from16 v0, p0

    iget v0, v0, Lcom/konka/epg/ui/EpgRecordMenuActivity;->eventBaseTime:I

    move/from16 v17, v0

    move/from16 v0, v17

    int-to-long v0, v0

    move-wide/from16 v17, v0

    const-wide/16 v19, 0x3e8

    mul-long v17, v17, v19

    const-wide/16 v19, 0x1

    add-long v17, v17, v19

    move-wide/from16 v0, v17

    invoke-virtual {v5, v0, v1}, Landroid/text/format/Time;->set(J)V

    :try_start_0
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/konka/epg/ui/EpgRecordMenuActivity;->eventInfoList:Ljava/util/ArrayList;

    move-object/from16 v17, v0

    invoke-virtual/range {v17 .. v17}, Ljava/util/ArrayList;->clear()V

    sget-object v17, Lcom/konka/epg/ui/EpgRecordMenuActivity;->ed:Lcom/konka/kkinterface/tv/EpgDesk;

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/konka/epg/ui/EpgRecordMenuActivity;->curProgInfo:Lcom/mstar/android/tvapi/common/vo/ProgramInfo;

    move-object/from16 v18, v0

    move-object/from16 v0, v18

    iget-short v0, v0, Lcom/mstar/android/tvapi/common/vo/ProgramInfo;->serviceType:S

    move/from16 v18, v0

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/konka/epg/ui/EpgRecordMenuActivity;->curProgInfo:Lcom/mstar/android/tvapi/common/vo/ProgramInfo;

    move-object/from16 v19, v0

    move-object/from16 v0, v19

    iget v0, v0, Lcom/mstar/android/tvapi/common/vo/ProgramInfo;->number:I

    move/from16 v19, v0

    const/16 v20, 0x1

    move-object/from16 v0, v17

    move/from16 v1, v18

    move/from16 v2, v19

    move/from16 v3, v20

    invoke-interface {v0, v1, v2, v5, v3}, Lcom/konka/kkinterface/tv/EpgDesk;->getEventInfo(SILandroid/text/format/Time;I)Ljava/util/ArrayList;

    move-result-object v17

    move-object/from16 v0, v17

    move-object/from16 v1, p0

    iput-object v0, v1, Lcom/konka/epg/ui/EpgRecordMenuActivity;->eventInfoList:Ljava/util/ArrayList;

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/konka/epg/ui/EpgRecordMenuActivity;->eventInfoList:Ljava/util/ArrayList;

    move-object/from16 v17, v0

    const/16 v18, 0x0

    invoke-virtual/range {v17 .. v18}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v17

    move-object/from16 v0, v17

    check-cast v0, Lcom/mstar/android/tvapi/dtv/vo/EpgEventInfo;

    move-object v9, v0
    :try_end_0
    .catch Lcom/mstar/android/tvapi/common/exception/TvCommonException; {:try_start_0 .. :try_end_0} :catch_0

    :goto_2
    sget-object v17, Lcom/konka/epg/ui/EpgRecordMenuActivity;->eventTimerInfo:Lcom/mstar/android/tvapi/common/vo/EpgEventTimerInfo;

    const/16 v18, 0x2

    move/from16 v0, v18

    move-object/from16 v1, v17

    iput-short v0, v1, Lcom/mstar/android/tvapi/common/vo/EpgEventTimerInfo;->enTimerType:S

    sget-object v17, Lcom/konka/epg/ui/EpgRecordMenuActivity;->eventTimerInfo:Lcom/mstar/android/tvapi/common/vo/EpgEventTimerInfo;

    const/16 v18, 0x81

    move/from16 v0, v18

    move-object/from16 v1, v17

    iput-short v0, v1, Lcom/mstar/android/tvapi/common/vo/EpgEventTimerInfo;->enRepeatMode:S

    sget-object v17, Ljava/lang/System;->out:Ljava/io/PrintStream;

    new-instance v18, Ljava/lang/StringBuilder;

    const-string v19, "\n======>>>EventInfo StartTime "

    invoke-direct/range {v18 .. v19}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    iget v0, v9, Lcom/mstar/android/tvapi/dtv/vo/EpgEventInfo;->durationTime:I

    move/from16 v19, v0

    invoke-virtual/range {v18 .. v19}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v18

    invoke-virtual/range {v18 .. v18}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v18

    invoke-virtual/range {v17 .. v18}, Ljava/io/PrintStream;->println(Ljava/lang/String;)V

    sget-object v17, Ljava/lang/System;->out:Ljava/io/PrintStream;

    new-instance v18, Ljava/lang/StringBuilder;

    const-string v19, "\n======>>>BaseTime StartTime "

    invoke-direct/range {v18 .. v19}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    move-object/from16 v0, p0

    iget v0, v0, Lcom/konka/epg/ui/EpgRecordMenuActivity;->eventBaseTime:I

    move/from16 v19, v0

    invoke-virtual/range {v18 .. v19}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v18

    invoke-virtual/range {v18 .. v18}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v18

    invoke-virtual/range {v17 .. v18}, Ljava/io/PrintStream;->println(Ljava/lang/String;)V

    sget-object v17, Lcom/konka/epg/ui/EpgRecordMenuActivity;->eventTimerInfo:Lcom/mstar/android/tvapi/common/vo/EpgEventTimerInfo;

    move-object/from16 v0, p0

    iget v0, v0, Lcom/konka/epg/ui/EpgRecordMenuActivity;->eventBaseTime:I

    move/from16 v18, v0

    move/from16 v0, v18

    move-object/from16 v1, v17

    iput v0, v1, Lcom/mstar/android/tvapi/common/vo/EpgEventTimerInfo;->startTime:I

    sget-object v17, Lcom/konka/epg/ui/EpgRecordMenuActivity;->eventTimerInfo:Lcom/mstar/android/tvapi/common/vo/EpgEventTimerInfo;

    iget v0, v9, Lcom/mstar/android/tvapi/dtv/vo/EpgEventInfo;->durationTime:I

    move/from16 v18, v0

    move/from16 v0, v18

    move-object/from16 v1, v17

    iput v0, v1, Lcom/mstar/android/tvapi/common/vo/EpgEventTimerInfo;->durationTime:I

    sget-object v17, Lcom/konka/epg/ui/EpgRecordMenuActivity;->eventTimerInfo:Lcom/mstar/android/tvapi/common/vo/EpgEventTimerInfo;

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/konka/epg/ui/EpgRecordMenuActivity;->curProgInfo:Lcom/mstar/android/tvapi/common/vo/ProgramInfo;

    move-object/from16 v18, v0

    move-object/from16 v0, v18

    iget-short v0, v0, Lcom/mstar/android/tvapi/common/vo/ProgramInfo;->serviceType:S

    move/from16 v18, v0

    move/from16 v0, v18

    move-object/from16 v1, v17

    iput v0, v1, Lcom/mstar/android/tvapi/common/vo/EpgEventTimerInfo;->serviceType:I

    sget-object v17, Lcom/konka/epg/ui/EpgRecordMenuActivity;->eventTimerInfo:Lcom/mstar/android/tvapi/common/vo/EpgEventTimerInfo;

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/konka/epg/ui/EpgRecordMenuActivity;->curProgInfo:Lcom/mstar/android/tvapi/common/vo/ProgramInfo;

    move-object/from16 v18, v0

    move-object/from16 v0, v18

    iget v0, v0, Lcom/mstar/android/tvapi/common/vo/ProgramInfo;->number:I

    move/from16 v18, v0

    move/from16 v0, v18

    move-object/from16 v1, v17

    iput v0, v1, Lcom/mstar/android/tvapi/common/vo/EpgEventTimerInfo;->serviceNumber:I

    sget-object v17, Lcom/konka/epg/ui/EpgRecordMenuActivity;->eventTimerInfo:Lcom/mstar/android/tvapi/common/vo/EpgEventTimerInfo;

    move-object/from16 v0, p0

    iget v0, v0, Lcom/konka/epg/ui/EpgRecordMenuActivity;->eventIdx:I

    move/from16 v18, v0

    move/from16 v0, v18

    move-object/from16 v1, v17

    iput v0, v1, Lcom/mstar/android/tvapi/common/vo/EpgEventTimerInfo;->eventID:I

    sget-object v17, Ljava/lang/System;->out:Ljava/io/PrintStream;

    new-instance v18, Ljava/lang/StringBuilder;

    const-string v19, "\n======>>>eventTimerInfo.durationTime "

    invoke-direct/range {v18 .. v19}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    sget-object v19, Lcom/konka/epg/ui/EpgRecordMenuActivity;->eventTimerInfo:Lcom/mstar/android/tvapi/common/vo/EpgEventTimerInfo;

    move-object/from16 v0, v19

    iget v0, v0, Lcom/mstar/android/tvapi/common/vo/EpgEventTimerInfo;->durationTime:I

    move/from16 v19, v0

    invoke-virtual/range {v18 .. v19}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v18

    invoke-virtual/range {v18 .. v18}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v18

    invoke-virtual/range {v17 .. v18}, Ljava/io/PrintStream;->println(Ljava/lang/String;)V

    const/16 v16, 0x0

    new-instance v15, Landroid/text/format/Time;

    invoke-direct {v15}, Landroid/text/format/Time;-><init>()V

    iget v0, v9, Lcom/mstar/android/tvapi/dtv/vo/EpgEventInfo;->startTime:I

    move/from16 v17, v0

    move/from16 v0, v17

    int-to-long v0, v0

    move-wide/from16 v17, v0

    const-wide/16 v19, 0x3e8

    mul-long v17, v17, v19

    move-wide/from16 v0, v17

    invoke-virtual {v15, v0, v1}, Landroid/text/format/Time;->set(J)V

    new-instance v17, Ljava/lang/StringBuilder;

    iget v0, v15, Landroid/text/format/Time;->minute:I

    move/from16 v18, v0

    invoke-static/range {v18 .. v18}, Ljava/lang/String;->valueOf(I)Ljava/lang/String;

    move-result-object v18

    invoke-direct/range {v17 .. v18}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual/range {v17 .. v17}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v16

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/konka/epg/ui/EpgRecordMenuActivity;->startMinuteValue:Landroid/widget/TextView;

    move-object/from16 v17, v0

    move-object/from16 v0, v17

    move-object/from16 v1, v16

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    new-instance v17, Ljava/lang/StringBuilder;

    iget v0, v15, Landroid/text/format/Time;->hour:I

    move/from16 v18, v0

    invoke-static/range {v18 .. v18}, Ljava/lang/String;->valueOf(I)Ljava/lang/String;

    move-result-object v18

    invoke-direct/range {v17 .. v18}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual/range {v17 .. v17}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v16

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/konka/epg/ui/EpgRecordMenuActivity;->startHourValue:Landroid/widget/TextView;

    move-object/from16 v17, v0

    move-object/from16 v0, v17

    move-object/from16 v1, v16

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    new-instance v17, Ljava/lang/StringBuilder;

    iget v0, v15, Landroid/text/format/Time;->month:I

    move/from16 v18, v0

    add-int/lit8 v18, v18, 0x1

    invoke-static/range {v18 .. v18}, Ljava/lang/String;->valueOf(I)Ljava/lang/String;

    move-result-object v18

    invoke-direct/range {v17 .. v18}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual/range {v17 .. v17}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v16

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/konka/epg/ui/EpgRecordMenuActivity;->startMonthValue:Landroid/widget/TextView;

    move-object/from16 v17, v0

    move-object/from16 v0, v17

    move-object/from16 v1, v16

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    new-instance v17, Ljava/lang/StringBuilder;

    iget v0, v15, Landroid/text/format/Time;->monthDay:I

    move/from16 v18, v0

    invoke-static/range {v18 .. v18}, Ljava/lang/String;->valueOf(I)Ljava/lang/String;

    move-result-object v18

    invoke-direct/range {v17 .. v18}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual/range {v17 .. v17}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v16

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/konka/epg/ui/EpgRecordMenuActivity;->startDateValue:Landroid/widget/TextView;

    move-object/from16 v17, v0

    move-object/from16 v0, v17

    move-object/from16 v1, v16

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    new-instance v8, Landroid/text/format/Time;

    invoke-direct {v8}, Landroid/text/format/Time;-><init>()V

    iget v0, v9, Lcom/mstar/android/tvapi/dtv/vo/EpgEventInfo;->startTime:I

    move/from16 v17, v0

    iget v0, v9, Lcom/mstar/android/tvapi/dtv/vo/EpgEventInfo;->durationTime:I

    move/from16 v18, v0

    add-int v17, v17, v18

    move/from16 v0, v17

    int-to-long v0, v0

    move-wide/from16 v17, v0

    const-wide/16 v19, 0x3e8

    mul-long v17, v17, v19

    move-wide/from16 v0, v17

    invoke-virtual {v8, v0, v1}, Landroid/text/format/Time;->set(J)V

    new-instance v17, Ljava/lang/StringBuilder;

    iget v0, v8, Landroid/text/format/Time;->minute:I

    move/from16 v18, v0

    invoke-static/range {v18 .. v18}, Ljava/lang/String;->valueOf(I)Ljava/lang/String;

    move-result-object v18

    invoke-direct/range {v17 .. v18}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual/range {v17 .. v17}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v16

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/konka/epg/ui/EpgRecordMenuActivity;->endMinuteValue:Landroid/widget/TextView;

    move-object/from16 v17, v0

    move-object/from16 v0, v17

    move-object/from16 v1, v16

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    new-instance v17, Ljava/lang/StringBuilder;

    iget v0, v8, Landroid/text/format/Time;->hour:I

    move/from16 v18, v0

    invoke-static/range {v18 .. v18}, Ljava/lang/String;->valueOf(I)Ljava/lang/String;

    move-result-object v18

    invoke-direct/range {v17 .. v18}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual/range {v17 .. v17}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v16

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/konka/epg/ui/EpgRecordMenuActivity;->endHourValue:Landroid/widget/TextView;

    move-object/from16 v17, v0

    move-object/from16 v0, v17

    move-object/from16 v1, v16

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    new-instance v17, Ljava/lang/StringBuilder;

    iget v0, v8, Landroid/text/format/Time;->month:I

    move/from16 v18, v0

    add-int/lit8 v18, v18, 0x1

    invoke-static/range {v18 .. v18}, Ljava/lang/String;->valueOf(I)Ljava/lang/String;

    move-result-object v18

    invoke-direct/range {v17 .. v18}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual/range {v17 .. v17}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v16

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/konka/epg/ui/EpgRecordMenuActivity;->endMonthValue:Landroid/widget/TextView;

    move-object/from16 v17, v0

    move-object/from16 v0, v17

    move-object/from16 v1, v16

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    new-instance v17, Ljava/lang/StringBuilder;

    iget v0, v8, Landroid/text/format/Time;->monthDay:I

    move/from16 v18, v0

    invoke-static/range {v18 .. v18}, Ljava/lang/String;->valueOf(I)Ljava/lang/String;

    move-result-object v18

    invoke-direct/range {v17 .. v18}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual/range {v17 .. v17}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v16

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/konka/epg/ui/EpgRecordMenuActivity;->endDateValue:Landroid/widget/TextView;

    move-object/from16 v17, v0

    move-object/from16 v0, v17

    move-object/from16 v1, v16

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    sget-object v17, Lcom/konka/epg/ui/EpgRecordMenuActivity;->eventTimerInfo:Lcom/mstar/android/tvapi/common/vo/EpgEventTimerInfo;

    move-object/from16 v0, v17

    iget-short v0, v0, Lcom/mstar/android/tvapi/common/vo/EpgEventTimerInfo;->enRepeatMode:S

    move/from16 v17, v0

    packed-switch v17, :pswitch_data_0

    :pswitch_0
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/konka/epg/ui/EpgRecordMenuActivity;->modeValue:Landroid/widget/TextView;

    move-object/from16 v17, v0

    const v18, 0x7f08002a    # com.konka.epg.R.string.epg_reminder_once_str

    invoke-virtual/range {v17 .. v18}, Landroid/widget/TextView;->setText(I)V

    :goto_3
    new-instance v6, Lcom/konka/epg/ui/EpgRecordMenuActivity$1;

    move-object/from16 v0, p0

    invoke-direct {v6, v0}, Lcom/konka/epg/ui/EpgRecordMenuActivity$1;-><init>(Lcom/konka/epg/ui/EpgRecordMenuActivity;)V

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/konka/epg/ui/EpgRecordMenuActivity;->channelValue:Landroid/widget/TextView;

    move-object/from16 v17, v0

    move-object/from16 v0, v17

    invoke-virtual {v0, v6}, Landroid/widget/TextView;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/konka/epg/ui/EpgRecordMenuActivity;->startMinuteValue:Landroid/widget/TextView;

    move-object/from16 v17, v0

    move-object/from16 v0, v17

    invoke-virtual {v0, v6}, Landroid/widget/TextView;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/konka/epg/ui/EpgRecordMenuActivity;->startHourValue:Landroid/widget/TextView;

    move-object/from16 v17, v0

    move-object/from16 v0, v17

    invoke-virtual {v0, v6}, Landroid/widget/TextView;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/konka/epg/ui/EpgRecordMenuActivity;->endMinuteValue:Landroid/widget/TextView;

    move-object/from16 v17, v0

    move-object/from16 v0, v17

    invoke-virtual {v0, v6}, Landroid/widget/TextView;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/konka/epg/ui/EpgRecordMenuActivity;->endHourValue:Landroid/widget/TextView;

    move-object/from16 v17, v0

    move-object/from16 v0, v17

    invoke-virtual {v0, v6}, Landroid/widget/TextView;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/konka/epg/ui/EpgRecordMenuActivity;->modeValue:Landroid/widget/TextView;

    move-object/from16 v17, v0

    move-object/from16 v0, v17

    invoke-virtual {v0, v6}, Landroid/widget/TextView;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    return-void

    :cond_1
    sget-object v17, Lcom/konka/epg/ui/EpgRecordMenuActivity;->cd:Lcom/konka/kkinterface/tv/ChannelDesk;

    move-object/from16 v0, v17

    invoke-interface {v0, v10}, Lcom/konka/kkinterface/tv/ChannelDesk;->getProgramInfoByIndex(I)Lcom/mstar/android/tvapi/common/vo/ProgramInfo;

    move-result-object v11

    iget v0, v11, Lcom/mstar/android/tvapi/common/vo/ProgramInfo;->number:I

    move/from16 v17, v0

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/konka/epg/ui/EpgRecordMenuActivity;->curProgInfo:Lcom/mstar/android/tvapi/common/vo/ProgramInfo;

    move-object/from16 v18, v0

    move-object/from16 v0, v18

    iget v0, v0, Lcom/mstar/android/tvapi/common/vo/ProgramInfo;->number:I

    move/from16 v18, v0

    move/from16 v0, v17

    move/from16 v1, v18

    if-ne v0, v1, :cond_2

    iget-short v0, v11, Lcom/mstar/android/tvapi/common/vo/ProgramInfo;->serviceType:S

    move/from16 v17, v0

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/konka/epg/ui/EpgRecordMenuActivity;->curProgInfo:Lcom/mstar/android/tvapi/common/vo/ProgramInfo;

    move-object/from16 v18, v0

    move-object/from16 v0, v18

    iget-short v0, v0, Lcom/mstar/android/tvapi/common/vo/ProgramInfo;->serviceType:S

    move/from16 v18, v0

    move/from16 v0, v17

    move/from16 v1, v18

    if-ne v0, v1, :cond_2

    sput v10, Lcom/konka/epg/ui/EpgRecordMenuActivity;->userProgramIndex:I

    goto/16 :goto_1

    :cond_2
    add-int/lit8 v10, v10, 0x1

    goto/16 :goto_0

    :catch_0
    move-exception v7

    invoke-virtual {v7}, Lcom/mstar/android/tvapi/common/exception/TvCommonException;->printStackTrace()V

    goto/16 :goto_2

    :pswitch_1
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/konka/epg/ui/EpgRecordMenuActivity;->modeValue:Landroid/widget/TextView;

    move-object/from16 v17, v0

    const v18, 0x7f08002a    # com.konka.epg.R.string.epg_reminder_once_str

    invoke-virtual/range {v17 .. v18}, Landroid/widget/TextView;->setText(I)V

    goto/16 :goto_3

    :pswitch_2
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/konka/epg/ui/EpgRecordMenuActivity;->modeValue:Landroid/widget/TextView;

    move-object/from16 v17, v0

    const v18, 0x7f08002b    # com.konka.epg.R.string.epg_reminder_daily_str

    invoke-virtual/range {v17 .. v18}, Landroid/widget/TextView;->setText(I)V

    goto/16 :goto_3

    :pswitch_3
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/konka/epg/ui/EpgRecordMenuActivity;->modeValue:Landroid/widget/TextView;

    move-object/from16 v17, v0

    const v18, 0x7f08002c    # com.konka.epg.R.string.epg_reminder_weekly_str

    invoke-virtual/range {v17 .. v18}, Landroid/widget/TextView;->setText(I)V

    goto/16 :goto_3

    nop

    :pswitch_data_0
    .packed-switch 0x7f
        :pswitch_2
        :pswitch_0
        :pswitch_1
        :pswitch_3
    .end packed-switch
.end method

.method public onKeyDown(ILandroid/view/KeyEvent;)Z
    .locals 9
    .param p1    # I
    .param p2    # Landroid/view/KeyEvent;

    const/4 v6, 0x0

    const/4 v4, 0x1

    const/4 v1, 0x0

    sparse-switch p1, :sswitch_data_0

    invoke-super {p0, p1, p2}, Landroid/app/Activity;->onKeyDown(ILandroid/view/KeyEvent;)Z

    move-result v4

    :cond_0
    :goto_0
    return v4

    :sswitch_0
    invoke-virtual {p0}, Lcom/konka/epg/ui/EpgRecordMenuActivity;->changeFocusDown()V

    goto :goto_0

    :sswitch_1
    invoke-virtual {p0}, Lcom/konka/epg/ui/EpgRecordMenuActivity;->changeFocusUp()V

    goto :goto_0

    :sswitch_2
    iget-object v5, p0, Lcom/konka/epg/ui/EpgRecordMenuActivity;->channelValue:Landroid/widget/TextView;

    invoke-virtual {v5}, Landroid/widget/TextView;->isFocused()Z

    move-result v5

    if-eqz v5, :cond_1

    invoke-virtual {p0, v4}, Lcom/konka/epg/ui/EpgRecordMenuActivity;->changeRecordProgramNumber(Z)V

    goto :goto_0

    :cond_1
    iget-object v5, p0, Lcom/konka/epg/ui/EpgRecordMenuActivity;->startMinuteValue:Landroid/widget/TextView;

    invoke-virtual {v5}, Landroid/widget/TextView;->isFocused()Z

    move-result v5

    if-eqz v5, :cond_2

    invoke-virtual {p0, v4}, Lcom/konka/epg/ui/EpgRecordMenuActivity;->changeRecordStartMin(Z)V

    goto :goto_0

    :cond_2
    iget-object v5, p0, Lcom/konka/epg/ui/EpgRecordMenuActivity;->startHourValue:Landroid/widget/TextView;

    invoke-virtual {v5}, Landroid/widget/TextView;->isFocused()Z

    move-result v5

    if-eqz v5, :cond_3

    invoke-virtual {p0, v4}, Lcom/konka/epg/ui/EpgRecordMenuActivity;->changeRecordStartHour(Z)V

    goto :goto_0

    :cond_3
    iget-object v5, p0, Lcom/konka/epg/ui/EpgRecordMenuActivity;->endMinuteValue:Landroid/widget/TextView;

    invoke-virtual {v5}, Landroid/widget/TextView;->isFocused()Z

    move-result v5

    if-eqz v5, :cond_4

    invoke-virtual {p0, v4}, Lcom/konka/epg/ui/EpgRecordMenuActivity;->changeRecordEndMin(Z)V

    goto :goto_0

    :cond_4
    iget-object v5, p0, Lcom/konka/epg/ui/EpgRecordMenuActivity;->endHourValue:Landroid/widget/TextView;

    invoke-virtual {v5}, Landroid/widget/TextView;->isFocused()Z

    move-result v5

    if-eqz v5, :cond_5

    invoke-virtual {p0, v4}, Lcom/konka/epg/ui/EpgRecordMenuActivity;->changeRecordEndHour(Z)V

    goto :goto_0

    :cond_5
    iget-object v5, p0, Lcom/konka/epg/ui/EpgRecordMenuActivity;->modeValue:Landroid/widget/TextView;

    invoke-virtual {v5}, Landroid/widget/TextView;->isFocused()Z

    move-result v5

    if-eqz v5, :cond_0

    invoke-virtual {p0, v4}, Lcom/konka/epg/ui/EpgRecordMenuActivity;->changeRecordMode(Z)V

    goto :goto_0

    :sswitch_3
    iget-object v5, p0, Lcom/konka/epg/ui/EpgRecordMenuActivity;->channelValue:Landroid/widget/TextView;

    invoke-virtual {v5}, Landroid/widget/TextView;->isFocused()Z

    move-result v5

    if-eqz v5, :cond_6

    invoke-virtual {p0, v6}, Lcom/konka/epg/ui/EpgRecordMenuActivity;->changeRecordProgramNumber(Z)V

    goto :goto_0

    :cond_6
    iget-object v5, p0, Lcom/konka/epg/ui/EpgRecordMenuActivity;->startMinuteValue:Landroid/widget/TextView;

    invoke-virtual {v5}, Landroid/widget/TextView;->isFocused()Z

    move-result v5

    if-eqz v5, :cond_7

    invoke-virtual {p0, v6}, Lcom/konka/epg/ui/EpgRecordMenuActivity;->changeRecordStartMin(Z)V

    goto :goto_0

    :cond_7
    iget-object v5, p0, Lcom/konka/epg/ui/EpgRecordMenuActivity;->startHourValue:Landroid/widget/TextView;

    invoke-virtual {v5}, Landroid/widget/TextView;->isFocused()Z

    move-result v5

    if-eqz v5, :cond_8

    invoke-virtual {p0, v6}, Lcom/konka/epg/ui/EpgRecordMenuActivity;->changeRecordStartHour(Z)V

    goto :goto_0

    :cond_8
    iget-object v5, p0, Lcom/konka/epg/ui/EpgRecordMenuActivity;->endMinuteValue:Landroid/widget/TextView;

    invoke-virtual {v5}, Landroid/widget/TextView;->isFocused()Z

    move-result v5

    if-eqz v5, :cond_9

    invoke-virtual {p0, v6}, Lcom/konka/epg/ui/EpgRecordMenuActivity;->changeRecordEndMin(Z)V

    goto :goto_0

    :cond_9
    iget-object v5, p0, Lcom/konka/epg/ui/EpgRecordMenuActivity;->endHourValue:Landroid/widget/TextView;

    invoke-virtual {v5}, Landroid/widget/TextView;->isFocused()Z

    move-result v5

    if-eqz v5, :cond_a

    invoke-virtual {p0, v6}, Lcom/konka/epg/ui/EpgRecordMenuActivity;->changeRecordEndHour(Z)V

    goto/16 :goto_0

    :cond_a
    iget-object v5, p0, Lcom/konka/epg/ui/EpgRecordMenuActivity;->modeValue:Landroid/widget/TextView;

    invoke-virtual {v5}, Landroid/widget/TextView;->isFocused()Z

    move-result v5

    if-eqz v5, :cond_0

    invoke-virtual {p0, v6}, Lcom/konka/epg/ui/EpgRecordMenuActivity;->changeRecordMode(Z)V

    goto/16 :goto_0

    :sswitch_4
    new-instance v1, Landroid/content/Intent;

    const-class v5, Lcom/konka/epg/ui/EpgMainMenuActivity;

    invoke-direct {v1, p0, v5}, Landroid/content/Intent;-><init>(Landroid/content/Context;Ljava/lang/Class;)V

    const-class v5, Lcom/konka/epg/ui/EpgMainMenuActivity;

    invoke-virtual {v1, p0, v5}, Landroid/content/Intent;->setClass(Landroid/content/Context;Ljava/lang/Class;)Landroid/content/Intent;

    const-string v5, "FocusIndex"

    iget v6, p0, Lcom/konka/epg/ui/EpgRecordMenuActivity;->eventIdx:I

    invoke-virtual {v1, v5, v6}, Landroid/content/Intent;->putExtra(Ljava/lang/String;I)Landroid/content/Intent;

    invoke-virtual {p0, v1}, Lcom/konka/epg/ui/EpgRecordMenuActivity;->startActivity(Landroid/content/Intent;)V

    invoke-virtual {p0}, Lcom/konka/epg/ui/EpgRecordMenuActivity;->finish()V

    goto/16 :goto_0

    :sswitch_5
    sget-object v5, Ljava/lang/System;->out:Ljava/io/PrintStream;

    const-string v6, "\n###########addEpgEvent recorder KEYCODE_ENTER\n"

    invoke-virtual {v5, v6}, Ljava/io/PrintStream;->println(Ljava/lang/String;)V

    :try_start_0
    sget-object v5, Lcom/konka/epg/ui/EpgRecordMenuActivity;->eventTimerInfo:Lcom/mstar/android/tvapi/common/vo/EpgEventTimerInfo;

    iget v6, v5, Lcom/mstar/android/tvapi/common/vo/EpgEventTimerInfo;->startTime:I

    iget-wide v7, p0, Lcom/konka/epg/ui/EpgRecordMenuActivity;->offsetTimeInS:J

    long-to-int v7, v7

    sub-int/2addr v6, v7

    iput v6, v5, Lcom/mstar/android/tvapi/common/vo/EpgEventTimerInfo;->startTime:I

    sget-object v5, Lcom/konka/epg/ui/EpgRecordMenuActivity;->ed:Lcom/konka/kkinterface/tv/EpgDesk;

    sget-object v6, Lcom/konka/epg/ui/EpgRecordMenuActivity;->eventTimerInfo:Lcom/mstar/android/tvapi/common/vo/EpgEventTimerInfo;

    invoke-interface {v5, v6}, Lcom/konka/kkinterface/tv/EpgDesk;->addEpgEvent(Lcom/mstar/android/tvapi/common/vo/EpgEventTimerInfo;)Lcom/mstar/android/tvapi/dtv/vo/EnumEpgTimerCheck;

    move-result-object v2

    const-string v5, "addEpgEvent"

    new-instance v6, Ljava/lang/StringBuilder;

    const-string v7, "===========>>>> status is "

    invoke-direct {v6, v7}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v6, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v6}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v6

    invoke-static {v5, v6}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    const-string v5, "addEpgEvent"

    new-instance v6, Ljava/lang/StringBuilder;

    const-string v7, "========>>> eventTimerInfo startTime["

    invoke-direct {v6, v7}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    sget-object v7, Lcom/konka/epg/ui/EpgRecordMenuActivity;->eventTimerInfo:Lcom/mstar/android/tvapi/common/vo/EpgEventTimerInfo;

    iget v7, v7, Lcom/mstar/android/tvapi/common/vo/EpgEventTimerInfo;->startTime:I

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v6

    const-string v7, "],durationTime["

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    sget-object v7, Lcom/konka/epg/ui/EpgRecordMenuActivity;->eventTimerInfo:Lcom/mstar/android/tvapi/common/vo/EpgEventTimerInfo;

    iget v7, v7, Lcom/mstar/android/tvapi/common/vo/EpgEventTimerInfo;->durationTime:I

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v6

    const-string v7, "]"

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v6}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v6

    invoke-static {v5, v6}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    iget-object v5, p0, Lcom/konka/epg/ui/EpgRecordMenuActivity;->bookToast:[Ljava/lang/String;

    invoke-virtual {v2}, Lcom/mstar/android/tvapi/dtv/vo/EnumEpgTimerCheck;->ordinal()I

    move-result v6

    aget-object v5, v5, v6

    const/16 v6, 0x1e

    invoke-static {p0, v5, v6}, Landroid/widget/Toast;->makeText(Landroid/content/Context;Ljava/lang/CharSequence;I)Landroid/widget/Toast;

    move-result-object v3

    invoke-virtual {v3}, Landroid/widget/Toast;->show()V
    :try_end_0
    .catch Lcom/mstar/android/tvapi/common/exception/TvCommonException; {:try_start_0 .. :try_end_0} :catch_0

    :goto_1
    new-instance v1, Landroid/content/Intent;

    const-class v5, Lcom/konka/epg/ui/EpgMainMenuActivity;

    invoke-direct {v1, p0, v5}, Landroid/content/Intent;-><init>(Landroid/content/Context;Ljava/lang/Class;)V

    const-class v5, Lcom/konka/epg/ui/EpgMainMenuActivity;

    invoke-virtual {v1, p0, v5}, Landroid/content/Intent;->setClass(Landroid/content/Context;Ljava/lang/Class;)Landroid/content/Intent;

    const-string v5, "FocusIndex"

    iget v6, p0, Lcom/konka/epg/ui/EpgRecordMenuActivity;->eventIdx:I

    invoke-virtual {v1, v5, v6}, Landroid/content/Intent;->putExtra(Ljava/lang/String;I)Landroid/content/Intent;

    invoke-virtual {p0, v1}, Lcom/konka/epg/ui/EpgRecordMenuActivity;->startActivity(Landroid/content/Intent;)V

    invoke-virtual {p0}, Lcom/konka/epg/ui/EpgRecordMenuActivity;->finish()V

    goto/16 :goto_0

    :catch_0
    move-exception v0

    invoke-virtual {v0}, Lcom/mstar/android/tvapi/common/exception/TvCommonException;->printStackTrace()V

    goto :goto_1

    :sswitch_data_0
    .sparse-switch
        0x4 -> :sswitch_4
        0x13 -> :sswitch_1
        0x14 -> :sswitch_0
        0x15 -> :sswitch_2
        0x16 -> :sswitch_3
        0x42 -> :sswitch_5
        0xb7 -> :sswitch_4
    .end sparse-switch
.end method
