.class Lcom/konka/epg/ui/EpgMainMenuViewHolder$3;
.super Ljava/util/TimerTask;
.source "EpgMainMenuViewHolder.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/konka/epg/ui/EpgMainMenuViewHolder;->startRefreshFirstDay()V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/konka/epg/ui/EpgMainMenuViewHolder;


# direct methods
.method constructor <init>(Lcom/konka/epg/ui/EpgMainMenuViewHolder;)V
    .locals 0

    iput-object p1, p0, Lcom/konka/epg/ui/EpgMainMenuViewHolder$3;->this$0:Lcom/konka/epg/ui/EpgMainMenuViewHolder;

    invoke-direct {p0}, Ljava/util/TimerTask;-><init>()V

    return-void
.end method


# virtual methods
.method public run()V
    .locals 2

    iget-object v0, p0, Lcom/konka/epg/ui/EpgMainMenuViewHolder$3;->this$0:Lcom/konka/epg/ui/EpgMainMenuViewHolder;

    invoke-virtual {v0}, Lcom/konka/epg/ui/EpgMainMenuViewHolder;->getIsActive()Z

    move-result v0

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/konka/epg/ui/EpgMainMenuViewHolder$3;->this$0:Lcom/konka/epg/ui/EpgMainMenuViewHolder;

    invoke-virtual {v0}, Lcom/konka/epg/ui/EpgMainMenuViewHolder;->getIsSignalTabled()Z

    move-result v0

    if-eqz v0, :cond_0

    const-string v0, "refresh first day in timertask"

    invoke-static {v0}, Lcom/konka/epg/util/EpgDebugPrint;->Debug(Ljava/lang/String;)V

    iget-object v0, p0, Lcom/konka/epg/ui/EpgMainMenuViewHolder$3;->this$0:Lcom/konka/epg/ui/EpgMainMenuViewHolder;

    sget-object v1, Lcom/konka/epg/ui/EpgMainMenuActivity;->currentServiceType:Lcom/mstar/android/tvapi/common/vo/EnumServiceType;

    invoke-virtual {v0, v1}, Lcom/konka/epg/ui/EpgMainMenuViewHolder;->refreshFirstDayEvent(Lcom/mstar/android/tvapi/common/vo/EnumServiceType;)V

    :cond_0
    return-void
.end method
