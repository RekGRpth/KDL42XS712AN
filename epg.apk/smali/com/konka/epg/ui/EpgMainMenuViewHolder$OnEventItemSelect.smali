.class Lcom/konka/epg/ui/EpgMainMenuViewHolder$OnEventItemSelect;
.super Ljava/lang/Object;
.source "EpgMainMenuViewHolder.java"

# interfaces
.implements Landroid/widget/AdapterView$OnItemSelectedListener;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/konka/epg/ui/EpgMainMenuViewHolder;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x2
    name = "OnEventItemSelect"
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Object;",
        "Landroid/widget/AdapterView$OnItemSelectedListener;"
    }
.end annotation


# instance fields
.field final synthetic this$0:Lcom/konka/epg/ui/EpgMainMenuViewHolder;


# direct methods
.method private constructor <init>(Lcom/konka/epg/ui/EpgMainMenuViewHolder;)V
    .locals 0

    iput-object p1, p0, Lcom/konka/epg/ui/EpgMainMenuViewHolder$OnEventItemSelect;->this$0:Lcom/konka/epg/ui/EpgMainMenuViewHolder;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method synthetic constructor <init>(Lcom/konka/epg/ui/EpgMainMenuViewHolder;Lcom/konka/epg/ui/EpgMainMenuViewHolder$OnEventItemSelect;)V
    .locals 0

    invoke-direct {p0, p1}, Lcom/konka/epg/ui/EpgMainMenuViewHolder$OnEventItemSelect;-><init>(Lcom/konka/epg/ui/EpgMainMenuViewHolder;)V

    return-void
.end method


# virtual methods
.method public onItemSelected(Landroid/widget/AdapterView;Landroid/view/View;IJ)V
    .locals 4
    .param p2    # Landroid/view/View;
    .param p3    # I
    .param p4    # J
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Landroid/widget/AdapterView",
            "<*>;",
            "Landroid/view/View;",
            "IJ)V"
        }
    .end annotation

    const/4 v3, 0x0

    iget-object v1, p0, Lcom/konka/epg/ui/EpgMainMenuViewHolder$OnEventItemSelect;->this$0:Lcom/konka/epg/ui/EpgMainMenuViewHolder;

    invoke-static {v1, p3}, Lcom/konka/epg/ui/EpgMainMenuViewHolder;->access$6(Lcom/konka/epg/ui/EpgMainMenuViewHolder;I)V

    iget-object v1, p0, Lcom/konka/epg/ui/EpgMainMenuViewHolder$OnEventItemSelect;->this$0:Lcom/konka/epg/ui/EpgMainMenuViewHolder;

    # getter for: Lcom/konka/epg/ui/EpgMainMenuViewHolder;->m_WeekEventAdapterList:Ljava/util/ArrayList;
    invoke-static {v1}, Lcom/konka/epg/ui/EpgMainMenuViewHolder;->access$7(Lcom/konka/epg/ui/EpgMainMenuViewHolder;)Ljava/util/ArrayList;

    move-result-object v1

    iget-object v2, p0, Lcom/konka/epg/ui/EpgMainMenuViewHolder$OnEventItemSelect;->this$0:Lcom/konka/epg/ui/EpgMainMenuViewHolder;

    # getter for: Lcom/konka/epg/ui/EpgMainMenuViewHolder;->iCurrentDayId:I
    invoke-static {v2}, Lcom/konka/epg/ui/EpgMainMenuViewHolder;->access$4(Lcom/konka/epg/ui/EpgMainMenuViewHolder;)I

    move-result v2

    invoke-virtual {v1, v2}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/konka/epg/adapter/EventListAdapter;

    iget-object v2, p0, Lcom/konka/epg/ui/EpgMainMenuViewHolder$OnEventItemSelect;->this$0:Lcom/konka/epg/ui/EpgMainMenuViewHolder;

    # getter for: Lcom/konka/epg/ui/EpgMainMenuViewHolder;->iPosInEventList:I
    invoke-static {v2}, Lcom/konka/epg/ui/EpgMainMenuViewHolder;->access$8(Lcom/konka/epg/ui/EpgMainMenuViewHolder;)I

    move-result v2

    invoke-virtual {v1, v2}, Lcom/konka/epg/adapter/EventListAdapter;->getItem(I)Lcom/konka/kkimplements/tv/vo/KonkaEventInfo;

    move-result-object v0

    iget-object v1, p0, Lcom/konka/epg/ui/EpgMainMenuViewHolder$OnEventItemSelect;->this$0:Lcom/konka/epg/ui/EpgMainMenuViewHolder;

    # getter for: Lcom/konka/epg/ui/EpgMainMenuViewHolder;->iCurrentDayId:I
    invoke-static {v1}, Lcom/konka/epg/ui/EpgMainMenuViewHolder;->access$4(Lcom/konka/epg/ui/EpgMainMenuViewHolder;)I

    move-result v1

    if-nez v1, :cond_0

    if-eqz p3, :cond_1

    iget-object v1, p0, Lcom/konka/epg/ui/EpgMainMenuViewHolder$OnEventItemSelect;->this$0:Lcom/konka/epg/ui/EpgMainMenuViewHolder;

    # getter for: Lcom/konka/epg/ui/EpgMainMenuViewHolder;->m_WeekEventAdapterList:Ljava/util/ArrayList;
    invoke-static {v1}, Lcom/konka/epg/ui/EpgMainMenuViewHolder;->access$7(Lcom/konka/epg/ui/EpgMainMenuViewHolder;)Ljava/util/ArrayList;

    move-result-object v1

    invoke-virtual {v1, v3}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/konka/epg/adapter/EventListAdapter;

    invoke-virtual {v1, v3}, Lcom/konka/epg/adapter/EventListAdapter;->setFocus(Z)V

    iget-object v1, p0, Lcom/konka/epg/ui/EpgMainMenuViewHolder$OnEventItemSelect;->this$0:Lcom/konka/epg/ui/EpgMainMenuViewHolder;

    # getter for: Lcom/konka/epg/ui/EpgMainMenuViewHolder;->m_WeekEventAdapterList:Ljava/util/ArrayList;
    invoke-static {v1}, Lcom/konka/epg/ui/EpgMainMenuViewHolder;->access$7(Lcom/konka/epg/ui/EpgMainMenuViewHolder;)Ljava/util/ArrayList;

    move-result-object v1

    invoke-virtual {v1, v3}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/konka/epg/adapter/EventListAdapter;

    invoke-virtual {v1}, Lcom/konka/epg/adapter/EventListAdapter;->notifyDataSetChanged()V

    :cond_0
    :goto_0
    return-void

    :cond_1
    iget-object v1, p0, Lcom/konka/epg/ui/EpgMainMenuViewHolder$OnEventItemSelect;->this$0:Lcom/konka/epg/ui/EpgMainMenuViewHolder;

    # getter for: Lcom/konka/epg/ui/EpgMainMenuViewHolder;->m_WeekEventAdapterList:Ljava/util/ArrayList;
    invoke-static {v1}, Lcom/konka/epg/ui/EpgMainMenuViewHolder;->access$7(Lcom/konka/epg/ui/EpgMainMenuViewHolder;)Ljava/util/ArrayList;

    move-result-object v1

    invoke-virtual {v1, v3}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/konka/epg/adapter/EventListAdapter;

    const/4 v2, 0x1

    invoke-virtual {v1, v2}, Lcom/konka/epg/adapter/EventListAdapter;->setFocus(Z)V

    iget-object v1, p0, Lcom/konka/epg/ui/EpgMainMenuViewHolder$OnEventItemSelect;->this$0:Lcom/konka/epg/ui/EpgMainMenuViewHolder;

    # getter for: Lcom/konka/epg/ui/EpgMainMenuViewHolder;->m_WeekEventAdapterList:Ljava/util/ArrayList;
    invoke-static {v1}, Lcom/konka/epg/ui/EpgMainMenuViewHolder;->access$7(Lcom/konka/epg/ui/EpgMainMenuViewHolder;)Ljava/util/ArrayList;

    move-result-object v1

    invoke-virtual {v1, v3}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/konka/epg/adapter/EventListAdapter;

    invoke-virtual {v1}, Lcom/konka/epg/adapter/EventListAdapter;->notifyDataSetChanged()V

    goto :goto_0
.end method

.method public onNothingSelected(Landroid/widget/AdapterView;)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Landroid/widget/AdapterView",
            "<*>;)V"
        }
    .end annotation

    return-void
.end method
