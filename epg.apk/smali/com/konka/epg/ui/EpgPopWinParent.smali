.class public abstract Lcom/konka/epg/ui/EpgPopWinParent;
.super Ljava/lang/Object;
.source "EpgPopWinParent.java"


# instance fields
.field protected activity:Landroid/app/Activity;

.field protected epgMainMenuViewHolder:Lcom/konka/epg/ui/EpgMainMenuViewHolder;

.field protected epgServiceProvider:Lcom/konka/epg/service/EpgServiceProvider;

.field private iResID:I

.field protected lastFocusPos:Lcom/konka/epg/ui/EpgMainMenuActivity$EnumFocusPosition;

.field protected m_CancelBtn:Landroid/widget/Button;

.field protected m_ConfirmBtn:Landroid/widget/Button;

.field protected m_ContentText:Landroid/widget/TextView;

.field protected m_ContentView:Landroid/view/View;

.field protected m_Context:Landroid/content/Context;

.field protected m_Inflater:Landroid/view/LayoutInflater;

.field protected m_Parent:Landroid/view/View;

.field protected m_PopupWindow:Landroid/widget/PopupWindow;

.field protected m_TitleText:Landroid/widget/TextView;


# direct methods
.method protected constructor <init>(Landroid/app/Activity;Landroid/view/View;Landroid/content/Context;)V
    .locals 2
    .param p1    # Landroid/app/Activity;
    .param p2    # Landroid/view/View;
    .param p3    # Landroid/content/Context;

    const/4 v1, 0x0

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    const/4 v0, 0x0

    iput v0, p0, Lcom/konka/epg/ui/EpgPopWinParent;->iResID:I

    iput-object v1, p0, Lcom/konka/epg/ui/EpgPopWinParent;->m_Parent:Landroid/view/View;

    iput-object v1, p0, Lcom/konka/epg/ui/EpgPopWinParent;->m_ContentView:Landroid/view/View;

    iput-object v1, p0, Lcom/konka/epg/ui/EpgPopWinParent;->m_Context:Landroid/content/Context;

    iput-object v1, p0, Lcom/konka/epg/ui/EpgPopWinParent;->m_PopupWindow:Landroid/widget/PopupWindow;

    iput-object v1, p0, Lcom/konka/epg/ui/EpgPopWinParent;->m_Inflater:Landroid/view/LayoutInflater;

    iput-object v1, p0, Lcom/konka/epg/ui/EpgPopWinParent;->m_TitleText:Landroid/widget/TextView;

    iput-object v1, p0, Lcom/konka/epg/ui/EpgPopWinParent;->m_ContentText:Landroid/widget/TextView;

    iput-object v1, p0, Lcom/konka/epg/ui/EpgPopWinParent;->m_ConfirmBtn:Landroid/widget/Button;

    iput-object v1, p0, Lcom/konka/epg/ui/EpgPopWinParent;->m_CancelBtn:Landroid/widget/Button;

    iput-object v1, p0, Lcom/konka/epg/ui/EpgPopWinParent;->activity:Landroid/app/Activity;

    iput-object v1, p0, Lcom/konka/epg/ui/EpgPopWinParent;->epgMainMenuViewHolder:Lcom/konka/epg/ui/EpgMainMenuViewHolder;

    iput-object v1, p0, Lcom/konka/epg/ui/EpgPopWinParent;->epgServiceProvider:Lcom/konka/epg/service/EpgServiceProvider;

    iput-object v1, p0, Lcom/konka/epg/ui/EpgPopWinParent;->lastFocusPos:Lcom/konka/epg/ui/EpgMainMenuActivity$EnumFocusPosition;

    iput-object p1, p0, Lcom/konka/epg/ui/EpgPopWinParent;->activity:Landroid/app/Activity;

    iput-object p2, p0, Lcom/konka/epg/ui/EpgPopWinParent;->m_Parent:Landroid/view/View;

    iput-object p3, p0, Lcom/konka/epg/ui/EpgPopWinParent;->m_Context:Landroid/content/Context;

    return-void
.end method

.method protected constructor <init>(Landroid/app/Activity;Landroid/view/View;Landroid/content/Context;Lcom/konka/epg/ui/EpgMainMenuViewHolder;)V
    .locals 2
    .param p1    # Landroid/app/Activity;
    .param p2    # Landroid/view/View;
    .param p3    # Landroid/content/Context;
    .param p4    # Lcom/konka/epg/ui/EpgMainMenuViewHolder;

    const/4 v1, 0x0

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    const/4 v0, 0x0

    iput v0, p0, Lcom/konka/epg/ui/EpgPopWinParent;->iResID:I

    iput-object v1, p0, Lcom/konka/epg/ui/EpgPopWinParent;->m_Parent:Landroid/view/View;

    iput-object v1, p0, Lcom/konka/epg/ui/EpgPopWinParent;->m_ContentView:Landroid/view/View;

    iput-object v1, p0, Lcom/konka/epg/ui/EpgPopWinParent;->m_Context:Landroid/content/Context;

    iput-object v1, p0, Lcom/konka/epg/ui/EpgPopWinParent;->m_PopupWindow:Landroid/widget/PopupWindow;

    iput-object v1, p0, Lcom/konka/epg/ui/EpgPopWinParent;->m_Inflater:Landroid/view/LayoutInflater;

    iput-object v1, p0, Lcom/konka/epg/ui/EpgPopWinParent;->m_TitleText:Landroid/widget/TextView;

    iput-object v1, p0, Lcom/konka/epg/ui/EpgPopWinParent;->m_ContentText:Landroid/widget/TextView;

    iput-object v1, p0, Lcom/konka/epg/ui/EpgPopWinParent;->m_ConfirmBtn:Landroid/widget/Button;

    iput-object v1, p0, Lcom/konka/epg/ui/EpgPopWinParent;->m_CancelBtn:Landroid/widget/Button;

    iput-object v1, p0, Lcom/konka/epg/ui/EpgPopWinParent;->activity:Landroid/app/Activity;

    iput-object v1, p0, Lcom/konka/epg/ui/EpgPopWinParent;->epgMainMenuViewHolder:Lcom/konka/epg/ui/EpgMainMenuViewHolder;

    iput-object v1, p0, Lcom/konka/epg/ui/EpgPopWinParent;->epgServiceProvider:Lcom/konka/epg/service/EpgServiceProvider;

    iput-object v1, p0, Lcom/konka/epg/ui/EpgPopWinParent;->lastFocusPos:Lcom/konka/epg/ui/EpgMainMenuActivity$EnumFocusPosition;

    iput-object p1, p0, Lcom/konka/epg/ui/EpgPopWinParent;->activity:Landroid/app/Activity;

    iput-object p2, p0, Lcom/konka/epg/ui/EpgPopWinParent;->m_Parent:Landroid/view/View;

    iput-object p3, p0, Lcom/konka/epg/ui/EpgPopWinParent;->m_Context:Landroid/content/Context;

    iput-object p4, p0, Lcom/konka/epg/ui/EpgPopWinParent;->epgMainMenuViewHolder:Lcom/konka/epg/ui/EpgMainMenuViewHolder;

    return-void
.end method

.method protected constructor <init>(Landroid/view/View;Landroid/content/Context;)V
    .locals 2
    .param p1    # Landroid/view/View;
    .param p2    # Landroid/content/Context;

    const/4 v1, 0x0

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    const/4 v0, 0x0

    iput v0, p0, Lcom/konka/epg/ui/EpgPopWinParent;->iResID:I

    iput-object v1, p0, Lcom/konka/epg/ui/EpgPopWinParent;->m_Parent:Landroid/view/View;

    iput-object v1, p0, Lcom/konka/epg/ui/EpgPopWinParent;->m_ContentView:Landroid/view/View;

    iput-object v1, p0, Lcom/konka/epg/ui/EpgPopWinParent;->m_Context:Landroid/content/Context;

    iput-object v1, p0, Lcom/konka/epg/ui/EpgPopWinParent;->m_PopupWindow:Landroid/widget/PopupWindow;

    iput-object v1, p0, Lcom/konka/epg/ui/EpgPopWinParent;->m_Inflater:Landroid/view/LayoutInflater;

    iput-object v1, p0, Lcom/konka/epg/ui/EpgPopWinParent;->m_TitleText:Landroid/widget/TextView;

    iput-object v1, p0, Lcom/konka/epg/ui/EpgPopWinParent;->m_ContentText:Landroid/widget/TextView;

    iput-object v1, p0, Lcom/konka/epg/ui/EpgPopWinParent;->m_ConfirmBtn:Landroid/widget/Button;

    iput-object v1, p0, Lcom/konka/epg/ui/EpgPopWinParent;->m_CancelBtn:Landroid/widget/Button;

    iput-object v1, p0, Lcom/konka/epg/ui/EpgPopWinParent;->activity:Landroid/app/Activity;

    iput-object v1, p0, Lcom/konka/epg/ui/EpgPopWinParent;->epgMainMenuViewHolder:Lcom/konka/epg/ui/EpgMainMenuViewHolder;

    iput-object v1, p0, Lcom/konka/epg/ui/EpgPopWinParent;->epgServiceProvider:Lcom/konka/epg/service/EpgServiceProvider;

    iput-object v1, p0, Lcom/konka/epg/ui/EpgPopWinParent;->lastFocusPos:Lcom/konka/epg/ui/EpgMainMenuActivity$EnumFocusPosition;

    iput-object p1, p0, Lcom/konka/epg/ui/EpgPopWinParent;->m_Parent:Landroid/view/View;

    iput-object p2, p0, Lcom/konka/epg/ui/EpgPopWinParent;->m_Context:Landroid/content/Context;

    return-void
.end method

.method protected constructor <init>(Landroid/view/View;Landroid/content/Context;Lcom/konka/epg/ui/EpgMainMenuViewHolder;)V
    .locals 2
    .param p1    # Landroid/view/View;
    .param p2    # Landroid/content/Context;
    .param p3    # Lcom/konka/epg/ui/EpgMainMenuViewHolder;

    const/4 v1, 0x0

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    const/4 v0, 0x0

    iput v0, p0, Lcom/konka/epg/ui/EpgPopWinParent;->iResID:I

    iput-object v1, p0, Lcom/konka/epg/ui/EpgPopWinParent;->m_Parent:Landroid/view/View;

    iput-object v1, p0, Lcom/konka/epg/ui/EpgPopWinParent;->m_ContentView:Landroid/view/View;

    iput-object v1, p0, Lcom/konka/epg/ui/EpgPopWinParent;->m_Context:Landroid/content/Context;

    iput-object v1, p0, Lcom/konka/epg/ui/EpgPopWinParent;->m_PopupWindow:Landroid/widget/PopupWindow;

    iput-object v1, p0, Lcom/konka/epg/ui/EpgPopWinParent;->m_Inflater:Landroid/view/LayoutInflater;

    iput-object v1, p0, Lcom/konka/epg/ui/EpgPopWinParent;->m_TitleText:Landroid/widget/TextView;

    iput-object v1, p0, Lcom/konka/epg/ui/EpgPopWinParent;->m_ContentText:Landroid/widget/TextView;

    iput-object v1, p0, Lcom/konka/epg/ui/EpgPopWinParent;->m_ConfirmBtn:Landroid/widget/Button;

    iput-object v1, p0, Lcom/konka/epg/ui/EpgPopWinParent;->m_CancelBtn:Landroid/widget/Button;

    iput-object v1, p0, Lcom/konka/epg/ui/EpgPopWinParent;->activity:Landroid/app/Activity;

    iput-object v1, p0, Lcom/konka/epg/ui/EpgPopWinParent;->epgMainMenuViewHolder:Lcom/konka/epg/ui/EpgMainMenuViewHolder;

    iput-object v1, p0, Lcom/konka/epg/ui/EpgPopWinParent;->epgServiceProvider:Lcom/konka/epg/service/EpgServiceProvider;

    iput-object v1, p0, Lcom/konka/epg/ui/EpgPopWinParent;->lastFocusPos:Lcom/konka/epg/ui/EpgMainMenuActivity$EnumFocusPosition;

    iput-object p1, p0, Lcom/konka/epg/ui/EpgPopWinParent;->m_Parent:Landroid/view/View;

    iput-object p2, p0, Lcom/konka/epg/ui/EpgPopWinParent;->m_Context:Landroid/content/Context;

    iput-object p3, p0, Lcom/konka/epg/ui/EpgPopWinParent;->epgMainMenuViewHolder:Lcom/konka/epg/ui/EpgMainMenuViewHolder;

    return-void
.end method

.method protected constructor <init>(Landroid/view/View;Landroid/content/Context;Lcom/konka/epg/ui/EpgMainMenuViewHolder;Lcom/konka/epg/service/EpgServiceProvider;)V
    .locals 2
    .param p1    # Landroid/view/View;
    .param p2    # Landroid/content/Context;
    .param p3    # Lcom/konka/epg/ui/EpgMainMenuViewHolder;
    .param p4    # Lcom/konka/epg/service/EpgServiceProvider;

    const/4 v1, 0x0

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    const/4 v0, 0x0

    iput v0, p0, Lcom/konka/epg/ui/EpgPopWinParent;->iResID:I

    iput-object v1, p0, Lcom/konka/epg/ui/EpgPopWinParent;->m_Parent:Landroid/view/View;

    iput-object v1, p0, Lcom/konka/epg/ui/EpgPopWinParent;->m_ContentView:Landroid/view/View;

    iput-object v1, p0, Lcom/konka/epg/ui/EpgPopWinParent;->m_Context:Landroid/content/Context;

    iput-object v1, p0, Lcom/konka/epg/ui/EpgPopWinParent;->m_PopupWindow:Landroid/widget/PopupWindow;

    iput-object v1, p0, Lcom/konka/epg/ui/EpgPopWinParent;->m_Inflater:Landroid/view/LayoutInflater;

    iput-object v1, p0, Lcom/konka/epg/ui/EpgPopWinParent;->m_TitleText:Landroid/widget/TextView;

    iput-object v1, p0, Lcom/konka/epg/ui/EpgPopWinParent;->m_ContentText:Landroid/widget/TextView;

    iput-object v1, p0, Lcom/konka/epg/ui/EpgPopWinParent;->m_ConfirmBtn:Landroid/widget/Button;

    iput-object v1, p0, Lcom/konka/epg/ui/EpgPopWinParent;->m_CancelBtn:Landroid/widget/Button;

    iput-object v1, p0, Lcom/konka/epg/ui/EpgPopWinParent;->activity:Landroid/app/Activity;

    iput-object v1, p0, Lcom/konka/epg/ui/EpgPopWinParent;->epgMainMenuViewHolder:Lcom/konka/epg/ui/EpgMainMenuViewHolder;

    iput-object v1, p0, Lcom/konka/epg/ui/EpgPopWinParent;->epgServiceProvider:Lcom/konka/epg/service/EpgServiceProvider;

    iput-object v1, p0, Lcom/konka/epg/ui/EpgPopWinParent;->lastFocusPos:Lcom/konka/epg/ui/EpgMainMenuActivity$EnumFocusPosition;

    iput-object p1, p0, Lcom/konka/epg/ui/EpgPopWinParent;->m_Parent:Landroid/view/View;

    iput-object p2, p0, Lcom/konka/epg/ui/EpgPopWinParent;->m_Context:Landroid/content/Context;

    iput-object p3, p0, Lcom/konka/epg/ui/EpgPopWinParent;->epgMainMenuViewHolder:Lcom/konka/epg/ui/EpgMainMenuViewHolder;

    iput-object p4, p0, Lcom/konka/epg/ui/EpgPopWinParent;->epgServiceProvider:Lcom/konka/epg/service/EpgServiceProvider;

    return-void
.end method


# virtual methods
.method abstract findView()V
.end method

.method protected initEpgPopWin(I)V
    .locals 3
    .param p1    # I

    iput p1, p0, Lcom/konka/epg/ui/EpgPopWinParent;->iResID:I

    iget-object v0, p0, Lcom/konka/epg/ui/EpgPopWinParent;->m_Context:Landroid/content/Context;

    invoke-static {v0}, Landroid/view/LayoutInflater;->from(Landroid/content/Context;)Landroid/view/LayoutInflater;

    move-result-object v0

    iput-object v0, p0, Lcom/konka/epg/ui/EpgPopWinParent;->m_Inflater:Landroid/view/LayoutInflater;

    iget-object v0, p0, Lcom/konka/epg/ui/EpgPopWinParent;->m_Inflater:Landroid/view/LayoutInflater;

    iget v1, p0, Lcom/konka/epg/ui/EpgPopWinParent;->iResID:I

    const/4 v2, 0x0

    invoke-virtual {v0, v1, v2}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;)Landroid/view/View;

    move-result-object v0

    iput-object v0, p0, Lcom/konka/epg/ui/EpgPopWinParent;->m_ContentView:Landroid/view/View;

    invoke-virtual {p0}, Lcom/konka/epg/ui/EpgPopWinParent;->findView()V

    invoke-virtual {p0}, Lcom/konka/epg/ui/EpgPopWinParent;->setListener()V

    return-void
.end method

.method abstract setListener()V
.end method

.method protected showEpgPopWin(II)V
    .locals 4
    .param p1    # I
    .param p2    # I

    const/4 v3, -0x1

    iget-object v0, p0, Lcom/konka/epg/ui/EpgPopWinParent;->m_PopupWindow:Landroid/widget/PopupWindow;

    if-nez v0, :cond_0

    new-instance v0, Landroid/widget/PopupWindow;

    iget-object v1, p0, Lcom/konka/epg/ui/EpgPopWinParent;->m_ContentView:Landroid/view/View;

    const/4 v2, 0x1

    invoke-direct {v0, v1, v3, v3, v2}, Landroid/widget/PopupWindow;-><init>(Landroid/view/View;IIZ)V

    iput-object v0, p0, Lcom/konka/epg/ui/EpgPopWinParent;->m_PopupWindow:Landroid/widget/PopupWindow;

    iget-object v0, p0, Lcom/konka/epg/ui/EpgPopWinParent;->m_PopupWindow:Landroid/widget/PopupWindow;

    const/high16 v1, 0x7f070000    # com.konka.epg.R.style.PopWindowAnim

    invoke-virtual {v0, v1}, Landroid/widget/PopupWindow;->setAnimationStyle(I)V

    :cond_0
    iget-object v0, p0, Lcom/konka/epg/ui/EpgPopWinParent;->m_PopupWindow:Landroid/widget/PopupWindow;

    iget-object v1, p0, Lcom/konka/epg/ui/EpgPopWinParent;->m_Parent:Landroid/view/View;

    const/16 v2, 0x11

    invoke-virtual {v0, v1, v2, p1, p2}, Landroid/widget/PopupWindow;->showAtLocation(Landroid/view/View;III)V

    sget-object v0, Lcom/konka/epg/ui/EpgMainMenuActivity;->currentMainFocus:Lcom/konka/epg/ui/EpgMainMenuActivity$EnumFocusPosition;

    iput-object v0, p0, Lcom/konka/epg/ui/EpgPopWinParent;->lastFocusPos:Lcom/konka/epg/ui/EpgMainMenuActivity$EnumFocusPosition;

    sget-object v0, Lcom/konka/epg/ui/EpgMainMenuActivity$EnumFocusPosition;->E_FCS_POS_POPUP_WIN:Lcom/konka/epg/ui/EpgMainMenuActivity$EnumFocusPosition;

    sput-object v0, Lcom/konka/epg/ui/EpgMainMenuActivity;->currentMainFocus:Lcom/konka/epg/ui/EpgMainMenuActivity$EnumFocusPosition;

    return-void
.end method
