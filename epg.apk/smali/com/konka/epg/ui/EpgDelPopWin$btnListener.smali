.class Lcom/konka/epg/ui/EpgDelPopWin$btnListener;
.super Ljava/lang/Object;
.source "EpgDelPopWin.java"

# interfaces
.implements Landroid/view/View$OnClickListener;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/konka/epg/ui/EpgDelPopWin;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x2
    name = "btnListener"
.end annotation


# instance fields
.field final synthetic this$0:Lcom/konka/epg/ui/EpgDelPopWin;


# direct methods
.method private constructor <init>(Lcom/konka/epg/ui/EpgDelPopWin;)V
    .locals 0

    iput-object p1, p0, Lcom/konka/epg/ui/EpgDelPopWin$btnListener;->this$0:Lcom/konka/epg/ui/EpgDelPopWin;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method synthetic constructor <init>(Lcom/konka/epg/ui/EpgDelPopWin;Lcom/konka/epg/ui/EpgDelPopWin$btnListener;)V
    .locals 0

    invoke-direct {p0, p1}, Lcom/konka/epg/ui/EpgDelPopWin$btnListener;-><init>(Lcom/konka/epg/ui/EpgDelPopWin;)V

    return-void
.end method


# virtual methods
.method public onClick(Landroid/view/View;)V
    .locals 3
    .param p1    # Landroid/view/View;

    invoke-virtual {p1}, Landroid/view/View;->getId()I

    move-result v0

    packed-switch v0, :pswitch_data_0

    :goto_0
    return-void

    :pswitch_0
    iget-object v0, p0, Lcom/konka/epg/ui/EpgDelPopWin$btnListener;->this$0:Lcom/konka/epg/ui/EpgDelPopWin;

    # getter for: Lcom/konka/epg/ui/EpgDelPopWin;->m_EpgDesk:Lcom/konka/kkinterface/tv/EpgDesk;
    invoke-static {v0}, Lcom/konka/epg/ui/EpgDelPopWin;->access$0(Lcom/konka/epg/ui/EpgDelPopWin;)Lcom/konka/kkinterface/tv/EpgDesk;

    move-result-object v0

    iget-object v1, p0, Lcom/konka/epg/ui/EpgDelPopWin$btnListener;->this$0:Lcom/konka/epg/ui/EpgDelPopWin;

    # getter for: Lcom/konka/epg/ui/EpgDelPopWin;->iIndex:I
    invoke-static {v1}, Lcom/konka/epg/ui/EpgDelPopWin;->access$1(Lcom/konka/epg/ui/EpgDelPopWin;)I

    move-result v1

    const/4 v2, 0x0

    invoke-interface {v0, v1, v2}, Lcom/konka/kkinterface/tv/EpgDesk;->delEpgTimerEvent(IZ)Z

    iget-object v0, p0, Lcom/konka/epg/ui/EpgDelPopWin$btnListener;->this$0:Lcom/konka/epg/ui/EpgDelPopWin;

    # getter for: Lcom/konka/epg/ui/EpgDelPopWin;->m_BookListAdapter:Lcom/konka/epg/adapter/BookListAdapter;
    invoke-static {v0}, Lcom/konka/epg/ui/EpgDelPopWin;->access$2(Lcom/konka/epg/ui/EpgDelPopWin;)Lcom/konka/epg/adapter/BookListAdapter;

    move-result-object v0

    iget-object v1, p0, Lcom/konka/epg/ui/EpgDelPopWin$btnListener;->this$0:Lcom/konka/epg/ui/EpgDelPopWin;

    # getter for: Lcom/konka/epg/ui/EpgDelPopWin;->iIndex:I
    invoke-static {v1}, Lcom/konka/epg/ui/EpgDelPopWin;->access$1(Lcom/konka/epg/ui/EpgDelPopWin;)I

    move-result v1

    invoke-virtual {v0, v1}, Lcom/konka/epg/adapter/BookListAdapter;->delItem(I)V

    iget-object v0, p0, Lcom/konka/epg/ui/EpgDelPopWin$btnListener;->this$0:Lcom/konka/epg/ui/EpgDelPopWin;

    # getter for: Lcom/konka/epg/ui/EpgDelPopWin;->m_BookListAdapter:Lcom/konka/epg/adapter/BookListAdapter;
    invoke-static {v0}, Lcom/konka/epg/ui/EpgDelPopWin;->access$2(Lcom/konka/epg/ui/EpgDelPopWin;)Lcom/konka/epg/adapter/BookListAdapter;

    move-result-object v0

    invoke-virtual {v0}, Lcom/konka/epg/adapter/BookListAdapter;->notifyDataSetChanged()V

    iget-object v0, p0, Lcom/konka/epg/ui/EpgDelPopWin$btnListener;->this$0:Lcom/konka/epg/ui/EpgDelPopWin;

    iget-object v0, v0, Lcom/konka/epg/ui/EpgDelPopWin;->lastFocusPos:Lcom/konka/epg/ui/EpgMainMenuActivity$EnumFocusPosition;

    sput-object v0, Lcom/konka/epg/ui/EpgMainMenuActivity;->currentMainFocus:Lcom/konka/epg/ui/EpgMainMenuActivity$EnumFocusPosition;

    iget-object v0, p0, Lcom/konka/epg/ui/EpgDelPopWin$btnListener;->this$0:Lcom/konka/epg/ui/EpgDelPopWin;

    iget-object v0, v0, Lcom/konka/epg/ui/EpgDelPopWin;->m_PopupWindow:Landroid/widget/PopupWindow;

    invoke-virtual {v0}, Landroid/widget/PopupWindow;->dismiss()V

    goto :goto_0

    :pswitch_1
    iget-object v0, p0, Lcom/konka/epg/ui/EpgDelPopWin$btnListener;->this$0:Lcom/konka/epg/ui/EpgDelPopWin;

    iget-object v0, v0, Lcom/konka/epg/ui/EpgDelPopWin;->lastFocusPos:Lcom/konka/epg/ui/EpgMainMenuActivity$EnumFocusPosition;

    sput-object v0, Lcom/konka/epg/ui/EpgMainMenuActivity;->currentMainFocus:Lcom/konka/epg/ui/EpgMainMenuActivity$EnumFocusPosition;

    iget-object v0, p0, Lcom/konka/epg/ui/EpgDelPopWin$btnListener;->this$0:Lcom/konka/epg/ui/EpgDelPopWin;

    iget-object v0, v0, Lcom/konka/epg/ui/EpgDelPopWin;->m_PopupWindow:Landroid/widget/PopupWindow;

    invoke-virtual {v0}, Landroid/widget/PopupWindow;->dismiss()V

    goto :goto_0

    :pswitch_data_0
    .packed-switch 0x7f0a003d
        :pswitch_0    # com.konka.epg.R.id.epg_popwin_confrim_btn
        :pswitch_1    # com.konka.epg.R.id.epg_popwin_cancel_btn
    .end packed-switch
.end method
