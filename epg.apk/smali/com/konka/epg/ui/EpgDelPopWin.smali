.class public Lcom/konka/epg/ui/EpgDelPopWin;
.super Lcom/konka/epg/ui/EpgPopWinParent;
.source "EpgDelPopWin.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/konka/epg/ui/EpgDelPopWin$btnListener;
    }
.end annotation


# instance fields
.field private iIndex:I

.field private m_BookListAdapter:Lcom/konka/epg/adapter/BookListAdapter;

.field private m_EpgDesk:Lcom/konka/kkinterface/tv/EpgDesk;


# direct methods
.method protected constructor <init>(Landroid/view/View;Landroid/content/Context;)V
    .locals 1
    .param p1    # Landroid/view/View;
    .param p2    # Landroid/content/Context;

    const/4 v0, 0x0

    invoke-direct {p0, p1, p2}, Lcom/konka/epg/ui/EpgPopWinParent;-><init>(Landroid/view/View;Landroid/content/Context;)V

    iput-object v0, p0, Lcom/konka/epg/ui/EpgDelPopWin;->m_EpgDesk:Lcom/konka/kkinterface/tv/EpgDesk;

    iput-object v0, p0, Lcom/konka/epg/ui/EpgDelPopWin;->m_BookListAdapter:Lcom/konka/epg/adapter/BookListAdapter;

    const/4 v0, 0x0

    iput v0, p0, Lcom/konka/epg/ui/EpgDelPopWin;->iIndex:I

    return-void
.end method

.method static synthetic access$0(Lcom/konka/epg/ui/EpgDelPopWin;)Lcom/konka/kkinterface/tv/EpgDesk;
    .locals 1

    iget-object v0, p0, Lcom/konka/epg/ui/EpgDelPopWin;->m_EpgDesk:Lcom/konka/kkinterface/tv/EpgDesk;

    return-object v0
.end method

.method static synthetic access$1(Lcom/konka/epg/ui/EpgDelPopWin;)I
    .locals 1

    iget v0, p0, Lcom/konka/epg/ui/EpgDelPopWin;->iIndex:I

    return v0
.end method

.method static synthetic access$2(Lcom/konka/epg/ui/EpgDelPopWin;)Lcom/konka/epg/adapter/BookListAdapter;
    .locals 1

    iget-object v0, p0, Lcom/konka/epg/ui/EpgDelPopWin;->m_BookListAdapter:Lcom/konka/epg/adapter/BookListAdapter;

    return-object v0
.end method


# virtual methods
.method findView()V
    .locals 2

    iget-object v0, p0, Lcom/konka/epg/ui/EpgDelPopWin;->m_ContentView:Landroid/view/View;

    const v1, 0x7f0a003d    # com.konka.epg.R.id.epg_popwin_confrim_btn

    invoke-virtual {v0, v1}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/Button;

    iput-object v0, p0, Lcom/konka/epg/ui/EpgDelPopWin;->m_ConfirmBtn:Landroid/widget/Button;

    iget-object v0, p0, Lcom/konka/epg/ui/EpgDelPopWin;->m_ContentView:Landroid/view/View;

    const v1, 0x7f0a003e    # com.konka.epg.R.id.epg_popwin_cancel_btn

    invoke-virtual {v0, v1}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/Button;

    iput-object v0, p0, Lcom/konka/epg/ui/EpgDelPopWin;->m_CancelBtn:Landroid/widget/Button;

    return-void
.end method

.method protected initEpgPopWin(ILcom/konka/kkinterface/tv/EpgDesk;Lcom/konka/epg/adapter/BookListAdapter;I)V
    .locals 0
    .param p1    # I
    .param p2    # Lcom/konka/kkinterface/tv/EpgDesk;
    .param p3    # Lcom/konka/epg/adapter/BookListAdapter;
    .param p4    # I

    invoke-super {p0, p1}, Lcom/konka/epg/ui/EpgPopWinParent;->initEpgPopWin(I)V

    iput-object p2, p0, Lcom/konka/epg/ui/EpgDelPopWin;->m_EpgDesk:Lcom/konka/kkinterface/tv/EpgDesk;

    iput-object p3, p0, Lcom/konka/epg/ui/EpgDelPopWin;->m_BookListAdapter:Lcom/konka/epg/adapter/BookListAdapter;

    iput p4, p0, Lcom/konka/epg/ui/EpgDelPopWin;->iIndex:I

    return-void
.end method

.method setListener()V
    .locals 2

    new-instance v0, Lcom/konka/epg/ui/EpgDelPopWin$btnListener;

    const/4 v1, 0x0

    invoke-direct {v0, p0, v1}, Lcom/konka/epg/ui/EpgDelPopWin$btnListener;-><init>(Lcom/konka/epg/ui/EpgDelPopWin;Lcom/konka/epg/ui/EpgDelPopWin$btnListener;)V

    iget-object v1, p0, Lcom/konka/epg/ui/EpgDelPopWin;->m_ConfirmBtn:Landroid/widget/Button;

    invoke-virtual {v1, v0}, Landroid/widget/Button;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    iget-object v1, p0, Lcom/konka/epg/ui/EpgDelPopWin;->m_CancelBtn:Landroid/widget/Button;

    invoke-virtual {v1, v0}, Landroid/widget/Button;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    return-void
.end method

.method protected showEpgPopWin(II)V
    .locals 0
    .param p1    # I
    .param p2    # I

    invoke-super {p0, p1, p2}, Lcom/konka/epg/ui/EpgPopWinParent;->showEpgPopWin(II)V

    return-void
.end method
