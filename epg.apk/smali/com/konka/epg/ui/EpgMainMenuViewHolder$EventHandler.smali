.class final Lcom/konka/epg/ui/EpgMainMenuViewHolder$EventHandler;
.super Landroid/os/Handler;
.source "EpgMainMenuViewHolder.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/konka/epg/ui/EpgMainMenuViewHolder;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x12
    name = "EventHandler"
.end annotation


# instance fields
.field final synthetic this$0:Lcom/konka/epg/ui/EpgMainMenuViewHolder;


# direct methods
.method private constructor <init>(Lcom/konka/epg/ui/EpgMainMenuViewHolder;)V
    .locals 0

    iput-object p1, p0, Lcom/konka/epg/ui/EpgMainMenuViewHolder$EventHandler;->this$0:Lcom/konka/epg/ui/EpgMainMenuViewHolder;

    invoke-direct {p0}, Landroid/os/Handler;-><init>()V

    return-void
.end method

.method synthetic constructor <init>(Lcom/konka/epg/ui/EpgMainMenuViewHolder;Lcom/konka/epg/ui/EpgMainMenuViewHolder$EventHandler;)V
    .locals 0

    invoke-direct {p0, p1}, Lcom/konka/epg/ui/EpgMainMenuViewHolder$EventHandler;-><init>(Lcom/konka/epg/ui/EpgMainMenuViewHolder;)V

    return-void
.end method


# virtual methods
.method public handleMessage(Landroid/os/Message;)V
    .locals 3
    .param p1    # Landroid/os/Message;

    const v2, 0x7f020021    # com.konka.epg.R.drawable.epg_type

    iget v0, p1, Landroid/os/Message;->what:I

    packed-switch v0, :pswitch_data_0

    :cond_0
    :goto_0
    return-void

    :pswitch_0
    iget-object v0, p0, Lcom/konka/epg/ui/EpgMainMenuViewHolder$EventHandler;->this$0:Lcom/konka/epg/ui/EpgMainMenuViewHolder;

    # getter for: Lcom/konka/epg/ui/EpgMainMenuViewHolder;->m_EpgStatus:I
    invoke-static {v0}, Lcom/konka/epg/ui/EpgMainMenuViewHolder;->access$11(Lcom/konka/epg/ui/EpgMainMenuViewHolder;)I

    move-result v0

    const/16 v1, 0x450

    if-ne v0, v1, :cond_0

    sget-object v0, Lcom/konka/epg/ui/EpgMainMenuActivity;->currentServiceType:Lcom/mstar/android/tvapi/common/vo/EnumServiceType;

    invoke-virtual {v0}, Lcom/mstar/android/tvapi/common/vo/EnumServiceType;->ordinal()I

    move-result v0

    sget-object v1, Lcom/mstar/android/tvapi/common/vo/EnumServiceType;->E_SERVICETYPE_DTV:Lcom/mstar/android/tvapi/common/vo/EnumServiceType;

    invoke-virtual {v1}, Lcom/mstar/android/tvapi/common/vo/EnumServiceType;->ordinal()I

    move-result v1

    if-ne v0, v1, :cond_1

    iget-object v0, p0, Lcom/konka/epg/ui/EpgMainMenuViewHolder$EventHandler;->this$0:Lcom/konka/epg/ui/EpgMainMenuViewHolder;

    # getter for: Lcom/konka/epg/ui/EpgMainMenuViewHolder;->m_EpgTypeView:Landroid/view/View;
    invoke-static {v0}, Lcom/konka/epg/ui/EpgMainMenuViewHolder;->access$12(Lcom/konka/epg/ui/EpgMainMenuViewHolder;)Landroid/view/View;

    move-result-object v0

    invoke-virtual {v0, v2}, Landroid/view/View;->setBackgroundResource(I)V

    :cond_1
    iget-object v0, p0, Lcom/konka/epg/ui/EpgMainMenuViewHolder$EventHandler;->this$0:Lcom/konka/epg/ui/EpgMainMenuViewHolder;

    # getter for: Lcom/konka/epg/ui/EpgMainMenuViewHolder;->m_EpgTypeTxt:Landroid/widget/TextView;
    invoke-static {v0}, Lcom/konka/epg/ui/EpgMainMenuViewHolder;->access$13(Lcom/konka/epg/ui/EpgMainMenuViewHolder;)Landroid/widget/TextView;

    move-result-object v0

    const v1, 0x7f080009    # com.konka.epg.R.string.epg_Mainmenu_SS_No_Module

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setText(I)V

    goto :goto_0

    :pswitch_1
    iget-object v0, p0, Lcom/konka/epg/ui/EpgMainMenuViewHolder$EventHandler;->this$0:Lcom/konka/epg/ui/EpgMainMenuViewHolder;

    # getter for: Lcom/konka/epg/ui/EpgMainMenuViewHolder;->m_EpgStatus:I
    invoke-static {v0}, Lcom/konka/epg/ui/EpgMainMenuViewHolder;->access$11(Lcom/konka/epg/ui/EpgMainMenuViewHolder;)I

    move-result v0

    const/16 v1, 0x452

    if-ne v0, v1, :cond_0

    sget-object v0, Lcom/konka/epg/ui/EpgMainMenuActivity;->currentServiceType:Lcom/mstar/android/tvapi/common/vo/EnumServiceType;

    invoke-virtual {v0}, Lcom/mstar/android/tvapi/common/vo/EnumServiceType;->ordinal()I

    move-result v0

    sget-object v1, Lcom/mstar/android/tvapi/common/vo/EnumServiceType;->E_SERVICETYPE_DTV:Lcom/mstar/android/tvapi/common/vo/EnumServiceType;

    invoke-virtual {v1}, Lcom/mstar/android/tvapi/common/vo/EnumServiceType;->ordinal()I

    move-result v1

    if-ne v0, v1, :cond_2

    iget-object v0, p0, Lcom/konka/epg/ui/EpgMainMenuViewHolder$EventHandler;->this$0:Lcom/konka/epg/ui/EpgMainMenuViewHolder;

    # getter for: Lcom/konka/epg/ui/EpgMainMenuViewHolder;->m_EpgTypeView:Landroid/view/View;
    invoke-static {v0}, Lcom/konka/epg/ui/EpgMainMenuViewHolder;->access$12(Lcom/konka/epg/ui/EpgMainMenuViewHolder;)Landroid/view/View;

    move-result-object v0

    invoke-virtual {v0, v2}, Landroid/view/View;->setBackgroundResource(I)V

    :cond_2
    iget-object v0, p0, Lcom/konka/epg/ui/EpgMainMenuViewHolder$EventHandler;->this$0:Lcom/konka/epg/ui/EpgMainMenuViewHolder;

    # getter for: Lcom/konka/epg/ui/EpgMainMenuViewHolder;->m_EpgTypeTxt:Landroid/widget/TextView;
    invoke-static {v0}, Lcom/konka/epg/ui/EpgMainMenuViewHolder;->access$13(Lcom/konka/epg/ui/EpgMainMenuViewHolder;)Landroid/widget/TextView;

    move-result-object v0

    const v1, 0x7f08000a    # com.konka.epg.R.string.epg_Mainmenu_SS_Scrambled

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setText(I)V

    goto :goto_0

    nop

    :pswitch_data_0
    .packed-switch 0x4b0
        :pswitch_0
        :pswitch_1
    .end packed-switch
.end method
