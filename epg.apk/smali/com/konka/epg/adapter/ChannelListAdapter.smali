.class public Lcom/konka/epg/adapter/ChannelListAdapter;
.super Landroid/widget/BaseAdapter;
.source "ChannelListAdapter.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/konka/epg/adapter/ChannelListAdapter$ViewHolder;
    }
.end annotation


# instance fields
.field private iSelect:I

.field private isFocus:Z

.field private mInflater:Landroid/view/LayoutInflater;

.field private m_ChannelData:Ljava/util/ArrayList;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/ArrayList",
            "<",
            "Lcom/konka/kkimplements/tv/vo/KonkaProgInfo;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method public constructor <init>(Landroid/content/Context;)V
    .locals 2
    .param p1    # Landroid/content/Context;

    const/4 v1, 0x0

    invoke-direct {p0}, Landroid/widget/BaseAdapter;-><init>()V

    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, Lcom/konka/epg/adapter/ChannelListAdapter;->m_ChannelData:Ljava/util/ArrayList;

    iput-boolean v1, p0, Lcom/konka/epg/adapter/ChannelListAdapter;->isFocus:Z

    iput v1, p0, Lcom/konka/epg/adapter/ChannelListAdapter;->iSelect:I

    invoke-static {p1}, Landroid/view/LayoutInflater;->from(Landroid/content/Context;)Landroid/view/LayoutInflater;

    move-result-object v0

    iput-object v0, p0, Lcom/konka/epg/adapter/ChannelListAdapter;->mInflater:Landroid/view/LayoutInflater;

    return-void
.end method


# virtual methods
.method public addItem(Lcom/konka/kkimplements/tv/vo/KonkaProgInfo;)V
    .locals 1
    .param p1    # Lcom/konka/kkimplements/tv/vo/KonkaProgInfo;

    iget-object v0, p0, Lcom/konka/epg/adapter/ChannelListAdapter;->m_ChannelData:Ljava/util/ArrayList;

    invoke-virtual {v0, p1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    return-void
.end method

.method public clearItme()V
    .locals 1

    iget-object v0, p0, Lcom/konka/epg/adapter/ChannelListAdapter;->m_ChannelData:Ljava/util/ArrayList;

    invoke-virtual {v0}, Ljava/util/ArrayList;->clear()V

    return-void
.end method

.method public getCount()I
    .locals 1

    iget-object v0, p0, Lcom/konka/epg/adapter/ChannelListAdapter;->m_ChannelData:Ljava/util/ArrayList;

    invoke-virtual {v0}, Ljava/util/ArrayList;->size()I

    move-result v0

    return v0
.end method

.method public getItem(I)Lcom/konka/kkimplements/tv/vo/KonkaProgInfo;
    .locals 1
    .param p1    # I

    iget-object v0, p0, Lcom/konka/epg/adapter/ChannelListAdapter;->m_ChannelData:Ljava/util/ArrayList;

    invoke-virtual {v0, p1}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/konka/kkimplements/tv/vo/KonkaProgInfo;

    return-object v0
.end method

.method public bridge synthetic getItem(I)Ljava/lang/Object;
    .locals 1

    invoke-virtual {p0, p1}, Lcom/konka/epg/adapter/ChannelListAdapter;->getItem(I)Lcom/konka/kkimplements/tv/vo/KonkaProgInfo;

    move-result-object v0

    return-object v0
.end method

.method public getItemId(I)J
    .locals 2
    .param p1    # I

    int-to-long v0, p1

    return-wide v0
.end method

.method public getView(ILandroid/view/View;Landroid/view/ViewGroup;)Landroid/view/View;
    .locals 4
    .param p1    # I
    .param p2    # Landroid/view/View;
    .param p3    # Landroid/view/ViewGroup;

    const/4 v3, 0x0

    const/4 v0, 0x0

    if-nez p2, :cond_1

    new-instance v0, Lcom/konka/epg/adapter/ChannelListAdapter$ViewHolder;

    invoke-direct {v0, p0, v3}, Lcom/konka/epg/adapter/ChannelListAdapter$ViewHolder;-><init>(Lcom/konka/epg/adapter/ChannelListAdapter;Lcom/konka/epg/adapter/ChannelListAdapter$ViewHolder;)V

    iget-object v1, p0, Lcom/konka/epg/adapter/ChannelListAdapter;->mInflater:Landroid/view/LayoutInflater;

    const v2, 0x7f030003    # com.konka.epg.R.layout.epg_mainmenu_channel_listitem

    invoke-virtual {v1, v2, v3}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;)Landroid/view/View;

    move-result-object p2

    const v1, 0x7f0a0037    # com.konka.epg.R.id.epg_channellist_name_text

    invoke-virtual {p2, v1}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v1

    check-cast v1, Landroid/widget/TextView;

    iput-object v1, v0, Lcom/konka/epg/adapter/ChannelListAdapter$ViewHolder;->channel:Landroid/widget/TextView;

    const v1, 0x7f0a0038    # com.konka.epg.R.id.epg_channellist_playhint_img

    invoke-virtual {p2, v1}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v1

    check-cast v1, Landroid/widget/ImageView;

    iput-object v1, v0, Lcom/konka/epg/adapter/ChannelListAdapter$ViewHolder;->playHint:Landroid/widget/ImageView;

    invoke-virtual {p2, v0}, Landroid/view/View;->setTag(Ljava/lang/Object;)V

    :goto_0
    iget-object v2, v0, Lcom/konka/epg/adapter/ChannelListAdapter$ViewHolder;->channel:Landroid/widget/TextView;

    new-instance v3, Ljava/lang/StringBuilder;

    iget-object v1, p0, Lcom/konka/epg/adapter/ChannelListAdapter;->m_ChannelData:Ljava/util/ArrayList;

    invoke-virtual {v1, p1}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/konka/kkimplements/tv/vo/KonkaProgInfo;

    iget v1, v1, Lcom/konka/kkimplements/tv/vo/KonkaProgInfo;->_mProgNo:I

    invoke-static {v1}, Ljava/lang/String;->valueOf(I)Ljava/lang/String;

    move-result-object v1

    invoke-direct {v3, v1}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    const-string v1, " "

    invoke-virtual {v3, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    iget-object v1, p0, Lcom/konka/epg/adapter/ChannelListAdapter;->m_ChannelData:Ljava/util/ArrayList;

    invoke-virtual {v1, p1}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/konka/kkimplements/tv/vo/KonkaProgInfo;

    iget-object v1, v1, Lcom/konka/kkimplements/tv/vo/KonkaProgInfo;->_mProgName:Ljava/lang/String;

    invoke-virtual {v3, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v2, v1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    iget-object v1, v0, Lcom/konka/epg/adapter/ChannelListAdapter$ViewHolder;->playHint:Landroid/widget/ImageView;

    const/4 v2, 0x4

    invoke-virtual {v1, v2}, Landroid/widget/ImageView;->setVisibility(I)V

    iget v1, p0, Lcom/konka/epg/adapter/ChannelListAdapter;->iSelect:I

    if-ne v1, p1, :cond_0

    iget-boolean v1, p0, Lcom/konka/epg/adapter/ChannelListAdapter;->isFocus:Z

    if-nez v1, :cond_0

    iget-object v1, v0, Lcom/konka/epg/adapter/ChannelListAdapter$ViewHolder;->playHint:Landroid/widget/ImageView;

    const/4 v2, 0x0

    invoke-virtual {v1, v2}, Landroid/widget/ImageView;->setVisibility(I)V

    :cond_0
    return-object p2

    :cond_1
    invoke-virtual {p2}, Landroid/view/View;->getTag()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/konka/epg/adapter/ChannelListAdapter$ViewHolder;

    goto :goto_0
.end method

.method public setIsFocus(Z)V
    .locals 0
    .param p1    # Z

    iput-boolean p1, p0, Lcom/konka/epg/adapter/ChannelListAdapter;->isFocus:Z

    return-void
.end method

.method public setSelect(I)V
    .locals 0
    .param p1    # I

    iput p1, p0, Lcom/konka/epg/adapter/ChannelListAdapter;->iSelect:I

    return-void
.end method
