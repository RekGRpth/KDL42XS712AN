.class public Lcom/konka/epg/adapter/BookListAdapter;
.super Landroid/widget/BaseAdapter;
.source "BookListAdapter.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/konka/epg/adapter/BookListAdapter$ViewHolder;
    }
.end annotation


# instance fields
.field private final EPG_EVENT_NONE:I

.field private final EPG_EVENT_RECORDER:I

.field private final EPG_EVENT_REMIDER:I

.field private final EPG_REPEAT_AUTO:I

.field private final EPG_REPEAT_DAILY:I

.field private final EPG_REPEAT_ONCE:I

.field private final EPG_REPEAT_WEEKLY:I

.field private mInflater:Landroid/view/LayoutInflater;

.field private m_BookData:Ljava/util/ArrayList;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/ArrayList",
            "<",
            "Lcom/konka/kkinterface/tv/DtvInterface$EPG_SCHEDULE_EVENT_INFO;",
            ">;"
        }
    .end annotation
.end field

.field private time:Landroid/text/format/Time;


# direct methods
.method public constructor <init>(Landroid/content/Context;)V
    .locals 2
    .param p1    # Landroid/content/Context;

    const/4 v1, 0x0

    invoke-direct {p0}, Landroid/widget/BaseAdapter;-><init>()V

    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, Lcom/konka/epg/adapter/BookListAdapter;->m_BookData:Ljava/util/ArrayList;

    new-instance v0, Landroid/text/format/Time;

    invoke-direct {v0}, Landroid/text/format/Time;-><init>()V

    iput-object v0, p0, Lcom/konka/epg/adapter/BookListAdapter;->time:Landroid/text/format/Time;

    iput v1, p0, Lcom/konka/epg/adapter/BookListAdapter;->EPG_EVENT_NONE:I

    const/4 v0, 0x1

    iput v0, p0, Lcom/konka/epg/adapter/BookListAdapter;->EPG_EVENT_REMIDER:I

    const/4 v0, 0x2

    iput v0, p0, Lcom/konka/epg/adapter/BookListAdapter;->EPG_EVENT_RECORDER:I

    iput v1, p0, Lcom/konka/epg/adapter/BookListAdapter;->EPG_REPEAT_AUTO:I

    const/16 v0, 0x81

    iput v0, p0, Lcom/konka/epg/adapter/BookListAdapter;->EPG_REPEAT_ONCE:I

    const/16 v0, 0x7f

    iput v0, p0, Lcom/konka/epg/adapter/BookListAdapter;->EPG_REPEAT_DAILY:I

    const/16 v0, 0x82

    iput v0, p0, Lcom/konka/epg/adapter/BookListAdapter;->EPG_REPEAT_WEEKLY:I

    invoke-static {p1}, Landroid/view/LayoutInflater;->from(Landroid/content/Context;)Landroid/view/LayoutInflater;

    move-result-object v0

    iput-object v0, p0, Lcom/konka/epg/adapter/BookListAdapter;->mInflater:Landroid/view/LayoutInflater;

    return-void
.end method


# virtual methods
.method public addItem(Lcom/konka/kkinterface/tv/DtvInterface$EPG_SCHEDULE_EVENT_INFO;)V
    .locals 1
    .param p1    # Lcom/konka/kkinterface/tv/DtvInterface$EPG_SCHEDULE_EVENT_INFO;

    iget-object v0, p0, Lcom/konka/epg/adapter/BookListAdapter;->m_BookData:Ljava/util/ArrayList;

    invoke-virtual {v0, p1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    return-void
.end method

.method public delAllItem()V
    .locals 1

    iget-object v0, p0, Lcom/konka/epg/adapter/BookListAdapter;->m_BookData:Ljava/util/ArrayList;

    invoke-virtual {v0}, Ljava/util/ArrayList;->clear()V

    return-void
.end method

.method public delItem(I)V
    .locals 1
    .param p1    # I

    iget-object v0, p0, Lcom/konka/epg/adapter/BookListAdapter;->m_BookData:Ljava/util/ArrayList;

    invoke-virtual {v0, p1}, Ljava/util/ArrayList;->remove(I)Ljava/lang/Object;

    return-void
.end method

.method public getCount()I
    .locals 1

    iget-object v0, p0, Lcom/konka/epg/adapter/BookListAdapter;->m_BookData:Ljava/util/ArrayList;

    invoke-virtual {v0}, Ljava/util/ArrayList;->size()I

    move-result v0

    return v0
.end method

.method public getItem(I)Lcom/konka/kkinterface/tv/DtvInterface$EPG_SCHEDULE_EVENT_INFO;
    .locals 1
    .param p1    # I

    iget-object v0, p0, Lcom/konka/epg/adapter/BookListAdapter;->m_BookData:Ljava/util/ArrayList;

    invoke-virtual {v0, p1}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/konka/kkinterface/tv/DtvInterface$EPG_SCHEDULE_EVENT_INFO;

    return-object v0
.end method

.method public bridge synthetic getItem(I)Ljava/lang/Object;
    .locals 1

    invoke-virtual {p0, p1}, Lcom/konka/epg/adapter/BookListAdapter;->getItem(I)Lcom/konka/kkinterface/tv/DtvInterface$EPG_SCHEDULE_EVENT_INFO;

    move-result-object v0

    return-object v0
.end method

.method public getItemId(I)J
    .locals 2
    .param p1    # I

    int-to-long v0, p1

    return-wide v0
.end method

.method public getView(ILandroid/view/View;Landroid/view/ViewGroup;)Landroid/view/View;
    .locals 10
    .param p1    # I
    .param p2    # Landroid/view/View;
    .param p3    # Landroid/view/ViewGroup;

    const/4 v3, 0x0

    const v9, 0x7f08002a    # com.konka.epg.R.string.epg_reminder_once_str

    const/4 v8, 0x0

    const/4 v7, 0x4

    const/4 v0, 0x0

    if-nez p2, :cond_0

    new-instance v0, Lcom/konka/epg/adapter/BookListAdapter$ViewHolder;

    invoke-direct {v0, p0, v3}, Lcom/konka/epg/adapter/BookListAdapter$ViewHolder;-><init>(Lcom/konka/epg/adapter/BookListAdapter;Lcom/konka/epg/adapter/BookListAdapter$ViewHolder;)V

    iget-object v1, p0, Lcom/konka/epg/adapter/BookListAdapter;->mInflater:Landroid/view/LayoutInflater;

    const v2, 0x7f030001    # com.konka.epg.R.layout.epg_bookmenu_book_listitem

    invoke-virtual {v1, v2, v3}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;)Landroid/view/View;

    move-result-object p2

    const v1, 0x7f0a0008    # com.konka.epg.R.id.epg_booklist_channelid_text

    invoke-virtual {p2, v1}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v1

    check-cast v1, Landroid/widget/TextView;

    iput-object v1, v0, Lcom/konka/epg/adapter/BookListAdapter$ViewHolder;->channelId:Landroid/widget/TextView;

    const v1, 0x7f0a0009    # com.konka.epg.R.id.epg_booklist_channelname_text

    invoke-virtual {p2, v1}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v1

    check-cast v1, Landroid/widget/TextView;

    iput-object v1, v0, Lcom/konka/epg/adapter/BookListAdapter$ViewHolder;->channelName:Landroid/widget/TextView;

    const v1, 0x7f0a000a    # com.konka.epg.R.id.epg_booklist_eventmode_text

    invoke-virtual {p2, v1}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v1

    check-cast v1, Landroid/widget/TextView;

    iput-object v1, v0, Lcom/konka/epg/adapter/BookListAdapter$ViewHolder;->eventmode:Landroid/widget/TextView;

    const v1, 0x7f0a000b    # com.konka.epg.R.id.epg_booklist_view_item_mode

    invoke-virtual {p2, v1}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v1

    check-cast v1, Landroid/widget/ImageView;

    iput-object v1, v0, Lcom/konka/epg/adapter/BookListAdapter$ViewHolder;->modeIcon:Landroid/widget/ImageView;

    const v1, 0x7f0a000c    # com.konka.epg.R.id.epg_booklist_view_item_reminder

    invoke-virtual {p2, v1}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v1

    check-cast v1, Landroid/widget/ImageView;

    iput-object v1, v0, Lcom/konka/epg/adapter/BookListAdapter$ViewHolder;->reminderIcon:Landroid/widget/ImageView;

    const v1, 0x7f0a000d    # com.konka.epg.R.id.epg_booklist_view_item_video

    invoke-virtual {p2, v1}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v1

    check-cast v1, Landroid/widget/ImageView;

    iput-object v1, v0, Lcom/konka/epg/adapter/BookListAdapter$ViewHolder;->recordIcon:Landroid/widget/ImageView;

    const v1, 0x7f0a000e    # com.konka.epg.R.id.epg_booklist_eventname_text

    invoke-virtual {p2, v1}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v1

    check-cast v1, Landroid/widget/TextView;

    iput-object v1, v0, Lcom/konka/epg/adapter/BookListAdapter$ViewHolder;->eventName:Landroid/widget/TextView;

    invoke-virtual {p2, v0}, Landroid/view/View;->setTag(Ljava/lang/Object;)V

    :goto_0
    iget-object v2, v0, Lcom/konka/epg/adapter/BookListAdapter$ViewHolder;->channelId:Landroid/widget/TextView;

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    iget-object v1, p0, Lcom/konka/epg/adapter/BookListAdapter;->m_BookData:Ljava/util/ArrayList;

    invoke-virtual {v1, p1}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/konka/kkinterface/tv/DtvInterface$EPG_SCHEDULE_EVENT_INFO;

    iget v1, v1, Lcom/konka/kkinterface/tv/DtvInterface$EPG_SCHEDULE_EVENT_INFO;->progNumber:I

    invoke-virtual {v3, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v2, v1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    iget-object v2, v0, Lcom/konka/epg/adapter/BookListAdapter$ViewHolder;->channelName:Landroid/widget/TextView;

    iget-object v1, p0, Lcom/konka/epg/adapter/BookListAdapter;->m_BookData:Ljava/util/ArrayList;

    invoke-virtual {v1, p1}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/konka/kkinterface/tv/DtvInterface$EPG_SCHEDULE_EVENT_INFO;

    iget-object v1, v1, Lcom/konka/kkinterface/tv/DtvInterface$EPG_SCHEDULE_EVENT_INFO;->progName:Ljava/lang/String;

    invoke-virtual {v2, v1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    iget-object v2, p0, Lcom/konka/epg/adapter/BookListAdapter;->time:Landroid/text/format/Time;

    iget-object v1, p0, Lcom/konka/epg/adapter/BookListAdapter;->m_BookData:Ljava/util/ArrayList;

    invoke-virtual {v1, p1}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/konka/kkinterface/tv/DtvInterface$EPG_SCHEDULE_EVENT_INFO;

    iget v1, v1, Lcom/konka/kkinterface/tv/DtvInterface$EPG_SCHEDULE_EVENT_INFO;->startTime:I

    int-to-long v3, v1

    const-wide/16 v5, 0x3e8

    mul-long/2addr v3, v5

    invoke-virtual {v2, v3, v4}, Landroid/text/format/Time;->set(J)V

    iget-object v2, v0, Lcom/konka/epg/adapter/BookListAdapter$ViewHolder;->eventName:Landroid/widget/TextView;

    new-instance v1, Ljava/lang/StringBuilder;

    iget-object v3, p0, Lcom/konka/epg/adapter/BookListAdapter;->time:Landroid/text/format/Time;

    invoke-static {v3}, Lcom/konka/epg/util/EpgFormat;->formatDayText(Landroid/text/format/Time;)Ljava/lang/String;

    move-result-object v3

    invoke-static {v3}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v3

    invoke-direct {v1, v3}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    const-string v3, " \t"

    invoke-virtual {v1, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    iget-object v1, p0, Lcom/konka/epg/adapter/BookListAdapter;->m_BookData:Ljava/util/ArrayList;

    invoke-virtual {v1, p1}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/konka/kkinterface/tv/DtvInterface$EPG_SCHEDULE_EVENT_INFO;

    iget v1, v1, Lcom/konka/kkinterface/tv/DtvInterface$EPG_SCHEDULE_EVENT_INFO;->startTime:I

    invoke-static {v1}, Lcom/konka/epg/util/EpgFormat;->formatTimeText(I)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v3, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v3, "\t"

    invoke-virtual {v1, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    iget-object v1, p0, Lcom/konka/epg/adapter/BookListAdapter;->m_BookData:Ljava/util/ArrayList;

    invoke-virtual {v1, p1}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/konka/kkinterface/tv/DtvInterface$EPG_SCHEDULE_EVENT_INFO;

    iget-object v1, v1, Lcom/konka/kkinterface/tv/DtvInterface$EPG_SCHEDULE_EVENT_INFO;->eventName:Ljava/lang/String;

    invoke-virtual {v3, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v2, v1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    iget-object v1, p0, Lcom/konka/epg/adapter/BookListAdapter;->m_BookData:Ljava/util/ArrayList;

    invoke-virtual {v1, p1}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/konka/kkinterface/tv/DtvInterface$EPG_SCHEDULE_EVENT_INFO;

    iget v1, v1, Lcom/konka/kkinterface/tv/DtvInterface$EPG_SCHEDULE_EVENT_INFO;->timerType:I

    const/4 v2, 0x1

    if-ne v1, v2, :cond_1

    iget-object v1, v0, Lcom/konka/epg/adapter/BookListAdapter$ViewHolder;->reminderIcon:Landroid/widget/ImageView;

    invoke-virtual {v1, v8}, Landroid/widget/ImageView;->setVisibility(I)V

    :goto_1
    iget-object v1, p0, Lcom/konka/epg/adapter/BookListAdapter;->m_BookData:Ljava/util/ArrayList;

    invoke-virtual {v1, p1}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/konka/kkinterface/tv/DtvInterface$EPG_SCHEDULE_EVENT_INFO;

    iget v1, v1, Lcom/konka/kkinterface/tv/DtvInterface$EPG_SCHEDULE_EVENT_INFO;->timerType:I

    const/4 v2, 0x2

    if-ne v1, v2, :cond_2

    iget-object v1, v0, Lcom/konka/epg/adapter/BookListAdapter$ViewHolder;->recordIcon:Landroid/widget/ImageView;

    invoke-virtual {v1, v8}, Landroid/widget/ImageView;->setVisibility(I)V

    :goto_2
    iget-object v1, v0, Lcom/konka/epg/adapter/BookListAdapter$ViewHolder;->modeIcon:Landroid/widget/ImageView;

    invoke-virtual {v1, v7}, Landroid/widget/ImageView;->setVisibility(I)V

    iget-object v1, p0, Lcom/konka/epg/adapter/BookListAdapter;->m_BookData:Ljava/util/ArrayList;

    invoke-virtual {v1, p1}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/konka/kkinterface/tv/DtvInterface$EPG_SCHEDULE_EVENT_INFO;

    iget v1, v1, Lcom/konka/kkinterface/tv/DtvInterface$EPG_SCHEDULE_EVENT_INFO;->repeatMode:I

    packed-switch v1, :pswitch_data_0

    :pswitch_0
    iget-object v1, v0, Lcom/konka/epg/adapter/BookListAdapter$ViewHolder;->eventmode:Landroid/widget/TextView;

    invoke-virtual {v1, v9}, Landroid/widget/TextView;->setText(I)V

    :goto_3
    return-object p2

    :cond_0
    invoke-virtual {p2}, Landroid/view/View;->getTag()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/konka/epg/adapter/BookListAdapter$ViewHolder;

    goto/16 :goto_0

    :cond_1
    iget-object v1, v0, Lcom/konka/epg/adapter/BookListAdapter$ViewHolder;->reminderIcon:Landroid/widget/ImageView;

    invoke-virtual {v1, v7}, Landroid/widget/ImageView;->setVisibility(I)V

    goto :goto_1

    :cond_2
    iget-object v1, v0, Lcom/konka/epg/adapter/BookListAdapter$ViewHolder;->recordIcon:Landroid/widget/ImageView;

    invoke-virtual {v1, v7}, Landroid/widget/ImageView;->setVisibility(I)V

    goto :goto_2

    :pswitch_1
    iget-object v1, v0, Lcom/konka/epg/adapter/BookListAdapter$ViewHolder;->eventmode:Landroid/widget/TextView;

    invoke-virtual {v1, v9}, Landroid/widget/TextView;->setText(I)V

    goto :goto_3

    :pswitch_2
    iget-object v1, v0, Lcom/konka/epg/adapter/BookListAdapter$ViewHolder;->eventmode:Landroid/widget/TextView;

    const v2, 0x7f08002b    # com.konka.epg.R.string.epg_reminder_daily_str

    invoke-virtual {v1, v2}, Landroid/widget/TextView;->setText(I)V

    goto :goto_3

    :pswitch_3
    iget-object v1, v0, Lcom/konka/epg/adapter/BookListAdapter$ViewHolder;->eventmode:Landroid/widget/TextView;

    const v2, 0x7f08002c    # com.konka.epg.R.string.epg_reminder_weekly_str

    invoke-virtual {v1, v2}, Landroid/widget/TextView;->setText(I)V

    goto :goto_3

    nop

    :pswitch_data_0
    .packed-switch 0x7f
        :pswitch_2
        :pswitch_0
        :pswitch_1
        :pswitch_3
    .end packed-switch
.end method
