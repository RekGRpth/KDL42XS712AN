.class public Lcom/konka/epg/TVRootApp;
.super Landroid/app/Application;
.source "TVRootApp.java"


# static fields
.field public static mHandler:Landroid/os/Handler;

.field private static pictureSkin:Lcom/mstar/android/tv/TvPictureManager;

.field private static s3dSkin:Lcom/mstar/android/tv/TvS3DManager;


# instance fields
.field private audioSkin:Lcom/mstar/android/tv/TvAudioManager;

.field private commonSkin:Lcom/mstar/android/tv/TvCommonManager;

.field private pipSkin:Lcom/mstar/android/tv/TvPipPopManager;

.field private timerSkin:Lcom/mstar/android/tv/TvTimerManager;

.field private tvDeskProvider:Lcom/konka/kkinterface/tv/TvDeskProvider;

.field private wmParams:Landroid/view/WindowManager$LayoutParams;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    const/4 v0, 0x0

    sput-object v0, Lcom/konka/epg/TVRootApp;->pictureSkin:Lcom/mstar/android/tv/TvPictureManager;

    sput-object v0, Lcom/konka/epg/TVRootApp;->s3dSkin:Lcom/mstar/android/tv/TvS3DManager;

    sput-object v0, Lcom/konka/epg/TVRootApp;->mHandler:Landroid/os/Handler;

    return-void
.end method

.method public constructor <init>()V
    .locals 1

    const/4 v0, 0x0

    invoke-direct {p0}, Landroid/app/Application;-><init>()V

    iput-object v0, p0, Lcom/konka/epg/TVRootApp;->tvDeskProvider:Lcom/konka/kkinterface/tv/TvDeskProvider;

    iput-object v0, p0, Lcom/konka/epg/TVRootApp;->commonSkin:Lcom/mstar/android/tv/TvCommonManager;

    iput-object v0, p0, Lcom/konka/epg/TVRootApp;->pipSkin:Lcom/mstar/android/tv/TvPipPopManager;

    iput-object v0, p0, Lcom/konka/epg/TVRootApp;->audioSkin:Lcom/mstar/android/tv/TvAudioManager;

    iput-object v0, p0, Lcom/konka/epg/TVRootApp;->timerSkin:Lcom/mstar/android/tv/TvTimerManager;

    new-instance v0, Landroid/view/WindowManager$LayoutParams;

    invoke-direct {v0}, Landroid/view/WindowManager$LayoutParams;-><init>()V

    iput-object v0, p0, Lcom/konka/epg/TVRootApp;->wmParams:Landroid/view/WindowManager$LayoutParams;

    return-void
.end method

.method public static getPictureSkin()Lcom/mstar/android/tv/TvPictureManager;
    .locals 1

    sget-object v0, Lcom/konka/epg/TVRootApp;->pictureSkin:Lcom/mstar/android/tv/TvPictureManager;

    return-object v0
.end method

.method public static getS3dSkin()Lcom/mstar/android/tv/TvS3DManager;
    .locals 1

    sget-object v0, Lcom/konka/epg/TVRootApp;->s3dSkin:Lcom/mstar/android/tv/TvS3DManager;

    return-object v0
.end method


# virtual methods
.method public getAudioSkin()Lcom/mstar/android/tv/TvAudioManager;
    .locals 1

    iget-object v0, p0, Lcom/konka/epg/TVRootApp;->audioSkin:Lcom/mstar/android/tv/TvAudioManager;

    return-object v0
.end method

.method public getCommonSkin()Lcom/mstar/android/tv/TvCommonManager;
    .locals 1

    iget-object v0, p0, Lcom/konka/epg/TVRootApp;->commonSkin:Lcom/mstar/android/tv/TvCommonManager;

    return-object v0
.end method

.method public getMywmParams()Landroid/view/WindowManager$LayoutParams;
    .locals 1

    iget-object v0, p0, Lcom/konka/epg/TVRootApp;->wmParams:Landroid/view/WindowManager$LayoutParams;

    return-object v0
.end method

.method public getPipSkin()Lcom/mstar/android/tv/TvPipPopManager;
    .locals 1

    iget-object v0, p0, Lcom/konka/epg/TVRootApp;->pipSkin:Lcom/mstar/android/tv/TvPipPopManager;

    return-object v0
.end method

.method public getTimerSkin()Lcom/mstar/android/tv/TvTimerManager;
    .locals 1

    iget-object v0, p0, Lcom/konka/epg/TVRootApp;->timerSkin:Lcom/mstar/android/tv/TvTimerManager;

    return-object v0
.end method

.method public getTvDeskProvider()Lcom/konka/kkinterface/tv/TvDeskProvider;
    .locals 1

    iget-object v0, p0, Lcom/konka/epg/TVRootApp;->tvDeskProvider:Lcom/konka/kkinterface/tv/TvDeskProvider;

    return-object v0
.end method

.method public onCreate()V
    .locals 2

    invoke-super {p0}, Landroid/app/Application;->onCreate()V

    invoke-virtual {p0}, Lcom/konka/epg/TVRootApp;->getApplicationContext()Landroid/content/Context;

    move-result-object v0

    invoke-static {v0}, Lcom/konka/kkimplements/tv/TvDeskProviderImpl;->getInstance(Landroid/content/Context;)Lcom/konka/kkinterface/tv/TvDeskProvider;

    move-result-object v0

    iput-object v0, p0, Lcom/konka/epg/TVRootApp;->tvDeskProvider:Lcom/konka/kkinterface/tv/TvDeskProvider;

    invoke-static {}, Lcom/mstar/android/tv/TvCommonManager;->getInstance()Lcom/mstar/android/tv/TvCommonManager;

    move-result-object v0

    iput-object v0, p0, Lcom/konka/epg/TVRootApp;->commonSkin:Lcom/mstar/android/tv/TvCommonManager;

    invoke-static {}, Lcom/mstar/android/tv/TvPipPopManager;->getInstance()Lcom/mstar/android/tv/TvPipPopManager;

    move-result-object v0

    iput-object v0, p0, Lcom/konka/epg/TVRootApp;->pipSkin:Lcom/mstar/android/tv/TvPipPopManager;

    invoke-static {}, Lcom/mstar/android/tv/TvAudioManager;->getInstance()Lcom/mstar/android/tv/TvAudioManager;

    move-result-object v0

    iput-object v0, p0, Lcom/konka/epg/TVRootApp;->audioSkin:Lcom/mstar/android/tv/TvAudioManager;

    invoke-static {}, Lcom/mstar/android/tv/TvPictureManager;->getInstance()Lcom/mstar/android/tv/TvPictureManager;

    move-result-object v0

    sput-object v0, Lcom/konka/epg/TVRootApp;->pictureSkin:Lcom/mstar/android/tv/TvPictureManager;

    invoke-static {}, Lcom/mstar/android/tv/TvTimerManager;->getInstance()Lcom/mstar/android/tv/TvTimerManager;

    move-result-object v0

    iput-object v0, p0, Lcom/konka/epg/TVRootApp;->timerSkin:Lcom/mstar/android/tv/TvTimerManager;

    invoke-static {}, Lcom/mstar/android/tv/TvS3DManager;->getInstance()Lcom/mstar/android/tv/TvS3DManager;

    move-result-object v0

    sput-object v0, Lcom/konka/epg/TVRootApp;->s3dSkin:Lcom/mstar/android/tv/TvS3DManager;

    iget-object v0, p0, Lcom/konka/epg/TVRootApp;->tvDeskProvider:Lcom/konka/kkinterface/tv/TvDeskProvider;

    invoke-interface {v0}, Lcom/konka/kkinterface/tv/TvDeskProvider;->getEpgManagerInstance()Lcom/konka/kkinterface/tv/EpgDesk;

    move-result-object v0

    invoke-interface {v0}, Lcom/konka/kkinterface/tv/EpgDesk;->openDB()V

    new-instance v0, Landroid/os/Handler;

    invoke-direct {v0}, Landroid/os/Handler;-><init>()V

    sput-object v0, Lcom/konka/epg/TVRootApp;->mHandler:Landroid/os/Handler;

    invoke-static {}, Lcom/konka/epg/CrashHandler;->getInstance()Lcom/konka/epg/CrashHandler;

    move-result-object v0

    invoke-virtual {p0}, Lcom/konka/epg/TVRootApp;->getApplicationContext()Landroid/content/Context;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/konka/epg/CrashHandler;->init(Landroid/content/Context;)V

    const-string v0, "TVRootApp Create"

    invoke-static {v0}, Lcom/konka/epg/util/EpgDebugPrint;->Debug(Ljava/lang/String;)V

    return-void
.end method

.method public onTerminate()V
    .locals 1

    iget-object v0, p0, Lcom/konka/epg/TVRootApp;->tvDeskProvider:Lcom/konka/kkinterface/tv/TvDeskProvider;

    invoke-interface {v0}, Lcom/konka/kkinterface/tv/TvDeskProvider;->getEpgManagerInstance()Lcom/konka/kkinterface/tv/EpgDesk;

    move-result-object v0

    invoke-interface {v0}, Lcom/konka/kkinterface/tv/EpgDesk;->closeDB()V

    invoke-super {p0}, Landroid/app/Application;->onTerminate()V

    return-void
.end method
