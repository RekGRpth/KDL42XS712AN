.class public final Lcom/konka/epg/R$drawable;
.super Ljava/lang/Object;
.source "R.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/konka/epg/R;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x19
    name = "drawable"
.end annotation


# static fields
.field public static final alpha_img:I = 0x7f020000

.field public static final booklistselector:I = 0x7f020001

.field public static final buttonselector:I = 0x7f020002

.field public static final channellistselector:I = 0x7f020003

.field public static final common_img_back:I = 0x7f020004

.field public static final common_img_ok:I = 0x7f020005

.field public static final common_img_pagepoint_disable:I = 0x7f020006

.field public static final common_img_pagepoint_enable:I = 0x7f020007

.field public static final divider:I = 0x7f020008

.field public static final epg_act_item_select:I = 0x7f020009

.field public static final epg_bg:I = 0x7f02000a

.field public static final epg_blue:I = 0x7f02000b

.field public static final epg_book_bg:I = 0x7f02000c

.field public static final epg_book_event:I = 0x7f02000d

.field public static final epg_book_item_select:I = 0x7f02000e

.field public static final epg_btn_focus:I = 0x7f02000f

.field public static final epg_btn_unfocus:I = 0x7f020010

.field public static final epg_channel_play:I = 0x7f020011

.field public static final epg_clock:I = 0x7f020012

.field public static final epg_day_img:I = 0x7f020013

.field public static final epg_del_hint:I = 0x7f020014

.field public static final epg_event_info:I = 0x7f020015

.field public static final epg_event_play:I = 0x7f020016

.field public static final epg_event_play1:I = 0x7f020017

.field public static final epg_image_bookmanager_icon:I = 0x7f020018

.field public static final epg_image_left_arrow_icon:I = 0x7f020019

.field public static final epg_image_record_icon:I = 0x7f02001a

.field public static final epg_image_right_arrow_icon:I = 0x7f02001b

.field public static final epg_info_bg:I = 0x7f02001c

.field public static final epg_item_select:I = 0x7f02001d

.field public static final epg_search_edit:I = 0x7f02001e

.field public static final epg_search_result_down:I = 0x7f02001f

.field public static final epg_tv_item_select:I = 0x7f020020

.field public static final epg_type:I = 0x7f020021

.field public static final epgtypeencryp:I = 0x7f020022

.field public static final epgtypenull:I = 0x7f020023

.field public static final eventlistselector:I = 0x7f020024

.field public static final icon:I = 0x7f020025

.field public static final program_epg_recorder_select:I = 0x7f020026

.field public static final program_epg_select:I = 0x7f020027

.field public static final programme_epg_img_focus:I = 0x7f020028

.field public static final programme_epg_img_focus2:I = 0x7f020029

.field public static final record_img_bg:I = 0x7f02002a

.field public static final record_img_hint_r:I = 0x7f02002b

.field public static final record_img_star:I = 0x7f02002c

.field public static final record_img_star2:I = 0x7f02002d

.field public static final reminder_img_bg:I = 0x7f02002e

.field public static final reminder_img_hint_b:I = 0x7f02002f

.field public static final reminder_img_star:I = 0x7f020030

.field public static final schedule_list_img_icon_mode_unfocus:I = 0x7f020031

.field public static final schedule_list_img_icon_reminder_unfocus:I = 0x7f020032

.field public static final schedule_list_img_icon_video_unfocus:I = 0x7f020033

.field public static final textcolor:I = 0x7f020034


# direct methods
.method public constructor <init>()V
    .locals 0

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method
