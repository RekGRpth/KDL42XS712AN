.class public Lcom/konka/epg/util/EpgConstant;
.super Ljava/lang/Object;
.source "EpgConstant.java"


# static fields
.field public static final EPG_DRAW_SS_NO_CI_MODULE:I = 0x4b0

.field public static final EPG_DRAW_SS_SCRAMBLED_PROGRAM:I = 0x4b1

.field public static final EPG_INVALID_PARAMETER:I = -0x1

.field public static final EPG_SCREEN_MODE:I = 0x44e

.field public static final EPG_SIGNAL_LOCK:I = 0x44d

.field public static final EPG_SIGNAL_UNLOCK:I = 0x44c

.field public static final EPG_SS_AUDIO_ONLY:I = 0x455

.field public static final EPG_SS_CH_BLOCK:I = 0x453

.field public static final EPG_SS_CI_PLUS_AUTHENTICATION:I = 0x451

.field public static final EPG_SS_COMMON_VIDEO:I = 0x457

.field public static final EPG_SS_DATA_ONLY:I = 0x456

.field public static final EPG_SS_INVALID_PMT:I = 0x459

.field public static final EPG_SS_INVALID_SERVICE:I = 0x44f

.field public static final EPG_SS_MAX:I = 0x45a

.field public static final EPG_SS_NO_CI_MODULE:I = 0x450

.field public static final EPG_SS_PARENTAL_BLOCK:I = 0x454

.field public static final EPG_SS_SCRAMBLED_PROGRAM:I = 0x452

.field public static final EPG_SS_UNSUPPORTED_FORMAT:I = 0x458

.field public static final M_COMPARE_TIME:I = 0x1004

.field public static final M_PROG_SEL_DELAY:I = 0x1007

.field public static final M_REFRESH_EVENTLIST_ALL:I = 0x1001

.field public static final M_REFRESH_EVENTLIST_DAY:I = 0x1002

.field public static final M_REFRESH_SYSTIME:I = 0x1000

.field public static final M_SCALE_MAIN_WINDOW:I = 0x1008

.field public static final M_SERVICETYPE_SEL_DELAY:I = 0x1009

.field public static final M_SETDISPLAYWIN_BY_TIME:I = 0x1006

.field public static final R_DISPLAY_WIN_HEIGHT:I = 0x14b

.field public static final R_DISPLAY_WIN_WIDTH:I = 0x259

.field public static final R_DISPLAY_WIN_X:I = 0x83

.field public static final R_DISPLAY_WIN_Y:I = 0x27d

.field public static final T_REFRESH_FIRSTDAY_DELAY:I = 0x1f40

.field public static final T_REFRESH_SYSTIME_DELAY:I = 0x7530


# direct methods
.method public constructor <init>()V
    .locals 0

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method
