.class public Lcom/konka/epg/util/EpgToast;
.super Ljava/lang/Object;
.source "EpgToast.java"


# static fields
.field private static _mCxt:Landroid/content/Context;

.field private static _mInstance:Lcom/konka/epg/util/EpgToast;


# instance fields
.field private _mToast:Landroid/widget/Toast;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    const/4 v0, 0x0

    sput-object v0, Lcom/konka/epg/util/EpgToast;->_mInstance:Lcom/konka/epg/util/EpgToast;

    sput-object v0, Lcom/konka/epg/util/EpgToast;->_mCxt:Landroid/content/Context;

    return-void
.end method

.method public constructor <init>()V
    .locals 3

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    const/4 v0, 0x0

    iput-object v0, p0, Lcom/konka/epg/util/EpgToast;->_mToast:Landroid/widget/Toast;

    sget-object v0, Lcom/konka/epg/util/EpgToast;->_mCxt:Landroid/content/Context;

    const/high16 v1, 0x7f080000    # com.konka.epg.R.string.epg_App_Name_Str

    const/4 v2, 0x0

    invoke-static {v0, v1, v2}, Landroid/widget/Toast;->makeText(Landroid/content/Context;II)Landroid/widget/Toast;

    move-result-object v0

    iput-object v0, p0, Lcom/konka/epg/util/EpgToast;->_mToast:Landroid/widget/Toast;

    return-void
.end method

.method public static getInstance()Lcom/konka/epg/util/EpgToast;
    .locals 1

    sget-object v0, Lcom/konka/epg/util/EpgToast;->_mInstance:Lcom/konka/epg/util/EpgToast;

    if-nez v0, :cond_0

    new-instance v0, Lcom/konka/epg/util/EpgToast;

    invoke-direct {v0}, Lcom/konka/epg/util/EpgToast;-><init>()V

    sput-object v0, Lcom/konka/epg/util/EpgToast;->_mInstance:Lcom/konka/epg/util/EpgToast;

    :cond_0
    sget-object v0, Lcom/konka/epg/util/EpgToast;->_mInstance:Lcom/konka/epg/util/EpgToast;

    return-object v0
.end method

.method public static setContext(Landroid/content/Context;)V
    .locals 0
    .param p0    # Landroid/content/Context;

    sput-object p0, Lcom/konka/epg/util/EpgToast;->_mCxt:Landroid/content/Context;

    return-void
.end method


# virtual methods
.method public hideToast()V
    .locals 1

    iget-object v0, p0, Lcom/konka/epg/util/EpgToast;->_mToast:Landroid/widget/Toast;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/konka/epg/util/EpgToast;->_mToast:Landroid/widget/Toast;

    invoke-virtual {v0}, Landroid/widget/Toast;->cancel()V

    :cond_0
    return-void
.end method

.method public showToast(IIII)V
    .locals 2
    .param p1    # I
    .param p2    # I
    .param p3    # I
    .param p4    # I

    iget-object v0, p0, Lcom/konka/epg/util/EpgToast;->_mToast:Landroid/widget/Toast;

    const/16 v1, 0x11

    invoke-virtual {v0, v1, p2, p3}, Landroid/widget/Toast;->setGravity(III)V

    iget-object v0, p0, Lcom/konka/epg/util/EpgToast;->_mToast:Landroid/widget/Toast;

    invoke-virtual {v0}, Landroid/widget/Toast;->getView()Landroid/view/View;

    move-result-object v0

    const v1, 0x102000b    # android.R.id.message

    invoke-virtual {v0, v1}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    const/high16 v1, 0x41900000    # 18.0f

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setTextSize(F)V

    iget-object v0, p0, Lcom/konka/epg/util/EpgToast;->_mToast:Landroid/widget/Toast;

    invoke-virtual {v0, p4}, Landroid/widget/Toast;->setDuration(I)V

    iget-object v0, p0, Lcom/konka/epg/util/EpgToast;->_mToast:Landroid/widget/Toast;

    invoke-virtual {v0, p1}, Landroid/widget/Toast;->setText(I)V

    iget-object v0, p0, Lcom/konka/epg/util/EpgToast;->_mToast:Landroid/widget/Toast;

    invoke-virtual {v0}, Landroid/widget/Toast;->show()V

    return-void
.end method
