.class public interface abstract Lcom/konka/epg/service/EpgServiceProvider;
.super Ljava/lang/Object;
.source "EpgServiceProvider.java"


# virtual methods
.method public abstract bookEvent(Lcom/konka/kkimplements/tv/vo/KonkaEventInfo;)Lcom/konka/kkinterface/tv/DtvInterface$RESULT_ADDEVENT;
.end method

.method public abstract closeDB()V
.end method

.method public abstract closeTVWidgetSurface()V
.end method

.method public abstract delBookedEvent(IZ)Z
.end method

.method public abstract delayProgSel(I)V
.end method

.method public abstract delayServiceTypeSel(Lcom/mstar/android/tvapi/common/vo/EnumServiceType;I)V
.end method

.method public abstract getCurServiceType()I
.end method

.method public abstract getDtvChannlAdapter()Lcom/konka/epg/adapter/ChannelListAdapter;
.end method

.method public abstract getEventDescribe(I)Ljava/lang/String;
.end method

.method public abstract getEventOfDay(Lcom/mstar/android/tvapi/common/vo/EnumServiceType;I)V
.end method

.method public abstract getEventOfFirstDay(Lcom/mstar/android/tvapi/common/vo/EnumServiceType;I)V
.end method

.method public abstract getEventOfWeek(Lcom/mstar/android/tvapi/common/vo/EnumServiceType;I)V
.end method

.method public abstract getHandler()Lcom/konka/epg/ui/EpgMainMenuActivity$EpgHandler;
.end method

.method public abstract getM_WeekEventAdapterList()Ljava/util/ArrayList;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/ArrayList",
            "<",
            "Lcom/konka/epg/adapter/EventListAdapter;",
            ">;"
        }
    .end annotation
.end method

.method public abstract getRadioChannelAdapter()Lcom/konka/epg/adapter/ChannelListAdapter;
.end method

.method public abstract getTimeInfo()Landroid/text/format/Time;
.end method

.method public abstract initPosInChannelList()I
.end method

.method public abstract initTimeInfoFromDtv()V
.end method

.method public abstract isSignalStabled()Z
.end method

.method public abstract openDB()V
.end method

.method public abstract refreshDayEvent(Lcom/mstar/android/tvapi/common/vo/EnumServiceType;II)V
.end method

.method public abstract releaseHandle()V
.end method

.method public abstract scaleWindow()V
.end method

.method public abstract setActivity(Landroid/app/Activity;)V
.end method

.method public abstract setDisplayWin(IIII)V
.end method

.method public abstract showTVWidgetSurface(IIII)V
.end method
