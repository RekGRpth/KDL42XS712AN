.class Lcom/konka/epg/service/EpgSrvProImp$6;
.super Ljava/lang/Object;
.source "EpgSrvProImp.java"

# interfaces
.implements Landroid/view/SurfaceHolder$Callback;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/konka/epg/service/EpgSrvProImp;->showTVWidgetSurface(IIII)V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/konka/epg/service/EpgSrvProImp;


# direct methods
.method constructor <init>(Lcom/konka/epg/service/EpgSrvProImp;)V
    .locals 0

    iput-object p1, p0, Lcom/konka/epg/service/EpgSrvProImp$6;->this$0:Lcom/konka/epg/service/EpgSrvProImp;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public surfaceChanged(Landroid/view/SurfaceHolder;III)V
    .locals 0
    .param p1    # Landroid/view/SurfaceHolder;
    .param p2    # I
    .param p3    # I
    .param p4    # I

    return-void
.end method

.method public surfaceCreated(Landroid/view/SurfaceHolder;)V
    .locals 2
    .param p1    # Landroid/view/SurfaceHolder;

    const-string v1, "surface created"

    invoke-static {v1}, Lcom/konka/epg/util/EpgDebugPrint;->Debug(Ljava/lang/String;)V

    :try_start_0
    iget-object v1, p0, Lcom/konka/epg/service/EpgSrvProImp$6;->this$0:Lcom/konka/epg/service/EpgSrvProImp;

    # getter for: Lcom/konka/epg/service/EpgSrvProImp;->mtvplayer:Lcom/mstar/android/tvapi/common/TvPlayer;
    invoke-static {v1}, Lcom/konka/epg/service/EpgSrvProImp;->access$9(Lcom/konka/epg/service/EpgSrvProImp;)Lcom/mstar/android/tvapi/common/TvPlayer;

    move-result-object v1

    invoke-interface {v1, p1}, Lcom/mstar/android/tvapi/common/TvPlayer;->setDisplay(Landroid/view/SurfaceHolder;)V
    :try_end_0
    .catch Lcom/mstar/android/tvapi/common/exception/TvCommonException; {:try_start_0 .. :try_end_0} :catch_0

    :goto_0
    return-void

    :catch_0
    move-exception v0

    invoke-virtual {v0}, Lcom/mstar/android/tvapi/common/exception/TvCommonException;->printStackTrace()V

    goto :goto_0
.end method

.method public surfaceDestroyed(Landroid/view/SurfaceHolder;)V
    .locals 1
    .param p1    # Landroid/view/SurfaceHolder;

    const-string v0, "surface destory"

    invoke-static {v0}, Lcom/konka/epg/util/EpgDebugPrint;->Debug(Ljava/lang/String;)V

    return-void
.end method
