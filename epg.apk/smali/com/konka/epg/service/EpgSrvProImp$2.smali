.class Lcom/konka/epg/service/EpgSrvProImp$2;
.super Ljava/lang/Object;
.source "EpgSrvProImp.java"

# interfaces
.implements Ljava/lang/Runnable;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/konka/epg/service/EpgSrvProImp;->getEventOfWeek(Lcom/mstar/android/tvapi/common/vo/EnumServiceType;I)V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/konka/epg/service/EpgSrvProImp;

.field private final synthetic val$eServiceType:Lcom/mstar/android/tvapi/common/vo/EnumServiceType;

.field private final synthetic val$iChannel:I


# direct methods
.method constructor <init>(Lcom/konka/epg/service/EpgSrvProImp;Lcom/mstar/android/tvapi/common/vo/EnumServiceType;I)V
    .locals 0

    iput-object p1, p0, Lcom/konka/epg/service/EpgSrvProImp$2;->this$0:Lcom/konka/epg/service/EpgSrvProImp;

    iput-object p2, p0, Lcom/konka/epg/service/EpgSrvProImp$2;->val$eServiceType:Lcom/mstar/android/tvapi/common/vo/EnumServiceType;

    iput p3, p0, Lcom/konka/epg/service/EpgSrvProImp$2;->val$iChannel:I

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method static synthetic access$0(Lcom/konka/epg/service/EpgSrvProImp$2;)Lcom/konka/epg/service/EpgSrvProImp;
    .locals 1

    iget-object v0, p0, Lcom/konka/epg/service/EpgSrvProImp$2;->this$0:Lcom/konka/epg/service/EpgSrvProImp;

    return-object v0
.end method


# virtual methods
.method public run()V
    .locals 12

    const/4 v11, 0x6

    iget-object v8, p0, Lcom/konka/epg/service/EpgSrvProImp$2;->this$0:Lcom/konka/epg/service/EpgSrvProImp;

    iget-object v9, p0, Lcom/konka/epg/service/EpgSrvProImp$2;->val$eServiceType:Lcom/mstar/android/tvapi/common/vo/EnumServiceType;

    iget v10, p0, Lcom/konka/epg/service/EpgSrvProImp$2;->val$iChannel:I

    # invokes: Lcom/konka/epg/service/EpgSrvProImp;->getCurProgNum(Lcom/mstar/android/tvapi/common/vo/EnumServiceType;I)Landroid/content/ContentValues;
    invoke-static {v8, v9, v10}, Lcom/konka/epg/service/EpgSrvProImp;->access$5(Lcom/konka/epg/service/EpgSrvProImp;Lcom/mstar/android/tvapi/common/vo/EnumServiceType;I)Landroid/content/ContentValues;

    move-result-object v7

    const-string v8, "sServiceType"

    invoke-virtual {v7, v8}, Landroid/content/ContentValues;->getAsShort(Ljava/lang/String;)Ljava/lang/Short;

    move-result-object v8

    invoke-virtual {v8}, Ljava/lang/Short;->shortValue()S

    move-result v6

    const-string v8, "iProgNo"

    invoke-virtual {v7, v8}, Landroid/content/ContentValues;->getAsInteger(Ljava/lang/String;)Ljava/lang/Integer;

    move-result-object v8

    invoke-virtual {v8}, Ljava/lang/Integer;->intValue()I

    move-result v4

    const/4 v5, 0x0

    iget-object v8, p0, Lcom/konka/epg/service/EpgSrvProImp$2;->this$0:Lcom/konka/epg/service/EpgSrvProImp;

    iget-object v9, p0, Lcom/konka/epg/service/EpgSrvProImp$2;->val$eServiceType:Lcom/mstar/android/tvapi/common/vo/EnumServiceType;

    iget v10, p0, Lcom/konka/epg/service/EpgSrvProImp$2;->val$iChannel:I

    # invokes: Lcom/konka/epg/service/EpgSrvProImp;->setCurProg(Lcom/mstar/android/tvapi/common/vo/EnumServiceType;I)V
    invoke-static {v8, v9, v10}, Lcom/konka/epg/service/EpgSrvProImp;->access$6(Lcom/konka/epg/service/EpgSrvProImp;Lcom/mstar/android/tvapi/common/vo/EnumServiceType;I)V

    iget-object v8, p0, Lcom/konka/epg/service/EpgSrvProImp$2;->this$0:Lcom/konka/epg/service/EpgSrvProImp;

    # getter for: Lcom/konka/epg/service/EpgSrvProImp;->m_WeekEventAdapterList:Ljava/util/ArrayList;
    invoke-static {v8}, Lcom/konka/epg/service/EpgSrvProImp;->access$7(Lcom/konka/epg/service/EpgSrvProImp;)Ljava/util/ArrayList;

    move-result-object v9

    monitor-enter v9

    :try_start_0
    iget-object v8, p0, Lcom/konka/epg/service/EpgSrvProImp$2;->this$0:Lcom/konka/epg/service/EpgSrvProImp;

    # getter for: Lcom/konka/epg/service/EpgSrvProImp;->epgDesk:Lcom/konka/kkinterface/tv/EpgDesk;
    invoke-static {v8}, Lcom/konka/epg/service/EpgSrvProImp;->access$0(Lcom/konka/epg/service/EpgSrvProImp;)Lcom/konka/kkinterface/tv/EpgDesk;

    move-result-object v8

    invoke-interface {v8, v6, v4}, Lcom/konka/kkinterface/tv/EpgDesk;->getEventList(SI)Ljava/util/ArrayList;

    move-result-object v3

    const/4 v1, 0x0

    :goto_0
    const/4 v8, 0x7

    if-lt v1, v8, :cond_1

    if-eqz v3, :cond_0

    :goto_1
    invoke-virtual {v3}, Ljava/util/ArrayList;->size()I

    move-result v8

    if-lt v5, v8, :cond_2

    :cond_0
    monitor-exit v9
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    new-instance v0, Landroid/os/Bundle;

    invoke-direct {v0}, Landroid/os/Bundle;-><init>()V

    const-string v8, "Type"

    const/16 v9, 0x1001

    invoke-virtual {v0, v8, v9}, Landroid/os/Bundle;->putInt(Ljava/lang/String;I)V

    const-string v8, "ServiceType"

    invoke-virtual {v0, v8, v6}, Landroid/os/Bundle;->putInt(Ljava/lang/String;I)V

    const-string v8, "dayId"

    const/4 v9, 0x0

    invoke-virtual {v0, v8, v9}, Landroid/os/Bundle;->putInt(Ljava/lang/String;I)V

    const-string v8, "channelId"

    iget v9, p0, Lcom/konka/epg/service/EpgSrvProImp$2;->val$iChannel:I

    invoke-virtual {v0, v8, v9}, Landroid/os/Bundle;->putInt(Ljava/lang/String;I)V

    sget-object v8, Lcom/konka/epg/TVRootApp;->mHandler:Landroid/os/Handler;

    new-instance v9, Lcom/konka/epg/service/EpgSrvProImp$2$1;

    invoke-direct {v9, p0, v0}, Lcom/konka/epg/service/EpgSrvProImp$2$1;-><init>(Lcom/konka/epg/service/EpgSrvProImp$2;Landroid/os/Bundle;)V

    invoke-virtual {v8, v9}, Landroid/os/Handler;->post(Ljava/lang/Runnable;)Z

    return-void

    :cond_1
    :try_start_1
    iget-object v8, p0, Lcom/konka/epg/service/EpgSrvProImp$2;->this$0:Lcom/konka/epg/service/EpgSrvProImp;

    # getter for: Lcom/konka/epg/service/EpgSrvProImp;->m_WeekEventAdapterList:Ljava/util/ArrayList;
    invoke-static {v8}, Lcom/konka/epg/service/EpgSrvProImp;->access$7(Lcom/konka/epg/service/EpgSrvProImp;)Ljava/util/ArrayList;

    move-result-object v8

    invoke-virtual {v8, v1}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v8

    check-cast v8, Lcom/konka/epg/adapter/EventListAdapter;

    invoke-virtual {v8}, Lcom/konka/epg/adapter/EventListAdapter;->delAllItem()V

    add-int/lit8 v1, v1, 0x1

    goto :goto_0

    :cond_2
    invoke-virtual {v3, v5}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lcom/konka/kkimplements/tv/vo/KonkaEventInfo;

    iget v8, v2, Lcom/konka/kkimplements/tv/vo/KonkaEventInfo;->_mWhichDay:I

    if-le v8, v11, :cond_3

    const/4 v8, 0x6

    iput v8, v2, Lcom/konka/kkimplements/tv/vo/KonkaEventInfo;->_mWhichDay:I

    :cond_3
    iget-object v8, p0, Lcom/konka/epg/service/EpgSrvProImp$2;->this$0:Lcom/konka/epg/service/EpgSrvProImp;

    # getter for: Lcom/konka/epg/service/EpgSrvProImp;->m_WeekEventAdapterList:Ljava/util/ArrayList;
    invoke-static {v8}, Lcom/konka/epg/service/EpgSrvProImp;->access$7(Lcom/konka/epg/service/EpgSrvProImp;)Ljava/util/ArrayList;

    move-result-object v8

    iget v10, v2, Lcom/konka/kkimplements/tv/vo/KonkaEventInfo;->_mWhichDay:I

    invoke-virtual {v8, v10}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v8

    check-cast v8, Lcom/konka/epg/adapter/EventListAdapter;

    invoke-virtual {v8, v2}, Lcom/konka/epg/adapter/EventListAdapter;->addItem(Lcom/konka/kkimplements/tv/vo/KonkaEventInfo;)V

    add-int/lit8 v5, v5, 0x1

    goto :goto_1

    :catchall_0
    move-exception v8

    monitor-exit v9
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    throw v8
.end method
