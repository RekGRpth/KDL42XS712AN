.class Lcom/konka/epg/service/EpgSrvProImp$1;
.super Ljava/lang/Object;
.source "EpgSrvProImp.java"

# interfaces
.implements Ljava/lang/Runnable;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/konka/epg/service/EpgSrvProImp;->initTimeInfoFromDtv()V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/konka/epg/service/EpgSrvProImp;


# direct methods
.method constructor <init>(Lcom/konka/epg/service/EpgSrvProImp;)V
    .locals 0

    iput-object p1, p0, Lcom/konka/epg/service/EpgSrvProImp$1;->this$0:Lcom/konka/epg/service/EpgSrvProImp;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public run()V
    .locals 4

    :goto_0
    iget-object v2, p0, Lcom/konka/epg/service/EpgSrvProImp$1;->this$0:Lcom/konka/epg/service/EpgSrvProImp;

    iget-object v3, p0, Lcom/konka/epg/service/EpgSrvProImp$1;->this$0:Lcom/konka/epg/service/EpgSrvProImp;

    # getter for: Lcom/konka/epg/service/EpgSrvProImp;->epgDesk:Lcom/konka/kkinterface/tv/EpgDesk;
    invoke-static {v3}, Lcom/konka/epg/service/EpgSrvProImp;->access$0(Lcom/konka/epg/service/EpgSrvProImp;)Lcom/konka/kkinterface/tv/EpgDesk;

    move-result-object v3

    invoke-interface {v3}, Lcom/konka/kkinterface/tv/EpgDesk;->getCurClkTime()Landroid/text/format/Time;

    move-result-object v3

    invoke-static {v2, v3}, Lcom/konka/epg/service/EpgSrvProImp;->access$1(Lcom/konka/epg/service/EpgSrvProImp;Landroid/text/format/Time;)V

    iget-object v2, p0, Lcom/konka/epg/service/EpgSrvProImp$1;->this$0:Lcom/konka/epg/service/EpgSrvProImp;

    # getter for: Lcom/konka/epg/service/EpgSrvProImp;->timeInfo:Landroid/text/format/Time;
    invoke-static {v2}, Lcom/konka/epg/service/EpgSrvProImp;->access$2(Lcom/konka/epg/service/EpgSrvProImp;)Landroid/text/format/Time;

    move-result-object v2

    if-nez v2, :cond_0

    iget-object v2, p0, Lcom/konka/epg/service/EpgSrvProImp$1;->this$0:Lcom/konka/epg/service/EpgSrvProImp;

    new-instance v3, Landroid/text/format/Time;

    invoke-direct {v3}, Landroid/text/format/Time;-><init>()V

    invoke-static {v2, v3}, Lcom/konka/epg/service/EpgSrvProImp;->access$1(Lcom/konka/epg/service/EpgSrvProImp;Landroid/text/format/Time;)V

    iget-object v2, p0, Lcom/konka/epg/service/EpgSrvProImp$1;->this$0:Lcom/konka/epg/service/EpgSrvProImp;

    # getter for: Lcom/konka/epg/service/EpgSrvProImp;->timeInfo:Landroid/text/format/Time;
    invoke-static {v2}, Lcom/konka/epg/service/EpgSrvProImp;->access$2(Lcom/konka/epg/service/EpgSrvProImp;)Landroid/text/format/Time;

    move-result-object v2

    invoke-virtual {v2}, Landroid/text/format/Time;->setToNow()V

    :cond_0
    iget-object v2, p0, Lcom/konka/epg/service/EpgSrvProImp$1;->this$0:Lcom/konka/epg/service/EpgSrvProImp;

    # getter for: Lcom/konka/epg/service/EpgSrvProImp;->epgHandler:Lcom/konka/epg/ui/EpgMainMenuActivity$EpgHandler;
    invoke-static {v2}, Lcom/konka/epg/service/EpgSrvProImp;->access$3(Lcom/konka/epg/service/EpgSrvProImp;)Lcom/konka/epg/ui/EpgMainMenuActivity$EpgHandler;

    move-result-object v2

    invoke-virtual {v2}, Lcom/konka/epg/ui/EpgMainMenuActivity$EpgHandler;->obtainMessage()Landroid/os/Message;

    move-result-object v1

    const/16 v2, 0x1000

    iput v2, v1, Landroid/os/Message;->what:I

    iget-object v2, p0, Lcom/konka/epg/service/EpgSrvProImp$1;->this$0:Lcom/konka/epg/service/EpgSrvProImp;

    # getter for: Lcom/konka/epg/service/EpgSrvProImp;->epgHandler:Lcom/konka/epg/ui/EpgMainMenuActivity$EpgHandler;
    invoke-static {v2}, Lcom/konka/epg/service/EpgSrvProImp;->access$3(Lcom/konka/epg/service/EpgSrvProImp;)Lcom/konka/epg/ui/EpgMainMenuActivity$EpgHandler;

    move-result-object v2

    invoke-virtual {v2, v1}, Lcom/konka/epg/ui/EpgMainMenuActivity$EpgHandler;->sendMessage(Landroid/os/Message;)Z

    const-wide/16 v2, 0x7530

    :try_start_0
    invoke-static {v2, v3}, Ljava/lang/Thread;->sleep(J)V
    :try_end_0
    .catch Ljava/lang/InterruptedException; {:try_start_0 .. :try_end_0} :catch_0

    goto :goto_0

    :catch_0
    move-exception v0

    invoke-virtual {v0}, Ljava/lang/InterruptedException;->printStackTrace()V

    goto :goto_0
.end method
