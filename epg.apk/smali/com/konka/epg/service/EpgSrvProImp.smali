.class public Lcom/konka/epg/service/EpgSrvProImp;
.super Ljava/lang/Object;
.source "EpgSrvProImp.java"

# interfaces
.implements Lcom/konka/epg/service/EpgServiceProvider;


# static fields
.field private static synthetic $SWITCH_TABLE$com$mstar$android$tvapi$common$vo$EnumServiceType:[I

.field private static epgSrvProImp:Lcom/konka/epg/service/EpgSrvProImp;


# instance fields
.field private activity:Landroid/app/Activity;

.field private channelDesk:Lcom/konka/kkinterface/tv/ChannelDesk;

.field private commonDesk:Lcom/konka/kkinterface/tv/CommonDesk;

.field private context:Landroid/content/Context;

.field private eCacheThread:Ljava/util/concurrent/ExecutorService;

.field private epgDesk:Lcom/konka/kkinterface/tv/EpgDesk;

.field private epgHandler:Lcom/konka/epg/ui/EpgMainMenuActivity$EpgHandler;

.field private epgServiceListener:Lcom/konka/epg/service/EpgServiceListener;

.field private m_DtvChannelAdapter:Lcom/konka/epg/adapter/ChannelListAdapter;

.field private m_RadioChannelAdapter:Lcom/konka/epg/adapter/ChannelListAdapter;

.field private m_WeekEventAdapterList:Ljava/util/ArrayList;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/ArrayList",
            "<",
            "Lcom/konka/epg/adapter/EventListAdapter;",
            ">;"
        }
    .end annotation
.end field

.field private mtvplayer:Lcom/mstar/android/tvapi/common/TvPlayer;

.field private sufaceView:Landroid/view/SurfaceView;

.field private surfaceParams:Landroid/view/WindowManager$LayoutParams;

.field private timeInfo:Landroid/text/format/Time;

.field private tvDeskProvider:Lcom/konka/kkinterface/tv/TvDeskProvider;


# direct methods
.method static synthetic $SWITCH_TABLE$com$mstar$android$tvapi$common$vo$EnumServiceType()[I
    .locals 3

    sget-object v0, Lcom/konka/epg/service/EpgSrvProImp;->$SWITCH_TABLE$com$mstar$android$tvapi$common$vo$EnumServiceType:[I

    if-eqz v0, :cond_0

    :goto_0
    return-object v0

    :cond_0
    invoke-static {}, Lcom/mstar/android/tvapi/common/vo/EnumServiceType;->values()[Lcom/mstar/android/tvapi/common/vo/EnumServiceType;

    move-result-object v0

    array-length v0, v0

    new-array v0, v0, [I

    :try_start_0
    sget-object v1, Lcom/mstar/android/tvapi/common/vo/EnumServiceType;->E_SERVICETYPE_ATV:Lcom/mstar/android/tvapi/common/vo/EnumServiceType;

    invoke-virtual {v1}, Lcom/mstar/android/tvapi/common/vo/EnumServiceType;->ordinal()I

    move-result v1

    const/4 v2, 0x1

    aput v2, v0, v1
    :try_end_0
    .catch Ljava/lang/NoSuchFieldError; {:try_start_0 .. :try_end_0} :catch_5

    :goto_1
    :try_start_1
    sget-object v1, Lcom/mstar/android/tvapi/common/vo/EnumServiceType;->E_SERVICETYPE_DATA:Lcom/mstar/android/tvapi/common/vo/EnumServiceType;

    invoke-virtual {v1}, Lcom/mstar/android/tvapi/common/vo/EnumServiceType;->ordinal()I

    move-result v1

    const/4 v2, 0x4

    aput v2, v0, v1
    :try_end_1
    .catch Ljava/lang/NoSuchFieldError; {:try_start_1 .. :try_end_1} :catch_4

    :goto_2
    :try_start_2
    sget-object v1, Lcom/mstar/android/tvapi/common/vo/EnumServiceType;->E_SERVICETYPE_DTV:Lcom/mstar/android/tvapi/common/vo/EnumServiceType;

    invoke-virtual {v1}, Lcom/mstar/android/tvapi/common/vo/EnumServiceType;->ordinal()I

    move-result v1

    const/4 v2, 0x2

    aput v2, v0, v1
    :try_end_2
    .catch Ljava/lang/NoSuchFieldError; {:try_start_2 .. :try_end_2} :catch_3

    :goto_3
    :try_start_3
    sget-object v1, Lcom/mstar/android/tvapi/common/vo/EnumServiceType;->E_SERVICETYPE_INVALID:Lcom/mstar/android/tvapi/common/vo/EnumServiceType;

    invoke-virtual {v1}, Lcom/mstar/android/tvapi/common/vo/EnumServiceType;->ordinal()I

    move-result v1

    const/4 v2, 0x6

    aput v2, v0, v1
    :try_end_3
    .catch Ljava/lang/NoSuchFieldError; {:try_start_3 .. :try_end_3} :catch_2

    :goto_4
    :try_start_4
    sget-object v1, Lcom/mstar/android/tvapi/common/vo/EnumServiceType;->E_SERVICETYPE_RADIO:Lcom/mstar/android/tvapi/common/vo/EnumServiceType;

    invoke-virtual {v1}, Lcom/mstar/android/tvapi/common/vo/EnumServiceType;->ordinal()I

    move-result v1

    const/4 v2, 0x3

    aput v2, v0, v1
    :try_end_4
    .catch Ljava/lang/NoSuchFieldError; {:try_start_4 .. :try_end_4} :catch_1

    :goto_5
    :try_start_5
    sget-object v1, Lcom/mstar/android/tvapi/common/vo/EnumServiceType;->E_SERVICETYPE_UNITED_TV:Lcom/mstar/android/tvapi/common/vo/EnumServiceType;

    invoke-virtual {v1}, Lcom/mstar/android/tvapi/common/vo/EnumServiceType;->ordinal()I

    move-result v1

    const/4 v2, 0x5

    aput v2, v0, v1
    :try_end_5
    .catch Ljava/lang/NoSuchFieldError; {:try_start_5 .. :try_end_5} :catch_0

    :goto_6
    sput-object v0, Lcom/konka/epg/service/EpgSrvProImp;->$SWITCH_TABLE$com$mstar$android$tvapi$common$vo$EnumServiceType:[I

    goto :goto_0

    :catch_0
    move-exception v1

    goto :goto_6

    :catch_1
    move-exception v1

    goto :goto_5

    :catch_2
    move-exception v1

    goto :goto_4

    :catch_3
    move-exception v1

    goto :goto_3

    :catch_4
    move-exception v1

    goto :goto_2

    :catch_5
    move-exception v1

    goto :goto_1
.end method

.method static constructor <clinit>()V
    .locals 1

    const/4 v0, 0x0

    sput-object v0, Lcom/konka/epg/service/EpgSrvProImp;->epgSrvProImp:Lcom/konka/epg/service/EpgSrvProImp;

    return-void
.end method

.method public constructor <init>(Landroid/app/Activity;)V
    .locals 2
    .param p1    # Landroid/app/Activity;

    const/4 v1, 0x0

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    iput-object v1, p0, Lcom/konka/epg/service/EpgSrvProImp;->activity:Landroid/app/Activity;

    iput-object v1, p0, Lcom/konka/epg/service/EpgSrvProImp;->context:Landroid/content/Context;

    iput-object v1, p0, Lcom/konka/epg/service/EpgSrvProImp;->tvDeskProvider:Lcom/konka/kkinterface/tv/TvDeskProvider;

    iput-object v1, p0, Lcom/konka/epg/service/EpgSrvProImp;->epgDesk:Lcom/konka/kkinterface/tv/EpgDesk;

    iput-object v1, p0, Lcom/konka/epg/service/EpgSrvProImp;->channelDesk:Lcom/konka/kkinterface/tv/ChannelDesk;

    iput-object v1, p0, Lcom/konka/epg/service/EpgSrvProImp;->commonDesk:Lcom/konka/kkinterface/tv/CommonDesk;

    iput-object v1, p0, Lcom/konka/epg/service/EpgSrvProImp;->m_DtvChannelAdapter:Lcom/konka/epg/adapter/ChannelListAdapter;

    iput-object v1, p0, Lcom/konka/epg/service/EpgSrvProImp;->m_RadioChannelAdapter:Lcom/konka/epg/adapter/ChannelListAdapter;

    iput-object v1, p0, Lcom/konka/epg/service/EpgSrvProImp;->m_WeekEventAdapterList:Ljava/util/ArrayList;

    iput-object v1, p0, Lcom/konka/epg/service/EpgSrvProImp;->timeInfo:Landroid/text/format/Time;

    iput-object v1, p0, Lcom/konka/epg/service/EpgSrvProImp;->epgHandler:Lcom/konka/epg/ui/EpgMainMenuActivity$EpgHandler;

    iput-object v1, p0, Lcom/konka/epg/service/EpgSrvProImp;->eCacheThread:Ljava/util/concurrent/ExecutorService;

    iput-object v1, p0, Lcom/konka/epg/service/EpgSrvProImp;->sufaceView:Landroid/view/SurfaceView;

    iput-object v1, p0, Lcom/konka/epg/service/EpgSrvProImp;->surfaceParams:Landroid/view/WindowManager$LayoutParams;

    invoke-static {}, Lcom/mstar/android/tvapi/common/TvManager;->getInstance()Lcom/mstar/android/tvapi/common/TvManager;

    move-result-object v0

    invoke-virtual {v0}, Lcom/mstar/android/tvapi/common/TvManager;->getPlayerManager()Lcom/mstar/android/tvapi/common/TvPlayer;

    move-result-object v0

    iput-object v0, p0, Lcom/konka/epg/service/EpgSrvProImp;->mtvplayer:Lcom/mstar/android/tvapi/common/TvPlayer;

    iput-object v1, p0, Lcom/konka/epg/service/EpgSrvProImp;->epgServiceListener:Lcom/konka/epg/service/EpgServiceListener;

    invoke-virtual {p1}, Landroid/app/Activity;->getApplicationContext()Landroid/content/Context;

    move-result-object v0

    iput-object v0, p0, Lcom/konka/epg/service/EpgSrvProImp;->context:Landroid/content/Context;

    new-instance v0, Landroid/text/format/Time;

    invoke-direct {v0}, Landroid/text/format/Time;-><init>()V

    iput-object v0, p0, Lcom/konka/epg/service/EpgSrvProImp;->timeInfo:Landroid/text/format/Time;

    invoke-static {}, Ljava/util/concurrent/Executors;->newCachedThreadPool()Ljava/util/concurrent/ExecutorService;

    move-result-object v0

    iput-object v0, p0, Lcom/konka/epg/service/EpgSrvProImp;->eCacheThread:Ljava/util/concurrent/ExecutorService;

    invoke-virtual {p0, p1}, Lcom/konka/epg/service/EpgSrvProImp;->setActivity(Landroid/app/Activity;)V

    return-void
.end method

.method static synthetic access$0(Lcom/konka/epg/service/EpgSrvProImp;)Lcom/konka/kkinterface/tv/EpgDesk;
    .locals 1

    iget-object v0, p0, Lcom/konka/epg/service/EpgSrvProImp;->epgDesk:Lcom/konka/kkinterface/tv/EpgDesk;

    return-object v0
.end method

.method static synthetic access$1(Lcom/konka/epg/service/EpgSrvProImp;Landroid/text/format/Time;)V
    .locals 0

    iput-object p1, p0, Lcom/konka/epg/service/EpgSrvProImp;->timeInfo:Landroid/text/format/Time;

    return-void
.end method

.method static synthetic access$2(Lcom/konka/epg/service/EpgSrvProImp;)Landroid/text/format/Time;
    .locals 1

    iget-object v0, p0, Lcom/konka/epg/service/EpgSrvProImp;->timeInfo:Landroid/text/format/Time;

    return-object v0
.end method

.method static synthetic access$3(Lcom/konka/epg/service/EpgSrvProImp;)Lcom/konka/epg/ui/EpgMainMenuActivity$EpgHandler;
    .locals 1

    iget-object v0, p0, Lcom/konka/epg/service/EpgSrvProImp;->epgHandler:Lcom/konka/epg/ui/EpgMainMenuActivity$EpgHandler;

    return-object v0
.end method

.method static synthetic access$5(Lcom/konka/epg/service/EpgSrvProImp;Lcom/mstar/android/tvapi/common/vo/EnumServiceType;I)Landroid/content/ContentValues;
    .locals 1

    invoke-direct {p0, p1, p2}, Lcom/konka/epg/service/EpgSrvProImp;->getCurProgNum(Lcom/mstar/android/tvapi/common/vo/EnumServiceType;I)Landroid/content/ContentValues;

    move-result-object v0

    return-object v0
.end method

.method static synthetic access$6(Lcom/konka/epg/service/EpgSrvProImp;Lcom/mstar/android/tvapi/common/vo/EnumServiceType;I)V
    .locals 0

    invoke-direct {p0, p1, p2}, Lcom/konka/epg/service/EpgSrvProImp;->setCurProg(Lcom/mstar/android/tvapi/common/vo/EnumServiceType;I)V

    return-void
.end method

.method static synthetic access$7(Lcom/konka/epg/service/EpgSrvProImp;)Ljava/util/ArrayList;
    .locals 1

    iget-object v0, p0, Lcom/konka/epg/service/EpgSrvProImp;->m_WeekEventAdapterList:Ljava/util/ArrayList;

    return-object v0
.end method

.method static synthetic access$8(Lcom/konka/epg/service/EpgSrvProImp;)Lcom/konka/epg/service/EpgServiceListener;
    .locals 1

    iget-object v0, p0, Lcom/konka/epg/service/EpgSrvProImp;->epgServiceListener:Lcom/konka/epg/service/EpgServiceListener;

    return-object v0
.end method

.method static synthetic access$9(Lcom/konka/epg/service/EpgSrvProImp;)Lcom/mstar/android/tvapi/common/TvPlayer;
    .locals 1

    iget-object v0, p0, Lcom/konka/epg/service/EpgSrvProImp;->mtvplayer:Lcom/mstar/android/tvapi/common/TvPlayer;

    return-object v0
.end method

.method private getChannelListAdapter()V
    .locals 4

    iget-object v2, p0, Lcom/konka/epg/service/EpgSrvProImp;->m_DtvChannelAdapter:Lcom/konka/epg/adapter/ChannelListAdapter;

    invoke-virtual {v2}, Lcom/konka/epg/adapter/ChannelListAdapter;->clearItme()V

    iget-object v2, p0, Lcom/konka/epg/service/EpgSrvProImp;->epgDesk:Lcom/konka/kkinterface/tv/EpgDesk;

    sget-object v3, Lcom/mstar/android/tvapi/common/vo/EnumServiceType;->E_SERVICETYPE_DTV:Lcom/mstar/android/tvapi/common/vo/EnumServiceType;

    invoke-interface {v2, v3}, Lcom/konka/kkinterface/tv/EpgDesk;->getProgListByServiceType(Lcom/mstar/android/tvapi/common/vo/EnumServiceType;)Ljava/util/ArrayList;

    move-result-object v1

    if-eqz v1, :cond_0

    invoke-virtual {v1}, Ljava/util/ArrayList;->size()I

    move-result v2

    if-nez v2, :cond_4

    :cond_0
    const-string v2, "Epg Count is 0"

    invoke-static {v2}, Lcom/konka/epg/util/EpgDebugPrint;->Debug(Ljava/lang/String;)V

    :cond_1
    iget-object v2, p0, Lcom/konka/epg/service/EpgSrvProImp;->m_RadioChannelAdapter:Lcom/konka/epg/adapter/ChannelListAdapter;

    invoke-virtual {v2}, Lcom/konka/epg/adapter/ChannelListAdapter;->clearItme()V

    iget-object v2, p0, Lcom/konka/epg/service/EpgSrvProImp;->epgDesk:Lcom/konka/kkinterface/tv/EpgDesk;

    sget-object v3, Lcom/mstar/android/tvapi/common/vo/EnumServiceType;->E_SERVICETYPE_RADIO:Lcom/mstar/android/tvapi/common/vo/EnumServiceType;

    invoke-interface {v2, v3}, Lcom/konka/kkinterface/tv/EpgDesk;->getProgListByServiceType(Lcom/mstar/android/tvapi/common/vo/EnumServiceType;)Ljava/util/ArrayList;

    move-result-object v1

    if-eqz v1, :cond_2

    invoke-virtual {v1}, Ljava/util/ArrayList;->size()I

    move-result v2

    if-nez v2, :cond_5

    :cond_2
    const-string v2, "Radio Count is 0"

    invoke-static {v2}, Lcom/konka/epg/util/EpgDebugPrint;->Debug(Ljava/lang/String;)V

    :cond_3
    return-void

    :cond_4
    new-instance v2, Ljava/lang/StringBuilder;

    const-string v3, "TV List Count----->"

    invoke-direct {v2, v3}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v1}, Ljava/util/ArrayList;->size()I

    move-result v3

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v2}, Lcom/konka/epg/util/EpgDebugPrint;->Debug(Ljava/lang/String;)V

    const/4 v0, 0x0

    :goto_0
    invoke-virtual {v1}, Ljava/util/ArrayList;->size()I

    move-result v2

    if-ge v0, v2, :cond_1

    iget-object v3, p0, Lcom/konka/epg/service/EpgSrvProImp;->m_DtvChannelAdapter:Lcom/konka/epg/adapter/ChannelListAdapter;

    invoke-virtual {v1, v0}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lcom/konka/kkimplements/tv/vo/KonkaProgInfo;

    invoke-virtual {v3, v2}, Lcom/konka/epg/adapter/ChannelListAdapter;->addItem(Lcom/konka/kkimplements/tv/vo/KonkaProgInfo;)V

    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    :cond_5
    const/4 v0, 0x0

    :goto_1
    invoke-virtual {v1}, Ljava/util/ArrayList;->size()I

    move-result v2

    if-ge v0, v2, :cond_3

    iget-object v3, p0, Lcom/konka/epg/service/EpgSrvProImp;->m_RadioChannelAdapter:Lcom/konka/epg/adapter/ChannelListAdapter;

    invoke-virtual {v1, v0}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lcom/konka/kkimplements/tv/vo/KonkaProgInfo;

    invoke-virtual {v3, v2}, Lcom/konka/epg/adapter/ChannelListAdapter;->addItem(Lcom/konka/kkimplements/tv/vo/KonkaProgInfo;)V

    add-int/lit8 v0, v0, 0x1

    goto :goto_1
.end method

.method private declared-synchronized getCurProgNum(Lcom/mstar/android/tvapi/common/vo/EnumServiceType;I)Landroid/content/ContentValues;
    .locals 5
    .param p1    # Lcom/mstar/android/tvapi/common/vo/EnumServiceType;
    .param p2    # I

    monitor-enter p0

    :try_start_0
    new-instance v2, Landroid/content/ContentValues;

    invoke-direct {v2}, Landroid/content/ContentValues;-><init>()V

    const/4 v1, 0x0

    const/4 v0, 0x0

    invoke-static {}, Lcom/konka/epg/service/EpgSrvProImp;->$SWITCH_TABLE$com$mstar$android$tvapi$common$vo$EnumServiceType()[I

    move-result-object v3

    invoke-virtual {p1}, Lcom/mstar/android/tvapi/common/vo/EnumServiceType;->ordinal()I

    move-result v4

    aget v3, v3, v4

    packed-switch v3, :pswitch_data_0

    :goto_0
    const-string v3, "sServiceType"

    invoke-static {v1}, Ljava/lang/Short;->valueOf(S)Ljava/lang/Short;

    move-result-object v4

    invoke-virtual {v2, v3, v4}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Short;)V

    const-string v3, "iProgNo"

    invoke-static {v0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v4

    invoke-virtual {v2, v3, v4}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Integer;)V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    monitor-exit p0

    return-object v2

    :pswitch_0
    :try_start_1
    sget-object v3, Lcom/mstar/android/tvapi/common/vo/EnumServiceType;->E_SERVICETYPE_DTV:Lcom/mstar/android/tvapi/common/vo/EnumServiceType;

    invoke-virtual {v3}, Lcom/mstar/android/tvapi/common/vo/EnumServiceType;->ordinal()I

    move-result v3

    int-to-short v1, v3

    iget-object v3, p0, Lcom/konka/epg/service/EpgSrvProImp;->m_DtvChannelAdapter:Lcom/konka/epg/adapter/ChannelListAdapter;

    invoke-virtual {v3}, Lcom/konka/epg/adapter/ChannelListAdapter;->getCount()I

    move-result v3

    if-eqz v3, :cond_0

    iget-object v3, p0, Lcom/konka/epg/service/EpgSrvProImp;->m_DtvChannelAdapter:Lcom/konka/epg/adapter/ChannelListAdapter;

    invoke-virtual {v3, p2}, Lcom/konka/epg/adapter/ChannelListAdapter;->getItem(I)Lcom/konka/kkimplements/tv/vo/KonkaProgInfo;

    move-result-object v3

    iget v0, v3, Lcom/konka/kkimplements/tv/vo/KonkaProgInfo;->_mProgNo:I

    new-instance v3, Ljava/lang/StringBuilder;

    const-string v4, "getEventOfWeekiProgNo is -------->"

    invoke-direct {v3, v4}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v3, v0}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v3

    const-string v4, " iIndex is ---->"

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3, p2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v3}, Lcom/konka/epg/util/EpgDebugPrint;->Debug(Ljava/lang/String;)V
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    goto :goto_0

    :catchall_0
    move-exception v3

    monitor-exit p0

    throw v3

    :cond_0
    :try_start_2
    const-string v3, "ProgList is ---------> 0"

    invoke-static {v3}, Lcom/konka/epg/util/EpgDebugPrint;->Debug(Ljava/lang/String;)V

    goto :goto_0

    :pswitch_1
    sget-object v3, Lcom/mstar/android/tvapi/common/vo/EnumServiceType;->E_SERVICETYPE_RADIO:Lcom/mstar/android/tvapi/common/vo/EnumServiceType;

    invoke-virtual {v3}, Lcom/mstar/android/tvapi/common/vo/EnumServiceType;->ordinal()I

    move-result v3

    int-to-short v1, v3

    iget-object v3, p0, Lcom/konka/epg/service/EpgSrvProImp;->m_RadioChannelAdapter:Lcom/konka/epg/adapter/ChannelListAdapter;

    invoke-virtual {v3}, Lcom/konka/epg/adapter/ChannelListAdapter;->getCount()I

    move-result v3

    if-eqz v3, :cond_1

    iget-object v3, p0, Lcom/konka/epg/service/EpgSrvProImp;->m_RadioChannelAdapter:Lcom/konka/epg/adapter/ChannelListAdapter;

    invoke-virtual {v3, p2}, Lcom/konka/epg/adapter/ChannelListAdapter;->getItem(I)Lcom/konka/kkimplements/tv/vo/KonkaProgInfo;

    move-result-object v3

    iget v0, v3, Lcom/konka/kkimplements/tv/vo/KonkaProgInfo;->_mProgNo:I

    new-instance v3, Ljava/lang/StringBuilder;

    const-string v4, "getEventOfWeekiProgNo is -------->"

    invoke-direct {v3, v4}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v3, v0}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v3

    const-string v4, " iIndex is ---->"

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3, p2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v3}, Lcom/konka/epg/util/EpgDebugPrint;->Debug(Ljava/lang/String;)V

    goto/16 :goto_0

    :cond_1
    const-string v3, "ProgList is ---------> 0"

    invoke-static {v3}, Lcom/konka/epg/util/EpgDebugPrint;->Debug(Ljava/lang/String;)V
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    goto/16 :goto_0

    :pswitch_data_0
    .packed-switch 0x2
        :pswitch_0
        :pswitch_1
    .end packed-switch
.end method

.method public static getEpgMgrInstance(Landroid/app/Activity;)Lcom/konka/epg/service/EpgSrvProImp;
    .locals 1
    .param p0    # Landroid/app/Activity;

    sget-object v0, Lcom/konka/epg/service/EpgSrvProImp;->epgSrvProImp:Lcom/konka/epg/service/EpgSrvProImp;

    if-nez v0, :cond_0

    new-instance v0, Lcom/konka/epg/service/EpgSrvProImp;

    invoke-direct {v0, p0}, Lcom/konka/epg/service/EpgSrvProImp;-><init>(Landroid/app/Activity;)V

    sput-object v0, Lcom/konka/epg/service/EpgSrvProImp;->epgSrvProImp:Lcom/konka/epg/service/EpgSrvProImp;

    :cond_0
    sget-object v0, Lcom/konka/epg/service/EpgSrvProImp;->epgSrvProImp:Lcom/konka/epg/service/EpgSrvProImp;

    return-object v0
.end method

.method private initdesk()V
    .locals 5

    iget-object v2, p0, Lcom/konka/epg/service/EpgSrvProImp;->activity:Landroid/app/Activity;

    invoke-virtual {v2}, Landroid/app/Activity;->getApplication()Landroid/app/Application;

    move-result-object v0

    check-cast v0, Lcom/konka/epg/TVRootApp;

    invoke-virtual {v0}, Lcom/konka/epg/TVRootApp;->getTvDeskProvider()Lcom/konka/kkinterface/tv/TvDeskProvider;

    move-result-object v2

    iput-object v2, p0, Lcom/konka/epg/service/EpgSrvProImp;->tvDeskProvider:Lcom/konka/kkinterface/tv/TvDeskProvider;

    iget-object v2, p0, Lcom/konka/epg/service/EpgSrvProImp;->tvDeskProvider:Lcom/konka/kkinterface/tv/TvDeskProvider;

    invoke-interface {v2}, Lcom/konka/kkinterface/tv/TvDeskProvider;->getEpgManagerInstance()Lcom/konka/kkinterface/tv/EpgDesk;

    move-result-object v2

    iput-object v2, p0, Lcom/konka/epg/service/EpgSrvProImp;->epgDesk:Lcom/konka/kkinterface/tv/EpgDesk;

    iget-object v2, p0, Lcom/konka/epg/service/EpgSrvProImp;->tvDeskProvider:Lcom/konka/kkinterface/tv/TvDeskProvider;

    invoke-interface {v2}, Lcom/konka/kkinterface/tv/TvDeskProvider;->getChannelManagerInstance()Lcom/konka/kkinterface/tv/ChannelDesk;

    move-result-object v2

    iput-object v2, p0, Lcom/konka/epg/service/EpgSrvProImp;->channelDesk:Lcom/konka/kkinterface/tv/ChannelDesk;

    iget-object v2, p0, Lcom/konka/epg/service/EpgSrvProImp;->tvDeskProvider:Lcom/konka/kkinterface/tv/TvDeskProvider;

    invoke-interface {v2}, Lcom/konka/kkinterface/tv/TvDeskProvider;->getCommonManagerInstance()Lcom/konka/kkinterface/tv/CommonDesk;

    move-result-object v2

    iput-object v2, p0, Lcom/konka/epg/service/EpgSrvProImp;->commonDesk:Lcom/konka/kkinterface/tv/CommonDesk;

    iget-object v2, p0, Lcom/konka/epg/service/EpgSrvProImp;->tvDeskProvider:Lcom/konka/kkinterface/tv/TvDeskProvider;

    invoke-interface {v2}, Lcom/konka/kkinterface/tv/TvDeskProvider;->getCommonManagerInstance()Lcom/konka/kkinterface/tv/CommonDesk;

    move-result-object v2

    iget-object v3, p0, Lcom/konka/epg/service/EpgSrvProImp;->epgHandler:Lcom/konka/epg/ui/EpgMainMenuActivity$EpgHandler;

    const/4 v4, 0x0

    invoke-interface {v2, v3, v4}, Lcom/konka/kkinterface/tv/CommonDesk;->setHandler(Landroid/os/Handler;I)Z

    iget-object v2, p0, Lcom/konka/epg/service/EpgSrvProImp;->epgDesk:Lcom/konka/kkinterface/tv/EpgDesk;

    invoke-interface {v2}, Lcom/konka/kkinterface/tv/EpgDesk;->getDtvProgCount()Lcom/konka/kkimplements/tv/vo/KonkaProgCount;

    move-result-object v1

    new-instance v2, Ljava/lang/StringBuilder;

    const-string v3, "DTV Count---->"

    invoke-direct {v2, v3}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    iget v3, v1, Lcom/konka/kkimplements/tv/vo/KonkaProgCount;->DTVAllProgCount:I

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, "DTV Radio Count------>"

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    iget v3, v1, Lcom/konka/kkimplements/tv/vo/KonkaProgCount;->DTVRadioProgCount:I

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, "DTV TV Count"

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    iget v3, v1, Lcom/konka/kkimplements/tv/vo/KonkaProgCount;->DTVTvProgCount:I

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v2}, Lcom/konka/epg/util/EpgDebugPrint;->Debug(Ljava/lang/String;)V

    return-void
.end method

.method private initlist()V
    .locals 4

    new-instance v1, Lcom/konka/epg/adapter/ChannelListAdapter;

    iget-object v2, p0, Lcom/konka/epg/service/EpgSrvProImp;->context:Landroid/content/Context;

    invoke-direct {v1, v2}, Lcom/konka/epg/adapter/ChannelListAdapter;-><init>(Landroid/content/Context;)V

    iput-object v1, p0, Lcom/konka/epg/service/EpgSrvProImp;->m_DtvChannelAdapter:Lcom/konka/epg/adapter/ChannelListAdapter;

    new-instance v1, Lcom/konka/epg/adapter/ChannelListAdapter;

    iget-object v2, p0, Lcom/konka/epg/service/EpgSrvProImp;->context:Landroid/content/Context;

    invoke-direct {v1, v2}, Lcom/konka/epg/adapter/ChannelListAdapter;-><init>(Landroid/content/Context;)V

    iput-object v1, p0, Lcom/konka/epg/service/EpgSrvProImp;->m_RadioChannelAdapter:Lcom/konka/epg/adapter/ChannelListAdapter;

    invoke-direct {p0}, Lcom/konka/epg/service/EpgSrvProImp;->getChannelListAdapter()V

    new-instance v1, Ljava/util/ArrayList;

    invoke-direct {v1}, Ljava/util/ArrayList;-><init>()V

    iput-object v1, p0, Lcom/konka/epg/service/EpgSrvProImp;->m_WeekEventAdapterList:Ljava/util/ArrayList;

    const/4 v0, 0x0

    :goto_0
    const/4 v1, 0x7

    if-lt v0, v1, :cond_0

    return-void

    :cond_0
    iget-object v1, p0, Lcom/konka/epg/service/EpgSrvProImp;->m_WeekEventAdapterList:Ljava/util/ArrayList;

    new-instance v2, Lcom/konka/epg/adapter/EventListAdapter;

    iget-object v3, p0, Lcom/konka/epg/service/EpgSrvProImp;->context:Landroid/content/Context;

    invoke-direct {v2, v3}, Lcom/konka/epg/adapter/EventListAdapter;-><init>(Landroid/content/Context;)V

    invoke-virtual {v1, v2}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    add-int/lit8 v0, v0, 0x1

    goto :goto_0
.end method

.method private declared-synchronized setCurProg(Lcom/mstar/android/tvapi/common/vo/EnumServiceType;I)V
    .locals 2
    .param p1    # Lcom/mstar/android/tvapi/common/vo/EnumServiceType;
    .param p2    # I

    monitor-enter p0

    :try_start_0
    invoke-static {}, Lcom/konka/epg/service/EpgSrvProImp;->$SWITCH_TABLE$com$mstar$android$tvapi$common$vo$EnumServiceType()[I

    move-result-object v0

    invoke-virtual {p1}, Lcom/mstar/android/tvapi/common/vo/EnumServiceType;->ordinal()I

    move-result v1

    aget v0, v0, v1

    packed-switch v0, :pswitch_data_0

    :goto_0
    const-string v0, "\u8bbe\u5b9a\u8282\u76ee"

    invoke-static {v0}, Lcom/konka/epg/util/EpgDebugPrint;->Debug(Ljava/lang/String;)V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    monitor-exit p0

    return-void

    :pswitch_0
    :try_start_1
    iget-object v0, p0, Lcom/konka/epg/service/EpgSrvProImp;->epgDesk:Lcom/konka/kkinterface/tv/EpgDesk;

    iget-object v1, p0, Lcom/konka/epg/service/EpgSrvProImp;->m_DtvChannelAdapter:Lcom/konka/epg/adapter/ChannelListAdapter;

    invoke-virtual {v1, p2}, Lcom/konka/epg/adapter/ChannelListAdapter;->getItem(I)Lcom/konka/kkimplements/tv/vo/KonkaProgInfo;

    move-result-object v1

    invoke-interface {v0, v1}, Lcom/konka/kkinterface/tv/EpgDesk;->ProgramSel(Lcom/konka/kkimplements/tv/vo/KonkaProgInfo;)V
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    goto :goto_0

    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0

    :pswitch_1
    :try_start_2
    iget-object v0, p0, Lcom/konka/epg/service/EpgSrvProImp;->epgDesk:Lcom/konka/kkinterface/tv/EpgDesk;

    iget-object v1, p0, Lcom/konka/epg/service/EpgSrvProImp;->m_RadioChannelAdapter:Lcom/konka/epg/adapter/ChannelListAdapter;

    invoke-virtual {v1, p2}, Lcom/konka/epg/adapter/ChannelListAdapter;->getItem(I)Lcom/konka/kkimplements/tv/vo/KonkaProgInfo;

    move-result-object v1

    invoke-interface {v0, v1}, Lcom/konka/kkinterface/tv/EpgDesk;->ProgramSel(Lcom/konka/kkimplements/tv/vo/KonkaProgInfo;)V
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    goto :goto_0

    :pswitch_data_0
    .packed-switch 0x2
        :pswitch_0
        :pswitch_1
    .end packed-switch
.end method


# virtual methods
.method public bookEvent(Lcom/konka/kkimplements/tv/vo/KonkaEventInfo;)Lcom/konka/kkinterface/tv/DtvInterface$RESULT_ADDEVENT;
    .locals 1
    .param p1    # Lcom/konka/kkimplements/tv/vo/KonkaEventInfo;

    iget-object v0, p0, Lcom/konka/epg/service/EpgSrvProImp;->epgDesk:Lcom/konka/kkinterface/tv/EpgDesk;

    invoke-interface {v0, p1}, Lcom/konka/kkinterface/tv/EpgDesk;->addEpgTimerEvent(Lcom/konka/kkimplements/tv/vo/KonkaEventInfo;)Lcom/konka/kkinterface/tv/DtvInterface$RESULT_ADDEVENT;

    move-result-object v0

    return-object v0
.end method

.method public closeDB()V
    .locals 1

    iget-object v0, p0, Lcom/konka/epg/service/EpgSrvProImp;->epgDesk:Lcom/konka/kkinterface/tv/EpgDesk;

    invoke-interface {v0}, Lcom/konka/kkinterface/tv/EpgDesk;->closeDB()V

    return-void
.end method

.method public closeTVWidgetSurface()V
    .locals 2

    invoke-static {}, Lcom/konka/epg/TVRootApp;->getPictureSkin()Lcom/mstar/android/tv/TvPictureManager;

    move-result-object v1

    invoke-virtual {v1}, Lcom/mstar/android/tv/TvPictureManager;->getVideoArc()Lcom/mstar/android/tvapi/common/vo/EnumVideoArcType;

    move-result-object v0

    invoke-static {}, Lcom/konka/epg/TVRootApp;->getPictureSkin()Lcom/mstar/android/tv/TvPictureManager;

    move-result-object v1

    invoke-virtual {v1, v0}, Lcom/mstar/android/tv/TvPictureManager;->setVideoArc(Lcom/mstar/android/tvapi/common/vo/EnumVideoArcType;)Z

    return-void
.end method

.method public delBookedEvent(IZ)Z
    .locals 1
    .param p1    # I
    .param p2    # Z

    iget-object v0, p0, Lcom/konka/epg/service/EpgSrvProImp;->epgDesk:Lcom/konka/kkinterface/tv/EpgDesk;

    invoke-interface {v0, p1, p2}, Lcom/konka/kkinterface/tv/EpgDesk;->delEpgTimerEvent(IZ)Z

    move-result v0

    return v0
.end method

.method public delayProgSel(I)V
    .locals 2
    .param p1    # I

    iget-object v0, p0, Lcom/konka/epg/service/EpgSrvProImp;->eCacheThread:Ljava/util/concurrent/ExecutorService;

    new-instance v1, Lcom/konka/epg/service/EpgSrvProImp$5;

    invoke-direct {v1, p0, p1}, Lcom/konka/epg/service/EpgSrvProImp$5;-><init>(Lcom/konka/epg/service/EpgSrvProImp;I)V

    invoke-interface {v0, v1}, Ljava/util/concurrent/ExecutorService;->execute(Ljava/lang/Runnable;)V

    return-void
.end method

.method public delayServiceTypeSel(Lcom/mstar/android/tvapi/common/vo/EnumServiceType;I)V
    .locals 2
    .param p1    # Lcom/mstar/android/tvapi/common/vo/EnumServiceType;
    .param p2    # I

    iget-object v0, p0, Lcom/konka/epg/service/EpgSrvProImp;->eCacheThread:Ljava/util/concurrent/ExecutorService;

    new-instance v1, Lcom/konka/epg/service/EpgSrvProImp$8;

    invoke-direct {v1, p0, p2, p1}, Lcom/konka/epg/service/EpgSrvProImp$8;-><init>(Lcom/konka/epg/service/EpgSrvProImp;ILcom/mstar/android/tvapi/common/vo/EnumServiceType;)V

    invoke-interface {v0, v1}, Ljava/util/concurrent/ExecutorService;->execute(Ljava/lang/Runnable;)V

    return-void
.end method

.method public getCurServiceType()I
    .locals 5

    const/4 v0, 0x2

    iget-object v2, p0, Lcom/konka/epg/service/EpgSrvProImp;->epgDesk:Lcom/konka/kkinterface/tv/EpgDesk;

    invoke-interface {v2}, Lcom/konka/kkinterface/tv/EpgDesk;->getCurKonkaProgInfo()Lcom/konka/kkimplements/tv/vo/KonkaProgInfo;

    move-result-object v1

    invoke-static {}, Lcom/konka/epg/service/EpgSrvProImp;->$SWITCH_TABLE$com$mstar$android$tvapi$common$vo$EnumServiceType()[I

    move-result-object v2

    invoke-static {}, Lcom/mstar/android/tvapi/common/vo/EnumServiceType;->values()[Lcom/mstar/android/tvapi/common/vo/EnumServiceType;

    move-result-object v3

    iget-short v4, v1, Lcom/konka/kkimplements/tv/vo/KonkaProgInfo;->_mProgType:S

    aget-object v3, v3, v4

    invoke-virtual {v3}, Lcom/mstar/android/tvapi/common/vo/EnumServiceType;->ordinal()I

    move-result v3

    aget v2, v2, v3

    packed-switch v2, :pswitch_data_0

    const/4 v0, 0x0

    :goto_0
    return v0

    :pswitch_0
    const/4 v0, 0x0

    goto :goto_0

    :pswitch_1
    const/4 v0, 0x1

    goto :goto_0

    :pswitch_data_0
    .packed-switch 0x2
        :pswitch_0
        :pswitch_1
    .end packed-switch
.end method

.method public getDtvChannlAdapter()Lcom/konka/epg/adapter/ChannelListAdapter;
    .locals 1

    iget-object v0, p0, Lcom/konka/epg/service/EpgSrvProImp;->m_DtvChannelAdapter:Lcom/konka/epg/adapter/ChannelListAdapter;

    return-object v0
.end method

.method public getEventDescribe(I)Ljava/lang/String;
    .locals 1
    .param p1    # I

    iget-object v0, p0, Lcom/konka/epg/service/EpgSrvProImp;->epgDesk:Lcom/konka/kkinterface/tv/EpgDesk;

    invoke-interface {v0, p1}, Lcom/konka/kkinterface/tv/EpgDesk;->getEventDetailDescriptor(I)Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public getEventOfDay(Lcom/mstar/android/tvapi/common/vo/EnumServiceType;I)V
    .locals 2
    .param p1    # Lcom/mstar/android/tvapi/common/vo/EnumServiceType;
    .param p2    # I

    iget-object v0, p0, Lcom/konka/epg/service/EpgSrvProImp;->eCacheThread:Ljava/util/concurrent/ExecutorService;

    new-instance v1, Lcom/konka/epg/service/EpgSrvProImp$4;

    invoke-direct {v1, p0, p1, p2}, Lcom/konka/epg/service/EpgSrvProImp$4;-><init>(Lcom/konka/epg/service/EpgSrvProImp;Lcom/mstar/android/tvapi/common/vo/EnumServiceType;I)V

    invoke-interface {v0, v1}, Ljava/util/concurrent/ExecutorService;->execute(Ljava/lang/Runnable;)V

    return-void
.end method

.method public getEventOfFirstDay(Lcom/mstar/android/tvapi/common/vo/EnumServiceType;I)V
    .locals 2
    .param p1    # Lcom/mstar/android/tvapi/common/vo/EnumServiceType;
    .param p2    # I

    iget-object v0, p0, Lcom/konka/epg/service/EpgSrvProImp;->eCacheThread:Ljava/util/concurrent/ExecutorService;

    new-instance v1, Lcom/konka/epg/service/EpgSrvProImp$3;

    invoke-direct {v1, p0, p1, p2}, Lcom/konka/epg/service/EpgSrvProImp$3;-><init>(Lcom/konka/epg/service/EpgSrvProImp;Lcom/mstar/android/tvapi/common/vo/EnumServiceType;I)V

    invoke-interface {v0, v1}, Ljava/util/concurrent/ExecutorService;->execute(Ljava/lang/Runnable;)V

    return-void
.end method

.method public getEventOfWeek(Lcom/mstar/android/tvapi/common/vo/EnumServiceType;I)V
    .locals 2
    .param p1    # Lcom/mstar/android/tvapi/common/vo/EnumServiceType;
    .param p2    # I

    iget-object v0, p0, Lcom/konka/epg/service/EpgSrvProImp;->eCacheThread:Ljava/util/concurrent/ExecutorService;

    new-instance v1, Lcom/konka/epg/service/EpgSrvProImp$2;

    invoke-direct {v1, p0, p1, p2}, Lcom/konka/epg/service/EpgSrvProImp$2;-><init>(Lcom/konka/epg/service/EpgSrvProImp;Lcom/mstar/android/tvapi/common/vo/EnumServiceType;I)V

    invoke-interface {v0, v1}, Ljava/util/concurrent/ExecutorService;->execute(Ljava/lang/Runnable;)V

    return-void
.end method

.method public getHandler()Lcom/konka/epg/ui/EpgMainMenuActivity$EpgHandler;
    .locals 1

    iget-object v0, p0, Lcom/konka/epg/service/EpgSrvProImp;->epgHandler:Lcom/konka/epg/ui/EpgMainMenuActivity$EpgHandler;

    return-object v0
.end method

.method public getM_WeekEventAdapterList()Ljava/util/ArrayList;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/ArrayList",
            "<",
            "Lcom/konka/epg/adapter/EventListAdapter;",
            ">;"
        }
    .end annotation

    iget-object v0, p0, Lcom/konka/epg/service/EpgSrvProImp;->m_WeekEventAdapterList:Ljava/util/ArrayList;

    return-object v0
.end method

.method public getRadioChannelAdapter()Lcom/konka/epg/adapter/ChannelListAdapter;
    .locals 1

    iget-object v0, p0, Lcom/konka/epg/service/EpgSrvProImp;->m_RadioChannelAdapter:Lcom/konka/epg/adapter/ChannelListAdapter;

    return-object v0
.end method

.method public getTimeInfo()Landroid/text/format/Time;
    .locals 1

    iget-object v0, p0, Lcom/konka/epg/service/EpgSrvProImp;->timeInfo:Landroid/text/format/Time;

    return-object v0
.end method

.method public initPosInChannelList()I
    .locals 6

    const/4 v1, 0x0

    iget-object v3, p0, Lcom/konka/epg/service/EpgSrvProImp;->epgDesk:Lcom/konka/kkinterface/tv/EpgDesk;

    invoke-interface {v3}, Lcom/konka/kkinterface/tv/EpgDesk;->getCurKonkaProgInfo()Lcom/konka/kkimplements/tv/vo/KonkaProgInfo;

    move-result-object v2

    invoke-static {}, Lcom/konka/epg/service/EpgSrvProImp;->$SWITCH_TABLE$com$mstar$android$tvapi$common$vo$EnumServiceType()[I

    move-result-object v3

    invoke-static {}, Lcom/mstar/android/tvapi/common/vo/EnumServiceType;->values()[Lcom/mstar/android/tvapi/common/vo/EnumServiceType;

    move-result-object v4

    iget-short v5, v2, Lcom/konka/kkimplements/tv/vo/KonkaProgInfo;->_mProgType:S

    aget-object v4, v4, v5

    invoke-virtual {v4}, Lcom/mstar/android/tvapi/common/vo/EnumServiceType;->ordinal()I

    move-result v4

    aget v3, v3, v4

    packed-switch v3, :pswitch_data_0

    const/4 v1, -0x1

    :cond_0
    :goto_0
    return v1

    :pswitch_0
    const/4 v0, 0x0

    :goto_1
    iget-object v3, p0, Lcom/konka/epg/service/EpgSrvProImp;->m_DtvChannelAdapter:Lcom/konka/epg/adapter/ChannelListAdapter;

    invoke-virtual {v3}, Lcom/konka/epg/adapter/ChannelListAdapter;->getCount()I

    move-result v3

    if-ge v0, v3, :cond_0

    new-instance v3, Ljava/lang/StringBuilder;

    const-string v4, "Now prog No is ----->"

    invoke-direct {v3, v4}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    iget v4, v2, Lcom/konka/kkimplements/tv/vo/KonkaProgInfo;->_mProgNo:I

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v3

    const-string v4, " Now prog No in List is----->"

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    iget-object v4, p0, Lcom/konka/epg/service/EpgSrvProImp;->m_DtvChannelAdapter:Lcom/konka/epg/adapter/ChannelListAdapter;

    invoke-virtual {v4, v0}, Lcom/konka/epg/adapter/ChannelListAdapter;->getItem(I)Lcom/konka/kkimplements/tv/vo/KonkaProgInfo;

    move-result-object v4

    iget v4, v4, Lcom/konka/kkimplements/tv/vo/KonkaProgInfo;->_mProgNo:I

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v3}, Lcom/konka/epg/util/EpgDebugPrint;->Debug(Ljava/lang/String;)V

    iget v3, v2, Lcom/konka/kkimplements/tv/vo/KonkaProgInfo;->_mProgNo:I

    iget-object v4, p0, Lcom/konka/epg/service/EpgSrvProImp;->m_DtvChannelAdapter:Lcom/konka/epg/adapter/ChannelListAdapter;

    invoke-virtual {v4, v0}, Lcom/konka/epg/adapter/ChannelListAdapter;->getItem(I)Lcom/konka/kkimplements/tv/vo/KonkaProgInfo;

    move-result-object v4

    iget v4, v4, Lcom/konka/kkimplements/tv/vo/KonkaProgInfo;->_mProgNo:I

    if-ne v3, v4, :cond_1

    move v1, v0

    goto :goto_0

    :cond_1
    const/4 v1, -0x1

    add-int/lit8 v0, v0, 0x1

    goto :goto_1

    :pswitch_1
    const/4 v0, 0x0

    :goto_2
    iget-object v3, p0, Lcom/konka/epg/service/EpgSrvProImp;->m_RadioChannelAdapter:Lcom/konka/epg/adapter/ChannelListAdapter;

    invoke-virtual {v3}, Lcom/konka/epg/adapter/ChannelListAdapter;->getCount()I

    move-result v3

    if-ge v0, v3, :cond_0

    iget v3, v2, Lcom/konka/kkimplements/tv/vo/KonkaProgInfo;->_mProgNo:I

    iget-object v4, p0, Lcom/konka/epg/service/EpgSrvProImp;->m_RadioChannelAdapter:Lcom/konka/epg/adapter/ChannelListAdapter;

    invoke-virtual {v4, v0}, Lcom/konka/epg/adapter/ChannelListAdapter;->getItem(I)Lcom/konka/kkimplements/tv/vo/KonkaProgInfo;

    move-result-object v4

    iget v4, v4, Lcom/konka/kkimplements/tv/vo/KonkaProgInfo;->_mProgNo:I

    if-ne v3, v4, :cond_2

    move v1, v0

    goto :goto_0

    :cond_2
    const/4 v1, -0x1

    add-int/lit8 v0, v0, 0x1

    goto :goto_2

    :pswitch_data_0
    .packed-switch 0x2
        :pswitch_0
        :pswitch_1
    .end packed-switch
.end method

.method public initTimeInfoFromDtv()V
    .locals 2

    iget-object v0, p0, Lcom/konka/epg/service/EpgSrvProImp;->eCacheThread:Ljava/util/concurrent/ExecutorService;

    new-instance v1, Lcom/konka/epg/service/EpgSrvProImp$1;

    invoke-direct {v1, p0}, Lcom/konka/epg/service/EpgSrvProImp$1;-><init>(Lcom/konka/epg/service/EpgSrvProImp;)V

    invoke-interface {v0, v1}, Ljava/util/concurrent/ExecutorService;->execute(Ljava/lang/Runnable;)V

    return-void
.end method

.method public isSignalStabled()Z
    .locals 1

    iget-object v0, p0, Lcom/konka/epg/service/EpgSrvProImp;->channelDesk:Lcom/konka/kkinterface/tv/ChannelDesk;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/konka/epg/service/EpgSrvProImp;->channelDesk:Lcom/konka/kkinterface/tv/ChannelDesk;

    invoke-interface {v0}, Lcom/konka/kkinterface/tv/ChannelDesk;->isSignalStabled()Z

    move-result v0

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public openDB()V
    .locals 1

    iget-object v0, p0, Lcom/konka/epg/service/EpgSrvProImp;->epgDesk:Lcom/konka/kkinterface/tv/EpgDesk;

    invoke-interface {v0}, Lcom/konka/kkinterface/tv/EpgDesk;->openDB()V

    return-void
.end method

.method public refreshDayEvent(Lcom/mstar/android/tvapi/common/vo/EnumServiceType;II)V
    .locals 2
    .param p1    # Lcom/mstar/android/tvapi/common/vo/EnumServiceType;
    .param p2    # I
    .param p3    # I

    iget-object v0, p0, Lcom/konka/epg/service/EpgSrvProImp;->eCacheThread:Ljava/util/concurrent/ExecutorService;

    new-instance v1, Lcom/konka/epg/service/EpgSrvProImp$7;

    invoke-direct {v1, p0, p1, p2, p3}, Lcom/konka/epg/service/EpgSrvProImp$7;-><init>(Lcom/konka/epg/service/EpgSrvProImp;Lcom/mstar/android/tvapi/common/vo/EnumServiceType;II)V

    invoke-interface {v0, v1}, Ljava/util/concurrent/ExecutorService;->execute(Ljava/lang/Runnable;)V

    return-void
.end method

.method public releaseHandle()V
    .locals 2

    iget-object v0, p0, Lcom/konka/epg/service/EpgSrvProImp;->tvDeskProvider:Lcom/konka/kkinterface/tv/TvDeskProvider;

    invoke-interface {v0}, Lcom/konka/kkinterface/tv/TvDeskProvider;->getCommonManagerInstance()Lcom/konka/kkinterface/tv/CommonDesk;

    move-result-object v0

    const/4 v1, 0x0

    invoke-interface {v0, v1}, Lcom/konka/kkinterface/tv/CommonDesk;->releaseHandler(I)V

    return-void
.end method

.method public scaleWindow()V
    .locals 6

    const/4 v3, 0x0

    new-instance v1, Lcom/mstar/android/tvapi/common/vo/VideoWindowType;

    invoke-direct {v1}, Lcom/mstar/android/tvapi/common/vo/VideoWindowType;-><init>()V

    iget-object v2, p0, Lcom/konka/epg/service/EpgSrvProImp;->surfaceParams:Landroid/view/WindowManager$LayoutParams;

    iget v4, v2, Landroid/view/WindowManager$LayoutParams;->height:I

    iget-object v2, p0, Lcom/konka/epg/service/EpgSrvProImp;->commonDesk:Lcom/konka/kkinterface/tv/CommonDesk;

    iget-object v5, p0, Lcom/konka/epg/service/EpgSrvProImp;->context:Landroid/content/Context;

    invoke-interface {v2, v5}, Lcom/konka/kkinterface/tv/CommonDesk;->is1280x720(Landroid/content/Context;)Z

    move-result v2

    if-eqz v2, :cond_2

    const/16 v2, -0x5d

    :goto_0
    add-int/2addr v2, v4

    iput v2, v1, Lcom/mstar/android/tvapi/common/vo/VideoWindowType;->height:I

    iget-object v2, p0, Lcom/konka/epg/service/EpgSrvProImp;->surfaceParams:Landroid/view/WindowManager$LayoutParams;

    iget v4, v2, Landroid/view/WindowManager$LayoutParams;->width:I

    iget-object v2, p0, Lcom/konka/epg/service/EpgSrvProImp;->commonDesk:Lcom/konka/kkinterface/tv/CommonDesk;

    iget-object v5, p0, Lcom/konka/epg/service/EpgSrvProImp;->context:Landroid/content/Context;

    invoke-interface {v2, v5}, Lcom/konka/kkinterface/tv/CommonDesk;->is1280x720(Landroid/content/Context;)Z

    move-result v2

    if-eqz v2, :cond_3

    const/16 v2, -0xab

    :goto_1
    add-int/2addr v2, v4

    iput v2, v1, Lcom/mstar/android/tvapi/common/vo/VideoWindowType;->width:I

    iget-object v2, p0, Lcom/konka/epg/service/EpgSrvProImp;->surfaceParams:Landroid/view/WindowManager$LayoutParams;

    iget v4, v2, Landroid/view/WindowManager$LayoutParams;->x:I

    iget-object v2, p0, Lcom/konka/epg/service/EpgSrvProImp;->commonDesk:Lcom/konka/kkinterface/tv/CommonDesk;

    iget-object v5, p0, Lcom/konka/epg/service/EpgSrvProImp;->context:Landroid/content/Context;

    invoke-interface {v2, v5}, Lcom/konka/kkinterface/tv/CommonDesk;->is1280x720(Landroid/content/Context;)Z

    move-result v2

    if-eqz v2, :cond_4

    const/16 v2, -0x28

    :goto_2
    add-int/2addr v2, v4

    iput v2, v1, Lcom/mstar/android/tvapi/common/vo/VideoWindowType;->x:I

    iget-object v2, p0, Lcom/konka/epg/service/EpgSrvProImp;->surfaceParams:Landroid/view/WindowManager$LayoutParams;

    iget v2, v2, Landroid/view/WindowManager$LayoutParams;->y:I

    iget-object v4, p0, Lcom/konka/epg/service/EpgSrvProImp;->commonDesk:Lcom/konka/kkinterface/tv/CommonDesk;

    iget-object v5, p0, Lcom/konka/epg/service/EpgSrvProImp;->context:Landroid/content/Context;

    invoke-interface {v4, v5}, Lcom/konka/kkinterface/tv/CommonDesk;->is1280x720(Landroid/content/Context;)Z

    move-result v4

    if-eqz v4, :cond_0

    const/16 v3, -0xb9

    :cond_0
    add-int/2addr v2, v3

    iput v2, v1, Lcom/mstar/android/tvapi/common/vo/VideoWindowType;->y:I

    :try_start_0
    invoke-static {}, Lcom/mstar/android/tvapi/common/TvManager;->getInstance()Lcom/mstar/android/tvapi/common/TvManager;

    move-result-object v2

    if-eqz v2, :cond_1

    invoke-static {}, Lcom/mstar/android/tvapi/common/TvManager;->getInstance()Lcom/mstar/android/tvapi/common/TvManager;

    move-result-object v2

    invoke-virtual {v2}, Lcom/mstar/android/tvapi/common/TvManager;->getPictureManager()Lcom/mstar/android/tvapi/common/PictureManager;

    move-result-object v2

    if-eqz v2, :cond_1

    invoke-static {}, Lcom/mstar/android/tvapi/common/TvManager;->getInstance()Lcom/mstar/android/tvapi/common/TvManager;

    move-result-object v2

    invoke-virtual {v2}, Lcom/mstar/android/tvapi/common/TvManager;->getPictureManager()Lcom/mstar/android/tvapi/common/PictureManager;

    move-result-object v2

    sget-object v3, Lcom/mstar/android/tvapi/common/vo/EnumScalerWindow;->E_MAIN_WINDOW:Lcom/mstar/android/tvapi/common/vo/EnumScalerWindow;

    invoke-virtual {v2, v3}, Lcom/mstar/android/tvapi/common/PictureManager;->selectWindow(Lcom/mstar/android/tvapi/common/vo/EnumScalerWindow;)Z

    invoke-static {}, Lcom/mstar/android/tvapi/common/TvManager;->getInstance()Lcom/mstar/android/tvapi/common/TvManager;

    move-result-object v2

    invoke-virtual {v2}, Lcom/mstar/android/tvapi/common/TvManager;->getPictureManager()Lcom/mstar/android/tvapi/common/PictureManager;

    move-result-object v2

    invoke-virtual {v2, v1}, Lcom/mstar/android/tvapi/common/PictureManager;->setDisplayWindow(Lcom/mstar/android/tvapi/common/vo/VideoWindowType;)V

    invoke-static {}, Lcom/mstar/android/tvapi/common/TvManager;->getInstance()Lcom/mstar/android/tvapi/common/TvManager;

    move-result-object v2

    invoke-virtual {v2}, Lcom/mstar/android/tvapi/common/TvManager;->getPictureManager()Lcom/mstar/android/tvapi/common/PictureManager;

    move-result-object v2

    invoke-virtual {v2}, Lcom/mstar/android/tvapi/common/PictureManager;->scaleWindow()Z
    :try_end_0
    .catch Lcom/mstar/android/tvapi/common/exception/TvCommonException; {:try_start_0 .. :try_end_0} :catch_0

    :cond_1
    :goto_3
    return-void

    :cond_2
    move v2, v3

    goto :goto_0

    :cond_3
    move v2, v3

    goto :goto_1

    :cond_4
    move v2, v3

    goto :goto_2

    :catch_0
    move-exception v0

    invoke-virtual {v0}, Lcom/mstar/android/tvapi/common/exception/TvCommonException;->printStackTrace()V

    goto :goto_3
.end method

.method public setActivity(Landroid/app/Activity;)V
    .locals 2
    .param p1    # Landroid/app/Activity;

    iput-object p1, p0, Lcom/konka/epg/service/EpgSrvProImp;->activity:Landroid/app/Activity;

    new-instance v1, Lcom/konka/epg/ui/EpgMainMenuActivity$EpgHandler;

    iget-object v0, p0, Lcom/konka/epg/service/EpgSrvProImp;->activity:Landroid/app/Activity;

    check-cast v0, Lcom/konka/epg/ui/EpgMainMenuActivity;

    invoke-virtual {v0}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    invoke-direct {v1, v0}, Lcom/konka/epg/ui/EpgMainMenuActivity$EpgHandler;-><init>(Lcom/konka/epg/ui/EpgMainMenuActivity;)V

    iput-object v1, p0, Lcom/konka/epg/service/EpgSrvProImp;->epgHandler:Lcom/konka/epg/ui/EpgMainMenuActivity$EpgHandler;

    invoke-direct {p0}, Lcom/konka/epg/service/EpgSrvProImp;->initdesk()V

    invoke-direct {p0}, Lcom/konka/epg/service/EpgSrvProImp;->initlist()V

    return-void
.end method

.method public setDisplayWin(IIII)V
    .locals 1
    .param p1    # I
    .param p2    # I
    .param p3    # I
    .param p4    # I

    iget-object v0, p0, Lcom/konka/epg/service/EpgSrvProImp;->epgDesk:Lcom/konka/kkinterface/tv/EpgDesk;

    invoke-interface {v0, p1, p2, p3, p4}, Lcom/konka/kkinterface/tv/EpgDesk;->setDispalyWindow(IIII)V

    const-string v0, "\u8bbe\u7f6e\u7a97\u53e3"

    invoke-static {v0}, Lcom/konka/epg/util/EpgDebugPrint;->Debug(Ljava/lang/String;)V

    return-void
.end method

.method public setListener(Lcom/konka/epg/service/EpgServiceListener;)V
    .locals 0
    .param p1    # Lcom/konka/epg/service/EpgServiceListener;

    iput-object p1, p0, Lcom/konka/epg/service/EpgSrvProImp;->epgServiceListener:Lcom/konka/epg/service/EpgServiceListener;

    return-void
.end method

.method public showTVWidgetSurface(IIII)V
    .locals 3
    .param p1    # I
    .param p2    # I
    .param p3    # I
    .param p4    # I

    iget-object v1, p0, Lcom/konka/epg/service/EpgSrvProImp;->activity:Landroid/app/Activity;

    const v2, 0x7f0a0014    # com.konka.epg.R.id.epg_mainmenu_surface_view

    invoke-virtual {v1, v2}, Landroid/app/Activity;->findViewById(I)Landroid/view/View;

    move-result-object v1

    check-cast v1, Landroid/view/SurfaceView;

    iput-object v1, p0, Lcom/konka/epg/service/EpgSrvProImp;->sufaceView:Landroid/view/SurfaceView;

    iget-object v1, p0, Lcom/konka/epg/service/EpgSrvProImp;->sufaceView:Landroid/view/SurfaceView;

    invoke-virtual {v1}, Landroid/view/SurfaceView;->getHolder()Landroid/view/SurfaceHolder;

    move-result-object v1

    const/4 v2, 0x3

    invoke-interface {v1, v2}, Landroid/view/SurfaceHolder;->setType(I)V

    iget-object v1, p0, Lcom/konka/epg/service/EpgSrvProImp;->activity:Landroid/app/Activity;

    invoke-virtual {v1}, Landroid/app/Activity;->getApplication()Landroid/app/Application;

    move-result-object v1

    check-cast v1, Lcom/konka/epg/TVRootApp;

    invoke-virtual {v1}, Lcom/konka/epg/TVRootApp;->getMywmParams()Landroid/view/WindowManager$LayoutParams;

    move-result-object v1

    iput-object v1, p0, Lcom/konka/epg/service/EpgSrvProImp;->surfaceParams:Landroid/view/WindowManager$LayoutParams;

    iget-object v1, p0, Lcom/konka/epg/service/EpgSrvProImp;->surfaceParams:Landroid/view/WindowManager$LayoutParams;

    iput p1, v1, Landroid/view/WindowManager$LayoutParams;->x:I

    iget-object v1, p0, Lcom/konka/epg/service/EpgSrvProImp;->surfaceParams:Landroid/view/WindowManager$LayoutParams;

    iput p2, v1, Landroid/view/WindowManager$LayoutParams;->y:I

    iget-object v1, p0, Lcom/konka/epg/service/EpgSrvProImp;->surfaceParams:Landroid/view/WindowManager$LayoutParams;

    iput p3, v1, Landroid/view/WindowManager$LayoutParams;->width:I

    iget-object v1, p0, Lcom/konka/epg/service/EpgSrvProImp;->surfaceParams:Landroid/view/WindowManager$LayoutParams;

    iput p4, v1, Landroid/view/WindowManager$LayoutParams;->height:I

    iget-object v1, p0, Lcom/konka/epg/service/EpgSrvProImp;->surfaceParams:Landroid/view/WindowManager$LayoutParams;

    const/16 v2, 0x33

    iput v2, v1, Landroid/view/WindowManager$LayoutParams;->gravity:I

    new-instance v0, Lcom/konka/epg/service/EpgSrvProImp$6;

    invoke-direct {v0, p0}, Lcom/konka/epg/service/EpgSrvProImp$6;-><init>(Lcom/konka/epg/service/EpgSrvProImp;)V

    iget-object v1, p0, Lcom/konka/epg/service/EpgSrvProImp;->sufaceView:Landroid/view/SurfaceView;

    invoke-virtual {v1}, Landroid/view/SurfaceView;->getHolder()Landroid/view/SurfaceHolder;

    move-result-object v1

    invoke-interface {v1, v0}, Landroid/view/SurfaceHolder;->addCallback(Landroid/view/SurfaceHolder$Callback;)V

    return-void
.end method
