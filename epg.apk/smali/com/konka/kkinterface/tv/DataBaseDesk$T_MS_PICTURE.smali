.class public Lcom/konka/kkinterface/tv/DataBaseDesk$T_MS_PICTURE;
.super Ljava/lang/Object;
.source "DataBaseDesk.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/konka/kkinterface/tv/DataBaseDesk;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x9
    name = "T_MS_PICTURE"
.end annotation


# instance fields
.field public backlight:S

.field public brightness:S

.field public contrast:S

.field public eColorTemp:Lcom/konka/kkinterface/tv/DataBaseDesk$EN_MS_COLOR_TEMP;

.field public eDynamicBacklight:Lcom/konka/kkinterface/tv/DataBaseDesk$EN_MS_PIC_ADV;

.field public eDynamicContrast:Lcom/konka/kkinterface/tv/DataBaseDesk$EN_MS_PIC_ADV;

.field public ePerfectClear:Lcom/konka/kkinterface/tv/DataBaseDesk$EN_MS_PIC_ADV;

.field public eVibrantColour:Lcom/konka/kkinterface/tv/DataBaseDesk$EN_MS_PIC_ADV;

.field public hue:S

.field public saturation:S

.field public sharpness:S


# direct methods
.method public constructor <init>(SSSSSSLcom/konka/kkinterface/tv/DataBaseDesk$EN_MS_COLOR_TEMP;Lcom/konka/kkinterface/tv/DataBaseDesk$EN_MS_PIC_ADV;Lcom/konka/kkinterface/tv/DataBaseDesk$EN_MS_PIC_ADV;Lcom/konka/kkinterface/tv/DataBaseDesk$EN_MS_PIC_ADV;Lcom/konka/kkinterface/tv/DataBaseDesk$EN_MS_PIC_ADV;)V
    .locals 0
    .param p1    # S
    .param p2    # S
    .param p3    # S
    .param p4    # S
    .param p5    # S
    .param p6    # S
    .param p7    # Lcom/konka/kkinterface/tv/DataBaseDesk$EN_MS_COLOR_TEMP;
    .param p8    # Lcom/konka/kkinterface/tv/DataBaseDesk$EN_MS_PIC_ADV;
    .param p9    # Lcom/konka/kkinterface/tv/DataBaseDesk$EN_MS_PIC_ADV;
    .param p10    # Lcom/konka/kkinterface/tv/DataBaseDesk$EN_MS_PIC_ADV;
    .param p11    # Lcom/konka/kkinterface/tv/DataBaseDesk$EN_MS_PIC_ADV;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    iput-short p1, p0, Lcom/konka/kkinterface/tv/DataBaseDesk$T_MS_PICTURE;->backlight:S

    iput-short p2, p0, Lcom/konka/kkinterface/tv/DataBaseDesk$T_MS_PICTURE;->contrast:S

    iput-short p3, p0, Lcom/konka/kkinterface/tv/DataBaseDesk$T_MS_PICTURE;->brightness:S

    iput-short p4, p0, Lcom/konka/kkinterface/tv/DataBaseDesk$T_MS_PICTURE;->saturation:S

    iput-short p5, p0, Lcom/konka/kkinterface/tv/DataBaseDesk$T_MS_PICTURE;->sharpness:S

    iput-short p6, p0, Lcom/konka/kkinterface/tv/DataBaseDesk$T_MS_PICTURE;->hue:S

    iput-object p7, p0, Lcom/konka/kkinterface/tv/DataBaseDesk$T_MS_PICTURE;->eColorTemp:Lcom/konka/kkinterface/tv/DataBaseDesk$EN_MS_COLOR_TEMP;

    iput-object p8, p0, Lcom/konka/kkinterface/tv/DataBaseDesk$T_MS_PICTURE;->eVibrantColour:Lcom/konka/kkinterface/tv/DataBaseDesk$EN_MS_PIC_ADV;

    iput-object p9, p0, Lcom/konka/kkinterface/tv/DataBaseDesk$T_MS_PICTURE;->ePerfectClear:Lcom/konka/kkinterface/tv/DataBaseDesk$EN_MS_PIC_ADV;

    iput-object p10, p0, Lcom/konka/kkinterface/tv/DataBaseDesk$T_MS_PICTURE;->eDynamicContrast:Lcom/konka/kkinterface/tv/DataBaseDesk$EN_MS_PIC_ADV;

    iput-object p11, p0, Lcom/konka/kkinterface/tv/DataBaseDesk$T_MS_PICTURE;->eDynamicBacklight:Lcom/konka/kkinterface/tv/DataBaseDesk$EN_MS_PIC_ADV;

    return-void
.end method
