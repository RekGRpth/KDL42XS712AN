.class public interface abstract Lcom/konka/kkinterface/tv/ChannelDesk;
.super Ljava/lang/Object;
.source "ChannelDesk.java"

# interfaces
.implements Lcom/konka/kkinterface/tv/AtvInterface;
.implements Lcom/konka/kkinterface/tv/BaseDesk;
.implements Lcom/konka/kkinterface/tv/DtvInterface;


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/konka/kkinterface/tv/ChannelDesk$EN_TUNING_SCAN_TYPE;,
        Lcom/konka/kkinterface/tv/ChannelDesk$TV_TS_STATUS;
    }
.end annotation


# static fields
.field public static final max_atv_count:I = 0xff

.field public static final max_dtv_count:I = 0x3e8

.field public static final msrvPlayer:Ljava/lang/Object;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    const/4 v0, 0x0

    sput-object v0, Lcom/konka/kkinterface/tv/ChannelDesk;->msrvPlayer:Ljava/lang/Object;

    return-void
.end method


# virtual methods
.method public abstract GetTsStatus()Lcom/konka/kkinterface/tv/ChannelDesk$TV_TS_STATUS;
.end method

.method public abstract changeToFirstService(Lcom/mstar/android/tvapi/common/vo/EnumFirstServiceInputType;Lcom/mstar/android/tvapi/common/vo/EnumFirstServiceType;)Z
.end method

.method public abstract closeSubtitle()Z
.end method

.method public abstract closeTeletext()Z
.end method

.method public abstract getAtvVolumeCompensation(I)I
.end method

.method public abstract getAudioInfo()Lcom/mstar/android/tvapi/dtv/vo/DtvAudioInfo;
.end method

.method public abstract getChinaDvbcRegion()Lcom/mstar/android/tvapi/dtv/dvb/dvbc/vo/EnumChinaDvbcRegion;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/mstar/android/tvapi/common/exception/TvCommonException;
        }
    .end annotation
.end method

.method public abstract getCurrProgramInfo()Lcom/mstar/android/tvapi/common/vo/ProgramInfo;
.end method

.method public abstract getCurrentLanguageIndex(Ljava/lang/String;)Lcom/mstar/android/tvapi/common/vo/TvOsType$EnumLanguage;
.end method

.method public abstract getCurrentSignalInformation()Lcom/mstar/android/tvapi/common/vo/DtvProgramSignalInfo;
.end method

.method public abstract getNitFrequencyByDtvRegion(Lcom/mstar/android/tvapi/dtv/dvb/dvbc/vo/EnumChinaDvbcRegion;)[I
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/mstar/android/tvapi/common/exception/TvCommonException;
        }
    .end annotation
.end method

.method public abstract getProgramCount(Lcom/mstar/android/tvapi/common/vo/EnumProgramCountType;)I
.end method

.method public abstract getProgramInfo(Lcom/mstar/android/tvapi/common/vo/ProgramInfoQueryCriteria;Lcom/mstar/android/tvapi/common/vo/EnumProgramInfoType;)Lcom/mstar/android/tvapi/common/vo/ProgramInfo;
.end method

.method public abstract getProgramInfoByIndex(I)Lcom/mstar/android/tvapi/common/vo/ProgramInfo;
.end method

.method public abstract getProgramName(ILcom/konka/kkinterface/tv/DtvInterface$MEMBER_SERVICETYPE;S)Ljava/lang/String;
.end method

.method public abstract getSIFMtsMode()Lcom/mstar/android/tvapi/common/vo/EnumAtvAudioModeType;
.end method

.method public abstract getSubtitleInfo()Lcom/mstar/android/tvapi/dtv/vo/DtvSubtitleInfo;
.end method

.method public abstract getSystemCountry()Lcom/konka/kkinterface/tv/DataBaseDesk$MEMBER_COUNTRY;
.end method

.method public abstract getUserScanType()Lcom/konka/kkinterface/tv/ChannelDesk$EN_TUNING_SCAN_TYPE;
.end method

.method public abstract getVideoStandard()Lcom/mstar/android/tvapi/common/vo/EnumAvdVideoStandardType;
.end method

.method public abstract hasTeletextClockSignal()Z
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/mstar/android/tvapi/common/exception/TvCommonException;
        }
    .end annotation
.end method

.method public abstract hasTeletextSignal()Z
.end method

.method public abstract isSignalStabled()Z
.end method

.method public abstract isTeletextDisplayed()Z
.end method

.method public abstract isTeletextSubtitleChannel()Z
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/mstar/android/tvapi/common/exception/TvCommonException;
        }
    .end annotation
.end method

.method public abstract isTtxChannel()Z
.end method

.method public abstract makeSourceAtv()V
.end method

.method public abstract makeSourceDtv()V
.end method

.method public abstract moveProgram(II)V
.end method

.method public abstract openSubtitle(I)Z
.end method

.method public abstract openTeletext(Lcom/mstar/android/tvapi/common/vo/EnumTeletextMode;)Z
.end method

.method public abstract programDown()Z
.end method

.method public abstract programReturn()Z
.end method

.method public abstract programSel(ILcom/konka/kkinterface/tv/DtvInterface$MEMBER_SERVICETYPE;)Z
.end method

.method public abstract programUp()Z
.end method

.method public abstract readTurnChannelInterval(Landroid/content/Context;)I
.end method

.method public abstract saveAtvProgram(I)Z
.end method

.method public abstract sendTeletextCommand(Lcom/mstar/android/tvapi/common/vo/EnumTeletextCommand;)Z
.end method

.method public abstract setAtvVolumeCompensation(II)Z
.end method

.method public abstract setCableOperator(Lcom/mstar/android/tvapi/common/vo/EnumCableOperator;)V
.end method

.method public abstract setChannelChangeFreezeMode(Z)V
.end method

.method public abstract setChinaDvbcRegion(Lcom/mstar/android/tvapi/dtv/dvb/dvbc/vo/EnumChinaDvbcRegion;)V
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/mstar/android/tvapi/common/exception/TvCommonException;
        }
    .end annotation
.end method

.method public abstract setProgramName(ISLjava/lang/String;)V
.end method

.method public abstract setSystemCountry(Lcom/konka/kkinterface/tv/DataBaseDesk$MEMBER_COUNTRY;)V
.end method

.method public abstract setUserScanType(Lcom/konka/kkinterface/tv/ChannelDesk$EN_TUNING_SCAN_TYPE;)V
.end method

.method public abstract startAutoUpdateScan()V
.end method

.method public abstract startQuickScan()Z
.end method

.method public abstract switchAudioTrack(I)V
.end method
