.class public interface abstract Lcom/konka/kkinterface/tv/DataBaseDesk;
.super Ljava/lang/Object;
.source "DataBaseDesk.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/konka/kkinterface/tv/DataBaseDesk$AUDIOMODE_TYPE_;,
        Lcom/konka/kkinterface/tv/DataBaseDesk$AUDIO_PEQ_PARAM;,
        Lcom/konka/kkinterface/tv/DataBaseDesk$ColorWheelMode;,
        Lcom/konka/kkinterface/tv/DataBaseDesk$CustomerCfgMiscSetting;,
        Lcom/konka/kkinterface/tv/DataBaseDesk$EN_3D_INPUT_TYPE;,
        Lcom/konka/kkinterface/tv/DataBaseDesk$EN_3D_OUTPUT_TYPE;,
        Lcom/konka/kkinterface/tv/DataBaseDesk$EN_AUDYSSEY_DYNAMIC_VOLUME_MODE;,
        Lcom/konka/kkinterface/tv/DataBaseDesk$EN_AUDYSSEY_EQ_MODE;,
        Lcom/konka/kkinterface/tv/DataBaseDesk$EN_AUD_MODE;,
        Lcom/konka/kkinterface/tv/DataBaseDesk$EN_CABLE_OPERATORS;,
        Lcom/konka/kkinterface/tv/DataBaseDesk$EN_CI_FUNCTION;,
        Lcom/konka/kkinterface/tv/DataBaseDesk$EN_DISPLAY_RES_TYPE;,
        Lcom/konka/kkinterface/tv/DataBaseDesk$EN_DISPLAY_TVFORMAT;,
        Lcom/konka/kkinterface/tv/DataBaseDesk$EN_MFC;,
        Lcom/konka/kkinterface/tv/DataBaseDesk$EN_MS_3D_IMPL_TYPE;,
        Lcom/konka/kkinterface/tv/DataBaseDesk$EN_MS_CHANNEL_SWITCH_MODE;,
        Lcom/konka/kkinterface/tv/DataBaseDesk$EN_MS_COLOR_TEMP;,
        Lcom/konka/kkinterface/tv/DataBaseDesk$EN_MS_COLOR_TEMP_INPUT_SOURCE;,
        Lcom/konka/kkinterface/tv/DataBaseDesk$EN_MS_Dynamic_Contrast;,
        Lcom/konka/kkinterface/tv/DataBaseDesk$EN_MS_FILM;,
        Lcom/konka/kkinterface/tv/DataBaseDesk$EN_MS_LOCALDIMMING;,
        Lcom/konka/kkinterface/tv/DataBaseDesk$EN_MS_MPEG_NR;,
        Lcom/konka/kkinterface/tv/DataBaseDesk$EN_MS_NR;,
        Lcom/konka/kkinterface/tv/DataBaseDesk$EN_MS_OFFLINE_DET_MODE;,
        Lcom/konka/kkinterface/tv/DataBaseDesk$EN_MS_PICTURE;,
        Lcom/konka/kkinterface/tv/DataBaseDesk$EN_MS_PIC_ADV;,
        Lcom/konka/kkinterface/tv/DataBaseDesk$EN_MS_POWERON_LOGO;,
        Lcom/konka/kkinterface/tv/DataBaseDesk$EN_MS_POWERON_MUSIC;,
        Lcom/konka/kkinterface/tv/DataBaseDesk$EN_MS_SUPER;,
        Lcom/konka/kkinterface/tv/DataBaseDesk$EN_MS_VIDEOITEM;,
        Lcom/konka/kkinterface/tv/DataBaseDesk$EN_SATELLITE_PLATFORM;,
        Lcom/konka/kkinterface/tv/DataBaseDesk$EN_SOUND_AD_OUTPUT;,
        Lcom/konka/kkinterface/tv/DataBaseDesk$EN_SOUND_MODE;,
        Lcom/konka/kkinterface/tv/DataBaseDesk$EN_SPDIF_MODE;,
        Lcom/konka/kkinterface/tv/DataBaseDesk$EN_SRS_SET_TYPE;,
        Lcom/konka/kkinterface/tv/DataBaseDesk$EN_SURROUND_MODE;,
        Lcom/konka/kkinterface/tv/DataBaseDesk$EN_SURROUND_SYSTEM_TYPE;,
        Lcom/konka/kkinterface/tv/DataBaseDesk$EN_SURROUND_TYPE;,
        Lcom/konka/kkinterface/tv/DataBaseDesk$EN_SYSTEM_FACTORY_DB_COMMAND;,
        Lcom/konka/kkinterface/tv/DataBaseDesk$EN_ThreeD_Video;,
        Lcom/konka/kkinterface/tv/DataBaseDesk$EN_ThreeD_Video_3DDEPTH;,
        Lcom/konka/kkinterface/tv/DataBaseDesk$EN_ThreeD_Video_3DOFFSET;,
        Lcom/konka/kkinterface/tv/DataBaseDesk$EN_ThreeD_Video_3DOUTPUTASPECT;,
        Lcom/konka/kkinterface/tv/DataBaseDesk$EN_ThreeD_Video_3DTO2D;,
        Lcom/konka/kkinterface/tv/DataBaseDesk$EN_ThreeD_Video_AUTOSTART;,
        Lcom/konka/kkinterface/tv/DataBaseDesk$EN_ThreeD_Video_DISPLAYFORMAT;,
        Lcom/konka/kkinterface/tv/DataBaseDesk$EN_ThreeD_Video_LRVIEWSWITCH;,
        Lcom/konka/kkinterface/tv/DataBaseDesk$EN_ThreeD_Video_SELFADAPTIVE_DETECT;,
        Lcom/konka/kkinterface/tv/DataBaseDesk$EN_ThreeD_Video_SELFADAPTIVE_LEVEL;,
        Lcom/konka/kkinterface/tv/DataBaseDesk$EN_VD_SIGNALTYPE;,
        Lcom/konka/kkinterface/tv/DataBaseDesk$E_ADC_SET_INDEX;,
        Lcom/konka/kkinterface/tv/DataBaseDesk$EnumSwitchOnOff;,
        Lcom/konka/kkinterface/tv/DataBaseDesk$HdmiAudioSource;,
        Lcom/konka/kkinterface/tv/DataBaseDesk$KK_SRS_SET;,
        Lcom/konka/kkinterface/tv/DataBaseDesk$MAPI_AVD_VideoStandardType;,
        Lcom/konka/kkinterface/tv/DataBaseDesk$MAPI_VIDEO_3D_ARC_Type;,
        Lcom/konka/kkinterface/tv/DataBaseDesk$MAPI_VIDEO_ARC_Type;,
        Lcom/konka/kkinterface/tv/DataBaseDesk$MAPI_VIDEO_OUT_VE_SYS;,
        Lcom/konka/kkinterface/tv/DataBaseDesk$MAX_DTV_Resolution_Info;,
        Lcom/konka/kkinterface/tv/DataBaseDesk$MAX_HDMI_Resolution_Info;,
        Lcom/konka/kkinterface/tv/DataBaseDesk$MAX_YPbPr_Resolution_Info;,
        Lcom/konka/kkinterface/tv/DataBaseDesk$MEMBER_COUNTRY;,
        Lcom/konka/kkinterface/tv/DataBaseDesk$MFC_MODE;,
        Lcom/konka/kkinterface/tv/DataBaseDesk$MS_ADC_SETTING;,
        Lcom/konka/kkinterface/tv/DataBaseDesk$MS_CEC_SETTING;,
        Lcom/konka/kkinterface/tv/DataBaseDesk$MS_FACTORY_CUS_DEF_SETTING;,
        Lcom/konka/kkinterface/tv/DataBaseDesk$MS_FACTORY_DTV_OVERSCAN_SETTING;,
        Lcom/konka/kkinterface/tv/DataBaseDesk$MS_FACTORY_EXTERN_SETTING;,
        Lcom/konka/kkinterface/tv/DataBaseDesk$MS_FACTORY_HDMI_OVERSCAN_SETTING;,
        Lcom/konka/kkinterface/tv/DataBaseDesk$MS_FACTORY_SSC_SET;,
        Lcom/konka/kkinterface/tv/DataBaseDesk$MS_FACTORY_VD_OVERSCAN_SETTING;,
        Lcom/konka/kkinterface/tv/DataBaseDesk$MS_FACTORY_YPbPr_OVERSCAN_SETTING;,
        Lcom/konka/kkinterface/tv/DataBaseDesk$MS_Factory_NS_VD_SET;,
        Lcom/konka/kkinterface/tv/DataBaseDesk$MS_Factory_NS_VIF_SET;,
        Lcom/konka/kkinterface/tv/DataBaseDesk$MS_NLA_POINT;,
        Lcom/konka/kkinterface/tv/DataBaseDesk$MS_NLA_SETTING;,
        Lcom/konka/kkinterface/tv/DataBaseDesk$MS_NLA_SET_INDEX;,
        Lcom/konka/kkinterface/tv/DataBaseDesk$MS_USER_LOCATION_SETTING;,
        Lcom/konka/kkinterface/tv/DataBaseDesk$MS_USER_SOUND_SETTING;,
        Lcom/konka/kkinterface/tv/DataBaseDesk$MS_USER_SUBTITLE_SETTING;,
        Lcom/konka/kkinterface/tv/DataBaseDesk$MS_USER_SYSTEM_SETTING;,
        Lcom/konka/kkinterface/tv/DataBaseDesk$SPDIF_TYPE;,
        Lcom/konka/kkinterface/tv/DataBaseDesk$ST_FACTORY_CI_SETTING;,
        Lcom/konka/kkinterface/tv/DataBaseDesk$ST_FACTORY_PEQ_SETTING;,
        Lcom/konka/kkinterface/tv/DataBaseDesk$ST_MAPI_3D_INFO;,
        Lcom/konka/kkinterface/tv/DataBaseDesk$ST_MAPI_BLOCK_SYS_SETTING;,
        Lcom/konka/kkinterface/tv/DataBaseDesk$ST_MAPI_VIDEO_WINDOW_INFO;,
        Lcom/konka/kkinterface/tv/DataBaseDesk$SkinToneMode;,
        Lcom/konka/kkinterface/tv/DataBaseDesk$SmartEnergySavingMode;,
        Lcom/konka/kkinterface/tv/DataBaseDesk$SoundModeSeting;,
        Lcom/konka/kkinterface/tv/DataBaseDesk$T_MS_CALIBRATION_DATA;,
        Lcom/konka/kkinterface/tv/DataBaseDesk$T_MS_COLOR_TEMP;,
        Lcom/konka/kkinterface/tv/DataBaseDesk$T_MS_COLOR_TEMPEX;,
        Lcom/konka/kkinterface/tv/DataBaseDesk$T_MS_COLOR_TEMPEX_DATA;,
        Lcom/konka/kkinterface/tv/DataBaseDesk$T_MS_COLOR_TEMP_DATA;,
        Lcom/konka/kkinterface/tv/DataBaseDesk$T_MS_NR_MODE;,
        Lcom/konka/kkinterface/tv/DataBaseDesk$T_MS_OVERSCAN_SETTING_USER;,
        Lcom/konka/kkinterface/tv/DataBaseDesk$T_MS_PICTURE;,
        Lcom/konka/kkinterface/tv/DataBaseDesk$T_MS_SUB_COLOR;,
        Lcom/konka/kkinterface/tv/DataBaseDesk$T_MS_VIDEO;,
        Lcom/konka/kkinterface/tv/DataBaseDesk$ThreeD_Video_MODE;
    }
.end annotation


# static fields
.field public static final EN_3D_SELFADAPTIVE_LEVEL_HIGH:I = 0x2

.field public static final EN_3D_SELFADAPTIVE_LEVEL_LOW:I = 0x0

.field public static final EN_3D_SELFADAPTIVE_LEVEL_MAX:I = 0x3

.field public static final EN_3D_SELFADAPTIVE_LEVEL_MIDDLE:I = 0x1

.field public static final EN_AUDIO_HIDEV_BW_L1:I = 0x1

.field public static final EN_AUDIO_HIDEV_BW_L2:I = 0x2

.field public static final EN_AUDIO_HIDEV_BW_L3:I = 0x3

.field public static final EN_AUDIO_HIDEV_BW_MAX:I = 0x4

.field public static final EN_AUDIO_HIDEV_OFF:I = 0x0

.field public static final EN_POWER_MODE_DIRECT:I = 0x2

.field public static final EN_POWER_MODE_MAX:I = 0x3

.field public static final EN_POWER_MODE_MEMORY:I = 0x1

.field public static final EN_POWER_MODE_SECONDARY:I = 0x0

.field public static final FACTORY_PRE_SET_ATV:I = 0x0

.field public static final FACTORY_PRE_SET_DTV:I = 0x1

.field public static final FACTORY_PRE_SET_NUM:I = 0x2

.field public static final TEST_PATTERN_MODE_BLACK:I = 0x4

.field public static final TEST_PATTERN_MODE_BLUE:I = 0x3

.field public static final TEST_PATTERN_MODE_GRAY:I = 0x0

.field public static final TEST_PATTERN_MODE_GREEN:I = 0x2

.field public static final TEST_PATTERN_MODE_OFF:I = 0x5

.field public static final TEST_PATTERN_MODE_RED:I = 0x1

.field public static final T_3DInfo_IDX:S = 0x0s

.field public static final T_3DSetting_IDX:S = 0x1s

.field public static final T_ADCAdjust_IDX:S = 0x25s

.field public static final T_ATVDefaultPrograms_IDX:S = 0x35s

.field public static final T_ATVOverscanSetting_IDX:S = 0x33s

.field public static final T_BlockSysSetting_IDX:S = 0x2s

.field public static final T_CECSetting_IDX:S = 0x3s

.field public static final T_CISetting_IDX:S = 0x4s

.field public static final T_ChinaDVBCSetting_IDX:S = 0x24s

.field public static final T_DB_VERSION_IDX:S = 0x5s

.field public static final T_DTVDefaultPrograms_IDX:S = 0x36s

.field public static final T_DTVOverscanSetting_IDX:S = 0x31s

.field public static final T_DvbtPresetting_IDX:S = 0x6s

.field public static final T_EpgTimer_IDX:S = 0x7s

.field public static final T_FacrotyColorTempEx3D_IDX:S = 0x38s

.field public static final T_FacrotyColorTempEx_IDX:S = 0x27s

.field public static final T_FacrotyColorTemp_IDX:S = 0x26s

.field public static final T_FactoryAudioSetting_IDX:S = 0x32s

.field public static final T_FactoryCusDefSetting_IDX:S = 0x34s

.field public static final T_FactoryExtern_IDX:S = 0x28s

.field public static final T_Factory_DB_VERSION_IDX:S = 0x2es

.field public static final T_FavTypeName_IDX:S = 0x8s

.field public static final T_HDMIOverscanSetting_IDX:S = 0x2fs

.field public static final T_InputSource_Type_IDX:S = 0x9s

.field public static final T_IsdbSysSetting_IDX:S = 0xas

.field public static final T_IsdbUserSetting_IDX:S = 0xbs

.field public static final T_MAX_IDX:S = 0x39s

.field public static final T_MHLAutoSwtich_IDX:S = 0x37s

.field public static final T_MediumSetting_IDX:S = 0xcs

.field public static final T_MfcMode_IDX:S = 0xds

.field public static final T_NRMode_IDX:S = 0xes

.field public static final T_NitInfo_IDX:S = 0xfs

.field public static final T_Nit_TSInfo_IDX:S = 0x10s

.field public static final T_NonLinearAdjust3D_IDX:S = 0x37s

.field public static final T_NonLinearAdjust_IDX:S = 0x2bs

.field public static final T_NonStarndardAdjust_IDX:S = 0x29s

.field public static final T_OADInfo_IDX:S = 0x11s

.field public static final T_OADInfo_UntDescriptor_IDX:S = 0x12s

.field public static final T_OADWakeUpInfo_IDX:S = 0x13s

.field public static final T_OverscanAdjust_IDX:S = 0x2cs

.field public static final T_PEQAdjust_IDX:S = 0x2ds

.field public static final T_PicMode_Setting_IDX:S = 0x14s

.field public static final T_PipSetting_IDX:S = 0x15s

.field public static final T_SSCAdjust_IDX:S = 0x2as

.field public static final T_SoundMode_Setting_IDX:S = 0x16s

.field public static final T_SoundSetting_IDX:S = 0x17s

.field public static final T_SubtitleSetting_IDX:S = 0x18s

.field public static final T_SystemSetting_IDX:S = 0x19s

.field public static final T_ThreeDVideoMode_IDX:S = 0x1as

.field public static final T_ThreeDVideoRouterSetting_IDX:S = 0x23s

.field public static final T_TimeSetting_IDX:S = 0x1bs

.field public static final T_USER_COLORTEMP_EX_IDX:S = 0x1ds

.field public static final T_USER_COLORTEMP_IDX:S = 0x1cs

.field public static final T_UserLocationSetting_IDX:S = 0x1es

.field public static final T_UserMMSetting_IDX:S = 0x1fs

.field public static final T_UserOverScanMode_IDX:S = 0x20s

.field public static final T_UserPCModeSetting_IDX:S = 0x21s

.field public static final T_VideoSetting_IDX:S = 0x22s

.field public static final T_YPbPrOverscanSetting_IDX:S = 0x30s


# virtual methods
.method public abstract IsChildLocked()Z
.end method

.method public abstract IsSystemLocked()Z
.end method

.method public abstract SyncUserSettingDB()V
.end method

.method public abstract UpdateDB()V
.end method

.method public abstract getAdcSetting()Lcom/konka/kkinterface/tv/DataBaseDesk$MS_ADC_SETTING;
.end method

.method public abstract getAlTimeshiftMode()Z
.end method

.method public abstract getBurnInMode()Z
.end method

.method public abstract getCECVar()Lcom/konka/kkinterface/tv/DataBaseDesk$MS_CEC_SETTING;
.end method

.method public abstract getCustomerCfgMiscSetting()Lcom/konka/kkinterface/tv/DataBaseDesk$CustomerCfgMiscSetting;
.end method

.method public abstract getDTVCity()Lcom/mstar/android/tvapi/dtv/dvb/dvbc/vo/EnumChinaDvbcRegion;
.end method

.method public abstract getDVBCNetTableFrequency()I
.end method

.method public abstract getDefinitionOnOff()Lcom/konka/kkinterface/tv/DataBaseDesk$EnumSwitchOnOff;
.end method

.method public abstract getDialogClarityOnOff()Lcom/konka/kkinterface/tv/DataBaseDesk$EnumSwitchOnOff;
.end method

.method public abstract getDynamicBLMode()I
.end method

.method public abstract getFactoryCusDefSetting()Lcom/konka/kkinterface/tv/DataBaseDesk$MS_FACTORY_CUS_DEF_SETTING;
.end method

.method public abstract getFactoryExt()Lcom/konka/kkinterface/tv/DataBaseDesk$MS_FACTORY_EXTERN_SETTING;
.end method

.method public abstract getLocationSet()Lcom/konka/kkinterface/tv/DataBaseDesk$MS_USER_LOCATION_SETTING;
.end method

.method public abstract getNoStandSet()Lcom/konka/kkinterface/tv/DataBaseDesk$MS_Factory_NS_VD_SET;
.end method

.method public abstract getNoStandVifSet()Lcom/konka/kkinterface/tv/DataBaseDesk$MS_Factory_NS_VIF_SET;
.end method

.method public abstract getParentalControlRating()I
.end method

.method public abstract getPowerOnMode()I
.end method

.method public abstract getSRSOnOff()Lcom/konka/kkinterface/tv/DataBaseDesk$EnumSwitchOnOff;
.end method

.method public abstract getSRSSet()Lcom/konka/kkinterface/tv/DataBaseDesk$KK_SRS_SET;
.end method

.method public abstract getSound()Lcom/konka/kkinterface/tv/DataBaseDesk$MS_USER_SOUND_SETTING;
.end method

.method public abstract getSoundMode(Lcom/konka/kkinterface/tv/DataBaseDesk$EN_SOUND_MODE;)Lcom/konka/kkinterface/tv/DataBaseDesk$SoundModeSeting;
.end method

.method public abstract getSpdifMode()Lcom/konka/kkinterface/tv/DataBaseDesk$SPDIF_TYPE;
.end method

.method public abstract getSscSet()Lcom/konka/kkinterface/tv/DataBaseDesk$MS_FACTORY_SSC_SET;
.end method

.method public abstract getSubtitleSet()Lcom/konka/kkinterface/tv/DataBaseDesk$MS_USER_SUBTITLE_SETTING;
.end method

.method public abstract getSystemLockPassword()I
.end method

.method public abstract getTruebaseOnOff()Lcom/konka/kkinterface/tv/DataBaseDesk$EnumSwitchOnOff;
.end method

.method public abstract getUsrData()Lcom/konka/kkinterface/tv/DataBaseDesk$MS_USER_SYSTEM_SETTING;
.end method

.method public abstract getVideo()Lcom/konka/kkinterface/tv/DataBaseDesk$T_MS_VIDEO;
.end method

.method public abstract getVideoTemp()Lcom/konka/kkinterface/tv/DataBaseDesk$T_MS_COLOR_TEMP;
.end method

.method public abstract getVideoTempEx()Lcom/konka/kkinterface/tv/DataBaseDesk$T_MS_COLOR_TEMPEX_DATA;
.end method

.method public abstract loadEssentialDataFromDB()V
.end method

.method public abstract queryAutoMHLSwitch()I
.end method

.method public abstract queryColorTemp(II)I
.end method

.method public abstract queryCurCountry()I
.end method

.method public abstract queryEventName(S)Ljava/lang/String;
.end method

.method public abstract queryServiceName(S)Ljava/lang/String;
.end method

.method public abstract querySourceIdent()I
.end method

.method public abstract querySourceSwit()I
.end method

.method public abstract queryVideo3DSelfAdaptiveDetectMode(I)Lcom/konka/kkinterface/tv/DataBaseDesk$EN_ThreeD_Video_SELFADAPTIVE_DETECT;
.end method

.method public abstract restoreUsrDB(Lcom/konka/kkinterface/tv/DataBaseDesk$EN_SYSTEM_FACTORY_DB_COMMAND;)Z
.end method

.method public abstract saveInputSource(Lcom/mstar/android/tvapi/common/vo/TvOsType$EnumInputSource;)Z
.end method

.method public abstract setAdcSetting(Lcom/konka/kkinterface/tv/DataBaseDesk$MS_ADC_SETTING;)Z
.end method

.method public abstract setAlTimeshiftMode(Z)V
.end method

.method public abstract setBlockSysPassword(I)V
.end method

.method public abstract setBurnInMode(Z)V
.end method

.method public abstract setCECVar(Lcom/konka/kkinterface/tv/DataBaseDesk$MS_CEC_SETTING;)Z
.end method

.method public abstract setCustomerCfgMiscSetting(Lcom/konka/kkinterface/tv/DataBaseDesk$CustomerCfgMiscSetting;)Z
.end method

.method public abstract setDTVCity(Lcom/mstar/android/tvapi/dtv/dvb/dvbc/vo/EnumChinaDvbcRegion;)V
.end method

.method public abstract setDVBCNetTableFrequency(I)V
.end method

.method public abstract setDefinitionOnOff(Lcom/konka/kkinterface/tv/DataBaseDesk$EnumSwitchOnOff;)V
.end method

.method public abstract setDialogClarityOnOff(Lcom/konka/kkinterface/tv/DataBaseDesk$EnumSwitchOnOff;)V
.end method

.method public abstract setDynamicBLMode(Lcom/konka/kkinterface/tv/DataBaseDesk$EN_MS_LOCALDIMMING;)V
.end method

.method public abstract setFactoryCusDefSetting(Lcom/konka/kkinterface/tv/DataBaseDesk$MS_FACTORY_CUS_DEF_SETTING;)Z
.end method

.method public abstract setFactoryExt(Lcom/konka/kkinterface/tv/DataBaseDesk$MS_FACTORY_EXTERN_SETTING;)Z
.end method

.method public abstract setLocationSet(Lcom/konka/kkinterface/tv/DataBaseDesk$MS_USER_LOCATION_SETTING;)Z
.end method

.method public abstract setNoStandSet(Lcom/konka/kkinterface/tv/DataBaseDesk$MS_Factory_NS_VD_SET;)Z
.end method

.method public abstract setNoStandVifSet(Lcom/konka/kkinterface/tv/DataBaseDesk$MS_Factory_NS_VIF_SET;)Z
.end method

.method public abstract setParentalControl(I)V
.end method

.method public abstract setSRSOnOff(Lcom/konka/kkinterface/tv/DataBaseDesk$EnumSwitchOnOff;)V
.end method

.method public abstract setSRSSet(Lcom/konka/kkinterface/tv/DataBaseDesk$KK_SRS_SET;Lcom/konka/kkinterface/tv/DataBaseDesk$EN_SRS_SET_TYPE;)Z
.end method

.method public abstract setSound(Lcom/konka/kkinterface/tv/DataBaseDesk$MS_USER_SOUND_SETTING;)Z
.end method

.method public abstract setSoundMode(Lcom/konka/kkinterface/tv/DataBaseDesk$EN_SOUND_MODE;)Z
.end method

.method public abstract setSoundMode(Lcom/konka/kkinterface/tv/DataBaseDesk$EN_SOUND_MODE;Lcom/konka/kkinterface/tv/DataBaseDesk$SoundModeSeting;)Z
.end method

.method public abstract setSpdifMode(Lcom/konka/kkinterface/tv/DataBaseDesk$SPDIF_TYPE;)V
.end method

.method public abstract setSscSet(Lcom/konka/kkinterface/tv/DataBaseDesk$MS_FACTORY_SSC_SET;)Z
.end method

.method public abstract setSubtitleSet(Lcom/konka/kkinterface/tv/DataBaseDesk$MS_USER_SUBTITLE_SETTING;)Z
.end method

.method public abstract setSystemLock(I)V
.end method

.method public abstract setTruebaseOnOff(Lcom/konka/kkinterface/tv/DataBaseDesk$EnumSwitchOnOff;)V
.end method

.method public abstract setUsrData(Lcom/konka/kkinterface/tv/DataBaseDesk$MS_USER_SYSTEM_SETTING;)Z
.end method

.method public abstract setVideo(Lcom/konka/kkinterface/tv/DataBaseDesk$T_MS_VIDEO;)Z
.end method

.method public abstract setVideoTemp(Lcom/konka/kkinterface/tv/DataBaseDesk$T_MS_COLOR_TEMP;)Z
.end method

.method public abstract setVideoTempEx(Lcom/konka/kkinterface/tv/DataBaseDesk$T_MS_COLOR_TEMPEX_DATA;)Z
.end method

.method public abstract updateAutoMHLSwitch(S)V
.end method

.method public abstract updateEventName(Ljava/lang/String;S)V
.end method

.method public abstract updateServiceName(Ljava/lang/String;S)V
.end method

.method public abstract updateSourceIdent(S)V
.end method

.method public abstract updateSourceSwit(S)V
.end method
