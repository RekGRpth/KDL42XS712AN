.class public final enum Lcom/konka/kkinterface/tv/CommonDesk$EnumDeskEvent;
.super Ljava/lang/Enum;
.source "CommonDesk.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/konka/kkinterface/tv/CommonDesk;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x4019
    name = "EnumDeskEvent"
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Enum",
        "<",
        "Lcom/konka/kkinterface/tv/CommonDesk$EnumDeskEvent;",
        ">;"
    }
.end annotation


# static fields
.field private static final synthetic ENUM$VALUES:[Lcom/konka/kkinterface/tv/CommonDesk$EnumDeskEvent;

.field public static final enum EV_ATV_AUTO_TUNING_SCAN_INFO:Lcom/konka/kkinterface/tv/CommonDesk$EnumDeskEvent;

.field public static final enum EV_ATV_MANUAL_TUNING_SCAN_INFO:Lcom/konka/kkinterface/tv/CommonDesk$EnumDeskEvent;

.field public static final enum EV_ATV_PROGRAM_INFO_READY:Lcom/konka/kkinterface/tv/CommonDesk$EnumDeskEvent;

.field public static final enum EV_AUDIO_MODE_CHANGE:Lcom/konka/kkinterface/tv/CommonDesk$EnumDeskEvent;

.field public static final enum EV_CHANGE_TTX_STATUS:Lcom/konka/kkinterface/tv/CommonDesk$EnumDeskEvent;

.field public static final enum EV_CI_LOAD_CREDENTIAL_FAIL:Lcom/konka/kkinterface/tv/CommonDesk$EnumDeskEvent;

.field public static final enum EV_DTV_AUTO_TUNING_SCAN_INFO:Lcom/konka/kkinterface/tv/CommonDesk$EnumDeskEvent;

.field public static final enum EV_DTV_AUTO_UPDATE_SCAN:Lcom/konka/kkinterface/tv/CommonDesk$EnumDeskEvent;

.field public static final enum EV_DTV_CHANNELNAME_READY:Lcom/konka/kkinterface/tv/CommonDesk$EnumDeskEvent;

.field public static final enum EV_DTV_PRI_COMPONENT_MISSING:Lcom/konka/kkinterface/tv/CommonDesk$EnumDeskEvent;

.field public static final enum EV_DTV_PROGRAM_INFO_READY:Lcom/konka/kkinterface/tv/CommonDesk$EnumDeskEvent;

.field public static final enum EV_EPGTIMER_SIMULCAST:Lcom/konka/kkinterface/tv/CommonDesk$EnumDeskEvent;

.field public static final enum EV_GINGA_STATUS_MODE:Lcom/konka/kkinterface/tv/CommonDesk$EnumDeskEvent;

.field public static final enum EV_HBBTV_STATUS_MODE:Lcom/konka/kkinterface/tv/CommonDesk$EnumDeskEvent;

.field public static final enum EV_HBBTV_UI_EVENT:Lcom/konka/kkinterface/tv/CommonDesk$EnumDeskEvent;

.field public static final enum EV_MHEG5_EVENT_HANDLER:Lcom/konka/kkinterface/tv/CommonDesk$EnumDeskEvent;

.field public static final enum EV_MHEG5_RETURN_KEY:Lcom/konka/kkinterface/tv/CommonDesk$EnumDeskEvent;

.field public static final enum EV_MHEG5_STATUS_MODE:Lcom/konka/kkinterface/tv/CommonDesk$EnumDeskEvent;

.field public static final enum EV_OAD_DOWNLOAD:Lcom/konka/kkinterface/tv/CommonDesk$EnumDeskEvent;

.field public static final enum EV_OAD_HANDLER:Lcom/konka/kkinterface/tv/CommonDesk$EnumDeskEvent;

.field public static final enum EV_OAD_TIMEOUT:Lcom/konka/kkinterface/tv/CommonDesk$EnumDeskEvent;

.field public static final enum EV_POPUP_DIALOG:Lcom/konka/kkinterface/tv/CommonDesk$EnumDeskEvent;

.field public static final enum EV_POPUP_SCAN_DIALOGE_FREQUENCY_CHANGE:Lcom/konka/kkinterface/tv/CommonDesk$EnumDeskEvent;

.field public static final enum EV_POPUP_SCAN_DIALOGE_LOSS_SIGNAL:Lcom/konka/kkinterface/tv/CommonDesk$EnumDeskEvent;

.field public static final enum EV_POPUP_SCAN_DIALOGE_NEW_MULTIPLEX:Lcom/konka/kkinterface/tv/CommonDesk$EnumDeskEvent;

.field public static final enum EV_PVR_NOTIFY_ALWAYS_TIMESHIFT_PROGRAM_NOTREADY:Lcom/konka/kkinterface/tv/CommonDesk$EnumDeskEvent;

.field public static final enum EV_PVR_NOTIFY_ALWAYS_TIMESHIFT_PROGRAM_READY:Lcom/konka/kkinterface/tv/CommonDesk$EnumDeskEvent;

.field public static final enum EV_PVR_NOTIFY_CI_PLUS_PROTECTION:Lcom/konka/kkinterface/tv/CommonDesk$EnumDeskEvent;

.field public static final enum EV_PVR_NOTIFY_CI_PLUS_RETENTION_LIMIT_UPDATE:Lcom/konka/kkinterface/tv/CommonDesk$EnumDeskEvent;

.field public static final enum EV_PVR_NOTIFY_OVER_RUN:Lcom/konka/kkinterface/tv/CommonDesk$EnumDeskEvent;

.field public static final enum EV_PVR_NOTIFY_PARENTAL_CONTROL:Lcom/konka/kkinterface/tv/CommonDesk$EnumDeskEvent;

.field public static final enum EV_PVR_NOTIFY_PLAYBACK_BEGIN:Lcom/konka/kkinterface/tv/CommonDesk$EnumDeskEvent;

.field public static final enum EV_PVR_NOTIFY_PLAYBACK_SPEED_CHANGE:Lcom/konka/kkinterface/tv/CommonDesk$EnumDeskEvent;

.field public static final enum EV_PVR_NOTIFY_PLAYBACK_STOP:Lcom/konka/kkinterface/tv/CommonDesk$EnumDeskEvent;

.field public static final enum EV_PVR_NOTIFY_PLAYBACK_TIME:Lcom/konka/kkinterface/tv/CommonDesk$EnumDeskEvent;

.field public static final enum EV_PVR_NOTIFY_RECORD_SIZE:Lcom/konka/kkinterface/tv/CommonDesk$EnumDeskEvent;

.field public static final enum EV_PVR_NOTIFY_RECORD_STOP:Lcom/konka/kkinterface/tv/CommonDesk$EnumDeskEvent;

.field public static final enum EV_PVR_NOTIFY_RECORD_TIME:Lcom/konka/kkinterface/tv/CommonDesk$EnumDeskEvent;

.field public static final enum EV_PVR_NOTIFY_TIMESHIFT_OVERWRITES_AFTER:Lcom/konka/kkinterface/tv/CommonDesk$EnumDeskEvent;

.field public static final enum EV_PVR_NOTIFY_TIMESHIFT_OVERWRITES_BEFORE:Lcom/konka/kkinterface/tv/CommonDesk$EnumDeskEvent;

.field public static final enum EV_PVR_NOTIFY_USB_REMOVED:Lcom/konka/kkinterface/tv/CommonDesk$EnumDeskEvent;

.field public static final enum EV_RCT_PRESENCE:Lcom/konka/kkinterface/tv/CommonDesk$EnumDeskEvent;

.field public static final enum EV_SCREEN_SAVER_MODE:Lcom/konka/kkinterface/tv/CommonDesk$EnumDeskEvent;

.field public static final enum EV_SIGNAL_LOCK:Lcom/konka/kkinterface/tv/CommonDesk$EnumDeskEvent;

.field public static final enum EV_SIGNAL_UNLOCK:Lcom/konka/kkinterface/tv/CommonDesk$EnumDeskEvent;

.field public static final enum EV_TS_CHANGE:Lcom/konka/kkinterface/tv/CommonDesk$EnumDeskEvent;


# direct methods
.method static constructor <clinit>()V
    .locals 8

    const/4 v7, 0x4

    const/4 v6, 0x3

    const/4 v5, 0x2

    const/4 v4, 0x1

    const/4 v3, 0x0

    new-instance v0, Lcom/konka/kkinterface/tv/CommonDesk$EnumDeskEvent;

    const-string v1, "EV_DTV_CHANNELNAME_READY"

    invoke-direct {v0, v1, v3}, Lcom/konka/kkinterface/tv/CommonDesk$EnumDeskEvent;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/konka/kkinterface/tv/CommonDesk$EnumDeskEvent;->EV_DTV_CHANNELNAME_READY:Lcom/konka/kkinterface/tv/CommonDesk$EnumDeskEvent;

    new-instance v0, Lcom/konka/kkinterface/tv/CommonDesk$EnumDeskEvent;

    const-string v1, "EV_ATV_AUTO_TUNING_SCAN_INFO"

    invoke-direct {v0, v1, v4}, Lcom/konka/kkinterface/tv/CommonDesk$EnumDeskEvent;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/konka/kkinterface/tv/CommonDesk$EnumDeskEvent;->EV_ATV_AUTO_TUNING_SCAN_INFO:Lcom/konka/kkinterface/tv/CommonDesk$EnumDeskEvent;

    new-instance v0, Lcom/konka/kkinterface/tv/CommonDesk$EnumDeskEvent;

    const-string v1, "EV_ATV_MANUAL_TUNING_SCAN_INFO"

    invoke-direct {v0, v1, v5}, Lcom/konka/kkinterface/tv/CommonDesk$EnumDeskEvent;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/konka/kkinterface/tv/CommonDesk$EnumDeskEvent;->EV_ATV_MANUAL_TUNING_SCAN_INFO:Lcom/konka/kkinterface/tv/CommonDesk$EnumDeskEvent;

    new-instance v0, Lcom/konka/kkinterface/tv/CommonDesk$EnumDeskEvent;

    const-string v1, "EV_DTV_AUTO_TUNING_SCAN_INFO"

    invoke-direct {v0, v1, v6}, Lcom/konka/kkinterface/tv/CommonDesk$EnumDeskEvent;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/konka/kkinterface/tv/CommonDesk$EnumDeskEvent;->EV_DTV_AUTO_TUNING_SCAN_INFO:Lcom/konka/kkinterface/tv/CommonDesk$EnumDeskEvent;

    new-instance v0, Lcom/konka/kkinterface/tv/CommonDesk$EnumDeskEvent;

    const-string v1, "EV_DTV_PROGRAM_INFO_READY"

    invoke-direct {v0, v1, v7}, Lcom/konka/kkinterface/tv/CommonDesk$EnumDeskEvent;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/konka/kkinterface/tv/CommonDesk$EnumDeskEvent;->EV_DTV_PROGRAM_INFO_READY:Lcom/konka/kkinterface/tv/CommonDesk$EnumDeskEvent;

    new-instance v0, Lcom/konka/kkinterface/tv/CommonDesk$EnumDeskEvent;

    const-string v1, "EV_SIGNAL_LOCK"

    const/4 v2, 0x5

    invoke-direct {v0, v1, v2}, Lcom/konka/kkinterface/tv/CommonDesk$EnumDeskEvent;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/konka/kkinterface/tv/CommonDesk$EnumDeskEvent;->EV_SIGNAL_LOCK:Lcom/konka/kkinterface/tv/CommonDesk$EnumDeskEvent;

    new-instance v0, Lcom/konka/kkinterface/tv/CommonDesk$EnumDeskEvent;

    const-string v1, "EV_SIGNAL_UNLOCK"

    const/4 v2, 0x6

    invoke-direct {v0, v1, v2}, Lcom/konka/kkinterface/tv/CommonDesk$EnumDeskEvent;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/konka/kkinterface/tv/CommonDesk$EnumDeskEvent;->EV_SIGNAL_UNLOCK:Lcom/konka/kkinterface/tv/CommonDesk$EnumDeskEvent;

    new-instance v0, Lcom/konka/kkinterface/tv/CommonDesk$EnumDeskEvent;

    const-string v1, "EV_POPUP_DIALOG"

    const/4 v2, 0x7

    invoke-direct {v0, v1, v2}, Lcom/konka/kkinterface/tv/CommonDesk$EnumDeskEvent;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/konka/kkinterface/tv/CommonDesk$EnumDeskEvent;->EV_POPUP_DIALOG:Lcom/konka/kkinterface/tv/CommonDesk$EnumDeskEvent;

    new-instance v0, Lcom/konka/kkinterface/tv/CommonDesk$EnumDeskEvent;

    const-string v1, "EV_SCREEN_SAVER_MODE"

    const/16 v2, 0x8

    invoke-direct {v0, v1, v2}, Lcom/konka/kkinterface/tv/CommonDesk$EnumDeskEvent;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/konka/kkinterface/tv/CommonDesk$EnumDeskEvent;->EV_SCREEN_SAVER_MODE:Lcom/konka/kkinterface/tv/CommonDesk$EnumDeskEvent;

    new-instance v0, Lcom/konka/kkinterface/tv/CommonDesk$EnumDeskEvent;

    const-string v1, "EV_CI_LOAD_CREDENTIAL_FAIL"

    const/16 v2, 0x9

    invoke-direct {v0, v1, v2}, Lcom/konka/kkinterface/tv/CommonDesk$EnumDeskEvent;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/konka/kkinterface/tv/CommonDesk$EnumDeskEvent;->EV_CI_LOAD_CREDENTIAL_FAIL:Lcom/konka/kkinterface/tv/CommonDesk$EnumDeskEvent;

    new-instance v0, Lcom/konka/kkinterface/tv/CommonDesk$EnumDeskEvent;

    const-string v1, "EV_EPGTIMER_SIMULCAST"

    const/16 v2, 0xa

    invoke-direct {v0, v1, v2}, Lcom/konka/kkinterface/tv/CommonDesk$EnumDeskEvent;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/konka/kkinterface/tv/CommonDesk$EnumDeskEvent;->EV_EPGTIMER_SIMULCAST:Lcom/konka/kkinterface/tv/CommonDesk$EnumDeskEvent;

    new-instance v0, Lcom/konka/kkinterface/tv/CommonDesk$EnumDeskEvent;

    const-string v1, "EV_HBBTV_STATUS_MODE"

    const/16 v2, 0xb

    invoke-direct {v0, v1, v2}, Lcom/konka/kkinterface/tv/CommonDesk$EnumDeskEvent;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/konka/kkinterface/tv/CommonDesk$EnumDeskEvent;->EV_HBBTV_STATUS_MODE:Lcom/konka/kkinterface/tv/CommonDesk$EnumDeskEvent;

    new-instance v0, Lcom/konka/kkinterface/tv/CommonDesk$EnumDeskEvent;

    const-string v1, "EV_MHEG5_STATUS_MODE"

    const/16 v2, 0xc

    invoke-direct {v0, v1, v2}, Lcom/konka/kkinterface/tv/CommonDesk$EnumDeskEvent;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/konka/kkinterface/tv/CommonDesk$EnumDeskEvent;->EV_MHEG5_STATUS_MODE:Lcom/konka/kkinterface/tv/CommonDesk$EnumDeskEvent;

    new-instance v0, Lcom/konka/kkinterface/tv/CommonDesk$EnumDeskEvent;

    const-string v1, "EV_MHEG5_RETURN_KEY"

    const/16 v2, 0xd

    invoke-direct {v0, v1, v2}, Lcom/konka/kkinterface/tv/CommonDesk$EnumDeskEvent;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/konka/kkinterface/tv/CommonDesk$EnumDeskEvent;->EV_MHEG5_RETURN_KEY:Lcom/konka/kkinterface/tv/CommonDesk$EnumDeskEvent;

    new-instance v0, Lcom/konka/kkinterface/tv/CommonDesk$EnumDeskEvent;

    const-string v1, "EV_OAD_HANDLER"

    const/16 v2, 0xe

    invoke-direct {v0, v1, v2}, Lcom/konka/kkinterface/tv/CommonDesk$EnumDeskEvent;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/konka/kkinterface/tv/CommonDesk$EnumDeskEvent;->EV_OAD_HANDLER:Lcom/konka/kkinterface/tv/CommonDesk$EnumDeskEvent;

    new-instance v0, Lcom/konka/kkinterface/tv/CommonDesk$EnumDeskEvent;

    const-string v1, "EV_OAD_DOWNLOAD"

    const/16 v2, 0xf

    invoke-direct {v0, v1, v2}, Lcom/konka/kkinterface/tv/CommonDesk$EnumDeskEvent;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/konka/kkinterface/tv/CommonDesk$EnumDeskEvent;->EV_OAD_DOWNLOAD:Lcom/konka/kkinterface/tv/CommonDesk$EnumDeskEvent;

    new-instance v0, Lcom/konka/kkinterface/tv/CommonDesk$EnumDeskEvent;

    const-string v1, "EV_PVR_NOTIFY_PLAYBACK_TIME"

    const/16 v2, 0x10

    invoke-direct {v0, v1, v2}, Lcom/konka/kkinterface/tv/CommonDesk$EnumDeskEvent;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/konka/kkinterface/tv/CommonDesk$EnumDeskEvent;->EV_PVR_NOTIFY_PLAYBACK_TIME:Lcom/konka/kkinterface/tv/CommonDesk$EnumDeskEvent;

    new-instance v0, Lcom/konka/kkinterface/tv/CommonDesk$EnumDeskEvent;

    const-string v1, "EV_PVR_NOTIFY_PLAYBACK_SPEED_CHANGE"

    const/16 v2, 0x11

    invoke-direct {v0, v1, v2}, Lcom/konka/kkinterface/tv/CommonDesk$EnumDeskEvent;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/konka/kkinterface/tv/CommonDesk$EnumDeskEvent;->EV_PVR_NOTIFY_PLAYBACK_SPEED_CHANGE:Lcom/konka/kkinterface/tv/CommonDesk$EnumDeskEvent;

    new-instance v0, Lcom/konka/kkinterface/tv/CommonDesk$EnumDeskEvent;

    const-string v1, "EV_PVR_NOTIFY_RECORD_TIME"

    const/16 v2, 0x12

    invoke-direct {v0, v1, v2}, Lcom/konka/kkinterface/tv/CommonDesk$EnumDeskEvent;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/konka/kkinterface/tv/CommonDesk$EnumDeskEvent;->EV_PVR_NOTIFY_RECORD_TIME:Lcom/konka/kkinterface/tv/CommonDesk$EnumDeskEvent;

    new-instance v0, Lcom/konka/kkinterface/tv/CommonDesk$EnumDeskEvent;

    const-string v1, "EV_PVR_NOTIFY_RECORD_SIZE"

    const/16 v2, 0x13

    invoke-direct {v0, v1, v2}, Lcom/konka/kkinterface/tv/CommonDesk$EnumDeskEvent;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/konka/kkinterface/tv/CommonDesk$EnumDeskEvent;->EV_PVR_NOTIFY_RECORD_SIZE:Lcom/konka/kkinterface/tv/CommonDesk$EnumDeskEvent;

    new-instance v0, Lcom/konka/kkinterface/tv/CommonDesk$EnumDeskEvent;

    const-string v1, "EV_PVR_NOTIFY_RECORD_STOP"

    const/16 v2, 0x14

    invoke-direct {v0, v1, v2}, Lcom/konka/kkinterface/tv/CommonDesk$EnumDeskEvent;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/konka/kkinterface/tv/CommonDesk$EnumDeskEvent;->EV_PVR_NOTIFY_RECORD_STOP:Lcom/konka/kkinterface/tv/CommonDesk$EnumDeskEvent;

    new-instance v0, Lcom/konka/kkinterface/tv/CommonDesk$EnumDeskEvent;

    const-string v1, "EV_PVR_NOTIFY_PLAYBACK_STOP"

    const/16 v2, 0x15

    invoke-direct {v0, v1, v2}, Lcom/konka/kkinterface/tv/CommonDesk$EnumDeskEvent;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/konka/kkinterface/tv/CommonDesk$EnumDeskEvent;->EV_PVR_NOTIFY_PLAYBACK_STOP:Lcom/konka/kkinterface/tv/CommonDesk$EnumDeskEvent;

    new-instance v0, Lcom/konka/kkinterface/tv/CommonDesk$EnumDeskEvent;

    const-string v1, "EV_PVR_NOTIFY_PLAYBACK_BEGIN"

    const/16 v2, 0x16

    invoke-direct {v0, v1, v2}, Lcom/konka/kkinterface/tv/CommonDesk$EnumDeskEvent;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/konka/kkinterface/tv/CommonDesk$EnumDeskEvent;->EV_PVR_NOTIFY_PLAYBACK_BEGIN:Lcom/konka/kkinterface/tv/CommonDesk$EnumDeskEvent;

    new-instance v0, Lcom/konka/kkinterface/tv/CommonDesk$EnumDeskEvent;

    const-string v1, "EV_PVR_NOTIFY_TIMESHIFT_OVERWRITES_BEFORE"

    const/16 v2, 0x17

    invoke-direct {v0, v1, v2}, Lcom/konka/kkinterface/tv/CommonDesk$EnumDeskEvent;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/konka/kkinterface/tv/CommonDesk$EnumDeskEvent;->EV_PVR_NOTIFY_TIMESHIFT_OVERWRITES_BEFORE:Lcom/konka/kkinterface/tv/CommonDesk$EnumDeskEvent;

    new-instance v0, Lcom/konka/kkinterface/tv/CommonDesk$EnumDeskEvent;

    const-string v1, "EV_PVR_NOTIFY_TIMESHIFT_OVERWRITES_AFTER"

    const/16 v2, 0x18

    invoke-direct {v0, v1, v2}, Lcom/konka/kkinterface/tv/CommonDesk$EnumDeskEvent;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/konka/kkinterface/tv/CommonDesk$EnumDeskEvent;->EV_PVR_NOTIFY_TIMESHIFT_OVERWRITES_AFTER:Lcom/konka/kkinterface/tv/CommonDesk$EnumDeskEvent;

    new-instance v0, Lcom/konka/kkinterface/tv/CommonDesk$EnumDeskEvent;

    const-string v1, "EV_PVR_NOTIFY_OVER_RUN"

    const/16 v2, 0x19

    invoke-direct {v0, v1, v2}, Lcom/konka/kkinterface/tv/CommonDesk$EnumDeskEvent;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/konka/kkinterface/tv/CommonDesk$EnumDeskEvent;->EV_PVR_NOTIFY_OVER_RUN:Lcom/konka/kkinterface/tv/CommonDesk$EnumDeskEvent;

    new-instance v0, Lcom/konka/kkinterface/tv/CommonDesk$EnumDeskEvent;

    const-string v1, "EV_PVR_NOTIFY_USB_REMOVED"

    const/16 v2, 0x1a

    invoke-direct {v0, v1, v2}, Lcom/konka/kkinterface/tv/CommonDesk$EnumDeskEvent;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/konka/kkinterface/tv/CommonDesk$EnumDeskEvent;->EV_PVR_NOTIFY_USB_REMOVED:Lcom/konka/kkinterface/tv/CommonDesk$EnumDeskEvent;

    new-instance v0, Lcom/konka/kkinterface/tv/CommonDesk$EnumDeskEvent;

    const-string v1, "EV_PVR_NOTIFY_CI_PLUS_PROTECTION"

    const/16 v2, 0x1b

    invoke-direct {v0, v1, v2}, Lcom/konka/kkinterface/tv/CommonDesk$EnumDeskEvent;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/konka/kkinterface/tv/CommonDesk$EnumDeskEvent;->EV_PVR_NOTIFY_CI_PLUS_PROTECTION:Lcom/konka/kkinterface/tv/CommonDesk$EnumDeskEvent;

    new-instance v0, Lcom/konka/kkinterface/tv/CommonDesk$EnumDeskEvent;

    const-string v1, "EV_PVR_NOTIFY_PARENTAL_CONTROL"

    const/16 v2, 0x1c

    invoke-direct {v0, v1, v2}, Lcom/konka/kkinterface/tv/CommonDesk$EnumDeskEvent;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/konka/kkinterface/tv/CommonDesk$EnumDeskEvent;->EV_PVR_NOTIFY_PARENTAL_CONTROL:Lcom/konka/kkinterface/tv/CommonDesk$EnumDeskEvent;

    new-instance v0, Lcom/konka/kkinterface/tv/CommonDesk$EnumDeskEvent;

    const-string v1, "EV_PVR_NOTIFY_ALWAYS_TIMESHIFT_PROGRAM_READY"

    const/16 v2, 0x1d

    invoke-direct {v0, v1, v2}, Lcom/konka/kkinterface/tv/CommonDesk$EnumDeskEvent;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/konka/kkinterface/tv/CommonDesk$EnumDeskEvent;->EV_PVR_NOTIFY_ALWAYS_TIMESHIFT_PROGRAM_READY:Lcom/konka/kkinterface/tv/CommonDesk$EnumDeskEvent;

    new-instance v0, Lcom/konka/kkinterface/tv/CommonDesk$EnumDeskEvent;

    const-string v1, "EV_PVR_NOTIFY_ALWAYS_TIMESHIFT_PROGRAM_NOTREADY"

    const/16 v2, 0x1e

    invoke-direct {v0, v1, v2}, Lcom/konka/kkinterface/tv/CommonDesk$EnumDeskEvent;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/konka/kkinterface/tv/CommonDesk$EnumDeskEvent;->EV_PVR_NOTIFY_ALWAYS_TIMESHIFT_PROGRAM_NOTREADY:Lcom/konka/kkinterface/tv/CommonDesk$EnumDeskEvent;

    new-instance v0, Lcom/konka/kkinterface/tv/CommonDesk$EnumDeskEvent;

    const-string v1, "EV_PVR_NOTIFY_CI_PLUS_RETENTION_LIMIT_UPDATE"

    const/16 v2, 0x1f

    invoke-direct {v0, v1, v2}, Lcom/konka/kkinterface/tv/CommonDesk$EnumDeskEvent;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/konka/kkinterface/tv/CommonDesk$EnumDeskEvent;->EV_PVR_NOTIFY_CI_PLUS_RETENTION_LIMIT_UPDATE:Lcom/konka/kkinterface/tv/CommonDesk$EnumDeskEvent;

    new-instance v0, Lcom/konka/kkinterface/tv/CommonDesk$EnumDeskEvent;

    const-string v1, "EV_DTV_AUTO_UPDATE_SCAN"

    const/16 v2, 0x20

    invoke-direct {v0, v1, v2}, Lcom/konka/kkinterface/tv/CommonDesk$EnumDeskEvent;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/konka/kkinterface/tv/CommonDesk$EnumDeskEvent;->EV_DTV_AUTO_UPDATE_SCAN:Lcom/konka/kkinterface/tv/CommonDesk$EnumDeskEvent;

    new-instance v0, Lcom/konka/kkinterface/tv/CommonDesk$EnumDeskEvent;

    const-string v1, "EV_TS_CHANGE"

    const/16 v2, 0x21

    invoke-direct {v0, v1, v2}, Lcom/konka/kkinterface/tv/CommonDesk$EnumDeskEvent;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/konka/kkinterface/tv/CommonDesk$EnumDeskEvent;->EV_TS_CHANGE:Lcom/konka/kkinterface/tv/CommonDesk$EnumDeskEvent;

    new-instance v0, Lcom/konka/kkinterface/tv/CommonDesk$EnumDeskEvent;

    const-string v1, "EV_POPUP_SCAN_DIALOGE_LOSS_SIGNAL"

    const/16 v2, 0x22

    invoke-direct {v0, v1, v2}, Lcom/konka/kkinterface/tv/CommonDesk$EnumDeskEvent;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/konka/kkinterface/tv/CommonDesk$EnumDeskEvent;->EV_POPUP_SCAN_DIALOGE_LOSS_SIGNAL:Lcom/konka/kkinterface/tv/CommonDesk$EnumDeskEvent;

    new-instance v0, Lcom/konka/kkinterface/tv/CommonDesk$EnumDeskEvent;

    const-string v1, "EV_POPUP_SCAN_DIALOGE_NEW_MULTIPLEX"

    const/16 v2, 0x23

    invoke-direct {v0, v1, v2}, Lcom/konka/kkinterface/tv/CommonDesk$EnumDeskEvent;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/konka/kkinterface/tv/CommonDesk$EnumDeskEvent;->EV_POPUP_SCAN_DIALOGE_NEW_MULTIPLEX:Lcom/konka/kkinterface/tv/CommonDesk$EnumDeskEvent;

    new-instance v0, Lcom/konka/kkinterface/tv/CommonDesk$EnumDeskEvent;

    const-string v1, "EV_POPUP_SCAN_DIALOGE_FREQUENCY_CHANGE"

    const/16 v2, 0x24

    invoke-direct {v0, v1, v2}, Lcom/konka/kkinterface/tv/CommonDesk$EnumDeskEvent;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/konka/kkinterface/tv/CommonDesk$EnumDeskEvent;->EV_POPUP_SCAN_DIALOGE_FREQUENCY_CHANGE:Lcom/konka/kkinterface/tv/CommonDesk$EnumDeskEvent;

    new-instance v0, Lcom/konka/kkinterface/tv/CommonDesk$EnumDeskEvent;

    const-string v1, "EV_RCT_PRESENCE"

    const/16 v2, 0x25

    invoke-direct {v0, v1, v2}, Lcom/konka/kkinterface/tv/CommonDesk$EnumDeskEvent;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/konka/kkinterface/tv/CommonDesk$EnumDeskEvent;->EV_RCT_PRESENCE:Lcom/konka/kkinterface/tv/CommonDesk$EnumDeskEvent;

    new-instance v0, Lcom/konka/kkinterface/tv/CommonDesk$EnumDeskEvent;

    const-string v1, "EV_CHANGE_TTX_STATUS"

    const/16 v2, 0x26

    invoke-direct {v0, v1, v2}, Lcom/konka/kkinterface/tv/CommonDesk$EnumDeskEvent;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/konka/kkinterface/tv/CommonDesk$EnumDeskEvent;->EV_CHANGE_TTX_STATUS:Lcom/konka/kkinterface/tv/CommonDesk$EnumDeskEvent;

    new-instance v0, Lcom/konka/kkinterface/tv/CommonDesk$EnumDeskEvent;

    const-string v1, "EV_DTV_PRI_COMPONENT_MISSING"

    const/16 v2, 0x27

    invoke-direct {v0, v1, v2}, Lcom/konka/kkinterface/tv/CommonDesk$EnumDeskEvent;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/konka/kkinterface/tv/CommonDesk$EnumDeskEvent;->EV_DTV_PRI_COMPONENT_MISSING:Lcom/konka/kkinterface/tv/CommonDesk$EnumDeskEvent;

    new-instance v0, Lcom/konka/kkinterface/tv/CommonDesk$EnumDeskEvent;

    const-string v1, "EV_AUDIO_MODE_CHANGE"

    const/16 v2, 0x28

    invoke-direct {v0, v1, v2}, Lcom/konka/kkinterface/tv/CommonDesk$EnumDeskEvent;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/konka/kkinterface/tv/CommonDesk$EnumDeskEvent;->EV_AUDIO_MODE_CHANGE:Lcom/konka/kkinterface/tv/CommonDesk$EnumDeskEvent;

    new-instance v0, Lcom/konka/kkinterface/tv/CommonDesk$EnumDeskEvent;

    const-string v1, "EV_MHEG5_EVENT_HANDLER"

    const/16 v2, 0x29

    invoke-direct {v0, v1, v2}, Lcom/konka/kkinterface/tv/CommonDesk$EnumDeskEvent;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/konka/kkinterface/tv/CommonDesk$EnumDeskEvent;->EV_MHEG5_EVENT_HANDLER:Lcom/konka/kkinterface/tv/CommonDesk$EnumDeskEvent;

    new-instance v0, Lcom/konka/kkinterface/tv/CommonDesk$EnumDeskEvent;

    const-string v1, "EV_OAD_TIMEOUT"

    const/16 v2, 0x2a

    invoke-direct {v0, v1, v2}, Lcom/konka/kkinterface/tv/CommonDesk$EnumDeskEvent;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/konka/kkinterface/tv/CommonDesk$EnumDeskEvent;->EV_OAD_TIMEOUT:Lcom/konka/kkinterface/tv/CommonDesk$EnumDeskEvent;

    new-instance v0, Lcom/konka/kkinterface/tv/CommonDesk$EnumDeskEvent;

    const-string v1, "EV_GINGA_STATUS_MODE"

    const/16 v2, 0x2b

    invoke-direct {v0, v1, v2}, Lcom/konka/kkinterface/tv/CommonDesk$EnumDeskEvent;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/konka/kkinterface/tv/CommonDesk$EnumDeskEvent;->EV_GINGA_STATUS_MODE:Lcom/konka/kkinterface/tv/CommonDesk$EnumDeskEvent;

    new-instance v0, Lcom/konka/kkinterface/tv/CommonDesk$EnumDeskEvent;

    const-string v1, "EV_HBBTV_UI_EVENT"

    const/16 v2, 0x2c

    invoke-direct {v0, v1, v2}, Lcom/konka/kkinterface/tv/CommonDesk$EnumDeskEvent;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/konka/kkinterface/tv/CommonDesk$EnumDeskEvent;->EV_HBBTV_UI_EVENT:Lcom/konka/kkinterface/tv/CommonDesk$EnumDeskEvent;

    new-instance v0, Lcom/konka/kkinterface/tv/CommonDesk$EnumDeskEvent;

    const-string v1, "EV_ATV_PROGRAM_INFO_READY"

    const/16 v2, 0x2d

    invoke-direct {v0, v1, v2}, Lcom/konka/kkinterface/tv/CommonDesk$EnumDeskEvent;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/konka/kkinterface/tv/CommonDesk$EnumDeskEvent;->EV_ATV_PROGRAM_INFO_READY:Lcom/konka/kkinterface/tv/CommonDesk$EnumDeskEvent;

    const/16 v0, 0x2e

    new-array v0, v0, [Lcom/konka/kkinterface/tv/CommonDesk$EnumDeskEvent;

    sget-object v1, Lcom/konka/kkinterface/tv/CommonDesk$EnumDeskEvent;->EV_DTV_CHANNELNAME_READY:Lcom/konka/kkinterface/tv/CommonDesk$EnumDeskEvent;

    aput-object v1, v0, v3

    sget-object v1, Lcom/konka/kkinterface/tv/CommonDesk$EnumDeskEvent;->EV_ATV_AUTO_TUNING_SCAN_INFO:Lcom/konka/kkinterface/tv/CommonDesk$EnumDeskEvent;

    aput-object v1, v0, v4

    sget-object v1, Lcom/konka/kkinterface/tv/CommonDesk$EnumDeskEvent;->EV_ATV_MANUAL_TUNING_SCAN_INFO:Lcom/konka/kkinterface/tv/CommonDesk$EnumDeskEvent;

    aput-object v1, v0, v5

    sget-object v1, Lcom/konka/kkinterface/tv/CommonDesk$EnumDeskEvent;->EV_DTV_AUTO_TUNING_SCAN_INFO:Lcom/konka/kkinterface/tv/CommonDesk$EnumDeskEvent;

    aput-object v1, v0, v6

    sget-object v1, Lcom/konka/kkinterface/tv/CommonDesk$EnumDeskEvent;->EV_DTV_PROGRAM_INFO_READY:Lcom/konka/kkinterface/tv/CommonDesk$EnumDeskEvent;

    aput-object v1, v0, v7

    const/4 v1, 0x5

    sget-object v2, Lcom/konka/kkinterface/tv/CommonDesk$EnumDeskEvent;->EV_SIGNAL_LOCK:Lcom/konka/kkinterface/tv/CommonDesk$EnumDeskEvent;

    aput-object v2, v0, v1

    const/4 v1, 0x6

    sget-object v2, Lcom/konka/kkinterface/tv/CommonDesk$EnumDeskEvent;->EV_SIGNAL_UNLOCK:Lcom/konka/kkinterface/tv/CommonDesk$EnumDeskEvent;

    aput-object v2, v0, v1

    const/4 v1, 0x7

    sget-object v2, Lcom/konka/kkinterface/tv/CommonDesk$EnumDeskEvent;->EV_POPUP_DIALOG:Lcom/konka/kkinterface/tv/CommonDesk$EnumDeskEvent;

    aput-object v2, v0, v1

    const/16 v1, 0x8

    sget-object v2, Lcom/konka/kkinterface/tv/CommonDesk$EnumDeskEvent;->EV_SCREEN_SAVER_MODE:Lcom/konka/kkinterface/tv/CommonDesk$EnumDeskEvent;

    aput-object v2, v0, v1

    const/16 v1, 0x9

    sget-object v2, Lcom/konka/kkinterface/tv/CommonDesk$EnumDeskEvent;->EV_CI_LOAD_CREDENTIAL_FAIL:Lcom/konka/kkinterface/tv/CommonDesk$EnumDeskEvent;

    aput-object v2, v0, v1

    const/16 v1, 0xa

    sget-object v2, Lcom/konka/kkinterface/tv/CommonDesk$EnumDeskEvent;->EV_EPGTIMER_SIMULCAST:Lcom/konka/kkinterface/tv/CommonDesk$EnumDeskEvent;

    aput-object v2, v0, v1

    const/16 v1, 0xb

    sget-object v2, Lcom/konka/kkinterface/tv/CommonDesk$EnumDeskEvent;->EV_HBBTV_STATUS_MODE:Lcom/konka/kkinterface/tv/CommonDesk$EnumDeskEvent;

    aput-object v2, v0, v1

    const/16 v1, 0xc

    sget-object v2, Lcom/konka/kkinterface/tv/CommonDesk$EnumDeskEvent;->EV_MHEG5_STATUS_MODE:Lcom/konka/kkinterface/tv/CommonDesk$EnumDeskEvent;

    aput-object v2, v0, v1

    const/16 v1, 0xd

    sget-object v2, Lcom/konka/kkinterface/tv/CommonDesk$EnumDeskEvent;->EV_MHEG5_RETURN_KEY:Lcom/konka/kkinterface/tv/CommonDesk$EnumDeskEvent;

    aput-object v2, v0, v1

    const/16 v1, 0xe

    sget-object v2, Lcom/konka/kkinterface/tv/CommonDesk$EnumDeskEvent;->EV_OAD_HANDLER:Lcom/konka/kkinterface/tv/CommonDesk$EnumDeskEvent;

    aput-object v2, v0, v1

    const/16 v1, 0xf

    sget-object v2, Lcom/konka/kkinterface/tv/CommonDesk$EnumDeskEvent;->EV_OAD_DOWNLOAD:Lcom/konka/kkinterface/tv/CommonDesk$EnumDeskEvent;

    aput-object v2, v0, v1

    const/16 v1, 0x10

    sget-object v2, Lcom/konka/kkinterface/tv/CommonDesk$EnumDeskEvent;->EV_PVR_NOTIFY_PLAYBACK_TIME:Lcom/konka/kkinterface/tv/CommonDesk$EnumDeskEvent;

    aput-object v2, v0, v1

    const/16 v1, 0x11

    sget-object v2, Lcom/konka/kkinterface/tv/CommonDesk$EnumDeskEvent;->EV_PVR_NOTIFY_PLAYBACK_SPEED_CHANGE:Lcom/konka/kkinterface/tv/CommonDesk$EnumDeskEvent;

    aput-object v2, v0, v1

    const/16 v1, 0x12

    sget-object v2, Lcom/konka/kkinterface/tv/CommonDesk$EnumDeskEvent;->EV_PVR_NOTIFY_RECORD_TIME:Lcom/konka/kkinterface/tv/CommonDesk$EnumDeskEvent;

    aput-object v2, v0, v1

    const/16 v1, 0x13

    sget-object v2, Lcom/konka/kkinterface/tv/CommonDesk$EnumDeskEvent;->EV_PVR_NOTIFY_RECORD_SIZE:Lcom/konka/kkinterface/tv/CommonDesk$EnumDeskEvent;

    aput-object v2, v0, v1

    const/16 v1, 0x14

    sget-object v2, Lcom/konka/kkinterface/tv/CommonDesk$EnumDeskEvent;->EV_PVR_NOTIFY_RECORD_STOP:Lcom/konka/kkinterface/tv/CommonDesk$EnumDeskEvent;

    aput-object v2, v0, v1

    const/16 v1, 0x15

    sget-object v2, Lcom/konka/kkinterface/tv/CommonDesk$EnumDeskEvent;->EV_PVR_NOTIFY_PLAYBACK_STOP:Lcom/konka/kkinterface/tv/CommonDesk$EnumDeskEvent;

    aput-object v2, v0, v1

    const/16 v1, 0x16

    sget-object v2, Lcom/konka/kkinterface/tv/CommonDesk$EnumDeskEvent;->EV_PVR_NOTIFY_PLAYBACK_BEGIN:Lcom/konka/kkinterface/tv/CommonDesk$EnumDeskEvent;

    aput-object v2, v0, v1

    const/16 v1, 0x17

    sget-object v2, Lcom/konka/kkinterface/tv/CommonDesk$EnumDeskEvent;->EV_PVR_NOTIFY_TIMESHIFT_OVERWRITES_BEFORE:Lcom/konka/kkinterface/tv/CommonDesk$EnumDeskEvent;

    aput-object v2, v0, v1

    const/16 v1, 0x18

    sget-object v2, Lcom/konka/kkinterface/tv/CommonDesk$EnumDeskEvent;->EV_PVR_NOTIFY_TIMESHIFT_OVERWRITES_AFTER:Lcom/konka/kkinterface/tv/CommonDesk$EnumDeskEvent;

    aput-object v2, v0, v1

    const/16 v1, 0x19

    sget-object v2, Lcom/konka/kkinterface/tv/CommonDesk$EnumDeskEvent;->EV_PVR_NOTIFY_OVER_RUN:Lcom/konka/kkinterface/tv/CommonDesk$EnumDeskEvent;

    aput-object v2, v0, v1

    const/16 v1, 0x1a

    sget-object v2, Lcom/konka/kkinterface/tv/CommonDesk$EnumDeskEvent;->EV_PVR_NOTIFY_USB_REMOVED:Lcom/konka/kkinterface/tv/CommonDesk$EnumDeskEvent;

    aput-object v2, v0, v1

    const/16 v1, 0x1b

    sget-object v2, Lcom/konka/kkinterface/tv/CommonDesk$EnumDeskEvent;->EV_PVR_NOTIFY_CI_PLUS_PROTECTION:Lcom/konka/kkinterface/tv/CommonDesk$EnumDeskEvent;

    aput-object v2, v0, v1

    const/16 v1, 0x1c

    sget-object v2, Lcom/konka/kkinterface/tv/CommonDesk$EnumDeskEvent;->EV_PVR_NOTIFY_PARENTAL_CONTROL:Lcom/konka/kkinterface/tv/CommonDesk$EnumDeskEvent;

    aput-object v2, v0, v1

    const/16 v1, 0x1d

    sget-object v2, Lcom/konka/kkinterface/tv/CommonDesk$EnumDeskEvent;->EV_PVR_NOTIFY_ALWAYS_TIMESHIFT_PROGRAM_READY:Lcom/konka/kkinterface/tv/CommonDesk$EnumDeskEvent;

    aput-object v2, v0, v1

    const/16 v1, 0x1e

    sget-object v2, Lcom/konka/kkinterface/tv/CommonDesk$EnumDeskEvent;->EV_PVR_NOTIFY_ALWAYS_TIMESHIFT_PROGRAM_NOTREADY:Lcom/konka/kkinterface/tv/CommonDesk$EnumDeskEvent;

    aput-object v2, v0, v1

    const/16 v1, 0x1f

    sget-object v2, Lcom/konka/kkinterface/tv/CommonDesk$EnumDeskEvent;->EV_PVR_NOTIFY_CI_PLUS_RETENTION_LIMIT_UPDATE:Lcom/konka/kkinterface/tv/CommonDesk$EnumDeskEvent;

    aput-object v2, v0, v1

    const/16 v1, 0x20

    sget-object v2, Lcom/konka/kkinterface/tv/CommonDesk$EnumDeskEvent;->EV_DTV_AUTO_UPDATE_SCAN:Lcom/konka/kkinterface/tv/CommonDesk$EnumDeskEvent;

    aput-object v2, v0, v1

    const/16 v1, 0x21

    sget-object v2, Lcom/konka/kkinterface/tv/CommonDesk$EnumDeskEvent;->EV_TS_CHANGE:Lcom/konka/kkinterface/tv/CommonDesk$EnumDeskEvent;

    aput-object v2, v0, v1

    const/16 v1, 0x22

    sget-object v2, Lcom/konka/kkinterface/tv/CommonDesk$EnumDeskEvent;->EV_POPUP_SCAN_DIALOGE_LOSS_SIGNAL:Lcom/konka/kkinterface/tv/CommonDesk$EnumDeskEvent;

    aput-object v2, v0, v1

    const/16 v1, 0x23

    sget-object v2, Lcom/konka/kkinterface/tv/CommonDesk$EnumDeskEvent;->EV_POPUP_SCAN_DIALOGE_NEW_MULTIPLEX:Lcom/konka/kkinterface/tv/CommonDesk$EnumDeskEvent;

    aput-object v2, v0, v1

    const/16 v1, 0x24

    sget-object v2, Lcom/konka/kkinterface/tv/CommonDesk$EnumDeskEvent;->EV_POPUP_SCAN_DIALOGE_FREQUENCY_CHANGE:Lcom/konka/kkinterface/tv/CommonDesk$EnumDeskEvent;

    aput-object v2, v0, v1

    const/16 v1, 0x25

    sget-object v2, Lcom/konka/kkinterface/tv/CommonDesk$EnumDeskEvent;->EV_RCT_PRESENCE:Lcom/konka/kkinterface/tv/CommonDesk$EnumDeskEvent;

    aput-object v2, v0, v1

    const/16 v1, 0x26

    sget-object v2, Lcom/konka/kkinterface/tv/CommonDesk$EnumDeskEvent;->EV_CHANGE_TTX_STATUS:Lcom/konka/kkinterface/tv/CommonDesk$EnumDeskEvent;

    aput-object v2, v0, v1

    const/16 v1, 0x27

    sget-object v2, Lcom/konka/kkinterface/tv/CommonDesk$EnumDeskEvent;->EV_DTV_PRI_COMPONENT_MISSING:Lcom/konka/kkinterface/tv/CommonDesk$EnumDeskEvent;

    aput-object v2, v0, v1

    const/16 v1, 0x28

    sget-object v2, Lcom/konka/kkinterface/tv/CommonDesk$EnumDeskEvent;->EV_AUDIO_MODE_CHANGE:Lcom/konka/kkinterface/tv/CommonDesk$EnumDeskEvent;

    aput-object v2, v0, v1

    const/16 v1, 0x29

    sget-object v2, Lcom/konka/kkinterface/tv/CommonDesk$EnumDeskEvent;->EV_MHEG5_EVENT_HANDLER:Lcom/konka/kkinterface/tv/CommonDesk$EnumDeskEvent;

    aput-object v2, v0, v1

    const/16 v1, 0x2a

    sget-object v2, Lcom/konka/kkinterface/tv/CommonDesk$EnumDeskEvent;->EV_OAD_TIMEOUT:Lcom/konka/kkinterface/tv/CommonDesk$EnumDeskEvent;

    aput-object v2, v0, v1

    const/16 v1, 0x2b

    sget-object v2, Lcom/konka/kkinterface/tv/CommonDesk$EnumDeskEvent;->EV_GINGA_STATUS_MODE:Lcom/konka/kkinterface/tv/CommonDesk$EnumDeskEvent;

    aput-object v2, v0, v1

    const/16 v1, 0x2c

    sget-object v2, Lcom/konka/kkinterface/tv/CommonDesk$EnumDeskEvent;->EV_HBBTV_UI_EVENT:Lcom/konka/kkinterface/tv/CommonDesk$EnumDeskEvent;

    aput-object v2, v0, v1

    const/16 v1, 0x2d

    sget-object v2, Lcom/konka/kkinterface/tv/CommonDesk$EnumDeskEvent;->EV_ATV_PROGRAM_INFO_READY:Lcom/konka/kkinterface/tv/CommonDesk$EnumDeskEvent;

    aput-object v2, v0, v1

    sput-object v0, Lcom/konka/kkinterface/tv/CommonDesk$EnumDeskEvent;->ENUM$VALUES:[Lcom/konka/kkinterface/tv/CommonDesk$EnumDeskEvent;

    return-void
.end method

.method private constructor <init>(Ljava/lang/String;I)V
    .locals 0

    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    return-void
.end method

.method public static valueOf(Ljava/lang/String;)Lcom/konka/kkinterface/tv/CommonDesk$EnumDeskEvent;
    .locals 1

    const-class v0, Lcom/konka/kkinterface/tv/CommonDesk$EnumDeskEvent;

    invoke-static {v0, p0}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;

    move-result-object v0

    check-cast v0, Lcom/konka/kkinterface/tv/CommonDesk$EnumDeskEvent;

    return-object v0
.end method

.method public static values()[Lcom/konka/kkinterface/tv/CommonDesk$EnumDeskEvent;
    .locals 4

    const/4 v3, 0x0

    sget-object v0, Lcom/konka/kkinterface/tv/CommonDesk$EnumDeskEvent;->ENUM$VALUES:[Lcom/konka/kkinterface/tv/CommonDesk$EnumDeskEvent;

    array-length v1, v0

    new-array v2, v1, [Lcom/konka/kkinterface/tv/CommonDesk$EnumDeskEvent;

    invoke-static {v0, v3, v2, v3, v1}, Ljava/lang/System;->arraycopy(Ljava/lang/Object;ILjava/lang/Object;II)V

    return-object v2
.end method
