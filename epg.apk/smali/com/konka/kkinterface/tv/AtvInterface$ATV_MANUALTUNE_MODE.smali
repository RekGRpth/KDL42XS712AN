.class public final enum Lcom/konka/kkinterface/tv/AtvInterface$ATV_MANUALTUNE_MODE;
.super Ljava/lang/Enum;
.source "AtvInterface.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/konka/kkinterface/tv/AtvInterface;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x4019
    name = "ATV_MANUALTUNE_MODE"
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Enum",
        "<",
        "Lcom/konka/kkinterface/tv/AtvInterface$ATV_MANUALTUNE_MODE;",
        ">;"
    }
.end annotation


# static fields
.field private static final synthetic ENUM$VALUES:[Lcom/konka/kkinterface/tv/AtvInterface$ATV_MANUALTUNE_MODE;

.field public static final enum E_MANUAL_TUNE_MODE_FINE_TUNE_DOWN:Lcom/konka/kkinterface/tv/AtvInterface$ATV_MANUALTUNE_MODE;

.field public static final enum E_MANUAL_TUNE_MODE_FINE_TUNE_ONE_FREQUENCY:Lcom/konka/kkinterface/tv/AtvInterface$ATV_MANUALTUNE_MODE;

.field public static final enum E_MANUAL_TUNE_MODE_FINE_TUNE_UP:Lcom/konka/kkinterface/tv/AtvInterface$ATV_MANUALTUNE_MODE;

.field public static final enum E_MANUAL_TUNE_MODE_SEARCH_ONE_TO_DOWN:Lcom/konka/kkinterface/tv/AtvInterface$ATV_MANUALTUNE_MODE;

.field public static final enum E_MANUAL_TUNE_MODE_SEARCH_ONE_TO_UP:Lcom/konka/kkinterface/tv/AtvInterface$ATV_MANUALTUNE_MODE;

.field public static final enum E_MANUAL_TUNE_MODE_UNDEFINE:Lcom/konka/kkinterface/tv/AtvInterface$ATV_MANUALTUNE_MODE;


# direct methods
.method static constructor <clinit>()V
    .locals 8

    const/4 v7, 0x4

    const/4 v6, 0x3

    const/4 v5, 0x2

    const/4 v4, 0x1

    const/4 v3, 0x0

    new-instance v0, Lcom/konka/kkinterface/tv/AtvInterface$ATV_MANUALTUNE_MODE;

    const-string v1, "E_MANUAL_TUNE_MODE_SEARCH_ONE_TO_UP"

    invoke-direct {v0, v1, v3}, Lcom/konka/kkinterface/tv/AtvInterface$ATV_MANUALTUNE_MODE;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/konka/kkinterface/tv/AtvInterface$ATV_MANUALTUNE_MODE;->E_MANUAL_TUNE_MODE_SEARCH_ONE_TO_UP:Lcom/konka/kkinterface/tv/AtvInterface$ATV_MANUALTUNE_MODE;

    new-instance v0, Lcom/konka/kkinterface/tv/AtvInterface$ATV_MANUALTUNE_MODE;

    const-string v1, "E_MANUAL_TUNE_MODE_SEARCH_ONE_TO_DOWN"

    invoke-direct {v0, v1, v4}, Lcom/konka/kkinterface/tv/AtvInterface$ATV_MANUALTUNE_MODE;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/konka/kkinterface/tv/AtvInterface$ATV_MANUALTUNE_MODE;->E_MANUAL_TUNE_MODE_SEARCH_ONE_TO_DOWN:Lcom/konka/kkinterface/tv/AtvInterface$ATV_MANUALTUNE_MODE;

    new-instance v0, Lcom/konka/kkinterface/tv/AtvInterface$ATV_MANUALTUNE_MODE;

    const-string v1, "E_MANUAL_TUNE_MODE_FINE_TUNE_ONE_FREQUENCY"

    invoke-direct {v0, v1, v5}, Lcom/konka/kkinterface/tv/AtvInterface$ATV_MANUALTUNE_MODE;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/konka/kkinterface/tv/AtvInterface$ATV_MANUALTUNE_MODE;->E_MANUAL_TUNE_MODE_FINE_TUNE_ONE_FREQUENCY:Lcom/konka/kkinterface/tv/AtvInterface$ATV_MANUALTUNE_MODE;

    new-instance v0, Lcom/konka/kkinterface/tv/AtvInterface$ATV_MANUALTUNE_MODE;

    const-string v1, "E_MANUAL_TUNE_MODE_FINE_TUNE_UP"

    invoke-direct {v0, v1, v6}, Lcom/konka/kkinterface/tv/AtvInterface$ATV_MANUALTUNE_MODE;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/konka/kkinterface/tv/AtvInterface$ATV_MANUALTUNE_MODE;->E_MANUAL_TUNE_MODE_FINE_TUNE_UP:Lcom/konka/kkinterface/tv/AtvInterface$ATV_MANUALTUNE_MODE;

    new-instance v0, Lcom/konka/kkinterface/tv/AtvInterface$ATV_MANUALTUNE_MODE;

    const-string v1, "E_MANUAL_TUNE_MODE_FINE_TUNE_DOWN"

    invoke-direct {v0, v1, v7}, Lcom/konka/kkinterface/tv/AtvInterface$ATV_MANUALTUNE_MODE;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/konka/kkinterface/tv/AtvInterface$ATV_MANUALTUNE_MODE;->E_MANUAL_TUNE_MODE_FINE_TUNE_DOWN:Lcom/konka/kkinterface/tv/AtvInterface$ATV_MANUALTUNE_MODE;

    new-instance v0, Lcom/konka/kkinterface/tv/AtvInterface$ATV_MANUALTUNE_MODE;

    const-string v1, "E_MANUAL_TUNE_MODE_UNDEFINE"

    const/4 v2, 0x5

    invoke-direct {v0, v1, v2}, Lcom/konka/kkinterface/tv/AtvInterface$ATV_MANUALTUNE_MODE;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/konka/kkinterface/tv/AtvInterface$ATV_MANUALTUNE_MODE;->E_MANUAL_TUNE_MODE_UNDEFINE:Lcom/konka/kkinterface/tv/AtvInterface$ATV_MANUALTUNE_MODE;

    const/4 v0, 0x6

    new-array v0, v0, [Lcom/konka/kkinterface/tv/AtvInterface$ATV_MANUALTUNE_MODE;

    sget-object v1, Lcom/konka/kkinterface/tv/AtvInterface$ATV_MANUALTUNE_MODE;->E_MANUAL_TUNE_MODE_SEARCH_ONE_TO_UP:Lcom/konka/kkinterface/tv/AtvInterface$ATV_MANUALTUNE_MODE;

    aput-object v1, v0, v3

    sget-object v1, Lcom/konka/kkinterface/tv/AtvInterface$ATV_MANUALTUNE_MODE;->E_MANUAL_TUNE_MODE_SEARCH_ONE_TO_DOWN:Lcom/konka/kkinterface/tv/AtvInterface$ATV_MANUALTUNE_MODE;

    aput-object v1, v0, v4

    sget-object v1, Lcom/konka/kkinterface/tv/AtvInterface$ATV_MANUALTUNE_MODE;->E_MANUAL_TUNE_MODE_FINE_TUNE_ONE_FREQUENCY:Lcom/konka/kkinterface/tv/AtvInterface$ATV_MANUALTUNE_MODE;

    aput-object v1, v0, v5

    sget-object v1, Lcom/konka/kkinterface/tv/AtvInterface$ATV_MANUALTUNE_MODE;->E_MANUAL_TUNE_MODE_FINE_TUNE_UP:Lcom/konka/kkinterface/tv/AtvInterface$ATV_MANUALTUNE_MODE;

    aput-object v1, v0, v6

    sget-object v1, Lcom/konka/kkinterface/tv/AtvInterface$ATV_MANUALTUNE_MODE;->E_MANUAL_TUNE_MODE_FINE_TUNE_DOWN:Lcom/konka/kkinterface/tv/AtvInterface$ATV_MANUALTUNE_MODE;

    aput-object v1, v0, v7

    const/4 v1, 0x5

    sget-object v2, Lcom/konka/kkinterface/tv/AtvInterface$ATV_MANUALTUNE_MODE;->E_MANUAL_TUNE_MODE_UNDEFINE:Lcom/konka/kkinterface/tv/AtvInterface$ATV_MANUALTUNE_MODE;

    aput-object v2, v0, v1

    sput-object v0, Lcom/konka/kkinterface/tv/AtvInterface$ATV_MANUALTUNE_MODE;->ENUM$VALUES:[Lcom/konka/kkinterface/tv/AtvInterface$ATV_MANUALTUNE_MODE;

    return-void
.end method

.method private constructor <init>(Ljava/lang/String;I)V
    .locals 0

    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    return-void
.end method

.method public static valueOf(Ljava/lang/String;)Lcom/konka/kkinterface/tv/AtvInterface$ATV_MANUALTUNE_MODE;
    .locals 1

    const-class v0, Lcom/konka/kkinterface/tv/AtvInterface$ATV_MANUALTUNE_MODE;

    invoke-static {v0, p0}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;

    move-result-object v0

    check-cast v0, Lcom/konka/kkinterface/tv/AtvInterface$ATV_MANUALTUNE_MODE;

    return-object v0
.end method

.method public static values()[Lcom/konka/kkinterface/tv/AtvInterface$ATV_MANUALTUNE_MODE;
    .locals 4

    const/4 v3, 0x0

    sget-object v0, Lcom/konka/kkinterface/tv/AtvInterface$ATV_MANUALTUNE_MODE;->ENUM$VALUES:[Lcom/konka/kkinterface/tv/AtvInterface$ATV_MANUALTUNE_MODE;

    array-length v1, v0

    new-array v2, v1, [Lcom/konka/kkinterface/tv/AtvInterface$ATV_MANUALTUNE_MODE;

    invoke-static {v0, v3, v2, v3, v1}, Ljava/lang/System;->arraycopy(Ljava/lang/Object;ILjava/lang/Object;II)V

    return-object v2
.end method
