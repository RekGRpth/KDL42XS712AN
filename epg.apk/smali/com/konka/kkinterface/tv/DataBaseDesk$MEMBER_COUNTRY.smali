.class public final enum Lcom/konka/kkinterface/tv/DataBaseDesk$MEMBER_COUNTRY;
.super Ljava/lang/Enum;
.source "DataBaseDesk.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/konka/kkinterface/tv/DataBaseDesk;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x4019
    name = "MEMBER_COUNTRY"
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Enum",
        "<",
        "Lcom/konka/kkinterface/tv/DataBaseDesk$MEMBER_COUNTRY;",
        ">;"
    }
.end annotation


# static fields
.field private static final synthetic ENUM$VALUES:[Lcom/konka/kkinterface/tv/DataBaseDesk$MEMBER_COUNTRY;

.field public static final enum E_ALGERIA:Lcom/konka/kkinterface/tv/DataBaseDesk$MEMBER_COUNTRY;

.field public static final enum E_ARAB:Lcom/konka/kkinterface/tv/DataBaseDesk$MEMBER_COUNTRY;

.field public static final enum E_ARGENTINA:Lcom/konka/kkinterface/tv/DataBaseDesk$MEMBER_COUNTRY;

.field public static final enum E_AUSTRALIA:Lcom/konka/kkinterface/tv/DataBaseDesk$MEMBER_COUNTRY;

.field public static final enum E_AUSTRIA:Lcom/konka/kkinterface/tv/DataBaseDesk$MEMBER_COUNTRY;

.field public static final enum E_AZERBAIJAN:Lcom/konka/kkinterface/tv/DataBaseDesk$MEMBER_COUNTRY;

.field public static final enum E_BELGIUM:Lcom/konka/kkinterface/tv/DataBaseDesk$MEMBER_COUNTRY;

.field public static final enum E_BELIZE:Lcom/konka/kkinterface/tv/DataBaseDesk$MEMBER_COUNTRY;

.field public static final enum E_BOLIVIA:Lcom/konka/kkinterface/tv/DataBaseDesk$MEMBER_COUNTRY;

.field public static final enum E_BRAZIL:Lcom/konka/kkinterface/tv/DataBaseDesk$MEMBER_COUNTRY;

.field public static final enum E_BULGARIA:Lcom/konka/kkinterface/tv/DataBaseDesk$MEMBER_COUNTRY;

.field public static final enum E_CANADA:Lcom/konka/kkinterface/tv/DataBaseDesk$MEMBER_COUNTRY;

.field public static final enum E_CHILE:Lcom/konka/kkinterface/tv/DataBaseDesk$MEMBER_COUNTRY;

.field public static final enum E_CHINA:Lcom/konka/kkinterface/tv/DataBaseDesk$MEMBER_COUNTRY;

.field public static final enum E_COSTARICA:Lcom/konka/kkinterface/tv/DataBaseDesk$MEMBER_COUNTRY;

.field public static final enum E_COUNTRY_NUM:Lcom/konka/kkinterface/tv/DataBaseDesk$MEMBER_COUNTRY;

.field public static final enum E_CROATIA:Lcom/konka/kkinterface/tv/DataBaseDesk$MEMBER_COUNTRY;

.field public static final enum E_CZECH:Lcom/konka/kkinterface/tv/DataBaseDesk$MEMBER_COUNTRY;

.field public static final enum E_DENMARK:Lcom/konka/kkinterface/tv/DataBaseDesk$MEMBER_COUNTRY;

.field public static final enum E_ECUADOR:Lcom/konka/kkinterface/tv/DataBaseDesk$MEMBER_COUNTRY;

.field public static final enum E_EGYPT:Lcom/konka/kkinterface/tv/DataBaseDesk$MEMBER_COUNTRY;

.field public static final enum E_ESTONIA:Lcom/konka/kkinterface/tv/DataBaseDesk$MEMBER_COUNTRY;

.field public static final enum E_ETHIOPIA:Lcom/konka/kkinterface/tv/DataBaseDesk$MEMBER_COUNTRY;

.field public static final enum E_FIJI:Lcom/konka/kkinterface/tv/DataBaseDesk$MEMBER_COUNTRY;

.field public static final enum E_FINLAND:Lcom/konka/kkinterface/tv/DataBaseDesk$MEMBER_COUNTRY;

.field public static final enum E_FRANCE:Lcom/konka/kkinterface/tv/DataBaseDesk$MEMBER_COUNTRY;

.field public static final enum E_GERMANY:Lcom/konka/kkinterface/tv/DataBaseDesk$MEMBER_COUNTRY;

.field public static final enum E_GREECE:Lcom/konka/kkinterface/tv/DataBaseDesk$MEMBER_COUNTRY;

.field public static final enum E_GUATEMALA:Lcom/konka/kkinterface/tv/DataBaseDesk$MEMBER_COUNTRY;

.field public static final enum E_HEBREW:Lcom/konka/kkinterface/tv/DataBaseDesk$MEMBER_COUNTRY;

.field public static final enum E_HUNGARY:Lcom/konka/kkinterface/tv/DataBaseDesk$MEMBER_COUNTRY;

.field public static final enum E_INDIA:Lcom/konka/kkinterface/tv/DataBaseDesk$MEMBER_COUNTRY;

.field public static final enum E_INDONESIA:Lcom/konka/kkinterface/tv/DataBaseDesk$MEMBER_COUNTRY;

.field public static final enum E_IRAN:Lcom/konka/kkinterface/tv/DataBaseDesk$MEMBER_COUNTRY;

.field public static final enum E_IRAQ:Lcom/konka/kkinterface/tv/DataBaseDesk$MEMBER_COUNTRY;

.field public static final enum E_IRELAND:Lcom/konka/kkinterface/tv/DataBaseDesk$MEMBER_COUNTRY;

.field public static final enum E_ISRAEL:Lcom/konka/kkinterface/tv/DataBaseDesk$MEMBER_COUNTRY;

.field public static final enum E_ITALY:Lcom/konka/kkinterface/tv/DataBaseDesk$MEMBER_COUNTRY;

.field public static final enum E_JAPAN:Lcom/konka/kkinterface/tv/DataBaseDesk$MEMBER_COUNTRY;

.field public static final enum E_JORDAN:Lcom/konka/kkinterface/tv/DataBaseDesk$MEMBER_COUNTRY;

.field public static final enum E_KUWAIT:Lcom/konka/kkinterface/tv/DataBaseDesk$MEMBER_COUNTRY;

.field public static final enum E_LATVIA:Lcom/konka/kkinterface/tv/DataBaseDesk$MEMBER_COUNTRY;

.field public static final enum E_LIBYA:Lcom/konka/kkinterface/tv/DataBaseDesk$MEMBER_COUNTRY;

.field public static final enum E_LITHUANIA:Lcom/konka/kkinterface/tv/DataBaseDesk$MEMBER_COUNTRY;

.field public static final enum E_LUXEMBOURG:Lcom/konka/kkinterface/tv/DataBaseDesk$MEMBER_COUNTRY;

.field public static final enum E_MALDIVES:Lcom/konka/kkinterface/tv/DataBaseDesk$MEMBER_COUNTRY;

.field public static final enum E_MEXICO:Lcom/konka/kkinterface/tv/DataBaseDesk$MEMBER_COUNTRY;

.field public static final enum E_MOROCCO:Lcom/konka/kkinterface/tv/DataBaseDesk$MEMBER_COUNTRY;

.field public static final enum E_NAMIBIA:Lcom/konka/kkinterface/tv/DataBaseDesk$MEMBER_COUNTRY;

.field public static final enum E_NETHERLANDS:Lcom/konka/kkinterface/tv/DataBaseDesk$MEMBER_COUNTRY;

.field public static final enum E_NEWZEALAND:Lcom/konka/kkinterface/tv/DataBaseDesk$MEMBER_COUNTRY;

.field public static final enum E_NICARAGUA:Lcom/konka/kkinterface/tv/DataBaseDesk$MEMBER_COUNTRY;

.field public static final enum E_NIGERIA:Lcom/konka/kkinterface/tv/DataBaseDesk$MEMBER_COUNTRY;

.field public static final enum E_NORWAY:Lcom/konka/kkinterface/tv/DataBaseDesk$MEMBER_COUNTRY;

.field public static final enum E_OTHERS:Lcom/konka/kkinterface/tv/DataBaseDesk$MEMBER_COUNTRY;

.field public static final enum E_PAKISTAN:Lcom/konka/kkinterface/tv/DataBaseDesk$MEMBER_COUNTRY;

.field public static final enum E_PARAGUAY:Lcom/konka/kkinterface/tv/DataBaseDesk$MEMBER_COUNTRY;

.field public static final enum E_PERU:Lcom/konka/kkinterface/tv/DataBaseDesk$MEMBER_COUNTRY;

.field public static final enum E_PHILIPPINES:Lcom/konka/kkinterface/tv/DataBaseDesk$MEMBER_COUNTRY;

.field public static final enum E_POLAND:Lcom/konka/kkinterface/tv/DataBaseDesk$MEMBER_COUNTRY;

.field public static final enum E_PORTUGAL:Lcom/konka/kkinterface/tv/DataBaseDesk$MEMBER_COUNTRY;

.field public static final enum E_QATAR:Lcom/konka/kkinterface/tv/DataBaseDesk$MEMBER_COUNTRY;

.field public static final enum E_RUMANIA:Lcom/konka/kkinterface/tv/DataBaseDesk$MEMBER_COUNTRY;

.field public static final enum E_RUSSIA:Lcom/konka/kkinterface/tv/DataBaseDesk$MEMBER_COUNTRY;

.field public static final enum E_SAUDI_ARABIA:Lcom/konka/kkinterface/tv/DataBaseDesk$MEMBER_COUNTRY;

.field public static final enum E_SERBIA:Lcom/konka/kkinterface/tv/DataBaseDesk$MEMBER_COUNTRY;

.field public static final enum E_SLOVAKIA:Lcom/konka/kkinterface/tv/DataBaseDesk$MEMBER_COUNTRY;

.field public static final enum E_SLOVENIA:Lcom/konka/kkinterface/tv/DataBaseDesk$MEMBER_COUNTRY;

.field public static final enum E_SOUTHAFRICA:Lcom/konka/kkinterface/tv/DataBaseDesk$MEMBER_COUNTRY;

.field public static final enum E_SOUTHKOREA:Lcom/konka/kkinterface/tv/DataBaseDesk$MEMBER_COUNTRY;

.field public static final enum E_SPAIN:Lcom/konka/kkinterface/tv/DataBaseDesk$MEMBER_COUNTRY;

.field public static final enum E_SWEDEN:Lcom/konka/kkinterface/tv/DataBaseDesk$MEMBER_COUNTRY;

.field public static final enum E_SWITZERLAND:Lcom/konka/kkinterface/tv/DataBaseDesk$MEMBER_COUNTRY;

.field public static final enum E_TAIWAN:Lcom/konka/kkinterface/tv/DataBaseDesk$MEMBER_COUNTRY;

.field public static final enum E_TAJIKISTAN:Lcom/konka/kkinterface/tv/DataBaseDesk$MEMBER_COUNTRY;

.field public static final enum E_THAILAND:Lcom/konka/kkinterface/tv/DataBaseDesk$MEMBER_COUNTRY;

.field public static final enum E_TUNIS:Lcom/konka/kkinterface/tv/DataBaseDesk$MEMBER_COUNTRY;

.field public static final enum E_TURKEY:Lcom/konka/kkinterface/tv/DataBaseDesk$MEMBER_COUNTRY;

.field public static final enum E_UK:Lcom/konka/kkinterface/tv/DataBaseDesk$MEMBER_COUNTRY;

.field public static final enum E_URUGUAY:Lcom/konka/kkinterface/tv/DataBaseDesk$MEMBER_COUNTRY;

.field public static final enum E_US:Lcom/konka/kkinterface/tv/DataBaseDesk$MEMBER_COUNTRY;

.field public static final enum E_UZBEK:Lcom/konka/kkinterface/tv/DataBaseDesk$MEMBER_COUNTRY;

.field public static final enum E_VENEZUELA:Lcom/konka/kkinterface/tv/DataBaseDesk$MEMBER_COUNTRY;

.field public static final enum E_YEMEN:Lcom/konka/kkinterface/tv/DataBaseDesk$MEMBER_COUNTRY;

.field public static final enum E_ZEMBABWE:Lcom/konka/kkinterface/tv/DataBaseDesk$MEMBER_COUNTRY;


# direct methods
.method static constructor <clinit>()V
    .locals 8

    const/4 v7, 0x4

    const/4 v6, 0x3

    const/4 v5, 0x2

    const/4 v4, 0x1

    const/4 v3, 0x0

    new-instance v0, Lcom/konka/kkinterface/tv/DataBaseDesk$MEMBER_COUNTRY;

    const-string v1, "E_AUSTRALIA"

    invoke-direct {v0, v1, v3}, Lcom/konka/kkinterface/tv/DataBaseDesk$MEMBER_COUNTRY;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/konka/kkinterface/tv/DataBaseDesk$MEMBER_COUNTRY;->E_AUSTRALIA:Lcom/konka/kkinterface/tv/DataBaseDesk$MEMBER_COUNTRY;

    new-instance v0, Lcom/konka/kkinterface/tv/DataBaseDesk$MEMBER_COUNTRY;

    const-string v1, "E_AUSTRIA"

    invoke-direct {v0, v1, v4}, Lcom/konka/kkinterface/tv/DataBaseDesk$MEMBER_COUNTRY;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/konka/kkinterface/tv/DataBaseDesk$MEMBER_COUNTRY;->E_AUSTRIA:Lcom/konka/kkinterface/tv/DataBaseDesk$MEMBER_COUNTRY;

    new-instance v0, Lcom/konka/kkinterface/tv/DataBaseDesk$MEMBER_COUNTRY;

    const-string v1, "E_BELGIUM"

    invoke-direct {v0, v1, v5}, Lcom/konka/kkinterface/tv/DataBaseDesk$MEMBER_COUNTRY;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/konka/kkinterface/tv/DataBaseDesk$MEMBER_COUNTRY;->E_BELGIUM:Lcom/konka/kkinterface/tv/DataBaseDesk$MEMBER_COUNTRY;

    new-instance v0, Lcom/konka/kkinterface/tv/DataBaseDesk$MEMBER_COUNTRY;

    const-string v1, "E_BULGARIA"

    invoke-direct {v0, v1, v6}, Lcom/konka/kkinterface/tv/DataBaseDesk$MEMBER_COUNTRY;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/konka/kkinterface/tv/DataBaseDesk$MEMBER_COUNTRY;->E_BULGARIA:Lcom/konka/kkinterface/tv/DataBaseDesk$MEMBER_COUNTRY;

    new-instance v0, Lcom/konka/kkinterface/tv/DataBaseDesk$MEMBER_COUNTRY;

    const-string v1, "E_CROATIA"

    invoke-direct {v0, v1, v7}, Lcom/konka/kkinterface/tv/DataBaseDesk$MEMBER_COUNTRY;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/konka/kkinterface/tv/DataBaseDesk$MEMBER_COUNTRY;->E_CROATIA:Lcom/konka/kkinterface/tv/DataBaseDesk$MEMBER_COUNTRY;

    new-instance v0, Lcom/konka/kkinterface/tv/DataBaseDesk$MEMBER_COUNTRY;

    const-string v1, "E_CZECH"

    const/4 v2, 0x5

    invoke-direct {v0, v1, v2}, Lcom/konka/kkinterface/tv/DataBaseDesk$MEMBER_COUNTRY;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/konka/kkinterface/tv/DataBaseDesk$MEMBER_COUNTRY;->E_CZECH:Lcom/konka/kkinterface/tv/DataBaseDesk$MEMBER_COUNTRY;

    new-instance v0, Lcom/konka/kkinterface/tv/DataBaseDesk$MEMBER_COUNTRY;

    const-string v1, "E_DENMARK"

    const/4 v2, 0x6

    invoke-direct {v0, v1, v2}, Lcom/konka/kkinterface/tv/DataBaseDesk$MEMBER_COUNTRY;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/konka/kkinterface/tv/DataBaseDesk$MEMBER_COUNTRY;->E_DENMARK:Lcom/konka/kkinterface/tv/DataBaseDesk$MEMBER_COUNTRY;

    new-instance v0, Lcom/konka/kkinterface/tv/DataBaseDesk$MEMBER_COUNTRY;

    const-string v1, "E_FINLAND"

    const/4 v2, 0x7

    invoke-direct {v0, v1, v2}, Lcom/konka/kkinterface/tv/DataBaseDesk$MEMBER_COUNTRY;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/konka/kkinterface/tv/DataBaseDesk$MEMBER_COUNTRY;->E_FINLAND:Lcom/konka/kkinterface/tv/DataBaseDesk$MEMBER_COUNTRY;

    new-instance v0, Lcom/konka/kkinterface/tv/DataBaseDesk$MEMBER_COUNTRY;

    const-string v1, "E_FRANCE"

    const/16 v2, 0x8

    invoke-direct {v0, v1, v2}, Lcom/konka/kkinterface/tv/DataBaseDesk$MEMBER_COUNTRY;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/konka/kkinterface/tv/DataBaseDesk$MEMBER_COUNTRY;->E_FRANCE:Lcom/konka/kkinterface/tv/DataBaseDesk$MEMBER_COUNTRY;

    new-instance v0, Lcom/konka/kkinterface/tv/DataBaseDesk$MEMBER_COUNTRY;

    const-string v1, "E_GERMANY"

    const/16 v2, 0x9

    invoke-direct {v0, v1, v2}, Lcom/konka/kkinterface/tv/DataBaseDesk$MEMBER_COUNTRY;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/konka/kkinterface/tv/DataBaseDesk$MEMBER_COUNTRY;->E_GERMANY:Lcom/konka/kkinterface/tv/DataBaseDesk$MEMBER_COUNTRY;

    new-instance v0, Lcom/konka/kkinterface/tv/DataBaseDesk$MEMBER_COUNTRY;

    const-string v1, "E_GREECE"

    const/16 v2, 0xa

    invoke-direct {v0, v1, v2}, Lcom/konka/kkinterface/tv/DataBaseDesk$MEMBER_COUNTRY;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/konka/kkinterface/tv/DataBaseDesk$MEMBER_COUNTRY;->E_GREECE:Lcom/konka/kkinterface/tv/DataBaseDesk$MEMBER_COUNTRY;

    new-instance v0, Lcom/konka/kkinterface/tv/DataBaseDesk$MEMBER_COUNTRY;

    const-string v1, "E_HUNGARY"

    const/16 v2, 0xb

    invoke-direct {v0, v1, v2}, Lcom/konka/kkinterface/tv/DataBaseDesk$MEMBER_COUNTRY;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/konka/kkinterface/tv/DataBaseDesk$MEMBER_COUNTRY;->E_HUNGARY:Lcom/konka/kkinterface/tv/DataBaseDesk$MEMBER_COUNTRY;

    new-instance v0, Lcom/konka/kkinterface/tv/DataBaseDesk$MEMBER_COUNTRY;

    const-string v1, "E_ITALY"

    const/16 v2, 0xc

    invoke-direct {v0, v1, v2}, Lcom/konka/kkinterface/tv/DataBaseDesk$MEMBER_COUNTRY;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/konka/kkinterface/tv/DataBaseDesk$MEMBER_COUNTRY;->E_ITALY:Lcom/konka/kkinterface/tv/DataBaseDesk$MEMBER_COUNTRY;

    new-instance v0, Lcom/konka/kkinterface/tv/DataBaseDesk$MEMBER_COUNTRY;

    const-string v1, "E_LUXEMBOURG"

    const/16 v2, 0xd

    invoke-direct {v0, v1, v2}, Lcom/konka/kkinterface/tv/DataBaseDesk$MEMBER_COUNTRY;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/konka/kkinterface/tv/DataBaseDesk$MEMBER_COUNTRY;->E_LUXEMBOURG:Lcom/konka/kkinterface/tv/DataBaseDesk$MEMBER_COUNTRY;

    new-instance v0, Lcom/konka/kkinterface/tv/DataBaseDesk$MEMBER_COUNTRY;

    const-string v1, "E_NETHERLANDS"

    const/16 v2, 0xe

    invoke-direct {v0, v1, v2}, Lcom/konka/kkinterface/tv/DataBaseDesk$MEMBER_COUNTRY;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/konka/kkinterface/tv/DataBaseDesk$MEMBER_COUNTRY;->E_NETHERLANDS:Lcom/konka/kkinterface/tv/DataBaseDesk$MEMBER_COUNTRY;

    new-instance v0, Lcom/konka/kkinterface/tv/DataBaseDesk$MEMBER_COUNTRY;

    const-string v1, "E_NORWAY"

    const/16 v2, 0xf

    invoke-direct {v0, v1, v2}, Lcom/konka/kkinterface/tv/DataBaseDesk$MEMBER_COUNTRY;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/konka/kkinterface/tv/DataBaseDesk$MEMBER_COUNTRY;->E_NORWAY:Lcom/konka/kkinterface/tv/DataBaseDesk$MEMBER_COUNTRY;

    new-instance v0, Lcom/konka/kkinterface/tv/DataBaseDesk$MEMBER_COUNTRY;

    const-string v1, "E_POLAND"

    const/16 v2, 0x10

    invoke-direct {v0, v1, v2}, Lcom/konka/kkinterface/tv/DataBaseDesk$MEMBER_COUNTRY;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/konka/kkinterface/tv/DataBaseDesk$MEMBER_COUNTRY;->E_POLAND:Lcom/konka/kkinterface/tv/DataBaseDesk$MEMBER_COUNTRY;

    new-instance v0, Lcom/konka/kkinterface/tv/DataBaseDesk$MEMBER_COUNTRY;

    const-string v1, "E_PORTUGAL"

    const/16 v2, 0x11

    invoke-direct {v0, v1, v2}, Lcom/konka/kkinterface/tv/DataBaseDesk$MEMBER_COUNTRY;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/konka/kkinterface/tv/DataBaseDesk$MEMBER_COUNTRY;->E_PORTUGAL:Lcom/konka/kkinterface/tv/DataBaseDesk$MEMBER_COUNTRY;

    new-instance v0, Lcom/konka/kkinterface/tv/DataBaseDesk$MEMBER_COUNTRY;

    const-string v1, "E_RUMANIA"

    const/16 v2, 0x12

    invoke-direct {v0, v1, v2}, Lcom/konka/kkinterface/tv/DataBaseDesk$MEMBER_COUNTRY;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/konka/kkinterface/tv/DataBaseDesk$MEMBER_COUNTRY;->E_RUMANIA:Lcom/konka/kkinterface/tv/DataBaseDesk$MEMBER_COUNTRY;

    new-instance v0, Lcom/konka/kkinterface/tv/DataBaseDesk$MEMBER_COUNTRY;

    const-string v1, "E_RUSSIA"

    const/16 v2, 0x13

    invoke-direct {v0, v1, v2}, Lcom/konka/kkinterface/tv/DataBaseDesk$MEMBER_COUNTRY;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/konka/kkinterface/tv/DataBaseDesk$MEMBER_COUNTRY;->E_RUSSIA:Lcom/konka/kkinterface/tv/DataBaseDesk$MEMBER_COUNTRY;

    new-instance v0, Lcom/konka/kkinterface/tv/DataBaseDesk$MEMBER_COUNTRY;

    const-string v1, "E_SERBIA"

    const/16 v2, 0x14

    invoke-direct {v0, v1, v2}, Lcom/konka/kkinterface/tv/DataBaseDesk$MEMBER_COUNTRY;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/konka/kkinterface/tv/DataBaseDesk$MEMBER_COUNTRY;->E_SERBIA:Lcom/konka/kkinterface/tv/DataBaseDesk$MEMBER_COUNTRY;

    new-instance v0, Lcom/konka/kkinterface/tv/DataBaseDesk$MEMBER_COUNTRY;

    const-string v1, "E_SLOVENIA"

    const/16 v2, 0x15

    invoke-direct {v0, v1, v2}, Lcom/konka/kkinterface/tv/DataBaseDesk$MEMBER_COUNTRY;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/konka/kkinterface/tv/DataBaseDesk$MEMBER_COUNTRY;->E_SLOVENIA:Lcom/konka/kkinterface/tv/DataBaseDesk$MEMBER_COUNTRY;

    new-instance v0, Lcom/konka/kkinterface/tv/DataBaseDesk$MEMBER_COUNTRY;

    const-string v1, "E_SPAIN"

    const/16 v2, 0x16

    invoke-direct {v0, v1, v2}, Lcom/konka/kkinterface/tv/DataBaseDesk$MEMBER_COUNTRY;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/konka/kkinterface/tv/DataBaseDesk$MEMBER_COUNTRY;->E_SPAIN:Lcom/konka/kkinterface/tv/DataBaseDesk$MEMBER_COUNTRY;

    new-instance v0, Lcom/konka/kkinterface/tv/DataBaseDesk$MEMBER_COUNTRY;

    const-string v1, "E_SWEDEN"

    const/16 v2, 0x17

    invoke-direct {v0, v1, v2}, Lcom/konka/kkinterface/tv/DataBaseDesk$MEMBER_COUNTRY;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/konka/kkinterface/tv/DataBaseDesk$MEMBER_COUNTRY;->E_SWEDEN:Lcom/konka/kkinterface/tv/DataBaseDesk$MEMBER_COUNTRY;

    new-instance v0, Lcom/konka/kkinterface/tv/DataBaseDesk$MEMBER_COUNTRY;

    const-string v1, "E_SWITZERLAND"

    const/16 v2, 0x18

    invoke-direct {v0, v1, v2}, Lcom/konka/kkinterface/tv/DataBaseDesk$MEMBER_COUNTRY;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/konka/kkinterface/tv/DataBaseDesk$MEMBER_COUNTRY;->E_SWITZERLAND:Lcom/konka/kkinterface/tv/DataBaseDesk$MEMBER_COUNTRY;

    new-instance v0, Lcom/konka/kkinterface/tv/DataBaseDesk$MEMBER_COUNTRY;

    const-string v1, "E_UK"

    const/16 v2, 0x19

    invoke-direct {v0, v1, v2}, Lcom/konka/kkinterface/tv/DataBaseDesk$MEMBER_COUNTRY;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/konka/kkinterface/tv/DataBaseDesk$MEMBER_COUNTRY;->E_UK:Lcom/konka/kkinterface/tv/DataBaseDesk$MEMBER_COUNTRY;

    new-instance v0, Lcom/konka/kkinterface/tv/DataBaseDesk$MEMBER_COUNTRY;

    const-string v1, "E_NEWZEALAND"

    const/16 v2, 0x1a

    invoke-direct {v0, v1, v2}, Lcom/konka/kkinterface/tv/DataBaseDesk$MEMBER_COUNTRY;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/konka/kkinterface/tv/DataBaseDesk$MEMBER_COUNTRY;->E_NEWZEALAND:Lcom/konka/kkinterface/tv/DataBaseDesk$MEMBER_COUNTRY;

    new-instance v0, Lcom/konka/kkinterface/tv/DataBaseDesk$MEMBER_COUNTRY;

    const-string v1, "E_ARAB"

    const/16 v2, 0x1b

    invoke-direct {v0, v1, v2}, Lcom/konka/kkinterface/tv/DataBaseDesk$MEMBER_COUNTRY;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/konka/kkinterface/tv/DataBaseDesk$MEMBER_COUNTRY;->E_ARAB:Lcom/konka/kkinterface/tv/DataBaseDesk$MEMBER_COUNTRY;

    new-instance v0, Lcom/konka/kkinterface/tv/DataBaseDesk$MEMBER_COUNTRY;

    const-string v1, "E_ESTONIA"

    const/16 v2, 0x1c

    invoke-direct {v0, v1, v2}, Lcom/konka/kkinterface/tv/DataBaseDesk$MEMBER_COUNTRY;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/konka/kkinterface/tv/DataBaseDesk$MEMBER_COUNTRY;->E_ESTONIA:Lcom/konka/kkinterface/tv/DataBaseDesk$MEMBER_COUNTRY;

    new-instance v0, Lcom/konka/kkinterface/tv/DataBaseDesk$MEMBER_COUNTRY;

    const-string v1, "E_HEBREW"

    const/16 v2, 0x1d

    invoke-direct {v0, v1, v2}, Lcom/konka/kkinterface/tv/DataBaseDesk$MEMBER_COUNTRY;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/konka/kkinterface/tv/DataBaseDesk$MEMBER_COUNTRY;->E_HEBREW:Lcom/konka/kkinterface/tv/DataBaseDesk$MEMBER_COUNTRY;

    new-instance v0, Lcom/konka/kkinterface/tv/DataBaseDesk$MEMBER_COUNTRY;

    const-string v1, "E_LATVIA"

    const/16 v2, 0x1e

    invoke-direct {v0, v1, v2}, Lcom/konka/kkinterface/tv/DataBaseDesk$MEMBER_COUNTRY;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/konka/kkinterface/tv/DataBaseDesk$MEMBER_COUNTRY;->E_LATVIA:Lcom/konka/kkinterface/tv/DataBaseDesk$MEMBER_COUNTRY;

    new-instance v0, Lcom/konka/kkinterface/tv/DataBaseDesk$MEMBER_COUNTRY;

    const-string v1, "E_SLOVAKIA"

    const/16 v2, 0x1f

    invoke-direct {v0, v1, v2}, Lcom/konka/kkinterface/tv/DataBaseDesk$MEMBER_COUNTRY;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/konka/kkinterface/tv/DataBaseDesk$MEMBER_COUNTRY;->E_SLOVAKIA:Lcom/konka/kkinterface/tv/DataBaseDesk$MEMBER_COUNTRY;

    new-instance v0, Lcom/konka/kkinterface/tv/DataBaseDesk$MEMBER_COUNTRY;

    const-string v1, "E_TURKEY"

    const/16 v2, 0x20

    invoke-direct {v0, v1, v2}, Lcom/konka/kkinterface/tv/DataBaseDesk$MEMBER_COUNTRY;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/konka/kkinterface/tv/DataBaseDesk$MEMBER_COUNTRY;->E_TURKEY:Lcom/konka/kkinterface/tv/DataBaseDesk$MEMBER_COUNTRY;

    new-instance v0, Lcom/konka/kkinterface/tv/DataBaseDesk$MEMBER_COUNTRY;

    const-string v1, "E_IRELAND"

    const/16 v2, 0x21

    invoke-direct {v0, v1, v2}, Lcom/konka/kkinterface/tv/DataBaseDesk$MEMBER_COUNTRY;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/konka/kkinterface/tv/DataBaseDesk$MEMBER_COUNTRY;->E_IRELAND:Lcom/konka/kkinterface/tv/DataBaseDesk$MEMBER_COUNTRY;

    new-instance v0, Lcom/konka/kkinterface/tv/DataBaseDesk$MEMBER_COUNTRY;

    const-string v1, "E_JAPAN"

    const/16 v2, 0x22

    invoke-direct {v0, v1, v2}, Lcom/konka/kkinterface/tv/DataBaseDesk$MEMBER_COUNTRY;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/konka/kkinterface/tv/DataBaseDesk$MEMBER_COUNTRY;->E_JAPAN:Lcom/konka/kkinterface/tv/DataBaseDesk$MEMBER_COUNTRY;

    new-instance v0, Lcom/konka/kkinterface/tv/DataBaseDesk$MEMBER_COUNTRY;

    const-string v1, "E_PHILIPPINES"

    const/16 v2, 0x23

    invoke-direct {v0, v1, v2}, Lcom/konka/kkinterface/tv/DataBaseDesk$MEMBER_COUNTRY;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/konka/kkinterface/tv/DataBaseDesk$MEMBER_COUNTRY;->E_PHILIPPINES:Lcom/konka/kkinterface/tv/DataBaseDesk$MEMBER_COUNTRY;

    new-instance v0, Lcom/konka/kkinterface/tv/DataBaseDesk$MEMBER_COUNTRY;

    const-string v1, "E_THAILAND"

    const/16 v2, 0x24

    invoke-direct {v0, v1, v2}, Lcom/konka/kkinterface/tv/DataBaseDesk$MEMBER_COUNTRY;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/konka/kkinterface/tv/DataBaseDesk$MEMBER_COUNTRY;->E_THAILAND:Lcom/konka/kkinterface/tv/DataBaseDesk$MEMBER_COUNTRY;

    new-instance v0, Lcom/konka/kkinterface/tv/DataBaseDesk$MEMBER_COUNTRY;

    const-string v1, "E_MALDIVES"

    const/16 v2, 0x25

    invoke-direct {v0, v1, v2}, Lcom/konka/kkinterface/tv/DataBaseDesk$MEMBER_COUNTRY;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/konka/kkinterface/tv/DataBaseDesk$MEMBER_COUNTRY;->E_MALDIVES:Lcom/konka/kkinterface/tv/DataBaseDesk$MEMBER_COUNTRY;

    new-instance v0, Lcom/konka/kkinterface/tv/DataBaseDesk$MEMBER_COUNTRY;

    const-string v1, "E_URUGUAY"

    const/16 v2, 0x26

    invoke-direct {v0, v1, v2}, Lcom/konka/kkinterface/tv/DataBaseDesk$MEMBER_COUNTRY;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/konka/kkinterface/tv/DataBaseDesk$MEMBER_COUNTRY;->E_URUGUAY:Lcom/konka/kkinterface/tv/DataBaseDesk$MEMBER_COUNTRY;

    new-instance v0, Lcom/konka/kkinterface/tv/DataBaseDesk$MEMBER_COUNTRY;

    const-string v1, "E_PERU"

    const/16 v2, 0x27

    invoke-direct {v0, v1, v2}, Lcom/konka/kkinterface/tv/DataBaseDesk$MEMBER_COUNTRY;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/konka/kkinterface/tv/DataBaseDesk$MEMBER_COUNTRY;->E_PERU:Lcom/konka/kkinterface/tv/DataBaseDesk$MEMBER_COUNTRY;

    new-instance v0, Lcom/konka/kkinterface/tv/DataBaseDesk$MEMBER_COUNTRY;

    const-string v1, "E_ARGENTINA"

    const/16 v2, 0x28

    invoke-direct {v0, v1, v2}, Lcom/konka/kkinterface/tv/DataBaseDesk$MEMBER_COUNTRY;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/konka/kkinterface/tv/DataBaseDesk$MEMBER_COUNTRY;->E_ARGENTINA:Lcom/konka/kkinterface/tv/DataBaseDesk$MEMBER_COUNTRY;

    new-instance v0, Lcom/konka/kkinterface/tv/DataBaseDesk$MEMBER_COUNTRY;

    const-string v1, "E_CHILE"

    const/16 v2, 0x29

    invoke-direct {v0, v1, v2}, Lcom/konka/kkinterface/tv/DataBaseDesk$MEMBER_COUNTRY;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/konka/kkinterface/tv/DataBaseDesk$MEMBER_COUNTRY;->E_CHILE:Lcom/konka/kkinterface/tv/DataBaseDesk$MEMBER_COUNTRY;

    new-instance v0, Lcom/konka/kkinterface/tv/DataBaseDesk$MEMBER_COUNTRY;

    const-string v1, "E_VENEZUELA"

    const/16 v2, 0x2a

    invoke-direct {v0, v1, v2}, Lcom/konka/kkinterface/tv/DataBaseDesk$MEMBER_COUNTRY;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/konka/kkinterface/tv/DataBaseDesk$MEMBER_COUNTRY;->E_VENEZUELA:Lcom/konka/kkinterface/tv/DataBaseDesk$MEMBER_COUNTRY;

    new-instance v0, Lcom/konka/kkinterface/tv/DataBaseDesk$MEMBER_COUNTRY;

    const-string v1, "E_ECUADOR"

    const/16 v2, 0x2b

    invoke-direct {v0, v1, v2}, Lcom/konka/kkinterface/tv/DataBaseDesk$MEMBER_COUNTRY;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/konka/kkinterface/tv/DataBaseDesk$MEMBER_COUNTRY;->E_ECUADOR:Lcom/konka/kkinterface/tv/DataBaseDesk$MEMBER_COUNTRY;

    new-instance v0, Lcom/konka/kkinterface/tv/DataBaseDesk$MEMBER_COUNTRY;

    const-string v1, "E_COSTARICA"

    const/16 v2, 0x2c

    invoke-direct {v0, v1, v2}, Lcom/konka/kkinterface/tv/DataBaseDesk$MEMBER_COUNTRY;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/konka/kkinterface/tv/DataBaseDesk$MEMBER_COUNTRY;->E_COSTARICA:Lcom/konka/kkinterface/tv/DataBaseDesk$MEMBER_COUNTRY;

    new-instance v0, Lcom/konka/kkinterface/tv/DataBaseDesk$MEMBER_COUNTRY;

    const-string v1, "E_PARAGUAY"

    const/16 v2, 0x2d

    invoke-direct {v0, v1, v2}, Lcom/konka/kkinterface/tv/DataBaseDesk$MEMBER_COUNTRY;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/konka/kkinterface/tv/DataBaseDesk$MEMBER_COUNTRY;->E_PARAGUAY:Lcom/konka/kkinterface/tv/DataBaseDesk$MEMBER_COUNTRY;

    new-instance v0, Lcom/konka/kkinterface/tv/DataBaseDesk$MEMBER_COUNTRY;

    const-string v1, "E_BOLIVIA"

    const/16 v2, 0x2e

    invoke-direct {v0, v1, v2}, Lcom/konka/kkinterface/tv/DataBaseDesk$MEMBER_COUNTRY;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/konka/kkinterface/tv/DataBaseDesk$MEMBER_COUNTRY;->E_BOLIVIA:Lcom/konka/kkinterface/tv/DataBaseDesk$MEMBER_COUNTRY;

    new-instance v0, Lcom/konka/kkinterface/tv/DataBaseDesk$MEMBER_COUNTRY;

    const-string v1, "E_BELIZE"

    const/16 v2, 0x2f

    invoke-direct {v0, v1, v2}, Lcom/konka/kkinterface/tv/DataBaseDesk$MEMBER_COUNTRY;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/konka/kkinterface/tv/DataBaseDesk$MEMBER_COUNTRY;->E_BELIZE:Lcom/konka/kkinterface/tv/DataBaseDesk$MEMBER_COUNTRY;

    new-instance v0, Lcom/konka/kkinterface/tv/DataBaseDesk$MEMBER_COUNTRY;

    const-string v1, "E_NICARAGUA"

    const/16 v2, 0x30

    invoke-direct {v0, v1, v2}, Lcom/konka/kkinterface/tv/DataBaseDesk$MEMBER_COUNTRY;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/konka/kkinterface/tv/DataBaseDesk$MEMBER_COUNTRY;->E_NICARAGUA:Lcom/konka/kkinterface/tv/DataBaseDesk$MEMBER_COUNTRY;

    new-instance v0, Lcom/konka/kkinterface/tv/DataBaseDesk$MEMBER_COUNTRY;

    const-string v1, "E_GUATEMALA"

    const/16 v2, 0x31

    invoke-direct {v0, v1, v2}, Lcom/konka/kkinterface/tv/DataBaseDesk$MEMBER_COUNTRY;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/konka/kkinterface/tv/DataBaseDesk$MEMBER_COUNTRY;->E_GUATEMALA:Lcom/konka/kkinterface/tv/DataBaseDesk$MEMBER_COUNTRY;

    new-instance v0, Lcom/konka/kkinterface/tv/DataBaseDesk$MEMBER_COUNTRY;

    const-string v1, "E_CHINA"

    const/16 v2, 0x32

    invoke-direct {v0, v1, v2}, Lcom/konka/kkinterface/tv/DataBaseDesk$MEMBER_COUNTRY;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/konka/kkinterface/tv/DataBaseDesk$MEMBER_COUNTRY;->E_CHINA:Lcom/konka/kkinterface/tv/DataBaseDesk$MEMBER_COUNTRY;

    new-instance v0, Lcom/konka/kkinterface/tv/DataBaseDesk$MEMBER_COUNTRY;

    const-string v1, "E_TAIWAN"

    const/16 v2, 0x33

    invoke-direct {v0, v1, v2}, Lcom/konka/kkinterface/tv/DataBaseDesk$MEMBER_COUNTRY;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/konka/kkinterface/tv/DataBaseDesk$MEMBER_COUNTRY;->E_TAIWAN:Lcom/konka/kkinterface/tv/DataBaseDesk$MEMBER_COUNTRY;

    new-instance v0, Lcom/konka/kkinterface/tv/DataBaseDesk$MEMBER_COUNTRY;

    const-string v1, "E_BRAZIL"

    const/16 v2, 0x34

    invoke-direct {v0, v1, v2}, Lcom/konka/kkinterface/tv/DataBaseDesk$MEMBER_COUNTRY;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/konka/kkinterface/tv/DataBaseDesk$MEMBER_COUNTRY;->E_BRAZIL:Lcom/konka/kkinterface/tv/DataBaseDesk$MEMBER_COUNTRY;

    new-instance v0, Lcom/konka/kkinterface/tv/DataBaseDesk$MEMBER_COUNTRY;

    const-string v1, "E_CANADA"

    const/16 v2, 0x35

    invoke-direct {v0, v1, v2}, Lcom/konka/kkinterface/tv/DataBaseDesk$MEMBER_COUNTRY;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/konka/kkinterface/tv/DataBaseDesk$MEMBER_COUNTRY;->E_CANADA:Lcom/konka/kkinterface/tv/DataBaseDesk$MEMBER_COUNTRY;

    new-instance v0, Lcom/konka/kkinterface/tv/DataBaseDesk$MEMBER_COUNTRY;

    const-string v1, "E_MEXICO"

    const/16 v2, 0x36

    invoke-direct {v0, v1, v2}, Lcom/konka/kkinterface/tv/DataBaseDesk$MEMBER_COUNTRY;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/konka/kkinterface/tv/DataBaseDesk$MEMBER_COUNTRY;->E_MEXICO:Lcom/konka/kkinterface/tv/DataBaseDesk$MEMBER_COUNTRY;

    new-instance v0, Lcom/konka/kkinterface/tv/DataBaseDesk$MEMBER_COUNTRY;

    const-string v1, "E_US"

    const/16 v2, 0x37

    invoke-direct {v0, v1, v2}, Lcom/konka/kkinterface/tv/DataBaseDesk$MEMBER_COUNTRY;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/konka/kkinterface/tv/DataBaseDesk$MEMBER_COUNTRY;->E_US:Lcom/konka/kkinterface/tv/DataBaseDesk$MEMBER_COUNTRY;

    new-instance v0, Lcom/konka/kkinterface/tv/DataBaseDesk$MEMBER_COUNTRY;

    const-string v1, "E_SOUTHKOREA"

    const/16 v2, 0x38

    invoke-direct {v0, v1, v2}, Lcom/konka/kkinterface/tv/DataBaseDesk$MEMBER_COUNTRY;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/konka/kkinterface/tv/DataBaseDesk$MEMBER_COUNTRY;->E_SOUTHKOREA:Lcom/konka/kkinterface/tv/DataBaseDesk$MEMBER_COUNTRY;

    new-instance v0, Lcom/konka/kkinterface/tv/DataBaseDesk$MEMBER_COUNTRY;

    const-string v1, "E_FIJI"

    const/16 v2, 0x39

    invoke-direct {v0, v1, v2}, Lcom/konka/kkinterface/tv/DataBaseDesk$MEMBER_COUNTRY;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/konka/kkinterface/tv/DataBaseDesk$MEMBER_COUNTRY;->E_FIJI:Lcom/konka/kkinterface/tv/DataBaseDesk$MEMBER_COUNTRY;

    new-instance v0, Lcom/konka/kkinterface/tv/DataBaseDesk$MEMBER_COUNTRY;

    const-string v1, "E_UZBEK"

    const/16 v2, 0x3a

    invoke-direct {v0, v1, v2}, Lcom/konka/kkinterface/tv/DataBaseDesk$MEMBER_COUNTRY;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/konka/kkinterface/tv/DataBaseDesk$MEMBER_COUNTRY;->E_UZBEK:Lcom/konka/kkinterface/tv/DataBaseDesk$MEMBER_COUNTRY;

    new-instance v0, Lcom/konka/kkinterface/tv/DataBaseDesk$MEMBER_COUNTRY;

    const-string v1, "E_TAJIKISTAN"

    const/16 v2, 0x3b

    invoke-direct {v0, v1, v2}, Lcom/konka/kkinterface/tv/DataBaseDesk$MEMBER_COUNTRY;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/konka/kkinterface/tv/DataBaseDesk$MEMBER_COUNTRY;->E_TAJIKISTAN:Lcom/konka/kkinterface/tv/DataBaseDesk$MEMBER_COUNTRY;

    new-instance v0, Lcom/konka/kkinterface/tv/DataBaseDesk$MEMBER_COUNTRY;

    const-string v1, "E_ETHIOPIA"

    const/16 v2, 0x3c

    invoke-direct {v0, v1, v2}, Lcom/konka/kkinterface/tv/DataBaseDesk$MEMBER_COUNTRY;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/konka/kkinterface/tv/DataBaseDesk$MEMBER_COUNTRY;->E_ETHIOPIA:Lcom/konka/kkinterface/tv/DataBaseDesk$MEMBER_COUNTRY;

    new-instance v0, Lcom/konka/kkinterface/tv/DataBaseDesk$MEMBER_COUNTRY;

    const-string v1, "E_AZERBAIJAN"

    const/16 v2, 0x3d

    invoke-direct {v0, v1, v2}, Lcom/konka/kkinterface/tv/DataBaseDesk$MEMBER_COUNTRY;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/konka/kkinterface/tv/DataBaseDesk$MEMBER_COUNTRY;->E_AZERBAIJAN:Lcom/konka/kkinterface/tv/DataBaseDesk$MEMBER_COUNTRY;

    new-instance v0, Lcom/konka/kkinterface/tv/DataBaseDesk$MEMBER_COUNTRY;

    const-string v1, "E_SOUTHAFRICA"

    const/16 v2, 0x3e

    invoke-direct {v0, v1, v2}, Lcom/konka/kkinterface/tv/DataBaseDesk$MEMBER_COUNTRY;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/konka/kkinterface/tv/DataBaseDesk$MEMBER_COUNTRY;->E_SOUTHAFRICA:Lcom/konka/kkinterface/tv/DataBaseDesk$MEMBER_COUNTRY;

    new-instance v0, Lcom/konka/kkinterface/tv/DataBaseDesk$MEMBER_COUNTRY;

    const-string v1, "E_ALGERIA"

    const/16 v2, 0x3f

    invoke-direct {v0, v1, v2}, Lcom/konka/kkinterface/tv/DataBaseDesk$MEMBER_COUNTRY;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/konka/kkinterface/tv/DataBaseDesk$MEMBER_COUNTRY;->E_ALGERIA:Lcom/konka/kkinterface/tv/DataBaseDesk$MEMBER_COUNTRY;

    new-instance v0, Lcom/konka/kkinterface/tv/DataBaseDesk$MEMBER_COUNTRY;

    const-string v1, "E_EGYPT"

    const/16 v2, 0x40

    invoke-direct {v0, v1, v2}, Lcom/konka/kkinterface/tv/DataBaseDesk$MEMBER_COUNTRY;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/konka/kkinterface/tv/DataBaseDesk$MEMBER_COUNTRY;->E_EGYPT:Lcom/konka/kkinterface/tv/DataBaseDesk$MEMBER_COUNTRY;

    new-instance v0, Lcom/konka/kkinterface/tv/DataBaseDesk$MEMBER_COUNTRY;

    const-string v1, "E_SAUDI_ARABIA"

    const/16 v2, 0x41

    invoke-direct {v0, v1, v2}, Lcom/konka/kkinterface/tv/DataBaseDesk$MEMBER_COUNTRY;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/konka/kkinterface/tv/DataBaseDesk$MEMBER_COUNTRY;->E_SAUDI_ARABIA:Lcom/konka/kkinterface/tv/DataBaseDesk$MEMBER_COUNTRY;

    new-instance v0, Lcom/konka/kkinterface/tv/DataBaseDesk$MEMBER_COUNTRY;

    const-string v1, "E_IRAN"

    const/16 v2, 0x42

    invoke-direct {v0, v1, v2}, Lcom/konka/kkinterface/tv/DataBaseDesk$MEMBER_COUNTRY;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/konka/kkinterface/tv/DataBaseDesk$MEMBER_COUNTRY;->E_IRAN:Lcom/konka/kkinterface/tv/DataBaseDesk$MEMBER_COUNTRY;

    new-instance v0, Lcom/konka/kkinterface/tv/DataBaseDesk$MEMBER_COUNTRY;

    const-string v1, "E_IRAQ"

    const/16 v2, 0x43

    invoke-direct {v0, v1, v2}, Lcom/konka/kkinterface/tv/DataBaseDesk$MEMBER_COUNTRY;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/konka/kkinterface/tv/DataBaseDesk$MEMBER_COUNTRY;->E_IRAQ:Lcom/konka/kkinterface/tv/DataBaseDesk$MEMBER_COUNTRY;

    new-instance v0, Lcom/konka/kkinterface/tv/DataBaseDesk$MEMBER_COUNTRY;

    const-string v1, "E_NAMIBIA"

    const/16 v2, 0x44

    invoke-direct {v0, v1, v2}, Lcom/konka/kkinterface/tv/DataBaseDesk$MEMBER_COUNTRY;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/konka/kkinterface/tv/DataBaseDesk$MEMBER_COUNTRY;->E_NAMIBIA:Lcom/konka/kkinterface/tv/DataBaseDesk$MEMBER_COUNTRY;

    new-instance v0, Lcom/konka/kkinterface/tv/DataBaseDesk$MEMBER_COUNTRY;

    const-string v1, "E_JORDAN"

    const/16 v2, 0x45

    invoke-direct {v0, v1, v2}, Lcom/konka/kkinterface/tv/DataBaseDesk$MEMBER_COUNTRY;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/konka/kkinterface/tv/DataBaseDesk$MEMBER_COUNTRY;->E_JORDAN:Lcom/konka/kkinterface/tv/DataBaseDesk$MEMBER_COUNTRY;

    new-instance v0, Lcom/konka/kkinterface/tv/DataBaseDesk$MEMBER_COUNTRY;

    const-string v1, "E_KUWAIT"

    const/16 v2, 0x46

    invoke-direct {v0, v1, v2}, Lcom/konka/kkinterface/tv/DataBaseDesk$MEMBER_COUNTRY;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/konka/kkinterface/tv/DataBaseDesk$MEMBER_COUNTRY;->E_KUWAIT:Lcom/konka/kkinterface/tv/DataBaseDesk$MEMBER_COUNTRY;

    new-instance v0, Lcom/konka/kkinterface/tv/DataBaseDesk$MEMBER_COUNTRY;

    const-string v1, "E_INDONESIA"

    const/16 v2, 0x47

    invoke-direct {v0, v1, v2}, Lcom/konka/kkinterface/tv/DataBaseDesk$MEMBER_COUNTRY;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/konka/kkinterface/tv/DataBaseDesk$MEMBER_COUNTRY;->E_INDONESIA:Lcom/konka/kkinterface/tv/DataBaseDesk$MEMBER_COUNTRY;

    new-instance v0, Lcom/konka/kkinterface/tv/DataBaseDesk$MEMBER_COUNTRY;

    const-string v1, "E_ISRAEL"

    const/16 v2, 0x48

    invoke-direct {v0, v1, v2}, Lcom/konka/kkinterface/tv/DataBaseDesk$MEMBER_COUNTRY;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/konka/kkinterface/tv/DataBaseDesk$MEMBER_COUNTRY;->E_ISRAEL:Lcom/konka/kkinterface/tv/DataBaseDesk$MEMBER_COUNTRY;

    new-instance v0, Lcom/konka/kkinterface/tv/DataBaseDesk$MEMBER_COUNTRY;

    const-string v1, "E_QATAR"

    const/16 v2, 0x49

    invoke-direct {v0, v1, v2}, Lcom/konka/kkinterface/tv/DataBaseDesk$MEMBER_COUNTRY;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/konka/kkinterface/tv/DataBaseDesk$MEMBER_COUNTRY;->E_QATAR:Lcom/konka/kkinterface/tv/DataBaseDesk$MEMBER_COUNTRY;

    new-instance v0, Lcom/konka/kkinterface/tv/DataBaseDesk$MEMBER_COUNTRY;

    const-string v1, "E_NIGERIA"

    const/16 v2, 0x4a

    invoke-direct {v0, v1, v2}, Lcom/konka/kkinterface/tv/DataBaseDesk$MEMBER_COUNTRY;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/konka/kkinterface/tv/DataBaseDesk$MEMBER_COUNTRY;->E_NIGERIA:Lcom/konka/kkinterface/tv/DataBaseDesk$MEMBER_COUNTRY;

    new-instance v0, Lcom/konka/kkinterface/tv/DataBaseDesk$MEMBER_COUNTRY;

    const-string v1, "E_ZEMBABWE"

    const/16 v2, 0x4b

    invoke-direct {v0, v1, v2}, Lcom/konka/kkinterface/tv/DataBaseDesk$MEMBER_COUNTRY;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/konka/kkinterface/tv/DataBaseDesk$MEMBER_COUNTRY;->E_ZEMBABWE:Lcom/konka/kkinterface/tv/DataBaseDesk$MEMBER_COUNTRY;

    new-instance v0, Lcom/konka/kkinterface/tv/DataBaseDesk$MEMBER_COUNTRY;

    const-string v1, "E_LITHUANIA"

    const/16 v2, 0x4c

    invoke-direct {v0, v1, v2}, Lcom/konka/kkinterface/tv/DataBaseDesk$MEMBER_COUNTRY;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/konka/kkinterface/tv/DataBaseDesk$MEMBER_COUNTRY;->E_LITHUANIA:Lcom/konka/kkinterface/tv/DataBaseDesk$MEMBER_COUNTRY;

    new-instance v0, Lcom/konka/kkinterface/tv/DataBaseDesk$MEMBER_COUNTRY;

    const-string v1, "E_MOROCCO"

    const/16 v2, 0x4d

    invoke-direct {v0, v1, v2}, Lcom/konka/kkinterface/tv/DataBaseDesk$MEMBER_COUNTRY;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/konka/kkinterface/tv/DataBaseDesk$MEMBER_COUNTRY;->E_MOROCCO:Lcom/konka/kkinterface/tv/DataBaseDesk$MEMBER_COUNTRY;

    new-instance v0, Lcom/konka/kkinterface/tv/DataBaseDesk$MEMBER_COUNTRY;

    const-string v1, "E_TUNIS"

    const/16 v2, 0x4e

    invoke-direct {v0, v1, v2}, Lcom/konka/kkinterface/tv/DataBaseDesk$MEMBER_COUNTRY;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/konka/kkinterface/tv/DataBaseDesk$MEMBER_COUNTRY;->E_TUNIS:Lcom/konka/kkinterface/tv/DataBaseDesk$MEMBER_COUNTRY;

    new-instance v0, Lcom/konka/kkinterface/tv/DataBaseDesk$MEMBER_COUNTRY;

    const-string v1, "E_INDIA"

    const/16 v2, 0x4f

    invoke-direct {v0, v1, v2}, Lcom/konka/kkinterface/tv/DataBaseDesk$MEMBER_COUNTRY;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/konka/kkinterface/tv/DataBaseDesk$MEMBER_COUNTRY;->E_INDIA:Lcom/konka/kkinterface/tv/DataBaseDesk$MEMBER_COUNTRY;

    new-instance v0, Lcom/konka/kkinterface/tv/DataBaseDesk$MEMBER_COUNTRY;

    const-string v1, "E_YEMEN"

    const/16 v2, 0x50

    invoke-direct {v0, v1, v2}, Lcom/konka/kkinterface/tv/DataBaseDesk$MEMBER_COUNTRY;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/konka/kkinterface/tv/DataBaseDesk$MEMBER_COUNTRY;->E_YEMEN:Lcom/konka/kkinterface/tv/DataBaseDesk$MEMBER_COUNTRY;

    new-instance v0, Lcom/konka/kkinterface/tv/DataBaseDesk$MEMBER_COUNTRY;

    const-string v1, "E_LIBYA"

    const/16 v2, 0x51

    invoke-direct {v0, v1, v2}, Lcom/konka/kkinterface/tv/DataBaseDesk$MEMBER_COUNTRY;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/konka/kkinterface/tv/DataBaseDesk$MEMBER_COUNTRY;->E_LIBYA:Lcom/konka/kkinterface/tv/DataBaseDesk$MEMBER_COUNTRY;

    new-instance v0, Lcom/konka/kkinterface/tv/DataBaseDesk$MEMBER_COUNTRY;

    const-string v1, "E_PAKISTAN"

    const/16 v2, 0x52

    invoke-direct {v0, v1, v2}, Lcom/konka/kkinterface/tv/DataBaseDesk$MEMBER_COUNTRY;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/konka/kkinterface/tv/DataBaseDesk$MEMBER_COUNTRY;->E_PAKISTAN:Lcom/konka/kkinterface/tv/DataBaseDesk$MEMBER_COUNTRY;

    new-instance v0, Lcom/konka/kkinterface/tv/DataBaseDesk$MEMBER_COUNTRY;

    const-string v1, "E_OTHERS"

    const/16 v2, 0x53

    invoke-direct {v0, v1, v2}, Lcom/konka/kkinterface/tv/DataBaseDesk$MEMBER_COUNTRY;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/konka/kkinterface/tv/DataBaseDesk$MEMBER_COUNTRY;->E_OTHERS:Lcom/konka/kkinterface/tv/DataBaseDesk$MEMBER_COUNTRY;

    new-instance v0, Lcom/konka/kkinterface/tv/DataBaseDesk$MEMBER_COUNTRY;

    const-string v1, "E_COUNTRY_NUM"

    const/16 v2, 0x54

    invoke-direct {v0, v1, v2}, Lcom/konka/kkinterface/tv/DataBaseDesk$MEMBER_COUNTRY;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/konka/kkinterface/tv/DataBaseDesk$MEMBER_COUNTRY;->E_COUNTRY_NUM:Lcom/konka/kkinterface/tv/DataBaseDesk$MEMBER_COUNTRY;

    const/16 v0, 0x55

    new-array v0, v0, [Lcom/konka/kkinterface/tv/DataBaseDesk$MEMBER_COUNTRY;

    sget-object v1, Lcom/konka/kkinterface/tv/DataBaseDesk$MEMBER_COUNTRY;->E_AUSTRALIA:Lcom/konka/kkinterface/tv/DataBaseDesk$MEMBER_COUNTRY;

    aput-object v1, v0, v3

    sget-object v1, Lcom/konka/kkinterface/tv/DataBaseDesk$MEMBER_COUNTRY;->E_AUSTRIA:Lcom/konka/kkinterface/tv/DataBaseDesk$MEMBER_COUNTRY;

    aput-object v1, v0, v4

    sget-object v1, Lcom/konka/kkinterface/tv/DataBaseDesk$MEMBER_COUNTRY;->E_BELGIUM:Lcom/konka/kkinterface/tv/DataBaseDesk$MEMBER_COUNTRY;

    aput-object v1, v0, v5

    sget-object v1, Lcom/konka/kkinterface/tv/DataBaseDesk$MEMBER_COUNTRY;->E_BULGARIA:Lcom/konka/kkinterface/tv/DataBaseDesk$MEMBER_COUNTRY;

    aput-object v1, v0, v6

    sget-object v1, Lcom/konka/kkinterface/tv/DataBaseDesk$MEMBER_COUNTRY;->E_CROATIA:Lcom/konka/kkinterface/tv/DataBaseDesk$MEMBER_COUNTRY;

    aput-object v1, v0, v7

    const/4 v1, 0x5

    sget-object v2, Lcom/konka/kkinterface/tv/DataBaseDesk$MEMBER_COUNTRY;->E_CZECH:Lcom/konka/kkinterface/tv/DataBaseDesk$MEMBER_COUNTRY;

    aput-object v2, v0, v1

    const/4 v1, 0x6

    sget-object v2, Lcom/konka/kkinterface/tv/DataBaseDesk$MEMBER_COUNTRY;->E_DENMARK:Lcom/konka/kkinterface/tv/DataBaseDesk$MEMBER_COUNTRY;

    aput-object v2, v0, v1

    const/4 v1, 0x7

    sget-object v2, Lcom/konka/kkinterface/tv/DataBaseDesk$MEMBER_COUNTRY;->E_FINLAND:Lcom/konka/kkinterface/tv/DataBaseDesk$MEMBER_COUNTRY;

    aput-object v2, v0, v1

    const/16 v1, 0x8

    sget-object v2, Lcom/konka/kkinterface/tv/DataBaseDesk$MEMBER_COUNTRY;->E_FRANCE:Lcom/konka/kkinterface/tv/DataBaseDesk$MEMBER_COUNTRY;

    aput-object v2, v0, v1

    const/16 v1, 0x9

    sget-object v2, Lcom/konka/kkinterface/tv/DataBaseDesk$MEMBER_COUNTRY;->E_GERMANY:Lcom/konka/kkinterface/tv/DataBaseDesk$MEMBER_COUNTRY;

    aput-object v2, v0, v1

    const/16 v1, 0xa

    sget-object v2, Lcom/konka/kkinterface/tv/DataBaseDesk$MEMBER_COUNTRY;->E_GREECE:Lcom/konka/kkinterface/tv/DataBaseDesk$MEMBER_COUNTRY;

    aput-object v2, v0, v1

    const/16 v1, 0xb

    sget-object v2, Lcom/konka/kkinterface/tv/DataBaseDesk$MEMBER_COUNTRY;->E_HUNGARY:Lcom/konka/kkinterface/tv/DataBaseDesk$MEMBER_COUNTRY;

    aput-object v2, v0, v1

    const/16 v1, 0xc

    sget-object v2, Lcom/konka/kkinterface/tv/DataBaseDesk$MEMBER_COUNTRY;->E_ITALY:Lcom/konka/kkinterface/tv/DataBaseDesk$MEMBER_COUNTRY;

    aput-object v2, v0, v1

    const/16 v1, 0xd

    sget-object v2, Lcom/konka/kkinterface/tv/DataBaseDesk$MEMBER_COUNTRY;->E_LUXEMBOURG:Lcom/konka/kkinterface/tv/DataBaseDesk$MEMBER_COUNTRY;

    aput-object v2, v0, v1

    const/16 v1, 0xe

    sget-object v2, Lcom/konka/kkinterface/tv/DataBaseDesk$MEMBER_COUNTRY;->E_NETHERLANDS:Lcom/konka/kkinterface/tv/DataBaseDesk$MEMBER_COUNTRY;

    aput-object v2, v0, v1

    const/16 v1, 0xf

    sget-object v2, Lcom/konka/kkinterface/tv/DataBaseDesk$MEMBER_COUNTRY;->E_NORWAY:Lcom/konka/kkinterface/tv/DataBaseDesk$MEMBER_COUNTRY;

    aput-object v2, v0, v1

    const/16 v1, 0x10

    sget-object v2, Lcom/konka/kkinterface/tv/DataBaseDesk$MEMBER_COUNTRY;->E_POLAND:Lcom/konka/kkinterface/tv/DataBaseDesk$MEMBER_COUNTRY;

    aput-object v2, v0, v1

    const/16 v1, 0x11

    sget-object v2, Lcom/konka/kkinterface/tv/DataBaseDesk$MEMBER_COUNTRY;->E_PORTUGAL:Lcom/konka/kkinterface/tv/DataBaseDesk$MEMBER_COUNTRY;

    aput-object v2, v0, v1

    const/16 v1, 0x12

    sget-object v2, Lcom/konka/kkinterface/tv/DataBaseDesk$MEMBER_COUNTRY;->E_RUMANIA:Lcom/konka/kkinterface/tv/DataBaseDesk$MEMBER_COUNTRY;

    aput-object v2, v0, v1

    const/16 v1, 0x13

    sget-object v2, Lcom/konka/kkinterface/tv/DataBaseDesk$MEMBER_COUNTRY;->E_RUSSIA:Lcom/konka/kkinterface/tv/DataBaseDesk$MEMBER_COUNTRY;

    aput-object v2, v0, v1

    const/16 v1, 0x14

    sget-object v2, Lcom/konka/kkinterface/tv/DataBaseDesk$MEMBER_COUNTRY;->E_SERBIA:Lcom/konka/kkinterface/tv/DataBaseDesk$MEMBER_COUNTRY;

    aput-object v2, v0, v1

    const/16 v1, 0x15

    sget-object v2, Lcom/konka/kkinterface/tv/DataBaseDesk$MEMBER_COUNTRY;->E_SLOVENIA:Lcom/konka/kkinterface/tv/DataBaseDesk$MEMBER_COUNTRY;

    aput-object v2, v0, v1

    const/16 v1, 0x16

    sget-object v2, Lcom/konka/kkinterface/tv/DataBaseDesk$MEMBER_COUNTRY;->E_SPAIN:Lcom/konka/kkinterface/tv/DataBaseDesk$MEMBER_COUNTRY;

    aput-object v2, v0, v1

    const/16 v1, 0x17

    sget-object v2, Lcom/konka/kkinterface/tv/DataBaseDesk$MEMBER_COUNTRY;->E_SWEDEN:Lcom/konka/kkinterface/tv/DataBaseDesk$MEMBER_COUNTRY;

    aput-object v2, v0, v1

    const/16 v1, 0x18

    sget-object v2, Lcom/konka/kkinterface/tv/DataBaseDesk$MEMBER_COUNTRY;->E_SWITZERLAND:Lcom/konka/kkinterface/tv/DataBaseDesk$MEMBER_COUNTRY;

    aput-object v2, v0, v1

    const/16 v1, 0x19

    sget-object v2, Lcom/konka/kkinterface/tv/DataBaseDesk$MEMBER_COUNTRY;->E_UK:Lcom/konka/kkinterface/tv/DataBaseDesk$MEMBER_COUNTRY;

    aput-object v2, v0, v1

    const/16 v1, 0x1a

    sget-object v2, Lcom/konka/kkinterface/tv/DataBaseDesk$MEMBER_COUNTRY;->E_NEWZEALAND:Lcom/konka/kkinterface/tv/DataBaseDesk$MEMBER_COUNTRY;

    aput-object v2, v0, v1

    const/16 v1, 0x1b

    sget-object v2, Lcom/konka/kkinterface/tv/DataBaseDesk$MEMBER_COUNTRY;->E_ARAB:Lcom/konka/kkinterface/tv/DataBaseDesk$MEMBER_COUNTRY;

    aput-object v2, v0, v1

    const/16 v1, 0x1c

    sget-object v2, Lcom/konka/kkinterface/tv/DataBaseDesk$MEMBER_COUNTRY;->E_ESTONIA:Lcom/konka/kkinterface/tv/DataBaseDesk$MEMBER_COUNTRY;

    aput-object v2, v0, v1

    const/16 v1, 0x1d

    sget-object v2, Lcom/konka/kkinterface/tv/DataBaseDesk$MEMBER_COUNTRY;->E_HEBREW:Lcom/konka/kkinterface/tv/DataBaseDesk$MEMBER_COUNTRY;

    aput-object v2, v0, v1

    const/16 v1, 0x1e

    sget-object v2, Lcom/konka/kkinterface/tv/DataBaseDesk$MEMBER_COUNTRY;->E_LATVIA:Lcom/konka/kkinterface/tv/DataBaseDesk$MEMBER_COUNTRY;

    aput-object v2, v0, v1

    const/16 v1, 0x1f

    sget-object v2, Lcom/konka/kkinterface/tv/DataBaseDesk$MEMBER_COUNTRY;->E_SLOVAKIA:Lcom/konka/kkinterface/tv/DataBaseDesk$MEMBER_COUNTRY;

    aput-object v2, v0, v1

    const/16 v1, 0x20

    sget-object v2, Lcom/konka/kkinterface/tv/DataBaseDesk$MEMBER_COUNTRY;->E_TURKEY:Lcom/konka/kkinterface/tv/DataBaseDesk$MEMBER_COUNTRY;

    aput-object v2, v0, v1

    const/16 v1, 0x21

    sget-object v2, Lcom/konka/kkinterface/tv/DataBaseDesk$MEMBER_COUNTRY;->E_IRELAND:Lcom/konka/kkinterface/tv/DataBaseDesk$MEMBER_COUNTRY;

    aput-object v2, v0, v1

    const/16 v1, 0x22

    sget-object v2, Lcom/konka/kkinterface/tv/DataBaseDesk$MEMBER_COUNTRY;->E_JAPAN:Lcom/konka/kkinterface/tv/DataBaseDesk$MEMBER_COUNTRY;

    aput-object v2, v0, v1

    const/16 v1, 0x23

    sget-object v2, Lcom/konka/kkinterface/tv/DataBaseDesk$MEMBER_COUNTRY;->E_PHILIPPINES:Lcom/konka/kkinterface/tv/DataBaseDesk$MEMBER_COUNTRY;

    aput-object v2, v0, v1

    const/16 v1, 0x24

    sget-object v2, Lcom/konka/kkinterface/tv/DataBaseDesk$MEMBER_COUNTRY;->E_THAILAND:Lcom/konka/kkinterface/tv/DataBaseDesk$MEMBER_COUNTRY;

    aput-object v2, v0, v1

    const/16 v1, 0x25

    sget-object v2, Lcom/konka/kkinterface/tv/DataBaseDesk$MEMBER_COUNTRY;->E_MALDIVES:Lcom/konka/kkinterface/tv/DataBaseDesk$MEMBER_COUNTRY;

    aput-object v2, v0, v1

    const/16 v1, 0x26

    sget-object v2, Lcom/konka/kkinterface/tv/DataBaseDesk$MEMBER_COUNTRY;->E_URUGUAY:Lcom/konka/kkinterface/tv/DataBaseDesk$MEMBER_COUNTRY;

    aput-object v2, v0, v1

    const/16 v1, 0x27

    sget-object v2, Lcom/konka/kkinterface/tv/DataBaseDesk$MEMBER_COUNTRY;->E_PERU:Lcom/konka/kkinterface/tv/DataBaseDesk$MEMBER_COUNTRY;

    aput-object v2, v0, v1

    const/16 v1, 0x28

    sget-object v2, Lcom/konka/kkinterface/tv/DataBaseDesk$MEMBER_COUNTRY;->E_ARGENTINA:Lcom/konka/kkinterface/tv/DataBaseDesk$MEMBER_COUNTRY;

    aput-object v2, v0, v1

    const/16 v1, 0x29

    sget-object v2, Lcom/konka/kkinterface/tv/DataBaseDesk$MEMBER_COUNTRY;->E_CHILE:Lcom/konka/kkinterface/tv/DataBaseDesk$MEMBER_COUNTRY;

    aput-object v2, v0, v1

    const/16 v1, 0x2a

    sget-object v2, Lcom/konka/kkinterface/tv/DataBaseDesk$MEMBER_COUNTRY;->E_VENEZUELA:Lcom/konka/kkinterface/tv/DataBaseDesk$MEMBER_COUNTRY;

    aput-object v2, v0, v1

    const/16 v1, 0x2b

    sget-object v2, Lcom/konka/kkinterface/tv/DataBaseDesk$MEMBER_COUNTRY;->E_ECUADOR:Lcom/konka/kkinterface/tv/DataBaseDesk$MEMBER_COUNTRY;

    aput-object v2, v0, v1

    const/16 v1, 0x2c

    sget-object v2, Lcom/konka/kkinterface/tv/DataBaseDesk$MEMBER_COUNTRY;->E_COSTARICA:Lcom/konka/kkinterface/tv/DataBaseDesk$MEMBER_COUNTRY;

    aput-object v2, v0, v1

    const/16 v1, 0x2d

    sget-object v2, Lcom/konka/kkinterface/tv/DataBaseDesk$MEMBER_COUNTRY;->E_PARAGUAY:Lcom/konka/kkinterface/tv/DataBaseDesk$MEMBER_COUNTRY;

    aput-object v2, v0, v1

    const/16 v1, 0x2e

    sget-object v2, Lcom/konka/kkinterface/tv/DataBaseDesk$MEMBER_COUNTRY;->E_BOLIVIA:Lcom/konka/kkinterface/tv/DataBaseDesk$MEMBER_COUNTRY;

    aput-object v2, v0, v1

    const/16 v1, 0x2f

    sget-object v2, Lcom/konka/kkinterface/tv/DataBaseDesk$MEMBER_COUNTRY;->E_BELIZE:Lcom/konka/kkinterface/tv/DataBaseDesk$MEMBER_COUNTRY;

    aput-object v2, v0, v1

    const/16 v1, 0x30

    sget-object v2, Lcom/konka/kkinterface/tv/DataBaseDesk$MEMBER_COUNTRY;->E_NICARAGUA:Lcom/konka/kkinterface/tv/DataBaseDesk$MEMBER_COUNTRY;

    aput-object v2, v0, v1

    const/16 v1, 0x31

    sget-object v2, Lcom/konka/kkinterface/tv/DataBaseDesk$MEMBER_COUNTRY;->E_GUATEMALA:Lcom/konka/kkinterface/tv/DataBaseDesk$MEMBER_COUNTRY;

    aput-object v2, v0, v1

    const/16 v1, 0x32

    sget-object v2, Lcom/konka/kkinterface/tv/DataBaseDesk$MEMBER_COUNTRY;->E_CHINA:Lcom/konka/kkinterface/tv/DataBaseDesk$MEMBER_COUNTRY;

    aput-object v2, v0, v1

    const/16 v1, 0x33

    sget-object v2, Lcom/konka/kkinterface/tv/DataBaseDesk$MEMBER_COUNTRY;->E_TAIWAN:Lcom/konka/kkinterface/tv/DataBaseDesk$MEMBER_COUNTRY;

    aput-object v2, v0, v1

    const/16 v1, 0x34

    sget-object v2, Lcom/konka/kkinterface/tv/DataBaseDesk$MEMBER_COUNTRY;->E_BRAZIL:Lcom/konka/kkinterface/tv/DataBaseDesk$MEMBER_COUNTRY;

    aput-object v2, v0, v1

    const/16 v1, 0x35

    sget-object v2, Lcom/konka/kkinterface/tv/DataBaseDesk$MEMBER_COUNTRY;->E_CANADA:Lcom/konka/kkinterface/tv/DataBaseDesk$MEMBER_COUNTRY;

    aput-object v2, v0, v1

    const/16 v1, 0x36

    sget-object v2, Lcom/konka/kkinterface/tv/DataBaseDesk$MEMBER_COUNTRY;->E_MEXICO:Lcom/konka/kkinterface/tv/DataBaseDesk$MEMBER_COUNTRY;

    aput-object v2, v0, v1

    const/16 v1, 0x37

    sget-object v2, Lcom/konka/kkinterface/tv/DataBaseDesk$MEMBER_COUNTRY;->E_US:Lcom/konka/kkinterface/tv/DataBaseDesk$MEMBER_COUNTRY;

    aput-object v2, v0, v1

    const/16 v1, 0x38

    sget-object v2, Lcom/konka/kkinterface/tv/DataBaseDesk$MEMBER_COUNTRY;->E_SOUTHKOREA:Lcom/konka/kkinterface/tv/DataBaseDesk$MEMBER_COUNTRY;

    aput-object v2, v0, v1

    const/16 v1, 0x39

    sget-object v2, Lcom/konka/kkinterface/tv/DataBaseDesk$MEMBER_COUNTRY;->E_FIJI:Lcom/konka/kkinterface/tv/DataBaseDesk$MEMBER_COUNTRY;

    aput-object v2, v0, v1

    const/16 v1, 0x3a

    sget-object v2, Lcom/konka/kkinterface/tv/DataBaseDesk$MEMBER_COUNTRY;->E_UZBEK:Lcom/konka/kkinterface/tv/DataBaseDesk$MEMBER_COUNTRY;

    aput-object v2, v0, v1

    const/16 v1, 0x3b

    sget-object v2, Lcom/konka/kkinterface/tv/DataBaseDesk$MEMBER_COUNTRY;->E_TAJIKISTAN:Lcom/konka/kkinterface/tv/DataBaseDesk$MEMBER_COUNTRY;

    aput-object v2, v0, v1

    const/16 v1, 0x3c

    sget-object v2, Lcom/konka/kkinterface/tv/DataBaseDesk$MEMBER_COUNTRY;->E_ETHIOPIA:Lcom/konka/kkinterface/tv/DataBaseDesk$MEMBER_COUNTRY;

    aput-object v2, v0, v1

    const/16 v1, 0x3d

    sget-object v2, Lcom/konka/kkinterface/tv/DataBaseDesk$MEMBER_COUNTRY;->E_AZERBAIJAN:Lcom/konka/kkinterface/tv/DataBaseDesk$MEMBER_COUNTRY;

    aput-object v2, v0, v1

    const/16 v1, 0x3e

    sget-object v2, Lcom/konka/kkinterface/tv/DataBaseDesk$MEMBER_COUNTRY;->E_SOUTHAFRICA:Lcom/konka/kkinterface/tv/DataBaseDesk$MEMBER_COUNTRY;

    aput-object v2, v0, v1

    const/16 v1, 0x3f

    sget-object v2, Lcom/konka/kkinterface/tv/DataBaseDesk$MEMBER_COUNTRY;->E_ALGERIA:Lcom/konka/kkinterface/tv/DataBaseDesk$MEMBER_COUNTRY;

    aput-object v2, v0, v1

    const/16 v1, 0x40

    sget-object v2, Lcom/konka/kkinterface/tv/DataBaseDesk$MEMBER_COUNTRY;->E_EGYPT:Lcom/konka/kkinterface/tv/DataBaseDesk$MEMBER_COUNTRY;

    aput-object v2, v0, v1

    const/16 v1, 0x41

    sget-object v2, Lcom/konka/kkinterface/tv/DataBaseDesk$MEMBER_COUNTRY;->E_SAUDI_ARABIA:Lcom/konka/kkinterface/tv/DataBaseDesk$MEMBER_COUNTRY;

    aput-object v2, v0, v1

    const/16 v1, 0x42

    sget-object v2, Lcom/konka/kkinterface/tv/DataBaseDesk$MEMBER_COUNTRY;->E_IRAN:Lcom/konka/kkinterface/tv/DataBaseDesk$MEMBER_COUNTRY;

    aput-object v2, v0, v1

    const/16 v1, 0x43

    sget-object v2, Lcom/konka/kkinterface/tv/DataBaseDesk$MEMBER_COUNTRY;->E_IRAQ:Lcom/konka/kkinterface/tv/DataBaseDesk$MEMBER_COUNTRY;

    aput-object v2, v0, v1

    const/16 v1, 0x44

    sget-object v2, Lcom/konka/kkinterface/tv/DataBaseDesk$MEMBER_COUNTRY;->E_NAMIBIA:Lcom/konka/kkinterface/tv/DataBaseDesk$MEMBER_COUNTRY;

    aput-object v2, v0, v1

    const/16 v1, 0x45

    sget-object v2, Lcom/konka/kkinterface/tv/DataBaseDesk$MEMBER_COUNTRY;->E_JORDAN:Lcom/konka/kkinterface/tv/DataBaseDesk$MEMBER_COUNTRY;

    aput-object v2, v0, v1

    const/16 v1, 0x46

    sget-object v2, Lcom/konka/kkinterface/tv/DataBaseDesk$MEMBER_COUNTRY;->E_KUWAIT:Lcom/konka/kkinterface/tv/DataBaseDesk$MEMBER_COUNTRY;

    aput-object v2, v0, v1

    const/16 v1, 0x47

    sget-object v2, Lcom/konka/kkinterface/tv/DataBaseDesk$MEMBER_COUNTRY;->E_INDONESIA:Lcom/konka/kkinterface/tv/DataBaseDesk$MEMBER_COUNTRY;

    aput-object v2, v0, v1

    const/16 v1, 0x48

    sget-object v2, Lcom/konka/kkinterface/tv/DataBaseDesk$MEMBER_COUNTRY;->E_ISRAEL:Lcom/konka/kkinterface/tv/DataBaseDesk$MEMBER_COUNTRY;

    aput-object v2, v0, v1

    const/16 v1, 0x49

    sget-object v2, Lcom/konka/kkinterface/tv/DataBaseDesk$MEMBER_COUNTRY;->E_QATAR:Lcom/konka/kkinterface/tv/DataBaseDesk$MEMBER_COUNTRY;

    aput-object v2, v0, v1

    const/16 v1, 0x4a

    sget-object v2, Lcom/konka/kkinterface/tv/DataBaseDesk$MEMBER_COUNTRY;->E_NIGERIA:Lcom/konka/kkinterface/tv/DataBaseDesk$MEMBER_COUNTRY;

    aput-object v2, v0, v1

    const/16 v1, 0x4b

    sget-object v2, Lcom/konka/kkinterface/tv/DataBaseDesk$MEMBER_COUNTRY;->E_ZEMBABWE:Lcom/konka/kkinterface/tv/DataBaseDesk$MEMBER_COUNTRY;

    aput-object v2, v0, v1

    const/16 v1, 0x4c

    sget-object v2, Lcom/konka/kkinterface/tv/DataBaseDesk$MEMBER_COUNTRY;->E_LITHUANIA:Lcom/konka/kkinterface/tv/DataBaseDesk$MEMBER_COUNTRY;

    aput-object v2, v0, v1

    const/16 v1, 0x4d

    sget-object v2, Lcom/konka/kkinterface/tv/DataBaseDesk$MEMBER_COUNTRY;->E_MOROCCO:Lcom/konka/kkinterface/tv/DataBaseDesk$MEMBER_COUNTRY;

    aput-object v2, v0, v1

    const/16 v1, 0x4e

    sget-object v2, Lcom/konka/kkinterface/tv/DataBaseDesk$MEMBER_COUNTRY;->E_TUNIS:Lcom/konka/kkinterface/tv/DataBaseDesk$MEMBER_COUNTRY;

    aput-object v2, v0, v1

    const/16 v1, 0x4f

    sget-object v2, Lcom/konka/kkinterface/tv/DataBaseDesk$MEMBER_COUNTRY;->E_INDIA:Lcom/konka/kkinterface/tv/DataBaseDesk$MEMBER_COUNTRY;

    aput-object v2, v0, v1

    const/16 v1, 0x50

    sget-object v2, Lcom/konka/kkinterface/tv/DataBaseDesk$MEMBER_COUNTRY;->E_YEMEN:Lcom/konka/kkinterface/tv/DataBaseDesk$MEMBER_COUNTRY;

    aput-object v2, v0, v1

    const/16 v1, 0x51

    sget-object v2, Lcom/konka/kkinterface/tv/DataBaseDesk$MEMBER_COUNTRY;->E_LIBYA:Lcom/konka/kkinterface/tv/DataBaseDesk$MEMBER_COUNTRY;

    aput-object v2, v0, v1

    const/16 v1, 0x52

    sget-object v2, Lcom/konka/kkinterface/tv/DataBaseDesk$MEMBER_COUNTRY;->E_PAKISTAN:Lcom/konka/kkinterface/tv/DataBaseDesk$MEMBER_COUNTRY;

    aput-object v2, v0, v1

    const/16 v1, 0x53

    sget-object v2, Lcom/konka/kkinterface/tv/DataBaseDesk$MEMBER_COUNTRY;->E_OTHERS:Lcom/konka/kkinterface/tv/DataBaseDesk$MEMBER_COUNTRY;

    aput-object v2, v0, v1

    const/16 v1, 0x54

    sget-object v2, Lcom/konka/kkinterface/tv/DataBaseDesk$MEMBER_COUNTRY;->E_COUNTRY_NUM:Lcom/konka/kkinterface/tv/DataBaseDesk$MEMBER_COUNTRY;

    aput-object v2, v0, v1

    sput-object v0, Lcom/konka/kkinterface/tv/DataBaseDesk$MEMBER_COUNTRY;->ENUM$VALUES:[Lcom/konka/kkinterface/tv/DataBaseDesk$MEMBER_COUNTRY;

    return-void
.end method

.method private constructor <init>(Ljava/lang/String;I)V
    .locals 0

    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    return-void
.end method

.method public static valueOf(Ljava/lang/String;)Lcom/konka/kkinterface/tv/DataBaseDesk$MEMBER_COUNTRY;
    .locals 1

    const-class v0, Lcom/konka/kkinterface/tv/DataBaseDesk$MEMBER_COUNTRY;

    invoke-static {v0, p0}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;

    move-result-object v0

    check-cast v0, Lcom/konka/kkinterface/tv/DataBaseDesk$MEMBER_COUNTRY;

    return-object v0
.end method

.method public static values()[Lcom/konka/kkinterface/tv/DataBaseDesk$MEMBER_COUNTRY;
    .locals 4

    const/4 v3, 0x0

    sget-object v0, Lcom/konka/kkinterface/tv/DataBaseDesk$MEMBER_COUNTRY;->ENUM$VALUES:[Lcom/konka/kkinterface/tv/DataBaseDesk$MEMBER_COUNTRY;

    array-length v1, v0

    new-array v2, v1, [Lcom/konka/kkinterface/tv/DataBaseDesk$MEMBER_COUNTRY;

    invoke-static {v0, v3, v2, v3, v1}, Ljava/lang/System;->arraycopy(Ljava/lang/Object;ILjava/lang/Object;II)V

    return-object v2
.end method
