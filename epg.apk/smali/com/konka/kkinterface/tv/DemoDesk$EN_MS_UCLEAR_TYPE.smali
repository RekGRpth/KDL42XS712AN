.class public final enum Lcom/konka/kkinterface/tv/DemoDesk$EN_MS_UCLEAR_TYPE;
.super Ljava/lang/Enum;
.source "DemoDesk.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/konka/kkinterface/tv/DemoDesk;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x4019
    name = "EN_MS_UCLEAR_TYPE"
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Enum",
        "<",
        "Lcom/konka/kkinterface/tv/DemoDesk$EN_MS_UCLEAR_TYPE;",
        ">;"
    }
.end annotation


# static fields
.field private static final synthetic ENUM$VALUES:[Lcom/konka/kkinterface/tv/DemoDesk$EN_MS_UCLEAR_TYPE;

.field public static final enum E_MS_UCLEAR_OFF:Lcom/konka/kkinterface/tv/DemoDesk$EN_MS_UCLEAR_TYPE;

.field public static final enum E_MS_UCLEAR_ON:Lcom/konka/kkinterface/tv/DemoDesk$EN_MS_UCLEAR_TYPE;


# direct methods
.method static constructor <clinit>()V
    .locals 4

    const/4 v3, 0x1

    const/4 v2, 0x0

    new-instance v0, Lcom/konka/kkinterface/tv/DemoDesk$EN_MS_UCLEAR_TYPE;

    const-string v1, "E_MS_UCLEAR_OFF"

    invoke-direct {v0, v1, v2}, Lcom/konka/kkinterface/tv/DemoDesk$EN_MS_UCLEAR_TYPE;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/konka/kkinterface/tv/DemoDesk$EN_MS_UCLEAR_TYPE;->E_MS_UCLEAR_OFF:Lcom/konka/kkinterface/tv/DemoDesk$EN_MS_UCLEAR_TYPE;

    new-instance v0, Lcom/konka/kkinterface/tv/DemoDesk$EN_MS_UCLEAR_TYPE;

    const-string v1, "E_MS_UCLEAR_ON"

    invoke-direct {v0, v1, v3}, Lcom/konka/kkinterface/tv/DemoDesk$EN_MS_UCLEAR_TYPE;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/konka/kkinterface/tv/DemoDesk$EN_MS_UCLEAR_TYPE;->E_MS_UCLEAR_ON:Lcom/konka/kkinterface/tv/DemoDesk$EN_MS_UCLEAR_TYPE;

    const/4 v0, 0x2

    new-array v0, v0, [Lcom/konka/kkinterface/tv/DemoDesk$EN_MS_UCLEAR_TYPE;

    sget-object v1, Lcom/konka/kkinterface/tv/DemoDesk$EN_MS_UCLEAR_TYPE;->E_MS_UCLEAR_OFF:Lcom/konka/kkinterface/tv/DemoDesk$EN_MS_UCLEAR_TYPE;

    aput-object v1, v0, v2

    sget-object v1, Lcom/konka/kkinterface/tv/DemoDesk$EN_MS_UCLEAR_TYPE;->E_MS_UCLEAR_ON:Lcom/konka/kkinterface/tv/DemoDesk$EN_MS_UCLEAR_TYPE;

    aput-object v1, v0, v3

    sput-object v0, Lcom/konka/kkinterface/tv/DemoDesk$EN_MS_UCLEAR_TYPE;->ENUM$VALUES:[Lcom/konka/kkinterface/tv/DemoDesk$EN_MS_UCLEAR_TYPE;

    return-void
.end method

.method private constructor <init>(Ljava/lang/String;I)V
    .locals 0

    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    return-void
.end method

.method public static valueOf(Ljava/lang/String;)Lcom/konka/kkinterface/tv/DemoDesk$EN_MS_UCLEAR_TYPE;
    .locals 1

    const-class v0, Lcom/konka/kkinterface/tv/DemoDesk$EN_MS_UCLEAR_TYPE;

    invoke-static {v0, p0}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;

    move-result-object v0

    check-cast v0, Lcom/konka/kkinterface/tv/DemoDesk$EN_MS_UCLEAR_TYPE;

    return-object v0
.end method

.method public static values()[Lcom/konka/kkinterface/tv/DemoDesk$EN_MS_UCLEAR_TYPE;
    .locals 4

    const/4 v3, 0x0

    sget-object v0, Lcom/konka/kkinterface/tv/DemoDesk$EN_MS_UCLEAR_TYPE;->ENUM$VALUES:[Lcom/konka/kkinterface/tv/DemoDesk$EN_MS_UCLEAR_TYPE;

    array-length v1, v0

    new-array v2, v1, [Lcom/konka/kkinterface/tv/DemoDesk$EN_MS_UCLEAR_TYPE;

    invoke-static {v0, v3, v2, v3, v1}, Ljava/lang/System;->arraycopy(Ljava/lang/Object;ILjava/lang/Object;II)V

    return-object v2
.end method
