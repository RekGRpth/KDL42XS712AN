.class public Lcom/konka/kkimplements/tv/mstar/DeskAtvPlayerEventListener;
.super Ljava/lang/Object;
.source "DeskAtvPlayerEventListener.java"

# interfaces
.implements Lcom/mstar/android/tvapi/atv/listener/OnAtvPlayerEventListener;


# static fields
.field private static atvEventListener:Lcom/konka/kkimplements/tv/mstar/DeskAtvPlayerEventListener;


# instance fields
.field private m_handler:Landroid/os/Handler;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    const/4 v0, 0x0

    sput-object v0, Lcom/konka/kkimplements/tv/mstar/DeskAtvPlayerEventListener;->atvEventListener:Lcom/konka/kkimplements/tv/mstar/DeskAtvPlayerEventListener;

    return-void
.end method

.method public constructor <init>()V
    .locals 1

    const/4 v0, 0x0

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    iput-object v0, p0, Lcom/konka/kkimplements/tv/mstar/DeskAtvPlayerEventListener;->m_handler:Landroid/os/Handler;

    iput-object v0, p0, Lcom/konka/kkimplements/tv/mstar/DeskAtvPlayerEventListener;->m_handler:Landroid/os/Handler;

    return-void
.end method

.method static getInstance()Lcom/konka/kkimplements/tv/mstar/DeskAtvPlayerEventListener;
    .locals 1

    sget-object v0, Lcom/konka/kkimplements/tv/mstar/DeskAtvPlayerEventListener;->atvEventListener:Lcom/konka/kkimplements/tv/mstar/DeskAtvPlayerEventListener;

    if-nez v0, :cond_0

    new-instance v0, Lcom/konka/kkimplements/tv/mstar/DeskAtvPlayerEventListener;

    invoke-direct {v0}, Lcom/konka/kkimplements/tv/mstar/DeskAtvPlayerEventListener;-><init>()V

    sput-object v0, Lcom/konka/kkimplements/tv/mstar/DeskAtvPlayerEventListener;->atvEventListener:Lcom/konka/kkimplements/tv/mstar/DeskAtvPlayerEventListener;

    :cond_0
    sget-object v0, Lcom/konka/kkimplements/tv/mstar/DeskAtvPlayerEventListener;->atvEventListener:Lcom/konka/kkimplements/tv/mstar/DeskAtvPlayerEventListener;

    return-object v0
.end method


# virtual methods
.method public attachHandler(Landroid/os/Handler;)V
    .locals 0
    .param p1    # Landroid/os/Handler;

    iput-object p1, p0, Lcom/konka/kkimplements/tv/mstar/DeskAtvPlayerEventListener;->m_handler:Landroid/os/Handler;

    return-void
.end method

.method public onAtvAutoTuningScanInfo(ILcom/mstar/android/tvapi/atv/vo/AtvEventScan;)Z
    .locals 4
    .param p1    # I
    .param p2    # Lcom/mstar/android/tvapi/atv/vo/AtvEventScan;

    iget-object v2, p0, Lcom/konka/kkimplements/tv/mstar/DeskAtvPlayerEventListener;->m_handler:Landroid/os/Handler;

    if-nez v2, :cond_0

    const/4 v2, 0x0

    :goto_0
    return v2

    :cond_0
    new-instance v0, Landroid/os/Bundle;

    invoke-direct {v0}, Landroid/os/Bundle;-><init>()V

    iget-object v2, p0, Lcom/konka/kkimplements/tv/mstar/DeskAtvPlayerEventListener;->m_handler:Landroid/os/Handler;

    invoke-virtual {v2}, Landroid/os/Handler;->obtainMessage()Landroid/os/Message;

    move-result-object v1

    sget-object v2, Lcom/mstar/android/tvapi/common/vo/TvOsType$EnumInputSource;->E_INPUT_SOURCE_ATV:Lcom/mstar/android/tvapi/common/vo/TvOsType$EnumInputSource;

    invoke-virtual {v2}, Lcom/mstar/android/tvapi/common/vo/TvOsType$EnumInputSource;->ordinal()I

    move-result v2

    iput v2, v1, Landroid/os/Message;->what:I

    const-string v2, "percent"

    iget-short v3, p2, Lcom/mstar/android/tvapi/atv/vo/AtvEventScan;->percent:S

    invoke-virtual {v0, v2, v3}, Landroid/os/Bundle;->putInt(Ljava/lang/String;I)V

    const-string v2, "frequency"

    iget v3, p2, Lcom/mstar/android/tvapi/atv/vo/AtvEventScan;->frequencyKHz:I

    invoke-virtual {v0, v2, v3}, Landroid/os/Bundle;->putInt(Ljava/lang/String;I)V

    const-string v2, "scanNum"

    iget-short v3, p2, Lcom/mstar/android/tvapi/atv/vo/AtvEventScan;->scannedChannelNum:S

    invoke-virtual {v0, v2, v3}, Landroid/os/Bundle;->putInt(Ljava/lang/String;I)V

    invoke-virtual {v1, v0}, Landroid/os/Message;->setData(Landroid/os/Bundle;)V

    iget-object v2, p0, Lcom/konka/kkimplements/tv/mstar/DeskAtvPlayerEventListener;->m_handler:Landroid/os/Handler;

    invoke-virtual {v2, v1}, Landroid/os/Handler;->sendMessage(Landroid/os/Message;)Z

    const/4 v2, 0x1

    goto :goto_0
.end method

.method public onAtvManualTuningScanInfo(ILcom/mstar/android/tvapi/atv/vo/AtvEventScan;)Z
    .locals 4
    .param p1    # I
    .param p2    # Lcom/mstar/android/tvapi/atv/vo/AtvEventScan;

    iget-object v2, p0, Lcom/konka/kkimplements/tv/mstar/DeskAtvPlayerEventListener;->m_handler:Landroid/os/Handler;

    if-nez v2, :cond_0

    const/4 v2, 0x0

    :goto_0
    return v2

    :cond_0
    new-instance v0, Landroid/os/Bundle;

    invoke-direct {v0}, Landroid/os/Bundle;-><init>()V

    iget-object v2, p0, Lcom/konka/kkimplements/tv/mstar/DeskAtvPlayerEventListener;->m_handler:Landroid/os/Handler;

    invoke-virtual {v2}, Landroid/os/Handler;->obtainMessage()Landroid/os/Message;

    move-result-object v1

    sget-object v2, Lcom/mstar/android/tvapi/common/vo/TvOsType$EnumInputSource;->E_INPUT_SOURCE_ATV:Lcom/mstar/android/tvapi/common/vo/TvOsType$EnumInputSource;

    invoke-virtual {v2}, Lcom/mstar/android/tvapi/common/vo/TvOsType$EnumInputSource;->ordinal()I

    move-result v2

    iput v2, v1, Landroid/os/Message;->what:I

    const-string v2, "percent"

    iget-short v3, p2, Lcom/mstar/android/tvapi/atv/vo/AtvEventScan;->percent:S

    invoke-virtual {v0, v2, v3}, Landroid/os/Bundle;->putInt(Ljava/lang/String;I)V

    const-string v2, "frequency"

    iget v3, p2, Lcom/mstar/android/tvapi/atv/vo/AtvEventScan;->frequencyKHz:I

    invoke-virtual {v0, v2, v3}, Landroid/os/Bundle;->putInt(Ljava/lang/String;I)V

    const-string v2, "scanNum"

    iget-short v3, p2, Lcom/mstar/android/tvapi/atv/vo/AtvEventScan;->scannedChannelNum:S

    invoke-virtual {v0, v2, v3}, Landroid/os/Bundle;->putInt(Ljava/lang/String;I)V

    invoke-virtual {v1, v0}, Landroid/os/Message;->setData(Landroid/os/Bundle;)V

    iget-object v2, p0, Lcom/konka/kkimplements/tv/mstar/DeskAtvPlayerEventListener;->m_handler:Landroid/os/Handler;

    invoke-virtual {v2, v1}, Landroid/os/Handler;->sendMessage(Landroid/os/Message;)Z

    const/4 v2, 0x1

    goto :goto_0
.end method

.method public onAtvProgramInfoReady(I)Z
    .locals 4
    .param p1    # I

    const-string v2, "TvApp"

    const-string v3, "onAtvProgramInfoReady"

    invoke-static {v2, v3}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    iget-object v2, p0, Lcom/konka/kkimplements/tv/mstar/DeskAtvPlayerEventListener;->m_handler:Landroid/os/Handler;

    if-nez v2, :cond_0

    const/4 v2, 0x0

    :goto_0
    return v2

    :cond_0
    new-instance v0, Landroid/os/Bundle;

    invoke-direct {v0}, Landroid/os/Bundle;-><init>()V

    iget-object v2, p0, Lcom/konka/kkimplements/tv/mstar/DeskAtvPlayerEventListener;->m_handler:Landroid/os/Handler;

    invoke-virtual {v2}, Landroid/os/Handler;->obtainMessage()Landroid/os/Message;

    move-result-object v1

    const/16 v2, 0x41

    iput v2, v1, Landroid/os/Message;->what:I

    const-string v2, "CmdIndex"

    sget-object v3, Lcom/konka/kkinterface/tv/CommonDesk$EnumDeskEvent;->EV_ATV_PROGRAM_INFO_READY:Lcom/konka/kkinterface/tv/CommonDesk$EnumDeskEvent;

    invoke-virtual {v3}, Lcom/konka/kkinterface/tv/CommonDesk$EnumDeskEvent;->ordinal()I

    move-result v3

    invoke-virtual {v0, v2, v3}, Landroid/os/Bundle;->putInt(Ljava/lang/String;I)V

    iget-object v2, p0, Lcom/konka/kkimplements/tv/mstar/DeskAtvPlayerEventListener;->m_handler:Landroid/os/Handler;

    invoke-virtual {v2, v1}, Landroid/os/Handler;->sendMessage(Landroid/os/Message;)Z

    const/4 v2, 0x1

    goto :goto_0
.end method

.method public onSignalLock(I)Z
    .locals 5
    .param p1    # I

    const/4 v4, 0x0

    const-string v2, "TvApp"

    const-string v3, "onSignalLock in DeskAtvPlayerEventListener"

    invoke-static {v2, v3}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    iget-object v2, p0, Lcom/konka/kkimplements/tv/mstar/DeskAtvPlayerEventListener;->m_handler:Landroid/os/Handler;

    if-nez v2, :cond_0

    :goto_0
    return v4

    :cond_0
    iget-object v2, p0, Lcom/konka/kkimplements/tv/mstar/DeskAtvPlayerEventListener;->m_handler:Landroid/os/Handler;

    invoke-virtual {v2}, Landroid/os/Handler;->obtainMessage()Landroid/os/Message;

    move-result-object v1

    sget-object v2, Lcom/konka/kkinterface/tv/CommonDesk$EnumDeskEvent;->EV_SIGNAL_LOCK:Lcom/konka/kkinterface/tv/CommonDesk$EnumDeskEvent;

    invoke-virtual {v2}, Lcom/konka/kkinterface/tv/CommonDesk$EnumDeskEvent;->ordinal()I

    move-result v2

    iput v2, v1, Landroid/os/Message;->what:I

    new-instance v0, Landroid/os/Bundle;

    invoke-direct {v0}, Landroid/os/Bundle;-><init>()V

    const-string v2, "MsgSource"

    const-string v3, "DeskAtvPlayerEventListener"

    invoke-virtual {v0, v2, v3}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V

    invoke-virtual {v1, v0}, Landroid/os/Message;->setData(Landroid/os/Bundle;)V

    iget-object v2, p0, Lcom/konka/kkimplements/tv/mstar/DeskAtvPlayerEventListener;->m_handler:Landroid/os/Handler;

    invoke-virtual {v2, v1}, Landroid/os/Handler;->sendMessage(Landroid/os/Message;)Z

    goto :goto_0
.end method

.method public onSignalUnLock(I)Z
    .locals 5
    .param p1    # I

    const/4 v4, 0x0

    const-string v2, "TvApp"

    const-string v3, "onSignalUnLock in DeskAtvPlayerEventListener"

    invoke-static {v2, v3}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    iget-object v2, p0, Lcom/konka/kkimplements/tv/mstar/DeskAtvPlayerEventListener;->m_handler:Landroid/os/Handler;

    if-nez v2, :cond_0

    :goto_0
    return v4

    :cond_0
    iget-object v2, p0, Lcom/konka/kkimplements/tv/mstar/DeskAtvPlayerEventListener;->m_handler:Landroid/os/Handler;

    invoke-virtual {v2}, Landroid/os/Handler;->obtainMessage()Landroid/os/Message;

    move-result-object v1

    sget-object v2, Lcom/konka/kkinterface/tv/CommonDesk$EnumDeskEvent;->EV_SIGNAL_UNLOCK:Lcom/konka/kkinterface/tv/CommonDesk$EnumDeskEvent;

    invoke-virtual {v2}, Lcom/konka/kkinterface/tv/CommonDesk$EnumDeskEvent;->ordinal()I

    move-result v2

    iput v2, v1, Landroid/os/Message;->what:I

    new-instance v0, Landroid/os/Bundle;

    invoke-direct {v0}, Landroid/os/Bundle;-><init>()V

    const-string v2, "MsgSource"

    const-string v3, "DeskAtvPlayerEventListener"

    invoke-virtual {v0, v2, v3}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V

    invoke-virtual {v1, v0}, Landroid/os/Message;->setData(Landroid/os/Bundle;)V

    iget-object v2, p0, Lcom/konka/kkimplements/tv/mstar/DeskAtvPlayerEventListener;->m_handler:Landroid/os/Handler;

    invoke-virtual {v2, v1}, Landroid/os/Handler;->sendMessage(Landroid/os/Message;)Z

    goto :goto_0
.end method

.method public releaseHandler()V
    .locals 1

    const/4 v0, 0x0

    iput-object v0, p0, Lcom/konka/kkimplements/tv/mstar/DeskAtvPlayerEventListener;->m_handler:Landroid/os/Handler;

    return-void
.end method
