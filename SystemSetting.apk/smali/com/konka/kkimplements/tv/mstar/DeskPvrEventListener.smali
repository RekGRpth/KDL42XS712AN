.class public Lcom/konka/kkimplements/tv/mstar/DeskPvrEventListener;
.super Ljava/lang/Object;
.source "DeskPvrEventListener.java"

# interfaces
.implements Lcom/mstar/android/tvapi/common/PvrManager$OnPvrEventListener;


# instance fields
.field private m_handler:Landroid/os/Handler;


# direct methods
.method public constructor <init>()V
    .locals 1

    const/4 v0, 0x0

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    iput-object v0, p0, Lcom/konka/kkimplements/tv/mstar/DeskPvrEventListener;->m_handler:Landroid/os/Handler;

    iput-object v0, p0, Lcom/konka/kkimplements/tv/mstar/DeskPvrEventListener;->m_handler:Landroid/os/Handler;

    return-void
.end method


# virtual methods
.method public attachHandler(Landroid/os/Handler;)V
    .locals 0
    .param p1    # Landroid/os/Handler;

    iput-object p1, p0, Lcom/konka/kkimplements/tv/mstar/DeskPvrEventListener;->m_handler:Landroid/os/Handler;

    return-void
.end method

.method public onPvrNotifyFormatFinished(Lcom/mstar/android/tvapi/common/PvrManager;III)Z
    .locals 4
    .param p1    # Lcom/mstar/android/tvapi/common/PvrManager;
    .param p2    # I
    .param p3    # I
    .param p4    # I

    const/4 v3, 0x0

    iget-object v2, p0, Lcom/konka/kkimplements/tv/mstar/DeskPvrEventListener;->m_handler:Landroid/os/Handler;

    if-nez v2, :cond_0

    :goto_0
    return v3

    :cond_0
    new-instance v0, Landroid/os/Bundle;

    invoke-direct {v0}, Landroid/os/Bundle;-><init>()V

    iget-object v2, p0, Lcom/konka/kkimplements/tv/mstar/DeskPvrEventListener;->m_handler:Landroid/os/Handler;

    invoke-virtual {v2}, Landroid/os/Handler;->obtainMessage()Landroid/os/Message;

    move-result-object v1

    iput p2, v1, Landroid/os/Message;->what:I

    const-string v2, "usbInserted"

    invoke-virtual {v0, v2, p4}, Landroid/os/Bundle;->putInt(Ljava/lang/String;I)V

    invoke-virtual {v1, v0}, Landroid/os/Message;->setData(Landroid/os/Bundle;)V

    iget-object v2, p0, Lcom/konka/kkimplements/tv/mstar/DeskPvrEventListener;->m_handler:Landroid/os/Handler;

    invoke-virtual {v2, v1}, Landroid/os/Handler;->sendMessage(Landroid/os/Message;)Z

    goto :goto_0
.end method

.method public onPvrNotifyPlaybackBegin(Lcom/mstar/android/tvapi/common/PvrManager;III)Z
    .locals 4
    .param p1    # Lcom/mstar/android/tvapi/common/PvrManager;
    .param p2    # I
    .param p3    # I
    .param p4    # I

    const/4 v3, 0x0

    iget-object v2, p0, Lcom/konka/kkimplements/tv/mstar/DeskPvrEventListener;->m_handler:Landroid/os/Handler;

    if-nez v2, :cond_0

    :goto_0
    return v3

    :cond_0
    new-instance v0, Landroid/os/Bundle;

    invoke-direct {v0}, Landroid/os/Bundle;-><init>()V

    iget-object v2, p0, Lcom/konka/kkimplements/tv/mstar/DeskPvrEventListener;->m_handler:Landroid/os/Handler;

    invoke-virtual {v2}, Landroid/os/Handler;->obtainMessage()Landroid/os/Message;

    move-result-object v1

    iput p2, v1, Landroid/os/Message;->what:I

    const-string v2, "playbackbegin"

    invoke-virtual {v0, v2, p3}, Landroid/os/Bundle;->putInt(Ljava/lang/String;I)V

    invoke-virtual {v1, v0}, Landroid/os/Message;->setData(Landroid/os/Bundle;)V

    iget-object v2, p0, Lcom/konka/kkimplements/tv/mstar/DeskPvrEventListener;->m_handler:Landroid/os/Handler;

    invoke-virtual {v2, v1}, Landroid/os/Handler;->sendMessage(Landroid/os/Message;)Z

    goto :goto_0
.end method

.method public onPvrNotifyPlaybackStop(Lcom/mstar/android/tvapi/common/PvrManager;III)Z
    .locals 4
    .param p1    # Lcom/mstar/android/tvapi/common/PvrManager;
    .param p2    # I
    .param p3    # I
    .param p4    # I

    const/4 v3, 0x0

    iget-object v2, p0, Lcom/konka/kkimplements/tv/mstar/DeskPvrEventListener;->m_handler:Landroid/os/Handler;

    if-nez v2, :cond_0

    :goto_0
    return v3

    :cond_0
    new-instance v0, Landroid/os/Bundle;

    invoke-direct {v0}, Landroid/os/Bundle;-><init>()V

    iget-object v2, p0, Lcom/konka/kkimplements/tv/mstar/DeskPvrEventListener;->m_handler:Landroid/os/Handler;

    invoke-virtual {v2}, Landroid/os/Handler;->obtainMessage()Landroid/os/Message;

    move-result-object v1

    iput p2, v1, Landroid/os/Message;->what:I

    const-string v2, "playbackstop"

    invoke-virtual {v0, v2, p3}, Landroid/os/Bundle;->putInt(Ljava/lang/String;I)V

    invoke-virtual {v1, v0}, Landroid/os/Message;->setData(Landroid/os/Bundle;)V

    iget-object v2, p0, Lcom/konka/kkimplements/tv/mstar/DeskPvrEventListener;->m_handler:Landroid/os/Handler;

    invoke-virtual {v2, v1}, Landroid/os/Handler;->sendMessage(Landroid/os/Message;)Z

    goto :goto_0
.end method

.method public onPvrNotifyUsbInserted(Lcom/mstar/android/tvapi/common/PvrManager;III)Z
    .locals 4
    .param p1    # Lcom/mstar/android/tvapi/common/PvrManager;
    .param p2    # I
    .param p3    # I
    .param p4    # I

    const/4 v3, 0x0

    iget-object v2, p0, Lcom/konka/kkimplements/tv/mstar/DeskPvrEventListener;->m_handler:Landroid/os/Handler;

    if-nez v2, :cond_0

    :goto_0
    return v3

    :cond_0
    new-instance v0, Landroid/os/Bundle;

    invoke-direct {v0}, Landroid/os/Bundle;-><init>()V

    iget-object v2, p0, Lcom/konka/kkimplements/tv/mstar/DeskPvrEventListener;->m_handler:Landroid/os/Handler;

    invoke-virtual {v2}, Landroid/os/Handler;->obtainMessage()Landroid/os/Message;

    move-result-object v1

    iput p2, v1, Landroid/os/Message;->what:I

    const-string v2, "usbInserted"

    invoke-virtual {v0, v2, p4}, Landroid/os/Bundle;->putInt(Ljava/lang/String;I)V

    invoke-virtual {v1, v0}, Landroid/os/Message;->setData(Landroid/os/Bundle;)V

    iget-object v2, p0, Lcom/konka/kkimplements/tv/mstar/DeskPvrEventListener;->m_handler:Landroid/os/Handler;

    invoke-virtual {v2, v1}, Landroid/os/Handler;->sendMessage(Landroid/os/Message;)Z

    goto :goto_0
.end method

.method public onPvrNotifyUsbRemoved(Lcom/mstar/android/tvapi/common/PvrManager;III)Z
    .locals 4
    .param p1    # Lcom/mstar/android/tvapi/common/PvrManager;
    .param p2    # I
    .param p3    # I
    .param p4    # I

    const/4 v3, 0x0

    iget-object v2, p0, Lcom/konka/kkimplements/tv/mstar/DeskPvrEventListener;->m_handler:Landroid/os/Handler;

    if-nez v2, :cond_0

    :goto_0
    return v3

    :cond_0
    new-instance v0, Landroid/os/Bundle;

    invoke-direct {v0}, Landroid/os/Bundle;-><init>()V

    iget-object v2, p0, Lcom/konka/kkimplements/tv/mstar/DeskPvrEventListener;->m_handler:Landroid/os/Handler;

    invoke-virtual {v2}, Landroid/os/Handler;->obtainMessage()Landroid/os/Message;

    move-result-object v1

    iput p2, v1, Landroid/os/Message;->what:I

    const-string v2, "usbInserted"

    invoke-virtual {v0, v2, p4}, Landroid/os/Bundle;->putInt(Ljava/lang/String;I)V

    invoke-virtual {v1, v0}, Landroid/os/Message;->setData(Landroid/os/Bundle;)V

    iget-object v2, p0, Lcom/konka/kkimplements/tv/mstar/DeskPvrEventListener;->m_handler:Landroid/os/Handler;

    invoke-virtual {v2, v1}, Landroid/os/Handler;->sendMessage(Landroid/os/Message;)Z

    goto :goto_0
.end method

.method public releaseHandler()V
    .locals 1

    const/4 v0, 0x0

    iput-object v0, p0, Lcom/konka/kkimplements/tv/mstar/DeskPvrEventListener;->m_handler:Landroid/os/Handler;

    return-void
.end method
