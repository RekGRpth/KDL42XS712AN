.class public Lcom/konka/utilities/StorageInfor;
.super Ljava/lang/Object;
.source "StorageInfor.java"


# instance fields
.field public mFreeStrorage:J

.field public mTotalStorage:J

.field public mUsedStorage:J


# direct methods
.method constructor <init>(JJJ)V
    .locals 0
    .param p1    # J
    .param p3    # J
    .param p5    # J

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    iput-wide p3, p0, Lcom/konka/utilities/StorageInfor;->mFreeStrorage:J

    iput-wide p1, p0, Lcom/konka/utilities/StorageInfor;->mTotalStorage:J

    iput-wide p5, p0, Lcom/konka/utilities/StorageInfor;->mUsedStorage:J

    return-void
.end method
