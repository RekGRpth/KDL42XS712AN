.class public final enum Lcom/konka/kkinterface/tv/DataBaseDesk$EN_MS_POWERON_MUSIC;
.super Ljava/lang/Enum;
.source "DataBaseDesk.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/konka/kkinterface/tv/DataBaseDesk;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x4019
    name = "EN_MS_POWERON_MUSIC"
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Enum",
        "<",
        "Lcom/konka/kkinterface/tv/DataBaseDesk$EN_MS_POWERON_MUSIC;",
        ">;"
    }
.end annotation


# static fields
.field private static final synthetic ENUM$VALUES:[Lcom/konka/kkinterface/tv/DataBaseDesk$EN_MS_POWERON_MUSIC;

.field public static final enum MS_POWERON_MUSIC_DEFAULT:Lcom/konka/kkinterface/tv/DataBaseDesk$EN_MS_POWERON_MUSIC;

.field public static final enum MS_POWERON_MUSIC_NUM:Lcom/konka/kkinterface/tv/DataBaseDesk$EN_MS_POWERON_MUSIC;

.field public static final enum MS_POWERON_MUSIC_OFF:Lcom/konka/kkinterface/tv/DataBaseDesk$EN_MS_POWERON_MUSIC;

.field public static final enum MS_POWERON_MUSIC_USER:Lcom/konka/kkinterface/tv/DataBaseDesk$EN_MS_POWERON_MUSIC;


# direct methods
.method static constructor <clinit>()V
    .locals 6

    const/4 v5, 0x3

    const/4 v4, 0x2

    const/4 v3, 0x1

    const/4 v2, 0x0

    new-instance v0, Lcom/konka/kkinterface/tv/DataBaseDesk$EN_MS_POWERON_MUSIC;

    const-string v1, "MS_POWERON_MUSIC_OFF"

    invoke-direct {v0, v1, v2}, Lcom/konka/kkinterface/tv/DataBaseDesk$EN_MS_POWERON_MUSIC;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/konka/kkinterface/tv/DataBaseDesk$EN_MS_POWERON_MUSIC;->MS_POWERON_MUSIC_OFF:Lcom/konka/kkinterface/tv/DataBaseDesk$EN_MS_POWERON_MUSIC;

    new-instance v0, Lcom/konka/kkinterface/tv/DataBaseDesk$EN_MS_POWERON_MUSIC;

    const-string v1, "MS_POWERON_MUSIC_DEFAULT"

    invoke-direct {v0, v1, v3}, Lcom/konka/kkinterface/tv/DataBaseDesk$EN_MS_POWERON_MUSIC;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/konka/kkinterface/tv/DataBaseDesk$EN_MS_POWERON_MUSIC;->MS_POWERON_MUSIC_DEFAULT:Lcom/konka/kkinterface/tv/DataBaseDesk$EN_MS_POWERON_MUSIC;

    new-instance v0, Lcom/konka/kkinterface/tv/DataBaseDesk$EN_MS_POWERON_MUSIC;

    const-string v1, "MS_POWERON_MUSIC_USER"

    invoke-direct {v0, v1, v4}, Lcom/konka/kkinterface/tv/DataBaseDesk$EN_MS_POWERON_MUSIC;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/konka/kkinterface/tv/DataBaseDesk$EN_MS_POWERON_MUSIC;->MS_POWERON_MUSIC_USER:Lcom/konka/kkinterface/tv/DataBaseDesk$EN_MS_POWERON_MUSIC;

    new-instance v0, Lcom/konka/kkinterface/tv/DataBaseDesk$EN_MS_POWERON_MUSIC;

    const-string v1, "MS_POWERON_MUSIC_NUM"

    invoke-direct {v0, v1, v5}, Lcom/konka/kkinterface/tv/DataBaseDesk$EN_MS_POWERON_MUSIC;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/konka/kkinterface/tv/DataBaseDesk$EN_MS_POWERON_MUSIC;->MS_POWERON_MUSIC_NUM:Lcom/konka/kkinterface/tv/DataBaseDesk$EN_MS_POWERON_MUSIC;

    const/4 v0, 0x4

    new-array v0, v0, [Lcom/konka/kkinterface/tv/DataBaseDesk$EN_MS_POWERON_MUSIC;

    sget-object v1, Lcom/konka/kkinterface/tv/DataBaseDesk$EN_MS_POWERON_MUSIC;->MS_POWERON_MUSIC_OFF:Lcom/konka/kkinterface/tv/DataBaseDesk$EN_MS_POWERON_MUSIC;

    aput-object v1, v0, v2

    sget-object v1, Lcom/konka/kkinterface/tv/DataBaseDesk$EN_MS_POWERON_MUSIC;->MS_POWERON_MUSIC_DEFAULT:Lcom/konka/kkinterface/tv/DataBaseDesk$EN_MS_POWERON_MUSIC;

    aput-object v1, v0, v3

    sget-object v1, Lcom/konka/kkinterface/tv/DataBaseDesk$EN_MS_POWERON_MUSIC;->MS_POWERON_MUSIC_USER:Lcom/konka/kkinterface/tv/DataBaseDesk$EN_MS_POWERON_MUSIC;

    aput-object v1, v0, v4

    sget-object v1, Lcom/konka/kkinterface/tv/DataBaseDesk$EN_MS_POWERON_MUSIC;->MS_POWERON_MUSIC_NUM:Lcom/konka/kkinterface/tv/DataBaseDesk$EN_MS_POWERON_MUSIC;

    aput-object v1, v0, v5

    sput-object v0, Lcom/konka/kkinterface/tv/DataBaseDesk$EN_MS_POWERON_MUSIC;->ENUM$VALUES:[Lcom/konka/kkinterface/tv/DataBaseDesk$EN_MS_POWERON_MUSIC;

    return-void
.end method

.method private constructor <init>(Ljava/lang/String;I)V
    .locals 0

    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    return-void
.end method

.method public static valueOf(Ljava/lang/String;)Lcom/konka/kkinterface/tv/DataBaseDesk$EN_MS_POWERON_MUSIC;
    .locals 1

    const-class v0, Lcom/konka/kkinterface/tv/DataBaseDesk$EN_MS_POWERON_MUSIC;

    invoke-static {v0, p0}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;

    move-result-object v0

    check-cast v0, Lcom/konka/kkinterface/tv/DataBaseDesk$EN_MS_POWERON_MUSIC;

    return-object v0
.end method

.method public static values()[Lcom/konka/kkinterface/tv/DataBaseDesk$EN_MS_POWERON_MUSIC;
    .locals 4

    const/4 v3, 0x0

    sget-object v0, Lcom/konka/kkinterface/tv/DataBaseDesk$EN_MS_POWERON_MUSIC;->ENUM$VALUES:[Lcom/konka/kkinterface/tv/DataBaseDesk$EN_MS_POWERON_MUSIC;

    array-length v1, v0

    new-array v2, v1, [Lcom/konka/kkinterface/tv/DataBaseDesk$EN_MS_POWERON_MUSIC;

    invoke-static {v0, v3, v2, v3, v1}, Ljava/lang/System;->arraycopy(Ljava/lang/Object;ILjava/lang/Object;II)V

    return-object v2
.end method
