.class public final enum Lcom/konka/kkinterface/tv/CommonDesk$MSRV_SIGNALPROC_SYNC_STATUS;
.super Ljava/lang/Enum;
.source "CommonDesk.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/konka/kkinterface/tv/CommonDesk;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x4019
    name = "MSRV_SIGNALPROC_SYNC_STATUS"
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Enum",
        "<",
        "Lcom/konka/kkinterface/tv/CommonDesk$MSRV_SIGNALPROC_SYNC_STATUS;",
        ">;"
    }
.end annotation


# static fields
.field private static final synthetic ENUM$VALUES:[Lcom/konka/kkinterface/tv/CommonDesk$MSRV_SIGNALPROC_SYNC_STATUS;

.field public static final enum MSRV_SIGNALPROC_AUTO_ADJUST:Lcom/konka/kkinterface/tv/CommonDesk$MSRV_SIGNALPROC_SYNC_STATUS;

.field public static final enum MSRV_SIGNALPROC_MAX:Lcom/konka/kkinterface/tv/CommonDesk$MSRV_SIGNALPROC_SYNC_STATUS;

.field public static final enum MSRV_SIGNALPROC_NOSYNC:Lcom/konka/kkinterface/tv/CommonDesk$MSRV_SIGNALPROC_SYNC_STATUS;

.field public static final enum MSRV_SIGNALPROC_STABLE_SUPPORT_MODE:Lcom/konka/kkinterface/tv/CommonDesk$MSRV_SIGNALPROC_SYNC_STATUS;

.field public static final enum MSRV_SIGNALPROC_STABLE_UN_SUPPORT_MODE:Lcom/konka/kkinterface/tv/CommonDesk$MSRV_SIGNALPROC_SYNC_STATUS;

.field public static final enum MSRV_SIGNALPROC_UNSTABLE:Lcom/konka/kkinterface/tv/CommonDesk$MSRV_SIGNALPROC_SYNC_STATUS;


# direct methods
.method static constructor <clinit>()V
    .locals 8

    const/4 v7, 0x4

    const/4 v6, 0x3

    const/4 v5, 0x2

    const/4 v4, 0x1

    const/4 v3, 0x0

    new-instance v0, Lcom/konka/kkinterface/tv/CommonDesk$MSRV_SIGNALPROC_SYNC_STATUS;

    const-string v1, "MSRV_SIGNALPROC_NOSYNC"

    invoke-direct {v0, v1, v3}, Lcom/konka/kkinterface/tv/CommonDesk$MSRV_SIGNALPROC_SYNC_STATUS;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/konka/kkinterface/tv/CommonDesk$MSRV_SIGNALPROC_SYNC_STATUS;->MSRV_SIGNALPROC_NOSYNC:Lcom/konka/kkinterface/tv/CommonDesk$MSRV_SIGNALPROC_SYNC_STATUS;

    new-instance v0, Lcom/konka/kkinterface/tv/CommonDesk$MSRV_SIGNALPROC_SYNC_STATUS;

    const-string v1, "MSRV_SIGNALPROC_STABLE_SUPPORT_MODE"

    invoke-direct {v0, v1, v4}, Lcom/konka/kkinterface/tv/CommonDesk$MSRV_SIGNALPROC_SYNC_STATUS;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/konka/kkinterface/tv/CommonDesk$MSRV_SIGNALPROC_SYNC_STATUS;->MSRV_SIGNALPROC_STABLE_SUPPORT_MODE:Lcom/konka/kkinterface/tv/CommonDesk$MSRV_SIGNALPROC_SYNC_STATUS;

    new-instance v0, Lcom/konka/kkinterface/tv/CommonDesk$MSRV_SIGNALPROC_SYNC_STATUS;

    const-string v1, "MSRV_SIGNALPROC_STABLE_UN_SUPPORT_MODE"

    invoke-direct {v0, v1, v5}, Lcom/konka/kkinterface/tv/CommonDesk$MSRV_SIGNALPROC_SYNC_STATUS;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/konka/kkinterface/tv/CommonDesk$MSRV_SIGNALPROC_SYNC_STATUS;->MSRV_SIGNALPROC_STABLE_UN_SUPPORT_MODE:Lcom/konka/kkinterface/tv/CommonDesk$MSRV_SIGNALPROC_SYNC_STATUS;

    new-instance v0, Lcom/konka/kkinterface/tv/CommonDesk$MSRV_SIGNALPROC_SYNC_STATUS;

    const-string v1, "MSRV_SIGNALPROC_UNSTABLE"

    invoke-direct {v0, v1, v6}, Lcom/konka/kkinterface/tv/CommonDesk$MSRV_SIGNALPROC_SYNC_STATUS;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/konka/kkinterface/tv/CommonDesk$MSRV_SIGNALPROC_SYNC_STATUS;->MSRV_SIGNALPROC_UNSTABLE:Lcom/konka/kkinterface/tv/CommonDesk$MSRV_SIGNALPROC_SYNC_STATUS;

    new-instance v0, Lcom/konka/kkinterface/tv/CommonDesk$MSRV_SIGNALPROC_SYNC_STATUS;

    const-string v1, "MSRV_SIGNALPROC_AUTO_ADJUST"

    invoke-direct {v0, v1, v7}, Lcom/konka/kkinterface/tv/CommonDesk$MSRV_SIGNALPROC_SYNC_STATUS;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/konka/kkinterface/tv/CommonDesk$MSRV_SIGNALPROC_SYNC_STATUS;->MSRV_SIGNALPROC_AUTO_ADJUST:Lcom/konka/kkinterface/tv/CommonDesk$MSRV_SIGNALPROC_SYNC_STATUS;

    new-instance v0, Lcom/konka/kkinterface/tv/CommonDesk$MSRV_SIGNALPROC_SYNC_STATUS;

    const-string v1, "MSRV_SIGNALPROC_MAX"

    const/4 v2, 0x5

    invoke-direct {v0, v1, v2}, Lcom/konka/kkinterface/tv/CommonDesk$MSRV_SIGNALPROC_SYNC_STATUS;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/konka/kkinterface/tv/CommonDesk$MSRV_SIGNALPROC_SYNC_STATUS;->MSRV_SIGNALPROC_MAX:Lcom/konka/kkinterface/tv/CommonDesk$MSRV_SIGNALPROC_SYNC_STATUS;

    const/4 v0, 0x6

    new-array v0, v0, [Lcom/konka/kkinterface/tv/CommonDesk$MSRV_SIGNALPROC_SYNC_STATUS;

    sget-object v1, Lcom/konka/kkinterface/tv/CommonDesk$MSRV_SIGNALPROC_SYNC_STATUS;->MSRV_SIGNALPROC_NOSYNC:Lcom/konka/kkinterface/tv/CommonDesk$MSRV_SIGNALPROC_SYNC_STATUS;

    aput-object v1, v0, v3

    sget-object v1, Lcom/konka/kkinterface/tv/CommonDesk$MSRV_SIGNALPROC_SYNC_STATUS;->MSRV_SIGNALPROC_STABLE_SUPPORT_MODE:Lcom/konka/kkinterface/tv/CommonDesk$MSRV_SIGNALPROC_SYNC_STATUS;

    aput-object v1, v0, v4

    sget-object v1, Lcom/konka/kkinterface/tv/CommonDesk$MSRV_SIGNALPROC_SYNC_STATUS;->MSRV_SIGNALPROC_STABLE_UN_SUPPORT_MODE:Lcom/konka/kkinterface/tv/CommonDesk$MSRV_SIGNALPROC_SYNC_STATUS;

    aput-object v1, v0, v5

    sget-object v1, Lcom/konka/kkinterface/tv/CommonDesk$MSRV_SIGNALPROC_SYNC_STATUS;->MSRV_SIGNALPROC_UNSTABLE:Lcom/konka/kkinterface/tv/CommonDesk$MSRV_SIGNALPROC_SYNC_STATUS;

    aput-object v1, v0, v6

    sget-object v1, Lcom/konka/kkinterface/tv/CommonDesk$MSRV_SIGNALPROC_SYNC_STATUS;->MSRV_SIGNALPROC_AUTO_ADJUST:Lcom/konka/kkinterface/tv/CommonDesk$MSRV_SIGNALPROC_SYNC_STATUS;

    aput-object v1, v0, v7

    const/4 v1, 0x5

    sget-object v2, Lcom/konka/kkinterface/tv/CommonDesk$MSRV_SIGNALPROC_SYNC_STATUS;->MSRV_SIGNALPROC_MAX:Lcom/konka/kkinterface/tv/CommonDesk$MSRV_SIGNALPROC_SYNC_STATUS;

    aput-object v2, v0, v1

    sput-object v0, Lcom/konka/kkinterface/tv/CommonDesk$MSRV_SIGNALPROC_SYNC_STATUS;->ENUM$VALUES:[Lcom/konka/kkinterface/tv/CommonDesk$MSRV_SIGNALPROC_SYNC_STATUS;

    return-void
.end method

.method private constructor <init>(Ljava/lang/String;I)V
    .locals 0

    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    return-void
.end method

.method public static valueOf(Ljava/lang/String;)Lcom/konka/kkinterface/tv/CommonDesk$MSRV_SIGNALPROC_SYNC_STATUS;
    .locals 1

    const-class v0, Lcom/konka/kkinterface/tv/CommonDesk$MSRV_SIGNALPROC_SYNC_STATUS;

    invoke-static {v0, p0}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;

    move-result-object v0

    check-cast v0, Lcom/konka/kkinterface/tv/CommonDesk$MSRV_SIGNALPROC_SYNC_STATUS;

    return-object v0
.end method

.method public static values()[Lcom/konka/kkinterface/tv/CommonDesk$MSRV_SIGNALPROC_SYNC_STATUS;
    .locals 4

    const/4 v3, 0x0

    sget-object v0, Lcom/konka/kkinterface/tv/CommonDesk$MSRV_SIGNALPROC_SYNC_STATUS;->ENUM$VALUES:[Lcom/konka/kkinterface/tv/CommonDesk$MSRV_SIGNALPROC_SYNC_STATUS;

    array-length v1, v0

    new-array v2, v1, [Lcom/konka/kkinterface/tv/CommonDesk$MSRV_SIGNALPROC_SYNC_STATUS;

    invoke-static {v0, v3, v2, v3, v1}, Ljava/lang/System;->arraycopy(Ljava/lang/Object;ILjava/lang/Object;II)V

    return-object v2
.end method
