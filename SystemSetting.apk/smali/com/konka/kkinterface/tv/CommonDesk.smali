.class public interface abstract Lcom/konka/kkinterface/tv/CommonDesk;
.super Ljava/lang/Object;
.source "CommonDesk.java"

# interfaces
.implements Lcom/konka/kkinterface/tv/BaseDesk;


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/konka/kkinterface/tv/CommonDesk$ApplicationsInfo;,
        Lcom/konka/kkinterface/tv/CommonDesk$AudioInfo;,
        Lcom/konka/kkinterface/tv/CommonDesk$BatteryInfo;,
        Lcom/konka/kkinterface/tv/CommonDesk$CPUInfo;,
        Lcom/konka/kkinterface/tv/CommonDesk$CiCardInfo;,
        Lcom/konka/kkinterface/tv/CommonDesk$CustomerInfo;,
        Lcom/konka/kkinterface/tv/CommonDesk$EN_SCAN_TYPE;,
        Lcom/konka/kkinterface/tv/CommonDesk$EnumDeskEvent;,
        Lcom/konka/kkinterface/tv/CommonDesk$EnumScreenMode;,
        Lcom/konka/kkinterface/tv/CommonDesk$EnumSignalProgSyncStatus;,
        Lcom/konka/kkinterface/tv/CommonDesk$ExternalStorageInfo;,
        Lcom/konka/kkinterface/tv/CommonDesk$InputSourceInfo;,
        Lcom/konka/kkinterface/tv/CommonDesk$LocalDimmingInfo;,
        Lcom/konka/kkinterface/tv/CommonDesk$MSRV_SIGNALPROC_SYNC_STATUS;,
        Lcom/konka/kkinterface/tv/CommonDesk$Panel4K2K;,
        Lcom/konka/kkinterface/tv/CommonDesk$PanelSupportInfo;,
        Lcom/konka/kkinterface/tv/CommonDesk$PipInfo;,
        Lcom/konka/kkinterface/tv/CommonDesk$ST_VIDEO_INFO;,
        Lcom/konka/kkinterface/tv/CommonDesk$SystemBoardInfo;,
        Lcom/konka/kkinterface/tv/CommonDesk$SystemBuildInfo;,
        Lcom/konka/kkinterface/tv/CommonDesk$ThreeDImplType;,
        Lcom/konka/kkinterface/tv/CommonDesk$TypeOf3DImpl;,
        Lcom/konka/kkinterface/tv/CommonDesk$UrsaSelectEnumType;,
        Lcom/konka/kkinterface/tv/CommonDesk$WifiDeviceInfo;
    }
.end annotation


# static fields
.field public static final Cmd_CommonVedio:I = 0x45

.field public static final Cmd_SignalLock:I = 0x42

.field public static final Cmd_SignalUnLock:I = 0x43

.field public static final Cmd_SourceInfo:I = 0x41

.field public static final Cmd_TvApkExit:I = 0x44

.field public static final Cmd_XXX_Max:I = 0x60

.field public static final Cmd_XXX_Min:I = 0x40

.field public static final DEBUG_FLAG:Z = true

.field public static final SETIS_END_COMPLETE:I = -0x65

.field public static final SETIS_START:I = -0x64


# virtual methods
.method public abstract ExecSetInputSource(Lcom/mstar/android/tvapi/common/vo/TvOsType$EnumInputSource;)Z
.end method

.method public abstract GetCurrentInputSource()Lcom/mstar/android/tvapi/common/vo/TvOsType$EnumInputSource;
.end method

.method public abstract SetInputSource(Lcom/mstar/android/tvapi/common/vo/TvOsType$EnumInputSource;)V
.end method

.method public abstract SetInputSource(Lcom/mstar/android/tvapi/common/vo/TvOsType$EnumInputSource;Z)V
.end method

.method public abstract disableTvosIr()V
.end method

.method public abstract enterSleepMode(ZZ)Z
.end method

.method public abstract get3DImplType()Lcom/konka/kkinterface/tv/CommonDesk$TypeOf3DImpl;
.end method

.method public abstract get3DImplement()Lcom/konka/kkinterface/tv/CommonDesk$ThreeDImplType;
.end method

.method public abstract getApplicationsInfo()Lcom/konka/kkinterface/tv/CommonDesk$ApplicationsInfo;
.end method

.method public abstract getAudioInfo()Lcom/konka/kkinterface/tv/CommonDesk$AudioInfo;
.end method

.method public abstract getBatteryInfo()Lcom/konka/kkinterface/tv/CommonDesk$BatteryInfo;
.end method

.method public abstract getCPUInfo()Lcom/konka/kkinterface/tv/CommonDesk$CPUInfo;
.end method

.method public abstract getCiCardInfo()Lcom/konka/kkinterface/tv/CommonDesk$CiCardInfo;
.end method

.method public abstract getCustomerInfo()Lcom/konka/kkinterface/tv/CommonDesk$CustomerInfo;
.end method

.method public abstract getExternalStorageInfo()Lcom/konka/kkinterface/tv/CommonDesk$ExternalStorageInfo;
.end method

.method public abstract getInputSourceInfo()Lcom/konka/kkinterface/tv/CommonDesk$InputSourceInfo;
.end method

.method public abstract getLocalDimmingInfo()Lcom/konka/kkinterface/tv/CommonDesk$LocalDimmingInfo;
.end method

.method public abstract getPanel4K2K()Lcom/konka/kkinterface/tv/CommonDesk$Panel4K2K;
.end method

.method public abstract getPanelSupportInfo()Lcom/konka/kkinterface/tv/CommonDesk$PanelSupportInfo;
.end method

.method public abstract getPipInfo()Lcom/konka/kkinterface/tv/CommonDesk$PipInfo;
.end method

.method public abstract getSourceList()[I
.end method

.method public abstract getSystemBoardInfo()Lcom/konka/kkinterface/tv/CommonDesk$SystemBoardInfo;
.end method

.method public abstract getSystemBuildInfo()Lcom/konka/kkinterface/tv/CommonDesk$SystemBuildInfo;
.end method

.method public abstract getUrsaSelect()Lcom/konka/kkinterface/tv/CommonDesk$UrsaSelectEnumType;
.end method

.method public abstract getVideoInfo()Lcom/konka/kkinterface/tv/CommonDesk$ST_VIDEO_INFO;
.end method

.method public abstract getWifiDeviceInfo()Lcom/konka/kkinterface/tv/CommonDesk$WifiDeviceInfo;
.end method

.method public abstract is1280x720(Landroid/content/Context;)Z
.end method

.method public abstract isEnableAdvancedPipSyncview()Z
.end method

.method public abstract isEnableAlTimeShift()Z
.end method

.method public abstract isEnableCEC()Z
.end method

.method public abstract isEnableCI()Z
.end method

.method public abstract isEnableDVBC()Z
.end method

.method public abstract isEnableDemoMode()Z
.end method

.method public abstract isEnableMonitor()Z
.end method

.method public abstract isEnableStickerDemo()Z
.end method

.method public abstract isEnableUSBInputSource()Z
.end method

.method public abstract isEnableUpgradeOnline()Z
.end method

.method public abstract isEnbaleMsgPush()Z
.end method

.method public abstract isEnbaleScreenShot()Z
.end method

.method public abstract isHdmiSignalMode()Z
.end method

.method public abstract isSignalStable()Z
.end method

.method public abstract isSupport3D()Z
.end method

.method public abstract printfE(Ljava/lang/String;)V
.end method

.method public abstract printfE(Ljava/lang/String;Ljava/lang/String;)V
.end method

.method public abstract printfI(Ljava/lang/String;)V
.end method

.method public abstract printfI(Ljava/lang/String;Ljava/lang/String;)V
.end method

.method public abstract printfV(Ljava/lang/String;)V
.end method

.method public abstract printfV(Ljava/lang/String;Ljava/lang/String;)V
.end method

.method public abstract printfW(Ljava/lang/String;)V
.end method

.method public abstract printfW(Ljava/lang/String;Ljava/lang/String;)V
.end method

.method public abstract setDisplayHolder(Landroid/view/SurfaceHolder;)Z
.end method

.method public abstract setGpioDeviceStatus(IZ)Z
.end method

.method public abstract setTimeZone(Lcom/mstar/android/tvapi/common/vo/TvOsType$EnumTimeZone;Z)V
.end method

.method public abstract startMsrv()Z
.end method
