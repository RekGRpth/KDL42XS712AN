.class public final enum Lcom/konka/kkinterface/tv/CommonDesk$UrsaSelectEnumType;
.super Ljava/lang/Enum;
.source "CommonDesk.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/konka/kkinterface/tv/CommonDesk;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x4019
    name = "UrsaSelectEnumType"
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Enum",
        "<",
        "Lcom/konka/kkinterface/tv/CommonDesk$UrsaSelectEnumType;",
        ">;"
    }
.end annotation


# static fields
.field private static final synthetic ENUM$VALUES:[Lcom/konka/kkinterface/tv/CommonDesk$UrsaSelectEnumType;

.field public static final enum OTHER_IC:Lcom/konka/kkinterface/tv/CommonDesk$UrsaSelectEnumType;

.field public static final enum SIXM30_IC:Lcom/konka/kkinterface/tv/CommonDesk$UrsaSelectEnumType;

.field public static final enum SIXM40_IC:Lcom/konka/kkinterface/tv/CommonDesk$UrsaSelectEnumType;


# direct methods
.method static constructor <clinit>()V
    .locals 5

    const/4 v4, 0x2

    const/4 v3, 0x1

    const/4 v2, 0x0

    new-instance v0, Lcom/konka/kkinterface/tv/CommonDesk$UrsaSelectEnumType;

    const-string v1, "OTHER_IC"

    invoke-direct {v0, v1, v2}, Lcom/konka/kkinterface/tv/CommonDesk$UrsaSelectEnumType;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/konka/kkinterface/tv/CommonDesk$UrsaSelectEnumType;->OTHER_IC:Lcom/konka/kkinterface/tv/CommonDesk$UrsaSelectEnumType;

    new-instance v0, Lcom/konka/kkinterface/tv/CommonDesk$UrsaSelectEnumType;

    const-string v1, "SIXM30_IC"

    invoke-direct {v0, v1, v3}, Lcom/konka/kkinterface/tv/CommonDesk$UrsaSelectEnumType;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/konka/kkinterface/tv/CommonDesk$UrsaSelectEnumType;->SIXM30_IC:Lcom/konka/kkinterface/tv/CommonDesk$UrsaSelectEnumType;

    new-instance v0, Lcom/konka/kkinterface/tv/CommonDesk$UrsaSelectEnumType;

    const-string v1, "SIXM40_IC"

    invoke-direct {v0, v1, v4}, Lcom/konka/kkinterface/tv/CommonDesk$UrsaSelectEnumType;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/konka/kkinterface/tv/CommonDesk$UrsaSelectEnumType;->SIXM40_IC:Lcom/konka/kkinterface/tv/CommonDesk$UrsaSelectEnumType;

    const/4 v0, 0x3

    new-array v0, v0, [Lcom/konka/kkinterface/tv/CommonDesk$UrsaSelectEnumType;

    sget-object v1, Lcom/konka/kkinterface/tv/CommonDesk$UrsaSelectEnumType;->OTHER_IC:Lcom/konka/kkinterface/tv/CommonDesk$UrsaSelectEnumType;

    aput-object v1, v0, v2

    sget-object v1, Lcom/konka/kkinterface/tv/CommonDesk$UrsaSelectEnumType;->SIXM30_IC:Lcom/konka/kkinterface/tv/CommonDesk$UrsaSelectEnumType;

    aput-object v1, v0, v3

    sget-object v1, Lcom/konka/kkinterface/tv/CommonDesk$UrsaSelectEnumType;->SIXM40_IC:Lcom/konka/kkinterface/tv/CommonDesk$UrsaSelectEnumType;

    aput-object v1, v0, v4

    sput-object v0, Lcom/konka/kkinterface/tv/CommonDesk$UrsaSelectEnumType;->ENUM$VALUES:[Lcom/konka/kkinterface/tv/CommonDesk$UrsaSelectEnumType;

    return-void
.end method

.method private constructor <init>(Ljava/lang/String;I)V
    .locals 0

    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    return-void
.end method

.method public static valueOf(Ljava/lang/String;)Lcom/konka/kkinterface/tv/CommonDesk$UrsaSelectEnumType;
    .locals 1

    const-class v0, Lcom/konka/kkinterface/tv/CommonDesk$UrsaSelectEnumType;

    invoke-static {v0, p0}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;

    move-result-object v0

    check-cast v0, Lcom/konka/kkinterface/tv/CommonDesk$UrsaSelectEnumType;

    return-object v0
.end method

.method public static values()[Lcom/konka/kkinterface/tv/CommonDesk$UrsaSelectEnumType;
    .locals 4

    const/4 v3, 0x0

    sget-object v0, Lcom/konka/kkinterface/tv/CommonDesk$UrsaSelectEnumType;->ENUM$VALUES:[Lcom/konka/kkinterface/tv/CommonDesk$UrsaSelectEnumType;

    array-length v1, v0

    new-array v2, v1, [Lcom/konka/kkinterface/tv/CommonDesk$UrsaSelectEnumType;

    invoke-static {v0, v3, v2, v3, v1}, Ljava/lang/System;->arraycopy(Ljava/lang/Object;ILjava/lang/Object;II)V

    return-object v2
.end method
