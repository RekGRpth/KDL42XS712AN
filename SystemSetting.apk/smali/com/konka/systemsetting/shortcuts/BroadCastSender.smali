.class public Lcom/konka/systemsetting/shortcuts/BroadCastSender;
.super Landroid/app/Activity;
.source "BroadCastSender.java"


# direct methods
.method public constructor <init>()V
    .locals 0

    invoke-direct {p0}, Landroid/app/Activity;-><init>()V

    return-void
.end method


# virtual methods
.method protected onCreate(Landroid/os/Bundle;)V
    .locals 4
    .param p1    # Landroid/os/Bundle;

    invoke-super {p0, p1}, Landroid/app/Activity;->onCreate(Landroid/os/Bundle;)V

    const v1, 0x7f03000a    # com.konka.systemsetting.R.layout.short_cut_view

    invoke-virtual {p0, v1}, Lcom/konka/systemsetting/shortcuts/BroadCastSender;->setContentView(I)V

    new-instance v0, Landroid/content/Intent;

    invoke-direct {v0}, Landroid/content/Intent;-><init>()V

    invoke-virtual {p0}, Lcom/konka/systemsetting/shortcuts/BroadCastSender;->getIntent()Landroid/content/Intent;

    move-result-object v1

    const-string v2, "action"

    invoke-virtual {v1, v2}, Landroid/content/Intent;->getStringExtra(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    if-eqz v1, :cond_0

    invoke-virtual {p0}, Lcom/konka/systemsetting/shortcuts/BroadCastSender;->getIntent()Landroid/content/Intent;

    move-result-object v1

    const-string v2, "menu_name"

    invoke-virtual {v1, v2}, Landroid/content/Intent;->getStringExtra(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    if-eqz v1, :cond_0

    invoke-virtual {p0}, Lcom/konka/systemsetting/shortcuts/BroadCastSender;->getIntent()Landroid/content/Intent;

    move-result-object v1

    const-string v2, "action"

    invoke-virtual {v1, v2}, Landroid/content/Intent;->getStringExtra(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/content/Intent;->setAction(Ljava/lang/String;)Landroid/content/Intent;

    const-string v1, "menu_name"

    invoke-virtual {p0}, Lcom/konka/systemsetting/shortcuts/BroadCastSender;->getIntent()Landroid/content/Intent;

    move-result-object v2

    const-string v3, "menu_name"

    invoke-virtual {v2, v3}, Landroid/content/Intent;->getStringExtra(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    const/high16 v1, 0x10200000

    invoke-virtual {v0, v1}, Landroid/content/Intent;->addFlags(I)Landroid/content/Intent;

    invoke-virtual {p0, v0}, Lcom/konka/systemsetting/shortcuts/BroadCastSender;->sendBroadcast(Landroid/content/Intent;)V

    :cond_0
    invoke-virtual {p0}, Lcom/konka/systemsetting/shortcuts/BroadCastSender;->finish()V

    return-void
.end method
