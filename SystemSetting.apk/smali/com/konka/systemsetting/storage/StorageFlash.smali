.class public Lcom/konka/systemsetting/storage/StorageFlash;
.super Ljava/lang/Object;
.source "StorageFlash.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/konka/systemsetting/storage/StorageFlash$Ress;
    }
.end annotation


# static fields
.field private static CURRENT_LANGUAGE:Ljava/lang/String;


# instance fields
.field private linearLayout_itemFreeSpace:Landroid/widget/LinearLayout;

.field private linearLayout_itemTotalSpace:Landroid/widget/LinearLayout;

.field private mContext:Lcom/konka/systemsetting/MainActivity;

.field private mDataFileStats:Landroid/os/StatFs;

.field private mFreeStorage:J

.field private mTotalStorage:J

.field private m_strFreeSpace:Ljava/lang/String;

.field private m_strTotalSpace:Ljava/lang/String;


# direct methods
.method public constructor <init>(Lcom/konka/systemsetting/MainActivity;)V
    .locals 3
    .param p1    # Lcom/konka/systemsetting/MainActivity;

    const-wide/16 v1, 0x0

    const/4 v0, 0x0

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    iput-wide v1, p0, Lcom/konka/systemsetting/storage/StorageFlash;->mTotalStorage:J

    iput-wide v1, p0, Lcom/konka/systemsetting/storage/StorageFlash;->mFreeStorage:J

    iput-object v0, p0, Lcom/konka/systemsetting/storage/StorageFlash;->mContext:Lcom/konka/systemsetting/MainActivity;

    iput-object v0, p0, Lcom/konka/systemsetting/storage/StorageFlash;->linearLayout_itemFreeSpace:Landroid/widget/LinearLayout;

    iput-object v0, p0, Lcom/konka/systemsetting/storage/StorageFlash;->linearLayout_itemTotalSpace:Landroid/widget/LinearLayout;

    iput-object v0, p0, Lcom/konka/systemsetting/storage/StorageFlash;->m_strFreeSpace:Ljava/lang/String;

    iput-object v0, p0, Lcom/konka/systemsetting/storage/StorageFlash;->m_strTotalSpace:Ljava/lang/String;

    iput-object p1, p0, Lcom/konka/systemsetting/storage/StorageFlash;->mContext:Lcom/konka/systemsetting/MainActivity;

    invoke-direct {p0}, Lcom/konka/systemsetting/storage/StorageFlash;->initUIComponents()V

    invoke-direct {p0}, Lcom/konka/systemsetting/storage/StorageFlash;->initUIData()V

    invoke-direct {p0}, Lcom/konka/systemsetting/storage/StorageFlash;->updateUIState()V

    return-void
.end method

.method private initUIComponents()V
    .locals 2

    iget-object v0, p0, Lcom/konka/systemsetting/storage/StorageFlash;->mContext:Lcom/konka/systemsetting/MainActivity;

    const v1, 0x7f09007c    # com.konka.systemsetting.R.id.sys_storage_flash_freespace

    invoke-virtual {v0, v1}, Lcom/konka/systemsetting/MainActivity;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/LinearLayout;

    iput-object v0, p0, Lcom/konka/systemsetting/storage/StorageFlash;->linearLayout_itemFreeSpace:Landroid/widget/LinearLayout;

    iget-object v0, p0, Lcom/konka/systemsetting/storage/StorageFlash;->mContext:Lcom/konka/systemsetting/MainActivity;

    const v1, 0x7f09007d    # com.konka.systemsetting.R.id.sys_storage_flash_totalspace

    invoke-virtual {v0, v1}, Lcom/konka/systemsetting/MainActivity;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/LinearLayout;

    iput-object v0, p0, Lcom/konka/systemsetting/storage/StorageFlash;->linearLayout_itemTotalSpace:Landroid/widget/LinearLayout;

    return-void
.end method

.method private initUIData()V
    .locals 4

    new-instance v0, Landroid/os/StatFs;

    const-string v1, "/data"

    invoke-direct {v0, v1}, Landroid/os/StatFs;-><init>(Ljava/lang/String;)V

    iput-object v0, p0, Lcom/konka/systemsetting/storage/StorageFlash;->mDataFileStats:Landroid/os/StatFs;

    iget-object v0, p0, Lcom/konka/systemsetting/storage/StorageFlash;->mDataFileStats:Landroid/os/StatFs;

    const-string v1, "/data"

    invoke-virtual {v0, v1}, Landroid/os/StatFs;->restat(Ljava/lang/String;)V

    iget-object v0, p0, Lcom/konka/systemsetting/storage/StorageFlash;->mDataFileStats:Landroid/os/StatFs;

    invoke-virtual {v0}, Landroid/os/StatFs;->getBlockCount()I

    move-result v0

    int-to-long v0, v0

    iget-object v2, p0, Lcom/konka/systemsetting/storage/StorageFlash;->mDataFileStats:Landroid/os/StatFs;

    invoke-virtual {v2}, Landroid/os/StatFs;->getBlockSize()I

    move-result v2

    int-to-long v2, v2

    mul-long/2addr v0, v2

    iput-wide v0, p0, Lcom/konka/systemsetting/storage/StorageFlash;->mTotalStorage:J

    iget-object v0, p0, Lcom/konka/systemsetting/storage/StorageFlash;->mDataFileStats:Landroid/os/StatFs;

    invoke-virtual {v0}, Landroid/os/StatFs;->getAvailableBlocks()I

    move-result v0

    int-to-long v0, v0

    iget-object v2, p0, Lcom/konka/systemsetting/storage/StorageFlash;->mDataFileStats:Landroid/os/StatFs;

    invoke-virtual {v2}, Landroid/os/StatFs;->getBlockSize()I

    move-result v2

    int-to-long v2, v2

    mul-long/2addr v0, v2

    iput-wide v0, p0, Lcom/konka/systemsetting/storage/StorageFlash;->mFreeStorage:J

    iget-wide v0, p0, Lcom/konka/systemsetting/storage/StorageFlash;->mFreeStorage:J

    invoke-virtual {p0, v0, v1}, Lcom/konka/systemsetting/storage/StorageFlash;->sizeToAutoUnit(J)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/konka/systemsetting/storage/StorageFlash;->m_strFreeSpace:Ljava/lang/String;

    iget-wide v0, p0, Lcom/konka/systemsetting/storage/StorageFlash;->mTotalStorage:J

    invoke-virtual {p0, v0, v1}, Lcom/konka/systemsetting/storage/StorageFlash;->sizeToAutoUnit(J)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/konka/systemsetting/storage/StorageFlash;->m_strTotalSpace:Ljava/lang/String;

    return-void
.end method

.method private updateUIState()V
    .locals 4

    const/4 v3, 0x1

    iget-object v2, p0, Lcom/konka/systemsetting/storage/StorageFlash;->linearLayout_itemFreeSpace:Landroid/widget/LinearLayout;

    invoke-virtual {v2, v3}, Landroid/widget/LinearLayout;->getChildAt(I)Landroid/view/View;

    move-result-object v1

    check-cast v1, Landroid/widget/TextView;

    iget-object v2, p0, Lcom/konka/systemsetting/storage/StorageFlash;->m_strFreeSpace:Ljava/lang/String;

    invoke-virtual {v1, v2}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    iget-object v2, p0, Lcom/konka/systemsetting/storage/StorageFlash;->linearLayout_itemTotalSpace:Landroid/widget/LinearLayout;

    invoke-virtual {v2, v3}, Landroid/widget/LinearLayout;->getChildAt(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    iget-object v2, p0, Lcom/konka/systemsetting/storage/StorageFlash;->m_strTotalSpace:Ljava/lang/String;

    invoke-virtual {v0, v2}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    return-void
.end method


# virtual methods
.method public sizeToAutoUnit(J)Ljava/lang/String;
    .locals 10
    .param p1    # J

    const-wide/16 v2, 0x1

    const/4 v9, 0x3

    const/4 v8, 0x1

    const-wide/high16 v6, 0x4090000000000000L    # 1024.0

    const-wide/16 v4, 0x400

    iget-object v0, p0, Lcom/konka/systemsetting/storage/StorageFlash;->mContext:Lcom/konka/systemsetting/MainActivity;

    invoke-virtual {v0}, Lcom/konka/systemsetting/MainActivity;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    const v1, 0x7f060001    # com.konka.systemsetting.R.string.lang_flag

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v0

    sput-object v0, Lcom/konka/systemsetting/storage/StorageFlash;->CURRENT_LANGUAGE:Ljava/lang/String;

    div-long v0, p1, v4

    div-long/2addr v0, v4

    div-long/2addr v0, v4

    cmp-long v0, v0, v2

    if-ltz v0, :cond_1

    sget-object v0, Lcom/konka/systemsetting/storage/StorageFlash;->CURRENT_LANGUAGE:Ljava/lang/String;

    const-string v1, "rus"

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    new-instance v0, Ljava/lang/StringBuilder;

    div-long v1, p1, v4

    div-long/2addr v1, v4

    div-long/2addr v1, v4

    invoke-static {v1, v2}, Ljava/lang/String;->valueOf(J)Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    new-instance v1, Ljava/lang/StringBuilder;

    div-long v2, p1, v4

    div-long/2addr v2, v4

    rem-long/2addr v2, v4

    long-to-double v2, v2

    div-double/2addr v2, v6

    invoke-static {v2, v3}, Ljava/lang/String;->valueOf(D)Ljava/lang/String;

    move-result-object v2

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v1, v8, v9}, Ljava/lang/String;->substring(II)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, "\u0413\u0431"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    :goto_0
    return-object v0

    :cond_0
    new-instance v0, Ljava/lang/StringBuilder;

    div-long v1, p1, v4

    div-long/2addr v1, v4

    div-long/2addr v1, v4

    invoke-static {v1, v2}, Ljava/lang/String;->valueOf(J)Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    new-instance v1, Ljava/lang/StringBuilder;

    div-long v2, p1, v4

    div-long/2addr v2, v4

    rem-long/2addr v2, v4

    long-to-double v2, v2

    div-double/2addr v2, v6

    invoke-static {v2, v3}, Ljava/lang/String;->valueOf(D)Ljava/lang/String;

    move-result-object v2

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v1, v8, v9}, Ljava/lang/String;->substring(II)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, "GB"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    goto :goto_0

    :cond_1
    div-long v0, p1, v4

    div-long/2addr v0, v4

    cmp-long v0, v0, v2

    if-ltz v0, :cond_3

    sget-object v0, Lcom/konka/systemsetting/storage/StorageFlash;->CURRENT_LANGUAGE:Ljava/lang/String;

    const-string v1, "rus"

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_2

    new-instance v0, Ljava/lang/StringBuilder;

    div-long v1, p1, v4

    div-long/2addr v1, v4

    invoke-static {v1, v2}, Ljava/lang/String;->valueOf(J)Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    new-instance v1, Ljava/lang/StringBuilder;

    div-long v2, p1, v4

    rem-long/2addr v2, v4

    long-to-double v2, v2

    div-double/2addr v2, v6

    invoke-static {v2, v3}, Ljava/lang/String;->valueOf(D)Ljava/lang/String;

    move-result-object v2

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v1, v8, v9}, Ljava/lang/String;->substring(II)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, "\u041c\u0431"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    goto :goto_0

    :cond_2
    new-instance v0, Ljava/lang/StringBuilder;

    div-long v1, p1, v4

    div-long/2addr v1, v4

    invoke-static {v1, v2}, Ljava/lang/String;->valueOf(J)Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    new-instance v1, Ljava/lang/StringBuilder;

    div-long v2, p1, v4

    rem-long/2addr v2, v4

    long-to-double v2, v2

    div-double/2addr v2, v6

    invoke-static {v2, v3}, Ljava/lang/String;->valueOf(D)Ljava/lang/String;

    move-result-object v2

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v1, v8, v9}, Ljava/lang/String;->substring(II)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, "MB"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    goto/16 :goto_0

    :cond_3
    new-instance v0, Ljava/lang/StringBuilder;

    div-long v1, p1, v4

    invoke-static {v1, v2}, Ljava/lang/String;->valueOf(J)Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    new-instance v1, Ljava/lang/StringBuilder;

    rem-long v2, p1, v4

    long-to-double v2, v2

    div-double/2addr v2, v6

    invoke-static {v2, v3}, Ljava/lang/String;->valueOf(D)Ljava/lang/String;

    move-result-object v2

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v1, v8, v9}, Ljava/lang/String;->substring(II)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, "KB"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    goto/16 :goto_0
.end method
