.class public Lcom/konka/systemsetting/storage/StorageSdcard;
.super Ljava/lang/Object;
.source "StorageSdcard.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/konka/systemsetting/storage/StorageSdcard$UsbReceiver;
    }
.end annotation


# instance fields
.field private final REFRESH_MENU:I

.field private btn_itemFormat:Landroid/widget/Button;

.field private btn_itemUnmount:Landroid/widget/Button;

.field private linearLayout_itemFreeSpace:Landroid/widget/LinearLayout;

.field private linearLayout_itemTotalSpace:Landroid/widget/LinearLayout;

.field private linearLayout_nosdcard:Landroid/widget/LinearLayout;

.field private linearLayout_sdcardinfo:Landroid/widget/LinearLayout;

.field private mContext:Lcom/konka/systemsetting/MainActivity;

.field private m_bWaittoFormat:Z

.field private m_currItemId:I

.field private m_storage:Lcom/konka/utilities/Storage;

.field private m_storageManager:Lcom/mstar/android/storage/MStorageManager;

.field private m_strFreeSpace:Ljava/lang/String;

.field private m_strTotalSpace:Ljava/lang/String;

.field private m_strformatPath:Ljava/lang/String;

.field private usbReceiver:Lcom/konka/systemsetting/storage/StorageSdcard$UsbReceiver;


# direct methods
.method public constructor <init>(Lcom/konka/systemsetting/MainActivity;)V
    .locals 5
    .param p1    # Lcom/konka/systemsetting/MainActivity;

    const/4 v4, 0x4

    const/4 v3, 0x0

    const/4 v2, 0x0

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    iput-object v2, p0, Lcom/konka/systemsetting/storage/StorageSdcard;->mContext:Lcom/konka/systemsetting/MainActivity;

    iput-object v2, p0, Lcom/konka/systemsetting/storage/StorageSdcard;->btn_itemUnmount:Landroid/widget/Button;

    iput-object v2, p0, Lcom/konka/systemsetting/storage/StorageSdcard;->btn_itemFormat:Landroid/widget/Button;

    iput-object v2, p0, Lcom/konka/systemsetting/storage/StorageSdcard;->linearLayout_itemTotalSpace:Landroid/widget/LinearLayout;

    iput-object v2, p0, Lcom/konka/systemsetting/storage/StorageSdcard;->linearLayout_itemFreeSpace:Landroid/widget/LinearLayout;

    iput-object v2, p0, Lcom/konka/systemsetting/storage/StorageSdcard;->linearLayout_nosdcard:Landroid/widget/LinearLayout;

    iput-object v2, p0, Lcom/konka/systemsetting/storage/StorageSdcard;->linearLayout_sdcardinfo:Landroid/widget/LinearLayout;

    const v1, 0x7f090080    # com.konka.systemsetting.R.id.sys_storage_sdcard_unmount

    iput v1, p0, Lcom/konka/systemsetting/storage/StorageSdcard;->m_currItemId:I

    iput-object v2, p0, Lcom/konka/systemsetting/storage/StorageSdcard;->m_storage:Lcom/konka/utilities/Storage;

    iput-object v2, p0, Lcom/konka/systemsetting/storage/StorageSdcard;->m_storageManager:Lcom/mstar/android/storage/MStorageManager;

    iput-boolean v3, p0, Lcom/konka/systemsetting/storage/StorageSdcard;->m_bWaittoFormat:Z

    const/4 v1, 0x1

    iput v1, p0, Lcom/konka/systemsetting/storage/StorageSdcard;->REFRESH_MENU:I

    iput-object p1, p0, Lcom/konka/systemsetting/storage/StorageSdcard;->mContext:Lcom/konka/systemsetting/MainActivity;

    iget-object v1, p0, Lcom/konka/systemsetting/storage/StorageSdcard;->mContext:Lcom/konka/systemsetting/MainActivity;

    invoke-static {v1}, Lcom/mstar/android/storage/MStorageManager;->getInstance(Landroid/content/Context;)Lcom/mstar/android/storage/MStorageManager;

    move-result-object v1

    iput-object v1, p0, Lcom/konka/systemsetting/storage/StorageSdcard;->m_storageManager:Lcom/mstar/android/storage/MStorageManager;

    invoke-direct {p0}, Lcom/konka/systemsetting/storage/StorageSdcard;->initUIComponents()V

    new-instance v1, Lcom/konka/systemsetting/storage/StorageSdcard$UsbReceiver;

    invoke-direct {v1, p0}, Lcom/konka/systemsetting/storage/StorageSdcard$UsbReceiver;-><init>(Lcom/konka/systemsetting/storage/StorageSdcard;)V

    iput-object v1, p0, Lcom/konka/systemsetting/storage/StorageSdcard;->usbReceiver:Lcom/konka/systemsetting/storage/StorageSdcard$UsbReceiver;

    new-instance v0, Landroid/content/IntentFilter;

    const-string v1, "android.intent.action.MEDIA_MOUNTED"

    invoke-direct {v0, v1}, Landroid/content/IntentFilter;-><init>(Ljava/lang/String;)V

    const-string v1, "file"

    invoke-virtual {v0, v1}, Landroid/content/IntentFilter;->addDataScheme(Ljava/lang/String;)V

    iget-object v1, p0, Lcom/konka/systemsetting/storage/StorageSdcard;->mContext:Lcom/konka/systemsetting/MainActivity;

    iget-object v2, p0, Lcom/konka/systemsetting/storage/StorageSdcard;->usbReceiver:Lcom/konka/systemsetting/storage/StorageSdcard$UsbReceiver;

    invoke-virtual {v1, v2, v0}, Lcom/konka/systemsetting/MainActivity;->registerReceiver(Landroid/content/BroadcastReceiver;Landroid/content/IntentFilter;)Landroid/content/Intent;

    new-instance v0, Landroid/content/IntentFilter;

    const-string v1, "android.intent.action.MEDIA_UNMOUNTED"

    invoke-direct {v0, v1}, Landroid/content/IntentFilter;-><init>(Ljava/lang/String;)V

    const-string v1, "file"

    invoke-virtual {v0, v1}, Landroid/content/IntentFilter;->addDataScheme(Ljava/lang/String;)V

    iget-object v1, p0, Lcom/konka/systemsetting/storage/StorageSdcard;->mContext:Lcom/konka/systemsetting/MainActivity;

    iget-object v2, p0, Lcom/konka/systemsetting/storage/StorageSdcard;->usbReceiver:Lcom/konka/systemsetting/storage/StorageSdcard$UsbReceiver;

    invoke-virtual {v1, v2, v0}, Lcom/konka/systemsetting/MainActivity;->registerReceiver(Landroid/content/BroadcastReceiver;Landroid/content/IntentFilter;)Landroid/content/Intent;

    new-instance v1, Lcom/konka/utilities/Storage;

    invoke-direct {v1}, Lcom/konka/utilities/Storage;-><init>()V

    iput-object v1, p0, Lcom/konka/systemsetting/storage/StorageSdcard;->m_storage:Lcom/konka/utilities/Storage;

    iget-object v1, p0, Lcom/konka/systemsetting/storage/StorageSdcard;->m_storage:Lcom/konka/utilities/Storage;

    invoke-virtual {v1}, Lcom/konka/utilities/Storage;->isSDMounted()Z

    move-result v1

    if-nez v1, :cond_0

    iget-object v1, p0, Lcom/konka/systemsetting/storage/StorageSdcard;->linearLayout_sdcardinfo:Landroid/widget/LinearLayout;

    invoke-virtual {v1, v4}, Landroid/widget/LinearLayout;->setVisibility(I)V

    iget-object v1, p0, Lcom/konka/systemsetting/storage/StorageSdcard;->linearLayout_nosdcard:Landroid/widget/LinearLayout;

    invoke-virtual {v1, v3}, Landroid/widget/LinearLayout;->setVisibility(I)V

    :goto_0
    return-void

    :cond_0
    iget-object v1, p0, Lcom/konka/systemsetting/storage/StorageSdcard;->linearLayout_nosdcard:Landroid/widget/LinearLayout;

    invoke-virtual {v1, v4}, Landroid/widget/LinearLayout;->setVisibility(I)V

    iget-object v1, p0, Lcom/konka/systemsetting/storage/StorageSdcard;->linearLayout_sdcardinfo:Landroid/widget/LinearLayout;

    invoke-virtual {v1, v3}, Landroid/widget/LinearLayout;->setVisibility(I)V

    invoke-direct {p0}, Lcom/konka/systemsetting/storage/StorageSdcard;->initUIData()V

    invoke-direct {p0}, Lcom/konka/systemsetting/storage/StorageSdcard;->updateUIState()V

    goto :goto_0
.end method

.method static synthetic access$0(Lcom/konka/systemsetting/storage/StorageSdcard;)Lcom/konka/systemsetting/MainActivity;
    .locals 1

    iget-object v0, p0, Lcom/konka/systemsetting/storage/StorageSdcard;->mContext:Lcom/konka/systemsetting/MainActivity;

    return-object v0
.end method

.method static synthetic access$1(Lcom/konka/systemsetting/storage/StorageSdcard;)Z
    .locals 1

    iget-boolean v0, p0, Lcom/konka/systemsetting/storage/StorageSdcard;->m_bWaittoFormat:Z

    return v0
.end method

.method static synthetic access$2(Lcom/konka/systemsetting/storage/StorageSdcard;)Ljava/lang/String;
    .locals 1

    iget-object v0, p0, Lcom/konka/systemsetting/storage/StorageSdcard;->m_strformatPath:Ljava/lang/String;

    return-object v0
.end method

.method static synthetic access$3(Lcom/konka/systemsetting/storage/StorageSdcard;Z)V
    .locals 0

    iput-boolean p1, p0, Lcom/konka/systemsetting/storage/StorageSdcard;->m_bWaittoFormat:Z

    return-void
.end method

.method static synthetic access$4(Lcom/konka/systemsetting/storage/StorageSdcard;Ljava/lang/String;)V
    .locals 0

    iput-object p1, p0, Lcom/konka/systemsetting/storage/StorageSdcard;->m_strformatPath:Ljava/lang/String;

    return-void
.end method

.method static synthetic access$5(Lcom/konka/systemsetting/storage/StorageSdcard;)Lcom/mstar/android/storage/MStorageManager;
    .locals 1

    iget-object v0, p0, Lcom/konka/systemsetting/storage/StorageSdcard;->m_storageManager:Lcom/mstar/android/storage/MStorageManager;

    return-object v0
.end method

.method static synthetic access$6(Lcom/konka/systemsetting/storage/StorageSdcard;I)V
    .locals 0

    iput p1, p0, Lcom/konka/systemsetting/storage/StorageSdcard;->m_currItemId:I

    return-void
.end method

.method static synthetic access$7(Lcom/konka/systemsetting/storage/StorageSdcard;)I
    .locals 1

    iget v0, p0, Lcom/konka/systemsetting/storage/StorageSdcard;->m_currItemId:I

    return v0
.end method

.method static synthetic access$8(Lcom/konka/systemsetting/storage/StorageSdcard;)V
    .locals 0

    invoke-direct {p0}, Lcom/konka/systemsetting/storage/StorageSdcard;->unmountSDCard()V

    return-void
.end method

.method static synthetic access$9(Lcom/konka/systemsetting/storage/StorageSdcard;)V
    .locals 0

    invoke-direct {p0}, Lcom/konka/systemsetting/storage/StorageSdcard;->formatSDCard()V

    return-void
.end method

.method private formatSDCard()V
    .locals 4

    const/4 v2, 0x0

    const/4 v3, 0x1

    invoke-static {}, Landroid/os/Environment;->getExternalStorageDirectory()Ljava/io/File;

    move-result-object v0

    invoke-virtual {v0}, Ljava/io/File;->getPath()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/konka/systemsetting/storage/StorageSdcard;->m_strformatPath:Ljava/lang/String;

    iget-object v0, p0, Lcom/konka/systemsetting/storage/StorageSdcard;->m_storageManager:Lcom/mstar/android/storage/MStorageManager;

    iget-object v1, p0, Lcom/konka/systemsetting/storage/StorageSdcard;->m_strformatPath:Ljava/lang/String;

    invoke-virtual {v0, v1}, Lcom/mstar/android/storage/MStorageManager;->getVolumeState(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    const-string v1, "mounted"

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    iput-boolean v3, p0, Lcom/konka/systemsetting/storage/StorageSdcard;->m_bWaittoFormat:Z

    iget-object v0, p0, Lcom/konka/systemsetting/storage/StorageSdcard;->m_storageManager:Lcom/mstar/android/storage/MStorageManager;

    iget-object v1, p0, Lcom/konka/systemsetting/storage/StorageSdcard;->m_strformatPath:Ljava/lang/String;

    const/4 v2, 0x0

    invoke-virtual {v0, v1, v3, v2}, Lcom/mstar/android/storage/MStorageManager;->unmountVolume(Ljava/lang/String;ZZ)V

    :goto_0
    return-void

    :cond_0
    iget-object v0, p0, Lcom/konka/systemsetting/storage/StorageSdcard;->m_storageManager:Lcom/mstar/android/storage/MStorageManager;

    iget-object v1, p0, Lcom/konka/systemsetting/storage/StorageSdcard;->m_strformatPath:Ljava/lang/String;

    invoke-virtual {v0, v1}, Lcom/mstar/android/storage/MStorageManager;->getVolumeState(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    const-string v1, "unmounted"

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_3

    iget-object v0, p0, Lcom/konka/systemsetting/storage/StorageSdcard;->m_storageManager:Lcom/mstar/android/storage/MStorageManager;

    iget-object v1, p0, Lcom/konka/systemsetting/storage/StorageSdcard;->m_strformatPath:Ljava/lang/String;

    invoke-virtual {v0, v1}, Lcom/mstar/android/storage/MStorageManager;->formatVolume(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_2

    new-instance v0, Ljava/lang/StringBuilder;

    const-string v1, "Success to format "

    invoke-direct {v0, v1}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    iget-object v1, p0, Lcom/konka/systemsetting/storage/StorageSdcard;->m_strformatPath:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Lcom/konka/debuginfo/logPrint;->Debug(Ljava/lang/String;)V

    iget-object v0, p0, Lcom/konka/systemsetting/storage/StorageSdcard;->m_storageManager:Lcom/mstar/android/storage/MStorageManager;

    iget-object v1, p0, Lcom/konka/systemsetting/storage/StorageSdcard;->m_strformatPath:Ljava/lang/String;

    invoke-virtual {v0, v1}, Lcom/mstar/android/storage/MStorageManager;->mountVolume(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_1

    new-instance v0, Ljava/lang/StringBuilder;

    const-string v1, "Success to mount "

    invoke-direct {v0, v1}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    iget-object v1, p0, Lcom/konka/systemsetting/storage/StorageSdcard;->m_strformatPath:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, " again"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Lcom/konka/debuginfo/logPrint;->Debug(Ljava/lang/String;)V

    :goto_1
    iput-object v2, p0, Lcom/konka/systemsetting/storage/StorageSdcard;->m_strformatPath:Ljava/lang/String;

    goto :goto_0

    :cond_1
    new-instance v0, Ljava/lang/StringBuilder;

    const-string v1, "Fail to mount "

    invoke-direct {v0, v1}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    iget-object v1, p0, Lcom/konka/systemsetting/storage/StorageSdcard;->m_strformatPath:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, " again"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Lcom/konka/debuginfo/logPrint;->Debug(Ljava/lang/String;)V

    goto :goto_1

    :cond_2
    new-instance v0, Ljava/lang/StringBuilder;

    const-string v1, "Fail to format "

    invoke-direct {v0, v1}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    iget-object v1, p0, Lcom/konka/systemsetting/storage/StorageSdcard;->m_strformatPath:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Lcom/konka/debuginfo/logPrint;->Debug(Ljava/lang/String;)V

    goto :goto_1

    :cond_3
    new-instance v0, Ljava/lang/StringBuilder;

    const-string v1, "Can not format "

    invoke-direct {v0, v1}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    iget-object v1, p0, Lcom/konka/systemsetting/storage/StorageSdcard;->m_strformatPath:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Lcom/konka/debuginfo/logPrint;->Debug(Ljava/lang/String;)V

    iput-object v2, p0, Lcom/konka/systemsetting/storage/StorageSdcard;->m_strformatPath:Ljava/lang/String;

    goto/16 :goto_0
.end method

.method private initUIComponents()V
    .locals 5

    const v4, 0x7f090086    # com.konka.systemsetting.R.id.linearlayout_storage_tab_sdcard

    const v3, 0x7f090081    # com.konka.systemsetting.R.id.sys_storage_sdcard_format

    const v2, 0x7f090080    # com.konka.systemsetting.R.id.sys_storage_sdcard_unmount

    iget-object v0, p0, Lcom/konka/systemsetting/storage/StorageSdcard;->mContext:Lcom/konka/systemsetting/MainActivity;

    const v1, 0x7f09007e    # com.konka.systemsetting.R.id.ll_no_sd_card

    invoke-virtual {v0, v1}, Lcom/konka/systemsetting/MainActivity;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/LinearLayout;

    iput-object v0, p0, Lcom/konka/systemsetting/storage/StorageSdcard;->linearLayout_nosdcard:Landroid/widget/LinearLayout;

    iget-object v0, p0, Lcom/konka/systemsetting/storage/StorageSdcard;->mContext:Lcom/konka/systemsetting/MainActivity;

    const v1, 0x7f09007f    # com.konka.systemsetting.R.id.ll_sd_info

    invoke-virtual {v0, v1}, Lcom/konka/systemsetting/MainActivity;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/LinearLayout;

    iput-object v0, p0, Lcom/konka/systemsetting/storage/StorageSdcard;->linearLayout_sdcardinfo:Landroid/widget/LinearLayout;

    iget-object v0, p0, Lcom/konka/systemsetting/storage/StorageSdcard;->mContext:Lcom/konka/systemsetting/MainActivity;

    invoke-virtual {v0, v2}, Lcom/konka/systemsetting/MainActivity;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/Button;

    iput-object v0, p0, Lcom/konka/systemsetting/storage/StorageSdcard;->btn_itemUnmount:Landroid/widget/Button;

    iget-object v0, p0, Lcom/konka/systemsetting/storage/StorageSdcard;->mContext:Lcom/konka/systemsetting/MainActivity;

    invoke-virtual {v0, v3}, Lcom/konka/systemsetting/MainActivity;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/Button;

    iput-object v0, p0, Lcom/konka/systemsetting/storage/StorageSdcard;->btn_itemFormat:Landroid/widget/Button;

    iget-object v0, p0, Lcom/konka/systemsetting/storage/StorageSdcard;->mContext:Lcom/konka/systemsetting/MainActivity;

    const v1, 0x7f090083    # com.konka.systemsetting.R.id.sys_storage_sdcard_totalspace

    invoke-virtual {v0, v1}, Lcom/konka/systemsetting/MainActivity;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/LinearLayout;

    iput-object v0, p0, Lcom/konka/systemsetting/storage/StorageSdcard;->linearLayout_itemTotalSpace:Landroid/widget/LinearLayout;

    iget-object v0, p0, Lcom/konka/systemsetting/storage/StorageSdcard;->mContext:Lcom/konka/systemsetting/MainActivity;

    const v1, 0x7f090082    # com.konka.systemsetting.R.id.sys_storage_sdcard_freespace

    invoke-virtual {v0, v1}, Lcom/konka/systemsetting/MainActivity;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/LinearLayout;

    iput-object v0, p0, Lcom/konka/systemsetting/storage/StorageSdcard;->linearLayout_itemFreeSpace:Landroid/widget/LinearLayout;

    iget-object v0, p0, Lcom/konka/systemsetting/storage/StorageSdcard;->btn_itemUnmount:Landroid/widget/Button;

    invoke-virtual {v0, v2}, Landroid/widget/Button;->setNextFocusUpId(I)V

    iget-object v0, p0, Lcom/konka/systemsetting/storage/StorageSdcard;->btn_itemFormat:Landroid/widget/Button;

    invoke-virtual {v0, v3}, Landroid/widget/Button;->setNextFocusDownId(I)V

    iget-object v0, p0, Lcom/konka/systemsetting/storage/StorageSdcard;->btn_itemUnmount:Landroid/widget/Button;

    invoke-virtual {v0, v4}, Landroid/widget/Button;->setNextFocusLeftId(I)V

    iget-object v0, p0, Lcom/konka/systemsetting/storage/StorageSdcard;->btn_itemFormat:Landroid/widget/Button;

    invoke-virtual {v0, v4}, Landroid/widget/Button;->setNextFocusLeftId(I)V

    invoke-direct {p0}, Lcom/konka/systemsetting/storage/StorageSdcard;->setOnFocusChangeListener()V

    invoke-direct {p0}, Lcom/konka/systemsetting/storage/StorageSdcard;->setOnClickListener()V

    return-void
.end method

.method private initUIData()V
    .locals 5

    iget-object v4, p0, Lcom/konka/systemsetting/storage/StorageSdcard;->m_storage:Lcom/konka/utilities/Storage;

    invoke-virtual {v4}, Lcom/konka/utilities/Storage;->getDiskStorage()Lcom/konka/utilities/StorageInfor;

    move-result-object v4

    iget-wide v0, v4, Lcom/konka/utilities/StorageInfor;->mFreeStrorage:J

    iget-object v4, p0, Lcom/konka/systemsetting/storage/StorageSdcard;->m_storage:Lcom/konka/utilities/Storage;

    invoke-virtual {v4}, Lcom/konka/utilities/Storage;->getDiskStorage()Lcom/konka/utilities/StorageInfor;

    move-result-object v4

    iget-wide v2, v4, Lcom/konka/utilities/StorageInfor;->mTotalStorage:J

    iget-object v4, p0, Lcom/konka/systemsetting/storage/StorageSdcard;->mContext:Lcom/konka/systemsetting/MainActivity;

    invoke-static {v4, v0, v1}, Lcom/konka/utilities/Tools;->sizeToAutoUnit(Landroid/content/Context;J)Ljava/lang/String;

    move-result-object v4

    iput-object v4, p0, Lcom/konka/systemsetting/storage/StorageSdcard;->m_strFreeSpace:Ljava/lang/String;

    iget-object v4, p0, Lcom/konka/systemsetting/storage/StorageSdcard;->mContext:Lcom/konka/systemsetting/MainActivity;

    invoke-static {v4, v2, v3}, Lcom/konka/utilities/Tools;->sizeToAutoUnit(Landroid/content/Context;J)Ljava/lang/String;

    move-result-object v4

    iput-object v4, p0, Lcom/konka/systemsetting/storage/StorageSdcard;->m_strTotalSpace:Ljava/lang/String;

    return-void
.end method

.method private setOnClickListener()V
    .locals 2

    new-instance v0, Lcom/konka/systemsetting/storage/StorageSdcard$2;

    invoke-direct {v0, p0}, Lcom/konka/systemsetting/storage/StorageSdcard$2;-><init>(Lcom/konka/systemsetting/storage/StorageSdcard;)V

    iget-object v1, p0, Lcom/konka/systemsetting/storage/StorageSdcard;->btn_itemUnmount:Landroid/widget/Button;

    invoke-virtual {v1, v0}, Landroid/widget/Button;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    iget-object v1, p0, Lcom/konka/systemsetting/storage/StorageSdcard;->btn_itemFormat:Landroid/widget/Button;

    invoke-virtual {v1, v0}, Landroid/widget/Button;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    return-void
.end method

.method private setOnFocusChangeListener()V
    .locals 2

    new-instance v0, Lcom/konka/systemsetting/storage/StorageSdcard$1;

    invoke-direct {v0, p0}, Lcom/konka/systemsetting/storage/StorageSdcard$1;-><init>(Lcom/konka/systemsetting/storage/StorageSdcard;)V

    iget-object v1, p0, Lcom/konka/systemsetting/storage/StorageSdcard;->btn_itemUnmount:Landroid/widget/Button;

    invoke-virtual {v1, v0}, Landroid/widget/Button;->setOnFocusChangeListener(Landroid/view/View$OnFocusChangeListener;)V

    iget-object v1, p0, Lcom/konka/systemsetting/storage/StorageSdcard;->btn_itemFormat:Landroid/widget/Button;

    invoke-virtual {v1, v0}, Landroid/widget/Button;->setOnFocusChangeListener(Landroid/view/View$OnFocusChangeListener;)V

    return-void
.end method

.method private unmountSDCard()V
    .locals 4

    invoke-static {}, Landroid/os/Environment;->getExternalStorageDirectory()Ljava/io/File;

    move-result-object v1

    invoke-virtual {v1}, Ljava/io/File;->getPath()Ljava/lang/String;

    move-result-object v0

    iget-object v1, p0, Lcom/konka/systemsetting/storage/StorageSdcard;->m_storageManager:Lcom/mstar/android/storage/MStorageManager;

    const/4 v2, 0x1

    const/4 v3, 0x0

    invoke-virtual {v1, v0, v2, v3}, Lcom/mstar/android/storage/MStorageManager;->unmountVolume(Ljava/lang/String;ZZ)V

    return-void
.end method

.method private updateUIState()V
    .locals 4

    const/4 v3, 0x1

    iget-object v2, p0, Lcom/konka/systemsetting/storage/StorageSdcard;->linearLayout_itemTotalSpace:Landroid/widget/LinearLayout;

    invoke-virtual {v2, v3}, Landroid/widget/LinearLayout;->getChildAt(I)Landroid/view/View;

    move-result-object v1

    check-cast v1, Landroid/widget/TextView;

    iget-object v2, p0, Lcom/konka/systemsetting/storage/StorageSdcard;->linearLayout_itemFreeSpace:Landroid/widget/LinearLayout;

    invoke-virtual {v2, v3}, Landroid/widget/LinearLayout;->getChildAt(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    iget-object v2, p0, Lcom/konka/systemsetting/storage/StorageSdcard;->m_strTotalSpace:Ljava/lang/String;

    invoke-virtual {v1, v2}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    iget-object v2, p0, Lcom/konka/systemsetting/storage/StorageSdcard;->m_strFreeSpace:Ljava/lang/String;

    invoke-virtual {v0, v2}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    return-void
.end method


# virtual methods
.method public OnDestory()V
    .locals 2

    iget-object v0, p0, Lcom/konka/systemsetting/storage/StorageSdcard;->mContext:Lcom/konka/systemsetting/MainActivity;

    iget-object v1, p0, Lcom/konka/systemsetting/storage/StorageSdcard;->usbReceiver:Lcom/konka/systemsetting/storage/StorageSdcard$UsbReceiver;

    invoke-virtual {v0, v1}, Lcom/konka/systemsetting/MainActivity;->unregisterReceiver(Landroid/content/BroadcastReceiver;)V

    return-void
.end method

.method public refreshMenu()V
    .locals 4

    const/4 v3, 0x4

    const/4 v2, 0x0

    const-string v0, "refresh menu"

    invoke-static {v0}, Lcom/konka/debuginfo/logPrint;->Debug(Ljava/lang/String;)V

    iget-object v0, p0, Lcom/konka/systemsetting/storage/StorageSdcard;->mContext:Lcom/konka/systemsetting/MainActivity;

    const v1, 0x7f090086    # com.konka.systemsetting.R.id.linearlayout_storage_tab_sdcard

    invoke-virtual {v0, v1}, Lcom/konka/systemsetting/MainActivity;->findViewById(I)Landroid/view/View;

    move-result-object v0

    invoke-virtual {v0}, Landroid/view/View;->requestFocus()Z

    iget-object v0, p0, Lcom/konka/systemsetting/storage/StorageSdcard;->m_storage:Lcom/konka/utilities/Storage;

    invoke-virtual {v0}, Lcom/konka/utilities/Storage;->isSDMounted()Z

    move-result v0

    if-nez v0, :cond_0

    const-string v0, "the sd card in not mounted"

    invoke-static {v0}, Lcom/konka/debuginfo/logPrint;->Debug(Ljava/lang/String;)V

    iget-object v0, p0, Lcom/konka/systemsetting/storage/StorageSdcard;->linearLayout_sdcardinfo:Landroid/widget/LinearLayout;

    invoke-virtual {v0, v3}, Landroid/widget/LinearLayout;->setVisibility(I)V

    const-string v0, "menu of sd card info is invisible"

    invoke-static {v0}, Lcom/konka/debuginfo/logPrint;->Debug(Ljava/lang/String;)V

    iget-object v0, p0, Lcom/konka/systemsetting/storage/StorageSdcard;->linearLayout_nosdcard:Landroid/widget/LinearLayout;

    invoke-virtual {v0, v2}, Landroid/widget/LinearLayout;->setVisibility(I)V

    const-string v0, "menu of no sd card is visible"

    invoke-static {v0}, Lcom/konka/debuginfo/logPrint;->Debug(Ljava/lang/String;)V

    :goto_0
    return-void

    :cond_0
    iget-object v0, p0, Lcom/konka/systemsetting/storage/StorageSdcard;->linearLayout_nosdcard:Landroid/widget/LinearLayout;

    invoke-virtual {v0, v3}, Landroid/widget/LinearLayout;->setVisibility(I)V

    iget-object v0, p0, Lcom/konka/systemsetting/storage/StorageSdcard;->linearLayout_sdcardinfo:Landroid/widget/LinearLayout;

    invoke-virtual {v0, v2}, Landroid/widget/LinearLayout;->setVisibility(I)V

    invoke-direct {p0}, Lcom/konka/systemsetting/storage/StorageSdcard;->initUIData()V

    invoke-direct {p0}, Lcom/konka/systemsetting/storage/StorageSdcard;->updateUIState()V

    goto :goto_0
.end method
