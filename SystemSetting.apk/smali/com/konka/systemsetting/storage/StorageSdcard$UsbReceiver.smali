.class public Lcom/konka/systemsetting/storage/StorageSdcard$UsbReceiver;
.super Landroid/content/BroadcastReceiver;
.source "StorageSdcard.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/konka/systemsetting/storage/StorageSdcard;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x1
    name = "UsbReceiver"
.end annotation


# instance fields
.field final synthetic this$0:Lcom/konka/systemsetting/storage/StorageSdcard;


# direct methods
.method public constructor <init>(Lcom/konka/systemsetting/storage/StorageSdcard;)V
    .locals 0

    iput-object p1, p0, Lcom/konka/systemsetting/storage/StorageSdcard$UsbReceiver;->this$0:Lcom/konka/systemsetting/storage/StorageSdcard;

    invoke-direct {p0}, Landroid/content/BroadcastReceiver;-><init>()V

    return-void
.end method


# virtual methods
.method public onReceive(Landroid/content/Context;Landroid/content/Intent;)V
    .locals 5
    .param p1    # Landroid/content/Context;
    .param p2    # Landroid/content/Intent;

    invoke-virtual {p2}, Landroid/content/Intent;->getAction()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p2}, Landroid/content/Intent;->getData()Landroid/net/Uri;

    move-result-object v2

    invoke-virtual {v2}, Landroid/net/Uri;->getPath()Ljava/lang/String;

    move-result-object v1

    const-string v3, "android.intent.action.MEDIA_MOUNTED"

    invoke-virtual {v0, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-eqz v3, :cond_2

    new-instance v3, Ljava/lang/StringBuilder;

    const-string v4, "Mounted:"

    invoke-direct {v3, v4}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v3, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v3}, Lcom/konka/debuginfo/logPrint;->Debug(Ljava/lang/String;)V

    iget-object v3, p0, Lcom/konka/systemsetting/storage/StorageSdcard$UsbReceiver;->this$0:Lcom/konka/systemsetting/storage/StorageSdcard;

    # getter for: Lcom/konka/systemsetting/storage/StorageSdcard;->mContext:Lcom/konka/systemsetting/MainActivity;
    invoke-static {v3}, Lcom/konka/systemsetting/storage/StorageSdcard;->access$0(Lcom/konka/systemsetting/storage/StorageSdcard;)Lcom/konka/systemsetting/MainActivity;

    move-result-object v3

    const v4, 0x7f0600c4    # com.konka.systemsetting.R.string.warning_mounted

    invoke-virtual {v3, v4}, Lcom/konka/systemsetting/MainActivity;->toastText(I)V

    iget-object v3, p0, Lcom/konka/systemsetting/storage/StorageSdcard$UsbReceiver;->this$0:Lcom/konka/systemsetting/storage/StorageSdcard;

    invoke-virtual {v3}, Lcom/konka/systemsetting/storage/StorageSdcard;->refreshMenu()V

    :cond_0
    :goto_0
    iget-object v3, p0, Lcom/konka/systemsetting/storage/StorageSdcard$UsbReceiver;->this$0:Lcom/konka/systemsetting/storage/StorageSdcard;

    # getter for: Lcom/konka/systemsetting/storage/StorageSdcard;->m_bWaittoFormat:Z
    invoke-static {v3}, Lcom/konka/systemsetting/storage/StorageSdcard;->access$1(Lcom/konka/systemsetting/storage/StorageSdcard;)Z

    move-result v3

    if-eqz v3, :cond_1

    iget-object v3, p0, Lcom/konka/systemsetting/storage/StorageSdcard$UsbReceiver;->this$0:Lcom/konka/systemsetting/storage/StorageSdcard;

    # getter for: Lcom/konka/systemsetting/storage/StorageSdcard;->m_strformatPath:Ljava/lang/String;
    invoke-static {v3}, Lcom/konka/systemsetting/storage/StorageSdcard;->access$2(Lcom/konka/systemsetting/storage/StorageSdcard;)Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v1, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-eqz v3, :cond_1

    iget-object v3, p0, Lcom/konka/systemsetting/storage/StorageSdcard$UsbReceiver;->this$0:Lcom/konka/systemsetting/storage/StorageSdcard;

    const/4 v4, 0x0

    invoke-static {v3, v4}, Lcom/konka/systemsetting/storage/StorageSdcard;->access$3(Lcom/konka/systemsetting/storage/StorageSdcard;Z)V

    iget-object v3, p0, Lcom/konka/systemsetting/storage/StorageSdcard$UsbReceiver;->this$0:Lcom/konka/systemsetting/storage/StorageSdcard;

    const/4 v4, 0x0

    invoke-static {v3, v4}, Lcom/konka/systemsetting/storage/StorageSdcard;->access$4(Lcom/konka/systemsetting/storage/StorageSdcard;Ljava/lang/String;)V

    const-string v3, "android.intent.action.MEDIA_UNMOUNTED"

    invoke-virtual {v0, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-nez v3, :cond_3

    new-instance v3, Ljava/lang/StringBuilder;

    const-string v4, "Fail to unmount "

    invoke-direct {v3, v4}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v3, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    const-string v4, " for format"

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v3}, Lcom/konka/debuginfo/logPrint;->Debug(Ljava/lang/String;)V

    :cond_1
    :goto_1
    return-void

    :cond_2
    const-string v3, "android.intent.action.MEDIA_UNMOUNTED"

    invoke-virtual {v0, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-eqz v3, :cond_0

    new-instance v3, Ljava/lang/StringBuilder;

    const-string v4, "Unmounted:"

    invoke-direct {v3, v4}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v3, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v3}, Lcom/konka/debuginfo/logPrint;->Debug(Ljava/lang/String;)V

    iget-object v3, p0, Lcom/konka/systemsetting/storage/StorageSdcard$UsbReceiver;->this$0:Lcom/konka/systemsetting/storage/StorageSdcard;

    # getter for: Lcom/konka/systemsetting/storage/StorageSdcard;->mContext:Lcom/konka/systemsetting/MainActivity;
    invoke-static {v3}, Lcom/konka/systemsetting/storage/StorageSdcard;->access$0(Lcom/konka/systemsetting/storage/StorageSdcard;)Lcom/konka/systemsetting/MainActivity;

    move-result-object v3

    const v4, 0x7f0600c6    # com.konka.systemsetting.R.string.warning_unmount_complete

    invoke-virtual {v3, v4}, Lcom/konka/systemsetting/MainActivity;->toastText(I)V

    iget-object v3, p0, Lcom/konka/systemsetting/storage/StorageSdcard$UsbReceiver;->this$0:Lcom/konka/systemsetting/storage/StorageSdcard;

    invoke-virtual {v3}, Lcom/konka/systemsetting/storage/StorageSdcard;->refreshMenu()V

    goto :goto_0

    :cond_3
    new-instance v3, Ljava/lang/StringBuilder;

    const-string v4, "Success to unmount "

    invoke-direct {v3, v4}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v3, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    const-string v4, " for format"

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v3}, Lcom/konka/debuginfo/logPrint;->Debug(Ljava/lang/String;)V

    iget-object v3, p0, Lcom/konka/systemsetting/storage/StorageSdcard$UsbReceiver;->this$0:Lcom/konka/systemsetting/storage/StorageSdcard;

    # getter for: Lcom/konka/systemsetting/storage/StorageSdcard;->m_storageManager:Lcom/mstar/android/storage/MStorageManager;
    invoke-static {v3}, Lcom/konka/systemsetting/storage/StorageSdcard;->access$5(Lcom/konka/systemsetting/storage/StorageSdcard;)Lcom/mstar/android/storage/MStorageManager;

    move-result-object v3

    invoke-virtual {v3, v1}, Lcom/mstar/android/storage/MStorageManager;->formatVolume(Ljava/lang/String;)Z

    move-result v3

    if-eqz v3, :cond_5

    new-instance v3, Ljava/lang/StringBuilder;

    const-string v4, "Success to format "

    invoke-direct {v3, v4}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v3, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v3}, Lcom/konka/debuginfo/logPrint;->Debug(Ljava/lang/String;)V

    iget-object v3, p0, Lcom/konka/systemsetting/storage/StorageSdcard$UsbReceiver;->this$0:Lcom/konka/systemsetting/storage/StorageSdcard;

    # getter for: Lcom/konka/systemsetting/storage/StorageSdcard;->m_storageManager:Lcom/mstar/android/storage/MStorageManager;
    invoke-static {v3}, Lcom/konka/systemsetting/storage/StorageSdcard;->access$5(Lcom/konka/systemsetting/storage/StorageSdcard;)Lcom/mstar/android/storage/MStorageManager;

    move-result-object v3

    invoke-virtual {v3, v1}, Lcom/mstar/android/storage/MStorageManager;->mountVolume(Ljava/lang/String;)Z

    move-result v3

    if-eqz v3, :cond_4

    new-instance v3, Ljava/lang/StringBuilder;

    const-string v4, "Success to mount "

    invoke-direct {v3, v4}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v3, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    const-string v4, " again"

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v3}, Lcom/konka/debuginfo/logPrint;->Debug(Ljava/lang/String;)V

    iget-object v3, p0, Lcom/konka/systemsetting/storage/StorageSdcard$UsbReceiver;->this$0:Lcom/konka/systemsetting/storage/StorageSdcard;

    # getter for: Lcom/konka/systemsetting/storage/StorageSdcard;->mContext:Lcom/konka/systemsetting/MainActivity;
    invoke-static {v3}, Lcom/konka/systemsetting/storage/StorageSdcard;->access$0(Lcom/konka/systemsetting/storage/StorageSdcard;)Lcom/konka/systemsetting/MainActivity;

    move-result-object v3

    const v4, 0x7f0600c8    # com.konka.systemsetting.R.string.warning_formate_complete

    invoke-virtual {v3, v4}, Lcom/konka/systemsetting/MainActivity;->toastText(I)V

    goto/16 :goto_1

    :cond_4
    new-instance v3, Ljava/lang/StringBuilder;

    const-string v4, "Fail to mount "

    invoke-direct {v3, v4}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v3, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    const-string v4, " again"

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v3}, Lcom/konka/debuginfo/logPrint;->Debug(Ljava/lang/String;)V

    goto/16 :goto_1

    :cond_5
    new-instance v3, Ljava/lang/StringBuilder;

    const-string v4, "Fail to format "

    invoke-direct {v3, v4}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v3, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v3}, Lcom/konka/debuginfo/logPrint;->Debug(Ljava/lang/String;)V

    goto/16 :goto_1
.end method
