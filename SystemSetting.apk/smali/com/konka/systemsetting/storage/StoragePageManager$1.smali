.class Lcom/konka/systemsetting/storage/StoragePageManager$1;
.super Ljava/lang/Object;
.source "StoragePageManager.java"

# interfaces
.implements Landroid/view/View$OnFocusChangeListener;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/konka/systemsetting/storage/StoragePageManager;->SetOnFocusChangeListener()V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/konka/systemsetting/storage/StoragePageManager;


# direct methods
.method constructor <init>(Lcom/konka/systemsetting/storage/StoragePageManager;)V
    .locals 0

    iput-object p1, p0, Lcom/konka/systemsetting/storage/StoragePageManager$1;->this$0:Lcom/konka/systemsetting/storage/StoragePageManager;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onFocusChange(Landroid/view/View;Z)V
    .locals 6
    .param p1    # Landroid/view/View;
    .param p2    # Z

    const/4 v5, 0x2

    const/4 v4, 0x1

    const/4 v3, 0x0

    invoke-virtual {p1}, Landroid/view/View;->getId()I

    move-result v0

    if-eqz p2, :cond_1

    packed-switch v0, :pswitch_data_0

    :goto_0
    return-void

    :pswitch_0
    iget-object v1, p0, Lcom/konka/systemsetting/storage/StoragePageManager$1;->this$0:Lcom/konka/systemsetting/storage/StoragePageManager;

    iget-object v2, p0, Lcom/konka/systemsetting/storage/StoragePageManager$1;->this$0:Lcom/konka/systemsetting/storage/StoragePageManager;

    # getter for: Lcom/konka/systemsetting/storage/StoragePageManager;->m_iCurPage:I
    invoke-static {v2}, Lcom/konka/systemsetting/storage/StoragePageManager;->access$0(Lcom/konka/systemsetting/storage/StoragePageManager;)I

    move-result v2

    invoke-static {v1, v2}, Lcom/konka/systemsetting/storage/StoragePageManager;->access$1(Lcom/konka/systemsetting/storage/StoragePageManager;I)V

    iget-object v1, p0, Lcom/konka/systemsetting/storage/StoragePageManager$1;->this$0:Lcom/konka/systemsetting/storage/StoragePageManager;

    invoke-static {v1, v3}, Lcom/konka/systemsetting/storage/StoragePageManager;->access$2(Lcom/konka/systemsetting/storage/StoragePageManager;I)V

    :goto_1
    iget-object v1, p0, Lcom/konka/systemsetting/storage/StoragePageManager$1;->this$0:Lcom/konka/systemsetting/storage/StoragePageManager;

    # getter for: Lcom/konka/systemsetting/storage/StoragePageManager;->m_iCurPage:I
    invoke-static {v1}, Lcom/konka/systemsetting/storage/StoragePageManager;->access$0(Lcom/konka/systemsetting/storage/StoragePageManager;)I

    move-result v1

    iget-object v2, p0, Lcom/konka/systemsetting/storage/StoragePageManager$1;->this$0:Lcom/konka/systemsetting/storage/StoragePageManager;

    # getter for: Lcom/konka/systemsetting/storage/StoragePageManager;->m_iLastFocusPage:I
    invoke-static {v2}, Lcom/konka/systemsetting/storage/StoragePageManager;->access$3(Lcom/konka/systemsetting/storage/StoragePageManager;)I

    move-result v2

    if-eq v1, v2, :cond_0

    iget-object v1, p0, Lcom/konka/systemsetting/storage/StoragePageManager$1;->this$0:Lcom/konka/systemsetting/storage/StoragePageManager;

    iget-object v2, p0, Lcom/konka/systemsetting/storage/StoragePageManager$1;->this$0:Lcom/konka/systemsetting/storage/StoragePageManager;

    # getter for: Lcom/konka/systemsetting/storage/StoragePageManager;->m_iLastFocusPage:I
    invoke-static {v2}, Lcom/konka/systemsetting/storage/StoragePageManager;->access$3(Lcom/konka/systemsetting/storage/StoragePageManager;)I

    move-result v2

    # invokes: Lcom/konka/systemsetting/storage/StoragePageManager;->changeItemStatus(II)V
    invoke-static {v1, v2, v5}, Lcom/konka/systemsetting/storage/StoragePageManager;->access$4(Lcom/konka/systemsetting/storage/StoragePageManager;II)V

    :cond_0
    iget-object v1, p0, Lcom/konka/systemsetting/storage/StoragePageManager$1;->this$0:Lcom/konka/systemsetting/storage/StoragePageManager;

    iget-object v2, p0, Lcom/konka/systemsetting/storage/StoragePageManager$1;->this$0:Lcom/konka/systemsetting/storage/StoragePageManager;

    # getter for: Lcom/konka/systemsetting/storage/StoragePageManager;->m_iCurPage:I
    invoke-static {v2}, Lcom/konka/systemsetting/storage/StoragePageManager;->access$0(Lcom/konka/systemsetting/storage/StoragePageManager;)I

    move-result v2

    # invokes: Lcom/konka/systemsetting/storage/StoragePageManager;->changeItemStatus(II)V
    invoke-static {v1, v2, v3}, Lcom/konka/systemsetting/storage/StoragePageManager;->access$4(Lcom/konka/systemsetting/storage/StoragePageManager;II)V

    iget-object v1, p0, Lcom/konka/systemsetting/storage/StoragePageManager$1;->this$0:Lcom/konka/systemsetting/storage/StoragePageManager;

    iget-object v2, p0, Lcom/konka/systemsetting/storage/StoragePageManager$1;->this$0:Lcom/konka/systemsetting/storage/StoragePageManager;

    # getter for: Lcom/konka/systemsetting/storage/StoragePageManager;->m_iCurPage:I
    invoke-static {v2}, Lcom/konka/systemsetting/storage/StoragePageManager;->access$0(Lcom/konka/systemsetting/storage/StoragePageManager;)I

    move-result v2

    invoke-virtual {v1, v2}, Lcom/konka/systemsetting/storage/StoragePageManager;->showPage(I)V

    goto :goto_0

    :pswitch_1
    iget-object v1, p0, Lcom/konka/systemsetting/storage/StoragePageManager$1;->this$0:Lcom/konka/systemsetting/storage/StoragePageManager;

    iget-object v2, p0, Lcom/konka/systemsetting/storage/StoragePageManager$1;->this$0:Lcom/konka/systemsetting/storage/StoragePageManager;

    # getter for: Lcom/konka/systemsetting/storage/StoragePageManager;->m_iCurPage:I
    invoke-static {v2}, Lcom/konka/systemsetting/storage/StoragePageManager;->access$0(Lcom/konka/systemsetting/storage/StoragePageManager;)I

    move-result v2

    invoke-static {v1, v2}, Lcom/konka/systemsetting/storage/StoragePageManager;->access$1(Lcom/konka/systemsetting/storage/StoragePageManager;I)V

    iget-object v1, p0, Lcom/konka/systemsetting/storage/StoragePageManager$1;->this$0:Lcom/konka/systemsetting/storage/StoragePageManager;

    invoke-static {v1, v4}, Lcom/konka/systemsetting/storage/StoragePageManager;->access$2(Lcom/konka/systemsetting/storage/StoragePageManager;I)V

    goto :goto_1

    :pswitch_2
    iget-object v1, p0, Lcom/konka/systemsetting/storage/StoragePageManager$1;->this$0:Lcom/konka/systemsetting/storage/StoragePageManager;

    iget-object v2, p0, Lcom/konka/systemsetting/storage/StoragePageManager$1;->this$0:Lcom/konka/systemsetting/storage/StoragePageManager;

    # getter for: Lcom/konka/systemsetting/storage/StoragePageManager;->m_iCurPage:I
    invoke-static {v2}, Lcom/konka/systemsetting/storage/StoragePageManager;->access$0(Lcom/konka/systemsetting/storage/StoragePageManager;)I

    move-result v2

    invoke-static {v1, v2}, Lcom/konka/systemsetting/storage/StoragePageManager;->access$1(Lcom/konka/systemsetting/storage/StoragePageManager;I)V

    iget-object v1, p0, Lcom/konka/systemsetting/storage/StoragePageManager$1;->this$0:Lcom/konka/systemsetting/storage/StoragePageManager;

    invoke-static {v1, v5}, Lcom/konka/systemsetting/storage/StoragePageManager;->access$2(Lcom/konka/systemsetting/storage/StoragePageManager;I)V

    goto :goto_1

    :cond_1
    iget-object v1, p0, Lcom/konka/systemsetting/storage/StoragePageManager$1;->this$0:Lcom/konka/systemsetting/storage/StoragePageManager;

    iget-object v2, p0, Lcom/konka/systemsetting/storage/StoragePageManager$1;->this$0:Lcom/konka/systemsetting/storage/StoragePageManager;

    # getter for: Lcom/konka/systemsetting/storage/StoragePageManager;->m_iCurPage:I
    invoke-static {v2}, Lcom/konka/systemsetting/storage/StoragePageManager;->access$0(Lcom/konka/systemsetting/storage/StoragePageManager;)I

    move-result v2

    # invokes: Lcom/konka/systemsetting/storage/StoragePageManager;->changeItemStatus(II)V
    invoke-static {v1, v2, v4}, Lcom/konka/systemsetting/storage/StoragePageManager;->access$4(Lcom/konka/systemsetting/storage/StoragePageManager;II)V

    goto :goto_0

    nop

    :pswitch_data_0
    .packed-switch 0x7f090085
        :pswitch_0    # com.konka.systemsetting.R.id.linearlayout_storage_tab_flash
        :pswitch_1    # com.konka.systemsetting.R.id.linearlayout_storage_tab_sdcard
        :pswitch_2    # com.konka.systemsetting.R.id.linearlayout_storage_tab_app_install_position
    .end packed-switch
.end method
