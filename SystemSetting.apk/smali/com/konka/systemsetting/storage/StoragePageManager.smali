.class public Lcom/konka/systemsetting/storage/StoragePageManager;
.super Ljava/lang/Object;
.source "StoragePageManager.java"


# instance fields
.field protected final PAGE_APP_INSTALL_POSITION:I

.field protected final PAGE_FLASH:I

.field protected final PAGE_SDCARD:I

.field protected final STATE_CURRENT_FOCUSED:I

.field protected final STATE_LAST_FOCUSED:I

.field protected final STATE_NO_FOCUSE:I

.field private mContext:Lcom/konka/systemsetting/MainActivity;

.field private m_ItemAppInstallPosition:Lcom/konka/systemsetting/storage/StorageAppInstallPosition;

.field private m_ItemFlash:Lcom/konka/systemsetting/storage/StorageFlash;

.field private m_ItemSdCard:Lcom/konka/systemsetting/storage/StorageSdcard;

.field private m_ViewHolder:Lcom/konka/systemsetting/ViewHolder;

.field private m_iCurPage:I

.field private m_iLastFocusPage:I


# direct methods
.method public constructor <init>(Lcom/konka/systemsetting/MainActivity;I)V
    .locals 3
    .param p1    # Lcom/konka/systemsetting/MainActivity;
    .param p2    # I

    const/4 v2, 0x2

    const/4 v1, 0x1

    const/4 v0, 0x0

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    iput v0, p0, Lcom/konka/systemsetting/storage/StoragePageManager;->STATE_CURRENT_FOCUSED:I

    iput v1, p0, Lcom/konka/systemsetting/storage/StoragePageManager;->STATE_LAST_FOCUSED:I

    iput v2, p0, Lcom/konka/systemsetting/storage/StoragePageManager;->STATE_NO_FOCUSE:I

    iput v0, p0, Lcom/konka/systemsetting/storage/StoragePageManager;->PAGE_FLASH:I

    iput v1, p0, Lcom/konka/systemsetting/storage/StoragePageManager;->PAGE_SDCARD:I

    iput v2, p0, Lcom/konka/systemsetting/storage/StoragePageManager;->PAGE_APP_INSTALL_POSITION:I

    iput v0, p0, Lcom/konka/systemsetting/storage/StoragePageManager;->m_iCurPage:I

    const/4 v0, -0x1

    iput v0, p0, Lcom/konka/systemsetting/storage/StoragePageManager;->m_iLastFocusPage:I

    iput-object p1, p0, Lcom/konka/systemsetting/storage/StoragePageManager;->mContext:Lcom/konka/systemsetting/MainActivity;

    new-instance v0, Lcom/konka/systemsetting/ViewHolder;

    iget-object v1, p0, Lcom/konka/systemsetting/storage/StoragePageManager;->mContext:Lcom/konka/systemsetting/MainActivity;

    invoke-direct {v0, v1}, Lcom/konka/systemsetting/ViewHolder;-><init>(Lcom/konka/systemsetting/MainActivity;)V

    iput-object v0, p0, Lcom/konka/systemsetting/storage/StoragePageManager;->m_ViewHolder:Lcom/konka/systemsetting/ViewHolder;

    iget-object v0, p0, Lcom/konka/systemsetting/storage/StoragePageManager;->m_ViewHolder:Lcom/konka/systemsetting/ViewHolder;

    invoke-virtual {v0}, Lcom/konka/systemsetting/ViewHolder;->findViewForPages()V

    sget-object v0, Lcom/konka/systemsetting/SysRootApp;->mPlatform:Ljava/lang/String;

    const-string v1, "6a800C"

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/konka/systemsetting/storage/StoragePageManager;->m_ViewHolder:Lcom/konka/systemsetting/ViewHolder;

    iget-object v0, v0, Lcom/konka/systemsetting/ViewHolder;->linearlayout_tabSdCard:Landroid/widget/LinearLayout;

    const/16 v1, 0x8

    invoke-virtual {v0, v1}, Landroid/widget/LinearLayout;->setVisibility(I)V

    iget-object v0, p0, Lcom/konka/systemsetting/storage/StoragePageManager;->m_ViewHolder:Lcom/konka/systemsetting/ViewHolder;

    iget-object v0, v0, Lcom/konka/systemsetting/ViewHolder;->linearlayout_tabFlash:Landroid/widget/LinearLayout;

    const v1, 0x7f090085    # com.konka.systemsetting.R.id.linearlayout_storage_tab_flash

    invoke-virtual {v0, v1}, Landroid/widget/LinearLayout;->setNextFocusDownId(I)V

    :cond_0
    iput p2, p0, Lcom/konka/systemsetting/storage/StoragePageManager;->m_iCurPage:I

    iget v0, p0, Lcom/konka/systemsetting/storage/StoragePageManager;->m_iCurPage:I

    invoke-virtual {p0, v0}, Lcom/konka/systemsetting/storage/StoragePageManager;->showPage(I)V

    invoke-direct {p0}, Lcom/konka/systemsetting/storage/StoragePageManager;->SetOnFocusChangeListener()V

    return-void
.end method

.method private SetOnFocusChangeListener()V
    .locals 2

    new-instance v0, Lcom/konka/systemsetting/storage/StoragePageManager$1;

    invoke-direct {v0, p0}, Lcom/konka/systemsetting/storage/StoragePageManager$1;-><init>(Lcom/konka/systemsetting/storage/StoragePageManager;)V

    iget-object v1, p0, Lcom/konka/systemsetting/storage/StoragePageManager;->m_ViewHolder:Lcom/konka/systemsetting/ViewHolder;

    iget-object v1, v1, Lcom/konka/systemsetting/ViewHolder;->linearlayout_tabFlash:Landroid/widget/LinearLayout;

    invoke-virtual {v1, v0}, Landroid/widget/LinearLayout;->setOnFocusChangeListener(Landroid/view/View$OnFocusChangeListener;)V

    iget-object v1, p0, Lcom/konka/systemsetting/storage/StoragePageManager;->m_ViewHolder:Lcom/konka/systemsetting/ViewHolder;

    iget-object v1, v1, Lcom/konka/systemsetting/ViewHolder;->linearlayout_tabSdCard:Landroid/widget/LinearLayout;

    invoke-virtual {v1, v0}, Landroid/widget/LinearLayout;->setOnFocusChangeListener(Landroid/view/View$OnFocusChangeListener;)V

    iget-object v1, p0, Lcom/konka/systemsetting/storage/StoragePageManager;->m_ViewHolder:Lcom/konka/systemsetting/ViewHolder;

    iget-object v1, v1, Lcom/konka/systemsetting/ViewHolder;->linearlayout_tabAppInstallPosition:Landroid/widget/LinearLayout;

    invoke-virtual {v1, v0}, Landroid/widget/LinearLayout;->setOnFocusChangeListener(Landroid/view/View$OnFocusChangeListener;)V

    return-void
.end method

.method static synthetic access$0(Lcom/konka/systemsetting/storage/StoragePageManager;)I
    .locals 1

    iget v0, p0, Lcom/konka/systemsetting/storage/StoragePageManager;->m_iCurPage:I

    return v0
.end method

.method static synthetic access$1(Lcom/konka/systemsetting/storage/StoragePageManager;I)V
    .locals 0

    iput p1, p0, Lcom/konka/systemsetting/storage/StoragePageManager;->m_iLastFocusPage:I

    return-void
.end method

.method static synthetic access$2(Lcom/konka/systemsetting/storage/StoragePageManager;I)V
    .locals 0

    iput p1, p0, Lcom/konka/systemsetting/storage/StoragePageManager;->m_iCurPage:I

    return-void
.end method

.method static synthetic access$3(Lcom/konka/systemsetting/storage/StoragePageManager;)I
    .locals 1

    iget v0, p0, Lcom/konka/systemsetting/storage/StoragePageManager;->m_iLastFocusPage:I

    return v0
.end method

.method static synthetic access$4(Lcom/konka/systemsetting/storage/StoragePageManager;II)V
    .locals 0

    invoke-direct {p0, p1, p2}, Lcom/konka/systemsetting/storage/StoragePageManager;->changeItemStatus(II)V

    return-void
.end method

.method private changeItemStatus(II)V
    .locals 6
    .param p1    # I
    .param p2    # I

    const/4 v5, 0x0

    const v4, -0x87e2

    const/4 v3, -0x1

    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "change item statusid="

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, " iState="

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v1}, Lcom/konka/debuginfo/logPrint;->Debug(Ljava/lang/String;)V

    packed-switch p1, :pswitch_data_0

    const-string v1, "The item id is wrong!"

    invoke-static {v1}, Lcom/konka/debuginfo/logPrint;->Warning(Ljava/lang/String;)V

    :goto_0
    return-void

    :pswitch_0
    iget-object v1, p0, Lcom/konka/systemsetting/storage/StoragePageManager;->m_ViewHolder:Lcom/konka/systemsetting/ViewHolder;

    iget-object v1, v1, Lcom/konka/systemsetting/ViewHolder;->linearlayout_tabFlash:Landroid/widget/LinearLayout;

    invoke-virtual {v1, v5}, Landroid/widget/LinearLayout;->getChildAt(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    packed-switch p2, :pswitch_data_1

    const-string v1, "The item state is wrong!"

    invoke-static {v1}, Lcom/konka/debuginfo/logPrint;->Warning(Ljava/lang/String;)V

    goto :goto_0

    :pswitch_1
    invoke-virtual {v0, v3}, Landroid/widget/TextView;->setTextColor(I)V

    goto :goto_0

    :pswitch_2
    invoke-virtual {v0, v4}, Landroid/widget/TextView;->setTextColor(I)V

    goto :goto_0

    :pswitch_3
    invoke-virtual {v0, v3}, Landroid/widget/TextView;->setTextColor(I)V

    goto :goto_0

    :pswitch_4
    iget-object v1, p0, Lcom/konka/systemsetting/storage/StoragePageManager;->m_ViewHolder:Lcom/konka/systemsetting/ViewHolder;

    iget-object v1, v1, Lcom/konka/systemsetting/ViewHolder;->linearlayout_tabSdCard:Landroid/widget/LinearLayout;

    invoke-virtual {v1, v5}, Landroid/widget/LinearLayout;->getChildAt(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    packed-switch p2, :pswitch_data_2

    const-string v1, "The item state is wrong!"

    invoke-static {v1}, Lcom/konka/debuginfo/logPrint;->Warning(Ljava/lang/String;)V

    goto :goto_0

    :pswitch_5
    invoke-virtual {v0, v3}, Landroid/widget/TextView;->setTextColor(I)V

    goto :goto_0

    :pswitch_6
    invoke-virtual {v0, v4}, Landroid/widget/TextView;->setTextColor(I)V

    goto :goto_0

    :pswitch_7
    invoke-virtual {v0, v3}, Landroid/widget/TextView;->setTextColor(I)V

    goto :goto_0

    :pswitch_8
    iget-object v1, p0, Lcom/konka/systemsetting/storage/StoragePageManager;->m_ViewHolder:Lcom/konka/systemsetting/ViewHolder;

    iget-object v1, v1, Lcom/konka/systemsetting/ViewHolder;->linearlayout_tabAppInstallPosition:Landroid/widget/LinearLayout;

    invoke-virtual {v1, v5}, Landroid/widget/LinearLayout;->getChildAt(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    packed-switch p2, :pswitch_data_3

    const-string v1, "The item state is wrong!"

    invoke-static {v1}, Lcom/konka/debuginfo/logPrint;->Warning(Ljava/lang/String;)V

    goto :goto_0

    :pswitch_9
    invoke-virtual {v0, v3}, Landroid/widget/TextView;->setTextColor(I)V

    goto :goto_0

    :pswitch_a
    invoke-virtual {v0, v4}, Landroid/widget/TextView;->setTextColor(I)V

    goto :goto_0

    :pswitch_b
    invoke-virtual {v0, v3}, Landroid/widget/TextView;->setTextColor(I)V

    goto :goto_0

    nop

    :pswitch_data_0
    .packed-switch 0x0
        :pswitch_0
        :pswitch_4
        :pswitch_8
    .end packed-switch

    :pswitch_data_1
    .packed-switch 0x0
        :pswitch_1
        :pswitch_2
        :pswitch_3
    .end packed-switch

    :pswitch_data_2
    .packed-switch 0x0
        :pswitch_5
        :pswitch_6
        :pswitch_7
    .end packed-switch

    :pswitch_data_3
    .packed-switch 0x0
        :pswitch_9
        :pswitch_a
        :pswitch_b
    .end packed-switch
.end method


# virtual methods
.method public OnDestory()V
    .locals 1

    iget-object v0, p0, Lcom/konka/systemsetting/storage/StoragePageManager;->m_ItemSdCard:Lcom/konka/systemsetting/storage/StorageSdcard;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/konka/systemsetting/storage/StoragePageManager;->m_ItemSdCard:Lcom/konka/systemsetting/storage/StorageSdcard;

    invoke-virtual {v0}, Lcom/konka/systemsetting/storage/StorageSdcard;->OnDestory()V

    :goto_0
    return-void

    :cond_0
    const-string v0, "the m_ItemSdCard is null"

    invoke-static {v0}, Lcom/konka/debuginfo/logPrint;->Debug(Ljava/lang/String;)V

    goto :goto_0
.end method

.method public resetItemStatus()V
    .locals 2

    const/4 v1, 0x2

    iget v0, p0, Lcom/konka/systemsetting/storage/StoragePageManager;->m_iCurPage:I

    invoke-direct {p0, v0, v1}, Lcom/konka/systemsetting/storage/StoragePageManager;->changeItemStatus(II)V

    iget v0, p0, Lcom/konka/systemsetting/storage/StoragePageManager;->m_iLastFocusPage:I

    invoke-direct {p0, v0, v1}, Lcom/konka/systemsetting/storage/StoragePageManager;->changeItemStatus(II)V

    return-void
.end method

.method protected setLastFocus(II)V
    .locals 0
    .param p1    # I
    .param p2    # I

    invoke-direct {p0, p1, p2}, Lcom/konka/systemsetting/storage/StoragePageManager;->changeItemStatus(II)V

    return-void
.end method

.method public showPage(I)V
    .locals 2
    .param p1    # I

    iget-object v0, p0, Lcom/konka/systemsetting/storage/StoragePageManager;->m_ViewHolder:Lcom/konka/systemsetting/ViewHolder;

    iget-object v0, v0, Lcom/konka/systemsetting/ViewHolder;->viewflipper_storage:Landroid/widget/ViewFlipper;

    invoke-virtual {v0, p1}, Landroid/widget/ViewFlipper;->setDisplayedChild(I)V

    packed-switch p1, :pswitch_data_0

    :cond_0
    :goto_0
    return-void

    :pswitch_0
    iget-object v0, p0, Lcom/konka/systemsetting/storage/StoragePageManager;->m_ItemFlash:Lcom/konka/systemsetting/storage/StorageFlash;

    if-nez v0, :cond_0

    new-instance v0, Lcom/konka/systemsetting/storage/StorageFlash;

    iget-object v1, p0, Lcom/konka/systemsetting/storage/StoragePageManager;->mContext:Lcom/konka/systemsetting/MainActivity;

    invoke-direct {v0, v1}, Lcom/konka/systemsetting/storage/StorageFlash;-><init>(Lcom/konka/systemsetting/MainActivity;)V

    iput-object v0, p0, Lcom/konka/systemsetting/storage/StoragePageManager;->m_ItemFlash:Lcom/konka/systemsetting/storage/StorageFlash;

    goto :goto_0

    :pswitch_1
    iget-object v0, p0, Lcom/konka/systemsetting/storage/StoragePageManager;->m_ItemSdCard:Lcom/konka/systemsetting/storage/StorageSdcard;

    if-nez v0, :cond_1

    const-string v0, "the sdCard page is null,so create"

    invoke-static {v0}, Lcom/konka/debuginfo/logPrint;->Debug(Ljava/lang/String;)V

    new-instance v0, Lcom/konka/systemsetting/storage/StorageSdcard;

    iget-object v1, p0, Lcom/konka/systemsetting/storage/StoragePageManager;->mContext:Lcom/konka/systemsetting/MainActivity;

    invoke-direct {v0, v1}, Lcom/konka/systemsetting/storage/StorageSdcard;-><init>(Lcom/konka/systemsetting/MainActivity;)V

    iput-object v0, p0, Lcom/konka/systemsetting/storage/StoragePageManager;->m_ItemSdCard:Lcom/konka/systemsetting/storage/StorageSdcard;

    iget-object v0, p0, Lcom/konka/systemsetting/storage/StoragePageManager;->m_ViewHolder:Lcom/konka/systemsetting/ViewHolder;

    iget-object v0, v0, Lcom/konka/systemsetting/ViewHolder;->linearlayout_tabSdCard:Landroid/widget/LinearLayout;

    invoke-virtual {v0}, Landroid/widget/LinearLayout;->requestFocus()Z

    goto :goto_0

    :cond_1
    const-string v0, "the sdCard page has been create,so no need to create"

    invoke-static {v0}, Lcom/konka/debuginfo/logPrint;->Debug(Ljava/lang/String;)V

    iget-object v0, p0, Lcom/konka/systemsetting/storage/StoragePageManager;->m_ItemSdCard:Lcom/konka/systemsetting/storage/StorageSdcard;

    invoke-virtual {v0}, Lcom/konka/systemsetting/storage/StorageSdcard;->refreshMenu()V

    goto :goto_0

    :pswitch_2
    iget-object v0, p0, Lcom/konka/systemsetting/storage/StoragePageManager;->m_ItemAppInstallPosition:Lcom/konka/systemsetting/storage/StorageAppInstallPosition;

    if-nez v0, :cond_0

    new-instance v0, Lcom/konka/systemsetting/storage/StorageAppInstallPosition;

    iget-object v1, p0, Lcom/konka/systemsetting/storage/StoragePageManager;->mContext:Lcom/konka/systemsetting/MainActivity;

    invoke-direct {v0, v1}, Lcom/konka/systemsetting/storage/StorageAppInstallPosition;-><init>(Lcom/konka/systemsetting/MainActivity;)V

    iput-object v0, p0, Lcom/konka/systemsetting/storage/StoragePageManager;->m_ItemAppInstallPosition:Lcom/konka/systemsetting/storage/StorageAppInstallPosition;

    goto :goto_0

    :pswitch_data_0
    .packed-switch 0x0
        :pswitch_0
        :pswitch_1
        :pswitch_2
    .end packed-switch
.end method
