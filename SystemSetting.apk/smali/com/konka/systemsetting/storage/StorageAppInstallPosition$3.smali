.class Lcom/konka/systemsetting/storage/StorageAppInstallPosition$3;
.super Ljava/lang/Object;
.source "StorageAppInstallPosition.java"

# interfaces
.implements Landroid/content/DialogInterface$OnClickListener;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/konka/systemsetting/storage/StorageAppInstallPosition;->showLanguageSettingDialog()V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/konka/systemsetting/storage/StorageAppInstallPosition;


# direct methods
.method constructor <init>(Lcom/konka/systemsetting/storage/StorageAppInstallPosition;)V
    .locals 0

    iput-object p1, p0, Lcom/konka/systemsetting/storage/StorageAppInstallPosition$3;->this$0:Lcom/konka/systemsetting/storage/StorageAppInstallPosition;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onClick(Landroid/content/DialogInterface;I)V
    .locals 3
    .param p1    # Landroid/content/DialogInterface;
    .param p2    # I

    const/4 v0, 0x0

    iget-object v1, p0, Lcom/konka/systemsetting/storage/StorageAppInstallPosition$3;->this$0:Lcom/konka/systemsetting/storage/StorageAppInstallPosition;

    invoke-static {v1, p2}, Lcom/konka/systemsetting/storage/StorageAppInstallPosition;->access$2(Lcom/konka/systemsetting/storage/StorageAppInstallPosition;I)V

    if-nez p2, :cond_1

    const/4 v0, 0x1

    :cond_0
    :goto_0
    iget-object v1, p0, Lcom/konka/systemsetting/storage/StorageAppInstallPosition$3;->this$0:Lcom/konka/systemsetting/storage/StorageAppInstallPosition;

    # getter for: Lcom/konka/systemsetting/storage/StorageAppInstallPosition;->mContext:Lcom/konka/systemsetting/MainActivity;
    invoke-static {v1}, Lcom/konka/systemsetting/storage/StorageAppInstallPosition;->access$3(Lcom/konka/systemsetting/storage/StorageAppInstallPosition;)Lcom/konka/systemsetting/MainActivity;

    move-result-object v1

    invoke-virtual {v1}, Lcom/konka/systemsetting/MainActivity;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v1

    const-string v2, "install_app_location_type"

    invoke-static {v1, v2, v0}, Landroid/provider/Settings$Secure;->putInt(Landroid/content/ContentResolver;Ljava/lang/String;I)Z

    iget-object v1, p0, Lcom/konka/systemsetting/storage/StorageAppInstallPosition$3;->this$0:Lcom/konka/systemsetting/storage/StorageAppInstallPosition;

    # invokes: Lcom/konka/systemsetting/storage/StorageAppInstallPosition;->updateUI()V
    invoke-static {v1}, Lcom/konka/systemsetting/storage/StorageAppInstallPosition;->access$1(Lcom/konka/systemsetting/storage/StorageAppInstallPosition;)V

    invoke-interface {p1}, Landroid/content/DialogInterface;->dismiss()V

    return-void

    :cond_1
    const/4 v1, 0x1

    if-ne p2, v1, :cond_0

    const/4 v0, 0x2

    goto :goto_0
.end method
