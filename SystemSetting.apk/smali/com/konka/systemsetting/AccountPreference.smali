.class public Lcom/konka/systemsetting/AccountPreference;
.super Landroid/preference/Preference;
.source "AccountPreference.java"


# static fields
.field public static final SYNC_DISABLED:I = 0x1

.field public static final SYNC_ENABLED:I = 0x0

.field public static final SYNC_ERROR:I = 0x2

.field private static final TAG:Ljava/lang/String; = "AccountPreference"


# instance fields
.field private mAccount:Landroid/accounts/Account;

.field private mAuthorities:Ljava/util/ArrayList;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/ArrayList",
            "<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation
.end field

.field private mProviderIcon:Landroid/graphics/drawable/Drawable;

.field private mProviderIconView:Landroid/widget/ImageView;

.field private mStatus:I

.field private mSyncStatusIcon:Landroid/widget/ImageView;


# direct methods
.method public constructor <init>(Landroid/content/Context;Landroid/accounts/Account;Landroid/graphics/drawable/Drawable;Ljava/util/ArrayList;)V
    .locals 1
    .param p1    # Landroid/content/Context;
    .param p2    # Landroid/accounts/Account;
    .param p3    # Landroid/graphics/drawable/Drawable;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Landroid/content/Context;",
            "Landroid/accounts/Account;",
            "Landroid/graphics/drawable/Drawable;",
            "Ljava/util/ArrayList",
            "<",
            "Ljava/lang/String;",
            ">;)V"
        }
    .end annotation

    invoke-direct {p0, p1}, Landroid/preference/Preference;-><init>(Landroid/content/Context;)V

    iput-object p2, p0, Lcom/konka/systemsetting/AccountPreference;->mAccount:Landroid/accounts/Account;

    iput-object p4, p0, Lcom/konka/systemsetting/AccountPreference;->mAuthorities:Ljava/util/ArrayList;

    iput-object p3, p0, Lcom/konka/systemsetting/AccountPreference;->mProviderIcon:Landroid/graphics/drawable/Drawable;

    const/high16 v0, 0x7f030000    # com.konka.systemsetting.R.layout.account_preference

    invoke-virtual {p0, v0}, Lcom/konka/systemsetting/AccountPreference;->setWidgetLayoutResource(I)V

    iget-object v0, p0, Lcom/konka/systemsetting/AccountPreference;->mAccount:Landroid/accounts/Account;

    iget-object v0, v0, Landroid/accounts/Account;->name:Ljava/lang/String;

    invoke-virtual {p0, v0}, Lcom/konka/systemsetting/AccountPreference;->setTitle(Ljava/lang/CharSequence;)V

    const-string v0, ""

    invoke-virtual {p0, v0}, Lcom/konka/systemsetting/AccountPreference;->setSummary(Ljava/lang/CharSequence;)V

    const/4 v0, 0x0

    invoke-virtual {p0, v0}, Lcom/konka/systemsetting/AccountPreference;->setPersistent(Z)V

    const/4 v0, 0x1

    invoke-virtual {p0, v0}, Lcom/konka/systemsetting/AccountPreference;->setSyncStatus(I)V

    iget-object v0, p0, Lcom/konka/systemsetting/AccountPreference;->mProviderIcon:Landroid/graphics/drawable/Drawable;

    invoke-virtual {p0, v0}, Lcom/konka/systemsetting/AccountPreference;->setIcon(Landroid/graphics/drawable/Drawable;)V

    return-void
.end method

.method private getSyncContentDescription(I)Ljava/lang/String;
    .locals 4
    .param p1    # I

    const v3, 0x7f0600f7    # com.konka.systemsetting.R.string.accessibility_sync_error

    packed-switch p1, :pswitch_data_0

    const-string v0, "AccountPreference"

    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "Unknown sync status: "

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    invoke-virtual {p0}, Lcom/konka/systemsetting/AccountPreference;->getContext()Landroid/content/Context;

    move-result-object v0

    invoke-virtual {v0, v3}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v0

    :goto_0
    return-object v0

    :pswitch_0
    invoke-virtual {p0}, Lcom/konka/systemsetting/AccountPreference;->getContext()Landroid/content/Context;

    move-result-object v0

    const v1, 0x7f0600f5    # com.konka.systemsetting.R.string.accessibility_sync_enabled

    invoke-virtual {v0, v1}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v0

    goto :goto_0

    :pswitch_1
    invoke-virtual {p0}, Lcom/konka/systemsetting/AccountPreference;->getContext()Landroid/content/Context;

    move-result-object v0

    const v1, 0x7f0600f6    # com.konka.systemsetting.R.string.accessibility_sync_disabled

    invoke-virtual {v0, v1}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v0

    goto :goto_0

    :pswitch_2
    invoke-virtual {p0}, Lcom/konka/systemsetting/AccountPreference;->getContext()Landroid/content/Context;

    move-result-object v0

    invoke-virtual {v0, v3}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v0

    goto :goto_0

    :pswitch_data_0
    .packed-switch 0x0
        :pswitch_0
        :pswitch_1
        :pswitch_2
    .end packed-switch
.end method

.method private getSyncStatusIcon(I)I
    .locals 4
    .param p1    # I

    packed-switch p1, :pswitch_data_0

    const v0, 0x7f02000e    # com.konka.systemsetting.R.drawable.ic_sync_red_holo

    const-string v1, "AccountPreference"

    new-instance v2, Ljava/lang/StringBuilder;

    const-string v3, "Unknown sync status: "

    invoke-direct {v2, v3}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v2, p1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    :goto_0
    return v0

    :pswitch_0
    const v0, 0x7f02000c    # com.konka.systemsetting.R.drawable.ic_sync_green_holo

    goto :goto_0

    :pswitch_1
    const v0, 0x7f02000d    # com.konka.systemsetting.R.drawable.ic_sync_grey_holo

    goto :goto_0

    :pswitch_2
    const v0, 0x7f02000e    # com.konka.systemsetting.R.drawable.ic_sync_red_holo

    goto :goto_0

    nop

    :pswitch_data_0
    .packed-switch 0x0
        :pswitch_0
        :pswitch_1
        :pswitch_2
    .end packed-switch
.end method

.method private getSyncStatusMessage(I)I
    .locals 4
    .param p1    # I

    packed-switch p1, :pswitch_data_0

    const v0, 0x7f0600f4    # com.konka.systemsetting.R.string.sync_error

    const-string v1, "AccountPreference"

    new-instance v2, Ljava/lang/StringBuilder;

    const-string v3, "Unknown sync status: "

    invoke-direct {v2, v3}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v2, p1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    :goto_0
    return v0

    :pswitch_0
    const v0, 0x7f0600f2    # com.konka.systemsetting.R.string.sync_enabled

    goto :goto_0

    :pswitch_1
    const v0, 0x7f0600f3    # com.konka.systemsetting.R.string.sync_disabled

    goto :goto_0

    :pswitch_2
    const v0, 0x7f0600f4    # com.konka.systemsetting.R.string.sync_error

    goto :goto_0

    nop

    :pswitch_data_0
    .packed-switch 0x0
        :pswitch_0
        :pswitch_1
        :pswitch_2
    .end packed-switch
.end method


# virtual methods
.method public compareTo(Landroid/preference/Preference;)I
    .locals 2
    .param p1    # Landroid/preference/Preference;

    instance-of v0, p1, Lcom/konka/systemsetting/AccountPreference;

    if-nez v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    iget-object v0, p0, Lcom/konka/systemsetting/AccountPreference;->mAccount:Landroid/accounts/Account;

    iget-object v0, v0, Landroid/accounts/Account;->name:Ljava/lang/String;

    check-cast p1, Lcom/konka/systemsetting/AccountPreference;

    iget-object v1, p1, Lcom/konka/systemsetting/AccountPreference;->mAccount:Landroid/accounts/Account;

    iget-object v1, v1, Landroid/accounts/Account;->name:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/String;->compareTo(Ljava/lang/String;)I

    move-result v0

    goto :goto_0
.end method

.method public getAccount()Landroid/accounts/Account;
    .locals 1

    iget-object v0, p0, Lcom/konka/systemsetting/AccountPreference;->mAccount:Landroid/accounts/Account;

    return-object v0
.end method

.method public getAuthorities()Ljava/util/ArrayList;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/ArrayList",
            "<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation

    iget-object v0, p0, Lcom/konka/systemsetting/AccountPreference;->mAuthorities:Ljava/util/ArrayList;

    return-object v0
.end method

.method protected onBindView(Landroid/view/View;)V
    .locals 2
    .param p1    # Landroid/view/View;

    invoke-super {p0, p1}, Landroid/preference/Preference;->onBindView(Landroid/view/View;)V

    iget v0, p0, Lcom/konka/systemsetting/AccountPreference;->mStatus:I

    invoke-direct {p0, v0}, Lcom/konka/systemsetting/AccountPreference;->getSyncStatusMessage(I)I

    move-result v0

    invoke-virtual {p0, v0}, Lcom/konka/systemsetting/AccountPreference;->setSummary(I)V

    const/high16 v0, 0x7f090000    # com.konka.systemsetting.R.id.syncStatusIcon

    invoke-virtual {p1, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/ImageView;

    iput-object v0, p0, Lcom/konka/systemsetting/AccountPreference;->mSyncStatusIcon:Landroid/widget/ImageView;

    iget-object v0, p0, Lcom/konka/systemsetting/AccountPreference;->mSyncStatusIcon:Landroid/widget/ImageView;

    iget v1, p0, Lcom/konka/systemsetting/AccountPreference;->mStatus:I

    invoke-direct {p0, v1}, Lcom/konka/systemsetting/AccountPreference;->getSyncStatusIcon(I)I

    move-result v1

    invoke-virtual {v0, v1}, Landroid/widget/ImageView;->setImageResource(I)V

    iget-object v0, p0, Lcom/konka/systemsetting/AccountPreference;->mSyncStatusIcon:Landroid/widget/ImageView;

    iget v1, p0, Lcom/konka/systemsetting/AccountPreference;->mStatus:I

    invoke-direct {p0, v1}, Lcom/konka/systemsetting/AccountPreference;->getSyncContentDescription(I)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/widget/ImageView;->setContentDescription(Ljava/lang/CharSequence;)V

    return-void
.end method

.method public setProviderIcon(Landroid/graphics/drawable/Drawable;)V
    .locals 1
    .param p1    # Landroid/graphics/drawable/Drawable;

    iput-object p1, p0, Lcom/konka/systemsetting/AccountPreference;->mProviderIcon:Landroid/graphics/drawable/Drawable;

    iget-object v0, p0, Lcom/konka/systemsetting/AccountPreference;->mProviderIconView:Landroid/widget/ImageView;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/konka/systemsetting/AccountPreference;->mProviderIconView:Landroid/widget/ImageView;

    invoke-virtual {v0, p1}, Landroid/widget/ImageView;->setImageDrawable(Landroid/graphics/drawable/Drawable;)V

    :cond_0
    return-void
.end method

.method public setSyncStatus(I)V
    .locals 2
    .param p1    # I

    iput p1, p0, Lcom/konka/systemsetting/AccountPreference;->mStatus:I

    iget-object v0, p0, Lcom/konka/systemsetting/AccountPreference;->mSyncStatusIcon:Landroid/widget/ImageView;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/konka/systemsetting/AccountPreference;->mSyncStatusIcon:Landroid/widget/ImageView;

    invoke-direct {p0, p1}, Lcom/konka/systemsetting/AccountPreference;->getSyncStatusIcon(I)I

    move-result v1

    invoke-virtual {v0, v1}, Landroid/widget/ImageView;->setImageResource(I)V

    :cond_0
    invoke-direct {p0, p1}, Lcom/konka/systemsetting/AccountPreference;->getSyncStatusMessage(I)I

    move-result v0

    invoke-virtual {p0, v0}, Lcom/konka/systemsetting/AccountPreference;->setSummary(I)V

    return-void
.end method
