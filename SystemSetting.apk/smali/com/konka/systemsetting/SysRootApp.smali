.class public Lcom/konka/systemsetting/SysRootApp;
.super Landroid/app/Application;
.source "SysRootApp.java"


# static fields
.field private static audioSkin:Lcom/mstar/android/tv/TvAudioManager;

.field private static commonSkin:Lcom/mstar/android/tv/TvCommonManager;

.field public static isCiCardSupportable:Z

.field public static isSDCardSupportable:Z

.field public static mPlatform:Ljava/lang/String;

.field private static mSysHandler:Lcom/konka/systemsetting/SysHandler;

.field private static pictureSkin:Lcom/mstar/android/tv/TvPictureManager;

.field private static timerSkin:Lcom/mstar/android/tv/TvTimerManager;

.field private static tvDeskProvider:Lcom/konka/kkinterface/tv/TvDeskProvider;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    const/4 v0, 0x0

    sput-object v0, Lcom/konka/systemsetting/SysRootApp;->mSysHandler:Lcom/konka/systemsetting/SysHandler;

    return-void
.end method

.method public constructor <init>()V
    .locals 0

    invoke-direct {p0}, Landroid/app/Application;-><init>()V

    return-void
.end method

.method public static getAudioSkin()Lcom/mstar/android/tv/TvAudioManager;
    .locals 1

    sget-object v0, Lcom/konka/systemsetting/SysRootApp;->audioSkin:Lcom/mstar/android/tv/TvAudioManager;

    return-object v0
.end method

.method public static getCommonSkin()Lcom/mstar/android/tv/TvCommonManager;
    .locals 1

    sget-object v0, Lcom/konka/systemsetting/SysRootApp;->commonSkin:Lcom/mstar/android/tv/TvCommonManager;

    return-object v0
.end method

.method public static getPictureSkin()Lcom/mstar/android/tv/TvPictureManager;
    .locals 1

    sget-object v0, Lcom/konka/systemsetting/SysRootApp;->pictureSkin:Lcom/mstar/android/tv/TvPictureManager;

    return-object v0
.end method

.method public static getSysHandler()Lcom/konka/systemsetting/SysHandler;
    .locals 1

    sget-object v0, Lcom/konka/systemsetting/SysRootApp;->mSysHandler:Lcom/konka/systemsetting/SysHandler;

    return-object v0
.end method

.method public static getTimerSkin()Lcom/mstar/android/tv/TvTimerManager;
    .locals 1

    sget-object v0, Lcom/konka/systemsetting/SysRootApp;->timerSkin:Lcom/mstar/android/tv/TvTimerManager;

    return-object v0
.end method

.method public static getTvDeskProvider()Lcom/konka/kkinterface/tv/TvDeskProvider;
    .locals 1

    sget-object v0, Lcom/konka/systemsetting/SysRootApp;->tvDeskProvider:Lcom/konka/kkinterface/tv/TvDeskProvider;

    return-object v0
.end method

.method private initAutoTimeSettings()V
    .locals 10

    const/4 v7, 0x0

    const/4 v6, 0x1

    invoke-virtual {p0}, Lcom/konka/systemsetting/SysRootApp;->getApplicationContext()Landroid/content/Context;

    move-result-object v5

    const-string v8, "userSettings"

    invoke-virtual {v5, v8, v7}, Landroid/content/Context;->getSharedPreferences(Ljava/lang/String;I)Landroid/content/SharedPreferences;

    move-result-object v4

    sget-object v5, Lcom/konka/systemsetting/SysRootApp;->tvDeskProvider:Lcom/konka/kkinterface/tv/TvDeskProvider;

    invoke-interface {v5}, Lcom/konka/kkinterface/tv/TvDeskProvider;->getSettingManagerInstance()Lcom/konka/kkinterface/tv/SettingDesk;

    move-result-object v5

    invoke-interface {v5}, Lcom/konka/kkinterface/tv/SettingDesk;->GetSystemAutoTimeType()I

    move-result v2

    const-string v5, "[SystemSetting trace auto time]"

    new-instance v8, Ljava/lang/StringBuilder;

    const-string v9, " fetch auto time index "

    invoke-direct {v8, v9}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v8, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v8

    invoke-virtual {v8}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v8

    invoke-static {v5, v8}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    invoke-virtual {p0}, Lcom/konka/systemsetting/SysRootApp;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v8

    const-string v9, "auto_time"

    if-ne v2, v6, :cond_1

    move v5, v6

    :goto_0
    invoke-static {v8, v9, v5}, Landroid/provider/Settings$Global;->putInt(Landroid/content/ContentResolver;Ljava/lang/String;I)Z

    const/4 v5, 0x2

    if-ne v2, v5, :cond_2

    move v3, v6

    :goto_1
    const-string v5, "[SystemSetting trace auto time]"

    new-instance v6, Ljava/lang/StringBuilder;

    const-string v7, " setAutoTimeMode "

    invoke-direct {v6, v7}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v6, v3}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v6}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v6

    invoke-static {v5, v6}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    invoke-virtual {p0}, Lcom/konka/systemsetting/SysRootApp;->getApplicationContext()Landroid/content/Context;

    move-result-object v5

    invoke-static {v5}, Lcom/konka/kkimplements/tv/mstar/DataBaseDeskImpl;->getDataBaseMgrInstance(Landroid/content/Context;)Lcom/konka/kkimplements/tv/mstar/DataBaseDeskImpl;

    move-result-object v5

    invoke-virtual {v5, v3}, Lcom/konka/kkimplements/tv/mstar/DataBaseDeskImpl;->setAutoTimeMode(I)V

    new-instance v0, Landroid/text/format/Time;

    invoke-direct {v0}, Landroid/text/format/Time;-><init>()V

    invoke-virtual {v0}, Landroid/text/format/Time;->setToNow()V

    const-string v5, "[Systemsetting]"

    new-instance v6, Ljava/lang/StringBuilder;

    const-string v7, "Time changed,set time to supernova!"

    invoke-direct {v6, v7}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v0}, Landroid/text/format/Time;->toString()Ljava/lang/String;

    move-result-object v7

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v6}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v6

    invoke-static {v5, v6}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    iget v5, v0, Landroid/text/format/Time;->month:I

    add-int/lit8 v5, v5, 0x1

    iput v5, v0, Landroid/text/format/Time;->month:I

    :try_start_0
    invoke-static {}, Lcom/mstar/android/tvapi/common/TvManager;->getInstance()Lcom/mstar/android/tvapi/common/TvManager;

    move-result-object v5

    if-eqz v5, :cond_0

    invoke-static {}, Lcom/mstar/android/tvapi/common/TvManager;->getInstance()Lcom/mstar/android/tvapi/common/TvManager;

    move-result-object v5

    invoke-virtual {v5}, Lcom/mstar/android/tvapi/common/TvManager;->getTimerManager()Lcom/mstar/android/tvapi/common/TimerManager;

    move-result-object v5

    const/4 v6, 0x1

    invoke-virtual {v5, v0, v6}, Lcom/mstar/android/tvapi/common/TimerManager;->setClkTime(Landroid/text/format/Time;Z)V
    :try_end_0
    .catch Lcom/mstar/android/tvapi/common/exception/TvCommonException; {:try_start_0 .. :try_end_0} :catch_0

    :cond_0
    :goto_2
    return-void

    :cond_1
    move v5, v7

    goto :goto_0

    :cond_2
    move v3, v7

    goto :goto_1

    :catch_0
    move-exception v1

    invoke-virtual {v1}, Lcom/mstar/android/tvapi/common/exception/TvCommonException;->printStackTrace()V

    goto :goto_2
.end method

.method private initTvDeskProvider()V
    .locals 1

    invoke-virtual {p0}, Lcom/konka/systemsetting/SysRootApp;->getApplicationContext()Landroid/content/Context;

    move-result-object v0

    invoke-static {v0}, Lcom/konka/kkimplements/tv/TvDeskProviderImpl;->getInstance(Landroid/content/Context;)Lcom/konka/kkinterface/tv/TvDeskProvider;

    move-result-object v0

    sput-object v0, Lcom/konka/systemsetting/SysRootApp;->tvDeskProvider:Lcom/konka/kkinterface/tv/TvDeskProvider;

    return-void
.end method

.method public static setSysHandler(Lcom/konka/systemsetting/SysHandler;)V
    .locals 0
    .param p0    # Lcom/konka/systemsetting/SysHandler;

    sput-object p0, Lcom/konka/systemsetting/SysRootApp;->mSysHandler:Lcom/konka/systemsetting/SysHandler;

    return-void
.end method


# virtual methods
.method public onCreate()V
    .locals 4

    const/4 v2, 0x0

    const/4 v1, 0x1

    invoke-direct {p0}, Lcom/konka/systemsetting/SysRootApp;->initTvDeskProvider()V

    invoke-static {}, Lcom/mstar/android/tv/TvTimerManager;->getInstance()Lcom/mstar/android/tv/TvTimerManager;

    move-result-object v0

    sput-object v0, Lcom/konka/systemsetting/SysRootApp;->timerSkin:Lcom/mstar/android/tv/TvTimerManager;

    invoke-static {}, Lcom/mstar/android/tv/TvPictureManager;->getInstance()Lcom/mstar/android/tv/TvPictureManager;

    move-result-object v0

    sput-object v0, Lcom/konka/systemsetting/SysRootApp;->pictureSkin:Lcom/mstar/android/tv/TvPictureManager;

    invoke-static {}, Lcom/mstar/android/tv/TvCommonManager;->getInstance()Lcom/mstar/android/tv/TvCommonManager;

    move-result-object v0

    sput-object v0, Lcom/konka/systemsetting/SysRootApp;->commonSkin:Lcom/mstar/android/tv/TvCommonManager;

    invoke-static {}, Lcom/mstar/android/tv/TvAudioManager;->getInstance()Lcom/mstar/android/tv/TvAudioManager;

    move-result-object v0

    sput-object v0, Lcom/konka/systemsetting/SysRootApp;->audioSkin:Lcom/mstar/android/tv/TvAudioManager;

    sget-object v0, Lcom/konka/systemsetting/SysRootApp;->tvDeskProvider:Lcom/konka/kkinterface/tv/TvDeskProvider;

    invoke-interface {v0}, Lcom/konka/kkinterface/tv/TvDeskProvider;->getCommonManagerInstance()Lcom/konka/kkinterface/tv/CommonDesk;

    move-result-object v0

    invoke-interface {v0}, Lcom/konka/kkinterface/tv/CommonDesk;->getSystemBoardInfo()Lcom/konka/kkinterface/tv/CommonDesk$SystemBoardInfo;

    move-result-object v0

    iget-object v0, v0, Lcom/konka/kkinterface/tv/CommonDesk$SystemBoardInfo;->strPlatform:Ljava/lang/String;

    sput-object v0, Lcom/konka/systemsetting/SysRootApp;->mPlatform:Ljava/lang/String;

    new-instance v0, Ljava/lang/StringBuilder;

    const-string v3, "onCreate,the platform ==="

    invoke-direct {v0, v3}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    sget-object v3, Lcom/konka/systemsetting/SysRootApp;->mPlatform:Ljava/lang/String;

    invoke-virtual {v0, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Lcom/konka/debuginfo/logPrint;->Debug(Ljava/lang/String;)V

    sget-object v0, Lcom/konka/systemsetting/SysRootApp;->tvDeskProvider:Lcom/konka/kkinterface/tv/TvDeskProvider;

    invoke-interface {v0}, Lcom/konka/kkinterface/tv/TvDeskProvider;->getCommonManagerInstance()Lcom/konka/kkinterface/tv/CommonDesk;

    move-result-object v0

    invoke-interface {v0}, Lcom/konka/kkinterface/tv/CommonDesk;->getExternalStorageInfo()Lcom/konka/kkinterface/tv/CommonDesk$ExternalStorageInfo;

    move-result-object v0

    iget v0, v0, Lcom/konka/kkinterface/tv/CommonDesk$ExternalStorageInfo;->hasSDCardSlot:I

    if-ne v0, v1, :cond_0

    move v0, v1

    :goto_0
    sput-boolean v0, Lcom/konka/systemsetting/SysRootApp;->isSDCardSupportable:Z

    sget-object v0, Lcom/konka/systemsetting/SysRootApp;->tvDeskProvider:Lcom/konka/kkinterface/tv/TvDeskProvider;

    invoke-interface {v0}, Lcom/konka/kkinterface/tv/TvDeskProvider;->getCommonManagerInstance()Lcom/konka/kkinterface/tv/CommonDesk;

    move-result-object v0

    invoke-interface {v0}, Lcom/konka/kkinterface/tv/CommonDesk;->getCiCardInfo()Lcom/konka/kkinterface/tv/CommonDesk$CiCardInfo;

    move-result-object v0

    iget v0, v0, Lcom/konka/kkinterface/tv/CommonDesk$CiCardInfo;->enableCiCard:I

    if-ne v0, v1, :cond_1

    :goto_1
    sput-boolean v1, Lcom/konka/systemsetting/SysRootApp;->isCiCardSupportable:Z

    invoke-direct {p0}, Lcom/konka/systemsetting/SysRootApp;->initAutoTimeSettings()V

    invoke-super {p0}, Landroid/app/Application;->onCreate()V

    return-void

    :cond_0
    move v0, v2

    goto :goto_0

    :cond_1
    move v1, v2

    goto :goto_1
.end method

.method public onTerminate()V
    .locals 0

    invoke-super {p0}, Landroid/app/Application;->onTerminate()V

    return-void
.end method
