.class public final Lcom/konka/systemsetting/R$id;
.super Ljava/lang/Object;
.source "R.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/konka/systemsetting/R;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x19
    name = "id"
.end annotation


# static fields
.field public static final aboutItem:I = 0x7f09008c

.field public static final action_button:I = 0x7f090010

.field public static final active_layout:I = 0x7f090005

.field public static final add_msg:I = 0x7f09000a

.field public static final add_msg_expander:I = 0x7f090009

.field public static final admin_description:I = 0x7f090008

.field public static final admin_icon:I = 0x7f090006

.field public static final admin_name:I = 0x7f090007

.field public static final admin_policies:I = 0x7f09000c

.field public static final admin_warning:I = 0x7f09000b

.field public static final btn_open_source_licenses:I = 0x7f0900ad

.field public static final btn_upgrade_byusb:I = 0x7f0900a3

.field public static final btn_upgrade_online:I = 0x7f0900a4

.field public static final buttonPanel:I = 0x7f09000d

.field public static final button_system_factoryreset:I = 0x7f090090

.field public static final cancel_button:I = 0x7f09000f

.field public static final content:I = 0x7f09008d

.field public static final deviceDetails:I = 0x7f0900b9

.field public static final divider:I = 0x7f0900b8

.field public static final et_softap_password:I = 0x7f09004b

.field public static final et_softap_ssid:I = 0x7f090048

.field public static final finish_button:I = 0x7f090003

.field public static final finish_button_area:I = 0x7f090002

.field public static final imgview_systeminfo_disclaimer:I = 0x7f09008e

.field public static final item_resolution:I = 0x7f0900a9

.field public static final iv:I = 0x7f0900c1

.field public static final leftSpacer:I = 0x7f09000e

.field public static final level:I = 0x7f0900c0

.field public static final linearlayout_individ_tab_energysaving:I = 0x7f090021

.field public static final linearlayout_individ_tab_timesetting:I = 0x7f090022

.field public static final linearlayout_individpage_energysaving:I = 0x7f090024

.field public static final linearlayout_individpage_timeselection:I = 0x7f090025

.field public static final linearlayout_network_tab_pppoe:I = 0x7f09003e

.field public static final linearlayout_network_tab_wired:I = 0x7f09003c

.field public static final linearlayout_network_tab_wireless:I = 0x7f09003d

.field public static final linearlayout_networkpage_ppoe:I = 0x7f090044

.field public static final linearlayout_networkpage_wired:I = 0x7f090042

.field public static final linearlayout_networkpage_wireless:I = 0x7f090043

.field public static final linearlayout_storage_page_app_install_position:I = 0x7f09008b

.field public static final linearlayout_storage_page_flash:I = 0x7f090089

.field public static final linearlayout_storage_page_sdcard:I = 0x7f09008a

.field public static final linearlayout_storage_tab_app_install_position:I = 0x7f090087

.field public static final linearlayout_storage_tab_flash:I = 0x7f090085

.field public static final linearlayout_storage_tab_sdcard:I = 0x7f090086

.field public static final linearlayout_system_inputsettings:I = 0x7f09009e

.field public static final linearlayout_system_languageandmusic:I = 0x7f09009d

.field public static final linearlayout_system_tab_factoryreset:I = 0x7f09009b

.field public static final linearlayout_system_tab_inputsettings:I = 0x7f090098

.field public static final linearlayout_system_tab_languageandmusic:I = 0x7f090097

.field public static final linearlayout_system_tab_menu:I = 0x7f090096

.field public static final linearlayout_system_tab_safetyandsoftupgrade:I = 0x7f09009a

.field public static final linearlayout_system_tab_systeminfo:I = 0x7f090099

.field public static final linearlayout_systempage_factoryreset:I = 0x7f0900a1

.field public static final linearlayout_systempage_softupgrade:I = 0x7f0900a0

.field public static final linearlayout_systempage_systeminfo:I = 0x7f09009f

.field public static final ll_no_sd_card:I = 0x7f09007e

.field public static final ll_sd_info:I = 0x7f09007f

.field public static final name:I = 0x7f0900b7

.field public static final page_softap:I = 0x7f090045

.field public static final provider_icon:I = 0x7f0900b4

.field public static final provider_id:I = 0x7f0900b6

.field public static final rightSpacer:I = 0x7f090011

.field public static final scanning_progress:I = 0x7f090013

.field public static final scanning_text:I = 0x7f090014

.field public static final softap_detect:I = 0x7f090050

.field public static final softap_dnssvr:I = 0x7f09004e

.field public static final softap_ipaddr:I = 0x7f09004d

.field public static final softap_password:I = 0x7f09004a

.field public static final softap_security:I = 0x7f090049

.field public static final softap_set:I = 0x7f09004f

.field public static final softap_showpwd:I = 0x7f09004c

.field public static final softap_ssid:I = 0x7f090047

.field public static final softap_switch:I = 0x7f090046

.field public static final ssid:I = 0x7f0900bf

.field public static final syncStatusIcon:I = 0x7f090000

.field public static final sync_active:I = 0x7f090016

.field public static final sync_failed:I = 0x7f090015

.field public static final sync_settings:I = 0x7f090004

.field public static final sync_settings_error_info:I = 0x7f090001

.field public static final sys_energy_menu:I = 0x7f090020

.field public static final sys_individ_energysv_item_backlight:I = 0x7f09001a

.field public static final sys_individ_energysv_item_backlight_seekbar:I = 0x7f09001c

.field public static final sys_individ_energysv_item_backlight_valuemax:I = 0x7f09001d

.field public static final sys_individ_energysv_item_backlight_valuemin:I = 0x7f09001b

.field public static final sys_individ_energysv_item_energyconservation:I = 0x7f090018

.field public static final sys_individ_energysv_item_noactionstandby:I = 0x7f09001f

.field public static final sys_individ_energysv_item_nosignalstandby:I = 0x7f09001e

.field public static final sys_individ_energysv_item_screensaver:I = 0x7f090019

.field public static final sys_individ_item_appsetting:I = 0x7f090017

.field public static final sys_individ_page:I = 0x7f0900b1

.field public static final sys_individ_system_safety:I = 0x7f0900a2

.field public static final sys_individ_timing_item_autoadjust:I = 0x7f090026

.field public static final sys_individ_timing_item_bootchannel:I = 0x7f09002e

.field public static final sys_individ_timing_item_bootswitch:I = 0x7f09002c

.field public static final sys_individ_timing_item_boottime:I = 0x7f09002d

.field public static final sys_individ_timing_item_datesetting:I = 0x7f090027

.field public static final sys_individ_timing_item_haltswitch:I = 0x7f09002a

.field public static final sys_individ_timing_item_halttime:I = 0x7f09002b

.field public static final sys_individ_timing_item_timesetting:I = 0x7f090028

.field public static final sys_individ_timing_item_timezonesetting:I = 0x7f090029

.field public static final sys_individ_view_flipper:I = 0x7f090023

.field public static final sys_input_item_current:I = 0x7f090091

.field public static final sys_input_item_settings:I = 0x7f090012

.field public static final sys_main_menu_item_individ:I = 0x7f090032

.field public static final sys_main_menu_item_network:I = 0x7f090030

.field public static final sys_main_menu_item_storage:I = 0x7f090033

.field public static final sys_main_menu_item_system:I = 0x7f090031

.field public static final sys_main_menu_view_flipper:I = 0x7f0900ae

.field public static final sys_network_menu:I = 0x7f09003b

.field public static final sys_network_page:I = 0x7f0900af

.field public static final sys_network_pppoe_autodial:I = 0x7f090038

.field public static final sys_network_pppoe_edittext_password:I = 0x7f090037

.field public static final sys_network_pppoe_edittext_username:I = 0x7f090035

.field public static final sys_network_pppoe_password:I = 0x7f090036

.field public static final sys_network_pppoe_set:I = 0x7f09003a

.field public static final sys_network_pppoe_showpwd:I = 0x7f090039

.field public static final sys_network_pppoe_username:I = 0x7f090034

.field public static final sys_network_view_flipper:I = 0x7f090041

.field public static final sys_network_wired_edittext_defaultgateway:I = 0x7f090069

.field public static final sys_network_wired_edittext_dnsserver:I = 0x7f09006b

.field public static final sys_network_wired_edittext_ipaddr:I = 0x7f090065

.field public static final sys_network_wired_edittext_subnetmask:I = 0x7f090067

.field public static final sys_network_wired_item_defaultgateway:I = 0x7f090068

.field public static final sys_network_wired_item_detect:I = 0x7f09006d

.field public static final sys_network_wired_item_dnsserver:I = 0x7f09006a

.field public static final sys_network_wired_item_ipaddr:I = 0x7f090064

.field public static final sys_network_wired_item_ipmode:I = 0x7f090063

.field public static final sys_network_wired_item_set:I = 0x7f09006c

.field public static final sys_network_wired_item_subnetmask:I = 0x7f090066

.field public static final sys_network_wireless_item_detect:I = 0x7f090079

.field public static final sys_network_wireless_item_link1:I = 0x7f090073

.field public static final sys_network_wireless_item_link2:I = 0x7f090074

.field public static final sys_network_wireless_item_link3:I = 0x7f090075

.field public static final sys_network_wireless_item_link4:I = 0x7f090076

.field public static final sys_network_wireless_item_link5:I = 0x7f090077

.field public static final sys_network_wireless_item_scan:I = 0x7f090078

.field public static final sys_network_wireless_item_wifiswitch:I = 0x7f09006e

.field public static final sys_network_wireless_item_wps:I = 0x7f09007a

.field public static final sys_network_wireless_tip:I = 0x7f090071

.field public static final sys_network_wireless_tip_wifi:I = 0x7f09006f

.field public static final sys_network_wireless_wifiswitch:I = 0x7f090070

.field public static final sys_setting_first_menu:I = 0x7f09002f

.field public static final sys_storage_app_install_position:I = 0x7f09007b

.field public static final sys_storage_flash_freespace:I = 0x7f09007c

.field public static final sys_storage_flash_totalspace:I = 0x7f09007d

.field public static final sys_storage_menu:I = 0x7f090084

.field public static final sys_storage_page:I = 0x7f0900b2

.field public static final sys_storage_sdcard_format:I = 0x7f090081

.field public static final sys_storage_sdcard_freespace:I = 0x7f090082

.field public static final sys_storage_sdcard_totalspace:I = 0x7f090083

.field public static final sys_storage_sdcard_unmount:I = 0x7f090080

.field public static final sys_storage_view_flipper:I = 0x7f090088

.field public static final sys_system_item_language:I = 0x7f090092

.field public static final sys_system_item_mic:I = 0x7f090093

.field public static final sys_system_item_music:I = 0x7f090095

.field public static final sys_system_micswitch:I = 0x7f090094

.field public static final sys_system_page:I = 0x7f0900b0

.field public static final tab_softap:I = 0x7f09003f

.field public static final tab_wireless_display:I = 0x7f090040

.field public static final textview_systeminfo_test:I = 0x7f09008f

.field public static final title_area:I = 0x7f0900b3

.field public static final tv_android_version:I = 0x7f0900a7

.field public static final tv_emmc_size:I = 0x7f0900a6

.field public static final tv_macaddr:I = 0x7f0900ac

.field public static final tv_ram_size:I = 0x7f0900a5

.field public static final tv_resolution:I = 0x7f0900aa

.field public static final tv_serialnum:I = 0x7f0900ab

.field public static final tv_tv_version:I = 0x7f0900a8

.field public static final user_id:I = 0x7f0900b5

.field public static final viewflipper_systempage:I = 0x7f09009c

.field public static final wifi_ap1:I = 0x7f0900ba

.field public static final wifi_ap2:I = 0x7f0900bb

.field public static final wifi_ap3:I = 0x7f0900bc

.field public static final wifi_ap4:I = 0x7f0900bd

.field public static final wifi_ap5:I = 0x7f0900be

.field public static final wifi_box:I = 0x7f090051

.field public static final wifi_box1:I = 0x7f090053

.field public static final wifi_box2:I = 0x7f09005a

.field public static final wifi_box_btn_cancel:I = 0x7f09005f

.field public static final wifi_box_btn_connect:I = 0x7f090062

.field public static final wifi_box_btn_forget:I = 0x7f090061

.field public static final wifi_box_btn_save:I = 0x7f090060

.field public static final wifi_box_cb_setip:I = 0x7f090059

.field public static final wifi_box_cb_showpwd:I = 0x7f090058

.field public static final wifi_box_et_pwd:I = 0x7f090057

.field public static final wifi_box_input:I = 0x7f090056

.field public static final wifi_box_security:I = 0x7f090054

.field public static final wifi_box_signalstrength:I = 0x7f090055

.field public static final wifi_box_title:I = 0x7f090052

.field public static final wifi_container:I = 0x7f090072

.field public static final wifi_ipmode:I = 0x7f09005b

.field public static final wifiipet_dns1:I = 0x7f09005e

.field public static final wifiipet_gatway:I = 0x7f09005d

.field public static final wifiipet_ipaddr:I = 0x7f09005c

.field public static final wps_dialog_btn:I = 0x7f0900c5

.field public static final wps_dialog_txt:I = 0x7f0900c2

.field public static final wps_progress_bar:I = 0x7f0900c4

.field public static final wps_timeout_bar:I = 0x7f0900c3

.field public static final wps_txt:I = 0x7f0900c6


# direct methods
.method public constructor <init>()V
    .locals 0

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method
