.class public Lcom/konka/systemsetting/ViewHolder;
.super Ljava/lang/Object;
.source "ViewHolder.java"


# instance fields
.field public btnDetectWifi:Landroid/widget/Button;

.field public btnSetWifi:Landroid/widget/Button;

.field public linearLayout_tabSoftApSettings:Landroid/widget/LinearLayout;

.field public linearLayout_tabWirelessDisplay:Landroid/widget/LinearLayout;

.field public linearlayout_itemWifiHPs:[Landroid/widget/LinearLayout;

.field public linearlayout_itemWifiSwitch:Landroid/widget/LinearLayout;

.field public linearlayout_tabAppInstallPosition:Landroid/widget/LinearLayout;

.field public linearlayout_tabEnergySaving:Landroid/widget/LinearLayout;

.field public linearlayout_tabFactoryReset:Landroid/widget/LinearLayout;

.field public linearlayout_tabFlash:Landroid/widget/LinearLayout;

.field public linearlayout_tabInputSettings:Landroid/widget/LinearLayout;

.field public linearlayout_tabLanguageAndMusic:Landroid/widget/LinearLayout;

.field public linearlayout_tabPPoeSettings:Landroid/widget/LinearLayout;

.field public linearlayout_tabSdCard:Landroid/widget/LinearLayout;

.field public linearlayout_tabSoftUpgrade:Landroid/widget/LinearLayout;

.field public linearlayout_tabSystemInfo:Landroid/widget/LinearLayout;

.field public linearlayout_tabTimeSetting:Landroid/widget/LinearLayout;

.field public linearlayout_tabWiredSettings:Landroid/widget/LinearLayout;

.field public linearlayout_tabWirelessSettings:Landroid/widget/LinearLayout;

.field private mContext:Lcom/konka/systemsetting/MainActivity;

.field public mCurrItemId:Ljava/lang/Integer;

.field public textView_ComTip:Landroid/widget/TextView;

.field public textView_WifiSwitch:Landroid/widget/TextView;

.field public textView_WifiTip:Landroid/widget/TextView;

.field public viewflipper_individ:Landroid/widget/ViewFlipper;

.field public viewflipper_network:Landroid/widget/ViewFlipper;

.field public viewflipper_storage:Landroid/widget/ViewFlipper;

.field public viewflipper_systempage:Landroid/widget/ViewFlipper;


# direct methods
.method public constructor <init>(Lcom/konka/systemsetting/MainActivity;)V
    .locals 2
    .param p1    # Lcom/konka/systemsetting/MainActivity;

    const/4 v1, 0x0

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    const v0, 0x7f090063    # com.konka.systemsetting.R.id.sys_network_wired_item_ipmode

    invoke-static {v0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v0

    iput-object v0, p0, Lcom/konka/systemsetting/ViewHolder;->mCurrItemId:Ljava/lang/Integer;

    iput-object v1, p0, Lcom/konka/systemsetting/ViewHolder;->linearlayout_itemWifiSwitch:Landroid/widget/LinearLayout;

    iput-object v1, p0, Lcom/konka/systemsetting/ViewHolder;->linearlayout_itemWifiHPs:[Landroid/widget/LinearLayout;

    iput-object v1, p0, Lcom/konka/systemsetting/ViewHolder;->btnSetWifi:Landroid/widget/Button;

    iput-object v1, p0, Lcom/konka/systemsetting/ViewHolder;->btnDetectWifi:Landroid/widget/Button;

    iput-object v1, p0, Lcom/konka/systemsetting/ViewHolder;->textView_ComTip:Landroid/widget/TextView;

    iput-object v1, p0, Lcom/konka/systemsetting/ViewHolder;->textView_WifiTip:Landroid/widget/TextView;

    iput-object v1, p0, Lcom/konka/systemsetting/ViewHolder;->textView_WifiSwitch:Landroid/widget/TextView;

    iput-object p1, p0, Lcom/konka/systemsetting/ViewHolder;->mContext:Lcom/konka/systemsetting/MainActivity;

    return-void
.end method


# virtual methods
.method public findViewForPages()V
    .locals 7

    const v6, 0x7f090021    # com.konka.systemsetting.R.id.linearlayout_individ_tab_energysaving

    const v5, 0x7f090085    # com.konka.systemsetting.R.id.linearlayout_storage_tab_flash

    const v4, 0x7f090033    # com.konka.systemsetting.R.id.sys_main_menu_item_storage

    const v3, 0x7f090031    # com.konka.systemsetting.R.id.sys_main_menu_item_system

    const v2, 0x7f090030    # com.konka.systemsetting.R.id.sys_main_menu_item_network

    iget-object v0, p0, Lcom/konka/systemsetting/ViewHolder;->mContext:Lcom/konka/systemsetting/MainActivity;

    const v1, 0x7f09009c    # com.konka.systemsetting.R.id.viewflipper_systempage

    invoke-virtual {v0, v1}, Lcom/konka/systemsetting/MainActivity;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/ViewFlipper;

    iput-object v0, p0, Lcom/konka/systemsetting/ViewHolder;->viewflipper_systempage:Landroid/widget/ViewFlipper;

    iget-object v0, p0, Lcom/konka/systemsetting/ViewHolder;->mContext:Lcom/konka/systemsetting/MainActivity;

    const v1, 0x7f090097    # com.konka.systemsetting.R.id.linearlayout_system_tab_languageandmusic

    invoke-virtual {v0, v1}, Lcom/konka/systemsetting/MainActivity;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/LinearLayout;

    iput-object v0, p0, Lcom/konka/systemsetting/ViewHolder;->linearlayout_tabLanguageAndMusic:Landroid/widget/LinearLayout;

    iget-object v0, p0, Lcom/konka/systemsetting/ViewHolder;->mContext:Lcom/konka/systemsetting/MainActivity;

    const v1, 0x7f090098    # com.konka.systemsetting.R.id.linearlayout_system_tab_inputsettings

    invoke-virtual {v0, v1}, Lcom/konka/systemsetting/MainActivity;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/LinearLayout;

    iput-object v0, p0, Lcom/konka/systemsetting/ViewHolder;->linearlayout_tabInputSettings:Landroid/widget/LinearLayout;

    iget-object v0, p0, Lcom/konka/systemsetting/ViewHolder;->mContext:Lcom/konka/systemsetting/MainActivity;

    const v1, 0x7f090099    # com.konka.systemsetting.R.id.linearlayout_system_tab_systeminfo

    invoke-virtual {v0, v1}, Lcom/konka/systemsetting/MainActivity;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/LinearLayout;

    iput-object v0, p0, Lcom/konka/systemsetting/ViewHolder;->linearlayout_tabSystemInfo:Landroid/widget/LinearLayout;

    iget-object v0, p0, Lcom/konka/systemsetting/ViewHolder;->mContext:Lcom/konka/systemsetting/MainActivity;

    const v1, 0x7f09009a    # com.konka.systemsetting.R.id.linearlayout_system_tab_safetyandsoftupgrade

    invoke-virtual {v0, v1}, Lcom/konka/systemsetting/MainActivity;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/LinearLayout;

    iput-object v0, p0, Lcom/konka/systemsetting/ViewHolder;->linearlayout_tabSoftUpgrade:Landroid/widget/LinearLayout;

    iget-object v0, p0, Lcom/konka/systemsetting/ViewHolder;->mContext:Lcom/konka/systemsetting/MainActivity;

    const v1, 0x7f09009b    # com.konka.systemsetting.R.id.linearlayout_system_tab_factoryreset

    invoke-virtual {v0, v1}, Lcom/konka/systemsetting/MainActivity;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/LinearLayout;

    iput-object v0, p0, Lcom/konka/systemsetting/ViewHolder;->linearlayout_tabFactoryReset:Landroid/widget/LinearLayout;

    iget-object v0, p0, Lcom/konka/systemsetting/ViewHolder;->linearlayout_tabLanguageAndMusic:Landroid/widget/LinearLayout;

    const v1, 0x7f090092    # com.konka.systemsetting.R.id.sys_system_item_language

    invoke-virtual {v0, v1}, Landroid/widget/LinearLayout;->setNextFocusRightId(I)V

    iget-object v0, p0, Lcom/konka/systemsetting/ViewHolder;->linearlayout_tabInputSettings:Landroid/widget/LinearLayout;

    const v1, 0x7f090091    # com.konka.systemsetting.R.id.sys_input_item_current

    invoke-virtual {v0, v1}, Landroid/widget/LinearLayout;->setNextFocusRightId(I)V

    iget-object v0, p0, Lcom/konka/systemsetting/ViewHolder;->linearlayout_tabSoftUpgrade:Landroid/widget/LinearLayout;

    const v1, 0x7f0900a3    # com.konka.systemsetting.R.id.btn_upgrade_byusb

    invoke-virtual {v0, v1}, Landroid/widget/LinearLayout;->setNextFocusRightId(I)V

    iget-object v0, p0, Lcom/konka/systemsetting/ViewHolder;->linearlayout_tabLanguageAndMusic:Landroid/widget/LinearLayout;

    invoke-virtual {v0, v3}, Landroid/widget/LinearLayout;->setNextFocusLeftId(I)V

    iget-object v0, p0, Lcom/konka/systemsetting/ViewHolder;->linearlayout_tabInputSettings:Landroid/widget/LinearLayout;

    invoke-virtual {v0, v3}, Landroid/widget/LinearLayout;->setNextFocusLeftId(I)V

    iget-object v0, p0, Lcom/konka/systemsetting/ViewHolder;->linearlayout_tabSystemInfo:Landroid/widget/LinearLayout;

    invoke-virtual {v0, v3}, Landroid/widget/LinearLayout;->setNextFocusLeftId(I)V

    iget-object v0, p0, Lcom/konka/systemsetting/ViewHolder;->linearlayout_tabSoftUpgrade:Landroid/widget/LinearLayout;

    invoke-virtual {v0, v3}, Landroid/widget/LinearLayout;->setNextFocusLeftId(I)V

    iget-object v0, p0, Lcom/konka/systemsetting/ViewHolder;->linearlayout_tabFactoryReset:Landroid/widget/LinearLayout;

    invoke-virtual {v0, v3}, Landroid/widget/LinearLayout;->setNextFocusLeftId(I)V

    iget-object v0, p0, Lcom/konka/systemsetting/ViewHolder;->mContext:Lcom/konka/systemsetting/MainActivity;

    const v1, 0x7f090023    # com.konka.systemsetting.R.id.sys_individ_view_flipper

    invoke-virtual {v0, v1}, Lcom/konka/systemsetting/MainActivity;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/ViewFlipper;

    iput-object v0, p0, Lcom/konka/systemsetting/ViewHolder;->viewflipper_individ:Landroid/widget/ViewFlipper;

    iget-object v0, p0, Lcom/konka/systemsetting/ViewHolder;->mContext:Lcom/konka/systemsetting/MainActivity;

    const v1, 0x7f090022    # com.konka.systemsetting.R.id.linearlayout_individ_tab_timesetting

    invoke-virtual {v0, v1}, Lcom/konka/systemsetting/MainActivity;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/LinearLayout;

    iput-object v0, p0, Lcom/konka/systemsetting/ViewHolder;->linearlayout_tabTimeSetting:Landroid/widget/LinearLayout;

    iget-object v0, p0, Lcom/konka/systemsetting/ViewHolder;->mContext:Lcom/konka/systemsetting/MainActivity;

    invoke-virtual {v0, v6}, Lcom/konka/systemsetting/MainActivity;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/LinearLayout;

    iput-object v0, p0, Lcom/konka/systemsetting/ViewHolder;->linearlayout_tabEnergySaving:Landroid/widget/LinearLayout;

    iget-object v0, p0, Lcom/konka/systemsetting/ViewHolder;->linearlayout_tabTimeSetting:Landroid/widget/LinearLayout;

    const v1, 0x7f090032    # com.konka.systemsetting.R.id.sys_main_menu_item_individ

    invoke-virtual {v0, v1}, Landroid/widget/LinearLayout;->setNextFocusLeftId(I)V

    iget-object v0, p0, Lcom/konka/systemsetting/ViewHolder;->linearlayout_tabEnergySaving:Landroid/widget/LinearLayout;

    const v1, 0x7f090032    # com.konka.systemsetting.R.id.sys_main_menu_item_individ

    invoke-virtual {v0, v1}, Landroid/widget/LinearLayout;->setNextFocusLeftId(I)V

    iget-object v0, p0, Lcom/konka/systemsetting/ViewHolder;->linearlayout_tabTimeSetting:Landroid/widget/LinearLayout;

    const v1, 0x7f090026    # com.konka.systemsetting.R.id.sys_individ_timing_item_autoadjust

    invoke-virtual {v0, v1}, Landroid/widget/LinearLayout;->setNextFocusRightId(I)V

    iget-object v0, p0, Lcom/konka/systemsetting/ViewHolder;->linearlayout_tabEnergySaving:Landroid/widget/LinearLayout;

    const v1, 0x7f09001e    # com.konka.systemsetting.R.id.sys_individ_energysv_item_nosignalstandby

    invoke-virtual {v0, v1}, Landroid/widget/LinearLayout;->setNextFocusRightId(I)V

    iget-object v0, p0, Lcom/konka/systemsetting/ViewHolder;->linearlayout_tabEnergySaving:Landroid/widget/LinearLayout;

    invoke-virtual {v0, v6}, Landroid/widget/LinearLayout;->setNextFocusUpId(I)V

    iget-object v0, p0, Lcom/konka/systemsetting/ViewHolder;->mContext:Lcom/konka/systemsetting/MainActivity;

    const v1, 0x7f090088    # com.konka.systemsetting.R.id.sys_storage_view_flipper

    invoke-virtual {v0, v1}, Lcom/konka/systemsetting/MainActivity;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/ViewFlipper;

    iput-object v0, p0, Lcom/konka/systemsetting/ViewHolder;->viewflipper_storage:Landroid/widget/ViewFlipper;

    iget-object v0, p0, Lcom/konka/systemsetting/ViewHolder;->mContext:Lcom/konka/systemsetting/MainActivity;

    invoke-virtual {v0, v5}, Lcom/konka/systemsetting/MainActivity;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/LinearLayout;

    iput-object v0, p0, Lcom/konka/systemsetting/ViewHolder;->linearlayout_tabFlash:Landroid/widget/LinearLayout;

    iget-object v0, p0, Lcom/konka/systemsetting/ViewHolder;->mContext:Lcom/konka/systemsetting/MainActivity;

    const v1, 0x7f090086    # com.konka.systemsetting.R.id.linearlayout_storage_tab_sdcard

    invoke-virtual {v0, v1}, Lcom/konka/systemsetting/MainActivity;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/LinearLayout;

    iput-object v0, p0, Lcom/konka/systemsetting/ViewHolder;->linearlayout_tabSdCard:Landroid/widget/LinearLayout;

    iget-object v0, p0, Lcom/konka/systemsetting/ViewHolder;->mContext:Lcom/konka/systemsetting/MainActivity;

    const v1, 0x7f090087    # com.konka.systemsetting.R.id.linearlayout_storage_tab_app_install_position

    invoke-virtual {v0, v1}, Lcom/konka/systemsetting/MainActivity;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/LinearLayout;

    iput-object v0, p0, Lcom/konka/systemsetting/ViewHolder;->linearlayout_tabAppInstallPosition:Landroid/widget/LinearLayout;

    iget-object v0, p0, Lcom/konka/systemsetting/ViewHolder;->linearlayout_tabFlash:Landroid/widget/LinearLayout;

    invoke-virtual {v0, v4}, Landroid/widget/LinearLayout;->setNextFocusLeftId(I)V

    iget-object v0, p0, Lcom/konka/systemsetting/ViewHolder;->linearlayout_tabSdCard:Landroid/widget/LinearLayout;

    invoke-virtual {v0, v4}, Landroid/widget/LinearLayout;->setNextFocusLeftId(I)V

    iget-object v0, p0, Lcom/konka/systemsetting/ViewHolder;->linearlayout_tabAppInstallPosition:Landroid/widget/LinearLayout;

    invoke-virtual {v0, v4}, Landroid/widget/LinearLayout;->setNextFocusLeftId(I)V

    iget-object v0, p0, Lcom/konka/systemsetting/ViewHolder;->linearlayout_tabFlash:Landroid/widget/LinearLayout;

    invoke-virtual {v0, v5}, Landroid/widget/LinearLayout;->setNextFocusRightId(I)V

    iget-object v0, p0, Lcom/konka/systemsetting/ViewHolder;->linearlayout_tabSdCard:Landroid/widget/LinearLayout;

    const v1, 0x7f090080    # com.konka.systemsetting.R.id.sys_storage_sdcard_unmount

    invoke-virtual {v0, v1}, Landroid/widget/LinearLayout;->setNextFocusRightId(I)V

    iget-object v0, p0, Lcom/konka/systemsetting/ViewHolder;->linearlayout_tabAppInstallPosition:Landroid/widget/LinearLayout;

    const v1, 0x7f09007b    # com.konka.systemsetting.R.id.sys_storage_app_install_position

    invoke-virtual {v0, v1}, Landroid/widget/LinearLayout;->setNextFocusRightId(I)V

    iget-object v0, p0, Lcom/konka/systemsetting/ViewHolder;->linearlayout_tabFlash:Landroid/widget/LinearLayout;

    invoke-virtual {v0, v5}, Landroid/widget/LinearLayout;->setNextFocusUpId(I)V

    iget-object v0, p0, Lcom/konka/systemsetting/ViewHolder;->linearlayout_tabAppInstallPosition:Landroid/widget/LinearLayout;

    const v1, 0x7f090087    # com.konka.systemsetting.R.id.linearlayout_storage_tab_app_install_position

    invoke-virtual {v0, v1}, Landroid/widget/LinearLayout;->setNextFocusDownId(I)V

    iget-object v0, p0, Lcom/konka/systemsetting/ViewHolder;->mContext:Lcom/konka/systemsetting/MainActivity;

    const v1, 0x7f090041    # com.konka.systemsetting.R.id.sys_network_view_flipper

    invoke-virtual {v0, v1}, Lcom/konka/systemsetting/MainActivity;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/ViewFlipper;

    iput-object v0, p0, Lcom/konka/systemsetting/ViewHolder;->viewflipper_network:Landroid/widget/ViewFlipper;

    iget-object v0, p0, Lcom/konka/systemsetting/ViewHolder;->mContext:Lcom/konka/systemsetting/MainActivity;

    const v1, 0x7f09003c    # com.konka.systemsetting.R.id.linearlayout_network_tab_wired

    invoke-virtual {v0, v1}, Lcom/konka/systemsetting/MainActivity;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/LinearLayout;

    iput-object v0, p0, Lcom/konka/systemsetting/ViewHolder;->linearlayout_tabWiredSettings:Landroid/widget/LinearLayout;

    iget-object v0, p0, Lcom/konka/systemsetting/ViewHolder;->mContext:Lcom/konka/systemsetting/MainActivity;

    const v1, 0x7f09003d    # com.konka.systemsetting.R.id.linearlayout_network_tab_wireless

    invoke-virtual {v0, v1}, Lcom/konka/systemsetting/MainActivity;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/LinearLayout;

    iput-object v0, p0, Lcom/konka/systemsetting/ViewHolder;->linearlayout_tabWirelessSettings:Landroid/widget/LinearLayout;

    iget-object v0, p0, Lcom/konka/systemsetting/ViewHolder;->mContext:Lcom/konka/systemsetting/MainActivity;

    const v1, 0x7f09003e    # com.konka.systemsetting.R.id.linearlayout_network_tab_pppoe

    invoke-virtual {v0, v1}, Lcom/konka/systemsetting/MainActivity;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/LinearLayout;

    iput-object v0, p0, Lcom/konka/systemsetting/ViewHolder;->linearlayout_tabPPoeSettings:Landroid/widget/LinearLayout;

    iget-object v0, p0, Lcom/konka/systemsetting/ViewHolder;->mContext:Lcom/konka/systemsetting/MainActivity;

    const v1, 0x7f09003f    # com.konka.systemsetting.R.id.tab_softap

    invoke-virtual {v0, v1}, Lcom/konka/systemsetting/MainActivity;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/LinearLayout;

    iput-object v0, p0, Lcom/konka/systemsetting/ViewHolder;->linearLayout_tabSoftApSettings:Landroid/widget/LinearLayout;

    iget-object v0, p0, Lcom/konka/systemsetting/ViewHolder;->mContext:Lcom/konka/systemsetting/MainActivity;

    const v1, 0x7f090040    # com.konka.systemsetting.R.id.tab_wireless_display

    invoke-virtual {v0, v1}, Lcom/konka/systemsetting/MainActivity;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/LinearLayout;

    iput-object v0, p0, Lcom/konka/systemsetting/ViewHolder;->linearLayout_tabWirelessDisplay:Landroid/widget/LinearLayout;

    iget-object v0, p0, Lcom/konka/systemsetting/ViewHolder;->linearlayout_tabWiredSettings:Landroid/widget/LinearLayout;

    invoke-virtual {v0, v2}, Landroid/widget/LinearLayout;->setNextFocusLeftId(I)V

    iget-object v0, p0, Lcom/konka/systemsetting/ViewHolder;->linearlayout_tabWirelessSettings:Landroid/widget/LinearLayout;

    invoke-virtual {v0, v2}, Landroid/widget/LinearLayout;->setNextFocusLeftId(I)V

    iget-object v0, p0, Lcom/konka/systemsetting/ViewHolder;->linearlayout_tabPPoeSettings:Landroid/widget/LinearLayout;

    invoke-virtual {v0, v2}, Landroid/widget/LinearLayout;->setNextFocusLeftId(I)V

    iget-object v0, p0, Lcom/konka/systemsetting/ViewHolder;->linearLayout_tabSoftApSettings:Landroid/widget/LinearLayout;

    invoke-virtual {v0, v2}, Landroid/widget/LinearLayout;->setNextFocusLeftId(I)V

    iget-object v0, p0, Lcom/konka/systemsetting/ViewHolder;->linearLayout_tabWirelessDisplay:Landroid/widget/LinearLayout;

    invoke-virtual {v0, v2}, Landroid/widget/LinearLayout;->setNextFocusLeftId(I)V

    iget-object v0, p0, Lcom/konka/systemsetting/ViewHolder;->linearlayout_tabWiredSettings:Landroid/widget/LinearLayout;

    const v1, 0x7f09003c    # com.konka.systemsetting.R.id.linearlayout_network_tab_wired

    invoke-virtual {v0, v1}, Landroid/widget/LinearLayout;->setNextFocusUpId(I)V

    iget-object v0, p0, Lcom/konka/systemsetting/ViewHolder;->linearLayout_tabSoftApSettings:Landroid/widget/LinearLayout;

    const v1, 0x7f090040    # com.konka.systemsetting.R.id.tab_wireless_display

    invoke-virtual {v0, v1}, Landroid/widget/LinearLayout;->setNextFocusDownId(I)V

    iget-object v0, p0, Lcom/konka/systemsetting/ViewHolder;->linearlayout_tabWiredSettings:Landroid/widget/LinearLayout;

    const v1, 0x7f090063    # com.konka.systemsetting.R.id.sys_network_wired_item_ipmode

    invoke-virtual {v0, v1}, Landroid/widget/LinearLayout;->setNextFocusRightId(I)V

    iget-object v0, p0, Lcom/konka/systemsetting/ViewHolder;->linearlayout_tabWirelessSettings:Landroid/widget/LinearLayout;

    const v1, 0x7f09006e    # com.konka.systemsetting.R.id.sys_network_wireless_item_wifiswitch

    invoke-virtual {v0, v1}, Landroid/widget/LinearLayout;->setNextFocusRightId(I)V

    iget-object v0, p0, Lcom/konka/systemsetting/ViewHolder;->linearLayout_tabSoftApSettings:Landroid/widget/LinearLayout;

    const v1, 0x7f090046    # com.konka.systemsetting.R.id.softap_switch

    invoke-virtual {v0, v1}, Landroid/widget/LinearLayout;->setNextFocusRightId(I)V

    return-void
.end method
