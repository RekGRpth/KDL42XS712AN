.class Lcom/konka/systemsetting/accounts/AccountSyncSettings$1$1;
.super Ljava/lang/Object;
.source "AccountSyncSettings.java"

# interfaces
.implements Landroid/accounts/AccountManagerCallback;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/konka/systemsetting/accounts/AccountSyncSettings$1;->onClick(Landroid/content/DialogInterface;I)V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Object;",
        "Landroid/accounts/AccountManagerCallback",
        "<",
        "Ljava/lang/Boolean;",
        ">;"
    }
.end annotation


# instance fields
.field final synthetic this$1:Lcom/konka/systemsetting/accounts/AccountSyncSettings$1;


# direct methods
.method constructor <init>(Lcom/konka/systemsetting/accounts/AccountSyncSettings$1;)V
    .locals 0

    iput-object p1, p0, Lcom/konka/systemsetting/accounts/AccountSyncSettings$1$1;->this$1:Lcom/konka/systemsetting/accounts/AccountSyncSettings$1;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public run(Landroid/accounts/AccountManagerFuture;)V
    .locals 3
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Landroid/accounts/AccountManagerFuture",
            "<",
            "Ljava/lang/Boolean;",
            ">;)V"
        }
    .end annotation

    const/4 v0, 0x1

    :try_start_0
    invoke-interface {p1}, Landroid/accounts/AccountManagerFuture;->getResult()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Ljava/lang/Boolean;

    invoke-virtual {v1}, Ljava/lang/Boolean;->booleanValue()Z
    :try_end_0
    .catch Landroid/accounts/OperationCanceledException; {:try_start_0 .. :try_end_0} :catch_2
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_1
    .catch Landroid/accounts/AuthenticatorException; {:try_start_0 .. :try_end_0} :catch_0

    move-result v1

    if-eqz v1, :cond_0

    const/4 v0, 0x0

    :cond_0
    :goto_0
    if-eqz v0, :cond_1

    iget-object v1, p0, Lcom/konka/systemsetting/accounts/AccountSyncSettings$1$1;->this$1:Lcom/konka/systemsetting/accounts/AccountSyncSettings$1;

    # getter for: Lcom/konka/systemsetting/accounts/AccountSyncSettings$1;->this$0:Lcom/konka/systemsetting/accounts/AccountSyncSettings;
    invoke-static {v1}, Lcom/konka/systemsetting/accounts/AccountSyncSettings$1;->access$0(Lcom/konka/systemsetting/accounts/AccountSyncSettings$1;)Lcom/konka/systemsetting/accounts/AccountSyncSettings;

    move-result-object v1

    const/16 v2, 0x65

    # invokes: Lcom/konka/systemsetting/accounts/AccountSyncSettings;->showDialog(I)V
    invoke-static {v1, v2}, Lcom/konka/systemsetting/accounts/AccountSyncSettings;->access$2(Lcom/konka/systemsetting/accounts/AccountSyncSettings;I)V

    :goto_1
    return-void

    :cond_1
    iget-object v1, p0, Lcom/konka/systemsetting/accounts/AccountSyncSettings$1$1;->this$1:Lcom/konka/systemsetting/accounts/AccountSyncSettings$1;

    # getter for: Lcom/konka/systemsetting/accounts/AccountSyncSettings$1;->this$0:Lcom/konka/systemsetting/accounts/AccountSyncSettings;
    invoke-static {v1}, Lcom/konka/systemsetting/accounts/AccountSyncSettings$1;->access$0(Lcom/konka/systemsetting/accounts/AccountSyncSettings$1;)Lcom/konka/systemsetting/accounts/AccountSyncSettings;

    move-result-object v1

    invoke-virtual {v1}, Lcom/konka/systemsetting/accounts/AccountSyncSettings;->finish()V

    goto :goto_1

    :catch_0
    move-exception v1

    goto :goto_0

    :catch_1
    move-exception v1

    goto :goto_0

    :catch_2
    move-exception v1

    goto :goto_0
.end method
