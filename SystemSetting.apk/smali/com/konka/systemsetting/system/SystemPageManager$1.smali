.class Lcom/konka/systemsetting/system/SystemPageManager$1;
.super Ljava/lang/Object;
.source "SystemPageManager.java"

# interfaces
.implements Landroid/view/View$OnFocusChangeListener;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/konka/systemsetting/system/SystemPageManager;->SetOnFocusChangeListener()V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/konka/systemsetting/system/SystemPageManager;


# direct methods
.method constructor <init>(Lcom/konka/systemsetting/system/SystemPageManager;)V
    .locals 0

    iput-object p1, p0, Lcom/konka/systemsetting/system/SystemPageManager$1;->this$0:Lcom/konka/systemsetting/system/SystemPageManager;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onFocusChange(Landroid/view/View;Z)V
    .locals 6
    .param p1    # Landroid/view/View;
    .param p2    # Z

    const/4 v5, 0x2

    const/4 v4, 0x1

    const/4 v3, 0x0

    invoke-virtual {p1}, Landroid/view/View;->getId()I

    move-result v0

    if-eqz p2, :cond_1

    packed-switch v0, :pswitch_data_0

    :goto_0
    return-void

    :pswitch_0
    iget-object v1, p0, Lcom/konka/systemsetting/system/SystemPageManager$1;->this$0:Lcom/konka/systemsetting/system/SystemPageManager;

    iget-object v2, p0, Lcom/konka/systemsetting/system/SystemPageManager$1;->this$0:Lcom/konka/systemsetting/system/SystemPageManager;

    # getter for: Lcom/konka/systemsetting/system/SystemPageManager;->m_iCurPage:I
    invoke-static {v2}, Lcom/konka/systemsetting/system/SystemPageManager;->access$0(Lcom/konka/systemsetting/system/SystemPageManager;)I

    move-result v2

    invoke-static {v1, v2}, Lcom/konka/systemsetting/system/SystemPageManager;->access$1(Lcom/konka/systemsetting/system/SystemPageManager;I)V

    iget-object v1, p0, Lcom/konka/systemsetting/system/SystemPageManager$1;->this$0:Lcom/konka/systemsetting/system/SystemPageManager;

    invoke-static {v1, v3}, Lcom/konka/systemsetting/system/SystemPageManager;->access$2(Lcom/konka/systemsetting/system/SystemPageManager;I)V

    :goto_1
    iget-object v1, p0, Lcom/konka/systemsetting/system/SystemPageManager$1;->this$0:Lcom/konka/systemsetting/system/SystemPageManager;

    # getter for: Lcom/konka/systemsetting/system/SystemPageManager;->m_iCurPage:I
    invoke-static {v1}, Lcom/konka/systemsetting/system/SystemPageManager;->access$0(Lcom/konka/systemsetting/system/SystemPageManager;)I

    move-result v1

    iget-object v2, p0, Lcom/konka/systemsetting/system/SystemPageManager$1;->this$0:Lcom/konka/systemsetting/system/SystemPageManager;

    # getter for: Lcom/konka/systemsetting/system/SystemPageManager;->m_iLastFocusPage:I
    invoke-static {v2}, Lcom/konka/systemsetting/system/SystemPageManager;->access$3(Lcom/konka/systemsetting/system/SystemPageManager;)I

    move-result v2

    if-eq v1, v2, :cond_0

    iget-object v1, p0, Lcom/konka/systemsetting/system/SystemPageManager$1;->this$0:Lcom/konka/systemsetting/system/SystemPageManager;

    iget-object v2, p0, Lcom/konka/systemsetting/system/SystemPageManager$1;->this$0:Lcom/konka/systemsetting/system/SystemPageManager;

    # getter for: Lcom/konka/systemsetting/system/SystemPageManager;->m_iLastFocusPage:I
    invoke-static {v2}, Lcom/konka/systemsetting/system/SystemPageManager;->access$3(Lcom/konka/systemsetting/system/SystemPageManager;)I

    move-result v2

    # invokes: Lcom/konka/systemsetting/system/SystemPageManager;->changeItemStatus(II)V
    invoke-static {v1, v2, v5}, Lcom/konka/systemsetting/system/SystemPageManager;->access$4(Lcom/konka/systemsetting/system/SystemPageManager;II)V

    :cond_0
    iget-object v1, p0, Lcom/konka/systemsetting/system/SystemPageManager$1;->this$0:Lcom/konka/systemsetting/system/SystemPageManager;

    iget-object v2, p0, Lcom/konka/systemsetting/system/SystemPageManager$1;->this$0:Lcom/konka/systemsetting/system/SystemPageManager;

    # getter for: Lcom/konka/systemsetting/system/SystemPageManager;->m_iCurPage:I
    invoke-static {v2}, Lcom/konka/systemsetting/system/SystemPageManager;->access$0(Lcom/konka/systemsetting/system/SystemPageManager;)I

    move-result v2

    # invokes: Lcom/konka/systemsetting/system/SystemPageManager;->changeItemStatus(II)V
    invoke-static {v1, v2, v3}, Lcom/konka/systemsetting/system/SystemPageManager;->access$4(Lcom/konka/systemsetting/system/SystemPageManager;II)V

    iget-object v1, p0, Lcom/konka/systemsetting/system/SystemPageManager$1;->this$0:Lcom/konka/systemsetting/system/SystemPageManager;

    iget-object v2, p0, Lcom/konka/systemsetting/system/SystemPageManager$1;->this$0:Lcom/konka/systemsetting/system/SystemPageManager;

    # getter for: Lcom/konka/systemsetting/system/SystemPageManager;->m_iCurPage:I
    invoke-static {v2}, Lcom/konka/systemsetting/system/SystemPageManager;->access$0(Lcom/konka/systemsetting/system/SystemPageManager;)I

    move-result v2

    invoke-virtual {v1, v2}, Lcom/konka/systemsetting/system/SystemPageManager;->showPage(I)V

    goto :goto_0

    :pswitch_1
    iget-object v1, p0, Lcom/konka/systemsetting/system/SystemPageManager$1;->this$0:Lcom/konka/systemsetting/system/SystemPageManager;

    iget-object v2, p0, Lcom/konka/systemsetting/system/SystemPageManager$1;->this$0:Lcom/konka/systemsetting/system/SystemPageManager;

    # getter for: Lcom/konka/systemsetting/system/SystemPageManager;->m_iCurPage:I
    invoke-static {v2}, Lcom/konka/systemsetting/system/SystemPageManager;->access$0(Lcom/konka/systemsetting/system/SystemPageManager;)I

    move-result v2

    invoke-static {v1, v2}, Lcom/konka/systemsetting/system/SystemPageManager;->access$1(Lcom/konka/systemsetting/system/SystemPageManager;I)V

    iget-object v1, p0, Lcom/konka/systemsetting/system/SystemPageManager$1;->this$0:Lcom/konka/systemsetting/system/SystemPageManager;

    invoke-static {v1, v4}, Lcom/konka/systemsetting/system/SystemPageManager;->access$2(Lcom/konka/systemsetting/system/SystemPageManager;I)V

    goto :goto_1

    :pswitch_2
    iget-object v1, p0, Lcom/konka/systemsetting/system/SystemPageManager$1;->this$0:Lcom/konka/systemsetting/system/SystemPageManager;

    iget-object v2, p0, Lcom/konka/systemsetting/system/SystemPageManager$1;->this$0:Lcom/konka/systemsetting/system/SystemPageManager;

    # getter for: Lcom/konka/systemsetting/system/SystemPageManager;->m_iCurPage:I
    invoke-static {v2}, Lcom/konka/systemsetting/system/SystemPageManager;->access$0(Lcom/konka/systemsetting/system/SystemPageManager;)I

    move-result v2

    invoke-static {v1, v2}, Lcom/konka/systemsetting/system/SystemPageManager;->access$1(Lcom/konka/systemsetting/system/SystemPageManager;I)V

    iget-object v1, p0, Lcom/konka/systemsetting/system/SystemPageManager$1;->this$0:Lcom/konka/systemsetting/system/SystemPageManager;

    invoke-static {v1, v5}, Lcom/konka/systemsetting/system/SystemPageManager;->access$2(Lcom/konka/systemsetting/system/SystemPageManager;I)V

    goto :goto_1

    :pswitch_3
    iget-object v1, p0, Lcom/konka/systemsetting/system/SystemPageManager$1;->this$0:Lcom/konka/systemsetting/system/SystemPageManager;

    iget-object v2, p0, Lcom/konka/systemsetting/system/SystemPageManager$1;->this$0:Lcom/konka/systemsetting/system/SystemPageManager;

    # getter for: Lcom/konka/systemsetting/system/SystemPageManager;->m_iCurPage:I
    invoke-static {v2}, Lcom/konka/systemsetting/system/SystemPageManager;->access$0(Lcom/konka/systemsetting/system/SystemPageManager;)I

    move-result v2

    invoke-static {v1, v2}, Lcom/konka/systemsetting/system/SystemPageManager;->access$1(Lcom/konka/systemsetting/system/SystemPageManager;I)V

    iget-object v1, p0, Lcom/konka/systemsetting/system/SystemPageManager$1;->this$0:Lcom/konka/systemsetting/system/SystemPageManager;

    const/4 v2, 0x3

    invoke-static {v1, v2}, Lcom/konka/systemsetting/system/SystemPageManager;->access$2(Lcom/konka/systemsetting/system/SystemPageManager;I)V

    goto :goto_1

    :pswitch_4
    iget-object v1, p0, Lcom/konka/systemsetting/system/SystemPageManager$1;->this$0:Lcom/konka/systemsetting/system/SystemPageManager;

    iget-object v2, p0, Lcom/konka/systemsetting/system/SystemPageManager$1;->this$0:Lcom/konka/systemsetting/system/SystemPageManager;

    # getter for: Lcom/konka/systemsetting/system/SystemPageManager;->m_iCurPage:I
    invoke-static {v2}, Lcom/konka/systemsetting/system/SystemPageManager;->access$0(Lcom/konka/systemsetting/system/SystemPageManager;)I

    move-result v2

    invoke-static {v1, v2}, Lcom/konka/systemsetting/system/SystemPageManager;->access$1(Lcom/konka/systemsetting/system/SystemPageManager;I)V

    iget-object v1, p0, Lcom/konka/systemsetting/system/SystemPageManager$1;->this$0:Lcom/konka/systemsetting/system/SystemPageManager;

    const/4 v2, 0x4

    invoke-static {v1, v2}, Lcom/konka/systemsetting/system/SystemPageManager;->access$2(Lcom/konka/systemsetting/system/SystemPageManager;I)V

    goto :goto_1

    :cond_1
    iget-object v1, p0, Lcom/konka/systemsetting/system/SystemPageManager$1;->this$0:Lcom/konka/systemsetting/system/SystemPageManager;

    iget-object v2, p0, Lcom/konka/systemsetting/system/SystemPageManager$1;->this$0:Lcom/konka/systemsetting/system/SystemPageManager;

    # getter for: Lcom/konka/systemsetting/system/SystemPageManager;->m_iCurPage:I
    invoke-static {v2}, Lcom/konka/systemsetting/system/SystemPageManager;->access$0(Lcom/konka/systemsetting/system/SystemPageManager;)I

    move-result v2

    # invokes: Lcom/konka/systemsetting/system/SystemPageManager;->changeItemStatus(II)V
    invoke-static {v1, v2, v4}, Lcom/konka/systemsetting/system/SystemPageManager;->access$4(Lcom/konka/systemsetting/system/SystemPageManager;II)V

    goto/16 :goto_0

    :pswitch_data_0
    .packed-switch 0x7f090097
        :pswitch_0    # com.konka.systemsetting.R.id.linearlayout_system_tab_languageandmusic
        :pswitch_1    # com.konka.systemsetting.R.id.linearlayout_system_tab_inputsettings
        :pswitch_2    # com.konka.systemsetting.R.id.linearlayout_system_tab_systeminfo
        :pswitch_3    # com.konka.systemsetting.R.id.linearlayout_system_tab_safetyandsoftupgrade
        :pswitch_4    # com.konka.systemsetting.R.id.linearlayout_system_tab_factoryreset
    .end packed-switch
.end method
