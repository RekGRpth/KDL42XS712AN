.class Lcom/konka/systemsetting/system/SystemLanguageAndMusic$6;
.super Ljava/lang/Object;
.source "SystemLanguageAndMusic.java"

# interfaces
.implements Landroid/view/View$OnFocusChangeListener;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/konka/systemsetting/system/SystemLanguageAndMusic;->SetOnFocusChangeListener()V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/konka/systemsetting/system/SystemLanguageAndMusic;


# direct methods
.method constructor <init>(Lcom/konka/systemsetting/system/SystemLanguageAndMusic;)V
    .locals 0

    iput-object p1, p0, Lcom/konka/systemsetting/system/SystemLanguageAndMusic$6;->this$0:Lcom/konka/systemsetting/system/SystemLanguageAndMusic;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onFocusChange(Landroid/view/View;Z)V
    .locals 4
    .param p1    # Landroid/view/View;
    .param p2    # Z

    const v3, 0x7f020032    # com.konka.systemsetting.R.drawable.syssettingthirditemsinglefocus

    const v2, 0x7f020031    # com.konka.systemsetting.R.drawable.syssettingthirditemsingle

    iget-object v0, p0, Lcom/konka/systemsetting/system/SystemLanguageAndMusic$6;->this$0:Lcom/konka/systemsetting/system/SystemLanguageAndMusic;

    invoke-virtual {p1}, Landroid/view/View;->getId()I

    move-result v1

    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/konka/systemsetting/system/SystemLanguageAndMusic;->access$2(Lcom/konka/systemsetting/system/SystemLanguageAndMusic;Ljava/lang/Integer;)V

    iget-object v0, p0, Lcom/konka/systemsetting/system/SystemLanguageAndMusic$6;->this$0:Lcom/konka/systemsetting/system/SystemLanguageAndMusic;

    # getter for: Lcom/konka/systemsetting/system/SystemLanguageAndMusic;->m_currItemId:Ljava/lang/Integer;
    invoke-static {v0}, Lcom/konka/systemsetting/system/SystemLanguageAndMusic;->access$3(Lcom/konka/systemsetting/system/SystemLanguageAndMusic;)Ljava/lang/Integer;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/Integer;->intValue()I

    move-result v0

    packed-switch v0, :pswitch_data_0

    :goto_0
    :pswitch_0
    return-void

    :pswitch_1
    if-eqz p2, :cond_0

    iget-object v0, p0, Lcom/konka/systemsetting/system/SystemLanguageAndMusic$6;->this$0:Lcom/konka/systemsetting/system/SystemLanguageAndMusic;

    # getter for: Lcom/konka/systemsetting/system/SystemLanguageAndMusic;->mContext:Lcom/konka/systemsetting/MainActivity;
    invoke-static {v0}, Lcom/konka/systemsetting/system/SystemLanguageAndMusic;->access$1(Lcom/konka/systemsetting/system/SystemLanguageAndMusic;)Lcom/konka/systemsetting/MainActivity;

    move-result-object v0

    iget-object v0, v0, Lcom/konka/systemsetting/MainActivity;->m_systemPageManager:Lcom/konka/systemsetting/system/SystemPageManager;

    iget-object v1, p0, Lcom/konka/systemsetting/system/SystemLanguageAndMusic$6;->this$0:Lcom/konka/systemsetting/system/SystemLanguageAndMusic;

    # getter for: Lcom/konka/systemsetting/system/SystemLanguageAndMusic;->mContext:Lcom/konka/systemsetting/MainActivity;
    invoke-static {v1}, Lcom/konka/systemsetting/system/SystemLanguageAndMusic;->access$1(Lcom/konka/systemsetting/system/SystemLanguageAndMusic;)Lcom/konka/systemsetting/MainActivity;

    move-result-object v1

    iget-object v1, v1, Lcom/konka/systemsetting/MainActivity;->m_systemPageManager:Lcom/konka/systemsetting/system/SystemPageManager;

    invoke-virtual {v1}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    const/4 v1, 0x0

    iget-object v2, p0, Lcom/konka/systemsetting/system/SystemLanguageAndMusic$6;->this$0:Lcom/konka/systemsetting/system/SystemLanguageAndMusic;

    # getter for: Lcom/konka/systemsetting/system/SystemLanguageAndMusic;->mContext:Lcom/konka/systemsetting/MainActivity;
    invoke-static {v2}, Lcom/konka/systemsetting/system/SystemLanguageAndMusic;->access$1(Lcom/konka/systemsetting/system/SystemLanguageAndMusic;)Lcom/konka/systemsetting/MainActivity;

    move-result-object v2

    iget-object v2, v2, Lcom/konka/systemsetting/MainActivity;->m_systemPageManager:Lcom/konka/systemsetting/system/SystemPageManager;

    invoke-virtual {v2}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    const/4 v2, 0x1

    invoke-virtual {v0, v1, v2}, Lcom/konka/systemsetting/system/SystemPageManager;->setLastFocus(II)V

    iget-object v0, p0, Lcom/konka/systemsetting/system/SystemLanguageAndMusic$6;->this$0:Lcom/konka/systemsetting/system/SystemLanguageAndMusic;

    iget-object v0, v0, Lcom/konka/systemsetting/system/SystemLanguageAndMusic;->linearlayout_itemCurrentLanguage:Landroid/widget/LinearLayout;

    invoke-virtual {v0, v3}, Landroid/widget/LinearLayout;->setBackgroundResource(I)V

    goto :goto_0

    :cond_0
    iget-object v0, p0, Lcom/konka/systemsetting/system/SystemLanguageAndMusic$6;->this$0:Lcom/konka/systemsetting/system/SystemLanguageAndMusic;

    iget-object v0, v0, Lcom/konka/systemsetting/system/SystemLanguageAndMusic;->linearlayout_itemCurrentLanguage:Landroid/widget/LinearLayout;

    invoke-virtual {v0, v2}, Landroid/widget/LinearLayout;->setBackgroundResource(I)V

    goto :goto_0

    :pswitch_2
    if-eqz p2, :cond_1

    iget-object v0, p0, Lcom/konka/systemsetting/system/SystemLanguageAndMusic$6;->this$0:Lcom/konka/systemsetting/system/SystemLanguageAndMusic;

    iget-object v0, v0, Lcom/konka/systemsetting/system/SystemLanguageAndMusic;->linearlayout_itemMic:Landroid/widget/LinearLayout;

    invoke-virtual {v0, v3}, Landroid/widget/LinearLayout;->setBackgroundResource(I)V

    goto :goto_0

    :cond_1
    iget-object v0, p0, Lcom/konka/systemsetting/system/SystemLanguageAndMusic$6;->this$0:Lcom/konka/systemsetting/system/SystemLanguageAndMusic;

    iget-object v0, v0, Lcom/konka/systemsetting/system/SystemLanguageAndMusic;->linearlayout_itemMic:Landroid/widget/LinearLayout;

    invoke-virtual {v0, v2}, Landroid/widget/LinearLayout;->setBackgroundResource(I)V

    goto :goto_0

    :pswitch_3
    if-eqz p2, :cond_2

    iget-object v0, p0, Lcom/konka/systemsetting/system/SystemLanguageAndMusic$6;->this$0:Lcom/konka/systemsetting/system/SystemLanguageAndMusic;

    iget-object v0, v0, Lcom/konka/systemsetting/system/SystemLanguageAndMusic;->linearlayout_itemBootingMusic:Landroid/widget/LinearLayout;

    invoke-virtual {v0, v3}, Landroid/widget/LinearLayout;->setBackgroundResource(I)V

    goto :goto_0

    :cond_2
    iget-object v0, p0, Lcom/konka/systemsetting/system/SystemLanguageAndMusic$6;->this$0:Lcom/konka/systemsetting/system/SystemLanguageAndMusic;

    iget-object v0, v0, Lcom/konka/systemsetting/system/SystemLanguageAndMusic;->linearlayout_itemBootingMusic:Landroid/widget/LinearLayout;

    invoke-virtual {v0, v2}, Landroid/widget/LinearLayout;->setBackgroundResource(I)V

    goto :goto_0

    :pswitch_data_0
    .packed-switch 0x7f090092
        :pswitch_1    # com.konka.systemsetting.R.id.sys_system_item_language
        :pswitch_2    # com.konka.systemsetting.R.id.sys_system_item_mic
        :pswitch_0    # com.konka.systemsetting.R.id.sys_system_micswitch
        :pswitch_3    # com.konka.systemsetting.R.id.sys_system_item_music
    .end packed-switch
.end method
