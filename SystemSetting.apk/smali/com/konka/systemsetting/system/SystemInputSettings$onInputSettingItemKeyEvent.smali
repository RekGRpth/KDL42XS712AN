.class Lcom/konka/systemsetting/system/SystemInputSettings$onInputSettingItemKeyEvent;
.super Ljava/lang/Object;
.source "SystemInputSettings.java"

# interfaces
.implements Landroid/view/View$OnKeyListener;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/konka/systemsetting/system/SystemInputSettings;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x2
    name = "onInputSettingItemKeyEvent"
.end annotation


# instance fields
.field final synthetic this$0:Lcom/konka/systemsetting/system/SystemInputSettings;


# direct methods
.method private constructor <init>(Lcom/konka/systemsetting/system/SystemInputSettings;)V
    .locals 0

    iput-object p1, p0, Lcom/konka/systemsetting/system/SystemInputSettings$onInputSettingItemKeyEvent;->this$0:Lcom/konka/systemsetting/system/SystemInputSettings;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method synthetic constructor <init>(Lcom/konka/systemsetting/system/SystemInputSettings;Lcom/konka/systemsetting/system/SystemInputSettings$onInputSettingItemKeyEvent;)V
    .locals 0

    invoke-direct {p0, p1}, Lcom/konka/systemsetting/system/SystemInputSettings$onInputSettingItemKeyEvent;-><init>(Lcom/konka/systemsetting/system/SystemInputSettings;)V

    return-void
.end method


# virtual methods
.method public onKey(Landroid/view/View;ILandroid/view/KeyEvent;)Z
    .locals 8
    .param p1    # Landroid/view/View;
    .param p2    # I
    .param p3    # Landroid/view/KeyEvent;

    const/4 v6, 0x0

    const/16 v7, 0x42

    if-eq p2, v7, :cond_0

    const/16 v7, 0x17

    if-ne p2, v7, :cond_1

    :cond_0
    invoke-virtual {p3}, Landroid/view/KeyEvent;->getAction()I

    move-result v7

    if-nez v7, :cond_1

    check-cast p1, Landroid/widget/LinearLayout;

    invoke-virtual {p1, v6}, Landroid/widget/LinearLayout;->getChildAt(I)Landroid/view/View;

    move-result-object v5

    check-cast v5, Landroid/widget/TextView;

    const/4 v1, 0x0

    const/4 v0, 0x0

    :goto_0
    iget-object v6, p0, Lcom/konka/systemsetting/system/SystemInputSettings$onInputSettingItemKeyEvent;->this$0:Lcom/konka/systemsetting/system/SystemInputSettings;

    # getter for: Lcom/konka/systemsetting/system/SystemInputSettings;->strInputAppNames:[Ljava/lang/String;
    invoke-static {v6}, Lcom/konka/systemsetting/system/SystemInputSettings;->access$1(Lcom/konka/systemsetting/system/SystemInputSettings;)[Ljava/lang/String;

    move-result-object v6

    array-length v6, v6

    if-lt v0, v6, :cond_2

    :goto_1
    new-instance v2, Landroid/content/Intent;

    const-string v6, "android.intent.action.MAIN"

    invoke-direct {v2, v6}, Landroid/content/Intent;-><init>(Ljava/lang/String;)V

    iget-object v6, p0, Lcom/konka/systemsetting/system/SystemInputSettings$onInputSettingItemKeyEvent;->this$0:Lcom/konka/systemsetting/system/SystemInputSettings;

    # getter for: Lcom/konka/systemsetting/system/SystemInputSettings;->list:Ljava/util/List;
    invoke-static {v6}, Lcom/konka/systemsetting/system/SystemInputSettings;->access$2(Lcom/konka/systemsetting/system/SystemInputSettings;)Ljava/util/List;

    move-result-object v6

    invoke-interface {v6, v1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v6

    check-cast v6, Landroid/view/inputmethod/InputMethodInfo;

    invoke-virtual {v6}, Landroid/view/inputmethod/InputMethodInfo;->getPackageName()Ljava/lang/String;

    move-result-object v4

    iget-object v6, p0, Lcom/konka/systemsetting/system/SystemInputSettings$onInputSettingItemKeyEvent;->this$0:Lcom/konka/systemsetting/system/SystemInputSettings;

    # getter for: Lcom/konka/systemsetting/system/SystemInputSettings;->list:Ljava/util/List;
    invoke-static {v6}, Lcom/konka/systemsetting/system/SystemInputSettings;->access$2(Lcom/konka/systemsetting/system/SystemInputSettings;)Ljava/util/List;

    move-result-object v6

    invoke-interface {v6, v1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v6

    check-cast v6, Landroid/view/inputmethod/InputMethodInfo;

    invoke-virtual {v6}, Landroid/view/inputmethod/InputMethodInfo;->getSettingsActivity()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v4, v3}, Landroid/content/Intent;->setClassName(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    iget-object v6, p0, Lcom/konka/systemsetting/system/SystemInputSettings$onInputSettingItemKeyEvent;->this$0:Lcom/konka/systemsetting/system/SystemInputSettings;

    # getter for: Lcom/konka/systemsetting/system/SystemInputSettings;->mContext:Lcom/konka/systemsetting/MainActivity;
    invoke-static {v6}, Lcom/konka/systemsetting/system/SystemInputSettings;->access$3(Lcom/konka/systemsetting/system/SystemInputSettings;)Lcom/konka/systemsetting/MainActivity;

    move-result-object v6

    invoke-virtual {v6, v2}, Lcom/konka/systemsetting/MainActivity;->startActivity(Landroid/content/Intent;)V

    const/4 v6, 0x1

    :cond_1
    return v6

    :cond_2
    iget-object v6, p0, Lcom/konka/systemsetting/system/SystemInputSettings$onInputSettingItemKeyEvent;->this$0:Lcom/konka/systemsetting/system/SystemInputSettings;

    # getter for: Lcom/konka/systemsetting/system/SystemInputSettings;->strInputAppNames:[Ljava/lang/String;
    invoke-static {v6}, Lcom/konka/systemsetting/system/SystemInputSettings;->access$1(Lcom/konka/systemsetting/system/SystemInputSettings;)[Ljava/lang/String;

    move-result-object v6

    aget-object v6, v6, v0

    invoke-virtual {v5}, Landroid/widget/TextView;->getText()Ljava/lang/CharSequence;

    move-result-object v7

    invoke-interface {v7}, Ljava/lang/CharSequence;->toString()Ljava/lang/String;

    move-result-object v7

    invoke-virtual {v6, v7}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v6

    if-eqz v6, :cond_3

    move v1, v0

    goto :goto_1

    :cond_3
    add-int/lit8 v0, v0, 0x1

    goto :goto_0
.end method
