.class public Lcom/konka/systemsetting/system/SystemSoftUpgrade;
.super Ljava/lang/Object;
.source "SystemSoftUpgrade.java"


# static fields
.field static UPGRATE_END_FAIL:I

.field static UPGRATE_END_FILE_NOT_FOUND:I

.field static UPGRATE_END_SUCCESS:I

.field static UPGRATE_START:I


# instance fields
.field private mWarnInstallApps:Landroid/content/DialogInterface;

.field private m_btnUpgradeByUSB:Landroid/widget/Button;

.field private m_btnUpgradeOnline:Landroid/widget/Button;

.field private m_context:Lcom/konka/systemsetting/MainActivity;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    const/4 v0, 0x0

    sput v0, Lcom/konka/systemsetting/system/SystemSoftUpgrade;->UPGRATE_END_FAIL:I

    const/4 v0, 0x1

    sput v0, Lcom/konka/systemsetting/system/SystemSoftUpgrade;->UPGRATE_END_SUCCESS:I

    const/4 v0, 0x2

    sput v0, Lcom/konka/systemsetting/system/SystemSoftUpgrade;->UPGRATE_END_FILE_NOT_FOUND:I

    const/4 v0, 0x3

    sput v0, Lcom/konka/systemsetting/system/SystemSoftUpgrade;->UPGRATE_START:I

    return-void
.end method

.method public constructor <init>(Lcom/konka/systemsetting/MainActivity;)V
    .locals 2
    .param p1    # Lcom/konka/systemsetting/MainActivity;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    iput-object p1, p0, Lcom/konka/systemsetting/system/SystemSoftUpgrade;->m_context:Lcom/konka/systemsetting/MainActivity;

    iget-object v0, p0, Lcom/konka/systemsetting/system/SystemSoftUpgrade;->m_context:Lcom/konka/systemsetting/MainActivity;

    const v1, 0x7f0900a4    # com.konka.systemsetting.R.id.btn_upgrade_online

    invoke-virtual {v0, v1}, Lcom/konka/systemsetting/MainActivity;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/Button;

    iput-object v0, p0, Lcom/konka/systemsetting/system/SystemSoftUpgrade;->m_btnUpgradeOnline:Landroid/widget/Button;

    iget-object v0, p0, Lcom/konka/systemsetting/system/SystemSoftUpgrade;->m_context:Lcom/konka/systemsetting/MainActivity;

    const v1, 0x7f0900a3    # com.konka.systemsetting.R.id.btn_upgrade_byusb

    invoke-virtual {v0, v1}, Lcom/konka/systemsetting/MainActivity;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/Button;

    iput-object v0, p0, Lcom/konka/systemsetting/system/SystemSoftUpgrade;->m_btnUpgradeByUSB:Landroid/widget/Button;

    iget-object v0, p0, Lcom/konka/systemsetting/system/SystemSoftUpgrade;->m_context:Lcom/konka/systemsetting/MainActivity;

    invoke-static {v0}, Lcom/konka/kkimplements/tv/TvDeskProviderImpl;->getInstance(Landroid/content/Context;)Lcom/konka/kkinterface/tv/TvDeskProvider;

    move-result-object v0

    invoke-interface {v0}, Lcom/konka/kkinterface/tv/TvDeskProvider;->getCommonManagerInstance()Lcom/konka/kkinterface/tv/CommonDesk;

    move-result-object v0

    invoke-interface {v0}, Lcom/konka/kkinterface/tv/CommonDesk;->isEnableUpgradeOnline()Z

    move-result v0

    if-nez v0, :cond_0

    iget-object v0, p0, Lcom/konka/systemsetting/system/SystemSoftUpgrade;->m_btnUpgradeOnline:Landroid/widget/Button;

    const/16 v1, 0x8

    invoke-virtual {v0, v1}, Landroid/widget/Button;->setVisibility(I)V

    :cond_0
    invoke-direct {p0}, Lcom/konka/systemsetting/system/SystemSoftUpgrade;->initUIComponents()V

    invoke-direct {p0}, Lcom/konka/systemsetting/system/SystemSoftUpgrade;->setOnClickListener()V

    invoke-direct {p0}, Lcom/konka/systemsetting/system/SystemSoftUpgrade;->setOnFocusChangeListener()V

    return-void
.end method

.method private static FindFileOnUSB(Ljava/lang/String;)Ljava/lang/String;
    .locals 7
    .param p0    # Ljava/lang/String;

    const/4 v0, 0x0

    new-instance v4, Ljava/io/File;

    const-string v5, "/mnt/usb/"

    invoke-direct {v4, v5}, Ljava/io/File;-><init>(Ljava/lang/String;)V

    if-eqz v4, :cond_0

    invoke-virtual {v4}, Ljava/io/File;->exists()Z

    move-result v5

    if-eqz v5, :cond_0

    invoke-virtual {v4}, Ljava/io/File;->listFiles()[Ljava/io/File;

    move-result-object v3

    const/4 v1, 0x0

    :goto_0
    array-length v5, v3

    if-lt v1, v5, :cond_1

    :cond_0
    :goto_1
    return-object v0

    :cond_1
    new-instance v5, Ljava/lang/StringBuilder;

    const-string v6, "file directory===="

    invoke-direct {v5, v6}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    aget-object v6, v3, v1

    invoke-virtual {v6}, Ljava/io/File;->getPath()Ljava/lang/String;

    move-result-object v6

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    invoke-static {v5}, Lcom/konka/debuginfo/logPrint;->Debug(Ljava/lang/String;)V

    aget-object v5, v3, v1

    invoke-virtual {v5}, Ljava/io/File;->isDirectory()Z

    move-result v5

    if-eqz v5, :cond_2

    new-instance v2, Ljava/io/File;

    new-instance v5, Ljava/lang/StringBuilder;

    aget-object v6, v3, v1

    invoke-virtual {v6}, Ljava/io/File;->getPath()Ljava/lang/String;

    move-result-object v6

    invoke-static {v6}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v6

    invoke-direct {v5, v6}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    const-string v6, "/"

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5, p0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    invoke-direct {v2, v5}, Ljava/io/File;-><init>(Ljava/lang/String;)V

    new-instance v5, Ljava/lang/StringBuilder;

    const-string v6, "file path===="

    invoke-direct {v5, v6}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v2}, Ljava/io/File;->getPath()Ljava/lang/String;

    move-result-object v6

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    invoke-static {v5}, Lcom/konka/debuginfo/logPrint;->Debug(Ljava/lang/String;)V

    if-eqz v2, :cond_2

    invoke-virtual {v2}, Ljava/io/File;->exists()Z

    move-result v5

    if-eqz v5, :cond_2

    invoke-virtual {v2}, Ljava/io/File;->getPath()Ljava/lang/String;

    move-result-object v0

    goto :goto_1

    :cond_2
    add-int/lit8 v1, v1, 0x1

    goto :goto_0
.end method

.method static synthetic access$0(Lcom/konka/systemsetting/system/SystemSoftUpgrade;)V
    .locals 0

    invoke-direct {p0}, Lcom/konka/systemsetting/system/SystemSoftUpgrade;->startUpgradeActivity()V

    return-void
.end method

.method static synthetic access$1(Lcom/konka/systemsetting/system/SystemSoftUpgrade;)V
    .locals 0

    invoke-direct {p0}, Lcom/konka/systemsetting/system/SystemSoftUpgrade;->startUpgradeByUSB()V

    return-void
.end method

.method static synthetic access$2(Lcom/konka/systemsetting/system/SystemSoftUpgrade;)Lcom/konka/systemsetting/MainActivity;
    .locals 1

    iget-object v0, p0, Lcom/konka/systemsetting/system/SystemSoftUpgrade;->m_context:Lcom/konka/systemsetting/MainActivity;

    return-object v0
.end method

.method private initUIComponents()V
    .locals 3

    const v2, 0x7f0900a4    # com.konka.systemsetting.R.id.btn_upgrade_online

    const v1, 0x7f0900a3    # com.konka.systemsetting.R.id.btn_upgrade_byusb

    iget-object v0, p0, Lcom/konka/systemsetting/system/SystemSoftUpgrade;->m_btnUpgradeByUSB:Landroid/widget/Button;

    invoke-virtual {v0, v2}, Landroid/widget/Button;->setNextFocusUpId(I)V

    iget-object v0, p0, Lcom/konka/systemsetting/system/SystemSoftUpgrade;->m_btnUpgradeByUSB:Landroid/widget/Button;

    invoke-virtual {v0, v2}, Landroid/widget/Button;->setNextFocusDownId(I)V

    iget-object v0, p0, Lcom/konka/systemsetting/system/SystemSoftUpgrade;->m_btnUpgradeOnline:Landroid/widget/Button;

    invoke-virtual {v0, v1}, Landroid/widget/Button;->setNextFocusUpId(I)V

    iget-object v0, p0, Lcom/konka/systemsetting/system/SystemSoftUpgrade;->m_btnUpgradeOnline:Landroid/widget/Button;

    invoke-virtual {v0, v1}, Landroid/widget/Button;->setNextFocusDownId(I)V

    return-void
.end method

.method private setOnClickListener()V
    .locals 2

    new-instance v0, Lcom/konka/systemsetting/system/SystemSoftUpgrade$1;

    invoke-direct {v0, p0}, Lcom/konka/systemsetting/system/SystemSoftUpgrade$1;-><init>(Lcom/konka/systemsetting/system/SystemSoftUpgrade;)V

    iget-object v1, p0, Lcom/konka/systemsetting/system/SystemSoftUpgrade;->m_btnUpgradeOnline:Landroid/widget/Button;

    invoke-virtual {v1, v0}, Landroid/widget/Button;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    iget-object v1, p0, Lcom/konka/systemsetting/system/SystemSoftUpgrade;->m_btnUpgradeByUSB:Landroid/widget/Button;

    invoke-virtual {v1, v0}, Landroid/widget/Button;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    return-void
.end method

.method private setOnFocusChangeListener()V
    .locals 2

    new-instance v0, Lcom/konka/systemsetting/system/SystemSoftUpgrade$2;

    invoke-direct {v0, p0}, Lcom/konka/systemsetting/system/SystemSoftUpgrade$2;-><init>(Lcom/konka/systemsetting/system/SystemSoftUpgrade;)V

    iget-object v1, p0, Lcom/konka/systemsetting/system/SystemSoftUpgrade;->m_btnUpgradeOnline:Landroid/widget/Button;

    invoke-virtual {v1, v0}, Landroid/widget/Button;->setOnFocusChangeListener(Landroid/view/View$OnFocusChangeListener;)V

    iget-object v1, p0, Lcom/konka/systemsetting/system/SystemSoftUpgrade;->m_btnUpgradeByUSB:Landroid/widget/Button;

    invoke-virtual {v1, v0}, Landroid/widget/Button;->setOnFocusChangeListener(Landroid/view/View$OnFocusChangeListener;)V

    return-void
.end method

.method private startUpgradeActivity()V
    .locals 3

    const-string v1, "start the activity=====com.konka.upgrade"

    invoke-static {v1}, Lcom/konka/debuginfo/logPrint;->Debug(Ljava/lang/String;)V

    new-instance v0, Landroid/content/Intent;

    invoke-direct {v0}, Landroid/content/Intent;-><init>()V

    const-string v1, "com.konka.upgrade"

    const-string v2, "com.konka.upgrade.UpgradeActivity"

    invoke-virtual {v0, v1, v2}, Landroid/content/Intent;->setClassName(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    const/high16 v1, 0x10200000

    invoke-virtual {v0, v1}, Landroid/content/Intent;->addFlags(I)Landroid/content/Intent;

    iget-object v1, p0, Lcom/konka/systemsetting/system/SystemSoftUpgrade;->m_context:Lcom/konka/systemsetting/MainActivity;

    invoke-virtual {v1, v0}, Lcom/konka/systemsetting/MainActivity;->startActivity(Landroid/content/Intent;)V

    return-void
.end method

.method private startUpgradeByUSB()V
    .locals 5

    const-string v2, "MstarUpgrade.bin"

    invoke-static {v2}, Lcom/konka/systemsetting/system/SystemSoftUpgrade;->FindFileOnUSB(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    if-nez v1, :cond_0

    iget-object v2, p0, Lcom/konka/systemsetting/system/SystemSoftUpgrade;->m_context:Lcom/konka/systemsetting/MainActivity;

    const v3, 0x7f0600d4    # com.konka.systemsetting.R.string.usbupgrade_filenotfound

    const/4 v4, 0x0

    invoke-static {v2, v3, v4}, Landroid/widget/Toast;->makeText(Landroid/content/Context;II)Landroid/widget/Toast;

    move-result-object v2

    invoke-virtual {v2}, Landroid/widget/Toast;->show()V

    :goto_0
    return-void

    :cond_0
    new-instance v0, Landroid/app/AlertDialog$Builder;

    iget-object v2, p0, Lcom/konka/systemsetting/system/SystemSoftUpgrade;->m_context:Lcom/konka/systemsetting/MainActivity;

    invoke-direct {v0, v2}, Landroid/app/AlertDialog$Builder;-><init>(Landroid/content/Context;)V

    const v2, 0x7f06002d    # com.konka.systemsetting.R.string.str_systempage_upgrade_byusb

    invoke-virtual {v0, v2}, Landroid/app/AlertDialog$Builder;->setTitle(I)Landroid/app/AlertDialog$Builder;

    const v2, 0x7f0600d5    # com.konka.systemsetting.R.string.usbupgrade_reboot

    invoke-virtual {v0, v2}, Landroid/app/AlertDialog$Builder;->setMessage(I)Landroid/app/AlertDialog$Builder;

    const v2, 0x7f0600d6    # com.konka.systemsetting.R.string.usbupgrade_cancel

    new-instance v3, Lcom/konka/systemsetting/system/SystemSoftUpgrade$3;

    invoke-direct {v3, p0}, Lcom/konka/systemsetting/system/SystemSoftUpgrade$3;-><init>(Lcom/konka/systemsetting/system/SystemSoftUpgrade;)V

    invoke-virtual {v0, v2, v3}, Landroid/app/AlertDialog$Builder;->setNegativeButton(ILandroid/content/DialogInterface$OnClickListener;)Landroid/app/AlertDialog$Builder;

    const v2, 0x7f0600d7    # com.konka.systemsetting.R.string.usbupgrade_exec

    new-instance v3, Lcom/konka/systemsetting/system/SystemSoftUpgrade$4;

    invoke-direct {v3, p0}, Lcom/konka/systemsetting/system/SystemSoftUpgrade$4;-><init>(Lcom/konka/systemsetting/system/SystemSoftUpgrade;)V

    invoke-virtual {v0, v2, v3}, Landroid/app/AlertDialog$Builder;->setNeutralButton(ILandroid/content/DialogInterface$OnClickListener;)Landroid/app/AlertDialog$Builder;

    invoke-virtual {v0}, Landroid/app/AlertDialog$Builder;->show()Landroid/app/AlertDialog;

    goto :goto_0
.end method


# virtual methods
.method protected getContentResolver()Landroid/content/ContentResolver;
    .locals 1

    iget-object v0, p0, Lcom/konka/systemsetting/system/SystemSoftUpgrade;->m_context:Lcom/konka/systemsetting/MainActivity;

    invoke-virtual {v0}, Lcom/konka/systemsetting/MainActivity;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v0

    return-object v0
.end method
