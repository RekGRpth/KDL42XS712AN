.class Lcom/konka/systemsetting/system/SystemInputSettings$1;
.super Ljava/lang/Object;
.source "SystemInputSettings.java"

# interfaces
.implements Landroid/content/DialogInterface$OnClickListener;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/konka/systemsetting/system/SystemInputSettings;->showInputPickerDialog()V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/konka/systemsetting/system/SystemInputSettings;


# direct methods
.method constructor <init>(Lcom/konka/systemsetting/system/SystemInputSettings;)V
    .locals 0

    iput-object p1, p0, Lcom/konka/systemsetting/system/SystemInputSettings$1;->this$0:Lcom/konka/systemsetting/system/SystemInputSettings;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onClick(Landroid/content/DialogInterface;I)V
    .locals 3
    .param p1    # Landroid/content/DialogInterface;
    .param p2    # I

    iget-object v0, p0, Lcom/konka/systemsetting/system/SystemInputSettings$1;->this$0:Lcom/konka/systemsetting/system/SystemInputSettings;

    # getter for: Lcom/konka/systemsetting/system/SystemInputSettings;->tvCurrInput:Landroid/widget/TextView;
    invoke-static {v0}, Lcom/konka/systemsetting/system/SystemInputSettings;->access$4(Lcom/konka/systemsetting/system/SystemInputSettings;)Landroid/widget/TextView;

    move-result-object v0

    iget-object v1, p0, Lcom/konka/systemsetting/system/SystemInputSettings$1;->this$0:Lcom/konka/systemsetting/system/SystemInputSettings;

    # getter for: Lcom/konka/systemsetting/system/SystemInputSettings;->strInputAppNames:[Ljava/lang/String;
    invoke-static {v1}, Lcom/konka/systemsetting/system/SystemInputSettings;->access$1(Lcom/konka/systemsetting/system/SystemInputSettings;)[Ljava/lang/String;

    move-result-object v1

    aget-object v1, v1, p2

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    iget-object v0, p0, Lcom/konka/systemsetting/system/SystemInputSettings$1;->this$0:Lcom/konka/systemsetting/system/SystemInputSettings;

    # getter for: Lcom/konka/systemsetting/system/SystemInputSettings;->mContext:Lcom/konka/systemsetting/MainActivity;
    invoke-static {v0}, Lcom/konka/systemsetting/system/SystemInputSettings;->access$3(Lcom/konka/systemsetting/system/SystemInputSettings;)Lcom/konka/systemsetting/MainActivity;

    move-result-object v0

    invoke-virtual {v0}, Lcom/konka/systemsetting/MainActivity;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v1

    const-string v2, "default_input_method"

    iget-object v0, p0, Lcom/konka/systemsetting/system/SystemInputSettings$1;->this$0:Lcom/konka/systemsetting/system/SystemInputSettings;

    # getter for: Lcom/konka/systemsetting/system/SystemInputSettings;->list:Ljava/util/List;
    invoke-static {v0}, Lcom/konka/systemsetting/system/SystemInputSettings;->access$2(Lcom/konka/systemsetting/system/SystemInputSettings;)Ljava/util/List;

    move-result-object v0

    invoke-interface {v0, p2}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/view/inputmethod/InputMethodInfo;

    invoke-virtual {v0}, Landroid/view/inputmethod/InputMethodInfo;->getId()Ljava/lang/String;

    move-result-object v0

    invoke-static {v1, v2, v0}, Landroid/provider/Settings$Secure;->putString(Landroid/content/ContentResolver;Ljava/lang/String;Ljava/lang/String;)Z

    invoke-interface {p1}, Landroid/content/DialogInterface;->dismiss()V

    return-void
.end method
