.class public Lcom/konka/systemsetting/system/SystemInputSettings;
.super Ljava/lang/Object;
.source "SystemInputSettings.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/konka/systemsetting/system/SystemInputSettings$onClickEvent;,
        Lcom/konka/systemsetting/system/SystemInputSettings$onFocusChangeEvent;,
        Lcom/konka/systemsetting/system/SystemInputSettings$onInputSettingItemClickEvent;,
        Lcom/konka/systemsetting/system/SystemInputSettings$onInputSettingItemKeyEvent;
    }
.end annotation


# instance fields
.field private itemCurrInput:Landroid/widget/LinearLayout;

.field private itemInputsContainer:Landroid/widget/LinearLayout;

.field private list:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Landroid/view/inputmethod/InputMethodInfo;",
            ">;"
        }
    .end annotation
.end field

.field private mContext:Lcom/konka/systemsetting/MainActivity;

.field private manager:Landroid/view/inputmethod/InputMethodManager;

.field private strCurrInput:Ljava/lang/CharSequence;

.field private strInputAppNames:[Ljava/lang/String;

.field private tvCurrInput:Landroid/widget/TextView;


# direct methods
.method public constructor <init>(Lcom/konka/systemsetting/MainActivity;)V
    .locals 3
    .param p1    # Lcom/konka/systemsetting/MainActivity;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    iput-object p1, p0, Lcom/konka/systemsetting/system/SystemInputSettings;->mContext:Lcom/konka/systemsetting/MainActivity;

    iget-object v1, p0, Lcom/konka/systemsetting/system/SystemInputSettings;->mContext:Lcom/konka/systemsetting/MainActivity;

    const-string v2, "input_method"

    invoke-virtual {v1, v2}, Lcom/konka/systemsetting/MainActivity;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Landroid/view/inputmethod/InputMethodManager;

    iput-object v1, p0, Lcom/konka/systemsetting/system/SystemInputSettings;->manager:Landroid/view/inputmethod/InputMethodManager;

    iget-object v1, p0, Lcom/konka/systemsetting/system/SystemInputSettings;->manager:Landroid/view/inputmethod/InputMethodManager;

    invoke-virtual {v1}, Landroid/view/inputmethod/InputMethodManager;->getInputMethodList()Ljava/util/List;

    move-result-object v1

    iput-object v1, p0, Lcom/konka/systemsetting/system/SystemInputSettings;->list:Ljava/util/List;

    iget-object v1, p0, Lcom/konka/systemsetting/system/SystemInputSettings;->list:Ljava/util/List;

    invoke-interface {v1}, Ljava/util/List;->size()I

    move-result v1

    new-array v1, v1, [Ljava/lang/String;

    iput-object v1, p0, Lcom/konka/systemsetting/system/SystemInputSettings;->strInputAppNames:[Ljava/lang/String;

    const/4 v0, 0x0

    :goto_0
    iget-object v1, p0, Lcom/konka/systemsetting/system/SystemInputSettings;->list:Ljava/util/List;

    invoke-interface {v1}, Ljava/util/List;->size()I

    move-result v1

    if-lt v0, v1, :cond_0

    iget-object v1, p0, Lcom/konka/systemsetting/system/SystemInputSettings;->mContext:Lcom/konka/systemsetting/MainActivity;

    invoke-virtual {v1}, Lcom/konka/systemsetting/MainActivity;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v1

    const-string v2, "default_input_method"

    invoke-static {v1, v2}, Landroid/provider/Settings$Secure;->getString(Landroid/content/ContentResolver;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    iput-object v1, p0, Lcom/konka/systemsetting/system/SystemInputSettings;->strCurrInput:Ljava/lang/CharSequence;

    iget-object v1, p0, Lcom/konka/systemsetting/system/SystemInputSettings;->strCurrInput:Ljava/lang/CharSequence;

    invoke-interface {v1}, Ljava/lang/CharSequence;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-direct {p0, v1}, Lcom/konka/systemsetting/system/SystemInputSettings;->getInputAppName(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    iput-object v1, p0, Lcom/konka/systemsetting/system/SystemInputSettings;->strCurrInput:Ljava/lang/CharSequence;

    invoke-direct {p0}, Lcom/konka/systemsetting/system/SystemInputSettings;->findViews()V

    return-void

    :cond_0
    iget-object v2, p0, Lcom/konka/systemsetting/system/SystemInputSettings;->strInputAppNames:[Ljava/lang/String;

    iget-object v1, p0, Lcom/konka/systemsetting/system/SystemInputSettings;->list:Ljava/util/List;

    invoke-interface {v1, v0}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Landroid/view/inputmethod/InputMethodInfo;

    invoke-direct {p0, v1}, Lcom/konka/systemsetting/system/SystemInputSettings;->getInputAppName(Landroid/view/inputmethod/InputMethodInfo;)Ljava/lang/String;

    move-result-object v1

    aput-object v1, v2, v0

    add-int/lit8 v0, v0, 0x1

    goto :goto_0
.end method

.method static synthetic access$0(Lcom/konka/systemsetting/system/SystemInputSettings;)V
    .locals 0

    invoke-direct {p0}, Lcom/konka/systemsetting/system/SystemInputSettings;->showInputPickerDialog()V

    return-void
.end method

.method static synthetic access$1(Lcom/konka/systemsetting/system/SystemInputSettings;)[Ljava/lang/String;
    .locals 1

    iget-object v0, p0, Lcom/konka/systemsetting/system/SystemInputSettings;->strInputAppNames:[Ljava/lang/String;

    return-object v0
.end method

.method static synthetic access$2(Lcom/konka/systemsetting/system/SystemInputSettings;)Ljava/util/List;
    .locals 1

    iget-object v0, p0, Lcom/konka/systemsetting/system/SystemInputSettings;->list:Ljava/util/List;

    return-object v0
.end method

.method static synthetic access$3(Lcom/konka/systemsetting/system/SystemInputSettings;)Lcom/konka/systemsetting/MainActivity;
    .locals 1

    iget-object v0, p0, Lcom/konka/systemsetting/system/SystemInputSettings;->mContext:Lcom/konka/systemsetting/MainActivity;

    return-object v0
.end method

.method static synthetic access$4(Lcom/konka/systemsetting/system/SystemInputSettings;)Landroid/widget/TextView;
    .locals 1

    iget-object v0, p0, Lcom/konka/systemsetting/system/SystemInputSettings;->tvCurrInput:Landroid/widget/TextView;

    return-object v0
.end method

.method private findViews()V
    .locals 12

    const v11, 0x7f090098    # com.konka.systemsetting.R.id.linearlayout_system_tab_inputsettings

    const v10, 0x7f090091    # com.konka.systemsetting.R.id.sys_input_item_current

    const/4 v9, 0x0

    const/4 v8, 0x0

    const/4 v7, 0x1

    iget-object v5, p0, Lcom/konka/systemsetting/system/SystemInputSettings;->mContext:Lcom/konka/systemsetting/MainActivity;

    invoke-virtual {v5, v10}, Lcom/konka/systemsetting/MainActivity;->findViewById(I)Landroid/view/View;

    move-result-object v5

    check-cast v5, Landroid/widget/LinearLayout;

    iput-object v5, p0, Lcom/konka/systemsetting/system/SystemInputSettings;->itemCurrInput:Landroid/widget/LinearLayout;

    iget-object v5, p0, Lcom/konka/systemsetting/system/SystemInputSettings;->itemCurrInput:Landroid/widget/LinearLayout;

    invoke-virtual {v5, v7}, Landroid/widget/LinearLayout;->setClickable(Z)V

    iget-object v5, p0, Lcom/konka/systemsetting/system/SystemInputSettings;->itemCurrInput:Landroid/widget/LinearLayout;

    invoke-virtual {v5, v7}, Landroid/widget/LinearLayout;->setFocusable(Z)V

    iget-object v5, p0, Lcom/konka/systemsetting/system/SystemInputSettings;->itemCurrInput:Landroid/widget/LinearLayout;

    invoke-virtual {v5, v7}, Landroid/widget/LinearLayout;->setFocusableInTouchMode(Z)V

    iget-object v5, p0, Lcom/konka/systemsetting/system/SystemInputSettings;->itemCurrInput:Landroid/widget/LinearLayout;

    new-instance v6, Lcom/konka/systemsetting/system/SystemInputSettings$onFocusChangeEvent;

    invoke-direct {v6, p0, v8}, Lcom/konka/systemsetting/system/SystemInputSettings$onFocusChangeEvent;-><init>(Lcom/konka/systemsetting/system/SystemInputSettings;Lcom/konka/systemsetting/system/SystemInputSettings$onFocusChangeEvent;)V

    invoke-virtual {v5, v6}, Landroid/widget/LinearLayout;->setOnFocusChangeListener(Landroid/view/View$OnFocusChangeListener;)V

    iget-object v5, p0, Lcom/konka/systemsetting/system/SystemInputSettings;->itemCurrInput:Landroid/widget/LinearLayout;

    invoke-virtual {v5, v11}, Landroid/widget/LinearLayout;->setNextFocusLeftId(I)V

    iget-object v5, p0, Lcom/konka/systemsetting/system/SystemInputSettings;->itemCurrInput:Landroid/widget/LinearLayout;

    invoke-virtual {v5, v10}, Landroid/widget/LinearLayout;->setNextFocusUpId(I)V

    iget-object v5, p0, Lcom/konka/systemsetting/system/SystemInputSettings;->itemCurrInput:Landroid/widget/LinearLayout;

    invoke-virtual {v5, v7}, Landroid/widget/LinearLayout;->getChildAt(I)Landroid/view/View;

    move-result-object v5

    check-cast v5, Landroid/widget/TextView;

    iput-object v5, p0, Lcom/konka/systemsetting/system/SystemInputSettings;->tvCurrInput:Landroid/widget/TextView;

    iget-object v5, p0, Lcom/konka/systemsetting/system/SystemInputSettings;->tvCurrInput:Landroid/widget/TextView;

    iget-object v6, p0, Lcom/konka/systemsetting/system/SystemInputSettings;->strCurrInput:Ljava/lang/CharSequence;

    invoke-virtual {v5, v6}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    iget-object v5, p0, Lcom/konka/systemsetting/system/SystemInputSettings;->mContext:Lcom/konka/systemsetting/MainActivity;

    const v6, 0x7f090012    # com.konka.systemsetting.R.id.sys_input_item_settings

    invoke-virtual {v5, v6}, Lcom/konka/systemsetting/MainActivity;->findViewById(I)Landroid/view/View;

    move-result-object v5

    check-cast v5, Landroid/widget/LinearLayout;

    iput-object v5, p0, Lcom/konka/systemsetting/system/SystemInputSettings;->itemInputsContainer:Landroid/widget/LinearLayout;

    iget-object v5, p0, Lcom/konka/systemsetting/system/SystemInputSettings;->list:Ljava/util/List;

    if-nez v5, :cond_0

    const-string v5, "#####-Input-Method-Error: Not any input method found!-#####"

    invoke-static {v5}, Lcom/konka/debuginfo/logPrint;->Debug(Ljava/lang/String;)V

    :goto_0
    return-void

    :cond_0
    const/4 v1, 0x0

    :goto_1
    iget-object v5, p0, Lcom/konka/systemsetting/system/SystemInputSettings;->list:Ljava/util/List;

    invoke-interface {v5}, Ljava/util/List;->size()I

    move-result v5

    if-lt v1, v5, :cond_1

    iget-object v5, p0, Lcom/konka/systemsetting/system/SystemInputSettings;->itemCurrInput:Landroid/widget/LinearLayout;

    new-instance v6, Lcom/konka/systemsetting/system/SystemInputSettings$onClickEvent;

    invoke-direct {v6, p0, v8}, Lcom/konka/systemsetting/system/SystemInputSettings$onClickEvent;-><init>(Lcom/konka/systemsetting/system/SystemInputSettings;Lcom/konka/systemsetting/system/SystemInputSettings$onClickEvent;)V

    invoke-virtual {v5, v6}, Landroid/widget/LinearLayout;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    goto :goto_0

    :cond_1
    iget-object v5, p0, Lcom/konka/systemsetting/system/SystemInputSettings;->mContext:Lcom/konka/systemsetting/MainActivity;

    const-string v6, "layout_inflater"

    invoke-virtual {v5, v6}, Lcom/konka/systemsetting/MainActivity;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Landroid/view/LayoutInflater;

    const v5, 0x7f030005    # com.konka.systemsetting.R.layout.input_item

    invoke-virtual {v2, v5, v8}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/LinearLayout;

    invoke-virtual {v0, v9}, Landroid/widget/LinearLayout;->getChildAt(I)Landroid/view/View;

    move-result-object v3

    check-cast v3, Landroid/widget/LinearLayout;

    invoke-virtual {v3, v9}, Landroid/widget/LinearLayout;->getChildAt(I)Landroid/view/View;

    move-result-object v4

    check-cast v4, Landroid/widget/TextView;

    iget-object v5, p0, Lcom/konka/systemsetting/system/SystemInputSettings;->list:Ljava/util/List;

    invoke-interface {v5, v1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v5

    check-cast v5, Landroid/view/inputmethod/InputMethodInfo;

    invoke-direct {p0, v5}, Lcom/konka/systemsetting/system/SystemInputSettings;->getInputAppName(Landroid/view/inputmethod/InputMethodInfo;)Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v4, v5}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    invoke-virtual {v3, v7}, Landroid/widget/LinearLayout;->setClickable(Z)V

    invoke-virtual {v3, v7}, Landroid/widget/LinearLayout;->setFocusable(Z)V

    invoke-virtual {v3, v7}, Landroid/widget/LinearLayout;->setFocusableInTouchMode(Z)V

    new-instance v5, Lcom/konka/systemsetting/system/SystemInputSettings$onFocusChangeEvent;

    invoke-direct {v5, p0, v8}, Lcom/konka/systemsetting/system/SystemInputSettings$onFocusChangeEvent;-><init>(Lcom/konka/systemsetting/system/SystemInputSettings;Lcom/konka/systemsetting/system/SystemInputSettings$onFocusChangeEvent;)V

    invoke-virtual {v3, v5}, Landroid/widget/LinearLayout;->setOnFocusChangeListener(Landroid/view/View$OnFocusChangeListener;)V

    invoke-virtual {v3, v11}, Landroid/widget/LinearLayout;->setNextFocusLeftId(I)V

    new-instance v5, Lcom/konka/systemsetting/system/SystemInputSettings$onInputSettingItemClickEvent;

    invoke-direct {v5, p0, v8}, Lcom/konka/systemsetting/system/SystemInputSettings$onInputSettingItemClickEvent;-><init>(Lcom/konka/systemsetting/system/SystemInputSettings;Lcom/konka/systemsetting/system/SystemInputSettings$onInputSettingItemClickEvent;)V

    invoke-virtual {v3, v5}, Landroid/widget/LinearLayout;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    new-instance v5, Lcom/konka/systemsetting/system/SystemInputSettings$onInputSettingItemKeyEvent;

    invoke-direct {v5, p0, v8}, Lcom/konka/systemsetting/system/SystemInputSettings$onInputSettingItemKeyEvent;-><init>(Lcom/konka/systemsetting/system/SystemInputSettings;Lcom/konka/systemsetting/system/SystemInputSettings$onInputSettingItemKeyEvent;)V

    invoke-virtual {v3, v5}, Landroid/widget/LinearLayout;->setOnKeyListener(Landroid/view/View$OnKeyListener;)V

    iget-object v5, p0, Lcom/konka/systemsetting/system/SystemInputSettings;->itemInputsContainer:Landroid/widget/LinearLayout;

    invoke-virtual {v5, v0}, Landroid/widget/LinearLayout;->addView(Landroid/view/View;)V

    add-int/lit8 v1, v1, 0x1

    goto :goto_1
.end method

.method private getInputAppName(Landroid/view/inputmethod/InputMethodInfo;)Ljava/lang/String;
    .locals 2
    .param p1    # Landroid/view/inputmethod/InputMethodInfo;

    if-nez p1, :cond_0

    const-string v0, ""

    :goto_0
    return-object v0

    :cond_0
    invoke-virtual {p1}, Landroid/view/inputmethod/InputMethodInfo;->getPackageName()Ljava/lang/String;

    move-result-object v0

    iget-object v1, p0, Lcom/konka/systemsetting/system/SystemInputSettings;->mContext:Lcom/konka/systemsetting/MainActivity;

    invoke-virtual {v1}, Lcom/konka/systemsetting/MainActivity;->getPackageManager()Landroid/content/pm/PackageManager;

    move-result-object v1

    invoke-virtual {p1, v1}, Landroid/view/inputmethod/InputMethodInfo;->loadLabel(Landroid/content/pm/PackageManager;)Ljava/lang/CharSequence;

    move-result-object v1

    invoke-interface {v1}, Ljava/lang/CharSequence;->toString()Ljava/lang/String;

    move-result-object v0

    goto :goto_0
.end method

.method private getInputAppName(Ljava/lang/String;)Ljava/lang/String;
    .locals 6
    .param p1    # Ljava/lang/String;

    const-string v2, ""

    iget-object v4, p0, Lcom/konka/systemsetting/system/SystemInputSettings;->list:Ljava/util/List;

    invoke-interface {v4}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v4

    :cond_0
    invoke-interface {v4}, Ljava/util/Iterator;->hasNext()Z

    move-result v5

    if-nez v5, :cond_1

    move-object v3, v2

    :goto_0
    return-object v3

    :cond_1
    invoke-interface {v4}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/view/inputmethod/InputMethodInfo;

    invoke-virtual {v0}, Landroid/view/inputmethod/InputMethodInfo;->getId()Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v5, p1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v5

    if-eqz v5, :cond_0

    move-object v1, v0

    invoke-direct {p0, v1}, Lcom/konka/systemsetting/system/SystemInputSettings;->getInputAppName(Landroid/view/inputmethod/InputMethodInfo;)Ljava/lang/String;

    move-result-object v2

    move-object v3, v2

    goto :goto_0
.end method

.method private showInputPickerDialog()V
    .locals 6

    new-instance v0, Landroid/app/AlertDialog$Builder;

    iget-object v4, p0, Lcom/konka/systemsetting/system/SystemInputSettings;->mContext:Lcom/konka/systemsetting/MainActivity;

    invoke-direct {v0, v4}, Landroid/app/AlertDialog$Builder;-><init>(Landroid/content/Context;)V

    const v4, 0x7f06004d    # com.konka.systemsetting.R.string.str_input_settings

    invoke-virtual {v0, v4}, Landroid/app/AlertDialog$Builder;->setTitle(I)Landroid/app/AlertDialog$Builder;

    const v4, 0x7f020022    # com.konka.systemsetting.R.drawable.syssettingfirsticonpersonalunfocus

    invoke-virtual {v0, v4}, Landroid/app/AlertDialog$Builder;->setIcon(I)Landroid/app/AlertDialog$Builder;

    const/4 v3, 0x0

    const/4 v2, 0x0

    :goto_0
    iget-object v4, p0, Lcom/konka/systemsetting/system/SystemInputSettings;->strInputAppNames:[Ljava/lang/String;

    array-length v4, v4

    if-lt v2, v4, :cond_0

    new-instance v1, Lcom/konka/systemsetting/system/SystemInputSettings$1;

    invoke-direct {v1, p0}, Lcom/konka/systemsetting/system/SystemInputSettings$1;-><init>(Lcom/konka/systemsetting/system/SystemInputSettings;)V

    iget-object v4, p0, Lcom/konka/systemsetting/system/SystemInputSettings;->strInputAppNames:[Ljava/lang/String;

    invoke-virtual {v0, v4, v3, v1}, Landroid/app/AlertDialog$Builder;->setSingleChoiceItems([Ljava/lang/CharSequence;ILandroid/content/DialogInterface$OnClickListener;)Landroid/app/AlertDialog$Builder;

    invoke-virtual {v0}, Landroid/app/AlertDialog$Builder;->show()Landroid/app/AlertDialog;

    return-void

    :cond_0
    iget-object v4, p0, Lcom/konka/systemsetting/system/SystemInputSettings;->strInputAppNames:[Ljava/lang/String;

    aget-object v4, v4, v2

    iget-object v5, p0, Lcom/konka/systemsetting/system/SystemInputSettings;->tvCurrInput:Landroid/widget/TextView;

    invoke-virtual {v5}, Landroid/widget/TextView;->getText()Ljava/lang/CharSequence;

    move-result-object v5

    invoke-interface {v5}, Ljava/lang/CharSequence;->toString()Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v4, v5}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v4

    if-eqz v4, :cond_1

    move v3, v2

    :cond_1
    add-int/lit8 v2, v2, 0x1

    goto :goto_0
.end method
