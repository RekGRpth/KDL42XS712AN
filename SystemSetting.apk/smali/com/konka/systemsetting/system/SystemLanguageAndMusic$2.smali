.class Lcom/konka/systemsetting/system/SystemLanguageAndMusic$2;
.super Ljava/lang/Object;
.source "SystemLanguageAndMusic.java"

# interfaces
.implements Landroid/view/View$OnClickListener;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/konka/systemsetting/system/SystemLanguageAndMusic;->SetOnClickListener()V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/konka/systemsetting/system/SystemLanguageAndMusic;


# direct methods
.method constructor <init>(Lcom/konka/systemsetting/system/SystemLanguageAndMusic;)V
    .locals 0

    iput-object p1, p0, Lcom/konka/systemsetting/system/SystemLanguageAndMusic$2;->this$0:Lcom/konka/systemsetting/system/SystemLanguageAndMusic;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onClick(Landroid/view/View;)V
    .locals 2
    .param p1    # Landroid/view/View;

    iget-object v0, p0, Lcom/konka/systemsetting/system/SystemLanguageAndMusic$2;->this$0:Lcom/konka/systemsetting/system/SystemLanguageAndMusic;

    invoke-virtual {p1}, Landroid/view/View;->getId()I

    move-result v1

    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/konka/systemsetting/system/SystemLanguageAndMusic;->access$2(Lcom/konka/systemsetting/system/SystemLanguageAndMusic;Ljava/lang/Integer;)V

    iget-object v0, p0, Lcom/konka/systemsetting/system/SystemLanguageAndMusic$2;->this$0:Lcom/konka/systemsetting/system/SystemLanguageAndMusic;

    # getter for: Lcom/konka/systemsetting/system/SystemLanguageAndMusic;->m_currItemId:Ljava/lang/Integer;
    invoke-static {v0}, Lcom/konka/systemsetting/system/SystemLanguageAndMusic;->access$3(Lcom/konka/systemsetting/system/SystemLanguageAndMusic;)Ljava/lang/Integer;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/Integer;->intValue()I

    move-result v0

    packed-switch v0, :pswitch_data_0

    :goto_0
    :pswitch_0
    return-void

    :pswitch_1
    iget-object v0, p0, Lcom/konka/systemsetting/system/SystemLanguageAndMusic$2;->this$0:Lcom/konka/systemsetting/system/SystemLanguageAndMusic;

    # invokes: Lcom/konka/systemsetting/system/SystemLanguageAndMusic;->showLanguageSettingDialog()V
    invoke-static {v0}, Lcom/konka/systemsetting/system/SystemLanguageAndMusic;->access$4(Lcom/konka/systemsetting/system/SystemLanguageAndMusic;)V

    :goto_1
    iget-object v0, p0, Lcom/konka/systemsetting/system/SystemLanguageAndMusic$2;->this$0:Lcom/konka/systemsetting/system/SystemLanguageAndMusic;

    # invokes: Lcom/konka/systemsetting/system/SystemLanguageAndMusic;->updateState()V
    invoke-static {v0}, Lcom/konka/systemsetting/system/SystemLanguageAndMusic;->access$7(Lcom/konka/systemsetting/system/SystemLanguageAndMusic;)V

    goto :goto_0

    :pswitch_2
    iget-object v0, p0, Lcom/konka/systemsetting/system/SystemLanguageAndMusic$2;->this$0:Lcom/konka/systemsetting/system/SystemLanguageAndMusic;

    # invokes: Lcom/konka/systemsetting/system/SystemLanguageAndMusic;->setMicStatus()V
    invoke-static {v0}, Lcom/konka/systemsetting/system/SystemLanguageAndMusic;->access$5(Lcom/konka/systemsetting/system/SystemLanguageAndMusic;)V

    goto :goto_1

    :pswitch_3
    iget-object v0, p0, Lcom/konka/systemsetting/system/SystemLanguageAndMusic$2;->this$0:Lcom/konka/systemsetting/system/SystemLanguageAndMusic;

    # invokes: Lcom/konka/systemsetting/system/SystemLanguageAndMusic;->showMusicSettingDialog()V
    invoke-static {v0}, Lcom/konka/systemsetting/system/SystemLanguageAndMusic;->access$6(Lcom/konka/systemsetting/system/SystemLanguageAndMusic;)V

    goto :goto_1

    :pswitch_data_0
    .packed-switch 0x7f090092
        :pswitch_1    # com.konka.systemsetting.R.id.sys_system_item_language
        :pswitch_2    # com.konka.systemsetting.R.id.sys_system_item_mic
        :pswitch_0    # com.konka.systemsetting.R.id.sys_system_micswitch
        :pswitch_3    # com.konka.systemsetting.R.id.sys_system_item_music
    .end packed-switch
.end method
