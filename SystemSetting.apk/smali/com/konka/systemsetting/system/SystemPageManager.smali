.class public Lcom/konka/systemsetting/system/SystemPageManager;
.super Ljava/lang/Object;
.source "SystemPageManager.java"


# instance fields
.field protected final PAGE_FACTORYRESET:I

.field protected final PAGE_INPUTSETTINGS:I

.field protected final PAGE_LANGANDMUSIC:I

.field protected final PAGE_SOFTUPGRADE:I

.field protected final PAGE_SYSTEMINFO:I

.field protected final STATE_CURRENT_FOCUSED:I

.field protected final STATE_LAST_FOCUSED:I

.field protected final STATE_NO_FOCUSE:I

.field private mContext:Lcom/konka/systemsetting/MainActivity;

.field private m_disclaimer:Lcom/konka/systemsetting/system/SystemDisclaimer;

.field private m_factoryReset:Lcom/konka/systemsetting/system/SystemFactoryReset;

.field private m_iCurPage:I

.field private m_iLastFocusPage:I

.field private m_inputSettings:Lcom/konka/systemsetting/system/SystemInputSettings;

.field private m_langAndMusic:Lcom/konka/systemsetting/system/SystemLanguageAndMusic;

.field private m_softUpgrade:Lcom/konka/systemsetting/system/SystemSoftUpgrade;

.field private m_systemInfo:Lcom/konka/systemsetting/system/SystemInfo;

.field private m_viewHolder:Lcom/konka/systemsetting/ViewHolder;


# direct methods
.method public constructor <init>(Lcom/konka/systemsetting/MainActivity;I)V
    .locals 3
    .param p1    # Lcom/konka/systemsetting/MainActivity;
    .param p2    # I

    const/4 v2, 0x2

    const/4 v0, 0x1

    const/4 v1, 0x0

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    iput v1, p0, Lcom/konka/systemsetting/system/SystemPageManager;->STATE_CURRENT_FOCUSED:I

    iput v0, p0, Lcom/konka/systemsetting/system/SystemPageManager;->STATE_LAST_FOCUSED:I

    iput v2, p0, Lcom/konka/systemsetting/system/SystemPageManager;->STATE_NO_FOCUSE:I

    iput v1, p0, Lcom/konka/systemsetting/system/SystemPageManager;->PAGE_LANGANDMUSIC:I

    iput v0, p0, Lcom/konka/systemsetting/system/SystemPageManager;->PAGE_INPUTSETTINGS:I

    iput v2, p0, Lcom/konka/systemsetting/system/SystemPageManager;->PAGE_SYSTEMINFO:I

    const/4 v0, 0x3

    iput v0, p0, Lcom/konka/systemsetting/system/SystemPageManager;->PAGE_SOFTUPGRADE:I

    const/4 v0, 0x4

    iput v0, p0, Lcom/konka/systemsetting/system/SystemPageManager;->PAGE_FACTORYRESET:I

    iput v1, p0, Lcom/konka/systemsetting/system/SystemPageManager;->m_iCurPage:I

    const/4 v0, -0x1

    iput v0, p0, Lcom/konka/systemsetting/system/SystemPageManager;->m_iLastFocusPage:I

    iput-object p1, p0, Lcom/konka/systemsetting/system/SystemPageManager;->mContext:Lcom/konka/systemsetting/MainActivity;

    new-instance v0, Lcom/konka/systemsetting/ViewHolder;

    iget-object v1, p0, Lcom/konka/systemsetting/system/SystemPageManager;->mContext:Lcom/konka/systemsetting/MainActivity;

    invoke-direct {v0, v1}, Lcom/konka/systemsetting/ViewHolder;-><init>(Lcom/konka/systemsetting/MainActivity;)V

    iput-object v0, p0, Lcom/konka/systemsetting/system/SystemPageManager;->m_viewHolder:Lcom/konka/systemsetting/ViewHolder;

    iget-object v0, p0, Lcom/konka/systemsetting/system/SystemPageManager;->m_viewHolder:Lcom/konka/systemsetting/ViewHolder;

    invoke-virtual {v0}, Lcom/konka/systemsetting/ViewHolder;->findViewForPages()V

    iput p2, p0, Lcom/konka/systemsetting/system/SystemPageManager;->m_iCurPage:I

    iget v0, p0, Lcom/konka/systemsetting/system/SystemPageManager;->m_iCurPage:I

    invoke-virtual {p0, v0}, Lcom/konka/systemsetting/system/SystemPageManager;->showPage(I)V

    invoke-direct {p0}, Lcom/konka/systemsetting/system/SystemPageManager;->SetOnFocusChangeListener()V

    return-void
.end method

.method private SetOnFocusChangeListener()V
    .locals 2

    new-instance v0, Lcom/konka/systemsetting/system/SystemPageManager$1;

    invoke-direct {v0, p0}, Lcom/konka/systemsetting/system/SystemPageManager$1;-><init>(Lcom/konka/systemsetting/system/SystemPageManager;)V

    iget-object v1, p0, Lcom/konka/systemsetting/system/SystemPageManager;->m_viewHolder:Lcom/konka/systemsetting/ViewHolder;

    iget-object v1, v1, Lcom/konka/systemsetting/ViewHolder;->linearlayout_tabLanguageAndMusic:Landroid/widget/LinearLayout;

    invoke-virtual {v1, v0}, Landroid/widget/LinearLayout;->setOnFocusChangeListener(Landroid/view/View$OnFocusChangeListener;)V

    iget-object v1, p0, Lcom/konka/systemsetting/system/SystemPageManager;->m_viewHolder:Lcom/konka/systemsetting/ViewHolder;

    iget-object v1, v1, Lcom/konka/systemsetting/ViewHolder;->linearlayout_tabInputSettings:Landroid/widget/LinearLayout;

    invoke-virtual {v1, v0}, Landroid/widget/LinearLayout;->setOnFocusChangeListener(Landroid/view/View$OnFocusChangeListener;)V

    iget-object v1, p0, Lcom/konka/systemsetting/system/SystemPageManager;->m_viewHolder:Lcom/konka/systemsetting/ViewHolder;

    iget-object v1, v1, Lcom/konka/systemsetting/ViewHolder;->linearlayout_tabSystemInfo:Landroid/widget/LinearLayout;

    invoke-virtual {v1, v0}, Landroid/widget/LinearLayout;->setOnFocusChangeListener(Landroid/view/View$OnFocusChangeListener;)V

    iget-object v1, p0, Lcom/konka/systemsetting/system/SystemPageManager;->m_viewHolder:Lcom/konka/systemsetting/ViewHolder;

    iget-object v1, v1, Lcom/konka/systemsetting/ViewHolder;->linearlayout_tabSoftUpgrade:Landroid/widget/LinearLayout;

    invoke-virtual {v1, v0}, Landroid/widget/LinearLayout;->setOnFocusChangeListener(Landroid/view/View$OnFocusChangeListener;)V

    iget-object v1, p0, Lcom/konka/systemsetting/system/SystemPageManager;->m_viewHolder:Lcom/konka/systemsetting/ViewHolder;

    iget-object v1, v1, Lcom/konka/systemsetting/ViewHolder;->linearlayout_tabFactoryReset:Landroid/widget/LinearLayout;

    invoke-virtual {v1, v0}, Landroid/widget/LinearLayout;->setOnFocusChangeListener(Landroid/view/View$OnFocusChangeListener;)V

    return-void
.end method

.method static synthetic access$0(Lcom/konka/systemsetting/system/SystemPageManager;)I
    .locals 1

    iget v0, p0, Lcom/konka/systemsetting/system/SystemPageManager;->m_iCurPage:I

    return v0
.end method

.method static synthetic access$1(Lcom/konka/systemsetting/system/SystemPageManager;I)V
    .locals 0

    iput p1, p0, Lcom/konka/systemsetting/system/SystemPageManager;->m_iLastFocusPage:I

    return-void
.end method

.method static synthetic access$2(Lcom/konka/systemsetting/system/SystemPageManager;I)V
    .locals 0

    iput p1, p0, Lcom/konka/systemsetting/system/SystemPageManager;->m_iCurPage:I

    return-void
.end method

.method static synthetic access$3(Lcom/konka/systemsetting/system/SystemPageManager;)I
    .locals 1

    iget v0, p0, Lcom/konka/systemsetting/system/SystemPageManager;->m_iLastFocusPage:I

    return v0
.end method

.method static synthetic access$4(Lcom/konka/systemsetting/system/SystemPageManager;II)V
    .locals 0

    invoke-direct {p0, p1, p2}, Lcom/konka/systemsetting/system/SystemPageManager;->changeItemStatus(II)V

    return-void
.end method

.method private changeItemStatus(II)V
    .locals 6
    .param p1    # I
    .param p2    # I

    const/4 v5, 0x0

    const v4, -0x87e2

    const/4 v3, -0x1

    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "change item statusid="

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, " iState="

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v1}, Lcom/konka/debuginfo/logPrint;->Debug(Ljava/lang/String;)V

    packed-switch p1, :pswitch_data_0

    const-string v1, "The item id is wrong!"

    invoke-static {v1}, Lcom/konka/debuginfo/logPrint;->Warning(Ljava/lang/String;)V

    :goto_0
    return-void

    :pswitch_0
    iget-object v1, p0, Lcom/konka/systemsetting/system/SystemPageManager;->m_viewHolder:Lcom/konka/systemsetting/ViewHolder;

    iget-object v1, v1, Lcom/konka/systemsetting/ViewHolder;->linearlayout_tabLanguageAndMusic:Landroid/widget/LinearLayout;

    invoke-virtual {v1, v5}, Landroid/widget/LinearLayout;->getChildAt(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    packed-switch p2, :pswitch_data_1

    const-string v1, "The item state is wrong!"

    invoke-static {v1}, Lcom/konka/debuginfo/logPrint;->Warning(Ljava/lang/String;)V

    goto :goto_0

    :pswitch_1
    invoke-virtual {v0, v3}, Landroid/widget/TextView;->setTextColor(I)V

    goto :goto_0

    :pswitch_2
    invoke-virtual {v0, v4}, Landroid/widget/TextView;->setTextColor(I)V

    goto :goto_0

    :pswitch_3
    invoke-virtual {v0, v3}, Landroid/widget/TextView;->setTextColor(I)V

    goto :goto_0

    :pswitch_4
    iget-object v1, p0, Lcom/konka/systemsetting/system/SystemPageManager;->m_viewHolder:Lcom/konka/systemsetting/ViewHolder;

    iget-object v1, v1, Lcom/konka/systemsetting/ViewHolder;->linearlayout_tabInputSettings:Landroid/widget/LinearLayout;

    invoke-virtual {v1, v5}, Landroid/widget/LinearLayout;->getChildAt(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    packed-switch p2, :pswitch_data_2

    const-string v1, "The item state is wrong!"

    invoke-static {v1}, Lcom/konka/debuginfo/logPrint;->Warning(Ljava/lang/String;)V

    goto :goto_0

    :pswitch_5
    invoke-virtual {v0, v3}, Landroid/widget/TextView;->setTextColor(I)V

    goto :goto_0

    :pswitch_6
    invoke-virtual {v0, v4}, Landroid/widget/TextView;->setTextColor(I)V

    goto :goto_0

    :pswitch_7
    invoke-virtual {v0, v3}, Landroid/widget/TextView;->setTextColor(I)V

    goto :goto_0

    :pswitch_8
    iget-object v1, p0, Lcom/konka/systemsetting/system/SystemPageManager;->m_viewHolder:Lcom/konka/systemsetting/ViewHolder;

    iget-object v1, v1, Lcom/konka/systemsetting/ViewHolder;->linearlayout_tabSystemInfo:Landroid/widget/LinearLayout;

    invoke-virtual {v1, v5}, Landroid/widget/LinearLayout;->getChildAt(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    packed-switch p2, :pswitch_data_3

    const-string v1, "The item state is wrong!"

    invoke-static {v1}, Lcom/konka/debuginfo/logPrint;->Warning(Ljava/lang/String;)V

    goto :goto_0

    :pswitch_9
    invoke-virtual {v0, v3}, Landroid/widget/TextView;->setTextColor(I)V

    goto :goto_0

    :pswitch_a
    invoke-virtual {v0, v4}, Landroid/widget/TextView;->setTextColor(I)V

    goto :goto_0

    :pswitch_b
    invoke-virtual {v0, v3}, Landroid/widget/TextView;->setTextColor(I)V

    goto :goto_0

    :pswitch_c
    iget-object v1, p0, Lcom/konka/systemsetting/system/SystemPageManager;->m_viewHolder:Lcom/konka/systemsetting/ViewHolder;

    iget-object v1, v1, Lcom/konka/systemsetting/ViewHolder;->linearlayout_tabSoftUpgrade:Landroid/widget/LinearLayout;

    invoke-virtual {v1, v5}, Landroid/widget/LinearLayout;->getChildAt(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    packed-switch p2, :pswitch_data_4

    const-string v1, "The item state is wrong!"

    invoke-static {v1}, Lcom/konka/debuginfo/logPrint;->Warning(Ljava/lang/String;)V

    goto :goto_0

    :pswitch_d
    invoke-virtual {v0, v3}, Landroid/widget/TextView;->setTextColor(I)V

    goto :goto_0

    :pswitch_e
    invoke-virtual {v0, v4}, Landroid/widget/TextView;->setTextColor(I)V

    goto :goto_0

    :pswitch_f
    invoke-virtual {v0, v3}, Landroid/widget/TextView;->setTextColor(I)V

    goto :goto_0

    :pswitch_10
    iget-object v1, p0, Lcom/konka/systemsetting/system/SystemPageManager;->m_viewHolder:Lcom/konka/systemsetting/ViewHolder;

    iget-object v1, v1, Lcom/konka/systemsetting/ViewHolder;->linearlayout_tabFactoryReset:Landroid/widget/LinearLayout;

    invoke-virtual {v1, v5}, Landroid/widget/LinearLayout;->getChildAt(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    packed-switch p2, :pswitch_data_5

    const-string v1, "The item state is wrong!"

    invoke-static {v1}, Lcom/konka/debuginfo/logPrint;->Warning(Ljava/lang/String;)V

    goto/16 :goto_0

    :pswitch_11
    invoke-virtual {v0, v3}, Landroid/widget/TextView;->setTextColor(I)V

    goto/16 :goto_0

    :pswitch_12
    invoke-virtual {v0, v4}, Landroid/widget/TextView;->setTextColor(I)V

    goto/16 :goto_0

    :pswitch_13
    invoke-virtual {v0, v3}, Landroid/widget/TextView;->setTextColor(I)V

    goto/16 :goto_0

    nop

    :pswitch_data_0
    .packed-switch 0x0
        :pswitch_0
        :pswitch_4
        :pswitch_8
        :pswitch_c
        :pswitch_10
    .end packed-switch

    :pswitch_data_1
    .packed-switch 0x0
        :pswitch_1
        :pswitch_2
        :pswitch_3
    .end packed-switch

    :pswitch_data_2
    .packed-switch 0x0
        :pswitch_5
        :pswitch_6
        :pswitch_7
    .end packed-switch

    :pswitch_data_3
    .packed-switch 0x0
        :pswitch_9
        :pswitch_a
        :pswitch_b
    .end packed-switch

    :pswitch_data_4
    .packed-switch 0x0
        :pswitch_d
        :pswitch_e
        :pswitch_f
    .end packed-switch

    :pswitch_data_5
    .packed-switch 0x0
        :pswitch_11
        :pswitch_12
        :pswitch_13
    .end packed-switch
.end method


# virtual methods
.method public resetItemStatus()V
    .locals 2

    const/4 v1, 0x2

    iget v0, p0, Lcom/konka/systemsetting/system/SystemPageManager;->m_iCurPage:I

    invoke-direct {p0, v0, v1}, Lcom/konka/systemsetting/system/SystemPageManager;->changeItemStatus(II)V

    iget v0, p0, Lcom/konka/systemsetting/system/SystemPageManager;->m_iLastFocusPage:I

    invoke-direct {p0, v0, v1}, Lcom/konka/systemsetting/system/SystemPageManager;->changeItemStatus(II)V

    return-void
.end method

.method protected setLastFocus(II)V
    .locals 0
    .param p1    # I
    .param p2    # I

    invoke-direct {p0, p1, p2}, Lcom/konka/systemsetting/system/SystemPageManager;->changeItemStatus(II)V

    return-void
.end method

.method public showPage(I)V
    .locals 2
    .param p1    # I

    iget-object v0, p0, Lcom/konka/systemsetting/system/SystemPageManager;->m_viewHolder:Lcom/konka/systemsetting/ViewHolder;

    iget-object v0, v0, Lcom/konka/systemsetting/ViewHolder;->viewflipper_systempage:Landroid/widget/ViewFlipper;

    invoke-virtual {v0, p1}, Landroid/widget/ViewFlipper;->setDisplayedChild(I)V

    packed-switch p1, :pswitch_data_0

    :cond_0
    :goto_0
    return-void

    :pswitch_0
    iget-object v0, p0, Lcom/konka/systemsetting/system/SystemPageManager;->m_langAndMusic:Lcom/konka/systemsetting/system/SystemLanguageAndMusic;

    if-nez v0, :cond_0

    new-instance v0, Lcom/konka/systemsetting/system/SystemLanguageAndMusic;

    iget-object v1, p0, Lcom/konka/systemsetting/system/SystemPageManager;->mContext:Lcom/konka/systemsetting/MainActivity;

    invoke-direct {v0, v1}, Lcom/konka/systemsetting/system/SystemLanguageAndMusic;-><init>(Lcom/konka/systemsetting/MainActivity;)V

    iput-object v0, p0, Lcom/konka/systemsetting/system/SystemPageManager;->m_langAndMusic:Lcom/konka/systemsetting/system/SystemLanguageAndMusic;

    goto :goto_0

    :pswitch_1
    iget-object v0, p0, Lcom/konka/systemsetting/system/SystemPageManager;->m_inputSettings:Lcom/konka/systemsetting/system/SystemInputSettings;

    if-nez v0, :cond_0

    new-instance v0, Lcom/konka/systemsetting/system/SystemInputSettings;

    iget-object v1, p0, Lcom/konka/systemsetting/system/SystemPageManager;->mContext:Lcom/konka/systemsetting/MainActivity;

    invoke-direct {v0, v1}, Lcom/konka/systemsetting/system/SystemInputSettings;-><init>(Lcom/konka/systemsetting/MainActivity;)V

    iput-object v0, p0, Lcom/konka/systemsetting/system/SystemPageManager;->m_inputSettings:Lcom/konka/systemsetting/system/SystemInputSettings;

    iget-object v0, p0, Lcom/konka/systemsetting/system/SystemPageManager;->m_viewHolder:Lcom/konka/systemsetting/ViewHolder;

    iget-object v0, v0, Lcom/konka/systemsetting/ViewHolder;->linearlayout_tabInputSettings:Landroid/widget/LinearLayout;

    invoke-virtual {v0}, Landroid/widget/LinearLayout;->requestFocus()Z

    goto :goto_0

    :pswitch_2
    iget-object v0, p0, Lcom/konka/systemsetting/system/SystemPageManager;->m_systemInfo:Lcom/konka/systemsetting/system/SystemInfo;

    if-nez v0, :cond_0

    new-instance v0, Lcom/konka/systemsetting/system/SystemInfo;

    iget-object v1, p0, Lcom/konka/systemsetting/system/SystemPageManager;->mContext:Lcom/konka/systemsetting/MainActivity;

    invoke-direct {v0, v1}, Lcom/konka/systemsetting/system/SystemInfo;-><init>(Lcom/konka/systemsetting/MainActivity;)V

    iput-object v0, p0, Lcom/konka/systemsetting/system/SystemPageManager;->m_systemInfo:Lcom/konka/systemsetting/system/SystemInfo;

    iget-object v0, p0, Lcom/konka/systemsetting/system/SystemPageManager;->m_viewHolder:Lcom/konka/systemsetting/ViewHolder;

    iget-object v0, v0, Lcom/konka/systemsetting/ViewHolder;->linearlayout_tabSystemInfo:Landroid/widget/LinearLayout;

    invoke-virtual {v0}, Landroid/widget/LinearLayout;->requestFocus()Z

    goto :goto_0

    :pswitch_3
    iget-object v0, p0, Lcom/konka/systemsetting/system/SystemPageManager;->m_softUpgrade:Lcom/konka/systemsetting/system/SystemSoftUpgrade;

    if-nez v0, :cond_0

    new-instance v0, Lcom/konka/systemsetting/system/SystemSoftUpgrade;

    iget-object v1, p0, Lcom/konka/systemsetting/system/SystemPageManager;->mContext:Lcom/konka/systemsetting/MainActivity;

    invoke-direct {v0, v1}, Lcom/konka/systemsetting/system/SystemSoftUpgrade;-><init>(Lcom/konka/systemsetting/MainActivity;)V

    iput-object v0, p0, Lcom/konka/systemsetting/system/SystemPageManager;->m_softUpgrade:Lcom/konka/systemsetting/system/SystemSoftUpgrade;

    iget-object v0, p0, Lcom/konka/systemsetting/system/SystemPageManager;->m_viewHolder:Lcom/konka/systemsetting/ViewHolder;

    iget-object v0, v0, Lcom/konka/systemsetting/ViewHolder;->linearlayout_tabSoftUpgrade:Landroid/widget/LinearLayout;

    invoke-virtual {v0}, Landroid/widget/LinearLayout;->requestFocus()Z

    goto :goto_0

    :pswitch_4
    iget-object v0, p0, Lcom/konka/systemsetting/system/SystemPageManager;->m_factoryReset:Lcom/konka/systemsetting/system/SystemFactoryReset;

    if-nez v0, :cond_0

    new-instance v0, Lcom/konka/systemsetting/system/SystemFactoryReset;

    iget-object v1, p0, Lcom/konka/systemsetting/system/SystemPageManager;->mContext:Lcom/konka/systemsetting/MainActivity;

    invoke-direct {v0, v1}, Lcom/konka/systemsetting/system/SystemFactoryReset;-><init>(Lcom/konka/systemsetting/MainActivity;)V

    iput-object v0, p0, Lcom/konka/systemsetting/system/SystemPageManager;->m_factoryReset:Lcom/konka/systemsetting/system/SystemFactoryReset;

    iget-object v0, p0, Lcom/konka/systemsetting/system/SystemPageManager;->m_viewHolder:Lcom/konka/systemsetting/ViewHolder;

    iget-object v0, v0, Lcom/konka/systemsetting/ViewHolder;->linearlayout_tabFactoryReset:Landroid/widget/LinearLayout;

    invoke-virtual {v0}, Landroid/widget/LinearLayout;->requestFocus()Z

    goto :goto_0

    nop

    :pswitch_data_0
    .packed-switch 0x0
        :pswitch_0
        :pswitch_1
        :pswitch_2
        :pswitch_3
        :pswitch_4
    .end packed-switch
.end method
