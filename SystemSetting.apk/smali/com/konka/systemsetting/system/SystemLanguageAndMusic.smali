.class public Lcom/konka/systemsetting/system/SystemLanguageAndMusic;
.super Ljava/lang/Object;
.source "SystemLanguageAndMusic.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/konka/systemsetting/system/SystemLanguageAndMusic$LanguageListAdapter;
    }
.end annotation


# static fields
.field protected static final Arabic:I = 0x1

.field protected static final Chinese:I = 0x9

.field protected static final ENGLISH:I = 0x0

.field protected static final Franch:I = 0x5

.field protected static final Hebrew:I = 0x8

.field protected static final Indonesia:I = 0x6

.field protected static final Kurdish:I = 0x3

.field protected static final MUSIC_1:I = 0x1

.field protected static final MUSIC_2:I = 0x2

.field protected static final MUSIC_OFF:I = 0x0

.field protected static final Persian:I = 0x2

.field protected static final Russia:I = 0x4

.field protected static final Thai:I = 0xa

.field protected static final Turkish:I = 0x7

.field private static m_iCurrMusicIndex:I


# instance fields
.field private language_values:[Ljava/lang/CharSequence;

.field private languages:[Lcom/konka/kkimplements/tv/LanguageItem;

.field protected linearlayout_itemBootingMusic:Landroid/widget/LinearLayout;

.field protected linearlayout_itemCurrentLanguage:Landroid/widget/LinearLayout;

.field protected linearlayout_itemMic:Landroid/widget/LinearLayout;

.field private mContext:Lcom/konka/systemsetting/MainActivity;

.field private m_currItemId:Ljava/lang/Integer;

.field private m_iCurrLangIndex:I

.field private m_iCurrLangIndexInDatabase:I

.field private m_iPreLangIndex:I

.field private m_locale:Ljava/util/Locale;

.field private m_strLocales:[Ljava/lang/String;

.field private music_values:[Ljava/lang/CharSequence;

.field private settingDesk:Lcom/konka/kkinterface/tv/SettingDesk;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    const/4 v0, 0x0

    sput v0, Lcom/konka/systemsetting/system/SystemLanguageAndMusic;->m_iCurrMusicIndex:I

    return-void
.end method

.method public constructor <init>(Lcom/konka/systemsetting/MainActivity;)V
    .locals 5
    .param p1    # Lcom/konka/systemsetting/MainActivity;

    const/4 v4, 0x0

    const/4 v3, 0x0

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    iput-object v4, p0, Lcom/konka/systemsetting/system/SystemLanguageAndMusic;->language_values:[Ljava/lang/CharSequence;

    const/16 v0, 0xb

    new-array v0, v0, [Ljava/lang/String;

    const-string v1, "en_US"

    aput-object v1, v0, v3

    const/4 v1, 0x1

    const-string v2, "ar_EG"

    aput-object v2, v0, v1

    const/4 v1, 0x2

    const-string v2, "fa_IR"

    aput-object v2, v0, v1

    const/4 v1, 0x3

    const-string v2, "kd_KD"

    aput-object v2, v0, v1

    const/4 v1, 0x4

    const-string v2, "ru_RU"

    aput-object v2, v0, v1

    const/4 v1, 0x5

    const-string v2, "fr_FR"

    aput-object v2, v0, v1

    const/4 v1, 0x6

    const-string v2, "in_ID"

    aput-object v2, v0, v1

    const/4 v1, 0x7

    const-string v2, "tr_TR"

    aput-object v2, v0, v1

    const/16 v1, 0x8

    const-string v2, "iw_IL"

    aput-object v2, v0, v1

    const/16 v1, 0x9

    const-string v2, "zh_CN"

    aput-object v2, v0, v1

    const/16 v1, 0xa

    const-string v2, "th_TH"

    aput-object v2, v0, v1

    iput-object v0, p0, Lcom/konka/systemsetting/system/SystemLanguageAndMusic;->m_strLocales:[Ljava/lang/String;

    iput v3, p0, Lcom/konka/systemsetting/system/SystemLanguageAndMusic;->m_iCurrLangIndex:I

    iput v3, p0, Lcom/konka/systemsetting/system/SystemLanguageAndMusic;->m_iCurrLangIndexInDatabase:I

    iput v3, p0, Lcom/konka/systemsetting/system/SystemLanguageAndMusic;->m_iPreLangIndex:I

    iput-object v4, p0, Lcom/konka/systemsetting/system/SystemLanguageAndMusic;->music_values:[Ljava/lang/CharSequence;

    const v0, 0x7f090092    # com.konka.systemsetting.R.id.sys_system_item_language

    invoke-static {v0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v0

    iput-object v0, p0, Lcom/konka/systemsetting/system/SystemLanguageAndMusic;->m_currItemId:Ljava/lang/Integer;

    iput-object p1, p0, Lcom/konka/systemsetting/system/SystemLanguageAndMusic;->mContext:Lcom/konka/systemsetting/MainActivity;

    iget-object v0, p0, Lcom/konka/systemsetting/system/SystemLanguageAndMusic;->mContext:Lcom/konka/systemsetting/MainActivity;

    invoke-static {v0}, Lcom/konka/kkimplements/tv/LanguageUtil;->getCustomeLanguageList(Landroid/content/Context;)[Lcom/konka/kkimplements/tv/LanguageItem;

    move-result-object v0

    iput-object v0, p0, Lcom/konka/systemsetting/system/SystemLanguageAndMusic;->languages:[Lcom/konka/kkimplements/tv/LanguageItem;

    invoke-static {}, Lcom/konka/systemsetting/SysRootApp;->getTvDeskProvider()Lcom/konka/kkinterface/tv/TvDeskProvider;

    move-result-object v0

    invoke-interface {v0}, Lcom/konka/kkinterface/tv/TvDeskProvider;->getSettingManagerInstance()Lcom/konka/kkinterface/tv/SettingDesk;

    move-result-object v0

    iput-object v0, p0, Lcom/konka/systemsetting/system/SystemLanguageAndMusic;->settingDesk:Lcom/konka/kkinterface/tv/SettingDesk;

    iget-object v0, p0, Lcom/konka/systemsetting/system/SystemLanguageAndMusic;->settingDesk:Lcom/konka/kkinterface/tv/SettingDesk;

    if-eqz v0, :cond_0

    const-string v0, "the setting desk is good in use!!!"

    invoke-static {v0}, Lcom/konka/debuginfo/logPrint;->Debug(Ljava/lang/String;)V

    :goto_0
    iget-object v0, p0, Lcom/konka/systemsetting/system/SystemLanguageAndMusic;->mContext:Lcom/konka/systemsetting/MainActivity;

    invoke-virtual {v0}, Lcom/konka/systemsetting/MainActivity;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    const v1, 0x7f070006    # com.konka.systemsetting.R.array.str_arr_system_language

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getTextArray(I)[Ljava/lang/CharSequence;

    move-result-object v0

    iput-object v0, p0, Lcom/konka/systemsetting/system/SystemLanguageAndMusic;->language_values:[Ljava/lang/CharSequence;

    iget-object v0, p0, Lcom/konka/systemsetting/system/SystemLanguageAndMusic;->mContext:Lcom/konka/systemsetting/MainActivity;

    invoke-virtual {v0}, Lcom/konka/systemsetting/MainActivity;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    const v1, 0x7f070007    # com.konka.systemsetting.R.array.str_arr_system_music

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getTextArray(I)[Ljava/lang/CharSequence;

    move-result-object v0

    iput-object v0, p0, Lcom/konka/systemsetting/system/SystemLanguageAndMusic;->music_values:[Ljava/lang/CharSequence;

    invoke-direct {p0}, Lcom/konka/systemsetting/system/SystemLanguageAndMusic;->initUIComponents()V

    return-void

    :cond_0
    const-string v0, "the setting desk is null null null!"

    invoke-static {v0}, Lcom/konka/debuginfo/logPrint;->Debug(Ljava/lang/String;)V

    goto :goto_0
.end method

.method private SetOnClickListener()V
    .locals 2

    new-instance v0, Lcom/konka/systemsetting/system/SystemLanguageAndMusic$2;

    invoke-direct {v0, p0}, Lcom/konka/systemsetting/system/SystemLanguageAndMusic$2;-><init>(Lcom/konka/systemsetting/system/SystemLanguageAndMusic;)V

    iget-object v1, p0, Lcom/konka/systemsetting/system/SystemLanguageAndMusic;->linearlayout_itemCurrentLanguage:Landroid/widget/LinearLayout;

    invoke-virtual {v1, v0}, Landroid/widget/LinearLayout;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    iget-object v1, p0, Lcom/konka/systemsetting/system/SystemLanguageAndMusic;->linearlayout_itemMic:Landroid/widget/LinearLayout;

    invoke-virtual {v1, v0}, Landroid/widget/LinearLayout;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    iget-object v1, p0, Lcom/konka/systemsetting/system/SystemLanguageAndMusic;->linearlayout_itemBootingMusic:Landroid/widget/LinearLayout;

    invoke-virtual {v1, v0}, Landroid/widget/LinearLayout;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    return-void
.end method

.method private SetOnFocusChangeListener()V
    .locals 2

    new-instance v0, Lcom/konka/systemsetting/system/SystemLanguageAndMusic$6;

    invoke-direct {v0, p0}, Lcom/konka/systemsetting/system/SystemLanguageAndMusic$6;-><init>(Lcom/konka/systemsetting/system/SystemLanguageAndMusic;)V

    iget-object v1, p0, Lcom/konka/systemsetting/system/SystemLanguageAndMusic;->linearlayout_itemCurrentLanguage:Landroid/widget/LinearLayout;

    invoke-virtual {v1, v0}, Landroid/widget/LinearLayout;->setOnFocusChangeListener(Landroid/view/View$OnFocusChangeListener;)V

    iget-object v1, p0, Lcom/konka/systemsetting/system/SystemLanguageAndMusic;->linearlayout_itemMic:Landroid/widget/LinearLayout;

    invoke-virtual {v1, v0}, Landroid/widget/LinearLayout;->setOnFocusChangeListener(Landroid/view/View$OnFocusChangeListener;)V

    iget-object v1, p0, Lcom/konka/systemsetting/system/SystemLanguageAndMusic;->linearlayout_itemBootingMusic:Landroid/widget/LinearLayout;

    invoke-virtual {v1, v0}, Landroid/widget/LinearLayout;->setOnFocusChangeListener(Landroid/view/View$OnFocusChangeListener;)V

    return-void
.end method

.method private SetOnKeyListener()V
    .locals 2

    new-instance v0, Lcom/konka/systemsetting/system/SystemLanguageAndMusic$1;

    invoke-direct {v0, p0}, Lcom/konka/systemsetting/system/SystemLanguageAndMusic$1;-><init>(Lcom/konka/systemsetting/system/SystemLanguageAndMusic;)V

    iget-object v1, p0, Lcom/konka/systemsetting/system/SystemLanguageAndMusic;->linearlayout_itemCurrentLanguage:Landroid/widget/LinearLayout;

    invoke-virtual {v1, v0}, Landroid/widget/LinearLayout;->setOnKeyListener(Landroid/view/View$OnKeyListener;)V

    iget-object v1, p0, Lcom/konka/systemsetting/system/SystemLanguageAndMusic;->linearlayout_itemMic:Landroid/widget/LinearLayout;

    invoke-virtual {v1, v0}, Landroid/widget/LinearLayout;->setOnKeyListener(Landroid/view/View$OnKeyListener;)V

    iget-object v1, p0, Lcom/konka/systemsetting/system/SystemLanguageAndMusic;->linearlayout_itemBootingMusic:Landroid/widget/LinearLayout;

    invoke-virtual {v1, v0}, Landroid/widget/LinearLayout;->setOnKeyListener(Landroid/view/View$OnKeyListener;)V

    return-void
.end method

.method static synthetic access$0(Lcom/konka/systemsetting/system/SystemLanguageAndMusic;)[Lcom/konka/kkimplements/tv/LanguageItem;
    .locals 1

    iget-object v0, p0, Lcom/konka/systemsetting/system/SystemLanguageAndMusic;->languages:[Lcom/konka/kkimplements/tv/LanguageItem;

    return-object v0
.end method

.method static synthetic access$1(Lcom/konka/systemsetting/system/SystemLanguageAndMusic;)Lcom/konka/systemsetting/MainActivity;
    .locals 1

    iget-object v0, p0, Lcom/konka/systemsetting/system/SystemLanguageAndMusic;->mContext:Lcom/konka/systemsetting/MainActivity;

    return-object v0
.end method

.method static synthetic access$10(I)V
    .locals 0

    sput p0, Lcom/konka/systemsetting/system/SystemLanguageAndMusic;->m_iCurrMusicIndex:I

    return-void
.end method

.method static synthetic access$11()I
    .locals 1

    sget v0, Lcom/konka/systemsetting/system/SystemLanguageAndMusic;->m_iCurrMusicIndex:I

    return v0
.end method

.method static synthetic access$12(Lcom/konka/systemsetting/system/SystemLanguageAndMusic;I)V
    .locals 0

    invoke-direct {p0, p1}, Lcom/konka/systemsetting/system/SystemLanguageAndMusic;->switchMusic(I)V

    return-void
.end method

.method static synthetic access$2(Lcom/konka/systemsetting/system/SystemLanguageAndMusic;Ljava/lang/Integer;)V
    .locals 0

    iput-object p1, p0, Lcom/konka/systemsetting/system/SystemLanguageAndMusic;->m_currItemId:Ljava/lang/Integer;

    return-void
.end method

.method static synthetic access$3(Lcom/konka/systemsetting/system/SystemLanguageAndMusic;)Ljava/lang/Integer;
    .locals 1

    iget-object v0, p0, Lcom/konka/systemsetting/system/SystemLanguageAndMusic;->m_currItemId:Ljava/lang/Integer;

    return-object v0
.end method

.method static synthetic access$4(Lcom/konka/systemsetting/system/SystemLanguageAndMusic;)V
    .locals 0

    invoke-direct {p0}, Lcom/konka/systemsetting/system/SystemLanguageAndMusic;->showLanguageSettingDialog()V

    return-void
.end method

.method static synthetic access$5(Lcom/konka/systemsetting/system/SystemLanguageAndMusic;)V
    .locals 0

    invoke-direct {p0}, Lcom/konka/systemsetting/system/SystemLanguageAndMusic;->setMicStatus()V

    return-void
.end method

.method static synthetic access$6(Lcom/konka/systemsetting/system/SystemLanguageAndMusic;)V
    .locals 0

    invoke-direct {p0}, Lcom/konka/systemsetting/system/SystemLanguageAndMusic;->showMusicSettingDialog()V

    return-void
.end method

.method static synthetic access$7(Lcom/konka/systemsetting/system/SystemLanguageAndMusic;)V
    .locals 0

    invoke-direct {p0}, Lcom/konka/systemsetting/system/SystemLanguageAndMusic;->updateState()V

    return-void
.end method

.method static synthetic access$8(Lcom/konka/systemsetting/system/SystemLanguageAndMusic;I)V
    .locals 0

    iput p1, p0, Lcom/konka/systemsetting/system/SystemLanguageAndMusic;->m_iCurrLangIndexInDatabase:I

    return-void
.end method

.method static synthetic access$9(Lcom/konka/systemsetting/system/SystemLanguageAndMusic;)V
    .locals 0

    invoke-direct {p0}, Lcom/konka/systemsetting/system/SystemLanguageAndMusic;->confirmLanguageDialog()V

    return-void
.end method

.method private confirmLanguageDialog()V
    .locals 1

    iget v0, p0, Lcom/konka/systemsetting/system/SystemLanguageAndMusic;->m_iCurrLangIndex:I

    iput v0, p0, Lcom/konka/systemsetting/system/SystemLanguageAndMusic;->m_iPreLangIndex:I

    iget v0, p0, Lcom/konka/systemsetting/system/SystemLanguageAndMusic;->m_iCurrLangIndexInDatabase:I

    invoke-direct {p0, v0}, Lcom/konka/systemsetting/system/SystemLanguageAndMusic;->switchLanguage(I)V

    return-void
.end method

.method private getImageIdByState(Z)I
    .locals 1
    .param p1    # Z

    if-eqz p1, :cond_0

    iget-object v0, p0, Lcom/konka/systemsetting/system/SystemLanguageAndMusic;->mContext:Lcom/konka/systemsetting/MainActivity;

    iget v0, v0, Lcom/konka/systemsetting/MainActivity;->IMG_SWITCH_ON:I

    :goto_0
    return v0

    :cond_0
    iget-object v0, p0, Lcom/konka/systemsetting/system/SystemLanguageAndMusic;->mContext:Lcom/konka/systemsetting/MainActivity;

    iget v0, v0, Lcom/konka/systemsetting/MainActivity;->IMG_SWITCH_OFF:I

    goto :goto_0
.end method

.method private getLocale(Ljava/lang/String;)Ljava/util/Locale;
    .locals 4
    .param p1    # Ljava/lang/String;

    new-instance v0, Ljava/util/Locale;

    const/4 v1, 0x0

    const/4 v2, 0x2

    invoke-virtual {p1, v1, v2}, Ljava/lang/String;->substring(II)Ljava/lang/String;

    move-result-object v1

    const/4 v2, 0x3

    const/4 v3, 0x5

    invoke-virtual {p1, v2, v3}, Ljava/lang/String;->substring(II)Ljava/lang/String;

    move-result-object v2

    invoke-direct {v0, v1, v2}, Ljava/util/Locale;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    iput-object v0, p0, Lcom/konka/systemsetting/system/SystemLanguageAndMusic;->m_locale:Ljava/util/Locale;

    iget-object v0, p0, Lcom/konka/systemsetting/system/SystemLanguageAndMusic;->m_locale:Ljava/util/Locale;

    return-object v0
.end method

.method private initData()V
    .locals 6

    iget-object v3, p0, Lcom/konka/systemsetting/system/SystemLanguageAndMusic;->mContext:Lcom/konka/systemsetting/MainActivity;

    invoke-virtual {v3}, Lcom/konka/systemsetting/MainActivity;->getResources()Landroid/content/res/Resources;

    move-result-object v3

    invoke-virtual {v3}, Landroid/content/res/Resources;->getConfiguration()Landroid/content/res/Configuration;

    move-result-object v0

    iget-object v3, v0, Landroid/content/res/Configuration;->locale:Ljava/util/Locale;

    invoke-virtual {v3}, Ljava/util/Locale;->getCountry()Ljava/lang/String;

    move-result-object v2

    new-instance v3, Ljava/lang/StringBuilder;

    const-string v4, "#the country is +++"

    invoke-direct {v3, v4}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v3, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v3}, Lcom/konka/debuginfo/logPrint;->Debug(Ljava/lang/String;)V

    iget-object v3, p0, Lcom/konka/systemsetting/system/SystemLanguageAndMusic;->languages:[Lcom/konka/kkimplements/tv/LanguageItem;

    invoke-static {v3, v2}, Lcom/konka/kkimplements/tv/LanguageUtil;->getCurrentLanguageIndex([Lcom/konka/kkimplements/tv/LanguageItem;Ljava/lang/String;)I

    move-result v3

    iput v3, p0, Lcom/konka/systemsetting/system/SystemLanguageAndMusic;->m_iCurrLangIndexInDatabase:I

    const/4 v1, 0x0

    :goto_0
    iget-object v3, p0, Lcom/konka/systemsetting/system/SystemLanguageAndMusic;->m_strLocales:[Ljava/lang/String;

    array-length v3, v3

    if-lt v1, v3, :cond_0

    :goto_1
    iget-object v3, p0, Lcom/konka/systemsetting/system/SystemLanguageAndMusic;->settingDesk:Lcom/konka/kkinterface/tv/SettingDesk;

    invoke-interface {v3}, Lcom/konka/kkinterface/tv/SettingDesk;->GetEnvironmentPowerOnMusicMode()Lcom/mstar/android/tvapi/common/vo/EnumPowerOnMusicMode;

    move-result-object v3

    invoke-virtual {v3}, Lcom/mstar/android/tvapi/common/vo/EnumPowerOnMusicMode;->ordinal()I

    move-result v3

    sput v3, Lcom/konka/systemsetting/system/SystemLanguageAndMusic;->m_iCurrMusicIndex:I

    return-void

    :cond_0
    iget-object v3, p0, Lcom/konka/systemsetting/system/SystemLanguageAndMusic;->m_strLocales:[Ljava/lang/String;

    aget-object v3, v3, v1

    iget-object v4, p0, Lcom/konka/systemsetting/system/SystemLanguageAndMusic;->languages:[Lcom/konka/kkimplements/tv/LanguageItem;

    iget v5, p0, Lcom/konka/systemsetting/system/SystemLanguageAndMusic;->m_iCurrLangIndexInDatabase:I

    aget-object v4, v4, v5

    iget-object v4, v4, Lcom/konka/kkimplements/tv/LanguageItem;->mLanguage:Ljava/lang/String;

    invoke-virtual {v3, v4}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-eqz v3, :cond_1

    iput v1, p0, Lcom/konka/systemsetting/system/SystemLanguageAndMusic;->m_iCurrLangIndex:I

    goto :goto_1

    :cond_1
    add-int/lit8 v1, v1, 0x1

    goto :goto_0
.end method

.method private initUIComponents()V
    .locals 6

    const v5, 0x7f090097    # com.konka.systemsetting.R.id.linearlayout_system_tab_languageandmusic

    const v4, 0x7f090093    # com.konka.systemsetting.R.id.sys_system_item_mic

    const v3, 0x7f090092    # com.konka.systemsetting.R.id.sys_system_item_language

    const/4 v2, 0x1

    iget-object v0, p0, Lcom/konka/systemsetting/system/SystemLanguageAndMusic;->mContext:Lcom/konka/systemsetting/MainActivity;

    invoke-virtual {v0, v3}, Lcom/konka/systemsetting/MainActivity;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/LinearLayout;

    iput-object v0, p0, Lcom/konka/systemsetting/system/SystemLanguageAndMusic;->linearlayout_itemCurrentLanguage:Landroid/widget/LinearLayout;

    iget-object v0, p0, Lcom/konka/systemsetting/system/SystemLanguageAndMusic;->mContext:Lcom/konka/systemsetting/MainActivity;

    invoke-virtual {v0, v4}, Lcom/konka/systemsetting/MainActivity;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/LinearLayout;

    iput-object v0, p0, Lcom/konka/systemsetting/system/SystemLanguageAndMusic;->linearlayout_itemMic:Landroid/widget/LinearLayout;

    iget-object v0, p0, Lcom/konka/systemsetting/system/SystemLanguageAndMusic;->mContext:Lcom/konka/systemsetting/MainActivity;

    const v1, 0x7f090095    # com.konka.systemsetting.R.id.sys_system_item_music

    invoke-virtual {v0, v1}, Lcom/konka/systemsetting/MainActivity;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/LinearLayout;

    iput-object v0, p0, Lcom/konka/systemsetting/system/SystemLanguageAndMusic;->linearlayout_itemBootingMusic:Landroid/widget/LinearLayout;

    iget-object v0, p0, Lcom/konka/systemsetting/system/SystemLanguageAndMusic;->linearlayout_itemCurrentLanguage:Landroid/widget/LinearLayout;

    invoke-virtual {v0, v2}, Landroid/widget/LinearLayout;->setClickable(Z)V

    iget-object v0, p0, Lcom/konka/systemsetting/system/SystemLanguageAndMusic;->linearlayout_itemCurrentLanguage:Landroid/widget/LinearLayout;

    invoke-virtual {v0, v2}, Landroid/widget/LinearLayout;->setFocusable(Z)V

    iget-object v0, p0, Lcom/konka/systemsetting/system/SystemLanguageAndMusic;->linearlayout_itemCurrentLanguage:Landroid/widget/LinearLayout;

    invoke-virtual {v0, v2}, Landroid/widget/LinearLayout;->setFocusableInTouchMode(Z)V

    iget-object v0, p0, Lcom/konka/systemsetting/system/SystemLanguageAndMusic;->linearlayout_itemMic:Landroid/widget/LinearLayout;

    invoke-virtual {v0, v2}, Landroid/widget/LinearLayout;->setClickable(Z)V

    iget-object v0, p0, Lcom/konka/systemsetting/system/SystemLanguageAndMusic;->linearlayout_itemMic:Landroid/widget/LinearLayout;

    invoke-virtual {v0, v2}, Landroid/widget/LinearLayout;->setFocusable(Z)V

    iget-object v0, p0, Lcom/konka/systemsetting/system/SystemLanguageAndMusic;->linearlayout_itemMic:Landroid/widget/LinearLayout;

    invoke-virtual {v0, v2}, Landroid/widget/LinearLayout;->setFocusableInTouchMode(Z)V

    iget-object v0, p0, Lcom/konka/systemsetting/system/SystemLanguageAndMusic;->linearlayout_itemBootingMusic:Landroid/widget/LinearLayout;

    invoke-virtual {v0, v2}, Landroid/widget/LinearLayout;->setClickable(Z)V

    iget-object v0, p0, Lcom/konka/systemsetting/system/SystemLanguageAndMusic;->linearlayout_itemBootingMusic:Landroid/widget/LinearLayout;

    invoke-virtual {v0, v2}, Landroid/widget/LinearLayout;->setFocusable(Z)V

    iget-object v0, p0, Lcom/konka/systemsetting/system/SystemLanguageAndMusic;->linearlayout_itemBootingMusic:Landroid/widget/LinearLayout;

    invoke-virtual {v0, v2}, Landroid/widget/LinearLayout;->setFocusableInTouchMode(Z)V

    iget-object v0, p0, Lcom/konka/systemsetting/system/SystemLanguageAndMusic;->linearlayout_itemCurrentLanguage:Landroid/widget/LinearLayout;

    invoke-virtual {v0, v5}, Landroid/widget/LinearLayout;->setNextFocusLeftId(I)V

    iget-object v0, p0, Lcom/konka/systemsetting/system/SystemLanguageAndMusic;->linearlayout_itemMic:Landroid/widget/LinearLayout;

    invoke-virtual {v0, v5}, Landroid/widget/LinearLayout;->setNextFocusLeftId(I)V

    iget-object v0, p0, Lcom/konka/systemsetting/system/SystemLanguageAndMusic;->linearlayout_itemBootingMusic:Landroid/widget/LinearLayout;

    invoke-virtual {v0, v5}, Landroid/widget/LinearLayout;->setNextFocusLeftId(I)V

    iget-object v0, p0, Lcom/konka/systemsetting/system/SystemLanguageAndMusic;->linearlayout_itemCurrentLanguage:Landroid/widget/LinearLayout;

    invoke-virtual {v0, v4}, Landroid/widget/LinearLayout;->setNextFocusDownId(I)V

    iget-object v0, p0, Lcom/konka/systemsetting/system/SystemLanguageAndMusic;->linearlayout_itemMic:Landroid/widget/LinearLayout;

    invoke-virtual {v0, v3}, Landroid/widget/LinearLayout;->setNextFocusDownId(I)V

    iget-object v0, p0, Lcom/konka/systemsetting/system/SystemLanguageAndMusic;->linearlayout_itemCurrentLanguage:Landroid/widget/LinearLayout;

    invoke-virtual {v0, v4}, Landroid/widget/LinearLayout;->setNextFocusUpId(I)V

    iget-object v0, p0, Lcom/konka/systemsetting/system/SystemLanguageAndMusic;->linearlayout_itemMic:Landroid/widget/LinearLayout;

    invoke-virtual {v0, v3}, Landroid/widget/LinearLayout;->setNextFocusUpId(I)V

    invoke-direct {p0}, Lcom/konka/systemsetting/system/SystemLanguageAndMusic;->initData()V

    invoke-direct {p0}, Lcom/konka/systemsetting/system/SystemLanguageAndMusic;->updateState()V

    invoke-direct {p0}, Lcom/konka/systemsetting/system/SystemLanguageAndMusic;->SetOnKeyListener()V

    invoke-direct {p0}, Lcom/konka/systemsetting/system/SystemLanguageAndMusic;->SetOnClickListener()V

    invoke-direct {p0}, Lcom/konka/systemsetting/system/SystemLanguageAndMusic;->SetOnFocusChangeListener()V

    return-void
.end method

.method private isMicOn()Lcom/konka/kkinterface/tv/DataBaseDesk$MS_USER_SYSTEM_SETTING;
    .locals 1

    iget-object v0, p0, Lcom/konka/systemsetting/system/SystemLanguageAndMusic;->mContext:Lcom/konka/systemsetting/MainActivity;

    invoke-static {v0}, Lcom/konka/kkimplements/tv/mstar/DataBaseDeskImpl;->getDataBaseMgrInstance(Landroid/content/Context;)Lcom/konka/kkimplements/tv/mstar/DataBaseDeskImpl;

    move-result-object v0

    invoke-virtual {v0}, Lcom/konka/kkimplements/tv/mstar/DataBaseDeskImpl;->queryUserSysSetting()Lcom/konka/kkinterface/tv/DataBaseDesk$MS_USER_SYSTEM_SETTING;

    move-result-object v0

    return-object v0
.end method

.method private setInputSource2Storage()V
    .locals 7

    :try_start_0
    invoke-direct {p0}, Lcom/konka/systemsetting/system/SystemLanguageAndMusic;->isMicOn()Lcom/konka/kkinterface/tv/DataBaseDesk$MS_USER_SYSTEM_SETTING;

    move-result-object v2

    invoke-static {}, Lcom/mstar/android/tvapi/common/TvManager;->getInstance()Lcom/mstar/android/tvapi/common/TvManager;

    move-result-object v3

    if-eqz v3, :cond_0

    sget-object v1, Lcom/mstar/android/tvapi/common/vo/TvOsType$EnumInputSource;->E_INPUT_SOURCE_NONE:Lcom/mstar/android/tvapi/common/vo/TvOsType$EnumInputSource;

    iget-boolean v3, v2, Lcom/konka/kkinterface/tv/DataBaseDesk$MS_USER_SYSTEM_SETTING;->bMicOn:Z

    if-eqz v3, :cond_1

    sget-object v1, Lcom/mstar/android/tvapi/common/vo/TvOsType$EnumInputSource;->E_INPUT_SOURCE_KTV:Lcom/mstar/android/tvapi/common/vo/TvOsType$EnumInputSource;

    :goto_0
    invoke-static {}, Lcom/mstar/android/tvapi/common/TvManager;->getInstance()Lcom/mstar/android/tvapi/common/TvManager;

    move-result-object v3

    invoke-virtual {v3}, Lcom/mstar/android/tvapi/common/TvManager;->getCurrentInputSource()Lcom/mstar/android/tvapi/common/vo/TvOsType$EnumInputSource;

    move-result-object v3

    if-eq v3, v1, :cond_0

    invoke-static {}, Lcom/mstar/android/tvapi/common/TvManager;->getInstance()Lcom/mstar/android/tvapi/common/TvManager;

    move-result-object v3

    const/4 v4, 0x0

    const/4 v5, 0x0

    const/4 v6, 0x0

    invoke-virtual {v3, v1, v4, v5, v6}, Lcom/mstar/android/tvapi/common/TvManager;->setInputSource(Lcom/mstar/android/tvapi/common/vo/TvOsType$EnumInputSource;ZZZ)Z

    :cond_0
    :goto_1
    return-void

    :cond_1
    sget-object v1, Lcom/mstar/android/tvapi/common/vo/TvOsType$EnumInputSource;->E_INPUT_SOURCE_STORAGE:Lcom/mstar/android/tvapi/common/vo/TvOsType$EnumInputSource;
    :try_end_0
    .catch Lcom/mstar/android/tvapi/common/exception/TvCommonException; {:try_start_0 .. :try_end_0} :catch_0

    goto :goto_0

    :catch_0
    move-exception v0

    invoke-virtual {v0}, Lcom/mstar/android/tvapi/common/exception/TvCommonException;->printStackTrace()V

    goto :goto_1
.end method

.method private setMicOnOff(Z)V
    .locals 3
    .param p1    # Z

    iget-object v1, p0, Lcom/konka/systemsetting/system/SystemLanguageAndMusic;->linearlayout_itemMic:Landroid/widget/LinearLayout;

    const/4 v2, 0x1

    invoke-virtual {v1, v2}, Landroid/widget/LinearLayout;->getChildAt(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/ImageView;

    invoke-direct {p0, p1}, Lcom/konka/systemsetting/system/SystemLanguageAndMusic;->getImageIdByState(Z)I

    move-result v1

    invoke-virtual {v0, v1}, Landroid/widget/ImageView;->setImageResource(I)V

    return-void
.end method

.method private setMicStatus()V
    .locals 2

    invoke-direct {p0}, Lcom/konka/systemsetting/system/SystemLanguageAndMusic;->isMicOn()Lcom/konka/kkinterface/tv/DataBaseDesk$MS_USER_SYSTEM_SETTING;

    move-result-object v0

    iget-boolean v1, v0, Lcom/konka/kkinterface/tv/DataBaseDesk$MS_USER_SYSTEM_SETTING;->bMicOn:Z

    if-eqz v1, :cond_0

    const/4 v1, 0x0

    :goto_0
    iput-boolean v1, v0, Lcom/konka/kkinterface/tv/DataBaseDesk$MS_USER_SYSTEM_SETTING;->bMicOn:Z

    iget-object v1, p0, Lcom/konka/systemsetting/system/SystemLanguageAndMusic;->mContext:Lcom/konka/systemsetting/MainActivity;

    invoke-static {v1}, Lcom/konka/kkimplements/tv/mstar/DataBaseDeskImpl;->getDataBaseMgrInstance(Landroid/content/Context;)Lcom/konka/kkimplements/tv/mstar/DataBaseDeskImpl;

    move-result-object v1

    invoke-virtual {v1, v0}, Lcom/konka/kkimplements/tv/mstar/DataBaseDeskImpl;->updateUserSysSetting(Lcom/konka/kkinterface/tv/DataBaseDesk$MS_USER_SYSTEM_SETTING;)V

    invoke-direct {p0}, Lcom/konka/systemsetting/system/SystemLanguageAndMusic;->setInputSource2Storage()V

    return-void

    :cond_0
    const/4 v1, 0x1

    goto :goto_0
.end method

.method private setTextCurrentLang()V
    .locals 3

    iget-object v1, p0, Lcom/konka/systemsetting/system/SystemLanguageAndMusic;->linearlayout_itemCurrentLanguage:Landroid/widget/LinearLayout;

    if-nez v1, :cond_0

    :goto_0
    return-void

    :cond_0
    iget-object v1, p0, Lcom/konka/systemsetting/system/SystemLanguageAndMusic;->linearlayout_itemCurrentLanguage:Landroid/widget/LinearLayout;

    const/4 v2, 0x1

    invoke-virtual {v1, v2}, Landroid/widget/LinearLayout;->getChildAt(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    iget-object v1, p0, Lcom/konka/systemsetting/system/SystemLanguageAndMusic;->language_values:[Ljava/lang/CharSequence;

    iget v2, p0, Lcom/konka/systemsetting/system/SystemLanguageAndMusic;->m_iCurrLangIndex:I

    aget-object v1, v1, v2

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    goto :goto_0
.end method

.method private setTextCurrentMusic()V
    .locals 3

    iget-object v1, p0, Lcom/konka/systemsetting/system/SystemLanguageAndMusic;->linearlayout_itemBootingMusic:Landroid/widget/LinearLayout;

    if-nez v1, :cond_0

    :goto_0
    return-void

    :cond_0
    iget-object v1, p0, Lcom/konka/systemsetting/system/SystemLanguageAndMusic;->linearlayout_itemBootingMusic:Landroid/widget/LinearLayout;

    const/4 v2, 0x1

    invoke-virtual {v1, v2}, Landroid/widget/LinearLayout;->getChildAt(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    iget-object v1, p0, Lcom/konka/systemsetting/system/SystemLanguageAndMusic;->music_values:[Ljava/lang/CharSequence;

    sget v2, Lcom/konka/systemsetting/system/SystemLanguageAndMusic;->m_iCurrMusicIndex:I

    aget-object v1, v1, v2

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    goto :goto_0
.end method

.method private showLanguageSettingDialog()V
    .locals 4

    new-instance v0, Landroid/app/AlertDialog$Builder;

    iget-object v2, p0, Lcom/konka/systemsetting/system/SystemLanguageAndMusic;->mContext:Lcom/konka/systemsetting/MainActivity;

    invoke-direct {v0, v2}, Landroid/app/AlertDialog$Builder;-><init>(Landroid/content/Context;)V

    const v2, 0x7f060049    # com.konka.systemsetting.R.string.str_system_language_title

    invoke-virtual {v0, v2}, Landroid/app/AlertDialog$Builder;->setTitle(I)Landroid/app/AlertDialog$Builder;

    const v2, 0x7f020026    # com.konka.systemsetting.R.drawable.syssettingfirsticonsysunfocus

    invoke-virtual {v0, v2}, Landroid/app/AlertDialog$Builder;->setIcon(I)Landroid/app/AlertDialog$Builder;

    new-instance v1, Lcom/konka/systemsetting/system/SystemLanguageAndMusic$3;

    invoke-direct {v1, p0}, Lcom/konka/systemsetting/system/SystemLanguageAndMusic$3;-><init>(Lcom/konka/systemsetting/system/SystemLanguageAndMusic;)V

    new-instance v2, Lcom/konka/systemsetting/system/SystemLanguageAndMusic$LanguageListAdapter;

    invoke-direct {v2, p0}, Lcom/konka/systemsetting/system/SystemLanguageAndMusic$LanguageListAdapter;-><init>(Lcom/konka/systemsetting/system/SystemLanguageAndMusic;)V

    iget v3, p0, Lcom/konka/systemsetting/system/SystemLanguageAndMusic;->m_iCurrLangIndexInDatabase:I

    invoke-virtual {v0, v2, v3, v1}, Landroid/app/AlertDialog$Builder;->setSingleChoiceItems(Landroid/widget/ListAdapter;ILandroid/content/DialogInterface$OnClickListener;)Landroid/app/AlertDialog$Builder;

    invoke-virtual {v0}, Landroid/app/AlertDialog$Builder;->show()Landroid/app/AlertDialog;

    return-void
.end method

.method private showMusicSettingDialog()V
    .locals 4

    new-instance v0, Landroid/app/AlertDialog$Builder;

    iget-object v2, p0, Lcom/konka/systemsetting/system/SystemLanguageAndMusic;->mContext:Lcom/konka/systemsetting/MainActivity;

    invoke-direct {v0, v2}, Landroid/app/AlertDialog$Builder;-><init>(Landroid/content/Context;)V

    const v2, 0x7f06004c    # com.konka.systemsetting.R.string.str_system_item_bootingmusic

    invoke-virtual {v0, v2}, Landroid/app/AlertDialog$Builder;->setTitle(I)Landroid/app/AlertDialog$Builder;

    const v2, 0x7f020026    # com.konka.systemsetting.R.drawable.syssettingfirsticonsysunfocus

    invoke-virtual {v0, v2}, Landroid/app/AlertDialog$Builder;->setIcon(I)Landroid/app/AlertDialog$Builder;

    new-instance v1, Lcom/konka/systemsetting/system/SystemLanguageAndMusic$4;

    invoke-direct {v1, p0}, Lcom/konka/systemsetting/system/SystemLanguageAndMusic$4;-><init>(Lcom/konka/systemsetting/system/SystemLanguageAndMusic;)V

    iget-object v2, p0, Lcom/konka/systemsetting/system/SystemLanguageAndMusic;->music_values:[Ljava/lang/CharSequence;

    sget v3, Lcom/konka/systemsetting/system/SystemLanguageAndMusic;->m_iCurrMusicIndex:I

    invoke-virtual {v0, v2, v3, v1}, Landroid/app/AlertDialog$Builder;->setSingleChoiceItems([Ljava/lang/CharSequence;ILandroid/content/DialogInterface$OnClickListener;)Landroid/app/AlertDialog$Builder;

    new-instance v2, Lcom/konka/systemsetting/system/SystemLanguageAndMusic$5;

    invoke-direct {v2, p0}, Lcom/konka/systemsetting/system/SystemLanguageAndMusic$5;-><init>(Lcom/konka/systemsetting/system/SystemLanguageAndMusic;)V

    invoke-virtual {v0, v2}, Landroid/app/AlertDialog$Builder;->setOnItemSelectedListener(Landroid/widget/AdapterView$OnItemSelectedListener;)Landroid/app/AlertDialog$Builder;

    invoke-virtual {v0}, Landroid/app/AlertDialog$Builder;->show()Landroid/app/AlertDialog;

    return-void
.end method

.method private switchLanguage(I)V
    .locals 8
    .param p1    # I

    new-instance v4, Landroid/content/Intent;

    const-string v5, "com.konka.systemsetting.action.MainActivity"

    invoke-direct {v4, v5}, Landroid/content/Intent;-><init>(Ljava/lang/String;)V

    const-string v5, "menu_name"

    const-string v6, "system_language"

    invoke-virtual {v4, v5, v6}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    iget-object v5, p0, Lcom/konka/systemsetting/system/SystemLanguageAndMusic;->mContext:Lcom/konka/systemsetting/MainActivity;

    invoke-virtual {v5, v4}, Lcom/konka/systemsetting/MainActivity;->startActivity(Landroid/content/Intent;)V

    const/4 v2, 0x0

    :goto_0
    iget-object v5, p0, Lcom/konka/systemsetting/system/SystemLanguageAndMusic;->m_strLocales:[Ljava/lang/String;

    array-length v5, v5

    if-lt v2, v5, :cond_0

    :goto_1
    iget-object v5, p0, Lcom/konka/systemsetting/system/SystemLanguageAndMusic;->m_strLocales:[Ljava/lang/String;

    iget v6, p0, Lcom/konka/systemsetting/system/SystemLanguageAndMusic;->m_iCurrLangIndex:I

    aget-object v5, v5, v6

    invoke-direct {p0, v5}, Lcom/konka/systemsetting/system/SystemLanguageAndMusic;->getLocale(Ljava/lang/String;)Ljava/util/Locale;

    move-result-object v5

    iput-object v5, p0, Lcom/konka/systemsetting/system/SystemLanguageAndMusic;->m_locale:Ljava/util/Locale;

    invoke-static {}, Landroid/app/ActivityManagerNative;->getDefault()Landroid/app/IActivityManager;

    move-result-object v3

    :try_start_0
    invoke-interface {v3}, Landroid/app/IActivityManager;->getConfiguration()Landroid/content/res/Configuration;

    move-result-object v0

    iget-object v5, p0, Lcom/konka/systemsetting/system/SystemLanguageAndMusic;->m_locale:Ljava/util/Locale;

    iput-object v5, v0, Landroid/content/res/Configuration;->locale:Ljava/util/Locale;

    const/4 v5, 0x1

    iput-boolean v5, v0, Landroid/content/res/Configuration;->userSetLocale:Z

    invoke-interface {v3, v0}, Landroid/app/IActivityManager;->updateConfiguration(Landroid/content/res/Configuration;)V

    const-string v5, "com.android.providers.settings"

    invoke-static {v5}, Landroid/app/backup/BackupManager;->dataChanged(Ljava/lang/String;)V
    :try_end_0
    .catch Landroid/os/RemoteException; {:try_start_0 .. :try_end_0} :catch_0

    :goto_2
    return-void

    :cond_0
    iget-object v5, p0, Lcom/konka/systemsetting/system/SystemLanguageAndMusic;->m_strLocales:[Ljava/lang/String;

    aget-object v5, v5, v2

    iget-object v6, p0, Lcom/konka/systemsetting/system/SystemLanguageAndMusic;->languages:[Lcom/konka/kkimplements/tv/LanguageItem;

    iget v7, p0, Lcom/konka/systemsetting/system/SystemLanguageAndMusic;->m_iCurrLangIndexInDatabase:I

    aget-object v6, v6, v7

    iget-object v6, v6, Lcom/konka/kkimplements/tv/LanguageItem;->mLanguage:Ljava/lang/String;

    invoke-virtual {v5, v6}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v5

    if-eqz v5, :cond_1

    iput v2, p0, Lcom/konka/systemsetting/system/SystemLanguageAndMusic;->m_iCurrLangIndex:I

    goto :goto_1

    :cond_1
    add-int/lit8 v2, v2, 0x1

    goto :goto_0

    :catch_0
    move-exception v1

    invoke-virtual {v1}, Landroid/os/RemoteException;->printStackTrace()V

    goto :goto_2
.end method

.method private switchMusic(I)V
    .locals 3
    .param p1    # I

    iget-object v0, p0, Lcom/konka/systemsetting/system/SystemLanguageAndMusic;->settingDesk:Lcom/konka/kkinterface/tv/SettingDesk;

    invoke-static {}, Lcom/mstar/android/tvapi/common/vo/EnumPowerOnMusicMode;->values()[Lcom/mstar/android/tvapi/common/vo/EnumPowerOnMusicMode;

    move-result-object v1

    sget v2, Lcom/konka/systemsetting/system/SystemLanguageAndMusic;->m_iCurrMusicIndex:I

    aget-object v1, v1, v2

    invoke-interface {v0, v1}, Lcom/konka/kkinterface/tv/SettingDesk;->SetEnvironmentPowerOnMusicMode(Lcom/mstar/android/tvapi/common/vo/EnumPowerOnMusicMode;)Z

    return-void
.end method

.method private updateState()V
    .locals 1

    invoke-direct {p0}, Lcom/konka/systemsetting/system/SystemLanguageAndMusic;->setTextCurrentLang()V

    invoke-direct {p0}, Lcom/konka/systemsetting/system/SystemLanguageAndMusic;->setTextCurrentMusic()V

    invoke-direct {p0}, Lcom/konka/systemsetting/system/SystemLanguageAndMusic;->isMicOn()Lcom/konka/kkinterface/tv/DataBaseDesk$MS_USER_SYSTEM_SETTING;

    move-result-object v0

    iget-boolean v0, v0, Lcom/konka/kkinterface/tv/DataBaseDesk$MS_USER_SYSTEM_SETTING;->bMicOn:Z

    invoke-direct {p0, v0}, Lcom/konka/systemsetting/system/SystemLanguageAndMusic;->setMicOnOff(Z)V

    return-void
.end method
