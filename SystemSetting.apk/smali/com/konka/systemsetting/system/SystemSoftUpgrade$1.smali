.class Lcom/konka/systemsetting/system/SystemSoftUpgrade$1;
.super Ljava/lang/Object;
.source "SystemSoftUpgrade.java"

# interfaces
.implements Landroid/view/View$OnClickListener;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/konka/systemsetting/system/SystemSoftUpgrade;->setOnClickListener()V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/konka/systemsetting/system/SystemSoftUpgrade;


# direct methods
.method constructor <init>(Lcom/konka/systemsetting/system/SystemSoftUpgrade;)V
    .locals 0

    iput-object p1, p0, Lcom/konka/systemsetting/system/SystemSoftUpgrade$1;->this$0:Lcom/konka/systemsetting/system/SystemSoftUpgrade;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onClick(Landroid/view/View;)V
    .locals 1
    .param p1    # Landroid/view/View;

    invoke-virtual {p1}, Landroid/view/View;->getId()I

    move-result v0

    packed-switch v0, :pswitch_data_0

    :goto_0
    return-void

    :pswitch_0
    iget-object v0, p0, Lcom/konka/systemsetting/system/SystemSoftUpgrade$1;->this$0:Lcom/konka/systemsetting/system/SystemSoftUpgrade;

    # invokes: Lcom/konka/systemsetting/system/SystemSoftUpgrade;->startUpgradeActivity()V
    invoke-static {v0}, Lcom/konka/systemsetting/system/SystemSoftUpgrade;->access$0(Lcom/konka/systemsetting/system/SystemSoftUpgrade;)V

    goto :goto_0

    :pswitch_1
    iget-object v0, p0, Lcom/konka/systemsetting/system/SystemSoftUpgrade$1;->this$0:Lcom/konka/systemsetting/system/SystemSoftUpgrade;

    # invokes: Lcom/konka/systemsetting/system/SystemSoftUpgrade;->startUpgradeByUSB()V
    invoke-static {v0}, Lcom/konka/systemsetting/system/SystemSoftUpgrade;->access$1(Lcom/konka/systemsetting/system/SystemSoftUpgrade;)V

    goto :goto_0

    :pswitch_data_0
    .packed-switch 0x7f0900a3
        :pswitch_1    # com.konka.systemsetting.R.id.btn_upgrade_byusb
        :pswitch_0    # com.konka.systemsetting.R.id.btn_upgrade_online
    .end packed-switch
.end method
