.class public Lcom/konka/systemsetting/common/Content$Ress;
.super Ljava/lang/Object;
.source "Content.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/konka/systemsetting/common/Content;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x9
    name = "Ress"
.end annotation


# static fields
.field public static final ARR_AUTO_TIME:I = 0x7f070002

.field public static final ARR_BOOT_CHANNEL:I = 0x7f070001

.field public static final ARR_COM_SWITCH:I = 0x7f070000

.field public static final ARR_CONS_MODE:I = 0x7f070003

.field public static final ARR_NOS_MODE:I = 0x7f070004

.field public static final ARR_SYSTEM_LANGUAGE:I = 0x7f070006

.field public static final ARR_SYSTEM_MUSIC:I = 0x7f070007

.field public static final ARR_WIFI_SWITCH:I = 0x7f070008

.field public static final ARR_WIRE_MODE:I = 0x7f070009

.field public static final CBOX_WIFI_SHOWPWD:I = 0x7f090058

.field public static final COLOR_DISABLED:I = -0x515152

.field public static final COLOR_ENABLED:I = -0x1000000

.field public static final COLOR_WIFI_TIP:I = -0x515152

.field public static final EDITTEXT_DNSSERVER:I = 0x7f09006b

.field public static final EDITTEXT_GATEWAY:I = 0x7f090069

.field public static final EDITTEXT_IPADDRESS:I = 0x7f090065

.field public static final EDITTEXT_PASSWORD:I = 0x7f090037

.field public static final EDITTEXT_SUBNETMASK:I = 0x7f090067

.field public static final EDITTEXT_USRNAME:I = 0x7f090035

.field public static final EDITTEXT_WIFI_PASSWORD:I = 0x7f090057

.field public static final ID_FORMAT:I = 0x7f090081

.field public static final ID_FREESPACE:I = 0x7f090082

.field public static final ID_SND_SDCARD:I = 0x7f090086

.field public static final ID_TOTALSPACE:I = 0x7f090083

.field public static final ID_UNMOUNT:I = 0x7f090080

.field public static final IMG_ALTDLG_ICON:I = 0x7f020026

.field public static final IMG_BOTTOM_FOCUS:I = 0x7f02002e

.field public static final IMG_BOTTOM_UNFOCUS:I = 0x7f02002d

.field public static final IMG_ITEM_FOCUS:I = 0x7f020032

.field public static final IMG_ITEM_UNFOCUS:I = 0x7f020031

.field public static final IMG_MID_FOCUS:I = 0x7f020030

.field public static final IMG_MID_UNFOCUS:I = 0x7f02002f

.field public static final IMG_NETWORK_ICON:I = 0x7f020020

.field public static final IMG_ONE_PX:I = 0x7f020012

.field public static final IMG_PERSONAL_ICON:I = 0x7f020022

.field public static final IMG_SINGLE_FOCUS:I = 0x7f020032

.field public static final IMG_SINGLE_UNFOCUS:I = 0x7f020031

.field public static final IMG_SWITCH_OFF:I = 0x7f020010

.field public static final IMG_SWITCH_ON:I = 0x7f020011

.field public static final IMG_TOP_FOCUS:I = 0x7f020034

.field public static final IMG_TOP_UNFOCUS:I = 0x7f020033

.field public static final IMG_WIFI_LOCKED:I = 0x7f02003a

.field public static final IMG_WIFI_LVL0:I = 0x7f020036

.field public static final IMG_WIFI_LVL1:I = 0x7f020037

.field public static final IMG_WIFI_LVL2:I = 0x7f020038

.field public static final IMG_WIFI_LVL3:I = 0x7f020039

.field public static final IPMODE_AUTOSET:I = 0x0

.field public static final IPMODE_MANUAL:I = 0x1

.field public static final ITEM_ANDROID_KEYBOARD:I = 0x7f090012

.field public static final ITEM_APP_INSTALL:I = 0x7f090017

.field public static final ITEM_AUTODIAL:I = 0x7f090038

.field public static final ITEM_AUTO_ADJUST:I = 0x7f090026

.field public static final ITEM_BACKLIGHT:I = 0x7f09001a

.field public static final ITEM_BOOT_CHANNEL:I = 0x7f09002e

.field public static final ITEM_BOOT_SWITCH:I = 0x7f09002c

.field public static final ITEM_BOOT_TIME:I = 0x7f09002d

.field public static final ITEM_CONNECTION:[I

.field public static final ITEM_CONNECTION_0:I = 0x7f090073

.field public static final ITEM_CONNECTION_1:I = 0x7f090074

.field public static final ITEM_CONNECTION_2:I = 0x7f090075

.field public static final ITEM_CONNECTION_3:I = 0x7f090076

.field public static final ITEM_CONNECTION_4:I = 0x7f090077

.field public static final ITEM_CURRINPUT:I = 0x7f090091

.field public static final ITEM_DATE_SETTING:I = 0x7f090027

.field public static final ITEM_DETECTWIFI:I = 0x7f090079

.field public static final ITEM_DETECTWIRE:I = 0x7f09006d

.field public static final ITEM_DNSSERVER:I = 0x7f09006a

.field public static final ITEM_ENERGYSAVING:I = 0x7f090018

.field public static final ITEM_GATEWAY:I = 0x7f090068

.field public static final ITEM_HALT_SWITCH:I = 0x7f09002a

.field public static final ITEM_HALT_TIME:I = 0x7f09002b

.field public static final ITEM_IPADDRESS:I = 0x7f090064

.field public static final ITEM_IPMODE:I = 0x7f090063

.field public static final ITEM_LANG:I = 0x7f090092

.field public static final ITEM_MIC:I = 0x7f090093

.field public static final ITEM_MUSIC:I = 0x7f090095

.field public static final ITEM_NOACTIONSTANDBY:I = 0x7f09001f

.field public static final ITEM_NOSIGNALSTANDBY:I = 0x7f09001e

.field public static final ITEM_PASSWROD:I = 0x7f090036

.field public static final ITEM_SCREENSAVER:I = 0x7f090019

.field public static final ITEM_SETPPPOE:I = 0x7f09003a

.field public static final ITEM_SETWIRE:I = 0x7f09006c

.field public static final ITEM_SHOWPWD:I = 0x7f090039

.field public static final ITEM_SND_ENERGYSAVING:I = 0x7f090021

.field public static final ITEM_SND_TIMESELECTION:I = 0x7f090022

.field public static final ITEM_SUBNETMASK:I = 0x7f090066

.field public static final ITEM_TAB_INPUT:I = 0x7f090098

.field public static final ITEM_TAB_LM:I = 0x7f090097

.field public static final ITEM_TAB_PPPOE:I = 0x7f09003e

.field public static final ITEM_TAB_WIFI:I = 0x7f09003d

.field public static final ITEM_TAB_WIRE:I = 0x7f09003c

.field public static final ITEM_TIMEZONE_SETTING:I = 0x7f090029

.field public static final ITEM_TIME_SETTING:I = 0x7f090028

.field public static final ITEM_USERNAME:I = 0x7f090034

.field public static final ITEM_WIFISWITCH:I = 0x7f09006e

.field public static final LAYOUT_WIFI_BOX:I = 0x7f030014

.field public static final MENU_WIFI_BOX:I = 0x7f090051

.field public static final MENU_WIFI_INPUT:I = 0x7f090056

.field public static final SEEKBAR_BACKLIGHT:I = 0x7f09001c

.field public static final SIZE_WIFI_TIP:I = 0x10

.field public static final STR_ALTTITLE_LANG:I = 0x7f060049

.field public static final STR_ALTTITLE_MUSIC:I = 0x7f06004c

.field public static final STR_APP_ALLOWED:I = 0x7f06001e

.field public static final STR_APP_ATTENTION:I = 0x7f060020

.field public static final STR_APP_CONTEXT:I = 0x7f060021

.field public static final STR_APP_NOTALLOWED:I = 0x7f06001f

.field public static final STR_APP_SETTING:I = 0x7f06001c

.field public static final STR_APP_UNKNOWN:I = 0x7f06001d

.field public static final STR_AUTO_ADJUST:I = 0x7f060011

.field public static final STR_BOOT_CHANNEL:I = 0x7f06001a

.field public static final STR_BOOT_SWITCH:I = 0x7f060018

.field public static final STR_BOOT_TIME:I = 0x7f060019

.field public static final STR_CABLE_CONNECTED:I = 0x7f06006a

.field public static final STR_CABLE_MOUNT:I = 0x7f060070

.field public static final STR_CABLE_UNMOUNT:I = 0x7f060071

.field public static final STR_CHKDNSSVR_FAIL:I = 0x7f060066

.field public static final STR_CHKGATEWAY_FAIL:I = 0x7f060064

.field public static final STR_CHKIPADDR_FAIL:I = 0x7f060063

.field public static final STR_CHKNETMASK_FAIL:I = 0x7f060065

.field public static final STR_CHK_USNAME_PWD:I = 0x7f0600a9

.field public static final STR_CURRINPUT:I = 0x7f06004e

.field public static final STR_DATE_SETTING:I = 0x7f060012

.field public static final STR_DISCONNECT_STATE:I = 0x7f060093

.field public static final STR_DLGTITLE_ENERGYSAVING:I = 0x7f060022

.field public static final STR_DLGTITLE_NOOPERATIONSTANDBY:I = 0x7f060025

.field public static final STR_DLG_CANCLE:I = 0x7f06000d

.field public static final STR_DLG_SET:I = 0x7f06000c

.field public static final STR_ETHENET_FINISH_SETTING:I = 0x7f060069

.field public static final STR_ETHERNET_MODE:I = 0x7f06009b

.field public static final STR_ETHERNET_STATE_DISABLED:I = 0x7f06006e

.field public static final STR_ETH_CONFIG_EXISTS:I = 0x7f06006d

.field public static final STR_ETH_NOTACTIVE:I = 0x7f06006f

.field public static final STR_HALT_SWITCH:I = 0x7f060016

.field public static final STR_HATL_TIME:I = 0x7f060017

.field public static final STR_INPUTSETTINGS:I = 0x7f06004d

.field public static final STR_IS_SCANNING:I = 0x7f06008d

.field public static final STR_NEW_MSG:I = 0x7f0600a2

.field public static final STR_NO_ACTIVE_NETWORK:I = 0x7f060087

.field public static final STR_PPPOE_AUTO_FAIL:I = 0x7f0600a1

.field public static final STR_PPPOE_AUTO_SUCCESS:I = 0x7f0600a0

.field public static final STR_PPPOE_DISCONETED:I = 0x7f0600a7

.field public static final STR_PPPOE_ISDIALING:I = 0x7f0600a8

.field public static final STR_PPPOE_NAMEHINT:I = 0x7f0600a5

.field public static final STR_PPPOE_PWDHINT:I = 0x7f0600a6

.field public static final STR_PPPOE_TIPS:I = 0x7f0600a3

.field public static final STR_SWITCH_OFF:I = 0x7f060055

.field public static final STR_SWITCH_ON:I = 0x7f060054

.field public static final STR_TIMEZONE_SELECT:I = 0x7f06001b

.field public static final STR_TIME_SETTING:I = 0x7f060013

.field public static final STR_WIFI_CLOSED:I = 0x7f060082

.field public static final STR_WIFI_CONNECTED:I = 0x7f060091

.field public static final STR_WIFI_DISABLED:I = 0x7f060082

.field public static final STR_WIFI_DISABLING:I = 0x7f060081

.field public static final STR_WIFI_ENABLED:I = 0x7f06007f

.field public static final STR_WIFI_FOUND:I = 0x7f060086

.field public static final STR_WIFI_MODE:I = 0x7f06009c

.field public static final STR_WIFI_NODEVICE:I = 0x7f060085

.field public static final STR_WIFI_NOTFOUND:I = 0x7f060087

.field public static final STR_WIFI_TIP_CONNECTED:Ljava/lang/CharSequence;

.field public static final STR_WIFI_UNREACHABLE:I = 0x7f06008a

.field public static final SWITCH_OFF:I = 0x1

.field public static final SWITCH_ON:I = 0x0

.field public static final TEXTVIEW_COMTIP:I = 0x7f090071

.field public static final TEXTVIEW_WIFISWITCH:I = 0x7f090070

.field public static final TEXTVIEW_WIFITIP:I = 0x7f09006f

.field public static final TEXTVIEW_WIFI_SECURITY:I = 0x7f090054

.field public static final TEXTVIEW_WIFI_SIGNALSTR:I = 0x7f090055

.field public static final TEXTVIEW_WIFI_TITLE:I = 0x7f090052

.field public static final WIFI_DEVICE_REMOVED:I = 0x7f060084


# direct methods
.method static constructor <clinit>()V
    .locals 1

    const/4 v0, 0x5

    new-array v0, v0, [I

    fill-array-data v0, :array_0

    sput-object v0, Lcom/konka/systemsetting/common/Content$Ress;->ITEM_CONNECTION:[I

    const-string v0, ""

    sput-object v0, Lcom/konka/systemsetting/common/Content$Ress;->STR_WIFI_TIP_CONNECTED:Ljava/lang/CharSequence;

    return-void

    nop

    :array_0
    .array-data 4
        0x7f090073    # com.konka.systemsetting.R.id.sys_network_wireless_item_link1
        0x7f090074    # com.konka.systemsetting.R.id.sys_network_wireless_item_link2
        0x7f090075    # com.konka.systemsetting.R.id.sys_network_wireless_item_link3
        0x7f090076    # com.konka.systemsetting.R.id.sys_network_wireless_item_link4
        0x7f090077    # com.konka.systemsetting.R.id.sys_network_wireless_item_link5
    .end array-data
.end method

.method public constructor <init>()V
    .locals 0

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method
