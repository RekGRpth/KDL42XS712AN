.class public Lcom/konka/systemsetting/common/Content$wifi;
.super Ljava/lang/Object;
.source "Content.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/konka/systemsetting/common/Content;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x9
    name = "wifi"
.end annotation


# static fields
.field public static HPCounter:Ljava/lang/Integer;

.field public static HPNames:[Ljava/lang/CharSequence;

.field public static currHPFirstIndex:Ljava/lang/Integer;

.field public static currSelectIndex:Ljava/lang/Integer;

.field public static isOpen:Z

.field public static wifiSwitch:[Ljava/lang/CharSequence;


# direct methods
.method static constructor <clinit>()V
    .locals 3

    const/4 v2, 0x0

    const/4 v1, 0x0

    sput-boolean v1, Lcom/konka/systemsetting/common/Content$wifi;->isOpen:Z

    sput-object v2, Lcom/konka/systemsetting/common/Content$wifi;->HPNames:[Ljava/lang/CharSequence;

    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v0

    sput-object v0, Lcom/konka/systemsetting/common/Content$wifi;->HPCounter:Ljava/lang/Integer;

    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v0

    sput-object v0, Lcom/konka/systemsetting/common/Content$wifi;->currHPFirstIndex:Ljava/lang/Integer;

    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v0

    sput-object v0, Lcom/konka/systemsetting/common/Content$wifi;->currSelectIndex:Ljava/lang/Integer;

    sput-object v2, Lcom/konka/systemsetting/common/Content$wifi;->wifiSwitch:[Ljava/lang/CharSequence;

    return-void
.end method

.method public constructor <init>()V
    .locals 0

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method private static decreaseCurrSelectedIndex()Z
    .locals 2

    const/4 v0, 0x1

    sget-object v1, Lcom/konka/systemsetting/common/Content$wifi;->currSelectIndex:Ljava/lang/Integer;

    invoke-virtual {v1}, Ljava/lang/Integer;->intValue()I

    move-result v1

    if-lez v1, :cond_0

    sget-object v1, Lcom/konka/systemsetting/common/Content$wifi;->currSelectIndex:Ljava/lang/Integer;

    invoke-virtual {v1}, Ljava/lang/Integer;->intValue()I

    move-result v1

    add-int/lit8 v1, v1, -0x1

    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    sput-object v1, Lcom/konka/systemsetting/common/Content$wifi;->currSelectIndex:Ljava/lang/Integer;

    :goto_0
    return v0

    :cond_0
    sget-object v1, Lcom/konka/systemsetting/common/Content$wifi;->currHPFirstIndex:Ljava/lang/Integer;

    invoke-virtual {v1}, Ljava/lang/Integer;->intValue()I

    move-result v1

    if-lez v1, :cond_1

    sget-object v1, Lcom/konka/systemsetting/common/Content$wifi;->currHPFirstIndex:Ljava/lang/Integer;

    invoke-virtual {v1}, Ljava/lang/Integer;->intValue()I

    move-result v1

    add-int/lit8 v1, v1, -0x1

    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    sput-object v1, Lcom/konka/systemsetting/common/Content$wifi;->currHPFirstIndex:Ljava/lang/Integer;

    goto :goto_0

    :cond_1
    const/4 v0, 0x0

    goto :goto_0
.end method

.method private static increaseCurrSelectedIndex()Z
    .locals 3

    const/4 v0, 0x1

    sget-object v1, Lcom/konka/systemsetting/common/Content$wifi;->currSelectIndex:Ljava/lang/Integer;

    invoke-virtual {v1}, Ljava/lang/Integer;->intValue()I

    move-result v1

    sget-object v2, Lcom/konka/systemsetting/common/Content$wifi;->HPCounter:Ljava/lang/Integer;

    invoke-virtual {v2}, Ljava/lang/Integer;->intValue()I

    move-result v2

    add-int/lit8 v2, v2, -0x1

    if-ge v1, v2, :cond_0

    sget-object v1, Lcom/konka/systemsetting/common/Content$wifi;->currSelectIndex:Ljava/lang/Integer;

    invoke-virtual {v1}, Ljava/lang/Integer;->intValue()I

    move-result v1

    add-int/lit8 v1, v1, 0x1

    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    sput-object v1, Lcom/konka/systemsetting/common/Content$wifi;->currSelectIndex:Ljava/lang/Integer;

    :goto_0
    return v0

    :cond_0
    sget-object v1, Lcom/konka/systemsetting/common/Content$wifi;->currHPFirstIndex:Ljava/lang/Integer;

    invoke-virtual {v1}, Ljava/lang/Integer;->intValue()I

    move-result v1

    sget-object v2, Lcom/konka/systemsetting/common/Content$wifi;->HPCounter:Ljava/lang/Integer;

    invoke-virtual {v2}, Ljava/lang/Integer;->intValue()I

    move-result v2

    add-int/2addr v1, v2

    sget-object v2, Lcom/konka/systemsetting/common/Content$wifi;->HPNames:[Ljava/lang/CharSequence;

    array-length v2, v2

    add-int/lit8 v2, v2, -0x1

    if-gt v1, v2, :cond_1

    sget-object v1, Lcom/konka/systemsetting/common/Content$wifi;->currHPFirstIndex:Ljava/lang/Integer;

    invoke-virtual {v1}, Ljava/lang/Integer;->intValue()I

    move-result v1

    add-int/lit8 v1, v1, 0x1

    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    sput-object v1, Lcom/konka/systemsetting/common/Content$wifi;->currHPFirstIndex:Ljava/lang/Integer;

    goto :goto_0

    :cond_1
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public static nextHotspot()Z
    .locals 1

    invoke-static {}, Lcom/konka/systemsetting/common/Content$wifi;->increaseCurrSelectedIndex()Z

    move-result v0

    return v0
.end method

.method public static preHotspot()Z
    .locals 1

    invoke-static {}, Lcom/konka/systemsetting/common/Content$wifi;->decreaseCurrSelectedIndex()Z

    move-result v0

    return v0
.end method
