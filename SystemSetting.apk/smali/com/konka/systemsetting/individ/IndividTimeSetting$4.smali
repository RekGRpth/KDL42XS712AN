.class Lcom/konka/systemsetting/individ/IndividTimeSetting$4;
.super Landroid/content/BroadcastReceiver;
.source "IndividTimeSetting.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/konka/systemsetting/individ/IndividTimeSetting;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/konka/systemsetting/individ/IndividTimeSetting;


# direct methods
.method constructor <init>(Lcom/konka/systemsetting/individ/IndividTimeSetting;)V
    .locals 0

    iput-object p1, p0, Lcom/konka/systemsetting/individ/IndividTimeSetting$4;->this$0:Lcom/konka/systemsetting/individ/IndividTimeSetting;

    invoke-direct {p0}, Landroid/content/BroadcastReceiver;-><init>()V

    return-void
.end method


# virtual methods
.method public onReceive(Landroid/content/Context;Landroid/content/Intent;)V
    .locals 6
    .param p1    # Landroid/content/Context;
    .param p2    # Landroid/content/Intent;

    invoke-virtual {p2}, Landroid/content/Intent;->getAction()Ljava/lang/String;

    move-result-object v3

    const-string v4, "CHANGE_TIME_FROM_DTV"

    invoke-virtual {v3, v4}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-eqz v3, :cond_0

    const-string v3, "millis"

    const-wide/16 v4, 0x0

    invoke-virtual {p2, v3, v4, v5}, Landroid/content/Intent;->getLongExtra(Ljava/lang/String;J)J

    move-result-wide v1

    iget-object v3, p0, Lcom/konka/systemsetting/individ/IndividTimeSetting$4;->this$0:Lcom/konka/systemsetting/individ/IndividTimeSetting;

    # invokes: Lcom/konka/systemsetting/individ/IndividTimeSetting;->setTimeMillis(J)V
    invoke-static {v3, v1, v2}, Lcom/konka/systemsetting/individ/IndividTimeSetting;->access$4(Lcom/konka/systemsetting/individ/IndividTimeSetting;J)V

    :cond_0
    invoke-virtual {p2}, Landroid/content/Intent;->getAction()Ljava/lang/String;

    move-result-object v3

    const-string v4, "android.intent.action.TIME_SET"

    invoke-virtual {v3, v4}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-eqz v3, :cond_1

    const-string v3, "\n\n **********mDateTimeReceiver receive message android.intent.action.TIME_SET\n\n "

    invoke-static {v3}, Lcom/konka/debuginfo/logPrint;->Debug(Ljava/lang/String;)V

    :cond_1
    const-string v3, "!!!!!!!!!!!!!!!!!!!!!!!!!!onResume refresh updateAutoTimeUI!!!!!!!!!!!!!!!!!!!"

    invoke-static {v3}, Lcom/konka/debuginfo/logPrint;->Debug(Ljava/lang/String;)V

    iget-object v3, p0, Lcom/konka/systemsetting/individ/IndividTimeSetting$4;->this$0:Lcom/konka/systemsetting/individ/IndividTimeSetting;

    # invokes: Lcom/konka/systemsetting/individ/IndividTimeSetting;->updateAutoTimeUI()V
    invoke-static {v3}, Lcom/konka/systemsetting/individ/IndividTimeSetting;->access$5(Lcom/konka/systemsetting/individ/IndividTimeSetting;)V

    iget-object v3, p0, Lcom/konka/systemsetting/individ/IndividTimeSetting$4;->this$0:Lcom/konka/systemsetting/individ/IndividTimeSetting;

    iget-object v4, p0, Lcom/konka/systemsetting/individ/IndividTimeSetting$4;->this$0:Lcom/konka/systemsetting/individ/IndividTimeSetting;

    # getter for: Lcom/konka/systemsetting/individ/IndividTimeSetting;->mContext:Lcom/konka/systemsetting/MainActivity;
    invoke-static {v4}, Lcom/konka/systemsetting/individ/IndividTimeSetting;->access$6(Lcom/konka/systemsetting/individ/IndividTimeSetting;)Lcom/konka/systemsetting/MainActivity;

    move-result-object v4

    invoke-virtual {v3, v4}, Lcom/konka/systemsetting/individ/IndividTimeSetting;->updateTimeAndDateDisplay(Landroid/content/Context;)V

    :try_start_0
    new-instance v3, Ljava/lang/StringBuilder;

    const-string v4, "xxxxx==>timemanager before setTimeZone :"

    invoke-direct {v3, v4}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    iget-object v4, p0, Lcom/konka/systemsetting/individ/IndividTimeSetting$4;->this$0:Lcom/konka/systemsetting/individ/IndividTimeSetting;

    # getter for: Lcom/konka/systemsetting/individ/IndividTimeSetting;->tMgr:Lcom/mstar/android/tvapi/common/TimerManager;
    invoke-static {v4}, Lcom/konka/systemsetting/individ/IndividTimeSetting;->access$7(Lcom/konka/systemsetting/individ/IndividTimeSetting;)Lcom/mstar/android/tvapi/common/TimerManager;

    move-result-object v4

    invoke-virtual {v4}, Lcom/mstar/android/tvapi/common/TimerManager;->getClkTime()Landroid/text/format/Time;

    move-result-object v4

    const/4 v5, 0x1

    invoke-virtual {v4, v5}, Landroid/text/format/Time;->toMillis(Z)J

    move-result-wide v4

    invoke-virtual {v3, v4, v5}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v3}, Lcom/konka/debuginfo/logPrint;->Debug(Ljava/lang/String;)V

    new-instance v3, Ljava/lang/StringBuilder;

    const-string v4, "xxxxx==>timemanager before setTimeZone :"

    invoke-direct {v3, v4}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    iget-object v4, p0, Lcom/konka/systemsetting/individ/IndividTimeSetting$4;->this$0:Lcom/konka/systemsetting/individ/IndividTimeSetting;

    # getter for: Lcom/konka/systemsetting/individ/IndividTimeSetting;->tMgr:Lcom/mstar/android/tvapi/common/TimerManager;
    invoke-static {v4}, Lcom/konka/systemsetting/individ/IndividTimeSetting;->access$7(Lcom/konka/systemsetting/individ/IndividTimeSetting;)Lcom/mstar/android/tvapi/common/TimerManager;

    move-result-object v4

    invoke-virtual {v4}, Lcom/mstar/android/tvapi/common/TimerManager;->getClkTime()Landroid/text/format/Time;

    move-result-object v4

    const/4 v5, 0x0

    invoke-virtual {v4, v5}, Landroid/text/format/Time;->toMillis(Z)J

    move-result-wide v4

    invoke-virtual {v3, v4, v5}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v3}, Lcom/konka/debuginfo/logPrint;->Debug(Ljava/lang/String;)V
    :try_end_0
    .catch Lcom/mstar/android/tvapi/common/exception/TvCommonException; {:try_start_0 .. :try_end_0} :catch_0

    :goto_0
    iget-object v3, p0, Lcom/konka/systemsetting/individ/IndividTimeSetting$4;->this$0:Lcom/konka/systemsetting/individ/IndividTimeSetting;

    # invokes: Lcom/konka/systemsetting/individ/IndividTimeSetting;->updateTimeZone()V
    invoke-static {v3}, Lcom/konka/systemsetting/individ/IndividTimeSetting;->access$8(Lcom/konka/systemsetting/individ/IndividTimeSetting;)V

    :try_start_1
    new-instance v3, Ljava/lang/StringBuilder;

    const-string v4, "xxxxx==>timemanager after setTimeZone :"

    invoke-direct {v3, v4}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    iget-object v4, p0, Lcom/konka/systemsetting/individ/IndividTimeSetting$4;->this$0:Lcom/konka/systemsetting/individ/IndividTimeSetting;

    # getter for: Lcom/konka/systemsetting/individ/IndividTimeSetting;->tMgr:Lcom/mstar/android/tvapi/common/TimerManager;
    invoke-static {v4}, Lcom/konka/systemsetting/individ/IndividTimeSetting;->access$7(Lcom/konka/systemsetting/individ/IndividTimeSetting;)Lcom/mstar/android/tvapi/common/TimerManager;

    move-result-object v4

    invoke-virtual {v4}, Lcom/mstar/android/tvapi/common/TimerManager;->getClkTime()Landroid/text/format/Time;

    move-result-object v4

    const/4 v5, 0x1

    invoke-virtual {v4, v5}, Landroid/text/format/Time;->toMillis(Z)J

    move-result-wide v4

    invoke-virtual {v3, v4, v5}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v3}, Lcom/konka/debuginfo/logPrint;->Debug(Ljava/lang/String;)V

    new-instance v3, Ljava/lang/StringBuilder;

    const-string v4, "xxxxx==>timemanager after setTimeZone :"

    invoke-direct {v3, v4}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    iget-object v4, p0, Lcom/konka/systemsetting/individ/IndividTimeSetting$4;->this$0:Lcom/konka/systemsetting/individ/IndividTimeSetting;

    # getter for: Lcom/konka/systemsetting/individ/IndividTimeSetting;->tMgr:Lcom/mstar/android/tvapi/common/TimerManager;
    invoke-static {v4}, Lcom/konka/systemsetting/individ/IndividTimeSetting;->access$7(Lcom/konka/systemsetting/individ/IndividTimeSetting;)Lcom/mstar/android/tvapi/common/TimerManager;

    move-result-object v4

    invoke-virtual {v4}, Lcom/mstar/android/tvapi/common/TimerManager;->getClkTime()Landroid/text/format/Time;

    move-result-object v4

    const/4 v5, 0x0

    invoke-virtual {v4, v5}, Landroid/text/format/Time;->toMillis(Z)J

    move-result-wide v4

    invoke-virtual {v3, v4, v5}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v3}, Lcom/konka/debuginfo/logPrint;->Debug(Ljava/lang/String;)V
    :try_end_1
    .catch Lcom/mstar/android/tvapi/common/exception/TvCommonException; {:try_start_1 .. :try_end_1} :catch_1

    :goto_1
    const-string v3, "SystemSetting"

    new-instance v4, Ljava/lang/StringBuilder;

    const-string v5, "================>>>> DATE TIME ACTION = "

    invoke-direct {v4, v5}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {p2}, Landroid/content/Intent;->getAction()Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-static {v3, v4}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    return-void

    :catch_0
    move-exception v0

    invoke-virtual {v0}, Lcom/mstar/android/tvapi/common/exception/TvCommonException;->printStackTrace()V

    goto :goto_0

    :catch_1
    move-exception v0

    invoke-virtual {v0}, Lcom/mstar/android/tvapi/common/exception/TvCommonException;->printStackTrace()V

    goto :goto_1
.end method
