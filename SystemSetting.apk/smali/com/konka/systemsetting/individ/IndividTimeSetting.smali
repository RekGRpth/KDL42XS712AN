.class public Lcom/konka/systemsetting/individ/IndividTimeSetting;
.super Ljava/lang/Object;
.source "IndividTimeSetting.java"

# interfaces
.implements Landroid/app/DatePickerDialog$OnDateSetListener;
.implements Landroid/app/TimePickerDialog$OnTimeSetListener;
.implements Landroid/view/View$OnClickListener;
.implements Landroid/view/View$OnFocusChangeListener;
.implements Landroid/view/View$OnKeyListener;


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/konka/systemsetting/individ/IndividTimeSetting$ITime;,
        Lcom/konka/systemsetting/individ/IndividTimeSetting$MyComparator;,
        Lcom/konka/systemsetting/individ/IndividTimeSetting$MyHandler;
    }
.end annotation


# static fields
.field private static final BOOT_TIME_PICKER_DLG:I = 0x4

.field private static final DATE_PICKER_DLG:I = 0x1

.field private static final HALT_TIME_PICKER_DLG:I = 0x3

.field private static final HOURS_1:I = 0x36ee80

.field private static final HOURS_12:Ljava/lang/String; = "12"

.field private static final HOURS_24:Ljava/lang/String; = "24"

.field private static final KEY_DISPLAYNAME:Ljava/lang/String; = "name"

.field private static final KEY_GMT:Ljava/lang/String; = "gmt"

.field private static final KEY_ID:Ljava/lang/String; = "id"

.field private static final KEY_OFFSET:Ljava/lang/String; = "offset"

.field private static final TIMEZONE_SETTING:Ljava/lang/String; = "timeZoneSetting"

.field private static final TIME_PICKER_DLG:I = 0x2

.field public static final USER_SETTINGS:Ljava/lang/String; = "userSettings"

.field private static final XMLTAG_TIMEZONE:Ljava/lang/String; = "timezone"

.field private static m_isBootSwitchOn:Z

.field private static m_isHaltSwitchOn:Z


# instance fields
.field private final TAG:Ljava/lang/String;

.field private adapter:Landroid/widget/SimpleAdapter;

.field private channelDesk:Lcom/konka/kkinterface/tv/ChannelDesk;

.field private commonDesk:Lcom/konka/kkinterface/tv/CommonDesk;

.field private epgDesk:Lcom/konka/kkinterface/tv/EpgDesk;

.field private getTimer:Ljava/lang/Thread;

.field private inputSource:Lcom/mstar/android/tvapi/common/vo/TvOsType$EnumInputSource;

.field private isDTRUnregisted:Z

.field protected itemAutoAdjust:Landroid/widget/LinearLayout;

.field protected itemBootChannel:Landroid/widget/LinearLayout;

.field protected itemBootSwitch:Landroid/widget/LinearLayout;

.field protected itemBootTime:Landroid/widget/LinearLayout;

.field protected itemDateSetting:Landroid/widget/LinearLayout;

.field protected itemHaltSwitch:Landroid/widget/LinearLayout;

.field protected itemHaltTime:Landroid/widget/LinearLayout;

.field protected itemTimeSetting:Landroid/widget/LinearLayout;

.field protected itemTimeZoneSetting:Landroid/widget/LinearLayout;

.field private mArrCurrent:[Ljava/lang/CharSequence;

.field private mContext:Lcom/konka/systemsetting/MainActivity;

.field private mCurrNTPServerIdx:I

.field private mDateTimeReceiver:Landroid/content/BroadcastReceiver;

.field private mEnumOnTimeSource:[Lcom/mstar/android/tvapi/common/vo/EnumTimeOnTimerSource;

.field private mInputSourceName:[Ljava/lang/String;

.field private m_iAutoTimeSelectIndex:Ljava/lang/Integer;

.field private m_iBootChannelIndex:Ljava/lang/Integer;

.field private m_iTimeZoneSelectIndex:Ljava/lang/Integer;

.field private m_isDtvTime:Ljava/lang/Integer;

.field private myHandler:Lcom/konka/systemsetting/individ/IndividTimeSetting$MyHandler;

.field private nArrCurrent:[Ljava/lang/CharSequence;

.field private onBootTimeSetListener:Landroid/app/TimePickerDialog$OnTimeSetListener;

.field private onHalTimeSetListener:Landroid/app/TimePickerDialog$OnTimeSetListener;

.field private serviceProvider:Lcom/konka/kkinterface/tv/TvDeskProvider;

.field private settingDesk:Lcom/konka/kkinterface/tv/SettingDesk;

.field private tMgr:Lcom/mstar/android/tvapi/common/TimerManager;

.field private timeFromNet:J

.field private timerOff:Lcom/mstar/android/tvapi/common/vo/TimerPowerOffModeStatus;

.field private timerOn:Lcom/mstar/android/tvapi/common/vo/TimerPowerOn;

.field private userSettings:Landroid/content/SharedPreferences;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    const/4 v0, 0x1

    sput-boolean v0, Lcom/konka/systemsetting/individ/IndividTimeSetting;->m_isHaltSwitchOn:Z

    sput-boolean v0, Lcom/konka/systemsetting/individ/IndividTimeSetting;->m_isBootSwitchOn:Z

    return-void
.end method

.method public constructor <init>(Lcom/konka/systemsetting/MainActivity;)V
    .locals 3
    .param p1    # Lcom/konka/systemsetting/MainActivity;

    const/4 v2, 0x0

    const/4 v1, 0x0

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    const-string v0, "SystemSetting"

    iput-object v0, p0, Lcom/konka/systemsetting/individ/IndividTimeSetting;->TAG:Ljava/lang/String;

    iput-object v1, p0, Lcom/konka/systemsetting/individ/IndividTimeSetting;->itemAutoAdjust:Landroid/widget/LinearLayout;

    iput-object v1, p0, Lcom/konka/systemsetting/individ/IndividTimeSetting;->itemDateSetting:Landroid/widget/LinearLayout;

    iput-object v1, p0, Lcom/konka/systemsetting/individ/IndividTimeSetting;->itemTimeSetting:Landroid/widget/LinearLayout;

    iput-object v1, p0, Lcom/konka/systemsetting/individ/IndividTimeSetting;->itemTimeZoneSetting:Landroid/widget/LinearLayout;

    iput-object v1, p0, Lcom/konka/systemsetting/individ/IndividTimeSetting;->itemHaltSwitch:Landroid/widget/LinearLayout;

    iput-object v1, p0, Lcom/konka/systemsetting/individ/IndividTimeSetting;->itemHaltTime:Landroid/widget/LinearLayout;

    iput-object v1, p0, Lcom/konka/systemsetting/individ/IndividTimeSetting;->itemBootSwitch:Landroid/widget/LinearLayout;

    iput-object v1, p0, Lcom/konka/systemsetting/individ/IndividTimeSetting;->itemBootTime:Landroid/widget/LinearLayout;

    iput-object v1, p0, Lcom/konka/systemsetting/individ/IndividTimeSetting;->itemBootChannel:Landroid/widget/LinearLayout;

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v0

    iput-object v0, p0, Lcom/konka/systemsetting/individ/IndividTimeSetting;->m_isDtvTime:Ljava/lang/Integer;

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v0

    iput-object v0, p0, Lcom/konka/systemsetting/individ/IndividTimeSetting;->m_iBootChannelIndex:Ljava/lang/Integer;

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v0

    iput-object v0, p0, Lcom/konka/systemsetting/individ/IndividTimeSetting;->m_iTimeZoneSelectIndex:Ljava/lang/Integer;

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v0

    iput-object v0, p0, Lcom/konka/systemsetting/individ/IndividTimeSetting;->m_iAutoTimeSelectIndex:Ljava/lang/Integer;

    iput-object v1, p0, Lcom/konka/systemsetting/individ/IndividTimeSetting;->tMgr:Lcom/mstar/android/tvapi/common/TimerManager;

    iput-object v1, p0, Lcom/konka/systemsetting/individ/IndividTimeSetting;->mArrCurrent:[Ljava/lang/CharSequence;

    iput-object v1, p0, Lcom/konka/systemsetting/individ/IndividTimeSetting;->nArrCurrent:[Ljava/lang/CharSequence;

    iput v2, p0, Lcom/konka/systemsetting/individ/IndividTimeSetting;->mCurrNTPServerIdx:I

    new-instance v0, Lcom/konka/systemsetting/individ/IndividTimeSetting$MyHandler;

    invoke-direct {v0, p0, v1}, Lcom/konka/systemsetting/individ/IndividTimeSetting$MyHandler;-><init>(Lcom/konka/systemsetting/individ/IndividTimeSetting;Lcom/konka/systemsetting/individ/IndividTimeSetting$MyHandler;)V

    iput-object v0, p0, Lcom/konka/systemsetting/individ/IndividTimeSetting;->myHandler:Lcom/konka/systemsetting/individ/IndividTimeSetting$MyHandler;

    new-instance v0, Ljava/lang/Thread;

    new-instance v1, Lcom/konka/systemsetting/individ/IndividTimeSetting$1;

    invoke-direct {v1, p0}, Lcom/konka/systemsetting/individ/IndividTimeSetting$1;-><init>(Lcom/konka/systemsetting/individ/IndividTimeSetting;)V

    invoke-direct {v0, v1}, Ljava/lang/Thread;-><init>(Ljava/lang/Runnable;)V

    iput-object v0, p0, Lcom/konka/systemsetting/individ/IndividTimeSetting;->getTimer:Ljava/lang/Thread;

    new-instance v0, Lcom/konka/systemsetting/individ/IndividTimeSetting$2;

    invoke-direct {v0, p0}, Lcom/konka/systemsetting/individ/IndividTimeSetting$2;-><init>(Lcom/konka/systemsetting/individ/IndividTimeSetting;)V

    iput-object v0, p0, Lcom/konka/systemsetting/individ/IndividTimeSetting;->onHalTimeSetListener:Landroid/app/TimePickerDialog$OnTimeSetListener;

    new-instance v0, Lcom/konka/systemsetting/individ/IndividTimeSetting$3;

    invoke-direct {v0, p0}, Lcom/konka/systemsetting/individ/IndividTimeSetting$3;-><init>(Lcom/konka/systemsetting/individ/IndividTimeSetting;)V

    iput-object v0, p0, Lcom/konka/systemsetting/individ/IndividTimeSetting;->onBootTimeSetListener:Landroid/app/TimePickerDialog$OnTimeSetListener;

    new-instance v0, Lcom/konka/systemsetting/individ/IndividTimeSetting$4;

    invoke-direct {v0, p0}, Lcom/konka/systemsetting/individ/IndividTimeSetting$4;-><init>(Lcom/konka/systemsetting/individ/IndividTimeSetting;)V

    iput-object v0, p0, Lcom/konka/systemsetting/individ/IndividTimeSetting;->mDateTimeReceiver:Landroid/content/BroadcastReceiver;

    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/konka/systemsetting/individ/IndividTimeSetting;->isDTRUnregisted:Z

    const-wide/16 v0, 0x0

    iput-wide v0, p0, Lcom/konka/systemsetting/individ/IndividTimeSetting;->timeFromNet:J

    iput-object p1, p0, Lcom/konka/systemsetting/individ/IndividTimeSetting;->mContext:Lcom/konka/systemsetting/MainActivity;

    invoke-static {}, Lcom/mstar/android/tvapi/common/TvManager;->getInstance()Lcom/mstar/android/tvapi/common/TvManager;

    move-result-object v0

    if-eqz v0, :cond_0

    invoke-static {}, Lcom/mstar/android/tvapi/common/TvManager;->getInstance()Lcom/mstar/android/tvapi/common/TvManager;

    move-result-object v0

    invoke-virtual {v0}, Lcom/mstar/android/tvapi/common/TvManager;->getTimerManager()Lcom/mstar/android/tvapi/common/TimerManager;

    move-result-object v0

    iput-object v0, p0, Lcom/konka/systemsetting/individ/IndividTimeSetting;->tMgr:Lcom/mstar/android/tvapi/common/TimerManager;

    :cond_0
    iget-object v0, p0, Lcom/konka/systemsetting/individ/IndividTimeSetting;->tMgr:Lcom/mstar/android/tvapi/common/TimerManager;

    if-nez v0, :cond_1

    const-string v0, "Individ_Time_Settings: TimerManager is null !!!"

    invoke-direct {p0, v0}, Lcom/konka/systemsetting/individ/IndividTimeSetting;->debug(Ljava/lang/String;)V

    :goto_0
    iget-object v0, p0, Lcom/konka/systemsetting/individ/IndividTimeSetting;->mContext:Lcom/konka/systemsetting/MainActivity;

    invoke-static {v0}, Lcom/konka/kkimplements/tv/TvDeskProviderImpl;->getInstance(Landroid/content/Context;)Lcom/konka/kkinterface/tv/TvDeskProvider;

    move-result-object v0

    iput-object v0, p0, Lcom/konka/systemsetting/individ/IndividTimeSetting;->serviceProvider:Lcom/konka/kkinterface/tv/TvDeskProvider;

    iget-object v0, p0, Lcom/konka/systemsetting/individ/IndividTimeSetting;->serviceProvider:Lcom/konka/kkinterface/tv/TvDeskProvider;

    invoke-interface {v0}, Lcom/konka/kkinterface/tv/TvDeskProvider;->getCommonManagerInstance()Lcom/konka/kkinterface/tv/CommonDesk;

    move-result-object v0

    iput-object v0, p0, Lcom/konka/systemsetting/individ/IndividTimeSetting;->commonDesk:Lcom/konka/kkinterface/tv/CommonDesk;

    iget-object v0, p0, Lcom/konka/systemsetting/individ/IndividTimeSetting;->serviceProvider:Lcom/konka/kkinterface/tv/TvDeskProvider;

    invoke-interface {v0}, Lcom/konka/kkinterface/tv/TvDeskProvider;->getChannelManagerInstance()Lcom/konka/kkinterface/tv/ChannelDesk;

    move-result-object v0

    iput-object v0, p0, Lcom/konka/systemsetting/individ/IndividTimeSetting;->channelDesk:Lcom/konka/kkinterface/tv/ChannelDesk;

    iget-object v0, p0, Lcom/konka/systemsetting/individ/IndividTimeSetting;->serviceProvider:Lcom/konka/kkinterface/tv/TvDeskProvider;

    invoke-interface {v0}, Lcom/konka/kkinterface/tv/TvDeskProvider;->getSettingManagerInstance()Lcom/konka/kkinterface/tv/SettingDesk;

    move-result-object v0

    iput-object v0, p0, Lcom/konka/systemsetting/individ/IndividTimeSetting;->settingDesk:Lcom/konka/kkinterface/tv/SettingDesk;

    iget-object v0, p0, Lcom/konka/systemsetting/individ/IndividTimeSetting;->commonDesk:Lcom/konka/kkinterface/tv/CommonDesk;

    invoke-interface {v0}, Lcom/konka/kkinterface/tv/CommonDesk;->GetCurrentInputSource()Lcom/mstar/android/tvapi/common/vo/TvOsType$EnumInputSource;

    move-result-object v0

    iput-object v0, p0, Lcom/konka/systemsetting/individ/IndividTimeSetting;->inputSource:Lcom/mstar/android/tvapi/common/vo/TvOsType$EnumInputSource;

    iget-object v0, p0, Lcom/konka/systemsetting/individ/IndividTimeSetting;->serviceProvider:Lcom/konka/kkinterface/tv/TvDeskProvider;

    invoke-interface {v0}, Lcom/konka/kkinterface/tv/TvDeskProvider;->getEpgManagerInstance()Lcom/konka/kkinterface/tv/EpgDesk;

    move-result-object v0

    iput-object v0, p0, Lcom/konka/systemsetting/individ/IndividTimeSetting;->epgDesk:Lcom/konka/kkinterface/tv/EpgDesk;

    invoke-direct {p0}, Lcom/konka/systemsetting/individ/IndividTimeSetting;->dataConfigurate()V

    invoke-direct {p0}, Lcom/konka/systemsetting/individ/IndividTimeSetting;->initAll()V

    invoke-virtual {p0}, Lcom/konka/systemsetting/individ/IndividTimeSetting;->onResume()V

    return-void

    :cond_1
    const-string v0, "Individ_Time_Settings: TimerManager is good in use!"

    invoke-direct {p0, v0}, Lcom/konka/systemsetting/individ/IndividTimeSetting;->debug(Ljava/lang/String;)V

    goto :goto_0
.end method

.method static synthetic access$0(Lcom/konka/systemsetting/individ/IndividTimeSetting;)V
    .locals 0

    invoke-direct {p0}, Lcom/konka/systemsetting/individ/IndividTimeSetting;->initDateTime()V

    return-void
.end method

.method static synthetic access$1(Lcom/konka/systemsetting/individ/IndividTimeSetting;)Lcom/konka/systemsetting/individ/IndividTimeSetting$MyHandler;
    .locals 1

    iget-object v0, p0, Lcom/konka/systemsetting/individ/IndividTimeSetting;->myHandler:Lcom/konka/systemsetting/individ/IndividTimeSetting$MyHandler;

    return-object v0
.end method

.method static synthetic access$10(Lcom/konka/systemsetting/individ/IndividTimeSetting;)V
    .locals 0

    invoke-direct {p0}, Lcom/konka/systemsetting/individ/IndividTimeSetting;->refreshUISate()V

    return-void
.end method

.method static synthetic access$11(Lcom/konka/systemsetting/individ/IndividTimeSetting;)Ljava/lang/Integer;
    .locals 1

    iget-object v0, p0, Lcom/konka/systemsetting/individ/IndividTimeSetting;->m_iAutoTimeSelectIndex:Ljava/lang/Integer;

    return-object v0
.end method

.method static synthetic access$12(Lcom/konka/systemsetting/individ/IndividTimeSetting;Ljava/lang/Integer;)V
    .locals 0

    iput-object p1, p0, Lcom/konka/systemsetting/individ/IndividTimeSetting;->m_iBootChannelIndex:Ljava/lang/Integer;

    return-void
.end method

.method static synthetic access$13(Lcom/konka/systemsetting/individ/IndividTimeSetting;)V
    .locals 0

    invoke-direct {p0}, Lcom/konka/systemsetting/individ/IndividTimeSetting;->updateBootChannelUI()V

    return-void
.end method

.method static synthetic access$14(Lcom/konka/systemsetting/individ/IndividTimeSetting;)Landroid/content/SharedPreferences;
    .locals 1

    iget-object v0, p0, Lcom/konka/systemsetting/individ/IndividTimeSetting;->userSettings:Landroid/content/SharedPreferences;

    return-object v0
.end method

.method static synthetic access$15(Lcom/konka/systemsetting/individ/IndividTimeSetting;Ljava/lang/Integer;)V
    .locals 0

    iput-object p1, p0, Lcom/konka/systemsetting/individ/IndividTimeSetting;->m_iTimeZoneSelectIndex:Ljava/lang/Integer;

    return-void
.end method

.method static synthetic access$16(Lcom/konka/systemsetting/individ/IndividTimeSetting;)V
    .locals 0

    invoke-direct {p0}, Lcom/konka/systemsetting/individ/IndividTimeSetting;->updateTimeZoneSelectUI()V

    return-void
.end method

.method static synthetic access$2(Lcom/konka/systemsetting/individ/IndividTimeSetting;)V
    .locals 0

    invoke-direct {p0}, Lcom/konka/systemsetting/individ/IndividTimeSetting;->updateHaltTimeUI()V

    return-void
.end method

.method static synthetic access$3(Lcom/konka/systemsetting/individ/IndividTimeSetting;)V
    .locals 0

    invoke-direct {p0}, Lcom/konka/systemsetting/individ/IndividTimeSetting;->updateBootTimeUI()V

    return-void
.end method

.method static synthetic access$4(Lcom/konka/systemsetting/individ/IndividTimeSetting;J)V
    .locals 0

    invoke-direct {p0, p1, p2}, Lcom/konka/systemsetting/individ/IndividTimeSetting;->setTimeMillis(J)V

    return-void
.end method

.method static synthetic access$5(Lcom/konka/systemsetting/individ/IndividTimeSetting;)V
    .locals 0

    invoke-direct {p0}, Lcom/konka/systemsetting/individ/IndividTimeSetting;->updateAutoTimeUI()V

    return-void
.end method

.method static synthetic access$6(Lcom/konka/systemsetting/individ/IndividTimeSetting;)Lcom/konka/systemsetting/MainActivity;
    .locals 1

    iget-object v0, p0, Lcom/konka/systemsetting/individ/IndividTimeSetting;->mContext:Lcom/konka/systemsetting/MainActivity;

    return-object v0
.end method

.method static synthetic access$7(Lcom/konka/systemsetting/individ/IndividTimeSetting;)Lcom/mstar/android/tvapi/common/TimerManager;
    .locals 1

    iget-object v0, p0, Lcom/konka/systemsetting/individ/IndividTimeSetting;->tMgr:Lcom/mstar/android/tvapi/common/TimerManager;

    return-object v0
.end method

.method static synthetic access$8(Lcom/konka/systemsetting/individ/IndividTimeSetting;)V
    .locals 0

    invoke-direct {p0}, Lcom/konka/systemsetting/individ/IndividTimeSetting;->updateTimeZone()V

    return-void
.end method

.method static synthetic access$9(Lcom/konka/systemsetting/individ/IndividTimeSetting;)V
    .locals 0

    invoke-direct {p0}, Lcom/konka/systemsetting/individ/IndividTimeSetting;->getInitData()V

    return-void
.end method

.method private static addItem(Ljava/util/List;Ljava/lang/String;Ljava/lang/String;J)V
    .locals 9
    .param p1    # Ljava/lang/String;
    .param p2    # Ljava/lang/String;
    .param p3    # J
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List",
            "<",
            "Ljava/util/HashMap",
            "<",
            "Ljava/lang/String;",
            "Ljava/lang/Object;",
            ">;>;",
            "Ljava/lang/String;",
            "Ljava/lang/String;",
            "J)V"
        }
    .end annotation

    new-instance v0, Ljava/util/HashMap;

    invoke-direct {v0}, Ljava/util/HashMap;-><init>()V

    const-string v6, "id"

    invoke-virtual {v0, v6, p1}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    invoke-static {p1}, Ljava/util/TimeZone;->getTimeZone(Ljava/lang/String;)Ljava/util/TimeZone;

    move-result-object v5

    invoke-virtual {v5, p3, p4}, Ljava/util/TimeZone;->getOffset(J)I

    move-result v3

    invoke-static {v3}, Ljava/lang/Math;->abs(I)I

    move-result v4

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v6, "GMT"

    invoke-virtual {v2, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    if-gez v3, :cond_1

    const/16 v6, 0x2d

    invoke-virtual {v2, v6}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;

    :goto_0
    const v6, 0x36ee80

    div-int v6, v4, v6

    invoke-virtual {v2, v6}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    const/16 v6, 0x3a

    invoke-virtual {v2, v6}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;

    const v6, 0xea60

    div-int v1, v4, v6

    rem-int/lit8 v1, v1, 0x3c

    const/16 v6, 0xa

    if-ge v1, v6, :cond_0

    const/16 v6, 0x30

    invoke-virtual {v2, v6}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;

    :cond_0
    invoke-virtual {v2, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    const-string v6, "name"

    new-instance v7, Ljava/lang/StringBuilder;

    invoke-static {p2}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v8

    invoke-direct {v7, v8}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    const-string v8, "\n"

    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v8

    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    invoke-virtual {v7}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v7

    invoke-virtual {v0, v6, v7}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    const-string v6, "gmt"

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v7

    invoke-virtual {v0, v6, v7}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    const-string v6, "offset"

    invoke-static {v3}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v7

    invoke-virtual {v0, v6, v7}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    invoke-interface {p0, v0}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    return-void

    :cond_1
    const/16 v6, 0x2b

    invoke-virtual {v2, v6}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;

    goto :goto_0
.end method

.method private changeSwitchState(I)V
    .locals 3
    .param p1    # I

    const/4 v1, 0x1

    const/4 v0, 0x0

    packed-switch p1, :pswitch_data_0

    :pswitch_0
    const-string v0, "DateTimeSetting: changeSwitchState Error, on such id!"

    invoke-static {v0}, Lcom/konka/debuginfo/logPrint;->Debug(Ljava/lang/String;)V

    :goto_0
    return-void

    :pswitch_1
    sget-boolean v2, Lcom/konka/systemsetting/individ/IndividTimeSetting;->m_isHaltSwitchOn:Z

    if-eqz v2, :cond_0

    :goto_1
    sput-boolean v0, Lcom/konka/systemsetting/individ/IndividTimeSetting;->m_isHaltSwitchOn:Z

    invoke-virtual {p0}, Lcom/konka/systemsetting/individ/IndividTimeSetting;->updateHaltSwitch()V

    invoke-direct {p0}, Lcom/konka/systemsetting/individ/IndividTimeSetting;->updateHaltSwitchUI()V

    const-string v0, "DateTimeSetting: changeSwitchState_haltswitch!"

    invoke-static {v0}, Lcom/konka/debuginfo/logPrint;->Debug(Ljava/lang/String;)V

    goto :goto_0

    :cond_0
    move v0, v1

    goto :goto_1

    :pswitch_2
    sget-boolean v2, Lcom/konka/systemsetting/individ/IndividTimeSetting;->m_isBootSwitchOn:Z

    if-eqz v2, :cond_1

    :goto_2
    sput-boolean v0, Lcom/konka/systemsetting/individ/IndividTimeSetting;->m_isBootSwitchOn:Z

    invoke-virtual {p0}, Lcom/konka/systemsetting/individ/IndividTimeSetting;->updateBootSwitch()V

    invoke-direct {p0}, Lcom/konka/systemsetting/individ/IndividTimeSetting;->updateBootSwitchUI()V

    const-string v0, "DateTimeSetting: changeSwitchState_bootswitch!"

    invoke-static {v0}, Lcom/konka/debuginfo/logPrint;->Debug(Ljava/lang/String;)V

    goto :goto_0

    :cond_1
    move v0, v1

    goto :goto_2

    nop

    :pswitch_data_0
    .packed-switch 0x7f09002a
        :pswitch_1    # com.konka.systemsetting.R.id.sys_individ_timing_item_haltswitch
        :pswitch_0    # com.konka.systemsetting.R.id.sys_individ_timing_item_halttime
        :pswitch_2    # com.konka.systemsetting.R.id.sys_individ_timing_item_bootswitch
    .end packed-switch
.end method

.method private dataConfigurate()V
    .locals 15

    const v13, 0x7f070001    # com.konka.systemsetting.R.array.str_arr_individ_timing_boot_channel

    const/4 v11, 0x0

    const/4 v14, 0x1

    iget-object v10, p0, Lcom/konka/systemsetting/individ/IndividTimeSetting;->commonDesk:Lcom/konka/kkinterface/tv/CommonDesk;

    invoke-interface {v10}, Lcom/konka/kkinterface/tv/CommonDesk;->getInputSourceInfo()Lcom/konka/kkinterface/tv/CommonDesk$InputSourceInfo;

    move-result-object v10

    iget v2, v10, Lcom/konka/kkinterface/tv/CommonDesk$InputSourceInfo;->sourceCountDTV:I

    iget-object v10, p0, Lcom/konka/systemsetting/individ/IndividTimeSetting;->commonDesk:Lcom/konka/kkinterface/tv/CommonDesk;

    invoke-interface {v10}, Lcom/konka/kkinterface/tv/CommonDesk;->getInputSourceInfo()Lcom/konka/kkinterface/tv/CommonDesk$InputSourceInfo;

    move-result-object v10

    iget v0, v10, Lcom/konka/kkinterface/tv/CommonDesk$InputSourceInfo;->sourceCountATV:I

    iget-object v10, p0, Lcom/konka/systemsetting/individ/IndividTimeSetting;->commonDesk:Lcom/konka/kkinterface/tv/CommonDesk;

    invoke-interface {v10}, Lcom/konka/kkinterface/tv/CommonDesk;->getInputSourceInfo()Lcom/konka/kkinterface/tv/CommonDesk$InputSourceInfo;

    move-result-object v10

    iget v1, v10, Lcom/konka/kkinterface/tv/CommonDesk$InputSourceInfo;->sourceCountAV:I

    iget-object v10, p0, Lcom/konka/systemsetting/individ/IndividTimeSetting;->commonDesk:Lcom/konka/kkinterface/tv/CommonDesk;

    invoke-interface {v10}, Lcom/konka/kkinterface/tv/CommonDesk;->getInputSourceInfo()Lcom/konka/kkinterface/tv/CommonDesk$InputSourceInfo;

    move-result-object v10

    iget v6, v10, Lcom/konka/kkinterface/tv/CommonDesk$InputSourceInfo;->sourceCountYPbPr:I

    iget-object v10, p0, Lcom/konka/systemsetting/individ/IndividTimeSetting;->commonDesk:Lcom/konka/kkinterface/tv/CommonDesk;

    invoke-interface {v10}, Lcom/konka/kkinterface/tv/CommonDesk;->getInputSourceInfo()Lcom/konka/kkinterface/tv/CommonDesk$InputSourceInfo;

    move-result-object v10

    iget v5, v10, Lcom/konka/kkinterface/tv/CommonDesk$InputSourceInfo;->sourceCountVGA:I

    iget-object v10, p0, Lcom/konka/systemsetting/individ/IndividTimeSetting;->commonDesk:Lcom/konka/kkinterface/tv/CommonDesk;

    invoke-interface {v10}, Lcom/konka/kkinterface/tv/CommonDesk;->getInputSourceInfo()Lcom/konka/kkinterface/tv/CommonDesk$InputSourceInfo;

    move-result-object v10

    iget v3, v10, Lcom/konka/kkinterface/tv/CommonDesk$InputSourceInfo;->sourceCountHDMI:I

    add-int v10, v2, v0

    add-int/2addr v10, v1

    add-int/2addr v10, v6

    add-int/2addr v10, v5

    add-int v4, v10, v3

    new-array v10, v4, [Ljava/lang/String;

    iput-object v10, p0, Lcom/konka/systemsetting/individ/IndividTimeSetting;->mInputSourceName:[Ljava/lang/String;

    new-array v10, v4, [Lcom/mstar/android/tvapi/common/vo/EnumTimeOnTimerSource;

    iput-object v10, p0, Lcom/konka/systemsetting/individ/IndividTimeSetting;->mEnumOnTimeSource:[Lcom/mstar/android/tvapi/common/vo/EnumTimeOnTimerSource;

    const/4 v7, 0x0

    if-ne v2, v14, :cond_0

    iget-object v10, p0, Lcom/konka/systemsetting/individ/IndividTimeSetting;->mInputSourceName:[Ljava/lang/String;

    iget-object v12, p0, Lcom/konka/systemsetting/individ/IndividTimeSetting;->mContext:Lcom/konka/systemsetting/MainActivity;

    invoke-virtual {v12}, Lcom/konka/systemsetting/MainActivity;->getResources()Landroid/content/res/Resources;

    move-result-object v12

    invoke-virtual {v12, v13}, Landroid/content/res/Resources;->getStringArray(I)[Ljava/lang/String;

    move-result-object v12

    aget-object v12, v12, v11

    aput-object v12, v10, v7

    iget-object v10, p0, Lcom/konka/systemsetting/individ/IndividTimeSetting;->mEnumOnTimeSource:[Lcom/mstar/android/tvapi/common/vo/EnumTimeOnTimerSource;

    sget-object v12, Lcom/mstar/android/tvapi/common/vo/EnumTimeOnTimerSource;->EN_Time_OnTimer_Source_DTV:Lcom/mstar/android/tvapi/common/vo/EnumTimeOnTimerSource;

    aput-object v12, v10, v7

    add-int/lit8 v7, v7, 0x1

    :cond_0
    if-ne v0, v14, :cond_1

    iget-object v10, p0, Lcom/konka/systemsetting/individ/IndividTimeSetting;->mInputSourceName:[Ljava/lang/String;

    iget-object v12, p0, Lcom/konka/systemsetting/individ/IndividTimeSetting;->mContext:Lcom/konka/systemsetting/MainActivity;

    invoke-virtual {v12}, Lcom/konka/systemsetting/MainActivity;->getResources()Landroid/content/res/Resources;

    move-result-object v12

    invoke-virtual {v12, v13}, Landroid/content/res/Resources;->getStringArray(I)[Ljava/lang/String;

    move-result-object v12

    aget-object v12, v12, v14

    aput-object v12, v10, v7

    iget-object v10, p0, Lcom/konka/systemsetting/individ/IndividTimeSetting;->mEnumOnTimeSource:[Lcom/mstar/android/tvapi/common/vo/EnumTimeOnTimerSource;

    sget-object v12, Lcom/mstar/android/tvapi/common/vo/EnumTimeOnTimerSource;->EN_Time_OnTimer_Source_ATV:Lcom/mstar/android/tvapi/common/vo/EnumTimeOnTimerSource;

    aput-object v12, v10, v7

    add-int/lit8 v7, v7, 0x1

    :cond_1
    const/4 v8, 0x0

    :goto_0
    if-lt v8, v1, :cond_3

    const/4 v8, 0x0

    :goto_1
    if-lt v8, v6, :cond_7

    if-ne v5, v14, :cond_2

    iget-object v10, p0, Lcom/konka/systemsetting/individ/IndividTimeSetting;->mInputSourceName:[Ljava/lang/String;

    const-string v12, "VGA"

    aput-object v12, v10, v7

    iget-object v10, p0, Lcom/konka/systemsetting/individ/IndividTimeSetting;->mEnumOnTimeSource:[Lcom/mstar/android/tvapi/common/vo/EnumTimeOnTimerSource;

    sget-object v12, Lcom/mstar/android/tvapi/common/vo/EnumTimeOnTimerSource;->EN_Time_OnTimer_Source_RGB:Lcom/mstar/android/tvapi/common/vo/EnumTimeOnTimerSource;

    aput-object v12, v10, v7

    add-int/lit8 v7, v7, 0x1

    :cond_2
    const/4 v8, 0x0

    :goto_2
    if-lt v8, v3, :cond_9

    iget-object v10, p0, Lcom/konka/systemsetting/individ/IndividTimeSetting;->mInputSourceName:[Ljava/lang/String;

    iput-object v10, p0, Lcom/konka/systemsetting/individ/IndividTimeSetting;->mArrCurrent:[Ljava/lang/CharSequence;

    const-string v10, "===>individ==>enum_inputsources:"

    invoke-static {v10}, Lcom/konka/debuginfo/logPrint;->Debug(Ljava/lang/String;)V

    iget-object v12, p0, Lcom/konka/systemsetting/individ/IndividTimeSetting;->mInputSourceName:[Ljava/lang/String;

    array-length v13, v12

    move v10, v11

    :goto_3
    if-lt v10, v13, :cond_e

    return-void

    :cond_3
    if-nez v8, :cond_5

    if-le v1, v14, :cond_4

    iget-object v10, p0, Lcom/konka/systemsetting/individ/IndividTimeSetting;->mInputSourceName:[Ljava/lang/String;

    const-string v12, "AV1"

    aput-object v12, v10, v7

    :goto_4
    iget-object v10, p0, Lcom/konka/systemsetting/individ/IndividTimeSetting;->mEnumOnTimeSource:[Lcom/mstar/android/tvapi/common/vo/EnumTimeOnTimerSource;

    sget-object v12, Lcom/mstar/android/tvapi/common/vo/EnumTimeOnTimerSource;->EN_Time_OnTimer_Source_AV:Lcom/mstar/android/tvapi/common/vo/EnumTimeOnTimerSource;

    aput-object v12, v10, v7

    :goto_5
    add-int/lit8 v8, v8, 0x1

    add-int/lit8 v7, v7, 0x1

    goto :goto_0

    :cond_4
    iget-object v10, p0, Lcom/konka/systemsetting/individ/IndividTimeSetting;->mInputSourceName:[Ljava/lang/String;

    const-string v12, "AV"

    aput-object v12, v10, v7

    goto :goto_4

    :cond_5
    iget-object v10, p0, Lcom/konka/systemsetting/individ/IndividTimeSetting;->mInputSourceName:[Ljava/lang/String;

    new-instance v12, Ljava/lang/StringBuilder;

    const-string v13, "AV"

    invoke-direct {v12, v13}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    add-int/lit8 v13, v8, 0x1

    invoke-virtual {v12, v13}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v12

    invoke-virtual {v12}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v12

    aput-object v12, v10, v7

    iget-object v12, p0, Lcom/konka/systemsetting/individ/IndividTimeSetting;->mEnumOnTimeSource:[Lcom/mstar/android/tvapi/common/vo/EnumTimeOnTimerSource;

    if-ne v8, v14, :cond_6

    sget-object v10, Lcom/mstar/android/tvapi/common/vo/EnumTimeOnTimerSource;->EN_Time_OnTimer_Source_AV2:Lcom/mstar/android/tvapi/common/vo/EnumTimeOnTimerSource;

    :goto_6
    aput-object v10, v12, v7

    goto :goto_5

    :cond_6
    sget-object v10, Lcom/mstar/android/tvapi/common/vo/EnumTimeOnTimerSource;->EN_Time_OnTimer_Source_AV2:Lcom/mstar/android/tvapi/common/vo/EnumTimeOnTimerSource;

    goto :goto_6

    :cond_7
    if-nez v8, :cond_8

    iget-object v10, p0, Lcom/konka/systemsetting/individ/IndividTimeSetting;->mInputSourceName:[Ljava/lang/String;

    const-string v12, "YPbPr"

    aput-object v12, v10, v7

    iget-object v10, p0, Lcom/konka/systemsetting/individ/IndividTimeSetting;->mEnumOnTimeSource:[Lcom/mstar/android/tvapi/common/vo/EnumTimeOnTimerSource;

    sget-object v12, Lcom/mstar/android/tvapi/common/vo/EnumTimeOnTimerSource;->EN_Time_OnTimer_Source_COMPONENT:Lcom/mstar/android/tvapi/common/vo/EnumTimeOnTimerSource;

    aput-object v12, v10, v7

    :goto_7
    add-int/lit8 v8, v8, 0x1

    add-int/lit8 v7, v7, 0x1

    goto :goto_1

    :cond_8
    iget-object v10, p0, Lcom/konka/systemsetting/individ/IndividTimeSetting;->mInputSourceName:[Ljava/lang/String;

    new-instance v12, Ljava/lang/StringBuilder;

    const-string v13, "YPbPr"

    invoke-direct {v12, v13}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    add-int/lit8 v13, v8, 0x1

    invoke-virtual {v12, v13}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v12

    invoke-virtual {v12}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v12

    aput-object v12, v10, v7

    iget-object v10, p0, Lcom/konka/systemsetting/individ/IndividTimeSetting;->mEnumOnTimeSource:[Lcom/mstar/android/tvapi/common/vo/EnumTimeOnTimerSource;

    sget-object v12, Lcom/mstar/android/tvapi/common/vo/EnumTimeOnTimerSource;->EN_Time_OnTimer_Source_COMPONENT2:Lcom/mstar/android/tvapi/common/vo/EnumTimeOnTimerSource;

    aput-object v12, v10, v7

    goto :goto_7

    :cond_9
    if-nez v8, :cond_b

    if-le v3, v14, :cond_a

    iget-object v10, p0, Lcom/konka/systemsetting/individ/IndividTimeSetting;->mInputSourceName:[Ljava/lang/String;

    const-string v12, "HDMI1"

    aput-object v12, v10, v7

    :goto_8
    iget-object v10, p0, Lcom/konka/systemsetting/individ/IndividTimeSetting;->mEnumOnTimeSource:[Lcom/mstar/android/tvapi/common/vo/EnumTimeOnTimerSource;

    sget-object v12, Lcom/mstar/android/tvapi/common/vo/EnumTimeOnTimerSource;->EN_Time_OnTimer_Source_HDMI:Lcom/mstar/android/tvapi/common/vo/EnumTimeOnTimerSource;

    aput-object v12, v10, v7

    :goto_9
    add-int/lit8 v8, v8, 0x1

    add-int/lit8 v7, v7, 0x1

    goto/16 :goto_2

    :cond_a
    iget-object v10, p0, Lcom/konka/systemsetting/individ/IndividTimeSetting;->mInputSourceName:[Ljava/lang/String;

    const-string v12, "HDMI"

    aput-object v12, v10, v7

    goto :goto_8

    :cond_b
    iget-object v10, p0, Lcom/konka/systemsetting/individ/IndividTimeSetting;->mInputSourceName:[Ljava/lang/String;

    new-instance v12, Ljava/lang/StringBuilder;

    const-string v13, "HDMI"

    invoke-direct {v12, v13}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    add-int/lit8 v13, v8, 0x1

    invoke-virtual {v12, v13}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v12

    invoke-virtual {v12}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v12

    aput-object v12, v10, v7

    if-ne v8, v14, :cond_c

    iget-object v10, p0, Lcom/konka/systemsetting/individ/IndividTimeSetting;->mEnumOnTimeSource:[Lcom/mstar/android/tvapi/common/vo/EnumTimeOnTimerSource;

    sget-object v12, Lcom/mstar/android/tvapi/common/vo/EnumTimeOnTimerSource;->EN_Time_OnTimer_Source_HDMI2:Lcom/mstar/android/tvapi/common/vo/EnumTimeOnTimerSource;

    aput-object v12, v10, v7

    goto :goto_9

    :cond_c
    const/4 v10, 0x2

    if-ne v8, v10, :cond_d

    iget-object v10, p0, Lcom/konka/systemsetting/individ/IndividTimeSetting;->mEnumOnTimeSource:[Lcom/mstar/android/tvapi/common/vo/EnumTimeOnTimerSource;

    sget-object v12, Lcom/mstar/android/tvapi/common/vo/EnumTimeOnTimerSource;->EN_Time_OnTimer_Source_HDMI3:Lcom/mstar/android/tvapi/common/vo/EnumTimeOnTimerSource;

    aput-object v12, v10, v7

    goto :goto_9

    :cond_d
    iget-object v10, p0, Lcom/konka/systemsetting/individ/IndividTimeSetting;->mEnumOnTimeSource:[Lcom/mstar/android/tvapi/common/vo/EnumTimeOnTimerSource;

    sget-object v12, Lcom/mstar/android/tvapi/common/vo/EnumTimeOnTimerSource;->EN_Time_OnTimer_Source_HDMI4:Lcom/mstar/android/tvapi/common/vo/EnumTimeOnTimerSource;

    aput-object v12, v10, v7

    goto :goto_9

    :cond_e
    aget-object v9, v12, v10

    invoke-static {v9}, Lcom/konka/debuginfo/logPrint;->Debug(Ljava/lang/String;)V

    add-int/lit8 v10, v10, 0x1

    goto/16 :goto_3
.end method

.method private debug(Ljava/lang/String;)V
    .locals 0
    .param p1    # Ljava/lang/String;

    invoke-static {p1}, Lcom/konka/debuginfo/logPrint;->Debug(Ljava/lang/String;)V

    return-void
.end method

.method private static formatOffset(I)[C
    .locals 6
    .param p0    # I

    const/4 v5, 0x3

    div-int/lit16 v3, p0, 0x3e8

    div-int/lit8 p0, v3, 0x3c

    const/16 v3, 0x9

    new-array v0, v3, [C

    const/4 v3, 0x0

    const/16 v4, 0x47

    aput-char v4, v0, v3

    const/4 v3, 0x1

    const/16 v4, 0x4d

    aput-char v4, v0, v3

    const/4 v3, 0x2

    const/16 v4, 0x54

    aput-char v4, v0, v3

    if-gez p0, :cond_0

    const/16 v3, 0x2d

    aput-char v3, v0, v5

    neg-int p0, p0

    :goto_0
    div-int/lit8 v1, p0, 0x3c

    rem-int/lit8 v2, p0, 0x3c

    const/4 v3, 0x4

    div-int/lit8 v4, v1, 0xa

    add-int/lit8 v4, v4, 0x30

    int-to-char v4, v4

    aput-char v4, v0, v3

    const/4 v3, 0x5

    rem-int/lit8 v4, v1, 0xa

    add-int/lit8 v4, v4, 0x30

    int-to-char v4, v4

    aput-char v4, v0, v3

    const/4 v3, 0x6

    const/16 v4, 0x3a

    aput-char v4, v0, v3

    const/4 v3, 0x7

    div-int/lit8 v4, v2, 0xa

    add-int/lit8 v4, v4, 0x30

    int-to-char v4, v4

    aput-char v4, v0, v3

    const/16 v3, 0x8

    rem-int/lit8 v4, v2, 0xa

    add-int/lit8 v4, v4, 0x30

    int-to-char v4, v4

    aput-char v4, v0, v3

    return-object v0

    :cond_0
    const/16 v3, 0x2b

    aput-char v3, v0, v5

    goto :goto_0
.end method

.method private getFocusImageById(I)I
    .locals 2
    .param p1    # I

    const/high16 v0, -0x1000000

    packed-switch p1, :pswitch_data_0

    const-string v1, "DateTimeSetting: onKeyDown-error, no such item id."

    invoke-static {v1}, Lcom/konka/debuginfo/logPrint;->Debug(Ljava/lang/String;)V

    :goto_0
    return v0

    :pswitch_0
    const v0, 0x7f020032    # com.konka.systemsetting.R.drawable.syssettingthirditemsinglefocus

    goto :goto_0

    :pswitch_1
    const v0, 0x7f020034    # com.konka.systemsetting.R.drawable.syssettingthirditemtopfocus

    goto :goto_0

    :pswitch_2
    const v0, 0x7f020030    # com.konka.systemsetting.R.drawable.syssettingthirditemmidfocus

    goto :goto_0

    :pswitch_3
    const v0, 0x7f02002e    # com.konka.systemsetting.R.drawable.syssettingthirditembottomfocus

    goto :goto_0

    nop

    :pswitch_data_0
    .packed-switch 0x7f090026
        :pswitch_1    # com.konka.systemsetting.R.id.sys_individ_timing_item_autoadjust
        :pswitch_2    # com.konka.systemsetting.R.id.sys_individ_timing_item_datesetting
        :pswitch_3    # com.konka.systemsetting.R.id.sys_individ_timing_item_timesetting
        :pswitch_0    # com.konka.systemsetting.R.id.sys_individ_timing_item_timezonesetting
        :pswitch_1    # com.konka.systemsetting.R.id.sys_individ_timing_item_haltswitch
        :pswitch_3    # com.konka.systemsetting.R.id.sys_individ_timing_item_halttime
        :pswitch_1    # com.konka.systemsetting.R.id.sys_individ_timing_item_bootswitch
        :pswitch_2    # com.konka.systemsetting.R.id.sys_individ_timing_item_boottime
        :pswitch_3    # com.konka.systemsetting.R.id.sys_individ_timing_item_bootchannel
    .end packed-switch
.end method

.method private static getGMTTxt(Ljava/util/TimeZone;)Ljava/lang/String;
    .locals 7
    .param p0    # Ljava/util/TimeZone;

    invoke-static {}, Ljava/util/Calendar;->getInstance()Ljava/util/Calendar;

    move-result-object v6

    invoke-virtual {v6}, Ljava/util/Calendar;->getTimeInMillis()J

    move-result-wide v0

    invoke-virtual {p0, v0, v1}, Ljava/util/TimeZone;->getOffset(J)I

    move-result v4

    invoke-static {v4}, Ljava/lang/Math;->abs(I)I

    move-result v5

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string v6, "GMT"

    invoke-virtual {v3, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    if-gez v4, :cond_1

    const/16 v6, 0x2d

    invoke-virtual {v3, v6}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;

    :goto_0
    const v6, 0x36ee80

    div-int v6, v5, v6

    invoke-virtual {v3, v6}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    const/16 v6, 0x3a

    invoke-virtual {v3, v6}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;

    const v6, 0xea60

    div-int v2, v5, v6

    rem-int/lit8 v2, v2, 0x3c

    const/16 v6, 0xa

    if-ge v2, v6, :cond_0

    const/16 v6, 0x30

    invoke-virtual {v3, v6}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;

    :cond_0
    invoke-virtual {v3, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v6

    return-object v6

    :cond_1
    const/16 v6, 0x2b

    invoke-virtual {v3, v6}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;

    goto :goto_0
.end method

.method private getIdxByInputSrc(Lcom/mstar/android/tvapi/common/vo/EnumTimeOnTimerSource;)I
    .locals 6
    .param p1    # Lcom/mstar/android/tvapi/common/vo/EnumTimeOnTimerSource;

    const/4 v2, 0x0

    const/4 v1, 0x0

    new-instance v3, Ljava/lang/StringBuilder;

    const-string v4, "getIdxByInputSrc========the source is ===="

    invoke-direct {v3, v4}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v3, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v3}, Lcom/konka/debuginfo/logPrint;->Debug(Ljava/lang/String;)V

    iget-object v4, p0, Lcom/konka/systemsetting/individ/IndividTimeSetting;->mEnumOnTimeSource:[Lcom/mstar/android/tvapi/common/vo/EnumTimeOnTimerSource;

    array-length v5, v4

    move v3, v2

    :goto_0
    if-lt v3, v5, :cond_1

    move v1, v2

    :cond_0
    return v1

    :cond_1
    aget-object v0, v4, v3

    if-eq v0, p1, :cond_0

    add-int/lit8 v1, v1, 0x1

    add-int/lit8 v3, v3, 0x1

    goto :goto_0
.end method

.method private getImageIdByState(Z)I
    .locals 1
    .param p1    # Z

    if-eqz p1, :cond_0

    iget-object v0, p0, Lcom/konka/systemsetting/individ/IndividTimeSetting;->mContext:Lcom/konka/systemsetting/MainActivity;

    iget v0, v0, Lcom/konka/systemsetting/MainActivity;->IMG_SWITCH_ON:I

    :goto_0
    return v0

    :cond_0
    iget-object v0, p0, Lcom/konka/systemsetting/individ/IndividTimeSetting;->mContext:Lcom/konka/systemsetting/MainActivity;

    iget v0, v0, Lcom/konka/systemsetting/MainActivity;->IMG_SWITCH_OFF:I

    goto :goto_0
.end method

.method private getInitData()V
    .locals 5

    const/4 v1, 0x1

    const/4 v2, 0x0

    :try_start_0
    iget-object v3, p0, Lcom/konka/systemsetting/individ/IndividTimeSetting;->tMgr:Lcom/mstar/android/tvapi/common/TimerManager;

    invoke-virtual {v3}, Lcom/mstar/android/tvapi/common/TimerManager;->getOffModeStatus()Lcom/mstar/android/tvapi/common/vo/TimerPowerOffModeStatus;

    move-result-object v3

    invoke-virtual {v3}, Lcom/mstar/android/tvapi/common/vo/TimerPowerOffModeStatus;->getTimerPeriod()Lcom/mstar/android/tvapi/common/vo/EnumTimerPeriod;

    move-result-object v3

    sget-object v4, Lcom/mstar/android/tvapi/common/vo/EnumTimerPeriod;->EN_Timer_Off:Lcom/mstar/android/tvapi/common/vo/EnumTimerPeriod;

    if-eq v3, v4, :cond_0

    move v3, v1

    :goto_0
    sput-boolean v3, Lcom/konka/systemsetting/individ/IndividTimeSetting;->m_isHaltSwitchOn:Z

    iget-object v3, p0, Lcom/konka/systemsetting/individ/IndividTimeSetting;->tMgr:Lcom/mstar/android/tvapi/common/TimerManager;

    invoke-virtual {v3}, Lcom/mstar/android/tvapi/common/TimerManager;->getOnTime()Lcom/mstar/android/tvapi/common/vo/TimerPowerOn;

    move-result-object v3

    invoke-virtual {v3}, Lcom/mstar/android/tvapi/common/vo/TimerPowerOn;->getTimerPeriod()Lcom/mstar/android/tvapi/common/vo/EnumTimerPeriod;

    move-result-object v3

    sget-object v4, Lcom/mstar/android/tvapi/common/vo/EnumTimerPeriod;->EN_Timer_Off:Lcom/mstar/android/tvapi/common/vo/EnumTimerPeriod;

    if-eq v3, v4, :cond_1

    :goto_1
    sput-boolean v1, Lcom/konka/systemsetting/individ/IndividTimeSetting;->m_isBootSwitchOn:Z

    iget-object v1, p0, Lcom/konka/systemsetting/individ/IndividTimeSetting;->tMgr:Lcom/mstar/android/tvapi/common/TimerManager;

    invoke-virtual {v1}, Lcom/mstar/android/tvapi/common/TimerManager;->getOnTime()Lcom/mstar/android/tvapi/common/vo/TimerPowerOn;

    move-result-object v1

    invoke-virtual {v1}, Lcom/mstar/android/tvapi/common/vo/TimerPowerOn;->getTvSource()Lcom/mstar/android/tvapi/common/vo/EnumTimeOnTimerSource;

    move-result-object v1

    invoke-direct {p0, v1}, Lcom/konka/systemsetting/individ/IndividTimeSetting;->getIdxByInputSrc(Lcom/mstar/android/tvapi/common/vo/EnumTimeOnTimerSource;)I

    move-result v1

    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    iput-object v1, p0, Lcom/konka/systemsetting/individ/IndividTimeSetting;->m_iBootChannelIndex:Ljava/lang/Integer;
    :try_end_0
    .catch Lcom/mstar/android/tvapi/common/exception/TvCommonException; {:try_start_0 .. :try_end_0} :catch_0

    :goto_2
    return-void

    :cond_0
    move v3, v2

    goto :goto_0

    :cond_1
    move v1, v2

    goto :goto_1

    :catch_0
    move-exception v0

    invoke-virtual {v0}, Lcom/mstar/android/tvapi/common/exception/TvCommonException;->printStackTrace()V

    goto :goto_2
.end method

.method private getTimeString()Ljava/lang/String;
    .locals 4

    invoke-static {}, Ljava/util/Calendar;->getInstance()Ljava/util/Calendar;

    move-result-object v0

    new-instance v2, Ljava/lang/StringBuilder;

    const/4 v3, 0x1

    invoke-virtual {v0, v3}, Ljava/util/Calendar;->get(I)I

    move-result v3

    invoke-static {v3}, Ljava/lang/String;->valueOf(I)Ljava/lang/String;

    move-result-object v3

    invoke-direct {v2, v3}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    const-string v3, "-"

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    const/4 v3, 0x2

    invoke-virtual {v0, v3}, Ljava/util/Calendar;->get(I)I

    move-result v3

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, "-"

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    const/4 v3, 0x5

    invoke-virtual {v0, v3}, Ljava/util/Calendar;->get(I)I

    move-result v3

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-static {v1}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v3

    invoke-direct {v2, v3}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    const-string v3, "-"

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    const/16 v3, 0xb

    invoke-virtual {v0, v3}, Ljava/util/Calendar;->get(I)I

    move-result v3

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, "-"

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    const/16 v3, 0xc

    invoke-virtual {v0, v3}, Ljava/util/Calendar;->get(I)I

    move-result v3

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    return-object v1
.end method

.method static getTimeZoneText(Ljava/util/TimeZone;)Ljava/lang/String;
    .locals 5
    .param p0    # Ljava/util/TimeZone;

    new-instance v3, Ljava/util/Date;

    invoke-direct {v3}, Ljava/util/Date;-><init>()V

    invoke-virtual {p0, v3}, Ljava/util/TimeZone;->inDaylightTime(Ljava/util/Date;)Z

    move-result v0

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    invoke-static {p0}, Lcom/konka/systemsetting/individ/IndividTimeSetting;->getGMTTxt(Ljava/util/TimeZone;)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v2, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    const-string v3, ", "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    const/4 v4, 0x1

    invoke-virtual {p0, v0, v4}, Ljava/util/TimeZone;->getDisplayName(ZI)Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    new-instance v3, Ljava/lang/StringBuilder;

    const-string v4, "getTimeZoneText: "

    invoke-direct {v3, v4}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v3, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v3}, Lcom/konka/debuginfo/logPrint;->Debug(Ljava/lang/String;)V

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    return-object v3
.end method

.method private getUnFocusImageById(I)I
    .locals 2
    .param p1    # I

    const/high16 v0, -0x1000000

    packed-switch p1, :pswitch_data_0

    const-string v1, "DateTimeSetting: onKeyDown-error, no such item id."

    invoke-static {v1}, Lcom/konka/debuginfo/logPrint;->Debug(Ljava/lang/String;)V

    :goto_0
    return v0

    :pswitch_0
    const v0, 0x7f020031    # com.konka.systemsetting.R.drawable.syssettingthirditemsingle

    goto :goto_0

    :pswitch_1
    const v0, 0x7f020033    # com.konka.systemsetting.R.drawable.syssettingthirditemtop

    goto :goto_0

    :pswitch_2
    const v0, 0x7f02002f    # com.konka.systemsetting.R.drawable.syssettingthirditemmid

    goto :goto_0

    :pswitch_3
    const v0, 0x7f02002d    # com.konka.systemsetting.R.drawable.syssettingthirditembottom

    goto :goto_0

    nop

    :pswitch_data_0
    .packed-switch 0x7f090026
        :pswitch_1    # com.konka.systemsetting.R.id.sys_individ_timing_item_autoadjust
        :pswitch_2    # com.konka.systemsetting.R.id.sys_individ_timing_item_datesetting
        :pswitch_3    # com.konka.systemsetting.R.id.sys_individ_timing_item_timesetting
        :pswitch_0    # com.konka.systemsetting.R.id.sys_individ_timing_item_timezonesetting
        :pswitch_1    # com.konka.systemsetting.R.id.sys_individ_timing_item_haltswitch
        :pswitch_3    # com.konka.systemsetting.R.id.sys_individ_timing_item_halttime
        :pswitch_1    # com.konka.systemsetting.R.id.sys_individ_timing_item_bootswitch
        :pswitch_2    # com.konka.systemsetting.R.id.sys_individ_timing_item_boottime
        :pswitch_3    # com.konka.systemsetting.R.id.sys_individ_timing_item_bootchannel
    .end packed-switch
.end method

.method private static getZones(Landroid/content/Context;)Ljava/util/List;
    .locals 10
    .param p0    # Landroid/content/Context;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Landroid/content/Context;",
            ")",
            "Ljava/util/List",
            "<",
            "Ljava/util/HashMap",
            "<",
            "Ljava/lang/String;",
            "Ljava/lang/Object;",
            ">;>;"
        }
    .end annotation

    const/4 v9, 0x3

    const/4 v8, 0x2

    new-instance v4, Ljava/util/ArrayList;

    invoke-direct {v4}, Ljava/util/ArrayList;-><init>()V

    invoke-static {}, Ljava/util/Calendar;->getInstance()Ljava/util/Calendar;

    move-result-object v6

    invoke-virtual {v6}, Ljava/util/Calendar;->getTimeInMillis()J

    move-result-wide v0

    :try_start_0
    invoke-virtual {p0}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v6

    const v7, 0x7f040003    # com.konka.systemsetting.R.xml.timezones

    invoke-virtual {v6, v7}, Landroid/content/res/Resources;->getXml(I)Landroid/content/res/XmlResourceParser;

    move-result-object v5

    :cond_0
    invoke-interface {v5}, Landroid/content/res/XmlResourceParser;->next()I

    move-result v6

    if-ne v6, v8, :cond_0

    invoke-interface {v5}, Landroid/content/res/XmlResourceParser;->next()I

    :goto_0
    invoke-interface {v5}, Landroid/content/res/XmlResourceParser;->getEventType()I

    move-result v6

    if-ne v6, v9, :cond_3

    invoke-interface {v5}, Landroid/content/res/XmlResourceParser;->close()V

    :cond_1
    :goto_1
    return-object v4

    :cond_2
    invoke-interface {v5}, Landroid/content/res/XmlResourceParser;->getEventType()I

    move-result v6

    const/4 v7, 0x1

    if-eq v6, v7, :cond_1

    invoke-interface {v5}, Landroid/content/res/XmlResourceParser;->next()I

    :cond_3
    invoke-interface {v5}, Landroid/content/res/XmlResourceParser;->getEventType()I

    move-result v6

    if-ne v6, v8, :cond_2

    invoke-interface {v5}, Landroid/content/res/XmlResourceParser;->getName()Ljava/lang/String;

    move-result-object v6

    const-string v7, "timezone"

    invoke-virtual {v6, v7}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v6

    if-eqz v6, :cond_4

    const/4 v6, 0x0

    invoke-interface {v5, v6}, Landroid/content/res/XmlResourceParser;->getAttributeValue(I)Ljava/lang/String;

    move-result-object v3

    invoke-interface {v5}, Landroid/content/res/XmlResourceParser;->nextText()Ljava/lang/String;

    move-result-object v2

    invoke-static {v4, v3, v2, v0, v1}, Lcom/konka/systemsetting/individ/IndividTimeSetting;->addItem(Ljava/util/List;Ljava/lang/String;Ljava/lang/String;J)V

    :cond_4
    :goto_2
    invoke-interface {v5}, Landroid/content/res/XmlResourceParser;->getEventType()I

    move-result v6

    if-ne v6, v9, :cond_5

    invoke-interface {v5}, Landroid/content/res/XmlResourceParser;->next()I

    goto :goto_0

    :catch_0
    move-exception v6

    goto :goto_1

    :cond_5
    invoke-interface {v5}, Landroid/content/res/XmlResourceParser;->next()I
    :try_end_0
    .catch Lorg/xmlpull/v1/XmlPullParserException; {:try_start_0 .. :try_end_0} :catch_0
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_1

    goto :goto_2

    :catch_1
    move-exception v6

    goto :goto_1
.end method

.method private initAll()V
    .locals 3

    const/4 v2, 0x0

    invoke-direct {p0}, Lcom/konka/systemsetting/individ/IndividTimeSetting;->initUIComponents()V

    invoke-direct {p0}, Lcom/konka/systemsetting/individ/IndividTimeSetting;->setListener()V

    iget-object v0, p0, Lcom/konka/systemsetting/individ/IndividTimeSetting;->mContext:Lcom/konka/systemsetting/MainActivity;

    const-string v1, "userSettings"

    invoke-virtual {v0, v1, v2}, Lcom/konka/systemsetting/MainActivity;->getSharedPreferences(Ljava/lang/String;I)Landroid/content/SharedPreferences;

    move-result-object v0

    iput-object v0, p0, Lcom/konka/systemsetting/individ/IndividTimeSetting;->userSettings:Landroid/content/SharedPreferences;

    iget-object v0, p0, Lcom/konka/systemsetting/individ/IndividTimeSetting;->userSettings:Landroid/content/SharedPreferences;

    const-string v1, "timeZoneSetting"

    invoke-interface {v0, v1, v2}, Landroid/content/SharedPreferences;->getInt(Ljava/lang/String;I)I

    move-result v0

    invoke-static {v0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v0

    iput-object v0, p0, Lcom/konka/systemsetting/individ/IndividTimeSetting;->m_iTimeZoneSelectIndex:Ljava/lang/Integer;

    iget-object v0, p0, Lcom/konka/systemsetting/individ/IndividTimeSetting;->settingDesk:Lcom/konka/kkinterface/tv/SettingDesk;

    invoke-interface {v0}, Lcom/konka/kkinterface/tv/SettingDesk;->GetSystemAutoTimeType()I

    move-result v0

    invoke-static {v0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v0

    iput-object v0, p0, Lcom/konka/systemsetting/individ/IndividTimeSetting;->m_iAutoTimeSelectIndex:Ljava/lang/Integer;

    iget-object v0, p0, Lcom/konka/systemsetting/individ/IndividTimeSetting;->getTimer:Ljava/lang/Thread;

    invoke-virtual {v0}, Ljava/lang/Thread;->start()V

    return-void
.end method

.method private initDateTime()V
    .locals 3

    invoke-direct {p0}, Lcom/konka/systemsetting/individ/IndividTimeSetting;->isDefaults()Z

    move-result v1

    if-nez v1, :cond_0

    invoke-direct {p0}, Lcom/konka/systemsetting/individ/IndividTimeSetting;->setDefaults()V

    :cond_0
    invoke-virtual {p0}, Lcom/konka/systemsetting/individ/IndividTimeSetting;->is24Hour()Z

    move-result v1

    if-nez v1, :cond_1

    const/4 v1, 0x1

    invoke-virtual {p0, v1}, Lcom/konka/systemsetting/individ/IndividTimeSetting;->set24Hour(Z)V

    :cond_1
    :try_start_0
    iget-object v1, p0, Lcom/konka/systemsetting/individ/IndividTimeSetting;->tMgr:Lcom/mstar/android/tvapi/common/TimerManager;

    invoke-virtual {v1}, Lcom/mstar/android/tvapi/common/TimerManager;->getOnTime()Lcom/mstar/android/tvapi/common/vo/TimerPowerOn;

    move-result-object v1

    iput-object v1, p0, Lcom/konka/systemsetting/individ/IndividTimeSetting;->timerOn:Lcom/mstar/android/tvapi/common/vo/TimerPowerOn;

    iget-object v1, p0, Lcom/konka/systemsetting/individ/IndividTimeSetting;->tMgr:Lcom/mstar/android/tvapi/common/TimerManager;

    invoke-virtual {v1}, Lcom/mstar/android/tvapi/common/TimerManager;->getOffModeStatus()Lcom/mstar/android/tvapi/common/vo/TimerPowerOffModeStatus;

    move-result-object v1

    iput-object v1, p0, Lcom/konka/systemsetting/individ/IndividTimeSetting;->timerOff:Lcom/mstar/android/tvapi/common/vo/TimerPowerOffModeStatus;

    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "init_PowerOn_Time: "

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    iget-object v2, p0, Lcom/konka/systemsetting/individ/IndividTimeSetting;->timerOn:Lcom/mstar/android/tvapi/common/vo/TimerPowerOn;

    invoke-direct {p0, v2}, Lcom/konka/systemsetting/individ/IndividTimeSetting;->timeToString(Landroid/text/format/Time;)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-direct {p0, v1}, Lcom/konka/systemsetting/individ/IndividTimeSetting;->debug(Ljava/lang/String;)V

    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "init_PowerOff_Time: "

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    iget-object v2, p0, Lcom/konka/systemsetting/individ/IndividTimeSetting;->timerOff:Lcom/mstar/android/tvapi/common/vo/TimerPowerOffModeStatus;

    invoke-direct {p0, v2}, Lcom/konka/systemsetting/individ/IndividTimeSetting;->timeToString(Landroid/text/format/Time;)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-direct {p0, v1}, Lcom/konka/systemsetting/individ/IndividTimeSetting;->debug(Ljava/lang/String;)V
    :try_end_0
    .catch Lcom/mstar/android/tvapi/common/exception/TvCommonException; {:try_start_0 .. :try_end_0} :catch_0

    :goto_0
    return-void

    :catch_0
    move-exception v0

    invoke-virtual {v0}, Lcom/mstar/android/tvapi/common/exception/TvCommonException;->printStackTrace()V

    goto :goto_0
.end method

.method private initUIComponents()V
    .locals 5

    const v4, 0x7f09002e    # com.konka.systemsetting.R.id.sys_individ_timing_item_bootchannel

    const v3, 0x7f090026    # com.konka.systemsetting.R.id.sys_individ_timing_item_autoadjust

    const v2, 0x7f090022    # com.konka.systemsetting.R.id.linearlayout_individ_tab_timesetting

    iget-object v0, p0, Lcom/konka/systemsetting/individ/IndividTimeSetting;->mContext:Lcom/konka/systemsetting/MainActivity;

    invoke-virtual {v0}, Lcom/konka/systemsetting/MainActivity;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    const v1, 0x7f070002    # com.konka.systemsetting.R.array.str_arr_individ_auto_time

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getTextArray(I)[Ljava/lang/CharSequence;

    move-result-object v0

    iput-object v0, p0, Lcom/konka/systemsetting/individ/IndividTimeSetting;->nArrCurrent:[Ljava/lang/CharSequence;

    iget-object v0, p0, Lcom/konka/systemsetting/individ/IndividTimeSetting;->mContext:Lcom/konka/systemsetting/MainActivity;

    invoke-virtual {v0, v3}, Lcom/konka/systemsetting/MainActivity;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/LinearLayout;

    iput-object v0, p0, Lcom/konka/systemsetting/individ/IndividTimeSetting;->itemAutoAdjust:Landroid/widget/LinearLayout;

    iget-object v0, p0, Lcom/konka/systemsetting/individ/IndividTimeSetting;->mContext:Lcom/konka/systemsetting/MainActivity;

    const v1, 0x7f090027    # com.konka.systemsetting.R.id.sys_individ_timing_item_datesetting

    invoke-virtual {v0, v1}, Lcom/konka/systemsetting/MainActivity;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/LinearLayout;

    iput-object v0, p0, Lcom/konka/systemsetting/individ/IndividTimeSetting;->itemDateSetting:Landroid/widget/LinearLayout;

    iget-object v0, p0, Lcom/konka/systemsetting/individ/IndividTimeSetting;->mContext:Lcom/konka/systemsetting/MainActivity;

    const v1, 0x7f090028    # com.konka.systemsetting.R.id.sys_individ_timing_item_timesetting

    invoke-virtual {v0, v1}, Lcom/konka/systemsetting/MainActivity;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/LinearLayout;

    iput-object v0, p0, Lcom/konka/systemsetting/individ/IndividTimeSetting;->itemTimeSetting:Landroid/widget/LinearLayout;

    iget-object v0, p0, Lcom/konka/systemsetting/individ/IndividTimeSetting;->mContext:Lcom/konka/systemsetting/MainActivity;

    const v1, 0x7f090029    # com.konka.systemsetting.R.id.sys_individ_timing_item_timezonesetting

    invoke-virtual {v0, v1}, Lcom/konka/systemsetting/MainActivity;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/LinearLayout;

    iput-object v0, p0, Lcom/konka/systemsetting/individ/IndividTimeSetting;->itemTimeZoneSetting:Landroid/widget/LinearLayout;

    iget-object v0, p0, Lcom/konka/systemsetting/individ/IndividTimeSetting;->mContext:Lcom/konka/systemsetting/MainActivity;

    const v1, 0x7f09002a    # com.konka.systemsetting.R.id.sys_individ_timing_item_haltswitch

    invoke-virtual {v0, v1}, Lcom/konka/systemsetting/MainActivity;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/LinearLayout;

    iput-object v0, p0, Lcom/konka/systemsetting/individ/IndividTimeSetting;->itemHaltSwitch:Landroid/widget/LinearLayout;

    iget-object v0, p0, Lcom/konka/systemsetting/individ/IndividTimeSetting;->mContext:Lcom/konka/systemsetting/MainActivity;

    const v1, 0x7f09002b    # com.konka.systemsetting.R.id.sys_individ_timing_item_halttime

    invoke-virtual {v0, v1}, Lcom/konka/systemsetting/MainActivity;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/LinearLayout;

    iput-object v0, p0, Lcom/konka/systemsetting/individ/IndividTimeSetting;->itemHaltTime:Landroid/widget/LinearLayout;

    iget-object v0, p0, Lcom/konka/systemsetting/individ/IndividTimeSetting;->mContext:Lcom/konka/systemsetting/MainActivity;

    const v1, 0x7f09002c    # com.konka.systemsetting.R.id.sys_individ_timing_item_bootswitch

    invoke-virtual {v0, v1}, Lcom/konka/systemsetting/MainActivity;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/LinearLayout;

    iput-object v0, p0, Lcom/konka/systemsetting/individ/IndividTimeSetting;->itemBootSwitch:Landroid/widget/LinearLayout;

    iget-object v0, p0, Lcom/konka/systemsetting/individ/IndividTimeSetting;->mContext:Lcom/konka/systemsetting/MainActivity;

    const v1, 0x7f09002d    # com.konka.systemsetting.R.id.sys_individ_timing_item_boottime

    invoke-virtual {v0, v1}, Lcom/konka/systemsetting/MainActivity;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/LinearLayout;

    iput-object v0, p0, Lcom/konka/systemsetting/individ/IndividTimeSetting;->itemBootTime:Landroid/widget/LinearLayout;

    iget-object v0, p0, Lcom/konka/systemsetting/individ/IndividTimeSetting;->mContext:Lcom/konka/systemsetting/MainActivity;

    invoke-virtual {v0, v4}, Lcom/konka/systemsetting/MainActivity;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/LinearLayout;

    iput-object v0, p0, Lcom/konka/systemsetting/individ/IndividTimeSetting;->itemBootChannel:Landroid/widget/LinearLayout;

    iget-object v0, p0, Lcom/konka/systemsetting/individ/IndividTimeSetting;->itemAutoAdjust:Landroid/widget/LinearLayout;

    invoke-virtual {v0, v2}, Landroid/widget/LinearLayout;->setNextFocusLeftId(I)V

    iget-object v0, p0, Lcom/konka/systemsetting/individ/IndividTimeSetting;->itemDateSetting:Landroid/widget/LinearLayout;

    invoke-virtual {v0, v2}, Landroid/widget/LinearLayout;->setNextFocusLeftId(I)V

    iget-object v0, p0, Lcom/konka/systemsetting/individ/IndividTimeSetting;->itemTimeSetting:Landroid/widget/LinearLayout;

    invoke-virtual {v0, v2}, Landroid/widget/LinearLayout;->setNextFocusLeftId(I)V

    iget-object v0, p0, Lcom/konka/systemsetting/individ/IndividTimeSetting;->itemTimeZoneSetting:Landroid/widget/LinearLayout;

    invoke-virtual {v0, v2}, Landroid/widget/LinearLayout;->setNextFocusLeftId(I)V

    iget-object v0, p0, Lcom/konka/systemsetting/individ/IndividTimeSetting;->itemHaltSwitch:Landroid/widget/LinearLayout;

    invoke-virtual {v0, v2}, Landroid/widget/LinearLayout;->setNextFocusLeftId(I)V

    iget-object v0, p0, Lcom/konka/systemsetting/individ/IndividTimeSetting;->itemHaltTime:Landroid/widget/LinearLayout;

    invoke-virtual {v0, v2}, Landroid/widget/LinearLayout;->setNextFocusLeftId(I)V

    iget-object v0, p0, Lcom/konka/systemsetting/individ/IndividTimeSetting;->itemBootSwitch:Landroid/widget/LinearLayout;

    invoke-virtual {v0, v2}, Landroid/widget/LinearLayout;->setNextFocusLeftId(I)V

    iget-object v0, p0, Lcom/konka/systemsetting/individ/IndividTimeSetting;->itemBootTime:Landroid/widget/LinearLayout;

    invoke-virtual {v0, v2}, Landroid/widget/LinearLayout;->setNextFocusLeftId(I)V

    iget-object v0, p0, Lcom/konka/systemsetting/individ/IndividTimeSetting;->itemBootChannel:Landroid/widget/LinearLayout;

    invoke-virtual {v0, v2}, Landroid/widget/LinearLayout;->setNextFocusLeftId(I)V

    iget-object v0, p0, Lcom/konka/systemsetting/individ/IndividTimeSetting;->itemAutoAdjust:Landroid/widget/LinearLayout;

    invoke-virtual {v0, v3}, Landroid/widget/LinearLayout;->setNextFocusUpId(I)V

    iget-object v0, p0, Lcom/konka/systemsetting/individ/IndividTimeSetting;->itemBootChannel:Landroid/widget/LinearLayout;

    invoke-virtual {v0, v4}, Landroid/widget/LinearLayout;->setNextFocusDownId(I)V

    return-void
.end method

.method private isDefaults()Z
    .locals 4

    const/4 v1, 0x0

    iget-object v2, p0, Lcom/konka/systemsetting/individ/IndividTimeSetting;->tMgr:Lcom/mstar/android/tvapi/common/TimerManager;

    if-nez v2, :cond_0

    const-string v2, "KK.lxsf: TimerManager is null!!!!!"

    invoke-static {v2}, Lcom/konka/debuginfo/logPrint;->Debug(Ljava/lang/String;)V

    :cond_0
    :try_start_0
    iget-object v2, p0, Lcom/konka/systemsetting/individ/IndividTimeSetting;->tMgr:Lcom/mstar/android/tvapi/common/TimerManager;

    invoke-virtual {v2}, Lcom/mstar/android/tvapi/common/TimerManager;->getTimeZone()Lcom/mstar/android/tvapi/common/vo/TvOsType$EnumTimeZone;

    move-result-object v2

    sget-object v3, Lcom/mstar/android/tvapi/common/vo/TvOsType$EnumTimeZone;->E_TIMEZONE_BEIJING:Lcom/mstar/android/tvapi/common/vo/TvOsType$EnumTimeZone;

    invoke-virtual {v2, v3}, Lcom/mstar/android/tvapi/common/vo/TvOsType$EnumTimeZone;->equals(Ljava/lang/Object;)Z
    :try_end_0
    .catch Lcom/mstar/android/tvapi/common/exception/TvCommonException; {:try_start_0 .. :try_end_0} :catch_0

    move-result v1

    :goto_0
    return v1

    :catch_0
    move-exception v0

    invoke-virtual {v0}, Lcom/mstar/android/tvapi/common/exception/TvCommonException;->printStackTrace()V

    goto :goto_0
.end method

.method private onEnterKeyDownOrOnMouseClick(I)Z
    .locals 4
    .param p1    # I

    const/4 v1, 0x0

    const/4 v2, 0x1

    packed-switch p1, :pswitch_data_0

    const-string v0, "DateTimeSetting: onKeyDown-error, no such item id."

    invoke-static {v0}, Lcom/konka/debuginfo/logPrint;->Debug(Ljava/lang/String;)V

    :goto_0
    return v1

    :pswitch_0
    iget-object v0, p0, Lcom/konka/systemsetting/individ/IndividTimeSetting;->settingDesk:Lcom/konka/kkinterface/tv/SettingDesk;

    invoke-interface {v0}, Lcom/konka/kkinterface/tv/SettingDesk;->GetSystemAutoTimeType()I

    move-result v0

    invoke-static {v0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v0

    iput-object v0, p0, Lcom/konka/systemsetting/individ/IndividTimeSetting;->m_iAutoTimeSelectIndex:Ljava/lang/Integer;

    iget-object v0, p0, Lcom/konka/systemsetting/individ/IndividTimeSetting;->m_iAutoTimeSelectIndex:Ljava/lang/Integer;

    invoke-virtual {v0}, Ljava/lang/Integer;->intValue()I

    move-result v0

    if-ne v0, v2, :cond_0

    move v0, v1

    :goto_1
    invoke-static {v0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v0

    iput-object v0, p0, Lcom/konka/systemsetting/individ/IndividTimeSetting;->m_iAutoTimeSelectIndex:Ljava/lang/Integer;

    iget-object v0, p0, Lcom/konka/systemsetting/individ/IndividTimeSetting;->settingDesk:Lcom/konka/kkinterface/tv/SettingDesk;

    iget-object v3, p0, Lcom/konka/systemsetting/individ/IndividTimeSetting;->m_iAutoTimeSelectIndex:Ljava/lang/Integer;

    invoke-virtual {v3}, Ljava/lang/Integer;->intValue()I

    move-result v3

    invoke-interface {v0, v3}, Lcom/konka/kkinterface/tv/SettingDesk;->SetSystemAutoTimeType(I)Z

    iget-object v0, p0, Lcom/konka/systemsetting/individ/IndividTimeSetting;->m_iAutoTimeSelectIndex:Ljava/lang/Integer;

    invoke-virtual {v0}, Ljava/lang/Integer;->intValue()I

    move-result v0

    if-ne v0, v2, :cond_1

    invoke-direct {p0, v2}, Lcom/konka/systemsetting/individ/IndividTimeSetting;->setAutoTime(Z)V

    :goto_2
    invoke-direct {p0}, Lcom/konka/systemsetting/individ/IndividTimeSetting;->updateAutoTimeUI()V

    const-string v0, "DateTimeSetting: changeAutoTimeState()."

    invoke-static {v0}, Lcom/konka/debuginfo/logPrint;->Debug(Ljava/lang/String;)V

    :goto_3
    move v1, v2

    goto :goto_0

    :cond_0
    move v0, v2

    goto :goto_1

    :cond_1
    invoke-direct {p0, v1}, Lcom/konka/systemsetting/individ/IndividTimeSetting;->setAutoTime(Z)V

    goto :goto_2

    :pswitch_1
    invoke-direct {p0, p1}, Lcom/konka/systemsetting/individ/IndividTimeSetting;->changeSwitchState(I)V

    const-string v0, "DateTimeSetting: changeSwitchState(currItemId)."

    invoke-static {v0}, Lcom/konka/debuginfo/logPrint;->Debug(Ljava/lang/String;)V

    goto :goto_3

    :pswitch_2
    invoke-direct {p0, v2}, Lcom/konka/systemsetting/individ/IndividTimeSetting;->showDialog(I)V

    const-string v0, "DateTimeSetting: showDialog(DATE_PICKER_DLG)."

    invoke-static {v0}, Lcom/konka/debuginfo/logPrint;->Debug(Ljava/lang/String;)V

    goto :goto_3

    :pswitch_3
    const/4 v0, 0x2

    invoke-direct {p0, v0}, Lcom/konka/systemsetting/individ/IndividTimeSetting;->showDialog(I)V

    const-string v0, "DateTimeSetting: showDialog(TIME_PICKER_DLG);."

    invoke-static {v0}, Lcom/konka/debuginfo/logPrint;->Debug(Ljava/lang/String;)V

    goto :goto_3

    :pswitch_4
    const/4 v0, 0x3

    invoke-direct {p0, v0}, Lcom/konka/systemsetting/individ/IndividTimeSetting;->showDialog(I)V

    const-string v0, "DateTimeSetting: showDialog(HALT_TIME_PICKER_DLG);."

    invoke-static {v0}, Lcom/konka/debuginfo/logPrint;->Debug(Ljava/lang/String;)V

    goto :goto_3

    :pswitch_5
    const/4 v0, 0x4

    invoke-direct {p0, v0}, Lcom/konka/systemsetting/individ/IndividTimeSetting;->showDialog(I)V

    const-string v0, "DateTimeSetting: showDialog(BOOT_TIME_PICKER_DLG);."

    invoke-static {v0}, Lcom/konka/debuginfo/logPrint;->Debug(Ljava/lang/String;)V

    goto :goto_3

    :pswitch_6
    invoke-direct {p0}, Lcom/konka/systemsetting/individ/IndividTimeSetting;->showChannelPickerDialog()V

    const-string v0, "DateTimeSetting: showChannelPickerDialog."

    invoke-static {v0}, Lcom/konka/debuginfo/logPrint;->Debug(Ljava/lang/String;)V

    goto :goto_3

    :pswitch_7
    invoke-direct {p0}, Lcom/konka/systemsetting/individ/IndividTimeSetting;->showTimeZonePickerDialog()V

    const-string v0, "DateTimeSetting: showTimePickerDialog."

    invoke-static {v0}, Lcom/konka/debuginfo/logPrint;->Debug(Ljava/lang/String;)V

    goto :goto_3

    :pswitch_data_0
    .packed-switch 0x7f090026
        :pswitch_0    # com.konka.systemsetting.R.id.sys_individ_timing_item_autoadjust
        :pswitch_2    # com.konka.systemsetting.R.id.sys_individ_timing_item_datesetting
        :pswitch_3    # com.konka.systemsetting.R.id.sys_individ_timing_item_timesetting
        :pswitch_7    # com.konka.systemsetting.R.id.sys_individ_timing_item_timezonesetting
        :pswitch_1    # com.konka.systemsetting.R.id.sys_individ_timing_item_haltswitch
        :pswitch_4    # com.konka.systemsetting.R.id.sys_individ_timing_item_halttime
        :pswitch_1    # com.konka.systemsetting.R.id.sys_individ_timing_item_bootswitch
        :pswitch_5    # com.konka.systemsetting.R.id.sys_individ_timing_item_boottime
        :pswitch_6    # com.konka.systemsetting.R.id.sys_individ_timing_item_bootchannel
    .end packed-switch
.end method

.method private refreshUISate()V
    .locals 7

    const v6, 0x7f09002e    # com.konka.systemsetting.R.id.sys_individ_timing_item_bootchannel

    const v5, 0x7f09002c    # com.konka.systemsetting.R.id.sys_individ_timing_item_bootswitch

    const v4, 0x7f09002a    # com.konka.systemsetting.R.id.sys_individ_timing_item_haltswitch

    const v3, 0x7f090028    # com.konka.systemsetting.R.id.sys_individ_timing_item_timesetting

    const/4 v2, 0x1

    iget-object v0, p0, Lcom/konka/systemsetting/individ/IndividTimeSetting;->itemAutoAdjust:Landroid/widget/LinearLayout;

    invoke-direct {p0, v0, v2}, Lcom/konka/systemsetting/individ/IndividTimeSetting;->setImageItemEnabled(Landroid/widget/LinearLayout;Z)V

    iget-object v0, p0, Lcom/konka/systemsetting/individ/IndividTimeSetting;->itemDateSetting:Landroid/widget/LinearLayout;

    invoke-virtual {v0, v3}, Landroid/widget/LinearLayout;->setNextFocusDownId(I)V

    iget-object v0, p0, Lcom/konka/systemsetting/individ/IndividTimeSetting;->itemDateSetting:Landroid/widget/LinearLayout;

    const v1, 0x7f090026    # com.konka.systemsetting.R.id.sys_individ_timing_item_autoadjust

    invoke-virtual {v0, v1}, Landroid/widget/LinearLayout;->setNextFocusUpId(I)V

    iget-object v0, p0, Lcom/konka/systemsetting/individ/IndividTimeSetting;->itemTimeSetting:Landroid/widget/LinearLayout;

    const v1, 0x7f090027    # com.konka.systemsetting.R.id.sys_individ_timing_item_datesetting

    invoke-virtual {v0, v1}, Landroid/widget/LinearLayout;->setNextFocusUpId(I)V

    iget-object v0, p0, Lcom/konka/systemsetting/individ/IndividTimeSetting;->itemTimeSetting:Landroid/widget/LinearLayout;

    const v1, 0x7f090029    # com.konka.systemsetting.R.id.sys_individ_timing_item_timezonesetting

    invoke-virtual {v0, v1}, Landroid/widget/LinearLayout;->setNextFocusDownId(I)V

    iget-object v0, p0, Lcom/konka/systemsetting/individ/IndividTimeSetting;->itemTimeZoneSetting:Landroid/widget/LinearLayout;

    invoke-direct {p0, v0, v2}, Lcom/konka/systemsetting/individ/IndividTimeSetting;->setImageItemEnabled(Landroid/widget/LinearLayout;Z)V

    iget-object v0, p0, Lcom/konka/systemsetting/individ/IndividTimeSetting;->itemTimeZoneSetting:Landroid/widget/LinearLayout;

    invoke-virtual {v0, v3}, Landroid/widget/LinearLayout;->setNextFocusUpId(I)V

    iget-object v0, p0, Lcom/konka/systemsetting/individ/IndividTimeSetting;->itemTimeZoneSetting:Landroid/widget/LinearLayout;

    invoke-virtual {v0, v4}, Landroid/widget/LinearLayout;->setNextFocusDownId(I)V

    iget-object v0, p0, Lcom/konka/systemsetting/individ/IndividTimeSetting;->itemHaltSwitch:Landroid/widget/LinearLayout;

    invoke-direct {p0, v0, v2}, Lcom/konka/systemsetting/individ/IndividTimeSetting;->setImageItemEnabled(Landroid/widget/LinearLayout;Z)V

    iget-object v0, p0, Lcom/konka/systemsetting/individ/IndividTimeSetting;->itemHaltTime:Landroid/widget/LinearLayout;

    invoke-virtual {v0, v5}, Landroid/widget/LinearLayout;->setNextFocusDownId(I)V

    iget-object v0, p0, Lcom/konka/systemsetting/individ/IndividTimeSetting;->itemHaltTime:Landroid/widget/LinearLayout;

    invoke-virtual {v0, v4}, Landroid/widget/LinearLayout;->setNextFocusUpId(I)V

    iget-object v0, p0, Lcom/konka/systemsetting/individ/IndividTimeSetting;->itemBootSwitch:Landroid/widget/LinearLayout;

    invoke-direct {p0, v0, v2}, Lcom/konka/systemsetting/individ/IndividTimeSetting;->setImageItemEnabled(Landroid/widget/LinearLayout;Z)V

    iget-object v0, p0, Lcom/konka/systemsetting/individ/IndividTimeSetting;->itemBootTime:Landroid/widget/LinearLayout;

    invoke-virtual {v0, v6}, Landroid/widget/LinearLayout;->setNextFocusDownId(I)V

    iget-object v0, p0, Lcom/konka/systemsetting/individ/IndividTimeSetting;->itemBootTime:Landroid/widget/LinearLayout;

    invoke-virtual {v0, v5}, Landroid/widget/LinearLayout;->setNextFocusUpId(I)V

    iget-object v0, p0, Lcom/konka/systemsetting/individ/IndividTimeSetting;->itemBootChannel:Landroid/widget/LinearLayout;

    invoke-virtual {v0, v6}, Landroid/widget/LinearLayout;->setNextFocusDownId(I)V

    invoke-direct {p0}, Lcom/konka/systemsetting/individ/IndividTimeSetting;->updateAutoTimeUI()V

    invoke-direct {p0}, Lcom/konka/systemsetting/individ/IndividTimeSetting;->updateTimeZone()V

    invoke-direct {p0}, Lcom/konka/systemsetting/individ/IndividTimeSetting;->updateBootSwitchUI()V

    invoke-direct {p0}, Lcom/konka/systemsetting/individ/IndividTimeSetting;->updateHaltSwitchUI()V

    invoke-direct {p0}, Lcom/konka/systemsetting/individ/IndividTimeSetting;->updateBootChannelUI()V

    invoke-direct {p0}, Lcom/konka/systemsetting/individ/IndividTimeSetting;->updateHaltTimeUI()V

    invoke-direct {p0}, Lcom/konka/systemsetting/individ/IndividTimeSetting;->updateBootTimeUI()V

    return-void
.end method

.method private registerDTReceiver()V
    .locals 4

    const/4 v3, 0x0

    new-instance v0, Landroid/content/IntentFilter;

    invoke-direct {v0}, Landroid/content/IntentFilter;-><init>()V

    const-string v1, "android.intent.action.TIME_TICK"

    invoke-virtual {v0, v1}, Landroid/content/IntentFilter;->addAction(Ljava/lang/String;)V

    const-string v1, "android.intent.action.TIME_SET"

    invoke-virtual {v0, v1}, Landroid/content/IntentFilter;->addAction(Ljava/lang/String;)V

    const-string v1, "android.intent.action.TIMEZONE_CHANGED"

    invoke-virtual {v0, v1}, Landroid/content/IntentFilter;->addAction(Ljava/lang/String;)V

    const-string v1, "CHANGE_TIME_FROM_DTV"

    invoke-virtual {v0, v1}, Landroid/content/IntentFilter;->addAction(Ljava/lang/String;)V

    iget-object v1, p0, Lcom/konka/systemsetting/individ/IndividTimeSetting;->mContext:Lcom/konka/systemsetting/MainActivity;

    iget-object v2, p0, Lcom/konka/systemsetting/individ/IndividTimeSetting;->mDateTimeReceiver:Landroid/content/BroadcastReceiver;

    invoke-virtual {v1, v2, v0, v3, v3}, Lcom/konka/systemsetting/MainActivity;->registerReceiver(Landroid/content/BroadcastReceiver;Landroid/content/IntentFilter;Ljava/lang/String;Landroid/os/Handler;)Landroid/content/Intent;

    iget-object v1, p0, Lcom/konka/systemsetting/individ/IndividTimeSetting;->mContext:Lcom/konka/systemsetting/MainActivity;

    invoke-virtual {p0, v1}, Lcom/konka/systemsetting/individ/IndividTimeSetting;->updateTimeAndDateDisplay(Landroid/content/Context;)V

    return-void
.end method

.method private setAutoTime(Z)V
    .locals 3
    .param p1    # Z

    iget-object v0, p0, Lcom/konka/systemsetting/individ/IndividTimeSetting;->mContext:Lcom/konka/systemsetting/MainActivity;

    invoke-virtual {v0}, Lcom/konka/systemsetting/MainActivity;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v1

    const-string v2, "auto_time"

    if-eqz p1, :cond_0

    const/4 v0, 0x1

    :goto_0
    invoke-static {v1, v2, v0}, Landroid/provider/Settings$Global;->putInt(Landroid/content/ContentResolver;Ljava/lang/String;I)Z

    new-instance v0, Ljava/lang/StringBuilder;

    const-string v1, "xxxxx==>time after Settings.Global.AUTO_TIME:"

    invoke-direct {v0, v1}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-direct {p0}, Lcom/konka/systemsetting/individ/IndividTimeSetting;->getTimeString()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Lcom/konka/debuginfo/logPrint;->Debug(Ljava/lang/String;)V

    return-void

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method private setDate(III)V
    .locals 7
    .param p1    # I
    .param p2    # I
    .param p3    # I

    invoke-static {}, Ljava/util/Calendar;->getInstance()Ljava/util/Calendar;

    move-result-object v0

    const/4 v3, 0x1

    invoke-virtual {v0, v3, p1}, Ljava/util/Calendar;->set(II)V

    const/4 v3, 0x2

    invoke-virtual {v0, v3, p2}, Ljava/util/Calendar;->set(II)V

    const/4 v3, 0x5

    invoke-virtual {v0, v3, p3}, Ljava/util/Calendar;->set(II)V

    invoke-virtual {v0}, Ljava/util/Calendar;->getTimeInMillis()J

    move-result-wide v1

    invoke-direct {p0, v1, v2}, Lcom/konka/systemsetting/individ/IndividTimeSetting;->setTVTime(J)V

    const-wide/16 v3, 0x3e8

    div-long v3, v1, v3

    const-wide/32 v5, 0x7fffffff

    cmp-long v3, v3, v5

    if-gez v3, :cond_0

    invoke-static {v1, v2}, Landroid/os/SystemClock;->setCurrentTimeMillis(J)Z

    :cond_0
    return-void
.end method

.method private setDefaults()V
    .locals 2

    :try_start_0
    iget-object v1, p0, Lcom/konka/systemsetting/individ/IndividTimeSetting;->tMgr:Lcom/mstar/android/tvapi/common/TimerManager;

    invoke-virtual {v1}, Lcom/mstar/android/tvapi/common/TimerManager;->setTimeFormat24HRs()V
    :try_end_0
    .catch Lcom/mstar/android/tvapi/common/exception/TvCommonException; {:try_start_0 .. :try_end_0} :catch_0

    :goto_0
    return-void

    :catch_0
    move-exception v0

    invoke-virtual {v0}, Lcom/mstar/android/tvapi/common/exception/TvCommonException;->printStackTrace()V

    goto :goto_0
.end method

.method private setImageItemEnabled(Landroid/widget/LinearLayout;Z)V
    .locals 2
    .param p1    # Landroid/widget/LinearLayout;
    .param p2    # Z

    const/4 v1, 0x0

    invoke-virtual {p1, v1}, Landroid/widget/LinearLayout;->getChildAt(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    if-eqz p2, :cond_0

    const/high16 v1, -0x1000000

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setTextColor(I)V

    :goto_0
    invoke-virtual {p1, p2}, Landroid/widget/LinearLayout;->setEnabled(Z)V

    invoke-virtual {p1, p2}, Landroid/widget/LinearLayout;->setClickable(Z)V

    invoke-virtual {p1, p2}, Landroid/widget/LinearLayout;->setFocusable(Z)V

    invoke-virtual {p1, p2}, Landroid/widget/LinearLayout;->setFocusableInTouchMode(Z)V

    return-void

    :cond_0
    const v1, -0x515152

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setTextColor(I)V

    goto :goto_0
.end method

.method private setListener()V
    .locals 1

    iget-object v0, p0, Lcom/konka/systemsetting/individ/IndividTimeSetting;->itemAutoAdjust:Landroid/widget/LinearLayout;

    invoke-virtual {v0, p0}, Landroid/widget/LinearLayout;->setOnKeyListener(Landroid/view/View$OnKeyListener;)V

    iget-object v0, p0, Lcom/konka/systemsetting/individ/IndividTimeSetting;->itemDateSetting:Landroid/widget/LinearLayout;

    invoke-virtual {v0, p0}, Landroid/widget/LinearLayout;->setOnKeyListener(Landroid/view/View$OnKeyListener;)V

    iget-object v0, p0, Lcom/konka/systemsetting/individ/IndividTimeSetting;->itemTimeSetting:Landroid/widget/LinearLayout;

    invoke-virtual {v0, p0}, Landroid/widget/LinearLayout;->setOnKeyListener(Landroid/view/View$OnKeyListener;)V

    iget-object v0, p0, Lcom/konka/systemsetting/individ/IndividTimeSetting;->itemTimeZoneSetting:Landroid/widget/LinearLayout;

    invoke-virtual {v0, p0}, Landroid/widget/LinearLayout;->setOnKeyListener(Landroid/view/View$OnKeyListener;)V

    iget-object v0, p0, Lcom/konka/systemsetting/individ/IndividTimeSetting;->itemHaltSwitch:Landroid/widget/LinearLayout;

    invoke-virtual {v0, p0}, Landroid/widget/LinearLayout;->setOnKeyListener(Landroid/view/View$OnKeyListener;)V

    iget-object v0, p0, Lcom/konka/systemsetting/individ/IndividTimeSetting;->itemHaltTime:Landroid/widget/LinearLayout;

    invoke-virtual {v0, p0}, Landroid/widget/LinearLayout;->setOnKeyListener(Landroid/view/View$OnKeyListener;)V

    iget-object v0, p0, Lcom/konka/systemsetting/individ/IndividTimeSetting;->itemBootSwitch:Landroid/widget/LinearLayout;

    invoke-virtual {v0, p0}, Landroid/widget/LinearLayout;->setOnKeyListener(Landroid/view/View$OnKeyListener;)V

    iget-object v0, p0, Lcom/konka/systemsetting/individ/IndividTimeSetting;->itemBootTime:Landroid/widget/LinearLayout;

    invoke-virtual {v0, p0}, Landroid/widget/LinearLayout;->setOnKeyListener(Landroid/view/View$OnKeyListener;)V

    iget-object v0, p0, Lcom/konka/systemsetting/individ/IndividTimeSetting;->itemBootChannel:Landroid/widget/LinearLayout;

    invoke-virtual {v0, p0}, Landroid/widget/LinearLayout;->setOnKeyListener(Landroid/view/View$OnKeyListener;)V

    iget-object v0, p0, Lcom/konka/systemsetting/individ/IndividTimeSetting;->itemAutoAdjust:Landroid/widget/LinearLayout;

    invoke-virtual {v0, p0}, Landroid/widget/LinearLayout;->setOnFocusChangeListener(Landroid/view/View$OnFocusChangeListener;)V

    iget-object v0, p0, Lcom/konka/systemsetting/individ/IndividTimeSetting;->itemDateSetting:Landroid/widget/LinearLayout;

    invoke-virtual {v0, p0}, Landroid/widget/LinearLayout;->setOnFocusChangeListener(Landroid/view/View$OnFocusChangeListener;)V

    iget-object v0, p0, Lcom/konka/systemsetting/individ/IndividTimeSetting;->itemTimeSetting:Landroid/widget/LinearLayout;

    invoke-virtual {v0, p0}, Landroid/widget/LinearLayout;->setOnFocusChangeListener(Landroid/view/View$OnFocusChangeListener;)V

    iget-object v0, p0, Lcom/konka/systemsetting/individ/IndividTimeSetting;->itemTimeZoneSetting:Landroid/widget/LinearLayout;

    invoke-virtual {v0, p0}, Landroid/widget/LinearLayout;->setOnFocusChangeListener(Landroid/view/View$OnFocusChangeListener;)V

    iget-object v0, p0, Lcom/konka/systemsetting/individ/IndividTimeSetting;->itemHaltSwitch:Landroid/widget/LinearLayout;

    invoke-virtual {v0, p0}, Landroid/widget/LinearLayout;->setOnFocusChangeListener(Landroid/view/View$OnFocusChangeListener;)V

    iget-object v0, p0, Lcom/konka/systemsetting/individ/IndividTimeSetting;->itemHaltTime:Landroid/widget/LinearLayout;

    invoke-virtual {v0, p0}, Landroid/widget/LinearLayout;->setOnFocusChangeListener(Landroid/view/View$OnFocusChangeListener;)V

    iget-object v0, p0, Lcom/konka/systemsetting/individ/IndividTimeSetting;->itemBootSwitch:Landroid/widget/LinearLayout;

    invoke-virtual {v0, p0}, Landroid/widget/LinearLayout;->setOnFocusChangeListener(Landroid/view/View$OnFocusChangeListener;)V

    iget-object v0, p0, Lcom/konka/systemsetting/individ/IndividTimeSetting;->itemBootTime:Landroid/widget/LinearLayout;

    invoke-virtual {v0, p0}, Landroid/widget/LinearLayout;->setOnFocusChangeListener(Landroid/view/View$OnFocusChangeListener;)V

    iget-object v0, p0, Lcom/konka/systemsetting/individ/IndividTimeSetting;->itemBootChannel:Landroid/widget/LinearLayout;

    invoke-virtual {v0, p0}, Landroid/widget/LinearLayout;->setOnFocusChangeListener(Landroid/view/View$OnFocusChangeListener;)V

    iget-object v0, p0, Lcom/konka/systemsetting/individ/IndividTimeSetting;->itemAutoAdjust:Landroid/widget/LinearLayout;

    invoke-virtual {v0, p0}, Landroid/widget/LinearLayout;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    iget-object v0, p0, Lcom/konka/systemsetting/individ/IndividTimeSetting;->itemDateSetting:Landroid/widget/LinearLayout;

    invoke-virtual {v0, p0}, Landroid/widget/LinearLayout;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    iget-object v0, p0, Lcom/konka/systemsetting/individ/IndividTimeSetting;->itemTimeSetting:Landroid/widget/LinearLayout;

    invoke-virtual {v0, p0}, Landroid/widget/LinearLayout;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    iget-object v0, p0, Lcom/konka/systemsetting/individ/IndividTimeSetting;->itemTimeZoneSetting:Landroid/widget/LinearLayout;

    invoke-virtual {v0, p0}, Landroid/widget/LinearLayout;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    iget-object v0, p0, Lcom/konka/systemsetting/individ/IndividTimeSetting;->itemHaltSwitch:Landroid/widget/LinearLayout;

    invoke-virtual {v0, p0}, Landroid/widget/LinearLayout;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    iget-object v0, p0, Lcom/konka/systemsetting/individ/IndividTimeSetting;->itemHaltTime:Landroid/widget/LinearLayout;

    invoke-virtual {v0, p0}, Landroid/widget/LinearLayout;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    iget-object v0, p0, Lcom/konka/systemsetting/individ/IndividTimeSetting;->itemBootSwitch:Landroid/widget/LinearLayout;

    invoke-virtual {v0, p0}, Landroid/widget/LinearLayout;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    iget-object v0, p0, Lcom/konka/systemsetting/individ/IndividTimeSetting;->itemBootTime:Landroid/widget/LinearLayout;

    invoke-virtual {v0, p0}, Landroid/widget/LinearLayout;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    iget-object v0, p0, Lcom/konka/systemsetting/individ/IndividTimeSetting;->itemBootChannel:Landroid/widget/LinearLayout;

    invoke-virtual {v0, p0}, Landroid/widget/LinearLayout;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    return-void
.end method

.method private setOptionItemEnabled(Landroid/widget/LinearLayout;Z)V
    .locals 5
    .param p1    # Landroid/widget/LinearLayout;
    .param p2    # Z

    const v4, -0x515152

    const/high16 v3, -0x1000000

    const/4 v2, 0x0

    invoke-virtual {p1, v2}, Landroid/widget/LinearLayout;->getChildAt(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    const/4 v2, 0x1

    invoke-virtual {p1, v2}, Landroid/widget/LinearLayout;->getChildAt(I)Landroid/view/View;

    move-result-object v1

    check-cast v1, Landroid/widget/TextView;

    if-eqz p2, :cond_0

    invoke-virtual {v0, v3}, Landroid/widget/TextView;->setTextColor(I)V

    invoke-virtual {v1, v3}, Landroid/widget/TextView;->setTextColor(I)V

    :goto_0
    invoke-virtual {p1, p2}, Landroid/widget/LinearLayout;->setEnabled(Z)V

    invoke-virtual {p1, p2}, Landroid/widget/LinearLayout;->setClickable(Z)V

    invoke-virtual {p1, p2}, Landroid/widget/LinearLayout;->setFocusable(Z)V

    invoke-virtual {p1, p2}, Landroid/widget/LinearLayout;->setFocusableInTouchMode(Z)V

    return-void

    :cond_0
    invoke-virtual {v0, v4}, Landroid/widget/TextView;->setTextColor(I)V

    invoke-virtual {v1, v4}, Landroid/widget/TextView;->setTextColor(I)V

    goto :goto_0
.end method

.method private setTVTime(J)V
    .locals 5
    .param p1    # J

    invoke-static {}, Ljava/util/TimeZone;->getDefault()Ljava/util/TimeZone;

    move-result-object v3

    invoke-virtual {v3}, Ljava/util/TimeZone;->getRawOffset()I

    move-result v1

    new-instance v2, Landroid/text/format/Time;

    invoke-direct {v2}, Landroid/text/format/Time;-><init>()V

    invoke-virtual {v2, p1, p2}, Landroid/text/format/Time;->set(J)V

    iget v3, v2, Landroid/text/format/Time;->month:I

    add-int/lit8 v3, v3, 0x1

    iput v3, v2, Landroid/text/format/Time;->month:I

    :try_start_0
    iget-object v3, p0, Lcom/konka/systemsetting/individ/IndividTimeSetting;->tMgr:Lcom/mstar/android/tvapi/common/TimerManager;

    const/4 v4, 0x1

    invoke-virtual {v3, v2, v4}, Lcom/mstar/android/tvapi/common/TimerManager;->setClkTime(Landroid/text/format/Time;Z)V
    :try_end_0
    .catch Lcom/mstar/android/tvapi/common/exception/TvCommonException; {:try_start_0 .. :try_end_0} :catch_0

    :goto_0
    return-void

    :catch_0
    move-exception v0

    invoke-virtual {v0}, Lcom/mstar/android/tvapi/common/exception/TvCommonException;->printStackTrace()V

    goto :goto_0
.end method

.method private setTime(II)V
    .locals 7
    .param p1    # I
    .param p2    # I

    const/4 v4, 0x0

    invoke-static {}, Ljava/util/Calendar;->getInstance()Ljava/util/Calendar;

    move-result-object v0

    const/16 v3, 0xb

    invoke-virtual {v0, v3, p1}, Ljava/util/Calendar;->set(II)V

    const/16 v3, 0xc

    invoke-virtual {v0, v3, p2}, Ljava/util/Calendar;->set(II)V

    const/16 v3, 0xd

    invoke-virtual {v0, v3, v4}, Ljava/util/Calendar;->set(II)V

    const/16 v3, 0xe

    invoke-virtual {v0, v3, v4}, Ljava/util/Calendar;->set(II)V

    invoke-virtual {v0}, Ljava/util/Calendar;->getTimeInMillis()J

    move-result-wide v1

    invoke-direct {p0, v1, v2}, Lcom/konka/systemsetting/individ/IndividTimeSetting;->setTVTime(J)V

    const-wide/16 v3, 0x3e8

    div-long v3, v1, v3

    const-wide/32 v5, 0x7fffffff

    cmp-long v3, v3, v5

    if-gez v3, :cond_0

    invoke-static {v1, v2}, Landroid/os/SystemClock;->setCurrentTimeMillis(J)Z

    :cond_0
    return-void
.end method

.method private setTimeMillis(J)V
    .locals 4
    .param p1    # J

    invoke-direct {p0, p1, p2}, Lcom/konka/systemsetting/individ/IndividTimeSetting;->setTVTime(J)V

    const-wide/16 v0, 0x3e8

    div-long v0, p1, v0

    const-wide/32 v2, 0x7fffffff

    cmp-long v0, v0, v2

    if-gez v0, :cond_0

    invoke-static {p1, p2}, Landroid/os/SystemClock;->setCurrentTimeMillis(J)Z

    :cond_0
    return-void
.end method

.method private showChannelPickerDialog()V
    .locals 4

    new-instance v0, Landroid/app/AlertDialog$Builder;

    iget-object v2, p0, Lcom/konka/systemsetting/individ/IndividTimeSetting;->mContext:Lcom/konka/systemsetting/MainActivity;

    invoke-direct {v0, v2}, Landroid/app/AlertDialog$Builder;-><init>(Landroid/content/Context;)V

    const v2, 0x7f06001a    # com.konka.systemsetting.R.string.str_individ_timing_item_boot_channel

    invoke-virtual {v0, v2}, Landroid/app/AlertDialog$Builder;->setTitle(I)Landroid/app/AlertDialog$Builder;

    const v2, 0x7f020022    # com.konka.systemsetting.R.drawable.syssettingfirsticonpersonalunfocus

    invoke-virtual {v0, v2}, Landroid/app/AlertDialog$Builder;->setIcon(I)Landroid/app/AlertDialog$Builder;

    new-instance v1, Lcom/konka/systemsetting/individ/IndividTimeSetting$5;

    invoke-direct {v1, p0}, Lcom/konka/systemsetting/individ/IndividTimeSetting$5;-><init>(Lcom/konka/systemsetting/individ/IndividTimeSetting;)V

    iget-object v2, p0, Lcom/konka/systemsetting/individ/IndividTimeSetting;->mArrCurrent:[Ljava/lang/CharSequence;

    iget-object v3, p0, Lcom/konka/systemsetting/individ/IndividTimeSetting;->m_iBootChannelIndex:Ljava/lang/Integer;

    invoke-virtual {v3}, Ljava/lang/Integer;->intValue()I

    move-result v3

    invoke-virtual {v0, v2, v3, v1}, Landroid/app/AlertDialog$Builder;->setSingleChoiceItems([Ljava/lang/CharSequence;ILandroid/content/DialogInterface$OnClickListener;)Landroid/app/AlertDialog$Builder;

    invoke-virtual {v0}, Landroid/app/AlertDialog$Builder;->show()Landroid/app/AlertDialog;

    return-void
.end method

.method private showDialog(I)V
    .locals 14
    .param p1    # I

    new-instance v7, Landroid/text/format/Time;

    invoke-direct {v7}, Landroid/text/format/Time;-><init>()V

    invoke-virtual {v7}, Landroid/text/format/Time;->setToNow()V

    const/4 v2, 0x1

    invoke-virtual {v7, v2}, Landroid/text/format/Time;->toMillis(Z)J

    move-result-wide v2

    invoke-virtual {v7, v2, v3}, Landroid/text/format/Time;->set(J)V

    const/4 v1, 0x0

    packed-switch p1, :pswitch_data_0

    const-string v2, "KK.lxsf: illegale dialog id in DateTimeSetting!!!"

    invoke-static {v2}, Lcom/konka/debuginfo/logPrint;->Debug(Ljava/lang/String;)V

    :goto_0
    if-eqz v1, :cond_0

    invoke-virtual {v1}, Landroid/app/Dialog;->show()V

    :cond_0
    const/4 v2, 0x1

    if-ne p1, v2, :cond_2

    move-object v2, v1

    check-cast v2, Landroid/app/DatePickerDialog;

    invoke-virtual {v2}, Landroid/app/DatePickerDialog;->getDatePicker()Landroid/widget/DatePicker;

    move-result-object v2

    invoke-virtual {v2}, Landroid/widget/DatePicker;->getCalendarView()Landroid/widget/CalendarView;

    move-result-object v2

    const/16 v3, 0x8

    invoke-virtual {v2, v3}, Landroid/widget/CalendarView;->setVisibility(I)V

    check-cast v1, Landroid/app/DatePickerDialog;

    invoke-virtual {v1}, Landroid/app/DatePickerDialog;->getDatePicker()Landroid/widget/DatePicker;

    move-result-object v2

    const v3, 0x1020366    # android.R.id.titleDividerTop

    invoke-virtual {v2, v3}, Landroid/widget/DatePicker;->findViewById(I)Landroid/view/View;

    move-result-object v13

    instance-of v2, v13, Landroid/widget/NumberPicker;

    if-eqz v2, :cond_1

    move-object v2, v13

    check-cast v2, Landroid/widget/NumberPicker;

    invoke-virtual {v2}, Landroid/widget/NumberPicker;->getChildCount()I

    move-result v2

    if-lez v2, :cond_1

    check-cast v13, Landroid/widget/NumberPicker;

    const/4 v2, 0x0

    invoke-virtual {v13, v2}, Landroid/widget/NumberPicker;->getChildAt(I)Landroid/view/View;

    move-result-object v2

    invoke-virtual {v2}, Landroid/view/View;->requestFocus()Z

    :cond_1
    :goto_1
    return-void

    :pswitch_0
    new-instance v1, Landroid/app/DatePickerDialog;

    iget-object v2, p0, Lcom/konka/systemsetting/individ/IndividTimeSetting;->mContext:Lcom/konka/systemsetting/MainActivity;

    iget v4, v7, Landroid/text/format/Time;->year:I

    iget v5, v7, Landroid/text/format/Time;->month:I

    iget v6, v7, Landroid/text/format/Time;->monthDay:I

    move-object v3, p0

    invoke-direct/range {v1 .. v6}, Landroid/app/DatePickerDialog;-><init>(Landroid/content/Context;Landroid/app/DatePickerDialog$OnDateSetListener;III)V

    const v2, 0x7f060012    # com.konka.systemsetting.R.string.str_individ_timing_item_datesetting

    invoke-virtual {v1, v2}, Landroid/app/Dialog;->setTitle(I)V

    goto :goto_0

    :pswitch_1
    new-instance v1, Landroid/app/TimePickerDialog;

    iget-object v2, p0, Lcom/konka/systemsetting/individ/IndividTimeSetting;->mContext:Lcom/konka/systemsetting/MainActivity;

    iget v4, v7, Landroid/text/format/Time;->hour:I

    iget v5, v7, Landroid/text/format/Time;->minute:I

    const/4 v6, 0x1

    move-object v3, p0

    invoke-direct/range {v1 .. v6}, Landroid/app/TimePickerDialog;-><init>(Landroid/content/Context;Landroid/app/TimePickerDialog$OnTimeSetListener;IIZ)V

    const v2, 0x7f060013    # com.konka.systemsetting.R.string.str_individ_timing_item_timesetting

    invoke-virtual {v1, v2}, Landroid/app/Dialog;->setTitle(I)V

    goto :goto_0

    :pswitch_2
    new-instance v1, Landroid/app/TimePickerDialog;

    iget-object v2, p0, Lcom/konka/systemsetting/individ/IndividTimeSetting;->mContext:Lcom/konka/systemsetting/MainActivity;

    iget-object v3, p0, Lcom/konka/systemsetting/individ/IndividTimeSetting;->onHalTimeSetListener:Landroid/app/TimePickerDialog$OnTimeSetListener;

    iget-object v4, p0, Lcom/konka/systemsetting/individ/IndividTimeSetting;->timerOff:Lcom/mstar/android/tvapi/common/vo/TimerPowerOffModeStatus;

    iget v4, v4, Lcom/mstar/android/tvapi/common/vo/TimerPowerOffModeStatus;->hour:I

    iget-object v5, p0, Lcom/konka/systemsetting/individ/IndividTimeSetting;->timerOff:Lcom/mstar/android/tvapi/common/vo/TimerPowerOffModeStatus;

    iget v5, v5, Lcom/mstar/android/tvapi/common/vo/TimerPowerOffModeStatus;->minute:I

    const/4 v6, 0x1

    invoke-direct/range {v1 .. v6}, Landroid/app/TimePickerDialog;-><init>(Landroid/content/Context;Landroid/app/TimePickerDialog$OnTimeSetListener;IIZ)V

    const v2, 0x7f060017    # com.konka.systemsetting.R.string.str_individ_timing_item_halttime

    invoke-virtual {v1, v2}, Landroid/app/Dialog;->setTitle(I)V

    goto :goto_0

    :pswitch_3
    new-instance v1, Landroid/app/TimePickerDialog;

    iget-object v2, p0, Lcom/konka/systemsetting/individ/IndividTimeSetting;->mContext:Lcom/konka/systemsetting/MainActivity;

    iget-object v3, p0, Lcom/konka/systemsetting/individ/IndividTimeSetting;->onBootTimeSetListener:Landroid/app/TimePickerDialog$OnTimeSetListener;

    iget-object v4, p0, Lcom/konka/systemsetting/individ/IndividTimeSetting;->timerOn:Lcom/mstar/android/tvapi/common/vo/TimerPowerOn;

    iget v4, v4, Lcom/mstar/android/tvapi/common/vo/TimerPowerOn;->hour:I

    iget-object v5, p0, Lcom/konka/systemsetting/individ/IndividTimeSetting;->timerOn:Lcom/mstar/android/tvapi/common/vo/TimerPowerOn;

    iget v5, v5, Lcom/mstar/android/tvapi/common/vo/TimerPowerOn;->minute:I

    const/4 v6, 0x1

    invoke-direct/range {v1 .. v6}, Landroid/app/TimePickerDialog;-><init>(Landroid/content/Context;Landroid/app/TimePickerDialog$OnTimeSetListener;IIZ)V

    const v2, 0x7f060019    # com.konka.systemsetting.R.string.str_individ_timing_item_boottime

    invoke-virtual {v1, v2}, Landroid/app/Dialog;->setTitle(I)V

    goto/16 :goto_0

    :cond_2
    const/4 v2, 0x2

    if-eq p1, v2, :cond_3

    const/4 v2, 0x3

    if-eq p1, v2, :cond_3

    const/4 v2, 0x4

    if-ne p1, v2, :cond_1

    :cond_3
    invoke-virtual {v1}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v8

    const/4 v12, 0x0

    :try_start_0
    const-string v2, "mTimePicker"

    invoke-virtual {v8, v2}, Ljava/lang/Class;->getDeclaredField(Ljava/lang/String;)Ljava/lang/reflect/Field;

    move-result-object v10

    const/4 v2, 0x1

    invoke-virtual {v10, v2}, Ljava/lang/reflect/Field;->setAccessible(Z)V

    invoke-virtual {v10, v1}, Ljava/lang/reflect/Field;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v2

    move-object v0, v2

    check-cast v0, Landroid/widget/TimePicker;

    move-object v12, v0
    :try_end_0
    .catch Ljava/lang/NoSuchFieldException; {:try_start_0 .. :try_end_0} :catch_0
    .catch Ljava/lang/IllegalArgumentException; {:try_start_0 .. :try_end_0} :catch_1
    .catch Ljava/lang/IllegalAccessException; {:try_start_0 .. :try_end_0} :catch_2

    :goto_2
    if-eqz v12, :cond_1

    invoke-virtual {v12}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v11

    const/4 v13, 0x0

    :try_start_1
    const-string v2, "mHourSpinner"

    invoke-virtual {v11, v2}, Ljava/lang/Class;->getDeclaredField(Ljava/lang/String;)Ljava/lang/reflect/Field;

    move-result-object v10

    const/4 v2, 0x1

    invoke-virtual {v10, v2}, Ljava/lang/reflect/Field;->setAccessible(Z)V

    invoke-virtual {v10, v12}, Ljava/lang/reflect/Field;->get(Ljava/lang/Object;)Ljava/lang/Object;
    :try_end_1
    .catch Ljava/lang/Exception; {:try_start_1 .. :try_end_1} :catch_3

    move-result-object v13

    :goto_3
    instance-of v2, v13, Landroid/widget/NumberPicker;

    if-eqz v2, :cond_1

    move-object v2, v13

    check-cast v2, Landroid/widget/NumberPicker;

    invoke-virtual {v2}, Landroid/widget/NumberPicker;->getChildCount()I

    move-result v2

    if-lez v2, :cond_1

    check-cast v13, Landroid/widget/NumberPicker;

    const/4 v2, 0x0

    invoke-virtual {v13, v2}, Landroid/widget/NumberPicker;->getChildAt(I)Landroid/view/View;

    move-result-object v2

    invoke-virtual {v2}, Landroid/view/View;->requestFocus()Z

    goto/16 :goto_1

    :catch_0
    move-exception v9

    invoke-virtual {v9}, Ljava/lang/NoSuchFieldException;->printStackTrace()V

    goto :goto_2

    :catch_1
    move-exception v9

    invoke-virtual {v9}, Ljava/lang/IllegalArgumentException;->printStackTrace()V

    goto :goto_2

    :catch_2
    move-exception v9

    invoke-virtual {v9}, Ljava/lang/IllegalAccessException;->printStackTrace()V

    goto :goto_2

    :catch_3
    move-exception v9

    invoke-virtual {v9}, Ljava/lang/Exception;->printStackTrace()V

    goto :goto_3

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
        :pswitch_1
        :pswitch_2
        :pswitch_3
    .end packed-switch
.end method

.method private showTimeZonePickerDialog()V
    .locals 4

    new-instance v0, Landroid/app/AlertDialog$Builder;

    iget-object v2, p0, Lcom/konka/systemsetting/individ/IndividTimeSetting;->mContext:Lcom/konka/systemsetting/MainActivity;

    invoke-direct {v0, v2}, Landroid/app/AlertDialog$Builder;-><init>(Landroid/content/Context;)V

    const v2, 0x7f06001b    # com.konka.systemsetting.R.string.str_individ_timing_item_boot_timezone

    invoke-virtual {v0, v2}, Landroid/app/AlertDialog$Builder;->setTitle(I)Landroid/app/AlertDialog$Builder;

    const v2, 0x7f020022    # com.konka.systemsetting.R.drawable.syssettingfirsticonpersonalunfocus

    invoke-virtual {v0, v2}, Landroid/app/AlertDialog$Builder;->setIcon(I)Landroid/app/AlertDialog$Builder;

    new-instance v1, Lcom/konka/systemsetting/individ/IndividTimeSetting$6;

    invoke-direct {v1, p0}, Lcom/konka/systemsetting/individ/IndividTimeSetting$6;-><init>(Lcom/konka/systemsetting/individ/IndividTimeSetting;)V

    iget-object v2, p0, Lcom/konka/systemsetting/individ/IndividTimeSetting;->mContext:Lcom/konka/systemsetting/MainActivity;

    invoke-virtual {p0, v2}, Lcom/konka/systemsetting/individ/IndividTimeSetting;->constructTimezoneAdapter(Landroid/content/Context;)Landroid/widget/SimpleAdapter;

    move-result-object v2

    iget-object v3, p0, Lcom/konka/systemsetting/individ/IndividTimeSetting;->m_iTimeZoneSelectIndex:Ljava/lang/Integer;

    invoke-virtual {v3}, Ljava/lang/Integer;->intValue()I

    move-result v3

    invoke-virtual {v0, v2, v3, v1}, Landroid/app/AlertDialog$Builder;->setSingleChoiceItems(Landroid/widget/ListAdapter;ILandroid/content/DialogInterface$OnClickListener;)Landroid/app/AlertDialog$Builder;

    invoke-virtual {v0}, Landroid/app/AlertDialog$Builder;->show()Landroid/app/AlertDialog;

    return-void
.end method

.method private timeToString(Landroid/text/format/Time;)Ljava/lang/String;
    .locals 2
    .param p1    # Landroid/text/format/Time;

    new-instance v0, Ljava/lang/StringBuilder;

    iget v1, p1, Landroid/text/format/Time;->year:I

    invoke-static {v1}, Ljava/lang/String;->valueOf(I)Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    const-string v1, "-"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget v1, p1, Landroid/text/format/Time;->month:I

    invoke-virtual {p0, v1}, Lcom/konka/systemsetting/individ/IndividTimeSetting;->formatTimeField(I)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, "-"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget v1, p1, Landroid/text/format/Time;->monthDay:I

    invoke-virtual {p0, v1}, Lcom/konka/systemsetting/individ/IndividTimeSetting;->formatTimeField(I)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, "  "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget v1, p1, Landroid/text/format/Time;->hour:I

    invoke-virtual {p0, v1}, Lcom/konka/systemsetting/individ/IndividTimeSetting;->formatTimeField(I)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, ":"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget v1, p1, Landroid/text/format/Time;->minute:I

    invoke-virtual {p0, v1}, Lcom/konka/systemsetting/individ/IndividTimeSetting;->formatTimeField(I)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, ":"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget v1, p1, Landroid/text/format/Time;->second:I

    invoke-virtual {p0, v1}, Lcom/konka/systemsetting/individ/IndividTimeSetting;->formatTimeField(I)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, "\n"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method private timeUpdated()V
    .locals 3

    new-instance v0, Landroid/content/Intent;

    const-string v1, "android.intent.action.TIME_SET"

    invoke-direct {v0, v1}, Landroid/content/Intent;-><init>(Ljava/lang/String;)V

    iget-object v1, p0, Lcom/konka/systemsetting/individ/IndividTimeSetting;->mContext:Lcom/konka/systemsetting/MainActivity;

    invoke-virtual {v1, v0}, Lcom/konka/systemsetting/MainActivity;->sendBroadcast(Landroid/content/Intent;)V

    const-string v1, "timeUpdated()===>System_Time_Change"

    const-string v2, "android.intent.action.TIME_SET"

    invoke-static {v1, v2}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    return-void
.end method

.method private unregisterDTReceiver()V
    .locals 2

    iget-object v0, p0, Lcom/konka/systemsetting/individ/IndividTimeSetting;->mContext:Lcom/konka/systemsetting/MainActivity;

    iget-object v1, p0, Lcom/konka/systemsetting/individ/IndividTimeSetting;->mDateTimeReceiver:Landroid/content/BroadcastReceiver;

    invoke-virtual {v0, v1}, Lcom/konka/systemsetting/MainActivity;->unregisterReceiver(Landroid/content/BroadcastReceiver;)V

    return-void
.end method

.method private updateAutoTimeUI()V
    .locals 9

    const/4 v5, 0x0

    const/4 v6, 0x1

    iget-object v7, p0, Lcom/konka/systemsetting/individ/IndividTimeSetting;->settingDesk:Lcom/konka/kkinterface/tv/SettingDesk;

    invoke-interface {v7}, Lcom/konka/kkinterface/tv/SettingDesk;->GetSystemAutoTimeType()I

    move-result v7

    invoke-static {v7}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v7

    iput-object v7, p0, Lcom/konka/systemsetting/individ/IndividTimeSetting;->m_iAutoTimeSelectIndex:Ljava/lang/Integer;

    iget-object v7, p0, Lcom/konka/systemsetting/individ/IndividTimeSetting;->m_iAutoTimeSelectIndex:Ljava/lang/Integer;

    invoke-virtual {v7}, Ljava/lang/Integer;->intValue()I

    move-result v7

    if-nez v7, :cond_0

    move v2, v5

    :goto_0
    iget-object v7, p0, Lcom/konka/systemsetting/individ/IndividTimeSetting;->itemAutoAdjust:Landroid/widget/LinearLayout;

    invoke-virtual {v7, v6}, Landroid/widget/LinearLayout;->getChildAt(I)Landroid/view/View;

    move-result-object v1

    check-cast v1, Landroid/widget/ImageView;

    iget-object v7, p0, Lcom/konka/systemsetting/individ/IndividTimeSetting;->itemAutoAdjust:Landroid/widget/LinearLayout;

    invoke-virtual {v7, v5}, Landroid/widget/LinearLayout;->getChildAt(I)Landroid/view/View;

    move-result-object v4

    check-cast v4, Landroid/widget/TextView;

    iget-object v7, p0, Lcom/konka/systemsetting/individ/IndividTimeSetting;->nArrCurrent:[Ljava/lang/CharSequence;

    aget-object v7, v7, v6

    invoke-virtual {v4, v7}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    invoke-direct {p0, v2}, Lcom/konka/systemsetting/individ/IndividTimeSetting;->getImageIdByState(Z)I

    move-result v7

    invoke-virtual {v1, v7}, Landroid/widget/ImageView;->setImageResource(I)V

    iget-object v8, p0, Lcom/konka/systemsetting/individ/IndividTimeSetting;->itemDateSetting:Landroid/widget/LinearLayout;

    if-eqz v2, :cond_1

    move v7, v5

    :goto_1
    invoke-direct {p0, v8, v7}, Lcom/konka/systemsetting/individ/IndividTimeSetting;->setOptionItemEnabled(Landroid/widget/LinearLayout;Z)V

    iget-object v7, p0, Lcom/konka/systemsetting/individ/IndividTimeSetting;->itemTimeSetting:Landroid/widget/LinearLayout;

    if-eqz v2, :cond_2

    :goto_2
    invoke-direct {p0, v7, v5}, Lcom/konka/systemsetting/individ/IndividTimeSetting;->setOptionItemEnabled(Landroid/widget/LinearLayout;Z)V

    if-nez v2, :cond_3

    const v0, 0x7f090027    # com.konka.systemsetting.R.id.sys_individ_timing_item_datesetting

    :goto_3
    iget-object v5, p0, Lcom/konka/systemsetting/individ/IndividTimeSetting;->itemAutoAdjust:Landroid/widget/LinearLayout;

    invoke-virtual {v5, v0}, Landroid/widget/LinearLayout;->setNextFocusDownId(I)V

    if-nez v2, :cond_4

    const v3, 0x7f090028    # com.konka.systemsetting.R.id.sys_individ_timing_item_timesetting

    :goto_4
    iget-object v5, p0, Lcom/konka/systemsetting/individ/IndividTimeSetting;->itemTimeZoneSetting:Landroid/widget/LinearLayout;

    invoke-virtual {v5, v3}, Landroid/widget/LinearLayout;->setNextFocusUpId(I)V

    return-void

    :cond_0
    move v2, v6

    goto :goto_0

    :cond_1
    move v7, v6

    goto :goto_1

    :cond_2
    move v5, v6

    goto :goto_2

    :cond_3
    const v0, 0x7f090029    # com.konka.systemsetting.R.id.sys_individ_timing_item_timezonesetting

    goto :goto_3

    :cond_4
    const v3, 0x7f090026    # com.konka.systemsetting.R.id.sys_individ_timing_item_autoadjust

    goto :goto_4
.end method

.method private updateBootChannelUI()V
    .locals 3

    iget-object v1, p0, Lcom/konka/systemsetting/individ/IndividTimeSetting;->mArrCurrent:[Ljava/lang/CharSequence;

    if-eqz v1, :cond_0

    iget-object v1, p0, Lcom/konka/systemsetting/individ/IndividTimeSetting;->itemBootChannel:Landroid/widget/LinearLayout;

    const/4 v2, 0x1

    invoke-virtual {v1, v2}, Landroid/widget/LinearLayout;->getChildAt(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    iget-object v1, p0, Lcom/konka/systemsetting/individ/IndividTimeSetting;->mArrCurrent:[Ljava/lang/CharSequence;

    iget-object v2, p0, Lcom/konka/systemsetting/individ/IndividTimeSetting;->m_iBootChannelIndex:Ljava/lang/Integer;

    invoke-virtual {v2}, Ljava/lang/Integer;->intValue()I

    move-result v2

    aget-object v1, v1, v2

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    :goto_0
    return-void

    :cond_0
    const-string v1, "SystemSetting"

    const-string v2, "updateBootChannelUI-- mArrCurrent is null!"

    invoke-static {v1, v2}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_0
.end method

.method private updateBootSwitchUI()V
    .locals 4

    iget-object v2, p0, Lcom/konka/systemsetting/individ/IndividTimeSetting;->itemBootSwitch:Landroid/widget/LinearLayout;

    const/4 v3, 0x1

    invoke-virtual {v2, v3}, Landroid/widget/LinearLayout;->getChildAt(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/ImageView;

    sget-boolean v2, Lcom/konka/systemsetting/individ/IndividTimeSetting;->m_isBootSwitchOn:Z

    invoke-direct {p0, v2}, Lcom/konka/systemsetting/individ/IndividTimeSetting;->getImageIdByState(Z)I

    move-result v2

    invoke-virtual {v0, v2}, Landroid/widget/ImageView;->setImageResource(I)V

    iget-object v2, p0, Lcom/konka/systemsetting/individ/IndividTimeSetting;->itemBootTime:Landroid/widget/LinearLayout;

    sget-boolean v3, Lcom/konka/systemsetting/individ/IndividTimeSetting;->m_isBootSwitchOn:Z

    invoke-direct {p0, v2, v3}, Lcom/konka/systemsetting/individ/IndividTimeSetting;->setOptionItemEnabled(Landroid/widget/LinearLayout;Z)V

    iget-object v2, p0, Lcom/konka/systemsetting/individ/IndividTimeSetting;->itemBootChannel:Landroid/widget/LinearLayout;

    sget-boolean v3, Lcom/konka/systemsetting/individ/IndividTimeSetting;->m_isBootSwitchOn:Z

    invoke-direct {p0, v2, v3}, Lcom/konka/systemsetting/individ/IndividTimeSetting;->setOptionItemEnabled(Landroid/widget/LinearLayout;Z)V

    sget-boolean v2, Lcom/konka/systemsetting/individ/IndividTimeSetting;->m_isBootSwitchOn:Z

    if-eqz v2, :cond_0

    const v1, 0x7f09002d    # com.konka.systemsetting.R.id.sys_individ_timing_item_boottime

    :goto_0
    iget-object v2, p0, Lcom/konka/systemsetting/individ/IndividTimeSetting;->itemBootSwitch:Landroid/widget/LinearLayout;

    invoke-virtual {v2, v1}, Landroid/widget/LinearLayout;->setNextFocusDownId(I)V

    return-void

    :cond_0
    const v1, 0x7f09002c    # com.konka.systemsetting.R.id.sys_individ_timing_item_bootswitch

    goto :goto_0
.end method

.method private updateBootTimeUI()V
    .locals 3

    iget-object v1, p0, Lcom/konka/systemsetting/individ/IndividTimeSetting;->itemBootTime:Landroid/widget/LinearLayout;

    const/4 v2, 0x1

    invoke-virtual {v1, v2}, Landroid/widget/LinearLayout;->getChildAt(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    new-instance v1, Ljava/lang/StringBuilder;

    iget-object v2, p0, Lcom/konka/systemsetting/individ/IndividTimeSetting;->timerOn:Lcom/mstar/android/tvapi/common/vo/TimerPowerOn;

    iget v2, v2, Lcom/mstar/android/tvapi/common/vo/TimerPowerOn;->hour:I

    invoke-virtual {p0, v2}, Lcom/konka/systemsetting/individ/IndividTimeSetting;->formatTimeField(I)Ljava/lang/String;

    move-result-object v2

    invoke-static {v2}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v2

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    const-string v2, ":"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget-object v2, p0, Lcom/konka/systemsetting/individ/IndividTimeSetting;->timerOn:Lcom/mstar/android/tvapi/common/vo/TimerPowerOn;

    iget v2, v2, Lcom/mstar/android/tvapi/common/vo/TimerPowerOn;->minute:I

    invoke-virtual {p0, v2}, Lcom/konka/systemsetting/individ/IndividTimeSetting;->formatTimeField(I)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    return-void
.end method

.method private updateDateUI(Ljava/lang/String;)V
    .locals 3
    .param p1    # Ljava/lang/String;

    iget-object v1, p0, Lcom/konka/systemsetting/individ/IndividTimeSetting;->itemDateSetting:Landroid/widget/LinearLayout;

    const/4 v2, 0x1

    invoke-virtual {v1, v2}, Landroid/widget/LinearLayout;->getChildAt(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    invoke-virtual {v0, p1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    return-void
.end method

.method private updateHaltSwitchUI()V
    .locals 6

    const v3, 0x7f09002b    # com.konka.systemsetting.R.id.sys_individ_timing_item_halttime

    iget-object v4, p0, Lcom/konka/systemsetting/individ/IndividTimeSetting;->itemHaltSwitch:Landroid/widget/LinearLayout;

    const/4 v5, 0x1

    invoke-virtual {v4, v5}, Landroid/widget/LinearLayout;->getChildAt(I)Landroid/view/View;

    move-result-object v2

    check-cast v2, Landroid/widget/ImageView;

    sget-boolean v4, Lcom/konka/systemsetting/individ/IndividTimeSetting;->m_isHaltSwitchOn:Z

    invoke-direct {p0, v4}, Lcom/konka/systemsetting/individ/IndividTimeSetting;->getImageIdByState(Z)I

    move-result v4

    invoke-virtual {v2, v4}, Landroid/widget/ImageView;->setImageResource(I)V

    iget-object v4, p0, Lcom/konka/systemsetting/individ/IndividTimeSetting;->itemHaltTime:Landroid/widget/LinearLayout;

    sget-boolean v5, Lcom/konka/systemsetting/individ/IndividTimeSetting;->m_isHaltSwitchOn:Z

    invoke-direct {p0, v4, v5}, Lcom/konka/systemsetting/individ/IndividTimeSetting;->setOptionItemEnabled(Landroid/widget/LinearLayout;Z)V

    sget-boolean v4, Lcom/konka/systemsetting/individ/IndividTimeSetting;->m_isHaltSwitchOn:Z

    if-eqz v4, :cond_0

    move v1, v3

    :goto_0
    sget-boolean v4, Lcom/konka/systemsetting/individ/IndividTimeSetting;->m_isHaltSwitchOn:Z

    if-eqz v4, :cond_1

    move v0, v3

    :goto_1
    iget-object v3, p0, Lcom/konka/systemsetting/individ/IndividTimeSetting;->itemHaltSwitch:Landroid/widget/LinearLayout;

    invoke-virtual {v3, v1}, Landroid/widget/LinearLayout;->setNextFocusDownId(I)V

    iget-object v3, p0, Lcom/konka/systemsetting/individ/IndividTimeSetting;->itemBootSwitch:Landroid/widget/LinearLayout;

    invoke-virtual {v3, v0}, Landroid/widget/LinearLayout;->setNextFocusUpId(I)V

    return-void

    :cond_0
    const v1, 0x7f09002c    # com.konka.systemsetting.R.id.sys_individ_timing_item_bootswitch

    goto :goto_0

    :cond_1
    const v0, 0x7f09002a    # com.konka.systemsetting.R.id.sys_individ_timing_item_haltswitch

    goto :goto_1
.end method

.method private updateHaltTimeUI()V
    .locals 3

    iget-object v1, p0, Lcom/konka/systemsetting/individ/IndividTimeSetting;->itemHaltTime:Landroid/widget/LinearLayout;

    const/4 v2, 0x1

    invoke-virtual {v1, v2}, Landroid/widget/LinearLayout;->getChildAt(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    new-instance v1, Ljava/lang/StringBuilder;

    iget-object v2, p0, Lcom/konka/systemsetting/individ/IndividTimeSetting;->timerOff:Lcom/mstar/android/tvapi/common/vo/TimerPowerOffModeStatus;

    iget v2, v2, Lcom/mstar/android/tvapi/common/vo/TimerPowerOffModeStatus;->hour:I

    invoke-virtual {p0, v2}, Lcom/konka/systemsetting/individ/IndividTimeSetting;->formatTimeField(I)Ljava/lang/String;

    move-result-object v2

    invoke-static {v2}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v2

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    const-string v2, ":"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget-object v2, p0, Lcom/konka/systemsetting/individ/IndividTimeSetting;->timerOff:Lcom/mstar/android/tvapi/common/vo/TimerPowerOffModeStatus;

    iget v2, v2, Lcom/mstar/android/tvapi/common/vo/TimerPowerOffModeStatus;->minute:I

    invoke-virtual {p0, v2}, Lcom/konka/systemsetting/individ/IndividTimeSetting;->formatTimeField(I)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    return-void
.end method

.method private updateTimeUI(Ljava/lang/String;)V
    .locals 3
    .param p1    # Ljava/lang/String;

    iget-object v1, p0, Lcom/konka/systemsetting/individ/IndividTimeSetting;->itemTimeSetting:Landroid/widget/LinearLayout;

    const/4 v2, 0x1

    invoke-virtual {v1, v2}, Landroid/widget/LinearLayout;->getChildAt(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    invoke-virtual {v0, p1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    return-void
.end method

.method private updateTimeZone()V
    .locals 5

    invoke-static {}, Ljava/util/Calendar;->getInstance()Ljava/util/Calendar;

    move-result-object v0

    invoke-virtual {v0}, Ljava/util/Calendar;->getTimeZone()Ljava/util/TimeZone;

    move-result-object v3

    invoke-static {v3}, Lcom/konka/systemsetting/individ/IndividTimeSetting;->getTimeZoneText(Ljava/util/TimeZone;)Ljava/lang/String;

    move-result-object v1

    iget-object v3, p0, Lcom/konka/systemsetting/individ/IndividTimeSetting;->itemTimeZoneSetting:Landroid/widget/LinearLayout;

    const/4 v4, 0x1

    invoke-virtual {v3, v4}, Landroid/widget/LinearLayout;->getChildAt(I)Landroid/view/View;

    move-result-object v2

    check-cast v2, Landroid/widget/TextView;

    invoke-virtual {v2, v1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    new-instance v3, Ljava/lang/StringBuilder;

    const-string v4, "updateTimeZone: set="

    invoke-direct {v3, v4}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v3, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v3}, Lcom/konka/debuginfo/logPrint;->Debug(Ljava/lang/String;)V

    return-void
.end method

.method private updateTimeZoneSelectUI()V
    .locals 4

    iget-object v2, p0, Lcom/konka/systemsetting/individ/IndividTimeSetting;->adapter:Landroid/widget/SimpleAdapter;

    if-eqz v2, :cond_0

    iget-object v2, p0, Lcom/konka/systemsetting/individ/IndividTimeSetting;->adapter:Landroid/widget/SimpleAdapter;

    iget-object v3, p0, Lcom/konka/systemsetting/individ/IndividTimeSetting;->m_iTimeZoneSelectIndex:Ljava/lang/Integer;

    invoke-virtual {v3}, Ljava/lang/Integer;->intValue()I

    move-result v3

    invoke-virtual {v2, v3}, Landroid/widget/SimpleAdapter;->getItem(I)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Ljava/util/HashMap;

    iget-object v2, p0, Lcom/konka/systemsetting/individ/IndividTimeSetting;->mContext:Lcom/konka/systemsetting/MainActivity;

    const-string v3, "alarm"

    invoke-virtual {v2, v3}, Lcom/konka/systemsetting/MainActivity;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/app/AlarmManager;

    const-string v2, "id"

    invoke-virtual {v1, v2}, Ljava/util/HashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0, v2}, Landroid/app/AlarmManager;->setTimeZone(Ljava/lang/String;)V

    new-instance v2, Ljava/lang/StringBuilder;

    const-string v3, "updateTimeZoneSelectUI==>time after setTimeZone :"

    invoke-direct {v2, v3}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-direct {p0}, Lcom/konka/systemsetting/individ/IndividTimeSetting;->getTimeString()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v2}, Lcom/konka/debuginfo/logPrint;->Debug(Ljava/lang/String;)V

    invoke-direct {p0}, Lcom/konka/systemsetting/individ/IndividTimeSetting;->updateTimeZone()V

    :goto_0
    return-void

    :cond_0
    const-string v2, "SystemSetting"

    const-string v3, "updateTimeZoneSelectUI-- nArrCurrent is null!"

    invoke-static {v2, v3}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_0
.end method


# virtual methods
.method public constructTimezoneAdapter(Landroid/content/Context;)Landroid/widget/SimpleAdapter;
    .locals 8
    .param p1    # Landroid/content/Context;

    const/4 v3, 0x1

    const/4 v1, 0x0

    new-array v4, v3, [Ljava/lang/String;

    const-string v0, "name"

    aput-object v0, v4, v1

    new-array v5, v3, [I

    const v0, 0x1020014    # android.R.id.text1

    aput v0, v5, v1

    const-string v7, "offset"

    new-instance v6, Lcom/konka/systemsetting/individ/IndividTimeSetting$MyComparator;

    const-string v0, "offset"

    invoke-direct {v6, v0}, Lcom/konka/systemsetting/individ/IndividTimeSetting$MyComparator;-><init>(Ljava/lang/String;)V

    invoke-static {p1}, Lcom/konka/systemsetting/individ/IndividTimeSetting;->getZones(Landroid/content/Context;)Ljava/util/List;

    move-result-object v2

    invoke-static {v2, v6}, Ljava/util/Collections;->sort(Ljava/util/List;Ljava/util/Comparator;)V

    new-instance v0, Landroid/widget/SimpleAdapter;

    const v3, 0x7f03000b    # com.konka.systemsetting.R.layout.simple_list_item_single_choice_timezone

    move-object v1, p1

    invoke-direct/range {v0 .. v5}, Landroid/widget/SimpleAdapter;-><init>(Landroid/content/Context;Ljava/util/List;I[Ljava/lang/String;[I)V

    iput-object v0, p0, Lcom/konka/systemsetting/individ/IndividTimeSetting;->adapter:Landroid/widget/SimpleAdapter;

    iget-object v0, p0, Lcom/konka/systemsetting/individ/IndividTimeSetting;->adapter:Landroid/widget/SimpleAdapter;

    return-object v0
.end method

.method public formatTimeField(I)Ljava/lang/String;
    .locals 3
    .param p1    # I

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/String;->length()I

    move-result v1

    const/4 v2, 0x1

    if-ne v1, v2, :cond_0

    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "0"

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    :cond_0
    return-object v0
.end method

.method protected getDateFormat()Ljava/lang/String;
    .locals 2

    iget-object v0, p0, Lcom/konka/systemsetting/individ/IndividTimeSetting;->mContext:Lcom/konka/systemsetting/MainActivity;

    invoke-virtual {v0}, Lcom/konka/systemsetting/MainActivity;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v0

    const-string v1, "date_format"

    invoke-static {v0, v1}, Landroid/provider/Settings$System;->getString(Landroid/content/ContentResolver;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method protected is24Hour()Z
    .locals 1

    iget-object v0, p0, Lcom/konka/systemsetting/individ/IndividTimeSetting;->mContext:Lcom/konka/systemsetting/MainActivity;

    invoke-static {v0}, Landroid/text/format/DateFormat;->is24HourFormat(Landroid/content/Context;)Z

    move-result v0

    return v0
.end method

.method public onClick(Landroid/view/View;)V
    .locals 1
    .param p1    # Landroid/view/View;

    invoke-virtual {p1}, Landroid/view/View;->getId()I

    move-result v0

    invoke-direct {p0, v0}, Lcom/konka/systemsetting/individ/IndividTimeSetting;->onEnterKeyDownOrOnMouseClick(I)Z

    return-void
.end method

.method public onDateSet(Landroid/widget/DatePicker;III)V
    .locals 1
    .param p1    # Landroid/widget/DatePicker;
    .param p2    # I
    .param p3    # I
    .param p4    # I

    invoke-direct {p0, p2, p3, p4}, Lcom/konka/systemsetting/individ/IndividTimeSetting;->setDate(III)V

    iget-object v0, p0, Lcom/konka/systemsetting/individ/IndividTimeSetting;->mContext:Lcom/konka/systemsetting/MainActivity;

    invoke-virtual {p0, v0}, Lcom/konka/systemsetting/individ/IndividTimeSetting;->updateTimeAndDateDisplay(Landroid/content/Context;)V

    return-void
.end method

.method public onFocusChange(Landroid/view/View;Z)V
    .locals 6
    .param p1    # Landroid/view/View;
    .param p2    # Z

    const/4 v5, 0x1

    const/4 v0, 0x0

    invoke-virtual {p1}, Landroid/view/View;->getId()I

    move-result v1

    if-eqz p2, :cond_0

    iget-object v2, p0, Lcom/konka/systemsetting/individ/IndividTimeSetting;->mContext:Lcom/konka/systemsetting/MainActivity;

    iget-object v2, v2, Lcom/konka/systemsetting/MainActivity;->m_individPageManager:Lcom/konka/systemsetting/individ/IndividPageManager;

    iget-object v3, p0, Lcom/konka/systemsetting/individ/IndividTimeSetting;->mContext:Lcom/konka/systemsetting/MainActivity;

    iget-object v3, v3, Lcom/konka/systemsetting/MainActivity;->m_individPageManager:Lcom/konka/systemsetting/individ/IndividPageManager;

    invoke-virtual {v3}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    invoke-static {v5}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v3

    iget-object v4, p0, Lcom/konka/systemsetting/individ/IndividTimeSetting;->mContext:Lcom/konka/systemsetting/MainActivity;

    iget-object v4, v4, Lcom/konka/systemsetting/MainActivity;->m_individPageManager:Lcom/konka/systemsetting/individ/IndividPageManager;

    invoke-virtual {v4}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    invoke-static {v5}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v4

    invoke-virtual {v2, v3, v4}, Lcom/konka/systemsetting/individ/IndividPageManager;->setLastFocus(Ljava/lang/Integer;Ljava/lang/Integer;)V

    invoke-direct {p0, v1}, Lcom/konka/systemsetting/individ/IndividTimeSetting;->getFocusImageById(I)I

    move-result v0

    :goto_0
    invoke-virtual {p1, v0}, Landroid/view/View;->setBackgroundResource(I)V

    return-void

    :cond_0
    invoke-direct {p0, v1}, Lcom/konka/systemsetting/individ/IndividTimeSetting;->getUnFocusImageById(I)I

    move-result v0

    goto :goto_0
.end method

.method public onKey(Landroid/view/View;ILandroid/view/KeyEvent;)Z
    .locals 1
    .param p1    # Landroid/view/View;
    .param p2    # I
    .param p3    # Landroid/view/KeyEvent;

    const/16 v0, 0x17

    if-ne p2, v0, :cond_0

    invoke-virtual {p3}, Landroid/view/KeyEvent;->getAction()I

    move-result v0

    if-nez v0, :cond_0

    invoke-virtual {p1}, Landroid/view/View;->getId()I

    move-result v0

    invoke-direct {p0, v0}, Lcom/konka/systemsetting/individ/IndividTimeSetting;->onEnterKeyDownOrOnMouseClick(I)Z

    move-result v0

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public onPause()V
    .locals 1

    iget-boolean v0, p0, Lcom/konka/systemsetting/individ/IndividTimeSetting;->isDTRUnregisted:Z

    if-nez v0, :cond_0

    invoke-direct {p0}, Lcom/konka/systemsetting/individ/IndividTimeSetting;->unregisterDTReceiver()V

    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/konka/systemsetting/individ/IndividTimeSetting;->isDTRUnregisted:Z

    :cond_0
    return-void
.end method

.method public onResume()V
    .locals 1

    iget-boolean v0, p0, Lcom/konka/systemsetting/individ/IndividTimeSetting;->isDTRUnregisted:Z

    if-eqz v0, :cond_0

    invoke-direct {p0}, Lcom/konka/systemsetting/individ/IndividTimeSetting;->registerDTReceiver()V

    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/konka/systemsetting/individ/IndividTimeSetting;->isDTRUnregisted:Z

    :cond_0
    return-void
.end method

.method public onTimeSet(Landroid/widget/TimePicker;II)V
    .locals 1
    .param p1    # Landroid/widget/TimePicker;
    .param p2    # I
    .param p3    # I

    invoke-direct {p0, p2, p3}, Lcom/konka/systemsetting/individ/IndividTimeSetting;->setTime(II)V

    iget-object v0, p0, Lcom/konka/systemsetting/individ/IndividTimeSetting;->mContext:Lcom/konka/systemsetting/MainActivity;

    invoke-virtual {p0, v0}, Lcom/konka/systemsetting/individ/IndividTimeSetting;->updateTimeAndDateDisplay(Landroid/content/Context;)V

    return-void
.end method

.method protected set24Hour(Z)V
    .locals 3
    .param p1    # Z

    iget-object v0, p0, Lcom/konka/systemsetting/individ/IndividTimeSetting;->mContext:Lcom/konka/systemsetting/MainActivity;

    invoke-virtual {v0}, Lcom/konka/systemsetting/MainActivity;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v1

    const-string v2, "time_12_24"

    if-eqz p1, :cond_0

    const-string v0, "24"

    :goto_0
    invoke-static {v1, v2, v0}, Landroid/provider/Settings$System;->putString(Landroid/content/ContentResolver;Ljava/lang/String;Ljava/lang/String;)Z

    return-void

    :cond_0
    const-string v0, "12"

    goto :goto_0
.end method

.method updateBootChannel()V
    .locals 6

    :try_start_0
    iget-object v1, p0, Lcom/konka/systemsetting/individ/IndividTimeSetting;->tMgr:Lcom/mstar/android/tvapi/common/TimerManager;

    invoke-virtual {v1}, Lcom/mstar/android/tvapi/common/TimerManager;->getOnTime()Lcom/mstar/android/tvapi/common/vo/TimerPowerOn;

    move-result-object v1

    iput-object v1, p0, Lcom/konka/systemsetting/individ/IndividTimeSetting;->timerOn:Lcom/mstar/android/tvapi/common/vo/TimerPowerOn;

    iget-object v1, p0, Lcom/konka/systemsetting/individ/IndividTimeSetting;->timerOn:Lcom/mstar/android/tvapi/common/vo/TimerPowerOn;

    sget-object v2, Lcom/mstar/android/tvapi/common/vo/EnumTimerBootType;->EN_TIMER_BOOT_ON_TIMER:Lcom/mstar/android/tvapi/common/vo/EnumTimerBootType;

    invoke-virtual {v1, v2}, Lcom/mstar/android/tvapi/common/vo/TimerPowerOn;->setBootMode(Lcom/mstar/android/tvapi/common/vo/EnumTimerBootType;)V

    iget-object v1, p0, Lcom/konka/systemsetting/individ/IndividTimeSetting;->timerOn:Lcom/mstar/android/tvapi/common/vo/TimerPowerOn;

    sget-object v2, Lcom/mstar/android/tvapi/common/vo/EnumTimerPeriod;->EN_Timer_Once:Lcom/mstar/android/tvapi/common/vo/EnumTimerPeriod;

    invoke-virtual {v1, v2}, Lcom/mstar/android/tvapi/common/vo/TimerPowerOn;->setTimerPeriod(Lcom/mstar/android/tvapi/common/vo/EnumTimerPeriod;)V

    iget-object v1, p0, Lcom/konka/systemsetting/individ/IndividTimeSetting;->timerOn:Lcom/mstar/android/tvapi/common/vo/TimerPowerOn;

    iget-object v2, p0, Lcom/konka/systemsetting/individ/IndividTimeSetting;->mEnumOnTimeSource:[Lcom/mstar/android/tvapi/common/vo/EnumTimeOnTimerSource;

    iget-object v3, p0, Lcom/konka/systemsetting/individ/IndividTimeSetting;->m_iBootChannelIndex:Ljava/lang/Integer;

    invoke-virtual {v3}, Ljava/lang/Integer;->intValue()I

    move-result v3

    aget-object v2, v2, v3

    invoke-virtual {v1, v2}, Lcom/mstar/android/tvapi/common/vo/TimerPowerOn;->setTvSource(Lcom/mstar/android/tvapi/common/vo/EnumTimeOnTimerSource;)V

    iget-object v1, p0, Lcom/konka/systemsetting/individ/IndividTimeSetting;->tMgr:Lcom/mstar/android/tvapi/common/TimerManager;

    iget-object v2, p0, Lcom/konka/systemsetting/individ/IndividTimeSetting;->timerOn:Lcom/mstar/android/tvapi/common/vo/TimerPowerOn;

    const/4 v3, 0x1

    const/4 v4, 0x1

    sget-object v5, Lcom/mstar/android/tvapi/common/vo/EnumEpgTimerActType;->EN_EPGTIMER_ACT_NONE:Lcom/mstar/android/tvapi/common/vo/EnumEpgTimerActType;

    invoke-virtual {v1, v2, v3, v4, v5}, Lcom/mstar/android/tvapi/common/TimerManager;->setOnTime(Lcom/mstar/android/tvapi/common/vo/TimerPowerOn;ZZLcom/mstar/android/tvapi/common/vo/EnumEpgTimerActType;)V
    :try_end_0
    .catch Lcom/mstar/android/tvapi/common/exception/TvCommonException; {:try_start_0 .. :try_end_0} :catch_0

    :goto_0
    return-void

    :catch_0
    move-exception v0

    invoke-virtual {v0}, Lcom/mstar/android/tvapi/common/exception/TvCommonException;->printStackTrace()V

    goto :goto_0
.end method

.method updateBootSwitch()V
    .locals 9

    :try_start_0
    iget-object v0, p0, Lcom/konka/systemsetting/individ/IndividTimeSetting;->tMgr:Lcom/mstar/android/tvapi/common/TimerManager;

    invoke-virtual {v0}, Lcom/mstar/android/tvapi/common/TimerManager;->getOnTime()Lcom/mstar/android/tvapi/common/vo/TimerPowerOn;

    move-result-object v0

    iput-object v0, p0, Lcom/konka/systemsetting/individ/IndividTimeSetting;->timerOn:Lcom/mstar/android/tvapi/common/vo/TimerPowerOn;

    sget-boolean v0, Lcom/konka/systemsetting/individ/IndividTimeSetting;->m_isBootSwitchOn:Z

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/konka/systemsetting/individ/IndividTimeSetting;->timerOn:Lcom/mstar/android/tvapi/common/vo/TimerPowerOn;

    sget-object v1, Lcom/mstar/android/tvapi/common/vo/EnumTimerPeriod;->EN_Timer_Once:Lcom/mstar/android/tvapi/common/vo/EnumTimerPeriod;

    invoke-virtual {v0, v1}, Lcom/mstar/android/tvapi/common/vo/TimerPowerOn;->setTimerPeriod(Lcom/mstar/android/tvapi/common/vo/EnumTimerPeriod;)V

    :goto_0
    new-instance v7, Landroid/text/format/Time;

    invoke-direct {v7}, Landroid/text/format/Time;-><init>()V

    invoke-virtual {v7}, Landroid/text/format/Time;->setToNow()V

    iget-object v0, p0, Lcom/konka/systemsetting/individ/IndividTimeSetting;->timerOn:Lcom/mstar/android/tvapi/common/vo/TimerPowerOn;

    iget-object v1, p0, Lcom/konka/systemsetting/individ/IndividTimeSetting;->timerOn:Lcom/mstar/android/tvapi/common/vo/TimerPowerOn;

    iget v1, v1, Lcom/mstar/android/tvapi/common/vo/TimerPowerOn;->second:I

    iget-object v2, p0, Lcom/konka/systemsetting/individ/IndividTimeSetting;->timerOn:Lcom/mstar/android/tvapi/common/vo/TimerPowerOn;

    iget v2, v2, Lcom/mstar/android/tvapi/common/vo/TimerPowerOn;->minute:I

    iget-object v3, p0, Lcom/konka/systemsetting/individ/IndividTimeSetting;->timerOn:Lcom/mstar/android/tvapi/common/vo/TimerPowerOn;

    iget v3, v3, Lcom/mstar/android/tvapi/common/vo/TimerPowerOn;->hour:I

    iget v4, v7, Landroid/text/format/Time;->monthDay:I

    iget v5, v7, Landroid/text/format/Time;->month:I

    add-int/lit8 v5, v5, 0x1

    iget v6, v7, Landroid/text/format/Time;->year:I

    invoke-virtual/range {v0 .. v6}, Lcom/mstar/android/tvapi/common/vo/TimerPowerOn;->set(IIIIII)V

    iget-object v0, p0, Lcom/konka/systemsetting/individ/IndividTimeSetting;->tMgr:Lcom/mstar/android/tvapi/common/TimerManager;

    iget-object v1, p0, Lcom/konka/systemsetting/individ/IndividTimeSetting;->timerOn:Lcom/mstar/android/tvapi/common/vo/TimerPowerOn;

    const/4 v2, 0x1

    const/4 v3, 0x0

    sget-object v4, Lcom/mstar/android/tvapi/common/vo/EnumEpgTimerActType;->EN_EPGTIMER_ACT_NONE:Lcom/mstar/android/tvapi/common/vo/EnumEpgTimerActType;

    invoke-virtual {v0, v1, v2, v3, v4}, Lcom/mstar/android/tvapi/common/TimerManager;->setOnTime(Lcom/mstar/android/tvapi/common/vo/TimerPowerOn;ZZLcom/mstar/android/tvapi/common/vo/EnumEpgTimerActType;)V

    :goto_1
    return-void

    :cond_0
    iget-object v0, p0, Lcom/konka/systemsetting/individ/IndividTimeSetting;->timerOn:Lcom/mstar/android/tvapi/common/vo/TimerPowerOn;

    sget-object v1, Lcom/mstar/android/tvapi/common/vo/EnumTimerPeriod;->EN_Timer_Off:Lcom/mstar/android/tvapi/common/vo/EnumTimerPeriod;

    invoke-virtual {v0, v1}, Lcom/mstar/android/tvapi/common/vo/TimerPowerOn;->setTimerPeriod(Lcom/mstar/android/tvapi/common/vo/EnumTimerPeriod;)V
    :try_end_0
    .catch Lcom/mstar/android/tvapi/common/exception/TvCommonException; {:try_start_0 .. :try_end_0} :catch_0

    goto :goto_0

    :catch_0
    move-exception v8

    invoke-virtual {v8}, Lcom/mstar/android/tvapi/common/exception/TvCommonException;->printStackTrace()V

    goto :goto_1
.end method

.method updateBootTime(II)V
    .locals 9
    .param p1    # I
    .param p2    # I

    :try_start_0
    new-instance v7, Landroid/text/format/Time;

    invoke-direct {v7}, Landroid/text/format/Time;-><init>()V

    invoke-virtual {v7}, Landroid/text/format/Time;->setToNow()V

    iget-object v0, p0, Lcom/konka/systemsetting/individ/IndividTimeSetting;->tMgr:Lcom/mstar/android/tvapi/common/TimerManager;

    invoke-virtual {v0}, Lcom/mstar/android/tvapi/common/TimerManager;->getOnTime()Lcom/mstar/android/tvapi/common/vo/TimerPowerOn;

    move-result-object v0

    iput-object v0, p0, Lcom/konka/systemsetting/individ/IndividTimeSetting;->timerOn:Lcom/mstar/android/tvapi/common/vo/TimerPowerOn;

    iget-object v0, p0, Lcom/konka/systemsetting/individ/IndividTimeSetting;->timerOn:Lcom/mstar/android/tvapi/common/vo/TimerPowerOn;

    iget-object v1, p0, Lcom/konka/systemsetting/individ/IndividTimeSetting;->timerOn:Lcom/mstar/android/tvapi/common/vo/TimerPowerOn;

    iget v1, v1, Lcom/mstar/android/tvapi/common/vo/TimerPowerOn;->second:I

    iget v4, v7, Landroid/text/format/Time;->monthDay:I

    iget v2, v7, Landroid/text/format/Time;->month:I

    add-int/lit8 v5, v2, 0x1

    iget v6, v7, Landroid/text/format/Time;->year:I

    move v2, p2

    move v3, p1

    invoke-virtual/range {v0 .. v6}, Lcom/mstar/android/tvapi/common/vo/TimerPowerOn;->set(IIIIII)V

    iget-object v0, p0, Lcom/konka/systemsetting/individ/IndividTimeSetting;->timerOn:Lcom/mstar/android/tvapi/common/vo/TimerPowerOn;

    sget-object v1, Lcom/mstar/android/tvapi/common/vo/EnumTimerBootType;->EN_TIMER_BOOT_ON_TIMER:Lcom/mstar/android/tvapi/common/vo/EnumTimerBootType;

    invoke-virtual {v0, v1}, Lcom/mstar/android/tvapi/common/vo/TimerPowerOn;->setBootMode(Lcom/mstar/android/tvapi/common/vo/EnumTimerBootType;)V

    iget-object v0, p0, Lcom/konka/systemsetting/individ/IndividTimeSetting;->timerOn:Lcom/mstar/android/tvapi/common/vo/TimerPowerOn;

    sget-object v1, Lcom/mstar/android/tvapi/common/vo/EnumTimerPeriod;->EN_Timer_Once:Lcom/mstar/android/tvapi/common/vo/EnumTimerPeriod;

    invoke-virtual {v0, v1}, Lcom/mstar/android/tvapi/common/vo/TimerPowerOn;->setTimerPeriod(Lcom/mstar/android/tvapi/common/vo/EnumTimerPeriod;)V

    iget-object v0, p0, Lcom/konka/systemsetting/individ/IndividTimeSetting;->timerOn:Lcom/mstar/android/tvapi/common/vo/TimerPowerOn;

    iget-object v1, p0, Lcom/konka/systemsetting/individ/IndividTimeSetting;->mEnumOnTimeSource:[Lcom/mstar/android/tvapi/common/vo/EnumTimeOnTimerSource;

    iget-object v2, p0, Lcom/konka/systemsetting/individ/IndividTimeSetting;->m_iBootChannelIndex:Ljava/lang/Integer;

    invoke-virtual {v2}, Ljava/lang/Integer;->intValue()I

    move-result v2

    aget-object v1, v1, v2

    invoke-virtual {v0, v1}, Lcom/mstar/android/tvapi/common/vo/TimerPowerOn;->setTvSource(Lcom/mstar/android/tvapi/common/vo/EnumTimeOnTimerSource;)V

    iget-object v0, p0, Lcom/konka/systemsetting/individ/IndividTimeSetting;->tMgr:Lcom/mstar/android/tvapi/common/TimerManager;

    iget-object v1, p0, Lcom/konka/systemsetting/individ/IndividTimeSetting;->timerOn:Lcom/mstar/android/tvapi/common/vo/TimerPowerOn;

    const/4 v2, 0x1

    const/4 v3, 0x1

    sget-object v4, Lcom/mstar/android/tvapi/common/vo/EnumEpgTimerActType;->EN_EPGTIMER_ACT_NONE:Lcom/mstar/android/tvapi/common/vo/EnumEpgTimerActType;

    invoke-virtual {v0, v1, v2, v3, v4}, Lcom/mstar/android/tvapi/common/TimerManager;->setOnTime(Lcom/mstar/android/tvapi/common/vo/TimerPowerOn;ZZLcom/mstar/android/tvapi/common/vo/EnumEpgTimerActType;)V

    new-instance v0, Ljava/lang/StringBuilder;

    const-string v1, "update_PowerOn_Time: "

    invoke-direct {v0, v1}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    iget-object v1, p0, Lcom/konka/systemsetting/individ/IndividTimeSetting;->timerOn:Lcom/mstar/android/tvapi/common/vo/TimerPowerOn;

    invoke-direct {p0, v1}, Lcom/konka/systemsetting/individ/IndividTimeSetting;->timeToString(Landroid/text/format/Time;)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-direct {p0, v0}, Lcom/konka/systemsetting/individ/IndividTimeSetting;->debug(Ljava/lang/String;)V
    :try_end_0
    .catch Lcom/mstar/android/tvapi/common/exception/TvCommonException; {:try_start_0 .. :try_end_0} :catch_0

    :goto_0
    return-void

    :catch_0
    move-exception v8

    invoke-virtual {v8}, Lcom/mstar/android/tvapi/common/exception/TvCommonException;->printStackTrace()V

    goto :goto_0
.end method

.method public updateDateUI(Ljava/util/Calendar;)V
    .locals 6
    .param p1    # Ljava/util/Calendar;

    const/4 v5, 0x1

    const/4 v3, 0x0

    new-instance v1, Lcom/konka/systemsetting/individ/IndividTimeSetting$ITime;

    invoke-direct {v1, p0, v3, v3}, Lcom/konka/systemsetting/individ/IndividTimeSetting$ITime;-><init>(Lcom/konka/systemsetting/individ/IndividTimeSetting;II)V

    iget-object v3, p0, Lcom/konka/systemsetting/individ/IndividTimeSetting;->itemDateSetting:Landroid/widget/LinearLayout;

    if-eqz v3, :cond_0

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-virtual {p1, v5}, Ljava/util/Calendar;->get(I)I

    move-result v4

    invoke-virtual {v1, v4}, Lcom/konka/systemsetting/individ/IndividTimeSetting$ITime;->formatTimeField(I)Ljava/lang/String;

    move-result-object v4

    invoke-static {v4}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v4

    invoke-direct {v3, v4}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    const-string v4, " - "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    const/4 v4, 0x2

    invoke-virtual {p1, v4}, Ljava/util/Calendar;->get(I)I

    move-result v4

    invoke-virtual {v1, v4}, Lcom/konka/systemsetting/individ/IndividTimeSetting$ITime;->formatTimeField(I)Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    const-string v4, " - "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    const/4 v4, 0x5

    invoke-virtual {p1, v4}, Ljava/util/Calendar;->get(I)I

    move-result v4

    invoke-virtual {v1, v4}, Lcom/konka/systemsetting/individ/IndividTimeSetting$ITime;->formatTimeField(I)Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    iget-object v3, p0, Lcom/konka/systemsetting/individ/IndividTimeSetting;->itemDateSetting:Landroid/widget/LinearLayout;

    invoke-virtual {v3, v5}, Landroid/widget/LinearLayout;->getChildAt(I)Landroid/view/View;

    move-result-object v2

    check-cast v2, Landroid/widget/TextView;

    invoke-virtual {v2, v0}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    :cond_0
    return-void
.end method

.method updateHaltSwitch()V
    .locals 9

    :try_start_0
    iget-object v0, p0, Lcom/konka/systemsetting/individ/IndividTimeSetting;->tMgr:Lcom/mstar/android/tvapi/common/TimerManager;

    invoke-virtual {v0}, Lcom/mstar/android/tvapi/common/TimerManager;->getOffModeStatus()Lcom/mstar/android/tvapi/common/vo/TimerPowerOffModeStatus;

    move-result-object v0

    iput-object v0, p0, Lcom/konka/systemsetting/individ/IndividTimeSetting;->timerOff:Lcom/mstar/android/tvapi/common/vo/TimerPowerOffModeStatus;

    sget-boolean v0, Lcom/konka/systemsetting/individ/IndividTimeSetting;->m_isHaltSwitchOn:Z

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/konka/systemsetting/individ/IndividTimeSetting;->timerOff:Lcom/mstar/android/tvapi/common/vo/TimerPowerOffModeStatus;

    sget-object v1, Lcom/mstar/android/tvapi/common/vo/EnumTimerPeriod;->EN_Timer_Once:Lcom/mstar/android/tvapi/common/vo/EnumTimerPeriod;

    invoke-virtual {v0, v1}, Lcom/mstar/android/tvapi/common/vo/TimerPowerOffModeStatus;->setTimerPeriod(Lcom/mstar/android/tvapi/common/vo/EnumTimerPeriod;)V

    :goto_0
    new-instance v7, Landroid/text/format/Time;

    invoke-direct {v7}, Landroid/text/format/Time;-><init>()V

    invoke-virtual {v7}, Landroid/text/format/Time;->setToNow()V

    iget-object v0, p0, Lcom/konka/systemsetting/individ/IndividTimeSetting;->timerOff:Lcom/mstar/android/tvapi/common/vo/TimerPowerOffModeStatus;

    iget-object v1, p0, Lcom/konka/systemsetting/individ/IndividTimeSetting;->timerOff:Lcom/mstar/android/tvapi/common/vo/TimerPowerOffModeStatus;

    iget v1, v1, Lcom/mstar/android/tvapi/common/vo/TimerPowerOffModeStatus;->second:I

    iget-object v2, p0, Lcom/konka/systemsetting/individ/IndividTimeSetting;->timerOff:Lcom/mstar/android/tvapi/common/vo/TimerPowerOffModeStatus;

    iget v2, v2, Lcom/mstar/android/tvapi/common/vo/TimerPowerOffModeStatus;->minute:I

    iget-object v3, p0, Lcom/konka/systemsetting/individ/IndividTimeSetting;->timerOff:Lcom/mstar/android/tvapi/common/vo/TimerPowerOffModeStatus;

    iget v3, v3, Lcom/mstar/android/tvapi/common/vo/TimerPowerOffModeStatus;->hour:I

    iget v4, v7, Landroid/text/format/Time;->monthDay:I

    iget v5, v7, Landroid/text/format/Time;->month:I

    add-int/lit8 v5, v5, 0x1

    iget v6, v7, Landroid/text/format/Time;->year:I

    invoke-virtual/range {v0 .. v6}, Lcom/mstar/android/tvapi/common/vo/TimerPowerOffModeStatus;->set(IIIIII)V

    iget-object v0, p0, Lcom/konka/systemsetting/individ/IndividTimeSetting;->tMgr:Lcom/mstar/android/tvapi/common/TimerManager;

    iget-object v1, p0, Lcom/konka/systemsetting/individ/IndividTimeSetting;->timerOff:Lcom/mstar/android/tvapi/common/vo/TimerPowerOffModeStatus;

    const/4 v2, 0x1

    invoke-virtual {v0, v1, v2}, Lcom/mstar/android/tvapi/common/TimerManager;->setOffModeStatus(Lcom/mstar/android/tvapi/common/vo/TimerPowerOffModeStatus;Z)V

    :goto_1
    return-void

    :cond_0
    iget-object v0, p0, Lcom/konka/systemsetting/individ/IndividTimeSetting;->timerOff:Lcom/mstar/android/tvapi/common/vo/TimerPowerOffModeStatus;

    sget-object v1, Lcom/mstar/android/tvapi/common/vo/EnumTimerPeriod;->EN_Timer_Off:Lcom/mstar/android/tvapi/common/vo/EnumTimerPeriod;

    invoke-virtual {v0, v1}, Lcom/mstar/android/tvapi/common/vo/TimerPowerOffModeStatus;->setTimerPeriod(Lcom/mstar/android/tvapi/common/vo/EnumTimerPeriod;)V
    :try_end_0
    .catch Lcom/mstar/android/tvapi/common/exception/TvCommonException; {:try_start_0 .. :try_end_0} :catch_0

    goto :goto_0

    :catch_0
    move-exception v8

    invoke-virtual {v8}, Lcom/mstar/android/tvapi/common/exception/TvCommonException;->printStackTrace()V

    goto :goto_1
.end method

.method updateHaltTime(II)V
    .locals 9
    .param p1    # I
    .param p2    # I

    :try_start_0
    new-instance v7, Landroid/text/format/Time;

    invoke-direct {v7}, Landroid/text/format/Time;-><init>()V

    invoke-virtual {v7}, Landroid/text/format/Time;->setToNow()V

    iget-object v0, p0, Lcom/konka/systemsetting/individ/IndividTimeSetting;->tMgr:Lcom/mstar/android/tvapi/common/TimerManager;

    sget-object v1, Lcom/mstar/android/tvapi/common/vo/EnumOffTimerMode;->OFF_MODE_TIMER:Lcom/mstar/android/tvapi/common/vo/EnumOffTimerMode;

    invoke-virtual {v0, v1}, Lcom/mstar/android/tvapi/common/TimerManager;->disablePowerOffMode(Lcom/mstar/android/tvapi/common/vo/EnumOffTimerMode;)V

    iget-object v0, p0, Lcom/konka/systemsetting/individ/IndividTimeSetting;->tMgr:Lcom/mstar/android/tvapi/common/TimerManager;

    invoke-virtual {v0}, Lcom/mstar/android/tvapi/common/TimerManager;->getOffModeStatus()Lcom/mstar/android/tvapi/common/vo/TimerPowerOffModeStatus;

    move-result-object v0

    iput-object v0, p0, Lcom/konka/systemsetting/individ/IndividTimeSetting;->timerOff:Lcom/mstar/android/tvapi/common/vo/TimerPowerOffModeStatus;

    iget-object v0, p0, Lcom/konka/systemsetting/individ/IndividTimeSetting;->timerOff:Lcom/mstar/android/tvapi/common/vo/TimerPowerOffModeStatus;

    iget-object v1, p0, Lcom/konka/systemsetting/individ/IndividTimeSetting;->timerOff:Lcom/mstar/android/tvapi/common/vo/TimerPowerOffModeStatus;

    iget v1, v1, Lcom/mstar/android/tvapi/common/vo/TimerPowerOffModeStatus;->second:I

    iget v4, v7, Landroid/text/format/Time;->monthDay:I

    iget v2, v7, Landroid/text/format/Time;->month:I

    add-int/lit8 v5, v2, 0x1

    iget v6, v7, Landroid/text/format/Time;->year:I

    move v2, p2

    move v3, p1

    invoke-virtual/range {v0 .. v6}, Lcom/mstar/android/tvapi/common/vo/TimerPowerOffModeStatus;->set(IIIIII)V

    iget-object v0, p0, Lcom/konka/systemsetting/individ/IndividTimeSetting;->timerOff:Lcom/mstar/android/tvapi/common/vo/TimerPowerOffModeStatus;

    sget-object v1, Lcom/mstar/android/tvapi/common/vo/EnumTimerPeriod;->EN_Timer_Once:Lcom/mstar/android/tvapi/common/vo/EnumTimerPeriod;

    invoke-virtual {v0, v1}, Lcom/mstar/android/tvapi/common/vo/TimerPowerOffModeStatus;->setTimerPeriod(Lcom/mstar/android/tvapi/common/vo/EnumTimerPeriod;)V

    iget-object v0, p0, Lcom/konka/systemsetting/individ/IndividTimeSetting;->tMgr:Lcom/mstar/android/tvapi/common/TimerManager;

    iget-object v1, p0, Lcom/konka/systemsetting/individ/IndividTimeSetting;->timerOff:Lcom/mstar/android/tvapi/common/vo/TimerPowerOffModeStatus;

    const/4 v2, 0x1

    invoke-virtual {v0, v1, v2}, Lcom/mstar/android/tvapi/common/TimerManager;->setOffModeStatus(Lcom/mstar/android/tvapi/common/vo/TimerPowerOffModeStatus;Z)V

    new-instance v0, Ljava/lang/StringBuilder;

    const-string v1, "update_PowerOff_Time: "

    invoke-direct {v0, v1}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    iget-object v1, p0, Lcom/konka/systemsetting/individ/IndividTimeSetting;->timerOff:Lcom/mstar/android/tvapi/common/vo/TimerPowerOffModeStatus;

    invoke-direct {p0, v1}, Lcom/konka/systemsetting/individ/IndividTimeSetting;->timeToString(Landroid/text/format/Time;)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-direct {p0, v0}, Lcom/konka/systemsetting/individ/IndividTimeSetting;->debug(Ljava/lang/String;)V
    :try_end_0
    .catch Lcom/mstar/android/tvapi/common/exception/TvCommonException; {:try_start_0 .. :try_end_0} :catch_0

    :goto_0
    return-void

    :catch_0
    move-exception v8

    invoke-virtual {v8}, Lcom/mstar/android/tvapi/common/exception/TvCommonException;->printStackTrace()V

    goto :goto_0
.end method

.method public updateTimeAndDateDisplay(Landroid/content/Context;)V
    .locals 9
    .param p1    # Landroid/content/Context;

    const/4 v8, 0x1

    new-instance v0, Landroid/text/format/Time;

    invoke-direct {v0}, Landroid/text/format/Time;-><init>()V

    invoke-virtual {v0}, Landroid/text/format/Time;->setToNow()V

    invoke-virtual {v0, v8}, Landroid/text/format/Time;->toMillis(Z)J

    move-result-wide v6

    invoke-virtual {v0, v6, v7}, Landroid/text/format/Time;->set(J)V

    if-eqz v0, :cond_0

    new-instance v1, Ljava/util/Date;

    invoke-virtual {v0, v8}, Landroid/text/format/Time;->toMillis(Z)J

    move-result-wide v6

    invoke-direct {v1, v6, v7}, Ljava/util/Date;-><init>(J)V

    new-instance v2, Ljava/text/SimpleDateFormat;

    const-string v6, "yyyy-MM-dd"

    invoke-direct {v2, v6}, Ljava/text/SimpleDateFormat;-><init>(Ljava/lang/String;)V

    new-instance v5, Ljava/text/SimpleDateFormat;

    const-string v6, "HH:mm"

    invoke-direct {v5, v6}, Ljava/text/SimpleDateFormat;-><init>(Ljava/lang/String;)V

    invoke-virtual {v5, v1}, Ljava/text/SimpleDateFormat;->format(Ljava/util/Date;)Ljava/lang/String;

    move-result-object v6

    invoke-direct {p0, v6}, Lcom/konka/systemsetting/individ/IndividTimeSetting;->updateTimeUI(Ljava/lang/String;)V

    invoke-virtual {v2, v1}, Ljava/text/SimpleDateFormat;->format(Ljava/util/Date;)Ljava/lang/String;

    move-result-object v6

    invoke-direct {p0, v6}, Lcom/konka/systemsetting/individ/IndividTimeSetting;->updateDateUI(Ljava/lang/String;)V

    :goto_0
    return-void

    :cond_0
    invoke-static {p1}, Landroid/text/format/DateFormat;->getDateFormat(Landroid/content/Context;)Ljava/text/DateFormat;

    move-result-object v4

    invoke-static {}, Ljava/util/Calendar;->getInstance()Ljava/util/Calendar;

    move-result-object v3

    iget-object v6, p0, Lcom/konka/systemsetting/individ/IndividTimeSetting;->mContext:Lcom/konka/systemsetting/MainActivity;

    invoke-static {v6}, Landroid/text/format/DateFormat;->getTimeFormat(Landroid/content/Context;)Ljava/text/DateFormat;

    move-result-object v6

    invoke-virtual {v3}, Ljava/util/Calendar;->getTime()Ljava/util/Date;

    move-result-object v7

    invoke-virtual {v6, v7}, Ljava/text/DateFormat;->format(Ljava/util/Date;)Ljava/lang/String;

    move-result-object v6

    invoke-direct {p0, v6}, Lcom/konka/systemsetting/individ/IndividTimeSetting;->updateTimeUI(Ljava/lang/String;)V

    new-instance v6, Ljava/lang/StringBuilder;

    const-string v7, "xxxxx==>time after set3 :"

    invoke-direct {v6, v7}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-direct {p0}, Lcom/konka/systemsetting/individ/IndividTimeSetting;->getTimeString()Ljava/lang/String;

    move-result-object v7

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v6}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v6

    invoke-static {v6}, Lcom/konka/debuginfo/logPrint;->Debug(Ljava/lang/String;)V

    invoke-virtual {v3}, Ljava/util/Calendar;->getTime()Ljava/util/Date;

    move-result-object v6

    invoke-virtual {v4, v6}, Ljava/text/DateFormat;->format(Ljava/util/Date;)Ljava/lang/String;

    move-result-object v6

    invoke-direct {p0, v6}, Lcom/konka/systemsetting/individ/IndividTimeSetting;->updateDateUI(Ljava/lang/String;)V

    goto :goto_0
.end method

.method public updateTimeUI(Ljava/util/Calendar;)V
    .locals 5
    .param p1    # Ljava/util/Calendar;

    const/4 v3, 0x0

    new-instance v1, Lcom/konka/systemsetting/individ/IndividTimeSetting$ITime;

    invoke-direct {v1, p0, v3, v3}, Lcom/konka/systemsetting/individ/IndividTimeSetting$ITime;-><init>(Lcom/konka/systemsetting/individ/IndividTimeSetting;II)V

    iget-object v3, p0, Lcom/konka/systemsetting/individ/IndividTimeSetting;->itemTimeSetting:Landroid/widget/LinearLayout;

    if-eqz v3, :cond_0

    new-instance v3, Ljava/lang/StringBuilder;

    const/16 v4, 0xb

    invoke-virtual {p1, v4}, Ljava/util/Calendar;->get(I)I

    move-result v4

    invoke-virtual {v1, v4}, Lcom/konka/systemsetting/individ/IndividTimeSetting$ITime;->formatTimeField(I)Ljava/lang/String;

    move-result-object v4

    invoke-static {v4}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v4

    invoke-direct {v3, v4}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    const-string v4, " : "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    const/16 v4, 0xc

    invoke-virtual {p1, v4}, Ljava/util/Calendar;->get(I)I

    move-result v4

    invoke-virtual {v1, v4}, Lcom/konka/systemsetting/individ/IndividTimeSetting$ITime;->formatTimeField(I)Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    iget-object v3, p0, Lcom/konka/systemsetting/individ/IndividTimeSetting;->itemTimeSetting:Landroid/widget/LinearLayout;

    const/4 v4, 0x1

    invoke-virtual {v3, v4}, Landroid/widget/LinearLayout;->getChildAt(I)Landroid/view/View;

    move-result-object v2

    check-cast v2, Landroid/widget/TextView;

    invoke-virtual {v2, v0}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    :cond_0
    return-void
.end method
