.class Lcom/konka/systemsetting/individ/IndividAppSetting$1;
.super Ljava/lang/Object;
.source "IndividAppSetting.java"

# interfaces
.implements Landroid/view/View$OnFocusChangeListener;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/konka/systemsetting/individ/IndividAppSetting;-><init>(Lcom/konka/systemsetting/MainActivity;)V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/konka/systemsetting/individ/IndividAppSetting;


# direct methods
.method constructor <init>(Lcom/konka/systemsetting/individ/IndividAppSetting;)V
    .locals 0

    iput-object p1, p0, Lcom/konka/systemsetting/individ/IndividAppSetting$1;->this$0:Lcom/konka/systemsetting/individ/IndividAppSetting;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onFocusChange(Landroid/view/View;Z)V
    .locals 3
    .param p1    # Landroid/view/View;
    .param p2    # Z

    if-eqz p2, :cond_0

    iget-object v0, p0, Lcom/konka/systemsetting/individ/IndividAppSetting$1;->this$0:Lcom/konka/systemsetting/individ/IndividAppSetting;

    # getter for: Lcom/konka/systemsetting/individ/IndividAppSetting;->mContext:Lcom/konka/systemsetting/MainActivity;
    invoke-static {v0}, Lcom/konka/systemsetting/individ/IndividAppSetting;->access$0(Lcom/konka/systemsetting/individ/IndividAppSetting;)Lcom/konka/systemsetting/MainActivity;

    move-result-object v0

    iget-object v0, v0, Lcom/konka/systemsetting/MainActivity;->m_individPageManager:Lcom/konka/systemsetting/individ/IndividPageManager;

    iget-object v1, p0, Lcom/konka/systemsetting/individ/IndividAppSetting$1;->this$0:Lcom/konka/systemsetting/individ/IndividAppSetting;

    # getter for: Lcom/konka/systemsetting/individ/IndividAppSetting;->mContext:Lcom/konka/systemsetting/MainActivity;
    invoke-static {v1}, Lcom/konka/systemsetting/individ/IndividAppSetting;->access$0(Lcom/konka/systemsetting/individ/IndividAppSetting;)Lcom/konka/systemsetting/MainActivity;

    move-result-object v1

    iget-object v1, v1, Lcom/konka/systemsetting/MainActivity;->m_individPageManager:Lcom/konka/systemsetting/individ/IndividPageManager;

    invoke-virtual {v1}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    const/4 v1, 0x2

    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    iget-object v2, p0, Lcom/konka/systemsetting/individ/IndividAppSetting$1;->this$0:Lcom/konka/systemsetting/individ/IndividAppSetting;

    # getter for: Lcom/konka/systemsetting/individ/IndividAppSetting;->mContext:Lcom/konka/systemsetting/MainActivity;
    invoke-static {v2}, Lcom/konka/systemsetting/individ/IndividAppSetting;->access$0(Lcom/konka/systemsetting/individ/IndividAppSetting;)Lcom/konka/systemsetting/MainActivity;

    move-result-object v2

    iget-object v2, v2, Lcom/konka/systemsetting/MainActivity;->m_individPageManager:Lcom/konka/systemsetting/individ/IndividPageManager;

    invoke-virtual {v2}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    const/4 v2, 0x1

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Lcom/konka/systemsetting/individ/IndividPageManager;->setLastFocus(Ljava/lang/Integer;Ljava/lang/Integer;)V

    iget-object v0, p0, Lcom/konka/systemsetting/individ/IndividAppSetting$1;->this$0:Lcom/konka/systemsetting/individ/IndividAppSetting;

    iget-object v0, v0, Lcom/konka/systemsetting/individ/IndividAppSetting;->itemAppInst:Landroid/widget/LinearLayout;

    const v1, 0x7f020032    # com.konka.systemsetting.R.drawable.syssettingthirditemsinglefocus

    invoke-virtual {v0, v1}, Landroid/widget/LinearLayout;->setBackgroundResource(I)V

    :goto_0
    return-void

    :cond_0
    iget-object v0, p0, Lcom/konka/systemsetting/individ/IndividAppSetting$1;->this$0:Lcom/konka/systemsetting/individ/IndividAppSetting;

    iget-object v0, v0, Lcom/konka/systemsetting/individ/IndividAppSetting;->itemAppInst:Landroid/widget/LinearLayout;

    const v1, 0x7f020031    # com.konka.systemsetting.R.drawable.syssettingthirditemsingle

    invoke-virtual {v0, v1}, Landroid/widget/LinearLayout;->setBackgroundResource(I)V

    goto :goto_0
.end method
