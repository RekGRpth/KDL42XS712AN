.class Lcom/konka/systemsetting/individ/IndividConservation$1;
.super Landroid/content/BroadcastReceiver;
.source "IndividConservation.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/konka/systemsetting/individ/IndividConservation;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/konka/systemsetting/individ/IndividConservation;


# direct methods
.method constructor <init>(Lcom/konka/systemsetting/individ/IndividConservation;)V
    .locals 0

    iput-object p1, p0, Lcom/konka/systemsetting/individ/IndividConservation$1;->this$0:Lcom/konka/systemsetting/individ/IndividConservation;

    invoke-direct {p0}, Landroid/content/BroadcastReceiver;-><init>()V

    return-void
.end method


# virtual methods
.method public onReceive(Landroid/content/Context;Landroid/content/Intent;)V
    .locals 6
    .param p1    # Landroid/content/Context;
    .param p2    # Landroid/content/Intent;

    iget-object v2, p0, Lcom/konka/systemsetting/individ/IndividConservation$1;->this$0:Lcom/konka/systemsetting/individ/IndividConservation;

    # getter for: Lcom/konka/systemsetting/individ/IndividConservation;->mContext:Lcom/konka/systemsetting/MainActivity;
    invoke-static {v2}, Lcom/konka/systemsetting/individ/IndividConservation;->access$0(Lcom/konka/systemsetting/individ/IndividConservation;)Lcom/konka/systemsetting/MainActivity;

    move-result-object v2

    const-string v3, "power"

    invoke-virtual {v2, v3}, Lcom/konka/systemsetting/MainActivity;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Landroid/os/PowerManager;

    invoke-static {}, Landroid/os/SystemClock;->uptimeMillis()J

    move-result-wide v2

    const-wide/16 v4, 0x3e8

    add-long/2addr v2, v4

    invoke-virtual {v1, v2, v3}, Landroid/os/PowerManager;->goToSleep(J)V

    invoke-virtual {p2}, Landroid/content/Intent;->getAction()Ljava/lang/String;

    move-result-object v2

    const-string v3, "android.intent.action.SCREEN_OFF"

    invoke-virtual {v2, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_0

    :try_start_0
    const-string v2, "The TV is shutting down"

    invoke-static {v2}, Lcom/konka/debuginfo/logPrint;->Debug(Ljava/lang/String;)V

    const-string v2, "The TV is shutting down"

    invoke-static {v2}, Lcom/konka/debuginfo/logPrint;->Debug(Ljava/lang/String;)V

    const-string v2, "The TV is shutting down"

    invoke-static {v2}, Lcom/konka/debuginfo/logPrint;->Debug(Ljava/lang/String;)V
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    :cond_0
    :goto_0
    return-void

    :catch_0
    move-exception v0

    invoke-virtual {v0}, Ljava/lang/Exception;->getMessage()Ljava/lang/String;

    move-result-object v2

    invoke-static {v2}, Lcom/konka/debuginfo/logPrint;->Debug(Ljava/lang/String;)V

    goto :goto_0
.end method
