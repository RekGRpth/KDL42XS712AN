.class Lcom/konka/systemsetting/individ/IndividConservation$2;
.super Ljava/lang/Object;
.source "IndividConservation.java"

# interfaces
.implements Landroid/view/View$OnKeyListener;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/konka/systemsetting/individ/IndividConservation;->SetOnKeyListener()V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/konka/systemsetting/individ/IndividConservation;


# direct methods
.method constructor <init>(Lcom/konka/systemsetting/individ/IndividConservation;)V
    .locals 0

    iput-object p1, p0, Lcom/konka/systemsetting/individ/IndividConservation$2;->this$0:Lcom/konka/systemsetting/individ/IndividConservation;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onKey(Landroid/view/View;ILandroid/view/KeyEvent;)Z
    .locals 7
    .param p1    # Landroid/view/View;
    .param p2    # I
    .param p3    # Landroid/view/KeyEvent;

    const/4 v6, 0x2

    const/4 v1, 0x0

    const/16 v5, 0x42

    const/16 v4, 0x17

    const/4 v0, 0x1

    iget-object v2, p0, Lcom/konka/systemsetting/individ/IndividConservation$2;->this$0:Lcom/konka/systemsetting/individ/IndividConservation;

    invoke-virtual {p1}, Landroid/view/View;->getId()I

    move-result v3

    invoke-static {v3}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v3

    invoke-static {v2, v3}, Lcom/konka/systemsetting/individ/IndividConservation;->access$6(Lcom/konka/systemsetting/individ/IndividConservation;Ljava/lang/Integer;)V

    iget-object v2, p0, Lcom/konka/systemsetting/individ/IndividConservation$2;->this$0:Lcom/konka/systemsetting/individ/IndividConservation;

    # getter for: Lcom/konka/systemsetting/individ/IndividConservation;->m_currItemId:Ljava/lang/Integer;
    invoke-static {v2}, Lcom/konka/systemsetting/individ/IndividConservation;->access$7(Lcom/konka/systemsetting/individ/IndividConservation;)Ljava/lang/Integer;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/Integer;->intValue()I

    move-result v2

    const v3, 0x7f09001a    # com.konka.systemsetting.R.id.sys_individ_energysv_item_backlight

    if-eq v2, v3, :cond_0

    iget-object v2, p0, Lcom/konka/systemsetting/individ/IndividConservation$2;->this$0:Lcom/konka/systemsetting/individ/IndividConservation;

    # getter for: Lcom/konka/systemsetting/individ/IndividConservation;->m_currItemId:Ljava/lang/Integer;
    invoke-static {v2}, Lcom/konka/systemsetting/individ/IndividConservation;->access$7(Lcom/konka/systemsetting/individ/IndividConservation;)Ljava/lang/Integer;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/Integer;->intValue()I

    move-result v2

    const v3, 0x7f09001c    # com.konka.systemsetting.R.id.sys_individ_energysv_item_backlight_seekbar

    if-ne v2, v3, :cond_6

    :cond_0
    const/16 v2, 0x15

    if-ne p2, v2, :cond_2

    invoke-virtual {p3}, Landroid/view/KeyEvent;->getAction()I

    move-result v2

    if-nez v2, :cond_2

    iget-object v1, p0, Lcom/konka/systemsetting/individ/IndividConservation$2;->this$0:Lcom/konka/systemsetting/individ/IndividConservation;

    # invokes: Lcom/konka/systemsetting/individ/IndividConservation;->decreaseBackLightVal()V
    invoke-static {v1}, Lcom/konka/systemsetting/individ/IndividConservation;->access$8(Lcom/konka/systemsetting/individ/IndividConservation;)V

    iget-object v1, p0, Lcom/konka/systemsetting/individ/IndividConservation$2;->this$0:Lcom/konka/systemsetting/individ/IndividConservation;

    # invokes: Lcom/konka/systemsetting/individ/IndividConservation;->updateUIState(I)V
    invoke-static {v1, v6}, Lcom/konka/systemsetting/individ/IndividConservation;->access$9(Lcom/konka/systemsetting/individ/IndividConservation;I)V

    :cond_1
    :goto_0
    return v0

    :cond_2
    const/16 v2, 0x16

    if-ne p2, v2, :cond_3

    invoke-virtual {p3}, Landroid/view/KeyEvent;->getAction()I

    move-result v2

    if-nez v2, :cond_3

    iget-object v1, p0, Lcom/konka/systemsetting/individ/IndividConservation$2;->this$0:Lcom/konka/systemsetting/individ/IndividConservation;

    # invokes: Lcom/konka/systemsetting/individ/IndividConservation;->increaseBackLightVal()V
    invoke-static {v1}, Lcom/konka/systemsetting/individ/IndividConservation;->access$10(Lcom/konka/systemsetting/individ/IndividConservation;)V

    iget-object v1, p0, Lcom/konka/systemsetting/individ/IndividConservation$2;->this$0:Lcom/konka/systemsetting/individ/IndividConservation;

    # invokes: Lcom/konka/systemsetting/individ/IndividConservation;->updateUIState(I)V
    invoke-static {v1, v6}, Lcom/konka/systemsetting/individ/IndividConservation;->access$9(Lcom/konka/systemsetting/individ/IndividConservation;I)V

    goto :goto_0

    :cond_3
    if-eq p2, v5, :cond_4

    if-ne p2, v4, :cond_5

    :cond_4
    invoke-virtual {p3}, Landroid/view/KeyEvent;->getAction()I

    move-result v2

    if-eqz v2, :cond_1

    :cond_5
    move v0, v1

    goto :goto_0

    :cond_6
    iget-object v2, p0, Lcom/konka/systemsetting/individ/IndividConservation$2;->this$0:Lcom/konka/systemsetting/individ/IndividConservation;

    # getter for: Lcom/konka/systemsetting/individ/IndividConservation;->m_currItemId:Ljava/lang/Integer;
    invoke-static {v2}, Lcom/konka/systemsetting/individ/IndividConservation;->access$7(Lcom/konka/systemsetting/individ/IndividConservation;)Ljava/lang/Integer;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/Integer;->intValue()I

    move-result v2

    const v3, 0x7f090018    # com.konka.systemsetting.R.id.sys_individ_energysv_item_energyconservation

    if-ne v2, v3, :cond_8

    if-eq p2, v5, :cond_7

    if-ne p2, v4, :cond_a

    :cond_7
    invoke-virtual {p3}, Landroid/view/KeyEvent;->getAction()I

    move-result v2

    if-nez v2, :cond_a

    iget-object v1, p0, Lcom/konka/systemsetting/individ/IndividConservation$2;->this$0:Lcom/konka/systemsetting/individ/IndividConservation;

    # invokes: Lcom/konka/systemsetting/individ/IndividConservation;->showEnergySavingPickerDialog()V
    invoke-static {v1}, Lcom/konka/systemsetting/individ/IndividConservation;->access$11(Lcom/konka/systemsetting/individ/IndividConservation;)V

    goto :goto_0

    :cond_8
    iget-object v2, p0, Lcom/konka/systemsetting/individ/IndividConservation$2;->this$0:Lcom/konka/systemsetting/individ/IndividConservation;

    # getter for: Lcom/konka/systemsetting/individ/IndividConservation;->m_currItemId:Ljava/lang/Integer;
    invoke-static {v2}, Lcom/konka/systemsetting/individ/IndividConservation;->access$7(Lcom/konka/systemsetting/individ/IndividConservation;)Ljava/lang/Integer;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/Integer;->intValue()I

    move-result v2

    const v3, 0x7f09001f    # com.konka.systemsetting.R.id.sys_individ_energysv_item_noactionstandby

    if-ne v2, v3, :cond_a

    if-eq p2, v5, :cond_9

    if-ne p2, v4, :cond_a

    :cond_9
    invoke-virtual {p3}, Landroid/view/KeyEvent;->getAction()I

    move-result v2

    if-nez v2, :cond_a

    iget-object v1, p0, Lcom/konka/systemsetting/individ/IndividConservation$2;->this$0:Lcom/konka/systemsetting/individ/IndividConservation;

    # invokes: Lcom/konka/systemsetting/individ/IndividConservation;->showNoOperationStandbyPickerDialog()V
    invoke-static {v1}, Lcom/konka/systemsetting/individ/IndividConservation;->access$12(Lcom/konka/systemsetting/individ/IndividConservation;)V

    goto :goto_0

    :cond_a
    sparse-switch p2, :sswitch_data_0

    :cond_b
    move v0, v1

    goto :goto_0

    :sswitch_0
    invoke-virtual {p3}, Landroid/view/KeyEvent;->getAction()I

    move-result v2

    if-nez v2, :cond_b

    iget-object v1, p0, Lcom/konka/systemsetting/individ/IndividConservation$2;->this$0:Lcom/konka/systemsetting/individ/IndividConservation;

    iget-object v2, p0, Lcom/konka/systemsetting/individ/IndividConservation$2;->this$0:Lcom/konka/systemsetting/individ/IndividConservation;

    # getter for: Lcom/konka/systemsetting/individ/IndividConservation;->m_currItemId:Ljava/lang/Integer;
    invoke-static {v2}, Lcom/konka/systemsetting/individ/IndividConservation;->access$7(Lcom/konka/systemsetting/individ/IndividConservation;)Ljava/lang/Integer;

    move-result-object v2

    # invokes: Lcom/konka/systemsetting/individ/IndividConservation;->changeItemState(Ljava/lang/Integer;)Z
    invoke-static {v1, v2}, Lcom/konka/systemsetting/individ/IndividConservation;->access$13(Lcom/konka/systemsetting/individ/IndividConservation;Ljava/lang/Integer;)Z

    goto :goto_0

    nop

    :sswitch_data_0
    .sparse-switch
        0x17 -> :sswitch_0
        0x42 -> :sswitch_0
    .end sparse-switch
.end method
