.class Lcom/konka/systemsetting/individ/IndividTimeSetting$ITime;
.super Ljava/lang/Object;
.source "IndividTimeSetting.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/konka/systemsetting/individ/IndividTimeSetting;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x2
    name = "ITime"
.end annotation


# instance fields
.field public hour:I

.field public minute:I

.field final synthetic this$0:Lcom/konka/systemsetting/individ/IndividTimeSetting;


# direct methods
.method public constructor <init>(Lcom/konka/systemsetting/individ/IndividTimeSetting;II)V
    .locals 1
    .param p2    # I
    .param p3    # I

    const/4 v0, 0x0

    iput-object p1, p0, Lcom/konka/systemsetting/individ/IndividTimeSetting$ITime;->this$0:Lcom/konka/systemsetting/individ/IndividTimeSetting;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    iput v0, p0, Lcom/konka/systemsetting/individ/IndividTimeSetting$ITime;->hour:I

    iput v0, p0, Lcom/konka/systemsetting/individ/IndividTimeSetting$ITime;->minute:I

    iput p2, p0, Lcom/konka/systemsetting/individ/IndividTimeSetting$ITime;->hour:I

    iput p3, p0, Lcom/konka/systemsetting/individ/IndividTimeSetting$ITime;->minute:I

    return-void
.end method


# virtual methods
.method public formatTimeField(I)Ljava/lang/String;
    .locals 3
    .param p1    # I

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/String;->length()I

    move-result v1

    const/4 v2, 0x1

    if-ne v1, v2, :cond_0

    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "0"

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    :cond_0
    return-object v0
.end method

.method public toString()Ljava/lang/String;
    .locals 2

    new-instance v0, Ljava/lang/StringBuilder;

    iget v1, p0, Lcom/konka/systemsetting/individ/IndividTimeSetting$ITime;->hour:I

    invoke-virtual {p0, v1}, Lcom/konka/systemsetting/individ/IndividTimeSetting$ITime;->formatTimeField(I)Ljava/lang/String;

    move-result-object v1

    invoke-static {v1}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    const-string v1, ":"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget v1, p0, Lcom/konka/systemsetting/individ/IndividTimeSetting$ITime;->minute:I

    invoke-virtual {p0, v1}, Lcom/konka/systemsetting/individ/IndividTimeSetting$ITime;->formatTimeField(I)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method
