.class Lcom/konka/systemsetting/individ/IndividConservation$8;
.super Ljava/lang/Object;
.source "IndividConservation.java"

# interfaces
.implements Landroid/view/View$OnFocusChangeListener;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/konka/systemsetting/individ/IndividConservation;->SetOnFocusChangeListener()V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/konka/systemsetting/individ/IndividConservation;


# direct methods
.method constructor <init>(Lcom/konka/systemsetting/individ/IndividConservation;)V
    .locals 0

    iput-object p1, p0, Lcom/konka/systemsetting/individ/IndividConservation$8;->this$0:Lcom/konka/systemsetting/individ/IndividConservation;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onFocusChange(Landroid/view/View;Z)V
    .locals 9
    .param p1    # Landroid/view/View;
    .param p2    # Z

    const v8, 0x7f020031    # com.konka.systemsetting.R.drawable.syssettingthirditemsingle

    const/4 v4, 0x4

    const/4 v7, 0x3

    const/4 v6, 0x1

    const/4 v5, 0x0

    iget-object v2, p0, Lcom/konka/systemsetting/individ/IndividConservation$8;->this$0:Lcom/konka/systemsetting/individ/IndividConservation;

    invoke-virtual {p1}, Landroid/view/View;->getId()I

    move-result v3

    invoke-static {v3}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v3

    invoke-static {v2, v3}, Lcom/konka/systemsetting/individ/IndividConservation;->access$6(Lcom/konka/systemsetting/individ/IndividConservation;Ljava/lang/Integer;)V

    if-eqz p2, :cond_0

    iget-object v2, p0, Lcom/konka/systemsetting/individ/IndividConservation$8;->this$0:Lcom/konka/systemsetting/individ/IndividConservation;

    # getter for: Lcom/konka/systemsetting/individ/IndividConservation;->mContext:Lcom/konka/systemsetting/MainActivity;
    invoke-static {v2}, Lcom/konka/systemsetting/individ/IndividConservation;->access$0(Lcom/konka/systemsetting/individ/IndividConservation;)Lcom/konka/systemsetting/MainActivity;

    move-result-object v2

    iget-object v2, v2, Lcom/konka/systemsetting/MainActivity;->m_individPageManager:Lcom/konka/systemsetting/individ/IndividPageManager;

    iget-object v3, p0, Lcom/konka/systemsetting/individ/IndividConservation$8;->this$0:Lcom/konka/systemsetting/individ/IndividConservation;

    # getter for: Lcom/konka/systemsetting/individ/IndividConservation;->mContext:Lcom/konka/systemsetting/MainActivity;
    invoke-static {v3}, Lcom/konka/systemsetting/individ/IndividConservation;->access$0(Lcom/konka/systemsetting/individ/IndividConservation;)Lcom/konka/systemsetting/MainActivity;

    move-result-object v3

    iget-object v3, v3, Lcom/konka/systemsetting/MainActivity;->m_individPageManager:Lcom/konka/systemsetting/individ/IndividPageManager;

    invoke-virtual {v3}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    invoke-static {v5}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v3

    iget-object v4, p0, Lcom/konka/systemsetting/individ/IndividConservation$8;->this$0:Lcom/konka/systemsetting/individ/IndividConservation;

    # getter for: Lcom/konka/systemsetting/individ/IndividConservation;->mContext:Lcom/konka/systemsetting/MainActivity;
    invoke-static {v4}, Lcom/konka/systemsetting/individ/IndividConservation;->access$0(Lcom/konka/systemsetting/individ/IndividConservation;)Lcom/konka/systemsetting/MainActivity;

    move-result-object v4

    iget-object v4, v4, Lcom/konka/systemsetting/MainActivity;->m_individPageManager:Lcom/konka/systemsetting/individ/IndividPageManager;

    invoke-virtual {v4}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    invoke-static {v6}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v4

    invoke-virtual {v2, v3, v4}, Lcom/konka/systemsetting/individ/IndividPageManager;->setLastFocus(Ljava/lang/Integer;Ljava/lang/Integer;)V

    iget-object v2, p0, Lcom/konka/systemsetting/individ/IndividConservation$8;->this$0:Lcom/konka/systemsetting/individ/IndividConservation;

    # getter for: Lcom/konka/systemsetting/individ/IndividConservation;->m_currItemId:Ljava/lang/Integer;
    invoke-static {v2}, Lcom/konka/systemsetting/individ/IndividConservation;->access$7(Lcom/konka/systemsetting/individ/IndividConservation;)Ljava/lang/Integer;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/Integer;->intValue()I

    move-result v2

    packed-switch v2, :pswitch_data_0

    :goto_0
    :pswitch_0
    return-void

    :pswitch_1
    const v2, 0x7f020032    # com.konka.systemsetting.R.drawable.syssettingthirditemsinglefocus

    invoke-virtual {p1, v2}, Landroid/view/View;->setBackgroundResource(I)V

    goto :goto_0

    :pswitch_2
    iget-object v2, p0, Lcom/konka/systemsetting/individ/IndividConservation$8;->this$0:Lcom/konka/systemsetting/individ/IndividConservation;

    invoke-static {v2, v6}, Lcom/konka/systemsetting/individ/IndividConservation;->access$18(Lcom/konka/systemsetting/individ/IndividConservation;Z)V

    new-instance v2, Ljava/lang/Thread;

    iget-object v3, p0, Lcom/konka/systemsetting/individ/IndividConservation$8;->this$0:Lcom/konka/systemsetting/individ/IndividConservation;

    # getter for: Lcom/konka/systemsetting/individ/IndividConservation;->tRunSetBL:Lcom/konka/systemsetting/individ/IndividConservation$runSetBL;
    invoke-static {v3}, Lcom/konka/systemsetting/individ/IndividConservation;->access$19(Lcom/konka/systemsetting/individ/IndividConservation;)Lcom/konka/systemsetting/individ/IndividConservation$runSetBL;

    move-result-object v3

    invoke-direct {v2, v3}, Ljava/lang/Thread;-><init>(Ljava/lang/Runnable;)V

    invoke-virtual {v2}, Ljava/lang/Thread;->start()V

    iget-object v2, p0, Lcom/konka/systemsetting/individ/IndividConservation$8;->this$0:Lcom/konka/systemsetting/individ/IndividConservation;

    # getter for: Lcom/konka/systemsetting/individ/IndividConservation;->linearLayout_BackLight:Landroid/widget/LinearLayout;
    invoke-static {v2}, Lcom/konka/systemsetting/individ/IndividConservation;->access$20(Lcom/konka/systemsetting/individ/IndividConservation;)Landroid/widget/LinearLayout;

    move-result-object v2

    const v3, 0x7f020032    # com.konka.systemsetting.R.drawable.syssettingthirditemsinglefocus

    invoke-virtual {v2, v3}, Landroid/widget/LinearLayout;->setBackgroundResource(I)V

    iget-object v2, p0, Lcom/konka/systemsetting/individ/IndividConservation$8;->this$0:Lcom/konka/systemsetting/individ/IndividConservation;

    # getter for: Lcom/konka/systemsetting/individ/IndividConservation;->seekbar_BackLight:Landroid/widget/SeekBar;
    invoke-static {v2}, Lcom/konka/systemsetting/individ/IndividConservation;->access$16(Lcom/konka/systemsetting/individ/IndividConservation;)Landroid/widget/SeekBar;

    move-result-object v2

    invoke-virtual {v2, v5}, Landroid/widget/SeekBar;->setVisibility(I)V

    iget-object v2, p0, Lcom/konka/systemsetting/individ/IndividConservation$8;->this$0:Lcom/konka/systemsetting/individ/IndividConservation;

    # getter for: Lcom/konka/systemsetting/individ/IndividConservation;->linearLayout_BackLight:Landroid/widget/LinearLayout;
    invoke-static {v2}, Lcom/konka/systemsetting/individ/IndividConservation;->access$20(Lcom/konka/systemsetting/individ/IndividConservation;)Landroid/widget/LinearLayout;

    move-result-object v2

    invoke-virtual {v2, v6}, Landroid/widget/LinearLayout;->getChildAt(I)Landroid/view/View;

    move-result-object v1

    check-cast v1, Landroid/widget/TextView;

    invoke-virtual {v1, v5}, Landroid/widget/TextView;->setVisibility(I)V

    iget-object v2, p0, Lcom/konka/systemsetting/individ/IndividConservation$8;->this$0:Lcom/konka/systemsetting/individ/IndividConservation;

    # getter for: Lcom/konka/systemsetting/individ/IndividConservation;->linearLayout_BackLight:Landroid/widget/LinearLayout;
    invoke-static {v2}, Lcom/konka/systemsetting/individ/IndividConservation;->access$20(Lcom/konka/systemsetting/individ/IndividConservation;)Landroid/widget/LinearLayout;

    move-result-object v2

    invoke-virtual {v2, v7}, Landroid/widget/LinearLayout;->getChildAt(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    iget-object v2, p0, Lcom/konka/systemsetting/individ/IndividConservation$8;->this$0:Lcom/konka/systemsetting/individ/IndividConservation;

    # getter for: Lcom/konka/systemsetting/individ/IndividConservation;->m_iBackLightVal:I
    invoke-static {v2}, Lcom/konka/systemsetting/individ/IndividConservation;->access$3(Lcom/konka/systemsetting/individ/IndividConservation;)I

    move-result v2

    invoke-static {v2}, Ljava/lang/String;->valueOf(I)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0, v2}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    goto :goto_0

    :cond_0
    iget-object v2, p0, Lcom/konka/systemsetting/individ/IndividConservation$8;->this$0:Lcom/konka/systemsetting/individ/IndividConservation;

    # getter for: Lcom/konka/systemsetting/individ/IndividConservation;->m_currItemId:Ljava/lang/Integer;
    invoke-static {v2}, Lcom/konka/systemsetting/individ/IndividConservation;->access$7(Lcom/konka/systemsetting/individ/IndividConservation;)Ljava/lang/Integer;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/Integer;->intValue()I

    move-result v2

    packed-switch v2, :pswitch_data_1

    :pswitch_3
    goto :goto_0

    :pswitch_4
    invoke-virtual {p1, v8}, Landroid/view/View;->setBackgroundResource(I)V

    goto :goto_0

    :pswitch_5
    iget-object v2, p0, Lcom/konka/systemsetting/individ/IndividConservation$8;->this$0:Lcom/konka/systemsetting/individ/IndividConservation;

    invoke-static {v2, v5}, Lcom/konka/systemsetting/individ/IndividConservation;->access$18(Lcom/konka/systemsetting/individ/IndividConservation;Z)V

    iget-object v2, p0, Lcom/konka/systemsetting/individ/IndividConservation$8;->this$0:Lcom/konka/systemsetting/individ/IndividConservation;

    # getter for: Lcom/konka/systemsetting/individ/IndividConservation;->linearLayout_BackLight:Landroid/widget/LinearLayout;
    invoke-static {v2}, Lcom/konka/systemsetting/individ/IndividConservation;->access$20(Lcom/konka/systemsetting/individ/IndividConservation;)Landroid/widget/LinearLayout;

    move-result-object v2

    invoke-virtual {v2, v8}, Landroid/widget/LinearLayout;->setBackgroundResource(I)V

    iget-object v2, p0, Lcom/konka/systemsetting/individ/IndividConservation$8;->this$0:Lcom/konka/systemsetting/individ/IndividConservation;

    # getter for: Lcom/konka/systemsetting/individ/IndividConservation;->seekbar_BackLight:Landroid/widget/SeekBar;
    invoke-static {v2}, Lcom/konka/systemsetting/individ/IndividConservation;->access$16(Lcom/konka/systemsetting/individ/IndividConservation;)Landroid/widget/SeekBar;

    move-result-object v2

    invoke-virtual {v2, v4}, Landroid/widget/SeekBar;->setVisibility(I)V

    iget-object v2, p0, Lcom/konka/systemsetting/individ/IndividConservation$8;->this$0:Lcom/konka/systemsetting/individ/IndividConservation;

    # getter for: Lcom/konka/systemsetting/individ/IndividConservation;->linearLayout_BackLight:Landroid/widget/LinearLayout;
    invoke-static {v2}, Lcom/konka/systemsetting/individ/IndividConservation;->access$20(Lcom/konka/systemsetting/individ/IndividConservation;)Landroid/widget/LinearLayout;

    move-result-object v2

    invoke-virtual {v2, v6}, Landroid/widget/LinearLayout;->getChildAt(I)Landroid/view/View;

    move-result-object v1

    check-cast v1, Landroid/widget/TextView;

    invoke-virtual {v1, v4}, Landroid/widget/TextView;->setVisibility(I)V

    iget-object v2, p0, Lcom/konka/systemsetting/individ/IndividConservation$8;->this$0:Lcom/konka/systemsetting/individ/IndividConservation;

    # getter for: Lcom/konka/systemsetting/individ/IndividConservation;->linearLayout_BackLight:Landroid/widget/LinearLayout;
    invoke-static {v2}, Lcom/konka/systemsetting/individ/IndividConservation;->access$20(Lcom/konka/systemsetting/individ/IndividConservation;)Landroid/widget/LinearLayout;

    move-result-object v2

    invoke-virtual {v2, v7}, Landroid/widget/LinearLayout;->getChildAt(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    iget-object v2, p0, Lcom/konka/systemsetting/individ/IndividConservation$8;->this$0:Lcom/konka/systemsetting/individ/IndividConservation;

    # getter for: Lcom/konka/systemsetting/individ/IndividConservation;->m_iBackLightVal:I
    invoke-static {v2}, Lcom/konka/systemsetting/individ/IndividConservation;->access$3(Lcom/konka/systemsetting/individ/IndividConservation;)I

    move-result v2

    invoke-static {v2}, Ljava/lang/String;->valueOf(I)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0, v2}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    goto/16 :goto_0

    :pswitch_data_0
    .packed-switch 0x7f090018
        :pswitch_1    # com.konka.systemsetting.R.id.sys_individ_energysv_item_energyconservation
        :pswitch_1    # com.konka.systemsetting.R.id.sys_individ_energysv_item_screensaver
        :pswitch_2    # com.konka.systemsetting.R.id.sys_individ_energysv_item_backlight
        :pswitch_0    # com.konka.systemsetting.R.id.sys_individ_energysv_item_backlight_valuemin
        :pswitch_2    # com.konka.systemsetting.R.id.sys_individ_energysv_item_backlight_seekbar
        :pswitch_0    # com.konka.systemsetting.R.id.sys_individ_energysv_item_backlight_valuemax
        :pswitch_1    # com.konka.systemsetting.R.id.sys_individ_energysv_item_nosignalstandby
        :pswitch_1    # com.konka.systemsetting.R.id.sys_individ_energysv_item_noactionstandby
    .end packed-switch

    :pswitch_data_1
    .packed-switch 0x7f090018
        :pswitch_4    # com.konka.systemsetting.R.id.sys_individ_energysv_item_energyconservation
        :pswitch_4    # com.konka.systemsetting.R.id.sys_individ_energysv_item_screensaver
        :pswitch_5    # com.konka.systemsetting.R.id.sys_individ_energysv_item_backlight
        :pswitch_3    # com.konka.systemsetting.R.id.sys_individ_energysv_item_backlight_valuemin
        :pswitch_5    # com.konka.systemsetting.R.id.sys_individ_energysv_item_backlight_seekbar
        :pswitch_3    # com.konka.systemsetting.R.id.sys_individ_energysv_item_backlight_valuemax
        :pswitch_4    # com.konka.systemsetting.R.id.sys_individ_energysv_item_nosignalstandby
        :pswitch_4    # com.konka.systemsetting.R.id.sys_individ_energysv_item_noactionstandby
    .end packed-switch
.end method
