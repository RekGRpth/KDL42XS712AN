.class Lcom/konka/systemsetting/individ/IndividComItem$ItemOnKeyEvent;
.super Ljava/lang/Object;
.source "IndividComItem.java"

# interfaces
.implements Landroid/view/View$OnKeyListener;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/konka/systemsetting/individ/IndividComItem;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x2
    name = "ItemOnKeyEvent"
.end annotation


# instance fields
.field final synthetic this$0:Lcom/konka/systemsetting/individ/IndividComItem;


# direct methods
.method private constructor <init>(Lcom/konka/systemsetting/individ/IndividComItem;)V
    .locals 0

    iput-object p1, p0, Lcom/konka/systemsetting/individ/IndividComItem$ItemOnKeyEvent;->this$0:Lcom/konka/systemsetting/individ/IndividComItem;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method synthetic constructor <init>(Lcom/konka/systemsetting/individ/IndividComItem;Lcom/konka/systemsetting/individ/IndividComItem$ItemOnKeyEvent;)V
    .locals 0

    invoke-direct {p0, p1}, Lcom/konka/systemsetting/individ/IndividComItem$ItemOnKeyEvent;-><init>(Lcom/konka/systemsetting/individ/IndividComItem;)V

    return-void
.end method


# virtual methods
.method public onKey(Landroid/view/View;ILandroid/view/KeyEvent;)Z
    .locals 3
    .param p1    # Landroid/view/View;
    .param p2    # I
    .param p3    # Landroid/view/KeyEvent;

    const/4 v0, 0x0

    const/16 v1, 0x16

    if-ne p2, v1, :cond_0

    invoke-virtual {p3}, Landroid/view/KeyEvent;->getAction()I

    move-result v1

    if-nez v1, :cond_0

    iget-object v1, p0, Lcom/konka/systemsetting/individ/IndividComItem$ItemOnKeyEvent;->this$0:Lcom/konka/systemsetting/individ/IndividComItem;

    # getter for: Lcom/konka/systemsetting/individ/IndividComItem;->strItemValues:[Ljava/lang/CharSequence;
    invoke-static {v1}, Lcom/konka/systemsetting/individ/IndividComItem;->access$0(Lcom/konka/systemsetting/individ/IndividComItem;)[Ljava/lang/CharSequence;

    move-result-object v1

    if-nez v1, :cond_1

    sget-object v1, Ljava/lang/System;->out:Ljava/io/PrintStream;

    const-string v2, "Third Menu Item Value Array Without Initalization!"

    invoke-virtual {v1, v2}, Ljava/io/PrintStream;->println(Ljava/lang/String;)V

    :cond_0
    :goto_0
    return v0

    :cond_1
    iget-object v1, p0, Lcom/konka/systemsetting/individ/IndividComItem$ItemOnKeyEvent;->this$0:Lcom/konka/systemsetting/individ/IndividComItem;

    # getter for: Lcom/konka/systemsetting/individ/IndividComItem;->iValueIndex:Ljava/lang/Integer;
    invoke-static {v1}, Lcom/konka/systemsetting/individ/IndividComItem;->access$1(Lcom/konka/systemsetting/individ/IndividComItem;)Ljava/lang/Integer;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/Integer;->intValue()I

    move-result v2

    add-int/lit8 v2, v2, 0x1

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    invoke-static {v1, v2}, Lcom/konka/systemsetting/individ/IndividComItem;->access$2(Lcom/konka/systemsetting/individ/IndividComItem;Ljava/lang/Integer;)V

    iget-object v1, p0, Lcom/konka/systemsetting/individ/IndividComItem$ItemOnKeyEvent;->this$0:Lcom/konka/systemsetting/individ/IndividComItem;

    # getter for: Lcom/konka/systemsetting/individ/IndividComItem;->iValueIndex:Ljava/lang/Integer;
    invoke-static {v1}, Lcom/konka/systemsetting/individ/IndividComItem;->access$1(Lcom/konka/systemsetting/individ/IndividComItem;)Ljava/lang/Integer;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/Integer;->intValue()I

    move-result v1

    iget-object v2, p0, Lcom/konka/systemsetting/individ/IndividComItem$ItemOnKeyEvent;->this$0:Lcom/konka/systemsetting/individ/IndividComItem;

    # getter for: Lcom/konka/systemsetting/individ/IndividComItem;->strItemValues:[Ljava/lang/CharSequence;
    invoke-static {v2}, Lcom/konka/systemsetting/individ/IndividComItem;->access$0(Lcom/konka/systemsetting/individ/IndividComItem;)[Ljava/lang/CharSequence;

    move-result-object v2

    array-length v2, v2

    if-lt v1, v2, :cond_2

    iget-object v1, p0, Lcom/konka/systemsetting/individ/IndividComItem$ItemOnKeyEvent;->this$0:Lcom/konka/systemsetting/individ/IndividComItem;

    invoke-static {v0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v0

    invoke-static {v1, v0}, Lcom/konka/systemsetting/individ/IndividComItem;->access$2(Lcom/konka/systemsetting/individ/IndividComItem;Ljava/lang/Integer;)V

    :cond_2
    iget-object v0, p0, Lcom/konka/systemsetting/individ/IndividComItem$ItemOnKeyEvent;->this$0:Lcom/konka/systemsetting/individ/IndividComItem;

    iget-object v1, p0, Lcom/konka/systemsetting/individ/IndividComItem$ItemOnKeyEvent;->this$0:Lcom/konka/systemsetting/individ/IndividComItem;

    # getter for: Lcom/konka/systemsetting/individ/IndividComItem;->iValueIndex:Ljava/lang/Integer;
    invoke-static {v1}, Lcom/konka/systemsetting/individ/IndividComItem;->access$1(Lcom/konka/systemsetting/individ/IndividComItem;)Ljava/lang/Integer;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/konka/systemsetting/individ/IndividComItem;->setItemValueIndex(Ljava/lang/Integer;)V

    sget-object v0, Ljava/lang/System;->out:Ljava/io/PrintStream;

    const-string v1, "Third Menu Item On Right Key Down, Item Value Updated!"

    invoke-virtual {v0, v1}, Ljava/io/PrintStream;->println(Ljava/lang/String;)V

    const/4 v0, 0x1

    goto :goto_0
.end method
