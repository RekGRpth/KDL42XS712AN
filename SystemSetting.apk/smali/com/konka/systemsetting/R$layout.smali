.class public final Lcom/konka/systemsetting/R$layout;
.super Ljava/lang/Object;
.source "R.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/konka/systemsetting/R;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x19
    name = "layout"
.end annotation


# static fields
.field public static final account_preference:I = 0x7f030000

.field public static final account_sync_screen:I = 0x7f030001

.field public static final activity_wifi_display_settings:I = 0x7f030002

.field public static final add_account_screen:I = 0x7f030003

.field public static final device_admin_add:I = 0x7f030004

.field public static final input_item:I = 0x7f030005

.field public static final manage_accounts_screen:I = 0x7f030006

.field public static final preference_empty_list:I = 0x7f030007

.field public static final preference_progress_category:I = 0x7f030008

.field public static final preference_widget_sync_toggle:I = 0x7f030009

.field public static final short_cut_view:I = 0x7f03000a

.field public static final simple_list_item_single_choice_timezone:I = 0x7f03000b

.field public static final sys_individ_appsetting:I = 0x7f03000c

.field public static final sys_individ_energysaving:I = 0x7f03000d

.field public static final sys_individ_setting_page:I = 0x7f03000e

.field public static final sys_individ_timeselection:I = 0x7f03000f

.field public static final sys_main_menu:I = 0x7f030010

.field public static final sys_network_pppoe:I = 0x7f030011

.field public static final sys_network_setting_page:I = 0x7f030012

.field public static final sys_network_softap:I = 0x7f030013

.field public static final sys_network_wifi_box:I = 0x7f030014

.field public static final sys_network_wired:I = 0x7f030015

.field public static final sys_network_wireless:I = 0x7f030016

.field public static final sys_storage_app_install_position:I = 0x7f030017

.field public static final sys_storage_flash:I = 0x7f030018

.field public static final sys_storage_sdcard:I = 0x7f030019

.field public static final sys_storage_setting_page:I = 0x7f03001a

.field public static final sys_sysinfo_item:I = 0x7f03001b

.field public static final sys_system_disclaimer:I = 0x7f03001c

.field public static final sys_system_factoryreset:I = 0x7f03001d

.field public static final sys_system_inputsettings:I = 0x7f03001e

.field public static final sys_system_languageandmusic:I = 0x7f03001f

.field public static final sys_system_setting_page:I = 0x7f030020

.field public static final sys_system_softupgrade:I = 0x7f030021

.field public static final sys_system_systeminfo:I = 0x7f030022

.field public static final system_home_page_layout:I = 0x7f030023

.field public static final title:I = 0x7f030024

.field public static final toast_dlg:I = 0x7f030025

.field public static final wifi_accesspoint:I = 0x7f030026

.field public static final wifi_display_options:I = 0x7f030027

.field public static final wifi_display_preference:I = 0x7f030028

.field public static final wifi_hotspots_list:I = 0x7f030029

.field public static final wifi_list_item:I = 0x7f03002a

.field public static final wifi_wps_dialog:I = 0x7f03002b

.field public static final wifi_wps_success:I = 0x7f03002c


# direct methods
.method public constructor <init>()V
    .locals 0

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method
