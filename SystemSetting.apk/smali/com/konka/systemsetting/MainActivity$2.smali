.class Lcom/konka/systemsetting/MainActivity$2;
.super Ljava/lang/Object;
.source "MainActivity.java"

# interfaces
.implements Landroid/view/View$OnFocusChangeListener;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/konka/systemsetting/MainActivity;->SetOnFocusChangeListener()V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/konka/systemsetting/MainActivity;


# direct methods
.method constructor <init>(Lcom/konka/systemsetting/MainActivity;)V
    .locals 0

    iput-object p1, p0, Lcom/konka/systemsetting/MainActivity$2;->this$0:Lcom/konka/systemsetting/MainActivity;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onFocusChange(Landroid/view/View;Z)V
    .locals 6
    .param p1    # Landroid/view/View;
    .param p2    # Z

    const/4 v5, 0x2

    const/4 v4, 0x1

    const/4 v3, 0x0

    invoke-virtual {p1}, Landroid/view/View;->getId()I

    move-result v0

    if-eqz p2, :cond_5

    iget-object v1, p0, Lcom/konka/systemsetting/MainActivity$2;->this$0:Lcom/konka/systemsetting/MainActivity;

    iget-object v1, v1, Lcom/konka/systemsetting/MainActivity;->m_systemPageManager:Lcom/konka/systemsetting/system/SystemPageManager;

    if-eqz v1, :cond_0

    iget-object v1, p0, Lcom/konka/systemsetting/MainActivity$2;->this$0:Lcom/konka/systemsetting/MainActivity;

    iget-object v1, v1, Lcom/konka/systemsetting/MainActivity;->m_systemPageManager:Lcom/konka/systemsetting/system/SystemPageManager;

    invoke-virtual {v1}, Lcom/konka/systemsetting/system/SystemPageManager;->resetItemStatus()V

    :cond_0
    iget-object v1, p0, Lcom/konka/systemsetting/MainActivity$2;->this$0:Lcom/konka/systemsetting/MainActivity;

    iget-object v1, v1, Lcom/konka/systemsetting/MainActivity;->m_individPageManager:Lcom/konka/systemsetting/individ/IndividPageManager;

    if-eqz v1, :cond_1

    iget-object v1, p0, Lcom/konka/systemsetting/MainActivity$2;->this$0:Lcom/konka/systemsetting/MainActivity;

    iget-object v1, v1, Lcom/konka/systemsetting/MainActivity;->m_individPageManager:Lcom/konka/systemsetting/individ/IndividPageManager;

    invoke-virtual {v1}, Lcom/konka/systemsetting/individ/IndividPageManager;->resetItemStatus()V

    :cond_1
    iget-object v1, p0, Lcom/konka/systemsetting/MainActivity$2;->this$0:Lcom/konka/systemsetting/MainActivity;

    iget-object v1, v1, Lcom/konka/systemsetting/MainActivity;->m_networkPageManager:Lcom/konka/systemsetting/net/NetworkPageManager;

    if-eqz v1, :cond_2

    iget-object v1, p0, Lcom/konka/systemsetting/MainActivity$2;->this$0:Lcom/konka/systemsetting/MainActivity;

    iget-object v1, v1, Lcom/konka/systemsetting/MainActivity;->m_networkPageManager:Lcom/konka/systemsetting/net/NetworkPageManager;

    invoke-virtual {v1}, Lcom/konka/systemsetting/net/NetworkPageManager;->resetItemStatus()V

    :cond_2
    iget-object v1, p0, Lcom/konka/systemsetting/MainActivity$2;->this$0:Lcom/konka/systemsetting/MainActivity;

    iget-object v1, v1, Lcom/konka/systemsetting/MainActivity;->m_storagePgaeManager:Lcom/konka/systemsetting/storage/StoragePageManager;

    if-eqz v1, :cond_3

    iget-object v1, p0, Lcom/konka/systemsetting/MainActivity$2;->this$0:Lcom/konka/systemsetting/MainActivity;

    iget-object v1, v1, Lcom/konka/systemsetting/MainActivity;->m_storagePgaeManager:Lcom/konka/systemsetting/storage/StoragePageManager;

    invoke-virtual {v1}, Lcom/konka/systemsetting/storage/StoragePageManager;->resetItemStatus()V

    :cond_3
    packed-switch v0, :pswitch_data_0

    :goto_0
    return-void

    :pswitch_0
    iget-object v1, p0, Lcom/konka/systemsetting/MainActivity$2;->this$0:Lcom/konka/systemsetting/MainActivity;

    iget-object v2, p0, Lcom/konka/systemsetting/MainActivity$2;->this$0:Lcom/konka/systemsetting/MainActivity;

    # getter for: Lcom/konka/systemsetting/MainActivity;->m_iCurPage:I
    invoke-static {v2}, Lcom/konka/systemsetting/MainActivity;->access$4(Lcom/konka/systemsetting/MainActivity;)I

    move-result v2

    invoke-static {v1, v2}, Lcom/konka/systemsetting/MainActivity;->access$5(Lcom/konka/systemsetting/MainActivity;I)V

    iget-object v1, p0, Lcom/konka/systemsetting/MainActivity$2;->this$0:Lcom/konka/systemsetting/MainActivity;

    invoke-static {v1, v3}, Lcom/konka/systemsetting/MainActivity;->access$6(Lcom/konka/systemsetting/MainActivity;I)V

    :goto_1
    iget-object v1, p0, Lcom/konka/systemsetting/MainActivity$2;->this$0:Lcom/konka/systemsetting/MainActivity;

    # getter for: Lcom/konka/systemsetting/MainActivity;->m_iCurPage:I
    invoke-static {v1}, Lcom/konka/systemsetting/MainActivity;->access$4(Lcom/konka/systemsetting/MainActivity;)I

    move-result v1

    iget-object v2, p0, Lcom/konka/systemsetting/MainActivity$2;->this$0:Lcom/konka/systemsetting/MainActivity;

    # getter for: Lcom/konka/systemsetting/MainActivity;->m_iLastFocusPage:I
    invoke-static {v2}, Lcom/konka/systemsetting/MainActivity;->access$7(Lcom/konka/systemsetting/MainActivity;)I

    move-result v2

    if-eq v1, v2, :cond_4

    iget-object v1, p0, Lcom/konka/systemsetting/MainActivity$2;->this$0:Lcom/konka/systemsetting/MainActivity;

    iget-object v2, p0, Lcom/konka/systemsetting/MainActivity$2;->this$0:Lcom/konka/systemsetting/MainActivity;

    # getter for: Lcom/konka/systemsetting/MainActivity;->m_iLastFocusPage:I
    invoke-static {v2}, Lcom/konka/systemsetting/MainActivity;->access$7(Lcom/konka/systemsetting/MainActivity;)I

    move-result v2

    # invokes: Lcom/konka/systemsetting/MainActivity;->changeItemStatus(II)V
    invoke-static {v1, v2, v5}, Lcom/konka/systemsetting/MainActivity;->access$8(Lcom/konka/systemsetting/MainActivity;II)V

    :cond_4
    iget-object v1, p0, Lcom/konka/systemsetting/MainActivity$2;->this$0:Lcom/konka/systemsetting/MainActivity;

    iget-object v2, p0, Lcom/konka/systemsetting/MainActivity$2;->this$0:Lcom/konka/systemsetting/MainActivity;

    # getter for: Lcom/konka/systemsetting/MainActivity;->m_iCurPage:I
    invoke-static {v2}, Lcom/konka/systemsetting/MainActivity;->access$4(Lcom/konka/systemsetting/MainActivity;)I

    move-result v2

    # invokes: Lcom/konka/systemsetting/MainActivity;->changeItemStatus(II)V
    invoke-static {v1, v2, v3}, Lcom/konka/systemsetting/MainActivity;->access$8(Lcom/konka/systemsetting/MainActivity;II)V

    iget-object v1, p0, Lcom/konka/systemsetting/MainActivity$2;->this$0:Lcom/konka/systemsetting/MainActivity;

    iget-object v2, p0, Lcom/konka/systemsetting/MainActivity$2;->this$0:Lcom/konka/systemsetting/MainActivity;

    # getter for: Lcom/konka/systemsetting/MainActivity;->m_iCurPage:I
    invoke-static {v2}, Lcom/konka/systemsetting/MainActivity;->access$4(Lcom/konka/systemsetting/MainActivity;)I

    move-result v2

    # invokes: Lcom/konka/systemsetting/MainActivity;->showPage(II)V
    invoke-static {v1, v2, v3}, Lcom/konka/systemsetting/MainActivity;->access$9(Lcom/konka/systemsetting/MainActivity;II)V

    goto :goto_0

    :pswitch_1
    iget-object v1, p0, Lcom/konka/systemsetting/MainActivity$2;->this$0:Lcom/konka/systemsetting/MainActivity;

    iget-object v2, p0, Lcom/konka/systemsetting/MainActivity$2;->this$0:Lcom/konka/systemsetting/MainActivity;

    # getter for: Lcom/konka/systemsetting/MainActivity;->m_iCurPage:I
    invoke-static {v2}, Lcom/konka/systemsetting/MainActivity;->access$4(Lcom/konka/systemsetting/MainActivity;)I

    move-result v2

    invoke-static {v1, v2}, Lcom/konka/systemsetting/MainActivity;->access$5(Lcom/konka/systemsetting/MainActivity;I)V

    iget-object v1, p0, Lcom/konka/systemsetting/MainActivity$2;->this$0:Lcom/konka/systemsetting/MainActivity;

    invoke-static {v1, v4}, Lcom/konka/systemsetting/MainActivity;->access$6(Lcom/konka/systemsetting/MainActivity;I)V

    goto :goto_1

    :pswitch_2
    iget-object v1, p0, Lcom/konka/systemsetting/MainActivity$2;->this$0:Lcom/konka/systemsetting/MainActivity;

    iget-object v2, p0, Lcom/konka/systemsetting/MainActivity$2;->this$0:Lcom/konka/systemsetting/MainActivity;

    # getter for: Lcom/konka/systemsetting/MainActivity;->m_iCurPage:I
    invoke-static {v2}, Lcom/konka/systemsetting/MainActivity;->access$4(Lcom/konka/systemsetting/MainActivity;)I

    move-result v2

    invoke-static {v1, v2}, Lcom/konka/systemsetting/MainActivity;->access$5(Lcom/konka/systemsetting/MainActivity;I)V

    iget-object v1, p0, Lcom/konka/systemsetting/MainActivity$2;->this$0:Lcom/konka/systemsetting/MainActivity;

    invoke-static {v1, v5}, Lcom/konka/systemsetting/MainActivity;->access$6(Lcom/konka/systemsetting/MainActivity;I)V

    goto :goto_1

    :pswitch_3
    iget-object v1, p0, Lcom/konka/systemsetting/MainActivity$2;->this$0:Lcom/konka/systemsetting/MainActivity;

    iget-object v2, p0, Lcom/konka/systemsetting/MainActivity$2;->this$0:Lcom/konka/systemsetting/MainActivity;

    # getter for: Lcom/konka/systemsetting/MainActivity;->m_iCurPage:I
    invoke-static {v2}, Lcom/konka/systemsetting/MainActivity;->access$4(Lcom/konka/systemsetting/MainActivity;)I

    move-result v2

    invoke-static {v1, v2}, Lcom/konka/systemsetting/MainActivity;->access$5(Lcom/konka/systemsetting/MainActivity;I)V

    iget-object v1, p0, Lcom/konka/systemsetting/MainActivity$2;->this$0:Lcom/konka/systemsetting/MainActivity;

    const/4 v2, 0x3

    invoke-static {v1, v2}, Lcom/konka/systemsetting/MainActivity;->access$6(Lcom/konka/systemsetting/MainActivity;I)V

    goto :goto_1

    :cond_5
    iget-object v1, p0, Lcom/konka/systemsetting/MainActivity$2;->this$0:Lcom/konka/systemsetting/MainActivity;

    iget-object v2, p0, Lcom/konka/systemsetting/MainActivity$2;->this$0:Lcom/konka/systemsetting/MainActivity;

    # getter for: Lcom/konka/systemsetting/MainActivity;->m_iCurPage:I
    invoke-static {v2}, Lcom/konka/systemsetting/MainActivity;->access$4(Lcom/konka/systemsetting/MainActivity;)I

    move-result v2

    # invokes: Lcom/konka/systemsetting/MainActivity;->changeItemStatus(II)V
    invoke-static {v1, v2, v4}, Lcom/konka/systemsetting/MainActivity;->access$8(Lcom/konka/systemsetting/MainActivity;II)V

    goto :goto_0

    nop

    :pswitch_data_0
    .packed-switch 0x7f090030
        :pswitch_0    # com.konka.systemsetting.R.id.sys_main_menu_item_network
        :pswitch_1    # com.konka.systemsetting.R.id.sys_main_menu_item_system
        :pswitch_2    # com.konka.systemsetting.R.id.sys_main_menu_item_individ
        :pswitch_3    # com.konka.systemsetting.R.id.sys_main_menu_item_storage
    .end packed-switch
.end method
