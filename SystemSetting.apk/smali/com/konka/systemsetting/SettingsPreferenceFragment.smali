.class public Lcom/konka/systemsetting/SettingsPreferenceFragment;
.super Landroid/preference/PreferenceFragment;
.source "SettingsPreferenceFragment.java"

# interfaces
.implements Lcom/konka/systemsetting/DialogCreatable;


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/konka/systemsetting/SettingsPreferenceFragment$SettingsDialogFragment;
    }
.end annotation


# static fields
.field private static final TAG:Ljava/lang/String; = "SettingsPreferenceFragment"


# instance fields
.field private mDialogFragment:Lcom/konka/systemsetting/SettingsPreferenceFragment$SettingsDialogFragment;


# direct methods
.method public constructor <init>()V
    .locals 0

    invoke-direct {p0}, Landroid/preference/PreferenceFragment;-><init>()V

    return-void
.end method

.method static synthetic access$0(Lcom/konka/systemsetting/SettingsPreferenceFragment;Lcom/konka/systemsetting/SettingsPreferenceFragment$SettingsDialogFragment;)V
    .locals 0

    iput-object p1, p0, Lcom/konka/systemsetting/SettingsPreferenceFragment;->mDialogFragment:Lcom/konka/systemsetting/SettingsPreferenceFragment$SettingsDialogFragment;

    return-void
.end method

.method static synthetic access$1(Lcom/konka/systemsetting/SettingsPreferenceFragment;)Lcom/konka/systemsetting/SettingsPreferenceFragment$SettingsDialogFragment;
    .locals 1

    iget-object v0, p0, Lcom/konka/systemsetting/SettingsPreferenceFragment;->mDialogFragment:Lcom/konka/systemsetting/SettingsPreferenceFragment$SettingsDialogFragment;

    return-object v0
.end method


# virtual methods
.method public finish()V
    .locals 1

    invoke-virtual {p0}, Lcom/konka/systemsetting/SettingsPreferenceFragment;->getActivity()Landroid/app/Activity;

    move-result-object v0

    invoke-virtual {v0}, Landroid/app/Activity;->onBackPressed()V

    return-void
.end method

.method public final finishFragment()V
    .locals 1

    invoke-virtual {p0}, Lcom/konka/systemsetting/SettingsPreferenceFragment;->getActivity()Landroid/app/Activity;

    move-result-object v0

    invoke-virtual {v0}, Landroid/app/Activity;->onBackPressed()V

    return-void
.end method

.method protected getContentResolver()Landroid/content/ContentResolver;
    .locals 1

    invoke-virtual {p0}, Lcom/konka/systemsetting/SettingsPreferenceFragment;->getActivity()Landroid/app/Activity;

    move-result-object v0

    invoke-virtual {v0}, Landroid/app/Activity;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v0

    return-object v0
.end method

.method protected getPackageManager()Landroid/content/pm/PackageManager;
    .locals 1

    invoke-virtual {p0}, Lcom/konka/systemsetting/SettingsPreferenceFragment;->getActivity()Landroid/app/Activity;

    move-result-object v0

    invoke-virtual {v0}, Landroid/app/Activity;->getPackageManager()Landroid/content/pm/PackageManager;

    move-result-object v0

    return-object v0
.end method

.method protected getSystemService(Ljava/lang/String;)Ljava/lang/Object;
    .locals 1
    .param p1    # Ljava/lang/String;

    invoke-virtual {p0}, Lcom/konka/systemsetting/SettingsPreferenceFragment;->getActivity()Landroid/app/Activity;

    move-result-object v0

    invoke-virtual {v0, p1}, Landroid/app/Activity;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v0

    return-object v0
.end method

.method public onActivityCreated(Landroid/os/Bundle;)V
    .locals 0
    .param p1    # Landroid/os/Bundle;

    invoke-super {p0, p1}, Landroid/preference/PreferenceFragment;->onActivityCreated(Landroid/os/Bundle;)V

    return-void
.end method

.method public onCreateDialog(I)Landroid/app/Dialog;
    .locals 1
    .param p1    # I

    const/4 v0, 0x0

    return-object v0
.end method

.method public onDetach()V
    .locals 1

    invoke-virtual {p0}, Lcom/konka/systemsetting/SettingsPreferenceFragment;->isRemoving()Z

    move-result v0

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/konka/systemsetting/SettingsPreferenceFragment;->mDialogFragment:Lcom/konka/systemsetting/SettingsPreferenceFragment$SettingsDialogFragment;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/konka/systemsetting/SettingsPreferenceFragment;->mDialogFragment:Lcom/konka/systemsetting/SettingsPreferenceFragment$SettingsDialogFragment;

    invoke-virtual {v0}, Lcom/konka/systemsetting/SettingsPreferenceFragment$SettingsDialogFragment;->dismiss()V

    const/4 v0, 0x0

    iput-object v0, p0, Lcom/konka/systemsetting/SettingsPreferenceFragment;->mDialogFragment:Lcom/konka/systemsetting/SettingsPreferenceFragment$SettingsDialogFragment;

    :cond_0
    invoke-super {p0}, Landroid/preference/PreferenceFragment;->onDetach()V

    return-void
.end method

.method protected removeDialog(I)V
    .locals 1
    .param p1    # I

    iget-object v0, p0, Lcom/konka/systemsetting/SettingsPreferenceFragment;->mDialogFragment:Lcom/konka/systemsetting/SettingsPreferenceFragment$SettingsDialogFragment;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/konka/systemsetting/SettingsPreferenceFragment;->mDialogFragment:Lcom/konka/systemsetting/SettingsPreferenceFragment$SettingsDialogFragment;

    invoke-virtual {v0}, Lcom/konka/systemsetting/SettingsPreferenceFragment$SettingsDialogFragment;->getDialogId()I

    move-result v0

    if-ne v0, p1, :cond_0

    iget-object v0, p0, Lcom/konka/systemsetting/SettingsPreferenceFragment;->mDialogFragment:Lcom/konka/systemsetting/SettingsPreferenceFragment$SettingsDialogFragment;

    invoke-virtual {v0}, Lcom/konka/systemsetting/SettingsPreferenceFragment$SettingsDialogFragment;->dismiss()V

    :cond_0
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/konka/systemsetting/SettingsPreferenceFragment;->mDialogFragment:Lcom/konka/systemsetting/SettingsPreferenceFragment$SettingsDialogFragment;

    return-void
.end method

.method protected setOnCancelListener(Landroid/content/DialogInterface$OnCancelListener;)V
    .locals 1
    .param p1    # Landroid/content/DialogInterface$OnCancelListener;

    iget-object v0, p0, Lcom/konka/systemsetting/SettingsPreferenceFragment;->mDialogFragment:Lcom/konka/systemsetting/SettingsPreferenceFragment$SettingsDialogFragment;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/konka/systemsetting/SettingsPreferenceFragment;->mDialogFragment:Lcom/konka/systemsetting/SettingsPreferenceFragment$SettingsDialogFragment;

    invoke-static {v0, p1}, Lcom/konka/systemsetting/SettingsPreferenceFragment$SettingsDialogFragment;->access$0(Lcom/konka/systemsetting/SettingsPreferenceFragment$SettingsDialogFragment;Landroid/content/DialogInterface$OnCancelListener;)V

    :cond_0
    return-void
.end method

.method protected setOnDismissListener(Landroid/content/DialogInterface$OnDismissListener;)V
    .locals 1
    .param p1    # Landroid/content/DialogInterface$OnDismissListener;

    iget-object v0, p0, Lcom/konka/systemsetting/SettingsPreferenceFragment;->mDialogFragment:Lcom/konka/systemsetting/SettingsPreferenceFragment$SettingsDialogFragment;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/konka/systemsetting/SettingsPreferenceFragment;->mDialogFragment:Lcom/konka/systemsetting/SettingsPreferenceFragment$SettingsDialogFragment;

    invoke-static {v0, p1}, Lcom/konka/systemsetting/SettingsPreferenceFragment$SettingsDialogFragment;->access$1(Lcom/konka/systemsetting/SettingsPreferenceFragment$SettingsDialogFragment;Landroid/content/DialogInterface$OnDismissListener;)V

    :cond_0
    return-void
.end method

.method protected showDialog(I)V
    .locals 3
    .param p1    # I

    iget-object v0, p0, Lcom/konka/systemsetting/SettingsPreferenceFragment;->mDialogFragment:Lcom/konka/systemsetting/SettingsPreferenceFragment$SettingsDialogFragment;

    if-eqz v0, :cond_0

    const-string v0, "SettingsPreferenceFragment"

    const-string v1, "Old dialog fragment not null!"

    invoke-static {v0, v1}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    :cond_0
    new-instance v0, Lcom/konka/systemsetting/SettingsPreferenceFragment$SettingsDialogFragment;

    invoke-direct {v0, p0, p1}, Lcom/konka/systemsetting/SettingsPreferenceFragment$SettingsDialogFragment;-><init>(Lcom/konka/systemsetting/DialogCreatable;I)V

    iput-object v0, p0, Lcom/konka/systemsetting/SettingsPreferenceFragment;->mDialogFragment:Lcom/konka/systemsetting/SettingsPreferenceFragment$SettingsDialogFragment;

    iget-object v0, p0, Lcom/konka/systemsetting/SettingsPreferenceFragment;->mDialogFragment:Lcom/konka/systemsetting/SettingsPreferenceFragment$SettingsDialogFragment;

    invoke-virtual {p0}, Lcom/konka/systemsetting/SettingsPreferenceFragment;->getActivity()Landroid/app/Activity;

    move-result-object v1

    invoke-virtual {v1}, Landroid/app/Activity;->getFragmentManager()Landroid/app/FragmentManager;

    move-result-object v1

    invoke-static {p1}, Ljava/lang/Integer;->toString(I)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Lcom/konka/systemsetting/SettingsPreferenceFragment$SettingsDialogFragment;->show(Landroid/app/FragmentManager;Ljava/lang/String;)V

    return-void
.end method

.method public startFragment(Landroid/app/Fragment;Ljava/lang/String;ILandroid/os/Bundle;)Z
    .locals 7
    .param p1    # Landroid/app/Fragment;
    .param p2    # Ljava/lang/String;
    .param p3    # I
    .param p4    # Landroid/os/Bundle;

    invoke-virtual {p0}, Lcom/konka/systemsetting/SettingsPreferenceFragment;->getActivity()Landroid/app/Activity;

    move-result-object v1

    instance-of v1, v1, Landroid/preference/PreferenceActivity;

    if-eqz v1, :cond_0

    invoke-virtual {p0}, Lcom/konka/systemsetting/SettingsPreferenceFragment;->getActivity()Landroid/app/Activity;

    move-result-object v0

    check-cast v0, Landroid/preference/PreferenceActivity;

    const v3, 0x7f0600e5    # com.konka.systemsetting.R.string.lock_settings_picker_title

    const/4 v4, 0x0

    move-object v1, p2

    move-object v2, p4

    move-object v5, p1

    move v6, p3

    invoke-virtual/range {v0 .. v6}, Landroid/preference/PreferenceActivity;->startPreferencePanel(Ljava/lang/String;Landroid/os/Bundle;ILjava/lang/CharSequence;Landroid/app/Fragment;I)V

    const/4 v1, 0x1

    :goto_0
    return v1

    :cond_0
    const-string v1, "SettingsPreferenceFragment"

    new-instance v2, Ljava/lang/StringBuilder;

    const-string v3, "Parent isn\'t PreferenceActivity, thus there\'s no way to launch the given Fragment (name: "

    invoke-direct {v2, v3}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v2, p2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, ", requestCode: "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, p3}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, ")"

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;)I

    const/4 v1, 0x0

    goto :goto_0
.end method
