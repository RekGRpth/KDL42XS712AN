.class Lcom/konka/systemsetting/net/NetworkWifiConfig$Scanner;
.super Landroid/os/Handler;
.source "NetworkWifiConfig.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/konka/systemsetting/net/NetworkWifiConfig;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x2
    name = "Scanner"
.end annotation


# instance fields
.field private mRetry:I

.field final synthetic this$0:Lcom/konka/systemsetting/net/NetworkWifiConfig;


# direct methods
.method private constructor <init>(Lcom/konka/systemsetting/net/NetworkWifiConfig;)V
    .locals 1

    iput-object p1, p0, Lcom/konka/systemsetting/net/NetworkWifiConfig$Scanner;->this$0:Lcom/konka/systemsetting/net/NetworkWifiConfig;

    invoke-direct {p0}, Landroid/os/Handler;-><init>()V

    const/4 v0, 0x0

    iput v0, p0, Lcom/konka/systemsetting/net/NetworkWifiConfig$Scanner;->mRetry:I

    return-void
.end method

.method synthetic constructor <init>(Lcom/konka/systemsetting/net/NetworkWifiConfig;Lcom/konka/systemsetting/net/NetworkWifiConfig$Scanner;)V
    .locals 0

    invoke-direct {p0, p1}, Lcom/konka/systemsetting/net/NetworkWifiConfig$Scanner;-><init>(Lcom/konka/systemsetting/net/NetworkWifiConfig;)V

    return-void
.end method


# virtual methods
.method forceScan()V
    .locals 1

    const/4 v0, 0x0

    invoke-virtual {p0, v0}, Lcom/konka/systemsetting/net/NetworkWifiConfig$Scanner;->removeMessages(I)V

    invoke-virtual {p0, v0}, Lcom/konka/systemsetting/net/NetworkWifiConfig$Scanner;->sendEmptyMessage(I)Z

    return-void
.end method

.method public handleMessage(Landroid/os/Message;)V
    .locals 3
    .param p1    # Landroid/os/Message;

    const/4 v2, 0x0

    iget-object v0, p0, Lcom/konka/systemsetting/net/NetworkWifiConfig$Scanner;->this$0:Lcom/konka/systemsetting/net/NetworkWifiConfig;

    # getter for: Lcom/konka/systemsetting/net/NetworkWifiConfig;->manager:Landroid/net/wifi/WifiManager;
    invoke-static {v0}, Lcom/konka/systemsetting/net/NetworkWifiConfig;->access$7(Lcom/konka/systemsetting/net/NetworkWifiConfig;)Landroid/net/wifi/WifiManager;

    move-result-object v0

    invoke-virtual {v0}, Landroid/net/wifi/WifiManager;->isWifiEnabled()Z

    move-result v0

    if-eqz v0, :cond_1

    iget-object v0, p0, Lcom/konka/systemsetting/net/NetworkWifiConfig$Scanner;->this$0:Lcom/konka/systemsetting/net/NetworkWifiConfig;

    const-string v1, "===>startScanActive"

    invoke-virtual {v0, v1}, Lcom/konka/systemsetting/net/NetworkWifiConfig;->log(Ljava/lang/String;)V

    iget-object v0, p0, Lcom/konka/systemsetting/net/NetworkWifiConfig$Scanner;->this$0:Lcom/konka/systemsetting/net/NetworkWifiConfig;

    # getter for: Lcom/konka/systemsetting/net/NetworkWifiConfig;->manager:Landroid/net/wifi/WifiManager;
    invoke-static {v0}, Lcom/konka/systemsetting/net/NetworkWifiConfig;->access$7(Lcom/konka/systemsetting/net/NetworkWifiConfig;)Landroid/net/wifi/WifiManager;

    move-result-object v0

    invoke-virtual {v0}, Landroid/net/wifi/WifiManager;->startScanActive()Z

    move-result v0

    if-eqz v0, :cond_2

    iput v2, p0, Lcom/konka/systemsetting/net/NetworkWifiConfig$Scanner;->mRetry:I

    :cond_0
    const-wide/16 v0, 0x2710

    invoke-virtual {p0, v2, v0, v1}, Lcom/konka/systemsetting/net/NetworkWifiConfig$Scanner;->sendEmptyMessageDelayed(IJ)Z

    :cond_1
    :goto_0
    return-void

    :cond_2
    iget v0, p0, Lcom/konka/systemsetting/net/NetworkWifiConfig$Scanner;->mRetry:I

    add-int/lit8 v0, v0, 0x1

    iput v0, p0, Lcom/konka/systemsetting/net/NetworkWifiConfig$Scanner;->mRetry:I

    const/4 v1, 0x3

    if-lt v0, v1, :cond_0

    iput v2, p0, Lcom/konka/systemsetting/net/NetworkWifiConfig$Scanner;->mRetry:I

    iget-object v0, p0, Lcom/konka/systemsetting/net/NetworkWifiConfig$Scanner;->this$0:Lcom/konka/systemsetting/net/NetworkWifiConfig;

    const-string v1, "===>retry to scan failed for over 3 times, do not scan anymore"

    invoke-virtual {v0, v1}, Lcom/konka/systemsetting/net/NetworkWifiConfig;->log(Ljava/lang/String;)V

    goto :goto_0
.end method

.method pause()V
    .locals 1

    const/4 v0, 0x0

    iput v0, p0, Lcom/konka/systemsetting/net/NetworkWifiConfig$Scanner;->mRetry:I

    invoke-virtual {p0, v0}, Lcom/konka/systemsetting/net/NetworkWifiConfig$Scanner;->removeMessages(I)V

    return-void
.end method

.method resume()V
    .locals 2

    const/4 v1, 0x0

    invoke-virtual {p0, v1}, Lcom/konka/systemsetting/net/NetworkWifiConfig$Scanner;->hasMessages(I)Z

    move-result v0

    if-nez v0, :cond_0

    invoke-virtual {p0, v1}, Lcom/konka/systemsetting/net/NetworkWifiConfig$Scanner;->sendEmptyMessage(I)Z

    :cond_0
    return-void
.end method
