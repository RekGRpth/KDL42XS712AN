.class Lcom/konka/systemsetting/net/NetworkSoftApViewHolder$1;
.super Ljava/lang/Object;
.source "NetworkSoftApViewHolder.java"

# interfaces
.implements Landroid/view/View$OnFocusChangeListener;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/konka/systemsetting/net/NetworkSoftApViewHolder;->setListeners()V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/konka/systemsetting/net/NetworkSoftApViewHolder;


# direct methods
.method constructor <init>(Lcom/konka/systemsetting/net/NetworkSoftApViewHolder;)V
    .locals 0

    iput-object p1, p0, Lcom/konka/systemsetting/net/NetworkSoftApViewHolder$1;->this$0:Lcom/konka/systemsetting/net/NetworkSoftApViewHolder;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onFocusChange(Landroid/view/View;Z)V
    .locals 2
    .param p1    # Landroid/view/View;
    .param p2    # Z

    if-eqz p2, :cond_0

    iget-object v0, p0, Lcom/konka/systemsetting/net/NetworkSoftApViewHolder$1;->this$0:Lcom/konka/systemsetting/net/NetworkSoftApViewHolder;

    iget-object v0, v0, Lcom/konka/systemsetting/net/NetworkSoftApViewHolder;->etSSID:Landroid/widget/EditText;

    invoke-virtual {v0}, Landroid/widget/EditText;->selectAll()V

    iget-object v0, p0, Lcom/konka/systemsetting/net/NetworkSoftApViewHolder$1;->this$0:Lcom/konka/systemsetting/net/NetworkSoftApViewHolder;

    iget-object v0, v0, Lcom/konka/systemsetting/net/NetworkSoftApViewHolder;->itemSSID:Landroid/widget/LinearLayout;

    const v1, 0x7f020034    # com.konka.systemsetting.R.drawable.syssettingthirditemtopfocus

    invoke-virtual {v0, v1}, Landroid/widget/LinearLayout;->setBackgroundResource(I)V

    :goto_0
    return-void

    :cond_0
    iget-object v0, p0, Lcom/konka/systemsetting/net/NetworkSoftApViewHolder$1;->this$0:Lcom/konka/systemsetting/net/NetworkSoftApViewHolder;

    iget-object v0, v0, Lcom/konka/systemsetting/net/NetworkSoftApViewHolder;->itemSSID:Landroid/widget/LinearLayout;

    const v1, 0x7f020033    # com.konka.systemsetting.R.drawable.syssettingthirditemtop

    invoke-virtual {v0, v1}, Landroid/widget/LinearLayout;->setBackgroundResource(I)V

    goto :goto_0
.end method
