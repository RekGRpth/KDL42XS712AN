.class public Lcom/konka/systemsetting/net/NetworkPageManager;
.super Ljava/lang/Object;
.source "NetworkPageManager.java"


# instance fields
.field protected final PAGE_PPOE_CONFIG:I

.field protected final PAGE_SOFTAP_CONFIG:I

.field protected final PAGE_WIRED_CONFIG:I

.field protected final PAGE_WIRELESS_CONFIG:I

.field protected final PAGE_WIRELESS_DISPLAY:I

.field protected final STATE_CURRENT_FOCUSED:I

.field protected final STATE_LAST_FOCUSED:I

.field protected final STATE_NO_FOCUSE:I

.field currentConnectFormat:I

.field private focusIsRight:Z

.field isInPageOfWifiCfg:Z

.field isModifyConnectFormat:Z

.field isPPPOEModifyConnectFormat:Z

.field isWireAutoIpAddress:Z

.field isWirePPPPOE:Z

.field isWirelessAutoIpAddress:Z

.field private mContext:Lcom/konka/systemsetting/MainActivity;

.field private mViewHolder:Lcom/konka/systemsetting/ViewHolder;

.field protected m_PppoeConfig:Lcom/konka/systemsetting/net/NetworkPPPoeConfig;

.field protected m_SoftApConfig:Lcom/konka/systemsetting/net/NetworkSoftApConfig;

.field protected m_WifiConfig:Lcom/konka/systemsetting/net/NetworkWifiConfig;

.field protected m_WiredConfig:Lcom/konka/systemsetting/net/NetworkWireConfig;

.field private m_iCurPage:I

.field private m_iLastFocusPage:I

.field pppoeIsConnect:Z


# direct methods
.method public constructor <init>(Lcom/konka/systemsetting/MainActivity;Lcom/konka/systemsetting/ViewHolder;I)V
    .locals 3
    .param p1    # Lcom/konka/systemsetting/MainActivity;
    .param p2    # Lcom/konka/systemsetting/ViewHolder;
    .param p3    # I

    const/4 v2, 0x2

    const/4 v0, 0x1

    const/4 v1, 0x0

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    iput-boolean v1, p0, Lcom/konka/systemsetting/net/NetworkPageManager;->focusIsRight:Z

    iput-boolean v1, p0, Lcom/konka/systemsetting/net/NetworkPageManager;->isModifyConnectFormat:Z

    iput-boolean v1, p0, Lcom/konka/systemsetting/net/NetworkPageManager;->isPPPOEModifyConnectFormat:Z

    iput-boolean v0, p0, Lcom/konka/systemsetting/net/NetworkPageManager;->isWirePPPPOE:Z

    iput v1, p0, Lcom/konka/systemsetting/net/NetworkPageManager;->currentConnectFormat:I

    iput-boolean v1, p0, Lcom/konka/systemsetting/net/NetworkPageManager;->pppoeIsConnect:Z

    iput v1, p0, Lcom/konka/systemsetting/net/NetworkPageManager;->STATE_CURRENT_FOCUSED:I

    iput v0, p0, Lcom/konka/systemsetting/net/NetworkPageManager;->STATE_LAST_FOCUSED:I

    iput v2, p0, Lcom/konka/systemsetting/net/NetworkPageManager;->STATE_NO_FOCUSE:I

    iput v1, p0, Lcom/konka/systemsetting/net/NetworkPageManager;->PAGE_WIRED_CONFIG:I

    iput v0, p0, Lcom/konka/systemsetting/net/NetworkPageManager;->PAGE_WIRELESS_CONFIG:I

    iput v2, p0, Lcom/konka/systemsetting/net/NetworkPageManager;->PAGE_PPOE_CONFIG:I

    const/4 v0, 0x3

    iput v0, p0, Lcom/konka/systemsetting/net/NetworkPageManager;->PAGE_SOFTAP_CONFIG:I

    const/4 v0, 0x4

    iput v0, p0, Lcom/konka/systemsetting/net/NetworkPageManager;->PAGE_WIRELESS_DISPLAY:I

    iput v1, p0, Lcom/konka/systemsetting/net/NetworkPageManager;->m_iCurPage:I

    const/4 v0, -0x1

    iput v0, p0, Lcom/konka/systemsetting/net/NetworkPageManager;->m_iLastFocusPage:I

    iput-boolean v1, p0, Lcom/konka/systemsetting/net/NetworkPageManager;->isInPageOfWifiCfg:Z

    iput-object p1, p0, Lcom/konka/systemsetting/net/NetworkPageManager;->mContext:Lcom/konka/systemsetting/MainActivity;

    iput-object p2, p0, Lcom/konka/systemsetting/net/NetworkPageManager;->mViewHolder:Lcom/konka/systemsetting/ViewHolder;

    iput p3, p0, Lcom/konka/systemsetting/net/NetworkPageManager;->m_iCurPage:I

    iget v0, p0, Lcom/konka/systemsetting/net/NetworkPageManager;->m_iCurPage:I

    invoke-virtual {p0, v0}, Lcom/konka/systemsetting/net/NetworkPageManager;->showPage(I)V

    invoke-direct {p0}, Lcom/konka/systemsetting/net/NetworkPageManager;->SetOnFocusChangeListener()V

    return-void
.end method

.method private SetOnFocusChangeListener()V
    .locals 2

    new-instance v0, Lcom/konka/systemsetting/net/NetworkPageManager$1;

    invoke-direct {v0, p0}, Lcom/konka/systemsetting/net/NetworkPageManager$1;-><init>(Lcom/konka/systemsetting/net/NetworkPageManager;)V

    iget-object v1, p0, Lcom/konka/systemsetting/net/NetworkPageManager;->mViewHolder:Lcom/konka/systemsetting/ViewHolder;

    iget-object v1, v1, Lcom/konka/systemsetting/ViewHolder;->linearlayout_tabWiredSettings:Landroid/widget/LinearLayout;

    invoke-virtual {v1, v0}, Landroid/widget/LinearLayout;->setOnFocusChangeListener(Landroid/view/View$OnFocusChangeListener;)V

    iget-object v1, p0, Lcom/konka/systemsetting/net/NetworkPageManager;->mViewHolder:Lcom/konka/systemsetting/ViewHolder;

    iget-object v1, v1, Lcom/konka/systemsetting/ViewHolder;->linearlayout_tabWirelessSettings:Landroid/widget/LinearLayout;

    invoke-virtual {v1, v0}, Landroid/widget/LinearLayout;->setOnFocusChangeListener(Landroid/view/View$OnFocusChangeListener;)V

    iget-object v1, p0, Lcom/konka/systemsetting/net/NetworkPageManager;->mViewHolder:Lcom/konka/systemsetting/ViewHolder;

    iget-object v1, v1, Lcom/konka/systemsetting/ViewHolder;->linearlayout_tabPPoeSettings:Landroid/widget/LinearLayout;

    invoke-virtual {v1, v0}, Landroid/widget/LinearLayout;->setOnFocusChangeListener(Landroid/view/View$OnFocusChangeListener;)V

    iget-object v1, p0, Lcom/konka/systemsetting/net/NetworkPageManager;->mViewHolder:Lcom/konka/systemsetting/ViewHolder;

    iget-object v1, v1, Lcom/konka/systemsetting/ViewHolder;->linearLayout_tabSoftApSettings:Landroid/widget/LinearLayout;

    invoke-virtual {v1, v0}, Landroid/widget/LinearLayout;->setOnFocusChangeListener(Landroid/view/View$OnFocusChangeListener;)V

    iget-object v1, p0, Lcom/konka/systemsetting/net/NetworkPageManager;->mViewHolder:Lcom/konka/systemsetting/ViewHolder;

    iget-object v1, v1, Lcom/konka/systemsetting/ViewHolder;->linearLayout_tabWirelessDisplay:Landroid/widget/LinearLayout;

    invoke-virtual {v1, v0}, Landroid/widget/LinearLayout;->setOnFocusChangeListener(Landroid/view/View$OnFocusChangeListener;)V

    return-void
.end method

.method static synthetic access$0(Lcom/konka/systemsetting/net/NetworkPageManager;)I
    .locals 1

    iget v0, p0, Lcom/konka/systemsetting/net/NetworkPageManager;->m_iCurPage:I

    return v0
.end method

.method static synthetic access$1(Lcom/konka/systemsetting/net/NetworkPageManager;I)V
    .locals 0

    iput p1, p0, Lcom/konka/systemsetting/net/NetworkPageManager;->m_iLastFocusPage:I

    return-void
.end method

.method static synthetic access$2(Lcom/konka/systemsetting/net/NetworkPageManager;I)V
    .locals 0

    iput p1, p0, Lcom/konka/systemsetting/net/NetworkPageManager;->m_iCurPage:I

    return-void
.end method

.method static synthetic access$3(Lcom/konka/systemsetting/net/NetworkPageManager;)I
    .locals 1

    iget v0, p0, Lcom/konka/systemsetting/net/NetworkPageManager;->m_iLastFocusPage:I

    return v0
.end method

.method static synthetic access$4(Lcom/konka/systemsetting/net/NetworkPageManager;II)V
    .locals 0

    invoke-direct {p0, p1, p2}, Lcom/konka/systemsetting/net/NetworkPageManager;->changeItemStatus(II)V

    return-void
.end method

.method private changeItemStatus(II)V
    .locals 6
    .param p1    # I
    .param p2    # I

    const v5, -0x87e2

    const/4 v4, 0x0

    const/4 v3, -0x1

    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "change item statusid="

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, " iState="

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v1}, Lcom/konka/debuginfo/logPrint;->Debug(Ljava/lang/String;)V

    packed-switch p1, :pswitch_data_0

    const-string v1, "The item id is wrong!"

    invoke-static {v1}, Lcom/konka/debuginfo/logPrint;->Warning(Ljava/lang/String;)V

    :goto_0
    return-void

    :pswitch_0
    iget-object v1, p0, Lcom/konka/systemsetting/net/NetworkPageManager;->mViewHolder:Lcom/konka/systemsetting/ViewHolder;

    iget-object v1, v1, Lcom/konka/systemsetting/ViewHolder;->linearlayout_tabWiredSettings:Landroid/widget/LinearLayout;

    invoke-virtual {v1, v4}, Landroid/widget/LinearLayout;->getChildAt(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    packed-switch p2, :pswitch_data_1

    const-string v1, "The item state is wrong!"

    invoke-static {v1}, Lcom/konka/debuginfo/logPrint;->Warning(Ljava/lang/String;)V

    goto :goto_0

    :pswitch_1
    invoke-virtual {v0, v3}, Landroid/widget/TextView;->setTextColor(I)V

    goto :goto_0

    :pswitch_2
    invoke-virtual {v0, v5}, Landroid/widget/TextView;->setTextColor(I)V

    goto :goto_0

    :pswitch_3
    invoke-virtual {v0, v3}, Landroid/widget/TextView;->setTextColor(I)V

    goto :goto_0

    :pswitch_4
    iget-object v1, p0, Lcom/konka/systemsetting/net/NetworkPageManager;->mViewHolder:Lcom/konka/systemsetting/ViewHolder;

    iget-object v1, v1, Lcom/konka/systemsetting/ViewHolder;->linearlayout_tabWirelessSettings:Landroid/widget/LinearLayout;

    invoke-virtual {v1, v4}, Landroid/widget/LinearLayout;->getChildAt(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    packed-switch p2, :pswitch_data_2

    const-string v1, "The item state is wrong!"

    invoke-static {v1}, Lcom/konka/debuginfo/logPrint;->Warning(Ljava/lang/String;)V

    goto :goto_0

    :pswitch_5
    iput-boolean v4, p0, Lcom/konka/systemsetting/net/NetworkPageManager;->isInPageOfWifiCfg:Z

    invoke-virtual {v0, v3}, Landroid/widget/TextView;->setTextColor(I)V

    goto :goto_0

    :pswitch_6
    const/4 v1, 0x1

    iput-boolean v1, p0, Lcom/konka/systemsetting/net/NetworkPageManager;->isInPageOfWifiCfg:Z

    invoke-virtual {v0, v5}, Landroid/widget/TextView;->setTextColor(I)V

    goto :goto_0

    :pswitch_7
    iput-boolean v4, p0, Lcom/konka/systemsetting/net/NetworkPageManager;->isInPageOfWifiCfg:Z

    invoke-virtual {v0, v3}, Landroid/widget/TextView;->setTextColor(I)V

    goto :goto_0

    :pswitch_8
    iget-object v1, p0, Lcom/konka/systemsetting/net/NetworkPageManager;->mViewHolder:Lcom/konka/systemsetting/ViewHolder;

    iget-object v1, v1, Lcom/konka/systemsetting/ViewHolder;->linearlayout_tabPPoeSettings:Landroid/widget/LinearLayout;

    invoke-virtual {v1, v4}, Landroid/widget/LinearLayout;->getChildAt(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    packed-switch p2, :pswitch_data_3

    const-string v1, "The item state is wrong!"

    invoke-static {v1}, Lcom/konka/debuginfo/logPrint;->Warning(Ljava/lang/String;)V

    goto :goto_0

    :pswitch_9
    invoke-virtual {v0, v3}, Landroid/widget/TextView;->setTextColor(I)V

    goto :goto_0

    :pswitch_a
    invoke-virtual {v0, v5}, Landroid/widget/TextView;->setTextColor(I)V

    goto :goto_0

    :pswitch_b
    invoke-virtual {v0, v3}, Landroid/widget/TextView;->setTextColor(I)V

    goto :goto_0

    :pswitch_c
    iget-object v1, p0, Lcom/konka/systemsetting/net/NetworkPageManager;->mViewHolder:Lcom/konka/systemsetting/ViewHolder;

    iget-object v1, v1, Lcom/konka/systemsetting/ViewHolder;->linearLayout_tabSoftApSettings:Landroid/widget/LinearLayout;

    invoke-virtual {v1, v4}, Landroid/widget/LinearLayout;->getChildAt(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    packed-switch p2, :pswitch_data_4

    const-string v1, "The item state is wrong!"

    invoke-static {v1}, Lcom/konka/debuginfo/logPrint;->Warning(Ljava/lang/String;)V

    goto :goto_0

    :pswitch_d
    invoke-virtual {v0, v3}, Landroid/widget/TextView;->setTextColor(I)V

    goto :goto_0

    :pswitch_e
    invoke-virtual {v0, v5}, Landroid/widget/TextView;->setTextColor(I)V

    goto :goto_0

    :pswitch_f
    invoke-virtual {v0, v3}, Landroid/widget/TextView;->setTextColor(I)V

    goto/16 :goto_0

    :pswitch_data_0
    .packed-switch 0x0
        :pswitch_0
        :pswitch_4
        :pswitch_8
        :pswitch_c
    .end packed-switch

    :pswitch_data_1
    .packed-switch 0x0
        :pswitch_1
        :pswitch_2
        :pswitch_3
    .end packed-switch

    :pswitch_data_2
    .packed-switch 0x0
        :pswitch_5
        :pswitch_6
        :pswitch_7
    .end packed-switch

    :pswitch_data_3
    .packed-switch 0x0
        :pswitch_9
        :pswitch_a
        :pswitch_b
    .end packed-switch

    :pswitch_data_4
    .packed-switch 0x0
        :pswitch_d
        :pswitch_e
        :pswitch_f
    .end packed-switch
.end method


# virtual methods
.method public isLastFocusPage(I)Z
    .locals 1
    .param p1    # I

    const/4 v0, 0x1

    if-ne p1, v0, :cond_0

    iget-boolean v0, p0, Lcom/konka/systemsetting/net/NetworkPageManager;->isInPageOfWifiCfg:Z

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public onDestroy()V
    .locals 1

    iget-object v0, p0, Lcom/konka/systemsetting/net/NetworkPageManager;->m_WifiConfig:Lcom/konka/systemsetting/net/NetworkWifiConfig;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/konka/systemsetting/net/NetworkPageManager;->m_WifiConfig:Lcom/konka/systemsetting/net/NetworkWifiConfig;

    invoke-virtual {v0}, Lcom/konka/systemsetting/net/NetworkWifiConfig;->onDestroy()V

    :cond_0
    iget-object v0, p0, Lcom/konka/systemsetting/net/NetworkPageManager;->m_SoftApConfig:Lcom/konka/systemsetting/net/NetworkSoftApConfig;

    if-eqz v0, :cond_1

    iget-object v0, p0, Lcom/konka/systemsetting/net/NetworkPageManager;->m_SoftApConfig:Lcom/konka/systemsetting/net/NetworkSoftApConfig;

    invoke-virtual {v0}, Lcom/konka/systemsetting/net/NetworkSoftApConfig;->onDestroy()V

    :cond_1
    return-void
.end method

.method public resetItemStatus()V
    .locals 2

    const/4 v1, 0x2

    iget v0, p0, Lcom/konka/systemsetting/net/NetworkPageManager;->m_iCurPage:I

    invoke-direct {p0, v0, v1}, Lcom/konka/systemsetting/net/NetworkPageManager;->changeItemStatus(II)V

    iget v0, p0, Lcom/konka/systemsetting/net/NetworkPageManager;->m_iLastFocusPage:I

    invoke-direct {p0, v0, v1}, Lcom/konka/systemsetting/net/NetworkPageManager;->changeItemStatus(II)V

    return-void
.end method

.method protected setLastFocus(Ljava/lang/Integer;Ljava/lang/Integer;)V
    .locals 2
    .param p1    # Ljava/lang/Integer;
    .param p2    # Ljava/lang/Integer;

    invoke-virtual {p1}, Ljava/lang/Integer;->intValue()I

    move-result v0

    invoke-virtual {p2}, Ljava/lang/Integer;->intValue()I

    move-result v1

    invoke-direct {p0, v0, v1}, Lcom/konka/systemsetting/net/NetworkPageManager;->changeItemStatus(II)V

    return-void
.end method

.method public showPage(I)V
    .locals 3
    .param p1    # I

    iget-object v1, p0, Lcom/konka/systemsetting/net/NetworkPageManager;->mViewHolder:Lcom/konka/systemsetting/ViewHolder;

    iget-object v1, v1, Lcom/konka/systemsetting/ViewHolder;->viewflipper_network:Landroid/widget/ViewFlipper;

    invoke-virtual {v1, p1}, Landroid/widget/ViewFlipper;->setDisplayedChild(I)V

    packed-switch p1, :pswitch_data_0

    :cond_0
    :goto_0
    return-void

    :pswitch_0
    iget-object v1, p0, Lcom/konka/systemsetting/net/NetworkPageManager;->m_WifiConfig:Lcom/konka/systemsetting/net/NetworkWifiConfig;

    if-eqz v1, :cond_1

    iget-object v1, p0, Lcom/konka/systemsetting/net/NetworkPageManager;->m_WifiConfig:Lcom/konka/systemsetting/net/NetworkWifiConfig;

    invoke-virtual {v1}, Lcom/konka/systemsetting/net/NetworkWifiConfig;->onDestroy()V

    :cond_1
    iget-object v1, p0, Lcom/konka/systemsetting/net/NetworkPageManager;->m_WiredConfig:Lcom/konka/systemsetting/net/NetworkWireConfig;

    if-nez v1, :cond_2

    new-instance v1, Lcom/konka/systemsetting/net/NetworkWireConfig;

    iget-object v2, p0, Lcom/konka/systemsetting/net/NetworkPageManager;->mContext:Lcom/konka/systemsetting/MainActivity;

    invoke-direct {v1, v2}, Lcom/konka/systemsetting/net/NetworkWireConfig;-><init>(Lcom/konka/systemsetting/MainActivity;)V

    iput-object v1, p0, Lcom/konka/systemsetting/net/NetworkPageManager;->m_WiredConfig:Lcom/konka/systemsetting/net/NetworkWireConfig;

    goto :goto_0

    :cond_2
    iget-object v1, p0, Lcom/konka/systemsetting/net/NetworkPageManager;->m_WiredConfig:Lcom/konka/systemsetting/net/NetworkWireConfig;

    invoke-virtual {v1}, Lcom/konka/systemsetting/net/NetworkWireConfig;->refreshConfiguration()V

    goto :goto_0

    :pswitch_1
    iget-object v1, p0, Lcom/konka/systemsetting/net/NetworkPageManager;->m_WifiConfig:Lcom/konka/systemsetting/net/NetworkWifiConfig;

    if-nez v1, :cond_3

    new-instance v1, Lcom/konka/systemsetting/net/NetworkWifiConfig;

    iget-object v2, p0, Lcom/konka/systemsetting/net/NetworkPageManager;->mContext:Lcom/konka/systemsetting/MainActivity;

    invoke-direct {v1, v2}, Lcom/konka/systemsetting/net/NetworkWifiConfig;-><init>(Lcom/konka/systemsetting/MainActivity;)V

    iput-object v1, p0, Lcom/konka/systemsetting/net/NetworkPageManager;->m_WifiConfig:Lcom/konka/systemsetting/net/NetworkWifiConfig;

    goto :goto_0

    :cond_3
    iget-object v1, p0, Lcom/konka/systemsetting/net/NetworkPageManager;->m_WifiConfig:Lcom/konka/systemsetting/net/NetworkWifiConfig;

    invoke-virtual {v1}, Lcom/konka/systemsetting/net/NetworkWifiConfig;->onResume()V

    goto :goto_0

    :pswitch_2
    iget-object v1, p0, Lcom/konka/systemsetting/net/NetworkPageManager;->m_WifiConfig:Lcom/konka/systemsetting/net/NetworkWifiConfig;

    if-eqz v1, :cond_4

    iget-object v1, p0, Lcom/konka/systemsetting/net/NetworkPageManager;->m_WifiConfig:Lcom/konka/systemsetting/net/NetworkWifiConfig;

    invoke-virtual {v1}, Lcom/konka/systemsetting/net/NetworkWifiConfig;->onDestroy()V

    :cond_4
    iget-object v1, p0, Lcom/konka/systemsetting/net/NetworkPageManager;->m_PppoeConfig:Lcom/konka/systemsetting/net/NetworkPPPoeConfig;

    if-nez v1, :cond_0

    new-instance v1, Lcom/konka/systemsetting/net/NetworkPPPoeConfig;

    iget-object v2, p0, Lcom/konka/systemsetting/net/NetworkPageManager;->mContext:Lcom/konka/systemsetting/MainActivity;

    invoke-direct {v1, v2}, Lcom/konka/systemsetting/net/NetworkPPPoeConfig;-><init>(Lcom/konka/systemsetting/MainActivity;)V

    iput-object v1, p0, Lcom/konka/systemsetting/net/NetworkPageManager;->m_PppoeConfig:Lcom/konka/systemsetting/net/NetworkPPPoeConfig;

    goto :goto_0

    :pswitch_3
    iget-object v1, p0, Lcom/konka/systemsetting/net/NetworkPageManager;->m_WifiConfig:Lcom/konka/systemsetting/net/NetworkWifiConfig;

    if-eqz v1, :cond_5

    iget-object v1, p0, Lcom/konka/systemsetting/net/NetworkPageManager;->m_WifiConfig:Lcom/konka/systemsetting/net/NetworkWifiConfig;

    invoke-virtual {v1}, Lcom/konka/systemsetting/net/NetworkWifiConfig;->onDestroy()V

    :cond_5
    iget-object v1, p0, Lcom/konka/systemsetting/net/NetworkPageManager;->m_SoftApConfig:Lcom/konka/systemsetting/net/NetworkSoftApConfig;

    if-nez v1, :cond_0

    new-instance v1, Lcom/konka/systemsetting/net/NetworkSoftApConfig;

    iget-object v2, p0, Lcom/konka/systemsetting/net/NetworkPageManager;->mContext:Lcom/konka/systemsetting/MainActivity;

    invoke-direct {v1, v2, p0}, Lcom/konka/systemsetting/net/NetworkSoftApConfig;-><init>(Lcom/konka/systemsetting/MainActivity;Lcom/konka/systemsetting/net/NetworkPageManager;)V

    iput-object v1, p0, Lcom/konka/systemsetting/net/NetworkPageManager;->m_SoftApConfig:Lcom/konka/systemsetting/net/NetworkSoftApConfig;

    goto :goto_0

    :pswitch_4
    new-instance v0, Landroid/content/Intent;

    iget-object v1, p0, Lcom/konka/systemsetting/net/NetworkPageManager;->mContext:Lcom/konka/systemsetting/MainActivity;

    const-class v2, Lcom/konka/systemsetting/net/WifiDisplaySettingsActivity;

    invoke-direct {v0, v1, v2}, Landroid/content/Intent;-><init>(Landroid/content/Context;Ljava/lang/Class;)V

    iget-object v1, p0, Lcom/konka/systemsetting/net/NetworkPageManager;->mContext:Lcom/konka/systemsetting/MainActivity;

    invoke-virtual {v1, v0}, Lcom/konka/systemsetting/MainActivity;->startActivity(Landroid/content/Intent;)V

    goto :goto_0

    nop

    :pswitch_data_0
    .packed-switch 0x0
        :pswitch_0
        :pswitch_1
        :pswitch_2
        :pswitch_3
        :pswitch_4
    .end packed-switch
.end method
