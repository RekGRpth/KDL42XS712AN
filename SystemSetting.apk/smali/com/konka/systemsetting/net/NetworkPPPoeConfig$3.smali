.class Lcom/konka/systemsetting/net/NetworkPPPoeConfig$3;
.super Ljava/lang/Object;
.source "NetworkPPPoeConfig.java"

# interfaces
.implements Landroid/view/View$OnFocusChangeListener;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/konka/systemsetting/net/NetworkPPPoeConfig;->setOnFocusChangeListener()V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/konka/systemsetting/net/NetworkPPPoeConfig;


# direct methods
.method constructor <init>(Lcom/konka/systemsetting/net/NetworkPPPoeConfig;)V
    .locals 0

    iput-object p1, p0, Lcom/konka/systemsetting/net/NetworkPPPoeConfig$3;->this$0:Lcom/konka/systemsetting/net/NetworkPPPoeConfig;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onFocusChange(Landroid/view/View;Z)V
    .locals 3
    .param p1    # Landroid/view/View;
    .param p2    # Z

    invoke-virtual {p1}, Landroid/view/View;->getId()I

    move-result v0

    if-eqz p2, :cond_0

    packed-switch v0, :pswitch_data_0

    :goto_0
    return-void

    :pswitch_0
    iget-object v1, p0, Lcom/konka/systemsetting/net/NetworkPPPoeConfig$3;->this$0:Lcom/konka/systemsetting/net/NetworkPPPoeConfig;

    # getter for: Lcom/konka/systemsetting/net/NetworkPPPoeConfig;->mEditName:Landroid/widget/EditText;
    invoke-static {v1}, Lcom/konka/systemsetting/net/NetworkPPPoeConfig;->access$1(Lcom/konka/systemsetting/net/NetworkPPPoeConfig;)Landroid/widget/EditText;

    move-result-object v1

    invoke-virtual {v1}, Landroid/widget/EditText;->requestFocus()Z

    goto :goto_0

    :pswitch_1
    iget-object v1, p0, Lcom/konka/systemsetting/net/NetworkPPPoeConfig$3;->this$0:Lcom/konka/systemsetting/net/NetworkPPPoeConfig;

    # getter for: Lcom/konka/systemsetting/net/NetworkPPPoeConfig;->mEditPwd:Landroid/widget/EditText;
    invoke-static {v1}, Lcom/konka/systemsetting/net/NetworkPPPoeConfig;->access$2(Lcom/konka/systemsetting/net/NetworkPPPoeConfig;)Landroid/widget/EditText;

    move-result-object v1

    invoke-virtual {v1}, Landroid/widget/EditText;->requestFocus()Z

    goto :goto_0

    :pswitch_2
    iget-object v1, p0, Lcom/konka/systemsetting/net/NetworkPPPoeConfig$3;->this$0:Lcom/konka/systemsetting/net/NetworkPPPoeConfig;

    # getter for: Lcom/konka/systemsetting/net/NetworkPPPoeConfig;->mEditName:Landroid/widget/EditText;
    invoke-static {v1}, Lcom/konka/systemsetting/net/NetworkPPPoeConfig;->access$1(Lcom/konka/systemsetting/net/NetworkPPPoeConfig;)Landroid/widget/EditText;

    move-result-object v1

    invoke-virtual {v1}, Landroid/widget/EditText;->selectAll()V

    iget-object v1, p0, Lcom/konka/systemsetting/net/NetworkPPPoeConfig$3;->this$0:Lcom/konka/systemsetting/net/NetworkPPPoeConfig;

    # getter for: Lcom/konka/systemsetting/net/NetworkPPPoeConfig;->mItemName:Landroid/widget/LinearLayout;
    invoke-static {v1}, Lcom/konka/systemsetting/net/NetworkPPPoeConfig;->access$3(Lcom/konka/systemsetting/net/NetworkPPPoeConfig;)Landroid/widget/LinearLayout;

    move-result-object v1

    const v2, 0x7f020034    # com.konka.systemsetting.R.drawable.syssettingthirditemtopfocus

    invoke-virtual {v1, v2}, Landroid/widget/LinearLayout;->setBackgroundResource(I)V

    goto :goto_0

    :pswitch_3
    iget-object v1, p0, Lcom/konka/systemsetting/net/NetworkPPPoeConfig$3;->this$0:Lcom/konka/systemsetting/net/NetworkPPPoeConfig;

    # getter for: Lcom/konka/systemsetting/net/NetworkPPPoeConfig;->mEditPwd:Landroid/widget/EditText;
    invoke-static {v1}, Lcom/konka/systemsetting/net/NetworkPPPoeConfig;->access$2(Lcom/konka/systemsetting/net/NetworkPPPoeConfig;)Landroid/widget/EditText;

    move-result-object v1

    invoke-virtual {v1}, Landroid/widget/EditText;->selectAll()V

    iget-object v1, p0, Lcom/konka/systemsetting/net/NetworkPPPoeConfig$3;->this$0:Lcom/konka/systemsetting/net/NetworkPPPoeConfig;

    # getter for: Lcom/konka/systemsetting/net/NetworkPPPoeConfig;->mItemPwd:Landroid/widget/LinearLayout;
    invoke-static {v1}, Lcom/konka/systemsetting/net/NetworkPPPoeConfig;->access$4(Lcom/konka/systemsetting/net/NetworkPPPoeConfig;)Landroid/widget/LinearLayout;

    move-result-object v1

    const v2, 0x7f02002e    # com.konka.systemsetting.R.drawable.syssettingthirditembottomfocus

    invoke-virtual {v1, v2}, Landroid/widget/LinearLayout;->setBackgroundResource(I)V

    goto :goto_0

    :pswitch_4
    const v1, 0x7f020032    # com.konka.systemsetting.R.drawable.syssettingthirditemsinglefocus

    invoke-virtual {p1, v1}, Landroid/view/View;->setBackgroundResource(I)V

    goto :goto_0

    :cond_0
    packed-switch v0, :pswitch_data_1

    :pswitch_5
    goto :goto_0

    :pswitch_6
    iget-object v1, p0, Lcom/konka/systemsetting/net/NetworkPPPoeConfig$3;->this$0:Lcom/konka/systemsetting/net/NetworkPPPoeConfig;

    # getter for: Lcom/konka/systemsetting/net/NetworkPPPoeConfig;->mEditName:Landroid/widget/EditText;
    invoke-static {v1}, Lcom/konka/systemsetting/net/NetworkPPPoeConfig;->access$1(Lcom/konka/systemsetting/net/NetworkPPPoeConfig;)Landroid/widget/EditText;

    move-result-object v1

    invoke-virtual {v1}, Landroid/widget/EditText;->clearFocus()V

    iget-object v1, p0, Lcom/konka/systemsetting/net/NetworkPPPoeConfig$3;->this$0:Lcom/konka/systemsetting/net/NetworkPPPoeConfig;

    # getter for: Lcom/konka/systemsetting/net/NetworkPPPoeConfig;->mItemName:Landroid/widget/LinearLayout;
    invoke-static {v1}, Lcom/konka/systemsetting/net/NetworkPPPoeConfig;->access$3(Lcom/konka/systemsetting/net/NetworkPPPoeConfig;)Landroid/widget/LinearLayout;

    move-result-object v1

    const v2, 0x7f020033    # com.konka.systemsetting.R.drawable.syssettingthirditemtop

    invoke-virtual {v1, v2}, Landroid/widget/LinearLayout;->setBackgroundResource(I)V

    goto :goto_0

    :pswitch_7
    iget-object v1, p0, Lcom/konka/systemsetting/net/NetworkPPPoeConfig$3;->this$0:Lcom/konka/systemsetting/net/NetworkPPPoeConfig;

    # getter for: Lcom/konka/systemsetting/net/NetworkPPPoeConfig;->mEditPwd:Landroid/widget/EditText;
    invoke-static {v1}, Lcom/konka/systemsetting/net/NetworkPPPoeConfig;->access$2(Lcom/konka/systemsetting/net/NetworkPPPoeConfig;)Landroid/widget/EditText;

    move-result-object v1

    invoke-virtual {v1}, Landroid/widget/EditText;->selectAll()V

    iget-object v1, p0, Lcom/konka/systemsetting/net/NetworkPPPoeConfig$3;->this$0:Lcom/konka/systemsetting/net/NetworkPPPoeConfig;

    # getter for: Lcom/konka/systemsetting/net/NetworkPPPoeConfig;->mItemPwd:Landroid/widget/LinearLayout;
    invoke-static {v1}, Lcom/konka/systemsetting/net/NetworkPPPoeConfig;->access$4(Lcom/konka/systemsetting/net/NetworkPPPoeConfig;)Landroid/widget/LinearLayout;

    move-result-object v1

    const v2, 0x7f02002d    # com.konka.systemsetting.R.drawable.syssettingthirditembottom

    invoke-virtual {v1, v2}, Landroid/widget/LinearLayout;->setBackgroundResource(I)V

    goto :goto_0

    :pswitch_8
    const v1, 0x7f020031    # com.konka.systemsetting.R.drawable.syssettingthirditemsingle

    invoke-virtual {p1, v1}, Landroid/view/View;->setBackgroundResource(I)V

    goto :goto_0

    :pswitch_data_0
    .packed-switch 0x7f090034
        :pswitch_0    # com.konka.systemsetting.R.id.sys_network_pppoe_username
        :pswitch_2    # com.konka.systemsetting.R.id.sys_network_pppoe_edittext_username
        :pswitch_1    # com.konka.systemsetting.R.id.sys_network_pppoe_password
        :pswitch_3    # com.konka.systemsetting.R.id.sys_network_pppoe_edittext_password
        :pswitch_4    # com.konka.systemsetting.R.id.sys_network_pppoe_autodial
        :pswitch_4    # com.konka.systemsetting.R.id.sys_network_pppoe_showpwd
        :pswitch_4    # com.konka.systemsetting.R.id.sys_network_pppoe_set
    .end packed-switch

    :pswitch_data_1
    .packed-switch 0x7f090035
        :pswitch_6    # com.konka.systemsetting.R.id.sys_network_pppoe_edittext_username
        :pswitch_5    # com.konka.systemsetting.R.id.sys_network_pppoe_password
        :pswitch_7    # com.konka.systemsetting.R.id.sys_network_pppoe_edittext_password
        :pswitch_8    # com.konka.systemsetting.R.id.sys_network_pppoe_autodial
        :pswitch_8    # com.konka.systemsetting.R.id.sys_network_pppoe_showpwd
        :pswitch_8    # com.konka.systemsetting.R.id.sys_network_pppoe_set
    .end packed-switch
.end method
