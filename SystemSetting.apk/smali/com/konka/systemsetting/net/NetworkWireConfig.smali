.class public Lcom/konka/systemsetting/net/NetworkWireConfig;
.super Ljava/lang/Object;
.source "NetworkWireConfig.java"

# interfaces
.implements Landroid/view/View$OnClickListener;
.implements Landroid/view/View$OnFocusChangeListener;
.implements Landroid/view/View$OnKeyListener;


# static fields
.field public static final TYPE_ETHERNET:I = 0x9

.field public static final TYPE_WIFI:I = 0x1


# instance fields
.field private ETH_CONN_MODE_DHCP:Ljava/lang/String;

.field private ETH_CONN_MODE_MANUAL:Ljava/lang/String;

.field etItemOnFocusChangeEvent:Landroid/view/View$OnFocusChangeListener;

.field etOnFocusChangeEvent:Landroid/view/View$OnFocusChangeListener;

.field public mBtnDetectWire:Landroid/widget/Button;

.field public mBtnSetWire:Landroid/widget/Button;

.field private mContext:Lcom/konka/systemsetting/MainActivity;

.field public mEditDnsSvr:Landroid/widget/EditText;

.field public mEditGateway:Landroid/widget/EditText;

.field public mEditIpAddr:Landroid/widget/EditText;

.field public mEditNetmask:Landroid/widget/EditText;

.field private mEthInfo:Landroid/net/ethernet/EthernetDevInfo;

.field private mEthManager:Landroid/net/ethernet/EthernetManager;

.field private final mEthStateReceiver:Landroid/content/BroadcastReceiver;

.field public mItemDnsSvr:Landroid/widget/LinearLayout;

.field public mItemGateway:Landroid/widget/LinearLayout;

.field public mItemIpAddr:Landroid/widget/LinearLayout;

.field public mItemIpMode:Landroid/widget/LinearLayout;

.field public mItemNetmask:Landroid/widget/LinearLayout;

.field public mTxtIpMode:Landroid/widget/TextView;


# direct methods
.method public constructor <init>(Lcom/konka/systemsetting/MainActivity;)V
    .locals 1
    .param p1    # Lcom/konka/systemsetting/MainActivity;

    const/4 v0, 0x0

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    iput-object v0, p0, Lcom/konka/systemsetting/net/NetworkWireConfig;->mContext:Lcom/konka/systemsetting/MainActivity;

    iput-object v0, p0, Lcom/konka/systemsetting/net/NetworkWireConfig;->mItemIpMode:Landroid/widget/LinearLayout;

    iput-object v0, p0, Lcom/konka/systemsetting/net/NetworkWireConfig;->mItemIpAddr:Landroid/widget/LinearLayout;

    iput-object v0, p0, Lcom/konka/systemsetting/net/NetworkWireConfig;->mItemNetmask:Landroid/widget/LinearLayout;

    iput-object v0, p0, Lcom/konka/systemsetting/net/NetworkWireConfig;->mItemGateway:Landroid/widget/LinearLayout;

    iput-object v0, p0, Lcom/konka/systemsetting/net/NetworkWireConfig;->mItemDnsSvr:Landroid/widget/LinearLayout;

    iput-object v0, p0, Lcom/konka/systemsetting/net/NetworkWireConfig;->mBtnSetWire:Landroid/widget/Button;

    iput-object v0, p0, Lcom/konka/systemsetting/net/NetworkWireConfig;->mBtnDetectWire:Landroid/widget/Button;

    iput-object v0, p0, Lcom/konka/systemsetting/net/NetworkWireConfig;->mTxtIpMode:Landroid/widget/TextView;

    iput-object v0, p0, Lcom/konka/systemsetting/net/NetworkWireConfig;->mEditIpAddr:Landroid/widget/EditText;

    iput-object v0, p0, Lcom/konka/systemsetting/net/NetworkWireConfig;->mEditNetmask:Landroid/widget/EditText;

    iput-object v0, p0, Lcom/konka/systemsetting/net/NetworkWireConfig;->mEditGateway:Landroid/widget/EditText;

    iput-object v0, p0, Lcom/konka/systemsetting/net/NetworkWireConfig;->mEditDnsSvr:Landroid/widget/EditText;

    const-string v0, "dhcp"

    iput-object v0, p0, Lcom/konka/systemsetting/net/NetworkWireConfig;->ETH_CONN_MODE_DHCP:Ljava/lang/String;

    const-string v0, "manual"

    iput-object v0, p0, Lcom/konka/systemsetting/net/NetworkWireConfig;->ETH_CONN_MODE_MANUAL:Ljava/lang/String;

    new-instance v0, Lcom/konka/systemsetting/net/NetworkWireConfig$1;

    invoke-direct {v0, p0}, Lcom/konka/systemsetting/net/NetworkWireConfig$1;-><init>(Lcom/konka/systemsetting/net/NetworkWireConfig;)V

    iput-object v0, p0, Lcom/konka/systemsetting/net/NetworkWireConfig;->mEthStateReceiver:Landroid/content/BroadcastReceiver;

    new-instance v0, Lcom/konka/systemsetting/net/NetworkWireConfig$2;

    invoke-direct {v0, p0}, Lcom/konka/systemsetting/net/NetworkWireConfig$2;-><init>(Lcom/konka/systemsetting/net/NetworkWireConfig;)V

    iput-object v0, p0, Lcom/konka/systemsetting/net/NetworkWireConfig;->etOnFocusChangeEvent:Landroid/view/View$OnFocusChangeListener;

    new-instance v0, Lcom/konka/systemsetting/net/NetworkWireConfig$3;

    invoke-direct {v0, p0}, Lcom/konka/systemsetting/net/NetworkWireConfig$3;-><init>(Lcom/konka/systemsetting/net/NetworkWireConfig;)V

    iput-object v0, p0, Lcom/konka/systemsetting/net/NetworkWireConfig;->etItemOnFocusChangeEvent:Landroid/view/View$OnFocusChangeListener;

    iput-object p1, p0, Lcom/konka/systemsetting/net/NetworkWireConfig;->mContext:Lcom/konka/systemsetting/MainActivity;

    invoke-static {}, Landroid/net/ethernet/EthernetManager;->getInstance()Landroid/net/ethernet/EthernetManager;

    move-result-object v0

    iput-object v0, p0, Lcom/konka/systemsetting/net/NetworkWireConfig;->mEthManager:Landroid/net/ethernet/EthernetManager;

    invoke-direct {p0}, Lcom/konka/systemsetting/net/NetworkWireConfig;->initUIComponents()V

    invoke-direct {p0}, Lcom/konka/systemsetting/net/NetworkWireConfig;->setOnKeyListener()V

    invoke-direct {p0}, Lcom/konka/systemsetting/net/NetworkWireConfig;->setOnClickListener()V

    invoke-direct {p0}, Lcom/konka/systemsetting/net/NetworkWireConfig;->setOnFocusChangeListner()V

    invoke-virtual {p0}, Lcom/konka/systemsetting/net/NetworkWireConfig;->refreshConfiguration()V

    return-void
.end method

.method static synthetic access$0(Lcom/konka/systemsetting/net/NetworkWireConfig;II)V
    .locals 0

    invoke-direct {p0, p1, p2}, Lcom/konka/systemsetting/net/NetworkWireConfig;->handleEthStateChanged(II)V

    return-void
.end method

.method static synthetic access$1(Lcom/konka/systemsetting/net/NetworkWireConfig;Landroid/net/NetworkInfo;)V
    .locals 0

    invoke-direct {p0, p1}, Lcom/konka/systemsetting/net/NetworkWireConfig;->handleNetworkStateChanged(Landroid/net/NetworkInfo;)V

    return-void
.end method

.method static synthetic access$2(Lcom/konka/systemsetting/net/NetworkWireConfig;I)V
    .locals 0

    invoke-direct {p0, p1}, Lcom/konka/systemsetting/net/NetworkWireConfig;->setItemBgByFocusEdit(I)V

    return-void
.end method

.method static synthetic access$3(Lcom/konka/systemsetting/net/NetworkWireConfig;I)V
    .locals 0

    invoke-direct {p0, p1}, Lcom/konka/systemsetting/net/NetworkWireConfig;->setItemBgByLoseFocusEdit(I)V

    return-void
.end method

.method private changeIpMode()V
    .locals 0

    invoke-virtual {p0}, Lcom/konka/systemsetting/net/NetworkWireConfig;->changeIPModeUI()V

    invoke-direct {p0}, Lcom/konka/systemsetting/net/NetworkWireConfig;->getConfigFromManager()V

    invoke-virtual {p0}, Lcom/konka/systemsetting/net/NetworkWireConfig;->updateWireStateToUI()V

    return-void
.end method

.method private checkEthConfiguration()Z
    .locals 4

    const/4 v1, 0x1

    sget-boolean v2, Lcom/konka/systemsetting/common/Content$ethernet;->isAutoSet:Z

    if-eqz v2, :cond_1

    :cond_0
    :goto_0
    return v1

    :cond_1
    const-string v0, ""

    sget-object v2, Lcom/konka/systemsetting/common/Content$ethernet;->ipaddress:Ljava/lang/CharSequence;

    invoke-interface {v2}, Ljava/lang/CharSequence;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/String;->trim()Ljava/lang/String;

    move-result-object v2

    invoke-static {v2}, Lcom/konka/utilities/Tools;->matchIP(Ljava/lang/String;)Z

    move-result v2

    if-nez v2, :cond_2

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-static {v0}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v3

    invoke-direct {v2, v3}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    const v3, 0x7f060063    # com.konka.systemsetting.R.string.check_ip_failure

    invoke-direct {p0, v3}, Lcom/konka/systemsetting/net/NetworkWireConfig;->getText(I)Ljava/lang/CharSequence;

    move-result-object v3

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, "\n"

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    :cond_2
    sget-object v2, Lcom/konka/systemsetting/common/Content$ethernet;->netmask:Ljava/lang/CharSequence;

    invoke-interface {v2}, Ljava/lang/CharSequence;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/String;->trim()Ljava/lang/String;

    move-result-object v2

    invoke-static {v2}, Lcom/konka/utilities/Tools;->matchIP(Ljava/lang/String;)Z

    move-result v2

    if-nez v2, :cond_3

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-static {v0}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v3

    invoke-direct {v2, v3}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    const v3, 0x7f060065    # com.konka.systemsetting.R.string.check_netmask_failure

    invoke-direct {p0, v3}, Lcom/konka/systemsetting/net/NetworkWireConfig;->getText(I)Ljava/lang/CharSequence;

    move-result-object v3

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, "\n"

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    :cond_3
    sget-object v2, Lcom/konka/systemsetting/common/Content$ethernet;->gateway:Ljava/lang/CharSequence;

    invoke-interface {v2}, Ljava/lang/CharSequence;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/String;->trim()Ljava/lang/String;

    move-result-object v2

    invoke-static {v2}, Lcom/konka/utilities/Tools;->matchIP(Ljava/lang/String;)Z

    move-result v2

    if-nez v2, :cond_4

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-static {v0}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v3

    invoke-direct {v2, v3}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    const v3, 0x7f060064    # com.konka.systemsetting.R.string.check_default_failure

    invoke-direct {p0, v3}, Lcom/konka/systemsetting/net/NetworkWireConfig;->getText(I)Ljava/lang/CharSequence;

    move-result-object v3

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, "\n"

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    :cond_4
    sget-object v2, Lcom/konka/systemsetting/common/Content$ethernet;->firstdns:Ljava/lang/CharSequence;

    invoke-interface {v2}, Ljava/lang/CharSequence;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/String;->trim()Ljava/lang/String;

    move-result-object v2

    invoke-static {v2}, Lcom/konka/utilities/Tools;->matchIP(Ljava/lang/String;)Z

    move-result v2

    if-nez v2, :cond_5

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-static {v0}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v3

    invoke-direct {v2, v3}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    const v3, 0x7f060066    # com.konka.systemsetting.R.string.check_dns_failure

    invoke-direct {p0, v3}, Lcom/konka/systemsetting/net/NetworkWireConfig;->getText(I)Ljava/lang/CharSequence;

    move-result-object v3

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    :cond_5
    const-string v2, ""

    if-eq v0, v2, :cond_0

    invoke-direct {p0, v0}, Lcom/konka/systemsetting/net/NetworkWireConfig;->toastText(Ljava/lang/String;)V

    const/4 v1, 0x0

    goto/16 :goto_0
.end method

.method private debug(Ljava/lang/String;)V
    .locals 0
    .param p1    # Ljava/lang/String;

    invoke-static {p1}, Lcom/konka/debuginfo/logPrint;->Debug(Ljava/lang/String;)V

    return-void
.end method

.method private detectNetworkState()Z
    .locals 5

    const/4 v2, 0x1

    iget-object v3, p0, Lcom/konka/systemsetting/net/NetworkWireConfig;->mContext:Lcom/konka/systemsetting/MainActivity;

    const-string v4, "connectivity"

    invoke-virtual {v3, v4}, Lcom/konka/systemsetting/MainActivity;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/net/ConnectivityManager;

    const/16 v3, 0x9

    invoke-virtual {v0, v3}, Landroid/net/ConnectivityManager;->getNetworkInfo(I)Landroid/net/NetworkInfo;

    move-result-object v3

    invoke-virtual {v3}, Landroid/net/NetworkInfo;->getState()Landroid/net/NetworkInfo$State;

    move-result-object v1

    invoke-virtual {v1}, Landroid/net/NetworkInfo$State;->ordinal()I

    move-result v3

    sget-object v4, Landroid/net/NetworkInfo$State;->CONNECTING:Landroid/net/NetworkInfo$State;

    invoke-virtual {v4}, Landroid/net/NetworkInfo$State;->ordinal()I

    move-result v4

    if-ne v3, v4, :cond_0

    const v3, 0x7f06010e    # com.konka.systemsetting.R.string.eth_connecting

    invoke-direct {p0, v3}, Lcom/konka/systemsetting/net/NetworkWireConfig;->toastText(I)V

    :goto_0
    return v2

    :cond_0
    invoke-virtual {v1}, Landroid/net/NetworkInfo$State;->ordinal()I

    move-result v3

    sget-object v4, Landroid/net/NetworkInfo$State;->CONNECTED:Landroid/net/NetworkInfo$State;

    invoke-virtual {v4}, Landroid/net/NetworkInfo$State;->ordinal()I

    move-result v4

    if-ne v3, v4, :cond_1

    const v3, 0x7f06006c    # com.konka.systemsetting.R.string.eth_connected

    invoke-direct {p0, v3}, Lcom/konka/systemsetting/net/NetworkWireConfig;->toastText(I)V

    goto :goto_0

    :cond_1
    invoke-virtual {v1}, Landroid/net/NetworkInfo$State;->ordinal()I

    move-result v3

    sget-object v4, Landroid/net/NetworkInfo$State;->SUSPENDED:Landroid/net/NetworkInfo$State;

    invoke-virtual {v4}, Landroid/net/NetworkInfo$State;->ordinal()I

    move-result v4

    if-ne v3, v4, :cond_2

    const v3, 0x7f06010f    # com.konka.systemsetting.R.string.eth_suspended

    invoke-direct {p0, v3}, Lcom/konka/systemsetting/net/NetworkWireConfig;->toastText(I)V

    goto :goto_0

    :cond_2
    invoke-virtual {v1}, Landroid/net/NetworkInfo$State;->ordinal()I

    move-result v3

    sget-object v4, Landroid/net/NetworkInfo$State;->DISCONNECTING:Landroid/net/NetworkInfo$State;

    invoke-virtual {v4}, Landroid/net/NetworkInfo$State;->ordinal()I

    move-result v4

    if-ne v3, v4, :cond_3

    const v3, 0x7f060110    # com.konka.systemsetting.R.string.eth_disconnecting

    invoke-direct {p0, v3}, Lcom/konka/systemsetting/net/NetworkWireConfig;->toastText(I)V

    goto :goto_0

    :cond_3
    invoke-virtual {v1}, Landroid/net/NetworkInfo$State;->ordinal()I

    move-result v3

    sget-object v4, Landroid/net/NetworkInfo$State;->DISCONNECTED:Landroid/net/NetworkInfo$State;

    invoke-virtual {v4}, Landroid/net/NetworkInfo$State;->ordinal()I

    move-result v4

    if-ne v3, v4, :cond_4

    const v3, 0x7f0600b3    # com.konka.systemsetting.R.string.eth_disconnected

    invoke-direct {p0, v3}, Lcom/konka/systemsetting/net/NetworkWireConfig;->toastText(I)V

    goto :goto_0

    :cond_4
    invoke-virtual {v1}, Landroid/net/NetworkInfo$State;->ordinal()I

    move-result v3

    sget-object v4, Landroid/net/NetworkInfo$State;->UNKNOWN:Landroid/net/NetworkInfo$State;

    invoke-virtual {v4}, Landroid/net/NetworkInfo$State;->ordinal()I

    move-result v4

    if-ne v3, v4, :cond_5

    const v3, 0x7f0600b4    # com.konka.systemsetting.R.string.eth_unkown

    invoke-direct {p0, v3}, Lcom/konka/systemsetting/net/NetworkWireConfig;->toastText(I)V

    goto :goto_0

    :cond_5
    const v2, 0x7f0600b6    # com.konka.systemsetting.R.string.eth_enable

    invoke-direct {p0, v2}, Lcom/konka/systemsetting/net/NetworkWireConfig;->toastText(I)V

    const/4 v2, 0x0

    goto :goto_0
.end method

.method private getConfigFromManager()V
    .locals 3

    :try_start_0
    iget-object v2, p0, Lcom/konka/systemsetting/net/NetworkWireConfig;->mEthManager:Landroid/net/ethernet/EthernetManager;

    invoke-virtual {v2}, Landroid/net/ethernet/EthernetManager;->getSavedConfig()Landroid/net/ethernet/EthernetDevInfo;

    move-result-object v1

    invoke-virtual {v1}, Landroid/net/ethernet/EthernetDevInfo;->getIpAddress()Ljava/lang/String;

    move-result-object v2

    if-eqz v2, :cond_0

    invoke-virtual {v1}, Landroid/net/ethernet/EthernetDevInfo;->getIpAddress()Ljava/lang/String;

    move-result-object v2

    sput-object v2, Lcom/konka/systemsetting/common/Content$ethernet;->ipaddress:Ljava/lang/CharSequence;

    :cond_0
    invoke-virtual {v1}, Landroid/net/ethernet/EthernetDevInfo;->getNetMask()Ljava/lang/String;

    move-result-object v2

    if-eqz v2, :cond_1

    invoke-virtual {v1}, Landroid/net/ethernet/EthernetDevInfo;->getNetMask()Ljava/lang/String;

    move-result-object v2

    sput-object v2, Lcom/konka/systemsetting/common/Content$ethernet;->netmask:Ljava/lang/CharSequence;

    :cond_1
    invoke-virtual {v1}, Landroid/net/ethernet/EthernetDevInfo;->getRouteAddr()Ljava/lang/String;

    move-result-object v2

    if-eqz v2, :cond_2

    invoke-virtual {v1}, Landroid/net/ethernet/EthernetDevInfo;->getRouteAddr()Ljava/lang/String;

    move-result-object v2

    sput-object v2, Lcom/konka/systemsetting/common/Content$ethernet;->gateway:Ljava/lang/CharSequence;

    :cond_2
    invoke-virtual {v1}, Landroid/net/ethernet/EthernetDevInfo;->getDnsAddr()Ljava/lang/String;

    move-result-object v2

    if-eqz v2, :cond_3

    invoke-virtual {v1}, Landroid/net/ethernet/EthernetDevInfo;->getDnsAddr()Ljava/lang/String;

    move-result-object v2

    sput-object v2, Lcom/konka/systemsetting/common/Content$ethernet;->firstdns:Ljava/lang/CharSequence;
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    :cond_3
    :goto_0
    return-void

    :catch_0
    move-exception v0

    invoke-virtual {v0}, Ljava/lang/Exception;->getMessage()Ljava/lang/String;

    move-result-object v2

    invoke-direct {p0, v2}, Lcom/konka/systemsetting/net/NetworkWireConfig;->toastText(Ljava/lang/String;)V

    goto :goto_0
.end method

.method private getFocusImageById(I)I
    .locals 2
    .param p1    # I

    const/high16 v0, -0x1000000

    packed-switch p1, :pswitch_data_0

    :pswitch_0
    const v0, 0x7f020032    # com.konka.systemsetting.R.drawable.syssettingthirditemsinglefocus

    const-string v1, "Cable network setting: get focus image by id error, no such id!!!"

    invoke-direct {p0, v1}, Lcom/konka/systemsetting/net/NetworkWireConfig;->debug(Ljava/lang/String;)V

    :goto_0
    return v0

    :pswitch_1
    const v0, 0x7f020032    # com.konka.systemsetting.R.drawable.syssettingthirditemsinglefocus

    goto :goto_0

    :pswitch_2
    const v0, 0x7f020034    # com.konka.systemsetting.R.drawable.syssettingthirditemtopfocus

    goto :goto_0

    :pswitch_3
    const v0, 0x7f020030    # com.konka.systemsetting.R.drawable.syssettingthirditemmidfocus

    goto :goto_0

    :pswitch_4
    const v0, 0x7f02002e    # com.konka.systemsetting.R.drawable.syssettingthirditembottomfocus

    goto :goto_0

    :pswitch_data_0
    .packed-switch 0x7f090063
        :pswitch_2    # com.konka.systemsetting.R.id.sys_network_wired_item_ipmode
        :pswitch_3    # com.konka.systemsetting.R.id.sys_network_wired_item_ipaddr
        :pswitch_0    # com.konka.systemsetting.R.id.sys_network_wired_edittext_ipaddr
        :pswitch_3    # com.konka.systemsetting.R.id.sys_network_wired_item_subnetmask
        :pswitch_0    # com.konka.systemsetting.R.id.sys_network_wired_edittext_subnetmask
        :pswitch_3    # com.konka.systemsetting.R.id.sys_network_wired_item_defaultgateway
        :pswitch_0    # com.konka.systemsetting.R.id.sys_network_wired_edittext_defaultgateway
        :pswitch_4    # com.konka.systemsetting.R.id.sys_network_wired_item_dnsserver
        :pswitch_0    # com.konka.systemsetting.R.id.sys_network_wired_edittext_dnsserver
        :pswitch_1    # com.konka.systemsetting.R.id.sys_network_wired_item_set
        :pswitch_1    # com.konka.systemsetting.R.id.sys_network_wired_item_detect
    .end packed-switch
.end method

.method private getText(I)Ljava/lang/CharSequence;
    .locals 1
    .param p1    # I

    iget-object v0, p0, Lcom/konka/systemsetting/net/NetworkWireConfig;->mContext:Lcom/konka/systemsetting/MainActivity;

    invoke-virtual {v0, p1}, Lcom/konka/systemsetting/MainActivity;->getText(I)Ljava/lang/CharSequence;

    move-result-object v0

    return-object v0
.end method

.method private getUnFocusImageById(I)I
    .locals 2
    .param p1    # I

    const/high16 v0, -0x1000000

    packed-switch p1, :pswitch_data_0

    :pswitch_0
    const-string v1, "Cable network setting: get no focus image by id error, no such id!!!"

    invoke-direct {p0, v1}, Lcom/konka/systemsetting/net/NetworkWireConfig;->debug(Ljava/lang/String;)V

    :goto_0
    return v0

    :pswitch_1
    const v0, 0x7f020031    # com.konka.systemsetting.R.drawable.syssettingthirditemsingle

    goto :goto_0

    :pswitch_2
    const v0, 0x7f020033    # com.konka.systemsetting.R.drawable.syssettingthirditemtop

    goto :goto_0

    :pswitch_3
    const v0, 0x7f02002f    # com.konka.systemsetting.R.drawable.syssettingthirditemmid

    goto :goto_0

    :pswitch_4
    const v0, 0x7f02002d    # com.konka.systemsetting.R.drawable.syssettingthirditembottom

    goto :goto_0

    nop

    :pswitch_data_0
    .packed-switch 0x7f090063
        :pswitch_2    # com.konka.systemsetting.R.id.sys_network_wired_item_ipmode
        :pswitch_3    # com.konka.systemsetting.R.id.sys_network_wired_item_ipaddr
        :pswitch_0    # com.konka.systemsetting.R.id.sys_network_wired_edittext_ipaddr
        :pswitch_3    # com.konka.systemsetting.R.id.sys_network_wired_item_subnetmask
        :pswitch_0    # com.konka.systemsetting.R.id.sys_network_wired_edittext_subnetmask
        :pswitch_3    # com.konka.systemsetting.R.id.sys_network_wired_item_defaultgateway
        :pswitch_0    # com.konka.systemsetting.R.id.sys_network_wired_edittext_defaultgateway
        :pswitch_4    # com.konka.systemsetting.R.id.sys_network_wired_item_dnsserver
        :pswitch_0    # com.konka.systemsetting.R.id.sys_network_wired_edittext_dnsserver
        :pswitch_1    # com.konka.systemsetting.R.id.sys_network_wired_item_set
        :pswitch_1    # com.konka.systemsetting.R.id.sys_network_wired_item_detect
    .end packed-switch
.end method

.method private handleEthStateChanged(II)V
    .locals 4
    .param p1    # I
    .param p2    # I

    sget-object v1, Ljava/lang/System;->out:Ljava/io/PrintStream;

    const-string v2, "==>handleEthStateChanged"

    invoke-virtual {v1, v2}, Ljava/io/PrintStream;->println(Ljava/lang/String;)V

    sget-object v1, Ljava/lang/System;->out:Ljava/io/PrintStream;

    new-instance v2, Ljava/lang/StringBuilder;

    const-string v3, "==>ethState"

    invoke-direct {v2, v3}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v2, p1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/io/PrintStream;->println(Ljava/lang/String;)V

    sget-object v1, Ljava/lang/System;->out:Ljava/io/PrintStream;

    new-instance v2, Ljava/lang/StringBuilder;

    const-string v3, "==>previousEthState"

    invoke-direct {v2, v3}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v2, p2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/io/PrintStream;->println(Ljava/lang/String;)V

    move v0, p1

    invoke-direct {p0}, Lcom/konka/systemsetting/net/NetworkWireConfig;->initCurrentNetState()V

    return-void
.end method

.method private handleNetworkStateChanged(Landroid/net/NetworkInfo;)V
    .locals 2
    .param p1    # Landroid/net/NetworkInfo;

    new-instance v0, Ljava/lang/StringBuilder;

    const-string v1, "Received network state changed to "

    invoke-direct {v0, v1}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v0, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-direct {p0, v0}, Lcom/konka/systemsetting/net/NetworkWireConfig;->debug(Ljava/lang/String;)V

    return-void
.end method

.method private initCurrentNetState()V
    .locals 0

    return-void
.end method

.method private initUIComponents()V
    .locals 7

    const v6, 0x7f090068    # com.konka.systemsetting.R.id.sys_network_wired_item_defaultgateway

    const v5, 0x7f090066    # com.konka.systemsetting.R.id.sys_network_wired_item_subnetmask

    const v4, 0x7f090063    # com.konka.systemsetting.R.id.sys_network_wired_item_ipmode

    const/4 v3, 0x1

    const v2, 0x7f09003c    # com.konka.systemsetting.R.id.linearlayout_network_tab_wired

    iget-object v0, p0, Lcom/konka/systemsetting/net/NetworkWireConfig;->mContext:Lcom/konka/systemsetting/MainActivity;

    invoke-virtual {v0}, Lcom/konka/systemsetting/MainActivity;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    const v1, 0x7f070009    # com.konka.systemsetting.R.array.str_arr_network_wire_mode

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getTextArray(I)[Ljava/lang/CharSequence;

    move-result-object v0

    sput-object v0, Lcom/konka/systemsetting/common/Content$ethernet;->IpMode:[Ljava/lang/CharSequence;

    iget-object v0, p0, Lcom/konka/systemsetting/net/NetworkWireConfig;->mContext:Lcom/konka/systemsetting/MainActivity;

    invoke-virtual {v0, v4}, Lcom/konka/systemsetting/MainActivity;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/LinearLayout;

    iput-object v0, p0, Lcom/konka/systemsetting/net/NetworkWireConfig;->mItemIpMode:Landroid/widget/LinearLayout;

    iget-object v0, p0, Lcom/konka/systemsetting/net/NetworkWireConfig;->mContext:Lcom/konka/systemsetting/MainActivity;

    const v1, 0x7f090064    # com.konka.systemsetting.R.id.sys_network_wired_item_ipaddr

    invoke-virtual {v0, v1}, Lcom/konka/systemsetting/MainActivity;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/LinearLayout;

    iput-object v0, p0, Lcom/konka/systemsetting/net/NetworkWireConfig;->mItemIpAddr:Landroid/widget/LinearLayout;

    iget-object v0, p0, Lcom/konka/systemsetting/net/NetworkWireConfig;->mContext:Lcom/konka/systemsetting/MainActivity;

    invoke-virtual {v0, v5}, Lcom/konka/systemsetting/MainActivity;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/LinearLayout;

    iput-object v0, p0, Lcom/konka/systemsetting/net/NetworkWireConfig;->mItemNetmask:Landroid/widget/LinearLayout;

    iget-object v0, p0, Lcom/konka/systemsetting/net/NetworkWireConfig;->mContext:Lcom/konka/systemsetting/MainActivity;

    invoke-virtual {v0, v6}, Lcom/konka/systemsetting/MainActivity;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/LinearLayout;

    iput-object v0, p0, Lcom/konka/systemsetting/net/NetworkWireConfig;->mItemGateway:Landroid/widget/LinearLayout;

    iget-object v0, p0, Lcom/konka/systemsetting/net/NetworkWireConfig;->mContext:Lcom/konka/systemsetting/MainActivity;

    const v1, 0x7f09006a    # com.konka.systemsetting.R.id.sys_network_wired_item_dnsserver

    invoke-virtual {v0, v1}, Lcom/konka/systemsetting/MainActivity;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/LinearLayout;

    iput-object v0, p0, Lcom/konka/systemsetting/net/NetworkWireConfig;->mItemDnsSvr:Landroid/widget/LinearLayout;

    iget-object v0, p0, Lcom/konka/systemsetting/net/NetworkWireConfig;->mContext:Lcom/konka/systemsetting/MainActivity;

    const v1, 0x7f09006c    # com.konka.systemsetting.R.id.sys_network_wired_item_set

    invoke-virtual {v0, v1}, Lcom/konka/systemsetting/MainActivity;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/Button;

    iput-object v0, p0, Lcom/konka/systemsetting/net/NetworkWireConfig;->mBtnSetWire:Landroid/widget/Button;

    iget-object v0, p0, Lcom/konka/systemsetting/net/NetworkWireConfig;->mContext:Lcom/konka/systemsetting/MainActivity;

    const v1, 0x7f09006d    # com.konka.systemsetting.R.id.sys_network_wired_item_detect

    invoke-virtual {v0, v1}, Lcom/konka/systemsetting/MainActivity;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/Button;

    iput-object v0, p0, Lcom/konka/systemsetting/net/NetworkWireConfig;->mBtnDetectWire:Landroid/widget/Button;

    iget-object v0, p0, Lcom/konka/systemsetting/net/NetworkWireConfig;->mItemIpMode:Landroid/widget/LinearLayout;

    invoke-virtual {v0, v3}, Landroid/widget/LinearLayout;->getChildAt(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    iput-object v0, p0, Lcom/konka/systemsetting/net/NetworkWireConfig;->mTxtIpMode:Landroid/widget/TextView;

    iget-object v0, p0, Lcom/konka/systemsetting/net/NetworkWireConfig;->mContext:Lcom/konka/systemsetting/MainActivity;

    const v1, 0x7f090065    # com.konka.systemsetting.R.id.sys_network_wired_edittext_ipaddr

    invoke-virtual {v0, v1}, Lcom/konka/systemsetting/MainActivity;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/EditText;

    iput-object v0, p0, Lcom/konka/systemsetting/net/NetworkWireConfig;->mEditIpAddr:Landroid/widget/EditText;

    iget-object v0, p0, Lcom/konka/systemsetting/net/NetworkWireConfig;->mContext:Lcom/konka/systemsetting/MainActivity;

    const v1, 0x7f090067    # com.konka.systemsetting.R.id.sys_network_wired_edittext_subnetmask

    invoke-virtual {v0, v1}, Lcom/konka/systemsetting/MainActivity;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/EditText;

    iput-object v0, p0, Lcom/konka/systemsetting/net/NetworkWireConfig;->mEditNetmask:Landroid/widget/EditText;

    iget-object v0, p0, Lcom/konka/systemsetting/net/NetworkWireConfig;->mContext:Lcom/konka/systemsetting/MainActivity;

    const v1, 0x7f090069    # com.konka.systemsetting.R.id.sys_network_wired_edittext_defaultgateway

    invoke-virtual {v0, v1}, Lcom/konka/systemsetting/MainActivity;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/EditText;

    iput-object v0, p0, Lcom/konka/systemsetting/net/NetworkWireConfig;->mEditGateway:Landroid/widget/EditText;

    iget-object v0, p0, Lcom/konka/systemsetting/net/NetworkWireConfig;->mContext:Lcom/konka/systemsetting/MainActivity;

    const v1, 0x7f09006b    # com.konka.systemsetting.R.id.sys_network_wired_edittext_dnsserver

    invoke-virtual {v0, v1}, Lcom/konka/systemsetting/MainActivity;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/EditText;

    iput-object v0, p0, Lcom/konka/systemsetting/net/NetworkWireConfig;->mEditDnsSvr:Landroid/widget/EditText;

    iget-object v0, p0, Lcom/konka/systemsetting/net/NetworkWireConfig;->mItemIpMode:Landroid/widget/LinearLayout;

    invoke-virtual {v0, v3}, Landroid/widget/LinearLayout;->setClickable(Z)V

    iget-object v0, p0, Lcom/konka/systemsetting/net/NetworkWireConfig;->mItemIpMode:Landroid/widget/LinearLayout;

    invoke-virtual {v0, v3}, Landroid/widget/LinearLayout;->setFocusable(Z)V

    iget-object v0, p0, Lcom/konka/systemsetting/net/NetworkWireConfig;->mItemIpMode:Landroid/widget/LinearLayout;

    invoke-virtual {v0, v3}, Landroid/widget/LinearLayout;->setFocusableInTouchMode(Z)V

    iget-object v0, p0, Lcom/konka/systemsetting/net/NetworkWireConfig;->mItemIpMode:Landroid/widget/LinearLayout;

    invoke-virtual {v0, v2}, Landroid/widget/LinearLayout;->setNextFocusLeftId(I)V

    iget-object v0, p0, Lcom/konka/systemsetting/net/NetworkWireConfig;->mEditIpAddr:Landroid/widget/EditText;

    invoke-virtual {v0, v2}, Landroid/widget/EditText;->setNextFocusLeftId(I)V

    iget-object v0, p0, Lcom/konka/systemsetting/net/NetworkWireConfig;->mEditNetmask:Landroid/widget/EditText;

    invoke-virtual {v0, v2}, Landroid/widget/EditText;->setNextFocusLeftId(I)V

    iget-object v0, p0, Lcom/konka/systemsetting/net/NetworkWireConfig;->mEditGateway:Landroid/widget/EditText;

    invoke-virtual {v0, v2}, Landroid/widget/EditText;->setNextFocusLeftId(I)V

    iget-object v0, p0, Lcom/konka/systemsetting/net/NetworkWireConfig;->mEditDnsSvr:Landroid/widget/EditText;

    invoke-virtual {v0, v2}, Landroid/widget/EditText;->setNextFocusLeftId(I)V

    iget-object v0, p0, Lcom/konka/systemsetting/net/NetworkWireConfig;->mBtnSetWire:Landroid/widget/Button;

    invoke-virtual {v0, v2}, Landroid/widget/Button;->setNextFocusLeftId(I)V

    iget-object v0, p0, Lcom/konka/systemsetting/net/NetworkWireConfig;->mBtnDetectWire:Landroid/widget/Button;

    invoke-virtual {v0, v2}, Landroid/widget/Button;->setNextFocusLeftId(I)V

    iget-object v0, p0, Lcom/konka/systemsetting/net/NetworkWireConfig;->mEditIpAddr:Landroid/widget/EditText;

    invoke-virtual {v0, v5}, Landroid/widget/EditText;->setNextFocusDownId(I)V

    iget-object v0, p0, Lcom/konka/systemsetting/net/NetworkWireConfig;->mEditNetmask:Landroid/widget/EditText;

    invoke-virtual {v0, v6}, Landroid/widget/EditText;->setNextFocusDownId(I)V

    iget-object v0, p0, Lcom/konka/systemsetting/net/NetworkWireConfig;->mEditGateway:Landroid/widget/EditText;

    const v1, 0x7f09006a    # com.konka.systemsetting.R.id.sys_network_wired_item_dnsserver

    invoke-virtual {v0, v1}, Landroid/widget/EditText;->setNextFocusDownId(I)V

    iget-object v0, p0, Lcom/konka/systemsetting/net/NetworkWireConfig;->mEditDnsSvr:Landroid/widget/EditText;

    const v1, 0x7f09006c    # com.konka.systemsetting.R.id.sys_network_wired_item_set

    invoke-virtual {v0, v1}, Landroid/widget/EditText;->setNextFocusDownId(I)V

    iget-object v0, p0, Lcom/konka/systemsetting/net/NetworkWireConfig;->mBtnSetWire:Landroid/widget/Button;

    const v1, 0x7f09006d    # com.konka.systemsetting.R.id.sys_network_wired_item_detect

    invoke-virtual {v0, v1}, Landroid/widget/Button;->setNextFocusDownId(I)V

    iget-object v0, p0, Lcom/konka/systemsetting/net/NetworkWireConfig;->mBtnDetectWire:Landroid/widget/Button;

    const v1, 0x7f09006d    # com.konka.systemsetting.R.id.sys_network_wired_item_detect

    invoke-virtual {v0, v1}, Landroid/widget/Button;->setNextFocusDownId(I)V

    iget-object v0, p0, Lcom/konka/systemsetting/net/NetworkWireConfig;->mItemIpMode:Landroid/widget/LinearLayout;

    invoke-virtual {v0, v4}, Landroid/widget/LinearLayout;->setNextFocusUpId(I)V

    iget-object v0, p0, Lcom/konka/systemsetting/net/NetworkWireConfig;->mEditIpAddr:Landroid/widget/EditText;

    invoke-virtual {v0, v4}, Landroid/widget/EditText;->setNextFocusUpId(I)V

    iget-object v0, p0, Lcom/konka/systemsetting/net/NetworkWireConfig;->mEditNetmask:Landroid/widget/EditText;

    const v1, 0x7f090064    # com.konka.systemsetting.R.id.sys_network_wired_item_ipaddr

    invoke-virtual {v0, v1}, Landroid/widget/EditText;->setNextFocusUpId(I)V

    iget-object v0, p0, Lcom/konka/systemsetting/net/NetworkWireConfig;->mEditGateway:Landroid/widget/EditText;

    invoke-virtual {v0, v5}, Landroid/widget/EditText;->setNextFocusUpId(I)V

    iget-object v0, p0, Lcom/konka/systemsetting/net/NetworkWireConfig;->mEditDnsSvr:Landroid/widget/EditText;

    invoke-virtual {v0, v6}, Landroid/widget/EditText;->setNextFocusUpId(I)V

    iget-object v0, p0, Lcom/konka/systemsetting/net/NetworkWireConfig;->mBtnDetectWire:Landroid/widget/Button;

    const v1, 0x7f09006c    # com.konka.systemsetting.R.id.sys_network_wired_item_set

    invoke-virtual {v0, v1}, Landroid/widget/Button;->setNextFocusUpId(I)V

    return-void
.end method

.method private onKeyEnterDown(I)Z
    .locals 4
    .param p1    # I

    const-string v2, "input_method"

    const/4 v0, 0x2

    iget-object v3, p0, Lcom/konka/systemsetting/net/NetworkWireConfig;->mContext:Lcom/konka/systemsetting/MainActivity;

    invoke-virtual {v3, v2}, Lcom/konka/systemsetting/MainActivity;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Landroid/view/inputmethod/InputMethodManager;

    packed-switch p1, :pswitch_data_0

    :pswitch_0
    const/4 v3, 0x0

    :goto_0
    return v3

    :pswitch_1
    invoke-direct {p0}, Lcom/konka/systemsetting/net/NetworkWireConfig;->changeIpMode()V

    :cond_0
    :goto_1
    const/4 v3, 0x1

    goto :goto_0

    :pswitch_2
    iget-object v3, p0, Lcom/konka/systemsetting/net/NetworkWireConfig;->mEditIpAddr:Landroid/widget/EditText;

    invoke-virtual {v3}, Landroid/widget/EditText;->requestFocus()Z

    iget-object v3, p0, Lcom/konka/systemsetting/net/NetworkWireConfig;->mEditIpAddr:Landroid/widget/EditText;

    invoke-virtual {v1, v3, v0}, Landroid/view/inputmethod/InputMethodManager;->showSoftInput(Landroid/view/View;I)Z

    goto :goto_1

    :pswitch_3
    iget-object v3, p0, Lcom/konka/systemsetting/net/NetworkWireConfig;->mEditNetmask:Landroid/widget/EditText;

    invoke-virtual {v3}, Landroid/widget/EditText;->requestFocus()Z

    iget-object v3, p0, Lcom/konka/systemsetting/net/NetworkWireConfig;->mEditNetmask:Landroid/widget/EditText;

    invoke-virtual {v1, v3, v0}, Landroid/view/inputmethod/InputMethodManager;->showSoftInput(Landroid/view/View;I)Z

    goto :goto_1

    :pswitch_4
    iget-object v3, p0, Lcom/konka/systemsetting/net/NetworkWireConfig;->mEditGateway:Landroid/widget/EditText;

    invoke-virtual {v3}, Landroid/widget/EditText;->requestFocus()Z

    iget-object v3, p0, Lcom/konka/systemsetting/net/NetworkWireConfig;->mEditGateway:Landroid/widget/EditText;

    invoke-virtual {v1, v3, v0}, Landroid/view/inputmethod/InputMethodManager;->showSoftInput(Landroid/view/View;I)Z

    goto :goto_1

    :pswitch_5
    iget-object v3, p0, Lcom/konka/systemsetting/net/NetworkWireConfig;->mEditDnsSvr:Landroid/widget/EditText;

    invoke-virtual {v3}, Landroid/widget/EditText;->requestFocus()Z

    iget-object v3, p0, Lcom/konka/systemsetting/net/NetworkWireConfig;->mEditDnsSvr:Landroid/widget/EditText;

    invoke-virtual {v1, v3, v0}, Landroid/view/inputmethod/InputMethodManager;->showSoftInput(Landroid/view/View;I)Z

    goto :goto_1

    :pswitch_6
    invoke-direct {p0}, Lcom/konka/systemsetting/net/NetworkWireConfig;->checkEthConfiguration()Z

    move-result v3

    if-eqz v3, :cond_0

    invoke-direct {p0}, Lcom/konka/systemsetting/net/NetworkWireConfig;->setEthConfiguration()V

    goto :goto_1

    :pswitch_7
    invoke-direct {p0}, Lcom/konka/systemsetting/net/NetworkWireConfig;->detectNetworkState()Z

    goto :goto_1

    nop

    :pswitch_data_0
    .packed-switch 0x7f090063
        :pswitch_1    # com.konka.systemsetting.R.id.sys_network_wired_item_ipmode
        :pswitch_0    # com.konka.systemsetting.R.id.sys_network_wired_item_ipaddr
        :pswitch_2    # com.konka.systemsetting.R.id.sys_network_wired_edittext_ipaddr
        :pswitch_0    # com.konka.systemsetting.R.id.sys_network_wired_item_subnetmask
        :pswitch_3    # com.konka.systemsetting.R.id.sys_network_wired_edittext_subnetmask
        :pswitch_0    # com.konka.systemsetting.R.id.sys_network_wired_item_defaultgateway
        :pswitch_4    # com.konka.systemsetting.R.id.sys_network_wired_edittext_defaultgateway
        :pswitch_0    # com.konka.systemsetting.R.id.sys_network_wired_item_dnsserver
        :pswitch_5    # com.konka.systemsetting.R.id.sys_network_wired_edittext_dnsserver
        :pswitch_6    # com.konka.systemsetting.R.id.sys_network_wired_item_set
        :pswitch_7    # com.konka.systemsetting.R.id.sys_network_wired_item_detect
    .end packed-switch
.end method

.method private setEthConfiguration()V
    .locals 7

    const/4 v6, 0x0

    new-instance v2, Landroid/net/ethernet/EthernetDevInfo;

    invoke-direct {v2}, Landroid/net/ethernet/EthernetDevInfo;-><init>()V

    const-string v5, "eth0"

    invoke-virtual {v2, v5}, Landroid/net/ethernet/EthernetDevInfo;->setIfName(Ljava/lang/String;)V

    sget-boolean v5, Lcom/konka/systemsetting/common/Content$ethernet;->isAutoSet:Z

    if-eqz v5, :cond_0

    const-string v5, "dhcp"

    invoke-virtual {v2, v5}, Landroid/net/ethernet/EthernetDevInfo;->setConnectMode(Ljava/lang/String;)Z

    invoke-virtual {v2, v6}, Landroid/net/ethernet/EthernetDevInfo;->setIpAddress(Ljava/lang/String;)V

    invoke-virtual {v2, v6}, Landroid/net/ethernet/EthernetDevInfo;->setNetMask(Ljava/lang/String;)V

    invoke-virtual {v2, v6}, Landroid/net/ethernet/EthernetDevInfo;->setRouteAddr(Ljava/lang/String;)V

    invoke-virtual {v2, v6}, Landroid/net/ethernet/EthernetDevInfo;->setDnsAddr(Ljava/lang/String;)V

    :goto_0
    iget-object v5, p0, Lcom/konka/systemsetting/net/NetworkWireConfig;->mContext:Lcom/konka/systemsetting/MainActivity;

    const-string v6, "wifi"

    invoke-virtual {v5, v6}, Lcom/konka/systemsetting/MainActivity;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v4

    check-cast v4, Landroid/net/wifi/WifiManager;

    const/4 v5, 0x0

    invoke-virtual {v4, v5}, Landroid/net/wifi/WifiManager;->setWifiEnabled(Z)Z

    :try_start_0
    iget-object v5, p0, Lcom/konka/systemsetting/net/NetworkWireConfig;->mEthManager:Landroid/net/ethernet/EthernetManager;

    invoke-virtual {v5, v2}, Landroid/net/ethernet/EthernetManager;->updateDevInfo(Landroid/net/ethernet/EthernetDevInfo;)V

    iget-object v5, p0, Lcom/konka/systemsetting/net/NetworkWireConfig;->mEthManager:Landroid/net/ethernet/EthernetManager;

    const/4 v6, 0x1

    invoke-virtual {v5, v6}, Landroid/net/ethernet/EthernetManager;->setEnabled(Z)V

    const v5, 0x7f060069    # com.konka.systemsetting.R.string.str_network_eth_set_done

    invoke-direct {p0, v5}, Lcom/konka/systemsetting/net/NetworkWireConfig;->toastText(I)V

    invoke-direct {p0}, Lcom/konka/systemsetting/net/NetworkWireConfig;->getConfigFromManager()V

    invoke-virtual {p0}, Lcom/konka/systemsetting/net/NetworkWireConfig;->updateWireStateToUI()V

    const-string v0, "com.konka.settings.ETHNET_ON_SET"

    new-instance v3, Landroid/content/Intent;

    invoke-direct {v3, v0}, Landroid/content/Intent;-><init>(Ljava/lang/String;)V

    iget-object v5, p0, Lcom/konka/systemsetting/net/NetworkWireConfig;->mContext:Lcom/konka/systemsetting/MainActivity;

    invoke-virtual {v5, v3}, Lcom/konka/systemsetting/MainActivity;->sendBroadcast(Landroid/content/Intent;)V
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    :goto_1
    return-void

    :cond_0
    const-string v5, "manual"

    invoke-virtual {v2, v5}, Landroid/net/ethernet/EthernetDevInfo;->setConnectMode(Ljava/lang/String;)Z

    sget-object v5, Lcom/konka/systemsetting/common/Content$ethernet;->ipaddress:Ljava/lang/CharSequence;

    invoke-interface {v5}, Ljava/lang/CharSequence;->toString()Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v2, v5}, Landroid/net/ethernet/EthernetDevInfo;->setIpAddress(Ljava/lang/String;)V

    sget-object v5, Lcom/konka/systemsetting/common/Content$ethernet;->netmask:Ljava/lang/CharSequence;

    invoke-interface {v5}, Ljava/lang/CharSequence;->toString()Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v2, v5}, Landroid/net/ethernet/EthernetDevInfo;->setNetMask(Ljava/lang/String;)V

    sget-object v5, Lcom/konka/systemsetting/common/Content$ethernet;->gateway:Ljava/lang/CharSequence;

    invoke-interface {v5}, Ljava/lang/CharSequence;->toString()Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v2, v5}, Landroid/net/ethernet/EthernetDevInfo;->setRouteAddr(Ljava/lang/String;)V

    sget-object v5, Lcom/konka/systemsetting/common/Content$ethernet;->firstdns:Ljava/lang/CharSequence;

    invoke-interface {v5}, Ljava/lang/CharSequence;->toString()Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v2, v5}, Landroid/net/ethernet/EthernetDevInfo;->setDnsAddr(Ljava/lang/String;)V

    goto :goto_0

    :catch_0
    move-exception v1

    invoke-virtual {v1}, Ljava/lang/Exception;->getMessage()Ljava/lang/String;

    move-result-object v5

    invoke-direct {p0, v5}, Lcom/konka/systemsetting/net/NetworkWireConfig;->toastText(Ljava/lang/String;)V

    goto :goto_1
.end method

.method private setItemBgByFocusEdit(I)V
    .locals 2
    .param p1    # I

    const v1, 0x7f020030    # com.konka.systemsetting.R.drawable.syssettingthirditemmidfocus

    packed-switch p1, :pswitch_data_0

    :pswitch_0
    const-string v0, "Cable network setting: setItemBgByFocusEdit error, on such edit id!!!"

    invoke-direct {p0, v0}, Lcom/konka/systemsetting/net/NetworkWireConfig;->debug(Ljava/lang/String;)V

    :goto_0
    return-void

    :pswitch_1
    iget-object v0, p0, Lcom/konka/systemsetting/net/NetworkWireConfig;->mEditIpAddr:Landroid/widget/EditText;

    invoke-virtual {v0}, Landroid/widget/EditText;->selectAll()V

    iget-object v0, p0, Lcom/konka/systemsetting/net/NetworkWireConfig;->mItemIpAddr:Landroid/widget/LinearLayout;

    invoke-virtual {v0, v1}, Landroid/widget/LinearLayout;->setBackgroundResource(I)V

    goto :goto_0

    :pswitch_2
    iget-object v0, p0, Lcom/konka/systemsetting/net/NetworkWireConfig;->mEditNetmask:Landroid/widget/EditText;

    invoke-virtual {v0}, Landroid/widget/EditText;->selectAll()V

    iget-object v0, p0, Lcom/konka/systemsetting/net/NetworkWireConfig;->mItemNetmask:Landroid/widget/LinearLayout;

    invoke-virtual {v0, v1}, Landroid/widget/LinearLayout;->setBackgroundResource(I)V

    goto :goto_0

    :pswitch_3
    iget-object v0, p0, Lcom/konka/systemsetting/net/NetworkWireConfig;->mEditGateway:Landroid/widget/EditText;

    invoke-virtual {v0}, Landroid/widget/EditText;->selectAll()V

    iget-object v0, p0, Lcom/konka/systemsetting/net/NetworkWireConfig;->mItemGateway:Landroid/widget/LinearLayout;

    invoke-virtual {v0, v1}, Landroid/widget/LinearLayout;->setBackgroundResource(I)V

    goto :goto_0

    :pswitch_4
    iget-object v0, p0, Lcom/konka/systemsetting/net/NetworkWireConfig;->mEditDnsSvr:Landroid/widget/EditText;

    invoke-virtual {v0}, Landroid/widget/EditText;->selectAll()V

    iget-object v0, p0, Lcom/konka/systemsetting/net/NetworkWireConfig;->mItemDnsSvr:Landroid/widget/LinearLayout;

    const v1, 0x7f02002e    # com.konka.systemsetting.R.drawable.syssettingthirditembottomfocus

    invoke-virtual {v0, v1}, Landroid/widget/LinearLayout;->setBackgroundResource(I)V

    goto :goto_0

    nop

    :pswitch_data_0
    .packed-switch 0x7f090065
        :pswitch_1    # com.konka.systemsetting.R.id.sys_network_wired_edittext_ipaddr
        :pswitch_0    # com.konka.systemsetting.R.id.sys_network_wired_item_subnetmask
        :pswitch_2    # com.konka.systemsetting.R.id.sys_network_wired_edittext_subnetmask
        :pswitch_0    # com.konka.systemsetting.R.id.sys_network_wired_item_defaultgateway
        :pswitch_3    # com.konka.systemsetting.R.id.sys_network_wired_edittext_defaultgateway
        :pswitch_0    # com.konka.systemsetting.R.id.sys_network_wired_item_dnsserver
        :pswitch_4    # com.konka.systemsetting.R.id.sys_network_wired_edittext_dnsserver
    .end packed-switch
.end method

.method private setItemBgByLoseFocusEdit(I)V
    .locals 4
    .param p1    # I

    const v3, 0x7f02002f    # com.konka.systemsetting.R.drawable.syssettingthirditemmid

    const-string v0, ""

    packed-switch p1, :pswitch_data_0

    :goto_0
    :pswitch_0
    return-void

    :pswitch_1
    iget-object v1, p0, Lcom/konka/systemsetting/net/NetworkWireConfig;->mEditIpAddr:Landroid/widget/EditText;

    invoke-virtual {v1}, Landroid/widget/EditText;->getText()Landroid/text/Editable;

    move-result-object v1

    invoke-interface {v1}, Landroid/text/Editable;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/String;->trim()Ljava/lang/String;

    move-result-object v0

    sput-object v0, Lcom/konka/systemsetting/common/Content$ethernet;->ipaddress:Ljava/lang/CharSequence;

    iget-object v1, p0, Lcom/konka/systemsetting/net/NetworkWireConfig;->mEditIpAddr:Landroid/widget/EditText;

    sget-object v2, Lcom/konka/systemsetting/common/Content$ethernet;->ipaddress:Ljava/lang/CharSequence;

    invoke-virtual {v1, v2}, Landroid/widget/EditText;->setText(Ljava/lang/CharSequence;)V

    iget-object v1, p0, Lcom/konka/systemsetting/net/NetworkWireConfig;->mItemIpAddr:Landroid/widget/LinearLayout;

    invoke-virtual {v1, v3}, Landroid/widget/LinearLayout;->setBackgroundResource(I)V

    goto :goto_0

    :pswitch_2
    iget-object v1, p0, Lcom/konka/systemsetting/net/NetworkWireConfig;->mEditNetmask:Landroid/widget/EditText;

    invoke-virtual {v1}, Landroid/widget/EditText;->getText()Landroid/text/Editable;

    move-result-object v1

    invoke-interface {v1}, Landroid/text/Editable;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/String;->trim()Ljava/lang/String;

    move-result-object v0

    sput-object v0, Lcom/konka/systemsetting/common/Content$ethernet;->netmask:Ljava/lang/CharSequence;

    iget-object v1, p0, Lcom/konka/systemsetting/net/NetworkWireConfig;->mEditNetmask:Landroid/widget/EditText;

    sget-object v2, Lcom/konka/systemsetting/common/Content$ethernet;->netmask:Ljava/lang/CharSequence;

    invoke-virtual {v1, v2}, Landroid/widget/EditText;->setText(Ljava/lang/CharSequence;)V

    iget-object v1, p0, Lcom/konka/systemsetting/net/NetworkWireConfig;->mItemNetmask:Landroid/widget/LinearLayout;

    invoke-virtual {v1, v3}, Landroid/widget/LinearLayout;->setBackgroundResource(I)V

    goto :goto_0

    :pswitch_3
    iget-object v1, p0, Lcom/konka/systemsetting/net/NetworkWireConfig;->mEditGateway:Landroid/widget/EditText;

    invoke-virtual {v1}, Landroid/widget/EditText;->getText()Landroid/text/Editable;

    move-result-object v1

    invoke-interface {v1}, Landroid/text/Editable;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/String;->trim()Ljava/lang/String;

    move-result-object v0

    sput-object v0, Lcom/konka/systemsetting/common/Content$ethernet;->gateway:Ljava/lang/CharSequence;

    iget-object v1, p0, Lcom/konka/systemsetting/net/NetworkWireConfig;->mEditGateway:Landroid/widget/EditText;

    sget-object v2, Lcom/konka/systemsetting/common/Content$ethernet;->gateway:Ljava/lang/CharSequence;

    invoke-virtual {v1, v2}, Landroid/widget/EditText;->setText(Ljava/lang/CharSequence;)V

    iget-object v1, p0, Lcom/konka/systemsetting/net/NetworkWireConfig;->mItemGateway:Landroid/widget/LinearLayout;

    invoke-virtual {v1, v3}, Landroid/widget/LinearLayout;->setBackgroundResource(I)V

    goto :goto_0

    :pswitch_4
    iget-object v1, p0, Lcom/konka/systemsetting/net/NetworkWireConfig;->mEditDnsSvr:Landroid/widget/EditText;

    invoke-virtual {v1}, Landroid/widget/EditText;->getText()Landroid/text/Editable;

    move-result-object v1

    invoke-interface {v1}, Landroid/text/Editable;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/String;->trim()Ljava/lang/String;

    move-result-object v0

    sput-object v0, Lcom/konka/systemsetting/common/Content$ethernet;->firstdns:Ljava/lang/CharSequence;

    iget-object v1, p0, Lcom/konka/systemsetting/net/NetworkWireConfig;->mEditDnsSvr:Landroid/widget/EditText;

    sget-object v2, Lcom/konka/systemsetting/common/Content$ethernet;->firstdns:Ljava/lang/CharSequence;

    invoke-virtual {v1, v2}, Landroid/widget/EditText;->setText(Ljava/lang/CharSequence;)V

    iget-object v1, p0, Lcom/konka/systemsetting/net/NetworkWireConfig;->mItemDnsSvr:Landroid/widget/LinearLayout;

    const v2, 0x7f02002d    # com.konka.systemsetting.R.drawable.syssettingthirditembottom

    invoke-virtual {v1, v2}, Landroid/widget/LinearLayout;->setBackgroundResource(I)V

    goto :goto_0

    :pswitch_data_0
    .packed-switch 0x7f090065
        :pswitch_1    # com.konka.systemsetting.R.id.sys_network_wired_edittext_ipaddr
        :pswitch_0    # com.konka.systemsetting.R.id.sys_network_wired_item_subnetmask
        :pswitch_2    # com.konka.systemsetting.R.id.sys_network_wired_edittext_subnetmask
        :pswitch_0    # com.konka.systemsetting.R.id.sys_network_wired_item_defaultgateway
        :pswitch_3    # com.konka.systemsetting.R.id.sys_network_wired_edittext_defaultgateway
        :pswitch_0    # com.konka.systemsetting.R.id.sys_network_wired_item_dnsserver
        :pswitch_4    # com.konka.systemsetting.R.id.sys_network_wired_edittext_dnsserver
    .end packed-switch
.end method

.method private setOnClickListener()V
    .locals 1

    iget-object v0, p0, Lcom/konka/systemsetting/net/NetworkWireConfig;->mItemIpMode:Landroid/widget/LinearLayout;

    invoke-virtual {v0, p0}, Landroid/widget/LinearLayout;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    iget-object v0, p0, Lcom/konka/systemsetting/net/NetworkWireConfig;->mItemIpAddr:Landroid/widget/LinearLayout;

    invoke-virtual {v0, p0}, Landroid/widget/LinearLayout;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    iget-object v0, p0, Lcom/konka/systemsetting/net/NetworkWireConfig;->mItemGateway:Landroid/widget/LinearLayout;

    invoke-virtual {v0, p0}, Landroid/widget/LinearLayout;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    iget-object v0, p0, Lcom/konka/systemsetting/net/NetworkWireConfig;->mItemNetmask:Landroid/widget/LinearLayout;

    invoke-virtual {v0, p0}, Landroid/widget/LinearLayout;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    iget-object v0, p0, Lcom/konka/systemsetting/net/NetworkWireConfig;->mItemDnsSvr:Landroid/widget/LinearLayout;

    invoke-virtual {v0, p0}, Landroid/widget/LinearLayout;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    iget-object v0, p0, Lcom/konka/systemsetting/net/NetworkWireConfig;->mBtnSetWire:Landroid/widget/Button;

    invoke-virtual {v0, p0}, Landroid/widget/Button;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    iget-object v0, p0, Lcom/konka/systemsetting/net/NetworkWireConfig;->mBtnDetectWire:Landroid/widget/Button;

    invoke-virtual {v0, p0}, Landroid/widget/Button;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    return-void
.end method

.method private setOnFocusChangeListner()V
    .locals 2

    iget-object v0, p0, Lcom/konka/systemsetting/net/NetworkWireConfig;->mItemIpMode:Landroid/widget/LinearLayout;

    invoke-virtual {v0, p0}, Landroid/widget/LinearLayout;->setOnFocusChangeListener(Landroid/view/View$OnFocusChangeListener;)V

    iget-object v0, p0, Lcom/konka/systemsetting/net/NetworkWireConfig;->mBtnSetWire:Landroid/widget/Button;

    invoke-virtual {v0, p0}, Landroid/widget/Button;->setOnFocusChangeListener(Landroid/view/View$OnFocusChangeListener;)V

    iget-object v0, p0, Lcom/konka/systemsetting/net/NetworkWireConfig;->mBtnDetectWire:Landroid/widget/Button;

    invoke-virtual {v0, p0}, Landroid/widget/Button;->setOnFocusChangeListener(Landroid/view/View$OnFocusChangeListener;)V

    iget-object v0, p0, Lcom/konka/systemsetting/net/NetworkWireConfig;->mEditIpAddr:Landroid/widget/EditText;

    iget-object v1, p0, Lcom/konka/systemsetting/net/NetworkWireConfig;->etOnFocusChangeEvent:Landroid/view/View$OnFocusChangeListener;

    invoke-virtual {v0, v1}, Landroid/widget/EditText;->setOnFocusChangeListener(Landroid/view/View$OnFocusChangeListener;)V

    iget-object v0, p0, Lcom/konka/systemsetting/net/NetworkWireConfig;->mEditNetmask:Landroid/widget/EditText;

    iget-object v1, p0, Lcom/konka/systemsetting/net/NetworkWireConfig;->etOnFocusChangeEvent:Landroid/view/View$OnFocusChangeListener;

    invoke-virtual {v0, v1}, Landroid/widget/EditText;->setOnFocusChangeListener(Landroid/view/View$OnFocusChangeListener;)V

    iget-object v0, p0, Lcom/konka/systemsetting/net/NetworkWireConfig;->mEditGateway:Landroid/widget/EditText;

    iget-object v1, p0, Lcom/konka/systemsetting/net/NetworkWireConfig;->etOnFocusChangeEvent:Landroid/view/View$OnFocusChangeListener;

    invoke-virtual {v0, v1}, Landroid/widget/EditText;->setOnFocusChangeListener(Landroid/view/View$OnFocusChangeListener;)V

    iget-object v0, p0, Lcom/konka/systemsetting/net/NetworkWireConfig;->mEditDnsSvr:Landroid/widget/EditText;

    iget-object v1, p0, Lcom/konka/systemsetting/net/NetworkWireConfig;->etOnFocusChangeEvent:Landroid/view/View$OnFocusChangeListener;

    invoke-virtual {v0, v1}, Landroid/widget/EditText;->setOnFocusChangeListener(Landroid/view/View$OnFocusChangeListener;)V

    iget-object v0, p0, Lcom/konka/systemsetting/net/NetworkWireConfig;->mItemIpAddr:Landroid/widget/LinearLayout;

    iget-object v1, p0, Lcom/konka/systemsetting/net/NetworkWireConfig;->etItemOnFocusChangeEvent:Landroid/view/View$OnFocusChangeListener;

    invoke-virtual {v0, v1}, Landroid/widget/LinearLayout;->setOnFocusChangeListener(Landroid/view/View$OnFocusChangeListener;)V

    iget-object v0, p0, Lcom/konka/systemsetting/net/NetworkWireConfig;->mItemNetmask:Landroid/widget/LinearLayout;

    iget-object v1, p0, Lcom/konka/systemsetting/net/NetworkWireConfig;->etItemOnFocusChangeEvent:Landroid/view/View$OnFocusChangeListener;

    invoke-virtual {v0, v1}, Landroid/widget/LinearLayout;->setOnFocusChangeListener(Landroid/view/View$OnFocusChangeListener;)V

    iget-object v0, p0, Lcom/konka/systemsetting/net/NetworkWireConfig;->mItemGateway:Landroid/widget/LinearLayout;

    iget-object v1, p0, Lcom/konka/systemsetting/net/NetworkWireConfig;->etItemOnFocusChangeEvent:Landroid/view/View$OnFocusChangeListener;

    invoke-virtual {v0, v1}, Landroid/widget/LinearLayout;->setOnFocusChangeListener(Landroid/view/View$OnFocusChangeListener;)V

    iget-object v0, p0, Lcom/konka/systemsetting/net/NetworkWireConfig;->mItemDnsSvr:Landroid/widget/LinearLayout;

    iget-object v1, p0, Lcom/konka/systemsetting/net/NetworkWireConfig;->etItemOnFocusChangeEvent:Landroid/view/View$OnFocusChangeListener;

    invoke-virtual {v0, v1}, Landroid/widget/LinearLayout;->setOnFocusChangeListener(Landroid/view/View$OnFocusChangeListener;)V

    return-void
.end method

.method private setOnKeyListener()V
    .locals 1

    iget-object v0, p0, Lcom/konka/systemsetting/net/NetworkWireConfig;->mItemIpMode:Landroid/widget/LinearLayout;

    invoke-virtual {v0, p0}, Landroid/widget/LinearLayout;->setOnKeyListener(Landroid/view/View$OnKeyListener;)V

    iget-object v0, p0, Lcom/konka/systemsetting/net/NetworkWireConfig;->mBtnSetWire:Landroid/widget/Button;

    invoke-virtual {v0, p0}, Landroid/widget/Button;->setOnKeyListener(Landroid/view/View$OnKeyListener;)V

    iget-object v0, p0, Lcom/konka/systemsetting/net/NetworkWireConfig;->mBtnDetectWire:Landroid/widget/Button;

    invoke-virtual {v0, p0}, Landroid/widget/Button;->setOnKeyListener(Landroid/view/View$OnKeyListener;)V

    iget-object v0, p0, Lcom/konka/systemsetting/net/NetworkWireConfig;->mEditIpAddr:Landroid/widget/EditText;

    invoke-virtual {v0, p0}, Landroid/widget/EditText;->setOnKeyListener(Landroid/view/View$OnKeyListener;)V

    iget-object v0, p0, Lcom/konka/systemsetting/net/NetworkWireConfig;->mEditNetmask:Landroid/widget/EditText;

    invoke-virtual {v0, p0}, Landroid/widget/EditText;->setOnKeyListener(Landroid/view/View$OnKeyListener;)V

    iget-object v0, p0, Lcom/konka/systemsetting/net/NetworkWireConfig;->mEditGateway:Landroid/widget/EditText;

    invoke-virtual {v0, p0}, Landroid/widget/EditText;->setOnKeyListener(Landroid/view/View$OnKeyListener;)V

    iget-object v0, p0, Lcom/konka/systemsetting/net/NetworkWireConfig;->mEditDnsSvr:Landroid/widget/EditText;

    invoke-virtual {v0, p0}, Landroid/widget/EditText;->setOnKeyListener(Landroid/view/View$OnKeyListener;)V

    return-void
.end method

.method private toastText(I)V
    .locals 1
    .param p1    # I

    iget-object v0, p0, Lcom/konka/systemsetting/net/NetworkWireConfig;->mContext:Lcom/konka/systemsetting/MainActivity;

    invoke-virtual {v0, p1}, Lcom/konka/systemsetting/MainActivity;->toastText(I)V

    return-void
.end method

.method private toastText(Ljava/lang/String;)V
    .locals 1
    .param p1    # Ljava/lang/String;

    iget-object v0, p0, Lcom/konka/systemsetting/net/NetworkWireConfig;->mContext:Lcom/konka/systemsetting/MainActivity;

    invoke-virtual {v0, p1}, Lcom/konka/systemsetting/MainActivity;->toastText(Ljava/lang/String;)V

    return-void
.end method


# virtual methods
.method public changeIPModeUI()V
    .locals 4

    const/4 v3, 0x1

    const/4 v2, 0x0

    sget-boolean v1, Lcom/konka/systemsetting/common/Content$ethernet;->isAutoSet:Z

    if-eqz v1, :cond_0

    move v1, v2

    :goto_0
    sput-boolean v1, Lcom/konka/systemsetting/common/Content$ethernet;->isAutoSet:Z

    sget-boolean v1, Lcom/konka/systemsetting/common/Content$ethernet;->isAutoSet:Z

    if-eqz v1, :cond_1

    move v0, v2

    :goto_1
    iget-object v1, p0, Lcom/konka/systemsetting/net/NetworkWireConfig;->mTxtIpMode:Landroid/widget/TextView;

    sget-object v2, Lcom/konka/systemsetting/common/Content$ethernet;->IpMode:[Ljava/lang/CharSequence;

    aget-object v2, v2, v0

    invoke-virtual {v1, v2}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    return-void

    :cond_0
    move v1, v3

    goto :goto_0

    :cond_1
    move v0, v3

    goto :goto_1
.end method

.method public onClick(Landroid/view/View;)V
    .locals 1
    .param p1    # Landroid/view/View;

    invoke-virtual {p1}, Landroid/view/View;->getId()I

    move-result v0

    invoke-direct {p0, v0}, Lcom/konka/systemsetting/net/NetworkWireConfig;->onKeyEnterDown(I)Z

    return-void
.end method

.method public onFocusChange(Landroid/view/View;Z)V
    .locals 5
    .param p1    # Landroid/view/View;
    .param p2    # Z

    invoke-virtual {p1}, Landroid/view/View;->getId()I

    move-result v0

    const/high16 v1, -0x1000000

    if-eqz p2, :cond_0

    iget-object v2, p0, Lcom/konka/systemsetting/net/NetworkWireConfig;->mContext:Lcom/konka/systemsetting/MainActivity;

    iget-object v2, v2, Lcom/konka/systemsetting/MainActivity;->m_networkPageManager:Lcom/konka/systemsetting/net/NetworkPageManager;

    iget-object v3, p0, Lcom/konka/systemsetting/net/NetworkWireConfig;->mContext:Lcom/konka/systemsetting/MainActivity;

    iget-object v3, v3, Lcom/konka/systemsetting/MainActivity;->m_networkPageManager:Lcom/konka/systemsetting/net/NetworkPageManager;

    invoke-virtual {v3}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    const/4 v3, 0x0

    invoke-static {v3}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v3

    iget-object v4, p0, Lcom/konka/systemsetting/net/NetworkWireConfig;->mContext:Lcom/konka/systemsetting/MainActivity;

    iget-object v4, v4, Lcom/konka/systemsetting/MainActivity;->m_networkPageManager:Lcom/konka/systemsetting/net/NetworkPageManager;

    invoke-virtual {v4}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    const/4 v4, 0x1

    invoke-static {v4}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v4

    invoke-virtual {v2, v3, v4}, Lcom/konka/systemsetting/net/NetworkPageManager;->setLastFocus(Ljava/lang/Integer;Ljava/lang/Integer;)V

    invoke-direct {p0, v0}, Lcom/konka/systemsetting/net/NetworkWireConfig;->getFocusImageById(I)I

    move-result v1

    :goto_0
    invoke-virtual {p1, v1}, Landroid/view/View;->setBackgroundResource(I)V

    return-void

    :cond_0
    invoke-direct {p0, v0}, Lcom/konka/systemsetting/net/NetworkWireConfig;->getUnFocusImageById(I)I

    move-result v1

    goto :goto_0
.end method

.method public onKey(Landroid/view/View;ILandroid/view/KeyEvent;)Z
    .locals 2
    .param p1    # Landroid/view/View;
    .param p2    # I
    .param p3    # Landroid/view/KeyEvent;

    invoke-virtual {p1}, Landroid/view/View;->getId()I

    move-result v0

    sparse-switch p2, :sswitch_data_0

    :cond_0
    const/4 v1, 0x0

    :goto_0
    return v1

    :sswitch_0
    invoke-virtual {p3}, Landroid/view/KeyEvent;->getAction()I

    move-result v1

    if-nez v1, :cond_0

    invoke-direct {p0, v0}, Lcom/konka/systemsetting/net/NetworkWireConfig;->onKeyEnterDown(I)Z

    move-result v1

    goto :goto_0

    :sswitch_data_0
    .sparse-switch
        0x17 -> :sswitch_0
        0x42 -> :sswitch_0
    .end sparse-switch
.end method

.method public refreshConfiguration()V
    .locals 3

    iget-object v1, p0, Lcom/konka/systemsetting/net/NetworkWireConfig;->mEthManager:Landroid/net/ethernet/EthernetManager;

    invoke-virtual {v1}, Landroid/net/ethernet/EthernetManager;->getSavedConfig()Landroid/net/ethernet/EthernetDevInfo;

    move-result-object v1

    iput-object v1, p0, Lcom/konka/systemsetting/net/NetworkWireConfig;->mEthInfo:Landroid/net/ethernet/EthernetDevInfo;

    iget-object v1, p0, Lcom/konka/systemsetting/net/NetworkWireConfig;->mEthInfo:Landroid/net/ethernet/EthernetDevInfo;

    if-nez v1, :cond_0

    new-instance v1, Landroid/net/ethernet/EthernetDevInfo;

    invoke-direct {v1}, Landroid/net/ethernet/EthernetDevInfo;-><init>()V

    iput-object v1, p0, Lcom/konka/systemsetting/net/NetworkWireConfig;->mEthInfo:Landroid/net/ethernet/EthernetDevInfo;

    iget-object v1, p0, Lcom/konka/systemsetting/net/NetworkWireConfig;->mEthInfo:Landroid/net/ethernet/EthernetDevInfo;

    const-string v2, "dhcp"

    invoke-virtual {v1, v2}, Landroid/net/ethernet/EthernetDevInfo;->setConnectMode(Ljava/lang/String;)Z

    :cond_0
    iget-object v1, p0, Lcom/konka/systemsetting/net/NetworkWireConfig;->mEthInfo:Landroid/net/ethernet/EthernetDevInfo;

    invoke-virtual {v1}, Landroid/net/ethernet/EthernetDevInfo;->getConnectMode()Ljava/lang/String;

    move-result-object v0

    iget-object v1, p0, Lcom/konka/systemsetting/net/NetworkWireConfig;->ETH_CONN_MODE_DHCP:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_2

    const/4 v1, 0x1

    sput-boolean v1, Lcom/konka/systemsetting/common/Content$ethernet;->isAutoSet:Z

    :cond_1
    :goto_0
    iget-object v1, p0, Lcom/konka/systemsetting/net/NetworkWireConfig;->mEthManager:Landroid/net/ethernet/EthernetManager;

    invoke-virtual {v1}, Landroid/net/ethernet/EthernetManager;->isConfigured()Z

    move-result v1

    if-eqz v1, :cond_3

    iget-object v1, p0, Lcom/konka/systemsetting/net/NetworkWireConfig;->mEthManager:Landroid/net/ethernet/EthernetManager;

    invoke-virtual {v1}, Landroid/net/ethernet/EthernetManager;->getSavedConfig()Landroid/net/ethernet/EthernetDevInfo;

    move-result-object v1

    iput-object v1, p0, Lcom/konka/systemsetting/net/NetworkWireConfig;->mEthInfo:Landroid/net/ethernet/EthernetDevInfo;

    iget-object v1, p0, Lcom/konka/systemsetting/net/NetworkWireConfig;->mEthInfo:Landroid/net/ethernet/EthernetDevInfo;

    invoke-virtual {v1}, Landroid/net/ethernet/EthernetDevInfo;->getIpAddress()Ljava/lang/String;

    move-result-object v1

    sput-object v1, Lcom/konka/systemsetting/common/Content$ethernet;->ipaddress:Ljava/lang/CharSequence;

    iget-object v1, p0, Lcom/konka/systemsetting/net/NetworkWireConfig;->mEthInfo:Landroid/net/ethernet/EthernetDevInfo;

    invoke-virtual {v1}, Landroid/net/ethernet/EthernetDevInfo;->getNetMask()Ljava/lang/String;

    move-result-object v1

    sput-object v1, Lcom/konka/systemsetting/common/Content$ethernet;->netmask:Ljava/lang/CharSequence;

    iget-object v1, p0, Lcom/konka/systemsetting/net/NetworkWireConfig;->mEthInfo:Landroid/net/ethernet/EthernetDevInfo;

    invoke-virtual {v1}, Landroid/net/ethernet/EthernetDevInfo;->getRouteAddr()Ljava/lang/String;

    move-result-object v1

    sput-object v1, Lcom/konka/systemsetting/common/Content$ethernet;->gateway:Ljava/lang/CharSequence;

    iget-object v1, p0, Lcom/konka/systemsetting/net/NetworkWireConfig;->mEthInfo:Landroid/net/ethernet/EthernetDevInfo;

    invoke-virtual {v1}, Landroid/net/ethernet/EthernetDevInfo;->getDnsAddr()Ljava/lang/String;

    move-result-object v1

    sput-object v1, Lcom/konka/systemsetting/common/Content$ethernet;->firstdns:Ljava/lang/CharSequence;

    :goto_1
    invoke-virtual {p0}, Lcom/konka/systemsetting/net/NetworkWireConfig;->updateWireStateToUI()V

    return-void

    :cond_2
    iget-object v1, p0, Lcom/konka/systemsetting/net/NetworkWireConfig;->ETH_CONN_MODE_MANUAL:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_1

    const/4 v1, 0x0

    sput-boolean v1, Lcom/konka/systemsetting/common/Content$ethernet;->isAutoSet:Z

    goto :goto_0

    :cond_3
    const-string v1, "ethernet is not configured yet."

    invoke-direct {p0, v1}, Lcom/konka/systemsetting/net/NetworkWireConfig;->debug(Ljava/lang/String;)V

    goto :goto_1
.end method

.method public setWireItemEnabled(Landroid/widget/LinearLayout;Z)V
    .locals 5
    .param p1    # Landroid/widget/LinearLayout;
    .param p2    # Z

    const v4, -0x515152

    const/high16 v3, -0x1000000

    const/4 v2, 0x0

    invoke-virtual {p1, v2}, Landroid/widget/LinearLayout;->getChildAt(I)Landroid/view/View;

    move-result-object v1

    check-cast v1, Landroid/widget/TextView;

    const/4 v2, 0x1

    invoke-virtual {p1, v2}, Landroid/widget/LinearLayout;->getChildAt(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/EditText;

    if-eqz p2, :cond_0

    invoke-virtual {v1, v3}, Landroid/widget/TextView;->setTextColor(I)V

    invoke-virtual {v0, v3}, Landroid/widget/EditText;->setTextColor(I)V

    :goto_0
    invoke-virtual {v0, p2}, Landroid/widget/EditText;->setEnabled(Z)V

    invoke-virtual {v0, p2}, Landroid/widget/EditText;->setClickable(Z)V

    invoke-virtual {v0, p2}, Landroid/widget/EditText;->setFocusable(Z)V

    invoke-virtual {v0, p2}, Landroid/widget/EditText;->setFocusableInTouchMode(Z)V

    invoke-virtual {p1, p2}, Landroid/widget/LinearLayout;->setClickable(Z)V

    return-void

    :cond_0
    invoke-virtual {v1, v4}, Landroid/widget/TextView;->setTextColor(I)V

    invoke-virtual {v0, v4}, Landroid/widget/EditText;->setTextColor(I)V

    goto :goto_0
.end method

.method public updateWireStateToUI()V
    .locals 4

    const/4 v2, 0x1

    const/4 v1, 0x0

    sget-boolean v3, Lcom/konka/systemsetting/common/Content$ethernet;->isAutoSet:Z

    if-eqz v3, :cond_0

    move v0, v1

    :goto_0
    iget-object v3, p0, Lcom/konka/systemsetting/net/NetworkWireConfig;->mItemIpAddr:Landroid/widget/LinearLayout;

    invoke-virtual {p0, v3, v0}, Lcom/konka/systemsetting/net/NetworkWireConfig;->setWireItemEnabled(Landroid/widget/LinearLayout;Z)V

    iget-object v3, p0, Lcom/konka/systemsetting/net/NetworkWireConfig;->mItemNetmask:Landroid/widget/LinearLayout;

    invoke-virtual {p0, v3, v0}, Lcom/konka/systemsetting/net/NetworkWireConfig;->setWireItemEnabled(Landroid/widget/LinearLayout;Z)V

    iget-object v3, p0, Lcom/konka/systemsetting/net/NetworkWireConfig;->mItemGateway:Landroid/widget/LinearLayout;

    invoke-virtual {p0, v3, v0}, Lcom/konka/systemsetting/net/NetworkWireConfig;->setWireItemEnabled(Landroid/widget/LinearLayout;Z)V

    iget-object v3, p0, Lcom/konka/systemsetting/net/NetworkWireConfig;->mItemDnsSvr:Landroid/widget/LinearLayout;

    invoke-virtual {p0, v3, v0}, Lcom/konka/systemsetting/net/NetworkWireConfig;->setWireItemEnabled(Landroid/widget/LinearLayout;Z)V

    sget-boolean v3, Lcom/konka/systemsetting/common/Content$ethernet;->isAutoSet:Z

    if-eqz v3, :cond_1

    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    sput-object v1, Lcom/konka/systemsetting/common/Content$ethernet;->currIpMode:Ljava/lang/Integer;

    iget-object v1, p0, Lcom/konka/systemsetting/net/NetworkWireConfig;->mItemIpMode:Landroid/widget/LinearLayout;

    const v2, 0x7f09006c    # com.konka.systemsetting.R.id.sys_network_wired_item_set

    invoke-virtual {v1, v2}, Landroid/widget/LinearLayout;->setNextFocusDownId(I)V

    iget-object v1, p0, Lcom/konka/systemsetting/net/NetworkWireConfig;->mBtnSetWire:Landroid/widget/Button;

    const v2, 0x7f090063    # com.konka.systemsetting.R.id.sys_network_wired_item_ipmode

    invoke-virtual {v1, v2}, Landroid/widget/Button;->setNextFocusUpId(I)V

    :goto_1
    iget-object v1, p0, Lcom/konka/systemsetting/net/NetworkWireConfig;->mTxtIpMode:Landroid/widget/TextView;

    sget-object v2, Lcom/konka/systemsetting/common/Content$ethernet;->IpMode:[Ljava/lang/CharSequence;

    sget-object v3, Lcom/konka/systemsetting/common/Content$ethernet;->currIpMode:Ljava/lang/Integer;

    invoke-virtual {v3}, Ljava/lang/Integer;->intValue()I

    move-result v3

    aget-object v2, v2, v3

    invoke-virtual {v1, v2}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    iget-object v1, p0, Lcom/konka/systemsetting/net/NetworkWireConfig;->mEditIpAddr:Landroid/widget/EditText;

    sget-object v2, Lcom/konka/systemsetting/common/Content$ethernet;->ipaddress:Ljava/lang/CharSequence;

    invoke-virtual {v1, v2}, Landroid/widget/EditText;->setText(Ljava/lang/CharSequence;)V

    iget-object v1, p0, Lcom/konka/systemsetting/net/NetworkWireConfig;->mEditNetmask:Landroid/widget/EditText;

    sget-object v2, Lcom/konka/systemsetting/common/Content$ethernet;->netmask:Ljava/lang/CharSequence;

    invoke-virtual {v1, v2}, Landroid/widget/EditText;->setText(Ljava/lang/CharSequence;)V

    iget-object v1, p0, Lcom/konka/systemsetting/net/NetworkWireConfig;->mEditGateway:Landroid/widget/EditText;

    sget-object v2, Lcom/konka/systemsetting/common/Content$ethernet;->gateway:Ljava/lang/CharSequence;

    invoke-virtual {v1, v2}, Landroid/widget/EditText;->setText(Ljava/lang/CharSequence;)V

    iget-object v1, p0, Lcom/konka/systemsetting/net/NetworkWireConfig;->mEditDnsSvr:Landroid/widget/EditText;

    sget-object v2, Lcom/konka/systemsetting/common/Content$ethernet;->firstdns:Ljava/lang/CharSequence;

    invoke-virtual {v1, v2}, Landroid/widget/EditText;->setText(Ljava/lang/CharSequence;)V

    return-void

    :cond_0
    move v0, v2

    goto :goto_0

    :cond_1
    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    sput-object v1, Lcom/konka/systemsetting/common/Content$ethernet;->currIpMode:Ljava/lang/Integer;

    iget-object v1, p0, Lcom/konka/systemsetting/net/NetworkWireConfig;->mItemIpMode:Landroid/widget/LinearLayout;

    const v2, 0x7f090064    # com.konka.systemsetting.R.id.sys_network_wired_item_ipaddr

    invoke-virtual {v1, v2}, Landroid/widget/LinearLayout;->setNextFocusDownId(I)V

    iget-object v1, p0, Lcom/konka/systemsetting/net/NetworkWireConfig;->mBtnSetWire:Landroid/widget/Button;

    const v2, 0x7f09006a    # com.konka.systemsetting.R.id.sys_network_wired_item_dnsserver

    invoke-virtual {v1, v2}, Landroid/widget/Button;->setNextFocusUpId(I)V

    goto :goto_1
.end method
