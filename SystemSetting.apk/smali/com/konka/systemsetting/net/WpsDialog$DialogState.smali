.class final enum Lcom/konka/systemsetting/net/WpsDialog$DialogState;
.super Ljava/lang/Enum;
.source "WpsDialog.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/konka/systemsetting/net/WpsDialog;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x401a
    name = "DialogState"
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Enum",
        "<",
        "Lcom/konka/systemsetting/net/WpsDialog$DialogState;",
        ">;"
    }
.end annotation


# static fields
.field public static final enum CONNECTED:Lcom/konka/systemsetting/net/WpsDialog$DialogState;

.field private static final synthetic ENUM$VALUES:[Lcom/konka/systemsetting/net/WpsDialog$DialogState;

.field public static final enum WPS_COMPLETE:Lcom/konka/systemsetting/net/WpsDialog$DialogState;

.field public static final enum WPS_FAILED:Lcom/konka/systemsetting/net/WpsDialog$DialogState;

.field public static final enum WPS_INIT:Lcom/konka/systemsetting/net/WpsDialog$DialogState;

.field public static final enum WPS_START:Lcom/konka/systemsetting/net/WpsDialog$DialogState;


# direct methods
.method static constructor <clinit>()V
    .locals 7

    const/4 v6, 0x4

    const/4 v5, 0x3

    const/4 v4, 0x2

    const/4 v3, 0x1

    const/4 v2, 0x0

    new-instance v0, Lcom/konka/systemsetting/net/WpsDialog$DialogState;

    const-string v1, "WPS_INIT"

    invoke-direct {v0, v1, v2}, Lcom/konka/systemsetting/net/WpsDialog$DialogState;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/konka/systemsetting/net/WpsDialog$DialogState;->WPS_INIT:Lcom/konka/systemsetting/net/WpsDialog$DialogState;

    new-instance v0, Lcom/konka/systemsetting/net/WpsDialog$DialogState;

    const-string v1, "WPS_START"

    invoke-direct {v0, v1, v3}, Lcom/konka/systemsetting/net/WpsDialog$DialogState;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/konka/systemsetting/net/WpsDialog$DialogState;->WPS_START:Lcom/konka/systemsetting/net/WpsDialog$DialogState;

    new-instance v0, Lcom/konka/systemsetting/net/WpsDialog$DialogState;

    const-string v1, "WPS_COMPLETE"

    invoke-direct {v0, v1, v4}, Lcom/konka/systemsetting/net/WpsDialog$DialogState;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/konka/systemsetting/net/WpsDialog$DialogState;->WPS_COMPLETE:Lcom/konka/systemsetting/net/WpsDialog$DialogState;

    new-instance v0, Lcom/konka/systemsetting/net/WpsDialog$DialogState;

    const-string v1, "CONNECTED"

    invoke-direct {v0, v1, v5}, Lcom/konka/systemsetting/net/WpsDialog$DialogState;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/konka/systemsetting/net/WpsDialog$DialogState;->CONNECTED:Lcom/konka/systemsetting/net/WpsDialog$DialogState;

    new-instance v0, Lcom/konka/systemsetting/net/WpsDialog$DialogState;

    const-string v1, "WPS_FAILED"

    invoke-direct {v0, v1, v6}, Lcom/konka/systemsetting/net/WpsDialog$DialogState;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/konka/systemsetting/net/WpsDialog$DialogState;->WPS_FAILED:Lcom/konka/systemsetting/net/WpsDialog$DialogState;

    const/4 v0, 0x5

    new-array v0, v0, [Lcom/konka/systemsetting/net/WpsDialog$DialogState;

    sget-object v1, Lcom/konka/systemsetting/net/WpsDialog$DialogState;->WPS_INIT:Lcom/konka/systemsetting/net/WpsDialog$DialogState;

    aput-object v1, v0, v2

    sget-object v1, Lcom/konka/systemsetting/net/WpsDialog$DialogState;->WPS_START:Lcom/konka/systemsetting/net/WpsDialog$DialogState;

    aput-object v1, v0, v3

    sget-object v1, Lcom/konka/systemsetting/net/WpsDialog$DialogState;->WPS_COMPLETE:Lcom/konka/systemsetting/net/WpsDialog$DialogState;

    aput-object v1, v0, v4

    sget-object v1, Lcom/konka/systemsetting/net/WpsDialog$DialogState;->CONNECTED:Lcom/konka/systemsetting/net/WpsDialog$DialogState;

    aput-object v1, v0, v5

    sget-object v1, Lcom/konka/systemsetting/net/WpsDialog$DialogState;->WPS_FAILED:Lcom/konka/systemsetting/net/WpsDialog$DialogState;

    aput-object v1, v0, v6

    sput-object v0, Lcom/konka/systemsetting/net/WpsDialog$DialogState;->ENUM$VALUES:[Lcom/konka/systemsetting/net/WpsDialog$DialogState;

    return-void
.end method

.method private constructor <init>(Ljava/lang/String;I)V
    .locals 0

    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    return-void
.end method

.method public static valueOf(Ljava/lang/String;)Lcom/konka/systemsetting/net/WpsDialog$DialogState;
    .locals 1

    const-class v0, Lcom/konka/systemsetting/net/WpsDialog$DialogState;

    invoke-static {v0, p0}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;

    move-result-object v0

    check-cast v0, Lcom/konka/systemsetting/net/WpsDialog$DialogState;

    return-object v0
.end method

.method public static values()[Lcom/konka/systemsetting/net/WpsDialog$DialogState;
    .locals 4

    const/4 v3, 0x0

    sget-object v0, Lcom/konka/systemsetting/net/WpsDialog$DialogState;->ENUM$VALUES:[Lcom/konka/systemsetting/net/WpsDialog$DialogState;

    array-length v1, v0

    new-array v2, v1, [Lcom/konka/systemsetting/net/WpsDialog$DialogState;

    invoke-static {v0, v3, v2, v3, v1}, Ljava/lang/System;->arraycopy(Ljava/lang/Object;ILjava/lang/Object;II)V

    return-object v2
.end method
