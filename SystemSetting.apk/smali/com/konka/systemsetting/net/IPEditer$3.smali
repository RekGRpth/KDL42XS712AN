.class Lcom/konka/systemsetting/net/IPEditer$3;
.super Ljava/lang/Object;
.source "IPEditer.java"

# interfaces
.implements Landroid/view/View$OnKeyListener;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/konka/systemsetting/net/IPEditer;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/konka/systemsetting/net/IPEditer;


# direct methods
.method constructor <init>(Lcom/konka/systemsetting/net/IPEditer;)V
    .locals 0

    iput-object p1, p0, Lcom/konka/systemsetting/net/IPEditer$3;->this$0:Lcom/konka/systemsetting/net/IPEditer;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onKey(Landroid/view/View;ILandroid/view/KeyEvent;)Z
    .locals 4
    .param p1    # Landroid/view/View;
    .param p2    # I
    .param p3    # Landroid/view/KeyEvent;

    const/4 v0, 0x1

    const/4 v1, 0x0

    iget-object v2, p0, Lcom/konka/systemsetting/net/IPEditer$3;->this$0:Lcom/konka/systemsetting/net/IPEditer;

    # invokes: Lcom/konka/systemsetting/net/IPEditer;->isNumberKey(I)Z
    invoke-static {v2, p2}, Lcom/konka/systemsetting/net/IPEditer;->access$4(Lcom/konka/systemsetting/net/IPEditer;I)Z

    move-result v2

    if-eqz v2, :cond_1

    invoke-virtual {p3}, Landroid/view/KeyEvent;->getAction()I

    move-result v2

    if-nez v2, :cond_1

    iget-object v2, p0, Lcom/konka/systemsetting/net/IPEditer$3;->this$0:Lcom/konka/systemsetting/net/IPEditer;

    # invokes: Lcom/konka/systemsetting/net/IPEditer;->isSelected()Z
    invoke-static {v2}, Lcom/konka/systemsetting/net/IPEditer;->access$5(Lcom/konka/systemsetting/net/IPEditer;)Z

    move-result v2

    if-eqz v2, :cond_0

    iget-object v2, p0, Lcom/konka/systemsetting/net/IPEditer$3;->this$0:Lcom/konka/systemsetting/net/IPEditer;

    iget-object v3, p0, Lcom/konka/systemsetting/net/IPEditer$3;->this$0:Lcom/konka/systemsetting/net/IPEditer;

    # invokes: Lcom/konka/systemsetting/net/IPEditer;->getChar(I)C
    invoke-static {v3, p2}, Lcom/konka/systemsetting/net/IPEditer;->access$6(Lcom/konka/systemsetting/net/IPEditer;I)C

    move-result v3

    invoke-static {v3}, Ljava/lang/String;->valueOf(C)Ljava/lang/String;

    move-result-object v3

    # invokes: Lcom/konka/systemsetting/net/IPEditer;->replaceCurrField(Ljava/lang/String;)V
    invoke-static {v2, v3}, Lcom/konka/systemsetting/net/IPEditer;->access$7(Lcom/konka/systemsetting/net/IPEditer;Ljava/lang/String;)V

    :cond_0
    iget-object v2, p0, Lcom/konka/systemsetting/net/IPEditer$3;->this$0:Lcom/konka/systemsetting/net/IPEditer;

    iget-object v3, p0, Lcom/konka/systemsetting/net/IPEditer$3;->this$0:Lcom/konka/systemsetting/net/IPEditer;

    # invokes: Lcom/konka/systemsetting/net/IPEditer;->getChar(I)C
    invoke-static {v3, p2}, Lcom/konka/systemsetting/net/IPEditer;->access$6(Lcom/konka/systemsetting/net/IPEditer;I)C

    move-result v3

    # invokes: Lcom/konka/systemsetting/net/IPEditer;->fieldAppend(C)V
    invoke-static {v2, v3}, Lcom/konka/systemsetting/net/IPEditer;->access$8(Lcom/konka/systemsetting/net/IPEditer;C)V

    iget-object v2, p0, Lcom/konka/systemsetting/net/IPEditer$3;->this$0:Lcom/konka/systemsetting/net/IPEditer;

    # getter for: Lcom/konka/systemsetting/net/IPEditer;->mEditText:Landroid/widget/EditText;
    invoke-static {v2}, Lcom/konka/systemsetting/net/IPEditer;->access$9(Lcom/konka/systemsetting/net/IPEditer;)Landroid/widget/EditText;

    move-result-object v2

    iget-object v3, p0, Lcom/konka/systemsetting/net/IPEditer$3;->this$0:Lcom/konka/systemsetting/net/IPEditer;

    # getter for: Lcom/konka/systemsetting/net/IPEditer;->strIP:Ljava/lang/String;
    invoke-static {v3}, Lcom/konka/systemsetting/net/IPEditer;->access$2(Lcom/konka/systemsetting/net/IPEditer;)Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Landroid/widget/EditText;->setText(Ljava/lang/CharSequence;)V

    :cond_1
    sparse-switch p2, :sswitch_data_0

    :cond_2
    move v0, v1

    :goto_0
    return v0

    :sswitch_0
    invoke-virtual {p3}, Landroid/view/KeyEvent;->getAction()I

    move-result v2

    if-nez v2, :cond_2

    iget-object v2, p0, Lcom/konka/systemsetting/net/IPEditer$3;->this$0:Lcom/konka/systemsetting/net/IPEditer;

    # invokes: Lcom/konka/systemsetting/net/IPEditer;->isSelected()Z
    invoke-static {v2}, Lcom/konka/systemsetting/net/IPEditer;->access$5(Lcom/konka/systemsetting/net/IPEditer;)Z

    move-result v2

    if-nez v2, :cond_3

    iget-object v2, p0, Lcom/konka/systemsetting/net/IPEditer$3;->this$0:Lcom/konka/systemsetting/net/IPEditer;

    # getter for: Lcom/konka/systemsetting/net/IPEditer;->mContext:Landroid/app/Activity;
    invoke-static {v2}, Lcom/konka/systemsetting/net/IPEditer;->access$10(Lcom/konka/systemsetting/net/IPEditer;)Landroid/app/Activity;

    move-result-object v2

    const-string v3, "Back !seleted"

    invoke-static {v2, v3, v1}, Landroid/widget/Toast;->makeText(Landroid/content/Context;Ljava/lang/CharSequence;I)Landroid/widget/Toast;

    move-result-object v1

    invoke-virtual {v1}, Landroid/widget/Toast;->show()V

    goto :goto_0

    :cond_3
    iget-object v2, p0, Lcom/konka/systemsetting/net/IPEditer$3;->this$0:Lcom/konka/systemsetting/net/IPEditer;

    # getter for: Lcom/konka/systemsetting/net/IPEditer;->mContext:Landroid/app/Activity;
    invoke-static {v2}, Lcom/konka/systemsetting/net/IPEditer;->access$10(Lcom/konka/systemsetting/net/IPEditer;)Landroid/app/Activity;

    move-result-object v2

    const-string v3, "Back seleted"

    invoke-static {v2, v3, v1}, Landroid/widget/Toast;->makeText(Landroid/content/Context;Ljava/lang/CharSequence;I)Landroid/widget/Toast;

    move-result-object v1

    invoke-virtual {v1}, Landroid/widget/Toast;->show()V

    goto :goto_0

    :sswitch_1
    invoke-virtual {p3}, Landroid/view/KeyEvent;->getAction()I

    move-result v2

    if-nez v2, :cond_2

    iget-object v1, p0, Lcom/konka/systemsetting/net/IPEditer$3;->this$0:Lcom/konka/systemsetting/net/IPEditer;

    # invokes: Lcom/konka/systemsetting/net/IPEditer;->keyBoardLeft()V
    invoke-static {v1}, Lcom/konka/systemsetting/net/IPEditer;->access$11(Lcom/konka/systemsetting/net/IPEditer;)V

    goto :goto_0

    :sswitch_2
    invoke-virtual {p3}, Landroid/view/KeyEvent;->getAction()I

    move-result v2

    if-nez v2, :cond_2

    iget-object v1, p0, Lcom/konka/systemsetting/net/IPEditer$3;->this$0:Lcom/konka/systemsetting/net/IPEditer;

    # invokes: Lcom/konka/systemsetting/net/IPEditer;->keyBoardRight()V
    invoke-static {v1}, Lcom/konka/systemsetting/net/IPEditer;->access$12(Lcom/konka/systemsetting/net/IPEditer;)V

    goto :goto_0

    :sswitch_3
    invoke-virtual {p3}, Landroid/view/KeyEvent;->getAction()I

    move-result v2

    if-nez v2, :cond_2

    iget-object v2, p0, Lcom/konka/systemsetting/net/IPEditer$3;->this$0:Lcom/konka/systemsetting/net/IPEditer;

    # invokes: Lcom/konka/systemsetting/net/IPEditer;->isSelected()Z
    invoke-static {v2}, Lcom/konka/systemsetting/net/IPEditer;->access$5(Lcom/konka/systemsetting/net/IPEditer;)Z

    move-result v2

    if-eqz v2, :cond_4

    iget-object v1, p0, Lcom/konka/systemsetting/net/IPEditer$3;->this$0:Lcom/konka/systemsetting/net/IPEditer;

    # getter for: Lcom/konka/systemsetting/net/IPEditer;->mEditText:Landroid/widget/EditText;
    invoke-static {v1}, Lcom/konka/systemsetting/net/IPEditer;->access$9(Lcom/konka/systemsetting/net/IPEditer;)Landroid/widget/EditText;

    move-result-object v1

    iget-object v2, p0, Lcom/konka/systemsetting/net/IPEditer$3;->this$0:Lcom/konka/systemsetting/net/IPEditer;

    # getter for: Lcom/konka/systemsetting/net/IPEditer;->mCurrPos:I
    invoke-static {v2}, Lcom/konka/systemsetting/net/IPEditer;->access$13(Lcom/konka/systemsetting/net/IPEditer;)I

    move-result v2

    invoke-virtual {v1, v2}, Landroid/widget/EditText;->setSelection(I)V

    goto :goto_0

    :cond_4
    iget-object v2, p0, Lcom/konka/systemsetting/net/IPEditer$3;->this$0:Lcom/konka/systemsetting/net/IPEditer;

    # getter for: Lcom/konka/systemsetting/net/IPEditer;->mContext:Landroid/app/Activity;
    invoke-static {v2}, Lcom/konka/systemsetting/net/IPEditer;->access$10(Lcom/konka/systemsetting/net/IPEditer;)Landroid/app/Activity;

    move-result-object v2

    const-string v3, "finish"

    invoke-static {v2, v3, v1}, Landroid/widget/Toast;->makeText(Landroid/content/Context;Ljava/lang/CharSequence;I)Landroid/widget/Toast;

    move-result-object v1

    invoke-virtual {v1}, Landroid/widget/Toast;->show()V

    goto :goto_0

    :sswitch_data_0
    .sparse-switch
        0x4 -> :sswitch_0
        0x15 -> :sswitch_1
        0x16 -> :sswitch_2
        0x17 -> :sswitch_3
        0x42 -> :sswitch_3
        0x9e -> :sswitch_1
    .end sparse-switch
.end method
