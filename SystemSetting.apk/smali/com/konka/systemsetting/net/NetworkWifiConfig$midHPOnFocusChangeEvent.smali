.class Lcom/konka/systemsetting/net/NetworkWifiConfig$midHPOnFocusChangeEvent;
.super Ljava/lang/Object;
.source "NetworkWifiConfig.java"

# interfaces
.implements Landroid/view/View$OnFocusChangeListener;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/konka/systemsetting/net/NetworkWifiConfig;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x2
    name = "midHPOnFocusChangeEvent"
.end annotation


# instance fields
.field final synthetic this$0:Lcom/konka/systemsetting/net/NetworkWifiConfig;


# direct methods
.method private constructor <init>(Lcom/konka/systemsetting/net/NetworkWifiConfig;)V
    .locals 0

    iput-object p1, p0, Lcom/konka/systemsetting/net/NetworkWifiConfig$midHPOnFocusChangeEvent;->this$0:Lcom/konka/systemsetting/net/NetworkWifiConfig;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method synthetic constructor <init>(Lcom/konka/systemsetting/net/NetworkWifiConfig;Lcom/konka/systemsetting/net/NetworkWifiConfig$midHPOnFocusChangeEvent;)V
    .locals 0

    invoke-direct {p0, p1}, Lcom/konka/systemsetting/net/NetworkWifiConfig$midHPOnFocusChangeEvent;-><init>(Lcom/konka/systemsetting/net/NetworkWifiConfig;)V

    return-void
.end method


# virtual methods
.method public onFocusChange(Landroid/view/View;Z)V
    .locals 4
    .param p1    # Landroid/view/View;
    .param p2    # Z

    const/4 v3, 0x1

    if-eqz p2, :cond_1

    iget-object v0, p0, Lcom/konka/systemsetting/net/NetworkWifiConfig$midHPOnFocusChangeEvent;->this$0:Lcom/konka/systemsetting/net/NetworkWifiConfig;

    # getter for: Lcom/konka/systemsetting/net/NetworkWifiConfig;->mContext:Lcom/konka/systemsetting/MainActivity;
    invoke-static {v0}, Lcom/konka/systemsetting/net/NetworkWifiConfig;->access$4(Lcom/konka/systemsetting/net/NetworkWifiConfig;)Lcom/konka/systemsetting/MainActivity;

    move-result-object v0

    iget-object v0, v0, Lcom/konka/systemsetting/MainActivity;->m_networkPageManager:Lcom/konka/systemsetting/net/NetworkPageManager;

    iget-object v1, p0, Lcom/konka/systemsetting/net/NetworkWifiConfig$midHPOnFocusChangeEvent;->this$0:Lcom/konka/systemsetting/net/NetworkWifiConfig;

    # getter for: Lcom/konka/systemsetting/net/NetworkWifiConfig;->mContext:Lcom/konka/systemsetting/MainActivity;
    invoke-static {v1}, Lcom/konka/systemsetting/net/NetworkWifiConfig;->access$4(Lcom/konka/systemsetting/net/NetworkWifiConfig;)Lcom/konka/systemsetting/MainActivity;

    move-result-object v1

    iget-object v1, v1, Lcom/konka/systemsetting/MainActivity;->m_networkPageManager:Lcom/konka/systemsetting/net/NetworkPageManager;

    invoke-virtual {v1}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    invoke-static {v3}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    iget-object v2, p0, Lcom/konka/systemsetting/net/NetworkWifiConfig$midHPOnFocusChangeEvent;->this$0:Lcom/konka/systemsetting/net/NetworkWifiConfig;

    # getter for: Lcom/konka/systemsetting/net/NetworkWifiConfig;->mContext:Lcom/konka/systemsetting/MainActivity;
    invoke-static {v2}, Lcom/konka/systemsetting/net/NetworkWifiConfig;->access$4(Lcom/konka/systemsetting/net/NetworkWifiConfig;)Lcom/konka/systemsetting/MainActivity;

    move-result-object v2

    iget-object v2, v2, Lcom/konka/systemsetting/MainActivity;->m_networkPageManager:Lcom/konka/systemsetting/net/NetworkPageManager;

    invoke-virtual {v2}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    invoke-static {v3}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Lcom/konka/systemsetting/net/NetworkPageManager;->setLastFocus(Ljava/lang/Integer;Ljava/lang/Integer;)V

    const v0, 0x7f020030    # com.konka.systemsetting.R.drawable.syssettingthirditemmidfocus

    invoke-virtual {p1, v0}, Landroid/view/View;->setBackgroundResource(I)V

    iget-object v0, p0, Lcom/konka/systemsetting/net/NetworkWifiConfig$midHPOnFocusChangeEvent;->this$0:Lcom/konka/systemsetting/net/NetworkWifiConfig;

    # getter for: Lcom/konka/systemsetting/net/NetworkWifiConfig;->mResults:Ljava/util/List;
    invoke-static {v0}, Lcom/konka/systemsetting/net/NetworkWifiConfig;->access$13(Lcom/konka/systemsetting/net/NetworkWifiConfig;)Ljava/util/List;

    move-result-object v0

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/konka/systemsetting/net/NetworkWifiConfig$midHPOnFocusChangeEvent;->this$0:Lcom/konka/systemsetting/net/NetworkWifiConfig;

    # getter for: Lcom/konka/systemsetting/net/NetworkWifiConfig;->mResults:Ljava/util/List;
    invoke-static {v0}, Lcom/konka/systemsetting/net/NetworkWifiConfig;->access$13(Lcom/konka/systemsetting/net/NetworkWifiConfig;)Ljava/util/List;

    move-result-object v0

    invoke-interface {v0}, Ljava/util/List;->size()I

    move-result v0

    iget-object v1, p0, Lcom/konka/systemsetting/net/NetworkWifiConfig$midHPOnFocusChangeEvent;->this$0:Lcom/konka/systemsetting/net/NetworkWifiConfig;

    # getter for: Lcom/konka/systemsetting/net/NetworkWifiConfig;->iCurrFirstIndex:I
    invoke-static {v1}, Lcom/konka/systemsetting/net/NetworkWifiConfig;->access$14(Lcom/konka/systemsetting/net/NetworkWifiConfig;)I

    move-result v1

    iget-object v2, p0, Lcom/konka/systemsetting/net/NetworkWifiConfig$midHPOnFocusChangeEvent;->this$0:Lcom/konka/systemsetting/net/NetworkWifiConfig;

    # getter for: Lcom/konka/systemsetting/net/NetworkWifiConfig;->iCurrSeletedIndex:I
    invoke-static {v2}, Lcom/konka/systemsetting/net/NetworkWifiConfig;->access$9(Lcom/konka/systemsetting/net/NetworkWifiConfig;)I

    move-result v2

    add-int/2addr v1, v2

    if-le v0, v1, :cond_0

    iget-object v1, p0, Lcom/konka/systemsetting/net/NetworkWifiConfig$midHPOnFocusChangeEvent;->this$0:Lcom/konka/systemsetting/net/NetworkWifiConfig;

    iget-object v0, p0, Lcom/konka/systemsetting/net/NetworkWifiConfig$midHPOnFocusChangeEvent;->this$0:Lcom/konka/systemsetting/net/NetworkWifiConfig;

    # getter for: Lcom/konka/systemsetting/net/NetworkWifiConfig;->mResults:Ljava/util/List;
    invoke-static {v0}, Lcom/konka/systemsetting/net/NetworkWifiConfig;->access$13(Lcom/konka/systemsetting/net/NetworkWifiConfig;)Ljava/util/List;

    move-result-object v0

    iget-object v2, p0, Lcom/konka/systemsetting/net/NetworkWifiConfig$midHPOnFocusChangeEvent;->this$0:Lcom/konka/systemsetting/net/NetworkWifiConfig;

    # getter for: Lcom/konka/systemsetting/net/NetworkWifiConfig;->iCurrFirstIndex:I
    invoke-static {v2}, Lcom/konka/systemsetting/net/NetworkWifiConfig;->access$14(Lcom/konka/systemsetting/net/NetworkWifiConfig;)I

    move-result v2

    iget-object v3, p0, Lcom/konka/systemsetting/net/NetworkWifiConfig$midHPOnFocusChangeEvent;->this$0:Lcom/konka/systemsetting/net/NetworkWifiConfig;

    # getter for: Lcom/konka/systemsetting/net/NetworkWifiConfig;->iCurrSeletedIndex:I
    invoke-static {v3}, Lcom/konka/systemsetting/net/NetworkWifiConfig;->access$9(Lcom/konka/systemsetting/net/NetworkWifiConfig;)I

    move-result v3

    add-int/2addr v2, v3

    invoke-interface {v0, v2}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/net/wifi/ScanResult;

    invoke-static {v1, v0}, Lcom/konka/systemsetting/net/NetworkWifiConfig;->access$15(Lcom/konka/systemsetting/net/NetworkWifiConfig;Landroid/net/wifi/ScanResult;)V

    iget-object v0, p0, Lcom/konka/systemsetting/net/NetworkWifiConfig$midHPOnFocusChangeEvent;->this$0:Lcom/konka/systemsetting/net/NetworkWifiConfig;

    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "the current focus HP===:"

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    iget-object v2, p0, Lcom/konka/systemsetting/net/NetworkWifiConfig$midHPOnFocusChangeEvent;->this$0:Lcom/konka/systemsetting/net/NetworkWifiConfig;

    # getter for: Lcom/konka/systemsetting/net/NetworkWifiConfig;->srCurLinkHP:Landroid/net/wifi/ScanResult;
    invoke-static {v2}, Lcom/konka/systemsetting/net/NetworkWifiConfig;->access$16(Lcom/konka/systemsetting/net/NetworkWifiConfig;)Landroid/net/wifi/ScanResult;

    move-result-object v2

    iget-object v2, v2, Landroid/net/wifi/ScanResult;->SSID:Ljava/lang/String;

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/konka/systemsetting/net/NetworkWifiConfig;->log(Ljava/lang/String;)V

    :cond_0
    :goto_0
    return-void

    :cond_1
    const v0, 0x7f02002f    # com.konka.systemsetting.R.drawable.syssettingthirditemmid

    invoke-virtual {p1, v0}, Landroid/view/View;->setBackgroundResource(I)V

    goto :goto_0
.end method
