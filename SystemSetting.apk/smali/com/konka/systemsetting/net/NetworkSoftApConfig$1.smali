.class Lcom/konka/systemsetting/net/NetworkSoftApConfig$1;
.super Landroid/content/BroadcastReceiver;
.source "NetworkSoftApConfig.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/konka/systemsetting/net/NetworkSoftApConfig;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/konka/systemsetting/net/NetworkSoftApConfig;


# direct methods
.method constructor <init>(Lcom/konka/systemsetting/net/NetworkSoftApConfig;)V
    .locals 0

    iput-object p1, p0, Lcom/konka/systemsetting/net/NetworkSoftApConfig$1;->this$0:Lcom/konka/systemsetting/net/NetworkSoftApConfig;

    invoke-direct {p0}, Landroid/content/BroadcastReceiver;-><init>()V

    return-void
.end method


# virtual methods
.method public onReceive(Landroid/content/Context;Landroid/content/Intent;)V
    .locals 3
    .param p1    # Landroid/content/Context;
    .param p2    # Landroid/content/Intent;

    const-string v0, "android.net.wifi.WIFI_AP_STATE_CHANGED"

    invoke-virtual {p2}, Landroid/content/Intent;->getAction()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/konka/systemsetting/net/NetworkSoftApConfig$1;->this$0:Lcom/konka/systemsetting/net/NetworkSoftApConfig;

    const-string v1, "wifi_state"

    const/16 v2, 0xe

    invoke-virtual {p2, v1, v2}, Landroid/content/Intent;->getIntExtra(Ljava/lang/String;I)I

    move-result v1

    # invokes: Lcom/konka/systemsetting/net/NetworkSoftApConfig;->handleWifiApStateChanged(I)V
    invoke-static {v0, v1}, Lcom/konka/systemsetting/net/NetworkSoftApConfig;->access$0(Lcom/konka/systemsetting/net/NetworkSoftApConfig;I)V

    :cond_0
    return-void
.end method
