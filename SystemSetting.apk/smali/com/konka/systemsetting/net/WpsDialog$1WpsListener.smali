.class Lcom/konka/systemsetting/net/WpsDialog$1WpsListener;
.super Ljava/lang/Object;
.source "WpsDialog.java"

# interfaces
.implements Landroid/net/wifi/WifiManager$WpsListener;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/konka/systemsetting/net/WpsDialog;-><init>(Landroid/content/Context;I)V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = "WpsListener"
.end annotation


# instance fields
.field final synthetic this$0:Lcom/konka/systemsetting/net/WpsDialog;


# direct methods
.method constructor <init>(Lcom/konka/systemsetting/net/WpsDialog;)V
    .locals 0

    iput-object p1, p0, Lcom/konka/systemsetting/net/WpsDialog$1WpsListener;->this$0:Lcom/konka/systemsetting/net/WpsDialog;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onCompletion()V
    .locals 4

    iget-object v0, p0, Lcom/konka/systemsetting/net/WpsDialog$1WpsListener;->this$0:Lcom/konka/systemsetting/net/WpsDialog;

    sget-object v1, Lcom/konka/systemsetting/net/WpsDialog$DialogState;->WPS_COMPLETE:Lcom/konka/systemsetting/net/WpsDialog$DialogState;

    iget-object v2, p0, Lcom/konka/systemsetting/net/WpsDialog$1WpsListener;->this$0:Lcom/konka/systemsetting/net/WpsDialog;

    # getter for: Lcom/konka/systemsetting/net/WpsDialog;->mContext:Landroid/content/Context;
    invoke-static {v2}, Lcom/konka/systemsetting/net/WpsDialog;->access$0(Lcom/konka/systemsetting/net/WpsDialog;)Landroid/content/Context;

    move-result-object v2

    const v3, 0x7f060157    # com.konka.systemsetting.R.string.wifi_wps_complete

    invoke-virtual {v2, v3}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v2

    # invokes: Lcom/konka/systemsetting/net/WpsDialog;->updateDialog(Lcom/konka/systemsetting/net/WpsDialog$DialogState;Ljava/lang/String;)V
    invoke-static {v0, v1, v2}, Lcom/konka/systemsetting/net/WpsDialog;->access$1(Lcom/konka/systemsetting/net/WpsDialog;Lcom/konka/systemsetting/net/WpsDialog$DialogState;Ljava/lang/String;)V

    return-void
.end method

.method public onFailure(I)V
    .locals 3
    .param p1    # I

    packed-switch p1, :pswitch_data_0

    :pswitch_0
    iget-object v1, p0, Lcom/konka/systemsetting/net/WpsDialog$1WpsListener;->this$0:Lcom/konka/systemsetting/net/WpsDialog;

    # getter for: Lcom/konka/systemsetting/net/WpsDialog;->mContext:Landroid/content/Context;
    invoke-static {v1}, Lcom/konka/systemsetting/net/WpsDialog;->access$0(Lcom/konka/systemsetting/net/WpsDialog;)Landroid/content/Context;

    move-result-object v1

    const v2, 0x7f06015c    # com.konka.systemsetting.R.string.wifi_wps_failed_generic

    invoke-virtual {v1, v2}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v0

    :goto_0
    iget-object v1, p0, Lcom/konka/systemsetting/net/WpsDialog$1WpsListener;->this$0:Lcom/konka/systemsetting/net/WpsDialog;

    sget-object v2, Lcom/konka/systemsetting/net/WpsDialog$DialogState;->WPS_FAILED:Lcom/konka/systemsetting/net/WpsDialog$DialogState;

    # invokes: Lcom/konka/systemsetting/net/WpsDialog;->updateDialog(Lcom/konka/systemsetting/net/WpsDialog$DialogState;Ljava/lang/String;)V
    invoke-static {v1, v2, v0}, Lcom/konka/systemsetting/net/WpsDialog;->access$1(Lcom/konka/systemsetting/net/WpsDialog;Lcom/konka/systemsetting/net/WpsDialog$DialogState;Ljava/lang/String;)V

    return-void

    :pswitch_1
    iget-object v1, p0, Lcom/konka/systemsetting/net/WpsDialog$1WpsListener;->this$0:Lcom/konka/systemsetting/net/WpsDialog;

    # getter for: Lcom/konka/systemsetting/net/WpsDialog;->mContext:Landroid/content/Context;
    invoke-static {v1}, Lcom/konka/systemsetting/net/WpsDialog;->access$0(Lcom/konka/systemsetting/net/WpsDialog;)Landroid/content/Context;

    move-result-object v1

    const v2, 0x7f060158    # com.konka.systemsetting.R.string.wifi_wps_failed_overlap

    invoke-virtual {v1, v2}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v0

    goto :goto_0

    :pswitch_2
    iget-object v1, p0, Lcom/konka/systemsetting/net/WpsDialog$1WpsListener;->this$0:Lcom/konka/systemsetting/net/WpsDialog;

    # getter for: Lcom/konka/systemsetting/net/WpsDialog;->mContext:Landroid/content/Context;
    invoke-static {v1}, Lcom/konka/systemsetting/net/WpsDialog;->access$0(Lcom/konka/systemsetting/net/WpsDialog;)Landroid/content/Context;

    move-result-object v1

    const v2, 0x7f060159    # com.konka.systemsetting.R.string.wifi_wps_failed_wep

    invoke-virtual {v1, v2}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v0

    goto :goto_0

    :pswitch_3
    iget-object v1, p0, Lcom/konka/systemsetting/net/WpsDialog$1WpsListener;->this$0:Lcom/konka/systemsetting/net/WpsDialog;

    # getter for: Lcom/konka/systemsetting/net/WpsDialog;->mContext:Landroid/content/Context;
    invoke-static {v1}, Lcom/konka/systemsetting/net/WpsDialog;->access$0(Lcom/konka/systemsetting/net/WpsDialog;)Landroid/content/Context;

    move-result-object v1

    const v2, 0x7f06015a    # com.konka.systemsetting.R.string.wifi_wps_failed_tkip

    invoke-virtual {v1, v2}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v0

    goto :goto_0

    :pswitch_4
    iget-object v1, p0, Lcom/konka/systemsetting/net/WpsDialog$1WpsListener;->this$0:Lcom/konka/systemsetting/net/WpsDialog;

    # getter for: Lcom/konka/systemsetting/net/WpsDialog;->mContext:Landroid/content/Context;
    invoke-static {v1}, Lcom/konka/systemsetting/net/WpsDialog;->access$0(Lcom/konka/systemsetting/net/WpsDialog;)Landroid/content/Context;

    move-result-object v1

    const v2, 0x7f06015b    # com.konka.systemsetting.R.string.wifi_wps_in_progress

    invoke-virtual {v1, v2}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v0

    goto :goto_0

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_4
        :pswitch_0
        :pswitch_1
        :pswitch_2
        :pswitch_3
    .end packed-switch
.end method

.method public onStartSuccess(Ljava/lang/String;)V
    .locals 5
    .param p1    # Ljava/lang/String;

    if-eqz p1, :cond_0

    iget-object v0, p0, Lcom/konka/systemsetting/net/WpsDialog$1WpsListener;->this$0:Lcom/konka/systemsetting/net/WpsDialog;

    sget-object v1, Lcom/konka/systemsetting/net/WpsDialog$DialogState;->WPS_START:Lcom/konka/systemsetting/net/WpsDialog$DialogState;

    iget-object v2, p0, Lcom/konka/systemsetting/net/WpsDialog$1WpsListener;->this$0:Lcom/konka/systemsetting/net/WpsDialog;

    # getter for: Lcom/konka/systemsetting/net/WpsDialog;->mContext:Landroid/content/Context;
    invoke-static {v2}, Lcom/konka/systemsetting/net/WpsDialog;->access$0(Lcom/konka/systemsetting/net/WpsDialog;)Landroid/content/Context;

    move-result-object v2

    const v3, 0x7f060155    # com.konka.systemsetting.R.string.wifi_wps_onstart_pin

    invoke-virtual {v2, v3}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v2

    const/4 v3, 0x1

    new-array v3, v3, [Ljava/lang/Object;

    const/4 v4, 0x0

    aput-object p1, v3, v4

    invoke-static {v2, v3}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v2

    # invokes: Lcom/konka/systemsetting/net/WpsDialog;->updateDialog(Lcom/konka/systemsetting/net/WpsDialog$DialogState;Ljava/lang/String;)V
    invoke-static {v0, v1, v2}, Lcom/konka/systemsetting/net/WpsDialog;->access$1(Lcom/konka/systemsetting/net/WpsDialog;Lcom/konka/systemsetting/net/WpsDialog$DialogState;Ljava/lang/String;)V

    :goto_0
    return-void

    :cond_0
    iget-object v0, p0, Lcom/konka/systemsetting/net/WpsDialog$1WpsListener;->this$0:Lcom/konka/systemsetting/net/WpsDialog;

    sget-object v1, Lcom/konka/systemsetting/net/WpsDialog$DialogState;->WPS_START:Lcom/konka/systemsetting/net/WpsDialog$DialogState;

    iget-object v2, p0, Lcom/konka/systemsetting/net/WpsDialog$1WpsListener;->this$0:Lcom/konka/systemsetting/net/WpsDialog;

    # getter for: Lcom/konka/systemsetting/net/WpsDialog;->mContext:Landroid/content/Context;
    invoke-static {v2}, Lcom/konka/systemsetting/net/WpsDialog;->access$0(Lcom/konka/systemsetting/net/WpsDialog;)Landroid/content/Context;

    move-result-object v2

    const v3, 0x7f060156    # com.konka.systemsetting.R.string.wifi_wps_onstart_pbc

    invoke-virtual {v2, v3}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v2

    # invokes: Lcom/konka/systemsetting/net/WpsDialog;->updateDialog(Lcom/konka/systemsetting/net/WpsDialog$DialogState;Ljava/lang/String;)V
    invoke-static {v0, v1, v2}, Lcom/konka/systemsetting/net/WpsDialog;->access$1(Lcom/konka/systemsetting/net/WpsDialog;Lcom/konka/systemsetting/net/WpsDialog$DialogState;Ljava/lang/String;)V

    goto :goto_0
.end method
