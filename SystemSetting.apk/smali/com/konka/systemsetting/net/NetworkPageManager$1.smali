.class Lcom/konka/systemsetting/net/NetworkPageManager$1;
.super Ljava/lang/Object;
.source "NetworkPageManager.java"

# interfaces
.implements Landroid/view/View$OnFocusChangeListener;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/konka/systemsetting/net/NetworkPageManager;->SetOnFocusChangeListener()V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/konka/systemsetting/net/NetworkPageManager;


# direct methods
.method constructor <init>(Lcom/konka/systemsetting/net/NetworkPageManager;)V
    .locals 0

    iput-object p1, p0, Lcom/konka/systemsetting/net/NetworkPageManager$1;->this$0:Lcom/konka/systemsetting/net/NetworkPageManager;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onFocusChange(Landroid/view/View;Z)V
    .locals 6
    .param p1    # Landroid/view/View;
    .param p2    # Z

    const/4 v5, 0x2

    const/4 v4, 0x1

    const/4 v3, 0x0

    invoke-virtual {p1}, Landroid/view/View;->getId()I

    move-result v0

    if-eqz p2, :cond_1

    packed-switch v0, :pswitch_data_0

    :goto_0
    return-void

    :pswitch_0
    iget-object v1, p0, Lcom/konka/systemsetting/net/NetworkPageManager$1;->this$0:Lcom/konka/systemsetting/net/NetworkPageManager;

    iget-object v2, p0, Lcom/konka/systemsetting/net/NetworkPageManager$1;->this$0:Lcom/konka/systemsetting/net/NetworkPageManager;

    # getter for: Lcom/konka/systemsetting/net/NetworkPageManager;->m_iCurPage:I
    invoke-static {v2}, Lcom/konka/systemsetting/net/NetworkPageManager;->access$0(Lcom/konka/systemsetting/net/NetworkPageManager;)I

    move-result v2

    invoke-static {v1, v2}, Lcom/konka/systemsetting/net/NetworkPageManager;->access$1(Lcom/konka/systemsetting/net/NetworkPageManager;I)V

    iget-object v1, p0, Lcom/konka/systemsetting/net/NetworkPageManager$1;->this$0:Lcom/konka/systemsetting/net/NetworkPageManager;

    invoke-static {v1, v3}, Lcom/konka/systemsetting/net/NetworkPageManager;->access$2(Lcom/konka/systemsetting/net/NetworkPageManager;I)V

    :goto_1
    iget-object v1, p0, Lcom/konka/systemsetting/net/NetworkPageManager$1;->this$0:Lcom/konka/systemsetting/net/NetworkPageManager;

    # getter for: Lcom/konka/systemsetting/net/NetworkPageManager;->m_iCurPage:I
    invoke-static {v1}, Lcom/konka/systemsetting/net/NetworkPageManager;->access$0(Lcom/konka/systemsetting/net/NetworkPageManager;)I

    move-result v1

    iget-object v2, p0, Lcom/konka/systemsetting/net/NetworkPageManager$1;->this$0:Lcom/konka/systemsetting/net/NetworkPageManager;

    # getter for: Lcom/konka/systemsetting/net/NetworkPageManager;->m_iLastFocusPage:I
    invoke-static {v2}, Lcom/konka/systemsetting/net/NetworkPageManager;->access$3(Lcom/konka/systemsetting/net/NetworkPageManager;)I

    move-result v2

    if-eq v1, v2, :cond_0

    iget-object v1, p0, Lcom/konka/systemsetting/net/NetworkPageManager$1;->this$0:Lcom/konka/systemsetting/net/NetworkPageManager;

    iget-object v2, p0, Lcom/konka/systemsetting/net/NetworkPageManager$1;->this$0:Lcom/konka/systemsetting/net/NetworkPageManager;

    # getter for: Lcom/konka/systemsetting/net/NetworkPageManager;->m_iLastFocusPage:I
    invoke-static {v2}, Lcom/konka/systemsetting/net/NetworkPageManager;->access$3(Lcom/konka/systemsetting/net/NetworkPageManager;)I

    move-result v2

    # invokes: Lcom/konka/systemsetting/net/NetworkPageManager;->changeItemStatus(II)V
    invoke-static {v1, v2, v5}, Lcom/konka/systemsetting/net/NetworkPageManager;->access$4(Lcom/konka/systemsetting/net/NetworkPageManager;II)V

    :cond_0
    iget-object v1, p0, Lcom/konka/systemsetting/net/NetworkPageManager$1;->this$0:Lcom/konka/systemsetting/net/NetworkPageManager;

    iget-object v2, p0, Lcom/konka/systemsetting/net/NetworkPageManager$1;->this$0:Lcom/konka/systemsetting/net/NetworkPageManager;

    # getter for: Lcom/konka/systemsetting/net/NetworkPageManager;->m_iCurPage:I
    invoke-static {v2}, Lcom/konka/systemsetting/net/NetworkPageManager;->access$0(Lcom/konka/systemsetting/net/NetworkPageManager;)I

    move-result v2

    # invokes: Lcom/konka/systemsetting/net/NetworkPageManager;->changeItemStatus(II)V
    invoke-static {v1, v2, v3}, Lcom/konka/systemsetting/net/NetworkPageManager;->access$4(Lcom/konka/systemsetting/net/NetworkPageManager;II)V

    iget-object v1, p0, Lcom/konka/systemsetting/net/NetworkPageManager$1;->this$0:Lcom/konka/systemsetting/net/NetworkPageManager;

    iget-object v2, p0, Lcom/konka/systemsetting/net/NetworkPageManager$1;->this$0:Lcom/konka/systemsetting/net/NetworkPageManager;

    # getter for: Lcom/konka/systemsetting/net/NetworkPageManager;->m_iCurPage:I
    invoke-static {v2}, Lcom/konka/systemsetting/net/NetworkPageManager;->access$0(Lcom/konka/systemsetting/net/NetworkPageManager;)I

    move-result v2

    invoke-virtual {v1, v2}, Lcom/konka/systemsetting/net/NetworkPageManager;->showPage(I)V

    goto :goto_0

    :pswitch_1
    iget-object v1, p0, Lcom/konka/systemsetting/net/NetworkPageManager$1;->this$0:Lcom/konka/systemsetting/net/NetworkPageManager;

    iget-object v2, p0, Lcom/konka/systemsetting/net/NetworkPageManager$1;->this$0:Lcom/konka/systemsetting/net/NetworkPageManager;

    # getter for: Lcom/konka/systemsetting/net/NetworkPageManager;->m_iCurPage:I
    invoke-static {v2}, Lcom/konka/systemsetting/net/NetworkPageManager;->access$0(Lcom/konka/systemsetting/net/NetworkPageManager;)I

    move-result v2

    invoke-static {v1, v2}, Lcom/konka/systemsetting/net/NetworkPageManager;->access$1(Lcom/konka/systemsetting/net/NetworkPageManager;I)V

    iget-object v1, p0, Lcom/konka/systemsetting/net/NetworkPageManager$1;->this$0:Lcom/konka/systemsetting/net/NetworkPageManager;

    invoke-static {v1, v4}, Lcom/konka/systemsetting/net/NetworkPageManager;->access$2(Lcom/konka/systemsetting/net/NetworkPageManager;I)V

    goto :goto_1

    :pswitch_2
    iget-object v1, p0, Lcom/konka/systemsetting/net/NetworkPageManager$1;->this$0:Lcom/konka/systemsetting/net/NetworkPageManager;

    iget-object v2, p0, Lcom/konka/systemsetting/net/NetworkPageManager$1;->this$0:Lcom/konka/systemsetting/net/NetworkPageManager;

    # getter for: Lcom/konka/systemsetting/net/NetworkPageManager;->m_iCurPage:I
    invoke-static {v2}, Lcom/konka/systemsetting/net/NetworkPageManager;->access$0(Lcom/konka/systemsetting/net/NetworkPageManager;)I

    move-result v2

    invoke-static {v1, v2}, Lcom/konka/systemsetting/net/NetworkPageManager;->access$1(Lcom/konka/systemsetting/net/NetworkPageManager;I)V

    iget-object v1, p0, Lcom/konka/systemsetting/net/NetworkPageManager$1;->this$0:Lcom/konka/systemsetting/net/NetworkPageManager;

    invoke-static {v1, v5}, Lcom/konka/systemsetting/net/NetworkPageManager;->access$2(Lcom/konka/systemsetting/net/NetworkPageManager;I)V

    goto :goto_1

    :pswitch_3
    iget-object v1, p0, Lcom/konka/systemsetting/net/NetworkPageManager$1;->this$0:Lcom/konka/systemsetting/net/NetworkPageManager;

    iget-object v2, p0, Lcom/konka/systemsetting/net/NetworkPageManager$1;->this$0:Lcom/konka/systemsetting/net/NetworkPageManager;

    # getter for: Lcom/konka/systemsetting/net/NetworkPageManager;->m_iCurPage:I
    invoke-static {v2}, Lcom/konka/systemsetting/net/NetworkPageManager;->access$0(Lcom/konka/systemsetting/net/NetworkPageManager;)I

    move-result v2

    invoke-static {v1, v2}, Lcom/konka/systemsetting/net/NetworkPageManager;->access$1(Lcom/konka/systemsetting/net/NetworkPageManager;I)V

    iget-object v1, p0, Lcom/konka/systemsetting/net/NetworkPageManager$1;->this$0:Lcom/konka/systemsetting/net/NetworkPageManager;

    const/4 v2, 0x3

    invoke-static {v1, v2}, Lcom/konka/systemsetting/net/NetworkPageManager;->access$2(Lcom/konka/systemsetting/net/NetworkPageManager;I)V

    goto :goto_1

    :pswitch_4
    iget-object v1, p0, Lcom/konka/systemsetting/net/NetworkPageManager$1;->this$0:Lcom/konka/systemsetting/net/NetworkPageManager;

    iget-object v2, p0, Lcom/konka/systemsetting/net/NetworkPageManager$1;->this$0:Lcom/konka/systemsetting/net/NetworkPageManager;

    # getter for: Lcom/konka/systemsetting/net/NetworkPageManager;->m_iCurPage:I
    invoke-static {v2}, Lcom/konka/systemsetting/net/NetworkPageManager;->access$0(Lcom/konka/systemsetting/net/NetworkPageManager;)I

    move-result v2

    invoke-static {v1, v2}, Lcom/konka/systemsetting/net/NetworkPageManager;->access$1(Lcom/konka/systemsetting/net/NetworkPageManager;I)V

    iget-object v1, p0, Lcom/konka/systemsetting/net/NetworkPageManager$1;->this$0:Lcom/konka/systemsetting/net/NetworkPageManager;

    const/4 v2, 0x4

    invoke-static {v1, v2}, Lcom/konka/systemsetting/net/NetworkPageManager;->access$2(Lcom/konka/systemsetting/net/NetworkPageManager;I)V

    goto :goto_1

    :cond_1
    iget-object v1, p0, Lcom/konka/systemsetting/net/NetworkPageManager$1;->this$0:Lcom/konka/systemsetting/net/NetworkPageManager;

    iget-object v2, p0, Lcom/konka/systemsetting/net/NetworkPageManager$1;->this$0:Lcom/konka/systemsetting/net/NetworkPageManager;

    # getter for: Lcom/konka/systemsetting/net/NetworkPageManager;->m_iCurPage:I
    invoke-static {v2}, Lcom/konka/systemsetting/net/NetworkPageManager;->access$0(Lcom/konka/systemsetting/net/NetworkPageManager;)I

    move-result v2

    # invokes: Lcom/konka/systemsetting/net/NetworkPageManager;->changeItemStatus(II)V
    invoke-static {v1, v2, v4}, Lcom/konka/systemsetting/net/NetworkPageManager;->access$4(Lcom/konka/systemsetting/net/NetworkPageManager;II)V

    goto/16 :goto_0

    :pswitch_data_0
    .packed-switch 0x7f09003c
        :pswitch_0    # com.konka.systemsetting.R.id.linearlayout_network_tab_wired
        :pswitch_1    # com.konka.systemsetting.R.id.linearlayout_network_tab_wireless
        :pswitch_2    # com.konka.systemsetting.R.id.linearlayout_network_tab_pppoe
        :pswitch_3    # com.konka.systemsetting.R.id.tab_softap
        :pswitch_4    # com.konka.systemsetting.R.id.tab_wireless_display
    .end packed-switch
.end method
