.class Lcom/konka/systemsetting/net/NetworkSoftApConfig$2;
.super Lcom/konka/systemsetting/net/NetworkSoftApViewHolder;
.source "NetworkSoftApConfig.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/konka/systemsetting/net/NetworkSoftApConfig;-><init>(Lcom/konka/systemsetting/MainActivity;Lcom/konka/systemsetting/net/NetworkPageManager;)V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/konka/systemsetting/net/NetworkSoftApConfig;


# direct methods
.method constructor <init>(Lcom/konka/systemsetting/net/NetworkSoftApConfig;Lcom/konka/systemsetting/MainActivity;)V
    .locals 0
    .param p2    # Lcom/konka/systemsetting/MainActivity;

    iput-object p1, p0, Lcom/konka/systemsetting/net/NetworkSoftApConfig$2;->this$0:Lcom/konka/systemsetting/net/NetworkSoftApConfig;

    invoke-direct {p0, p2}, Lcom/konka/systemsetting/net/NetworkSoftApViewHolder;-><init>(Lcom/konka/systemsetting/MainActivity;)V

    return-void
.end method


# virtual methods
.method public itemOnClick(I)Z
    .locals 4
    .param p1    # I

    const/4 v1, 0x1

    const/4 v0, 0x0

    packed-switch p1, :pswitch_data_0

    :goto_0
    :pswitch_0
    invoke-super {p0, p1}, Lcom/konka/systemsetting/net/NetworkSoftApViewHolder;->itemOnClick(I)Z

    move-result v0

    return v0

    :pswitch_1
    iget-object v2, p0, Lcom/konka/systemsetting/net/NetworkSoftApConfig$2;->this$0:Lcom/konka/systemsetting/net/NetworkSoftApConfig;

    iget-object v3, p0, Lcom/konka/systemsetting/net/NetworkSoftApConfig$2;->this$0:Lcom/konka/systemsetting/net/NetworkSoftApConfig;

    # getter for: Lcom/konka/systemsetting/net/NetworkSoftApConfig;->isWifiAPEnabled:Z
    invoke-static {v3}, Lcom/konka/systemsetting/net/NetworkSoftApConfig;->access$1(Lcom/konka/systemsetting/net/NetworkSoftApConfig;)Z

    move-result v3

    if-eqz v3, :cond_0

    :goto_1
    invoke-static {v2, v0}, Lcom/konka/systemsetting/net/NetworkSoftApConfig;->access$2(Lcom/konka/systemsetting/net/NetworkSoftApConfig;Z)V

    const-string v0, "NET_WIFI_AP ==>"

    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "Change SoftAP Switch: isWifiAPEnabled = "

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    iget-object v2, p0, Lcom/konka/systemsetting/net/NetworkSoftApConfig$2;->this$0:Lcom/konka/systemsetting/net/NetworkSoftApConfig;

    # getter for: Lcom/konka/systemsetting/net/NetworkSoftApConfig;->isWifiAPEnabled:Z
    invoke-static {v2}, Lcom/konka/systemsetting/net/NetworkSoftApConfig;->access$1(Lcom/konka/systemsetting/net/NetworkSoftApConfig;)Z

    move-result v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    iget-object v0, p0, Lcom/konka/systemsetting/net/NetworkSoftApConfig$2;->this$0:Lcom/konka/systemsetting/net/NetworkSoftApConfig;

    # invokes: Lcom/konka/systemsetting/net/NetworkSoftApConfig;->updateSwitch()V
    invoke-static {v0}, Lcom/konka/systemsetting/net/NetworkSoftApConfig;->access$3(Lcom/konka/systemsetting/net/NetworkSoftApConfig;)V

    iget-object v0, p0, Lcom/konka/systemsetting/net/NetworkSoftApConfig$2;->this$0:Lcom/konka/systemsetting/net/NetworkSoftApConfig;

    iget-object v1, p0, Lcom/konka/systemsetting/net/NetworkSoftApConfig$2;->this$0:Lcom/konka/systemsetting/net/NetworkSoftApConfig;

    # getter for: Lcom/konka/systemsetting/net/NetworkSoftApConfig;->isWifiAPEnabled:Z
    invoke-static {v1}, Lcom/konka/systemsetting/net/NetworkSoftApConfig;->access$1(Lcom/konka/systemsetting/net/NetworkSoftApConfig;)Z

    move-result v1

    # invokes: Lcom/konka/systemsetting/net/NetworkSoftApConfig;->updateSwitchUI(Z)V
    invoke-static {v0, v1}, Lcom/konka/systemsetting/net/NetworkSoftApConfig;->access$4(Lcom/konka/systemsetting/net/NetworkSoftApConfig;Z)V

    goto :goto_0

    :cond_0
    move v0, v1

    goto :goto_1

    :pswitch_2
    iget-object v0, p0, Lcom/konka/systemsetting/net/NetworkSoftApConfig$2;->this$0:Lcom/konka/systemsetting/net/NetworkSoftApConfig;

    # invokes: Lcom/konka/systemsetting/net/NetworkSoftApConfig;->showSecuritySelectionDialog()V
    invoke-static {v0}, Lcom/konka/systemsetting/net/NetworkSoftApConfig;->access$5(Lcom/konka/systemsetting/net/NetworkSoftApConfig;)V

    goto :goto_0

    :pswitch_3
    iget-object v2, p0, Lcom/konka/systemsetting/net/NetworkSoftApConfig$2;->this$0:Lcom/konka/systemsetting/net/NetworkSoftApConfig;

    iget-object v3, p0, Lcom/konka/systemsetting/net/NetworkSoftApConfig$2;->this$0:Lcom/konka/systemsetting/net/NetworkSoftApConfig;

    # getter for: Lcom/konka/systemsetting/net/NetworkSoftApConfig;->isPwdToBeShown:Z
    invoke-static {v3}, Lcom/konka/systemsetting/net/NetworkSoftApConfig;->access$6(Lcom/konka/systemsetting/net/NetworkSoftApConfig;)Z

    move-result v3

    if-eqz v3, :cond_1

    :goto_2
    invoke-static {v2, v0}, Lcom/konka/systemsetting/net/NetworkSoftApConfig;->access$7(Lcom/konka/systemsetting/net/NetworkSoftApConfig;Z)V

    iget-object v0, p0, Lcom/konka/systemsetting/net/NetworkSoftApConfig$2;->this$0:Lcom/konka/systemsetting/net/NetworkSoftApConfig;

    # invokes: Lcom/konka/systemsetting/net/NetworkSoftApConfig;->updateShowPwdUI()V
    invoke-static {v0}, Lcom/konka/systemsetting/net/NetworkSoftApConfig;->access$8(Lcom/konka/systemsetting/net/NetworkSoftApConfig;)V

    goto :goto_0

    :cond_1
    move v0, v1

    goto :goto_2

    :pswitch_4
    iget-object v0, p0, Lcom/konka/systemsetting/net/NetworkSoftApConfig$2;->this$0:Lcom/konka/systemsetting/net/NetworkSoftApConfig;

    # invokes: Lcom/konka/systemsetting/net/NetworkSoftApConfig;->onItemSetClick()V
    invoke-static {v0}, Lcom/konka/systemsetting/net/NetworkSoftApConfig;->access$9(Lcom/konka/systemsetting/net/NetworkSoftApConfig;)V

    goto :goto_0

    :pswitch_5
    iget-object v0, p0, Lcom/konka/systemsetting/net/NetworkSoftApConfig$2;->this$0:Lcom/konka/systemsetting/net/NetworkSoftApConfig;

    # invokes: Lcom/konka/systemsetting/net/NetworkSoftApConfig;->onItemDetectClick()Z
    invoke-static {v0}, Lcom/konka/systemsetting/net/NetworkSoftApConfig;->access$10(Lcom/konka/systemsetting/net/NetworkSoftApConfig;)Z

    goto :goto_0

    nop

    :pswitch_data_0
    .packed-switch 0x7f090046
        :pswitch_1    # com.konka.systemsetting.R.id.softap_switch
        :pswitch_0    # com.konka.systemsetting.R.id.softap_ssid
        :pswitch_0    # com.konka.systemsetting.R.id.et_softap_ssid
        :pswitch_2    # com.konka.systemsetting.R.id.softap_security
        :pswitch_0    # com.konka.systemsetting.R.id.softap_password
        :pswitch_0    # com.konka.systemsetting.R.id.et_softap_password
        :pswitch_3    # com.konka.systemsetting.R.id.softap_showpwd
        :pswitch_0    # com.konka.systemsetting.R.id.softap_ipaddr
        :pswitch_0    # com.konka.systemsetting.R.id.softap_dnssvr
        :pswitch_4    # com.konka.systemsetting.R.id.softap_set
        :pswitch_5    # com.konka.systemsetting.R.id.softap_detect
    .end packed-switch
.end method

.method public itemOnKeyDown(I)Z
    .locals 3
    .param p1    # I

    const/4 v0, 0x1

    packed-switch p1, :pswitch_data_0

    :pswitch_0
    const/4 v0, 0x0

    :goto_0
    return v0

    :pswitch_1
    iget-object v1, p0, Lcom/konka/systemsetting/net/NetworkSoftApConfig$2;->itemSSID:Landroid/widget/LinearLayout;

    invoke-virtual {v1}, Landroid/widget/LinearLayout;->requestFocus()Z

    goto :goto_0

    :pswitch_2
    iget-object v1, p0, Lcom/konka/systemsetting/net/NetworkSoftApConfig$2;->itemSecurity:Landroid/widget/LinearLayout;

    invoke-virtual {v1}, Landroid/widget/LinearLayout;->requestFocus()Z

    goto :goto_0

    :pswitch_3
    const/4 v1, 0x2

    iget-object v2, p0, Lcom/konka/systemsetting/net/NetworkSoftApConfig$2;->this$0:Lcom/konka/systemsetting/net/NetworkSoftApConfig;

    # getter for: Lcom/konka/systemsetting/net/NetworkSoftApConfig;->mCurrentSec:I
    invoke-static {v2}, Lcom/konka/systemsetting/net/NetworkSoftApConfig;->access$11(Lcom/konka/systemsetting/net/NetworkSoftApConfig;)I

    move-result v2

    if-ne v1, v2, :cond_0

    iget-object v1, p0, Lcom/konka/systemsetting/net/NetworkSoftApConfig$2;->itemSet:Landroid/widget/Button;

    invoke-virtual {v1}, Landroid/widget/Button;->requestFocus()Z

    goto :goto_0

    :cond_0
    iget-object v1, p0, Lcom/konka/systemsetting/net/NetworkSoftApConfig$2;->itemPassword:Landroid/widget/LinearLayout;

    invoke-virtual {v1}, Landroid/widget/LinearLayout;->requestFocus()Z

    goto :goto_0

    :pswitch_4
    iget-object v1, p0, Lcom/konka/systemsetting/net/NetworkSoftApConfig$2;->itemShowPwd:Landroid/widget/LinearLayout;

    invoke-virtual {v1}, Landroid/widget/LinearLayout;->requestFocus()Z

    goto :goto_0

    :pswitch_5
    iget-object v1, p0, Lcom/konka/systemsetting/net/NetworkSoftApConfig$2;->itemSet:Landroid/widget/Button;

    invoke-virtual {v1}, Landroid/widget/Button;->requestFocus()Z

    goto :goto_0

    :pswitch_6
    iget-object v1, p0, Lcom/konka/systemsetting/net/NetworkSoftApConfig$2;->itemDetect:Landroid/widget/Button;

    invoke-virtual {v1}, Landroid/widget/Button;->requestFocus()Z

    goto :goto_0

    nop

    :pswitch_data_0
    .packed-switch 0x7f090046
        :pswitch_1    # com.konka.systemsetting.R.id.softap_switch
        :pswitch_2    # com.konka.systemsetting.R.id.softap_ssid
        :pswitch_0    # com.konka.systemsetting.R.id.et_softap_ssid
        :pswitch_3    # com.konka.systemsetting.R.id.softap_security
        :pswitch_4    # com.konka.systemsetting.R.id.softap_password
        :pswitch_0    # com.konka.systemsetting.R.id.et_softap_password
        :pswitch_5    # com.konka.systemsetting.R.id.softap_showpwd
        :pswitch_0    # com.konka.systemsetting.R.id.softap_ipaddr
        :pswitch_0    # com.konka.systemsetting.R.id.softap_dnssvr
        :pswitch_6    # com.konka.systemsetting.R.id.softap_set
        :pswitch_6    # com.konka.systemsetting.R.id.softap_detect
    .end packed-switch
.end method

.method public itemOnKeyUp(I)Z
    .locals 3
    .param p1    # I

    const/4 v0, 0x1

    packed-switch p1, :pswitch_data_0

    :pswitch_0
    const/4 v0, 0x0

    :goto_0
    return v0

    :pswitch_1
    iget-object v1, p0, Lcom/konka/systemsetting/net/NetworkSoftApConfig$2;->itemSwitch:Landroid/widget/LinearLayout;

    invoke-virtual {v1}, Landroid/widget/LinearLayout;->requestFocus()Z

    goto :goto_0

    :pswitch_2
    iget-object v1, p0, Lcom/konka/systemsetting/net/NetworkSoftApConfig$2;->itemSSID:Landroid/widget/LinearLayout;

    invoke-virtual {v1}, Landroid/widget/LinearLayout;->requestFocus()Z

    goto :goto_0

    :pswitch_3
    iget-object v1, p0, Lcom/konka/systemsetting/net/NetworkSoftApConfig$2;->itemSecurity:Landroid/widget/LinearLayout;

    invoke-virtual {v1}, Landroid/widget/LinearLayout;->requestFocus()Z

    goto :goto_0

    :pswitch_4
    iget-object v1, p0, Lcom/konka/systemsetting/net/NetworkSoftApConfig$2;->itemPassword:Landroid/widget/LinearLayout;

    invoke-virtual {v1}, Landroid/widget/LinearLayout;->requestFocus()Z

    goto :goto_0

    :pswitch_5
    const/4 v1, 0x2

    iget-object v2, p0, Lcom/konka/systemsetting/net/NetworkSoftApConfig$2;->this$0:Lcom/konka/systemsetting/net/NetworkSoftApConfig;

    # getter for: Lcom/konka/systemsetting/net/NetworkSoftApConfig;->mCurrentSec:I
    invoke-static {v2}, Lcom/konka/systemsetting/net/NetworkSoftApConfig;->access$11(Lcom/konka/systemsetting/net/NetworkSoftApConfig;)I

    move-result v2

    if-ne v1, v2, :cond_0

    iget-object v1, p0, Lcom/konka/systemsetting/net/NetworkSoftApConfig$2;->itemSecurity:Landroid/widget/LinearLayout;

    invoke-virtual {v1}, Landroid/widget/LinearLayout;->requestFocus()Z

    goto :goto_0

    :cond_0
    iget-object v1, p0, Lcom/konka/systemsetting/net/NetworkSoftApConfig$2;->itemShowPwd:Landroid/widget/LinearLayout;

    invoke-virtual {v1}, Landroid/widget/LinearLayout;->requestFocus()Z

    goto :goto_0

    :pswitch_6
    iget-object v1, p0, Lcom/konka/systemsetting/net/NetworkSoftApConfig$2;->itemSet:Landroid/widget/Button;

    invoke-virtual {v1}, Landroid/widget/Button;->requestFocus()Z

    goto :goto_0

    nop

    :pswitch_data_0
    .packed-switch 0x7f090046
        :pswitch_1    # com.konka.systemsetting.R.id.softap_switch
        :pswitch_1    # com.konka.systemsetting.R.id.softap_ssid
        :pswitch_0    # com.konka.systemsetting.R.id.et_softap_ssid
        :pswitch_2    # com.konka.systemsetting.R.id.softap_security
        :pswitch_3    # com.konka.systemsetting.R.id.softap_password
        :pswitch_0    # com.konka.systemsetting.R.id.et_softap_password
        :pswitch_4    # com.konka.systemsetting.R.id.softap_showpwd
        :pswitch_0    # com.konka.systemsetting.R.id.softap_ipaddr
        :pswitch_0    # com.konka.systemsetting.R.id.softap_dnssvr
        :pswitch_5    # com.konka.systemsetting.R.id.softap_set
        :pswitch_6    # com.konka.systemsetting.R.id.softap_detect
    .end packed-switch
.end method
