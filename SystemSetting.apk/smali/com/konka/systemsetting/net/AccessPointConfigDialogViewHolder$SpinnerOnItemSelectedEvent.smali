.class Lcom/konka/systemsetting/net/AccessPointConfigDialogViewHolder$SpinnerOnItemSelectedEvent;
.super Ljava/lang/Object;
.source "AccessPointConfigDialogViewHolder.java"

# interfaces
.implements Landroid/widget/AdapterView$OnItemSelectedListener;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/konka/systemsetting/net/AccessPointConfigDialogViewHolder;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x2
    name = "SpinnerOnItemSelectedEvent"
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Object;",
        "Landroid/widget/AdapterView$OnItemSelectedListener;"
    }
.end annotation


# instance fields
.field final synthetic this$0:Lcom/konka/systemsetting/net/AccessPointConfigDialogViewHolder;


# direct methods
.method private constructor <init>(Lcom/konka/systemsetting/net/AccessPointConfigDialogViewHolder;)V
    .locals 0

    iput-object p1, p0, Lcom/konka/systemsetting/net/AccessPointConfigDialogViewHolder$SpinnerOnItemSelectedEvent;->this$0:Lcom/konka/systemsetting/net/AccessPointConfigDialogViewHolder;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method synthetic constructor <init>(Lcom/konka/systemsetting/net/AccessPointConfigDialogViewHolder;Lcom/konka/systemsetting/net/AccessPointConfigDialogViewHolder$SpinnerOnItemSelectedEvent;)V
    .locals 0

    invoke-direct {p0, p1}, Lcom/konka/systemsetting/net/AccessPointConfigDialogViewHolder$SpinnerOnItemSelectedEvent;-><init>(Lcom/konka/systemsetting/net/AccessPointConfigDialogViewHolder;)V

    return-void
.end method


# virtual methods
.method public onItemSelected(Landroid/widget/AdapterView;Landroid/view/View;IJ)V
    .locals 4
    .param p2    # Landroid/view/View;
    .param p3    # I
    .param p4    # J
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Landroid/widget/AdapterView",
            "<*>;",
            "Landroid/view/View;",
            "IJ)V"
        }
    .end annotation

    const/4 v3, 0x1

    const/4 v2, 0x0

    if-nez p3, :cond_0

    iget-object v0, p0, Lcom/konka/systemsetting/net/AccessPointConfigDialogViewHolder$SpinnerOnItemSelectedEvent;->this$0:Lcom/konka/systemsetting/net/AccessPointConfigDialogViewHolder;

    const-string v1, "Wifi_IP_MODE_CHANGE, current mode: DHCP."

    # invokes: Lcom/konka/systemsetting/net/AccessPointConfigDialogViewHolder;->log(Ljava/lang/String;)V
    invoke-static {v0, v1}, Lcom/konka/systemsetting/net/AccessPointConfigDialogViewHolder;->access$3(Lcom/konka/systemsetting/net/AccessPointConfigDialogViewHolder;Ljava/lang/String;)V

    iget-object v0, p0, Lcom/konka/systemsetting/net/AccessPointConfigDialogViewHolder$SpinnerOnItemSelectedEvent;->this$0:Lcom/konka/systemsetting/net/AccessPointConfigDialogViewHolder;

    # invokes: Lcom/konka/systemsetting/net/AccessPointConfigDialogViewHolder;->setIPConfigurable(Z)V
    invoke-static {v0, v2}, Lcom/konka/systemsetting/net/AccessPointConfigDialogViewHolder;->access$4(Lcom/konka/systemsetting/net/AccessPointConfigDialogViewHolder;Z)V

    iget-object v0, p0, Lcom/konka/systemsetting/net/AccessPointConfigDialogViewHolder$SpinnerOnItemSelectedEvent;->this$0:Lcom/konka/systemsetting/net/AccessPointConfigDialogViewHolder;

    # getter for: Lcom/konka/systemsetting/net/AccessPointConfigDialogViewHolder;->mCurrAP:Lcom/konka/systemsetting/net/AccessPointConfigDialog;
    invoke-static {v0}, Lcom/konka/systemsetting/net/AccessPointConfigDialogViewHolder;->access$5(Lcom/konka/systemsetting/net/AccessPointConfigDialogViewHolder;)Lcom/konka/systemsetting/net/AccessPointConfigDialog;

    move-result-object v0

    invoke-virtual {v0, v3}, Lcom/konka/systemsetting/net/AccessPointConfigDialog;->setIpModeAuto(Z)Z

    :goto_0
    return-void

    :cond_0
    iget-object v0, p0, Lcom/konka/systemsetting/net/AccessPointConfigDialogViewHolder$SpinnerOnItemSelectedEvent;->this$0:Lcom/konka/systemsetting/net/AccessPointConfigDialogViewHolder;

    const-string v1, "Wifi_IP_MODE_CHANGE, current mode: STATIC."

    # invokes: Lcom/konka/systemsetting/net/AccessPointConfigDialogViewHolder;->log(Ljava/lang/String;)V
    invoke-static {v0, v1}, Lcom/konka/systemsetting/net/AccessPointConfigDialogViewHolder;->access$3(Lcom/konka/systemsetting/net/AccessPointConfigDialogViewHolder;Ljava/lang/String;)V

    iget-object v0, p0, Lcom/konka/systemsetting/net/AccessPointConfigDialogViewHolder$SpinnerOnItemSelectedEvent;->this$0:Lcom/konka/systemsetting/net/AccessPointConfigDialogViewHolder;

    # invokes: Lcom/konka/systemsetting/net/AccessPointConfigDialogViewHolder;->setIPConfigurable(Z)V
    invoke-static {v0, v3}, Lcom/konka/systemsetting/net/AccessPointConfigDialogViewHolder;->access$4(Lcom/konka/systemsetting/net/AccessPointConfigDialogViewHolder;Z)V

    iget-object v0, p0, Lcom/konka/systemsetting/net/AccessPointConfigDialogViewHolder$SpinnerOnItemSelectedEvent;->this$0:Lcom/konka/systemsetting/net/AccessPointConfigDialogViewHolder;

    # getter for: Lcom/konka/systemsetting/net/AccessPointConfigDialogViewHolder;->mCurrAP:Lcom/konka/systemsetting/net/AccessPointConfigDialog;
    invoke-static {v0}, Lcom/konka/systemsetting/net/AccessPointConfigDialogViewHolder;->access$5(Lcom/konka/systemsetting/net/AccessPointConfigDialogViewHolder;)Lcom/konka/systemsetting/net/AccessPointConfigDialog;

    move-result-object v0

    invoke-virtual {v0, v2}, Lcom/konka/systemsetting/net/AccessPointConfigDialog;->setIpModeAuto(Z)Z

    goto :goto_0
.end method

.method public onNothingSelected(Landroid/widget/AdapterView;)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Landroid/widget/AdapterView",
            "<*>;)V"
        }
    .end annotation

    return-void
.end method
