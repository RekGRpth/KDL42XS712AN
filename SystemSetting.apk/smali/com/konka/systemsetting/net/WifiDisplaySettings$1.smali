.class Lcom/konka/systemsetting/net/WifiDisplaySettings$1;
.super Ljava/lang/Object;
.source "WifiDisplaySettings.java"

# interfaces
.implements Landroid/widget/CompoundButton$OnCheckedChangeListener;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/konka/systemsetting/net/WifiDisplaySettings;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/konka/systemsetting/net/WifiDisplaySettings;


# direct methods
.method constructor <init>(Lcom/konka/systemsetting/net/WifiDisplaySettings;)V
    .locals 0

    iput-object p1, p0, Lcom/konka/systemsetting/net/WifiDisplaySettings$1;->this$0:Lcom/konka/systemsetting/net/WifiDisplaySettings;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onCheckedChanged(Landroid/widget/CompoundButton;Z)V
    .locals 3
    .param p1    # Landroid/widget/CompoundButton;
    .param p2    # Z

    iget-object v0, p0, Lcom/konka/systemsetting/net/WifiDisplaySettings$1;->this$0:Lcom/konka/systemsetting/net/WifiDisplaySettings;

    invoke-static {v0, p2}, Lcom/konka/systemsetting/net/WifiDisplaySettings;->access$0(Lcom/konka/systemsetting/net/WifiDisplaySettings;Z)V

    iget-object v0, p0, Lcom/konka/systemsetting/net/WifiDisplaySettings$1;->this$0:Lcom/konka/systemsetting/net/WifiDisplaySettings;

    # invokes: Lcom/konka/systemsetting/net/WifiDisplaySettings;->getContentResolver()Landroid/content/ContentResolver;
    invoke-static {v0}, Lcom/konka/systemsetting/net/WifiDisplaySettings;->access$1(Lcom/konka/systemsetting/net/WifiDisplaySettings;)Landroid/content/ContentResolver;

    move-result-object v1

    const-string v2, "wifi_display_on"

    if-eqz p2, :cond_0

    const/4 v0, 0x1

    :goto_0
    invoke-static {v1, v2, v0}, Landroid/provider/Settings$Global;->putInt(Landroid/content/ContentResolver;Ljava/lang/String;I)Z

    return-void

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method
