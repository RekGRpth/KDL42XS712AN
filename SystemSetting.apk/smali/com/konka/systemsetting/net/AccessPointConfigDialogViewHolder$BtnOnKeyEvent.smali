.class Lcom/konka/systemsetting/net/AccessPointConfigDialogViewHolder$BtnOnKeyEvent;
.super Ljava/lang/Object;
.source "AccessPointConfigDialogViewHolder.java"

# interfaces
.implements Landroid/view/View$OnKeyListener;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/konka/systemsetting/net/AccessPointConfigDialogViewHolder;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x2
    name = "BtnOnKeyEvent"
.end annotation


# instance fields
.field final synthetic this$0:Lcom/konka/systemsetting/net/AccessPointConfigDialogViewHolder;


# direct methods
.method private constructor <init>(Lcom/konka/systemsetting/net/AccessPointConfigDialogViewHolder;)V
    .locals 0

    iput-object p1, p0, Lcom/konka/systemsetting/net/AccessPointConfigDialogViewHolder$BtnOnKeyEvent;->this$0:Lcom/konka/systemsetting/net/AccessPointConfigDialogViewHolder;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method synthetic constructor <init>(Lcom/konka/systemsetting/net/AccessPointConfigDialogViewHolder;Lcom/konka/systemsetting/net/AccessPointConfigDialogViewHolder$BtnOnKeyEvent;)V
    .locals 0

    invoke-direct {p0, p1}, Lcom/konka/systemsetting/net/AccessPointConfigDialogViewHolder$BtnOnKeyEvent;-><init>(Lcom/konka/systemsetting/net/AccessPointConfigDialogViewHolder;)V

    return-void
.end method


# virtual methods
.method public onKey(Landroid/view/View;ILandroid/view/KeyEvent;)Z
    .locals 2
    .param p1    # Landroid/view/View;
    .param p2    # I
    .param p3    # Landroid/view/KeyEvent;

    invoke-virtual {p3}, Landroid/view/KeyEvent;->getAction()I

    move-result v0

    if-nez v0, :cond_0

    sparse-switch p2, :sswitch_data_0

    :cond_0
    :goto_0
    const/4 v0, 0x0

    return v0

    :sswitch_0
    iget-object v0, p0, Lcom/konka/systemsetting/net/AccessPointConfigDialogViewHolder$BtnOnKeyEvent;->this$0:Lcom/konka/systemsetting/net/AccessPointConfigDialogViewHolder;

    invoke-virtual {p1}, Landroid/view/View;->getId()I

    move-result v1

    # invokes: Lcom/konka/systemsetting/net/AccessPointConfigDialogViewHolder;->btnOnEnterKeyDown(I)V
    invoke-static {v0, v1}, Lcom/konka/systemsetting/net/AccessPointConfigDialogViewHolder;->access$2(Lcom/konka/systemsetting/net/AccessPointConfigDialogViewHolder;I)V

    goto :goto_0

    nop

    :sswitch_data_0
    .sparse-switch
        0x17 -> :sswitch_0
        0x42 -> :sswitch_0
    .end sparse-switch
.end method
