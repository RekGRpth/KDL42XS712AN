.class Lcom/konka/systemsetting/net/NetworkPPPoeConfig$CheckPPPoEThread;
.super Ljava/lang/Thread;
.source "NetworkPPPoeConfig.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/konka/systemsetting/net/NetworkPPPoeConfig;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = "CheckPPPoEThread"
.end annotation


# instance fields
.field mCheckStatus:Landroid/net/pppoe/PPPOE_STA;

.field final synthetic this$0:Lcom/konka/systemsetting/net/NetworkPPPoeConfig;

.field threadLoop:I


# direct methods
.method public constructor <init>(Lcom/konka/systemsetting/net/NetworkPPPoeConfig;Landroid/net/pppoe/PPPOE_STA;)V
    .locals 1
    .param p2    # Landroid/net/pppoe/PPPOE_STA;

    iput-object p1, p0, Lcom/konka/systemsetting/net/NetworkPPPoeConfig$CheckPPPoEThread;->this$0:Lcom/konka/systemsetting/net/NetworkPPPoeConfig;

    invoke-direct {p0}, Ljava/lang/Thread;-><init>()V

    const-string v0, "CheckPPPoEThread INIT"

    invoke-static {v0}, Lcom/konka/debuginfo/logPrint;->Debug(Ljava/lang/String;)V

    const/4 v0, 0x0

    iput v0, p0, Lcom/konka/systemsetting/net/NetworkPPPoeConfig$CheckPPPoEThread;->threadLoop:I

    iput-object p2, p0, Lcom/konka/systemsetting/net/NetworkPPPoeConfig$CheckPPPoEThread;->mCheckStatus:Landroid/net/pppoe/PPPOE_STA;

    return-void
.end method


# virtual methods
.method public cancelSelf()V
    .locals 1

    const/16 v0, 0x3c

    iput v0, p0, Lcom/konka/systemsetting/net/NetworkPPPoeConfig$CheckPPPoEThread;->threadLoop:I

    return-void
.end method

.method public run()V
    .locals 5

    :goto_0
    iget-object v2, p0, Lcom/konka/systemsetting/net/NetworkPPPoeConfig$CheckPPPoEThread;->this$0:Lcom/konka/systemsetting/net/NetworkPPPoeConfig;

    iget-object v2, v2, Lcom/konka/systemsetting/net/NetworkPPPoeConfig;->mPPPOE:Landroid/net/pppoe/PppoeManager;

    invoke-virtual {v2}, Landroid/net/pppoe/PppoeManager;->PppoeGetStatus()Landroid/net/pppoe/PPPOE_STA;

    move-result-object v2

    iget-object v3, p0, Lcom/konka/systemsetting/net/NetworkPPPoeConfig$CheckPPPoEThread;->mCheckStatus:Landroid/net/pppoe/PPPOE_STA;

    if-ne v2, v3, :cond_0

    iget v2, p0, Lcom/konka/systemsetting/net/NetworkPPPoeConfig$CheckPPPoEThread;->threadLoop:I

    add-int/lit8 v3, v2, 0x1

    iput v3, p0, Lcom/konka/systemsetting/net/NetworkPPPoeConfig$CheckPPPoEThread;->threadLoop:I

    const/16 v3, 0x1e

    if-lt v2, v3, :cond_1

    :cond_0
    new-instance v2, Ljava/lang/StringBuilder;

    const-string v3, "ThreadEnd id="

    invoke-direct {v2, v3}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {p0}, Lcom/konka/systemsetting/net/NetworkPPPoeConfig$CheckPPPoEThread;->getId()J

    move-result-wide v3

    invoke-virtual {v2, v3, v4}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v2}, Lcom/konka/debuginfo/logPrint;->Debug(Ljava/lang/String;)V

    new-instance v1, Landroid/os/Message;

    invoke-direct {v1}, Landroid/os/Message;-><init>()V

    const/4 v2, 0x1

    iput v2, v1, Landroid/os/Message;->what:I

    iget-object v2, p0, Lcom/konka/systemsetting/net/NetworkPPPoeConfig$CheckPPPoEThread;->this$0:Lcom/konka/systemsetting/net/NetworkPPPoeConfig;

    # getter for: Lcom/konka/systemsetting/net/NetworkPPPoeConfig;->mHandler:Landroid/os/Handler;
    invoke-static {v2}, Lcom/konka/systemsetting/net/NetworkPPPoeConfig;->access$0(Lcom/konka/systemsetting/net/NetworkPPPoeConfig;)Landroid/os/Handler;

    move-result-object v2

    invoke-virtual {v2, v1}, Landroid/os/Handler;->sendMessage(Landroid/os/Message;)Z

    return-void

    :cond_1
    const-string v2, "CheckPPPoEThread"

    invoke-static {v2}, Lcom/konka/debuginfo/logPrint;->Debug(Ljava/lang/String;)V

    new-instance v1, Landroid/os/Message;

    invoke-direct {v1}, Landroid/os/Message;-><init>()V

    const/4 v2, 0x0

    iput v2, v1, Landroid/os/Message;->what:I

    iget-object v2, p0, Lcom/konka/systemsetting/net/NetworkPPPoeConfig$CheckPPPoEThread;->this$0:Lcom/konka/systemsetting/net/NetworkPPPoeConfig;

    # getter for: Lcom/konka/systemsetting/net/NetworkPPPoeConfig;->mHandler:Landroid/os/Handler;
    invoke-static {v2}, Lcom/konka/systemsetting/net/NetworkPPPoeConfig;->access$0(Lcom/konka/systemsetting/net/NetworkPPPoeConfig;)Landroid/os/Handler;

    move-result-object v2

    invoke-virtual {v2, v1}, Landroid/os/Handler;->sendMessage(Landroid/os/Message;)Z

    const-wide/16 v2, 0x3e8

    :try_start_0
    invoke-static {v2, v3}, Ljava/lang/Thread;->sleep(J)V
    :try_end_0
    .catch Ljava/lang/InterruptedException; {:try_start_0 .. :try_end_0} :catch_0

    goto :goto_0

    :catch_0
    move-exception v0

    invoke-static {}, Ljava/lang/Thread;->currentThread()Ljava/lang/Thread;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/Thread;->interrupt()V

    goto :goto_0
.end method
