.class Lcom/konka/systemsetting/net/PPPOEIsAutoDialerService$1;
.super Ljava/lang/Object;
.source "PPPOEIsAutoDialerService.java"

# interfaces
.implements Ljava/lang/Runnable;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/konka/systemsetting/net/PPPOEIsAutoDialerService;->pppoeDialer()V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/konka/systemsetting/net/PPPOEIsAutoDialerService;


# direct methods
.method constructor <init>(Lcom/konka/systemsetting/net/PPPOEIsAutoDialerService;)V
    .locals 0

    iput-object p1, p0, Lcom/konka/systemsetting/net/PPPOEIsAutoDialerService$1;->this$0:Lcom/konka/systemsetting/net/PPPOEIsAutoDialerService;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public run()V
    .locals 4

    const v3, 0x7f020012    # com.konka.systemsetting.R.drawable.one_px

    iget-object v1, p0, Lcom/konka/systemsetting/net/PPPOEIsAutoDialerService$1;->this$0:Lcom/konka/systemsetting/net/PPPOEIsAutoDialerService;

    iget-object v1, v1, Lcom/konka/systemsetting/net/PPPOEIsAutoDialerService;->mPPPOE:Landroid/net/pppoe/PppoeManager;

    invoke-virtual {v1}, Landroid/net/pppoe/PppoeManager;->PppoeGetStatus()Landroid/net/pppoe/PPPOE_STA;

    move-result-object v0

    sget-object v1, Landroid/net/pppoe/PPPOE_STA;->CONNECTED:Landroid/net/pppoe/PPPOE_STA;

    if-ne v0, v1, :cond_1

    iget-object v1, p0, Lcom/konka/systemsetting/net/PPPOEIsAutoDialerService$1;->this$0:Lcom/konka/systemsetting/net/PPPOEIsAutoDialerService;

    const v2, 0x7f0600a0    # com.konka.systemsetting.R.string.pppoe_auto_success

    # invokes: Lcom/konka/systemsetting/net/PPPOEIsAutoDialerService;->showNotification(II)V
    invoke-static {v1, v3, v2}, Lcom/konka/systemsetting/net/PPPOEIsAutoDialerService;->access$0(Lcom/konka/systemsetting/net/PPPOEIsAutoDialerService;II)V

    :cond_0
    :goto_0
    return-void

    :cond_1
    sget-object v1, Landroid/net/pppoe/PPPOE_STA;->DISCONNECTED:Landroid/net/pppoe/PPPOE_STA;

    if-ne v0, v1, :cond_0

    iget-object v1, p0, Lcom/konka/systemsetting/net/PPPOEIsAutoDialerService$1;->this$0:Lcom/konka/systemsetting/net/PPPOEIsAutoDialerService;

    const v2, 0x7f0600a1    # com.konka.systemsetting.R.string.pppoe_auto_failure

    # invokes: Lcom/konka/systemsetting/net/PPPOEIsAutoDialerService;->showNotification(II)V
    invoke-static {v1, v3, v2}, Lcom/konka/systemsetting/net/PPPOEIsAutoDialerService;->access$0(Lcom/konka/systemsetting/net/PPPOEIsAutoDialerService;II)V

    goto :goto_0
.end method
