.class public Lcom/konka/systemsetting/net/AccessPointConfigDialog;
.super Landroid/app/AlertDialog;
.source "AccessPointConfigDialog.java"


# static fields
.field private static final SECURITY_EAP:I = 0x3

.field private static final SECURITY_NONE:I = 0x0

.field private static final SECURITY_PSK:I = 0x2

.field private static final SECURITY_WEP:I = 0x1

.field private static final SIGNAL_0:I = 0x0

.field private static final SIGNAL_1:I = 0x1

.field private static final SIGNAL_2:I = 0x2

.field private static final SIGNAL_3:I = 0x3

.field private static final SIGNAL_4:I = 0x4

.field private static final SIGNAL_5:I = 0x5

.field private static final SIGNAL_6:I = 0x6

.field private static final SIGNAL_NUM_LEVELS:I = 0x7

.field private static final TAG:Ljava/lang/String; = "==========>Net_Wifi_Cfg==>APConfigDialog"

.field private static bWaitToVerify:Z


# instance fields
.field public isConfiged:Z

.field public isConnected:Z

.field private mConnectListener:Landroid/net/wifi/WifiManager$ActionListener;

.field private mContext:Lcom/konka/systemsetting/MainActivity;

.field private mForgetListener:Landroid/net/wifi/WifiManager$ActionListener;

.field private mHolder:Lcom/konka/systemsetting/net/AccessPointConfigDialogViewHolder;

.field public mIpAssignment:Landroid/net/wifi/WifiConfiguration$IpAssignment;

.field public mLinkProperties:Landroid/net/LinkProperties;

.field public mProxySettings:Landroid/net/wifi/WifiConfiguration$ProxySettings;

.field public mResult:Landroid/net/wifi/ScanResult;

.field private mSaveListener:Landroid/net/wifi/WifiManager$ActionListener;

.field public manager:Landroid/net/wifi/WifiManager;

.field public networkId:I


# direct methods
.method static constructor <clinit>()V
    .locals 1

    const/4 v0, 0x0

    sput-boolean v0, Lcom/konka/systemsetting/net/AccessPointConfigDialog;->bWaitToVerify:Z

    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/net/wifi/ScanResult;Landroid/net/wifi/WifiManager;)V
    .locals 2
    .param p1    # Landroid/content/Context;
    .param p2    # Landroid/net/wifi/ScanResult;
    .param p3    # Landroid/net/wifi/WifiManager;

    const/4 v0, 0x0

    invoke-direct {p0, p1}, Landroid/app/AlertDialog;-><init>(Landroid/content/Context;)V

    iput-boolean v0, p0, Lcom/konka/systemsetting/net/AccessPointConfigDialog;->isConnected:Z

    iput-boolean v0, p0, Lcom/konka/systemsetting/net/AccessPointConfigDialog;->isConfiged:Z

    const/4 v0, -0x1

    iput v0, p0, Lcom/konka/systemsetting/net/AccessPointConfigDialog;->networkId:I

    sget-object v0, Landroid/net/wifi/WifiConfiguration$IpAssignment;->UNASSIGNED:Landroid/net/wifi/WifiConfiguration$IpAssignment;

    iput-object v0, p0, Lcom/konka/systemsetting/net/AccessPointConfigDialog;->mIpAssignment:Landroid/net/wifi/WifiConfiguration$IpAssignment;

    sget-object v0, Landroid/net/wifi/WifiConfiguration$ProxySettings;->UNASSIGNED:Landroid/net/wifi/WifiConfiguration$ProxySettings;

    iput-object v0, p0, Lcom/konka/systemsetting/net/AccessPointConfigDialog;->mProxySettings:Landroid/net/wifi/WifiConfiguration$ProxySettings;

    new-instance v0, Landroid/net/LinkProperties;

    invoke-direct {v0}, Landroid/net/LinkProperties;-><init>()V

    iput-object v0, p0, Lcom/konka/systemsetting/net/AccessPointConfigDialog;->mLinkProperties:Landroid/net/LinkProperties;

    check-cast p1, Lcom/konka/systemsetting/MainActivity;

    iput-object p1, p0, Lcom/konka/systemsetting/net/AccessPointConfigDialog;->mContext:Lcom/konka/systemsetting/MainActivity;

    iput-object p2, p0, Lcom/konka/systemsetting/net/AccessPointConfigDialog;->mResult:Landroid/net/wifi/ScanResult;

    iput-object p3, p0, Lcom/konka/systemsetting/net/AccessPointConfigDialog;->manager:Landroid/net/wifi/WifiManager;

    new-instance v0, Lcom/konka/systemsetting/net/AccessPointConfigDialog$1;

    invoke-direct {v0, p0}, Lcom/konka/systemsetting/net/AccessPointConfigDialog$1;-><init>(Lcom/konka/systemsetting/net/AccessPointConfigDialog;)V

    iput-object v0, p0, Lcom/konka/systemsetting/net/AccessPointConfigDialog;->mSaveListener:Landroid/net/wifi/WifiManager$ActionListener;

    new-instance v0, Lcom/konka/systemsetting/net/AccessPointConfigDialog$2;

    invoke-direct {v0, p0}, Lcom/konka/systemsetting/net/AccessPointConfigDialog$2;-><init>(Lcom/konka/systemsetting/net/AccessPointConfigDialog;)V

    iput-object v0, p0, Lcom/konka/systemsetting/net/AccessPointConfigDialog;->mConnectListener:Landroid/net/wifi/WifiManager$ActionListener;

    new-instance v0, Lcom/konka/systemsetting/net/AccessPointConfigDialog$3;

    invoke-direct {v0, p0}, Lcom/konka/systemsetting/net/AccessPointConfigDialog$3;-><init>(Lcom/konka/systemsetting/net/AccessPointConfigDialog;)V

    iput-object v0, p0, Lcom/konka/systemsetting/net/AccessPointConfigDialog;->mForgetListener:Landroid/net/wifi/WifiManager$ActionListener;

    new-instance v0, Lcom/konka/systemsetting/net/AccessPointConfigDialogViewHolder;

    iget-object v1, p0, Lcom/konka/systemsetting/net/AccessPointConfigDialog;->mContext:Lcom/konka/systemsetting/MainActivity;

    invoke-direct {v0, v1, p0}, Lcom/konka/systemsetting/net/AccessPointConfigDialogViewHolder;-><init>(Lcom/konka/systemsetting/MainActivity;Lcom/konka/systemsetting/net/AccessPointConfigDialog;)V

    iput-object v0, p0, Lcom/konka/systemsetting/net/AccessPointConfigDialog;->mHolder:Lcom/konka/systemsetting/net/AccessPointConfigDialogViewHolder;

    iget-object v0, p0, Lcom/konka/systemsetting/net/AccessPointConfigDialog;->mHolder:Lcom/konka/systemsetting/net/AccessPointConfigDialogViewHolder;

    invoke-virtual {v0}, Lcom/konka/systemsetting/net/AccessPointConfigDialogViewHolder;->getDialogView()Landroid/widget/LinearLayout;

    move-result-object v0

    invoke-virtual {p0, v0}, Lcom/konka/systemsetting/net/AccessPointConfigDialog;->setView(Landroid/view/View;)V

    return-void
.end method

.method static synthetic access$0(Lcom/konka/systemsetting/net/AccessPointConfigDialog;)Lcom/konka/systemsetting/MainActivity;
    .locals 1

    iget-object v0, p0, Lcom/konka/systemsetting/net/AccessPointConfigDialog;->mContext:Lcom/konka/systemsetting/MainActivity;

    return-object v0
.end method

.method private convertToQuotedString(Ljava/lang/String;)Ljava/lang/String;
    .locals 2
    .param p1    # Ljava/lang/String;

    new-instance v0, Ljava/lang/StringBuilder;

    const-string v1, "\""

    invoke-direct {v0, v1}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v0, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, "\""

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method private getConfig()Landroid/net/wifi/WifiConfiguration;
    .locals 10

    const v9, 0x7f06008b    # com.konka.systemsetting.R.string.wifi_pwdemptyerror

    const/4 v8, 0x1

    const/4 v4, 0x0

    const/4 v7, 0x0

    iget-boolean v5, p0, Lcom/konka/systemsetting/net/AccessPointConfigDialog;->isConnected:Z

    if-nez v5, :cond_0

    iget-boolean v5, p0, Lcom/konka/systemsetting/net/AccessPointConfigDialog;->isConfiged:Z

    if-eqz v5, :cond_3

    :cond_0
    iget-object v5, p0, Lcom/konka/systemsetting/net/AccessPointConfigDialog;->manager:Landroid/net/wifi/WifiManager;

    invoke-virtual {v5}, Landroid/net/wifi/WifiManager;->getConfiguredNetworks()Ljava/util/List;

    move-result-object v5

    invoke-interface {v5}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v5

    :cond_1
    invoke-interface {v5}, Ljava/util/Iterator;->hasNext()Z

    move-result v6

    if-nez v6, :cond_2

    const-string v5, " getConfig(): ==> \u5f53\u524dResult\u5bf9\u5e94\u7684AP\u672a\u8fde\u63a5\u4e14\u672a\u4fdd\ufffd?. null error!!!"

    invoke-virtual {p0, v5}, Lcom/konka/systemsetting/net/AccessPointConfigDialog;->log(Ljava/lang/String;)V

    move-object v1, v4

    :goto_0
    return-object v1

    :cond_2
    invoke-interface {v5}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/net/wifi/WifiConfiguration;

    iget-object v6, p0, Lcom/konka/systemsetting/net/AccessPointConfigDialog;->mResult:Landroid/net/wifi/ScanResult;

    iget-object v6, v6, Landroid/net/wifi/ScanResult;->SSID:Ljava/lang/String;

    iget-object v7, v0, Landroid/net/wifi/WifiConfiguration;->SSID:Ljava/lang/String;

    invoke-static {v7}, Lcom/konka/systemsetting/net/AccessPointConfigDialog;->removeDoubleQuotes(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v7

    invoke-virtual {v6, v7}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v6

    if-eqz v6, :cond_1

    move-object v1, v0

    goto :goto_0

    :cond_3
    iget-object v5, p0, Lcom/konka/systemsetting/net/AccessPointConfigDialog;->mHolder:Lcom/konka/systemsetting/net/AccessPointConfigDialogViewHolder;

    iget-object v5, v5, Lcom/konka/systemsetting/net/AccessPointConfigDialogViewHolder;->etPassword:Landroid/widget/EditText;

    invoke-virtual {v5}, Landroid/widget/EditText;->getText()Landroid/text/Editable;

    move-result-object v5

    invoke-interface {v5}, Landroid/text/Editable;->toString()Ljava/lang/String;

    move-result-object v3

    new-instance v5, Ljava/lang/StringBuilder;

    const-string v6, " getConfig(): ==>password : "

    invoke-direct {v5, v6}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v5, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    invoke-virtual {p0, v5}, Lcom/konka/systemsetting/net/AccessPointConfigDialog;->log(Ljava/lang/String;)V

    new-instance v1, Landroid/net/wifi/WifiConfiguration;

    invoke-direct {v1}, Landroid/net/wifi/WifiConfiguration;-><init>()V

    iget-object v5, p0, Lcom/konka/systemsetting/net/AccessPointConfigDialog;->mResult:Landroid/net/wifi/ScanResult;

    iget-object v5, v5, Landroid/net/wifi/ScanResult;->SSID:Ljava/lang/String;

    invoke-direct {p0, v5}, Lcom/konka/systemsetting/net/AccessPointConfigDialog;->convertToQuotedString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v5

    iput-object v5, v1, Landroid/net/wifi/WifiConfiguration;->SSID:Ljava/lang/String;

    new-instance v5, Ljava/lang/StringBuilder;

    const-string v6, " getConfig(): ==>ssid : "

    invoke-direct {v5, v6}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    iget-object v6, v1, Landroid/net/wifi/WifiConfiguration;->SSID:Ljava/lang/String;

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    invoke-virtual {p0, v5}, Lcom/konka/systemsetting/net/AccessPointConfigDialog;->log(Ljava/lang/String;)V

    iget-object v5, p0, Lcom/konka/systemsetting/net/AccessPointConfigDialog;->mResult:Landroid/net/wifi/ScanResult;

    iget-object v5, v5, Landroid/net/wifi/ScanResult;->capabilities:Ljava/lang/String;

    invoke-virtual {p0, v5}, Lcom/konka/systemsetting/net/AccessPointConfigDialog;->getSecurity(Ljava/lang/String;)I

    move-result v5

    packed-switch v5, :pswitch_data_0

    move-object v1, v4

    goto :goto_0

    :pswitch_0
    const-string v4, " getConfig(): ==>getSecurity() : SECURITY_NONE"

    invoke-virtual {p0, v4}, Lcom/konka/systemsetting/net/AccessPointConfigDialog;->log(Ljava/lang/String;)V

    iget-object v4, v1, Landroid/net/wifi/WifiConfiguration;->allowedKeyManagement:Ljava/util/BitSet;

    invoke-virtual {v4, v7}, Ljava/util/BitSet;->set(I)V

    :goto_1
    sget-object v4, Landroid/net/wifi/WifiConfiguration$ProxySettings;->NONE:Landroid/net/wifi/WifiConfiguration$ProxySettings;

    iput-object v4, v1, Landroid/net/wifi/WifiConfiguration;->proxySettings:Landroid/net/wifi/WifiConfiguration$ProxySettings;

    goto :goto_0

    :pswitch_1
    const-string v5, " getConfig(): ==>getSecurity() : SECURITY_WEP"

    invoke-virtual {p0, v5}, Lcom/konka/systemsetting/net/AccessPointConfigDialog;->log(Ljava/lang/String;)V

    if-nez v3, :cond_4

    const-string v5, " getConfig(): ==>SECURITY_WEP-->password == null"

    invoke-virtual {p0, v5}, Lcom/konka/systemsetting/net/AccessPointConfigDialog;->log(Ljava/lang/String;)V

    move-object v1, v4

    goto/16 :goto_0

    :cond_4
    invoke-virtual {v3}, Ljava/lang/String;->length()I

    move-result v5

    if-eqz v5, :cond_7

    invoke-virtual {v3}, Ljava/lang/String;->length()I

    move-result v2

    const/16 v4, 0xa

    if-eq v2, v4, :cond_5

    const/16 v4, 0x1a

    if-eq v2, v4, :cond_5

    const/16 v4, 0x3a

    if-ne v2, v4, :cond_6

    :cond_5
    const-string v4, "[0-9A-Fa-f]*"

    invoke-virtual {v3, v4}, Ljava/lang/String;->matches(Ljava/lang/String;)Z

    move-result v4

    if-eqz v4, :cond_6

    iget-object v4, v1, Landroid/net/wifi/WifiConfiguration;->wepKeys:[Ljava/lang/String;

    aput-object v3, v4, v7

    :goto_2
    iget-object v4, v1, Landroid/net/wifi/WifiConfiguration;->allowedKeyManagement:Ljava/util/BitSet;

    invoke-virtual {v4, v7}, Ljava/util/BitSet;->set(I)V

    iget-object v4, v1, Landroid/net/wifi/WifiConfiguration;->allowedAuthAlgorithms:Ljava/util/BitSet;

    invoke-virtual {v4, v7}, Ljava/util/BitSet;->set(I)V

    iget-object v4, v1, Landroid/net/wifi/WifiConfiguration;->allowedAuthAlgorithms:Ljava/util/BitSet;

    invoke-virtual {v4, v8}, Ljava/util/BitSet;->set(I)V

    goto :goto_1

    :cond_6
    iget-object v4, v1, Landroid/net/wifi/WifiConfiguration;->wepKeys:[Ljava/lang/String;

    new-instance v5, Ljava/lang/StringBuilder;

    const-string v6, "\""

    invoke-direct {v5, v6}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v5, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    const-string v6, "\""

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    aput-object v5, v4, v7

    goto :goto_2

    :cond_7
    iget-object v5, p0, Lcom/konka/systemsetting/net/AccessPointConfigDialog;->mContext:Lcom/konka/systemsetting/MainActivity;

    invoke-static {v5, v9, v7}, Landroid/widget/Toast;->makeText(Landroid/content/Context;II)Landroid/widget/Toast;

    move-result-object v5

    invoke-virtual {v5}, Landroid/widget/Toast;->show()V

    move-object v1, v4

    goto/16 :goto_0

    :pswitch_2
    const-string v5, " getConfig(): ==>getSecurity() : SECURITY_PSK"

    invoke-virtual {p0, v5}, Lcom/konka/systemsetting/net/AccessPointConfigDialog;->log(Ljava/lang/String;)V

    if-nez v3, :cond_8

    const-string v5, " getConfig(): ==>SECURITY_PSK-->password == null"

    invoke-virtual {p0, v5}, Lcom/konka/systemsetting/net/AccessPointConfigDialog;->log(Ljava/lang/String;)V

    move-object v1, v4

    goto/16 :goto_0

    :cond_8
    invoke-virtual {v3}, Ljava/lang/String;->length()I

    move-result v5

    if-eqz v5, :cond_a

    const-string v4, "[0-9A-Fa-f]{64}"

    invoke-virtual {v3, v4}, Ljava/lang/String;->matches(Ljava/lang/String;)Z

    move-result v4

    if-eqz v4, :cond_9

    iput-object v3, v1, Landroid/net/wifi/WifiConfiguration;->preSharedKey:Ljava/lang/String;

    :goto_3
    iget-object v4, v1, Landroid/net/wifi/WifiConfiguration;->allowedKeyManagement:Ljava/util/BitSet;

    invoke-virtual {v4, v8}, Ljava/util/BitSet;->set(I)V

    goto/16 :goto_1

    :cond_9
    new-instance v4, Ljava/lang/StringBuilder;

    const-string v5, "\""

    invoke-direct {v4, v5}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v4, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    const-string v5, "\""

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    iput-object v4, v1, Landroid/net/wifi/WifiConfiguration;->preSharedKey:Ljava/lang/String;

    goto :goto_3

    :cond_a
    iget-object v5, p0, Lcom/konka/systemsetting/net/AccessPointConfigDialog;->mContext:Lcom/konka/systemsetting/MainActivity;

    invoke-static {v5, v9, v7}, Landroid/widget/Toast;->makeText(Landroid/content/Context;II)Landroid/widget/Toast;

    move-result-object v5

    invoke-virtual {v5}, Landroid/widget/Toast;->show()V

    move-object v1, v4

    goto/16 :goto_0

    nop

    :pswitch_data_0
    .packed-switch 0x0
        :pswitch_0
        :pswitch_1
        :pswitch_2
    .end packed-switch
.end method

.method private getConfig(Landroid/net/wifi/WifiInfo;)Landroid/net/wifi/WifiConfiguration;
    .locals 12
    .param p1    # Landroid/net/wifi/WifiInfo;

    const/4 v11, 0x1

    const/4 v7, 0x0

    const/4 v10, 0x0

    const/4 v8, -0x1

    invoke-virtual {p1}, Landroid/net/wifi/WifiInfo;->getNetworkId()I

    move-result v9

    if-eq v8, v9, :cond_0

    if-nez p1, :cond_1

    :cond_0
    const-string v8, "getConfig(WifiInfo) : currWifiInfo.getNetworkId = -1 | currWifiInfo = null"

    invoke-virtual {p0, v8}, Lcom/konka/systemsetting/net/AccessPointConfigDialog;->log(Ljava/lang/String;)V

    move-object v1, v7

    :goto_0
    return-object v1

    :cond_1
    const/4 v3, 0x0

    new-instance v1, Landroid/net/wifi/WifiConfiguration;

    invoke-direct {v1}, Landroid/net/wifi/WifiConfiguration;-><init>()V

    iget-object v8, p0, Lcom/konka/systemsetting/net/AccessPointConfigDialog;->manager:Landroid/net/wifi/WifiManager;

    invoke-virtual {v8}, Landroid/net/wifi/WifiManager;->getConfiguredNetworks()Ljava/util/List;

    move-result-object v2

    new-instance v8, Ljava/lang/StringBuilder;

    const-string v9, "\""

    invoke-direct {v8, v9}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {p1}, Landroid/net/wifi/WifiInfo;->getSSID()Ljava/lang/String;

    move-result-object v9

    invoke-virtual {v8, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v8

    const-string v9, "\""

    invoke-virtual {v8, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v8

    invoke-virtual {v8}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v6

    new-instance v8, Ljava/lang/StringBuilder;

    const-string v9, "===>configs.size() = "

    invoke-direct {v8, v9}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-interface {v2}, Ljava/util/List;->size()I

    move-result v9

    invoke-virtual {v8, v9}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v8

    invoke-virtual {v8}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v8

    invoke-virtual {p0, v8}, Lcom/konka/systemsetting/net/AccessPointConfigDialog;->log(Ljava/lang/String;)V

    invoke-interface {v2}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v8

    :cond_2
    invoke-interface {v8}, Ljava/util/Iterator;->hasNext()Z

    move-result v9

    if-nez v9, :cond_3

    :goto_1
    if-nez v3, :cond_4

    const-string v8, "mResult == null"

    invoke-virtual {p0, v8}, Lcom/konka/systemsetting/net/AccessPointConfigDialog;->log(Ljava/lang/String;)V

    move-object v1, v7

    goto :goto_0

    :cond_3
    invoke-interface {v8}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/net/wifi/WifiConfiguration;

    iget-object v9, v0, Landroid/net/wifi/WifiConfiguration;->SSID:Ljava/lang/String;

    invoke-virtual {v6, v9}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v9

    if-eqz v9, :cond_2

    move-object v3, v0

    new-instance v8, Ljava/lang/StringBuilder;

    const-string v9, "===>configs.toString() = "

    invoke-direct {v8, v9}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v0}, Landroid/net/wifi/WifiConfiguration;->toString()Ljava/lang/String;

    move-result-object v9

    invoke-virtual {v8, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v8

    invoke-virtual {v8}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v8

    invoke-virtual {p0, v8}, Lcom/konka/systemsetting/net/AccessPointConfigDialog;->log(Ljava/lang/String;)V

    goto :goto_1

    :cond_4
    invoke-direct {p0, v3}, Lcom/konka/systemsetting/net/AccessPointConfigDialog;->getSecurity(Landroid/net/wifi/WifiConfiguration;)I

    move-result v5

    packed-switch v5, :pswitch_data_0

    move-object v1, v7

    goto :goto_0

    :pswitch_0
    iget-object v7, v1, Landroid/net/wifi/WifiConfiguration;->allowedKeyManagement:Ljava/util/BitSet;

    invoke-virtual {v7, v10}, Ljava/util/BitSet;->set(I)V

    :goto_2
    iget-object v7, p0, Lcom/konka/systemsetting/net/AccessPointConfigDialog;->mHolder:Lcom/konka/systemsetting/net/AccessPointConfigDialogViewHolder;

    iget-object v7, v7, Lcom/konka/systemsetting/net/AccessPointConfigDialogViewHolder;->spIpMode:Landroid/widget/Spinner;

    invoke-virtual {v7}, Landroid/widget/Spinner;->getSelectedItemPosition()I

    move-result v4

    invoke-virtual {p1}, Landroid/net/wifi/WifiInfo;->getNetworkId()I

    move-result v7

    iput v7, v1, Landroid/net/wifi/WifiConfiguration;->networkId:I

    sget-object v7, Landroid/net/wifi/WifiConfiguration$ProxySettings;->NONE:Landroid/net/wifi/WifiConfiguration$ProxySettings;

    iput-object v7, v1, Landroid/net/wifi/WifiConfiguration;->proxySettings:Landroid/net/wifi/WifiConfiguration$ProxySettings;

    if-nez v4, :cond_5

    sget-object v7, Landroid/net/wifi/WifiConfiguration$IpAssignment;->DHCP:Landroid/net/wifi/WifiConfiguration$IpAssignment;

    :goto_3
    iput-object v7, v1, Landroid/net/wifi/WifiConfiguration;->ipAssignment:Landroid/net/wifi/WifiConfiguration$IpAssignment;

    new-instance v7, Landroid/net/LinkProperties;

    iget-object v8, p0, Lcom/konka/systemsetting/net/AccessPointConfigDialog;->mLinkProperties:Landroid/net/LinkProperties;

    invoke-direct {v7, v8}, Landroid/net/LinkProperties;-><init>(Landroid/net/LinkProperties;)V

    iput-object v7, v1, Landroid/net/wifi/WifiConfiguration;->linkProperties:Landroid/net/LinkProperties;

    goto/16 :goto_0

    :pswitch_1
    iget-object v7, v1, Landroid/net/wifi/WifiConfiguration;->allowedKeyManagement:Ljava/util/BitSet;

    invoke-virtual {v7, v10}, Ljava/util/BitSet;->set(I)V

    iget-object v7, v1, Landroid/net/wifi/WifiConfiguration;->allowedAuthAlgorithms:Ljava/util/BitSet;

    invoke-virtual {v7, v10}, Ljava/util/BitSet;->set(I)V

    iget-object v7, v1, Landroid/net/wifi/WifiConfiguration;->allowedAuthAlgorithms:Ljava/util/BitSet;

    invoke-virtual {v7, v11}, Ljava/util/BitSet;->set(I)V

    goto :goto_2

    :pswitch_2
    iget-object v7, v1, Landroid/net/wifi/WifiConfiguration;->allowedKeyManagement:Ljava/util/BitSet;

    invoke-virtual {v7, v11}, Ljava/util/BitSet;->set(I)V

    goto :goto_2

    :pswitch_3
    iget-object v7, v1, Landroid/net/wifi/WifiConfiguration;->allowedKeyManagement:Ljava/util/BitSet;

    const/4 v8, 0x2

    invoke-virtual {v7, v8}, Ljava/util/BitSet;->set(I)V

    iget-object v7, v1, Landroid/net/wifi/WifiConfiguration;->allowedKeyManagement:Ljava/util/BitSet;

    const/4 v8, 0x3

    invoke-virtual {v7, v8}, Ljava/util/BitSet;->set(I)V

    iget-object v7, v1, Landroid/net/wifi/WifiConfiguration;->eap:Landroid/net/wifi/WifiConfiguration$EnterpriseField;

    invoke-direct {p0, v1}, Lcom/konka/systemsetting/net/AccessPointConfigDialog;->getSecurityString(Landroid/net/wifi/WifiConfiguration;)Ljava/lang/String;

    move-result-object v8

    invoke-virtual {v7, v8}, Landroid/net/wifi/WifiConfiguration$EnterpriseField;->setValue(Ljava/lang/String;)V

    iget-object v7, v1, Landroid/net/wifi/WifiConfiguration;->phase2:Landroid/net/wifi/WifiConfiguration$EnterpriseField;

    const-string v8, ""

    invoke-virtual {v7, v8}, Landroid/net/wifi/WifiConfiguration$EnterpriseField;->setValue(Ljava/lang/String;)V

    iget-object v7, v1, Landroid/net/wifi/WifiConfiguration;->ca_cert:Landroid/net/wifi/WifiConfiguration$EnterpriseField;

    const-string v8, ""

    invoke-virtual {v7, v8}, Landroid/net/wifi/WifiConfiguration$EnterpriseField;->setValue(Ljava/lang/String;)V

    iget-object v7, v1, Landroid/net/wifi/WifiConfiguration;->client_cert:Landroid/net/wifi/WifiConfiguration$EnterpriseField;

    const-string v8, ""

    invoke-virtual {v7, v8}, Landroid/net/wifi/WifiConfiguration$EnterpriseField;->setValue(Ljava/lang/String;)V

    iget-object v7, v1, Landroid/net/wifi/WifiConfiguration;->key_id:Landroid/net/wifi/WifiConfiguration$EnterpriseField;

    const-string v8, ""

    invoke-virtual {v7, v8}, Landroid/net/wifi/WifiConfiguration$EnterpriseField;->setValue(Ljava/lang/String;)V

    iget-object v7, v1, Landroid/net/wifi/WifiConfiguration;->identity:Landroid/net/wifi/WifiConfiguration$EnterpriseField;

    const-string v8, ""

    invoke-virtual {v7, v8}, Landroid/net/wifi/WifiConfiguration$EnterpriseField;->setValue(Ljava/lang/String;)V

    iget-object v7, v1, Landroid/net/wifi/WifiConfiguration;->anonymous_identity:Landroid/net/wifi/WifiConfiguration$EnterpriseField;

    const-string v8, ""

    invoke-virtual {v7, v8}, Landroid/net/wifi/WifiConfiguration$EnterpriseField;->setValue(Ljava/lang/String;)V

    goto :goto_2

    :cond_5
    sget-object v7, Landroid/net/wifi/WifiConfiguration$IpAssignment;->STATIC:Landroid/net/wifi/WifiConfiguration$IpAssignment;

    goto :goto_3

    nop

    :pswitch_data_0
    .packed-switch 0x0
        :pswitch_0
        :pswitch_1
        :pswitch_2
        :pswitch_3
    .end packed-switch
.end method

.method private getSecurity(Landroid/net/wifi/WifiConfiguration;)I
    .locals 5
    .param p1    # Landroid/net/wifi/WifiConfiguration;

    const/4 v3, 0x3

    const/4 v2, 0x2

    const/4 v0, 0x1

    const/4 v1, 0x0

    iget-object v4, p1, Landroid/net/wifi/WifiConfiguration;->allowedKeyManagement:Ljava/util/BitSet;

    invoke-virtual {v4, v0}, Ljava/util/BitSet;->get(I)Z

    move-result v4

    if-eqz v4, :cond_1

    move v0, v2

    :cond_0
    :goto_0
    return v0

    :cond_1
    iget-object v4, p1, Landroid/net/wifi/WifiConfiguration;->allowedKeyManagement:Ljava/util/BitSet;

    invoke-virtual {v4, v2}, Ljava/util/BitSet;->get(I)Z

    move-result v2

    if-nez v2, :cond_2

    iget-object v2, p1, Landroid/net/wifi/WifiConfiguration;->allowedKeyManagement:Ljava/util/BitSet;

    invoke-virtual {v2, v3}, Ljava/util/BitSet;->get(I)Z

    move-result v2

    if-eqz v2, :cond_3

    :cond_2
    move v0, v3

    goto :goto_0

    :cond_3
    iget-object v2, p1, Landroid/net/wifi/WifiConfiguration;->wepKeys:[Ljava/lang/String;

    aget-object v2, v2, v1

    if-nez v2, :cond_0

    move v0, v1

    goto :goto_0
.end method

.method private getSecurityString(Landroid/net/wifi/WifiConfiguration;)Ljava/lang/String;
    .locals 2
    .param p1    # Landroid/net/wifi/WifiConfiguration;

    iget-object v0, p1, Landroid/net/wifi/WifiConfiguration;->allowedKeyManagement:Ljava/util/BitSet;

    const/4 v1, 0x1

    invoke-virtual {v0, v1}, Ljava/util/BitSet;->get(I)Z

    move-result v0

    if-eqz v0, :cond_0

    const-string v0, "WPA_PSK"

    :goto_0
    return-object v0

    :cond_0
    iget-object v0, p1, Landroid/net/wifi/WifiConfiguration;->allowedKeyManagement:Ljava/util/BitSet;

    const/4 v1, 0x2

    invoke-virtual {v0, v1}, Ljava/util/BitSet;->get(I)Z

    move-result v0

    if-nez v0, :cond_1

    iget-object v0, p1, Landroid/net/wifi/WifiConfiguration;->allowedKeyManagement:Ljava/util/BitSet;

    const/4 v1, 0x3

    invoke-virtual {v0, v1}, Ljava/util/BitSet;->get(I)Z

    move-result v0

    if-eqz v0, :cond_2

    :cond_1
    const-string v0, "WPA_EAP"

    goto :goto_0

    :cond_2
    iget-object v0, p1, Landroid/net/wifi/WifiConfiguration;->wepKeys:[Ljava/lang/String;

    const/4 v1, 0x0

    aget-object v0, v0, v1

    if-eqz v0, :cond_3

    const-string v0, "SECURITY_WEP"

    goto :goto_0

    :cond_3
    const-string v0, "SECURITY_NONE"

    goto :goto_0
.end method

.method static removeDoubleQuotes(Ljava/lang/String;)Ljava/lang/String;
    .locals 4
    .param p0    # Ljava/lang/String;

    const/16 v3, 0x22

    const/4 v2, 0x1

    if-nez p0, :cond_1

    const/4 p0, 0x0

    :cond_0
    :goto_0
    return-object p0

    :cond_1
    invoke-virtual {p0}, Ljava/lang/String;->length()I

    move-result v0

    if-le v0, v2, :cond_0

    const/4 v1, 0x0

    invoke-virtual {p0, v1}, Ljava/lang/String;->charAt(I)C

    move-result v1

    if-ne v1, v3, :cond_0

    add-int/lit8 v1, v0, -0x1

    invoke-virtual {p0, v1}, Ljava/lang/String;->charAt(I)C

    move-result v1

    if-ne v1, v3, :cond_0

    add-int/lit8 v1, v0, -0x1

    invoke-virtual {p0, v2, v1}, Ljava/lang/String;->substring(II)Ljava/lang/String;

    move-result-object p0

    goto :goto_0
.end method

.method private setStaticIP()Z
    .locals 12

    const/4 v8, 0x0

    const-string v9, "setStaticIP "

    invoke-virtual {p0, v9}, Lcom/konka/systemsetting/net/AccessPointConfigDialog;->log(Ljava/lang/String;)V

    iget-object v9, p0, Lcom/konka/systemsetting/net/AccessPointConfigDialog;->mLinkProperties:Landroid/net/LinkProperties;

    invoke-virtual {v9}, Landroid/net/LinkProperties;->clear()V

    iget-object v9, p0, Lcom/konka/systemsetting/net/AccessPointConfigDialog;->mHolder:Lcom/konka/systemsetting/net/AccessPointConfigDialogViewHolder;

    iget-object v9, v9, Lcom/konka/systemsetting/net/AccessPointConfigDialogViewHolder;->etIpAddr:Landroid/widget/EditText;

    invoke-virtual {v9}, Landroid/widget/EditText;->getText()Landroid/text/Editable;

    move-result-object v9

    invoke-interface {v9}, Landroid/text/Editable;->toString()Ljava/lang/String;

    move-result-object v9

    invoke-virtual {v9}, Ljava/lang/String;->trim()Ljava/lang/String;

    move-result-object v6

    iget-object v9, p0, Lcom/konka/systemsetting/net/AccessPointConfigDialog;->mHolder:Lcom/konka/systemsetting/net/AccessPointConfigDialogViewHolder;

    iget-object v9, v9, Lcom/konka/systemsetting/net/AccessPointConfigDialogViewHolder;->etGateway:Landroid/widget/EditText;

    invoke-virtual {v9}, Landroid/widget/EditText;->getText()Landroid/text/Editable;

    move-result-object v9

    invoke-interface {v9}, Landroid/text/Editable;->toString()Ljava/lang/String;

    move-result-object v9

    invoke-virtual {v9}, Ljava/lang/String;->trim()Ljava/lang/String;

    move-result-object v3

    iget-object v9, p0, Lcom/konka/systemsetting/net/AccessPointConfigDialog;->mHolder:Lcom/konka/systemsetting/net/AccessPointConfigDialogViewHolder;

    iget-object v9, v9, Lcom/konka/systemsetting/net/AccessPointConfigDialogViewHolder;->etDns1:Landroid/widget/EditText;

    invoke-virtual {v9}, Landroid/widget/EditText;->getText()Landroid/text/Editable;

    move-result-object v9

    invoke-interface {v9}, Landroid/text/Editable;->toString()Ljava/lang/String;

    move-result-object v9

    invoke-virtual {v9}, Ljava/lang/String;->trim()Ljava/lang/String;

    move-result-object v0

    const-string v7, ""

    invoke-static {v6}, Lcom/konka/utilities/Tools;->matchIP(Ljava/lang/String;)Z

    move-result v9

    if-nez v9, :cond_1

    new-instance v9, Ljava/lang/StringBuilder;

    invoke-static {v7}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v10

    invoke-direct {v9, v10}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    const v10, 0x7f060063    # com.konka.systemsetting.R.string.check_ip_failure

    invoke-virtual {p0, v10}, Lcom/konka/systemsetting/net/AccessPointConfigDialog;->getText(I)Ljava/lang/String;

    move-result-object v10

    invoke-virtual {v9, v10}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v9

    invoke-virtual {v9}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v7

    :cond_0
    :goto_0
    const-string v9, ""

    if-eq v7, v9, :cond_3

    invoke-direct {p0, v7}, Lcom/konka/systemsetting/net/AccessPointConfigDialog;->toastText(Ljava/lang/String;)V

    :goto_1
    return v8

    :cond_1
    invoke-static {v3}, Lcom/konka/utilities/Tools;->matchIP(Ljava/lang/String;)Z

    move-result v9

    if-nez v9, :cond_2

    new-instance v9, Ljava/lang/StringBuilder;

    invoke-static {v7}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v10

    invoke-direct {v9, v10}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    const v10, 0x7f060064    # com.konka.systemsetting.R.string.check_default_failure

    invoke-virtual {p0, v10}, Lcom/konka/systemsetting/net/AccessPointConfigDialog;->getText(I)Ljava/lang/String;

    move-result-object v10

    invoke-virtual {v9, v10}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v9

    invoke-virtual {v9}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v7

    goto :goto_0

    :cond_2
    invoke-static {v0}, Lcom/konka/utilities/Tools;->matchIP(Ljava/lang/String;)Z

    move-result v9

    if-nez v9, :cond_0

    new-instance v9, Ljava/lang/StringBuilder;

    invoke-static {v7}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v10

    invoke-direct {v9, v10}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    const v10, 0x7f060066    # com.konka.systemsetting.R.string.check_dns_failure

    invoke-virtual {p0, v10}, Lcom/konka/systemsetting/net/AccessPointConfigDialog;->getText(I)Ljava/lang/String;

    move-result-object v10

    invoke-virtual {v9, v10}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v9

    invoke-virtual {v9}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v7

    goto :goto_0

    :cond_3
    const/4 v5, 0x0

    :try_start_0
    invoke-static {v6}, Landroid/net/NetworkUtils;->numericToInetAddress(Ljava/lang/String;)Ljava/net/InetAddress;

    move-result-object v5

    new-instance v9, Ljava/lang/StringBuilder;

    const-string v10, "setStaticIP--> inetAddr"

    invoke-direct {v9, v10}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v5}, Ljava/net/InetAddress;->toString()Ljava/lang/String;

    move-result-object v10

    invoke-virtual {v9, v10}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v9

    invoke-virtual {v9}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v9

    invoke-virtual {p0, v9}, Lcom/konka/systemsetting/net/AccessPointConfigDialog;->log(Ljava/lang/String;)V
    :try_end_0
    .catch Ljava/lang/IllegalArgumentException; {:try_start_0 .. :try_end_0} :catch_0

    iget-object v9, p0, Lcom/konka/systemsetting/net/AccessPointConfigDialog;->mLinkProperties:Landroid/net/LinkProperties;

    new-instance v10, Landroid/net/LinkAddress;

    const/16 v11, 0x18

    invoke-direct {v10, v5, v11}, Landroid/net/LinkAddress;-><init>(Ljava/net/InetAddress;I)V

    invoke-virtual {v9, v10}, Landroid/net/LinkProperties;->addLinkAddress(Landroid/net/LinkAddress;)V

    const/4 v4, 0x0

    :try_start_1
    invoke-static {v3}, Landroid/net/NetworkUtils;->numericToInetAddress(Ljava/lang/String;)Ljava/net/InetAddress;

    move-result-object v4

    new-instance v9, Ljava/lang/StringBuilder;

    const-string v10, "setStaticIP--> gatewayAddr"

    invoke-direct {v9, v10}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v4}, Ljava/net/InetAddress;->toString()Ljava/lang/String;

    move-result-object v10

    invoke-virtual {v9, v10}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v9

    invoke-virtual {v9}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v9

    invoke-virtual {p0, v9}, Lcom/konka/systemsetting/net/AccessPointConfigDialog;->log(Ljava/lang/String;)V
    :try_end_1
    .catch Ljava/lang/IllegalArgumentException; {:try_start_1 .. :try_end_1} :catch_1

    iget-object v9, p0, Lcom/konka/systemsetting/net/AccessPointConfigDialog;->mLinkProperties:Landroid/net/LinkProperties;

    new-instance v10, Landroid/net/RouteInfo;

    invoke-direct {v10, v4}, Landroid/net/RouteInfo;-><init>(Ljava/net/InetAddress;)V

    invoke-virtual {v9, v10}, Landroid/net/LinkProperties;->addRoute(Landroid/net/RouteInfo;)V

    const/4 v1, 0x0

    :try_start_2
    invoke-static {v0}, Landroid/net/NetworkUtils;->numericToInetAddress(Ljava/lang/String;)Ljava/net/InetAddress;

    move-result-object v1

    new-instance v9, Ljava/lang/StringBuilder;

    const-string v10, "setStaticIP--> dnsAddr"

    invoke-direct {v9, v10}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v1}, Ljava/net/InetAddress;->toString()Ljava/lang/String;

    move-result-object v10

    invoke-virtual {v9, v10}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v9

    invoke-virtual {v9}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v9

    invoke-virtual {p0, v9}, Lcom/konka/systemsetting/net/AccessPointConfigDialog;->log(Ljava/lang/String;)V
    :try_end_2
    .catch Ljava/lang/IllegalArgumentException; {:try_start_2 .. :try_end_2} :catch_2

    iget-object v8, p0, Lcom/konka/systemsetting/net/AccessPointConfigDialog;->mLinkProperties:Landroid/net/LinkProperties;

    invoke-virtual {v8, v1}, Landroid/net/LinkProperties;->addDns(Ljava/net/InetAddress;)V

    const/4 v8, 0x1

    goto/16 :goto_1

    :catch_0
    move-exception v2

    const-string v9, "Invalid ip"

    invoke-virtual {p0, v9}, Lcom/konka/systemsetting/net/AccessPointConfigDialog;->logError(Ljava/lang/String;)V

    goto/16 :goto_1

    :catch_1
    move-exception v2

    const-string v9, "Invalid gatewayAddr"

    invoke-virtual {p0, v9}, Lcom/konka/systemsetting/net/AccessPointConfigDialog;->logError(Ljava/lang/String;)V

    goto/16 :goto_1

    :catch_2
    move-exception v2

    const-string v9, "Invalid dns"

    invoke-virtual {p0, v9}, Lcom/konka/systemsetting/net/AccessPointConfigDialog;->logError(Ljava/lang/String;)V

    goto/16 :goto_1
.end method

.method private toastText(Ljava/lang/String;)V
    .locals 1
    .param p1    # Ljava/lang/String;

    iget-object v0, p0, Lcom/konka/systemsetting/net/AccessPointConfigDialog;->mContext:Lcom/konka/systemsetting/MainActivity;

    invoke-virtual {v0, p1}, Lcom/konka/systemsetting/MainActivity;->toastText(Ljava/lang/String;)V

    return-void
.end method


# virtual methods
.method public dismiss()V
    .locals 2

    iget-object v0, p0, Lcom/konka/systemsetting/net/AccessPointConfigDialog;->mContext:Lcom/konka/systemsetting/MainActivity;

    iget-object v0, v0, Lcom/konka/systemsetting/MainActivity;->m_networkPageManager:Lcom/konka/systemsetting/net/NetworkPageManager;

    iget-object v0, v0, Lcom/konka/systemsetting/net/NetworkPageManager;->m_WifiConfig:Lcom/konka/systemsetting/net/NetworkWifiConfig;

    const/4 v1, 0x0

    iput-boolean v1, v0, Lcom/konka/systemsetting/net/NetworkWifiConfig;->isAPSetting:Z

    invoke-super {p0}, Landroid/app/AlertDialog;->dismiss()V

    return-void
.end method

.method public getSecurity(Ljava/lang/String;)I
    .locals 1
    .param p1    # Ljava/lang/String;

    const-string v0, "WEP"

    invoke-virtual {p1, v0}, Ljava/lang/String;->contains(Ljava/lang/CharSequence;)Z

    move-result v0

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const-string v0, "PSK"

    invoke-virtual {p1, v0}, Ljava/lang/String;->contains(Ljava/lang/CharSequence;)Z

    move-result v0

    if-eqz v0, :cond_1

    const/4 v0, 0x2

    goto :goto_0

    :cond_1
    const-string v0, "EAP"

    invoke-virtual {p1, v0}, Ljava/lang/String;->contains(Ljava/lang/CharSequence;)Z

    move-result v0

    if-eqz v0, :cond_2

    const/4 v0, 0x3

    goto :goto_0

    :cond_2
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public getSecurityString(Ljava/lang/String;)Ljava/lang/String;
    .locals 1
    .param p1    # Ljava/lang/String;

    const-string v0, "WEP"

    invoke-virtual {p1, v0}, Ljava/lang/String;->contains(Ljava/lang/CharSequence;)Z

    move-result v0

    if-eqz v0, :cond_0

    const-string v0, "WEP"

    :goto_0
    return-object v0

    :cond_0
    const-string v0, "PSK"

    invoke-virtual {p1, v0}, Ljava/lang/String;->contains(Ljava/lang/CharSequence;)Z

    move-result v0

    if-eqz v0, :cond_1

    const-string v0, "WPA/WPA2 PSK"

    goto :goto_0

    :cond_1
    const-string v0, "EAP"

    invoke-virtual {p1, v0}, Ljava/lang/String;->contains(Ljava/lang/CharSequence;)Z

    move-result v0

    if-eqz v0, :cond_2

    const-string v0, "EAP"

    goto :goto_0

    :cond_2
    const-string v0, "OPEN"

    goto :goto_0
.end method

.method public getSignalStrength()Ljava/lang/String;
    .locals 4

    iget-object v2, p0, Lcom/konka/systemsetting/net/AccessPointConfigDialog;->mResult:Landroid/net/wifi/ScanResult;

    iget v2, v2, Landroid/net/wifi/ScanResult;->level:I

    const/4 v3, 0x7

    invoke-static {v2, v3}, Landroid/net/wifi/WifiManager;->calculateSignalLevel(II)I

    move-result v0

    const-string v1, ""

    packed-switch v0, :pswitch_data_0

    :goto_0
    return-object v1

    :pswitch_0
    const v2, 0x7f0600c9    # com.konka.systemsetting.R.string.wifi_signal_weak

    invoke-virtual {p0, v2}, Lcom/konka/systemsetting/net/AccessPointConfigDialog;->getText(I)Ljava/lang/String;

    move-result-object v1

    goto :goto_0

    :pswitch_1
    const v2, 0x7f0600ca    # com.konka.systemsetting.R.string.wifi_signal_normal

    invoke-virtual {p0, v2}, Lcom/konka/systemsetting/net/AccessPointConfigDialog;->getText(I)Ljava/lang/String;

    move-result-object v1

    goto :goto_0

    :pswitch_2
    const v2, 0x7f0600cb    # com.konka.systemsetting.R.string.wifi_signal_less

    invoke-virtual {p0, v2}, Lcom/konka/systemsetting/net/AccessPointConfigDialog;->getText(I)Ljava/lang/String;

    move-result-object v1

    goto :goto_0

    :pswitch_3
    const v2, 0x7f0600cc    # com.konka.systemsetting.R.string.wifi_signal_strong

    invoke-virtual {p0, v2}, Lcom/konka/systemsetting/net/AccessPointConfigDialog;->getText(I)Ljava/lang/String;

    move-result-object v1

    goto :goto_0

    nop

    :pswitch_data_0
    .packed-switch 0x0
        :pswitch_0
        :pswitch_1
        :pswitch_1
        :pswitch_2
        :pswitch_2
        :pswitch_3
        :pswitch_3
    .end packed-switch
.end method

.method getText(I)Ljava/lang/String;
    .locals 1
    .param p1    # I

    iget-object v0, p0, Lcom/konka/systemsetting/net/AccessPointConfigDialog;->mContext:Lcom/konka/systemsetting/MainActivity;

    invoke-virtual {v0, p1}, Lcom/konka/systemsetting/MainActivity;->getText(I)Ljava/lang/CharSequence;

    move-result-object v0

    invoke-interface {v0}, Ljava/lang/CharSequence;->toString()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method log(Ljava/lang/String;)V
    .locals 1
    .param p1    # Ljava/lang/String;

    const-string v0, "==========>Net_Wifi_Cfg==>APConfigDialog"

    invoke-static {v0, p1}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    return-void
.end method

.method logError(Ljava/lang/String;)V
    .locals 1
    .param p1    # Ljava/lang/String;

    const-string v0, "==========>Net_Wifi_Cfg==>APConfigDialog"

    invoke-static {v0, p1}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    return-void
.end method

.method public onCancel()V
    .locals 0

    invoke-virtual {p0}, Lcom/konka/systemsetting/net/AccessPointConfigDialog;->dismiss()V

    return-void
.end method

.method public onConnect()V
    .locals 4

    invoke-direct {p0}, Lcom/konka/systemsetting/net/AccessPointConfigDialog;->getConfig()Landroid/net/wifi/WifiConfiguration;

    move-result-object v0

    if-nez v0, :cond_0

    const-string v2, " onConnect: error, config = null !!!"

    invoke-virtual {p0, v2}, Lcom/konka/systemsetting/net/AccessPointConfigDialog;->log(Ljava/lang/String;)V

    iget-object v2, p0, Lcom/konka/systemsetting/net/AccessPointConfigDialog;->mHolder:Lcom/konka/systemsetting/net/AccessPointConfigDialogViewHolder;

    iget-object v2, v2, Lcom/konka/systemsetting/net/AccessPointConfigDialogViewHolder;->etPassword:Landroid/widget/EditText;

    invoke-virtual {v2}, Landroid/widget/EditText;->requestFocus()Z

    :goto_0
    return-void

    :cond_0
    new-instance v2, Ljava/lang/StringBuilder;

    const-string v3, " onConnect: connectNetwork(Config); Config_Details:\n"

    invoke-direct {v2, v3}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v0}, Landroid/net/wifi/WifiConfiguration;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {p0, v2}, Lcom/konka/systemsetting/net/AccessPointConfigDialog;->log(Ljava/lang/String;)V

    iget-object v2, p0, Lcom/konka/systemsetting/net/AccessPointConfigDialog;->mHolder:Lcom/konka/systemsetting/net/AccessPointConfigDialogViewHolder;

    iget-object v2, v2, Lcom/konka/systemsetting/net/AccessPointConfigDialogViewHolder;->spIpMode:Landroid/widget/Spinner;

    invoke-virtual {v2}, Landroid/widget/Spinner;->getSelectedItemPosition()I

    move-result v1

    if-nez v1, :cond_2

    sget-object v2, Landroid/net/wifi/WifiConfiguration$IpAssignment;->DHCP:Landroid/net/wifi/WifiConfiguration$IpAssignment;

    :goto_1
    iput-object v2, v0, Landroid/net/wifi/WifiConfiguration;->ipAssignment:Landroid/net/wifi/WifiConfiguration$IpAssignment;

    const/4 v2, 0x1

    if-ne v1, v2, :cond_1

    sget-object v2, Landroid/net/wifi/WifiConfiguration$ProxySettings;->NONE:Landroid/net/wifi/WifiConfiguration$ProxySettings;

    iput-object v2, v0, Landroid/net/wifi/WifiConfiguration;->proxySettings:Landroid/net/wifi/WifiConfiguration$ProxySettings;

    invoke-direct {p0}, Lcom/konka/systemsetting/net/AccessPointConfigDialog;->setStaticIP()Z

    new-instance v2, Landroid/net/LinkProperties;

    iget-object v3, p0, Lcom/konka/systemsetting/net/AccessPointConfigDialog;->mLinkProperties:Landroid/net/LinkProperties;

    invoke-direct {v2, v3}, Landroid/net/LinkProperties;-><init>(Landroid/net/LinkProperties;)V

    iput-object v2, v0, Landroid/net/wifi/WifiConfiguration;->linkProperties:Landroid/net/LinkProperties;

    :cond_1
    iget-object v2, p0, Lcom/konka/systemsetting/net/AccessPointConfigDialog;->manager:Landroid/net/wifi/WifiManager;

    iget-object v3, p0, Lcom/konka/systemsetting/net/AccessPointConfigDialog;->mConnectListener:Landroid/net/wifi/WifiManager$ActionListener;

    invoke-virtual {v2, v0, v3}, Landroid/net/wifi/WifiManager;->connect(Landroid/net/wifi/WifiConfiguration;Landroid/net/wifi/WifiManager$ActionListener;)V

    invoke-virtual {p0}, Lcom/konka/systemsetting/net/AccessPointConfigDialog;->dismiss()V

    goto :goto_0

    :cond_2
    sget-object v2, Landroid/net/wifi/WifiConfiguration$IpAssignment;->STATIC:Landroid/net/wifi/WifiConfiguration$IpAssignment;

    goto :goto_1
.end method

.method public onForget()V
    .locals 3

    iget-boolean v0, p0, Lcom/konka/systemsetting/net/AccessPointConfigDialog;->isConfiged:Z

    if-nez v0, :cond_0

    iget-boolean v0, p0, Lcom/konka/systemsetting/net/AccessPointConfigDialog;->isConnected:Z

    if-eqz v0, :cond_1

    :cond_0
    new-instance v0, Ljava/lang/StringBuilder;

    const-string v1, " onForget: forgetNetowrk(NetworkId); NetworkId = "

    invoke-direct {v0, v1}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    iget v1, p0, Lcom/konka/systemsetting/net/AccessPointConfigDialog;->networkId:I

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p0, v0}, Lcom/konka/systemsetting/net/AccessPointConfigDialog;->log(Ljava/lang/String;)V

    iget-object v0, p0, Lcom/konka/systemsetting/net/AccessPointConfigDialog;->manager:Landroid/net/wifi/WifiManager;

    iget v1, p0, Lcom/konka/systemsetting/net/AccessPointConfigDialog;->networkId:I

    iget-object v2, p0, Lcom/konka/systemsetting/net/AccessPointConfigDialog;->mForgetListener:Landroid/net/wifi/WifiManager$ActionListener;

    invoke-virtual {v0, v1, v2}, Landroid/net/wifi/WifiManager;->forget(ILandroid/net/wifi/WifiManager$ActionListener;)V

    :goto_0
    invoke-virtual {p0}, Lcom/konka/systemsetting/net/AccessPointConfigDialog;->dismiss()V

    return-void

    :cond_1
    const-string v0, " onForget: error!!! you cann\'t forget a network never save on your device."

    invoke-virtual {p0, v0}, Lcom/konka/systemsetting/net/AccessPointConfigDialog;->log(Ljava/lang/String;)V

    goto :goto_0
.end method

.method public onSave()V
    .locals 4

    invoke-direct {p0}, Lcom/konka/systemsetting/net/AccessPointConfigDialog;->getConfig()Landroid/net/wifi/WifiConfiguration;

    move-result-object v0

    iget-object v2, p0, Lcom/konka/systemsetting/net/AccessPointConfigDialog;->mHolder:Lcom/konka/systemsetting/net/AccessPointConfigDialogViewHolder;

    iget-object v2, v2, Lcom/konka/systemsetting/net/AccessPointConfigDialogViewHolder;->spIpMode:Landroid/widget/Spinner;

    invoke-virtual {v2}, Landroid/widget/Spinner;->getSelectedItemPosition()I

    move-result v1

    if-nez v1, :cond_1

    sget-object v2, Landroid/net/wifi/WifiConfiguration$IpAssignment;->DHCP:Landroid/net/wifi/WifiConfiguration$IpAssignment;

    :goto_0
    iput-object v2, v0, Landroid/net/wifi/WifiConfiguration;->ipAssignment:Landroid/net/wifi/WifiConfiguration$IpAssignment;

    const/4 v2, 0x1

    if-ne v1, v2, :cond_0

    sget-object v2, Landroid/net/wifi/WifiConfiguration$ProxySettings;->NONE:Landroid/net/wifi/WifiConfiguration$ProxySettings;

    iput-object v2, v0, Landroid/net/wifi/WifiConfiguration;->proxySettings:Landroid/net/wifi/WifiConfiguration$ProxySettings;

    invoke-direct {p0}, Lcom/konka/systemsetting/net/AccessPointConfigDialog;->setStaticIP()Z

    new-instance v2, Landroid/net/LinkProperties;

    iget-object v3, p0, Lcom/konka/systemsetting/net/AccessPointConfigDialog;->mLinkProperties:Landroid/net/LinkProperties;

    invoke-direct {v2, v3}, Landroid/net/LinkProperties;-><init>(Landroid/net/LinkProperties;)V

    iput-object v2, v0, Landroid/net/wifi/WifiConfiguration;->linkProperties:Landroid/net/LinkProperties;

    :cond_0
    iget-object v2, p0, Lcom/konka/systemsetting/net/AccessPointConfigDialog;->manager:Landroid/net/wifi/WifiManager;

    iget-object v3, p0, Lcom/konka/systemsetting/net/AccessPointConfigDialog;->mSaveListener:Landroid/net/wifi/WifiManager$ActionListener;

    invoke-virtual {v2, v0, v3}, Landroid/net/wifi/WifiManager;->save(Landroid/net/wifi/WifiConfiguration;Landroid/net/wifi/WifiManager$ActionListener;)V

    invoke-virtual {p0}, Lcom/konka/systemsetting/net/AccessPointConfigDialog;->dismiss()V

    return-void

    :cond_1
    sget-object v2, Landroid/net/wifi/WifiConfiguration$IpAssignment;->STATIC:Landroid/net/wifi/WifiConfiguration$IpAssignment;

    goto :goto_0
.end method

.method public setIpModeAuto(Z)Z
    .locals 6
    .param p1    # Z

    const/4 v3, 0x0

    new-instance v4, Ljava/lang/StringBuilder;

    const-string v5, "setIpModeAuto : "

    invoke-direct {v4, v5}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v4, p1}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {p0, v1}, Lcom/konka/systemsetting/net/AccessPointConfigDialog;->log(Ljava/lang/String;)V

    iget-object v4, p0, Lcom/konka/systemsetting/net/AccessPointConfigDialog;->mContext:Lcom/konka/systemsetting/MainActivity;

    iget-object v4, v4, Lcom/konka/systemsetting/MainActivity;->m_networkPageManager:Lcom/konka/systemsetting/net/NetworkPageManager;

    iget-object v4, v4, Lcom/konka/systemsetting/net/NetworkPageManager;->m_PppoeConfig:Lcom/konka/systemsetting/net/NetworkPPPoeConfig;

    if-eqz v4, :cond_0

    iget-object v4, p0, Lcom/konka/systemsetting/net/AccessPointConfigDialog;->mContext:Lcom/konka/systemsetting/MainActivity;

    iget-object v4, v4, Lcom/konka/systemsetting/MainActivity;->m_networkPageManager:Lcom/konka/systemsetting/net/NetworkPageManager;

    iget-object v4, v4, Lcom/konka/systemsetting/net/NetworkPageManager;->m_PppoeConfig:Lcom/konka/systemsetting/net/NetworkPPPoeConfig;

    invoke-virtual {v4}, Lcom/konka/systemsetting/net/NetworkPPPoeConfig;->isPppoeActive()Z

    move-result v4

    if-eqz v4, :cond_0

    new-instance v4, Ljava/lang/StringBuilder;

    invoke-static {v1}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v5

    invoke-direct {v4, v5}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    const-string v5, "===> pppoe has connected"

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {p0, v4}, Lcom/konka/systemsetting/net/AccessPointConfigDialog;->log(Ljava/lang/String;)V

    :goto_0
    return v3

    :cond_0
    iget-object v4, p0, Lcom/konka/systemsetting/net/AccessPointConfigDialog;->manager:Landroid/net/wifi/WifiManager;

    invoke-virtual {v4}, Landroid/net/wifi/WifiManager;->isWifiEnabled()Z

    move-result v4

    if-nez v4, :cond_1

    new-instance v4, Ljava/lang/StringBuilder;

    invoke-static {v1}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v5

    invoke-direct {v4, v5}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    const-string v5, " Wifi is not enabled yet !!!"

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {p0, v4}, Lcom/konka/systemsetting/net/AccessPointConfigDialog;->log(Ljava/lang/String;)V

    goto :goto_0

    :cond_1
    iget-object v4, p0, Lcom/konka/systemsetting/net/AccessPointConfigDialog;->manager:Landroid/net/wifi/WifiManager;

    invoke-virtual {v4}, Landroid/net/wifi/WifiManager;->getConnectionInfo()Landroid/net/wifi/WifiInfo;

    move-result-object v2

    new-instance v4, Ljava/lang/StringBuilder;

    const-string v5, "setIpModeAuto-getConnectionInfo:"

    invoke-direct {v4, v5}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v2}, Landroid/net/wifi/WifiInfo;->toString()Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {p0, v4}, Lcom/konka/systemsetting/net/AccessPointConfigDialog;->log(Ljava/lang/String;)V

    invoke-direct {p0, v2}, Lcom/konka/systemsetting/net/AccessPointConfigDialog;->getConfig(Landroid/net/wifi/WifiInfo;)Landroid/net/wifi/WifiConfiguration;

    move-result-object v0

    if-nez v0, :cond_2

    new-instance v4, Ljava/lang/StringBuilder;

    invoke-static {v1}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v5

    invoke-direct {v4, v5}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    const-string v5, "config is null !!!"

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {p0, v4}, Lcom/konka/systemsetting/net/AccessPointConfigDialog;->log(Ljava/lang/String;)V

    goto :goto_0

    :cond_2
    new-instance v4, Ljava/lang/StringBuilder;

    const-string v5, "setIpModeAuto-getConfig():"

    invoke-direct {v4, v5}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v0}, Landroid/net/wifi/WifiConfiguration;->toString()Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {p0, v4}, Lcom/konka/systemsetting/net/AccessPointConfigDialog;->log(Ljava/lang/String;)V

    if-eqz p1, :cond_3

    const-string v3, "setIpModeAuto-saveNetwork(cfg): cfg.ipAssignment = DHCP."

    invoke-virtual {p0, v3}, Lcom/konka/systemsetting/net/AccessPointConfigDialog;->log(Ljava/lang/String;)V

    sget-object v3, Landroid/net/wifi/WifiConfiguration$IpAssignment;->DHCP:Landroid/net/wifi/WifiConfiguration$IpAssignment;

    iput-object v3, v0, Landroid/net/wifi/WifiConfiguration;->ipAssignment:Landroid/net/wifi/WifiConfiguration$IpAssignment;

    :goto_1
    const/4 v3, 0x1

    goto :goto_0

    :cond_3
    sget-object v4, Landroid/net/wifi/WifiConfiguration$IpAssignment;->STATIC:Landroid/net/wifi/WifiConfiguration$IpAssignment;

    iput-object v4, v0, Landroid/net/wifi/WifiConfiguration;->ipAssignment:Landroid/net/wifi/WifiConfiguration$IpAssignment;

    invoke-direct {p0}, Lcom/konka/systemsetting/net/AccessPointConfigDialog;->setStaticIP()Z

    move-result v4

    if-eqz v4, :cond_4

    const-string v3, "setIpModeAuto-saveNetwork(cfg): cfg.ipAssignment = STATIC."

    invoke-virtual {p0, v3}, Lcom/konka/systemsetting/net/AccessPointConfigDialog;->log(Ljava/lang/String;)V

    new-instance v3, Landroid/net/LinkProperties;

    iget-object v4, p0, Lcom/konka/systemsetting/net/AccessPointConfigDialog;->mLinkProperties:Landroid/net/LinkProperties;

    invoke-direct {v3, v4}, Landroid/net/LinkProperties;-><init>(Landroid/net/LinkProperties;)V

    iput-object v3, v0, Landroid/net/wifi/WifiConfiguration;->linkProperties:Landroid/net/LinkProperties;

    goto :goto_1

    :cond_4
    const-string v4, "setIpModeAuto-configurationStaticIP(): error!!!"

    invoke-virtual {p0, v4}, Lcom/konka/systemsetting/net/AccessPointConfigDialog;->log(Ljava/lang/String;)V

    goto/16 :goto_0
.end method
