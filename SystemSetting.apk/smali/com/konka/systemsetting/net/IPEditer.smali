.class public Lcom/konka/systemsetting/net/IPEditer;
.super Ljava/lang/Object;
.source "IPEditer.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/konka/systemsetting/net/IPEditer$ESTATE;
    }
.end annotation


# instance fields
.field private mContext:Landroid/app/Activity;

.field private mCurrField:I

.field private mCurrPos:I

.field private mEditText:Landroid/widget/EditText;

.field private mSelectedEnd:I

.field private mSelectedStart:I

.field private onFocusChangeListener:Landroid/view/View$OnFocusChangeListener;

.field private onKeyListener:Landroid/view/View$OnKeyListener;

.field private pos_fstDot:I

.field private pos_sndDot:I

.field private pos_thdDot:I

.field private strIP:Ljava/lang/String;

.field private strfield1:Ljava/lang/String;

.field private strfield2:Ljava/lang/String;

.field private strfield3:Ljava/lang/String;

.field private strfield4:Ljava/lang/String;

.field private textWatcher:Landroid/text/TextWatcher;


# direct methods
.method public constructor <init>(Landroid/app/Activity;)V
    .locals 2
    .param p1    # Landroid/app/Activity;

    const/4 v1, 0x0

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    const-string v0, "0.0.0.0"

    iput-object v0, p0, Lcom/konka/systemsetting/net/IPEditer;->strIP:Ljava/lang/String;

    iput v1, p0, Lcom/konka/systemsetting/net/IPEditer;->pos_fstDot:I

    iput v1, p0, Lcom/konka/systemsetting/net/IPEditer;->pos_sndDot:I

    iput v1, p0, Lcom/konka/systemsetting/net/IPEditer;->pos_thdDot:I

    const-string v0, ""

    iput-object v0, p0, Lcom/konka/systemsetting/net/IPEditer;->strfield1:Ljava/lang/String;

    const-string v0, ""

    iput-object v0, p0, Lcom/konka/systemsetting/net/IPEditer;->strfield2:Ljava/lang/String;

    const-string v0, ""

    iput-object v0, p0, Lcom/konka/systemsetting/net/IPEditer;->strfield3:Ljava/lang/String;

    const-string v0, ""

    iput-object v0, p0, Lcom/konka/systemsetting/net/IPEditer;->strfield4:Ljava/lang/String;

    const/4 v0, 0x1

    iput v0, p0, Lcom/konka/systemsetting/net/IPEditer;->mCurrField:I

    iput v1, p0, Lcom/konka/systemsetting/net/IPEditer;->mSelectedStart:I

    iput v1, p0, Lcom/konka/systemsetting/net/IPEditer;->mSelectedEnd:I

    iput v1, p0, Lcom/konka/systemsetting/net/IPEditer;->mCurrPos:I

    new-instance v0, Lcom/konka/systemsetting/net/IPEditer$1;

    invoke-direct {v0, p0}, Lcom/konka/systemsetting/net/IPEditer$1;-><init>(Lcom/konka/systemsetting/net/IPEditer;)V

    iput-object v0, p0, Lcom/konka/systemsetting/net/IPEditer;->onFocusChangeListener:Landroid/view/View$OnFocusChangeListener;

    new-instance v0, Lcom/konka/systemsetting/net/IPEditer$2;

    invoke-direct {v0, p0}, Lcom/konka/systemsetting/net/IPEditer$2;-><init>(Lcom/konka/systemsetting/net/IPEditer;)V

    iput-object v0, p0, Lcom/konka/systemsetting/net/IPEditer;->textWatcher:Landroid/text/TextWatcher;

    new-instance v0, Lcom/konka/systemsetting/net/IPEditer$3;

    invoke-direct {v0, p0}, Lcom/konka/systemsetting/net/IPEditer$3;-><init>(Lcom/konka/systemsetting/net/IPEditer;)V

    iput-object v0, p0, Lcom/konka/systemsetting/net/IPEditer;->onKeyListener:Landroid/view/View$OnKeyListener;

    iput-object p1, p0, Lcom/konka/systemsetting/net/IPEditer;->mContext:Landroid/app/Activity;

    return-void
.end method

.method static synthetic access$0(Lcom/konka/systemsetting/net/IPEditer;)I
    .locals 1

    iget v0, p0, Lcom/konka/systemsetting/net/IPEditer;->mCurrField:I

    return v0
.end method

.method static synthetic access$1(Lcom/konka/systemsetting/net/IPEditer;Ljava/lang/String;)V
    .locals 0

    iput-object p1, p0, Lcom/konka/systemsetting/net/IPEditer;->strIP:Ljava/lang/String;

    return-void
.end method

.method static synthetic access$10(Lcom/konka/systemsetting/net/IPEditer;)Landroid/app/Activity;
    .locals 1

    iget-object v0, p0, Lcom/konka/systemsetting/net/IPEditer;->mContext:Landroid/app/Activity;

    return-object v0
.end method

.method static synthetic access$11(Lcom/konka/systemsetting/net/IPEditer;)V
    .locals 0

    invoke-direct {p0}, Lcom/konka/systemsetting/net/IPEditer;->keyBoardLeft()V

    return-void
.end method

.method static synthetic access$12(Lcom/konka/systemsetting/net/IPEditer;)V
    .locals 0

    invoke-direct {p0}, Lcom/konka/systemsetting/net/IPEditer;->keyBoardRight()V

    return-void
.end method

.method static synthetic access$13(Lcom/konka/systemsetting/net/IPEditer;)I
    .locals 1

    iget v0, p0, Lcom/konka/systemsetting/net/IPEditer;->mCurrPos:I

    return v0
.end method

.method static synthetic access$2(Lcom/konka/systemsetting/net/IPEditer;)Ljava/lang/String;
    .locals 1

    iget-object v0, p0, Lcom/konka/systemsetting/net/IPEditer;->strIP:Ljava/lang/String;

    return-object v0
.end method

.method static synthetic access$3(Lcom/konka/systemsetting/net/IPEditer;)V
    .locals 0

    invoke-direct {p0}, Lcom/konka/systemsetting/net/IPEditer;->updateDotsPos()V

    return-void
.end method

.method static synthetic access$4(Lcom/konka/systemsetting/net/IPEditer;I)Z
    .locals 1

    invoke-direct {p0, p1}, Lcom/konka/systemsetting/net/IPEditer;->isNumberKey(I)Z

    move-result v0

    return v0
.end method

.method static synthetic access$5(Lcom/konka/systemsetting/net/IPEditer;)Z
    .locals 1

    invoke-direct {p0}, Lcom/konka/systemsetting/net/IPEditer;->isSelected()Z

    move-result v0

    return v0
.end method

.method static synthetic access$6(Lcom/konka/systemsetting/net/IPEditer;I)C
    .locals 1

    invoke-direct {p0, p1}, Lcom/konka/systemsetting/net/IPEditer;->getChar(I)C

    move-result v0

    return v0
.end method

.method static synthetic access$7(Lcom/konka/systemsetting/net/IPEditer;Ljava/lang/String;)V
    .locals 0

    invoke-direct {p0, p1}, Lcom/konka/systemsetting/net/IPEditer;->replaceCurrField(Ljava/lang/String;)V

    return-void
.end method

.method static synthetic access$8(Lcom/konka/systemsetting/net/IPEditer;C)V
    .locals 0

    invoke-direct {p0, p1}, Lcom/konka/systemsetting/net/IPEditer;->fieldAppend(C)V

    return-void
.end method

.method static synthetic access$9(Lcom/konka/systemsetting/net/IPEditer;)Landroid/widget/EditText;
    .locals 1

    iget-object v0, p0, Lcom/konka/systemsetting/net/IPEditer;->mEditText:Landroid/widget/EditText;

    return-object v0
.end method

.method private cursorLeft()V
    .locals 1

    iget v0, p0, Lcom/konka/systemsetting/net/IPEditer;->mCurrPos:I

    add-int/lit8 v0, v0, -0x1

    iput v0, p0, Lcom/konka/systemsetting/net/IPEditer;->mCurrPos:I

    iget v0, p0, Lcom/konka/systemsetting/net/IPEditer;->mCurrPos:I

    if-gtz v0, :cond_0

    const/4 v0, 0x0

    iput v0, p0, Lcom/konka/systemsetting/net/IPEditer;->mCurrPos:I

    :cond_0
    return-void
.end method

.method private cursorRight()V
    .locals 2

    iget v0, p0, Lcom/konka/systemsetting/net/IPEditer;->mCurrPos:I

    add-int/lit8 v0, v0, 0x1

    iput v0, p0, Lcom/konka/systemsetting/net/IPEditer;->mCurrPos:I

    iget v0, p0, Lcom/konka/systemsetting/net/IPEditer;->mCurrPos:I

    iget-object v1, p0, Lcom/konka/systemsetting/net/IPEditer;->strIP:Ljava/lang/String;

    invoke-virtual {v1}, Ljava/lang/String;->length()I

    move-result v1

    if-lt v0, v1, :cond_0

    iget-object v0, p0, Lcom/konka/systemsetting/net/IPEditer;->strIP:Ljava/lang/String;

    invoke-virtual {v0}, Ljava/lang/String;->length()I

    move-result v0

    iput v0, p0, Lcom/konka/systemsetting/net/IPEditer;->mCurrPos:I

    :cond_0
    return-void
.end method

.method private fieldAppend(C)V
    .locals 6
    .param p1    # C

    iget v1, p0, Lcom/konka/systemsetting/net/IPEditer;->mCurrField:I

    packed-switch v1, :pswitch_data_0

    :goto_0
    return-void

    :pswitch_0
    iget-object v1, p0, Lcom/konka/systemsetting/net/IPEditer;->strfield1:Ljava/lang/String;

    invoke-direct {p0, v1, p1}, Lcom/konka/systemsetting/net/IPEditer;->fieldFixed(Ljava/lang/String;C)Ljava/lang/String;

    move-result-object v1

    iput-object v1, p0, Lcom/konka/systemsetting/net/IPEditer;->strfield1:Ljava/lang/String;

    :goto_1
    iget-object v1, p0, Lcom/konka/systemsetting/net/IPEditer;->mEditText:Landroid/widget/EditText;

    iget-object v2, p0, Lcom/konka/systemsetting/net/IPEditer;->strfield1:Ljava/lang/String;

    iget-object v3, p0, Lcom/konka/systemsetting/net/IPEditer;->strfield2:Ljava/lang/String;

    iget-object v4, p0, Lcom/konka/systemsetting/net/IPEditer;->strfield3:Ljava/lang/String;

    iget-object v5, p0, Lcom/konka/systemsetting/net/IPEditer;->strfield4:Ljava/lang/String;

    invoke-static {v2, v3, v4, v5}, Lcom/konka/systemsetting/net/IPEditer;->formatIP(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Landroid/widget/EditText;->setText(Ljava/lang/CharSequence;)V

    iget-object v1, p0, Lcom/konka/systemsetting/net/IPEditer;->mEditText:Landroid/widget/EditText;

    invoke-virtual {v1}, Landroid/widget/EditText;->getText()Landroid/text/Editable;

    move-result-object v1

    invoke-interface {v1}, Landroid/text/Editable;->toString()Ljava/lang/String;

    move-result-object v1

    iput-object v1, p0, Lcom/konka/systemsetting/net/IPEditer;->strIP:Ljava/lang/String;

    const/4 v0, 0x0

    iget v1, p0, Lcom/konka/systemsetting/net/IPEditer;->mCurrField:I

    packed-switch v1, :pswitch_data_1

    goto :goto_0

    :pswitch_1
    iget v0, p0, Lcom/konka/systemsetting/net/IPEditer;->pos_fstDot:I

    :goto_2
    iget-object v1, p0, Lcom/konka/systemsetting/net/IPEditer;->mEditText:Landroid/widget/EditText;

    invoke-virtual {v1, v0}, Landroid/widget/EditText;->setSelection(I)V

    goto :goto_0

    :pswitch_2
    iget-object v1, p0, Lcom/konka/systemsetting/net/IPEditer;->strfield2:Ljava/lang/String;

    invoke-direct {p0, v1, p1}, Lcom/konka/systemsetting/net/IPEditer;->fieldFixed(Ljava/lang/String;C)Ljava/lang/String;

    move-result-object v1

    iput-object v1, p0, Lcom/konka/systemsetting/net/IPEditer;->strfield2:Ljava/lang/String;

    goto :goto_1

    :pswitch_3
    iget-object v1, p0, Lcom/konka/systemsetting/net/IPEditer;->strfield3:Ljava/lang/String;

    invoke-direct {p0, v1, p1}, Lcom/konka/systemsetting/net/IPEditer;->fieldFixed(Ljava/lang/String;C)Ljava/lang/String;

    move-result-object v1

    iput-object v1, p0, Lcom/konka/systemsetting/net/IPEditer;->strfield3:Ljava/lang/String;

    goto :goto_1

    :pswitch_4
    iget-object v1, p0, Lcom/konka/systemsetting/net/IPEditer;->strfield4:Ljava/lang/String;

    invoke-direct {p0, v1, p1}, Lcom/konka/systemsetting/net/IPEditer;->fieldFixed(Ljava/lang/String;C)Ljava/lang/String;

    move-result-object v1

    iput-object v1, p0, Lcom/konka/systemsetting/net/IPEditer;->strfield4:Ljava/lang/String;

    goto :goto_1

    :pswitch_5
    iget v0, p0, Lcom/konka/systemsetting/net/IPEditer;->pos_sndDot:I

    goto :goto_2

    :pswitch_6
    iget v0, p0, Lcom/konka/systemsetting/net/IPEditer;->pos_thdDot:I

    goto :goto_2

    :pswitch_7
    iget-object v1, p0, Lcom/konka/systemsetting/net/IPEditer;->strIP:Ljava/lang/String;

    invoke-virtual {v1}, Ljava/lang/String;->length()I

    move-result v0

    goto :goto_2

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
        :pswitch_2
        :pswitch_3
        :pswitch_4
    .end packed-switch

    :pswitch_data_1
    .packed-switch 0x1
        :pswitch_1
        :pswitch_5
        :pswitch_6
        :pswitch_7
    .end packed-switch
.end method

.method private fieldFixed(Ljava/lang/String;)Ljava/lang/String;
    .locals 5
    .param p1    # Ljava/lang/String;

    move-object v1, p1

    invoke-virtual {v1}, Ljava/lang/String;->length()I

    move-result v0

    const/4 v3, 0x3

    if-lt v0, v3, :cond_0

    add-int/lit8 v3, v0, -0x2

    invoke-virtual {v1, v3, v0}, Ljava/lang/String;->substring(II)Ljava/lang/String;

    move-result-object v1

    :cond_0
    invoke-static {v1}, Ljava/lang/Integer;->parseInt(Ljava/lang/String;)I

    move-result v3

    invoke-static {v3}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/Integer;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v2}, Ljava/lang/Integer;->intValue()I

    move-result v3

    const/16 v4, 0xff

    if-le v3, v4, :cond_1

    const/4 v3, 0x1

    const/4 v4, 0x2

    invoke-virtual {v1, v3, v4}, Ljava/lang/String;->substring(II)Ljava/lang/String;

    move-result-object v1

    :cond_1
    return-object v1
.end method

.method private fieldFixed(Ljava/lang/String;C)Ljava/lang/String;
    .locals 5
    .param p1    # Ljava/lang/String;
    .param p2    # C

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-static {p1}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v4

    invoke-direct {v3, v4}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-static {p2}, Ljava/lang/String;->valueOf(C)Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/String;->length()I

    move-result v0

    const/4 v3, 0x3

    if-lt v0, v3, :cond_0

    add-int/lit8 v3, v0, -0x2

    invoke-virtual {v1, v3, v0}, Ljava/lang/String;->substring(II)Ljava/lang/String;

    move-result-object v1

    :cond_0
    invoke-static {v1}, Ljava/lang/Integer;->parseInt(Ljava/lang/String;)I

    move-result v3

    invoke-static {v3}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/Integer;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v2}, Ljava/lang/Integer;->intValue()I

    move-result v3

    const/16 v4, 0xff

    if-le v3, v4, :cond_1

    const/4 v3, 0x1

    const/4 v4, 0x2

    invoke-virtual {v1, v3, v4}, Ljava/lang/String;->substring(II)Ljava/lang/String;

    move-result-object v1

    :cond_1
    return-object v1
.end method

.method private fieldLeft()V
    .locals 2

    const/4 v1, 0x1

    iget v0, p0, Lcom/konka/systemsetting/net/IPEditer;->mCurrField:I

    add-int/lit8 v0, v0, -0x1

    iput v0, p0, Lcom/konka/systemsetting/net/IPEditer;->mCurrField:I

    iget v0, p0, Lcom/konka/systemsetting/net/IPEditer;->mCurrField:I

    if-gt v0, v1, :cond_0

    iput v1, p0, Lcom/konka/systemsetting/net/IPEditer;->mCurrField:I

    :cond_0
    return-void
.end method

.method private fieldRight()V
    .locals 2

    const/4 v1, 0x4

    iget v0, p0, Lcom/konka/systemsetting/net/IPEditer;->mCurrField:I

    add-int/lit8 v0, v0, 0x1

    iput v0, p0, Lcom/konka/systemsetting/net/IPEditer;->mCurrField:I

    iget v0, p0, Lcom/konka/systemsetting/net/IPEditer;->mCurrField:I

    if-lt v0, v1, :cond_0

    iput v1, p0, Lcom/konka/systemsetting/net/IPEditer;->mCurrField:I

    :cond_0
    return-void
.end method

.method public static formatIP(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;
    .locals 2
    .param p0    # Ljava/lang/String;
    .param p1    # Ljava/lang/String;
    .param p2    # Ljava/lang/String;
    .param p3    # Ljava/lang/String;

    new-instance v0, Ljava/lang/StringBuilder;

    invoke-static {p0}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    const-string v1, "."

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, "."

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0, p2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, "."

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0, p3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method private getChar(I)C
    .locals 1
    .param p1    # I

    const/4 v0, 0x0

    sparse-switch p1, :sswitch_data_0

    const/16 v0, 0x30

    :goto_0
    return v0

    :sswitch_0
    const/16 v0, 0x30

    goto :goto_0

    :sswitch_1
    const/16 v0, 0x31

    goto :goto_0

    :sswitch_2
    const/16 v0, 0x32

    goto :goto_0

    :sswitch_3
    const/16 v0, 0x33

    goto :goto_0

    :sswitch_4
    const/16 v0, 0x34

    goto :goto_0

    :sswitch_5
    const/16 v0, 0x35

    goto :goto_0

    :sswitch_6
    const/16 v0, 0x36

    goto :goto_0

    :sswitch_7
    const/16 v0, 0x37

    goto :goto_0

    :sswitch_8
    const/16 v0, 0x38

    goto :goto_0

    :sswitch_9
    const/16 v0, 0x39

    goto :goto_0

    nop

    :sswitch_data_0
    .sparse-switch
        0x7 -> :sswitch_0
        0x8 -> :sswitch_1
        0x9 -> :sswitch_2
        0xa -> :sswitch_3
        0xb -> :sswitch_4
        0xc -> :sswitch_5
        0xd -> :sswitch_6
        0xe -> :sswitch_7
        0xf -> :sswitch_8
        0x10 -> :sswitch_9
        0x90 -> :sswitch_0
        0x91 -> :sswitch_1
        0x92 -> :sswitch_2
        0x93 -> :sswitch_3
        0x94 -> :sswitch_4
        0x95 -> :sswitch_5
        0x96 -> :sswitch_6
        0x97 -> :sswitch_7
        0x98 -> :sswitch_8
        0x99 -> :sswitch_9
    .end sparse-switch
.end method

.method private isFollowDot()Z
    .locals 2

    invoke-direct {p0}, Lcom/konka/systemsetting/net/IPEditer;->updateDotsPos()V

    iget v0, p0, Lcom/konka/systemsetting/net/IPEditer;->mCurrPos:I

    if-eqz v0, :cond_0

    iget v0, p0, Lcom/konka/systemsetting/net/IPEditer;->mCurrPos:I

    iget v1, p0, Lcom/konka/systemsetting/net/IPEditer;->pos_fstDot:I

    add-int/lit8 v1, v1, 0x1

    if-eq v0, v1, :cond_0

    iget v0, p0, Lcom/konka/systemsetting/net/IPEditer;->mCurrPos:I

    iget v1, p0, Lcom/konka/systemsetting/net/IPEditer;->pos_sndDot:I

    add-int/lit8 v1, v1, 0x1

    if-eq v0, v1, :cond_0

    iget v0, p0, Lcom/konka/systemsetting/net/IPEditer;->mCurrPos:I

    iget v1, p0, Lcom/konka/systemsetting/net/IPEditer;->pos_thdDot:I

    add-int/lit8 v1, v1, 0x1

    if-eq v0, v1, :cond_0

    const/4 v0, 0x0

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x1

    goto :goto_0
.end method

.method private isFrontDot()Z
    .locals 2

    invoke-direct {p0}, Lcom/konka/systemsetting/net/IPEditer;->updateDotsPos()V

    iget v0, p0, Lcom/konka/systemsetting/net/IPEditer;->mCurrPos:I

    iget-object v1, p0, Lcom/konka/systemsetting/net/IPEditer;->strIP:Ljava/lang/String;

    invoke-virtual {v1}, Ljava/lang/String;->length()I

    move-result v1

    if-eq v0, v1, :cond_0

    iget v0, p0, Lcom/konka/systemsetting/net/IPEditer;->mCurrPos:I

    iget v1, p0, Lcom/konka/systemsetting/net/IPEditer;->pos_fstDot:I

    if-eq v0, v1, :cond_0

    iget v0, p0, Lcom/konka/systemsetting/net/IPEditer;->mCurrPos:I

    iget v1, p0, Lcom/konka/systemsetting/net/IPEditer;->pos_sndDot:I

    if-eq v0, v1, :cond_0

    iget v0, p0, Lcom/konka/systemsetting/net/IPEditer;->mCurrPos:I

    iget v1, p0, Lcom/konka/systemsetting/net/IPEditer;->pos_thdDot:I

    if-eq v0, v1, :cond_0

    const/4 v0, 0x0

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x1

    goto :goto_0
.end method

.method private isNumberKey(I)Z
    .locals 1
    .param p1    # I

    sparse-switch p1, :sswitch_data_0

    const/4 v0, 0x0

    :goto_0
    return v0

    :sswitch_0
    const/4 v0, 0x1

    goto :goto_0

    nop

    :sswitch_data_0
    .sparse-switch
        0x7 -> :sswitch_0
        0x8 -> :sswitch_0
        0x9 -> :sswitch_0
        0xa -> :sswitch_0
        0xb -> :sswitch_0
        0xc -> :sswitch_0
        0xd -> :sswitch_0
        0xe -> :sswitch_0
        0xf -> :sswitch_0
        0x10 -> :sswitch_0
        0x90 -> :sswitch_0
        0x91 -> :sswitch_0
        0x92 -> :sswitch_0
        0x93 -> :sswitch_0
        0x94 -> :sswitch_0
        0x95 -> :sswitch_0
        0x96 -> :sswitch_0
        0x97 -> :sswitch_0
        0x98 -> :sswitch_0
        0x99 -> :sswitch_0
    .end sparse-switch
.end method

.method private isSelected()Z
    .locals 3

    iget-object v1, p0, Lcom/konka/systemsetting/net/IPEditer;->mEditText:Landroid/widget/EditText;

    invoke-virtual {v1}, Landroid/widget/EditText;->getEditableText()Landroid/text/Editable;

    move-result-object v1

    invoke-static {v1}, Landroid/text/Selection;->getSelectionStart(Ljava/lang/CharSequence;)I

    move-result v1

    iput v1, p0, Lcom/konka/systemsetting/net/IPEditer;->mSelectedStart:I

    iget-object v1, p0, Lcom/konka/systemsetting/net/IPEditer;->mEditText:Landroid/widget/EditText;

    invoke-virtual {v1}, Landroid/widget/EditText;->getEditableText()Landroid/text/Editable;

    move-result-object v1

    invoke-static {v1}, Landroid/text/Selection;->getSelectionEnd(Ljava/lang/CharSequence;)I

    move-result v1

    iput v1, p0, Lcom/konka/systemsetting/net/IPEditer;->mSelectedEnd:I

    iget v1, p0, Lcom/konka/systemsetting/net/IPEditer;->mSelectedEnd:I

    iput v1, p0, Lcom/konka/systemsetting/net/IPEditer;->mCurrPos:I

    iget v1, p0, Lcom/konka/systemsetting/net/IPEditer;->mSelectedStart:I

    iget v2, p0, Lcom/konka/systemsetting/net/IPEditer;->mSelectedEnd:I

    if-ne v1, v2, :cond_0

    const/4 v0, 0x0

    :goto_0
    return v0

    :cond_0
    iget v1, p0, Lcom/konka/systemsetting/net/IPEditer;->mSelectedEnd:I

    iput v1, p0, Lcom/konka/systemsetting/net/IPEditer;->mCurrPos:I

    const/4 v0, 0x1

    goto :goto_0
.end method

.method private keyBoardLeft()V
    .locals 2

    invoke-direct {p0}, Lcom/konka/systemsetting/net/IPEditer;->isSelected()Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-direct {p0}, Lcom/konka/systemsetting/net/IPEditer;->fieldLeft()V

    iget v0, p0, Lcom/konka/systemsetting/net/IPEditer;->mCurrField:I

    invoke-virtual {p0, v0}, Lcom/konka/systemsetting/net/IPEditer;->setSelectionField(I)Z

    :goto_0
    return-void

    :cond_0
    invoke-direct {p0}, Lcom/konka/systemsetting/net/IPEditer;->cursorLeft()V

    invoke-direct {p0}, Lcom/konka/systemsetting/net/IPEditer;->isFollowDot()Z

    move-result v0

    if-eqz v0, :cond_1

    invoke-direct {p0}, Lcom/konka/systemsetting/net/IPEditer;->fieldLeft()V

    iget v0, p0, Lcom/konka/systemsetting/net/IPEditer;->mCurrField:I

    invoke-virtual {p0, v0}, Lcom/konka/systemsetting/net/IPEditer;->setSelectionField(I)Z

    goto :goto_0

    :cond_1
    iget-object v0, p0, Lcom/konka/systemsetting/net/IPEditer;->mEditText:Landroid/widget/EditText;

    iget v1, p0, Lcom/konka/systemsetting/net/IPEditer;->mCurrPos:I

    invoke-virtual {v0, v1}, Landroid/widget/EditText;->setSelection(I)V

    goto :goto_0
.end method

.method private keyBoardRight()V
    .locals 2

    invoke-direct {p0}, Lcom/konka/systemsetting/net/IPEditer;->isSelected()Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-direct {p0}, Lcom/konka/systemsetting/net/IPEditer;->fieldRight()V

    iget v0, p0, Lcom/konka/systemsetting/net/IPEditer;->mCurrField:I

    invoke-virtual {p0, v0}, Lcom/konka/systemsetting/net/IPEditer;->setSelectionField(I)Z

    :goto_0
    return-void

    :cond_0
    invoke-direct {p0}, Lcom/konka/systemsetting/net/IPEditer;->isFrontDot()Z

    move-result v0

    if-eqz v0, :cond_1

    invoke-direct {p0}, Lcom/konka/systemsetting/net/IPEditer;->fieldRight()V

    iget v0, p0, Lcom/konka/systemsetting/net/IPEditer;->mCurrField:I

    invoke-virtual {p0, v0}, Lcom/konka/systemsetting/net/IPEditer;->setSelectionField(I)Z

    goto :goto_0

    :cond_1
    invoke-direct {p0}, Lcom/konka/systemsetting/net/IPEditer;->cursorRight()V

    iget-object v0, p0, Lcom/konka/systemsetting/net/IPEditer;->mEditText:Landroid/widget/EditText;

    iget v1, p0, Lcom/konka/systemsetting/net/IPEditer;->mCurrPos:I

    invoke-virtual {v0, v1}, Landroid/widget/EditText;->setSelection(I)V

    goto :goto_0
.end method

.method private replaceCurrField(Ljava/lang/String;)V
    .locals 1
    .param p1    # Ljava/lang/String;

    iget v0, p0, Lcom/konka/systemsetting/net/IPEditer;->mCurrField:I

    packed-switch v0, :pswitch_data_0

    :goto_0
    return-void

    :pswitch_0
    iput-object p1, p0, Lcom/konka/systemsetting/net/IPEditer;->strfield1:Ljava/lang/String;

    goto :goto_0

    :pswitch_1
    iput-object p1, p0, Lcom/konka/systemsetting/net/IPEditer;->strfield2:Ljava/lang/String;

    goto :goto_0

    :pswitch_2
    iput-object p1, p0, Lcom/konka/systemsetting/net/IPEditer;->strfield3:Ljava/lang/String;

    goto :goto_0

    :pswitch_3
    iput-object p1, p0, Lcom/konka/systemsetting/net/IPEditer;->strfield4:Ljava/lang/String;

    goto :goto_0

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
        :pswitch_1
        :pswitch_2
        :pswitch_3
    .end packed-switch
.end method

.method private setSelection()V
    .locals 2

    const/4 v0, 0x0

    iget v1, p0, Lcom/konka/systemsetting/net/IPEditer;->mCurrField:I

    packed-switch v1, :pswitch_data_0

    :goto_0
    return-void

    :pswitch_0
    iget v0, p0, Lcom/konka/systemsetting/net/IPEditer;->pos_fstDot:I

    :goto_1
    iget-object v1, p0, Lcom/konka/systemsetting/net/IPEditer;->mEditText:Landroid/widget/EditText;

    invoke-virtual {v1, v0}, Landroid/widget/EditText;->setSelection(I)V

    goto :goto_0

    :pswitch_1
    iget v0, p0, Lcom/konka/systemsetting/net/IPEditer;->pos_sndDot:I

    goto :goto_1

    :pswitch_2
    iget v0, p0, Lcom/konka/systemsetting/net/IPEditer;->pos_thdDot:I

    goto :goto_1

    :pswitch_3
    iget-object v1, p0, Lcom/konka/systemsetting/net/IPEditer;->strIP:Ljava/lang/String;

    invoke-virtual {v1}, Ljava/lang/String;->length()I

    move-result v0

    goto :goto_1

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
        :pswitch_1
        :pswitch_2
        :pswitch_3
    .end packed-switch
.end method

.method private updateDotsPos()V
    .locals 4

    iget-object v0, p0, Lcom/konka/systemsetting/net/IPEditer;->strIP:Ljava/lang/String;

    const-string v1, "."

    invoke-virtual {v0, v1}, Ljava/lang/String;->indexOf(Ljava/lang/String;)I

    move-result v1

    iput v1, p0, Lcom/konka/systemsetting/net/IPEditer;->pos_fstDot:I

    const-string v1, "\\."

    const-string v2, "#"

    invoke-virtual {v0, v1, v2}, Ljava/lang/String;->replaceFirst(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    const-string v1, "."

    invoke-virtual {v0, v1}, Ljava/lang/String;->indexOf(Ljava/lang/String;)I

    move-result v1

    iput v1, p0, Lcom/konka/systemsetting/net/IPEditer;->pos_sndDot:I

    const-string v1, "\\."

    const-string v2, "#"

    invoke-virtual {v0, v1, v2}, Ljava/lang/String;->replaceFirst(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    const-string v1, "."

    invoke-virtual {v0, v1}, Ljava/lang/String;->indexOf(Ljava/lang/String;)I

    move-result v1

    iput v1, p0, Lcom/konka/systemsetting/net/IPEditer;->pos_thdDot:I

    iget-object v1, p0, Lcom/konka/systemsetting/net/IPEditer;->strIP:Ljava/lang/String;

    const/4 v2, 0x0

    iget v3, p0, Lcom/konka/systemsetting/net/IPEditer;->pos_fstDot:I

    invoke-virtual {v1, v2, v3}, Ljava/lang/String;->substring(II)Ljava/lang/String;

    move-result-object v1

    iput-object v1, p0, Lcom/konka/systemsetting/net/IPEditer;->strfield1:Ljava/lang/String;

    iget-object v1, p0, Lcom/konka/systemsetting/net/IPEditer;->strIP:Ljava/lang/String;

    iget v2, p0, Lcom/konka/systemsetting/net/IPEditer;->pos_fstDot:I

    add-int/lit8 v2, v2, 0x1

    iget v3, p0, Lcom/konka/systemsetting/net/IPEditer;->pos_sndDot:I

    invoke-virtual {v1, v2, v3}, Ljava/lang/String;->substring(II)Ljava/lang/String;

    move-result-object v1

    iput-object v1, p0, Lcom/konka/systemsetting/net/IPEditer;->strfield2:Ljava/lang/String;

    iget-object v1, p0, Lcom/konka/systemsetting/net/IPEditer;->strIP:Ljava/lang/String;

    iget v2, p0, Lcom/konka/systemsetting/net/IPEditer;->pos_sndDot:I

    add-int/lit8 v2, v2, 0x1

    iget v3, p0, Lcom/konka/systemsetting/net/IPEditer;->pos_thdDot:I

    invoke-virtual {v1, v2, v3}, Ljava/lang/String;->substring(II)Ljava/lang/String;

    move-result-object v1

    iput-object v1, p0, Lcom/konka/systemsetting/net/IPEditer;->strfield3:Ljava/lang/String;

    iget-object v1, p0, Lcom/konka/systemsetting/net/IPEditer;->strIP:Ljava/lang/String;

    iget v2, p0, Lcom/konka/systemsetting/net/IPEditer;->pos_thdDot:I

    add-int/lit8 v2, v2, 0x1

    iget-object v3, p0, Lcom/konka/systemsetting/net/IPEditer;->strIP:Ljava/lang/String;

    invoke-virtual {v3}, Ljava/lang/String;->length()I

    move-result v3

    invoke-virtual {v1, v2, v3}, Ljava/lang/String;->substring(II)Ljava/lang/String;

    move-result-object v1

    iput-object v1, p0, Lcom/konka/systemsetting/net/IPEditer;->strfield4:Ljava/lang/String;

    return-void
.end method


# virtual methods
.method public setEditText(Landroid/widget/EditText;)V
    .locals 2
    .param p1    # Landroid/widget/EditText;

    iput-object p1, p0, Lcom/konka/systemsetting/net/IPEditer;->mEditText:Landroid/widget/EditText;

    iget-object v0, p0, Lcom/konka/systemsetting/net/IPEditer;->mEditText:Landroid/widget/EditText;

    iget-object v1, p0, Lcom/konka/systemsetting/net/IPEditer;->textWatcher:Landroid/text/TextWatcher;

    invoke-virtual {v0, v1}, Landroid/widget/EditText;->addTextChangedListener(Landroid/text/TextWatcher;)V

    iget-object v0, p0, Lcom/konka/systemsetting/net/IPEditer;->mEditText:Landroid/widget/EditText;

    iget-object v1, p0, Lcom/konka/systemsetting/net/IPEditer;->onKeyListener:Landroid/view/View$OnKeyListener;

    invoke-virtual {v0, v1}, Landroid/widget/EditText;->setOnKeyListener(Landroid/view/View$OnKeyListener;)V

    iget-object v0, p0, Lcom/konka/systemsetting/net/IPEditer;->mEditText:Landroid/widget/EditText;

    invoke-virtual {v0}, Landroid/widget/EditText;->getText()Landroid/text/Editable;

    move-result-object v0

    invoke-interface {v0}, Landroid/text/Editable;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/String;->isEmpty()Z

    move-result v0

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/konka/systemsetting/net/IPEditer;->mEditText:Landroid/widget/EditText;

    iget-object v1, p0, Lcom/konka/systemsetting/net/IPEditer;->strIP:Ljava/lang/String;

    invoke-virtual {v0, v1}, Landroid/widget/EditText;->setText(Ljava/lang/CharSequence;)V

    :goto_0
    iget v0, p0, Lcom/konka/systemsetting/net/IPEditer;->mCurrField:I

    invoke-virtual {p0, v0}, Lcom/konka/systemsetting/net/IPEditer;->setSelectionField(I)Z

    iget-object v0, p0, Lcom/konka/systemsetting/net/IPEditer;->mEditText:Landroid/widget/EditText;

    iget-object v1, p0, Lcom/konka/systemsetting/net/IPEditer;->onFocusChangeListener:Landroid/view/View$OnFocusChangeListener;

    invoke-virtual {v0, v1}, Landroid/widget/EditText;->setOnFocusChangeListener(Landroid/view/View$OnFocusChangeListener;)V

    return-void

    :cond_0
    iget-object v0, p0, Lcom/konka/systemsetting/net/IPEditer;->mEditText:Landroid/widget/EditText;

    invoke-virtual {v0}, Landroid/widget/EditText;->getText()Landroid/text/Editable;

    move-result-object v0

    invoke-interface {v0}, Landroid/text/Editable;->toString()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/konka/systemsetting/net/IPEditer;->strIP:Ljava/lang/String;

    goto :goto_0
.end method

.method public setSelectionField(I)Z
    .locals 3
    .param p1    # I

    const/4 v0, 0x0

    invoke-direct {p0}, Lcom/konka/systemsetting/net/IPEditer;->updateDotsPos()V

    packed-switch p1, :pswitch_data_0

    :goto_0
    return v0

    :pswitch_0
    iget-object v1, p0, Lcom/konka/systemsetting/net/IPEditer;->mEditText:Landroid/widget/EditText;

    iget v2, p0, Lcom/konka/systemsetting/net/IPEditer;->pos_fstDot:I

    invoke-virtual {v1, v0, v2}, Landroid/widget/EditText;->setSelection(II)V

    :goto_1
    iget-object v0, p0, Lcom/konka/systemsetting/net/IPEditer;->mEditText:Landroid/widget/EditText;

    invoke-virtual {v0}, Landroid/widget/EditText;->getEditableText()Landroid/text/Editable;

    move-result-object v0

    invoke-static {v0}, Landroid/text/Selection;->getSelectionStart(Ljava/lang/CharSequence;)I

    move-result v0

    iput v0, p0, Lcom/konka/systemsetting/net/IPEditer;->mSelectedStart:I

    iget-object v0, p0, Lcom/konka/systemsetting/net/IPEditer;->mEditText:Landroid/widget/EditText;

    invoke-virtual {v0}, Landroid/widget/EditText;->getEditableText()Landroid/text/Editable;

    move-result-object v0

    invoke-static {v0}, Landroid/text/Selection;->getSelectionEnd(Ljava/lang/CharSequence;)I

    move-result v0

    iput v0, p0, Lcom/konka/systemsetting/net/IPEditer;->mSelectedEnd:I

    iget v0, p0, Lcom/konka/systemsetting/net/IPEditer;->mSelectedEnd:I

    iput v0, p0, Lcom/konka/systemsetting/net/IPEditer;->mCurrPos:I

    const/4 v0, 0x1

    goto :goto_0

    :pswitch_1
    iget-object v0, p0, Lcom/konka/systemsetting/net/IPEditer;->mEditText:Landroid/widget/EditText;

    iget v1, p0, Lcom/konka/systemsetting/net/IPEditer;->pos_fstDot:I

    add-int/lit8 v1, v1, 0x1

    iget v2, p0, Lcom/konka/systemsetting/net/IPEditer;->pos_sndDot:I

    invoke-virtual {v0, v1, v2}, Landroid/widget/EditText;->setSelection(II)V

    goto :goto_1

    :pswitch_2
    iget-object v0, p0, Lcom/konka/systemsetting/net/IPEditer;->mEditText:Landroid/widget/EditText;

    iget v1, p0, Lcom/konka/systemsetting/net/IPEditer;->pos_sndDot:I

    add-int/lit8 v1, v1, 0x1

    iget v2, p0, Lcom/konka/systemsetting/net/IPEditer;->pos_thdDot:I

    invoke-virtual {v0, v1, v2}, Landroid/widget/EditText;->setSelection(II)V

    goto :goto_1

    :pswitch_3
    iget-object v0, p0, Lcom/konka/systemsetting/net/IPEditer;->mEditText:Landroid/widget/EditText;

    iget v1, p0, Lcom/konka/systemsetting/net/IPEditer;->pos_thdDot:I

    add-int/lit8 v1, v1, 0x1

    iget-object v2, p0, Lcom/konka/systemsetting/net/IPEditer;->strIP:Ljava/lang/String;

    invoke-virtual {v2}, Ljava/lang/String;->length()I

    move-result v2

    invoke-virtual {v0, v1, v2}, Landroid/widget/EditText;->setSelection(II)V

    goto :goto_1

    nop

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
        :pswitch_1
        :pswitch_2
        :pswitch_3
    .end packed-switch
.end method
