.class public Lcom/konka/systemsetting/net/CurretNetStatusBak;
.super Ljava/lang/Object;
.source "CurretNetStatusBak.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/konka/systemsetting/net/CurretNetStatusBak$NetType;
    }
.end annotation


# static fields
.field private static synthetic $SWITCH_TABLE$com$konka$systemsetting$net$CurretNetStatusBak$NetType:[I


# instance fields
.field public mPPPoEpwd:Ljava/lang/String;

.field public mPPPoEusr:Ljava/lang/String;

.field public mWifiDNS:Ljava/lang/String;

.field public mWifiDNSbak:Ljava/lang/String;

.field public mWifiIP:Ljava/lang/String;

.field public mWifiMask:Ljava/lang/String;

.field public mWifiRoute:Ljava/lang/String;

.field public mWireDNS:Ljava/lang/String;

.field public mWireDNSbak:Ljava/lang/String;

.field public mWireIP:Ljava/lang/String;

.field public mWireMask:Ljava/lang/String;

.field public mWireRoute:Ljava/lang/String;


# direct methods
.method static synthetic $SWITCH_TABLE$com$konka$systemsetting$net$CurretNetStatusBak$NetType()[I
    .locals 3

    sget-object v0, Lcom/konka/systemsetting/net/CurretNetStatusBak;->$SWITCH_TABLE$com$konka$systemsetting$net$CurretNetStatusBak$NetType:[I

    if-eqz v0, :cond_0

    :goto_0
    return-object v0

    :cond_0
    invoke-static {}, Lcom/konka/systemsetting/net/CurretNetStatusBak$NetType;->values()[Lcom/konka/systemsetting/net/CurretNetStatusBak$NetType;

    move-result-object v0

    array-length v0, v0

    new-array v0, v0, [I

    :try_start_0
    sget-object v1, Lcom/konka/systemsetting/net/CurretNetStatusBak$NetType;->EN_PPPOE:Lcom/konka/systemsetting/net/CurretNetStatusBak$NetType;

    invoke-virtual {v1}, Lcom/konka/systemsetting/net/CurretNetStatusBak$NetType;->ordinal()I

    move-result v1

    const/4 v2, 0x3

    aput v2, v0, v1
    :try_end_0
    .catch Ljava/lang/NoSuchFieldError; {:try_start_0 .. :try_end_0} :catch_2

    :goto_1
    :try_start_1
    sget-object v1, Lcom/konka/systemsetting/net/CurretNetStatusBak$NetType;->EN_WIFI:Lcom/konka/systemsetting/net/CurretNetStatusBak$NetType;

    invoke-virtual {v1}, Lcom/konka/systemsetting/net/CurretNetStatusBak$NetType;->ordinal()I

    move-result v1

    const/4 v2, 0x2

    aput v2, v0, v1
    :try_end_1
    .catch Ljava/lang/NoSuchFieldError; {:try_start_1 .. :try_end_1} :catch_1

    :goto_2
    :try_start_2
    sget-object v1, Lcom/konka/systemsetting/net/CurretNetStatusBak$NetType;->EN_WIRE:Lcom/konka/systemsetting/net/CurretNetStatusBak$NetType;

    invoke-virtual {v1}, Lcom/konka/systemsetting/net/CurretNetStatusBak$NetType;->ordinal()I

    move-result v1

    const/4 v2, 0x1

    aput v2, v0, v1
    :try_end_2
    .catch Ljava/lang/NoSuchFieldError; {:try_start_2 .. :try_end_2} :catch_0

    :goto_3
    sput-object v0, Lcom/konka/systemsetting/net/CurretNetStatusBak;->$SWITCH_TABLE$com$konka$systemsetting$net$CurretNetStatusBak$NetType:[I

    goto :goto_0

    :catch_0
    move-exception v1

    goto :goto_3

    :catch_1
    move-exception v1

    goto :goto_2

    :catch_2
    move-exception v1

    goto :goto_1
.end method

.method public constructor <init>()V
    .locals 0

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method private String(Ljava/lang/String;)Ljava/lang/Object;
    .locals 1
    .param p1    # Ljava/lang/String;

    const/4 v0, 0x0

    return-object v0
.end method


# virtual methods
.method public clearDate(Lcom/konka/systemsetting/net/CurretNetStatusBak$NetType;)V
    .locals 2
    .param p1    # Lcom/konka/systemsetting/net/CurretNetStatusBak$NetType;

    invoke-static {}, Lcom/konka/systemsetting/net/CurretNetStatusBak;->$SWITCH_TABLE$com$konka$systemsetting$net$CurretNetStatusBak$NetType()[I

    move-result-object v0

    invoke-virtual {p1}, Lcom/konka/systemsetting/net/CurretNetStatusBak$NetType;->ordinal()I

    move-result v1

    aget v0, v0, v1

    packed-switch v0, :pswitch_data_0

    :goto_0
    return-void

    :pswitch_0
    const-string v0, ""

    iput-object v0, p0, Lcom/konka/systemsetting/net/CurretNetStatusBak;->mWireIP:Ljava/lang/String;

    const-string v0, ""

    iput-object v0, p0, Lcom/konka/systemsetting/net/CurretNetStatusBak;->mWireMask:Ljava/lang/String;

    const-string v0, ""

    iput-object v0, p0, Lcom/konka/systemsetting/net/CurretNetStatusBak;->mWireRoute:Ljava/lang/String;

    const-string v0, ""

    iput-object v0, p0, Lcom/konka/systemsetting/net/CurretNetStatusBak;->mWireDNS:Ljava/lang/String;

    const-string v0, ""

    iput-object v0, p0, Lcom/konka/systemsetting/net/CurretNetStatusBak;->mWireDNSbak:Ljava/lang/String;

    goto :goto_0

    :pswitch_1
    const-string v0, ""

    iput-object v0, p0, Lcom/konka/systemsetting/net/CurretNetStatusBak;->mWifiIP:Ljava/lang/String;

    const-string v0, ""

    iput-object v0, p0, Lcom/konka/systemsetting/net/CurretNetStatusBak;->mWifiMask:Ljava/lang/String;

    const-string v0, ""

    iput-object v0, p0, Lcom/konka/systemsetting/net/CurretNetStatusBak;->mWifiRoute:Ljava/lang/String;

    const-string v0, ""

    iput-object v0, p0, Lcom/konka/systemsetting/net/CurretNetStatusBak;->mWifiDNS:Ljava/lang/String;

    const-string v0, ""

    iput-object v0, p0, Lcom/konka/systemsetting/net/CurretNetStatusBak;->mWifiDNSbak:Ljava/lang/String;

    goto :goto_0

    :pswitch_2
    const-string v0, ""

    iput-object v0, p0, Lcom/konka/systemsetting/net/CurretNetStatusBak;->mPPPoEusr:Ljava/lang/String;

    const-string v0, ""

    iput-object v0, p0, Lcom/konka/systemsetting/net/CurretNetStatusBak;->mPPPoEpwd:Ljava/lang/String;

    goto :goto_0

    nop

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
        :pswitch_1
        :pswitch_2
    .end packed-switch
.end method

.method public isPPPoEUpdate(Ljava/lang/String;Ljava/lang/String;)Z
    .locals 2
    .param p1    # Ljava/lang/String;
    .param p2    # Ljava/lang/String;

    const/4 v0, 0x1

    iget-object v1, p0, Lcom/konka/systemsetting/net/CurretNetStatusBak;->mPPPoEusr:Ljava/lang/String;

    invoke-virtual {v1, p1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-nez v1, :cond_1

    :cond_0
    :goto_0
    return v0

    :cond_1
    iget-object v1, p0, Lcom/konka/systemsetting/net/CurretNetStatusBak;->mPPPoEpwd:Ljava/lang/String;

    invoke-virtual {v1, p2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_0

    const/4 v0, 0x0

    goto :goto_0
.end method

.method public isStaticIpUpdate(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)Z
    .locals 3
    .param p1    # Ljava/lang/String;
    .param p2    # Ljava/lang/String;
    .param p3    # Ljava/lang/String;
    .param p4    # Ljava/lang/String;
    .param p5    # Ljava/lang/String;

    const/4 v1, 0x1

    const-string v0, "..."

    invoke-virtual {v0, p1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_0

    const-string p1, "0.0.0.0"

    :cond_0
    invoke-virtual {v0, p2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_1

    const-string p2, "0.0.0.0"

    :cond_1
    invoke-virtual {v0, p3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_2

    const-string p3, "0.0.0.0"

    :cond_2
    invoke-virtual {v0, p4}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_3

    const-string p4, "0.0.0.0"

    :cond_3
    iget-object v2, p0, Lcom/konka/systemsetting/net/CurretNetStatusBak;->mWireIP:Ljava/lang/String;

    if-nez v2, :cond_4

    const-string v2, "0.0.0.0"

    iput-object v2, p0, Lcom/konka/systemsetting/net/CurretNetStatusBak;->mWireIP:Ljava/lang/String;

    :cond_4
    iget-object v2, p0, Lcom/konka/systemsetting/net/CurretNetStatusBak;->mWireMask:Ljava/lang/String;

    if-nez v2, :cond_5

    const-string v2, "0.0.0.0"

    iput-object v2, p0, Lcom/konka/systemsetting/net/CurretNetStatusBak;->mWireMask:Ljava/lang/String;

    :cond_5
    iget-object v2, p0, Lcom/konka/systemsetting/net/CurretNetStatusBak;->mWireRoute:Ljava/lang/String;

    if-nez v2, :cond_6

    const-string v2, "0.0.0.0"

    iput-object v2, p0, Lcom/konka/systemsetting/net/CurretNetStatusBak;->mWireRoute:Ljava/lang/String;

    :cond_6
    iget-object v2, p0, Lcom/konka/systemsetting/net/CurretNetStatusBak;->mWireDNS:Ljava/lang/String;

    if-nez v2, :cond_7

    const-string v2, "0.0.0.0"

    iput-object v2, p0, Lcom/konka/systemsetting/net/CurretNetStatusBak;->mWireDNS:Ljava/lang/String;

    :cond_7
    iget-object v2, p0, Lcom/konka/systemsetting/net/CurretNetStatusBak;->mWireIP:Ljava/lang/String;

    invoke-virtual {v2, p1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-nez v2, :cond_9

    :cond_8
    :goto_0
    return v1

    :cond_9
    iget-object v2, p0, Lcom/konka/systemsetting/net/CurretNetStatusBak;->mWireMask:Ljava/lang/String;

    invoke-virtual {v2, p2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_8

    iget-object v2, p0, Lcom/konka/systemsetting/net/CurretNetStatusBak;->mWireRoute:Ljava/lang/String;

    invoke-virtual {v2, p3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_8

    iget-object v2, p0, Lcom/konka/systemsetting/net/CurretNetStatusBak;->mWireDNS:Ljava/lang/String;

    invoke-virtual {v2, p4}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_8

    const/4 v1, 0x0

    goto :goto_0
.end method
