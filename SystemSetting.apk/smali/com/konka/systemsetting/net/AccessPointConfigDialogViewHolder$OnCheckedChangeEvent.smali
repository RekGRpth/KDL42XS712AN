.class Lcom/konka/systemsetting/net/AccessPointConfigDialogViewHolder$OnCheckedChangeEvent;
.super Ljava/lang/Object;
.source "AccessPointConfigDialogViewHolder.java"

# interfaces
.implements Landroid/widget/CompoundButton$OnCheckedChangeListener;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/konka/systemsetting/net/AccessPointConfigDialogViewHolder;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x2
    name = "OnCheckedChangeEvent"
.end annotation


# instance fields
.field final synthetic this$0:Lcom/konka/systemsetting/net/AccessPointConfigDialogViewHolder;


# direct methods
.method private constructor <init>(Lcom/konka/systemsetting/net/AccessPointConfigDialogViewHolder;)V
    .locals 0

    iput-object p1, p0, Lcom/konka/systemsetting/net/AccessPointConfigDialogViewHolder$OnCheckedChangeEvent;->this$0:Lcom/konka/systemsetting/net/AccessPointConfigDialogViewHolder;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method synthetic constructor <init>(Lcom/konka/systemsetting/net/AccessPointConfigDialogViewHolder;Lcom/konka/systemsetting/net/AccessPointConfigDialogViewHolder$OnCheckedChangeEvent;)V
    .locals 0

    invoke-direct {p0, p1}, Lcom/konka/systemsetting/net/AccessPointConfigDialogViewHolder$OnCheckedChangeEvent;-><init>(Lcom/konka/systemsetting/net/AccessPointConfigDialogViewHolder;)V

    return-void
.end method


# virtual methods
.method public onCheckedChanged(Landroid/widget/CompoundButton;Z)V
    .locals 2
    .param p1    # Landroid/widget/CompoundButton;
    .param p2    # Z

    invoke-virtual {p1}, Landroid/widget/CompoundButton;->getId()I

    move-result v0

    packed-switch v0, :pswitch_data_0

    :goto_0
    return-void

    :pswitch_0
    iget-object v1, p0, Lcom/konka/systemsetting/net/AccessPointConfigDialogViewHolder$OnCheckedChangeEvent;->this$0:Lcom/konka/systemsetting/net/AccessPointConfigDialogViewHolder;

    # invokes: Lcom/konka/systemsetting/net/AccessPointConfigDialogViewHolder;->changeCBShowPwdState(Z)V
    invoke-static {v1, p2}, Lcom/konka/systemsetting/net/AccessPointConfigDialogViewHolder;->access$0(Lcom/konka/systemsetting/net/AccessPointConfigDialogViewHolder;Z)V

    goto :goto_0

    :pswitch_1
    iget-object v1, p0, Lcom/konka/systemsetting/net/AccessPointConfigDialogViewHolder$OnCheckedChangeEvent;->this$0:Lcom/konka/systemsetting/net/AccessPointConfigDialogViewHolder;

    # invokes: Lcom/konka/systemsetting/net/AccessPointConfigDialogViewHolder;->changeCBSetIPState(Z)V
    invoke-static {v1, p2}, Lcom/konka/systemsetting/net/AccessPointConfigDialogViewHolder;->access$1(Lcom/konka/systemsetting/net/AccessPointConfigDialogViewHolder;Z)V

    goto :goto_0

    :pswitch_data_0
    .packed-switch 0x7f090058
        :pswitch_0    # com.konka.systemsetting.R.id.wifi_box_cb_showpwd
        :pswitch_1    # com.konka.systemsetting.R.id.wifi_box_cb_setip
    .end packed-switch
.end method
