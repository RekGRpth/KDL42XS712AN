.class public Lcom/konka/systemsetting/net/NetworkSoftApConfig;
.super Ljava/lang/Object;
.source "NetworkSoftApConfig.java"


# static fields
.field private static final TAG:Ljava/lang/String; = "NET_WIFI_AP ==>"


# instance fields
.field private final MAX_PWD_LENGTH:I

.field private final MAX_SSID_LENGTH:I

.field private final SECURE_OPEN:I

.field private final SECURE_WPA:I

.field private final SECURE_WPA2:I

.field private SecType:[I

.field private isPwdToBeShown:Z

.field isUnregisted:Z

.field private isWifiAPEnabled:Z

.field mContext:Lcom/konka/systemsetting/MainActivity;

.field private mCurrentSec:I

.field mHolder:Lcom/konka/systemsetting/net/NetworkSoftApViewHolder;

.field mPageMgr:Lcom/konka/systemsetting/net/NetworkPageManager;

.field mWifiApConfig:Landroid/net/wifi/WifiConfiguration;

.field mWifiMgr:Landroid/net/wifi/WifiManager;

.field receiver:Landroid/content/BroadcastReceiver;


# direct methods
.method public constructor <init>(Lcom/konka/systemsetting/MainActivity;Lcom/konka/systemsetting/net/NetworkPageManager;)V
    .locals 3
    .param p1    # Lcom/konka/systemsetting/MainActivity;
    .param p2    # Lcom/konka/systemsetting/net/NetworkPageManager;

    const/4 v2, 0x1

    const/4 v1, 0x0

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    const/4 v0, 0x3

    new-array v0, v0, [I

    fill-array-data v0, :array_0

    iput-object v0, p0, Lcom/konka/systemsetting/net/NetworkSoftApConfig;->SecType:[I

    iput v1, p0, Lcom/konka/systemsetting/net/NetworkSoftApConfig;->SECURE_WPA:I

    iput v2, p0, Lcom/konka/systemsetting/net/NetworkSoftApConfig;->SECURE_WPA2:I

    const/4 v0, 0x2

    iput v0, p0, Lcom/konka/systemsetting/net/NetworkSoftApConfig;->SECURE_OPEN:I

    const/16 v0, 0x20

    iput v0, p0, Lcom/konka/systemsetting/net/NetworkSoftApConfig;->MAX_SSID_LENGTH:I

    const/16 v0, 0x14

    iput v0, p0, Lcom/konka/systemsetting/net/NetworkSoftApConfig;->MAX_PWD_LENGTH:I

    iput v1, p0, Lcom/konka/systemsetting/net/NetworkSoftApConfig;->mCurrentSec:I

    iput-boolean v1, p0, Lcom/konka/systemsetting/net/NetworkSoftApConfig;->isPwdToBeShown:Z

    new-instance v0, Lcom/konka/systemsetting/net/NetworkSoftApConfig$1;

    invoke-direct {v0, p0}, Lcom/konka/systemsetting/net/NetworkSoftApConfig$1;-><init>(Lcom/konka/systemsetting/net/NetworkSoftApConfig;)V

    iput-object v0, p0, Lcom/konka/systemsetting/net/NetworkSoftApConfig;->receiver:Landroid/content/BroadcastReceiver;

    iput-boolean v2, p0, Lcom/konka/systemsetting/net/NetworkSoftApConfig;->isUnregisted:Z

    iput-object p1, p0, Lcom/konka/systemsetting/net/NetworkSoftApConfig;->mContext:Lcom/konka/systemsetting/MainActivity;

    iput-object p2, p0, Lcom/konka/systemsetting/net/NetworkSoftApConfig;->mPageMgr:Lcom/konka/systemsetting/net/NetworkPageManager;

    new-instance v0, Lcom/konka/systemsetting/net/NetworkSoftApConfig$2;

    iget-object v1, p0, Lcom/konka/systemsetting/net/NetworkSoftApConfig;->mContext:Lcom/konka/systemsetting/MainActivity;

    invoke-direct {v0, p0, v1}, Lcom/konka/systemsetting/net/NetworkSoftApConfig$2;-><init>(Lcom/konka/systemsetting/net/NetworkSoftApConfig;Lcom/konka/systemsetting/MainActivity;)V

    iput-object v0, p0, Lcom/konka/systemsetting/net/NetworkSoftApConfig;->mHolder:Lcom/konka/systemsetting/net/NetworkSoftApViewHolder;

    iget-object v0, p0, Lcom/konka/systemsetting/net/NetworkSoftApConfig;->mContext:Lcom/konka/systemsetting/MainActivity;

    const-string v1, "wifi"

    invoke-virtual {v0, v1}, Lcom/konka/systemsetting/MainActivity;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/net/wifi/WifiManager;

    iput-object v0, p0, Lcom/konka/systemsetting/net/NetworkSoftApConfig;->mWifiMgr:Landroid/net/wifi/WifiManager;

    invoke-virtual {p0}, Lcom/konka/systemsetting/net/NetworkSoftApConfig;->onResume()V

    return-void

    :array_0
    .array-data 4
        0x7f0600e0    # com.konka.systemsetting.R.string.softap_security_wpa
        0x7f0600e1    # com.konka.systemsetting.R.string.softap_security_wpa2
        0x7f0600e2    # com.konka.systemsetting.R.string.softap_security_open
    .end array-data
.end method

.method static synthetic access$0(Lcom/konka/systemsetting/net/NetworkSoftApConfig;I)V
    .locals 0

    invoke-direct {p0, p1}, Lcom/konka/systemsetting/net/NetworkSoftApConfig;->handleWifiApStateChanged(I)V

    return-void
.end method

.method static synthetic access$1(Lcom/konka/systemsetting/net/NetworkSoftApConfig;)Z
    .locals 1

    iget-boolean v0, p0, Lcom/konka/systemsetting/net/NetworkSoftApConfig;->isWifiAPEnabled:Z

    return v0
.end method

.method static synthetic access$10(Lcom/konka/systemsetting/net/NetworkSoftApConfig;)Z
    .locals 1

    invoke-direct {p0}, Lcom/konka/systemsetting/net/NetworkSoftApConfig;->onItemDetectClick()Z

    move-result v0

    return v0
.end method

.method static synthetic access$11(Lcom/konka/systemsetting/net/NetworkSoftApConfig;)I
    .locals 1

    iget v0, p0, Lcom/konka/systemsetting/net/NetworkSoftApConfig;->mCurrentSec:I

    return v0
.end method

.method static synthetic access$12(Lcom/konka/systemsetting/net/NetworkSoftApConfig;I)V
    .locals 0

    iput p1, p0, Lcom/konka/systemsetting/net/NetworkSoftApConfig;->mCurrentSec:I

    return-void
.end method

.method static synthetic access$13(Lcom/konka/systemsetting/net/NetworkSoftApConfig;)V
    .locals 0

    invoke-direct {p0}, Lcom/konka/systemsetting/net/NetworkSoftApConfig;->updateSecurityUI()V

    return-void
.end method

.method static synthetic access$2(Lcom/konka/systemsetting/net/NetworkSoftApConfig;Z)V
    .locals 0

    iput-boolean p1, p0, Lcom/konka/systemsetting/net/NetworkSoftApConfig;->isWifiAPEnabled:Z

    return-void
.end method

.method static synthetic access$3(Lcom/konka/systemsetting/net/NetworkSoftApConfig;)V
    .locals 0

    invoke-direct {p0}, Lcom/konka/systemsetting/net/NetworkSoftApConfig;->updateSwitch()V

    return-void
.end method

.method static synthetic access$4(Lcom/konka/systemsetting/net/NetworkSoftApConfig;Z)V
    .locals 0

    invoke-direct {p0, p1}, Lcom/konka/systemsetting/net/NetworkSoftApConfig;->updateSwitchUI(Z)V

    return-void
.end method

.method static synthetic access$5(Lcom/konka/systemsetting/net/NetworkSoftApConfig;)V
    .locals 0

    invoke-direct {p0}, Lcom/konka/systemsetting/net/NetworkSoftApConfig;->showSecuritySelectionDialog()V

    return-void
.end method

.method static synthetic access$6(Lcom/konka/systemsetting/net/NetworkSoftApConfig;)Z
    .locals 1

    iget-boolean v0, p0, Lcom/konka/systemsetting/net/NetworkSoftApConfig;->isPwdToBeShown:Z

    return v0
.end method

.method static synthetic access$7(Lcom/konka/systemsetting/net/NetworkSoftApConfig;Z)V
    .locals 0

    iput-boolean p1, p0, Lcom/konka/systemsetting/net/NetworkSoftApConfig;->isPwdToBeShown:Z

    return-void
.end method

.method static synthetic access$8(Lcom/konka/systemsetting/net/NetworkSoftApConfig;)V
    .locals 0

    invoke-direct {p0}, Lcom/konka/systemsetting/net/NetworkSoftApConfig;->updateShowPwdUI()V

    return-void
.end method

.method static synthetic access$9(Lcom/konka/systemsetting/net/NetworkSoftApConfig;)V
    .locals 0

    invoke-direct {p0}, Lcom/konka/systemsetting/net/NetworkSoftApConfig;->onItemSetClick()V

    return-void
.end method

.method private getSecurityTypeIndex(Landroid/net/wifi/WifiConfiguration;)I
    .locals 3
    .param p1    # Landroid/net/wifi/WifiConfiguration;

    const/4 v0, 0x1

    iget-object v1, p1, Landroid/net/wifi/WifiConfiguration;->allowedKeyManagement:Ljava/util/BitSet;

    invoke-virtual {v1, v0}, Ljava/util/BitSet;->get(I)Z

    move-result v1

    if-eqz v1, :cond_1

    const/4 v0, 0x0

    :cond_0
    :goto_0
    return v0

    :cond_1
    iget-object v1, p1, Landroid/net/wifi/WifiConfiguration;->allowedKeyManagement:Ljava/util/BitSet;

    const/4 v2, 0x4

    invoke-virtual {v1, v2}, Ljava/util/BitSet;->get(I)Z

    move-result v1

    if-nez v1, :cond_0

    const/4 v0, 0x2

    goto :goto_0
.end method

.method private handleWifiApStateChanged(I)V
    .locals 4
    .param p1    # I

    const/4 v3, 0x0

    const/4 v2, 0x1

    const-string v0, "NET_WIFI_AP ==>"

    const-string v1, "handleWifiApStateChanged"

    invoke-static {v0, v1}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    packed-switch p1, :pswitch_data_0

    const-string v0, "NET_WIFI_AP ==>"

    const-string v1, "failed"

    invoke-static {v0, v1}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    iget-object v0, p0, Lcom/konka/systemsetting/net/NetworkSoftApConfig;->mHolder:Lcom/konka/systemsetting/net/NetworkSoftApViewHolder;

    iget-object v1, p0, Lcom/konka/systemsetting/net/NetworkSoftApConfig;->mHolder:Lcom/konka/systemsetting/net/NetworkSoftApViewHolder;

    iget-object v1, v1, Lcom/konka/systemsetting/net/NetworkSoftApViewHolder;->itemSwitch:Landroid/widget/LinearLayout;

    invoke-virtual {v0, v1, v2}, Lcom/konka/systemsetting/net/NetworkSoftApViewHolder;->enableItem(Landroid/widget/LinearLayout;Z)V

    :goto_0
    return-void

    :pswitch_0
    const-string v0, "NET_WIFI_AP ==>"

    const-string v1, "WIFI_AP_STATE_ENABLING"

    invoke-static {v0, v1}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    iget-object v0, p0, Lcom/konka/systemsetting/net/NetworkSoftApConfig;->mHolder:Lcom/konka/systemsetting/net/NetworkSoftApViewHolder;

    iget-object v1, p0, Lcom/konka/systemsetting/net/NetworkSoftApConfig;->mHolder:Lcom/konka/systemsetting/net/NetworkSoftApViewHolder;

    iget-object v1, v1, Lcom/konka/systemsetting/net/NetworkSoftApViewHolder;->itemSwitch:Landroid/widget/LinearLayout;

    invoke-virtual {v0, v1, v3}, Lcom/konka/systemsetting/net/NetworkSoftApViewHolder;->enableItem(Landroid/widget/LinearLayout;Z)V

    goto :goto_0

    :pswitch_1
    const-string v0, "NET_WIFI_AP ==>"

    const-string v1, "WIFI_AP_STATE_ENABLED"

    invoke-static {v0, v1}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    iget-object v0, p0, Lcom/konka/systemsetting/net/NetworkSoftApConfig;->mHolder:Lcom/konka/systemsetting/net/NetworkSoftApViewHolder;

    iget-object v1, p0, Lcom/konka/systemsetting/net/NetworkSoftApConfig;->mHolder:Lcom/konka/systemsetting/net/NetworkSoftApViewHolder;

    iget-object v1, v1, Lcom/konka/systemsetting/net/NetworkSoftApViewHolder;->itemSwitch:Landroid/widget/LinearLayout;

    invoke-virtual {v0, v1, v2}, Lcom/konka/systemsetting/net/NetworkSoftApViewHolder;->enableItem(Landroid/widget/LinearLayout;Z)V

    invoke-virtual {p0}, Lcom/konka/systemsetting/net/NetworkSoftApConfig;->initWifiAP()V

    goto :goto_0

    :pswitch_2
    const-string v0, "NET_WIFI_AP ==>"

    const-string v1, "WIFI_AP_STATE_DISABLING"

    invoke-static {v0, v1}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    iget-object v0, p0, Lcom/konka/systemsetting/net/NetworkSoftApConfig;->mHolder:Lcom/konka/systemsetting/net/NetworkSoftApViewHolder;

    iget-object v1, p0, Lcom/konka/systemsetting/net/NetworkSoftApConfig;->mHolder:Lcom/konka/systemsetting/net/NetworkSoftApViewHolder;

    iget-object v1, v1, Lcom/konka/systemsetting/net/NetworkSoftApViewHolder;->itemSwitch:Landroid/widget/LinearLayout;

    invoke-virtual {v0, v1, v3}, Lcom/konka/systemsetting/net/NetworkSoftApViewHolder;->enableItem(Landroid/widget/LinearLayout;Z)V

    goto :goto_0

    :pswitch_3
    const-string v0, "NET_WIFI_AP ==>"

    const-string v1, "WIFI_AP_STATE_DISABLED"

    invoke-static {v0, v1}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    iget-object v0, p0, Lcom/konka/systemsetting/net/NetworkSoftApConfig;->mHolder:Lcom/konka/systemsetting/net/NetworkSoftApViewHolder;

    iget-object v1, p0, Lcom/konka/systemsetting/net/NetworkSoftApConfig;->mHolder:Lcom/konka/systemsetting/net/NetworkSoftApViewHolder;

    iget-object v1, v1, Lcom/konka/systemsetting/net/NetworkSoftApViewHolder;->itemSwitch:Landroid/widget/LinearLayout;

    invoke-virtual {v0, v1, v2}, Lcom/konka/systemsetting/net/NetworkSoftApViewHolder;->enableItem(Landroid/widget/LinearLayout;Z)V

    invoke-virtual {p0}, Lcom/konka/systemsetting/net/NetworkSoftApConfig;->initWifiAP()V

    goto :goto_0

    nop

    :pswitch_data_0
    .packed-switch 0xa
        :pswitch_2
        :pswitch_3
        :pswitch_0
        :pswitch_1
    .end packed-switch
.end method

.method private onItemDetectClick()Z
    .locals 5

    const/4 v2, 0x1

    iget-object v3, p0, Lcom/konka/systemsetting/net/NetworkSoftApConfig;->mContext:Lcom/konka/systemsetting/MainActivity;

    const-string v4, "connectivity"

    invoke-virtual {v3, v4}, Lcom/konka/systemsetting/MainActivity;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/net/ConnectivityManager;

    const/16 v3, 0x9

    invoke-virtual {v0, v3}, Landroid/net/ConnectivityManager;->getNetworkInfo(I)Landroid/net/NetworkInfo;

    move-result-object v3

    invoke-virtual {v3}, Landroid/net/NetworkInfo;->getState()Landroid/net/NetworkInfo$State;

    move-result-object v1

    invoke-virtual {v1}, Landroid/net/NetworkInfo$State;->ordinal()I

    move-result v3

    sget-object v4, Landroid/net/NetworkInfo$State;->CONNECTING:Landroid/net/NetworkInfo$State;

    invoke-virtual {v4}, Landroid/net/NetworkInfo$State;->ordinal()I

    move-result v4

    if-ne v3, v4, :cond_0

    iget-object v3, p0, Lcom/konka/systemsetting/net/NetworkSoftApConfig;->mContext:Lcom/konka/systemsetting/MainActivity;

    const v4, 0x7f06010e    # com.konka.systemsetting.R.string.eth_connecting

    invoke-virtual {v3, v4}, Lcom/konka/systemsetting/MainActivity;->toastText(I)V

    :goto_0
    return v2

    :cond_0
    invoke-virtual {v1}, Landroid/net/NetworkInfo$State;->ordinal()I

    move-result v3

    sget-object v4, Landroid/net/NetworkInfo$State;->CONNECTED:Landroid/net/NetworkInfo$State;

    invoke-virtual {v4}, Landroid/net/NetworkInfo$State;->ordinal()I

    move-result v4

    if-ne v3, v4, :cond_1

    iget-object v3, p0, Lcom/konka/systemsetting/net/NetworkSoftApConfig;->mContext:Lcom/konka/systemsetting/MainActivity;

    const v4, 0x7f06006c    # com.konka.systemsetting.R.string.eth_connected

    invoke-virtual {v3, v4}, Lcom/konka/systemsetting/MainActivity;->toastText(I)V

    goto :goto_0

    :cond_1
    invoke-virtual {v1}, Landroid/net/NetworkInfo$State;->ordinal()I

    move-result v3

    sget-object v4, Landroid/net/NetworkInfo$State;->SUSPENDED:Landroid/net/NetworkInfo$State;

    invoke-virtual {v4}, Landroid/net/NetworkInfo$State;->ordinal()I

    move-result v4

    if-ne v3, v4, :cond_2

    iget-object v3, p0, Lcom/konka/systemsetting/net/NetworkSoftApConfig;->mContext:Lcom/konka/systemsetting/MainActivity;

    const v4, 0x7f06010f    # com.konka.systemsetting.R.string.eth_suspended

    invoke-virtual {v3, v4}, Lcom/konka/systemsetting/MainActivity;->toastText(I)V

    goto :goto_0

    :cond_2
    invoke-virtual {v1}, Landroid/net/NetworkInfo$State;->ordinal()I

    move-result v3

    sget-object v4, Landroid/net/NetworkInfo$State;->DISCONNECTING:Landroid/net/NetworkInfo$State;

    invoke-virtual {v4}, Landroid/net/NetworkInfo$State;->ordinal()I

    move-result v4

    if-ne v3, v4, :cond_3

    iget-object v3, p0, Lcom/konka/systemsetting/net/NetworkSoftApConfig;->mContext:Lcom/konka/systemsetting/MainActivity;

    const v4, 0x7f060110    # com.konka.systemsetting.R.string.eth_disconnecting

    invoke-virtual {v3, v4}, Lcom/konka/systemsetting/MainActivity;->toastText(I)V

    goto :goto_0

    :cond_3
    invoke-virtual {v1}, Landroid/net/NetworkInfo$State;->ordinal()I

    move-result v3

    sget-object v4, Landroid/net/NetworkInfo$State;->DISCONNECTED:Landroid/net/NetworkInfo$State;

    invoke-virtual {v4}, Landroid/net/NetworkInfo$State;->ordinal()I

    move-result v4

    if-ne v3, v4, :cond_4

    iget-object v3, p0, Lcom/konka/systemsetting/net/NetworkSoftApConfig;->mContext:Lcom/konka/systemsetting/MainActivity;

    const v4, 0x7f0600b3    # com.konka.systemsetting.R.string.eth_disconnected

    invoke-virtual {v3, v4}, Lcom/konka/systemsetting/MainActivity;->toastText(I)V

    goto :goto_0

    :cond_4
    invoke-virtual {v1}, Landroid/net/NetworkInfo$State;->ordinal()I

    move-result v3

    sget-object v4, Landroid/net/NetworkInfo$State;->UNKNOWN:Landroid/net/NetworkInfo$State;

    invoke-virtual {v4}, Landroid/net/NetworkInfo$State;->ordinal()I

    move-result v4

    if-ne v3, v4, :cond_5

    iget-object v3, p0, Lcom/konka/systemsetting/net/NetworkSoftApConfig;->mContext:Lcom/konka/systemsetting/MainActivity;

    const v4, 0x7f0600b4    # com.konka.systemsetting.R.string.eth_unkown

    invoke-virtual {v3, v4}, Lcom/konka/systemsetting/MainActivity;->toastText(I)V

    goto :goto_0

    :cond_5
    iget-object v2, p0, Lcom/konka/systemsetting/net/NetworkSoftApConfig;->mContext:Lcom/konka/systemsetting/MainActivity;

    const v3, 0x7f0600b6    # com.konka.systemsetting.R.string.eth_enable

    invoke-virtual {v2, v3}, Lcom/konka/systemsetting/MainActivity;->toastText(I)V

    const/4 v2, 0x0

    goto :goto_0
.end method

.method private onItemSetClick()V
    .locals 0

    invoke-virtual {p0}, Lcom/konka/systemsetting/net/NetworkSoftApConfig;->saveWifiApConfig()V

    return-void
.end method

.method private setPasswordLayoutVisibale()V
    .locals 5

    const/16 v4, 0x8

    const/4 v3, 0x1

    const/4 v2, 0x0

    const/4 v0, 0x2

    iget v1, p0, Lcom/konka/systemsetting/net/NetworkSoftApConfig;->mCurrentSec:I

    if-eq v0, v1, :cond_0

    iget-object v0, p0, Lcom/konka/systemsetting/net/NetworkSoftApConfig;->mHolder:Lcom/konka/systemsetting/net/NetworkSoftApViewHolder;

    iget-object v0, v0, Lcom/konka/systemsetting/net/NetworkSoftApViewHolder;->itemSecurity:Landroid/widget/LinearLayout;

    const v1, 0x7f09004a    # com.konka.systemsetting.R.id.softap_password

    invoke-virtual {v0, v1}, Landroid/widget/LinearLayout;->setNextFocusDownId(I)V

    iget-object v0, p0, Lcom/konka/systemsetting/net/NetworkSoftApConfig;->mHolder:Lcom/konka/systemsetting/net/NetworkSoftApViewHolder;

    iget-object v0, v0, Lcom/konka/systemsetting/net/NetworkSoftApViewHolder;->itemSet:Landroid/widget/Button;

    const v1, 0x7f09004c    # com.konka.systemsetting.R.id.softap_showpwd

    invoke-virtual {v0, v1}, Landroid/widget/Button;->setNextFocusUpId(I)V

    iget-object v0, p0, Lcom/konka/systemsetting/net/NetworkSoftApConfig;->mHolder:Lcom/konka/systemsetting/net/NetworkSoftApViewHolder;

    iget-object v0, v0, Lcom/konka/systemsetting/net/NetworkSoftApViewHolder;->itemPassword:Landroid/widget/LinearLayout;

    invoke-virtual {v0, v2}, Landroid/widget/LinearLayout;->setVisibility(I)V

    iget-object v0, p0, Lcom/konka/systemsetting/net/NetworkSoftApConfig;->mHolder:Lcom/konka/systemsetting/net/NetworkSoftApViewHolder;

    iget-object v0, v0, Lcom/konka/systemsetting/net/NetworkSoftApViewHolder;->itemShowPwd:Landroid/widget/LinearLayout;

    invoke-virtual {v0, v2}, Landroid/widget/LinearLayout;->setVisibility(I)V

    iget-object v0, p0, Lcom/konka/systemsetting/net/NetworkSoftApConfig;->mHolder:Lcom/konka/systemsetting/net/NetworkSoftApViewHolder;

    iget-object v1, p0, Lcom/konka/systemsetting/net/NetworkSoftApConfig;->mHolder:Lcom/konka/systemsetting/net/NetworkSoftApViewHolder;

    iget-object v1, v1, Lcom/konka/systemsetting/net/NetworkSoftApViewHolder;->itemPassword:Landroid/widget/LinearLayout;

    invoke-virtual {v0, v1, v3}, Lcom/konka/systemsetting/net/NetworkSoftApViewHolder;->enableItem(Landroid/widget/LinearLayout;Z)V

    iget-object v0, p0, Lcom/konka/systemsetting/net/NetworkSoftApConfig;->mHolder:Lcom/konka/systemsetting/net/NetworkSoftApViewHolder;

    iget-object v1, p0, Lcom/konka/systemsetting/net/NetworkSoftApConfig;->mHolder:Lcom/konka/systemsetting/net/NetworkSoftApViewHolder;

    iget-object v1, v1, Lcom/konka/systemsetting/net/NetworkSoftApViewHolder;->itemShowPwd:Landroid/widget/LinearLayout;

    invoke-virtual {v0, v1, v3}, Lcom/konka/systemsetting/net/NetworkSoftApViewHolder;->enableItem(Landroid/widget/LinearLayout;Z)V

    :goto_0
    return-void

    :cond_0
    iget-object v0, p0, Lcom/konka/systemsetting/net/NetworkSoftApConfig;->mHolder:Lcom/konka/systemsetting/net/NetworkSoftApViewHolder;

    iget-object v0, v0, Lcom/konka/systemsetting/net/NetworkSoftApViewHolder;->itemSecurity:Landroid/widget/LinearLayout;

    const v1, 0x7f09004f    # com.konka.systemsetting.R.id.softap_set

    invoke-virtual {v0, v1}, Landroid/widget/LinearLayout;->setNextFocusDownId(I)V

    iget-object v0, p0, Lcom/konka/systemsetting/net/NetworkSoftApConfig;->mHolder:Lcom/konka/systemsetting/net/NetworkSoftApViewHolder;

    iget-object v0, v0, Lcom/konka/systemsetting/net/NetworkSoftApViewHolder;->itemSet:Landroid/widget/Button;

    const v1, 0x7f090049    # com.konka.systemsetting.R.id.softap_security

    invoke-virtual {v0, v1}, Landroid/widget/Button;->setNextFocusUpId(I)V

    iget-object v0, p0, Lcom/konka/systemsetting/net/NetworkSoftApConfig;->mHolder:Lcom/konka/systemsetting/net/NetworkSoftApViewHolder;

    iget-object v0, v0, Lcom/konka/systemsetting/net/NetworkSoftApViewHolder;->itemPassword:Landroid/widget/LinearLayout;

    invoke-virtual {v0, v4}, Landroid/widget/LinearLayout;->setVisibility(I)V

    iget-object v0, p0, Lcom/konka/systemsetting/net/NetworkSoftApConfig;->mHolder:Lcom/konka/systemsetting/net/NetworkSoftApViewHolder;

    iget-object v0, v0, Lcom/konka/systemsetting/net/NetworkSoftApViewHolder;->itemShowPwd:Landroid/widget/LinearLayout;

    invoke-virtual {v0, v4}, Landroid/widget/LinearLayout;->setVisibility(I)V

    iget-object v0, p0, Lcom/konka/systemsetting/net/NetworkSoftApConfig;->mHolder:Lcom/konka/systemsetting/net/NetworkSoftApViewHolder;

    iget-object v1, p0, Lcom/konka/systemsetting/net/NetworkSoftApConfig;->mHolder:Lcom/konka/systemsetting/net/NetworkSoftApViewHolder;

    iget-object v1, v1, Lcom/konka/systemsetting/net/NetworkSoftApViewHolder;->itemPassword:Landroid/widget/LinearLayout;

    invoke-virtual {v0, v1, v2}, Lcom/konka/systemsetting/net/NetworkSoftApViewHolder;->enableItem(Landroid/widget/LinearLayout;Z)V

    iget-object v0, p0, Lcom/konka/systemsetting/net/NetworkSoftApConfig;->mHolder:Lcom/konka/systemsetting/net/NetworkSoftApViewHolder;

    iget-object v1, p0, Lcom/konka/systemsetting/net/NetworkSoftApConfig;->mHolder:Lcom/konka/systemsetting/net/NetworkSoftApViewHolder;

    iget-object v1, v1, Lcom/konka/systemsetting/net/NetworkSoftApViewHolder;->itemShowPwd:Landroid/widget/LinearLayout;

    invoke-virtual {v0, v1, v2}, Lcom/konka/systemsetting/net/NetworkSoftApViewHolder;->enableItem(Landroid/widget/LinearLayout;Z)V

    goto :goto_0
.end method

.method private showSecuritySelectionDialog()V
    .locals 6

    new-instance v0, Landroid/app/AlertDialog$Builder;

    iget-object v3, p0, Lcom/konka/systemsetting/net/NetworkSoftApConfig;->mContext:Lcom/konka/systemsetting/MainActivity;

    invoke-direct {v0, v3}, Landroid/app/AlertDialog$Builder;-><init>(Landroid/content/Context;)V

    iget-object v3, p0, Lcom/konka/systemsetting/net/NetworkSoftApConfig;->mContext:Lcom/konka/systemsetting/MainActivity;

    const v4, 0x7f0600d8    # com.konka.systemsetting.R.string.softap

    invoke-virtual {v3, v4}, Lcom/konka/systemsetting/MainActivity;->getText(I)Ljava/lang/CharSequence;

    move-result-object v3

    invoke-interface {v3}, Ljava/lang/CharSequence;->toString()Ljava/lang/String;

    move-result-object v2

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-static {v2}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v4

    invoke-direct {v3, v4}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    const-string v4, ": "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    iget-object v4, p0, Lcom/konka/systemsetting/net/NetworkSoftApConfig;->mContext:Lcom/konka/systemsetting/MainActivity;

    const v5, 0x7f0600db    # com.konka.systemsetting.R.string.softap_security

    invoke-virtual {v4, v5}, Lcom/konka/systemsetting/MainActivity;->getText(I)Ljava/lang/CharSequence;

    move-result-object v4

    invoke-interface {v4}, Ljava/lang/CharSequence;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0, v2}, Landroid/app/AlertDialog$Builder;->setTitle(Ljava/lang/CharSequence;)Landroid/app/AlertDialog$Builder;

    const v3, 0x7f020020    # com.konka.systemsetting.R.drawable.syssettingfirsticonnetunfocus

    invoke-virtual {v0, v3}, Landroid/app/AlertDialog$Builder;->setIcon(I)Landroid/app/AlertDialog$Builder;

    new-instance v1, Lcom/konka/systemsetting/net/NetworkSoftApConfig$3;

    invoke-direct {v1, p0}, Lcom/konka/systemsetting/net/NetworkSoftApConfig$3;-><init>(Lcom/konka/systemsetting/net/NetworkSoftApConfig;)V

    const v3, 0x7f07000c    # com.konka.systemsetting.R.array.softap_security_vals

    iget v4, p0, Lcom/konka/systemsetting/net/NetworkSoftApConfig;->mCurrentSec:I

    invoke-virtual {v0, v3, v4, v1}, Landroid/app/AlertDialog$Builder;->setSingleChoiceItems(IILandroid/content/DialogInterface$OnClickListener;)Landroid/app/AlertDialog$Builder;

    invoke-virtual {v0}, Landroid/app/AlertDialog$Builder;->show()Landroid/app/AlertDialog;

    return-void
.end method

.method private showWifiApInfo()V
    .locals 3

    iget-object v0, p0, Lcom/konka/systemsetting/net/NetworkSoftApConfig;->mWifiApConfig:Landroid/net/wifi/WifiConfiguration;

    if-nez v0, :cond_0

    const-string v0, "NET_WIFI_AP ==>"

    const-string v1, "mWifiApConfig ==  null"

    invoke-static {v0, v1}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    :goto_0
    return-void

    :cond_0
    iget-object v0, p0, Lcom/konka/systemsetting/net/NetworkSoftApConfig;->mHolder:Lcom/konka/systemsetting/net/NetworkSoftApViewHolder;

    iget-object v0, v0, Lcom/konka/systemsetting/net/NetworkSoftApViewHolder;->etSSID:Landroid/widget/EditText;

    iget-object v1, p0, Lcom/konka/systemsetting/net/NetworkSoftApConfig;->mWifiApConfig:Landroid/net/wifi/WifiConfiguration;

    iget-object v1, v1, Landroid/net/wifi/WifiConfiguration;->SSID:Ljava/lang/String;

    invoke-virtual {v0, v1}, Landroid/widget/EditText;->setText(Ljava/lang/CharSequence;)V

    iget-object v0, p0, Lcom/konka/systemsetting/net/NetworkSoftApConfig;->mHolder:Lcom/konka/systemsetting/net/NetworkSoftApViewHolder;

    iget-object v0, v0, Lcom/konka/systemsetting/net/NetworkSoftApViewHolder;->itemSecurity:Landroid/widget/LinearLayout;

    const/4 v1, 0x1

    invoke-virtual {v0, v1}, Landroid/widget/LinearLayout;->getChildAt(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    iget-object v1, p0, Lcom/konka/systemsetting/net/NetworkSoftApConfig;->SecType:[I

    iget v2, p0, Lcom/konka/systemsetting/net/NetworkSoftApConfig;->mCurrentSec:I

    aget v1, v1, v2

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setText(I)V

    const/4 v0, 0x2

    iget-object v1, p0, Lcom/konka/systemsetting/net/NetworkSoftApConfig;->mWifiApConfig:Landroid/net/wifi/WifiConfiguration;

    invoke-direct {p0, v1}, Lcom/konka/systemsetting/net/NetworkSoftApConfig;->getSecurityTypeIndex(Landroid/net/wifi/WifiConfiguration;)I

    move-result v1

    if-eq v0, v1, :cond_1

    iget-object v0, p0, Lcom/konka/systemsetting/net/NetworkSoftApConfig;->mHolder:Lcom/konka/systemsetting/net/NetworkSoftApViewHolder;

    iget-object v0, v0, Lcom/konka/systemsetting/net/NetworkSoftApViewHolder;->etPassword:Landroid/widget/EditText;

    iget-object v1, p0, Lcom/konka/systemsetting/net/NetworkSoftApConfig;->mWifiApConfig:Landroid/net/wifi/WifiConfiguration;

    iget-object v1, v1, Landroid/net/wifi/WifiConfiguration;->preSharedKey:Ljava/lang/String;

    invoke-virtual {v0, v1}, Landroid/widget/EditText;->setText(Ljava/lang/CharSequence;)V

    goto :goto_0

    :cond_1
    iget-object v0, p0, Lcom/konka/systemsetting/net/NetworkSoftApConfig;->mHolder:Lcom/konka/systemsetting/net/NetworkSoftApViewHolder;

    iget-object v0, v0, Lcom/konka/systemsetting/net/NetworkSoftApViewHolder;->etPassword:Landroid/widget/EditText;

    const-string v1, ""

    invoke-virtual {v0, v1}, Landroid/widget/EditText;->setText(Ljava/lang/CharSequence;)V

    goto :goto_0
.end method

.method private updateSecurityUI()V
    .locals 3

    iget-object v1, p0, Lcom/konka/systemsetting/net/NetworkSoftApConfig;->mHolder:Lcom/konka/systemsetting/net/NetworkSoftApViewHolder;

    iget-object v1, v1, Lcom/konka/systemsetting/net/NetworkSoftApViewHolder;->itemSecurity:Landroid/widget/LinearLayout;

    const/4 v2, 0x1

    invoke-virtual {v1, v2}, Landroid/widget/LinearLayout;->getChildAt(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    iget-object v1, p0, Lcom/konka/systemsetting/net/NetworkSoftApConfig;->SecType:[I

    iget v2, p0, Lcom/konka/systemsetting/net/NetworkSoftApConfig;->mCurrentSec:I

    aget v1, v1, v2

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setText(I)V

    invoke-direct {p0}, Lcom/konka/systemsetting/net/NetworkSoftApConfig;->setPasswordLayoutVisibale()V

    return-void
.end method

.method private updateShowPwdUI()V
    .locals 4

    invoke-static {}, Landroid/text/method/HideReturnsTransformationMethod;->getInstance()Landroid/text/method/HideReturnsTransformationMethod;

    move-result-object v0

    iget-boolean v2, p0, Lcom/konka/systemsetting/net/NetworkSoftApConfig;->isPwdToBeShown:Z

    if-eqz v2, :cond_0

    invoke-static {}, Landroid/text/method/PasswordTransformationMethod;->getInstance()Landroid/text/method/PasswordTransformationMethod;

    move-result-object v0

    :goto_0
    iget-object v2, p0, Lcom/konka/systemsetting/net/NetworkSoftApConfig;->mHolder:Lcom/konka/systemsetting/net/NetworkSoftApViewHolder;

    iget-object v2, v2, Lcom/konka/systemsetting/net/NetworkSoftApViewHolder;->etPassword:Landroid/widget/EditText;

    invoke-virtual {v2, v0}, Landroid/widget/EditText;->setTransformationMethod(Landroid/text/method/TransformationMethod;)V

    iget-boolean v2, p0, Lcom/konka/systemsetting/net/NetworkSoftApConfig;->isPwdToBeShown:Z

    if-eqz v2, :cond_1

    iget-object v2, p0, Lcom/konka/systemsetting/net/NetworkSoftApConfig;->mContext:Lcom/konka/systemsetting/MainActivity;

    iget v1, v2, Lcom/konka/systemsetting/MainActivity;->IMG_SWITCH_OFF:I

    :goto_1
    iget-object v2, p0, Lcom/konka/systemsetting/net/NetworkSoftApConfig;->mHolder:Lcom/konka/systemsetting/net/NetworkSoftApViewHolder;

    iget-object v2, v2, Lcom/konka/systemsetting/net/NetworkSoftApViewHolder;->itemShowPwd:Landroid/widget/LinearLayout;

    const/4 v3, 0x1

    invoke-virtual {v2, v3}, Landroid/widget/LinearLayout;->getChildAt(I)Landroid/view/View;

    move-result-object v2

    check-cast v2, Landroid/widget/ImageView;

    invoke-virtual {v2, v1}, Landroid/widget/ImageView;->setBackgroundResource(I)V

    return-void

    :cond_0
    invoke-static {}, Landroid/text/method/HideReturnsTransformationMethod;->getInstance()Landroid/text/method/HideReturnsTransformationMethod;

    move-result-object v0

    goto :goto_0

    :cond_1
    iget-object v2, p0, Lcom/konka/systemsetting/net/NetworkSoftApConfig;->mContext:Lcom/konka/systemsetting/MainActivity;

    iget v1, v2, Lcom/konka/systemsetting/MainActivity;->IMG_SWITCH_ON:I

    goto :goto_1
.end method

.method private updateSwitch()V
    .locals 3

    const/4 v1, 0x0

    invoke-virtual {p0}, Lcom/konka/systemsetting/net/NetworkSoftApConfig;->checkNetStatus()Z

    move-result v0

    if-nez v0, :cond_0

    iput-boolean v1, p0, Lcom/konka/systemsetting/net/NetworkSoftApConfig;->isWifiAPEnabled:Z

    :goto_0
    return-void

    :cond_0
    iget-object v0, p0, Lcom/konka/systemsetting/net/NetworkSoftApConfig;->mWifiMgr:Landroid/net/wifi/WifiManager;

    invoke-virtual {v0, v1}, Landroid/net/wifi/WifiManager;->setWifiEnabled(Z)Z

    iget-object v0, p0, Lcom/konka/systemsetting/net/NetworkSoftApConfig;->mWifiMgr:Landroid/net/wifi/WifiManager;

    iget-object v1, p0, Lcom/konka/systemsetting/net/NetworkSoftApConfig;->mWifiApConfig:Landroid/net/wifi/WifiConfiguration;

    iget-boolean v2, p0, Lcom/konka/systemsetting/net/NetworkSoftApConfig;->isWifiAPEnabled:Z

    invoke-virtual {v0, v1, v2}, Landroid/net/wifi/WifiManager;->setWifiApEnabled(Landroid/net/wifi/WifiConfiguration;Z)Z

    goto :goto_0
.end method

.method private updateSwitchUI(Z)V
    .locals 5
    .param p1    # Z

    const/4 v4, 0x1

    if-eqz p1, :cond_0

    iget-object v2, p0, Lcom/konka/systemsetting/net/NetworkSoftApConfig;->mContext:Lcom/konka/systemsetting/MainActivity;

    iget v1, v2, Lcom/konka/systemsetting/MainActivity;->IMG_SWITCH_ON:I

    :goto_0
    iget-object v2, p0, Lcom/konka/systemsetting/net/NetworkSoftApConfig;->mHolder:Lcom/konka/systemsetting/net/NetworkSoftApViewHolder;

    iget-object v2, v2, Lcom/konka/systemsetting/net/NetworkSoftApViewHolder;->itemSwitch:Landroid/widget/LinearLayout;

    invoke-virtual {v2, v4}, Landroid/widget/LinearLayout;->getChildAt(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/ImageView;

    invoke-virtual {v0, v1}, Landroid/widget/ImageView;->setBackgroundResource(I)V

    iget-object v2, p0, Lcom/konka/systemsetting/net/NetworkSoftApConfig;->mHolder:Lcom/konka/systemsetting/net/NetworkSoftApViewHolder;

    iget-object v3, p0, Lcom/konka/systemsetting/net/NetworkSoftApConfig;->mHolder:Lcom/konka/systemsetting/net/NetworkSoftApViewHolder;

    iget-object v3, v3, Lcom/konka/systemsetting/net/NetworkSoftApViewHolder;->itemSwitch:Landroid/widget/LinearLayout;

    invoke-virtual {v2, v3, v4}, Lcom/konka/systemsetting/net/NetworkSoftApViewHolder;->enableItem(Landroid/widget/LinearLayout;Z)V

    iget-object v2, p0, Lcom/konka/systemsetting/net/NetworkSoftApConfig;->mHolder:Lcom/konka/systemsetting/net/NetworkSoftApViewHolder;

    iget-object v3, p0, Lcom/konka/systemsetting/net/NetworkSoftApConfig;->mHolder:Lcom/konka/systemsetting/net/NetworkSoftApViewHolder;

    iget-object v3, v3, Lcom/konka/systemsetting/net/NetworkSoftApViewHolder;->itemSSID:Landroid/widget/LinearLayout;

    invoke-virtual {v2, v3, p1}, Lcom/konka/systemsetting/net/NetworkSoftApViewHolder;->enableItem(Landroid/widget/LinearLayout;Z)V

    iget-object v2, p0, Lcom/konka/systemsetting/net/NetworkSoftApConfig;->mHolder:Lcom/konka/systemsetting/net/NetworkSoftApViewHolder;

    iget-object v3, p0, Lcom/konka/systemsetting/net/NetworkSoftApConfig;->mHolder:Lcom/konka/systemsetting/net/NetworkSoftApViewHolder;

    iget-object v3, v3, Lcom/konka/systemsetting/net/NetworkSoftApViewHolder;->itemSecurity:Landroid/widget/LinearLayout;

    invoke-virtual {v2, v3, p1}, Lcom/konka/systemsetting/net/NetworkSoftApViewHolder;->enableItem(Landroid/widget/LinearLayout;Z)V

    iget-object v2, p0, Lcom/konka/systemsetting/net/NetworkSoftApConfig;->mHolder:Lcom/konka/systemsetting/net/NetworkSoftApViewHolder;

    iget-object v3, p0, Lcom/konka/systemsetting/net/NetworkSoftApConfig;->mHolder:Lcom/konka/systemsetting/net/NetworkSoftApViewHolder;

    iget-object v3, v3, Lcom/konka/systemsetting/net/NetworkSoftApViewHolder;->itemPassword:Landroid/widget/LinearLayout;

    invoke-virtual {v2, v3, p1}, Lcom/konka/systemsetting/net/NetworkSoftApViewHolder;->enableItem(Landroid/widget/LinearLayout;Z)V

    iget-object v2, p0, Lcom/konka/systemsetting/net/NetworkSoftApConfig;->mHolder:Lcom/konka/systemsetting/net/NetworkSoftApViewHolder;

    iget-object v3, p0, Lcom/konka/systemsetting/net/NetworkSoftApConfig;->mHolder:Lcom/konka/systemsetting/net/NetworkSoftApViewHolder;

    iget-object v3, v3, Lcom/konka/systemsetting/net/NetworkSoftApViewHolder;->itemShowPwd:Landroid/widget/LinearLayout;

    invoke-virtual {v2, v3, p1}, Lcom/konka/systemsetting/net/NetworkSoftApViewHolder;->enableItem(Landroid/widget/LinearLayout;Z)V

    iget-object v2, p0, Lcom/konka/systemsetting/net/NetworkSoftApConfig;->mHolder:Lcom/konka/systemsetting/net/NetworkSoftApViewHolder;

    iget-object v3, p0, Lcom/konka/systemsetting/net/NetworkSoftApConfig;->mHolder:Lcom/konka/systemsetting/net/NetworkSoftApViewHolder;

    iget-object v3, v3, Lcom/konka/systemsetting/net/NetworkSoftApViewHolder;->itemSet:Landroid/widget/Button;

    invoke-virtual {v2, v3, p1}, Lcom/konka/systemsetting/net/NetworkSoftApViewHolder;->enableItem(Landroid/widget/Button;Z)V

    iget-object v2, p0, Lcom/konka/systemsetting/net/NetworkSoftApConfig;->mHolder:Lcom/konka/systemsetting/net/NetworkSoftApViewHolder;

    iget-object v3, p0, Lcom/konka/systemsetting/net/NetworkSoftApConfig;->mHolder:Lcom/konka/systemsetting/net/NetworkSoftApViewHolder;

    iget-object v3, v3, Lcom/konka/systemsetting/net/NetworkSoftApViewHolder;->itemDetect:Landroid/widget/Button;

    invoke-virtual {v2, v3, p1}, Lcom/konka/systemsetting/net/NetworkSoftApViewHolder;->enableItem(Landroid/widget/Button;Z)V

    return-void

    :cond_0
    iget-object v2, p0, Lcom/konka/systemsetting/net/NetworkSoftApConfig;->mContext:Lcom/konka/systemsetting/MainActivity;

    iget v1, v2, Lcom/konka/systemsetting/MainActivity;->IMG_SWITCH_OFF:I

    goto :goto_0
.end method


# virtual methods
.method public checkNetStatus()Z
    .locals 5

    const v4, 0x7f060104    # com.konka.systemsetting.R.string.softap_wiredpppoeconnecting

    const v3, 0x7f060103    # com.konka.systemsetting.R.string.softap_dongle_notfound

    const/4 v0, 0x0

    invoke-static {}, Lcom/mstar/android/wifi/MWifiManager;->getInstance()Lcom/mstar/android/wifi/MWifiManager;

    move-result-object v1

    invoke-virtual {v1}, Lcom/mstar/android/wifi/MWifiManager;->isWifiDeviceExist()Z

    move-result v1

    if-nez v1, :cond_0

    iget-object v1, p0, Lcom/konka/systemsetting/net/NetworkSoftApConfig;->mContext:Lcom/konka/systemsetting/MainActivity;

    invoke-virtual {v1, v3}, Lcom/konka/systemsetting/MainActivity;->toastText(I)V

    const-string v1, "NET_WIFI_AP ==>"

    iget-object v2, p0, Lcom/konka/systemsetting/net/NetworkSoftApConfig;->mContext:Lcom/konka/systemsetting/MainActivity;

    invoke-virtual {v2, v3}, Lcom/konka/systemsetting/MainActivity;->getText(I)Ljava/lang/CharSequence;

    move-result-object v2

    invoke-interface {v2}, Ljava/lang/CharSequence;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    :goto_0
    return v0

    :cond_0
    iget-object v1, p0, Lcom/konka/systemsetting/net/NetworkSoftApConfig;->mPageMgr:Lcom/konka/systemsetting/net/NetworkPageManager;

    iget-object v1, v1, Lcom/konka/systemsetting/net/NetworkPageManager;->m_PppoeConfig:Lcom/konka/systemsetting/net/NetworkPPPoeConfig;

    if-eqz v1, :cond_1

    iget-object v1, p0, Lcom/konka/systemsetting/net/NetworkSoftApConfig;->mPageMgr:Lcom/konka/systemsetting/net/NetworkPageManager;

    iget-object v1, v1, Lcom/konka/systemsetting/net/NetworkPageManager;->m_PppoeConfig:Lcom/konka/systemsetting/net/NetworkPPPoeConfig;

    invoke-virtual {v1}, Lcom/konka/systemsetting/net/NetworkPPPoeConfig;->isPppoeActive()Z

    move-result v1

    if-eqz v1, :cond_1

    iget-object v1, p0, Lcom/konka/systemsetting/net/NetworkSoftApConfig;->mPageMgr:Lcom/konka/systemsetting/net/NetworkPageManager;

    iget-object v1, v1, Lcom/konka/systemsetting/net/NetworkPageManager;->m_PppoeConfig:Lcom/konka/systemsetting/net/NetworkPPPoeConfig;

    invoke-virtual {v1}, Lcom/konka/systemsetting/net/NetworkPPPoeConfig;->isWirePPPoE()Z

    move-result v1

    if-nez v1, :cond_1

    iget-object v1, p0, Lcom/konka/systemsetting/net/NetworkSoftApConfig;->mContext:Lcom/konka/systemsetting/MainActivity;

    invoke-virtual {v1, v4}, Lcom/konka/systemsetting/MainActivity;->toastText(I)V

    const-string v1, "NET_WIFI_AP ==>"

    iget-object v2, p0, Lcom/konka/systemsetting/net/NetworkSoftApConfig;->mContext:Lcom/konka/systemsetting/MainActivity;

    invoke-virtual {v2, v4}, Lcom/konka/systemsetting/MainActivity;->getText(I)Ljava/lang/CharSequence;

    move-result-object v2

    invoke-interface {v2}, Ljava/lang/CharSequence;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_0

    :cond_1
    const/4 v0, 0x1

    goto :goto_0
.end method

.method public getApConfig()Landroid/net/wifi/WifiConfiguration;
    .locals 5

    const/4 v4, 0x0

    new-instance v0, Landroid/net/wifi/WifiConfiguration;

    invoke-direct {v0}, Landroid/net/wifi/WifiConfiguration;-><init>()V

    iget-object v2, p0, Lcom/konka/systemsetting/net/NetworkSoftApConfig;->mHolder:Lcom/konka/systemsetting/net/NetworkSoftApViewHolder;

    iget-object v2, v2, Lcom/konka/systemsetting/net/NetworkSoftApViewHolder;->etSSID:Landroid/widget/EditText;

    invoke-virtual {v2}, Landroid/widget/EditText;->getText()Landroid/text/Editable;

    move-result-object v2

    invoke-interface {v2}, Landroid/text/Editable;->toString()Ljava/lang/String;

    move-result-object v2

    iput-object v2, v0, Landroid/net/wifi/WifiConfiguration;->SSID:Ljava/lang/String;

    iget v2, p0, Lcom/konka/systemsetting/net/NetworkSoftApConfig;->mCurrentSec:I

    packed-switch v2, :pswitch_data_0

    const/4 v0, 0x0

    :goto_0
    return-object v0

    :pswitch_0
    iget-object v2, v0, Landroid/net/wifi/WifiConfiguration;->allowedKeyManagement:Ljava/util/BitSet;

    invoke-virtual {v2, v4}, Ljava/util/BitSet;->set(I)V

    goto :goto_0

    :pswitch_1
    iget-object v2, v0, Landroid/net/wifi/WifiConfiguration;->allowedKeyManagement:Ljava/util/BitSet;

    const/4 v3, 0x1

    invoke-virtual {v2, v3}, Ljava/util/BitSet;->set(I)V

    iget-object v2, v0, Landroid/net/wifi/WifiConfiguration;->allowedAuthAlgorithms:Ljava/util/BitSet;

    invoke-virtual {v2, v4}, Ljava/util/BitSet;->set(I)V

    iget-object v2, p0, Lcom/konka/systemsetting/net/NetworkSoftApConfig;->mHolder:Lcom/konka/systemsetting/net/NetworkSoftApViewHolder;

    iget-object v2, v2, Lcom/konka/systemsetting/net/NetworkSoftApViewHolder;->etPassword:Landroid/widget/EditText;

    invoke-virtual {v2}, Landroid/widget/EditText;->length()I

    move-result v2

    if-eqz v2, :cond_0

    iget-object v2, p0, Lcom/konka/systemsetting/net/NetworkSoftApConfig;->mHolder:Lcom/konka/systemsetting/net/NetworkSoftApViewHolder;

    iget-object v2, v2, Lcom/konka/systemsetting/net/NetworkSoftApViewHolder;->etPassword:Landroid/widget/EditText;

    invoke-virtual {v2}, Landroid/widget/EditText;->getText()Landroid/text/Editable;

    move-result-object v2

    invoke-interface {v2}, Landroid/text/Editable;->toString()Ljava/lang/String;

    move-result-object v1

    iput-object v1, v0, Landroid/net/wifi/WifiConfiguration;->preSharedKey:Ljava/lang/String;

    :cond_0
    const-string v2, "NET_WIFI_AP ==>"

    new-instance v3, Ljava/lang/StringBuilder;

    const-string v4, "getApConfig() -> SECURE_WPA -> config.preSharedKey = "

    invoke-direct {v3, v4}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    iget-object v4, v0, Landroid/net/wifi/WifiConfiguration;->preSharedKey:Ljava/lang/String;

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v2, v3}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    const-string v2, "NET_WIFI_AP ==>"

    new-instance v3, Ljava/lang/StringBuilder;

    const-string v4, "curr EditText_Pwd = "

    invoke-direct {v3, v4}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    iget-object v4, p0, Lcom/konka/systemsetting/net/NetworkSoftApConfig;->mHolder:Lcom/konka/systemsetting/net/NetworkSoftApViewHolder;

    iget-object v4, v4, Lcom/konka/systemsetting/net/NetworkSoftApViewHolder;->etPassword:Landroid/widget/EditText;

    invoke-virtual {v4}, Landroid/widget/EditText;->getText()Landroid/text/Editable;

    move-result-object v4

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v2, v3}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_0

    :pswitch_2
    iget-object v2, v0, Landroid/net/wifi/WifiConfiguration;->allowedKeyManagement:Ljava/util/BitSet;

    const/4 v3, 0x4

    invoke-virtual {v2, v3}, Ljava/util/BitSet;->set(I)V

    iget-object v2, v0, Landroid/net/wifi/WifiConfiguration;->allowedAuthAlgorithms:Ljava/util/BitSet;

    invoke-virtual {v2, v4}, Ljava/util/BitSet;->set(I)V

    iget-object v2, p0, Lcom/konka/systemsetting/net/NetworkSoftApConfig;->mHolder:Lcom/konka/systemsetting/net/NetworkSoftApViewHolder;

    iget-object v2, v2, Lcom/konka/systemsetting/net/NetworkSoftApViewHolder;->etPassword:Landroid/widget/EditText;

    invoke-virtual {v2}, Landroid/widget/EditText;->length()I

    move-result v2

    if-eqz v2, :cond_1

    iget-object v2, p0, Lcom/konka/systemsetting/net/NetworkSoftApConfig;->mHolder:Lcom/konka/systemsetting/net/NetworkSoftApViewHolder;

    iget-object v2, v2, Lcom/konka/systemsetting/net/NetworkSoftApViewHolder;->etPassword:Landroid/widget/EditText;

    invoke-virtual {v2}, Landroid/widget/EditText;->getText()Landroid/text/Editable;

    move-result-object v2

    invoke-interface {v2}, Landroid/text/Editable;->toString()Ljava/lang/String;

    move-result-object v1

    iput-object v1, v0, Landroid/net/wifi/WifiConfiguration;->preSharedKey:Ljava/lang/String;

    :cond_1
    const-string v2, "NET_WIFI_AP ==>"

    new-instance v3, Ljava/lang/StringBuilder;

    const-string v4, "getApConfig() -> SECURE_WPA2 -> config.preSharedKey = "

    invoke-direct {v3, v4}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    iget-object v4, v0, Landroid/net/wifi/WifiConfiguration;->preSharedKey:Ljava/lang/String;

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v2, v3}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    const-string v2, "NET_WIFI_AP ==>"

    new-instance v3, Ljava/lang/StringBuilder;

    const-string v4, "curr EditText_Pwd = "

    invoke-direct {v3, v4}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    iget-object v4, p0, Lcom/konka/systemsetting/net/NetworkSoftApConfig;->mHolder:Lcom/konka/systemsetting/net/NetworkSoftApViewHolder;

    iget-object v4, v4, Lcom/konka/systemsetting/net/NetworkSoftApViewHolder;->etPassword:Landroid/widget/EditText;

    invoke-virtual {v4}, Landroid/widget/EditText;->getText()Landroid/text/Editable;

    move-result-object v4

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v2, v3}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    goto/16 :goto_0

    :pswitch_data_0
    .packed-switch 0x0
        :pswitch_1
        :pswitch_2
        :pswitch_0
    .end packed-switch
.end method

.method public initWifiAP()V
    .locals 3

    iget-object v0, p0, Lcom/konka/systemsetting/net/NetworkSoftApConfig;->mWifiMgr:Landroid/net/wifi/WifiManager;

    invoke-virtual {v0}, Landroid/net/wifi/WifiManager;->isWifiApEnabled()Z

    move-result v0

    iput-boolean v0, p0, Lcom/konka/systemsetting/net/NetworkSoftApConfig;->isWifiAPEnabled:Z

    iget-object v0, p0, Lcom/konka/systemsetting/net/NetworkSoftApConfig;->mWifiMgr:Landroid/net/wifi/WifiManager;

    invoke-virtual {v0}, Landroid/net/wifi/WifiManager;->getWifiApConfiguration()Landroid/net/wifi/WifiConfiguration;

    move-result-object v0

    iput-object v0, p0, Lcom/konka/systemsetting/net/NetworkSoftApConfig;->mWifiApConfig:Landroid/net/wifi/WifiConfiguration;

    iget-object v0, p0, Lcom/konka/systemsetting/net/NetworkSoftApConfig;->mWifiApConfig:Landroid/net/wifi/WifiConfiguration;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/konka/systemsetting/net/NetworkSoftApConfig;->mWifiApConfig:Landroid/net/wifi/WifiConfiguration;

    invoke-direct {p0, v0}, Lcom/konka/systemsetting/net/NetworkSoftApConfig;->getSecurityTypeIndex(Landroid/net/wifi/WifiConfiguration;)I

    move-result v0

    iput v0, p0, Lcom/konka/systemsetting/net/NetworkSoftApConfig;->mCurrentSec:I

    const-string v0, "NET_WIFI_AP ==>"

    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "mWifiApConfig.toString()="

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    iget-object v2, p0, Lcom/konka/systemsetting/net/NetworkSoftApConfig;->mWifiApConfig:Landroid/net/wifi/WifiConfiguration;

    invoke-virtual {v2}, Landroid/net/wifi/WifiConfiguration;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    :goto_0
    invoke-direct {p0}, Lcom/konka/systemsetting/net/NetworkSoftApConfig;->showWifiApInfo()V

    invoke-direct {p0}, Lcom/konka/systemsetting/net/NetworkSoftApConfig;->updateSecurityUI()V

    invoke-direct {p0}, Lcom/konka/systemsetting/net/NetworkSoftApConfig;->updateShowPwdUI()V

    iget-boolean v0, p0, Lcom/konka/systemsetting/net/NetworkSoftApConfig;->isWifiAPEnabled:Z

    invoke-direct {p0, v0}, Lcom/konka/systemsetting/net/NetworkSoftApConfig;->updateSwitchUI(Z)V

    return-void

    :cond_0
    const-string v0, "NET_WIFI_AP ==>"

    const-string v1, "mWifiApConfig is null"

    invoke-static {v0, v1}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_0
.end method

.method public onDestroy()V
    .locals 2

    iget-boolean v0, p0, Lcom/konka/systemsetting/net/NetworkSoftApConfig;->isUnregisted:Z

    if-nez v0, :cond_0

    iget-object v0, p0, Lcom/konka/systemsetting/net/NetworkSoftApConfig;->mContext:Lcom/konka/systemsetting/MainActivity;

    iget-object v1, p0, Lcom/konka/systemsetting/net/NetworkSoftApConfig;->receiver:Landroid/content/BroadcastReceiver;

    invoke-virtual {v0, v1}, Lcom/konka/systemsetting/MainActivity;->unregisterReceiver(Landroid/content/BroadcastReceiver;)V

    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/konka/systemsetting/net/NetworkSoftApConfig;->isUnregisted:Z

    :cond_0
    return-void
.end method

.method public onResume()V
    .locals 3

    iget-boolean v1, p0, Lcom/konka/systemsetting/net/NetworkSoftApConfig;->isUnregisted:Z

    if-eqz v1, :cond_0

    new-instance v0, Landroid/content/IntentFilter;

    const-string v1, "android.net.wifi.WIFI_AP_STATE_CHANGED"

    invoke-direct {v0, v1}, Landroid/content/IntentFilter;-><init>(Ljava/lang/String;)V

    iget-object v1, p0, Lcom/konka/systemsetting/net/NetworkSoftApConfig;->mContext:Lcom/konka/systemsetting/MainActivity;

    iget-object v2, p0, Lcom/konka/systemsetting/net/NetworkSoftApConfig;->receiver:Landroid/content/BroadcastReceiver;

    invoke-virtual {v1, v2, v0}, Lcom/konka/systemsetting/MainActivity;->registerReceiver(Landroid/content/BroadcastReceiver;Landroid/content/IntentFilter;)Landroid/content/Intent;

    const/4 v1, 0x0

    iput-boolean v1, p0, Lcom/konka/systemsetting/net/NetworkSoftApConfig;->isUnregisted:Z

    :cond_0
    invoke-virtual {p0}, Lcom/konka/systemsetting/net/NetworkSoftApConfig;->initWifiAP()V

    return-void
.end method

.method public saveWifiApConfig()V
    .locals 4

    const/4 v1, 0x2

    iget v2, p0, Lcom/konka/systemsetting/net/NetworkSoftApConfig;->mCurrentSec:I

    if-eq v1, v2, :cond_0

    iget-object v1, p0, Lcom/konka/systemsetting/net/NetworkSoftApConfig;->mHolder:Lcom/konka/systemsetting/net/NetworkSoftApViewHolder;

    iget-object v1, v1, Lcom/konka/systemsetting/net/NetworkSoftApViewHolder;->etPassword:Landroid/widget/EditText;

    invoke-virtual {v1}, Landroid/widget/EditText;->length()I

    move-result v1

    const/16 v2, 0x8

    if-ge v1, v2, :cond_0

    iget-object v1, p0, Lcom/konka/systemsetting/net/NetworkSoftApConfig;->mContext:Lcom/konka/systemsetting/MainActivity;

    const v2, 0x7f060100    # com.konka.systemsetting.R.string.softap_pwd_notice

    invoke-virtual {v1, v2}, Lcom/konka/systemsetting/MainActivity;->toastText(I)V

    :goto_0
    return-void

    :cond_0
    iget-object v1, p0, Lcom/konka/systemsetting/net/NetworkSoftApConfig;->mHolder:Lcom/konka/systemsetting/net/NetworkSoftApViewHolder;

    iget-object v1, v1, Lcom/konka/systemsetting/net/NetworkSoftApViewHolder;->etSSID:Landroid/widget/EditText;

    invoke-virtual {v1}, Landroid/widget/EditText;->length()I

    move-result v1

    if-nez v1, :cond_1

    iget-object v1, p0, Lcom/konka/systemsetting/net/NetworkSoftApConfig;->mContext:Lcom/konka/systemsetting/MainActivity;

    const v2, 0x7f0600ff    # com.konka.systemsetting.R.string.softap_ssid_empty

    invoke-virtual {v1, v2}, Lcom/konka/systemsetting/MainActivity;->toastText(I)V

    goto :goto_0

    :cond_1
    iget-object v1, p0, Lcom/konka/systemsetting/net/NetworkSoftApConfig;->mHolder:Lcom/konka/systemsetting/net/NetworkSoftApViewHolder;

    iget-object v1, v1, Lcom/konka/systemsetting/net/NetworkSoftApViewHolder;->etSSID:Landroid/widget/EditText;

    invoke-virtual {v1}, Landroid/widget/EditText;->length()I

    move-result v1

    const/16 v2, 0x20

    if-gt v1, v2, :cond_2

    iget-object v1, p0, Lcom/konka/systemsetting/net/NetworkSoftApConfig;->mHolder:Lcom/konka/systemsetting/net/NetworkSoftApViewHolder;

    iget-object v1, v1, Lcom/konka/systemsetting/net/NetworkSoftApViewHolder;->etPassword:Landroid/widget/EditText;

    invoke-virtual {v1}, Landroid/widget/EditText;->length()I

    move-result v1

    const/16 v2, 0x14

    if-le v1, v2, :cond_3

    :cond_2
    iget-object v1, p0, Lcom/konka/systemsetting/net/NetworkSoftApConfig;->mContext:Lcom/konka/systemsetting/MainActivity;

    const v2, 0x7f060101    # com.konka.systemsetting.R.string.softap_ssid_pwd_too_long

    invoke-virtual {v1, v2}, Lcom/konka/systemsetting/MainActivity;->toastText(I)V

    goto :goto_0

    :cond_3
    invoke-virtual {p0}, Lcom/konka/systemsetting/net/NetworkSoftApConfig;->getApConfig()Landroid/net/wifi/WifiConfiguration;

    move-result-object v0

    if-eqz v0, :cond_5

    iget-object v1, p0, Lcom/konka/systemsetting/net/NetworkSoftApConfig;->mWifiMgr:Landroid/net/wifi/WifiManager;

    invoke-virtual {v1}, Landroid/net/wifi/WifiManager;->getWifiApState()I

    move-result v1

    const/16 v2, 0xd

    if-ne v1, v2, :cond_4

    iget-object v1, p0, Lcom/konka/systemsetting/net/NetworkSoftApConfig;->mWifiMgr:Landroid/net/wifi/WifiManager;

    const/4 v2, 0x0

    const/4 v3, 0x0

    invoke-virtual {v1, v2, v3}, Landroid/net/wifi/WifiManager;->setWifiApEnabled(Landroid/net/wifi/WifiConfiguration;Z)Z

    iget-object v1, p0, Lcom/konka/systemsetting/net/NetworkSoftApConfig;->mWifiMgr:Landroid/net/wifi/WifiManager;

    const/4 v2, 0x1

    invoke-virtual {v1, v0, v2}, Landroid/net/wifi/WifiManager;->setWifiApEnabled(Landroid/net/wifi/WifiConfiguration;Z)Z

    :goto_1
    const-string v1, "NET_WIFI_AP ==>"

    const-string v2, "saveWifiApConfig"

    invoke-static {v1, v2}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    iget-object v1, p0, Lcom/konka/systemsetting/net/NetworkSoftApConfig;->mContext:Lcom/konka/systemsetting/MainActivity;

    const v2, 0x7f060102    # com.konka.systemsetting.R.string.softap_config_success

    invoke-virtual {v1, v2}, Lcom/konka/systemsetting/MainActivity;->toastText(I)V

    goto :goto_0

    :cond_4
    iget-object v1, p0, Lcom/konka/systemsetting/net/NetworkSoftApConfig;->mWifiMgr:Landroid/net/wifi/WifiManager;

    invoke-virtual {v1, v0}, Landroid/net/wifi/WifiManager;->setWifiApConfiguration(Landroid/net/wifi/WifiConfiguration;)Z

    goto :goto_1

    :cond_5
    const-string v1, "NET_WIFI_AP ==>"

    const-string v2, "getApConfig == null;"

    invoke-static {v1, v2}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_0
.end method
