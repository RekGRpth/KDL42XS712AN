.class Lcom/konka/systemsetting/net/WpsDialog$4;
.super Ljava/lang/Object;
.source "WpsDialog.java"

# interfaces
.implements Ljava/lang/Runnable;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/konka/systemsetting/net/WpsDialog;->updateDialog(Lcom/konka/systemsetting/net/WpsDialog$DialogState;Ljava/lang/String;)V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# static fields
.field private static synthetic $SWITCH_TABLE$com$konka$systemsetting$net$WpsDialog$DialogState:[I


# instance fields
.field final synthetic this$0:Lcom/konka/systemsetting/net/WpsDialog;

.field private final synthetic val$msg:Ljava/lang/String;

.field private final synthetic val$state:Lcom/konka/systemsetting/net/WpsDialog$DialogState;


# direct methods
.method static synthetic $SWITCH_TABLE$com$konka$systemsetting$net$WpsDialog$DialogState()[I
    .locals 3

    sget-object v0, Lcom/konka/systemsetting/net/WpsDialog$4;->$SWITCH_TABLE$com$konka$systemsetting$net$WpsDialog$DialogState:[I

    if-eqz v0, :cond_0

    :goto_0
    return-object v0

    :cond_0
    invoke-static {}, Lcom/konka/systemsetting/net/WpsDialog$DialogState;->values()[Lcom/konka/systemsetting/net/WpsDialog$DialogState;

    move-result-object v0

    array-length v0, v0

    new-array v0, v0, [I

    :try_start_0
    sget-object v1, Lcom/konka/systemsetting/net/WpsDialog$DialogState;->CONNECTED:Lcom/konka/systemsetting/net/WpsDialog$DialogState;

    invoke-virtual {v1}, Lcom/konka/systemsetting/net/WpsDialog$DialogState;->ordinal()I

    move-result v1

    const/4 v2, 0x4

    aput v2, v0, v1
    :try_end_0
    .catch Ljava/lang/NoSuchFieldError; {:try_start_0 .. :try_end_0} :catch_4

    :goto_1
    :try_start_1
    sget-object v1, Lcom/konka/systemsetting/net/WpsDialog$DialogState;->WPS_COMPLETE:Lcom/konka/systemsetting/net/WpsDialog$DialogState;

    invoke-virtual {v1}, Lcom/konka/systemsetting/net/WpsDialog$DialogState;->ordinal()I

    move-result v1

    const/4 v2, 0x3

    aput v2, v0, v1
    :try_end_1
    .catch Ljava/lang/NoSuchFieldError; {:try_start_1 .. :try_end_1} :catch_3

    :goto_2
    :try_start_2
    sget-object v1, Lcom/konka/systemsetting/net/WpsDialog$DialogState;->WPS_FAILED:Lcom/konka/systemsetting/net/WpsDialog$DialogState;

    invoke-virtual {v1}, Lcom/konka/systemsetting/net/WpsDialog$DialogState;->ordinal()I

    move-result v1

    const/4 v2, 0x5

    aput v2, v0, v1
    :try_end_2
    .catch Ljava/lang/NoSuchFieldError; {:try_start_2 .. :try_end_2} :catch_2

    :goto_3
    :try_start_3
    sget-object v1, Lcom/konka/systemsetting/net/WpsDialog$DialogState;->WPS_INIT:Lcom/konka/systemsetting/net/WpsDialog$DialogState;

    invoke-virtual {v1}, Lcom/konka/systemsetting/net/WpsDialog$DialogState;->ordinal()I

    move-result v1

    const/4 v2, 0x1

    aput v2, v0, v1
    :try_end_3
    .catch Ljava/lang/NoSuchFieldError; {:try_start_3 .. :try_end_3} :catch_1

    :goto_4
    :try_start_4
    sget-object v1, Lcom/konka/systemsetting/net/WpsDialog$DialogState;->WPS_START:Lcom/konka/systemsetting/net/WpsDialog$DialogState;

    invoke-virtual {v1}, Lcom/konka/systemsetting/net/WpsDialog$DialogState;->ordinal()I

    move-result v1

    const/4 v2, 0x2

    aput v2, v0, v1
    :try_end_4
    .catch Ljava/lang/NoSuchFieldError; {:try_start_4 .. :try_end_4} :catch_0

    :goto_5
    sput-object v0, Lcom/konka/systemsetting/net/WpsDialog$4;->$SWITCH_TABLE$com$konka$systemsetting$net$WpsDialog$DialogState:[I

    goto :goto_0

    :catch_0
    move-exception v1

    goto :goto_5

    :catch_1
    move-exception v1

    goto :goto_4

    :catch_2
    move-exception v1

    goto :goto_3

    :catch_3
    move-exception v1

    goto :goto_2

    :catch_4
    move-exception v1

    goto :goto_1
.end method

.method constructor <init>(Lcom/konka/systemsetting/net/WpsDialog;Lcom/konka/systemsetting/net/WpsDialog$DialogState;Ljava/lang/String;)V
    .locals 0

    iput-object p1, p0, Lcom/konka/systemsetting/net/WpsDialog$4;->this$0:Lcom/konka/systemsetting/net/WpsDialog;

    iput-object p2, p0, Lcom/konka/systemsetting/net/WpsDialog$4;->val$state:Lcom/konka/systemsetting/net/WpsDialog$DialogState;

    iput-object p3, p0, Lcom/konka/systemsetting/net/WpsDialog$4;->val$msg:Ljava/lang/String;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public run()V
    .locals 4

    const/16 v3, 0x8

    invoke-static {}, Lcom/konka/systemsetting/net/WpsDialog$4;->$SWITCH_TABLE$com$konka$systemsetting$net$WpsDialog$DialogState()[I

    move-result-object v0

    iget-object v1, p0, Lcom/konka/systemsetting/net/WpsDialog$4;->val$state:Lcom/konka/systemsetting/net/WpsDialog$DialogState;

    invoke-virtual {v1}, Lcom/konka/systemsetting/net/WpsDialog$DialogState;->ordinal()I

    move-result v1

    aget v0, v0, v1

    packed-switch v0, :pswitch_data_0

    :cond_0
    :goto_0
    iget-object v0, p0, Lcom/konka/systemsetting/net/WpsDialog$4;->this$0:Lcom/konka/systemsetting/net/WpsDialog;

    # getter for: Lcom/konka/systemsetting/net/WpsDialog;->mTextView:Landroid/widget/TextView;
    invoke-static {v0}, Lcom/konka/systemsetting/net/WpsDialog;->access$9(Lcom/konka/systemsetting/net/WpsDialog;)Landroid/widget/TextView;

    move-result-object v0

    iget-object v1, p0, Lcom/konka/systemsetting/net/WpsDialog$4;->val$msg:Ljava/lang/String;

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    return-void

    :pswitch_0
    iget-object v0, p0, Lcom/konka/systemsetting/net/WpsDialog$4;->this$0:Lcom/konka/systemsetting/net/WpsDialog;

    # getter for: Lcom/konka/systemsetting/net/WpsDialog;->mTimeoutBar:Landroid/widget/ProgressBar;
    invoke-static {v0}, Lcom/konka/systemsetting/net/WpsDialog;->access$4(Lcom/konka/systemsetting/net/WpsDialog;)Landroid/widget/ProgressBar;

    move-result-object v0

    invoke-virtual {v0, v3}, Landroid/widget/ProgressBar;->setVisibility(I)V

    iget-object v0, p0, Lcom/konka/systemsetting/net/WpsDialog$4;->this$0:Lcom/konka/systemsetting/net/WpsDialog;

    # getter for: Lcom/konka/systemsetting/net/WpsDialog;->mProgressBar:Landroid/widget/ProgressBar;
    invoke-static {v0}, Lcom/konka/systemsetting/net/WpsDialog;->access$5(Lcom/konka/systemsetting/net/WpsDialog;)Landroid/widget/ProgressBar;

    move-result-object v0

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Landroid/widget/ProgressBar;->setVisibility(I)V

    goto :goto_0

    :pswitch_1
    iget-object v0, p0, Lcom/konka/systemsetting/net/WpsDialog$4;->this$0:Lcom/konka/systemsetting/net/WpsDialog;

    # getter for: Lcom/konka/systemsetting/net/WpsDialog;->mButton:Landroid/widget/Button;
    invoke-static {v0}, Lcom/konka/systemsetting/net/WpsDialog;->access$6(Lcom/konka/systemsetting/net/WpsDialog;)Landroid/widget/Button;

    move-result-object v0

    iget-object v1, p0, Lcom/konka/systemsetting/net/WpsDialog$4;->this$0:Lcom/konka/systemsetting/net/WpsDialog;

    # getter for: Lcom/konka/systemsetting/net/WpsDialog;->mContext:Landroid/content/Context;
    invoke-static {v1}, Lcom/konka/systemsetting/net/WpsDialog;->access$0(Lcom/konka/systemsetting/net/WpsDialog;)Landroid/content/Context;

    move-result-object v1

    const v2, 0x7f060154    # com.konka.systemsetting.R.string.wps_ok

    invoke-virtual {v1, v2}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/widget/Button;->setText(Ljava/lang/CharSequence;)V

    iget-object v0, p0, Lcom/konka/systemsetting/net/WpsDialog$4;->this$0:Lcom/konka/systemsetting/net/WpsDialog;

    # getter for: Lcom/konka/systemsetting/net/WpsDialog;->mTimeoutBar:Landroid/widget/ProgressBar;
    invoke-static {v0}, Lcom/konka/systemsetting/net/WpsDialog;->access$4(Lcom/konka/systemsetting/net/WpsDialog;)Landroid/widget/ProgressBar;

    move-result-object v0

    invoke-virtual {v0, v3}, Landroid/widget/ProgressBar;->setVisibility(I)V

    iget-object v0, p0, Lcom/konka/systemsetting/net/WpsDialog$4;->this$0:Lcom/konka/systemsetting/net/WpsDialog;

    # getter for: Lcom/konka/systemsetting/net/WpsDialog;->mProgressBar:Landroid/widget/ProgressBar;
    invoke-static {v0}, Lcom/konka/systemsetting/net/WpsDialog;->access$5(Lcom/konka/systemsetting/net/WpsDialog;)Landroid/widget/ProgressBar;

    move-result-object v0

    invoke-virtual {v0, v3}, Landroid/widget/ProgressBar;->setVisibility(I)V

    iget-object v0, p0, Lcom/konka/systemsetting/net/WpsDialog$4;->this$0:Lcom/konka/systemsetting/net/WpsDialog;

    # getter for: Lcom/konka/systemsetting/net/WpsDialog;->mReceiver:Landroid/content/BroadcastReceiver;
    invoke-static {v0}, Lcom/konka/systemsetting/net/WpsDialog;->access$7(Lcom/konka/systemsetting/net/WpsDialog;)Landroid/content/BroadcastReceiver;

    move-result-object v0

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/konka/systemsetting/net/WpsDialog$4;->this$0:Lcom/konka/systemsetting/net/WpsDialog;

    # getter for: Lcom/konka/systemsetting/net/WpsDialog;->mContext:Landroid/content/Context;
    invoke-static {v0}, Lcom/konka/systemsetting/net/WpsDialog;->access$0(Lcom/konka/systemsetting/net/WpsDialog;)Landroid/content/Context;

    move-result-object v0

    iget-object v1, p0, Lcom/konka/systemsetting/net/WpsDialog$4;->this$0:Lcom/konka/systemsetting/net/WpsDialog;

    # getter for: Lcom/konka/systemsetting/net/WpsDialog;->mReceiver:Landroid/content/BroadcastReceiver;
    invoke-static {v1}, Lcom/konka/systemsetting/net/WpsDialog;->access$7(Lcom/konka/systemsetting/net/WpsDialog;)Landroid/content/BroadcastReceiver;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/content/Context;->unregisterReceiver(Landroid/content/BroadcastReceiver;)V

    iget-object v0, p0, Lcom/konka/systemsetting/net/WpsDialog$4;->this$0:Lcom/konka/systemsetting/net/WpsDialog;

    const/4 v1, 0x0

    invoke-static {v0, v1}, Lcom/konka/systemsetting/net/WpsDialog;->access$8(Lcom/konka/systemsetting/net/WpsDialog;Landroid/content/BroadcastReceiver;)V

    goto :goto_0

    nop

    :pswitch_data_0
    .packed-switch 0x3
        :pswitch_0
        :pswitch_1
        :pswitch_1
    .end packed-switch
.end method
