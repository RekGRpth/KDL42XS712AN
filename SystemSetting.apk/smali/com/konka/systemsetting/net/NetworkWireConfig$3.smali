.class Lcom/konka/systemsetting/net/NetworkWireConfig$3;
.super Ljava/lang/Object;
.source "NetworkWireConfig.java"

# interfaces
.implements Landroid/view/View$OnFocusChangeListener;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/konka/systemsetting/net/NetworkWireConfig;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/konka/systemsetting/net/NetworkWireConfig;


# direct methods
.method constructor <init>(Lcom/konka/systemsetting/net/NetworkWireConfig;)V
    .locals 0

    iput-object p1, p0, Lcom/konka/systemsetting/net/NetworkWireConfig$3;->this$0:Lcom/konka/systemsetting/net/NetworkWireConfig;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onFocusChange(Landroid/view/View;Z)V
    .locals 1
    .param p1    # Landroid/view/View;
    .param p2    # Z

    invoke-virtual {p1}, Landroid/view/View;->getId()I

    move-result v0

    packed-switch v0, :pswitch_data_0

    :goto_0
    :pswitch_0
    return-void

    :pswitch_1
    iget-object v0, p0, Lcom/konka/systemsetting/net/NetworkWireConfig$3;->this$0:Lcom/konka/systemsetting/net/NetworkWireConfig;

    iget-object v0, v0, Lcom/konka/systemsetting/net/NetworkWireConfig;->mEditIpAddr:Landroid/widget/EditText;

    invoke-virtual {v0}, Landroid/widget/EditText;->requestFocus()Z

    goto :goto_0

    :pswitch_2
    iget-object v0, p0, Lcom/konka/systemsetting/net/NetworkWireConfig$3;->this$0:Lcom/konka/systemsetting/net/NetworkWireConfig;

    iget-object v0, v0, Lcom/konka/systemsetting/net/NetworkWireConfig;->mEditNetmask:Landroid/widget/EditText;

    invoke-virtual {v0}, Landroid/widget/EditText;->requestFocus()Z

    goto :goto_0

    :pswitch_3
    iget-object v0, p0, Lcom/konka/systemsetting/net/NetworkWireConfig$3;->this$0:Lcom/konka/systemsetting/net/NetworkWireConfig;

    iget-object v0, v0, Lcom/konka/systemsetting/net/NetworkWireConfig;->mEditGateway:Landroid/widget/EditText;

    invoke-virtual {v0}, Landroid/widget/EditText;->requestFocus()Z

    goto :goto_0

    :pswitch_4
    iget-object v0, p0, Lcom/konka/systemsetting/net/NetworkWireConfig$3;->this$0:Lcom/konka/systemsetting/net/NetworkWireConfig;

    iget-object v0, v0, Lcom/konka/systemsetting/net/NetworkWireConfig;->mEditDnsSvr:Landroid/widget/EditText;

    invoke-virtual {v0}, Landroid/widget/EditText;->requestFocus()Z

    goto :goto_0

    :pswitch_data_0
    .packed-switch 0x7f090064
        :pswitch_1    # com.konka.systemsetting.R.id.sys_network_wired_item_ipaddr
        :pswitch_0    # com.konka.systemsetting.R.id.sys_network_wired_edittext_ipaddr
        :pswitch_2    # com.konka.systemsetting.R.id.sys_network_wired_item_subnetmask
        :pswitch_0    # com.konka.systemsetting.R.id.sys_network_wired_edittext_subnetmask
        :pswitch_3    # com.konka.systemsetting.R.id.sys_network_wired_item_defaultgateway
        :pswitch_0    # com.konka.systemsetting.R.id.sys_network_wired_edittext_defaultgateway
        :pswitch_4    # com.konka.systemsetting.R.id.sys_network_wired_item_dnsserver
    .end packed-switch
.end method
