.class public Lcom/konka/systemsetting/net/NetworkDetector;
.super Ljava/lang/Object;
.source "NetworkDetector.java"

# interfaces
.implements Ljava/lang/Runnable;


# static fields
.field protected static NetworkIsLinked:Z


# instance fields
.field private htmlCodeSize:I

.field protected isSpontaneousNotice:Z

.field protected sleepMillisecond:I

.field protected sleepMillisecondWhenNetworkUnLinked:I

.field private urls:[Ljava/lang/String;


# direct methods
.method public constructor <init>()V
    .locals 3

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    const/4 v0, 0x2

    new-array v0, v0, [Ljava/lang/String;

    const/4 v1, 0x0

    const-string v2, "http://www.baidu.com"

    aput-object v2, v0, v1

    const/4 v1, 0x1

    const-string v2, "http://www.google.cn"

    aput-object v2, v0, v1

    iput-object v0, p0, Lcom/konka/systemsetting/net/NetworkDetector;->urls:[Ljava/lang/String;

    const/16 v0, 0x32

    iput v0, p0, Lcom/konka/systemsetting/net/NetworkDetector;->htmlCodeSize:I

    const/16 v0, 0x1388

    iput v0, p0, Lcom/konka/systemsetting/net/NetworkDetector;->sleepMillisecond:I

    const/16 v0, 0x2710

    iput v0, p0, Lcom/konka/systemsetting/net/NetworkDetector;->sleepMillisecondWhenNetworkUnLinked:I

    return-void
.end method

.method public static IsNetworkLinking()Z
    .locals 1

    sget-boolean v0, Lcom/konka/systemsetting/net/NetworkDetector;->NetworkIsLinked:Z

    return v0
.end method

.method private canGetHtmlCode(Ljava/lang/String;)Z
    .locals 8
    .param p1    # Ljava/lang/String;

    const-string v2, ""

    :try_start_0
    new-instance v5, Ljava/net/URL;

    invoke-direct {v5, p1}, Ljava/net/URL;-><init>(Ljava/lang/String;)V

    invoke-virtual {v5}, Ljava/net/URL;->openConnection()Ljava/net/URLConnection;

    move-result-object v1

    check-cast v1, Ljava/net/HttpURLConnection;

    invoke-virtual {v5}, Ljava/net/URL;->openConnection()Ljava/net/URLConnection;

    move-result-object v1

    check-cast v1, Ljava/net/HttpURLConnection;

    const-string v6, "User-Agent"

    const-string v7, "Mozilla/4.0"

    invoke-virtual {v1, v6, v7}, Ljava/net/HttpURLConnection;->setRequestProperty(Ljava/lang/String;Ljava/lang/String;)V

    invoke-virtual {v1}, Ljava/net/HttpURLConnection;->connect()V

    invoke-virtual {v1}, Ljava/net/HttpURLConnection;->getInputStream()Ljava/io/InputStream;

    move-result-object v4

    iget v6, p0, Lcom/konka/systemsetting/net/NetworkDetector;->htmlCodeSize:I

    new-array v0, v6, [B

    invoke-virtual {v4, v0}, Ljava/io/InputStream;->read([B)I

    new-instance v3, Ljava/lang/String;

    invoke-direct {v3, v0}, Ljava/lang/String;-><init>([B)V
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    move-object v2, v3

    :goto_0
    if-eqz v2, :cond_0

    const-string v6, ""

    invoke-virtual {v2, v6}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v6

    if-eqz v6, :cond_1

    :cond_0
    const/4 v6, 0x0

    :goto_1
    return v6

    :cond_1
    const/4 v6, 0x1

    goto :goto_1

    :catch_0
    move-exception v6

    goto :goto_0
.end method


# virtual methods
.method protected isNetworkLinked()V
    .locals 3

    const/4 v0, 0x0

    const/4 v1, 0x0

    :goto_0
    iget-object v2, p0, Lcom/konka/systemsetting/net/NetworkDetector;->urls:[Ljava/lang/String;

    array-length v2, v2

    if-lt v1, v2, :cond_0

    :goto_1
    sput-boolean v0, Lcom/konka/systemsetting/net/NetworkDetector;->NetworkIsLinked:Z

    return-void

    :cond_0
    iget-object v2, p0, Lcom/konka/systemsetting/net/NetworkDetector;->urls:[Ljava/lang/String;

    aget-object v2, v2, v1

    invoke-direct {p0, v2}, Lcom/konka/systemsetting/net/NetworkDetector;->canGetHtmlCode(Ljava/lang/String;)Z

    move-result v2

    if-eqz v2, :cond_1

    const/4 v0, 0x1

    goto :goto_1

    :cond_1
    add-int/lit8 v1, v1, 0x1

    goto :goto_0
.end method

.method protected isPrintMessage(Z)V
    .locals 3
    .param p1    # Z

    if-eqz p1, :cond_0

    new-instance v0, Ljava/lang/StringBuffer;

    invoke-direct {v0}, Ljava/lang/StringBuffer;-><init>()V

    const-string v1, "------------->"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    const-string v1, "\u7f51\u7edc\u4e2d\u65ad, "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    iget v1, p0, Lcom/konka/systemsetting/net/NetworkDetector;->sleepMillisecondWhenNetworkUnLinked:I

    invoke-virtual {v0, v1}, Ljava/lang/StringBuffer;->append(I)Ljava/lang/StringBuffer;

    const-string v1, " \u6beb\u79d2\u540e\u518d\u6b21\u68c0\u6d4b!<-------------"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    sget-object v1, Ljava/lang/System;->out:Ljava/io/PrintStream;

    invoke-virtual {v0}, Ljava/lang/StringBuffer;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/io/PrintStream;->println(Ljava/lang/String;)V

    :cond_0
    return-void
.end method

.method public isSpontaneousNotice(Z)V
    .locals 0
    .param p1    # Z

    iput-boolean p1, p0, Lcom/konka/systemsetting/net/NetworkDetector;->isSpontaneousNotice:Z

    return-void
.end method

.method public run()V
    .locals 2

    :goto_0
    :try_start_0
    invoke-virtual {p0}, Lcom/konka/systemsetting/net/NetworkDetector;->isNetworkLinked()V

    sget-boolean v0, Lcom/konka/systemsetting/net/NetworkDetector;->NetworkIsLinked:Z

    if-nez v0, :cond_0

    iget-boolean v0, p0, Lcom/konka/systemsetting/net/NetworkDetector;->isSpontaneousNotice:Z

    invoke-virtual {p0, v0}, Lcom/konka/systemsetting/net/NetworkDetector;->isPrintMessage(Z)V

    iget v0, p0, Lcom/konka/systemsetting/net/NetworkDetector;->sleepMillisecondWhenNetworkUnLinked:I

    int-to-long v0, v0

    invoke-static {v0, v1}, Ljava/lang/Thread;->sleep(J)V

    :cond_0
    iget-boolean v0, p0, Lcom/konka/systemsetting/net/NetworkDetector;->isSpontaneousNotice:Z

    if-eqz v0, :cond_1

    sget-object v0, Ljava/lang/System;->out:Ljava/io/PrintStream;

    invoke-static {}, Lcom/konka/systemsetting/net/NetworkDetector;->IsNetworkLinking()Z

    move-result v1

    invoke-virtual {v0, v1}, Ljava/io/PrintStream;->println(Z)V

    :cond_1
    iget v0, p0, Lcom/konka/systemsetting/net/NetworkDetector;->sleepMillisecond:I

    int-to-long v0, v0

    invoke-static {v0, v1}, Ljava/lang/Thread;->sleep(J)V
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    goto :goto_0

    :catch_0
    move-exception v0

    goto :goto_0
.end method

.method public setHtmlCodeSize(I)V
    .locals 0
    .param p1    # I

    if-lez p1, :cond_0

    iput p1, p0, Lcom/konka/systemsetting/net/NetworkDetector;->htmlCodeSize:I

    :cond_0
    return-void
.end method

.method public setSleepMillisecondWhenNetworkUnLinked(I)V
    .locals 1
    .param p1    # I

    const/16 v0, 0x64

    if-le p1, v0, :cond_0

    iput p1, p0, Lcom/konka/systemsetting/net/NetworkDetector;->sleepMillisecondWhenNetworkUnLinked:I

    :cond_0
    return-void
.end method

.method public setSleepMillisecont(I)V
    .locals 1
    .param p1    # I

    const/16 v0, 0x64

    if-le p1, v0, :cond_0

    iput p1, p0, Lcom/konka/systemsetting/net/NetworkDetector;->sleepMillisecond:I

    :cond_0
    return-void
.end method

.method public setURLs([Ljava/lang/String;)V
    .locals 1
    .param p1    # [Ljava/lang/String;

    if-eqz p1, :cond_0

    array-length v0, p1

    if-lez v0, :cond_0

    iput-object p1, p0, Lcom/konka/systemsetting/net/NetworkDetector;->urls:[Ljava/lang/String;

    :cond_0
    return-void
.end method
