.class Lcom/konka/systemsetting/net/WifiDisplaySettings$2;
.super Landroid/content/BroadcastReceiver;
.source "WifiDisplaySettings.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/konka/systemsetting/net/WifiDisplaySettings;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/konka/systemsetting/net/WifiDisplaySettings;


# direct methods
.method constructor <init>(Lcom/konka/systemsetting/net/WifiDisplaySettings;)V
    .locals 0

    iput-object p1, p0, Lcom/konka/systemsetting/net/WifiDisplaySettings$2;->this$0:Lcom/konka/systemsetting/net/WifiDisplaySettings;

    invoke-direct {p0}, Landroid/content/BroadcastReceiver;-><init>()V

    return-void
.end method


# virtual methods
.method public onReceive(Landroid/content/Context;Landroid/content/Intent;)V
    .locals 3
    .param p1    # Landroid/content/Context;
    .param p2    # Landroid/content/Intent;

    invoke-virtual {p2}, Landroid/content/Intent;->getAction()Ljava/lang/String;

    move-result-object v0

    const-string v2, "android.hardware.display.action.WIFI_DISPLAY_STATUS_CHANGED"

    invoke-virtual {v0, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_0

    const-string v2, "android.hardware.display.extra.WIFI_DISPLAY_STATUS"

    invoke-virtual {p2, v2}, Landroid/content/Intent;->getParcelableExtra(Ljava/lang/String;)Landroid/os/Parcelable;

    move-result-object v1

    check-cast v1, Landroid/hardware/display/WifiDisplayStatus;

    iget-object v2, p0, Lcom/konka/systemsetting/net/WifiDisplaySettings$2;->this$0:Lcom/konka/systemsetting/net/WifiDisplaySettings;

    invoke-static {v2, v1}, Lcom/konka/systemsetting/net/WifiDisplaySettings;->access$2(Lcom/konka/systemsetting/net/WifiDisplaySettings;Landroid/hardware/display/WifiDisplayStatus;)V

    iget-object v2, p0, Lcom/konka/systemsetting/net/WifiDisplaySettings$2;->this$0:Lcom/konka/systemsetting/net/WifiDisplaySettings;

    # invokes: Lcom/konka/systemsetting/net/WifiDisplaySettings;->applyState()V
    invoke-static {v2}, Lcom/konka/systemsetting/net/WifiDisplaySettings;->access$3(Lcom/konka/systemsetting/net/WifiDisplaySettings;)V

    :cond_0
    return-void
.end method
