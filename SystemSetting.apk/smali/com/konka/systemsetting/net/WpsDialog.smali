.class public Lcom/konka/systemsetting/net/WpsDialog;
.super Landroid/app/Dialog;
.source "WpsDialog.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/konka/systemsetting/net/WpsDialog$DialogState;
    }
.end annotation


# static fields
.field private static final TAG:Ljava/lang/String; = "WpsDialog"

.field private static final WPS_TIMEOUT_S:I = 0x78


# instance fields
.field private mButton:Landroid/widget/Button;

.field private mContext:Landroid/content/Context;

.field mDialogState:Lcom/konka/systemsetting/net/WpsDialog$DialogState;

.field private final mFilter:Landroid/content/IntentFilter;

.field private mHandler:Landroid/os/Handler;

.field private mProgressBar:Landroid/widget/ProgressBar;

.field private mReceiver:Landroid/content/BroadcastReceiver;

.field private mTextView:Landroid/widget/TextView;

.field private mTimeoutBar:Landroid/widget/ProgressBar;

.field private mTimer:Ljava/util/Timer;

.field private mView:Landroid/view/View;

.field private mWifiManager:Landroid/net/wifi/WifiManager;

.field private mWpsListener:Landroid/net/wifi/WifiManager$WpsListener;

.field private mWpsSetup:I


# direct methods
.method public constructor <init>(Landroid/content/Context;I)V
    .locals 2
    .param p1    # Landroid/content/Context;
    .param p2    # I

    const v0, 0x7f080006    # com.konka.systemsetting.R.style.NobackDialog

    invoke-direct {p0, p1, v0}, Landroid/app/Dialog;-><init>(Landroid/content/Context;I)V

    new-instance v0, Landroid/os/Handler;

    invoke-direct {v0}, Landroid/os/Handler;-><init>()V

    iput-object v0, p0, Lcom/konka/systemsetting/net/WpsDialog;->mHandler:Landroid/os/Handler;

    sget-object v0, Lcom/konka/systemsetting/net/WpsDialog$DialogState;->WPS_INIT:Lcom/konka/systemsetting/net/WpsDialog$DialogState;

    iput-object v0, p0, Lcom/konka/systemsetting/net/WpsDialog;->mDialogState:Lcom/konka/systemsetting/net/WpsDialog$DialogState;

    iput-object p1, p0, Lcom/konka/systemsetting/net/WpsDialog;->mContext:Landroid/content/Context;

    iput p2, p0, Lcom/konka/systemsetting/net/WpsDialog;->mWpsSetup:I

    new-instance v0, Lcom/konka/systemsetting/net/WpsDialog$1WpsListener;

    invoke-direct {v0, p0}, Lcom/konka/systemsetting/net/WpsDialog$1WpsListener;-><init>(Lcom/konka/systemsetting/net/WpsDialog;)V

    iput-object v0, p0, Lcom/konka/systemsetting/net/WpsDialog;->mWpsListener:Landroid/net/wifi/WifiManager$WpsListener;

    new-instance v0, Landroid/content/IntentFilter;

    invoke-direct {v0}, Landroid/content/IntentFilter;-><init>()V

    iput-object v0, p0, Lcom/konka/systemsetting/net/WpsDialog;->mFilter:Landroid/content/IntentFilter;

    iget-object v0, p0, Lcom/konka/systemsetting/net/WpsDialog;->mFilter:Landroid/content/IntentFilter;

    const-string v1, "android.net.wifi.STATE_CHANGE"

    invoke-virtual {v0, v1}, Landroid/content/IntentFilter;->addAction(Ljava/lang/String;)V

    new-instance v0, Lcom/konka/systemsetting/net/WpsDialog$1;

    invoke-direct {v0, p0}, Lcom/konka/systemsetting/net/WpsDialog$1;-><init>(Lcom/konka/systemsetting/net/WpsDialog;)V

    iput-object v0, p0, Lcom/konka/systemsetting/net/WpsDialog;->mReceiver:Landroid/content/BroadcastReceiver;

    return-void
.end method

.method static synthetic access$0(Lcom/konka/systemsetting/net/WpsDialog;)Landroid/content/Context;
    .locals 1

    iget-object v0, p0, Lcom/konka/systemsetting/net/WpsDialog;->mContext:Landroid/content/Context;

    return-object v0
.end method

.method static synthetic access$1(Lcom/konka/systemsetting/net/WpsDialog;Lcom/konka/systemsetting/net/WpsDialog$DialogState;Ljava/lang/String;)V
    .locals 0

    invoke-direct {p0, p1, p2}, Lcom/konka/systemsetting/net/WpsDialog;->updateDialog(Lcom/konka/systemsetting/net/WpsDialog$DialogState;Ljava/lang/String;)V

    return-void
.end method

.method static synthetic access$2(Lcom/konka/systemsetting/net/WpsDialog;Landroid/content/Context;Landroid/content/Intent;)V
    .locals 0

    invoke-direct {p0, p1, p2}, Lcom/konka/systemsetting/net/WpsDialog;->handleEvent(Landroid/content/Context;Landroid/content/Intent;)V

    return-void
.end method

.method static synthetic access$3(Lcom/konka/systemsetting/net/WpsDialog;)Landroid/os/Handler;
    .locals 1

    iget-object v0, p0, Lcom/konka/systemsetting/net/WpsDialog;->mHandler:Landroid/os/Handler;

    return-object v0
.end method

.method static synthetic access$4(Lcom/konka/systemsetting/net/WpsDialog;)Landroid/widget/ProgressBar;
    .locals 1

    iget-object v0, p0, Lcom/konka/systemsetting/net/WpsDialog;->mTimeoutBar:Landroid/widget/ProgressBar;

    return-object v0
.end method

.method static synthetic access$5(Lcom/konka/systemsetting/net/WpsDialog;)Landroid/widget/ProgressBar;
    .locals 1

    iget-object v0, p0, Lcom/konka/systemsetting/net/WpsDialog;->mProgressBar:Landroid/widget/ProgressBar;

    return-object v0
.end method

.method static synthetic access$6(Lcom/konka/systemsetting/net/WpsDialog;)Landroid/widget/Button;
    .locals 1

    iget-object v0, p0, Lcom/konka/systemsetting/net/WpsDialog;->mButton:Landroid/widget/Button;

    return-object v0
.end method

.method static synthetic access$7(Lcom/konka/systemsetting/net/WpsDialog;)Landroid/content/BroadcastReceiver;
    .locals 1

    iget-object v0, p0, Lcom/konka/systemsetting/net/WpsDialog;->mReceiver:Landroid/content/BroadcastReceiver;

    return-object v0
.end method

.method static synthetic access$8(Lcom/konka/systemsetting/net/WpsDialog;Landroid/content/BroadcastReceiver;)V
    .locals 0

    iput-object p1, p0, Lcom/konka/systemsetting/net/WpsDialog;->mReceiver:Landroid/content/BroadcastReceiver;

    return-void
.end method

.method static synthetic access$9(Lcom/konka/systemsetting/net/WpsDialog;)Landroid/widget/TextView;
    .locals 1

    iget-object v0, p0, Lcom/konka/systemsetting/net/WpsDialog;->mTextView:Landroid/widget/TextView;

    return-object v0
.end method

.method private handleEvent(Landroid/content/Context;Landroid/content/Intent;)V
    .locals 9
    .param p1    # Landroid/content/Context;
    .param p2    # Landroid/content/Intent;

    invoke-virtual {p2}, Landroid/content/Intent;->getAction()Ljava/lang/String;

    move-result-object v0

    const-string v5, "android.net.wifi.STATE_CHANGE"

    invoke-virtual {v5, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v5

    if-eqz v5, :cond_0

    const-string v5, "networkInfo"

    invoke-virtual {p2, v5}, Landroid/content/Intent;->getParcelableExtra(Ljava/lang/String;)Landroid/os/Parcelable;

    move-result-object v1

    check-cast v1, Landroid/net/NetworkInfo;

    invoke-virtual {v1}, Landroid/net/NetworkInfo;->getDetailedState()Landroid/net/NetworkInfo$DetailedState;

    move-result-object v3

    sget-object v5, Landroid/net/NetworkInfo$DetailedState;->CONNECTED:Landroid/net/NetworkInfo$DetailedState;

    if-ne v3, v5, :cond_0

    iget-object v5, p0, Lcom/konka/systemsetting/net/WpsDialog;->mDialogState:Lcom/konka/systemsetting/net/WpsDialog$DialogState;

    sget-object v6, Lcom/konka/systemsetting/net/WpsDialog$DialogState;->WPS_COMPLETE:Lcom/konka/systemsetting/net/WpsDialog$DialogState;

    if-ne v5, v6, :cond_0

    iget-object v5, p0, Lcom/konka/systemsetting/net/WpsDialog;->mWifiManager:Landroid/net/wifi/WifiManager;

    invoke-virtual {v5}, Landroid/net/wifi/WifiManager;->getConnectionInfo()Landroid/net/wifi/WifiInfo;

    move-result-object v4

    if-eqz v4, :cond_0

    iget-object v5, p0, Lcom/konka/systemsetting/net/WpsDialog;->mContext:Landroid/content/Context;

    const v6, 0x7f06015f    # com.konka.systemsetting.R.string.wifi_wps_connected

    invoke-virtual {v5, v6}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v5

    const/4 v6, 0x1

    new-array v6, v6, [Ljava/lang/Object;

    const/4 v7, 0x0

    invoke-virtual {v4}, Landroid/net/wifi/WifiInfo;->getSSID()Ljava/lang/String;

    move-result-object v8

    aput-object v8, v6, v7

    invoke-static {v5, v6}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v2

    sget-object v5, Lcom/konka/systemsetting/net/WpsDialog$DialogState;->CONNECTED:Lcom/konka/systemsetting/net/WpsDialog$DialogState;

    invoke-direct {p0, v5, v2}, Lcom/konka/systemsetting/net/WpsDialog;->updateDialog(Lcom/konka/systemsetting/net/WpsDialog$DialogState;Ljava/lang/String;)V

    :cond_0
    return-void
.end method

.method private showToast()V
    .locals 6

    :try_start_0
    iget-object v3, p0, Lcom/konka/systemsetting/net/WpsDialog;->mContext:Landroid/content/Context;

    invoke-static {v3}, Landroid/view/LayoutInflater;->from(Landroid/content/Context;)Landroid/view/LayoutInflater;

    move-result-object v3

    const v4, 0x7f03002c    # com.konka.systemsetting.R.layout.wifi_wps_success

    const/4 v5, 0x0

    invoke-virtual {v3, v4, v5}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;)Landroid/view/View;

    move-result-object v0

    new-instance v2, Landroid/widget/Toast;

    iget-object v3, p0, Lcom/konka/systemsetting/net/WpsDialog;->mContext:Landroid/content/Context;

    invoke-direct {v2, v3}, Landroid/widget/Toast;-><init>(Landroid/content/Context;)V

    invoke-virtual {v2, v0}, Landroid/widget/Toast;->setView(Landroid/view/View;)V

    const/4 v3, 0x1

    invoke-virtual {v2, v3}, Landroid/widget/Toast;->setDuration(I)V

    invoke-virtual {v2}, Landroid/widget/Toast;->show()V
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    :goto_0
    return-void

    :catch_0
    move-exception v1

    invoke-virtual {v1}, Ljava/lang/Exception;->printStackTrace()V

    goto :goto_0
.end method

.method private updateDialog(Lcom/konka/systemsetting/net/WpsDialog$DialogState;Ljava/lang/String;)V
    .locals 2
    .param p1    # Lcom/konka/systemsetting/net/WpsDialog$DialogState;
    .param p2    # Ljava/lang/String;

    iget-object v0, p0, Lcom/konka/systemsetting/net/WpsDialog;->mDialogState:Lcom/konka/systemsetting/net/WpsDialog$DialogState;

    invoke-virtual {v0}, Lcom/konka/systemsetting/net/WpsDialog$DialogState;->ordinal()I

    move-result v0

    invoke-virtual {p1}, Lcom/konka/systemsetting/net/WpsDialog$DialogState;->ordinal()I

    move-result v1

    if-lt v0, v1, :cond_0

    :goto_0
    return-void

    :cond_0
    iput-object p1, p0, Lcom/konka/systemsetting/net/WpsDialog;->mDialogState:Lcom/konka/systemsetting/net/WpsDialog$DialogState;

    iget-object v0, p0, Lcom/konka/systemsetting/net/WpsDialog;->mHandler:Landroid/os/Handler;

    new-instance v1, Lcom/konka/systemsetting/net/WpsDialog$4;

    invoke-direct {v1, p0, p1, p2}, Lcom/konka/systemsetting/net/WpsDialog$4;-><init>(Lcom/konka/systemsetting/net/WpsDialog;Lcom/konka/systemsetting/net/WpsDialog$DialogState;Ljava/lang/String;)V

    invoke-virtual {v0, v1}, Landroid/os/Handler;->post(Ljava/lang/Runnable;)Z

    goto :goto_0
.end method


# virtual methods
.method protected onCreate(Landroid/os/Bundle;)V
    .locals 3
    .param p1    # Landroid/os/Bundle;

    invoke-virtual {p0}, Lcom/konka/systemsetting/net/WpsDialog;->getLayoutInflater()Landroid/view/LayoutInflater;

    move-result-object v0

    const v1, 0x7f03002b    # com.konka.systemsetting.R.layout.wifi_wps_dialog

    const/4 v2, 0x0

    invoke-virtual {v0, v1, v2}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;)Landroid/view/View;

    move-result-object v0

    iput-object v0, p0, Lcom/konka/systemsetting/net/WpsDialog;->mView:Landroid/view/View;

    iget-object v0, p0, Lcom/konka/systemsetting/net/WpsDialog;->mView:Landroid/view/View;

    const v1, 0x7f0900c2    # com.konka.systemsetting.R.id.wps_dialog_txt

    invoke-virtual {v0, v1}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    iput-object v0, p0, Lcom/konka/systemsetting/net/WpsDialog;->mTextView:Landroid/widget/TextView;

    iget-object v0, p0, Lcom/konka/systemsetting/net/WpsDialog;->mTextView:Landroid/widget/TextView;

    const v1, 0x7f06015d    # com.konka.systemsetting.R.string.wifi_wps_setup_msg

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setText(I)V

    iget-object v0, p0, Lcom/konka/systemsetting/net/WpsDialog;->mView:Landroid/view/View;

    const v1, 0x7f0900c3    # com.konka.systemsetting.R.id.wps_timeout_bar

    invoke-virtual {v0, v1}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/ProgressBar;

    iput-object v0, p0, Lcom/konka/systemsetting/net/WpsDialog;->mTimeoutBar:Landroid/widget/ProgressBar;

    iget-object v0, p0, Lcom/konka/systemsetting/net/WpsDialog;->mTimeoutBar:Landroid/widget/ProgressBar;

    const/16 v1, 0x78

    invoke-virtual {v0, v1}, Landroid/widget/ProgressBar;->setMax(I)V

    iget-object v0, p0, Lcom/konka/systemsetting/net/WpsDialog;->mTimeoutBar:Landroid/widget/ProgressBar;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Landroid/widget/ProgressBar;->setProgress(I)V

    iget-object v0, p0, Lcom/konka/systemsetting/net/WpsDialog;->mView:Landroid/view/View;

    const v1, 0x7f0900c4    # com.konka.systemsetting.R.id.wps_progress_bar

    invoke-virtual {v0, v1}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/ProgressBar;

    iput-object v0, p0, Lcom/konka/systemsetting/net/WpsDialog;->mProgressBar:Landroid/widget/ProgressBar;

    iget-object v0, p0, Lcom/konka/systemsetting/net/WpsDialog;->mProgressBar:Landroid/widget/ProgressBar;

    const/16 v1, 0x8

    invoke-virtual {v0, v1}, Landroid/widget/ProgressBar;->setVisibility(I)V

    iget-object v0, p0, Lcom/konka/systemsetting/net/WpsDialog;->mView:Landroid/view/View;

    const v1, 0x7f0900c5    # com.konka.systemsetting.R.id.wps_dialog_btn

    invoke-virtual {v0, v1}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/Button;

    iput-object v0, p0, Lcom/konka/systemsetting/net/WpsDialog;->mButton:Landroid/widget/Button;

    iget-object v0, p0, Lcom/konka/systemsetting/net/WpsDialog;->mButton:Landroid/widget/Button;

    const v1, 0x7f06015e    # com.konka.systemsetting.R.string.wifi_cancel

    invoke-virtual {v0, v1}, Landroid/widget/Button;->setText(I)V

    iget-object v0, p0, Lcom/konka/systemsetting/net/WpsDialog;->mButton:Landroid/widget/Button;

    new-instance v1, Lcom/konka/systemsetting/net/WpsDialog$2;

    invoke-direct {v1, p0}, Lcom/konka/systemsetting/net/WpsDialog$2;-><init>(Lcom/konka/systemsetting/net/WpsDialog;)V

    invoke-virtual {v0, v1}, Landroid/widget/Button;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    iget-object v0, p0, Lcom/konka/systemsetting/net/WpsDialog;->mContext:Landroid/content/Context;

    const-string v1, "wifi"

    invoke-virtual {v0, v1}, Landroid/content/Context;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/net/wifi/WifiManager;

    iput-object v0, p0, Lcom/konka/systemsetting/net/WpsDialog;->mWifiManager:Landroid/net/wifi/WifiManager;

    iget-object v0, p0, Lcom/konka/systemsetting/net/WpsDialog;->mView:Landroid/view/View;

    invoke-virtual {p0, v0}, Lcom/konka/systemsetting/net/WpsDialog;->setContentView(Landroid/view/View;)V

    invoke-super {p0, p1}, Landroid/app/Dialog;->onCreate(Landroid/os/Bundle;)V

    return-void
.end method

.method protected onStart()V
    .locals 7

    const-wide/16 v2, 0x3e8

    new-instance v0, Ljava/util/Timer;

    const/4 v1, 0x0

    invoke-direct {v0, v1}, Ljava/util/Timer;-><init>(Z)V

    iput-object v0, p0, Lcom/konka/systemsetting/net/WpsDialog;->mTimer:Ljava/util/Timer;

    iget-object v0, p0, Lcom/konka/systemsetting/net/WpsDialog;->mTimer:Ljava/util/Timer;

    new-instance v1, Lcom/konka/systemsetting/net/WpsDialog$3;

    invoke-direct {v1, p0}, Lcom/konka/systemsetting/net/WpsDialog$3;-><init>(Lcom/konka/systemsetting/net/WpsDialog;)V

    move-wide v4, v2

    invoke-virtual/range {v0 .. v5}, Ljava/util/Timer;->schedule(Ljava/util/TimerTask;JJ)V

    iget-object v0, p0, Lcom/konka/systemsetting/net/WpsDialog;->mContext:Landroid/content/Context;

    iget-object v1, p0, Lcom/konka/systemsetting/net/WpsDialog;->mReceiver:Landroid/content/BroadcastReceiver;

    iget-object v2, p0, Lcom/konka/systemsetting/net/WpsDialog;->mFilter:Landroid/content/IntentFilter;

    invoke-virtual {v0, v1, v2}, Landroid/content/Context;->registerReceiver(Landroid/content/BroadcastReceiver;Landroid/content/IntentFilter;)Landroid/content/Intent;

    new-instance v6, Landroid/net/wifi/WpsInfo;

    invoke-direct {v6}, Landroid/net/wifi/WpsInfo;-><init>()V

    iget v0, p0, Lcom/konka/systemsetting/net/WpsDialog;->mWpsSetup:I

    iput v0, v6, Landroid/net/wifi/WpsInfo;->setup:I

    iget-object v0, p0, Lcom/konka/systemsetting/net/WpsDialog;->mWifiManager:Landroid/net/wifi/WifiManager;

    iget-object v1, p0, Lcom/konka/systemsetting/net/WpsDialog;->mWpsListener:Landroid/net/wifi/WifiManager$WpsListener;

    invoke-virtual {v0, v6, v1}, Landroid/net/wifi/WifiManager;->startWps(Landroid/net/wifi/WpsInfo;Landroid/net/wifi/WifiManager$WpsListener;)V

    return-void
.end method

.method protected onStop()V
    .locals 3

    const/4 v2, 0x0

    iget-object v0, p0, Lcom/konka/systemsetting/net/WpsDialog;->mDialogState:Lcom/konka/systemsetting/net/WpsDialog$DialogState;

    sget-object v1, Lcom/konka/systemsetting/net/WpsDialog$DialogState;->WPS_COMPLETE:Lcom/konka/systemsetting/net/WpsDialog$DialogState;

    if-eq v0, v1, :cond_0

    iget-object v0, p0, Lcom/konka/systemsetting/net/WpsDialog;->mWifiManager:Landroid/net/wifi/WifiManager;

    invoke-virtual {v0, v2}, Landroid/net/wifi/WifiManager;->cancelWps(Landroid/net/wifi/WifiManager$ActionListener;)V

    :cond_0
    iget-object v0, p0, Lcom/konka/systemsetting/net/WpsDialog;->mReceiver:Landroid/content/BroadcastReceiver;

    if-eqz v0, :cond_1

    iget-object v0, p0, Lcom/konka/systemsetting/net/WpsDialog;->mContext:Landroid/content/Context;

    iget-object v1, p0, Lcom/konka/systemsetting/net/WpsDialog;->mReceiver:Landroid/content/BroadcastReceiver;

    invoke-virtual {v0, v1}, Landroid/content/Context;->unregisterReceiver(Landroid/content/BroadcastReceiver;)V

    iput-object v2, p0, Lcom/konka/systemsetting/net/WpsDialog;->mReceiver:Landroid/content/BroadcastReceiver;

    :cond_1
    iget-object v0, p0, Lcom/konka/systemsetting/net/WpsDialog;->mTimer:Ljava/util/Timer;

    if-eqz v0, :cond_2

    iget-object v0, p0, Lcom/konka/systemsetting/net/WpsDialog;->mTimer:Ljava/util/Timer;

    invoke-virtual {v0}, Ljava/util/Timer;->cancel()V

    :cond_2
    return-void
.end method
