.class public Lcom/konka/systemsetting/net/NetworkWifiConfig$WifiReceiver;
.super Landroid/content/BroadcastReceiver;
.source "NetworkWifiConfig.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/konka/systemsetting/net/NetworkWifiConfig;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x1
    name = "WifiReceiver"
.end annotation


# instance fields
.field final synthetic this$0:Lcom/konka/systemsetting/net/NetworkWifiConfig;


# direct methods
.method public constructor <init>(Lcom/konka/systemsetting/net/NetworkWifiConfig;)V
    .locals 0

    iput-object p1, p0, Lcom/konka/systemsetting/net/NetworkWifiConfig$WifiReceiver;->this$0:Lcom/konka/systemsetting/net/NetworkWifiConfig;

    invoke-direct {p0}, Landroid/content/BroadcastReceiver;-><init>()V

    return-void
.end method


# virtual methods
.method public onReceive(Landroid/content/Context;Landroid/content/Intent;)V
    .locals 5
    .param p1    # Landroid/content/Context;
    .param p2    # Landroid/content/Intent;

    invoke-virtual {p2}, Landroid/content/Intent;->getAction()Ljava/lang/String;

    move-result-object v0

    iget-object v2, p0, Lcom/konka/systemsetting/net/NetworkWifiConfig$WifiReceiver;->this$0:Lcom/konka/systemsetting/net/NetworkWifiConfig;

    new-instance v3, Ljava/lang/StringBuilder;

    const-string v4, "WifiReceiver->"

    invoke-direct {v3, v4}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v3, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Lcom/konka/systemsetting/net/NetworkWifiConfig;->log(Ljava/lang/String;)V

    const-string v2, "android.net.wifi.RSSI_CHANGED"

    invoke-virtual {v2, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-nez v2, :cond_0

    const-string v2, "android.net.wifi.SCAN_RESULTS"

    invoke-virtual {v2, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_1

    iget-object v2, p0, Lcom/konka/systemsetting/net/NetworkWifiConfig$WifiReceiver;->this$0:Lcom/konka/systemsetting/net/NetworkWifiConfig;

    const/4 v3, 0x1

    invoke-static {v2, v3}, Lcom/konka/systemsetting/net/NetworkWifiConfig;->access$17(Lcom/konka/systemsetting/net/NetworkWifiConfig;Z)V

    iget-object v2, p0, Lcom/konka/systemsetting/net/NetworkWifiConfig$WifiReceiver;->this$0:Lcom/konka/systemsetting/net/NetworkWifiConfig;

    # invokes: Lcom/konka/systemsetting/net/NetworkWifiConfig;->updateScanlist()V
    invoke-static {v2}, Lcom/konka/systemsetting/net/NetworkWifiConfig;->access$18(Lcom/konka/systemsetting/net/NetworkWifiConfig;)V

    iget-object v2, p0, Lcom/konka/systemsetting/net/NetworkWifiConfig$WifiReceiver;->this$0:Lcom/konka/systemsetting/net/NetworkWifiConfig;

    const/4 v3, 0x0

    invoke-static {v2, v3}, Lcom/konka/systemsetting/net/NetworkWifiConfig;->access$17(Lcom/konka/systemsetting/net/NetworkWifiConfig;Z)V

    :cond_0
    :goto_0
    return-void

    :cond_1
    const-string v2, "android.net.wifi.LINK_CONFIGURATION_CHANGED"

    invoke-virtual {v2, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_2

    iget-object v2, p0, Lcom/konka/systemsetting/net/NetworkWifiConfig$WifiReceiver;->this$0:Lcom/konka/systemsetting/net/NetworkWifiConfig;

    # invokes: Lcom/konka/systemsetting/net/NetworkWifiConfig;->updateScanlist()V
    invoke-static {v2}, Lcom/konka/systemsetting/net/NetworkWifiConfig;->access$18(Lcom/konka/systemsetting/net/NetworkWifiConfig;)V

    goto :goto_0

    :cond_2
    const-string v2, "android.net.wifi.CONFIGURED_NETWORKS_CHANGE"

    invoke-virtual {v2, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_3

    iget-object v2, p0, Lcom/konka/systemsetting/net/NetworkWifiConfig$WifiReceiver;->this$0:Lcom/konka/systemsetting/net/NetworkWifiConfig;

    # invokes: Lcom/konka/systemsetting/net/NetworkWifiConfig;->updateScanlist()V
    invoke-static {v2}, Lcom/konka/systemsetting/net/NetworkWifiConfig;->access$18(Lcom/konka/systemsetting/net/NetworkWifiConfig;)V

    goto :goto_0

    :cond_3
    const-string v2, "android.net.wifi.WIFI_STATE_CHANGED"

    invoke-virtual {v2, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_4

    iget-object v2, p0, Lcom/konka/systemsetting/net/NetworkWifiConfig$WifiReceiver;->this$0:Lcom/konka/systemsetting/net/NetworkWifiConfig;

    invoke-virtual {v2, p2}, Lcom/konka/systemsetting/net/NetworkWifiConfig;->handleWifiStateChanged(Landroid/content/Intent;)V

    goto :goto_0

    :cond_4
    const-string v2, "android.net.wifi.NETWORK_IDS_CHANGED"

    invoke-virtual {v2, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-nez v2, :cond_0

    const-string v2, "android.net.wifi.STATE_CHANGE"

    invoke-virtual {v2, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_0

    const-string v2, "networkInfo"

    invoke-virtual {p2, v2}, Landroid/content/Intent;->getParcelableExtra(Ljava/lang/String;)Landroid/os/Parcelable;

    move-result-object v1

    check-cast v1, Landroid/net/NetworkInfo;

    iget-object v2, p0, Lcom/konka/systemsetting/net/NetworkWifiConfig$WifiReceiver;->this$0:Lcom/konka/systemsetting/net/NetworkWifiConfig;

    invoke-virtual {v1}, Landroid/net/NetworkInfo;->getDetailedState()Landroid/net/NetworkInfo$DetailedState;

    move-result-object v3

    # invokes: Lcom/konka/systemsetting/net/NetworkWifiConfig;->updateConnectionState(Landroid/net/NetworkInfo$DetailedState;)V
    invoke-static {v2, v3}, Lcom/konka/systemsetting/net/NetworkWifiConfig;->access$19(Lcom/konka/systemsetting/net/NetworkWifiConfig;Landroid/net/NetworkInfo$DetailedState;)V

    goto :goto_0
.end method
