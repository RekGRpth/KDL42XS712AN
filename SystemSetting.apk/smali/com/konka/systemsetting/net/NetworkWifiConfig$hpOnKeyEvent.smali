.class Lcom/konka/systemsetting/net/NetworkWifiConfig$hpOnKeyEvent;
.super Ljava/lang/Object;
.source "NetworkWifiConfig.java"

# interfaces
.implements Landroid/view/View$OnKeyListener;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/konka/systemsetting/net/NetworkWifiConfig;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x2
    name = "hpOnKeyEvent"
.end annotation


# instance fields
.field final synthetic this$0:Lcom/konka/systemsetting/net/NetworkWifiConfig;


# direct methods
.method private constructor <init>(Lcom/konka/systemsetting/net/NetworkWifiConfig;)V
    .locals 0

    iput-object p1, p0, Lcom/konka/systemsetting/net/NetworkWifiConfig$hpOnKeyEvent;->this$0:Lcom/konka/systemsetting/net/NetworkWifiConfig;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method synthetic constructor <init>(Lcom/konka/systemsetting/net/NetworkWifiConfig;Lcom/konka/systemsetting/net/NetworkWifiConfig$hpOnKeyEvent;)V
    .locals 0

    invoke-direct {p0, p1}, Lcom/konka/systemsetting/net/NetworkWifiConfig$hpOnKeyEvent;-><init>(Lcom/konka/systemsetting/net/NetworkWifiConfig;)V

    return-void
.end method


# virtual methods
.method public onKey(Landroid/view/View;ILandroid/view/KeyEvent;)Z
    .locals 3
    .param p1    # Landroid/view/View;
    .param p2    # I
    .param p3    # Landroid/view/KeyEvent;

    const/4 v0, 0x0

    sparse-switch p2, :sswitch_data_0

    :goto_0
    return v0

    :sswitch_0
    invoke-virtual {p3}, Landroid/view/KeyEvent;->getAction()I

    move-result v0

    if-nez v0, :cond_0

    iget-object v0, p0, Lcom/konka/systemsetting/net/NetworkWifiConfig$hpOnKeyEvent;->this$0:Lcom/konka/systemsetting/net/NetworkWifiConfig;

    # invokes: Lcom/konka/systemsetting/net/NetworkWifiConfig;->hpOnUpKeyDown()V
    invoke-static {v0}, Lcom/konka/systemsetting/net/NetworkWifiConfig;->access$2(Lcom/konka/systemsetting/net/NetworkWifiConfig;)V

    :cond_0
    :goto_1
    const/4 v0, 0x1

    goto :goto_0

    :sswitch_1
    invoke-virtual {p3}, Landroid/view/KeyEvent;->getAction()I

    move-result v0

    if-nez v0, :cond_0

    iget-object v0, p0, Lcom/konka/systemsetting/net/NetworkWifiConfig$hpOnKeyEvent;->this$0:Lcom/konka/systemsetting/net/NetworkWifiConfig;

    # invokes: Lcom/konka/systemsetting/net/NetworkWifiConfig;->hpOnDownKeyDown()V
    invoke-static {v0}, Lcom/konka/systemsetting/net/NetworkWifiConfig;->access$3(Lcom/konka/systemsetting/net/NetworkWifiConfig;)V

    goto :goto_1

    :sswitch_2
    invoke-virtual {p3}, Landroid/view/KeyEvent;->getAction()I

    move-result v1

    if-nez v1, :cond_0

    iget-object v1, p0, Lcom/konka/systemsetting/net/NetworkWifiConfig$hpOnKeyEvent;->this$0:Lcom/konka/systemsetting/net/NetworkWifiConfig;

    # getter for: Lcom/konka/systemsetting/net/NetworkWifiConfig;->itemHotspots:[Landroid/widget/LinearLayout;
    invoke-static {v1}, Lcom/konka/systemsetting/net/NetworkWifiConfig;->access$8(Lcom/konka/systemsetting/net/NetworkWifiConfig;)[Landroid/widget/LinearLayout;

    move-result-object v1

    iget-object v2, p0, Lcom/konka/systemsetting/net/NetworkWifiConfig$hpOnKeyEvent;->this$0:Lcom/konka/systemsetting/net/NetworkWifiConfig;

    # getter for: Lcom/konka/systemsetting/net/NetworkWifiConfig;->iCurrSeletedIndex:I
    invoke-static {v2}, Lcom/konka/systemsetting/net/NetworkWifiConfig;->access$9(Lcom/konka/systemsetting/net/NetworkWifiConfig;)I

    move-result v2

    aget-object v1, v1, v2

    invoke-virtual {v1, v0}, Landroid/widget/LinearLayout;->getChildAt(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    invoke-virtual {v0}, Landroid/widget/TextView;->getVisibility()I

    move-result v0

    if-nez v0, :cond_0

    iget-object v0, p0, Lcom/konka/systemsetting/net/NetworkWifiConfig$hpOnKeyEvent;->this$0:Lcom/konka/systemsetting/net/NetworkWifiConfig;

    # invokes: Lcom/konka/systemsetting/net/NetworkWifiConfig;->showAPSettingDlg()V
    invoke-static {v0}, Lcom/konka/systemsetting/net/NetworkWifiConfig;->access$12(Lcom/konka/systemsetting/net/NetworkWifiConfig;)V

    goto :goto_1

    :sswitch_data_0
    .sparse-switch
        0x13 -> :sswitch_0
        0x14 -> :sswitch_1
        0x17 -> :sswitch_2
        0x42 -> :sswitch_2
    .end sparse-switch
.end method
