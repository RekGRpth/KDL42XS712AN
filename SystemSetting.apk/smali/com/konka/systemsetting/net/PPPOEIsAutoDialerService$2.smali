.class Lcom/konka/systemsetting/net/PPPOEIsAutoDialerService$2;
.super Ljava/lang/Thread;
.source "PPPOEIsAutoDialerService.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/konka/systemsetting/net/PPPOEIsAutoDialerService;->pppoeDialer()V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/konka/systemsetting/net/PPPOEIsAutoDialerService;


# direct methods
.method constructor <init>(Lcom/konka/systemsetting/net/PPPOEIsAutoDialerService;)V
    .locals 0

    iput-object p1, p0, Lcom/konka/systemsetting/net/PPPOEIsAutoDialerService$2;->this$0:Lcom/konka/systemsetting/net/PPPOEIsAutoDialerService;

    invoke-direct {p0}, Ljava/lang/Thread;-><init>()V

    return-void
.end method


# virtual methods
.method public run()V
    .locals 4

    :goto_0
    iget-object v1, p0, Lcom/konka/systemsetting/net/PPPOEIsAutoDialerService$2;->this$0:Lcom/konka/systemsetting/net/PPPOEIsAutoDialerService;

    iget-object v1, v1, Lcom/konka/systemsetting/net/PPPOEIsAutoDialerService;->mPPPOE:Landroid/net/pppoe/PppoeManager;

    invoke-virtual {v1}, Landroid/net/pppoe/PppoeManager;->PppoeGetStatus()Landroid/net/pppoe/PPPOE_STA;

    move-result-object v1

    sget-object v2, Landroid/net/pppoe/PPPOE_STA;->CONNECTING:Landroid/net/pppoe/PPPOE_STA;

    if-eq v1, v2, :cond_0

    iget-object v1, p0, Lcom/konka/systemsetting/net/PPPOEIsAutoDialerService$2;->this$0:Lcom/konka/systemsetting/net/PPPOEIsAutoDialerService;

    # getter for: Lcom/konka/systemsetting/net/PPPOEIsAutoDialerService;->mHandler:Landroid/os/Handler;
    invoke-static {v1}, Lcom/konka/systemsetting/net/PPPOEIsAutoDialerService;->access$1(Lcom/konka/systemsetting/net/PPPOEIsAutoDialerService;)Landroid/os/Handler;

    move-result-object v1

    iget-object v2, p0, Lcom/konka/systemsetting/net/PPPOEIsAutoDialerService$2;->this$0:Lcom/konka/systemsetting/net/PPPOEIsAutoDialerService;

    # getter for: Lcom/konka/systemsetting/net/PPPOEIsAutoDialerService;->updateState:Ljava/lang/Runnable;
    invoke-static {v2}, Lcom/konka/systemsetting/net/PPPOEIsAutoDialerService;->access$2(Lcom/konka/systemsetting/net/PPPOEIsAutoDialerService;)Ljava/lang/Runnable;

    move-result-object v2

    invoke-virtual {v1, v2}, Landroid/os/Handler;->post(Ljava/lang/Runnable;)Z

    return-void

    :cond_0
    const-wide/16 v1, 0x3e8

    :try_start_0
    invoke-static {v1, v2}, Lcom/konka/systemsetting/net/PPPOEIsAutoDialerService$2;->sleep(J)V
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    goto :goto_0

    :catch_0
    move-exception v0

    const-string v1, "PppoeSettings"

    new-instance v2, Ljava/lang/StringBuilder;

    const-string v3, "Exception: "

    invoke-direct {v2, v3}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v0}, Ljava/lang/Exception;->getMessage()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_0
.end method
