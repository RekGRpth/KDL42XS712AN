.class public Lcom/konka/systemsetting/MainActivity;
.super Landroid/app/Activity;
.source "MainActivity.java"


# static fields
.field public static final PAGE_INDIVID:I = 0x2

.field public static final PAGE_NETWORK:I = 0x0

.field public static final PAGE_STORAGE:I = 0x3

.field public static final PAGE_SYSTEM:I = 0x1

.field public static final STATE_CURRENT_FOCUSED:I = 0x0

.field public static final STATE_LAST_FOCUSED:I = 0x1

.field public static final STATE_NO_FOCUSE:I = 0x2

.field public static final SUBPAGE_INIDIVIDUAL_ENERGY:I = 0x0

.field public static final SUBPAGE_INIDIVIDUAL_TIMING:I = 0x1

.field public static final SUBPAGE_NET_LAN:I = 0x0

.field public static final SUBPAGE_NET_PPOE:I = 0x2

.field public static final SUBPAGE_NET_WIRELESS:I = 0x1

.field public static final SUBPAGE_STORAGE_RAM:I = 0x0

.field public static final SUBPAGE_STORAGE_SD:I = 0x1

.field public static final SUBPAGE_SYS_DISCLAIMER:I = 0x4

.field public static final SUBPAGE_SYS_INFO:I = 0x2

.field public static final SUBPAGE_SYS_INPUT:I = 0x1

.field public static final SUBPAGE_SYS_LANGUAGE:I = 0x0

.field public static final SUBPAGE_SYS_RESET:I = 0x5

.field public static final SUBPAGE_SYS_UPGRADE:I = 0x3


# instance fields
.field public final DLG_INIT_TIME_SETTING:I

.field public IMG_SWITCH_OFF:I

.field public IMG_SWITCH_ON:I

.field private bPassportConnectOK:Z

.field private connPassport:Landroid/content/ServiceConnection;

.field private gstrSN:Ljava/lang/String;

.field private iPerson:Lcom/konka/passport/service/UserInfo;

.field private mHangingDialog:Landroid/app/ProgressDialog;

.field public mViewHolder:Lcom/konka/systemsetting/ViewHolder;

.field private m_iCurPage:I

.field private m_iLastFocusPage:I

.field public m_individPageManager:Lcom/konka/systemsetting/individ/IndividPageManager;

.field public m_itemEnergy:Landroid/widget/LinearLayout;

.field public m_itemNetwork:Landroid/widget/LinearLayout;

.field public m_itemStorage:Landroid/widget/LinearLayout;

.field public m_itemSystem:Landroid/widget/LinearLayout;

.field public m_networkPageManager:Lcom/konka/systemsetting/net/NetworkPageManager;

.field public m_storagePgaeManager:Lcom/konka/systemsetting/storage/StoragePageManager;

.field public m_systemPageManager:Lcom/konka/systemsetting/system/SystemPageManager;

.field private vfMainMenu:Landroid/widget/ViewFlipper;


# direct methods
.method public constructor <init>()V
    .locals 3

    const/4 v2, 0x0

    const/high16 v1, -0x1000000

    const/4 v0, 0x0

    invoke-direct {p0}, Landroid/app/Activity;-><init>()V

    iput-object v2, p0, Lcom/konka/systemsetting/MainActivity;->mViewHolder:Lcom/konka/systemsetting/ViewHolder;

    iput-object v2, p0, Lcom/konka/systemsetting/MainActivity;->gstrSN:Ljava/lang/String;

    iput v0, p0, Lcom/konka/systemsetting/MainActivity;->m_iCurPage:I

    iput v0, p0, Lcom/konka/systemsetting/MainActivity;->m_iLastFocusPage:I

    iput v1, p0, Lcom/konka/systemsetting/MainActivity;->IMG_SWITCH_ON:I

    iput v1, p0, Lcom/konka/systemsetting/MainActivity;->IMG_SWITCH_OFF:I

    iput-boolean v0, p0, Lcom/konka/systemsetting/MainActivity;->bPassportConnectOK:Z

    new-instance v0, Lcom/konka/systemsetting/MainActivity$1;

    invoke-direct {v0, p0}, Lcom/konka/systemsetting/MainActivity$1;-><init>(Lcom/konka/systemsetting/MainActivity;)V

    iput-object v0, p0, Lcom/konka/systemsetting/MainActivity;->connPassport:Landroid/content/ServiceConnection;

    const/16 v0, 0x31

    iput v0, p0, Lcom/konka/systemsetting/MainActivity;->DLG_INIT_TIME_SETTING:I

    return-void
.end method

.method private SetOnFocusChangeListener()V
    .locals 2

    new-instance v0, Lcom/konka/systemsetting/MainActivity$2;

    invoke-direct {v0, p0}, Lcom/konka/systemsetting/MainActivity$2;-><init>(Lcom/konka/systemsetting/MainActivity;)V

    iget-object v1, p0, Lcom/konka/systemsetting/MainActivity;->m_itemNetwork:Landroid/widget/LinearLayout;

    invoke-virtual {v1, v0}, Landroid/widget/LinearLayout;->setOnFocusChangeListener(Landroid/view/View$OnFocusChangeListener;)V

    iget-object v1, p0, Lcom/konka/systemsetting/MainActivity;->m_itemSystem:Landroid/widget/LinearLayout;

    invoke-virtual {v1, v0}, Landroid/widget/LinearLayout;->setOnFocusChangeListener(Landroid/view/View$OnFocusChangeListener;)V

    iget-object v1, p0, Lcom/konka/systemsetting/MainActivity;->m_itemEnergy:Landroid/widget/LinearLayout;

    invoke-virtual {v1, v0}, Landroid/widget/LinearLayout;->setOnFocusChangeListener(Landroid/view/View$OnFocusChangeListener;)V

    iget-object v1, p0, Lcom/konka/systemsetting/MainActivity;->m_itemStorage:Landroid/widget/LinearLayout;

    invoke-virtual {v1, v0}, Landroid/widget/LinearLayout;->setOnFocusChangeListener(Landroid/view/View$OnFocusChangeListener;)V

    return-void
.end method

.method static synthetic access$0(Lcom/konka/systemsetting/MainActivity;Lcom/konka/passport/service/UserInfo;)V
    .locals 0

    iput-object p1, p0, Lcom/konka/systemsetting/MainActivity;->iPerson:Lcom/konka/passport/service/UserInfo;

    return-void
.end method

.method static synthetic access$1(Lcom/konka/systemsetting/MainActivity;)Lcom/konka/passport/service/UserInfo;
    .locals 1

    iget-object v0, p0, Lcom/konka/systemsetting/MainActivity;->iPerson:Lcom/konka/passport/service/UserInfo;

    return-object v0
.end method

.method static synthetic access$2(Lcom/konka/systemsetting/MainActivity;Ljava/lang/String;)V
    .locals 0

    iput-object p1, p0, Lcom/konka/systemsetting/MainActivity;->gstrSN:Ljava/lang/String;

    return-void
.end method

.method static synthetic access$3(Lcom/konka/systemsetting/MainActivity;)Ljava/lang/String;
    .locals 1

    iget-object v0, p0, Lcom/konka/systemsetting/MainActivity;->gstrSN:Ljava/lang/String;

    return-object v0
.end method

.method static synthetic access$4(Lcom/konka/systemsetting/MainActivity;)I
    .locals 1

    iget v0, p0, Lcom/konka/systemsetting/MainActivity;->m_iCurPage:I

    return v0
.end method

.method static synthetic access$5(Lcom/konka/systemsetting/MainActivity;I)V
    .locals 0

    iput p1, p0, Lcom/konka/systemsetting/MainActivity;->m_iLastFocusPage:I

    return-void
.end method

.method static synthetic access$6(Lcom/konka/systemsetting/MainActivity;I)V
    .locals 0

    iput p1, p0, Lcom/konka/systemsetting/MainActivity;->m_iCurPage:I

    return-void
.end method

.method static synthetic access$7(Lcom/konka/systemsetting/MainActivity;)I
    .locals 1

    iget v0, p0, Lcom/konka/systemsetting/MainActivity;->m_iLastFocusPage:I

    return v0
.end method

.method static synthetic access$8(Lcom/konka/systemsetting/MainActivity;II)V
    .locals 0

    invoke-direct {p0, p1, p2}, Lcom/konka/systemsetting/MainActivity;->changeItemStatus(II)V

    return-void
.end method

.method static synthetic access$9(Lcom/konka/systemsetting/MainActivity;II)V
    .locals 0

    invoke-direct {p0, p1, p2}, Lcom/konka/systemsetting/MainActivity;->showPage(II)V

    return-void
.end method

.method private changeItemStatus(II)V
    .locals 8
    .param p1    # I
    .param p2    # I

    const v7, 0x7f020020    # com.konka.systemsetting.R.drawable.syssettingfirsticonnetunfocus

    const/4 v6, 0x1

    const/4 v5, 0x0

    const v4, -0x87e2

    const/4 v3, -0x1

    packed-switch p1, :pswitch_data_0

    const-string v2, "The item id is wrong!"

    invoke-static {v2}, Lcom/konka/debuginfo/logPrint;->Warning(Ljava/lang/String;)V

    :goto_0
    return-void

    :pswitch_0
    iget-object v2, p0, Lcom/konka/systemsetting/MainActivity;->m_itemNetwork:Landroid/widget/LinearLayout;

    invoke-virtual {v2, v5}, Landroid/widget/LinearLayout;->getChildAt(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/ImageView;

    iget-object v2, p0, Lcom/konka/systemsetting/MainActivity;->m_itemNetwork:Landroid/widget/LinearLayout;

    invoke-virtual {v2, v6}, Landroid/widget/LinearLayout;->getChildAt(I)Landroid/view/View;

    move-result-object v1

    check-cast v1, Landroid/widget/TextView;

    packed-switch p2, :pswitch_data_1

    const-string v2, "The item state is wrong!"

    invoke-static {v2}, Lcom/konka/debuginfo/logPrint;->Warning(Ljava/lang/String;)V

    goto :goto_0

    :pswitch_1
    invoke-virtual {v1, v3}, Landroid/widget/TextView;->setTextColor(I)V

    invoke-virtual {v0, v7}, Landroid/widget/ImageView;->setBackgroundResource(I)V

    goto :goto_0

    :pswitch_2
    invoke-virtual {v1, v4}, Landroid/widget/TextView;->setTextColor(I)V

    const v2, 0x7f02001f    # com.konka.systemsetting.R.drawable.syssettingfirsticonnetlastfocus

    invoke-virtual {v0, v2}, Landroid/widget/ImageView;->setBackgroundResource(I)V

    goto :goto_0

    :pswitch_3
    invoke-virtual {v1, v3}, Landroid/widget/TextView;->setTextColor(I)V

    invoke-virtual {v0, v7}, Landroid/widget/ImageView;->setBackgroundResource(I)V

    goto :goto_0

    :pswitch_4
    iget-object v2, p0, Lcom/konka/systemsetting/MainActivity;->m_itemSystem:Landroid/widget/LinearLayout;

    invoke-virtual {v2, v5}, Landroid/widget/LinearLayout;->getChildAt(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/ImageView;

    iget-object v2, p0, Lcom/konka/systemsetting/MainActivity;->m_itemSystem:Landroid/widget/LinearLayout;

    invoke-virtual {v2, v6}, Landroid/widget/LinearLayout;->getChildAt(I)Landroid/view/View;

    move-result-object v1

    check-cast v1, Landroid/widget/TextView;

    packed-switch p2, :pswitch_data_2

    const-string v2, "The item state is wrong!"

    invoke-static {v2}, Lcom/konka/debuginfo/logPrint;->Warning(Ljava/lang/String;)V

    goto :goto_0

    :pswitch_5
    invoke-virtual {v1, v3}, Landroid/widget/TextView;->setTextColor(I)V

    const v2, 0x7f020026    # com.konka.systemsetting.R.drawable.syssettingfirsticonsysunfocus

    invoke-virtual {v0, v2}, Landroid/widget/ImageView;->setBackgroundResource(I)V

    goto :goto_0

    :pswitch_6
    invoke-virtual {v1, v4}, Landroid/widget/TextView;->setTextColor(I)V

    const v2, 0x7f020025    # com.konka.systemsetting.R.drawable.syssettingfirsticonsyslastfocus

    invoke-virtual {v0, v2}, Landroid/widget/ImageView;->setBackgroundResource(I)V

    goto :goto_0

    :pswitch_7
    invoke-virtual {v1, v3}, Landroid/widget/TextView;->setTextColor(I)V

    const v2, 0x7f020026    # com.konka.systemsetting.R.drawable.syssettingfirsticonsysunfocus

    invoke-virtual {v0, v2}, Landroid/widget/ImageView;->setBackgroundResource(I)V

    goto :goto_0

    :pswitch_8
    iget-object v2, p0, Lcom/konka/systemsetting/MainActivity;->m_itemEnergy:Landroid/widget/LinearLayout;

    invoke-virtual {v2, v5}, Landroid/widget/LinearLayout;->getChildAt(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/ImageView;

    iget-object v2, p0, Lcom/konka/systemsetting/MainActivity;->m_itemEnergy:Landroid/widget/LinearLayout;

    invoke-virtual {v2, v6}, Landroid/widget/LinearLayout;->getChildAt(I)Landroid/view/View;

    move-result-object v1

    check-cast v1, Landroid/widget/TextView;

    packed-switch p2, :pswitch_data_3

    const-string v2, "The item state is wrong!"

    invoke-static {v2}, Lcom/konka/debuginfo/logPrint;->Warning(Ljava/lang/String;)V

    goto/16 :goto_0

    :pswitch_9
    invoke-virtual {v1, v3}, Landroid/widget/TextView;->setTextColor(I)V

    const v2, 0x7f020022    # com.konka.systemsetting.R.drawable.syssettingfirsticonpersonalunfocus

    invoke-virtual {v0, v2}, Landroid/widget/ImageView;->setBackgroundResource(I)V

    goto/16 :goto_0

    :pswitch_a
    invoke-virtual {v1, v4}, Landroid/widget/TextView;->setTextColor(I)V

    const v2, 0x7f020021    # com.konka.systemsetting.R.drawable.syssettingfirsticonpersonallastfocus

    invoke-virtual {v0, v2}, Landroid/widget/ImageView;->setBackgroundResource(I)V

    goto/16 :goto_0

    :pswitch_b
    invoke-virtual {v1, v3}, Landroid/widget/TextView;->setTextColor(I)V

    const v2, 0x7f020022    # com.konka.systemsetting.R.drawable.syssettingfirsticonpersonalunfocus

    invoke-virtual {v0, v2}, Landroid/widget/ImageView;->setBackgroundResource(I)V

    goto/16 :goto_0

    :pswitch_c
    iget-object v2, p0, Lcom/konka/systemsetting/MainActivity;->m_itemStorage:Landroid/widget/LinearLayout;

    invoke-virtual {v2, v5}, Landroid/widget/LinearLayout;->getChildAt(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/ImageView;

    iget-object v2, p0, Lcom/konka/systemsetting/MainActivity;->m_itemStorage:Landroid/widget/LinearLayout;

    invoke-virtual {v2, v6}, Landroid/widget/LinearLayout;->getChildAt(I)Landroid/view/View;

    move-result-object v1

    check-cast v1, Landroid/widget/TextView;

    packed-switch p2, :pswitch_data_4

    const-string v2, "The item state is wrong!"

    invoke-static {v2}, Lcom/konka/debuginfo/logPrint;->Warning(Ljava/lang/String;)V

    goto/16 :goto_0

    :pswitch_d
    invoke-virtual {v1, v3}, Landroid/widget/TextView;->setTextColor(I)V

    const v2, 0x7f020024    # com.konka.systemsetting.R.drawable.syssettingfirsticonsaveunfocus

    invoke-virtual {v0, v2}, Landroid/widget/ImageView;->setBackgroundResource(I)V

    goto/16 :goto_0

    :pswitch_e
    invoke-virtual {v1, v4}, Landroid/widget/TextView;->setTextColor(I)V

    const v2, 0x7f020023    # com.konka.systemsetting.R.drawable.syssettingfirsticonsavelastfocus

    invoke-virtual {v0, v2}, Landroid/widget/ImageView;->setBackgroundResource(I)V

    goto/16 :goto_0

    :pswitch_f
    invoke-virtual {v1, v3}, Landroid/widget/TextView;->setTextColor(I)V

    const v2, 0x7f020024    # com.konka.systemsetting.R.drawable.syssettingfirsticonsaveunfocus

    invoke-virtual {v0, v2}, Landroid/widget/ImageView;->setBackgroundResource(I)V

    goto/16 :goto_0

    :pswitch_data_0
    .packed-switch 0x0
        :pswitch_0
        :pswitch_4
        :pswitch_8
        :pswitch_c
    .end packed-switch

    :pswitch_data_1
    .packed-switch 0x0
        :pswitch_1
        :pswitch_2
        :pswitch_3
    .end packed-switch

    :pswitch_data_2
    .packed-switch 0x0
        :pswitch_5
        :pswitch_6
        :pswitch_7
    .end packed-switch

    :pswitch_data_3
    .packed-switch 0x0
        :pswitch_9
        :pswitch_a
        :pswitch_b
    .end packed-switch

    :pswitch_data_4
    .packed-switch 0x0
        :pswitch_d
        :pswitch_e
        :pswitch_f
    .end packed-switch
.end method

.method private getIntentPageByAction()Ljava/lang/String;
    .locals 5

    const/4 v1, 0x0

    invoke-virtual {p0}, Lcom/konka/systemsetting/MainActivity;->getIntent()Landroid/content/Intent;

    move-result-object v2

    invoke-virtual {v2}, Landroid/content/Intent;->getAction()Ljava/lang/String;

    move-result-object v0

    const-string v2, "MainActivity"

    new-instance v3, Ljava/lang/StringBuilder;

    const-string v4, "SystemSetting action = "

    invoke-direct {v3, v4}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v3, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v2, v3}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    const-string v2, "android.settings.WIRELESS_SETTINGS"

    invoke-virtual {v2, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_1

    const-string v1, "net_lan"

    :cond_0
    :goto_0
    return-object v1

    :cond_1
    const-string v2, "android.net.wifi.PICK_WIFI_NETWORK"

    invoke-virtual {v2, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_2

    const-string v1, "net_wireless"

    goto :goto_0

    :cond_2
    const-string v2, "android.settings.INPUT_METHOD_SUBTYPE_SETTINGS"

    invoke-virtual {v2, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_3

    const-string v1, "system_language"

    goto :goto_0

    :cond_3
    const-string v2, "android.settings.SET_MICROPHONE"

    invoke-virtual {v2, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_0

    const-string v1, "system_microphone"

    goto :goto_0
.end method

.method private getSnStr()Ljava/lang/String;
    .locals 20

    const/16 v1, 0x1d

    const/16 v6, 0x300

    const/4 v2, 0x0

    const/4 v4, 0x6

    const/16 v3, 0x80

    const/16 v5, 0x32

    const/16 v15, 0x300

    new-array v7, v15, [S

    const/16 v15, 0x32

    new-array v8, v15, [B

    :try_start_0
    invoke-static {}, Lcom/mstar/android/tvapi/common/TvManager;->getInstance()Lcom/mstar/android/tvapi/common/TvManager;
    :try_end_0
    .catch Lcom/mstar/android/tvapi/common/exception/TvCommonException; {:try_start_0 .. :try_end_0} :catch_0

    move-result-object v14

    if-nez v14, :cond_1

    const-string v13, ""

    :cond_0
    :goto_0
    return-object v13

    :cond_1
    const/16 v15, 0x1d

    const/16 v16, 0x300

    :try_start_1
    invoke-virtual/range {v14 .. v16}, Lcom/mstar/android/tvapi/common/TvManager;->readFromSpiFlashByBank(II)[S
    :try_end_1
    .catch Lcom/mstar/android/tvapi/common/exception/TvCommonException; {:try_start_1 .. :try_end_1} :catch_0

    move-result-object v7

    :goto_1
    const-string v15, "%02X"

    const/16 v16, 0x1

    move/from16 v0, v16

    new-array v0, v0, [Ljava/lang/Object;

    move-object/from16 v16, v0

    const/16 v17, 0x0

    const/16 v18, 0x0

    aget-short v18, v7, v18

    move/from16 v0, v18

    int-to-byte v0, v0

    move/from16 v18, v0

    invoke-static/range {v18 .. v18}, Ljava/lang/Byte;->valueOf(B)Ljava/lang/Byte;

    move-result-object v18

    aput-object v18, v16, v17

    invoke-static/range {v15 .. v16}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v11

    const/4 v10, 0x1

    :goto_2
    const/4 v15, 0x6

    if-lt v10, v15, :cond_2

    sget-object v15, Ljava/lang/System;->out:Ljava/io/PrintStream;

    new-instance v16, Ljava/lang/StringBuilder;

    const-string v17, "MacAddr: "

    invoke-direct/range {v16 .. v17}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    move-object/from16 v0, v16

    invoke-virtual {v0, v11}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v16

    invoke-virtual/range {v16 .. v16}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v16

    invoke-virtual/range {v15 .. v16}, Ljava/io/PrintStream;->println(Ljava/lang/String;)V

    const/4 v10, 0x0

    :goto_3
    const/16 v15, 0x32

    if-lt v10, v15, :cond_3

    new-instance v15, Ljava/lang/String;

    invoke-direct {v15, v8}, Ljava/lang/String;-><init>([B)V

    invoke-virtual {v15}, Ljava/lang/String;->trim()Ljava/lang/String;

    move-result-object v13

    sget-object v15, Ljava/lang/System;->out:Ljava/io/PrintStream;

    new-instance v16, Ljava/lang/StringBuilder;

    const-string v17, "SerialNum: "

    invoke-direct/range {v16 .. v17}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    move-object/from16 v0, v16

    invoke-virtual {v0, v13}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v16

    invoke-virtual/range {v16 .. v16}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v16

    invoke-virtual/range {v15 .. v16}, Ljava/io/PrintStream;->println(Ljava/lang/String;)V

    const-string v15, "^[A-Z0-9]{20}(_[A-F0-9]{8})?$"

    invoke-static {v15}, Ljava/util/regex/Pattern;->compile(Ljava/lang/String;)Ljava/util/regex/Pattern;

    move-result-object v12

    invoke-virtual {v12, v13}, Ljava/util/regex/Pattern;->matcher(Ljava/lang/CharSequence;)Ljava/util/regex/Matcher;

    move-result-object v15

    invoke-virtual {v15}, Ljava/util/regex/Matcher;->matches()Z

    move-result v15

    if-nez v15, :cond_0

    const-string v13, ""

    goto :goto_0

    :catch_0
    move-exception v9

    invoke-virtual {v9}, Lcom/mstar/android/tvapi/common/exception/TvCommonException;->printStackTrace()V

    goto :goto_1

    :cond_2
    new-instance v15, Ljava/lang/StringBuilder;

    invoke-static {v11}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v16

    invoke-direct/range {v15 .. v16}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    const-string v16, ":%02X"

    const/16 v17, 0x1

    move/from16 v0, v17

    new-array v0, v0, [Ljava/lang/Object;

    move-object/from16 v17, v0

    const/16 v18, 0x0

    add-int/lit8 v19, v10, 0x0

    aget-short v19, v7, v19

    move/from16 v0, v19

    int-to-byte v0, v0

    move/from16 v19, v0

    invoke-static/range {v19 .. v19}, Ljava/lang/Byte;->valueOf(B)Ljava/lang/Byte;

    move-result-object v19

    aput-object v19, v17, v18

    invoke-static/range {v16 .. v17}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v16

    invoke-virtual/range {v15 .. v16}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v15

    invoke-virtual {v15}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v11

    add-int/lit8 v10, v10, 0x1

    goto/16 :goto_2

    :cond_3
    add-int/lit16 v15, v10, 0x80

    aget-short v15, v7, v15

    int-to-byte v15, v15

    aput-byte v15, v8, v10

    add-int/lit8 v10, v10, 0x1

    goto :goto_3
.end method

.method private initItems()V
    .locals 2

    const v0, 0x7f090030    # com.konka.systemsetting.R.id.sys_main_menu_item_network

    invoke-virtual {p0, v0}, Lcom/konka/systemsetting/MainActivity;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/LinearLayout;

    iput-object v0, p0, Lcom/konka/systemsetting/MainActivity;->m_itemNetwork:Landroid/widget/LinearLayout;

    iget-object v0, p0, Lcom/konka/systemsetting/MainActivity;->m_itemNetwork:Landroid/widget/LinearLayout;

    const v1, 0x7f09003c    # com.konka.systemsetting.R.id.linearlayout_network_tab_wired

    invoke-virtual {v0, v1}, Landroid/widget/LinearLayout;->setNextFocusRightId(I)V

    const v0, 0x7f090031    # com.konka.systemsetting.R.id.sys_main_menu_item_system

    invoke-virtual {p0, v0}, Lcom/konka/systemsetting/MainActivity;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/LinearLayout;

    iput-object v0, p0, Lcom/konka/systemsetting/MainActivity;->m_itemSystem:Landroid/widget/LinearLayout;

    iget-object v0, p0, Lcom/konka/systemsetting/MainActivity;->m_itemSystem:Landroid/widget/LinearLayout;

    const v1, 0x7f090097    # com.konka.systemsetting.R.id.linearlayout_system_tab_languageandmusic

    invoke-virtual {v0, v1}, Landroid/widget/LinearLayout;->setNextFocusRightId(I)V

    const v0, 0x7f090032    # com.konka.systemsetting.R.id.sys_main_menu_item_individ

    invoke-virtual {p0, v0}, Lcom/konka/systemsetting/MainActivity;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/LinearLayout;

    iput-object v0, p0, Lcom/konka/systemsetting/MainActivity;->m_itemEnergy:Landroid/widget/LinearLayout;

    iget-object v0, p0, Lcom/konka/systemsetting/MainActivity;->m_itemEnergy:Landroid/widget/LinearLayout;

    const v1, 0x7f090021    # com.konka.systemsetting.R.id.linearlayout_individ_tab_energysaving

    invoke-virtual {v0, v1}, Landroid/widget/LinearLayout;->setNextFocusRightId(I)V

    const v0, 0x7f090033    # com.konka.systemsetting.R.id.sys_main_menu_item_storage

    invoke-virtual {p0, v0}, Lcom/konka/systemsetting/MainActivity;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/LinearLayout;

    iput-object v0, p0, Lcom/konka/systemsetting/MainActivity;->m_itemStorage:Landroid/widget/LinearLayout;

    iget-object v0, p0, Lcom/konka/systemsetting/MainActivity;->m_itemStorage:Landroid/widget/LinearLayout;

    const v1, 0x7f090085    # com.konka.systemsetting.R.id.linearlayout_storage_tab_flash

    invoke-virtual {v0, v1}, Landroid/widget/LinearLayout;->setNextFocusRightId(I)V

    return-void
.end method

.method private initSystem()V
    .locals 3

    new-instance v0, Landroid/content/Intent;

    invoke-direct {v0}, Landroid/content/Intent;-><init>()V

    const-string v1, "com.konka.passport.service.USERINFO_SERVICE"

    invoke-virtual {v0, v1}, Landroid/content/Intent;->setAction(Ljava/lang/String;)Landroid/content/Intent;

    iget-object v1, p0, Lcom/konka/systemsetting/MainActivity;->connPassport:Landroid/content/ServiceConnection;

    const/4 v2, 0x1

    invoke-virtual {p0, v0, v1, v2}, Lcom/konka/systemsetting/MainActivity;->bindService(Landroid/content/Intent;Landroid/content/ServiceConnection;I)Z

    move-result v1

    iput-boolean v1, p0, Lcom/konka/systemsetting/MainActivity;->bPassportConnectOK:Z

    return-void
.end method

.method private intentPage()V
    .locals 4

    invoke-virtual {p0}, Lcom/konka/systemsetting/MainActivity;->getIntent()Landroid/content/Intent;

    move-result-object v0

    const-string v2, "menu_name"

    invoke-virtual {v0, v2}, Landroid/content/Intent;->getStringExtra(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    new-instance v2, Ljava/lang/StringBuilder;

    const-string v3, "will show the menu====="

    invoke-direct {v2, v3}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v2, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v2}, Lcom/konka/debuginfo/logPrint;->Debug(Ljava/lang/String;)V

    if-nez v1, :cond_0

    invoke-direct {p0}, Lcom/konka/systemsetting/MainActivity;->getIntentPageByAction()Ljava/lang/String;

    move-result-object v1

    :cond_0
    if-eqz v1, :cond_1

    invoke-direct {p0, v1}, Lcom/konka/systemsetting/MainActivity;->locatePage(Ljava/lang/String;)V

    :cond_1
    return-void
.end method

.method private locatePage(Ljava/lang/String;)V
    .locals 7
    .param p1    # Ljava/lang/String;

    const/4 v6, 0x3

    const/4 v5, 0x2

    const/4 v4, 0x0

    const/4 v3, 0x1

    const-string v2, "net_lan"

    invoke-virtual {v2, p1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_0

    invoke-direct {p0, v4, v4}, Lcom/konka/systemsetting/MainActivity;->showPage(II)V

    iget-object v2, p0, Lcom/konka/systemsetting/MainActivity;->mViewHolder:Lcom/konka/systemsetting/ViewHolder;

    iget-object v2, v2, Lcom/konka/systemsetting/ViewHolder;->linearlayout_tabWiredSettings:Landroid/widget/LinearLayout;

    invoke-virtual {v2}, Landroid/widget/LinearLayout;->requestFocus()Z

    :goto_0
    return-void

    :cond_0
    const-string v2, "net_wireless"

    invoke-virtual {v2, p1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_1

    invoke-direct {p0, v4, v3}, Lcom/konka/systemsetting/MainActivity;->showPage(II)V

    iget-object v2, p0, Lcom/konka/systemsetting/MainActivity;->mViewHolder:Lcom/konka/systemsetting/ViewHolder;

    iget-object v2, v2, Lcom/konka/systemsetting/ViewHolder;->linearlayout_tabWirelessSettings:Landroid/widget/LinearLayout;

    invoke-virtual {v2}, Landroid/widget/LinearLayout;->requestFocus()Z

    goto :goto_0

    :cond_1
    const-string v2, "net_PPOE"

    invoke-virtual {v2, p1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_2

    invoke-direct {p0, v4, v5}, Lcom/konka/systemsetting/MainActivity;->showPage(II)V

    iget-object v2, p0, Lcom/konka/systemsetting/MainActivity;->mViewHolder:Lcom/konka/systemsetting/ViewHolder;

    iget-object v2, v2, Lcom/konka/systemsetting/ViewHolder;->linearlayout_tabPPoeSettings:Landroid/widget/LinearLayout;

    invoke-virtual {v2}, Landroid/widget/LinearLayout;->requestFocus()Z

    goto :goto_0

    :cond_2
    const-string v2, "system_language"

    invoke-virtual {v2, p1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_3

    invoke-direct {p0, v3, v4}, Lcom/konka/systemsetting/MainActivity;->showPage(II)V

    const v2, 0x7f090092    # com.konka.systemsetting.R.id.sys_system_item_language

    invoke-virtual {p0, v2}, Lcom/konka/systemsetting/MainActivity;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/LinearLayout;

    invoke-virtual {v0}, Landroid/widget/LinearLayout;->requestFocus()Z

    goto :goto_0

    :cond_3
    const-string v2, "system_microphone"

    invoke-virtual {v2, p1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_4

    invoke-direct {p0, v3, v4}, Lcom/konka/systemsetting/MainActivity;->showPage(II)V

    const v2, 0x7f090093    # com.konka.systemsetting.R.id.sys_system_item_mic

    invoke-virtual {p0, v2}, Lcom/konka/systemsetting/MainActivity;->findViewById(I)Landroid/view/View;

    move-result-object v1

    check-cast v1, Landroid/widget/LinearLayout;

    invoke-virtual {v1}, Landroid/widget/LinearLayout;->requestFocus()Z

    goto :goto_0

    :cond_4
    const-string v2, "system_info"

    invoke-virtual {v2, p1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_5

    invoke-direct {p0, v3, v5}, Lcom/konka/systemsetting/MainActivity;->showPage(II)V

    iget-object v2, p0, Lcom/konka/systemsetting/MainActivity;->mViewHolder:Lcom/konka/systemsetting/ViewHolder;

    iget-object v2, v2, Lcom/konka/systemsetting/ViewHolder;->linearlayout_tabSystemInfo:Landroid/widget/LinearLayout;

    invoke-virtual {v2}, Landroid/widget/LinearLayout;->requestFocus()Z

    goto :goto_0

    :cond_5
    const-string v2, "system_input"

    invoke-virtual {v2, p1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_6

    invoke-direct {p0, v3, v3}, Lcom/konka/systemsetting/MainActivity;->showPage(II)V

    iget-object v2, p0, Lcom/konka/systemsetting/MainActivity;->mViewHolder:Lcom/konka/systemsetting/ViewHolder;

    iget-object v2, v2, Lcom/konka/systemsetting/ViewHolder;->linearlayout_tabInputSettings:Landroid/widget/LinearLayout;

    invoke-virtual {v2}, Landroid/widget/LinearLayout;->requestFocus()Z

    goto :goto_0

    :cond_6
    const-string v2, "system_reset"

    invoke-virtual {v2, p1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_7

    const/4 v2, 0x5

    invoke-direct {p0, v3, v2}, Lcom/konka/systemsetting/MainActivity;->showPage(II)V

    iget-object v2, p0, Lcom/konka/systemsetting/MainActivity;->mViewHolder:Lcom/konka/systemsetting/ViewHolder;

    iget-object v2, v2, Lcom/konka/systemsetting/ViewHolder;->linearlayout_tabFactoryReset:Landroid/widget/LinearLayout;

    invoke-virtual {v2}, Landroid/widget/LinearLayout;->requestFocus()Z

    goto/16 :goto_0

    :cond_7
    const-string v2, "system_upgrade"

    invoke-virtual {v2, p1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_8

    invoke-direct {p0, v3, v6}, Lcom/konka/systemsetting/MainActivity;->showPage(II)V

    iget-object v2, p0, Lcom/konka/systemsetting/MainActivity;->mViewHolder:Lcom/konka/systemsetting/ViewHolder;

    iget-object v2, v2, Lcom/konka/systemsetting/ViewHolder;->linearlayout_tabSoftUpgrade:Landroid/widget/LinearLayout;

    invoke-virtual {v2}, Landroid/widget/LinearLayout;->requestFocus()Z

    goto/16 :goto_0

    :cond_8
    const-string v2, "individual_timing"

    invoke-virtual {v2, p1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_9

    invoke-direct {p0, v5, v3}, Lcom/konka/systemsetting/MainActivity;->showPage(II)V

    iget-object v2, p0, Lcom/konka/systemsetting/MainActivity;->mViewHolder:Lcom/konka/systemsetting/ViewHolder;

    iget-object v2, v2, Lcom/konka/systemsetting/ViewHolder;->linearlayout_tabTimeSetting:Landroid/widget/LinearLayout;

    invoke-virtual {v2}, Landroid/widget/LinearLayout;->requestFocus()Z

    goto/16 :goto_0

    :cond_9
    const-string v2, "individual_energy"

    invoke-virtual {v2, p1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_a

    invoke-direct {p0, v5, v4}, Lcom/konka/systemsetting/MainActivity;->showPage(II)V

    iget-object v2, p0, Lcom/konka/systemsetting/MainActivity;->mViewHolder:Lcom/konka/systemsetting/ViewHolder;

    iget-object v2, v2, Lcom/konka/systemsetting/ViewHolder;->linearlayout_tabEnergySaving:Landroid/widget/LinearLayout;

    invoke-virtual {v2}, Landroid/widget/LinearLayout;->requestFocus()Z

    goto/16 :goto_0

    :cond_a
    const-string v2, "storage_ram"

    invoke-virtual {v2, p1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_b

    invoke-direct {p0, v6, v4}, Lcom/konka/systemsetting/MainActivity;->showPage(II)V

    iget-object v2, p0, Lcom/konka/systemsetting/MainActivity;->mViewHolder:Lcom/konka/systemsetting/ViewHolder;

    iget-object v2, v2, Lcom/konka/systemsetting/ViewHolder;->linearlayout_tabFlash:Landroid/widget/LinearLayout;

    invoke-virtual {v2}, Landroid/widget/LinearLayout;->requestFocus()Z

    goto/16 :goto_0

    :cond_b
    const-string v2, "storage_sd"

    invoke-virtual {v2, p1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_c

    invoke-direct {p0, v6, v3}, Lcom/konka/systemsetting/MainActivity;->showPage(II)V

    iget-object v2, p0, Lcom/konka/systemsetting/MainActivity;->mViewHolder:Lcom/konka/systemsetting/ViewHolder;

    iget-object v2, v2, Lcom/konka/systemsetting/ViewHolder;->linearlayout_tabSdCard:Landroid/widget/LinearLayout;

    invoke-virtual {v2}, Landroid/widget/LinearLayout;->requestFocus()Z

    goto/16 :goto_0

    :cond_c
    const-string v2, "no right page to show"

    invoke-static {v2}, Lcom/konka/debuginfo/logPrint;->Debug(Ljava/lang/String;)V

    goto/16 :goto_0
.end method

.method private showPage(II)V
    .locals 2
    .param p1    # I
    .param p2    # I

    iget-object v0, p0, Lcom/konka/systemsetting/MainActivity;->vfMainMenu:Landroid/widget/ViewFlipper;

    invoke-virtual {v0, p1}, Landroid/widget/ViewFlipper;->setDisplayedChild(I)V

    packed-switch p1, :pswitch_data_0

    :cond_0
    :goto_0
    return-void

    :pswitch_0
    iget-object v0, p0, Lcom/konka/systemsetting/MainActivity;->m_networkPageManager:Lcom/konka/systemsetting/net/NetworkPageManager;

    if-nez v0, :cond_0

    new-instance v0, Lcom/konka/systemsetting/net/NetworkPageManager;

    iget-object v1, p0, Lcom/konka/systemsetting/MainActivity;->mViewHolder:Lcom/konka/systemsetting/ViewHolder;

    invoke-direct {v0, p0, v1, p2}, Lcom/konka/systemsetting/net/NetworkPageManager;-><init>(Lcom/konka/systemsetting/MainActivity;Lcom/konka/systemsetting/ViewHolder;I)V

    iput-object v0, p0, Lcom/konka/systemsetting/MainActivity;->m_networkPageManager:Lcom/konka/systemsetting/net/NetworkPageManager;

    goto :goto_0

    :pswitch_1
    iget-object v0, p0, Lcom/konka/systemsetting/MainActivity;->m_systemPageManager:Lcom/konka/systemsetting/system/SystemPageManager;

    if-nez v0, :cond_0

    new-instance v0, Lcom/konka/systemsetting/system/SystemPageManager;

    invoke-direct {v0, p0, p2}, Lcom/konka/systemsetting/system/SystemPageManager;-><init>(Lcom/konka/systemsetting/MainActivity;I)V

    iput-object v0, p0, Lcom/konka/systemsetting/MainActivity;->m_systemPageManager:Lcom/konka/systemsetting/system/SystemPageManager;

    goto :goto_0

    :pswitch_2
    iget-object v0, p0, Lcom/konka/systemsetting/MainActivity;->m_individPageManager:Lcom/konka/systemsetting/individ/IndividPageManager;

    if-nez v0, :cond_0

    new-instance v0, Lcom/konka/systemsetting/individ/IndividPageManager;

    invoke-direct {v0, p0, p2}, Lcom/konka/systemsetting/individ/IndividPageManager;-><init>(Lcom/konka/systemsetting/MainActivity;I)V

    iput-object v0, p0, Lcom/konka/systemsetting/MainActivity;->m_individPageManager:Lcom/konka/systemsetting/individ/IndividPageManager;

    goto :goto_0

    :pswitch_3
    iget-object v0, p0, Lcom/konka/systemsetting/MainActivity;->m_storagePgaeManager:Lcom/konka/systemsetting/storage/StoragePageManager;

    if-nez v0, :cond_0

    new-instance v0, Lcom/konka/systemsetting/storage/StoragePageManager;

    invoke-direct {v0, p0, p2}, Lcom/konka/systemsetting/storage/StoragePageManager;-><init>(Lcom/konka/systemsetting/MainActivity;I)V

    iput-object v0, p0, Lcom/konka/systemsetting/MainActivity;->m_storagePgaeManager:Lcom/konka/systemsetting/storage/StoragePageManager;

    goto :goto_0

    nop

    :pswitch_data_0
    .packed-switch 0x0
        :pswitch_0
        :pswitch_1
        :pswitch_2
        :pswitch_3
    .end packed-switch
.end method


# virtual methods
.method public getSN()Ljava/lang/String;
    .locals 1

    invoke-direct {p0}, Lcom/konka/systemsetting/MainActivity;->getSnStr()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public getSwitchImages()V
    .locals 2

    const v1, 0x7f060001    # com.konka.systemsetting.R.string.lang_flag

    invoke-virtual {p0, v1}, Lcom/konka/systemsetting/MainActivity;->getText(I)Ljava/lang/CharSequence;

    move-result-object v1

    invoke-interface {v1}, Ljava/lang/CharSequence;->toString()Ljava/lang/String;

    move-result-object v0

    const-string v1, "zh_cn"

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_0

    const v1, 0x7f020011    # com.konka.systemsetting.R.drawable.on

    iput v1, p0, Lcom/konka/systemsetting/MainActivity;->IMG_SWITCH_ON:I

    const v1, 0x7f020010    # com.konka.systemsetting.R.drawable.off

    iput v1, p0, Lcom/konka/systemsetting/MainActivity;->IMG_SWITCH_OFF:I

    :goto_0
    return-void

    :cond_0
    const-string v1, "zh_tw"

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_1

    const v1, 0x7f020046    # com.konka.systemsetting.R.drawable.zhtw_on

    iput v1, p0, Lcom/konka/systemsetting/MainActivity;->IMG_SWITCH_ON:I

    const v1, 0x7f020045    # com.konka.systemsetting.R.drawable.zhtw_off

    iput v1, p0, Lcom/konka/systemsetting/MainActivity;->IMG_SWITCH_OFF:I

    goto :goto_0

    :cond_1
    const v1, 0x7f020003    # com.konka.systemsetting.R.drawable.en_on

    iput v1, p0, Lcom/konka/systemsetting/MainActivity;->IMG_SWITCH_ON:I

    const v1, 0x7f020002    # com.konka.systemsetting.R.drawable.en_off

    iput v1, p0, Lcom/konka/systemsetting/MainActivity;->IMG_SWITCH_OFF:I

    goto :goto_0
.end method

.method public hideDialog(I)V
    .locals 1
    .param p1    # I

    packed-switch p1, :pswitch_data_0

    :cond_0
    :goto_0
    return-void

    :pswitch_0
    iget-object v0, p0, Lcom/konka/systemsetting/MainActivity;->mHangingDialog:Landroid/app/ProgressDialog;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/konka/systemsetting/MainActivity;->mHangingDialog:Landroid/app/ProgressDialog;

    invoke-virtual {v0}, Landroid/app/ProgressDialog;->dismiss()V

    goto :goto_0

    :pswitch_data_0
    .packed-switch 0x31
        :pswitch_0
    .end packed-switch
.end method

.method public onCreate(Landroid/os/Bundle;)V
    .locals 1
    .param p1    # Landroid/os/Bundle;

    invoke-super {p0, p1}, Landroid/app/Activity;->onCreate(Landroid/os/Bundle;)V

    const v0, 0x7f030023    # com.konka.systemsetting.R.layout.system_home_page_layout

    invoke-virtual {p0, v0}, Lcom/konka/systemsetting/MainActivity;->setContentView(I)V

    const v0, 0x7f0900ae    # com.konka.systemsetting.R.id.sys_main_menu_view_flipper

    invoke-virtual {p0, v0}, Lcom/konka/systemsetting/MainActivity;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/ViewFlipper;

    iput-object v0, p0, Lcom/konka/systemsetting/MainActivity;->vfMainMenu:Landroid/widget/ViewFlipper;

    new-instance v0, Lcom/konka/systemsetting/ViewHolder;

    invoke-direct {v0, p0}, Lcom/konka/systemsetting/ViewHolder;-><init>(Lcom/konka/systemsetting/MainActivity;)V

    iput-object v0, p0, Lcom/konka/systemsetting/MainActivity;->mViewHolder:Lcom/konka/systemsetting/ViewHolder;

    iget-object v0, p0, Lcom/konka/systemsetting/MainActivity;->mViewHolder:Lcom/konka/systemsetting/ViewHolder;

    invoke-virtual {v0}, Lcom/konka/systemsetting/ViewHolder;->findViewForPages()V

    invoke-direct {p0}, Lcom/konka/systemsetting/MainActivity;->initSystem()V

    invoke-direct {p0}, Lcom/konka/systemsetting/MainActivity;->initItems()V

    invoke-direct {p0}, Lcom/konka/systemsetting/MainActivity;->SetOnFocusChangeListener()V

    invoke-virtual {p0}, Lcom/konka/systemsetting/MainActivity;->getSwitchImages()V

    invoke-direct {p0}, Lcom/konka/systemsetting/MainActivity;->intentPage()V

    return-void
.end method

.method protected onCreateDialog(I)Landroid/app/Dialog;
    .locals 3
    .param p1    # I

    const/4 v2, 0x1

    packed-switch p1, :pswitch_data_0

    const/4 v0, 0x0

    :goto_0
    return-object v0

    :pswitch_0
    new-instance v0, Landroid/app/ProgressDialog;

    invoke-direct {v0, p0}, Landroid/app/ProgressDialog;-><init>(Landroid/content/Context;)V

    iput-object v0, p0, Lcom/konka/systemsetting/MainActivity;->mHangingDialog:Landroid/app/ProgressDialog;

    iget-object v0, p0, Lcom/konka/systemsetting/MainActivity;->mHangingDialog:Landroid/app/ProgressDialog;

    const v1, 0x7f06000a    # com.konka.systemsetting.R.string.str_individpage_timesetting

    invoke-virtual {v0, v1}, Landroid/app/ProgressDialog;->setTitle(I)V

    iget-object v0, p0, Lcom/konka/systemsetting/MainActivity;->mHangingDialog:Landroid/app/ProgressDialog;

    const v1, 0x7f020022    # com.konka.systemsetting.R.drawable.syssettingfirsticonpersonalunfocus

    invoke-virtual {v0, v1}, Landroid/app/ProgressDialog;->setIcon(I)V

    iget-object v0, p0, Lcom/konka/systemsetting/MainActivity;->mHangingDialog:Landroid/app/ProgressDialog;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Landroid/app/ProgressDialog;->setProgressStyle(I)V

    iget-object v0, p0, Lcom/konka/systemsetting/MainActivity;->mHangingDialog:Landroid/app/ProgressDialog;

    invoke-virtual {v0, v2}, Landroid/app/ProgressDialog;->setIndeterminate(Z)V

    iget-object v0, p0, Lcom/konka/systemsetting/MainActivity;->mHangingDialog:Landroid/app/ProgressDialog;

    invoke-virtual {v0, v2}, Landroid/app/ProgressDialog;->setCancelable(Z)V

    iget-object v0, p0, Lcom/konka/systemsetting/MainActivity;->mHangingDialog:Landroid/app/ProgressDialog;

    const v1, 0x7f060010    # com.konka.systemsetting.R.string.str_individ_timesetting_initing

    invoke-virtual {p0, v1}, Lcom/konka/systemsetting/MainActivity;->getText(I)Ljava/lang/CharSequence;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/app/ProgressDialog;->setMessage(Ljava/lang/CharSequence;)V

    iget-object v0, p0, Lcom/konka/systemsetting/MainActivity;->mHangingDialog:Landroid/app/ProgressDialog;

    const v1, 0x7f06000d    # com.konka.systemsetting.R.string.str_individ_dialog_cancel

    invoke-virtual {p0, v1}, Lcom/konka/systemsetting/MainActivity;->getText(I)Ljava/lang/CharSequence;

    move-result-object v1

    new-instance v2, Lcom/konka/systemsetting/MainActivity$3;

    invoke-direct {v2, p0}, Lcom/konka/systemsetting/MainActivity$3;-><init>(Lcom/konka/systemsetting/MainActivity;)V

    invoke-virtual {v0, v1, v2}, Landroid/app/ProgressDialog;->setButton(Ljava/lang/CharSequence;Landroid/content/DialogInterface$OnClickListener;)V

    iget-object v0, p0, Lcom/konka/systemsetting/MainActivity;->mHangingDialog:Landroid/app/ProgressDialog;

    goto :goto_0

    nop

    :pswitch_data_0
    .packed-switch 0x31
        :pswitch_0
    .end packed-switch
.end method

.method public onDestroy()V
    .locals 1

    const-string v0, "com.konka.syssetting.MainActivity.onDestroy()"

    invoke-static {v0}, Lcom/konka/debuginfo/logPrint;->Debug(Ljava/lang/String;)V

    iget-object v0, p0, Lcom/konka/systemsetting/MainActivity;->m_storagePgaeManager:Lcom/konka/systemsetting/storage/StoragePageManager;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/konka/systemsetting/MainActivity;->m_storagePgaeManager:Lcom/konka/systemsetting/storage/StoragePageManager;

    invoke-virtual {v0}, Lcom/konka/systemsetting/storage/StoragePageManager;->OnDestory()V

    :cond_0
    iget-object v0, p0, Lcom/konka/systemsetting/MainActivity;->m_individPageManager:Lcom/konka/systemsetting/individ/IndividPageManager;

    if-eqz v0, :cond_1

    iget-object v0, p0, Lcom/konka/systemsetting/MainActivity;->m_individPageManager:Lcom/konka/systemsetting/individ/IndividPageManager;

    invoke-virtual {v0}, Lcom/konka/systemsetting/individ/IndividPageManager;->onDestroy()V

    :cond_1
    invoke-super {p0}, Landroid/app/Activity;->onDestroy()V

    iget-object v0, p0, Lcom/konka/systemsetting/MainActivity;->m_networkPageManager:Lcom/konka/systemsetting/net/NetworkPageManager;

    if-eqz v0, :cond_2

    iget-object v0, p0, Lcom/konka/systemsetting/MainActivity;->m_networkPageManager:Lcom/konka/systemsetting/net/NetworkPageManager;

    invoke-virtual {v0}, Lcom/konka/systemsetting/net/NetworkPageManager;->onDestroy()V

    :cond_2
    return-void
.end method

.method protected onNewIntent(Landroid/content/Intent;)V
    .locals 0
    .param p1    # Landroid/content/Intent;

    invoke-super {p0, p1}, Landroid/app/Activity;->onNewIntent(Landroid/content/Intent;)V

    invoke-virtual {p0, p1}, Lcom/konka/systemsetting/MainActivity;->setIntent(Landroid/content/Intent;)V

    return-void
.end method

.method protected onPause()V
    .locals 1

    const-string v0, "onPause"

    invoke-static {v0}, Lcom/konka/debuginfo/logPrint;->Debug(Ljava/lang/String;)V

    iget-object v0, p0, Lcom/konka/systemsetting/MainActivity;->m_individPageManager:Lcom/konka/systemsetting/individ/IndividPageManager;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/konka/systemsetting/MainActivity;->m_individPageManager:Lcom/konka/systemsetting/individ/IndividPageManager;

    invoke-virtual {v0}, Lcom/konka/systemsetting/individ/IndividPageManager;->onPause()V

    :cond_0
    invoke-super {p0}, Landroid/app/Activity;->onPause()V

    return-void
.end method

.method protected onRestart()V
    .locals 0

    invoke-super {p0}, Landroid/app/Activity;->onRestart()V

    invoke-direct {p0}, Lcom/konka/systemsetting/MainActivity;->intentPage()V

    return-void
.end method

.method protected onResume()V
    .locals 1

    const-string v0, "OnResume"

    invoke-static {v0}, Lcom/konka/debuginfo/logPrint;->Debug(Ljava/lang/String;)V

    iget-object v0, p0, Lcom/konka/systemsetting/MainActivity;->m_individPageManager:Lcom/konka/systemsetting/individ/IndividPageManager;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/konka/systemsetting/MainActivity;->m_individPageManager:Lcom/konka/systemsetting/individ/IndividPageManager;

    invoke-virtual {v0}, Lcom/konka/systemsetting/individ/IndividPageManager;->onResume()V

    :cond_0
    invoke-super {p0}, Landroid/app/Activity;->onResume()V

    return-void
.end method

.method protected onStart()V
    .locals 1

    invoke-static {}, Lcom/konka/systemsetting/SysRootApp;->getTvDeskProvider()Lcom/konka/kkinterface/tv/TvDeskProvider;

    move-result-object v0

    invoke-interface {v0}, Lcom/konka/kkinterface/tv/TvDeskProvider;->getDataBaseManagerInstance()Lcom/konka/kkinterface/tv/DataBaseDesk;

    move-result-object v0

    invoke-interface {v0}, Lcom/konka/kkinterface/tv/DataBaseDesk;->SyncUserSettingDB()V

    invoke-super {p0}, Landroid/app/Activity;->onStart()V

    return-void
.end method

.method public onStop()V
    .locals 1

    invoke-super {p0}, Landroid/app/Activity;->onStop()V

    iget-boolean v0, p0, Lcom/konka/systemsetting/MainActivity;->bPassportConnectOK:Z

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/konka/systemsetting/MainActivity;->connPassport:Landroid/content/ServiceConnection;

    invoke-virtual {p0, v0}, Lcom/konka/systemsetting/MainActivity;->unbindService(Landroid/content/ServiceConnection;)V

    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/konka/systemsetting/MainActivity;->bPassportConnectOK:Z

    :cond_0
    iget-object v0, p0, Lcom/konka/systemsetting/MainActivity;->m_networkPageManager:Lcom/konka/systemsetting/net/NetworkPageManager;

    if-eqz v0, :cond_1

    iget-object v0, p0, Lcom/konka/systemsetting/MainActivity;->m_networkPageManager:Lcom/konka/systemsetting/net/NetworkPageManager;

    invoke-virtual {v0}, Lcom/konka/systemsetting/net/NetworkPageManager;->onDestroy()V

    :cond_1
    iget-object v0, p0, Lcom/konka/systemsetting/MainActivity;->m_individPageManager:Lcom/konka/systemsetting/individ/IndividPageManager;

    if-eqz v0, :cond_2

    iget-object v0, p0, Lcom/konka/systemsetting/MainActivity;->m_individPageManager:Lcom/konka/systemsetting/individ/IndividPageManager;

    invoke-virtual {v0}, Lcom/konka/systemsetting/individ/IndividPageManager;->onDestroy()V

    :cond_2
    return-void
.end method

.method public toastText(I)V
    .locals 4
    .param p1    # I

    const/4 v3, 0x0

    invoke-static {p0, p1, v3}, Landroid/widget/Toast;->makeText(Landroid/content/Context;II)Landroid/widget/Toast;

    move-result-object v0

    const/16 v1, 0x51

    const/16 v2, 0x64

    invoke-virtual {v0, v1, v3, v2}, Landroid/widget/Toast;->setGravity(III)V

    invoke-virtual {v0}, Landroid/widget/Toast;->show()V

    return-void
.end method

.method public toastText(III)V
    .locals 6
    .param p1    # I
    .param p2    # I
    .param p3    # I

    const/4 v4, -0x2

    new-instance v0, Landroid/widget/LinearLayout;

    invoke-direct {v0, p0}, Landroid/widget/LinearLayout;-><init>(Landroid/content/Context;)V

    new-instance v3, Landroid/view/ViewGroup$LayoutParams;

    invoke-direct {v3, v4, v4}, Landroid/view/ViewGroup$LayoutParams;-><init>(II)V

    invoke-virtual {v0, v3}, Landroid/widget/LinearLayout;->setLayoutParams(Landroid/view/ViewGroup$LayoutParams;)V

    const v3, 0x7f020035    # com.konka.systemsetting.R.drawable.toast_bg

    invoke-virtual {v0, v3}, Landroid/widget/LinearLayout;->setBackgroundResource(I)V

    new-instance v1, Landroid/widget/TextView;

    invoke-direct {v1, p0}, Landroid/widget/TextView;-><init>(Landroid/content/Context;)V

    invoke-virtual {v1, p1}, Landroid/widget/TextView;->setText(I)V

    new-instance v3, Landroid/view/ViewGroup$LayoutParams;

    const/16 v4, 0x1c2

    const/16 v5, 0xa0

    invoke-direct {v3, v4, v5}, Landroid/view/ViewGroup$LayoutParams;-><init>(II)V

    invoke-virtual {v1, v3}, Landroid/widget/TextView;->setLayoutParams(Landroid/view/ViewGroup$LayoutParams;)V

    invoke-virtual {v0, v1}, Landroid/widget/LinearLayout;->addView(Landroid/view/View;)V

    new-instance v2, Landroid/widget/Toast;

    invoke-direct {v2, p0}, Landroid/widget/Toast;-><init>(Landroid/content/Context;)V

    const/16 v3, 0x3e8

    invoke-virtual {v2, v3}, Landroid/widget/Toast;->setDuration(I)V

    invoke-virtual {v2, v0}, Landroid/widget/Toast;->setView(Landroid/view/View;)V

    const/16 v3, 0x50

    invoke-virtual {v2, v3, p2, p3}, Landroid/widget/Toast;->setGravity(III)V

    invoke-virtual {v2}, Landroid/widget/Toast;->show()V

    return-void
.end method

.method public toastText(Ljava/lang/String;)V
    .locals 4
    .param p1    # Ljava/lang/String;

    const/16 v1, 0xc8

    invoke-static {p0, p1, v1}, Landroid/widget/Toast;->makeText(Landroid/content/Context;Ljava/lang/CharSequence;I)Landroid/widget/Toast;

    move-result-object v0

    const/16 v1, 0x51

    const/4 v2, 0x0

    const/16 v3, 0x64

    invoke-virtual {v0, v1, v2, v3}, Landroid/widget/Toast;->setGravity(III)V

    invoke-virtual {v0}, Landroid/widget/Toast;->show()V

    return-void
.end method
