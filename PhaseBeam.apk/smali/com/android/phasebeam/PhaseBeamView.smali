.class public Lcom/android/phasebeam/PhaseBeamView;
.super Landroid/renderscript/RSSurfaceView;
.source "PhaseBeamView.java"


# instance fields
.field private mRS:Landroid/renderscript/RenderScriptGL;

.field private mRender:Lcom/android/phasebeam/PhaseBeamRS;


# direct methods
.method public constructor <init>(Landroid/content/Context;)V
    .locals 1
    .param p1    # Landroid/content/Context;

    const/4 v0, 0x1

    invoke-direct {p0, p1}, Landroid/renderscript/RSSurfaceView;-><init>(Landroid/content/Context;)V

    invoke-virtual {p0, v0}, Lcom/android/phasebeam/PhaseBeamView;->setFocusable(Z)V

    invoke-virtual {p0, v0}, Lcom/android/phasebeam/PhaseBeamView;->setFocusableInTouchMode(Z)V

    return-void
.end method


# virtual methods
.method protected onDetachedFromWindow()V
    .locals 3

    const/4 v2, 0x0

    const/4 v1, 0x0

    iget-object v0, p0, Lcom/android/phasebeam/PhaseBeamView;->mRS:Landroid/renderscript/RenderScriptGL;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/android/phasebeam/PhaseBeamView;->mRS:Landroid/renderscript/RenderScriptGL;

    invoke-virtual {v0, v2, v1, v1}, Landroid/renderscript/RenderScriptGL;->setSurface(Landroid/view/SurfaceHolder;II)V

    iput-object v2, p0, Lcom/android/phasebeam/PhaseBeamView;->mRS:Landroid/renderscript/RenderScriptGL;

    invoke-virtual {p0}, Lcom/android/phasebeam/PhaseBeamView;->destroyRenderScriptGL()V

    :cond_0
    return-void
.end method

.method public surfaceChanged(Landroid/view/SurfaceHolder;III)V
    .locals 7
    .param p1    # Landroid/view/SurfaceHolder;
    .param p2    # I
    .param p3    # I
    .param p4    # I

    invoke-super {p0, p1, p2, p3, p4}, Landroid/renderscript/RSSurfaceView;->surfaceChanged(Landroid/view/SurfaceHolder;III)V

    iget-object v0, p0, Lcom/android/phasebeam/PhaseBeamView;->mRS:Landroid/renderscript/RenderScriptGL;

    if-nez v0, :cond_0

    new-instance v6, Landroid/renderscript/RenderScriptGL$SurfaceConfig;

    invoke-direct {v6}, Landroid/renderscript/RenderScriptGL$SurfaceConfig;-><init>()V

    invoke-virtual {p0, v6}, Lcom/android/phasebeam/PhaseBeamView;->createRenderScriptGL(Landroid/renderscript/RenderScriptGL$SurfaceConfig;)Landroid/renderscript/RenderScriptGL;

    move-result-object v0

    iput-object v0, p0, Lcom/android/phasebeam/PhaseBeamView;->mRS:Landroid/renderscript/RenderScriptGL;

    iget-object v0, p0, Lcom/android/phasebeam/PhaseBeamView;->mRS:Landroid/renderscript/RenderScriptGL;

    invoke-virtual {v0, p1, p3, p4}, Landroid/renderscript/RenderScriptGL;->setSurface(Landroid/view/SurfaceHolder;II)V

    new-instance v0, Lcom/android/phasebeam/PhaseBeamRS;

    invoke-direct {v0}, Lcom/android/phasebeam/PhaseBeamRS;-><init>()V

    iput-object v0, p0, Lcom/android/phasebeam/PhaseBeamView;->mRender:Lcom/android/phasebeam/PhaseBeamRS;

    iget-object v0, p0, Lcom/android/phasebeam/PhaseBeamView;->mRender:Lcom/android/phasebeam/PhaseBeamRS;

    const/16 v1, 0xf0

    iget-object v2, p0, Lcom/android/phasebeam/PhaseBeamView;->mRS:Landroid/renderscript/RenderScriptGL;

    invoke-virtual {p0}, Lcom/android/phasebeam/PhaseBeamView;->getResources()Landroid/content/res/Resources;

    move-result-object v3

    move v4, p3

    move v5, p4

    invoke-virtual/range {v0 .. v5}, Lcom/android/phasebeam/PhaseBeamRS;->init(ILandroid/renderscript/RenderScriptGL;Landroid/content/res/Resources;II)V

    :cond_0
    return-void
.end method
