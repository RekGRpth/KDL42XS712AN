.class public Lcom/android/phasebeam/ScriptC_phasebeam;
.super Landroid/renderscript/ScriptC;
.source "ScriptC_phasebeam.java"


# instance fields
.field private __ALLOCATION:Landroid/renderscript/Element;

.field private __F32:Landroid/renderscript/Element;

.field private __MESH:Landroid/renderscript/Element;

.field private __PROGRAM_FRAGMENT:Landroid/renderscript/Element;

.field private __PROGRAM_VERTEX:Landroid/renderscript/Element;

.field private mExportVar_beamMesh:Landroid/renderscript/Mesh;

.field private mExportVar_beamParticles:Lcom/android/phasebeam/ScriptField_Particle;

.field private mExportVar_densityDPI:F

.field private mExportVar_dotMesh:Landroid/renderscript/Mesh;

.field private mExportVar_dotParticles:Lcom/android/phasebeam/ScriptField_Particle;

.field private mExportVar_fragBg:Landroid/renderscript/ProgramFragment;

.field private mExportVar_fragDots:Landroid/renderscript/ProgramFragment;

.field private mExportVar_gBackgroundMesh:Landroid/renderscript/Mesh;

.field private mExportVar_textureBeam:Landroid/renderscript/Allocation;

.field private mExportVar_textureDot:Landroid/renderscript/Allocation;

.field private mExportVar_vertBg:Landroid/renderscript/ProgramVertex;

.field private mExportVar_vertDots:Landroid/renderscript/ProgramVertex;

.field private mExportVar_vertexColors:Lcom/android/phasebeam/ScriptField_VertexColor_s;

.field private mExportVar_xOffset:F


# direct methods
.method public constructor <init>(Landroid/renderscript/RenderScript;Landroid/content/res/Resources;I)V
    .locals 1
    .param p1    # Landroid/renderscript/RenderScript;
    .param p2    # Landroid/content/res/Resources;
    .param p3    # I

    invoke-direct {p0, p1, p2, p3}, Landroid/renderscript/ScriptC;-><init>(Landroid/renderscript/RenderScript;Landroid/content/res/Resources;I)V

    invoke-static {p1}, Landroid/renderscript/Element;->ALLOCATION(Landroid/renderscript/RenderScript;)Landroid/renderscript/Element;

    move-result-object v0

    iput-object v0, p0, Lcom/android/phasebeam/ScriptC_phasebeam;->__ALLOCATION:Landroid/renderscript/Element;

    invoke-static {p1}, Landroid/renderscript/Element;->PROGRAM_VERTEX(Landroid/renderscript/RenderScript;)Landroid/renderscript/Element;

    move-result-object v0

    iput-object v0, p0, Lcom/android/phasebeam/ScriptC_phasebeam;->__PROGRAM_VERTEX:Landroid/renderscript/Element;

    invoke-static {p1}, Landroid/renderscript/Element;->PROGRAM_FRAGMENT(Landroid/renderscript/RenderScript;)Landroid/renderscript/Element;

    move-result-object v0

    iput-object v0, p0, Lcom/android/phasebeam/ScriptC_phasebeam;->__PROGRAM_FRAGMENT:Landroid/renderscript/Element;

    invoke-static {p1}, Landroid/renderscript/Element;->MESH(Landroid/renderscript/RenderScript;)Landroid/renderscript/Element;

    move-result-object v0

    iput-object v0, p0, Lcom/android/phasebeam/ScriptC_phasebeam;->__MESH:Landroid/renderscript/Element;

    invoke-static {p1}, Landroid/renderscript/Element;->F32(Landroid/renderscript/RenderScript;)Landroid/renderscript/Element;

    move-result-object v0

    iput-object v0, p0, Lcom/android/phasebeam/ScriptC_phasebeam;->__F32:Landroid/renderscript/Element;

    const/high16 v0, 0x3f000000    # 0.5f

    iput v0, p0, Lcom/android/phasebeam/ScriptC_phasebeam;->mExportVar_xOffset:F

    return-void
.end method


# virtual methods
.method public bind_beamParticles(Lcom/android/phasebeam/ScriptField_Particle;)V
    .locals 2
    .param p1    # Lcom/android/phasebeam/ScriptField_Particle;

    const/16 v1, 0x9

    iput-object p1, p0, Lcom/android/phasebeam/ScriptC_phasebeam;->mExportVar_beamParticles:Lcom/android/phasebeam/ScriptField_Particle;

    if-nez p1, :cond_0

    const/4 v0, 0x0

    invoke-virtual {p0, v0, v1}, Lcom/android/phasebeam/ScriptC_phasebeam;->bindAllocation(Landroid/renderscript/Allocation;I)V

    :goto_0
    return-void

    :cond_0
    invoke-virtual {p1}, Lcom/android/phasebeam/ScriptField_Particle;->getAllocation()Landroid/renderscript/Allocation;

    move-result-object v0

    invoke-virtual {p0, v0, v1}, Lcom/android/phasebeam/ScriptC_phasebeam;->bindAllocation(Landroid/renderscript/Allocation;I)V

    goto :goto_0
.end method

.method public bind_dotParticles(Lcom/android/phasebeam/ScriptField_Particle;)V
    .locals 2
    .param p1    # Lcom/android/phasebeam/ScriptField_Particle;

    const/16 v1, 0x8

    iput-object p1, p0, Lcom/android/phasebeam/ScriptC_phasebeam;->mExportVar_dotParticles:Lcom/android/phasebeam/ScriptField_Particle;

    if-nez p1, :cond_0

    const/4 v0, 0x0

    invoke-virtual {p0, v0, v1}, Lcom/android/phasebeam/ScriptC_phasebeam;->bindAllocation(Landroid/renderscript/Allocation;I)V

    :goto_0
    return-void

    :cond_0
    invoke-virtual {p1}, Lcom/android/phasebeam/ScriptField_Particle;->getAllocation()Landroid/renderscript/Allocation;

    move-result-object v0

    invoke-virtual {p0, v0, v1}, Lcom/android/phasebeam/ScriptC_phasebeam;->bindAllocation(Landroid/renderscript/Allocation;I)V

    goto :goto_0
.end method

.method public bind_vertexColors(Lcom/android/phasebeam/ScriptField_VertexColor_s;)V
    .locals 2
    .param p1    # Lcom/android/phasebeam/ScriptField_VertexColor_s;

    const/4 v1, 0x7

    iput-object p1, p0, Lcom/android/phasebeam/ScriptC_phasebeam;->mExportVar_vertexColors:Lcom/android/phasebeam/ScriptField_VertexColor_s;

    if-nez p1, :cond_0

    const/4 v0, 0x0

    invoke-virtual {p0, v0, v1}, Lcom/android/phasebeam/ScriptC_phasebeam;->bindAllocation(Landroid/renderscript/Allocation;I)V

    :goto_0
    return-void

    :cond_0
    invoke-virtual {p1}, Lcom/android/phasebeam/ScriptField_VertexColor_s;->getAllocation()Landroid/renderscript/Allocation;

    move-result-object v0

    invoke-virtual {p0, v0, v1}, Lcom/android/phasebeam/ScriptC_phasebeam;->bindAllocation(Landroid/renderscript/Allocation;I)V

    goto :goto_0
.end method

.method public invoke_positionParticles()V
    .locals 1

    const/4 v0, 0x0

    invoke-virtual {p0, v0}, Lcom/android/phasebeam/ScriptC_phasebeam;->invoke(I)V

    return-void
.end method

.method public declared-synchronized set_beamMesh(Landroid/renderscript/Mesh;)V
    .locals 1
    .param p1    # Landroid/renderscript/Mesh;

    monitor-enter p0

    const/16 v0, 0xb

    :try_start_0
    invoke-virtual {p0, v0, p1}, Lcom/android/phasebeam/ScriptC_phasebeam;->setVar(ILandroid/renderscript/BaseObj;)V

    iput-object p1, p0, Lcom/android/phasebeam/ScriptC_phasebeam;->mExportVar_beamMesh:Landroid/renderscript/Mesh;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    monitor-exit p0

    return-void

    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method public declared-synchronized set_densityDPI(F)V
    .locals 1
    .param p1    # F

    monitor-enter p0

    const/16 v0, 0xd

    :try_start_0
    invoke-virtual {p0, v0, p1}, Lcom/android/phasebeam/ScriptC_phasebeam;->setVar(IF)V

    iput p1, p0, Lcom/android/phasebeam/ScriptC_phasebeam;->mExportVar_densityDPI:F
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    monitor-exit p0

    return-void

    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method public declared-synchronized set_dotMesh(Landroid/renderscript/Mesh;)V
    .locals 1
    .param p1    # Landroid/renderscript/Mesh;

    monitor-enter p0

    const/16 v0, 0xa

    :try_start_0
    invoke-virtual {p0, v0, p1}, Lcom/android/phasebeam/ScriptC_phasebeam;->setVar(ILandroid/renderscript/BaseObj;)V

    iput-object p1, p0, Lcom/android/phasebeam/ScriptC_phasebeam;->mExportVar_dotMesh:Landroid/renderscript/Mesh;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    monitor-exit p0

    return-void

    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method public declared-synchronized set_fragBg(Landroid/renderscript/ProgramFragment;)V
    .locals 1
    .param p1    # Landroid/renderscript/ProgramFragment;

    monitor-enter p0

    const/4 v0, 0x3

    :try_start_0
    invoke-virtual {p0, v0, p1}, Lcom/android/phasebeam/ScriptC_phasebeam;->setVar(ILandroid/renderscript/BaseObj;)V

    iput-object p1, p0, Lcom/android/phasebeam/ScriptC_phasebeam;->mExportVar_fragBg:Landroid/renderscript/ProgramFragment;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    monitor-exit p0

    return-void

    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method public declared-synchronized set_fragDots(Landroid/renderscript/ProgramFragment;)V
    .locals 1
    .param p1    # Landroid/renderscript/ProgramFragment;

    monitor-enter p0

    const/4 v0, 0x5

    :try_start_0
    invoke-virtual {p0, v0, p1}, Lcom/android/phasebeam/ScriptC_phasebeam;->setVar(ILandroid/renderscript/BaseObj;)V

    iput-object p1, p0, Lcom/android/phasebeam/ScriptC_phasebeam;->mExportVar_fragDots:Landroid/renderscript/ProgramFragment;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    monitor-exit p0

    return-void

    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method public declared-synchronized set_gBackgroundMesh(Landroid/renderscript/Mesh;)V
    .locals 1
    .param p1    # Landroid/renderscript/Mesh;

    monitor-enter p0

    const/16 v0, 0xc

    :try_start_0
    invoke-virtual {p0, v0, p1}, Lcom/android/phasebeam/ScriptC_phasebeam;->setVar(ILandroid/renderscript/BaseObj;)V

    iput-object p1, p0, Lcom/android/phasebeam/ScriptC_phasebeam;->mExportVar_gBackgroundMesh:Landroid/renderscript/Mesh;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    monitor-exit p0

    return-void

    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method public declared-synchronized set_textureBeam(Landroid/renderscript/Allocation;)V
    .locals 1
    .param p1    # Landroid/renderscript/Allocation;

    monitor-enter p0

    const/4 v0, 0x1

    :try_start_0
    invoke-virtual {p0, v0, p1}, Lcom/android/phasebeam/ScriptC_phasebeam;->setVar(ILandroid/renderscript/BaseObj;)V

    iput-object p1, p0, Lcom/android/phasebeam/ScriptC_phasebeam;->mExportVar_textureBeam:Landroid/renderscript/Allocation;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    monitor-exit p0

    return-void

    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method public declared-synchronized set_textureDot(Landroid/renderscript/Allocation;)V
    .locals 1
    .param p1    # Landroid/renderscript/Allocation;

    monitor-enter p0

    const/4 v0, 0x0

    :try_start_0
    invoke-virtual {p0, v0, p1}, Lcom/android/phasebeam/ScriptC_phasebeam;->setVar(ILandroid/renderscript/BaseObj;)V

    iput-object p1, p0, Lcom/android/phasebeam/ScriptC_phasebeam;->mExportVar_textureDot:Landroid/renderscript/Allocation;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    monitor-exit p0

    return-void

    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method public declared-synchronized set_vertBg(Landroid/renderscript/ProgramVertex;)V
    .locals 1
    .param p1    # Landroid/renderscript/ProgramVertex;

    monitor-enter p0

    const/4 v0, 0x2

    :try_start_0
    invoke-virtual {p0, v0, p1}, Lcom/android/phasebeam/ScriptC_phasebeam;->setVar(ILandroid/renderscript/BaseObj;)V

    iput-object p1, p0, Lcom/android/phasebeam/ScriptC_phasebeam;->mExportVar_vertBg:Landroid/renderscript/ProgramVertex;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    monitor-exit p0

    return-void

    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method public declared-synchronized set_vertDots(Landroid/renderscript/ProgramVertex;)V
    .locals 1
    .param p1    # Landroid/renderscript/ProgramVertex;

    monitor-enter p0

    const/4 v0, 0x4

    :try_start_0
    invoke-virtual {p0, v0, p1}, Lcom/android/phasebeam/ScriptC_phasebeam;->setVar(ILandroid/renderscript/BaseObj;)V

    iput-object p1, p0, Lcom/android/phasebeam/ScriptC_phasebeam;->mExportVar_vertDots:Landroid/renderscript/ProgramVertex;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    monitor-exit p0

    return-void

    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method
