.class final enum Lcom/google/android/gsf/login/TermsOfServiceActivity$GenderAdapter$HintState;
.super Ljava/lang/Enum;
.source "TermsOfServiceActivity.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/google/android/gsf/login/TermsOfServiceActivity$GenderAdapter;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x401a
    name = "HintState"
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Enum",
        "<",
        "Lcom/google/android/gsf/login/TermsOfServiceActivity$GenderAdapter$HintState;",
        ">;"
    }
.end annotation


# static fields
.field private static final synthetic $VALUES:[Lcom/google/android/gsf/login/TermsOfServiceActivity$GenderAdapter$HintState;

.field public static final enum GONE:Lcom/google/android/gsf/login/TermsOfServiceActivity$GenderAdapter$HintState;

.field public static final enum REMOVE:Lcom/google/android/gsf/login/TermsOfServiceActivity$GenderAdapter$HintState;

.field public static final enum SHOW:Lcom/google/android/gsf/login/TermsOfServiceActivity$GenderAdapter$HintState;


# direct methods
.method static constructor <clinit>()V
    .locals 5

    const/4 v4, 0x2

    const/4 v3, 0x1

    const/4 v2, 0x0

    new-instance v0, Lcom/google/android/gsf/login/TermsOfServiceActivity$GenderAdapter$HintState;

    const-string v1, "SHOW"

    invoke-direct {v0, v1, v2}, Lcom/google/android/gsf/login/TermsOfServiceActivity$GenderAdapter$HintState;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/google/android/gsf/login/TermsOfServiceActivity$GenderAdapter$HintState;->SHOW:Lcom/google/android/gsf/login/TermsOfServiceActivity$GenderAdapter$HintState;

    new-instance v0, Lcom/google/android/gsf/login/TermsOfServiceActivity$GenderAdapter$HintState;

    const-string v1, "REMOVE"

    invoke-direct {v0, v1, v3}, Lcom/google/android/gsf/login/TermsOfServiceActivity$GenderAdapter$HintState;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/google/android/gsf/login/TermsOfServiceActivity$GenderAdapter$HintState;->REMOVE:Lcom/google/android/gsf/login/TermsOfServiceActivity$GenderAdapter$HintState;

    new-instance v0, Lcom/google/android/gsf/login/TermsOfServiceActivity$GenderAdapter$HintState;

    const-string v1, "GONE"

    invoke-direct {v0, v1, v4}, Lcom/google/android/gsf/login/TermsOfServiceActivity$GenderAdapter$HintState;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/google/android/gsf/login/TermsOfServiceActivity$GenderAdapter$HintState;->GONE:Lcom/google/android/gsf/login/TermsOfServiceActivity$GenderAdapter$HintState;

    const/4 v0, 0x3

    new-array v0, v0, [Lcom/google/android/gsf/login/TermsOfServiceActivity$GenderAdapter$HintState;

    sget-object v1, Lcom/google/android/gsf/login/TermsOfServiceActivity$GenderAdapter$HintState;->SHOW:Lcom/google/android/gsf/login/TermsOfServiceActivity$GenderAdapter$HintState;

    aput-object v1, v0, v2

    sget-object v1, Lcom/google/android/gsf/login/TermsOfServiceActivity$GenderAdapter$HintState;->REMOVE:Lcom/google/android/gsf/login/TermsOfServiceActivity$GenderAdapter$HintState;

    aput-object v1, v0, v3

    sget-object v1, Lcom/google/android/gsf/login/TermsOfServiceActivity$GenderAdapter$HintState;->GONE:Lcom/google/android/gsf/login/TermsOfServiceActivity$GenderAdapter$HintState;

    aput-object v1, v0, v4

    sput-object v0, Lcom/google/android/gsf/login/TermsOfServiceActivity$GenderAdapter$HintState;->$VALUES:[Lcom/google/android/gsf/login/TermsOfServiceActivity$GenderAdapter$HintState;

    return-void
.end method

.method private constructor <init>(Ljava/lang/String;I)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()V"
        }
    .end annotation

    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    return-void
.end method

.method public static valueOf(Ljava/lang/String;)Lcom/google/android/gsf/login/TermsOfServiceActivity$GenderAdapter$HintState;
    .locals 1

    const-class v0, Lcom/google/android/gsf/login/TermsOfServiceActivity$GenderAdapter$HintState;

    invoke-static {v0, p0}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;

    move-result-object v0

    check-cast v0, Lcom/google/android/gsf/login/TermsOfServiceActivity$GenderAdapter$HintState;

    return-object v0
.end method

.method public static values()[Lcom/google/android/gsf/login/TermsOfServiceActivity$GenderAdapter$HintState;
    .locals 1

    sget-object v0, Lcom/google/android/gsf/login/TermsOfServiceActivity$GenderAdapter$HintState;->$VALUES:[Lcom/google/android/gsf/login/TermsOfServiceActivity$GenderAdapter$HintState;

    invoke-virtual {v0}, [Lcom/google/android/gsf/login/TermsOfServiceActivity$GenderAdapter$HintState;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [Lcom/google/android/gsf/login/TermsOfServiceActivity$GenderAdapter$HintState;

    return-object v0
.end method
