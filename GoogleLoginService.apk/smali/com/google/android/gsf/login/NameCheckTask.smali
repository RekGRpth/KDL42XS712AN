.class public Lcom/google/android/gsf/login/NameCheckTask;
.super Lcom/google/android/gsf/login/BackgroundTask;
.source "NameCheckTask.java"

# interfaces
.implements Landroid/view/View$OnClickListener;


# direct methods
.method public constructor <init>()V
    .locals 0

    invoke-direct {p0}, Lcom/google/android/gsf/login/BackgroundTask;-><init>()V

    return-void
.end method

.method static synthetic access$000(Lcom/google/android/gsf/login/NameCheckTask;)Lcom/google/android/gsf/loginservice/GLSUser$GLSSession;
    .locals 1
    .param p0    # Lcom/google/android/gsf/login/NameCheckTask;

    iget-object v0, p0, Lcom/google/android/gsf/loginservice/BaseActivity;->mSession:Lcom/google/android/gsf/loginservice/GLSUser$GLSSession;

    return-object v0
.end method


# virtual methods
.method public onCreate(Landroid/os/Bundle;)V
    .locals 1
    .param p1    # Landroid/os/Bundle;

    invoke-super {p0, p1}, Lcom/google/android/gsf/login/BackgroundTask;->onCreate(Landroid/os/Bundle;)V

    const v0, 0x7f080031    # com.google.android.gsf.login.R.string.plus_name_check_title

    invoke-virtual {p0, v0}, Lcom/google/android/gsf/login/NameCheckTask;->setTitle(I)V

    const v0, 0x7f080032    # com.google.android.gsf.login.R.string.plus_name_check_text

    invoke-virtual {p0, v0}, Lcom/google/android/gsf/login/NameCheckTask;->setMessage(I)V

    return-void
.end method

.method public start()V
    .locals 3

    invoke-super {p0}, Lcom/google/android/gsf/login/BackgroundTask;->start()V

    iget-object v1, p0, Lcom/google/android/gsf/login/BackgroundTask;->mHandler:Landroid/os/Handler;

    const/4 v2, 0x4

    invoke-virtual {v1, v2}, Landroid/os/Handler;->obtainMessage(I)Landroid/os/Message;

    move-result-object v0

    new-instance v1, Lcom/google/android/gsf/login/NameCheckTask$1;

    invoke-direct {v1, p0, v0, v0}, Lcom/google/android/gsf/login/NameCheckTask$1;-><init>(Lcom/google/android/gsf/login/NameCheckTask;Landroid/os/Message;Landroid/os/Message;)V

    iput-object v1, p0, Lcom/google/android/gsf/login/BackgroundTask;->mTaskThread:Lcom/google/android/gsf/login/CancelableCallbackThread;

    iget-object v1, p0, Lcom/google/android/gsf/login/BackgroundTask;->mTaskThread:Lcom/google/android/gsf/login/CancelableCallbackThread;

    invoke-virtual {v1}, Lcom/google/android/gsf/login/CancelableCallbackThread;->start()V

    return-void
.end method
