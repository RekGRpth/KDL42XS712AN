.class Lcom/google/android/gsf/login/CreateAccountTask$1;
.super Lcom/google/android/gsf/login/CancelableCallbackThread;
.source "CreateAccountTask.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/google/android/gsf/login/CreateAccountTask;->start()V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/google/android/gsf/login/CreateAccountTask;

.field final synthetic val$resultMessage:Landroid/os/Message;


# direct methods
.method constructor <init>(Lcom/google/android/gsf/login/CreateAccountTask;Landroid/os/Message;Landroid/os/Message;)V
    .locals 0
    .param p2    # Landroid/os/Message;

    iput-object p1, p0, Lcom/google/android/gsf/login/CreateAccountTask$1;->this$0:Lcom/google/android/gsf/login/CreateAccountTask;

    iput-object p3, p0, Lcom/google/android/gsf/login/CreateAccountTask$1;->val$resultMessage:Landroid/os/Message;

    invoke-direct {p0, p2}, Lcom/google/android/gsf/login/CancelableCallbackThread;-><init>(Landroid/os/Message;)V

    return-void
.end method


# virtual methods
.method protected runInBackground()V
    .locals 4

    iget-object v1, p0, Lcom/google/android/gsf/login/CreateAccountTask$1;->this$0:Lcom/google/android/gsf/login/CreateAccountTask;

    iget-object v1, v1, Lcom/google/android/gsf/login/BaseActivity;->mBackendStub:Lcom/google/android/gsf/login/BackendStub;

    iget-object v2, p0, Lcom/google/android/gsf/login/CreateAccountTask$1;->this$0:Lcom/google/android/gsf/login/CreateAccountTask;

    iget-object v3, p0, Lcom/google/android/gsf/login/CreateAccountTask$1;->this$0:Lcom/google/android/gsf/login/CreateAccountTask;

    # getter for: Lcom/google/android/gsf/loginservice/BaseActivity;->mSession:Lcom/google/android/gsf/loginservice/GLSUser$GLSSession;
    invoke-static {v3}, Lcom/google/android/gsf/login/CreateAccountTask;->access$000(Lcom/google/android/gsf/login/CreateAccountTask;)Lcom/google/android/gsf/loginservice/GLSUser$GLSSession;

    move-result-object v3

    invoke-virtual {v1, v2, v3}, Lcom/google/android/gsf/login/BackendStub;->createAccountSync(Landroid/content/Context;Lcom/google/android/gsf/loginservice/GLSUser$GLSSession;)Lcom/google/android/gsf/loginservice/GLSUser$Status;

    move-result-object v0

    sget-object v1, Lcom/google/android/gsf/loginservice/GLSUser$Status;->ALREADY_HAS_GMAIL:Lcom/google/android/gsf/loginservice/GLSUser$Status;

    if-ne v0, v1, :cond_0

    sget-object v0, Lcom/google/android/gsf/loginservice/GLSUser$Status;->SUCCESS:Lcom/google/android/gsf/loginservice/GLSUser$Status;

    :cond_0
    if-eqz v0, :cond_1

    iget-object v1, p0, Lcom/google/android/gsf/login/CreateAccountTask$1;->val$resultMessage:Landroid/os/Message;

    invoke-virtual {v0, v1}, Lcom/google/android/gsf/loginservice/GLSUser$Status;->toMessage(Landroid/os/Message;)V

    :cond_1
    iget-object v1, p0, Lcom/google/android/gsf/login/CancelableCallbackThread;->mIsCanceled:Ljava/util/concurrent/atomic/AtomicBoolean;

    invoke-virtual {v1}, Ljava/util/concurrent/atomic/AtomicBoolean;->get()Z

    move-result v1

    if-nez v1, :cond_2

    iget-object v1, p0, Lcom/google/android/gsf/login/CreateAccountTask$1;->this$0:Lcom/google/android/gsf/login/CreateAccountTask;

    const-wide/16 v2, 0x3e8

    invoke-virtual {v1, v2, v3}, Lcom/google/android/gsf/login/CreateAccountTask;->ensureDelay(J)V

    :cond_2
    sget-object v1, Lcom/google/android/gsf/loginservice/GLSUser$Status;->SUCCESS:Lcom/google/android/gsf/loginservice/GLSUser$Status;

    if-ne v0, v1, :cond_3

    iget-object v1, p0, Lcom/google/android/gsf/login/CreateAccountTask$1;->this$0:Lcom/google/android/gsf/login/CreateAccountTask;

    invoke-virtual {v1}, Lcom/google/android/gsf/login/CreateAccountTask;->getMarket()Lcom/google/android/gsf/login/MarketHelper;

    move-result-object v1

    iget-object v2, p0, Lcom/google/android/gsf/login/CreateAccountTask$1;->this$0:Lcom/google/android/gsf/login/CreateAccountTask;

    # getter for: Lcom/google/android/gsf/loginservice/BaseActivity;->mSession:Lcom/google/android/gsf/loginservice/GLSUser$GLSSession;
    invoke-static {v2}, Lcom/google/android/gsf/login/CreateAccountTask;->access$100(Lcom/google/android/gsf/login/CreateAccountTask;)Lcom/google/android/gsf/loginservice/GLSUser$GLSSession;

    move-result-object v2

    invoke-virtual {v1, v2}, Lcom/google/android/gsf/login/MarketHelper;->allowCreditCard(Lcom/google/android/gsf/loginservice/GLSUser$GLSSession;)Z

    move-result v1

    if-eqz v1, :cond_3

    iget-object v1, p0, Lcom/google/android/gsf/login/CreateAccountTask$1;->this$0:Lcom/google/android/gsf/login/CreateAccountTask;

    invoke-virtual {v1}, Lcom/google/android/gsf/login/CreateAccountTask;->getMarket()Lcom/google/android/gsf/login/MarketHelper;

    move-result-object v1

    iget-object v2, p0, Lcom/google/android/gsf/login/CreateAccountTask$1;->this$0:Lcom/google/android/gsf/login/CreateAccountTask;

    # getter for: Lcom/google/android/gsf/loginservice/BaseActivity;->mSession:Lcom/google/android/gsf/loginservice/GLSUser$GLSSession;
    invoke-static {v2}, Lcom/google/android/gsf/login/CreateAccountTask;->access$200(Lcom/google/android/gsf/login/CreateAccountTask;)Lcom/google/android/gsf/loginservice/GLSUser$GLSSession;

    move-result-object v2

    invoke-virtual {v1, v2}, Lcom/google/android/gsf/login/MarketHelper;->setOffersValues(Lcom/google/android/gsf/loginservice/GLSUser$GLSSession;)V

    :cond_3
    return-void
.end method
