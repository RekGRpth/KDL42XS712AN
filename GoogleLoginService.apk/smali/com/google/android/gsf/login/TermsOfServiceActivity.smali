.class public Lcom/google/android/gsf/login/TermsOfServiceActivity;
.super Lcom/google/android/gsf/login/BaseActivity;
.source "TermsOfServiceActivity.java"

# interfaces
.implements Lcom/google/android/gsf/login/GenderSpinner$OnSetSelectionListener;


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/google/android/gsf/login/TermsOfServiceActivity$1;,
        Lcom/google/android/gsf/login/TermsOfServiceActivity$GenderAdapter;
    }
.end annotation


# instance fields
.field mGenderAdapter:Lcom/google/android/gsf/login/TermsOfServiceActivity$GenderAdapter;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/google/android/gsf/login/TermsOfServiceActivity$GenderAdapter",
            "<",
            "Ljava/lang/CharSequence;",
            ">;"
        }
    .end annotation
.end field

.field mGenderSpinner:Lcom/google/android/gsf/login/GenderSpinner;

.field private mGooglePlayOptInCheckBox:Landroid/widget/CheckBox;

.field mNextButton:Landroid/view/View;

.field private mShowChrome:Z


# direct methods
.method public constructor <init>()V
    .locals 0

    invoke-direct {p0}, Lcom/google/android/gsf/login/BaseActivity;-><init>()V

    return-void
.end method

.method private setOrClearUserDataFromView(ILcom/google/android/gsf/login/BackendStub$Key;)V
    .locals 4
    .param p1    # I
    .param p2    # Lcom/google/android/gsf/login/BackendStub$Key;

    invoke-virtual {p0, p1}, Lcom/google/android/gsf/login/TermsOfServiceActivity;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/CheckBox;

    invoke-virtual {v0}, Landroid/widget/CheckBox;->getVisibility()I

    move-result v1

    if-nez v1, :cond_1

    iget-object v1, p0, Lcom/google/android/gsf/loginservice/BaseActivity;->mSession:Lcom/google/android/gsf/loginservice/GLSUser$GLSSession;

    iget-object v2, v1, Lcom/google/android/gsf/loginservice/GLSUser$GLSSession;->mUserData:Ljava/util/HashMap;

    invoke-virtual {p2}, Lcom/google/android/gsf/login/BackendStub$Key;->getWire()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v0}, Landroid/widget/CheckBox;->isChecked()Z

    move-result v1

    if-eqz v1, :cond_0

    const-string v1, "1"

    :goto_0
    invoke-virtual {v2, v3, v1}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    :goto_1
    return-void

    :cond_0
    const-string v1, "0"

    goto :goto_0

    :cond_1
    iget-object v1, p0, Lcom/google/android/gsf/loginservice/BaseActivity;->mSession:Lcom/google/android/gsf/loginservice/GLSUser$GLSSession;

    iget-object v1, v1, Lcom/google/android/gsf/loginservice/GLSUser$GLSSession;->mUserData:Ljava/util/HashMap;

    invoke-virtual {p2}, Lcom/google/android/gsf/login/BackendStub$Key;->getWire()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/util/HashMap;->remove(Ljava/lang/Object;)Ljava/lang/Object;

    goto :goto_1
.end method


# virtual methods
.method protected initViews()V
    .locals 12

    const v11, 0x7f0b0053    # com.google.android.gsf.login.R.id.learn_more

    const v10, 0x7f0b0050    # com.google.android.gsf.login.R.id.gender_spinner

    const/4 v6, 0x1

    const/16 v9, 0x8

    const v5, 0x7f0b002f    # com.google.android.gsf.login.R.id.google_play_opt_in

    invoke-virtual {p0, v5}, Lcom/google/android/gsf/login/TermsOfServiceActivity;->findViewById(I)Landroid/view/View;

    move-result-object v5

    check-cast v5, Landroid/widget/CheckBox;

    iput-object v5, p0, Lcom/google/android/gsf/login/TermsOfServiceActivity;->mGooglePlayOptInCheckBox:Landroid/widget/CheckBox;

    iget-object v5, p0, Lcom/google/android/gsf/login/TermsOfServiceActivity;->mGooglePlayOptInCheckBox:Landroid/widget/CheckBox;

    invoke-virtual {p0}, Lcom/google/android/gsf/login/TermsOfServiceActivity;->getGooglePlayOptInDefault()Z

    move-result v7

    invoke-virtual {v5, v7}, Landroid/widget/CheckBox;->setChecked(Z)V

    const v5, 0x7f0b000a    # com.google.android.gsf.login.R.id.next_button

    invoke-virtual {p0, v5}, Lcom/google/android/gsf/login/TermsOfServiceActivity;->findViewById(I)Landroid/view/View;

    move-result-object v5

    iput-object v5, p0, Lcom/google/android/gsf/login/TermsOfServiceActivity;->mNextButton:Landroid/view/View;

    iget-object v5, p0, Lcom/google/android/gsf/login/TermsOfServiceActivity;->mNextButton:Landroid/view/View;

    invoke-virtual {p0, v5, v6}, Lcom/google/android/gsf/login/TermsOfServiceActivity;->setDefaultButton(Landroid/view/View;Z)V

    const v5, 0x7f0b0013    # com.google.android.gsf.login.R.id.back_button

    invoke-virtual {p0, v5}, Lcom/google/android/gsf/login/TermsOfServiceActivity;->findViewById(I)Landroid/view/View;

    move-result-object v5

    invoke-virtual {p0, v5}, Lcom/google/android/gsf/login/TermsOfServiceActivity;->setBackButton(Landroid/view/View;)V

    iget-object v5, p0, Lcom/google/android/gsf/loginservice/BaseActivity;->mSession:Lcom/google/android/gsf/loginservice/GLSUser$GLSSession;

    iget-boolean v5, v5, Lcom/google/android/gsf/loginservice/GLSUser$GLSSession;->mCreatingAccount:Z

    if-nez v5, :cond_0

    iget-object v5, p0, Lcom/google/android/gsf/loginservice/BaseActivity;->mSession:Lcom/google/android/gsf/loginservice/GLSUser$GLSSession;

    iget-boolean v5, v5, Lcom/google/android/gsf/loginservice/GLSUser$GLSSession;->mUserSelectedGooglePlus:Z

    if-eqz v5, :cond_0

    const v5, 0x7f0b0002    # com.google.android.gsf.login.R.id.title

    invoke-virtual {p0, v5}, Lcom/google/android/gsf/login/TermsOfServiceActivity;->findViewById(I)Landroid/view/View;

    move-result-object v3

    check-cast v3, Landroid/widget/TextView;

    const v5, 0x7f08008d    # com.google.android.gsf.login.R.string.account_tos_plus_title

    invoke-virtual {v3, v5}, Landroid/widget/TextView;->setText(I)V

    :cond_0
    invoke-virtual {p0, v10}, Lcom/google/android/gsf/login/TermsOfServiceActivity;->findViewById(I)Landroid/view/View;

    move-result-object v5

    check-cast v5, Lcom/google/android/gsf/login/GenderSpinner;

    iput-object v5, p0, Lcom/google/android/gsf/login/TermsOfServiceActivity;->mGenderSpinner:Lcom/google/android/gsf/login/GenderSpinner;

    const v5, 0x7f050003    # com.google.android.gsf.login.R.array.gender_array

    const v7, 0x7f03000d    # com.google.android.gsf.login.R.layout.gender_spinner_item

    invoke-static {p0, v5, v7}, Lcom/google/android/gsf/login/TermsOfServiceActivity$GenderAdapter;->createFromResource(Landroid/content/Context;II)Lcom/google/android/gsf/login/TermsOfServiceActivity$GenderAdapter;

    move-result-object v5

    iput-object v5, p0, Lcom/google/android/gsf/login/TermsOfServiceActivity;->mGenderAdapter:Lcom/google/android/gsf/login/TermsOfServiceActivity$GenderAdapter;

    iget-object v5, p0, Lcom/google/android/gsf/login/TermsOfServiceActivity;->mGenderAdapter:Lcom/google/android/gsf/login/TermsOfServiceActivity$GenderAdapter;

    const v7, 0x7f03000c    # com.google.android.gsf.login.R.layout.gender_spinner_dropdown_item

    invoke-virtual {v5, v7}, Lcom/google/android/gsf/login/TermsOfServiceActivity$GenderAdapter;->setDropDownViewResource(I)V

    iget-object v5, p0, Lcom/google/android/gsf/login/TermsOfServiceActivity;->mGenderAdapter:Lcom/google/android/gsf/login/TermsOfServiceActivity$GenderAdapter;

    iget-object v7, p0, Lcom/google/android/gsf/login/TermsOfServiceActivity;->mGenderSpinner:Lcom/google/android/gsf/login/GenderSpinner;

    invoke-virtual {v7}, Lcom/google/android/gsf/login/GenderSpinner;->getPrompt()Ljava/lang/CharSequence;

    move-result-object v7

    invoke-virtual {v5, v7}, Lcom/google/android/gsf/login/TermsOfServiceActivity$GenderAdapter;->setHint(Ljava/lang/CharSequence;)V

    iget-object v5, p0, Lcom/google/android/gsf/login/TermsOfServiceActivity;->mGenderSpinner:Lcom/google/android/gsf/login/GenderSpinner;

    iget-object v7, p0, Lcom/google/android/gsf/login/TermsOfServiceActivity;->mGenderAdapter:Lcom/google/android/gsf/login/TermsOfServiceActivity$GenderAdapter;

    invoke-virtual {v5, v7}, Lcom/google/android/gsf/login/GenderSpinner;->setAdapter(Landroid/widget/SpinnerAdapter;)V

    iget-object v5, p0, Lcom/google/android/gsf/login/TermsOfServiceActivity;->mGenderSpinner:Lcom/google/android/gsf/login/GenderSpinner;

    invoke-virtual {v5, p0}, Lcom/google/android/gsf/login/GenderSpinner;->setOnSetSelectionListener(Lcom/google/android/gsf/login/GenderSpinner$OnSetSelectionListener;)V

    invoke-virtual {p0, v11}, Lcom/google/android/gsf/login/TermsOfServiceActivity;->findViewById(I)Landroid/view/View;

    move-result-object v4

    check-cast v4, Landroid/widget/TextView;

    const v5, 0x7f080098    # com.google.android.gsf.login.R.string.learn_more

    const v7, 0x7f0800ab    # com.google.android.gsf.login.R.string.learn_more_web_history_title

    const v8, 0x7f0800aa    # com.google.android.gsf.login.R.string.learn_more_web_history

    invoke-static {p0, v4, v5, v7, v8}, Lcom/google/android/gsf/login/LearnMoreActivity;->linkifyAndSetText(Lcom/google/android/gsf/login/BaseActivity;Landroid/widget/TextView;III)V

    iget-boolean v5, p0, Lcom/google/android/gsf/login/TermsOfServiceActivity;->mShowChrome:Z

    if-eqz v5, :cond_3

    const v2, 0x7f08009a    # com.google.android.gsf.login.R.string.account_creation_tos_with_chrome

    :goto_0
    iget-object v5, p0, Lcom/google/android/gsf/loginservice/BaseActivity;->mSession:Lcom/google/android/gsf/loginservice/GLSUser$GLSSession;

    iget-boolean v5, v5, Lcom/google/android/gsf/loginservice/GLSUser$GLSSession;->mCreatingAccount:Z

    if-nez v5, :cond_1

    const v5, 0x7f0b0052    # com.google.android.gsf.login.R.id.agree_web_history

    invoke-virtual {p0, v5}, Lcom/google/android/gsf/login/TermsOfServiceActivity;->findViewById(I)Landroid/view/View;

    move-result-object v5

    invoke-virtual {v5, v9}, Landroid/view/View;->setVisibility(I)V

    invoke-virtual {p0, v11}, Lcom/google/android/gsf/login/TermsOfServiceActivity;->findViewById(I)Landroid/view/View;

    move-result-object v5

    invoke-virtual {v5, v9}, Landroid/view/View;->setVisibility(I)V

    iget-object v5, p0, Lcom/google/android/gsf/login/TermsOfServiceActivity;->mGooglePlayOptInCheckBox:Landroid/widget/CheckBox;

    invoke-virtual {v5, v9}, Landroid/widget/CheckBox;->setVisibility(I)V

    :cond_1
    const v5, 0x7f0b0054    # com.google.android.gsf.login.R.id.terms

    invoke-virtual {p0, v5}, Lcom/google/android/gsf/login/TermsOfServiceActivity;->findViewById(I)Landroid/view/View;

    move-result-object v1

    check-cast v1, Landroid/widget/TextView;

    if-eqz v1, :cond_2

    if-lez v2, :cond_4

    invoke-static {p0, v2}, Lcom/google/android/gsf/login/LinkSpan;->linkify(Lcom/google/android/gsf/login/BaseActivity;I)Ljava/lang/CharSequence;

    move-result-object v0

    invoke-virtual {v1, v0}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    invoke-static {}, Landroid/text/method/LinkMovementMethod;->getInstance()Landroid/text/method/MovementMethod;

    move-result-object v5

    invoke-virtual {v1, v5}, Landroid/widget/TextView;->setMovementMethod(Landroid/text/method/MovementMethod;)V

    :cond_2
    :goto_1
    iget-object v5, p0, Lcom/google/android/gsf/loginservice/BaseActivity;->mSession:Lcom/google/android/gsf/loginservice/GLSUser$GLSSession;

    iget-boolean v5, v5, Lcom/google/android/gsf/loginservice/GLSUser$GLSSession;->mUserSelectedGooglePlus:Z

    if-nez v5, :cond_5

    invoke-virtual {p0, v10}, Lcom/google/android/gsf/login/TermsOfServiceActivity;->findViewById(I)Landroid/view/View;

    move-result-object v5

    invoke-virtual {v5, v9}, Landroid/view/View;->setVisibility(I)V

    const v5, 0x7f0b0051    # com.google.android.gsf.login.R.id.agree_personalized_content

    invoke-virtual {p0, v5}, Lcom/google/android/gsf/login/TermsOfServiceActivity;->findViewById(I)Landroid/view/View;

    move-result-object v5

    invoke-virtual {v5, v9}, Landroid/view/View;->setVisibility(I)V

    :goto_2
    return-void

    :cond_3
    const v2, 0x7f080099    # com.google.android.gsf.login.R.string.account_creation_tos

    goto :goto_0

    :cond_4
    invoke-virtual {v1, v9}, Landroid/widget/TextView;->setVisibility(I)V

    goto :goto_1

    :cond_5
    iget-object v7, p0, Lcom/google/android/gsf/login/TermsOfServiceActivity;->mNextButton:Landroid/view/View;

    iget-object v5, p0, Lcom/google/android/gsf/login/TermsOfServiceActivity;->mGenderAdapter:Lcom/google/android/gsf/login/TermsOfServiceActivity$GenderAdapter;

    invoke-virtual {v5}, Lcom/google/android/gsf/login/TermsOfServiceActivity$GenderAdapter;->isHintShowing()Z

    move-result v5

    if-nez v5, :cond_6

    move v5, v6

    :goto_3
    invoke-virtual {v7, v5}, Landroid/view/View;->setEnabled(Z)V

    goto :goto_2

    :cond_6
    const/4 v5, 0x0

    goto :goto_3
.end method

.method public onConfigurationChanged(Landroid/content/res/Configuration;)V
    .locals 1
    .param p1    # Landroid/content/res/Configuration;

    invoke-super {p0, p1}, Lcom/google/android/gsf/login/BaseActivity;->onConfigurationChanged(Landroid/content/res/Configuration;)V

    iget-object v0, p0, Lcom/google/android/gsf/login/TermsOfServiceActivity;->mGenderSpinner:Lcom/google/android/gsf/login/GenderSpinner;

    invoke-virtual {v0}, Lcom/google/android/gsf/login/GenderSpinner;->dismissMenu()V

    return-void
.end method

.method protected onCreate(Landroid/os/Bundle;)V
    .locals 1
    .param p1    # Landroid/os/Bundle;

    invoke-super {p0, p1}, Lcom/google/android/gsf/login/BaseActivity;->onCreate(Landroid/os/Bundle;)V

    invoke-virtual {p0}, Lcom/google/android/gsf/login/TermsOfServiceActivity;->isChromeInstalled()Z

    move-result v0

    iput-boolean v0, p0, Lcom/google/android/gsf/login/TermsOfServiceActivity;->mShowChrome:Z

    const v0, 0x7f03001e    # com.google.android.gsf.login.R.layout.terms_of_service_activity

    invoke-virtual {p0, v0}, Lcom/google/android/gsf/login/TermsOfServiceActivity;->setContentView(I)V

    invoke-virtual {p0}, Lcom/google/android/gsf/login/TermsOfServiceActivity;->initViews()V

    invoke-virtual {p0}, Lcom/google/android/gsf/login/TermsOfServiceActivity;->updateWidgetState()V

    return-void
.end method

.method public onSetSelection(Lcom/google/android/gsf/login/GenderSpinner;I)V
    .locals 2
    .param p1    # Lcom/google/android/gsf/login/GenderSpinner;
    .param p2    # I

    iget-object v0, p0, Lcom/google/android/gsf/login/TermsOfServiceActivity;->mGenderAdapter:Lcom/google/android/gsf/login/TermsOfServiceActivity$GenderAdapter;

    invoke-virtual {v0}, Lcom/google/android/gsf/login/TermsOfServiceActivity$GenderAdapter;->hideHint()V

    iget-object v1, p0, Lcom/google/android/gsf/login/TermsOfServiceActivity;->mNextButton:Landroid/view/View;

    iget-object v0, p0, Lcom/google/android/gsf/login/TermsOfServiceActivity;->mGenderAdapter:Lcom/google/android/gsf/login/TermsOfServiceActivity$GenderAdapter;

    invoke-virtual {v0}, Lcom/google/android/gsf/login/TermsOfServiceActivity$GenderAdapter;->isHintShowing()Z

    move-result v0

    if-nez v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    invoke-virtual {v1, v0}, Landroid/view/View;->setEnabled(Z)V

    return-void

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public start()V
    .locals 4

    const/4 v3, 0x1

    invoke-super {p0}, Lcom/google/android/gsf/login/BaseActivity;->start()V

    const v0, 0x7f0b0052    # com.google.android.gsf.login.R.id.agree_web_history

    sget-object v1, Lcom/google/android/gsf/login/BackendStub$Key;->AGREE_WEB_HISTORY:Lcom/google/android/gsf/login/BackendStub$Key;

    invoke-direct {p0, v0, v1}, Lcom/google/android/gsf/login/TermsOfServiceActivity;->setOrClearUserDataFromView(ILcom/google/android/gsf/login/BackendStub$Key;)V

    const v0, 0x7f0b0051    # com.google.android.gsf.login.R.id.agree_personalized_content

    sget-object v1, Lcom/google/android/gsf/login/BackendStub$Key;->AGREE_PERSONALIZED_CONTENT:Lcom/google/android/gsf/login/BackendStub$Key;

    invoke-direct {p0, v0, v1}, Lcom/google/android/gsf/login/TermsOfServiceActivity;->setOrClearUserDataFromView(ILcom/google/android/gsf/login/BackendStub$Key;)V

    iget-object v0, p0, Lcom/google/android/gsf/loginservice/BaseActivity;->mSession:Lcom/google/android/gsf/loginservice/GLSUser$GLSSession;

    iget-object v0, v0, Lcom/google/android/gsf/loginservice/GLSUser$GLSSession;->mUserData:Ljava/util/HashMap;

    sget-object v1, Lcom/google/android/gsf/loginservice/GLSUser$RequestKey;->GENDER:Lcom/google/android/gsf/loginservice/GLSUser$RequestKey;

    invoke-virtual {v1}, Lcom/google/android/gsf/loginservice/GLSUser$RequestKey;->getWire()Ljava/lang/String;

    move-result-object v1

    iget-object v2, p0, Lcom/google/android/gsf/login/TermsOfServiceActivity;->mGenderSpinner:Lcom/google/android/gsf/login/GenderSpinner;

    invoke-virtual {v2}, Lcom/google/android/gsf/login/GenderSpinner;->getSelectedItemPosition()I

    move-result v2

    invoke-static {v2}, Ljava/lang/Integer;->toString(I)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    iget-object v0, p0, Lcom/google/android/gsf/loginservice/BaseActivity;->mSession:Lcom/google/android/gsf/loginservice/GLSUser$GLSSession;

    iput-boolean v3, v0, Lcom/google/android/gsf/loginservice/GLSUser$GLSSession;->mTermsOfServiceShown:Z

    iget-object v0, p0, Lcom/google/android/gsf/loginservice/BaseActivity;->mSession:Lcom/google/android/gsf/loginservice/GLSUser$GLSSession;

    invoke-static {v3}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v1

    iput-object v1, v0, Lcom/google/android/gsf/loginservice/GLSUser$GLSSession;->mAgreedToMobileTos:Ljava/lang/Boolean;

    iget-object v0, p0, Lcom/google/android/gsf/loginservice/BaseActivity;->mSession:Lcom/google/android/gsf/loginservice/GLSUser$GLSSession;

    iput-boolean v3, v0, Lcom/google/android/gsf/loginservice/GLSUser$GLSSession;->mAgreedToPlayTos:Z

    iget-object v0, p0, Lcom/google/android/gsf/login/TermsOfServiceActivity;->mGooglePlayOptInCheckBox:Landroid/widget/CheckBox;

    invoke-virtual {v0}, Landroid/widget/CheckBox;->getVisibility()I

    move-result v0

    if-nez v0, :cond_0

    iget-object v0, p0, Lcom/google/android/gsf/loginservice/BaseActivity;->mSession:Lcom/google/android/gsf/loginservice/GLSUser$GLSSession;

    iget-object v1, p0, Lcom/google/android/gsf/login/TermsOfServiceActivity;->mGooglePlayOptInCheckBox:Landroid/widget/CheckBox;

    invoke-virtual {v1}, Landroid/widget/CheckBox;->isChecked()Z

    move-result v1

    iput-boolean v1, v0, Lcom/google/android/gsf/loginservice/GLSUser$GLSSession;->mAgreedToPlayEmail:Z

    :cond_0
    iget-boolean v0, p0, Lcom/google/android/gsf/login/TermsOfServiceActivity;->mShowChrome:Z

    if-eqz v0, :cond_1

    iget-object v0, p0, Lcom/google/android/gsf/loginservice/BaseActivity;->mSession:Lcom/google/android/gsf/loginservice/GLSUser$GLSSession;

    iput-boolean v3, v0, Lcom/google/android/gsf/loginservice/GLSUser$GLSSession;->mAgreedToChromeTosAndPrivacy:Z

    :cond_1
    const/4 v0, -0x1

    invoke-virtual {p0, v0}, Lcom/google/android/gsf/login/TermsOfServiceActivity;->setResult(I)V

    invoke-virtual {p0}, Lcom/google/android/gsf/login/TermsOfServiceActivity;->finish()V

    return-void
.end method
