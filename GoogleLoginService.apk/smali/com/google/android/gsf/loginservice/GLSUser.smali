.class public Lcom/google/android/gsf/loginservice/GLSUser;
.super Ljava/lang/Object;
.source "GLSUser.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/google/android/gsf/loginservice/GLSUser$1;,
        Lcom/google/android/gsf/loginservice/GLSUser$HttpTestInjector;,
        Lcom/google/android/gsf/loginservice/GLSUser$Status;,
        Lcom/google/android/gsf/loginservice/GLSUser$ResponseKey;,
        Lcom/google/android/gsf/loginservice/GLSUser$RequestKey;,
        Lcom/google/android/gsf/loginservice/GLSUser$GLSContext;,
        Lcom/google/android/gsf/loginservice/GLSUser$GLSSession;
    }
.end annotation


# static fields
.field private static OAUTH_PREFIX:Ljava/lang/String;

.field private static sGLSContext:Lcom/google/android/gsf/loginservice/GLSUser$GLSContext;

.field private static sWireToStatus:Ljava/util/Map;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Map",
            "<",
            "Ljava/lang/String;",
            "Lcom/google/android/gsf/loginservice/GLSUser$Status;",
            ">;"
        }
    .end annotation
.end field


# instance fields
.field private captchaAnswer:Ljava/lang/String;

.field private captchaToken:Ljava/lang/String;

.field public existing:Z

.field mAccount:Landroid/accounts/Account;

.field public mEmail:Ljava/lang/String;

.field public mEncryptedPassword:Ljava/lang/String;

.field private mHash:Ljava/lang/String;

.field public mMasterToken:Ljava/lang/String;

.field private mUpdatedPassword:Ljava/lang/String;

.field public mUseBrowserFlow:Z


# direct methods
.method static constructor <clinit>()V
    .locals 1

    const-string v0, "oauth:"

    sput-object v0, Lcom/google/android/gsf/loginservice/GLSUser;->OAUTH_PREFIX:Ljava/lang/String;

    new-instance v0, Ljava/util/HashMap;

    invoke-direct {v0}, Ljava/util/HashMap;-><init>()V

    sput-object v0, Lcom/google/android/gsf/loginservice/GLSUser;->sWireToStatus:Ljava/util/Map;

    return-void
.end method

.method private constructor <init>(Ljava/lang/String;Z)V
    .locals 6
    .param p1    # Ljava/lang/String;
    .param p2    # Z

    const/4 v5, 0x0

    invoke-direct/range {p0 .. p0}, Ljava/lang/Object;-><init>()V

    iput-object p1, p0, Lcom/google/android/gsf/loginservice/GLSUser;->mEmail:Ljava/lang/String;

    iput-boolean p2, p0, Lcom/google/android/gsf/loginservice/GLSUser;->existing:Z

    if-eqz p1, :cond_0

    new-instance v1, Landroid/accounts/Account;

    const-string v2, "com.google"

    invoke-direct {v1, p1, v2}, Landroid/accounts/Account;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    iput-object v1, p0, Lcom/google/android/gsf/loginservice/GLSUser;->mAccount:Landroid/accounts/Account;

    :cond_0
    if-eqz p2, :cond_3

    sget-object v1, Lcom/google/android/gsf/loginservice/GLSUser;->sGLSContext:Lcom/google/android/gsf/loginservice/GLSUser$GLSContext;

    iget-object v1, v1, Lcom/google/android/gsf/loginservice/GLSUser$GLSContext;->mAccountManager:Landroid/accounts/AccountManager;

    iget-object v2, p0, Lcom/google/android/gsf/loginservice/GLSUser;->mAccount:Landroid/accounts/Account;

    invoke-virtual {v1, v2}, Landroid/accounts/AccountManager;->getPassword(Landroid/accounts/Account;)Ljava/lang/String;

    move-result-object v1

    iput-object v1, p0, Lcom/google/android/gsf/loginservice/GLSUser;->mEncryptedPassword:Ljava/lang/String;

    sget-object v1, Lcom/google/android/gsf/loginservice/GLSUser;->sGLSContext:Lcom/google/android/gsf/loginservice/GLSUser$GLSContext;

    iget-object v1, v1, Lcom/google/android/gsf/loginservice/GLSUser$GLSContext;->mAccountManager:Landroid/accounts/AccountManager;

    iget-object v2, p0, Lcom/google/android/gsf/loginservice/GLSUser;->mAccount:Landroid/accounts/Account;

    const-string v3, "oauthAccessToken"

    invoke-virtual {v1, v2, v3}, Landroid/accounts/AccountManager;->getUserData(Landroid/accounts/Account;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v1

    if-nez v1, :cond_2

    const/4 v1, 0x1

    iput-boolean v1, p0, Lcom/google/android/gsf/loginservice/GLSUser;->mUseBrowserFlow:Z

    sget v1, Landroid/os/Build$VERSION;->SDK_INT:I

    const/16 v2, 0x8

    if-gt v1, v2, :cond_4

    iput-object v0, p0, Lcom/google/android/gsf/loginservice/GLSUser;->mMasterToken:Ljava/lang/String;

    iget-object v1, p0, Lcom/google/android/gsf/loginservice/GLSUser;->mEncryptedPassword:Ljava/lang/String;

    invoke-static {v1}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v1

    if-eqz v1, :cond_1

    sget-object v1, Lcom/google/android/gsf/loginservice/GLSUser;->sGLSContext:Lcom/google/android/gsf/loginservice/GLSUser$GLSContext;

    iget-object v1, v1, Lcom/google/android/gsf/loginservice/GLSUser$GLSContext;->mAccountManager:Landroid/accounts/AccountManager;

    iget-object v2, p0, Lcom/google/android/gsf/loginservice/GLSUser;->mAccount:Landroid/accounts/Account;

    invoke-virtual {v1, v2, v0}, Landroid/accounts/AccountManager;->setPassword(Landroid/accounts/Account;Ljava/lang/String;)V

    :cond_1
    iput-object v5, p0, Lcom/google/android/gsf/loginservice/GLSUser;->mEncryptedPassword:Ljava/lang/String;

    :cond_2
    :goto_0
    iget-object v1, p0, Lcom/google/android/gsf/loginservice/GLSUser;->mEncryptedPassword:Ljava/lang/String;

    if-eqz v1, :cond_3

    iget-object v1, p0, Lcom/google/android/gsf/loginservice/GLSUser;->mEncryptedPassword:Ljava/lang/String;

    const/16 v2, 0x20

    invoke-virtual {v1, v2}, Ljava/lang/String;->indexOf(I)I

    move-result v1

    if-lez v1, :cond_3

    iget-object v1, p0, Lcom/google/android/gsf/loginservice/GLSUser;->mEncryptedPassword:Ljava/lang/String;

    iput-object v1, p0, Lcom/google/android/gsf/loginservice/GLSUser;->mMasterToken:Ljava/lang/String;

    iput-object v5, p0, Lcom/google/android/gsf/loginservice/GLSUser;->mEncryptedPassword:Ljava/lang/String;

    :cond_3
    return-void

    :cond_4
    const-string v1, "1"

    invoke-virtual {v1, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-nez v1, :cond_5

    sget-object v1, Lcom/google/android/gsf/loginservice/GLSUser;->sGLSContext:Lcom/google/android/gsf/loginservice/GLSUser$GLSContext;

    iget-object v1, v1, Lcom/google/android/gsf/loginservice/GLSUser$GLSContext;->mAccountManager:Landroid/accounts/AccountManager;

    iget-object v2, p0, Lcom/google/android/gsf/loginservice/GLSUser;->mAccount:Landroid/accounts/Account;

    const-string v3, "oauthAccessToken"

    const-string v4, "1"

    invoke-virtual {v1, v2, v3, v4}, Landroid/accounts/AccountManager;->setUserData(Landroid/accounts/Account;Ljava/lang/String;Ljava/lang/String;)V

    sget-object v1, Lcom/google/android/gsf/loginservice/GLSUser;->sGLSContext:Lcom/google/android/gsf/loginservice/GLSUser$GLSContext;

    iget-object v1, v1, Lcom/google/android/gsf/loginservice/GLSUser$GLSContext;->mAccountManager:Landroid/accounts/AccountManager;

    iget-object v2, p0, Lcom/google/android/gsf/loginservice/GLSUser;->mAccount:Landroid/accounts/Account;

    invoke-virtual {v1, v2, v0}, Landroid/accounts/AccountManager;->setPassword(Landroid/accounts/Account;Ljava/lang/String;)V

    iput-object v0, p0, Lcom/google/android/gsf/loginservice/GLSUser;->mMasterToken:Ljava/lang/String;

    iput-object v5, p0, Lcom/google/android/gsf/loginservice/GLSUser;->mEncryptedPassword:Ljava/lang/String;

    goto :goto_0

    :cond_5
    iget-object v1, p0, Lcom/google/android/gsf/loginservice/GLSUser;->mEncryptedPassword:Ljava/lang/String;

    iput-object v1, p0, Lcom/google/android/gsf/loginservice/GLSUser;->mMasterToken:Ljava/lang/String;

    iput-object v5, p0, Lcom/google/android/gsf/loginservice/GLSUser;->mEncryptedPassword:Ljava/lang/String;

    goto :goto_0
.end method

.method static synthetic access$000()Lcom/google/android/gsf/loginservice/GLSUser$GLSContext;
    .locals 1

    sget-object v0, Lcom/google/android/gsf/loginservice/GLSUser;->sGLSContext:Lcom/google/android/gsf/loginservice/GLSUser$GLSContext;

    return-object v0
.end method

.method static synthetic access$100()Ljava/util/Map;
    .locals 1

    sget-object v0, Lcom/google/android/gsf/loginservice/GLSUser;->sWireToStatus:Ljava/util/Map;

    return-object v0
.end method

.method private add(Ljava/util/ArrayList;Ljava/lang/String;Ljava/lang/String;)V
    .locals 1
    .param p2    # Ljava/lang/String;
    .param p3    # Ljava/lang/String;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/ArrayList",
            "<",
            "Lorg/apache/http/NameValuePair;",
            ">;",
            "Ljava/lang/String;",
            "Ljava/lang/String;",
            ")V"
        }
    .end annotation

    invoke-static {p3}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v0

    if-nez v0, :cond_0

    new-instance v0, Lorg/apache/http/message/BasicNameValuePair;

    invoke-direct {v0, p2, p3}, Lorg/apache/http/message/BasicNameValuePair;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    invoke-virtual {p1, v0}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    :cond_0
    return-void
.end method

.method public static addPhoto(Lcom/google/android/gsf/loginservice/GLSUser$GLSSession;Lorg/json/JSONStringer;)V
    .locals 0
    .param p0    # Lcom/google/android/gsf/loginservice/GLSUser$GLSSession;
    .param p1    # Lorg/json/JSONStringer;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lorg/json/JSONException;
        }
    .end annotation

    return-void
.end method

.method private cacheKeyFromUidAndTokenType(Ljava/lang/String;I)Ljava/lang/String;
    .locals 5
    .param p1    # Ljava/lang/String;
    .param p2    # I

    const/4 v3, 0x0

    const-string v4, "SID"

    invoke-virtual {v4, p1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v4

    if-nez v4, :cond_0

    const-string v4, "LSID"

    invoke-virtual {v4, p1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v4

    if-eqz v4, :cond_1

    :cond_0
    :goto_0
    return-object p1

    :cond_1
    const-string v4, "sierra"

    invoke-virtual {v4, p1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v4

    if-nez v4, :cond_0

    const-string v4, "sierrasandbox"

    invoke-virtual {v4, p1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v4

    if-nez v4, :cond_0

    const-string v4, "android"

    invoke-virtual {v4, p1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v4

    if-nez v4, :cond_0

    const-string v4, "androidsecure"

    invoke-virtual {v4, p1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v4

    if-nez v4, :cond_0

    invoke-virtual {p0, p2}, Lcom/google/android/gsf/loginservice/GLSUser;->getAppPackage(I)Ljava/lang/String;

    move-result-object v0

    if-nez v0, :cond_2

    move-object p1, v3

    goto :goto_0

    :cond_2
    invoke-virtual {p0, v0}, Lcom/google/android/gsf/loginservice/GLSUser;->getSignatureFingerprint(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v2

    if-nez v2, :cond_3

    move-object p1, v3

    goto :goto_0

    :cond_3
    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    const-string v3, ":"

    invoke-virtual {v1, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object p1

    goto :goto_0
.end method

.method public static ensureNotOnMainThread(Landroid/content/Context;)V
    .locals 2

    invoke-static {}, Landroid/os/Looper;->myLooper()Landroid/os/Looper;

    move-result-object v0

    if-eqz v0, :cond_0

    invoke-virtual {p0}, Landroid/content/Context;->getMainLooper()Landroid/os/Looper;

    move-result-object v1

    if-ne v0, v1, :cond_0

    new-instance v0, Ljava/lang/IllegalStateException;

    const-string v1, "calling this from your main thread can lead to deadlock"

    invoke-direct {v0, v1}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    throw v0

    :cond_0
    return-void
.end method

.method private firstSave(ZLjava/lang/String;Ljava/lang/String;Ljava/lang/String;Z)V
    .locals 7
    .param p1    # Z
    .param p2    # Ljava/lang/String;
    .param p3    # Ljava/lang/String;
    .param p4    # Ljava/lang/String;
    .param p5    # Z

    const/4 v2, 0x1

    const/4 v3, 0x0

    new-instance v0, Ljava/util/HashSet;

    invoke-direct {v0}, Ljava/util/HashSet;-><init>()V

    if-eqz p4, :cond_1

    const-string v1, ","

    invoke-virtual {p4, v1}, Ljava/lang/String;->split(Ljava/lang/String;)[Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Ljava/util/Collections;->addAll(Ljava/util/Collection;[Ljava/lang/Object;)Z

    if-eqz p5, :cond_1

    const-string v1, "cl"

    invoke-virtual {v0, v1}, Ljava/util/HashSet;->contains(Ljava/lang/Object;)Z

    move-result v1

    if-nez v1, :cond_1

    const-string v1, "cl"

    invoke-virtual {v0, v1}, Ljava/util/HashSet;->add(Ljava/lang/Object;)Z

    invoke-virtual {p4}, Ljava/lang/String;->length()I

    move-result v1

    if-eqz v1, :cond_0

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v1, p4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v4, ","

    invoke-virtual {v1, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object p4

    :cond_0
    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v1, p4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v4, "cl"

    invoke-virtual {v1, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object p4

    :cond_1
    iput-boolean p1, p0, Lcom/google/android/gsf/loginservice/GLSUser;->mUseBrowserFlow:Z

    invoke-virtual {p0, p1, p4}, Lcom/google/android/gsf/loginservice/GLSUser;->addAccount(ZLjava/lang/String;)V

    if-eqz p2, :cond_2

    if-eqz p3, :cond_2

    sget-object v1, Lcom/google/android/gsf/loginservice/GLSUser;->sGLSContext:Lcom/google/android/gsf/loginservice/GLSUser$GLSContext;

    iget-object v1, v1, Lcom/google/android/gsf/loginservice/GLSUser$GLSContext;->mAccountManager:Landroid/accounts/AccountManager;

    iget-object v4, p0, Lcom/google/android/gsf/loginservice/GLSUser;->mAccount:Landroid/accounts/Account;

    const-string v5, "SID"

    invoke-virtual {v1, v4, v5, p2}, Landroid/accounts/AccountManager;->setAuthToken(Landroid/accounts/Account;Ljava/lang/String;Ljava/lang/String;)V

    sget-object v1, Lcom/google/android/gsf/loginservice/GLSUser;->sGLSContext:Lcom/google/android/gsf/loginservice/GLSUser$GLSContext;

    iget-object v1, v1, Lcom/google/android/gsf/loginservice/GLSUser$GLSContext;->mAccountManager:Landroid/accounts/AccountManager;

    iget-object v4, p0, Lcom/google/android/gsf/loginservice/GLSUser;->mAccount:Landroid/accounts/Account;

    const-string v5, "LSID"

    invoke-virtual {v1, v4, v5, p3}, Landroid/accounts/AccountManager;->setAuthToken(Landroid/accounts/Account;Ljava/lang/String;Ljava/lang/String;)V

    :cond_2
    sget-object v1, Lcom/google/android/gsf/loginservice/GLSUser;->sGLSContext:Lcom/google/android/gsf/loginservice/GLSUser$GLSContext;

    iget-object v1, v1, Lcom/google/android/gsf/loginservice/GLSUser$GLSContext;->mContext:Landroid/content/Context;

    new-instance v4, Landroid/content/Intent;

    const-string v5, "android.server.checkin.CHECKIN"

    invoke-direct {v4, v5}, Landroid/content/Intent;-><init>(Ljava/lang/String;)V

    invoke-virtual {v1, v4}, Landroid/content/Context;->sendBroadcast(Landroid/content/Intent;)V

    if-eqz p4, :cond_3

    iget-object v4, p0, Lcom/google/android/gsf/loginservice/GLSUser;->mAccount:Landroid/accounts/Account;

    const-string v5, "gmail-ls"

    const-string v1, "mail"

    invoke-virtual {v0, v1}, Ljava/util/HashSet;->contains(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_4

    move v1, v2

    :goto_0
    invoke-static {v4, v5, v1}, Landroid/content/ContentResolver;->setIsSyncable(Landroid/accounts/Account;Ljava/lang/String;I)V

    iget-object v4, p0, Lcom/google/android/gsf/loginservice/GLSUser;->mAccount:Landroid/accounts/Account;

    const-string v5, "com.android.calendar"

    const-string v1, "cl"

    invoke-virtual {v0, v1}, Ljava/util/HashSet;->contains(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_5

    move v1, v2

    :goto_1
    invoke-static {v4, v5, v1}, Landroid/content/ContentResolver;->setIsSyncable(Landroid/accounts/Account;Ljava/lang/String;I)V

    iget-object v1, p0, Lcom/google/android/gsf/loginservice/GLSUser;->mAccount:Landroid/accounts/Account;

    const-string v4, "com.android.contacts"

    iget-object v5, p0, Lcom/google/android/gsf/loginservice/GLSUser;->mAccount:Landroid/accounts/Account;

    iget-object v5, v5, Landroid/accounts/Account;->name:Ljava/lang/String;

    const-string v6, "@youtube.com"

    invoke-virtual {v5, v6}, Ljava/lang/String;->endsWith(Ljava/lang/String;)Z

    move-result v5

    if-eqz v5, :cond_6

    :goto_2
    invoke-static {v1, v4, v3}, Landroid/content/ContentResolver;->setIsSyncable(Landroid/accounts/Account;Ljava/lang/String;I)V

    :cond_3
    return-void

    :cond_4
    move v1, v3

    goto :goto_0

    :cond_5
    move v1, v3

    goto :goto_1

    :cond_6
    move v3, v2

    goto :goto_2
.end method

.method public static declared-synchronized get(Landroid/content/Context;Ljava/lang/String;)Lcom/google/android/gsf/loginservice/GLSUser;
    .locals 2
    .param p0    # Landroid/content/Context;
    .param p1    # Ljava/lang/String;

    const-class v0, Lcom/google/android/gsf/loginservice/GLSUser;

    monitor-enter v0

    const/4 v1, 0x0

    :try_start_0
    invoke-static {p0, p1, v1}, Lcom/google/android/gsf/loginservice/GLSUser;->get(Landroid/content/Context;Ljava/lang/String;Z)Lcom/google/android/gsf/loginservice/GLSUser;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    move-result-object v1

    monitor-exit v0

    return-object v1

    :catchall_0
    move-exception v1

    monitor-exit v0

    throw v1
.end method

.method public static declared-synchronized get(Landroid/content/Context;Ljava/lang/String;Z)Lcom/google/android/gsf/loginservice/GLSUser;
    .locals 9
    .param p0    # Landroid/content/Context;
    .param p1    # Ljava/lang/String;
    .param p2    # Z

    const-class v7, Lcom/google/android/gsf/loginservice/GLSUser;

    monitor-enter v7

    :try_start_0
    invoke-static {p0}, Lcom/google/android/gsf/loginservice/GLSUser;->getGLSContext(Landroid/content/Context;)Lcom/google/android/gsf/loginservice/GLSUser$GLSContext;

    if-nez p1, :cond_0

    new-instance v5, Lcom/google/android/gsf/loginservice/GLSUser;

    const/4 v6, 0x0

    const/4 v8, 0x0

    invoke-direct {v5, v6, v8}, Lcom/google/android/gsf/loginservice/GLSUser;-><init>(Ljava/lang/String;Z)V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    :goto_0
    monitor-exit v7

    return-object v5

    :cond_0
    :try_start_1
    sget-object v6, Lcom/google/android/gsf/loginservice/GLSUser;->sGLSContext:Lcom/google/android/gsf/loginservice/GLSUser$GLSContext;

    iget-object v6, v6, Lcom/google/android/gsf/loginservice/GLSUser$GLSContext;->mAccountManager:Landroid/accounts/AccountManager;

    const-string v8, "com.google"

    invoke-virtual {v6, v8}, Landroid/accounts/AccountManager;->getAccountsByType(Ljava/lang/String;)[Landroid/accounts/Account;

    move-result-object v1

    move-object v2, v1

    array-length v4, v2

    const/4 v3, 0x0

    :goto_1
    if-ge v3, v4, :cond_2

    aget-object v0, v2, v3

    iget-object v6, v0, Landroid/accounts/Account;->name:Ljava/lang/String;

    invoke-virtual {p1, v6}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v6

    if-eqz v6, :cond_1

    new-instance v5, Lcom/google/android/gsf/loginservice/GLSUser;

    const/4 v6, 0x1

    invoke-direct {v5, p1, v6}, Lcom/google/android/gsf/loginservice/GLSUser;-><init>(Ljava/lang/String;Z)V
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    goto :goto_0

    :catchall_0
    move-exception v6

    monitor-exit v7

    throw v6

    :cond_1
    add-int/lit8 v3, v3, 0x1

    goto :goto_1

    :cond_2
    :try_start_2
    new-instance v5, Lcom/google/android/gsf/loginservice/GLSUser;

    const/4 v6, 0x0

    invoke-direct {v5, p1, v6}, Lcom/google/android/gsf/loginservice/GLSUser;-><init>(Ljava/lang/String;Z)V
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    goto :goto_0
.end method

.method private getAuth(Ljava/util/Map;Ljava/lang/String;)Ljava/lang/String;
    .locals 1
    .param p2    # Ljava/lang/String;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/Map",
            "<",
            "Ljava/lang/String;",
            "Ljava/lang/String;",
            ">;",
            "Ljava/lang/String;",
            ")",
            "Ljava/lang/String;"
        }
    .end annotation

    const-string v0, "SID"

    invoke-virtual {v0, p2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_0

    const-string v0, "LSID"

    invoke-virtual {v0, p2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_1

    :cond_0
    invoke-interface {p1, p2}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/String;

    :goto_0
    return-object v0

    :cond_1
    sget-object v0, Lcom/google/android/gsf/loginservice/GLSUser$ResponseKey;->AUTH:Lcom/google/android/gsf/loginservice/GLSUser$ResponseKey;

    invoke-virtual {v0}, Lcom/google/android/gsf/loginservice/GLSUser$ResponseKey;->getWire()Ljava/lang/String;

    move-result-object v0

    invoke-interface {p1, v0}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/String;

    goto :goto_0
.end method

.method static getAuthTokenLabel(Landroid/content/Context;Ljava/lang/String;)Ljava/lang/String;
    .locals 6

    const/4 v0, 0x0

    sget-object v1, Lcom/google/android/gsf/loginservice/GLSUser;->OAUTH_PREFIX:Ljava/lang/String;

    invoke-virtual {p1, v1}, Ljava/lang/String;->startsWith(Ljava/lang/String;)Z

    move-result v1

    if-eqz v1, :cond_1

    :cond_0
    :goto_0
    return-object v0

    :cond_1
    :try_start_0
    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "com.google.android.googleapps.permission.GOOGLE_AUTH."

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {p0}, Landroid/content/Context;->getPackageManager()Landroid/content/pm/PackageManager;

    move-result-object v2

    const/4 v3, 0x0

    invoke-virtual {v2, v1, v3}, Landroid/content/pm/PackageManager;->getPermissionInfo(Ljava/lang/String;I)Landroid/content/pm/PermissionInfo;

    move-result-object v1

    iget v3, v1, Landroid/content/pm/PermissionInfo;->labelRes:I

    if-eqz v3, :cond_2

    iget-object v3, v1, Landroid/content/pm/PermissionInfo;->packageName:Ljava/lang/String;

    iget v4, v1, Landroid/content/pm/PermissionInfo;->labelRes:I

    const/4 v5, 0x0

    invoke-virtual {v2, v3, v4, v5}, Landroid/content/pm/PackageManager;->getText(Ljava/lang/String;ILandroid/content/pm/ApplicationInfo;)Ljava/lang/CharSequence;

    move-result-object v2

    if-eqz v2, :cond_2

    invoke-virtual {v2}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object v0

    goto :goto_0

    :cond_2
    iget-object v2, v1, Landroid/content/pm/PermissionInfo;->nonLocalizedLabel:Ljava/lang/CharSequence;

    if-eqz v2, :cond_3

    iget-object v1, v1, Landroid/content/pm/PermissionInfo;->nonLocalizedLabel:Ljava/lang/CharSequence;

    invoke-virtual {v1}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object v0

    goto :goto_0

    :cond_3
    iget-object v2, v1, Landroid/content/pm/PermissionInfo;->name:Ljava/lang/String;

    if-eqz v2, :cond_0

    iget-object v0, v1, Landroid/content/pm/PermissionInfo;->name:Ljava/lang/String;
    :try_end_0
    .catch Landroid/content/pm/PackageManager$NameNotFoundException; {:try_start_0 .. :try_end_0} :catch_0

    goto :goto_0

    :catch_0
    move-exception v1

    goto :goto_0
.end method

.method private getAuthtokenRaw(Ljava/lang/String;ILcom/google/android/gsf/loginservice/GLSUser$GLSSession;Landroid/os/Bundle;ZLjava/lang/String;Z)Ljava/util/Map;
    .locals 20
    .param p1    # Ljava/lang/String;
    .param p2    # I
    .param p3    # Lcom/google/android/gsf/loginservice/GLSUser$GLSSession;
    .param p4    # Landroid/os/Bundle;
    .param p5    # Z
    .param p6    # Ljava/lang/String;
    .param p7    # Z
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/String;",
            "I",
            "Lcom/google/android/gsf/loginservice/GLSUser$GLSSession;",
            "Landroid/os/Bundle;",
            "Z",
            "Ljava/lang/String;",
            "Z)",
            "Ljava/util/Map",
            "<",
            "Ljava/lang/String;",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation

    new-instance v18, Ljava/util/HashMap;

    invoke-direct/range {v18 .. v18}, Ljava/util/HashMap;-><init>()V

    if-nez p3, :cond_0

    const-string v3, "GLSUser"

    const-string v4, "No session"

    invoke-static {v3, v4}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;)I

    sget-object v3, Lcom/google/android/gsf/loginservice/GLSUser;->sGLSContext:Lcom/google/android/gsf/loginservice/GLSUser$GLSContext;

    invoke-virtual {v3}, Lcom/google/android/gsf/loginservice/GLSUser$GLSContext;->newSession()Lcom/google/android/gsf/loginservice/GLSUser$GLSSession;

    move-result-object p3

    :cond_0
    const/4 v3, 0x0

    move-object/from16 v0, p3

    iput-object v3, v0, Lcom/google/android/gsf/loginservice/GLSUser$GLSSession;->mDetail:Ljava/lang/String;

    const/4 v3, 0x0

    move-object/from16 v0, p3

    iput-object v3, v0, Lcom/google/android/gsf/loginservice/GLSUser$GLSSession;->mError:Lcom/google/android/gsf/loginservice/GLSUser$Status;

    invoke-direct/range {p0 .. p0}, Lcom/google/android/gsf/loginservice/GLSUser;->hasSecret()Z

    move-result v3

    if-nez v3, :cond_1

    invoke-static/range {p6 .. p6}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v3

    if-eqz v3, :cond_1

    sget-object v3, Lcom/google/android/gsf/loginservice/GLSUser;->sGLSContext:Lcom/google/android/gsf/loginservice/GLSUser$GLSContext;

    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    const-string v5, "No secrets - returning BAD_AUTH "

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    move-object/from16 v0, p0

    iget-object v5, v0, Lcom/google/android/gsf/loginservice/GLSUser;->mEmail:Ljava/lang/String;

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    const-string v5, " "

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    move-object/from16 v0, p1

    invoke-virtual {v4, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v3, v4}, Lcom/google/android/gsf/loginservice/GLSUser$GLSContext;->log(Ljava/lang/String;)V

    sget-object v3, Lcom/google/android/gsf/loginservice/GLSUser$ResponseKey;->STATUS:Lcom/google/android/gsf/loginservice/GLSUser$ResponseKey;

    invoke-virtual {v3}, Lcom/google/android/gsf/loginservice/GLSUser$ResponseKey;->getWire()Ljava/lang/String;

    move-result-object v3

    sget-object v4, Lcom/google/android/gsf/loginservice/GLSUser$Status;->BAD_AUTHENTICATION:Lcom/google/android/gsf/loginservice/GLSUser$Status;

    invoke-virtual {v4}, Lcom/google/android/gsf/loginservice/GLSUser$Status;->getWire()Ljava/lang/String;

    move-result-object v4

    move-object/from16 v0, v18

    invoke-interface {v0, v3, v4}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    move-object/from16 v19, v18

    :goto_0
    return-object v19

    :cond_1
    new-instance v9, Ljava/util/ArrayList;

    invoke-direct {v9}, Ljava/util/ArrayList;-><init>()V

    move-object/from16 v3, p0

    move-object/from16 v4, p1

    move/from16 v5, p2

    move-object/from16 v6, p3

    move-object/from16 v7, p4

    move/from16 v8, p5

    move-object/from16 v10, p6

    move/from16 v11, p7

    invoke-direct/range {v3 .. v11}, Lcom/google/android/gsf/loginservice/GLSUser;->prepareRequest(Ljava/lang/String;ILcom/google/android/gsf/loginservice/GLSUser$GLSSession;Landroid/os/Bundle;ZLjava/util/ArrayList;Ljava/lang/String;Z)V

    const/4 v13, 0x0

    :try_start_0
    new-instance v13, Lorg/apache/http/client/entity/UrlEncodedFormEntity;

    invoke-direct {v13, v9}, Lorg/apache/http/client/entity/UrlEncodedFormEntity;-><init>(Ljava/util/List;)V
    :try_end_0
    .catch Ljava/io/UnsupportedEncodingException; {:try_start_0 .. :try_end_0} :catch_0

    const/4 v14, 0x0

    :try_start_1
    sget-object v3, Lcom/google/android/gsf/loginservice/GLSUser;->sGLSContext:Lcom/google/android/gsf/loginservice/GLSUser$GLSContext;

    const-string v4, "https://android.clients.google.com/auth"

    invoke-interface {v13}, Lorg/apache/http/HttpEntity;->getContentType()Lorg/apache/http/Header;

    move-result-object v5

    new-instance v6, Ljava/lang/StringBuilder;

    invoke-direct {v6}, Ljava/lang/StringBuilder;-><init>()V

    move-object/from16 v0, p0

    iget-object v7, v0, Lcom/google/android/gsf/loginservice/GLSUser;->mEmail:Ljava/lang/String;

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    const-string v7, ":"

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    move-object/from16 v0, p1

    invoke-virtual {v6, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v6}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v6

    invoke-virtual {v3, v4, v13, v5, v6}, Lcom/google/android/gsf/loginservice/GLSUser$GLSContext;->httpPost(Ljava/lang/String;Lorg/apache/http/HttpEntity;Lorg/apache/http/Header;Ljava/lang/String;)Lorg/apache/http/HttpResponse;

    move-result-object v15

    invoke-interface {v15}, Lorg/apache/http/HttpResponse;->getEntity()Lorg/apache/http/HttpEntity;

    move-result-object v3

    invoke-static {v3}, Lorg/apache/http/util/EntityUtils;->toString(Lorg/apache/http/HttpEntity;)Ljava/lang/String;

    move-result-object v16

    invoke-interface {v15}, Lorg/apache/http/HttpResponse;->getStatusLine()Lorg/apache/http/StatusLine;

    move-result-object v3

    invoke-interface {v3}, Lorg/apache/http/StatusLine;->getStatusCode()I

    move-result v17

    const v3, 0x320d2

    move-object/from16 v0, p1

    invoke-static {v3, v0}, Lcom/google/android/gsf/login/Compat;->eventLogWriteEvent(ILjava/lang/String;)V

    move-object/from16 v0, p0

    move-object/from16 v1, v16

    invoke-direct {v0, v1}, Lcom/google/android/gsf/loginservice/GLSUser;->parseResponse(Ljava/lang/String;)Ljava/util/Map;

    move-result-object v18

    const-string v3, "x-status"

    invoke-static/range {v17 .. v17}, Ljava/lang/Integer;->toString(I)Ljava/lang/String;

    move-result-object v4

    move-object/from16 v0, v18

    invoke-interface {v0, v3, v4}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    const/4 v3, 0x0

    move-object/from16 v0, p0

    move-object/from16 v1, v18

    move/from16 v2, v17

    invoke-direct {v0, v9, v1, v3, v2}, Lcom/google/android/gsf/loginservice/GLSUser;->logRequest(Ljava/util/ArrayList;Ljava/util/Map;Ljava/lang/String;I)V

    sget-object v3, Lcom/google/android/gsf/loginservice/GLSUser$ResponseKey;->STATUS:Lcom/google/android/gsf/loginservice/GLSUser$ResponseKey;

    invoke-virtual {v3}, Lcom/google/android/gsf/loginservice/GLSUser$ResponseKey;->getWire()Ljava/lang/String;

    move-result-object v3

    move-object/from16 v0, v18

    invoke-interface {v0, v3}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v3

    move-object v0, v3

    check-cast v0, Ljava/lang/String;

    move-object v14, v0
    :try_end_1
    .catch Ljava/io/IOException; {:try_start_1 .. :try_end_1} :catch_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    const v4, 0x320d2

    const/4 v3, 0x2

    new-array v5, v3, [Ljava/lang/Object;

    const/4 v6, 0x0

    if-nez v14, :cond_3

    const-string v3, "OK"

    :goto_1
    aput-object v3, v5, v6

    const/4 v3, 0x1

    aput-object p1, v5, v3

    invoke-static {v4, v5}, Landroid/util/EventLog;->writeEvent(I[Ljava/lang/Object;)I

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string v4, "getAuthtoken("

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/google/android/gsf/loginservice/GLSUser;->mEmail:Ljava/lang/String;

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    const-string v4, ", "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    move-object/from16 v0, p1

    invoke-virtual {v3, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    const-string v4, ") -> "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    if-nez v14, :cond_2

    const-string v14, "ok"

    :cond_2
    invoke-virtual {v3, v14}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    move-object/from16 v0, p0

    invoke-direct {v0, v3}, Lcom/google/android/gsf/loginservice/GLSUser;->log(Ljava/lang/String;)V

    move-object/from16 v19, v18

    goto/16 :goto_0

    :catch_0
    move-exception v12

    new-instance v3, Ljava/lang/AssertionError;

    invoke-direct {v3, v12}, Ljava/lang/AssertionError;-><init>(Ljava/lang/Object;)V

    throw v3

    :cond_3
    move-object v3, v14

    goto :goto_1

    :catch_1
    move-exception v12

    :try_start_2
    sget-object v3, Lcom/google/android/gsf/loginservice/GLSUser$Status;->NETWORK_ERROR:Lcom/google/android/gsf/loginservice/GLSUser$Status;

    invoke-virtual {v3}, Lcom/google/android/gsf/loginservice/GLSUser$Status;->getWire()Ljava/lang/String;

    move-result-object v14

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string v4, "IOException in getAuthtoken("

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/google/android/gsf/loginservice/GLSUser;->mEmail:Ljava/lang/String;

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    const-string v4, ", "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    move-object/from16 v0, p1

    invoke-virtual {v3, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    const-string v4, " -> "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v12}, Ljava/io/IOException;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    move-object/from16 v0, p0

    invoke-direct {v0, v3}, Lcom/google/android/gsf/loginservice/GLSUser;->log(Ljava/lang/String;)V

    sget-object v3, Lcom/google/android/gsf/loginservice/GLSUser$ResponseKey;->STATUS:Lcom/google/android/gsf/loginservice/GLSUser$ResponseKey;

    invoke-virtual {v3}, Lcom/google/android/gsf/loginservice/GLSUser$ResponseKey;->getWire()Ljava/lang/String;

    move-result-object v3

    sget-object v4, Lcom/google/android/gsf/loginservice/GLSUser$Status;->NETWORK_ERROR:Lcom/google/android/gsf/loginservice/GLSUser$Status;

    invoke-virtual {v4}, Lcom/google/android/gsf/loginservice/GLSUser$Status;->getWire()Ljava/lang/String;

    move-result-object v4

    move-object/from16 v0, v18

    invoke-interface {v0, v3, v4}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    const/4 v3, 0x0

    invoke-virtual {v12}, Ljava/io/IOException;->toString()Ljava/lang/String;

    move-result-object v4

    const/4 v5, 0x0

    move-object/from16 v0, p0

    invoke-direct {v0, v9, v3, v4, v5}, Lcom/google/android/gsf/loginservice/GLSUser;->logRequest(Ljava/util/ArrayList;Ljava/util/Map;Ljava/lang/String;I)V
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    const v4, 0x320d2

    const/4 v3, 0x2

    new-array v5, v3, [Ljava/lang/Object;

    const/4 v6, 0x0

    if-nez v14, :cond_5

    const-string v3, "OK"

    :goto_2
    aput-object v3, v5, v6

    const/4 v3, 0x1

    aput-object p1, v5, v3

    invoke-static {v4, v5}, Landroid/util/EventLog;->writeEvent(I[Ljava/lang/Object;)I

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string v4, "getAuthtoken("

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/google/android/gsf/loginservice/GLSUser;->mEmail:Ljava/lang/String;

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    const-string v4, ", "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    move-object/from16 v0, p1

    invoke-virtual {v3, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    const-string v4, ") -> "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    if-nez v14, :cond_4

    const-string v14, "ok"

    :cond_4
    invoke-virtual {v3, v14}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    move-object/from16 v0, p0

    invoke-direct {v0, v3}, Lcom/google/android/gsf/loginservice/GLSUser;->log(Ljava/lang/String;)V

    move-object/from16 v19, v18

    goto/16 :goto_0

    :cond_5
    move-object v3, v14

    goto :goto_2

    :catchall_0
    move-exception v3

    move-object v4, v3

    const v5, 0x320d2

    const/4 v3, 0x2

    new-array v6, v3, [Ljava/lang/Object;

    const/4 v7, 0x0

    if-nez v14, :cond_7

    const-string v3, "OK"

    :goto_3
    aput-object v3, v6, v7

    const/4 v3, 0x1

    aput-object p1, v6, v3

    invoke-static {v5, v6}, Landroid/util/EventLog;->writeEvent(I[Ljava/lang/Object;)I

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string v5, "getAuthtoken("

    invoke-virtual {v3, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    move-object/from16 v0, p0

    iget-object v5, v0, Lcom/google/android/gsf/loginservice/GLSUser;->mEmail:Ljava/lang/String;

    invoke-virtual {v3, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    const-string v5, ", "

    invoke-virtual {v3, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    move-object/from16 v0, p1

    invoke-virtual {v3, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    const-string v5, ") -> "

    invoke-virtual {v3, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    if-nez v14, :cond_6

    const-string v14, "ok"

    :cond_6
    invoke-virtual {v3, v14}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    move-object/from16 v0, p0

    invoke-direct {v0, v3}, Lcom/google/android/gsf/loginservice/GLSUser;->log(Ljava/lang/String;)V

    throw v4

    :cond_7
    move-object v3, v14

    goto :goto_3
.end method

.method public static getError(Landroid/content/Intent;)Lcom/google/android/gsf/loginservice/GLSUser$Status;
    .locals 1
    .param p0    # Landroid/content/Intent;

    sget-object v0, Lcom/google/android/gsf/loginservice/GLSUser;->sGLSContext:Lcom/google/android/gsf/loginservice/GLSUser$GLSContext;

    iget-object v0, v0, Lcom/google/android/gsf/loginservice/GLSUser$GLSContext;->mContext:Landroid/content/Context;

    invoke-static {v0, p0}, Lcom/google/android/gsf/loginservice/GLSUser$Status;->fromIntent(Landroid/content/Context;Landroid/content/Intent;)Lcom/google/android/gsf/loginservice/GLSUser$Status;

    move-result-object v0

    return-object v0
.end method

.method public static declared-synchronized getGLSContext(Landroid/content/Context;)Lcom/google/android/gsf/loginservice/GLSUser$GLSContext;
    .locals 7
    .param p0    # Landroid/content/Context;

    const-class v6, Lcom/google/android/gsf/loginservice/GLSUser;

    monitor-enter v6

    :try_start_0
    sget-object v0, Lcom/google/android/gsf/loginservice/GLSUser;->sGLSContext:Lcom/google/android/gsf/loginservice/GLSUser$GLSContext;

    if-nez v0, :cond_1

    invoke-virtual {p0}, Landroid/content/Context;->getApplicationContext()Landroid/content/Context;

    move-result-object v0

    if-eqz v0, :cond_0

    invoke-virtual {p0}, Landroid/content/Context;->getApplicationContext()Landroid/content/Context;

    move-result-object p0

    :cond_0
    new-instance v0, Lcom/google/android/gsf/loginservice/GLSUser$GLSContext;

    invoke-static {p0}, Landroid/accounts/AccountManager;->get(Landroid/content/Context;)Landroid/accounts/AccountManager;

    move-result-object v2

    invoke-virtual {p0}, Landroid/content/Context;->getPackageManager()Landroid/content/pm/PackageManager;

    move-result-object v3

    invoke-static {p0}, Lcom/google/android/gsf/loginservice/GLSUser;->getHttpClient(Landroid/content/Context;)Lcom/google/android/common/http/GoogleHttpClient;

    move-result-object v4

    const/4 v5, 0x0

    move-object v1, p0

    invoke-direct/range {v0 .. v5}, Lcom/google/android/gsf/loginservice/GLSUser$GLSContext;-><init>(Landroid/content/Context;Landroid/accounts/AccountManager;Landroid/content/pm/PackageManager;Lorg/apache/http/client/HttpClient;Lcom/google/android/gsf/loginservice/GLSUser$1;)V

    sput-object v0, Lcom/google/android/gsf/loginservice/GLSUser;->sGLSContext:Lcom/google/android/gsf/loginservice/GLSUser$GLSContext;

    :cond_1
    sget-object v0, Lcom/google/android/gsf/loginservice/GLSUser;->sGLSContext:Lcom/google/android/gsf/loginservice/GLSUser$GLSContext;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    monitor-exit v6

    return-object v0

    :catchall_0
    move-exception v0

    monitor-exit v6

    throw v0
.end method

.method static getHttpClient(Landroid/content/Context;)Lcom/google/android/common/http/GoogleHttpClient;
    .locals 5
    .param p0    # Landroid/content/Context;

    const/16 v4, 0x7530

    new-instance v0, Lcom/google/android/common/http/GoogleHttpClient;

    const-string v2, "GoogleLoginService/1.3"

    const/4 v3, 0x0

    invoke-direct {v0, p0, v2, v3}, Lcom/google/android/common/http/GoogleHttpClient;-><init>(Landroid/content/Context;Ljava/lang/String;Z)V

    invoke-virtual {v0}, Lcom/google/android/common/http/GoogleHttpClient;->getParams()Lorg/apache/http/params/HttpParams;

    move-result-object v1

    invoke-static {v1, v4}, Lorg/apache/http/params/HttpConnectionParams;->setConnectionTimeout(Lorg/apache/http/params/HttpParams;I)V

    invoke-static {v1, v4}, Lorg/apache/http/params/HttpConnectionParams;->setSoTimeout(Lorg/apache/http/params/HttpParams;I)V

    const-wide/16 v2, 0x7530

    invoke-static {v1, v2, v3}, Lorg/apache/http/conn/params/ConnManagerParams;->setTimeout(Lorg/apache/http/params/HttpParams;J)V

    return-object v0
.end method

.method public static getOrCreateSession(Landroid/content/Context;Landroid/os/Bundle;)Lcom/google/android/gsf/loginservice/GLSUser$GLSSession;
    .locals 2

    if-nez p1, :cond_0

    const/4 v0, 0x0

    :goto_0
    if-eqz v0, :cond_1

    invoke-static {p0}, Lcom/google/android/gsf/loginservice/GLSUser;->getGLSContext(Landroid/content/Context;)Lcom/google/android/gsf/loginservice/GLSUser$GLSContext;

    move-result-object v1

    invoke-virtual {v1, v0}, Lcom/google/android/gsf/loginservice/GLSUser$GLSContext;->getSession(Ljava/lang/String;)Lcom/google/android/gsf/loginservice/GLSUser$GLSSession;

    move-result-object v0

    :goto_1
    return-object v0

    :cond_0
    const-string v0, "session"

    invoke-virtual {p1, v0}, Landroid/os/Bundle;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    goto :goto_0

    :cond_1
    const-string v0, "GLSUser"

    const-string v1, "GLS Activity without session"

    invoke-static {v0, v1}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;)I

    invoke-static {p0}, Lcom/google/android/gsf/loginservice/GLSUser;->getGLSContext(Landroid/content/Context;)Lcom/google/android/gsf/loginservice/GLSUser$GLSContext;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/android/gsf/loginservice/GLSUser$GLSContext;->newSession()Lcom/google/android/gsf/loginservice/GLSUser$GLSSession;

    move-result-object v0

    goto :goto_1
.end method

.method private getPermissionKey(ILjava/lang/String;)Ljava/lang/String;
    .locals 3

    const/4 v0, 0x0

    invoke-virtual {p0, p1}, Lcom/google/android/gsf/loginservice/GLSUser;->getAppPackage(I)Ljava/lang/String;

    move-result-object v1

    if-eqz v1, :cond_0

    invoke-virtual {p0, v1}, Lcom/google/android/gsf/loginservice/GLSUser;->getSignatureFingerprint(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    :cond_0
    if-nez v0, :cond_1

    invoke-static {p1}, Ljava/lang/Integer;->toString(I)Ljava/lang/String;

    move-result-object v0

    :cond_1
    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "perm."

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, "."

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0, p2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public static getUid(Ljava/lang/String;)I
    .locals 5
    .param p0    # Ljava/lang/String;

    const/4 v2, 0x0

    if-nez p0, :cond_0

    :goto_0
    return v2

    :cond_0
    :try_start_0
    sget-object v3, Lcom/google/android/gsf/loginservice/GLSUser;->sGLSContext:Lcom/google/android/gsf/loginservice/GLSUser$GLSContext;

    iget-object v3, v3, Lcom/google/android/gsf/loginservice/GLSUser$GLSContext;->mPM:Landroid/content/pm/PackageManager;

    const/16 v4, 0x80

    invoke-virtual {v3, p0, v4}, Landroid/content/pm/PackageManager;->getPackageInfo(Ljava/lang/String;I)Landroid/content/pm/PackageInfo;

    move-result-object v1

    iget-object v3, v1, Landroid/content/pm/PackageInfo;->applicationInfo:Landroid/content/pm/ApplicationInfo;

    iget v2, v3, Landroid/content/pm/ApplicationInfo;->uid:I
    :try_end_0
    .catch Landroid/content/pm/PackageManager$NameNotFoundException; {:try_start_0 .. :try_end_0} :catch_0

    goto :goto_0

    :catch_0
    move-exception v0

    goto :goto_0
.end method

.method private handlePermission(Ljava/util/Map;ILjava/lang/String;Landroid/os/Bundle;ZLcom/google/android/gsf/loginservice/GLSUser$GLSSession;Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;
    .locals 7
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/Map",
            "<",
            "Ljava/lang/String;",
            "Ljava/lang/String;",
            ">;I",
            "Ljava/lang/String;",
            "Landroid/os/Bundle;",
            "Z",
            "Lcom/google/android/gsf/loginservice/GLSUser$GLSSession;",
            "Ljava/lang/String;",
            "Ljava/lang/String;",
            ")",
            "Landroid/content/Intent;"
        }
    .end annotation

    const/4 v1, 0x0

    sget-object v0, Lcom/google/android/gsf/loginservice/GLSUser$ResponseKey;->PERMISSION_ADVICE:Lcom/google/android/gsf/loginservice/GLSUser$ResponseKey;

    invoke-virtual {v0}, Lcom/google/android/gsf/loginservice/GLSUser$ResponseKey;->getWire()Ljava/lang/String;

    move-result-object v0

    invoke-interface {p1, v0}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/String;

    if-nez v0, :cond_2

    if-nez p5, :cond_0

    if-nez p8, :cond_1

    if-eqz p7, :cond_1

    :cond_0
    move-object v0, v1

    :goto_0
    return-object v0

    :cond_1
    sget-object v2, Lcom/google/android/gsf/loginservice/GLSUser$Status;->NEED_PERMISSION:Lcom/google/android/gsf/loginservice/GLSUser$Status;

    move-object v0, p0

    move-object v1, p1

    move v3, p2

    move-object v4, p3

    move-object v5, p6

    move-object v6, p4

    invoke-virtual/range {v0 .. v6}, Lcom/google/android/gsf/loginservice/GLSUser;->errorIntent(Ljava/util/Map;Lcom/google/android/gsf/loginservice/GLSUser$Status;ILjava/lang/String;Lcom/google/android/gsf/loginservice/GLSUser$GLSSession;Landroid/os/Bundle;)Landroid/content/Intent;

    move-result-object v0

    goto :goto_0

    :cond_2
    const-string v2, "auto"

    invoke-virtual {v2, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_3

    const-string v0, "1"

    invoke-virtual {p0, p3, p2, v0, v1}, Lcom/google/android/gsf/loginservice/GLSUser;->setStoredPermission(Ljava/lang/String;ILjava/lang/String;Landroid/os/Bundle;)V

    move-object v0, v1

    goto :goto_0

    :cond_3
    sget-object v2, Lcom/google/android/gsf/loginservice/GLSUser$Status;->NEED_PERMISSION:Lcom/google/android/gsf/loginservice/GLSUser$Status;

    move-object v0, p0

    move-object v1, p1

    move v3, p2

    move-object v4, p3

    move-object v5, p6

    move-object v6, p4

    invoke-virtual/range {v0 .. v6}, Lcom/google/android/gsf/loginservice/GLSUser;->errorIntent(Ljava/util/Map;Lcom/google/android/gsf/loginservice/GLSUser$Status;ILjava/lang/String;Lcom/google/android/gsf/loginservice/GLSUser$GLSSession;Landroid/os/Bundle;)Landroid/content/Intent;

    move-result-object v0

    goto :goto_0
.end method

.method private hasPermission(Ljava/lang/String;I)Z
    .locals 4
    .param p1    # Ljava/lang/String;
    .param p2    # I

    const/4 v2, 0x1

    invoke-direct {p0, p2}, Lcom/google/android/gsf/loginservice/GLSUser;->inSystemImage(I)Z

    move-result v1

    if-eqz v1, :cond_1

    :cond_0
    :goto_0
    return v2

    :cond_1
    sget-object v3, Lcom/google/android/gsf/loginservice/GLSUser;->sGLSContext:Lcom/google/android/gsf/loginservice/GLSUser$GLSContext;

    iget-object v3, v3, Lcom/google/android/gsf/loginservice/GLSUser$GLSContext;->mContext:Landroid/content/Context;

    invoke-virtual {v3}, Landroid/content/Context;->getApplicationInfo()Landroid/content/pm/ApplicationInfo;

    move-result-object v3

    iget v0, v3, Landroid/content/pm/ApplicationInfo;->uid:I

    if-eq v0, p2, :cond_2

    invoke-direct {p0, v0, p2}, Lcom/google/android/gsf/loginservice/GLSUser;->sameSignature(II)Z

    move-result v3

    if-eqz v3, :cond_3

    :cond_2
    sget-object v3, Lcom/google/android/gsf/loginservice/GLSUser;->sGLSContext:Lcom/google/android/gsf/loginservice/GLSUser$GLSContext;

    iget-object v3, v3, Lcom/google/android/gsf/loginservice/GLSUser$GLSContext;->mTestNoPermission:Ljava/lang/String;

    invoke-virtual {p1, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-eqz v3, :cond_0

    :cond_3
    invoke-virtual {p0, p1, p2}, Lcom/google/android/gsf/loginservice/GLSUser;->getStoredPermission(Ljava/lang/String;I)Z

    move-result v2

    goto :goto_0
.end method

.method private hasSecret()Z
    .locals 1

    iget-object v0, p0, Lcom/google/android/gsf/loginservice/GLSUser;->mMasterToken:Ljava/lang/String;

    invoke-static {v0}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v0

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/google/android/gsf/loginservice/GLSUser;->mEncryptedPassword:Ljava/lang/String;

    invoke-static {v0}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v0

    if-nez v0, :cond_1

    :cond_0
    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_1
    const/4 v0, 0x0

    goto :goto_0
.end method

.method private inSystemImage(I)Z
    .locals 10
    .param p1    # I

    const/4 v7, 0x0

    sget-object v8, Lcom/google/android/gsf/loginservice/GLSUser;->sGLSContext:Lcom/google/android/gsf/loginservice/GLSUser$GLSContext;

    iget-object v8, v8, Lcom/google/android/gsf/loginservice/GLSUser$GLSContext;->mPM:Landroid/content/pm/PackageManager;

    invoke-virtual {v8, p1}, Landroid/content/pm/PackageManager;->getPackagesForUid(I)[Ljava/lang/String;

    move-result-object v6

    move-object v0, v6

    array-length v3, v0

    const/4 v2, 0x0

    :goto_0
    if-ge v2, v3, :cond_0

    aget-object v4, v0, v2

    :try_start_0
    sget-object v8, Lcom/google/android/gsf/loginservice/GLSUser;->sGLSContext:Lcom/google/android/gsf/loginservice/GLSUser$GLSContext;

    iget-object v8, v8, Lcom/google/android/gsf/loginservice/GLSUser$GLSContext;->mPM:Landroid/content/pm/PackageManager;

    const/4 v9, 0x0

    invoke-virtual {v8, v4, v9}, Landroid/content/pm/PackageManager;->getPackageInfo(Ljava/lang/String;I)Landroid/content/pm/PackageInfo;

    move-result-object v5

    if-eqz v5, :cond_1

    iget-object v8, v5, Landroid/content/pm/PackageInfo;->applicationInfo:Landroid/content/pm/ApplicationInfo;

    iget v8, v8, Landroid/content/pm/ApplicationInfo;->flags:I
    :try_end_0
    .catch Landroid/content/pm/PackageManager$NameNotFoundException; {:try_start_0 .. :try_end_0} :catch_0

    and-int/lit8 v8, v8, 0x1

    if-eqz v8, :cond_1

    const/4 v7, 0x1

    :cond_0
    :goto_1
    return v7

    :catch_0
    move-exception v1

    goto :goto_1

    :cond_1
    add-int/lit8 v2, v2, 0x1

    goto :goto_0
.end method

.method private log(Ljava/lang/String;)V
    .locals 1
    .param p1    # Ljava/lang/String;

    sget-object v0, Lcom/google/android/gsf/loginservice/GLSUser;->sGLSContext:Lcom/google/android/gsf/loginservice/GLSUser$GLSContext;

    if-eqz v0, :cond_0

    sget-object v0, Lcom/google/android/gsf/loginservice/GLSUser;->sGLSContext:Lcom/google/android/gsf/loginservice/GLSUser$GLSContext;

    invoke-virtual {v0, p1}, Lcom/google/android/gsf/loginservice/GLSUser$GLSContext;->log(Ljava/lang/String;)V

    :cond_0
    return-void
.end method

.method private logRequest(Ljava/util/ArrayList;Ljava/util/Map;Ljava/lang/String;I)V
    .locals 7
    .param p3    # Ljava/lang/String;
    .param p4    # I
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/ArrayList",
            "<",
            "Lorg/apache/http/NameValuePair;",
            ">;",
            "Ljava/util/Map",
            "<",
            "Ljava/lang/String;",
            "Ljava/lang/String;",
            ">;",
            "Ljava/lang/String;",
            "I)V"
        }
    .end annotation

    new-instance v3, Ljava/lang/StringBuffer;

    invoke-direct {v3}, Ljava/lang/StringBuffer;-><init>()V

    const-string v5, " Request: "

    invoke-virtual {v3, v5}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    invoke-virtual {p1}, Ljava/util/ArrayList;->iterator()Ljava/util/Iterator;

    move-result-object v0

    :cond_0
    :goto_0
    invoke-interface {v0}, Ljava/util/Iterator;->hasNext()Z

    move-result v5

    if-eqz v5, :cond_1

    invoke-interface {v0}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v4

    check-cast v4, Lorg/apache/http/NameValuePair;

    invoke-interface {v4}, Lorg/apache/http/NameValuePair;->getName()Ljava/lang/String;

    move-result-object v2

    sget-object v5, Lcom/google/android/gsf/loginservice/GLSUser$RequestKey;->TOKEN:Lcom/google/android/gsf/loginservice/GLSUser$RequestKey;

    invoke-virtual {v5}, Lcom/google/android/gsf/loginservice/GLSUser$RequestKey;->getWire()Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v5, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v5

    if-nez v5, :cond_0

    sget-object v5, Lcom/google/android/gsf/loginservice/GLSUser$RequestKey;->ENCRYPTED_PASSWORD:Lcom/google/android/gsf/loginservice/GLSUser$RequestKey;

    invoke-virtual {v5}, Lcom/google/android/gsf/loginservice/GLSUser$RequestKey;->getWire()Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v5, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v5

    if-nez v5, :cond_0

    invoke-virtual {v3, v2}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    move-result-object v5

    const-string v6, "="

    invoke-virtual {v5, v6}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    move-result-object v5

    invoke-interface {v4}, Lorg/apache/http/NameValuePair;->getValue()Ljava/lang/String;

    move-result-object v6

    invoke-virtual {v5, v6}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    move-result-object v5

    const-string v6, "&"

    invoke-virtual {v5, v6}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    goto :goto_0

    :cond_1
    new-instance v5, Ljava/lang/StringBuilder;

    invoke-direct {v5}, Ljava/lang/StringBuilder;-><init>()V

    const-string v6, " RESULT: "

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5, p4}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v5

    const-string v6, " {"

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v3, v5}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    if-eqz p2, :cond_2

    invoke-interface {p2}, Ljava/util/Map;->keySet()Ljava/util/Set;

    move-result-object v5

    invoke-interface {v5}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    move-result-object v0

    :goto_1
    invoke-interface {v0}, Ljava/util/Iterator;->hasNext()Z

    move-result v5

    if-eqz v5, :cond_2

    invoke-interface {v0}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Ljava/lang/String;

    invoke-virtual {v3, v1}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    move-result-object v5

    const-string v6, "="

    invoke-virtual {v5, v6}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    move-result-object v6

    invoke-interface {p2, v1}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v5

    check-cast v5, Ljava/lang/String;

    invoke-virtual {v6, v5}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    move-result-object v5

    const-string v6, ";"

    invoke-virtual {v5, v6}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    goto :goto_1

    :cond_2
    if-eqz p3, :cond_3

    new-instance v5, Ljava/lang/StringBuilder;

    invoke-direct {v5}, Ljava/lang/StringBuilder;-><init>()V

    const-string v6, " } Message: "

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5, p3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v3, v5}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    :cond_3
    invoke-virtual {v3}, Ljava/lang/StringBuffer;->toString()Ljava/lang/String;

    move-result-object v5

    invoke-direct {p0, v5}, Lcom/google/android/gsf/loginservice/GLSUser;->log(Ljava/lang/String;)V

    return-void
.end method

.method private parseResponse(Ljava/lang/String;)Ljava/util/Map;
    .locals 8
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/String;",
            ")",
            "Ljava/util/Map",
            "<",
            "Ljava/lang/String;",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation

    const/4 v1, 0x0

    new-instance v2, Ljava/util/HashMap;

    invoke-direct {v2}, Ljava/util/HashMap;-><init>()V

    const-string v0, "\n"

    invoke-virtual {p1, v0}, Ljava/lang/String;->split(Ljava/lang/String;)[Ljava/lang/String;

    move-result-object v3

    array-length v4, v3

    move v0, v1

    :goto_0
    if-ge v0, v4, :cond_1

    aget-object v5, v3, v0

    const-string v6, "="

    invoke-virtual {v5, v6}, Ljava/lang/String;->indexOf(Ljava/lang/String;)I

    move-result v6

    if-ltz v6, :cond_0

    invoke-virtual {v5, v1, v6}, Ljava/lang/String;->substring(II)Ljava/lang/String;

    move-result-object v7

    add-int/lit8 v6, v6, 0x1

    invoke-virtual {v5, v6}, Ljava/lang/String;->substring(I)Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v2, v7, v5}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    :cond_0
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    :cond_1
    return-object v2
.end method

.method private prepareRequest(Ljava/lang/String;ILcom/google/android/gsf/loginservice/GLSUser$GLSSession;Landroid/os/Bundle;ZLjava/util/ArrayList;Ljava/lang/String;Z)V
    .locals 10
    .param p1    # Ljava/lang/String;
    .param p2    # I
    .param p3    # Lcom/google/android/gsf/loginservice/GLSUser$GLSSession;
    .param p4    # Landroid/os/Bundle;
    .param p5    # Z
    .param p7    # Ljava/lang/String;
    .param p8    # Z
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/String;",
            "I",
            "Lcom/google/android/gsf/loginservice/GLSUser$GLSSession;",
            "Landroid/os/Bundle;",
            "Z",
            "Ljava/util/ArrayList",
            "<",
            "Lorg/apache/http/NameValuePair;",
            ">;",
            "Ljava/lang/String;",
            "Z)V"
        }
    .end annotation

    new-instance v7, Lorg/apache/http/message/BasicNameValuePair;

    const-string v8, "accountType"

    const-string v9, "HOSTED_OR_GOOGLE"

    invoke-direct {v7, v8, v9}, Lorg/apache/http/message/BasicNameValuePair;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    move-object/from16 v0, p6

    invoke-virtual {v0, v7}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    iget-object v7, p0, Lcom/google/android/gsf/loginservice/GLSUser;->mEmail:Ljava/lang/String;

    if-eqz v7, :cond_0

    new-instance v7, Lorg/apache/http/message/BasicNameValuePair;

    sget-object v8, Lcom/google/android/gsf/loginservice/GLSUser$RequestKey;->EMAIL:Lcom/google/android/gsf/loginservice/GLSUser$RequestKey;

    invoke-virtual {v8}, Lcom/google/android/gsf/loginservice/GLSUser$RequestKey;->getWire()Ljava/lang/String;

    move-result-object v8

    iget-object v9, p0, Lcom/google/android/gsf/loginservice/GLSUser;->mEmail:Ljava/lang/String;

    invoke-direct {v7, v8, v9}, Lorg/apache/http/message/BasicNameValuePair;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    move-object/from16 v0, p6

    invoke-virtual {v0, v7}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    :cond_0
    if-eqz p5, :cond_1

    new-instance v7, Lorg/apache/http/message/BasicNameValuePair;

    sget-object v8, Lcom/google/android/gsf/loginservice/GLSUser$RequestKey;->STORED_PERMISSION:Lcom/google/android/gsf/loginservice/GLSUser$RequestKey;

    invoke-virtual {v8}, Lcom/google/android/gsf/loginservice/GLSUser$RequestKey;->getWire()Ljava/lang/String;

    move-result-object v8

    const-string v9, "1"

    invoke-direct {v7, v8, v9}, Lorg/apache/http/message/BasicNameValuePair;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    move-object/from16 v0, p6

    invoke-virtual {v0, v7}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    :cond_1
    if-eqz p8, :cond_2

    new-instance v7, Lorg/apache/http/message/BasicNameValuePair;

    sget-object v8, Lcom/google/android/gsf/loginservice/GLSUser$RequestKey;->ADDED_ACCOUNT:Lcom/google/android/gsf/loginservice/GLSUser$RequestKey;

    invoke-virtual {v8}, Lcom/google/android/gsf/loginservice/GLSUser$RequestKey;->getWire()Ljava/lang/String;

    move-result-object v8

    const-string v9, "1"

    invoke-direct {v7, v8, v9}, Lorg/apache/http/message/BasicNameValuePair;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    move-object/from16 v0, p6

    invoke-virtual {v0, v7}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    :cond_2
    iget-boolean v7, p3, Lcom/google/android/gsf/loginservice/GLSUser$GLSSession;->mCreatingAccount:Z

    if-eqz v7, :cond_3

    new-instance v7, Lorg/apache/http/message/BasicNameValuePair;

    sget-object v8, Lcom/google/android/gsf/loginservice/GLSUser$RequestKey;->CREATED:Lcom/google/android/gsf/loginservice/GLSUser$RequestKey;

    invoke-virtual {v8}, Lcom/google/android/gsf/loginservice/GLSUser$RequestKey;->getWire()Ljava/lang/String;

    move-result-object v8

    const-string v9, "1"

    invoke-direct {v7, v8, v9}, Lorg/apache/http/message/BasicNameValuePair;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    move-object/from16 v0, p6

    invoke-virtual {v0, v7}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    :cond_3
    iget-object v7, p0, Lcom/google/android/gsf/loginservice/GLSUser;->mEncryptedPassword:Ljava/lang/String;

    if-eqz v7, :cond_9

    new-instance v7, Lorg/apache/http/message/BasicNameValuePair;

    sget-object v8, Lcom/google/android/gsf/loginservice/GLSUser$RequestKey;->ENCRYPTED_PASSWORD:Lcom/google/android/gsf/loginservice/GLSUser$RequestKey;

    invoke-virtual {v8}, Lcom/google/android/gsf/loginservice/GLSUser$RequestKey;->getWire()Ljava/lang/String;

    move-result-object v8

    iget-object v9, p0, Lcom/google/android/gsf/loginservice/GLSUser;->mEncryptedPassword:Ljava/lang/String;

    invoke-direct {v7, v8, v9}, Lorg/apache/http/message/BasicNameValuePair;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    move-object/from16 v0, p6

    invoke-virtual {v0, v7}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    :goto_0
    new-instance v7, Lorg/apache/http/message/BasicNameValuePair;

    sget-object v8, Lcom/google/android/gsf/loginservice/GLSUser$RequestKey;->SERVICE:Lcom/google/android/gsf/loginservice/GLSUser$RequestKey;

    invoke-virtual {v8}, Lcom/google/android/gsf/loginservice/GLSUser$RequestKey;->getWire()Ljava/lang/String;

    move-result-object v8

    invoke-direct {v7, v8, p1}, Lorg/apache/http/message/BasicNameValuePair;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    move-object/from16 v0, p6

    invoke-virtual {v0, v7}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    new-instance v7, Lorg/apache/http/message/BasicNameValuePair;

    sget-object v8, Lcom/google/android/gsf/loginservice/GLSUser$RequestKey;->ACCOUNT_SOURCE:Lcom/google/android/gsf/loginservice/GLSUser$RequestKey;

    invoke-virtual {v8}, Lcom/google/android/gsf/loginservice/GLSUser$RequestKey;->getWire()Ljava/lang/String;

    move-result-object v8

    const-string v9, "android"

    invoke-direct {v7, v8, v9}, Lorg/apache/http/message/BasicNameValuePair;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    move-object/from16 v0, p6

    invoke-virtual {v0, v7}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    sget-object v7, Lcom/google/android/gsf/loginservice/GLSUser;->sGLSContext:Lcom/google/android/gsf/loginservice/GLSUser$GLSContext;

    invoke-virtual {v7}, Lcom/google/android/gsf/loginservice/GLSUser$GLSContext;->getAndroidIdHex()Ljava/lang/String;

    move-result-object v1

    if-eqz v1, :cond_4

    new-instance v7, Lorg/apache/http/message/BasicNameValuePair;

    sget-object v8, Lcom/google/android/gsf/loginservice/GLSUser$RequestKey;->ANDROID_ID:Lcom/google/android/gsf/loginservice/GLSUser$RequestKey;

    invoke-virtual {v8}, Lcom/google/android/gsf/loginservice/GLSUser$RequestKey;->getWire()Ljava/lang/String;

    move-result-object v8

    invoke-direct {v7, v8, v1}, Lorg/apache/http/message/BasicNameValuePair;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    move-object/from16 v0, p6

    invoke-virtual {v0, v7}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    :cond_4
    iget-object v7, p0, Lcom/google/android/gsf/loginservice/GLSUser;->captchaToken:Ljava/lang/String;

    invoke-static {v7}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v7

    if-nez v7, :cond_5

    iget-object v7, p0, Lcom/google/android/gsf/loginservice/GLSUser;->captchaAnswer:Ljava/lang/String;

    invoke-static {v7}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v7

    if-nez v7, :cond_5

    new-instance v7, Lorg/apache/http/message/BasicNameValuePair;

    sget-object v8, Lcom/google/android/gsf/loginservice/GLSUser$RequestKey;->CAPTCHA_TOKEN:Lcom/google/android/gsf/loginservice/GLSUser$RequestKey;

    invoke-virtual {v8}, Lcom/google/android/gsf/loginservice/GLSUser$RequestKey;->getWire()Ljava/lang/String;

    move-result-object v8

    iget-object v9, p0, Lcom/google/android/gsf/loginservice/GLSUser;->captchaToken:Ljava/lang/String;

    invoke-direct {v7, v8, v9}, Lorg/apache/http/message/BasicNameValuePair;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    move-object/from16 v0, p6

    invoke-virtual {v0, v7}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    new-instance v7, Lorg/apache/http/message/BasicNameValuePair;

    sget-object v8, Lcom/google/android/gsf/loginservice/GLSUser$RequestKey;->CAPTCHA_ANSWER:Lcom/google/android/gsf/loginservice/GLSUser$RequestKey;

    invoke-virtual {v8}, Lcom/google/android/gsf/loginservice/GLSUser$RequestKey;->getWire()Ljava/lang/String;

    move-result-object v8

    iget-object v9, p0, Lcom/google/android/gsf/loginservice/GLSUser;->captchaAnswer:Ljava/lang/String;

    invoke-direct {v7, v8, v9}, Lorg/apache/http/message/BasicNameValuePair;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    move-object/from16 v0, p6

    invoke-virtual {v0, v7}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    const/4 v7, 0x0

    iput-object v7, p0, Lcom/google/android/gsf/loginservice/GLSUser;->captchaToken:Ljava/lang/String;

    const/4 v7, 0x0

    iput-object v7, p0, Lcom/google/android/gsf/loginservice/GLSUser;->captchaAnswer:Ljava/lang/String;

    :cond_5
    iget-boolean v7, p0, Lcom/google/android/gsf/loginservice/GLSUser;->mUseBrowserFlow:Z

    if-eqz v7, :cond_6

    new-instance v7, Lorg/apache/http/message/BasicNameValuePair;

    sget-object v8, Lcom/google/android/gsf/loginservice/GLSUser$RequestKey;->BROWSER_FLOW:Lcom/google/android/gsf/loginservice/GLSUser$RequestKey;

    invoke-virtual {v8}, Lcom/google/android/gsf/loginservice/GLSUser$RequestKey;->getWire()Ljava/lang/String;

    move-result-object v8

    const-string v9, "1"

    invoke-direct {v7, v8, v9}, Lorg/apache/http/message/BasicNameValuePair;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    move-object/from16 v0, p6

    invoke-virtual {v0, v7}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    :cond_6
    if-eqz p2, :cond_b

    invoke-virtual {p0, p2}, Lcom/google/android/gsf/loginservice/GLSUser;->getAppPackage(I)Ljava/lang/String;

    move-result-object v6

    new-instance v7, Lorg/apache/http/message/BasicNameValuePair;

    sget-object v8, Lcom/google/android/gsf/loginservice/GLSUser$RequestKey;->PACKAGE:Lcom/google/android/gsf/loginservice/GLSUser$RequestKey;

    invoke-virtual {v8}, Lcom/google/android/gsf/loginservice/GLSUser$RequestKey;->getWire()Ljava/lang/String;

    move-result-object v8

    invoke-direct {v7, v8, v6}, Lorg/apache/http/message/BasicNameValuePair;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    move-object/from16 v0, p6

    invoke-virtual {v0, v7}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    new-instance v7, Lorg/apache/http/message/BasicNameValuePair;

    sget-object v8, Lcom/google/android/gsf/loginservice/GLSUser$RequestKey;->PACKAGE_SIG:Lcom/google/android/gsf/loginservice/GLSUser$RequestKey;

    invoke-virtual {v8}, Lcom/google/android/gsf/loginservice/GLSUser$RequestKey;->getWire()Ljava/lang/String;

    move-result-object v8

    invoke-virtual {p0, v6}, Lcom/google/android/gsf/loginservice/GLSUser;->getSignatureFingerprint(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v9

    invoke-direct {v7, v8, v9}, Lorg/apache/http/message/BasicNameValuePair;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    move-object/from16 v0, p6

    invoke-virtual {v0, v7}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    if-eqz p4, :cond_b

    sget-object v7, Lcom/google/android/gsf/loginservice/GLSUser$RequestKey;->CLIENT_ID:Lcom/google/android/gsf/loginservice/GLSUser$RequestKey;

    invoke-virtual {v7}, Lcom/google/android/gsf/loginservice/GLSUser$RequestKey;->getWire()Ljava/lang/String;

    move-result-object v7

    invoke-virtual {p4, v7}, Landroid/os/Bundle;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v2

    if-eqz v2, :cond_7

    new-instance v7, Lorg/apache/http/message/BasicNameValuePair;

    sget-object v8, Lcom/google/android/gsf/loginservice/GLSUser$RequestKey;->CLIENT_ID:Lcom/google/android/gsf/loginservice/GLSUser$RequestKey;

    invoke-virtual {v8}, Lcom/google/android/gsf/loginservice/GLSUser$RequestKey;->getWire()Ljava/lang/String;

    move-result-object v8

    invoke-direct {v7, v8, v2}, Lorg/apache/http/message/BasicNameValuePair;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    move-object/from16 v0, p6

    invoke-virtual {v0, v7}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    :cond_7
    invoke-virtual {p4}, Landroid/os/Bundle;->keySet()Ljava/util/Set;

    move-result-object v7

    invoke-interface {v7}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    move-result-object v5

    :cond_8
    :goto_1
    invoke-interface {v5}, Ljava/util/Iterator;->hasNext()Z

    move-result v7

    if-eqz v7, :cond_b

    invoke-interface {v5}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v4

    check-cast v4, Ljava/lang/String;

    sget-object v7, Lcom/google/android/gsf/loginservice/GLSUser$RequestKey;->OAUTH2_EXTRA_PREFIX:Lcom/google/android/gsf/loginservice/GLSUser$RequestKey;

    invoke-virtual {v7}, Lcom/google/android/gsf/loginservice/GLSUser$RequestKey;->getWire()Ljava/lang/String;

    move-result-object v7

    invoke-virtual {v4, v7}, Ljava/lang/String;->startsWith(Ljava/lang/String;)Z

    move-result v7

    if-eqz v7, :cond_8

    new-instance v7, Lorg/apache/http/message/BasicNameValuePair;

    invoke-virtual {p4, v4}, Landroid/os/Bundle;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v8

    invoke-direct {v7, v4, v8}, Lorg/apache/http/message/BasicNameValuePair;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    move-object/from16 v0, p6

    invoke-virtual {v0, v7}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    goto :goto_1

    :cond_9
    if-nez p7, :cond_a

    new-instance v7, Lorg/apache/http/message/BasicNameValuePair;

    sget-object v8, Lcom/google/android/gsf/loginservice/GLSUser$RequestKey;->TOKEN:Lcom/google/android/gsf/loginservice/GLSUser$RequestKey;

    invoke-virtual {v8}, Lcom/google/android/gsf/loginservice/GLSUser$RequestKey;->getWire()Ljava/lang/String;

    move-result-object v8

    iget-object v9, p0, Lcom/google/android/gsf/loginservice/GLSUser;->mMasterToken:Ljava/lang/String;

    invoke-direct {v7, v8, v9}, Lorg/apache/http/message/BasicNameValuePair;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    move-object/from16 v0, p6

    invoke-virtual {v0, v7}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    goto/16 :goto_0

    :cond_a
    new-instance v7, Lorg/apache/http/message/BasicNameValuePair;

    sget-object v8, Lcom/google/android/gsf/loginservice/GLSUser$RequestKey;->TOKEN:Lcom/google/android/gsf/loginservice/GLSUser$RequestKey;

    invoke-virtual {v8}, Lcom/google/android/gsf/loginservice/GLSUser$RequestKey;->getWire()Ljava/lang/String;

    move-result-object v8

    move-object/from16 v0, p7

    invoke-direct {v7, v8, v0}, Lorg/apache/http/message/BasicNameValuePair;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    move-object/from16 v0, p6

    invoke-virtual {v0, v7}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    new-instance v7, Lorg/apache/http/message/BasicNameValuePair;

    sget-object v8, Lcom/google/android/gsf/loginservice/GLSUser$RequestKey;->ACCESS_TOKEN:Lcom/google/android/gsf/loginservice/GLSUser$RequestKey;

    invoke-virtual {v8}, Lcom/google/android/gsf/loginservice/GLSUser$RequestKey;->getWire()Ljava/lang/String;

    move-result-object v8

    const-string v9, "1"

    invoke-direct {v7, v8, v9}, Lorg/apache/http/message/BasicNameValuePair;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    move-object/from16 v0, p6

    invoke-virtual {v0, v7}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    goto/16 :goto_0

    :cond_b
    sget-object v7, Lcom/google/android/gsf/loginservice/GLSUser;->sGLSContext:Lcom/google/android/gsf/loginservice/GLSUser$GLSContext;

    iget-object v7, v7, Lcom/google/android/gsf/loginservice/GLSUser$GLSContext;->mContext:Landroid/content/Context;

    invoke-virtual {v7}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v7

    const-string v8, "device_country"

    const/4 v9, 0x0

    invoke-static {v7, v8, v9}, Lcom/google/android/gsf/Gservices;->getString(Landroid/content/ContentResolver;Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v3

    sget-object v7, Lcom/google/android/gsf/loginservice/GLSUser$RequestKey;->DEVICE_COUNTRY:Lcom/google/android/gsf/loginservice/GLSUser$RequestKey;

    invoke-virtual {v7}, Lcom/google/android/gsf/loginservice/GLSUser$RequestKey;->getWire()Ljava/lang/String;

    move-result-object v7

    move-object/from16 v0, p6

    invoke-direct {p0, v0, v7, v3}, Lcom/google/android/gsf/loginservice/GLSUser;->add(Ljava/util/ArrayList;Ljava/lang/String;Ljava/lang/String;)V

    sget-object v7, Lcom/google/android/gsf/loginservice/GLSUser$RequestKey;->OPERATOR_COUNTRY:Lcom/google/android/gsf/loginservice/GLSUser$RequestKey;

    invoke-virtual {v7}, Lcom/google/android/gsf/loginservice/GLSUser$RequestKey;->getWire()Ljava/lang/String;

    move-result-object v7

    move-object/from16 v0, p6

    invoke-direct {p0, v0, v7, v3}, Lcom/google/android/gsf/loginservice/GLSUser;->add(Ljava/util/ArrayList;Ljava/lang/String;Ljava/lang/String;)V

    sget-object v7, Lcom/google/android/gsf/loginservice/GLSUser$RequestKey;->LANGUAGE:Lcom/google/android/gsf/loginservice/GLSUser$RequestKey;

    invoke-virtual {v7}, Lcom/google/android/gsf/loginservice/GLSUser$RequestKey;->getWire()Ljava/lang/String;

    move-result-object v7

    invoke-static {}, Ljava/util/Locale;->getDefault()Ljava/util/Locale;

    move-result-object v8

    invoke-virtual {v8}, Ljava/util/Locale;->getLanguage()Ljava/lang/String;

    move-result-object v8

    move-object/from16 v0, p6

    invoke-direct {p0, v0, v7, v8}, Lcom/google/android/gsf/loginservice/GLSUser;->add(Ljava/util/ArrayList;Ljava/lang/String;Ljava/lang/String;)V

    sget-object v7, Lcom/google/android/gsf/loginservice/GLSUser$RequestKey;->SDK_VERSION:Lcom/google/android/gsf/loginservice/GLSUser$RequestKey;

    invoke-virtual {v7}, Lcom/google/android/gsf/loginservice/GLSUser$RequestKey;->getWire()Ljava/lang/String;

    move-result-object v7

    sget v8, Landroid/os/Build$VERSION;->SDK_INT:I

    invoke-static {v8}, Ljava/lang/Integer;->toString(I)Ljava/lang/String;

    move-result-object v8

    move-object/from16 v0, p6

    invoke-direct {p0, v0, v7, v8}, Lcom/google/android/gsf/loginservice/GLSUser;->add(Ljava/util/ArrayList;Ljava/lang/String;Ljava/lang/String;)V

    return-void
.end method

.method private processResponse(Ljava/util/Map;ILjava/lang/String;Landroid/os/Bundle;ZLcom/google/android/gsf/loginservice/GLSUser$GLSSession;)Landroid/content/Intent;
    .locals 12
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/Map",
            "<",
            "Ljava/lang/String;",
            "Ljava/lang/String;",
            ">;I",
            "Ljava/lang/String;",
            "Landroid/os/Bundle;",
            "Z",
            "Lcom/google/android/gsf/loginservice/GLSUser$GLSSession;",
            ")",
            "Landroid/content/Intent;"
        }
    .end annotation

    invoke-direct {p0, p1, p3}, Lcom/google/android/gsf/loginservice/GLSUser;->getAuth(Ljava/util/Map;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v9

    sget-object v1, Lcom/google/android/gsf/loginservice/GLSUser$ResponseKey;->STATUS:Lcom/google/android/gsf/loginservice/GLSUser$ResponseKey;

    invoke-virtual {v1}, Lcom/google/android/gsf/loginservice/GLSUser$ResponseKey;->getWire()Ljava/lang/String;

    move-result-object v1

    invoke-interface {p1, v1}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v8

    check-cast v8, Ljava/lang/String;

    move-object v1, p0

    move-object v2, p1

    move v3, p2

    move-object v4, p3

    move-object/from16 v5, p4

    move/from16 v6, p5

    move-object/from16 v7, p6

    invoke-direct/range {v1 .. v9}, Lcom/google/android/gsf/loginservice/GLSUser;->handlePermission(Ljava/util/Map;ILjava/lang/String;Landroid/os/Bundle;ZLcom/google/android/gsf/loginservice/GLSUser$GLSSession;Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    move-result-object v1

    if-eqz v1, :cond_0

    :goto_0
    return-object v1

    :cond_0
    const/4 v1, 0x1

    invoke-direct {p0, p1, v1}, Lcom/google/android/gsf/loginservice/GLSUser;->processTokenResponse(Ljava/util/Map;Z)V

    if-eqz v9, :cond_7

    const-string v1, "0"

    sget-object v2, Lcom/google/android/gsf/loginservice/GLSUser$ResponseKey;->CAN_UPGRADE_PLUS:Lcom/google/android/gsf/loginservice/GLSUser$ResponseKey;

    invoke-virtual {v2}, Lcom/google/android/gsf/loginservice/GLSUser$ResponseKey;->getWire()Ljava/lang/String;

    move-result-object v2

    invoke-interface {p1, v2}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-nez v1, :cond_5

    const/4 v1, 0x1

    :goto_1
    move-object/from16 v0, p6

    iput-boolean v1, v0, Lcom/google/android/gsf/loginservice/GLSUser$GLSSession;->mAllowGooglePlus:Z

    iget-boolean v1, p0, Lcom/google/android/gsf/loginservice/GLSUser;->existing:Z

    if-eqz v1, :cond_4

    const-string v1, "SID"

    invoke-virtual {v1, p3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-nez v1, :cond_1

    const-string v1, "LSID"

    invoke-virtual {v1, p3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-nez v1, :cond_1

    sget-object v1, Lcom/google/android/gsf/loginservice/GLSUser$ResponseKey;->EXPIRY_IN_S:Lcom/google/android/gsf/loginservice/GLSUser$ResponseKey;

    invoke-virtual {v1}, Lcom/google/android/gsf/loginservice/GLSUser$ResponseKey;->getWire()Ljava/lang/String;

    move-result-object v1

    invoke-interface {p1, v1}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Ljava/lang/String;

    invoke-virtual {p0, p3, p2, v9, v1}, Lcom/google/android/gsf/loginservice/GLSUser;->cacheToken(Ljava/lang/String;ILjava/lang/String;Ljava/lang/String;)V

    :cond_1
    const-string v1, "SID"

    invoke-interface {p1, v1}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Ljava/lang/String;

    const-string v2, "LSID"

    invoke-interface {p1, v2}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Ljava/lang/String;

    if-eqz v1, :cond_2

    const-string v3, "SID"

    const/4 v4, 0x0

    invoke-virtual {p0, v3, p2, v1, v4}, Lcom/google/android/gsf/loginservice/GLSUser;->cacheToken(Ljava/lang/String;ILjava/lang/String;Ljava/lang/String;)V

    :cond_2
    if-eqz v2, :cond_3

    const-string v1, "LSID"

    const/4 v3, 0x0

    invoke-virtual {p0, v1, p2, v2, v3}, Lcom/google/android/gsf/loginservice/GLSUser;->cacheToken(Ljava/lang/String;ILjava/lang/String;Ljava/lang/String;)V

    :cond_3
    iget-object v1, p0, Lcom/google/android/gsf/loginservice/GLSUser;->mUpdatedPassword:Ljava/lang/String;

    if-eqz v1, :cond_4

    invoke-virtual {p0}, Lcom/google/android/gsf/loginservice/GLSUser;->updateSecret()V

    const/4 v1, 0x0

    iput-object v1, p0, Lcom/google/android/gsf/loginservice/GLSUser;->mUpdatedPassword:Ljava/lang/String;

    :cond_4
    iget-boolean v1, p0, Lcom/google/android/gsf/loginservice/GLSUser;->existing:Z

    if-nez v1, :cond_6

    const/4 v11, 0x1

    :goto_2
    move-object v5, p0

    move-object v6, p1

    move-object v7, p3

    move v8, p2

    move-object/from16 v10, p6

    invoke-direct/range {v5 .. v11}, Lcom/google/android/gsf/loginservice/GLSUser;->successIntent(Ljava/util/Map;Ljava/lang/String;ILjava/lang/String;Lcom/google/android/gsf/loginservice/GLSUser$GLSSession;Z)Landroid/content/Intent;

    move-result-object v1

    goto :goto_0

    :cond_5
    const/4 v1, 0x0

    goto :goto_1

    :cond_6
    const/4 v11, 0x0

    goto :goto_2

    :cond_7
    if-nez v8, :cond_8

    const-string v1, "GLSUser"

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "Couldn\'t get error message from reply:"

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;)I

    sget-object v1, Lcom/google/android/gsf/loginservice/GLSUser$Status;->SERVICE_UNAVAILABLE:Lcom/google/android/gsf/loginservice/GLSUser$Status;

    invoke-virtual {v1}, Lcom/google/android/gsf/loginservice/GLSUser$Status;->getWire()Ljava/lang/String;

    move-result-object v8

    :cond_8
    const-string v1, "badauth"

    invoke-virtual {v1, v8}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_9

    sget-object v1, Lcom/google/android/gsf/loginservice/GLSUser$Status;->BAD_AUTHENTICATION:Lcom/google/android/gsf/loginservice/GLSUser$Status;

    invoke-virtual {v1}, Lcom/google/android/gsf/loginservice/GLSUser$Status;->getWire()Ljava/lang/String;

    move-result-object v8

    :cond_9
    invoke-static {v8}, Lcom/google/android/gsf/loginservice/GLSUser$Status;->fromWire(Ljava/lang/String;)Lcom/google/android/gsf/loginservice/GLSUser$Status;

    move-result-object v3

    sget-object v1, Lcom/google/android/gsf/loginservice/GLSUser$Status;->BAD_AUTHENTICATION:Lcom/google/android/gsf/loginservice/GLSUser$Status;

    if-ne v3, v1, :cond_a

    sget-object v1, Lcom/google/android/gsf/loginservice/GLSUser$ResponseKey;->INFO:Lcom/google/android/gsf/loginservice/GLSUser$ResponseKey;

    invoke-virtual {v1}, Lcom/google/android/gsf/loginservice/GLSUser$ResponseKey;->getWire()Ljava/lang/String;

    move-result-object v1

    invoke-interface {p1, v1}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Ljava/lang/String;

    sget-object v2, Lcom/google/android/gsf/loginservice/GLSUser$Status;->NEEDS_2F:Lcom/google/android/gsf/loginservice/GLSUser$Status;

    invoke-virtual {v2}, Lcom/google/android/gsf/loginservice/GLSUser$Status;->getWire()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v2, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_a

    sget-object v3, Lcom/google/android/gsf/loginservice/GLSUser$Status;->NEEDS_2F:Lcom/google/android/gsf/loginservice/GLSUser$Status;

    :cond_a
    move-object v1, p0

    move-object v2, p1

    move v4, p2

    move-object v5, p3

    move-object/from16 v6, p6

    move-object/from16 v7, p4

    invoke-virtual/range {v1 .. v7}, Lcom/google/android/gsf/loginservice/GLSUser;->errorIntent(Ljava/util/Map;Lcom/google/android/gsf/loginservice/GLSUser$Status;ILjava/lang/String;Lcom/google/android/gsf/loginservice/GLSUser$GLSSession;Landroid/os/Bundle;)Landroid/content/Intent;

    move-result-object v4

    sget-object v1, Lcom/google/android/gsf/loginservice/GLSUser$Status;->CAPTCHA:Lcom/google/android/gsf/loginservice/GLSUser$Status;

    if-ne v3, v1, :cond_c

    sget-object v1, Lcom/google/android/gsf/loginservice/GLSUser$ResponseKey;->CAPTCHA_TOKEN_RES:Lcom/google/android/gsf/loginservice/GLSUser$ResponseKey;

    invoke-virtual {v1}, Lcom/google/android/gsf/loginservice/GLSUser$ResponseKey;->getWire()Ljava/lang/String;

    move-result-object v1

    invoke-interface {p1, v1}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Ljava/lang/String;

    sget-object v2, Lcom/google/android/gsf/loginservice/GLSUser$ResponseKey;->CAPTCHA_TOKEN_RES:Lcom/google/android/gsf/loginservice/GLSUser$ResponseKey;

    invoke-virtual {v2}, Lcom/google/android/gsf/loginservice/GLSUser$ResponseKey;->getWire()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v4, v2, v1}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    sget-object v2, Lcom/google/android/gsf/loginservice/GLSUser$ResponseKey;->CAPTCHA_URL:Lcom/google/android/gsf/loginservice/GLSUser$ResponseKey;

    invoke-virtual {v2}, Lcom/google/android/gsf/loginservice/GLSUser$ResponseKey;->getWire()Ljava/lang/String;

    move-result-object v2

    invoke-interface {p1, v2}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Ljava/lang/String;

    move-object/from16 v0, p6

    invoke-virtual {p0, v1, v2, v4, v0}, Lcom/google/android/gsf/loginservice/GLSUser;->getCaptchaData(Ljava/lang/String;Ljava/lang/String;Landroid/content/Intent;Lcom/google/android/gsf/loginservice/GLSUser$GLSSession;)V

    :cond_b
    :goto_3
    const-string v1, "GLSUser"

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "GLS error: "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, " "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    iget-object v3, p0, Lcom/google/android/gsf/loginservice/GLSUser;->mEmail:Ljava/lang/String;

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, " "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, p3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    move-object v1, v4

    goto/16 :goto_0

    :cond_c
    sget-object v1, Lcom/google/android/gsf/loginservice/GLSUser$Status;->BAD_AUTHENTICATION:Lcom/google/android/gsf/loginservice/GLSUser$Status;

    if-ne v3, v1, :cond_d

    iget-object v1, p0, Lcom/google/android/gsf/loginservice/GLSUser;->mEncryptedPassword:Ljava/lang/String;

    if-eqz v1, :cond_b

    invoke-virtual {p0}, Lcom/google/android/gsf/loginservice/GLSUser;->resetPassword()V

    goto :goto_3

    :cond_d
    sget-object v1, Lcom/google/android/gsf/loginservice/GLSUser$Status;->CLIENT_LOGIN_DISABLED:Lcom/google/android/gsf/loginservice/GLSUser$Status;

    if-ne v3, v1, :cond_b

    invoke-virtual {p0}, Lcom/google/android/gsf/loginservice/GLSUser;->resetPassword()V

    sget-object v1, Lcom/google/android/gsf/loginservice/GLSUser;->sGLSContext:Lcom/google/android/gsf/loginservice/GLSUser$GLSContext;

    iget-object v1, v1, Lcom/google/android/gsf/loginservice/GLSUser$GLSContext;->mAccountManager:Landroid/accounts/AccountManager;

    iget-object v2, p0, Lcom/google/android/gsf/loginservice/GLSUser;->mAccount:Landroid/accounts/Account;

    const-string v3, "oauthAccessToken"

    const-string v5, "1"

    invoke-virtual {v1, v2, v3, v5}, Landroid/accounts/AccountManager;->setUserData(Landroid/accounts/Account;Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_3
.end method

.method private processTokenResponse(Ljava/util/Map;Z)V
    .locals 5
    .param p2    # Z
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/Map",
            "<",
            "Ljava/lang/String;",
            "Ljava/lang/String;",
            ">;Z)V"
        }
    .end annotation

    const/4 v2, 0x0

    sget-object v1, Lcom/google/android/gsf/loginservice/GLSUser$ResponseKey;->TOKEN:Lcom/google/android/gsf/loginservice/GLSUser$ResponseKey;

    invoke-virtual {v1}, Lcom/google/android/gsf/loginservice/GLSUser$ResponseKey;->getWire()Ljava/lang/String;

    move-result-object v1

    invoke-interface {p1, v1}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/String;

    if-eqz v0, :cond_0

    iget-object v1, p0, Lcom/google/android/gsf/loginservice/GLSUser;->mMasterToken:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-nez v1, :cond_0

    iput-object v0, p0, Lcom/google/android/gsf/loginservice/GLSUser;->mMasterToken:Ljava/lang/String;

    iput-object v2, p0, Lcom/google/android/gsf/loginservice/GLSUser;->mEncryptedPassword:Ljava/lang/String;

    iput-object v2, p0, Lcom/google/android/gsf/loginservice/GLSUser;->mHash:Ljava/lang/String;

    iget-object v1, p0, Lcom/google/android/gsf/loginservice/GLSUser;->mAccount:Landroid/accounts/Account;

    if-eqz v1, :cond_0

    if-eqz p2, :cond_0

    sget-object v1, Lcom/google/android/gsf/loginservice/GLSUser;->sGLSContext:Lcom/google/android/gsf/loginservice/GLSUser$GLSContext;

    iget-object v1, v1, Lcom/google/android/gsf/loginservice/GLSUser$GLSContext;->mAccountManager:Landroid/accounts/AccountManager;

    iget-object v2, p0, Lcom/google/android/gsf/loginservice/GLSUser;->mAccount:Landroid/accounts/Account;

    invoke-virtual {v1, v2, v0}, Landroid/accounts/AccountManager;->setPassword(Landroid/accounts/Account;Ljava/lang/String;)V

    iget-boolean v1, p0, Lcom/google/android/gsf/loginservice/GLSUser;->mUseBrowserFlow:Z

    if-eqz v1, :cond_0

    sget v1, Landroid/os/Build$VERSION;->SDK_INT:I

    const/16 v2, 0x8

    if-gt v1, v2, :cond_0

    sget-object v1, Lcom/google/android/gsf/loginservice/GLSUser;->sGLSContext:Lcom/google/android/gsf/loginservice/GLSUser$GLSContext;

    iget-object v1, v1, Lcom/google/android/gsf/loginservice/GLSUser$GLSContext;->mAccountManager:Landroid/accounts/AccountManager;

    iget-object v2, p0, Lcom/google/android/gsf/loginservice/GLSUser;->mAccount:Landroid/accounts/Account;

    const-string v3, "oauthAccessToken"

    iget-object v4, p0, Lcom/google/android/gsf/loginservice/GLSUser;->mMasterToken:Ljava/lang/String;

    invoke-virtual {v1, v2, v3, v4}, Landroid/accounts/AccountManager;->setUserData(Landroid/accounts/Account;Ljava/lang/String;Ljava/lang/String;)V

    :cond_0
    return-void
.end method

.method private refreshServicesAndSyncAdapters(Ljava/lang/String;)V
    .locals 9
    .param p1    # Ljava/lang/String;

    sget-object v6, Lcom/google/android/gsf/loginservice/GLSUser;->sGLSContext:Lcom/google/android/gsf/loginservice/GLSUser$GLSContext;

    iget-object v6, v6, Lcom/google/android/gsf/loginservice/GLSUser$GLSContext;->mAccountManager:Landroid/accounts/AccountManager;

    iget-object v7, p0, Lcom/google/android/gsf/loginservice/GLSUser;->mAccount:Landroid/accounts/Account;

    sget-object v8, Lcom/google/android/gsf/loginservice/GLSUser$ResponseKey;->SERVICES:Lcom/google/android/gsf/loginservice/GLSUser$ResponseKey;

    invoke-virtual {v8}, Lcom/google/android/gsf/loginservice/GLSUser$ResponseKey;->getWire()Ljava/lang/String;

    move-result-object v8

    invoke-virtual {v6, v7, v8}, Landroid/accounts/AccountManager;->getUserData(Landroid/accounts/Account;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v3

    if-eqz v3, :cond_1

    invoke-virtual {v3, p1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v6

    if-eqz v6, :cond_1

    :cond_0
    return-void

    :cond_1
    sget-object v6, Lcom/google/android/gsf/loginservice/GLSUser;->sGLSContext:Lcom/google/android/gsf/loginservice/GLSUser$GLSContext;

    iget-object v6, v6, Lcom/google/android/gsf/loginservice/GLSUser$GLSContext;->mAccountManager:Landroid/accounts/AccountManager;

    iget-object v7, p0, Lcom/google/android/gsf/loginservice/GLSUser;->mAccount:Landroid/accounts/Account;

    sget-object v8, Lcom/google/android/gsf/loginservice/GLSUser$ResponseKey;->SERVICES:Lcom/google/android/gsf/loginservice/GLSUser$ResponseKey;

    invoke-virtual {v8}, Lcom/google/android/gsf/loginservice/GLSUser$ResponseKey;->getWire()Ljava/lang/String;

    move-result-object v8

    invoke-virtual {v6, v7, v8, p1}, Landroid/accounts/AccountManager;->setUserData(Landroid/accounts/Account;Ljava/lang/String;Ljava/lang/String;)V

    invoke-static {}, Landroid/content/ContentResolver;->getSyncAdapterTypes()[Landroid/content/SyncAdapterType;

    move-result-object v5

    move-object v0, v5

    array-length v2, v0

    const/4 v1, 0x0

    :goto_0
    if-ge v1, v2, :cond_0

    aget-object v4, v0, v1

    iget-object v6, p0, Lcom/google/android/gsf/loginservice/GLSUser;->mAccount:Landroid/accounts/Account;

    iget-object v6, v6, Landroid/accounts/Account;->type:Ljava/lang/String;

    iget-object v7, v4, Landroid/content/SyncAdapterType;->accountType:Ljava/lang/String;

    invoke-virtual {v6, v7}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v6

    if-eqz v6, :cond_2

    iget-object v6, p0, Lcom/google/android/gsf/loginservice/GLSUser;->mAccount:Landroid/accounts/Account;

    iget-object v7, v4, Landroid/content/SyncAdapterType;->authority:Ljava/lang/String;

    invoke-static {v6, v7}, Landroid/content/ContentResolver;->getIsSyncable(Landroid/accounts/Account;Ljava/lang/String;)I

    move-result v6

    if-nez v6, :cond_2

    iget-object v6, p0, Lcom/google/android/gsf/loginservice/GLSUser;->mAccount:Landroid/accounts/Account;

    iget-object v7, v4, Landroid/content/SyncAdapterType;->authority:Ljava/lang/String;

    const/4 v8, -0x1

    invoke-static {v6, v7, v8}, Landroid/content/ContentResolver;->setIsSyncable(Landroid/accounts/Account;Ljava/lang/String;I)V

    iget-object v6, p0, Lcom/google/android/gsf/loginservice/GLSUser;->mAccount:Landroid/accounts/Account;

    iget-object v7, v4, Landroid/content/SyncAdapterType;->authority:Ljava/lang/String;

    new-instance v8, Landroid/os/Bundle;

    invoke-direct {v8}, Landroid/os/Bundle;-><init>()V

    invoke-static {v6, v7, v8}, Landroid/content/ContentResolver;->requestSync(Landroid/accounts/Account;Ljava/lang/String;Landroid/os/Bundle;)V

    :cond_2
    add-int/lit8 v1, v1, 0x1

    goto :goto_0
.end method

.method private sameSignature(II)Z
    .locals 1
    .param p1    # I
    .param p2    # I

    sget-object v0, Lcom/google/android/gsf/loginservice/GLSUser;->sGLSContext:Lcom/google/android/gsf/loginservice/GLSUser$GLSContext;

    iget-object v0, v0, Lcom/google/android/gsf/loginservice/GLSUser$GLSContext;->mPM:Landroid/content/pm/PackageManager;

    invoke-virtual {v0, p1, p2}, Landroid/content/pm/PackageManager;->checkSignatures(II)I

    move-result v0

    if-nez v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method static signatureDigest(Landroid/content/pm/Signature;)Ljava/lang/String;
    .locals 3
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/security/NoSuchAlgorithmException;
        }
    .end annotation

    const/4 v0, 0x0

    invoke-virtual {p0}, Landroid/content/pm/Signature;->toByteArray()[B

    move-result-object v1

    const-string v2, "SHA1"

    invoke-static {v2}, Ljava/security/MessageDigest;->getInstance(Ljava/lang/String;)Ljava/security/MessageDigest;

    move-result-object v2

    if-nez v2, :cond_1

    :cond_0
    :goto_0
    return-object v0

    :cond_1
    invoke-virtual {v2, v1}, Ljava/security/MessageDigest;->digest([B)[B

    move-result-object v1

    if-eqz v1, :cond_0

    invoke-static {v1}, Lcom/google/android/gsf/loginservice/GLSUser;->toHex([B)Ljava/lang/String;

    move-result-object v0

    goto :goto_0
.end method

.method private successIntent(Ljava/util/Map;Ljava/lang/String;ILjava/lang/String;Lcom/google/android/gsf/loginservice/GLSUser$GLSSession;Z)Landroid/content/Intent;
    .locals 7
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/Map",
            "<",
            "Ljava/lang/String;",
            "Ljava/lang/String;",
            ">;",
            "Ljava/lang/String;",
            "I",
            "Ljava/lang/String;",
            "Lcom/google/android/gsf/loginservice/GLSUser$GLSSession;",
            "Z)",
            "Landroid/content/Intent;"
        }
    .end annotation

    new-instance v2, Landroid/content/Intent;

    invoke-direct {v2}, Landroid/content/Intent;-><init>()V

    const-string v0, "authAccount"

    iget-object v1, p0, Lcom/google/android/gsf/loginservice/GLSUser;->mEmail:Ljava/lang/String;

    invoke-virtual {v2, v0, v1}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    const-string v0, "accountType"

    iget-object v1, p0, Lcom/google/android/gsf/loginservice/GLSUser;->mAccount:Landroid/accounts/Account;

    iget-object v1, v1, Landroid/accounts/Account;->type:Ljava/lang/String;

    invoke-virtual {v2, v0, v1}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    if-nez p4, :cond_0

    invoke-direct {p0, p1, p2}, Lcom/google/android/gsf/loginservice/GLSUser;->getAuth(Ljava/util/Map;Ljava/lang/String;)Ljava/lang/String;

    move-result-object p4

    :cond_0
    if-nez p6, :cond_1

    const-string v0, "authtoken"

    invoke-virtual {v2, v0, p4}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    :cond_1
    sget-object v0, Lcom/google/android/gsf/loginservice/GLSUser$ResponseKey;->SERVICES:Lcom/google/android/gsf/loginservice/GLSUser$ResponseKey;

    invoke-virtual {v0}, Lcom/google/android/gsf/loginservice/GLSUser$ResponseKey;->getWire()Ljava/lang/String;

    move-result-object v0

    invoke-interface {p1, v0}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/String;

    if-eqz v0, :cond_4

    const-string v1, ","

    invoke-virtual {v0, v1}, Ljava/lang/String;->split(Ljava/lang/String;)[Ljava/lang/String;

    move-result-object v3

    array-length v4, v3

    const/4 v1, 0x0

    :goto_0
    if-ge v1, v4, :cond_3

    aget-object v5, v3, v1

    sget-object v6, Lcom/google/android/gsf/loginservice/GLSUser$ResponseKey;->SERVICE_GPLUS:Lcom/google/android/gsf/loginservice/GLSUser$ResponseKey;

    invoke-virtual {v6}, Lcom/google/android/gsf/loginservice/GLSUser$ResponseKey;->getWire()Ljava/lang/String;

    move-result-object v6

    invoke-virtual {v5}, Ljava/lang/String;->trim()Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v6, v5}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v5

    if-eqz v5, :cond_2

    const/4 v5, 0x1

    iput-boolean v5, p5, Lcom/google/android/gsf/loginservice/GLSUser$GLSSession;->mHasGooglePlus:Z

    :cond_2
    add-int/lit8 v1, v1, 0x1

    goto :goto_0

    :cond_3
    invoke-direct {p0, v0}, Lcom/google/android/gsf/loginservice/GLSUser;->refreshServicesAndSyncAdapters(Ljava/lang/String;)V

    :cond_4
    sget-object v0, Lcom/google/android/gsf/loginservice/GLSUser$ResponseKey;->FIRST_NAME:Lcom/google/android/gsf/loginservice/GLSUser$ResponseKey;

    invoke-virtual {v0}, Lcom/google/android/gsf/loginservice/GLSUser$ResponseKey;->getWire()Ljava/lang/String;

    move-result-object v0

    invoke-interface {p1, v0}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/String;

    sget-object v1, Lcom/google/android/gsf/loginservice/GLSUser$ResponseKey;->LAST_NAME:Lcom/google/android/gsf/loginservice/GLSUser$ResponseKey;

    invoke-virtual {v1}, Lcom/google/android/gsf/loginservice/GLSUser$ResponseKey;->getWire()Ljava/lang/String;

    move-result-object v1

    invoke-interface {p1, v1}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Ljava/lang/String;

    if-eqz v0, :cond_5

    if-eqz v1, :cond_5

    iget-object v3, p5, Lcom/google/android/gsf/loginservice/GLSUser$GLSSession;->mUserData:Ljava/util/HashMap;

    sget-object v4, Lcom/google/android/gsf/loginservice/GLSUser$ResponseKey;->FIRST_NAME:Lcom/google/android/gsf/loginservice/GLSUser$ResponseKey;

    invoke-virtual {v4}, Lcom/google/android/gsf/loginservice/GLSUser$ResponseKey;->getWire()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v3, v4, v0}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    iget-object v0, p5, Lcom/google/android/gsf/loginservice/GLSUser$GLSSession;->mUserData:Ljava/util/HashMap;

    sget-object v3, Lcom/google/android/gsf/loginservice/GLSUser$ResponseKey;->LAST_NAME:Lcom/google/android/gsf/loginservice/GLSUser$ResponseKey;

    invoke-virtual {v3}, Lcom/google/android/gsf/loginservice/GLSUser$ResponseKey;->getWire()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v0, v3, v1}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    :cond_5
    sget-object v0, Lcom/google/android/gsf/loginservice/GLSUser$ResponseKey;->PICASA_USER:Lcom/google/android/gsf/loginservice/GLSUser$ResponseKey;

    invoke-virtual {v0}, Lcom/google/android/gsf/loginservice/GLSUser$ResponseKey;->getWire()Ljava/lang/String;

    move-result-object v0

    invoke-interface {p1, v0}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/String;

    if-eqz v0, :cond_6

    iget-object v1, p5, Lcom/google/android/gsf/loginservice/GLSUser$GLSSession;->mUserData:Ljava/util/HashMap;

    sget-object v3, Lcom/google/android/gsf/loginservice/GLSUser$ResponseKey;->PICASA_USER:Lcom/google/android/gsf/loginservice/GLSUser$ResponseKey;

    invoke-virtual {v3}, Lcom/google/android/gsf/loginservice/GLSUser$ResponseKey;->getWire()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v1, v3, v0}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    :cond_6
    return-object v2
.end method

.method private static toHex([B)Ljava/lang/String;
    .locals 7

    const/4 v1, 0x0

    new-instance v2, Ljava/lang/StringBuffer;

    array-length v0, p0

    mul-int/lit8 v0, v0, 0x2

    invoke-direct {v2, v0}, Ljava/lang/StringBuffer;-><init>(I)V

    array-length v3, p0

    move v0, v1

    :goto_0
    if-ge v0, v3, :cond_0

    aget-byte v4, p0, v0

    const-string v5, "%02x"

    const/4 v6, 0x1

    new-array v6, v6, [Ljava/lang/Object;

    invoke-static {v4}, Ljava/lang/Byte;->valueOf(B)Ljava/lang/Byte;

    move-result-object v4

    aput-object v4, v6, v1

    invoke-static {v5, v6}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v2, v4}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    :cond_0
    invoke-virtual {v2}, Ljava/lang/StringBuffer;->toString()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method


# virtual methods
.method public accountExists(Ljava/lang/String;)Z
    .locals 6
    .param p1    # Ljava/lang/String;

    invoke-virtual {p0}, Lcom/google/android/gsf/loginservice/GLSUser;->getAccountManager()Landroid/accounts/AccountManager;

    move-result-object v4

    const-string v5, "com.google"

    invoke-virtual {v4, v5}, Landroid/accounts/AccountManager;->getAccountsByType(Ljava/lang/String;)[Landroid/accounts/Account;

    move-result-object v1

    array-length v3, v1

    const/4 v2, 0x0

    :goto_0
    if-ge v2, v3, :cond_1

    aget-object v0, v1, v2

    iget-object v4, v0, Landroid/accounts/Account;->name:Ljava/lang/String;

    invoke-virtual {v4, p1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v4

    if-eqz v4, :cond_0

    const/4 v4, 0x1

    :goto_1
    return v4

    :cond_0
    add-int/lit8 v2, v2, 0x1

    goto :goto_0

    :cond_1
    const/4 v4, 0x0

    goto :goto_1
.end method

.method public addAccount(Lcom/google/android/gsf/loginservice/GLSUser$GLSSession;ZZZLjava/lang/String;)Landroid/content/Intent;
    .locals 14
    .param p1    # Lcom/google/android/gsf/loginservice/GLSUser$GLSSession;
    .param p2    # Z
    .param p3    # Z
    .param p4    # Z
    .param p5    # Ljava/lang/String;

    const-string v1, "ac2dm"

    const/4 v2, 0x0

    new-instance v4, Landroid/os/Bundle;

    invoke-direct {v4}, Landroid/os/Bundle;-><init>()V

    const/4 v5, 0x1

    const/4 v7, 0x1

    move-object v0, p0

    move-object v3, p1

    move-object/from16 v6, p5

    invoke-direct/range {v0 .. v7}, Lcom/google/android/gsf/loginservice/GLSUser;->getAuthtokenRaw(Ljava/lang/String;ILcom/google/android/gsf/loginservice/GLSUser$GLSSession;Landroid/os/Bundle;ZLjava/lang/String;Z)Ljava/util/Map;

    move-result-object v1

    const/4 v0, 0x0

    iput-object v0, p1, Lcom/google/android/gsf/loginservice/GLSUser$GLSSession;->mAccessToken:Ljava/lang/String;

    const-string v0, "SID"

    invoke-interface {v1, v0}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v4

    check-cast v4, Ljava/lang/String;

    const-string v0, "LSID"

    invoke-interface {v1, v0}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v5

    check-cast v5, Ljava/lang/String;

    if-eqz v4, :cond_3

    const/4 v0, 0x0

    invoke-direct {p0, v1, v0}, Lcom/google/android/gsf/loginservice/GLSUser;->processTokenResponse(Ljava/util/Map;Z)V

    sget-object v0, Lcom/google/android/gsf/loginservice/GLSUser$RequestKey;->EMAIL:Lcom/google/android/gsf/loginservice/GLSUser$RequestKey;

    invoke-virtual {v0}, Lcom/google/android/gsf/loginservice/GLSUser$RequestKey;->getWire()Ljava/lang/String;

    move-result-object v0

    invoke-interface {v1, v0}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v13

    check-cast v13, Ljava/lang/String;

    if-eqz v13, :cond_0

    iget-object v0, p0, Lcom/google/android/gsf/loginservice/GLSUser;->mEmail:Ljava/lang/String;

    invoke-virtual {v13, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_0

    iput-object v13, p0, Lcom/google/android/gsf/loginservice/GLSUser;->mEmail:Ljava/lang/String;

    iget-object v0, p0, Lcom/google/android/gsf/loginservice/GLSUser;->mEmail:Ljava/lang/String;

    iput-object v0, p1, Lcom/google/android/gsf/loginservice/GLSUser$GLSSession;->mUsername:Ljava/lang/String;

    new-instance v0, Landroid/accounts/Account;

    iget-object v2, p0, Lcom/google/android/gsf/loginservice/GLSUser;->mEmail:Ljava/lang/String;

    const-string v3, "com.google"

    invoke-direct {v0, v2, v3}, Landroid/accounts/Account;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    iput-object v0, p0, Lcom/google/android/gsf/loginservice/GLSUser;->mAccount:Landroid/accounts/Account;

    iget-object v0, p0, Lcom/google/android/gsf/loginservice/GLSUser;->mEmail:Ljava/lang/String;

    invoke-virtual {p0, v0}, Lcom/google/android/gsf/loginservice/GLSUser;->accountExists(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_0

    const-string v2, "SID"

    const/4 v3, 0x0

    const/4 v4, 0x0

    const/4 v6, 0x1

    move-object v0, p0

    move-object v5, p1

    invoke-direct/range {v0 .. v6}, Lcom/google/android/gsf/loginservice/GLSUser;->successIntent(Ljava/util/Map;Ljava/lang/String;ILjava/lang/String;Lcom/google/android/gsf/loginservice/GLSUser$GLSSession;Z)Landroid/content/Intent;

    move-result-object v0

    :goto_0
    return-object v0

    :cond_0
    sget-object v0, Lcom/google/android/gsf/loginservice/GLSUser$ResponseKey;->SERVICES:Lcom/google/android/gsf/loginservice/GLSUser$ResponseKey;

    invoke-virtual {v0}, Lcom/google/android/gsf/loginservice/GLSUser$ResponseKey;->getWire()Ljava/lang/String;

    move-result-object v0

    invoke-interface {v1, v0}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v6

    check-cast v6, Ljava/lang/String;

    move-object v2, p0

    move/from16 v3, p2

    move/from16 v7, p3

    invoke-direct/range {v2 .. v7}, Lcom/google/android/gsf/loginservice/GLSUser;->firstSave(ZLjava/lang/String;Ljava/lang/String;Ljava/lang/String;Z)V

    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/google/android/gsf/loginservice/GLSUser;->existing:Z

    const-string v0, "useGoogleMail"

    invoke-interface {v1, v0}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    if-eqz v0, :cond_1

    sget-object v0, Lcom/google/android/gsf/loginservice/GLSUser;->sGLSContext:Lcom/google/android/gsf/loginservice/GLSUser$GLSContext;

    iget-object v0, v0, Lcom/google/android/gsf/loginservice/GLSUser$GLSContext;->mContext:Landroid/content/Context;

    invoke-static {v0}, Lcom/google/android/gsf/loginservice/GoogleMail;->switchToGoogleMail(Landroid/content/Context;)V

    :cond_1
    if-eqz p4, :cond_2

    invoke-virtual {p0}, Lcom/google/android/gsf/loginservice/GLSUser;->setBackupAccount()Z

    :cond_2
    :goto_1
    const/4 v8, 0x0

    const-string v9, "SID"

    const/4 v10, 0x0

    const/4 v11, 0x1

    move-object v6, p0

    move-object v7, v1

    move-object v12, p1

    invoke-direct/range {v6 .. v12}, Lcom/google/android/gsf/loginservice/GLSUser;->processResponse(Ljava/util/Map;ILjava/lang/String;Landroid/os/Bundle;ZLcom/google/android/gsf/loginservice/GLSUser$GLSSession;)Landroid/content/Intent;

    move-result-object v0

    goto :goto_0

    :cond_3
    if-eqz p3, :cond_2

    const/4 v7, 0x0

    const/4 v8, 0x0

    const/4 v9, 0x0

    const-string v10, ""

    const/4 v11, 0x1

    move-object v6, p0

    invoke-direct/range {v6 .. v11}, Lcom/google/android/gsf/loginservice/GLSUser;->firstSave(ZLjava/lang/String;Ljava/lang/String;Ljava/lang/String;Z)V

    goto :goto_1
.end method

.method public addAccount(ZLjava/lang/String;)V
    .locals 4
    .param p1    # Z
    .param p2    # Ljava/lang/String;

    new-instance v0, Landroid/os/Bundle;

    invoke-direct {v0}, Landroid/os/Bundle;-><init>()V

    const-string v2, "flags"

    const/4 v3, 0x1

    invoke-static {v3}, Ljava/lang/String;->valueOf(I)Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v0, v2, v3}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V

    iget-object v2, p0, Lcom/google/android/gsf/loginservice/GLSUser;->mMasterToken:Ljava/lang/String;

    if-eqz v2, :cond_3

    iget-object v1, p0, Lcom/google/android/gsf/loginservice/GLSUser;->mMasterToken:Ljava/lang/String;

    :goto_0
    iget-boolean v2, p0, Lcom/google/android/gsf/loginservice/GLSUser;->mUseBrowserFlow:Z

    if-eqz v2, :cond_0

    sget v2, Landroid/os/Build$VERSION;->SDK_INT:I

    const/16 v3, 0x8

    if-gt v2, v3, :cond_4

    const-string v2, "oauthAccessToken"

    invoke-virtual {v0, v2, v1}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V

    :cond_0
    :goto_1
    iget-object v2, p0, Lcom/google/android/gsf/loginservice/GLSUser;->mHash:Ljava/lang/String;

    if-eqz v2, :cond_1

    const-string v2, "sha1hash"

    iget-object v3, p0, Lcom/google/android/gsf/loginservice/GLSUser;->mHash:Ljava/lang/String;

    invoke-virtual {v0, v2, v3}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V

    :cond_1
    if-eqz p2, :cond_2

    sget-object v2, Lcom/google/android/gsf/loginservice/GLSUser$ResponseKey;->SERVICES:Lcom/google/android/gsf/loginservice/GLSUser$ResponseKey;

    invoke-virtual {v2}, Lcom/google/android/gsf/loginservice/GLSUser$ResponseKey;->getWire()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0, v2, p2}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V

    :cond_2
    sget-object v2, Lcom/google/android/gsf/loginservice/GLSUser;->sGLSContext:Lcom/google/android/gsf/loginservice/GLSUser$GLSContext;

    iget-object v2, v2, Lcom/google/android/gsf/loginservice/GLSUser$GLSContext;->mAccountManager:Landroid/accounts/AccountManager;

    iget-object v3, p0, Lcom/google/android/gsf/loginservice/GLSUser;->mAccount:Landroid/accounts/Account;

    invoke-virtual {v2, v3, v1, v0}, Landroid/accounts/AccountManager;->addAccountExplicitly(Landroid/accounts/Account;Ljava/lang/String;Landroid/os/Bundle;)Z

    const/4 v2, 0x0

    iput-object v2, p0, Lcom/google/android/gsf/loginservice/GLSUser;->mUpdatedPassword:Ljava/lang/String;

    return-void

    :cond_3
    iget-object v1, p0, Lcom/google/android/gsf/loginservice/GLSUser;->mEncryptedPassword:Ljava/lang/String;

    goto :goto_0

    :cond_4
    const-string v2, "oauthAccessToken"

    const-string v3, "1"

    invoke-virtual {v0, v2, v3}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_1
.end method

.method public addCreatedAccount(Lcom/google/android/gsf/loginservice/GLSUser$GLSSession;)V
    .locals 6
    .param p1    # Lcom/google/android/gsf/loginservice/GLSUser$GLSSession;

    const/4 v2, 0x0

    const/4 v3, 0x1

    iget-boolean v4, p1, Lcom/google/android/gsf/loginservice/GLSUser$GLSSession;->mSetupWizard:Z

    const/4 v5, 0x0

    move-object v0, p0

    move-object v1, p1

    invoke-virtual/range {v0 .. v5}, Lcom/google/android/gsf/loginservice/GLSUser;->addAccount(Lcom/google/android/gsf/loginservice/GLSUser$GLSSession;ZZZLjava/lang/String;)Landroid/content/Intent;

    iget-boolean v0, p1, Lcom/google/android/gsf/loginservice/GLSUser$GLSSession;->mSetupWizard:Z

    if-eqz v0, :cond_0

    invoke-virtual {p0}, Lcom/google/android/gsf/loginservice/GLSUser;->setBackupAccount()Z

    :cond_0
    return-void
.end method

.method public addWithRequestToken(Lcom/google/android/gsf/loginservice/GLSUser$GLSSession;Ljava/lang/String;Z)Landroid/content/Intent;
    .locals 7
    .param p1    # Lcom/google/android/gsf/loginservice/GLSUser$GLSSession;
    .param p2    # Ljava/lang/String;
    .param p3    # Z

    const/4 v0, 0x0

    iput-object v0, p0, Lcom/google/android/gsf/loginservice/GLSUser;->mMasterToken:Ljava/lang/String;

    iput-object v0, p0, Lcom/google/android/gsf/loginservice/GLSUser;->mEncryptedPassword:Ljava/lang/String;

    const/4 v2, 0x1

    const/4 v3, 0x0

    move-object v0, p0

    move-object v1, p1

    move v4, p3

    move-object v5, p2

    invoke-virtual/range {v0 .. v5}, Lcom/google/android/gsf/loginservice/GLSUser;->addAccount(Lcom/google/android/gsf/loginservice/GLSUser$GLSSession;ZZZLjava/lang/String;)Landroid/content/Intent;

    move-result-object v6

    return-object v6
.end method

.method public cacheToken(Ljava/lang/String;ILjava/lang/String;Ljava/lang/String;)V
    .locals 5
    .param p1    # Ljava/lang/String;
    .param p2    # I
    .param p3    # Ljava/lang/String;
    .param p4    # Ljava/lang/String;

    const-string v1, "weblogin:"

    invoke-virtual {p1, v1}, Ljava/lang/String;->startsWith(Ljava/lang/String;)Z

    move-result v1

    if-eqz v1, :cond_1

    :cond_0
    :goto_0
    return-void

    :cond_1
    if-eqz p2, :cond_0

    invoke-direct {p0, p1, p2}, Lcom/google/android/gsf/loginservice/GLSUser;->cacheKeyFromUidAndTokenType(Ljava/lang/String;I)Ljava/lang/String;

    move-result-object v0

    if-nez v0, :cond_2

    const-string v1, "GLSUser"

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "not caching since unable to generate a cache key for uid "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, p2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, ", service "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_0

    :cond_2
    sget-object v1, Lcom/google/android/gsf/loginservice/GLSUser;->sGLSContext:Lcom/google/android/gsf/loginservice/GLSUser$GLSContext;

    iget-object v1, v1, Lcom/google/android/gsf/loginservice/GLSUser$GLSContext;->mAccountManager:Landroid/accounts/AccountManager;

    iget-object v2, p0, Lcom/google/android/gsf/loginservice/GLSUser;->mAccount:Landroid/accounts/Account;

    invoke-virtual {v1, v2, v0, p3}, Landroid/accounts/AccountManager;->setAuthToken(Landroid/accounts/Account;Ljava/lang/String;Ljava/lang/String;)V

    if-eqz p4, :cond_0

    sget-object v1, Lcom/google/android/gsf/loginservice/GLSUser;->sGLSContext:Lcom/google/android/gsf/loginservice/GLSUser$GLSContext;

    iget-object v1, v1, Lcom/google/android/gsf/loginservice/GLSUser$GLSContext;->mAccountManager:Landroid/accounts/AccountManager;

    iget-object v2, p0, Lcom/google/android/gsf/loginservice/GLSUser;->mAccount:Landroid/accounts/Account;

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string v4, "EXP:"

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v1, v2, v3, p4}, Landroid/accounts/AccountManager;->setUserData(Landroid/accounts/Account;Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_0
.end method

.method public checkGplus(Lcom/google/android/gsf/loginservice/GLSUser$GLSSession;)Landroid/content/Intent;
    .locals 8
    .param p1    # Lcom/google/android/gsf/loginservice/GLSUser$GLSSession;

    const/4 v2, 0x0

    const/4 v4, 0x0

    const/4 v5, 0x1

    const-string v1, "LSID"

    move-object v0, p0

    move-object v3, p1

    move-object v6, v4

    move v7, v5

    invoke-direct/range {v0 .. v7}, Lcom/google/android/gsf/loginservice/GLSUser;->getAuthtokenRaw(Ljava/lang/String;ILcom/google/android/gsf/loginservice/GLSUser$GLSSession;Landroid/os/Bundle;ZLjava/lang/String;Z)Ljava/util/Map;

    move-result-object v1

    const-string v3, "LSID"

    move-object v0, p0

    move-object v6, p1

    invoke-direct/range {v0 .. v6}, Lcom/google/android/gsf/loginservice/GLSUser;->processResponse(Ljava/util/Map;ILjava/lang/String;Landroid/os/Bundle;ZLcom/google/android/gsf/loginservice/GLSUser$GLSSession;)Landroid/content/Intent;

    move-result-object v0

    return-object v0
.end method

.method public createProfile(Lcom/google/android/gsf/loginservice/GLSUser$GLSSession;)Lorg/json/JSONObject;
    .locals 7

    :try_start_0
    new-instance v6, Lorg/json/JSONStringer;

    invoke-direct {v6}, Lorg/json/JSONStringer;-><init>()V

    invoke-virtual {v6}, Lorg/json/JSONStringer;->object()Lorg/json/JSONStringer;

    invoke-virtual {p0, v6}, Lcom/google/android/gsf/loginservice/GLSUser;->prepareRequest(Lorg/json/JSONStringer;)V

    const-string v1, "LSID"

    const/4 v2, 0x0

    const/4 v4, 0x0

    const/4 v5, 0x1

    move-object v0, p0

    move-object v3, p1

    invoke-virtual/range {v0 .. v5}, Lcom/google/android/gsf/loginservice/GLSUser;->getAuthtoken(Ljava/lang/String;ILcom/google/android/gsf/loginservice/GLSUser$GLSSession;Landroid/os/Bundle;Z)Landroid/content/Intent;

    move-result-object v0

    invoke-virtual {p0, v0}, Lcom/google/android/gsf/loginservice/GLSUser;->getToken(Landroid/content/Intent;)Ljava/lang/String;

    move-result-object v0

    if-nez v0, :cond_1

    sget-object v0, Lcom/google/android/gsf/loginservice/GLSUser$Status;->BAD_AUTHENTICATION:Lcom/google/android/gsf/loginservice/GLSUser$Status;

    invoke-static {v0}, Lcom/google/android/gsf/loginservice/GLSUser$GLSContext;->jsonError(Lcom/google/android/gsf/loginservice/GLSUser$Status;)Lorg/json/JSONObject;

    move-result-object v0

    :cond_0
    :goto_0
    return-object v0

    :cond_1
    sget-object v1, Lcom/google/android/gsf/loginservice/GLSUser$RequestKey;->LSID:Lcom/google/android/gsf/loginservice/GLSUser$RequestKey;

    invoke-virtual {v1}, Lcom/google/android/gsf/loginservice/GLSUser$RequestKey;->getWire()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v6, v1}, Lorg/json/JSONStringer;->key(Ljava/lang/String;)Lorg/json/JSONStringer;

    move-result-object v1

    invoke-virtual {v1, v0}, Lorg/json/JSONStringer;->value(Ljava/lang/Object;)Lorg/json/JSONStringer;

    sget-object v0, Lcom/google/android/gsf/loginservice/GLSUser;->sGLSContext:Lcom/google/android/gsf/loginservice/GLSUser$GLSContext;

    sget-object v1, Lcom/google/android/gsf/loginservice/GLSUser$RequestKey;->FIRST_NAME:Lcom/google/android/gsf/loginservice/GLSUser$RequestKey;

    invoke-virtual {v1}, Lcom/google/android/gsf/loginservice/GLSUser$RequestKey;->getWire()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, p1, v6, v1}, Lcom/google/android/gsf/loginservice/GLSUser$GLSContext;->copyFromProfile(Lcom/google/android/gsf/loginservice/GLSUser$GLSSession;Lorg/json/JSONStringer;Ljava/lang/String;)V

    sget-object v0, Lcom/google/android/gsf/loginservice/GLSUser;->sGLSContext:Lcom/google/android/gsf/loginservice/GLSUser$GLSContext;

    sget-object v1, Lcom/google/android/gsf/loginservice/GLSUser$RequestKey;->LAST_NAME:Lcom/google/android/gsf/loginservice/GLSUser$RequestKey;

    invoke-virtual {v1}, Lcom/google/android/gsf/loginservice/GLSUser$RequestKey;->getWire()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, p1, v6, v1}, Lcom/google/android/gsf/loginservice/GLSUser$GLSContext;->copyFromProfile(Lcom/google/android/gsf/loginservice/GLSUser$GLSSession;Lorg/json/JSONStringer;Ljava/lang/String;)V

    sget-object v0, Lcom/google/android/gsf/loginservice/GLSUser;->sGLSContext:Lcom/google/android/gsf/loginservice/GLSUser$GLSContext;

    sget-object v1, Lcom/google/android/gsf/loginservice/GLSUser$RequestKey;->GENDER:Lcom/google/android/gsf/loginservice/GLSUser$RequestKey;

    invoke-virtual {v1}, Lcom/google/android/gsf/loginservice/GLSUser$RequestKey;->getWire()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, p1, v6, v1}, Lcom/google/android/gsf/loginservice/GLSUser$GLSContext;->copyFromProfile(Lcom/google/android/gsf/loginservice/GLSUser$GLSSession;Lorg/json/JSONStringer;Ljava/lang/String;)V

    sget-object v0, Lcom/google/android/gsf/loginservice/GLSUser$RequestKey;->AGREE_PERSONALIZED_CONTENT:Lcom/google/android/gsf/loginservice/GLSUser$RequestKey;

    invoke-virtual {v0}, Lcom/google/android/gsf/loginservice/GLSUser$RequestKey;->getWire()Ljava/lang/String;

    move-result-object v0

    iget-object v1, p1, Lcom/google/android/gsf/loginservice/GLSUser$GLSSession;->mUserData:Ljava/util/HashMap;

    invoke-virtual {v1, v0}, Ljava/util/HashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v1

    if-eqz v1, :cond_2

    sget-object v1, Lcom/google/android/gsf/loginservice/GLSUser;->sGLSContext:Lcom/google/android/gsf/loginservice/GLSUser$GLSContext;

    invoke-virtual {v1, p1, v6, v0}, Lcom/google/android/gsf/loginservice/GLSUser$GLSContext;->copyFromProfile(Lcom/google/android/gsf/loginservice/GLSUser$GLSSession;Lorg/json/JSONStringer;Ljava/lang/String;)V

    :cond_2
    iget-object v0, p1, Lcom/google/android/gsf/loginservice/GLSUser$GLSSession;->mAgreedToMobileTos:Ljava/lang/Boolean;

    if-eqz v0, :cond_3

    sget-object v0, Lcom/google/android/gsf/loginservice/GLSUser$RequestKey;->AGREE_MOBILE_TOS:Lcom/google/android/gsf/loginservice/GLSUser$RequestKey;

    invoke-virtual {v0}, Lcom/google/android/gsf/loginservice/GLSUser$RequestKey;->getWire()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v6, v0}, Lorg/json/JSONStringer;->key(Ljava/lang/String;)Lorg/json/JSONStringer;

    move-result-object v1

    iget-object v0, p1, Lcom/google/android/gsf/loginservice/GLSUser$GLSSession;->mAgreedToMobileTos:Ljava/lang/Boolean;

    invoke-virtual {v0}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v0

    if-eqz v0, :cond_5

    const-string v0, "1"

    :goto_1
    invoke-virtual {v1, v0}, Lorg/json/JSONStringer;->value(Ljava/lang/Object;)Lorg/json/JSONStringer;

    :cond_3
    iget-boolean v0, p1, Lcom/google/android/gsf/loginservice/GLSUser$GLSSession;->mCreatingAccount:Z

    if-eqz v0, :cond_4

    sget-object v0, Lcom/google/android/gsf/loginservice/GLSUser$RequestKey;->CREATED:Lcom/google/android/gsf/loginservice/GLSUser$RequestKey;

    invoke-virtual {v0}, Lcom/google/android/gsf/loginservice/GLSUser$RequestKey;->getWire()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v6, v0}, Lorg/json/JSONStringer;->key(Ljava/lang/String;)Lorg/json/JSONStringer;

    move-result-object v0

    const-string v1, "1"

    invoke-virtual {v0, v1}, Lorg/json/JSONStringer;->value(Ljava/lang/Object;)Lorg/json/JSONStringer;

    :cond_4
    invoke-static {p1, v6}, Lcom/google/android/gsf/loginservice/GLSUser;->addPhoto(Lcom/google/android/gsf/loginservice/GLSUser$GLSSession;Lorg/json/JSONStringer;)V

    invoke-virtual {v6}, Lorg/json/JSONStringer;->endObject()Lorg/json/JSONStringer;

    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v1, "createProfile request: "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-direct {p0, v0}, Lcom/google/android/gsf/loginservice/GLSUser;->log(Ljava/lang/String;)V

    sget-object v0, Lcom/google/android/gsf/loginservice/GLSUser;->sGLSContext:Lcom/google/android/gsf/loginservice/GLSUser$GLSContext;

    const-string v1, "https://android.clients.google.com/setup/createprofile"

    const-string v2, "createProfile"

    invoke-virtual {v0, v1, v6, v2}, Lcom/google/android/gsf/loginservice/GLSUser$GLSContext;->httpJson(Ljava/lang/String;Lorg/json/JSONStringer;Ljava/lang/String;)Lorg/json/JSONObject;

    move-result-object v0

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "createProfile response: "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v0}, Lorg/json/JSONObject;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-direct {p0, v1}, Lcom/google/android/gsf/loginservice/GLSUser;->log(Ljava/lang/String;)V

    sget-object v1, Lcom/google/android/gsf/loginservice/GLSUser$ResponseKey;->SERVICES:Lcom/google/android/gsf/loginservice/GLSUser$ResponseKey;

    invoke-virtual {v1}, Lcom/google/android/gsf/loginservice/GLSUser$ResponseKey;->getWire()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Lorg/json/JSONObject;->has(Ljava/lang/String;)Z

    move-result v1

    if-eqz v1, :cond_0

    sget-object v1, Lcom/google/android/gsf/loginservice/GLSUser$ResponseKey;->SERVICES:Lcom/google/android/gsf/loginservice/GLSUser$ResponseKey;

    invoke-virtual {v1}, Lcom/google/android/gsf/loginservice/GLSUser$ResponseKey;->getWire()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Lorg/json/JSONObject;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    if-eqz v1, :cond_0

    invoke-direct {p0, v1}, Lcom/google/android/gsf/loginservice/GLSUser;->refreshServicesAndSyncAdapters(Ljava/lang/String;)V
    :try_end_0
    .catch Lorg/json/JSONException; {:try_start_0 .. :try_end_0} :catch_0

    goto/16 :goto_0

    :catch_0
    move-exception v0

    sget-object v0, Lcom/google/android/gsf/loginservice/GLSUser$Status;->NETWORK_ERROR:Lcom/google/android/gsf/loginservice/GLSUser$Status;

    invoke-static {v0}, Lcom/google/android/gsf/loginservice/GLSUser$GLSContext;->jsonError(Lcom/google/android/gsf/loginservice/GLSUser$Status;)Lorg/json/JSONObject;

    move-result-object v0

    goto/16 :goto_0

    :cond_5
    :try_start_1
    const-string v0, "0"
    :try_end_1
    .catch Lorg/json/JSONException; {:try_start_1 .. :try_end_1} :catch_0

    goto :goto_1
.end method

.method public errorIntent(Ljava/util/Map;Lcom/google/android/gsf/loginservice/GLSUser$Status;ILjava/lang/String;Lcom/google/android/gsf/loginservice/GLSUser$GLSSession;Landroid/os/Bundle;)Landroid/content/Intent;
    .locals 9
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/Map",
            "<",
            "Ljava/lang/String;",
            "Ljava/lang/String;",
            ">;",
            "Lcom/google/android/gsf/loginservice/GLSUser$Status;",
            "I",
            "Ljava/lang/String;",
            "Lcom/google/android/gsf/loginservice/GLSUser$GLSSession;",
            "Landroid/os/Bundle;",
            ")",
            "Landroid/content/Intent;"
        }
    .end annotation

    sget-object v0, Lcom/google/android/gsf/loginservice/GLSUser;->sGLSContext:Lcom/google/android/gsf/loginservice/GLSUser$GLSContext;

    iget-object v0, v0, Lcom/google/android/gsf/loginservice/GLSUser$GLSContext;->mContext:Landroid/content/Context;

    new-instance v1, Landroid/content/Intent;

    const-class v2, Lcom/google/android/gsf/login/LoginActivity;

    invoke-direct {v1, v0, v2}, Landroid/content/Intent;-><init>(Landroid/content/Context;Ljava/lang/Class;)V

    sget-object v2, Lcom/google/android/gsf/loginservice/GLSUser;->sGLSContext:Lcom/google/android/gsf/loginservice/GLSUser$GLSContext;

    invoke-virtual {v2, v1, p5}, Lcom/google/android/gsf/loginservice/GLSUser$GLSContext;->addSession(Landroid/content/Intent;Lcom/google/android/gsf/loginservice/GLSUser$GLSSession;)Lcom/google/android/gsf/loginservice/GLSUser$GLSSession;

    move-result-object v2

    iput p3, v2, Lcom/google/android/gsf/loginservice/GLSUser$GLSSession;->mCallingUID:I

    const-string v3, "accountAuthenticatorResponse"

    iget-object v4, v2, Lcom/google/android/gsf/loginservice/GLSUser$GLSSession;->mAccountAuthenticatorResponse:Landroid/accounts/AccountAuthenticatorResponse;

    invoke-virtual {v1, v3, v4}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Landroid/os/Parcelable;)Landroid/content/Intent;

    const-string v3, "authAccount"

    iget-object v4, p0, Lcom/google/android/gsf/loginservice/GLSUser;->mEmail:Ljava/lang/String;

    invoke-virtual {v1, v3, v4}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    sget-object v3, Lcom/google/android/gsf/loginservice/GLSUser$RequestKey;->SERVICE:Lcom/google/android/gsf/loginservice/GLSUser$RequestKey;

    invoke-virtual {v3}, Lcom/google/android/gsf/loginservice/GLSUser$RequestKey;->getWire()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v1, v3, p4}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    const-string v3, "pendingIntent"

    iget-object v4, v2, Lcom/google/android/gsf/loginservice/GLSUser$GLSSession;->mPendingIntent:Landroid/app/PendingIntent;

    invoke-virtual {v1, v3, v4}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Landroid/os/Parcelable;)Landroid/content/Intent;

    sget-object v3, Lcom/google/android/gsf/loginservice/GLSUser;->sGLSContext:Lcom/google/android/gsf/loginservice/GLSUser$GLSContext;

    iget-object v3, v3, Lcom/google/android/gsf/loginservice/GLSUser$GLSContext;->mContext:Landroid/content/Context;

    invoke-virtual {p2, v3, v1}, Lcom/google/android/gsf/loginservice/GLSUser$Status;->toIntent(Landroid/content/Context;Landroid/content/Intent;)V

    const-string v3, "authFailedMessage"

    const v4, 0x7f0800bb    # com.google.android.gsf.login.R.string.gls_notification_login_error

    invoke-virtual {v0, v4}, Landroid/content/Context;->getText(I)Ljava/lang/CharSequence;

    move-result-object v0

    invoke-virtual {v1, v3, v0}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/CharSequence;)Landroid/content/Intent;

    if-eqz p1, :cond_1

    sget-object v0, Lcom/google/android/gsf/loginservice/GLSUser$ResponseKey;->PERMISSION:Lcom/google/android/gsf/loginservice/GLSUser$ResponseKey;

    invoke-virtual {v0}, Lcom/google/android/gsf/loginservice/GLSUser$ResponseKey;->getWire()Ljava/lang/String;

    move-result-object v0

    invoke-interface {p1, v0}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/String;

    if-eqz v0, :cond_0

    const-string v3, "\\|"

    invoke-virtual {v0, v3}, Ljava/lang/String;->split(Ljava/lang/String;)[Ljava/lang/String;

    move-result-object v3

    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, v2, Lcom/google/android/gsf/loginservice/GLSUser$GLSSession;->mPermission:Ljava/util/ArrayList;

    array-length v4, v3

    const/4 v0, 0x0

    :goto_0
    if-ge v0, v4, :cond_0

    aget-object v5, v3, v0

    :try_start_0
    iget-object v6, v2, Lcom/google/android/gsf/loginservice/GLSUser$GLSSession;->mPermission:Ljava/util/ArrayList;

    const-string v7, "UTF-8"

    invoke-static {v5, v7}, Ljava/net/URLDecoder;->decode(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v7

    invoke-virtual {v6, v7}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z
    :try_end_0
    .catch Ljava/io/UnsupportedEncodingException; {:try_start_0 .. :try_end_0} :catch_0

    :goto_1
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    :catch_0
    move-exception v6

    const-string v6, "GLSUser"

    new-instance v7, Ljava/lang/StringBuilder;

    invoke-direct {v7}, Ljava/lang/StringBuilder;-><init>()V

    const-string v8, "Error decoding "

    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    invoke-virtual {v7, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    invoke-static {v6, v5}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_1

    :cond_0
    sget-object v0, Lcom/google/android/gsf/loginservice/GLSUser$ResponseKey;->DETAIL:Lcom/google/android/gsf/loginservice/GLSUser$ResponseKey;

    invoke-virtual {v0}, Lcom/google/android/gsf/loginservice/GLSUser$ResponseKey;->getWire()Ljava/lang/String;

    move-result-object v0

    invoke-interface {p1, v0}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/String;

    iput-object v0, v2, Lcom/google/android/gsf/loginservice/GLSUser$GLSSession;->mDetail:Ljava/lang/String;

    sget-object v0, Lcom/google/android/gsf/loginservice/GLSUser$ResponseKey;->URL:Lcom/google/android/gsf/loginservice/GLSUser$ResponseKey;

    invoke-virtual {v0}, Lcom/google/android/gsf/loginservice/GLSUser$ResponseKey;->getWire()Ljava/lang/String;

    move-result-object v0

    invoke-interface {p1, v0}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/String;

    iput-object v0, v2, Lcom/google/android/gsf/loginservice/GLSUser$GLSSession;->mUrl:Ljava/lang/String;

    :cond_1
    return-object v1
.end method

.method public getAccount()Landroid/accounts/Account;
    .locals 1

    iget-object v0, p0, Lcom/google/android/gsf/loginservice/GLSUser;->mAccount:Landroid/accounts/Account;

    return-object v0
.end method

.method public getAccountManager()Landroid/accounts/AccountManager;
    .locals 1

    sget-object v0, Lcom/google/android/gsf/loginservice/GLSUser;->sGLSContext:Lcom/google/android/gsf/loginservice/GLSUser$GLSContext;

    iget-object v0, v0, Lcom/google/android/gsf/loginservice/GLSUser$GLSContext;->mAccountManager:Landroid/accounts/AccountManager;

    return-object v0
.end method

.method getAppPackage(I)Ljava/lang/String;
    .locals 2
    .param p1    # I

    sget-object v1, Lcom/google/android/gsf/loginservice/GLSUser;->sGLSContext:Lcom/google/android/gsf/loginservice/GLSUser$GLSContext;

    iget-object v1, v1, Lcom/google/android/gsf/loginservice/GLSUser$GLSContext;->mPM:Landroid/content/pm/PackageManager;

    invoke-virtual {v1, p1}, Landroid/content/pm/PackageManager;->getPackagesForUid(I)[Ljava/lang/String;

    move-result-object v0

    if-eqz v0, :cond_0

    array-length v1, v0

    if-nez v1, :cond_1

    :cond_0
    const/4 v1, 0x0

    :goto_0
    return-object v1

    :cond_1
    const/4 v1, 0x0

    aget-object v1, v0, v1

    goto :goto_0
.end method

.method public getAuthtoken(Ljava/lang/String;ILcom/google/android/gsf/loginservice/GLSUser$GLSSession;)Landroid/content/Intent;
    .locals 8
    .param p1    # Ljava/lang/String;
    .param p2    # I
    .param p3    # Lcom/google/android/gsf/loginservice/GLSUser$GLSSession;

    iget-object v4, p3, Lcom/google/android/gsf/loginservice/GLSUser$GLSSession;->mAccountManagerOptions:Landroid/os/Bundle;

    if-nez p2, :cond_0

    iget-object v7, p3, Lcom/google/android/gsf/loginservice/GLSUser$GLSSession;->mPendingIntent:Landroid/app/PendingIntent;

    if-eqz v7, :cond_0

    invoke-virtual {v7}, Landroid/app/PendingIntent;->getTargetPackage()Ljava/lang/String;

    move-result-object v6

    invoke-static {v6}, Lcom/google/android/gsf/loginservice/GLSUser;->getUid(Ljava/lang/String;)I

    move-result p2

    iput p2, p3, Lcom/google/android/gsf/loginservice/GLSUser$GLSSession;->mCallingUID:I

    :cond_0
    if-nez p2, :cond_1

    const/4 v5, 0x1

    :goto_0
    move-object v0, p0

    move-object v1, p1

    move v2, p2

    move-object v3, p3

    invoke-virtual/range {v0 .. v5}, Lcom/google/android/gsf/loginservice/GLSUser;->getAuthtoken(Ljava/lang/String;ILcom/google/android/gsf/loginservice/GLSUser$GLSSession;Landroid/os/Bundle;Z)Landroid/content/Intent;

    move-result-object v0

    return-object v0

    :cond_1
    invoke-direct {p0, p1, p2}, Lcom/google/android/gsf/loginservice/GLSUser;->hasPermission(Ljava/lang/String;I)Z

    move-result v5

    goto :goto_0
.end method

.method public getAuthtoken(Ljava/lang/String;ILcom/google/android/gsf/loginservice/GLSUser$GLSSession;Landroid/os/Bundle;Z)Landroid/content/Intent;
    .locals 13
    .param p1    # Ljava/lang/String;
    .param p2    # I
    .param p3    # Lcom/google/android/gsf/loginservice/GLSUser$GLSSession;
    .param p4    # Landroid/os/Bundle;
    .param p5    # Z

    if-nez p1, :cond_0

    const-string p1, "ac2dm"

    :cond_0
    if-eqz p5, :cond_1

    iget-boolean v0, p0, Lcom/google/android/gsf/loginservice/GLSUser;->existing:Z

    if-eqz v0, :cond_1

    invoke-direct {p0}, Lcom/google/android/gsf/loginservice/GLSUser;->hasSecret()Z

    move-result v0

    if-eqz v0, :cond_1

    invoke-virtual {p0, p1, p2}, Lcom/google/android/gsf/loginservice/GLSUser;->getCachedToken(Ljava/lang/String;I)Ljava/lang/String;

    move-result-object v4

    if-eqz v4, :cond_1

    new-instance v1, Ljava/util/HashMap;

    invoke-direct {v1}, Ljava/util/HashMap;-><init>()V

    const/4 v6, 0x0

    move-object v0, p0

    move-object v2, p1

    move v3, p2

    move-object/from16 v5, p3

    invoke-direct/range {v0 .. v6}, Lcom/google/android/gsf/loginservice/GLSUser;->successIntent(Ljava/util/Map;Ljava/lang/String;ILjava/lang/String;Lcom/google/android/gsf/loginservice/GLSUser$GLSSession;Z)Landroid/content/Intent;

    move-result-object v0

    :goto_0
    return-object v0

    :cond_1
    const/4 v11, 0x0

    const/4 v12, 0x0

    move-object v5, p0

    move-object v6, p1

    move v7, p2

    move-object/from16 v8, p3

    move-object/from16 v9, p4

    move/from16 v10, p5

    invoke-direct/range {v5 .. v12}, Lcom/google/android/gsf/loginservice/GLSUser;->getAuthtokenRaw(Ljava/lang/String;ILcom/google/android/gsf/loginservice/GLSUser$GLSSession;Landroid/os/Bundle;ZLjava/lang/String;Z)Ljava/util/Map;

    move-result-object v6

    move-object v5, p0

    move v7, p2

    move-object v8, p1

    move-object/from16 v9, p4

    move/from16 v10, p5

    move-object/from16 v11, p3

    invoke-direct/range {v5 .. v11}, Lcom/google/android/gsf/loginservice/GLSUser;->processResponse(Ljava/util/Map;ILjava/lang/String;Landroid/os/Bundle;ZLcom/google/android/gsf/loginservice/GLSUser$GLSSession;)Landroid/content/Intent;

    move-result-object v0

    goto :goto_0
.end method

.method getCachedToken(Ljava/lang/String;I)Ljava/lang/String;
    .locals 10
    .param p1    # Ljava/lang/String;
    .param p2    # I

    const/4 v5, 0x0

    if-nez p1, :cond_1

    move-object v2, v5

    :cond_0
    :goto_0
    return-object v2

    :cond_1
    if-nez p2, :cond_2

    move-object v2, v5

    goto :goto_0

    :cond_2
    const-string v6, "weblogin:"

    invoke-virtual {p1, v6}, Ljava/lang/String;->startsWith(Ljava/lang/String;)Z

    move-result v6

    if-eqz v6, :cond_3

    move-object v2, v5

    goto :goto_0

    :cond_3
    invoke-direct {p0, p1, p2}, Lcom/google/android/gsf/loginservice/GLSUser;->cacheKeyFromUidAndTokenType(Ljava/lang/String;I)Ljava/lang/String;

    move-result-object v0

    if-nez v0, :cond_4

    const-string v6, "GLSUser"

    new-instance v7, Ljava/lang/StringBuilder;

    invoke-direct {v7}, Ljava/lang/StringBuilder;-><init>()V

    const-string v8, "not checking cache since unable to generate a cache key for uid "

    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    invoke-virtual {v7, p2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v7

    const-string v8, ", service "

    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    invoke-virtual {v7, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    invoke-virtual {v7}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v7

    invoke-static {v6, v7}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    move-object v2, v5

    goto :goto_0

    :cond_4
    sget-object v6, Lcom/google/android/gsf/loginservice/GLSUser;->sGLSContext:Lcom/google/android/gsf/loginservice/GLSUser$GLSContext;

    iget-object v6, v6, Lcom/google/android/gsf/loginservice/GLSUser$GLSContext;->mAccountManager:Landroid/accounts/AccountManager;

    iget-object v7, p0, Lcom/google/android/gsf/loginservice/GLSUser;->mAccount:Landroid/accounts/Account;

    invoke-virtual {v6, v7, v0}, Landroid/accounts/AccountManager;->peekAuthToken(Landroid/accounts/Account;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v2

    if-eqz v2, :cond_0

    sget-object v6, Lcom/google/android/gsf/loginservice/GLSUser;->sGLSContext:Lcom/google/android/gsf/loginservice/GLSUser$GLSContext;

    iget-object v6, v6, Lcom/google/android/gsf/loginservice/GLSUser$GLSContext;->mAccountManager:Landroid/accounts/AccountManager;

    iget-object v7, p0, Lcom/google/android/gsf/loginservice/GLSUser;->mAccount:Landroid/accounts/Account;

    new-instance v8, Ljava/lang/StringBuilder;

    invoke-direct {v8}, Ljava/lang/StringBuilder;-><init>()V

    const-string v9, "EXP:"

    invoke-virtual {v8, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v8

    invoke-virtual {v8, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v8

    invoke-virtual {v8}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v8

    invoke-virtual {v6, v7, v8}, Landroid/accounts/AccountManager;->getUserData(Landroid/accounts/Account;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    if-eqz v1, :cond_0

    invoke-static {v1}, Ljava/lang/Long;->parseLong(Ljava/lang/String;)J

    move-result-wide v3

    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v6

    const-wide/16 v8, 0x3e8

    div-long/2addr v6, v8

    cmp-long v6, v3, v6

    if-gez v6, :cond_0

    sget-object v6, Lcom/google/android/gsf/loginservice/GLSUser;->sGLSContext:Lcom/google/android/gsf/loginservice/GLSUser$GLSContext;

    iget-object v6, v6, Lcom/google/android/gsf/loginservice/GLSUser$GLSContext;->mAccountManager:Landroid/accounts/AccountManager;

    const-string v7, "com.google"

    invoke-virtual {v6, v7, v2}, Landroid/accounts/AccountManager;->invalidateAuthToken(Ljava/lang/String;Ljava/lang/String;)V

    move-object v2, v5

    goto :goto_0
.end method

.method getCaptchaData(Ljava/lang/String;Ljava/lang/String;Landroid/content/Intent;Lcom/google/android/gsf/loginservice/GLSUser$GLSSession;)V
    .locals 4

    const/4 v2, 0x2

    const-string v0, "http"

    invoke-virtual {p2, v0}, Ljava/lang/String;->startsWith(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_2

    :goto_0
    const-string v0, "GLSUser"

    invoke-static {v0, v2}, Landroid/util/Log;->isLoggable(Ljava/lang/String;I)Z

    move-result v0

    if-eqz v0, :cond_0

    const-string v0, "GLSUser"

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "captcha url is ["

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, "]"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Landroid/util/Log;->v(Ljava/lang/String;Ljava/lang/String;)I

    :cond_0
    const/4 v0, 0x1

    invoke-static {v0}, Landroid/support/v4/net/TrafficStatsCompat;->setThreadStatsTag(I)V

    :try_start_0
    new-instance v0, Lorg/apache/http/client/methods/HttpGet;

    invoke-direct {v0, p2}, Lorg/apache/http/client/methods/HttpGet;-><init>(Ljava/lang/String;)V

    sget-object v1, Lcom/google/android/gsf/loginservice/GLSUser;->sGLSContext:Lcom/google/android/gsf/loginservice/GLSUser$GLSContext;

    iget-object v1, v1, Lcom/google/android/gsf/loginservice/GLSUser$GLSContext;->mHttpClient:Lorg/apache/http/client/HttpClient;

    invoke-interface {v1, v0}, Lorg/apache/http/client/HttpClient;->execute(Lorg/apache/http/client/methods/HttpUriRequest;)Lorg/apache/http/HttpResponse;

    move-result-object v0

    const-string v1, "GLSUser"

    const/4 v2, 0x2

    invoke-static {v1, v2}, Landroid/util/Log;->isLoggable(Ljava/lang/String;I)Z

    move-result v1

    if-eqz v1, :cond_1

    const-string v1, "GLSUser"

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "bitmap response is "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-interface {v0}, Lorg/apache/http/HttpResponse;->getStatusLine()Lorg/apache/http/StatusLine;

    move-result-object v3

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2}, Landroid/util/Log;->v(Ljava/lang/String;Ljava/lang/String;)I

    :cond_1
    const-string v1, "X-Google-Captcha-Error"

    invoke-interface {v0, v1}, Lorg/apache/http/HttpResponse;->containsHeader(Ljava/lang/String;)Z

    move-result v1

    if-eqz v1, :cond_3

    sget-object v0, Lcom/google/android/gsf/loginservice/GLSUser$Status;->NETWORK_ERROR:Lcom/google/android/gsf/loginservice/GLSUser$Status;

    sget-object v1, Lcom/google/android/gsf/loginservice/GLSUser;->sGLSContext:Lcom/google/android/gsf/loginservice/GLSUser$GLSContext;

    iget-object v1, v1, Lcom/google/android/gsf/loginservice/GLSUser$GLSContext;->mContext:Landroid/content/Context;

    invoke-virtual {v0, v1, p3}, Lcom/google/android/gsf/loginservice/GLSUser$Status;->toIntent(Landroid/content/Context;Landroid/content/Intent;)V
    :try_end_0
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    invoke-static {}, Landroid/support/v4/net/TrafficStatsCompat;->clearThreadStatsTag()V

    :goto_1
    return-void

    :cond_2
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v1, "https://www.google.com/accounts/"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0, p2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object p2

    goto/16 :goto_0

    :cond_3
    :try_start_1
    invoke-interface {v0}, Lorg/apache/http/HttpResponse;->getEntity()Lorg/apache/http/HttpEntity;

    move-result-object v0

    invoke-static {v0}, Lorg/apache/http/util/EntityUtils;->toByteArray(Lorg/apache/http/HttpEntity;)[B

    move-result-object v0

    sget-object v1, Lcom/google/android/gsf/loginservice/GLSUser$ResponseKey;->CAPTCHA_DATA:Lcom/google/android/gsf/loginservice/GLSUser$ResponseKey;

    invoke-virtual {v1}, Lcom/google/android/gsf/loginservice/GLSUser$ResponseKey;->getWire()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {p3, v1, v0}, Landroid/content/Intent;->putExtra(Ljava/lang/String;[B)Landroid/content/Intent;

    iput-object p1, p4, Lcom/google/android/gsf/loginservice/GLSUser$GLSSession;->mCaptchaToken:Ljava/lang/String;

    const/4 v1, 0x0

    array-length v2, v0

    invoke-static {v0, v1, v2}, Landroid/graphics/BitmapFactory;->decodeByteArray([BII)Landroid/graphics/Bitmap;

    move-result-object v0

    iget-object v1, p4, Lcom/google/android/gsf/loginservice/GLSUser$GLSSession;->mUserData:Ljava/util/HashMap;

    sget-object v2, Lcom/google/android/gsf/loginservice/GLSUser$RequestKey;->CAPTCHA_BITMAP:Lcom/google/android/gsf/loginservice/GLSUser$RequestKey;

    invoke-virtual {v2}, Lcom/google/android/gsf/loginservice/GLSUser$RequestKey;->getWire()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2, v0}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;
    :try_end_1
    .catch Ljava/io/IOException; {:try_start_1 .. :try_end_1} :catch_0
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    invoke-static {}, Landroid/support/v4/net/TrafficStatsCompat;->clearThreadStatsTag()V

    goto :goto_1

    :catch_0
    move-exception v0

    :try_start_2
    sget-object v0, Lcom/google/android/gsf/loginservice/GLSUser$Status;->NETWORK_ERROR:Lcom/google/android/gsf/loginservice/GLSUser$Status;

    sget-object v1, Lcom/google/android/gsf/loginservice/GLSUser;->sGLSContext:Lcom/google/android/gsf/loginservice/GLSUser$GLSContext;

    iget-object v1, v1, Lcom/google/android/gsf/loginservice/GLSUser$GLSContext;->mContext:Landroid/content/Context;

    invoke-virtual {v0, v1, p3}, Lcom/google/android/gsf/loginservice/GLSUser$Status;->toIntent(Landroid/content/Context;Landroid/content/Intent;)V
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    invoke-static {}, Landroid/support/v4/net/TrafficStatsCompat;->clearThreadStatsTag()V

    goto :goto_1

    :catchall_0
    move-exception v0

    invoke-static {}, Landroid/support/v4/net/TrafficStatsCompat;->clearThreadStatsTag()V

    throw v0
.end method

.method getSignatureFingerprint(Ljava/lang/String;)Ljava/lang/String;
    .locals 6
    .param p1    # Ljava/lang/String;

    const/4 v3, 0x0

    :try_start_0
    sget-object v4, Lcom/google/android/gsf/loginservice/GLSUser;->sGLSContext:Lcom/google/android/gsf/loginservice/GLSUser$GLSContext;

    iget-object v4, v4, Lcom/google/android/gsf/loginservice/GLSUser$GLSContext;->mPM:Landroid/content/pm/PackageManager;

    const/4 v5, 0x0

    invoke-virtual {v4, p1, v5}, Landroid/content/pm/PackageManager;->getApplicationInfo(Ljava/lang/String;I)Landroid/content/pm/ApplicationInfo;

    move-result-object v0

    if-nez v0, :cond_1

    :cond_0
    :goto_0
    return-object v3

    :cond_1
    sget-object v4, Lcom/google/android/gsf/loginservice/GLSUser;->sGLSContext:Lcom/google/android/gsf/loginservice/GLSUser$GLSContext;

    iget-object v4, v4, Lcom/google/android/gsf/loginservice/GLSUser$GLSContext;->mPM:Landroid/content/pm/PackageManager;

    const/16 v5, 0x40

    invoke-virtual {v4, p1, v5}, Landroid/content/pm/PackageManager;->getPackageInfo(Ljava/lang/String;I)Landroid/content/pm/PackageInfo;

    move-result-object v2

    if-eqz v2, :cond_0

    iget-object v4, v2, Landroid/content/pm/PackageInfo;->signatures:[Landroid/content/pm/Signature;

    if-eqz v4, :cond_0

    iget-object v4, v2, Landroid/content/pm/PackageInfo;->signatures:[Landroid/content/pm/Signature;

    array-length v4, v4

    if-eqz v4, :cond_0

    iget-object v4, v2, Landroid/content/pm/PackageInfo;->signatures:[Landroid/content/pm/Signature;

    const/4 v5, 0x0

    aget-object v4, v4, v5

    if-eqz v4, :cond_0

    iget-object v4, v2, Landroid/content/pm/PackageInfo;->signatures:[Landroid/content/pm/Signature;

    const/4 v5, 0x0

    aget-object v4, v4, v5

    invoke-static {v4}, Lcom/google/android/gsf/loginservice/GLSUser;->signatureDigest(Landroid/content/pm/Signature;)Ljava/lang/String;
    :try_end_0
    .catch Landroid/content/pm/PackageManager$NameNotFoundException; {:try_start_0 .. :try_end_0} :catch_0
    .catch Ljava/security/NoSuchAlgorithmException; {:try_start_0 .. :try_end_0} :catch_1

    move-result-object v3

    goto :goto_0

    :catch_0
    move-exception v1

    goto :goto_0

    :catch_1
    move-exception v1

    goto :goto_0
.end method

.method public getStoredPermission(Ljava/lang/String;I)Z
    .locals 6

    const/4 v5, 0x0

    sget-object v0, Lcom/google/android/gsf/loginservice/GLSUser;->sGLSContext:Lcom/google/android/gsf/loginservice/GLSUser$GLSContext;

    iget-object v0, v0, Lcom/google/android/gsf/loginservice/GLSUser$GLSContext;->mAccountManager:Landroid/accounts/AccountManager;

    iget-object v1, p0, Lcom/google/android/gsf/loginservice/GLSUser;->mAccount:Landroid/accounts/Account;

    invoke-direct {p0, p2, p1}, Lcom/google/android/gsf/loginservice/GLSUser;->getPermissionKey(ILjava/lang/String;)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Landroid/accounts/AccountManager;->getUserData(Landroid/accounts/Account;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    const-string v1, "1"

    invoke-virtual {v1, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-nez v1, :cond_0

    sget-object v0, Lcom/google/android/gsf/loginservice/GLSUser;->sGLSContext:Lcom/google/android/gsf/loginservice/GLSUser$GLSContext;

    iget-object v0, v0, Lcom/google/android/gsf/loginservice/GLSUser$GLSContext;->mAccountManager:Landroid/accounts/AccountManager;

    iget-object v1, p0, Lcom/google/android/gsf/loginservice/GLSUser;->mAccount:Landroid/accounts/Account;

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "perm."

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, p2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, "."

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Landroid/accounts/AccountManager;->getUserData(Landroid/accounts/Account;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    if-eqz v0, :cond_0

    sget-object v1, Lcom/google/android/gsf/loginservice/GLSUser;->sGLSContext:Lcom/google/android/gsf/loginservice/GLSUser$GLSContext;

    iget-object v1, v1, Lcom/google/android/gsf/loginservice/GLSUser$GLSContext;->mAccountManager:Landroid/accounts/AccountManager;

    iget-object v2, p0, Lcom/google/android/gsf/loginservice/GLSUser;->mAccount:Landroid/accounts/Account;

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string v4, "perm."

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3, p2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v3

    const-string v4, "."

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v1, v2, v3, v5}, Landroid/accounts/AccountManager;->setUserData(Landroid/accounts/Account;Ljava/lang/String;Ljava/lang/String;)V

    invoke-virtual {p0, p1, p2, v0, v5}, Lcom/google/android/gsf/loginservice/GLSUser;->setStoredPermission(Ljava/lang/String;ILjava/lang/String;Landroid/os/Bundle;)V

    :cond_0
    const-string v1, "1"

    invoke-virtual {v1, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    return v0
.end method

.method public getToken(Landroid/content/Intent;)Ljava/lang/String;
    .locals 1
    .param p1    # Landroid/content/Intent;

    const-string v0, "authtoken"

    invoke-virtual {p1, v0}, Landroid/content/Intent;->getStringExtra(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public getToken(Lcom/google/android/gsf/loginservice/GLSUser$GLSSession;Ljava/lang/String;)Ljava/lang/String;
    .locals 2
    .param p1    # Lcom/google/android/gsf/loginservice/GLSUser$GLSSession;
    .param p2    # Ljava/lang/String;

    const/4 v1, 0x0

    invoke-virtual {p0, p2, v1, p1}, Lcom/google/android/gsf/loginservice/GLSUser;->getAuthtoken(Ljava/lang/String;ILcom/google/android/gsf/loginservice/GLSUser$GLSSession;)Landroid/content/Intent;

    move-result-object v0

    invoke-virtual {p0, v0}, Lcom/google/android/gsf/loginservice/GLSUser;->getToken(Landroid/content/Intent;)Ljava/lang/String;

    move-result-object v1

    return-object v1
.end method

.method public getTokenIntent(Ljava/lang/String;Lcom/google/android/gsf/loginservice/GLSUser$GLSSession;)Landroid/content/Intent;
    .locals 1
    .param p1    # Ljava/lang/String;
    .param p2    # Lcom/google/android/gsf/loginservice/GLSUser$GLSSession;

    const/4 v0, 0x0

    invoke-virtual {p0, p1, v0, p2}, Lcom/google/android/gsf/loginservice/GLSUser;->getAuthtoken(Ljava/lang/String;ILcom/google/android/gsf/loginservice/GLSUser$GLSSession;)Landroid/content/Intent;

    move-result-object v0

    return-object v0
.end method

.method public hasService(Ljava/lang/String;)Z
    .locals 10
    .param p1    # Ljava/lang/String;

    const/4 v6, 0x0

    sget-object v7, Lcom/google/android/gsf/loginservice/GLSUser;->sGLSContext:Lcom/google/android/gsf/loginservice/GLSUser$GLSContext;

    iget-object v7, v7, Lcom/google/android/gsf/loginservice/GLSUser$GLSContext;->mAccountManager:Landroid/accounts/AccountManager;

    iget-object v8, p0, Lcom/google/android/gsf/loginservice/GLSUser;->mAccount:Landroid/accounts/Account;

    sget-object v9, Lcom/google/android/gsf/loginservice/GLSUser$ResponseKey;->SERVICES:Lcom/google/android/gsf/loginservice/GLSUser$ResponseKey;

    invoke-virtual {v9}, Lcom/google/android/gsf/loginservice/GLSUser$ResponseKey;->getWire()Ljava/lang/String;

    move-result-object v9

    invoke-virtual {v7, v8, v9}, Landroid/accounts/AccountManager;->getUserData(Landroid/accounts/Account;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    if-nez v1, :cond_1

    :cond_0
    :goto_0
    return v6

    :cond_1
    const-string v7, ","

    invoke-virtual {v1, v7}, Ljava/lang/String;->split(Ljava/lang/String;)[Ljava/lang/String;

    move-result-object v2

    move-object v3, v2

    array-length v5, v3

    const/4 v4, 0x0

    :goto_1
    if-ge v4, v5, :cond_0

    aget-object v0, v3, v4

    invoke-virtual {v0, p1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v7

    if-eqz v7, :cond_2

    const/4 v6, 0x1

    goto :goto_0

    :cond_2
    add-int/lit8 v4, v4, 0x1

    goto :goto_1
.end method

.method public injectTestResponse(Ljava/lang/String;Ljava/lang/String;)V
    .locals 3
    .param p1    # Ljava/lang/String;
    .param p2    # Ljava/lang/String;

    sget-object v0, Lcom/google/android/gsf/loginservice/GLSUser;->sGLSContext:Lcom/google/android/gsf/loginservice/GLSUser$GLSContext;

    invoke-virtual {v0}, Lcom/google/android/gsf/loginservice/GLSUser$GLSContext;->getInjector()Lcom/google/android/gsf/loginservice/GLSUser$HttpTestInjector;

    move-result-object v0

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    iget-object v2, p0, Lcom/google/android/gsf/loginservice/GLSUser;->mEmail:Ljava/lang/String;

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, ":"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1, p2}, Lcom/google/android/gsf/loginservice/GLSUser$HttpTestInjector;->injectTestResponse(Ljava/lang/String;Ljava/lang/String;)V

    return-void
.end method

.method public invalidateToken(Ljava/lang/String;I)V
    .locals 4
    .param p1    # Ljava/lang/String;
    .param p2    # I

    invoke-direct {p0, p1, p2}, Lcom/google/android/gsf/loginservice/GLSUser;->cacheKeyFromUidAndTokenType(Ljava/lang/String;I)Ljava/lang/String;

    move-result-object v0

    if-nez v0, :cond_1

    :cond_0
    :goto_0
    return-void

    :cond_1
    sget-object v2, Lcom/google/android/gsf/loginservice/GLSUser;->sGLSContext:Lcom/google/android/gsf/loginservice/GLSUser$GLSContext;

    iget-object v2, v2, Lcom/google/android/gsf/loginservice/GLSUser$GLSContext;->mAccountManager:Landroid/accounts/AccountManager;

    iget-object v3, p0, Lcom/google/android/gsf/loginservice/GLSUser;->mAccount:Landroid/accounts/Account;

    invoke-virtual {v2, v3, v0}, Landroid/accounts/AccountManager;->peekAuthToken(Landroid/accounts/Account;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    if-eqz v1, :cond_0

    sget-object v2, Lcom/google/android/gsf/loginservice/GLSUser;->sGLSContext:Lcom/google/android/gsf/loginservice/GLSUser$GLSContext;

    iget-object v2, v2, Lcom/google/android/gsf/loginservice/GLSUser$GLSContext;->mAccountManager:Landroid/accounts/AccountManager;

    const-string v3, "com.google"

    invoke-virtual {v2, v3, v1}, Landroid/accounts/AccountManager;->invalidateAuthToken(Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_0
.end method

.method public isBrowser()Z
    .locals 1

    iget-boolean v0, p0, Lcom/google/android/gsf/loginservice/GLSUser;->mUseBrowserFlow:Z

    return v0
.end method

.method public prepareRequest(Lorg/json/JSONStringer;)V
    .locals 5
    .param p1    # Lorg/json/JSONStringer;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lorg/json/JSONException;
        }
    .end annotation

    const/4 v4, 0x0

    sget-object v2, Lcom/google/android/gsf/loginservice/GLSUser$RequestKey;->EMAIL:Lcom/google/android/gsf/loginservice/GLSUser$RequestKey;

    invoke-virtual {v2}, Lcom/google/android/gsf/loginservice/GLSUser$RequestKey;->getWire()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {p1, v2}, Lorg/json/JSONStringer;->key(Ljava/lang/String;)Lorg/json/JSONStringer;

    move-result-object v2

    iget-object v3, p0, Lcom/google/android/gsf/loginservice/GLSUser;->mEmail:Ljava/lang/String;

    invoke-virtual {v2, v3}, Lorg/json/JSONStringer;->value(Ljava/lang/Object;)Lorg/json/JSONStringer;

    sget-object v2, Lcom/google/android/gsf/loginservice/GLSUser;->sGLSContext:Lcom/google/android/gsf/loginservice/GLSUser$GLSContext;

    invoke-virtual {v2}, Lcom/google/android/gsf/loginservice/GLSUser$GLSContext;->getAndroidIdHex()Ljava/lang/String;

    move-result-object v0

    if-eqz v0, :cond_0

    sget-object v2, Lcom/google/android/gsf/loginservice/GLSUser$RequestKey;->ANDROID_ID:Lcom/google/android/gsf/loginservice/GLSUser$RequestKey;

    invoke-virtual {v2}, Lcom/google/android/gsf/loginservice/GLSUser$RequestKey;->getWire()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {p1, v2}, Lorg/json/JSONStringer;->key(Ljava/lang/String;)Lorg/json/JSONStringer;

    move-result-object v2

    invoke-virtual {v2, v0}, Lorg/json/JSONStringer;->value(Ljava/lang/Object;)Lorg/json/JSONStringer;

    :cond_0
    iget-object v2, p0, Lcom/google/android/gsf/loginservice/GLSUser;->captchaToken:Ljava/lang/String;

    invoke-static {v2}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v2

    if-nez v2, :cond_1

    iget-object v2, p0, Lcom/google/android/gsf/loginservice/GLSUser;->captchaAnswer:Ljava/lang/String;

    invoke-static {v2}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v2

    if-nez v2, :cond_1

    sget-object v2, Lcom/google/android/gsf/loginservice/GLSUser$RequestKey;->CAPTCHA_TOKEN:Lcom/google/android/gsf/loginservice/GLSUser$RequestKey;

    invoke-virtual {v2}, Lcom/google/android/gsf/loginservice/GLSUser$RequestKey;->getWire()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {p1, v2}, Lorg/json/JSONStringer;->key(Ljava/lang/String;)Lorg/json/JSONStringer;

    move-result-object v2

    iget-object v3, p0, Lcom/google/android/gsf/loginservice/GLSUser;->captchaToken:Ljava/lang/String;

    invoke-virtual {v2, v3}, Lorg/json/JSONStringer;->value(Ljava/lang/Object;)Lorg/json/JSONStringer;

    sget-object v2, Lcom/google/android/gsf/loginservice/GLSUser$RequestKey;->CAPTCHA_ANSWER:Lcom/google/android/gsf/loginservice/GLSUser$RequestKey;

    invoke-virtual {v2}, Lcom/google/android/gsf/loginservice/GLSUser$RequestKey;->getWire()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {p1, v2}, Lorg/json/JSONStringer;->key(Ljava/lang/String;)Lorg/json/JSONStringer;

    move-result-object v2

    iget-object v3, p0, Lcom/google/android/gsf/loginservice/GLSUser;->captchaAnswer:Ljava/lang/String;

    invoke-virtual {v2, v3}, Lorg/json/JSONStringer;->value(Ljava/lang/Object;)Lorg/json/JSONStringer;

    iput-object v4, p0, Lcom/google/android/gsf/loginservice/GLSUser;->captchaToken:Ljava/lang/String;

    iput-object v4, p0, Lcom/google/android/gsf/loginservice/GLSUser;->captchaAnswer:Ljava/lang/String;

    :cond_1
    iget-boolean v2, p0, Lcom/google/android/gsf/loginservice/GLSUser;->mUseBrowserFlow:Z

    if-eqz v2, :cond_2

    sget-object v2, Lcom/google/android/gsf/loginservice/GLSUser$RequestKey;->BROWSER_FLOW:Lcom/google/android/gsf/loginservice/GLSUser$RequestKey;

    invoke-virtual {v2}, Lcom/google/android/gsf/loginservice/GLSUser$RequestKey;->getWire()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {p1, v2}, Lorg/json/JSONStringer;->key(Ljava/lang/String;)Lorg/json/JSONStringer;

    move-result-object v2

    const-string v3, "1"

    invoke-virtual {v2, v3}, Lorg/json/JSONStringer;->value(Ljava/lang/Object;)Lorg/json/JSONStringer;

    :cond_2
    sget-object v2, Lcom/google/android/gsf/loginservice/GLSUser;->sGLSContext:Lcom/google/android/gsf/loginservice/GLSUser$GLSContext;

    iget-object v2, v2, Lcom/google/android/gsf/loginservice/GLSUser$GLSContext;->mContext:Landroid/content/Context;

    invoke-virtual {v2}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v2

    const-string v3, "device_country"

    invoke-static {v2, v3, v4}, Lcom/google/android/gsf/Gservices;->getString(Landroid/content/ContentResolver;Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    sget-object v2, Lcom/google/android/gsf/loginservice/GLSUser$RequestKey;->OPERATOR_COUNTRY:Lcom/google/android/gsf/loginservice/GLSUser$RequestKey;

    invoke-virtual {v2}, Lcom/google/android/gsf/loginservice/GLSUser$RequestKey;->getWire()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {p1, v2}, Lorg/json/JSONStringer;->key(Ljava/lang/String;)Lorg/json/JSONStringer;

    move-result-object v2

    invoke-virtual {v2, v1}, Lorg/json/JSONStringer;->value(Ljava/lang/Object;)Lorg/json/JSONStringer;

    sget-object v2, Lcom/google/android/gsf/loginservice/GLSUser$RequestKey;->DEVICE_COUNTRY:Lcom/google/android/gsf/loginservice/GLSUser$RequestKey;

    invoke-virtual {v2}, Lcom/google/android/gsf/loginservice/GLSUser$RequestKey;->getWire()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {p1, v2}, Lorg/json/JSONStringer;->key(Ljava/lang/String;)Lorg/json/JSONStringer;

    move-result-object v2

    invoke-virtual {v2, v1}, Lorg/json/JSONStringer;->value(Ljava/lang/Object;)Lorg/json/JSONStringer;

    sget-object v2, Lcom/google/android/gsf/loginservice/GLSUser$RequestKey;->LANGUAGE:Lcom/google/android/gsf/loginservice/GLSUser$RequestKey;

    invoke-virtual {v2}, Lcom/google/android/gsf/loginservice/GLSUser$RequestKey;->getWire()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {p1, v2}, Lorg/json/JSONStringer;->key(Ljava/lang/String;)Lorg/json/JSONStringer;

    move-result-object v2

    invoke-static {}, Ljava/util/Locale;->getDefault()Ljava/util/Locale;

    move-result-object v3

    invoke-virtual {v3}, Ljava/util/Locale;->getLanguage()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Lorg/json/JSONStringer;->value(Ljava/lang/Object;)Lorg/json/JSONStringer;

    return-void
.end method

.method public remove()V
    .locals 4
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Landroid/accounts/AuthenticatorException;,
            Ljava/io/IOException;
        }
    .end annotation

    :try_start_0
    sget-object v0, Lcom/google/android/gsf/loginservice/GLSUser;->sGLSContext:Lcom/google/android/gsf/loginservice/GLSUser$GLSContext;

    iget-object v0, v0, Lcom/google/android/gsf/loginservice/GLSUser$GLSContext;->mAccountManager:Landroid/accounts/AccountManager;

    iget-object v1, p0, Lcom/google/android/gsf/loginservice/GLSUser;->mAccount:Landroid/accounts/Account;

    const/4 v2, 0x0

    const/4 v3, 0x0

    invoke-virtual {v0, v1, v2, v3}, Landroid/accounts/AccountManager;->removeAccount(Landroid/accounts/Account;Landroid/accounts/AccountManagerCallback;Landroid/os/Handler;)Landroid/accounts/AccountManagerFuture;

    move-result-object v0

    invoke-interface {v0}, Landroid/accounts/AccountManagerFuture;->getResult()Ljava/lang/Object;
    :try_end_0
    .catch Landroid/accounts/OperationCanceledException; {:try_start_0 .. :try_end_0} :catch_0

    return-void

    :catch_0
    move-exception v0

    new-instance v0, Ljava/io/IOException;

    const-string v1, "Timeout"

    invoke-direct {v0, v1}, Ljava/io/IOException;-><init>(Ljava/lang/String;)V

    throw v0
.end method

.method public declared-synchronized removeSession(Ljava/lang/String;)V
    .locals 1
    .param p1    # Ljava/lang/String;

    monitor-enter p0

    :try_start_0
    sget-object v0, Lcom/google/android/gsf/loginservice/GLSUser;->sGLSContext:Lcom/google/android/gsf/loginservice/GLSUser$GLSContext;

    iget-object v0, v0, Lcom/google/android/gsf/loginservice/GLSUser$GLSContext;->mSessions:Ljava/util/LinkedHashMap;

    invoke-virtual {v0, p1}, Ljava/util/LinkedHashMap;->remove(Ljava/lang/Object;)Ljava/lang/Object;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    monitor-exit p0

    return-void

    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method resetPassword()V
    .locals 4

    const/4 v3, 0x0

    iget-boolean v0, p0, Lcom/google/android/gsf/loginservice/GLSUser;->existing:Z

    if-eqz v0, :cond_1

    iget-object v0, p0, Lcom/google/android/gsf/loginservice/GLSUser;->mMasterToken:Ljava/lang/String;

    invoke-static {v0}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v0

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/google/android/gsf/loginservice/GLSUser;->mEncryptedPassword:Ljava/lang/String;

    invoke-static {v0}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v0

    if-nez v0, :cond_1

    :cond_0
    iput-object v3, p0, Lcom/google/android/gsf/loginservice/GLSUser;->mEncryptedPassword:Ljava/lang/String;

    iput-object v3, p0, Lcom/google/android/gsf/loginservice/GLSUser;->mMasterToken:Ljava/lang/String;

    sget-object v0, Lcom/google/android/gsf/loginservice/GLSUser;->sGLSContext:Lcom/google/android/gsf/loginservice/GLSUser$GLSContext;

    iget-object v0, v0, Lcom/google/android/gsf/loginservice/GLSUser$GLSContext;->mAccountManager:Landroid/accounts/AccountManager;

    iget-object v1, p0, Lcom/google/android/gsf/loginservice/GLSUser;->mAccount:Landroid/accounts/Account;

    const-string v2, ""

    invoke-virtual {v0, v1, v2}, Landroid/accounts/AccountManager;->setPassword(Landroid/accounts/Account;Ljava/lang/String;)V

    sget-object v0, Lcom/google/android/gsf/loginservice/GLSUser;->sGLSContext:Lcom/google/android/gsf/loginservice/GLSUser$GLSContext;

    iget-object v0, v0, Lcom/google/android/gsf/loginservice/GLSUser$GLSContext;->mAccountManager:Landroid/accounts/AccountManager;

    iget-object v1, p0, Lcom/google/android/gsf/loginservice/GLSUser;->mAccount:Landroid/accounts/Account;

    const-string v2, "oauthAccessToken"

    invoke-virtual {v0, v1, v2, v3}, Landroid/accounts/AccountManager;->setUserData(Landroid/accounts/Account;Ljava/lang/String;Ljava/lang/String;)V

    sget-object v0, Lcom/google/android/gsf/loginservice/GLSUser;->sGLSContext:Lcom/google/android/gsf/loginservice/GLSUser$GLSContext;

    iget-object v0, v0, Lcom/google/android/gsf/loginservice/GLSUser$GLSContext;->mAccountManager:Landroid/accounts/AccountManager;

    iget-object v1, p0, Lcom/google/android/gsf/loginservice/GLSUser;->mAccount:Landroid/accounts/Account;

    const-string v2, "sha1hash"

    const-string v3, ""

    invoke-virtual {v0, v1, v2, v3}, Landroid/accounts/AccountManager;->setUserData(Landroid/accounts/Account;Ljava/lang/String;Ljava/lang/String;)V

    :cond_1
    return-void
.end method

.method public setBackupAccount()Z
    .locals 4

    new-instance v1, Landroid/content/Intent;

    const-string v2, "com.google.android.backup.SetBackupAccount"

    invoke-direct {v1, v2}, Landroid/content/Intent;-><init>(Ljava/lang/String;)V

    const-string v2, "backupAccount"

    iget-object v3, p0, Lcom/google/android/gsf/loginservice/GLSUser;->mAccount:Landroid/accounts/Account;

    invoke-virtual {v1, v2, v3}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Landroid/os/Parcelable;)Landroid/content/Intent;

    const-string v2, "backupUserHandle"

    invoke-static {}, Landroid/os/Process;->myUserHandle()Landroid/os/UserHandle;

    move-result-object v3

    invoke-virtual {v1, v2, v3}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Landroid/os/Parcelable;)Landroid/content/Intent;

    :try_start_0
    sget-object v2, Lcom/google/android/gsf/loginservice/GLSUser;->sGLSContext:Lcom/google/android/gsf/loginservice/GLSUser$GLSContext;

    iget-object v2, v2, Lcom/google/android/gsf/loginservice/GLSUser$GLSContext;->mContext:Landroid/content/Context;

    invoke-virtual {v2, v1}, Landroid/content/Context;->startService(Landroid/content/Intent;)Landroid/content/ComponentName;
    :try_end_0
    .catch Landroid/content/ActivityNotFoundException; {:try_start_0 .. :try_end_0} :catch_0

    const/4 v2, 0x1

    :goto_0
    return v2

    :catch_0
    move-exception v0

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "Could not enable backup "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-direct {p0, v2}, Lcom/google/android/gsf/loginservice/GLSUser;->log(Ljava/lang/String;)V

    const/4 v2, 0x0

    goto :goto_0
.end method

.method public setBrowserFlow(Z)V
    .locals 4
    .param p1    # Z

    iput-boolean p1, p0, Lcom/google/android/gsf/loginservice/GLSUser;->mUseBrowserFlow:Z

    sget-object v0, Lcom/google/android/gsf/loginservice/GLSUser;->sGLSContext:Lcom/google/android/gsf/loginservice/GLSUser$GLSContext;

    iget-object v1, v0, Lcom/google/android/gsf/loginservice/GLSUser$GLSContext;->mAccountManager:Landroid/accounts/AccountManager;

    iget-object v2, p0, Lcom/google/android/gsf/loginservice/GLSUser;->mAccount:Landroid/accounts/Account;

    const-string v3, "oauthAccessToken"

    if-eqz p1, :cond_0

    const-string v0, "1"

    :goto_0
    invoke-virtual {v1, v2, v3, v0}, Landroid/accounts/AccountManager;->setUserData(Landroid/accounts/Account;Ljava/lang/String;Ljava/lang/String;)V

    return-void

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public setCaptcha(Ljava/lang/String;Ljava/lang/String;)V
    .locals 0
    .param p1    # Ljava/lang/String;
    .param p2    # Ljava/lang/String;

    iput-object p1, p0, Lcom/google/android/gsf/loginservice/GLSUser;->captchaToken:Ljava/lang/String;

    iput-object p2, p0, Lcom/google/android/gsf/loginservice/GLSUser;->captchaAnswer:Ljava/lang/String;

    return-void
.end method

.method public setPassword(Ljava/lang/String;)V
    .locals 2
    .param p1    # Ljava/lang/String;

    sget-object v0, Lcom/google/android/gsf/loginservice/GLSUser;->sGLSContext:Lcom/google/android/gsf/loginservice/GLSUser$GLSContext;

    iget-object v0, v0, Lcom/google/android/gsf/loginservice/GLSUser$GLSContext;->mContext:Landroid/content/Context;

    invoke-virtual {v0}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v0

    iget-object v1, p0, Lcom/google/android/gsf/loginservice/GLSUser;->mEmail:Ljava/lang/String;

    invoke-static {v0, v1, p1}, Lcom/google/android/gsf/loginservice/PasswordEncrypter;->encryptPassword(Landroid/content/ContentResolver;Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/gsf/loginservice/GLSUser;->mEncryptedPassword:Ljava/lang/String;

    iget-object v0, p0, Lcom/google/android/gsf/loginservice/GLSUser;->mEmail:Ljava/lang/String;

    invoke-static {v0, p1}, Lcom/google/android/gsf/loginservice/PasswordEncrypter;->hashPassword(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/gsf/loginservice/GLSUser;->mHash:Ljava/lang/String;

    iget-object v0, p0, Lcom/google/android/gsf/loginservice/GLSUser;->mEncryptedPassword:Ljava/lang/String;

    iput-object v0, p0, Lcom/google/android/gsf/loginservice/GLSUser;->mUpdatedPassword:Ljava/lang/String;

    const/4 v0, 0x0

    iput-object v0, p0, Lcom/google/android/gsf/loginservice/GLSUser;->mMasterToken:Ljava/lang/String;

    return-void
.end method

.method public setStoredPermission(Ljava/lang/String;ILjava/lang/String;Landroid/os/Bundle;)V
    .locals 3
    .param p1    # Ljava/lang/String;
    .param p2    # I
    .param p3    # Ljava/lang/String;
    .param p4    # Landroid/os/Bundle;

    sget-object v0, Lcom/google/android/gsf/loginservice/GLSUser;->sGLSContext:Lcom/google/android/gsf/loginservice/GLSUser$GLSContext;

    iget-object v0, v0, Lcom/google/android/gsf/loginservice/GLSUser$GLSContext;->mAccountManager:Landroid/accounts/AccountManager;

    iget-object v1, p0, Lcom/google/android/gsf/loginservice/GLSUser;->mAccount:Landroid/accounts/Account;

    invoke-direct {p0, p2, p1}, Lcom/google/android/gsf/loginservice/GLSUser;->getPermissionKey(ILjava/lang/String;)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0, v1, v2, p3}, Landroid/accounts/AccountManager;->setUserData(Landroid/accounts/Account;Ljava/lang/String;Ljava/lang/String;)V

    return-void
.end method

.method public setToken(Ljava/lang/String;)V
    .locals 1
    .param p1    # Ljava/lang/String;

    iput-object p1, p0, Lcom/google/android/gsf/loginservice/GLSUser;->mMasterToken:Ljava/lang/String;

    const/4 v0, 0x0

    iput-object v0, p0, Lcom/google/android/gsf/loginservice/GLSUser;->mEncryptedPassword:Ljava/lang/String;

    iput-object p1, p0, Lcom/google/android/gsf/loginservice/GLSUser;->mUpdatedPassword:Ljava/lang/String;

    return-void
.end method

.method public updateSecret()V
    .locals 4

    iget-object v0, p0, Lcom/google/android/gsf/loginservice/GLSUser;->mMasterToken:Ljava/lang/String;

    if-eqz v0, :cond_1

    sget-object v0, Lcom/google/android/gsf/loginservice/GLSUser;->sGLSContext:Lcom/google/android/gsf/loginservice/GLSUser$GLSContext;

    iget-object v0, v0, Lcom/google/android/gsf/loginservice/GLSUser$GLSContext;->mAccountManager:Landroid/accounts/AccountManager;

    iget-object v1, p0, Lcom/google/android/gsf/loginservice/GLSUser;->mAccount:Landroid/accounts/Account;

    iget-object v2, p0, Lcom/google/android/gsf/loginservice/GLSUser;->mMasterToken:Ljava/lang/String;

    invoke-virtual {v0, v1, v2}, Landroid/accounts/AccountManager;->setPassword(Landroid/accounts/Account;Ljava/lang/String;)V

    iget-boolean v0, p0, Lcom/google/android/gsf/loginservice/GLSUser;->mUseBrowserFlow:Z

    if-eqz v0, :cond_0

    sget v0, Landroid/os/Build$VERSION;->SDK_INT:I

    const/16 v1, 0x8

    if-gt v0, v1, :cond_0

    sget-object v0, Lcom/google/android/gsf/loginservice/GLSUser;->sGLSContext:Lcom/google/android/gsf/loginservice/GLSUser$GLSContext;

    iget-object v0, v0, Lcom/google/android/gsf/loginservice/GLSUser$GLSContext;->mAccountManager:Landroid/accounts/AccountManager;

    iget-object v1, p0, Lcom/google/android/gsf/loginservice/GLSUser;->mAccount:Landroid/accounts/Account;

    const-string v2, "oauthAccessToken"

    iget-object v3, p0, Lcom/google/android/gsf/loginservice/GLSUser;->mMasterToken:Ljava/lang/String;

    invoke-virtual {v0, v1, v2, v3}, Landroid/accounts/AccountManager;->setUserData(Landroid/accounts/Account;Ljava/lang/String;Ljava/lang/String;)V

    :cond_0
    :goto_0
    return-void

    :cond_1
    sget-object v0, Lcom/google/android/gsf/loginservice/GLSUser;->sGLSContext:Lcom/google/android/gsf/loginservice/GLSUser$GLSContext;

    iget-object v0, v0, Lcom/google/android/gsf/loginservice/GLSUser$GLSContext;->mAccountManager:Landroid/accounts/AccountManager;

    iget-object v1, p0, Lcom/google/android/gsf/loginservice/GLSUser;->mAccount:Landroid/accounts/Account;

    iget-object v2, p0, Lcom/google/android/gsf/loginservice/GLSUser;->mEncryptedPassword:Ljava/lang/String;

    invoke-virtual {v0, v1, v2}, Landroid/accounts/AccountManager;->setPassword(Landroid/accounts/Account;Ljava/lang/String;)V

    iget-object v0, p0, Lcom/google/android/gsf/loginservice/GLSUser;->mHash:Ljava/lang/String;

    if-eqz v0, :cond_0

    sget-object v0, Lcom/google/android/gsf/loginservice/GLSUser;->sGLSContext:Lcom/google/android/gsf/loginservice/GLSUser$GLSContext;

    iget-object v0, v0, Lcom/google/android/gsf/loginservice/GLSUser$GLSContext;->mAccountManager:Landroid/accounts/AccountManager;

    iget-object v1, p0, Lcom/google/android/gsf/loginservice/GLSUser;->mAccount:Landroid/accounts/Account;

    const-string v2, "sha1hash"

    iget-object v3, p0, Lcom/google/android/gsf/loginservice/GLSUser;->mHash:Ljava/lang/String;

    invoke-virtual {v0, v1, v2, v3}, Landroid/accounts/AccountManager;->setUserData(Landroid/accounts/Account;Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_0
.end method

.method public updateWithPassword(Lcom/google/android/gsf/loginservice/GLSUser$GLSSession;)Landroid/content/Intent;
    .locals 9
    .param p1    # Lcom/google/android/gsf/loginservice/GLSUser$GLSSession;

    const/4 v5, 0x1

    const/4 v2, 0x0

    const/4 v4, 0x0

    iput-object v4, p0, Lcom/google/android/gsf/loginservice/GLSUser;->mMasterToken:Ljava/lang/String;

    const-string v1, "ac2dm"

    move-object v0, p0

    move-object v3, p1

    move-object v6, v4

    move v7, v2

    invoke-direct/range {v0 .. v7}, Lcom/google/android/gsf/loginservice/GLSUser;->getAuthtokenRaw(Ljava/lang/String;ILcom/google/android/gsf/loginservice/GLSUser$GLSSession;Landroid/os/Bundle;ZLjava/lang/String;Z)Ljava/util/Map;

    move-result-object v1

    const-string v3, "ac2dm"

    move-object v0, p0

    move-object v6, p1

    invoke-direct/range {v0 .. v6}, Lcom/google/android/gsf/loginservice/GLSUser;->processResponse(Ljava/util/Map;ILjava/lang/String;Landroid/os/Bundle;ZLcom/google/android/gsf/loginservice/GLSUser$GLSSession;)Landroid/content/Intent;

    move-result-object v8

    invoke-virtual {p0, v8}, Lcom/google/android/gsf/loginservice/GLSUser;->getToken(Landroid/content/Intent;)Ljava/lang/String;

    move-result-object v0

    if-eqz v0, :cond_0

    invoke-virtual {p0}, Lcom/google/android/gsf/loginservice/GLSUser;->updateSecret()V

    :cond_0
    return-object v8
.end method

.method public updateWithRequestToken(Lcom/google/android/gsf/loginservice/GLSUser$GLSSession;Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;
    .locals 9
    .param p1    # Lcom/google/android/gsf/loginservice/GLSUser$GLSSession;
    .param p2    # Ljava/lang/String;
    .param p3    # Ljava/lang/String;

    const/4 v5, 0x1

    const/4 v2, 0x0

    const/4 v4, 0x0

    iput-object v4, p0, Lcom/google/android/gsf/loginservice/GLSUser;->mMasterToken:Ljava/lang/String;

    iput-object v4, p0, Lcom/google/android/gsf/loginservice/GLSUser;->mEncryptedPassword:Ljava/lang/String;

    const-string v1, "ac2dm"

    if-nez p3, :cond_1

    move v7, v5

    :goto_0
    move-object v0, p0

    move-object v3, p1

    move-object v6, p2

    invoke-direct/range {v0 .. v7}, Lcom/google/android/gsf/loginservice/GLSUser;->getAuthtokenRaw(Ljava/lang/String;ILcom/google/android/gsf/loginservice/GLSUser$GLSSession;Landroid/os/Bundle;ZLjava/lang/String;Z)Ljava/util/Map;

    move-result-object v1

    iput-object v4, p1, Lcom/google/android/gsf/loginservice/GLSUser$GLSSession;->mAccessToken:Ljava/lang/String;

    const-string v3, "ac2dm"

    move-object v0, p0

    move-object v6, p1

    invoke-direct/range {v0 .. v6}, Lcom/google/android/gsf/loginservice/GLSUser;->processResponse(Ljava/util/Map;ILjava/lang/String;Landroid/os/Bundle;ZLcom/google/android/gsf/loginservice/GLSUser$GLSSession;)Landroid/content/Intent;

    move-result-object v8

    invoke-virtual {p0, v8}, Lcom/google/android/gsf/loginservice/GLSUser;->getToken(Landroid/content/Intent;)Ljava/lang/String;

    move-result-object v0

    if-eqz v0, :cond_0

    invoke-virtual {p0}, Lcom/google/android/gsf/loginservice/GLSUser;->updateSecret()V

    :cond_0
    return-object v8

    :cond_1
    move v7, v2

    goto :goto_0
.end method
