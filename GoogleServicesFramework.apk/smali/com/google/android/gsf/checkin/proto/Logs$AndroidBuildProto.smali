.class public final Lcom/google/android/gsf/checkin/proto/Logs$AndroidBuildProto;
.super Lcom/google/protobuf/micro/MessageMicro;
.source "Logs.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/google/android/gsf/checkin/proto/Logs;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x19
    name = "AndroidBuildProto"
.end annotation


# instance fields
.field private bootloader_:Ljava/lang/String;

.field private buildProduct_:Ljava/lang/String;

.field private cachedSize:I

.field private carrier_:Ljava/lang/String;

.field private client_:Ljava/lang/String;

.field private device_:Ljava/lang/String;

.field private googleServices_:I

.field private hasBootloader:Z

.field private hasBuildProduct:Z

.field private hasCarrier:Z

.field private hasClient:Z

.field private hasDevice:Z

.field private hasGoogleServices:Z

.field private hasId:Z

.field private hasManufacturer:Z

.field private hasModel:Z

.field private hasOtaInstalled:Z

.field private hasProduct:Z

.field private hasRadio:Z

.field private hasSdkVersion:Z

.field private hasTimestamp:Z

.field private id_:Ljava/lang/String;

.field private manufacturer_:Ljava/lang/String;

.field private model_:Ljava/lang/String;

.field private otaInstalled_:Z

.field private product_:Ljava/lang/String;

.field private radio_:Ljava/lang/String;

.field private sdkVersion_:I

.field private timestamp_:J


# direct methods
.method public constructor <init>()V
    .locals 3

    const/4 v2, 0x0

    invoke-direct {p0}, Lcom/google/protobuf/micro/MessageMicro;-><init>()V

    const-string v0, ""

    iput-object v0, p0, Lcom/google/android/gsf/checkin/proto/Logs$AndroidBuildProto;->id_:Ljava/lang/String;

    const-string v0, ""

    iput-object v0, p0, Lcom/google/android/gsf/checkin/proto/Logs$AndroidBuildProto;->radio_:Ljava/lang/String;

    const-string v0, ""

    iput-object v0, p0, Lcom/google/android/gsf/checkin/proto/Logs$AndroidBuildProto;->bootloader_:Ljava/lang/String;

    const-string v0, ""

    iput-object v0, p0, Lcom/google/android/gsf/checkin/proto/Logs$AndroidBuildProto;->product_:Ljava/lang/String;

    const-string v0, ""

    iput-object v0, p0, Lcom/google/android/gsf/checkin/proto/Logs$AndroidBuildProto;->carrier_:Ljava/lang/String;

    const-string v0, ""

    iput-object v0, p0, Lcom/google/android/gsf/checkin/proto/Logs$AndroidBuildProto;->client_:Ljava/lang/String;

    const-wide/16 v0, 0x0

    iput-wide v0, p0, Lcom/google/android/gsf/checkin/proto/Logs$AndroidBuildProto;->timestamp_:J

    iput v2, p0, Lcom/google/android/gsf/checkin/proto/Logs$AndroidBuildProto;->googleServices_:I

    iput v2, p0, Lcom/google/android/gsf/checkin/proto/Logs$AndroidBuildProto;->sdkVersion_:I

    const-string v0, ""

    iput-object v0, p0, Lcom/google/android/gsf/checkin/proto/Logs$AndroidBuildProto;->device_:Ljava/lang/String;

    const-string v0, ""

    iput-object v0, p0, Lcom/google/android/gsf/checkin/proto/Logs$AndroidBuildProto;->model_:Ljava/lang/String;

    const-string v0, ""

    iput-object v0, p0, Lcom/google/android/gsf/checkin/proto/Logs$AndroidBuildProto;->manufacturer_:Ljava/lang/String;

    const-string v0, ""

    iput-object v0, p0, Lcom/google/android/gsf/checkin/proto/Logs$AndroidBuildProto;->buildProduct_:Ljava/lang/String;

    iput-boolean v2, p0, Lcom/google/android/gsf/checkin/proto/Logs$AndroidBuildProto;->otaInstalled_:Z

    const/4 v0, -0x1

    iput v0, p0, Lcom/google/android/gsf/checkin/proto/Logs$AndroidBuildProto;->cachedSize:I

    return-void
.end method


# virtual methods
.method public getBootloader()Ljava/lang/String;
    .locals 1

    iget-object v0, p0, Lcom/google/android/gsf/checkin/proto/Logs$AndroidBuildProto;->bootloader_:Ljava/lang/String;

    return-object v0
.end method

.method public getBuildProduct()Ljava/lang/String;
    .locals 1

    iget-object v0, p0, Lcom/google/android/gsf/checkin/proto/Logs$AndroidBuildProto;->buildProduct_:Ljava/lang/String;

    return-object v0
.end method

.method public getCachedSize()I
    .locals 1

    iget v0, p0, Lcom/google/android/gsf/checkin/proto/Logs$AndroidBuildProto;->cachedSize:I

    if-gez v0, :cond_0

    invoke-virtual {p0}, Lcom/google/android/gsf/checkin/proto/Logs$AndroidBuildProto;->getSerializedSize()I

    :cond_0
    iget v0, p0, Lcom/google/android/gsf/checkin/proto/Logs$AndroidBuildProto;->cachedSize:I

    return v0
.end method

.method public getCarrier()Ljava/lang/String;
    .locals 1

    iget-object v0, p0, Lcom/google/android/gsf/checkin/proto/Logs$AndroidBuildProto;->carrier_:Ljava/lang/String;

    return-object v0
.end method

.method public getClient()Ljava/lang/String;
    .locals 1

    iget-object v0, p0, Lcom/google/android/gsf/checkin/proto/Logs$AndroidBuildProto;->client_:Ljava/lang/String;

    return-object v0
.end method

.method public getDevice()Ljava/lang/String;
    .locals 1

    iget-object v0, p0, Lcom/google/android/gsf/checkin/proto/Logs$AndroidBuildProto;->device_:Ljava/lang/String;

    return-object v0
.end method

.method public getGoogleServices()I
    .locals 1

    iget v0, p0, Lcom/google/android/gsf/checkin/proto/Logs$AndroidBuildProto;->googleServices_:I

    return v0
.end method

.method public getId()Ljava/lang/String;
    .locals 1

    iget-object v0, p0, Lcom/google/android/gsf/checkin/proto/Logs$AndroidBuildProto;->id_:Ljava/lang/String;

    return-object v0
.end method

.method public getManufacturer()Ljava/lang/String;
    .locals 1

    iget-object v0, p0, Lcom/google/android/gsf/checkin/proto/Logs$AndroidBuildProto;->manufacturer_:Ljava/lang/String;

    return-object v0
.end method

.method public getModel()Ljava/lang/String;
    .locals 1

    iget-object v0, p0, Lcom/google/android/gsf/checkin/proto/Logs$AndroidBuildProto;->model_:Ljava/lang/String;

    return-object v0
.end method

.method public getOtaInstalled()Z
    .locals 1

    iget-boolean v0, p0, Lcom/google/android/gsf/checkin/proto/Logs$AndroidBuildProto;->otaInstalled_:Z

    return v0
.end method

.method public getProduct()Ljava/lang/String;
    .locals 1

    iget-object v0, p0, Lcom/google/android/gsf/checkin/proto/Logs$AndroidBuildProto;->product_:Ljava/lang/String;

    return-object v0
.end method

.method public getRadio()Ljava/lang/String;
    .locals 1

    iget-object v0, p0, Lcom/google/android/gsf/checkin/proto/Logs$AndroidBuildProto;->radio_:Ljava/lang/String;

    return-object v0
.end method

.method public getSdkVersion()I
    .locals 1

    iget v0, p0, Lcom/google/android/gsf/checkin/proto/Logs$AndroidBuildProto;->sdkVersion_:I

    return v0
.end method

.method public getSerializedSize()I
    .locals 4

    const/4 v0, 0x0

    invoke-virtual {p0}, Lcom/google/android/gsf/checkin/proto/Logs$AndroidBuildProto;->hasId()Z

    move-result v1

    if-eqz v1, :cond_0

    const/4 v1, 0x1

    invoke-virtual {p0}, Lcom/google/android/gsf/checkin/proto/Logs$AndroidBuildProto;->getId()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2}, Lcom/google/protobuf/micro/CodedOutputStreamMicro;->computeStringSize(ILjava/lang/String;)I

    move-result v1

    add-int/2addr v0, v1

    :cond_0
    invoke-virtual {p0}, Lcom/google/android/gsf/checkin/proto/Logs$AndroidBuildProto;->hasProduct()Z

    move-result v1

    if-eqz v1, :cond_1

    const/4 v1, 0x2

    invoke-virtual {p0}, Lcom/google/android/gsf/checkin/proto/Logs$AndroidBuildProto;->getProduct()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2}, Lcom/google/protobuf/micro/CodedOutputStreamMicro;->computeStringSize(ILjava/lang/String;)I

    move-result v1

    add-int/2addr v0, v1

    :cond_1
    invoke-virtual {p0}, Lcom/google/android/gsf/checkin/proto/Logs$AndroidBuildProto;->hasCarrier()Z

    move-result v1

    if-eqz v1, :cond_2

    const/4 v1, 0x3

    invoke-virtual {p0}, Lcom/google/android/gsf/checkin/proto/Logs$AndroidBuildProto;->getCarrier()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2}, Lcom/google/protobuf/micro/CodedOutputStreamMicro;->computeStringSize(ILjava/lang/String;)I

    move-result v1

    add-int/2addr v0, v1

    :cond_2
    invoke-virtual {p0}, Lcom/google/android/gsf/checkin/proto/Logs$AndroidBuildProto;->hasRadio()Z

    move-result v1

    if-eqz v1, :cond_3

    const/4 v1, 0x4

    invoke-virtual {p0}, Lcom/google/android/gsf/checkin/proto/Logs$AndroidBuildProto;->getRadio()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2}, Lcom/google/protobuf/micro/CodedOutputStreamMicro;->computeStringSize(ILjava/lang/String;)I

    move-result v1

    add-int/2addr v0, v1

    :cond_3
    invoke-virtual {p0}, Lcom/google/android/gsf/checkin/proto/Logs$AndroidBuildProto;->hasBootloader()Z

    move-result v1

    if-eqz v1, :cond_4

    const/4 v1, 0x5

    invoke-virtual {p0}, Lcom/google/android/gsf/checkin/proto/Logs$AndroidBuildProto;->getBootloader()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2}, Lcom/google/protobuf/micro/CodedOutputStreamMicro;->computeStringSize(ILjava/lang/String;)I

    move-result v1

    add-int/2addr v0, v1

    :cond_4
    invoke-virtual {p0}, Lcom/google/android/gsf/checkin/proto/Logs$AndroidBuildProto;->hasClient()Z

    move-result v1

    if-eqz v1, :cond_5

    const/4 v1, 0x6

    invoke-virtual {p0}, Lcom/google/android/gsf/checkin/proto/Logs$AndroidBuildProto;->getClient()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2}, Lcom/google/protobuf/micro/CodedOutputStreamMicro;->computeStringSize(ILjava/lang/String;)I

    move-result v1

    add-int/2addr v0, v1

    :cond_5
    invoke-virtual {p0}, Lcom/google/android/gsf/checkin/proto/Logs$AndroidBuildProto;->hasTimestamp()Z

    move-result v1

    if-eqz v1, :cond_6

    const/4 v1, 0x7

    invoke-virtual {p0}, Lcom/google/android/gsf/checkin/proto/Logs$AndroidBuildProto;->getTimestamp()J

    move-result-wide v2

    invoke-static {v1, v2, v3}, Lcom/google/protobuf/micro/CodedOutputStreamMicro;->computeInt64Size(IJ)I

    move-result v1

    add-int/2addr v0, v1

    :cond_6
    invoke-virtual {p0}, Lcom/google/android/gsf/checkin/proto/Logs$AndroidBuildProto;->hasGoogleServices()Z

    move-result v1

    if-eqz v1, :cond_7

    const/16 v1, 0x8

    invoke-virtual {p0}, Lcom/google/android/gsf/checkin/proto/Logs$AndroidBuildProto;->getGoogleServices()I

    move-result v2

    invoke-static {v1, v2}, Lcom/google/protobuf/micro/CodedOutputStreamMicro;->computeInt32Size(II)I

    move-result v1

    add-int/2addr v0, v1

    :cond_7
    invoke-virtual {p0}, Lcom/google/android/gsf/checkin/proto/Logs$AndroidBuildProto;->hasDevice()Z

    move-result v1

    if-eqz v1, :cond_8

    const/16 v1, 0x9

    invoke-virtual {p0}, Lcom/google/android/gsf/checkin/proto/Logs$AndroidBuildProto;->getDevice()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2}, Lcom/google/protobuf/micro/CodedOutputStreamMicro;->computeStringSize(ILjava/lang/String;)I

    move-result v1

    add-int/2addr v0, v1

    :cond_8
    invoke-virtual {p0}, Lcom/google/android/gsf/checkin/proto/Logs$AndroidBuildProto;->hasSdkVersion()Z

    move-result v1

    if-eqz v1, :cond_9

    const/16 v1, 0xa

    invoke-virtual {p0}, Lcom/google/android/gsf/checkin/proto/Logs$AndroidBuildProto;->getSdkVersion()I

    move-result v2

    invoke-static {v1, v2}, Lcom/google/protobuf/micro/CodedOutputStreamMicro;->computeInt32Size(II)I

    move-result v1

    add-int/2addr v0, v1

    :cond_9
    invoke-virtual {p0}, Lcom/google/android/gsf/checkin/proto/Logs$AndroidBuildProto;->hasModel()Z

    move-result v1

    if-eqz v1, :cond_a

    const/16 v1, 0xb

    invoke-virtual {p0}, Lcom/google/android/gsf/checkin/proto/Logs$AndroidBuildProto;->getModel()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2}, Lcom/google/protobuf/micro/CodedOutputStreamMicro;->computeStringSize(ILjava/lang/String;)I

    move-result v1

    add-int/2addr v0, v1

    :cond_a
    invoke-virtual {p0}, Lcom/google/android/gsf/checkin/proto/Logs$AndroidBuildProto;->hasManufacturer()Z

    move-result v1

    if-eqz v1, :cond_b

    const/16 v1, 0xc

    invoke-virtual {p0}, Lcom/google/android/gsf/checkin/proto/Logs$AndroidBuildProto;->getManufacturer()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2}, Lcom/google/protobuf/micro/CodedOutputStreamMicro;->computeStringSize(ILjava/lang/String;)I

    move-result v1

    add-int/2addr v0, v1

    :cond_b
    invoke-virtual {p0}, Lcom/google/android/gsf/checkin/proto/Logs$AndroidBuildProto;->hasBuildProduct()Z

    move-result v1

    if-eqz v1, :cond_c

    const/16 v1, 0xd

    invoke-virtual {p0}, Lcom/google/android/gsf/checkin/proto/Logs$AndroidBuildProto;->getBuildProduct()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2}, Lcom/google/protobuf/micro/CodedOutputStreamMicro;->computeStringSize(ILjava/lang/String;)I

    move-result v1

    add-int/2addr v0, v1

    :cond_c
    invoke-virtual {p0}, Lcom/google/android/gsf/checkin/proto/Logs$AndroidBuildProto;->hasOtaInstalled()Z

    move-result v1

    if-eqz v1, :cond_d

    const/16 v1, 0xe

    invoke-virtual {p0}, Lcom/google/android/gsf/checkin/proto/Logs$AndroidBuildProto;->getOtaInstalled()Z

    move-result v2

    invoke-static {v1, v2}, Lcom/google/protobuf/micro/CodedOutputStreamMicro;->computeBoolSize(IZ)I

    move-result v1

    add-int/2addr v0, v1

    :cond_d
    iput v0, p0, Lcom/google/android/gsf/checkin/proto/Logs$AndroidBuildProto;->cachedSize:I

    return v0
.end method

.method public getTimestamp()J
    .locals 2

    iget-wide v0, p0, Lcom/google/android/gsf/checkin/proto/Logs$AndroidBuildProto;->timestamp_:J

    return-wide v0
.end method

.method public hasBootloader()Z
    .locals 1

    iget-boolean v0, p0, Lcom/google/android/gsf/checkin/proto/Logs$AndroidBuildProto;->hasBootloader:Z

    return v0
.end method

.method public hasBuildProduct()Z
    .locals 1

    iget-boolean v0, p0, Lcom/google/android/gsf/checkin/proto/Logs$AndroidBuildProto;->hasBuildProduct:Z

    return v0
.end method

.method public hasCarrier()Z
    .locals 1

    iget-boolean v0, p0, Lcom/google/android/gsf/checkin/proto/Logs$AndroidBuildProto;->hasCarrier:Z

    return v0
.end method

.method public hasClient()Z
    .locals 1

    iget-boolean v0, p0, Lcom/google/android/gsf/checkin/proto/Logs$AndroidBuildProto;->hasClient:Z

    return v0
.end method

.method public hasDevice()Z
    .locals 1

    iget-boolean v0, p0, Lcom/google/android/gsf/checkin/proto/Logs$AndroidBuildProto;->hasDevice:Z

    return v0
.end method

.method public hasGoogleServices()Z
    .locals 1

    iget-boolean v0, p0, Lcom/google/android/gsf/checkin/proto/Logs$AndroidBuildProto;->hasGoogleServices:Z

    return v0
.end method

.method public hasId()Z
    .locals 1

    iget-boolean v0, p0, Lcom/google/android/gsf/checkin/proto/Logs$AndroidBuildProto;->hasId:Z

    return v0
.end method

.method public hasManufacturer()Z
    .locals 1

    iget-boolean v0, p0, Lcom/google/android/gsf/checkin/proto/Logs$AndroidBuildProto;->hasManufacturer:Z

    return v0
.end method

.method public hasModel()Z
    .locals 1

    iget-boolean v0, p0, Lcom/google/android/gsf/checkin/proto/Logs$AndroidBuildProto;->hasModel:Z

    return v0
.end method

.method public hasOtaInstalled()Z
    .locals 1

    iget-boolean v0, p0, Lcom/google/android/gsf/checkin/proto/Logs$AndroidBuildProto;->hasOtaInstalled:Z

    return v0
.end method

.method public hasProduct()Z
    .locals 1

    iget-boolean v0, p0, Lcom/google/android/gsf/checkin/proto/Logs$AndroidBuildProto;->hasProduct:Z

    return v0
.end method

.method public hasRadio()Z
    .locals 1

    iget-boolean v0, p0, Lcom/google/android/gsf/checkin/proto/Logs$AndroidBuildProto;->hasRadio:Z

    return v0
.end method

.method public hasSdkVersion()Z
    .locals 1

    iget-boolean v0, p0, Lcom/google/android/gsf/checkin/proto/Logs$AndroidBuildProto;->hasSdkVersion:Z

    return v0
.end method

.method public hasTimestamp()Z
    .locals 1

    iget-boolean v0, p0, Lcom/google/android/gsf/checkin/proto/Logs$AndroidBuildProto;->hasTimestamp:Z

    return v0
.end method

.method public mergeFrom(Lcom/google/protobuf/micro/CodedInputStreamMicro;)Lcom/google/android/gsf/checkin/proto/Logs$AndroidBuildProto;
    .locals 3
    .param p1    # Lcom/google/protobuf/micro/CodedInputStreamMicro;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    :cond_0
    :goto_0
    invoke-virtual {p1}, Lcom/google/protobuf/micro/CodedInputStreamMicro;->readTag()I

    move-result v0

    sparse-switch v0, :sswitch_data_0

    invoke-virtual {p0, p1, v0}, Lcom/google/android/gsf/checkin/proto/Logs$AndroidBuildProto;->parseUnknownField(Lcom/google/protobuf/micro/CodedInputStreamMicro;I)Z

    move-result v1

    if-nez v1, :cond_0

    :sswitch_0
    return-object p0

    :sswitch_1
    invoke-virtual {p1}, Lcom/google/protobuf/micro/CodedInputStreamMicro;->readString()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {p0, v1}, Lcom/google/android/gsf/checkin/proto/Logs$AndroidBuildProto;->setId(Ljava/lang/String;)Lcom/google/android/gsf/checkin/proto/Logs$AndroidBuildProto;

    goto :goto_0

    :sswitch_2
    invoke-virtual {p1}, Lcom/google/protobuf/micro/CodedInputStreamMicro;->readString()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {p0, v1}, Lcom/google/android/gsf/checkin/proto/Logs$AndroidBuildProto;->setProduct(Ljava/lang/String;)Lcom/google/android/gsf/checkin/proto/Logs$AndroidBuildProto;

    goto :goto_0

    :sswitch_3
    invoke-virtual {p1}, Lcom/google/protobuf/micro/CodedInputStreamMicro;->readString()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {p0, v1}, Lcom/google/android/gsf/checkin/proto/Logs$AndroidBuildProto;->setCarrier(Ljava/lang/String;)Lcom/google/android/gsf/checkin/proto/Logs$AndroidBuildProto;

    goto :goto_0

    :sswitch_4
    invoke-virtual {p1}, Lcom/google/protobuf/micro/CodedInputStreamMicro;->readString()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {p0, v1}, Lcom/google/android/gsf/checkin/proto/Logs$AndroidBuildProto;->setRadio(Ljava/lang/String;)Lcom/google/android/gsf/checkin/proto/Logs$AndroidBuildProto;

    goto :goto_0

    :sswitch_5
    invoke-virtual {p1}, Lcom/google/protobuf/micro/CodedInputStreamMicro;->readString()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {p0, v1}, Lcom/google/android/gsf/checkin/proto/Logs$AndroidBuildProto;->setBootloader(Ljava/lang/String;)Lcom/google/android/gsf/checkin/proto/Logs$AndroidBuildProto;

    goto :goto_0

    :sswitch_6
    invoke-virtual {p1}, Lcom/google/protobuf/micro/CodedInputStreamMicro;->readString()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {p0, v1}, Lcom/google/android/gsf/checkin/proto/Logs$AndroidBuildProto;->setClient(Ljava/lang/String;)Lcom/google/android/gsf/checkin/proto/Logs$AndroidBuildProto;

    goto :goto_0

    :sswitch_7
    invoke-virtual {p1}, Lcom/google/protobuf/micro/CodedInputStreamMicro;->readInt64()J

    move-result-wide v1

    invoke-virtual {p0, v1, v2}, Lcom/google/android/gsf/checkin/proto/Logs$AndroidBuildProto;->setTimestamp(J)Lcom/google/android/gsf/checkin/proto/Logs$AndroidBuildProto;

    goto :goto_0

    :sswitch_8
    invoke-virtual {p1}, Lcom/google/protobuf/micro/CodedInputStreamMicro;->readInt32()I

    move-result v1

    invoke-virtual {p0, v1}, Lcom/google/android/gsf/checkin/proto/Logs$AndroidBuildProto;->setGoogleServices(I)Lcom/google/android/gsf/checkin/proto/Logs$AndroidBuildProto;

    goto :goto_0

    :sswitch_9
    invoke-virtual {p1}, Lcom/google/protobuf/micro/CodedInputStreamMicro;->readString()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {p0, v1}, Lcom/google/android/gsf/checkin/proto/Logs$AndroidBuildProto;->setDevice(Ljava/lang/String;)Lcom/google/android/gsf/checkin/proto/Logs$AndroidBuildProto;

    goto :goto_0

    :sswitch_a
    invoke-virtual {p1}, Lcom/google/protobuf/micro/CodedInputStreamMicro;->readInt32()I

    move-result v1

    invoke-virtual {p0, v1}, Lcom/google/android/gsf/checkin/proto/Logs$AndroidBuildProto;->setSdkVersion(I)Lcom/google/android/gsf/checkin/proto/Logs$AndroidBuildProto;

    goto :goto_0

    :sswitch_b
    invoke-virtual {p1}, Lcom/google/protobuf/micro/CodedInputStreamMicro;->readString()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {p0, v1}, Lcom/google/android/gsf/checkin/proto/Logs$AndroidBuildProto;->setModel(Ljava/lang/String;)Lcom/google/android/gsf/checkin/proto/Logs$AndroidBuildProto;

    goto :goto_0

    :sswitch_c
    invoke-virtual {p1}, Lcom/google/protobuf/micro/CodedInputStreamMicro;->readString()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {p0, v1}, Lcom/google/android/gsf/checkin/proto/Logs$AndroidBuildProto;->setManufacturer(Ljava/lang/String;)Lcom/google/android/gsf/checkin/proto/Logs$AndroidBuildProto;

    goto :goto_0

    :sswitch_d
    invoke-virtual {p1}, Lcom/google/protobuf/micro/CodedInputStreamMicro;->readString()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {p0, v1}, Lcom/google/android/gsf/checkin/proto/Logs$AndroidBuildProto;->setBuildProduct(Ljava/lang/String;)Lcom/google/android/gsf/checkin/proto/Logs$AndroidBuildProto;

    goto :goto_0

    :sswitch_e
    invoke-virtual {p1}, Lcom/google/protobuf/micro/CodedInputStreamMicro;->readBool()Z

    move-result v1

    invoke-virtual {p0, v1}, Lcom/google/android/gsf/checkin/proto/Logs$AndroidBuildProto;->setOtaInstalled(Z)Lcom/google/android/gsf/checkin/proto/Logs$AndroidBuildProto;

    goto :goto_0

    :sswitch_data_0
    .sparse-switch
        0x0 -> :sswitch_0
        0xa -> :sswitch_1
        0x12 -> :sswitch_2
        0x1a -> :sswitch_3
        0x22 -> :sswitch_4
        0x2a -> :sswitch_5
        0x32 -> :sswitch_6
        0x38 -> :sswitch_7
        0x40 -> :sswitch_8
        0x4a -> :sswitch_9
        0x50 -> :sswitch_a
        0x5a -> :sswitch_b
        0x62 -> :sswitch_c
        0x6a -> :sswitch_d
        0x70 -> :sswitch_e
    .end sparse-switch
.end method

.method public bridge synthetic mergeFrom(Lcom/google/protobuf/micro/CodedInputStreamMicro;)Lcom/google/protobuf/micro/MessageMicro;
    .locals 1
    .param p1    # Lcom/google/protobuf/micro/CodedInputStreamMicro;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    invoke-virtual {p0, p1}, Lcom/google/android/gsf/checkin/proto/Logs$AndroidBuildProto;->mergeFrom(Lcom/google/protobuf/micro/CodedInputStreamMicro;)Lcom/google/android/gsf/checkin/proto/Logs$AndroidBuildProto;

    move-result-object v0

    return-object v0
.end method

.method public setBootloader(Ljava/lang/String;)Lcom/google/android/gsf/checkin/proto/Logs$AndroidBuildProto;
    .locals 1
    .param p1    # Ljava/lang/String;

    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/google/android/gsf/checkin/proto/Logs$AndroidBuildProto;->hasBootloader:Z

    iput-object p1, p0, Lcom/google/android/gsf/checkin/proto/Logs$AndroidBuildProto;->bootloader_:Ljava/lang/String;

    return-object p0
.end method

.method public setBuildProduct(Ljava/lang/String;)Lcom/google/android/gsf/checkin/proto/Logs$AndroidBuildProto;
    .locals 1
    .param p1    # Ljava/lang/String;

    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/google/android/gsf/checkin/proto/Logs$AndroidBuildProto;->hasBuildProduct:Z

    iput-object p1, p0, Lcom/google/android/gsf/checkin/proto/Logs$AndroidBuildProto;->buildProduct_:Ljava/lang/String;

    return-object p0
.end method

.method public setCarrier(Ljava/lang/String;)Lcom/google/android/gsf/checkin/proto/Logs$AndroidBuildProto;
    .locals 1
    .param p1    # Ljava/lang/String;

    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/google/android/gsf/checkin/proto/Logs$AndroidBuildProto;->hasCarrier:Z

    iput-object p1, p0, Lcom/google/android/gsf/checkin/proto/Logs$AndroidBuildProto;->carrier_:Ljava/lang/String;

    return-object p0
.end method

.method public setClient(Ljava/lang/String;)Lcom/google/android/gsf/checkin/proto/Logs$AndroidBuildProto;
    .locals 1
    .param p1    # Ljava/lang/String;

    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/google/android/gsf/checkin/proto/Logs$AndroidBuildProto;->hasClient:Z

    iput-object p1, p0, Lcom/google/android/gsf/checkin/proto/Logs$AndroidBuildProto;->client_:Ljava/lang/String;

    return-object p0
.end method

.method public setDevice(Ljava/lang/String;)Lcom/google/android/gsf/checkin/proto/Logs$AndroidBuildProto;
    .locals 1
    .param p1    # Ljava/lang/String;

    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/google/android/gsf/checkin/proto/Logs$AndroidBuildProto;->hasDevice:Z

    iput-object p1, p0, Lcom/google/android/gsf/checkin/proto/Logs$AndroidBuildProto;->device_:Ljava/lang/String;

    return-object p0
.end method

.method public setGoogleServices(I)Lcom/google/android/gsf/checkin/proto/Logs$AndroidBuildProto;
    .locals 1
    .param p1    # I

    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/google/android/gsf/checkin/proto/Logs$AndroidBuildProto;->hasGoogleServices:Z

    iput p1, p0, Lcom/google/android/gsf/checkin/proto/Logs$AndroidBuildProto;->googleServices_:I

    return-object p0
.end method

.method public setId(Ljava/lang/String;)Lcom/google/android/gsf/checkin/proto/Logs$AndroidBuildProto;
    .locals 1
    .param p1    # Ljava/lang/String;

    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/google/android/gsf/checkin/proto/Logs$AndroidBuildProto;->hasId:Z

    iput-object p1, p0, Lcom/google/android/gsf/checkin/proto/Logs$AndroidBuildProto;->id_:Ljava/lang/String;

    return-object p0
.end method

.method public setManufacturer(Ljava/lang/String;)Lcom/google/android/gsf/checkin/proto/Logs$AndroidBuildProto;
    .locals 1
    .param p1    # Ljava/lang/String;

    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/google/android/gsf/checkin/proto/Logs$AndroidBuildProto;->hasManufacturer:Z

    iput-object p1, p0, Lcom/google/android/gsf/checkin/proto/Logs$AndroidBuildProto;->manufacturer_:Ljava/lang/String;

    return-object p0
.end method

.method public setModel(Ljava/lang/String;)Lcom/google/android/gsf/checkin/proto/Logs$AndroidBuildProto;
    .locals 1
    .param p1    # Ljava/lang/String;

    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/google/android/gsf/checkin/proto/Logs$AndroidBuildProto;->hasModel:Z

    iput-object p1, p0, Lcom/google/android/gsf/checkin/proto/Logs$AndroidBuildProto;->model_:Ljava/lang/String;

    return-object p0
.end method

.method public setOtaInstalled(Z)Lcom/google/android/gsf/checkin/proto/Logs$AndroidBuildProto;
    .locals 1
    .param p1    # Z

    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/google/android/gsf/checkin/proto/Logs$AndroidBuildProto;->hasOtaInstalled:Z

    iput-boolean p1, p0, Lcom/google/android/gsf/checkin/proto/Logs$AndroidBuildProto;->otaInstalled_:Z

    return-object p0
.end method

.method public setProduct(Ljava/lang/String;)Lcom/google/android/gsf/checkin/proto/Logs$AndroidBuildProto;
    .locals 1
    .param p1    # Ljava/lang/String;

    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/google/android/gsf/checkin/proto/Logs$AndroidBuildProto;->hasProduct:Z

    iput-object p1, p0, Lcom/google/android/gsf/checkin/proto/Logs$AndroidBuildProto;->product_:Ljava/lang/String;

    return-object p0
.end method

.method public setRadio(Ljava/lang/String;)Lcom/google/android/gsf/checkin/proto/Logs$AndroidBuildProto;
    .locals 1
    .param p1    # Ljava/lang/String;

    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/google/android/gsf/checkin/proto/Logs$AndroidBuildProto;->hasRadio:Z

    iput-object p1, p0, Lcom/google/android/gsf/checkin/proto/Logs$AndroidBuildProto;->radio_:Ljava/lang/String;

    return-object p0
.end method

.method public setSdkVersion(I)Lcom/google/android/gsf/checkin/proto/Logs$AndroidBuildProto;
    .locals 1
    .param p1    # I

    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/google/android/gsf/checkin/proto/Logs$AndroidBuildProto;->hasSdkVersion:Z

    iput p1, p0, Lcom/google/android/gsf/checkin/proto/Logs$AndroidBuildProto;->sdkVersion_:I

    return-object p0
.end method

.method public setTimestamp(J)Lcom/google/android/gsf/checkin/proto/Logs$AndroidBuildProto;
    .locals 1
    .param p1    # J

    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/google/android/gsf/checkin/proto/Logs$AndroidBuildProto;->hasTimestamp:Z

    iput-wide p1, p0, Lcom/google/android/gsf/checkin/proto/Logs$AndroidBuildProto;->timestamp_:J

    return-object p0
.end method

.method public writeTo(Lcom/google/protobuf/micro/CodedOutputStreamMicro;)V
    .locals 3
    .param p1    # Lcom/google/protobuf/micro/CodedOutputStreamMicro;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    invoke-virtual {p0}, Lcom/google/android/gsf/checkin/proto/Logs$AndroidBuildProto;->hasId()Z

    move-result v0

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    invoke-virtual {p0}, Lcom/google/android/gsf/checkin/proto/Logs$AndroidBuildProto;->getId()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {p1, v0, v1}, Lcom/google/protobuf/micro/CodedOutputStreamMicro;->writeString(ILjava/lang/String;)V

    :cond_0
    invoke-virtual {p0}, Lcom/google/android/gsf/checkin/proto/Logs$AndroidBuildProto;->hasProduct()Z

    move-result v0

    if-eqz v0, :cond_1

    const/4 v0, 0x2

    invoke-virtual {p0}, Lcom/google/android/gsf/checkin/proto/Logs$AndroidBuildProto;->getProduct()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {p1, v0, v1}, Lcom/google/protobuf/micro/CodedOutputStreamMicro;->writeString(ILjava/lang/String;)V

    :cond_1
    invoke-virtual {p0}, Lcom/google/android/gsf/checkin/proto/Logs$AndroidBuildProto;->hasCarrier()Z

    move-result v0

    if-eqz v0, :cond_2

    const/4 v0, 0x3

    invoke-virtual {p0}, Lcom/google/android/gsf/checkin/proto/Logs$AndroidBuildProto;->getCarrier()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {p1, v0, v1}, Lcom/google/protobuf/micro/CodedOutputStreamMicro;->writeString(ILjava/lang/String;)V

    :cond_2
    invoke-virtual {p0}, Lcom/google/android/gsf/checkin/proto/Logs$AndroidBuildProto;->hasRadio()Z

    move-result v0

    if-eqz v0, :cond_3

    const/4 v0, 0x4

    invoke-virtual {p0}, Lcom/google/android/gsf/checkin/proto/Logs$AndroidBuildProto;->getRadio()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {p1, v0, v1}, Lcom/google/protobuf/micro/CodedOutputStreamMicro;->writeString(ILjava/lang/String;)V

    :cond_3
    invoke-virtual {p0}, Lcom/google/android/gsf/checkin/proto/Logs$AndroidBuildProto;->hasBootloader()Z

    move-result v0

    if-eqz v0, :cond_4

    const/4 v0, 0x5

    invoke-virtual {p0}, Lcom/google/android/gsf/checkin/proto/Logs$AndroidBuildProto;->getBootloader()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {p1, v0, v1}, Lcom/google/protobuf/micro/CodedOutputStreamMicro;->writeString(ILjava/lang/String;)V

    :cond_4
    invoke-virtual {p0}, Lcom/google/android/gsf/checkin/proto/Logs$AndroidBuildProto;->hasClient()Z

    move-result v0

    if-eqz v0, :cond_5

    const/4 v0, 0x6

    invoke-virtual {p0}, Lcom/google/android/gsf/checkin/proto/Logs$AndroidBuildProto;->getClient()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {p1, v0, v1}, Lcom/google/protobuf/micro/CodedOutputStreamMicro;->writeString(ILjava/lang/String;)V

    :cond_5
    invoke-virtual {p0}, Lcom/google/android/gsf/checkin/proto/Logs$AndroidBuildProto;->hasTimestamp()Z

    move-result v0

    if-eqz v0, :cond_6

    const/4 v0, 0x7

    invoke-virtual {p0}, Lcom/google/android/gsf/checkin/proto/Logs$AndroidBuildProto;->getTimestamp()J

    move-result-wide v1

    invoke-virtual {p1, v0, v1, v2}, Lcom/google/protobuf/micro/CodedOutputStreamMicro;->writeInt64(IJ)V

    :cond_6
    invoke-virtual {p0}, Lcom/google/android/gsf/checkin/proto/Logs$AndroidBuildProto;->hasGoogleServices()Z

    move-result v0

    if-eqz v0, :cond_7

    const/16 v0, 0x8

    invoke-virtual {p0}, Lcom/google/android/gsf/checkin/proto/Logs$AndroidBuildProto;->getGoogleServices()I

    move-result v1

    invoke-virtual {p1, v0, v1}, Lcom/google/protobuf/micro/CodedOutputStreamMicro;->writeInt32(II)V

    :cond_7
    invoke-virtual {p0}, Lcom/google/android/gsf/checkin/proto/Logs$AndroidBuildProto;->hasDevice()Z

    move-result v0

    if-eqz v0, :cond_8

    const/16 v0, 0x9

    invoke-virtual {p0}, Lcom/google/android/gsf/checkin/proto/Logs$AndroidBuildProto;->getDevice()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {p1, v0, v1}, Lcom/google/protobuf/micro/CodedOutputStreamMicro;->writeString(ILjava/lang/String;)V

    :cond_8
    invoke-virtual {p0}, Lcom/google/android/gsf/checkin/proto/Logs$AndroidBuildProto;->hasSdkVersion()Z

    move-result v0

    if-eqz v0, :cond_9

    const/16 v0, 0xa

    invoke-virtual {p0}, Lcom/google/android/gsf/checkin/proto/Logs$AndroidBuildProto;->getSdkVersion()I

    move-result v1

    invoke-virtual {p1, v0, v1}, Lcom/google/protobuf/micro/CodedOutputStreamMicro;->writeInt32(II)V

    :cond_9
    invoke-virtual {p0}, Lcom/google/android/gsf/checkin/proto/Logs$AndroidBuildProto;->hasModel()Z

    move-result v0

    if-eqz v0, :cond_a

    const/16 v0, 0xb

    invoke-virtual {p0}, Lcom/google/android/gsf/checkin/proto/Logs$AndroidBuildProto;->getModel()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {p1, v0, v1}, Lcom/google/protobuf/micro/CodedOutputStreamMicro;->writeString(ILjava/lang/String;)V

    :cond_a
    invoke-virtual {p0}, Lcom/google/android/gsf/checkin/proto/Logs$AndroidBuildProto;->hasManufacturer()Z

    move-result v0

    if-eqz v0, :cond_b

    const/16 v0, 0xc

    invoke-virtual {p0}, Lcom/google/android/gsf/checkin/proto/Logs$AndroidBuildProto;->getManufacturer()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {p1, v0, v1}, Lcom/google/protobuf/micro/CodedOutputStreamMicro;->writeString(ILjava/lang/String;)V

    :cond_b
    invoke-virtual {p0}, Lcom/google/android/gsf/checkin/proto/Logs$AndroidBuildProto;->hasBuildProduct()Z

    move-result v0

    if-eqz v0, :cond_c

    const/16 v0, 0xd

    invoke-virtual {p0}, Lcom/google/android/gsf/checkin/proto/Logs$AndroidBuildProto;->getBuildProduct()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {p1, v0, v1}, Lcom/google/protobuf/micro/CodedOutputStreamMicro;->writeString(ILjava/lang/String;)V

    :cond_c
    invoke-virtual {p0}, Lcom/google/android/gsf/checkin/proto/Logs$AndroidBuildProto;->hasOtaInstalled()Z

    move-result v0

    if-eqz v0, :cond_d

    const/16 v0, 0xe

    invoke-virtual {p0}, Lcom/google/android/gsf/checkin/proto/Logs$AndroidBuildProto;->getOtaInstalled()Z

    move-result v1

    invoke-virtual {p1, v0, v1}, Lcom/google/protobuf/micro/CodedOutputStreamMicro;->writeBool(IZ)V

    :cond_d
    return-void
.end method
