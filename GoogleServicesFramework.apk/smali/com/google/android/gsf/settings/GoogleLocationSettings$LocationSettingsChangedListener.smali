.class public Lcom/google/android/gsf/settings/GoogleLocationSettings$LocationSettingsChangedListener;
.super Landroid/content/BroadcastReceiver;
.source "GoogleLocationSettings.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/google/android/gsf/settings/GoogleLocationSettings;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x9
    name = "LocationSettingsChangedListener"
.end annotation


# direct methods
.method public constructor <init>()V
    .locals 0

    invoke-direct {p0}, Landroid/content/BroadcastReceiver;-><init>()V

    return-void
.end method


# virtual methods
.method public onReceive(Landroid/content/Context;Landroid/content/Intent;)V
    .locals 3
    .param p1    # Landroid/content/Context;
    .param p2    # Landroid/content/Intent;

    const/4 v1, 0x1

    invoke-static {p1}, Lcom/google/android/gsf/settings/GoogleLocationSettingHelper;->getUseLocationForServices(Landroid/content/Context;)I

    move-result v2

    if-ne v2, v1, :cond_0

    :goto_0
    invoke-virtual {p1}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v0

    invoke-static {v0, v1}, Lcom/google/android/gsf/settings/GoogleLocationSettings;->copyPackagePrefixLists(Landroid/content/ContentResolver;Z)V

    return-void

    :cond_0
    const/4 v1, 0x0

    goto :goto_0
.end method
