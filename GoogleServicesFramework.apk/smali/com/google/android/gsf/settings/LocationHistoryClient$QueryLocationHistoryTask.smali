.class Lcom/google/android/gsf/settings/LocationHistoryClient$QueryLocationHistoryTask;
.super Landroid/os/AsyncTask;
.source "LocationHistoryClient.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/google/android/gsf/settings/LocationHistoryClient;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = "QueryLocationHistoryTask"
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Landroid/os/AsyncTask",
        "<[",
        "Landroid/accounts/Account;",
        "Ljava/lang/Integer;",
        "[I>;"
    }
.end annotation


# instance fields
.field mAccounts:[Landroid/accounts/Account;

.field mCallback:Lcom/google/android/gsf/settings/LocationHistoryClient$LocationHistoryCallback;

.field final synthetic this$0:Lcom/google/android/gsf/settings/LocationHistoryClient;


# direct methods
.method public constructor <init>(Lcom/google/android/gsf/settings/LocationHistoryClient;Lcom/google/android/gsf/settings/LocationHistoryClient$LocationHistoryCallback;)V
    .locals 0
    .param p2    # Lcom/google/android/gsf/settings/LocationHistoryClient$LocationHistoryCallback;

    iput-object p1, p0, Lcom/google/android/gsf/settings/LocationHistoryClient$QueryLocationHistoryTask;->this$0:Lcom/google/android/gsf/settings/LocationHistoryClient;

    invoke-direct {p0}, Landroid/os/AsyncTask;-><init>()V

    iput-object p2, p0, Lcom/google/android/gsf/settings/LocationHistoryClient$QueryLocationHistoryTask;->mCallback:Lcom/google/android/gsf/settings/LocationHistoryClient$LocationHistoryCallback;

    return-void
.end method


# virtual methods
.method public bridge synthetic doInBackground([Ljava/lang/Object;)Ljava/lang/Object;
    .locals 1
    .param p1    # [Ljava/lang/Object;

    check-cast p1, [[Landroid/accounts/Account;

    invoke-virtual {p0, p1}, Lcom/google/android/gsf/settings/LocationHistoryClient$QueryLocationHistoryTask;->doInBackground([[Landroid/accounts/Account;)[I

    move-result-object v0

    return-object v0
.end method

.method public varargs doInBackground([[Landroid/accounts/Account;)[I
    .locals 4
    .param p1    # [[Landroid/accounts/Account;

    const/4 v2, 0x0

    aget-object v2, p1, v2

    iput-object v2, p0, Lcom/google/android/gsf/settings/LocationHistoryClient$QueryLocationHistoryTask;->mAccounts:[Landroid/accounts/Account;

    iget-object v2, p0, Lcom/google/android/gsf/settings/LocationHistoryClient$QueryLocationHistoryTask;->mAccounts:[Landroid/accounts/Account;

    array-length v2, v2

    new-array v1, v2, [I

    const/4 v0, 0x0

    :goto_0
    iget-object v2, p0, Lcom/google/android/gsf/settings/LocationHistoryClient$QueryLocationHistoryTask;->mAccounts:[Landroid/accounts/Account;

    array-length v2, v2

    if-ge v0, v2, :cond_0

    iget-object v2, p0, Lcom/google/android/gsf/settings/LocationHistoryClient$QueryLocationHistoryTask;->this$0:Lcom/google/android/gsf/settings/LocationHistoryClient;

    iget-object v3, p0, Lcom/google/android/gsf/settings/LocationHistoryClient$QueryLocationHistoryTask;->mAccounts:[Landroid/accounts/Account;

    aget-object v3, v3, v0

    # invokes: Lcom/google/android/gsf/settings/LocationHistoryClient;->getLocationHistoryState(Landroid/accounts/Account;)I
    invoke-static {v2, v3}, Lcom/google/android/gsf/settings/LocationHistoryClient;->access$000(Lcom/google/android/gsf/settings/LocationHistoryClient;Landroid/accounts/Account;)I

    move-result v2

    aput v2, v1, v0

    aget v2, v1, v0

    const/4 v3, 0x2

    if-ne v2, v3, :cond_1

    :cond_0
    return-object v1

    :cond_1
    add-int/lit8 v0, v0, 0x1

    goto :goto_0
.end method

.method public bridge synthetic onPostExecute(Ljava/lang/Object;)V
    .locals 0
    .param p1    # Ljava/lang/Object;

    check-cast p1, [I

    invoke-virtual {p0, p1}, Lcom/google/android/gsf/settings/LocationHistoryClient$QueryLocationHistoryTask;->onPostExecute([I)V

    return-void
.end method

.method public onPostExecute([I)V
    .locals 1
    .param p1    # [I

    iget-object v0, p0, Lcom/google/android/gsf/settings/LocationHistoryClient$QueryLocationHistoryTask;->mCallback:Lcom/google/android/gsf/settings/LocationHistoryClient$LocationHistoryCallback;

    invoke-interface {v0, p1}, Lcom/google/android/gsf/settings/LocationHistoryClient$LocationHistoryCallback;->onResult([I)V

    return-void
.end method
