.class Lcom/google/android/gsf/settings/LocationHistoryClient;
.super Ljava/lang/Object;
.source "LocationHistoryClient.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/google/android/gsf/settings/LocationHistoryClient$ModifyLocationHistoryTask;,
        Lcom/google/android/gsf/settings/LocationHistoryClient$QueryLocationHistoryTask;,
        Lcom/google/android/gsf/settings/LocationHistoryClient$LocationHistoryCallback;
    }
.end annotation


# instance fields
.field final mAccountManager:Landroid/accounts/AccountManager;

.field final mContext:Landroid/content/Context;


# direct methods
.method constructor <init>(Landroid/content/Context;)V
    .locals 2
    .param p1    # Landroid/content/Context;

    invoke-direct/range {p0 .. p0}, Ljava/lang/Object;-><init>()V

    iput-object p1, p0, Lcom/google/android/gsf/settings/LocationHistoryClient;->mContext:Landroid/content/Context;

    iget-object v0, p0, Lcom/google/android/gsf/settings/LocationHistoryClient;->mContext:Landroid/content/Context;

    const-string v1, "account"

    invoke-virtual {v0, v1}, Landroid/content/Context;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/accounts/AccountManager;

    iput-object v0, p0, Lcom/google/android/gsf/settings/LocationHistoryClient;->mAccountManager:Landroid/accounts/AccountManager;

    return-void
.end method

.method static synthetic access$000(Lcom/google/android/gsf/settings/LocationHistoryClient;Landroid/accounts/Account;)I
    .locals 1
    .param p0    # Lcom/google/android/gsf/settings/LocationHistoryClient;
    .param p1    # Landroid/accounts/Account;

    invoke-direct {p0, p1}, Lcom/google/android/gsf/settings/LocationHistoryClient;->getLocationHistoryState(Landroid/accounts/Account;)I

    move-result v0

    return v0
.end method

.method static synthetic access$100(Lcom/google/android/gsf/settings/LocationHistoryClient;Landroid/accounts/Account;Z)I
    .locals 1
    .param p0    # Lcom/google/android/gsf/settings/LocationHistoryClient;
    .param p1    # Landroid/accounts/Account;
    .param p2    # Z

    invoke-direct {p0, p1, p2}, Lcom/google/android/gsf/settings/LocationHistoryClient;->putLocationHistoryState(Landroid/accounts/Account;Z)I

    move-result v0

    return v0
.end method

.method private getLocationHistoryState(Landroid/accounts/Account;)I
    .locals 11
    .param p1    # Landroid/accounts/Account;

    const/4 v7, 0x2

    const/4 v5, 0x0

    invoke-virtual {p0, p1}, Lcom/google/android/gsf/settings/LocationHistoryClient;->getAuthToken(Landroid/accounts/Account;)Ljava/lang/String;

    move-result-object v0

    new-instance v1, Lcom/google/android/common/http/GoogleHttpClient;

    iget-object v8, p0, Lcom/google/android/gsf/settings/LocationHistoryClient;->mContext:Landroid/content/Context;

    const-string v9, "GoogAppsLocHist 1.0"

    invoke-direct {v1, v8, v9, v5}, Lcom/google/android/common/http/GoogleHttpClient;-><init>(Landroid/content/Context;Ljava/lang/String;Z)V

    new-instance v3, Lorg/apache/http/client/methods/HttpGet;

    const-string v8, "https://android.clients.google.com/locationhistory/getsavehistory"

    invoke-direct {v3, v8}, Lorg/apache/http/client/methods/HttpGet;-><init>(Ljava/lang/String;)V

    const-string v8, "Authorization"

    new-instance v9, Ljava/lang/StringBuilder;

    invoke-direct {v9}, Ljava/lang/StringBuilder;-><init>()V

    const-string v10, "GoogleLogin auth="

    invoke-virtual {v9, v10}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v9

    invoke-virtual {v9, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v9

    invoke-virtual {v9}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v9

    invoke-virtual {v3, v8, v9}, Lorg/apache/http/client/methods/HttpGet;->setHeader(Ljava/lang/String;Ljava/lang/String;)V

    const-string v8, "Host"

    const-string v9, "android.clients.google.com"

    invoke-virtual {v3, v8, v9}, Lorg/apache/http/client/methods/HttpGet;->setHeader(Ljava/lang/String;Ljava/lang/String;)V

    :try_start_0
    invoke-virtual {v1, v3}, Lcom/google/android/common/http/GoogleHttpClient;->execute(Lorg/apache/http/client/methods/HttpUriRequest;)Lorg/apache/http/HttpResponse;

    move-result-object v4

    invoke-interface {v4}, Lorg/apache/http/HttpResponse;->getStatusLine()Lorg/apache/http/StatusLine;

    move-result-object v6

    const-string v8, "LocationHistoryClient"

    new-instance v9, Ljava/lang/StringBuilder;

    invoke-direct {v9}, Ljava/lang/StringBuilder;-><init>()V

    const-string v10, "Retrieving location history: "

    invoke-virtual {v9, v10}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v9

    invoke-interface {v6}, Lorg/apache/http/StatusLine;->getStatusCode()I

    move-result v10

    invoke-virtual {v9, v10}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v9

    invoke-virtual {v9}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v9

    invoke-static {v8, v9}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    invoke-interface {v6}, Lorg/apache/http/StatusLine;->getStatusCode()I

    move-result v8

    const/16 v9, 0xc8

    if-eq v8, v9, :cond_0

    move v5, v7

    :goto_0
    return v5

    :cond_0
    const-string v8, "locationSetting"

    invoke-interface {v4, v8}, Lorg/apache/http/HttpResponse;->getHeaders(Ljava/lang/String;)[Lorg/apache/http/Header;

    move-result-object v2

    if-eqz v2, :cond_2

    array-length v8, v2

    if-lez v8, :cond_2

    const/4 v8, 0x0

    aget-object v8, v2, v8

    invoke-interface {v8}, Lorg/apache/http/Header;->getValue()Ljava/lang/String;

    move-result-object v8

    const-string v9, "true"

    invoke-virtual {v8, v9}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v8

    if-eqz v8, :cond_1

    const/4 v5, 0x1

    :cond_1
    const-string v8, "LocationHistoryClient"

    new-instance v9, Ljava/lang/StringBuilder;

    invoke-direct {v9}, Ljava/lang/StringBuilder;-><init>()V

    const-string v10, "Location history value: "

    invoke-virtual {v9, v10}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v9

    invoke-virtual {v9, v5}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v9

    invoke-virtual {v9}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v9

    invoke-static {v8, v9}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I
    :try_end_0
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_0

    goto :goto_0

    :catch_0
    move-exception v8

    :cond_2
    move v5, v7

    goto :goto_0
.end method

.method private putLocationHistoryState(Landroid/accounts/Account;Z)I
    .locals 10
    .param p1    # Landroid/accounts/Account;
    .param p2    # Z

    const/4 v6, 0x2

    const/4 v5, 0x0

    invoke-virtual {p0, p1}, Lcom/google/android/gsf/settings/LocationHistoryClient;->getAuthToken(Landroid/accounts/Account;)Ljava/lang/String;

    move-result-object v0

    new-instance v1, Lcom/google/android/common/http/GoogleHttpClient;

    iget-object v7, p0, Lcom/google/android/gsf/settings/LocationHistoryClient;->mContext:Landroid/content/Context;

    const-string v8, "GoogAppsLocHist 1.0"

    invoke-direct {v1, v7, v8, v5}, Lcom/google/android/common/http/GoogleHttpClient;-><init>(Landroid/content/Context;Ljava/lang/String;Z)V

    new-instance v2, Lorg/apache/http/client/methods/HttpPost;

    const-string v7, "https://android.clients.google.com/locationhistory/setsavehistory"

    invoke-direct {v2, v7}, Lorg/apache/http/client/methods/HttpPost;-><init>(Ljava/lang/String;)V

    const-string v7, "Authorization"

    new-instance v8, Ljava/lang/StringBuilder;

    invoke-direct {v8}, Ljava/lang/StringBuilder;-><init>()V

    const-string v9, "GoogleLogin auth="

    invoke-virtual {v8, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v8

    invoke-virtual {v8, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v8

    invoke-virtual {v8}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v8

    invoke-virtual {v2, v7, v8}, Lorg/apache/http/client/methods/HttpPost;->setHeader(Ljava/lang/String;Ljava/lang/String;)V

    const-string v7, "Host"

    const-string v8, "android.clients.google.com"

    invoke-virtual {v2, v7, v8}, Lorg/apache/http/client/methods/HttpPost;->setHeader(Ljava/lang/String;Ljava/lang/String;)V

    const-string v7, "Accept-Encoding"

    const-string v8, "gzip"

    invoke-virtual {v2, v7, v8}, Lorg/apache/http/client/methods/HttpPost;->setHeader(Ljava/lang/String;Ljava/lang/String;)V

    const-string v7, "Content-Type"

    const-string v8, "application/x-www-form-urlencoded"

    invoke-virtual {v2, v7, v8}, Lorg/apache/http/client/methods/HttpPost;->setHeader(Ljava/lang/String;Ljava/lang/String;)V

    :try_start_0
    new-instance v7, Lorg/apache/http/entity/StringEntity;

    new-instance v8, Ljava/lang/StringBuilder;

    invoke-direct {v8}, Ljava/lang/StringBuilder;-><init>()V

    const-string v9, "locationSetting="

    invoke-virtual {v8, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v8

    invoke-virtual {v8, p2}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v8

    invoke-virtual {v8}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v8

    invoke-direct {v7, v8}, Lorg/apache/http/entity/StringEntity;-><init>(Ljava/lang/String;)V

    invoke-virtual {v2, v7}, Lorg/apache/http/client/methods/HttpPost;->setEntity(Lorg/apache/http/HttpEntity;)V

    invoke-virtual {v1, v2}, Lcom/google/android/common/http/GoogleHttpClient;->execute(Lorg/apache/http/client/methods/HttpUriRequest;)Lorg/apache/http/HttpResponse;

    move-result-object v3

    invoke-interface {v3}, Lorg/apache/http/HttpResponse;->getStatusLine()Lorg/apache/http/StatusLine;

    move-result-object v4

    const-string v7, "LocationHistoryClient"

    new-instance v8, Ljava/lang/StringBuilder;

    invoke-direct {v8}, Ljava/lang/StringBuilder;-><init>()V

    const-string v9, "Updating location history: "

    invoke-virtual {v8, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v8

    invoke-interface {v4}, Lorg/apache/http/StatusLine;->getStatusCode()I

    move-result v9

    invoke-virtual {v8, v9}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v8

    invoke-virtual {v8}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v8

    invoke-static {v7, v8}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    invoke-interface {v4}, Lorg/apache/http/StatusLine;->getStatusCode()I
    :try_end_0
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_0

    move-result v7

    const/16 v8, 0xc8

    if-eq v7, v8, :cond_1

    move v5, v6

    :cond_0
    :goto_0
    return v5

    :cond_1
    if-eqz p2, :cond_0

    const/4 v5, 0x1

    goto :goto_0

    :catch_0
    move-exception v5

    move v5, v6

    goto :goto_0
.end method


# virtual methods
.method getAuthToken(Landroid/accounts/Account;)Ljava/lang/String;
    .locals 4
    .param p1    # Landroid/accounts/Account;

    :try_start_0
    iget-object v1, p0, Lcom/google/android/gsf/settings/LocationHistoryClient;->mAccountManager:Landroid/accounts/AccountManager;

    const-string v2, "ac2dm"

    const/4 v3, 0x1

    invoke-virtual {v1, p1, v2, v3}, Landroid/accounts/AccountManager;->blockingGetAuthToken(Landroid/accounts/Account;Ljava/lang/String;Z)Ljava/lang/String;
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    move-result-object v1

    :goto_0
    return-object v1

    :catch_0
    move-exception v0

    const/4 v1, 0x0

    goto :goto_0
.end method

.method getGoogleAccounts()[Landroid/accounts/Account;
    .locals 2

    iget-object v0, p0, Lcom/google/android/gsf/settings/LocationHistoryClient;->mAccountManager:Landroid/accounts/AccountManager;

    const-string v1, "com.google"

    invoke-virtual {v0, v1}, Landroid/accounts/AccountManager;->getAccountsByType(Ljava/lang/String;)[Landroid/accounts/Account;

    move-result-object v0

    return-object v0
.end method

.method modifyLocationHistoryEnabled([Landroid/accounts/Account;[ILcom/google/android/gsf/settings/LocationHistoryClient$LocationHistoryCallback;)V
    .locals 3
    .param p1    # [Landroid/accounts/Account;
    .param p2    # [I
    .param p3    # Lcom/google/android/gsf/settings/LocationHistoryClient$LocationHistoryCallback;

    new-instance v0, Lcom/google/android/gsf/settings/LocationHistoryClient$ModifyLocationHistoryTask;

    invoke-direct {v0, p0, p2, p3}, Lcom/google/android/gsf/settings/LocationHistoryClient$ModifyLocationHistoryTask;-><init>(Lcom/google/android/gsf/settings/LocationHistoryClient;[ILcom/google/android/gsf/settings/LocationHistoryClient$LocationHistoryCallback;)V

    const/4 v1, 0x1

    new-array v1, v1, [[Landroid/accounts/Account;

    const/4 v2, 0x0

    aput-object p1, v1, v2

    invoke-virtual {v0, v1}, Lcom/google/android/gsf/settings/LocationHistoryClient$ModifyLocationHistoryTask;->execute([Ljava/lang/Object;)Landroid/os/AsyncTask;

    return-void
.end method

.method queryLocationHistoryEnabled([Landroid/accounts/Account;Lcom/google/android/gsf/settings/LocationHistoryClient$LocationHistoryCallback;)V
    .locals 3
    .param p1    # [Landroid/accounts/Account;
    .param p2    # Lcom/google/android/gsf/settings/LocationHistoryClient$LocationHistoryCallback;

    new-instance v0, Lcom/google/android/gsf/settings/LocationHistoryClient$QueryLocationHistoryTask;

    invoke-direct {v0, p0, p2}, Lcom/google/android/gsf/settings/LocationHistoryClient$QueryLocationHistoryTask;-><init>(Lcom/google/android/gsf/settings/LocationHistoryClient;Lcom/google/android/gsf/settings/LocationHistoryClient$LocationHistoryCallback;)V

    const/4 v1, 0x1

    new-array v1, v1, [[Landroid/accounts/Account;

    const/4 v2, 0x0

    aput-object p1, v1, v2

    invoke-virtual {v0, v1}, Lcom/google/android/gsf/settings/LocationHistoryClient$QueryLocationHistoryTask;->execute([Ljava/lang/Object;)Landroid/os/AsyncTask;

    return-void
.end method
