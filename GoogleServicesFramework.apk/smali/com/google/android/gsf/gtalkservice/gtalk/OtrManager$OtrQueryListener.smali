.class Lcom/google/android/gsf/gtalkservice/gtalk/OtrManager$OtrQueryListener;
.super Ljava/lang/Object;
.source "OtrManager.java"

# interfaces
.implements Lorg/jivesoftware/smack/PacketListener;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/google/android/gsf/gtalkservice/gtalk/OtrManager;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x2
    name = "OtrQueryListener"
.end annotation


# instance fields
.field final synthetic this$0:Lcom/google/android/gsf/gtalkservice/gtalk/OtrManager;


# direct methods
.method private constructor <init>(Lcom/google/android/gsf/gtalkservice/gtalk/OtrManager;)V
    .locals 0

    iput-object p1, p0, Lcom/google/android/gsf/gtalkservice/gtalk/OtrManager$OtrQueryListener;->this$0:Lcom/google/android/gsf/gtalkservice/gtalk/OtrManager;

    invoke-direct/range {p0 .. p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method synthetic constructor <init>(Lcom/google/android/gsf/gtalkservice/gtalk/OtrManager;Lcom/google/android/gsf/gtalkservice/gtalk/OtrManager$1;)V
    .locals 0
    .param p1    # Lcom/google/android/gsf/gtalkservice/gtalk/OtrManager;
    .param p2    # Lcom/google/android/gsf/gtalkservice/gtalk/OtrManager$1;

    invoke-direct {p0, p1}, Lcom/google/android/gsf/gtalkservice/gtalk/OtrManager$OtrQueryListener;-><init>(Lcom/google/android/gsf/gtalkservice/gtalk/OtrManager;)V

    return-void
.end method

.method private processOtrQueryResult(Lcom/google/android/gsf/gtalkservice/extensions/OtrQuery;)V
    .locals 15
    .param p1    # Lcom/google/android/gsf/gtalkservice/extensions/OtrQuery;

    invoke-virtual/range {p1 .. p1}, Lcom/google/android/gsf/gtalkservice/extensions/OtrQuery;->getDefaultValue()Lcom/google/android/gsf/gtalkservice/extensions/OtrQuery$DefaultValue;

    move-result-object v0

    sget-object v12, Lcom/google/android/gsf/gtalkservice/gtalk/OtrManager$3;->$SwitchMap$com$google$android$gsf$gtalkservice$extensions$OtrQuery$DefaultValue:[I

    invoke-virtual {v0}, Lcom/google/android/gsf/gtalkservice/extensions/OtrQuery$DefaultValue;->ordinal()I

    move-result v13

    aget v12, v12, v13

    packed-switch v12, :pswitch_data_0

    const/4 v1, 0x0

    :goto_0
    invoke-virtual/range {p1 .. p1}, Lcom/google/android/gsf/gtalkservice/extensions/OtrQuery;->getItems()Ljava/util/ArrayList;

    move-result-object v7

    new-instance v9, Ljava/lang/StringBuilder;

    invoke-direct {v9}, Ljava/lang/StringBuilder;-><init>()V

    new-instance v10, Ljava/lang/StringBuilder;

    invoke-direct {v10}, Ljava/lang/StringBuilder;-><init>()V

    const/4 v3, 0x0

    invoke-virtual {v7}, Ljava/util/ArrayList;->iterator()Ljava/util/Iterator;

    move-result-object v5

    :cond_0
    :goto_1
    invoke-interface {v5}, Ljava/util/Iterator;->hasNext()Z

    move-result v12

    if-eqz v12, :cond_2

    invoke-interface {v5}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v6

    check-cast v6, Lcom/google/android/gsf/gtalkservice/extensions/OtrQuery$Item;

    invoke-virtual {v6}, Lcom/google/android/gsf/gtalkservice/extensions/OtrQuery$Item;->isEnabled()Z

    move-result v2

    if-eq v2, v1, :cond_0

    add-int/lit8 v4, v3, 0x1

    if-lez v3, :cond_1

    const-string v12, " OR "

    invoke-virtual {v9, v12}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    const-string v12, " AND "

    invoke-virtual {v10, v12}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    :cond_1
    const-string v12, "username"

    invoke-virtual {v9, v12}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v12

    const-string v13, "=?"

    invoke-virtual {v12, v13}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    const-string v12, "username"

    invoke-virtual {v10, v12}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v12

    const-string v13, "!=?"

    invoke-virtual {v12, v13}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move v3, v4

    goto :goto_1

    :pswitch_0
    const/4 v1, 0x1

    goto :goto_0

    :cond_2
    new-array v11, v3, [Ljava/lang/String;

    const/4 v3, 0x0

    invoke-virtual {v7}, Ljava/util/ArrayList;->iterator()Ljava/util/Iterator;

    move-result-object v5

    :cond_3
    :goto_2
    invoke-interface {v5}, Ljava/util/Iterator;->hasNext()Z

    move-result v12

    if-eqz v12, :cond_4

    invoke-interface {v5}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v6

    check-cast v6, Lcom/google/android/gsf/gtalkservice/extensions/OtrQuery$Item;

    invoke-virtual {v6}, Lcom/google/android/gsf/gtalkservice/extensions/OtrQuery$Item;->getJid()Ljava/lang/String;

    move-result-object v8

    invoke-virtual {v6}, Lcom/google/android/gsf/gtalkservice/extensions/OtrQuery$Item;->isEnabled()Z

    move-result v2

    if-eq v2, v1, :cond_3

    add-int/lit8 v4, v3, 0x1

    aput-object v8, v11, v3

    move v3, v4

    goto :goto_2

    :cond_4
    if-lez v3, :cond_5

    iget-object v12, p0, Lcom/google/android/gsf/gtalkservice/gtalk/OtrManager$OtrQueryListener;->this$0:Lcom/google/android/gsf/gtalkservice/gtalk/OtrManager;

    # getter for: Lcom/google/android/gsf/gtalkservice/gtalk/OtrManager;->mResolver:Landroid/content/ContentResolver;
    invoke-static {v12}, Lcom/google/android/gsf/gtalkservice/gtalk/OtrManager;->access$200(Lcom/google/android/gsf/gtalkservice/gtalk/OtrManager;)Landroid/content/ContentResolver;

    move-result-object v13

    if-eqz v1, :cond_6

    const/4 v12, 0x0

    :goto_3
    invoke-virtual {v9}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v14

    invoke-static {v13, v12, v14, v11}, Lcom/google/android/gsf/gtalkservice/gtalk/DatabaseHelper;->updateOtrForContacts(Landroid/content/ContentResolver;ILjava/lang/String;[Ljava/lang/String;)V

    :cond_5
    iget-object v12, p0, Lcom/google/android/gsf/gtalkservice/gtalk/OtrManager$OtrQueryListener;->this$0:Lcom/google/android/gsf/gtalkservice/gtalk/OtrManager;

    # getter for: Lcom/google/android/gsf/gtalkservice/gtalk/OtrManager;->mResolver:Landroid/content/ContentResolver;
    invoke-static {v12}, Lcom/google/android/gsf/gtalkservice/gtalk/OtrManager;->access$200(Lcom/google/android/gsf/gtalkservice/gtalk/OtrManager;)Landroid/content/ContentResolver;

    move-result-object v13

    if-eqz v1, :cond_7

    const/4 v12, 0x1

    :goto_4
    invoke-virtual {v10}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v14

    invoke-static {v13, v12, v14, v11}, Lcom/google/android/gsf/gtalkservice/gtalk/DatabaseHelper;->updateOtrForContacts(Landroid/content/ContentResolver;ILjava/lang/String;[Ljava/lang/String;)V

    return-void

    :cond_6
    const/4 v12, 0x1

    goto :goto_3

    :cond_7
    const/4 v12, 0x0

    goto :goto_4

    nop

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
    .end packed-switch
.end method

.method private processOtrQuerySet(Lcom/google/android/gsf/gtalkservice/extensions/OtrQuery;)V
    .locals 15
    .param p1    # Lcom/google/android/gsf/gtalkservice/extensions/OtrQuery;

    invoke-virtual/range {p1 .. p1}, Lcom/google/android/gsf/gtalkservice/extensions/OtrQuery;->getItems()Ljava/util/ArrayList;

    move-result-object v5

    const/4 v12, 0x3

    new-array v7, v12, [Ljava/lang/StringBuilder;

    const/4 v12, 0x3

    new-array v11, v12, [I

    invoke-virtual {v5}, Ljava/util/ArrayList;->iterator()Ljava/util/Iterator;

    move-result-object v2

    :goto_0
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    move-result v12

    if-eqz v12, :cond_4

    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v4

    check-cast v4, Lcom/google/android/gsf/gtalkservice/extensions/OtrQuery$Item;

    invoke-virtual {v4}, Lcom/google/android/gsf/gtalkservice/extensions/OtrQuery$Item;->getJid()Ljava/lang/String;

    move-result-object v6

    invoke-virtual {v4}, Lcom/google/android/gsf/gtalkservice/extensions/OtrQuery$Item;->isEnabled()Z

    move-result v1

    invoke-virtual {v4}, Lcom/google/android/gsf/gtalkservice/extensions/OtrQuery$Item;->getChangedByBuddy()Z

    move-result v0

    if-eqz v1, :cond_3

    if-eqz v0, :cond_2

    const/4 v3, 0x2

    :goto_1
    aget-object v12, v7, v3

    if-nez v12, :cond_0

    new-instance v12, Ljava/lang/StringBuilder;

    invoke-direct {v12}, Ljava/lang/StringBuilder;-><init>()V

    aput-object v12, v7, v3

    :cond_0
    aget v12, v11, v3

    add-int/lit8 v13, v12, 0x1

    aput v13, v11, v3

    if-lez v12, :cond_1

    aget-object v12, v7, v3

    const-string v13, " OR "

    invoke-virtual {v12, v13}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    :cond_1
    aget-object v12, v7, v3

    const-string v13, "username"

    invoke-virtual {v12, v13}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v12

    const-string v13, "=?"

    invoke-virtual {v12, v13}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    goto :goto_0

    :cond_2
    const/4 v3, 0x1

    goto :goto_1

    :cond_3
    const/4 v3, 0x0

    goto :goto_1

    :cond_4
    const/4 v8, 0x0

    const/4 v9, 0x0

    const/4 v10, 0x0

    const/4 v12, 0x0

    aget v12, v11, v12

    if-lez v12, :cond_5

    const/4 v12, 0x0

    aget v12, v11, v12

    new-array v8, v12, [Ljava/lang/String;

    :cond_5
    const/4 v12, 0x1

    aget v12, v11, v12

    if-lez v12, :cond_6

    const/4 v12, 0x1

    aget v12, v11, v12

    new-array v9, v12, [Ljava/lang/String;

    :cond_6
    const/4 v12, 0x2

    aget v12, v11, v12

    if-lez v12, :cond_7

    const/4 v12, 0x2

    aget v12, v11, v12

    new-array v10, v12, [Ljava/lang/String;

    :cond_7
    const/4 v12, 0x0

    const/4 v13, 0x0

    aput v13, v11, v12

    const/4 v12, 0x1

    const/4 v13, 0x0

    aput v13, v11, v12

    const/4 v12, 0x2

    const/4 v13, 0x0

    aput v13, v11, v12

    invoke-virtual {v5}, Ljava/util/ArrayList;->iterator()Ljava/util/Iterator;

    move-result-object v2

    :goto_2
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    move-result v12

    if-eqz v12, :cond_a

    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v4

    check-cast v4, Lcom/google/android/gsf/gtalkservice/extensions/OtrQuery$Item;

    invoke-virtual {v4}, Lcom/google/android/gsf/gtalkservice/extensions/OtrQuery$Item;->getJid()Ljava/lang/String;

    move-result-object v6

    invoke-virtual {v4}, Lcom/google/android/gsf/gtalkservice/extensions/OtrQuery$Item;->isEnabled()Z

    move-result v1

    if-eqz v1, :cond_9

    invoke-virtual {v4}, Lcom/google/android/gsf/gtalkservice/extensions/OtrQuery$Item;->getChangedByBuddy()Z

    move-result v0

    if-eqz v0, :cond_8

    const/4 v12, 0x2

    aget v13, v11, v12

    add-int/lit8 v14, v13, 0x1

    aput v14, v11, v12

    aput-object v6, v10, v13

    goto :goto_2

    :cond_8
    const/4 v12, 0x1

    aget v13, v11, v12

    add-int/lit8 v14, v13, 0x1

    aput v14, v11, v12

    aput-object v6, v9, v13

    goto :goto_2

    :cond_9
    const/4 v12, 0x0

    aget v13, v11, v12

    add-int/lit8 v14, v13, 0x1

    aput v14, v11, v12

    aput-object v6, v8, v13

    goto :goto_2

    :cond_a
    const/4 v12, 0x0

    aget v12, v11, v12

    if-lez v12, :cond_b

    iget-object v12, p0, Lcom/google/android/gsf/gtalkservice/gtalk/OtrManager$OtrQueryListener;->this$0:Lcom/google/android/gsf/gtalkservice/gtalk/OtrManager;

    # getter for: Lcom/google/android/gsf/gtalkservice/gtalk/OtrManager;->mResolver:Landroid/content/ContentResolver;
    invoke-static {v12}, Lcom/google/android/gsf/gtalkservice/gtalk/OtrManager;->access$200(Lcom/google/android/gsf/gtalkservice/gtalk/OtrManager;)Landroid/content/ContentResolver;

    move-result-object v12

    const/4 v13, 0x0

    const/4 v14, 0x0

    aget-object v14, v7, v14

    invoke-virtual {v14}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v14

    invoke-static {v12, v13, v14, v8}, Lcom/google/android/gsf/gtalkservice/gtalk/DatabaseHelper;->updateOtrForContacts(Landroid/content/ContentResolver;ILjava/lang/String;[Ljava/lang/String;)V

    :cond_b
    const/4 v12, 0x1

    aget v12, v11, v12

    if-lez v12, :cond_c

    iget-object v12, p0, Lcom/google/android/gsf/gtalkservice/gtalk/OtrManager$OtrQueryListener;->this$0:Lcom/google/android/gsf/gtalkservice/gtalk/OtrManager;

    # getter for: Lcom/google/android/gsf/gtalkservice/gtalk/OtrManager;->mResolver:Landroid/content/ContentResolver;
    invoke-static {v12}, Lcom/google/android/gsf/gtalkservice/gtalk/OtrManager;->access$200(Lcom/google/android/gsf/gtalkservice/gtalk/OtrManager;)Landroid/content/ContentResolver;

    move-result-object v12

    const/4 v13, 0x2

    const/4 v14, 0x1

    aget-object v14, v7, v14

    invoke-virtual {v14}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v14

    invoke-static {v12, v13, v14, v9}, Lcom/google/android/gsf/gtalkservice/gtalk/DatabaseHelper;->updateOtrForContacts(Landroid/content/ContentResolver;ILjava/lang/String;[Ljava/lang/String;)V

    :cond_c
    const/4 v12, 0x2

    aget v12, v11, v12

    if-lez v12, :cond_d

    iget-object v12, p0, Lcom/google/android/gsf/gtalkservice/gtalk/OtrManager$OtrQueryListener;->this$0:Lcom/google/android/gsf/gtalkservice/gtalk/OtrManager;

    # getter for: Lcom/google/android/gsf/gtalkservice/gtalk/OtrManager;->mResolver:Landroid/content/ContentResolver;
    invoke-static {v12}, Lcom/google/android/gsf/gtalkservice/gtalk/OtrManager;->access$200(Lcom/google/android/gsf/gtalkservice/gtalk/OtrManager;)Landroid/content/ContentResolver;

    move-result-object v12

    const/4 v13, 0x3

    const/4 v14, 0x2

    aget-object v14, v7, v14

    invoke-virtual {v14}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v14

    invoke-static {v12, v13, v14, v10}, Lcom/google/android/gsf/gtalkservice/gtalk/DatabaseHelper;->updateOtrForContacts(Landroid/content/ContentResolver;ILjava/lang/String;[Ljava/lang/String;)V

    :cond_d
    return-void
.end method


# virtual methods
.method public processPacket(Lorg/jivesoftware/smack/packet/Packet;)V
    .locals 10
    .param p1    # Lorg/jivesoftware/smack/packet/Packet;

    const/4 v3, 0x1

    const/4 v7, 0x0

    move-object v6, p1

    check-cast v6, Lcom/google/android/gsf/gtalkservice/extensions/OtrQuery;

    invoke-virtual {p1}, Lorg/jivesoftware/smack/packet/Packet;->getAccountId()J

    move-result-wide v0

    invoke-virtual {v6}, Lcom/google/android/gsf/gtalkservice/extensions/OtrQuery;->getEtag()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v6}, Lcom/google/android/gsf/gtalkservice/extensions/OtrQuery;->getNotModified()Ljava/lang/Boolean;

    move-result-object v5

    if-eqz v5, :cond_1

    invoke-virtual {v5}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v8

    if-eqz v8, :cond_1

    move v4, v3

    :goto_0
    if-eqz v4, :cond_2

    const-string v7, "GTalkService"

    const/4 v8, 0x3

    invoke-static {v7, v8}, Lcom/google/android/gsf/gtalkservice/Log;->isLoggable(Ljava/lang/String;I)Z

    move-result v7

    if-eqz v7, :cond_0

    iget-object v7, p0, Lcom/google/android/gsf/gtalkservice/gtalk/OtrManager$OtrQueryListener;->this$0:Lcom/google/android/gsf/gtalkservice/gtalk/OtrManager;

    const-string v8, "OTR not modified"

    # invokes: Lcom/google/android/gsf/gtalkservice/gtalk/OtrManager;->log(Ljava/lang/String;)V
    invoke-static {v7, v8}, Lcom/google/android/gsf/gtalkservice/gtalk/OtrManager;->access$100(Lcom/google/android/gsf/gtalkservice/gtalk/OtrManager;Ljava/lang/String;)V

    :cond_0
    :goto_1
    return-void

    :cond_1
    move v4, v7

    goto :goto_0

    :cond_2
    invoke-virtual {v6}, Lcom/google/android/gsf/gtalkservice/extensions/OtrQuery;->getType()Lorg/jivesoftware/smack/packet/IQ$Type;

    move-result-object v8

    sget-object v9, Lorg/jivesoftware/smack/packet/IQ$Type;->RESULT:Lorg/jivesoftware/smack/packet/IQ$Type;

    if-ne v8, v9, :cond_3

    :goto_2
    if-eqz v3, :cond_4

    invoke-direct {p0, v6}, Lcom/google/android/gsf/gtalkservice/gtalk/OtrManager$OtrQueryListener;->processOtrQueryResult(Lcom/google/android/gsf/gtalkservice/extensions/OtrQuery;)V

    :goto_3
    iget-object v7, p0, Lcom/google/android/gsf/gtalkservice/gtalk/OtrManager$OtrQueryListener;->this$0:Lcom/google/android/gsf/gtalkservice/gtalk/OtrManager;

    # getter for: Lcom/google/android/gsf/gtalkservice/gtalk/OtrManager;->mResolver:Landroid/content/ContentResolver;
    invoke-static {v7}, Lcom/google/android/gsf/gtalkservice/gtalk/OtrManager;->access$200(Lcom/google/android/gsf/gtalkservice/gtalk/OtrManager;)Landroid/content/ContentResolver;

    move-result-object v7

    invoke-static {v7, v0, v1, v2}, Lcom/google/android/gsf/gtalkservice/gtalk/DatabaseHelper;->setOtrEtag(Landroid/content/ContentResolver;JLjava/lang/String;)V

    goto :goto_1

    :cond_3
    move v3, v7

    goto :goto_2

    :cond_4
    invoke-direct {p0, v6}, Lcom/google/android/gsf/gtalkservice/gtalk/OtrManager$OtrQueryListener;->processOtrQuerySet(Lcom/google/android/gsf/gtalkservice/extensions/OtrQuery;)V

    goto :goto_3
.end method
