.class Lcom/google/android/gsf/gtalkservice/Endpoint$ConnectionMadeEvent;
.super Lcom/google/android/gsf/gtalkservice/Endpoint$ConnectionStateEvent;
.source "Endpoint.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/google/android/gsf/gtalkservice/Endpoint;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0xa
    name = "ConnectionMadeEvent"
.end annotation


# instance fields
.field private mHostIpAddress:Ljava/lang/String;

.field private mPresenceIsAvailable:Z

.field private mPresenceIsInvisible:Z

.field private mPresenceShow:I


# direct methods
.method constructor <init>(ILcom/google/android/gtalkservice/Presence;Ljava/lang/String;)V
    .locals 1
    .param p1    # I
    .param p2    # Lcom/google/android/gtalkservice/Presence;
    .param p3    # Ljava/lang/String;

    const/4 v0, 0x0

    invoke-direct {p0, p1}, Lcom/google/android/gsf/gtalkservice/Endpoint$ConnectionStateEvent;-><init>(I)V

    if-eqz p2, :cond_0

    invoke-virtual {p2}, Lcom/google/android/gtalkservice/Presence;->isAvailable()Z

    move-result v0

    iput-boolean v0, p0, Lcom/google/android/gsf/gtalkservice/Endpoint$ConnectionMadeEvent;->mPresenceIsAvailable:Z

    invoke-virtual {p2}, Lcom/google/android/gtalkservice/Presence;->isInvisible()Z

    move-result v0

    iput-boolean v0, p0, Lcom/google/android/gsf/gtalkservice/Endpoint$ConnectionMadeEvent;->mPresenceIsInvisible:Z

    invoke-virtual {p2}, Lcom/google/android/gtalkservice/Presence;->getShow()Lcom/google/android/gtalkservice/Presence$Show;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/android/gtalkservice/Presence$Show;->ordinal()I

    move-result v0

    iput v0, p0, Lcom/google/android/gsf/gtalkservice/Endpoint$ConnectionMadeEvent;->mPresenceShow:I

    :goto_0
    iput-object p3, p0, Lcom/google/android/gsf/gtalkservice/Endpoint$ConnectionMadeEvent;->mHostIpAddress:Ljava/lang/String;

    return-void

    :cond_0
    iput-boolean v0, p0, Lcom/google/android/gsf/gtalkservice/Endpoint$ConnectionMadeEvent;->mPresenceIsAvailable:Z

    iput-boolean v0, p0, Lcom/google/android/gsf/gtalkservice/Endpoint$ConnectionMadeEvent;->mPresenceIsInvisible:Z

    iput v0, p0, Lcom/google/android/gsf/gtalkservice/Endpoint$ConnectionMadeEvent;->mPresenceShow:I

    goto :goto_0
.end method


# virtual methods
.method dump(Ljava/io/PrintWriter;)V
    .locals 3
    .param p1    # Ljava/io/PrintWriter;

    invoke-super {p0, p1}, Lcom/google/android/gsf/gtalkservice/Endpoint$ConnectionStateEvent;->dump(Ljava/io/PrintWriter;)V

    iget-boolean v1, p0, Lcom/google/android/gsf/gtalkservice/Endpoint$ConnectionMadeEvent;->mPresenceIsAvailable:Z

    if-nez v1, :cond_1

    const-string v0, "UNAVAILABLE"

    :goto_0
    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, ", "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {p1, v1}, Ljava/io/PrintWriter;->print(Ljava/lang/String;)V

    iget-object v1, p0, Lcom/google/android/gsf/gtalkservice/Endpoint$ConnectionMadeEvent;->mHostIpAddress:Ljava/lang/String;

    if-eqz v1, :cond_0

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, ", host="

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget-object v2, p0, Lcom/google/android/gsf/gtalkservice/Endpoint$ConnectionMadeEvent;->mHostIpAddress:Ljava/lang/String;

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {p1, v1}, Ljava/io/PrintWriter;->print(Ljava/lang/String;)V

    :cond_0
    return-void

    :cond_1
    iget-boolean v1, p0, Lcom/google/android/gsf/gtalkservice/Endpoint$ConnectionMadeEvent;->mPresenceIsInvisible:Z

    if-eqz v1, :cond_2

    const-string v0, "INVISIBLE"

    goto :goto_0

    :cond_2
    iget v1, p0, Lcom/google/android/gsf/gtalkservice/Endpoint$ConnectionMadeEvent;->mPresenceShow:I

    sget-object v2, Lcom/google/android/gtalkservice/Presence$Show;->NONE:Lcom/google/android/gtalkservice/Presence$Show;

    invoke-virtual {v2}, Lcom/google/android/gtalkservice/Presence$Show;->ordinal()I

    move-result v2

    if-eq v1, v2, :cond_3

    iget v1, p0, Lcom/google/android/gsf/gtalkservice/Endpoint$ConnectionMadeEvent;->mPresenceShow:I

    sget-object v2, Lcom/google/android/gtalkservice/Presence$Show;->AVAILABLE:Lcom/google/android/gtalkservice/Presence$Show;

    invoke-virtual {v2}, Lcom/google/android/gtalkservice/Presence$Show;->ordinal()I

    move-result v2

    if-ne v1, v2, :cond_4

    :cond_3
    const-string v0, "AVAILABLE"

    goto :goto_0

    :cond_4
    iget v1, p0, Lcom/google/android/gsf/gtalkservice/Endpoint$ConnectionMadeEvent;->mPresenceShow:I

    sget-object v2, Lcom/google/android/gtalkservice/Presence$Show;->DND:Lcom/google/android/gtalkservice/Presence$Show;

    invoke-virtual {v2}, Lcom/google/android/gtalkservice/Presence$Show;->ordinal()I

    move-result v2

    if-ne v1, v2, :cond_5

    const-string v0, "DND"

    goto :goto_0

    :cond_5
    const-string v0, "AWAY"

    goto :goto_0
.end method
