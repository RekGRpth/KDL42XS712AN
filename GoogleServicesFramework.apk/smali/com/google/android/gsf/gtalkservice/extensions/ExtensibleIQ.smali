.class public Lcom/google/android/gsf/gtalkservice/extensions/ExtensibleIQ;
.super Lorg/jivesoftware/smack/packet/IQ;
.source "ExtensibleIQ.java"


# instance fields
.field private mElementName:Ljava/lang/String;

.field private mExtension:Ljava/lang/String;

.field private mNamespace:Ljava/lang/String;


# direct methods
.method public constructor <init>()V
    .locals 0

    invoke-direct {p0}, Lorg/jivesoftware/smack/packet/IQ;-><init>()V

    return-void
.end method

.method public constructor <init>(Ljava/lang/String;Ljava/lang/String;)V
    .locals 0
    .param p1    # Ljava/lang/String;
    .param p2    # Ljava/lang/String;

    invoke-direct {p0}, Lorg/jivesoftware/smack/packet/IQ;-><init>()V

    invoke-virtual {p0, p1}, Lcom/google/android/gsf/gtalkservice/extensions/ExtensibleIQ;->setElementName(Ljava/lang/String;)V

    invoke-virtual {p0, p2}, Lcom/google/android/gsf/gtalkservice/extensions/ExtensibleIQ;->setNamespace(Ljava/lang/String;)V

    return-void
.end method


# virtual methods
.method public getChildElementXML()Ljava/lang/String;
    .locals 1

    iget-object v0, p0, Lcom/google/android/gsf/gtalkservice/extensions/ExtensibleIQ;->mExtension:Ljava/lang/String;

    return-object v0
.end method

.method public getElementName()Ljava/lang/String;
    .locals 1

    iget-object v0, p0, Lcom/google/android/gsf/gtalkservice/extensions/ExtensibleIQ;->mElementName:Ljava/lang/String;

    return-object v0
.end method

.method protected getExtensionProtoBuf()Lcom/google/common/io/protocol/ProtoBuf;
    .locals 1

    invoke-super {p0}, Lorg/jivesoftware/smack/packet/IQ;->getExtensionProtoBuf()Lcom/google/common/io/protocol/ProtoBuf;

    move-result-object v0

    return-object v0
.end method

.method public getNamespace()Ljava/lang/String;
    .locals 1

    iget-object v0, p0, Lcom/google/android/gsf/gtalkservice/extensions/ExtensibleIQ;->mNamespace:Ljava/lang/String;

    return-object v0
.end method

.method public setElementName(Ljava/lang/String;)V
    .locals 0
    .param p1    # Ljava/lang/String;

    iput-object p1, p0, Lcom/google/android/gsf/gtalkservice/extensions/ExtensibleIQ;->mElementName:Ljava/lang/String;

    return-void
.end method

.method public setExtension(Ljava/lang/String;)V
    .locals 0
    .param p1    # Ljava/lang/String;

    iput-object p1, p0, Lcom/google/android/gsf/gtalkservice/extensions/ExtensibleIQ;->mExtension:Ljava/lang/String;

    return-void
.end method

.method public setNamespace(Ljava/lang/String;)V
    .locals 0
    .param p1    # Ljava/lang/String;

    iput-object p1, p0, Lcom/google/android/gsf/gtalkservice/extensions/ExtensibleIQ;->mNamespace:Ljava/lang/String;

    return-void
.end method
