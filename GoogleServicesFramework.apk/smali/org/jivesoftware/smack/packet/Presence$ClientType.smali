.class public Lorg/jivesoftware/smack/packet/Presence$ClientType;
.super Ljava/lang/Object;
.source "Presence.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lorg/jivesoftware/smack/packet/Presence;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x9
    name = "ClientType"
.end annotation


# static fields
.field public static final ANDROID:Lorg/jivesoftware/smack/packet/Presence$ClientType;

.field public static final MOBILE:Lorg/jivesoftware/smack/packet/Presence$ClientType;


# instance fields
.field private value:Ljava/lang/String;


# direct methods
.method static constructor <clinit>()V
    .locals 2

    new-instance v0, Lorg/jivesoftware/smack/packet/Presence$ClientType;

    const-string v1, "android"

    invoke-direct {v0, v1}, Lorg/jivesoftware/smack/packet/Presence$ClientType;-><init>(Ljava/lang/String;)V

    sput-object v0, Lorg/jivesoftware/smack/packet/Presence$ClientType;->ANDROID:Lorg/jivesoftware/smack/packet/Presence$ClientType;

    new-instance v0, Lorg/jivesoftware/smack/packet/Presence$ClientType;

    const-string v1, "mobile"

    invoke-direct {v0, v1}, Lorg/jivesoftware/smack/packet/Presence$ClientType;-><init>(Ljava/lang/String;)V

    sput-object v0, Lorg/jivesoftware/smack/packet/Presence$ClientType;->MOBILE:Lorg/jivesoftware/smack/packet/Presence$ClientType;

    return-void
.end method

.method private constructor <init>(Ljava/lang/String;)V
    .locals 0
    .param p1    # Ljava/lang/String;

    invoke-direct/range {p0 .. p0}, Ljava/lang/Object;-><init>()V

    iput-object p1, p0, Lorg/jivesoftware/smack/packet/Presence$ClientType;->value:Ljava/lang/String;

    return-void
.end method


# virtual methods
.method public toString()Ljava/lang/String;
    .locals 1

    iget-object v0, p0, Lorg/jivesoftware/smack/packet/Presence$ClientType;->value:Ljava/lang/String;

    return-object v0
.end method
