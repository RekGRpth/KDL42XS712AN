.class public Lnetwork/util/NIOUtils;
.super Ljava/lang/Object;
.source "NIOUtils.java"


# direct methods
.method constructor <init>()V
    .locals 0

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method public static cancelKeySilently(Ljava/nio/channels/SelectionKey;)V
    .locals 1
    .param p0    # Ljava/nio/channels/SelectionKey;

    if-eqz p0, :cond_0

    :try_start_0
    invoke-virtual {p0}, Ljava/nio/channels/SelectionKey;->cancel()V
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    :cond_0
    :goto_0
    return-void

    :catch_0
    move-exception v0

    goto :goto_0
.end method

.method public static closeChannelSilently(Ljava/nio/channels/Channel;)V
    .locals 1
    .param p0    # Ljava/nio/channels/Channel;

    if-eqz p0, :cond_0

    :try_start_0
    invoke-interface {p0}, Ljava/nio/channels/Channel;->close()V
    :try_end_0
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_0

    :cond_0
    :goto_0
    return-void

    :catch_0
    move-exception v0

    goto :goto_0
.end method

.method public static closeKeyAndChannelSilently(Ljava/nio/channels/SelectionKey;Ljava/nio/channels/Channel;)V
    .locals 0
    .param p0    # Ljava/nio/channels/SelectionKey;
    .param p1    # Ljava/nio/channels/Channel;

    invoke-static {p1}, Lnetwork/util/NIOUtils;->closeChannelSilently(Ljava/nio/channels/Channel;)V

    invoke-static {p0}, Lnetwork/util/NIOUtils;->cancelKeySilently(Ljava/nio/channels/SelectionKey;)V

    return-void
.end method

.method public static compact([Ljava/nio/ByteBuffer;)[Ljava/nio/ByteBuffer;
    .locals 4
    .param p0    # [Ljava/nio/ByteBuffer;

    const/4 v0, 0x0

    :goto_0
    array-length v2, p0

    if-lt v0, v2, :cond_1

    const/4 p0, 0x0

    :cond_0
    :goto_1
    return-object p0

    :cond_1
    aget-object v2, p0, v0

    invoke-virtual {v2}, Ljava/nio/ByteBuffer;->remaining()I

    move-result v2

    if-lez v2, :cond_2

    if-eqz v0, :cond_0

    array-length v2, p0

    sub-int/2addr v2, v0

    new-array v1, v2, [Ljava/nio/ByteBuffer;

    const/4 v2, 0x0

    array-length v3, p0

    sub-int/2addr v3, v0

    invoke-static {p0, v0, v1, v2, v3}, Ljava/lang/System;->arraycopy(Ljava/lang/Object;ILjava/lang/Object;II)V

    move-object p0, v1

    goto :goto_1

    :cond_2
    add-int/lit8 v0, v0, 0x1

    goto :goto_0
.end method

.method public static concat(Ljava/nio/ByteBuffer;[Ljava/nio/ByteBuffer;)[Ljava/nio/ByteBuffer;
    .locals 2
    .param p0    # Ljava/nio/ByteBuffer;
    .param p1    # [Ljava/nio/ByteBuffer;

    const/4 v0, 0x1

    new-array v0, v0, [Ljava/nio/ByteBuffer;

    const/4 v1, 0x0

    aput-object p0, v0, v1

    invoke-static {v0, p1}, Lnetwork/util/NIOUtils;->concat([Ljava/nio/ByteBuffer;[Ljava/nio/ByteBuffer;)[Ljava/nio/ByteBuffer;

    move-result-object v0

    return-object v0
.end method

.method public static concat([Ljava/nio/ByteBuffer;Ljava/nio/ByteBuffer;)[Ljava/nio/ByteBuffer;
    .locals 2
    .param p0    # [Ljava/nio/ByteBuffer;
    .param p1    # Ljava/nio/ByteBuffer;

    const/4 v0, 0x1

    new-array v0, v0, [Ljava/nio/ByteBuffer;

    const/4 v1, 0x0

    aput-object p1, v0, v1

    invoke-static {p0, v0}, Lnetwork/util/NIOUtils;->concat([Ljava/nio/ByteBuffer;[Ljava/nio/ByteBuffer;)[Ljava/nio/ByteBuffer;

    move-result-object v0

    return-object v0
.end method

.method public static concat([Ljava/nio/ByteBuffer;[Ljava/nio/ByteBuffer;)[Ljava/nio/ByteBuffer;
    .locals 4
    .param p0    # [Ljava/nio/ByteBuffer;
    .param p1    # [Ljava/nio/ByteBuffer;

    const/4 v3, 0x0

    if-eqz p0, :cond_0

    array-length v1, p0

    if-nez v1, :cond_1

    :cond_0
    move-object v0, p1

    :goto_0
    return-object v0

    :cond_1
    if-eqz p1, :cond_2

    array-length v1, p1

    if-nez v1, :cond_3

    :cond_2
    move-object v0, p0

    goto :goto_0

    :cond_3
    array-length v1, p0

    array-length v2, p1

    add-int/2addr v1, v2

    new-array v0, v1, [Ljava/nio/ByteBuffer;

    array-length v1, p0

    invoke-static {p0, v3, v0, v3, v1}, Ljava/lang/System;->arraycopy(Ljava/lang/Object;ILjava/lang/Object;II)V

    array-length v1, p0

    array-length v2, p1

    invoke-static {p1, v3, v0, v1, v2}, Ljava/lang/System;->arraycopy(Ljava/lang/Object;ILjava/lang/Object;II)V

    goto :goto_0
.end method

.method public static copy(Ljava/nio/ByteBuffer;)Ljava/nio/ByteBuffer;
    .locals 2
    .param p0    # Ljava/nio/ByteBuffer;

    if-nez p0, :cond_0

    const/4 v0, 0x0

    :goto_0
    return-object v0

    :cond_0
    invoke-virtual {p0}, Ljava/nio/ByteBuffer;->remaining()I

    move-result v1

    invoke-static {v1}, Ljava/nio/ByteBuffer;->allocate(I)Ljava/nio/ByteBuffer;

    move-result-object v0

    invoke-virtual {v0, p0}, Ljava/nio/ByteBuffer;->put(Ljava/nio/ByteBuffer;)Ljava/nio/ByteBuffer;

    invoke-virtual {v0}, Ljava/nio/ByteBuffer;->flip()Ljava/nio/Buffer;

    goto :goto_0
.end method

.method public static getPacketSizeFromByteArray([BIZ)I
    .locals 6
    .param p0    # [B
    .param p1    # I
    .param p2    # Z

    const-wide/16 v1, 0x0

    if-eqz p2, :cond_2

    const/4 v0, 0x0

    :goto_0
    if-lt v0, p1, :cond_1

    :cond_0
    long-to-int v4, v1

    return v4

    :cond_1
    const/16 v4, 0x8

    shl-long/2addr v1, v4

    aget-byte v4, p0, v0

    and-int/lit16 v4, v4, 0xff

    int-to-long v4, v4

    add-long/2addr v1, v4

    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    :cond_2
    const/4 v3, 0x0

    const/4 v0, 0x0

    :goto_1
    if-ge v0, p1, :cond_0

    aget-byte v4, p0, v0

    and-int/lit16 v4, v4, 0xff

    shl-int/2addr v4, v3

    int-to-long v4, v4

    add-long/2addr v1, v4

    add-int/lit8 v3, v3, 0x8

    add-int/lit8 v0, v0, 0x1

    goto :goto_1
.end method

.method public static getPacketSizeFromByteBuffer(Ljava/nio/ByteBuffer;IZ)I
    .locals 6
    .param p0    # Ljava/nio/ByteBuffer;
    .param p1    # I
    .param p2    # Z

    const-wide/16 v1, 0x0

    if-eqz p2, :cond_2

    const/4 v0, 0x0

    :goto_0
    if-lt v0, p1, :cond_1

    :cond_0
    long-to-int v4, v1

    return v4

    :cond_1
    const/16 v4, 0x8

    shl-long/2addr v1, v4

    invoke-virtual {p0}, Ljava/nio/ByteBuffer;->get()B

    move-result v4

    and-int/lit16 v4, v4, 0xff

    int-to-long v4, v4

    add-long/2addr v1, v4

    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    :cond_2
    const/4 v3, 0x0

    const/4 v0, 0x0

    :goto_1
    if-ge v0, p1, :cond_0

    invoke-virtual {p0}, Ljava/nio/ByteBuffer;->get()B

    move-result v4

    and-int/lit16 v4, v4, 0xff

    shl-int/2addr v4, v3

    int-to-long v4, v4

    add-long/2addr v1, v4

    add-int/lit8 v3, v3, 0x8

    add-int/lit8 v0, v0, 0x1

    goto :goto_1
.end method

.method public static isEmpty([Ljava/nio/ByteBuffer;)Z
    .locals 5
    .param p0    # [Ljava/nio/ByteBuffer;

    const/4 v1, 0x0

    array-length v3, p0

    move v2, v1

    :goto_0
    if-lt v2, v3, :cond_1

    const/4 v1, 0x1

    :cond_0
    return v1

    :cond_1
    aget-object v0, p0, v2

    invoke-virtual {v0}, Ljava/nio/ByteBuffer;->remaining()I

    move-result v4

    if-gtz v4, :cond_0

    add-int/lit8 v2, v2, 0x1

    goto :goto_0
.end method

.method public static join(Ljava/nio/ByteBuffer;Ljava/nio/ByteBuffer;)Ljava/nio/ByteBuffer;
    .locals 3
    .param p0    # Ljava/nio/ByteBuffer;
    .param p1    # Ljava/nio/ByteBuffer;

    if-eqz p1, :cond_0

    invoke-virtual {p1}, Ljava/nio/ByteBuffer;->remaining()I

    move-result v1

    if-nez v1, :cond_1

    :cond_0
    invoke-static {p0}, Lnetwork/util/NIOUtils;->copy(Ljava/nio/ByteBuffer;)Ljava/nio/ByteBuffer;

    move-result-object v0

    :goto_0
    return-object v0

    :cond_1
    if-eqz p0, :cond_2

    invoke-virtual {p0}, Ljava/nio/ByteBuffer;->remaining()I

    move-result v1

    if-nez v1, :cond_3

    :cond_2
    invoke-static {p1}, Lnetwork/util/NIOUtils;->copy(Ljava/nio/ByteBuffer;)Ljava/nio/ByteBuffer;

    move-result-object v0

    goto :goto_0

    :cond_3
    invoke-virtual {p0}, Ljava/nio/ByteBuffer;->remaining()I

    move-result v1

    invoke-virtual {p1}, Ljava/nio/ByteBuffer;->remaining()I

    move-result v2

    add-int/2addr v1, v2

    invoke-static {v1}, Ljava/nio/ByteBuffer;->allocate(I)Ljava/nio/ByteBuffer;

    move-result-object v0

    invoke-virtual {v0, p0}, Ljava/nio/ByteBuffer;->put(Ljava/nio/ByteBuffer;)Ljava/nio/ByteBuffer;

    invoke-virtual {v0, p1}, Ljava/nio/ByteBuffer;->put(Ljava/nio/ByteBuffer;)Ljava/nio/ByteBuffer;

    invoke-virtual {v0}, Ljava/nio/ByteBuffer;->flip()Ljava/nio/Buffer;

    goto :goto_0
.end method

.method public static remaining([Ljava/nio/ByteBuffer;)J
    .locals 7
    .param p0    # [Ljava/nio/ByteBuffer;

    const-wide/16 v1, 0x0

    array-length v4, p0

    const/4 v3, 0x0

    :goto_0
    if-lt v3, v4, :cond_0

    return-wide v1

    :cond_0
    aget-object v0, p0, v3

    invoke-virtual {v0}, Ljava/nio/ByteBuffer;->remaining()I

    move-result v5

    int-to-long v5, v5

    add-long/2addr v1, v5

    add-int/lit8 v3, v3, 0x1

    goto :goto_0
.end method

.method public static setHeaderForPacketSize([BIIZ)V
    .locals 5
    .param p0    # [B
    .param p1    # I
    .param p2    # I
    .param p3    # Z

    if-gez p2, :cond_0

    new-instance v2, Ljava/lang/IllegalArgumentException;

    const-string v3, "Payload size is less than 0."

    invoke-direct {v2, v3}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v2

    :cond_0
    const/4 v2, 0x4

    if-eq p1, v2, :cond_1

    mul-int/lit8 v2, p1, 0x8

    shr-int v2, p2, v2

    if-lez v2, :cond_1

    new-instance v2, Ljava/lang/IllegalArgumentException;

    new-instance v3, Ljava/lang/StringBuilder;

    const-string v4, "Payload size cannot be encoded into "

    invoke-direct {v3, v4}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v3, p1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v3

    const-string v4, " byte(s)."

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-direct {v2, v3}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v2

    :cond_1
    const/4 v0, 0x0

    :goto_0
    if-lt v0, p1, :cond_2

    return-void

    :cond_2
    if-eqz p3, :cond_3

    add-int/lit8 v2, p1, -0x1

    sub-int v1, v2, v0

    :goto_1
    mul-int/lit8 v2, v1, 0x8

    shr-int v2, p2, v2

    and-int/lit16 v2, v2, 0xff

    int-to-byte v2, v2

    aput-byte v2, p0, v0

    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    :cond_3
    move v1, v0

    goto :goto_1
.end method

.method public static setPacketSizeInByteBuffer(Ljava/nio/ByteBuffer;IIZ)V
    .locals 5
    .param p0    # Ljava/nio/ByteBuffer;
    .param p1    # I
    .param p2    # I
    .param p3    # Z

    if-gez p2, :cond_0

    new-instance v2, Ljava/lang/IllegalArgumentException;

    const-string v3, "Payload size is less than 0."

    invoke-direct {v2, v3}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v2

    :cond_0
    const/4 v2, 0x4

    if-eq p1, v2, :cond_1

    mul-int/lit8 v2, p1, 0x8

    shr-int v2, p2, v2

    if-lez v2, :cond_1

    new-instance v2, Ljava/lang/IllegalArgumentException;

    new-instance v3, Ljava/lang/StringBuilder;

    const-string v4, "Payload size cannot be encoded into "

    invoke-direct {v3, v4}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v3, p1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v3

    const-string v4, " byte(s)."

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-direct {v2, v3}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v2

    :cond_1
    const/4 v0, 0x0

    :goto_0
    if-lt v0, p1, :cond_2

    return-void

    :cond_2
    if-eqz p3, :cond_3

    add-int/lit8 v2, p1, -0x1

    sub-int v1, v2, v0

    :goto_1
    mul-int/lit8 v2, v1, 0x8

    shr-int v2, p2, v2

    and-int/lit16 v2, v2, 0xff

    int-to-byte v2, v2

    invoke-virtual {p0, v2}, Ljava/nio/ByteBuffer;->put(B)Ljava/nio/ByteBuffer;

    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    :cond_3
    move v1, v0

    goto :goto_1
.end method
