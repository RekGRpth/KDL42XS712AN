.class public Lnetwork/NetCore/asyncService;
.super Ljava/lang/Object;
.source "asyncService.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lnetwork/NetCore/asyncService$RegisterChannelEvent;,
        Lnetwork/NetCore/asyncService$ShutdownEvent;
    }
.end annotation


# static fields
.field public static final DEFAULT_IO_BUFFER_SIZE:I = 0x10000


# instance fields
.field private m_exceptionObserver:Lnetwork/Interface/ExceptionObserver;

.field private final m_internalEventQueue:Ljava/util/Queue;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Queue",
            "<",
            "Ljava/lang/Runnable;",
            ">;"
        }
    .end annotation
.end field

.field private final m_selector:Ljava/nio/channels/Selector;

.field private m_sharedBuffer:Ljava/nio/ByteBuffer;


# direct methods
.method public constructor <init>()V
    .locals 1
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    const/high16 v0, 0x10000

    invoke-direct {p0, v0}, Lnetwork/NetCore/asyncService;-><init>(I)V

    return-void
.end method

.method public constructor <init>(I)V
    .locals 1
    .param p1    # I
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    invoke-static {}, Ljava/nio/channels/Selector;->open()Ljava/nio/channels/Selector;

    move-result-object v0

    iput-object v0, p0, Lnetwork/NetCore/asyncService;->m_selector:Ljava/nio/channels/Selector;

    new-instance v0, Ljava/util/concurrent/ConcurrentLinkedQueue;

    invoke-direct {v0}, Ljava/util/concurrent/ConcurrentLinkedQueue;-><init>()V

    iput-object v0, p0, Lnetwork/NetCore/asyncService;->m_internalEventQueue:Ljava/util/Queue;

    sget-object v0, Lnetwork/Interface/ExceptionObserver;->DEFAULT:Lnetwork/Interface/ExceptionObserver;

    iput-object v0, p0, Lnetwork/NetCore/asyncService;->m_exceptionObserver:Lnetwork/Interface/ExceptionObserver;

    invoke-virtual {p0, p1}, Lnetwork/NetCore/asyncService;->setBufferSize(I)V

    return-void
.end method

.method static synthetic access$0(Lnetwork/NetCore/asyncService;)Ljava/nio/channels/Selector;
    .locals 1

    iget-object v0, p0, Lnetwork/NetCore/asyncService;->m_selector:Ljava/nio/channels/Selector;

    return-object v0
.end method

.method static synthetic access$1(Lnetwork/NetCore/asyncService;Lnetwork/Interface/ExceptionObserver;)V
    .locals 0

    iput-object p1, p0, Lnetwork/NetCore/asyncService;->m_exceptionObserver:Lnetwork/Interface/ExceptionObserver;

    return-void
.end method

.method private executeQueue()V
    .locals 3

    :goto_0
    iget-object v2, p0, Lnetwork/NetCore/asyncService;->m_internalEventQueue:Ljava/util/Queue;

    invoke-interface {v2}, Ljava/util/Queue;->poll()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/Runnable;

    if-nez v0, :cond_0

    return-void

    :cond_0
    :try_start_0
    invoke-interface {v0}, Ljava/lang/Runnable;->run()V
    :try_end_0
    .catch Ljava/lang/Throwable; {:try_start_0 .. :try_end_0} :catch_0

    goto :goto_0

    :catch_0
    move-exception v1

    invoke-virtual {p0, v1}, Lnetwork/NetCore/asyncService;->notifyException(Ljava/lang/Throwable;)V

    goto :goto_0
.end method

.method private handleKey(Ljava/nio/channels/SelectionKey;)V
    .locals 3
    .param p1    # Ljava/nio/channels/SelectionKey;

    invoke-virtual {p1}, Ljava/nio/channels/SelectionKey;->attachment()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lnetwork/InterfaceImpl/ChannelResponder;

    :try_start_0
    invoke-virtual {p1}, Ljava/nio/channels/SelectionKey;->isReadable()Z

    move-result v2

    if-eqz v2, :cond_0

    invoke-virtual {v1}, Lnetwork/InterfaceImpl/ChannelResponder;->socketReadyForRead()V

    :cond_0
    invoke-virtual {p1}, Ljava/nio/channels/SelectionKey;->isWritable()Z

    move-result v2

    if-eqz v2, :cond_1

    invoke-virtual {v1}, Lnetwork/InterfaceImpl/ChannelResponder;->socketReadyForWrite()V

    :cond_1
    invoke-virtual {p1}, Ljava/nio/channels/SelectionKey;->isAcceptable()Z

    move-result v2

    if-eqz v2, :cond_2

    invoke-virtual {v1}, Lnetwork/InterfaceImpl/ChannelResponder;->socketReadyForAccept()V

    :cond_2
    invoke-virtual {p1}, Ljava/nio/channels/SelectionKey;->isConnectable()Z

    move-result v2

    if-eqz v2, :cond_3

    invoke-virtual {v1}, Lnetwork/InterfaceImpl/ChannelResponder;->socketReadyForConnect()V
    :try_end_0
    .catch Ljava/nio/channels/CancelledKeyException; {:try_start_0 .. :try_end_0} :catch_0

    :cond_3
    :goto_0
    return-void

    :catch_0
    move-exception v0

    invoke-virtual {v1, v0}, Lnetwork/InterfaceImpl/ChannelResponder;->close(Ljava/lang/Exception;)V

    goto :goto_0
.end method

.method private handleSelectedKeys()V
    .locals 4

    iget-object v3, p0, Lnetwork/NetCore/asyncService;->m_selector:Ljava/nio/channels/Selector;

    invoke-virtual {v3}, Ljava/nio/channels/Selector;->selectedKeys()Ljava/util/Set;

    move-result-object v3

    invoke-interface {v3}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    move-result-object v0

    :goto_0
    invoke-interface {v0}, Ljava/util/Iterator;->hasNext()Z

    move-result v3

    if-nez v3, :cond_0

    return-void

    :cond_0
    invoke-interface {v0}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Ljava/nio/channels/SelectionKey;

    invoke-interface {v0}, Ljava/util/Iterator;->remove()V

    :try_start_0
    invoke-direct {p0, v1}, Lnetwork/NetCore/asyncService;->handleKey(Ljava/nio/channels/SelectionKey;)V
    :try_end_0
    .catch Ljava/lang/Throwable; {:try_start_0 .. :try_end_0} :catch_0

    goto :goto_0

    :catch_0
    move-exception v2

    invoke-virtual {p0, v2}, Lnetwork/NetCore/asyncService;->notifyException(Ljava/lang/Throwable;)V

    goto :goto_0
.end method


# virtual methods
.method public close()V
    .locals 2

    invoke-virtual {p0}, Lnetwork/NetCore/asyncService;->isOpen()Z

    move-result v0

    if-nez v0, :cond_0

    :goto_0
    return-void

    :cond_0
    new-instance v0, Lnetwork/NetCore/asyncService$ShutdownEvent;

    const/4 v1, 0x0

    invoke-direct {v0, p0, v1}, Lnetwork/NetCore/asyncService$ShutdownEvent;-><init>(Lnetwork/NetCore/asyncService;Lnetwork/NetCore/asyncService$ShutdownEvent;)V

    invoke-virtual {p0, v0}, Lnetwork/NetCore/asyncService;->queue(Ljava/lang/Runnable;)V

    goto :goto_0
.end method

.method public getBufferSize()I
    .locals 1

    iget-object v0, p0, Lnetwork/NetCore/asyncService;->m_sharedBuffer:Ljava/nio/ByteBuffer;

    invoke-virtual {v0}, Ljava/nio/ByteBuffer;->capacity()I

    move-result v0

    return v0
.end method

.method public getQueue()Ljava/util/Queue;
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/Queue",
            "<",
            "Ljava/lang/Runnable;",
            ">;"
        }
    .end annotation

    new-instance v0, Ljava/util/LinkedList;

    iget-object v1, p0, Lnetwork/NetCore/asyncService;->m_internalEventQueue:Ljava/util/Queue;

    invoke-direct {v0, v1}, Ljava/util/LinkedList;-><init>(Ljava/util/Collection;)V

    return-object v0
.end method

.method public getSharedBuffer()Ljava/nio/ByteBuffer;
    .locals 1

    iget-object v0, p0, Lnetwork/NetCore/asyncService;->m_sharedBuffer:Ljava/nio/ByteBuffer;

    return-object v0
.end method

.method public isOpen()Z
    .locals 1

    iget-object v0, p0, Lnetwork/NetCore/asyncService;->m_selector:Ljava/nio/channels/Selector;

    invoke-virtual {v0}, Ljava/nio/channels/Selector;->isOpen()Z

    move-result v0

    return v0
.end method

.method public notifyException(Ljava/lang/Throwable;)V
    .locals 3
    .param p1    # Ljava/lang/Throwable;

    :try_start_0
    iget-object v1, p0, Lnetwork/NetCore/asyncService;->m_exceptionObserver:Lnetwork/Interface/ExceptionObserver;

    invoke-interface {v1, p1}, Lnetwork/Interface/ExceptionObserver;->notifyExceptionThrown(Ljava/lang/Throwable;)V
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    :goto_0
    return-void

    :catch_0
    move-exception v0

    sget-object v1, Ljava/lang/System;->err:Ljava/io/PrintStream;

    const-string v2, "Failed to log the following exception to the exception observer:"

    invoke-virtual {v1, v2}, Ljava/io/PrintStream;->println(Ljava/lang/String;)V

    sget-object v1, Ljava/lang/System;->err:Ljava/io/PrintStream;

    invoke-virtual {v1, v0}, Ljava/io/PrintStream;->println(Ljava/lang/Object;)V

    invoke-virtual {v0}, Ljava/lang/Exception;->printStackTrace()V

    goto :goto_0
.end method

.method public openSSLServerSocket(Ljavax/net/ssl/SSLContext;I)Lnetwork/Interface/asyncServerSocketSSL;
    .locals 1
    .param p1    # Ljavax/net/ssl/SSLContext;
    .param p2    # I
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    const/4 v0, -0x1

    invoke-virtual {p0, p1, p2, v0}, Lnetwork/NetCore/asyncService;->openSSLServerSocket(Ljavax/net/ssl/SSLContext;II)Lnetwork/Interface/asyncServerSocketSSL;

    move-result-object v0

    return-object v0
.end method

.method public openSSLServerSocket(Ljavax/net/ssl/SSLContext;II)Lnetwork/Interface/asyncServerSocketSSL;
    .locals 1
    .param p1    # Ljavax/net/ssl/SSLContext;
    .param p2    # I
    .param p3    # I
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    new-instance v0, Ljava/net/InetSocketAddress;

    invoke-direct {v0, p2}, Ljava/net/InetSocketAddress;-><init>(I)V

    invoke-virtual {p0, p1, v0, p3}, Lnetwork/NetCore/asyncService;->openSSLServerSocket(Ljavax/net/ssl/SSLContext;Ljava/net/InetSocketAddress;I)Lnetwork/Interface/asyncServerSocketSSL;

    move-result-object v0

    return-object v0
.end method

.method public openSSLServerSocket(Ljavax/net/ssl/SSLContext;Ljava/net/InetSocketAddress;I)Lnetwork/Interface/asyncServerSocketSSL;
    .locals 4
    .param p1    # Ljavax/net/ssl/SSLContext;
    .param p2    # Ljava/net/InetSocketAddress;
    .param p3    # I
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    invoke-static {}, Ljava/nio/channels/ServerSocketChannel;->open()Ljava/nio/channels/ServerSocketChannel;

    move-result-object v0

    invoke-virtual {v0}, Ljava/nio/channels/ServerSocketChannel;->socket()Ljava/net/ServerSocket;

    move-result-object v2

    const/4 v3, 0x1

    invoke-virtual {v2, v3}, Ljava/net/ServerSocket;->setReuseAddress(Z)V

    invoke-virtual {v0}, Ljava/nio/channels/ServerSocketChannel;->socket()Ljava/net/ServerSocket;

    move-result-object v2

    invoke-virtual {v2, p2, p3}, Ljava/net/ServerSocket;->bind(Ljava/net/SocketAddress;I)V

    const/4 v2, 0x0

    invoke-virtual {v0, v2}, Ljava/nio/channels/ServerSocketChannel;->configureBlocking(Z)Ljava/nio/channels/SelectableChannel;

    new-instance v1, Lnetwork/InterfaceImpl/SSLServerSocketChannelResponder;

    invoke-direct {v1, p1, p0, v0, p2}, Lnetwork/InterfaceImpl/SSLServerSocketChannelResponder;-><init>(Ljavax/net/ssl/SSLContext;Lnetwork/NetCore/asyncService;Ljava/nio/channels/ServerSocketChannel;Ljava/net/InetSocketAddress;)V

    new-instance v2, Lnetwork/NetCore/asyncService$RegisterChannelEvent;

    const/4 v3, 0x0

    invoke-direct {v2, p0, v1, v3}, Lnetwork/NetCore/asyncService$RegisterChannelEvent;-><init>(Lnetwork/NetCore/asyncService;Lnetwork/InterfaceImpl/ChannelResponder;Lnetwork/NetCore/asyncService$RegisterChannelEvent;)V

    invoke-virtual {p0, v2}, Lnetwork/NetCore/asyncService;->queue(Ljava/lang/Runnable;)V

    return-object v1
.end method

.method public openSSLSocket(Ljavax/net/ssl/SSLEngine;Ljava/lang/String;I)Lnetwork/Interface/NIOSocket;
    .locals 1
    .param p1    # Ljavax/net/ssl/SSLEngine;
    .param p2    # Ljava/lang/String;
    .param p3    # I
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    invoke-static {p2}, Ljava/net/InetAddress;->getByName(Ljava/lang/String;)Ljava/net/InetAddress;

    move-result-object v0

    invoke-virtual {p0, p1, v0, p3}, Lnetwork/NetCore/asyncService;->openSSLSocket(Ljavax/net/ssl/SSLEngine;Ljava/net/InetAddress;I)Lnetwork/Interface/NIOSocketSSL;

    move-result-object v0

    return-object v0
.end method

.method public openSSLSocket(Ljavax/net/ssl/SSLEngine;Ljava/net/InetAddress;I)Lnetwork/Interface/NIOSocketSSL;
    .locals 5
    .param p1    # Ljavax/net/ssl/SSLEngine;
    .param p2    # Ljava/net/InetAddress;
    .param p3    # I
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    invoke-static {}, Ljava/nio/channels/SocketChannel;->open()Ljava/nio/channels/SocketChannel;

    move-result-object v1

    const/4 v2, 0x0

    invoke-virtual {v1, v2}, Ljava/nio/channels/SocketChannel;->configureBlocking(Z)Ljava/nio/channels/SelectableChannel;

    new-instance v0, Ljava/net/InetSocketAddress;

    invoke-direct {v0, p2, p3}, Ljava/net/InetSocketAddress;-><init>(Ljava/net/InetAddress;I)V

    invoke-virtual {v1, v0}, Ljava/nio/channels/SocketChannel;->connect(Ljava/net/SocketAddress;)Z

    new-instance v2, Lnetwork/InterfaceImpl/SSLSocketChannelResponder;

    invoke-virtual {p0, v1, v0}, Lnetwork/NetCore/asyncService;->registerSocketChannel(Ljava/nio/channels/SocketChannel;Ljava/net/InetSocketAddress;)Lnetwork/Interface/NIOSocket;

    move-result-object v3

    const/4 v4, 0x1

    invoke-direct {v2, p0, v3, p1, v4}, Lnetwork/InterfaceImpl/SSLSocketChannelResponder;-><init>(Lnetwork/NetCore/asyncService;Lnetwork/Interface/NIOSocket;Ljavax/net/ssl/SSLEngine;Z)V

    return-object v2
.end method

.method public openServerSocket(I)Lnetwork/Interface/asyncServerSocket;
    .locals 1
    .param p1    # I
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    const/4 v0, -0x1

    invoke-virtual {p0, p1, v0}, Lnetwork/NetCore/asyncService;->openServerSocket(II)Lnetwork/Interface/asyncServerSocket;

    move-result-object v0

    return-object v0
.end method

.method public openServerSocket(II)Lnetwork/Interface/asyncServerSocket;
    .locals 1
    .param p1    # I
    .param p2    # I
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    new-instance v0, Ljava/net/InetSocketAddress;

    invoke-direct {v0, p1}, Ljava/net/InetSocketAddress;-><init>(I)V

    invoke-virtual {p0, v0, p2}, Lnetwork/NetCore/asyncService;->openServerSocket(Ljava/net/InetSocketAddress;I)Lnetwork/Interface/asyncServerSocket;

    move-result-object v0

    return-object v0
.end method

.method public openServerSocket(Ljava/net/InetSocketAddress;I)Lnetwork/Interface/asyncServerSocket;
    .locals 4
    .param p1    # Ljava/net/InetSocketAddress;
    .param p2    # I
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    invoke-static {}, Ljava/nio/channels/ServerSocketChannel;->open()Ljava/nio/channels/ServerSocketChannel;

    move-result-object v0

    invoke-virtual {v0}, Ljava/nio/channels/ServerSocketChannel;->socket()Ljava/net/ServerSocket;

    move-result-object v2

    const/4 v3, 0x1

    invoke-virtual {v2, v3}, Ljava/net/ServerSocket;->setReuseAddress(Z)V

    invoke-virtual {v0}, Ljava/nio/channels/ServerSocketChannel;->socket()Ljava/net/ServerSocket;

    move-result-object v2

    invoke-virtual {v2, p1, p2}, Ljava/net/ServerSocket;->bind(Ljava/net/SocketAddress;I)V

    const/4 v2, 0x0

    invoke-virtual {v0, v2}, Ljava/nio/channels/ServerSocketChannel;->configureBlocking(Z)Ljava/nio/channels/SelectableChannel;

    new-instance v1, Lnetwork/InterfaceImpl/ServerSocketChannelResponder;

    invoke-direct {v1, p0, v0, p1}, Lnetwork/InterfaceImpl/ServerSocketChannelResponder;-><init>(Lnetwork/NetCore/asyncService;Ljava/nio/channels/ServerSocketChannel;Ljava/net/InetSocketAddress;)V

    new-instance v2, Lnetwork/NetCore/asyncService$RegisterChannelEvent;

    const/4 v3, 0x0

    invoke-direct {v2, p0, v1, v3}, Lnetwork/NetCore/asyncService$RegisterChannelEvent;-><init>(Lnetwork/NetCore/asyncService;Lnetwork/InterfaceImpl/ChannelResponder;Lnetwork/NetCore/asyncService$RegisterChannelEvent;)V

    invoke-virtual {p0, v2}, Lnetwork/NetCore/asyncService;->queue(Ljava/lang/Runnable;)V

    return-object v1
.end method

.method public openSocket(Ljava/lang/String;I)Lnetwork/Interface/NIOSocket;
    .locals 1
    .param p1    # Ljava/lang/String;
    .param p2    # I
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    invoke-static {p1}, Ljava/net/InetAddress;->getByName(Ljava/lang/String;)Ljava/net/InetAddress;

    move-result-object v0

    invoke-virtual {p0, v0, p2}, Lnetwork/NetCore/asyncService;->openSocket(Ljava/net/InetAddress;I)Lnetwork/Interface/NIOSocket;

    move-result-object v0

    return-object v0
.end method

.method public openSocket(Ljava/net/InetAddress;I)Lnetwork/Interface/NIOSocket;
    .locals 3
    .param p1    # Ljava/net/InetAddress;
    .param p2    # I
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    invoke-static {}, Ljava/nio/channels/SocketChannel;->open()Ljava/nio/channels/SocketChannel;

    move-result-object v1

    const/4 v2, 0x0

    invoke-virtual {v1, v2}, Ljava/nio/channels/SocketChannel;->configureBlocking(Z)Ljava/nio/channels/SelectableChannel;

    new-instance v0, Ljava/net/InetSocketAddress;

    invoke-direct {v0, p1, p2}, Ljava/net/InetSocketAddress;-><init>(Ljava/net/InetAddress;I)V

    invoke-virtual {v1, v0}, Ljava/nio/channels/SocketChannel;->connect(Ljava/net/SocketAddress;)Z

    invoke-virtual {p0, v1, v0}, Lnetwork/NetCore/asyncService;->registerSocketChannel(Ljava/nio/channels/SocketChannel;Ljava/net/InetSocketAddress;)Lnetwork/Interface/NIOSocket;

    move-result-object v2

    return-object v2
.end method

.method public queue(Ljava/lang/Runnable;)V
    .locals 1
    .param p1    # Ljava/lang/Runnable;

    iget-object v0, p0, Lnetwork/NetCore/asyncService;->m_internalEventQueue:Ljava/util/Queue;

    invoke-interface {v0, p1}, Ljava/util/Queue;->add(Ljava/lang/Object;)Z

    invoke-virtual {p0}, Lnetwork/NetCore/asyncService;->wakeup()V

    return-void
.end method

.method public registerSocketChannel(Ljava/nio/channels/SocketChannel;Ljava/net/InetSocketAddress;)Lnetwork/Interface/NIOSocket;
    .locals 3
    .param p1    # Ljava/nio/channels/SocketChannel;
    .param p2    # Ljava/net/InetSocketAddress;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    const/4 v1, 0x0

    invoke-virtual {p1, v1}, Ljava/nio/channels/SocketChannel;->configureBlocking(Z)Ljava/nio/channels/SelectableChannel;

    new-instance v0, Lnetwork/InterfaceImpl/SocketChannelResponder;

    invoke-direct {v0, p0, p1, p2}, Lnetwork/InterfaceImpl/SocketChannelResponder;-><init>(Lnetwork/NetCore/asyncService;Ljava/nio/channels/SocketChannel;Ljava/net/InetSocketAddress;)V

    new-instance v1, Lnetwork/NetCore/asyncService$RegisterChannelEvent;

    const/4 v2, 0x0

    invoke-direct {v1, p0, v0, v2}, Lnetwork/NetCore/asyncService$RegisterChannelEvent;-><init>(Lnetwork/NetCore/asyncService;Lnetwork/InterfaceImpl/ChannelResponder;Lnetwork/NetCore/asyncService$RegisterChannelEvent;)V

    invoke-virtual {p0, v1}, Lnetwork/NetCore/asyncService;->queue(Ljava/lang/Runnable;)V

    return-object v0
.end method

.method public declared-synchronized selectBlocking()V
    .locals 1
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    monitor-enter p0

    :try_start_0
    invoke-direct {p0}, Lnetwork/NetCore/asyncService;->executeQueue()V

    iget-object v0, p0, Lnetwork/NetCore/asyncService;->m_selector:Ljava/nio/channels/Selector;

    invoke-virtual {v0}, Ljava/nio/channels/Selector;->select()I

    move-result v0

    if-lez v0, :cond_0

    invoke-direct {p0}, Lnetwork/NetCore/asyncService;->handleSelectedKeys()V

    :cond_0
    invoke-direct {p0}, Lnetwork/NetCore/asyncService;->executeQueue()V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    monitor-exit p0

    return-void

    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method public declared-synchronized selectBlocking(J)V
    .locals 1
    .param p1    # J
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    monitor-enter p0

    :try_start_0
    invoke-direct {p0}, Lnetwork/NetCore/asyncService;->executeQueue()V

    iget-object v0, p0, Lnetwork/NetCore/asyncService;->m_selector:Ljava/nio/channels/Selector;

    invoke-virtual {v0, p1, p2}, Ljava/nio/channels/Selector;->select(J)I

    move-result v0

    if-lez v0, :cond_0

    invoke-direct {p0}, Lnetwork/NetCore/asyncService;->handleSelectedKeys()V

    :cond_0
    invoke-direct {p0}, Lnetwork/NetCore/asyncService;->executeQueue()V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    monitor-exit p0

    return-void

    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method public declared-synchronized selectNonBlocking()V
    .locals 1
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    monitor-enter p0

    :try_start_0
    invoke-direct {p0}, Lnetwork/NetCore/asyncService;->executeQueue()V

    iget-object v0, p0, Lnetwork/NetCore/asyncService;->m_selector:Ljava/nio/channels/Selector;

    invoke-virtual {v0}, Ljava/nio/channels/Selector;->selectNow()I

    move-result v0

    if-lez v0, :cond_0

    invoke-direct {p0}, Lnetwork/NetCore/asyncService;->handleSelectedKeys()V

    :cond_0
    invoke-direct {p0}, Lnetwork/NetCore/asyncService;->executeQueue()V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    monitor-exit p0

    return-void

    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method public setBufferSize(I)V
    .locals 2
    .param p1    # I

    const/16 v0, 0x100

    if-ge p1, v0, :cond_0

    new-instance v0, Ljava/lang/IllegalArgumentException;

    const-string v1, "The buffer must at least hold 256 bytes"

    invoke-direct {v0, v1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v0

    :cond_0
    invoke-static {p1}, Ljava/nio/ByteBuffer;->allocate(I)Ljava/nio/ByteBuffer;

    move-result-object v0

    iput-object v0, p0, Lnetwork/NetCore/asyncService;->m_sharedBuffer:Ljava/nio/ByteBuffer;

    return-void
.end method

.method public setExceptionObserver(Lnetwork/Interface/ExceptionObserver;)V
    .locals 2
    .param p1    # Lnetwork/Interface/ExceptionObserver;

    if-nez p1, :cond_0

    sget-object v0, Lnetwork/Interface/ExceptionObserver;->DEFAULT:Lnetwork/Interface/ExceptionObserver;

    :goto_0
    new-instance v1, Lnetwork/NetCore/asyncService$1;

    invoke-direct {v1, p0, v0}, Lnetwork/NetCore/asyncService$1;-><init>(Lnetwork/NetCore/asyncService;Lnetwork/Interface/ExceptionObserver;)V

    invoke-virtual {p0, v1}, Lnetwork/NetCore/asyncService;->queue(Ljava/lang/Runnable;)V

    return-void

    :cond_0
    move-object v0, p1

    goto :goto_0
.end method

.method public wakeup()V
    .locals 1

    iget-object v0, p0, Lnetwork/NetCore/asyncService;->m_selector:Ljava/nio/channels/Selector;

    invoke-virtual {v0}, Ljava/nio/channels/Selector;->wakeup()Ljava/nio/channels/Selector;

    return-void
.end method
