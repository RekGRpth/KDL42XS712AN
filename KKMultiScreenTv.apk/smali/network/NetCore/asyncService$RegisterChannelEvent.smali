.class Lnetwork/NetCore/asyncService$RegisterChannelEvent;
.super Ljava/lang/Object;
.source "asyncService.java"

# interfaces
.implements Ljava/lang/Runnable;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lnetwork/NetCore/asyncService;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x2
    name = "RegisterChannelEvent"
.end annotation


# instance fields
.field private final m_channelResponder:Lnetwork/InterfaceImpl/ChannelResponder;

.field final synthetic this$0:Lnetwork/NetCore/asyncService;


# direct methods
.method private constructor <init>(Lnetwork/NetCore/asyncService;Lnetwork/InterfaceImpl/ChannelResponder;)V
    .locals 0
    .param p2    # Lnetwork/InterfaceImpl/ChannelResponder;

    iput-object p1, p0, Lnetwork/NetCore/asyncService$RegisterChannelEvent;->this$0:Lnetwork/NetCore/asyncService;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    iput-object p2, p0, Lnetwork/NetCore/asyncService$RegisterChannelEvent;->m_channelResponder:Lnetwork/InterfaceImpl/ChannelResponder;

    return-void
.end method

.method synthetic constructor <init>(Lnetwork/NetCore/asyncService;Lnetwork/InterfaceImpl/ChannelResponder;Lnetwork/NetCore/asyncService$RegisterChannelEvent;)V
    .locals 0

    invoke-direct {p0, p1, p2}, Lnetwork/NetCore/asyncService$RegisterChannelEvent;-><init>(Lnetwork/NetCore/asyncService;Lnetwork/InterfaceImpl/ChannelResponder;)V

    return-void
.end method


# virtual methods
.method public run()V
    .locals 5

    :try_start_0
    iget-object v2, p0, Lnetwork/NetCore/asyncService$RegisterChannelEvent;->m_channelResponder:Lnetwork/InterfaceImpl/ChannelResponder;

    invoke-virtual {v2}, Lnetwork/InterfaceImpl/ChannelResponder;->getChannel()Ljava/nio/channels/SelectableChannel;

    move-result-object v2

    iget-object v3, p0, Lnetwork/NetCore/asyncService$RegisterChannelEvent;->this$0:Lnetwork/NetCore/asyncService;

    # getter for: Lnetwork/NetCore/asyncService;->m_selector:Ljava/nio/channels/Selector;
    invoke-static {v3}, Lnetwork/NetCore/asyncService;->access$0(Lnetwork/NetCore/asyncService;)Ljava/nio/channels/Selector;

    move-result-object v3

    iget-object v4, p0, Lnetwork/NetCore/asyncService$RegisterChannelEvent;->m_channelResponder:Lnetwork/InterfaceImpl/ChannelResponder;

    invoke-virtual {v4}, Lnetwork/InterfaceImpl/ChannelResponder;->getChannel()Ljava/nio/channels/SelectableChannel;

    move-result-object v4

    invoke-virtual {v4}, Ljava/nio/channels/SelectableChannel;->validOps()I

    move-result v4

    invoke-virtual {v2, v3, v4}, Ljava/nio/channels/SelectableChannel;->register(Ljava/nio/channels/Selector;I)Ljava/nio/channels/SelectionKey;

    move-result-object v1

    iget-object v2, p0, Lnetwork/NetCore/asyncService$RegisterChannelEvent;->m_channelResponder:Lnetwork/InterfaceImpl/ChannelResponder;

    invoke-virtual {v2, v1}, Lnetwork/InterfaceImpl/ChannelResponder;->setKey(Ljava/nio/channels/SelectionKey;)V

    iget-object v2, p0, Lnetwork/NetCore/asyncService$RegisterChannelEvent;->m_channelResponder:Lnetwork/InterfaceImpl/ChannelResponder;

    invoke-virtual {v1, v2}, Ljava/nio/channels/SelectionKey;->attach(Ljava/lang/Object;)Ljava/lang/Object;
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    :goto_0
    return-void

    :catch_0
    move-exception v0

    iget-object v2, p0, Lnetwork/NetCore/asyncService$RegisterChannelEvent;->m_channelResponder:Lnetwork/InterfaceImpl/ChannelResponder;

    invoke-virtual {v2, v0}, Lnetwork/InterfaceImpl/ChannelResponder;->close(Ljava/lang/Exception;)V

    goto :goto_0
.end method

.method public toString()Ljava/lang/String;
    .locals 2

    new-instance v0, Ljava/lang/StringBuilder;

    const-string v1, "Register["

    invoke-direct {v0, v1}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    iget-object v1, p0, Lnetwork/NetCore/asyncService$RegisterChannelEvent;->m_channelResponder:Lnetwork/InterfaceImpl/ChannelResponder;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, "]"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method
