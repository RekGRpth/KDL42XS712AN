.class Lnetwork/NetCore/asyncService$ShutdownEvent;
.super Ljava/lang/Object;
.source "asyncService.java"

# interfaces
.implements Ljava/lang/Runnable;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lnetwork/NetCore/asyncService;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x2
    name = "ShutdownEvent"
.end annotation


# instance fields
.field final synthetic this$0:Lnetwork/NetCore/asyncService;


# direct methods
.method private constructor <init>(Lnetwork/NetCore/asyncService;)V
    .locals 0

    iput-object p1, p0, Lnetwork/NetCore/asyncService$ShutdownEvent;->this$0:Lnetwork/NetCore/asyncService;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method synthetic constructor <init>(Lnetwork/NetCore/asyncService;Lnetwork/NetCore/asyncService$ShutdownEvent;)V
    .locals 0

    invoke-direct {p0, p1}, Lnetwork/NetCore/asyncService$ShutdownEvent;-><init>(Lnetwork/NetCore/asyncService;)V

    return-void
.end method


# virtual methods
.method public run()V
    .locals 3

    iget-object v1, p0, Lnetwork/NetCore/asyncService$ShutdownEvent;->this$0:Lnetwork/NetCore/asyncService;

    invoke-virtual {v1}, Lnetwork/NetCore/asyncService;->isOpen()Z

    move-result v1

    if-nez v1, :cond_0

    :goto_0
    return-void

    :cond_0
    iget-object v1, p0, Lnetwork/NetCore/asyncService$ShutdownEvent;->this$0:Lnetwork/NetCore/asyncService;

    # getter for: Lnetwork/NetCore/asyncService;->m_selector:Ljava/nio/channels/Selector;
    invoke-static {v1}, Lnetwork/NetCore/asyncService;->access$0(Lnetwork/NetCore/asyncService;)Ljava/nio/channels/Selector;

    move-result-object v1

    invoke-virtual {v1}, Ljava/nio/channels/Selector;->keys()Ljava/util/Set;

    move-result-object v1

    invoke-interface {v1}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    move-result-object v2

    :goto_1
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    move-result v1

    if-nez v1, :cond_1

    :try_start_0
    iget-object v1, p0, Lnetwork/NetCore/asyncService$ShutdownEvent;->this$0:Lnetwork/NetCore/asyncService;

    # getter for: Lnetwork/NetCore/asyncService;->m_selector:Ljava/nio/channels/Selector;
    invoke-static {v1}, Lnetwork/NetCore/asyncService;->access$0(Lnetwork/NetCore/asyncService;)Ljava/nio/channels/Selector;

    move-result-object v1

    invoke-virtual {v1}, Ljava/nio/channels/Selector;->close()V
    :try_end_0
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_0

    goto :goto_0

    :catch_0
    move-exception v1

    goto :goto_0

    :cond_1
    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/nio/channels/SelectionKey;

    :try_start_1
    invoke-static {v0}, Lnetwork/util/NIOUtils;->cancelKeySilently(Ljava/nio/channels/SelectionKey;)V

    invoke-virtual {v0}, Ljava/nio/channels/SelectionKey;->attachment()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lnetwork/InterfaceImpl/ChannelResponder;

    invoke-virtual {v1}, Lnetwork/InterfaceImpl/ChannelResponder;->close()V
    :try_end_1
    .catch Ljava/lang/Exception; {:try_start_1 .. :try_end_1} :catch_1

    goto :goto_1

    :catch_1
    move-exception v1

    goto :goto_1
.end method
