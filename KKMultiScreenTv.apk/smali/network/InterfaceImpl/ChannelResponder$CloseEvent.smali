.class Lnetwork/InterfaceImpl/ChannelResponder$CloseEvent;
.super Ljava/lang/Object;
.source "ChannelResponder.java"

# interfaces
.implements Ljava/lang/Runnable;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lnetwork/InterfaceImpl/ChannelResponder;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0xa
    name = "CloseEvent"
.end annotation


# instance fields
.field private final m_exception:Ljava/lang/Exception;

.field private final m_responder:Lnetwork/InterfaceImpl/ChannelResponder;


# direct methods
.method private constructor <init>(Lnetwork/InterfaceImpl/ChannelResponder;Ljava/lang/Exception;)V
    .locals 0
    .param p1    # Lnetwork/InterfaceImpl/ChannelResponder;
    .param p2    # Ljava/lang/Exception;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    iput-object p1, p0, Lnetwork/InterfaceImpl/ChannelResponder$CloseEvent;->m_responder:Lnetwork/InterfaceImpl/ChannelResponder;

    iput-object p2, p0, Lnetwork/InterfaceImpl/ChannelResponder$CloseEvent;->m_exception:Ljava/lang/Exception;

    return-void
.end method

.method synthetic constructor <init>(Lnetwork/InterfaceImpl/ChannelResponder;Ljava/lang/Exception;Lnetwork/InterfaceImpl/ChannelResponder$CloseEvent;)V
    .locals 0

    invoke-direct {p0, p1, p2}, Lnetwork/InterfaceImpl/ChannelResponder$CloseEvent;-><init>(Lnetwork/InterfaceImpl/ChannelResponder;Ljava/lang/Exception;)V

    return-void
.end method


# virtual methods
.method public run()V
    .locals 2

    iget-object v0, p0, Lnetwork/InterfaceImpl/ChannelResponder$CloseEvent;->m_responder:Lnetwork/InterfaceImpl/ChannelResponder;

    invoke-virtual {v0}, Lnetwork/InterfaceImpl/ChannelResponder;->isOpen()Z

    move-result v0

    if-eqz v0, :cond_0

    iget-object v0, p0, Lnetwork/InterfaceImpl/ChannelResponder$CloseEvent;->m_responder:Lnetwork/InterfaceImpl/ChannelResponder;

    const/4 v1, 0x0

    invoke-static {v0, v1}, Lnetwork/InterfaceImpl/ChannelResponder;->access$0(Lnetwork/InterfaceImpl/ChannelResponder;Z)V

    iget-object v0, p0, Lnetwork/InterfaceImpl/ChannelResponder$CloseEvent;->m_responder:Lnetwork/InterfaceImpl/ChannelResponder;

    invoke-virtual {v0}, Lnetwork/InterfaceImpl/ChannelResponder;->getKey()Ljava/nio/channels/SelectionKey;

    move-result-object v0

    iget-object v1, p0, Lnetwork/InterfaceImpl/ChannelResponder$CloseEvent;->m_responder:Lnetwork/InterfaceImpl/ChannelResponder;

    invoke-virtual {v1}, Lnetwork/InterfaceImpl/ChannelResponder;->getChannel()Ljava/nio/channels/SelectableChannel;

    move-result-object v1

    invoke-static {v0, v1}, Lnetwork/util/NIOUtils;->closeKeyAndChannelSilently(Ljava/nio/channels/SelectionKey;Ljava/nio/channels/Channel;)V

    iget-object v0, p0, Lnetwork/InterfaceImpl/ChannelResponder$CloseEvent;->m_responder:Lnetwork/InterfaceImpl/ChannelResponder;

    iget-object v1, p0, Lnetwork/InterfaceImpl/ChannelResponder$CloseEvent;->m_exception:Ljava/lang/Exception;

    invoke-virtual {v0, v1}, Lnetwork/InterfaceImpl/ChannelResponder;->shutdown(Ljava/lang/Exception;)V

    :cond_0
    return-void
.end method
