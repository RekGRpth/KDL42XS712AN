.class public Lnetwork/InterfaceImpl/ServerSocketChannelResponder;
.super Lnetwork/InterfaceImpl/ChannelResponder;
.source "ServerSocketChannelResponder.java"

# interfaces
.implements Lnetwork/Interface/asyncServerSocket;


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lnetwork/InterfaceImpl/ServerSocketChannelResponder$BeginListenEvent;
    }
.end annotation


# instance fields
.field private volatile m_connectionAcceptor:Lnetwork/Interface/ConnectionAcceptor;

.field private m_observer:Lnetwork/Interface/ServerSocketObserver;

.field private m_totalAcceptedConnections:J

.field private m_totalConnections:J

.field private m_totalFailedConnections:J

.field private m_totalRefusedConnections:J


# direct methods
.method public constructor <init>(Lnetwork/NetCore/asyncService;Ljava/nio/channels/ServerSocketChannel;Ljava/net/InetSocketAddress;)V
    .locals 3
    .param p1    # Lnetwork/NetCore/asyncService;
    .param p2    # Ljava/nio/channels/ServerSocketChannel;
    .param p3    # Ljava/net/InetSocketAddress;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    const-wide/16 v1, 0x0

    invoke-direct {p0, p1, p2, p3}, Lnetwork/InterfaceImpl/ChannelResponder;-><init>(Lnetwork/NetCore/asyncService;Ljava/nio/channels/SelectableChannel;Ljava/net/InetSocketAddress;)V

    const/4 v0, 0x0

    iput-object v0, p0, Lnetwork/InterfaceImpl/ServerSocketChannelResponder;->m_observer:Lnetwork/Interface/ServerSocketObserver;

    sget-object v0, Lnetwork/Interface/ConnectionAcceptor;->ALLOW:Lnetwork/Interface/ConnectionAcceptor;

    invoke-virtual {p0, v0}, Lnetwork/InterfaceImpl/ServerSocketChannelResponder;->setConnectionAcceptor(Lnetwork/Interface/ConnectionAcceptor;)V

    iput-wide v1, p0, Lnetwork/InterfaceImpl/ServerSocketChannelResponder;->m_totalRefusedConnections:J

    iput-wide v1, p0, Lnetwork/InterfaceImpl/ServerSocketChannelResponder;->m_totalAcceptedConnections:J

    iput-wide v1, p0, Lnetwork/InterfaceImpl/ServerSocketChannelResponder;->m_totalFailedConnections:J

    iput-wide v1, p0, Lnetwork/InterfaceImpl/ServerSocketChannelResponder;->m_totalConnections:J

    return-void
.end method

.method static synthetic access$1(Lnetwork/InterfaceImpl/ServerSocketChannelResponder;Lnetwork/Interface/ServerSocketObserver;)V
    .locals 0

    iput-object p1, p0, Lnetwork/InterfaceImpl/ServerSocketChannelResponder;->m_observer:Lnetwork/Interface/ServerSocketObserver;

    return-void
.end method

.method static synthetic access$2(Lnetwork/InterfaceImpl/ServerSocketChannelResponder;Ljava/lang/Exception;)V
    .locals 0

    invoke-direct {p0, p1}, Lnetwork/InterfaceImpl/ServerSocketChannelResponder;->notifyObserverSocketDied(Ljava/lang/Exception;)V

    return-void
.end method

.method private notifyAcceptFailed(Ljava/io/IOException;)V
    .locals 2
    .param p1    # Ljava/io/IOException;

    :try_start_0
    iget-object v1, p0, Lnetwork/InterfaceImpl/ServerSocketChannelResponder;->m_observer:Lnetwork/Interface/ServerSocketObserver;

    if-eqz v1, :cond_0

    iget-object v1, p0, Lnetwork/InterfaceImpl/ServerSocketChannelResponder;->m_observer:Lnetwork/Interface/ServerSocketObserver;

    invoke-interface {v1, p1}, Lnetwork/Interface/ServerSocketObserver;->acceptFailed(Ljava/io/IOException;)V
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    :cond_0
    :goto_0
    return-void

    :catch_0
    move-exception v0

    invoke-virtual {p0}, Lnetwork/InterfaceImpl/ServerSocketChannelResponder;->getasyncService()Lnetwork/NetCore/asyncService;

    move-result-object v1

    invoke-virtual {v1, v0}, Lnetwork/NetCore/asyncService;->notifyException(Ljava/lang/Throwable;)V

    goto :goto_0
.end method

.method private notifyNewConnection(Lnetwork/Interface/NIOSocket;)V
    .locals 2
    .param p1    # Lnetwork/Interface/NIOSocket;

    :try_start_0
    iget-object v1, p0, Lnetwork/InterfaceImpl/ServerSocketChannelResponder;->m_observer:Lnetwork/Interface/ServerSocketObserver;

    if-eqz v1, :cond_0

    iget-object v1, p0, Lnetwork/InterfaceImpl/ServerSocketChannelResponder;->m_observer:Lnetwork/Interface/ServerSocketObserver;

    invoke-interface {v1, p1}, Lnetwork/Interface/ServerSocketObserver;->newConnection(Lnetwork/Interface/NIOSocket;)V
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    :cond_0
    :goto_0
    return-void

    :catch_0
    move-exception v0

    invoke-virtual {p0}, Lnetwork/InterfaceImpl/ServerSocketChannelResponder;->getasyncService()Lnetwork/NetCore/asyncService;

    move-result-object v1

    invoke-virtual {v1, v0}, Lnetwork/NetCore/asyncService;->notifyException(Ljava/lang/Throwable;)V

    invoke-interface {p1}, Lnetwork/Interface/NIOSocket;->close()V

    goto :goto_0
.end method

.method private notifyObserverSocketDied(Ljava/lang/Exception;)V
    .locals 2
    .param p1    # Ljava/lang/Exception;

    :try_start_0
    iget-object v1, p0, Lnetwork/InterfaceImpl/ServerSocketChannelResponder;->m_observer:Lnetwork/Interface/ServerSocketObserver;

    if-eqz v1, :cond_0

    iget-object v1, p0, Lnetwork/InterfaceImpl/ServerSocketChannelResponder;->m_observer:Lnetwork/Interface/ServerSocketObserver;

    invoke-interface {v1, p1}, Lnetwork/Interface/ServerSocketObserver;->serverSocketDied(Ljava/lang/Exception;)V
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    :cond_0
    :goto_0
    return-void

    :catch_0
    move-exception v0

    invoke-virtual {p0}, Lnetwork/InterfaceImpl/ServerSocketChannelResponder;->getasyncService()Lnetwork/NetCore/asyncService;

    move-result-object v1

    invoke-virtual {v1, v0}, Lnetwork/NetCore/asyncService;->notifyException(Ljava/lang/Throwable;)V

    goto :goto_0
.end method


# virtual methods
.method public bridge synthetic getChannel()Ljava/nio/channels/SelectableChannel;
    .locals 1

    invoke-virtual {p0}, Lnetwork/InterfaceImpl/ServerSocketChannelResponder;->getChannel()Ljava/nio/channels/ServerSocketChannel;

    move-result-object v0

    return-object v0
.end method

.method public getChannel()Ljava/nio/channels/ServerSocketChannel;
    .locals 1

    invoke-super {p0}, Lnetwork/InterfaceImpl/ChannelResponder;->getChannel()Ljava/nio/channels/SelectableChannel;

    move-result-object v0

    check-cast v0, Ljava/nio/channels/ServerSocketChannel;

    return-object v0
.end method

.method public getTotalAcceptedConnections()J
    .locals 2

    iget-wide v0, p0, Lnetwork/InterfaceImpl/ServerSocketChannelResponder;->m_totalAcceptedConnections:J

    return-wide v0
.end method

.method public getTotalConnections()J
    .locals 2

    iget-wide v0, p0, Lnetwork/InterfaceImpl/ServerSocketChannelResponder;->m_totalConnections:J

    return-wide v0
.end method

.method public getTotalFailedConnections()J
    .locals 2

    iget-wide v0, p0, Lnetwork/InterfaceImpl/ServerSocketChannelResponder;->m_totalFailedConnections:J

    return-wide v0
.end method

.method public getTotalRefusedConnections()J
    .locals 2

    iget-wide v0, p0, Lnetwork/InterfaceImpl/ServerSocketChannelResponder;->m_totalRefusedConnections:J

    return-wide v0
.end method

.method public keyInitialized()V
    .locals 1

    const/16 v0, 0x10

    invoke-virtual {p0, v0}, Lnetwork/InterfaceImpl/ServerSocketChannelResponder;->addInterest(I)V

    return-void
.end method

.method public listen(Lnetwork/Interface/ServerSocketObserver;)V
    .locals 3
    .param p1    # Lnetwork/Interface/ServerSocketObserver;

    if-nez p1, :cond_0

    new-instance v0, Ljava/lang/NullPointerException;

    invoke-direct {v0}, Ljava/lang/NullPointerException;-><init>()V

    throw v0

    :cond_0
    invoke-virtual {p0}, Lnetwork/InterfaceImpl/ServerSocketChannelResponder;->markObserverSet()V

    invoke-virtual {p0}, Lnetwork/InterfaceImpl/ServerSocketChannelResponder;->getasyncService()Lnetwork/NetCore/asyncService;

    move-result-object v0

    new-instance v1, Lnetwork/InterfaceImpl/ServerSocketChannelResponder$BeginListenEvent;

    const/4 v2, 0x0

    invoke-direct {v1, p0, p1, v2}, Lnetwork/InterfaceImpl/ServerSocketChannelResponder$BeginListenEvent;-><init>(Lnetwork/InterfaceImpl/ServerSocketChannelResponder;Lnetwork/Interface/ServerSocketObserver;Lnetwork/InterfaceImpl/ServerSocketChannelResponder$BeginListenEvent;)V

    invoke-virtual {v0, v1}, Lnetwork/NetCore/asyncService;->queue(Ljava/lang/Runnable;)V

    return-void
.end method

.method public notifyWasCancelled()V
    .locals 0

    invoke-virtual {p0}, Lnetwork/InterfaceImpl/ServerSocketChannelResponder;->close()V

    return-void
.end method

.method registerSocket(Ljava/nio/channels/SocketChannel;Ljava/net/InetSocketAddress;)Lnetwork/Interface/NIOSocket;
    .locals 1
    .param p1    # Ljava/nio/channels/SocketChannel;
    .param p2    # Ljava/net/InetSocketAddress;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    invoke-virtual {p0}, Lnetwork/InterfaceImpl/ServerSocketChannelResponder;->getasyncService()Lnetwork/NetCore/asyncService;

    move-result-object v0

    invoke-virtual {v0, p1, p2}, Lnetwork/NetCore/asyncService;->registerSocketChannel(Ljava/nio/channels/SocketChannel;Ljava/net/InetSocketAddress;)Lnetwork/Interface/NIOSocket;

    move-result-object v0

    return-object v0
.end method

.method public setConnectionAcceptor(Lnetwork/Interface/ConnectionAcceptor;)V
    .locals 0
    .param p1    # Lnetwork/Interface/ConnectionAcceptor;

    if-nez p1, :cond_0

    sget-object p1, Lnetwork/Interface/ConnectionAcceptor;->DENY:Lnetwork/Interface/ConnectionAcceptor;

    :cond_0
    iput-object p1, p0, Lnetwork/InterfaceImpl/ServerSocketChannelResponder;->m_connectionAcceptor:Lnetwork/Interface/ConnectionAcceptor;

    return-void
.end method

.method protected shutdown(Ljava/lang/Exception;)V
    .locals 0
    .param p1    # Ljava/lang/Exception;

    invoke-direct {p0, p1}, Lnetwork/InterfaceImpl/ServerSocketChannelResponder;->notifyObserverSocketDied(Ljava/lang/Exception;)V

    return-void
.end method

.method public socket()Ljava/net/ServerSocket;
    .locals 1

    invoke-virtual {p0}, Lnetwork/InterfaceImpl/ServerSocketChannelResponder;->getChannel()Ljava/nio/channels/ServerSocketChannel;

    move-result-object v0

    invoke-virtual {v0}, Ljava/nio/channels/ServerSocketChannel;->socket()Ljava/net/ServerSocket;

    move-result-object v0

    return-object v0
.end method

.method public socketReadyForAccept()V
    .locals 7

    const-wide/16 v5, 0x1

    iget-wide v3, p0, Lnetwork/InterfaceImpl/ServerSocketChannelResponder;->m_totalConnections:J

    add-long/2addr v3, v5

    iput-wide v3, p0, Lnetwork/InterfaceImpl/ServerSocketChannelResponder;->m_totalConnections:J

    const/4 v2, 0x0

    :try_start_0
    invoke-virtual {p0}, Lnetwork/InterfaceImpl/ServerSocketChannelResponder;->getChannel()Ljava/nio/channels/ServerSocketChannel;

    move-result-object v3

    invoke-virtual {v3}, Ljava/nio/channels/ServerSocketChannel;->accept()Ljava/nio/channels/SocketChannel;

    move-result-object v2

    if-nez v2, :cond_0

    iget-wide v3, p0, Lnetwork/InterfaceImpl/ServerSocketChannelResponder;->m_totalConnections:J

    sub-long/2addr v3, v5

    iput-wide v3, p0, Lnetwork/InterfaceImpl/ServerSocketChannelResponder;->m_totalConnections:J

    :goto_0
    return-void

    :cond_0
    invoke-virtual {v2}, Ljava/nio/channels/SocketChannel;->socket()Ljava/net/Socket;

    move-result-object v3

    invoke-virtual {v3}, Ljava/net/Socket;->getRemoteSocketAddress()Ljava/net/SocketAddress;

    move-result-object v0

    check-cast v0, Ljava/net/InetSocketAddress;

    iget-object v3, p0, Lnetwork/InterfaceImpl/ServerSocketChannelResponder;->m_connectionAcceptor:Lnetwork/Interface/ConnectionAcceptor;

    invoke-interface {v3, v0}, Lnetwork/Interface/ConnectionAcceptor;->acceptConnection(Ljava/net/InetSocketAddress;)Z

    move-result v3

    if-nez v3, :cond_1

    iget-wide v3, p0, Lnetwork/InterfaceImpl/ServerSocketChannelResponder;->m_totalRefusedConnections:J

    add-long/2addr v3, v5

    iput-wide v3, p0, Lnetwork/InterfaceImpl/ServerSocketChannelResponder;->m_totalRefusedConnections:J

    invoke-static {v2}, Lnetwork/util/NIOUtils;->closeChannelSilently(Ljava/nio/channels/Channel;)V
    :try_end_0
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_0

    goto :goto_0

    :catch_0
    move-exception v1

    invoke-static {v2}, Lnetwork/util/NIOUtils;->closeChannelSilently(Ljava/nio/channels/Channel;)V

    iget-wide v3, p0, Lnetwork/InterfaceImpl/ServerSocketChannelResponder;->m_totalFailedConnections:J

    add-long/2addr v3, v5

    iput-wide v3, p0, Lnetwork/InterfaceImpl/ServerSocketChannelResponder;->m_totalFailedConnections:J

    invoke-direct {p0, v1}, Lnetwork/InterfaceImpl/ServerSocketChannelResponder;->notifyAcceptFailed(Ljava/io/IOException;)V

    goto :goto_0

    :cond_1
    :try_start_1
    invoke-virtual {p0, v2, v0}, Lnetwork/InterfaceImpl/ServerSocketChannelResponder;->registerSocket(Ljava/nio/channels/SocketChannel;Ljava/net/InetSocketAddress;)Lnetwork/Interface/NIOSocket;

    move-result-object v3

    invoke-direct {p0, v3}, Lnetwork/InterfaceImpl/ServerSocketChannelResponder;->notifyNewConnection(Lnetwork/Interface/NIOSocket;)V

    iget-wide v3, p0, Lnetwork/InterfaceImpl/ServerSocketChannelResponder;->m_totalAcceptedConnections:J

    add-long/2addr v3, v5

    iput-wide v3, p0, Lnetwork/InterfaceImpl/ServerSocketChannelResponder;->m_totalAcceptedConnections:J
    :try_end_1
    .catch Ljava/io/IOException; {:try_start_1 .. :try_end_1} :catch_0

    goto :goto_0
.end method
