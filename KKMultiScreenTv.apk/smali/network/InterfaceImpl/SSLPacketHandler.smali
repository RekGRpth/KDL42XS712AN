.class public Lnetwork/InterfaceImpl/SSLPacketHandler;
.super Ljava/lang/Object;
.source "SSLPacketHandler.java"

# interfaces
.implements Lnetwork/Interface/PacketReader;
.implements Lnetwork/Interface/PacketWriter;


# static fields
.field private static synthetic $SWITCH_TABLE$javax$net$ssl$SSLEngineResult$HandshakeStatus:[I

.field private static synthetic $SWITCH_TABLE$javax$net$ssl$SSLEngineResult$Status:[I

.field private static final SSL_BUFFER:Ljava/lang/ThreadLocal;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/lang/ThreadLocal",
            "<",
            "Ljava/nio/ByteBuffer;",
            ">;"
        }
    .end annotation
.end field

.field private static final TASK_HANDLER:Ljava/util/concurrent/Executor;


# instance fields
.field private final m_engine:Ljavax/net/ssl/SSLEngine;

.field private m_initialOutBuffer:[Ljava/nio/ByteBuffer;

.field private m_partialIncomingBuffer:Ljava/nio/ByteBuffer;

.field private m_reader:Lnetwork/Interface/PacketReader;

.field private final m_responder:Lnetwork/InterfaceImpl/SSLSocketChannelResponder;

.field private final m_socket:Lnetwork/Interface/NIOSocket;

.field private m_sslInitiated:Z

.field private m_writer:Lnetwork/Interface/PacketWriter;


# direct methods
.method static synthetic $SWITCH_TABLE$javax$net$ssl$SSLEngineResult$HandshakeStatus()[I
    .locals 3

    sget-object v0, Lnetwork/InterfaceImpl/SSLPacketHandler;->$SWITCH_TABLE$javax$net$ssl$SSLEngineResult$HandshakeStatus:[I

    if-eqz v0, :cond_0

    :goto_0
    return-object v0

    :cond_0
    invoke-static {}, Ljavax/net/ssl/SSLEngineResult$HandshakeStatus;->values()[Ljavax/net/ssl/SSLEngineResult$HandshakeStatus;

    move-result-object v0

    array-length v0, v0

    new-array v0, v0, [I

    :try_start_0
    sget-object v1, Ljavax/net/ssl/SSLEngineResult$HandshakeStatus;->FINISHED:Ljavax/net/ssl/SSLEngineResult$HandshakeStatus;

    invoke-virtual {v1}, Ljavax/net/ssl/SSLEngineResult$HandshakeStatus;->ordinal()I

    move-result v1

    const/4 v2, 0x1

    aput v2, v0, v1
    :try_end_0
    .catch Ljava/lang/NoSuchFieldError; {:try_start_0 .. :try_end_0} :catch_4

    :goto_1
    :try_start_1
    sget-object v1, Ljavax/net/ssl/SSLEngineResult$HandshakeStatus;->NEED_TASK:Ljavax/net/ssl/SSLEngineResult$HandshakeStatus;

    invoke-virtual {v1}, Ljavax/net/ssl/SSLEngineResult$HandshakeStatus;->ordinal()I

    move-result v1

    const/4 v2, 0x2

    aput v2, v0, v1
    :try_end_1
    .catch Ljava/lang/NoSuchFieldError; {:try_start_1 .. :try_end_1} :catch_3

    :goto_2
    :try_start_2
    sget-object v1, Ljavax/net/ssl/SSLEngineResult$HandshakeStatus;->NEED_UNWRAP:Ljavax/net/ssl/SSLEngineResult$HandshakeStatus;

    invoke-virtual {v1}, Ljavax/net/ssl/SSLEngineResult$HandshakeStatus;->ordinal()I

    move-result v1

    const/4 v2, 0x3

    aput v2, v0, v1
    :try_end_2
    .catch Ljava/lang/NoSuchFieldError; {:try_start_2 .. :try_end_2} :catch_2

    :goto_3
    :try_start_3
    sget-object v1, Ljavax/net/ssl/SSLEngineResult$HandshakeStatus;->NEED_WRAP:Ljavax/net/ssl/SSLEngineResult$HandshakeStatus;

    invoke-virtual {v1}, Ljavax/net/ssl/SSLEngineResult$HandshakeStatus;->ordinal()I

    move-result v1

    const/4 v2, 0x4

    aput v2, v0, v1
    :try_end_3
    .catch Ljava/lang/NoSuchFieldError; {:try_start_3 .. :try_end_3} :catch_1

    :goto_4
    :try_start_4
    sget-object v1, Ljavax/net/ssl/SSLEngineResult$HandshakeStatus;->NOT_HANDSHAKING:Ljavax/net/ssl/SSLEngineResult$HandshakeStatus;

    invoke-virtual {v1}, Ljavax/net/ssl/SSLEngineResult$HandshakeStatus;->ordinal()I

    move-result v1

    const/4 v2, 0x5

    aput v2, v0, v1
    :try_end_4
    .catch Ljava/lang/NoSuchFieldError; {:try_start_4 .. :try_end_4} :catch_0

    :goto_5
    sput-object v0, Lnetwork/InterfaceImpl/SSLPacketHandler;->$SWITCH_TABLE$javax$net$ssl$SSLEngineResult$HandshakeStatus:[I

    goto :goto_0

    :catch_0
    move-exception v1

    goto :goto_5

    :catch_1
    move-exception v1

    goto :goto_4

    :catch_2
    move-exception v1

    goto :goto_3

    :catch_3
    move-exception v1

    goto :goto_2

    :catch_4
    move-exception v1

    goto :goto_1
.end method

.method static synthetic $SWITCH_TABLE$javax$net$ssl$SSLEngineResult$Status()[I
    .locals 3

    sget-object v0, Lnetwork/InterfaceImpl/SSLPacketHandler;->$SWITCH_TABLE$javax$net$ssl$SSLEngineResult$Status:[I

    if-eqz v0, :cond_0

    :goto_0
    return-object v0

    :cond_0
    invoke-static {}, Ljavax/net/ssl/SSLEngineResult$Status;->values()[Ljavax/net/ssl/SSLEngineResult$Status;

    move-result-object v0

    array-length v0, v0

    new-array v0, v0, [I

    :try_start_0
    sget-object v1, Ljavax/net/ssl/SSLEngineResult$Status;->BUFFER_OVERFLOW:Ljavax/net/ssl/SSLEngineResult$Status;

    invoke-virtual {v1}, Ljavax/net/ssl/SSLEngineResult$Status;->ordinal()I

    move-result v1

    const/4 v2, 0x1

    aput v2, v0, v1
    :try_end_0
    .catch Ljava/lang/NoSuchFieldError; {:try_start_0 .. :try_end_0} :catch_3

    :goto_1
    :try_start_1
    sget-object v1, Ljavax/net/ssl/SSLEngineResult$Status;->BUFFER_UNDERFLOW:Ljavax/net/ssl/SSLEngineResult$Status;

    invoke-virtual {v1}, Ljavax/net/ssl/SSLEngineResult$Status;->ordinal()I

    move-result v1

    const/4 v2, 0x2

    aput v2, v0, v1
    :try_end_1
    .catch Ljava/lang/NoSuchFieldError; {:try_start_1 .. :try_end_1} :catch_2

    :goto_2
    :try_start_2
    sget-object v1, Ljavax/net/ssl/SSLEngineResult$Status;->CLOSED:Ljavax/net/ssl/SSLEngineResult$Status;

    invoke-virtual {v1}, Ljavax/net/ssl/SSLEngineResult$Status;->ordinal()I

    move-result v1

    const/4 v2, 0x3

    aput v2, v0, v1
    :try_end_2
    .catch Ljava/lang/NoSuchFieldError; {:try_start_2 .. :try_end_2} :catch_1

    :goto_3
    :try_start_3
    sget-object v1, Ljavax/net/ssl/SSLEngineResult$Status;->OK:Ljavax/net/ssl/SSLEngineResult$Status;

    invoke-virtual {v1}, Ljavax/net/ssl/SSLEngineResult$Status;->ordinal()I

    move-result v1

    const/4 v2, 0x4

    aput v2, v0, v1
    :try_end_3
    .catch Ljava/lang/NoSuchFieldError; {:try_start_3 .. :try_end_3} :catch_0

    :goto_4
    sput-object v0, Lnetwork/InterfaceImpl/SSLPacketHandler;->$SWITCH_TABLE$javax$net$ssl$SSLEngineResult$Status:[I

    goto :goto_0

    :catch_0
    move-exception v1

    goto :goto_4

    :catch_1
    move-exception v1

    goto :goto_3

    :catch_2
    move-exception v1

    goto :goto_2

    :catch_3
    move-exception v1

    goto :goto_1
.end method

.method static constructor <clinit>()V
    .locals 1

    invoke-static {}, Ljava/util/concurrent/Executors;->newSingleThreadExecutor()Ljava/util/concurrent/ExecutorService;

    move-result-object v0

    sput-object v0, Lnetwork/InterfaceImpl/SSLPacketHandler;->TASK_HANDLER:Ljava/util/concurrent/Executor;

    new-instance v0, Lnetwork/InterfaceImpl/SSLPacketHandler$1;

    invoke-direct {v0}, Lnetwork/InterfaceImpl/SSLPacketHandler$1;-><init>()V

    sput-object v0, Lnetwork/InterfaceImpl/SSLPacketHandler;->SSL_BUFFER:Ljava/lang/ThreadLocal;

    return-void
.end method

.method public constructor <init>(Ljavax/net/ssl/SSLEngine;Lnetwork/Interface/NIOSocket;Lnetwork/InterfaceImpl/SSLSocketChannelResponder;)V
    .locals 1
    .param p1    # Ljavax/net/ssl/SSLEngine;
    .param p2    # Lnetwork/Interface/NIOSocket;
    .param p3    # Lnetwork/InterfaceImpl/SSLSocketChannelResponder;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    iput-object p1, p0, Lnetwork/InterfaceImpl/SSLPacketHandler;->m_engine:Ljavax/net/ssl/SSLEngine;

    iput-object p2, p0, Lnetwork/InterfaceImpl/SSLPacketHandler;->m_socket:Lnetwork/Interface/NIOSocket;

    const/4 v0, 0x0

    iput-object v0, p0, Lnetwork/InterfaceImpl/SSLPacketHandler;->m_partialIncomingBuffer:Ljava/nio/ByteBuffer;

    sget-object v0, Lnetwork/NetIO/packetwriter/RawPacketWriter;->INSTANCE:Lnetwork/NetIO/packetwriter/RawPacketWriter;

    iput-object v0, p0, Lnetwork/InterfaceImpl/SSLPacketHandler;->m_writer:Lnetwork/Interface/PacketWriter;

    sget-object v0, Lnetwork/NetIO/packetreader/RawPacketReader;->INSTANCE:Lnetwork/NetIO/packetreader/RawPacketReader;

    iput-object v0, p0, Lnetwork/InterfaceImpl/SSLPacketHandler;->m_reader:Lnetwork/Interface/PacketReader;

    iput-object p3, p0, Lnetwork/InterfaceImpl/SSLPacketHandler;->m_responder:Lnetwork/InterfaceImpl/SSLSocketChannelResponder;

    const/4 v0, 0x0

    iput-boolean v0, p0, Lnetwork/InterfaceImpl/SSLPacketHandler;->m_sslInitiated:Z

    return-void
.end method

.method static synthetic access$0(Lnetwork/InterfaceImpl/SSLPacketHandler;)Lnetwork/Interface/NIOSocket;
    .locals 1

    iget-object v0, p0, Lnetwork/InterfaceImpl/SSLPacketHandler;->m_socket:Lnetwork/Interface/NIOSocket;

    return-object v0
.end method

.method static synthetic access$1(Lnetwork/InterfaceImpl/SSLPacketHandler;)Ljavax/net/ssl/SSLEngine;
    .locals 1

    iget-object v0, p0, Lnetwork/InterfaceImpl/SSLPacketHandler;->m_engine:Ljavax/net/ssl/SSLEngine;

    return-object v0
.end method

.method static synthetic access$2(Lnetwork/InterfaceImpl/SSLPacketHandler;Ljavax/net/ssl/SSLEngineResult$HandshakeStatus;)V
    .locals 0

    invoke-direct {p0, p1}, Lnetwork/InterfaceImpl/SSLPacketHandler;->reactToHandshakeStatus(Ljavax/net/ssl/SSLEngineResult$HandshakeStatus;)V

    return-void
.end method

.method private queueSSLTasks()V
    .locals 4

    iget-boolean v2, p0, Lnetwork/InterfaceImpl/SSLPacketHandler;->m_sslInitiated:Z

    if-nez v2, :cond_1

    :cond_0
    :goto_0
    return-void

    :cond_1
    const/4 v1, 0x0

    :goto_1
    iget-object v2, p0, Lnetwork/InterfaceImpl/SSLPacketHandler;->m_engine:Ljavax/net/ssl/SSLEngine;

    invoke-virtual {v2}, Ljavax/net/ssl/SSLEngine;->getDelegatedTask()Ljava/lang/Runnable;

    move-result-object v0

    if-nez v0, :cond_2

    if-eqz v1, :cond_0

    sget-object v2, Lnetwork/InterfaceImpl/SSLPacketHandler;->TASK_HANDLER:Ljava/util/concurrent/Executor;

    new-instance v3, Lnetwork/InterfaceImpl/SSLPacketHandler$2;

    invoke-direct {v3, p0}, Lnetwork/InterfaceImpl/SSLPacketHandler$2;-><init>(Lnetwork/InterfaceImpl/SSLPacketHandler;)V

    invoke-interface {v2, v3}, Ljava/util/concurrent/Executor;->execute(Ljava/lang/Runnable;)V

    goto :goto_0

    :cond_2
    sget-object v2, Lnetwork/InterfaceImpl/SSLPacketHandler;->TASK_HANDLER:Ljava/util/concurrent/Executor;

    invoke-interface {v2, v0}, Ljava/util/concurrent/Executor;->execute(Ljava/lang/Runnable;)V

    add-int/lit8 v1, v1, 0x1

    goto :goto_1
.end method

.method private reactToHandshakeStatus(Ljavax/net/ssl/SSLEngineResult$HandshakeStatus;)V
    .locals 3
    .param p1    # Ljavax/net/ssl/SSLEngineResult$HandshakeStatus;

    const/4 v2, 0x0

    iget-boolean v0, p0, Lnetwork/InterfaceImpl/SSLPacketHandler;->m_sslInitiated:Z

    if-nez v0, :cond_0

    :goto_0
    :pswitch_0
    return-void

    :cond_0
    invoke-static {}, Lnetwork/InterfaceImpl/SSLPacketHandler;->$SWITCH_TABLE$javax$net$ssl$SSLEngineResult$HandshakeStatus()[I

    move-result-object v0

    invoke-virtual {p1}, Ljavax/net/ssl/SSLEngineResult$HandshakeStatus;->ordinal()I

    move-result v1

    aget v0, v0, v1

    packed-switch v0, :pswitch_data_0

    goto :goto_0

    :pswitch_1
    iget-object v0, p0, Lnetwork/InterfaceImpl/SSLPacketHandler;->m_socket:Lnetwork/Interface/NIOSocket;

    new-array v1, v2, [B

    invoke-interface {v0, v1}, Lnetwork/Interface/NIOSocket;->write([B)Z

    goto :goto_0

    :pswitch_2
    invoke-direct {p0}, Lnetwork/InterfaceImpl/SSLPacketHandler;->queueSSLTasks()V

    goto :goto_0

    :pswitch_3
    iget-object v0, p0, Lnetwork/InterfaceImpl/SSLPacketHandler;->m_socket:Lnetwork/Interface/NIOSocket;

    new-array v1, v2, [B

    invoke-interface {v0, v1}, Lnetwork/Interface/NIOSocket;->write([B)Z

    goto :goto_0

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_1
        :pswitch_2
        :pswitch_0
        :pswitch_3
        :pswitch_0
    .end packed-switch
.end method

.method private retrieveDecryptedPacket(Ljava/nio/ByteBuffer;)[B
    .locals 2
    .param p1    # Ljava/nio/ByteBuffer;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lnetwork/exception/ProtocolViolationException;
        }
    .end annotation

    invoke-virtual {p1}, Ljava/nio/ByteBuffer;->flip()Ljava/nio/Buffer;

    iget-object v0, p0, Lnetwork/InterfaceImpl/SSLPacketHandler;->m_partialIncomingBuffer:Ljava/nio/ByteBuffer;

    invoke-static {v0, p1}, Lnetwork/util/NIOUtils;->join(Ljava/nio/ByteBuffer;Ljava/nio/ByteBuffer;)Ljava/nio/ByteBuffer;

    move-result-object v0

    iput-object v0, p0, Lnetwork/InterfaceImpl/SSLPacketHandler;->m_partialIncomingBuffer:Ljava/nio/ByteBuffer;

    iget-object v0, p0, Lnetwork/InterfaceImpl/SSLPacketHandler;->m_partialIncomingBuffer:Ljava/nio/ByteBuffer;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lnetwork/InterfaceImpl/SSLPacketHandler;->m_partialIncomingBuffer:Ljava/nio/ByteBuffer;

    invoke-virtual {v0}, Ljava/nio/ByteBuffer;->remaining()I

    move-result v0

    if-nez v0, :cond_1

    :cond_0
    sget-object v0, Lnetwork/InterfaceImpl/SSLPacketHandler;->SKIP_PACKET:[B

    :goto_0
    return-object v0

    :cond_1
    iget-object v0, p0, Lnetwork/InterfaceImpl/SSLPacketHandler;->m_reader:Lnetwork/Interface/PacketReader;

    iget-object v1, p0, Lnetwork/InterfaceImpl/SSLPacketHandler;->m_partialIncomingBuffer:Ljava/nio/ByteBuffer;

    invoke-interface {v0, v1}, Lnetwork/Interface/PacketReader;->nextPacket(Ljava/nio/ByteBuffer;)[B

    move-result-object v0

    goto :goto_0
.end method


# virtual methods
.method begin()V
    .locals 1
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljavax/net/ssl/SSLException;
        }
    .end annotation

    iget-object v0, p0, Lnetwork/InterfaceImpl/SSLPacketHandler;->m_engine:Ljavax/net/ssl/SSLEngine;

    invoke-virtual {v0}, Ljavax/net/ssl/SSLEngine;->beginHandshake()V

    const/4 v0, 0x1

    iput-boolean v0, p0, Lnetwork/InterfaceImpl/SSLPacketHandler;->m_sslInitiated:Z

    iget-object v0, p0, Lnetwork/InterfaceImpl/SSLPacketHandler;->m_engine:Ljavax/net/ssl/SSLEngine;

    invoke-virtual {v0}, Ljavax/net/ssl/SSLEngine;->getHandshakeStatus()Ljavax/net/ssl/SSLEngineResult$HandshakeStatus;

    move-result-object v0

    invoke-direct {p0, v0}, Lnetwork/InterfaceImpl/SSLPacketHandler;->reactToHandshakeStatus(Ljavax/net/ssl/SSLEngineResult$HandshakeStatus;)V

    return-void
.end method

.method public closeEngine()V
    .locals 2

    iget-boolean v0, p0, Lnetwork/InterfaceImpl/SSLPacketHandler;->m_sslInitiated:Z

    if-nez v0, :cond_0

    :goto_0
    return-void

    :cond_0
    iget-object v0, p0, Lnetwork/InterfaceImpl/SSLPacketHandler;->m_engine:Ljavax/net/ssl/SSLEngine;

    invoke-virtual {v0}, Ljavax/net/ssl/SSLEngine;->closeOutbound()V

    iget-object v0, p0, Lnetwork/InterfaceImpl/SSLPacketHandler;->m_responder:Lnetwork/InterfaceImpl/SSLSocketChannelResponder;

    const/4 v1, 0x0

    new-array v1, v1, [B

    invoke-virtual {v0, v1}, Lnetwork/InterfaceImpl/SSLSocketChannelResponder;->write([B)Z

    goto :goto_0
.end method

.method public getReader()Lnetwork/Interface/PacketReader;
    .locals 1

    iget-object v0, p0, Lnetwork/InterfaceImpl/SSLPacketHandler;->m_reader:Lnetwork/Interface/PacketReader;

    return-object v0
.end method

.method public getSSLEngine()Ljavax/net/ssl/SSLEngine;
    .locals 1

    iget-object v0, p0, Lnetwork/InterfaceImpl/SSLPacketHandler;->m_engine:Ljavax/net/ssl/SSLEngine;

    return-object v0
.end method

.method public getWriter()Lnetwork/Interface/PacketWriter;
    .locals 1

    iget-object v0, p0, Lnetwork/InterfaceImpl/SSLPacketHandler;->m_writer:Lnetwork/Interface/PacketWriter;

    return-object v0
.end method

.method public isEncrypted()Z
    .locals 1

    iget-boolean v0, p0, Lnetwork/InterfaceImpl/SSLPacketHandler;->m_sslInitiated:Z

    return v0
.end method

.method public nextPacket(Ljava/nio/ByteBuffer;)[B
    .locals 8
    .param p1    # Ljava/nio/ByteBuffer;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lnetwork/exception/ProtocolViolationException;
        }
    .end annotation

    const/4 v3, 0x0

    iget-boolean v4, p0, Lnetwork/InterfaceImpl/SSLPacketHandler;->m_sslInitiated:Z

    if-nez v4, :cond_0

    iget-object v3, p0, Lnetwork/InterfaceImpl/SSLPacketHandler;->m_reader:Lnetwork/Interface/PacketReader;

    invoke-interface {v3, p1}, Lnetwork/Interface/PacketReader;->nextPacket(Ljava/nio/ByteBuffer;)[B

    move-result-object v3

    :goto_0
    :pswitch_0
    return-object v3

    :cond_0
    :try_start_0
    sget-object v4, Lnetwork/InterfaceImpl/SSLPacketHandler;->SSL_BUFFER:Ljava/lang/ThreadLocal;

    invoke-virtual {v4}, Ljava/lang/ThreadLocal;->get()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Ljava/nio/ByteBuffer;

    invoke-virtual {v2}, Ljava/nio/ByteBuffer;->clear()Ljava/nio/Buffer;

    iget-object v4, p0, Lnetwork/InterfaceImpl/SSLPacketHandler;->m_engine:Ljavax/net/ssl/SSLEngine;

    invoke-virtual {v4, p1, v2}, Ljavax/net/ssl/SSLEngine;->unwrap(Ljava/nio/ByteBuffer;Ljava/nio/ByteBuffer;)Ljavax/net/ssl/SSLEngineResult;

    move-result-object v1

    invoke-static {}, Lnetwork/InterfaceImpl/SSLPacketHandler;->$SWITCH_TABLE$javax$net$ssl$SSLEngineResult$Status()[I

    move-result-object v4

    invoke-virtual {v1}, Ljavax/net/ssl/SSLEngineResult;->getStatus()Ljavax/net/ssl/SSLEngineResult$Status;

    move-result-object v5

    invoke-virtual {v5}, Ljavax/net/ssl/SSLEngineResult$Status;->ordinal()I

    move-result v5

    aget v4, v4, v5

    packed-switch v4, :pswitch_data_0

    invoke-virtual {v1}, Ljavax/net/ssl/SSLEngineResult;->getHandshakeStatus()Ljavax/net/ssl/SSLEngineResult$HandshakeStatus;

    move-result-object v4

    invoke-direct {p0, v4}, Lnetwork/InterfaceImpl/SSLPacketHandler;->reactToHandshakeStatus(Ljavax/net/ssl/SSLEngineResult$HandshakeStatus;)V

    invoke-direct {p0, v2}, Lnetwork/InterfaceImpl/SSLPacketHandler;->retrieveDecryptedPacket(Ljava/nio/ByteBuffer;)[B

    move-result-object v3

    goto :goto_0

    :pswitch_1
    new-instance v4, Lnetwork/exception/ProtocolViolationException;

    const-string v5, "SSL Buffer Overflow"

    invoke-direct {v4, v5}, Lnetwork/exception/ProtocolViolationException;-><init>(Ljava/lang/String;)V

    throw v4
    :try_end_0
    .catch Ljavax/net/ssl/SSLException; {:try_start_0 .. :try_end_0} :catch_0

    :catch_0
    move-exception v0

    iget-object v4, p0, Lnetwork/InterfaceImpl/SSLPacketHandler;->m_responder:Lnetwork/InterfaceImpl/SSLSocketChannelResponder;

    invoke-virtual {v4, v0}, Lnetwork/InterfaceImpl/SSLSocketChannelResponder;->closeDueToSSLException(Ljavax/net/ssl/SSLException;)V

    goto :goto_0

    :pswitch_2
    :try_start_1
    iget-object v4, p0, Lnetwork/InterfaceImpl/SSLPacketHandler;->m_responder:Lnetwork/InterfaceImpl/SSLSocketChannelResponder;

    iget-object v5, p0, Lnetwork/InterfaceImpl/SSLPacketHandler;->m_socket:Lnetwork/Interface/NIOSocket;

    new-instance v6, Ljava/io/EOFException;

    const-string v7, "SSL Connection closed"

    invoke-direct {v6, v7}, Ljava/io/EOFException;-><init>(Ljava/lang/String;)V

    invoke-virtual {v4, v5, v6}, Lnetwork/InterfaceImpl/SSLSocketChannelResponder;->connectionBroken(Lnetwork/Interface/NIOSocket;Ljava/lang/Exception;)V
    :try_end_1
    .catch Ljavax/net/ssl/SSLException; {:try_start_1 .. :try_end_1} :catch_0

    goto :goto_0

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_1
        :pswitch_0
        :pswitch_2
    .end packed-switch
.end method

.method public setReader(Lnetwork/Interface/PacketReader;)V
    .locals 0
    .param p1    # Lnetwork/Interface/PacketReader;

    iput-object p1, p0, Lnetwork/InterfaceImpl/SSLPacketHandler;->m_reader:Lnetwork/Interface/PacketReader;

    return-void
.end method

.method public setWriter(Lnetwork/Interface/PacketWriter;)V
    .locals 0
    .param p1    # Lnetwork/Interface/PacketWriter;

    iput-object p1, p0, Lnetwork/InterfaceImpl/SSLPacketHandler;->m_writer:Lnetwork/Interface/PacketWriter;

    return-void
.end method

.method public write([Ljava/nio/ByteBuffer;)[Ljava/nio/ByteBuffer;
    .locals 8
    .param p1    # [Ljava/nio/ByteBuffer;

    const/4 v5, 0x0

    iget-boolean v6, p0, Lnetwork/InterfaceImpl/SSLPacketHandler;->m_sslInitiated:Z

    if-nez v6, :cond_1

    iget-object v5, p0, Lnetwork/InterfaceImpl/SSLPacketHandler;->m_writer:Lnetwork/Interface/PacketWriter;

    invoke-interface {v5, p1}, Lnetwork/Interface/PacketWriter;->write([Ljava/nio/ByteBuffer;)[Ljava/nio/ByteBuffer;

    move-result-object v5

    :cond_0
    :goto_0
    return-object v5

    :cond_1
    iget-object v6, p0, Lnetwork/InterfaceImpl/SSLPacketHandler;->m_engine:Ljavax/net/ssl/SSLEngine;

    invoke-virtual {v6}, Ljavax/net/ssl/SSLEngine;->getHandshakeStatus()Ljavax/net/ssl/SSLEngineResult$HandshakeStatus;

    move-result-object v6

    sget-object v7, Ljavax/net/ssl/SSLEngineResult$HandshakeStatus;->NOT_HANDSHAKING:Ljavax/net/ssl/SSLEngineResult$HandshakeStatus;

    if-eq v6, v7, :cond_5

    invoke-static {p1}, Lnetwork/util/NIOUtils;->isEmpty([Ljava/nio/ByteBuffer;)Z

    move-result v6

    if-nez v6, :cond_2

    iget-object v6, p0, Lnetwork/InterfaceImpl/SSLPacketHandler;->m_initialOutBuffer:[Ljava/nio/ByteBuffer;

    iget-object v7, p0, Lnetwork/InterfaceImpl/SSLPacketHandler;->m_writer:Lnetwork/Interface/PacketWriter;

    invoke-interface {v7, p1}, Lnetwork/Interface/PacketWriter;->write([Ljava/nio/ByteBuffer;)[Ljava/nio/ByteBuffer;

    move-result-object v7

    invoke-static {v6, v7}, Lnetwork/util/NIOUtils;->concat([Ljava/nio/ByteBuffer;[Ljava/nio/ByteBuffer;)[Ljava/nio/ByteBuffer;

    move-result-object v6

    iput-object v6, p0, Lnetwork/InterfaceImpl/SSLPacketHandler;->m_initialOutBuffer:[Ljava/nio/ByteBuffer;

    const/4 v6, 0x0

    new-array p1, v6, [Ljava/nio/ByteBuffer;

    :cond_2
    sget-object v6, Lnetwork/InterfaceImpl/SSLPacketHandler;->SSL_BUFFER:Ljava/lang/ThreadLocal;

    invoke-virtual {v6}, Ljava/lang/ThreadLocal;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/nio/ByteBuffer;

    move-object v1, v5

    check-cast v1, [Ljava/nio/ByteBuffer;

    const/4 v4, 0x0

    :goto_1
    :try_start_0
    iget-object v6, p0, Lnetwork/InterfaceImpl/SSLPacketHandler;->m_engine:Ljavax/net/ssl/SSLEngine;

    invoke-virtual {v6}, Ljavax/net/ssl/SSLEngine;->getHandshakeStatus()Ljavax/net/ssl/SSLEngineResult$HandshakeStatus;

    move-result-object v6

    sget-object v7, Ljavax/net/ssl/SSLEngineResult$HandshakeStatus;->NEED_WRAP:Ljavax/net/ssl/SSLEngineResult$HandshakeStatus;

    if-eq v6, v7, :cond_3

    if-eqz v4, :cond_0

    invoke-virtual {v4}, Ljavax/net/ssl/SSLEngineResult;->getStatus()Ljavax/net/ssl/SSLEngineResult$Status;

    move-result-object v5

    sget-object v6, Ljavax/net/ssl/SSLEngineResult$Status;->OK:Ljavax/net/ssl/SSLEngineResult$Status;

    if-eq v5, v6, :cond_4

    new-instance v5, Ljavax/net/ssl/SSLException;

    new-instance v6, Ljava/lang/StringBuilder;

    const-string v7, "Unexpectedly not ok wrapping handshake data, was "

    invoke-direct {v6, v7}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v4}, Ljavax/net/ssl/SSLEngineResult;->getStatus()Ljavax/net/ssl/SSLEngineResult$Status;

    move-result-object v7

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v6}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v6

    invoke-direct {v5, v6}, Ljavax/net/ssl/SSLException;-><init>(Ljava/lang/String;)V

    throw v5
    :try_end_0
    .catch Ljavax/net/ssl/SSLException; {:try_start_0 .. :try_end_0} :catch_0

    :catch_0
    move-exception v2

    new-instance v5, Ljava/lang/RuntimeException;

    invoke-direct {v5, v2}, Ljava/lang/RuntimeException;-><init>(Ljava/lang/Throwable;)V

    throw v5

    :cond_3
    :try_start_1
    invoke-virtual {v0}, Ljava/nio/ByteBuffer;->clear()Ljava/nio/Buffer;

    iget-object v6, p0, Lnetwork/InterfaceImpl/SSLPacketHandler;->m_engine:Ljavax/net/ssl/SSLEngine;

    invoke-virtual {v6, p1, v0}, Ljavax/net/ssl/SSLEngine;->wrap([Ljava/nio/ByteBuffer;Ljava/nio/ByteBuffer;)Ljavax/net/ssl/SSLEngineResult;

    move-result-object v4

    invoke-virtual {v0}, Ljava/nio/ByteBuffer;->flip()Ljava/nio/Buffer;

    invoke-static {v0}, Lnetwork/util/NIOUtils;->copy(Ljava/nio/ByteBuffer;)Ljava/nio/ByteBuffer;

    move-result-object v6

    invoke-static {v1, v6}, Lnetwork/util/NIOUtils;->concat([Ljava/nio/ByteBuffer;Ljava/nio/ByteBuffer;)[Ljava/nio/ByteBuffer;

    move-result-object v1

    goto :goto_1

    :cond_4
    invoke-virtual {v4}, Ljavax/net/ssl/SSLEngineResult;->getHandshakeStatus()Ljavax/net/ssl/SSLEngineResult$HandshakeStatus;

    move-result-object v5

    invoke-direct {p0, v5}, Lnetwork/InterfaceImpl/SSLPacketHandler;->reactToHandshakeStatus(Ljavax/net/ssl/SSLEngineResult$HandshakeStatus;)V
    :try_end_1
    .catch Ljavax/net/ssl/SSLException; {:try_start_1 .. :try_end_1} :catch_0

    move-object v5, v1

    goto :goto_0

    :cond_5
    sget-object v6, Lnetwork/InterfaceImpl/SSLPacketHandler;->SSL_BUFFER:Ljava/lang/ThreadLocal;

    invoke-virtual {v6}, Ljava/lang/ThreadLocal;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/nio/ByteBuffer;

    invoke-virtual {v0}, Ljava/nio/ByteBuffer;->clear()Ljava/nio/Buffer;

    invoke-static {p1}, Lnetwork/util/NIOUtils;->isEmpty([Ljava/nio/ByteBuffer;)Z

    move-result v6

    if-eqz v6, :cond_7

    iget-object v6, p0, Lnetwork/InterfaceImpl/SSLPacketHandler;->m_initialOutBuffer:[Ljava/nio/ByteBuffer;

    if-eqz v6, :cond_0

    :goto_2
    iget-object v6, p0, Lnetwork/InterfaceImpl/SSLPacketHandler;->m_initialOutBuffer:[Ljava/nio/ByteBuffer;

    if-eqz v6, :cond_6

    iget-object v6, p0, Lnetwork/InterfaceImpl/SSLPacketHandler;->m_initialOutBuffer:[Ljava/nio/ByteBuffer;

    invoke-static {v6, p1}, Lnetwork/util/NIOUtils;->concat([Ljava/nio/ByteBuffer;[Ljava/nio/ByteBuffer;)[Ljava/nio/ByteBuffer;

    move-result-object p1

    iput-object v5, p0, Lnetwork/InterfaceImpl/SSLPacketHandler;->m_initialOutBuffer:[Ljava/nio/ByteBuffer;

    :cond_6
    move-object v3, v5

    check-cast v3, [Ljava/nio/ByteBuffer;

    :goto_3
    invoke-static {p1}, Lnetwork/util/NIOUtils;->isEmpty([Ljava/nio/ByteBuffer;)Z

    move-result v5

    if-eqz v5, :cond_8

    move-object v5, v3

    goto/16 :goto_0

    :cond_7
    iget-object v6, p0, Lnetwork/InterfaceImpl/SSLPacketHandler;->m_writer:Lnetwork/Interface/PacketWriter;

    invoke-interface {v6, p1}, Lnetwork/Interface/PacketWriter;->write([Ljava/nio/ByteBuffer;)[Ljava/nio/ByteBuffer;

    move-result-object p1

    goto :goto_2

    :cond_8
    invoke-virtual {v0}, Ljava/nio/ByteBuffer;->clear()Ljava/nio/Buffer;

    :try_start_2
    iget-object v5, p0, Lnetwork/InterfaceImpl/SSLPacketHandler;->m_engine:Ljavax/net/ssl/SSLEngine;

    invoke-virtual {v5, p1, v0}, Ljavax/net/ssl/SSLEngine;->wrap([Ljava/nio/ByteBuffer;Ljava/nio/ByteBuffer;)Ljavax/net/ssl/SSLEngineResult;
    :try_end_2
    .catch Ljavax/net/ssl/SSLException; {:try_start_2 .. :try_end_2} :catch_1

    invoke-virtual {v0}, Ljava/nio/ByteBuffer;->flip()Ljava/nio/Buffer;

    invoke-static {v0}, Lnetwork/util/NIOUtils;->copy(Ljava/nio/ByteBuffer;)Ljava/nio/ByteBuffer;

    move-result-object v5

    invoke-static {v3, v5}, Lnetwork/util/NIOUtils;->concat([Ljava/nio/ByteBuffer;Ljava/nio/ByteBuffer;)[Ljava/nio/ByteBuffer;

    move-result-object v3

    goto :goto_3

    :catch_1
    move-exception v2

    new-instance v5, Ljava/lang/RuntimeException;

    invoke-direct {v5, v2}, Ljava/lang/RuntimeException;-><init>(Ljava/lang/Throwable;)V

    throw v5
.end method
