.class Lnetwork/InterfaceImpl/SocketChannelResponder$BeginListenEvent;
.super Ljava/lang/Object;
.source "SocketChannelResponder.java"

# interfaces
.implements Ljava/lang/Runnable;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lnetwork/InterfaceImpl/SocketChannelResponder;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x2
    name = "BeginListenEvent"
.end annotation


# instance fields
.field private final m_newObserver:Lnetwork/Interface/SocketObserver;

.field private final m_responder:Lnetwork/InterfaceImpl/SocketChannelResponder;

.field final synthetic this$0:Lnetwork/InterfaceImpl/SocketChannelResponder;


# direct methods
.method private constructor <init>(Lnetwork/InterfaceImpl/SocketChannelResponder;Lnetwork/InterfaceImpl/SocketChannelResponder;Lnetwork/Interface/SocketObserver;)V
    .locals 0
    .param p2    # Lnetwork/InterfaceImpl/SocketChannelResponder;
    .param p3    # Lnetwork/Interface/SocketObserver;

    iput-object p1, p0, Lnetwork/InterfaceImpl/SocketChannelResponder$BeginListenEvent;->this$0:Lnetwork/InterfaceImpl/SocketChannelResponder;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    iput-object p2, p0, Lnetwork/InterfaceImpl/SocketChannelResponder$BeginListenEvent;->m_responder:Lnetwork/InterfaceImpl/SocketChannelResponder;

    iput-object p3, p0, Lnetwork/InterfaceImpl/SocketChannelResponder$BeginListenEvent;->m_newObserver:Lnetwork/Interface/SocketObserver;

    return-void
.end method

.method synthetic constructor <init>(Lnetwork/InterfaceImpl/SocketChannelResponder;Lnetwork/InterfaceImpl/SocketChannelResponder;Lnetwork/Interface/SocketObserver;Lnetwork/InterfaceImpl/SocketChannelResponder$BeginListenEvent;)V
    .locals 0

    invoke-direct {p0, p1, p2, p3}, Lnetwork/InterfaceImpl/SocketChannelResponder$BeginListenEvent;-><init>(Lnetwork/InterfaceImpl/SocketChannelResponder;Lnetwork/InterfaceImpl/SocketChannelResponder;Lnetwork/Interface/SocketObserver;)V

    return-void
.end method


# virtual methods
.method public run()V
    .locals 2

    iget-object v0, p0, Lnetwork/InterfaceImpl/SocketChannelResponder$BeginListenEvent;->m_responder:Lnetwork/InterfaceImpl/SocketChannelResponder;

    iget-object v1, p0, Lnetwork/InterfaceImpl/SocketChannelResponder$BeginListenEvent;->m_newObserver:Lnetwork/Interface/SocketObserver;

    invoke-static {v0, v1}, Lnetwork/InterfaceImpl/SocketChannelResponder;->access$1(Lnetwork/InterfaceImpl/SocketChannelResponder;Lnetwork/Interface/SocketObserver;)V

    iget-object v0, p0, Lnetwork/InterfaceImpl/SocketChannelResponder$BeginListenEvent;->m_responder:Lnetwork/InterfaceImpl/SocketChannelResponder;

    invoke-virtual {v0}, Lnetwork/InterfaceImpl/SocketChannelResponder;->isConnected()Z

    move-result v0

    if-eqz v0, :cond_0

    iget-object v0, p0, Lnetwork/InterfaceImpl/SocketChannelResponder$BeginListenEvent;->m_responder:Lnetwork/InterfaceImpl/SocketChannelResponder;

    # invokes: Lnetwork/InterfaceImpl/SocketChannelResponder;->notifyObserverOfConnect()V
    invoke-static {v0}, Lnetwork/InterfaceImpl/SocketChannelResponder;->access$2(Lnetwork/InterfaceImpl/SocketChannelResponder;)V

    :cond_0
    iget-object v0, p0, Lnetwork/InterfaceImpl/SocketChannelResponder$BeginListenEvent;->m_responder:Lnetwork/InterfaceImpl/SocketChannelResponder;

    invoke-virtual {v0}, Lnetwork/InterfaceImpl/SocketChannelResponder;->isOpen()Z

    move-result v0

    if-nez v0, :cond_1

    iget-object v0, p0, Lnetwork/InterfaceImpl/SocketChannelResponder$BeginListenEvent;->m_responder:Lnetwork/InterfaceImpl/SocketChannelResponder;

    const/4 v1, 0x0

    # invokes: Lnetwork/InterfaceImpl/SocketChannelResponder;->notifyObserverOfDisconnect(Ljava/lang/Exception;)V
    invoke-static {v0, v1}, Lnetwork/InterfaceImpl/SocketChannelResponder;->access$3(Lnetwork/InterfaceImpl/SocketChannelResponder;Ljava/lang/Exception;)V

    :cond_1
    iget-object v0, p0, Lnetwork/InterfaceImpl/SocketChannelResponder$BeginListenEvent;->m_responder:Lnetwork/InterfaceImpl/SocketChannelResponder;

    const/4 v1, 0x1

    invoke-virtual {v0, v1}, Lnetwork/InterfaceImpl/SocketChannelResponder;->addInterest(I)V

    return-void
.end method

.method public toString()Ljava/lang/String;
    .locals 2

    new-instance v0, Ljava/lang/StringBuilder;

    const-string v1, "BeginListen["

    invoke-direct {v0, v1}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    iget-object v1, p0, Lnetwork/InterfaceImpl/SocketChannelResponder$BeginListenEvent;->m_newObserver:Lnetwork/Interface/SocketObserver;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, "]"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method
