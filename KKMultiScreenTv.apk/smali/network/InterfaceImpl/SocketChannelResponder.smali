.class public Lnetwork/InterfaceImpl/SocketChannelResponder;
.super Lnetwork/InterfaceImpl/ChannelResponder;
.source "SocketChannelResponder.java"

# interfaces
.implements Lnetwork/Interface/NIOSocket;


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lnetwork/InterfaceImpl/SocketChannelResponder$AddInterestEvent;,
        Lnetwork/InterfaceImpl/SocketChannelResponder$BeginListenEvent;
    }
.end annotation


# instance fields
.field private final m_bytesInQueue:Ljava/util/concurrent/atomic/AtomicLong;

.field private m_maxQueueSize:I

.field private m_packetQueue:Ljava/util/concurrent/ConcurrentLinkedQueue;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/concurrent/ConcurrentLinkedQueue",
            "<",
            "Ljava/lang/Object;",
            ">;"
        }
    .end annotation
.end field

.field private m_packetReader:Lnetwork/Interface/PacketReader;

.field private volatile m_socketObserver:Lnetwork/Interface/SocketObserver;

.field private final m_socketReader:Lnetwork/Interface/SocketReader;

.field private final m_socketWriter:Lnetwork/Interface/SocketWriter;

.field private m_timeOpened:J


# direct methods
.method public constructor <init>(Lnetwork/NetCore/asyncService;Ljava/nio/channels/SocketChannel;Ljava/net/InetSocketAddress;)V
    .locals 3
    .param p1    # Lnetwork/NetCore/asyncService;
    .param p2    # Ljava/nio/channels/SocketChannel;
    .param p3    # Ljava/net/InetSocketAddress;

    invoke-direct {p0, p1, p2, p3}, Lnetwork/InterfaceImpl/ChannelResponder;-><init>(Lnetwork/NetCore/asyncService;Ljava/nio/channels/SelectableChannel;Ljava/net/InetSocketAddress;)V

    const/4 v0, 0x0

    iput-object v0, p0, Lnetwork/InterfaceImpl/SocketChannelResponder;->m_socketObserver:Lnetwork/Interface/SocketObserver;

    const/4 v0, -0x1

    iput v0, p0, Lnetwork/InterfaceImpl/SocketChannelResponder;->m_maxQueueSize:I

    const-wide/16 v0, -0x1

    iput-wide v0, p0, Lnetwork/InterfaceImpl/SocketChannelResponder;->m_timeOpened:J

    sget-object v0, Lnetwork/NetIO/packetreader/RawPacketReader;->INSTANCE:Lnetwork/NetIO/packetreader/RawPacketReader;

    iput-object v0, p0, Lnetwork/InterfaceImpl/SocketChannelResponder;->m_packetReader:Lnetwork/Interface/PacketReader;

    new-instance v0, Ljava/util/concurrent/atomic/AtomicLong;

    const-wide/16 v1, 0x0

    invoke-direct {v0, v1, v2}, Ljava/util/concurrent/atomic/AtomicLong;-><init>(J)V

    iput-object v0, p0, Lnetwork/InterfaceImpl/SocketChannelResponder;->m_bytesInQueue:Ljava/util/concurrent/atomic/AtomicLong;

    new-instance v0, Ljava/util/concurrent/ConcurrentLinkedQueue;

    invoke-direct {v0}, Ljava/util/concurrent/ConcurrentLinkedQueue;-><init>()V

    iput-object v0, p0, Lnetwork/InterfaceImpl/SocketChannelResponder;->m_packetQueue:Ljava/util/concurrent/ConcurrentLinkedQueue;

    new-instance v0, Lnetwork/Interface/SocketReader;

    invoke-direct {v0, p1}, Lnetwork/Interface/SocketReader;-><init>(Lnetwork/NetCore/asyncService;)V

    iput-object v0, p0, Lnetwork/InterfaceImpl/SocketChannelResponder;->m_socketReader:Lnetwork/Interface/SocketReader;

    new-instance v0, Lnetwork/Interface/SocketWriter;

    invoke-direct {v0}, Lnetwork/Interface/SocketWriter;-><init>()V

    iput-object v0, p0, Lnetwork/InterfaceImpl/SocketChannelResponder;->m_socketWriter:Lnetwork/Interface/SocketWriter;

    return-void
.end method

.method static synthetic access$1(Lnetwork/InterfaceImpl/SocketChannelResponder;Lnetwork/Interface/SocketObserver;)V
    .locals 0

    iput-object p1, p0, Lnetwork/InterfaceImpl/SocketChannelResponder;->m_socketObserver:Lnetwork/Interface/SocketObserver;

    return-void
.end method

.method static synthetic access$2(Lnetwork/InterfaceImpl/SocketChannelResponder;)V
    .locals 0

    invoke-direct {p0}, Lnetwork/InterfaceImpl/SocketChannelResponder;->notifyObserverOfConnect()V

    return-void
.end method

.method static synthetic access$3(Lnetwork/InterfaceImpl/SocketChannelResponder;Ljava/lang/Exception;)V
    .locals 0

    invoke-direct {p0, p1}, Lnetwork/InterfaceImpl/SocketChannelResponder;->notifyObserverOfDisconnect(Ljava/lang/Exception;)V

    return-void
.end method

.method static synthetic access$4(Lnetwork/InterfaceImpl/SocketChannelResponder;)Ljava/util/concurrent/ConcurrentLinkedQueue;
    .locals 1

    iget-object v0, p0, Lnetwork/InterfaceImpl/SocketChannelResponder;->m_packetQueue:Ljava/util/concurrent/ConcurrentLinkedQueue;

    return-object v0
.end method

.method static synthetic access$5(Lnetwork/InterfaceImpl/SocketChannelResponder;)Lnetwork/Interface/SocketWriter;
    .locals 1

    iget-object v0, p0, Lnetwork/InterfaceImpl/SocketChannelResponder;->m_socketWriter:Lnetwork/Interface/SocketWriter;

    return-object v0
.end method

.method private fillCurrentOutgoingBuffer()V
    .locals 6
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    iget-object v3, p0, Lnetwork/InterfaceImpl/SocketChannelResponder;->m_socketWriter:Lnetwork/Interface/SocketWriter;

    invoke-virtual {v3}, Lnetwork/Interface/SocketWriter;->isEmpty()Z

    move-result v3

    if-eqz v3, :cond_1

    iget-object v3, p0, Lnetwork/InterfaceImpl/SocketChannelResponder;->m_packetQueue:Ljava/util/concurrent/ConcurrentLinkedQueue;

    invoke-virtual {v3}, Ljava/util/concurrent/ConcurrentLinkedQueue;->poll()Ljava/lang/Object;

    move-result-object v1

    :goto_0
    if-eqz v1, :cond_0

    instance-of v3, v1, Ljava/lang/Runnable;

    if-nez v3, :cond_2

    :cond_0
    if-nez v1, :cond_3

    :cond_1
    :goto_1
    return-void

    :cond_2
    check-cast v1, Ljava/lang/Runnable;

    invoke-interface {v1}, Ljava/lang/Runnable;->run()V

    iget-object v3, p0, Lnetwork/InterfaceImpl/SocketChannelResponder;->m_packetQueue:Ljava/util/concurrent/ConcurrentLinkedQueue;

    invoke-virtual {v3}, Ljava/util/concurrent/ConcurrentLinkedQueue;->poll()Ljava/lang/Object;

    move-result-object v1

    goto :goto_0

    :cond_3
    const/4 v2, 0x0

    instance-of v3, v1, [B

    if-eqz v3, :cond_4

    move-object v0, v1

    check-cast v0, [B

    :goto_2
    iget-object v3, p0, Lnetwork/InterfaceImpl/SocketChannelResponder;->m_socketWriter:Lnetwork/Interface/SocketWriter;

    invoke-virtual {v3, v0, v2}, Lnetwork/Interface/SocketWriter;->setPacket([BLjava/lang/Object;)V

    iget-object v3, p0, Lnetwork/InterfaceImpl/SocketChannelResponder;->m_bytesInQueue:Ljava/util/concurrent/atomic/AtomicLong;

    array-length v4, v0

    neg-int v4, v4

    int-to-long v4, v4

    invoke-virtual {v3, v4, v5}, Ljava/util/concurrent/atomic/AtomicLong;->addAndGet(J)J

    goto :goto_1

    :cond_4
    move-object v3, v1

    check-cast v3, [Ljava/lang/Object;

    const/4 v4, 0x0

    aget-object v0, v3, v4

    check-cast v0, [B

    check-cast v1, [Ljava/lang/Object;

    const/4 v3, 0x1

    aget-object v2, v1, v3

    goto :goto_2
.end method

.method private notifyObserverOfConnect()V
    .locals 2

    :try_start_0
    iget-object v1, p0, Lnetwork/InterfaceImpl/SocketChannelResponder;->m_socketObserver:Lnetwork/Interface/SocketObserver;

    if-eqz v1, :cond_0

    iget-object v1, p0, Lnetwork/InterfaceImpl/SocketChannelResponder;->m_socketObserver:Lnetwork/Interface/SocketObserver;

    invoke-interface {v1, p0}, Lnetwork/Interface/SocketObserver;->connectionOpened(Lnetwork/Interface/NIOSocket;)V
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    :cond_0
    :goto_0
    return-void

    :catch_0
    move-exception v0

    invoke-virtual {p0}, Lnetwork/InterfaceImpl/SocketChannelResponder;->getasyncService()Lnetwork/NetCore/asyncService;

    move-result-object v1

    invoke-virtual {v1, v0}, Lnetwork/NetCore/asyncService;->notifyException(Ljava/lang/Throwable;)V

    goto :goto_0
.end method

.method private notifyObserverOfDisconnect(Ljava/lang/Exception;)V
    .locals 2
    .param p1    # Ljava/lang/Exception;

    :try_start_0
    iget-object v1, p0, Lnetwork/InterfaceImpl/SocketChannelResponder;->m_socketObserver:Lnetwork/Interface/SocketObserver;

    if-eqz v1, :cond_0

    iget-object v1, p0, Lnetwork/InterfaceImpl/SocketChannelResponder;->m_socketObserver:Lnetwork/Interface/SocketObserver;

    invoke-interface {v1, p0, p1}, Lnetwork/Interface/SocketObserver;->connectionBroken(Lnetwork/Interface/NIOSocket;Ljava/lang/Exception;)V
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    :cond_0
    :goto_0
    return-void

    :catch_0
    move-exception v0

    invoke-virtual {p0}, Lnetwork/InterfaceImpl/SocketChannelResponder;->getasyncService()Lnetwork/NetCore/asyncService;

    move-result-object v1

    invoke-virtual {v1, v0}, Lnetwork/NetCore/asyncService;->notifyException(Ljava/lang/Throwable;)V

    goto :goto_0
.end method

.method private notifyPacketReceived([B)V
    .locals 2
    .param p1    # [B

    :try_start_0
    iget-object v1, p0, Lnetwork/InterfaceImpl/SocketChannelResponder;->m_socketObserver:Lnetwork/Interface/SocketObserver;

    if-eqz v1, :cond_0

    iget-object v1, p0, Lnetwork/InterfaceImpl/SocketChannelResponder;->m_socketObserver:Lnetwork/Interface/SocketObserver;

    invoke-interface {v1, p0, p1}, Lnetwork/Interface/SocketObserver;->packetReceived(Lnetwork/Interface/NIOSocket;[B)V
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    :cond_0
    :goto_0
    return-void

    :catch_0
    move-exception v0

    invoke-virtual {p0}, Lnetwork/InterfaceImpl/SocketChannelResponder;->getasyncService()Lnetwork/NetCore/asyncService;

    move-result-object v1

    invoke-virtual {v1, v0}, Lnetwork/NetCore/asyncService;->notifyException(Ljava/lang/Throwable;)V

    goto :goto_0
.end method

.method private notifyPacketSent(Ljava/lang/Object;)V
    .locals 2
    .param p1    # Ljava/lang/Object;

    :try_start_0
    iget-object v1, p0, Lnetwork/InterfaceImpl/SocketChannelResponder;->m_socketObserver:Lnetwork/Interface/SocketObserver;

    if-eqz v1, :cond_0

    iget-object v1, p0, Lnetwork/InterfaceImpl/SocketChannelResponder;->m_socketObserver:Lnetwork/Interface/SocketObserver;

    invoke-interface {v1, p0, p1}, Lnetwork/Interface/SocketObserver;->packetSent(Lnetwork/Interface/NIOSocket;Ljava/lang/Object;)V
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    :cond_0
    :goto_0
    return-void

    :catch_0
    move-exception v0

    invoke-virtual {p0}, Lnetwork/InterfaceImpl/SocketChannelResponder;->getasyncService()Lnetwork/NetCore/asyncService;

    move-result-object v1

    invoke-virtual {v1, v0}, Lnetwork/NetCore/asyncService;->notifyException(Ljava/lang/Throwable;)V

    goto :goto_0
.end method


# virtual methods
.method public closeAfterWrite()V
    .locals 1

    new-instance v0, Lnetwork/InterfaceImpl/SocketChannelResponder$1;

    invoke-direct {v0, p0}, Lnetwork/InterfaceImpl/SocketChannelResponder$1;-><init>(Lnetwork/InterfaceImpl/SocketChannelResponder;)V

    invoke-virtual {p0, v0}, Lnetwork/InterfaceImpl/SocketChannelResponder;->queue(Ljava/lang/Runnable;)V

    return-void
.end method

.method public getBytesRead()J
    .locals 2

    iget-object v0, p0, Lnetwork/InterfaceImpl/SocketChannelResponder;->m_socketReader:Lnetwork/Interface/SocketReader;

    invoke-virtual {v0}, Lnetwork/Interface/SocketReader;->getBytesRead()J

    move-result-wide v0

    return-wide v0
.end method

.method public getBytesWritten()J
    .locals 2

    iget-object v0, p0, Lnetwork/InterfaceImpl/SocketChannelResponder;->m_socketWriter:Lnetwork/Interface/SocketWriter;

    invoke-virtual {v0}, Lnetwork/Interface/SocketWriter;->getBytesWritten()J

    move-result-wide v0

    return-wide v0
.end method

.method public bridge synthetic getChannel()Ljava/nio/channels/SelectableChannel;
    .locals 1

    invoke-virtual {p0}, Lnetwork/InterfaceImpl/SocketChannelResponder;->getChannel()Ljava/nio/channels/SocketChannel;

    move-result-object v0

    return-object v0
.end method

.method public getChannel()Ljava/nio/channels/SocketChannel;
    .locals 1

    invoke-super {p0}, Lnetwork/InterfaceImpl/ChannelResponder;->getChannel()Ljava/nio/channels/SelectableChannel;

    move-result-object v0

    check-cast v0, Ljava/nio/channels/SocketChannel;

    return-object v0
.end method

.method public getMaxQueueSize()I
    .locals 1

    iget v0, p0, Lnetwork/InterfaceImpl/SocketChannelResponder;->m_maxQueueSize:I

    return v0
.end method

.method public getSocket()Ljava/net/Socket;
    .locals 1

    invoke-virtual {p0}, Lnetwork/InterfaceImpl/SocketChannelResponder;->getChannel()Ljava/nio/channels/SocketChannel;

    move-result-object v0

    invoke-virtual {v0}, Ljava/nio/channels/SocketChannel;->socket()Ljava/net/Socket;

    move-result-object v0

    return-object v0
.end method

.method public getTimeOpen()J
    .locals 4

    iget-wide v0, p0, Lnetwork/InterfaceImpl/SocketChannelResponder;->m_timeOpened:J

    const-wide/16 v2, 0x0

    cmp-long v0, v0, v2

    if-lez v0, :cond_0

    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v0

    iget-wide v2, p0, Lnetwork/InterfaceImpl/SocketChannelResponder;->m_timeOpened:J

    sub-long/2addr v0, v2

    :goto_0
    return-wide v0

    :cond_0
    const-wide/16 v0, -0x1

    goto :goto_0
.end method

.method public getWriteQueueSize()J
    .locals 2

    iget-object v0, p0, Lnetwork/InterfaceImpl/SocketChannelResponder;->m_bytesInQueue:Ljava/util/concurrent/atomic/AtomicLong;

    invoke-virtual {v0}, Ljava/util/concurrent/atomic/AtomicLong;->get()J

    move-result-wide v0

    return-wide v0
.end method

.method public isConnected()Z
    .locals 1

    invoke-virtual {p0}, Lnetwork/InterfaceImpl/SocketChannelResponder;->getChannel()Ljava/nio/channels/SocketChannel;

    move-result-object v0

    invoke-virtual {v0}, Ljava/nio/channels/SocketChannel;->isConnected()Z

    move-result v0

    return v0
.end method

.method keyInitialized()V
    .locals 1

    invoke-virtual {p0}, Lnetwork/InterfaceImpl/SocketChannelResponder;->isConnected()Z

    move-result v0

    if-nez v0, :cond_0

    const/16 v0, 0x8

    invoke-virtual {p0, v0}, Lnetwork/InterfaceImpl/SocketChannelResponder;->addInterest(I)V

    :cond_0
    return-void
.end method

.method public listen(Lnetwork/Interface/SocketObserver;)V
    .locals 3
    .param p1    # Lnetwork/Interface/SocketObserver;

    invoke-virtual {p0}, Lnetwork/InterfaceImpl/SocketChannelResponder;->markObserverSet()V

    invoke-virtual {p0}, Lnetwork/InterfaceImpl/SocketChannelResponder;->getasyncService()Lnetwork/NetCore/asyncService;

    move-result-object v0

    new-instance v1, Lnetwork/InterfaceImpl/SocketChannelResponder$BeginListenEvent;

    if-nez p1, :cond_0

    sget-object p1, Lnetwork/Interface/SocketObserver;->NULL:Lnetwork/Interface/SocketObserver;

    :cond_0
    const/4 v2, 0x0

    invoke-direct {v1, p0, p0, p1, v2}, Lnetwork/InterfaceImpl/SocketChannelResponder$BeginListenEvent;-><init>(Lnetwork/InterfaceImpl/SocketChannelResponder;Lnetwork/InterfaceImpl/SocketChannelResponder;Lnetwork/Interface/SocketObserver;Lnetwork/InterfaceImpl/SocketChannelResponder$BeginListenEvent;)V

    invoke-virtual {v0, v1}, Lnetwork/NetCore/asyncService;->queue(Ljava/lang/Runnable;)V

    return-void
.end method

.method public notifyWasCancelled()V
    .locals 0

    invoke-virtual {p0}, Lnetwork/InterfaceImpl/SocketChannelResponder;->close()V

    return-void
.end method

.method public queue(Ljava/lang/Runnable;)V
    .locals 4
    .param p1    # Ljava/lang/Runnable;

    iget-object v0, p0, Lnetwork/InterfaceImpl/SocketChannelResponder;->m_packetQueue:Ljava/util/concurrent/ConcurrentLinkedQueue;

    invoke-virtual {v0, p1}, Ljava/util/concurrent/ConcurrentLinkedQueue;->offer(Ljava/lang/Object;)Z

    invoke-virtual {p0}, Lnetwork/InterfaceImpl/SocketChannelResponder;->getasyncService()Lnetwork/NetCore/asyncService;

    move-result-object v0

    new-instance v1, Lnetwork/InterfaceImpl/SocketChannelResponder$AddInterestEvent;

    const/4 v2, 0x4

    const/4 v3, 0x0

    invoke-direct {v1, p0, v2, v3}, Lnetwork/InterfaceImpl/SocketChannelResponder$AddInterestEvent;-><init>(Lnetwork/InterfaceImpl/SocketChannelResponder;ILnetwork/InterfaceImpl/SocketChannelResponder$AddInterestEvent;)V

    invoke-virtual {v0, v1}, Lnetwork/NetCore/asyncService;->queue(Ljava/lang/Runnable;)V

    return-void
.end method

.method public setMaxQueueSize(I)V
    .locals 0
    .param p1    # I

    iput p1, p0, Lnetwork/InterfaceImpl/SocketChannelResponder;->m_maxQueueSize:I

    return-void
.end method

.method public setPacketReader(Lnetwork/Interface/PacketReader;)V
    .locals 0
    .param p1    # Lnetwork/Interface/PacketReader;

    iput-object p1, p0, Lnetwork/InterfaceImpl/SocketChannelResponder;->m_packetReader:Lnetwork/Interface/PacketReader;

    return-void
.end method

.method public setPacketWriter(Lnetwork/Interface/PacketWriter;)V
    .locals 1
    .param p1    # Lnetwork/Interface/PacketWriter;

    if-nez p1, :cond_0

    new-instance v0, Ljava/lang/NullPointerException;

    invoke-direct {v0}, Ljava/lang/NullPointerException;-><init>()V

    throw v0

    :cond_0
    new-instance v0, Lnetwork/InterfaceImpl/SocketChannelResponder$2;

    invoke-direct {v0, p0, p1}, Lnetwork/InterfaceImpl/SocketChannelResponder$2;-><init>(Lnetwork/InterfaceImpl/SocketChannelResponder;Lnetwork/Interface/PacketWriter;)V

    invoke-virtual {p0, v0}, Lnetwork/InterfaceImpl/SocketChannelResponder;->queue(Ljava/lang/Runnable;)V

    return-void
.end method

.method protected shutdown(Ljava/lang/Exception;)V
    .locals 3
    .param p1    # Ljava/lang/Exception;

    const-wide/16 v0, -0x1

    iput-wide v0, p0, Lnetwork/InterfaceImpl/SocketChannelResponder;->m_timeOpened:J

    iget-object v0, p0, Lnetwork/InterfaceImpl/SocketChannelResponder;->m_packetQueue:Ljava/util/concurrent/ConcurrentLinkedQueue;

    invoke-virtual {v0}, Ljava/util/concurrent/ConcurrentLinkedQueue;->clear()V

    iget-object v0, p0, Lnetwork/InterfaceImpl/SocketChannelResponder;->m_bytesInQueue:Ljava/util/concurrent/atomic/AtomicLong;

    const-wide/16 v1, 0x0

    invoke-virtual {v0, v1, v2}, Ljava/util/concurrent/atomic/AtomicLong;->set(J)V

    invoke-direct {p0, p1}, Lnetwork/InterfaceImpl/SocketChannelResponder;->notifyObserverOfDisconnect(Ljava/lang/Exception;)V

    return-void
.end method

.method public socket()Ljava/net/Socket;
    .locals 1

    invoke-virtual {p0}, Lnetwork/InterfaceImpl/SocketChannelResponder;->getChannel()Ljava/nio/channels/SocketChannel;

    move-result-object v0

    invoke-virtual {v0}, Ljava/nio/channels/SocketChannel;->socket()Ljava/net/Socket;

    move-result-object v0

    return-object v0
.end method

.method public socketReadyForConnect()V
    .locals 3

    :try_start_0
    invoke-virtual {p0}, Lnetwork/InterfaceImpl/SocketChannelResponder;->isOpen()Z

    move-result v1

    if-nez v1, :cond_1

    :cond_0
    :goto_0
    return-void

    :cond_1
    invoke-virtual {p0}, Lnetwork/InterfaceImpl/SocketChannelResponder;->getChannel()Ljava/nio/channels/SocketChannel;

    move-result-object v1

    invoke-virtual {v1}, Ljava/nio/channels/SocketChannel;->finishConnect()Z

    move-result v1

    if-eqz v1, :cond_0

    const/16 v1, 0x8

    invoke-virtual {p0, v1}, Lnetwork/InterfaceImpl/SocketChannelResponder;->deleteInterest(I)V

    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v1

    iput-wide v1, p0, Lnetwork/InterfaceImpl/SocketChannelResponder;->m_timeOpened:J

    invoke-direct {p0}, Lnetwork/InterfaceImpl/SocketChannelResponder;->notifyObserverOfConnect()V
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    goto :goto_0

    :catch_0
    move-exception v0

    invoke-virtual {p0, v0}, Lnetwork/InterfaceImpl/SocketChannelResponder;->close(Ljava/lang/Exception;)V

    goto :goto_0
.end method

.method public socketReadyForRead()V
    .locals 5

    invoke-virtual {p0}, Lnetwork/InterfaceImpl/SocketChannelResponder;->isOpen()Z

    move-result v3

    if-nez v3, :cond_0

    :goto_0
    return-void

    :cond_0
    :try_start_0
    invoke-virtual {p0}, Lnetwork/InterfaceImpl/SocketChannelResponder;->isConnected()Z

    move-result v3

    if-nez v3, :cond_4

    new-instance v3, Ljava/io/IOException;

    const-string v4, "Channel not connected."

    invoke-direct {v3, v4}, Ljava/io/IOException;-><init>(Ljava/lang/String;)V

    throw v3
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    :catch_0
    move-exception v1

    invoke-virtual {p0, v1}, Lnetwork/InterfaceImpl/SocketChannelResponder;->close(Ljava/lang/Exception;)V

    goto :goto_0

    :cond_1
    :try_start_1
    iget-object v3, p0, Lnetwork/InterfaceImpl/SocketChannelResponder;->m_socketReader:Lnetwork/Interface/SocketReader;

    invoke-virtual {v3}, Lnetwork/Interface/SocketReader;->getBuffer()Ljava/nio/ByteBuffer;

    move-result-object v0

    :cond_2
    :goto_1
    invoke-virtual {v0}, Ljava/nio/ByteBuffer;->remaining()I

    move-result v3

    if-lez v3, :cond_3

    iget-object v3, p0, Lnetwork/InterfaceImpl/SocketChannelResponder;->m_packetReader:Lnetwork/Interface/PacketReader;

    invoke-interface {v3, v0}, Lnetwork/Interface/PacketReader;->nextPacket(Ljava/nio/ByteBuffer;)[B

    move-result-object v2

    if-nez v2, :cond_5

    :cond_3
    iget-object v3, p0, Lnetwork/InterfaceImpl/SocketChannelResponder;->m_socketReader:Lnetwork/Interface/SocketReader;

    invoke-virtual {v3}, Lnetwork/Interface/SocketReader;->compact()V

    :cond_4
    iget-object v3, p0, Lnetwork/InterfaceImpl/SocketChannelResponder;->m_socketReader:Lnetwork/Interface/SocketReader;

    invoke-virtual {p0}, Lnetwork/InterfaceImpl/SocketChannelResponder;->getChannel()Ljava/nio/channels/SocketChannel;

    move-result-object v4

    invoke-virtual {v3, v4}, Lnetwork/Interface/SocketReader;->read(Ljava/nio/channels/SocketChannel;)I

    move-result v3

    if-gtz v3, :cond_1

    goto :goto_0

    :cond_5
    sget-object v3, Lnetwork/Interface/PacketReader;->SKIP_PACKET:[B

    if-eq v2, v3, :cond_2

    invoke-direct {p0, v2}, Lnetwork/InterfaceImpl/SocketChannelResponder;->notifyPacketReceived([B)V
    :try_end_1
    .catch Ljava/lang/Exception; {:try_start_1 .. :try_end_1} :catch_0

    goto :goto_1
.end method

.method public socketReadyForWrite()V
    .locals 4

    const/4 v2, 0x4

    :try_start_0
    invoke-virtual {p0, v2}, Lnetwork/InterfaceImpl/SocketChannelResponder;->deleteInterest(I)V

    invoke-virtual {p0}, Lnetwork/InterfaceImpl/SocketChannelResponder;->isOpen()Z

    move-result v2

    if-nez v2, :cond_1

    :cond_0
    :goto_0
    return-void

    :cond_1
    invoke-direct {p0}, Lnetwork/InterfaceImpl/SocketChannelResponder;->fillCurrentOutgoingBuffer()V

    iget-object v2, p0, Lnetwork/InterfaceImpl/SocketChannelResponder;->m_socketWriter:Lnetwork/Interface/SocketWriter;

    invoke-virtual {v2}, Lnetwork/Interface/SocketWriter;->isEmpty()Z

    move-result v2

    if-nez v2, :cond_0

    :cond_2
    :goto_1
    iget-object v2, p0, Lnetwork/InterfaceImpl/SocketChannelResponder;->m_socketWriter:Lnetwork/Interface/SocketWriter;

    invoke-virtual {v2}, Lnetwork/Interface/SocketWriter;->isEmpty()Z

    move-result v2

    if-nez v2, :cond_0

    iget-object v2, p0, Lnetwork/InterfaceImpl/SocketChannelResponder;->m_socketWriter:Lnetwork/Interface/SocketWriter;

    invoke-virtual {p0}, Lnetwork/InterfaceImpl/SocketChannelResponder;->getChannel()Ljava/nio/channels/SocketChannel;

    move-result-object v3

    invoke-virtual {v2, v3}, Lnetwork/Interface/SocketWriter;->write(Ljava/nio/channels/SocketChannel;)Z

    move-result v0

    if-nez v0, :cond_3

    const/4 v2, 0x4

    invoke-virtual {p0, v2}, Lnetwork/InterfaceImpl/SocketChannelResponder;->addInterest(I)V
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    goto :goto_0

    :catch_0
    move-exception v1

    invoke-virtual {p0, v1}, Lnetwork/InterfaceImpl/SocketChannelResponder;->close(Ljava/lang/Exception;)V

    goto :goto_0

    :cond_3
    :try_start_1
    iget-object v2, p0, Lnetwork/InterfaceImpl/SocketChannelResponder;->m_socketWriter:Lnetwork/Interface/SocketWriter;

    invoke-virtual {v2}, Lnetwork/Interface/SocketWriter;->isEmpty()Z

    move-result v2

    if-eqz v2, :cond_2

    iget-object v2, p0, Lnetwork/InterfaceImpl/SocketChannelResponder;->m_socketWriter:Lnetwork/Interface/SocketWriter;

    invoke-virtual {v2}, Lnetwork/Interface/SocketWriter;->getTag()Ljava/lang/Object;

    move-result-object v2

    invoke-direct {p0, v2}, Lnetwork/InterfaceImpl/SocketChannelResponder;->notifyPacketSent(Ljava/lang/Object;)V

    invoke-direct {p0}, Lnetwork/InterfaceImpl/SocketChannelResponder;->fillCurrentOutgoingBuffer()V
    :try_end_1
    .catch Ljava/lang/Exception; {:try_start_1 .. :try_end_1} :catch_0

    goto :goto_1
.end method

.method public toString()Ljava/lang/String;
    .locals 2

    :try_start_0
    invoke-virtual {p0}, Lnetwork/InterfaceImpl/SocketChannelResponder;->getSocket()Ljava/net/Socket;

    move-result-object v1

    invoke-virtual {v1}, Ljava/net/Socket;->toString()Ljava/lang/String;
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    move-result-object v1

    :goto_0
    return-object v1

    :catch_0
    move-exception v0

    const-string v1, "Closed NIO Socket"

    goto :goto_0
.end method

.method public write([B)Z
    .locals 1
    .param p1    # [B

    const/4 v0, 0x0

    invoke-virtual {p0, p1, v0}, Lnetwork/InterfaceImpl/SocketChannelResponder;->write([BLjava/lang/Object;)Z

    move-result v0

    return v0
.end method

.method public write([BLjava/lang/Object;)Z
    .locals 7
    .param p1    # [B
    .param p2    # Ljava/lang/Object;

    const/4 v4, 0x1

    const/4 v3, 0x0

    iget-object v2, p0, Lnetwork/InterfaceImpl/SocketChannelResponder;->m_bytesInQueue:Ljava/util/concurrent/atomic/AtomicLong;

    array-length v5, p1

    int-to-long v5, v5

    invoke-virtual {v2, v5, v6}, Ljava/util/concurrent/atomic/AtomicLong;->addAndGet(J)J

    move-result-wide v0

    iget v2, p0, Lnetwork/InterfaceImpl/SocketChannelResponder;->m_maxQueueSize:I

    if-lez v2, :cond_0

    iget v2, p0, Lnetwork/InterfaceImpl/SocketChannelResponder;->m_maxQueueSize:I

    int-to-long v5, v2

    cmp-long v2, v0, v5

    if-lez v2, :cond_0

    iget-object v2, p0, Lnetwork/InterfaceImpl/SocketChannelResponder;->m_bytesInQueue:Ljava/util/concurrent/atomic/AtomicLong;

    array-length v4, p1

    neg-int v4, v4

    int-to-long v4, v4

    invoke-virtual {v2, v4, v5}, Ljava/util/concurrent/atomic/AtomicLong;->addAndGet(J)J

    move v2, v3

    :goto_0
    return v2

    :cond_0
    iget-object v5, p0, Lnetwork/InterfaceImpl/SocketChannelResponder;->m_packetQueue:Ljava/util/concurrent/ConcurrentLinkedQueue;

    if-nez p2, :cond_1

    :goto_1
    invoke-virtual {v5, p1}, Ljava/util/concurrent/ConcurrentLinkedQueue;->offer(Ljava/lang/Object;)Z

    invoke-virtual {p0}, Lnetwork/InterfaceImpl/SocketChannelResponder;->getasyncService()Lnetwork/NetCore/asyncService;

    move-result-object v2

    new-instance v3, Lnetwork/InterfaceImpl/SocketChannelResponder$AddInterestEvent;

    const/4 v5, 0x4

    const/4 v6, 0x0

    invoke-direct {v3, p0, v5, v6}, Lnetwork/InterfaceImpl/SocketChannelResponder$AddInterestEvent;-><init>(Lnetwork/InterfaceImpl/SocketChannelResponder;ILnetwork/InterfaceImpl/SocketChannelResponder$AddInterestEvent;)V

    invoke-virtual {v2, v3}, Lnetwork/NetCore/asyncService;->queue(Ljava/lang/Runnable;)V

    move v2, v4

    goto :goto_0

    :cond_1
    const/4 v2, 0x2

    new-array v2, v2, [Ljava/lang/Object;

    aput-object p1, v2, v3

    aput-object p2, v2, v4

    move-object p1, v2

    goto :goto_1
.end method
