.class public abstract Lnetwork/InterfaceImpl/ChannelResponder;
.super Ljava/lang/Object;
.source "ChannelResponder.java"

# interfaces
.implements Lnetwork/Interface/NIOAbstractSocket;


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lnetwork/InterfaceImpl/ChannelResponder$CloseEvent;
    }
.end annotation


# instance fields
.field private final m_address:Ljava/net/InetSocketAddress;

.field private final m_channel:Ljava/nio/channels/SelectableChannel;

.field private volatile m_interestOps:I

.field private final m_ip:Ljava/lang/String;

.field private volatile m_key:Ljava/nio/channels/SelectionKey;

.field private m_observerSet:Z

.field private volatile m_open:Z

.field private final m_port:I

.field private final m_service:Lnetwork/NetCore/asyncService;

.field private m_tag:Ljava/lang/Object;


# direct methods
.method protected constructor <init>(Lnetwork/NetCore/asyncService;Ljava/nio/channels/SelectableChannel;Ljava/net/InetSocketAddress;)V
    .locals 3
    .param p1    # Lnetwork/NetCore/asyncService;
    .param p2    # Ljava/nio/channels/SelectableChannel;
    .param p3    # Ljava/net/InetSocketAddress;

    const/4 v2, 0x0

    const/4 v1, 0x0

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    iput-object p2, p0, Lnetwork/InterfaceImpl/ChannelResponder;->m_channel:Ljava/nio/channels/SelectableChannel;

    iput-object p1, p0, Lnetwork/InterfaceImpl/ChannelResponder;->m_service:Lnetwork/NetCore/asyncService;

    const/4 v0, 0x1

    iput-boolean v0, p0, Lnetwork/InterfaceImpl/ChannelResponder;->m_open:Z

    iput-object v2, p0, Lnetwork/InterfaceImpl/ChannelResponder;->m_key:Ljava/nio/channels/SelectionKey;

    iput v1, p0, Lnetwork/InterfaceImpl/ChannelResponder;->m_interestOps:I

    iput-boolean v1, p0, Lnetwork/InterfaceImpl/ChannelResponder;->m_observerSet:Z

    iput-object p3, p0, Lnetwork/InterfaceImpl/ChannelResponder;->m_address:Ljava/net/InetSocketAddress;

    invoke-virtual {p3}, Ljava/net/InetSocketAddress;->getAddress()Ljava/net/InetAddress;

    move-result-object v0

    invoke-virtual {v0}, Ljava/net/InetAddress;->getHostAddress()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lnetwork/InterfaceImpl/ChannelResponder;->m_ip:Ljava/lang/String;

    invoke-virtual {p3}, Ljava/net/InetSocketAddress;->getPort()I

    move-result v0

    iput v0, p0, Lnetwork/InterfaceImpl/ChannelResponder;->m_port:I

    iput-object v2, p0, Lnetwork/InterfaceImpl/ChannelResponder;->m_tag:Ljava/lang/Object;

    return-void
.end method

.method static synthetic access$0(Lnetwork/InterfaceImpl/ChannelResponder;Z)V
    .locals 0

    iput-boolean p1, p0, Lnetwork/InterfaceImpl/ChannelResponder;->m_open:Z

    return-void
.end method

.method private synchronizeKeyInterestOps()V
    .locals 3

    iget-object v1, p0, Lnetwork/InterfaceImpl/ChannelResponder;->m_key:Ljava/nio/channels/SelectionKey;

    if-eqz v1, :cond_0

    :try_start_0
    iget-object v1, p0, Lnetwork/InterfaceImpl/ChannelResponder;->m_key:Ljava/nio/channels/SelectionKey;

    invoke-virtual {v1}, Ljava/nio/channels/SelectionKey;->interestOps()I

    move-result v0

    iget v1, p0, Lnetwork/InterfaceImpl/ChannelResponder;->m_interestOps:I

    and-int/lit8 v1, v1, 0x8

    if-eqz v1, :cond_1

    iget-object v1, p0, Lnetwork/InterfaceImpl/ChannelResponder;->m_key:Ljava/nio/channels/SelectionKey;

    const/16 v2, 0x8

    invoke-virtual {v1, v2}, Ljava/nio/channels/SelectionKey;->interestOps(I)Ljava/nio/channels/SelectionKey;

    :goto_0
    iget-object v1, p0, Lnetwork/InterfaceImpl/ChannelResponder;->m_key:Ljava/nio/channels/SelectionKey;

    invoke-virtual {v1}, Ljava/nio/channels/SelectionKey;->interestOps()I

    move-result v1

    if-eq v1, v0, :cond_0

    iget-object v1, p0, Lnetwork/InterfaceImpl/ChannelResponder;->m_service:Lnetwork/NetCore/asyncService;

    invoke-virtual {v1}, Lnetwork/NetCore/asyncService;->wakeup()V

    :cond_0
    :goto_1
    return-void

    :cond_1
    iget-object v1, p0, Lnetwork/InterfaceImpl/ChannelResponder;->m_key:Ljava/nio/channels/SelectionKey;

    iget v2, p0, Lnetwork/InterfaceImpl/ChannelResponder;->m_interestOps:I

    invoke-virtual {v1, v2}, Ljava/nio/channels/SelectionKey;->interestOps(I)Ljava/nio/channels/SelectionKey;
    :try_end_0
    .catch Ljava/nio/channels/CancelledKeyException; {:try_start_0 .. :try_end_0} :catch_0

    goto :goto_0

    :catch_0
    move-exception v1

    goto :goto_1
.end method


# virtual methods
.method protected addInterest(I)V
    .locals 1
    .param p1    # I

    iget v0, p0, Lnetwork/InterfaceImpl/ChannelResponder;->m_interestOps:I

    or-int/2addr v0, p1

    iput v0, p0, Lnetwork/InterfaceImpl/ChannelResponder;->m_interestOps:I

    invoke-direct {p0}, Lnetwork/InterfaceImpl/ChannelResponder;->synchronizeKeyInterestOps()V

    return-void
.end method

.method public close()V
    .locals 1

    const/4 v0, 0x0

    invoke-virtual {p0, v0}, Lnetwork/InterfaceImpl/ChannelResponder;->close(Ljava/lang/Exception;)V

    return-void
.end method

.method public close(Ljava/lang/Exception;)V
    .locals 3
    .param p1    # Ljava/lang/Exception;

    invoke-virtual {p0}, Lnetwork/InterfaceImpl/ChannelResponder;->isOpen()Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-virtual {p0}, Lnetwork/InterfaceImpl/ChannelResponder;->getasyncService()Lnetwork/NetCore/asyncService;

    move-result-object v0

    new-instance v1, Lnetwork/InterfaceImpl/ChannelResponder$CloseEvent;

    const/4 v2, 0x0

    invoke-direct {v1, p0, p1, v2}, Lnetwork/InterfaceImpl/ChannelResponder$CloseEvent;-><init>(Lnetwork/InterfaceImpl/ChannelResponder;Ljava/lang/Exception;Lnetwork/InterfaceImpl/ChannelResponder$CloseEvent;)V

    invoke-virtual {v0, v1}, Lnetwork/NetCore/asyncService;->queue(Ljava/lang/Runnable;)V

    :cond_0
    return-void
.end method

.method protected deleteInterest(I)V
    .locals 2
    .param p1    # I

    iget v0, p0, Lnetwork/InterfaceImpl/ChannelResponder;->m_interestOps:I

    xor-int/lit8 v1, p1, -0x1

    and-int/2addr v0, v1

    iput v0, p0, Lnetwork/InterfaceImpl/ChannelResponder;->m_interestOps:I

    invoke-direct {p0}, Lnetwork/InterfaceImpl/ChannelResponder;->synchronizeKeyInterestOps()V

    return-void
.end method

.method public getAddress()Ljava/net/InetSocketAddress;
    .locals 1

    iget-object v0, p0, Lnetwork/InterfaceImpl/ChannelResponder;->m_address:Ljava/net/InetSocketAddress;

    return-object v0
.end method

.method public getChannel()Ljava/nio/channels/SelectableChannel;
    .locals 1

    iget-object v0, p0, Lnetwork/InterfaceImpl/ChannelResponder;->m_channel:Ljava/nio/channels/SelectableChannel;

    return-object v0
.end method

.method public getIp()Ljava/lang/String;
    .locals 1

    iget-object v0, p0, Lnetwork/InterfaceImpl/ChannelResponder;->m_ip:Ljava/lang/String;

    return-object v0
.end method

.method protected getKey()Ljava/nio/channels/SelectionKey;
    .locals 1

    iget-object v0, p0, Lnetwork/InterfaceImpl/ChannelResponder;->m_key:Ljava/nio/channels/SelectionKey;

    return-object v0
.end method

.method public getPort()I
    .locals 1

    iget v0, p0, Lnetwork/InterfaceImpl/ChannelResponder;->m_port:I

    return v0
.end method

.method public getTag()Ljava/lang/Object;
    .locals 1

    iget-object v0, p0, Lnetwork/InterfaceImpl/ChannelResponder;->m_tag:Ljava/lang/Object;

    return-object v0
.end method

.method protected getasyncService()Lnetwork/NetCore/asyncService;
    .locals 1

    iget-object v0, p0, Lnetwork/InterfaceImpl/ChannelResponder;->m_service:Lnetwork/NetCore/asyncService;

    return-object v0
.end method

.method public isOpen()Z
    .locals 1

    iget-boolean v0, p0, Lnetwork/InterfaceImpl/ChannelResponder;->m_open:Z

    return v0
.end method

.method abstract keyInitialized()V
.end method

.method protected markObserverSet()V
    .locals 2

    monitor-enter p0

    :try_start_0
    iget-boolean v0, p0, Lnetwork/InterfaceImpl/ChannelResponder;->m_observerSet:Z

    if-eqz v0, :cond_0

    new-instance v0, Ljava/lang/IllegalStateException;

    const-string v1, "Listener already set."

    invoke-direct {v0, v1}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    throw v0

    :catchall_0
    move-exception v0

    monitor-exit p0
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    throw v0

    :cond_0
    const/4 v0, 0x1

    :try_start_1
    iput-boolean v0, p0, Lnetwork/InterfaceImpl/ChannelResponder;->m_observerSet:Z

    monitor-exit p0
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    return-void
.end method

.method public setKey(Ljava/nio/channels/SelectionKey;)V
    .locals 2
    .param p1    # Ljava/nio/channels/SelectionKey;

    iget-object v0, p0, Lnetwork/InterfaceImpl/ChannelResponder;->m_key:Ljava/nio/channels/SelectionKey;

    if-eqz v0, :cond_0

    new-instance v0, Ljava/lang/IllegalStateException;

    const-string v1, "Tried to set selection key twice"

    invoke-direct {v0, v1}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    throw v0

    :cond_0
    iput-object p1, p0, Lnetwork/InterfaceImpl/ChannelResponder;->m_key:Ljava/nio/channels/SelectionKey;

    invoke-virtual {p0}, Lnetwork/InterfaceImpl/ChannelResponder;->isOpen()Z

    move-result v0

    if-nez v0, :cond_1

    iget-object v0, p0, Lnetwork/InterfaceImpl/ChannelResponder;->m_key:Ljava/nio/channels/SelectionKey;

    invoke-static {v0}, Lnetwork/util/NIOUtils;->cancelKeySilently(Ljava/nio/channels/SelectionKey;)V

    :goto_0
    return-void

    :cond_1
    invoke-virtual {p0}, Lnetwork/InterfaceImpl/ChannelResponder;->keyInitialized()V

    invoke-direct {p0}, Lnetwork/InterfaceImpl/ChannelResponder;->synchronizeKeyInterestOps()V

    goto :goto_0
.end method

.method public setTag(Ljava/lang/Object;)V
    .locals 0
    .param p1    # Ljava/lang/Object;

    iput-object p1, p0, Lnetwork/InterfaceImpl/ChannelResponder;->m_tag:Ljava/lang/Object;

    return-void
.end method

.method protected abstract shutdown(Ljava/lang/Exception;)V
.end method

.method public socketReadyForAccept()V
    .locals 3

    new-instance v0, Ljava/lang/UnsupportedOperationException;

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {p0}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, " does not support accept."

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/lang/UnsupportedOperationException;-><init>(Ljava/lang/String;)V

    throw v0
.end method

.method public socketReadyForConnect()V
    .locals 3

    new-instance v0, Ljava/lang/UnsupportedOperationException;

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {p0}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, " does not support connect."

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/lang/UnsupportedOperationException;-><init>(Ljava/lang/String;)V

    throw v0
.end method

.method public socketReadyForRead()V
    .locals 3

    new-instance v0, Ljava/lang/UnsupportedOperationException;

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {p0}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, " does not support read."

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/lang/UnsupportedOperationException;-><init>(Ljava/lang/String;)V

    throw v0
.end method

.method public socketReadyForWrite()V
    .locals 3

    new-instance v0, Ljava/lang/UnsupportedOperationException;

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {p0}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, " does not support write."

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/lang/UnsupportedOperationException;-><init>(Ljava/lang/String;)V

    throw v0
.end method

.method public toString()Ljava/lang/String;
    .locals 2

    new-instance v0, Ljava/lang/StringBuilder;

    iget-object v1, p0, Lnetwork/InterfaceImpl/ChannelResponder;->m_ip:Ljava/lang/String;

    invoke-static {v1}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    const-string v1, ":"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget v1, p0, Lnetwork/InterfaceImpl/ChannelResponder;->m_port:I

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method
