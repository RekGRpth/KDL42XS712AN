.class Lnetwork/InterfaceImpl/ServerSocketChannelResponder$BeginListenEvent;
.super Ljava/lang/Object;
.source "ServerSocketChannelResponder.java"

# interfaces
.implements Ljava/lang/Runnable;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lnetwork/InterfaceImpl/ServerSocketChannelResponder;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x2
    name = "BeginListenEvent"
.end annotation


# instance fields
.field private final m_newObserver:Lnetwork/Interface/ServerSocketObserver;

.field final synthetic this$0:Lnetwork/InterfaceImpl/ServerSocketChannelResponder;


# direct methods
.method private constructor <init>(Lnetwork/InterfaceImpl/ServerSocketChannelResponder;Lnetwork/Interface/ServerSocketObserver;)V
    .locals 0
    .param p2    # Lnetwork/Interface/ServerSocketObserver;

    iput-object p1, p0, Lnetwork/InterfaceImpl/ServerSocketChannelResponder$BeginListenEvent;->this$0:Lnetwork/InterfaceImpl/ServerSocketChannelResponder;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    iput-object p2, p0, Lnetwork/InterfaceImpl/ServerSocketChannelResponder$BeginListenEvent;->m_newObserver:Lnetwork/Interface/ServerSocketObserver;

    return-void
.end method

.method synthetic constructor <init>(Lnetwork/InterfaceImpl/ServerSocketChannelResponder;Lnetwork/Interface/ServerSocketObserver;Lnetwork/InterfaceImpl/ServerSocketChannelResponder$BeginListenEvent;)V
    .locals 0

    invoke-direct {p0, p1, p2}, Lnetwork/InterfaceImpl/ServerSocketChannelResponder$BeginListenEvent;-><init>(Lnetwork/InterfaceImpl/ServerSocketChannelResponder;Lnetwork/Interface/ServerSocketObserver;)V

    return-void
.end method


# virtual methods
.method public run()V
    .locals 2

    iget-object v0, p0, Lnetwork/InterfaceImpl/ServerSocketChannelResponder$BeginListenEvent;->this$0:Lnetwork/InterfaceImpl/ServerSocketChannelResponder;

    iget-object v1, p0, Lnetwork/InterfaceImpl/ServerSocketChannelResponder$BeginListenEvent;->m_newObserver:Lnetwork/Interface/ServerSocketObserver;

    invoke-static {v0, v1}, Lnetwork/InterfaceImpl/ServerSocketChannelResponder;->access$1(Lnetwork/InterfaceImpl/ServerSocketChannelResponder;Lnetwork/Interface/ServerSocketObserver;)V

    iget-object v0, p0, Lnetwork/InterfaceImpl/ServerSocketChannelResponder$BeginListenEvent;->this$0:Lnetwork/InterfaceImpl/ServerSocketChannelResponder;

    invoke-virtual {v0}, Lnetwork/InterfaceImpl/ServerSocketChannelResponder;->isOpen()Z

    move-result v0

    if-nez v0, :cond_0

    iget-object v0, p0, Lnetwork/InterfaceImpl/ServerSocketChannelResponder$BeginListenEvent;->this$0:Lnetwork/InterfaceImpl/ServerSocketChannelResponder;

    const/4 v1, 0x0

    # invokes: Lnetwork/InterfaceImpl/ServerSocketChannelResponder;->notifyObserverSocketDied(Ljava/lang/Exception;)V
    invoke-static {v0, v1}, Lnetwork/InterfaceImpl/ServerSocketChannelResponder;->access$2(Lnetwork/InterfaceImpl/ServerSocketChannelResponder;Ljava/lang/Exception;)V

    :goto_0
    return-void

    :cond_0
    iget-object v0, p0, Lnetwork/InterfaceImpl/ServerSocketChannelResponder$BeginListenEvent;->this$0:Lnetwork/InterfaceImpl/ServerSocketChannelResponder;

    const/16 v1, 0x10

    invoke-virtual {v0, v1}, Lnetwork/InterfaceImpl/ServerSocketChannelResponder;->addInterest(I)V

    goto :goto_0
.end method

.method public toString()Ljava/lang/String;
    .locals 2

    new-instance v0, Ljava/lang/StringBuilder;

    const-string v1, "BeginListen["

    invoke-direct {v0, v1}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    iget-object v1, p0, Lnetwork/InterfaceImpl/ServerSocketChannelResponder$BeginListenEvent;->m_newObserver:Lnetwork/Interface/ServerSocketObserver;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, "]"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method
