.class public Lnetwork/InterfaceImpl/SSLServerSocketChannelResponder;
.super Lnetwork/InterfaceImpl/ServerSocketChannelResponder;
.source "SSLServerSocketChannelResponder.java"

# interfaces
.implements Lnetwork/Interface/asyncServerSocketSSL;


# instance fields
.field private final m_sslContext:Ljavax/net/ssl/SSLContext;


# direct methods
.method public constructor <init>(Ljavax/net/ssl/SSLContext;Lnetwork/NetCore/asyncService;Ljava/nio/channels/ServerSocketChannel;Ljava/net/InetSocketAddress;)V
    .locals 0
    .param p1    # Ljavax/net/ssl/SSLContext;
    .param p2    # Lnetwork/NetCore/asyncService;
    .param p3    # Ljava/nio/channels/ServerSocketChannel;
    .param p4    # Ljava/net/InetSocketAddress;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    invoke-direct {p0, p2, p3, p4}, Lnetwork/InterfaceImpl/ServerSocketChannelResponder;-><init>(Lnetwork/NetCore/asyncService;Ljava/nio/channels/ServerSocketChannel;Ljava/net/InetSocketAddress;)V

    iput-object p1, p0, Lnetwork/InterfaceImpl/SSLServerSocketChannelResponder;->m_sslContext:Ljavax/net/ssl/SSLContext;

    return-void
.end method


# virtual methods
.method public getSSLContext()Ljavax/net/ssl/SSLContext;
    .locals 1

    iget-object v0, p0, Lnetwork/InterfaceImpl/SSLServerSocketChannelResponder;->m_sslContext:Ljavax/net/ssl/SSLContext;

    return-object v0
.end method

.method registerSocket(Ljava/nio/channels/SocketChannel;Ljava/net/InetSocketAddress;)Lnetwork/Interface/NIOSocket;
    .locals 5
    .param p1    # Ljava/nio/channels/SocketChannel;
    .param p2    # Ljava/net/InetSocketAddress;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    invoke-super {p0, p1, p2}, Lnetwork/InterfaceImpl/ServerSocketChannelResponder;->registerSocket(Ljava/nio/channels/SocketChannel;Ljava/net/InetSocketAddress;)Lnetwork/Interface/NIOSocket;

    move-result-object v0

    new-instance v1, Lnetwork/InterfaceImpl/SSLSocketChannelResponder;

    invoke-virtual {p0}, Lnetwork/InterfaceImpl/SSLServerSocketChannelResponder;->getasyncService()Lnetwork/NetCore/asyncService;

    move-result-object v2

    iget-object v3, p0, Lnetwork/InterfaceImpl/SSLServerSocketChannelResponder;->m_sslContext:Ljavax/net/ssl/SSLContext;

    invoke-virtual {v3}, Ljavax/net/ssl/SSLContext;->createSSLEngine()Ljavax/net/ssl/SSLEngine;

    move-result-object v3

    const/4 v4, 0x0

    invoke-direct {v1, v2, v0, v3, v4}, Lnetwork/InterfaceImpl/SSLSocketChannelResponder;-><init>(Lnetwork/NetCore/asyncService;Lnetwork/Interface/NIOSocket;Ljavax/net/ssl/SSLEngine;Z)V

    return-object v1
.end method
