.class public interface abstract Lnetwork/Interface/NIOSocket;
.super Ljava/lang/Object;
.source "NIOSocket.java"

# interfaces
.implements Lnetwork/Interface/NIOAbstractSocket;


# virtual methods
.method public abstract closeAfterWrite()V
.end method

.method public abstract getBytesRead()J
.end method

.method public abstract getBytesWritten()J
.end method

.method public abstract getMaxQueueSize()I
.end method

.method public abstract getTimeOpen()J
.end method

.method public abstract getWriteQueueSize()J
.end method

.method public abstract listen(Lnetwork/Interface/SocketObserver;)V
.end method

.method public abstract queue(Ljava/lang/Runnable;)V
.end method

.method public abstract setMaxQueueSize(I)V
.end method

.method public abstract setPacketReader(Lnetwork/Interface/PacketReader;)V
.end method

.method public abstract setPacketWriter(Lnetwork/Interface/PacketWriter;)V
.end method

.method public abstract socket()Ljava/net/Socket;
.end method

.method public abstract write([B)Z
.end method

.method public abstract write([BLjava/lang/Object;)Z
.end method
