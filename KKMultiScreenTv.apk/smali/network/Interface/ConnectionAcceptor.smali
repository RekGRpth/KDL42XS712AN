.class public interface abstract Lnetwork/Interface/ConnectionAcceptor;
.super Ljava/lang/Object;
.source "ConnectionAcceptor.java"


# static fields
.field public static final ALLOW:Lnetwork/Interface/ConnectionAcceptor;

.field public static final DENY:Lnetwork/Interface/ConnectionAcceptor;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    new-instance v0, Lnetwork/Interface/ConnectionAcceptor$1;

    invoke-direct {v0}, Lnetwork/Interface/ConnectionAcceptor$1;-><init>()V

    sput-object v0, Lnetwork/Interface/ConnectionAcceptor;->DENY:Lnetwork/Interface/ConnectionAcceptor;

    new-instance v0, Lnetwork/Interface/ConnectionAcceptor$2;

    invoke-direct {v0}, Lnetwork/Interface/ConnectionAcceptor$2;-><init>()V

    sput-object v0, Lnetwork/Interface/ConnectionAcceptor;->ALLOW:Lnetwork/Interface/ConnectionAcceptor;

    return-void
.end method


# virtual methods
.method public abstract acceptConnection(Ljava/net/InetSocketAddress;)Z
.end method
