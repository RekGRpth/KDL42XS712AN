.class public interface abstract Lnetwork/Interface/NIOAbstractSocket;
.super Ljava/lang/Object;
.source "NIOAbstractSocket.java"


# virtual methods
.method public abstract close()V
.end method

.method public abstract getAddress()Ljava/net/InetSocketAddress;
.end method

.method public abstract getIp()Ljava/lang/String;
.end method

.method public abstract getPort()I
.end method

.method public abstract getTag()Ljava/lang/Object;
.end method

.method public abstract isOpen()Z
.end method

.method public abstract setTag(Ljava/lang/Object;)V
.end method
