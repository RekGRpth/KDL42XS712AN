.class public Lnetwork/Interface/SocketObserverAdapter;
.super Ljava/lang/Object;
.source "SocketObserverAdapter.java"

# interfaces
.implements Lnetwork/Interface/SocketObserver;


# direct methods
.method public constructor <init>()V
    .locals 0

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public connectionBroken(Lnetwork/Interface/NIOSocket;Ljava/lang/Exception;)V
    .locals 0
    .param p1    # Lnetwork/Interface/NIOSocket;
    .param p2    # Ljava/lang/Exception;

    return-void
.end method

.method public connectionOpened(Lnetwork/Interface/NIOSocket;)V
    .locals 0
    .param p1    # Lnetwork/Interface/NIOSocket;

    return-void
.end method

.method public packetReceived(Lnetwork/Interface/NIOSocket;[B)V
    .locals 0
    .param p1    # Lnetwork/Interface/NIOSocket;
    .param p2    # [B

    return-void
.end method

.method public packetSent(Lnetwork/Interface/NIOSocket;Ljava/lang/Object;)V
    .locals 0
    .param p1    # Lnetwork/Interface/NIOSocket;
    .param p2    # Ljava/lang/Object;

    return-void
.end method
