.class public interface abstract Lnetwork/Interface/SocketObserver;
.super Ljava/lang/Object;
.source "SocketObserver.java"


# static fields
.field public static final NULL:Lnetwork/Interface/SocketObserver;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    new-instance v0, Lnetwork/Interface/SocketObserverAdapter;

    invoke-direct {v0}, Lnetwork/Interface/SocketObserverAdapter;-><init>()V

    sput-object v0, Lnetwork/Interface/SocketObserver;->NULL:Lnetwork/Interface/SocketObserver;

    return-void
.end method


# virtual methods
.method public abstract connectionBroken(Lnetwork/Interface/NIOSocket;Ljava/lang/Exception;)V
.end method

.method public abstract connectionOpened(Lnetwork/Interface/NIOSocket;)V
.end method

.method public abstract packetReceived(Lnetwork/Interface/NIOSocket;[B)V
.end method

.method public abstract packetSent(Lnetwork/Interface/NIOSocket;Ljava/lang/Object;)V
.end method
