.class public interface abstract Lnetwork/Interface/asyncServerSocket;
.super Ljava/lang/Object;
.source "asyncServerSocket.java"

# interfaces
.implements Lnetwork/Interface/NIOAbstractSocket;


# virtual methods
.method public abstract getTotalAcceptedConnections()J
.end method

.method public abstract getTotalConnections()J
.end method

.method public abstract getTotalFailedConnections()J
.end method

.method public abstract getTotalRefusedConnections()J
.end method

.method public abstract listen(Lnetwork/Interface/ServerSocketObserver;)V
.end method

.method public abstract setConnectionAcceptor(Lnetwork/Interface/ConnectionAcceptor;)V
.end method

.method public abstract socket()Ljava/net/ServerSocket;
.end method
