.class public interface abstract Lnetwork/Interface/NIOSocketSSL;
.super Ljava/lang/Object;
.source "NIOSocketSSL.java"

# interfaces
.implements Lnetwork/Interface/NIOSocket;


# virtual methods
.method public abstract beginHandshake()V
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljavax/net/ssl/SSLException;
        }
    .end annotation
.end method

.method public abstract getSSLEngine()Ljavax/net/ssl/SSLEngine;
.end method

.method public abstract isEncrypted()Z
.end method
