.class Lnetwork/event/DelayedAction;
.super Ljava/lang/Object;
.source "DelayedAction.java"

# interfaces
.implements Ljava/lang/Comparable;
.implements Lnetwork/event/DelayedEvent;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Object;",
        "Ljava/lang/Comparable",
        "<",
        "Lnetwork/event/DelayedAction;",
        ">;",
        "Lnetwork/event/DelayedEvent;"
    }
.end annotation


# static fields
.field private static final s_nextId:Ljava/util/concurrent/atomic/AtomicLong;


# instance fields
.field private volatile m_call:Ljava/lang/Runnable;

.field private final m_id:J

.field private final m_time:J


# direct methods
.method static constructor <clinit>()V
    .locals 3

    new-instance v0, Ljava/util/concurrent/atomic/AtomicLong;

    const-wide/16 v1, 0x0

    invoke-direct {v0, v1, v2}, Ljava/util/concurrent/atomic/AtomicLong;-><init>(J)V

    sput-object v0, Lnetwork/event/DelayedAction;->s_nextId:Ljava/util/concurrent/atomic/AtomicLong;

    return-void
.end method

.method public constructor <init>(Ljava/lang/Runnable;J)V
    .locals 2
    .param p1    # Ljava/lang/Runnable;
    .param p2    # J

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    iput-object p1, p0, Lnetwork/event/DelayedAction;->m_call:Ljava/lang/Runnable;

    iput-wide p2, p0, Lnetwork/event/DelayedAction;->m_time:J

    sget-object v0, Lnetwork/event/DelayedAction;->s_nextId:Ljava/util/concurrent/atomic/AtomicLong;

    invoke-virtual {v0}, Ljava/util/concurrent/atomic/AtomicLong;->getAndIncrement()J

    move-result-wide v0

    iput-wide v0, p0, Lnetwork/event/DelayedAction;->m_id:J

    return-void
.end method


# virtual methods
.method public cancel()V
    .locals 1

    const/4 v0, 0x0

    iput-object v0, p0, Lnetwork/event/DelayedAction;->m_call:Ljava/lang/Runnable;

    return-void
.end method

.method public bridge synthetic compareTo(Ljava/lang/Object;)I
    .locals 1

    check-cast p1, Lnetwork/event/DelayedAction;

    invoke-virtual {p0, p1}, Lnetwork/event/DelayedAction;->compareTo(Lnetwork/event/DelayedAction;)I

    move-result v0

    return v0
.end method

.method public compareTo(Lnetwork/event/DelayedAction;)I
    .locals 6
    .param p1    # Lnetwork/event/DelayedAction;

    const/4 v0, 0x1

    const/4 v1, -0x1

    iget-wide v2, p0, Lnetwork/event/DelayedAction;->m_time:J

    iget-wide v4, p1, Lnetwork/event/DelayedAction;->m_time:J

    cmp-long v2, v2, v4

    if-gez v2, :cond_1

    move v0, v1

    :cond_0
    :goto_0
    return v0

    :cond_1
    iget-wide v2, p0, Lnetwork/event/DelayedAction;->m_time:J

    iget-wide v4, p1, Lnetwork/event/DelayedAction;->m_time:J

    cmp-long v2, v2, v4

    if-gtz v2, :cond_0

    iget-wide v2, p0, Lnetwork/event/DelayedAction;->m_id:J

    iget-wide v4, p1, Lnetwork/event/DelayedAction;->m_id:J

    cmp-long v2, v2, v4

    if-gez v2, :cond_2

    move v0, v1

    goto :goto_0

    :cond_2
    iget-wide v1, p0, Lnetwork/event/DelayedAction;->m_id:J

    iget-wide v3, p1, Lnetwork/event/DelayedAction;->m_id:J

    cmp-long v1, v1, v3

    if-gtz v1, :cond_0

    const/4 v0, 0x0

    goto :goto_0
.end method

.method public getCall()Ljava/lang/Runnable;
    .locals 1

    iget-object v0, p0, Lnetwork/event/DelayedAction;->m_call:Ljava/lang/Runnable;

    return-object v0
.end method

.method public getTime()J
    .locals 2

    iget-wide v0, p0, Lnetwork/event/DelayedAction;->m_time:J

    return-wide v0
.end method

.method run()V
    .locals 1

    iget-object v0, p0, Lnetwork/event/DelayedAction;->m_call:Ljava/lang/Runnable;

    if-eqz v0, :cond_0

    invoke-interface {v0}, Ljava/lang/Runnable;->run()V

    :cond_0
    return-void
.end method

.method public toString()Ljava/lang/String;
    .locals 4

    new-instance v0, Ljava/lang/StringBuilder;

    const-string v1, "DelayedAction @ "

    invoke-direct {v0, v1}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    new-instance v1, Ljava/util/Date;

    iget-wide v2, p0, Lnetwork/event/DelayedAction;->m_time:J

    invoke-direct {v1, v2, v3}, Ljava/util/Date;-><init>(J)V

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, " ["

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget-object v0, p0, Lnetwork/event/DelayedAction;->m_call:Ljava/lang/Runnable;

    if-nez v0, :cond_0

    const-string v0, "Cancelled"

    :goto_0
    invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, "]"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    return-object v0

    :cond_0
    iget-object v0, p0, Lnetwork/event/DelayedAction;->m_call:Ljava/lang/Runnable;

    goto :goto_0
.end method
