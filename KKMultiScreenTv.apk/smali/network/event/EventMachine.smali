.class public Lnetwork/event/EventMachine;
.super Ljava/lang/Object;
.source "EventMachine.java"


# instance fields
.field private final m_queue:Ljava/util/Queue;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Queue",
            "<",
            "Lnetwork/event/DelayedAction;",
            ">;"
        }
    .end annotation
.end field

.field private m_runThread:Ljava/lang/Thread;

.field private final m_service:Lnetwork/NetCore/asyncService;


# direct methods
.method public constructor <init>()V
    .locals 1
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    new-instance v0, Lnetwork/NetCore/asyncService;

    invoke-direct {v0}, Lnetwork/NetCore/asyncService;-><init>()V

    iput-object v0, p0, Lnetwork/event/EventMachine;->m_service:Lnetwork/NetCore/asyncService;

    new-instance v0, Ljava/util/concurrent/PriorityBlockingQueue;

    invoke-direct {v0}, Ljava/util/concurrent/PriorityBlockingQueue;-><init>()V

    iput-object v0, p0, Lnetwork/event/EventMachine;->m_queue:Ljava/util/Queue;

    const/4 v0, 0x0

    iput-object v0, p0, Lnetwork/event/EventMachine;->m_runThread:Ljava/lang/Thread;

    return-void
.end method

.method static synthetic access$0(Lnetwork/event/EventMachine;)Ljava/lang/Thread;
    .locals 1

    iget-object v0, p0, Lnetwork/event/EventMachine;->m_runThread:Ljava/lang/Thread;

    return-object v0
.end method

.method static synthetic access$1(Lnetwork/event/EventMachine;)V
    .locals 0
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/lang/Throwable;
        }
    .end annotation

    invoke-direct {p0}, Lnetwork/event/EventMachine;->select()V

    return-void
.end method

.method private queueAction(Ljava/lang/Runnable;J)Lnetwork/event/DelayedAction;
    .locals 2
    .param p1    # Ljava/lang/Runnable;
    .param p2    # J

    new-instance v0, Lnetwork/event/DelayedAction;

    invoke-direct {v0, p1, p2, p3}, Lnetwork/event/DelayedAction;-><init>(Ljava/lang/Runnable;J)V

    iget-object v1, p0, Lnetwork/event/EventMachine;->m_queue:Ljava/util/Queue;

    invoke-interface {v1, v0}, Ljava/util/Queue;->add(Ljava/lang/Object;)Z

    iget-object v1, p0, Lnetwork/event/EventMachine;->m_service:Lnetwork/NetCore/asyncService;

    invoke-virtual {v1}, Lnetwork/NetCore/asyncService;->wakeup()V

    return-object v0
.end method

.method private runNextAction()V
    .locals 1

    iget-object v0, p0, Lnetwork/event/EventMachine;->m_queue:Ljava/util/Queue;

    invoke-interface {v0}, Ljava/util/Queue;->poll()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lnetwork/event/DelayedAction;

    invoke-virtual {v0}, Lnetwork/event/DelayedAction;->run()V

    return-void
.end method

.method private select()V
    .locals 7
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/lang/Throwable;
        }
    .end annotation

    :goto_0
    invoke-virtual {p0}, Lnetwork/event/EventMachine;->timeOfNextEvent()J

    move-result-wide v3

    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v5

    cmp-long v3, v3, v5

    if-lez v3, :cond_0

    invoke-virtual {p0}, Lnetwork/event/EventMachine;->timeOfNextEvent()J

    move-result-wide v3

    const-wide v5, 0x7fffffffffffffffL

    cmp-long v3, v3, v5

    if-nez v3, :cond_1

    iget-object v3, p0, Lnetwork/event/EventMachine;->m_service:Lnetwork/NetCore/asyncService;

    invoke-virtual {v3}, Lnetwork/NetCore/asyncService;->selectBlocking()V

    :goto_1
    return-void

    :cond_0
    :try_start_0
    invoke-direct {p0}, Lnetwork/event/EventMachine;->runNextAction()V
    :try_end_0
    .catch Ljava/lang/Throwable; {:try_start_0 .. :try_end_0} :catch_0

    goto :goto_0

    :catch_0
    move-exception v2

    invoke-virtual {p0}, Lnetwork/event/EventMachine;->getasyncService()Lnetwork/NetCore/asyncService;

    move-result-object v3

    invoke-virtual {v3, v2}, Lnetwork/NetCore/asyncService;->notifyException(Ljava/lang/Throwable;)V

    goto :goto_0

    :cond_1
    invoke-virtual {p0}, Lnetwork/event/EventMachine;->timeOfNextEvent()J

    move-result-wide v3

    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v5

    sub-long v0, v3, v5

    iget-object v3, p0, Lnetwork/event/EventMachine;->m_service:Lnetwork/NetCore/asyncService;

    const-wide/16 v4, 0x1

    invoke-static {v4, v5, v0, v1}, Ljava/lang/Math;->max(JJ)J

    move-result-wide v4

    invoke-virtual {v3, v4, v5}, Lnetwork/NetCore/asyncService;->selectBlocking(J)V

    goto :goto_1
.end method


# virtual methods
.method public asyncExecute(Ljava/lang/Runnable;)V
    .locals 2
    .param p1    # Ljava/lang/Runnable;

    const-wide/16 v0, 0x0

    invoke-virtual {p0, p1, v0, v1}, Lnetwork/event/EventMachine;->executeLater(Ljava/lang/Runnable;J)Lnetwork/event/DelayedEvent;

    return-void
.end method

.method public executeAt(Ljava/lang/Runnable;Ljava/util/Date;)Lnetwork/event/DelayedEvent;
    .locals 2
    .param p1    # Ljava/lang/Runnable;
    .param p2    # Ljava/util/Date;

    invoke-virtual {p2}, Ljava/util/Date;->getTime()J

    move-result-wide v0

    invoke-direct {p0, p1, v0, v1}, Lnetwork/event/EventMachine;->queueAction(Ljava/lang/Runnable;J)Lnetwork/event/DelayedAction;

    move-result-object v0

    return-object v0
.end method

.method public executeLater(Ljava/lang/Runnable;J)Lnetwork/event/DelayedEvent;
    .locals 2
    .param p1    # Ljava/lang/Runnable;
    .param p2    # J

    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v0

    add-long/2addr v0, p2

    invoke-direct {p0, p1, v0, v1}, Lnetwork/event/EventMachine;->queueAction(Ljava/lang/Runnable;J)Lnetwork/event/DelayedAction;

    move-result-object v0

    return-object v0
.end method

.method public getQueue()Ljava/util/Queue;
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/Queue",
            "<",
            "Lnetwork/event/DelayedEvent;",
            ">;"
        }
    .end annotation

    new-instance v0, Ljava/util/PriorityQueue;

    iget-object v1, p0, Lnetwork/event/EventMachine;->m_queue:Ljava/util/Queue;

    invoke-direct {v0, v1}, Ljava/util/PriorityQueue;-><init>(Ljava/util/Collection;)V

    return-object v0
.end method

.method public getQueueSize()I
    .locals 1

    iget-object v0, p0, Lnetwork/event/EventMachine;->m_queue:Ljava/util/Queue;

    invoke-interface {v0}, Ljava/util/Queue;->size()I

    move-result v0

    return v0
.end method

.method public getasyncService()Lnetwork/NetCore/asyncService;
    .locals 1

    iget-object v0, p0, Lnetwork/event/EventMachine;->m_service:Lnetwork/NetCore/asyncService;

    return-object v0
.end method

.method public setObserver(Lnetwork/Interface/ExceptionObserver;)V
    .locals 1
    .param p1    # Lnetwork/Interface/ExceptionObserver;

    invoke-virtual {p0}, Lnetwork/event/EventMachine;->getasyncService()Lnetwork/NetCore/asyncService;

    move-result-object v0

    invoke-virtual {v0, p1}, Lnetwork/NetCore/asyncService;->setExceptionObserver(Lnetwork/Interface/ExceptionObserver;)V

    return-void
.end method

.method public declared-synchronized shutdown()V
    .locals 2

    monitor-enter p0

    :try_start_0
    iget-object v0, p0, Lnetwork/event/EventMachine;->m_runThread:Ljava/lang/Thread;

    if-nez v0, :cond_0

    new-instance v0, Ljava/lang/IllegalStateException;

    const-string v1, "The service is not running."

    invoke-direct {v0, v1}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    throw v0
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0

    :cond_0
    :try_start_1
    iget-object v0, p0, Lnetwork/event/EventMachine;->m_service:Lnetwork/NetCore/asyncService;

    invoke-virtual {v0}, Lnetwork/NetCore/asyncService;->close()V

    invoke-virtual {p0}, Lnetwork/event/EventMachine;->stop()V
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    monitor-exit p0

    return-void
.end method

.method public declared-synchronized start()V
    .locals 2

    monitor-enter p0

    :try_start_0
    iget-object v0, p0, Lnetwork/event/EventMachine;->m_runThread:Ljava/lang/Thread;

    if-eqz v0, :cond_0

    new-instance v0, Ljava/lang/IllegalStateException;

    const-string v1, "Service already running."

    invoke-direct {v0, v1}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    throw v0
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0

    :cond_0
    :try_start_1
    iget-object v0, p0, Lnetwork/event/EventMachine;->m_service:Lnetwork/NetCore/asyncService;

    invoke-virtual {v0}, Lnetwork/NetCore/asyncService;->isOpen()Z

    move-result v0

    if-nez v0, :cond_1

    new-instance v0, Ljava/lang/IllegalStateException;

    const-string v1, "Service has been shut down."

    invoke-direct {v0, v1}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    throw v0

    :cond_1
    new-instance v0, Lnetwork/event/EventMachine$1;

    invoke-direct {v0, p0}, Lnetwork/event/EventMachine$1;-><init>(Lnetwork/event/EventMachine;)V

    iput-object v0, p0, Lnetwork/event/EventMachine;->m_runThread:Ljava/lang/Thread;

    iget-object v0, p0, Lnetwork/event/EventMachine;->m_runThread:Ljava/lang/Thread;

    invoke-virtual {v0}, Ljava/lang/Thread;->start()V
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    monitor-exit p0

    return-void
.end method

.method public declared-synchronized stop()V
    .locals 2

    monitor-enter p0

    :try_start_0
    iget-object v0, p0, Lnetwork/event/EventMachine;->m_runThread:Ljava/lang/Thread;

    if-nez v0, :cond_0

    new-instance v0, Ljava/lang/IllegalStateException;

    const-string v1, "Service is not running."

    invoke-direct {v0, v1}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    throw v0
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0

    :cond_0
    const/4 v0, 0x0

    :try_start_1
    iput-object v0, p0, Lnetwork/event/EventMachine;->m_runThread:Ljava/lang/Thread;

    iget-object v0, p0, Lnetwork/event/EventMachine;->m_service:Lnetwork/NetCore/asyncService;

    invoke-virtual {v0}, Lnetwork/NetCore/asyncService;->wakeup()V
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    monitor-exit p0

    return-void
.end method

.method public timeOfNextEvent()J
    .locals 3

    iget-object v1, p0, Lnetwork/event/EventMachine;->m_queue:Ljava/util/Queue;

    invoke-interface {v1}, Ljava/util/Queue;->peek()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lnetwork/event/DelayedAction;

    if-nez v0, :cond_0

    const-wide v1, 0x7fffffffffffffffL

    :goto_0
    return-wide v1

    :cond_0
    invoke-virtual {v0}, Lnetwork/event/DelayedAction;->getTime()J

    move-result-wide v1

    goto :goto_0
.end method
