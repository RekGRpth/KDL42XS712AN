.class public Lnetwork/udp/UdpSearchThread$Listener;
.super Ljava/lang/Object;
.source "UdpSearchThread.java"

# interfaces
.implements Ljava/lang/Runnable;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lnetwork/udp/UdpSearchThread;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x4
    name = "Listener"
.end annotation


# instance fields
.field final synthetic this$0:Lnetwork/udp/UdpSearchThread;


# direct methods
.method protected constructor <init>(Lnetwork/udp/UdpSearchThread;)V
    .locals 0

    iput-object p1, p0, Lnetwork/udp/UdpSearchThread$Listener;->this$0:Lnetwork/udp/UdpSearchThread;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public run()V
    .locals 12

    const/4 v11, 0x1

    :cond_0
    :goto_0
    iget-object v7, p0, Lnetwork/udp/UdpSearchThread$Listener;->this$0:Lnetwork/udp/UdpSearchThread;

    # getter for: Lnetwork/udp/UdpSearchThread;->socket:Ljava/net/DatagramSocket;
    invoke-static {v7}, Lnetwork/udp/UdpSearchThread;->access$0(Lnetwork/udp/UdpSearchThread;)Ljava/net/DatagramSocket;

    move-result-object v7

    if-nez v7, :cond_1

    return-void

    :cond_1
    const/16 v7, 0x80

    :try_start_0
    new-array v0, v7, [B

    new-instance v4, Ljava/net/DatagramPacket;

    array-length v7, v0

    invoke-direct {v4, v0, v7}, Ljava/net/DatagramPacket;-><init>([BI)V

    const-wide/16 v7, 0x2710

    invoke-static {v7, v8}, Ljava/lang/Thread;->sleep(J)V

    iget-object v7, p0, Lnetwork/udp/UdpSearchThread$Listener;->this$0:Lnetwork/udp/UdpSearchThread;

    # getter for: Lnetwork/udp/UdpSearchThread;->socket:Ljava/net/DatagramSocket;
    invoke-static {v7}, Lnetwork/udp/UdpSearchThread;->access$0(Lnetwork/udp/UdpSearchThread;)Ljava/net/DatagramSocket;

    move-result-object v7

    invoke-virtual {v7, v4}, Ljava/net/DatagramSocket;->receive(Ljava/net/DatagramPacket;)V

    array-length v7, v0

    if-ge v7, v11, :cond_2

    const-string v7, "UdpSearchThread"

    new-instance v8, Ljava/lang/StringBuilder;

    const-string v9, "recv packet buffer.length: "

    invoke-direct {v8, v9}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    array-length v9, v0

    invoke-virtual {v8, v9}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v8

    invoke-virtual {v8}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v8

    invoke-static {v7, v8}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    :cond_2
    const-string v7, "UdpSearchThread"

    const-string v8, "------------------------------"

    invoke-static {v7, v8}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    array-length v7, v0

    if-lez v7, :cond_0

    invoke-virtual {v4}, Ljava/net/DatagramPacket;->getAddress()Ljava/net/InetAddress;

    move-result-object v7

    invoke-virtual {v7}, Ljava/net/InetAddress;->toString()Ljava/lang/String;

    move-result-object v7

    const-string v8, "/"

    invoke-virtual {v7, v8}, Ljava/lang/String;->split(Ljava/lang/String;)[Ljava/lang/String;

    move-result-object v7

    const/4 v8, 0x1

    aget-object v1, v7, v8

    invoke-virtual {v4}, Ljava/net/DatagramPacket;->getPort()I

    move-result v2

    iget-object v7, p0, Lnetwork/udp/UdpSearchThread$Listener;->this$0:Lnetwork/udp/UdpSearchThread;

    invoke-virtual {v7}, Lnetwork/udp/UdpSearchThread;->getLocalIpAddress()Ljava/lang/String;

    move-result-object v7

    invoke-virtual {v7, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v7

    if-nez v7, :cond_0

    array-length v7, v0

    const/16 v8, 0xc

    if-le v7, v8, :cond_3

    invoke-static {v0}, Lnetwork/udp/BroadThread;->ByteToString([B)Ljava/lang/String;

    move-result-object v6

    if-eqz v6, :cond_3

    sget-object v7, Lnetwork/udp/BroadThread;->connMap:Ljava/util/Map;

    invoke-static {v1}, Lutil/comm/ToolClass;->getStringIpToLong(Ljava/lang/String;)J

    move-result-wide v8

    invoke-static {v8, v9}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v8

    invoke-interface {v7, v8}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v7

    if-nez v7, :cond_3

    const-string v7, "UdpSearchThread"

    new-instance v8, Ljava/lang/StringBuilder;

    const-string v9, "ip: "

    invoke-direct {v8, v9}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v8, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v8

    const-string v9, " port "

    invoke-virtual {v8, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v8

    invoke-virtual {v8, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v8

    const-string v9, " receive broad buffer: "

    invoke-virtual {v8, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v8

    invoke-virtual {v8, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v8

    invoke-virtual {v8}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v8

    invoke-static {v7, v8}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    sget-object v7, Lnetwork/udp/BroadThread;->connMap:Ljava/util/Map;

    invoke-static {v1}, Lutil/comm/ToolClass;->getStringIpToLong(Ljava/lang/String;)J

    move-result-wide v8

    invoke-static {v8, v9}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v8

    new-instance v9, Ljava/lang/StringBuilder;

    invoke-static {v2}, Ljava/lang/String;->valueOf(I)Ljava/lang/String;

    move-result-object v10

    invoke-direct {v9, v10}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    const-string v10, "&"

    invoke-virtual {v9, v10}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v9

    invoke-virtual {v9, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v9

    invoke-virtual {v9}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v9

    invoke-interface {v7, v8, v9}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    :cond_3
    iget-object v7, p0, Lnetwork/udp/UdpSearchThread$Listener;->this$0:Lnetwork/udp/UdpSearchThread;

    # getter for: Lnetwork/udp/UdpSearchThread;->devName:Ljava/lang/String;
    invoke-static {v7}, Lnetwork/udp/UdpSearchThread;->access$1(Lnetwork/udp/UdpSearchThread;)Ljava/lang/String;

    move-result-object v7

    if-nez v7, :cond_4

    iget-object v7, p0, Lnetwork/udp/UdpSearchThread$Listener;->this$0:Lnetwork/udp/UdpSearchThread;

    const-string v8, "tv device"

    invoke-static {v7, v8}, Lnetwork/udp/UdpSearchThread;->access$2(Lnetwork/udp/UdpSearchThread;Ljava/lang/String;)V

    :cond_4
    new-instance v5, Ljava/net/DatagramPacket;

    iget-object v7, p0, Lnetwork/udp/UdpSearchThread$Listener;->this$0:Lnetwork/udp/UdpSearchThread;

    # getter for: Lnetwork/udp/UdpSearchThread;->buff:[B
    invoke-static {v7}, Lnetwork/udp/UdpSearchThread;->access$3(Lnetwork/udp/UdpSearchThread;)[B

    move-result-object v7

    iget-object v8, p0, Lnetwork/udp/UdpSearchThread$Listener;->this$0:Lnetwork/udp/UdpSearchThread;

    # getter for: Lnetwork/udp/UdpSearchThread;->buff:[B
    invoke-static {v8}, Lnetwork/udp/UdpSearchThread;->access$3(Lnetwork/udp/UdpSearchThread;)[B

    move-result-object v8

    array-length v8, v8

    invoke-static {v1}, Ljava/net/InetAddress;->getByName(Ljava/lang/String;)Ljava/net/InetAddress;

    move-result-object v9

    invoke-direct {v5, v7, v8, v9, v2}, Ljava/net/DatagramPacket;-><init>([BILjava/net/InetAddress;I)V

    iget-object v7, p0, Lnetwork/udp/UdpSearchThread$Listener;->this$0:Lnetwork/udp/UdpSearchThread;

    # getter for: Lnetwork/udp/UdpSearchThread;->socket:Ljava/net/DatagramSocket;
    invoke-static {v7}, Lnetwork/udp/UdpSearchThread;->access$0(Lnetwork/udp/UdpSearchThread;)Ljava/net/DatagramSocket;

    move-result-object v7

    invoke-virtual {v7, v5}, Ljava/net/DatagramSocket;->send(Ljava/net/DatagramPacket;)V
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    goto/16 :goto_0

    :catch_0
    move-exception v3

    sget-object v7, Ljava/lang/System;->err:Ljava/io/PrintStream;

    new-instance v8, Ljava/lang/StringBuilder;

    const-string v9, "Exception "

    invoke-direct {v8, v9}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v3}, Ljava/lang/Exception;->getMessage()Ljava/lang/String;

    move-result-object v9

    invoke-virtual {v8, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v8

    invoke-virtual {v8}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v8

    invoke-virtual {v7, v8}, Ljava/io/PrintStream;->println(Ljava/lang/String;)V

    goto/16 :goto_0
.end method
