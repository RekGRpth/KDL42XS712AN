.class public Lnetwork/udp/UdpBroadcast;
.super Ljava/lang/Object;
.source "UdpBroadcast.java"


# static fields
.field private static final TAG:Ljava/lang/String; = "UdpBroadcast"

.field public static devName:Ljava/lang/String;

.field public static devNameType:I

.field private static sendThread:Lnetwork/udp/BroadThread;


# instance fields
.field private ipList:Ljava/lang/String;

.field private localIp:Ljava/lang/String;

.field private mSerivceType:I

.field private s_broadpacket:LprotocolAnalysis/protocol/GetServerPacket;

.field private s_buffer:[B


# direct methods
.method static constructor <clinit>()V
    .locals 1

    const/4 v0, 0x0

    sput-object v0, Lnetwork/udp/UdpBroadcast;->sendThread:Lnetwork/udp/BroadThread;

    sput-object v0, Lnetwork/udp/UdpBroadcast;->devName:Ljava/lang/String;

    const/4 v0, -0x1

    sput v0, Lnetwork/udp/UdpBroadcast;->devNameType:I

    return-void
.end method

.method public constructor <init>()V
    .locals 3

    const/4 v2, 0x0

    const/4 v1, 0x0

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    const-string v0, ""

    iput-object v0, p0, Lnetwork/udp/UdpBroadcast;->ipList:Ljava/lang/String;

    iput-object v1, p0, Lnetwork/udp/UdpBroadcast;->localIp:Ljava/lang/String;

    iput v2, p0, Lnetwork/udp/UdpBroadcast;->mSerivceType:I

    iput-object v1, p0, Lnetwork/udp/UdpBroadcast;->s_buffer:[B

    iput-object v1, p0, Lnetwork/udp/UdpBroadcast;->s_broadpacket:LprotocolAnalysis/protocol/GetServerPacket;

    invoke-direct {p0, v2}, Lnetwork/udp/UdpBroadcast;->IpThreadStart(I)V

    return-void
.end method

.method private IpThreadStart(I)V
    .locals 7
    .param p1    # I

    const-string v3, "UdpBroadcast"

    new-instance v4, Ljava/lang/StringBuilder;

    const-string v5, "IpThreadStart,SerivceType: "

    invoke-direct {v4, v5}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v4, p1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-static {v3, v4}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    iput p1, p0, Lnetwork/udp/UdpBroadcast;->mSerivceType:I

    new-instance v1, LprotocolAnalysis/analysis/packetHandle;

    invoke-direct {v1}, LprotocolAnalysis/analysis/packetHandle;-><init>()V

    invoke-virtual {v1}, LprotocolAnalysis/analysis/packetHandle;->setDeviceNamePacket()[B

    move-result-object v2

    new-instance v3, Ljava/lang/String;

    invoke-direct {v3, v2}, Ljava/lang/String;-><init>([B)V

    sput-object v3, Lnetwork/udp/UdpBroadcast;->devName:Ljava/lang/String;

    sget-object v3, Lnetwork/udp/UdpBroadcast;->devName:Ljava/lang/String;

    const-string v4, "6a8001"

    invoke-virtual {v3, v4}, Ljava/lang/String;->indexOf(Ljava/lang/String;)I

    move-result v3

    if-ltz v3, :cond_1

    const/4 v3, 0x1

    sput v3, Lnetwork/udp/UdpBroadcast;->devNameType:I

    :cond_0
    :goto_0
    new-instance v3, LprotocolAnalysis/protocol/GetServerPacket;

    sget-object v4, Lnetwork/udp/UdpBroadcast;->devName:Ljava/lang/String;

    invoke-virtual {v4}, Ljava/lang/String;->length()I

    move-result v4

    invoke-direct {v3, p1, v4}, LprotocolAnalysis/protocol/GetServerPacket;-><init>(II)V

    iput-object v3, p0, Lnetwork/udp/UdpBroadcast;->s_broadpacket:LprotocolAnalysis/protocol/GetServerPacket;

    iget-object v3, p0, Lnetwork/udp/UdpBroadcast;->s_broadpacket:LprotocolAnalysis/protocol/GetServerPacket;

    const/4 v4, 0x5

    const/16 v5, 0x2ff

    sget-object v6, Lnetwork/udp/UdpBroadcast;->devName:Ljava/lang/String;

    invoke-virtual {v3, v4, v5, v6}, LprotocolAnalysis/protocol/GetServerPacket;->SetDeviceInfo(SSLjava/lang/String;)V

    iget-object v3, p0, Lnetwork/udp/UdpBroadcast;->s_broadpacket:LprotocolAnalysis/protocol/GetServerPacket;

    invoke-virtual {v3}, LprotocolAnalysis/protocol/GetServerPacket;->sizeOf()I

    move-result v3

    new-array v3, v3, [B

    iput-object v3, p0, Lnetwork/udp/UdpBroadcast;->s_buffer:[B

    iget-object v3, p0, Lnetwork/udp/UdpBroadcast;->s_broadpacket:LprotocolAnalysis/protocol/GetServerPacket;

    iget-object v4, p0, Lnetwork/udp/UdpBroadcast;->s_buffer:[B

    invoke-virtual {v3, v4}, LprotocolAnalysis/protocol/GetServerPacket;->format([B)V

    const-string v3, "UdpBroadcast"

    new-instance v4, Ljava/lang/StringBuilder;

    const-string v5, "s_broadpacket: "

    invoke-direct {v4, v5}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    iget-object v5, p0, Lnetwork/udp/UdpBroadcast;->s_broadpacket:LprotocolAnalysis/protocol/GetServerPacket;

    iget-object v6, p0, Lnetwork/udp/UdpBroadcast;->s_buffer:[B

    invoke-virtual {v5, v6}, LprotocolAnalysis/protocol/GetServerPacket;->printf([B)Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-static {v3, v4}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    :try_start_0
    new-instance v3, Lnetwork/udp/BroadThread;

    iget-object v4, p0, Lnetwork/udp/UdpBroadcast;->s_buffer:[B

    invoke-direct {v3, v4}, Lnetwork/udp/BroadThread;-><init>([B)V

    sput-object v3, Lnetwork/udp/UdpBroadcast;->sendThread:Lnetwork/udp/BroadThread;

    sget-object v3, Lnetwork/udp/UdpBroadcast;->sendThread:Lnetwork/udp/BroadThread;

    invoke-virtual {v3}, Lnetwork/udp/BroadThread;->start()V

    const-string v3, "UdpBroadcast"

    const-string v4, "ip search Thread Start! "

    invoke-static {v3, v4}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I
    :try_end_0
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_0

    :goto_1
    return-void

    :cond_1
    sget-object v3, Lnetwork/udp/UdpBroadcast;->devName:Ljava/lang/String;

    const-string v4, "6a800C"

    invoke-virtual {v3, v4}, Ljava/lang/String;->indexOf(Ljava/lang/String;)I

    move-result v3

    if-ltz v3, :cond_2

    const/4 v3, 0x3

    sput v3, Lnetwork/udp/UdpBroadcast;->devNameType:I

    goto :goto_0

    :cond_2
    sget-object v3, Lnetwork/udp/UdpBroadcast;->devName:Ljava/lang/String;

    const-string v4, "6a800"

    invoke-virtual {v3, v4}, Ljava/lang/String;->indexOf(Ljava/lang/String;)I

    move-result v3

    if-ltz v3, :cond_0

    const/4 v3, 0x2

    sput v3, Lnetwork/udp/UdpBroadcast;->devNameType:I

    goto :goto_0

    :catch_0
    move-exception v0

    invoke-virtual {v0}, Ljava/io/IOException;->printStackTrace()V

    goto :goto_1
.end method

.method private Open(I)V
    .locals 3
    .param p1    # I

    const-string v0, "UdpBroadcast"

    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "Open interface: "

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    invoke-direct {p0, p1}, Lnetwork/udp/UdpBroadcast;->IpThreadStart(I)V

    return-void
.end method

.method private closeScan()V
    .locals 3

    const-string v1, "UdpBroadcast"

    const-string v2, "closeScan"

    invoke-static {v1, v2}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    :try_start_0
    sget-object v1, Lnetwork/udp/UdpBroadcast;->sendThread:Lnetwork/udp/BroadThread;

    const/4 v2, 0x0

    iput-boolean v2, v1, Lnetwork/udp/BroadThread;->run_flag:Z

    const-wide/16 v1, 0x5dc

    invoke-static {v1, v2}, Ljava/lang/Thread;->sleep(J)V
    :try_end_0
    .catch Ljava/lang/InterruptedException; {:try_start_0 .. :try_end_0} :catch_0

    :goto_0
    return-void

    :catch_0
    move-exception v0

    invoke-virtual {v0}, Ljava/lang/InterruptedException;->printStackTrace()V

    goto :goto_0
.end method

.method private getNetIpAddrList()Ljava/lang/String;
    .locals 6

    const-string v2, "UdpBroadcast"

    const-string v3, "getNetIpAddrList !"

    invoke-static {v2, v3}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    sget-object v0, Lnetwork/udp/BroadThread;->connMap:Ljava/util/Map;

    invoke-interface {v0}, Ljava/util/Map;->isEmpty()Z

    move-result v2

    if-eqz v2, :cond_0

    const-string v2, "UdpBroadcast"

    const-string v3, "connMap.isEmpty()"

    invoke-static {v2, v3}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    const-string v2, "|<R>\u951f\u65a4\u62f7\u951f\u65a4\u62f7\u951f\u65a4\u62f7\u951f\u65a4\u62f7|<Q>\u951f\u527f\u7b79\u62f7\u951f\u65a4\u62f7\u951f\u65a4\u62f7|"

    iput-object v2, p0, Lnetwork/udp/UdpBroadcast;->ipList:Ljava/lang/String;

    :goto_0
    iget-object v2, p0, Lnetwork/udp/UdpBroadcast;->ipList:Ljava/lang/String;

    return-object v2

    :cond_0
    const-string v2, "|"

    iput-object v2, p0, Lnetwork/udp/UdpBroadcast;->ipList:Ljava/lang/String;

    invoke-interface {v0}, Ljava/util/Map;->keySet()Ljava/util/Set;

    move-result-object v2

    invoke-interface {v2}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    move-result-object v3

    :goto_1
    invoke-interface {v3}, Ljava/util/Iterator;->hasNext()Z

    move-result v2

    if-nez v2, :cond_1

    const-string v2, "UdpBroadcast"

    new-instance v3, Ljava/lang/StringBuilder;

    const-string v4, "getNetIpAddrList: "

    invoke-direct {v3, v4}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    iget-object v4, p0, Lnetwork/udp/UdpBroadcast;->ipList:Ljava/lang/String;

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v2, v3}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_0

    :cond_1
    invoke-interface {v3}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v1

    const-string v4, "UdpBroadcast"

    new-instance v5, Ljava/lang/StringBuilder;

    const-string v2, "IP: "

    invoke-direct {v5, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-interface {v0, v1}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Ljava/lang/String;

    invoke-virtual {v5, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v4, v2}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    iget-object v2, p0, Lnetwork/udp/UdpBroadcast;->ipList:Ljava/lang/String;

    new-instance v4, Ljava/lang/StringBuilder;

    invoke-static {v2}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v2

    invoke-direct {v4, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-interface {v0, v1}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Ljava/lang/String;

    invoke-virtual {v4, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    iput-object v2, p0, Lnetwork/udp/UdpBroadcast;->ipList:Ljava/lang/String;

    iget-object v2, p0, Lnetwork/udp/UdpBroadcast;->ipList:Ljava/lang/String;

    new-instance v4, Ljava/lang/StringBuilder;

    invoke-static {v2}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v2

    invoke-direct {v4, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    const-string v2, "|"

    invoke-virtual {v4, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    iput-object v2, p0, Lnetwork/udp/UdpBroadcast;->ipList:Ljava/lang/String;

    goto :goto_1
.end method


# virtual methods
.method public Clear()V
    .locals 3

    :try_start_0
    sget-object v1, Lnetwork/udp/UdpBroadcast;->sendThread:Lnetwork/udp/BroadThread;

    invoke-virtual {v1}, Lnetwork/udp/BroadThread;->ClearDataSet()V

    const-wide/16 v1, 0x7d0

    invoke-static {v1, v2}, Ljava/lang/Thread;->sleep(J)V
    :try_end_0
    .catch Ljava/lang/InterruptedException; {:try_start_0 .. :try_end_0} :catch_0

    :goto_0
    return-void

    :catch_0
    move-exception v0

    invoke-virtual {v0}, Ljava/lang/InterruptedException;->printStackTrace()V

    goto :goto_0
.end method

.method public Close()Z
    .locals 2

    const-string v0, "UdpBroadcast"

    const-string v1, "Close"

    invoke-static {v0, v1}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    invoke-direct {p0}, Lnetwork/udp/UdpBroadcast;->closeScan()V

    const/4 v0, 0x1

    return v0
.end method

.method public Get(I)Ljava/lang/String;
    .locals 3
    .param p1    # I

    iget v1, p0, Lnetwork/udp/UdpBroadcast;->mSerivceType:I

    if-eq v1, p1, :cond_0

    const-string v1, "UdpBroadcast"

    const-string v2, "to call get ,mSerivceType!=Type"

    invoke-static {v1, v2}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    :try_start_0
    invoke-virtual {p0, p1}, Lnetwork/udp/UdpBroadcast;->reSetPacket(I)V

    const-wide/16 v1, 0x7d0

    invoke-static {v1, v2}, Ljava/lang/Thread;->sleep(J)V
    :try_end_0
    .catch Ljava/lang/InterruptedException; {:try_start_0 .. :try_end_0} :catch_0

    :cond_0
    :goto_0
    invoke-direct {p0}, Lnetwork/udp/UdpBroadcast;->getNetIpAddrList()Ljava/lang/String;

    iget-object v1, p0, Lnetwork/udp/UdpBroadcast;->ipList:Ljava/lang/String;

    return-object v1

    :catch_0
    move-exception v0

    invoke-virtual {v0}, Ljava/lang/InterruptedException;->printStackTrace()V

    goto :goto_0
.end method

.method public Open()V
    .locals 1

    const/4 v0, 0x0

    invoke-direct {p0, v0}, Lnetwork/udp/UdpBroadcast;->Open(I)V

    return-void
.end method

.method public reSetPacket(I)V
    .locals 8
    .param p1    # I

    const/4 v7, 0x0

    const-string v4, "UdpBroadcast"

    new-instance v5, Ljava/lang/StringBuilder;

    const-string v6, "mSerivceType!=Typeold "

    invoke-direct {v5, v6}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    iget v6, p0, Lnetwork/udp/UdpBroadcast;->mSerivceType:I

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v5

    const-string v6, "new "

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5, p1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    invoke-static {v4, v5}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    iput-object v7, p0, Lnetwork/udp/UdpBroadcast;->s_broadpacket:LprotocolAnalysis/protocol/GetServerPacket;

    iput-object v7, p0, Lnetwork/udp/UdpBroadcast;->s_buffer:[B

    new-instance v2, LprotocolAnalysis/analysis/packetHandle;

    invoke-direct {v2}, LprotocolAnalysis/analysis/packetHandle;-><init>()V

    invoke-virtual {v2}, LprotocolAnalysis/analysis/packetHandle;->setDeviceNamePacket()[B

    move-result-object v3

    new-instance v0, Ljava/lang/String;

    invoke-direct {v0, v3}, Ljava/lang/String;-><init>([B)V

    new-instance v4, LprotocolAnalysis/protocol/GetServerPacket;

    invoke-virtual {v0}, Ljava/lang/String;->length()I

    move-result v5

    invoke-direct {v4, p1, v5}, LprotocolAnalysis/protocol/GetServerPacket;-><init>(II)V

    iput-object v4, p0, Lnetwork/udp/UdpBroadcast;->s_broadpacket:LprotocolAnalysis/protocol/GetServerPacket;

    iget-object v4, p0, Lnetwork/udp/UdpBroadcast;->s_broadpacket:LprotocolAnalysis/protocol/GetServerPacket;

    const/4 v5, 0x5

    const/16 v6, 0x2ff

    invoke-virtual {v4, v5, v6, v0}, LprotocolAnalysis/protocol/GetServerPacket;->SetDeviceInfo(SSLjava/lang/String;)V

    iget-object v4, p0, Lnetwork/udp/UdpBroadcast;->s_broadpacket:LprotocolAnalysis/protocol/GetServerPacket;

    invoke-virtual {v4}, LprotocolAnalysis/protocol/GetServerPacket;->sizeOf()I

    move-result v4

    new-array v4, v4, [B

    iput-object v4, p0, Lnetwork/udp/UdpBroadcast;->s_buffer:[B

    iget-object v4, p0, Lnetwork/udp/UdpBroadcast;->s_broadpacket:LprotocolAnalysis/protocol/GetServerPacket;

    iget-object v5, p0, Lnetwork/udp/UdpBroadcast;->s_buffer:[B

    invoke-virtual {v4, v5}, LprotocolAnalysis/protocol/GetServerPacket;->format([B)V

    sget-object v4, Lnetwork/udp/UdpBroadcast;->sendThread:Lnetwork/udp/BroadThread;

    if-nez v4, :cond_0

    const-wide/16 v4, 0xbb8

    :try_start_0
    invoke-static {v4, v5}, Ljava/lang/Thread;->sleep(J)V
    :try_end_0
    .catch Ljava/lang/InterruptedException; {:try_start_0 .. :try_end_0} :catch_0

    :goto_0
    iput p1, p0, Lnetwork/udp/UdpBroadcast;->mSerivceType:I

    return-void

    :catch_0
    move-exception v1

    invoke-virtual {v1}, Ljava/lang/InterruptedException;->printStackTrace()V

    goto :goto_0

    :cond_0
    sget-object v4, Lnetwork/udp/UdpBroadcast;->sendThread:Lnetwork/udp/BroadThread;

    iget-object v5, p0, Lnetwork/udp/UdpBroadcast;->s_buffer:[B

    iput-object v5, v4, Lnetwork/udp/BroadThread;->g_buffer:[B

    goto :goto_0
.end method
