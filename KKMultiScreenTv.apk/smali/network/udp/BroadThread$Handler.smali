.class public Lnetwork/udp/BroadThread$Handler;
.super Ljava/lang/Object;
.source "BroadThread.java"

# interfaces
.implements Ljava/lang/Runnable;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lnetwork/udp/BroadThread;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x4
    name = "Handler"
.end annotation


# instance fields
.field final synthetic this$0:Lnetwork/udp/BroadThread;


# direct methods
.method protected constructor <init>(Lnetwork/udp/BroadThread;)V
    .locals 0

    iput-object p1, p0, Lnetwork/udp/BroadThread$Handler;->this$0:Lnetwork/udp/BroadThread;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public run()V
    .locals 13

    const/4 v12, 0x1

    const-string v4, ""

    :cond_0
    :goto_0
    :try_start_0
    iget-object v8, p0, Lnetwork/udp/BroadThread$Handler;->this$0:Lnetwork/udp/BroadThread;

    # getter for: Lnetwork/udp/BroadThread;->s_socket:Ljava/net/DatagramSocket;
    invoke-static {v8}, Lnetwork/udp/BroadThread;->access$0(Lnetwork/udp/BroadThread;)Ljava/net/DatagramSocket;

    move-result-object v8

    if-nez v8, :cond_1

    return-void

    :cond_1
    const/16 v8, 0x80

    new-array v0, v8, [B

    array-length v8, v0

    invoke-static {v0, v8}, Lnetwork/udp/UdpService;->CreateDatagramPacket([BI)Ljava/net/DatagramPacket;

    move-result-object v5

    const-wide/16 v8, 0x3e8

    invoke-static {v8, v9}, Ljava/lang/Thread;->sleep(J)V

    iget-object v8, p0, Lnetwork/udp/BroadThread$Handler;->this$0:Lnetwork/udp/BroadThread;

    # getter for: Lnetwork/udp/BroadThread;->s_socket:Ljava/net/DatagramSocket;
    invoke-static {v8}, Lnetwork/udp/BroadThread;->access$0(Lnetwork/udp/BroadThread;)Ljava/net/DatagramSocket;

    move-result-object v8

    invoke-virtual {v8, v5}, Ljava/net/DatagramSocket;->receive(Ljava/net/DatagramPacket;)V

    array-length v8, v0

    if-ge v8, v12, :cond_2

    const-string v8, "BroadThread"

    new-instance v9, Ljava/lang/StringBuilder;

    const-string v10, "recv packet buffer.length: "

    invoke-direct {v9, v10}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    array-length v10, v0

    invoke-virtual {v9, v10}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v9

    invoke-virtual {v9}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v9

    invoke-static {v8, v9}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    :cond_2
    array-length v8, v0

    if-lez v8, :cond_0

    invoke-virtual {v5}, Ljava/net/DatagramPacket;->getAddress()Ljava/net/InetAddress;

    move-result-object v8

    invoke-virtual {v8}, Ljava/net/InetAddress;->toString()Ljava/lang/String;

    move-result-object v8

    const-string v9, "/"

    invoke-virtual {v8, v9}, Ljava/lang/String;->split(Ljava/lang/String;)[Ljava/lang/String;

    move-result-object v8

    const/4 v9, 0x1

    aget-object v4, v8, v9

    const-string v8, "."

    invoke-virtual {v4, v8}, Ljava/lang/String;->indexOf(Ljava/lang/String;)I

    move-result v8

    if-lez v8, :cond_0

    move-object v1, v4

    invoke-virtual {v5}, Ljava/net/DatagramPacket;->getPort()I

    move-result v2

    iget-object v8, p0, Lnetwork/udp/BroadThread$Handler;->this$0:Lnetwork/udp/BroadThread;

    # getter for: Lnetwork/udp/BroadThread;->devName:Ljava/lang/String;
    invoke-static {v8}, Lnetwork/udp/BroadThread;->access$1(Lnetwork/udp/BroadThread;)Ljava/lang/String;

    move-result-object v8

    if-nez v8, :cond_3

    iget-object v8, p0, Lnetwork/udp/BroadThread$Handler;->this$0:Lnetwork/udp/BroadThread;

    const-string v9, "tv device"

    invoke-static {v8, v9}, Lnetwork/udp/BroadThread;->access$2(Lnetwork/udp/BroadThread;Ljava/lang/String;)V

    :cond_3
    new-instance v6, Ljava/net/DatagramPacket;

    iget-object v8, p0, Lnetwork/udp/BroadThread$Handler;->this$0:Lnetwork/udp/BroadThread;

    # getter for: Lnetwork/udp/BroadThread;->buff:[B
    invoke-static {v8}, Lnetwork/udp/BroadThread;->access$3(Lnetwork/udp/BroadThread;)[B

    move-result-object v8

    iget-object v9, p0, Lnetwork/udp/BroadThread$Handler;->this$0:Lnetwork/udp/BroadThread;

    # getter for: Lnetwork/udp/BroadThread;->buff:[B
    invoke-static {v9}, Lnetwork/udp/BroadThread;->access$3(Lnetwork/udp/BroadThread;)[B

    move-result-object v9

    array-length v9, v9

    invoke-static {v1}, Ljava/net/InetAddress;->getByName(Ljava/lang/String;)Ljava/net/InetAddress;

    move-result-object v10

    invoke-direct {v6, v8, v9, v10, v2}, Ljava/net/DatagramPacket;-><init>([BILjava/net/InetAddress;I)V

    iget-object v8, p0, Lnetwork/udp/BroadThread$Handler;->this$0:Lnetwork/udp/BroadThread;

    # getter for: Lnetwork/udp/BroadThread;->s_socket:Ljava/net/DatagramSocket;
    invoke-static {v8}, Lnetwork/udp/BroadThread;->access$0(Lnetwork/udp/BroadThread;)Ljava/net/DatagramSocket;

    move-result-object v8

    invoke-virtual {v8, v6}, Ljava/net/DatagramSocket;->send(Ljava/net/DatagramPacket;)V

    invoke-static {v0}, Lnetwork/udp/BroadThread;->ByteToString([B)Ljava/lang/String;

    move-result-object v7

    const-string v8, "BroadThread"

    new-instance v9, Ljava/lang/StringBuilder;

    const-string v10, "to send  packet ip: "

    invoke-direct {v9, v10}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v9, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v9

    const-string v10, " dest_port "

    invoke-virtual {v9, v10}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v9

    invoke-virtual {v9, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v9

    const-string v10, " and sendingBroad buffer: "

    invoke-virtual {v9, v10}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v9

    invoke-virtual {v9, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v9

    invoke-virtual {v9}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v9

    invoke-static {v8, v9}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    sget-object v8, Lnetwork/udp/BroadThread;->connMap:Ljava/util/Map;

    invoke-static {v4}, Lutil/comm/ToolClass;->getStringIpToLong(Ljava/lang/String;)J

    move-result-wide v9

    invoke-static {v9, v10}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v9

    invoke-interface {v8, v9}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v8

    if-nez v8, :cond_0

    sget-object v8, Lnetwork/udp/BroadThread;->connMap:Ljava/util/Map;

    invoke-static {v4}, Lutil/comm/ToolClass;->getStringIpToLong(Ljava/lang/String;)J

    move-result-wide v9

    invoke-static {v9, v10}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v9

    new-instance v10, Ljava/lang/StringBuilder;

    invoke-static {v4}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v11

    invoke-direct {v10, v11}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    const-string v11, "&"

    invoke-virtual {v10, v11}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v10

    invoke-virtual {v10, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v10

    invoke-virtual {v10}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v10

    invoke-interface {v8, v9, v10}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;
    :try_end_0
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_0
    .catch Ljava/lang/InterruptedException; {:try_start_0 .. :try_end_0} :catch_1

    goto/16 :goto_0

    :catch_0
    move-exception v3

    sget-object v8, Ljava/lang/System;->err:Ljava/io/PrintStream;

    new-instance v9, Ljava/lang/StringBuilder;

    const-string v10, "IOException "

    invoke-direct {v9, v10}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v3}, Ljava/io/IOException;->getMessage()Ljava/lang/String;

    move-result-object v10

    invoke-virtual {v9, v10}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v9

    invoke-virtual {v9}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v9

    invoke-virtual {v8, v9}, Ljava/io/PrintStream;->println(Ljava/lang/String;)V

    goto/16 :goto_0

    :catch_1
    move-exception v3

    sget-object v8, Ljava/lang/System;->err:Ljava/io/PrintStream;

    new-instance v9, Ljava/lang/StringBuilder;

    const-string v10, "InterruptedException "

    invoke-direct {v9, v10}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v3}, Ljava/lang/InterruptedException;->getMessage()Ljava/lang/String;

    move-result-object v10

    invoke-virtual {v9, v10}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v9

    invoke-virtual {v9}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v9

    invoke-virtual {v8, v9}, Ljava/io/PrintStream;->println(Ljava/lang/String;)V

    goto/16 :goto_0
.end method
