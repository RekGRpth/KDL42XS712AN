.class public Lnetwork/udp/UdpService;
.super Ljava/lang/Object;
.source "UdpService.java"


# static fields
.field private static final TAG:Ljava/lang/String; = "UdpService"

.field private static final logger:Lutil/log/Logger;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    invoke-static {}, Lutil/log/Logger;->getLogger()Lutil/log/Logger;

    move-result-object v0

    sput-object v0, Lnetwork/udp/UdpService;->logger:Lutil/log/Logger;

    return-void
.end method

.method public constructor <init>()V
    .locals 0

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method public static CreateBroadUdpSocket(II)Ljava/net/DatagramSocket;
    .locals 6
    .param p0    # I
    .param p1    # I

    const/4 v1, 0x0

    :try_start_0
    new-instance v2, Ljava/net/DatagramSocket;

    invoke-direct {v2, p0}, Ljava/net/DatagramSocket;-><init>(I)V
    :try_end_0
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_0

    if-nez v2, :cond_0

    :try_start_1
    sget-object v3, Lnetwork/udp/UdpService;->logger:Lutil/log/Logger;

    const-string v4, "UdpService"

    const-string v5, "CreateUdpSocket: socket == null"

    invoke-virtual {v3, v4, v5}, Lutil/log/Logger;->error(Ljava/lang/String;Ljava/lang/String;)V

    :cond_0
    invoke-virtual {v2, p1}, Ljava/net/DatagramSocket;->setSoTimeout(I)V

    const/4 v3, 0x1

    invoke-virtual {v2, v3}, Ljava/net/DatagramSocket;->setBroadcast(Z)V
    :try_end_1
    .catch Ljava/io/IOException; {:try_start_1 .. :try_end_1} :catch_1

    move-object v1, v2

    :goto_0
    return-object v1

    :catch_0
    move-exception v0

    :goto_1
    sget-object v3, Lnetwork/udp/UdpService;->logger:Lutil/log/Logger;

    const-string v4, "UdpService"

    invoke-virtual {v0}, Ljava/io/IOException;->getMessage()Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v3, v4, v5}, Lutil/log/Logger;->debug(Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_0

    :catch_1
    move-exception v0

    move-object v1, v2

    goto :goto_1
.end method

.method public static CreateDatagramPacket([BI)Ljava/net/DatagramPacket;
    .locals 4
    .param p0    # [B
    .param p1    # I
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    const/4 v0, 0x0

    new-instance v0, Ljava/net/DatagramPacket;

    invoke-direct {v0, p0, p1}, Ljava/net/DatagramPacket;-><init>([BI)V

    if-nez v0, :cond_0

    sget-object v1, Lnetwork/udp/UdpService;->logger:Lutil/log/Logger;

    const-string v2, "UdpService"

    const-string v3, "CreateDatagramPacket: packet == null"

    invoke-virtual {v1, v2, v3}, Lutil/log/Logger;->error(Ljava/lang/String;Ljava/lang/String;)V

    :cond_0
    return-object v0
.end method

.method public static CreateDatagramPacket([BILjava/net/InetAddress;I)Ljava/net/DatagramPacket;
    .locals 4
    .param p0    # [B
    .param p1    # I
    .param p2    # Ljava/net/InetAddress;
    .param p3    # I
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    const/4 v0, 0x0

    new-instance v0, Ljava/net/DatagramPacket;

    invoke-direct {v0, p0, p1, p2, p3}, Ljava/net/DatagramPacket;-><init>([BILjava/net/InetAddress;I)V

    if-nez v0, :cond_0

    sget-object v1, Lnetwork/udp/UdpService;->logger:Lutil/log/Logger;

    const-string v2, "UdpService"

    const-string v3, "CreateDatagramPacket: packet == null"

    invoke-virtual {v1, v2, v3}, Lutil/log/Logger;->error(Ljava/lang/String;Ljava/lang/String;)V

    :cond_0
    return-object v0
.end method

.method public static CreateInetAddress(Ljava/lang/String;)Ljava/net/InetAddress;
    .locals 5
    .param p0    # Ljava/lang/String;

    const/4 v0, 0x0

    :try_start_0
    invoke-static {p0}, Ljava/net/InetAddress;->getByName(Ljava/lang/String;)Ljava/net/InetAddress;
    :try_end_0
    .catch Ljava/net/UnknownHostException; {:try_start_0 .. :try_end_0} :catch_0

    move-result-object v0

    :goto_0
    return-object v0

    :catch_0
    move-exception v1

    sget-object v2, Lnetwork/udp/UdpService;->logger:Lutil/log/Logger;

    const-string v3, "UdpService"

    invoke-virtual {v1}, Ljava/net/UnknownHostException;->getMessage()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v2, v3, v4}, Lutil/log/Logger;->debug(Ljava/lang/String;Ljava/lang/String;)V

    invoke-virtual {v1}, Ljava/net/UnknownHostException;->printStackTrace()V

    goto :goto_0
.end method
