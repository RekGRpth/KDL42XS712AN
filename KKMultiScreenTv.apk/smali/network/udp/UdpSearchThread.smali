.class public Lnetwork/udp/UdpSearchThread;
.super Ljava/lang/Object;
.source "UdpSearchThread.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lnetwork/udp/UdpSearchThread$Listener;
    }
.end annotation


# static fields
.field private static final DEF_NAME:Ljava/lang/String; = "tv device"

.field private static final SET_TIMEOUT:I = 0x7a120

.field private static final TAG:Ljava/lang/String; = "UdpSearchThread"

.field private static final UDP_REV_PORT:S = 0x1f40s

.field private static final UDP_SEND_PORT:S = 0x1f49s


# instance fields
.field private buff:[B

.field private businessFlag:Ljava/lang/String;

.field private devName:Ljava/lang/String;

.field private devPacket:LprotocolAnalysis/protocol/SendUdpInfoPacket;

.field private socket:Ljava/net/DatagramSocket;

.field private thread:Ljava/lang/Thread;


# direct methods
.method public constructor <init>()V
    .locals 6

    const/4 v1, 0x0

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    iput-object v1, p0, Lnetwork/udp/UdpSearchThread;->devName:Ljava/lang/String;

    iput-object v1, p0, Lnetwork/udp/UdpSearchThread;->devPacket:LprotocolAnalysis/protocol/SendUdpInfoPacket;

    iput-object v1, p0, Lnetwork/udp/UdpSearchThread;->thread:Ljava/lang/Thread;

    iput-object v1, p0, Lnetwork/udp/UdpSearchThread;->buff:[B

    iput-object v1, p0, Lnetwork/udp/UdpSearchThread;->businessFlag:Ljava/lang/String;

    :try_start_0
    new-instance v1, Ljava/net/DatagramSocket;

    const/16 v2, 0x1f40

    invoke-direct {v1, v2}, Ljava/net/DatagramSocket;-><init>(I)V

    iput-object v1, p0, Lnetwork/udp/UdpSearchThread;->socket:Ljava/net/DatagramSocket;

    iget-object v1, p0, Lnetwork/udp/UdpSearchThread;->socket:Ljava/net/DatagramSocket;

    const v2, 0x7a120

    invoke-virtual {v1, v2}, Ljava/net/DatagramSocket;->setSoTimeout(I)V
    :try_end_0
    .catch Ljava/net/SocketException; {:try_start_0 .. :try_end_0} :catch_0

    :goto_0
    invoke-direct {p0}, Lnetwork/udp/UdpSearchThread;->GetDeviceName()V

    new-instance v1, LprotocolAnalysis/protocol/SendUdpInfoPacket;

    iget-object v2, p0, Lnetwork/udp/UdpSearchThread;->devName:Ljava/lang/String;

    invoke-virtual {v2}, Ljava/lang/String;->length()I

    move-result v2

    invoke-direct {v1, v2}, LprotocolAnalysis/protocol/SendUdpInfoPacket;-><init>(I)V

    iput-object v1, p0, Lnetwork/udp/UdpSearchThread;->devPacket:LprotocolAnalysis/protocol/SendUdpInfoPacket;

    iget-object v1, p0, Lnetwork/udp/UdpSearchThread;->devPacket:LprotocolAnalysis/protocol/SendUdpInfoPacket;

    const/4 v2, 0x0

    const/4 v3, 0x5

    const/16 v4, 0x2ff

    iget-object v5, p0, Lnetwork/udp/UdpSearchThread;->devName:Ljava/lang/String;

    invoke-virtual {v1, v2, v3, v4, v5}, LprotocolAnalysis/protocol/SendUdpInfoPacket;->SetDeviceInfo(ISSLjava/lang/String;)V

    iget-object v1, p0, Lnetwork/udp/UdpSearchThread;->devPacket:LprotocolAnalysis/protocol/SendUdpInfoPacket;

    invoke-virtual {v1}, LprotocolAnalysis/protocol/SendUdpInfoPacket;->sizeOf()I

    move-result v1

    new-array v1, v1, [B

    iput-object v1, p0, Lnetwork/udp/UdpSearchThread;->buff:[B

    iget-object v1, p0, Lnetwork/udp/UdpSearchThread;->devPacket:LprotocolAnalysis/protocol/SendUdpInfoPacket;

    iget-object v2, p0, Lnetwork/udp/UdpSearchThread;->buff:[B

    invoke-virtual {v1, v2}, LprotocolAnalysis/protocol/SendUdpInfoPacket;->format([B)V

    const-string v1, "UdpSearchThread"

    new-instance v2, Ljava/lang/StringBuilder;

    const-string v3, "devPacket: "

    invoke-direct {v2, v3}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    iget-object v3, p0, Lnetwork/udp/UdpSearchThread;->devPacket:LprotocolAnalysis/protocol/SendUdpInfoPacket;

    iget-object v4, p0, Lnetwork/udp/UdpSearchThread;->buff:[B

    invoke-virtual {v3, v4}, LprotocolAnalysis/protocol/SendUdpInfoPacket;->printf([B)Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    return-void

    :catch_0
    move-exception v0

    invoke-virtual {v0}, Ljava/net/SocketException;->printStackTrace()V

    goto :goto_0
.end method

.method private GetDeviceName()V
    .locals 4

    new-instance v0, LprotocolAnalysis/analysis/packetHandle;

    invoke-direct {v0}, LprotocolAnalysis/analysis/packetHandle;-><init>()V

    invoke-virtual {v0}, LprotocolAnalysis/analysis/packetHandle;->setDeviceNamePacket()[B

    move-result-object v1

    new-instance v2, Ljava/lang/String;

    invoke-direct {v2, v1}, Ljava/lang/String;-><init>([B)V

    iput-object v2, p0, Lnetwork/udp/UdpSearchThread;->devName:Ljava/lang/String;

    const-string v2, "devName"

    iget-object v3, p0, Lnetwork/udp/UdpSearchThread;->devName:Ljava/lang/String;

    invoke-virtual {v3}, Ljava/lang/String;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v2, v3}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    return-void
.end method

.method static synthetic access$0(Lnetwork/udp/UdpSearchThread;)Ljava/net/DatagramSocket;
    .locals 1

    iget-object v0, p0, Lnetwork/udp/UdpSearchThread;->socket:Ljava/net/DatagramSocket;

    return-object v0
.end method

.method static synthetic access$1(Lnetwork/udp/UdpSearchThread;)Ljava/lang/String;
    .locals 1

    iget-object v0, p0, Lnetwork/udp/UdpSearchThread;->devName:Ljava/lang/String;

    return-object v0
.end method

.method static synthetic access$2(Lnetwork/udp/UdpSearchThread;Ljava/lang/String;)V
    .locals 0

    iput-object p1, p0, Lnetwork/udp/UdpSearchThread;->devName:Ljava/lang/String;

    return-void
.end method

.method static synthetic access$3(Lnetwork/udp/UdpSearchThread;)[B
    .locals 1

    iget-object v0, p0, Lnetwork/udp/UdpSearchThread;->buff:[B

    return-object v0
.end method


# virtual methods
.method public getLocalIpAddress()Ljava/lang/String;
    .locals 9

    new-instance v5, Ljava/lang/StringBuffer;

    const-string v6, ""

    invoke-direct {v5, v6}, Ljava/lang/StringBuffer;-><init>(Ljava/lang/String;)V

    :try_start_0
    invoke-static {}, Ljava/net/NetworkInterface;->getNetworkInterfaces()Ljava/util/Enumeration;

    move-result-object v1

    :cond_0
    invoke-interface {v1}, Ljava/util/Enumeration;->hasMoreElements()Z

    move-result v6

    if-nez v6, :cond_1

    const-string v6, "WifiPreference IpAddress"

    invoke-virtual {v5}, Ljava/lang/StringBuffer;->toString()Ljava/lang/String;

    move-result-object v7

    invoke-static {v6, v7}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    invoke-virtual {v5}, Ljava/lang/StringBuffer;->toString()Ljava/lang/String;

    move-result-object v6

    :goto_0
    return-object v6

    :cond_1
    invoke-interface {v1}, Ljava/util/Enumeration;->nextElement()Ljava/lang/Object;

    move-result-object v4

    check-cast v4, Ljava/net/NetworkInterface;

    invoke-virtual {v4}, Ljava/net/NetworkInterface;->getInetAddresses()Ljava/util/Enumeration;

    move-result-object v2

    :cond_2
    :goto_1
    invoke-interface {v2}, Ljava/util/Enumeration;->hasMoreElements()Z

    move-result v6

    if-eqz v6, :cond_0

    invoke-interface {v2}, Ljava/util/Enumeration;->nextElement()Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Ljava/net/InetAddress;

    invoke-virtual {v3}, Ljava/net/InetAddress;->getHostAddress()Ljava/lang/String;

    move-result-object v6

    const-string v7, "."

    invoke-virtual {v6, v7}, Ljava/lang/String;->indexOf(Ljava/lang/String;)I

    move-result v6

    if-lez v6, :cond_2

    invoke-virtual {v3}, Ljava/net/InetAddress;->getHostAddress()Ljava/lang/String;

    move-result-object v6

    const-string v7, "127.0.0.1"

    invoke-virtual {v6, v7}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v6

    if-nez v6, :cond_2

    invoke-virtual {v3}, Ljava/net/InetAddress;->getHostAddress()Ljava/lang/String;

    move-result-object v6

    invoke-virtual {v5, v6}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    goto :goto_1

    :catch_0
    move-exception v0

    sget-object v6, Ljava/lang/System;->err:Ljava/io/PrintStream;

    new-instance v7, Ljava/lang/StringBuilder;

    const-string v8, "Exception "

    invoke-direct {v7, v8}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v0}, Ljava/lang/Exception;->getMessage()Ljava/lang/String;

    move-result-object v8

    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    invoke-virtual {v7}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v7

    invoke-virtual {v6, v7}, Ljava/io/PrintStream;->println(Ljava/lang/String;)V

    const/4 v6, 0x0

    goto :goto_0
.end method

.method public listen()V
    .locals 2

    new-instance v0, Ljava/lang/Thread;

    new-instance v1, Lnetwork/udp/UdpSearchThread$Listener;

    invoke-direct {v1, p0}, Lnetwork/udp/UdpSearchThread$Listener;-><init>(Lnetwork/udp/UdpSearchThread;)V

    invoke-direct {v0, v1}, Ljava/lang/Thread;-><init>(Ljava/lang/Runnable;)V

    iput-object v0, p0, Lnetwork/udp/UdpSearchThread;->thread:Ljava/lang/Thread;

    iget-object v0, p0, Lnetwork/udp/UdpSearchThread;->thread:Ljava/lang/Thread;

    invoke-virtual {v0}, Ljava/lang/Thread;->start()V

    return-void
.end method

.method public unListen()V
    .locals 1

    iget-object v0, p0, Lnetwork/udp/UdpSearchThread;->thread:Ljava/lang/Thread;

    invoke-virtual {v0}, Ljava/lang/Thread;->interrupt()V

    return-void
.end method
