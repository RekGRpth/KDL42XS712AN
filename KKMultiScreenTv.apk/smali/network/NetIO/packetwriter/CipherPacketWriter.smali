.class public Lnetwork/NetIO/packetwriter/CipherPacketWriter;
.super Ljava/lang/Object;
.source "CipherPacketWriter.java"

# interfaces
.implements Lnetwork/Interface/PacketWriter;


# static fields
.field static final synthetic $assertionsDisabled:Z


# instance fields
.field private final m_cipher:Ljavax/crypto/Cipher;

.field private m_packetWriter:Lnetwork/Interface/PacketWriter;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    const-class v0, Lnetwork/NetIO/packetwriter/CipherPacketWriter;

    invoke-virtual {v0}, Ljava/lang/Class;->desiredAssertionStatus()Z

    move-result v0

    if-nez v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    sput-boolean v0, Lnetwork/NetIO/packetwriter/CipherPacketWriter;->$assertionsDisabled:Z

    return-void

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public constructor <init>(Ljavax/crypto/Cipher;Lnetwork/Interface/PacketWriter;)V
    .locals 0
    .param p1    # Ljavax/crypto/Cipher;
    .param p2    # Lnetwork/Interface/PacketWriter;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    iput-object p1, p0, Lnetwork/NetIO/packetwriter/CipherPacketWriter;->m_cipher:Ljavax/crypto/Cipher;

    iput-object p2, p0, Lnetwork/NetIO/packetwriter/CipherPacketWriter;->m_packetWriter:Lnetwork/Interface/PacketWriter;

    return-void
.end method


# virtual methods
.method public getPacketWriter()Lnetwork/Interface/PacketWriter;
    .locals 1

    iget-object v0, p0, Lnetwork/NetIO/packetwriter/CipherPacketWriter;->m_packetWriter:Lnetwork/Interface/PacketWriter;

    return-object v0
.end method

.method public setPacketWriter(Lnetwork/Interface/PacketWriter;)V
    .locals 0
    .param p1    # Lnetwork/Interface/PacketWriter;

    iput-object p1, p0, Lnetwork/NetIO/packetwriter/CipherPacketWriter;->m_packetWriter:Lnetwork/Interface/PacketWriter;

    return-void
.end method

.method public write([Ljava/nio/ByteBuffer;)[Ljava/nio/ByteBuffer;
    .locals 6
    .param p1    # [Ljava/nio/ByteBuffer;

    iget-object v3, p0, Lnetwork/NetIO/packetwriter/CipherPacketWriter;->m_packetWriter:Lnetwork/Interface/PacketWriter;

    invoke-interface {v3, p1}, Lnetwork/Interface/PacketWriter;->write([Ljava/nio/ByteBuffer;)[Ljava/nio/ByteBuffer;

    move-result-object p1

    array-length v3, p1

    new-array v2, v3, [Ljava/nio/ByteBuffer;

    const/4 v1, 0x0

    :goto_0
    :try_start_0
    array-length v3, p1

    if-lt v1, v3, :cond_0

    return-object v2

    :cond_0
    iget-object v3, p0, Lnetwork/NetIO/packetwriter/CipherPacketWriter;->m_cipher:Ljavax/crypto/Cipher;

    aget-object v4, p1, v1

    invoke-virtual {v4}, Ljava/nio/ByteBuffer;->remaining()I

    move-result v4

    invoke-virtual {v3, v4}, Ljavax/crypto/Cipher;->getOutputSize(I)I

    move-result v3

    invoke-static {v3}, Ljava/nio/ByteBuffer;->allocate(I)Ljava/nio/ByteBuffer;

    move-result-object v3

    aput-object v3, v2, v1

    array-length v3, p1

    add-int/lit8 v3, v3, -0x1

    if-ne v1, v3, :cond_1

    iget-object v3, p0, Lnetwork/NetIO/packetwriter/CipherPacketWriter;->m_cipher:Ljavax/crypto/Cipher;

    aget-object v4, p1, v1

    aget-object v5, v2, v1

    invoke-virtual {v3, v4, v5}, Ljavax/crypto/Cipher;->doFinal(Ljava/nio/ByteBuffer;Ljava/nio/ByteBuffer;)I

    :goto_1
    sget-boolean v3, Lnetwork/NetIO/packetwriter/CipherPacketWriter;->$assertionsDisabled:Z

    if-nez v3, :cond_2

    aget-object v3, p1, v1

    invoke-virtual {v3}, Ljava/nio/ByteBuffer;->remaining()I

    move-result v3

    if-eqz v3, :cond_2

    new-instance v3, Ljava/lang/AssertionError;

    invoke-direct {v3}, Ljava/lang/AssertionError;-><init>()V

    throw v3
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    :catch_0
    move-exception v0

    new-instance v3, Ljava/lang/RuntimeException;

    invoke-direct {v3, v0}, Ljava/lang/RuntimeException;-><init>(Ljava/lang/Throwable;)V

    throw v3

    :cond_1
    :try_start_1
    iget-object v3, p0, Lnetwork/NetIO/packetwriter/CipherPacketWriter;->m_cipher:Ljavax/crypto/Cipher;

    aget-object v4, p1, v1

    aget-object v5, v2, v1

    invoke-virtual {v3, v4, v5}, Ljavax/crypto/Cipher;->update(Ljava/nio/ByteBuffer;Ljava/nio/ByteBuffer;)I

    goto :goto_1

    :cond_2
    aget-object v3, v2, v1

    invoke-virtual {v3}, Ljava/nio/ByteBuffer;->flip()Ljava/nio/Buffer;
    :try_end_1
    .catch Ljava/lang/Exception; {:try_start_1 .. :try_end_1} :catch_0

    add-int/lit8 v1, v1, 0x1

    goto :goto_0
.end method
