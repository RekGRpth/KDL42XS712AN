.class public Lnetwork/NetIO/packetwriter/RegularPacketWriter;
.super Ljava/lang/Object;
.source "RegularPacketWriter.java"

# interfaces
.implements Lnetwork/Interface/PacketWriter;


# instance fields
.field private final m_bigEndian:Z

.field private final m_header:Ljava/nio/ByteBuffer;


# direct methods
.method public constructor <init>(IZ)V
    .locals 2
    .param p1    # I
    .param p2    # Z

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    const/4 v0, 0x1

    if-lt p1, v0, :cond_0

    const/4 v0, 0x4

    if-le p1, v0, :cond_1

    :cond_0
    new-instance v0, Ljava/lang/IllegalArgumentException;

    const-string v1, "Header must be between 1 and 4 bytes long."

    invoke-direct {v0, v1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v0

    :cond_1
    iput-boolean p2, p0, Lnetwork/NetIO/packetwriter/RegularPacketWriter;->m_bigEndian:Z

    invoke-static {p1}, Ljava/nio/ByteBuffer;->allocate(I)Ljava/nio/ByteBuffer;

    move-result-object v0

    iput-object v0, p0, Lnetwork/NetIO/packetwriter/RegularPacketWriter;->m_header:Ljava/nio/ByteBuffer;

    return-void
.end method


# virtual methods
.method public write([Ljava/nio/ByteBuffer;)[Ljava/nio/ByteBuffer;
    .locals 4
    .param p1    # [Ljava/nio/ByteBuffer;

    iget-object v0, p0, Lnetwork/NetIO/packetwriter/RegularPacketWriter;->m_header:Ljava/nio/ByteBuffer;

    invoke-virtual {v0}, Ljava/nio/ByteBuffer;->clear()Ljava/nio/Buffer;

    iget-object v0, p0, Lnetwork/NetIO/packetwriter/RegularPacketWriter;->m_header:Ljava/nio/ByteBuffer;

    iget-object v1, p0, Lnetwork/NetIO/packetwriter/RegularPacketWriter;->m_header:Ljava/nio/ByteBuffer;

    invoke-virtual {v1}, Ljava/nio/ByteBuffer;->capacity()I

    move-result v1

    invoke-static {p1}, Lnetwork/util/NIOUtils;->remaining([Ljava/nio/ByteBuffer;)J

    move-result-wide v2

    long-to-int v2, v2

    iget-boolean v3, p0, Lnetwork/NetIO/packetwriter/RegularPacketWriter;->m_bigEndian:Z

    invoke-static {v0, v1, v2, v3}, Lnetwork/util/NIOUtils;->setPacketSizeInByteBuffer(Ljava/nio/ByteBuffer;IIZ)V

    iget-object v0, p0, Lnetwork/NetIO/packetwriter/RegularPacketWriter;->m_header:Ljava/nio/ByteBuffer;

    invoke-virtual {v0}, Ljava/nio/ByteBuffer;->flip()Ljava/nio/Buffer;

    iget-object v0, p0, Lnetwork/NetIO/packetwriter/RegularPacketWriter;->m_header:Ljava/nio/ByteBuffer;

    invoke-static {v0, p1}, Lnetwork/util/NIOUtils;->concat(Ljava/nio/ByteBuffer;[Ljava/nio/ByteBuffer;)[Ljava/nio/ByteBuffer;

    move-result-object v0

    return-object v0
.end method
