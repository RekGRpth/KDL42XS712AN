.class public Lnetwork/NetIO/packetwriter/DelimiterPacketWriter;
.super Ljava/lang/Object;
.source "DelimiterPacketWriter.java"

# interfaces
.implements Lnetwork/Interface/PacketWriter;


# instance fields
.field private m_endByte:Ljava/nio/ByteBuffer;


# direct methods
.method public constructor <init>(B)V
    .locals 2
    .param p1    # B

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    const/4 v0, 0x1

    new-array v0, v0, [B

    const/4 v1, 0x0

    aput-byte p1, v0, v1

    invoke-static {v0}, Ljava/nio/ByteBuffer;->wrap([B)Ljava/nio/ByteBuffer;

    move-result-object v0

    iput-object v0, p0, Lnetwork/NetIO/packetwriter/DelimiterPacketWriter;->m_endByte:Ljava/nio/ByteBuffer;

    return-void
.end method


# virtual methods
.method public write([Ljava/nio/ByteBuffer;)[Ljava/nio/ByteBuffer;
    .locals 1
    .param p1    # [Ljava/nio/ByteBuffer;

    iget-object v0, p0, Lnetwork/NetIO/packetwriter/DelimiterPacketWriter;->m_endByte:Ljava/nio/ByteBuffer;

    invoke-virtual {v0}, Ljava/nio/ByteBuffer;->rewind()Ljava/nio/Buffer;

    iget-object v0, p0, Lnetwork/NetIO/packetwriter/DelimiterPacketWriter;->m_endByte:Ljava/nio/ByteBuffer;

    invoke-static {p1, v0}, Lnetwork/util/NIOUtils;->concat([Ljava/nio/ByteBuffer;Ljava/nio/ByteBuffer;)[Ljava/nio/ByteBuffer;

    move-result-object v0

    return-object v0
.end method
