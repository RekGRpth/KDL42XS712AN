.class public Lnetwork/NetIO/packetreader/CipherPacketReader;
.super Ljava/lang/Object;
.source "CipherPacketReader.java"

# interfaces
.implements Lnetwork/Interface/PacketReader;


# instance fields
.field private final m_cipher:Ljavax/crypto/Cipher;

.field private m_internalBuffer:Ljava/nio/ByteBuffer;

.field private m_reader:Lnetwork/Interface/PacketReader;


# direct methods
.method public constructor <init>(Ljavax/crypto/Cipher;Lnetwork/Interface/PacketReader;)V
    .locals 0
    .param p1    # Ljavax/crypto/Cipher;
    .param p2    # Lnetwork/Interface/PacketReader;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    iput-object p1, p0, Lnetwork/NetIO/packetreader/CipherPacketReader;->m_cipher:Ljavax/crypto/Cipher;

    iput-object p2, p0, Lnetwork/NetIO/packetreader/CipherPacketReader;->m_reader:Lnetwork/Interface/PacketReader;

    return-void
.end method


# virtual methods
.method public getReader()Lnetwork/Interface/PacketReader;
    .locals 1

    iget-object v0, p0, Lnetwork/NetIO/packetreader/CipherPacketReader;->m_reader:Lnetwork/Interface/PacketReader;

    return-object v0
.end method

.method public nextPacket(Ljava/nio/ByteBuffer;)[B
    .locals 5
    .param p1    # Ljava/nio/ByteBuffer;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lnetwork/exception/ProtocolViolationException;
        }
    .end annotation

    iget-object v3, p0, Lnetwork/NetIO/packetreader/CipherPacketReader;->m_internalBuffer:Ljava/nio/ByteBuffer;

    if-nez v3, :cond_3

    iget-object v3, p0, Lnetwork/NetIO/packetreader/CipherPacketReader;->m_cipher:Ljavax/crypto/Cipher;

    invoke-virtual {p1}, Ljava/nio/ByteBuffer;->remaining()I

    move-result v4

    invoke-virtual {v3, v4}, Ljavax/crypto/Cipher;->getOutputSize(I)I

    move-result v3

    invoke-static {v3}, Ljava/nio/ByteBuffer;->allocate(I)Ljava/nio/ByteBuffer;

    move-result-object v3

    iput-object v3, p0, Lnetwork/NetIO/packetreader/CipherPacketReader;->m_internalBuffer:Ljava/nio/ByteBuffer;

    :cond_0
    :goto_0
    invoke-virtual {p1}, Ljava/nio/ByteBuffer;->remaining()I

    move-result v3

    if-lez v3, :cond_1

    :try_start_0
    iget-object v3, p0, Lnetwork/NetIO/packetreader/CipherPacketReader;->m_cipher:Ljavax/crypto/Cipher;

    iget-object v4, p0, Lnetwork/NetIO/packetreader/CipherPacketReader;->m_internalBuffer:Ljava/nio/ByteBuffer;

    invoke-virtual {v3, p1, v4}, Ljavax/crypto/Cipher;->update(Ljava/nio/ByteBuffer;Ljava/nio/ByteBuffer;)I
    :try_end_0
    .catch Ljavax/crypto/ShortBufferException; {:try_start_0 .. :try_end_0} :catch_0

    iget-object v3, p0, Lnetwork/NetIO/packetreader/CipherPacketReader;->m_internalBuffer:Ljava/nio/ByteBuffer;

    invoke-virtual {v3}, Ljava/nio/ByteBuffer;->flip()Ljava/nio/Buffer;

    :cond_1
    iget-object v3, p0, Lnetwork/NetIO/packetreader/CipherPacketReader;->m_reader:Lnetwork/Interface/PacketReader;

    iget-object v4, p0, Lnetwork/NetIO/packetreader/CipherPacketReader;->m_internalBuffer:Ljava/nio/ByteBuffer;

    invoke-interface {v3, v4}, Lnetwork/Interface/PacketReader;->nextPacket(Ljava/nio/ByteBuffer;)[B

    move-result-object v2

    iget-object v3, p0, Lnetwork/NetIO/packetreader/CipherPacketReader;->m_internalBuffer:Ljava/nio/ByteBuffer;

    invoke-virtual {v3}, Ljava/nio/ByteBuffer;->remaining()I

    move-result v3

    if-nez v3, :cond_2

    const/4 v3, 0x0

    iput-object v3, p0, Lnetwork/NetIO/packetreader/CipherPacketReader;->m_internalBuffer:Ljava/nio/ByteBuffer;

    :cond_2
    return-object v2

    :cond_3
    invoke-virtual {p1}, Ljava/nio/ByteBuffer;->remaining()I

    move-result v3

    if-lez v3, :cond_0

    iget-object v3, p0, Lnetwork/NetIO/packetreader/CipherPacketReader;->m_cipher:Ljavax/crypto/Cipher;

    invoke-virtual {p1}, Ljava/nio/ByteBuffer;->remaining()I

    move-result v4

    invoke-virtual {v3, v4}, Ljavax/crypto/Cipher;->getOutputSize(I)I

    move-result v3

    iget-object v4, p0, Lnetwork/NetIO/packetreader/CipherPacketReader;->m_internalBuffer:Ljava/nio/ByteBuffer;

    invoke-virtual {v4}, Ljava/nio/ByteBuffer;->remaining()I

    move-result v4

    add-int/2addr v3, v4

    invoke-static {v3}, Ljava/nio/ByteBuffer;->allocate(I)Ljava/nio/ByteBuffer;

    move-result-object v1

    iget-object v3, p0, Lnetwork/NetIO/packetreader/CipherPacketReader;->m_internalBuffer:Ljava/nio/ByteBuffer;

    invoke-virtual {v1, v3}, Ljava/nio/ByteBuffer;->put(Ljava/nio/ByteBuffer;)Ljava/nio/ByteBuffer;

    iput-object v1, p0, Lnetwork/NetIO/packetreader/CipherPacketReader;->m_internalBuffer:Ljava/nio/ByteBuffer;

    goto :goto_0

    :catch_0
    move-exception v0

    new-instance v3, Lnetwork/exception/ProtocolViolationException;

    const-string v4, "Short buffer"

    invoke-direct {v3, v4}, Lnetwork/exception/ProtocolViolationException;-><init>(Ljava/lang/String;)V

    throw v3
.end method

.method public setReader(Lnetwork/Interface/PacketReader;)V
    .locals 0
    .param p1    # Lnetwork/Interface/PacketReader;

    iput-object p1, p0, Lnetwork/NetIO/packetreader/CipherPacketReader;->m_reader:Lnetwork/Interface/PacketReader;

    return-void
.end method
