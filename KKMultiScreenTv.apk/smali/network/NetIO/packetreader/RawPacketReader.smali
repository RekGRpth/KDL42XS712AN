.class public Lnetwork/NetIO/packetreader/RawPacketReader;
.super Ljava/lang/Object;
.source "RawPacketReader.java"

# interfaces
.implements Lnetwork/Interface/PacketReader;


# static fields
.field public static final INSTANCE:Lnetwork/NetIO/packetreader/RawPacketReader;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    new-instance v0, Lnetwork/NetIO/packetreader/RawPacketReader;

    invoke-direct {v0}, Lnetwork/NetIO/packetreader/RawPacketReader;-><init>()V

    sput-object v0, Lnetwork/NetIO/packetreader/RawPacketReader;->INSTANCE:Lnetwork/NetIO/packetreader/RawPacketReader;

    return-void
.end method

.method private constructor <init>()V
    .locals 0

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public nextPacket(Ljava/nio/ByteBuffer;)[B
    .locals 2
    .param p1    # Ljava/nio/ByteBuffer;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lnetwork/exception/ProtocolViolationException;
        }
    .end annotation

    invoke-virtual {p1}, Ljava/nio/ByteBuffer;->remaining()I

    move-result v1

    new-array v0, v1, [B

    invoke-virtual {p1, v0}, Ljava/nio/ByteBuffer;->get([B)Ljava/nio/ByteBuffer;

    return-object v0
.end method
