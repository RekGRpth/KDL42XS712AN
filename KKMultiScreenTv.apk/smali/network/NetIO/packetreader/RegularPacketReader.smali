.class public Lnetwork/NetIO/packetreader/RegularPacketReader;
.super Ljava/lang/Object;
.source "RegularPacketReader.java"

# interfaces
.implements Lnetwork/Interface/PacketReader;


# instance fields
.field private final m_bigEndian:Z

.field private final m_headerSize:I


# direct methods
.method public constructor <init>(IZ)V
    .locals 2
    .param p1    # I
    .param p2    # Z

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    const/4 v0, 0x1

    if-lt p1, v0, :cond_0

    const/4 v0, 0x4

    if-le p1, v0, :cond_1

    :cond_0
    new-instance v0, Ljava/lang/IllegalArgumentException;

    const-string v1, "Header must be between 1 and 4 bytes long."

    invoke-direct {v0, v1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v0

    :cond_1
    iput-boolean p2, p0, Lnetwork/NetIO/packetreader/RegularPacketReader;->m_bigEndian:Z

    iput p1, p0, Lnetwork/NetIO/packetreader/RegularPacketReader;->m_headerSize:I

    return-void
.end method


# virtual methods
.method public nextPacket(Ljava/nio/ByteBuffer;)[B
    .locals 4
    .param p1    # Ljava/nio/ByteBuffer;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lnetwork/exception/ProtocolViolationException;
        }
    .end annotation

    const/4 v1, 0x0

    invoke-virtual {p1}, Ljava/nio/ByteBuffer;->remaining()I

    move-result v2

    iget v3, p0, Lnetwork/NetIO/packetreader/RegularPacketReader;->m_headerSize:I

    if-ge v2, v3, :cond_0

    :goto_0
    return-object v1

    :cond_0
    invoke-virtual {p1}, Ljava/nio/ByteBuffer;->mark()Ljava/nio/Buffer;

    iget v2, p0, Lnetwork/NetIO/packetreader/RegularPacketReader;->m_headerSize:I

    iget-boolean v3, p0, Lnetwork/NetIO/packetreader/RegularPacketReader;->m_bigEndian:Z

    invoke-static {p1, v2, v3}, Lnetwork/util/NIOUtils;->getPacketSizeFromByteBuffer(Ljava/nio/ByteBuffer;IZ)I

    move-result v0

    invoke-virtual {p1}, Ljava/nio/ByteBuffer;->remaining()I

    move-result v2

    if-lt v2, v0, :cond_1

    new-array v1, v0, [B

    invoke-virtual {p1, v1}, Ljava/nio/ByteBuffer;->get([B)Ljava/nio/ByteBuffer;

    goto :goto_0

    :cond_1
    invoke-virtual {p1}, Ljava/nio/ByteBuffer;->reset()Ljava/nio/Buffer;

    goto :goto_0
.end method
