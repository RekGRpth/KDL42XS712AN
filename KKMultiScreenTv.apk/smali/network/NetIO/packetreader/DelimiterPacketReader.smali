.class public Lnetwork/NetIO/packetreader/DelimiterPacketReader;
.super Ljava/lang/Object;
.source "DelimiterPacketReader.java"

# interfaces
.implements Lnetwork/Interface/PacketReader;


# instance fields
.field private m_delimiter:B

.field private volatile m_maxPacketSize:I


# direct methods
.method public constructor <init>(B)V
    .locals 1
    .param p1    # B

    const/4 v0, -0x1

    invoke-direct {p0, p1, v0}, Lnetwork/NetIO/packetreader/DelimiterPacketReader;-><init>(BI)V

    return-void
.end method

.method public constructor <init>(BI)V
    .locals 3
    .param p1    # B
    .param p2    # I

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    const/4 v0, 0x1

    if-ge p2, v0, :cond_0

    const/4 v0, -0x1

    if-eq p2, v0, :cond_0

    new-instance v0, Ljava/lang/IllegalArgumentException;

    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "Max packet size must be larger that 1, was: "

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v1, p2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v0

    :cond_0
    iput-byte p1, p0, Lnetwork/NetIO/packetreader/DelimiterPacketReader;->m_delimiter:B

    iput p2, p0, Lnetwork/NetIO/packetreader/DelimiterPacketReader;->m_maxPacketSize:I

    return-void
.end method


# virtual methods
.method public getMaxPacketSize()I
    .locals 1

    iget v0, p0, Lnetwork/NetIO/packetreader/DelimiterPacketReader;->m_maxPacketSize:I

    return v0
.end method

.method public nextPacket(Ljava/nio/ByteBuffer;)[B
    .locals 6
    .param p1    # Ljava/nio/ByteBuffer;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lnetwork/exception/ProtocolViolationException;
        }
    .end annotation

    invoke-virtual {p1}, Ljava/nio/ByteBuffer;->mark()Ljava/nio/Buffer;

    const/4 v0, 0x0

    :cond_0
    invoke-virtual {p1}, Ljava/nio/ByteBuffer;->remaining()I

    move-result v3

    if-gtz v3, :cond_1

    invoke-virtual {p1}, Ljava/nio/ByteBuffer;->reset()Ljava/nio/Buffer;

    const/4 v2, 0x0

    :goto_0
    return-object v2

    :cond_1
    invoke-virtual {p1}, Ljava/nio/ByteBuffer;->get()B

    move-result v1

    iget-byte v3, p0, Lnetwork/NetIO/packetreader/DelimiterPacketReader;->m_delimiter:B

    if-ne v1, v3, :cond_2

    new-array v2, v0, [B

    invoke-virtual {p1}, Ljava/nio/ByteBuffer;->reset()Ljava/nio/Buffer;

    invoke-virtual {p1, v2}, Ljava/nio/ByteBuffer;->get([B)Ljava/nio/ByteBuffer;

    invoke-virtual {p1}, Ljava/nio/ByteBuffer;->get()B

    goto :goto_0

    :cond_2
    add-int/lit8 v0, v0, 0x1

    iget v3, p0, Lnetwork/NetIO/packetreader/DelimiterPacketReader;->m_maxPacketSize:I

    if-lez v3, :cond_0

    iget v3, p0, Lnetwork/NetIO/packetreader/DelimiterPacketReader;->m_maxPacketSize:I

    if-le v0, v3, :cond_0

    new-instance v3, Lnetwork/exception/ProtocolViolationException;

    new-instance v4, Ljava/lang/StringBuilder;

    const-string v5, "Packet exceeds max "

    invoke-direct {v4, v5}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    iget v5, p0, Lnetwork/NetIO/packetreader/DelimiterPacketReader;->m_maxPacketSize:I

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-direct {v3, v4}, Lnetwork/exception/ProtocolViolationException;-><init>(Ljava/lang/String;)V

    throw v3
.end method

.method public setMaxPacketSize(I)V
    .locals 0
    .param p1    # I

    iput p1, p0, Lnetwork/NetIO/packetreader/DelimiterPacketReader;->m_maxPacketSize:I

    return-void
.end method
