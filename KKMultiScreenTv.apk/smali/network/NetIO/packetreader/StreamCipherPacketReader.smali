.class public Lnetwork/NetIO/packetreader/StreamCipherPacketReader;
.super Ljava/lang/Object;
.source "StreamCipherPacketReader.java"

# interfaces
.implements Lnetwork/Interface/PacketReader;


# instance fields
.field private final m_cipher:Ljavax/crypto/Cipher;

.field private m_internalBuffer:Ljava/nio/ByteBuffer;

.field private m_reader:Lnetwork/Interface/PacketReader;


# direct methods
.method public constructor <init>(Ljavax/crypto/Cipher;Lnetwork/Interface/PacketReader;)V
    .locals 0
    .param p1    # Ljavax/crypto/Cipher;
    .param p2    # Lnetwork/Interface/PacketReader;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    iput-object p1, p0, Lnetwork/NetIO/packetreader/StreamCipherPacketReader;->m_cipher:Ljavax/crypto/Cipher;

    iput-object p2, p0, Lnetwork/NetIO/packetreader/StreamCipherPacketReader;->m_reader:Lnetwork/Interface/PacketReader;

    return-void
.end method


# virtual methods
.method public nextPacket(Ljava/nio/ByteBuffer;)[B
    .locals 6
    .param p1    # Ljava/nio/ByteBuffer;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lnetwork/exception/ProtocolViolationException;
        }
    .end annotation

    iget-object v4, p0, Lnetwork/NetIO/packetreader/StreamCipherPacketReader;->m_internalBuffer:Ljava/nio/ByteBuffer;

    if-nez v4, :cond_1

    iget-object v4, p0, Lnetwork/NetIO/packetreader/StreamCipherPacketReader;->m_cipher:Ljavax/crypto/Cipher;

    invoke-virtual {p1}, Ljava/nio/ByteBuffer;->remaining()I

    move-result v5

    invoke-virtual {v4, v5}, Ljavax/crypto/Cipher;->getOutputSize(I)I

    move-result v4

    invoke-static {v4}, Ljava/nio/ByteBuffer;->allocate(I)Ljava/nio/ByteBuffer;

    move-result-object v4

    iput-object v4, p0, Lnetwork/NetIO/packetreader/StreamCipherPacketReader;->m_internalBuffer:Ljava/nio/ByteBuffer;

    :goto_0
    :try_start_0
    iget-object v4, p0, Lnetwork/NetIO/packetreader/StreamCipherPacketReader;->m_cipher:Ljavax/crypto/Cipher;

    iget-object v5, p0, Lnetwork/NetIO/packetreader/StreamCipherPacketReader;->m_internalBuffer:Ljava/nio/ByteBuffer;

    invoke-virtual {v4, p1, v5}, Ljavax/crypto/Cipher;->update(Ljava/nio/ByteBuffer;Ljava/nio/ByteBuffer;)I
    :try_end_0
    .catch Ljavax/crypto/ShortBufferException; {:try_start_0 .. :try_end_0} :catch_0

    move-result v0

    iget-object v4, p0, Lnetwork/NetIO/packetreader/StreamCipherPacketReader;->m_internalBuffer:Ljava/nio/ByteBuffer;

    invoke-virtual {v4}, Ljava/nio/ByteBuffer;->flip()Ljava/nio/Buffer;

    iget-object v4, p0, Lnetwork/NetIO/packetreader/StreamCipherPacketReader;->m_reader:Lnetwork/Interface/PacketReader;

    iget-object v5, p0, Lnetwork/NetIO/packetreader/StreamCipherPacketReader;->m_internalBuffer:Ljava/nio/ByteBuffer;

    invoke-interface {v4, v5}, Lnetwork/Interface/PacketReader;->nextPacket(Ljava/nio/ByteBuffer;)[B

    move-result-object v3

    iget-object v4, p0, Lnetwork/NetIO/packetreader/StreamCipherPacketReader;->m_internalBuffer:Ljava/nio/ByteBuffer;

    invoke-virtual {v4}, Ljava/nio/ByteBuffer;->remaining()I

    move-result v4

    if-nez v4, :cond_0

    const/4 v4, 0x0

    iput-object v4, p0, Lnetwork/NetIO/packetreader/StreamCipherPacketReader;->m_internalBuffer:Ljava/nio/ByteBuffer;

    :cond_0
    return-object v3

    :cond_1
    iget-object v4, p0, Lnetwork/NetIO/packetreader/StreamCipherPacketReader;->m_cipher:Ljavax/crypto/Cipher;

    invoke-virtual {p1}, Ljava/nio/ByteBuffer;->remaining()I

    move-result v5

    invoke-virtual {v4, v5}, Ljavax/crypto/Cipher;->getOutputSize(I)I

    move-result v4

    iget-object v5, p0, Lnetwork/NetIO/packetreader/StreamCipherPacketReader;->m_internalBuffer:Ljava/nio/ByteBuffer;

    invoke-virtual {v5}, Ljava/nio/ByteBuffer;->remaining()I

    move-result v5

    add-int/2addr v4, v5

    invoke-static {v4}, Ljava/nio/ByteBuffer;->allocate(I)Ljava/nio/ByteBuffer;

    move-result-object v2

    iget-object v4, p0, Lnetwork/NetIO/packetreader/StreamCipherPacketReader;->m_internalBuffer:Ljava/nio/ByteBuffer;

    invoke-virtual {v2, v4}, Ljava/nio/ByteBuffer;->put(Ljava/nio/ByteBuffer;)Ljava/nio/ByteBuffer;

    iput-object v2, p0, Lnetwork/NetIO/packetreader/StreamCipherPacketReader;->m_internalBuffer:Ljava/nio/ByteBuffer;

    goto :goto_0

    :catch_0
    move-exception v1

    new-instance v4, Lnetwork/exception/ProtocolViolationException;

    const-string v5, "Short buffer"

    invoke-direct {v4, v5}, Lnetwork/exception/ProtocolViolationException;-><init>(Ljava/lang/String;)V

    throw v4
.end method
