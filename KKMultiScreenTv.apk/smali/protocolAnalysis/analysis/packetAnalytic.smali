.class public LprotocolAnalysis/analysis/packetAnalytic;
.super Ljava/lang/Object;
.source "packetAnalytic.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        LprotocolAnalysis/analysis/packetAnalytic$MEDIA_TYPE;
    }
.end annotation


# static fields
.field public static MDTAG:Ljava/lang/String; = null

.field public static final TOUCHSIGN:S = 0x2710s


# instance fields
.field headLen:[B


# direct methods
.method static constructor <clinit>()V
    .locals 1

    const-string v0, "packetAnalytic"

    sput-object v0, LprotocolAnalysis/analysis/packetAnalytic;->MDTAG:Ljava/lang/String;

    return-void
.end method

.method public constructor <init>()V
    .locals 1

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    const/4 v0, 0x2

    new-array v0, v0, [B

    iput-object v0, p0, LprotocolAnalysis/analysis/packetAnalytic;->headLen:[B

    return-void
.end method


# virtual methods
.method public getBoardKeyCode([B)I
    .locals 2
    .param p1    # [B

    const/4 v0, 0x4

    aget-byte v0, p1, v0

    invoke-virtual {p0, v0}, LprotocolAnalysis/analysis/packetAnalytic;->getUnsignedFromByte(B)I

    move-result v0

    shl-int/lit8 v0, v0, 0x20

    const/4 v1, 0x5

    aget-byte v1, p1, v1

    invoke-virtual {p0, v1}, LprotocolAnalysis/analysis/packetAnalytic;->getUnsignedFromByte(B)I

    move-result v1

    shl-int/lit8 v1, v1, 0x10

    or-int/2addr v0, v1

    const/4 v1, 0x6

    aget-byte v1, p1, v1

    invoke-virtual {p0, v1}, LprotocolAnalysis/analysis/packetAnalytic;->getUnsignedFromByte(B)I

    move-result v1

    shl-int/lit8 v1, v1, 0x8

    or-int/2addr v0, v1

    const/4 v1, 0x7

    aget-byte v1, p1, v1

    invoke-virtual {p0, v1}, LprotocolAnalysis/analysis/packetAnalytic;->getUnsignedFromByte(B)I

    move-result v1

    or-int/2addr v0, v1

    return v0
.end method

.method public getIrKeyCode([B)I
    .locals 3
    .param p1    # [B

    const/4 v0, 0x0

    const/4 v1, 0x0

    const/4 v2, 0x5

    aget-byte v2, p1, v2

    invoke-virtual {p0, v2}, LprotocolAnalysis/analysis/packetAnalytic;->getUnsignedFromByte(B)I

    move-result v2

    and-int/lit16 v2, v2, 0xff

    int-to-short v0, v2

    const/4 v2, 0x4

    aget-byte v2, p1, v2

    invoke-virtual {p0, v2}, LprotocolAnalysis/analysis/packetAnalytic;->getUnsignedFromByte(B)I

    move-result v2

    and-int/lit16 v2, v2, 0xff

    int-to-short v1, v2

    shl-int/lit8 v2, v1, 0x8

    int-to-short v1, v2

    or-int v2, v0, v1

    return v2
.end method

.method public getMouseEventValue([B)[S
    .locals 4
    .param p1    # [B

    const/4 v2, 0x4

    new-array v0, v2, [S

    const/4 v1, 0x0

    aget-byte v2, p1, v2

    invoke-virtual {p0, v2}, LprotocolAnalysis/analysis/packetAnalytic;->getUnsignedFromByte(B)I

    move-result v2

    int-to-short v2, v2

    aput-short v2, v0, v1

    const/4 v1, 0x1

    const/4 v2, 0x5

    aget-byte v2, p1, v2

    invoke-virtual {p0, v2}, LprotocolAnalysis/analysis/packetAnalytic;->getUnsignedFromByte(B)I

    move-result v2

    shl-int/lit8 v2, v2, 0x8

    const/4 v3, 0x6

    aget-byte v3, p1, v3

    invoke-virtual {p0, v3}, LprotocolAnalysis/analysis/packetAnalytic;->getUnsignedFromByte(B)I

    move-result v3

    or-int/2addr v2, v3

    int-to-short v2, v2

    aput-short v2, v0, v1

    const/4 v1, 0x2

    const/4 v2, 0x7

    aget-byte v2, p1, v2

    invoke-virtual {p0, v2}, LprotocolAnalysis/analysis/packetAnalytic;->getUnsignedFromByte(B)I

    move-result v2

    shl-int/lit8 v2, v2, 0x8

    const/16 v3, 0x8

    aget-byte v3, p1, v3

    invoke-virtual {p0, v3}, LprotocolAnalysis/analysis/packetAnalytic;->getUnsignedFromByte(B)I

    move-result v3

    or-int/2addr v2, v3

    int-to-short v2, v2

    aput-short v2, v0, v1

    const/4 v1, 0x3

    const/16 v2, 0x9

    aget-byte v2, p1, v2

    invoke-virtual {p0, v2}, LprotocolAnalysis/analysis/packetAnalytic;->getUnsignedFromByte(B)I

    move-result v2

    shl-int/lit8 v2, v2, 0x8

    const/16 v3, 0xa

    aget-byte v3, p1, v3

    invoke-virtual {p0, v3}, LprotocolAnalysis/analysis/packetAnalytic;->getUnsignedFromByte(B)I

    move-result v3

    or-int/2addr v2, v3

    int-to-short v2, v2

    aput-short v2, v0, v1

    return-object v0
.end method

.method public getMultiTouchValue([B)[S
    .locals 9
    .param p1    # [B

    const/4 v4, 0x6

    const/4 v8, 0x5

    const/4 v7, 0x4

    const/4 v6, 0x3

    const/4 v5, 0x2

    aget-byte v2, p1, v7

    invoke-virtual {p0, v2}, LprotocolAnalysis/analysis/packetAnalytic;->getUnsignedFromByte(B)I

    move-result v2

    shl-int/lit8 v2, v2, 0x8

    aget-byte v3, p1, v8

    invoke-virtual {p0, v3}, LprotocolAnalysis/analysis/packetAnalytic;->getUnsignedFromByte(B)I

    move-result v3

    add-int/2addr v2, v3

    int-to-short v1, v2

    new-array v0, v4, [S

    if-ne v1, v5, :cond_0

    const/4 v2, 0x0

    aget-byte v3, p1, v4

    invoke-virtual {p0, v3}, LprotocolAnalysis/analysis/packetAnalytic;->getUnsignedFromByte(B)I

    move-result v3

    shl-int/lit8 v3, v3, 0x8

    const/4 v4, 0x7

    aget-byte v4, p1, v4

    invoke-virtual {p0, v4}, LprotocolAnalysis/analysis/packetAnalytic;->getUnsignedFromByte(B)I

    move-result v4

    add-int/2addr v3, v4

    int-to-short v3, v3

    aput-short v3, v0, v2

    const/4 v2, 0x1

    const/16 v3, 0x8

    aget-byte v3, p1, v3

    invoke-virtual {p0, v3}, LprotocolAnalysis/analysis/packetAnalytic;->getUnsignedFromByte(B)I

    move-result v3

    shl-int/lit8 v3, v3, 0x8

    const/16 v4, 0x9

    aget-byte v4, p1, v4

    invoke-virtual {p0, v4}, LprotocolAnalysis/analysis/packetAnalytic;->getUnsignedFromByte(B)I

    move-result v4

    add-int/2addr v3, v4

    int-to-short v3, v3

    aput-short v3, v0, v2

    const/16 v2, 0xa

    aget-byte v2, p1, v2

    invoke-virtual {p0, v2}, LprotocolAnalysis/analysis/packetAnalytic;->getUnsignedFromByte(B)I

    move-result v2

    shl-int/lit8 v2, v2, 0x8

    const/16 v3, 0xb

    aget-byte v3, p1, v3

    invoke-virtual {p0, v3}, LprotocolAnalysis/analysis/packetAnalytic;->getUnsignedFromByte(B)I

    move-result v3

    or-int/2addr v2, v3

    int-to-short v2, v2

    aput-short v2, v0, v5

    const/16 v2, 0xc

    aget-byte v2, p1, v2

    invoke-virtual {p0, v2}, LprotocolAnalysis/analysis/packetAnalytic;->getUnsignedFromByte(B)I

    move-result v2

    shl-int/lit8 v2, v2, 0x8

    const/16 v3, 0xd

    aget-byte v3, p1, v3

    invoke-virtual {p0, v3}, LprotocolAnalysis/analysis/packetAnalytic;->getUnsignedFromByte(B)I

    move-result v3

    or-int/2addr v2, v3

    int-to-short v2, v2

    aput-short v2, v0, v6

    const/16 v2, 0x2710

    aput-short v2, v0, v7

    const/16 v2, 0x2710

    aput-short v2, v0, v8

    :goto_0
    return-object v0

    :cond_0
    if-ne v1, v6, :cond_1

    const/4 v2, 0x0

    aget-byte v3, p1, v4

    invoke-virtual {p0, v3}, LprotocolAnalysis/analysis/packetAnalytic;->getUnsignedFromByte(B)I

    move-result v3

    shl-int/lit8 v3, v3, 0x8

    const/4 v4, 0x7

    aget-byte v4, p1, v4

    invoke-virtual {p0, v4}, LprotocolAnalysis/analysis/packetAnalytic;->getUnsignedFromByte(B)I

    move-result v4

    add-int/2addr v3, v4

    int-to-short v3, v3

    aput-short v3, v0, v2

    const/4 v2, 0x1

    const/16 v3, 0x8

    aget-byte v3, p1, v3

    invoke-virtual {p0, v3}, LprotocolAnalysis/analysis/packetAnalytic;->getUnsignedFromByte(B)I

    move-result v3

    shl-int/lit8 v3, v3, 0x8

    const/16 v4, 0x9

    aget-byte v4, p1, v4

    invoke-virtual {p0, v4}, LprotocolAnalysis/analysis/packetAnalytic;->getUnsignedFromByte(B)I

    move-result v4

    add-int/2addr v3, v4

    int-to-short v3, v3

    aput-short v3, v0, v2

    const/16 v2, 0xa

    aget-byte v2, p1, v2

    invoke-virtual {p0, v2}, LprotocolAnalysis/analysis/packetAnalytic;->getUnsignedFromByte(B)I

    move-result v2

    shl-int/lit8 v2, v2, 0x8

    const/16 v3, 0xb

    aget-byte v3, p1, v3

    invoke-virtual {p0, v3}, LprotocolAnalysis/analysis/packetAnalytic;->getUnsignedFromByte(B)I

    move-result v3

    or-int/2addr v2, v3

    int-to-short v2, v2

    aput-short v2, v0, v5

    const/16 v2, 0xc

    aget-byte v2, p1, v2

    invoke-virtual {p0, v2}, LprotocolAnalysis/analysis/packetAnalytic;->getUnsignedFromByte(B)I

    move-result v2

    shl-int/lit8 v2, v2, 0x8

    const/16 v3, 0xd

    aget-byte v3, p1, v3

    invoke-virtual {p0, v3}, LprotocolAnalysis/analysis/packetAnalytic;->getUnsignedFromByte(B)I

    move-result v3

    or-int/2addr v2, v3

    int-to-short v2, v2

    aput-short v2, v0, v6

    const/16 v2, 0xe

    aget-byte v2, p1, v2

    invoke-virtual {p0, v2}, LprotocolAnalysis/analysis/packetAnalytic;->getUnsignedFromByte(B)I

    move-result v2

    shl-int/lit8 v2, v2, 0x8

    const/16 v3, 0xf

    aget-byte v3, p1, v3

    invoke-virtual {p0, v3}, LprotocolAnalysis/analysis/packetAnalytic;->getUnsignedFromByte(B)I

    move-result v3

    or-int/2addr v2, v3

    int-to-short v2, v2

    aput-short v2, v0, v7

    const/16 v2, 0x10

    aget-byte v2, p1, v2

    invoke-virtual {p0, v2}, LprotocolAnalysis/analysis/packetAnalytic;->getUnsignedFromByte(B)I

    move-result v2

    shl-int/lit8 v2, v2, 0x8

    const/16 v3, 0x11

    aget-byte v3, p1, v3

    invoke-virtual {p0, v3}, LprotocolAnalysis/analysis/packetAnalytic;->getUnsignedFromByte(B)I

    move-result v3

    or-int/2addr v2, v3

    int-to-short v2, v2

    aput-short v2, v0, v8

    goto/16 :goto_0

    :cond_1
    const/4 v0, 0x0

    check-cast v0, [S

    goto/16 :goto_0
.end method

.method public getPacketCmd([B)I
    .locals 5
    .param p1    # [B

    const/4 v1, 0x0

    sget-object v2, LprotocolAnalysis/analysis/packetAnalytic;->MDTAG:Ljava/lang/String;

    new-instance v3, Ljava/lang/StringBuilder;

    const-string v4, "get cmd exception:packet string:"

    invoke-direct {v3, v4}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    array-length v4, p1

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v2, v3}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    const/4 v2, 0x2

    :try_start_0
    aget-byte v2, p1, v2

    invoke-virtual {p0, v2}, LprotocolAnalysis/analysis/packetAnalytic;->getUnsignedFromByte(B)I

    move-result v2

    shl-int/lit8 v2, v2, 0x8

    const/4 v3, 0x3

    aget-byte v3, p1, v3

    invoke-virtual {p0, v3}, LprotocolAnalysis/analysis/packetAnalytic;->getUnsignedFromByte(B)I

    move-result v3

    or-int v1, v2, v3

    sget-object v2, LprotocolAnalysis/analysis/packetAnalytic;->MDTAG:Ljava/lang/String;

    new-instance v3, Ljava/lang/StringBuilder;

    const-string v4, "packet get cmd :0x"

    invoke-direct {v3, v4}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v3, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v2, v3}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    sget-object v2, LprotocolAnalysis/analysis/packetAnalytic;->MDTAG:Ljava/lang/String;

    new-instance v3, Ljava/lang/StringBuilder;

    const-string v4, "packet get cmd :0x"

    invoke-direct {v3, v4}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-static {v1}, Ljava/lang/Integer;->toHexString(I)Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v2, v3}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    :goto_0
    return v1

    :catch_0
    move-exception v0

    sget-object v2, LprotocolAnalysis/analysis/packetAnalytic;->MDTAG:Ljava/lang/String;

    new-instance v3, Ljava/lang/StringBuilder;

    const-string v4, "get cmd exception:packet string:"

    invoke-direct {v3, v4}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v3, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v2, v3}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_0
.end method

.method public getPacketDeviceIp()Ljava/lang/String;
    .locals 2

    const/4 v0, 0x0

    const/16 v1, 0x18

    return-object v0
.end method

.method public getPacketFileName([B)Ljava/lang/String;
    .locals 7
    .param p1    # [B

    const/4 v2, 0x0

    :try_start_0
    invoke-virtual {p0, p1}, LprotocolAnalysis/analysis/packetAnalytic;->getPacketCmd([B)I

    move-result v4

    const/16 v5, 0x2005

    if-ne v4, v5, :cond_0

    invoke-virtual {p0, p1}, LprotocolAnalysis/analysis/packetAnalytic;->getPacketLength([B)I

    move-result v4

    add-int/lit8 v1, v4, -0x2f

    new-instance v3, Ljava/lang/String;

    const/16 v4, 0x30

    const-string v5, "UTF-8"

    invoke-direct {v3, p1, v4, v1, v5}, Ljava/lang/String;-><init>([BIILjava/lang/String;)V
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    :try_start_1
    const-string v4, "sss"

    new-instance v5, Ljava/lang/StringBuilder;

    const-string v6, "getPacketFileName = "

    invoke-direct {v5, v6}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v5, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    invoke-static {v4, v5}, Landroid/util/Log;->v(Ljava/lang/String;Ljava/lang/String;)I
    :try_end_1
    .catch Ljava/lang/Exception; {:try_start_1 .. :try_end_1} :catch_1

    move-object v2, v3

    :goto_0
    :try_start_2
    sget-object v4, LprotocolAnalysis/analysis/packetAnalytic;->MDTAG:Ljava/lang/String;

    new-instance v5, Ljava/lang/StringBuilder;

    const-string v6, "packe get filename :"

    invoke-direct {v5, v6}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v5, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    invoke-static {v4, v5}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    :goto_1
    return-object v2

    :cond_0
    invoke-virtual {p0, p1}, LprotocolAnalysis/analysis/packetAnalytic;->getPacketLength([B)I

    move-result v4

    add-int/lit8 v1, v4, -0x2b

    new-instance v3, Ljava/lang/String;

    const/16 v4, 0x2c

    const-string v5, "UTF-8"

    invoke-direct {v3, p1, v4, v1, v5}, Ljava/lang/String;-><init>([BIILjava/lang/String;)V
    :try_end_2
    .catch Ljava/lang/Exception; {:try_start_2 .. :try_end_2} :catch_0

    move-object v2, v3

    goto :goto_0

    :catch_0
    move-exception v0

    :goto_2
    sget-object v4, LprotocolAnalysis/analysis/packetAnalytic;->MDTAG:Ljava/lang/String;

    new-instance v5, Ljava/lang/StringBuilder;

    const-string v6, "get filename exception:packet:"

    invoke-direct {v5, v6}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v5, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    invoke-static {v4, v5}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    const-string v2, ""

    goto :goto_1

    :catch_1
    move-exception v0

    move-object v2, v3

    goto :goto_2
.end method

.method public getPacketFileSize([B)I
    .locals 5
    .param p1    # [B

    const/4 v1, 0x0

    const/4 v2, 0x4

    :try_start_0
    aget-byte v2, p1, v2

    invoke-virtual {p0, v2}, LprotocolAnalysis/analysis/packetAnalytic;->getUnsignedFromByte(B)I

    move-result v2

    shl-int/lit8 v2, v2, 0x20

    const/4 v3, 0x5

    aget-byte v3, p1, v3

    invoke-virtual {p0, v3}, LprotocolAnalysis/analysis/packetAnalytic;->getUnsignedFromByte(B)I

    move-result v3

    shl-int/lit8 v3, v3, 0x10

    or-int/2addr v2, v3

    const/4 v3, 0x6

    aget-byte v3, p1, v3

    invoke-virtual {p0, v3}, LprotocolAnalysis/analysis/packetAnalytic;->getUnsignedFromByte(B)I

    move-result v3

    shl-int/lit8 v3, v3, 0x8

    or-int/2addr v2, v3

    const/4 v3, 0x7

    aget-byte v3, p1, v3

    invoke-virtual {p0, v3}, LprotocolAnalysis/analysis/packetAnalytic;->getUnsignedFromByte(B)I

    move-result v3

    or-int v1, v2, v3

    sget-object v2, LprotocolAnalysis/analysis/packetAnalytic;->MDTAG:Ljava/lang/String;

    new-instance v3, Ljava/lang/StringBuilder;

    const-string v4, "packet get filesize:0x"

    invoke-direct {v3, v4}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-static {v1}, Ljava/lang/Integer;->toHexString(I)Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v2, v3}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    :goto_0
    return v1

    :catch_0
    move-exception v0

    sget-object v2, LprotocolAnalysis/analysis/packetAnalytic;->MDTAG:Ljava/lang/String;

    new-instance v3, Ljava/lang/StringBuilder;

    const-string v4, "get len exception:packet string:"

    invoke-direct {v3, v4}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v3, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v2, v3}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_0
.end method

.method public getPacketFileTypeString([B)I
    .locals 6
    .param p1    # [B

    sget-object v3, LprotocolAnalysis/analysis/packetAnalytic$MEDIA_TYPE;->multiscreen_null:LprotocolAnalysis/analysis/packetAnalytic$MEDIA_TYPE;

    invoke-virtual {v3}, LprotocolAnalysis/analysis/packetAnalytic$MEDIA_TYPE;->ordinal()I

    move-result v1

    :try_start_0
    new-instance v2, Ljava/lang/String;

    invoke-virtual {p0, p1}, LprotocolAnalysis/analysis/packetAnalytic;->getPacketLength([B)I

    move-result v3

    add-int/lit8 v3, v3, 0x4

    add-int/lit8 v3, v3, -0x3

    const/4 v4, 0x3

    const-string v5, "UTF-8"

    invoke-direct {v2, p1, v3, v4, v5}, Ljava/lang/String;-><init>([BIILjava/lang/String;)V

    sget-object v3, LprotocolAnalysis/analysis/packetAnalytic;->MDTAG:Ljava/lang/String;

    new-instance v4, Ljava/lang/StringBuilder;

    const-string v5, "file type str1 ="

    invoke-direct {v4, v5}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v4, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-static {v3, v4}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    const-string v3, "&MP"

    invoke-virtual {v2, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-eqz v3, :cond_0

    sget-object v3, LprotocolAnalysis/analysis/packetAnalytic$MEDIA_TYPE;->multiscreen_photo:LprotocolAnalysis/analysis/packetAnalytic$MEDIA_TYPE;

    invoke-virtual {v3}, LprotocolAnalysis/analysis/packetAnalytic$MEDIA_TYPE;->ordinal()I
    :try_end_0
    .catch Ljava/io/UnsupportedEncodingException; {:try_start_0 .. :try_end_0} :catch_0

    move-result v1

    :goto_0
    sget-object v3, LprotocolAnalysis/analysis/packetAnalytic;->MDTAG:Ljava/lang/String;

    new-instance v4, Ljava/lang/StringBuilder;

    const-string v5, "file type = "

    invoke-direct {v4, v5}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v4, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-static {v3, v4}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    return v1

    :cond_0
    :try_start_1
    const-string v3, "&MA"

    invoke-virtual {v2, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-eqz v3, :cond_1

    sget-object v3, LprotocolAnalysis/analysis/packetAnalytic$MEDIA_TYPE;->multiscreen_music:LprotocolAnalysis/analysis/packetAnalytic$MEDIA_TYPE;

    invoke-virtual {v3}, LprotocolAnalysis/analysis/packetAnalytic$MEDIA_TYPE;->ordinal()I

    move-result v1

    goto :goto_0

    :cond_1
    const-string v3, "&MV"

    invoke-virtual {v2, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-eqz v3, :cond_2

    sget-object v3, LprotocolAnalysis/analysis/packetAnalytic$MEDIA_TYPE;->multiscreen_video:LprotocolAnalysis/analysis/packetAnalytic$MEDIA_TYPE;

    invoke-virtual {v3}, LprotocolAnalysis/analysis/packetAnalytic$MEDIA_TYPE;->ordinal()I

    move-result v1

    goto :goto_0

    :cond_2
    const-string v3, "&mb"

    invoke-virtual {v2, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-eqz v3, :cond_3

    sget-object v3, LprotocolAnalysis/analysis/packetAnalytic$MEDIA_TYPE;->multiscreen_bestv:LprotocolAnalysis/analysis/packetAnalytic$MEDIA_TYPE;

    invoke-virtual {v3}, LprotocolAnalysis/analysis/packetAnalytic$MEDIA_TYPE;->ordinal()I

    move-result v1

    goto :goto_0

    :cond_3
    sget-object v3, LprotocolAnalysis/analysis/packetAnalytic$MEDIA_TYPE;->multiscreen_null:LprotocolAnalysis/analysis/packetAnalytic$MEDIA_TYPE;

    invoke-virtual {v3}, LprotocolAnalysis/analysis/packetAnalytic$MEDIA_TYPE;->ordinal()I
    :try_end_1
    .catch Ljava/io/UnsupportedEncodingException; {:try_start_1 .. :try_end_1} :catch_0

    move-result v1

    goto :goto_0

    :catch_0
    move-exception v0

    invoke-virtual {v0}, Ljava/io/UnsupportedEncodingException;->printStackTrace()V

    goto :goto_0
.end method

.method public getPacketFunType([B)I
    .locals 5
    .param p1    # [B

    const/4 v1, 0x0

    const/4 v2, 0x6

    :try_start_0
    aget-byte v2, p1, v2

    invoke-virtual {p0, v2}, LprotocolAnalysis/analysis/packetAnalytic;->getUnsignedFromByte(B)I

    move-result v2

    shl-int/lit8 v2, v2, 0x8

    const/4 v3, 0x7

    aget-byte v3, p1, v3

    invoke-virtual {p0, v3}, LprotocolAnalysis/analysis/packetAnalytic;->getUnsignedFromByte(B)I

    move-result v3

    or-int v1, v2, v3

    sget-object v2, LprotocolAnalysis/analysis/packetAnalytic;->MDTAG:Ljava/lang/String;

    new-instance v3, Ljava/lang/StringBuilder;

    const-string v4, "packet get seek bar:0x"

    invoke-direct {v3, v4}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-static {v1}, Ljava/lang/Integer;->toHexString(I)Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v2, v3}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    :goto_0
    return v1

    :catch_0
    move-exception v0

    sget-object v2, LprotocolAnalysis/analysis/packetAnalytic;->MDTAG:Ljava/lang/String;

    new-instance v3, Ljava/lang/StringBuilder;

    const-string v4, "get len exception:packet string:"

    invoke-direct {v3, v4}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v3, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v2, v3}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_0
.end method

.method public getPacketHaveNextFlag([B)I
    .locals 5
    .param p1    # [B

    const/4 v1, 0x0

    const/16 v2, 0x8

    :try_start_0
    aget-byte v2, p1, v2

    invoke-virtual {p0, v2}, LprotocolAnalysis/analysis/packetAnalytic;->getUnsignedFromByte(B)I

    move-result v2

    shl-int/lit8 v2, v2, 0x20

    const/16 v3, 0x9

    aget-byte v3, p1, v3

    invoke-virtual {p0, v3}, LprotocolAnalysis/analysis/packetAnalytic;->getUnsignedFromByte(B)I

    move-result v3

    shl-int/lit8 v3, v3, 0x10

    or-int/2addr v2, v3

    const/16 v3, 0xa

    aget-byte v3, p1, v3

    invoke-virtual {p0, v3}, LprotocolAnalysis/analysis/packetAnalytic;->getUnsignedFromByte(B)I

    move-result v3

    shl-int/lit8 v3, v3, 0x8

    or-int/2addr v2, v3

    const/16 v3, 0xb

    aget-byte v3, p1, v3

    invoke-virtual {p0, v3}, LprotocolAnalysis/analysis/packetAnalytic;->getUnsignedFromByte(B)I

    move-result v3

    or-int v1, v2, v3

    sget-object v2, LprotocolAnalysis/analysis/packetAnalytic;->MDTAG:Ljava/lang/String;

    new-instance v3, Ljava/lang/StringBuilder;

    const-string v4, "packet get HaveNextFlag:0x"

    invoke-direct {v3, v4}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-static {v1}, Ljava/lang/Integer;->toHexString(I)Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v2, v3}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    :goto_0
    return v1

    :catch_0
    move-exception v0

    sget-object v2, LprotocolAnalysis/analysis/packetAnalytic;->MDTAG:Ljava/lang/String;

    new-instance v3, Ljava/lang/StringBuilder;

    const-string v4, "get len exception:HaveNextFlag:"

    invoke-direct {v3, v4}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v3, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v2, v3}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_0
.end method

.method public getPacketInputStringValue([B)Ljava/lang/String;
    .locals 8
    .param p1    # [B

    sget-object v5, LprotocolAnalysis/analysis/packetAnalytic;->MDTAG:Ljava/lang/String;

    const-string v6, "packe get inputStr ------"

    invoke-static {v5, v6}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    :try_start_0
    invoke-virtual {p0, p1}, LprotocolAnalysis/analysis/packetAnalytic;->getPacketLength([B)I

    move-result v3

    new-array v2, v3, [B

    const/4 v1, 0x0

    :goto_0
    array-length v5, v2

    if-lt v1, v5, :cond_0

    new-instance v4, Ljava/lang/String;

    const/4 v5, 0x0

    array-length v6, v2

    const-string v7, "UTF-8"

    invoke-direct {v4, v2, v5, v6, v7}, Ljava/lang/String;-><init>([BIILjava/lang/String;)V

    sget-object v5, LprotocolAnalysis/analysis/packetAnalytic;->MDTAG:Ljava/lang/String;

    new-instance v6, Ljava/lang/StringBuilder;

    const-string v7, "packe get inputStr :"

    invoke-direct {v6, v7}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v6, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v6}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v6

    invoke-static {v5, v6}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    :goto_1
    return-object v4

    :cond_0
    add-int/lit8 v5, v1, 0x4

    aget-byte v5, p1, v5

    aput-byte v5, v2, v1
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    add-int/lit8 v1, v1, 0x1

    goto :goto_0

    :catch_0
    move-exception v0

    sget-object v5, LprotocolAnalysis/analysis/packetAnalytic;->MDTAG:Ljava/lang/String;

    new-instance v6, Ljava/lang/StringBuilder;

    const-string v7, "get inputStr exception:packet:"

    invoke-direct {v6, v7}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v6, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v6}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v6

    invoke-static {v5, v6}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    const-string v4, ""

    goto :goto_1
.end method

.method public getPacketLength([B)I
    .locals 5
    .param p1    # [B

    const/4 v1, 0x0

    :try_start_0
    iget-object v2, p0, LprotocolAnalysis/analysis/packetAnalytic;->headLen:[B

    const/4 v3, 0x0

    const/4 v4, 0x0

    aget-byte v4, p1, v4

    aput-byte v4, v2, v3

    iget-object v2, p0, LprotocolAnalysis/analysis/packetAnalytic;->headLen:[B

    const/4 v3, 0x1

    const/4 v4, 0x1

    aget-byte v4, p1, v4

    aput-byte v4, v2, v3

    iget-object v2, p0, LprotocolAnalysis/analysis/packetAnalytic;->headLen:[B

    const/4 v3, 0x0

    invoke-static {v2, v3}, Lcom/konka/IntelligentControl/ioop/comClass/Protocol;->byteToShort([BZ)S

    move-result v1

    sget-object v2, LprotocolAnalysis/analysis/packetAnalytic;->MDTAG:Ljava/lang/String;

    new-instance v3, Ljava/lang/StringBuilder;

    const-string v4, "packet get len :0x"

    invoke-direct {v3, v4}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-static {v1}, Ljava/lang/Integer;->toHexString(I)Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v2, v3}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    :goto_0
    return v1

    :catch_0
    move-exception v0

    sget-object v2, LprotocolAnalysis/analysis/packetAnalytic;->MDTAG:Ljava/lang/String;

    new-instance v3, Ljava/lang/StringBuilder;

    const-string v4, "get cmd exception:packet string:"

    invoke-direct {v3, v4}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v3, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v2, v3}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_0
.end method

.method public getPacketOptionSystemType([B)I
    .locals 5
    .param p1    # [B

    const/4 v1, 0x0

    const/4 v2, 0x4

    :try_start_0
    aget-byte v2, p1, v2

    invoke-virtual {p0, v2}, LprotocolAnalysis/analysis/packetAnalytic;->getUnsignedFromByte(B)I

    move-result v2

    shl-int/lit8 v2, v2, 0x20

    const/4 v3, 0x5

    aget-byte v3, p1, v3

    invoke-virtual {p0, v3}, LprotocolAnalysis/analysis/packetAnalytic;->getUnsignedFromByte(B)I

    move-result v3

    shl-int/lit8 v3, v3, 0x10

    or-int v1, v2, v3

    sget-object v2, LprotocolAnalysis/analysis/packetAnalytic;->MDTAG:Ljava/lang/String;

    new-instance v3, Ljava/lang/StringBuilder;

    const-string v4, "packet get seek bar:0x"

    invoke-direct {v3, v4}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-static {v1}, Ljava/lang/Integer;->toHexString(I)Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v2, v3}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    :goto_0
    return v1

    :catch_0
    move-exception v0

    sget-object v2, LprotocolAnalysis/analysis/packetAnalytic;->MDTAG:Ljava/lang/String;

    new-instance v3, Ljava/lang/StringBuilder;

    const-string v4, "get len exception:packet string:"

    invoke-direct {v3, v4}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v3, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v2, v3}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_0
.end method

.method public getPacketResumePos([B)I
    .locals 5
    .param p1    # [B

    const/4 v1, 0x0

    const/16 v2, 0xc

    :try_start_0
    aget-byte v2, p1, v2

    invoke-virtual {p0, v2}, LprotocolAnalysis/analysis/packetAnalytic;->getUnsignedFromByte(B)I

    move-result v2

    shl-int/lit8 v2, v2, 0x20

    const/16 v3, 0xd

    aget-byte v3, p1, v3

    invoke-virtual {p0, v3}, LprotocolAnalysis/analysis/packetAnalytic;->getUnsignedFromByte(B)I

    move-result v3

    shl-int/lit8 v3, v3, 0x10

    or-int/2addr v2, v3

    const/16 v3, 0xe

    aget-byte v3, p1, v3

    invoke-virtual {p0, v3}, LprotocolAnalysis/analysis/packetAnalytic;->getUnsignedFromByte(B)I

    move-result v3

    shl-int/lit8 v3, v3, 0x8

    or-int/2addr v2, v3

    const/16 v3, 0xf

    aget-byte v3, p1, v3

    invoke-virtual {p0, v3}, LprotocolAnalysis/analysis/packetAnalytic;->getUnsignedFromByte(B)I

    move-result v3

    or-int v1, v2, v3

    sget-object v2, LprotocolAnalysis/analysis/packetAnalytic;->MDTAG:Ljava/lang/String;

    new-instance v3, Ljava/lang/StringBuilder;

    const-string v4, "packet get ResumePos:0x"

    invoke-direct {v3, v4}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-static {v1}, Ljava/lang/Integer;->toHexString(I)Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v2, v3}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    :goto_0
    return v1

    :catch_0
    move-exception v0

    sget-object v2, LprotocolAnalysis/analysis/packetAnalytic;->MDTAG:Ljava/lang/String;

    new-instance v3, Ljava/lang/StringBuilder;

    const-string v4, "get len exception:ResumePos:"

    invoke-direct {v3, v4}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v3, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v2, v3}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_0
.end method

.method public getPacketSeekBarPos([B)I
    .locals 5
    .param p1    # [B

    const/4 v1, 0x0

    const/4 v2, 0x4

    :try_start_0
    aget-byte v2, p1, v2

    invoke-virtual {p0, v2}, LprotocolAnalysis/analysis/packetAnalytic;->getUnsignedFromByte(B)I

    move-result v2

    shl-int/lit8 v2, v2, 0x20

    const/4 v3, 0x5

    aget-byte v3, p1, v3

    invoke-virtual {p0, v3}, LprotocolAnalysis/analysis/packetAnalytic;->getUnsignedFromByte(B)I

    move-result v3

    shl-int/lit8 v3, v3, 0x10

    add-int/2addr v2, v3

    const/4 v3, 0x6

    aget-byte v3, p1, v3

    invoke-virtual {p0, v3}, LprotocolAnalysis/analysis/packetAnalytic;->getUnsignedFromByte(B)I

    move-result v3

    shl-int/lit8 v3, v3, 0x8

    add-int/2addr v2, v3

    const/4 v3, 0x7

    aget-byte v3, p1, v3

    invoke-virtual {p0, v3}, LprotocolAnalysis/analysis/packetAnalytic;->getUnsignedFromByte(B)I

    move-result v3

    add-int v1, v2, v3

    sget-object v2, LprotocolAnalysis/analysis/packetAnalytic;->MDTAG:Ljava/lang/String;

    new-instance v3, Ljava/lang/StringBuilder;

    const-string v4, "packet get seek bar:0x"

    invoke-direct {v3, v4}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-static {v1}, Ljava/lang/Integer;->toHexString(I)Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v2, v3}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    :goto_0
    return v1

    :catch_0
    move-exception v0

    sget-object v2, LprotocolAnalysis/analysis/packetAnalytic;->MDTAG:Ljava/lang/String;

    new-instance v3, Ljava/lang/StringBuilder;

    const-string v4, "get len exception:packet string:"

    invoke-direct {v3, v4}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v3, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v2, v3}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_0
.end method

.method public getPacketWard([B)I
    .locals 6
    .param p1    # [B

    array-length v1, p1

    const/4 v2, 0x0

    add-int/lit8 v3, v1, -0x4

    :try_start_0
    aget-byte v3, p1, v3

    invoke-virtual {p0, v3}, LprotocolAnalysis/analysis/packetAnalytic;->getUnsignedFromByte(B)I

    move-result v3

    shl-int/lit8 v3, v3, 0x20

    add-int/lit8 v4, v1, -0x3

    aget-byte v4, p1, v4

    invoke-virtual {p0, v4}, LprotocolAnalysis/analysis/packetAnalytic;->getUnsignedFromByte(B)I

    move-result v4

    shl-int/lit8 v4, v4, 0x10

    or-int/2addr v3, v4

    add-int/lit8 v4, v1, -0x2

    aget-byte v4, p1, v4

    invoke-virtual {p0, v4}, LprotocolAnalysis/analysis/packetAnalytic;->getUnsignedFromByte(B)I

    move-result v4

    shl-int/lit8 v4, v4, 0x8

    or-int/2addr v3, v4

    add-int/lit8 v4, v1, -0x1

    aget-byte v4, p1, v4

    invoke-virtual {p0, v4}, LprotocolAnalysis/analysis/packetAnalytic;->getUnsignedFromByte(B)I
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    move-result v4

    or-int v2, v3, v4

    :goto_0
    return v2

    :catch_0
    move-exception v0

    const-string v3, "weikan"

    new-instance v4, Ljava/lang/StringBuilder;

    const-string v5, "length = "

    invoke-direct {v4, v5}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v4, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-static {v3, v4}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_0
.end method

.method public getPacketWeiKanType([B)I
    .locals 4
    .param p1    # [B

    const/4 v0, 0x0

    const/4 v1, 0x4

    :try_start_0
    aget-byte v1, p1, v1

    invoke-virtual {p0, v1}, LprotocolAnalysis/analysis/packetAnalytic;->getUnsignedFromByte(B)I

    move-result v1

    shl-int/lit8 v1, v1, 0x8

    const/4 v2, 0x5

    aget-byte v2, p1, v2

    invoke-virtual {p0, v2}, LprotocolAnalysis/analysis/packetAnalytic;->getUnsignedFromByte(B)I
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    move-result v2

    or-int v0, v1, v2

    :goto_0
    const-string v1, "weikan"

    new-instance v2, Ljava/lang/StringBuilder;

    const-string v3, "mWeiKanType:"

    invoke-direct {v2, v3}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    return v0

    :catch_0
    move-exception v1

    goto :goto_0
.end method

.method public getPacketmWeiKanURLType([B)I
    .locals 4
    .param p1    # [B

    const/4 v0, 0x0

    const/4 v1, 0x6

    :try_start_0
    aget-byte v1, p1, v1

    invoke-virtual {p0, v1}, LprotocolAnalysis/analysis/packetAnalytic;->getUnsignedFromByte(B)I

    move-result v1

    shl-int/lit8 v1, v1, 0x8

    const/4 v2, 0x7

    aget-byte v2, p1, v2

    invoke-virtual {p0, v2}, LprotocolAnalysis/analysis/packetAnalytic;->getUnsignedFromByte(B)I
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    move-result v2

    or-int v0, v1, v2

    :goto_0
    const-string v1, "weikan"

    new-instance v2, Ljava/lang/StringBuilder;

    const-string v3, "urlType:"

    invoke-direct {v2, v3}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    return v0

    :catch_0
    move-exception v1

    goto :goto_0
.end method

.method public getSensorValue([B)[S
    .locals 4
    .param p1    # [B

    const/4 v2, 0x4

    new-array v0, v2, [S

    const/4 v1, 0x0

    aget-byte v2, p1, v2

    invoke-virtual {p0, v2}, LprotocolAnalysis/analysis/packetAnalytic;->getUnsignedFromByte(B)I

    move-result v2

    shl-int/lit8 v2, v2, 0x8

    const/4 v3, 0x5

    aget-byte v3, p1, v3

    invoke-virtual {p0, v3}, LprotocolAnalysis/analysis/packetAnalytic;->getUnsignedFromByte(B)I

    move-result v3

    or-int/2addr v2, v3

    int-to-short v2, v2

    aput-short v2, v0, v1

    const/4 v1, 0x1

    const/4 v2, 0x6

    aget-byte v2, p1, v2

    invoke-virtual {p0, v2}, LprotocolAnalysis/analysis/packetAnalytic;->getUnsignedFromByte(B)I

    move-result v2

    shl-int/lit8 v2, v2, 0x8

    const/4 v3, 0x7

    aget-byte v3, p1, v3

    invoke-virtual {p0, v3}, LprotocolAnalysis/analysis/packetAnalytic;->getUnsignedFromByte(B)I

    move-result v3

    or-int/2addr v2, v3

    int-to-short v2, v2

    aput-short v2, v0, v1

    const/4 v1, 0x2

    const/16 v2, 0x8

    aget-byte v2, p1, v2

    invoke-virtual {p0, v2}, LprotocolAnalysis/analysis/packetAnalytic;->getUnsignedFromByte(B)I

    move-result v2

    shl-int/lit8 v2, v2, 0x8

    const/16 v3, 0x9

    aget-byte v3, p1, v3

    invoke-virtual {p0, v3}, LprotocolAnalysis/analysis/packetAnalytic;->getUnsignedFromByte(B)I

    move-result v3

    or-int/2addr v2, v3

    int-to-short v2, v2

    aput-short v2, v0, v1

    const/4 v1, 0x3

    const/16 v2, 0xa

    aget-byte v2, p1, v2

    invoke-virtual {p0, v2}, LprotocolAnalysis/analysis/packetAnalytic;->getUnsignedFromByte(B)I

    move-result v2

    shl-int/lit8 v2, v2, 0x8

    const/16 v3, 0xb

    aget-byte v3, p1, v3

    invoke-virtual {p0, v3}, LprotocolAnalysis/analysis/packetAnalytic;->getUnsignedFromByte(B)I

    move-result v3

    or-int/2addr v2, v3

    int-to-short v2, v2

    aput-short v2, v0, v1

    return-object v0
.end method

.method public getSingleTouchValue([B)[S
    .locals 8
    .param p1    # [B

    const/4 v7, 0x6

    const/4 v6, 0x5

    const/4 v5, 0x4

    const/16 v4, 0x2710

    new-array v0, v7, [S

    const/4 v1, 0x0

    aget-byte v2, p1, v5

    invoke-virtual {p0, v2}, LprotocolAnalysis/analysis/packetAnalytic;->getUnsignedFromByte(B)I

    move-result v2

    shl-int/lit8 v2, v2, 0x8

    aget-byte v3, p1, v6

    invoke-virtual {p0, v3}, LprotocolAnalysis/analysis/packetAnalytic;->getUnsignedFromByte(B)I

    move-result v3

    or-int/2addr v2, v3

    int-to-short v2, v2

    aput-short v2, v0, v1

    const/4 v1, 0x1

    aget-byte v2, p1, v7

    invoke-virtual {p0, v2}, LprotocolAnalysis/analysis/packetAnalytic;->getUnsignedFromByte(B)I

    move-result v2

    shl-int/lit8 v2, v2, 0x8

    const/4 v3, 0x7

    aget-byte v3, p1, v3

    invoke-virtual {p0, v3}, LprotocolAnalysis/analysis/packetAnalytic;->getUnsignedFromByte(B)I

    move-result v3

    or-int/2addr v2, v3

    int-to-short v2, v2

    aput-short v2, v0, v1

    const/4 v1, 0x2

    const/16 v2, 0x8

    aget-byte v2, p1, v2

    invoke-virtual {p0, v2}, LprotocolAnalysis/analysis/packetAnalytic;->getUnsignedFromByte(B)I

    move-result v2

    shl-int/lit8 v2, v2, 0x8

    const/16 v3, 0x9

    aget-byte v3, p1, v3

    invoke-virtual {p0, v3}, LprotocolAnalysis/analysis/packetAnalytic;->getUnsignedFromByte(B)I

    move-result v3

    or-int/2addr v2, v3

    int-to-short v2, v2

    aput-short v2, v0, v1

    const/4 v1, 0x3

    aput-short v4, v0, v1

    aput-short v4, v0, v5

    aput-short v4, v0, v6

    return-object v0
.end method

.method public getStatisticsValue([B)[S
    .locals 5
    .param p1    # [B

    const/4 v4, 0x1

    const/4 v3, 0x0

    new-array v0, v3, [S

    aget-byte v1, p1, v3

    invoke-virtual {p0, v1}, LprotocolAnalysis/analysis/packetAnalytic;->getUnsignedFromByte(B)I

    move-result v1

    shl-int/lit8 v1, v1, 0x8

    aget-byte v2, p1, v4

    invoke-virtual {p0, v2}, LprotocolAnalysis/analysis/packetAnalytic;->getUnsignedFromByte(B)I

    move-result v2

    or-int/2addr v1, v2

    int-to-short v1, v1

    aput-short v1, v0, v3

    const/4 v1, 0x2

    aget-byte v1, p1, v1

    invoke-virtual {p0, v1}, LprotocolAnalysis/analysis/packetAnalytic;->getUnsignedFromByte(B)I

    move-result v1

    shl-int/lit8 v1, v1, 0x8

    const/4 v2, 0x3

    aget-byte v2, p1, v2

    invoke-virtual {p0, v2}, LprotocolAnalysis/analysis/packetAnalytic;->getUnsignedFromByte(B)I

    move-result v2

    or-int/2addr v1, v2

    int-to-short v1, v1

    aput-short v1, v0, v4

    return-object v0
.end method

.method public getUnsignedFromByte(B)I
    .locals 4
    .param p1    # B

    const/4 v3, 0x1

    new-array v0, v3, [B

    const/4 v3, 0x0

    aput-byte p1, v0, v3

    new-instance v1, Ljava/io/ByteArrayInputStream;

    invoke-direct {v1, v0}, Ljava/io/ByteArrayInputStream;-><init>([B)V

    invoke-virtual {v1}, Ljava/io/ByteArrayInputStream;->read()I

    move-result v2

    return v2
.end method

.method public getWeiKanPacketURL([B)Ljava/lang/String;
    .locals 7
    .param p1    # [B

    const/4 v1, 0x0

    :try_start_0
    invoke-virtual {p0, p1}, LprotocolAnalysis/analysis/packetAnalytic;->getPacketCmd([B)I

    move-result v4

    const/16 v5, 0x2008

    if-ne v4, v5, :cond_0

    invoke-virtual {p0, p1}, LprotocolAnalysis/analysis/packetAnalytic;->getPacketLength([B)I

    move-result v4

    add-int/lit8 v3, v4, -0x4

    new-instance v2, Ljava/lang/String;

    const/16 v4, 0x8

    const-string v5, "UTF-8"

    invoke-direct {v2, p1, v4, v3, v5}, Ljava/lang/String;-><init>([BIILjava/lang/String;)V

    move-object v1, v2

    :cond_0
    const-string v4, "weikan"

    new-instance v5, Ljava/lang/StringBuilder;

    const-string v6, "weikanurl:"

    invoke-direct {v5, v6}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v1}, Ljava/lang/String;->toString()Ljava/lang/String;

    move-result-object v6

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    invoke-static {v4, v5}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    :goto_0
    return-object v1

    :catch_0
    move-exception v0

    const-string v4, "weikan"

    new-instance v5, Ljava/lang/StringBuilder;

    const-string v6, "get filename exception:packet:"

    invoke-direct {v5, v6}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v5, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    invoke-static {v4, v5}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    const-string v1, ""

    goto :goto_0
.end method
