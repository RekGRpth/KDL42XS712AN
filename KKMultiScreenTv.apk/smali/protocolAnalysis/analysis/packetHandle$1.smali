.class LprotocolAnalysis/analysis/packetHandle$1;
.super Ljava/lang/Object;
.source "packetHandle.java"

# interfaces
.implements Ljava/lang/Runnable;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = LprotocolAnalysis/analysis/packetHandle;->startPlayerResumePosFromMobile(Landroid/content/Context;[B)V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:LprotocolAnalysis/analysis/packetHandle;

.field private final synthetic val$context:Landroid/content/Context;

.field private final synthetic val$packet:[B


# direct methods
.method constructor <init>(LprotocolAnalysis/analysis/packetHandle;Landroid/content/Context;[B)V
    .locals 0

    iput-object p1, p0, LprotocolAnalysis/analysis/packetHandle$1;->this$0:LprotocolAnalysis/analysis/packetHandle;

    iput-object p2, p0, LprotocolAnalysis/analysis/packetHandle$1;->val$context:Landroid/content/Context;

    iput-object p3, p0, LprotocolAnalysis/analysis/packetHandle$1;->val$packet:[B

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public run()V
    .locals 4

    iget-object v1, p0, LprotocolAnalysis/analysis/packetHandle$1;->this$0:LprotocolAnalysis/analysis/packetHandle;

    # getter for: LprotocolAnalysis/analysis/packetHandle;->TAG:Ljava/lang/String;
    invoke-static {v1}, LprotocolAnalysis/analysis/packetHandle;->access$1(LprotocolAnalysis/analysis/packetHandle;)Ljava/lang/String;

    move-result-object v1

    new-instance v2, Ljava/lang/StringBuilder;

    iget-object v3, p0, LprotocolAnalysis/analysis/packetHandle$1;->this$0:LprotocolAnalysis/analysis/packetHandle;

    # getter for: LprotocolAnalysis/analysis/packetHandle;->TAG:Ljava/lang/String;
    invoke-static {v3}, LprotocolAnalysis/analysis/packetHandle;->access$1(LprotocolAnalysis/analysis/packetHandle;)Ljava/lang/String;

    move-result-object v3

    invoke-static {v3}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v3

    invoke-direct {v2, v3}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    const-string v3, " switchSuccess:"

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    sget-object v3, LprotocolAnalysis/analysis/packetHandle;->switchSuccess:Ljava/lang/Boolean;

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, ",dataservice.mediaType:"

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    iget-object v3, p0, LprotocolAnalysis/analysis/packetHandle$1;->this$0:LprotocolAnalysis/analysis/packetHandle;

    # getter for: LprotocolAnalysis/analysis/packetHandle;->dataservice:Lcom/konka/kkmultiscreen/DataService$BroadMsg;
    invoke-static {v3}, LprotocolAnalysis/analysis/packetHandle;->access$0(LprotocolAnalysis/analysis/packetHandle;)Lcom/konka/kkmultiscreen/DataService$BroadMsg;

    move-result-object v3

    iget v3, v3, Lcom/konka/kkmultiscreen/DataService$BroadMsg;->mediaType:I

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    iget-object v1, p0, LprotocolAnalysis/analysis/packetHandle$1;->this$0:LprotocolAnalysis/analysis/packetHandle;

    # getter for: LprotocolAnalysis/analysis/packetHandle;->dataservice:Lcom/konka/kkmultiscreen/DataService$BroadMsg;
    invoke-static {v1}, LprotocolAnalysis/analysis/packetHandle;->access$0(LprotocolAnalysis/analysis/packetHandle;)Lcom/konka/kkmultiscreen/DataService$BroadMsg;

    move-result-object v1

    iget v1, v1, Lcom/konka/kkmultiscreen/DataService$BroadMsg;->mediaType:I

    if-nez v1, :cond_1

    iget-object v1, p0, LprotocolAnalysis/analysis/packetHandle$1;->this$0:LprotocolAnalysis/analysis/packetHandle;

    iget-object v2, p0, LprotocolAnalysis/analysis/packetHandle$1;->val$context:Landroid/content/Context;

    iget-object v3, p0, LprotocolAnalysis/analysis/packetHandle$1;->val$packet:[B

    invoke-virtual {v1, v2, v3}, LprotocolAnalysis/analysis/packetHandle;->mediaShareStartPlayPos(Landroid/content/Context;[B)V

    :cond_0
    :goto_0
    return-void

    :cond_1
    iget-object v1, p0, LprotocolAnalysis/analysis/packetHandle$1;->this$0:LprotocolAnalysis/analysis/packetHandle;

    # getter for: LprotocolAnalysis/analysis/packetHandle;->dataservice:Lcom/konka/kkmultiscreen/DataService$BroadMsg;
    invoke-static {v1}, LprotocolAnalysis/analysis/packetHandle;->access$0(LprotocolAnalysis/analysis/packetHandle;)Lcom/konka/kkmultiscreen/DataService$BroadMsg;

    move-result-object v1

    iget v1, v1, Lcom/konka/kkmultiscreen/DataService$BroadMsg;->mediaType:I

    # getter for: LprotocolAnalysis/analysis/packetHandle;->packetAny:LprotocolAnalysis/analysis/packetAnalytic;
    invoke-static {}, LprotocolAnalysis/analysis/packetHandle;->access$2()LprotocolAnalysis/analysis/packetAnalytic;

    move-result-object v2

    iget-object v3, p0, LprotocolAnalysis/analysis/packetHandle$1;->val$packet:[B

    invoke-virtual {v2, v3}, LprotocolAnalysis/analysis/packetAnalytic;->getPacketFileTypeString([B)I

    move-result v2

    if-eq v1, v2, :cond_2

    sget-object v1, LprotocolAnalysis/analysis/packetHandle;->switchSuccess:Ljava/lang/Boolean;

    invoke-virtual {v1}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v1

    if-eqz v1, :cond_0

    :try_start_0
    iget-object v1, p0, LprotocolAnalysis/analysis/packetHandle$1;->this$0:LprotocolAnalysis/analysis/packetHandle;

    iget-object v2, p0, LprotocolAnalysis/analysis/packetHandle$1;->val$context:Landroid/content/Context;

    invoke-virtual {v1, v2}, LprotocolAnalysis/analysis/packetHandle;->switchMediaPlayer(Landroid/content/Context;)V

    const-wide/16 v1, 0x3e8

    invoke-static {v1, v2}, Ljava/lang/Thread;->sleep(J)V

    iget-object v1, p0, LprotocolAnalysis/analysis/packetHandle$1;->this$0:LprotocolAnalysis/analysis/packetHandle;

    iget-object v2, p0, LprotocolAnalysis/analysis/packetHandle$1;->val$context:Landroid/content/Context;

    iget-object v3, p0, LprotocolAnalysis/analysis/packetHandle$1;->val$packet:[B

    invoke-virtual {v1, v2, v3}, LprotocolAnalysis/analysis/packetHandle;->mediaShareStartPlayPos(Landroid/content/Context;[B)V
    :try_end_0
    .catch Ljava/lang/InterruptedException; {:try_start_0 .. :try_end_0} :catch_0

    goto :goto_0

    :catch_0
    move-exception v0

    invoke-virtual {v0}, Ljava/lang/InterruptedException;->printStackTrace()V

    goto :goto_0

    :cond_2
    iget-object v1, p0, LprotocolAnalysis/analysis/packetHandle$1;->this$0:LprotocolAnalysis/analysis/packetHandle;

    iget-object v2, p0, LprotocolAnalysis/analysis/packetHandle$1;->val$context:Landroid/content/Context;

    iget-object v3, p0, LprotocolAnalysis/analysis/packetHandle$1;->val$packet:[B

    invoke-virtual {v1, v2, v3}, LprotocolAnalysis/analysis/packetHandle;->sendMediaDataInfoToPlayerPos(Landroid/content/Context;[B)V

    goto :goto_0
.end method
