.class public final enum LprotocolAnalysis/analysis/MediaTypeEnum;
.super Ljava/lang/Enum;
.source "MediaTypeEnum.java"


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Enum",
        "<",
        "LprotocolAnalysis/analysis/MediaTypeEnum;",
        ">;"
    }
.end annotation


# static fields
.field private static final synthetic ENUM$VALUES:[LprotocolAnalysis/analysis/MediaTypeEnum;

.field public static final enum multiscreen_bestv:LprotocolAnalysis/analysis/MediaTypeEnum;

.field public static final enum multiscreen_music:LprotocolAnalysis/analysis/MediaTypeEnum;

.field public static final enum multiscreen_null:LprotocolAnalysis/analysis/MediaTypeEnum;

.field public static final enum multiscreen_num:LprotocolAnalysis/analysis/MediaTypeEnum;

.field public static final enum multiscreen_photo:LprotocolAnalysis/analysis/MediaTypeEnum;

.field public static final enum multiscreen_video:LprotocolAnalysis/analysis/MediaTypeEnum;

.field public static final enum multiscreen_weikan:LprotocolAnalysis/analysis/MediaTypeEnum;


# direct methods
.method static constructor <clinit>()V
    .locals 8

    const/4 v7, 0x4

    const/4 v6, 0x3

    const/4 v5, 0x2

    const/4 v4, 0x1

    const/4 v3, 0x0

    new-instance v0, LprotocolAnalysis/analysis/MediaTypeEnum;

    const-string v1, "multiscreen_null"

    invoke-direct {v0, v1, v3}, LprotocolAnalysis/analysis/MediaTypeEnum;-><init>(Ljava/lang/String;I)V

    sput-object v0, LprotocolAnalysis/analysis/MediaTypeEnum;->multiscreen_null:LprotocolAnalysis/analysis/MediaTypeEnum;

    new-instance v0, LprotocolAnalysis/analysis/MediaTypeEnum;

    const-string v1, "multiscreen_photo"

    invoke-direct {v0, v1, v4}, LprotocolAnalysis/analysis/MediaTypeEnum;-><init>(Ljava/lang/String;I)V

    sput-object v0, LprotocolAnalysis/analysis/MediaTypeEnum;->multiscreen_photo:LprotocolAnalysis/analysis/MediaTypeEnum;

    new-instance v0, LprotocolAnalysis/analysis/MediaTypeEnum;

    const-string v1, "multiscreen_music"

    invoke-direct {v0, v1, v5}, LprotocolAnalysis/analysis/MediaTypeEnum;-><init>(Ljava/lang/String;I)V

    sput-object v0, LprotocolAnalysis/analysis/MediaTypeEnum;->multiscreen_music:LprotocolAnalysis/analysis/MediaTypeEnum;

    new-instance v0, LprotocolAnalysis/analysis/MediaTypeEnum;

    const-string v1, "multiscreen_video"

    invoke-direct {v0, v1, v6}, LprotocolAnalysis/analysis/MediaTypeEnum;-><init>(Ljava/lang/String;I)V

    sput-object v0, LprotocolAnalysis/analysis/MediaTypeEnum;->multiscreen_video:LprotocolAnalysis/analysis/MediaTypeEnum;

    new-instance v0, LprotocolAnalysis/analysis/MediaTypeEnum;

    const-string v1, "multiscreen_bestv"

    invoke-direct {v0, v1, v7}, LprotocolAnalysis/analysis/MediaTypeEnum;-><init>(Ljava/lang/String;I)V

    sput-object v0, LprotocolAnalysis/analysis/MediaTypeEnum;->multiscreen_bestv:LprotocolAnalysis/analysis/MediaTypeEnum;

    new-instance v0, LprotocolAnalysis/analysis/MediaTypeEnum;

    const-string v1, "multiscreen_weikan"

    const/4 v2, 0x5

    invoke-direct {v0, v1, v2}, LprotocolAnalysis/analysis/MediaTypeEnum;-><init>(Ljava/lang/String;I)V

    sput-object v0, LprotocolAnalysis/analysis/MediaTypeEnum;->multiscreen_weikan:LprotocolAnalysis/analysis/MediaTypeEnum;

    new-instance v0, LprotocolAnalysis/analysis/MediaTypeEnum;

    const-string v1, "multiscreen_num"

    const/4 v2, 0x6

    invoke-direct {v0, v1, v2}, LprotocolAnalysis/analysis/MediaTypeEnum;-><init>(Ljava/lang/String;I)V

    sput-object v0, LprotocolAnalysis/analysis/MediaTypeEnum;->multiscreen_num:LprotocolAnalysis/analysis/MediaTypeEnum;

    const/4 v0, 0x7

    new-array v0, v0, [LprotocolAnalysis/analysis/MediaTypeEnum;

    sget-object v1, LprotocolAnalysis/analysis/MediaTypeEnum;->multiscreen_null:LprotocolAnalysis/analysis/MediaTypeEnum;

    aput-object v1, v0, v3

    sget-object v1, LprotocolAnalysis/analysis/MediaTypeEnum;->multiscreen_photo:LprotocolAnalysis/analysis/MediaTypeEnum;

    aput-object v1, v0, v4

    sget-object v1, LprotocolAnalysis/analysis/MediaTypeEnum;->multiscreen_music:LprotocolAnalysis/analysis/MediaTypeEnum;

    aput-object v1, v0, v5

    sget-object v1, LprotocolAnalysis/analysis/MediaTypeEnum;->multiscreen_video:LprotocolAnalysis/analysis/MediaTypeEnum;

    aput-object v1, v0, v6

    sget-object v1, LprotocolAnalysis/analysis/MediaTypeEnum;->multiscreen_bestv:LprotocolAnalysis/analysis/MediaTypeEnum;

    aput-object v1, v0, v7

    const/4 v1, 0x5

    sget-object v2, LprotocolAnalysis/analysis/MediaTypeEnum;->multiscreen_weikan:LprotocolAnalysis/analysis/MediaTypeEnum;

    aput-object v2, v0, v1

    const/4 v1, 0x6

    sget-object v2, LprotocolAnalysis/analysis/MediaTypeEnum;->multiscreen_num:LprotocolAnalysis/analysis/MediaTypeEnum;

    aput-object v2, v0, v1

    sput-object v0, LprotocolAnalysis/analysis/MediaTypeEnum;->ENUM$VALUES:[LprotocolAnalysis/analysis/MediaTypeEnum;

    return-void
.end method

.method private constructor <init>(Ljava/lang/String;I)V
    .locals 0

    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    return-void
.end method

.method public static valueOf(Ljava/lang/String;)LprotocolAnalysis/analysis/MediaTypeEnum;
    .locals 1

    const-class v0, LprotocolAnalysis/analysis/MediaTypeEnum;

    invoke-static {v0, p0}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;

    move-result-object v0

    check-cast v0, LprotocolAnalysis/analysis/MediaTypeEnum;

    return-object v0
.end method

.method public static values()[LprotocolAnalysis/analysis/MediaTypeEnum;
    .locals 4

    const/4 v3, 0x0

    sget-object v0, LprotocolAnalysis/analysis/MediaTypeEnum;->ENUM$VALUES:[LprotocolAnalysis/analysis/MediaTypeEnum;

    array-length v1, v0

    new-array v2, v1, [LprotocolAnalysis/analysis/MediaTypeEnum;

    invoke-static {v0, v3, v2, v3, v1}, Ljava/lang/System;->arraycopy(Ljava/lang/Object;ILjava/lang/Object;II)V

    return-object v2
.end method
