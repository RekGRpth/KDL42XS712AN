.class LprotocolAnalysis/analysis/packetHandle$DataShareStateReceiver;
.super Landroid/content/BroadcastReceiver;
.source "packetHandle.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = LprotocolAnalysis/analysis/packetHandle;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x2
    name = "DataShareStateReceiver"
.end annotation


# instance fields
.field final synthetic this$0:LprotocolAnalysis/analysis/packetHandle;


# direct methods
.method private constructor <init>(LprotocolAnalysis/analysis/packetHandle;)V
    .locals 0

    iput-object p1, p0, LprotocolAnalysis/analysis/packetHandle$DataShareStateReceiver;->this$0:LprotocolAnalysis/analysis/packetHandle;

    invoke-direct {p0}, Landroid/content/BroadcastReceiver;-><init>()V

    return-void
.end method

.method synthetic constructor <init>(LprotocolAnalysis/analysis/packetHandle;LprotocolAnalysis/analysis/packetHandle$DataShareStateReceiver;)V
    .locals 0

    invoke-direct {p0, p1}, LprotocolAnalysis/analysis/packetHandle$DataShareStateReceiver;-><init>(LprotocolAnalysis/analysis/packetHandle;)V

    return-void
.end method


# virtual methods
.method public onReceive(Landroid/content/Context;Landroid/content/Intent;)V
    .locals 9
    .param p1    # Landroid/content/Context;
    .param p2    # Landroid/content/Intent;

    const/4 v8, 0x0

    invoke-virtual {p2}, Landroid/content/Intent;->getExtras()Landroid/os/Bundle;

    move-result-object v0

    iget-object v5, p0, LprotocolAnalysis/analysis/packetHandle$DataShareStateReceiver;->this$0:LprotocolAnalysis/analysis/packetHandle;

    # getter for: LprotocolAnalysis/analysis/packetHandle;->dataservice:Lcom/konka/kkmultiscreen/DataService$BroadMsg;
    invoke-static {v5}, LprotocolAnalysis/analysis/packetHandle;->access$0(LprotocolAnalysis/analysis/packetHandle;)Lcom/konka/kkmultiscreen/DataService$BroadMsg;

    move-result-object v5

    const-string v6, "picdestroy"

    invoke-virtual {v0, v6, v8}, Landroid/os/Bundle;->getInt(Ljava/lang/String;I)I

    move-result v6

    iput v6, v5, Lcom/konka/kkmultiscreen/DataService$BroadMsg;->picPlayerCreat:I

    iget-object v5, p0, LprotocolAnalysis/analysis/packetHandle$DataShareStateReceiver;->this$0:LprotocolAnalysis/analysis/packetHandle;

    # getter for: LprotocolAnalysis/analysis/packetHandle;->dataservice:Lcom/konka/kkmultiscreen/DataService$BroadMsg;
    invoke-static {v5}, LprotocolAnalysis/analysis/packetHandle;->access$0(LprotocolAnalysis/analysis/packetHandle;)Lcom/konka/kkmultiscreen/DataService$BroadMsg;

    move-result-object v5

    const-string v6, "musicdestroy"

    invoke-virtual {v0, v6, v8}, Landroid/os/Bundle;->getInt(Ljava/lang/String;I)I

    move-result v6

    iput v6, v5, Lcom/konka/kkmultiscreen/DataService$BroadMsg;->musicPlayerCreat:I

    iget-object v5, p0, LprotocolAnalysis/analysis/packetHandle$DataShareStateReceiver;->this$0:LprotocolAnalysis/analysis/packetHandle;

    # getter for: LprotocolAnalysis/analysis/packetHandle;->dataservice:Lcom/konka/kkmultiscreen/DataService$BroadMsg;
    invoke-static {v5}, LprotocolAnalysis/analysis/packetHandle;->access$0(LprotocolAnalysis/analysis/packetHandle;)Lcom/konka/kkmultiscreen/DataService$BroadMsg;

    move-result-object v5

    const-string v6, "videodestroy"

    invoke-virtual {v0, v6, v8}, Landroid/os/Bundle;->getInt(Ljava/lang/String;I)I

    move-result v6

    iput v6, v5, Lcom/konka/kkmultiscreen/DataService$BroadMsg;->videoPlayerCreat:I

    const/4 v5, 0x1

    invoke-static {v5}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v5

    sput-object v5, LprotocolAnalysis/analysis/packetHandle;->switchSuccess:Ljava/lang/Boolean;

    const-string v4, "/data/data/com.konka.mediaSharePlayer/cache"

    new-instance v1, Ljava/io/File;

    invoke-direct {v1, v4}, Ljava/io/File;-><init>(Ljava/lang/String;)V

    invoke-virtual {v1}, Ljava/io/File;->listFiles()[Ljava/io/File;

    move-result-object v3

    if-eqz v3, :cond_0

    array-length v5, v3

    if-lez v5, :cond_0

    const/4 v2, 0x0

    :goto_0
    array-length v5, v3

    add-int/lit8 v5, v5, -0x1

    if-lt v2, v5, :cond_2

    :cond_0
    iget-object v5, p0, LprotocolAnalysis/analysis/packetHandle$DataShareStateReceiver;->this$0:LprotocolAnalysis/analysis/packetHandle;

    # getter for: LprotocolAnalysis/analysis/packetHandle;->TAG:Ljava/lang/String;
    invoke-static {v5}, LprotocolAnalysis/analysis/packetHandle;->access$1(LprotocolAnalysis/analysis/packetHandle;)Ljava/lang/String;

    move-result-object v5

    new-instance v6, Ljava/lang/StringBuilder;

    iget-object v7, p0, LprotocolAnalysis/analysis/packetHandle$DataShareStateReceiver;->this$0:LprotocolAnalysis/analysis/packetHandle;

    # getter for: LprotocolAnalysis/analysis/packetHandle;->TAG:Ljava/lang/String;
    invoke-static {v7}, LprotocolAnalysis/analysis/packetHandle;->access$1(LprotocolAnalysis/analysis/packetHandle;)Ljava/lang/String;

    move-result-object v7

    invoke-static {v7}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v7

    invoke-direct {v6, v7}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    const-string v7, "mediaType = "

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    iget-object v7, p0, LprotocolAnalysis/analysis/packetHandle$DataShareStateReceiver;->this$0:LprotocolAnalysis/analysis/packetHandle;

    # getter for: LprotocolAnalysis/analysis/packetHandle;->dataservice:Lcom/konka/kkmultiscreen/DataService$BroadMsg;
    invoke-static {v7}, LprotocolAnalysis/analysis/packetHandle;->access$0(LprotocolAnalysis/analysis/packetHandle;)Lcom/konka/kkmultiscreen/DataService$BroadMsg;

    move-result-object v7

    iget v7, v7, Lcom/konka/kkmultiscreen/DataService$BroadMsg;->mediaType:I

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v6}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v6

    invoke-static {v5, v6}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    iget-object v5, p0, LprotocolAnalysis/analysis/packetHandle$DataShareStateReceiver;->this$0:LprotocolAnalysis/analysis/packetHandle;

    # getter for: LprotocolAnalysis/analysis/packetHandle;->TAG:Ljava/lang/String;
    invoke-static {v5}, LprotocolAnalysis/analysis/packetHandle;->access$1(LprotocolAnalysis/analysis/packetHandle;)Ljava/lang/String;

    move-result-object v5

    new-instance v6, Ljava/lang/StringBuilder;

    const-string v7, "picPlayerCreat===="

    invoke-direct {v6, v7}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    iget-object v7, p0, LprotocolAnalysis/analysis/packetHandle$DataShareStateReceiver;->this$0:LprotocolAnalysis/analysis/packetHandle;

    # getter for: LprotocolAnalysis/analysis/packetHandle;->dataservice:Lcom/konka/kkmultiscreen/DataService$BroadMsg;
    invoke-static {v7}, LprotocolAnalysis/analysis/packetHandle;->access$0(LprotocolAnalysis/analysis/packetHandle;)Lcom/konka/kkmultiscreen/DataService$BroadMsg;

    move-result-object v7

    iget v7, v7, Lcom/konka/kkmultiscreen/DataService$BroadMsg;->picPlayerCreat:I

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v6

    const-string v7, "====musicPlayerCreat==="

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    iget-object v7, p0, LprotocolAnalysis/analysis/packetHandle$DataShareStateReceiver;->this$0:LprotocolAnalysis/analysis/packetHandle;

    # getter for: LprotocolAnalysis/analysis/packetHandle;->dataservice:Lcom/konka/kkmultiscreen/DataService$BroadMsg;
    invoke-static {v7}, LprotocolAnalysis/analysis/packetHandle;->access$0(LprotocolAnalysis/analysis/packetHandle;)Lcom/konka/kkmultiscreen/DataService$BroadMsg;

    move-result-object v7

    iget v7, v7, Lcom/konka/kkmultiscreen/DataService$BroadMsg;->musicPlayerCreat:I

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v6

    const-string v7, "====videoPlayerCreat==="

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    iget-object v7, p0, LprotocolAnalysis/analysis/packetHandle$DataShareStateReceiver;->this$0:LprotocolAnalysis/analysis/packetHandle;

    # getter for: LprotocolAnalysis/analysis/packetHandle;->dataservice:Lcom/konka/kkmultiscreen/DataService$BroadMsg;
    invoke-static {v7}, LprotocolAnalysis/analysis/packetHandle;->access$0(LprotocolAnalysis/analysis/packetHandle;)Lcom/konka/kkmultiscreen/DataService$BroadMsg;

    move-result-object v7

    iget v7, v7, Lcom/konka/kkmultiscreen/DataService$BroadMsg;->videoPlayerCreat:I

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v6}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v6

    invoke-static {v5, v6}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    iget-object v5, p0, LprotocolAnalysis/analysis/packetHandle$DataShareStateReceiver;->this$0:LprotocolAnalysis/analysis/packetHandle;

    # getter for: LprotocolAnalysis/analysis/packetHandle;->dataservice:Lcom/konka/kkmultiscreen/DataService$BroadMsg;
    invoke-static {v5}, LprotocolAnalysis/analysis/packetHandle;->access$0(LprotocolAnalysis/analysis/packetHandle;)Lcom/konka/kkmultiscreen/DataService$BroadMsg;

    move-result-object v5

    iget v5, v5, Lcom/konka/kkmultiscreen/DataService$BroadMsg;->picPlayerCreat:I

    iget-object v6, p0, LprotocolAnalysis/analysis/packetHandle$DataShareStateReceiver;->this$0:LprotocolAnalysis/analysis/packetHandle;

    # getter for: LprotocolAnalysis/analysis/packetHandle;->dataservice:Lcom/konka/kkmultiscreen/DataService$BroadMsg;
    invoke-static {v6}, LprotocolAnalysis/analysis/packetHandle;->access$0(LprotocolAnalysis/analysis/packetHandle;)Lcom/konka/kkmultiscreen/DataService$BroadMsg;

    move-result-object v6

    iget v6, v6, Lcom/konka/kkmultiscreen/DataService$BroadMsg;->musicPlayerCreat:I

    add-int/2addr v5, v6

    iget-object v6, p0, LprotocolAnalysis/analysis/packetHandle$DataShareStateReceiver;->this$0:LprotocolAnalysis/analysis/packetHandle;

    # getter for: LprotocolAnalysis/analysis/packetHandle;->dataservice:Lcom/konka/kkmultiscreen/DataService$BroadMsg;
    invoke-static {v6}, LprotocolAnalysis/analysis/packetHandle;->access$0(LprotocolAnalysis/analysis/packetHandle;)Lcom/konka/kkmultiscreen/DataService$BroadMsg;

    move-result-object v6

    iget v6, v6, Lcom/konka/kkmultiscreen/DataService$BroadMsg;->videoPlayerCreat:I

    add-int/2addr v5, v6

    iget-object v6, p0, LprotocolAnalysis/analysis/packetHandle$DataShareStateReceiver;->this$0:LprotocolAnalysis/analysis/packetHandle;

    # getter for: LprotocolAnalysis/analysis/packetHandle;->dataservice:Lcom/konka/kkmultiscreen/DataService$BroadMsg;
    invoke-static {v6}, LprotocolAnalysis/analysis/packetHandle;->access$0(LprotocolAnalysis/analysis/packetHandle;)Lcom/konka/kkmultiscreen/DataService$BroadMsg;

    move-result-object v6

    iget v6, v6, Lcom/konka/kkmultiscreen/DataService$BroadMsg;->bestvPlayerCreat:I

    add-int/2addr v5, v6

    if-nez v5, :cond_1

    iget-object v5, p0, LprotocolAnalysis/analysis/packetHandle$DataShareStateReceiver;->this$0:LprotocolAnalysis/analysis/packetHandle;

    # getter for: LprotocolAnalysis/analysis/packetHandle;->dataservice:Lcom/konka/kkmultiscreen/DataService$BroadMsg;
    invoke-static {v5}, LprotocolAnalysis/analysis/packetHandle;->access$0(LprotocolAnalysis/analysis/packetHandle;)Lcom/konka/kkmultiscreen/DataService$BroadMsg;

    move-result-object v5

    iput v8, v5, Lcom/konka/kkmultiscreen/DataService$BroadMsg;->mediaType:I

    :cond_1
    return-void

    :cond_2
    iget-object v5, p0, LprotocolAnalysis/analysis/packetHandle$DataShareStateReceiver;->this$0:LprotocolAnalysis/analysis/packetHandle;

    # getter for: LprotocolAnalysis/analysis/packetHandle;->TAG:Ljava/lang/String;
    invoke-static {v5}, LprotocolAnalysis/analysis/packetHandle;->access$1(LprotocolAnalysis/analysis/packetHandle;)Ljava/lang/String;

    move-result-object v5

    new-instance v6, Ljava/lang/StringBuilder;

    iget-object v7, p0, LprotocolAnalysis/analysis/packetHandle$DataShareStateReceiver;->this$0:LprotocolAnalysis/analysis/packetHandle;

    # getter for: LprotocolAnalysis/analysis/packetHandle;->TAG:Ljava/lang/String;
    invoke-static {v7}, LprotocolAnalysis/analysis/packetHandle;->access$1(LprotocolAnalysis/analysis/packetHandle;)Ljava/lang/String;

    move-result-object v7

    invoke-static {v7}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v7

    invoke-direct {v6, v7}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    const-string v7, "file name: "

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    aget-object v7, v3, v2

    invoke-virtual {v7}, Ljava/io/File;->getName()Ljava/lang/String;

    move-result-object v7

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v6}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v6

    invoke-static {v5, v6}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    aget-object v5, v3, v2

    invoke-virtual {v5}, Ljava/io/File;->delete()Z

    add-int/lit8 v2, v2, 0x1

    goto/16 :goto_0
.end method
