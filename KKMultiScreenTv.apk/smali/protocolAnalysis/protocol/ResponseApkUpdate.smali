.class public LprotocolAnalysis/protocol/ResponseApkUpdate;
.super LprotocolAnalysis/protocol/BaseProtocol;
.source "ResponseApkUpdate.java"


# static fields
.field public static final RESAPKUPDATE:I = 0x5002


# instance fields
.field private apkInfo:Ljava/lang/String;

.field public head:LprotocolAnalysis/protocol/NetHeader;


# direct methods
.method public constructor <init>()V
    .locals 0

    invoke-direct {p0}, LprotocolAnalysis/protocol/BaseProtocol;-><init>()V

    return-void
.end method


# virtual methods
.method public format([B)V
    .locals 5
    .param p1    # [B

    const/4 v3, 0x0

    invoke-static {p1, v3}, Ljava/util/Arrays;->fill([BB)V

    iget-object v2, p0, LprotocolAnalysis/protocol/ResponseApkUpdate;->head:LprotocolAnalysis/protocol/NetHeader;

    invoke-virtual {v2, p1, v3}, LprotocolAnalysis/protocol/NetHeader;->Format([BI)V

    :try_start_0
    iget-object v2, p0, LprotocolAnalysis/protocol/ResponseApkUpdate;->apkInfo:Ljava/lang/String;

    const-string v3, "UTF-8"

    invoke-virtual {v2, v3}, Ljava/lang/String;->getBytes(Ljava/lang/String;)[B

    move-result-object v0

    const/4 v2, 0x0

    iget-object v3, p0, LprotocolAnalysis/protocol/ResponseApkUpdate;->head:LprotocolAnalysis/protocol/NetHeader;

    invoke-virtual {v3}, LprotocolAnalysis/protocol/NetHeader;->sizeOf()I

    move-result v3

    array-length v4, v0

    invoke-static {v0, v2, p1, v3, v4}, Ljava/lang/System;->arraycopy(Ljava/lang/Object;ILjava/lang/Object;II)V
    :try_end_0
    .catch Ljava/io/UnsupportedEncodingException; {:try_start_0 .. :try_end_0} :catch_0

    :goto_0
    return-void

    :catch_0
    move-exception v1

    invoke-virtual {v1}, Ljava/io/UnsupportedEncodingException;->printStackTrace()V

    goto :goto_0
.end method

.method public printf([B)Ljava/lang/String;
    .locals 8
    .param p1    # [B

    const/4 v7, 0x2

    const/4 v6, 0x0

    iget-object v5, p0, LprotocolAnalysis/protocol/ResponseApkUpdate;->head:LprotocolAnalysis/protocol/NetHeader;

    invoke-virtual {v5, p1}, LprotocolAnalysis/protocol/NetHeader;->printf([B)Ljava/lang/String;

    move-result-object v0

    const/4 v5, 0x3

    new-array v4, v5, [B

    invoke-static {p1, v6, v4, v6, v7}, Ljava/lang/System;->arraycopy(Ljava/lang/Object;ILjava/lang/Object;II)V

    aput-byte v6, v4, v7

    invoke-static {v4}, LprotocolAnalysis/protocol/ResponseApkUpdate;->byteToShort([B)S

    move-result v3

    new-array v2, v3, [B

    iget-object v5, p0, LprotocolAnalysis/protocol/ResponseApkUpdate;->head:LprotocolAnalysis/protocol/NetHeader;

    invoke-virtual {v5}, LprotocolAnalysis/protocol/NetHeader;->sizeOf()I

    move-result v5

    invoke-static {p1, v5, v2, v6, v3}, Ljava/lang/System;->arraycopy(Ljava/lang/Object;ILjava/lang/Object;II)V

    new-instance v1, Ljava/lang/String;

    invoke-direct {v1, v2}, Ljava/lang/String;-><init>([B)V

    new-instance v5, Ljava/lang/StringBuilder;

    invoke-static {v0}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v6

    invoke-direct {v5, v6}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v5, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    return-object v5
.end method

.method public setFileListStr(Ljava/lang/String;)V
    .locals 3
    .param p1    # Ljava/lang/String;

    new-instance v0, LprotocolAnalysis/protocol/NetHeader;

    invoke-virtual {p1}, Ljava/lang/String;->length()I

    move-result v1

    int-to-short v1, v1

    const/16 v2, 0x5002

    invoke-direct {v0, v1, v2}, LprotocolAnalysis/protocol/NetHeader;-><init>(SS)V

    iput-object v0, p0, LprotocolAnalysis/protocol/ResponseApkUpdate;->head:LprotocolAnalysis/protocol/NetHeader;

    iput-object p1, p0, LprotocolAnalysis/protocol/ResponseApkUpdate;->apkInfo:Ljava/lang/String;

    return-void
.end method

.method public sizeOf()I
    .locals 5

    const/4 v1, 0x0

    :try_start_0
    iget-object v2, p0, LprotocolAnalysis/protocol/ResponseApkUpdate;->head:LprotocolAnalysis/protocol/NetHeader;

    invoke-virtual {v2}, LprotocolAnalysis/protocol/NetHeader;->sizeOf()I

    move-result v2

    iget-object v3, p0, LprotocolAnalysis/protocol/ResponseApkUpdate;->apkInfo:Ljava/lang/String;

    const-string v4, "UTF-8"

    invoke-virtual {v3, v4}, Ljava/lang/String;->getBytes(Ljava/lang/String;)[B

    move-result-object v3

    array-length v3, v3
    :try_end_0
    .catch Ljava/io/UnsupportedEncodingException; {:try_start_0 .. :try_end_0} :catch_0

    add-int v1, v2, v3

    :goto_0
    return v1

    :catch_0
    move-exception v0

    invoke-virtual {v0}, Ljava/io/UnsupportedEncodingException;->printStackTrace()V

    goto :goto_0
.end method
