.class public LprotocolAnalysis/protocol/InstallResponse;
.super LprotocolAnalysis/protocol/BaseProtocol;
.source "InstallResponse.java"


# static fields
.field public static final RESINSTALL:I = 0x500b


# instance fields
.field private apkName:Ljava/lang/String;

.field public head:LprotocolAnalysis/protocol/NetHeader;

.field private res:I


# direct methods
.method public constructor <init>()V
    .locals 0

    invoke-direct {p0}, LprotocolAnalysis/protocol/BaseProtocol;-><init>()V

    return-void
.end method


# virtual methods
.method public format([B)V
    .locals 5
    .param p1    # [B

    const/4 v3, 0x0

    invoke-static {p1, v3}, Ljava/util/Arrays;->fill([BB)V

    iget-object v2, p0, LprotocolAnalysis/protocol/InstallResponse;->head:LprotocolAnalysis/protocol/NetHeader;

    invoke-virtual {v2, p1, v3}, LprotocolAnalysis/protocol/NetHeader;->Format([BI)V

    iget v2, p0, LprotocolAnalysis/protocol/InstallResponse;->res:I

    iget-object v3, p0, LprotocolAnalysis/protocol/InstallResponse;->head:LprotocolAnalysis/protocol/NetHeader;

    invoke-virtual {v3}, LprotocolAnalysis/protocol/NetHeader;->sizeOf()I

    move-result v3

    invoke-static {v2, p1, v3}, LprotocolAnalysis/protocol/InstallResponse;->intToByte(I[BI)V

    :try_start_0
    iget-object v2, p0, LprotocolAnalysis/protocol/InstallResponse;->apkName:Ljava/lang/String;

    const-string v3, "UTF-8"

    invoke-virtual {v2, v3}, Ljava/lang/String;->getBytes(Ljava/lang/String;)[B

    move-result-object v0

    const/4 v2, 0x0

    iget-object v3, p0, LprotocolAnalysis/protocol/InstallResponse;->head:LprotocolAnalysis/protocol/NetHeader;

    invoke-virtual {v3}, LprotocolAnalysis/protocol/NetHeader;->sizeOf()I

    move-result v3

    add-int/lit8 v3, v3, 0x4

    array-length v4, v0

    invoke-static {v0, v2, p1, v3, v4}, Ljava/lang/System;->arraycopy(Ljava/lang/Object;ILjava/lang/Object;II)V
    :try_end_0
    .catch Ljava/io/UnsupportedEncodingException; {:try_start_0 .. :try_end_0} :catch_0

    :goto_0
    return-void

    :catch_0
    move-exception v1

    invoke-virtual {v1}, Ljava/io/UnsupportedEncodingException;->printStackTrace()V

    goto :goto_0
.end method

.method public printf([B)Ljava/lang/String;
    .locals 11
    .param p1    # [B

    const/4 v10, 0x4

    const/4 v8, 0x2

    const/4 v9, 0x0

    iget-object v7, p0, LprotocolAnalysis/protocol/InstallResponse;->head:LprotocolAnalysis/protocol/NetHeader;

    invoke-virtual {v7, p1}, LprotocolAnalysis/protocol/NetHeader;->printf([B)Ljava/lang/String;

    move-result-object v0

    const/4 v7, 0x3

    new-array v5, v7, [B

    invoke-static {p1, v9, v5, v9, v8}, Ljava/lang/System;->arraycopy(Ljava/lang/Object;ILjava/lang/Object;II)V

    aput-byte v9, v5, v8

    invoke-static {v5}, LprotocolAnalysis/protocol/InstallResponse;->byteToShort([B)S

    move-result v2

    const/4 v7, 0x5

    new-array v6, v7, [B

    iget-object v7, p0, LprotocolAnalysis/protocol/InstallResponse;->head:LprotocolAnalysis/protocol/NetHeader;

    invoke-virtual {v7}, LprotocolAnalysis/protocol/NetHeader;->sizeOf()I

    move-result v7

    invoke-static {p1, v7, v6, v9, v10}, Ljava/lang/System;->arraycopy(Ljava/lang/Object;ILjava/lang/Object;II)V

    aput-byte v9, v6, v10

    invoke-static {v6}, LprotocolAnalysis/protocol/InstallResponse;->byteToInt([B)I

    move-result v4

    new-array v1, v2, [B

    iget-object v7, p0, LprotocolAnalysis/protocol/InstallResponse;->head:LprotocolAnalysis/protocol/NetHeader;

    invoke-virtual {v7}, LprotocolAnalysis/protocol/NetHeader;->sizeOf()I

    move-result v7

    add-int/lit8 v7, v7, 0x4

    add-int/lit8 v8, v2, -0x4

    invoke-static {p1, v7, v1, v9, v8}, Ljava/lang/System;->arraycopy(Ljava/lang/Object;ILjava/lang/Object;II)V

    new-instance v3, Ljava/lang/String;

    invoke-direct {v3, v1}, Ljava/lang/String;-><init>([B)V

    new-instance v7, Ljava/lang/StringBuilder;

    invoke-static {v0}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v8

    invoke-direct {v7, v8}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v7, v4}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v7

    const-string v8, ","

    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    invoke-virtual {v7, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    invoke-virtual {v7}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v7

    return-object v7
.end method

.method public setData(ILjava/lang/String;)V
    .locals 3
    .param p1    # I
    .param p2    # Ljava/lang/String;

    new-instance v0, LprotocolAnalysis/protocol/NetHeader;

    invoke-virtual {p2}, Ljava/lang/String;->length()I

    move-result v1

    add-int/lit8 v1, v1, 0x4

    int-to-short v1, v1

    const/16 v2, 0x500b

    invoke-direct {v0, v1, v2}, LprotocolAnalysis/protocol/NetHeader;-><init>(SS)V

    iput-object v0, p0, LprotocolAnalysis/protocol/InstallResponse;->head:LprotocolAnalysis/protocol/NetHeader;

    iput-object p2, p0, LprotocolAnalysis/protocol/InstallResponse;->apkName:Ljava/lang/String;

    return-void
.end method

.method public sizeOf()I
    .locals 5

    const/4 v1, 0x0

    :try_start_0
    iget-object v2, p0, LprotocolAnalysis/protocol/InstallResponse;->head:LprotocolAnalysis/protocol/NetHeader;

    invoke-virtual {v2}, LprotocolAnalysis/protocol/NetHeader;->sizeOf()I

    move-result v2

    add-int/lit8 v2, v2, 0x4

    iget-object v3, p0, LprotocolAnalysis/protocol/InstallResponse;->apkName:Ljava/lang/String;

    const-string v4, "UTF-8"

    invoke-virtual {v3, v4}, Ljava/lang/String;->getBytes(Ljava/lang/String;)[B

    move-result-object v3

    array-length v3, v3
    :try_end_0
    .catch Ljava/io/UnsupportedEncodingException; {:try_start_0 .. :try_end_0} :catch_0

    add-int v1, v2, v3

    :goto_0
    return v1

    :catch_0
    move-exception v0

    invoke-virtual {v0}, Ljava/io/UnsupportedEncodingException;->printStackTrace()V

    goto :goto_0
.end method
