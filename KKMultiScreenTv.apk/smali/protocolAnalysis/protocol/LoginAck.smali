.class public LprotocolAnalysis/protocol/LoginAck;
.super Ljava/lang/Object;
.source "LoginAck.java"


# static fields
.field private static final logger:Lutil/log/Logger;


# instance fields
.field private bestv_flag:I

.field private bestv_id:Ljava/lang/String;

.field public byteArr:[B

.field public head:LprotocolAnalysis/protocol/NetHeader;

.field public lengh:I

.field private protocol:Lcom/konka/IntelligentControl/ioop/comClass/Protocol;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    invoke-static {}, Lutil/log/Logger;->getLogger()Lutil/log/Logger;

    move-result-object v0

    sput-object v0, LprotocolAnalysis/protocol/LoginAck;->logger:Lutil/log/Logger;

    return-void
.end method

.method public constructor <init>()V
    .locals 3

    const/4 v2, 0x0

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    const-string v0, ""

    iput-object v0, p0, LprotocolAnalysis/protocol/LoginAck;->bestv_id:Ljava/lang/String;

    iput v2, p0, LprotocolAnalysis/protocol/LoginAck;->bestv_flag:I

    const/4 v0, 0x0

    iput-object v0, p0, LprotocolAnalysis/protocol/LoginAck;->byteArr:[B

    const/16 v0, 0x800

    iput v0, p0, LprotocolAnalysis/protocol/LoginAck;->lengh:I

    new-instance v0, Lcom/konka/IntelligentControl/ioop/comClass/Protocol;

    invoke-direct {v0}, Lcom/konka/IntelligentControl/ioop/comClass/Protocol;-><init>()V

    iput-object v0, p0, LprotocolAnalysis/protocol/LoginAck;->protocol:Lcom/konka/IntelligentControl/ioop/comClass/Protocol;

    new-instance v0, LprotocolAnalysis/protocol/NetHeader;

    const/16 v1, 0x4200

    invoke-direct {v0, v2, v1}, LprotocolAnalysis/protocol/NetHeader;-><init>(SS)V

    iput-object v0, p0, LprotocolAnalysis/protocol/LoginAck;->head:LprotocolAnalysis/protocol/NetHeader;

    iget v0, p0, LprotocolAnalysis/protocol/LoginAck;->lengh:I

    new-array v0, v0, [B

    iput-object v0, p0, LprotocolAnalysis/protocol/LoginAck;->byteArr:[B

    iget v0, p0, LprotocolAnalysis/protocol/LoginAck;->lengh:I

    iput v0, p0, LprotocolAnalysis/protocol/LoginAck;->lengh:I

    return-void
.end method


# virtual methods
.method public Format([B)V
    .locals 4
    .param p1    # [B

    const/4 v3, 0x0

    iget-object v0, p0, LprotocolAnalysis/protocol/LoginAck;->head:LprotocolAnalysis/protocol/NetHeader;

    invoke-virtual {v0, p1, v3}, LprotocolAnalysis/protocol/NetHeader;->Format([BI)V

    iget v0, p0, LprotocolAnalysis/protocol/LoginAck;->bestv_flag:I

    iget-object v1, p0, LprotocolAnalysis/protocol/LoginAck;->head:LprotocolAnalysis/protocol/NetHeader;

    invoke-virtual {v1}, LprotocolAnalysis/protocol/NetHeader;->sizeOf()I

    move-result v1

    invoke-static {v0, p1, v1}, Lcom/konka/IntelligentControl/ioop/comClass/Protocol;->intToByte(I[BI)V

    iget-object v0, p0, LprotocolAnalysis/protocol/LoginAck;->byteArr:[B

    iget-object v1, p0, LprotocolAnalysis/protocol/LoginAck;->head:LprotocolAnalysis/protocol/NetHeader;

    invoke-virtual {v1}, LprotocolAnalysis/protocol/NetHeader;->sizeOf()I

    move-result v1

    add-int/lit8 v1, v1, 0x4

    iget v2, p0, LprotocolAnalysis/protocol/LoginAck;->lengh:I

    invoke-static {v0, v3, p1, v1, v2}, Ljava/lang/System;->arraycopy(Ljava/lang/Object;ILjava/lang/Object;II)V

    return-void
.end method

.method public Printf()Ljava/lang/String;
    .locals 3

    new-instance v1, Ljava/lang/StringBuilder;

    iget-object v2, p0, LprotocolAnalysis/protocol/LoginAck;->head:LprotocolAnalysis/protocol/NetHeader;

    iget-short v2, v2, LprotocolAnalysis/protocol/NetHeader;->data_len:S

    invoke-static {v2}, Ljava/lang/String;->valueOf(I)Ljava/lang/String;

    move-result-object v2

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    const-string v2, ","

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget-object v2, p0, LprotocolAnalysis/protocol/LoginAck;->head:LprotocolAnalysis/protocol/NetHeader;

    iget-short v2, v2, LprotocolAnalysis/protocol/NetHeader;->data_type:S

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, ","

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget v2, p0, LprotocolAnalysis/protocol/LoginAck;->bestv_flag:I

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, ","

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget-object v2, p0, LprotocolAnalysis/protocol/LoginAck;->bestv_id:Ljava/lang/String;

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public SetData(Ljava/lang/String;)V
    .locals 6
    .param p1    # Ljava/lang/String;

    const/4 v5, 0x0

    iget-object v2, p0, LprotocolAnalysis/protocol/LoginAck;->head:LprotocolAnalysis/protocol/NetHeader;

    iget-object v3, p0, LprotocolAnalysis/protocol/LoginAck;->byteArr:[B

    invoke-virtual {v2, v3, v5}, LprotocolAnalysis/protocol/NetHeader;->Format([BI)V

    const/4 v0, 0x0

    check-cast v0, [B

    :try_start_0
    const-string v2, "UTF-8"

    invoke-virtual {p1, v2}, Ljava/lang/String;->getBytes(Ljava/lang/String;)[B
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    move-result-object v0

    :goto_0
    invoke-virtual {p1}, Ljava/lang/String;->length()I

    move-result v2

    iget-object v3, p0, LprotocolAnalysis/protocol/LoginAck;->byteArr:[B

    array-length v3, v3

    if-ge v2, v3, :cond_0

    iget-object v2, p0, LprotocolAnalysis/protocol/LoginAck;->byteArr:[B

    array-length v3, v0

    invoke-static {v0, v5, v2, v5, v3}, Ljava/lang/System;->arraycopy(Ljava/lang/Object;ILjava/lang/Object;II)V

    array-length v2, v0

    iput v2, p0, LprotocolAnalysis/protocol/LoginAck;->lengh:I

    :goto_1
    iget-object v2, p0, LprotocolAnalysis/protocol/LoginAck;->head:LprotocolAnalysis/protocol/NetHeader;

    iget v3, p0, LprotocolAnalysis/protocol/LoginAck;->lengh:I

    int-to-short v3, v3

    iput-short v3, v2, LprotocolAnalysis/protocol/NetHeader;->data_len:S

    iget-object v2, p0, LprotocolAnalysis/protocol/LoginAck;->head:LprotocolAnalysis/protocol/NetHeader;

    iget-short v3, v2, LprotocolAnalysis/protocol/NetHeader;->data_len:S

    add-int/lit8 v3, v3, 0x4

    int-to-short v3, v3

    iput-short v3, v2, LprotocolAnalysis/protocol/NetHeader;->data_len:S

    const/4 v2, 0x1

    iput v2, p0, LprotocolAnalysis/protocol/LoginAck;->bestv_flag:I

    return-void

    :catch_0
    move-exception v1

    invoke-virtual {v1}, Ljava/lang/Exception;->printStackTrace()V

    goto :goto_0

    :cond_0
    invoke-virtual {p1}, Ljava/lang/String;->getBytes()[B

    move-result-object v2

    iget-object v3, p0, LprotocolAnalysis/protocol/LoginAck;->byteArr:[B

    iget-object v4, p0, LprotocolAnalysis/protocol/LoginAck;->byteArr:[B

    array-length v4, v4

    invoke-static {v2, v5, v3, v5, v4}, Ljava/lang/System;->arraycopy(Ljava/lang/Object;ILjava/lang/Object;II)V

    iget-object v2, p0, LprotocolAnalysis/protocol/LoginAck;->byteArr:[B

    array-length v2, v2

    iput v2, p0, LprotocolAnalysis/protocol/LoginAck;->lengh:I

    goto :goto_1
.end method

.method public getBestvId()Ljava/lang/String;
    .locals 1

    iget-object v0, p0, LprotocolAnalysis/protocol/LoginAck;->bestv_id:Ljava/lang/String;

    return-object v0
.end method

.method public sizeOf()I
    .locals 2

    iget-object v0, p0, LprotocolAnalysis/protocol/LoginAck;->head:LprotocolAnalysis/protocol/NetHeader;

    invoke-virtual {v0}, LprotocolAnalysis/protocol/NetHeader;->sizeOf()I

    move-result v0

    add-int/lit8 v0, v0, 0x4

    iget v1, p0, LprotocolAnalysis/protocol/LoginAck;->lengh:I

    add-int/2addr v0, v1

    return v0
.end method
