.class public LprotocolAnalysis/protocol/packetProtocol;
.super Ljava/lang/Object;
.source "packetProtocol.java"


# instance fields
.field STATETAG:Ljava/lang/String;

.field fileUrl:[B


# direct methods
.method public constructor <init>()V
    .locals 1

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    const/16 v0, 0x190

    new-array v0, v0, [B

    iput-object v0, p0, LprotocolAnalysis/protocol/packetProtocol;->fileUrl:[B

    const-string v0, "PlayStatePacket"

    iput-object v0, p0, LprotocolAnalysis/protocol/packetProtocol;->STATETAG:Ljava/lang/String;

    return-void
.end method

.method public static intToByteArray(I)[B
    .locals 3
    .param p0    # I

    const/4 v1, 0x4

    new-array v0, v1, [B

    const/4 v1, 0x3

    and-int/lit16 v2, p0, 0xff

    int-to-byte v2, v2

    aput-byte v2, v0, v1

    const/4 v1, 0x2

    const v2, 0xff00

    and-int/2addr v2, p0

    shr-int/lit8 v2, v2, 0x8

    int-to-byte v2, v2

    aput-byte v2, v0, v1

    const/4 v1, 0x1

    const/high16 v2, 0xff0000

    and-int/2addr v2, p0

    shr-int/lit8 v2, v2, 0x10

    int-to-byte v2, v2

    aput-byte v2, v0, v1

    const/4 v1, 0x0

    const/high16 v2, -0x1000000

    and-int/2addr v2, p0

    shr-int/lit8 v2, v2, 0x18

    int-to-byte v2, v2

    aput-byte v2, v0, v1

    return-object v0
.end method


# virtual methods
.method public SetPlayStatePacket(IIILjava/lang/String;)[B
    .locals 17
    .param p1    # I
    .param p2    # I
    .param p3    # I
    .param p4    # Ljava/lang/String;

    move-object/from16 v0, p0

    iget-object v14, v0, LprotocolAnalysis/protocol/packetProtocol;->STATETAG:Ljava/lang/String;

    new-instance v15, Ljava/lang/StringBuilder;

    const-string v16, "playState:"

    invoke-direct/range {v15 .. v16}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    move/from16 v0, p1

    invoke-virtual {v15, v0}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v15

    const-string v16, "curtime:"

    invoke-virtual/range {v15 .. v16}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v15

    move/from16 v0, p2

    invoke-virtual {v15, v0}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v15

    const-string v16, "totTime:"

    invoke-virtual/range {v15 .. v16}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v15

    move/from16 v0, p3

    invoke-virtual {v15, v0}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v15

    const-string v16, "path:"

    invoke-virtual/range {v15 .. v16}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v15

    move-object/from16 v0, p4

    invoke-virtual {v15, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v15

    invoke-virtual {v15}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v15

    invoke-static {v14, v15}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    const/4 v14, 0x4

    new-array v1, v14, [B

    const/4 v14, 0x2

    const/16 v15, 0x40

    aput-byte v15, v1, v14

    const/4 v14, 0x3

    const/4 v15, 0x2

    aput-byte v15, v1, v14

    invoke-static/range {p1 .. p1}, LprotocolAnalysis/protocol/packetProtocol;->intToByteArray(I)[B

    move-result-object v12

    invoke-static/range {p2 .. p2}, LprotocolAnalysis/protocol/packetProtocol;->intToByteArray(I)[B

    move-result-object v7

    invoke-static/range {p3 .. p3}, LprotocolAnalysis/protocol/packetProtocol;->intToByteArray(I)[B

    move-result-object v13

    :try_start_0
    const-string v14, "UTF-8"

    move-object/from16 v0, p4

    invoke-virtual {v0, v14}, Ljava/lang/String;->getBytes(Ljava/lang/String;)[B

    move-result-object v14

    move-object/from16 v0, p0

    iput-object v14, v0, LprotocolAnalysis/protocol/packetProtocol;->fileUrl:[B
    :try_end_0
    .catch Ljava/io/UnsupportedEncodingException; {:try_start_0 .. :try_end_0} :catch_0

    :goto_0
    array-length v14, v12

    array-length v15, v7

    add-int/2addr v14, v15

    array-length v15, v13

    add-int/2addr v14, v15

    move-object/from16 v0, p0

    iget-object v15, v0, LprotocolAnalysis/protocol/packetProtocol;->fileUrl:[B

    array-length v15, v15

    add-int/2addr v14, v15

    int-to-short v10, v14

    const/4 v14, 0x0

    move-object/from16 v0, p0

    invoke-virtual {v0, v10, v1, v14}, LprotocolAnalysis/protocol/packetProtocol;->shortToByte(S[BI)V

    const/4 v9, 0x0

    :goto_1
    array-length v14, v1

    if-lt v9, v14, :cond_0

    const/4 v2, 0x0

    array-length v14, v1

    add-int v5, v2, v14

    array-length v14, v12

    add-int v3, v5, v14

    array-length v14, v7

    add-int v6, v3, v14

    array-length v14, v13

    add-int v4, v6, v14

    array-length v14, v1

    add-int/2addr v14, v10

    new-array v11, v14, [B

    const/4 v14, 0x0

    array-length v15, v1

    invoke-static {v1, v14, v11, v2, v15}, Ljava/lang/System;->arraycopy(Ljava/lang/Object;ILjava/lang/Object;II)V

    const/4 v14, 0x0

    array-length v15, v12

    invoke-static {v12, v14, v11, v5, v15}, Ljava/lang/System;->arraycopy(Ljava/lang/Object;ILjava/lang/Object;II)V

    const/4 v14, 0x0

    array-length v15, v7

    invoke-static {v7, v14, v11, v3, v15}, Ljava/lang/System;->arraycopy(Ljava/lang/Object;ILjava/lang/Object;II)V

    const/4 v14, 0x0

    array-length v15, v13

    invoke-static {v13, v14, v11, v6, v15}, Ljava/lang/System;->arraycopy(Ljava/lang/Object;ILjava/lang/Object;II)V

    move-object/from16 v0, p0

    iget-object v14, v0, LprotocolAnalysis/protocol/packetProtocol;->fileUrl:[B

    const/4 v15, 0x0

    move-object/from16 v0, p0

    iget-object v0, v0, LprotocolAnalysis/protocol/packetProtocol;->fileUrl:[B

    move-object/from16 v16, v0

    move-object/from16 v0, v16

    array-length v0, v0

    move/from16 v16, v0

    move/from16 v0, v16

    invoke-static {v14, v15, v11, v4, v0}, Ljava/lang/System;->arraycopy(Ljava/lang/Object;ILjava/lang/Object;II)V

    return-object v11

    :catch_0
    move-exception v8

    invoke-virtual {v8}, Ljava/io/UnsupportedEncodingException;->printStackTrace()V

    goto :goto_0

    :cond_0
    sget-object v14, Ljava/lang/System;->out:Ljava/io/PrintStream;

    aget-byte v15, v1, v9

    invoke-virtual {v14, v15}, Ljava/io/PrintStream;->println(I)V

    add-int/lit8 v9, v9, 0x1

    goto :goto_1
.end method

.method public byteToInt([B)I
    .locals 6
    .param p1    # [B

    const/4 v0, 0x0

    const/4 v1, 0x0

    const/4 v2, 0x0

    const/4 v3, 0x0

    const/4 v4, 0x0

    const/4 v5, 0x3

    aget-byte v5, p1, v5

    and-int/lit16 v1, v5, 0xff

    const/4 v5, 0x2

    aget-byte v5, p1, v5

    and-int/lit16 v2, v5, 0xff

    const/4 v5, 0x1

    aget-byte v5, p1, v5

    and-int/lit16 v3, v5, 0xff

    const/4 v5, 0x0

    aget-byte v5, p1, v5

    and-int/lit16 v4, v5, 0xff

    shl-int/lit8 v4, v4, 0x18

    shl-int/lit8 v3, v3, 0x10

    shl-int/lit8 v2, v2, 0x8

    or-int v5, v1, v2

    or-int/2addr v5, v3

    or-int v0, v5, v4

    return v0
.end method

.method public shortToByte(S[BI)V
    .locals 2
    .param p1    # S
    .param p2    # [B
    .param p3    # I

    add-int/lit8 v0, p3, 0x0

    shr-int/lit8 v1, p1, 0x8

    int-to-byte v1, v1

    aput-byte v1, p2, v0

    add-int/lit8 v0, p3, 0x1

    shr-int/lit8 v1, p1, 0x0

    int-to-byte v1, v1

    aput-byte v1, p2, v0

    return-void
.end method
