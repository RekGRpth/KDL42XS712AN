.class public LprotocolAnalysis/protocol/Login;
.super Ljava/lang/Object;
.source "Login.java"


# instance fields
.field public head:LprotocolAnalysis/protocol/NetHeader;


# direct methods
.method public constructor <init>()V
    .locals 3

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    new-instance v0, LprotocolAnalysis/protocol/NetHeader;

    const/4 v1, 0x0

    const/16 v2, 0x3000

    invoke-direct {v0, v1, v2}, LprotocolAnalysis/protocol/NetHeader;-><init>(SS)V

    iput-object v0, p0, LprotocolAnalysis/protocol/Login;->head:LprotocolAnalysis/protocol/NetHeader;

    return-void
.end method


# virtual methods
.method public Format([B)V
    .locals 2
    .param p1    # [B

    iget-object v0, p0, LprotocolAnalysis/protocol/Login;->head:LprotocolAnalysis/protocol/NetHeader;

    const/4 v1, 0x0

    invoke-virtual {v0, p1, v1}, LprotocolAnalysis/protocol/NetHeader;->Format([BI)V

    return-void
.end method

.method public Printf([B)Ljava/lang/String;
    .locals 1
    .param p1    # [B

    iget-object v0, p0, LprotocolAnalysis/protocol/Login;->head:LprotocolAnalysis/protocol/NetHeader;

    invoke-virtual {v0, p1}, LprotocolAnalysis/protocol/NetHeader;->printf([B)Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public SizeOf()I
    .locals 1

    iget-object v0, p0, LprotocolAnalysis/protocol/Login;->head:LprotocolAnalysis/protocol/NetHeader;

    invoke-virtual {v0}, LprotocolAnalysis/protocol/NetHeader;->sizeOf()I

    move-result v0

    return v0
.end method
