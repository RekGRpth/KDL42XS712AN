.class public LprotocolAnalysis/protocol/UpgradeProgress;
.super LprotocolAnalysis/protocol/BaseProtocol;
.source "UpgradeProgress.java"


# static fields
.field public static final UPDATEPRE:I = 0x500c


# instance fields
.field public head:LprotocolAnalysis/protocol/NetHeader;

.field private upgradePos:I


# direct methods
.method public constructor <init>()V
    .locals 0

    invoke-direct {p0}, LprotocolAnalysis/protocol/BaseProtocol;-><init>()V

    return-void
.end method


# virtual methods
.method public format([B)V
    .locals 2
    .param p1    # [B

    const/4 v1, 0x0

    invoke-static {p1, v1}, Ljava/util/Arrays;->fill([BB)V

    iget-object v0, p0, LprotocolAnalysis/protocol/UpgradeProgress;->head:LprotocolAnalysis/protocol/NetHeader;

    invoke-virtual {v0, p1, v1}, LprotocolAnalysis/protocol/NetHeader;->Format([BI)V

    iget v0, p0, LprotocolAnalysis/protocol/UpgradeProgress;->upgradePos:I

    iget-object v1, p0, LprotocolAnalysis/protocol/UpgradeProgress;->head:LprotocolAnalysis/protocol/NetHeader;

    invoke-virtual {v1}, LprotocolAnalysis/protocol/NetHeader;->sizeOf()I

    move-result v1

    invoke-static {v0, p1, v1}, LprotocolAnalysis/protocol/UpgradeProgress;->intToByte(I[BI)V

    return-void
.end method

.method public printf([B)Ljava/lang/String;
    .locals 9
    .param p1    # [B

    const/4 v8, 0x4

    const/4 v7, 0x2

    const/4 v6, 0x0

    iget-object v5, p0, LprotocolAnalysis/protocol/UpgradeProgress;->head:LprotocolAnalysis/protocol/NetHeader;

    invoke-virtual {v5, p1}, LprotocolAnalysis/protocol/NetHeader;->printf([B)Ljava/lang/String;

    move-result-object v0

    const/4 v5, 0x3

    new-array v3, v5, [B

    invoke-static {p1, v6, v3, v6, v7}, Ljava/lang/System;->arraycopy(Ljava/lang/Object;ILjava/lang/Object;II)V

    aput-byte v6, v3, v7

    invoke-static {v3}, LprotocolAnalysis/protocol/UpgradeProgress;->byteToShort([B)S

    move-result v1

    const/4 v5, 0x5

    new-array v4, v5, [B

    iget-object v5, p0, LprotocolAnalysis/protocol/UpgradeProgress;->head:LprotocolAnalysis/protocol/NetHeader;

    invoke-virtual {v5}, LprotocolAnalysis/protocol/NetHeader;->sizeOf()I

    move-result v5

    invoke-static {p1, v5, v4, v6, v8}, Ljava/lang/System;->arraycopy(Ljava/lang/Object;ILjava/lang/Object;II)V

    aput-byte v6, v4, v8

    invoke-static {v4}, LprotocolAnalysis/protocol/UpgradeProgress;->byteToInt([B)I

    move-result v2

    new-instance v5, Ljava/lang/StringBuilder;

    invoke-static {v0}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v6

    invoke-direct {v5, v6}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v5, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    return-object v5
.end method

.method public setData(I)V
    .locals 3
    .param p1    # I

    new-instance v0, LprotocolAnalysis/protocol/NetHeader;

    const/4 v1, 0x4

    const/16 v2, 0x500c

    invoke-direct {v0, v1, v2}, LprotocolAnalysis/protocol/NetHeader;-><init>(SS)V

    iput-object v0, p0, LprotocolAnalysis/protocol/UpgradeProgress;->head:LprotocolAnalysis/protocol/NetHeader;

    iput p1, p0, LprotocolAnalysis/protocol/UpgradeProgress;->upgradePos:I

    return-void
.end method

.method public sizeOf()I
    .locals 1

    iget-object v0, p0, LprotocolAnalysis/protocol/UpgradeProgress;->head:LprotocolAnalysis/protocol/NetHeader;

    invoke-virtual {v0}, LprotocolAnalysis/protocol/NetHeader;->sizeOf()I

    move-result v0

    add-int/lit8 v0, v0, 0x4

    return v0
.end method
