.class public LprotocolAnalysis/protocol/SendUdpInfoPacket;
.super LprotocolAnalysis/protocol/BaseProtocol;
.source "SendUdpInfoPacket.java"


# instance fields
.field private dev_name:Ljava/lang/String;

.field private dev_type:S

.field private head:LprotocolAnalysis/protocol/NetHeader;

.field private server_id:I

.field private soft_version:S


# direct methods
.method public constructor <init>(I)V
    .locals 3
    .param p1    # I

    invoke-direct {p0}, LprotocolAnalysis/protocol/BaseProtocol;-><init>()V

    const/4 v0, 0x0

    iput v0, p0, LprotocolAnalysis/protocol/SendUdpInfoPacket;->server_id:I

    const/4 v0, 0x3

    iput-short v0, p0, LprotocolAnalysis/protocol/SendUdpInfoPacket;->soft_version:S

    const/16 v0, 0x2ff

    iput-short v0, p0, LprotocolAnalysis/protocol/SendUdpInfoPacket;->dev_type:S

    const/4 v0, 0x0

    iput-object v0, p0, LprotocolAnalysis/protocol/SendUdpInfoPacket;->dev_name:Ljava/lang/String;

    new-instance v0, LprotocolAnalysis/protocol/NetHeader;

    add-int/lit8 v1, p1, 0x4

    add-int/lit8 v1, v1, 0x2

    add-int/lit8 v1, v1, 0x2

    int-to-short v1, v1

    const/16 v2, 0x4070

    invoke-direct {v0, v1, v2}, LprotocolAnalysis/protocol/NetHeader;-><init>(SS)V

    iput-object v0, p0, LprotocolAnalysis/protocol/SendUdpInfoPacket;->head:LprotocolAnalysis/protocol/NetHeader;

    return-void
.end method


# virtual methods
.method public SetDeviceInfo(ISSLjava/lang/String;)V
    .locals 0
    .param p1    # I
    .param p2    # S
    .param p3    # S
    .param p4    # Ljava/lang/String;

    iput p1, p0, LprotocolAnalysis/protocol/SendUdpInfoPacket;->server_id:I

    iput-short p2, p0, LprotocolAnalysis/protocol/SendUdpInfoPacket;->soft_version:S

    iput-short p3, p0, LprotocolAnalysis/protocol/SendUdpInfoPacket;->dev_type:S

    iput-object p4, p0, LprotocolAnalysis/protocol/SendUdpInfoPacket;->dev_name:Ljava/lang/String;

    return-void
.end method

.method public format([B)V
    .locals 5
    .param p1    # [B

    const/4 v3, 0x0

    invoke-static {p1, v3}, Ljava/util/Arrays;->fill([BB)V

    iget-object v2, p0, LprotocolAnalysis/protocol/SendUdpInfoPacket;->head:LprotocolAnalysis/protocol/NetHeader;

    invoke-virtual {v2, p1, v3}, LprotocolAnalysis/protocol/NetHeader;->Format([BI)V

    iget v2, p0, LprotocolAnalysis/protocol/SendUdpInfoPacket;->server_id:I

    iget-object v3, p0, LprotocolAnalysis/protocol/SendUdpInfoPacket;->head:LprotocolAnalysis/protocol/NetHeader;

    invoke-virtual {v3}, LprotocolAnalysis/protocol/NetHeader;->sizeOf()I

    move-result v3

    invoke-static {v2, p1, v3}, LprotocolAnalysis/protocol/SendUdpInfoPacket;->intToByte(I[BI)V

    iget-short v2, p0, LprotocolAnalysis/protocol/SendUdpInfoPacket;->soft_version:S

    iget-object v3, p0, LprotocolAnalysis/protocol/SendUdpInfoPacket;->head:LprotocolAnalysis/protocol/NetHeader;

    invoke-virtual {v3}, LprotocolAnalysis/protocol/NetHeader;->sizeOf()I

    move-result v3

    add-int/lit8 v3, v3, 0x4

    invoke-static {v2, p1, v3}, LprotocolAnalysis/protocol/SendUdpInfoPacket;->shortToByte(S[BI)V

    iget-short v2, p0, LprotocolAnalysis/protocol/SendUdpInfoPacket;->dev_type:S

    iget-object v3, p0, LprotocolAnalysis/protocol/SendUdpInfoPacket;->head:LprotocolAnalysis/protocol/NetHeader;

    invoke-virtual {v3}, LprotocolAnalysis/protocol/NetHeader;->sizeOf()I

    move-result v3

    add-int/lit8 v3, v3, 0x6

    invoke-static {v2, p1, v3}, LprotocolAnalysis/protocol/SendUdpInfoPacket;->shortToByte(S[BI)V

    :try_start_0
    iget-object v2, p0, LprotocolAnalysis/protocol/SendUdpInfoPacket;->dev_name:Ljava/lang/String;

    const-string v3, "UTF-8"

    invoke-virtual {v2, v3}, Ljava/lang/String;->getBytes(Ljava/lang/String;)[B

    move-result-object v0

    const/4 v2, 0x0

    iget-object v3, p0, LprotocolAnalysis/protocol/SendUdpInfoPacket;->head:LprotocolAnalysis/protocol/NetHeader;

    invoke-virtual {v3}, LprotocolAnalysis/protocol/NetHeader;->sizeOf()I

    move-result v3

    add-int/lit8 v3, v3, 0x8

    array-length v4, v0

    invoke-static {v0, v2, p1, v3, v4}, Ljava/lang/System;->arraycopy(Ljava/lang/Object;ILjava/lang/Object;II)V
    :try_end_0
    .catch Ljava/io/UnsupportedEncodingException; {:try_start_0 .. :try_end_0} :catch_0

    :goto_0
    return-void

    :catch_0
    move-exception v1

    invoke-virtual {v1}, Ljava/io/UnsupportedEncodingException;->printStackTrace()V

    goto :goto_0
.end method

.method public printf([B)Ljava/lang/String;
    .locals 14
    .param p1    # [B

    iget-object v11, p0, LprotocolAnalysis/protocol/SendUdpInfoPacket;->head:LprotocolAnalysis/protocol/NetHeader;

    invoke-virtual {v11, p1}, LprotocolAnalysis/protocol/NetHeader;->printf([B)Ljava/lang/String;

    move-result-object v0

    const/4 v11, 0x5

    new-array v5, v11, [B

    iget-object v11, p0, LprotocolAnalysis/protocol/SendUdpInfoPacket;->head:LprotocolAnalysis/protocol/NetHeader;

    invoke-virtual {v11}, LprotocolAnalysis/protocol/NetHeader;->sizeOf()I

    move-result v11

    const/4 v12, 0x0

    const/4 v13, 0x4

    invoke-static {p1, v11, v5, v12, v13}, Ljava/lang/System;->arraycopy(Ljava/lang/Object;ILjava/lang/Object;II)V

    const/4 v11, 0x2

    const/4 v12, 0x0

    aput-byte v12, v5, v11

    new-instance v1, Ljava/lang/String;

    new-instance v11, Ljava/lang/StringBuilder;

    invoke-static {v5}, LprotocolAnalysis/protocol/SendUdpInfoPacket;->byteToInt([B)I

    move-result v12

    invoke-static {v12}, Ljava/lang/Integer;->toString(I)Ljava/lang/String;

    move-result-object v12

    invoke-static {v12}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v12

    invoke-direct {v11, v12}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    const-string v12, ","

    invoke-virtual {v11, v12}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v11

    invoke-virtual {v11}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v11

    invoke-direct {v1, v11}, Ljava/lang/String;-><init>(Ljava/lang/String;)V

    const/4 v11, 0x3

    new-array v6, v11, [B

    const/4 v11, 0x0

    const/4 v12, 0x0

    const/4 v13, 0x2

    invoke-static {p1, v11, v6, v12, v13}, Ljava/lang/System;->arraycopy(Ljava/lang/Object;ILjava/lang/Object;II)V

    const/4 v11, 0x2

    const/4 v12, 0x0

    aput-byte v12, v6, v11

    invoke-static {v6}, LprotocolAnalysis/protocol/SendUdpInfoPacket;->byteToShort([B)S

    move-result v3

    const/4 v11, 0x3

    new-array v7, v11, [B

    iget-object v11, p0, LprotocolAnalysis/protocol/SendUdpInfoPacket;->head:LprotocolAnalysis/protocol/NetHeader;

    invoke-virtual {v11}, LprotocolAnalysis/protocol/NetHeader;->sizeOf()I

    move-result v11

    add-int/lit8 v11, v11, 0x4

    const/4 v12, 0x0

    const/4 v13, 0x2

    invoke-static {p1, v11, v7, v12, v13}, Ljava/lang/System;->arraycopy(Ljava/lang/Object;ILjava/lang/Object;II)V

    const/4 v11, 0x2

    const/4 v12, 0x0

    aput-byte v12, v7, v11

    new-instance v10, Ljava/lang/String;

    new-instance v11, Ljava/lang/StringBuilder;

    invoke-static {v7}, LprotocolAnalysis/protocol/SendUdpInfoPacket;->byteToShort([B)S

    move-result v12

    invoke-static {v12}, Ljava/lang/Integer;->toString(I)Ljava/lang/String;

    move-result-object v12

    invoke-static {v12}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v12

    invoke-direct {v11, v12}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    const-string v12, ","

    invoke-virtual {v11, v12}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v11

    invoke-virtual {v11}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v11

    invoke-direct {v10, v11}, Ljava/lang/String;-><init>(Ljava/lang/String;)V

    const/4 v11, 0x3

    new-array v8, v11, [B

    iget-object v11, p0, LprotocolAnalysis/protocol/SendUdpInfoPacket;->head:LprotocolAnalysis/protocol/NetHeader;

    invoke-virtual {v11}, LprotocolAnalysis/protocol/NetHeader;->sizeOf()I

    move-result v11

    add-int/lit8 v11, v11, 0x6

    const/4 v12, 0x0

    const/4 v13, 0x2

    invoke-static {p1, v11, v8, v12, v13}, Ljava/lang/System;->arraycopy(Ljava/lang/Object;ILjava/lang/Object;II)V

    const/4 v11, 0x2

    const/4 v12, 0x0

    aput-byte v12, v7, v11

    new-instance v9, Ljava/lang/String;

    new-instance v11, Ljava/lang/StringBuilder;

    invoke-static {v8}, LprotocolAnalysis/protocol/SendUdpInfoPacket;->byteToShort([B)S

    move-result v12

    invoke-static {v12}, Ljava/lang/Integer;->toString(I)Ljava/lang/String;

    move-result-object v12

    invoke-static {v12}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v12

    invoke-direct {v11, v12}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    const-string v12, ","

    invoke-virtual {v11, v12}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v11

    invoke-virtual {v11}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v11

    invoke-direct {v9, v11}, Ljava/lang/String;-><init>(Ljava/lang/String;)V

    new-array v2, v3, [B

    iget-object v11, p0, LprotocolAnalysis/protocol/SendUdpInfoPacket;->head:LprotocolAnalysis/protocol/NetHeader;

    invoke-virtual {v11}, LprotocolAnalysis/protocol/NetHeader;->sizeOf()I

    move-result v11

    add-int/lit8 v11, v11, 0x8

    const/4 v12, 0x0

    add-int/lit8 v13, v3, -0x8

    invoke-static {p1, v11, v2, v12, v13}, Ljava/lang/System;->arraycopy(Ljava/lang/Object;ILjava/lang/Object;II)V

    new-instance v4, Ljava/lang/String;

    invoke-direct {v4, v2}, Ljava/lang/String;-><init>([B)V

    new-instance v11, Ljava/lang/StringBuilder;

    invoke-static {v0}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v12

    invoke-direct {v11, v12}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v11, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v11

    invoke-virtual {v11, v10}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v11

    invoke-virtual {v11, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v11

    invoke-virtual {v11, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v11

    invoke-virtual {v11}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v11

    return-object v11
.end method

.method public sizeOf()I
    .locals 2

    iget-object v0, p0, LprotocolAnalysis/protocol/SendUdpInfoPacket;->head:LprotocolAnalysis/protocol/NetHeader;

    invoke-virtual {v0}, LprotocolAnalysis/protocol/NetHeader;->sizeOf()I

    move-result v0

    add-int/lit8 v0, v0, 0x4

    add-int/lit8 v0, v0, 0x2

    add-int/lit8 v0, v0, 0x2

    iget-object v1, p0, LprotocolAnalysis/protocol/SendUdpInfoPacket;->dev_name:Ljava/lang/String;

    invoke-virtual {v1}, Ljava/lang/String;->length()I

    move-result v1

    add-int/2addr v0, v1

    return v0
.end method
