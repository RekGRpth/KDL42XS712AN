.class public LprotocolAnalysis/protocol/GetServerPacketAck;
.super LprotocolAnalysis/protocol/BaseProtocol;
.source "GetServerPacketAck.java"


# instance fields
.field public hd_type:S

.field public head:LprotocolAnalysis/protocol/NetHeader;

.field public os_type:S

.field public server_name:S

.field public service_id:I


# direct methods
.method public constructor <init>(I)V
    .locals 3
    .param p1    # I

    invoke-direct {p0}, LprotocolAnalysis/protocol/BaseProtocol;-><init>()V

    new-instance v0, LprotocolAnalysis/protocol/NetHeader;

    const/4 v1, 0x4

    const/16 v2, 0x4001

    invoke-direct {v0, v1, v2}, LprotocolAnalysis/protocol/NetHeader;-><init>(SS)V

    iput-object v0, p0, LprotocolAnalysis/protocol/GetServerPacketAck;->head:LprotocolAnalysis/protocol/NetHeader;

    iput p1, p0, LprotocolAnalysis/protocol/GetServerPacketAck;->service_id:I

    return-void
.end method


# virtual methods
.method public format([B)V
    .locals 0
    .param p1    # [B

    return-void
.end method

.method public printf([B)Ljava/lang/String;
    .locals 1
    .param p1    # [B

    const/4 v0, 0x0

    return-object v0
.end method

.method public sizeOf()I
    .locals 1

    const/4 v0, 0x0

    return v0
.end method
