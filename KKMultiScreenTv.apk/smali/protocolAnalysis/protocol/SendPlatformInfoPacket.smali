.class public LprotocolAnalysis/protocol/SendPlatformInfoPacket;
.super LprotocolAnalysis/protocol/BaseProtocol;
.source "SendPlatformInfoPacket.java"


# instance fields
.field public head:LprotocolAnalysis/protocol/NetHeader;

.field public mBusinessFlag:Ljava/lang/String;


# direct methods
.method public constructor <init>()V
    .locals 1

    invoke-direct {p0}, LprotocolAnalysis/protocol/BaseProtocol;-><init>()V

    const/4 v0, 0x0

    iput-object v0, p0, LprotocolAnalysis/protocol/SendPlatformInfoPacket;->mBusinessFlag:Ljava/lang/String;

    return-void
.end method

.method public constructor <init>(I)V
    .locals 3
    .param p1    # I

    invoke-direct {p0}, LprotocolAnalysis/protocol/BaseProtocol;-><init>()V

    const/4 v0, 0x0

    iput-object v0, p0, LprotocolAnalysis/protocol/SendPlatformInfoPacket;->mBusinessFlag:Ljava/lang/String;

    new-instance v0, LprotocolAnalysis/protocol/NetHeader;

    int-to-short v1, p1

    const/16 v2, 0x4203

    invoke-direct {v0, v1, v2}, LprotocolAnalysis/protocol/NetHeader;-><init>(SS)V

    iput-object v0, p0, LprotocolAnalysis/protocol/SendPlatformInfoPacket;->head:LprotocolAnalysis/protocol/NetHeader;

    const-string v0, "SendBusinessFlagInfoPacket"

    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "mBF_len== "

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    return-void
.end method


# virtual methods
.method public format([B)V
    .locals 5
    .param p1    # [B

    const/4 v3, 0x0

    invoke-static {p1, v3}, Ljava/util/Arrays;->fill([BB)V

    iget-object v2, p0, LprotocolAnalysis/protocol/SendPlatformInfoPacket;->head:LprotocolAnalysis/protocol/NetHeader;

    invoke-virtual {v2, p1, v3}, LprotocolAnalysis/protocol/NetHeader;->Format([BI)V

    :try_start_0
    iget-object v2, p0, LprotocolAnalysis/protocol/SendPlatformInfoPacket;->mBusinessFlag:Ljava/lang/String;

    const-string v3, "UTF-8"

    invoke-virtual {v2, v3}, Ljava/lang/String;->getBytes(Ljava/lang/String;)[B

    move-result-object v0

    const/4 v2, 0x0

    iget-object v3, p0, LprotocolAnalysis/protocol/SendPlatformInfoPacket;->head:LprotocolAnalysis/protocol/NetHeader;

    invoke-virtual {v3}, LprotocolAnalysis/protocol/NetHeader;->sizeOf()I

    move-result v3

    iget-object v4, p0, LprotocolAnalysis/protocol/SendPlatformInfoPacket;->mBusinessFlag:Ljava/lang/String;

    invoke-virtual {v4}, Ljava/lang/String;->length()I

    move-result v4

    invoke-static {v0, v2, p1, v3, v4}, Ljava/lang/System;->arraycopy(Ljava/lang/Object;ILjava/lang/Object;II)V

    const-string v2, "SendBusinessFlagInfoPacket"

    new-instance v3, Ljava/lang/StringBuilder;

    const-string v4, "arr.length == "

    invoke-direct {v3, v4}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    array-length v4, v0

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v2, v3}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I
    :try_end_0
    .catch Ljava/io/UnsupportedEncodingException; {:try_start_0 .. :try_end_0} :catch_0

    :goto_0
    return-void

    :catch_0
    move-exception v1

    invoke-virtual {v1}, Ljava/io/UnsupportedEncodingException;->printStackTrace()V

    goto :goto_0
.end method

.method public printf([B)Ljava/lang/String;
    .locals 5
    .param p1    # [B

    const/4 v4, 0x0

    iget-object v2, p0, LprotocolAnalysis/protocol/SendPlatformInfoPacket;->head:LprotocolAnalysis/protocol/NetHeader;

    invoke-virtual {v2, p1}, LprotocolAnalysis/protocol/NetHeader;->printf([B)Ljava/lang/String;

    move-result-object v0

    const/4 v2, 0x5

    new-array v1, v2, [B

    iget-object v2, p0, LprotocolAnalysis/protocol/SendPlatformInfoPacket;->head:LprotocolAnalysis/protocol/NetHeader;

    invoke-virtual {v2}, LprotocolAnalysis/protocol/NetHeader;->sizeOf()I

    move-result v2

    const/4 v3, 0x4

    invoke-static {p1, v2, v1, v4, v3}, Ljava/lang/System;->arraycopy(Ljava/lang/Object;ILjava/lang/Object;II)V

    const/4 v2, 0x2

    aput-byte v4, v1, v2

    sget-object v2, Ljava/lang/System;->out:Ljava/io/PrintStream;

    new-instance v3, Ljava/lang/StringBuilder;

    const-string v4, "t==============="

    invoke-direct {v3, v4}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-static {v1}, Ljava/util/Arrays;->toString([B)Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Ljava/io/PrintStream;->println(Ljava/lang/String;)V

    return-object v0
.end method

.method public setPlatformInfo(Ljava/lang/String;)V
    .locals 0
    .param p1    # Ljava/lang/String;

    iput-object p1, p0, LprotocolAnalysis/protocol/SendPlatformInfoPacket;->mBusinessFlag:Ljava/lang/String;

    return-void
.end method

.method public sizeOf()I
    .locals 2

    iget-object v0, p0, LprotocolAnalysis/protocol/SendPlatformInfoPacket;->head:LprotocolAnalysis/protocol/NetHeader;

    invoke-virtual {v0}, LprotocolAnalysis/protocol/NetHeader;->sizeOf()I

    move-result v0

    iget-object v1, p0, LprotocolAnalysis/protocol/SendPlatformInfoPacket;->mBusinessFlag:Ljava/lang/String;

    invoke-virtual {v1}, Ljava/lang/String;->length()I

    move-result v1

    add-int/2addr v0, v1

    return v0
.end method
