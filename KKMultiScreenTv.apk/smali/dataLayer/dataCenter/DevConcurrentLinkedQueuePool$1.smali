.class LdataLayer/dataCenter/DevConcurrentLinkedQueuePool$1;
.super Ljava/lang/Thread;
.source "DevConcurrentLinkedQueuePool.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = LdataLayer/dataCenter/DevConcurrentLinkedQueuePool;->mainRun(Ljava/lang/String;)V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:LdataLayer/dataCenter/DevConcurrentLinkedQueuePool;


# direct methods
.method constructor <init>(LdataLayer/dataCenter/DevConcurrentLinkedQueuePool;)V
    .locals 0

    iput-object p1, p0, LdataLayer/dataCenter/DevConcurrentLinkedQueuePool$1;->this$0:LdataLayer/dataCenter/DevConcurrentLinkedQueuePool;

    invoke-direct {p0}, Ljava/lang/Thread;-><init>()V

    return-void
.end method


# virtual methods
.method public run()V
    .locals 6

    const/4 v1, 0x0

    :goto_0
    const/4 v3, 0x2

    if-lt v1, v3, :cond_0

    invoke-virtual {p0}, LdataLayer/dataCenter/DevConcurrentLinkedQueuePool$1;->interrupt()V

    :goto_1
    return-void

    :cond_0
    :try_start_0
    new-instance v2, LdataLayer/dataCenter/DevConcurrentLinkedQueuePool$Worker;

    invoke-direct {v2, v1}, LdataLayer/dataCenter/DevConcurrentLinkedQueuePool$Worker;-><init>(I)V

    new-instance v3, Ljava/lang/Thread;

    invoke-direct {v3, v2}, Ljava/lang/Thread;-><init>(Ljava/lang/Runnable;)V

    invoke-virtual {v3}, Ljava/lang/Thread;->start()V
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    add-int/lit8 v1, v1, 0x1

    goto :goto_0

    :catch_0
    move-exception v0

    :try_start_1
    sget-object v3, Ljava/lang/System;->err:Ljava/io/PrintStream;

    new-instance v4, Ljava/lang/StringBuilder;

    const-string v5, "Exception "

    invoke-direct {v4, v5}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v0}, Ljava/lang/Exception;->getMessage()Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v3, v4}, Ljava/io/PrintStream;->println(Ljava/lang/String;)V
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    invoke-virtual {p0}, LdataLayer/dataCenter/DevConcurrentLinkedQueuePool$1;->interrupt()V

    goto :goto_1

    :catchall_0
    move-exception v3

    invoke-virtual {p0}, LdataLayer/dataCenter/DevConcurrentLinkedQueuePool$1;->interrupt()V

    throw v3
.end method
