.class public LdataLayer/dataCenter/userGroup$User;
.super Ljava/lang/Object;
.source "userGroup.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = LdataLayer/dataCenter/userGroup;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x9
    name = "User"
.end annotation


# instance fields
.field private m_name:Ljava/lang/String;

.field private final m_socket:Lnetwork/Interface/NIOSocket;

.field private writeLock:Ljava/lang/Object;


# direct methods
.method constructor <init>(Lnetwork/Interface/NIOSocket;)V
    .locals 2
    .param p1    # Lnetwork/Interface/NIOSocket;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    new-instance v0, Ljava/lang/Object;

    invoke-direct {v0}, Ljava/lang/Object;-><init>()V

    iput-object v0, p0, LdataLayer/dataCenter/userGroup$User;->writeLock:Ljava/lang/Object;

    iput-object p1, p0, LdataLayer/dataCenter/userGroup$User;->m_socket:Lnetwork/Interface/NIOSocket;

    new-instance v0, Ljava/lang/StringBuilder;

    invoke-interface {p1}, Lnetwork/Interface/NIOSocket;->getIp()Ljava/lang/String;

    move-result-object v1

    invoke-static {v1}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    const-string v1, ":"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-interface {p1}, Lnetwork/Interface/NIOSocket;->getPort()I

    move-result v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, LdataLayer/dataCenter/userGroup$User;->m_name:Ljava/lang/String;

    iget-object v0, p0, LdataLayer/dataCenter/userGroup$User;->m_name:Ljava/lang/String;

    invoke-virtual {v0}, Ljava/lang/String;->trim()Ljava/lang/String;

    return-void
.end method

.method static synthetic access$0(LdataLayer/dataCenter/userGroup$User;)Ljava/lang/String;
    .locals 1

    iget-object v0, p0, LdataLayer/dataCenter/userGroup$User;->m_name:Ljava/lang/String;

    return-object v0
.end method


# virtual methods
.method public sendReturnData([B)V
    .locals 2
    .param p1    # [B

    iget-object v0, p0, LdataLayer/dataCenter/userGroup$User;->m_name:Ljava/lang/String;

    if-eqz v0, :cond_0

    iget-object v1, p0, LdataLayer/dataCenter/userGroup$User;->writeLock:Ljava/lang/Object;

    monitor-enter v1

    :try_start_0
    iget-object v0, p0, LdataLayer/dataCenter/userGroup$User;->m_socket:Lnetwork/Interface/NIOSocket;

    invoke-interface {v0, p1}, Lnetwork/Interface/NIOSocket;->write([B)Z

    monitor-exit v1

    :cond_0
    return-void

    :catchall_0
    move-exception v0

    monitor-exit v1
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    throw v0
.end method

.method public toString()Ljava/lang/String;
    .locals 1

    iget-object v0, p0, LdataLayer/dataCenter/userGroup$User;->m_name:Ljava/lang/String;

    if-eqz v0, :cond_0

    iget-object v0, p0, LdataLayer/dataCenter/userGroup$User;->m_name:Ljava/lang/String;

    :goto_0
    return-object v0

    :cond_0
    const-string v0, "kkMutiScreenTv@null object"

    goto :goto_0
.end method
