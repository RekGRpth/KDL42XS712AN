.class Lutil/comm/myQueue$ListIterator;
.super Ljava/lang/Object;
.source "myQueue.java"

# interfaces
.implements Ljava/util/Iterator;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lutil/comm/myQueue;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x2
    name = "ListIterator"
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Object;",
        "Ljava/util/Iterator",
        "<TItem;>;"
    }
.end annotation


# instance fields
.field private current:Lutil/comm/myQueue$Node;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lutil/comm/myQueue",
            "<TItem;>.Node;"
        }
    .end annotation
.end field

.field final synthetic this$0:Lutil/comm/myQueue;


# direct methods
.method private constructor <init>(Lutil/comm/myQueue;)V
    .locals 1

    iput-object p1, p0, Lutil/comm/myQueue$ListIterator;->this$0:Lutil/comm/myQueue;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    # getter for: Lutil/comm/myQueue;->first:Lutil/comm/myQueue$Node;
    invoke-static {p1}, Lutil/comm/myQueue;->access$0(Lutil/comm/myQueue;)Lutil/comm/myQueue$Node;

    move-result-object v0

    iput-object v0, p0, Lutil/comm/myQueue$ListIterator;->current:Lutil/comm/myQueue$Node;

    return-void
.end method

.method synthetic constructor <init>(Lutil/comm/myQueue;Lutil/comm/myQueue$ListIterator;)V
    .locals 0

    invoke-direct {p0, p1}, Lutil/comm/myQueue$ListIterator;-><init>(Lutil/comm/myQueue;)V

    return-void
.end method


# virtual methods
.method public hasNext()Z
    .locals 1

    iget-object v0, p0, Lutil/comm/myQueue$ListIterator;->current:Lutil/comm/myQueue$Node;

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public next()Ljava/lang/Object;
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()TItem;"
        }
    .end annotation

    invoke-virtual {p0}, Lutil/comm/myQueue$ListIterator;->hasNext()Z

    move-result v1

    if-nez v1, :cond_0

    new-instance v1, Ljava/util/NoSuchElementException;

    invoke-direct {v1}, Ljava/util/NoSuchElementException;-><init>()V

    throw v1

    :cond_0
    iget-object v1, p0, Lutil/comm/myQueue$ListIterator;->current:Lutil/comm/myQueue$Node;

    # getter for: Lutil/comm/myQueue$Node;->item:Ljava/lang/Object;
    invoke-static {v1}, Lutil/comm/myQueue$Node;->access$0(Lutil/comm/myQueue$Node;)Ljava/lang/Object;

    move-result-object v0

    iget-object v1, p0, Lutil/comm/myQueue$ListIterator;->current:Lutil/comm/myQueue$Node;

    # getter for: Lutil/comm/myQueue$Node;->next:Lutil/comm/myQueue$Node;
    invoke-static {v1}, Lutil/comm/myQueue$Node;->access$1(Lutil/comm/myQueue$Node;)Lutil/comm/myQueue$Node;

    move-result-object v1

    iput-object v1, p0, Lutil/comm/myQueue$ListIterator;->current:Lutil/comm/myQueue$Node;

    return-object v0
.end method

.method public remove()V
    .locals 1

    new-instance v0, Ljava/lang/UnsupportedOperationException;

    invoke-direct {v0}, Ljava/lang/UnsupportedOperationException;-><init>()V

    throw v0
.end method
