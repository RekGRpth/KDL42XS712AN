.class public Lcom/konka/IntelligentControl/jniInterface/IfileOper;
.super Ljava/lang/Object;
.source "IfileOper.java"


# direct methods
.method static constructor <clinit>()V
    .locals 2

    :try_start_0
    const-string v1, "vInput"

    invoke-static {v1}, Ljava/lang/System;->loadLibrary(Ljava/lang/String;)V
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    :goto_0
    return-void

    :catch_0
    move-exception v0

    invoke-virtual {v0}, Ljava/lang/Exception;->printStackTrace()V

    goto :goto_0
.end method

.method public constructor <init>()V
    .locals 0

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public native close(I)V
.end method

.method public native handle([BI)V
.end method

.method public native openInput(Ljava/lang/String;)Ljava/lang/String;
.end method

.method public native setupAccsensorDevice()V
.end method

.method public native setupIrDevice()V
.end method

.method public native writeAccsensorDevice(III)V
.end method
