.class public Lcom/konka/IntelligentControl/ioop/specificOp/BestvOp;
.super Ljava/lang/Object;
.source "BestvOp.java"


# static fields
.field private static CONFFILE:Ljava/lang/String; = null

.field public static final TAG:Ljava/lang/String; = "bestv_Op"

.field private static content:Ljava/lang/String;

.field private static str:Ljava/lang/String;


# instance fields
.field public best_useid:Ljava/lang/String;

.field bestvSerivce:Lcom/bestv/ott/online/auth/IBesTVAuthService;

.field public loginData:[B

.field private mAuthService:Lcom/bestv/ott/online/auth/IBesTVAuthService;

.field private mServerConn:Landroid/content/ServiceConnection;

.field serverConn:Landroid/content/ServiceConnection;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    const-string v0, "/data/data/com.bestv.ctv/files/bestv.properties"

    sput-object v0, Lcom/konka/IntelligentControl/ioop/specificOp/BestvOp;->CONFFILE:Ljava/lang/String;

    const/4 v0, 0x0

    sput-object v0, Lcom/konka/IntelligentControl/ioop/specificOp/BestvOp;->content:Ljava/lang/String;

    const-string v0, "userid="

    sput-object v0, Lcom/konka/IntelligentControl/ioop/specificOp/BestvOp;->str:Ljava/lang/String;

    return-void
.end method

.method public constructor <init>(Landroid/content/Context;)V
    .locals 3
    .param p1    # Landroid/content/Context;

    const/4 v1, 0x0

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    iput-object v1, p0, Lcom/konka/IntelligentControl/ioop/specificOp/BestvOp;->loginData:[B

    iput-object v1, p0, Lcom/konka/IntelligentControl/ioop/specificOp/BestvOp;->bestvSerivce:Lcom/bestv/ott/online/auth/IBesTVAuthService;

    iput-object v1, p0, Lcom/konka/IntelligentControl/ioop/specificOp/BestvOp;->serverConn:Landroid/content/ServiceConnection;

    iput-object v1, p0, Lcom/konka/IntelligentControl/ioop/specificOp/BestvOp;->best_useid:Ljava/lang/String;

    iput-object v1, p0, Lcom/konka/IntelligentControl/ioop/specificOp/BestvOp;->mAuthService:Lcom/bestv/ott/online/auth/IBesTVAuthService;

    new-instance v1, Lcom/konka/IntelligentControl/ioop/specificOp/BestvOp$1;

    invoke-direct {v1, p0}, Lcom/konka/IntelligentControl/ioop/specificOp/BestvOp$1;-><init>(Lcom/konka/IntelligentControl/ioop/specificOp/BestvOp;)V

    iput-object v1, p0, Lcom/konka/IntelligentControl/ioop/specificOp/BestvOp;->mServerConn:Landroid/content/ServiceConnection;

    new-instance v0, Landroid/content/Intent;

    const-string v1, "com.bestv.ott.BesTVAuthService"

    invoke-direct {v0, v1}, Landroid/content/Intent;-><init>(Ljava/lang/String;)V

    iget-object v1, p0, Lcom/konka/IntelligentControl/ioop/specificOp/BestvOp;->mServerConn:Landroid/content/ServiceConnection;

    const/4 v2, 0x1

    invoke-virtual {p1, v0, v1, v2}, Landroid/content/Context;->bindService(Landroid/content/Intent;Landroid/content/ServiceConnection;I)Z

    return-void
.end method

.method public static ReadBestvInfoBuffer(Ljava/lang/String;)Ljava/lang/String;
    .locals 8
    .param p0    # Ljava/lang/String;

    new-instance v1, Ljava/io/File;

    sget-object v5, Lcom/konka/IntelligentControl/ioop/specificOp/BestvOp;->CONFFILE:Ljava/lang/String;

    invoke-direct {v1, v5}, Ljava/io/File;-><init>(Ljava/lang/String;)V

    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    sget-object v5, Ljava/lang/System;->out:Ljava/io/PrintStream;

    const-string v6, "\u4ecebestv\u4e2d\u8bfb\u53d6\u6570\u63441\ufffd7"

    invoke-virtual {v5, v6}, Ljava/io/PrintStream;->println(Ljava/lang/String;)V

    invoke-virtual {v1}, Ljava/io/File;->exists()Z

    move-result v5

    if-eqz v5, :cond_1

    invoke-virtual {v1}, Ljava/io/File;->isFile()Z

    move-result v5

    if-eqz v5, :cond_1

    :try_start_0
    sget-object v5, Ljava/lang/System;->out:Ljava/io/PrintStream;

    const-string v6, "bestv.conf\u6587\u4ef6\u5185\u5bb9\u4e3a\uff1a\r\n"

    invoke-virtual {v5, v6}, Ljava/io/PrintStream;->println(Ljava/lang/String;)V

    new-instance v3, Ljava/io/FileReader;

    invoke-direct {v3, v1}, Ljava/io/FileReader;-><init>(Ljava/io/File;)V

    new-instance v0, Ljava/io/BufferedReader;

    invoke-direct {v0, v3}, Ljava/io/BufferedReader;-><init>(Ljava/io/Reader;)V

    :goto_0
    invoke-virtual {v0}, Ljava/io/BufferedReader;->readLine()Ljava/lang/String;

    move-result-object p0

    if-nez p0, :cond_0

    invoke-virtual {v0}, Ljava/io/BufferedReader;->close()V

    invoke-virtual {v3}, Ljava/io/FileReader;->close()V
    :try_end_0
    .catch Ljava/io/FileNotFoundException; {:try_start_0 .. :try_end_0} :catch_0
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_1

    :goto_1
    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    return-object v5

    :cond_0
    :try_start_1
    sget-object v5, Ljava/lang/System;->out:Ljava/io/PrintStream;

    invoke-virtual {v5, p0}, Ljava/io/PrintStream;->println(Ljava/lang/String;)V

    new-instance v5, Ljava/lang/StringBuilder;

    invoke-static {p0}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v6

    invoke-direct {v5, v6}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    const-string v6, "\r\n"

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
    :try_end_1
    .catch Ljava/io/FileNotFoundException; {:try_start_1 .. :try_end_1} :catch_0
    .catch Ljava/io/IOException; {:try_start_1 .. :try_end_1} :catch_1

    goto :goto_0

    :catch_0
    move-exception v2

    sget-object v5, Ljava/lang/System;->out:Ljava/io/PrintStream;

    new-instance v6, Ljava/lang/StringBuilder;

    const-string v7, "\u627e\u4e0d\u5230\u6587\u4ef6\uff0c\u9519\u8bef\u4fe1\u606f\u4e3a\uff1a"

    invoke-direct {v6, v7}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v2}, Ljava/io/FileNotFoundException;->getMessage()Ljava/lang/String;

    move-result-object v7

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v6}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v6

    invoke-virtual {v5, v6}, Ljava/io/PrintStream;->println(Ljava/lang/String;)V

    goto :goto_1

    :catch_1
    move-exception v2

    invoke-virtual {v2}, Ljava/io/IOException;->printStackTrace()V

    goto :goto_1

    :cond_1
    sget-object v5, Ljava/lang/System;->out:Ljava/io/PrintStream;

    const-string v6, "bestv.conf\u6587\u4ef6\u4e0d\u5b58\u57041\ufffd7"

    invoke-virtual {v5, v6}, Ljava/io/PrintStream;->println(Ljava/lang/String;)V

    goto :goto_1
.end method

.method public static RootCommand(Ljava/lang/String;)Z
    .locals 1
    .param p0    # Ljava/lang/String;

    const/4 v0, 0x1

    return v0
.end method

.method static synthetic access$0(Lcom/konka/IntelligentControl/ioop/specificOp/BestvOp;Lcom/bestv/ott/online/auth/IBesTVAuthService;)V
    .locals 0

    iput-object p1, p0, Lcom/konka/IntelligentControl/ioop/specificOp/BestvOp;->mAuthService:Lcom/bestv/ott/online/auth/IBesTVAuthService;

    return-void
.end method

.method static synthetic access$1(Lcom/konka/IntelligentControl/ioop/specificOp/BestvOp;)Lcom/bestv/ott/online/auth/IBesTVAuthService;
    .locals 1

    iget-object v0, p0, Lcom/konka/IntelligentControl/ioop/specificOp/BestvOp;->mAuthService:Lcom/bestv/ott/online/auth/IBesTVAuthService;

    return-object v0
.end method


# virtual methods
.method public bestvAuth()Ljava/lang/String;
    .locals 2

    sget-object v0, Ljava/lang/System;->out:Ljava/io/PrintStream;

    const-string v1, "4444444444444 = "

    invoke-virtual {v0, v1}, Ljava/io/PrintStream;->println(Ljava/lang/String;)V

    new-instance v0, Lcom/konka/IntelligentControl/ioop/specificOp/BestvOp$2;

    invoke-direct {v0, p0}, Lcom/konka/IntelligentControl/ioop/specificOp/BestvOp$2;-><init>(Lcom/konka/IntelligentControl/ioop/specificOp/BestvOp;)V

    iput-object v0, p0, Lcom/konka/IntelligentControl/ioop/specificOp/BestvOp;->serverConn:Landroid/content/ServiceConnection;

    const/4 v0, 0x0

    return-object v0
.end method
