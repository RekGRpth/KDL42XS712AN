.class Lcom/konka/IntelligentControl/ioop/specificOp/BestvOp$2;
.super Ljava/lang/Object;
.source "BestvOp.java"

# interfaces
.implements Landroid/content/ServiceConnection;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/konka/IntelligentControl/ioop/specificOp/BestvOp;->bestvAuth()Ljava/lang/String;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/konka/IntelligentControl/ioop/specificOp/BestvOp;


# direct methods
.method constructor <init>(Lcom/konka/IntelligentControl/ioop/specificOp/BestvOp;)V
    .locals 0

    iput-object p1, p0, Lcom/konka/IntelligentControl/ioop/specificOp/BestvOp$2;->this$0:Lcom/konka/IntelligentControl/ioop/specificOp/BestvOp;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onServiceConnected(Landroid/content/ComponentName;Landroid/os/IBinder;)V
    .locals 10
    .param p1    # Landroid/content/ComponentName;
    .param p2    # Landroid/os/IBinder;

    const-string v6, "bestv_Op"

    new-instance v7, Ljava/lang/StringBuilder;

    const-string v8, "enter onServiceConnected("

    invoke-direct {v7, v8}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v7, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v7

    const-string v8, ", "

    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    invoke-virtual {v7, p2}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v7

    const-string v8, ")."

    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    invoke-virtual {v7}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v7

    invoke-static {v6, v7}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    iget-object v6, p0, Lcom/konka/IntelligentControl/ioop/specificOp/BestvOp$2;->this$0:Lcom/konka/IntelligentControl/ioop/specificOp/BestvOp;

    invoke-static {p2}, Lcom/bestv/ott/online/auth/IBesTVAuthService$Stub;->asInterface(Landroid/os/IBinder;)Lcom/bestv/ott/online/auth/IBesTVAuthService;

    move-result-object v7

    iput-object v7, v6, Lcom/konka/IntelligentControl/ioop/specificOp/BestvOp;->bestvSerivce:Lcom/bestv/ott/online/auth/IBesTVAuthService;

    const/16 v5, 0x1388

    :try_start_0
    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v0

    const-string v6, "bestv_Op"

    new-instance v7, Ljava/lang/StringBuilder;

    const-string v8, "$$$==============="

    invoke-direct {v7, v8}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    iget-object v8, p0, Lcom/konka/IntelligentControl/ioop/specificOp/BestvOp$2;->this$0:Lcom/konka/IntelligentControl/ioop/specificOp/BestvOp;

    iget-object v8, v8, Lcom/konka/IntelligentControl/ioop/specificOp/BestvOp;->bestvSerivce:Lcom/bestv/ott/online/auth/IBesTVAuthService;

    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v7

    invoke-virtual {v7}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v7

    invoke-static {v6, v7}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_1

    const/4 v4, 0x1

    const/4 v2, 0x1

    :cond_0
    :try_start_1
    iget-object v6, p0, Lcom/konka/IntelligentControl/ioop/specificOp/BestvOp$2;->this$0:Lcom/konka/IntelligentControl/ioop/specificOp/BestvOp;

    iget-object v6, v6, Lcom/konka/IntelligentControl/ioop/specificOp/BestvOp;->bestvSerivce:Lcom/bestv/ott/online/auth/IBesTVAuthService;

    invoke-interface {v6}, Lcom/bestv/ott/online/auth/IBesTVAuthService;->ifAuthOK()Z
    :try_end_1
    .catch Ljava/lang/Exception; {:try_start_1 .. :try_end_1} :catch_0

    move-result v4

    :goto_0
    if-eqz v4, :cond_3

    :cond_1
    :goto_1
    :try_start_2
    const-string v6, "bestv_Op"

    new-instance v7, Ljava/lang/StringBuilder;

    const-string v8, "$$$======"

    invoke-direct {v7, v8}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    iget-object v8, p0, Lcom/konka/IntelligentControl/ioop/specificOp/BestvOp$2;->this$0:Lcom/konka/IntelligentControl/ioop/specificOp/BestvOp;

    iget-object v8, v8, Lcom/konka/IntelligentControl/ioop/specificOp/BestvOp;->bestvSerivce:Lcom/bestv/ott/online/auth/IBesTVAuthService;

    invoke-interface {v8}, Lcom/bestv/ott/online/auth/IBesTVAuthService;->ifAuthOK()Z

    move-result v8

    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v7

    const-string v8, ","

    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v8

    sub-long/2addr v8, v0

    invoke-virtual {v7, v8, v9}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v7

    const-string v8, ","

    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    invoke-virtual {v7, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v7

    invoke-virtual {v7}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v7

    invoke-static {v6, v7}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    iget-object v6, p0, Lcom/konka/IntelligentControl/ioop/specificOp/BestvOp$2;->this$0:Lcom/konka/IntelligentControl/ioop/specificOp/BestvOp;

    iget-object v6, v6, Lcom/konka/IntelligentControl/ioop/specificOp/BestvOp;->bestvSerivce:Lcom/bestv/ott/online/auth/IBesTVAuthService;

    invoke-interface {v6}, Lcom/bestv/ott/online/auth/IBesTVAuthService;->ifAuthOK()Z

    move-result v6

    if-eqz v6, :cond_2

    iget-object v6, p0, Lcom/konka/IntelligentControl/ioop/specificOp/BestvOp$2;->this$0:Lcom/konka/IntelligentControl/ioop/specificOp/BestvOp;

    iget-object v7, p0, Lcom/konka/IntelligentControl/ioop/specificOp/BestvOp$2;->this$0:Lcom/konka/IntelligentControl/ioop/specificOp/BestvOp;

    iget-object v7, v7, Lcom/konka/IntelligentControl/ioop/specificOp/BestvOp;->bestvSerivce:Lcom/bestv/ott/online/auth/IBesTVAuthService;

    const-string v8, "UserID"

    invoke-interface {v7, v8}, Lcom/bestv/ott/online/auth/IBesTVAuthService;->getProfile(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v7

    iput-object v7, v6, Lcom/konka/IntelligentControl/ioop/specificOp/BestvOp;->best_useid:Ljava/lang/String;

    const-string v6, "bestv_Op"

    const-string v7, "$$$Auth OK"

    invoke-static {v6, v7}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    const-string v6, "bestv_Op"

    new-instance v7, Ljava/lang/StringBuilder;

    const-string v8, "$$$UserID:"

    invoke-direct {v7, v8}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    iget-object v8, p0, Lcom/konka/IntelligentControl/ioop/specificOp/BestvOp$2;->this$0:Lcom/konka/IntelligentControl/ioop/specificOp/BestvOp;

    iget-object v8, v8, Lcom/konka/IntelligentControl/ioop/specificOp/BestvOp;->bestvSerivce:Lcom/bestv/ott/online/auth/IBesTVAuthService;

    const-string v9, "UserID"

    invoke-interface {v8, v9}, Lcom/bestv/ott/online/auth/IBesTVAuthService;->getProfile(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v8

    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    invoke-virtual {v7}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v7

    invoke-static {v6, v7}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    const-string v6, "bestv_Op"

    new-instance v7, Ljava/lang/StringBuilder;

    const-string v8, "$$$UserGroup:"

    invoke-direct {v7, v8}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    iget-object v8, p0, Lcom/konka/IntelligentControl/ioop/specificOp/BestvOp$2;->this$0:Lcom/konka/IntelligentControl/ioop/specificOp/BestvOp;

    iget-object v8, v8, Lcom/konka/IntelligentControl/ioop/specificOp/BestvOp;->bestvSerivce:Lcom/bestv/ott/online/auth/IBesTVAuthService;

    const-string v9, "UserGroup"

    invoke-interface {v8, v9}, Lcom/bestv/ott/online/auth/IBesTVAuthService;->getProfile(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v8

    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    invoke-virtual {v7}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v7

    invoke-static {v6, v7}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    const-string v6, "bestv_Op"

    new-instance v7, Ljava/lang/StringBuilder;

    const-string v8, "$$$IMGSrvAddress:"

    invoke-direct {v7, v8}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    iget-object v8, p0, Lcom/konka/IntelligentControl/ioop/specificOp/BestvOp$2;->this$0:Lcom/konka/IntelligentControl/ioop/specificOp/BestvOp;

    iget-object v8, v8, Lcom/konka/IntelligentControl/ioop/specificOp/BestvOp;->bestvSerivce:Lcom/bestv/ott/online/auth/IBesTVAuthService;

    const-string v9, "IMGSrvAddress"

    invoke-interface {v8, v9}, Lcom/bestv/ott/online/auth/IBesTVAuthService;->getProfile(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v8

    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    invoke-virtual {v7}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v7

    invoke-static {v6, v7}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I
    :try_end_2
    .catch Ljava/lang/Exception; {:try_start_2 .. :try_end_2} :catch_1

    :cond_2
    :goto_2
    const-string v6, "bestv_Op"

    const-string v7, "leave onServiceConnected."

    invoke-static {v6, v7}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    return-void

    :catch_0
    move-exception v3

    :try_start_3
    invoke-virtual {v3}, Ljava/lang/Exception;->printStackTrace()V

    const/4 v4, 0x0

    goto/16 :goto_0

    :cond_3
    const-wide/16 v6, 0x64

    invoke-static {v6, v7}, Ljava/lang/Thread;->sleep(J)V
    :try_end_3
    .catch Ljava/lang/Exception; {:try_start_3 .. :try_end_3} :catch_1

    add-int/lit8 v5, v5, -0x64

    add-int/lit8 v2, v2, 0x1

    if-nez v4, :cond_1

    if-gtz v5, :cond_0

    goto/16 :goto_1

    :catch_1
    move-exception v3

    const-string v6, "bestv_Op"

    const-string v7, "$$$==============="

    invoke-static {v6, v7}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    invoke-virtual {v3}, Ljava/lang/Exception;->printStackTrace()V

    goto :goto_2
.end method

.method public onServiceDisconnected(Landroid/content/ComponentName;)V
    .locals 3
    .param p1    # Landroid/content/ComponentName;

    const-string v0, "SHARELOG"

    const-string v1, "disconnect service"

    invoke-static {v0, v1}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    const-string v0, "bestv_Op"

    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "enter onServiceDisconnected("

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, ")."

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    iget-object v0, p0, Lcom/konka/IntelligentControl/ioop/specificOp/BestvOp$2;->this$0:Lcom/konka/IntelligentControl/ioop/specificOp/BestvOp;

    const/4 v1, 0x0

    iput-object v1, v0, Lcom/konka/IntelligentControl/ioop/specificOp/BestvOp;->bestvSerivce:Lcom/bestv/ott/online/auth/IBesTVAuthService;

    const-string v0, "bestv_Op"

    const-string v1, "leave onServiceDisconnected."

    invoke-static {v0, v1}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    return-void
.end method
