.class public Lcom/konka/IntelligentControl/ioop/specificOp/CSensorOp;
.super Ljava/lang/Object;
.source "CSensorOp.java"


# static fields
.field public static final TAG:Ljava/lang/String; = "CSensorOp"


# direct methods
.method public constructor <init>()V
    .locals 0

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method static convert_to_x(F)I
    .locals 3
    .param p0    # F

    const/4 v0, 0x0

    const/4 v2, 0x0

    cmpg-float v2, p0, v2

    if-gez v2, :cond_0

    neg-float p0, p0

    const/4 v0, 0x1

    :cond_0
    const/high16 v2, 0x46800000    # 16384.0f

    mul-float/2addr v2, p0

    float-to-int v1, v2

    const/high16 v2, 0x10000000

    or-int/2addr v1, v2

    const/4 v2, 0x1

    if-ne v0, v2, :cond_1

    const/high16 v2, -0x80000000

    or-int/2addr v1, v2

    :cond_1
    return v1
.end method

.method static convert_to_y(F)I
    .locals 3
    .param p0    # F

    const/4 v0, 0x0

    const/4 v2, 0x0

    cmpg-float v2, p0, v2

    if-gez v2, :cond_0

    neg-float p0, p0

    const/4 v0, 0x1

    :cond_0
    const/high16 v2, 0x46800000    # 16384.0f

    mul-float/2addr v2, p0

    float-to-int v1, v2

    const/high16 v2, 0x20000000

    or-int/2addr v1, v2

    const/4 v2, 0x1

    if-ne v0, v2, :cond_1

    const/high16 v2, -0x80000000

    or-int/2addr v1, v2

    :cond_1
    return v1
.end method

.method static convert_to_z(F)I
    .locals 3
    .param p0    # F

    const/4 v0, 0x0

    const/4 v2, 0x0

    cmpg-float v2, p0, v2

    if-gez v2, :cond_0

    neg-float p0, p0

    const/4 v0, 0x1

    :cond_0
    const/high16 v2, 0x46800000    # 16384.0f

    mul-float/2addr v2, p0

    float-to-int v1, v2

    const/high16 v2, 0x40000000    # 2.0f

    or-int/2addr v1, v2

    const/4 v2, 0x1

    if-ne v0, v2, :cond_1

    const/high16 v2, -0x80000000

    or-int/2addr v1, v2

    :cond_1
    return v1
.end method

.method public static writeSensorBuffer(SSSS)V
    .locals 3
    .param p0    # S
    .param p1    # S
    .param p2    # S
    .param p3    # S

    const-string v0, "Sensor_fd: ------>"

    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, " 1 "

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v1, p0}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    invoke-static {p1, p2, p3}, Lcom/konka/IntelligentControl/ioop/fileOp/CFileOper;->writeSensorJniData(III)V

    const-string v0, "Sensor_fd: ------>"

    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, " 2 "

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v1, p0}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    return-void
.end method
