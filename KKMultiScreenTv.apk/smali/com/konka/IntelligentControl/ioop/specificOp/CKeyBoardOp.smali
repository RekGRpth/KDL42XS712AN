.class public Lcom/konka/IntelligentControl/ioop/specificOp/CKeyBoardOp;
.super Ljava/lang/Object;
.source "CKeyBoardOp.java"


# static fields
.field public static final TAG:Ljava/lang/String; = "CKeyBoardOp"

.field public static mInstrumentation:Landroid/app/Instrumentation;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    new-instance v0, Landroid/app/Instrumentation;

    invoke-direct {v0}, Landroid/app/Instrumentation;-><init>()V

    sput-object v0, Lcom/konka/IntelligentControl/ioop/specificOp/CKeyBoardOp;->mInstrumentation:Landroid/app/Instrumentation;

    return-void
.end method

.method public constructor <init>()V
    .locals 0

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method public static writeKeyBoardBuffer(I)V
    .locals 10
    .param p0    # I

    const/4 v9, 0x3

    const/4 v8, 0x2

    const/4 v7, 0x4

    const/4 v6, 0x1

    const/4 v5, 0x0

    const-string v2, "CKeyBoardOp"

    new-instance v3, Ljava/lang/StringBuilder;

    const-string v4, "keyboard key value: "

    invoke-direct {v3, v4}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v3, p0}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v2, v3}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    const v2, 0xf000

    and-int/2addr v2, p0

    shr-int/lit8 v2, v2, 0xc

    int-to-short v0, v2

    if-lez v0, :cond_3

    and-int/lit16 v2, p0, 0xfff

    int-to-short v1, v2

    if-ne v0, v6, :cond_1

    const-string v2, "CKeyBoardOp"

    new-instance v3, Ljava/lang/StringBuilder;

    const-string v4, "keyboard down key value: "

    invoke-direct {v3, v4}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v3, p0}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v3

    const-string v4, " "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v2, v3}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    sget-object v2, Lcom/konka/IntelligentControl/ioop/specificOp/CKeyBoardOp;->mInstrumentation:Landroid/app/Instrumentation;

    new-instance v3, Landroid/view/KeyEvent;

    invoke-direct {v3, v5, v1}, Landroid/view/KeyEvent;-><init>(II)V

    invoke-virtual {v2, v3}, Landroid/app/Instrumentation;->sendKeySync(Landroid/view/KeyEvent;)V

    :cond_0
    :goto_0
    return-void

    :cond_1
    if-ne v0, v8, :cond_2

    const-string v2, "CKeyBoardOp"

    new-instance v3, Ljava/lang/StringBuilder;

    const-string v4, "keyboard up key value: "

    invoke-direct {v3, v4}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v3, p0}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v3

    const-string v4, " "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v2, v3}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    sget-object v2, Lcom/konka/IntelligentControl/ioop/specificOp/CKeyBoardOp;->mInstrumentation:Landroid/app/Instrumentation;

    new-instance v3, Landroid/view/KeyEvent;

    invoke-direct {v3, v6, v1}, Landroid/view/KeyEvent;-><init>(II)V

    invoke-virtual {v2, v3}, Landroid/app/Instrumentation;->sendKeySync(Landroid/view/KeyEvent;)V

    goto :goto_0

    :cond_2
    if-ne v0, v9, :cond_0

    const-string v2, "CKeyBoardOp"

    new-instance v3, Ljava/lang/StringBuilder;

    const-string v4, "keyboard downing key value: "

    invoke-direct {v3, v4}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v3, p0}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v3

    const-string v4, " "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v2, v3}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_0

    :cond_3
    sget-object v2, Lcom/konka/IntelligentControl/ioop/fileOp/CDevPacketInfo;->ievent:[Lcom/konka/IntelligentControl/ioop/constDevdef/CInputEvent32;

    aget-object v2, v2, v5

    invoke-virtual {v2, v7, v7, v5}, Lcom/konka/IntelligentControl/ioop/constDevdef/CInputEvent32;->setValue(SSI)[B

    sget-object v2, Lcom/konka/IntelligentControl/ioop/fileOp/CDevPacketInfo;->ievent:[Lcom/konka/IntelligentControl/ioop/constDevdef/CInputEvent32;

    aget-object v2, v2, v6

    int-to-short v3, p0

    invoke-virtual {v2, v3, v6, v6}, Lcom/konka/IntelligentControl/ioop/constDevdef/CInputEvent32;->setValue(SSI)[B

    sget-object v2, Lcom/konka/IntelligentControl/ioop/fileOp/CDevPacketInfo;->ievent:[Lcom/konka/IntelligentControl/ioop/constDevdef/CInputEvent32;

    aget-object v2, v2, v8

    invoke-virtual {v2, v5, v5, v5}, Lcom/konka/IntelligentControl/ioop/constDevdef/CInputEvent32;->setValue(SSI)[B

    sget-object v2, Lcom/konka/IntelligentControl/ioop/fileOp/CDevPacketInfo;->ievent:[Lcom/konka/IntelligentControl/ioop/constDevdef/CInputEvent32;

    aget-object v2, v2, v5

    iget-object v2, v2, Lcom/konka/IntelligentControl/ioop/constDevdef/CInputEvent32;->InputEvent:[B

    invoke-static {}, Lcom/konka/IntelligentControl/ioop/fileOp/CFileOper;->getkeyboardFd()Ljava/io/DataOutputStream;

    move-result-object v3

    invoke-static {v2, v3}, Lcom/konka/IntelligentControl/ioop/fileOp/CFileOper;->writeData([BLjava/io/DataOutputStream;)V

    sget-object v2, Lcom/konka/IntelligentControl/ioop/fileOp/CDevPacketInfo;->ievent:[Lcom/konka/IntelligentControl/ioop/constDevdef/CInputEvent32;

    aget-object v2, v2, v6

    iget-object v2, v2, Lcom/konka/IntelligentControl/ioop/constDevdef/CInputEvent32;->InputEvent:[B

    invoke-static {}, Lcom/konka/IntelligentControl/ioop/fileOp/CFileOper;->getkeyboardFd()Ljava/io/DataOutputStream;

    move-result-object v3

    invoke-static {v2, v3}, Lcom/konka/IntelligentControl/ioop/fileOp/CFileOper;->writeData([BLjava/io/DataOutputStream;)V

    sget-object v2, Lcom/konka/IntelligentControl/ioop/fileOp/CDevPacketInfo;->ievent:[Lcom/konka/IntelligentControl/ioop/constDevdef/CInputEvent32;

    aget-object v2, v2, v8

    iget-object v2, v2, Lcom/konka/IntelligentControl/ioop/constDevdef/CInputEvent32;->InputEvent:[B

    invoke-static {}, Lcom/konka/IntelligentControl/ioop/fileOp/CFileOper;->getkeyboardFd()Ljava/io/DataOutputStream;

    move-result-object v3

    invoke-static {v2, v3}, Lcom/konka/IntelligentControl/ioop/fileOp/CFileOper;->writeData([BLjava/io/DataOutputStream;)V

    sget-object v2, Lcom/konka/IntelligentControl/ioop/fileOp/CDevPacketInfo;->ievent:[Lcom/konka/IntelligentControl/ioop/constDevdef/CInputEvent32;

    aget-object v2, v2, v9

    invoke-virtual {v2, v7, v7, v5}, Lcom/konka/IntelligentControl/ioop/constDevdef/CInputEvent32;->setValue(SSI)[B

    sget-object v2, Lcom/konka/IntelligentControl/ioop/fileOp/CDevPacketInfo;->ievent:[Lcom/konka/IntelligentControl/ioop/constDevdef/CInputEvent32;

    aget-object v2, v2, v7

    int-to-short v3, p0

    invoke-virtual {v2, v3, v6, v5}, Lcom/konka/IntelligentControl/ioop/constDevdef/CInputEvent32;->setValue(SSI)[B

    sget-object v2, Lcom/konka/IntelligentControl/ioop/fileOp/CDevPacketInfo;->ievent:[Lcom/konka/IntelligentControl/ioop/constDevdef/CInputEvent32;

    const/4 v3, 0x5

    aget-object v2, v2, v3

    invoke-virtual {v2, v5, v5, v5}, Lcom/konka/IntelligentControl/ioop/constDevdef/CInputEvent32;->setValue(SSI)[B

    sget-object v2, Lcom/konka/IntelligentControl/ioop/fileOp/CDevPacketInfo;->ievent:[Lcom/konka/IntelligentControl/ioop/constDevdef/CInputEvent32;

    aget-object v2, v2, v9

    iget-object v2, v2, Lcom/konka/IntelligentControl/ioop/constDevdef/CInputEvent32;->InputEvent:[B

    invoke-static {}, Lcom/konka/IntelligentControl/ioop/fileOp/CFileOper;->getkeyboardFd()Ljava/io/DataOutputStream;

    move-result-object v3

    invoke-static {v2, v3}, Lcom/konka/IntelligentControl/ioop/fileOp/CFileOper;->writeData([BLjava/io/DataOutputStream;)V

    sget-object v2, Lcom/konka/IntelligentControl/ioop/fileOp/CDevPacketInfo;->ievent:[Lcom/konka/IntelligentControl/ioop/constDevdef/CInputEvent32;

    aget-object v2, v2, v7

    iget-object v2, v2, Lcom/konka/IntelligentControl/ioop/constDevdef/CInputEvent32;->InputEvent:[B

    invoke-static {}, Lcom/konka/IntelligentControl/ioop/fileOp/CFileOper;->getkeyboardFd()Ljava/io/DataOutputStream;

    move-result-object v3

    invoke-static {v2, v3}, Lcom/konka/IntelligentControl/ioop/fileOp/CFileOper;->writeData([BLjava/io/DataOutputStream;)V

    sget-object v2, Lcom/konka/IntelligentControl/ioop/fileOp/CDevPacketInfo;->ievent:[Lcom/konka/IntelligentControl/ioop/constDevdef/CInputEvent32;

    const/4 v3, 0x5

    aget-object v2, v2, v3

    iget-object v2, v2, Lcom/konka/IntelligentControl/ioop/constDevdef/CInputEvent32;->InputEvent:[B

    invoke-static {}, Lcom/konka/IntelligentControl/ioop/fileOp/CFileOper;->getkeyboardFd()Ljava/io/DataOutputStream;

    move-result-object v3

    invoke-static {v2, v3}, Lcom/konka/IntelligentControl/ioop/fileOp/CFileOper;->writeData([BLjava/io/DataOutputStream;)V

    goto/16 :goto_0
.end method
