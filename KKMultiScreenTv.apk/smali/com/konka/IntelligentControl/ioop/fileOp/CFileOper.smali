.class public Lcom/konka/IntelligentControl/ioop/fileOp/CFileOper;
.super Ljava/lang/Object;
.source "CFileOper.java"


# static fields
.field private static acc_sensor_fd:Ljava/io/DataOutputStream; = null

.field private static final acc_sensor_path:Ljava/lang/String; = "Virtual AccSensor"

.field private static e:Ljava/lang/Throwable; = null

.field protected static fileHandle:Lcom/konka/IntelligentControl/ioop/fileOp/CFileOper; = null

.field protected static fos:Lcom/konka/IntelligentControl/jniInterface/IfileOper; = null

.field private static ir_fd:Ljava/io/DataOutputStream; = null

.field private static final ir_path:Ljava/lang/String; = "Konka Smart TV IR Receiver"

.field private static keyboard_fd:Ljava/io/DataOutputStream; = null

.field private static final keyboard_path:Ljava/lang/String; = "Tskeyboard"

.field private static mouse_fd:Ljava/io/DataOutputStream; = null

.field private static final mouse_path:Ljava/lang/String; = "Vmouse"

.field private static touch_fd:Ljava/io/DataOutputStream; = null

.field private static final touch_path:Ljava/lang/String; = "Vscreentouch"


# direct methods
.method static constructor <clinit>()V
    .locals 2

    const/4 v1, 0x0

    sput-object v1, Lcom/konka/IntelligentControl/ioop/fileOp/CFileOper;->ir_fd:Ljava/io/DataOutputStream;

    sput-object v1, Lcom/konka/IntelligentControl/ioop/fileOp/CFileOper;->keyboard_fd:Ljava/io/DataOutputStream;

    sput-object v1, Lcom/konka/IntelligentControl/ioop/fileOp/CFileOper;->touch_fd:Ljava/io/DataOutputStream;

    sput-object v1, Lcom/konka/IntelligentControl/ioop/fileOp/CFileOper;->mouse_fd:Ljava/io/DataOutputStream;

    sput-object v1, Lcom/konka/IntelligentControl/ioop/fileOp/CFileOper;->acc_sensor_fd:Ljava/io/DataOutputStream;

    sput-object v1, Lcom/konka/IntelligentControl/ioop/fileOp/CFileOper;->fileHandle:Lcom/konka/IntelligentControl/ioop/fileOp/CFileOper;

    new-instance v1, Lcom/konka/IntelligentControl/jniInterface/IfileOper;

    invoke-direct {v1}, Lcom/konka/IntelligentControl/jniInterface/IfileOper;-><init>()V

    sput-object v1, Lcom/konka/IntelligentControl/ioop/fileOp/CFileOper;->fos:Lcom/konka/IntelligentControl/jniInterface/IfileOper;

    new-instance v0, Lcom/konka/IntelligentControl/ioop/fileOp/CFileOper;

    invoke-direct {v0}, Lcom/konka/IntelligentControl/ioop/fileOp/CFileOper;-><init>()V

    return-void
.end method

.method public constructor <init>()V
    .locals 0

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    invoke-direct {p0}, Lcom/konka/IntelligentControl/ioop/fileOp/CFileOper;->vDevInit()V

    sput-object p0, Lcom/konka/IntelligentControl/ioop/fileOp/CFileOper;->fileHandle:Lcom/konka/IntelligentControl/ioop/fileOp/CFileOper;

    return-void
.end method

.method public static closeFd(Ljava/io/DataOutputStream;)V
    .locals 1
    .param p0    # Ljava/io/DataOutputStream;

    if-eqz p0, :cond_0

    :try_start_0
    invoke-virtual {p0}, Ljava/io/DataOutputStream;->flush()V

    invoke-virtual {p0}, Ljava/io/DataOutputStream;->close()V
    :try_end_0
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_0

    :cond_0
    :goto_0
    return-void

    :catch_0
    move-exception v0

    invoke-virtual {v0}, Ljava/io/IOException;->printStackTrace()V

    goto :goto_0
.end method

.method public static getFileHandle()Lcom/konka/IntelligentControl/ioop/fileOp/CFileOper;
    .locals 1

    sget-object v0, Lcom/konka/IntelligentControl/ioop/fileOp/CFileOper;->fileHandle:Lcom/konka/IntelligentControl/ioop/fileOp/CFileOper;

    if-nez v0, :cond_0

    new-instance v0, Lcom/konka/IntelligentControl/ioop/fileOp/CFileOper;

    invoke-direct {v0}, Lcom/konka/IntelligentControl/ioop/fileOp/CFileOper;-><init>()V

    sput-object v0, Lcom/konka/IntelligentControl/ioop/fileOp/CFileOper;->fileHandle:Lcom/konka/IntelligentControl/ioop/fileOp/CFileOper;

    :cond_0
    sget-object v0, Lcom/konka/IntelligentControl/ioop/fileOp/CFileOper;->fileHandle:Lcom/konka/IntelligentControl/ioop/fileOp/CFileOper;

    return-object v0
.end method

.method public static getIrFd()Ljava/io/DataOutputStream;
    .locals 1

    sget-object v0, Lcom/konka/IntelligentControl/ioop/fileOp/CFileOper;->ir_fd:Ljava/io/DataOutputStream;

    return-object v0
.end method

.method public static getkeyboardFd()Ljava/io/DataOutputStream;
    .locals 1

    sget-object v0, Lcom/konka/IntelligentControl/ioop/fileOp/CFileOper;->keyboard_fd:Ljava/io/DataOutputStream;

    return-object v0
.end method

.method public static getmouseFd()Ljava/io/DataOutputStream;
    .locals 1

    sget-object v0, Lcom/konka/IntelligentControl/ioop/fileOp/CFileOper;->mouse_fd:Ljava/io/DataOutputStream;

    return-object v0
.end method

.method public static getsensorFd()Ljava/io/DataOutputStream;
    .locals 1

    sget-object v0, Lcom/konka/IntelligentControl/ioop/fileOp/CFileOper;->acc_sensor_fd:Ljava/io/DataOutputStream;

    return-object v0
.end method

.method public static gettouchFd()Ljava/io/DataOutputStream;
    .locals 1

    sget-object v0, Lcom/konka/IntelligentControl/ioop/fileOp/CFileOper;->touch_fd:Ljava/io/DataOutputStream;

    return-object v0
.end method

.method private static openOptionsDialog()V
    .locals 3

    new-instance v0, Landroid/app/AlertDialog$Builder;

    const/4 v1, 0x0

    invoke-direct {v0, v1}, Landroid/app/AlertDialog$Builder;-><init>(Landroid/content/Context;)V

    const v1, 0x7f040005    # com.konka.kkmultiscreen.R.string.about_title

    invoke-virtual {v0, v1}, Landroid/app/AlertDialog$Builder;->setTitle(I)Landroid/app/AlertDialog$Builder;

    move-result-object v0

    const v1, 0x7f040006    # com.konka.kkmultiscreen.R.string.about_msg

    invoke-virtual {v0, v1}, Landroid/app/AlertDialog$Builder;->setMessage(I)Landroid/app/AlertDialog$Builder;

    move-result-object v0

    const v1, 0x7f040007    # com.konka.kkmultiscreen.R.string.ok_label

    new-instance v2, Lcom/konka/IntelligentControl/ioop/fileOp/CFileOper$1;

    invoke-direct {v2}, Lcom/konka/IntelligentControl/ioop/fileOp/CFileOper$1;-><init>()V

    invoke-virtual {v0, v1, v2}, Landroid/app/AlertDialog$Builder;->setPositiveButton(ILandroid/content/DialogInterface$OnClickListener;)Landroid/app/AlertDialog$Builder;

    move-result-object v0

    const v1, 0x7f040008    # com.konka.kkmultiscreen.R.string.quit_label

    new-instance v2, Lcom/konka/IntelligentControl/ioop/fileOp/CFileOper$2;

    invoke-direct {v2}, Lcom/konka/IntelligentControl/ioop/fileOp/CFileOper$2;-><init>()V

    invoke-virtual {v0, v1, v2}, Landroid/app/AlertDialog$Builder;->setNegativeButton(ILandroid/content/DialogInterface$OnClickListener;)Landroid/app/AlertDialog$Builder;

    move-result-object v0

    invoke-virtual {v0}, Landroid/app/AlertDialog$Builder;->show()Landroid/app/AlertDialog;

    return-void
.end method

.method public static setUpAccDev()V
    .locals 1

    sget-object v0, Lcom/konka/IntelligentControl/ioop/fileOp/CFileOper;->fos:Lcom/konka/IntelligentControl/jniInterface/IfileOper;

    invoke-virtual {v0}, Lcom/konka/IntelligentControl/jniInterface/IfileOper;->setupAccsensorDevice()V

    return-void
.end method

.method private vDevInit()V
    .locals 1

    sget-object v0, Lcom/konka/IntelligentControl/ioop/fileOp/CFileOper;->fos:Lcom/konka/IntelligentControl/jniInterface/IfileOper;

    invoke-virtual {v0}, Lcom/konka/IntelligentControl/jniInterface/IfileOper;->setupIrDevice()V

    const-string v0, "Konka Smart TV IR Receiver"

    invoke-virtual {p0, v0}, Lcom/konka/IntelligentControl/ioop/fileOp/CFileOper;->openFd(Ljava/lang/String;)V

    const-string v0, "Tskeyboard"

    invoke-virtual {p0, v0}, Lcom/konka/IntelligentControl/ioop/fileOp/CFileOper;->openFd(Ljava/lang/String;)V

    const-string v0, "Vscreentouch"

    invoke-virtual {p0, v0}, Lcom/konka/IntelligentControl/ioop/fileOp/CFileOper;->openFd(Ljava/lang/String;)V

    const-string v0, "Vmouse"

    invoke-virtual {p0, v0}, Lcom/konka/IntelligentControl/ioop/fileOp/CFileOper;->openFd(Ljava/lang/String;)V

    const-string v0, "Virtual AccSensor"

    invoke-virtual {p0, v0}, Lcom/konka/IntelligentControl/ioop/fileOp/CFileOper;->openFd(Ljava/lang/String;)V

    return-void
.end method

.method public static writeData([BLjava/io/DataOutputStream;)V
    .locals 3
    .param p0    # [B
    .param p1    # Ljava/io/DataOutputStream;

    if-eqz p1, :cond_0

    array-length v2, p0

    if-lez v2, :cond_0

    :try_start_0
    invoke-virtual {p1, p0}, Ljava/io/DataOutputStream;->write([B)V

    invoke-virtual {p1}, Ljava/io/DataOutputStream;->flush()V
    :try_end_0
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_0

    :cond_0
    :goto_0
    return-void

    :catch_0
    move-exception v0

    invoke-virtual {v0}, Ljava/io/IOException;->printStackTrace()V

    :try_start_1
    invoke-static {p1}, Lcom/konka/IntelligentControl/ioop/fileOp/CFileOper;->closeFd(Ljava/io/DataOutputStream;)V

    invoke-virtual {p1}, Ljava/io/DataOutputStream;->close()V

    invoke-static {p0, p1}, Lcom/konka/IntelligentControl/ioop/fileOp/CFileOper;->writeData([BLjava/io/DataOutputStream;)V
    :try_end_1
    .catch Ljava/io/IOException; {:try_start_1 .. :try_end_1} :catch_1

    goto :goto_0

    :catch_1
    move-exception v1

    invoke-static {}, Lcom/konka/IntelligentControl/ioop/fileOp/CFileOper;->openOptionsDialog()V

    goto :goto_0
.end method

.method public static writeSensorJniData(III)V
    .locals 2
    .param p0    # I
    .param p1    # I
    .param p2    # I

    const-string v0, "-----"

    const-string v1, "1"

    invoke-static {v0, v1}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    sget-object v0, Lcom/konka/IntelligentControl/ioop/fileOp/CFileOper;->fos:Lcom/konka/IntelligentControl/jniInterface/IfileOper;

    invoke-virtual {v0, p0, p1, p2}, Lcom/konka/IntelligentControl/jniInterface/IfileOper;->writeAccsensorDevice(III)V

    const-string v0, "-----"

    const-string v1, "2"

    invoke-static {v0, v1}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    return-void
.end method


# virtual methods
.method public openFd(Ljava/lang/String;)V
    .locals 5
    .param p1    # Ljava/lang/String;

    const-string v2, "--------------"

    new-instance v3, Ljava/lang/StringBuilder;

    const-string v4, "go to open fd file :"

    invoke-direct {v3, v4}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v3, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v2, v3}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    const-string v2, "AccSensor"

    invoke-virtual {p1, v2}, Ljava/lang/String;->indexOf(Ljava/lang/String;)I

    sget-object v2, Lcom/konka/IntelligentControl/ioop/fileOp/CFileOper;->fos:Lcom/konka/IntelligentControl/jniInterface/IfileOper;

    invoke-virtual {v2, p1}, Lcom/konka/IntelligentControl/jniInterface/IfileOper;->openInput(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    :try_start_0
    const-string v2, "IR"

    invoke-virtual {p1, v2}, Ljava/lang/String;->indexOf(Ljava/lang/String;)I

    move-result v2

    if-lez v2, :cond_1

    new-instance v2, Ljava/io/DataOutputStream;

    new-instance v3, Ljava/io/FileOutputStream;

    invoke-direct {v3, v1}, Ljava/io/FileOutputStream;-><init>(Ljava/lang/String;)V

    invoke-direct {v2, v3}, Ljava/io/DataOutputStream;-><init>(Ljava/io/OutputStream;)V

    sput-object v2, Lcom/konka/IntelligentControl/ioop/fileOp/CFileOper;->ir_fd:Ljava/io/DataOutputStream;

    const-string v2, "--------------"

    new-instance v3, Ljava/lang/StringBuilder;

    const-string v4, "file path "

    invoke-direct {v3, v4}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v3, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    const-string v4, " ir_fd: "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    sget-object v4, Lcom/konka/IntelligentControl/ioop/fileOp/CFileOper;->ir_fd:Ljava/io/DataOutputStream;

    invoke-virtual {v4}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    const-string v4, " name "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v2, v3}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    :cond_0
    :goto_0
    return-void

    :cond_1
    const-string v2, "keyboard"

    invoke-virtual {p1, v2}, Ljava/lang/String;->indexOf(Ljava/lang/String;)I

    move-result v2

    if-lez v2, :cond_2

    new-instance v2, Ljava/io/DataOutputStream;

    new-instance v3, Ljava/io/FileOutputStream;

    invoke-direct {v3, v1}, Ljava/io/FileOutputStream;-><init>(Ljava/lang/String;)V

    invoke-direct {v2, v3}, Ljava/io/DataOutputStream;-><init>(Ljava/io/OutputStream;)V

    sput-object v2, Lcom/konka/IntelligentControl/ioop/fileOp/CFileOper;->keyboard_fd:Ljava/io/DataOutputStream;

    const-string v2, "--------------"

    new-instance v3, Ljava/lang/StringBuilder;

    const-string v4, "file path "

    invoke-direct {v3, v4}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v3, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    const-string v4, " keyboard_fd: "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    sget-object v4, Lcom/konka/IntelligentControl/ioop/fileOp/CFileOper;->keyboard_fd:Ljava/io/DataOutputStream;

    invoke-virtual {v4}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    const-string v4, " name "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v2, v3}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    goto :goto_0

    :catch_0
    move-exception v0

    invoke-virtual {v0}, Ljava/lang/Exception;->printStackTrace()V

    goto :goto_0

    :cond_2
    :try_start_1
    const-string v2, "touch"

    invoke-virtual {p1, v2}, Ljava/lang/String;->indexOf(Ljava/lang/String;)I

    move-result v2

    if-lez v2, :cond_3

    new-instance v2, Ljava/io/DataOutputStream;

    new-instance v3, Ljava/io/FileOutputStream;

    invoke-direct {v3, v1}, Ljava/io/FileOutputStream;-><init>(Ljava/lang/String;)V

    invoke-direct {v2, v3}, Ljava/io/DataOutputStream;-><init>(Ljava/io/OutputStream;)V

    sput-object v2, Lcom/konka/IntelligentControl/ioop/fileOp/CFileOper;->touch_fd:Ljava/io/DataOutputStream;

    const-string v2, "--------------"

    new-instance v3, Ljava/lang/StringBuilder;

    const-string v4, "file path "

    invoke-direct {v3, v4}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v3, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    const-string v4, " touch_fd: "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    sget-object v4, Lcom/konka/IntelligentControl/ioop/fileOp/CFileOper;->touch_fd:Ljava/io/DataOutputStream;

    invoke-virtual {v4}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    const-string v4, " name "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v2, v3}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    goto/16 :goto_0

    :cond_3
    const-string v2, "mouse"

    invoke-virtual {p1, v2}, Ljava/lang/String;->indexOf(Ljava/lang/String;)I

    move-result v2

    if-lez v2, :cond_4

    new-instance v2, Ljava/io/DataOutputStream;

    new-instance v3, Ljava/io/FileOutputStream;

    invoke-direct {v3, v1}, Ljava/io/FileOutputStream;-><init>(Ljava/lang/String;)V

    invoke-direct {v2, v3}, Ljava/io/DataOutputStream;-><init>(Ljava/io/OutputStream;)V

    sput-object v2, Lcom/konka/IntelligentControl/ioop/fileOp/CFileOper;->mouse_fd:Ljava/io/DataOutputStream;

    const-string v2, "--------------"

    new-instance v3, Ljava/lang/StringBuilder;

    const-string v4, "file path "

    invoke-direct {v3, v4}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v3, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    const-string v4, " mouse_fd: "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    sget-object v4, Lcom/konka/IntelligentControl/ioop/fileOp/CFileOper;->mouse_fd:Ljava/io/DataOutputStream;

    invoke-virtual {v4}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    const-string v4, " name "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v2, v3}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    goto/16 :goto_0

    :cond_4
    const-string v2, "AccSensor"

    invoke-virtual {p1, v2}, Ljava/lang/String;->indexOf(Ljava/lang/String;)I

    move-result v2

    if-lez v2, :cond_0

    new-instance v2, Ljava/io/DataOutputStream;

    new-instance v3, Ljava/io/FileOutputStream;

    invoke-direct {v3, v1}, Ljava/io/FileOutputStream;-><init>(Ljava/lang/String;)V

    invoke-direct {v2, v3}, Ljava/io/DataOutputStream;-><init>(Ljava/io/OutputStream;)V

    sput-object v2, Lcom/konka/IntelligentControl/ioop/fileOp/CFileOper;->acc_sensor_fd:Ljava/io/DataOutputStream;

    const-string v2, "--------------"

    new-instance v3, Ljava/lang/StringBuilder;

    const-string v4, "file path "

    invoke-direct {v3, v4}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v3, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    const-string v4, " acc_sensor_fd: "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    sget-object v4, Lcom/konka/IntelligentControl/ioop/fileOp/CFileOper;->acc_sensor_fd:Ljava/io/DataOutputStream;

    invoke-virtual {v4}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    const-string v4, " name "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v2, v3}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I
    :try_end_1
    .catch Ljava/lang/Exception; {:try_start_1 .. :try_end_1} :catch_0

    goto/16 :goto_0
.end method
