.class public Lcom/konka/IntelligentControl/ioop/fileOp/CDevPacketInfo;
.super Ljava/lang/Object;
.source "CDevPacketInfo.java"


# static fields
.field public static final MAXEVENTNUM:S = 0xas

.field public static final TAG:Ljava/lang/String; = "CDevPacketInfo"

.field public static ievent:[Lcom/konka/IntelligentControl/ioop/constDevdef/CInputEvent32;


# direct methods
.method static constructor <clinit>()V
    .locals 2

    const/16 v1, 0xa

    new-array v1, v1, [Lcom/konka/IntelligentControl/ioop/constDevdef/CInputEvent32;

    sput-object v1, Lcom/konka/IntelligentControl/ioop/fileOp/CDevPacketInfo;->ievent:[Lcom/konka/IntelligentControl/ioop/constDevdef/CInputEvent32;

    new-instance v0, Lcom/konka/IntelligentControl/ioop/fileOp/CDevPacketInfo;

    invoke-direct {v0}, Lcom/konka/IntelligentControl/ioop/fileOp/CDevPacketInfo;-><init>()V

    return-void
.end method

.method constructor <init>()V
    .locals 3

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    const/4 v0, 0x0

    :goto_0
    sget-object v1, Lcom/konka/IntelligentControl/ioop/fileOp/CDevPacketInfo;->ievent:[Lcom/konka/IntelligentControl/ioop/constDevdef/CInputEvent32;

    array-length v1, v1

    if-lt v0, v1, :cond_0

    return-void

    :cond_0
    sget-object v1, Lcom/konka/IntelligentControl/ioop/fileOp/CDevPacketInfo;->ievent:[Lcom/konka/IntelligentControl/ioop/constDevdef/CInputEvent32;

    new-instance v2, Lcom/konka/IntelligentControl/ioop/constDevdef/CInputEvent32;

    invoke-direct {v2}, Lcom/konka/IntelligentControl/ioop/constDevdef/CInputEvent32;-><init>()V

    aput-object v2, v1, v0

    add-int/lit8 v0, v0, 0x1

    goto :goto_0
.end method

.method public static varargs writeDevBuffer(I[I)[B
    .locals 6
    .param p0    # I
    .param p1    # [I

    const/4 v4, 0x3

    const/4 v2, 0x2

    const/4 v1, 0x1

    const/4 v3, 0x0

    const/16 v0, 0x12c

    if-ne v0, p0, :cond_1

    aget v0, p1, v3

    int-to-short v0, v0

    invoke-static {v0}, Lcom/konka/IntelligentControl/ioop/specificOp/CIRemoteOp;->writeIRBuffer(S)V

    :cond_0
    :goto_0
    const/4 v0, 0x0

    return-object v0

    :cond_1
    const/16 v0, 0x12f

    if-ne v0, p0, :cond_2

    aget v0, p1, v3

    invoke-static {v0}, Lcom/konka/IntelligentControl/ioop/specificOp/CKeyBoardOp;->writeKeyBoardBuffer(I)V

    goto :goto_0

    :cond_2
    const/16 v0, 0x12e

    if-ne v0, p0, :cond_3

    aget v0, p1, v3

    int-to-short v0, v0

    aget v1, p1, v1

    int-to-short v1, v1

    aget v2, p1, v2

    int-to-short v2, v2

    aget v3, p1, v4

    int-to-short v3, v3

    const/4 v4, 0x4

    aget v4, p1, v4

    int-to-short v4, v4

    const/4 v5, 0x5

    aget v5, p1, v5

    int-to-short v5, v5

    invoke-static/range {v0 .. v5}, Lcom/konka/IntelligentControl/ioop/specificOp/CTouchOp;->writeTouchEventBuffer(IIIIII)V

    goto :goto_0

    :cond_3
    const/16 v0, 0x12d

    if-ne v0, p0, :cond_4

    aget v0, p1, v1

    int-to-short v0, v0

    aget v1, p1, v2

    int-to-short v1, v1

    aget v2, p1, v4

    int-to-short v2, v2

    aget v3, p1, v3

    int-to-short v3, v3

    invoke-static {v0, v1, v2, v3}, Lcom/konka/IntelligentControl/ioop/specificOp/CMouseOp;->writeMouseBuffer(SSSS)V

    goto :goto_0

    :cond_4
    const/16 v0, 0x130

    if-ne v0, p0, :cond_0

    aget v0, p1, v3

    int-to-short v0, v0

    aget v1, p1, v1

    int-to-short v1, v1

    aget v2, p1, v2

    int-to-short v2, v2

    aget v3, p1, v4

    int-to-short v3, v3

    invoke-static {v0, v1, v2, v3}, Lcom/konka/IntelligentControl/ioop/specificOp/CSensorOp;->writeSensorBuffer(SSSS)V

    goto :goto_0
.end method
