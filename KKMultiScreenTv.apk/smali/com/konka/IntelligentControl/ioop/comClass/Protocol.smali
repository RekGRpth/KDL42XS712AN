.class public Lcom/konka/IntelligentControl/ioop/comClass/Protocol;
.super Ljava/lang/Object;
.source "Protocol.java"


# static fields
.field public static cpuType:Z

.field public static g_protocol:Lcom/konka/IntelligentControl/ioop/comClass/Protocol;

.field public static sigInst:Z


# direct methods
.method static constructor <clinit>()V
    .locals 2

    const/4 v1, 0x0

    sput-boolean v1, Lcom/konka/IntelligentControl/ioop/comClass/Protocol;->cpuType:Z

    new-instance v0, Lcom/konka/IntelligentControl/ioop/comClass/Protocol;

    invoke-direct {v0}, Lcom/konka/IntelligentControl/ioop/comClass/Protocol;-><init>()V

    sput-object v0, Lcom/konka/IntelligentControl/ioop/comClass/Protocol;->g_protocol:Lcom/konka/IntelligentControl/ioop/comClass/Protocol;

    sput-boolean v1, Lcom/konka/IntelligentControl/ioop/comClass/Protocol;->sigInst:Z

    return-void
.end method

.method public constructor <init>()V
    .locals 8

    const/4 v7, 0x4

    const/4 v6, 0x1

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    sget-boolean v5, Lcom/konka/IntelligentControl/ioop/comClass/Protocol;->sigInst:Z

    if-eqz v5, :cond_0

    :goto_0
    return-void

    :cond_0
    new-array v4, v7, [B

    const/4 v5, 0x0

    aput-byte v6, v4, v5

    const/16 v1, 0xff

    const/4 v3, 0x0

    const/4 v2, 0x0

    const/4 v0, 0x0

    :goto_1
    if-lt v0, v7, :cond_1

    sput-boolean v6, Lcom/konka/IntelligentControl/ioop/comClass/Protocol;->cpuType:Z

    sput-boolean v6, Lcom/konka/IntelligentControl/ioop/comClass/Protocol;->sigInst:Z

    goto :goto_0

    :cond_1
    shl-int/lit8 v2, v2, 0x8

    aget-byte v5, v4, v0

    and-int v3, v5, v1

    or-int/2addr v2, v3

    add-int/lit8 v0, v0, 0x1

    goto :goto_1
.end method

.method public static Printf([BZ)Ljava/lang/String;
    .locals 4
    .param p0    # [B
    .param p1    # Z

    const/4 v3, 0x2

    new-array v1, v3, [B

    const/4 v2, 0x0

    invoke-static {p0, v3, v1, v2, v3}, Ljava/lang/System;->arraycopy(Ljava/lang/Object;ILjava/lang/Object;II)V

    invoke-static {v1, p1}, Lcom/konka/IntelligentControl/ioop/comClass/Protocol;->byteToShort([BZ)S

    move-result v0

    const/4 v2, 0x0

    return-object v2
.end method

.method public static byteToInt([B)I
    .locals 10
    .param p0    # [B

    const/4 v9, 0x3

    const/4 v8, 0x2

    const/4 v7, 0x1

    const/4 v6, 0x0

    const/4 v0, 0x0

    const/4 v1, 0x0

    const/4 v2, 0x0

    const/4 v3, 0x0

    const/4 v4, 0x0

    sget-boolean v5, Lcom/konka/IntelligentControl/ioop/comClass/Protocol;->cpuType:Z

    if-nez v5, :cond_0

    aget-byte v5, p0, v9

    and-int/lit16 v1, v5, 0xff

    aget-byte v5, p0, v8

    and-int/lit16 v2, v5, 0xff

    aget-byte v5, p0, v7

    and-int/lit16 v3, v5, 0xff

    aget-byte v5, p0, v6

    and-int/lit16 v4, v5, 0xff

    :goto_0
    shl-int/lit8 v4, v4, 0x18

    shl-int/lit8 v3, v3, 0x10

    shl-int/lit8 v2, v2, 0x8

    or-int v5, v1, v2

    or-int/2addr v5, v3

    or-int v0, v5, v4

    return v0

    :cond_0
    aget-byte v5, p0, v6

    and-int/lit16 v1, v5, 0xff

    aget-byte v5, p0, v7

    and-int/lit16 v2, v5, 0xff

    aget-byte v5, p0, v8

    and-int/lit16 v3, v5, 0xff

    aget-byte v5, p0, v9

    and-int/lit16 v4, v5, 0xff

    goto :goto_0
.end method

.method public static byteToShort([BZ)S
    .locals 6
    .param p0    # [B
    .param p1    # Z

    const/4 v5, 0x1

    const/4 v4, 0x0

    const/4 v0, 0x0

    const/4 v1, 0x0

    const/4 v2, 0x0

    if-eqz p1, :cond_0

    aget-byte v3, p0, v4

    and-int/lit16 v3, v3, 0xff

    int-to-short v1, v3

    aget-byte v3, p0, v5

    and-int/lit16 v3, v3, 0xff

    int-to-short v2, v3

    :goto_0
    shl-int/lit8 v3, v2, 0x8

    int-to-short v2, v3

    or-int v3, v1, v2

    int-to-short v0, v3

    return v0

    :cond_0
    aget-byte v3, p0, v5

    and-int/lit16 v3, v3, 0xff

    int-to-short v1, v3

    aget-byte v3, p0, v4

    and-int/lit16 v3, v3, 0xff

    int-to-short v2, v3

    goto :goto_0
.end method

.method public static intToByte(I[BI)V
    .locals 5
    .param p0    # I
    .param p1    # [B
    .param p2    # I

    const/high16 v4, 0xff0000

    const v3, 0xff00

    const/high16 v2, -0x1000000

    sget-boolean v0, Lcom/konka/IntelligentControl/ioop/comClass/Protocol;->cpuType:Z

    if-nez v0, :cond_0

    add-int/lit8 v0, p2, 0x3

    and-int/lit16 v1, p0, 0xff

    int-to-byte v1, v1

    aput-byte v1, p1, v0

    add-int/lit8 v0, p2, 0x2

    and-int v1, v3, p0

    shr-int/lit8 v1, v1, 0x8

    int-to-byte v1, v1

    aput-byte v1, p1, v0

    add-int/lit8 v0, p2, 0x1

    and-int v1, v4, p0

    shr-int/lit8 v1, v1, 0x10

    int-to-byte v1, v1

    aput-byte v1, p1, v0

    add-int/lit8 v0, p2, 0x0

    and-int v1, v2, p0

    shr-int/lit8 v1, v1, 0x18

    int-to-byte v1, v1

    aput-byte v1, p1, v0

    :goto_0
    return-void

    :cond_0
    add-int/lit8 v0, p2, 0x0

    and-int/lit16 v1, p0, 0xff

    int-to-byte v1, v1

    aput-byte v1, p1, v0

    add-int/lit8 v0, p2, 0x1

    and-int v1, v3, p0

    shr-int/lit8 v1, v1, 0x8

    int-to-byte v1, v1

    aput-byte v1, p1, v0

    add-int/lit8 v0, p2, 0x2

    and-int v1, v4, p0

    shr-int/lit8 v1, v1, 0x10

    int-to-byte v1, v1

    aput-byte v1, p1, v0

    add-int/lit8 v0, p2, 0x3

    and-int v1, v2, p0

    shr-int/lit8 v1, v1, 0x18

    int-to-byte v1, v1

    aput-byte v1, p1, v0

    goto :goto_0
.end method

.method public static longToByte(J[BI)V
    .locals 8
    .param p0    # J
    .param p2    # [B
    .param p3    # I

    const-wide/16 v1, 0xff

    const-wide/32 v6, -0x1000000

    const/16 v5, 0x18

    const/16 v4, 0x10

    const/16 v3, 0x8

    sget-boolean v0, Lcom/konka/IntelligentControl/ioop/comClass/Protocol;->cpuType:Z

    if-nez v0, :cond_0

    add-int/lit8 v0, p3, 0x3

    and-long/2addr v1, p0

    long-to-int v1, v1

    int-to-byte v1, v1

    aput-byte v1, p2, v0

    add-int/lit8 v0, p3, 0x2

    const-wide/32 v1, 0xff00

    and-long/2addr v1, p0

    shr-long/2addr v1, v3

    long-to-int v1, v1

    int-to-byte v1, v1

    aput-byte v1, p2, v0

    add-int/lit8 v0, p3, 0x1

    const-wide/32 v1, 0xff0000

    and-long/2addr v1, p0

    shr-long/2addr v1, v4

    long-to-int v1, v1

    int-to-byte v1, v1

    aput-byte v1, p2, v0

    add-int/lit8 v0, p3, 0x0

    and-long v1, v6, p0

    shr-long/2addr v1, v5

    long-to-int v1, v1

    int-to-byte v1, v1

    aput-byte v1, p2, v0

    :goto_0
    return-void

    :cond_0
    add-int/lit8 v0, p3, 0x0

    and-long/2addr v1, p0

    long-to-int v1, v1

    int-to-byte v1, v1

    aput-byte v1, p2, v0

    add-int/lit8 v0, p3, 0x1

    const-wide/32 v1, 0xff00

    and-long/2addr v1, p0

    shr-long/2addr v1, v3

    long-to-int v1, v1

    int-to-byte v1, v1

    aput-byte v1, p2, v0

    add-int/lit8 v0, p3, 0x2

    const-wide/32 v1, 0xff0000

    and-long/2addr v1, p0

    shr-long/2addr v1, v4

    long-to-int v1, v1

    int-to-byte v1, v1

    aput-byte v1, p2, v0

    add-int/lit8 v0, p3, 0x3

    and-long v1, v6, p0

    shr-long/2addr v1, v5

    long-to-int v1, v1

    int-to-byte v1, v1

    aput-byte v1, p2, v0

    goto :goto_0
.end method

.method public static shortToByte(S[BI)V
    .locals 2
    .param p0    # S
    .param p1    # [B
    .param p2    # I

    sget-boolean v0, Lcom/konka/IntelligentControl/ioop/comClass/Protocol;->cpuType:Z

    if-eqz v0, :cond_0

    add-int/lit8 v0, p2, 0x1

    shr-int/lit8 v1, p0, 0x8

    int-to-byte v1, v1

    aput-byte v1, p1, v0

    add-int/lit8 v0, p2, 0x0

    shr-int/lit8 v1, p0, 0x0

    int-to-byte v1, v1

    aput-byte v1, p1, v0

    :goto_0
    return-void

    :cond_0
    add-int/lit8 v0, p2, 0x0

    shr-int/lit8 v1, p0, 0x8

    int-to-byte v1, v1

    aput-byte v1, p1, v0

    add-int/lit8 v0, p2, 0x1

    shr-int/lit8 v1, p0, 0x0

    int-to-byte v1, v1

    aput-byte v1, p1, v0

    goto :goto_0
.end method
