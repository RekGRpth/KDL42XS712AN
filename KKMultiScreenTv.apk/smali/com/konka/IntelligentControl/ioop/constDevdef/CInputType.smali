.class public Lcom/konka/IntelligentControl/ioop/constDevdef/CInputType;
.super Ljava/lang/Object;
.source "CInputType.java"


# static fields
.field public static final ABS_MT_POSITION_X:S = 0x35s

.field public static final ABS_MT_POSITION_Y:S = 0x36s

.field public static final ABS_PRESSURE:S = 0x18s

.field public static final ABS_X:S = 0x0s

.field public static final ABS_Y:S = 0x1s

.field public static final BTN_TOUCH:S = 0x14as

.field public static final CTL_TYPE_BESTV_LOGIN:I = 0x3000

.field public static final DECIMAL_POINT_BIT:S = 0x4000s

.field public static final EV_ABS:S = 0x3s

.field public static final EV_KEY:S = 0x1s

.field public static final EV_MSC:S = 0x4s

.field public static final EV_REL:S = 0x2s

.field public static final EV_SYN:S = 0x0s

.field public static final IRTYPE:I = 0x12c

.field public static final KEYBOARDTYPE:I = 0x12f

.field public static final MAX_CDATA_LEN:I = 0x100

.field public static final MINUS_MASK:I = -0x80000000

.field public static final MOUSETYPE:I = 0x12d

.field public static final MSC_SCAN:S = 0x4s

.field public static final SENSORTYPE:I = 0x130

.field public static final SYN_MT_REPORT:S = 0x2s

.field public static final SYN_REPORT:S = 0x0s

.field public static final TOUCHTYPE:I = 0x12e

.field public static final X_VALUE_MASK:I = 0x10000000

.field public static final Y_VALUE_MASK:I = 0x20000000

.field public static final Z_VALUE_MASK:I = 0x40000000


# direct methods
.method public constructor <init>()V
    .locals 0

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method
