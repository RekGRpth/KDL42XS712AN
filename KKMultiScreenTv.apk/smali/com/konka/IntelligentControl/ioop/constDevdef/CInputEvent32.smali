.class public Lcom/konka/IntelligentControl/ioop/constDevdef/CInputEvent32;
.super Ljava/lang/Object;
.source "CInputEvent32.java"


# instance fields
.field public InputEvent:[B

.field protected code:S

.field protected tv_sec:J

.field protected tv_usec:J

.field protected type:S

.field protected value:I


# direct methods
.method public constructor <init>()V
    .locals 4

    const-wide/16 v2, 0x0

    const/4 v1, 0x0

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    const/16 v0, 0x10

    new-array v0, v0, [B

    iput-object v0, p0, Lcom/konka/IntelligentControl/ioop/constDevdef/CInputEvent32;->InputEvent:[B

    iput-wide v2, p0, Lcom/konka/IntelligentControl/ioop/constDevdef/CInputEvent32;->tv_sec:J

    iput-wide v2, p0, Lcom/konka/IntelligentControl/ioop/constDevdef/CInputEvent32;->tv_usec:J

    iput-short v1, p0, Lcom/konka/IntelligentControl/ioop/constDevdef/CInputEvent32;->type:S

    iput-short v1, p0, Lcom/konka/IntelligentControl/ioop/constDevdef/CInputEvent32;->code:S

    iput v1, p0, Lcom/konka/IntelligentControl/ioop/constDevdef/CInputEvent32;->value:I

    return-void
.end method


# virtual methods
.method protected format2InputEvent()V
    .locals 3

    sget-object v0, Lcom/konka/IntelligentControl/ioop/comClass/Protocol;->g_protocol:Lcom/konka/IntelligentControl/ioop/comClass/Protocol;

    iget-wide v0, p0, Lcom/konka/IntelligentControl/ioop/constDevdef/CInputEvent32;->tv_sec:J

    long-to-int v0, v0

    iget-object v1, p0, Lcom/konka/IntelligentControl/ioop/constDevdef/CInputEvent32;->InputEvent:[B

    const/4 v2, 0x0

    invoke-static {v0, v1, v2}, Lcom/konka/IntelligentControl/ioop/comClass/Protocol;->intToByte(I[BI)V

    sget-object v0, Lcom/konka/IntelligentControl/ioop/comClass/Protocol;->g_protocol:Lcom/konka/IntelligentControl/ioop/comClass/Protocol;

    iget-wide v0, p0, Lcom/konka/IntelligentControl/ioop/constDevdef/CInputEvent32;->tv_usec:J

    long-to-int v0, v0

    iget-object v1, p0, Lcom/konka/IntelligentControl/ioop/constDevdef/CInputEvent32;->InputEvent:[B

    const/4 v2, 0x4

    invoke-static {v0, v1, v2}, Lcom/konka/IntelligentControl/ioop/comClass/Protocol;->intToByte(I[BI)V

    sget-object v0, Lcom/konka/IntelligentControl/ioop/comClass/Protocol;->g_protocol:Lcom/konka/IntelligentControl/ioop/comClass/Protocol;

    iget-short v0, p0, Lcom/konka/IntelligentControl/ioop/constDevdef/CInputEvent32;->type:S

    iget-object v1, p0, Lcom/konka/IntelligentControl/ioop/constDevdef/CInputEvent32;->InputEvent:[B

    const/16 v2, 0x8

    invoke-static {v0, v1, v2}, Lcom/konka/IntelligentControl/ioop/comClass/Protocol;->shortToByte(S[BI)V

    sget-object v0, Lcom/konka/IntelligentControl/ioop/comClass/Protocol;->g_protocol:Lcom/konka/IntelligentControl/ioop/comClass/Protocol;

    iget-short v0, p0, Lcom/konka/IntelligentControl/ioop/constDevdef/CInputEvent32;->code:S

    iget-object v1, p0, Lcom/konka/IntelligentControl/ioop/constDevdef/CInputEvent32;->InputEvent:[B

    const/16 v2, 0xa

    invoke-static {v0, v1, v2}, Lcom/konka/IntelligentControl/ioop/comClass/Protocol;->shortToByte(S[BI)V

    sget-object v0, Lcom/konka/IntelligentControl/ioop/comClass/Protocol;->g_protocol:Lcom/konka/IntelligentControl/ioop/comClass/Protocol;

    iget v0, p0, Lcom/konka/IntelligentControl/ioop/constDevdef/CInputEvent32;->value:I

    iget-object v1, p0, Lcom/konka/IntelligentControl/ioop/constDevdef/CInputEvent32;->InputEvent:[B

    const/16 v2, 0xc

    invoke-static {v0, v1, v2}, Lcom/konka/IntelligentControl/ioop/comClass/Protocol;->intToByte(I[BI)V

    return-void
.end method

.method public setValue(SSI)[B
    .locals 11
    .param p1    # S
    .param p2    # S
    .param p3    # I

    const-wide/32 v7, 0xea60

    const-wide/16 v9, 0x3e8

    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v2

    const-wide/32 v5, 0x36ee80

    rem-long v5, v2, v5

    div-long v0, v5, v7

    rem-long v5, v2, v7

    div-long/2addr v5, v9

    long-to-int v4, v5

    const-wide/16 v5, 0x3c

    mul-long/2addr v5, v0

    int-to-long v7, v4

    add-long/2addr v5, v7

    iput-wide v5, p0, Lcom/konka/IntelligentControl/ioop/constDevdef/CInputEvent32;->tv_sec:J

    rem-long v5, v2, v9

    long-to-int v5, v5

    int-to-long v5, v5

    iput-wide v5, p0, Lcom/konka/IntelligentControl/ioop/constDevdef/CInputEvent32;->tv_usec:J

    iput-short p2, p0, Lcom/konka/IntelligentControl/ioop/constDevdef/CInputEvent32;->type:S

    iput-short p1, p0, Lcom/konka/IntelligentControl/ioop/constDevdef/CInputEvent32;->code:S

    iput p3, p0, Lcom/konka/IntelligentControl/ioop/constDevdef/CInputEvent32;->value:I

    invoke-virtual {p0}, Lcom/konka/IntelligentControl/ioop/constDevdef/CInputEvent32;->format2InputEvent()V

    iget-object v5, p0, Lcom/konka/IntelligentControl/ioop/constDevdef/CInputEvent32;->InputEvent:[B

    return-object v5
.end method
