.class Lcom/konka/InitApp/CInitAppSys$4;
.super Lnetwork/Interface/ServerSocketObserverAdapter;
.source "CInitAppSys.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/konka/InitApp/CInitAppSys;->InitNetWork()I
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/konka/InitApp/CInitAppSys;


# direct methods
.method constructor <init>(Lcom/konka/InitApp/CInitAppSys;)V
    .locals 0

    iput-object p1, p0, Lcom/konka/InitApp/CInitAppSys$4;->this$0:Lcom/konka/InitApp/CInitAppSys;

    invoke-direct {p0}, Lnetwork/Interface/ServerSocketObserverAdapter;-><init>()V

    return-void
.end method

.method static synthetic access$0(Lcom/konka/InitApp/CInitAppSys$4;)Lcom/konka/InitApp/CInitAppSys;
    .locals 1

    iget-object v0, p0, Lcom/konka/InitApp/CInitAppSys$4;->this$0:Lcom/konka/InitApp/CInitAppSys;

    return-object v0
.end method


# virtual methods
.method public newConnection(Lnetwork/Interface/NIOSocket;)V
    .locals 3
    .param p1    # Lnetwork/Interface/NIOSocket;

    sget-object v0, Ljava/lang/System;->out:Ljava/io/PrintStream;

    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "Client "

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-interface {p1}, Lnetwork/Interface/NIOSocket;->getIp()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, " connected."

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/io/PrintStream;->println(Ljava/lang/String;)V

    new-instance v0, Lcom/konka/InitApp/CInitAppSys$4$1;

    invoke-direct {v0, p0}, Lcom/konka/InitApp/CInitAppSys$4$1;-><init>(Lcom/konka/InitApp/CInitAppSys$4;)V

    invoke-interface {p1, v0}, Lnetwork/Interface/NIOSocket;->listen(Lnetwork/Interface/SocketObserver;)V

    return-void
.end method
