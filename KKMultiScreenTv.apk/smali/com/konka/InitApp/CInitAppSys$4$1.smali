.class Lcom/konka/InitApp/CInitAppSys$4$1;
.super Lnetwork/Interface/SocketObserverAdapter;
.source "CInitAppSys.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/konka/InitApp/CInitAppSys$4;->newConnection(Lnetwork/Interface/NIOSocket;)V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field iCount:I

.field lastTime:J

.field len:J

.field nowTime:J

.field final synthetic this$1:Lcom/konka/InitApp/CInitAppSys$4;


# direct methods
.method constructor <init>(Lcom/konka/InitApp/CInitAppSys$4;)V
    .locals 2

    const-wide/16 v0, -0x1

    iput-object p1, p0, Lcom/konka/InitApp/CInitAppSys$4$1;->this$1:Lcom/konka/InitApp/CInitAppSys$4;

    invoke-direct {p0}, Lnetwork/Interface/SocketObserverAdapter;-><init>()V

    iput-wide v0, p0, Lcom/konka/InitApp/CInitAppSys$4$1;->nowTime:J

    iput-wide v0, p0, Lcom/konka/InitApp/CInitAppSys$4$1;->lastTime:J

    const/4 v0, 0x0

    iput v0, p0, Lcom/konka/InitApp/CInitAppSys$4$1;->iCount:I

    const-wide/16 v0, 0x0

    iput-wide v0, p0, Lcom/konka/InitApp/CInitAppSys$4$1;->len:J

    return-void
.end method


# virtual methods
.method public connectionBroken(Lnetwork/Interface/NIOSocket;Ljava/lang/Exception;)V
    .locals 9
    .param p1    # Lnetwork/Interface/NIOSocket;
    .param p2    # Ljava/lang/Exception;

    const v8, 0x7f040010    # com.konka.kkmultiscreen.R.string.offTV

    sget-object v4, Ljava/lang/System;->out:Ljava/io/PrintStream;

    new-instance v5, Ljava/lang/StringBuilder;

    const-string v6, "Client "

    invoke-direct {v5, v6}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-interface {p1}, Lnetwork/Interface/NIOSocket;->getIp()Ljava/lang/String;

    move-result-object v6

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    const-string v6, " disconnected. "

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {p2}, Ljava/lang/Exception;->getMessage()Ljava/lang/String;

    move-result-object v6

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v4, v5}, Ljava/io/PrintStream;->println(Ljava/lang/String;)V

    iget-object v4, p0, Lcom/konka/InitApp/CInitAppSys$4$1;->this$1:Lcom/konka/InitApp/CInitAppSys$4;

    # getter for: Lcom/konka/InitApp/CInitAppSys$4;->this$0:Lcom/konka/InitApp/CInitAppSys;
    invoke-static {v4}, Lcom/konka/InitApp/CInitAppSys$4;->access$0(Lcom/konka/InitApp/CInitAppSys$4;)Lcom/konka/InitApp/CInitAppSys;

    move-result-object v4

    iget-object v4, v4, Lcom/konka/InitApp/CInitAppSys;->userCenter:LdataLayer/dataCenter/userGroup;

    invoke-virtual {v4, p1}, LdataLayer/dataCenter/userGroup;->closeConnection(Lnetwork/Interface/NIOSocket;)V

    new-instance v3, Landroid/os/Message;

    invoke-direct {v3}, Landroid/os/Message;-><init>()V

    const/16 v4, 0x2718

    iput v4, v3, Landroid/os/Message;->what:I

    new-instance v0, Landroid/os/Bundle;

    invoke-direct {v0}, Landroid/os/Bundle;-><init>()V

    sget-object v4, Lnetwork/udp/BroadThread;->connMap:Ljava/util/Map;

    if-eqz v4, :cond_2

    sget-object v4, Lnetwork/udp/BroadThread;->connMap:Ljava/util/Map;

    invoke-interface {p1}, Lnetwork/Interface/NIOSocket;->getIp()Ljava/lang/String;

    move-result-object v5

    invoke-static {v5}, Lutil/comm/ToolClass;->getStringIpToLong(Ljava/lang/String;)J

    move-result-wide v5

    invoke-static {v5, v6}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v5

    invoke-interface {v4, v5}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Ljava/lang/String;

    if-eqz v2, :cond_0

    const-string v4, "&"

    invoke-virtual {v2, v4}, Ljava/lang/String;->split(Ljava/lang/String;)[Ljava/lang/String;

    move-result-object v1

    array-length v4, v1

    if-lez v4, :cond_1

    array-length v4, v1

    add-int/lit8 v4, v4, -0x1

    aget-object v4, v1, v4

    if-eqz v4, :cond_1

    array-length v4, v1

    add-int/lit8 v4, v4, -0x1

    aget-object v4, v1, v4

    invoke-virtual {v4}, Ljava/lang/String;->length()I

    move-result v4

    const/4 v5, 0x1

    if-le v4, v5, :cond_1

    sget-object v4, Ljava/lang/System;->out:Ljava/io/PrintStream;

    new-instance v5, Ljava/lang/StringBuilder;

    const-string v6, "Client1111111111 "

    invoke-direct {v5, v6}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-interface {p1}, Lnetwork/Interface/NIOSocket;->getIp()Ljava/lang/String;

    move-result-object v6

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    const-string v6, " connected. key : "

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    const-string v6, " | "

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    array-length v6, v1

    add-int/lit8 v6, v6, -0x1

    aget-object v6, v1, v6

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v4, v5}, Ljava/io/PrintStream;->println(Ljava/lang/String;)V

    const-string v4, "MSGINFO:"

    new-instance v5, Ljava/lang/StringBuilder;

    iget-object v6, p0, Lcom/konka/InitApp/CInitAppSys$4$1;->this$1:Lcom/konka/InitApp/CInitAppSys$4;

    # getter for: Lcom/konka/InitApp/CInitAppSys$4;->this$0:Lcom/konka/InitApp/CInitAppSys;
    invoke-static {v6}, Lcom/konka/InitApp/CInitAppSys$4;->access$0(Lcom/konka/InitApp/CInitAppSys$4;)Lcom/konka/InitApp/CInitAppSys;

    move-result-object v6

    # getter for: Lcom/konka/InitApp/CInitAppSys;->action:Landroid/app/Application;
    invoke-static {v6}, Lcom/konka/InitApp/CInitAppSys;->access$0(Lcom/konka/InitApp/CInitAppSys;)Landroid/app/Application;

    move-result-object v6

    invoke-virtual {v6}, Landroid/app/Application;->getApplicationContext()Landroid/content/Context;

    move-result-object v6

    invoke-virtual {v6}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v6

    const v7, 0x7f04000b    # com.konka.kkmultiscreen.R.string.multiScreen_Equipment

    invoke-virtual {v6, v7}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v6

    invoke-static {v6}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v6

    invoke-direct {v5, v6}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    array-length v6, v1

    add-int/lit8 v6, v6, -0x1

    aget-object v6, v1, v6

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    const-string v6, " "

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    iget-object v6, p0, Lcom/konka/InitApp/CInitAppSys$4$1;->this$1:Lcom/konka/InitApp/CInitAppSys$4;

    # getter for: Lcom/konka/InitApp/CInitAppSys$4;->this$0:Lcom/konka/InitApp/CInitAppSys;
    invoke-static {v6}, Lcom/konka/InitApp/CInitAppSys$4;->access$0(Lcom/konka/InitApp/CInitAppSys$4;)Lcom/konka/InitApp/CInitAppSys;

    move-result-object v6

    # getter for: Lcom/konka/InitApp/CInitAppSys;->action:Landroid/app/Application;
    invoke-static {v6}, Lcom/konka/InitApp/CInitAppSys;->access$0(Lcom/konka/InitApp/CInitAppSys;)Landroid/app/Application;

    move-result-object v6

    invoke-virtual {v6}, Landroid/app/Application;->getApplicationContext()Landroid/content/Context;

    move-result-object v6

    invoke-virtual {v6}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v6

    invoke-virtual {v6, v8}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v6

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v0, v4, v5}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V

    :cond_0
    :goto_0
    invoke-virtual {v3, v0}, Landroid/os/Message;->setData(Landroid/os/Bundle;)V

    iget-object v4, p0, Lcom/konka/InitApp/CInitAppSys$4$1;->this$1:Lcom/konka/InitApp/CInitAppSys$4;

    # getter for: Lcom/konka/InitApp/CInitAppSys$4;->this$0:Lcom/konka/InitApp/CInitAppSys;
    invoke-static {v4}, Lcom/konka/InitApp/CInitAppSys$4;->access$0(Lcom/konka/InitApp/CInitAppSys$4;)Lcom/konka/InitApp/CInitAppSys;

    move-result-object v4

    # getter for: Lcom/konka/InitApp/CInitAppSys;->action:Landroid/app/Application;
    invoke-static {v4}, Lcom/konka/InitApp/CInitAppSys;->access$0(Lcom/konka/InitApp/CInitAppSys;)Landroid/app/Application;

    move-result-object v4

    check-cast v4, Lcom/konka/kkmultiscreen/KKMutiScreenTvApp;

    iget-object v4, v4, Lcom/konka/kkmultiscreen/KKMutiScreenTvApp;->mApplicationHandler:Landroid/os/Handler;

    invoke-virtual {v4, v3}, Landroid/os/Handler;->sendMessage(Landroid/os/Message;)Z

    :cond_1
    return-void

    :cond_2
    const-string v4, "MSGINFO:"

    new-instance v5, Ljava/lang/StringBuilder;

    iget-object v6, p0, Lcom/konka/InitApp/CInitAppSys$4$1;->this$1:Lcom/konka/InitApp/CInitAppSys$4;

    # getter for: Lcom/konka/InitApp/CInitAppSys$4;->this$0:Lcom/konka/InitApp/CInitAppSys;
    invoke-static {v6}, Lcom/konka/InitApp/CInitAppSys$4;->access$0(Lcom/konka/InitApp/CInitAppSys$4;)Lcom/konka/InitApp/CInitAppSys;

    move-result-object v6

    # getter for: Lcom/konka/InitApp/CInitAppSys;->action:Landroid/app/Application;
    invoke-static {v6}, Lcom/konka/InitApp/CInitAppSys;->access$0(Lcom/konka/InitApp/CInitAppSys;)Landroid/app/Application;

    move-result-object v6

    invoke-virtual {v6}, Landroid/app/Application;->getApplicationContext()Landroid/content/Context;

    move-result-object v6

    invoke-virtual {v6}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v6

    const v7, 0x7f04000f    # com.konka.kkmultiscreen.R.string.multi_ClientIp

    invoke-virtual {v6, v7}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v6

    invoke-static {v6}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v6

    invoke-direct {v5, v6}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-interface {p1}, Lnetwork/Interface/NIOSocket;->getIp()Ljava/lang/String;

    move-result-object v6

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    const-string v6, " "

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    iget-object v6, p0, Lcom/konka/InitApp/CInitAppSys$4$1;->this$1:Lcom/konka/InitApp/CInitAppSys$4;

    # getter for: Lcom/konka/InitApp/CInitAppSys$4;->this$0:Lcom/konka/InitApp/CInitAppSys;
    invoke-static {v6}, Lcom/konka/InitApp/CInitAppSys$4;->access$0(Lcom/konka/InitApp/CInitAppSys$4;)Lcom/konka/InitApp/CInitAppSys;

    move-result-object v6

    # getter for: Lcom/konka/InitApp/CInitAppSys;->action:Landroid/app/Application;
    invoke-static {v6}, Lcom/konka/InitApp/CInitAppSys;->access$0(Lcom/konka/InitApp/CInitAppSys;)Landroid/app/Application;

    move-result-object v6

    invoke-virtual {v6}, Landroid/app/Application;->getApplicationContext()Landroid/content/Context;

    move-result-object v6

    invoke-virtual {v6}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v6

    invoke-virtual {v6, v8}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v6

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v0, v4, v5}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_0
.end method

.method public connectionOpened(Lnetwork/Interface/NIOSocket;)V
    .locals 10
    .param p1    # Lnetwork/Interface/NIOSocket;

    const v9, 0x7f04000d    # com.konka.kkmultiscreen.R.string.connect_TV

    const/4 v8, 0x1

    iget-object v4, p0, Lcom/konka/InitApp/CInitAppSys$4$1;->this$1:Lcom/konka/InitApp/CInitAppSys$4;

    # getter for: Lcom/konka/InitApp/CInitAppSys$4;->this$0:Lcom/konka/InitApp/CInitAppSys;
    invoke-static {v4}, Lcom/konka/InitApp/CInitAppSys$4;->access$0(Lcom/konka/InitApp/CInitAppSys$4;)Lcom/konka/InitApp/CInitAppSys;

    move-result-object v4

    iget-object v4, v4, Lcom/konka/InitApp/CInitAppSys;->userCenter:LdataLayer/dataCenter/userGroup;

    invoke-virtual {v4, p1}, LdataLayer/dataCenter/userGroup;->newConnection(Lnetwork/Interface/NIOSocket;)V

    new-instance v3, Landroid/os/Message;

    invoke-direct {v3}, Landroid/os/Message;-><init>()V

    const/16 v4, 0x2718

    iput v4, v3, Landroid/os/Message;->what:I

    new-instance v0, Landroid/os/Bundle;

    invoke-direct {v0}, Landroid/os/Bundle;-><init>()V

    sget-object v4, Lnetwork/udp/BroadThread;->connMap:Ljava/util/Map;

    if-eqz v4, :cond_2

    sget-object v4, Lnetwork/udp/BroadThread;->connMap:Ljava/util/Map;

    invoke-interface {p1}, Lnetwork/Interface/NIOSocket;->getIp()Ljava/lang/String;

    move-result-object v5

    invoke-static {v5}, Lutil/comm/ToolClass;->getStringIpToLong(Ljava/lang/String;)J

    move-result-wide v5

    invoke-static {v5, v6}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v5

    invoke-interface {v4, v5}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Ljava/lang/String;

    if-eqz v2, :cond_0

    const-string v4, "&"

    invoke-virtual {v2, v4}, Ljava/lang/String;->split(Ljava/lang/String;)[Ljava/lang/String;

    move-result-object v1

    array-length v4, v1

    if-lez v4, :cond_1

    array-length v4, v1

    add-int/lit8 v4, v4, -0x1

    aget-object v4, v1, v4

    if-eqz v4, :cond_1

    array-length v4, v1

    add-int/lit8 v4, v4, -0x1

    aget-object v4, v1, v4

    invoke-virtual {v4}, Ljava/lang/String;->length()I

    move-result v4

    if-le v4, v8, :cond_1

    sget-object v4, Ljava/lang/System;->out:Ljava/io/PrintStream;

    new-instance v5, Ljava/lang/StringBuilder;

    const-string v6, "Client1111111111 "

    invoke-direct {v5, v6}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-interface {p1}, Lnetwork/Interface/NIOSocket;->getIp()Ljava/lang/String;

    move-result-object v6

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    const-string v6, " connected. key : "

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    const-string v6, " | "

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    array-length v6, v1

    add-int/lit8 v6, v6, -0x1

    aget-object v6, v1, v6

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v4, v5}, Ljava/io/PrintStream;->println(Ljava/lang/String;)V

    const-string v4, "MSGINFO:"

    new-instance v5, Ljava/lang/StringBuilder;

    invoke-direct {v5}, Ljava/lang/StringBuilder;-><init>()V

    iget-object v6, p0, Lcom/konka/InitApp/CInitAppSys$4$1;->this$1:Lcom/konka/InitApp/CInitAppSys$4;

    # getter for: Lcom/konka/InitApp/CInitAppSys$4;->this$0:Lcom/konka/InitApp/CInitAppSys;
    invoke-static {v6}, Lcom/konka/InitApp/CInitAppSys$4;->access$0(Lcom/konka/InitApp/CInitAppSys$4;)Lcom/konka/InitApp/CInitAppSys;

    move-result-object v6

    # getter for: Lcom/konka/InitApp/CInitAppSys;->action:Landroid/app/Application;
    invoke-static {v6}, Lcom/konka/InitApp/CInitAppSys;->access$0(Lcom/konka/InitApp/CInitAppSys;)Landroid/app/Application;

    move-result-object v6

    invoke-virtual {v6}, Landroid/app/Application;->getResources()Landroid/content/res/Resources;

    move-result-object v6

    const v7, 0x7f04000b    # com.konka.kkmultiscreen.R.string.multiScreen_Equipment

    invoke-virtual {v6, v7}, Landroid/content/res/Resources;->getText(I)Ljava/lang/CharSequence;

    move-result-object v6

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v5

    array-length v6, v1

    add-int/lit8 v6, v6, -0x1

    aget-object v6, v1, v6

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    const-string v6, " "

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    iget-object v6, p0, Lcom/konka/InitApp/CInitAppSys$4$1;->this$1:Lcom/konka/InitApp/CInitAppSys$4;

    # getter for: Lcom/konka/InitApp/CInitAppSys$4;->this$0:Lcom/konka/InitApp/CInitAppSys;
    invoke-static {v6}, Lcom/konka/InitApp/CInitAppSys$4;->access$0(Lcom/konka/InitApp/CInitAppSys$4;)Lcom/konka/InitApp/CInitAppSys;

    move-result-object v6

    # getter for: Lcom/konka/InitApp/CInitAppSys;->action:Landroid/app/Application;
    invoke-static {v6}, Lcom/konka/InitApp/CInitAppSys;->access$0(Lcom/konka/InitApp/CInitAppSys;)Landroid/app/Application;

    move-result-object v6

    invoke-virtual {v6}, Landroid/app/Application;->getApplicationContext()Landroid/content/Context;

    move-result-object v6

    invoke-virtual {v6}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v6

    invoke-virtual {v6, v9}, Landroid/content/res/Resources;->getText(I)Ljava/lang/CharSequence;

    move-result-object v6

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v0, v4, v5}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V

    :cond_0
    :goto_0
    invoke-virtual {v3, v0}, Landroid/os/Message;->setData(Landroid/os/Bundle;)V

    iget-object v4, p0, Lcom/konka/InitApp/CInitAppSys$4$1;->this$1:Lcom/konka/InitApp/CInitAppSys$4;

    # getter for: Lcom/konka/InitApp/CInitAppSys$4;->this$0:Lcom/konka/InitApp/CInitAppSys;
    invoke-static {v4}, Lcom/konka/InitApp/CInitAppSys$4;->access$0(Lcom/konka/InitApp/CInitAppSys$4;)Lcom/konka/InitApp/CInitAppSys;

    move-result-object v4

    # getter for: Lcom/konka/InitApp/CInitAppSys;->action:Landroid/app/Application;
    invoke-static {v4}, Lcom/konka/InitApp/CInitAppSys;->access$0(Lcom/konka/InitApp/CInitAppSys;)Landroid/app/Application;

    move-result-object v4

    check-cast v4, Lcom/konka/kkmultiscreen/KKMutiScreenTvApp;

    iput-boolean v8, v4, Lcom/konka/kkmultiscreen/KKMutiScreenTvApp;->showTag:Z

    iget-object v4, p0, Lcom/konka/InitApp/CInitAppSys$4$1;->this$1:Lcom/konka/InitApp/CInitAppSys$4;

    # getter for: Lcom/konka/InitApp/CInitAppSys$4;->this$0:Lcom/konka/InitApp/CInitAppSys;
    invoke-static {v4}, Lcom/konka/InitApp/CInitAppSys$4;->access$0(Lcom/konka/InitApp/CInitAppSys$4;)Lcom/konka/InitApp/CInitAppSys;

    move-result-object v4

    # getter for: Lcom/konka/InitApp/CInitAppSys;->action:Landroid/app/Application;
    invoke-static {v4}, Lcom/konka/InitApp/CInitAppSys;->access$0(Lcom/konka/InitApp/CInitAppSys;)Landroid/app/Application;

    move-result-object v4

    check-cast v4, Lcom/konka/kkmultiscreen/KKMutiScreenTvApp;

    iget-object v4, v4, Lcom/konka/kkmultiscreen/KKMutiScreenTvApp;->mApplicationHandler:Landroid/os/Handler;

    invoke-virtual {v4, v3}, Landroid/os/Handler;->sendMessage(Landroid/os/Message;)Z

    :cond_1
    return-void

    :cond_2
    const-string v4, "MSGINFO:"

    new-instance v5, Ljava/lang/StringBuilder;

    iget-object v6, p0, Lcom/konka/InitApp/CInitAppSys$4$1;->this$1:Lcom/konka/InitApp/CInitAppSys$4;

    # getter for: Lcom/konka/InitApp/CInitAppSys$4;->this$0:Lcom/konka/InitApp/CInitAppSys;
    invoke-static {v6}, Lcom/konka/InitApp/CInitAppSys$4;->access$0(Lcom/konka/InitApp/CInitAppSys$4;)Lcom/konka/InitApp/CInitAppSys;

    move-result-object v6

    # getter for: Lcom/konka/InitApp/CInitAppSys;->action:Landroid/app/Application;
    invoke-static {v6}, Lcom/konka/InitApp/CInitAppSys;->access$0(Lcom/konka/InitApp/CInitAppSys;)Landroid/app/Application;

    move-result-object v6

    invoke-virtual {v6}, Landroid/app/Application;->getApplicationContext()Landroid/content/Context;

    move-result-object v6

    invoke-virtual {v6}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v6

    const v7, 0x7f04000f    # com.konka.kkmultiscreen.R.string.multi_ClientIp

    invoke-virtual {v6, v7}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v6

    invoke-static {v6}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v6

    invoke-direct {v5, v6}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-interface {p1}, Lnetwork/Interface/NIOSocket;->getIp()Ljava/lang/String;

    move-result-object v6

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    const-string v6, " "

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    iget-object v6, p0, Lcom/konka/InitApp/CInitAppSys$4$1;->this$1:Lcom/konka/InitApp/CInitAppSys$4;

    # getter for: Lcom/konka/InitApp/CInitAppSys$4;->this$0:Lcom/konka/InitApp/CInitAppSys;
    invoke-static {v6}, Lcom/konka/InitApp/CInitAppSys$4;->access$0(Lcom/konka/InitApp/CInitAppSys$4;)Lcom/konka/InitApp/CInitAppSys;

    move-result-object v6

    # getter for: Lcom/konka/InitApp/CInitAppSys;->action:Landroid/app/Application;
    invoke-static {v6}, Lcom/konka/InitApp/CInitAppSys;->access$0(Lcom/konka/InitApp/CInitAppSys;)Landroid/app/Application;

    move-result-object v6

    invoke-virtual {v6}, Landroid/app/Application;->getApplicationContext()Landroid/content/Context;

    move-result-object v6

    invoke-virtual {v6}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v6

    invoke-virtual {v6, v9}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v6

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v0, v4, v5}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_0
.end method

.method public packetReceived(Lnetwork/Interface/NIOSocket;[B)V
    .locals 9
    .param p1    # Lnetwork/Interface/NIOSocket;
    .param p2    # [B

    const/4 v8, 0x1

    const/4 v7, 0x0

    iget v1, p0, Lcom/konka/InitApp/CInitAppSys$4$1;->iCount:I

    add-int/lit8 v1, v1, 0x1

    iput v1, p0, Lcom/konka/InitApp/CInitAppSys$4$1;->iCount:I

    iget-wide v1, p0, Lcom/konka/InitApp/CInitAppSys$4$1;->len:J

    array-length v3, p2

    int-to-long v3, v3

    add-long/2addr v1, v3

    iput-wide v1, p0, Lcom/konka/InitApp/CInitAppSys$4$1;->len:J

    iget-wide v1, p0, Lcom/konka/InitApp/CInitAppSys$4$1;->nowTime:J

    const-wide/16 v3, -0x1

    cmp-long v1, v1, v3

    if-nez v1, :cond_2

    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v1

    iput-wide v1, p0, Lcom/konka/InitApp/CInitAppSys$4$1;->nowTime:J

    iget-wide v1, p0, Lcom/konka/InitApp/CInitAppSys$4$1;->nowTime:J

    iput-wide v1, p0, Lcom/konka/InitApp/CInitAppSys$4$1;->lastTime:J

    :cond_0
    :goto_0
    iget-object v1, p0, Lcom/konka/InitApp/CInitAppSys$4$1;->this$1:Lcom/konka/InitApp/CInitAppSys$4;

    # getter for: Lcom/konka/InitApp/CInitAppSys$4;->this$0:Lcom/konka/InitApp/CInitAppSys;
    invoke-static {v1}, Lcom/konka/InitApp/CInitAppSys$4;->access$0(Lcom/konka/InitApp/CInitAppSys$4;)Lcom/konka/InitApp/CInitAppSys;

    move-result-object v1

    invoke-static {v1, v7}, Lcom/konka/InitApp/CInitAppSys;->access$1(Lcom/konka/InitApp/CInitAppSys;I)V

    iget-object v1, p0, Lcom/konka/InitApp/CInitAppSys$4$1;->this$1:Lcom/konka/InitApp/CInitAppSys$4;

    # getter for: Lcom/konka/InitApp/CInitAppSys$4;->this$0:Lcom/konka/InitApp/CInitAppSys;
    invoke-static {v1}, Lcom/konka/InitApp/CInitAppSys$4;->access$0(Lcom/konka/InitApp/CInitAppSys$4;)Lcom/konka/InitApp/CInitAppSys;

    move-result-object v1

    array-length v2, p2

    invoke-static {v1, v2}, Lcom/konka/InitApp/CInitAppSys;->access$2(Lcom/konka/InitApp/CInitAppSys;I)V

    :goto_1
    iget-object v1, p0, Lcom/konka/InitApp/CInitAppSys$4$1;->this$1:Lcom/konka/InitApp/CInitAppSys$4;

    # getter for: Lcom/konka/InitApp/CInitAppSys$4;->this$0:Lcom/konka/InitApp/CInitAppSys;
    invoke-static {v1}, Lcom/konka/InitApp/CInitAppSys$4;->access$0(Lcom/konka/InitApp/CInitAppSys$4;)Lcom/konka/InitApp/CInitAppSys;

    move-result-object v1

    # getter for: Lcom/konka/InitApp/CInitAppSys;->startBuff:I
    invoke-static {v1}, Lcom/konka/InitApp/CInitAppSys;->access$3(Lcom/konka/InitApp/CInitAppSys;)I

    move-result v1

    iget-object v2, p0, Lcom/konka/InitApp/CInitAppSys$4$1;->this$1:Lcom/konka/InitApp/CInitAppSys$4;

    # getter for: Lcom/konka/InitApp/CInitAppSys$4;->this$0:Lcom/konka/InitApp/CInitAppSys;
    invoke-static {v2}, Lcom/konka/InitApp/CInitAppSys$4;->access$0(Lcom/konka/InitApp/CInitAppSys$4;)Lcom/konka/InitApp/CInitAppSys;

    move-result-object v2

    # getter for: Lcom/konka/InitApp/CInitAppSys;->recvDataLen:I
    invoke-static {v2}, Lcom/konka/InitApp/CInitAppSys;->access$4(Lcom/konka/InitApp/CInitAppSys;)I

    move-result v2

    if-lt v1, v2, :cond_3

    :cond_1
    return-void

    :cond_2
    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v1

    iput-wide v1, p0, Lcom/konka/InitApp/CInitAppSys$4$1;->nowTime:J

    iget-wide v1, p0, Lcom/konka/InitApp/CInitAppSys$4$1;->nowTime:J

    iget-wide v3, p0, Lcom/konka/InitApp/CInitAppSys$4$1;->lastTime:J

    sub-long/2addr v1, v3

    const-wide/16 v3, 0x3e8

    cmp-long v1, v1, v3

    if-lez v1, :cond_0

    const-string v1, "CInitAppSys"

    new-instance v2, Ljava/lang/StringBuilder;

    const-string v3, "(nowTime - lastTime) = "

    invoke-direct {v2, v3}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    iget-wide v3, p0, Lcom/konka/InitApp/CInitAppSys$4$1;->nowTime:J

    iget-wide v5, p0, Lcom/konka/InitApp/CInitAppSys$4$1;->lastTime:J

    sub-long/2addr v3, v5

    invoke-virtual {v2, v3, v4}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, ", recv packet: "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    iget v3, p0, Lcom/konka/InitApp/CInitAppSys$4$1;->iCount:I

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, " recv data len: "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    iget-wide v3, p0, Lcom/konka/InitApp/CInitAppSys$4$1;->len:J

    invoke-virtual {v2, v3, v4}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    iput v7, p0, Lcom/konka/InitApp/CInitAppSys$4$1;->iCount:I

    const-wide/16 v1, 0x0

    iput-wide v1, p0, Lcom/konka/InitApp/CInitAppSys$4$1;->len:J

    iget-wide v1, p0, Lcom/konka/InitApp/CInitAppSys$4$1;->nowTime:J

    iput-wide v1, p0, Lcom/konka/InitApp/CInitAppSys$4$1;->lastTime:J

    goto :goto_0

    :cond_3
    iget-object v1, p0, Lcom/konka/InitApp/CInitAppSys$4$1;->this$1:Lcom/konka/InitApp/CInitAppSys$4;

    # getter for: Lcom/konka/InitApp/CInitAppSys$4;->this$0:Lcom/konka/InitApp/CInitAppSys;
    invoke-static {v1}, Lcom/konka/InitApp/CInitAppSys$4;->access$0(Lcom/konka/InitApp/CInitAppSys$4;)Lcom/konka/InitApp/CInitAppSys;

    move-result-object v1

    # getter for: Lcom/konka/InitApp/CInitAppSys;->headLen:[B
    invoke-static {v1}, Lcom/konka/InitApp/CInitAppSys;->access$5(Lcom/konka/InitApp/CInitAppSys;)[B

    move-result-object v1

    aput-byte v7, v1, v7

    iget-object v1, p0, Lcom/konka/InitApp/CInitAppSys$4$1;->this$1:Lcom/konka/InitApp/CInitAppSys$4;

    # getter for: Lcom/konka/InitApp/CInitAppSys$4;->this$0:Lcom/konka/InitApp/CInitAppSys;
    invoke-static {v1}, Lcom/konka/InitApp/CInitAppSys$4;->access$0(Lcom/konka/InitApp/CInitAppSys$4;)Lcom/konka/InitApp/CInitAppSys;

    move-result-object v1

    # getter for: Lcom/konka/InitApp/CInitAppSys;->headLen:[B
    invoke-static {v1}, Lcom/konka/InitApp/CInitAppSys;->access$5(Lcom/konka/InitApp/CInitAppSys;)[B

    move-result-object v1

    aput-byte v7, v1, v8

    iget-object v1, p0, Lcom/konka/InitApp/CInitAppSys$4$1;->this$1:Lcom/konka/InitApp/CInitAppSys$4;

    # getter for: Lcom/konka/InitApp/CInitAppSys$4;->this$0:Lcom/konka/InitApp/CInitAppSys;
    invoke-static {v1}, Lcom/konka/InitApp/CInitAppSys$4;->access$0(Lcom/konka/InitApp/CInitAppSys$4;)Lcom/konka/InitApp/CInitAppSys;

    move-result-object v1

    # getter for: Lcom/konka/InitApp/CInitAppSys;->startBuff:I
    invoke-static {v1}, Lcom/konka/InitApp/CInitAppSys;->access$3(Lcom/konka/InitApp/CInitAppSys;)I

    move-result v1

    iget-object v2, p0, Lcom/konka/InitApp/CInitAppSys$4$1;->this$1:Lcom/konka/InitApp/CInitAppSys$4;

    # getter for: Lcom/konka/InitApp/CInitAppSys$4;->this$0:Lcom/konka/InitApp/CInitAppSys;
    invoke-static {v2}, Lcom/konka/InitApp/CInitAppSys$4;->access$0(Lcom/konka/InitApp/CInitAppSys$4;)Lcom/konka/InitApp/CInitAppSys;

    move-result-object v2

    # getter for: Lcom/konka/InitApp/CInitAppSys;->headLen:[B
    invoke-static {v2}, Lcom/konka/InitApp/CInitAppSys;->access$5(Lcom/konka/InitApp/CInitAppSys;)[B

    move-result-object v2

    iget-object v3, p0, Lcom/konka/InitApp/CInitAppSys$4$1;->this$1:Lcom/konka/InitApp/CInitAppSys$4;

    # getter for: Lcom/konka/InitApp/CInitAppSys$4;->this$0:Lcom/konka/InitApp/CInitAppSys;
    invoke-static {v3}, Lcom/konka/InitApp/CInitAppSys$4;->access$0(Lcom/konka/InitApp/CInitAppSys$4;)Lcom/konka/InitApp/CInitAppSys;

    move-result-object v3

    # getter for: Lcom/konka/InitApp/CInitAppSys;->headLen:[B
    invoke-static {v3}, Lcom/konka/InitApp/CInitAppSys;->access$5(Lcom/konka/InitApp/CInitAppSys;)[B

    move-result-object v3

    array-length v3, v3

    invoke-static {p2, v1, v2, v7, v3}, Ljava/lang/System;->arraycopy(Ljava/lang/Object;ILjava/lang/Object;II)V

    iget-object v1, p0, Lcom/konka/InitApp/CInitAppSys$4$1;->this$1:Lcom/konka/InitApp/CInitAppSys$4;

    # getter for: Lcom/konka/InitApp/CInitAppSys$4;->this$0:Lcom/konka/InitApp/CInitAppSys;
    invoke-static {v1}, Lcom/konka/InitApp/CInitAppSys$4;->access$0(Lcom/konka/InitApp/CInitAppSys$4;)Lcom/konka/InitApp/CInitAppSys;

    move-result-object v1

    iget-object v2, p0, Lcom/konka/InitApp/CInitAppSys$4$1;->this$1:Lcom/konka/InitApp/CInitAppSys$4;

    # getter for: Lcom/konka/InitApp/CInitAppSys$4;->this$0:Lcom/konka/InitApp/CInitAppSys;
    invoke-static {v2}, Lcom/konka/InitApp/CInitAppSys$4;->access$0(Lcom/konka/InitApp/CInitAppSys$4;)Lcom/konka/InitApp/CInitAppSys;

    move-result-object v2

    # getter for: Lcom/konka/InitApp/CInitAppSys;->headLen:[B
    invoke-static {v2}, Lcom/konka/InitApp/CInitAppSys;->access$5(Lcom/konka/InitApp/CInitAppSys;)[B

    move-result-object v2

    invoke-static {v2, v7}, Lcom/konka/IntelligentControl/ioop/comClass/Protocol;->byteToShort([BZ)S

    move-result v2

    invoke-static {v1, v2}, Lcom/konka/InitApp/CInitAppSys;->access$6(Lcom/konka/InitApp/CInitAppSys;I)V

    iget-object v1, p0, Lcom/konka/InitApp/CInitAppSys$4$1;->this$1:Lcom/konka/InitApp/CInitAppSys$4;

    # getter for: Lcom/konka/InitApp/CInitAppSys$4;->this$0:Lcom/konka/InitApp/CInitAppSys;
    invoke-static {v1}, Lcom/konka/InitApp/CInitAppSys$4;->access$0(Lcom/konka/InitApp/CInitAppSys$4;)Lcom/konka/InitApp/CInitAppSys;

    move-result-object v1

    # getter for: Lcom/konka/InitApp/CInitAppSys;->packetLen:I
    invoke-static {v1}, Lcom/konka/InitApp/CInitAppSys;->access$7(Lcom/konka/InitApp/CInitAppSys;)I

    move-result v1

    if-ltz v1, :cond_1

    iget-object v1, p0, Lcom/konka/InitApp/CInitAppSys$4$1;->this$1:Lcom/konka/InitApp/CInitAppSys$4;

    # getter for: Lcom/konka/InitApp/CInitAppSys$4;->this$0:Lcom/konka/InitApp/CInitAppSys;
    invoke-static {v1}, Lcom/konka/InitApp/CInitAppSys$4;->access$0(Lcom/konka/InitApp/CInitAppSys$4;)Lcom/konka/InitApp/CInitAppSys;

    move-result-object v1

    # getter for: Lcom/konka/InitApp/CInitAppSys;->packetLen:I
    invoke-static {v1}, Lcom/konka/InitApp/CInitAppSys;->access$7(Lcom/konka/InitApp/CInitAppSys;)I

    move-result v1

    const v2, 0x8000

    if-gt v1, v2, :cond_1

    iget-object v1, p0, Lcom/konka/InitApp/CInitAppSys$4$1;->this$1:Lcom/konka/InitApp/CInitAppSys$4;

    # getter for: Lcom/konka/InitApp/CInitAppSys$4;->this$0:Lcom/konka/InitApp/CInitAppSys;
    invoke-static {v1}, Lcom/konka/InitApp/CInitAppSys$4;->access$0(Lcom/konka/InitApp/CInitAppSys$4;)Lcom/konka/InitApp/CInitAppSys;

    move-result-object v1

    # getter for: Lcom/konka/InitApp/CInitAppSys;->headLen:[B
    invoke-static {v1}, Lcom/konka/InitApp/CInitAppSys;->access$5(Lcom/konka/InitApp/CInitAppSys;)[B

    move-result-object v1

    aput-byte v7, v1, v7

    iget-object v1, p0, Lcom/konka/InitApp/CInitAppSys$4$1;->this$1:Lcom/konka/InitApp/CInitAppSys$4;

    # getter for: Lcom/konka/InitApp/CInitAppSys$4;->this$0:Lcom/konka/InitApp/CInitAppSys;
    invoke-static {v1}, Lcom/konka/InitApp/CInitAppSys$4;->access$0(Lcom/konka/InitApp/CInitAppSys$4;)Lcom/konka/InitApp/CInitAppSys;

    move-result-object v1

    # getter for: Lcom/konka/InitApp/CInitAppSys;->headLen:[B
    invoke-static {v1}, Lcom/konka/InitApp/CInitAppSys;->access$5(Lcom/konka/InitApp/CInitAppSys;)[B

    move-result-object v1

    aput-byte v7, v1, v8

    iget-object v1, p0, Lcom/konka/InitApp/CInitAppSys$4$1;->this$1:Lcom/konka/InitApp/CInitAppSys$4;

    # getter for: Lcom/konka/InitApp/CInitAppSys$4;->this$0:Lcom/konka/InitApp/CInitAppSys;
    invoke-static {v1}, Lcom/konka/InitApp/CInitAppSys$4;->access$0(Lcom/konka/InitApp/CInitAppSys$4;)Lcom/konka/InitApp/CInitAppSys;

    move-result-object v1

    # getter for: Lcom/konka/InitApp/CInitAppSys;->startBuff:I
    invoke-static {v1}, Lcom/konka/InitApp/CInitAppSys;->access$3(Lcom/konka/InitApp/CInitAppSys;)I

    move-result v1

    add-int/lit8 v1, v1, 0x2

    iget-object v2, p0, Lcom/konka/InitApp/CInitAppSys$4$1;->this$1:Lcom/konka/InitApp/CInitAppSys$4;

    # getter for: Lcom/konka/InitApp/CInitAppSys$4;->this$0:Lcom/konka/InitApp/CInitAppSys;
    invoke-static {v2}, Lcom/konka/InitApp/CInitAppSys$4;->access$0(Lcom/konka/InitApp/CInitAppSys$4;)Lcom/konka/InitApp/CInitAppSys;

    move-result-object v2

    # getter for: Lcom/konka/InitApp/CInitAppSys;->headLen:[B
    invoke-static {v2}, Lcom/konka/InitApp/CInitAppSys;->access$5(Lcom/konka/InitApp/CInitAppSys;)[B

    move-result-object v2

    iget-object v3, p0, Lcom/konka/InitApp/CInitAppSys$4$1;->this$1:Lcom/konka/InitApp/CInitAppSys$4;

    # getter for: Lcom/konka/InitApp/CInitAppSys$4;->this$0:Lcom/konka/InitApp/CInitAppSys;
    invoke-static {v3}, Lcom/konka/InitApp/CInitAppSys$4;->access$0(Lcom/konka/InitApp/CInitAppSys$4;)Lcom/konka/InitApp/CInitAppSys;

    move-result-object v3

    # getter for: Lcom/konka/InitApp/CInitAppSys;->headLen:[B
    invoke-static {v3}, Lcom/konka/InitApp/CInitAppSys;->access$5(Lcom/konka/InitApp/CInitAppSys;)[B

    move-result-object v3

    array-length v3, v3

    invoke-static {p2, v1, v2, v7, v3}, Ljava/lang/System;->arraycopy(Ljava/lang/Object;ILjava/lang/Object;II)V

    iget-object v1, p0, Lcom/konka/InitApp/CInitAppSys$4$1;->this$1:Lcom/konka/InitApp/CInitAppSys$4;

    # getter for: Lcom/konka/InitApp/CInitAppSys$4;->this$0:Lcom/konka/InitApp/CInitAppSys;
    invoke-static {v1}, Lcom/konka/InitApp/CInitAppSys$4;->access$0(Lcom/konka/InitApp/CInitAppSys$4;)Lcom/konka/InitApp/CInitAppSys;

    move-result-object v1

    iget-object v2, p0, Lcom/konka/InitApp/CInitAppSys$4$1;->this$1:Lcom/konka/InitApp/CInitAppSys$4;

    # getter for: Lcom/konka/InitApp/CInitAppSys$4;->this$0:Lcom/konka/InitApp/CInitAppSys;
    invoke-static {v2}, Lcom/konka/InitApp/CInitAppSys$4;->access$0(Lcom/konka/InitApp/CInitAppSys$4;)Lcom/konka/InitApp/CInitAppSys;

    move-result-object v2

    # getter for: Lcom/konka/InitApp/CInitAppSys;->headLen:[B
    invoke-static {v2}, Lcom/konka/InitApp/CInitAppSys;->access$5(Lcom/konka/InitApp/CInitAppSys;)[B

    move-result-object v2

    invoke-static {v2, v7}, Lcom/konka/IntelligentControl/ioop/comClass/Protocol;->byteToShort([BZ)S

    move-result v2

    invoke-static {v1, v2}, Lcom/konka/InitApp/CInitAppSys;->access$8(Lcom/konka/InitApp/CInitAppSys;I)V

    new-instance v0, LdataLayer/dataCenter/DevConcurrentLinkedQueuePool$CmdPacketWriter;

    invoke-direct {v0}, LdataLayer/dataCenter/DevConcurrentLinkedQueuePool$CmdPacketWriter;-><init>()V

    iget-object v1, p0, Lcom/konka/InitApp/CInitAppSys$4$1;->this$1:Lcom/konka/InitApp/CInitAppSys$4;

    # getter for: Lcom/konka/InitApp/CInitAppSys$4;->this$0:Lcom/konka/InitApp/CInitAppSys;
    invoke-static {v1}, Lcom/konka/InitApp/CInitAppSys$4;->access$0(Lcom/konka/InitApp/CInitAppSys$4;)Lcom/konka/InitApp/CInitAppSys;

    move-result-object v1

    # getter for: Lcom/konka/InitApp/CInitAppSys;->packetLen:I
    invoke-static {v1}, Lcom/konka/InitApp/CInitAppSys;->access$7(Lcom/konka/InitApp/CInitAppSys;)I

    move-result v1

    add-int/lit8 v1, v1, 0x4

    new-array v1, v1, [B

    iput-object v1, v0, LdataLayer/dataCenter/DevConcurrentLinkedQueuePool$CmdPacketWriter;->mBuffer:[B

    iget-object v1, p0, Lcom/konka/InitApp/CInitAppSys$4$1;->this$1:Lcom/konka/InitApp/CInitAppSys$4;

    # getter for: Lcom/konka/InitApp/CInitAppSys$4;->this$0:Lcom/konka/InitApp/CInitAppSys;
    invoke-static {v1}, Lcom/konka/InitApp/CInitAppSys$4;->access$0(Lcom/konka/InitApp/CInitAppSys$4;)Lcom/konka/InitApp/CInitAppSys;

    move-result-object v1

    # getter for: Lcom/konka/InitApp/CInitAppSys;->startBuff:I
    invoke-static {v1}, Lcom/konka/InitApp/CInitAppSys;->access$3(Lcom/konka/InitApp/CInitAppSys;)I

    move-result v1

    iget-object v2, v0, LdataLayer/dataCenter/DevConcurrentLinkedQueuePool$CmdPacketWriter;->mBuffer:[B

    iget-object v3, p0, Lcom/konka/InitApp/CInitAppSys$4$1;->this$1:Lcom/konka/InitApp/CInitAppSys$4;

    # getter for: Lcom/konka/InitApp/CInitAppSys$4;->this$0:Lcom/konka/InitApp/CInitAppSys;
    invoke-static {v3}, Lcom/konka/InitApp/CInitAppSys$4;->access$0(Lcom/konka/InitApp/CInitAppSys$4;)Lcom/konka/InitApp/CInitAppSys;

    move-result-object v3

    # getter for: Lcom/konka/InitApp/CInitAppSys;->packetLen:I
    invoke-static {v3}, Lcom/konka/InitApp/CInitAppSys;->access$7(Lcom/konka/InitApp/CInitAppSys;)I

    move-result v3

    add-int/lit8 v3, v3, 0x4

    invoke-static {p2, v1, v2, v7, v3}, Ljava/lang/System;->arraycopy(Ljava/lang/Object;ILjava/lang/Object;II)V

    iget-object v1, p0, Lcom/konka/InitApp/CInitAppSys$4$1;->this$1:Lcom/konka/InitApp/CInitAppSys$4;

    # getter for: Lcom/konka/InitApp/CInitAppSys$4;->this$0:Lcom/konka/InitApp/CInitAppSys;
    invoke-static {v1}, Lcom/konka/InitApp/CInitAppSys$4;->access$0(Lcom/konka/InitApp/CInitAppSys$4;)Lcom/konka/InitApp/CInitAppSys;

    move-result-object v1

    # getter for: Lcom/konka/InitApp/CInitAppSys;->packetCmd:I
    invoke-static {v1}, Lcom/konka/InitApp/CInitAppSys;->access$9(Lcom/konka/InitApp/CInitAppSys;)I

    move-result v1

    const/16 v2, 0x100b

    if-eq v1, v2, :cond_4

    iget-object v1, p0, Lcom/konka/InitApp/CInitAppSys$4$1;->this$1:Lcom/konka/InitApp/CInitAppSys$4;

    # getter for: Lcom/konka/InitApp/CInitAppSys$4;->this$0:Lcom/konka/InitApp/CInitAppSys;
    invoke-static {v1}, Lcom/konka/InitApp/CInitAppSys$4;->access$0(Lcom/konka/InitApp/CInitAppSys$4;)Lcom/konka/InitApp/CInitAppSys;

    move-result-object v1

    # getter for: Lcom/konka/InitApp/CInitAppSys;->packetCmd:I
    invoke-static {v1}, Lcom/konka/InitApp/CInitAppSys;->access$9(Lcom/konka/InitApp/CInitAppSys;)I

    move-result v1

    const/16 v2, 0x1004

    if-eq v1, v2, :cond_4

    iget-object v1, p0, Lcom/konka/InitApp/CInitAppSys$4$1;->this$1:Lcom/konka/InitApp/CInitAppSys$4;

    # getter for: Lcom/konka/InitApp/CInitAppSys$4;->this$0:Lcom/konka/InitApp/CInitAppSys;
    invoke-static {v1}, Lcom/konka/InitApp/CInitAppSys$4;->access$0(Lcom/konka/InitApp/CInitAppSys$4;)Lcom/konka/InitApp/CInitAppSys;

    move-result-object v1

    # getter for: Lcom/konka/InitApp/CInitAppSys;->packetCmd:I
    invoke-static {v1}, Lcom/konka/InitApp/CInitAppSys;->access$9(Lcom/konka/InitApp/CInitAppSys;)I

    move-result v1

    const/16 v2, 0x1005

    if-eq v1, v2, :cond_4

    iget-object v1, p0, Lcom/konka/InitApp/CInitAppSys$4$1;->this$1:Lcom/konka/InitApp/CInitAppSys$4;

    # getter for: Lcom/konka/InitApp/CInitAppSys$4;->this$0:Lcom/konka/InitApp/CInitAppSys;
    invoke-static {v1}, Lcom/konka/InitApp/CInitAppSys$4;->access$0(Lcom/konka/InitApp/CInitAppSys$4;)Lcom/konka/InitApp/CInitAppSys;

    move-result-object v1

    # getter for: Lcom/konka/InitApp/CInitAppSys;->packetCmd:I
    invoke-static {v1}, Lcom/konka/InitApp/CInitAppSys;->access$9(Lcom/konka/InitApp/CInitAppSys;)I

    move-result v1

    const/16 v2, 0x1006

    if-ne v1, v2, :cond_5

    :cond_4
    iget-object v1, p0, Lcom/konka/InitApp/CInitAppSys$4$1;->this$1:Lcom/konka/InitApp/CInitAppSys$4;

    # getter for: Lcom/konka/InitApp/CInitAppSys$4;->this$0:Lcom/konka/InitApp/CInitAppSys;
    invoke-static {v1}, Lcom/konka/InitApp/CInitAppSys$4;->access$0(Lcom/konka/InitApp/CInitAppSys$4;)Lcom/konka/InitApp/CInitAppSys;

    move-result-object v1

    # getter for: Lcom/konka/InitApp/CInitAppSys;->packetCmd:I
    invoke-static {v1}, Lcom/konka/InitApp/CInitAppSys;->access$9(Lcom/konka/InitApp/CInitAppSys;)I

    move-result v1

    iput v1, v0, LdataLayer/dataCenter/DevConcurrentLinkedQueuePool$CmdPacketWriter;->cmd:I

    iget-object v1, p0, Lcom/konka/InitApp/CInitAppSys$4$1;->this$1:Lcom/konka/InitApp/CInitAppSys$4;

    # getter for: Lcom/konka/InitApp/CInitAppSys$4;->this$0:Lcom/konka/InitApp/CInitAppSys;
    invoke-static {v1}, Lcom/konka/InitApp/CInitAppSys$4;->access$0(Lcom/konka/InitApp/CInitAppSys$4;)Lcom/konka/InitApp/CInitAppSys;

    move-result-object v1

    iget-object v1, v1, Lcom/konka/InitApp/CInitAppSys;->devConcurrentLinkedQueuePool:LdataLayer/dataCenter/DevConcurrentLinkedQueuePool;

    invoke-virtual {v1, v8, v0}, LdataLayer/dataCenter/DevConcurrentLinkedQueuePool;->inPoolQueue(ILdataLayer/dataCenter/DevConcurrentLinkedQueuePool$CmdPacketWriter;)V

    :goto_2
    iget-object v1, p0, Lcom/konka/InitApp/CInitAppSys$4$1;->this$1:Lcom/konka/InitApp/CInitAppSys$4;

    # getter for: Lcom/konka/InitApp/CInitAppSys$4;->this$0:Lcom/konka/InitApp/CInitAppSys;
    invoke-static {v1}, Lcom/konka/InitApp/CInitAppSys$4;->access$0(Lcom/konka/InitApp/CInitAppSys$4;)Lcom/konka/InitApp/CInitAppSys;

    move-result-object v1

    # getter for: Lcom/konka/InitApp/CInitAppSys;->startBuff:I
    invoke-static {v1}, Lcom/konka/InitApp/CInitAppSys;->access$3(Lcom/konka/InitApp/CInitAppSys;)I

    move-result v2

    iget-object v3, p0, Lcom/konka/InitApp/CInitAppSys$4$1;->this$1:Lcom/konka/InitApp/CInitAppSys$4;

    # getter for: Lcom/konka/InitApp/CInitAppSys$4;->this$0:Lcom/konka/InitApp/CInitAppSys;
    invoke-static {v3}, Lcom/konka/InitApp/CInitAppSys$4;->access$0(Lcom/konka/InitApp/CInitAppSys$4;)Lcom/konka/InitApp/CInitAppSys;

    move-result-object v3

    # getter for: Lcom/konka/InitApp/CInitAppSys;->packetLen:I
    invoke-static {v3}, Lcom/konka/InitApp/CInitAppSys;->access$7(Lcom/konka/InitApp/CInitAppSys;)I

    move-result v3

    add-int/2addr v2, v3

    invoke-static {v1, v2}, Lcom/konka/InitApp/CInitAppSys;->access$1(Lcom/konka/InitApp/CInitAppSys;I)V

    iget-object v1, p0, Lcom/konka/InitApp/CInitAppSys$4$1;->this$1:Lcom/konka/InitApp/CInitAppSys$4;

    # getter for: Lcom/konka/InitApp/CInitAppSys$4;->this$0:Lcom/konka/InitApp/CInitAppSys;
    invoke-static {v1}, Lcom/konka/InitApp/CInitAppSys$4;->access$0(Lcom/konka/InitApp/CInitAppSys$4;)Lcom/konka/InitApp/CInitAppSys;

    move-result-object v1

    # getter for: Lcom/konka/InitApp/CInitAppSys;->startBuff:I
    invoke-static {v1}, Lcom/konka/InitApp/CInitAppSys;->access$3(Lcom/konka/InitApp/CInitAppSys;)I

    move-result v2

    add-int/lit8 v2, v2, 0x4

    invoke-static {v1, v2}, Lcom/konka/InitApp/CInitAppSys;->access$1(Lcom/konka/InitApp/CInitAppSys;I)V

    goto/16 :goto_1

    :cond_5
    iput-object p1, v0, LdataLayer/dataCenter/DevConcurrentLinkedQueuePool$CmdPacketWriter;->sock:Lnetwork/Interface/NIOSocket;

    iget-object v1, p0, Lcom/konka/InitApp/CInitAppSys$4$1;->this$1:Lcom/konka/InitApp/CInitAppSys$4;

    # getter for: Lcom/konka/InitApp/CInitAppSys$4;->this$0:Lcom/konka/InitApp/CInitAppSys;
    invoke-static {v1}, Lcom/konka/InitApp/CInitAppSys$4;->access$0(Lcom/konka/InitApp/CInitAppSys$4;)Lcom/konka/InitApp/CInitAppSys;

    move-result-object v1

    iget-object v1, v1, Lcom/konka/InitApp/CInitAppSys;->devConcurrentLinkedQueuePool:LdataLayer/dataCenter/DevConcurrentLinkedQueuePool;

    invoke-virtual {v1, v7, v0}, LdataLayer/dataCenter/DevConcurrentLinkedQueuePool;->inPoolQueue(ILdataLayer/dataCenter/DevConcurrentLinkedQueuePool$CmdPacketWriter;)V

    goto :goto_2
.end method
