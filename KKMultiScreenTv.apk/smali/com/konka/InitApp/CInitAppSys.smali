.class public Lcom/konka/InitApp/CInitAppSys;
.super Ljava/lang/Object;
.source "CInitAppSys.java"


# static fields
.field public static PropertyFile:Ljava/lang/String;

.field public static best:Lcom/konka/IntelligentControl/ioop/specificOp/BestvOp;

.field public static tcpSvrIsRun:Z


# instance fields
.field private action:Landroid/app/Application;

.field public broadcast:Lnetwork/udp/UdpBroadcast;

.field public devConcurrentLinkedQueuePool:LdataLayer/dataCenter/DevConcurrentLinkedQueuePool;

.field private headLen:[B

.field private packetCmd:I

.field private packetLen:I

.field private recvDataLen:I

.field public service:Lnetwork/NetCore/asyncService;

.field private startBuff:I

.field public svrSocket:Lnetwork/Interface/asyncServerSocket;

.field public final tcpPort:I

.field public final udpPort:I

.field public userCenter:LdataLayer/dataCenter/userGroup;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    const/4 v0, 0x1

    sput-boolean v0, Lcom/konka/InitApp/CInitAppSys;->tcpSvrIsRun:Z

    const/4 v0, 0x0

    sput-object v0, Lcom/konka/InitApp/CInitAppSys;->best:Lcom/konka/IntelligentControl/ioop/specificOp/BestvOp;

    const-string v0, "funcFlag.properties"

    sput-object v0, Lcom/konka/InitApp/CInitAppSys;->PropertyFile:Ljava/lang/String;

    return-void
.end method

.method public constructor <init>(Landroid/app/Application;)V
    .locals 3
    .param p1    # Landroid/app/Application;

    const/4 v2, 0x0

    const/4 v1, 0x0

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    iput-object v2, p0, Lcom/konka/InitApp/CInitAppSys;->action:Landroid/app/Application;

    const/16 v0, 0x1f41

    iput v0, p0, Lcom/konka/InitApp/CInitAppSys;->tcpPort:I

    const/16 v0, 0x2711

    iput v0, p0, Lcom/konka/InitApp/CInitAppSys;->udpPort:I

    iput-object v2, p0, Lcom/konka/InitApp/CInitAppSys;->service:Lnetwork/NetCore/asyncService;

    iput-object v2, p0, Lcom/konka/InitApp/CInitAppSys;->svrSocket:Lnetwork/Interface/asyncServerSocket;

    new-instance v0, Lnetwork/udp/UdpBroadcast;

    invoke-direct {v0}, Lnetwork/udp/UdpBroadcast;-><init>()V

    iput-object v0, p0, Lcom/konka/InitApp/CInitAppSys;->broadcast:Lnetwork/udp/UdpBroadcast;

    new-instance v0, LdataLayer/dataCenter/userGroup;

    invoke-direct {v0}, LdataLayer/dataCenter/userGroup;-><init>()V

    iput-object v0, p0, Lcom/konka/InitApp/CInitAppSys;->userCenter:LdataLayer/dataCenter/userGroup;

    new-instance v0, LdataLayer/dataCenter/DevConcurrentLinkedQueuePool;

    invoke-direct {v0}, LdataLayer/dataCenter/DevConcurrentLinkedQueuePool;-><init>()V

    iput-object v0, p0, Lcom/konka/InitApp/CInitAppSys;->devConcurrentLinkedQueuePool:LdataLayer/dataCenter/DevConcurrentLinkedQueuePool;

    iput v1, p0, Lcom/konka/InitApp/CInitAppSys;->startBuff:I

    iput v1, p0, Lcom/konka/InitApp/CInitAppSys;->recvDataLen:I

    iput v1, p0, Lcom/konka/InitApp/CInitAppSys;->packetLen:I

    iput v1, p0, Lcom/konka/InitApp/CInitAppSys;->packetCmd:I

    const/4 v0, 0x2

    new-array v0, v0, [B

    iput-object v0, p0, Lcom/konka/InitApp/CInitAppSys;->headLen:[B

    iput-object p1, p0, Lcom/konka/InitApp/CInitAppSys;->action:Landroid/app/Application;

    invoke-virtual {p0}, Lcom/konka/InitApp/CInitAppSys;->Init()I

    return-void
.end method

.method static synthetic access$0(Lcom/konka/InitApp/CInitAppSys;)Landroid/app/Application;
    .locals 1

    iget-object v0, p0, Lcom/konka/InitApp/CInitAppSys;->action:Landroid/app/Application;

    return-object v0
.end method

.method static synthetic access$1(Lcom/konka/InitApp/CInitAppSys;I)V
    .locals 0

    iput p1, p0, Lcom/konka/InitApp/CInitAppSys;->startBuff:I

    return-void
.end method

.method static synthetic access$2(Lcom/konka/InitApp/CInitAppSys;I)V
    .locals 0

    iput p1, p0, Lcom/konka/InitApp/CInitAppSys;->recvDataLen:I

    return-void
.end method

.method static synthetic access$3(Lcom/konka/InitApp/CInitAppSys;)I
    .locals 1

    iget v0, p0, Lcom/konka/InitApp/CInitAppSys;->startBuff:I

    return v0
.end method

.method static synthetic access$4(Lcom/konka/InitApp/CInitAppSys;)I
    .locals 1

    iget v0, p0, Lcom/konka/InitApp/CInitAppSys;->recvDataLen:I

    return v0
.end method

.method static synthetic access$5(Lcom/konka/InitApp/CInitAppSys;)[B
    .locals 1

    iget-object v0, p0, Lcom/konka/InitApp/CInitAppSys;->headLen:[B

    return-object v0
.end method

.method static synthetic access$6(Lcom/konka/InitApp/CInitAppSys;I)V
    .locals 0

    iput p1, p0, Lcom/konka/InitApp/CInitAppSys;->packetLen:I

    return-void
.end method

.method static synthetic access$7(Lcom/konka/InitApp/CInitAppSys;)I
    .locals 1

    iget v0, p0, Lcom/konka/InitApp/CInitAppSys;->packetLen:I

    return v0
.end method

.method static synthetic access$8(Lcom/konka/InitApp/CInitAppSys;I)V
    .locals 0

    iput p1, p0, Lcom/konka/InitApp/CInitAppSys;->packetCmd:I

    return-void
.end method

.method static synthetic access$9(Lcom/konka/InitApp/CInitAppSys;)I
    .locals 1

    iget v0, p0, Lcom/konka/InitApp/CInitAppSys;->packetCmd:I

    return v0
.end method

.method public static chmodPermissions(Ljava/lang/String;)V
    .locals 3
    .param p0    # Ljava/lang/String;

    :try_start_0
    invoke-static {}, Ljava/lang/Runtime;->getRuntime()Ljava/lang/Runtime;

    move-result-object v2

    invoke-virtual {v2, p0}, Ljava/lang/Runtime;->exec(Ljava/lang/String;)Ljava/lang/Process;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/Process;->waitFor()I
    :try_end_0
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_0
    .catch Ljava/lang/InterruptedException; {:try_start_0 .. :try_end_0} :catch_1

    return-void

    :catch_0
    move-exception v0

    :goto_0
    invoke-virtual {v0}, Ljava/io/IOException;->printStackTrace()V

    goto :goto_0

    :catch_1
    move-exception v1

    :goto_1
    invoke-virtual {v1}, Ljava/lang/InterruptedException;->printStackTrace()V

    goto :goto_1
.end method

.method public static execLibFile(Landroid/content/Context;)V
    .locals 9
    .param p0    # Landroid/content/Context;

    const-string v1, "/data/data/com.konka.kkmultiscreen/bin"

    const-string v3, "tvliveserver"

    const-string v2, "ErrorInfo"

    new-instance v0, Ljava/io/File;

    invoke-direct {v0, v1}, Ljava/io/File;-><init>(Ljava/lang/String;)V

    new-instance v4, Ljava/io/File;

    invoke-direct {v4, v0, v3}, Ljava/io/File;-><init>(Ljava/io/File;Ljava/lang/String;)V

    invoke-virtual {v0}, Ljava/io/File;->exists()Z

    move-result v5

    if-eqz v5, :cond_1

    invoke-virtual {v0}, Ljava/io/File;->isDirectory()Z

    move-result v5

    if-eqz v5, :cond_1

    invoke-virtual {v4}, Ljava/io/File;->exists()Z

    move-result v5

    if-eqz v5, :cond_0

    invoke-virtual {v4}, Ljava/io/File;->isFile()Z

    move-result v5

    if-eqz v5, :cond_0

    invoke-virtual {v4}, Ljava/io/File;->length()J

    move-result-wide v5

    const-wide/16 v7, 0x0

    cmp-long v5, v5, v7

    if-eqz v5, :cond_0

    const-string v5, "exec run.sh---"

    const-string v6, "---run.sh"

    invoke-static {v5, v6}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    const-string v5, "chmod 777 /data/data/com.konka.kkmultiscreen/bin/run.sh"

    invoke-static {v5}, Lcom/konka/InitApp/CInitAppSys;->chmodPermissions(Ljava/lang/String;)V

    const-string v5, "chmod 777 /data/data/com.konka.kkmultiscreen/bin/tvliveserver"

    invoke-static {v5}, Lcom/konka/InitApp/CInitAppSys;->chmodPermissions(Ljava/lang/String;)V

    const-string v5, "/data/data/com.konka.kkmultiscreen/bin/run.sh"

    invoke-static {v5}, Lcom/konka/InitApp/CInitAppSys;->execTvliveserver(Ljava/lang/String;)V

    :goto_0
    return-void

    :cond_0
    invoke-virtual {v4}, Ljava/io/File;->delete()Z

    const-string v5, "tvliveserver"

    invoke-static {v5, p0}, Lcom/konka/InitApp/CreatTvliverServer;->copyTvliverserverToBin(Ljava/lang/String;Landroid/content/Context;)V

    const-string v5, "run.sh"

    invoke-static {v5, p0}, Lcom/konka/InitApp/CreatTvliverServer;->copyTvliverserverToBin(Ljava/lang/String;Landroid/content/Context;)V

    const-string v5, "ErrorInfo"

    const-string v6, "tvFile create -----1------"

    invoke-static {v5, v6}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    const-string v5, "chmod 777 /data/data/com.konka.kkmultiscreen/bin/run.sh"

    invoke-static {v5}, Lcom/konka/InitApp/CInitAppSys;->chmodPermissions(Ljava/lang/String;)V

    const-string v5, "chmod 777 /data/data/com.konka.kkmultiscreen/bin/tvliveserver"

    invoke-static {v5}, Lcom/konka/InitApp/CInitAppSys;->chmodPermissions(Ljava/lang/String;)V

    const-string v5, "/data/data/com.konka.kkmultiscreen/bin/run.sh"

    invoke-static {v5}, Lcom/konka/InitApp/CInitAppSys;->execTvliveserver(Ljava/lang/String;)V

    const-string v5, "ErrorInfo"

    const-string v6, " execTvliveserver -----2------"

    invoke-static {v5, v6}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_0

    :cond_1
    invoke-static {}, Lcom/konka/InitApp/CreatTvliverServer;->creatTvliverServer()V

    const-string v5, "tvliveserver"

    invoke-static {v5, p0}, Lcom/konka/InitApp/CreatTvliverServer;->copyTvliverserverToBin(Ljava/lang/String;Landroid/content/Context;)V

    const-string v5, "run.sh"

    invoke-static {v5, p0}, Lcom/konka/InitApp/CreatTvliverServer;->copyTvliverserverToBin(Ljava/lang/String;Landroid/content/Context;)V

    const-string v5, "chmod 777 /data/data/com.konka.kkmultiscreen/bin/run.sh"

    invoke-static {v5}, Lcom/konka/InitApp/CInitAppSys;->chmodPermissions(Ljava/lang/String;)V

    const-string v5, "chmod 777 /data/data/com.konka.kkmultiscreen/bin/tvliveserver"

    invoke-static {v5}, Lcom/konka/InitApp/CInitAppSys;->chmodPermissions(Ljava/lang/String;)V

    const-string v5, "/data/data/com.konka.kkmultiscreen/bin/run.sh"

    invoke-static {v5}, Lcom/konka/InitApp/CInitAppSys;->execTvliveserver(Ljava/lang/String;)V

    goto :goto_0
.end method

.method private static execTvliveserver(Ljava/lang/String;)V
    .locals 10
    .param p0    # Ljava/lang/String;

    :try_start_0
    const-string v6, "UseTime----------------------"

    const-string v7, "start exec----------------s"

    invoke-static {v6, v7}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v4

    invoke-static {}, Ljava/lang/Runtime;->getRuntime()Ljava/lang/Runtime;

    move-result-object v6

    invoke-virtual {v6, p0}, Ljava/lang/Runtime;->exec(Ljava/lang/String;)Ljava/lang/Process;

    move-result-object v3

    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v1

    const-string v6, "UseTime----------------------"

    new-instance v7, Ljava/lang/StringBuilder;

    invoke-direct {v7}, Ljava/lang/StringBuilder;-><init>()V

    sub-long v8, v1, v4

    invoke-virtual {v7, v8, v9}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v7

    invoke-virtual {v7}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v7

    invoke-static {v6, v7}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    :goto_0
    return-void

    :catch_0
    move-exception v0

    invoke-virtual {v0}, Ljava/lang/Exception;->printStackTrace()V

    goto :goto_0
.end method

.method private isOpenNetwork()Z
    .locals 5

    const v1, 0x493e0

    move v2, v1

    :goto_0
    add-int/lit8 v1, v2, -0x1

    if-gtz v2, :cond_0

    const/4 v3, 0x0

    :goto_1
    return v3

    :cond_0
    iget-object v3, p0, Lcom/konka/InitApp/CInitAppSys;->action:Landroid/app/Application;

    const-string v4, "connectivity"

    invoke-virtual {v3, v4}, Landroid/app/Application;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/net/ConnectivityManager;

    invoke-virtual {v0}, Landroid/net/ConnectivityManager;->getActiveNetworkInfo()Landroid/net/NetworkInfo;

    move-result-object v3

    if-eqz v3, :cond_2

    invoke-virtual {v0}, Landroid/net/ConnectivityManager;->getActiveNetworkInfo()Landroid/net/NetworkInfo;

    move-result-object v3

    invoke-virtual {v3}, Landroid/net/NetworkInfo;->isAvailable()Z

    move-result v3

    if-eqz v3, :cond_1

    const/4 v3, 0x1

    goto :goto_1

    :cond_1
    const-wide/16 v3, 0x7d0

    invoke-static {v3, v4}, Landroid/os/SystemClock;->sleep(J)V

    const-string v3, "CInitAppSys"

    const-string v4, "isOpenNetwork() sleep(2000) "

    invoke-static {v3, v4}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    move v2, v1

    goto :goto_0

    :cond_2
    const-wide/16 v3, 0x1770

    invoke-static {v3, v4}, Landroid/os/SystemClock;->sleep(J)V

    const-string v3, "CInitAppSys"

    const-string v4, "isOpenNetwork() sleep(6000) "

    invoke-static {v3, v4}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    move v2, v1

    goto :goto_0
.end method


# virtual methods
.method public Init()I
    .locals 3

    new-instance v1, Lcom/konka/InitApp/CInitAppSys$1;

    invoke-direct {v1, p0}, Lcom/konka/InitApp/CInitAppSys$1;-><init>(Lcom/konka/InitApp/CInitAppSys;)V

    invoke-virtual {v1}, Ljava/lang/Thread;->start()V

    new-instance v0, Lcom/konka/InitApp/CInitAppSys$2;

    invoke-direct {v0, p0}, Lcom/konka/InitApp/CInitAppSys$2;-><init>(Lcom/konka/InitApp/CInitAppSys;)V

    invoke-virtual {v0}, Ljava/lang/Thread;->start()V

    const/4 v2, 0x0

    return v2
.end method

.method public InitBesTV()I
    .locals 1

    const/4 v0, 0x0

    return v0
.end method

.method public InitDevInfo()I
    .locals 6

    const/4 v5, 0x0

    const-wide/16 v1, 0xbb8

    :try_start_0
    invoke-static {v1, v2}, Landroid/os/SystemClock;->sleep(J)V

    sget-object v1, LinterfaceLayer/devI0/IDevDataOperate;->IR:LinterfaceLayer/devI0/IDevDataOperate;

    const/4 v2, 0x1

    new-array v2, v2, [I

    const/4 v3, 0x0

    const/16 v4, 0xba

    aput v4, v2, v3

    invoke-interface {v1, v2}, LinterfaceLayer/devI0/IDevDataOperate;->wirteIRdata([I)V

    const-wide/16 v1, 0xbb8

    invoke-static {v1, v2}, Landroid/os/SystemClock;->sleep(J)V

    invoke-static {}, Lcom/konka/IntelligentControl/ioop/fileOp/CFileOper;->setUpAccDev()V
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    :goto_0
    return v5

    :catch_0
    move-exception v0

    invoke-virtual {v0}, Ljava/lang/Exception;->printStackTrace()V

    goto :goto_0
.end method

.method public InitIntelligentControl()I
    .locals 1

    const/4 v0, 0x0

    return v0
.end method

.method public InitMediaShare()I
    .locals 1

    const/4 v0, 0x0

    return v0
.end method

.method public InitNetWork()I
    .locals 7

    :try_start_0
    invoke-direct {p0}, Lcom/konka/InitApp/CInitAppSys;->isOpenNetwork()Z

    move-result v4

    if-nez v4, :cond_0

    new-instance v3, Landroid/os/Message;

    invoke-direct {v3}, Landroid/os/Message;-><init>()V

    const/16 v4, 0x2718

    iput v4, v3, Landroid/os/Message;->what:I

    new-instance v0, Landroid/os/Bundle;

    invoke-direct {v0}, Landroid/os/Bundle;-><init>()V

    const-string v4, "MSGINFO:"

    iget-object v5, p0, Lcom/konka/InitApp/CInitAppSys;->action:Landroid/app/Application;

    invoke-virtual {v5}, Landroid/app/Application;->getApplicationContext()Landroid/content/Context;

    move-result-object v5

    invoke-virtual {v5}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v5

    const v6, 0x7f04000c    # com.konka.kkmultiscreen.R.string.NetOpenFail

    invoke-virtual {v5, v6}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v0, v4, v5}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V

    invoke-virtual {v3, v0}, Landroid/os/Message;->setData(Landroid/os/Bundle;)V

    iget-object v4, p0, Lcom/konka/InitApp/CInitAppSys;->action:Landroid/app/Application;

    check-cast v4, Lcom/konka/kkmultiscreen/KKMutiScreenTvApp;

    iget-object v4, v4, Lcom/konka/kkmultiscreen/KKMutiScreenTvApp;->mApplicationHandler:Landroid/os/Handler;

    invoke-virtual {v4, v3}, Landroid/os/Handler;->sendMessage(Landroid/os/Message;)Z

    const/4 v4, -0x1

    :goto_0
    return v4

    :cond_0
    new-instance v2, Lcom/konka/InitApp/CInitAppSys$3;

    invoke-direct {v2, p0}, Lcom/konka/InitApp/CInitAppSys$3;-><init>(Lcom/konka/InitApp/CInitAppSys;)V

    invoke-virtual {v2}, Ljava/lang/Thread;->start()V

    new-instance v4, Lnetwork/NetCore/asyncService;

    invoke-direct {v4}, Lnetwork/NetCore/asyncService;-><init>()V

    iput-object v4, p0, Lcom/konka/InitApp/CInitAppSys;->service:Lnetwork/NetCore/asyncService;

    iget-object v4, p0, Lcom/konka/InitApp/CInitAppSys;->service:Lnetwork/NetCore/asyncService;

    const/16 v5, 0x1f41

    invoke-virtual {v4, v5}, Lnetwork/NetCore/asyncService;->openServerSocket(I)Lnetwork/Interface/asyncServerSocket;

    move-result-object v4

    iput-object v4, p0, Lcom/konka/InitApp/CInitAppSys;->svrSocket:Lnetwork/Interface/asyncServerSocket;

    iget-object v4, p0, Lcom/konka/InitApp/CInitAppSys;->svrSocket:Lnetwork/Interface/asyncServerSocket;

    new-instance v5, Lcom/konka/InitApp/CInitAppSys$4;

    invoke-direct {v5, p0}, Lcom/konka/InitApp/CInitAppSys$4;-><init>(Lcom/konka/InitApp/CInitAppSys;)V

    invoke-interface {v4, v5}, Lnetwork/Interface/asyncServerSocket;->listen(Lnetwork/Interface/ServerSocketObserver;)V

    iget-object v4, p0, Lcom/konka/InitApp/CInitAppSys;->svrSocket:Lnetwork/Interface/asyncServerSocket;

    sget-object v5, Lnetwork/Interface/ConnectionAcceptor;->ALLOW:Lnetwork/Interface/ConnectionAcceptor;

    invoke-interface {v4, v5}, Lnetwork/Interface/asyncServerSocket;->setConnectionAcceptor(Lnetwork/Interface/ConnectionAcceptor;)V

    new-instance v3, Landroid/os/Message;

    invoke-direct {v3}, Landroid/os/Message;-><init>()V

    const/16 v4, 0x2718

    iput v4, v3, Landroid/os/Message;->what:I

    new-instance v0, Landroid/os/Bundle;

    invoke-direct {v0}, Landroid/os/Bundle;-><init>()V

    const-string v4, "MSGINFO:"

    iget-object v5, p0, Lcom/konka/InitApp/CInitAppSys;->action:Landroid/app/Application;

    invoke-virtual {v5}, Landroid/app/Application;->getApplicationContext()Landroid/content/Context;

    move-result-object v5

    invoke-virtual {v5}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v5

    const v6, 0x7f040011    # com.konka.kkmultiscreen.R.string.open_MultiScreen_Service_Success

    invoke-virtual {v5, v6}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v0, v4, v5}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V

    const/4 v4, 0x1

    sput-boolean v4, Lcom/konka/kkmultiscreen/KKMutiScreenTvActivity;->mToastType:Z

    invoke-virtual {v3, v0}, Landroid/os/Message;->setData(Landroid/os/Bundle;)V

    iget-object v4, p0, Lcom/konka/InitApp/CInitAppSys;->action:Landroid/app/Application;

    check-cast v4, Lcom/konka/kkmultiscreen/KKMutiScreenTvApp;

    iget-object v4, v4, Lcom/konka/kkmultiscreen/KKMutiScreenTvApp;->mApplicationHandler:Landroid/os/Handler;

    invoke-virtual {v4, v3}, Landroid/os/Handler;->sendMessage(Landroid/os/Message;)Z

    :goto_1
    sget-boolean v4, Lcom/konka/InitApp/CInitAppSys;->tcpSvrIsRun:Z

    if-nez v4, :cond_1

    :goto_2
    const/4 v4, 0x0

    goto :goto_0

    :cond_1
    iget-object v4, p0, Lcom/konka/InitApp/CInitAppSys;->service:Lnetwork/NetCore/asyncService;

    const-wide/16 v5, 0x1

    invoke-virtual {v4, v5, v6}, Lnetwork/NetCore/asyncService;->selectBlocking(J)V
    :try_end_0
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_0

    goto :goto_1

    :catch_0
    move-exception v1

    invoke-virtual {v1}, Ljava/io/IOException;->printStackTrace()V

    goto :goto_2
.end method

.method public InitSysInfo()I
    .locals 1

    iget-object v0, p0, Lcom/konka/InitApp/CInitAppSys;->action:Landroid/app/Application;

    check-cast v0, Lcom/konka/kkmultiscreen/KKMutiScreenTvApp;

    invoke-virtual {v0}, Lcom/konka/kkmultiscreen/KKMutiScreenTvApp;->ToStartSerives()V

    iget-object v0, p0, Lcom/konka/InitApp/CInitAppSys;->action:Landroid/app/Application;

    check-cast v0, Lcom/konka/kkmultiscreen/KKMutiScreenTvApp;

    invoke-virtual {v0}, Lcom/konka/kkmultiscreen/KKMutiScreenTvApp;->ToRegisterReceiver()V

    const/4 v0, 0x0

    return v0
.end method

.method public startYidian()V
    .locals 5

    const-string v2, "startYidian"

    const-string v3, "richard intall or start yidian --------------begin"

    invoke-static {v2, v3}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    new-instance v0, Lcom/konka/kkmultiscreen/DataService;

    invoke-direct {v0}, Lcom/konka/kkmultiscreen/DataService;-><init>()V

    const-string v2, "com.rockitv.android"

    invoke-virtual {v0, v2}, Lcom/konka/kkmultiscreen/DataService;->isInstall(Ljava/lang/String;)Z

    move-result v2

    if-eqz v2, :cond_0

    const v2, 0x7f040009    # com.konka.kkmultiscreen.R.string.install_reminder

    invoke-virtual {v0, v2}, Lcom/konka/kkmultiscreen/DataService;->installYidian(I)Z

    :goto_0
    const-string v2, "startYidian"

    const-string v3, "richard intall or start yidian --------------end"

    invoke-static {v2, v3}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    return-void

    :cond_0
    new-instance v1, Landroid/content/Intent;

    invoke-direct {v1}, Landroid/content/Intent;-><init>()V

    const-string v2, "com.rockitv.android"

    const-string v3, "com.rockitv.android.MyService"

    invoke-virtual {v1, v2, v3}, Landroid/content/Intent;->setClassName(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    iget-object v2, p0, Lcom/konka/InitApp/CInitAppSys;->action:Landroid/app/Application;

    check-cast v2, Lcom/konka/kkmultiscreen/KKMutiScreenTvApp;

    invoke-virtual {v2, v1}, Lcom/konka/kkmultiscreen/KKMutiScreenTvApp;->startService(Landroid/content/Intent;)Landroid/content/ComponentName;

    iget-object v2, p0, Lcom/konka/InitApp/CInitAppSys;->action:Landroid/app/Application;

    check-cast v2, Lcom/konka/kkmultiscreen/KKMutiScreenTvApp;

    new-instance v3, Landroid/content/Intent;

    const-string v4, "com.rockitv.android.START_SERVER"

    invoke-direct {v3, v4}, Landroid/content/Intent;-><init>(Ljava/lang/String;)V

    invoke-virtual {v2, v3}, Lcom/konka/kkmultiscreen/KKMutiScreenTvApp;->sendBroadcast(Landroid/content/Intent;)V

    goto :goto_0
.end method
