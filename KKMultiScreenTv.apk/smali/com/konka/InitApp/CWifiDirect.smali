.class public Lcom/konka/InitApp/CWifiDirect;
.super Ljava/lang/Object;
.source "CWifiDirect.java"


# static fields
.field private static final TAG:Ljava/lang/String; = "CWifiDirect"


# instance fields
.field private action:Lcom/konka/kkmultiscreen/KKMutiScreenTvApp;

.field private channel:Landroid/net/wifi/p2p/WifiP2pManager$Channel;

.field private final intentFilter:Landroid/content/IntentFilter;

.field public isWifiP2pEnabled:Z

.field private mWifiManager:Landroid/net/wifi/WifiManager;

.field private p2p_manager:Landroid/net/wifi/p2p/WifiP2pManager;

.field private usedContext:Landroid/content/Context;

.field private usedLooper:Landroid/os/Looper;

.field private wifiDirectReceiver:Landroid/content/BroadcastReceiver;

.field public wifiDirectTread:Ljava/lang/Thread;


# direct methods
.method public constructor <init>(Lcom/konka/kkmultiscreen/KKMutiScreenTvApp;)V
    .locals 2
    .param p1    # Lcom/konka/kkmultiscreen/KKMutiScreenTvApp;

    const/4 v1, 0x0

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    iput-object v1, p0, Lcom/konka/InitApp/CWifiDirect;->action:Lcom/konka/kkmultiscreen/KKMutiScreenTvApp;

    iput-object v1, p0, Lcom/konka/InitApp/CWifiDirect;->mWifiManager:Landroid/net/wifi/WifiManager;

    new-instance v0, Landroid/content/IntentFilter;

    invoke-direct {v0}, Landroid/content/IntentFilter;-><init>()V

    iput-object v0, p0, Lcom/konka/InitApp/CWifiDirect;->intentFilter:Landroid/content/IntentFilter;

    iput-object v1, p0, Lcom/konka/InitApp/CWifiDirect;->channel:Landroid/net/wifi/p2p/WifiP2pManager$Channel;

    iput-object v1, p0, Lcom/konka/InitApp/CWifiDirect;->p2p_manager:Landroid/net/wifi/p2p/WifiP2pManager;

    iput-object v1, p0, Lcom/konka/InitApp/CWifiDirect;->wifiDirectReceiver:Landroid/content/BroadcastReceiver;

    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/konka/InitApp/CWifiDirect;->isWifiP2pEnabled:Z

    iput-object v1, p0, Lcom/konka/InitApp/CWifiDirect;->usedContext:Landroid/content/Context;

    iput-object v1, p0, Lcom/konka/InitApp/CWifiDirect;->usedLooper:Landroid/os/Looper;

    iput-object v1, p0, Lcom/konka/InitApp/CWifiDirect;->wifiDirectTread:Ljava/lang/Thread;

    iput-object p1, p0, Lcom/konka/InitApp/CWifiDirect;->action:Lcom/konka/kkmultiscreen/KKMutiScreenTvApp;

    invoke-virtual {p0}, Lcom/konka/InitApp/CWifiDirect;->Init()I

    return-void
.end method

.method static synthetic access$0(Lcom/konka/InitApp/CWifiDirect;)Landroid/net/wifi/p2p/WifiP2pManager;
    .locals 1

    iget-object v0, p0, Lcom/konka/InitApp/CWifiDirect;->p2p_manager:Landroid/net/wifi/p2p/WifiP2pManager;

    return-object v0
.end method

.method static synthetic access$1(Lcom/konka/InitApp/CWifiDirect;)Landroid/net/wifi/p2p/WifiP2pManager$Channel;
    .locals 1

    iget-object v0, p0, Lcom/konka/InitApp/CWifiDirect;->channel:Landroid/net/wifi/p2p/WifiP2pManager$Channel;

    return-object v0
.end method


# virtual methods
.method public Init()I
    .locals 5

    const/4 v4, 0x0

    const/4 v3, 0x0

    iget-object v0, p0, Lcom/konka/InitApp/CWifiDirect;->action:Lcom/konka/kkmultiscreen/KKMutiScreenTvApp;

    iget-object v1, p0, Lcom/konka/InitApp/CWifiDirect;->action:Lcom/konka/kkmultiscreen/KKMutiScreenTvApp;

    invoke-virtual {v1}, Lcom/konka/kkmultiscreen/KKMutiScreenTvApp;->getApplicationContext()Landroid/content/Context;

    const-string v1, "wifi"

    invoke-virtual {v0, v1}, Lcom/konka/kkmultiscreen/KKMutiScreenTvApp;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/net/wifi/WifiManager;

    iput-object v0, p0, Lcom/konka/InitApp/CWifiDirect;->mWifiManager:Landroid/net/wifi/WifiManager;

    iget-object v0, p0, Lcom/konka/InitApp/CWifiDirect;->mWifiManager:Landroid/net/wifi/WifiManager;

    if-eqz v0, :cond_1

    iget-object v0, p0, Lcom/konka/InitApp/CWifiDirect;->mWifiManager:Landroid/net/wifi/WifiManager;

    invoke-virtual {v0}, Landroid/net/wifi/WifiManager;->isWifiEnabled()Z

    :goto_0
    iget-object v0, p0, Lcom/konka/InitApp/CWifiDirect;->intentFilter:Landroid/content/IntentFilter;

    const-string v1, "android.net.wifi.p2p.STATE_CHANGED"

    invoke-virtual {v0, v1}, Landroid/content/IntentFilter;->addAction(Ljava/lang/String;)V

    iget-object v0, p0, Lcom/konka/InitApp/CWifiDirect;->intentFilter:Landroid/content/IntentFilter;

    const-string v1, "android.net.wifi.p2p.PEERS_CHANGED"

    invoke-virtual {v0, v1}, Landroid/content/IntentFilter;->addAction(Ljava/lang/String;)V

    iget-object v0, p0, Lcom/konka/InitApp/CWifiDirect;->intentFilter:Landroid/content/IntentFilter;

    const-string v1, "android.net.wifi.p2p.CONNECTION_STATE_CHANGE"

    invoke-virtual {v0, v1}, Landroid/content/IntentFilter;->addAction(Ljava/lang/String;)V

    iget-object v0, p0, Lcom/konka/InitApp/CWifiDirect;->intentFilter:Landroid/content/IntentFilter;

    const-string v1, "android.net.wifi.p2p.THIS_DEVICE_CHANGED"

    invoke-virtual {v0, v1}, Landroid/content/IntentFilter;->addAction(Ljava/lang/String;)V

    iget-object v0, p0, Lcom/konka/InitApp/CWifiDirect;->action:Lcom/konka/kkmultiscreen/KKMutiScreenTvApp;

    const-string v1, "wifip2p"

    invoke-virtual {v0, v1}, Lcom/konka/kkmultiscreen/KKMutiScreenTvApp;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/net/wifi/p2p/WifiP2pManager;

    iput-object v0, p0, Lcom/konka/InitApp/CWifiDirect;->p2p_manager:Landroid/net/wifi/p2p/WifiP2pManager;

    iget-object v0, p0, Lcom/konka/InitApp/CWifiDirect;->p2p_manager:Landroid/net/wifi/p2p/WifiP2pManager;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/konka/InitApp/CWifiDirect;->p2p_manager:Landroid/net/wifi/p2p/WifiP2pManager;

    iget-object v1, p0, Lcom/konka/InitApp/CWifiDirect;->action:Lcom/konka/kkmultiscreen/KKMutiScreenTvApp;

    invoke-virtual {v1}, Lcom/konka/kkmultiscreen/KKMutiScreenTvApp;->getApplicationContext()Landroid/content/Context;

    move-result-object v1

    iget-object v2, p0, Lcom/konka/InitApp/CWifiDirect;->action:Lcom/konka/kkmultiscreen/KKMutiScreenTvApp;

    invoke-virtual {v2}, Lcom/konka/kkmultiscreen/KKMutiScreenTvApp;->getMainLooper()Landroid/os/Looper;

    move-result-object v2

    invoke-virtual {v0, v1, v2, v3}, Landroid/net/wifi/p2p/WifiP2pManager;->initialize(Landroid/content/Context;Landroid/os/Looper;Landroid/net/wifi/p2p/WifiP2pManager$ChannelListener;)Landroid/net/wifi/p2p/WifiP2pManager$Channel;

    move-result-object v0

    iput-object v0, p0, Lcom/konka/InitApp/CWifiDirect;->channel:Landroid/net/wifi/p2p/WifiP2pManager$Channel;

    :cond_0
    invoke-virtual {p0, v3, v3}, Lcom/konka/InitApp/CWifiDirect;->openWifiDirectBroadcastReceiver(Landroid/os/Looper;Landroid/content/Context;)V

    return v4

    :cond_1
    iput-boolean v4, p0, Lcom/konka/InitApp/CWifiDirect;->isWifiP2pEnabled:Z

    goto :goto_0
.end method

.method public closeWifiDirect()V
    .locals 7

    const-string v3, "CWifiDirect"

    const-string v4, "closeWifiDirect()"

    invoke-static {v3, v4}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    sget-object v3, Landroid/os/Build$VERSION;->SDK:Ljava/lang/String;

    invoke-static {v3}, Ljava/lang/Integer;->valueOf(Ljava/lang/String;)Ljava/lang/Integer;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/Integer;->intValue()I

    move-result v3

    const/16 v4, 0xe

    if-lt v3, v4, :cond_0

    iget-object v3, p0, Lcom/konka/InitApp/CWifiDirect;->p2p_manager:Landroid/net/wifi/p2p/WifiP2pManager;

    invoke-virtual {v3}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v0

    const/4 v2, 0x0

    :try_start_0
    const-string v3, "disableP2p"

    const/4 v4, 0x1

    new-array v4, v4, [Ljava/lang/Class;

    const/4 v5, 0x0

    const-class v6, Landroid/net/wifi/p2p/WifiP2pManager$Channel;

    aput-object v6, v4, v5

    invoke-virtual {v0, v3, v4}, Ljava/lang/Class;->getDeclaredMethod(Ljava/lang/String;[Ljava/lang/Class;)Ljava/lang/reflect/Method;
    :try_end_0
    .catch Ljava/lang/NoSuchMethodException; {:try_start_0 .. :try_end_0} :catch_0

    move-result-object v2

    :goto_0
    if-eqz v2, :cond_1

    :try_start_1
    iget-object v3, p0, Lcom/konka/InitApp/CWifiDirect;->p2p_manager:Landroid/net/wifi/p2p/WifiP2pManager;

    const/4 v4, 0x1

    new-array v4, v4, [Ljava/lang/Object;

    const/4 v5, 0x0

    iget-object v6, p0, Lcom/konka/InitApp/CWifiDirect;->channel:Landroid/net/wifi/p2p/WifiP2pManager$Channel;

    aput-object v6, v4, v5

    invoke-virtual {v2, v3, v4}, Ljava/lang/reflect/Method;->invoke(Ljava/lang/Object;[Ljava/lang/Object;)Ljava/lang/Object;

    const/4 v3, 0x0

    iput-boolean v3, p0, Lcom/konka/InitApp/CWifiDirect;->isWifiP2pEnabled:Z
    :try_end_1
    .catch Ljava/lang/IllegalArgumentException; {:try_start_1 .. :try_end_1} :catch_1
    .catch Ljava/lang/IllegalAccessException; {:try_start_1 .. :try_end_1} :catch_2
    .catch Ljava/lang/reflect/InvocationTargetException; {:try_start_1 .. :try_end_1} :catch_3

    :cond_0
    :goto_1
    return-void

    :catch_0
    move-exception v1

    invoke-virtual {v1}, Ljava/lang/NoSuchMethodException;->printStackTrace()V

    goto :goto_0

    :cond_1
    :try_start_2
    const-string v3, "CWifiDirect"

    const-string v4, "not find method disableP2p"

    invoke-static {v3, v4}, Landroid/util/Log;->v(Ljava/lang/String;Ljava/lang/String;)I
    :try_end_2
    .catch Ljava/lang/IllegalArgumentException; {:try_start_2 .. :try_end_2} :catch_1
    .catch Ljava/lang/IllegalAccessException; {:try_start_2 .. :try_end_2} :catch_2
    .catch Ljava/lang/reflect/InvocationTargetException; {:try_start_2 .. :try_end_2} :catch_3

    goto :goto_1

    :catch_1
    move-exception v1

    invoke-virtual {v1}, Ljava/lang/IllegalArgumentException;->printStackTrace()V

    goto :goto_1

    :catch_2
    move-exception v1

    invoke-virtual {v1}, Ljava/lang/IllegalAccessException;->printStackTrace()V

    goto :goto_1

    :catch_3
    move-exception v1

    invoke-virtual {v1}, Ljava/lang/reflect/InvocationTargetException;->printStackTrace()V

    goto :goto_1
.end method

.method public openWifiDirect()V
    .locals 7

    const-string v3, "CWifiDirect"

    const-string v4, "openWifiDirect()"

    invoke-static {v3, v4}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    sget-object v3, Landroid/os/Build$VERSION;->SDK:Ljava/lang/String;

    invoke-static {v3}, Ljava/lang/Integer;->valueOf(Ljava/lang/String;)Ljava/lang/Integer;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/Integer;->intValue()I

    move-result v3

    const/16 v4, 0xe

    if-lt v3, v4, :cond_0

    iget-object v3, p0, Lcom/konka/InitApp/CWifiDirect;->p2p_manager:Landroid/net/wifi/p2p/WifiP2pManager;

    invoke-virtual {v3}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v0

    const/4 v2, 0x0

    :try_start_0
    const-string v3, "enableP2p"

    const/4 v4, 0x1

    new-array v4, v4, [Ljava/lang/Class;

    const/4 v5, 0x0

    const-class v6, Landroid/net/wifi/p2p/WifiP2pManager$Channel;

    aput-object v6, v4, v5

    invoke-virtual {v0, v3, v4}, Ljava/lang/Class;->getDeclaredMethod(Ljava/lang/String;[Ljava/lang/Class;)Ljava/lang/reflect/Method;
    :try_end_0
    .catch Ljava/lang/NoSuchMethodException; {:try_start_0 .. :try_end_0} :catch_0

    move-result-object v2

    :goto_0
    if-eqz v2, :cond_1

    :try_start_1
    iget-object v3, p0, Lcom/konka/InitApp/CWifiDirect;->p2p_manager:Landroid/net/wifi/p2p/WifiP2pManager;

    const/4 v4, 0x1

    new-array v4, v4, [Ljava/lang/Object;

    const/4 v5, 0x0

    iget-object v6, p0, Lcom/konka/InitApp/CWifiDirect;->channel:Landroid/net/wifi/p2p/WifiP2pManager$Channel;

    aput-object v6, v4, v5

    invoke-virtual {v2, v3, v4}, Ljava/lang/reflect/Method;->invoke(Ljava/lang/Object;[Ljava/lang/Object;)Ljava/lang/Object;

    const/4 v3, 0x1

    iput-boolean v3, p0, Lcom/konka/InitApp/CWifiDirect;->isWifiP2pEnabled:Z
    :try_end_1
    .catch Ljava/lang/Exception; {:try_start_1 .. :try_end_1} :catch_1

    :cond_0
    :goto_1
    return-void

    :catch_0
    move-exception v1

    invoke-virtual {v1}, Ljava/lang/NoSuchMethodException;->printStackTrace()V

    goto :goto_0

    :cond_1
    :try_start_2
    const-string v3, "CWifiDirect"

    const-string v4, "not find method enableP2p"

    invoke-static {v3, v4}, Landroid/util/Log;->v(Ljava/lang/String;Ljava/lang/String;)I
    :try_end_2
    .catch Ljava/lang/Exception; {:try_start_2 .. :try_end_2} :catch_1

    goto :goto_1

    :catch_1
    move-exception v1

    invoke-virtual {v1}, Ljava/lang/Exception;->printStackTrace()V

    goto :goto_1
.end method

.method public openWifiDirectBroadcastReceiver(Landroid/os/Looper;Landroid/content/Context;)V
    .locals 4
    .param p1    # Landroid/os/Looper;
    .param p2    # Landroid/content/Context;

    const/4 v3, 0x0

    sget-object v0, Landroid/os/Build$VERSION;->SDK:Ljava/lang/String;

    invoke-static {v0}, Ljava/lang/Integer;->valueOf(Ljava/lang/String;)Ljava/lang/Integer;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/Integer;->intValue()I

    move-result v0

    const/16 v1, 0xe

    if-ge v0, v1, :cond_0

    const-string v0, "CWifiDirect"

    const-string v1, "android.os.Build.VERSION.SDK < 14"

    invoke-static {v0, v1}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    :goto_0
    return-void

    :cond_0
    if-eqz p1, :cond_4

    if-eqz p2, :cond_4

    iget-object v0, p0, Lcom/konka/InitApp/CWifiDirect;->usedContext:Landroid/content/Context;

    if-ne v0, p2, :cond_1

    iget-object v0, p0, Lcom/konka/InitApp/CWifiDirect;->usedLooper:Landroid/os/Looper;

    if-eq p1, v0, :cond_4

    :cond_1
    iput-object p2, p0, Lcom/konka/InitApp/CWifiDirect;->usedContext:Landroid/content/Context;

    iput-object p1, p0, Lcom/konka/InitApp/CWifiDirect;->usedLooper:Landroid/os/Looper;

    iget-object v0, p0, Lcom/konka/InitApp/CWifiDirect;->channel:Landroid/net/wifi/p2p/WifiP2pManager$Channel;

    if-eqz v0, :cond_2

    iput-object v3, p0, Lcom/konka/InitApp/CWifiDirect;->channel:Landroid/net/wifi/p2p/WifiP2pManager$Channel;

    :cond_2
    iget-object v0, p0, Lcom/konka/InitApp/CWifiDirect;->p2p_manager:Landroid/net/wifi/p2p/WifiP2pManager;

    if-eqz v0, :cond_3

    iget-object v0, p0, Lcom/konka/InitApp/CWifiDirect;->p2p_manager:Landroid/net/wifi/p2p/WifiP2pManager;

    iget-object v1, p0, Lcom/konka/InitApp/CWifiDirect;->usedContext:Landroid/content/Context;

    iget-object v2, p0, Lcom/konka/InitApp/CWifiDirect;->usedLooper:Landroid/os/Looper;

    invoke-virtual {v0, v1, v2, v3}, Landroid/net/wifi/p2p/WifiP2pManager;->initialize(Landroid/content/Context;Landroid/os/Looper;Landroid/net/wifi/p2p/WifiP2pManager$ChannelListener;)Landroid/net/wifi/p2p/WifiP2pManager$Channel;

    move-result-object v0

    iput-object v0, p0, Lcom/konka/InitApp/CWifiDirect;->channel:Landroid/net/wifi/p2p/WifiP2pManager$Channel;

    :cond_3
    invoke-virtual {p0}, Lcom/konka/InitApp/CWifiDirect;->registerWifiDirect()V

    :cond_4
    iget-object v0, p0, Lcom/konka/InitApp/CWifiDirect;->channel:Landroid/net/wifi/p2p/WifiP2pManager$Channel;

    if-nez v0, :cond_6

    iget-object v0, p0, Lcom/konka/InitApp/CWifiDirect;->p2p_manager:Landroid/net/wifi/p2p/WifiP2pManager;

    if-eqz v0, :cond_5

    iget-object v0, p0, Lcom/konka/InitApp/CWifiDirect;->p2p_manager:Landroid/net/wifi/p2p/WifiP2pManager;

    iget-object v1, p0, Lcom/konka/InitApp/CWifiDirect;->action:Lcom/konka/kkmultiscreen/KKMutiScreenTvApp;

    invoke-virtual {v1}, Lcom/konka/kkmultiscreen/KKMutiScreenTvApp;->getApplicationContext()Landroid/content/Context;

    move-result-object v1

    iget-object v2, p0, Lcom/konka/InitApp/CWifiDirect;->action:Lcom/konka/kkmultiscreen/KKMutiScreenTvApp;

    invoke-virtual {v2}, Lcom/konka/kkmultiscreen/KKMutiScreenTvApp;->getMainLooper()Landroid/os/Looper;

    move-result-object v2

    invoke-virtual {v0, v1, v2, v3}, Landroid/net/wifi/p2p/WifiP2pManager;->initialize(Landroid/content/Context;Landroid/os/Looper;Landroid/net/wifi/p2p/WifiP2pManager$ChannelListener;)Landroid/net/wifi/p2p/WifiP2pManager$Channel;

    move-result-object v0

    iput-object v0, p0, Lcom/konka/InitApp/CWifiDirect;->channel:Landroid/net/wifi/p2p/WifiP2pManager$Channel;

    :cond_5
    invoke-virtual {p0}, Lcom/konka/InitApp/CWifiDirect;->registerWifiDirect()V

    :cond_6
    if-nez p1, :cond_7

    if-nez p2, :cond_7

    iget-object v0, p0, Lcom/konka/InitApp/CWifiDirect;->channel:Landroid/net/wifi/p2p/WifiP2pManager$Channel;

    if-eqz v0, :cond_7

    iget-object v0, p0, Lcom/konka/InitApp/CWifiDirect;->p2p_manager:Landroid/net/wifi/p2p/WifiP2pManager;

    if-eqz v0, :cond_7

    invoke-virtual {p0}, Lcom/konka/InitApp/CWifiDirect;->registerWifiDirect()V

    :cond_7
    invoke-virtual {p0}, Lcom/konka/InitApp/CWifiDirect;->startWifiDirectSearchTread()V

    goto :goto_0
.end method

.method public registerWifiDirect()V
    .locals 4

    sget-object v0, Landroid/os/Build$VERSION;->SDK:Ljava/lang/String;

    invoke-static {v0}, Ljava/lang/Integer;->valueOf(Ljava/lang/String;)Ljava/lang/Integer;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/Integer;->intValue()I

    move-result v0

    const/16 v1, 0xe

    if-lt v0, v1, :cond_0

    invoke-virtual {p0}, Lcom/konka/InitApp/CWifiDirect;->unregisterWifiDirectReceiver()V

    iget-object v0, p0, Lcom/konka/InitApp/CWifiDirect;->p2p_manager:Landroid/net/wifi/p2p/WifiP2pManager;

    if-eqz v0, :cond_1

    iget-object v0, p0, Lcom/konka/InitApp/CWifiDirect;->channel:Landroid/net/wifi/p2p/WifiP2pManager$Channel;

    if-eqz v0, :cond_1

    new-instance v0, Lcom/konka/kkmultiscreen/WiFiDirectBroadcastReceiver;

    iget-object v1, p0, Lcom/konka/InitApp/CWifiDirect;->p2p_manager:Landroid/net/wifi/p2p/WifiP2pManager;

    iget-object v2, p0, Lcom/konka/InitApp/CWifiDirect;->channel:Landroid/net/wifi/p2p/WifiP2pManager$Channel;

    iget-object v3, p0, Lcom/konka/InitApp/CWifiDirect;->action:Lcom/konka/kkmultiscreen/KKMutiScreenTvApp;

    invoke-direct {v0, v1, v2, v3}, Lcom/konka/kkmultiscreen/WiFiDirectBroadcastReceiver;-><init>(Landroid/net/wifi/p2p/WifiP2pManager;Landroid/net/wifi/p2p/WifiP2pManager$Channel;Lcom/konka/kkmultiscreen/KKMutiScreenTvApp;)V

    iput-object v0, p0, Lcom/konka/InitApp/CWifiDirect;->wifiDirectReceiver:Landroid/content/BroadcastReceiver;

    :goto_0
    iget-object v0, p0, Lcom/konka/InitApp/CWifiDirect;->wifiDirectReceiver:Landroid/content/BroadcastReceiver;

    if-eqz v0, :cond_2

    iget-object v0, p0, Lcom/konka/InitApp/CWifiDirect;->intentFilter:Landroid/content/IntentFilter;

    if-eqz v0, :cond_2

    iget-object v0, p0, Lcom/konka/InitApp/CWifiDirect;->action:Lcom/konka/kkmultiscreen/KKMutiScreenTvApp;

    iget-object v1, p0, Lcom/konka/InitApp/CWifiDirect;->wifiDirectReceiver:Landroid/content/BroadcastReceiver;

    iget-object v2, p0, Lcom/konka/InitApp/CWifiDirect;->intentFilter:Landroid/content/IntentFilter;

    invoke-virtual {v0, v1, v2}, Lcom/konka/kkmultiscreen/KKMutiScreenTvApp;->registerReceiver(Landroid/content/BroadcastReceiver;Landroid/content/IntentFilter;)Landroid/content/Intent;

    :cond_0
    :goto_1
    return-void

    :cond_1
    const-string v0, "CWifiDirect"

    const-string v1, "p2p_manager != null && channel != null"

    invoke-static {v0, v1}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_0

    :cond_2
    const-string v0, "CWifiDirect"

    const-string v1, "wifiDirectReceiver != null && intentFilter != null"

    invoke-static {v0, v1}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_1
.end method

.method public startWifiDirectSearchTread()V
    .locals 1

    iget-object v0, p0, Lcom/konka/InitApp/CWifiDirect;->wifiDirectTread:Ljava/lang/Thread;

    if-eqz v0, :cond_1

    :cond_0
    :goto_0
    return-void

    :cond_1
    iget-object v0, p0, Lcom/konka/InitApp/CWifiDirect;->wifiDirectTread:Ljava/lang/Thread;

    if-nez v0, :cond_0

    new-instance v0, Lcom/konka/InitApp/CWifiDirect$1;

    invoke-direct {v0, p0}, Lcom/konka/InitApp/CWifiDirect$1;-><init>(Lcom/konka/InitApp/CWifiDirect;)V

    iput-object v0, p0, Lcom/konka/InitApp/CWifiDirect;->wifiDirectTread:Ljava/lang/Thread;

    iget-object v0, p0, Lcom/konka/InitApp/CWifiDirect;->wifiDirectTread:Ljava/lang/Thread;

    invoke-virtual {v0}, Ljava/lang/Thread;->start()V

    goto :goto_0
.end method

.method public unregisterWifiDirectReceiver()V
    .locals 2

    iget-object v0, p0, Lcom/konka/InitApp/CWifiDirect;->wifiDirectReceiver:Landroid/content/BroadcastReceiver;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/konka/InitApp/CWifiDirect;->action:Lcom/konka/kkmultiscreen/KKMutiScreenTvApp;

    iget-object v1, p0, Lcom/konka/InitApp/CWifiDirect;->wifiDirectReceiver:Landroid/content/BroadcastReceiver;

    invoke-virtual {v0, v1}, Lcom/konka/kkmultiscreen/KKMutiScreenTvApp;->unregisterReceiver(Landroid/content/BroadcastReceiver;)V

    const/4 v0, 0x0

    iput-object v0, p0, Lcom/konka/InitApp/CWifiDirect;->wifiDirectReceiver:Landroid/content/BroadcastReceiver;

    :cond_0
    return-void
.end method
