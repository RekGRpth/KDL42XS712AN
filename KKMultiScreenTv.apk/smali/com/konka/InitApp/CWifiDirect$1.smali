.class Lcom/konka/InitApp/CWifiDirect$1;
.super Ljava/lang/Thread;
.source "CWifiDirect.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/konka/InitApp/CWifiDirect;->startWifiDirectSearchTread()V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/konka/InitApp/CWifiDirect;


# direct methods
.method constructor <init>(Lcom/konka/InitApp/CWifiDirect;)V
    .locals 0

    iput-object p1, p0, Lcom/konka/InitApp/CWifiDirect$1;->this$0:Lcom/konka/InitApp/CWifiDirect;

    invoke-direct {p0}, Ljava/lang/Thread;-><init>()V

    return-void
.end method


# virtual methods
.method public run()V
    .locals 5

    const/4 v4, 0x0

    :try_start_0
    iget-object v1, p0, Lcom/konka/InitApp/CWifiDirect$1;->this$0:Lcom/konka/InitApp/CWifiDirect;

    iget-boolean v1, v1, Lcom/konka/InitApp/CWifiDirect;->isWifiP2pEnabled:Z

    if-eqz v1, :cond_0

    iget-object v1, p0, Lcom/konka/InitApp/CWifiDirect$1;->this$0:Lcom/konka/InitApp/CWifiDirect;

    # getter for: Lcom/konka/InitApp/CWifiDirect;->p2p_manager:Landroid/net/wifi/p2p/WifiP2pManager;
    invoke-static {v1}, Lcom/konka/InitApp/CWifiDirect;->access$0(Lcom/konka/InitApp/CWifiDirect;)Landroid/net/wifi/p2p/WifiP2pManager;

    move-result-object v1

    iget-object v2, p0, Lcom/konka/InitApp/CWifiDirect$1;->this$0:Lcom/konka/InitApp/CWifiDirect;

    # getter for: Lcom/konka/InitApp/CWifiDirect;->channel:Landroid/net/wifi/p2p/WifiP2pManager$Channel;
    invoke-static {v2}, Lcom/konka/InitApp/CWifiDirect;->access$1(Lcom/konka/InitApp/CWifiDirect;)Landroid/net/wifi/p2p/WifiP2pManager$Channel;

    move-result-object v2

    new-instance v3, Lcom/konka/InitApp/CWifiDirect$1$1;

    invoke-direct {v3, p0}, Lcom/konka/InitApp/CWifiDirect$1$1;-><init>(Lcom/konka/InitApp/CWifiDirect$1;)V

    invoke-virtual {v1, v2, v3}, Landroid/net/wifi/p2p/WifiP2pManager;->discoverPeers(Landroid/net/wifi/p2p/WifiP2pManager$Channel;Landroid/net/wifi/p2p/WifiP2pManager$ActionListener;)V
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    :cond_0
    const-string v1, "CWifiDirect"

    const-string v2, "discover wifi direct dev Tread1 normal quit......"

    invoke-static {v1, v2}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    iget-object v1, p0, Lcom/konka/InitApp/CWifiDirect$1;->this$0:Lcom/konka/InitApp/CWifiDirect;

    iput-object v4, v1, Lcom/konka/InitApp/CWifiDirect;->wifiDirectTread:Ljava/lang/Thread;

    :goto_0
    return-void

    :catch_0
    move-exception v0

    :try_start_1
    const-string v1, "CWifiDirect"

    new-instance v2, Ljava/lang/StringBuilder;

    const-string v3, "discover wifi direct dev Tread1 Exception quit......"

    invoke-direct {v2, v3}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v0}, Ljava/lang/Exception;->getMessage()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    const-string v1, "CWifiDirect"

    const-string v2, "discover wifi direct dev Tread1 normal quit......"

    invoke-static {v1, v2}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    iget-object v1, p0, Lcom/konka/InitApp/CWifiDirect$1;->this$0:Lcom/konka/InitApp/CWifiDirect;

    iput-object v4, v1, Lcom/konka/InitApp/CWifiDirect;->wifiDirectTread:Ljava/lang/Thread;

    goto :goto_0

    :catchall_0
    move-exception v1

    const-string v2, "CWifiDirect"

    const-string v3, "discover wifi direct dev Tread1 normal quit......"

    invoke-static {v2, v3}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    iget-object v2, p0, Lcom/konka/InitApp/CWifiDirect$1;->this$0:Lcom/konka/InitApp/CWifiDirect;

    iput-object v4, v2, Lcom/konka/InitApp/CWifiDirect;->wifiDirectTread:Ljava/lang/Thread;

    throw v1
.end method
