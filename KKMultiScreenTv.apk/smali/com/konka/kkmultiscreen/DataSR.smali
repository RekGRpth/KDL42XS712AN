.class public Lcom/konka/kkmultiscreen/DataSR;
.super Ljava/lang/Object;
.source "DataSR.java"


# static fields
.field private static final LOG_TAG:Ljava/lang/String; = "DataSR"


# direct methods
.method public constructor <init>()V
    .locals 0

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method public static ToSendInnerData(Lcom/konka/kkmultiscreen/DataHelper;Lcom/konka/kkmultiscreen/MsgDef$MsgInner;)V
    .locals 0
    .param p0    # Lcom/konka/kkmultiscreen/DataHelper;
    .param p1    # Lcom/konka/kkmultiscreen/MsgDef$MsgInner;

    if-eqz p0, :cond_0

    invoke-virtual {p0, p1}, Lcom/konka/kkmultiscreen/DataHelper;->SendInnerMsgToService(Lcom/konka/kkmultiscreen/MsgDef$MsgInner;)Landroid/os/Parcel;

    :cond_0
    return-void
.end method

.method public static ToSendOuterData(Lcom/konka/kkmultiscreen/DataHelper;Lcom/konka/kkmultiscreen/MsgDef$MsgInner;)V
    .locals 0
    .param p0    # Lcom/konka/kkmultiscreen/DataHelper;
    .param p1    # Lcom/konka/kkmultiscreen/MsgDef$MsgInner;

    if-eqz p0, :cond_0

    invoke-virtual {p0, p1}, Lcom/konka/kkmultiscreen/DataHelper;->SendOuterMsgToService(Lcom/konka/kkmultiscreen/MsgDef$MsgInner;)Landroid/os/Parcel;

    :cond_0
    return-void
.end method
