.class public Lcom/konka/kkmultiscreen/WiFiDirectBroadcastReceiver$notifyServerAsyncTask;
.super Landroid/os/AsyncTask;
.source "WiFiDirectBroadcastReceiver.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/konka/kkmultiscreen/WiFiDirectBroadcastReceiver;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x9
    name = "notifyServerAsyncTask"
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Landroid/os/AsyncTask",
        "<",
        "Ljava/lang/Void;",
        "Ljava/lang/Void;",
        "Ljava/lang/String;",
        ">;"
    }
.end annotation


# static fields
.field private static final TAG:Ljava/lang/String; = "notifyServerAsyncTask"


# direct methods
.method public constructor <init>()V
    .locals 0

    invoke-direct {p0}, Landroid/os/AsyncTask;-><init>()V

    return-void
.end method


# virtual methods
.method protected bridge varargs synthetic doInBackground([Ljava/lang/Object;)Ljava/lang/Object;
    .locals 1

    check-cast p1, [Ljava/lang/Void;

    invoke-virtual {p0, p1}, Lcom/konka/kkmultiscreen/WiFiDirectBroadcastReceiver$notifyServerAsyncTask;->doInBackground([Ljava/lang/Void;)Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method protected varargs doInBackground([Ljava/lang/Void;)Ljava/lang/String;
    .locals 15
    .param p1    # [Ljava/lang/Void;

    :try_start_0
    # getter for: Lcom/konka/kkmultiscreen/WiFiDirectBroadcastReceiver;->mApp:Lcom/konka/kkmultiscreen/KKMutiScreenTvApp;
    invoke-static {}, Lcom/konka/kkmultiscreen/WiFiDirectBroadcastReceiver;->access$0()Lcom/konka/kkmultiscreen/KKMutiScreenTvApp;

    move-result-object v11

    # getter for: Lcom/konka/kkmultiscreen/WiFiDirectBroadcastReceiver;->mApp:Lcom/konka/kkmultiscreen/KKMutiScreenTvApp;
    invoke-static {}, Lcom/konka/kkmultiscreen/WiFiDirectBroadcastReceiver;->access$0()Lcom/konka/kkmultiscreen/KKMutiScreenTvApp;

    move-result-object v12

    invoke-virtual {v12}, Lcom/konka/kkmultiscreen/KKMutiScreenTvApp;->getApplicationContext()Landroid/content/Context;

    const-string v12, "wifi"

    invoke-virtual {v11, v12}, Lcom/konka/kkmultiscreen/KKMutiScreenTvApp;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v5

    check-cast v5, Landroid/net/wifi/WifiManager;

    if-eqz v5, :cond_0

    invoke-virtual {v5}, Landroid/net/wifi/WifiManager;->isWifiEnabled()Z

    move-result v11

    if-nez v11, :cond_0

    const/4 v11, 0x1

    invoke-virtual {v5, v11}, Landroid/net/wifi/WifiManager;->setWifiEnabled(Z)Z

    :cond_0
    new-instance v10, Ljava/net/ServerSocket;

    const/16 v11, 0x3039

    invoke-direct {v10, v11}, Ljava/net/ServerSocket;-><init>(I)V

    const/4 v11, 0x1

    invoke-virtual {v10, v11}, Ljava/net/ServerSocket;->setReuseAddress(Z)V

    const-string v11, "notifyServerAsyncTask"

    const-string v12, "Server: Socket opened"

    invoke-static {v11, v12}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    invoke-virtual {v10}, Ljava/net/ServerSocket;->accept()Ljava/net/Socket;

    move-result-object v1

    invoke-virtual {v1}, Ljava/net/Socket;->getInetAddress()Ljava/net/InetAddress;

    move-result-object v11

    invoke-virtual {v11}, Ljava/net/InetAddress;->getHostAddress()Ljava/lang/String;

    move-result-object v11

    sput-object v11, Lcom/konka/kkmultiscreen/WiFiDirectBroadcastReceiver$DeviceListFragment;->clientIp:Ljava/lang/String;

    const-string v11, "notifyServerAsyncTask"

    new-instance v12, Ljava/lang/StringBuilder;

    const-string v13, "Server: connection done "

    invoke-direct {v12, v13}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v1}, Ljava/net/Socket;->isConnected()Z

    move-result v13

    invoke-virtual {v12, v13}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v12

    const-string v13, " ip "

    invoke-virtual {v12, v13}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v12

    sget-object v13, Lcom/konka/kkmultiscreen/WiFiDirectBroadcastReceiver$DeviceListFragment;->clientIp:Ljava/lang/String;

    invoke-virtual {v12, v13}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v12

    invoke-virtual {v12}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v12

    invoke-static {v11, v12}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I
    :try_end_0
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_1

    :try_start_1
    invoke-virtual {v1}, Ljava/net/Socket;->getInputStream()Ljava/io/InputStream;

    move-result-object v4

    new-instance v6, Ljava/io/DataInputStream;

    invoke-direct {v6, v4}, Ljava/io/DataInputStream;-><init>(Ljava/io/InputStream;)V

    const/16 v11, 0x400

    new-array v0, v11, [B

    const-string v11, "notifyServerAsyncTask"

    const-string v12, "tv is client: Socket to read data!"

    invoke-static {v11, v12}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    invoke-virtual {v6, v0}, Ljava/io/DataInputStream;->read([B)I

    move-result v7

    if-lez v7, :cond_2

    add-int/lit8 v11, v7, 0x1

    new-array v2, v11, [B

    const/4 v11, 0x0

    const/4 v12, 0x0

    invoke-static {v0, v11, v2, v12, v7}, Ljava/lang/System;->arraycopy(Ljava/lang/Object;ILjava/lang/Object;II)V

    const/4 v11, 0x0

    aput-byte v11, v2, v7

    new-instance v11, Ljava/lang/String;

    const-string v12, "UTF-8"

    invoke-direct {v11, v2, v12}, Ljava/lang/String;-><init>([BLjava/lang/String;)V

    invoke-virtual {v11}, Ljava/lang/String;->trim()Ljava/lang/String;

    move-result-object v8

    const-string v11, "notifyServerAsyncTask"

    new-instance v12, Ljava/lang/StringBuilder;

    const-string v13, "tv is client: Socket had read data: "

    invoke-direct {v12, v13}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v12, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v12

    const-string v13, " ret "

    invoke-virtual {v12, v13}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v12

    invoke-virtual {v12, v7}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v12

    invoke-virtual {v12}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v12

    invoke-static {v11, v12}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    invoke-virtual {v8}, Ljava/lang/String;->length()I

    move-result v11

    if-lez v11, :cond_2

    sget-object v11, Lnetwork/udp/BroadThread;->connMap:Ljava/util/Map;

    if-eqz v11, :cond_2

    sget-object v11, Lnetwork/udp/BroadThread;->connMap:Ljava/util/Map;

    sget-object v12, Lcom/konka/kkmultiscreen/WiFiDirectBroadcastReceiver$DeviceListFragment;->serverIp:Ljava/lang/String;

    invoke-static {v12}, Lutil/comm/ToolClass;->getStringIpToLong(Ljava/lang/String;)J

    move-result-wide v12

    invoke-static {v12, v13}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v12

    invoke-interface {v11, v12}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v11

    if-nez v11, :cond_1

    sget-object v11, Lnetwork/udp/BroadThread;->connMap:Ljava/util/Map;

    sget-object v12, Lcom/konka/kkmultiscreen/WiFiDirectBroadcastReceiver$DeviceListFragment;->serverIp:Ljava/lang/String;

    invoke-static {v12}, Lutil/comm/ToolClass;->getStringIpToLong(Ljava/lang/String;)J

    move-result-wide v12

    invoke-static {v12, v13}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v12

    invoke-interface {v11, v12}, Ljava/util/Map;->remove(Ljava/lang/Object;)Ljava/lang/Object;

    :cond_1
    sget-object v11, Lnetwork/udp/BroadThread;->connMap:Ljava/util/Map;

    sget-object v12, Lcom/konka/kkmultiscreen/WiFiDirectBroadcastReceiver$DeviceListFragment;->serverIp:Ljava/lang/String;

    invoke-static {v12}, Lutil/comm/ToolClass;->getStringIpToLong(Ljava/lang/String;)J

    move-result-wide v12

    invoke-static {v12, v13}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v12

    new-instance v13, Ljava/lang/StringBuilder;

    sget-object v14, Lcom/konka/kkmultiscreen/WiFiDirectBroadcastReceiver$DeviceListFragment;->serverIp:Ljava/lang/String;

    invoke-static {v14}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v14

    invoke-direct {v13, v14}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    const-string v14, "&"

    invoke-virtual {v13, v14}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v13

    invoke-virtual {v13, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v13

    invoke-virtual {v13}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v13

    invoke-interface {v11, v12, v13}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    :cond_2
    invoke-virtual {v6}, Ljava/io/DataInputStream;->close()V

    invoke-virtual {v4}, Ljava/io/InputStream;->close()V
    :try_end_1
    .catch Ljava/io/IOException; {:try_start_1 .. :try_end_1} :catch_0

    :try_start_2
    invoke-virtual {v1}, Ljava/net/Socket;->close()V

    invoke-virtual {v10}, Ljava/net/ServerSocket;->close()V
    :try_end_2
    .catch Ljava/io/IOException; {:try_start_2 .. :try_end_2} :catch_1

    const-string v11, "send tv ip to client by this way!!!"

    :goto_0
    return-object v11

    :catch_0
    move-exception v9

    const/4 v11, 0x0

    goto :goto_0

    :catch_1
    move-exception v3

    const-string v11, "notifyServerAsyncTask"

    invoke-virtual {v3}, Ljava/io/IOException;->getMessage()Ljava/lang/String;

    move-result-object v12

    invoke-static {v11, v12}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    const/4 v11, 0x0

    goto :goto_0
.end method

.method protected bridge synthetic onPostExecute(Ljava/lang/Object;)V
    .locals 0

    check-cast p1, Ljava/lang/String;

    invoke-virtual {p0, p1}, Lcom/konka/kkmultiscreen/WiFiDirectBroadcastReceiver$notifyServerAsyncTask;->onPostExecute(Ljava/lang/String;)V

    return-void
.end method

.method protected onPostExecute(Ljava/lang/String;)V
    .locals 0
    .param p1    # Ljava/lang/String;

    return-void
.end method

.method protected onPreExecute()V
    .locals 0

    return-void
.end method
