.class public Lcom/konka/kkmultiscreen/WiFiDirectBroadcastReceiver$DeviceListFragment;
.super Ljava/lang/Object;
.source "WiFiDirectBroadcastReceiver.java"

# interfaces
.implements Landroid/net/wifi/p2p/WifiP2pManager$ConnectionInfoListener;
.implements Landroid/net/wifi/p2p/WifiP2pManager$PeerListListener;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/konka/kkmultiscreen/WiFiDirectBroadcastReceiver;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x9
    name = "DeviceListFragment"
.end annotation


# static fields
.field public static clientIp:Ljava/lang/String;

.field public static localWifi:Landroid/net/wifi/p2p/WifiP2pDevice;

.field private static lock_obj:Ljava/lang/Object;

.field private static peers:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Landroid/net/wifi/p2p/WifiP2pDevice;",
            ">;"
        }
    .end annotation
.end field

.field public static serverIp:Ljava/lang/String;

.field public static useingWifi:Landroid/net/wifi/p2p/WifiP2pDevice;


# direct methods
.method static constructor <clinit>()V
    .locals 2

    const/4 v1, 0x0

    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    sput-object v0, Lcom/konka/kkmultiscreen/WiFiDirectBroadcastReceiver$DeviceListFragment;->peers:Ljava/util/List;

    new-instance v0, Ljava/lang/Object;

    invoke-direct {v0}, Ljava/lang/Object;-><init>()V

    sput-object v0, Lcom/konka/kkmultiscreen/WiFiDirectBroadcastReceiver$DeviceListFragment;->lock_obj:Ljava/lang/Object;

    sput-object v1, Lcom/konka/kkmultiscreen/WiFiDirectBroadcastReceiver$DeviceListFragment;->useingWifi:Landroid/net/wifi/p2p/WifiP2pDevice;

    sput-object v1, Lcom/konka/kkmultiscreen/WiFiDirectBroadcastReceiver$DeviceListFragment;->localWifi:Landroid/net/wifi/p2p/WifiP2pDevice;

    sput-object v1, Lcom/konka/kkmultiscreen/WiFiDirectBroadcastReceiver$DeviceListFragment;->serverIp:Ljava/lang/String;

    sput-object v1, Lcom/konka/kkmultiscreen/WiFiDirectBroadcastReceiver$DeviceListFragment;->clientIp:Ljava/lang/String;

    return-void
.end method

.method public constructor <init>()V
    .locals 0

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method public static clearDeviceDataFromSet(Landroid/net/wifi/p2p/WifiP2pDevice;)V
    .locals 5
    .param p0    # Landroid/net/wifi/p2p/WifiP2pDevice;
    .annotation build Landroid/annotation/SuppressLint;
        value = {
            "NewApi"
        }
    .end annotation

    .annotation build Landroid/annotation/TargetApi;
        value = 0xe
    .end annotation

    const-string v1, "WiFiDirectBroadcastReceiver"

    new-instance v2, Ljava/lang/StringBuilder;

    const-string v3, "clearDeviceDataFromSet P2P device:  "

    invoke-direct {v2, v3}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    iget-object v3, p0, Landroid/net/wifi/p2p/WifiP2pDevice;->deviceName:Ljava/lang/String;

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    sget-object v2, Lcom/konka/kkmultiscreen/WiFiDirectBroadcastReceiver$DeviceListFragment;->lock_obj:Ljava/lang/Object;

    monitor-enter v2

    :try_start_0
    sget-object v1, Lcom/konka/kkmultiscreen/WiFiDirectBroadcastReceiver$DeviceListFragment;->peers:Ljava/util/List;

    invoke-interface {v1}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :cond_0
    :goto_0
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v3

    if-nez v3, :cond_1

    monitor-exit v2

    return-void

    :cond_1
    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/net/wifi/p2p/WifiP2pDevice;

    iget-object v3, v0, Landroid/net/wifi/p2p/WifiP2pDevice;->deviceName:Ljava/lang/String;

    iget-object v4, p0, Landroid/net/wifi/p2p/WifiP2pDevice;->deviceName:Ljava/lang/String;

    invoke-virtual {v3, v4}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v3

    if-eqz v3, :cond_0

    sget-object v3, Lcom/konka/kkmultiscreen/WiFiDirectBroadcastReceiver$DeviceListFragment;->peers:Ljava/util/List;

    invoke-interface {v3, p0}, Ljava/util/List;->remove(Ljava/lang/Object;)Z

    const/4 p0, 0x0

    goto :goto_0

    :catchall_0
    move-exception v1

    monitor-exit v2
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    throw v1
.end method

.method public static get(I)Landroid/net/wifi/p2p/WifiP2pDevice;
    .locals 4
    .param p0    # I
    .annotation build Landroid/annotation/SuppressLint;
        value = {
            "NewApi"
        }
    .end annotation

    .annotation build Landroid/annotation/TargetApi;
        value = 0xe
    .end annotation

    sget-object v3, Lcom/konka/kkmultiscreen/WiFiDirectBroadcastReceiver$DeviceListFragment;->lock_obj:Ljava/lang/Object;

    monitor-enter v3

    :try_start_0
    sget-object v2, Lcom/konka/kkmultiscreen/WiFiDirectBroadcastReceiver$DeviceListFragment;->peers:Ljava/util/List;

    invoke-interface {v2}, Ljava/util/List;->size()I

    move-result v1

    const/4 v0, 0x0

    :goto_0
    if-lt v0, v1, :cond_0

    monitor-exit v3

    const/4 v2, 0x0

    :goto_1
    return-object v2

    :cond_0
    if-ne v0, p0, :cond_1

    sget-object v2, Lcom/konka/kkmultiscreen/WiFiDirectBroadcastReceiver$DeviceListFragment;->peers:Ljava/util/List;

    invoke-interface {v2, p0}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Landroid/net/wifi/p2p/WifiP2pDevice;

    monitor-exit v3

    goto :goto_1

    :catchall_0
    move-exception v2

    monitor-exit v3
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    throw v2

    :cond_1
    add-int/lit8 v0, v0, 0x1

    goto :goto_0
.end method

.method public static get(Ljava/lang/String;)Landroid/net/wifi/p2p/WifiP2pDevice;
    .locals 4
    .param p0    # Ljava/lang/String;
    .annotation build Landroid/annotation/SuppressLint;
        value = {
            "NewApi"
        }
    .end annotation

    .annotation build Landroid/annotation/TargetApi;
        value = 0xe
    .end annotation

    sget-object v2, Lcom/konka/kkmultiscreen/WiFiDirectBroadcastReceiver$DeviceListFragment;->lock_obj:Ljava/lang/Object;

    monitor-enter v2

    :try_start_0
    sget-object v1, Lcom/konka/kkmultiscreen/WiFiDirectBroadcastReceiver$DeviceListFragment;->peers:Ljava/util/List;

    invoke-interface {v1}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :cond_0
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v3

    if-nez v3, :cond_1

    monitor-exit v2

    const/4 v0, 0x0

    :goto_0
    return-object v0

    :cond_1
    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/net/wifi/p2p/WifiP2pDevice;

    iget-object v3, v0, Landroid/net/wifi/p2p/WifiP2pDevice;->deviceName:Ljava/lang/String;

    invoke-virtual {v3, p0}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v3

    if-eqz v3, :cond_0

    monitor-exit v2

    goto :goto_0

    :catchall_0
    move-exception v1

    monitor-exit v2
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    throw v1
.end method

.method public static getDevCanUseNum()I
    .locals 2

    sget-object v1, Lcom/konka/kkmultiscreen/WiFiDirectBroadcastReceiver$DeviceListFragment;->lock_obj:Ljava/lang/Object;

    monitor-enter v1

    :try_start_0
    sget-object v0, Lcom/konka/kkmultiscreen/WiFiDirectBroadcastReceiver$DeviceListFragment;->peers:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->size()I

    move-result v0

    monitor-exit v1

    return v0

    :catchall_0
    move-exception v0

    monitor-exit v1
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    throw v0
.end method

.method public static inDeviceDataSet(Landroid/net/wifi/p2p/WifiP2pDevice;)V
    .locals 3
    .param p0    # Landroid/net/wifi/p2p/WifiP2pDevice;
    .annotation build Landroid/annotation/SuppressLint;
        value = {
            "NewApi"
        }
    .end annotation

    .annotation build Landroid/annotation/TargetApi;
        value = 0xe
    .end annotation

    const-string v0, "WiFiDirectBroadcastReceiver"

    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "inDeviceDataSet P2P device:  "

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    iget-object v2, p0, Landroid/net/wifi/p2p/WifiP2pDevice;->deviceName:Ljava/lang/String;

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    sget-object v0, Lcom/konka/kkmultiscreen/WiFiDirectBroadcastReceiver$DeviceListFragment;->localWifi:Landroid/net/wifi/p2p/WifiP2pDevice;

    if-nez v0, :cond_0

    sput-object p0, Lcom/konka/kkmultiscreen/WiFiDirectBroadcastReceiver$DeviceListFragment;->localWifi:Landroid/net/wifi/p2p/WifiP2pDevice;

    :goto_0
    return-void

    :cond_0
    sget-object v0, Lcom/konka/kkmultiscreen/WiFiDirectBroadcastReceiver$DeviceListFragment;->localWifi:Landroid/net/wifi/p2p/WifiP2pDevice;

    iget-object v0, v0, Landroid/net/wifi/p2p/WifiP2pDevice;->deviceName:Ljava/lang/String;

    iget-object v1, p0, Landroid/net/wifi/p2p/WifiP2pDevice;->deviceName:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_1

    sput-object p0, Lcom/konka/kkmultiscreen/WiFiDirectBroadcastReceiver$DeviceListFragment;->localWifi:Landroid/net/wifi/p2p/WifiP2pDevice;

    goto :goto_0

    :cond_1
    sput-object p0, Lcom/konka/kkmultiscreen/WiFiDirectBroadcastReceiver$DeviceListFragment;->useingWifi:Landroid/net/wifi/p2p/WifiP2pDevice;

    goto :goto_0
.end method

.method public static resetSet()Ljava/lang/String;
    .locals 2
    .annotation build Landroid/annotation/SuppressLint;
        value = {
            "NewApi"
        }
    .end annotation

    .annotation build Landroid/annotation/TargetApi;
        value = 0xe
    .end annotation

    const-string v0, "WiFiDirectBroadcastReceiver"

    const-string v1, "resetSet P2P device:  "

    invoke-static {v0, v1}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    sget-object v1, Lcom/konka/kkmultiscreen/WiFiDirectBroadcastReceiver$DeviceListFragment;->lock_obj:Ljava/lang/Object;

    monitor-enter v1

    :try_start_0
    sget-object v0, Lcom/konka/kkmultiscreen/WiFiDirectBroadcastReceiver$DeviceListFragment;->peers:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->clear()V

    monitor-exit v1

    const/4 v0, 0x0

    return-object v0

    :catchall_0
    move-exception v0

    monitor-exit v1
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    throw v0
.end method

.method public static size()I
    .locals 2

    sget-object v1, Lcom/konka/kkmultiscreen/WiFiDirectBroadcastReceiver$DeviceListFragment;->lock_obj:Ljava/lang/Object;

    monitor-enter v1

    :try_start_0
    sget-object v0, Lcom/konka/kkmultiscreen/WiFiDirectBroadcastReceiver$DeviceListFragment;->peers:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->size()I

    move-result v0

    monitor-exit v1

    return v0

    :catchall_0
    move-exception v0

    monitor-exit v1
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    throw v0
.end method


# virtual methods
.method public onConnectionInfoAvailable(Landroid/net/wifi/p2p/WifiP2pInfo;)V
    .locals 4
    .param p1    # Landroid/net/wifi/p2p/WifiP2pInfo;

    const/4 v3, 0x0

    const-string v0, "WiFiDirectBroadcastReceiver"

    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "onConnectionInfoAvailable "

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    iget-object v2, p1, Landroid/net/wifi/p2p/WifiP2pInfo;->groupOwnerAddress:Ljava/net/InetAddress;

    invoke-virtual {v2}, Ljava/net/InetAddress;->getHostAddress()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, " connectting"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    invoke-static {}, Lcom/konka/kkmultiscreen/KKMutiScreenTvActivity;->getLocalIpAddress()Ljava/lang/String;

    iget-boolean v0, p1, Landroid/net/wifi/p2p/WifiP2pInfo;->groupFormed:Z

    if-eqz v0, :cond_1

    iget-boolean v0, p1, Landroid/net/wifi/p2p/WifiP2pInfo;->isGroupOwner:Z

    if-eqz v0, :cond_1

    new-instance v0, Lcom/konka/kkmultiscreen/WiFiDirectBroadcastReceiver$notifyServerAsyncTask;

    invoke-direct {v0}, Lcom/konka/kkmultiscreen/WiFiDirectBroadcastReceiver$notifyServerAsyncTask;-><init>()V

    new-array v1, v3, [Ljava/lang/Void;

    invoke-virtual {v0, v1}, Lcom/konka/kkmultiscreen/WiFiDirectBroadcastReceiver$notifyServerAsyncTask;->execute([Ljava/lang/Object;)Landroid/os/AsyncTask;

    :cond_0
    :goto_0
    return-void

    :cond_1
    iget-boolean v0, p1, Landroid/net/wifi/p2p/WifiP2pInfo;->groupFormed:Z

    if-eqz v0, :cond_0

    iget-object v0, p1, Landroid/net/wifi/p2p/WifiP2pInfo;->groupOwnerAddress:Ljava/net/InetAddress;

    invoke-virtual {v0}, Ljava/net/InetAddress;->getHostAddress()Ljava/lang/String;

    move-result-object v0

    sput-object v0, Lcom/konka/kkmultiscreen/WiFiDirectBroadcastReceiver$DeviceListFragment;->serverIp:Ljava/lang/String;

    new-instance v0, Lcom/konka/kkmultiscreen/WiFiDirectBroadcastReceiver$notifyClientAsyncTask;

    invoke-direct {v0}, Lcom/konka/kkmultiscreen/WiFiDirectBroadcastReceiver$notifyClientAsyncTask;-><init>()V

    new-array v1, v3, [Ljava/lang/Void;

    invoke-virtual {v0, v1}, Lcom/konka/kkmultiscreen/WiFiDirectBroadcastReceiver$notifyClientAsyncTask;->execute([Ljava/lang/Object;)Landroid/os/AsyncTask;

    goto :goto_0
.end method

.method public onPeersAvailable(Landroid/net/wifi/p2p/WifiP2pDeviceList;)V
    .locals 0
    .param p1    # Landroid/net/wifi/p2p/WifiP2pDeviceList;
    .annotation build Landroid/annotation/SuppressLint;
        value = {
            "NewApi"
        }
    .end annotation

    .annotation build Landroid/annotation/TargetApi;
        value = 0xe
    .end annotation

    return-void
.end method
