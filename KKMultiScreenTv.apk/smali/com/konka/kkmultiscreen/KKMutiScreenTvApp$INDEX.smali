.class public final enum Lcom/konka/kkmultiscreen/KKMutiScreenTvApp$INDEX;
.super Ljava/lang/Enum;
.source "KKMutiScreenTvApp.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/konka/kkmultiscreen/KKMutiScreenTvApp;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x4019
    name = "INDEX"
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Enum",
        "<",
        "Lcom/konka/kkmultiscreen/KKMutiScreenTvApp$INDEX;",
        ">;"
    }
.end annotation


# static fields
.field public static final enum BESTV:Lcom/konka/kkmultiscreen/KKMutiScreenTvApp$INDEX;

.field public static final enum BESTV_DETAIL:Lcom/konka/kkmultiscreen/KKMutiScreenTvApp$INDEX;

.field private static final synthetic ENUM$VALUES:[Lcom/konka/kkmultiscreen/KKMutiScreenTvApp$INDEX;

.field public static final enum HOME:Lcom/konka/kkmultiscreen/KKMutiScreenTvApp$INDEX;

.field public static final enum MEDIA_SHARE:Lcom/konka/kkmultiscreen/KKMutiScreenTvApp$INDEX;


# direct methods
.method static constructor <clinit>()V
    .locals 6

    const/4 v5, 0x3

    const/4 v4, 0x2

    const/4 v3, 0x1

    const/4 v2, 0x0

    new-instance v0, Lcom/konka/kkmultiscreen/KKMutiScreenTvApp$INDEX;

    const-string v1, "HOME"

    invoke-direct {v0, v1, v2}, Lcom/konka/kkmultiscreen/KKMutiScreenTvApp$INDEX;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/konka/kkmultiscreen/KKMutiScreenTvApp$INDEX;->HOME:Lcom/konka/kkmultiscreen/KKMutiScreenTvApp$INDEX;

    new-instance v0, Lcom/konka/kkmultiscreen/KKMutiScreenTvApp$INDEX;

    const-string v1, "MEDIA_SHARE"

    invoke-direct {v0, v1, v3}, Lcom/konka/kkmultiscreen/KKMutiScreenTvApp$INDEX;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/konka/kkmultiscreen/KKMutiScreenTvApp$INDEX;->MEDIA_SHARE:Lcom/konka/kkmultiscreen/KKMutiScreenTvApp$INDEX;

    new-instance v0, Lcom/konka/kkmultiscreen/KKMutiScreenTvApp$INDEX;

    const-string v1, "BESTV"

    invoke-direct {v0, v1, v4}, Lcom/konka/kkmultiscreen/KKMutiScreenTvApp$INDEX;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/konka/kkmultiscreen/KKMutiScreenTvApp$INDEX;->BESTV:Lcom/konka/kkmultiscreen/KKMutiScreenTvApp$INDEX;

    new-instance v0, Lcom/konka/kkmultiscreen/KKMutiScreenTvApp$INDEX;

    const-string v1, "BESTV_DETAIL"

    invoke-direct {v0, v1, v5}, Lcom/konka/kkmultiscreen/KKMutiScreenTvApp$INDEX;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/konka/kkmultiscreen/KKMutiScreenTvApp$INDEX;->BESTV_DETAIL:Lcom/konka/kkmultiscreen/KKMutiScreenTvApp$INDEX;

    const/4 v0, 0x4

    new-array v0, v0, [Lcom/konka/kkmultiscreen/KKMutiScreenTvApp$INDEX;

    sget-object v1, Lcom/konka/kkmultiscreen/KKMutiScreenTvApp$INDEX;->HOME:Lcom/konka/kkmultiscreen/KKMutiScreenTvApp$INDEX;

    aput-object v1, v0, v2

    sget-object v1, Lcom/konka/kkmultiscreen/KKMutiScreenTvApp$INDEX;->MEDIA_SHARE:Lcom/konka/kkmultiscreen/KKMutiScreenTvApp$INDEX;

    aput-object v1, v0, v3

    sget-object v1, Lcom/konka/kkmultiscreen/KKMutiScreenTvApp$INDEX;->BESTV:Lcom/konka/kkmultiscreen/KKMutiScreenTvApp$INDEX;

    aput-object v1, v0, v4

    sget-object v1, Lcom/konka/kkmultiscreen/KKMutiScreenTvApp$INDEX;->BESTV_DETAIL:Lcom/konka/kkmultiscreen/KKMutiScreenTvApp$INDEX;

    aput-object v1, v0, v5

    sput-object v0, Lcom/konka/kkmultiscreen/KKMutiScreenTvApp$INDEX;->ENUM$VALUES:[Lcom/konka/kkmultiscreen/KKMutiScreenTvApp$INDEX;

    return-void
.end method

.method private constructor <init>(Ljava/lang/String;I)V
    .locals 0

    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    return-void
.end method

.method public static valueOf(Ljava/lang/String;)Lcom/konka/kkmultiscreen/KKMutiScreenTvApp$INDEX;
    .locals 1

    const-class v0, Lcom/konka/kkmultiscreen/KKMutiScreenTvApp$INDEX;

    invoke-static {v0, p0}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;

    move-result-object v0

    check-cast v0, Lcom/konka/kkmultiscreen/KKMutiScreenTvApp$INDEX;

    return-object v0
.end method

.method public static values()[Lcom/konka/kkmultiscreen/KKMutiScreenTvApp$INDEX;
    .locals 4

    const/4 v3, 0x0

    sget-object v0, Lcom/konka/kkmultiscreen/KKMutiScreenTvApp$INDEX;->ENUM$VALUES:[Lcom/konka/kkmultiscreen/KKMutiScreenTvApp$INDEX;

    array-length v1, v0

    new-array v2, v1, [Lcom/konka/kkmultiscreen/KKMutiScreenTvApp$INDEX;

    invoke-static {v0, v3, v2, v3, v1}, Ljava/lang/System;->arraycopy(Ljava/lang/Object;ILjava/lang/Object;II)V

    return-object v2
.end method
