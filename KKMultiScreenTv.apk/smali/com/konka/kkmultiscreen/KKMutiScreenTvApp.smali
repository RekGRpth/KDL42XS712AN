.class public Lcom/konka/kkmultiscreen/KKMutiScreenTvApp;
.super Landroid/app/Application;
.source "KKMutiScreenTvApp.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/konka/kkmultiscreen/KKMutiScreenTvApp$INDEX;,
        Lcom/konka/kkmultiscreen/KKMutiScreenTvApp$UpdateReceiver;
    }
.end annotation


# static fields
.field private static final CACHE_FOLDER:Ljava/lang/String; = ".cache"

.field private static final CRASH_REPORT_FOLDER:Ljava/lang/String; = "crash"

.field private static PREF_NAME:Ljava/lang/String; = null

.field private static final ROOT_FOLDER:Ljava/lang/String; = "weikan"

.field private static final SAVE_AS_FOLDER:Ljava/lang/String; = "weikan"

.field public static final SHOW_TOUCHES:Ljava/lang/String; = "show_touches"

.field private static final STORAGE_FOLDER:Ljava/lang/String; = "storage"

.field private static final TAG:Ljava/lang/String;

.field private static final UPDATE_FOLDER:Ljava/lang/String; = "update"

.field public static final ackAction:Ljava/lang/String; = "andriod.tv.network.ack"

.field public static mApp:Lcom/konka/kkmultiscreen/KKMutiScreenTvApp; = null

.field public static final rspAction:Ljava/lang/String; = "andriod.tv.network.rsp"


# instance fields
.field private dataHandle:Lcom/konka/kkmultiscreen/DataHelper;

.field private hasBind:Z

.field public mAppContext:Landroid/content/Context;

.field public mApplicationHandler:Landroid/os/Handler;

.field private mHasInited:Z

.field private receiver:Lcom/konka/kkmultiscreen/KKMutiScreenTvApp$UpdateReceiver;

.field public showTag:Z

.field public sysManager:Lcom/konka/InitApp/CInitAppSys;

.field public sysWifiDirect:Lcom/konka/InitApp/CWifiDirect;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    const-class v0, Lcom/konka/kkmultiscreen/KKMutiScreenTvApp;

    invoke-virtual {v0}, Ljava/lang/Class;->getSimpleName()Ljava/lang/String;

    move-result-object v0

    sput-object v0, Lcom/konka/kkmultiscreen/KKMutiScreenTvApp;->TAG:Ljava/lang/String;

    const/4 v0, 0x0

    sput-object v0, Lcom/konka/kkmultiscreen/KKMutiScreenTvApp;->mApp:Lcom/konka/kkmultiscreen/KKMutiScreenTvApp;

    const-string v0, "_server_state_pref"

    sput-object v0, Lcom/konka/kkmultiscreen/KKMutiScreenTvApp;->PREF_NAME:Ljava/lang/String;

    return-void
.end method

.method public constructor <init>()V
    .locals 2

    const/4 v1, 0x0

    const/4 v0, 0x0

    invoke-direct {p0}, Landroid/app/Application;-><init>()V

    iput-object v0, p0, Lcom/konka/kkmultiscreen/KKMutiScreenTvApp;->mAppContext:Landroid/content/Context;

    iput-object v0, p0, Lcom/konka/kkmultiscreen/KKMutiScreenTvApp;->dataHandle:Lcom/konka/kkmultiscreen/DataHelper;

    iput-object v0, p0, Lcom/konka/kkmultiscreen/KKMutiScreenTvApp;->receiver:Lcom/konka/kkmultiscreen/KKMutiScreenTvApp$UpdateReceiver;

    iput-object v0, p0, Lcom/konka/kkmultiscreen/KKMutiScreenTvApp;->sysManager:Lcom/konka/InitApp/CInitAppSys;

    iput-object v0, p0, Lcom/konka/kkmultiscreen/KKMutiScreenTvApp;->sysWifiDirect:Lcom/konka/InitApp/CWifiDirect;

    iput-boolean v1, p0, Lcom/konka/kkmultiscreen/KKMutiScreenTvApp;->hasBind:Z

    iput-boolean v1, p0, Lcom/konka/kkmultiscreen/KKMutiScreenTvApp;->showTag:Z

    new-instance v0, Lcom/konka/kkmultiscreen/KKMutiScreenTvApp$1;

    invoke-direct {v0, p0}, Lcom/konka/kkmultiscreen/KKMutiScreenTvApp$1;-><init>(Lcom/konka/kkmultiscreen/KKMutiScreenTvApp;)V

    iput-object v0, p0, Lcom/konka/kkmultiscreen/KKMutiScreenTvApp;->mApplicationHandler:Landroid/os/Handler;

    iput-boolean v1, p0, Lcom/konka/kkmultiscreen/KKMutiScreenTvApp;->mHasInited:Z

    return-void
.end method

.method static synthetic access$0()Ljava/lang/String;
    .locals 1

    sget-object v0, Lcom/konka/kkmultiscreen/KKMutiScreenTvApp;->TAG:Ljava/lang/String;

    return-object v0
.end method


# virtual methods
.method public SendDataToService(Ljava/lang/String;[B)V
    .locals 4
    .param p1    # Ljava/lang/String;
    .param p2    # [B

    const-string v2, "weikan"

    const-string v3, "have been send data to service!!!"

    invoke-static {v2, v3}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    new-instance v1, Landroid/content/Intent;

    iget-object v2, p0, Lcom/konka/kkmultiscreen/KKMutiScreenTvApp;->mAppContext:Landroid/content/Context;

    const-class v3, Lcom/konka/kkmultiscreen/DataService;

    invoke-direct {v1, v2, v3}, Landroid/content/Intent;-><init>(Landroid/content/Context;Ljava/lang/Class;)V

    new-instance v0, Landroid/os/Bundle;

    invoke-direct {v0}, Landroid/os/Bundle;-><init>()V

    const-string v2, "KEY"

    invoke-virtual {v0, v2, p1}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V

    const-string v2, "DATA"

    invoke-virtual {v0, v2, p2}, Landroid/os/Bundle;->putByteArray(Ljava/lang/String;[B)V

    invoke-virtual {v1, v0}, Landroid/content/Intent;->putExtras(Landroid/os/Bundle;)Landroid/content/Intent;

    iget-object v2, p0, Lcom/konka/kkmultiscreen/KKMutiScreenTvApp;->mAppContext:Landroid/content/Context;

    invoke-virtual {v2, v1}, Landroid/content/Context;->startService(Landroid/content/Intent;)Landroid/content/ComponentName;

    return-void
.end method

.method public ToOuterMsgToClient(Ljava/lang/String;[B)V
    .locals 4
    .param p1    # Ljava/lang/String;
    .param p2    # [B

    const/4 v0, 0x0

    :goto_0
    array-length v1, p2

    if-lt v0, v1, :cond_0

    iget-object v1, p0, Lcom/konka/kkmultiscreen/KKMutiScreenTvApp;->sysManager:Lcom/konka/InitApp/CInitAppSys;

    iget-object v1, v1, Lcom/konka/InitApp/CInitAppSys;->userCenter:LdataLayer/dataCenter/userGroup;

    invoke-virtual {v1, p1, p2}, LdataLayer/dataCenter/userGroup;->sendData(Ljava/lang/String;[B)V

    return-void

    :cond_0
    sget-object v1, Lcom/konka/kkmultiscreen/KKMutiScreenTvApp;->TAG:Ljava/lang/String;

    new-instance v2, Ljava/lang/StringBuilder;

    const-string v3, "bytesToSend[i]==========: "

    invoke-direct {v2, v3}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    aget-byte v3, p2, v0

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    add-int/lit8 v0, v0, 0x1

    goto :goto_0
.end method

.method public ToRegisterReceiver()V
    .locals 3

    sget-object v1, Lcom/konka/kkmultiscreen/KKMutiScreenTvApp;->TAG:Ljava/lang/String;

    const-string v2, "ToRegisterReceiver"

    invoke-static {v1, v2}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    iget-object v1, p0, Lcom/konka/kkmultiscreen/KKMutiScreenTvApp;->receiver:Lcom/konka/kkmultiscreen/KKMutiScreenTvApp$UpdateReceiver;

    if-eqz v1, :cond_0

    :goto_0
    return-void

    :cond_0
    new-instance v1, Lcom/konka/kkmultiscreen/KKMutiScreenTvApp$UpdateReceiver;

    invoke-direct {v1, p0}, Lcom/konka/kkmultiscreen/KKMutiScreenTvApp$UpdateReceiver;-><init>(Lcom/konka/kkmultiscreen/KKMutiScreenTvApp;)V

    iput-object v1, p0, Lcom/konka/kkmultiscreen/KKMutiScreenTvApp;->receiver:Lcom/konka/kkmultiscreen/KKMutiScreenTvApp$UpdateReceiver;

    new-instance v0, Landroid/content/IntentFilter;

    invoke-direct {v0}, Landroid/content/IntentFilter;-><init>()V

    const-string v1, "andriod.tv.network.ack"

    invoke-virtual {v0, v1}, Landroid/content/IntentFilter;->addAction(Ljava/lang/String;)V

    iget-object v1, p0, Lcom/konka/kkmultiscreen/KKMutiScreenTvApp;->receiver:Lcom/konka/kkmultiscreen/KKMutiScreenTvApp$UpdateReceiver;

    invoke-virtual {p0, v1, v0}, Lcom/konka/kkmultiscreen/KKMutiScreenTvApp;->registerReceiver(Landroid/content/BroadcastReceiver;Landroid/content/IntentFilter;)Landroid/content/Intent;

    goto :goto_0
.end method

.method public ToSendInnerMsgToService(Ljava/lang/String;I)V
    .locals 3
    .param p1    # Ljava/lang/String;
    .param p2    # I

    sget-object v1, Lcom/konka/kkmultiscreen/KKMutiScreenTvApp;->TAG:Ljava/lang/String;

    const-string v2, "ToSendInnerMsgToService"

    invoke-static {v1, v2}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    new-instance v0, Lcom/konka/kkmultiscreen/MsgDef$MsgInner;

    invoke-direct {v0}, Lcom/konka/kkmultiscreen/MsgDef$MsgInner;-><init>()V

    invoke-virtual {v0, p2}, Lcom/konka/kkmultiscreen/MsgDef$MsgInner;->setType(I)V

    invoke-virtual {v0, p1}, Lcom/konka/kkmultiscreen/MsgDef$MsgInner;->SetData(Ljava/lang/String;)V

    iget-object v1, p0, Lcom/konka/kkmultiscreen/KKMutiScreenTvApp;->dataHandle:Lcom/konka/kkmultiscreen/DataHelper;

    if-eqz v1, :cond_0

    iget-object v1, p0, Lcom/konka/kkmultiscreen/KKMutiScreenTvApp;->dataHandle:Lcom/konka/kkmultiscreen/DataHelper;

    invoke-virtual {v1, v0}, Lcom/konka/kkmultiscreen/DataHelper;->SendInnerMsgToService(Lcom/konka/kkmultiscreen/MsgDef$MsgInner;)Landroid/os/Parcel;

    :goto_0
    return-void

    :cond_0
    sget-object v1, Lcom/konka/kkmultiscreen/KKMutiScreenTvApp;->TAG:Ljava/lang/String;

    const-string v2, "error: dataHandle==null"

    invoke-static {v1, v2}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_0
.end method

.method public ToSendMsgToService(Landroid/content/Intent;)V
    .locals 1
    .param p1    # Landroid/content/Intent;

    iget-object v0, p0, Lcom/konka/kkmultiscreen/KKMutiScreenTvApp;->dataHandle:Lcom/konka/kkmultiscreen/DataHelper;

    invoke-virtual {v0, p1}, Lcom/konka/kkmultiscreen/DataHelper;->SendDataToService(Landroid/content/Intent;)V

    return-void
.end method

.method public ToStartSerives()V
    .locals 2

    sget-object v0, Lcom/konka/kkmultiscreen/KKMutiScreenTvApp;->TAG:Ljava/lang/String;

    const-string v1, "ToStartSerives"

    invoke-static {v0, v1}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    iget-object v0, p0, Lcom/konka/kkmultiscreen/KKMutiScreenTvApp;->dataHandle:Lcom/konka/kkmultiscreen/DataHelper;

    if-eqz v0, :cond_0

    :goto_0
    return-void

    :cond_0
    new-instance v0, Lcom/konka/kkmultiscreen/DataHelper;

    iget-object v1, p0, Lcom/konka/kkmultiscreen/KKMutiScreenTvApp;->mAppContext:Landroid/content/Context;

    invoke-direct {v0, v1}, Lcom/konka/kkmultiscreen/DataHelper;-><init>(Landroid/content/Context;)V

    iput-object v0, p0, Lcom/konka/kkmultiscreen/KKMutiScreenTvApp;->dataHandle:Lcom/konka/kkmultiscreen/DataHelper;

    goto :goto_0
.end method

.method public ToStopServices()V
    .locals 4

    const/4 v3, 0x0

    sget-object v1, Lcom/konka/kkmultiscreen/KKMutiScreenTvApp;->TAG:Ljava/lang/String;

    const-string v2, "ToStopServices"

    invoke-static {v1, v2}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    iget-object v1, p0, Lcom/konka/kkmultiscreen/KKMutiScreenTvApp;->receiver:Lcom/konka/kkmultiscreen/KKMutiScreenTvApp$UpdateReceiver;

    if-eqz v1, :cond_0

    iget-object v1, p0, Lcom/konka/kkmultiscreen/KKMutiScreenTvApp;->receiver:Lcom/konka/kkmultiscreen/KKMutiScreenTvApp$UpdateReceiver;

    invoke-virtual {p0, v1}, Lcom/konka/kkmultiscreen/KKMutiScreenTvApp;->unregisterReceiver(Landroid/content/BroadcastReceiver;)V

    iput-object v3, p0, Lcom/konka/kkmultiscreen/KKMutiScreenTvApp;->receiver:Lcom/konka/kkmultiscreen/KKMutiScreenTvApp$UpdateReceiver;

    :cond_0
    new-instance v0, Lcom/konka/kkmultiscreen/MsgDef$MsgInner;

    invoke-direct {v0}, Lcom/konka/kkmultiscreen/MsgDef$MsgInner;-><init>()V

    invoke-virtual {v0}, Lcom/konka/kkmultiscreen/MsgDef$MsgInner;->setExitSerivce()V

    iget-object v1, p0, Lcom/konka/kkmultiscreen/KKMutiScreenTvApp;->dataHandle:Lcom/konka/kkmultiscreen/DataHelper;

    invoke-static {v1, v0}, Lcom/konka/kkmultiscreen/DataSR;->ToSendInnerData(Lcom/konka/kkmultiscreen/DataHelper;Lcom/konka/kkmultiscreen/MsgDef$MsgInner;)V

    iput-object v3, p0, Lcom/konka/kkmultiscreen/KKMutiScreenTvApp;->dataHandle:Lcom/konka/kkmultiscreen/DataHelper;

    return-void
.end method

.method public closeTouchEffect(Landroid/content/ContentResolver;)V
    .locals 2
    .param p1    # Landroid/content/ContentResolver;

    const-string v0, "show_touches"

    const/4 v1, 0x0

    invoke-static {p1, v0, v1}, Landroid/provider/Settings$System;->putInt(Landroid/content/ContentResolver;Ljava/lang/String;I)Z

    return-void
.end method

.method public getBussinessPlatformInfo()Ljava/lang/String;
    .locals 4

    const-string v0, "11101011000"

    sget-object v1, Ljava/lang/System;->out:Ljava/io/PrintStream;

    new-instance v2, Ljava/lang/StringBuilder;

    const-string v3, "mBFlag==========="

    invoke-direct {v2, v3}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/io/PrintStream;->println(Ljava/lang/String;)V

    return-object v0
.end method

.method public getConfigPref()Landroid/content/SharedPreferences;
    .locals 3

    const-string v1, "_config_pref"

    const/4 v2, 0x0

    invoke-virtual {p0, v1, v2}, Lcom/konka/kkmultiscreen/KKMutiScreenTvApp;->getSharedPreferences(Ljava/lang/String;I)Landroid/content/SharedPreferences;

    move-result-object v0

    return-object v0
.end method

.method public getCrashFileDir()Ljava/io/File;
    .locals 3

    const-string v1, "crash"

    const/4 v2, 0x0

    invoke-virtual {p0, v1, v2}, Lcom/konka/kkmultiscreen/KKMutiScreenTvApp;->getDir(Ljava/lang/String;I)Ljava/io/File;

    move-result-object v0

    invoke-virtual {v0}, Ljava/io/File;->exists()Z

    move-result v1

    if-nez v1, :cond_0

    invoke-virtual {v0}, Ljava/io/File;->mkdirs()Z

    :cond_0
    return-object v0
.end method

.method public getPref()Landroid/content/SharedPreferences;
    .locals 2

    sget-object v0, Lcom/konka/kkmultiscreen/KKMutiScreenTvApp;->PREF_NAME:Ljava/lang/String;

    const/4 v1, 0x0

    invoke-virtual {p0, v0, v1}, Lcom/konka/kkmultiscreen/KKMutiScreenTvApp;->getSharedPreferences(Ljava/lang/String;I)Landroid/content/SharedPreferences;

    move-result-object v0

    return-object v0
.end method

.method public getServerStatePref()Landroid/content/SharedPreferences;
    .locals 3

    const-string v1, "_server_state"

    const/4 v2, 0x0

    invoke-virtual {p0, v1, v2}, Lcom/konka/kkmultiscreen/KKMutiScreenTvApp;->getSharedPreferences(Ljava/lang/String;I)Landroid/content/SharedPreferences;

    move-result-object v0

    return-object v0
.end method

.method public getStatePref()Landroid/content/SharedPreferences;
    .locals 3

    const-string v1, "_cur_state"

    const/4 v2, 0x0

    invoke-virtual {p0, v1, v2}, Lcom/konka/kkmultiscreen/KKMutiScreenTvApp;->getSharedPreferences(Ljava/lang/String;I)Landroid/content/SharedPreferences;

    move-result-object v0

    return-object v0
.end method

.method public hasInited()Z
    .locals 1

    iget-boolean v0, p0, Lcom/konka/kkmultiscreen/KKMutiScreenTvApp;->mHasInited:Z

    return v0
.end method

.method public isFirstStartup()Z
    .locals 6

    const/4 v5, 0x0

    const-string v3, "_is_first_startup"

    invoke-virtual {p0, v3, v5}, Lcom/konka/kkmultiscreen/KKMutiScreenTvApp;->getSharedPreferences(Ljava/lang/String;I)Landroid/content/SharedPreferences;

    move-result-object v2

    const-string v3, "is_first_startup"

    const/4 v4, 0x1

    invoke-interface {v2, v3, v4}, Landroid/content/SharedPreferences;->getBoolean(Ljava/lang/String;Z)Z

    move-result v1

    if-eqz v1, :cond_0

    invoke-interface {v2}, Landroid/content/SharedPreferences;->edit()Landroid/content/SharedPreferences$Editor;

    move-result-object v0

    const-string v3, "is_first_startup"

    invoke-interface {v0, v3, v5}, Landroid/content/SharedPreferences$Editor;->putBoolean(Ljava/lang/String;Z)Landroid/content/SharedPreferences$Editor;

    invoke-interface {v0}, Landroid/content/SharedPreferences$Editor;->commit()Z

    :cond_0
    return v1
.end method

.method public isHighResolution()Z
    .locals 2

    const/16 v0, 0x438

    const/16 v1, 0x1e0

    if-lt v0, v1, :cond_0

    const/4 v1, 0x1

    :goto_0
    return v1

    :cond_0
    const/4 v1, 0x0

    goto :goto_0
.end method

.method public onCreate()V
    .locals 5

    const/4 v4, 0x0

    const/16 v3, 0xa

    invoke-super {p0}, Landroid/app/Application;->onCreate()V

    sget-object v0, Lcom/konka/kkmultiscreen/KKMutiScreenTvApp;->mApp:Lcom/konka/kkmultiscreen/KKMutiScreenTvApp;

    if-eqz v0, :cond_0

    :goto_0
    return-void

    :cond_0
    sput-object p0, Lcom/konka/kkmultiscreen/KKMutiScreenTvApp;->mApp:Lcom/konka/kkmultiscreen/KKMutiScreenTvApp;

    invoke-virtual {p0}, Lcom/konka/kkmultiscreen/KKMutiScreenTvApp;->getBaseContext()Landroid/content/Context;

    move-result-object v0

    iput-object v0, p0, Lcom/konka/kkmultiscreen/KKMutiScreenTvApp;->mAppContext:Landroid/content/Context;

    sget-object v0, Lcom/konka/kkmultiscreen/KKMutiScreenTvApp;->TAG:Ljava/lang/String;

    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "---onCreate--- context: "

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {p0}, Lcom/konka/kkmultiscreen/KKMutiScreenTvApp;->getBaseContext()Landroid/content/Context;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lutil/log/CLog;->e(Ljava/lang/String;Ljava/lang/String;)V

    invoke-static {v3}, Landroid/os/Process;->setThreadPriority(I)V

    invoke-static {}, Landroid/os/Process;->myTid()I

    move-result v0

    invoke-static {v0, v3}, Landroid/os/Process;->setThreadPriority(II)V

    iput-object v4, p0, Lcom/konka/kkmultiscreen/KKMutiScreenTvApp;->sysManager:Lcom/konka/InitApp/CInitAppSys;

    new-instance v0, Lcom/konka/InitApp/CInitAppSys;

    sget-object v1, Lcom/konka/kkmultiscreen/KKMutiScreenTvApp;->mApp:Lcom/konka/kkmultiscreen/KKMutiScreenTvApp;

    invoke-direct {v0, v1}, Lcom/konka/InitApp/CInitAppSys;-><init>(Landroid/app/Application;)V

    iput-object v0, p0, Lcom/konka/kkmultiscreen/KKMutiScreenTvApp;->sysManager:Lcom/konka/InitApp/CInitAppSys;

    iput-object v4, p0, Lcom/konka/kkmultiscreen/KKMutiScreenTvApp;->sysWifiDirect:Lcom/konka/InitApp/CWifiDirect;

    new-instance v0, Lcom/konka/InitApp/CWifiDirect;

    sget-object v1, Lcom/konka/kkmultiscreen/KKMutiScreenTvApp;->mApp:Lcom/konka/kkmultiscreen/KKMutiScreenTvApp;

    invoke-direct {v0, v1}, Lcom/konka/InitApp/CWifiDirect;-><init>(Lcom/konka/kkmultiscreen/KKMutiScreenTvApp;)V

    iput-object v0, p0, Lcom/konka/kkmultiscreen/KKMutiScreenTvApp;->sysWifiDirect:Lcom/konka/InitApp/CWifiDirect;

    goto :goto_0
.end method

.method public onTerminate()V
    .locals 2

    invoke-super {p0}, Landroid/app/Application;->onTerminate()V

    sget-object v0, Lcom/konka/kkmultiscreen/KKMutiScreenTvApp;->TAG:Ljava/lang/String;

    const-string v1, "---onTerminate---"

    invoke-static {v0, v1}, Lutil/log/CLog;->e(Ljava/lang/String;Ljava/lang/String;)V

    const/4 v0, 0x0

    sput-object v0, Lcom/konka/kkmultiscreen/KKMutiScreenTvApp;->mApp:Lcom/konka/kkmultiscreen/KKMutiScreenTvApp;

    iget-object v0, p0, Lcom/konka/kkmultiscreen/KKMutiScreenTvApp;->sysWifiDirect:Lcom/konka/InitApp/CWifiDirect;

    invoke-virtual {v0}, Lcom/konka/InitApp/CWifiDirect;->unregisterWifiDirectReceiver()V

    invoke-virtual {p0}, Lcom/konka/kkmultiscreen/KKMutiScreenTvApp;->ToStopServices()V

    const/4 v0, 0x0

    invoke-static {v0}, Ljava/lang/System;->exit(I)V

    return-void
.end method

.method public openTouchEffect(Landroid/content/ContentResolver;)V
    .locals 2
    .param p1    # Landroid/content/ContentResolver;

    const-string v0, "show_touches"

    const/4 v1, 0x1

    invoke-static {p1, v0, v1}, Landroid/provider/Settings$System;->putInt(Landroid/content/ContentResolver;Ljava/lang/String;I)Z

    return-void
.end method

.method public setHasInited()V
    .locals 1

    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/konka/kkmultiscreen/KKMutiScreenTvApp;->mHasInited:Z

    return-void
.end method
