.class Lcom/konka/kkmultiscreen/DataHelper$1;
.super Ljava/lang/Object;
.source "DataHelper.java"

# interfaces
.implements Landroid/content/ServiceConnection;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/konka/kkmultiscreen/DataHelper;->init()Z
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/konka/kkmultiscreen/DataHelper;


# direct methods
.method constructor <init>(Lcom/konka/kkmultiscreen/DataHelper;)V
    .locals 0

    iput-object p1, p0, Lcom/konka/kkmultiscreen/DataHelper$1;->this$0:Lcom/konka/kkmultiscreen/DataHelper;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onServiceConnected(Landroid/content/ComponentName;Landroid/os/IBinder;)V
    .locals 2
    .param p1    # Landroid/content/ComponentName;
    .param p2    # Landroid/os/IBinder;

    const-string v0, "DataHelper"

    const-string v1, "onServiceConnected"

    invoke-static {v0, v1}, Lutil/log/CLog;->eLog(Ljava/lang/String;Ljava/lang/String;)V

    const/4 v0, 0x1

    invoke-static {v0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v0

    invoke-static {v0}, Lcom/konka/kkmultiscreen/DataHelper;->access$0(Ljava/lang/Integer;)V

    check-cast p2, Lcom/konka/kkmultiscreen/DataService$MyBinder;

    invoke-virtual {p2}, Lcom/konka/kkmultiscreen/DataService$MyBinder;->getService()Lcom/konka/kkmultiscreen/DataService;

    move-result-object v0

    sput-object v0, Lcom/konka/kkmultiscreen/DataHelper;->myService:Lcom/konka/kkmultiscreen/DataService;

    iget-object v0, p0, Lcom/konka/kkmultiscreen/DataHelper$1;->this$0:Lcom/konka/kkmultiscreen/DataHelper;

    # getter for: Lcom/konka/kkmultiscreen/DataHelper;->innerCacheMsg:Lcom/konka/kkmultiscreen/MsgDef$MsgInner;
    invoke-static {v0}, Lcom/konka/kkmultiscreen/DataHelper;->access$3(Lcom/konka/kkmultiscreen/DataHelper;)Lcom/konka/kkmultiscreen/MsgDef$MsgInner;

    move-result-object v0

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/konka/kkmultiscreen/DataHelper$1;->this$0:Lcom/konka/kkmultiscreen/DataHelper;

    iget-object v1, p0, Lcom/konka/kkmultiscreen/DataHelper$1;->this$0:Lcom/konka/kkmultiscreen/DataHelper;

    # getter for: Lcom/konka/kkmultiscreen/DataHelper;->innerCacheMsg:Lcom/konka/kkmultiscreen/MsgDef$MsgInner;
    invoke-static {v1}, Lcom/konka/kkmultiscreen/DataHelper;->access$3(Lcom/konka/kkmultiscreen/DataHelper;)Lcom/konka/kkmultiscreen/MsgDef$MsgInner;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/konka/kkmultiscreen/DataHelper;->SendInnerMsgToService(Lcom/konka/kkmultiscreen/MsgDef$MsgInner;)Landroid/os/Parcel;

    iget-object v0, p0, Lcom/konka/kkmultiscreen/DataHelper$1;->this$0:Lcom/konka/kkmultiscreen/DataHelper;

    const/4 v1, 0x0

    invoke-static {v0, v1}, Lcom/konka/kkmultiscreen/DataHelper;->access$4(Lcom/konka/kkmultiscreen/DataHelper;Lcom/konka/kkmultiscreen/MsgDef$MsgInner;)V

    :cond_0
    return-void
.end method

.method public onServiceDisconnected(Landroid/content/ComponentName;)V
    .locals 3
    .param p1    # Landroid/content/ComponentName;

    const/4 v2, 0x0

    const-string v0, "DataHelper"

    const-string v1, "onServiceDisconnected"

    invoke-static {v0, v1}, Lutil/log/CLog;->eLog(Ljava/lang/String;Ljava/lang/String;)V

    const/4 v0, 0x0

    sput-object v0, Lcom/konka/kkmultiscreen/DataHelper;->myService:Lcom/konka/kkmultiscreen/DataService;

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v0

    invoke-static {v0}, Lcom/konka/kkmultiscreen/DataHelper;->access$0(Ljava/lang/Integer;)V

    # getter for: Lcom/konka/kkmultiscreen/DataHelper;->serviceConnection:Landroid/content/ServiceConnection;
    invoke-static {}, Lcom/konka/kkmultiscreen/DataHelper;->access$1()Landroid/content/ServiceConnection;

    move-result-object v0

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/konka/kkmultiscreen/DataHelper$1;->this$0:Lcom/konka/kkmultiscreen/DataHelper;

    # getter for: Lcom/konka/kkmultiscreen/DataHelper;->activity:Landroid/content/Context;
    invoke-static {v0}, Lcom/konka/kkmultiscreen/DataHelper;->access$2(Lcom/konka/kkmultiscreen/DataHelper;)Landroid/content/Context;

    move-result-object v0

    # getter for: Lcom/konka/kkmultiscreen/DataHelper;->serviceConnection:Landroid/content/ServiceConnection;
    invoke-static {}, Lcom/konka/kkmultiscreen/DataHelper;->access$1()Landroid/content/ServiceConnection;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/content/Context;->unbindService(Landroid/content/ServiceConnection;)V

    :cond_0
    iget-object v0, p0, Lcom/konka/kkmultiscreen/DataHelper$1;->this$0:Lcom/konka/kkmultiscreen/DataHelper;

    # getter for: Lcom/konka/kkmultiscreen/DataHelper;->activity:Landroid/content/Context;
    invoke-static {v0}, Lcom/konka/kkmultiscreen/DataHelper;->access$2(Lcom/konka/kkmultiscreen/DataHelper;)Landroid/content/Context;

    move-result-object v0

    const-string v1, "2130968595"

    invoke-static {v0, v1, v2}, Landroid/widget/Toast;->makeText(Landroid/content/Context;Ljava/lang/CharSequence;I)Landroid/widget/Toast;

    move-result-object v0

    invoke-virtual {v0}, Landroid/widget/Toast;->show()V

    return-void
.end method
