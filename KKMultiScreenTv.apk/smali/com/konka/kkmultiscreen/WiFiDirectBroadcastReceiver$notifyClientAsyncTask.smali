.class public Lcom/konka/kkmultiscreen/WiFiDirectBroadcastReceiver$notifyClientAsyncTask;
.super Landroid/os/AsyncTask;
.source "WiFiDirectBroadcastReceiver.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/konka/kkmultiscreen/WiFiDirectBroadcastReceiver;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x9
    name = "notifyClientAsyncTask"
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Landroid/os/AsyncTask",
        "<",
        "Ljava/lang/Void;",
        "Ljava/lang/Void;",
        "Ljava/lang/String;",
        ">;"
    }
.end annotation


# static fields
.field private static final TAG:Ljava/lang/String; = "notifyClientAsyncTask"


# direct methods
.method public constructor <init>()V
    .locals 0

    invoke-direct {p0}, Landroid/os/AsyncTask;-><init>()V

    return-void
.end method


# virtual methods
.method protected bridge varargs synthetic doInBackground([Ljava/lang/Object;)Ljava/lang/Object;
    .locals 1

    check-cast p1, [Ljava/lang/Void;

    invoke-virtual {p0, p1}, Lcom/konka/kkmultiscreen/WiFiDirectBroadcastReceiver$notifyClientAsyncTask;->doInBackground([Ljava/lang/Void;)Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method protected varargs doInBackground([Ljava/lang/Void;)Ljava/lang/String;
    .locals 14
    .param p1    # [Ljava/lang/Void;

    :try_start_0
    # getter for: Lcom/konka/kkmultiscreen/WiFiDirectBroadcastReceiver;->mApp:Lcom/konka/kkmultiscreen/KKMutiScreenTvApp;
    invoke-static {}, Lcom/konka/kkmultiscreen/WiFiDirectBroadcastReceiver;->access$0()Lcom/konka/kkmultiscreen/KKMutiScreenTvApp;

    move-result-object v10

    # getter for: Lcom/konka/kkmultiscreen/WiFiDirectBroadcastReceiver;->mApp:Lcom/konka/kkmultiscreen/KKMutiScreenTvApp;
    invoke-static {}, Lcom/konka/kkmultiscreen/WiFiDirectBroadcastReceiver;->access$0()Lcom/konka/kkmultiscreen/KKMutiScreenTvApp;

    move-result-object v11

    invoke-virtual {v11}, Lcom/konka/kkmultiscreen/KKMutiScreenTvApp;->getApplicationContext()Landroid/content/Context;

    const-string v11, "wifi"

    invoke-virtual {v10, v11}, Lcom/konka/kkmultiscreen/KKMutiScreenTvApp;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v4

    check-cast v4, Landroid/net/wifi/WifiManager;

    if-eqz v4, :cond_0

    invoke-virtual {v4}, Landroid/net/wifi/WifiManager;->isWifiEnabled()Z

    move-result v10

    if-nez v10, :cond_0

    const/4 v10, 0x1

    invoke-virtual {v4, v10}, Landroid/net/wifi/WifiManager;->setWifiEnabled(Z)Z

    :cond_0
    sget-object v10, Lcom/konka/kkmultiscreen/WiFiDirectBroadcastReceiver$DeviceListFragment;->serverIp:Ljava/lang/String;

    invoke-static {v10}, Ljava/net/InetAddress;->getByName(Ljava/lang/String;)Ljava/net/InetAddress;

    move-result-object v0

    const-string v10, "notifyClientAsyncTask"

    const-string v11, "tv is client: Socket opened"

    invoke-static {v10, v11}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    new-instance v9, Ljava/net/Socket;

    invoke-direct {v9}, Ljava/net/Socket;-><init>()V

    const/4 v10, 0x0

    invoke-virtual {v9, v10}, Ljava/net/Socket;->bind(Ljava/net/SocketAddress;)V

    new-instance v10, Ljava/net/InetSocketAddress;

    const/16 v11, 0x3039

    invoke-direct {v10, v0, v11}, Ljava/net/InetSocketAddress;-><init>(Ljava/net/InetAddress;I)V

    const/16 v11, 0x2710

    invoke-virtual {v9, v10, v11}, Ljava/net/Socket;->connect(Ljava/net/SocketAddress;I)V

    const-string v10, "notifyClientAsyncTask"

    new-instance v11, Ljava/lang/StringBuilder;

    const-string v12, "client: connection done "

    invoke-direct {v11, v12}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v9}, Ljava/net/Socket;->isConnected()Z

    move-result v12

    invoke-virtual {v11, v12}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v11

    const-string v12, " sever ip "

    invoke-virtual {v11, v12}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v11

    sget-object v12, Lcom/konka/kkmultiscreen/WiFiDirectBroadcastReceiver$DeviceListFragment;->serverIp:Ljava/lang/String;

    invoke-virtual {v11, v12}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v11

    invoke-virtual {v11}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v11

    invoke-static {v10, v11}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I
    :try_end_0
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_1

    :try_start_1
    invoke-virtual {v9}, Ljava/net/Socket;->getInputStream()Ljava/io/InputStream;

    move-result-object v3

    new-instance v5, Ljava/io/DataInputStream;

    invoke-direct {v5, v3}, Ljava/io/DataInputStream;-><init>(Ljava/io/InputStream;)V

    const/16 v10, 0x400

    new-array v1, v10, [B

    const-string v10, "notifyClientAsyncTask"

    const-string v11, "tv is client: Socket to read data!"

    invoke-static {v10, v11}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    invoke-virtual {v5, v1}, Ljava/io/DataInputStream;->read([B)I

    move-result v6

    if-lez v6, :cond_2

    new-instance v10, Ljava/lang/String;

    const-string v11, "UTF-8"

    invoke-direct {v10, v1, v11}, Ljava/lang/String;-><init>([BLjava/lang/String;)V

    invoke-virtual {v10}, Ljava/lang/String;->trim()Ljava/lang/String;

    move-result-object v7

    const-string v10, "notifyClientAsyncTask"

    new-instance v11, Ljava/lang/StringBuilder;

    const-string v12, "tv is client: Socket had read data: "

    invoke-direct {v11, v12}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v11, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v11

    const-string v12, " ret "

    invoke-virtual {v11, v12}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v11

    invoke-virtual {v11, v6}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v11

    invoke-virtual {v11}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v11

    invoke-static {v10, v11}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    invoke-virtual {v7}, Ljava/lang/String;->length()I

    move-result v10

    if-lez v10, :cond_2

    sget-object v10, Lnetwork/udp/BroadThread;->connMap:Ljava/util/Map;

    if-eqz v10, :cond_2

    sget-object v10, Lnetwork/udp/BroadThread;->connMap:Ljava/util/Map;

    sget-object v11, Lcom/konka/kkmultiscreen/WiFiDirectBroadcastReceiver$DeviceListFragment;->serverIp:Ljava/lang/String;

    invoke-static {v11}, Lutil/comm/ToolClass;->getStringIpToLong(Ljava/lang/String;)J

    move-result-wide v11

    invoke-static {v11, v12}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v11

    invoke-interface {v10, v11}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v10

    if-nez v10, :cond_1

    sget-object v10, Lnetwork/udp/BroadThread;->connMap:Ljava/util/Map;

    sget-object v11, Lcom/konka/kkmultiscreen/WiFiDirectBroadcastReceiver$DeviceListFragment;->serverIp:Ljava/lang/String;

    invoke-static {v11}, Lutil/comm/ToolClass;->getStringIpToLong(Ljava/lang/String;)J

    move-result-wide v11

    invoke-static {v11, v12}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v11

    invoke-interface {v10, v11}, Ljava/util/Map;->remove(Ljava/lang/Object;)Ljava/lang/Object;

    :cond_1
    sget-object v10, Lnetwork/udp/BroadThread;->connMap:Ljava/util/Map;

    sget-object v11, Lcom/konka/kkmultiscreen/WiFiDirectBroadcastReceiver$DeviceListFragment;->serverIp:Ljava/lang/String;

    invoke-static {v11}, Lutil/comm/ToolClass;->getStringIpToLong(Ljava/lang/String;)J

    move-result-wide v11

    invoke-static {v11, v12}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v11

    new-instance v12, Ljava/lang/StringBuilder;

    sget-object v13, Lcom/konka/kkmultiscreen/WiFiDirectBroadcastReceiver$DeviceListFragment;->serverIp:Ljava/lang/String;

    invoke-static {v13}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v13

    invoke-direct {v12, v13}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    const-string v13, "&"

    invoke-virtual {v12, v13}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v12

    invoke-virtual {v12, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v12

    invoke-virtual {v12}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v12

    invoke-interface {v10, v11, v12}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    :cond_2
    invoke-virtual {v5}, Ljava/io/DataInputStream;->close()V

    invoke-virtual {v3}, Ljava/io/InputStream;->close()V
    :try_end_1
    .catch Ljava/io/IOException; {:try_start_1 .. :try_end_1} :catch_0

    :try_start_2
    invoke-virtual {v9}, Ljava/net/Socket;->close()V
    :try_end_2
    .catch Ljava/io/IOException; {:try_start_2 .. :try_end_2} :catch_1

    const-string v10, "send tv ip to client by this way!!!"

    :goto_0
    return-object v10

    :catch_0
    move-exception v8

    const/4 v10, 0x0

    goto :goto_0

    :catch_1
    move-exception v2

    const-string v10, "notifyClientAsyncTask"

    invoke-virtual {v2}, Ljava/io/IOException;->getMessage()Ljava/lang/String;

    move-result-object v11

    invoke-static {v10, v11}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    const/4 v10, 0x0

    goto :goto_0
.end method

.method protected bridge synthetic onPostExecute(Ljava/lang/Object;)V
    .locals 0

    check-cast p1, Ljava/lang/String;

    invoke-virtual {p0, p1}, Lcom/konka/kkmultiscreen/WiFiDirectBroadcastReceiver$notifyClientAsyncTask;->onPostExecute(Ljava/lang/String;)V

    return-void
.end method

.method protected onPostExecute(Ljava/lang/String;)V
    .locals 0
    .param p1    # Ljava/lang/String;

    return-void
.end method

.method protected onPreExecute()V
    .locals 0

    return-void
.end method
