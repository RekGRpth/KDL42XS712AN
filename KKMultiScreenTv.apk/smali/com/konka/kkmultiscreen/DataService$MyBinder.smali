.class public Lcom/konka/kkmultiscreen/DataService$MyBinder;
.super Landroid/os/Binder;
.source "DataService.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/konka/kkmultiscreen/DataService;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x1
    name = "MyBinder"
.end annotation


# instance fields
.field final synthetic this$0:Lcom/konka/kkmultiscreen/DataService;


# direct methods
.method public constructor <init>(Lcom/konka/kkmultiscreen/DataService;)V
    .locals 0

    iput-object p1, p0, Lcom/konka/kkmultiscreen/DataService$MyBinder;->this$0:Lcom/konka/kkmultiscreen/DataService;

    invoke-direct {p0}, Landroid/os/Binder;-><init>()V

    return-void
.end method


# virtual methods
.method public getService()Lcom/konka/kkmultiscreen/DataService;
    .locals 2

    const-string v0, "DataService"

    const-string v1, "getService."

    invoke-static {v0, v1}, Lutil/log/CLog;->wLog(Ljava/lang/String;Ljava/lang/String;)V

    iget-object v0, p0, Lcom/konka/kkmultiscreen/DataService$MyBinder;->this$0:Lcom/konka/kkmultiscreen/DataService;

    # getter for: Lcom/konka/kkmultiscreen/DataService;->packethandle:LprotocolAnalysis/analysis/packetHandle;
    invoke-static {v0}, Lcom/konka/kkmultiscreen/DataService;->access$0(Lcom/konka/kkmultiscreen/DataService;)LprotocolAnalysis/analysis/packetHandle;

    move-result-object v0

    sget-object v1, Lcom/konka/kkmultiscreen/KKMutiScreenTvApp;->mApp:Lcom/konka/kkmultiscreen/KKMutiScreenTvApp;

    iget-object v1, v1, Lcom/konka/kkmultiscreen/KKMutiScreenTvApp;->mAppContext:Landroid/content/Context;

    invoke-virtual {v0, v1}, LprotocolAnalysis/analysis/packetHandle;->closeInputMethod(Landroid/content/Context;)V

    iget-object v0, p0, Lcom/konka/kkmultiscreen/DataService$MyBinder;->this$0:Lcom/konka/kkmultiscreen/DataService;

    return-object v0
.end method

.method public handleTransactions(ILandroid/os/Parcel;Landroid/os/Parcel;I)Z
    .locals 11
    .param p1    # I
    .param p2    # Landroid/os/Parcel;
    .param p3    # Landroid/os/Parcel;
    .param p4    # I

    const/4 v10, 0x0

    const-string v7, "DataService"

    new-instance v8, Ljava/lang/StringBuilder;

    const-string v9, "para code is: "

    invoke-direct {v8, v9}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v8, p1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v8

    invoke-virtual {v8}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v8

    invoke-static {v7, v8}, Lutil/log/CLog;->wLog(Ljava/lang/String;Ljava/lang/String;)V

    invoke-virtual {p2}, Landroid/os/Parcel;->dataSize()I

    move-result v7

    invoke-static {v7}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v5

    invoke-virtual {p2}, Landroid/os/Parcel;->dataPosition()I

    move-result v7

    invoke-static {v7}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v3

    const-string v7, "DataService"

    new-instance v8, Ljava/lang/StringBuilder;

    const-string v9, "handleTransactions data size: pos:"

    invoke-direct {v8, v9}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v5}, Ljava/lang/Integer;->toString()Ljava/lang/String;

    move-result-object v9

    invoke-virtual {v8, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v8

    const-string v9, " "

    invoke-virtual {v8, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v8

    invoke-virtual {v8, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v8

    invoke-virtual {v8}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v8

    invoke-static {v7, v8}, Lutil/log/CLog;->wLog(Ljava/lang/String;Ljava/lang/String;)V

    # getter for: Lcom/konka/kkmultiscreen/DataService;->OUTERMSG:Ljava/lang/Integer;
    invoke-static {}, Lcom/konka/kkmultiscreen/DataService;->access$1()Ljava/lang/Integer;

    move-result-object v7

    invoke-virtual {v7}, Ljava/lang/Integer;->intValue()I

    move-result v7

    if-ne p1, v7, :cond_1

    const-string v7, "DataService"

    const-string v8, "MSG TYPE : OUTERMSG"

    invoke-static {v7, v8}, Lutil/log/CLog;->wLog(Ljava/lang/String;Ljava/lang/String;)V

    :cond_0
    :goto_0
    const/4 v7, 0x1

    return v7

    :cond_1
    # getter for: Lcom/konka/kkmultiscreen/DataService;->INNERMSG:Ljava/lang/Integer;
    invoke-static {}, Lcom/konka/kkmultiscreen/DataService;->access$2()Ljava/lang/Integer;

    move-result-object v7

    invoke-virtual {v7}, Ljava/lang/Integer;->intValue()I

    move-result v7

    if-ne p1, v7, :cond_0

    const-string v7, "DataService"

    const-string v8, "MSG TYPE : INNERMSG"

    invoke-static {v7, v8}, Lutil/log/CLog;->wLog(Ljava/lang/String;Ljava/lang/String;)V

    const/16 v2, 0x8

    const/4 v0, 0x3

    invoke-static {v10}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v4

    move v1, v0

    :goto_1
    add-int/lit8 v0, v1, -0x1

    if-gtz v1, :cond_2

    :goto_2
    const-string v7, "DataService"

    new-instance v8, Ljava/lang/StringBuilder;

    const-string v9, "MSG TYPE: INNERMSG  VAL:"

    invoke-direct {v8, v9}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v8, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v8

    invoke-virtual {v8}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v8

    invoke-static {v7, v8}, Lutil/log/CLog;->wLog(Ljava/lang/String;Ljava/lang/String;)V

    invoke-virtual {v3}, Ljava/lang/Integer;->intValue()I

    move-result v7

    add-int/2addr v7, v2

    add-int/lit8 v7, v7, 0x4

    invoke-virtual {p2, v7}, Landroid/os/Parcel;->setDataPosition(I)V

    const-string v6, ""

    invoke-virtual {v4}, Ljava/lang/Integer;->intValue()I

    move-result v7

    sparse-switch v7, :sswitch_data_0

    const-string v7, "DataService"

    const-string v8, "RECV ERROR MESSAGE!"

    invoke-static {v7, v8}, Lutil/log/CLog;->wLog(Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_0

    :cond_2
    invoke-virtual {v3}, Ljava/lang/Integer;->intValue()I

    move-result v7

    add-int/2addr v7, v2

    invoke-virtual {p2, v7}, Landroid/os/Parcel;->setDataPosition(I)V

    invoke-virtual {p2}, Landroid/os/Parcel;->readInt()I

    move-result v7

    invoke-static {v7}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/Integer;->intValue()I

    move-result v7

    const/16 v8, 0x2774

    if-gt v7, v8, :cond_3

    invoke-virtual {v4}, Ljava/lang/Integer;->intValue()I

    move-result v7

    const/16 v8, 0x2710

    if-ge v7, v8, :cond_4

    :cond_3
    add-int/lit8 v2, v2, 0x4

    move v1, v0

    goto :goto_1

    :cond_4
    invoke-static {v2}, Lcom/konka/kkmultiscreen/DataService;->access$3(I)V

    goto :goto_2

    :sswitch_0
    const-string v7, "DataService"

    const-string v8, "EXIT_SERVICE..........!"

    invoke-static {v7, v8}, Lutil/log/CLog;->wLog(Ljava/lang/String;Ljava/lang/String;)V

    invoke-static {v10}, Ljava/lang/System;->exit(I)V

    goto :goto_0

    :sswitch_1
    invoke-virtual {p2}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    move-result-object v6

    invoke-virtual {v6}, Ljava/lang/String;->trim()Ljava/lang/String;

    move-result-object v6

    const-string v7, "DataService"

    new-instance v8, Ljava/lang/StringBuilder;

    const-string v9, "TOAST_DATA data: "

    invoke-direct {v8, v9}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v8, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v8

    invoke-virtual {v8}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v8

    invoke-static {v7, v8}, Lutil/log/CLog;->wLog(Ljava/lang/String;Ljava/lang/String;)V

    goto/16 :goto_0

    nop

    :sswitch_data_0
    .sparse-switch
        0x2714 -> :sswitch_0
        0x2718 -> :sswitch_1
    .end sparse-switch
.end method

.method public onTransact(ILandroid/os/Parcel;Landroid/os/Parcel;I)Z
    .locals 2
    .param p1    # I
    .param p2    # Landroid/os/Parcel;
    .param p3    # Landroid/os/Parcel;
    .param p4    # I

    const-string v0, "DataService"

    const-string v1, "onTransact"

    invoke-static {v0, v1}, Lutil/log/CLog;->wLog(Ljava/lang/String;Ljava/lang/String;)V

    invoke-virtual {p0, p1, p2, p3, p4}, Lcom/konka/kkmultiscreen/DataService$MyBinder;->handleTransactions(ILandroid/os/Parcel;Landroid/os/Parcel;I)Z

    move-result v0

    return v0
.end method
