.class Lcom/konka/kkmultiscreen/MultiScreenWizardActity$1;
.super Ljava/lang/Object;
.source "MultiScreenWizardActity.java"

# interfaces
.implements Landroid/view/View$OnClickListener;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/konka/kkmultiscreen/MultiScreenWizardActity;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/konka/kkmultiscreen/MultiScreenWizardActity;


# direct methods
.method constructor <init>(Lcom/konka/kkmultiscreen/MultiScreenWizardActity;)V
    .locals 0

    iput-object p1, p0, Lcom/konka/kkmultiscreen/MultiScreenWizardActity$1;->this$0:Lcom/konka/kkmultiscreen/MultiScreenWizardActity;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onClick(Landroid/view/View;)V
    .locals 4
    .param p1    # Landroid/view/View;

    const/4 v3, 0x1

    invoke-virtual {p1}, Landroid/view/View;->getId()I

    move-result v1

    packed-switch v1, :pswitch_data_0

    :goto_0
    return-void

    :pswitch_0
    sput-boolean v3, Lcom/konka/kkmultiscreen/KKMutiScreenTvActivity;->mFinish:Z

    iget-object v1, p0, Lcom/konka/kkmultiscreen/MultiScreenWizardActity$1;->this$0:Lcom/konka/kkmultiscreen/MultiScreenWizardActity;

    invoke-virtual {v1}, Lcom/konka/kkmultiscreen/MultiScreenWizardActity;->finish()V

    goto :goto_0

    :pswitch_1
    iget-object v1, p0, Lcom/konka/kkmultiscreen/MultiScreenWizardActity$1;->this$0:Lcom/konka/kkmultiscreen/MultiScreenWizardActity;

    iget v1, v1, Lcom/konka/kkmultiscreen/MultiScreenWizardActity;->wizardState:I

    const/4 v2, 0x2

    if-ge v1, v2, :cond_0

    iget-object v1, p0, Lcom/konka/kkmultiscreen/MultiScreenWizardActity$1;->this$0:Lcom/konka/kkmultiscreen/MultiScreenWizardActity;

    iget v2, v1, Lcom/konka/kkmultiscreen/MultiScreenWizardActity;->wizardState:I

    add-int/lit8 v2, v2, 0x1

    iput v2, v1, Lcom/konka/kkmultiscreen/MultiScreenWizardActity;->wizardState:I

    :cond_0
    iget-object v1, p0, Lcom/konka/kkmultiscreen/MultiScreenWizardActity$1;->this$0:Lcom/konka/kkmultiscreen/MultiScreenWizardActity;

    iget-object v2, p0, Lcom/konka/kkmultiscreen/MultiScreenWizardActity$1;->this$0:Lcom/konka/kkmultiscreen/MultiScreenWizardActity;

    iget v2, v2, Lcom/konka/kkmultiscreen/MultiScreenWizardActity;->wizardState:I

    invoke-virtual {v1, v2}, Lcom/konka/kkmultiscreen/MultiScreenWizardActity;->setWizardState(I)V

    iget-object v1, p0, Lcom/konka/kkmultiscreen/MultiScreenWizardActity$1;->this$0:Lcom/konka/kkmultiscreen/MultiScreenWizardActity;

    # getter for: Lcom/konka/kkmultiscreen/MultiScreenWizardActity;->wizard_next:Landroid/widget/Button;
    invoke-static {v1}, Lcom/konka/kkmultiscreen/MultiScreenWizardActity;->access$0(Lcom/konka/kkmultiscreen/MultiScreenWizardActity;)Landroid/widget/Button;

    move-result-object v1

    invoke-virtual {v1, v3}, Landroid/widget/Button;->setFocusable(Z)V

    iget-object v1, p0, Lcom/konka/kkmultiscreen/MultiScreenWizardActity$1;->this$0:Lcom/konka/kkmultiscreen/MultiScreenWizardActity;

    # getter for: Lcom/konka/kkmultiscreen/MultiScreenWizardActity;->wizard_next:Landroid/widget/Button;
    invoke-static {v1}, Lcom/konka/kkmultiscreen/MultiScreenWizardActity;->access$0(Lcom/konka/kkmultiscreen/MultiScreenWizardActity;)Landroid/widget/Button;

    move-result-object v1

    invoke-virtual {v1}, Landroid/widget/Button;->requestFocus()Z

    iget-object v1, p0, Lcom/konka/kkmultiscreen/MultiScreenWizardActity$1;->this$0:Lcom/konka/kkmultiscreen/MultiScreenWizardActity;

    invoke-virtual {v1}, Lcom/konka/kkmultiscreen/MultiScreenWizardActity;->setButtonDrawableSource()V

    iget-object v1, p0, Lcom/konka/kkmultiscreen/MultiScreenWizardActity$1;->this$0:Lcom/konka/kkmultiscreen/MultiScreenWizardActity;

    # getter for: Lcom/konka/kkmultiscreen/MultiScreenWizardActity;->wizard_end:Landroid/widget/Button;
    invoke-static {v1}, Lcom/konka/kkmultiscreen/MultiScreenWizardActity;->access$1(Lcom/konka/kkmultiscreen/MultiScreenWizardActity;)Landroid/widget/Button;

    move-result-object v1

    const/4 v2, 0x0

    invoke-virtual {v1, v2}, Landroid/widget/Button;->setVisibility(I)V

    goto :goto_0

    :pswitch_2
    iget-object v1, p0, Lcom/konka/kkmultiscreen/MultiScreenWizardActity$1;->this$0:Lcom/konka/kkmultiscreen/MultiScreenWizardActity;

    iget v1, v1, Lcom/konka/kkmultiscreen/MultiScreenWizardActity;->wizardState:I

    if-ne v1, v3, :cond_1

    new-instance v0, Landroid/content/Intent;

    iget-object v1, p0, Lcom/konka/kkmultiscreen/MultiScreenWizardActity$1;->this$0:Lcom/konka/kkmultiscreen/MultiScreenWizardActity;

    const-class v2, Lcom/konka/kkmultiscreen/KKMutiScreenTvActivity;

    invoke-direct {v0, v1, v2}, Landroid/content/Intent;-><init>(Landroid/content/Context;Ljava/lang/Class;)V

    iget-object v1, p0, Lcom/konka/kkmultiscreen/MultiScreenWizardActity$1;->this$0:Lcom/konka/kkmultiscreen/MultiScreenWizardActity;

    invoke-virtual {v1, v0}, Lcom/konka/kkmultiscreen/MultiScreenWizardActity;->startActivity(Landroid/content/Intent;)V

    goto :goto_0

    :cond_1
    iget-object v1, p0, Lcom/konka/kkmultiscreen/MultiScreenWizardActity$1;->this$0:Lcom/konka/kkmultiscreen/MultiScreenWizardActity;

    iget v1, v1, Lcom/konka/kkmultiscreen/MultiScreenWizardActity;->wizardState:I

    if-le v1, v3, :cond_2

    iget-object v1, p0, Lcom/konka/kkmultiscreen/MultiScreenWizardActity$1;->this$0:Lcom/konka/kkmultiscreen/MultiScreenWizardActity;

    iget v2, v1, Lcom/konka/kkmultiscreen/MultiScreenWizardActity;->wizardState:I

    add-int/lit8 v2, v2, -0x1

    iput v2, v1, Lcom/konka/kkmultiscreen/MultiScreenWizardActity;->wizardState:I

    :cond_2
    iget-object v1, p0, Lcom/konka/kkmultiscreen/MultiScreenWizardActity$1;->this$0:Lcom/konka/kkmultiscreen/MultiScreenWizardActity;

    iget-object v2, p0, Lcom/konka/kkmultiscreen/MultiScreenWizardActity$1;->this$0:Lcom/konka/kkmultiscreen/MultiScreenWizardActity;

    iget v2, v2, Lcom/konka/kkmultiscreen/MultiScreenWizardActity;->wizardState:I

    invoke-virtual {v1, v2}, Lcom/konka/kkmultiscreen/MultiScreenWizardActity;->setWizardState(I)V

    iget-object v1, p0, Lcom/konka/kkmultiscreen/MultiScreenWizardActity$1;->this$0:Lcom/konka/kkmultiscreen/MultiScreenWizardActity;

    # getter for: Lcom/konka/kkmultiscreen/MultiScreenWizardActity;->wizard_pre:Landroid/widget/Button;
    invoke-static {v1}, Lcom/konka/kkmultiscreen/MultiScreenWizardActity;->access$2(Lcom/konka/kkmultiscreen/MultiScreenWizardActity;)Landroid/widget/Button;

    move-result-object v1

    invoke-virtual {v1, v3}, Landroid/widget/Button;->setFocusable(Z)V

    iget-object v1, p0, Lcom/konka/kkmultiscreen/MultiScreenWizardActity$1;->this$0:Lcom/konka/kkmultiscreen/MultiScreenWizardActity;

    # getter for: Lcom/konka/kkmultiscreen/MultiScreenWizardActity;->wizard_pre:Landroid/widget/Button;
    invoke-static {v1}, Lcom/konka/kkmultiscreen/MultiScreenWizardActity;->access$2(Lcom/konka/kkmultiscreen/MultiScreenWizardActity;)Landroid/widget/Button;

    move-result-object v1

    invoke-virtual {v1}, Landroid/widget/Button;->requestFocus()Z

    iget-object v1, p0, Lcom/konka/kkmultiscreen/MultiScreenWizardActity$1;->this$0:Lcom/konka/kkmultiscreen/MultiScreenWizardActity;

    # getter for: Lcom/konka/kkmultiscreen/MultiScreenWizardActity;->wizard_end:Landroid/widget/Button;
    invoke-static {v1}, Lcom/konka/kkmultiscreen/MultiScreenWizardActity;->access$1(Lcom/konka/kkmultiscreen/MultiScreenWizardActity;)Landroid/widget/Button;

    move-result-object v1

    const/16 v2, 0x8

    invoke-virtual {v1, v2}, Landroid/widget/Button;->setVisibility(I)V

    iget-object v1, p0, Lcom/konka/kkmultiscreen/MultiScreenWizardActity$1;->this$0:Lcom/konka/kkmultiscreen/MultiScreenWizardActity;

    invoke-virtual {v1}, Lcom/konka/kkmultiscreen/MultiScreenWizardActity;->setButtonDrawableSource()V

    goto/16 :goto_0

    nop

    :pswitch_data_0
    .packed-switch 0x7f050004
        :pswitch_1    # com.konka.kkmultiscreen.R.id.wizard_next
        :pswitch_2    # com.konka.kkmultiscreen.R.id.wizard_prev
        :pswitch_0    # com.konka.kkmultiscreen.R.id.wizard_end
    .end packed-switch
.end method
