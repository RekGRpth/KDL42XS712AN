.class public Lcom/konka/kkmultiscreen/KKMutiScreenTvActivity;
.super Landroid/app/Activity;
.source "KKMutiScreenTvActivity.java"


# static fields
.field public static final SHOW_TOUCHES:Ljava/lang/String; = "show_touches"

.field public static mFinish:Z

.field public static mToastType:Z


# instance fields
.field public final TAG:Ljava/lang/String;

.field private buttonreview:Landroid/widget/Button;

.field private clickListener:Landroid/view/View$OnClickListener;

.field private textIp:Landroid/widget/TextView;

.field private textVersion:Landroid/widget/TextView;

.field private wizardStep:I

.field private wizard_bg:Landroid/widget/ImageButton;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    const/4 v0, 0x0

    sput-boolean v0, Lcom/konka/kkmultiscreen/KKMutiScreenTvActivity;->mToastType:Z

    sput-boolean v0, Lcom/konka/kkmultiscreen/KKMutiScreenTvActivity;->mFinish:Z

    return-void
.end method

.method public constructor <init>()V
    .locals 1

    invoke-direct {p0}, Landroid/app/Activity;-><init>()V

    const-string v0, "KKMutiScreenTvActivity"

    iput-object v0, p0, Lcom/konka/kkmultiscreen/KKMutiScreenTvActivity;->TAG:Ljava/lang/String;

    const/4 v0, 0x0

    iput v0, p0, Lcom/konka/kkmultiscreen/KKMutiScreenTvActivity;->wizardStep:I

    new-instance v0, Lcom/konka/kkmultiscreen/KKMutiScreenTvActivity$1;

    invoke-direct {v0, p0}, Lcom/konka/kkmultiscreen/KKMutiScreenTvActivity$1;-><init>(Lcom/konka/kkmultiscreen/KKMutiScreenTvActivity;)V

    iput-object v0, p0, Lcom/konka/kkmultiscreen/KKMutiScreenTvActivity;->clickListener:Landroid/view/View$OnClickListener;

    return-void
.end method

.method public static getLocalIpAddress()Ljava/lang/String;
    .locals 8

    const-string v5, ""

    :try_start_0
    invoke-static {}, Ljava/net/NetworkInterface;->getNetworkInterfaces()Ljava/util/Enumeration;

    move-result-object v0

    :cond_0
    invoke-interface {v0}, Ljava/util/Enumeration;->hasMoreElements()Z

    move-result v6

    if-nez v6, :cond_1

    :goto_0
    return-object v5

    :cond_1
    invoke-interface {v0}, Ljava/util/Enumeration;->nextElement()Ljava/lang/Object;

    move-result-object v4

    check-cast v4, Ljava/net/NetworkInterface;

    invoke-virtual {v4}, Ljava/net/NetworkInterface;->getInetAddresses()Ljava/util/Enumeration;

    move-result-object v1

    :cond_2
    :goto_1
    invoke-interface {v1}, Ljava/util/Enumeration;->hasMoreElements()Z

    move-result v6

    if-eqz v6, :cond_0

    invoke-interface {v1}, Ljava/util/Enumeration;->nextElement()Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Ljava/net/InetAddress;

    invoke-virtual {v3}, Ljava/net/InetAddress;->isLoopbackAddress()Z

    move-result v6

    if-nez v6, :cond_2

    invoke-virtual {v3}, Ljava/net/InetAddress;->getHostAddress()Ljava/lang/String;

    move-result-object v6

    invoke-virtual {v6}, Ljava/lang/String;->toString()Ljava/lang/String;
    :try_end_0
    .catch Ljava/net/SocketException; {:try_start_0 .. :try_end_0} :catch_0

    move-result-object v5

    goto :goto_1

    :catch_0
    move-exception v2

    const-string v6, "WifiPreference IpAddress"

    invoke-virtual {v2}, Ljava/net/SocketException;->toString()Ljava/lang/String;

    move-result-object v7

    invoke-static {v6, v7}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_0
.end method


# virtual methods
.method public getLocalSerial()Ljava/lang/String;
    .locals 2

    sget-object v1, Lcom/konka/kkmultiscreen/KKMutiScreenTvApp;->mApp:Lcom/konka/kkmultiscreen/KKMutiScreenTvApp;

    iget-object v1, v1, Lcom/konka/kkmultiscreen/KKMutiScreenTvApp;->mAppContext:Landroid/content/Context;

    invoke-static {v1}, Lcom/konka/kkimplements/tv/TvDeskProviderImpl;->getInstance(Landroid/content/Context;)Lcom/konka/kkinterface/tv/TvDeskProvider;

    move-result-object v1

    invoke-interface {v1}, Lcom/konka/kkinterface/tv/TvDeskProvider;->getCommonManagerInstance()Lcom/konka/kkinterface/tv/CommonDesk;

    move-result-object v1

    invoke-interface {v1}, Lcom/konka/kkinterface/tv/CommonDesk;->getSystemBoardInfo()Lcom/konka/kkinterface/tv/CommonDesk$SystemBoardInfo;

    move-result-object v0

    iget-object v1, v0, Lcom/konka/kkinterface/tv/CommonDesk$SystemBoardInfo;->strSerial:Ljava/lang/String;

    return-object v1
.end method

.method public getLocalVersion()Ljava/lang/String;
    .locals 2

    sget-object v1, Lcom/konka/kkmultiscreen/KKMutiScreenTvApp;->mApp:Lcom/konka/kkmultiscreen/KKMutiScreenTvApp;

    iget-object v1, v1, Lcom/konka/kkmultiscreen/KKMutiScreenTvApp;->mAppContext:Landroid/content/Context;

    invoke-static {v1}, Lcom/konka/kkimplements/tv/TvDeskProviderImpl;->getInstance(Landroid/content/Context;)Lcom/konka/kkinterface/tv/TvDeskProvider;

    move-result-object v1

    invoke-interface {v1}, Lcom/konka/kkinterface/tv/TvDeskProvider;->getCommonManagerInstance()Lcom/konka/kkinterface/tv/CommonDesk;

    move-result-object v1

    invoke-interface {v1}, Lcom/konka/kkinterface/tv/CommonDesk;->getSystemBoardInfo()Lcom/konka/kkinterface/tv/CommonDesk$SystemBoardInfo;

    move-result-object v0

    iget-object v1, v0, Lcom/konka/kkinterface/tv/CommonDesk$SystemBoardInfo;->strVersion:Ljava/lang/String;

    return-object v1
.end method

.method public onCreate(Landroid/os/Bundle;)V
    .locals 6
    .param p1    # Landroid/os/Bundle;

    const/4 v5, 0x1

    const/high16 v4, 0x41a00000    # 20.0f

    invoke-super {p0, p1}, Landroid/app/Activity;->onCreate(Landroid/os/Bundle;)V

    const/high16 v0, 0x7f030000    # com.konka.kkmultiscreen.R.layout.main

    invoke-virtual {p0, v0}, Lcom/konka/kkmultiscreen/KKMutiScreenTvActivity;->setContentView(I)V

    const-string v0, "KKMutiScreenTvActivity"

    const-string v1, "onCreate--- "

    invoke-static {v0, v1}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    invoke-virtual {p0}, Lcom/konka/kkmultiscreen/KKMutiScreenTvActivity;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v0

    const-string v1, "show_touches"

    invoke-static {v0, v1, v5}, Landroid/provider/Settings$System;->putInt(Landroid/content/ContentResolver;Ljava/lang/String;I)Z

    const/high16 v0, 0x7f050000    # com.konka.kkmultiscreen.R.id.wizard_bg

    invoke-virtual {p0, v0}, Lcom/konka/kkmultiscreen/KKMutiScreenTvActivity;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/ImageButton;

    iput-object v0, p0, Lcom/konka/kkmultiscreen/KKMutiScreenTvActivity;->wizard_bg:Landroid/widget/ImageButton;

    const v0, 0x7f050002    # com.konka.kkmultiscreen.R.id.version

    invoke-virtual {p0, v0}, Lcom/konka/kkmultiscreen/KKMutiScreenTvActivity;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    iput-object v0, p0, Lcom/konka/kkmultiscreen/KKMutiScreenTvActivity;->textVersion:Landroid/widget/TextView;

    iget-object v0, p0, Lcom/konka/kkmultiscreen/KKMutiScreenTvActivity;->textVersion:Landroid/widget/TextView;

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-virtual {p0}, Lcom/konka/kkmultiscreen/KKMutiScreenTvActivity;->getResources()Landroid/content/res/Resources;

    move-result-object v2

    const v3, 0x7f040004    # com.konka.kkmultiscreen.R.string.version

    invoke-virtual {v2, v3}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v2

    invoke-static {v2}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v2

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    const-string v2, " V3.5.1 "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    iget-object v0, p0, Lcom/konka/kkmultiscreen/KKMutiScreenTvActivity;->textVersion:Landroid/widget/TextView;

    invoke-virtual {v0, v4}, Landroid/widget/TextView;->setTextSize(F)V

    const v0, 0x7f050001    # com.konka.kkmultiscreen.R.id.ip

    invoke-virtual {p0, v0}, Lcom/konka/kkmultiscreen/KKMutiScreenTvActivity;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    iput-object v0, p0, Lcom/konka/kkmultiscreen/KKMutiScreenTvActivity;->textIp:Landroid/widget/TextView;

    iget-object v0, p0, Lcom/konka/kkmultiscreen/KKMutiScreenTvActivity;->textIp:Landroid/widget/TextView;

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-virtual {p0}, Lcom/konka/kkmultiscreen/KKMutiScreenTvActivity;->getResources()Landroid/content/res/Resources;

    move-result-object v2

    const v3, 0x7f040003    # com.konka.kkmultiscreen.R.string.localip

    invoke-virtual {v2, v3}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v2

    invoke-static {v2}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v2

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-static {}, Lcom/konka/kkmultiscreen/KKMutiScreenTvActivity;->getLocalIpAddress()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    iget-object v0, p0, Lcom/konka/kkmultiscreen/KKMutiScreenTvActivity;->textIp:Landroid/widget/TextView;

    invoke-virtual {v0, v4}, Landroid/widget/TextView;->setTextSize(F)V

    const v0, 0x7f050003    # com.konka.kkmultiscreen.R.id.review

    invoke-virtual {p0, v0}, Lcom/konka/kkmultiscreen/KKMutiScreenTvActivity;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/Button;

    iput-object v0, p0, Lcom/konka/kkmultiscreen/KKMutiScreenTvActivity;->buttonreview:Landroid/widget/Button;

    iget-object v0, p0, Lcom/konka/kkmultiscreen/KKMutiScreenTvActivity;->buttonreview:Landroid/widget/Button;

    iget-object v1, p0, Lcom/konka/kkmultiscreen/KKMutiScreenTvActivity;->clickListener:Landroid/view/View$OnClickListener;

    invoke-virtual {v0, v1}, Landroid/widget/Button;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    iget-object v0, p0, Lcom/konka/kkmultiscreen/KKMutiScreenTvActivity;->buttonreview:Landroid/widget/Button;

    const v1, 0x7f02000e    # com.konka.kkmultiscreen.R.drawable.wizard_run_sel_btn

    invoke-virtual {v0, v1}, Landroid/widget/Button;->setBackgroundResource(I)V

    iget-object v0, p0, Lcom/konka/kkmultiscreen/KKMutiScreenTvActivity;->buttonreview:Landroid/widget/Button;

    invoke-virtual {v0, v5}, Landroid/widget/Button;->setFocusable(Z)V

    iget-object v0, p0, Lcom/konka/kkmultiscreen/KKMutiScreenTvActivity;->buttonreview:Landroid/widget/Button;

    invoke-virtual {v0}, Landroid/widget/Button;->requestFocus()Z

    return-void
.end method

.method public onKeyDown(ILandroid/view/KeyEvent;)Z
    .locals 5
    .param p1    # I
    .param p2    # Landroid/view/KeyEvent;

    const v4, 0x7f02000e    # com.konka.kkmultiscreen.R.drawable.wizard_run_sel_btn

    const/4 v3, 0x1

    const-string v0, "onkey down"

    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "onKeyDown: "

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    invoke-virtual {p2}, Landroid/view/KeyEvent;->getKeyCode()I

    move-result v0

    sparse-switch v0, :sswitch_data_0

    :goto_0
    invoke-super {p0, p1, p2}, Landroid/app/Activity;->onKeyDown(ILandroid/view/KeyEvent;)Z

    move-result v0

    return v0

    :sswitch_0
    invoke-virtual {p0}, Lcom/konka/kkmultiscreen/KKMutiScreenTvActivity;->finish()V

    sput-boolean v3, Lcom/konka/kkmultiscreen/KKMutiScreenTvActivity;->mFinish:Z

    goto :goto_0

    :sswitch_1
    const-string v0, "onkeydown"

    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "onkeydown2:"

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {p2}, Landroid/view/KeyEvent;->getKeyCode()I

    move-result v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_0

    :sswitch_2
    iget-object v0, p0, Lcom/konka/kkmultiscreen/KKMutiScreenTvActivity;->buttonreview:Landroid/widget/Button;

    invoke-virtual {v0, v3}, Landroid/widget/Button;->setFocusable(Z)V

    iget-object v0, p0, Lcom/konka/kkmultiscreen/KKMutiScreenTvActivity;->buttonreview:Landroid/widget/Button;

    invoke-virtual {v0}, Landroid/widget/Button;->requestFocus()Z

    iget-object v0, p0, Lcom/konka/kkmultiscreen/KKMutiScreenTvActivity;->buttonreview:Landroid/widget/Button;

    invoke-virtual {v0, v4}, Landroid/widget/Button;->setBackgroundResource(I)V

    goto :goto_0

    :sswitch_3
    iget-object v0, p0, Lcom/konka/kkmultiscreen/KKMutiScreenTvActivity;->buttonreview:Landroid/widget/Button;

    invoke-virtual {v0, v3}, Landroid/widget/Button;->setFocusable(Z)V

    iget-object v0, p0, Lcom/konka/kkmultiscreen/KKMutiScreenTvActivity;->buttonreview:Landroid/widget/Button;

    invoke-virtual {v0}, Landroid/widget/Button;->requestFocus()Z

    iget-object v0, p0, Lcom/konka/kkmultiscreen/KKMutiScreenTvActivity;->buttonreview:Landroid/widget/Button;

    invoke-virtual {v0, v4}, Landroid/widget/Button;->setBackgroundResource(I)V

    goto :goto_0

    nop

    :sswitch_data_0
    .sparse-switch
        0x4 -> :sswitch_0
        0x13 -> :sswitch_3
        0x14 -> :sswitch_2
        0x17 -> :sswitch_1
    .end sparse-switch
.end method

.method protected onResume()V
    .locals 2

    invoke-super {p0}, Landroid/app/Activity;->onResume()V

    sget-boolean v0, Lcom/konka/kkmultiscreen/KKMutiScreenTvActivity;->mToastType:Z

    if-eqz v0, :cond_0

    sget-boolean v0, Lcom/konka/kkmultiscreen/KKMutiScreenTvActivity;->mFinish:Z

    if-eqz v0, :cond_0

    invoke-virtual {p0}, Lcom/konka/kkmultiscreen/KKMutiScreenTvActivity;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    const v1, 0x7f040012    # com.konka.kkmultiscreen.R.string.open_MultiScreen_Service_opened

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v0

    const/4 v1, 0x0

    invoke-static {p0, v0, v1}, Landroid/widget/Toast;->makeText(Landroid/content/Context;Ljava/lang/CharSequence;I)Landroid/widget/Toast;

    move-result-object v0

    invoke-virtual {v0}, Landroid/widget/Toast;->show()V

    :cond_0
    return-void
.end method
