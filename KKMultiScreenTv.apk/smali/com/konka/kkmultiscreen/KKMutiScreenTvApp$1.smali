.class Lcom/konka/kkmultiscreen/KKMutiScreenTvApp$1;
.super Landroid/os/Handler;
.source "KKMutiScreenTvApp.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/konka/kkmultiscreen/KKMutiScreenTvApp;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field lastTime:J

.field nowTime:J

.field final synthetic this$0:Lcom/konka/kkmultiscreen/KKMutiScreenTvApp;


# direct methods
.method constructor <init>(Lcom/konka/kkmultiscreen/KKMutiScreenTvApp;)V
    .locals 2

    const-wide/16 v0, -0x1

    iput-object p1, p0, Lcom/konka/kkmultiscreen/KKMutiScreenTvApp$1;->this$0:Lcom/konka/kkmultiscreen/KKMutiScreenTvApp;

    invoke-direct {p0}, Landroid/os/Handler;-><init>()V

    iput-wide v0, p0, Lcom/konka/kkmultiscreen/KKMutiScreenTvApp$1;->nowTime:J

    iput-wide v0, p0, Lcom/konka/kkmultiscreen/KKMutiScreenTvApp$1;->lastTime:J

    return-void
.end method


# virtual methods
.method public handleMessage(Landroid/os/Message;)V
    .locals 6
    .param p1    # Landroid/os/Message;

    const/4 v5, 0x0

    iget-wide v1, p0, Lcom/konka/kkmultiscreen/KKMutiScreenTvApp$1;->nowTime:J

    const-wide/16 v3, -0x1

    cmp-long v1, v1, v3

    if-nez v1, :cond_2

    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v1

    iput-wide v1, p0, Lcom/konka/kkmultiscreen/KKMutiScreenTvApp$1;->nowTime:J

    iget-wide v1, p0, Lcom/konka/kkmultiscreen/KKMutiScreenTvApp$1;->nowTime:J

    iput-wide v1, p0, Lcom/konka/kkmultiscreen/KKMutiScreenTvApp$1;->lastTime:J

    :goto_0
    iget v1, p1, Landroid/os/Message;->what:I

    const/16 v2, 0x2718

    if-ne v1, v2, :cond_1

    invoke-virtual {p1}, Landroid/os/Message;->getData()Landroid/os/Bundle;

    move-result-object v1

    const-string v2, "MSGINFO:"

    invoke-virtual {v1, v2}, Landroid/os/Bundle;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    if-eqz v0, :cond_0

    invoke-virtual {v0}, Ljava/lang/String;->length()I

    move-result v1

    const/4 v2, 0x2

    if-le v1, v2, :cond_0

    iget-object v1, p0, Lcom/konka/kkmultiscreen/KKMutiScreenTvApp$1;->this$0:Lcom/konka/kkmultiscreen/KKMutiScreenTvApp;

    iget-object v1, v1, Lcom/konka/kkmultiscreen/KKMutiScreenTvApp;->mAppContext:Landroid/content/Context;

    invoke-static {v1, v0, v5}, Landroid/widget/Toast;->makeText(Landroid/content/Context;Ljava/lang/CharSequence;I)Landroid/widget/Toast;

    move-result-object v1

    invoke-virtual {v1}, Landroid/widget/Toast;->show()V

    :cond_0
    iget-object v1, p0, Lcom/konka/kkmultiscreen/KKMutiScreenTvApp$1;->this$0:Lcom/konka/kkmultiscreen/KKMutiScreenTvApp;

    iget-boolean v1, v1, Lcom/konka/kkmultiscreen/KKMutiScreenTvApp;->showTag:Z

    if-eqz v1, :cond_1

    iget-object v1, p0, Lcom/konka/kkmultiscreen/KKMutiScreenTvApp$1;->this$0:Lcom/konka/kkmultiscreen/KKMutiScreenTvApp;

    iput-boolean v5, v1, Lcom/konka/kkmultiscreen/KKMutiScreenTvApp;->showTag:Z

    :cond_1
    :goto_1
    return-void

    :cond_2
    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v1

    iput-wide v1, p0, Lcom/konka/kkmultiscreen/KKMutiScreenTvApp$1;->nowTime:J

    iget-wide v1, p0, Lcom/konka/kkmultiscreen/KKMutiScreenTvApp$1;->nowTime:J

    iget-wide v3, p0, Lcom/konka/kkmultiscreen/KKMutiScreenTvApp$1;->lastTime:J

    sub-long/2addr v1, v3

    const-wide/16 v3, 0xbb8

    cmp-long v1, v1, v3

    if-gez v1, :cond_3

    iget-object v1, p0, Lcom/konka/kkmultiscreen/KKMutiScreenTvApp$1;->this$0:Lcom/konka/kkmultiscreen/KKMutiScreenTvApp;

    iget-boolean v1, v1, Lcom/konka/kkmultiscreen/KKMutiScreenTvApp;->showTag:Z

    if-nez v1, :cond_3

    iget-wide v1, p0, Lcom/konka/kkmultiscreen/KKMutiScreenTvApp$1;->nowTime:J

    iput-wide v1, p0, Lcom/konka/kkmultiscreen/KKMutiScreenTvApp$1;->lastTime:J

    goto :goto_1

    :cond_3
    iget-wide v1, p0, Lcom/konka/kkmultiscreen/KKMutiScreenTvApp$1;->nowTime:J

    iput-wide v1, p0, Lcom/konka/kkmultiscreen/KKMutiScreenTvApp$1;->lastTime:J

    goto :goto_0
.end method
