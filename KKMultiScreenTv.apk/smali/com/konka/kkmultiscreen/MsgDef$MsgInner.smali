.class public Lcom/konka/kkmultiscreen/MsgDef$MsgInner;
.super Ljava/lang/Object;
.source "MsgDef.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/konka/kkmultiscreen/MsgDef;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x9
    name = "MsgInner"
.end annotation


# static fields
.field public static final BROADCAST_DATA:I = 0x2717

.field public static final BUILD_CONN:I = 0x2711

.field public static final CLOSE_CONN:I = 0x2712

.field public static final EXIT_SERVICE:I = 0x2714

.field public static final MSGDEFVAL:I = 0x2710

.field public static final MSGTYPE:I = 0x4e20

.field public static final REBUILD_CONN:I = 0x2713

.field public static final TOAST_DATA:I = 0x2718

.field public static final TOGET_TVIP:I = 0x2715

.field public static final TOINIT_SERACH:I = 0x2716

.field public static final TOPTAG:I = 0x2774


# instance fields
.field private data:Ljava/lang/String;

.field private order:I


# direct methods
.method public constructor <init>()V
    .locals 1

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    const/4 v0, 0x0

    iput v0, p0, Lcom/konka/kkmultiscreen/MsgDef$MsgInner;->order:I

    const-string v0, ""

    iput-object v0, p0, Lcom/konka/kkmultiscreen/MsgDef$MsgInner;->data:Ljava/lang/String;

    return-void
.end method


# virtual methods
.method public ReadData()Ljava/lang/String;
    .locals 1

    iget-object v0, p0, Lcom/konka/kkmultiscreen/MsgDef$MsgInner;->data:Ljava/lang/String;

    return-object v0
.end method

.method public ReadOrder()I
    .locals 1

    iget v0, p0, Lcom/konka/kkmultiscreen/MsgDef$MsgInner;->order:I

    return v0
.end method

.method public SetData(Ljava/lang/String;)V
    .locals 0
    .param p1    # Ljava/lang/String;

    iput-object p1, p0, Lcom/konka/kkmultiscreen/MsgDef$MsgInner;->data:Ljava/lang/String;

    return-void
.end method

.method public setBroadcastDataCmd()V
    .locals 1

    const/16 v0, 0x2717

    iput v0, p0, Lcom/konka/kkmultiscreen/MsgDef$MsgInner;->order:I

    return-void
.end method

.method public setCloseConn()V
    .locals 1

    const/16 v0, 0x2712

    iput v0, p0, Lcom/konka/kkmultiscreen/MsgDef$MsgInner;->order:I

    return-void
.end method

.method public setConnType()V
    .locals 1

    const/16 v0, 0x2711

    iput v0, p0, Lcom/konka/kkmultiscreen/MsgDef$MsgInner;->order:I

    return-void
.end method

.method public setExitSerivce()V
    .locals 1

    const/16 v0, 0x2714

    iput v0, p0, Lcom/konka/kkmultiscreen/MsgDef$MsgInner;->order:I

    return-void
.end method

.method public setInitSearchMod()V
    .locals 1

    const/16 v0, 0x2716

    iput v0, p0, Lcom/konka/kkmultiscreen/MsgDef$MsgInner;->order:I

    return-void
.end method

.method public setReConn()V
    .locals 1

    const/16 v0, 0x2713

    iput v0, p0, Lcom/konka/kkmultiscreen/MsgDef$MsgInner;->order:I

    return-void
.end method

.method public setToGetTvIP()V
    .locals 1

    const/16 v0, 0x2715

    iput v0, p0, Lcom/konka/kkmultiscreen/MsgDef$MsgInner;->order:I

    return-void
.end method

.method public setToastDataCmd()V
    .locals 1

    const/16 v0, 0x2718

    iput v0, p0, Lcom/konka/kkmultiscreen/MsgDef$MsgInner;->order:I

    return-void
.end method

.method public setType(I)V
    .locals 0
    .param p1    # I

    iput p1, p0, Lcom/konka/kkmultiscreen/MsgDef$MsgInner;->order:I

    return-void
.end method
