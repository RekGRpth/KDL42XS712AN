.class public Lcom/konka/kkmultiscreen/DataService;
.super Landroid/app/IntentService;
.source "DataService.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/konka/kkmultiscreen/DataService$BroadMsg;,
        Lcom/konka/kkmultiscreen/DataService$CommandReceiver;,
        Lcom/konka/kkmultiscreen/DataService$MyBinder;
    }
.end annotation


# static fields
.field private static final DESCRIPTOR_LEN:I = 0x8

.field private static final INNERMSG:Ljava/lang/Integer;

.field private static final OUTERMSG:Ljava/lang/Integer;

.field private static final TAG:Ljava/lang/String; = "DataService"

.field private static final apkFolder:Ljava/io/File;

.field static apkStr:Ljava/lang/String;

.field private static getOffSet:I

.field private static targApkName:Ljava/lang/String;

.field private static targFilePath:Ljava/lang/String;

.field private static yidianFilePath:Ljava/lang/String;


# instance fields
.field private cmdReceiver:Lcom/konka/kkmultiscreen/DataService$CommandReceiver;

.field private myBinder:Lcom/konka/kkmultiscreen/DataService$MyBinder;

.field private packethandle:LprotocolAnalysis/analysis/packetHandle;


# direct methods
.method static constructor <clinit>()V
    .locals 2

    const/16 v0, 0x4e21

    invoke-static {v0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v0

    sput-object v0, Lcom/konka/kkmultiscreen/DataService;->OUTERMSG:Ljava/lang/Integer;

    const/16 v0, 0x4e22

    invoke-static {v0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v0

    sput-object v0, Lcom/konka/kkmultiscreen/DataService;->INNERMSG:Ljava/lang/Integer;

    const-string v0, "/data/data/com.konka.kkmultiscreen/APK/"

    sput-object v0, Lcom/konka/kkmultiscreen/DataService;->apkStr:Ljava/lang/String;

    new-instance v0, Ljava/io/File;

    sget-object v1, Lcom/konka/kkmultiscreen/DataService;->apkStr:Ljava/lang/String;

    invoke-direct {v0, v1}, Ljava/io/File;-><init>(Ljava/lang/String;)V

    sput-object v0, Lcom/konka/kkmultiscreen/DataService;->apkFolder:Ljava/io/File;

    const-string v0, "multiScreenPlayer.apk"

    sput-object v0, Lcom/konka/kkmultiscreen/DataService;->targApkName:Ljava/lang/String;

    new-instance v0, Ljava/lang/StringBuilder;

    sget-object v1, Lcom/konka/kkmultiscreen/DataService;->apkStr:Ljava/lang/String;

    invoke-static {v1}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    sget-object v1, Lcom/konka/kkmultiscreen/DataService;->targApkName:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    sput-object v0, Lcom/konka/kkmultiscreen/DataService;->targFilePath:Ljava/lang/String;

    new-instance v0, Ljava/lang/StringBuilder;

    sget-object v1, Lcom/konka/kkmultiscreen/DataService;->apkStr:Ljava/lang/String;

    invoke-static {v1}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    const-string v1, "yidian.apk"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    sput-object v0, Lcom/konka/kkmultiscreen/DataService;->yidianFilePath:Ljava/lang/String;

    const/16 v0, 0x8

    sput v0, Lcom/konka/kkmultiscreen/DataService;->getOffSet:I

    return-void
.end method

.method public constructor <init>()V
    .locals 2

    const/4 v1, 0x0

    const-string v0, "DataService"

    invoke-direct {p0, v0}, Landroid/app/IntentService;-><init>(Ljava/lang/String;)V

    new-instance v0, Lcom/konka/kkmultiscreen/DataService$MyBinder;

    invoke-direct {v0, p0}, Lcom/konka/kkmultiscreen/DataService$MyBinder;-><init>(Lcom/konka/kkmultiscreen/DataService;)V

    iput-object v0, p0, Lcom/konka/kkmultiscreen/DataService;->myBinder:Lcom/konka/kkmultiscreen/DataService$MyBinder;

    iput-object v1, p0, Lcom/konka/kkmultiscreen/DataService;->cmdReceiver:Lcom/konka/kkmultiscreen/DataService$CommandReceiver;

    iput-object v1, p0, Lcom/konka/kkmultiscreen/DataService;->packethandle:LprotocolAnalysis/analysis/packetHandle;

    return-void
.end method

.method public constructor <init>(Ljava/lang/String;)V
    .locals 2
    .param p1    # Ljava/lang/String;

    const/4 v1, 0x0

    invoke-direct {p0, p1}, Landroid/app/IntentService;-><init>(Ljava/lang/String;)V

    new-instance v0, Lcom/konka/kkmultiscreen/DataService$MyBinder;

    invoke-direct {v0, p0}, Lcom/konka/kkmultiscreen/DataService$MyBinder;-><init>(Lcom/konka/kkmultiscreen/DataService;)V

    iput-object v0, p0, Lcom/konka/kkmultiscreen/DataService;->myBinder:Lcom/konka/kkmultiscreen/DataService$MyBinder;

    iput-object v1, p0, Lcom/konka/kkmultiscreen/DataService;->cmdReceiver:Lcom/konka/kkmultiscreen/DataService$CommandReceiver;

    iput-object v1, p0, Lcom/konka/kkmultiscreen/DataService;->packethandle:LprotocolAnalysis/analysis/packetHandle;

    const-string v0, "DataService"

    const-string v1, "DataService()"

    invoke-static {v0, v1}, Lutil/log/CLog;->wLog(Ljava/lang/String;Ljava/lang/String;)V

    return-void
.end method

.method private DoNetWorking(Ljava/lang/String;I)V
    .locals 3
    .param p1    # Ljava/lang/String;
    .param p2    # I

    const-string v1, "DataService"

    const-string v2, "DoNetWorking......"

    invoke-static {v1, v2}, Lutil/log/CLog;->wLog(Ljava/lang/String;Ljava/lang/String;)V

    new-instance v0, Lcom/konka/kkmultiscreen/DataService$1;

    invoke-direct {v0, p0, p2}, Lcom/konka/kkmultiscreen/DataService$1;-><init>(Lcom/konka/kkmultiscreen/DataService;I)V

    invoke-virtual {v0}, Ljava/lang/Thread;->start()V

    return-void
.end method

.method static synthetic access$0(Lcom/konka/kkmultiscreen/DataService;)LprotocolAnalysis/analysis/packetHandle;
    .locals 1

    iget-object v0, p0, Lcom/konka/kkmultiscreen/DataService;->packethandle:LprotocolAnalysis/analysis/packetHandle;

    return-object v0
.end method

.method static synthetic access$1()Ljava/lang/Integer;
    .locals 1

    sget-object v0, Lcom/konka/kkmultiscreen/DataService;->OUTERMSG:Ljava/lang/Integer;

    return-object v0
.end method

.method static synthetic access$2()Ljava/lang/Integer;
    .locals 1

    sget-object v0, Lcom/konka/kkmultiscreen/DataService;->INNERMSG:Ljava/lang/Integer;

    return-object v0
.end method

.method static synthetic access$3(I)V
    .locals 0

    sput p0, Lcom/konka/kkmultiscreen/DataService;->getOffSet:I

    return-void
.end method

.method public static creatAPKFile()V
    .locals 2

    :try_start_0
    sget-object v1, Lcom/konka/kkmultiscreen/DataService;->apkFolder:Ljava/io/File;

    invoke-virtual {v1}, Ljava/io/File;->exists()Z

    move-result v1

    if-eqz v1, :cond_0

    sget-object v1, Lcom/konka/kkmultiscreen/DataService;->apkFolder:Ljava/io/File;

    invoke-virtual {v1}, Ljava/io/File;->isDirectory()Z

    move-result v1

    if-eqz v1, :cond_0

    invoke-static {}, Lcom/konka/kkmultiscreen/DataService;->createPlayer()V

    :goto_0
    return-void

    :cond_0
    sget-object v1, Lcom/konka/kkmultiscreen/DataService;->apkFolder:Ljava/io/File;

    invoke-virtual {v1}, Ljava/io/File;->mkdir()Z
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    goto :goto_0

    :catch_0
    move-exception v0

    invoke-virtual {v0}, Ljava/lang/Exception;->printStackTrace()V

    goto :goto_0
.end method

.method public static creatApkFile()V
    .locals 6

    new-instance v0, Ljava/io/File;

    sget-object v2, Lcom/konka/kkmultiscreen/DataService;->apkFolder:Ljava/io/File;

    sget-object v3, Lcom/konka/kkmultiscreen/DataService;->targApkName:Ljava/lang/String;

    invoke-direct {v0, v2, v3}, Ljava/io/File;-><init>(Ljava/io/File;Ljava/lang/String;)V

    invoke-virtual {v0}, Ljava/io/File;->exists()Z

    move-result v2

    if-eqz v2, :cond_0

    invoke-virtual {v0}, Ljava/io/File;->isFile()Z

    move-result v2

    if-eqz v2, :cond_0

    invoke-virtual {v0}, Ljava/io/File;->length()J

    move-result-wide v2

    const-wide/16 v4, 0x0

    cmp-long v2, v2, v4

    if-nez v2, :cond_1

    :cond_0
    :try_start_0
    invoke-virtual {v0}, Ljava/io/File;->createNewFile()Z
    :try_end_0
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_0

    :cond_1
    :goto_0
    return-void

    :catch_0
    move-exception v1

    invoke-virtual {v1}, Ljava/io/IOException;->printStackTrace()V

    goto :goto_0
.end method

.method public static creatApkFolder()V
    .locals 2

    :try_start_0
    sget-object v1, Lcom/konka/kkmultiscreen/DataService;->apkFolder:Ljava/io/File;

    invoke-virtual {v1}, Ljava/io/File;->exists()Z

    move-result v1

    if-eqz v1, :cond_0

    sget-object v1, Lcom/konka/kkmultiscreen/DataService;->apkFolder:Ljava/io/File;

    invoke-virtual {v1}, Ljava/io/File;->isDirectory()Z

    move-result v1

    if-eqz v1, :cond_0

    invoke-static {}, Lcom/konka/kkmultiscreen/DataService;->creatApkFile()V

    :goto_0
    return-void

    :cond_0
    sget-object v1, Lcom/konka/kkmultiscreen/DataService;->apkFolder:Ljava/io/File;

    invoke-virtual {v1}, Ljava/io/File;->mkdir()Z

    invoke-static {}, Lcom/konka/kkmultiscreen/DataService;->creatApkFile()V
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    goto :goto_0

    :catch_0
    move-exception v0

    invoke-virtual {v0}, Ljava/lang/Exception;->printStackTrace()V

    goto :goto_0
.end method

.method public static createPlayer()V
    .locals 6

    new-instance v0, Ljava/io/File;

    sget-object v2, Lcom/konka/kkmultiscreen/DataService;->apkFolder:Ljava/io/File;

    sget-object v3, Lcom/konka/kkmultiscreen/DataService;->targApkName:Ljava/lang/String;

    invoke-direct {v0, v2, v3}, Ljava/io/File;-><init>(Ljava/io/File;Ljava/lang/String;)V

    invoke-virtual {v0}, Ljava/io/File;->exists()Z

    move-result v2

    if-eqz v2, :cond_0

    invoke-virtual {v0}, Ljava/io/File;->isFile()Z

    move-result v2

    if-eqz v2, :cond_0

    invoke-virtual {v0}, Ljava/io/File;->length()J

    move-result-wide v2

    const-wide/16 v4, 0x0

    cmp-long v2, v2, v4

    if-nez v2, :cond_1

    :cond_0
    :try_start_0
    invoke-virtual {v0}, Ljava/io/File;->createNewFile()Z

    sget-object v2, Ljava/lang/System;->out:Ljava/io/PrintStream;

    const-string v3, "file tvliveserver create success-----1"

    invoke-virtual {v2, v3}, Ljava/io/PrintStream;->println(Ljava/lang/String;)V
    :try_end_0
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_0

    :cond_1
    :goto_0
    return-void

    :catch_0
    move-exception v1

    sget-object v2, Ljava/lang/System;->out:Ljava/io/PrintStream;

    const-string v3, "Fail-----"

    invoke-virtual {v2, v3}, Ljava/io/PrintStream;->println(Ljava/lang/String;)V

    invoke-virtual {v1}, Ljava/io/IOException;->printStackTrace()V

    goto :goto_0
.end method

.method private installApk(I)Z
    .locals 12
    .param p1    # I

    const/4 v11, 0x1

    :try_start_0
    invoke-virtual {p0}, Lcom/konka/kkmultiscreen/DataService;->getBaseContext()Landroid/content/Context;

    move-result-object v8

    invoke-virtual {v8}, Landroid/content/Context;->getAssets()Landroid/content/res/AssetManager;

    move-result-object v0

    const-string v8, "multiScreenPlayer.apk"

    invoke-virtual {v0, v8}, Landroid/content/res/AssetManager;->open(Ljava/lang/String;)Ljava/io/InputStream;

    move-result-object v7

    invoke-static {}, Lcom/konka/kkmultiscreen/DataService;->creatApkFolder()V

    new-instance v5, Ljava/io/FileOutputStream;

    sget-object v8, Lcom/konka/kkmultiscreen/DataService;->targFilePath:Ljava/lang/String;

    invoke-direct {v5, v8}, Ljava/io/FileOutputStream;-><init>(Ljava/lang/String;)V

    const-string v8, "multiScreenPlayer.apk"

    const/4 v9, 0x1

    invoke-virtual {p0, v8, v9}, Lcom/konka/kkmultiscreen/DataService;->openFileOutput(Ljava/lang/String;I)Ljava/io/FileOutputStream;

    move-result-object v5

    const/16 v8, 0x1400

    new-array v2, v8, [B

    const/4 v3, 0x0

    :goto_0
    invoke-virtual {v7, v2}, Ljava/io/InputStream;->read([B)I

    move-result v3

    const/4 v8, -0x1

    if-ne v3, v8, :cond_0

    new-instance v6, Landroid/content/Intent;

    invoke-direct {v6}, Landroid/content/Intent;-><init>()V

    const/high16 v8, 0x10000000

    invoke-virtual {v6, v8}, Landroid/content/Intent;->addFlags(I)Landroid/content/Intent;

    const-string v8, "android.intent.action.VIEW"

    invoke-virtual {v6, v8}, Landroid/content/Intent;->setAction(Ljava/lang/String;)Landroid/content/Intent;

    new-instance v8, Ljava/io/File;

    new-instance v9, Ljava/lang/StringBuilder;

    invoke-direct {v9}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {p0}, Lcom/konka/kkmultiscreen/DataService;->getFilesDir()Ljava/io/File;

    move-result-object v10

    invoke-virtual {v9, v10}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v9

    const-string v10, "/multiScreenPlayer.apk"

    invoke-virtual {v9, v10}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v9

    invoke-virtual {v9}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v9

    invoke-direct {v8, v9}, Ljava/io/File;-><init>(Ljava/lang/String;)V

    invoke-static {v8}, Landroid/net/Uri;->fromFile(Ljava/io/File;)Landroid/net/Uri;

    move-result-object v1

    const-string v8, "DataService"

    new-instance v9, Ljava/lang/StringBuilder;

    const-string v10, "apkpath "

    invoke-direct {v9, v10}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v1}, Landroid/net/Uri;->toString()Ljava/lang/String;

    move-result-object v10

    invoke-virtual {v9, v10}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v9

    invoke-virtual {v9}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v9

    invoke-static {v8, v9}, Landroid/util/Log;->v(Ljava/lang/String;Ljava/lang/String;)I

    const-string v8, "application/vnd.android.package-archive"

    invoke-virtual {v6, v1, v8}, Landroid/content/Intent;->setDataAndType(Landroid/net/Uri;Ljava/lang/String;)Landroid/content/Intent;

    invoke-virtual {p0, v6}, Lcom/konka/kkmultiscreen/DataService;->startActivity(Landroid/content/Intent;)V

    invoke-virtual {v5}, Ljava/io/OutputStream;->close()V

    invoke-virtual {v7}, Ljava/io/InputStream;->close()V

    :goto_1
    return v11

    :cond_0
    const/4 v8, 0x0

    invoke-virtual {v5, v2, v8, v3}, Ljava/io/OutputStream;->write([BII)V

    sget-object v8, Ljava/lang/System;->out:Ljava/io/PrintStream;

    new-instance v9, Ljava/lang/StringBuilder;

    const-string v10, "byteread---- "

    invoke-direct {v9, v10}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v9, v3}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v9

    invoke-virtual {v9}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v9

    invoke-virtual {v8, v9}, Ljava/io/PrintStream;->println(Ljava/lang/String;)V
    :try_end_0
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_0

    goto :goto_0

    :catch_0
    move-exception v4

    invoke-virtual {v4}, Ljava/io/IOException;->printStackTrace()V

    const-string v8, "DataService"

    const-string v9, "install service apk catch IOException "

    invoke-static {v8, v9}, Landroid/util/Log;->v(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_1
.end method

.method private sendBroadMsg(Ljava/lang/String;)V
    .locals 0
    .param p1    # Ljava/lang/String;

    return-void
.end method


# virtual methods
.method public getIBind()Lcom/konka/kkmultiscreen/DataService$MyBinder;
    .locals 2

    const-string v0, "DataService"

    const-string v1, "getIBind"

    invoke-static {v0, v1}, Lutil/log/CLog;->wLog(Ljava/lang/String;Ljava/lang/String;)V

    iget-object v0, p0, Lcom/konka/kkmultiscreen/DataService;->myBinder:Lcom/konka/kkmultiscreen/DataService$MyBinder;

    return-object v0
.end method

.method public installYidian(I)Z
    .locals 12
    .param p1    # I

    const/4 v11, 0x1

    const-string v8, "weikan"

    const-string v9, "richard start yidian ---------------------3"

    invoke-static {v8, v9}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    :try_start_0
    invoke-virtual {p0}, Lcom/konka/kkmultiscreen/DataService;->getBaseContext()Landroid/content/Context;

    move-result-object v8

    invoke-virtual {v8}, Landroid/content/Context;->getAssets()Landroid/content/res/AssetManager;

    move-result-object v0

    const-string v8, "yidian.apk"

    invoke-virtual {v0, v8}, Landroid/content/res/AssetManager;->open(Ljava/lang/String;)Ljava/io/InputStream;

    move-result-object v7

    invoke-static {}, Lcom/konka/kkmultiscreen/DataService;->creatApkFolder()V

    new-instance v5, Ljava/io/FileOutputStream;

    sget-object v8, Lcom/konka/kkmultiscreen/DataService;->yidianFilePath:Ljava/lang/String;

    invoke-direct {v5, v8}, Ljava/io/FileOutputStream;-><init>(Ljava/lang/String;)V

    const-string v8, "yidian.apk"

    const/4 v9, 0x1

    invoke-virtual {p0, v8, v9}, Lcom/konka/kkmultiscreen/DataService;->openFileOutput(Ljava/lang/String;I)Ljava/io/FileOutputStream;

    move-result-object v5

    const/16 v8, 0x1400

    new-array v2, v8, [B

    const/4 v3, 0x0

    :goto_0
    invoke-virtual {v7, v2}, Ljava/io/InputStream;->read([B)I

    move-result v3

    const/4 v8, -0x1

    if-ne v3, v8, :cond_0

    new-instance v6, Landroid/content/Intent;

    invoke-direct {v6}, Landroid/content/Intent;-><init>()V

    const/high16 v8, 0x10000000

    invoke-virtual {v6, v8}, Landroid/content/Intent;->addFlags(I)Landroid/content/Intent;

    const-string v8, "android.intent.action.VIEW"

    invoke-virtual {v6, v8}, Landroid/content/Intent;->setAction(Ljava/lang/String;)Landroid/content/Intent;

    new-instance v8, Ljava/io/File;

    new-instance v9, Ljava/lang/StringBuilder;

    invoke-direct {v9}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {p0}, Lcom/konka/kkmultiscreen/DataService;->getFilesDir()Ljava/io/File;

    move-result-object v10

    invoke-virtual {v9, v10}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v9

    const-string v10, "/yidian.apk"

    invoke-virtual {v9, v10}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v9

    invoke-virtual {v9}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v9

    invoke-direct {v8, v9}, Ljava/io/File;-><init>(Ljava/lang/String;)V

    invoke-static {v8}, Landroid/net/Uri;->fromFile(Ljava/io/File;)Landroid/net/Uri;

    move-result-object v1

    const-string v8, "DataService"

    new-instance v9, Ljava/lang/StringBuilder;

    const-string v10, "apkpath "

    invoke-direct {v9, v10}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v1}, Landroid/net/Uri;->toString()Ljava/lang/String;

    move-result-object v10

    invoke-virtual {v9, v10}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v9

    invoke-virtual {v9}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v9

    invoke-static {v8, v9}, Landroid/util/Log;->v(Ljava/lang/String;Ljava/lang/String;)I

    const-string v8, "application/vnd.android.package-archive"

    invoke-virtual {v6, v1, v8}, Landroid/content/Intent;->setDataAndType(Landroid/net/Uri;Ljava/lang/String;)Landroid/content/Intent;

    invoke-virtual {p0, v6}, Lcom/konka/kkmultiscreen/DataService;->startActivity(Landroid/content/Intent;)V

    invoke-virtual {v5}, Ljava/io/OutputStream;->close()V

    invoke-virtual {v7}, Ljava/io/InputStream;->close()V

    :goto_1
    return v11

    :cond_0
    const/4 v8, 0x0

    invoke-virtual {v5, v2, v8, v3}, Ljava/io/OutputStream;->write([BII)V

    sget-object v8, Ljava/lang/System;->out:Ljava/io/PrintStream;

    new-instance v9, Ljava/lang/StringBuilder;

    const-string v10, "byteread---- "

    invoke-direct {v9, v10}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v9, v3}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v9

    invoke-virtual {v9}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v9

    invoke-virtual {v8, v9}, Ljava/io/PrintStream;->println(Ljava/lang/String;)V
    :try_end_0
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_0

    goto :goto_0

    :catch_0
    move-exception v4

    invoke-virtual {v4}, Ljava/io/IOException;->printStackTrace()V

    const-string v8, "DataService"

    const-string v9, "install service apk catch IOException "

    invoke-static {v8, v9}, Landroid/util/Log;->v(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_1
.end method

.method public isInstall(Ljava/lang/String;)Z
    .locals 8
    .param p1    # Ljava/lang/String;

    const/4 v4, 0x0

    invoke-virtual {p0}, Lcom/konka/kkmultiscreen/DataService;->getPackageManager()Landroid/content/pm/PackageManager;

    move-result-object v3

    invoke-virtual {v3, v4}, Landroid/content/pm/PackageManager;->getInstalledPackages(I)Ljava/util/List;

    move-result-object v0

    const-string v5, "DataService"

    new-instance v6, Ljava/lang/StringBuilder;

    const-string v7, "list size "

    invoke-direct {v6, v7}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-interface {v0}, Ljava/util/List;->size()I

    move-result v7

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v6}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v6

    invoke-static {v5, v6}, Landroid/util/Log;->v(Ljava/lang/String;Ljava/lang/String;)I

    if-nez v0, :cond_0

    :goto_0
    return v4

    :cond_0
    invoke-interface {v0}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v5

    :cond_1
    invoke-interface {v5}, Ljava/util/Iterator;->hasNext()Z

    move-result v6

    if-nez v6, :cond_2

    const/4 v4, 0x1

    goto :goto_0

    :cond_2
    invoke-interface {v5}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Landroid/content/pm/PackageInfo;

    iget-object v6, v2, Landroid/content/pm/PackageInfo;->packageName:Ljava/lang/String;

    invoke-virtual {v6, p1}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v6

    if-eqz v6, :cond_1

    new-instance v1, Landroid/os/Message;

    invoke-direct {v1}, Landroid/os/Message;-><init>()V

    goto :goto_0
.end method

.method public onBind(Landroid/content/Intent;)Landroid/os/IBinder;
    .locals 3
    .param p1    # Landroid/content/Intent;

    const-string v1, "DataService"

    const-string v2, "onBind."

    invoke-static {v1, v2}, Lutil/log/CLog;->wLog(Ljava/lang/String;Ljava/lang/String;)V

    iget-object v1, p0, Lcom/konka/kkmultiscreen/DataService;->myBinder:Lcom/konka/kkmultiscreen/DataService$MyBinder;

    if-nez v1, :cond_0

    new-instance v1, Lcom/konka/kkmultiscreen/DataService$MyBinder;

    invoke-direct {v1, p0}, Lcom/konka/kkmultiscreen/DataService$MyBinder;-><init>(Lcom/konka/kkmultiscreen/DataService;)V

    iput-object v1, p0, Lcom/konka/kkmultiscreen/DataService;->myBinder:Lcom/konka/kkmultiscreen/DataService$MyBinder;

    :cond_0
    new-instance v1, Lcom/konka/kkmultiscreen/DataService$CommandReceiver;

    invoke-direct {v1, p0}, Lcom/konka/kkmultiscreen/DataService$CommandReceiver;-><init>(Lcom/konka/kkmultiscreen/DataService;)V

    iput-object v1, p0, Lcom/konka/kkmultiscreen/DataService;->cmdReceiver:Lcom/konka/kkmultiscreen/DataService$CommandReceiver;

    new-instance v0, Landroid/content/IntentFilter;

    invoke-direct {v0}, Landroid/content/IntentFilter;-><init>()V

    const-string v1, "andriod.tv.network.rsp"

    invoke-virtual {v0, v1}, Landroid/content/IntentFilter;->addAction(Ljava/lang/String;)V

    iget-object v1, p0, Lcom/konka/kkmultiscreen/DataService;->cmdReceiver:Lcom/konka/kkmultiscreen/DataService$CommandReceiver;

    invoke-virtual {p0, v1, v0}, Lcom/konka/kkmultiscreen/DataService;->registerReceiver(Landroid/content/BroadcastReceiver;Landroid/content/IntentFilter;)Landroid/content/Intent;

    new-instance v1, LprotocolAnalysis/analysis/packetHandle;

    invoke-direct {v1}, LprotocolAnalysis/analysis/packetHandle;-><init>()V

    iput-object v1, p0, Lcom/konka/kkmultiscreen/DataService;->packethandle:LprotocolAnalysis/analysis/packetHandle;

    iget-object v1, p0, Lcom/konka/kkmultiscreen/DataService;->packethandle:LprotocolAnalysis/analysis/packetHandle;

    invoke-virtual {v1, p0}, LprotocolAnalysis/analysis/packetHandle;->registerPlayerStateBroadcastReceiver(Landroid/content/Context;)V

    iget-object v1, p0, Lcom/konka/kkmultiscreen/DataService;->packethandle:LprotocolAnalysis/analysis/packetHandle;

    invoke-virtual {v1, p0}, LprotocolAnalysis/analysis/packetHandle;->registerShareStateBroadcastReceiver(Landroid/content/Context;)V

    iget-object v1, p0, Lcom/konka/kkmultiscreen/DataService;->myBinder:Lcom/konka/kkmultiscreen/DataService$MyBinder;

    return-object v1
.end method

.method public onCreate()V
    .locals 0

    invoke-super {p0}, Landroid/app/IntentService;->onCreate()V

    return-void
.end method

.method public onDestroy()V
    .locals 2

    invoke-super {p0}, Landroid/app/IntentService;->onDestroy()V

    const-string v0, "DataService"

    const-string v1, "onDestroy."

    invoke-static {v0, v1}, Lutil/log/CLog;->wLog(Ljava/lang/String;Ljava/lang/String;)V

    iget-object v0, p0, Lcom/konka/kkmultiscreen/DataService;->cmdReceiver:Lcom/konka/kkmultiscreen/DataService$CommandReceiver;

    invoke-virtual {p0, v0}, Lcom/konka/kkmultiscreen/DataService;->unregisterReceiver(Landroid/content/BroadcastReceiver;)V

    iget-object v0, p0, Lcom/konka/kkmultiscreen/DataService;->packethandle:LprotocolAnalysis/analysis/packetHandle;

    invoke-virtual {v0, p0}, LprotocolAnalysis/analysis/packetHandle;->unregisterPlayerStateBroadcastReceiver(Landroid/content/Context;)V

    iget-object v0, p0, Lcom/konka/kkmultiscreen/DataService;->packethandle:LprotocolAnalysis/analysis/packetHandle;

    invoke-virtual {v0, p0}, LprotocolAnalysis/analysis/packetHandle;->unregisterShareStateBroadcastReceiver(Landroid/content/Context;)V

    return-void
.end method

.method protected onHandleIntent(Landroid/content/Intent;)V
    .locals 9
    .param p1    # Landroid/content/Intent;

    const v8, 0x7f040009    # com.konka.kkmultiscreen.R.string.install_reminder

    const/4 v7, 0x1

    invoke-static {}, Ljava/lang/Thread;->currentThread()Ljava/lang/Thread;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/Thread;->getId()J

    move-result-wide v1

    invoke-virtual {p1}, Landroid/content/Intent;->getExtras()Landroid/os/Bundle;

    move-result-object v0

    const/4 v3, 0x0

    if-eqz v0, :cond_0

    const-string v4, "KEY"

    invoke-virtual {v0, v4}, Landroid/os/Bundle;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v3

    :cond_0
    const-string v4, "DataService"

    new-instance v5, Ljava/lang/StringBuilder;

    const-string v6, "onHandleIntent(Intent intent) run in thread is: "

    invoke-direct {v5, v6}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v5, v1, v2}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v5

    const-string v6, " data: "

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    invoke-static {v4, v5}, Lutil/log/CLog;->wLog(Ljava/lang/String;Ljava/lang/String;)V

    const-string v4, "sendBestvAuth"

    invoke-virtual {v3, v4}, Ljava/lang/String;->indexOf(Ljava/lang/String;)I

    move-result v4

    if-ltz v4, :cond_2

    iget-object v4, p0, Lcom/konka/kkmultiscreen/DataService;->packethandle:LprotocolAnalysis/analysis/packetHandle;

    const-string v5, "@@"

    invoke-virtual {v3, v5}, Ljava/lang/String;->split(Ljava/lang/String;)[Ljava/lang/String;

    move-result-object v5

    aget-object v5, v5, v7

    const-string v6, "DATA"

    invoke-virtual {v0, v6}, Landroid/os/Bundle;->getByteArray(Ljava/lang/String;)[B

    move-result-object v6

    invoke-virtual {v4, v5, v6}, LprotocolAnalysis/analysis/packetHandle;->sendBestvAuth(Ljava/lang/String;[B)V

    :cond_1
    :goto_0
    return-void

    :cond_2
    const-string v4, "openInputMethod"

    invoke-virtual {v3, v4}, Ljava/lang/String;->indexOf(Ljava/lang/String;)I

    move-result v4

    if-ltz v4, :cond_4

    const-string v4, "com.konka.mediaSharePlayer"

    invoke-virtual {p0, v4}, Lcom/konka/kkmultiscreen/DataService;->isInstall(Ljava/lang/String;)Z

    move-result v4

    if-eqz v4, :cond_3

    const v4, 0x7f04000a    # com.konka.kkmultiscreen.R.string.install_input

    invoke-direct {p0, v4}, Lcom/konka/kkmultiscreen/DataService;->installApk(I)Z

    goto :goto_0

    :cond_3
    iget-object v4, p0, Lcom/konka/kkmultiscreen/DataService;->packethandle:LprotocolAnalysis/analysis/packetHandle;

    sget-object v5, Lcom/konka/kkmultiscreen/KKMutiScreenTvApp;->mApp:Lcom/konka/kkmultiscreen/KKMutiScreenTvApp;

    iget-object v5, v5, Lcom/konka/kkmultiscreen/KKMutiScreenTvApp;->mAppContext:Landroid/content/Context;

    invoke-virtual {v4, v5}, LprotocolAnalysis/analysis/packetHandle;->openInputMethod(Landroid/content/Context;)V

    goto :goto_0

    :cond_4
    const-string v4, "sendInputMethodData"

    invoke-virtual {v3, v4}, Ljava/lang/String;->indexOf(Ljava/lang/String;)I

    move-result v4

    if-ltz v4, :cond_5

    iget-object v4, p0, Lcom/konka/kkmultiscreen/DataService;->packethandle:LprotocolAnalysis/analysis/packetHandle;

    sget-object v5, Lcom/konka/kkmultiscreen/KKMutiScreenTvApp;->mApp:Lcom/konka/kkmultiscreen/KKMutiScreenTvApp;

    iget-object v5, v5, Lcom/konka/kkmultiscreen/KKMutiScreenTvApp;->mAppContext:Landroid/content/Context;

    const-string v6, "@@"

    invoke-virtual {v3, v6}, Ljava/lang/String;->split(Ljava/lang/String;)[Ljava/lang/String;

    move-result-object v6

    aget-object v6, v6, v7

    invoke-virtual {v4, v5, v6}, LprotocolAnalysis/analysis/packetHandle;->sendInputMethodResult(Landroid/content/Context;Ljava/lang/String;)V

    goto :goto_0

    :cond_5
    const-string v4, "closeInputMethod"

    invoke-virtual {v3, v4}, Ljava/lang/String;->indexOf(Ljava/lang/String;)I

    move-result v4

    if-ltz v4, :cond_6

    iget-object v4, p0, Lcom/konka/kkmultiscreen/DataService;->packethandle:LprotocolAnalysis/analysis/packetHandle;

    sget-object v5, Lcom/konka/kkmultiscreen/KKMutiScreenTvApp;->mApp:Lcom/konka/kkmultiscreen/KKMutiScreenTvApp;

    iget-object v5, v5, Lcom/konka/kkmultiscreen/KKMutiScreenTvApp;->mAppContext:Landroid/content/Context;

    invoke-virtual {v4, v5}, LprotocolAnalysis/analysis/packetHandle;->closeInputMethod(Landroid/content/Context;)V

    goto :goto_0

    :cond_6
    const-string v4, "startPlayer"

    invoke-virtual {v3, v4}, Ljava/lang/String;->indexOf(Ljava/lang/String;)I

    move-result v4

    if-ltz v4, :cond_8

    const-string v4, "com.konka.mediaSharePlayer"

    invoke-virtual {p0, v4}, Lcom/konka/kkmultiscreen/DataService;->isInstall(Ljava/lang/String;)Z

    move-result v4

    if-eqz v4, :cond_7

    invoke-direct {p0, v8}, Lcom/konka/kkmultiscreen/DataService;->installApk(I)Z

    goto :goto_0

    :cond_7
    iget-object v4, p0, Lcom/konka/kkmultiscreen/DataService;->packethandle:LprotocolAnalysis/analysis/packetHandle;

    sget-object v5, Lcom/konka/kkmultiscreen/KKMutiScreenTvApp;->mApp:Lcom/konka/kkmultiscreen/KKMutiScreenTvApp;

    iget-object v5, v5, Lcom/konka/kkmultiscreen/KKMutiScreenTvApp;->mAppContext:Landroid/content/Context;

    const-string v6, "DATA"

    invoke-virtual {v0, v6}, Landroid/os/Bundle;->getByteArray(Ljava/lang/String;)[B

    move-result-object v6

    invoke-virtual {v4, v5, v6}, LprotocolAnalysis/analysis/packetHandle;->startPlayer(Landroid/content/Context;[B)V

    goto :goto_0

    :cond_8
    const-string v4, "stopPlay"

    invoke-virtual {v3, v4}, Ljava/lang/String;->indexOf(Ljava/lang/String;)I

    move-result v4

    if-ltz v4, :cond_9

    iget-object v4, p0, Lcom/konka/kkmultiscreen/DataService;->packethandle:LprotocolAnalysis/analysis/packetHandle;

    sget-object v5, Lcom/konka/kkmultiscreen/KKMutiScreenTvApp;->mApp:Lcom/konka/kkmultiscreen/KKMutiScreenTvApp;

    iget-object v5, v5, Lcom/konka/kkmultiscreen/KKMutiScreenTvApp;->mAppContext:Landroid/content/Context;

    invoke-virtual {v4, v5}, LprotocolAnalysis/analysis/packetHandle;->stopPlay(Landroid/content/Context;)V

    goto/16 :goto_0

    :cond_9
    const-string v4, "seekTo"

    invoke-virtual {v3, v4}, Ljava/lang/String;->indexOf(Ljava/lang/String;)I

    move-result v4

    if-ltz v4, :cond_a

    iget-object v4, p0, Lcom/konka/kkmultiscreen/DataService;->packethandle:LprotocolAnalysis/analysis/packetHandle;

    sget-object v5, Lcom/konka/kkmultiscreen/KKMutiScreenTvApp;->mApp:Lcom/konka/kkmultiscreen/KKMutiScreenTvApp;

    iget-object v5, v5, Lcom/konka/kkmultiscreen/KKMutiScreenTvApp;->mAppContext:Landroid/content/Context;

    const-string v6, "@@"

    invoke-virtual {v3, v6}, Ljava/lang/String;->split(Ljava/lang/String;)[Ljava/lang/String;

    move-result-object v6

    aget-object v6, v6, v7

    invoke-static {v6}, Ljava/lang/Integer;->parseInt(Ljava/lang/String;)I

    move-result v6

    invoke-virtual {v4, v5, v6}, LprotocolAnalysis/analysis/packetHandle;->seekTo(Landroid/content/Context;I)V

    goto/16 :goto_0

    :cond_a
    const-string v4, "exitPlayer"

    invoke-virtual {v3, v4}, Ljava/lang/String;->indexOf(Ljava/lang/String;)I

    move-result v4

    if-ltz v4, :cond_b

    iget-object v4, p0, Lcom/konka/kkmultiscreen/DataService;->packethandle:LprotocolAnalysis/analysis/packetHandle;

    sget-object v5, Lcom/konka/kkmultiscreen/KKMutiScreenTvApp;->mApp:Lcom/konka/kkmultiscreen/KKMutiScreenTvApp;

    iget-object v5, v5, Lcom/konka/kkmultiscreen/KKMutiScreenTvApp;->mAppContext:Landroid/content/Context;

    invoke-virtual {v4, v5}, LprotocolAnalysis/analysis/packetHandle;->exitPlayer(Landroid/content/Context;)V

    goto/16 :goto_0

    :cond_b
    const-string v4, "Statistical"

    invoke-virtual {v3, v4}, Ljava/lang/String;->indexOf(Ljava/lang/String;)I

    move-result v4

    if-ltz v4, :cond_c

    iget-object v4, p0, Lcom/konka/kkmultiscreen/DataService;->packethandle:LprotocolAnalysis/analysis/packetHandle;

    sget-object v5, Lcom/konka/kkmultiscreen/KKMutiScreenTvApp;->mApp:Lcom/konka/kkmultiscreen/KKMutiScreenTvApp;

    iget-object v5, v5, Lcom/konka/kkmultiscreen/KKMutiScreenTvApp;->mAppContext:Landroid/content/Context;

    const-string v6, "DATA"

    invoke-virtual {v0, v6}, Landroid/os/Bundle;->getByteArray(Ljava/lang/String;)[B

    move-result-object v6

    invoke-virtual {v4, v5, v6}, LprotocolAnalysis/analysis/packetHandle;->multiScreenStatisticalInterface(Landroid/content/Context;[B)V

    goto/16 :goto_0

    :cond_c
    const-string v4, "sendPlayerStateToMobile"

    invoke-virtual {v3, v4}, Ljava/lang/String;->indexOf(Ljava/lang/String;)I

    move-result v4

    if-ltz v4, :cond_d

    iget-object v4, p0, Lcom/konka/kkmultiscreen/DataService;->packethandle:LprotocolAnalysis/analysis/packetHandle;

    const-string v5, "@@"

    invoke-virtual {v3, v5}, Ljava/lang/String;->split(Ljava/lang/String;)[Ljava/lang/String;

    move-result-object v5

    aget-object v5, v5, v7

    invoke-virtual {v4, v5}, LprotocolAnalysis/analysis/packetHandle;->sendPlayerStateToMobile(Ljava/lang/String;)V

    goto/16 :goto_0

    :cond_d
    const-string v4, "resumePos"

    invoke-virtual {v3, v4}, Ljava/lang/String;->indexOf(Ljava/lang/String;)I

    move-result v4

    if-ltz v4, :cond_e

    iget-object v4, p0, Lcom/konka/kkmultiscreen/DataService;->packethandle:LprotocolAnalysis/analysis/packetHandle;

    sget-object v5, Lcom/konka/kkmultiscreen/KKMutiScreenTvApp;->mApp:Lcom/konka/kkmultiscreen/KKMutiScreenTvApp;

    iget-object v5, v5, Lcom/konka/kkmultiscreen/KKMutiScreenTvApp;->mAppContext:Landroid/content/Context;

    const-string v6, "DATA"

    invoke-virtual {v0, v6}, Landroid/os/Bundle;->getByteArray(Ljava/lang/String;)[B

    move-result-object v6

    invoke-virtual {v4, v5, v6}, LprotocolAnalysis/analysis/packetHandle;->startPlayerResumePosFromMobile(Landroid/content/Context;[B)V

    goto/16 :goto_0

    :cond_e
    const-string v4, "sendCurrPlayStateToMobile"

    invoke-virtual {v3, v4}, Ljava/lang/String;->indexOf(Ljava/lang/String;)I

    move-result v4

    if-ltz v4, :cond_f

    iget-object v4, p0, Lcom/konka/kkmultiscreen/DataService;->packethandle:LprotocolAnalysis/analysis/packetHandle;

    sget-object v5, Lcom/konka/kkmultiscreen/KKMutiScreenTvApp;->mApp:Lcom/konka/kkmultiscreen/KKMutiScreenTvApp;

    iget-object v5, v5, Lcom/konka/kkmultiscreen/KKMutiScreenTvApp;->mAppContext:Landroid/content/Context;

    invoke-virtual {v4, v5}, LprotocolAnalysis/analysis/packetHandle;->sendCurrPlayStateToMobile(Landroid/content/Context;)V

    goto/16 :goto_0

    :cond_f
    const-string v4, "startPlayWeiKan"

    invoke-virtual {v3, v4}, Ljava/lang/String;->indexOf(Ljava/lang/String;)I

    move-result v4

    if-ltz v4, :cond_11

    const-string v4, "weikan"

    const-string v5, "have been ready to start play weikan!!!"

    invoke-static {v4, v5}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    const-string v4, "com.konka.mediaSharePlayer"

    invoke-virtual {p0, v4}, Lcom/konka/kkmultiscreen/DataService;->isInstall(Ljava/lang/String;)Z

    move-result v4

    if-eqz v4, :cond_10

    invoke-direct {p0, v8}, Lcom/konka/kkmultiscreen/DataService;->installApk(I)Z

    goto/16 :goto_0

    :cond_10
    iget-object v4, p0, Lcom/konka/kkmultiscreen/DataService;->packethandle:LprotocolAnalysis/analysis/packetHandle;

    sget-object v5, Lcom/konka/kkmultiscreen/KKMutiScreenTvApp;->mApp:Lcom/konka/kkmultiscreen/KKMutiScreenTvApp;

    iget-object v5, v5, Lcom/konka/kkmultiscreen/KKMutiScreenTvApp;->mAppContext:Landroid/content/Context;

    const-string v6, "DATA"

    invoke-virtual {v0, v6}, Landroid/os/Bundle;->getByteArray(Ljava/lang/String;)[B

    move-result-object v6

    invoke-virtual {v4, v5, v6}, LprotocolAnalysis/analysis/packetHandle;->startPlayWeiKan(Landroid/content/Context;[B)V

    goto/16 :goto_0

    :cond_11
    const-string v4, "startYidian"

    invoke-virtual {v3, v4}, Ljava/lang/String;->indexOf(Ljava/lang/String;)I

    move-result v4

    if-ltz v4, :cond_13

    const-string v4, "weikan"

    const-string v5, "richard start yidian ---------------------1"

    invoke-static {v4, v5}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    const-string v4, "com.rockitv.android"

    invoke-virtual {p0, v4}, Lcom/konka/kkmultiscreen/DataService;->isInstall(Ljava/lang/String;)Z

    move-result v4

    if-eqz v4, :cond_12

    const-string v4, "weikan"

    const-string v5, "richard start yidian ---------------------2"

    invoke-static {v4, v5}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    invoke-virtual {p0, v8}, Lcom/konka/kkmultiscreen/DataService;->installYidian(I)Z

    goto/16 :goto_0

    :cond_12
    const-string v4, "weikan"

    const-string v5, "richard start yidian ---------------------4"

    invoke-static {v4, v5}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    iget-object v4, p0, Lcom/konka/kkmultiscreen/DataService;->packethandle:LprotocolAnalysis/analysis/packetHandle;

    sget-object v5, Lcom/konka/kkmultiscreen/KKMutiScreenTvApp;->mApp:Lcom/konka/kkmultiscreen/KKMutiScreenTvApp;

    iget-object v5, v5, Lcom/konka/kkmultiscreen/KKMutiScreenTvApp;->mAppContext:Landroid/content/Context;

    invoke-virtual {v4, v5}, LprotocolAnalysis/analysis/packetHandle;->startYidianService(Landroid/content/Context;)V

    goto/16 :goto_0

    :cond_13
    const-string v4, "WeiKanForward"

    invoke-virtual {v3, v4}, Ljava/lang/String;->indexOf(Ljava/lang/String;)I

    move-result v4

    if-ltz v4, :cond_14

    iget-object v4, p0, Lcom/konka/kkmultiscreen/DataService;->packethandle:LprotocolAnalysis/analysis/packetHandle;

    sget-object v5, Lcom/konka/kkmultiscreen/KKMutiScreenTvApp;->mApp:Lcom/konka/kkmultiscreen/KKMutiScreenTvApp;

    iget-object v5, v5, Lcom/konka/kkmultiscreen/KKMutiScreenTvApp;->mAppContext:Landroid/content/Context;

    const-string v6, "DATA"

    invoke-virtual {v0, v6}, Landroid/os/Bundle;->getByteArray(Ljava/lang/String;)[B

    move-result-object v6

    invoke-virtual {v4, v5, v6}, LprotocolAnalysis/analysis/packetHandle;->startWeikanForward(Landroid/content/Context;[B)V

    goto/16 :goto_0

    :cond_14
    const-string v4, "WeiKanBackward"

    invoke-virtual {v3, v4}, Ljava/lang/String;->indexOf(Ljava/lang/String;)I

    move-result v4

    if-ltz v4, :cond_1

    iget-object v4, p0, Lcom/konka/kkmultiscreen/DataService;->packethandle:LprotocolAnalysis/analysis/packetHandle;

    sget-object v5, Lcom/konka/kkmultiscreen/KKMutiScreenTvApp;->mApp:Lcom/konka/kkmultiscreen/KKMutiScreenTvApp;

    iget-object v5, v5, Lcom/konka/kkmultiscreen/KKMutiScreenTvApp;->mAppContext:Landroid/content/Context;

    const-string v6, "DATA"

    invoke-virtual {v0, v6}, Landroid/os/Bundle;->getByteArray(Ljava/lang/String;)[B

    move-result-object v6

    invoke-virtual {v4, v5, v6}, LprotocolAnalysis/analysis/packetHandle;->startWeikanBackward(Landroid/content/Context;[B)V

    goto/16 :goto_0
.end method

.method public onRebind(Landroid/content/Intent;)V
    .locals 2
    .param p1    # Landroid/content/Intent;

    const-string v0, "DataService"

    const-string v1, "onRebind"

    invoke-static {v0, v1}, Lutil/log/CLog;->wLog(Ljava/lang/String;Ljava/lang/String;)V

    invoke-super {p0, p1}, Landroid/app/IntentService;->onRebind(Landroid/content/Intent;)V

    return-void
.end method

.method public onStart(Landroid/content/Intent;I)V
    .locals 2
    .param p1    # Landroid/content/Intent;
    .param p2    # I

    const-string v0, "DataService"

    const-string v1, "onStart."

    invoke-static {v0, v1}, Lutil/log/CLog;->wLog(Ljava/lang/String;Ljava/lang/String;)V

    invoke-super {p0, p1, p2}, Landroid/app/IntentService;->onStart(Landroid/content/Intent;I)V

    return-void
.end method

.method public onStartCommand(Landroid/content/Intent;II)I
    .locals 2
    .param p1    # Landroid/content/Intent;
    .param p2    # I
    .param p3    # I

    const-string v0, "DataService"

    const-string v1, "onStartCommand."

    invoke-static {v0, v1}, Lutil/log/CLog;->wLog(Ljava/lang/String;Ljava/lang/String;)V

    const/4 p2, 0x1

    invoke-super {p0, p1, p2, p3}, Landroid/app/IntentService;->onStartCommand(Landroid/content/Intent;II)I

    move-result v0

    return v0
.end method

.method public onUnbind(Landroid/content/Intent;)Z
    .locals 2
    .param p1    # Landroid/content/Intent;

    const-string v0, "DataService"

    const-string v1, "onUnbind"

    invoke-static {v0, v1}, Lutil/log/CLog;->wLog(Ljava/lang/String;Ljava/lang/String;)V

    invoke-super {p0, p1}, Landroid/app/IntentService;->onUnbind(Landroid/content/Intent;)Z

    move-result v0

    return v0
.end method
