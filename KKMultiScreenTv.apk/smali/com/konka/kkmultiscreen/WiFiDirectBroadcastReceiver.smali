.class public Lcom/konka/kkmultiscreen/WiFiDirectBroadcastReceiver;
.super Landroid/content/BroadcastReceiver;
.source "WiFiDirectBroadcastReceiver.java"


# annotations
.annotation build Landroid/annotation/TargetApi;
    value = 0xe
.end annotation

.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/konka/kkmultiscreen/WiFiDirectBroadcastReceiver$DeviceActionListener;,
        Lcom/konka/kkmultiscreen/WiFiDirectBroadcastReceiver$DeviceListFragment;,
        Lcom/konka/kkmultiscreen/WiFiDirectBroadcastReceiver$notifyClientAsyncTask;,
        Lcom/konka/kkmultiscreen/WiFiDirectBroadcastReceiver$notifyServerAsyncTask;
    }
.end annotation


# static fields
.field private static final TAG:Ljava/lang/String; = "WiFiDirectBroadcastReceiver"

.field private static mApp:Lcom/konka/kkmultiscreen/KKMutiScreenTvApp;


# instance fields
.field private channel:Landroid/net/wifi/p2p/WifiP2pManager$Channel;

.field private mWiFiDirect:Lcom/konka/kkmultiscreen/WiFiDirectBroadcastReceiver$DeviceListFragment;

.field private manager:Landroid/net/wifi/p2p/WifiP2pManager;


# direct methods
.method public constructor <init>(Landroid/net/wifi/p2p/WifiP2pManager;Landroid/net/wifi/p2p/WifiP2pManager$Channel;Lcom/konka/kkmultiscreen/KKMutiScreenTvApp;)V
    .locals 1
    .param p1    # Landroid/net/wifi/p2p/WifiP2pManager;
    .param p2    # Landroid/net/wifi/p2p/WifiP2pManager$Channel;
    .param p3    # Lcom/konka/kkmultiscreen/KKMutiScreenTvApp;

    invoke-direct {p0}, Landroid/content/BroadcastReceiver;-><init>()V

    iput-object p1, p0, Lcom/konka/kkmultiscreen/WiFiDirectBroadcastReceiver;->manager:Landroid/net/wifi/p2p/WifiP2pManager;

    iput-object p2, p0, Lcom/konka/kkmultiscreen/WiFiDirectBroadcastReceiver;->channel:Landroid/net/wifi/p2p/WifiP2pManager$Channel;

    new-instance v0, Lcom/konka/kkmultiscreen/WiFiDirectBroadcastReceiver$DeviceListFragment;

    invoke-direct {v0}, Lcom/konka/kkmultiscreen/WiFiDirectBroadcastReceiver$DeviceListFragment;-><init>()V

    iput-object v0, p0, Lcom/konka/kkmultiscreen/WiFiDirectBroadcastReceiver;->mWiFiDirect:Lcom/konka/kkmultiscreen/WiFiDirectBroadcastReceiver$DeviceListFragment;

    sput-object p3, Lcom/konka/kkmultiscreen/WiFiDirectBroadcastReceiver;->mApp:Lcom/konka/kkmultiscreen/KKMutiScreenTvApp;

    return-void
.end method

.method static synthetic access$0()Lcom/konka/kkmultiscreen/KKMutiScreenTvApp;
    .locals 1

    sget-object v0, Lcom/konka/kkmultiscreen/WiFiDirectBroadcastReceiver;->mApp:Lcom/konka/kkmultiscreen/KKMutiScreenTvApp;

    return-object v0
.end method

.method public static getDeviceStatus(I)Ljava/lang/String;
    .locals 1
    .param p0    # I

    packed-switch p0, :pswitch_data_0

    const-string v0, "\u72b6\u6001\u65e0\u6cd5\u83b7\u53d6"

    :goto_0
    return-object v0

    :pswitch_0
    const-string v0, "\u53ef\u4ee5\u4f7f\u7528"

    goto :goto_0

    :pswitch_1
    const-string v0, "\u6b63\u5728\u5efa\u7acb\u8fde\u63a5"

    goto :goto_0

    :pswitch_2
    const-string v0, "\u5df2\u7ecf\u8fde\u63a5"

    goto :goto_0

    :pswitch_3
    const-string v0, "\u8fde\u63a5\u5931\u8d25"

    goto :goto_0

    :pswitch_4
    const-string v0, "\u65e0\u6cd5\u4f7f\u7528"

    goto :goto_0

    nop

    :pswitch_data_0
    .packed-switch 0x0
        :pswitch_2
        :pswitch_1
        :pswitch_3
        :pswitch_0
        :pswitch_4
    .end packed-switch
.end method


# virtual methods
.method public onReceive(Landroid/content/Context;Landroid/content/Intent;)V
    .locals 7
    .param p1    # Landroid/content/Context;
    .param p2    # Landroid/content/Intent;

    invoke-virtual {p2}, Landroid/content/Intent;->getAction()Ljava/lang/String;

    move-result-object v0

    const-string v4, "android.net.wifi.p2p.STATE_CHANGED"

    invoke-virtual {v4, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v4

    if-eqz v4, :cond_2

    const-string v4, "wifi_p2p_state"

    const/4 v5, -0x1

    invoke-virtual {p2, v4, v5}, Landroid/content/Intent;->getIntExtra(Ljava/lang/String;I)I

    move-result v3

    const/4 v4, 0x2

    if-ne v3, v4, :cond_1

    const-string v4, "WiFiDirectBroadcastReceiver"

    const-string v5, "WIFI_P2P_STATE_CHANGED_ACTION \u5bf9\u7b49\u7f51\u7edc\u5df2\u7ecf\u542f\u7528"

    invoke-static {v4, v5}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    sget-object v4, Lcom/konka/kkmultiscreen/WiFiDirectBroadcastReceiver;->mApp:Lcom/konka/kkmultiscreen/KKMutiScreenTvApp;

    iget-object v4, v4, Lcom/konka/kkmultiscreen/KKMutiScreenTvApp;->sysWifiDirect:Lcom/konka/InitApp/CWifiDirect;

    const/4 v5, 0x1

    iput-boolean v5, v4, Lcom/konka/InitApp/CWifiDirect;->isWifiP2pEnabled:Z

    sget-object v4, Lcom/konka/kkmultiscreen/WiFiDirectBroadcastReceiver;->mApp:Lcom/konka/kkmultiscreen/KKMutiScreenTvApp;

    iget-object v4, v4, Lcom/konka/kkmultiscreen/KKMutiScreenTvApp;->sysWifiDirect:Lcom/konka/InitApp/CWifiDirect;

    invoke-virtual {v4}, Lcom/konka/InitApp/CWifiDirect;->startWifiDirectSearchTread()V

    :goto_0
    const-string v4, "WiFiDirectBroadcastReceiver"

    new-instance v5, Ljava/lang/StringBuilder;

    const-string v6, "P2P state changed - "

    invoke-direct {v5, v6}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v5, v3}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    invoke-static {v4, v5}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    :cond_0
    :goto_1
    return-void

    :cond_1
    const-string v4, "WiFiDirectBroadcastReceiver"

    const-string v5, "WIFI_P2P_STATE_CHANGED_ACTION \u5bf9\u7b49\u7f51\u7edc\u5df2\u7ecf\u5173\u95ed"

    invoke-static {v4, v5}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    sget-object v4, Lcom/konka/kkmultiscreen/WiFiDirectBroadcastReceiver;->mApp:Lcom/konka/kkmultiscreen/KKMutiScreenTvApp;

    iget-object v4, v4, Lcom/konka/kkmultiscreen/KKMutiScreenTvApp;->sysWifiDirect:Lcom/konka/InitApp/CWifiDirect;

    const/4 v5, 0x0

    iput-boolean v5, v4, Lcom/konka/InitApp/CWifiDirect;->isWifiP2pEnabled:Z

    goto :goto_0

    :cond_2
    const-string v4, "android.net.wifi.p2p.PEERS_CHANGED"

    invoke-virtual {v4, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v4

    if-eqz v4, :cond_4

    const-string v4, "WiFiDirectBroadcastReceiver"

    const-string v5, "WIFI_P2P_PEERS_CHANGED_ACTION \u5bf9\u7b49\u7f51\u7edc\u5217\u8868\u53d1\u751f\u53d8\u5316"

    invoke-static {v4, v5}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    iget-object v4, p0, Lcom/konka/kkmultiscreen/WiFiDirectBroadcastReceiver;->manager:Landroid/net/wifi/p2p/WifiP2pManager;

    if-eqz v4, :cond_3

    const-string v4, "WiFiDirectBroadcastReceiver"

    const-string v5, "call requestPeers() ,callback on PeerListListener.onPeersAvailable()"

    invoke-static {v4, v5}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    iget-object v4, p0, Lcom/konka/kkmultiscreen/WiFiDirectBroadcastReceiver;->manager:Landroid/net/wifi/p2p/WifiP2pManager;

    iget-object v5, p0, Lcom/konka/kkmultiscreen/WiFiDirectBroadcastReceiver;->channel:Landroid/net/wifi/p2p/WifiP2pManager$Channel;

    iget-object v6, p0, Lcom/konka/kkmultiscreen/WiFiDirectBroadcastReceiver;->mWiFiDirect:Lcom/konka/kkmultiscreen/WiFiDirectBroadcastReceiver$DeviceListFragment;

    invoke-virtual {v4, v5, v6}, Landroid/net/wifi/p2p/WifiP2pManager;->requestPeers(Landroid/net/wifi/p2p/WifiP2pManager$Channel;Landroid/net/wifi/p2p/WifiP2pManager$PeerListListener;)V

    :cond_3
    const-string v4, "WiFiDirectBroadcastReceiver"

    const-string v5, "P2P peers changed"

    invoke-static {v4, v5}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_1

    :cond_4
    const-string v4, "android.net.wifi.p2p.CONNECTION_STATE_CHANGE"

    invoke-virtual {v4, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v4

    if-eqz v4, :cond_6

    const-string v4, "WiFiDirectBroadcastReceiver"

    const-string v5, "WIFI_P2P_CONNECTION_CHANGED_ACTION \u5f53\u524d\u8fde\u63a5\u7684\u5bf9\u7b49\u7f51\u7edc\u72b6\u6001\u53d1\u9001\u53d8\u5316"

    invoke-static {v4, v5}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    iget-object v4, p0, Lcom/konka/kkmultiscreen/WiFiDirectBroadcastReceiver;->manager:Landroid/net/wifi/p2p/WifiP2pManager;

    if-eqz v4, :cond_0

    const-string v4, "networkInfo"

    invoke-virtual {p2, v4}, Landroid/content/Intent;->getParcelableExtra(Ljava/lang/String;)Landroid/os/Parcelable;

    move-result-object v2

    check-cast v2, Landroid/net/NetworkInfo;

    invoke-virtual {v2}, Landroid/net/NetworkInfo;->isConnected()Z

    move-result v4

    if-eqz v4, :cond_5

    iget-object v4, p0, Lcom/konka/kkmultiscreen/WiFiDirectBroadcastReceiver;->manager:Landroid/net/wifi/p2p/WifiP2pManager;

    iget-object v5, p0, Lcom/konka/kkmultiscreen/WiFiDirectBroadcastReceiver;->channel:Landroid/net/wifi/p2p/WifiP2pManager$Channel;

    iget-object v6, p0, Lcom/konka/kkmultiscreen/WiFiDirectBroadcastReceiver;->mWiFiDirect:Lcom/konka/kkmultiscreen/WiFiDirectBroadcastReceiver$DeviceListFragment;

    invoke-virtual {v4, v5, v6}, Landroid/net/wifi/p2p/WifiP2pManager;->requestConnectionInfo(Landroid/net/wifi/p2p/WifiP2pManager$Channel;Landroid/net/wifi/p2p/WifiP2pManager$ConnectionInfoListener;)V

    const-string v4, "WiFiDirectBroadcastReceiver"

    const-string v5, "call requestConnectionInfo() ,callback on ConnectionInfoListener.onConnectionInfoAvailable()"

    invoke-static {v4, v5}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_1

    :cond_5
    const-string v4, "WiFiDirectBroadcastReceiver"

    const-string v5, "one wifi direct channel is disconnect!: "

    invoke-static {v4, v5}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    sget-object v4, Lcom/konka/kkmultiscreen/WiFiDirectBroadcastReceiver;->mApp:Lcom/konka/kkmultiscreen/KKMutiScreenTvApp;

    iget-object v4, v4, Lcom/konka/kkmultiscreen/KKMutiScreenTvApp;->sysWifiDirect:Lcom/konka/InitApp/CWifiDirect;

    invoke-virtual {v4}, Lcom/konka/InitApp/CWifiDirect;->startWifiDirectSearchTread()V

    const/4 v4, 0x0

    sput-object v4, Lcom/konka/kkmultiscreen/WiFiDirectBroadcastReceiver$DeviceListFragment;->useingWifi:Landroid/net/wifi/p2p/WifiP2pDevice;

    goto :goto_1

    :cond_6
    const-string v4, "android.net.wifi.p2p.THIS_DEVICE_CHANGED"

    invoke-virtual {v4, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v4

    if-eqz v4, :cond_0

    const-string v4, "WiFiDirectBroadcastReceiver"

    const-string v5, "WIFI_P2P_THIS_DEVICE_CHANGED_ACTION \u5f53\u524d\u8bbe\u5907\u7684\u914d\u7f6e\u7684\u7f51\u7edc\u4fe1\u606f\u53d1\u9001\u53d8\u5316"

    invoke-static {v4, v5}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    const-string v4, "wifiP2pDevice"

    invoke-virtual {p2, v4}, Landroid/content/Intent;->getParcelableExtra(Ljava/lang/String;)Landroid/os/Parcelable;

    move-result-object v1

    check-cast v1, Landroid/net/wifi/p2p/WifiP2pDevice;

    const-string v4, "WiFiDirectBroadcastReceiver"

    new-instance v5, Ljava/lang/StringBuilder;

    const-string v6, " mWifiP2pDevice.deviceName "

    invoke-direct {v5, v6}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    iget-object v6, v1, Landroid/net/wifi/p2p/WifiP2pDevice;->deviceName:Ljava/lang/String;

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    invoke-static {v4, v5}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    const-string v4, "WiFiDirectBroadcastReceiver"

    new-instance v5, Ljava/lang/StringBuilder;

    const-string v6, " mWifiP2pDevice.status: "

    invoke-direct {v5, v6}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    iget v6, v1, Landroid/net/wifi/p2p/WifiP2pDevice;->status:I

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v5

    const-string v6, " "

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    iget v6, v1, Landroid/net/wifi/p2p/WifiP2pDevice;->status:I

    invoke-static {v6}, Lcom/konka/kkmultiscreen/WiFiDirectBroadcastReceiver;->getDeviceStatus(I)Ljava/lang/String;

    move-result-object v6

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    invoke-static {v4, v5}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    const-string v4, "WiFiDirectBroadcastReceiver"

    new-instance v5, Ljava/lang/StringBuilder;

    const-string v6, " mWifiP2pDevice.deviceAddress "

    invoke-direct {v5, v6}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    iget-object v6, v1, Landroid/net/wifi/p2p/WifiP2pDevice;->deviceAddress:Ljava/lang/String;

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    invoke-static {v4, v5}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    goto/16 :goto_1
.end method
