.class public Lcom/konka/kkmultiscreen/MultiScreenWizardActity;
.super Landroid/app/Activity;
.source "MultiScreenWizardActity.java"


# instance fields
.field private clickListener:Landroid/view/View$OnClickListener;

.field wizardState:I

.field private wizard_bg:Landroid/widget/ImageButton;

.field private wizard_end:Landroid/widget/Button;

.field private wizard_next:Landroid/widget/Button;

.field private wizard_pre:Landroid/widget/Button;


# direct methods
.method public constructor <init>()V
    .locals 1

    invoke-direct {p0}, Landroid/app/Activity;-><init>()V

    const/4 v0, 0x1

    iput v0, p0, Lcom/konka/kkmultiscreen/MultiScreenWizardActity;->wizardState:I

    new-instance v0, Lcom/konka/kkmultiscreen/MultiScreenWizardActity$1;

    invoke-direct {v0, p0}, Lcom/konka/kkmultiscreen/MultiScreenWizardActity$1;-><init>(Lcom/konka/kkmultiscreen/MultiScreenWizardActity;)V

    iput-object v0, p0, Lcom/konka/kkmultiscreen/MultiScreenWizardActity;->clickListener:Landroid/view/View$OnClickListener;

    return-void
.end method

.method static synthetic access$0(Lcom/konka/kkmultiscreen/MultiScreenWizardActity;)Landroid/widget/Button;
    .locals 1

    iget-object v0, p0, Lcom/konka/kkmultiscreen/MultiScreenWizardActity;->wizard_next:Landroid/widget/Button;

    return-object v0
.end method

.method static synthetic access$1(Lcom/konka/kkmultiscreen/MultiScreenWizardActity;)Landroid/widget/Button;
    .locals 1

    iget-object v0, p0, Lcom/konka/kkmultiscreen/MultiScreenWizardActity;->wizard_end:Landroid/widget/Button;

    return-object v0
.end method

.method static synthetic access$2(Lcom/konka/kkmultiscreen/MultiScreenWizardActity;)Landroid/widget/Button;
    .locals 1

    iget-object v0, p0, Lcom/konka/kkmultiscreen/MultiScreenWizardActity;->wizard_pre:Landroid/widget/Button;

    return-object v0
.end method


# virtual methods
.method public initWizard()V
    .locals 2

    const/4 v1, 0x0

    const/high16 v0, 0x7f050000    # com.konka.kkmultiscreen.R.id.wizard_bg

    invoke-virtual {p0, v0}, Lcom/konka/kkmultiscreen/MultiScreenWizardActity;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/ImageButton;

    iput-object v0, p0, Lcom/konka/kkmultiscreen/MultiScreenWizardActity;->wizard_bg:Landroid/widget/ImageButton;

    const v0, 0x7f050004    # com.konka.kkmultiscreen.R.id.wizard_next

    invoke-virtual {p0, v0}, Lcom/konka/kkmultiscreen/MultiScreenWizardActity;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/Button;

    iput-object v0, p0, Lcom/konka/kkmultiscreen/MultiScreenWizardActity;->wizard_next:Landroid/widget/Button;

    const v0, 0x7f050005    # com.konka.kkmultiscreen.R.id.wizard_prev

    invoke-virtual {p0, v0}, Lcom/konka/kkmultiscreen/MultiScreenWizardActity;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/Button;

    iput-object v0, p0, Lcom/konka/kkmultiscreen/MultiScreenWizardActity;->wizard_pre:Landroid/widget/Button;

    const v0, 0x7f050006    # com.konka.kkmultiscreen.R.id.wizard_end

    invoke-virtual {p0, v0}, Lcom/konka/kkmultiscreen/MultiScreenWizardActity;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/Button;

    iput-object v0, p0, Lcom/konka/kkmultiscreen/MultiScreenWizardActity;->wizard_end:Landroid/widget/Button;

    iget-object v0, p0, Lcom/konka/kkmultiscreen/MultiScreenWizardActity;->wizard_pre:Landroid/widget/Button;

    invoke-virtual {v0, v1}, Landroid/widget/Button;->setVisibility(I)V

    iget-object v0, p0, Lcom/konka/kkmultiscreen/MultiScreenWizardActity;->wizard_next:Landroid/widget/Button;

    invoke-virtual {v0, v1}, Landroid/widget/Button;->setVisibility(I)V

    iget-object v0, p0, Lcom/konka/kkmultiscreen/MultiScreenWizardActity;->wizard_next:Landroid/widget/Button;

    const/4 v1, 0x1

    invoke-virtual {v0, v1}, Landroid/widget/Button;->setFocusable(Z)V

    iget-object v0, p0, Lcom/konka/kkmultiscreen/MultiScreenWizardActity;->wizard_next:Landroid/widget/Button;

    invoke-virtual {v0}, Landroid/widget/Button;->requestFocus()Z

    invoke-virtual {p0}, Lcom/konka/kkmultiscreen/MultiScreenWizardActity;->setButtonDrawableSource()V

    iget-object v0, p0, Lcom/konka/kkmultiscreen/MultiScreenWizardActity;->wizard_next:Landroid/widget/Button;

    iget-object v1, p0, Lcom/konka/kkmultiscreen/MultiScreenWizardActity;->clickListener:Landroid/view/View$OnClickListener;

    invoke-virtual {v0, v1}, Landroid/widget/Button;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    iget-object v0, p0, Lcom/konka/kkmultiscreen/MultiScreenWizardActity;->wizard_pre:Landroid/widget/Button;

    iget-object v1, p0, Lcom/konka/kkmultiscreen/MultiScreenWizardActity;->clickListener:Landroid/view/View$OnClickListener;

    invoke-virtual {v0, v1}, Landroid/widget/Button;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    iget-object v0, p0, Lcom/konka/kkmultiscreen/MultiScreenWizardActity;->wizard_end:Landroid/widget/Button;

    iget-object v1, p0, Lcom/konka/kkmultiscreen/MultiScreenWizardActity;->clickListener:Landroid/view/View$OnClickListener;

    invoke-virtual {v0, v1}, Landroid/widget/Button;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    return-void
.end method

.method protected onCreate(Landroid/os/Bundle;)V
    .locals 1
    .param p1    # Landroid/os/Bundle;

    invoke-super {p0, p1}, Landroid/app/Activity;->onCreate(Landroid/os/Bundle;)V

    const v0, 0x7f030001    # com.konka.kkmultiscreen.R.layout.wizard

    invoke-virtual {p0, v0}, Lcom/konka/kkmultiscreen/MultiScreenWizardActity;->setContentView(I)V

    invoke-virtual {p0}, Lcom/konka/kkmultiscreen/MultiScreenWizardActity;->initWizard()V

    return-void
.end method

.method protected onDestroy()V
    .locals 0

    invoke-super {p0}, Landroid/app/Activity;->onDestroy()V

    return-void
.end method

.method public onKeyDown(ILandroid/view/KeyEvent;)Z
    .locals 4
    .param p1    # I
    .param p2    # Landroid/view/KeyEvent;

    const/4 v3, 0x1

    const-string v0, "onkey down"

    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "onKeyDown: "

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    invoke-virtual {p2}, Landroid/view/KeyEvent;->getKeyCode()I

    move-result v0

    sparse-switch v0, :sswitch_data_0

    :goto_0
    :sswitch_0
    invoke-super {p0, p1, p2}, Landroid/app/Activity;->onKeyDown(ILandroid/view/KeyEvent;)Z

    move-result v0

    return v0

    :sswitch_1
    invoke-virtual {p0}, Lcom/konka/kkmultiscreen/MultiScreenWizardActity;->finish()V

    goto :goto_0

    :sswitch_2
    iget-object v0, p0, Lcom/konka/kkmultiscreen/MultiScreenWizardActity;->wizard_next:Landroid/widget/Button;

    invoke-virtual {v0, v3}, Landroid/widget/Button;->setFocusable(Z)V

    iget-object v0, p0, Lcom/konka/kkmultiscreen/MultiScreenWizardActity;->wizard_next:Landroid/widget/Button;

    invoke-virtual {v0}, Landroid/widget/Button;->requestFocus()Z

    invoke-virtual {p0}, Lcom/konka/kkmultiscreen/MultiScreenWizardActity;->setButtonDrawableSource()V

    goto :goto_0

    :sswitch_3
    iget-object v0, p0, Lcom/konka/kkmultiscreen/MultiScreenWizardActity;->wizard_pre:Landroid/widget/Button;

    invoke-virtual {v0, v3}, Landroid/widget/Button;->setFocusable(Z)V

    iget-object v0, p0, Lcom/konka/kkmultiscreen/MultiScreenWizardActity;->wizard_pre:Landroid/widget/Button;

    invoke-virtual {v0}, Landroid/widget/Button;->requestFocus()Z

    invoke-virtual {p0}, Lcom/konka/kkmultiscreen/MultiScreenWizardActity;->setButtonDrawableSource()V

    goto :goto_0

    :sswitch_4
    iget-object v0, p0, Lcom/konka/kkmultiscreen/MultiScreenWizardActity;->wizard_pre:Landroid/widget/Button;

    invoke-virtual {v0, v3}, Landroid/widget/Button;->setFocusable(Z)V

    iget-object v0, p0, Lcom/konka/kkmultiscreen/MultiScreenWizardActity;->wizard_pre:Landroid/widget/Button;

    invoke-virtual {v0}, Landroid/widget/Button;->requestFocus()Z

    invoke-virtual {p0}, Lcom/konka/kkmultiscreen/MultiScreenWizardActity;->setButtonDrawableSource()V

    goto :goto_0

    :sswitch_5
    iget-object v0, p0, Lcom/konka/kkmultiscreen/MultiScreenWizardActity;->wizard_end:Landroid/widget/Button;

    invoke-virtual {v0, v3}, Landroid/widget/Button;->setFocusable(Z)V

    iget-object v0, p0, Lcom/konka/kkmultiscreen/MultiScreenWizardActity;->wizard_end:Landroid/widget/Button;

    invoke-virtual {v0}, Landroid/widget/Button;->requestFocus()Z

    invoke-virtual {p0}, Lcom/konka/kkmultiscreen/MultiScreenWizardActity;->setButtonDrawableSource()V

    goto :goto_0

    nop

    :sswitch_data_0
    .sparse-switch
        0x4 -> :sswitch_1
        0x13 -> :sswitch_4
        0x14 -> :sswitch_5
        0x15 -> :sswitch_3
        0x16 -> :sswitch_2
        0x17 -> :sswitch_0
    .end sparse-switch
.end method

.method protected onResume()V
    .locals 2

    invoke-super {p0}, Landroid/app/Activity;->onResume()V

    iget v0, p0, Lcom/konka/kkmultiscreen/MultiScreenWizardActity;->wizardState:I

    const/4 v1, 0x1

    if-ne v0, v1, :cond_0

    iget-object v0, p0, Lcom/konka/kkmultiscreen/MultiScreenWizardActity;->wizard_end:Landroid/widget/Button;

    const/16 v1, 0x8

    invoke-virtual {v0, v1}, Landroid/widget/Button;->setVisibility(I)V

    :cond_0
    return-void
.end method

.method public setButtonDrawableSource()V
    .locals 4

    const v1, 0x7f02000b    # com.konka.kkmultiscreen.R.drawable.wizard_pre_nor_btn

    const v3, 0x7f020009    # com.konka.kkmultiscreen.R.drawable.wizard_next_nor_btn

    const v2, 0x7f020007    # com.konka.kkmultiscreen.R.drawable.wizard_end_nor_btn

    iget-object v0, p0, Lcom/konka/kkmultiscreen/MultiScreenWizardActity;->wizard_pre:Landroid/widget/Button;

    invoke-virtual {v0}, Landroid/widget/Button;->isFocused()Z

    move-result v0

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/konka/kkmultiscreen/MultiScreenWizardActity;->wizard_pre:Landroid/widget/Button;

    const v1, 0x7f02000c    # com.konka.kkmultiscreen.R.drawable.wizard_pre_sel_btn

    invoke-virtual {v0, v1}, Landroid/widget/Button;->setBackgroundResource(I)V

    iget-object v0, p0, Lcom/konka/kkmultiscreen/MultiScreenWizardActity;->wizard_next:Landroid/widget/Button;

    invoke-virtual {v0, v3}, Landroid/widget/Button;->setBackgroundResource(I)V

    iget-object v0, p0, Lcom/konka/kkmultiscreen/MultiScreenWizardActity;->wizard_end:Landroid/widget/Button;

    invoke-virtual {v0, v2}, Landroid/widget/Button;->setBackgroundResource(I)V

    :goto_0
    return-void

    :cond_0
    iget-object v0, p0, Lcom/konka/kkmultiscreen/MultiScreenWizardActity;->wizard_next:Landroid/widget/Button;

    invoke-virtual {v0}, Landroid/widget/Button;->isFocused()Z

    move-result v0

    if-eqz v0, :cond_1

    iget-object v0, p0, Lcom/konka/kkmultiscreen/MultiScreenWizardActity;->wizard_pre:Landroid/widget/Button;

    invoke-virtual {v0, v1}, Landroid/widget/Button;->setBackgroundResource(I)V

    iget-object v0, p0, Lcom/konka/kkmultiscreen/MultiScreenWizardActity;->wizard_next:Landroid/widget/Button;

    const v1, 0x7f02000a    # com.konka.kkmultiscreen.R.drawable.wizard_next_sel_btn

    invoke-virtual {v0, v1}, Landroid/widget/Button;->setBackgroundResource(I)V

    iget-object v0, p0, Lcom/konka/kkmultiscreen/MultiScreenWizardActity;->wizard_end:Landroid/widget/Button;

    invoke-virtual {v0, v2}, Landroid/widget/Button;->setBackgroundResource(I)V

    goto :goto_0

    :cond_1
    iget-object v0, p0, Lcom/konka/kkmultiscreen/MultiScreenWizardActity;->wizard_pre:Landroid/widget/Button;

    invoke-virtual {v0, v1}, Landroid/widget/Button;->setBackgroundResource(I)V

    iget-object v0, p0, Lcom/konka/kkmultiscreen/MultiScreenWizardActity;->wizard_next:Landroid/widget/Button;

    invoke-virtual {v0, v3}, Landroid/widget/Button;->setBackgroundResource(I)V

    iget-object v0, p0, Lcom/konka/kkmultiscreen/MultiScreenWizardActity;->wizard_end:Landroid/widget/Button;

    const v1, 0x7f020008    # com.konka.kkmultiscreen.R.drawable.wizard_end_sel_btn

    invoke-virtual {v0, v1}, Landroid/widget/Button;->setBackgroundResource(I)V

    goto :goto_0
.end method

.method public setWizardState(I)V
    .locals 4
    .param p1    # I

    const/4 v3, 0x1

    const/4 v2, 0x0

    if-ne p1, v3, :cond_1

    iget-object v0, p0, Lcom/konka/kkmultiscreen/MultiScreenWizardActity;->wizard_bg:Landroid/widget/ImageButton;

    const v1, 0x7f020005    # com.konka.kkmultiscreen.R.drawable.wizard_bg1

    invoke-virtual {v0, v1}, Landroid/widget/ImageButton;->setBackgroundResource(I)V

    iget-object v0, p0, Lcom/konka/kkmultiscreen/MultiScreenWizardActity;->wizard_pre:Landroid/widget/Button;

    invoke-virtual {v0, v2}, Landroid/widget/Button;->setVisibility(I)V

    iget-object v0, p0, Lcom/konka/kkmultiscreen/MultiScreenWizardActity;->wizard_next:Landroid/widget/Button;

    invoke-virtual {v0, v2}, Landroid/widget/Button;->setVisibility(I)V

    iget-object v0, p0, Lcom/konka/kkmultiscreen/MultiScreenWizardActity;->wizard_next:Landroid/widget/Button;

    invoke-virtual {v0, v3}, Landroid/widget/Button;->setFocusable(Z)V

    iget-object v0, p0, Lcom/konka/kkmultiscreen/MultiScreenWizardActity;->wizard_next:Landroid/widget/Button;

    invoke-virtual {v0}, Landroid/widget/Button;->requestFocus()Z

    :cond_0
    :goto_0
    return-void

    :cond_1
    const/4 v0, 0x2

    if-ne p1, v0, :cond_0

    iget-object v0, p0, Lcom/konka/kkmultiscreen/MultiScreenWizardActity;->wizard_bg:Landroid/widget/ImageButton;

    const v1, 0x7f020006    # com.konka.kkmultiscreen.R.drawable.wizard_bg2

    invoke-virtual {v0, v1}, Landroid/widget/ImageButton;->setBackgroundResource(I)V

    iget-object v0, p0, Lcom/konka/kkmultiscreen/MultiScreenWizardActity;->wizard_next:Landroid/widget/Button;

    const/16 v1, 0x8

    invoke-virtual {v0, v1}, Landroid/widget/Button;->setVisibility(I)V

    iget-object v0, p0, Lcom/konka/kkmultiscreen/MultiScreenWizardActity;->wizard_pre:Landroid/widget/Button;

    invoke-virtual {v0, v2}, Landroid/widget/Button;->setVisibility(I)V

    iget-object v0, p0, Lcom/konka/kkmultiscreen/MultiScreenWizardActity;->wizard_pre:Landroid/widget/Button;

    invoke-virtual {v0, v3}, Landroid/widget/Button;->setFocusable(Z)V

    iget-object v0, p0, Lcom/konka/kkmultiscreen/MultiScreenWizardActity;->wizard_pre:Landroid/widget/Button;

    invoke-virtual {v0}, Landroid/widget/Button;->requestFocus()Z

    goto :goto_0
.end method
