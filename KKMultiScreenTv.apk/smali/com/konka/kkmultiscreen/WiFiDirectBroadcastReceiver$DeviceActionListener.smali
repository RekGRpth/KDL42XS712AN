.class public interface abstract Lcom/konka/kkmultiscreen/WiFiDirectBroadcastReceiver$DeviceActionListener;
.super Ljava/lang/Object;
.source "WiFiDirectBroadcastReceiver.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/konka/kkmultiscreen/WiFiDirectBroadcastReceiver;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x609
    name = "DeviceActionListener"
.end annotation


# virtual methods
.method public abstract cancelDisconnect()V
.end method

.method public abstract connect(Landroid/net/wifi/p2p/WifiP2pConfig;)V
.end method

.method public abstract disconnect()V
.end method

.method public abstract showDetails(Landroid/net/wifi/p2p/WifiP2pDevice;)V
.end method
