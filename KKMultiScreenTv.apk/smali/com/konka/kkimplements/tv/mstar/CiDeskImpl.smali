.class public Lcom/konka/kkimplements/tv/mstar/CiDeskImpl;
.super Lcom/konka/kkimplements/tv/mstar/BaseDeskImpl;
.source "CiDeskImpl.java"

# interfaces
.implements Lcom/konka/kkinterface/tv/CiDesk;


# static fields
.field private static ciMgrImpl:Lcom/konka/kkimplements/tv/mstar/CiDeskImpl;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    const/4 v0, 0x0

    sput-object v0, Lcom/konka/kkimplements/tv/mstar/CiDeskImpl;->ciMgrImpl:Lcom/konka/kkimplements/tv/mstar/CiDeskImpl;

    return-void
.end method

.method private constructor <init>()V
    .locals 0

    invoke-direct {p0}, Lcom/konka/kkimplements/tv/mstar/BaseDeskImpl;-><init>()V

    return-void
.end method

.method public static getCiMgrInstance()Lcom/konka/kkimplements/tv/mstar/CiDeskImpl;
    .locals 1

    sget-object v0, Lcom/konka/kkimplements/tv/mstar/CiDeskImpl;->ciMgrImpl:Lcom/konka/kkimplements/tv/mstar/CiDeskImpl;

    if-nez v0, :cond_0

    new-instance v0, Lcom/konka/kkimplements/tv/mstar/CiDeskImpl;

    invoke-direct {v0}, Lcom/konka/kkimplements/tv/mstar/CiDeskImpl;-><init>()V

    sput-object v0, Lcom/konka/kkimplements/tv/mstar/CiDeskImpl;->ciMgrImpl:Lcom/konka/kkimplements/tv/mstar/CiDeskImpl;

    :cond_0
    sget-object v0, Lcom/konka/kkimplements/tv/mstar/CiDeskImpl;->ciMgrImpl:Lcom/konka/kkimplements/tv/mstar/CiDeskImpl;

    return-object v0
.end method


# virtual methods
.method public answerEnq(Ljava/lang/String;)Z
    .locals 1
    .param p1    # Ljava/lang/String;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/mstar/android/tvapi/common/exception/TvCommonException;
        }
    .end annotation

    invoke-static {}, Lcom/mstar/android/tvapi/dtv/common/DtvManager;->getCiManager()Lcom/mstar/android/tvapi/dtv/common/CiManager;

    move-result-object v0

    invoke-virtual {v0, p1}, Lcom/mstar/android/tvapi/dtv/common/CiManager;->answerEnq(Ljava/lang/String;)Z

    move-result v0

    return v0
.end method

.method public answerMenu(S)V
    .locals 1
    .param p1    # S
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/mstar/android/tvapi/common/exception/TvCommonException;
        }
    .end annotation

    invoke-static {}, Lcom/mstar/android/tvapi/dtv/common/DtvManager;->getCiManager()Lcom/mstar/android/tvapi/dtv/common/CiManager;

    move-result-object v0

    invoke-virtual {v0, p1}, Lcom/mstar/android/tvapi/dtv/common/CiManager;->answerMenu(S)V

    return-void
.end method

.method public backEnq()Z
    .locals 1
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/mstar/android/tvapi/common/exception/TvCommonException;
        }
    .end annotation

    invoke-static {}, Lcom/mstar/android/tvapi/dtv/common/DtvManager;->getCiManager()Lcom/mstar/android/tvapi/dtv/common/CiManager;

    move-result-object v0

    invoke-virtual {v0}, Lcom/mstar/android/tvapi/dtv/common/CiManager;->backEnq()Z

    move-result v0

    return v0
.end method

.method public backMenu()V
    .locals 1
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/mstar/android/tvapi/common/exception/TvCommonException;
        }
    .end annotation

    invoke-static {}, Lcom/mstar/android/tvapi/dtv/common/DtvManager;->getCiManager()Lcom/mstar/android/tvapi/dtv/common/CiManager;

    move-result-object v0

    invoke-virtual {v0}, Lcom/mstar/android/tvapi/dtv/common/CiManager;->backMenu()V

    return-void
.end method

.method public close()V
    .locals 1
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/mstar/android/tvapi/common/exception/TvCommonException;
        }
    .end annotation

    invoke-static {}, Lcom/mstar/android/tvapi/dtv/common/DtvManager;->getCiManager()Lcom/mstar/android/tvapi/dtv/common/CiManager;

    move-result-object v0

    invoke-virtual {v0}, Lcom/mstar/android/tvapi/dtv/common/CiManager;->close()V

    return-void
.end method

.method public enterMenu()V
    .locals 1
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/mstar/android/tvapi/common/exception/TvCommonException;
        }
    .end annotation

    invoke-static {}, Lcom/mstar/android/tvapi/dtv/common/DtvManager;->getCiManager()Lcom/mstar/android/tvapi/dtv/common/CiManager;

    move-result-object v0

    invoke-virtual {v0}, Lcom/mstar/android/tvapi/dtv/common/CiManager;->enterMenu()V

    return-void
.end method

.method public getCardState()Lcom/mstar/android/tvapi/dtv/vo/EnumCardState;
    .locals 1
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/mstar/android/tvapi/common/exception/TvCommonException;
        }
    .end annotation

    invoke-static {}, Lcom/mstar/android/tvapi/dtv/common/DtvManager;->getCiManager()Lcom/mstar/android/tvapi/dtv/common/CiManager;

    move-result-object v0

    invoke-virtual {v0}, Lcom/mstar/android/tvapi/dtv/common/CiManager;->getCardState()Lcom/mstar/android/tvapi/dtv/vo/EnumCardState;

    move-result-object v0

    return-object v0
.end method

.method public getCiCredentialValidRange()Lcom/mstar/android/tvapi/dtv/common/CiManager$CredentialValidDateRange;
    .locals 1
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/mstar/android/tvapi/common/exception/TvCommonException;
        }
    .end annotation

    invoke-static {}, Lcom/mstar/android/tvapi/dtv/common/DtvManager;->getCiManager()Lcom/mstar/android/tvapi/dtv/common/CiManager;

    move-result-object v0

    invoke-virtual {v0}, Lcom/mstar/android/tvapi/dtv/common/CiManager;->getCiCredentialValidRange()Lcom/mstar/android/tvapi/dtv/common/CiManager$CredentialValidDateRange;

    move-result-object v0

    return-object v0
.end method

.method public getEnqAnsLength()S
    .locals 1
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/mstar/android/tvapi/common/exception/TvCommonException;
        }
    .end annotation

    invoke-static {}, Lcom/mstar/android/tvapi/dtv/common/DtvManager;->getCiManager()Lcom/mstar/android/tvapi/dtv/common/CiManager;

    move-result-object v0

    invoke-virtual {v0}, Lcom/mstar/android/tvapi/dtv/common/CiManager;->getEnqAnsLength()S

    move-result v0

    return v0
.end method

.method public getEnqBlindAnswer()S
    .locals 1
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/mstar/android/tvapi/common/exception/TvCommonException;
        }
    .end annotation

    invoke-static {}, Lcom/mstar/android/tvapi/dtv/common/DtvManager;->getCiManager()Lcom/mstar/android/tvapi/dtv/common/CiManager;

    move-result-object v0

    invoke-virtual {v0}, Lcom/mstar/android/tvapi/dtv/common/CiManager;->getEnqBlindAnswer()S

    move-result v0

    return v0
.end method

.method public getEnqLength()S
    .locals 1
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/mstar/android/tvapi/common/exception/TvCommonException;
        }
    .end annotation

    invoke-static {}, Lcom/mstar/android/tvapi/dtv/common/DtvManager;->getCiManager()Lcom/mstar/android/tvapi/dtv/common/CiManager;

    move-result-object v0

    invoke-virtual {v0}, Lcom/mstar/android/tvapi/dtv/common/CiManager;->getEnqLength()S

    move-result v0

    return v0
.end method

.method public getEnqString()Ljava/lang/String;
    .locals 1
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/mstar/android/tvapi/common/exception/TvCommonException;
        }
    .end annotation

    invoke-static {}, Lcom/mstar/android/tvapi/dtv/common/DtvManager;->getCiManager()Lcom/mstar/android/tvapi/dtv/common/CiManager;

    move-result-object v0

    invoke-virtual {v0}, Lcom/mstar/android/tvapi/dtv/common/CiManager;->getEnqString()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public getListBottomLength()I
    .locals 1
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/mstar/android/tvapi/common/exception/TvCommonException;
        }
    .end annotation

    invoke-static {}, Lcom/mstar/android/tvapi/dtv/common/DtvManager;->getCiManager()Lcom/mstar/android/tvapi/dtv/common/CiManager;

    move-result-object v0

    invoke-virtual {v0}, Lcom/mstar/android/tvapi/dtv/common/CiManager;->getListBottomLength()I

    move-result v0

    return v0
.end method

.method public getListBottomString()Ljava/lang/String;
    .locals 1
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/mstar/android/tvapi/common/exception/TvCommonException;
        }
    .end annotation

    invoke-static {}, Lcom/mstar/android/tvapi/dtv/common/DtvManager;->getCiManager()Lcom/mstar/android/tvapi/dtv/common/CiManager;

    move-result-object v0

    invoke-virtual {v0}, Lcom/mstar/android/tvapi/dtv/common/CiManager;->getListBottomString()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public getListChoiceNumber()S
    .locals 1
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/mstar/android/tvapi/common/exception/TvCommonException;
        }
    .end annotation

    invoke-static {}, Lcom/mstar/android/tvapi/dtv/common/DtvManager;->getCiManager()Lcom/mstar/android/tvapi/dtv/common/CiManager;

    move-result-object v0

    invoke-virtual {v0}, Lcom/mstar/android/tvapi/dtv/common/CiManager;->getListChoiceNumber()S

    move-result v0

    return v0
.end method

.method public getListSelectionString(I)Ljava/lang/String;
    .locals 1
    .param p1    # I
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/mstar/android/tvapi/common/exception/TvCommonException;
        }
    .end annotation

    invoke-static {}, Lcom/mstar/android/tvapi/dtv/common/DtvManager;->getCiManager()Lcom/mstar/android/tvapi/dtv/common/CiManager;

    move-result-object v0

    invoke-virtual {v0, p1}, Lcom/mstar/android/tvapi/dtv/common/CiManager;->getListSelectionString(I)Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public getListSubtitleLength()I
    .locals 1
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/mstar/android/tvapi/common/exception/TvCommonException;
        }
    .end annotation

    const/4 v0, 0x1

    return v0
.end method

.method public getListSubtitleString()Ljava/lang/String;
    .locals 1
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/mstar/android/tvapi/common/exception/TvCommonException;
        }
    .end annotation

    invoke-static {}, Lcom/mstar/android/tvapi/dtv/common/DtvManager;->getCiManager()Lcom/mstar/android/tvapi/dtv/common/CiManager;

    move-result-object v0

    invoke-virtual {v0}, Lcom/mstar/android/tvapi/dtv/common/CiManager;->getListSubtitleString()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public getListTitleLength()I
    .locals 1
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/mstar/android/tvapi/common/exception/TvCommonException;
        }
    .end annotation

    invoke-static {}, Lcom/mstar/android/tvapi/dtv/common/DtvManager;->getCiManager()Lcom/mstar/android/tvapi/dtv/common/CiManager;

    move-result-object v0

    invoke-virtual {v0}, Lcom/mstar/android/tvapi/dtv/common/CiManager;->getListTitleLength()I

    move-result v0

    return v0
.end method

.method public getListTitleString()Ljava/lang/String;
    .locals 1
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/mstar/android/tvapi/common/exception/TvCommonException;
        }
    .end annotation

    invoke-static {}, Lcom/mstar/android/tvapi/dtv/common/DtvManager;->getCiManager()Lcom/mstar/android/tvapi/dtv/common/CiManager;

    move-result-object v0

    invoke-virtual {v0}, Lcom/mstar/android/tvapi/dtv/common/CiManager;->getListTitleString()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public getMenuBottomLength()I
    .locals 1
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/mstar/android/tvapi/common/exception/TvCommonException;
        }
    .end annotation

    invoke-static {}, Lcom/mstar/android/tvapi/dtv/common/DtvManager;->getCiManager()Lcom/mstar/android/tvapi/dtv/common/CiManager;

    move-result-object v0

    invoke-virtual {v0}, Lcom/mstar/android/tvapi/dtv/common/CiManager;->getMenuBottomLength()I

    move-result v0

    return v0
.end method

.method public getMenuBottomString()Ljava/lang/String;
    .locals 1
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/mstar/android/tvapi/common/exception/TvCommonException;
        }
    .end annotation

    invoke-static {}, Lcom/mstar/android/tvapi/dtv/common/DtvManager;->getCiManager()Lcom/mstar/android/tvapi/dtv/common/CiManager;

    move-result-object v0

    invoke-virtual {v0}, Lcom/mstar/android/tvapi/dtv/common/CiManager;->getMenuBottomString()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public getMenuChoiceNumber()S
    .locals 1
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/mstar/android/tvapi/common/exception/TvCommonException;
        }
    .end annotation

    invoke-static {}, Lcom/mstar/android/tvapi/dtv/common/DtvManager;->getCiManager()Lcom/mstar/android/tvapi/dtv/common/CiManager;

    move-result-object v0

    invoke-virtual {v0}, Lcom/mstar/android/tvapi/dtv/common/CiManager;->getMenuChoiceNumber()S

    move-result v0

    return v0
.end method

.method public getMenuSelectionString(I)Ljava/lang/String;
    .locals 1
    .param p1    # I
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/mstar/android/tvapi/common/exception/TvCommonException;
        }
    .end annotation

    invoke-static {}, Lcom/mstar/android/tvapi/dtv/common/DtvManager;->getCiManager()Lcom/mstar/android/tvapi/dtv/common/CiManager;

    move-result-object v0

    invoke-virtual {v0, p1}, Lcom/mstar/android/tvapi/dtv/common/CiManager;->getMenuSelectionString(I)Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public getMenuString()Ljava/lang/String;
    .locals 1
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/mstar/android/tvapi/common/exception/TvCommonException;
        }
    .end annotation

    invoke-static {}, Lcom/mstar/android/tvapi/dtv/common/DtvManager;->getCiManager()Lcom/mstar/android/tvapi/dtv/common/CiManager;

    move-result-object v0

    invoke-virtual {v0}, Lcom/mstar/android/tvapi/dtv/common/CiManager;->getMenuString()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public getMenuSubtitleLength()I
    .locals 1
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/mstar/android/tvapi/common/exception/TvCommonException;
        }
    .end annotation

    invoke-static {}, Lcom/mstar/android/tvapi/dtv/common/DtvManager;->getCiManager()Lcom/mstar/android/tvapi/dtv/common/CiManager;

    move-result-object v0

    invoke-virtual {v0}, Lcom/mstar/android/tvapi/dtv/common/CiManager;->getMenuSubtitleLength()I

    move-result v0

    return v0
.end method

.method public getMenuSubtitleString()Ljava/lang/String;
    .locals 1
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/mstar/android/tvapi/common/exception/TvCommonException;
        }
    .end annotation

    invoke-static {}, Lcom/mstar/android/tvapi/dtv/common/DtvManager;->getCiManager()Lcom/mstar/android/tvapi/dtv/common/CiManager;

    move-result-object v0

    invoke-virtual {v0}, Lcom/mstar/android/tvapi/dtv/common/CiManager;->getMenuSubtitleString()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public getMenuTitleLength()I
    .locals 1
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/mstar/android/tvapi/common/exception/TvCommonException;
        }
    .end annotation

    invoke-static {}, Lcom/mstar/android/tvapi/dtv/common/DtvManager;->getCiManager()Lcom/mstar/android/tvapi/dtv/common/CiManager;

    move-result-object v0

    invoke-virtual {v0}, Lcom/mstar/android/tvapi/dtv/common/CiManager;->getMenuTitleLength()I

    move-result v0

    return v0
.end method

.method public getMenuTitleString()Ljava/lang/String;
    .locals 1
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/mstar/android/tvapi/common/exception/TvCommonException;
        }
    .end annotation

    invoke-static {}, Lcom/mstar/android/tvapi/dtv/common/DtvManager;->getCiManager()Lcom/mstar/android/tvapi/dtv/common/CiManager;

    move-result-object v0

    invoke-virtual {v0}, Lcom/mstar/android/tvapi/dtv/common/CiManager;->getMenuTitleString()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public getMmiType()Lcom/mstar/android/tvapi/dtv/vo/EnumMmiType;
    .locals 1
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/mstar/android/tvapi/common/exception/TvCommonException;
        }
    .end annotation

    invoke-static {}, Lcom/mstar/android/tvapi/dtv/common/DtvManager;->getCiManager()Lcom/mstar/android/tvapi/dtv/common/CiManager;

    move-result-object v0

    invoke-virtual {v0}, Lcom/mstar/android/tvapi/dtv/common/CiManager;->getMmiType()Lcom/mstar/android/tvapi/dtv/vo/EnumMmiType;

    move-result-object v0

    return-object v0
.end method

.method public isCiCredentialModeValid(S)Z
    .locals 1
    .param p1    # S
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/mstar/android/tvapi/common/exception/TvCommonException;
        }
    .end annotation

    invoke-static {}, Lcom/mstar/android/tvapi/dtv/common/DtvManager;->getCiManager()Lcom/mstar/android/tvapi/dtv/common/CiManager;

    move-result-object v0

    invoke-virtual {v0, p1}, Lcom/mstar/android/tvapi/dtv/common/CiManager;->isCiCredentialModeValid(S)Z

    move-result v0

    return v0
.end method

.method public isCiMenuOn()Z
    .locals 1
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/mstar/android/tvapi/common/exception/TvCommonException;
        }
    .end annotation

    invoke-static {}, Lcom/mstar/android/tvapi/dtv/common/DtvManager;->getCiManager()Lcom/mstar/android/tvapi/dtv/common/CiManager;

    move-result-object v0

    invoke-virtual {v0}, Lcom/mstar/android/tvapi/dtv/common/CiManager;->isCiMenuOn()Z

    move-result v0

    return v0
.end method

.method public isDataExisted()Z
    .locals 1
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/mstar/android/tvapi/common/exception/TvCommonException;
        }
    .end annotation

    invoke-static {}, Lcom/mstar/android/tvapi/dtv/common/DtvManager;->getCiManager()Lcom/mstar/android/tvapi/dtv/common/CiManager;

    move-result-object v0

    invoke-virtual {v0}, Lcom/mstar/android/tvapi/dtv/common/CiManager;->isDataExisted()Z

    move-result v0

    return v0
.end method

.method public setCiCredentialMode(S)V
    .locals 1
    .param p1    # S
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/mstar/android/tvapi/common/exception/TvCommonException;
        }
    .end annotation

    invoke-static {}, Lcom/mstar/android/tvapi/dtv/common/DtvManager;->getCiManager()Lcom/mstar/android/tvapi/dtv/common/CiManager;

    move-result-object v0

    invoke-virtual {v0, p1}, Lcom/mstar/android/tvapi/dtv/common/CiManager;->setCiCredentialMode(S)V

    return-void
.end method

.method public setDebugMode(Z)V
    .locals 1
    .param p1    # Z
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/mstar/android/tvapi/common/exception/TvCommonException;
        }
    .end annotation

    invoke-static {}, Lcom/mstar/android/tvapi/dtv/common/DtvManager;->getCiManager()Lcom/mstar/android/tvapi/dtv/common/CiManager;

    move-result-object v0

    invoke-virtual {v0, p1}, Lcom/mstar/android/tvapi/dtv/common/CiManager;->setDebugMode(Z)V

    return-void
.end method

.method public setOnCiEventListener(Lcom/mstar/android/tvapi/dtv/common/CiManager$OnCiEventListener;)V
    .locals 2
    .param p1    # Lcom/mstar/android/tvapi/dtv/common/CiManager$OnCiEventListener;

    :try_start_0
    invoke-static {}, Lcom/mstar/android/tvapi/dtv/common/DtvManager;->getCiManager()Lcom/mstar/android/tvapi/dtv/common/CiManager;

    move-result-object v1

    invoke-virtual {v1, p1}, Lcom/mstar/android/tvapi/dtv/common/CiManager;->setOnCiEventListener(Lcom/mstar/android/tvapi/dtv/common/CiManager$OnCiEventListener;)V
    :try_end_0
    .catch Lcom/mstar/android/tvapi/common/exception/TvCommonException; {:try_start_0 .. :try_end_0} :catch_0

    :goto_0
    return-void

    :catch_0
    move-exception v0

    invoke-virtual {v0}, Lcom/mstar/android/tvapi/common/exception/TvCommonException;->printStackTrace()V

    goto :goto_0
.end method
