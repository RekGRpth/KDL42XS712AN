.class public final enum Lcom/konka/kkinterface/tv/DtvInterface$AUDIO_TYPE;
.super Ljava/lang/Enum;
.source "DtvInterface.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/konka/kkinterface/tv/DtvInterface;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x4019
    name = "AUDIO_TYPE"
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Enum",
        "<",
        "Lcom/konka/kkinterface/tv/DtvInterface$AUDIO_TYPE;",
        ">;"
    }
.end annotation


# static fields
.field private static final synthetic ENUM$VALUES:[Lcom/konka/kkinterface/tv/DtvInterface$AUDIO_TYPE;

.field public static final enum E_AUDIOTYPE_AAC:Lcom/konka/kkinterface/tv/DtvInterface$AUDIO_TYPE;

.field public static final enum E_AUDIOTYPE_AACP:Lcom/konka/kkinterface/tv/DtvInterface$AUDIO_TYPE;

.field public static final enum E_AUDIOTYPE_AAC_V2:Lcom/konka/kkinterface/tv/DtvInterface$AUDIO_TYPE;

.field public static final enum E_AUDIOTYPE_AAC_V4:Lcom/konka/kkinterface/tv/DtvInterface$AUDIO_TYPE;

.field public static final enum E_AUDIOTYPE_AC3:Lcom/konka/kkinterface/tv/DtvInterface$AUDIO_TYPE;

.field public static final enum E_AUDIOTYPE_AC3P:Lcom/konka/kkinterface/tv/DtvInterface$AUDIO_TYPE;

.field public static final enum E_AUDIOTYPE_HEAAC:Lcom/konka/kkinterface/tv/DtvInterface$AUDIO_TYPE;

.field public static final enum E_AUDIOTYPE_HEAAC_V2:Lcom/konka/kkinterface/tv/DtvInterface$AUDIO_TYPE;

.field public static final enum E_AUDIOTYPE_HEAAC_V4:Lcom/konka/kkinterface/tv/DtvInterface$AUDIO_TYPE;

.field public static final enum E_AUDIOTYPE_MPEG:Lcom/konka/kkinterface/tv/DtvInterface$AUDIO_TYPE;


# instance fields
.field private keyValue:I


# direct methods
.method static constructor <clinit>()V
    .locals 9

    const/4 v8, 0x4

    const/4 v7, 0x3

    const/4 v6, 0x2

    const/4 v5, 0x1

    const/4 v4, 0x0

    new-instance v0, Lcom/konka/kkinterface/tv/DtvInterface$AUDIO_TYPE;

    const-string v1, "E_AUDIOTYPE_MPEG"

    invoke-direct {v0, v1, v4, v4}, Lcom/konka/kkinterface/tv/DtvInterface$AUDIO_TYPE;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/konka/kkinterface/tv/DtvInterface$AUDIO_TYPE;->E_AUDIOTYPE_MPEG:Lcom/konka/kkinterface/tv/DtvInterface$AUDIO_TYPE;

    new-instance v0, Lcom/konka/kkinterface/tv/DtvInterface$AUDIO_TYPE;

    const-string v1, "E_AUDIOTYPE_AC3"

    invoke-direct {v0, v1, v5, v5}, Lcom/konka/kkinterface/tv/DtvInterface$AUDIO_TYPE;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/konka/kkinterface/tv/DtvInterface$AUDIO_TYPE;->E_AUDIOTYPE_AC3:Lcom/konka/kkinterface/tv/DtvInterface$AUDIO_TYPE;

    new-instance v0, Lcom/konka/kkinterface/tv/DtvInterface$AUDIO_TYPE;

    const-string v1, "E_AUDIOTYPE_AAC"

    invoke-direct {v0, v1, v6, v6}, Lcom/konka/kkinterface/tv/DtvInterface$AUDIO_TYPE;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/konka/kkinterface/tv/DtvInterface$AUDIO_TYPE;->E_AUDIOTYPE_AAC:Lcom/konka/kkinterface/tv/DtvInterface$AUDIO_TYPE;

    new-instance v0, Lcom/konka/kkinterface/tv/DtvInterface$AUDIO_TYPE;

    const-string v1, "E_AUDIOTYPE_AC3P"

    invoke-direct {v0, v1, v7, v7}, Lcom/konka/kkinterface/tv/DtvInterface$AUDIO_TYPE;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/konka/kkinterface/tv/DtvInterface$AUDIO_TYPE;->E_AUDIOTYPE_AC3P:Lcom/konka/kkinterface/tv/DtvInterface$AUDIO_TYPE;

    new-instance v0, Lcom/konka/kkinterface/tv/DtvInterface$AUDIO_TYPE;

    const-string v1, "E_AUDIOTYPE_AACP"

    invoke-direct {v0, v1, v8, v8}, Lcom/konka/kkinterface/tv/DtvInterface$AUDIO_TYPE;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/konka/kkinterface/tv/DtvInterface$AUDIO_TYPE;->E_AUDIOTYPE_AACP:Lcom/konka/kkinterface/tv/DtvInterface$AUDIO_TYPE;

    new-instance v0, Lcom/konka/kkinterface/tv/DtvInterface$AUDIO_TYPE;

    const-string v1, "E_AUDIOTYPE_HEAAC"

    const/4 v2, 0x5

    const/16 v3, 0x10

    invoke-direct {v0, v1, v2, v3}, Lcom/konka/kkinterface/tv/DtvInterface$AUDIO_TYPE;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/konka/kkinterface/tv/DtvInterface$AUDIO_TYPE;->E_AUDIOTYPE_HEAAC:Lcom/konka/kkinterface/tv/DtvInterface$AUDIO_TYPE;

    new-instance v0, Lcom/konka/kkinterface/tv/DtvInterface$AUDIO_TYPE;

    const-string v1, "E_AUDIOTYPE_AAC_V2"

    const/4 v2, 0x6

    const/16 v3, 0x11

    invoke-direct {v0, v1, v2, v3}, Lcom/konka/kkinterface/tv/DtvInterface$AUDIO_TYPE;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/konka/kkinterface/tv/DtvInterface$AUDIO_TYPE;->E_AUDIOTYPE_AAC_V2:Lcom/konka/kkinterface/tv/DtvInterface$AUDIO_TYPE;

    new-instance v0, Lcom/konka/kkinterface/tv/DtvInterface$AUDIO_TYPE;

    const-string v1, "E_AUDIOTYPE_AAC_V4"

    const/4 v2, 0x7

    const/16 v3, 0x12

    invoke-direct {v0, v1, v2, v3}, Lcom/konka/kkinterface/tv/DtvInterface$AUDIO_TYPE;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/konka/kkinterface/tv/DtvInterface$AUDIO_TYPE;->E_AUDIOTYPE_AAC_V4:Lcom/konka/kkinterface/tv/DtvInterface$AUDIO_TYPE;

    new-instance v0, Lcom/konka/kkinterface/tv/DtvInterface$AUDIO_TYPE;

    const-string v1, "E_AUDIOTYPE_HEAAC_V2"

    const/16 v2, 0x8

    const/16 v3, 0x13

    invoke-direct {v0, v1, v2, v3}, Lcom/konka/kkinterface/tv/DtvInterface$AUDIO_TYPE;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/konka/kkinterface/tv/DtvInterface$AUDIO_TYPE;->E_AUDIOTYPE_HEAAC_V2:Lcom/konka/kkinterface/tv/DtvInterface$AUDIO_TYPE;

    new-instance v0, Lcom/konka/kkinterface/tv/DtvInterface$AUDIO_TYPE;

    const-string v1, "E_AUDIOTYPE_HEAAC_V4"

    const/16 v2, 0x9

    const/16 v3, 0x14

    invoke-direct {v0, v1, v2, v3}, Lcom/konka/kkinterface/tv/DtvInterface$AUDIO_TYPE;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/konka/kkinterface/tv/DtvInterface$AUDIO_TYPE;->E_AUDIOTYPE_HEAAC_V4:Lcom/konka/kkinterface/tv/DtvInterface$AUDIO_TYPE;

    const/16 v0, 0xa

    new-array v0, v0, [Lcom/konka/kkinterface/tv/DtvInterface$AUDIO_TYPE;

    sget-object v1, Lcom/konka/kkinterface/tv/DtvInterface$AUDIO_TYPE;->E_AUDIOTYPE_MPEG:Lcom/konka/kkinterface/tv/DtvInterface$AUDIO_TYPE;

    aput-object v1, v0, v4

    sget-object v1, Lcom/konka/kkinterface/tv/DtvInterface$AUDIO_TYPE;->E_AUDIOTYPE_AC3:Lcom/konka/kkinterface/tv/DtvInterface$AUDIO_TYPE;

    aput-object v1, v0, v5

    sget-object v1, Lcom/konka/kkinterface/tv/DtvInterface$AUDIO_TYPE;->E_AUDIOTYPE_AAC:Lcom/konka/kkinterface/tv/DtvInterface$AUDIO_TYPE;

    aput-object v1, v0, v6

    sget-object v1, Lcom/konka/kkinterface/tv/DtvInterface$AUDIO_TYPE;->E_AUDIOTYPE_AC3P:Lcom/konka/kkinterface/tv/DtvInterface$AUDIO_TYPE;

    aput-object v1, v0, v7

    sget-object v1, Lcom/konka/kkinterface/tv/DtvInterface$AUDIO_TYPE;->E_AUDIOTYPE_AACP:Lcom/konka/kkinterface/tv/DtvInterface$AUDIO_TYPE;

    aput-object v1, v0, v8

    const/4 v1, 0x5

    sget-object v2, Lcom/konka/kkinterface/tv/DtvInterface$AUDIO_TYPE;->E_AUDIOTYPE_HEAAC:Lcom/konka/kkinterface/tv/DtvInterface$AUDIO_TYPE;

    aput-object v2, v0, v1

    const/4 v1, 0x6

    sget-object v2, Lcom/konka/kkinterface/tv/DtvInterface$AUDIO_TYPE;->E_AUDIOTYPE_AAC_V2:Lcom/konka/kkinterface/tv/DtvInterface$AUDIO_TYPE;

    aput-object v2, v0, v1

    const/4 v1, 0x7

    sget-object v2, Lcom/konka/kkinterface/tv/DtvInterface$AUDIO_TYPE;->E_AUDIOTYPE_AAC_V4:Lcom/konka/kkinterface/tv/DtvInterface$AUDIO_TYPE;

    aput-object v2, v0, v1

    const/16 v1, 0x8

    sget-object v2, Lcom/konka/kkinterface/tv/DtvInterface$AUDIO_TYPE;->E_AUDIOTYPE_HEAAC_V2:Lcom/konka/kkinterface/tv/DtvInterface$AUDIO_TYPE;

    aput-object v2, v0, v1

    const/16 v1, 0x9

    sget-object v2, Lcom/konka/kkinterface/tv/DtvInterface$AUDIO_TYPE;->E_AUDIOTYPE_HEAAC_V4:Lcom/konka/kkinterface/tv/DtvInterface$AUDIO_TYPE;

    aput-object v2, v0, v1

    sput-object v0, Lcom/konka/kkinterface/tv/DtvInterface$AUDIO_TYPE;->ENUM$VALUES:[Lcom/konka/kkinterface/tv/DtvInterface$AUDIO_TYPE;

    return-void
.end method

.method private constructor <init>(Ljava/lang/String;II)V
    .locals 0
    .param p3    # I

    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    iput p3, p0, Lcom/konka/kkinterface/tv/DtvInterface$AUDIO_TYPE;->keyValue:I

    return-void
.end method

.method public static valueOf(Ljava/lang/String;)Lcom/konka/kkinterface/tv/DtvInterface$AUDIO_TYPE;
    .locals 1

    const-class v0, Lcom/konka/kkinterface/tv/DtvInterface$AUDIO_TYPE;

    invoke-static {v0, p0}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;

    move-result-object v0

    check-cast v0, Lcom/konka/kkinterface/tv/DtvInterface$AUDIO_TYPE;

    return-object v0
.end method

.method public static values()[Lcom/konka/kkinterface/tv/DtvInterface$AUDIO_TYPE;
    .locals 4

    const/4 v3, 0x0

    sget-object v0, Lcom/konka/kkinterface/tv/DtvInterface$AUDIO_TYPE;->ENUM$VALUES:[Lcom/konka/kkinterface/tv/DtvInterface$AUDIO_TYPE;

    array-length v1, v0

    new-array v2, v1, [Lcom/konka/kkinterface/tv/DtvInterface$AUDIO_TYPE;

    invoke-static {v0, v3, v2, v3, v1}, Ljava/lang/System;->arraycopy(Ljava/lang/Object;ILjava/lang/Object;II)V

    return-object v2
.end method


# virtual methods
.method public key_value()I
    .locals 1

    iget v0, p0, Lcom/konka/kkinterface/tv/DtvInterface$AUDIO_TYPE;->keyValue:I

    return v0
.end method
