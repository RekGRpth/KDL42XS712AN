.class public interface abstract Lcom/konka/kkinterface/tv/CaDesk;
.super Ljava/lang/Object;
.source "CaDesk.java"

# interfaces
.implements Lcom/konka/kkinterface/tv/BaseDesk;


# virtual methods
.method public abstract CaChangePin(Ljava/lang/String;Ljava/lang/String;)S
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/mstar/android/tvapi/common/exception/TvCommonException;
        }
    .end annotation
.end method

.method public abstract CaDelDetitleChkNum(SI)Z
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/mstar/android/tvapi/common/exception/TvCommonException;
        }
    .end annotation
.end method

.method public abstract CaDelEmail(I)V
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/mstar/android/tvapi/common/exception/TvCommonException;
        }
    .end annotation
.end method

.method public abstract CaGetACList(S)Lcom/mstar/android/tvapi/dtv/vo/CaACListInfo;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/mstar/android/tvapi/common/exception/TvCommonException;
        }
    .end annotation
.end method

.method public abstract CaGetCardSN()Lcom/mstar/android/tvapi/dtv/vo/CACardSNInfo;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/mstar/android/tvapi/common/exception/TvCommonException;
        }
    .end annotation
.end method

.method public abstract CaGetDetitleChkNums(S)Lcom/mstar/android/tvapi/dtv/vo/CaDetitleChkNums;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/mstar/android/tvapi/common/exception/TvCommonException;
        }
    .end annotation
.end method

.method public abstract CaGetDetitleReaded(S)Z
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/mstar/android/tvapi/common/exception/TvCommonException;
        }
    .end annotation
.end method

.method public abstract CaGetEmailContent(I)Lcom/mstar/android/tvapi/dtv/vo/CaEmailContentInfo;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/mstar/android/tvapi/common/exception/TvCommonException;
        }
    .end annotation
.end method

.method public abstract CaGetEmailHead(I)Lcom/mstar/android/tvapi/dtv/vo/CaEmailHeadInfo;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/mstar/android/tvapi/common/exception/TvCommonException;
        }
    .end annotation
.end method

.method public abstract CaGetEmailHeads(SS)Lcom/mstar/android/tvapi/dtv/vo/CaEmailHeadsInfo;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/mstar/android/tvapi/common/exception/TvCommonException;
        }
    .end annotation
.end method

.method public abstract CaGetEmailSpaceInfo()Lcom/mstar/android/tvapi/dtv/vo/CaEmailSpaceInfo;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/mstar/android/tvapi/common/exception/TvCommonException;
        }
    .end annotation
.end method

.method public abstract CaGetEntitleIDs(S)Lcom/mstar/android/tvapi/dtv/vo/CaEntitleIDs;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/mstar/android/tvapi/common/exception/TvCommonException;
        }
    .end annotation
.end method

.method public abstract CaGetIPPVProgram(S)Lcom/mstar/android/tvapi/dtv/vo/CaIPPVProgramInfos;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/mstar/android/tvapi/common/exception/TvCommonException;
        }
    .end annotation
.end method

.method public abstract CaGetOperatorChildStatus(S)Lcom/mstar/android/tvapi/dtv/vo/CaOperatorChildStatus;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/mstar/android/tvapi/common/exception/TvCommonException;
        }
    .end annotation
.end method

.method public abstract CaGetOperatorIds()Lcom/mstar/android/tvapi/dtv/vo/CaOperatorIds;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/mstar/android/tvapi/common/exception/TvCommonException;
        }
    .end annotation
.end method

.method public abstract CaGetOperatorInfo(S)Lcom/mstar/android/tvapi/dtv/vo/CaOperatorInfo;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/mstar/android/tvapi/common/exception/TvCommonException;
        }
    .end annotation
.end method

.method public abstract CaGetPlatformID()S
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/mstar/android/tvapi/common/exception/TvCommonException;
        }
    .end annotation
.end method

.method public abstract CaGetRating()Lcom/mstar/android/tvapi/dtv/vo/CARatingInfo;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/mstar/android/tvapi/common/exception/TvCommonException;
        }
    .end annotation
.end method

.method public abstract CaGetServiceEntitles(S)Lcom/mstar/android/tvapi/dtv/vo/CaServiceEntitles;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/mstar/android/tvapi/common/exception/TvCommonException;
        }
    .end annotation
.end method

.method public abstract CaGetSlotIDs(S)Lcom/mstar/android/tvapi/dtv/vo/CaSlotIDs;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/mstar/android/tvapi/common/exception/TvCommonException;
        }
    .end annotation
.end method

.method public abstract CaGetSlotInfo(SS)Lcom/mstar/android/tvapi/dtv/vo/CaSlotInfo;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/mstar/android/tvapi/common/exception/TvCommonException;
        }
    .end annotation
.end method

.method public abstract CaGetVer()I
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/mstar/android/tvapi/common/exception/TvCommonException;
        }
    .end annotation
.end method

.method public abstract CaGetWorkTime()Lcom/mstar/android/tvapi/dtv/vo/CaWorkTimeInfo;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/mstar/android/tvapi/common/exception/TvCommonException;
        }
    .end annotation
.end method

.method public abstract CaIsPaired(SLjava/lang/String;)S
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/mstar/android/tvapi/common/exception/TvCommonException;
        }
    .end annotation
.end method

.method public abstract CaOTAStateConfirm(II)Z
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/mstar/android/tvapi/common/exception/TvCommonException;
        }
    .end annotation
.end method

.method public abstract CaReadFeedDataFromParent(S)Lcom/mstar/android/tvapi/dtv/vo/CaFeedDataInfo;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/mstar/android/tvapi/common/exception/TvCommonException;
        }
    .end annotation
.end method

.method public abstract CaRefreshInterface()V
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/mstar/android/tvapi/common/exception/TvCommonException;
        }
    .end annotation
.end method

.method public abstract CaSetRating(Ljava/lang/String;S)S
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/mstar/android/tvapi/common/exception/TvCommonException;
        }
    .end annotation
.end method

.method public abstract CaSetWorkTime(Ljava/lang/String;Lcom/mstar/android/tvapi/dtv/vo/CaWorkTimeInfo;)S
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/mstar/android/tvapi/common/exception/TvCommonException;
        }
    .end annotation
.end method

.method public abstract CaStopIPPVBuyDlg(Lcom/mstar/android/tvapi/dtv/vo/CaStopIPPVBuyDlgInfo;)S
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/mstar/android/tvapi/common/exception/TvCommonException;
        }
    .end annotation
.end method

.method public abstract CaWriteFeedDataToChild(SLcom/mstar/android/tvapi/dtv/vo/CaFeedDataInfo;)S
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/mstar/android/tvapi/common/exception/TvCommonException;
        }
    .end annotation
.end method

.method public abstract getCurrentEvent()I
.end method

.method public abstract getCurrentMsgType()I
.end method

.method public abstract setCurrentEvent(I)V
.end method

.method public abstract setCurrentMsgType(I)V
.end method

.method public abstract setOnCaEventListener(Lcom/mstar/android/tvapi/dtv/common/CaManager$OnCaEventListener;)V
.end method
