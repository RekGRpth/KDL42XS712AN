.class public final enum Lcom/mstar/tv/service/aidl/EN_ThreeD_Video_3DTO2D;
.super Ljava/lang/Enum;
.source "EN_ThreeD_Video_3DTO2D.java"

# interfaces
.implements Landroid/os/Parcelable;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Enum",
        "<",
        "Lcom/mstar/tv/service/aidl/EN_ThreeD_Video_3DTO2D;",
        ">;",
        "Landroid/os/Parcelable;"
    }
.end annotation


# static fields
.field public static final CREATOR:Landroid/os/Parcelable$Creator;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Landroid/os/Parcelable$Creator",
            "<",
            "Lcom/mstar/tv/service/aidl/EN_ThreeD_Video_3DTO2D;",
            ">;"
        }
    .end annotation
.end field

.field public static final enum DB_ThreeD_Video_3DTO2D_AUTO:Lcom/mstar/tv/service/aidl/EN_ThreeD_Video_3DTO2D;

.field public static final enum DB_ThreeD_Video_3DTO2D_COUNT:Lcom/mstar/tv/service/aidl/EN_ThreeD_Video_3DTO2D;

.field public static final enum DB_ThreeD_Video_3DTO2D_FRAME_PACKING:Lcom/mstar/tv/service/aidl/EN_ThreeD_Video_3DTO2D;

.field public static final enum DB_ThreeD_Video_3DTO2D_LINE_ALTERNATIVE:Lcom/mstar/tv/service/aidl/EN_ThreeD_Video_3DTO2D;

.field public static final enum DB_ThreeD_Video_3DTO2D_NONE:Lcom/mstar/tv/service/aidl/EN_ThreeD_Video_3DTO2D;

.field public static final enum DB_ThreeD_Video_3DTO2D_SIDE_BY_SIDE:Lcom/mstar/tv/service/aidl/EN_ThreeD_Video_3DTO2D;

.field public static final enum DB_ThreeD_Video_3DTO2D_TOP_BOTTOM:Lcom/mstar/tv/service/aidl/EN_ThreeD_Video_3DTO2D;

.field private static final synthetic ENUM$VALUES:[Lcom/mstar/tv/service/aidl/EN_ThreeD_Video_3DTO2D;


# direct methods
.method static constructor <clinit>()V
    .locals 8

    const/4 v7, 0x4

    const/4 v6, 0x3

    const/4 v5, 0x2

    const/4 v4, 0x1

    const/4 v3, 0x0

    new-instance v0, Lcom/mstar/tv/service/aidl/EN_ThreeD_Video_3DTO2D;

    const-string v1, "DB_ThreeD_Video_3DTO2D_NONE"

    invoke-direct {v0, v1, v3}, Lcom/mstar/tv/service/aidl/EN_ThreeD_Video_3DTO2D;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/mstar/tv/service/aidl/EN_ThreeD_Video_3DTO2D;->DB_ThreeD_Video_3DTO2D_NONE:Lcom/mstar/tv/service/aidl/EN_ThreeD_Video_3DTO2D;

    new-instance v0, Lcom/mstar/tv/service/aidl/EN_ThreeD_Video_3DTO2D;

    const-string v1, "DB_ThreeD_Video_3DTO2D_SIDE_BY_SIDE"

    invoke-direct {v0, v1, v4}, Lcom/mstar/tv/service/aidl/EN_ThreeD_Video_3DTO2D;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/mstar/tv/service/aidl/EN_ThreeD_Video_3DTO2D;->DB_ThreeD_Video_3DTO2D_SIDE_BY_SIDE:Lcom/mstar/tv/service/aidl/EN_ThreeD_Video_3DTO2D;

    new-instance v0, Lcom/mstar/tv/service/aidl/EN_ThreeD_Video_3DTO2D;

    const-string v1, "DB_ThreeD_Video_3DTO2D_TOP_BOTTOM"

    invoke-direct {v0, v1, v5}, Lcom/mstar/tv/service/aidl/EN_ThreeD_Video_3DTO2D;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/mstar/tv/service/aidl/EN_ThreeD_Video_3DTO2D;->DB_ThreeD_Video_3DTO2D_TOP_BOTTOM:Lcom/mstar/tv/service/aidl/EN_ThreeD_Video_3DTO2D;

    new-instance v0, Lcom/mstar/tv/service/aidl/EN_ThreeD_Video_3DTO2D;

    const-string v1, "DB_ThreeD_Video_3DTO2D_FRAME_PACKING"

    invoke-direct {v0, v1, v6}, Lcom/mstar/tv/service/aidl/EN_ThreeD_Video_3DTO2D;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/mstar/tv/service/aidl/EN_ThreeD_Video_3DTO2D;->DB_ThreeD_Video_3DTO2D_FRAME_PACKING:Lcom/mstar/tv/service/aidl/EN_ThreeD_Video_3DTO2D;

    new-instance v0, Lcom/mstar/tv/service/aidl/EN_ThreeD_Video_3DTO2D;

    const-string v1, "DB_ThreeD_Video_3DTO2D_LINE_ALTERNATIVE"

    invoke-direct {v0, v1, v7}, Lcom/mstar/tv/service/aidl/EN_ThreeD_Video_3DTO2D;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/mstar/tv/service/aidl/EN_ThreeD_Video_3DTO2D;->DB_ThreeD_Video_3DTO2D_LINE_ALTERNATIVE:Lcom/mstar/tv/service/aidl/EN_ThreeD_Video_3DTO2D;

    new-instance v0, Lcom/mstar/tv/service/aidl/EN_ThreeD_Video_3DTO2D;

    const-string v1, "DB_ThreeD_Video_3DTO2D_AUTO"

    const/4 v2, 0x5

    invoke-direct {v0, v1, v2}, Lcom/mstar/tv/service/aidl/EN_ThreeD_Video_3DTO2D;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/mstar/tv/service/aidl/EN_ThreeD_Video_3DTO2D;->DB_ThreeD_Video_3DTO2D_AUTO:Lcom/mstar/tv/service/aidl/EN_ThreeD_Video_3DTO2D;

    new-instance v0, Lcom/mstar/tv/service/aidl/EN_ThreeD_Video_3DTO2D;

    const-string v1, "DB_ThreeD_Video_3DTO2D_COUNT"

    const/4 v2, 0x6

    invoke-direct {v0, v1, v2}, Lcom/mstar/tv/service/aidl/EN_ThreeD_Video_3DTO2D;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/mstar/tv/service/aidl/EN_ThreeD_Video_3DTO2D;->DB_ThreeD_Video_3DTO2D_COUNT:Lcom/mstar/tv/service/aidl/EN_ThreeD_Video_3DTO2D;

    const/4 v0, 0x7

    new-array v0, v0, [Lcom/mstar/tv/service/aidl/EN_ThreeD_Video_3DTO2D;

    sget-object v1, Lcom/mstar/tv/service/aidl/EN_ThreeD_Video_3DTO2D;->DB_ThreeD_Video_3DTO2D_NONE:Lcom/mstar/tv/service/aidl/EN_ThreeD_Video_3DTO2D;

    aput-object v1, v0, v3

    sget-object v1, Lcom/mstar/tv/service/aidl/EN_ThreeD_Video_3DTO2D;->DB_ThreeD_Video_3DTO2D_SIDE_BY_SIDE:Lcom/mstar/tv/service/aidl/EN_ThreeD_Video_3DTO2D;

    aput-object v1, v0, v4

    sget-object v1, Lcom/mstar/tv/service/aidl/EN_ThreeD_Video_3DTO2D;->DB_ThreeD_Video_3DTO2D_TOP_BOTTOM:Lcom/mstar/tv/service/aidl/EN_ThreeD_Video_3DTO2D;

    aput-object v1, v0, v5

    sget-object v1, Lcom/mstar/tv/service/aidl/EN_ThreeD_Video_3DTO2D;->DB_ThreeD_Video_3DTO2D_FRAME_PACKING:Lcom/mstar/tv/service/aidl/EN_ThreeD_Video_3DTO2D;

    aput-object v1, v0, v6

    sget-object v1, Lcom/mstar/tv/service/aidl/EN_ThreeD_Video_3DTO2D;->DB_ThreeD_Video_3DTO2D_LINE_ALTERNATIVE:Lcom/mstar/tv/service/aidl/EN_ThreeD_Video_3DTO2D;

    aput-object v1, v0, v7

    const/4 v1, 0x5

    sget-object v2, Lcom/mstar/tv/service/aidl/EN_ThreeD_Video_3DTO2D;->DB_ThreeD_Video_3DTO2D_AUTO:Lcom/mstar/tv/service/aidl/EN_ThreeD_Video_3DTO2D;

    aput-object v2, v0, v1

    const/4 v1, 0x6

    sget-object v2, Lcom/mstar/tv/service/aidl/EN_ThreeD_Video_3DTO2D;->DB_ThreeD_Video_3DTO2D_COUNT:Lcom/mstar/tv/service/aidl/EN_ThreeD_Video_3DTO2D;

    aput-object v2, v0, v1

    sput-object v0, Lcom/mstar/tv/service/aidl/EN_ThreeD_Video_3DTO2D;->ENUM$VALUES:[Lcom/mstar/tv/service/aidl/EN_ThreeD_Video_3DTO2D;

    new-instance v0, Lcom/mstar/tv/service/aidl/EN_ThreeD_Video_3DTO2D$1;

    invoke-direct {v0}, Lcom/mstar/tv/service/aidl/EN_ThreeD_Video_3DTO2D$1;-><init>()V

    sput-object v0, Lcom/mstar/tv/service/aidl/EN_ThreeD_Video_3DTO2D;->CREATOR:Landroid/os/Parcelable$Creator;

    return-void
.end method

.method private constructor <init>(Ljava/lang/String;I)V
    .locals 0

    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    return-void
.end method

.method public static valueOf(Ljava/lang/String;)Lcom/mstar/tv/service/aidl/EN_ThreeD_Video_3DTO2D;
    .locals 1

    const-class v0, Lcom/mstar/tv/service/aidl/EN_ThreeD_Video_3DTO2D;

    invoke-static {v0, p0}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;

    move-result-object v0

    check-cast v0, Lcom/mstar/tv/service/aidl/EN_ThreeD_Video_3DTO2D;

    return-object v0
.end method

.method public static values()[Lcom/mstar/tv/service/aidl/EN_ThreeD_Video_3DTO2D;
    .locals 4

    const/4 v3, 0x0

    sget-object v0, Lcom/mstar/tv/service/aidl/EN_ThreeD_Video_3DTO2D;->ENUM$VALUES:[Lcom/mstar/tv/service/aidl/EN_ThreeD_Video_3DTO2D;

    array-length v1, v0

    new-array v2, v1, [Lcom/mstar/tv/service/aidl/EN_ThreeD_Video_3DTO2D;

    invoke-static {v0, v3, v2, v3, v1}, Ljava/lang/System;->arraycopy(Ljava/lang/Object;ILjava/lang/Object;II)V

    return-object v2
.end method


# virtual methods
.method public describeContents()I
    .locals 1

    const/4 v0, 0x0

    return v0
.end method

.method public writeToParcel(Landroid/os/Parcel;I)V
    .locals 1
    .param p1    # Landroid/os/Parcel;
    .param p2    # I

    invoke-virtual {p0}, Lcom/mstar/tv/service/aidl/EN_ThreeD_Video_3DTO2D;->ordinal()I

    move-result v0

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeInt(I)V

    return-void
.end method
