.class public final enum Lcom/mstar/tv/service/aidl/EN_INPUT_SOURCE_TYPE;
.super Ljava/lang/Enum;
.source "EN_INPUT_SOURCE_TYPE.java"

# interfaces
.implements Landroid/os/Parcelable;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Enum",
        "<",
        "Lcom/mstar/tv/service/aidl/EN_INPUT_SOURCE_TYPE;",
        ">;",
        "Landroid/os/Parcelable;"
    }
.end annotation


# static fields
.field public static final CREATOR:Landroid/os/Parcelable$Creator;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Landroid/os/Parcelable$Creator",
            "<",
            "Lcom/mstar/tv/service/aidl/EN_INPUT_SOURCE_TYPE;",
            ">;"
        }
    .end annotation
.end field

.field private static final synthetic ENUM$VALUES:[Lcom/mstar/tv/service/aidl/EN_INPUT_SOURCE_TYPE;

.field public static final enum E_INPUT_SOURCE_ATV:Lcom/mstar/tv/service/aidl/EN_INPUT_SOURCE_TYPE;

.field public static final enum E_INPUT_SOURCE_CVBS:Lcom/mstar/tv/service/aidl/EN_INPUT_SOURCE_TYPE;

.field public static final enum E_INPUT_SOURCE_CVBS2:Lcom/mstar/tv/service/aidl/EN_INPUT_SOURCE_TYPE;

.field public static final enum E_INPUT_SOURCE_CVBS3:Lcom/mstar/tv/service/aidl/EN_INPUT_SOURCE_TYPE;

.field public static final enum E_INPUT_SOURCE_CVBS4:Lcom/mstar/tv/service/aidl/EN_INPUT_SOURCE_TYPE;

.field public static final enum E_INPUT_SOURCE_CVBS5:Lcom/mstar/tv/service/aidl/EN_INPUT_SOURCE_TYPE;

.field public static final enum E_INPUT_SOURCE_CVBS6:Lcom/mstar/tv/service/aidl/EN_INPUT_SOURCE_TYPE;

.field public static final enum E_INPUT_SOURCE_CVBS7:Lcom/mstar/tv/service/aidl/EN_INPUT_SOURCE_TYPE;

.field public static final enum E_INPUT_SOURCE_CVBS8:Lcom/mstar/tv/service/aidl/EN_INPUT_SOURCE_TYPE;

.field public static final enum E_INPUT_SOURCE_CVBS_MAX:Lcom/mstar/tv/service/aidl/EN_INPUT_SOURCE_TYPE;

.field public static final enum E_INPUT_SOURCE_DTV:Lcom/mstar/tv/service/aidl/EN_INPUT_SOURCE_TYPE;

.field public static final enum E_INPUT_SOURCE_DTV2:Lcom/mstar/tv/service/aidl/EN_INPUT_SOURCE_TYPE;

.field public static final enum E_INPUT_SOURCE_DVI:Lcom/mstar/tv/service/aidl/EN_INPUT_SOURCE_TYPE;

.field public static final enum E_INPUT_SOURCE_DVI2:Lcom/mstar/tv/service/aidl/EN_INPUT_SOURCE_TYPE;

.field public static final enum E_INPUT_SOURCE_DVI3:Lcom/mstar/tv/service/aidl/EN_INPUT_SOURCE_TYPE;

.field public static final enum E_INPUT_SOURCE_DVI4:Lcom/mstar/tv/service/aidl/EN_INPUT_SOURCE_TYPE;

.field public static final enum E_INPUT_SOURCE_DVI_MAX:Lcom/mstar/tv/service/aidl/EN_INPUT_SOURCE_TYPE;

.field public static final enum E_INPUT_SOURCE_HDMI:Lcom/mstar/tv/service/aidl/EN_INPUT_SOURCE_TYPE;

.field public static final enum E_INPUT_SOURCE_HDMI2:Lcom/mstar/tv/service/aidl/EN_INPUT_SOURCE_TYPE;

.field public static final enum E_INPUT_SOURCE_HDMI3:Lcom/mstar/tv/service/aidl/EN_INPUT_SOURCE_TYPE;

.field public static final enum E_INPUT_SOURCE_HDMI4:Lcom/mstar/tv/service/aidl/EN_INPUT_SOURCE_TYPE;

.field public static final enum E_INPUT_SOURCE_HDMI_MAX:Lcom/mstar/tv/service/aidl/EN_INPUT_SOURCE_TYPE;

.field public static final enum E_INPUT_SOURCE_JPEG:Lcom/mstar/tv/service/aidl/EN_INPUT_SOURCE_TYPE;

.field public static final enum E_INPUT_SOURCE_KTV:Lcom/mstar/tv/service/aidl/EN_INPUT_SOURCE_TYPE;

.field public static final enum E_INPUT_SOURCE_NONE:Lcom/mstar/tv/service/aidl/EN_INPUT_SOURCE_TYPE;

.field public static final enum E_INPUT_SOURCE_NUM:Lcom/mstar/tv/service/aidl/EN_INPUT_SOURCE_TYPE;

.field public static final enum E_INPUT_SOURCE_SCART:Lcom/mstar/tv/service/aidl/EN_INPUT_SOURCE_TYPE;

.field public static final enum E_INPUT_SOURCE_SCART2:Lcom/mstar/tv/service/aidl/EN_INPUT_SOURCE_TYPE;

.field public static final enum E_INPUT_SOURCE_SCART_MAX:Lcom/mstar/tv/service/aidl/EN_INPUT_SOURCE_TYPE;

.field public static final enum E_INPUT_SOURCE_STORAGE:Lcom/mstar/tv/service/aidl/EN_INPUT_SOURCE_TYPE;

.field public static final enum E_INPUT_SOURCE_STORAGE2:Lcom/mstar/tv/service/aidl/EN_INPUT_SOURCE_TYPE;

.field public static final enum E_INPUT_SOURCE_SVIDEO:Lcom/mstar/tv/service/aidl/EN_INPUT_SOURCE_TYPE;

.field public static final enum E_INPUT_SOURCE_SVIDEO2:Lcom/mstar/tv/service/aidl/EN_INPUT_SOURCE_TYPE;

.field public static final enum E_INPUT_SOURCE_SVIDEO3:Lcom/mstar/tv/service/aidl/EN_INPUT_SOURCE_TYPE;

.field public static final enum E_INPUT_SOURCE_SVIDEO4:Lcom/mstar/tv/service/aidl/EN_INPUT_SOURCE_TYPE;

.field public static final enum E_INPUT_SOURCE_SVIDEO_MAX:Lcom/mstar/tv/service/aidl/EN_INPUT_SOURCE_TYPE;

.field public static final enum E_INPUT_SOURCE_VGA:Lcom/mstar/tv/service/aidl/EN_INPUT_SOURCE_TYPE;

.field public static final enum E_INPUT_SOURCE_YPBPR:Lcom/mstar/tv/service/aidl/EN_INPUT_SOURCE_TYPE;

.field public static final enum E_INPUT_SOURCE_YPBPR2:Lcom/mstar/tv/service/aidl/EN_INPUT_SOURCE_TYPE;

.field public static final enum E_INPUT_SOURCE_YPBPR3:Lcom/mstar/tv/service/aidl/EN_INPUT_SOURCE_TYPE;

.field public static final enum E_INPUT_SOURCE_YPBPR_MAX:Lcom/mstar/tv/service/aidl/EN_INPUT_SOURCE_TYPE;


# direct methods
.method static constructor <clinit>()V
    .locals 8

    const/4 v7, 0x4

    const/4 v6, 0x3

    const/4 v5, 0x2

    const/4 v4, 0x1

    const/4 v3, 0x0

    new-instance v0, Lcom/mstar/tv/service/aidl/EN_INPUT_SOURCE_TYPE;

    const-string v1, "E_INPUT_SOURCE_VGA"

    invoke-direct {v0, v1, v3}, Lcom/mstar/tv/service/aidl/EN_INPUT_SOURCE_TYPE;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/mstar/tv/service/aidl/EN_INPUT_SOURCE_TYPE;->E_INPUT_SOURCE_VGA:Lcom/mstar/tv/service/aidl/EN_INPUT_SOURCE_TYPE;

    new-instance v0, Lcom/mstar/tv/service/aidl/EN_INPUT_SOURCE_TYPE;

    const-string v1, "E_INPUT_SOURCE_ATV"

    invoke-direct {v0, v1, v4}, Lcom/mstar/tv/service/aidl/EN_INPUT_SOURCE_TYPE;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/mstar/tv/service/aidl/EN_INPUT_SOURCE_TYPE;->E_INPUT_SOURCE_ATV:Lcom/mstar/tv/service/aidl/EN_INPUT_SOURCE_TYPE;

    new-instance v0, Lcom/mstar/tv/service/aidl/EN_INPUT_SOURCE_TYPE;

    const-string v1, "E_INPUT_SOURCE_CVBS"

    invoke-direct {v0, v1, v5}, Lcom/mstar/tv/service/aidl/EN_INPUT_SOURCE_TYPE;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/mstar/tv/service/aidl/EN_INPUT_SOURCE_TYPE;->E_INPUT_SOURCE_CVBS:Lcom/mstar/tv/service/aidl/EN_INPUT_SOURCE_TYPE;

    new-instance v0, Lcom/mstar/tv/service/aidl/EN_INPUT_SOURCE_TYPE;

    const-string v1, "E_INPUT_SOURCE_CVBS2"

    invoke-direct {v0, v1, v6}, Lcom/mstar/tv/service/aidl/EN_INPUT_SOURCE_TYPE;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/mstar/tv/service/aidl/EN_INPUT_SOURCE_TYPE;->E_INPUT_SOURCE_CVBS2:Lcom/mstar/tv/service/aidl/EN_INPUT_SOURCE_TYPE;

    new-instance v0, Lcom/mstar/tv/service/aidl/EN_INPUT_SOURCE_TYPE;

    const-string v1, "E_INPUT_SOURCE_CVBS3"

    invoke-direct {v0, v1, v7}, Lcom/mstar/tv/service/aidl/EN_INPUT_SOURCE_TYPE;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/mstar/tv/service/aidl/EN_INPUT_SOURCE_TYPE;->E_INPUT_SOURCE_CVBS3:Lcom/mstar/tv/service/aidl/EN_INPUT_SOURCE_TYPE;

    new-instance v0, Lcom/mstar/tv/service/aidl/EN_INPUT_SOURCE_TYPE;

    const-string v1, "E_INPUT_SOURCE_CVBS4"

    const/4 v2, 0x5

    invoke-direct {v0, v1, v2}, Lcom/mstar/tv/service/aidl/EN_INPUT_SOURCE_TYPE;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/mstar/tv/service/aidl/EN_INPUT_SOURCE_TYPE;->E_INPUT_SOURCE_CVBS4:Lcom/mstar/tv/service/aidl/EN_INPUT_SOURCE_TYPE;

    new-instance v0, Lcom/mstar/tv/service/aidl/EN_INPUT_SOURCE_TYPE;

    const-string v1, "E_INPUT_SOURCE_CVBS5"

    const/4 v2, 0x6

    invoke-direct {v0, v1, v2}, Lcom/mstar/tv/service/aidl/EN_INPUT_SOURCE_TYPE;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/mstar/tv/service/aidl/EN_INPUT_SOURCE_TYPE;->E_INPUT_SOURCE_CVBS5:Lcom/mstar/tv/service/aidl/EN_INPUT_SOURCE_TYPE;

    new-instance v0, Lcom/mstar/tv/service/aidl/EN_INPUT_SOURCE_TYPE;

    const-string v1, "E_INPUT_SOURCE_CVBS6"

    const/4 v2, 0x7

    invoke-direct {v0, v1, v2}, Lcom/mstar/tv/service/aidl/EN_INPUT_SOURCE_TYPE;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/mstar/tv/service/aidl/EN_INPUT_SOURCE_TYPE;->E_INPUT_SOURCE_CVBS6:Lcom/mstar/tv/service/aidl/EN_INPUT_SOURCE_TYPE;

    new-instance v0, Lcom/mstar/tv/service/aidl/EN_INPUT_SOURCE_TYPE;

    const-string v1, "E_INPUT_SOURCE_CVBS7"

    const/16 v2, 0x8

    invoke-direct {v0, v1, v2}, Lcom/mstar/tv/service/aidl/EN_INPUT_SOURCE_TYPE;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/mstar/tv/service/aidl/EN_INPUT_SOURCE_TYPE;->E_INPUT_SOURCE_CVBS7:Lcom/mstar/tv/service/aidl/EN_INPUT_SOURCE_TYPE;

    new-instance v0, Lcom/mstar/tv/service/aidl/EN_INPUT_SOURCE_TYPE;

    const-string v1, "E_INPUT_SOURCE_CVBS8"

    const/16 v2, 0x9

    invoke-direct {v0, v1, v2}, Lcom/mstar/tv/service/aidl/EN_INPUT_SOURCE_TYPE;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/mstar/tv/service/aidl/EN_INPUT_SOURCE_TYPE;->E_INPUT_SOURCE_CVBS8:Lcom/mstar/tv/service/aidl/EN_INPUT_SOURCE_TYPE;

    new-instance v0, Lcom/mstar/tv/service/aidl/EN_INPUT_SOURCE_TYPE;

    const-string v1, "E_INPUT_SOURCE_CVBS_MAX"

    const/16 v2, 0xa

    invoke-direct {v0, v1, v2}, Lcom/mstar/tv/service/aidl/EN_INPUT_SOURCE_TYPE;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/mstar/tv/service/aidl/EN_INPUT_SOURCE_TYPE;->E_INPUT_SOURCE_CVBS_MAX:Lcom/mstar/tv/service/aidl/EN_INPUT_SOURCE_TYPE;

    new-instance v0, Lcom/mstar/tv/service/aidl/EN_INPUT_SOURCE_TYPE;

    const-string v1, "E_INPUT_SOURCE_SVIDEO"

    const/16 v2, 0xb

    invoke-direct {v0, v1, v2}, Lcom/mstar/tv/service/aidl/EN_INPUT_SOURCE_TYPE;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/mstar/tv/service/aidl/EN_INPUT_SOURCE_TYPE;->E_INPUT_SOURCE_SVIDEO:Lcom/mstar/tv/service/aidl/EN_INPUT_SOURCE_TYPE;

    new-instance v0, Lcom/mstar/tv/service/aidl/EN_INPUT_SOURCE_TYPE;

    const-string v1, "E_INPUT_SOURCE_SVIDEO2"

    const/16 v2, 0xc

    invoke-direct {v0, v1, v2}, Lcom/mstar/tv/service/aidl/EN_INPUT_SOURCE_TYPE;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/mstar/tv/service/aidl/EN_INPUT_SOURCE_TYPE;->E_INPUT_SOURCE_SVIDEO2:Lcom/mstar/tv/service/aidl/EN_INPUT_SOURCE_TYPE;

    new-instance v0, Lcom/mstar/tv/service/aidl/EN_INPUT_SOURCE_TYPE;

    const-string v1, "E_INPUT_SOURCE_SVIDEO3"

    const/16 v2, 0xd

    invoke-direct {v0, v1, v2}, Lcom/mstar/tv/service/aidl/EN_INPUT_SOURCE_TYPE;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/mstar/tv/service/aidl/EN_INPUT_SOURCE_TYPE;->E_INPUT_SOURCE_SVIDEO3:Lcom/mstar/tv/service/aidl/EN_INPUT_SOURCE_TYPE;

    new-instance v0, Lcom/mstar/tv/service/aidl/EN_INPUT_SOURCE_TYPE;

    const-string v1, "E_INPUT_SOURCE_SVIDEO4"

    const/16 v2, 0xe

    invoke-direct {v0, v1, v2}, Lcom/mstar/tv/service/aidl/EN_INPUT_SOURCE_TYPE;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/mstar/tv/service/aidl/EN_INPUT_SOURCE_TYPE;->E_INPUT_SOURCE_SVIDEO4:Lcom/mstar/tv/service/aidl/EN_INPUT_SOURCE_TYPE;

    new-instance v0, Lcom/mstar/tv/service/aidl/EN_INPUT_SOURCE_TYPE;

    const-string v1, "E_INPUT_SOURCE_SVIDEO_MAX"

    const/16 v2, 0xf

    invoke-direct {v0, v1, v2}, Lcom/mstar/tv/service/aidl/EN_INPUT_SOURCE_TYPE;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/mstar/tv/service/aidl/EN_INPUT_SOURCE_TYPE;->E_INPUT_SOURCE_SVIDEO_MAX:Lcom/mstar/tv/service/aidl/EN_INPUT_SOURCE_TYPE;

    new-instance v0, Lcom/mstar/tv/service/aidl/EN_INPUT_SOURCE_TYPE;

    const-string v1, "E_INPUT_SOURCE_YPBPR"

    const/16 v2, 0x10

    invoke-direct {v0, v1, v2}, Lcom/mstar/tv/service/aidl/EN_INPUT_SOURCE_TYPE;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/mstar/tv/service/aidl/EN_INPUT_SOURCE_TYPE;->E_INPUT_SOURCE_YPBPR:Lcom/mstar/tv/service/aidl/EN_INPUT_SOURCE_TYPE;

    new-instance v0, Lcom/mstar/tv/service/aidl/EN_INPUT_SOURCE_TYPE;

    const-string v1, "E_INPUT_SOURCE_YPBPR2"

    const/16 v2, 0x11

    invoke-direct {v0, v1, v2}, Lcom/mstar/tv/service/aidl/EN_INPUT_SOURCE_TYPE;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/mstar/tv/service/aidl/EN_INPUT_SOURCE_TYPE;->E_INPUT_SOURCE_YPBPR2:Lcom/mstar/tv/service/aidl/EN_INPUT_SOURCE_TYPE;

    new-instance v0, Lcom/mstar/tv/service/aidl/EN_INPUT_SOURCE_TYPE;

    const-string v1, "E_INPUT_SOURCE_YPBPR3"

    const/16 v2, 0x12

    invoke-direct {v0, v1, v2}, Lcom/mstar/tv/service/aidl/EN_INPUT_SOURCE_TYPE;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/mstar/tv/service/aidl/EN_INPUT_SOURCE_TYPE;->E_INPUT_SOURCE_YPBPR3:Lcom/mstar/tv/service/aidl/EN_INPUT_SOURCE_TYPE;

    new-instance v0, Lcom/mstar/tv/service/aidl/EN_INPUT_SOURCE_TYPE;

    const-string v1, "E_INPUT_SOURCE_YPBPR_MAX"

    const/16 v2, 0x13

    invoke-direct {v0, v1, v2}, Lcom/mstar/tv/service/aidl/EN_INPUT_SOURCE_TYPE;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/mstar/tv/service/aidl/EN_INPUT_SOURCE_TYPE;->E_INPUT_SOURCE_YPBPR_MAX:Lcom/mstar/tv/service/aidl/EN_INPUT_SOURCE_TYPE;

    new-instance v0, Lcom/mstar/tv/service/aidl/EN_INPUT_SOURCE_TYPE;

    const-string v1, "E_INPUT_SOURCE_SCART"

    const/16 v2, 0x14

    invoke-direct {v0, v1, v2}, Lcom/mstar/tv/service/aidl/EN_INPUT_SOURCE_TYPE;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/mstar/tv/service/aidl/EN_INPUT_SOURCE_TYPE;->E_INPUT_SOURCE_SCART:Lcom/mstar/tv/service/aidl/EN_INPUT_SOURCE_TYPE;

    new-instance v0, Lcom/mstar/tv/service/aidl/EN_INPUT_SOURCE_TYPE;

    const-string v1, "E_INPUT_SOURCE_SCART2"

    const/16 v2, 0x15

    invoke-direct {v0, v1, v2}, Lcom/mstar/tv/service/aidl/EN_INPUT_SOURCE_TYPE;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/mstar/tv/service/aidl/EN_INPUT_SOURCE_TYPE;->E_INPUT_SOURCE_SCART2:Lcom/mstar/tv/service/aidl/EN_INPUT_SOURCE_TYPE;

    new-instance v0, Lcom/mstar/tv/service/aidl/EN_INPUT_SOURCE_TYPE;

    const-string v1, "E_INPUT_SOURCE_SCART_MAX"

    const/16 v2, 0x16

    invoke-direct {v0, v1, v2}, Lcom/mstar/tv/service/aidl/EN_INPUT_SOURCE_TYPE;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/mstar/tv/service/aidl/EN_INPUT_SOURCE_TYPE;->E_INPUT_SOURCE_SCART_MAX:Lcom/mstar/tv/service/aidl/EN_INPUT_SOURCE_TYPE;

    new-instance v0, Lcom/mstar/tv/service/aidl/EN_INPUT_SOURCE_TYPE;

    const-string v1, "E_INPUT_SOURCE_HDMI"

    const/16 v2, 0x17

    invoke-direct {v0, v1, v2}, Lcom/mstar/tv/service/aidl/EN_INPUT_SOURCE_TYPE;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/mstar/tv/service/aidl/EN_INPUT_SOURCE_TYPE;->E_INPUT_SOURCE_HDMI:Lcom/mstar/tv/service/aidl/EN_INPUT_SOURCE_TYPE;

    new-instance v0, Lcom/mstar/tv/service/aidl/EN_INPUT_SOURCE_TYPE;

    const-string v1, "E_INPUT_SOURCE_HDMI2"

    const/16 v2, 0x18

    invoke-direct {v0, v1, v2}, Lcom/mstar/tv/service/aidl/EN_INPUT_SOURCE_TYPE;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/mstar/tv/service/aidl/EN_INPUT_SOURCE_TYPE;->E_INPUT_SOURCE_HDMI2:Lcom/mstar/tv/service/aidl/EN_INPUT_SOURCE_TYPE;

    new-instance v0, Lcom/mstar/tv/service/aidl/EN_INPUT_SOURCE_TYPE;

    const-string v1, "E_INPUT_SOURCE_HDMI3"

    const/16 v2, 0x19

    invoke-direct {v0, v1, v2}, Lcom/mstar/tv/service/aidl/EN_INPUT_SOURCE_TYPE;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/mstar/tv/service/aidl/EN_INPUT_SOURCE_TYPE;->E_INPUT_SOURCE_HDMI3:Lcom/mstar/tv/service/aidl/EN_INPUT_SOURCE_TYPE;

    new-instance v0, Lcom/mstar/tv/service/aidl/EN_INPUT_SOURCE_TYPE;

    const-string v1, "E_INPUT_SOURCE_HDMI4"

    const/16 v2, 0x1a

    invoke-direct {v0, v1, v2}, Lcom/mstar/tv/service/aidl/EN_INPUT_SOURCE_TYPE;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/mstar/tv/service/aidl/EN_INPUT_SOURCE_TYPE;->E_INPUT_SOURCE_HDMI4:Lcom/mstar/tv/service/aidl/EN_INPUT_SOURCE_TYPE;

    new-instance v0, Lcom/mstar/tv/service/aidl/EN_INPUT_SOURCE_TYPE;

    const-string v1, "E_INPUT_SOURCE_HDMI_MAX"

    const/16 v2, 0x1b

    invoke-direct {v0, v1, v2}, Lcom/mstar/tv/service/aidl/EN_INPUT_SOURCE_TYPE;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/mstar/tv/service/aidl/EN_INPUT_SOURCE_TYPE;->E_INPUT_SOURCE_HDMI_MAX:Lcom/mstar/tv/service/aidl/EN_INPUT_SOURCE_TYPE;

    new-instance v0, Lcom/mstar/tv/service/aidl/EN_INPUT_SOURCE_TYPE;

    const-string v1, "E_INPUT_SOURCE_DTV"

    const/16 v2, 0x1c

    invoke-direct {v0, v1, v2}, Lcom/mstar/tv/service/aidl/EN_INPUT_SOURCE_TYPE;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/mstar/tv/service/aidl/EN_INPUT_SOURCE_TYPE;->E_INPUT_SOURCE_DTV:Lcom/mstar/tv/service/aidl/EN_INPUT_SOURCE_TYPE;

    new-instance v0, Lcom/mstar/tv/service/aidl/EN_INPUT_SOURCE_TYPE;

    const-string v1, "E_INPUT_SOURCE_DVI"

    const/16 v2, 0x1d

    invoke-direct {v0, v1, v2}, Lcom/mstar/tv/service/aidl/EN_INPUT_SOURCE_TYPE;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/mstar/tv/service/aidl/EN_INPUT_SOURCE_TYPE;->E_INPUT_SOURCE_DVI:Lcom/mstar/tv/service/aidl/EN_INPUT_SOURCE_TYPE;

    new-instance v0, Lcom/mstar/tv/service/aidl/EN_INPUT_SOURCE_TYPE;

    const-string v1, "E_INPUT_SOURCE_DVI2"

    const/16 v2, 0x1e

    invoke-direct {v0, v1, v2}, Lcom/mstar/tv/service/aidl/EN_INPUT_SOURCE_TYPE;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/mstar/tv/service/aidl/EN_INPUT_SOURCE_TYPE;->E_INPUT_SOURCE_DVI2:Lcom/mstar/tv/service/aidl/EN_INPUT_SOURCE_TYPE;

    new-instance v0, Lcom/mstar/tv/service/aidl/EN_INPUT_SOURCE_TYPE;

    const-string v1, "E_INPUT_SOURCE_DVI3"

    const/16 v2, 0x1f

    invoke-direct {v0, v1, v2}, Lcom/mstar/tv/service/aidl/EN_INPUT_SOURCE_TYPE;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/mstar/tv/service/aidl/EN_INPUT_SOURCE_TYPE;->E_INPUT_SOURCE_DVI3:Lcom/mstar/tv/service/aidl/EN_INPUT_SOURCE_TYPE;

    new-instance v0, Lcom/mstar/tv/service/aidl/EN_INPUT_SOURCE_TYPE;

    const-string v1, "E_INPUT_SOURCE_DVI4"

    const/16 v2, 0x20

    invoke-direct {v0, v1, v2}, Lcom/mstar/tv/service/aidl/EN_INPUT_SOURCE_TYPE;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/mstar/tv/service/aidl/EN_INPUT_SOURCE_TYPE;->E_INPUT_SOURCE_DVI4:Lcom/mstar/tv/service/aidl/EN_INPUT_SOURCE_TYPE;

    new-instance v0, Lcom/mstar/tv/service/aidl/EN_INPUT_SOURCE_TYPE;

    const-string v1, "E_INPUT_SOURCE_DVI_MAX"

    const/16 v2, 0x21

    invoke-direct {v0, v1, v2}, Lcom/mstar/tv/service/aidl/EN_INPUT_SOURCE_TYPE;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/mstar/tv/service/aidl/EN_INPUT_SOURCE_TYPE;->E_INPUT_SOURCE_DVI_MAX:Lcom/mstar/tv/service/aidl/EN_INPUT_SOURCE_TYPE;

    new-instance v0, Lcom/mstar/tv/service/aidl/EN_INPUT_SOURCE_TYPE;

    const-string v1, "E_INPUT_SOURCE_STORAGE"

    const/16 v2, 0x22

    invoke-direct {v0, v1, v2}, Lcom/mstar/tv/service/aidl/EN_INPUT_SOURCE_TYPE;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/mstar/tv/service/aidl/EN_INPUT_SOURCE_TYPE;->E_INPUT_SOURCE_STORAGE:Lcom/mstar/tv/service/aidl/EN_INPUT_SOURCE_TYPE;

    new-instance v0, Lcom/mstar/tv/service/aidl/EN_INPUT_SOURCE_TYPE;

    const-string v1, "E_INPUT_SOURCE_KTV"

    const/16 v2, 0x23

    invoke-direct {v0, v1, v2}, Lcom/mstar/tv/service/aidl/EN_INPUT_SOURCE_TYPE;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/mstar/tv/service/aidl/EN_INPUT_SOURCE_TYPE;->E_INPUT_SOURCE_KTV:Lcom/mstar/tv/service/aidl/EN_INPUT_SOURCE_TYPE;

    new-instance v0, Lcom/mstar/tv/service/aidl/EN_INPUT_SOURCE_TYPE;

    const-string v1, "E_INPUT_SOURCE_JPEG"

    const/16 v2, 0x24

    invoke-direct {v0, v1, v2}, Lcom/mstar/tv/service/aidl/EN_INPUT_SOURCE_TYPE;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/mstar/tv/service/aidl/EN_INPUT_SOURCE_TYPE;->E_INPUT_SOURCE_JPEG:Lcom/mstar/tv/service/aidl/EN_INPUT_SOURCE_TYPE;

    new-instance v0, Lcom/mstar/tv/service/aidl/EN_INPUT_SOURCE_TYPE;

    const-string v1, "E_INPUT_SOURCE_DTV2"

    const/16 v2, 0x25

    invoke-direct {v0, v1, v2}, Lcom/mstar/tv/service/aidl/EN_INPUT_SOURCE_TYPE;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/mstar/tv/service/aidl/EN_INPUT_SOURCE_TYPE;->E_INPUT_SOURCE_DTV2:Lcom/mstar/tv/service/aidl/EN_INPUT_SOURCE_TYPE;

    new-instance v0, Lcom/mstar/tv/service/aidl/EN_INPUT_SOURCE_TYPE;

    const-string v1, "E_INPUT_SOURCE_STORAGE2"

    const/16 v2, 0x26

    invoke-direct {v0, v1, v2}, Lcom/mstar/tv/service/aidl/EN_INPUT_SOURCE_TYPE;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/mstar/tv/service/aidl/EN_INPUT_SOURCE_TYPE;->E_INPUT_SOURCE_STORAGE2:Lcom/mstar/tv/service/aidl/EN_INPUT_SOURCE_TYPE;

    new-instance v0, Lcom/mstar/tv/service/aidl/EN_INPUT_SOURCE_TYPE;

    const-string v1, "E_INPUT_SOURCE_NUM"

    const/16 v2, 0x27

    invoke-direct {v0, v1, v2}, Lcom/mstar/tv/service/aidl/EN_INPUT_SOURCE_TYPE;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/mstar/tv/service/aidl/EN_INPUT_SOURCE_TYPE;->E_INPUT_SOURCE_NUM:Lcom/mstar/tv/service/aidl/EN_INPUT_SOURCE_TYPE;

    new-instance v0, Lcom/mstar/tv/service/aidl/EN_INPUT_SOURCE_TYPE;

    const-string v1, "E_INPUT_SOURCE_NONE"

    const/16 v2, 0x28

    invoke-direct {v0, v1, v2}, Lcom/mstar/tv/service/aidl/EN_INPUT_SOURCE_TYPE;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/mstar/tv/service/aidl/EN_INPUT_SOURCE_TYPE;->E_INPUT_SOURCE_NONE:Lcom/mstar/tv/service/aidl/EN_INPUT_SOURCE_TYPE;

    const/16 v0, 0x29

    new-array v0, v0, [Lcom/mstar/tv/service/aidl/EN_INPUT_SOURCE_TYPE;

    sget-object v1, Lcom/mstar/tv/service/aidl/EN_INPUT_SOURCE_TYPE;->E_INPUT_SOURCE_VGA:Lcom/mstar/tv/service/aidl/EN_INPUT_SOURCE_TYPE;

    aput-object v1, v0, v3

    sget-object v1, Lcom/mstar/tv/service/aidl/EN_INPUT_SOURCE_TYPE;->E_INPUT_SOURCE_ATV:Lcom/mstar/tv/service/aidl/EN_INPUT_SOURCE_TYPE;

    aput-object v1, v0, v4

    sget-object v1, Lcom/mstar/tv/service/aidl/EN_INPUT_SOURCE_TYPE;->E_INPUT_SOURCE_CVBS:Lcom/mstar/tv/service/aidl/EN_INPUT_SOURCE_TYPE;

    aput-object v1, v0, v5

    sget-object v1, Lcom/mstar/tv/service/aidl/EN_INPUT_SOURCE_TYPE;->E_INPUT_SOURCE_CVBS2:Lcom/mstar/tv/service/aidl/EN_INPUT_SOURCE_TYPE;

    aput-object v1, v0, v6

    sget-object v1, Lcom/mstar/tv/service/aidl/EN_INPUT_SOURCE_TYPE;->E_INPUT_SOURCE_CVBS3:Lcom/mstar/tv/service/aidl/EN_INPUT_SOURCE_TYPE;

    aput-object v1, v0, v7

    const/4 v1, 0x5

    sget-object v2, Lcom/mstar/tv/service/aidl/EN_INPUT_SOURCE_TYPE;->E_INPUT_SOURCE_CVBS4:Lcom/mstar/tv/service/aidl/EN_INPUT_SOURCE_TYPE;

    aput-object v2, v0, v1

    const/4 v1, 0x6

    sget-object v2, Lcom/mstar/tv/service/aidl/EN_INPUT_SOURCE_TYPE;->E_INPUT_SOURCE_CVBS5:Lcom/mstar/tv/service/aidl/EN_INPUT_SOURCE_TYPE;

    aput-object v2, v0, v1

    const/4 v1, 0x7

    sget-object v2, Lcom/mstar/tv/service/aidl/EN_INPUT_SOURCE_TYPE;->E_INPUT_SOURCE_CVBS6:Lcom/mstar/tv/service/aidl/EN_INPUT_SOURCE_TYPE;

    aput-object v2, v0, v1

    const/16 v1, 0x8

    sget-object v2, Lcom/mstar/tv/service/aidl/EN_INPUT_SOURCE_TYPE;->E_INPUT_SOURCE_CVBS7:Lcom/mstar/tv/service/aidl/EN_INPUT_SOURCE_TYPE;

    aput-object v2, v0, v1

    const/16 v1, 0x9

    sget-object v2, Lcom/mstar/tv/service/aidl/EN_INPUT_SOURCE_TYPE;->E_INPUT_SOURCE_CVBS8:Lcom/mstar/tv/service/aidl/EN_INPUT_SOURCE_TYPE;

    aput-object v2, v0, v1

    const/16 v1, 0xa

    sget-object v2, Lcom/mstar/tv/service/aidl/EN_INPUT_SOURCE_TYPE;->E_INPUT_SOURCE_CVBS_MAX:Lcom/mstar/tv/service/aidl/EN_INPUT_SOURCE_TYPE;

    aput-object v2, v0, v1

    const/16 v1, 0xb

    sget-object v2, Lcom/mstar/tv/service/aidl/EN_INPUT_SOURCE_TYPE;->E_INPUT_SOURCE_SVIDEO:Lcom/mstar/tv/service/aidl/EN_INPUT_SOURCE_TYPE;

    aput-object v2, v0, v1

    const/16 v1, 0xc

    sget-object v2, Lcom/mstar/tv/service/aidl/EN_INPUT_SOURCE_TYPE;->E_INPUT_SOURCE_SVIDEO2:Lcom/mstar/tv/service/aidl/EN_INPUT_SOURCE_TYPE;

    aput-object v2, v0, v1

    const/16 v1, 0xd

    sget-object v2, Lcom/mstar/tv/service/aidl/EN_INPUT_SOURCE_TYPE;->E_INPUT_SOURCE_SVIDEO3:Lcom/mstar/tv/service/aidl/EN_INPUT_SOURCE_TYPE;

    aput-object v2, v0, v1

    const/16 v1, 0xe

    sget-object v2, Lcom/mstar/tv/service/aidl/EN_INPUT_SOURCE_TYPE;->E_INPUT_SOURCE_SVIDEO4:Lcom/mstar/tv/service/aidl/EN_INPUT_SOURCE_TYPE;

    aput-object v2, v0, v1

    const/16 v1, 0xf

    sget-object v2, Lcom/mstar/tv/service/aidl/EN_INPUT_SOURCE_TYPE;->E_INPUT_SOURCE_SVIDEO_MAX:Lcom/mstar/tv/service/aidl/EN_INPUT_SOURCE_TYPE;

    aput-object v2, v0, v1

    const/16 v1, 0x10

    sget-object v2, Lcom/mstar/tv/service/aidl/EN_INPUT_SOURCE_TYPE;->E_INPUT_SOURCE_YPBPR:Lcom/mstar/tv/service/aidl/EN_INPUT_SOURCE_TYPE;

    aput-object v2, v0, v1

    const/16 v1, 0x11

    sget-object v2, Lcom/mstar/tv/service/aidl/EN_INPUT_SOURCE_TYPE;->E_INPUT_SOURCE_YPBPR2:Lcom/mstar/tv/service/aidl/EN_INPUT_SOURCE_TYPE;

    aput-object v2, v0, v1

    const/16 v1, 0x12

    sget-object v2, Lcom/mstar/tv/service/aidl/EN_INPUT_SOURCE_TYPE;->E_INPUT_SOURCE_YPBPR3:Lcom/mstar/tv/service/aidl/EN_INPUT_SOURCE_TYPE;

    aput-object v2, v0, v1

    const/16 v1, 0x13

    sget-object v2, Lcom/mstar/tv/service/aidl/EN_INPUT_SOURCE_TYPE;->E_INPUT_SOURCE_YPBPR_MAX:Lcom/mstar/tv/service/aidl/EN_INPUT_SOURCE_TYPE;

    aput-object v2, v0, v1

    const/16 v1, 0x14

    sget-object v2, Lcom/mstar/tv/service/aidl/EN_INPUT_SOURCE_TYPE;->E_INPUT_SOURCE_SCART:Lcom/mstar/tv/service/aidl/EN_INPUT_SOURCE_TYPE;

    aput-object v2, v0, v1

    const/16 v1, 0x15

    sget-object v2, Lcom/mstar/tv/service/aidl/EN_INPUT_SOURCE_TYPE;->E_INPUT_SOURCE_SCART2:Lcom/mstar/tv/service/aidl/EN_INPUT_SOURCE_TYPE;

    aput-object v2, v0, v1

    const/16 v1, 0x16

    sget-object v2, Lcom/mstar/tv/service/aidl/EN_INPUT_SOURCE_TYPE;->E_INPUT_SOURCE_SCART_MAX:Lcom/mstar/tv/service/aidl/EN_INPUT_SOURCE_TYPE;

    aput-object v2, v0, v1

    const/16 v1, 0x17

    sget-object v2, Lcom/mstar/tv/service/aidl/EN_INPUT_SOURCE_TYPE;->E_INPUT_SOURCE_HDMI:Lcom/mstar/tv/service/aidl/EN_INPUT_SOURCE_TYPE;

    aput-object v2, v0, v1

    const/16 v1, 0x18

    sget-object v2, Lcom/mstar/tv/service/aidl/EN_INPUT_SOURCE_TYPE;->E_INPUT_SOURCE_HDMI2:Lcom/mstar/tv/service/aidl/EN_INPUT_SOURCE_TYPE;

    aput-object v2, v0, v1

    const/16 v1, 0x19

    sget-object v2, Lcom/mstar/tv/service/aidl/EN_INPUT_SOURCE_TYPE;->E_INPUT_SOURCE_HDMI3:Lcom/mstar/tv/service/aidl/EN_INPUT_SOURCE_TYPE;

    aput-object v2, v0, v1

    const/16 v1, 0x1a

    sget-object v2, Lcom/mstar/tv/service/aidl/EN_INPUT_SOURCE_TYPE;->E_INPUT_SOURCE_HDMI4:Lcom/mstar/tv/service/aidl/EN_INPUT_SOURCE_TYPE;

    aput-object v2, v0, v1

    const/16 v1, 0x1b

    sget-object v2, Lcom/mstar/tv/service/aidl/EN_INPUT_SOURCE_TYPE;->E_INPUT_SOURCE_HDMI_MAX:Lcom/mstar/tv/service/aidl/EN_INPUT_SOURCE_TYPE;

    aput-object v2, v0, v1

    const/16 v1, 0x1c

    sget-object v2, Lcom/mstar/tv/service/aidl/EN_INPUT_SOURCE_TYPE;->E_INPUT_SOURCE_DTV:Lcom/mstar/tv/service/aidl/EN_INPUT_SOURCE_TYPE;

    aput-object v2, v0, v1

    const/16 v1, 0x1d

    sget-object v2, Lcom/mstar/tv/service/aidl/EN_INPUT_SOURCE_TYPE;->E_INPUT_SOURCE_DVI:Lcom/mstar/tv/service/aidl/EN_INPUT_SOURCE_TYPE;

    aput-object v2, v0, v1

    const/16 v1, 0x1e

    sget-object v2, Lcom/mstar/tv/service/aidl/EN_INPUT_SOURCE_TYPE;->E_INPUT_SOURCE_DVI2:Lcom/mstar/tv/service/aidl/EN_INPUT_SOURCE_TYPE;

    aput-object v2, v0, v1

    const/16 v1, 0x1f

    sget-object v2, Lcom/mstar/tv/service/aidl/EN_INPUT_SOURCE_TYPE;->E_INPUT_SOURCE_DVI3:Lcom/mstar/tv/service/aidl/EN_INPUT_SOURCE_TYPE;

    aput-object v2, v0, v1

    const/16 v1, 0x20

    sget-object v2, Lcom/mstar/tv/service/aidl/EN_INPUT_SOURCE_TYPE;->E_INPUT_SOURCE_DVI4:Lcom/mstar/tv/service/aidl/EN_INPUT_SOURCE_TYPE;

    aput-object v2, v0, v1

    const/16 v1, 0x21

    sget-object v2, Lcom/mstar/tv/service/aidl/EN_INPUT_SOURCE_TYPE;->E_INPUT_SOURCE_DVI_MAX:Lcom/mstar/tv/service/aidl/EN_INPUT_SOURCE_TYPE;

    aput-object v2, v0, v1

    const/16 v1, 0x22

    sget-object v2, Lcom/mstar/tv/service/aidl/EN_INPUT_SOURCE_TYPE;->E_INPUT_SOURCE_STORAGE:Lcom/mstar/tv/service/aidl/EN_INPUT_SOURCE_TYPE;

    aput-object v2, v0, v1

    const/16 v1, 0x23

    sget-object v2, Lcom/mstar/tv/service/aidl/EN_INPUT_SOURCE_TYPE;->E_INPUT_SOURCE_KTV:Lcom/mstar/tv/service/aidl/EN_INPUT_SOURCE_TYPE;

    aput-object v2, v0, v1

    const/16 v1, 0x24

    sget-object v2, Lcom/mstar/tv/service/aidl/EN_INPUT_SOURCE_TYPE;->E_INPUT_SOURCE_JPEG:Lcom/mstar/tv/service/aidl/EN_INPUT_SOURCE_TYPE;

    aput-object v2, v0, v1

    const/16 v1, 0x25

    sget-object v2, Lcom/mstar/tv/service/aidl/EN_INPUT_SOURCE_TYPE;->E_INPUT_SOURCE_DTV2:Lcom/mstar/tv/service/aidl/EN_INPUT_SOURCE_TYPE;

    aput-object v2, v0, v1

    const/16 v1, 0x26

    sget-object v2, Lcom/mstar/tv/service/aidl/EN_INPUT_SOURCE_TYPE;->E_INPUT_SOURCE_STORAGE2:Lcom/mstar/tv/service/aidl/EN_INPUT_SOURCE_TYPE;

    aput-object v2, v0, v1

    const/16 v1, 0x27

    sget-object v2, Lcom/mstar/tv/service/aidl/EN_INPUT_SOURCE_TYPE;->E_INPUT_SOURCE_NUM:Lcom/mstar/tv/service/aidl/EN_INPUT_SOURCE_TYPE;

    aput-object v2, v0, v1

    const/16 v1, 0x28

    sget-object v2, Lcom/mstar/tv/service/aidl/EN_INPUT_SOURCE_TYPE;->E_INPUT_SOURCE_NONE:Lcom/mstar/tv/service/aidl/EN_INPUT_SOURCE_TYPE;

    aput-object v2, v0, v1

    sput-object v0, Lcom/mstar/tv/service/aidl/EN_INPUT_SOURCE_TYPE;->ENUM$VALUES:[Lcom/mstar/tv/service/aidl/EN_INPUT_SOURCE_TYPE;

    new-instance v0, Lcom/mstar/tv/service/aidl/EN_INPUT_SOURCE_TYPE$1;

    invoke-direct {v0}, Lcom/mstar/tv/service/aidl/EN_INPUT_SOURCE_TYPE$1;-><init>()V

    sput-object v0, Lcom/mstar/tv/service/aidl/EN_INPUT_SOURCE_TYPE;->CREATOR:Landroid/os/Parcelable$Creator;

    return-void
.end method

.method private constructor <init>(Ljava/lang/String;I)V
    .locals 0

    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    return-void
.end method

.method public static valueOf(Ljava/lang/String;)Lcom/mstar/tv/service/aidl/EN_INPUT_SOURCE_TYPE;
    .locals 1

    const-class v0, Lcom/mstar/tv/service/aidl/EN_INPUT_SOURCE_TYPE;

    invoke-static {v0, p0}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;

    move-result-object v0

    check-cast v0, Lcom/mstar/tv/service/aidl/EN_INPUT_SOURCE_TYPE;

    return-object v0
.end method

.method public static values()[Lcom/mstar/tv/service/aidl/EN_INPUT_SOURCE_TYPE;
    .locals 4

    const/4 v3, 0x0

    sget-object v0, Lcom/mstar/tv/service/aidl/EN_INPUT_SOURCE_TYPE;->ENUM$VALUES:[Lcom/mstar/tv/service/aidl/EN_INPUT_SOURCE_TYPE;

    array-length v1, v0

    new-array v2, v1, [Lcom/mstar/tv/service/aidl/EN_INPUT_SOURCE_TYPE;

    invoke-static {v0, v3, v2, v3, v1}, Ljava/lang/System;->arraycopy(Ljava/lang/Object;ILjava/lang/Object;II)V

    return-object v2
.end method


# virtual methods
.method public describeContents()I
    .locals 1

    const/4 v0, 0x0

    return v0
.end method

.method public writeToParcel(Landroid/os/Parcel;I)V
    .locals 1
    .param p1    # Landroid/os/Parcel;
    .param p2    # I

    invoke-virtual {p0}, Lcom/mstar/tv/service/aidl/EN_INPUT_SOURCE_TYPE;->ordinal()I

    move-result v0

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeInt(I)V

    return-void
.end method
