.class public final enum Lcom/mstar/tv/service/aidl/EN_SCALER_WIN;
.super Ljava/lang/Enum;
.source "EN_SCALER_WIN.java"

# interfaces
.implements Landroid/os/Parcelable;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Enum",
        "<",
        "Lcom/mstar/tv/service/aidl/EN_SCALER_WIN;",
        ">;",
        "Landroid/os/Parcelable;"
    }
.end annotation


# static fields
.field public static final CREATOR:Landroid/os/Parcelable$Creator;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Landroid/os/Parcelable$Creator",
            "<",
            "Lcom/mstar/tv/service/aidl/EN_SCALER_WIN;",
            ">;"
        }
    .end annotation
.end field

.field private static final synthetic ENUM$VALUES:[Lcom/mstar/tv/service/aidl/EN_SCALER_WIN;

.field public static final enum MAPI_MAIN_WINDOW:Lcom/mstar/tv/service/aidl/EN_SCALER_WIN;

.field public static final enum MAPI_MAX_WINDOW:Lcom/mstar/tv/service/aidl/EN_SCALER_WIN;

.field public static final enum MAPI_SUB_WINDOW:Lcom/mstar/tv/service/aidl/EN_SCALER_WIN;


# direct methods
.method static constructor <clinit>()V
    .locals 5

    const/4 v4, 0x2

    const/4 v3, 0x1

    const/4 v2, 0x0

    new-instance v0, Lcom/mstar/tv/service/aidl/EN_SCALER_WIN;

    const-string v1, "MAPI_MAIN_WINDOW"

    invoke-direct {v0, v1, v2}, Lcom/mstar/tv/service/aidl/EN_SCALER_WIN;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/mstar/tv/service/aidl/EN_SCALER_WIN;->MAPI_MAIN_WINDOW:Lcom/mstar/tv/service/aidl/EN_SCALER_WIN;

    new-instance v0, Lcom/mstar/tv/service/aidl/EN_SCALER_WIN;

    const-string v1, "MAPI_SUB_WINDOW"

    invoke-direct {v0, v1, v3}, Lcom/mstar/tv/service/aidl/EN_SCALER_WIN;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/mstar/tv/service/aidl/EN_SCALER_WIN;->MAPI_SUB_WINDOW:Lcom/mstar/tv/service/aidl/EN_SCALER_WIN;

    new-instance v0, Lcom/mstar/tv/service/aidl/EN_SCALER_WIN;

    const-string v1, "MAPI_MAX_WINDOW"

    invoke-direct {v0, v1, v4}, Lcom/mstar/tv/service/aidl/EN_SCALER_WIN;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/mstar/tv/service/aidl/EN_SCALER_WIN;->MAPI_MAX_WINDOW:Lcom/mstar/tv/service/aidl/EN_SCALER_WIN;

    const/4 v0, 0x3

    new-array v0, v0, [Lcom/mstar/tv/service/aidl/EN_SCALER_WIN;

    sget-object v1, Lcom/mstar/tv/service/aidl/EN_SCALER_WIN;->MAPI_MAIN_WINDOW:Lcom/mstar/tv/service/aidl/EN_SCALER_WIN;

    aput-object v1, v0, v2

    sget-object v1, Lcom/mstar/tv/service/aidl/EN_SCALER_WIN;->MAPI_SUB_WINDOW:Lcom/mstar/tv/service/aidl/EN_SCALER_WIN;

    aput-object v1, v0, v3

    sget-object v1, Lcom/mstar/tv/service/aidl/EN_SCALER_WIN;->MAPI_MAX_WINDOW:Lcom/mstar/tv/service/aidl/EN_SCALER_WIN;

    aput-object v1, v0, v4

    sput-object v0, Lcom/mstar/tv/service/aidl/EN_SCALER_WIN;->ENUM$VALUES:[Lcom/mstar/tv/service/aidl/EN_SCALER_WIN;

    new-instance v0, Lcom/mstar/tv/service/aidl/EN_SCALER_WIN$1;

    invoke-direct {v0}, Lcom/mstar/tv/service/aidl/EN_SCALER_WIN$1;-><init>()V

    sput-object v0, Lcom/mstar/tv/service/aidl/EN_SCALER_WIN;->CREATOR:Landroid/os/Parcelable$Creator;

    return-void
.end method

.method private constructor <init>(Ljava/lang/String;I)V
    .locals 0

    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    return-void
.end method

.method public static valueOf(Ljava/lang/String;)Lcom/mstar/tv/service/aidl/EN_SCALER_WIN;
    .locals 1

    const-class v0, Lcom/mstar/tv/service/aidl/EN_SCALER_WIN;

    invoke-static {v0, p0}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;

    move-result-object v0

    check-cast v0, Lcom/mstar/tv/service/aidl/EN_SCALER_WIN;

    return-object v0
.end method

.method public static values()[Lcom/mstar/tv/service/aidl/EN_SCALER_WIN;
    .locals 4

    const/4 v3, 0x0

    sget-object v0, Lcom/mstar/tv/service/aidl/EN_SCALER_WIN;->ENUM$VALUES:[Lcom/mstar/tv/service/aidl/EN_SCALER_WIN;

    array-length v1, v0

    new-array v2, v1, [Lcom/mstar/tv/service/aidl/EN_SCALER_WIN;

    invoke-static {v0, v3, v2, v3, v1}, Ljava/lang/System;->arraycopy(Ljava/lang/Object;ILjava/lang/Object;II)V

    return-object v2
.end method


# virtual methods
.method public describeContents()I
    .locals 1

    const/4 v0, 0x0

    return v0
.end method

.method public writeToParcel(Landroid/os/Parcel;I)V
    .locals 1
    .param p1    # Landroid/os/Parcel;
    .param p2    # I

    invoke-virtual {p0}, Lcom/mstar/tv/service/aidl/EN_SCALER_WIN;->ordinal()I

    move-result v0

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeInt(I)V

    return-void
.end method
