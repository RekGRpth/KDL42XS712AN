.class public final enum Lcom/mstar/tv/service/aidl/EN_ATV_SYSTEM_STANDARD;
.super Ljava/lang/Enum;
.source "EN_ATV_SYSTEM_STANDARD.java"

# interfaces
.implements Landroid/os/Parcelable;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Enum",
        "<",
        "Lcom/mstar/tv/service/aidl/EN_ATV_SYSTEM_STANDARD;",
        ">;",
        "Landroid/os/Parcelable;"
    }
.end annotation


# static fields
.field public static final CREATOR:Landroid/os/Parcelable$Creator;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Landroid/os/Parcelable$Creator",
            "<",
            "Lcom/mstar/tv/service/aidl/EN_ATV_SYSTEM_STANDARD;",
            ">;"
        }
    .end annotation
.end field

.field private static final synthetic ENUM$VALUES:[Lcom/mstar/tv/service/aidl/EN_ATV_SYSTEM_STANDARD;

.field public static final enum E_BG:Lcom/mstar/tv/service/aidl/EN_ATV_SYSTEM_STANDARD;

.field public static final enum E_DK:Lcom/mstar/tv/service/aidl/EN_ATV_SYSTEM_STANDARD;

.field public static final enum E_I:Lcom/mstar/tv/service/aidl/EN_ATV_SYSTEM_STANDARD;

.field public static final enum E_L:Lcom/mstar/tv/service/aidl/EN_ATV_SYSTEM_STANDARD;

.field public static final enum E_M:Lcom/mstar/tv/service/aidl/EN_ATV_SYSTEM_STANDARD;

.field public static final enum E_NUM:Lcom/mstar/tv/service/aidl/EN_ATV_SYSTEM_STANDARD;


# direct methods
.method static constructor <clinit>()V
    .locals 8

    const/4 v7, 0x4

    const/4 v6, 0x3

    const/4 v5, 0x2

    const/4 v4, 0x1

    const/4 v3, 0x0

    new-instance v0, Lcom/mstar/tv/service/aidl/EN_ATV_SYSTEM_STANDARD;

    const-string v1, "E_BG"

    invoke-direct {v0, v1, v3}, Lcom/mstar/tv/service/aidl/EN_ATV_SYSTEM_STANDARD;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/mstar/tv/service/aidl/EN_ATV_SYSTEM_STANDARD;->E_BG:Lcom/mstar/tv/service/aidl/EN_ATV_SYSTEM_STANDARD;

    new-instance v0, Lcom/mstar/tv/service/aidl/EN_ATV_SYSTEM_STANDARD;

    const-string v1, "E_DK"

    invoke-direct {v0, v1, v4}, Lcom/mstar/tv/service/aidl/EN_ATV_SYSTEM_STANDARD;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/mstar/tv/service/aidl/EN_ATV_SYSTEM_STANDARD;->E_DK:Lcom/mstar/tv/service/aidl/EN_ATV_SYSTEM_STANDARD;

    new-instance v0, Lcom/mstar/tv/service/aidl/EN_ATV_SYSTEM_STANDARD;

    const-string v1, "E_I"

    invoke-direct {v0, v1, v5}, Lcom/mstar/tv/service/aidl/EN_ATV_SYSTEM_STANDARD;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/mstar/tv/service/aidl/EN_ATV_SYSTEM_STANDARD;->E_I:Lcom/mstar/tv/service/aidl/EN_ATV_SYSTEM_STANDARD;

    new-instance v0, Lcom/mstar/tv/service/aidl/EN_ATV_SYSTEM_STANDARD;

    const-string v1, "E_L"

    invoke-direct {v0, v1, v6}, Lcom/mstar/tv/service/aidl/EN_ATV_SYSTEM_STANDARD;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/mstar/tv/service/aidl/EN_ATV_SYSTEM_STANDARD;->E_L:Lcom/mstar/tv/service/aidl/EN_ATV_SYSTEM_STANDARD;

    new-instance v0, Lcom/mstar/tv/service/aidl/EN_ATV_SYSTEM_STANDARD;

    const-string v1, "E_M"

    invoke-direct {v0, v1, v7}, Lcom/mstar/tv/service/aidl/EN_ATV_SYSTEM_STANDARD;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/mstar/tv/service/aidl/EN_ATV_SYSTEM_STANDARD;->E_M:Lcom/mstar/tv/service/aidl/EN_ATV_SYSTEM_STANDARD;

    new-instance v0, Lcom/mstar/tv/service/aidl/EN_ATV_SYSTEM_STANDARD;

    const-string v1, "E_NUM"

    const/4 v2, 0x5

    invoke-direct {v0, v1, v2}, Lcom/mstar/tv/service/aidl/EN_ATV_SYSTEM_STANDARD;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/mstar/tv/service/aidl/EN_ATV_SYSTEM_STANDARD;->E_NUM:Lcom/mstar/tv/service/aidl/EN_ATV_SYSTEM_STANDARD;

    const/4 v0, 0x6

    new-array v0, v0, [Lcom/mstar/tv/service/aidl/EN_ATV_SYSTEM_STANDARD;

    sget-object v1, Lcom/mstar/tv/service/aidl/EN_ATV_SYSTEM_STANDARD;->E_BG:Lcom/mstar/tv/service/aidl/EN_ATV_SYSTEM_STANDARD;

    aput-object v1, v0, v3

    sget-object v1, Lcom/mstar/tv/service/aidl/EN_ATV_SYSTEM_STANDARD;->E_DK:Lcom/mstar/tv/service/aidl/EN_ATV_SYSTEM_STANDARD;

    aput-object v1, v0, v4

    sget-object v1, Lcom/mstar/tv/service/aidl/EN_ATV_SYSTEM_STANDARD;->E_I:Lcom/mstar/tv/service/aidl/EN_ATV_SYSTEM_STANDARD;

    aput-object v1, v0, v5

    sget-object v1, Lcom/mstar/tv/service/aidl/EN_ATV_SYSTEM_STANDARD;->E_L:Lcom/mstar/tv/service/aidl/EN_ATV_SYSTEM_STANDARD;

    aput-object v1, v0, v6

    sget-object v1, Lcom/mstar/tv/service/aidl/EN_ATV_SYSTEM_STANDARD;->E_M:Lcom/mstar/tv/service/aidl/EN_ATV_SYSTEM_STANDARD;

    aput-object v1, v0, v7

    const/4 v1, 0x5

    sget-object v2, Lcom/mstar/tv/service/aidl/EN_ATV_SYSTEM_STANDARD;->E_NUM:Lcom/mstar/tv/service/aidl/EN_ATV_SYSTEM_STANDARD;

    aput-object v2, v0, v1

    sput-object v0, Lcom/mstar/tv/service/aidl/EN_ATV_SYSTEM_STANDARD;->ENUM$VALUES:[Lcom/mstar/tv/service/aidl/EN_ATV_SYSTEM_STANDARD;

    new-instance v0, Lcom/mstar/tv/service/aidl/EN_ATV_SYSTEM_STANDARD$1;

    invoke-direct {v0}, Lcom/mstar/tv/service/aidl/EN_ATV_SYSTEM_STANDARD$1;-><init>()V

    sput-object v0, Lcom/mstar/tv/service/aidl/EN_ATV_SYSTEM_STANDARD;->CREATOR:Landroid/os/Parcelable$Creator;

    return-void
.end method

.method private constructor <init>(Ljava/lang/String;I)V
    .locals 0

    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    return-void
.end method

.method public static valueOf(Ljava/lang/String;)Lcom/mstar/tv/service/aidl/EN_ATV_SYSTEM_STANDARD;
    .locals 1

    const-class v0, Lcom/mstar/tv/service/aidl/EN_ATV_SYSTEM_STANDARD;

    invoke-static {v0, p0}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;

    move-result-object v0

    check-cast v0, Lcom/mstar/tv/service/aidl/EN_ATV_SYSTEM_STANDARD;

    return-object v0
.end method

.method public static values()[Lcom/mstar/tv/service/aidl/EN_ATV_SYSTEM_STANDARD;
    .locals 4

    const/4 v3, 0x0

    sget-object v0, Lcom/mstar/tv/service/aidl/EN_ATV_SYSTEM_STANDARD;->ENUM$VALUES:[Lcom/mstar/tv/service/aidl/EN_ATV_SYSTEM_STANDARD;

    array-length v1, v0

    new-array v2, v1, [Lcom/mstar/tv/service/aidl/EN_ATV_SYSTEM_STANDARD;

    invoke-static {v0, v3, v2, v3, v1}, Ljava/lang/System;->arraycopy(Ljava/lang/Object;ILjava/lang/Object;II)V

    return-object v2
.end method


# virtual methods
.method public describeContents()I
    .locals 1

    const/4 v0, 0x0

    return v0
.end method

.method public writeToParcel(Landroid/os/Parcel;I)V
    .locals 1
    .param p1    # Landroid/os/Parcel;
    .param p2    # I

    invoke-virtual {p0}, Lcom/mstar/tv/service/aidl/EN_ATV_SYSTEM_STANDARD;->ordinal()I

    move-result v0

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeInt(I)V

    return-void
.end method
