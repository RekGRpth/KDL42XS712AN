.class public Lcom/mstar/tv/service/aidl/VIDEO_WINDOW_TYPE;
.super Ljava/lang/Object;
.source "VIDEO_WINDOW_TYPE.java"

# interfaces
.implements Landroid/os/Parcelable;


# static fields
.field public static final CREATOR:Landroid/os/Parcelable$Creator;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Landroid/os/Parcelable$Creator",
            "<",
            "Lcom/mstar/tv/service/aidl/VIDEO_WINDOW_TYPE;",
            ">;"
        }
    .end annotation
.end field


# instance fields
.field public height:I

.field public width:I

.field public x:I

.field public y:I


# direct methods
.method static constructor <clinit>()V
    .locals 1

    new-instance v0, Lcom/mstar/tv/service/aidl/VIDEO_WINDOW_TYPE$1;

    invoke-direct {v0}, Lcom/mstar/tv/service/aidl/VIDEO_WINDOW_TYPE$1;-><init>()V

    sput-object v0, Lcom/mstar/tv/service/aidl/VIDEO_WINDOW_TYPE;->CREATOR:Landroid/os/Parcelable$Creator;

    return-void
.end method

.method public constructor <init>(IIII)V
    .locals 0
    .param p1    # I
    .param p2    # I
    .param p3    # I
    .param p4    # I

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    iput p1, p0, Lcom/mstar/tv/service/aidl/VIDEO_WINDOW_TYPE;->x:I

    iput p2, p0, Lcom/mstar/tv/service/aidl/VIDEO_WINDOW_TYPE;->y:I

    iput p3, p0, Lcom/mstar/tv/service/aidl/VIDEO_WINDOW_TYPE;->width:I

    iput p4, p0, Lcom/mstar/tv/service/aidl/VIDEO_WINDOW_TYPE;->height:I

    return-void
.end method

.method private constructor <init>(Landroid/os/Parcel;)V
    .locals 1
    .param p1    # Landroid/os/Parcel;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    invoke-virtual {p1}, Landroid/os/Parcel;->readInt()I

    move-result v0

    iput v0, p0, Lcom/mstar/tv/service/aidl/VIDEO_WINDOW_TYPE;->x:I

    invoke-virtual {p1}, Landroid/os/Parcel;->readInt()I

    move-result v0

    iput v0, p0, Lcom/mstar/tv/service/aidl/VIDEO_WINDOW_TYPE;->y:I

    invoke-virtual {p1}, Landroid/os/Parcel;->readInt()I

    move-result v0

    iput v0, p0, Lcom/mstar/tv/service/aidl/VIDEO_WINDOW_TYPE;->width:I

    invoke-virtual {p1}, Landroid/os/Parcel;->readInt()I

    move-result v0

    iput v0, p0, Lcom/mstar/tv/service/aidl/VIDEO_WINDOW_TYPE;->height:I

    return-void
.end method

.method synthetic constructor <init>(Landroid/os/Parcel;Lcom/mstar/tv/service/aidl/VIDEO_WINDOW_TYPE;)V
    .locals 0

    invoke-direct {p0, p1}, Lcom/mstar/tv/service/aidl/VIDEO_WINDOW_TYPE;-><init>(Landroid/os/Parcel;)V

    return-void
.end method


# virtual methods
.method public describeContents()I
    .locals 1

    const/4 v0, 0x0

    return v0
.end method

.method public writeToParcel(Landroid/os/Parcel;I)V
    .locals 1
    .param p1    # Landroid/os/Parcel;
    .param p2    # I

    iget v0, p0, Lcom/mstar/tv/service/aidl/VIDEO_WINDOW_TYPE;->x:I

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeInt(I)V

    iget v0, p0, Lcom/mstar/tv/service/aidl/VIDEO_WINDOW_TYPE;->y:I

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeInt(I)V

    iget v0, p0, Lcom/mstar/tv/service/aidl/VIDEO_WINDOW_TYPE;->width:I

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeInt(I)V

    iget v0, p0, Lcom/mstar/tv/service/aidl/VIDEO_WINDOW_TYPE;->height:I

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeInt(I)V

    return-void
.end method
