.class public final enum Lcom/mstar/tv/service/aidl/EN_MEMBER_COUNTRY;
.super Ljava/lang/Enum;
.source "EN_MEMBER_COUNTRY.java"

# interfaces
.implements Landroid/os/Parcelable;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Enum",
        "<",
        "Lcom/mstar/tv/service/aidl/EN_MEMBER_COUNTRY;",
        ">;",
        "Landroid/os/Parcelable;"
    }
.end annotation


# static fields
.field public static final CREATOR:Landroid/os/Parcelable$Creator;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Landroid/os/Parcelable$Creator",
            "<",
            "Lcom/mstar/tv/service/aidl/EN_MEMBER_COUNTRY;",
            ">;"
        }
    .end annotation
.end field

.field private static final synthetic ENUM$VALUES:[Lcom/mstar/tv/service/aidl/EN_MEMBER_COUNTRY;

.field public static final enum E_ARAB:Lcom/mstar/tv/service/aidl/EN_MEMBER_COUNTRY;

.field public static final enum E_AUSTRALIA:Lcom/mstar/tv/service/aidl/EN_MEMBER_COUNTRY;

.field public static final enum E_AUSTRIA:Lcom/mstar/tv/service/aidl/EN_MEMBER_COUNTRY;

.field public static final enum E_BELGIUM:Lcom/mstar/tv/service/aidl/EN_MEMBER_COUNTRY;

.field public static final enum E_BRAZIL:Lcom/mstar/tv/service/aidl/EN_MEMBER_COUNTRY;

.field public static final enum E_BULGARIA:Lcom/mstar/tv/service/aidl/EN_MEMBER_COUNTRY;

.field public static final enum E_CANADA:Lcom/mstar/tv/service/aidl/EN_MEMBER_COUNTRY;

.field public static final enum E_CHINA:Lcom/mstar/tv/service/aidl/EN_MEMBER_COUNTRY;

.field public static final enum E_COUNTRY_NUM:Lcom/mstar/tv/service/aidl/EN_MEMBER_COUNTRY;

.field public static final enum E_CROATIA:Lcom/mstar/tv/service/aidl/EN_MEMBER_COUNTRY;

.field public static final enum E_CZECH:Lcom/mstar/tv/service/aidl/EN_MEMBER_COUNTRY;

.field public static final enum E_DENMARK:Lcom/mstar/tv/service/aidl/EN_MEMBER_COUNTRY;

.field public static final enum E_ESTONIA:Lcom/mstar/tv/service/aidl/EN_MEMBER_COUNTRY;

.field public static final enum E_FINLAND:Lcom/mstar/tv/service/aidl/EN_MEMBER_COUNTRY;

.field public static final enum E_FRANCE:Lcom/mstar/tv/service/aidl/EN_MEMBER_COUNTRY;

.field public static final enum E_GERMANY:Lcom/mstar/tv/service/aidl/EN_MEMBER_COUNTRY;

.field public static final enum E_GREECE:Lcom/mstar/tv/service/aidl/EN_MEMBER_COUNTRY;

.field public static final enum E_HEBREW:Lcom/mstar/tv/service/aidl/EN_MEMBER_COUNTRY;

.field public static final enum E_HUNGARY:Lcom/mstar/tv/service/aidl/EN_MEMBER_COUNTRY;

.field public static final enum E_IRELAND:Lcom/mstar/tv/service/aidl/EN_MEMBER_COUNTRY;

.field public static final enum E_ITALY:Lcom/mstar/tv/service/aidl/EN_MEMBER_COUNTRY;

.field public static final enum E_LATVIA:Lcom/mstar/tv/service/aidl/EN_MEMBER_COUNTRY;

.field public static final enum E_LUXEMBOURG:Lcom/mstar/tv/service/aidl/EN_MEMBER_COUNTRY;

.field public static final enum E_MEXICO:Lcom/mstar/tv/service/aidl/EN_MEMBER_COUNTRY;

.field public static final enum E_NETHERLANDS:Lcom/mstar/tv/service/aidl/EN_MEMBER_COUNTRY;

.field public static final enum E_NEWZEALAND:Lcom/mstar/tv/service/aidl/EN_MEMBER_COUNTRY;

.field public static final enum E_NORWAY:Lcom/mstar/tv/service/aidl/EN_MEMBER_COUNTRY;

.field public static final enum E_OTHERS:Lcom/mstar/tv/service/aidl/EN_MEMBER_COUNTRY;

.field public static final enum E_POLAND:Lcom/mstar/tv/service/aidl/EN_MEMBER_COUNTRY;

.field public static final enum E_PORTUGAL:Lcom/mstar/tv/service/aidl/EN_MEMBER_COUNTRY;

.field public static final enum E_RUMANIA:Lcom/mstar/tv/service/aidl/EN_MEMBER_COUNTRY;

.field public static final enum E_RUSSIA:Lcom/mstar/tv/service/aidl/EN_MEMBER_COUNTRY;

.field public static final enum E_SERBIA:Lcom/mstar/tv/service/aidl/EN_MEMBER_COUNTRY;

.field public static final enum E_SLOVAKIA:Lcom/mstar/tv/service/aidl/EN_MEMBER_COUNTRY;

.field public static final enum E_SLOVENIA:Lcom/mstar/tv/service/aidl/EN_MEMBER_COUNTRY;

.field public static final enum E_SOUTHKOREA:Lcom/mstar/tv/service/aidl/EN_MEMBER_COUNTRY;

.field public static final enum E_SPAIN:Lcom/mstar/tv/service/aidl/EN_MEMBER_COUNTRY;

.field public static final enum E_SWEDEN:Lcom/mstar/tv/service/aidl/EN_MEMBER_COUNTRY;

.field public static final enum E_SWITZERLAND:Lcom/mstar/tv/service/aidl/EN_MEMBER_COUNTRY;

.field public static final enum E_TAIWAN:Lcom/mstar/tv/service/aidl/EN_MEMBER_COUNTRY;

.field public static final enum E_TURKEY:Lcom/mstar/tv/service/aidl/EN_MEMBER_COUNTRY;

.field public static final enum E_UK:Lcom/mstar/tv/service/aidl/EN_MEMBER_COUNTRY;

.field public static final enum E_US:Lcom/mstar/tv/service/aidl/EN_MEMBER_COUNTRY;


# direct methods
.method static constructor <clinit>()V
    .locals 8

    const/4 v7, 0x4

    const/4 v6, 0x3

    const/4 v5, 0x2

    const/4 v4, 0x1

    const/4 v3, 0x0

    new-instance v0, Lcom/mstar/tv/service/aidl/EN_MEMBER_COUNTRY;

    const-string v1, "E_AUSTRALIA"

    invoke-direct {v0, v1, v3}, Lcom/mstar/tv/service/aidl/EN_MEMBER_COUNTRY;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/mstar/tv/service/aidl/EN_MEMBER_COUNTRY;->E_AUSTRALIA:Lcom/mstar/tv/service/aidl/EN_MEMBER_COUNTRY;

    new-instance v0, Lcom/mstar/tv/service/aidl/EN_MEMBER_COUNTRY;

    const-string v1, "E_AUSTRIA"

    invoke-direct {v0, v1, v4}, Lcom/mstar/tv/service/aidl/EN_MEMBER_COUNTRY;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/mstar/tv/service/aidl/EN_MEMBER_COUNTRY;->E_AUSTRIA:Lcom/mstar/tv/service/aidl/EN_MEMBER_COUNTRY;

    new-instance v0, Lcom/mstar/tv/service/aidl/EN_MEMBER_COUNTRY;

    const-string v1, "E_BELGIUM"

    invoke-direct {v0, v1, v5}, Lcom/mstar/tv/service/aidl/EN_MEMBER_COUNTRY;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/mstar/tv/service/aidl/EN_MEMBER_COUNTRY;->E_BELGIUM:Lcom/mstar/tv/service/aidl/EN_MEMBER_COUNTRY;

    new-instance v0, Lcom/mstar/tv/service/aidl/EN_MEMBER_COUNTRY;

    const-string v1, "E_BULGARIA"

    invoke-direct {v0, v1, v6}, Lcom/mstar/tv/service/aidl/EN_MEMBER_COUNTRY;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/mstar/tv/service/aidl/EN_MEMBER_COUNTRY;->E_BULGARIA:Lcom/mstar/tv/service/aidl/EN_MEMBER_COUNTRY;

    new-instance v0, Lcom/mstar/tv/service/aidl/EN_MEMBER_COUNTRY;

    const-string v1, "E_CROATIA"

    invoke-direct {v0, v1, v7}, Lcom/mstar/tv/service/aidl/EN_MEMBER_COUNTRY;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/mstar/tv/service/aidl/EN_MEMBER_COUNTRY;->E_CROATIA:Lcom/mstar/tv/service/aidl/EN_MEMBER_COUNTRY;

    new-instance v0, Lcom/mstar/tv/service/aidl/EN_MEMBER_COUNTRY;

    const-string v1, "E_CZECH"

    const/4 v2, 0x5

    invoke-direct {v0, v1, v2}, Lcom/mstar/tv/service/aidl/EN_MEMBER_COUNTRY;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/mstar/tv/service/aidl/EN_MEMBER_COUNTRY;->E_CZECH:Lcom/mstar/tv/service/aidl/EN_MEMBER_COUNTRY;

    new-instance v0, Lcom/mstar/tv/service/aidl/EN_MEMBER_COUNTRY;

    const-string v1, "E_DENMARK"

    const/4 v2, 0x6

    invoke-direct {v0, v1, v2}, Lcom/mstar/tv/service/aidl/EN_MEMBER_COUNTRY;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/mstar/tv/service/aidl/EN_MEMBER_COUNTRY;->E_DENMARK:Lcom/mstar/tv/service/aidl/EN_MEMBER_COUNTRY;

    new-instance v0, Lcom/mstar/tv/service/aidl/EN_MEMBER_COUNTRY;

    const-string v1, "E_FINLAND"

    const/4 v2, 0x7

    invoke-direct {v0, v1, v2}, Lcom/mstar/tv/service/aidl/EN_MEMBER_COUNTRY;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/mstar/tv/service/aidl/EN_MEMBER_COUNTRY;->E_FINLAND:Lcom/mstar/tv/service/aidl/EN_MEMBER_COUNTRY;

    new-instance v0, Lcom/mstar/tv/service/aidl/EN_MEMBER_COUNTRY;

    const-string v1, "E_FRANCE"

    const/16 v2, 0x8

    invoke-direct {v0, v1, v2}, Lcom/mstar/tv/service/aidl/EN_MEMBER_COUNTRY;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/mstar/tv/service/aidl/EN_MEMBER_COUNTRY;->E_FRANCE:Lcom/mstar/tv/service/aidl/EN_MEMBER_COUNTRY;

    new-instance v0, Lcom/mstar/tv/service/aidl/EN_MEMBER_COUNTRY;

    const-string v1, "E_GERMANY"

    const/16 v2, 0x9

    invoke-direct {v0, v1, v2}, Lcom/mstar/tv/service/aidl/EN_MEMBER_COUNTRY;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/mstar/tv/service/aidl/EN_MEMBER_COUNTRY;->E_GERMANY:Lcom/mstar/tv/service/aidl/EN_MEMBER_COUNTRY;

    new-instance v0, Lcom/mstar/tv/service/aidl/EN_MEMBER_COUNTRY;

    const-string v1, "E_GREECE"

    const/16 v2, 0xa

    invoke-direct {v0, v1, v2}, Lcom/mstar/tv/service/aidl/EN_MEMBER_COUNTRY;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/mstar/tv/service/aidl/EN_MEMBER_COUNTRY;->E_GREECE:Lcom/mstar/tv/service/aidl/EN_MEMBER_COUNTRY;

    new-instance v0, Lcom/mstar/tv/service/aidl/EN_MEMBER_COUNTRY;

    const-string v1, "E_HUNGARY"

    const/16 v2, 0xb

    invoke-direct {v0, v1, v2}, Lcom/mstar/tv/service/aidl/EN_MEMBER_COUNTRY;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/mstar/tv/service/aidl/EN_MEMBER_COUNTRY;->E_HUNGARY:Lcom/mstar/tv/service/aidl/EN_MEMBER_COUNTRY;

    new-instance v0, Lcom/mstar/tv/service/aidl/EN_MEMBER_COUNTRY;

    const-string v1, "E_ITALY"

    const/16 v2, 0xc

    invoke-direct {v0, v1, v2}, Lcom/mstar/tv/service/aidl/EN_MEMBER_COUNTRY;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/mstar/tv/service/aidl/EN_MEMBER_COUNTRY;->E_ITALY:Lcom/mstar/tv/service/aidl/EN_MEMBER_COUNTRY;

    new-instance v0, Lcom/mstar/tv/service/aidl/EN_MEMBER_COUNTRY;

    const-string v1, "E_LUXEMBOURG"

    const/16 v2, 0xd

    invoke-direct {v0, v1, v2}, Lcom/mstar/tv/service/aidl/EN_MEMBER_COUNTRY;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/mstar/tv/service/aidl/EN_MEMBER_COUNTRY;->E_LUXEMBOURG:Lcom/mstar/tv/service/aidl/EN_MEMBER_COUNTRY;

    new-instance v0, Lcom/mstar/tv/service/aidl/EN_MEMBER_COUNTRY;

    const-string v1, "E_NETHERLANDS"

    const/16 v2, 0xe

    invoke-direct {v0, v1, v2}, Lcom/mstar/tv/service/aidl/EN_MEMBER_COUNTRY;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/mstar/tv/service/aidl/EN_MEMBER_COUNTRY;->E_NETHERLANDS:Lcom/mstar/tv/service/aidl/EN_MEMBER_COUNTRY;

    new-instance v0, Lcom/mstar/tv/service/aidl/EN_MEMBER_COUNTRY;

    const-string v1, "E_NORWAY"

    const/16 v2, 0xf

    invoke-direct {v0, v1, v2}, Lcom/mstar/tv/service/aidl/EN_MEMBER_COUNTRY;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/mstar/tv/service/aidl/EN_MEMBER_COUNTRY;->E_NORWAY:Lcom/mstar/tv/service/aidl/EN_MEMBER_COUNTRY;

    new-instance v0, Lcom/mstar/tv/service/aidl/EN_MEMBER_COUNTRY;

    const-string v1, "E_POLAND"

    const/16 v2, 0x10

    invoke-direct {v0, v1, v2}, Lcom/mstar/tv/service/aidl/EN_MEMBER_COUNTRY;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/mstar/tv/service/aidl/EN_MEMBER_COUNTRY;->E_POLAND:Lcom/mstar/tv/service/aidl/EN_MEMBER_COUNTRY;

    new-instance v0, Lcom/mstar/tv/service/aidl/EN_MEMBER_COUNTRY;

    const-string v1, "E_PORTUGAL"

    const/16 v2, 0x11

    invoke-direct {v0, v1, v2}, Lcom/mstar/tv/service/aidl/EN_MEMBER_COUNTRY;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/mstar/tv/service/aidl/EN_MEMBER_COUNTRY;->E_PORTUGAL:Lcom/mstar/tv/service/aidl/EN_MEMBER_COUNTRY;

    new-instance v0, Lcom/mstar/tv/service/aidl/EN_MEMBER_COUNTRY;

    const-string v1, "E_RUMANIA"

    const/16 v2, 0x12

    invoke-direct {v0, v1, v2}, Lcom/mstar/tv/service/aidl/EN_MEMBER_COUNTRY;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/mstar/tv/service/aidl/EN_MEMBER_COUNTRY;->E_RUMANIA:Lcom/mstar/tv/service/aidl/EN_MEMBER_COUNTRY;

    new-instance v0, Lcom/mstar/tv/service/aidl/EN_MEMBER_COUNTRY;

    const-string v1, "E_RUSSIA"

    const/16 v2, 0x13

    invoke-direct {v0, v1, v2}, Lcom/mstar/tv/service/aidl/EN_MEMBER_COUNTRY;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/mstar/tv/service/aidl/EN_MEMBER_COUNTRY;->E_RUSSIA:Lcom/mstar/tv/service/aidl/EN_MEMBER_COUNTRY;

    new-instance v0, Lcom/mstar/tv/service/aidl/EN_MEMBER_COUNTRY;

    const-string v1, "E_SERBIA"

    const/16 v2, 0x14

    invoke-direct {v0, v1, v2}, Lcom/mstar/tv/service/aidl/EN_MEMBER_COUNTRY;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/mstar/tv/service/aidl/EN_MEMBER_COUNTRY;->E_SERBIA:Lcom/mstar/tv/service/aidl/EN_MEMBER_COUNTRY;

    new-instance v0, Lcom/mstar/tv/service/aidl/EN_MEMBER_COUNTRY;

    const-string v1, "E_SLOVENIA"

    const/16 v2, 0x15

    invoke-direct {v0, v1, v2}, Lcom/mstar/tv/service/aidl/EN_MEMBER_COUNTRY;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/mstar/tv/service/aidl/EN_MEMBER_COUNTRY;->E_SLOVENIA:Lcom/mstar/tv/service/aidl/EN_MEMBER_COUNTRY;

    new-instance v0, Lcom/mstar/tv/service/aidl/EN_MEMBER_COUNTRY;

    const-string v1, "E_SPAIN"

    const/16 v2, 0x16

    invoke-direct {v0, v1, v2}, Lcom/mstar/tv/service/aidl/EN_MEMBER_COUNTRY;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/mstar/tv/service/aidl/EN_MEMBER_COUNTRY;->E_SPAIN:Lcom/mstar/tv/service/aidl/EN_MEMBER_COUNTRY;

    new-instance v0, Lcom/mstar/tv/service/aidl/EN_MEMBER_COUNTRY;

    const-string v1, "E_SWEDEN"

    const/16 v2, 0x17

    invoke-direct {v0, v1, v2}, Lcom/mstar/tv/service/aidl/EN_MEMBER_COUNTRY;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/mstar/tv/service/aidl/EN_MEMBER_COUNTRY;->E_SWEDEN:Lcom/mstar/tv/service/aidl/EN_MEMBER_COUNTRY;

    new-instance v0, Lcom/mstar/tv/service/aidl/EN_MEMBER_COUNTRY;

    const-string v1, "E_SWITZERLAND"

    const/16 v2, 0x18

    invoke-direct {v0, v1, v2}, Lcom/mstar/tv/service/aidl/EN_MEMBER_COUNTRY;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/mstar/tv/service/aidl/EN_MEMBER_COUNTRY;->E_SWITZERLAND:Lcom/mstar/tv/service/aidl/EN_MEMBER_COUNTRY;

    new-instance v0, Lcom/mstar/tv/service/aidl/EN_MEMBER_COUNTRY;

    const-string v1, "E_UK"

    const/16 v2, 0x19

    invoke-direct {v0, v1, v2}, Lcom/mstar/tv/service/aidl/EN_MEMBER_COUNTRY;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/mstar/tv/service/aidl/EN_MEMBER_COUNTRY;->E_UK:Lcom/mstar/tv/service/aidl/EN_MEMBER_COUNTRY;

    new-instance v0, Lcom/mstar/tv/service/aidl/EN_MEMBER_COUNTRY;

    const-string v1, "E_NEWZEALAND"

    const/16 v2, 0x1a

    invoke-direct {v0, v1, v2}, Lcom/mstar/tv/service/aidl/EN_MEMBER_COUNTRY;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/mstar/tv/service/aidl/EN_MEMBER_COUNTRY;->E_NEWZEALAND:Lcom/mstar/tv/service/aidl/EN_MEMBER_COUNTRY;

    new-instance v0, Lcom/mstar/tv/service/aidl/EN_MEMBER_COUNTRY;

    const-string v1, "E_ARAB"

    const/16 v2, 0x1b

    invoke-direct {v0, v1, v2}, Lcom/mstar/tv/service/aidl/EN_MEMBER_COUNTRY;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/mstar/tv/service/aidl/EN_MEMBER_COUNTRY;->E_ARAB:Lcom/mstar/tv/service/aidl/EN_MEMBER_COUNTRY;

    new-instance v0, Lcom/mstar/tv/service/aidl/EN_MEMBER_COUNTRY;

    const-string v1, "E_ESTONIA"

    const/16 v2, 0x1c

    invoke-direct {v0, v1, v2}, Lcom/mstar/tv/service/aidl/EN_MEMBER_COUNTRY;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/mstar/tv/service/aidl/EN_MEMBER_COUNTRY;->E_ESTONIA:Lcom/mstar/tv/service/aidl/EN_MEMBER_COUNTRY;

    new-instance v0, Lcom/mstar/tv/service/aidl/EN_MEMBER_COUNTRY;

    const-string v1, "E_HEBREW"

    const/16 v2, 0x1d

    invoke-direct {v0, v1, v2}, Lcom/mstar/tv/service/aidl/EN_MEMBER_COUNTRY;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/mstar/tv/service/aidl/EN_MEMBER_COUNTRY;->E_HEBREW:Lcom/mstar/tv/service/aidl/EN_MEMBER_COUNTRY;

    new-instance v0, Lcom/mstar/tv/service/aidl/EN_MEMBER_COUNTRY;

    const-string v1, "E_LATVIA"

    const/16 v2, 0x1e

    invoke-direct {v0, v1, v2}, Lcom/mstar/tv/service/aidl/EN_MEMBER_COUNTRY;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/mstar/tv/service/aidl/EN_MEMBER_COUNTRY;->E_LATVIA:Lcom/mstar/tv/service/aidl/EN_MEMBER_COUNTRY;

    new-instance v0, Lcom/mstar/tv/service/aidl/EN_MEMBER_COUNTRY;

    const-string v1, "E_SLOVAKIA"

    const/16 v2, 0x1f

    invoke-direct {v0, v1, v2}, Lcom/mstar/tv/service/aidl/EN_MEMBER_COUNTRY;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/mstar/tv/service/aidl/EN_MEMBER_COUNTRY;->E_SLOVAKIA:Lcom/mstar/tv/service/aidl/EN_MEMBER_COUNTRY;

    new-instance v0, Lcom/mstar/tv/service/aidl/EN_MEMBER_COUNTRY;

    const-string v1, "E_TURKEY"

    const/16 v2, 0x20

    invoke-direct {v0, v1, v2}, Lcom/mstar/tv/service/aidl/EN_MEMBER_COUNTRY;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/mstar/tv/service/aidl/EN_MEMBER_COUNTRY;->E_TURKEY:Lcom/mstar/tv/service/aidl/EN_MEMBER_COUNTRY;

    new-instance v0, Lcom/mstar/tv/service/aidl/EN_MEMBER_COUNTRY;

    const-string v1, "E_IRELAND"

    const/16 v2, 0x21

    invoke-direct {v0, v1, v2}, Lcom/mstar/tv/service/aidl/EN_MEMBER_COUNTRY;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/mstar/tv/service/aidl/EN_MEMBER_COUNTRY;->E_IRELAND:Lcom/mstar/tv/service/aidl/EN_MEMBER_COUNTRY;

    new-instance v0, Lcom/mstar/tv/service/aidl/EN_MEMBER_COUNTRY;

    const-string v1, "E_CHINA"

    const/16 v2, 0x22

    invoke-direct {v0, v1, v2}, Lcom/mstar/tv/service/aidl/EN_MEMBER_COUNTRY;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/mstar/tv/service/aidl/EN_MEMBER_COUNTRY;->E_CHINA:Lcom/mstar/tv/service/aidl/EN_MEMBER_COUNTRY;

    new-instance v0, Lcom/mstar/tv/service/aidl/EN_MEMBER_COUNTRY;

    const-string v1, "E_TAIWAN"

    const/16 v2, 0x23

    invoke-direct {v0, v1, v2}, Lcom/mstar/tv/service/aidl/EN_MEMBER_COUNTRY;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/mstar/tv/service/aidl/EN_MEMBER_COUNTRY;->E_TAIWAN:Lcom/mstar/tv/service/aidl/EN_MEMBER_COUNTRY;

    new-instance v0, Lcom/mstar/tv/service/aidl/EN_MEMBER_COUNTRY;

    const-string v1, "E_BRAZIL"

    const/16 v2, 0x24

    invoke-direct {v0, v1, v2}, Lcom/mstar/tv/service/aidl/EN_MEMBER_COUNTRY;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/mstar/tv/service/aidl/EN_MEMBER_COUNTRY;->E_BRAZIL:Lcom/mstar/tv/service/aidl/EN_MEMBER_COUNTRY;

    new-instance v0, Lcom/mstar/tv/service/aidl/EN_MEMBER_COUNTRY;

    const-string v1, "E_CANADA"

    const/16 v2, 0x25

    invoke-direct {v0, v1, v2}, Lcom/mstar/tv/service/aidl/EN_MEMBER_COUNTRY;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/mstar/tv/service/aidl/EN_MEMBER_COUNTRY;->E_CANADA:Lcom/mstar/tv/service/aidl/EN_MEMBER_COUNTRY;

    new-instance v0, Lcom/mstar/tv/service/aidl/EN_MEMBER_COUNTRY;

    const-string v1, "E_MEXICO"

    const/16 v2, 0x26

    invoke-direct {v0, v1, v2}, Lcom/mstar/tv/service/aidl/EN_MEMBER_COUNTRY;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/mstar/tv/service/aidl/EN_MEMBER_COUNTRY;->E_MEXICO:Lcom/mstar/tv/service/aidl/EN_MEMBER_COUNTRY;

    new-instance v0, Lcom/mstar/tv/service/aidl/EN_MEMBER_COUNTRY;

    const-string v1, "E_US"

    const/16 v2, 0x27

    invoke-direct {v0, v1, v2}, Lcom/mstar/tv/service/aidl/EN_MEMBER_COUNTRY;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/mstar/tv/service/aidl/EN_MEMBER_COUNTRY;->E_US:Lcom/mstar/tv/service/aidl/EN_MEMBER_COUNTRY;

    new-instance v0, Lcom/mstar/tv/service/aidl/EN_MEMBER_COUNTRY;

    const-string v1, "E_SOUTHKOREA"

    const/16 v2, 0x28

    invoke-direct {v0, v1, v2}, Lcom/mstar/tv/service/aidl/EN_MEMBER_COUNTRY;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/mstar/tv/service/aidl/EN_MEMBER_COUNTRY;->E_SOUTHKOREA:Lcom/mstar/tv/service/aidl/EN_MEMBER_COUNTRY;

    new-instance v0, Lcom/mstar/tv/service/aidl/EN_MEMBER_COUNTRY;

    const-string v1, "E_OTHERS"

    const/16 v2, 0x29

    invoke-direct {v0, v1, v2}, Lcom/mstar/tv/service/aidl/EN_MEMBER_COUNTRY;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/mstar/tv/service/aidl/EN_MEMBER_COUNTRY;->E_OTHERS:Lcom/mstar/tv/service/aidl/EN_MEMBER_COUNTRY;

    new-instance v0, Lcom/mstar/tv/service/aidl/EN_MEMBER_COUNTRY;

    const-string v1, "E_COUNTRY_NUM"

    const/16 v2, 0x2a

    invoke-direct {v0, v1, v2}, Lcom/mstar/tv/service/aidl/EN_MEMBER_COUNTRY;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/mstar/tv/service/aidl/EN_MEMBER_COUNTRY;->E_COUNTRY_NUM:Lcom/mstar/tv/service/aidl/EN_MEMBER_COUNTRY;

    const/16 v0, 0x2b

    new-array v0, v0, [Lcom/mstar/tv/service/aidl/EN_MEMBER_COUNTRY;

    sget-object v1, Lcom/mstar/tv/service/aidl/EN_MEMBER_COUNTRY;->E_AUSTRALIA:Lcom/mstar/tv/service/aidl/EN_MEMBER_COUNTRY;

    aput-object v1, v0, v3

    sget-object v1, Lcom/mstar/tv/service/aidl/EN_MEMBER_COUNTRY;->E_AUSTRIA:Lcom/mstar/tv/service/aidl/EN_MEMBER_COUNTRY;

    aput-object v1, v0, v4

    sget-object v1, Lcom/mstar/tv/service/aidl/EN_MEMBER_COUNTRY;->E_BELGIUM:Lcom/mstar/tv/service/aidl/EN_MEMBER_COUNTRY;

    aput-object v1, v0, v5

    sget-object v1, Lcom/mstar/tv/service/aidl/EN_MEMBER_COUNTRY;->E_BULGARIA:Lcom/mstar/tv/service/aidl/EN_MEMBER_COUNTRY;

    aput-object v1, v0, v6

    sget-object v1, Lcom/mstar/tv/service/aidl/EN_MEMBER_COUNTRY;->E_CROATIA:Lcom/mstar/tv/service/aidl/EN_MEMBER_COUNTRY;

    aput-object v1, v0, v7

    const/4 v1, 0x5

    sget-object v2, Lcom/mstar/tv/service/aidl/EN_MEMBER_COUNTRY;->E_CZECH:Lcom/mstar/tv/service/aidl/EN_MEMBER_COUNTRY;

    aput-object v2, v0, v1

    const/4 v1, 0x6

    sget-object v2, Lcom/mstar/tv/service/aidl/EN_MEMBER_COUNTRY;->E_DENMARK:Lcom/mstar/tv/service/aidl/EN_MEMBER_COUNTRY;

    aput-object v2, v0, v1

    const/4 v1, 0x7

    sget-object v2, Lcom/mstar/tv/service/aidl/EN_MEMBER_COUNTRY;->E_FINLAND:Lcom/mstar/tv/service/aidl/EN_MEMBER_COUNTRY;

    aput-object v2, v0, v1

    const/16 v1, 0x8

    sget-object v2, Lcom/mstar/tv/service/aidl/EN_MEMBER_COUNTRY;->E_FRANCE:Lcom/mstar/tv/service/aidl/EN_MEMBER_COUNTRY;

    aput-object v2, v0, v1

    const/16 v1, 0x9

    sget-object v2, Lcom/mstar/tv/service/aidl/EN_MEMBER_COUNTRY;->E_GERMANY:Lcom/mstar/tv/service/aidl/EN_MEMBER_COUNTRY;

    aput-object v2, v0, v1

    const/16 v1, 0xa

    sget-object v2, Lcom/mstar/tv/service/aidl/EN_MEMBER_COUNTRY;->E_GREECE:Lcom/mstar/tv/service/aidl/EN_MEMBER_COUNTRY;

    aput-object v2, v0, v1

    const/16 v1, 0xb

    sget-object v2, Lcom/mstar/tv/service/aidl/EN_MEMBER_COUNTRY;->E_HUNGARY:Lcom/mstar/tv/service/aidl/EN_MEMBER_COUNTRY;

    aput-object v2, v0, v1

    const/16 v1, 0xc

    sget-object v2, Lcom/mstar/tv/service/aidl/EN_MEMBER_COUNTRY;->E_ITALY:Lcom/mstar/tv/service/aidl/EN_MEMBER_COUNTRY;

    aput-object v2, v0, v1

    const/16 v1, 0xd

    sget-object v2, Lcom/mstar/tv/service/aidl/EN_MEMBER_COUNTRY;->E_LUXEMBOURG:Lcom/mstar/tv/service/aidl/EN_MEMBER_COUNTRY;

    aput-object v2, v0, v1

    const/16 v1, 0xe

    sget-object v2, Lcom/mstar/tv/service/aidl/EN_MEMBER_COUNTRY;->E_NETHERLANDS:Lcom/mstar/tv/service/aidl/EN_MEMBER_COUNTRY;

    aput-object v2, v0, v1

    const/16 v1, 0xf

    sget-object v2, Lcom/mstar/tv/service/aidl/EN_MEMBER_COUNTRY;->E_NORWAY:Lcom/mstar/tv/service/aidl/EN_MEMBER_COUNTRY;

    aput-object v2, v0, v1

    const/16 v1, 0x10

    sget-object v2, Lcom/mstar/tv/service/aidl/EN_MEMBER_COUNTRY;->E_POLAND:Lcom/mstar/tv/service/aidl/EN_MEMBER_COUNTRY;

    aput-object v2, v0, v1

    const/16 v1, 0x11

    sget-object v2, Lcom/mstar/tv/service/aidl/EN_MEMBER_COUNTRY;->E_PORTUGAL:Lcom/mstar/tv/service/aidl/EN_MEMBER_COUNTRY;

    aput-object v2, v0, v1

    const/16 v1, 0x12

    sget-object v2, Lcom/mstar/tv/service/aidl/EN_MEMBER_COUNTRY;->E_RUMANIA:Lcom/mstar/tv/service/aidl/EN_MEMBER_COUNTRY;

    aput-object v2, v0, v1

    const/16 v1, 0x13

    sget-object v2, Lcom/mstar/tv/service/aidl/EN_MEMBER_COUNTRY;->E_RUSSIA:Lcom/mstar/tv/service/aidl/EN_MEMBER_COUNTRY;

    aput-object v2, v0, v1

    const/16 v1, 0x14

    sget-object v2, Lcom/mstar/tv/service/aidl/EN_MEMBER_COUNTRY;->E_SERBIA:Lcom/mstar/tv/service/aidl/EN_MEMBER_COUNTRY;

    aput-object v2, v0, v1

    const/16 v1, 0x15

    sget-object v2, Lcom/mstar/tv/service/aidl/EN_MEMBER_COUNTRY;->E_SLOVENIA:Lcom/mstar/tv/service/aidl/EN_MEMBER_COUNTRY;

    aput-object v2, v0, v1

    const/16 v1, 0x16

    sget-object v2, Lcom/mstar/tv/service/aidl/EN_MEMBER_COUNTRY;->E_SPAIN:Lcom/mstar/tv/service/aidl/EN_MEMBER_COUNTRY;

    aput-object v2, v0, v1

    const/16 v1, 0x17

    sget-object v2, Lcom/mstar/tv/service/aidl/EN_MEMBER_COUNTRY;->E_SWEDEN:Lcom/mstar/tv/service/aidl/EN_MEMBER_COUNTRY;

    aput-object v2, v0, v1

    const/16 v1, 0x18

    sget-object v2, Lcom/mstar/tv/service/aidl/EN_MEMBER_COUNTRY;->E_SWITZERLAND:Lcom/mstar/tv/service/aidl/EN_MEMBER_COUNTRY;

    aput-object v2, v0, v1

    const/16 v1, 0x19

    sget-object v2, Lcom/mstar/tv/service/aidl/EN_MEMBER_COUNTRY;->E_UK:Lcom/mstar/tv/service/aidl/EN_MEMBER_COUNTRY;

    aput-object v2, v0, v1

    const/16 v1, 0x1a

    sget-object v2, Lcom/mstar/tv/service/aidl/EN_MEMBER_COUNTRY;->E_NEWZEALAND:Lcom/mstar/tv/service/aidl/EN_MEMBER_COUNTRY;

    aput-object v2, v0, v1

    const/16 v1, 0x1b

    sget-object v2, Lcom/mstar/tv/service/aidl/EN_MEMBER_COUNTRY;->E_ARAB:Lcom/mstar/tv/service/aidl/EN_MEMBER_COUNTRY;

    aput-object v2, v0, v1

    const/16 v1, 0x1c

    sget-object v2, Lcom/mstar/tv/service/aidl/EN_MEMBER_COUNTRY;->E_ESTONIA:Lcom/mstar/tv/service/aidl/EN_MEMBER_COUNTRY;

    aput-object v2, v0, v1

    const/16 v1, 0x1d

    sget-object v2, Lcom/mstar/tv/service/aidl/EN_MEMBER_COUNTRY;->E_HEBREW:Lcom/mstar/tv/service/aidl/EN_MEMBER_COUNTRY;

    aput-object v2, v0, v1

    const/16 v1, 0x1e

    sget-object v2, Lcom/mstar/tv/service/aidl/EN_MEMBER_COUNTRY;->E_LATVIA:Lcom/mstar/tv/service/aidl/EN_MEMBER_COUNTRY;

    aput-object v2, v0, v1

    const/16 v1, 0x1f

    sget-object v2, Lcom/mstar/tv/service/aidl/EN_MEMBER_COUNTRY;->E_SLOVAKIA:Lcom/mstar/tv/service/aidl/EN_MEMBER_COUNTRY;

    aput-object v2, v0, v1

    const/16 v1, 0x20

    sget-object v2, Lcom/mstar/tv/service/aidl/EN_MEMBER_COUNTRY;->E_TURKEY:Lcom/mstar/tv/service/aidl/EN_MEMBER_COUNTRY;

    aput-object v2, v0, v1

    const/16 v1, 0x21

    sget-object v2, Lcom/mstar/tv/service/aidl/EN_MEMBER_COUNTRY;->E_IRELAND:Lcom/mstar/tv/service/aidl/EN_MEMBER_COUNTRY;

    aput-object v2, v0, v1

    const/16 v1, 0x22

    sget-object v2, Lcom/mstar/tv/service/aidl/EN_MEMBER_COUNTRY;->E_CHINA:Lcom/mstar/tv/service/aidl/EN_MEMBER_COUNTRY;

    aput-object v2, v0, v1

    const/16 v1, 0x23

    sget-object v2, Lcom/mstar/tv/service/aidl/EN_MEMBER_COUNTRY;->E_TAIWAN:Lcom/mstar/tv/service/aidl/EN_MEMBER_COUNTRY;

    aput-object v2, v0, v1

    const/16 v1, 0x24

    sget-object v2, Lcom/mstar/tv/service/aidl/EN_MEMBER_COUNTRY;->E_BRAZIL:Lcom/mstar/tv/service/aidl/EN_MEMBER_COUNTRY;

    aput-object v2, v0, v1

    const/16 v1, 0x25

    sget-object v2, Lcom/mstar/tv/service/aidl/EN_MEMBER_COUNTRY;->E_CANADA:Lcom/mstar/tv/service/aidl/EN_MEMBER_COUNTRY;

    aput-object v2, v0, v1

    const/16 v1, 0x26

    sget-object v2, Lcom/mstar/tv/service/aidl/EN_MEMBER_COUNTRY;->E_MEXICO:Lcom/mstar/tv/service/aidl/EN_MEMBER_COUNTRY;

    aput-object v2, v0, v1

    const/16 v1, 0x27

    sget-object v2, Lcom/mstar/tv/service/aidl/EN_MEMBER_COUNTRY;->E_US:Lcom/mstar/tv/service/aidl/EN_MEMBER_COUNTRY;

    aput-object v2, v0, v1

    const/16 v1, 0x28

    sget-object v2, Lcom/mstar/tv/service/aidl/EN_MEMBER_COUNTRY;->E_SOUTHKOREA:Lcom/mstar/tv/service/aidl/EN_MEMBER_COUNTRY;

    aput-object v2, v0, v1

    const/16 v1, 0x29

    sget-object v2, Lcom/mstar/tv/service/aidl/EN_MEMBER_COUNTRY;->E_OTHERS:Lcom/mstar/tv/service/aidl/EN_MEMBER_COUNTRY;

    aput-object v2, v0, v1

    const/16 v1, 0x2a

    sget-object v2, Lcom/mstar/tv/service/aidl/EN_MEMBER_COUNTRY;->E_COUNTRY_NUM:Lcom/mstar/tv/service/aidl/EN_MEMBER_COUNTRY;

    aput-object v2, v0, v1

    sput-object v0, Lcom/mstar/tv/service/aidl/EN_MEMBER_COUNTRY;->ENUM$VALUES:[Lcom/mstar/tv/service/aidl/EN_MEMBER_COUNTRY;

    new-instance v0, Lcom/mstar/tv/service/aidl/EN_MEMBER_COUNTRY$1;

    invoke-direct {v0}, Lcom/mstar/tv/service/aidl/EN_MEMBER_COUNTRY$1;-><init>()V

    sput-object v0, Lcom/mstar/tv/service/aidl/EN_MEMBER_COUNTRY;->CREATOR:Landroid/os/Parcelable$Creator;

    return-void
.end method

.method private constructor <init>(Ljava/lang/String;I)V
    .locals 0

    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    return-void
.end method

.method public static valueOf(Ljava/lang/String;)Lcom/mstar/tv/service/aidl/EN_MEMBER_COUNTRY;
    .locals 1

    const-class v0, Lcom/mstar/tv/service/aidl/EN_MEMBER_COUNTRY;

    invoke-static {v0, p0}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;

    move-result-object v0

    check-cast v0, Lcom/mstar/tv/service/aidl/EN_MEMBER_COUNTRY;

    return-object v0
.end method

.method public static values()[Lcom/mstar/tv/service/aidl/EN_MEMBER_COUNTRY;
    .locals 4

    const/4 v3, 0x0

    sget-object v0, Lcom/mstar/tv/service/aidl/EN_MEMBER_COUNTRY;->ENUM$VALUES:[Lcom/mstar/tv/service/aidl/EN_MEMBER_COUNTRY;

    array-length v1, v0

    new-array v2, v1, [Lcom/mstar/tv/service/aidl/EN_MEMBER_COUNTRY;

    invoke-static {v0, v3, v2, v3, v1}, Ljava/lang/System;->arraycopy(Ljava/lang/Object;ILjava/lang/Object;II)V

    return-object v2
.end method


# virtual methods
.method public describeContents()I
    .locals 1

    const/4 v0, 0x0

    return v0
.end method

.method public writeToParcel(Landroid/os/Parcel;I)V
    .locals 1
    .param p1    # Landroid/os/Parcel;
    .param p2    # I

    invoke-virtual {p0}, Lcom/mstar/tv/service/aidl/EN_MEMBER_COUNTRY;->ordinal()I

    move-result v0

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeInt(I)V

    return-void
.end method
