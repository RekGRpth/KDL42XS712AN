.class public Lcom/mstar/tv/service/aidl/DvbcScanParam;
.super Ljava/lang/Object;
.source "DvbcScanParam.java"

# interfaces
.implements Landroid/os/Parcelable;


# static fields
.field public static final CREATOR:Landroid/os/Parcelable$Creator;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Landroid/os/Parcelable$Creator",
            "<",
            "Lcom/mstar/tv/service/aidl/DvbcScanParam;",
            ">;"
        }
    .end annotation
.end field


# instance fields
.field public QAM_Type:Lcom/mstar/tv/service/aidl/EN_CAB_CONSTEL_TYPE;

.field public u16SymbolRate:S

.field public u32NITFrequency:I


# direct methods
.method static constructor <clinit>()V
    .locals 1

    new-instance v0, Lcom/mstar/tv/service/aidl/DvbcScanParam$1;

    invoke-direct {v0}, Lcom/mstar/tv/service/aidl/DvbcScanParam$1;-><init>()V

    sput-object v0, Lcom/mstar/tv/service/aidl/DvbcScanParam;->CREATOR:Landroid/os/Parcelable$Creator;

    return-void
.end method

.method public constructor <init>()V
    .locals 2

    const/4 v1, 0x0

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    iput-short v1, p0, Lcom/mstar/tv/service/aidl/DvbcScanParam;->u16SymbolRate:S

    const/4 v0, 0x0

    iput-object v0, p0, Lcom/mstar/tv/service/aidl/DvbcScanParam;->QAM_Type:Lcom/mstar/tv/service/aidl/EN_CAB_CONSTEL_TYPE;

    iput v1, p0, Lcom/mstar/tv/service/aidl/DvbcScanParam;->u32NITFrequency:I

    return-void
.end method

.method private constructor <init>(Landroid/os/Parcel;)V
    .locals 2
    .param p1    # Landroid/os/Parcel;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    invoke-virtual {p1}, Landroid/os/Parcel;->readInt()I

    move-result v0

    int-to-short v0, v0

    iput-short v0, p0, Lcom/mstar/tv/service/aidl/DvbcScanParam;->u16SymbolRate:S

    invoke-static {}, Lcom/mstar/tv/service/aidl/EN_CAB_CONSTEL_TYPE;->values()[Lcom/mstar/tv/service/aidl/EN_CAB_CONSTEL_TYPE;

    move-result-object v0

    invoke-virtual {p1}, Landroid/os/Parcel;->readInt()I

    move-result v1

    aget-object v0, v0, v1

    iput-object v0, p0, Lcom/mstar/tv/service/aidl/DvbcScanParam;->QAM_Type:Lcom/mstar/tv/service/aidl/EN_CAB_CONSTEL_TYPE;

    invoke-virtual {p1}, Landroid/os/Parcel;->readInt()I

    move-result v0

    iput v0, p0, Lcom/mstar/tv/service/aidl/DvbcScanParam;->u32NITFrequency:I

    return-void
.end method

.method synthetic constructor <init>(Landroid/os/Parcel;Lcom/mstar/tv/service/aidl/DvbcScanParam;)V
    .locals 0

    invoke-direct {p0, p1}, Lcom/mstar/tv/service/aidl/DvbcScanParam;-><init>(Landroid/os/Parcel;)V

    return-void
.end method

.method public constructor <init>(SLcom/mstar/tv/service/aidl/EN_CAB_CONSTEL_TYPE;I)V
    .locals 0
    .param p1    # S
    .param p2    # Lcom/mstar/tv/service/aidl/EN_CAB_CONSTEL_TYPE;
    .param p3    # I

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    iput-short p1, p0, Lcom/mstar/tv/service/aidl/DvbcScanParam;->u16SymbolRate:S

    iput-object p2, p0, Lcom/mstar/tv/service/aidl/DvbcScanParam;->QAM_Type:Lcom/mstar/tv/service/aidl/EN_CAB_CONSTEL_TYPE;

    iput p3, p0, Lcom/mstar/tv/service/aidl/DvbcScanParam;->u32NITFrequency:I

    return-void
.end method


# virtual methods
.method public describeContents()I
    .locals 1

    const/4 v0, 0x0

    return v0
.end method

.method public writeToParcel(Landroid/os/Parcel;I)V
    .locals 1
    .param p1    # Landroid/os/Parcel;
    .param p2    # I

    iget-short v0, p0, Lcom/mstar/tv/service/aidl/DvbcScanParam;->u16SymbolRate:S

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeInt(I)V

    iget-object v0, p0, Lcom/mstar/tv/service/aidl/DvbcScanParam;->QAM_Type:Lcom/mstar/tv/service/aidl/EN_CAB_CONSTEL_TYPE;

    invoke-virtual {v0}, Lcom/mstar/tv/service/aidl/EN_CAB_CONSTEL_TYPE;->ordinal()I

    move-result v0

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeInt(I)V

    iget v0, p0, Lcom/mstar/tv/service/aidl/DvbcScanParam;->u32NITFrequency:I

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeInt(I)V

    return-void
.end method
