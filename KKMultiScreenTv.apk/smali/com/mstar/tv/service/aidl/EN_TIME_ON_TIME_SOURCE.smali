.class public final enum Lcom/mstar/tv/service/aidl/EN_TIME_ON_TIME_SOURCE;
.super Ljava/lang/Enum;
.source "EN_TIME_ON_TIME_SOURCE.java"

# interfaces
.implements Landroid/os/Parcelable;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Enum",
        "<",
        "Lcom/mstar/tv/service/aidl/EN_TIME_ON_TIME_SOURCE;",
        ">;",
        "Landroid/os/Parcelable;"
    }
.end annotation


# static fields
.field public static final CREATOR:Landroid/os/Parcelable$Creator;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Landroid/os/Parcelable$Creator",
            "<",
            "Lcom/mstar/tv/service/aidl/EN_TIME_ON_TIME_SOURCE;",
            ">;"
        }
    .end annotation
.end field

.field private static final synthetic ENUM$VALUES:[Lcom/mstar/tv/service/aidl/EN_TIME_ON_TIME_SOURCE;

.field public static final enum EN_Time_OnTimer_Source_ATV:Lcom/mstar/tv/service/aidl/EN_TIME_ON_TIME_SOURCE;

.field public static final enum EN_Time_OnTimer_Source_AV:Lcom/mstar/tv/service/aidl/EN_TIME_ON_TIME_SOURCE;

.field public static final enum EN_Time_OnTimer_Source_AV2:Lcom/mstar/tv/service/aidl/EN_TIME_ON_TIME_SOURCE;

.field public static final enum EN_Time_OnTimer_Source_AV3:Lcom/mstar/tv/service/aidl/EN_TIME_ON_TIME_SOURCE;

.field public static final enum EN_Time_OnTimer_Source_COMPONENT:Lcom/mstar/tv/service/aidl/EN_TIME_ON_TIME_SOURCE;

.field public static final enum EN_Time_OnTimer_Source_COMPONENT2:Lcom/mstar/tv/service/aidl/EN_TIME_ON_TIME_SOURCE;

.field public static final enum EN_Time_OnTimer_Source_DATA:Lcom/mstar/tv/service/aidl/EN_TIME_ON_TIME_SOURCE;

.field public static final enum EN_Time_OnTimer_Source_DLNA:Lcom/mstar/tv/service/aidl/EN_TIME_ON_TIME_SOURCE;

.field public static final enum EN_Time_OnTimer_Source_DTV:Lcom/mstar/tv/service/aidl/EN_TIME_ON_TIME_SOURCE;

.field public static final enum EN_Time_OnTimer_Source_HDMI:Lcom/mstar/tv/service/aidl/EN_TIME_ON_TIME_SOURCE;

.field public static final enum EN_Time_OnTimer_Source_HDMI2:Lcom/mstar/tv/service/aidl/EN_TIME_ON_TIME_SOURCE;

.field public static final enum EN_Time_OnTimer_Source_HDMI3:Lcom/mstar/tv/service/aidl/EN_TIME_ON_TIME_SOURCE;

.field public static final enum EN_Time_OnTimer_Source_HDMI4:Lcom/mstar/tv/service/aidl/EN_TIME_ON_TIME_SOURCE;

.field public static final enum EN_Time_OnTimer_Source_HOME_PAGE:Lcom/mstar/tv/service/aidl/EN_TIME_ON_TIME_SOURCE;

.field public static final enum EN_Time_OnTimer_Source_MPLAYER:Lcom/mstar/tv/service/aidl/EN_TIME_ON_TIME_SOURCE;

.field public static final enum EN_Time_OnTimer_Source_NUM:Lcom/mstar/tv/service/aidl/EN_TIME_ON_TIME_SOURCE;

.field public static final enum EN_Time_OnTimer_Source_RADIO:Lcom/mstar/tv/service/aidl/EN_TIME_ON_TIME_SOURCE;

.field public static final enum EN_Time_OnTimer_Source_REMEMBER:Lcom/mstar/tv/service/aidl/EN_TIME_ON_TIME_SOURCE;

.field public static final enum EN_Time_OnTimer_Source_RGB:Lcom/mstar/tv/service/aidl/EN_TIME_ON_TIME_SOURCE;

.field public static final enum EN_Time_OnTimer_Source_SCART:Lcom/mstar/tv/service/aidl/EN_TIME_ON_TIME_SOURCE;

.field public static final enum EN_Time_OnTimer_Source_SCART2:Lcom/mstar/tv/service/aidl/EN_TIME_ON_TIME_SOURCE;

.field public static final enum EN_Time_OnTimer_Source_SVIDEO:Lcom/mstar/tv/service/aidl/EN_TIME_ON_TIME_SOURCE;

.field public static final enum EN_Time_OnTimer_Source_SVIDEO2:Lcom/mstar/tv/service/aidl/EN_TIME_ON_TIME_SOURCE;


# direct methods
.method static constructor <clinit>()V
    .locals 8

    const/4 v7, 0x4

    const/4 v6, 0x3

    const/4 v5, 0x2

    const/4 v4, 0x1

    const/4 v3, 0x0

    new-instance v0, Lcom/mstar/tv/service/aidl/EN_TIME_ON_TIME_SOURCE;

    const-string v1, "EN_Time_OnTimer_Source_REMEMBER"

    invoke-direct {v0, v1, v3}, Lcom/mstar/tv/service/aidl/EN_TIME_ON_TIME_SOURCE;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/mstar/tv/service/aidl/EN_TIME_ON_TIME_SOURCE;->EN_Time_OnTimer_Source_REMEMBER:Lcom/mstar/tv/service/aidl/EN_TIME_ON_TIME_SOURCE;

    new-instance v0, Lcom/mstar/tv/service/aidl/EN_TIME_ON_TIME_SOURCE;

    const-string v1, "EN_Time_OnTimer_Source_HOME_PAGE"

    invoke-direct {v0, v1, v4}, Lcom/mstar/tv/service/aidl/EN_TIME_ON_TIME_SOURCE;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/mstar/tv/service/aidl/EN_TIME_ON_TIME_SOURCE;->EN_Time_OnTimer_Source_HOME_PAGE:Lcom/mstar/tv/service/aidl/EN_TIME_ON_TIME_SOURCE;

    new-instance v0, Lcom/mstar/tv/service/aidl/EN_TIME_ON_TIME_SOURCE;

    const-string v1, "EN_Time_OnTimer_Source_DTV"

    invoke-direct {v0, v1, v5}, Lcom/mstar/tv/service/aidl/EN_TIME_ON_TIME_SOURCE;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/mstar/tv/service/aidl/EN_TIME_ON_TIME_SOURCE;->EN_Time_OnTimer_Source_DTV:Lcom/mstar/tv/service/aidl/EN_TIME_ON_TIME_SOURCE;

    new-instance v0, Lcom/mstar/tv/service/aidl/EN_TIME_ON_TIME_SOURCE;

    const-string v1, "EN_Time_OnTimer_Source_ATV"

    invoke-direct {v0, v1, v6}, Lcom/mstar/tv/service/aidl/EN_TIME_ON_TIME_SOURCE;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/mstar/tv/service/aidl/EN_TIME_ON_TIME_SOURCE;->EN_Time_OnTimer_Source_ATV:Lcom/mstar/tv/service/aidl/EN_TIME_ON_TIME_SOURCE;

    new-instance v0, Lcom/mstar/tv/service/aidl/EN_TIME_ON_TIME_SOURCE;

    const-string v1, "EN_Time_OnTimer_Source_RADIO"

    invoke-direct {v0, v1, v7}, Lcom/mstar/tv/service/aidl/EN_TIME_ON_TIME_SOURCE;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/mstar/tv/service/aidl/EN_TIME_ON_TIME_SOURCE;->EN_Time_OnTimer_Source_RADIO:Lcom/mstar/tv/service/aidl/EN_TIME_ON_TIME_SOURCE;

    new-instance v0, Lcom/mstar/tv/service/aidl/EN_TIME_ON_TIME_SOURCE;

    const-string v1, "EN_Time_OnTimer_Source_DATA"

    const/4 v2, 0x5

    invoke-direct {v0, v1, v2}, Lcom/mstar/tv/service/aidl/EN_TIME_ON_TIME_SOURCE;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/mstar/tv/service/aidl/EN_TIME_ON_TIME_SOURCE;->EN_Time_OnTimer_Source_DATA:Lcom/mstar/tv/service/aidl/EN_TIME_ON_TIME_SOURCE;

    new-instance v0, Lcom/mstar/tv/service/aidl/EN_TIME_ON_TIME_SOURCE;

    const-string v1, "EN_Time_OnTimer_Source_SCART"

    const/4 v2, 0x6

    invoke-direct {v0, v1, v2}, Lcom/mstar/tv/service/aidl/EN_TIME_ON_TIME_SOURCE;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/mstar/tv/service/aidl/EN_TIME_ON_TIME_SOURCE;->EN_Time_OnTimer_Source_SCART:Lcom/mstar/tv/service/aidl/EN_TIME_ON_TIME_SOURCE;

    new-instance v0, Lcom/mstar/tv/service/aidl/EN_TIME_ON_TIME_SOURCE;

    const-string v1, "EN_Time_OnTimer_Source_SCART2"

    const/4 v2, 0x7

    invoke-direct {v0, v1, v2}, Lcom/mstar/tv/service/aidl/EN_TIME_ON_TIME_SOURCE;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/mstar/tv/service/aidl/EN_TIME_ON_TIME_SOURCE;->EN_Time_OnTimer_Source_SCART2:Lcom/mstar/tv/service/aidl/EN_TIME_ON_TIME_SOURCE;

    new-instance v0, Lcom/mstar/tv/service/aidl/EN_TIME_ON_TIME_SOURCE;

    const-string v1, "EN_Time_OnTimer_Source_COMPONENT"

    const/16 v2, 0x8

    invoke-direct {v0, v1, v2}, Lcom/mstar/tv/service/aidl/EN_TIME_ON_TIME_SOURCE;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/mstar/tv/service/aidl/EN_TIME_ON_TIME_SOURCE;->EN_Time_OnTimer_Source_COMPONENT:Lcom/mstar/tv/service/aidl/EN_TIME_ON_TIME_SOURCE;

    new-instance v0, Lcom/mstar/tv/service/aidl/EN_TIME_ON_TIME_SOURCE;

    const-string v1, "EN_Time_OnTimer_Source_COMPONENT2"

    const/16 v2, 0x9

    invoke-direct {v0, v1, v2}, Lcom/mstar/tv/service/aidl/EN_TIME_ON_TIME_SOURCE;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/mstar/tv/service/aidl/EN_TIME_ON_TIME_SOURCE;->EN_Time_OnTimer_Source_COMPONENT2:Lcom/mstar/tv/service/aidl/EN_TIME_ON_TIME_SOURCE;

    new-instance v0, Lcom/mstar/tv/service/aidl/EN_TIME_ON_TIME_SOURCE;

    const-string v1, "EN_Time_OnTimer_Source_RGB"

    const/16 v2, 0xa

    invoke-direct {v0, v1, v2}, Lcom/mstar/tv/service/aidl/EN_TIME_ON_TIME_SOURCE;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/mstar/tv/service/aidl/EN_TIME_ON_TIME_SOURCE;->EN_Time_OnTimer_Source_RGB:Lcom/mstar/tv/service/aidl/EN_TIME_ON_TIME_SOURCE;

    new-instance v0, Lcom/mstar/tv/service/aidl/EN_TIME_ON_TIME_SOURCE;

    const-string v1, "EN_Time_OnTimer_Source_HDMI"

    const/16 v2, 0xb

    invoke-direct {v0, v1, v2}, Lcom/mstar/tv/service/aidl/EN_TIME_ON_TIME_SOURCE;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/mstar/tv/service/aidl/EN_TIME_ON_TIME_SOURCE;->EN_Time_OnTimer_Source_HDMI:Lcom/mstar/tv/service/aidl/EN_TIME_ON_TIME_SOURCE;

    new-instance v0, Lcom/mstar/tv/service/aidl/EN_TIME_ON_TIME_SOURCE;

    const-string v1, "EN_Time_OnTimer_Source_HDMI2"

    const/16 v2, 0xc

    invoke-direct {v0, v1, v2}, Lcom/mstar/tv/service/aidl/EN_TIME_ON_TIME_SOURCE;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/mstar/tv/service/aidl/EN_TIME_ON_TIME_SOURCE;->EN_Time_OnTimer_Source_HDMI2:Lcom/mstar/tv/service/aidl/EN_TIME_ON_TIME_SOURCE;

    new-instance v0, Lcom/mstar/tv/service/aidl/EN_TIME_ON_TIME_SOURCE;

    const-string v1, "EN_Time_OnTimer_Source_HDMI3"

    const/16 v2, 0xd

    invoke-direct {v0, v1, v2}, Lcom/mstar/tv/service/aidl/EN_TIME_ON_TIME_SOURCE;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/mstar/tv/service/aidl/EN_TIME_ON_TIME_SOURCE;->EN_Time_OnTimer_Source_HDMI3:Lcom/mstar/tv/service/aidl/EN_TIME_ON_TIME_SOURCE;

    new-instance v0, Lcom/mstar/tv/service/aidl/EN_TIME_ON_TIME_SOURCE;

    const-string v1, "EN_Time_OnTimer_Source_HDMI4"

    const/16 v2, 0xe

    invoke-direct {v0, v1, v2}, Lcom/mstar/tv/service/aidl/EN_TIME_ON_TIME_SOURCE;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/mstar/tv/service/aidl/EN_TIME_ON_TIME_SOURCE;->EN_Time_OnTimer_Source_HDMI4:Lcom/mstar/tv/service/aidl/EN_TIME_ON_TIME_SOURCE;

    new-instance v0, Lcom/mstar/tv/service/aidl/EN_TIME_ON_TIME_SOURCE;

    const-string v1, "EN_Time_OnTimer_Source_AV"

    const/16 v2, 0xf

    invoke-direct {v0, v1, v2}, Lcom/mstar/tv/service/aidl/EN_TIME_ON_TIME_SOURCE;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/mstar/tv/service/aidl/EN_TIME_ON_TIME_SOURCE;->EN_Time_OnTimer_Source_AV:Lcom/mstar/tv/service/aidl/EN_TIME_ON_TIME_SOURCE;

    new-instance v0, Lcom/mstar/tv/service/aidl/EN_TIME_ON_TIME_SOURCE;

    const-string v1, "EN_Time_OnTimer_Source_AV2"

    const/16 v2, 0x10

    invoke-direct {v0, v1, v2}, Lcom/mstar/tv/service/aidl/EN_TIME_ON_TIME_SOURCE;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/mstar/tv/service/aidl/EN_TIME_ON_TIME_SOURCE;->EN_Time_OnTimer_Source_AV2:Lcom/mstar/tv/service/aidl/EN_TIME_ON_TIME_SOURCE;

    new-instance v0, Lcom/mstar/tv/service/aidl/EN_TIME_ON_TIME_SOURCE;

    const-string v1, "EN_Time_OnTimer_Source_AV3"

    const/16 v2, 0x11

    invoke-direct {v0, v1, v2}, Lcom/mstar/tv/service/aidl/EN_TIME_ON_TIME_SOURCE;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/mstar/tv/service/aidl/EN_TIME_ON_TIME_SOURCE;->EN_Time_OnTimer_Source_AV3:Lcom/mstar/tv/service/aidl/EN_TIME_ON_TIME_SOURCE;

    new-instance v0, Lcom/mstar/tv/service/aidl/EN_TIME_ON_TIME_SOURCE;

    const-string v1, "EN_Time_OnTimer_Source_SVIDEO"

    const/16 v2, 0x12

    invoke-direct {v0, v1, v2}, Lcom/mstar/tv/service/aidl/EN_TIME_ON_TIME_SOURCE;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/mstar/tv/service/aidl/EN_TIME_ON_TIME_SOURCE;->EN_Time_OnTimer_Source_SVIDEO:Lcom/mstar/tv/service/aidl/EN_TIME_ON_TIME_SOURCE;

    new-instance v0, Lcom/mstar/tv/service/aidl/EN_TIME_ON_TIME_SOURCE;

    const-string v1, "EN_Time_OnTimer_Source_SVIDEO2"

    const/16 v2, 0x13

    invoke-direct {v0, v1, v2}, Lcom/mstar/tv/service/aidl/EN_TIME_ON_TIME_SOURCE;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/mstar/tv/service/aidl/EN_TIME_ON_TIME_SOURCE;->EN_Time_OnTimer_Source_SVIDEO2:Lcom/mstar/tv/service/aidl/EN_TIME_ON_TIME_SOURCE;

    new-instance v0, Lcom/mstar/tv/service/aidl/EN_TIME_ON_TIME_SOURCE;

    const-string v1, "EN_Time_OnTimer_Source_MPLAYER"

    const/16 v2, 0x14

    invoke-direct {v0, v1, v2}, Lcom/mstar/tv/service/aidl/EN_TIME_ON_TIME_SOURCE;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/mstar/tv/service/aidl/EN_TIME_ON_TIME_SOURCE;->EN_Time_OnTimer_Source_MPLAYER:Lcom/mstar/tv/service/aidl/EN_TIME_ON_TIME_SOURCE;

    new-instance v0, Lcom/mstar/tv/service/aidl/EN_TIME_ON_TIME_SOURCE;

    const-string v1, "EN_Time_OnTimer_Source_DLNA"

    const/16 v2, 0x15

    invoke-direct {v0, v1, v2}, Lcom/mstar/tv/service/aidl/EN_TIME_ON_TIME_SOURCE;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/mstar/tv/service/aidl/EN_TIME_ON_TIME_SOURCE;->EN_Time_OnTimer_Source_DLNA:Lcom/mstar/tv/service/aidl/EN_TIME_ON_TIME_SOURCE;

    new-instance v0, Lcom/mstar/tv/service/aidl/EN_TIME_ON_TIME_SOURCE;

    const-string v1, "EN_Time_OnTimer_Source_NUM"

    const/16 v2, 0x16

    invoke-direct {v0, v1, v2}, Lcom/mstar/tv/service/aidl/EN_TIME_ON_TIME_SOURCE;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/mstar/tv/service/aidl/EN_TIME_ON_TIME_SOURCE;->EN_Time_OnTimer_Source_NUM:Lcom/mstar/tv/service/aidl/EN_TIME_ON_TIME_SOURCE;

    const/16 v0, 0x17

    new-array v0, v0, [Lcom/mstar/tv/service/aidl/EN_TIME_ON_TIME_SOURCE;

    sget-object v1, Lcom/mstar/tv/service/aidl/EN_TIME_ON_TIME_SOURCE;->EN_Time_OnTimer_Source_REMEMBER:Lcom/mstar/tv/service/aidl/EN_TIME_ON_TIME_SOURCE;

    aput-object v1, v0, v3

    sget-object v1, Lcom/mstar/tv/service/aidl/EN_TIME_ON_TIME_SOURCE;->EN_Time_OnTimer_Source_HOME_PAGE:Lcom/mstar/tv/service/aidl/EN_TIME_ON_TIME_SOURCE;

    aput-object v1, v0, v4

    sget-object v1, Lcom/mstar/tv/service/aidl/EN_TIME_ON_TIME_SOURCE;->EN_Time_OnTimer_Source_DTV:Lcom/mstar/tv/service/aidl/EN_TIME_ON_TIME_SOURCE;

    aput-object v1, v0, v5

    sget-object v1, Lcom/mstar/tv/service/aidl/EN_TIME_ON_TIME_SOURCE;->EN_Time_OnTimer_Source_ATV:Lcom/mstar/tv/service/aidl/EN_TIME_ON_TIME_SOURCE;

    aput-object v1, v0, v6

    sget-object v1, Lcom/mstar/tv/service/aidl/EN_TIME_ON_TIME_SOURCE;->EN_Time_OnTimer_Source_RADIO:Lcom/mstar/tv/service/aidl/EN_TIME_ON_TIME_SOURCE;

    aput-object v1, v0, v7

    const/4 v1, 0x5

    sget-object v2, Lcom/mstar/tv/service/aidl/EN_TIME_ON_TIME_SOURCE;->EN_Time_OnTimer_Source_DATA:Lcom/mstar/tv/service/aidl/EN_TIME_ON_TIME_SOURCE;

    aput-object v2, v0, v1

    const/4 v1, 0x6

    sget-object v2, Lcom/mstar/tv/service/aidl/EN_TIME_ON_TIME_SOURCE;->EN_Time_OnTimer_Source_SCART:Lcom/mstar/tv/service/aidl/EN_TIME_ON_TIME_SOURCE;

    aput-object v2, v0, v1

    const/4 v1, 0x7

    sget-object v2, Lcom/mstar/tv/service/aidl/EN_TIME_ON_TIME_SOURCE;->EN_Time_OnTimer_Source_SCART2:Lcom/mstar/tv/service/aidl/EN_TIME_ON_TIME_SOURCE;

    aput-object v2, v0, v1

    const/16 v1, 0x8

    sget-object v2, Lcom/mstar/tv/service/aidl/EN_TIME_ON_TIME_SOURCE;->EN_Time_OnTimer_Source_COMPONENT:Lcom/mstar/tv/service/aidl/EN_TIME_ON_TIME_SOURCE;

    aput-object v2, v0, v1

    const/16 v1, 0x9

    sget-object v2, Lcom/mstar/tv/service/aidl/EN_TIME_ON_TIME_SOURCE;->EN_Time_OnTimer_Source_COMPONENT2:Lcom/mstar/tv/service/aidl/EN_TIME_ON_TIME_SOURCE;

    aput-object v2, v0, v1

    const/16 v1, 0xa

    sget-object v2, Lcom/mstar/tv/service/aidl/EN_TIME_ON_TIME_SOURCE;->EN_Time_OnTimer_Source_RGB:Lcom/mstar/tv/service/aidl/EN_TIME_ON_TIME_SOURCE;

    aput-object v2, v0, v1

    const/16 v1, 0xb

    sget-object v2, Lcom/mstar/tv/service/aidl/EN_TIME_ON_TIME_SOURCE;->EN_Time_OnTimer_Source_HDMI:Lcom/mstar/tv/service/aidl/EN_TIME_ON_TIME_SOURCE;

    aput-object v2, v0, v1

    const/16 v1, 0xc

    sget-object v2, Lcom/mstar/tv/service/aidl/EN_TIME_ON_TIME_SOURCE;->EN_Time_OnTimer_Source_HDMI2:Lcom/mstar/tv/service/aidl/EN_TIME_ON_TIME_SOURCE;

    aput-object v2, v0, v1

    const/16 v1, 0xd

    sget-object v2, Lcom/mstar/tv/service/aidl/EN_TIME_ON_TIME_SOURCE;->EN_Time_OnTimer_Source_HDMI3:Lcom/mstar/tv/service/aidl/EN_TIME_ON_TIME_SOURCE;

    aput-object v2, v0, v1

    const/16 v1, 0xe

    sget-object v2, Lcom/mstar/tv/service/aidl/EN_TIME_ON_TIME_SOURCE;->EN_Time_OnTimer_Source_HDMI4:Lcom/mstar/tv/service/aidl/EN_TIME_ON_TIME_SOURCE;

    aput-object v2, v0, v1

    const/16 v1, 0xf

    sget-object v2, Lcom/mstar/tv/service/aidl/EN_TIME_ON_TIME_SOURCE;->EN_Time_OnTimer_Source_AV:Lcom/mstar/tv/service/aidl/EN_TIME_ON_TIME_SOURCE;

    aput-object v2, v0, v1

    const/16 v1, 0x10

    sget-object v2, Lcom/mstar/tv/service/aidl/EN_TIME_ON_TIME_SOURCE;->EN_Time_OnTimer_Source_AV2:Lcom/mstar/tv/service/aidl/EN_TIME_ON_TIME_SOURCE;

    aput-object v2, v0, v1

    const/16 v1, 0x11

    sget-object v2, Lcom/mstar/tv/service/aidl/EN_TIME_ON_TIME_SOURCE;->EN_Time_OnTimer_Source_AV3:Lcom/mstar/tv/service/aidl/EN_TIME_ON_TIME_SOURCE;

    aput-object v2, v0, v1

    const/16 v1, 0x12

    sget-object v2, Lcom/mstar/tv/service/aidl/EN_TIME_ON_TIME_SOURCE;->EN_Time_OnTimer_Source_SVIDEO:Lcom/mstar/tv/service/aidl/EN_TIME_ON_TIME_SOURCE;

    aput-object v2, v0, v1

    const/16 v1, 0x13

    sget-object v2, Lcom/mstar/tv/service/aidl/EN_TIME_ON_TIME_SOURCE;->EN_Time_OnTimer_Source_SVIDEO2:Lcom/mstar/tv/service/aidl/EN_TIME_ON_TIME_SOURCE;

    aput-object v2, v0, v1

    const/16 v1, 0x14

    sget-object v2, Lcom/mstar/tv/service/aidl/EN_TIME_ON_TIME_SOURCE;->EN_Time_OnTimer_Source_MPLAYER:Lcom/mstar/tv/service/aidl/EN_TIME_ON_TIME_SOURCE;

    aput-object v2, v0, v1

    const/16 v1, 0x15

    sget-object v2, Lcom/mstar/tv/service/aidl/EN_TIME_ON_TIME_SOURCE;->EN_Time_OnTimer_Source_DLNA:Lcom/mstar/tv/service/aidl/EN_TIME_ON_TIME_SOURCE;

    aput-object v2, v0, v1

    const/16 v1, 0x16

    sget-object v2, Lcom/mstar/tv/service/aidl/EN_TIME_ON_TIME_SOURCE;->EN_Time_OnTimer_Source_NUM:Lcom/mstar/tv/service/aidl/EN_TIME_ON_TIME_SOURCE;

    aput-object v2, v0, v1

    sput-object v0, Lcom/mstar/tv/service/aidl/EN_TIME_ON_TIME_SOURCE;->ENUM$VALUES:[Lcom/mstar/tv/service/aidl/EN_TIME_ON_TIME_SOURCE;

    new-instance v0, Lcom/mstar/tv/service/aidl/EN_TIME_ON_TIME_SOURCE$1;

    invoke-direct {v0}, Lcom/mstar/tv/service/aidl/EN_TIME_ON_TIME_SOURCE$1;-><init>()V

    sput-object v0, Lcom/mstar/tv/service/aidl/EN_TIME_ON_TIME_SOURCE;->CREATOR:Landroid/os/Parcelable$Creator;

    return-void
.end method

.method private constructor <init>(Ljava/lang/String;I)V
    .locals 0

    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    return-void
.end method

.method public static valueOf(Ljava/lang/String;)Lcom/mstar/tv/service/aidl/EN_TIME_ON_TIME_SOURCE;
    .locals 1

    const-class v0, Lcom/mstar/tv/service/aidl/EN_TIME_ON_TIME_SOURCE;

    invoke-static {v0, p0}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;

    move-result-object v0

    check-cast v0, Lcom/mstar/tv/service/aidl/EN_TIME_ON_TIME_SOURCE;

    return-object v0
.end method

.method public static values()[Lcom/mstar/tv/service/aidl/EN_TIME_ON_TIME_SOURCE;
    .locals 4

    const/4 v3, 0x0

    sget-object v0, Lcom/mstar/tv/service/aidl/EN_TIME_ON_TIME_SOURCE;->ENUM$VALUES:[Lcom/mstar/tv/service/aidl/EN_TIME_ON_TIME_SOURCE;

    array-length v1, v0

    new-array v2, v1, [Lcom/mstar/tv/service/aidl/EN_TIME_ON_TIME_SOURCE;

    invoke-static {v0, v3, v2, v3, v1}, Ljava/lang/System;->arraycopy(Ljava/lang/Object;ILjava/lang/Object;II)V

    return-object v2
.end method


# virtual methods
.method public describeContents()I
    .locals 1

    const/4 v0, 0x0

    return v0
.end method

.method public writeToParcel(Landroid/os/Parcel;I)V
    .locals 1
    .param p1    # Landroid/os/Parcel;
    .param p2    # I

    invoke-virtual {p0}, Lcom/mstar/tv/service/aidl/EN_TIME_ON_TIME_SOURCE;->ordinal()I

    move-result v0

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeInt(I)V

    return-void
.end method
