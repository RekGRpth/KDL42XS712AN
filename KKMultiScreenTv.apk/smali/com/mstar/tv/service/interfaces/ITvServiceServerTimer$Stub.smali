.class public abstract Lcom/mstar/tv/service/interfaces/ITvServiceServerTimer$Stub;
.super Landroid/os/Binder;
.source "ITvServiceServerTimer.java"

# interfaces
.implements Lcom/mstar/tv/service/interfaces/ITvServiceServerTimer;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/mstar/tv/service/interfaces/ITvServiceServerTimer;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x409
    name = "Stub"
.end annotation

.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/mstar/tv/service/interfaces/ITvServiceServerTimer$Stub$Proxy;
    }
.end annotation


# static fields
.field private static final DESCRIPTOR:Ljava/lang/String; = "com.mstar.tv.service.interfaces.ITvServiceServerTimer"

.field static final TRANSACTION_getCurTimer:I = 0x1

.field static final TRANSACTION_getOffTimer:I = 0x5

.field static final TRANSACTION_getOnTimeEvent:I = 0xd

.field static final TRANSACTION_getOnTimer:I = 0x3

.field static final TRANSACTION_getSleepMode:I = 0xb

.field static final TRANSACTION_isOffTimerEnable:I = 0x7

.field static final TRANSACTION_isOnTimerEnable:I = 0x9

.field static final TRANSACTION_setCurTimer:I = 0xe

.field static final TRANSACTION_setOffTimer:I = 0x4

.field static final TRANSACTION_setOffTimerEnable:I = 0x6

.field static final TRANSACTION_setOnTimeEvent:I = 0xc

.field static final TRANSACTION_setOnTimer:I = 0x2

.field static final TRANSACTION_setOnTimerEnable:I = 0x8

.field static final TRANSACTION_setSleepMode:I = 0xa


# direct methods
.method public constructor <init>()V
    .locals 1

    invoke-direct {p0}, Landroid/os/Binder;-><init>()V

    const-string v0, "com.mstar.tv.service.interfaces.ITvServiceServerTimer"

    invoke-virtual {p0, p0, v0}, Lcom/mstar/tv/service/interfaces/ITvServiceServerTimer$Stub;->attachInterface(Landroid/os/IInterface;Ljava/lang/String;)V

    return-void
.end method

.method public static asInterface(Landroid/os/IBinder;)Lcom/mstar/tv/service/interfaces/ITvServiceServerTimer;
    .locals 2
    .param p0    # Landroid/os/IBinder;

    if-nez p0, :cond_0

    const/4 v0, 0x0

    :goto_0
    return-object v0

    :cond_0
    const-string v1, "com.mstar.tv.service.interfaces.ITvServiceServerTimer"

    invoke-interface {p0, v1}, Landroid/os/IBinder;->queryLocalInterface(Ljava/lang/String;)Landroid/os/IInterface;

    move-result-object v0

    if-eqz v0, :cond_1

    instance-of v1, v0, Lcom/mstar/tv/service/interfaces/ITvServiceServerTimer;

    if-eqz v1, :cond_1

    check-cast v0, Lcom/mstar/tv/service/interfaces/ITvServiceServerTimer;

    goto :goto_0

    :cond_1
    new-instance v0, Lcom/mstar/tv/service/interfaces/ITvServiceServerTimer$Stub$Proxy;

    invoke-direct {v0, p0}, Lcom/mstar/tv/service/interfaces/ITvServiceServerTimer$Stub$Proxy;-><init>(Landroid/os/IBinder;)V

    goto :goto_0
.end method


# virtual methods
.method public asBinder()Landroid/os/IBinder;
    .locals 0

    return-object p0
.end method

.method public onTransact(ILandroid/os/Parcel;Landroid/os/Parcel;I)Z
    .locals 6
    .param p1    # I
    .param p2    # Landroid/os/Parcel;
    .param p3    # Landroid/os/Parcel;
    .param p4    # I
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Landroid/os/RemoteException;
        }
    .end annotation

    const/4 v3, 0x0

    const/4 v4, 0x1

    sparse-switch p1, :sswitch_data_0

    invoke-super {p0, p1, p2, p3, p4}, Landroid/os/Binder;->onTransact(ILandroid/os/Parcel;Landroid/os/Parcel;I)Z

    move-result v4

    :goto_0
    return v4

    :sswitch_0
    const-string v3, "com.mstar.tv.service.interfaces.ITvServiceServerTimer"

    invoke-virtual {p3, v3}, Landroid/os/Parcel;->writeString(Ljava/lang/String;)V

    goto :goto_0

    :sswitch_1
    const-string v5, "com.mstar.tv.service.interfaces.ITvServiceServerTimer"

    invoke-virtual {p2, v5}, Landroid/os/Parcel;->enforceInterface(Ljava/lang/String;)V

    invoke-virtual {p0}, Lcom/mstar/tv/service/interfaces/ITvServiceServerTimer$Stub;->getCurTimer()Lcom/mstar/tv/service/aidl/ST_Time;

    move-result-object v2

    invoke-virtual {p3}, Landroid/os/Parcel;->writeNoException()V

    if-eqz v2, :cond_0

    invoke-virtual {p3, v4}, Landroid/os/Parcel;->writeInt(I)V

    invoke-virtual {v2, p3, v4}, Lcom/mstar/tv/service/aidl/ST_Time;->writeToParcel(Landroid/os/Parcel;I)V

    goto :goto_0

    :cond_0
    invoke-virtual {p3, v3}, Landroid/os/Parcel;->writeInt(I)V

    goto :goto_0

    :sswitch_2
    const-string v5, "com.mstar.tv.service.interfaces.ITvServiceServerTimer"

    invoke-virtual {p2, v5}, Landroid/os/Parcel;->enforceInterface(Ljava/lang/String;)V

    invoke-virtual {p2}, Landroid/os/Parcel;->readInt()I

    move-result v5

    if-eqz v5, :cond_2

    sget-object v5, Lcom/mstar/tv/service/aidl/ST_Time;->CREATOR:Landroid/os/Parcelable$Creator;

    invoke-interface {v5, p2}, Landroid/os/Parcelable$Creator;->createFromParcel(Landroid/os/Parcel;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/mstar/tv/service/aidl/ST_Time;

    :goto_1
    invoke-virtual {p0, v0}, Lcom/mstar/tv/service/interfaces/ITvServiceServerTimer$Stub;->setOnTimer(Lcom/mstar/tv/service/aidl/ST_Time;)Z

    move-result v2

    invoke-virtual {p3}, Landroid/os/Parcel;->writeNoException()V

    if-eqz v2, :cond_1

    move v3, v4

    :cond_1
    invoke-virtual {p3, v3}, Landroid/os/Parcel;->writeInt(I)V

    goto :goto_0

    :cond_2
    const/4 v0, 0x0

    goto :goto_1

    :sswitch_3
    const-string v5, "com.mstar.tv.service.interfaces.ITvServiceServerTimer"

    invoke-virtual {p2, v5}, Landroid/os/Parcel;->enforceInterface(Ljava/lang/String;)V

    invoke-virtual {p0}, Lcom/mstar/tv/service/interfaces/ITvServiceServerTimer$Stub;->getOnTimer()Lcom/mstar/tv/service/aidl/ST_Time;

    move-result-object v2

    invoke-virtual {p3}, Landroid/os/Parcel;->writeNoException()V

    if-eqz v2, :cond_3

    invoke-virtual {p3, v4}, Landroid/os/Parcel;->writeInt(I)V

    invoke-virtual {v2, p3, v4}, Lcom/mstar/tv/service/aidl/ST_Time;->writeToParcel(Landroid/os/Parcel;I)V

    goto :goto_0

    :cond_3
    invoke-virtual {p3, v3}, Landroid/os/Parcel;->writeInt(I)V

    goto :goto_0

    :sswitch_4
    const-string v5, "com.mstar.tv.service.interfaces.ITvServiceServerTimer"

    invoke-virtual {p2, v5}, Landroid/os/Parcel;->enforceInterface(Ljava/lang/String;)V

    invoke-virtual {p2}, Landroid/os/Parcel;->readInt()I

    move-result v5

    if-eqz v5, :cond_5

    sget-object v5, Lcom/mstar/tv/service/aidl/ST_Time;->CREATOR:Landroid/os/Parcelable$Creator;

    invoke-interface {v5, p2}, Landroid/os/Parcelable$Creator;->createFromParcel(Landroid/os/Parcel;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/mstar/tv/service/aidl/ST_Time;

    :goto_2
    invoke-virtual {p0, v0}, Lcom/mstar/tv/service/interfaces/ITvServiceServerTimer$Stub;->setOffTimer(Lcom/mstar/tv/service/aidl/ST_Time;)Z

    move-result v2

    invoke-virtual {p3}, Landroid/os/Parcel;->writeNoException()V

    if-eqz v2, :cond_4

    move v3, v4

    :cond_4
    invoke-virtual {p3, v3}, Landroid/os/Parcel;->writeInt(I)V

    goto :goto_0

    :cond_5
    const/4 v0, 0x0

    goto :goto_2

    :sswitch_5
    const-string v5, "com.mstar.tv.service.interfaces.ITvServiceServerTimer"

    invoke-virtual {p2, v5}, Landroid/os/Parcel;->enforceInterface(Ljava/lang/String;)V

    invoke-virtual {p0}, Lcom/mstar/tv/service/interfaces/ITvServiceServerTimer$Stub;->getOffTimer()Lcom/mstar/tv/service/aidl/ST_Time;

    move-result-object v2

    invoke-virtual {p3}, Landroid/os/Parcel;->writeNoException()V

    if-eqz v2, :cond_6

    invoke-virtual {p3, v4}, Landroid/os/Parcel;->writeInt(I)V

    invoke-virtual {v2, p3, v4}, Lcom/mstar/tv/service/aidl/ST_Time;->writeToParcel(Landroid/os/Parcel;I)V

    goto/16 :goto_0

    :cond_6
    invoke-virtual {p3, v3}, Landroid/os/Parcel;->writeInt(I)V

    goto/16 :goto_0

    :sswitch_6
    const-string v5, "com.mstar.tv.service.interfaces.ITvServiceServerTimer"

    invoke-virtual {p2, v5}, Landroid/os/Parcel;->enforceInterface(Ljava/lang/String;)V

    invoke-virtual {p2}, Landroid/os/Parcel;->readInt()I

    move-result v5

    if-eqz v5, :cond_8

    move v0, v4

    :goto_3
    invoke-virtual {p0, v0}, Lcom/mstar/tv/service/interfaces/ITvServiceServerTimer$Stub;->setOffTimerEnable(Z)Z

    move-result v2

    invoke-virtual {p3}, Landroid/os/Parcel;->writeNoException()V

    if-eqz v2, :cond_7

    move v3, v4

    :cond_7
    invoke-virtual {p3, v3}, Landroid/os/Parcel;->writeInt(I)V

    goto/16 :goto_0

    :cond_8
    move v0, v3

    goto :goto_3

    :sswitch_7
    const-string v5, "com.mstar.tv.service.interfaces.ITvServiceServerTimer"

    invoke-virtual {p2, v5}, Landroid/os/Parcel;->enforceInterface(Ljava/lang/String;)V

    invoke-virtual {p0}, Lcom/mstar/tv/service/interfaces/ITvServiceServerTimer$Stub;->isOffTimerEnable()Z

    move-result v2

    invoke-virtual {p3}, Landroid/os/Parcel;->writeNoException()V

    if-eqz v2, :cond_9

    move v3, v4

    :cond_9
    invoke-virtual {p3, v3}, Landroid/os/Parcel;->writeInt(I)V

    goto/16 :goto_0

    :sswitch_8
    const-string v5, "com.mstar.tv.service.interfaces.ITvServiceServerTimer"

    invoke-virtual {p2, v5}, Landroid/os/Parcel;->enforceInterface(Ljava/lang/String;)V

    invoke-virtual {p2}, Landroid/os/Parcel;->readInt()I

    move-result v5

    if-eqz v5, :cond_b

    move v0, v4

    :goto_4
    invoke-virtual {p0, v0}, Lcom/mstar/tv/service/interfaces/ITvServiceServerTimer$Stub;->setOnTimerEnable(Z)Z

    move-result v2

    invoke-virtual {p3}, Landroid/os/Parcel;->writeNoException()V

    if-eqz v2, :cond_a

    move v3, v4

    :cond_a
    invoke-virtual {p3, v3}, Landroid/os/Parcel;->writeInt(I)V

    goto/16 :goto_0

    :cond_b
    move v0, v3

    goto :goto_4

    :sswitch_9
    const-string v5, "com.mstar.tv.service.interfaces.ITvServiceServerTimer"

    invoke-virtual {p2, v5}, Landroid/os/Parcel;->enforceInterface(Ljava/lang/String;)V

    invoke-virtual {p0}, Lcom/mstar/tv/service/interfaces/ITvServiceServerTimer$Stub;->isOnTimerEnable()Z

    move-result v2

    invoke-virtual {p3}, Landroid/os/Parcel;->writeNoException()V

    if-eqz v2, :cond_c

    move v3, v4

    :cond_c
    invoke-virtual {p3, v3}, Landroid/os/Parcel;->writeInt(I)V

    goto/16 :goto_0

    :sswitch_a
    const-string v5, "com.mstar.tv.service.interfaces.ITvServiceServerTimer"

    invoke-virtual {p2, v5}, Landroid/os/Parcel;->enforceInterface(Ljava/lang/String;)V

    invoke-virtual {p2}, Landroid/os/Parcel;->readInt()I

    move-result v5

    if-eqz v5, :cond_e

    sget-object v5, Lcom/mstar/tv/service/aidl/EN_SLEEP_TIME_STATE;->CREATOR:Landroid/os/Parcelable$Creator;

    invoke-interface {v5, p2}, Landroid/os/Parcelable$Creator;->createFromParcel(Landroid/os/Parcel;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/mstar/tv/service/aidl/EN_SLEEP_TIME_STATE;

    :goto_5
    invoke-virtual {p0, v0}, Lcom/mstar/tv/service/interfaces/ITvServiceServerTimer$Stub;->setSleepMode(Lcom/mstar/tv/service/aidl/EN_SLEEP_TIME_STATE;)Z

    move-result v2

    invoke-virtual {p3}, Landroid/os/Parcel;->writeNoException()V

    if-eqz v2, :cond_d

    move v3, v4

    :cond_d
    invoke-virtual {p3, v3}, Landroid/os/Parcel;->writeInt(I)V

    goto/16 :goto_0

    :cond_e
    const/4 v0, 0x0

    goto :goto_5

    :sswitch_b
    const-string v5, "com.mstar.tv.service.interfaces.ITvServiceServerTimer"

    invoke-virtual {p2, v5}, Landroid/os/Parcel;->enforceInterface(Ljava/lang/String;)V

    invoke-virtual {p0}, Lcom/mstar/tv/service/interfaces/ITvServiceServerTimer$Stub;->getSleepMode()Lcom/mstar/tv/service/aidl/EN_SLEEP_TIME_STATE;

    move-result-object v2

    invoke-virtual {p3}, Landroid/os/Parcel;->writeNoException()V

    if-eqz v2, :cond_f

    invoke-virtual {p3, v4}, Landroid/os/Parcel;->writeInt(I)V

    invoke-virtual {v2, p3, v4}, Lcom/mstar/tv/service/aidl/EN_SLEEP_TIME_STATE;->writeToParcel(Landroid/os/Parcel;I)V

    goto/16 :goto_0

    :cond_f
    invoke-virtual {p3, v3}, Landroid/os/Parcel;->writeInt(I)V

    goto/16 :goto_0

    :sswitch_c
    const-string v5, "com.mstar.tv.service.interfaces.ITvServiceServerTimer"

    invoke-virtual {p2, v5}, Landroid/os/Parcel;->enforceInterface(Ljava/lang/String;)V

    invoke-virtual {p2}, Landroid/os/Parcel;->readInt()I

    move-result v5

    if-eqz v5, :cond_11

    sget-object v5, Lcom/mstar/tv/service/aidl/ST_OnTime_TVDes;->CREATOR:Landroid/os/Parcelable$Creator;

    invoke-interface {v5, p2}, Landroid/os/Parcelable$Creator;->createFromParcel(Landroid/os/Parcel;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/mstar/tv/service/aidl/ST_OnTime_TVDes;

    :goto_6
    invoke-virtual {p0, v0}, Lcom/mstar/tv/service/interfaces/ITvServiceServerTimer$Stub;->setOnTimeEvent(Lcom/mstar/tv/service/aidl/ST_OnTime_TVDes;)Z

    move-result v2

    invoke-virtual {p3}, Landroid/os/Parcel;->writeNoException()V

    if-eqz v2, :cond_10

    move v3, v4

    :cond_10
    invoke-virtual {p3, v3}, Landroid/os/Parcel;->writeInt(I)V

    goto/16 :goto_0

    :cond_11
    const/4 v0, 0x0

    goto :goto_6

    :sswitch_d
    const-string v5, "com.mstar.tv.service.interfaces.ITvServiceServerTimer"

    invoke-virtual {p2, v5}, Landroid/os/Parcel;->enforceInterface(Ljava/lang/String;)V

    invoke-virtual {p0}, Lcom/mstar/tv/service/interfaces/ITvServiceServerTimer$Stub;->getOnTimeEvent()Lcom/mstar/tv/service/aidl/ST_OnTime_TVDes;

    move-result-object v2

    invoke-virtual {p3}, Landroid/os/Parcel;->writeNoException()V

    if-eqz v2, :cond_12

    invoke-virtual {p3, v4}, Landroid/os/Parcel;->writeInt(I)V

    invoke-virtual {v2, p3, v4}, Lcom/mstar/tv/service/aidl/ST_OnTime_TVDes;->writeToParcel(Landroid/os/Parcel;I)V

    goto/16 :goto_0

    :cond_12
    invoke-virtual {p3, v3}, Landroid/os/Parcel;->writeInt(I)V

    goto/16 :goto_0

    :sswitch_e
    const-string v5, "com.mstar.tv.service.interfaces.ITvServiceServerTimer"

    invoke-virtual {p2, v5}, Landroid/os/Parcel;->enforceInterface(Ljava/lang/String;)V

    invoke-virtual {p2}, Landroid/os/Parcel;->readInt()I

    move-result v5

    if-eqz v5, :cond_14

    sget-object v5, Lcom/mstar/tv/service/aidl/ST_Time;->CREATOR:Landroid/os/Parcelable$Creator;

    invoke-interface {v5, p2}, Landroid/os/Parcelable$Creator;->createFromParcel(Landroid/os/Parcel;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/mstar/tv/service/aidl/ST_Time;

    :goto_7
    invoke-virtual {p2}, Landroid/os/Parcel;->readInt()I

    move-result v5

    if-eqz v5, :cond_15

    move v1, v4

    :goto_8
    invoke-virtual {p0, v0, v1}, Lcom/mstar/tv/service/interfaces/ITvServiceServerTimer$Stub;->setCurTimer(Lcom/mstar/tv/service/aidl/ST_Time;Z)Z

    move-result v2

    invoke-virtual {p3}, Landroid/os/Parcel;->writeNoException()V

    if-eqz v2, :cond_13

    move v3, v4

    :cond_13
    invoke-virtual {p3, v3}, Landroid/os/Parcel;->writeInt(I)V

    goto/16 :goto_0

    :cond_14
    const/4 v0, 0x0

    goto :goto_7

    :cond_15
    move v1, v3

    goto :goto_8

    :sswitch_data_0
    .sparse-switch
        0x1 -> :sswitch_1
        0x2 -> :sswitch_2
        0x3 -> :sswitch_3
        0x4 -> :sswitch_4
        0x5 -> :sswitch_5
        0x6 -> :sswitch_6
        0x7 -> :sswitch_7
        0x8 -> :sswitch_8
        0x9 -> :sswitch_9
        0xa -> :sswitch_a
        0xb -> :sswitch_b
        0xc -> :sswitch_c
        0xd -> :sswitch_d
        0xe -> :sswitch_e
        0x5f4e5446 -> :sswitch_0
    .end sparse-switch
.end method
