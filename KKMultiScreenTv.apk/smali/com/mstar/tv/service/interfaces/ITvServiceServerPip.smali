.class public interface abstract Lcom/mstar/tv/service/interfaces/ITvServiceServerPip;
.super Ljava/lang/Object;
.source "ITvServiceServerPip.java"

# interfaces
.implements Landroid/os/IInterface;


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/mstar/tv/service/interfaces/ITvServiceServerPip$Stub;
    }
.end annotation


# virtual methods
.method public abstract checkPipSupport(Lcom/mstar/tv/service/aidl/EN_INPUT_SOURCE_TYPE;Lcom/mstar/tv/service/aidl/EN_INPUT_SOURCE_TYPE;)Z
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Landroid/os/RemoteException;
        }
    .end annotation
.end method

.method public abstract checkPipSupportOnSubSrc(Lcom/mstar/tv/service/aidl/EN_INPUT_SOURCE_TYPE;)Z
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Landroid/os/RemoteException;
        }
    .end annotation
.end method

.method public abstract checkPopSupport(Lcom/mstar/tv/service/aidl/EN_INPUT_SOURCE_TYPE;Lcom/mstar/tv/service/aidl/EN_INPUT_SOURCE_TYPE;)Z
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Landroid/os/RemoteException;
        }
    .end annotation
.end method

.method public abstract checkTravelingModeSupport(Lcom/mstar/tv/service/aidl/EN_INPUT_SOURCE_TYPE;Lcom/mstar/tv/service/aidl/EN_INPUT_SOURCE_TYPE;)Z
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Landroid/os/RemoteException;
        }
    .end annotation
.end method

.method public abstract disable3dDualView()Z
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Landroid/os/RemoteException;
        }
    .end annotation
.end method

.method public abstract disablePip()Z
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Landroid/os/RemoteException;
        }
    .end annotation
.end method

.method public abstract disablePop()Z
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Landroid/os/RemoteException;
        }
    .end annotation
.end method

.method public abstract disableTravelingMode()Z
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Landroid/os/RemoteException;
        }
    .end annotation
.end method

.method public abstract enable3dDualView(Lcom/mstar/tv/service/aidl/EN_INPUT_SOURCE_TYPE;Lcom/mstar/tv/service/aidl/EN_INPUT_SOURCE_TYPE;Lcom/mstar/tv/service/aidl/VIDEO_WINDOW_TYPE;Lcom/mstar/tv/service/aidl/VIDEO_WINDOW_TYPE;)Z
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Landroid/os/RemoteException;
        }
    .end annotation
.end method

.method public abstract enablePipMM(Lcom/mstar/tv/service/aidl/EN_INPUT_SOURCE_TYPE;Lcom/mstar/tv/service/aidl/VIDEO_WINDOW_TYPE;)Lcom/mstar/tv/service/aidl/EN_PIP_RETURN;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Landroid/os/RemoteException;
        }
    .end annotation
.end method

.method public abstract enablePipTV(Lcom/mstar/tv/service/aidl/EN_INPUT_SOURCE_TYPE;Lcom/mstar/tv/service/aidl/EN_INPUT_SOURCE_TYPE;Lcom/mstar/tv/service/aidl/VIDEO_WINDOW_TYPE;)Lcom/mstar/tv/service/aidl/EN_PIP_RETURN;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Landroid/os/RemoteException;
        }
    .end annotation
.end method

.method public abstract enablePopMM(Lcom/mstar/tv/service/aidl/EN_INPUT_SOURCE_TYPE;)Lcom/mstar/tv/service/aidl/EN_PIP_RETURN;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Landroid/os/RemoteException;
        }
    .end annotation
.end method

.method public abstract enablePopTV(Lcom/mstar/tv/service/aidl/EN_INPUT_SOURCE_TYPE;Lcom/mstar/tv/service/aidl/EN_INPUT_SOURCE_TYPE;)Lcom/mstar/tv/service/aidl/EN_PIP_RETURN;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Landroid/os/RemoteException;
        }
    .end annotation
.end method

.method public abstract enableTravelingModeMM(Lcom/mstar/tv/service/aidl/EN_INPUT_SOURCE_TYPE;)Lcom/mstar/tv/service/aidl/EN_PIP_RETURN;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Landroid/os/RemoteException;
        }
    .end annotation
.end method

.method public abstract enableTravelingModeTV(Lcom/mstar/tv/service/aidl/EN_INPUT_SOURCE_TYPE;Lcom/mstar/tv/service/aidl/EN_INPUT_SOURCE_TYPE;)Lcom/mstar/tv/service/aidl/EN_PIP_RETURN;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Landroid/os/RemoteException;
        }
    .end annotation
.end method

.method public abstract getIsDualViewOn()Z
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Landroid/os/RemoteException;
        }
    .end annotation
.end method

.method public abstract getIsPipOn()Z
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Landroid/os/RemoteException;
        }
    .end annotation
.end method

.method public abstract getIsPopOn()Z
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Landroid/os/RemoteException;
        }
    .end annotation
.end method

.method public abstract getMainWindowSourceList()Lcom/mstar/tv/service/aidl/IntArrayList;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Landroid/os/RemoteException;
        }
    .end annotation
.end method

.method public abstract getSubInputSource()Lcom/mstar/tv/service/aidl/EN_INPUT_SOURCE_TYPE;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Landroid/os/RemoteException;
        }
    .end annotation
.end method

.method public abstract getSubWindowSourceList(Z)Lcom/mstar/tv/service/aidl/IntArrayList;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Landroid/os/RemoteException;
        }
    .end annotation
.end method

.method public abstract isDualViewEnabled()Z
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Landroid/os/RemoteException;
        }
    .end annotation
.end method

.method public abstract isPipEnabled()Z
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Landroid/os/RemoteException;
        }
    .end annotation
.end method

.method public abstract isPipModeEnabled()Z
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Landroid/os/RemoteException;
        }
    .end annotation
.end method

.method public abstract isPopEnabled()Z
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Landroid/os/RemoteException;
        }
    .end annotation
.end method

.method public abstract setDualViewOnFlag(Z)Z
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Landroid/os/RemoteException;
        }
    .end annotation
.end method

.method public abstract setPipDisplayFocusWindow(Lcom/mstar/tv/service/aidl/EN_SCALER_WIN;)V
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Landroid/os/RemoteException;
        }
    .end annotation
.end method

.method public abstract setPipOnFlag(Z)Z
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Landroid/os/RemoteException;
        }
    .end annotation
.end method

.method public abstract setPipSubwindow(Lcom/mstar/tv/service/aidl/VIDEO_WINDOW_TYPE;)Z
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Landroid/os/RemoteException;
        }
    .end annotation
.end method

.method public abstract setPopOnFlag(Z)Z
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Landroid/os/RemoteException;
        }
    .end annotation
.end method
