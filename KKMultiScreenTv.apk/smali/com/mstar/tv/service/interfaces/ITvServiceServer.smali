.class public interface abstract Lcom/mstar/tv/service/interfaces/ITvServiceServer;
.super Ljava/lang/Object;
.source "ITvServiceServer.java"

# interfaces
.implements Landroid/os/IInterface;


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/mstar/tv/service/interfaces/ITvServiceServer$Stub;
    }
.end annotation


# virtual methods
.method public abstract getAudioManager()Lcom/mstar/tv/service/interfaces/ITvServiceServerAudio;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Landroid/os/RemoteException;
        }
    .end annotation
.end method

.method public abstract getChannelManager()Lcom/mstar/tv/service/interfaces/ITvServiceServerChannel;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Landroid/os/RemoteException;
        }
    .end annotation
.end method

.method public abstract getCommonManager()Lcom/mstar/tv/service/interfaces/ITvServiceServerCommon;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Landroid/os/RemoteException;
        }
    .end annotation
.end method

.method public abstract getPictureManager()Lcom/mstar/tv/service/interfaces/ITvServiceServerPicture;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Landroid/os/RemoteException;
        }
    .end annotation
.end method

.method public abstract getPipManager()Lcom/mstar/tv/service/interfaces/ITvServiceServerPip;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Landroid/os/RemoteException;
        }
    .end annotation
.end method

.method public abstract getS3DManager()Lcom/mstar/tv/service/interfaces/ITvServiceServerS3D;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Landroid/os/RemoteException;
        }
    .end annotation
.end method

.method public abstract getTimerManager()Lcom/mstar/tv/service/interfaces/ITvServiceServerTimer;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Landroid/os/RemoteException;
        }
    .end annotation
.end method

.method public abstract onKeyDown(Landroid/view/KeyEvent;)Z
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Landroid/os/RemoteException;
        }
    .end annotation
.end method
