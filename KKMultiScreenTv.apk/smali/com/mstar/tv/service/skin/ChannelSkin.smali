.class public Lcom/mstar/tv/service/skin/ChannelSkin;
.super Ljava/lang/Object;
.source "ChannelSkin.java"


# instance fields
.field private curChannelNumber:I

.field private curDtvRoute:S

.field private handler:Landroid/os/Handler;

.field private iTvServiceChannel:Lcom/mstar/tv/service/interfaces/ITvServiceServerChannel;

.field private isBindOk:Z

.field private prevChannelNumber:I

.field private superContext:Landroid/content/Context;

.field private tuning_scan_type:Lcom/mstar/tv/service/aidl/EN_TUNING_SCAN_TYPE;

.field protected tvServiceChannelConnection:Landroid/content/ServiceConnection;

.field private tv_tuning_status:Lcom/mstar/tv/service/aidl/EN_TV_TS_STATUS;


# direct methods
.method public constructor <init>(Landroid/content/Context;)V
    .locals 2
    .param p1    # Landroid/content/Context;

    const/4 v1, 0x0

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    const/4 v0, 0x0

    iput-object v0, p0, Lcom/mstar/tv/service/skin/ChannelSkin;->handler:Landroid/os/Handler;

    iput v1, p0, Lcom/mstar/tv/service/skin/ChannelSkin;->curChannelNumber:I

    iput v1, p0, Lcom/mstar/tv/service/skin/ChannelSkin;->prevChannelNumber:I

    sget-object v0, Lcom/mstar/tv/service/aidl/EN_TV_TS_STATUS;->E_TS_NONE:Lcom/mstar/tv/service/aidl/EN_TV_TS_STATUS;

    iput-object v0, p0, Lcom/mstar/tv/service/skin/ChannelSkin;->tv_tuning_status:Lcom/mstar/tv/service/aidl/EN_TV_TS_STATUS;

    sget-object v0, Lcom/mstar/tv/service/aidl/EN_TUNING_SCAN_TYPE;->E_SCAN_ATV:Lcom/mstar/tv/service/aidl/EN_TUNING_SCAN_TYPE;

    iput-object v0, p0, Lcom/mstar/tv/service/skin/ChannelSkin;->tuning_scan_type:Lcom/mstar/tv/service/aidl/EN_TUNING_SCAN_TYPE;

    iput-short v1, p0, Lcom/mstar/tv/service/skin/ChannelSkin;->curDtvRoute:S

    new-instance v0, Lcom/mstar/tv/service/skin/ChannelSkin$1;

    invoke-direct {v0, p0}, Lcom/mstar/tv/service/skin/ChannelSkin$1;-><init>(Lcom/mstar/tv/service/skin/ChannelSkin;)V

    iput-object v0, p0, Lcom/mstar/tv/service/skin/ChannelSkin;->tvServiceChannelConnection:Landroid/content/ServiceConnection;

    iput-object p1, p0, Lcom/mstar/tv/service/skin/ChannelSkin;->superContext:Landroid/content/Context;

    return-void
.end method

.method static synthetic access$0(Lcom/mstar/tv/service/skin/ChannelSkin;Lcom/mstar/tv/service/interfaces/ITvServiceServerChannel;)V
    .locals 0

    iput-object p1, p0, Lcom/mstar/tv/service/skin/ChannelSkin;->iTvServiceChannel:Lcom/mstar/tv/service/interfaces/ITvServiceServerChannel;

    return-void
.end method

.method static synthetic access$1(Lcom/mstar/tv/service/skin/ChannelSkin;)Landroid/os/Handler;
    .locals 1

    iget-object v0, p0, Lcom/mstar/tv/service/skin/ChannelSkin;->handler:Landroid/os/Handler;

    return-object v0
.end method


# virtual methods
.method public DtvStopScan()Z
    .locals 2

    :try_start_0
    iget-object v1, p0, Lcom/mstar/tv/service/skin/ChannelSkin;->iTvServiceChannel:Lcom/mstar/tv/service/interfaces/ITvServiceServerChannel;

    invoke-interface {v1}, Lcom/mstar/tv/service/interfaces/ITvServiceServerChannel;->DtvStopScan()Z
    :try_end_0
    .catch Landroid/os/RemoteException; {:try_start_0 .. :try_end_0} :catch_0

    move-result v1

    :goto_0
    return v1

    :catch_0
    move-exception v0

    invoke-virtual {v0}, Landroid/os/RemoteException;->printStackTrace()V

    const/4 v1, 0x0

    goto :goto_0
.end method

.method public GetTsStatus()Lcom/mstar/tv/service/aidl/EN_TV_TS_STATUS;
    .locals 1

    iget-object v0, p0, Lcom/mstar/tv/service/skin/ChannelSkin;->tv_tuning_status:Lcom/mstar/tv/service/aidl/EN_TV_TS_STATUS;

    return-object v0
.end method

.method public addProgramToFavorite(Lcom/mstar/tv/service/aidl/EN_FAVORITE_ID;III)V
    .locals 2
    .param p1    # Lcom/mstar/tv/service/aidl/EN_FAVORITE_ID;
    .param p2    # I
    .param p3    # I
    .param p4    # I

    :try_start_0
    iget-object v1, p0, Lcom/mstar/tv/service/skin/ChannelSkin;->iTvServiceChannel:Lcom/mstar/tv/service/interfaces/ITvServiceServerChannel;

    invoke-interface {v1, p1, p2, p3, p4}, Lcom/mstar/tv/service/interfaces/ITvServiceServerChannel;->addProgramToFavorite(Lcom/mstar/tv/service/aidl/EN_FAVORITE_ID;III)V
    :try_end_0
    .catch Landroid/os/RemoteException; {:try_start_0 .. :try_end_0} :catch_0

    :goto_0
    return-void

    :catch_0
    move-exception v0

    invoke-virtual {v0}, Landroid/os/RemoteException;->printStackTrace()V

    goto :goto_0
.end method

.method public atvGetCurrentFrequency()I
    .locals 2

    :try_start_0
    iget-object v1, p0, Lcom/mstar/tv/service/skin/ChannelSkin;->iTvServiceChannel:Lcom/mstar/tv/service/interfaces/ITvServiceServerChannel;

    invoke-interface {v1}, Lcom/mstar/tv/service/interfaces/ITvServiceServerChannel;->atvGetCurrentFrequency()I
    :try_end_0
    .catch Landroid/os/RemoteException; {:try_start_0 .. :try_end_0} :catch_0

    move-result v1

    :goto_0
    return v1

    :catch_0
    move-exception v0

    invoke-virtual {v0}, Landroid/os/RemoteException;->printStackTrace()V

    const/4 v1, -0x1

    goto :goto_0
.end method

.method public atvGetProgramInfo(Lcom/mstar/tv/service/aidl/EN_GET_PROGRAM_INFO;IILjava/lang/String;)I
    .locals 2
    .param p1    # Lcom/mstar/tv/service/aidl/EN_GET_PROGRAM_INFO;
    .param p2    # I
    .param p3    # I
    .param p4    # Ljava/lang/String;

    :try_start_0
    iget-object v1, p0, Lcom/mstar/tv/service/skin/ChannelSkin;->iTvServiceChannel:Lcom/mstar/tv/service/interfaces/ITvServiceServerChannel;

    invoke-interface {v1, p1, p2, p3, p4}, Lcom/mstar/tv/service/interfaces/ITvServiceServerChannel;->atvGetProgramInfo(Lcom/mstar/tv/service/aidl/EN_GET_PROGRAM_INFO;IILjava/lang/String;)I
    :try_end_0
    .catch Landroid/os/RemoteException; {:try_start_0 .. :try_end_0} :catch_0

    move-result v1

    :goto_0
    return v1

    :catch_0
    move-exception v0

    invoke-virtual {v0}, Landroid/os/RemoteException;->printStackTrace()V

    const/4 v1, -0x1

    goto :goto_0
.end method

.method public atvGetSoundSystem()Lcom/mstar/tv/service/aidl/EN_ATV_SYSTEM_STANDARD;
    .locals 2

    :try_start_0
    iget-object v1, p0, Lcom/mstar/tv/service/skin/ChannelSkin;->iTvServiceChannel:Lcom/mstar/tv/service/interfaces/ITvServiceServerChannel;

    invoke-interface {v1}, Lcom/mstar/tv/service/interfaces/ITvServiceServerChannel;->atvGetSoundSystem()Lcom/mstar/tv/service/aidl/EN_ATV_SYSTEM_STANDARD;
    :try_end_0
    .catch Landroid/os/RemoteException; {:try_start_0 .. :try_end_0} :catch_0

    move-result-object v1

    :goto_0
    return-object v1

    :catch_0
    move-exception v0

    invoke-virtual {v0}, Landroid/os/RemoteException;->printStackTrace()V

    const/4 v1, 0x0

    goto :goto_0
.end method

.method public atvGetVideoSystem()Lcom/mstar/tv/service/aidl/EN_AVD_VIDEO_STANDARD_TYPE;
    .locals 2

    :try_start_0
    iget-object v1, p0, Lcom/mstar/tv/service/skin/ChannelSkin;->iTvServiceChannel:Lcom/mstar/tv/service/interfaces/ITvServiceServerChannel;

    invoke-interface {v1}, Lcom/mstar/tv/service/interfaces/ITvServiceServerChannel;->atvGetVideoSystem()Lcom/mstar/tv/service/aidl/EN_AVD_VIDEO_STANDARD_TYPE;
    :try_end_0
    .catch Landroid/os/RemoteException; {:try_start_0 .. :try_end_0} :catch_0

    move-result-object v1

    :goto_0
    return-object v1

    :catch_0
    move-exception v0

    invoke-virtual {v0}, Landroid/os/RemoteException;->printStackTrace()V

    const/4 v1, 0x0

    goto :goto_0
.end method

.method public atvSetAutoTuningEnd()Z
    .locals 2

    :try_start_0
    iget-object v1, p0, Lcom/mstar/tv/service/skin/ChannelSkin;->iTvServiceChannel:Lcom/mstar/tv/service/interfaces/ITvServiceServerChannel;

    invoke-interface {v1}, Lcom/mstar/tv/service/interfaces/ITvServiceServerChannel;->atvSetAutoTuningEnd()Z
    :try_end_0
    .catch Landroid/os/RemoteException; {:try_start_0 .. :try_end_0} :catch_0

    move-result v1

    :goto_0
    return v1

    :catch_0
    move-exception v0

    invoke-virtual {v0}, Landroid/os/RemoteException;->printStackTrace()V

    const/4 v1, 0x0

    goto :goto_0
.end method

.method public atvSetAutoTuningPause()Z
    .locals 2

    :try_start_0
    iget-object v1, p0, Lcom/mstar/tv/service/skin/ChannelSkin;->iTvServiceChannel:Lcom/mstar/tv/service/interfaces/ITvServiceServerChannel;

    invoke-interface {v1}, Lcom/mstar/tv/service/interfaces/ITvServiceServerChannel;->atvSetAutoTuningPause()Z
    :try_end_0
    .catch Landroid/os/RemoteException; {:try_start_0 .. :try_end_0} :catch_0

    move-result v1

    :goto_0
    return v1

    :catch_0
    move-exception v0

    invoke-virtual {v0}, Landroid/os/RemoteException;->printStackTrace()V

    const/4 v1, 0x0

    goto :goto_0
.end method

.method public atvSetAutoTuningResume()Z
    .locals 2

    :try_start_0
    iget-object v1, p0, Lcom/mstar/tv/service/skin/ChannelSkin;->iTvServiceChannel:Lcom/mstar/tv/service/interfaces/ITvServiceServerChannel;

    invoke-interface {v1}, Lcom/mstar/tv/service/interfaces/ITvServiceServerChannel;->atvSetAutoTuningResume()Z
    :try_end_0
    .catch Landroid/os/RemoteException; {:try_start_0 .. :try_end_0} :catch_0

    move-result v1

    :goto_0
    return v1

    :catch_0
    move-exception v0

    invoke-virtual {v0}, Landroid/os/RemoteException;->printStackTrace()V

    const/4 v1, 0x0

    goto :goto_0
.end method

.method public atvSetAutoTuningStart(III)Z
    .locals 2
    .param p1    # I
    .param p2    # I
    .param p3    # I

    :try_start_0
    iget-object v1, p0, Lcom/mstar/tv/service/skin/ChannelSkin;->iTvServiceChannel:Lcom/mstar/tv/service/interfaces/ITvServiceServerChannel;

    invoke-interface {v1, p1, p2, p3}, Lcom/mstar/tv/service/interfaces/ITvServiceServerChannel;->atvSetAutoTuningStart(III)Z
    :try_end_0
    .catch Landroid/os/RemoteException; {:try_start_0 .. :try_end_0} :catch_0

    move-result v1

    :goto_0
    return v1

    :catch_0
    move-exception v0

    invoke-virtual {v0}, Landroid/os/RemoteException;->printStackTrace()V

    const/4 v1, 0x0

    goto :goto_0
.end method

.method public atvSetChannel(IZ)I
    .locals 2
    .param p1    # I
    .param p2    # Z

    :try_start_0
    iget-object v1, p0, Lcom/mstar/tv/service/skin/ChannelSkin;->iTvServiceChannel:Lcom/mstar/tv/service/interfaces/ITvServiceServerChannel;

    invoke-interface {v1, p1, p2}, Lcom/mstar/tv/service/interfaces/ITvServiceServerChannel;->atvSetChannel(IZ)I
    :try_end_0
    .catch Landroid/os/RemoteException; {:try_start_0 .. :try_end_0} :catch_0

    move-result v1

    :goto_0
    return v1

    :catch_0
    move-exception v0

    invoke-virtual {v0}, Landroid/os/RemoteException;->printStackTrace()V

    const/4 v1, -0x1

    goto :goto_0
.end method

.method public atvSetForceSoundSystem(Lcom/mstar/tv/service/aidl/EN_ATV_SYSTEM_STANDARD;)Z
    .locals 2
    .param p1    # Lcom/mstar/tv/service/aidl/EN_ATV_SYSTEM_STANDARD;

    :try_start_0
    iget-object v1, p0, Lcom/mstar/tv/service/skin/ChannelSkin;->iTvServiceChannel:Lcom/mstar/tv/service/interfaces/ITvServiceServerChannel;

    invoke-interface {v1, p1}, Lcom/mstar/tv/service/interfaces/ITvServiceServerChannel;->atvSetForceSoundSystem(Lcom/mstar/tv/service/aidl/EN_ATV_SYSTEM_STANDARD;)Z
    :try_end_0
    .catch Landroid/os/RemoteException; {:try_start_0 .. :try_end_0} :catch_0

    move-result v1

    :goto_0
    return v1

    :catch_0
    move-exception v0

    invoke-virtual {v0}, Landroid/os/RemoteException;->printStackTrace()V

    const/4 v1, 0x0

    goto :goto_0
.end method

.method public atvSetForceVedioSystem(Lcom/mstar/tv/service/aidl/EN_AVD_VIDEO_STANDARD_TYPE;)Z
    .locals 2
    .param p1    # Lcom/mstar/tv/service/aidl/EN_AVD_VIDEO_STANDARD_TYPE;

    :try_start_0
    iget-object v1, p0, Lcom/mstar/tv/service/skin/ChannelSkin;->iTvServiceChannel:Lcom/mstar/tv/service/interfaces/ITvServiceServerChannel;

    invoke-interface {v1, p1}, Lcom/mstar/tv/service/interfaces/ITvServiceServerChannel;->atvSetForceVedioSystem(Lcom/mstar/tv/service/aidl/EN_AVD_VIDEO_STANDARD_TYPE;)Z
    :try_end_0
    .catch Landroid/os/RemoteException; {:try_start_0 .. :try_end_0} :catch_0

    move-result v1

    :goto_0
    return v1

    :catch_0
    move-exception v0

    invoke-virtual {v0}, Landroid/os/RemoteException;->printStackTrace()V

    const/4 v1, 0x0

    goto :goto_0
.end method

.method public atvSetFrequency(I)Z
    .locals 2
    .param p1    # I

    :try_start_0
    iget-object v1, p0, Lcom/mstar/tv/service/skin/ChannelSkin;->iTvServiceChannel:Lcom/mstar/tv/service/interfaces/ITvServiceServerChannel;

    invoke-interface {v1, p1}, Lcom/mstar/tv/service/interfaces/ITvServiceServerChannel;->atvSetFrequency(I)Z
    :try_end_0
    .catch Landroid/os/RemoteException; {:try_start_0 .. :try_end_0} :catch_0

    move-result v1

    :goto_0
    return v1

    :catch_0
    move-exception v0

    invoke-virtual {v0}, Landroid/os/RemoteException;->printStackTrace()V

    const/4 v1, 0x0

    goto :goto_0
.end method

.method public atvSetManualTuningEnd()V
    .locals 2

    :try_start_0
    iget-object v1, p0, Lcom/mstar/tv/service/skin/ChannelSkin;->iTvServiceChannel:Lcom/mstar/tv/service/interfaces/ITvServiceServerChannel;

    invoke-interface {v1}, Lcom/mstar/tv/service/interfaces/ITvServiceServerChannel;->atvSetManualTuningEnd()V
    :try_end_0
    .catch Landroid/os/RemoteException; {:try_start_0 .. :try_end_0} :catch_0

    :goto_0
    return-void

    :catch_0
    move-exception v0

    invoke-virtual {v0}, Landroid/os/RemoteException;->printStackTrace()V

    goto :goto_0
.end method

.method public atvSetManualTuningStart(IILcom/mstar/tv/service/aidl/EN_ATV_MANUAL_TUNE_MODE;)Z
    .locals 2
    .param p1    # I
    .param p2    # I
    .param p3    # Lcom/mstar/tv/service/aidl/EN_ATV_MANUAL_TUNE_MODE;

    :try_start_0
    iget-object v1, p0, Lcom/mstar/tv/service/skin/ChannelSkin;->iTvServiceChannel:Lcom/mstar/tv/service/interfaces/ITvServiceServerChannel;

    invoke-interface {v1, p1, p2, p3}, Lcom/mstar/tv/service/interfaces/ITvServiceServerChannel;->atvSetManualTuningStart(IILcom/mstar/tv/service/aidl/EN_ATV_MANUAL_TUNE_MODE;)Z
    :try_end_0
    .catch Landroid/os/RemoteException; {:try_start_0 .. :try_end_0} :catch_0

    move-result v1

    :goto_0
    return v1

    :catch_0
    move-exception v0

    invoke-virtual {v0}, Landroid/os/RemoteException;->printStackTrace()V

    const/4 v1, 0x0

    goto :goto_0
.end method

.method public atvSetProgramInfo(Lcom/mstar/tv/service/aidl/EN_SET_PROGRAM_INFO;IILjava/lang/String;)I
    .locals 2
    .param p1    # Lcom/mstar/tv/service/aidl/EN_SET_PROGRAM_INFO;
    .param p2    # I
    .param p3    # I
    .param p4    # Ljava/lang/String;

    :try_start_0
    iget-object v1, p0, Lcom/mstar/tv/service/skin/ChannelSkin;->iTvServiceChannel:Lcom/mstar/tv/service/interfaces/ITvServiceServerChannel;

    invoke-interface {v1, p1, p2, p3, p4}, Lcom/mstar/tv/service/interfaces/ITvServiceServerChannel;->atvSetProgramInfo(Lcom/mstar/tv/service/aidl/EN_SET_PROGRAM_INFO;IILjava/lang/String;)I
    :try_end_0
    .catch Landroid/os/RemoteException; {:try_start_0 .. :try_end_0} :catch_0

    move-result v1

    :goto_0
    return v1

    :catch_0
    move-exception v0

    invoke-virtual {v0}, Landroid/os/RemoteException;->printStackTrace()V

    const/4 v1, -0x1

    goto :goto_0
.end method

.method public changeToFirstService(Lcom/mstar/tv/service/aidl/EN_FIRST_SERVICE_INPUT_TYPE;Lcom/mstar/tv/service/aidl/EN_FIRST_SERVICE_TYPE;)Z
    .locals 2
    .param p1    # Lcom/mstar/tv/service/aidl/EN_FIRST_SERVICE_INPUT_TYPE;
    .param p2    # Lcom/mstar/tv/service/aidl/EN_FIRST_SERVICE_TYPE;

    :try_start_0
    iget-object v1, p0, Lcom/mstar/tv/service/skin/ChannelSkin;->iTvServiceChannel:Lcom/mstar/tv/service/interfaces/ITvServiceServerChannel;

    invoke-interface {v1, p1, p2}, Lcom/mstar/tv/service/interfaces/ITvServiceServerChannel;->changeToFirstService(Lcom/mstar/tv/service/aidl/EN_FIRST_SERVICE_INPUT_TYPE;Lcom/mstar/tv/service/aidl/EN_FIRST_SERVICE_TYPE;)Z
    :try_end_0
    .catch Landroid/os/RemoteException; {:try_start_0 .. :try_end_0} :catch_0

    move-result v1

    :goto_0
    return v1

    :catch_0
    move-exception v0

    invoke-virtual {v0}, Landroid/os/RemoteException;->printStackTrace()V

    const/4 v1, 0x0

    goto :goto_0
.end method

.method public closeSubtitle()Z
    .locals 2

    :try_start_0
    iget-object v1, p0, Lcom/mstar/tv/service/skin/ChannelSkin;->iTvServiceChannel:Lcom/mstar/tv/service/interfaces/ITvServiceServerChannel;

    invoke-interface {v1}, Lcom/mstar/tv/service/interfaces/ITvServiceServerChannel;->closeSubtitle()Z
    :try_end_0
    .catch Landroid/os/RemoteException; {:try_start_0 .. :try_end_0} :catch_0

    move-result v1

    :goto_0
    return v1

    :catch_0
    move-exception v0

    invoke-virtual {v0}, Landroid/os/RemoteException;->printStackTrace()V

    const/4 v1, 0x0

    goto :goto_0
.end method

.method public connect(Landroid/os/Handler;)Z
    .locals 5
    .param p1    # Landroid/os/Handler;

    const/4 v4, 0x0

    iput-object p1, p0, Lcom/mstar/tv/service/skin/ChannelSkin;->handler:Landroid/os/Handler;

    const-string v3, "tv_services"

    invoke-static {v3}, Landroid/os/ServiceManager;->getService(Ljava/lang/String;)Landroid/os/IBinder;

    move-result-object v1

    invoke-static {v1}, Lcom/mstar/tv/service/interfaces/ITvServiceServer$Stub;->asInterface(Landroid/os/IBinder;)Lcom/mstar/tv/service/interfaces/ITvServiceServer;

    move-result-object v2

    if-eqz v2, :cond_0

    :try_start_0
    invoke-interface {v2}, Lcom/mstar/tv/service/interfaces/ITvServiceServer;->getChannelManager()Lcom/mstar/tv/service/interfaces/ITvServiceServerChannel;

    move-result-object v3

    iput-object v3, p0, Lcom/mstar/tv/service/skin/ChannelSkin;->iTvServiceChannel:Lcom/mstar/tv/service/interfaces/ITvServiceServerChannel;

    const/4 v3, 0x1

    iput-boolean v3, p0, Lcom/mstar/tv/service/skin/ChannelSkin;->isBindOk:Z
    :try_end_0
    .catch Landroid/os/RemoteException; {:try_start_0 .. :try_end_0} :catch_0

    :goto_0
    iget-boolean v3, p0, Lcom/mstar/tv/service/skin/ChannelSkin;->isBindOk:Z

    return v3

    :catch_0
    move-exception v0

    invoke-virtual {v0}, Landroid/os/RemoteException;->printStackTrace()V

    iput-boolean v4, p0, Lcom/mstar/tv/service/skin/ChannelSkin;->isBindOk:Z

    goto :goto_0

    :cond_0
    iput-boolean v4, p0, Lcom/mstar/tv/service/skin/ChannelSkin;->isBindOk:Z

    goto :goto_0
.end method

.method public deleteProgramFromFavorite(Lcom/mstar/tv/service/aidl/EN_FAVORITE_ID;III)V
    .locals 2
    .param p1    # Lcom/mstar/tv/service/aidl/EN_FAVORITE_ID;
    .param p2    # I
    .param p3    # I
    .param p4    # I

    :try_start_0
    iget-object v1, p0, Lcom/mstar/tv/service/skin/ChannelSkin;->iTvServiceChannel:Lcom/mstar/tv/service/interfaces/ITvServiceServerChannel;

    invoke-interface {v1, p1, p2, p3, p4}, Lcom/mstar/tv/service/interfaces/ITvServiceServerChannel;->deleteProgramFromFavorite(Lcom/mstar/tv/service/aidl/EN_FAVORITE_ID;III)V
    :try_end_0
    .catch Landroid/os/RemoteException; {:try_start_0 .. :try_end_0} :catch_0

    :goto_0
    return-void

    :catch_0
    move-exception v0

    invoke-virtual {v0}, Landroid/os/RemoteException;->printStackTrace()V

    goto :goto_0
.end method

.method public disconnect()V
    .locals 0

    return-void
.end method

.method public dtvAutoScan()Z
    .locals 2

    :try_start_0
    iget-object v1, p0, Lcom/mstar/tv/service/skin/ChannelSkin;->iTvServiceChannel:Lcom/mstar/tv/service/interfaces/ITvServiceServerChannel;

    invoke-interface {v1}, Lcom/mstar/tv/service/interfaces/ITvServiceServerChannel;->dtvAutoScan()Z
    :try_end_0
    .catch Landroid/os/RemoteException; {:try_start_0 .. :try_end_0} :catch_0

    move-result v1

    :goto_0
    return v1

    :catch_0
    move-exception v0

    invoke-virtual {v0}, Landroid/os/RemoteException;->printStackTrace()V

    const/4 v1, 0x0

    goto :goto_0
.end method

.method public dtvChangeManualScanRF(I)Z
    .locals 2
    .param p1    # I

    :try_start_0
    iget-object v1, p0, Lcom/mstar/tv/service/skin/ChannelSkin;->iTvServiceChannel:Lcom/mstar/tv/service/interfaces/ITvServiceServerChannel;

    invoke-interface {v1, p1}, Lcom/mstar/tv/service/interfaces/ITvServiceServerChannel;->dtvChangeManualScanRF(I)Z
    :try_end_0
    .catch Landroid/os/RemoteException; {:try_start_0 .. :try_end_0} :catch_0

    move-result v1

    :goto_0
    return v1

    :catch_0
    move-exception v0

    invoke-virtual {v0}, Landroid/os/RemoteException;->printStackTrace()V

    const/4 v1, 0x0

    goto :goto_0
.end method

.method public dtvFullScan()Z
    .locals 2

    :try_start_0
    iget-object v1, p0, Lcom/mstar/tv/service/skin/ChannelSkin;->iTvServiceChannel:Lcom/mstar/tv/service/interfaces/ITvServiceServerChannel;

    invoke-interface {v1}, Lcom/mstar/tv/service/interfaces/ITvServiceServerChannel;->dtvFullScan()Z
    :try_end_0
    .catch Landroid/os/RemoteException; {:try_start_0 .. :try_end_0} :catch_0

    move-result v1

    :goto_0
    return v1

    :catch_0
    move-exception v0

    invoke-virtual {v0}, Landroid/os/RemoteException;->printStackTrace()V

    const/4 v1, 0x0

    goto :goto_0
.end method

.method public dtvGetAntennaType()Lcom/mstar/tv/service/aidl/EN_ANTENNA_TYPE;
    .locals 2

    invoke-static {}, Lcom/mstar/tv/service/aidl/EN_ANTENNA_TYPE;->values()[Lcom/mstar/tv/service/aidl/EN_ANTENNA_TYPE;

    move-result-object v0

    invoke-virtual {p0}, Lcom/mstar/tv/service/skin/ChannelSkin;->getMSrvDtvRoute()I

    move-result v1

    aget-object v0, v0, v1

    return-object v0
.end method

.method public dtvManualScanFreq(I)Z
    .locals 2
    .param p1    # I

    :try_start_0
    iget-object v1, p0, Lcom/mstar/tv/service/skin/ChannelSkin;->iTvServiceChannel:Lcom/mstar/tv/service/interfaces/ITvServiceServerChannel;

    invoke-interface {v1, p1}, Lcom/mstar/tv/service/interfaces/ITvServiceServerChannel;->dtvManualScanFreq(I)Z
    :try_end_0
    .catch Landroid/os/RemoteException; {:try_start_0 .. :try_end_0} :catch_0

    move-result v1

    :goto_0
    return v1

    :catch_0
    move-exception v0

    invoke-virtual {v0}, Landroid/os/RemoteException;->printStackTrace()V

    const/4 v1, 0x0

    goto :goto_0
.end method

.method public dtvManualScanRF(I)Z
    .locals 2
    .param p1    # I

    :try_start_0
    iget-object v1, p0, Lcom/mstar/tv/service/skin/ChannelSkin;->iTvServiceChannel:Lcom/mstar/tv/service/interfaces/ITvServiceServerChannel;

    invoke-interface {v1, p1}, Lcom/mstar/tv/service/interfaces/ITvServiceServerChannel;->dtvManualScanRF(I)Z
    :try_end_0
    .catch Landroid/os/RemoteException; {:try_start_0 .. :try_end_0} :catch_0

    move-result v1

    :goto_0
    return v1

    :catch_0
    move-exception v0

    invoke-virtual {v0}, Landroid/os/RemoteException;->printStackTrace()V

    const/4 v1, 0x0

    goto :goto_0
.end method

.method public dtvPauseScan()Z
    .locals 2

    :try_start_0
    iget-object v1, p0, Lcom/mstar/tv/service/skin/ChannelSkin;->iTvServiceChannel:Lcom/mstar/tv/service/interfaces/ITvServiceServerChannel;

    invoke-interface {v1}, Lcom/mstar/tv/service/interfaces/ITvServiceServerChannel;->dtvPauseScan()Z
    :try_end_0
    .catch Landroid/os/RemoteException; {:try_start_0 .. :try_end_0} :catch_0

    move-result v1

    :goto_0
    return v1

    :catch_0
    move-exception v0

    invoke-virtual {v0}, Landroid/os/RemoteException;->printStackTrace()V

    const/4 v1, 0x0

    goto :goto_0
.end method

.method public dtvResumeScan()Z
    .locals 2

    :try_start_0
    iget-object v1, p0, Lcom/mstar/tv/service/skin/ChannelSkin;->iTvServiceChannel:Lcom/mstar/tv/service/interfaces/ITvServiceServerChannel;

    invoke-interface {v1}, Lcom/mstar/tv/service/interfaces/ITvServiceServerChannel;->dtvResumeScan()Z
    :try_end_0
    .catch Landroid/os/RemoteException; {:try_start_0 .. :try_end_0} :catch_0

    move-result v1

    :goto_0
    return v1

    :catch_0
    move-exception v0

    invoke-virtual {v0}, Landroid/os/RemoteException;->printStackTrace()V

    const/4 v1, 0x0

    goto :goto_0
.end method

.method public dtvSetAntennaType(Lcom/mstar/tv/service/aidl/EN_ANTENNA_TYPE;)V
    .locals 1
    .param p1    # Lcom/mstar/tv/service/aidl/EN_ANTENNA_TYPE;

    invoke-virtual {p1}, Lcom/mstar/tv/service/aidl/EN_ANTENNA_TYPE;->ordinal()I

    move-result v0

    int-to-short v0, v0

    invoke-virtual {p0, v0}, Lcom/mstar/tv/service/skin/ChannelSkin;->switchMSrvDtvRouteCmd(I)Z

    return-void
.end method

.method public dtvStartManualScan()Z
    .locals 2

    :try_start_0
    iget-object v1, p0, Lcom/mstar/tv/service/skin/ChannelSkin;->iTvServiceChannel:Lcom/mstar/tv/service/interfaces/ITvServiceServerChannel;

    invoke-interface {v1}, Lcom/mstar/tv/service/interfaces/ITvServiceServerChannel;->dtvStartManualScan()Z
    :try_end_0
    .catch Landroid/os/RemoteException; {:try_start_0 .. :try_end_0} :catch_0

    move-result v1

    :goto_0
    return v1

    :catch_0
    move-exception v0

    invoke-virtual {v0}, Landroid/os/RemoteException;->printStackTrace()V

    const/4 v1, 0x0

    goto :goto_0
.end method

.method public dtvplayCurrentProgram()Z
    .locals 2

    :try_start_0
    iget-object v1, p0, Lcom/mstar/tv/service/skin/ChannelSkin;->iTvServiceChannel:Lcom/mstar/tv/service/interfaces/ITvServiceServerChannel;

    invoke-interface {v1}, Lcom/mstar/tv/service/interfaces/ITvServiceServerChannel;->dtvplayCurrentProgram()Z
    :try_end_0
    .catch Landroid/os/RemoteException; {:try_start_0 .. :try_end_0} :catch_0

    move-result v1

    :goto_0
    return v1

    :catch_0
    move-exception v0

    invoke-virtual {v0}, Landroid/os/RemoteException;->printStackTrace()V

    const/4 v1, 0x0

    goto :goto_0
.end method

.method public dvbcgetScanParam(Lcom/mstar/tv/service/aidl/DvbcScanParam;)Z
    .locals 2
    .param p1    # Lcom/mstar/tv/service/aidl/DvbcScanParam;

    :try_start_0
    iget-object v1, p0, Lcom/mstar/tv/service/skin/ChannelSkin;->iTvServiceChannel:Lcom/mstar/tv/service/interfaces/ITvServiceServerChannel;

    invoke-interface {v1, p1}, Lcom/mstar/tv/service/interfaces/ITvServiceServerChannel;->dvbcgetScanParam(Lcom/mstar/tv/service/aidl/DvbcScanParam;)Z
    :try_end_0
    .catch Landroid/os/RemoteException; {:try_start_0 .. :try_end_0} :catch_0

    move-result v1

    :goto_0
    return v1

    :catch_0
    move-exception v0

    invoke-virtual {v0}, Landroid/os/RemoteException;->printStackTrace()V

    const/4 v1, 0x0

    goto :goto_0
.end method

.method public dvbcsetScanParam(ILcom/mstar/tv/service/aidl/EN_CAB_CONSTEL_TYPE;III)Z
    .locals 7
    .param p1    # I
    .param p2    # Lcom/mstar/tv/service/aidl/EN_CAB_CONSTEL_TYPE;
    .param p3    # I
    .param p4    # I
    .param p5    # I

    :try_start_0
    iget-object v0, p0, Lcom/mstar/tv/service/skin/ChannelSkin;->iTvServiceChannel:Lcom/mstar/tv/service/interfaces/ITvServiceServerChannel;

    move v1, p1

    move-object v2, p2

    move v3, p3

    move v4, p4

    move v5, p5

    invoke-interface/range {v0 .. v5}, Lcom/mstar/tv/service/interfaces/ITvServiceServerChannel;->dvbcsetScanParam(ILcom/mstar/tv/service/aidl/EN_CAB_CONSTEL_TYPE;III)Z
    :try_end_0
    .catch Landroid/os/RemoteException; {:try_start_0 .. :try_end_0} :catch_0

    move-result v0

    :goto_0
    return v0

    :catch_0
    move-exception v6

    invoke-virtual {v6}, Landroid/os/RemoteException;->printStackTrace()V

    const/4 v0, 0x0

    goto :goto_0
.end method

.method public getAtvStationName(I)Ljava/lang/String;
    .locals 2
    .param p1    # I

    :try_start_0
    iget-object v1, p0, Lcom/mstar/tv/service/skin/ChannelSkin;->iTvServiceChannel:Lcom/mstar/tv/service/interfaces/ITvServiceServerChannel;

    invoke-interface {v1, p1}, Lcom/mstar/tv/service/interfaces/ITvServiceServerChannel;->getAtvStationName(I)Ljava/lang/String;
    :try_end_0
    .catch Landroid/os/RemoteException; {:try_start_0 .. :try_end_0} :catch_0

    move-result-object v1

    :goto_0
    return-object v1

    :catch_0
    move-exception v0

    invoke-virtual {v0}, Landroid/os/RemoteException;->printStackTrace()V

    const/4 v1, 0x0

    goto :goto_0
.end method

.method public getCurrProgramInfo()Lcom/mstar/tv/service/aidl/ProgramInfo;
    .locals 2

    :try_start_0
    iget-object v1, p0, Lcom/mstar/tv/service/skin/ChannelSkin;->iTvServiceChannel:Lcom/mstar/tv/service/interfaces/ITvServiceServerChannel;

    invoke-interface {v1}, Lcom/mstar/tv/service/interfaces/ITvServiceServerChannel;->getCurrProgramInfo()Lcom/mstar/tv/service/aidl/ProgramInfo;
    :try_end_0
    .catch Landroid/os/RemoteException; {:try_start_0 .. :try_end_0} :catch_0

    move-result-object v1

    :goto_0
    return-object v1

    :catch_0
    move-exception v0

    invoke-virtual {v0}, Landroid/os/RemoteException;->printStackTrace()V

    const/4 v1, 0x0

    goto :goto_0
.end method

.method public getCurrentChannelNumber()I
    .locals 2

    :try_start_0
    iget-object v1, p0, Lcom/mstar/tv/service/skin/ChannelSkin;->iTvServiceChannel:Lcom/mstar/tv/service/interfaces/ITvServiceServerChannel;

    invoke-interface {v1}, Lcom/mstar/tv/service/interfaces/ITvServiceServerChannel;->getCurrentChannelNumber()I
    :try_end_0
    .catch Landroid/os/RemoteException; {:try_start_0 .. :try_end_0} :catch_0

    move-result v1

    :goto_0
    return v1

    :catch_0
    move-exception v0

    invoke-virtual {v0}, Landroid/os/RemoteException;->printStackTrace()V

    const/4 v1, -0x1

    goto :goto_0
.end method

.method public getCurrentLanguageIndex(Ljava/lang/String;)Lcom/mstar/tv/service/aidl/EN_MS_LANGUAGE;
    .locals 2
    .param p1    # Ljava/lang/String;

    :try_start_0
    iget-object v1, p0, Lcom/mstar/tv/service/skin/ChannelSkin;->iTvServiceChannel:Lcom/mstar/tv/service/interfaces/ITvServiceServerChannel;

    invoke-interface {v1, p1}, Lcom/mstar/tv/service/interfaces/ITvServiceServerChannel;->getCurrentLanguageIndex(Ljava/lang/String;)Lcom/mstar/tv/service/aidl/EN_MS_LANGUAGE;
    :try_end_0
    .catch Landroid/os/RemoteException; {:try_start_0 .. :try_end_0} :catch_0

    move-result-object v1

    :goto_0
    return-object v1

    :catch_0
    move-exception v0

    invoke-virtual {v0}, Landroid/os/RemoteException;->printStackTrace()V

    const/4 v1, 0x0

    goto :goto_0
.end method

.method public getCurrentMuxInfo()Lcom/mstar/tv/service/aidl/DvbMuxInfo;
    .locals 2

    :try_start_0
    iget-object v1, p0, Lcom/mstar/tv/service/skin/ChannelSkin;->iTvServiceChannel:Lcom/mstar/tv/service/interfaces/ITvServiceServerChannel;

    invoke-interface {v1}, Lcom/mstar/tv/service/interfaces/ITvServiceServerChannel;->getCurrentMuxInfo()Lcom/mstar/tv/service/aidl/DvbMuxInfo;
    :try_end_0
    .catch Landroid/os/RemoteException; {:try_start_0 .. :try_end_0} :catch_0

    move-result-object v1

    :goto_0
    return-object v1

    :catch_0
    move-exception v0

    invoke-virtual {v0}, Landroid/os/RemoteException;->printStackTrace()V

    const/4 v1, 0x0

    goto :goto_0
.end method

.method public getMSrvDtvRoute()I
    .locals 2

    :try_start_0
    iget-object v1, p0, Lcom/mstar/tv/service/skin/ChannelSkin;->iTvServiceChannel:Lcom/mstar/tv/service/interfaces/ITvServiceServerChannel;

    invoke-interface {v1}, Lcom/mstar/tv/service/interfaces/ITvServiceServerChannel;->getMSrvDtvRoute()I
    :try_end_0
    .catch Landroid/os/RemoteException; {:try_start_0 .. :try_end_0} :catch_0

    move-result v1

    :goto_0
    return v1

    :catch_0
    move-exception v0

    invoke-virtual {v0}, Landroid/os/RemoteException;->printStackTrace()V

    const/4 v1, -0x1

    goto :goto_0
.end method

.method public getProgramAttribute(Lcom/mstar/tv/service/aidl/EN_PROGRAM_ATTRIBUTE;IIIZ)Z
    .locals 7
    .param p1    # Lcom/mstar/tv/service/aidl/EN_PROGRAM_ATTRIBUTE;
    .param p2    # I
    .param p3    # I
    .param p4    # I
    .param p5    # Z

    :try_start_0
    iget-object v0, p0, Lcom/mstar/tv/service/skin/ChannelSkin;->iTvServiceChannel:Lcom/mstar/tv/service/interfaces/ITvServiceServerChannel;

    move-object v1, p1

    move v2, p2

    move v3, p3

    move v4, p4

    move v5, p5

    invoke-interface/range {v0 .. v5}, Lcom/mstar/tv/service/interfaces/ITvServiceServerChannel;->getProgramAttribute(Lcom/mstar/tv/service/aidl/EN_PROGRAM_ATTRIBUTE;IIIZ)Z
    :try_end_0
    .catch Landroid/os/RemoteException; {:try_start_0 .. :try_end_0} :catch_0

    move-result v0

    :goto_0
    return v0

    :catch_0
    move-exception v6

    invoke-virtual {v6}, Landroid/os/RemoteException;->printStackTrace()V

    const/4 v0, 0x0

    goto :goto_0
.end method

.method public getProgramCount(Lcom/mstar/tv/service/aidl/EN_PROGRAM_COUNT_TYPE;)I
    .locals 2
    .param p1    # Lcom/mstar/tv/service/aidl/EN_PROGRAM_COUNT_TYPE;

    :try_start_0
    iget-object v1, p0, Lcom/mstar/tv/service/skin/ChannelSkin;->iTvServiceChannel:Lcom/mstar/tv/service/interfaces/ITvServiceServerChannel;

    invoke-interface {v1, p1}, Lcom/mstar/tv/service/interfaces/ITvServiceServerChannel;->getProgramCount(Lcom/mstar/tv/service/aidl/EN_PROGRAM_COUNT_TYPE;)I
    :try_end_0
    .catch Landroid/os/RemoteException; {:try_start_0 .. :try_end_0} :catch_0

    move-result v1

    :goto_0
    return v1

    :catch_0
    move-exception v0

    invoke-virtual {v0}, Landroid/os/RemoteException;->printStackTrace()V

    const/4 v1, -0x1

    goto :goto_0
.end method

.method public getProgramCtrl(Lcom/mstar/tv/service/aidl/EN_GET_PROGRAM_CTRL;IILjava/lang/String;)I
    .locals 2
    .param p1    # Lcom/mstar/tv/service/aidl/EN_GET_PROGRAM_CTRL;
    .param p2    # I
    .param p3    # I
    .param p4    # Ljava/lang/String;

    :try_start_0
    iget-object v1, p0, Lcom/mstar/tv/service/skin/ChannelSkin;->iTvServiceChannel:Lcom/mstar/tv/service/interfaces/ITvServiceServerChannel;

    invoke-interface {v1, p1, p2, p3, p4}, Lcom/mstar/tv/service/interfaces/ITvServiceServerChannel;->getProgramCtrl(Lcom/mstar/tv/service/aidl/EN_GET_PROGRAM_CTRL;IILjava/lang/String;)I
    :try_end_0
    .catch Landroid/os/RemoteException; {:try_start_0 .. :try_end_0} :catch_0

    move-result v1

    :goto_0
    return v1

    :catch_0
    move-exception v0

    invoke-virtual {v0}, Landroid/os/RemoteException;->printStackTrace()V

    const/4 v1, -0x1

    goto :goto_0
.end method

.method public getProgramInfo(Lcom/mstar/tv/service/aidl/ProgramInfoQueryCriteria;Lcom/mstar/tv/service/aidl/EN_PROGRAM_INFO_TYPE;)Lcom/mstar/tv/service/aidl/ProgramInfo;
    .locals 2
    .param p1    # Lcom/mstar/tv/service/aidl/ProgramInfoQueryCriteria;
    .param p2    # Lcom/mstar/tv/service/aidl/EN_PROGRAM_INFO_TYPE;

    :try_start_0
    iget-object v1, p0, Lcom/mstar/tv/service/skin/ChannelSkin;->iTvServiceChannel:Lcom/mstar/tv/service/interfaces/ITvServiceServerChannel;

    invoke-interface {v1, p1, p2}, Lcom/mstar/tv/service/interfaces/ITvServiceServerChannel;->getProgramInfo(Lcom/mstar/tv/service/aidl/ProgramInfoQueryCriteria;Lcom/mstar/tv/service/aidl/EN_PROGRAM_INFO_TYPE;)Lcom/mstar/tv/service/aidl/ProgramInfo;
    :try_end_0
    .catch Landroid/os/RemoteException; {:try_start_0 .. :try_end_0} :catch_0

    move-result-object v1

    :goto_0
    return-object v1

    :catch_0
    move-exception v0

    invoke-virtual {v0}, Landroid/os/RemoteException;->printStackTrace()V

    const/4 v1, 0x0

    goto :goto_0
.end method

.method public getProgramName(ILcom/mstar/tv/service/aidl/EN_MEMBER_SERVICE_TYPE;I)Ljava/lang/String;
    .locals 2
    .param p1    # I
    .param p2    # Lcom/mstar/tv/service/aidl/EN_MEMBER_SERVICE_TYPE;
    .param p3    # I

    :try_start_0
    iget-object v1, p0, Lcom/mstar/tv/service/skin/ChannelSkin;->iTvServiceChannel:Lcom/mstar/tv/service/interfaces/ITvServiceServerChannel;

    invoke-interface {v1, p1, p2, p3}, Lcom/mstar/tv/service/interfaces/ITvServiceServerChannel;->getProgramName(ILcom/mstar/tv/service/aidl/EN_MEMBER_SERVICE_TYPE;I)Ljava/lang/String;
    :try_end_0
    .catch Landroid/os/RemoteException; {:try_start_0 .. :try_end_0} :catch_0

    move-result-object v1

    :goto_0
    return-object v1

    :catch_0
    move-exception v0

    invoke-virtual {v0}, Landroid/os/RemoteException;->printStackTrace()V

    const/4 v1, 0x0

    goto :goto_0
.end method

.method public getSIFMtsMode()Lcom/mstar/tv/service/aidl/EN_ATV_AUDIO_MODE_TYPE;
    .locals 2

    :try_start_0
    iget-object v1, p0, Lcom/mstar/tv/service/skin/ChannelSkin;->iTvServiceChannel:Lcom/mstar/tv/service/interfaces/ITvServiceServerChannel;

    invoke-interface {v1}, Lcom/mstar/tv/service/interfaces/ITvServiceServerChannel;->getSIFMtsMode()Lcom/mstar/tv/service/aidl/EN_ATV_AUDIO_MODE_TYPE;
    :try_end_0
    .catch Landroid/os/RemoteException; {:try_start_0 .. :try_end_0} :catch_0

    move-result-object v1

    :goto_0
    return-object v1

    :catch_0
    move-exception v0

    invoke-virtual {v0}, Landroid/os/RemoteException;->printStackTrace()V

    const/4 v1, 0x0

    goto :goto_0
.end method

.method public getSubtitleInfo()Lcom/mstar/tv/service/aidl/DtvSubtitleInfo;
    .locals 2

    :try_start_0
    iget-object v1, p0, Lcom/mstar/tv/service/skin/ChannelSkin;->iTvServiceChannel:Lcom/mstar/tv/service/interfaces/ITvServiceServerChannel;

    invoke-interface {v1}, Lcom/mstar/tv/service/interfaces/ITvServiceServerChannel;->getSubtitleInfo()Lcom/mstar/tv/service/aidl/DtvSubtitleInfo;
    :try_end_0
    .catch Landroid/os/RemoteException; {:try_start_0 .. :try_end_0} :catch_0

    move-result-object v1

    :goto_0
    return-object v1

    :catch_0
    move-exception v0

    invoke-virtual {v0}, Landroid/os/RemoteException;->printStackTrace()V

    const/4 v1, 0x0

    goto :goto_0
.end method

.method public getSystemCountry()Lcom/mstar/tv/service/aidl/EN_MEMBER_COUNTRY;
    .locals 2

    :try_start_0
    iget-object v1, p0, Lcom/mstar/tv/service/skin/ChannelSkin;->iTvServiceChannel:Lcom/mstar/tv/service/interfaces/ITvServiceServerChannel;

    invoke-interface {v1}, Lcom/mstar/tv/service/interfaces/ITvServiceServerChannel;->getSystemCountry()Lcom/mstar/tv/service/aidl/EN_MEMBER_COUNTRY;
    :try_end_0
    .catch Landroid/os/RemoteException; {:try_start_0 .. :try_end_0} :catch_0

    move-result-object v1

    :goto_0
    return-object v1

    :catch_0
    move-exception v0

    invoke-virtual {v0}, Landroid/os/RemoteException;->printStackTrace()V

    const/4 v1, 0x0

    goto :goto_0
.end method

.method public getUserScanType()Lcom/mstar/tv/service/aidl/EN_TUNING_SCAN_TYPE;
    .locals 1

    iget-object v0, p0, Lcom/mstar/tv/service/skin/ChannelSkin;->tuning_scan_type:Lcom/mstar/tv/service/aidl/EN_TUNING_SCAN_TYPE;

    return-object v0
.end method

.method public getVideoStandard()Lcom/mstar/tv/service/aidl/EN_AVD_VIDEO_STANDARD_TYPE;
    .locals 2

    :try_start_0
    iget-object v1, p0, Lcom/mstar/tv/service/skin/ChannelSkin;->iTvServiceChannel:Lcom/mstar/tv/service/interfaces/ITvServiceServerChannel;

    invoke-interface {v1}, Lcom/mstar/tv/service/interfaces/ITvServiceServerChannel;->getVideoStandard()Lcom/mstar/tv/service/aidl/EN_AVD_VIDEO_STANDARD_TYPE;
    :try_end_0
    .catch Landroid/os/RemoteException; {:try_start_0 .. :try_end_0} :catch_0

    move-result-object v1

    :goto_0
    return-object v1

    :catch_0
    move-exception v0

    invoke-virtual {v0}, Landroid/os/RemoteException;->printStackTrace()V

    const/4 v1, 0x0

    goto :goto_0
.end method

.method public isConnectionOk()Z
    .locals 1

    iget-object v0, p0, Lcom/mstar/tv/service/skin/ChannelSkin;->iTvServiceChannel:Lcom/mstar/tv/service/interfaces/ITvServiceServerChannel;

    if-nez v0, :cond_0

    const/4 v0, 0x0

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x1

    goto :goto_0
.end method

.method public isSignalStabled()Z
    .locals 2

    :try_start_0
    iget-object v1, p0, Lcom/mstar/tv/service/skin/ChannelSkin;->iTvServiceChannel:Lcom/mstar/tv/service/interfaces/ITvServiceServerChannel;

    invoke-interface {v1}, Lcom/mstar/tv/service/interfaces/ITvServiceServerChannel;->isSignalStabled()Z
    :try_end_0
    .catch Landroid/os/RemoteException; {:try_start_0 .. :try_end_0} :catch_0

    move-result v1

    :goto_0
    return v1

    :catch_0
    move-exception v0

    invoke-virtual {v0}, Landroid/os/RemoteException;->printStackTrace()V

    const/4 v1, 0x0

    goto :goto_0
.end method

.method public makeSourceAtv()Z
    .locals 2

    :try_start_0
    iget-object v1, p0, Lcom/mstar/tv/service/skin/ChannelSkin;->iTvServiceChannel:Lcom/mstar/tv/service/interfaces/ITvServiceServerChannel;

    invoke-interface {v1}, Lcom/mstar/tv/service/interfaces/ITvServiceServerChannel;->makeSourceAtv()Z
    :try_end_0
    .catch Landroid/os/RemoteException; {:try_start_0 .. :try_end_0} :catch_0

    move-result v1

    :goto_0
    return v1

    :catch_0
    move-exception v0

    invoke-virtual {v0}, Landroid/os/RemoteException;->printStackTrace()V

    const/4 v1, 0x0

    goto :goto_0
.end method

.method public makeSourceDtv()Z
    .locals 2

    :try_start_0
    iget-object v1, p0, Lcom/mstar/tv/service/skin/ChannelSkin;->iTvServiceChannel:Lcom/mstar/tv/service/interfaces/ITvServiceServerChannel;

    invoke-interface {v1}, Lcom/mstar/tv/service/interfaces/ITvServiceServerChannel;->makeSourceDtv()Z
    :try_end_0
    .catch Landroid/os/RemoteException; {:try_start_0 .. :try_end_0} :catch_0

    move-result v1

    :goto_0
    return v1

    :catch_0
    move-exception v0

    invoke-virtual {v0}, Landroid/os/RemoteException;->printStackTrace()V

    const/4 v1, 0x0

    goto :goto_0
.end method

.method public moveProgram(II)V
    .locals 2
    .param p1    # I
    .param p2    # I

    :try_start_0
    iget-object v1, p0, Lcom/mstar/tv/service/skin/ChannelSkin;->iTvServiceChannel:Lcom/mstar/tv/service/interfaces/ITvServiceServerChannel;

    invoke-interface {v1, p1, p2}, Lcom/mstar/tv/service/interfaces/ITvServiceServerChannel;->moveProgram(II)V
    :try_end_0
    .catch Landroid/os/RemoteException; {:try_start_0 .. :try_end_0} :catch_0

    :goto_0
    return-void

    :catch_0
    move-exception v0

    invoke-virtual {v0}, Landroid/os/RemoteException;->printStackTrace()V

    goto :goto_0
.end method

.method public openSubtitle(I)Z
    .locals 2
    .param p1    # I

    :try_start_0
    iget-object v1, p0, Lcom/mstar/tv/service/skin/ChannelSkin;->iTvServiceChannel:Lcom/mstar/tv/service/interfaces/ITvServiceServerChannel;

    invoke-interface {v1, p1}, Lcom/mstar/tv/service/interfaces/ITvServiceServerChannel;->openSubtitle(I)Z
    :try_end_0
    .catch Landroid/os/RemoteException; {:try_start_0 .. :try_end_0} :catch_0

    move-result v1

    :goto_0
    return v1

    :catch_0
    move-exception v0

    invoke-virtual {v0}, Landroid/os/RemoteException;->printStackTrace()V

    const/4 v1, 0x0

    goto :goto_0
.end method

.method public programDown()Z
    .locals 2

    :try_start_0
    iget-object v1, p0, Lcom/mstar/tv/service/skin/ChannelSkin;->iTvServiceChannel:Lcom/mstar/tv/service/interfaces/ITvServiceServerChannel;

    invoke-interface {v1}, Lcom/mstar/tv/service/interfaces/ITvServiceServerChannel;->programDown()Z
    :try_end_0
    .catch Landroid/os/RemoteException; {:try_start_0 .. :try_end_0} :catch_0

    move-result v1

    :goto_0
    return v1

    :catch_0
    move-exception v0

    invoke-virtual {v0}, Landroid/os/RemoteException;->printStackTrace()V

    const/4 v1, 0x0

    goto :goto_0
.end method

.method public programReturn()Z
    .locals 2

    :try_start_0
    iget-object v1, p0, Lcom/mstar/tv/service/skin/ChannelSkin;->iTvServiceChannel:Lcom/mstar/tv/service/interfaces/ITvServiceServerChannel;

    invoke-interface {v1}, Lcom/mstar/tv/service/interfaces/ITvServiceServerChannel;->programReturn()Z
    :try_end_0
    .catch Landroid/os/RemoteException; {:try_start_0 .. :try_end_0} :catch_0

    move-result v1

    :goto_0
    return v1

    :catch_0
    move-exception v0

    invoke-virtual {v0}, Landroid/os/RemoteException;->printStackTrace()V

    const/4 v1, 0x0

    goto :goto_0
.end method

.method public programSel(ILcom/mstar/tv/service/aidl/EN_MEMBER_SERVICE_TYPE;)Z
    .locals 2
    .param p1    # I
    .param p2    # Lcom/mstar/tv/service/aidl/EN_MEMBER_SERVICE_TYPE;

    :try_start_0
    iget-object v1, p0, Lcom/mstar/tv/service/skin/ChannelSkin;->iTvServiceChannel:Lcom/mstar/tv/service/interfaces/ITvServiceServerChannel;

    invoke-interface {v1, p1, p2}, Lcom/mstar/tv/service/interfaces/ITvServiceServerChannel;->programSel(ILcom/mstar/tv/service/aidl/EN_MEMBER_SERVICE_TYPE;)Z
    :try_end_0
    .catch Landroid/os/RemoteException; {:try_start_0 .. :try_end_0} :catch_0

    move-result v1

    :goto_0
    return v1

    :catch_0
    move-exception v0

    invoke-virtual {v0}, Landroid/os/RemoteException;->printStackTrace()V

    const/4 v1, 0x0

    goto :goto_0
.end method

.method public programUp()Z
    .locals 2

    :try_start_0
    iget-object v1, p0, Lcom/mstar/tv/service/skin/ChannelSkin;->iTvServiceChannel:Lcom/mstar/tv/service/interfaces/ITvServiceServerChannel;

    invoke-interface {v1}, Lcom/mstar/tv/service/interfaces/ITvServiceServerChannel;->programUp()Z
    :try_end_0
    .catch Landroid/os/RemoteException; {:try_start_0 .. :try_end_0} :catch_0

    move-result v1

    :goto_0
    return v1

    :catch_0
    move-exception v0

    invoke-virtual {v0}, Landroid/os/RemoteException;->printStackTrace()V

    const/4 v1, 0x0

    goto :goto_0
.end method

.method public sendAtvScaninfo()V
    .locals 2

    :try_start_0
    iget-object v1, p0, Lcom/mstar/tv/service/skin/ChannelSkin;->iTvServiceChannel:Lcom/mstar/tv/service/interfaces/ITvServiceServerChannel;

    invoke-interface {v1}, Lcom/mstar/tv/service/interfaces/ITvServiceServerChannel;->sendAtvScaninfo()V
    :try_end_0
    .catch Landroid/os/RemoteException; {:try_start_0 .. :try_end_0} :catch_0

    :goto_0
    return-void

    :catch_0
    move-exception v0

    invoke-virtual {v0}, Landroid/os/RemoteException;->printStackTrace()V

    goto :goto_0
.end method

.method public sendDtvScaninfo()V
    .locals 2

    :try_start_0
    iget-object v1, p0, Lcom/mstar/tv/service/skin/ChannelSkin;->iTvServiceChannel:Lcom/mstar/tv/service/interfaces/ITvServiceServerChannel;

    invoke-interface {v1}, Lcom/mstar/tv/service/interfaces/ITvServiceServerChannel;->sendDtvScaninfo()V
    :try_end_0
    .catch Landroid/os/RemoteException; {:try_start_0 .. :try_end_0} :catch_0

    :goto_0
    return-void

    :catch_0
    move-exception v0

    invoke-virtual {v0}, Landroid/os/RemoteException;->printStackTrace()V

    goto :goto_0
.end method

.method public setChannelChangeFreezeMode(Z)V
    .locals 2
    .param p1    # Z

    :try_start_0
    iget-object v1, p0, Lcom/mstar/tv/service/skin/ChannelSkin;->iTvServiceChannel:Lcom/mstar/tv/service/interfaces/ITvServiceServerChannel;

    invoke-interface {v1, p1}, Lcom/mstar/tv/service/interfaces/ITvServiceServerChannel;->setChannelChangeFreezeMode(Z)V
    :try_end_0
    .catch Landroid/os/RemoteException; {:try_start_0 .. :try_end_0} :catch_0

    :goto_0
    return-void

    :catch_0
    move-exception v0

    invoke-virtual {v0}, Landroid/os/RemoteException;->printStackTrace()V

    goto :goto_0
.end method

.method public setProgramAttribute(Lcom/mstar/tv/service/aidl/EN_PROGRAM_ATTRIBUTE;IIIZ)V
    .locals 7
    .param p1    # Lcom/mstar/tv/service/aidl/EN_PROGRAM_ATTRIBUTE;
    .param p2    # I
    .param p3    # I
    .param p4    # I
    .param p5    # Z

    :try_start_0
    iget-object v0, p0, Lcom/mstar/tv/service/skin/ChannelSkin;->iTvServiceChannel:Lcom/mstar/tv/service/interfaces/ITvServiceServerChannel;

    move-object v1, p1

    move v2, p2

    move v3, p3

    move v4, p4

    move v5, p5

    invoke-interface/range {v0 .. v5}, Lcom/mstar/tv/service/interfaces/ITvServiceServerChannel;->setProgramAttribute(Lcom/mstar/tv/service/aidl/EN_PROGRAM_ATTRIBUTE;IIIZ)V
    :try_end_0
    .catch Landroid/os/RemoteException; {:try_start_0 .. :try_end_0} :catch_0

    :goto_0
    return-void

    :catch_0
    move-exception v6

    invoke-virtual {v6}, Landroid/os/RemoteException;->printStackTrace()V

    goto :goto_0
.end method

.method public setProgramCtrl(Lcom/mstar/tv/service/aidl/EN_SET_PROGRAM_CTRL;IILjava/lang/String;)I
    .locals 2
    .param p1    # Lcom/mstar/tv/service/aidl/EN_SET_PROGRAM_CTRL;
    .param p2    # I
    .param p3    # I
    .param p4    # Ljava/lang/String;

    :try_start_0
    iget-object v1, p0, Lcom/mstar/tv/service/skin/ChannelSkin;->iTvServiceChannel:Lcom/mstar/tv/service/interfaces/ITvServiceServerChannel;

    invoke-interface {v1, p1, p2, p3, p4}, Lcom/mstar/tv/service/interfaces/ITvServiceServerChannel;->setProgramCtrl(Lcom/mstar/tv/service/aidl/EN_SET_PROGRAM_CTRL;IILjava/lang/String;)I
    :try_end_0
    .catch Landroid/os/RemoteException; {:try_start_0 .. :try_end_0} :catch_0

    move-result v1

    :goto_0
    return v1

    :catch_0
    move-exception v0

    invoke-virtual {v0}, Landroid/os/RemoteException;->printStackTrace()V

    const/4 v1, -0x1

    goto :goto_0
.end method

.method public setProgramName(IILjava/lang/String;)V
    .locals 2
    .param p1    # I
    .param p2    # I
    .param p3    # Ljava/lang/String;

    :try_start_0
    iget-object v1, p0, Lcom/mstar/tv/service/skin/ChannelSkin;->iTvServiceChannel:Lcom/mstar/tv/service/interfaces/ITvServiceServerChannel;

    invoke-interface {v1, p1, p2, p3}, Lcom/mstar/tv/service/interfaces/ITvServiceServerChannel;->setProgramName(IILjava/lang/String;)V
    :try_end_0
    .catch Landroid/os/RemoteException; {:try_start_0 .. :try_end_0} :catch_0

    :goto_0
    return-void

    :catch_0
    move-exception v0

    invoke-virtual {v0}, Landroid/os/RemoteException;->printStackTrace()V

    goto :goto_0
.end method

.method public setSystemCountry(Lcom/mstar/tv/service/aidl/EN_MEMBER_COUNTRY;)V
    .locals 2
    .param p1    # Lcom/mstar/tv/service/aidl/EN_MEMBER_COUNTRY;

    :try_start_0
    iget-object v1, p0, Lcom/mstar/tv/service/skin/ChannelSkin;->iTvServiceChannel:Lcom/mstar/tv/service/interfaces/ITvServiceServerChannel;

    invoke-interface {v1, p1}, Lcom/mstar/tv/service/interfaces/ITvServiceServerChannel;->setSystemCountry(Lcom/mstar/tv/service/aidl/EN_MEMBER_COUNTRY;)V
    :try_end_0
    .catch Landroid/os/RemoteException; {:try_start_0 .. :try_end_0} :catch_0

    :goto_0
    return-void

    :catch_0
    move-exception v0

    invoke-virtual {v0}, Landroid/os/RemoteException;->printStackTrace()V

    goto :goto_0
.end method

.method public setUserScanType(Lcom/mstar/tv/service/aidl/EN_TUNING_SCAN_TYPE;)V
    .locals 2
    .param p1    # Lcom/mstar/tv/service/aidl/EN_TUNING_SCAN_TYPE;

    :try_start_0
    iget-object v1, p0, Lcom/mstar/tv/service/skin/ChannelSkin;->iTvServiceChannel:Lcom/mstar/tv/service/interfaces/ITvServiceServerChannel;

    invoke-interface {v1, p1}, Lcom/mstar/tv/service/interfaces/ITvServiceServerChannel;->setUserScanType(Lcom/mstar/tv/service/aidl/EN_TUNING_SCAN_TYPE;)V
    :try_end_0
    .catch Landroid/os/RemoteException; {:try_start_0 .. :try_end_0} :catch_0

    :goto_0
    return-void

    :catch_0
    move-exception v0

    invoke-virtual {v0}, Landroid/os/RemoteException;->printStackTrace()V

    goto :goto_0
.end method

.method public switchAudioTrack(I)V
    .locals 2
    .param p1    # I

    :try_start_0
    iget-object v1, p0, Lcom/mstar/tv/service/skin/ChannelSkin;->iTvServiceChannel:Lcom/mstar/tv/service/interfaces/ITvServiceServerChannel;

    invoke-interface {v1, p1}, Lcom/mstar/tv/service/interfaces/ITvServiceServerChannel;->switchAudioTrack(I)V
    :try_end_0
    .catch Landroid/os/RemoteException; {:try_start_0 .. :try_end_0} :catch_0

    :goto_0
    return-void

    :catch_0
    move-exception v0

    invoke-virtual {v0}, Landroid/os/RemoteException;->printStackTrace()V

    goto :goto_0
.end method

.method public switchMSrvDtvRouteCmd(I)Z
    .locals 2
    .param p1    # I

    :try_start_0
    iget-object v1, p0, Lcom/mstar/tv/service/skin/ChannelSkin;->iTvServiceChannel:Lcom/mstar/tv/service/interfaces/ITvServiceServerChannel;

    invoke-interface {v1, p1}, Lcom/mstar/tv/service/interfaces/ITvServiceServerChannel;->switchMSrvDtvRouteCmd(I)Z
    :try_end_0
    .catch Landroid/os/RemoteException; {:try_start_0 .. :try_end_0} :catch_0

    move-result v1

    :goto_0
    return v1

    :catch_0
    move-exception v0

    invoke-virtual {v0}, Landroid/os/RemoteException;->printStackTrace()V

    const/4 v1, 0x0

    goto :goto_0
.end method
