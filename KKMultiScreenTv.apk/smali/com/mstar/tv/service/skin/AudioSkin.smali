.class public Lcom/mstar/tv/service/skin/AudioSkin;
.super Ljava/lang/Object;
.source "AudioSkin.java"


# instance fields
.field private handler:Landroid/os/Handler;

.field private iTvServiceAudio:Lcom/mstar/tv/service/interfaces/ITvServiceServerAudio;

.field private isBindOk:Z

.field private superContext:Landroid/content/Context;

.field protected tvServiceAudioConnection:Landroid/content/ServiceConnection;


# direct methods
.method public constructor <init>(Landroid/content/Context;)V
    .locals 1
    .param p1    # Landroid/content/Context;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    const/4 v0, 0x0

    iput-object v0, p0, Lcom/mstar/tv/service/skin/AudioSkin;->handler:Landroid/os/Handler;

    new-instance v0, Lcom/mstar/tv/service/skin/AudioSkin$1;

    invoke-direct {v0, p0}, Lcom/mstar/tv/service/skin/AudioSkin$1;-><init>(Lcom/mstar/tv/service/skin/AudioSkin;)V

    iput-object v0, p0, Lcom/mstar/tv/service/skin/AudioSkin;->tvServiceAudioConnection:Landroid/content/ServiceConnection;

    iput-object p1, p0, Lcom/mstar/tv/service/skin/AudioSkin;->superContext:Landroid/content/Context;

    return-void
.end method

.method static synthetic access$0(Lcom/mstar/tv/service/skin/AudioSkin;Lcom/mstar/tv/service/interfaces/ITvServiceServerAudio;)V
    .locals 0

    iput-object p1, p0, Lcom/mstar/tv/service/skin/AudioSkin;->iTvServiceAudio:Lcom/mstar/tv/service/interfaces/ITvServiceServerAudio;

    return-void
.end method

.method static synthetic access$1(Lcom/mstar/tv/service/skin/AudioSkin;)Landroid/os/Handler;
    .locals 1

    iget-object v0, p0, Lcom/mstar/tv/service/skin/AudioSkin;->handler:Landroid/os/Handler;

    return-object v0
.end method


# virtual methods
.method public GetMuteFlag()Z
    .locals 5

    const/4 v1, 0x0

    iget-object v2, p0, Lcom/mstar/tv/service/skin/AudioSkin;->iTvServiceAudio:Lcom/mstar/tv/service/interfaces/ITvServiceServerAudio;

    if-nez v2, :cond_0

    invoke-virtual {p0}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object v2

    new-instance v3, Ljava/lang/StringBuilder;

    const-string v4, "Audio service head is null!\t"

    invoke-direct {v3, v4}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    new-instance v4, Ljava/lang/Exception;

    invoke-direct {v4}, Ljava/lang/Exception;-><init>()V

    invoke-virtual {v4}, Ljava/lang/Exception;->getStackTrace()[Ljava/lang/StackTraceElement;

    move-result-object v4

    aget-object v4, v4, v1

    invoke-virtual {v4}, Ljava/lang/StackTraceElement;->getMethodName()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v2, v3}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    :goto_0
    return v1

    :cond_0
    :try_start_0
    iget-object v2, p0, Lcom/mstar/tv/service/skin/AudioSkin;->iTvServiceAudio:Lcom/mstar/tv/service/interfaces/ITvServiceServerAudio;

    invoke-interface {v2}, Lcom/mstar/tv/service/interfaces/ITvServiceServerAudio;->GetMuteFlag()Z
    :try_end_0
    .catch Landroid/os/RemoteException; {:try_start_0 .. :try_end_0} :catch_0

    move-result v1

    goto :goto_0

    :catch_0
    move-exception v0

    invoke-virtual {v0}, Landroid/os/RemoteException;->printStackTrace()V

    goto :goto_0
.end method

.method public connect(Landroid/os/Handler;)Z
    .locals 5
    .param p1    # Landroid/os/Handler;

    const/4 v4, 0x0

    iput-object p1, p0, Lcom/mstar/tv/service/skin/AudioSkin;->handler:Landroid/os/Handler;

    const-string v3, "tv_services"

    invoke-static {v3}, Landroid/os/ServiceManager;->getService(Ljava/lang/String;)Landroid/os/IBinder;

    move-result-object v1

    invoke-static {v1}, Lcom/mstar/tv/service/interfaces/ITvServiceServer$Stub;->asInterface(Landroid/os/IBinder;)Lcom/mstar/tv/service/interfaces/ITvServiceServer;

    move-result-object v2

    if-eqz v2, :cond_0

    :try_start_0
    invoke-interface {v2}, Lcom/mstar/tv/service/interfaces/ITvServiceServer;->getAudioManager()Lcom/mstar/tv/service/interfaces/ITvServiceServerAudio;

    move-result-object v3

    iput-object v3, p0, Lcom/mstar/tv/service/skin/AudioSkin;->iTvServiceAudio:Lcom/mstar/tv/service/interfaces/ITvServiceServerAudio;

    const/4 v3, 0x1

    iput-boolean v3, p0, Lcom/mstar/tv/service/skin/AudioSkin;->isBindOk:Z
    :try_end_0
    .catch Landroid/os/RemoteException; {:try_start_0 .. :try_end_0} :catch_0

    :goto_0
    iget-boolean v3, p0, Lcom/mstar/tv/service/skin/AudioSkin;->isBindOk:Z

    return v3

    :catch_0
    move-exception v0

    invoke-virtual {v0}, Landroid/os/RemoteException;->printStackTrace()V

    iput-boolean v4, p0, Lcom/mstar/tv/service/skin/AudioSkin;->isBindOk:Z

    goto :goto_0

    :cond_0
    iput-boolean v4, p0, Lcom/mstar/tv/service/skin/AudioSkin;->isBindOk:Z

    goto :goto_0
.end method

.method public disconnect()V
    .locals 0

    return-void
.end method

.method public getAVCMode()Z
    .locals 2

    :try_start_0
    iget-object v1, p0, Lcom/mstar/tv/service/skin/AudioSkin;->iTvServiceAudio:Lcom/mstar/tv/service/interfaces/ITvServiceServerAudio;

    invoke-interface {v1}, Lcom/mstar/tv/service/interfaces/ITvServiceServerAudio;->getAVCMode()Z
    :try_end_0
    .catch Landroid/os/RemoteException; {:try_start_0 .. :try_end_0} :catch_0

    move-result v1

    :goto_0
    return v1

    :catch_0
    move-exception v0

    invoke-virtual {v0}, Landroid/os/RemoteException;->printStackTrace()V

    const/4 v1, 0x0

    goto :goto_0
.end method

.method public getBalance()I
    .locals 2

    :try_start_0
    iget-object v1, p0, Lcom/mstar/tv/service/skin/AudioSkin;->iTvServiceAudio:Lcom/mstar/tv/service/interfaces/ITvServiceServerAudio;

    invoke-interface {v1}, Lcom/mstar/tv/service/interfaces/ITvServiceServerAudio;->getBalance()I
    :try_end_0
    .catch Landroid/os/RemoteException; {:try_start_0 .. :try_end_0} :catch_0

    move-result v1

    :goto_0
    return v1

    :catch_0
    move-exception v0

    invoke-virtual {v0}, Landroid/os/RemoteException;->printStackTrace()V

    const/4 v1, -0x1

    goto :goto_0
.end method

.method public getBass()I
    .locals 2

    :try_start_0
    iget-object v1, p0, Lcom/mstar/tv/service/skin/AudioSkin;->iTvServiceAudio:Lcom/mstar/tv/service/interfaces/ITvServiceServerAudio;

    invoke-interface {v1}, Lcom/mstar/tv/service/interfaces/ITvServiceServerAudio;->getBass()I
    :try_end_0
    .catch Landroid/os/RemoteException; {:try_start_0 .. :try_end_0} :catch_0

    move-result v1

    :goto_0
    return v1

    :catch_0
    move-exception v0

    invoke-virtual {v0}, Landroid/os/RemoteException;->printStackTrace()V

    const/4 v1, -0x1

    goto :goto_0
.end method

.method public getBassSwitch()Lcom/mstar/tv/service/aidl/EN_ON_OFF_TYPE;
    .locals 2

    :try_start_0
    iget-object v1, p0, Lcom/mstar/tv/service/skin/AudioSkin;->iTvServiceAudio:Lcom/mstar/tv/service/interfaces/ITvServiceServerAudio;

    invoke-interface {v1}, Lcom/mstar/tv/service/interfaces/ITvServiceServerAudio;->getBassSwitch()Lcom/mstar/tv/service/aidl/EN_ON_OFF_TYPE;
    :try_end_0
    .catch Landroid/os/RemoteException; {:try_start_0 .. :try_end_0} :catch_0

    move-result-object v1

    :goto_0
    return-object v1

    :catch_0
    move-exception v0

    invoke-virtual {v0}, Landroid/os/RemoteException;->printStackTrace()V

    const/4 v1, 0x0

    goto :goto_0
.end method

.method public getBassVolume()I
    .locals 2

    :try_start_0
    iget-object v1, p0, Lcom/mstar/tv/service/skin/AudioSkin;->iTvServiceAudio:Lcom/mstar/tv/service/interfaces/ITvServiceServerAudio;

    invoke-interface {v1}, Lcom/mstar/tv/service/interfaces/ITvServiceServerAudio;->getBassVolume()I
    :try_end_0
    .catch Landroid/os/RemoteException; {:try_start_0 .. :try_end_0} :catch_0

    move-result v1

    :goto_0
    return v1

    :catch_0
    move-exception v0

    invoke-virtual {v0}, Landroid/os/RemoteException;->printStackTrace()V

    const/4 v1, -0x1

    goto :goto_0
.end method

.method public getDGClarity()Lcom/mstar/tv/service/aidl/EN_ON_OFF_TYPE;
    .locals 2

    :try_start_0
    iget-object v1, p0, Lcom/mstar/tv/service/skin/AudioSkin;->iTvServiceAudio:Lcom/mstar/tv/service/interfaces/ITvServiceServerAudio;

    invoke-interface {v1}, Lcom/mstar/tv/service/interfaces/ITvServiceServerAudio;->getDGClarity()Lcom/mstar/tv/service/aidl/EN_ON_OFF_TYPE;
    :try_end_0
    .catch Landroid/os/RemoteException; {:try_start_0 .. :try_end_0} :catch_0

    move-result-object v1

    :goto_0
    return-object v1

    :catch_0
    move-exception v0

    invoke-virtual {v0}, Landroid/os/RemoteException;->printStackTrace()V

    const/4 v1, 0x0

    goto :goto_0
.end method

.method public getEarPhoneVolume()I
    .locals 5

    const/4 v1, 0x0

    iget-object v2, p0, Lcom/mstar/tv/service/skin/AudioSkin;->iTvServiceAudio:Lcom/mstar/tv/service/interfaces/ITvServiceServerAudio;

    if-nez v2, :cond_0

    invoke-virtual {p0}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object v2

    new-instance v3, Ljava/lang/StringBuilder;

    const-string v4, "Audio service head is null!\t"

    invoke-direct {v3, v4}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    new-instance v4, Ljava/lang/Exception;

    invoke-direct {v4}, Ljava/lang/Exception;-><init>()V

    invoke-virtual {v4}, Ljava/lang/Exception;->getStackTrace()[Ljava/lang/StackTraceElement;

    move-result-object v4

    aget-object v4, v4, v1

    invoke-virtual {v4}, Ljava/lang/StackTraceElement;->getMethodName()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v2, v3}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    :goto_0
    return v1

    :cond_0
    :try_start_0
    iget-object v2, p0, Lcom/mstar/tv/service/skin/AudioSkin;->iTvServiceAudio:Lcom/mstar/tv/service/interfaces/ITvServiceServerAudio;

    invoke-interface {v2}, Lcom/mstar/tv/service/interfaces/ITvServiceServerAudio;->getEarPhoneVolume()I
    :try_end_0
    .catch Landroid/os/RemoteException; {:try_start_0 .. :try_end_0} :catch_0

    move-result v1

    goto :goto_0

    :catch_0
    move-exception v0

    invoke-virtual {v0}, Landroid/os/RemoteException;->printStackTrace()V

    goto :goto_0
.end method

.method public getEqBand10k()I
    .locals 2

    :try_start_0
    iget-object v1, p0, Lcom/mstar/tv/service/skin/AudioSkin;->iTvServiceAudio:Lcom/mstar/tv/service/interfaces/ITvServiceServerAudio;

    invoke-interface {v1}, Lcom/mstar/tv/service/interfaces/ITvServiceServerAudio;->getEqBand10k()I
    :try_end_0
    .catch Landroid/os/RemoteException; {:try_start_0 .. :try_end_0} :catch_0

    move-result v1

    :goto_0
    return v1

    :catch_0
    move-exception v0

    invoke-virtual {v0}, Landroid/os/RemoteException;->printStackTrace()V

    const/4 v1, -0x1

    goto :goto_0
.end method

.method public getEqBand120()I
    .locals 2

    :try_start_0
    iget-object v1, p0, Lcom/mstar/tv/service/skin/AudioSkin;->iTvServiceAudio:Lcom/mstar/tv/service/interfaces/ITvServiceServerAudio;

    invoke-interface {v1}, Lcom/mstar/tv/service/interfaces/ITvServiceServerAudio;->getEqBand120()I
    :try_end_0
    .catch Landroid/os/RemoteException; {:try_start_0 .. :try_end_0} :catch_0

    move-result v1

    :goto_0
    return v1

    :catch_0
    move-exception v0

    invoke-virtual {v0}, Landroid/os/RemoteException;->printStackTrace()V

    const/4 v1, -0x1

    goto :goto_0
.end method

.method public getEqBand1500()I
    .locals 2

    :try_start_0
    iget-object v1, p0, Lcom/mstar/tv/service/skin/AudioSkin;->iTvServiceAudio:Lcom/mstar/tv/service/interfaces/ITvServiceServerAudio;

    invoke-interface {v1}, Lcom/mstar/tv/service/interfaces/ITvServiceServerAudio;->getEqBand1500()I
    :try_end_0
    .catch Landroid/os/RemoteException; {:try_start_0 .. :try_end_0} :catch_0

    move-result v1

    :goto_0
    return v1

    :catch_0
    move-exception v0

    invoke-virtual {v0}, Landroid/os/RemoteException;->printStackTrace()V

    const/4 v1, -0x1

    goto :goto_0
.end method

.method public getEqBand500()I
    .locals 2

    :try_start_0
    iget-object v1, p0, Lcom/mstar/tv/service/skin/AudioSkin;->iTvServiceAudio:Lcom/mstar/tv/service/interfaces/ITvServiceServerAudio;

    invoke-interface {v1}, Lcom/mstar/tv/service/interfaces/ITvServiceServerAudio;->getEqBand500()I
    :try_end_0
    .catch Landroid/os/RemoteException; {:try_start_0 .. :try_end_0} :catch_0

    move-result v1

    :goto_0
    return v1

    :catch_0
    move-exception v0

    invoke-virtual {v0}, Landroid/os/RemoteException;->printStackTrace()V

    const/4 v1, -0x1

    goto :goto_0
.end method

.method public getEqBand5k()I
    .locals 2

    :try_start_0
    iget-object v1, p0, Lcom/mstar/tv/service/skin/AudioSkin;->iTvServiceAudio:Lcom/mstar/tv/service/interfaces/ITvServiceServerAudio;

    invoke-interface {v1}, Lcom/mstar/tv/service/interfaces/ITvServiceServerAudio;->getEqBand5k()I
    :try_end_0
    .catch Landroid/os/RemoteException; {:try_start_0 .. :try_end_0} :catch_0

    move-result v1

    :goto_0
    return v1

    :catch_0
    move-exception v0

    invoke-virtual {v0}, Landroid/os/RemoteException;->printStackTrace()V

    const/4 v1, -0x1

    goto :goto_0
.end method

.method public getPowerOnOffMusic()Lcom/mstar/tv/service/aidl/EN_ON_OFF_TYPE;
    .locals 2

    :try_start_0
    iget-object v1, p0, Lcom/mstar/tv/service/skin/AudioSkin;->iTvServiceAudio:Lcom/mstar/tv/service/interfaces/ITvServiceServerAudio;

    invoke-interface {v1}, Lcom/mstar/tv/service/interfaces/ITvServiceServerAudio;->getPowerOnOffMusic()Lcom/mstar/tv/service/aidl/EN_ON_OFF_TYPE;
    :try_end_0
    .catch Landroid/os/RemoteException; {:try_start_0 .. :try_end_0} :catch_0

    move-result-object v1

    :goto_0
    return-object v1

    :catch_0
    move-exception v0

    invoke-virtual {v0}, Landroid/os/RemoteException;->printStackTrace()V

    const/4 v1, 0x0

    goto :goto_0
.end method

.method public getSeparateHear()Lcom/mstar/tv/service/aidl/EN_ON_OFF_TYPE;
    .locals 2

    :try_start_0
    iget-object v1, p0, Lcom/mstar/tv/service/skin/AudioSkin;->iTvServiceAudio:Lcom/mstar/tv/service/interfaces/ITvServiceServerAudio;

    invoke-interface {v1}, Lcom/mstar/tv/service/interfaces/ITvServiceServerAudio;->getSeparateHear()Lcom/mstar/tv/service/aidl/EN_ON_OFF_TYPE;
    :try_end_0
    .catch Landroid/os/RemoteException; {:try_start_0 .. :try_end_0} :catch_0

    move-result-object v1

    :goto_0
    return-object v1

    :catch_0
    move-exception v0

    invoke-virtual {v0}, Landroid/os/RemoteException;->printStackTrace()V

    const/4 v1, 0x0

    goto :goto_0
.end method

.method public getSoundMode()Lcom/mstar/tv/service/aidl/EN_SOUND_MODE;
    .locals 6

    const/4 v1, 0x0

    iget-object v2, p0, Lcom/mstar/tv/service/skin/AudioSkin;->iTvServiceAudio:Lcom/mstar/tv/service/interfaces/ITvServiceServerAudio;

    if-nez v2, :cond_0

    invoke-virtual {p0}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object v2

    new-instance v3, Ljava/lang/StringBuilder;

    const-string v4, "Audio service head is null!\t"

    invoke-direct {v3, v4}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    new-instance v4, Ljava/lang/Exception;

    invoke-direct {v4}, Ljava/lang/Exception;-><init>()V

    invoke-virtual {v4}, Ljava/lang/Exception;->getStackTrace()[Ljava/lang/StackTraceElement;

    move-result-object v4

    const/4 v5, 0x0

    aget-object v4, v4, v5

    invoke-virtual {v4}, Ljava/lang/StackTraceElement;->getMethodName()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v2, v3}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    :goto_0
    return-object v1

    :cond_0
    :try_start_0
    iget-object v2, p0, Lcom/mstar/tv/service/skin/AudioSkin;->iTvServiceAudio:Lcom/mstar/tv/service/interfaces/ITvServiceServerAudio;

    invoke-interface {v2}, Lcom/mstar/tv/service/interfaces/ITvServiceServerAudio;->getSoundMode()Lcom/mstar/tv/service/aidl/EN_SOUND_MODE;
    :try_end_0
    .catch Landroid/os/RemoteException; {:try_start_0 .. :try_end_0} :catch_0

    move-result-object v1

    goto :goto_0

    :catch_0
    move-exception v0

    invoke-virtual {v0}, Landroid/os/RemoteException;->printStackTrace()V

    goto :goto_0
.end method

.method public getSpdifOutMode()Lcom/mstar/tv/service/aidl/EN_SPDIF_OUT_MODE;
    .locals 2

    :try_start_0
    iget-object v1, p0, Lcom/mstar/tv/service/skin/AudioSkin;->iTvServiceAudio:Lcom/mstar/tv/service/interfaces/ITvServiceServerAudio;

    invoke-interface {v1}, Lcom/mstar/tv/service/interfaces/ITvServiceServerAudio;->getSpdifOutMode()Lcom/mstar/tv/service/aidl/EN_SPDIF_OUT_MODE;
    :try_end_0
    .catch Landroid/os/RemoteException; {:try_start_0 .. :try_end_0} :catch_0

    move-result-object v1

    :goto_0
    return-object v1

    :catch_0
    move-exception v0

    invoke-virtual {v0}, Landroid/os/RemoteException;->printStackTrace()V

    const/4 v1, 0x0

    goto :goto_0
.end method

.method public getSurroundMode()Lcom/mstar/tv/service/aidl/EN_SURROUND_MODE;
    .locals 2

    :try_start_0
    iget-object v1, p0, Lcom/mstar/tv/service/skin/AudioSkin;->iTvServiceAudio:Lcom/mstar/tv/service/interfaces/ITvServiceServerAudio;

    invoke-interface {v1}, Lcom/mstar/tv/service/interfaces/ITvServiceServerAudio;->getSurroundMode()Lcom/mstar/tv/service/aidl/EN_SURROUND_MODE;
    :try_end_0
    .catch Landroid/os/RemoteException; {:try_start_0 .. :try_end_0} :catch_0

    move-result-object v1

    :goto_0
    return-object v1

    :catch_0
    move-exception v0

    invoke-virtual {v0}, Landroid/os/RemoteException;->printStackTrace()V

    const/4 v1, 0x0

    goto :goto_0
.end method

.method public getTreble()I
    .locals 2

    :try_start_0
    iget-object v1, p0, Lcom/mstar/tv/service/skin/AudioSkin;->iTvServiceAudio:Lcom/mstar/tv/service/interfaces/ITvServiceServerAudio;

    invoke-interface {v1}, Lcom/mstar/tv/service/interfaces/ITvServiceServerAudio;->getTreble()I
    :try_end_0
    .catch Landroid/os/RemoteException; {:try_start_0 .. :try_end_0} :catch_0

    move-result v1

    :goto_0
    return v1

    :catch_0
    move-exception v0

    invoke-virtual {v0}, Landroid/os/RemoteException;->printStackTrace()V

    const/4 v1, -0x1

    goto :goto_0
.end method

.method public getTrueBass()Lcom/mstar/tv/service/aidl/EN_ON_OFF_TYPE;
    .locals 2

    :try_start_0
    iget-object v1, p0, Lcom/mstar/tv/service/skin/AudioSkin;->iTvServiceAudio:Lcom/mstar/tv/service/interfaces/ITvServiceServerAudio;

    invoke-interface {v1}, Lcom/mstar/tv/service/interfaces/ITvServiceServerAudio;->getTrueBass()Lcom/mstar/tv/service/aidl/EN_ON_OFF_TYPE;
    :try_end_0
    .catch Landroid/os/RemoteException; {:try_start_0 .. :try_end_0} :catch_0

    move-result-object v1

    :goto_0
    return-object v1

    :catch_0
    move-exception v0

    invoke-virtual {v0}, Landroid/os/RemoteException;->printStackTrace()V

    const/4 v1, 0x0

    goto :goto_0
.end method

.method public getVolume()I
    .locals 5

    const/4 v1, 0x0

    iget-object v2, p0, Lcom/mstar/tv/service/skin/AudioSkin;->iTvServiceAudio:Lcom/mstar/tv/service/interfaces/ITvServiceServerAudio;

    if-nez v2, :cond_0

    invoke-virtual {p0}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object v2

    new-instance v3, Ljava/lang/StringBuilder;

    const-string v4, "Audio service head is null!\t"

    invoke-direct {v3, v4}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    new-instance v4, Ljava/lang/Exception;

    invoke-direct {v4}, Ljava/lang/Exception;-><init>()V

    invoke-virtual {v4}, Ljava/lang/Exception;->getStackTrace()[Ljava/lang/StackTraceElement;

    move-result-object v4

    aget-object v4, v4, v1

    invoke-virtual {v4}, Ljava/lang/StackTraceElement;->getMethodName()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v2, v3}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    :goto_0
    return v1

    :cond_0
    :try_start_0
    iget-object v2, p0, Lcom/mstar/tv/service/skin/AudioSkin;->iTvServiceAudio:Lcom/mstar/tv/service/interfaces/ITvServiceServerAudio;

    invoke-interface {v2}, Lcom/mstar/tv/service/interfaces/ITvServiceServerAudio;->getVolume()I
    :try_end_0
    .catch Landroid/os/RemoteException; {:try_start_0 .. :try_end_0} :catch_0

    move-result v1

    goto :goto_0

    :catch_0
    move-exception v0

    invoke-virtual {v0}, Landroid/os/RemoteException;->printStackTrace()V

    goto :goto_0
.end method

.method public getWallmusic()Lcom/mstar/tv/service/aidl/EN_ON_OFF_TYPE;
    .locals 2

    :try_start_0
    iget-object v1, p0, Lcom/mstar/tv/service/skin/AudioSkin;->iTvServiceAudio:Lcom/mstar/tv/service/interfaces/ITvServiceServerAudio;

    invoke-interface {v1}, Lcom/mstar/tv/service/interfaces/ITvServiceServerAudio;->getWallmusic()Lcom/mstar/tv/service/aidl/EN_ON_OFF_TYPE;
    :try_end_0
    .catch Landroid/os/RemoteException; {:try_start_0 .. :try_end_0} :catch_0

    move-result-object v1

    :goto_0
    return-object v1

    :catch_0
    move-exception v0

    invoke-virtual {v0}, Landroid/os/RemoteException;->printStackTrace()V

    const/4 v1, 0x0

    goto :goto_0
.end method

.method public isConnectionOk()Z
    .locals 1

    iget-object v0, p0, Lcom/mstar/tv/service/skin/AudioSkin;->iTvServiceAudio:Lcom/mstar/tv/service/interfaces/ITvServiceServerAudio;

    if-nez v0, :cond_0

    const/4 v0, 0x0

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x1

    goto :goto_0
.end method

.method public setAVCMode(Z)Z
    .locals 2
    .param p1    # Z

    :try_start_0
    iget-object v1, p0, Lcom/mstar/tv/service/skin/AudioSkin;->iTvServiceAudio:Lcom/mstar/tv/service/interfaces/ITvServiceServerAudio;

    invoke-interface {v1, p1}, Lcom/mstar/tv/service/interfaces/ITvServiceServerAudio;->setAVCMode(Z)Z
    :try_end_0
    .catch Landroid/os/RemoteException; {:try_start_0 .. :try_end_0} :catch_0

    move-result v1

    :goto_0
    return v1

    :catch_0
    move-exception v0

    invoke-virtual {v0}, Landroid/os/RemoteException;->printStackTrace()V

    const/4 v1, 0x0

    goto :goto_0
.end method

.method public setBalance(I)Z
    .locals 2
    .param p1    # I

    :try_start_0
    iget-object v1, p0, Lcom/mstar/tv/service/skin/AudioSkin;->iTvServiceAudio:Lcom/mstar/tv/service/interfaces/ITvServiceServerAudio;

    invoke-interface {v1, p1}, Lcom/mstar/tv/service/interfaces/ITvServiceServerAudio;->setBalance(I)Z
    :try_end_0
    .catch Landroid/os/RemoteException; {:try_start_0 .. :try_end_0} :catch_0

    move-result v1

    :goto_0
    return v1

    :catch_0
    move-exception v0

    invoke-virtual {v0}, Landroid/os/RemoteException;->printStackTrace()V

    const/4 v1, 0x0

    goto :goto_0
.end method

.method public setBass(I)Z
    .locals 2
    .param p1    # I

    :try_start_0
    iget-object v1, p0, Lcom/mstar/tv/service/skin/AudioSkin;->iTvServiceAudio:Lcom/mstar/tv/service/interfaces/ITvServiceServerAudio;

    invoke-interface {v1, p1}, Lcom/mstar/tv/service/interfaces/ITvServiceServerAudio;->setBass(I)Z
    :try_end_0
    .catch Landroid/os/RemoteException; {:try_start_0 .. :try_end_0} :catch_0

    move-result v1

    :goto_0
    return v1

    :catch_0
    move-exception v0

    invoke-virtual {v0}, Landroid/os/RemoteException;->printStackTrace()V

    const/4 v1, 0x0

    goto :goto_0
.end method

.method public setBassSwitch(Lcom/mstar/tv/service/aidl/EN_ON_OFF_TYPE;)Z
    .locals 2
    .param p1    # Lcom/mstar/tv/service/aidl/EN_ON_OFF_TYPE;

    :try_start_0
    iget-object v1, p0, Lcom/mstar/tv/service/skin/AudioSkin;->iTvServiceAudio:Lcom/mstar/tv/service/interfaces/ITvServiceServerAudio;

    invoke-interface {v1, p1}, Lcom/mstar/tv/service/interfaces/ITvServiceServerAudio;->setBassSwitch(Lcom/mstar/tv/service/aidl/EN_ON_OFF_TYPE;)Z
    :try_end_0
    .catch Landroid/os/RemoteException; {:try_start_0 .. :try_end_0} :catch_0

    move-result v1

    :goto_0
    return v1

    :catch_0
    move-exception v0

    invoke-virtual {v0}, Landroid/os/RemoteException;->printStackTrace()V

    const/4 v1, 0x0

    goto :goto_0
.end method

.method public setBassVolume(I)Z
    .locals 2
    .param p1    # I

    :try_start_0
    iget-object v1, p0, Lcom/mstar/tv/service/skin/AudioSkin;->iTvServiceAudio:Lcom/mstar/tv/service/interfaces/ITvServiceServerAudio;

    invoke-interface {v1, p1}, Lcom/mstar/tv/service/interfaces/ITvServiceServerAudio;->setBassVolume(I)Z
    :try_end_0
    .catch Landroid/os/RemoteException; {:try_start_0 .. :try_end_0} :catch_0

    move-result v1

    :goto_0
    return v1

    :catch_0
    move-exception v0

    invoke-virtual {v0}, Landroid/os/RemoteException;->printStackTrace()V

    const/4 v1, 0x0

    goto :goto_0
.end method

.method public setDGClarity(Lcom/mstar/tv/service/aidl/EN_ON_OFF_TYPE;)Z
    .locals 2
    .param p1    # Lcom/mstar/tv/service/aidl/EN_ON_OFF_TYPE;

    :try_start_0
    iget-object v1, p0, Lcom/mstar/tv/service/skin/AudioSkin;->iTvServiceAudio:Lcom/mstar/tv/service/interfaces/ITvServiceServerAudio;

    invoke-interface {v1, p1}, Lcom/mstar/tv/service/interfaces/ITvServiceServerAudio;->setDGClarity(Lcom/mstar/tv/service/aidl/EN_ON_OFF_TYPE;)Z
    :try_end_0
    .catch Landroid/os/RemoteException; {:try_start_0 .. :try_end_0} :catch_0

    move-result v1

    :goto_0
    return v1

    :catch_0
    move-exception v0

    invoke-virtual {v0}, Landroid/os/RemoteException;->printStackTrace()V

    const/4 v1, 0x0

    goto :goto_0
.end method

.method public setEarPhoneVolume(I)Z
    .locals 6
    .param p1    # I

    const/16 v5, 0x64

    const/4 v1, 0x0

    iget-object v2, p0, Lcom/mstar/tv/service/skin/AudioSkin;->iTvServiceAudio:Lcom/mstar/tv/service/interfaces/ITvServiceServerAudio;

    if-nez v2, :cond_0

    invoke-virtual {p0}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object v2

    new-instance v3, Ljava/lang/StringBuilder;

    const-string v4, "Audio service head is null!\t"

    invoke-direct {v3, v4}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    new-instance v4, Ljava/lang/Exception;

    invoke-direct {v4}, Ljava/lang/Exception;-><init>()V

    invoke-virtual {v4}, Ljava/lang/Exception;->getStackTrace()[Ljava/lang/StackTraceElement;

    move-result-object v4

    aget-object v4, v4, v1

    invoke-virtual {v4}, Ljava/lang/StackTraceElement;->getMethodName()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v2, v3}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    :goto_0
    return v1

    :cond_0
    if-ltz p1, :cond_1

    if-le p1, v5, :cond_2

    :cond_1
    invoke-virtual {p0}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object v2

    new-instance v3, Ljava/lang/StringBuilder;

    const-string v4, "Input params out of boundary!\t"

    invoke-direct {v3, v4}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    new-instance v4, Ljava/lang/Exception;

    invoke-direct {v4}, Ljava/lang/Exception;-><init>()V

    invoke-virtual {v4}, Ljava/lang/Exception;->getStackTrace()[Ljava/lang/StackTraceElement;

    move-result-object v4

    aget-object v4, v4, v1

    invoke-virtual {v4}, Ljava/lang/StackTraceElement;->getMethodName()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v2, v3}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    if-gez p1, :cond_3

    const/4 p1, 0x0

    :cond_2
    :goto_1
    :try_start_0
    iget-object v2, p0, Lcom/mstar/tv/service/skin/AudioSkin;->iTvServiceAudio:Lcom/mstar/tv/service/interfaces/ITvServiceServerAudio;

    invoke-interface {v2, p1}, Lcom/mstar/tv/service/interfaces/ITvServiceServerAudio;->setEarPhoneVolume(I)Z
    :try_end_0
    .catch Landroid/os/RemoteException; {:try_start_0 .. :try_end_0} :catch_0

    move-result v1

    goto :goto_0

    :cond_3
    if-le p1, v5, :cond_2

    const/16 p1, 0x64

    goto :goto_1

    :catch_0
    move-exception v0

    invoke-virtual {v0}, Landroid/os/RemoteException;->printStackTrace()V

    goto :goto_0
.end method

.method public setEqBand10k(I)Z
    .locals 2
    .param p1    # I

    :try_start_0
    iget-object v1, p0, Lcom/mstar/tv/service/skin/AudioSkin;->iTvServiceAudio:Lcom/mstar/tv/service/interfaces/ITvServiceServerAudio;

    invoke-interface {v1, p1}, Lcom/mstar/tv/service/interfaces/ITvServiceServerAudio;->setEqBand10k(I)Z
    :try_end_0
    .catch Landroid/os/RemoteException; {:try_start_0 .. :try_end_0} :catch_0

    move-result v1

    :goto_0
    return v1

    :catch_0
    move-exception v0

    invoke-virtual {v0}, Landroid/os/RemoteException;->printStackTrace()V

    const/4 v1, 0x0

    goto :goto_0
.end method

.method public setEqBand120(I)Z
    .locals 2
    .param p1    # I

    :try_start_0
    iget-object v1, p0, Lcom/mstar/tv/service/skin/AudioSkin;->iTvServiceAudio:Lcom/mstar/tv/service/interfaces/ITvServiceServerAudio;

    invoke-interface {v1, p1}, Lcom/mstar/tv/service/interfaces/ITvServiceServerAudio;->setEqBand120(I)Z
    :try_end_0
    .catch Landroid/os/RemoteException; {:try_start_0 .. :try_end_0} :catch_0

    move-result v1

    :goto_0
    return v1

    :catch_0
    move-exception v0

    invoke-virtual {v0}, Landroid/os/RemoteException;->printStackTrace()V

    const/4 v1, 0x0

    goto :goto_0
.end method

.method public setEqBand1500(I)Z
    .locals 2
    .param p1    # I

    :try_start_0
    iget-object v1, p0, Lcom/mstar/tv/service/skin/AudioSkin;->iTvServiceAudio:Lcom/mstar/tv/service/interfaces/ITvServiceServerAudio;

    invoke-interface {v1, p1}, Lcom/mstar/tv/service/interfaces/ITvServiceServerAudio;->setEqBand1500(I)Z
    :try_end_0
    .catch Landroid/os/RemoteException; {:try_start_0 .. :try_end_0} :catch_0

    move-result v1

    :goto_0
    return v1

    :catch_0
    move-exception v0

    invoke-virtual {v0}, Landroid/os/RemoteException;->printStackTrace()V

    const/4 v1, 0x0

    goto :goto_0
.end method

.method public setEqBand500(I)Z
    .locals 2
    .param p1    # I

    :try_start_0
    iget-object v1, p0, Lcom/mstar/tv/service/skin/AudioSkin;->iTvServiceAudio:Lcom/mstar/tv/service/interfaces/ITvServiceServerAudio;

    invoke-interface {v1, p1}, Lcom/mstar/tv/service/interfaces/ITvServiceServerAudio;->setEqBand500(I)Z
    :try_end_0
    .catch Landroid/os/RemoteException; {:try_start_0 .. :try_end_0} :catch_0

    move-result v1

    :goto_0
    return v1

    :catch_0
    move-exception v0

    invoke-virtual {v0}, Landroid/os/RemoteException;->printStackTrace()V

    const/4 v1, 0x0

    goto :goto_0
.end method

.method public setEqBand5k(I)Z
    .locals 2
    .param p1    # I

    :try_start_0
    iget-object v1, p0, Lcom/mstar/tv/service/skin/AudioSkin;->iTvServiceAudio:Lcom/mstar/tv/service/interfaces/ITvServiceServerAudio;

    invoke-interface {v1, p1}, Lcom/mstar/tv/service/interfaces/ITvServiceServerAudio;->setEqBand5k(I)Z
    :try_end_0
    .catch Landroid/os/RemoteException; {:try_start_0 .. :try_end_0} :catch_0

    move-result v1

    :goto_0
    return v1

    :catch_0
    move-exception v0

    invoke-virtual {v0}, Landroid/os/RemoteException;->printStackTrace()V

    const/4 v1, 0x0

    goto :goto_0
.end method

.method public setMuteFlag(Z)Z
    .locals 5
    .param p1    # Z

    const/4 v1, 0x0

    iget-object v2, p0, Lcom/mstar/tv/service/skin/AudioSkin;->iTvServiceAudio:Lcom/mstar/tv/service/interfaces/ITvServiceServerAudio;

    if-nez v2, :cond_0

    invoke-virtual {p0}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object v2

    new-instance v3, Ljava/lang/StringBuilder;

    const-string v4, "Audio service head is null!\t"

    invoke-direct {v3, v4}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    new-instance v4, Ljava/lang/Exception;

    invoke-direct {v4}, Ljava/lang/Exception;-><init>()V

    invoke-virtual {v4}, Ljava/lang/Exception;->getStackTrace()[Ljava/lang/StackTraceElement;

    move-result-object v4

    aget-object v4, v4, v1

    invoke-virtual {v4}, Ljava/lang/StackTraceElement;->getMethodName()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v2, v3}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    :goto_0
    return v1

    :cond_0
    :try_start_0
    iget-object v2, p0, Lcom/mstar/tv/service/skin/AudioSkin;->iTvServiceAudio:Lcom/mstar/tv/service/interfaces/ITvServiceServerAudio;

    invoke-interface {v2, p1}, Lcom/mstar/tv/service/interfaces/ITvServiceServerAudio;->setMuteFlag(Z)Z
    :try_end_0
    .catch Landroid/os/RemoteException; {:try_start_0 .. :try_end_0} :catch_0

    move-result v1

    goto :goto_0

    :catch_0
    move-exception v0

    invoke-virtual {v0}, Landroid/os/RemoteException;->printStackTrace()V

    goto :goto_0
.end method

.method public setPowerOnOffMusic(Lcom/mstar/tv/service/aidl/EN_ON_OFF_TYPE;)Z
    .locals 2
    .param p1    # Lcom/mstar/tv/service/aidl/EN_ON_OFF_TYPE;

    :try_start_0
    iget-object v1, p0, Lcom/mstar/tv/service/skin/AudioSkin;->iTvServiceAudio:Lcom/mstar/tv/service/interfaces/ITvServiceServerAudio;

    invoke-interface {v1, p1}, Lcom/mstar/tv/service/interfaces/ITvServiceServerAudio;->setPowerOnOffMusic(Lcom/mstar/tv/service/aidl/EN_ON_OFF_TYPE;)Z
    :try_end_0
    .catch Landroid/os/RemoteException; {:try_start_0 .. :try_end_0} :catch_0

    move-result v1

    :goto_0
    return v1

    :catch_0
    move-exception v0

    invoke-virtual {v0}, Landroid/os/RemoteException;->printStackTrace()V

    const/4 v1, 0x0

    goto :goto_0
.end method

.method public setSeparateHear(Lcom/mstar/tv/service/aidl/EN_ON_OFF_TYPE;)Z
    .locals 2
    .param p1    # Lcom/mstar/tv/service/aidl/EN_ON_OFF_TYPE;

    :try_start_0
    iget-object v1, p0, Lcom/mstar/tv/service/skin/AudioSkin;->iTvServiceAudio:Lcom/mstar/tv/service/interfaces/ITvServiceServerAudio;

    invoke-interface {v1, p1}, Lcom/mstar/tv/service/interfaces/ITvServiceServerAudio;->setSeparateHear(Lcom/mstar/tv/service/aidl/EN_ON_OFF_TYPE;)Z
    :try_end_0
    .catch Landroid/os/RemoteException; {:try_start_0 .. :try_end_0} :catch_0

    move-result v1

    :goto_0
    return v1

    :catch_0
    move-exception v0

    invoke-virtual {v0}, Landroid/os/RemoteException;->printStackTrace()V

    const/4 v1, 0x0

    goto :goto_0
.end method

.method public setSoundMode(Lcom/mstar/tv/service/aidl/EN_SOUND_MODE;)Z
    .locals 5
    .param p1    # Lcom/mstar/tv/service/aidl/EN_SOUND_MODE;

    const/4 v1, 0x0

    iget-object v2, p0, Lcom/mstar/tv/service/skin/AudioSkin;->iTvServiceAudio:Lcom/mstar/tv/service/interfaces/ITvServiceServerAudio;

    if-nez v2, :cond_0

    invoke-virtual {p0}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object v2

    new-instance v3, Ljava/lang/StringBuilder;

    const-string v4, "Audio service head is null!\t"

    invoke-direct {v3, v4}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    new-instance v4, Ljava/lang/Exception;

    invoke-direct {v4}, Ljava/lang/Exception;-><init>()V

    invoke-virtual {v4}, Ljava/lang/Exception;->getStackTrace()[Ljava/lang/StackTraceElement;

    move-result-object v4

    aget-object v4, v4, v1

    invoke-virtual {v4}, Ljava/lang/StackTraceElement;->getMethodName()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v2, v3}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    :goto_0
    return v1

    :cond_0
    invoke-virtual {p1}, Lcom/mstar/tv/service/aidl/EN_SOUND_MODE;->ordinal()I

    move-result v2

    sget-object v3, Lcom/mstar/tv/service/aidl/EN_SOUND_MODE;->SOUND_MODE_NUM:Lcom/mstar/tv/service/aidl/EN_SOUND_MODE;

    invoke-virtual {v3}, Lcom/mstar/tv/service/aidl/EN_SOUND_MODE;->ordinal()I

    move-result v3

    if-lt v2, v3, :cond_1

    invoke-virtual {p0}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object v2

    new-instance v3, Ljava/lang/StringBuilder;

    const-string v4, "Input params out of boundary!\t"

    invoke-direct {v3, v4}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    new-instance v4, Ljava/lang/Exception;

    invoke-direct {v4}, Ljava/lang/Exception;-><init>()V

    invoke-virtual {v4}, Ljava/lang/Exception;->getStackTrace()[Ljava/lang/StackTraceElement;

    move-result-object v4

    aget-object v4, v4, v1

    invoke-virtual {v4}, Ljava/lang/StackTraceElement;->getMethodName()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v2, v3}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_0

    :cond_1
    :try_start_0
    iget-object v2, p0, Lcom/mstar/tv/service/skin/AudioSkin;->iTvServiceAudio:Lcom/mstar/tv/service/interfaces/ITvServiceServerAudio;

    invoke-interface {v2, p1}, Lcom/mstar/tv/service/interfaces/ITvServiceServerAudio;->setSoundMode(Lcom/mstar/tv/service/aidl/EN_SOUND_MODE;)Z
    :try_end_0
    .catch Landroid/os/RemoteException; {:try_start_0 .. :try_end_0} :catch_0

    move-result v1

    goto :goto_0

    :catch_0
    move-exception v0

    invoke-virtual {v0}, Landroid/os/RemoteException;->printStackTrace()V

    goto :goto_0
.end method

.method public setSpdifOutMode(Lcom/mstar/tv/service/aidl/EN_SPDIF_OUT_MODE;)Z
    .locals 2
    .param p1    # Lcom/mstar/tv/service/aidl/EN_SPDIF_OUT_MODE;

    :try_start_0
    iget-object v1, p0, Lcom/mstar/tv/service/skin/AudioSkin;->iTvServiceAudio:Lcom/mstar/tv/service/interfaces/ITvServiceServerAudio;

    invoke-interface {v1, p1}, Lcom/mstar/tv/service/interfaces/ITvServiceServerAudio;->setSpdifOutMode(Lcom/mstar/tv/service/aidl/EN_SPDIF_OUT_MODE;)Z
    :try_end_0
    .catch Landroid/os/RemoteException; {:try_start_0 .. :try_end_0} :catch_0

    move-result v1

    :goto_0
    return v1

    :catch_0
    move-exception v0

    invoke-virtual {v0}, Landroid/os/RemoteException;->printStackTrace()V

    const/4 v1, 0x0

    goto :goto_0
.end method

.method public setSurroundMode(Lcom/mstar/tv/service/aidl/EN_SURROUND_MODE;)Z
    .locals 2
    .param p1    # Lcom/mstar/tv/service/aidl/EN_SURROUND_MODE;

    :try_start_0
    iget-object v1, p0, Lcom/mstar/tv/service/skin/AudioSkin;->iTvServiceAudio:Lcom/mstar/tv/service/interfaces/ITvServiceServerAudio;

    invoke-interface {v1, p1}, Lcom/mstar/tv/service/interfaces/ITvServiceServerAudio;->setSurroundMode(Lcom/mstar/tv/service/aidl/EN_SURROUND_MODE;)Z
    :try_end_0
    .catch Landroid/os/RemoteException; {:try_start_0 .. :try_end_0} :catch_0

    move-result v1

    :goto_0
    return v1

    :catch_0
    move-exception v0

    invoke-virtual {v0}, Landroid/os/RemoteException;->printStackTrace()V

    const/4 v1, 0x0

    goto :goto_0
.end method

.method public setTreble(I)Z
    .locals 2
    .param p1    # I

    :try_start_0
    iget-object v1, p0, Lcom/mstar/tv/service/skin/AudioSkin;->iTvServiceAudio:Lcom/mstar/tv/service/interfaces/ITvServiceServerAudio;

    invoke-interface {v1, p1}, Lcom/mstar/tv/service/interfaces/ITvServiceServerAudio;->setTreble(I)Z
    :try_end_0
    .catch Landroid/os/RemoteException; {:try_start_0 .. :try_end_0} :catch_0

    move-result v1

    :goto_0
    return v1

    :catch_0
    move-exception v0

    invoke-virtual {v0}, Landroid/os/RemoteException;->printStackTrace()V

    const/4 v1, 0x0

    goto :goto_0
.end method

.method public setTrueBass(Lcom/mstar/tv/service/aidl/EN_ON_OFF_TYPE;)Z
    .locals 2
    .param p1    # Lcom/mstar/tv/service/aidl/EN_ON_OFF_TYPE;

    :try_start_0
    iget-object v1, p0, Lcom/mstar/tv/service/skin/AudioSkin;->iTvServiceAudio:Lcom/mstar/tv/service/interfaces/ITvServiceServerAudio;

    invoke-interface {v1, p1}, Lcom/mstar/tv/service/interfaces/ITvServiceServerAudio;->setTrueBass(Lcom/mstar/tv/service/aidl/EN_ON_OFF_TYPE;)Z
    :try_end_0
    .catch Landroid/os/RemoteException; {:try_start_0 .. :try_end_0} :catch_0

    move-result v1

    :goto_0
    return v1

    :catch_0
    move-exception v0

    invoke-virtual {v0}, Landroid/os/RemoteException;->printStackTrace()V

    const/4 v1, 0x0

    goto :goto_0
.end method

.method public setVolume(I)Z
    .locals 6
    .param p1    # I

    const/16 v5, 0x64

    const/4 v1, 0x0

    iget-object v2, p0, Lcom/mstar/tv/service/skin/AudioSkin;->iTvServiceAudio:Lcom/mstar/tv/service/interfaces/ITvServiceServerAudio;

    if-nez v2, :cond_0

    invoke-virtual {p0}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object v2

    new-instance v3, Ljava/lang/StringBuilder;

    const-string v4, "Audio service head is null!\t"

    invoke-direct {v3, v4}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    new-instance v4, Ljava/lang/Exception;

    invoke-direct {v4}, Ljava/lang/Exception;-><init>()V

    invoke-virtual {v4}, Ljava/lang/Exception;->getStackTrace()[Ljava/lang/StackTraceElement;

    move-result-object v4

    aget-object v4, v4, v1

    invoke-virtual {v4}, Ljava/lang/StackTraceElement;->getMethodName()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v2, v3}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    :goto_0
    return v1

    :cond_0
    if-ltz p1, :cond_1

    if-le p1, v5, :cond_2

    :cond_1
    invoke-virtual {p0}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object v2

    new-instance v3, Ljava/lang/StringBuilder;

    const-string v4, "Input params out of boundary!\t"

    invoke-direct {v3, v4}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    new-instance v4, Ljava/lang/Exception;

    invoke-direct {v4}, Ljava/lang/Exception;-><init>()V

    invoke-virtual {v4}, Ljava/lang/Exception;->getStackTrace()[Ljava/lang/StackTraceElement;

    move-result-object v4

    aget-object v4, v4, v1

    invoke-virtual {v4}, Ljava/lang/StackTraceElement;->getMethodName()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v2, v3}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    if-gez p1, :cond_3

    const/4 p1, 0x0

    :cond_2
    :goto_1
    :try_start_0
    iget-object v2, p0, Lcom/mstar/tv/service/skin/AudioSkin;->iTvServiceAudio:Lcom/mstar/tv/service/interfaces/ITvServiceServerAudio;

    invoke-interface {v2, p1}, Lcom/mstar/tv/service/interfaces/ITvServiceServerAudio;->setVolume(I)Z
    :try_end_0
    .catch Landroid/os/RemoteException; {:try_start_0 .. :try_end_0} :catch_0

    move-result v1

    goto :goto_0

    :cond_3
    if-le p1, v5, :cond_2

    const/16 p1, 0x64

    goto :goto_1

    :catch_0
    move-exception v0

    invoke-virtual {v0}, Landroid/os/RemoteException;->printStackTrace()V

    goto :goto_0
.end method

.method public setWallmusic(Lcom/mstar/tv/service/aidl/EN_ON_OFF_TYPE;)Z
    .locals 2
    .param p1    # Lcom/mstar/tv/service/aidl/EN_ON_OFF_TYPE;

    :try_start_0
    iget-object v1, p0, Lcom/mstar/tv/service/skin/AudioSkin;->iTvServiceAudio:Lcom/mstar/tv/service/interfaces/ITvServiceServerAudio;

    invoke-interface {v1, p1}, Lcom/mstar/tv/service/interfaces/ITvServiceServerAudio;->setWallmusic(Lcom/mstar/tv/service/aidl/EN_ON_OFF_TYPE;)Z
    :try_end_0
    .catch Landroid/os/RemoteException; {:try_start_0 .. :try_end_0} :catch_0

    move-result v1

    :goto_0
    return v1

    :catch_0
    move-exception v0

    invoke-virtual {v0}, Landroid/os/RemoteException;->printStackTrace()V

    const/4 v1, 0x0

    goto :goto_0
.end method
