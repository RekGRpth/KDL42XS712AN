.class Lcom/mstar/tv/service/skin/PictureSkin$1;
.super Ljava/lang/Object;
.source "PictureSkin.java"

# interfaces
.implements Landroid/content/ServiceConnection;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/mstar/tv/service/skin/PictureSkin;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/mstar/tv/service/skin/PictureSkin;


# direct methods
.method constructor <init>(Lcom/mstar/tv/service/skin/PictureSkin;)V
    .locals 0

    iput-object p1, p0, Lcom/mstar/tv/service/skin/PictureSkin$1;->this$0:Lcom/mstar/tv/service/skin/PictureSkin;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onServiceConnected(Landroid/content/ComponentName;Landroid/os/IBinder;)V
    .locals 5
    .param p1    # Landroid/content/ComponentName;
    .param p2    # Landroid/os/IBinder;

    iget-object v3, p0, Lcom/mstar/tv/service/skin/PictureSkin$1;->this$0:Lcom/mstar/tv/service/skin/PictureSkin;

    invoke-static {p2}, Lcom/mstar/tv/service/interfaces/ITvServiceServerPicture$Stub;->asInterface(Landroid/os/IBinder;)Lcom/mstar/tv/service/interfaces/ITvServiceServerPicture;

    move-result-object v4

    invoke-static {v3, v4}, Lcom/mstar/tv/service/skin/PictureSkin;->access$0(Lcom/mstar/tv/service/skin/PictureSkin;Lcom/mstar/tv/service/interfaces/ITvServiceServerPicture;)V

    iget-object v3, p0, Lcom/mstar/tv/service/skin/PictureSkin$1;->this$0:Lcom/mstar/tv/service/skin/PictureSkin;

    # getter for: Lcom/mstar/tv/service/skin/PictureSkin;->handler:Landroid/os/Handler;
    invoke-static {v3}, Lcom/mstar/tv/service/skin/PictureSkin;->access$1(Lcom/mstar/tv/service/skin/PictureSkin;)Landroid/os/Handler;

    move-result-object v3

    if-eqz v3, :cond_0

    iget-object v3, p0, Lcom/mstar/tv/service/skin/PictureSkin$1;->this$0:Lcom/mstar/tv/service/skin/PictureSkin;

    # getter for: Lcom/mstar/tv/service/skin/PictureSkin;->handler:Landroid/os/Handler;
    invoke-static {v3}, Lcom/mstar/tv/service/skin/PictureSkin;->access$1(Lcom/mstar/tv/service/skin/PictureSkin;)Landroid/os/Handler;

    move-result-object v3

    invoke-virtual {v3}, Landroid/os/Handler;->obtainMessage()Landroid/os/Message;

    move-result-object v2

    const v3, 0x7fffffff

    iput v3, v2, Landroid/os/Message;->what:I

    new-instance v0, Landroid/os/Bundle;

    invoke-direct {v0}, Landroid/os/Bundle;-><init>()V

    const-string v3, "Index"

    const v4, 0x7ffffffd

    invoke-virtual {v0, v3, v4}, Landroid/os/Bundle;->putInt(Ljava/lang/String;I)V

    invoke-virtual {v2, v0}, Landroid/os/Message;->setData(Landroid/os/Bundle;)V

    invoke-virtual {v2}, Landroid/os/Message;->sendToTarget()V

    :cond_0
    :try_start_0
    sget-object v3, Ljava/lang/System;->out:Ljava/io/PrintStream;

    iget-object v4, p0, Lcom/mstar/tv/service/skin/PictureSkin$1;->this$0:Lcom/mstar/tv/service/skin/PictureSkin;

    # getter for: Lcom/mstar/tv/service/skin/PictureSkin;->iTvServicePicture:Lcom/mstar/tv/service/interfaces/ITvServiceServerPicture;
    invoke-static {v4}, Lcom/mstar/tv/service/skin/PictureSkin;->access$2(Lcom/mstar/tv/service/skin/PictureSkin;)Lcom/mstar/tv/service/interfaces/ITvServiceServerPicture;

    move-result-object v4

    invoke-interface {v4}, Lcom/mstar/tv/service/interfaces/ITvServiceServerPicture;->getPictureModeIdx()Lcom/mstar/tv/service/aidl/EN_MS_PICTURE;

    move-result-object v4

    invoke-virtual {v3, v4}, Ljava/io/PrintStream;->println(Ljava/lang/Object;)V

    sget-object v3, Ljava/lang/System;->out:Ljava/io/PrintStream;

    iget-object v4, p0, Lcom/mstar/tv/service/skin/PictureSkin$1;->this$0:Lcom/mstar/tv/service/skin/PictureSkin;

    # getter for: Lcom/mstar/tv/service/skin/PictureSkin;->iTvServicePicture:Lcom/mstar/tv/service/interfaces/ITvServiceServerPicture;
    invoke-static {v4}, Lcom/mstar/tv/service/skin/PictureSkin;->access$2(Lcom/mstar/tv/service/skin/PictureSkin;)Lcom/mstar/tv/service/interfaces/ITvServiceServerPicture;

    move-result-object v4

    invoke-interface {v4}, Lcom/mstar/tv/service/interfaces/ITvServiceServerPicture;->getVideoArc()Lcom/mstar/tv/service/aidl/MAPI_VIDEO_ARC_Type;

    move-result-object v4

    invoke-virtual {v3, v4}, Ljava/io/PrintStream;->println(Ljava/lang/Object;)V
    :try_end_0
    .catch Landroid/os/RemoteException; {:try_start_0 .. :try_end_0} :catch_0

    :goto_0
    return-void

    :catch_0
    move-exception v1

    invoke-virtual {v1}, Landroid/os/RemoteException;->printStackTrace()V

    goto :goto_0
.end method

.method public onServiceDisconnected(Landroid/content/ComponentName;)V
    .locals 2
    .param p1    # Landroid/content/ComponentName;

    iget-object v0, p0, Lcom/mstar/tv/service/skin/PictureSkin$1;->this$0:Lcom/mstar/tv/service/skin/PictureSkin;

    const/4 v1, 0x0

    invoke-static {v0, v1}, Lcom/mstar/tv/service/skin/PictureSkin;->access$0(Lcom/mstar/tv/service/skin/PictureSkin;Lcom/mstar/tv/service/interfaces/ITvServiceServerPicture;)V

    return-void
.end method
