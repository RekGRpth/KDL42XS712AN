.class Lcom/mstar/tv/service/skin/S3DSkin$1;
.super Ljava/lang/Object;
.source "S3DSkin.java"

# interfaces
.implements Landroid/content/ServiceConnection;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/mstar/tv/service/skin/S3DSkin;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/mstar/tv/service/skin/S3DSkin;


# direct methods
.method constructor <init>(Lcom/mstar/tv/service/skin/S3DSkin;)V
    .locals 0

    iput-object p1, p0, Lcom/mstar/tv/service/skin/S3DSkin$1;->this$0:Lcom/mstar/tv/service/skin/S3DSkin;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onServiceConnected(Landroid/content/ComponentName;Landroid/os/IBinder;)V
    .locals 4
    .param p1    # Landroid/content/ComponentName;
    .param p2    # Landroid/os/IBinder;

    iget-object v2, p0, Lcom/mstar/tv/service/skin/S3DSkin$1;->this$0:Lcom/mstar/tv/service/skin/S3DSkin;

    invoke-static {p2}, Lcom/mstar/tv/service/interfaces/ITvServiceServerS3D$Stub;->asInterface(Landroid/os/IBinder;)Lcom/mstar/tv/service/interfaces/ITvServiceServerS3D;

    move-result-object v3

    invoke-static {v2, v3}, Lcom/mstar/tv/service/skin/S3DSkin;->access$0(Lcom/mstar/tv/service/skin/S3DSkin;Lcom/mstar/tv/service/interfaces/ITvServiceServerS3D;)V

    iget-object v2, p0, Lcom/mstar/tv/service/skin/S3DSkin$1;->this$0:Lcom/mstar/tv/service/skin/S3DSkin;

    # getter for: Lcom/mstar/tv/service/skin/S3DSkin;->handler:Landroid/os/Handler;
    invoke-static {v2}, Lcom/mstar/tv/service/skin/S3DSkin;->access$1(Lcom/mstar/tv/service/skin/S3DSkin;)Landroid/os/Handler;

    move-result-object v2

    if-eqz v2, :cond_0

    iget-object v2, p0, Lcom/mstar/tv/service/skin/S3DSkin$1;->this$0:Lcom/mstar/tv/service/skin/S3DSkin;

    # getter for: Lcom/mstar/tv/service/skin/S3DSkin;->handler:Landroid/os/Handler;
    invoke-static {v2}, Lcom/mstar/tv/service/skin/S3DSkin;->access$1(Lcom/mstar/tv/service/skin/S3DSkin;)Landroid/os/Handler;

    move-result-object v2

    invoke-virtual {v2}, Landroid/os/Handler;->obtainMessage()Landroid/os/Message;

    move-result-object v1

    const v2, 0x7fffffff

    iput v2, v1, Landroid/os/Message;->what:I

    new-instance v0, Landroid/os/Bundle;

    invoke-direct {v0}, Landroid/os/Bundle;-><init>()V

    const-string v2, "Index"

    const v3, 0x7ffffffa

    invoke-virtual {v0, v2, v3}, Landroid/os/Bundle;->putInt(Ljava/lang/String;I)V

    invoke-virtual {v1, v0}, Landroid/os/Message;->setData(Landroid/os/Bundle;)V

    invoke-virtual {v1}, Landroid/os/Message;->sendToTarget()V

    :cond_0
    return-void
.end method

.method public onServiceDisconnected(Landroid/content/ComponentName;)V
    .locals 2
    .param p1    # Landroid/content/ComponentName;

    iget-object v0, p0, Lcom/mstar/tv/service/skin/S3DSkin$1;->this$0:Lcom/mstar/tv/service/skin/S3DSkin;

    const/4 v1, 0x0

    invoke-static {v0, v1}, Lcom/mstar/tv/service/skin/S3DSkin;->access$0(Lcom/mstar/tv/service/skin/S3DSkin;Lcom/mstar/tv/service/interfaces/ITvServiceServerS3D;)V

    return-void
.end method
