.class public Lcom/android/server/MouseService;
.super Landroid/os/IMouseManager$Stub;
.source "MouseService.java"


# instance fields
.field private mApkMap:Ljava/util/HashMap;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/HashMap",
            "<",
            "Ljava/lang/String;",
            "Ljava/lang/Integer;",
            ">;"
        }
    .end annotation
.end field

.field private mVirtualMouseKey:Landroid/app/VirtualMouseKey;


# direct methods
.method public constructor <init>(Landroid/content/Context;)V
    .locals 1
    .param p1    # Landroid/content/Context;

    invoke-direct {p0}, Landroid/os/IMouseManager$Stub;-><init>()V

    const/4 v0, 0x0

    iput-object v0, p0, Lcom/android/server/MouseService;->mVirtualMouseKey:Landroid/app/VirtualMouseKey;

    new-instance v0, Ljava/util/HashMap;

    invoke-direct {v0}, Ljava/util/HashMap;-><init>()V

    iput-object v0, p0, Lcom/android/server/MouseService;->mApkMap:Ljava/util/HashMap;

    new-instance v0, Landroid/app/VirtualMouseKey;

    invoke-direct {v0}, Landroid/app/VirtualMouseKey;-><init>()V

    iput-object v0, p0, Lcom/android/server/MouseService;->mVirtualMouseKey:Landroid/app/VirtualMouseKey;

    return-void
.end method


# virtual methods
.method public addApk(Ljava/lang/String;)V
    .locals 2
    .param p1    # Ljava/lang/String;

    iget-object v0, p0, Lcom/android/server/MouseService;->mApkMap:Ljava/util/HashMap;

    invoke-virtual {v0, p1}, Ljava/util/HashMap;->containsKey(Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_0

    iget-object v0, p0, Lcom/android/server/MouseService;->mApkMap:Ljava/util/HashMap;

    const/4 v1, 0x0

    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    invoke-virtual {v0, p1, v1}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    :cond_0
    return-void
.end method

.method public getApkList()[Ljava/lang/String;
    .locals 6

    iget-object v5, p0, Lcom/android/server/MouseService;->mApkMap:Ljava/util/HashMap;

    invoke-virtual {v5}, Ljava/util/HashMap;->size()I

    move-result v4

    if-nez v4, :cond_1

    const/4 v0, 0x0

    :cond_0
    return-object v0

    :cond_1
    new-array v0, v4, [Ljava/lang/String;

    iget-object v5, p0, Lcom/android/server/MouseService;->mApkMap:Ljava/util/HashMap;

    invoke-virtual {v5}, Ljava/util/HashMap;->entrySet()Ljava/util/Set;

    move-result-object v5

    invoke-interface {v5}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    move-result-object v3

    const/4 v2, 0x0

    :goto_0
    invoke-interface {v3}, Ljava/util/Iterator;->hasNext()Z

    move-result v5

    if-eqz v5, :cond_0

    invoke-interface {v3}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Ljava/util/Map$Entry;

    invoke-interface {v1}, Ljava/util/Map$Entry;->getKey()Ljava/lang/Object;

    move-result-object v5

    check-cast v5, Ljava/lang/String;

    aput-object v5, v0, v2

    add-int/lit8 v2, v2, 0x1

    goto :goto_0
.end method

.method public getMouseControlFlag()Z
    .locals 1

    const/4 v0, 0x1

    return v0
.end method

.method public hasApk(Ljava/lang/String;)Z
    .locals 1
    .param p1    # Ljava/lang/String;

    iget-object v0, p0, Lcom/android/server/MouseService;->mApkMap:Ljava/util/HashMap;

    invoke-virtual {v0, p1}, Ljava/util/HashMap;->containsKey(Ljava/lang/Object;)Z

    move-result v0

    return v0
.end method

.method public removeAllApk()V
    .locals 1

    iget-object v0, p0, Lcom/android/server/MouseService;->mApkMap:Ljava/util/HashMap;

    invoke-virtual {v0}, Ljava/util/HashMap;->clear()V

    return-void
.end method

.method public removeApk(Ljava/lang/String;)V
    .locals 1
    .param p1    # Ljava/lang/String;

    iget-object v0, p0, Lcom/android/server/MouseService;->mApkMap:Ljava/util/HashMap;

    invoke-virtual {v0, p1}, Ljava/util/HashMap;->remove(Ljava/lang/Object;)Ljava/lang/Object;

    return-void
.end method

.method public sendKeyEvent(IIJ)V
    .locals 1
    .param p1    # I
    .param p2    # I
    .param p3    # J

    iget-object v0, p0, Lcom/android/server/MouseService;->mVirtualMouseKey:Landroid/app/VirtualMouseKey;

    if-nez v0, :cond_0

    :goto_0
    return-void

    :cond_0
    iget-object v0, p0, Lcom/android/server/MouseService;->mVirtualMouseKey:Landroid/app/VirtualMouseKey;

    invoke-virtual {v0, p1, p2, p3, p4}, Landroid/app/VirtualMouseKey;->handleVirtualMouseKeyEx(IIJ)Z

    goto :goto_0
.end method
