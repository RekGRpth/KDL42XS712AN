.class public Lcom/android/server/PppoeService;
.super Landroid/net/pppoe/IPppoeManager$Stub;
.source "PppoeService.java"


# static fields
.field private static final LINK_AUTH_FAIL:I = 0x20

.field private static final LINK_CONNECTING:I = 0x24

.field private static final LINK_DISCONNECTING:I = 0x23

.field private static final LINK_DOWN:I = 0x1f

.field private static final LINK_PPP_FAIL:I = 0x22

.field private static final LINK_TIME_OUT:I = 0x21

.field private static final LINK_UP:I = 0x1e

.field private static final TAG:Ljava/lang/String; = "PppoeService"


# instance fields
.field private mContext:Landroid/content/Context;

.field private mPppoeState:Ljava/lang/String;

.field private mSocketThread:Ljava/lang/Thread;


# direct methods
.method public constructor <init>(Landroid/content/Context;)V
    .locals 1
    .param p1    # Landroid/content/Context;

    const/4 v0, 0x0

    invoke-direct {p0}, Landroid/net/pppoe/IPppoeManager$Stub;-><init>()V

    iput-object v0, p0, Lcom/android/server/PppoeService;->mContext:Landroid/content/Context;

    iput-object v0, p0, Lcom/android/server/PppoeService;->mPppoeState:Ljava/lang/String;

    iput-object v0, p0, Lcom/android/server/PppoeService;->mSocketThread:Ljava/lang/Thread;

    iput-object p1, p0, Lcom/android/server/PppoeService;->mContext:Landroid/content/Context;

    const-string v0, "disconnect"

    iput-object v0, p0, Lcom/android/server/PppoeService;->mPppoeState:Ljava/lang/String;

    invoke-direct {p0}, Lcom/android/server/PppoeService;->startMonitorThread()V

    return-void
.end method

.method static synthetic access$000(Lcom/android/server/PppoeService;Ljava/lang/String;)V
    .locals 0
    .param p0    # Lcom/android/server/PppoeService;
    .param p1    # Ljava/lang/String;

    invoke-direct {p0, p1}, Lcom/android/server/PppoeService;->handleEvent(Ljava/lang/String;)V

    return-void
.end method

.method private getStateFromMsg(I)Ljava/lang/String;
    .locals 1
    .param p1    # I

    const-string v0, "disconnect"

    packed-switch p1, :pswitch_data_0

    :goto_0
    :pswitch_0
    return-object v0

    :pswitch_1
    const-string v0, "connect"

    goto :goto_0

    :pswitch_2
    const-string v0, "authfailed"

    goto :goto_0

    :pswitch_3
    const-string v0, "disconnecting"

    goto :goto_0

    :pswitch_4
    const-string v0, "connecting"

    goto :goto_0

    :pswitch_5
    const-string v0, "linktimeout"

    goto :goto_0

    :pswitch_6
    const-string v0, "failed"

    goto :goto_0

    :pswitch_data_0
    .packed-switch 0x0
        :pswitch_1
        :pswitch_0
        :pswitch_4
        :pswitch_3
        :pswitch_2
        :pswitch_5
        :pswitch_6
    .end packed-switch
.end method

.method private handleEvent(Ljava/lang/String;)V
    .locals 7
    .param p1    # Ljava/lang/String;

    const/4 v6, 0x1

    if-nez p1, :cond_0

    :goto_0
    return-void

    :cond_0
    const-string v4, ":"

    invoke-virtual {p1, v4}, Ljava/lang/String;->split(Ljava/lang/String;)[Ljava/lang/String;

    move-result-object v1

    array-length v4, v1

    const/4 v5, 0x2

    if-eq v4, v5, :cond_1

    const-string v4, "PppoeService"

    const-string v5, "pppoe event error"

    invoke-static {v4, v5}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_0

    :cond_1
    aget-object v4, v1, v6

    invoke-static {v4}, Ljava/lang/Integer;->parseInt(Ljava/lang/String;)I

    move-result v0

    const/4 v2, 0x1

    packed-switch v0, :pswitch_data_0

    :goto_1
    invoke-direct {p0, v2}, Lcom/android/server/PppoeService;->getStateFromMsg(I)Ljava/lang/String;

    move-result-object v3

    const-string v4, "authfailed"

    invoke-virtual {v3, v4}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v4

    if-nez v4, :cond_2

    invoke-virtual {p0, v3, v6}, Lcom/android/server/PppoeService;->setPppoeStatus(Ljava/lang/String;Z)V

    goto :goto_0

    :pswitch_0
    const/4 v2, 0x0

    goto :goto_1

    :pswitch_1
    const/4 v2, 0x1

    goto :goto_1

    :pswitch_2
    const/4 v2, 0x4

    goto :goto_1

    :cond_2
    const/4 v4, 0x0

    invoke-virtual {p0, v3, v4}, Lcom/android/server/PppoeService;->setPppoeStatus(Ljava/lang/String;Z)V

    goto :goto_0

    nop

    :pswitch_data_0
    .packed-switch 0x1e
        :pswitch_0
        :pswitch_1
        :pswitch_2
    .end packed-switch
.end method

.method private sendPppBroadcast(Ljava/lang/String;)V
    .locals 2
    .param p1    # Ljava/lang/String;

    new-instance v0, Landroid/content/Intent;

    invoke-direct {v0}, Landroid/content/Intent;-><init>()V

    const-string v1, "android.net.pppoe.PPPOE_STATE_ACTION"

    invoke-virtual {v0, v1}, Landroid/content/Intent;->setAction(Ljava/lang/String;)Landroid/content/Intent;

    const-string v1, "PppoeStatus"

    invoke-virtual {v0, v1, p1}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    iget-object v1, p0, Lcom/android/server/PppoeService;->mContext:Landroid/content/Context;

    invoke-virtual {v1, v0}, Landroid/content/Context;->sendBroadcast(Landroid/content/Intent;)V

    return-void
.end method

.method private startMonitorThread()V
    .locals 2

    new-instance v0, Lcom/android/server/PppoeService$1;

    const-string v1, "pppoe_monitor_thread"

    invoke-direct {v0, p0, v1}, Lcom/android/server/PppoeService$1;-><init>(Lcom/android/server/PppoeService;Ljava/lang/String;)V

    iput-object v0, p0, Lcom/android/server/PppoeService;->mSocketThread:Ljava/lang/Thread;

    iget-object v0, p0, Lcom/android/server/PppoeService;->mSocketThread:Ljava/lang/Thread;

    invoke-virtual {v0}, Ljava/lang/Thread;->start()V

    return-void
.end method


# virtual methods
.method public getPppoeStatus()Ljava/lang/String;
    .locals 1

    iget-object v0, p0, Lcom/android/server/PppoeService;->mPppoeState:Ljava/lang/String;

    return-object v0
.end method

.method public setPppoeStatus(Ljava/lang/String;Z)V
    .locals 2
    .param p1    # Ljava/lang/String;
    .param p2    # Z

    iput-object p1, p0, Lcom/android/server/PppoeService;->mPppoeState:Ljava/lang/String;

    if-eqz p2, :cond_0

    iget-object v0, p0, Lcom/android/server/PppoeService;->mPppoeState:Ljava/lang/String;

    invoke-direct {p0, v0}, Lcom/android/server/PppoeService;->sendPppBroadcast(Ljava/lang/String;)V

    if-eqz p1, :cond_0

    const-string v0, "disconnect"

    invoke-virtual {p1, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    const-string v0, "ctl.start"

    const-string v1, "pppoe-stop"

    invoke-static {v0, v1}, Landroid/os/SystemProperties;->set(Ljava/lang/String;Ljava/lang/String;)V

    :cond_0
    return-void
.end method
