.class public Lcom/android/server/tv/DeskAtvPlayerEventListener;
.super Ljava/lang/Object;
.source "DeskAtvPlayerEventListener.java"

# interfaces
.implements Lcom/mstar/android/tvapi/atv/listener/OnAtvPlayerEventListener;


# static fields
.field private static atvEventListener:Lcom/android/server/tv/DeskAtvPlayerEventListener;


# instance fields
.field private clients:Ljava/util/HashMap;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/HashMap",
            "<",
            "Ljava/lang/String;",
            "Lcom/mstar/android/tv/IAtvPlayerEventClient;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method static constructor <clinit>()V
    .locals 1

    const/4 v0, 0x0

    sput-object v0, Lcom/android/server/tv/DeskAtvPlayerEventListener;->atvEventListener:Lcom/android/server/tv/DeskAtvPlayerEventListener;

    return-void
.end method

.method public constructor <init>()V
    .locals 1

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    new-instance v0, Ljava/util/HashMap;

    invoke-direct {v0}, Ljava/util/HashMap;-><init>()V

    iput-object v0, p0, Lcom/android/server/tv/DeskAtvPlayerEventListener;->clients:Ljava/util/HashMap;

    return-void
.end method

.method public static getInstance()Lcom/android/server/tv/DeskAtvPlayerEventListener;
    .locals 1

    sget-object v0, Lcom/android/server/tv/DeskAtvPlayerEventListener;->atvEventListener:Lcom/android/server/tv/DeskAtvPlayerEventListener;

    if-nez v0, :cond_0

    new-instance v0, Lcom/android/server/tv/DeskAtvPlayerEventListener;

    invoke-direct {v0}, Lcom/android/server/tv/DeskAtvPlayerEventListener;-><init>()V

    sput-object v0, Lcom/android/server/tv/DeskAtvPlayerEventListener;->atvEventListener:Lcom/android/server/tv/DeskAtvPlayerEventListener;

    :cond_0
    sget-object v0, Lcom/android/server/tv/DeskAtvPlayerEventListener;->atvEventListener:Lcom/android/server/tv/DeskAtvPlayerEventListener;

    return-object v0
.end method


# virtual methods
.method public onAtvAutoTuningScanInfo(ILcom/mstar/android/tvapi/atv/vo/AtvEventScan;)Z
    .locals 4
    .param p1    # I
    .param p2    # Lcom/mstar/android/tvapi/atv/vo/AtvEventScan;

    iget-object v3, p0, Lcom/android/server/tv/DeskAtvPlayerEventListener;->clients:Ljava/util/HashMap;

    if-nez v3, :cond_0

    const/4 v3, 0x0

    :goto_0
    return v3

    :cond_0
    :try_start_0
    iget-object v3, p0, Lcom/android/server/tv/DeskAtvPlayerEventListener;->clients:Ljava/util/HashMap;

    invoke-virtual {v3}, Ljava/util/HashMap;->values()Ljava/util/Collection;

    move-result-object v3

    invoke-interface {v3}, Ljava/util/Collection;->iterator()Ljava/util/Iterator;

    move-result-object v2

    :goto_1
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    move-result v3

    if-eqz v3, :cond_1

    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/mstar/android/tv/IAtvPlayerEventClient;

    invoke-interface {v0, p1, p2}, Lcom/mstar/android/tv/IAtvPlayerEventClient;->onAtvAutoTuningScanInfo(ILcom/mstar/android/tvapi/atv/vo/AtvEventScan;)Z
    :try_end_0
    .catch Landroid/os/RemoteException; {:try_start_0 .. :try_end_0} :catch_0

    goto :goto_1

    :catch_0
    move-exception v1

    invoke-virtual {v1}, Landroid/os/RemoteException;->printStackTrace()V

    :cond_1
    const/4 v3, 0x1

    goto :goto_0
.end method

.method public onAtvManualTuningScanInfo(ILcom/mstar/android/tvapi/atv/vo/AtvEventScan;)Z
    .locals 4
    .param p1    # I
    .param p2    # Lcom/mstar/android/tvapi/atv/vo/AtvEventScan;

    iget-object v3, p0, Lcom/android/server/tv/DeskAtvPlayerEventListener;->clients:Ljava/util/HashMap;

    if-nez v3, :cond_0

    const/4 v3, 0x0

    :goto_0
    return v3

    :cond_0
    :try_start_0
    iget-object v3, p0, Lcom/android/server/tv/DeskAtvPlayerEventListener;->clients:Ljava/util/HashMap;

    invoke-virtual {v3}, Ljava/util/HashMap;->values()Ljava/util/Collection;

    move-result-object v3

    invoke-interface {v3}, Ljava/util/Collection;->iterator()Ljava/util/Iterator;

    move-result-object v2

    :goto_1
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    move-result v3

    if-eqz v3, :cond_1

    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/mstar/android/tv/IAtvPlayerEventClient;

    invoke-interface {v0, p1, p2}, Lcom/mstar/android/tv/IAtvPlayerEventClient;->onAtvAutoTuningScanInfo(ILcom/mstar/android/tvapi/atv/vo/AtvEventScan;)Z
    :try_end_0
    .catch Landroid/os/RemoteException; {:try_start_0 .. :try_end_0} :catch_0

    goto :goto_1

    :catch_0
    move-exception v1

    invoke-virtual {v1}, Landroid/os/RemoteException;->printStackTrace()V

    :cond_1
    const/4 v3, 0x1

    goto :goto_0
.end method

.method public onAtvProgramInfoReady(I)Z
    .locals 5
    .param p1    # I

    const-string v3, "TvApp"

    const-string v4, "onAtvProgramInfoReady"

    invoke-static {v3, v4}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    iget-object v3, p0, Lcom/android/server/tv/DeskAtvPlayerEventListener;->clients:Ljava/util/HashMap;

    if-nez v3, :cond_0

    const/4 v3, 0x0

    :goto_0
    return v3

    :cond_0
    :try_start_0
    iget-object v3, p0, Lcom/android/server/tv/DeskAtvPlayerEventListener;->clients:Ljava/util/HashMap;

    invoke-virtual {v3}, Ljava/util/HashMap;->values()Ljava/util/Collection;

    move-result-object v3

    invoke-interface {v3}, Ljava/util/Collection;->iterator()Ljava/util/Iterator;

    move-result-object v2

    :goto_1
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    move-result v3

    if-eqz v3, :cond_1

    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/mstar/android/tv/IAtvPlayerEventClient;

    invoke-interface {v0, p1}, Lcom/mstar/android/tv/IAtvPlayerEventClient;->onAtvProgramInfoReady(I)Z
    :try_end_0
    .catch Landroid/os/RemoteException; {:try_start_0 .. :try_end_0} :catch_0

    goto :goto_1

    :catch_0
    move-exception v1

    invoke-virtual {v1}, Landroid/os/RemoteException;->printStackTrace()V

    :cond_1
    const/4 v3, 0x1

    goto :goto_0
.end method

.method public onSignalLock(I)Z
    .locals 6
    .param p1    # I

    const/4 v5, 0x0

    const-string v3, "TvApp"

    const-string v4, "onSignalLock in DeskAtvPlayerEventListener"

    invoke-static {v3, v4}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    iget-object v3, p0, Lcom/android/server/tv/DeskAtvPlayerEventListener;->clients:Ljava/util/HashMap;

    if-nez v3, :cond_1

    :cond_0
    :goto_0
    return v5

    :cond_1
    :try_start_0
    iget-object v3, p0, Lcom/android/server/tv/DeskAtvPlayerEventListener;->clients:Ljava/util/HashMap;

    invoke-virtual {v3}, Ljava/util/HashMap;->values()Ljava/util/Collection;

    move-result-object v3

    invoke-interface {v3}, Ljava/util/Collection;->iterator()Ljava/util/Iterator;

    move-result-object v2

    :goto_1
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    move-result v3

    if-eqz v3, :cond_0

    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/mstar/android/tv/IAtvPlayerEventClient;

    invoke-interface {v0, p1}, Lcom/mstar/android/tv/IAtvPlayerEventClient;->onSignalLock(I)Z
    :try_end_0
    .catch Landroid/os/RemoteException; {:try_start_0 .. :try_end_0} :catch_0

    goto :goto_1

    :catch_0
    move-exception v1

    invoke-virtual {v1}, Landroid/os/RemoteException;->printStackTrace()V

    goto :goto_0
.end method

.method public onSignalUnLock(I)Z
    .locals 6
    .param p1    # I

    const/4 v5, 0x0

    const-string v3, "TvApp"

    const-string v4, "onSignalUnLock in DeskAtvPlayerEventListener"

    invoke-static {v3, v4}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    iget-object v3, p0, Lcom/android/server/tv/DeskAtvPlayerEventListener;->clients:Ljava/util/HashMap;

    if-nez v3, :cond_1

    :cond_0
    :goto_0
    return v5

    :cond_1
    :try_start_0
    iget-object v3, p0, Lcom/android/server/tv/DeskAtvPlayerEventListener;->clients:Ljava/util/HashMap;

    invoke-virtual {v3}, Ljava/util/HashMap;->values()Ljava/util/Collection;

    move-result-object v3

    invoke-interface {v3}, Ljava/util/Collection;->iterator()Ljava/util/Iterator;

    move-result-object v2

    :goto_1
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    move-result v3

    if-eqz v3, :cond_0

    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/mstar/android/tv/IAtvPlayerEventClient;

    invoke-interface {v0, p1}, Lcom/mstar/android/tv/IAtvPlayerEventClient;->onSignalUnLock(I)Z
    :try_end_0
    .catch Landroid/os/RemoteException; {:try_start_0 .. :try_end_0} :catch_0

    goto :goto_1

    :catch_0
    move-exception v1

    invoke-virtual {v1}, Landroid/os/RemoteException;->printStackTrace()V

    goto :goto_0
.end method

.method public releaseClient(Ljava/lang/String;)V
    .locals 1
    .param p1    # Ljava/lang/String;

    iget-object v0, p0, Lcom/android/server/tv/DeskAtvPlayerEventListener;->clients:Ljava/util/HashMap;

    invoke-virtual {v0, p1}, Ljava/util/HashMap;->remove(Ljava/lang/Object;)Ljava/lang/Object;

    return-void
.end method

.method public setClient(Ljava/lang/String;Lcom/mstar/android/tv/IAtvPlayerEventClient;)V
    .locals 1
    .param p1    # Ljava/lang/String;
    .param p2    # Lcom/mstar/android/tv/IAtvPlayerEventClient;

    iget-object v0, p0, Lcom/android/server/tv/DeskAtvPlayerEventListener;->clients:Ljava/util/HashMap;

    invoke-virtual {v0, p1, p2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    return-void
.end method
