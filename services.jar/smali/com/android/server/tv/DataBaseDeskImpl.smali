.class public Lcom/android/server/tv/DataBaseDeskImpl;
.super Ljava/lang/Object;
.source "DataBaseDeskImpl.java"

# interfaces
.implements Lcom/android/server/tv/DataBaseDesk;


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/android/server/tv/DataBaseDeskImpl$1;,
        Lcom/android/server/tv/DataBaseDeskImpl$USER_SETTING_FIELD;,
        Lcom/android/server/tv/DataBaseDeskImpl$S3D_TABLE_FIELD;
    }
.end annotation


# static fields
.field private static dataBaseMgrImpl:Lcom/android/server/tv/DataBaseDeskImpl;

.field private static mContext:Landroid/content/Context;


# instance fields
.field public astSoundModeSetting:[Lcom/android/server/tv/DataBaseDesk$SoundModeSeting;

.field colorParaEx:Lcom/android/server/tv/DataBaseDesk$T_MS_COLOR_TEMPEX_DATA;

.field private factoryCusSchema:Ljava/lang/String;

.field private factorySchema:Ljava/lang/String;

.field private handler:Landroid/os/Handler;

.field private mContentResolver:Landroid/content/ContentResolver;

.field mNoStandSet:Lcom/android/server/tv/DataBaseDesk$MS_Factory_NS_VD_SET;

.field mSscSet:Lcom/android/server/tv/DataBaseDesk$MS_FACTORY_SSC_SET;

.field mVifSet:Lcom/android/server/tv/DataBaseDesk$MS_Factory_NS_VIF_SET;

.field m_ATVOverscanSet:[[Lcom/android/server/tv/DataBaseDesk$ST_MAPI_VIDEO_WINDOW_INFO;

.field m_DTVOverscanSet:[[Lcom/android/server/tv/DataBaseDesk$ST_MAPI_VIDEO_WINDOW_INFO;

.field m_HDMIOverscanSet:[[Lcom/android/server/tv/DataBaseDesk$ST_MAPI_VIDEO_WINDOW_INFO;

.field m_VDOverscanSet:[[Lcom/android/server/tv/DataBaseDesk$ST_MAPI_VIDEO_WINDOW_INFO;

.field m_YPbPrOverscanSet:[[Lcom/android/server/tv/DataBaseDesk$ST_MAPI_VIDEO_WINDOW_INFO;

.field m_bADCAutoTune:Z

.field m_pastNLASet:Lcom/android/server/tv/DataBaseDesk$MS_NLA_SETTING;

.field m_stCISet:Lcom/android/server/tv/DataBaseDesk$ST_FACTORY_CI_SETTING;

.field m_stFactoryAdc:Lcom/android/server/tv/DataBaseDesk$MS_ADC_SETTING;

.field m_stFactoryColorTemp:Lcom/android/server/tv/DataBaseDesk$T_MS_COLOR_TEMP;

.field m_stFactoryColorTempEx:Lcom/android/server/tv/DataBaseDesk$T_MS_COLOR_TEMPEX;

.field m_stFactoryExt:Lcom/android/server/tv/DataBaseDesk$MS_FACTORY_EXTERN_SETTING;

.field m_stPEQSet:Lcom/android/server/tv/DataBaseDesk$ST_FACTORY_PEQ_SETTING;

.field soundpara:Lcom/android/server/tv/DataBaseDesk$MS_USER_SOUND_SETTING;

.field stCECPara:Lcom/android/server/tv/DataBaseDesk$MS_CEC_SETTING;

.field stSubtitleSet:Lcom/android/server/tv/DataBaseDesk$MS_USER_SUBTITLE_SETTING;

.field stUserLocationSetting:Lcom/android/server/tv/DataBaseDesk$MS_USER_LOCATION_SETTING;

.field stUsrColorTemp:Lcom/android/server/tv/DataBaseDesk$T_MS_COLOR_TEMP_DATA;

.field stUsrColorTempEx:[Lcom/android/server/tv/DataBaseDesk$T_MS_COLOR_TEMPEX_DATA;

.field stUsrData:Lcom/android/server/tv/DataBaseDesk$MS_USER_SYSTEM_SETTING;

.field private tableDirtyFlags:[Ljava/lang/Boolean;

.field private userSettingCusSchema:Ljava/lang/String;

.field private userSettingSchema:Ljava/lang/String;

.field videopara:Lcom/android/server/tv/DataBaseDesk$T_MS_VIDEO;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    const/4 v0, 0x0

    sput-object v0, Lcom/android/server/tv/DataBaseDeskImpl;->mContext:Landroid/content/Context;

    return-void
.end method

.method private constructor <init>()V
    .locals 2

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    const/4 v0, 0x0

    iput-object v0, p0, Lcom/android/server/tv/DataBaseDeskImpl;->mContentResolver:Landroid/content/ContentResolver;

    new-instance v0, Landroid/os/Handler;

    invoke-direct {v0}, Landroid/os/Handler;-><init>()V

    iput-object v0, p0, Lcom/android/server/tv/DataBaseDeskImpl;->handler:Landroid/os/Handler;

    const-string v0, "content://mstar.tv.usersetting"

    iput-object v0, p0, Lcom/android/server/tv/DataBaseDeskImpl;->userSettingSchema:Ljava/lang/String;

    const-string v0, "content://konka.tv.usersetting"

    iput-object v0, p0, Lcom/android/server/tv/DataBaseDeskImpl;->userSettingCusSchema:Ljava/lang/String;

    const-string v0, "content://mstar.tv.factory"

    iput-object v0, p0, Lcom/android/server/tv/DataBaseDeskImpl;->factorySchema:Ljava/lang/String;

    const-string v0, "content://konka.tv.factory"

    iput-object v0, p0, Lcom/android/server/tv/DataBaseDeskImpl;->factoryCusSchema:Ljava/lang/String;

    sget-object v0, Lcom/android/server/tv/DataBaseDesk$MAX_DTV_Resolution_Info;->E_DTV_MAX:Lcom/android/server/tv/DataBaseDesk$MAX_DTV_Resolution_Info;

    invoke-virtual {v0}, Lcom/android/server/tv/DataBaseDesk$MAX_DTV_Resolution_Info;->ordinal()I

    move-result v0

    sget-object v1, Lcom/android/server/tv/DataBaseDesk$MAPI_VIDEO_ARC_Type;->E_AR_MAX:Lcom/android/server/tv/DataBaseDesk$MAPI_VIDEO_ARC_Type;

    invoke-virtual {v1}, Lcom/android/server/tv/DataBaseDesk$MAPI_VIDEO_ARC_Type;->ordinal()I

    move-result v1

    filled-new-array {v0, v1}, [I

    move-result-object v0

    const-class v1, Lcom/android/server/tv/DataBaseDesk$ST_MAPI_VIDEO_WINDOW_INFO;

    invoke-static {v1, v0}, Ljava/lang/reflect/Array;->newInstance(Ljava/lang/Class;[I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [[Lcom/android/server/tv/DataBaseDesk$ST_MAPI_VIDEO_WINDOW_INFO;

    iput-object v0, p0, Lcom/android/server/tv/DataBaseDeskImpl;->m_DTVOverscanSet:[[Lcom/android/server/tv/DataBaseDesk$ST_MAPI_VIDEO_WINDOW_INFO;

    sget-object v0, Lcom/android/server/tv/DataBaseDesk$MAX_HDMI_Resolution_Info;->E_HDMI_MAX:Lcom/android/server/tv/DataBaseDesk$MAX_HDMI_Resolution_Info;

    invoke-virtual {v0}, Lcom/android/server/tv/DataBaseDesk$MAX_HDMI_Resolution_Info;->ordinal()I

    move-result v0

    sget-object v1, Lcom/android/server/tv/DataBaseDesk$MAPI_VIDEO_ARC_Type;->E_AR_MAX:Lcom/android/server/tv/DataBaseDesk$MAPI_VIDEO_ARC_Type;

    invoke-virtual {v1}, Lcom/android/server/tv/DataBaseDesk$MAPI_VIDEO_ARC_Type;->ordinal()I

    move-result v1

    filled-new-array {v0, v1}, [I

    move-result-object v0

    const-class v1, Lcom/android/server/tv/DataBaseDesk$ST_MAPI_VIDEO_WINDOW_INFO;

    invoke-static {v1, v0}, Ljava/lang/reflect/Array;->newInstance(Ljava/lang/Class;[I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [[Lcom/android/server/tv/DataBaseDesk$ST_MAPI_VIDEO_WINDOW_INFO;

    iput-object v0, p0, Lcom/android/server/tv/DataBaseDeskImpl;->m_HDMIOverscanSet:[[Lcom/android/server/tv/DataBaseDesk$ST_MAPI_VIDEO_WINDOW_INFO;

    sget-object v0, Lcom/android/server/tv/DataBaseDesk$MAX_YPbPr_Resolution_Info;->E_YPbPr_MAX:Lcom/android/server/tv/DataBaseDesk$MAX_YPbPr_Resolution_Info;

    invoke-virtual {v0}, Lcom/android/server/tv/DataBaseDesk$MAX_YPbPr_Resolution_Info;->ordinal()I

    move-result v0

    sget-object v1, Lcom/android/server/tv/DataBaseDesk$MAPI_VIDEO_ARC_Type;->E_AR_MAX:Lcom/android/server/tv/DataBaseDesk$MAPI_VIDEO_ARC_Type;

    invoke-virtual {v1}, Lcom/android/server/tv/DataBaseDesk$MAPI_VIDEO_ARC_Type;->ordinal()I

    move-result v1

    filled-new-array {v0, v1}, [I

    move-result-object v0

    const-class v1, Lcom/android/server/tv/DataBaseDesk$ST_MAPI_VIDEO_WINDOW_INFO;

    invoke-static {v1, v0}, Ljava/lang/reflect/Array;->newInstance(Ljava/lang/Class;[I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [[Lcom/android/server/tv/DataBaseDesk$ST_MAPI_VIDEO_WINDOW_INFO;

    iput-object v0, p0, Lcom/android/server/tv/DataBaseDeskImpl;->m_YPbPrOverscanSet:[[Lcom/android/server/tv/DataBaseDesk$ST_MAPI_VIDEO_WINDOW_INFO;

    sget-object v0, Lcom/android/server/tv/DataBaseDesk$EN_VD_SIGNALTYPE;->SIG_NUMS:Lcom/android/server/tv/DataBaseDesk$EN_VD_SIGNALTYPE;

    invoke-virtual {v0}, Lcom/android/server/tv/DataBaseDesk$EN_VD_SIGNALTYPE;->ordinal()I

    move-result v0

    sget-object v1, Lcom/android/server/tv/DataBaseDesk$MAPI_VIDEO_ARC_Type;->E_AR_MAX:Lcom/android/server/tv/DataBaseDesk$MAPI_VIDEO_ARC_Type;

    invoke-virtual {v1}, Lcom/android/server/tv/DataBaseDesk$MAPI_VIDEO_ARC_Type;->ordinal()I

    move-result v1

    filled-new-array {v0, v1}, [I

    move-result-object v0

    const-class v1, Lcom/android/server/tv/DataBaseDesk$ST_MAPI_VIDEO_WINDOW_INFO;

    invoke-static {v1, v0}, Ljava/lang/reflect/Array;->newInstance(Ljava/lang/Class;[I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [[Lcom/android/server/tv/DataBaseDesk$ST_MAPI_VIDEO_WINDOW_INFO;

    iput-object v0, p0, Lcom/android/server/tv/DataBaseDeskImpl;->m_VDOverscanSet:[[Lcom/android/server/tv/DataBaseDesk$ST_MAPI_VIDEO_WINDOW_INFO;

    sget-object v0, Lcom/android/server/tv/DataBaseDesk$EN_VD_SIGNALTYPE;->SIG_NUMS:Lcom/android/server/tv/DataBaseDesk$EN_VD_SIGNALTYPE;

    invoke-virtual {v0}, Lcom/android/server/tv/DataBaseDesk$EN_VD_SIGNALTYPE;->ordinal()I

    move-result v0

    sget-object v1, Lcom/android/server/tv/DataBaseDesk$MAPI_VIDEO_ARC_Type;->E_AR_MAX:Lcom/android/server/tv/DataBaseDesk$MAPI_VIDEO_ARC_Type;

    invoke-virtual {v1}, Lcom/android/server/tv/DataBaseDesk$MAPI_VIDEO_ARC_Type;->ordinal()I

    move-result v1

    filled-new-array {v0, v1}, [I

    move-result-object v0

    const-class v1, Lcom/android/server/tv/DataBaseDesk$ST_MAPI_VIDEO_WINDOW_INFO;

    invoke-static {v1, v0}, Ljava/lang/reflect/Array;->newInstance(Ljava/lang/Class;[I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [[Lcom/android/server/tv/DataBaseDesk$ST_MAPI_VIDEO_WINDOW_INFO;

    iput-object v0, p0, Lcom/android/server/tv/DataBaseDeskImpl;->m_ATVOverscanSet:[[Lcom/android/server/tv/DataBaseDesk$ST_MAPI_VIDEO_WINDOW_INFO;

    return-void
.end method

.method private InitSettingVar()Z
    .locals 11

    const-wide/16 v3, 0x0

    const/4 v10, 0x1

    const/16 v1, 0x80

    const/4 v9, 0x0

    const-string v0, "TvService"

    const-string v2, "SettingServiceImpl InitVar!!"

    invoke-static {v0, v2}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    new-instance v0, Lcom/android/server/tv/DataBaseDesk$MS_USER_SYSTEM_SETTING;

    invoke-direct {v0}, Lcom/android/server/tv/DataBaseDesk$MS_USER_SYSTEM_SETTING;-><init>()V

    iput-object v0, p0, Lcom/android/server/tv/DataBaseDeskImpl;->stUsrData:Lcom/android/server/tv/DataBaseDesk$MS_USER_SYSTEM_SETTING;

    iget-object v0, p0, Lcom/android/server/tv/DataBaseDeskImpl;->stUsrData:Lcom/android/server/tv/DataBaseDesk$MS_USER_SYSTEM_SETTING;

    const v2, 0xffff

    iput v2, v0, Lcom/android/server/tv/DataBaseDesk$MS_USER_SYSTEM_SETTING;->checkSum:I

    iget-object v0, p0, Lcom/android/server/tv/DataBaseDeskImpl;->stUsrData:Lcom/android/server/tv/DataBaseDesk$MS_USER_SYSTEM_SETTING;

    iput-boolean v10, v0, Lcom/android/server/tv/DataBaseDesk$MS_USER_SYSTEM_SETTING;->fRunInstallationGuide:Z

    iget-object v0, p0, Lcom/android/server/tv/DataBaseDeskImpl;->stUsrData:Lcom/android/server/tv/DataBaseDesk$MS_USER_SYSTEM_SETTING;

    iput-boolean v9, v0, Lcom/android/server/tv/DataBaseDesk$MS_USER_SYSTEM_SETTING;->fNoChannel:Z

    iget-object v0, p0, Lcom/android/server/tv/DataBaseDeskImpl;->stUsrData:Lcom/android/server/tv/DataBaseDesk$MS_USER_SYSTEM_SETTING;

    iput-boolean v9, v0, Lcom/android/server/tv/DataBaseDesk$MS_USER_SYSTEM_SETTING;->bDisableSiAutoUpdate:Z

    iget-object v0, p0, Lcom/android/server/tv/DataBaseDeskImpl;->stUsrData:Lcom/android/server/tv/DataBaseDesk$MS_USER_SYSTEM_SETTING;

    sget-object v2, Lcom/mstar/android/tvapi/common/vo/TvOsType$EnumInputSource;->E_INPUT_SOURCE_ATV:Lcom/mstar/android/tvapi/common/vo/TvOsType$EnumInputSource;

    iput-object v2, v0, Lcom/android/server/tv/DataBaseDesk$MS_USER_SYSTEM_SETTING;->enInputSourceType:Lcom/mstar/android/tvapi/common/vo/TvOsType$EnumInputSource;

    iget-object v0, p0, Lcom/android/server/tv/DataBaseDeskImpl;->stUsrData:Lcom/android/server/tv/DataBaseDesk$MS_USER_SYSTEM_SETTING;

    sget-object v2, Lcom/android/server/tv/DataBaseDesk$MEMBER_COUNTRY;->E_CHINA:Lcom/android/server/tv/DataBaseDesk$MEMBER_COUNTRY;

    iput-object v2, v0, Lcom/android/server/tv/DataBaseDesk$MS_USER_SYSTEM_SETTING;->Country:Lcom/android/server/tv/DataBaseDesk$MEMBER_COUNTRY;

    iget-object v0, p0, Lcom/android/server/tv/DataBaseDeskImpl;->stUsrData:Lcom/android/server/tv/DataBaseDesk$MS_USER_SYSTEM_SETTING;

    sget-object v2, Lcom/android/server/tv/DataBaseDesk$EN_CABLE_OPERATORS;->EN_CABLEOP_CDSMATV:Lcom/android/server/tv/DataBaseDesk$EN_CABLE_OPERATORS;

    iput-object v2, v0, Lcom/android/server/tv/DataBaseDesk$MS_USER_SYSTEM_SETTING;->enCableOperators:Lcom/android/server/tv/DataBaseDesk$EN_CABLE_OPERATORS;

    iget-object v0, p0, Lcom/android/server/tv/DataBaseDeskImpl;->stUsrData:Lcom/android/server/tv/DataBaseDesk$MS_USER_SYSTEM_SETTING;

    sget-object v2, Lcom/android/server/tv/DataBaseDesk$EN_SATELLITE_PLATFORM;->EN_SATEPF_HDPLUS:Lcom/android/server/tv/DataBaseDesk$EN_SATELLITE_PLATFORM;

    iput-object v2, v0, Lcom/android/server/tv/DataBaseDesk$MS_USER_SYSTEM_SETTING;->enSatellitePlatform:Lcom/android/server/tv/DataBaseDesk$EN_SATELLITE_PLATFORM;

    iget-object v0, p0, Lcom/android/server/tv/DataBaseDeskImpl;->stUsrData:Lcom/android/server/tv/DataBaseDesk$MS_USER_SYSTEM_SETTING;

    iput-short v9, v0, Lcom/android/server/tv/DataBaseDesk$MS_USER_SYSTEM_SETTING;->u8OADTime:S

    iget-object v0, p0, Lcom/android/server/tv/DataBaseDeskImpl;->stUsrData:Lcom/android/server/tv/DataBaseDesk$MS_USER_SYSTEM_SETTING;

    iput-short v9, v0, Lcom/android/server/tv/DataBaseDesk$MS_USER_SYSTEM_SETTING;->fOADScanAfterWakeup:S

    iget-object v0, p0, Lcom/android/server/tv/DataBaseDeskImpl;->stUsrData:Lcom/android/server/tv/DataBaseDesk$MS_USER_SYSTEM_SETTING;

    iput-short v9, v0, Lcom/android/server/tv/DataBaseDesk$MS_USER_SYSTEM_SETTING;->fAutoVolume:S

    iget-object v0, p0, Lcom/android/server/tv/DataBaseDeskImpl;->stUsrData:Lcom/android/server/tv/DataBaseDesk$MS_USER_SYSTEM_SETTING;

    iput-short v9, v0, Lcom/android/server/tv/DataBaseDesk$MS_USER_SYSTEM_SETTING;->fDcPowerOFFMode:S

    iget-object v0, p0, Lcom/android/server/tv/DataBaseDeskImpl;->stUsrData:Lcom/android/server/tv/DataBaseDesk$MS_USER_SYSTEM_SETTING;

    iput-short v9, v0, Lcom/android/server/tv/DataBaseDesk$MS_USER_SYSTEM_SETTING;->DtvRoute:S

    iget-object v0, p0, Lcom/android/server/tv/DataBaseDeskImpl;->stUsrData:Lcom/android/server/tv/DataBaseDesk$MS_USER_SYSTEM_SETTING;

    iput-short v9, v0, Lcom/android/server/tv/DataBaseDesk$MS_USER_SYSTEM_SETTING;->ScartOutRGB:S

    iget-object v0, p0, Lcom/android/server/tv/DataBaseDeskImpl;->stUsrData:Lcom/android/server/tv/DataBaseDesk$MS_USER_SYSTEM_SETTING;

    iput-short v9, v0, Lcom/android/server/tv/DataBaseDesk$MS_USER_SYSTEM_SETTING;->U8Transparency:S

    iget-object v0, p0, Lcom/android/server/tv/DataBaseDeskImpl;->stUsrData:Lcom/android/server/tv/DataBaseDesk$MS_USER_SYSTEM_SETTING;

    iput-wide v3, v0, Lcom/android/server/tv/DataBaseDesk$MS_USER_SYSTEM_SETTING;->u32MenuTimeOut:J

    iget-object v0, p0, Lcom/android/server/tv/DataBaseDeskImpl;->stUsrData:Lcom/android/server/tv/DataBaseDesk$MS_USER_SYSTEM_SETTING;

    iput-short v9, v0, Lcom/android/server/tv/DataBaseDesk$MS_USER_SYSTEM_SETTING;->AudioOnly:S

    iget-object v0, p0, Lcom/android/server/tv/DataBaseDeskImpl;->stUsrData:Lcom/android/server/tv/DataBaseDesk$MS_USER_SYSTEM_SETTING;

    iput-short v9, v0, Lcom/android/server/tv/DataBaseDesk$MS_USER_SYSTEM_SETTING;->bEnableWDT:S

    iget-object v0, p0, Lcom/android/server/tv/DataBaseDeskImpl;->stUsrData:Lcom/android/server/tv/DataBaseDesk$MS_USER_SYSTEM_SETTING;

    iput-short v9, v0, Lcom/android/server/tv/DataBaseDesk$MS_USER_SYSTEM_SETTING;->u8FavoriteRegion:S

    iget-object v0, p0, Lcom/android/server/tv/DataBaseDeskImpl;->stUsrData:Lcom/android/server/tv/DataBaseDesk$MS_USER_SYSTEM_SETTING;

    iput-short v9, v0, Lcom/android/server/tv/DataBaseDesk$MS_USER_SYSTEM_SETTING;->u8Bandwidth:S

    iget-object v0, p0, Lcom/android/server/tv/DataBaseDeskImpl;->stUsrData:Lcom/android/server/tv/DataBaseDesk$MS_USER_SYSTEM_SETTING;

    iput-short v9, v0, Lcom/android/server/tv/DataBaseDesk$MS_USER_SYSTEM_SETTING;->u8TimeShiftSizeType:S

    iget-object v0, p0, Lcom/android/server/tv/DataBaseDeskImpl;->stUsrData:Lcom/android/server/tv/DataBaseDesk$MS_USER_SYSTEM_SETTING;

    iput-short v9, v0, Lcom/android/server/tv/DataBaseDesk$MS_USER_SYSTEM_SETTING;->fOadScan:S

    iget-object v0, p0, Lcom/android/server/tv/DataBaseDeskImpl;->stUsrData:Lcom/android/server/tv/DataBaseDesk$MS_USER_SYSTEM_SETTING;

    iput-short v9, v0, Lcom/android/server/tv/DataBaseDesk$MS_USER_SYSTEM_SETTING;->bEnablePVRRecordAll:S

    iget-object v0, p0, Lcom/android/server/tv/DataBaseDeskImpl;->stUsrData:Lcom/android/server/tv/DataBaseDesk$MS_USER_SYSTEM_SETTING;

    iput-short v9, v0, Lcom/android/server/tv/DataBaseDesk$MS_USER_SYSTEM_SETTING;->u8ColorRangeMode:S

    iget-object v0, p0, Lcom/android/server/tv/DataBaseDeskImpl;->stUsrData:Lcom/android/server/tv/DataBaseDesk$MS_USER_SYSTEM_SETTING;

    iput-short v9, v0, Lcom/android/server/tv/DataBaseDesk$MS_USER_SYSTEM_SETTING;->u8HDMIAudioSource:S

    iget-object v0, p0, Lcom/android/server/tv/DataBaseDeskImpl;->stUsrData:Lcom/android/server/tv/DataBaseDesk$MS_USER_SYSTEM_SETTING;

    iput-short v9, v0, Lcom/android/server/tv/DataBaseDesk$MS_USER_SYSTEM_SETTING;->bEnableAlwaysTimeshift:S

    iget-object v0, p0, Lcom/android/server/tv/DataBaseDeskImpl;->stUsrData:Lcom/android/server/tv/DataBaseDesk$MS_USER_SYSTEM_SETTING;

    sget-object v2, Lcom/android/server/tv/DataBaseDesk$EN_MS_SUPER;->MS_SUPER_OFF:Lcom/android/server/tv/DataBaseDesk$EN_MS_SUPER;

    iput-object v2, v0, Lcom/android/server/tv/DataBaseDesk$MS_USER_SYSTEM_SETTING;->eSUPER:Lcom/android/server/tv/DataBaseDesk$EN_MS_SUPER;

    iget-object v0, p0, Lcom/android/server/tv/DataBaseDeskImpl;->stUsrData:Lcom/android/server/tv/DataBaseDesk$MS_USER_SYSTEM_SETTING;

    iput-boolean v9, v0, Lcom/android/server/tv/DataBaseDesk$MS_USER_SYSTEM_SETTING;->bUartBus:Z

    iget-object v0, p0, Lcom/android/server/tv/DataBaseDeskImpl;->stUsrData:Lcom/android/server/tv/DataBaseDesk$MS_USER_SYSTEM_SETTING;

    iput-short v9, v0, Lcom/android/server/tv/DataBaseDesk$MS_USER_SYSTEM_SETTING;->m_AutoZoom:S

    iget-object v0, p0, Lcom/android/server/tv/DataBaseDeskImpl;->stUsrData:Lcom/android/server/tv/DataBaseDesk$MS_USER_SYSTEM_SETTING;

    iput-boolean v9, v0, Lcom/android/server/tv/DataBaseDesk$MS_USER_SYSTEM_SETTING;->bOverScan:Z

    iget-object v0, p0, Lcom/android/server/tv/DataBaseDeskImpl;->stUsrData:Lcom/android/server/tv/DataBaseDesk$MS_USER_SYSTEM_SETTING;

    iput-short v9, v0, Lcom/android/server/tv/DataBaseDesk$MS_USER_SYSTEM_SETTING;->m_u8BrazilVideoStandardType:S

    iget-object v0, p0, Lcom/android/server/tv/DataBaseDeskImpl;->stUsrData:Lcom/android/server/tv/DataBaseDesk$MS_USER_SYSTEM_SETTING;

    iput-short v9, v0, Lcom/android/server/tv/DataBaseDesk$MS_USER_SYSTEM_SETTING;->m_u8SoftwareUpdateMode:S

    iget-object v0, p0, Lcom/android/server/tv/DataBaseDeskImpl;->stUsrData:Lcom/android/server/tv/DataBaseDesk$MS_USER_SYSTEM_SETTING;

    iput-wide v3, v0, Lcom/android/server/tv/DataBaseDesk$MS_USER_SYSTEM_SETTING;->u32OSD_Active_Time:J

    iget-object v0, p0, Lcom/android/server/tv/DataBaseDeskImpl;->stUsrData:Lcom/android/server/tv/DataBaseDesk$MS_USER_SYSTEM_SETTING;

    iput-boolean v9, v0, Lcom/android/server/tv/DataBaseDesk$MS_USER_SYSTEM_SETTING;->m_MessageBoxExist:Z

    iget-object v0, p0, Lcom/android/server/tv/DataBaseDeskImpl;->stUsrData:Lcom/android/server/tv/DataBaseDesk$MS_USER_SYSTEM_SETTING;

    iput v9, v0, Lcom/android/server/tv/DataBaseDesk$MS_USER_SYSTEM_SETTING;->u16LastOADVersion:I

    iget-object v0, p0, Lcom/android/server/tv/DataBaseDeskImpl;->stUsrData:Lcom/android/server/tv/DataBaseDesk$MS_USER_SYSTEM_SETTING;

    iput-boolean v9, v0, Lcom/android/server/tv/DataBaseDesk$MS_USER_SYSTEM_SETTING;->bEnableAutoChannelUpdate:Z

    iget-object v0, p0, Lcom/android/server/tv/DataBaseDeskImpl;->stUsrData:Lcom/android/server/tv/DataBaseDesk$MS_USER_SYSTEM_SETTING;

    iput-short v9, v0, Lcom/android/server/tv/DataBaseDesk$MS_USER_SYSTEM_SETTING;->u8OsdDuration:S

    iget-object v0, p0, Lcom/android/server/tv/DataBaseDeskImpl;->stUsrData:Lcom/android/server/tv/DataBaseDesk$MS_USER_SYSTEM_SETTING;

    sget-object v2, Lcom/android/server/tv/DataBaseDesk$EN_MS_CHANNEL_SWITCH_MODE;->MS_CHANNEL_SWM_BLACKSCREEN:Lcom/android/server/tv/DataBaseDesk$EN_MS_CHANNEL_SWITCH_MODE;

    iput-object v2, v0, Lcom/android/server/tv/DataBaseDesk$MS_USER_SYSTEM_SETTING;->eChSwMode:Lcom/android/server/tv/DataBaseDesk$EN_MS_CHANNEL_SWITCH_MODE;

    iget-object v0, p0, Lcom/android/server/tv/DataBaseDeskImpl;->stUsrData:Lcom/android/server/tv/DataBaseDesk$MS_USER_SYSTEM_SETTING;

    sget-object v2, Lcom/android/server/tv/DataBaseDesk$EN_MS_OFFLINE_DET_MODE;->MS_OFFLINE_DET_OFF:Lcom/android/server/tv/DataBaseDesk$EN_MS_OFFLINE_DET_MODE;

    iput-object v2, v0, Lcom/android/server/tv/DataBaseDesk$MS_USER_SYSTEM_SETTING;->eOffDetMode:Lcom/android/server/tv/DataBaseDesk$EN_MS_OFFLINE_DET_MODE;

    iget-object v0, p0, Lcom/android/server/tv/DataBaseDeskImpl;->stUsrData:Lcom/android/server/tv/DataBaseDesk$MS_USER_SYSTEM_SETTING;

    iput-boolean v9, v0, Lcom/android/server/tv/DataBaseDesk$MS_USER_SYSTEM_SETTING;->bBlueScreen:Z

    iget-object v0, p0, Lcom/android/server/tv/DataBaseDeskImpl;->stUsrData:Lcom/android/server/tv/DataBaseDesk$MS_USER_SYSTEM_SETTING;

    sget-object v2, Lcom/android/server/tv/DataBaseDesk$EN_MS_POWERON_MUSIC;->MS_POWERON_MUSIC_DEFAULT:Lcom/android/server/tv/DataBaseDesk$EN_MS_POWERON_MUSIC;

    iput-object v2, v0, Lcom/android/server/tv/DataBaseDesk$MS_USER_SYSTEM_SETTING;->ePWR_Music:Lcom/android/server/tv/DataBaseDesk$EN_MS_POWERON_MUSIC;

    iget-object v0, p0, Lcom/android/server/tv/DataBaseDeskImpl;->stUsrData:Lcom/android/server/tv/DataBaseDesk$MS_USER_SYSTEM_SETTING;

    sget-object v2, Lcom/android/server/tv/DataBaseDesk$EN_MS_POWERON_LOGO;->MS_POWERON_LOGO_DEFAULT:Lcom/android/server/tv/DataBaseDesk$EN_MS_POWERON_LOGO;

    iput-object v2, v0, Lcom/android/server/tv/DataBaseDesk$MS_USER_SYSTEM_SETTING;->ePWR_Logo:Lcom/android/server/tv/DataBaseDesk$EN_MS_POWERON_LOGO;

    iget-object v0, p0, Lcom/android/server/tv/DataBaseDeskImpl;->stUsrData:Lcom/android/server/tv/DataBaseDesk$MS_USER_SYSTEM_SETTING;

    sget-object v2, Lcom/mstar/android/tvapi/common/vo/TvOsType$EnumLanguage;->E_ENGLISH:Lcom/mstar/android/tvapi/common/vo/TvOsType$EnumLanguage;

    iput-object v2, v0, Lcom/android/server/tv/DataBaseDesk$MS_USER_SYSTEM_SETTING;->enLanguage:Lcom/mstar/android/tvapi/common/vo/TvOsType$EnumLanguage;

    new-instance v0, Lcom/android/server/tv/DataBaseDesk$T_MS_COLOR_TEMP_DATA;

    move v2, v1

    move v3, v1

    move v4, v1

    move v5, v1

    move v6, v1

    invoke-direct/range {v0 .. v6}, Lcom/android/server/tv/DataBaseDesk$T_MS_COLOR_TEMP_DATA;-><init>(SSSSSS)V

    iput-object v0, p0, Lcom/android/server/tv/DataBaseDeskImpl;->stUsrColorTemp:Lcom/android/server/tv/DataBaseDesk$T_MS_COLOR_TEMP_DATA;

    sget-object v0, Lcom/mstar/android/tvapi/common/vo/TvOsType$EnumInputSource;->E_INPUT_SOURCE_NUM:Lcom/mstar/android/tvapi/common/vo/TvOsType$EnumInputSource;

    invoke-virtual {v0}, Lcom/mstar/android/tvapi/common/vo/TvOsType$EnumInputSource;->ordinal()I

    move-result v0

    new-array v0, v0, [Lcom/android/server/tv/DataBaseDesk$T_MS_COLOR_TEMPEX_DATA;

    iput-object v0, p0, Lcom/android/server/tv/DataBaseDeskImpl;->stUsrColorTempEx:[Lcom/android/server/tv/DataBaseDesk$T_MS_COLOR_TEMPEX_DATA;

    const/4 v7, 0x0

    :goto_0
    sget-object v0, Lcom/mstar/android/tvapi/common/vo/TvOsType$EnumInputSource;->E_INPUT_SOURCE_NUM:Lcom/mstar/android/tvapi/common/vo/TvOsType$EnumInputSource;

    invoke-virtual {v0}, Lcom/mstar/android/tvapi/common/vo/TvOsType$EnumInputSource;->ordinal()I

    move-result v0

    if-ge v7, v0, :cond_0

    iget-object v8, p0, Lcom/android/server/tv/DataBaseDeskImpl;->stUsrColorTempEx:[Lcom/android/server/tv/DataBaseDesk$T_MS_COLOR_TEMPEX_DATA;

    new-instance v0, Lcom/android/server/tv/DataBaseDesk$T_MS_COLOR_TEMPEX_DATA;

    move v2, v1

    move v3, v1

    move v4, v1

    move v5, v1

    move v6, v1

    invoke-direct/range {v0 .. v6}, Lcom/android/server/tv/DataBaseDesk$T_MS_COLOR_TEMPEX_DATA;-><init>(IIIIII)V

    aput-object v0, v8, v7

    add-int/lit8 v7, v7, 0x1

    goto :goto_0

    :cond_0
    new-instance v0, Lcom/android/server/tv/DataBaseDesk$MS_USER_SUBTITLE_SETTING;

    sget-object v1, Lcom/mstar/android/tvapi/common/vo/TvOsType$EnumLanguage;->E_ENGLISH:Lcom/mstar/android/tvapi/common/vo/TvOsType$EnumLanguage;

    sget-object v2, Lcom/mstar/android/tvapi/common/vo/TvOsType$EnumLanguage;->E_ENGLISH:Lcom/mstar/android/tvapi/common/vo/TvOsType$EnumLanguage;

    invoke-direct {v0, v1, v2, v9, v9}, Lcom/android/server/tv/DataBaseDesk$MS_USER_SUBTITLE_SETTING;-><init>(Lcom/mstar/android/tvapi/common/vo/TvOsType$EnumLanguage;Lcom/mstar/android/tvapi/common/vo/TvOsType$EnumLanguage;ZZ)V

    iput-object v0, p0, Lcom/android/server/tv/DataBaseDeskImpl;->stSubtitleSet:Lcom/android/server/tv/DataBaseDesk$MS_USER_SUBTITLE_SETTING;

    new-instance v0, Lcom/android/server/tv/DataBaseDesk$MS_USER_LOCATION_SETTING;

    invoke-direct {v0, v9, v9, v9}, Lcom/android/server/tv/DataBaseDesk$MS_USER_LOCATION_SETTING;-><init>(III)V

    iput-object v0, p0, Lcom/android/server/tv/DataBaseDeskImpl;->stUserLocationSetting:Lcom/android/server/tv/DataBaseDesk$MS_USER_LOCATION_SETTING;

    return v10
.end method

.method public static getContext()Landroid/content/Context;
    .locals 1

    sget-object v0, Lcom/android/server/tv/DataBaseDeskImpl;->mContext:Landroid/content/Context;

    return-object v0
.end method

.method private getCurrentInputSource()I
    .locals 3

    const/4 v0, 0x0

    :try_start_0
    invoke-static {}, Lcom/mstar/android/tvapi/common/TvManager;->getInstance()Lcom/mstar/android/tvapi/common/TvManager;

    move-result-object v2

    if-eqz v2, :cond_0

    invoke-static {}, Lcom/mstar/android/tvapi/common/TvManager;->getInstance()Lcom/mstar/android/tvapi/common/TvManager;

    move-result-object v2

    invoke-virtual {v2}, Lcom/mstar/android/tvapi/common/TvManager;->getCurrentInputSource()Lcom/mstar/android/tvapi/common/vo/TvOsType$EnumInputSource;
    :try_end_0
    .catch Lcom/mstar/android/tvapi/common/exception/TvCommonException; {:try_start_0 .. :try_end_0} :catch_0

    move-result-object v0

    :cond_0
    :goto_0
    invoke-virtual {v0}, Lcom/mstar/android/tvapi/common/vo/TvOsType$EnumInputSource;->ordinal()I

    move-result v2

    return v2

    :catch_0
    move-exception v1

    invoke-virtual {v1}, Lcom/mstar/android/tvapi/common/exception/TvCommonException;->printStackTrace()V

    goto :goto_0
.end method

.method public static getDataBaseMgrInstance(Landroid/content/Context;)Lcom/android/server/tv/DataBaseDeskImpl;
    .locals 1
    .param p0    # Landroid/content/Context;

    sput-object p0, Lcom/android/server/tv/DataBaseDeskImpl;->mContext:Landroid/content/Context;

    sget-object v0, Lcom/android/server/tv/DataBaseDeskImpl;->dataBaseMgrImpl:Lcom/android/server/tv/DataBaseDeskImpl;

    if-nez v0, :cond_0

    new-instance v0, Lcom/android/server/tv/DataBaseDeskImpl;

    invoke-direct {v0}, Lcom/android/server/tv/DataBaseDeskImpl;-><init>()V

    sput-object v0, Lcom/android/server/tv/DataBaseDeskImpl;->dataBaseMgrImpl:Lcom/android/server/tv/DataBaseDeskImpl;

    :cond_0
    sget-object v0, Lcom/android/server/tv/DataBaseDeskImpl;->dataBaseMgrImpl:Lcom/android/server/tv/DataBaseDeskImpl;

    return-object v0
.end method

.method private initCECVar()Z
    .locals 6

    const/4 v2, 0x0

    new-instance v0, Lcom/android/server/tv/DataBaseDesk$MS_CEC_SETTING;

    const v1, 0xffff

    move v3, v2

    move v4, v2

    move v5, v2

    invoke-direct/range {v0 .. v5}, Lcom/android/server/tv/DataBaseDesk$MS_CEC_SETTING;-><init>(ISSSS)V

    iput-object v0, p0, Lcom/android/server/tv/DataBaseDeskImpl;->stCECPara:Lcom/android/server/tv/DataBaseDesk$MS_CEC_SETTING;

    const/4 v0, 0x1

    return v0
.end method

.method private initVarFactory()Z
    .locals 12

    new-instance v10, Lcom/android/server/tv/DataBaseDesk$T_MS_COLOR_TEMP;

    invoke-direct {v10}, Lcom/android/server/tv/DataBaseDesk$T_MS_COLOR_TEMP;-><init>()V

    iput-object v10, p0, Lcom/android/server/tv/DataBaseDeskImpl;->m_stFactoryColorTemp:Lcom/android/server/tv/DataBaseDesk$T_MS_COLOR_TEMP;

    new-instance v10, Lcom/android/server/tv/DataBaseDesk$MS_ADC_SETTING;

    invoke-direct {v10}, Lcom/android/server/tv/DataBaseDesk$MS_ADC_SETTING;-><init>()V

    iput-object v10, p0, Lcom/android/server/tv/DataBaseDeskImpl;->m_stFactoryAdc:Lcom/android/server/tv/DataBaseDesk$MS_ADC_SETTING;

    new-instance v10, Lcom/android/server/tv/DataBaseDesk$MS_NLA_SETTING;

    invoke-direct {v10}, Lcom/android/server/tv/DataBaseDesk$MS_NLA_SETTING;-><init>()V

    iput-object v10, p0, Lcom/android/server/tv/DataBaseDeskImpl;->m_pastNLASet:Lcom/android/server/tv/DataBaseDesk$MS_NLA_SETTING;

    new-instance v10, Lcom/android/server/tv/DataBaseDesk$MS_FACTORY_EXTERN_SETTING;

    invoke-direct {v10}, Lcom/android/server/tv/DataBaseDesk$MS_FACTORY_EXTERN_SETTING;-><init>()V

    iput-object v10, p0, Lcom/android/server/tv/DataBaseDeskImpl;->m_stFactoryExt:Lcom/android/server/tv/DataBaseDesk$MS_FACTORY_EXTERN_SETTING;

    new-instance v10, Lcom/android/server/tv/DataBaseDesk$MS_Factory_NS_VD_SET;

    invoke-direct {v10}, Lcom/android/server/tv/DataBaseDesk$MS_Factory_NS_VD_SET;-><init>()V

    iput-object v10, p0, Lcom/android/server/tv/DataBaseDeskImpl;->mNoStandSet:Lcom/android/server/tv/DataBaseDesk$MS_Factory_NS_VD_SET;

    new-instance v10, Lcom/android/server/tv/DataBaseDesk$MS_Factory_NS_VIF_SET;

    invoke-direct {v10}, Lcom/android/server/tv/DataBaseDesk$MS_Factory_NS_VIF_SET;-><init>()V

    iput-object v10, p0, Lcom/android/server/tv/DataBaseDeskImpl;->mVifSet:Lcom/android/server/tv/DataBaseDesk$MS_Factory_NS_VIF_SET;

    new-instance v10, Lcom/android/server/tv/DataBaseDesk$MS_FACTORY_SSC_SET;

    invoke-direct {v10}, Lcom/android/server/tv/DataBaseDesk$MS_FACTORY_SSC_SET;-><init>()V

    iput-object v10, p0, Lcom/android/server/tv/DataBaseDeskImpl;->mSscSet:Lcom/android/server/tv/DataBaseDesk$MS_FACTORY_SSC_SET;

    const/4 v10, 0x0

    iput-boolean v10, p0, Lcom/android/server/tv/DataBaseDeskImpl;->m_bADCAutoTune:Z

    sget-object v10, Lcom/android/server/tv/DataBaseDesk$MAX_DTV_Resolution_Info;->E_DTV_MAX:Lcom/android/server/tv/DataBaseDesk$MAX_DTV_Resolution_Info;

    invoke-virtual {v10}, Lcom/android/server/tv/DataBaseDesk$MAX_DTV_Resolution_Info;->ordinal()I

    move-result v8

    sget-object v10, Lcom/android/server/tv/DataBaseDesk$MAPI_VIDEO_ARC_Type;->E_AR_MAX:Lcom/android/server/tv/DataBaseDesk$MAPI_VIDEO_ARC_Type;

    invoke-virtual {v10}, Lcom/android/server/tv/DataBaseDesk$MAPI_VIDEO_ARC_Type;->ordinal()I

    move-result v9

    const/4 v0, 0x0

    :goto_0
    if-ge v0, v8, :cond_1

    const/4 v4, 0x0

    :goto_1
    if-ge v4, v9, :cond_0

    iget-object v10, p0, Lcom/android/server/tv/DataBaseDeskImpl;->m_DTVOverscanSet:[[Lcom/android/server/tv/DataBaseDesk$ST_MAPI_VIDEO_WINDOW_INFO;

    aget-object v10, v10, v0

    new-instance v11, Lcom/android/server/tv/DataBaseDesk$ST_MAPI_VIDEO_WINDOW_INFO;

    invoke-direct {v11}, Lcom/android/server/tv/DataBaseDesk$ST_MAPI_VIDEO_WINDOW_INFO;-><init>()V

    aput-object v11, v10, v4

    add-int/lit8 v4, v4, 0x1

    goto :goto_1

    :cond_0
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    :cond_1
    sget-object v10, Lcom/android/server/tv/DataBaseDesk$MAX_HDMI_Resolution_Info;->E_HDMI_MAX:Lcom/android/server/tv/DataBaseDesk$MAX_HDMI_Resolution_Info;

    invoke-virtual {v10}, Lcom/android/server/tv/DataBaseDesk$MAX_HDMI_Resolution_Info;->ordinal()I

    move-result v8

    sget-object v10, Lcom/android/server/tv/DataBaseDesk$MAPI_VIDEO_ARC_Type;->E_AR_MAX:Lcom/android/server/tv/DataBaseDesk$MAPI_VIDEO_ARC_Type;

    invoke-virtual {v10}, Lcom/android/server/tv/DataBaseDesk$MAPI_VIDEO_ARC_Type;->ordinal()I

    move-result v9

    const/4 v1, 0x0

    :goto_2
    if-ge v1, v8, :cond_3

    const/4 v5, 0x0

    :goto_3
    if-ge v5, v9, :cond_2

    iget-object v10, p0, Lcom/android/server/tv/DataBaseDeskImpl;->m_HDMIOverscanSet:[[Lcom/android/server/tv/DataBaseDesk$ST_MAPI_VIDEO_WINDOW_INFO;

    aget-object v10, v10, v1

    new-instance v11, Lcom/android/server/tv/DataBaseDesk$ST_MAPI_VIDEO_WINDOW_INFO;

    invoke-direct {v11}, Lcom/android/server/tv/DataBaseDesk$ST_MAPI_VIDEO_WINDOW_INFO;-><init>()V

    aput-object v11, v10, v5

    add-int/lit8 v5, v5, 0x1

    goto :goto_3

    :cond_2
    add-int/lit8 v1, v1, 0x1

    goto :goto_2

    :cond_3
    sget-object v10, Lcom/android/server/tv/DataBaseDesk$MAX_YPbPr_Resolution_Info;->E_YPbPr_MAX:Lcom/android/server/tv/DataBaseDesk$MAX_YPbPr_Resolution_Info;

    invoke-virtual {v10}, Lcom/android/server/tv/DataBaseDesk$MAX_YPbPr_Resolution_Info;->ordinal()I

    move-result v8

    sget-object v10, Lcom/android/server/tv/DataBaseDesk$MAPI_VIDEO_ARC_Type;->E_AR_MAX:Lcom/android/server/tv/DataBaseDesk$MAPI_VIDEO_ARC_Type;

    invoke-virtual {v10}, Lcom/android/server/tv/DataBaseDesk$MAPI_VIDEO_ARC_Type;->ordinal()I

    move-result v9

    const/4 v2, 0x0

    :goto_4
    if-ge v2, v8, :cond_5

    const/4 v6, 0x0

    :goto_5
    if-ge v6, v9, :cond_4

    iget-object v10, p0, Lcom/android/server/tv/DataBaseDeskImpl;->m_YPbPrOverscanSet:[[Lcom/android/server/tv/DataBaseDesk$ST_MAPI_VIDEO_WINDOW_INFO;

    aget-object v10, v10, v2

    new-instance v11, Lcom/android/server/tv/DataBaseDesk$ST_MAPI_VIDEO_WINDOW_INFO;

    invoke-direct {v11}, Lcom/android/server/tv/DataBaseDesk$ST_MAPI_VIDEO_WINDOW_INFO;-><init>()V

    aput-object v11, v10, v6

    add-int/lit8 v6, v6, 0x1

    goto :goto_5

    :cond_4
    add-int/lit8 v2, v2, 0x1

    goto :goto_4

    :cond_5
    sget-object v10, Lcom/android/server/tv/DataBaseDesk$EN_VD_SIGNALTYPE;->SIG_NUMS:Lcom/android/server/tv/DataBaseDesk$EN_VD_SIGNALTYPE;

    invoke-virtual {v10}, Lcom/android/server/tv/DataBaseDesk$EN_VD_SIGNALTYPE;->ordinal()I

    move-result v8

    sget-object v10, Lcom/android/server/tv/DataBaseDesk$MAPI_VIDEO_ARC_Type;->E_AR_MAX:Lcom/android/server/tv/DataBaseDesk$MAPI_VIDEO_ARC_Type;

    invoke-virtual {v10}, Lcom/android/server/tv/DataBaseDesk$MAPI_VIDEO_ARC_Type;->ordinal()I

    move-result v9

    const/4 v3, 0x0

    :goto_6
    if-ge v3, v8, :cond_7

    const/4 v7, 0x0

    :goto_7
    if-ge v7, v9, :cond_6

    iget-object v10, p0, Lcom/android/server/tv/DataBaseDeskImpl;->m_VDOverscanSet:[[Lcom/android/server/tv/DataBaseDesk$ST_MAPI_VIDEO_WINDOW_INFO;

    aget-object v10, v10, v3

    new-instance v11, Lcom/android/server/tv/DataBaseDesk$ST_MAPI_VIDEO_WINDOW_INFO;

    invoke-direct {v11}, Lcom/android/server/tv/DataBaseDesk$ST_MAPI_VIDEO_WINDOW_INFO;-><init>()V

    aput-object v11, v10, v7

    iget-object v10, p0, Lcom/android/server/tv/DataBaseDeskImpl;->m_ATVOverscanSet:[[Lcom/android/server/tv/DataBaseDesk$ST_MAPI_VIDEO_WINDOW_INFO;

    aget-object v10, v10, v3

    new-instance v11, Lcom/android/server/tv/DataBaseDesk$ST_MAPI_VIDEO_WINDOW_INFO;

    invoke-direct {v11}, Lcom/android/server/tv/DataBaseDesk$ST_MAPI_VIDEO_WINDOW_INFO;-><init>()V

    aput-object v11, v10, v7

    add-int/lit8 v7, v7, 0x1

    goto :goto_7

    :cond_6
    add-int/lit8 v3, v3, 0x1

    goto :goto_6

    :cond_7
    new-instance v10, Lcom/android/server/tv/DataBaseDesk$ST_FACTORY_PEQ_SETTING;

    invoke-direct {v10}, Lcom/android/server/tv/DataBaseDesk$ST_FACTORY_PEQ_SETTING;-><init>()V

    iput-object v10, p0, Lcom/android/server/tv/DataBaseDeskImpl;->m_stPEQSet:Lcom/android/server/tv/DataBaseDesk$ST_FACTORY_PEQ_SETTING;

    new-instance v10, Lcom/android/server/tv/DataBaseDesk$ST_FACTORY_CI_SETTING;

    invoke-direct {v10}, Lcom/android/server/tv/DataBaseDesk$ST_FACTORY_CI_SETTING;-><init>()V

    iput-object v10, p0, Lcom/android/server/tv/DataBaseDeskImpl;->m_stCISet:Lcom/android/server/tv/DataBaseDesk$ST_FACTORY_CI_SETTING;

    new-instance v10, Lcom/android/server/tv/DataBaseDesk$T_MS_COLOR_TEMP;

    invoke-direct {v10}, Lcom/android/server/tv/DataBaseDesk$T_MS_COLOR_TEMP;-><init>()V

    iput-object v10, p0, Lcom/android/server/tv/DataBaseDeskImpl;->m_stFactoryColorTemp:Lcom/android/server/tv/DataBaseDesk$T_MS_COLOR_TEMP;

    new-instance v10, Lcom/android/server/tv/DataBaseDesk$T_MS_COLOR_TEMPEX;

    invoke-direct {v10}, Lcom/android/server/tv/DataBaseDesk$T_MS_COLOR_TEMPEX;-><init>()V

    iput-object v10, p0, Lcom/android/server/tv/DataBaseDeskImpl;->m_stFactoryColorTempEx:Lcom/android/server/tv/DataBaseDesk$T_MS_COLOR_TEMPEX;

    const/4 v10, 0x1

    return v10
.end method

.method private initVarPicture()Z
    .locals 24

    const/4 v2, 0x7

    new-array v0, v2, [[S

    move-object/from16 v21, v0

    const/4 v2, 0x0

    const/4 v3, 0x5

    new-array v3, v3, [S

    fill-array-data v3, :array_0

    aput-object v3, v21, v2

    const/4 v2, 0x1

    const/4 v3, 0x5

    new-array v3, v3, [S

    fill-array-data v3, :array_1

    aput-object v3, v21, v2

    const/4 v2, 0x2

    const/4 v3, 0x5

    new-array v3, v3, [S

    fill-array-data v3, :array_2

    aput-object v3, v21, v2

    const/4 v2, 0x3

    const/4 v3, 0x5

    new-array v3, v3, [S

    fill-array-data v3, :array_3

    aput-object v3, v21, v2

    const/4 v2, 0x4

    const/4 v3, 0x5

    new-array v3, v3, [S

    fill-array-data v3, :array_4

    aput-object v3, v21, v2

    const/4 v2, 0x5

    const/4 v3, 0x5

    new-array v3, v3, [S

    fill-array-data v3, :array_5

    aput-object v3, v21, v2

    const/4 v2, 0x6

    const/4 v3, 0x5

    new-array v3, v3, [S

    fill-array-data v3, :array_6

    aput-object v3, v21, v2

    new-instance v2, Lcom/android/server/tv/DataBaseDesk$T_MS_VIDEO;

    invoke-direct {v2}, Lcom/android/server/tv/DataBaseDesk$T_MS_VIDEO;-><init>()V

    move-object/from16 v0, p0

    iput-object v2, v0, Lcom/android/server/tv/DataBaseDeskImpl;->videopara:Lcom/android/server/tv/DataBaseDesk$T_MS_VIDEO;

    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/android/server/tv/DataBaseDeskImpl;->videopara:Lcom/android/server/tv/DataBaseDesk$T_MS_VIDEO;

    const v3, 0xffff

    iput v3, v2, Lcom/android/server/tv/DataBaseDesk$T_MS_VIDEO;->CheckSum:I

    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/android/server/tv/DataBaseDeskImpl;->videopara:Lcom/android/server/tv/DataBaseDesk$T_MS_VIDEO;

    sget-object v3, Lcom/android/server/tv/DataBaseDesk$EN_MS_PICTURE;->PICTURE_NORMAL:Lcom/android/server/tv/DataBaseDesk$EN_MS_PICTURE;

    iput-object v3, v2, Lcom/android/server/tv/DataBaseDesk$T_MS_VIDEO;->ePicture:Lcom/android/server/tv/DataBaseDesk$EN_MS_PICTURE;

    sget-object v2, Lcom/android/server/tv/DataBaseDesk$EN_MS_PICTURE;->PICTURE_NUMS:Lcom/android/server/tv/DataBaseDesk$EN_MS_PICTURE;

    invoke-virtual {v2}, Lcom/android/server/tv/DataBaseDesk$EN_MS_PICTURE;->ordinal()I

    move-result v19

    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/android/server/tv/DataBaseDeskImpl;->videopara:Lcom/android/server/tv/DataBaseDesk$T_MS_VIDEO;

    move/from16 v0, v19

    new-array v3, v0, [Lcom/android/server/tv/DataBaseDesk$T_MS_PICTURE;

    iput-object v3, v2, Lcom/android/server/tv/DataBaseDesk$T_MS_VIDEO;->astPicture:[Lcom/android/server/tv/DataBaseDesk$T_MS_PICTURE;

    const/16 v20, 0x0

    :goto_0
    move/from16 v0, v20

    move/from16 v1, v19

    if-ge v0, v1, :cond_0

    const/16 v22, 0x0

    aget-object v2, v21, v20

    add-int/lit8 v23, v22, 0x1

    aget-short v4, v2, v22

    aget-object v2, v21, v20

    add-int/lit8 v22, v23, 0x1

    aget-short v5, v2, v23

    aget-object v2, v21, v20

    add-int/lit8 v23, v22, 0x1

    aget-short v6, v2, v22

    aget-object v2, v21, v20

    add-int/lit8 v22, v23, 0x1

    aget-short v7, v2, v23

    aget-object v2, v21, v20

    aget-short v8, v2, v22

    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/android/server/tv/DataBaseDeskImpl;->videopara:Lcom/android/server/tv/DataBaseDesk$T_MS_VIDEO;

    iget-object v14, v2, Lcom/android/server/tv/DataBaseDesk$T_MS_VIDEO;->astPicture:[Lcom/android/server/tv/DataBaseDesk$T_MS_PICTURE;

    new-instance v2, Lcom/android/server/tv/DataBaseDesk$T_MS_PICTURE;

    const/16 v3, 0x32

    sget-object v9, Lcom/android/server/tv/DataBaseDesk$EN_MS_COLOR_TEMP;->MS_COLOR_TEMP_NATURE:Lcom/android/server/tv/DataBaseDesk$EN_MS_COLOR_TEMP;

    sget-object v10, Lcom/android/server/tv/DataBaseDesk$EN_MS_PIC_ADV;->MS_MIDDLE:Lcom/android/server/tv/DataBaseDesk$EN_MS_PIC_ADV;

    sget-object v11, Lcom/android/server/tv/DataBaseDesk$EN_MS_PIC_ADV;->MS_MIDDLE:Lcom/android/server/tv/DataBaseDesk$EN_MS_PIC_ADV;

    sget-object v12, Lcom/android/server/tv/DataBaseDesk$EN_MS_PIC_ADV;->MS_MIDDLE:Lcom/android/server/tv/DataBaseDesk$EN_MS_PIC_ADV;

    sget-object v13, Lcom/android/server/tv/DataBaseDesk$EN_MS_PIC_ADV;->MS_MIDDLE:Lcom/android/server/tv/DataBaseDesk$EN_MS_PIC_ADV;

    invoke-direct/range {v2 .. v13}, Lcom/android/server/tv/DataBaseDesk$T_MS_PICTURE;-><init>(SSSSSSLcom/android/server/tv/DataBaseDesk$EN_MS_COLOR_TEMP;Lcom/android/server/tv/DataBaseDesk$EN_MS_PIC_ADV;Lcom/android/server/tv/DataBaseDesk$EN_MS_PIC_ADV;Lcom/android/server/tv/DataBaseDesk$EN_MS_PIC_ADV;Lcom/android/server/tv/DataBaseDesk$EN_MS_PIC_ADV;)V

    aput-object v2, v14, v20

    add-int/lit8 v20, v20, 0x1

    goto :goto_0

    :cond_0
    sget-object v2, Lcom/android/server/tv/DataBaseDesk$EN_MS_COLOR_TEMP;->MS_COLOR_TEMP_NUM:Lcom/android/server/tv/DataBaseDesk$EN_MS_COLOR_TEMP;

    invoke-virtual {v2}, Lcom/android/server/tv/DataBaseDesk$EN_MS_COLOR_TEMP;->ordinal()I

    move-result v19

    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/android/server/tv/DataBaseDeskImpl;->videopara:Lcom/android/server/tv/DataBaseDesk$T_MS_VIDEO;

    move/from16 v0, v19

    new-array v3, v0, [Lcom/android/server/tv/DataBaseDesk$T_MS_NR_MODE;

    iput-object v3, v2, Lcom/android/server/tv/DataBaseDesk$T_MS_VIDEO;->eNRMode:[Lcom/android/server/tv/DataBaseDesk$T_MS_NR_MODE;

    const/16 v20, 0x0

    :goto_1
    move/from16 v0, v20

    move/from16 v1, v19

    if-ge v0, v1, :cond_1

    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/android/server/tv/DataBaseDeskImpl;->videopara:Lcom/android/server/tv/DataBaseDesk$T_MS_VIDEO;

    iget-object v2, v2, Lcom/android/server/tv/DataBaseDesk$T_MS_VIDEO;->eNRMode:[Lcom/android/server/tv/DataBaseDesk$T_MS_NR_MODE;

    new-instance v3, Lcom/android/server/tv/DataBaseDesk$T_MS_NR_MODE;

    sget-object v9, Lcom/android/server/tv/DataBaseDesk$EN_MS_NR;->MS_NR_MIDDLE:Lcom/android/server/tv/DataBaseDesk$EN_MS_NR;

    sget-object v10, Lcom/android/server/tv/DataBaseDesk$EN_MS_MPEG_NR;->MS_MPEG_NR_MIDDLE:Lcom/android/server/tv/DataBaseDesk$EN_MS_MPEG_NR;

    invoke-direct {v3, v9, v10}, Lcom/android/server/tv/DataBaseDesk$T_MS_NR_MODE;-><init>(Lcom/android/server/tv/DataBaseDesk$EN_MS_NR;Lcom/android/server/tv/DataBaseDesk$EN_MS_MPEG_NR;)V

    aput-object v3, v2, v20

    add-int/lit8 v20, v20, 0x1

    goto :goto_1

    :cond_1
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/android/server/tv/DataBaseDeskImpl;->videopara:Lcom/android/server/tv/DataBaseDesk$T_MS_VIDEO;

    new-instance v3, Lcom/android/server/tv/DataBaseDesk$T_MS_SUB_COLOR;

    const v9, 0xffff

    const/4 v10, 0x0

    const/4 v11, 0x0

    invoke-direct {v3, v9, v10, v11}, Lcom/android/server/tv/DataBaseDesk$T_MS_SUB_COLOR;-><init>(ISS)V

    iput-object v3, v2, Lcom/android/server/tv/DataBaseDesk$T_MS_VIDEO;->g_astSubColor:Lcom/android/server/tv/DataBaseDesk$T_MS_SUB_COLOR;

    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/android/server/tv/DataBaseDeskImpl;->videopara:Lcom/android/server/tv/DataBaseDesk$T_MS_VIDEO;

    sget-object v3, Lcom/android/server/tv/DataBaseDesk$MAPI_VIDEO_ARC_Type;->E_AR_16x9:Lcom/android/server/tv/DataBaseDesk$MAPI_VIDEO_ARC_Type;

    iput-object v3, v2, Lcom/android/server/tv/DataBaseDesk$T_MS_VIDEO;->enARCType:Lcom/android/server/tv/DataBaseDesk$MAPI_VIDEO_ARC_Type;

    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/android/server/tv/DataBaseDeskImpl;->videopara:Lcom/android/server/tv/DataBaseDesk$T_MS_VIDEO;

    sget-object v3, Lcom/android/server/tv/DataBaseDesk$EN_DISPLAY_RES_TYPE;->DISPLAY_RES_FULL_HD:Lcom/android/server/tv/DataBaseDesk$EN_DISPLAY_RES_TYPE;

    iput-object v3, v2, Lcom/android/server/tv/DataBaseDesk$T_MS_VIDEO;->fOutput_RES:Lcom/android/server/tv/DataBaseDesk$EN_DISPLAY_RES_TYPE;

    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/android/server/tv/DataBaseDeskImpl;->videopara:Lcom/android/server/tv/DataBaseDesk$T_MS_VIDEO;

    sget-object v3, Lcom/android/server/tv/DataBaseDesk$MAPI_VIDEO_OUT_VE_SYS;->MAPI_VIDEO_OUT_VE_AUTO:Lcom/android/server/tv/DataBaseDesk$MAPI_VIDEO_OUT_VE_SYS;

    iput-object v3, v2, Lcom/android/server/tv/DataBaseDesk$T_MS_VIDEO;->tvsys:Lcom/android/server/tv/DataBaseDesk$MAPI_VIDEO_OUT_VE_SYS;

    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/android/server/tv/DataBaseDeskImpl;->videopara:Lcom/android/server/tv/DataBaseDesk$T_MS_VIDEO;

    sget-object v3, Lcom/android/server/tv/DataBaseDesk$MAPI_AVD_VideoStandardType;->E_MAPI_VIDEOSTANDARD_AUTO:Lcom/android/server/tv/DataBaseDesk$MAPI_AVD_VideoStandardType;

    iput-object v3, v2, Lcom/android/server/tv/DataBaseDesk$T_MS_VIDEO;->LastVideoStandardMode:Lcom/android/server/tv/DataBaseDesk$MAPI_AVD_VideoStandardType;

    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/android/server/tv/DataBaseDeskImpl;->videopara:Lcom/android/server/tv/DataBaseDesk$T_MS_VIDEO;

    sget-object v3, Lcom/android/server/tv/DataBaseDesk$AUDIOMODE_TYPE_;->E_AUDIOMODE_MONO_:Lcom/android/server/tv/DataBaseDesk$AUDIOMODE_TYPE_;

    iput-object v3, v2, Lcom/android/server/tv/DataBaseDesk$T_MS_VIDEO;->LastAudioStandardMode:Lcom/android/server/tv/DataBaseDesk$AUDIOMODE_TYPE_;

    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/android/server/tv/DataBaseDeskImpl;->videopara:Lcom/android/server/tv/DataBaseDesk$T_MS_VIDEO;

    sget-object v3, Lcom/android/server/tv/DataBaseDesk$EN_MS_Dynamic_Contrast;->MS_Dynamic_Contrast_OFF:Lcom/android/server/tv/DataBaseDesk$EN_MS_Dynamic_Contrast;

    iput-object v3, v2, Lcom/android/server/tv/DataBaseDesk$T_MS_VIDEO;->eDynamic_Contrast:Lcom/android/server/tv/DataBaseDesk$EN_MS_Dynamic_Contrast;

    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/android/server/tv/DataBaseDeskImpl;->videopara:Lcom/android/server/tv/DataBaseDesk$T_MS_VIDEO;

    sget-object v3, Lcom/android/server/tv/DataBaseDesk$EN_MS_FILM;->MS_FILM_OFF:Lcom/android/server/tv/DataBaseDesk$EN_MS_FILM;

    iput-object v3, v2, Lcom/android/server/tv/DataBaseDesk$T_MS_VIDEO;->eFilm:Lcom/android/server/tv/DataBaseDesk$EN_MS_FILM;

    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/android/server/tv/DataBaseDeskImpl;->videopara:Lcom/android/server/tv/DataBaseDesk$T_MS_VIDEO;

    new-instance v9, Lcom/android/server/tv/DataBaseDesk$ThreeD_Video_MODE;

    sget-object v10, Lcom/android/server/tv/DataBaseDesk$EN_ThreeD_Video;->DB_ThreeD_Video_OFF:Lcom/android/server/tv/DataBaseDesk$EN_ThreeD_Video;

    sget-object v11, Lcom/android/server/tv/DataBaseDesk$EN_ThreeD_Video_SELFADAPTIVE_DETECT;->DB_ThreeD_Video_SELF_ADAPTIVE_DETECT_OFF:Lcom/android/server/tv/DataBaseDesk$EN_ThreeD_Video_SELFADAPTIVE_DETECT;

    sget-object v12, Lcom/android/server/tv/DataBaseDesk$EN_ThreeD_Video_DISPLAYFORMAT;->DB_ThreeD_Video_DISPLAYFORMAT_NONE:Lcom/android/server/tv/DataBaseDesk$EN_ThreeD_Video_DISPLAYFORMAT;

    sget-object v13, Lcom/android/server/tv/DataBaseDesk$EN_ThreeD_Video_3DTO2D;->DB_ThreeD_Video_3DTO2D_NONE:Lcom/android/server/tv/DataBaseDesk$EN_ThreeD_Video_3DTO2D;

    sget-object v14, Lcom/android/server/tv/DataBaseDesk$EN_ThreeD_Video_3DDEPTH;->DB_ThreeD_Video_3DDEPTH_LEVEL_15:Lcom/android/server/tv/DataBaseDesk$EN_ThreeD_Video_3DDEPTH;

    sget-object v15, Lcom/android/server/tv/DataBaseDesk$EN_ThreeD_Video_3DOFFSET;->DB_ThreeD_Video_3DOFFSET_LEVEL_15:Lcom/android/server/tv/DataBaseDesk$EN_ThreeD_Video_3DOFFSET;

    sget-object v16, Lcom/android/server/tv/DataBaseDesk$EN_ThreeD_Video_AUTOSTART;->DB_ThreeD_Video_AUTOSTART_OFF:Lcom/android/server/tv/DataBaseDesk$EN_ThreeD_Video_AUTOSTART;

    sget-object v17, Lcom/android/server/tv/DataBaseDesk$EN_ThreeD_Video_3DOUTPUTASPECT;->DB_ThreeD_Video_3DOUTPUTASPECT_FULLSCREEN:Lcom/android/server/tv/DataBaseDesk$EN_ThreeD_Video_3DOUTPUTASPECT;

    sget-object v18, Lcom/android/server/tv/DataBaseDesk$EN_ThreeD_Video_LRVIEWSWITCH;->DB_ThreeD_Video_LRVIEWSWITCH_NOTEXCHANGE:Lcom/android/server/tv/DataBaseDesk$EN_ThreeD_Video_LRVIEWSWITCH;

    invoke-direct/range {v9 .. v18}, Lcom/android/server/tv/DataBaseDesk$ThreeD_Video_MODE;-><init>(Lcom/android/server/tv/DataBaseDesk$EN_ThreeD_Video;Lcom/android/server/tv/DataBaseDesk$EN_ThreeD_Video_SELFADAPTIVE_DETECT;Lcom/android/server/tv/DataBaseDesk$EN_ThreeD_Video_DISPLAYFORMAT;Lcom/android/server/tv/DataBaseDesk$EN_ThreeD_Video_3DTO2D;Lcom/android/server/tv/DataBaseDesk$EN_ThreeD_Video_3DDEPTH;Lcom/android/server/tv/DataBaseDesk$EN_ThreeD_Video_3DOFFSET;Lcom/android/server/tv/DataBaseDesk$EN_ThreeD_Video_AUTOSTART;Lcom/android/server/tv/DataBaseDesk$EN_ThreeD_Video_3DOUTPUTASPECT;Lcom/android/server/tv/DataBaseDesk$EN_ThreeD_Video_LRVIEWSWITCH;)V

    iput-object v9, v2, Lcom/android/server/tv/DataBaseDesk$T_MS_VIDEO;->ThreeDVideoMode:Lcom/android/server/tv/DataBaseDesk$ThreeD_Video_MODE;

    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/android/server/tv/DataBaseDeskImpl;->videopara:Lcom/android/server/tv/DataBaseDesk$T_MS_VIDEO;

    new-instance v3, Lcom/android/server/tv/DataBaseDesk$T_MS_OVERSCAN_SETTING_USER;

    const/4 v9, 0x0

    const/4 v10, 0x0

    const/4 v11, 0x0

    const/4 v12, 0x0

    invoke-direct {v3, v9, v10, v11, v12}, Lcom/android/server/tv/DataBaseDesk$T_MS_OVERSCAN_SETTING_USER;-><init>(SSSS)V

    iput-object v3, v2, Lcom/android/server/tv/DataBaseDesk$T_MS_VIDEO;->stUserOverScanMode:Lcom/android/server/tv/DataBaseDesk$T_MS_OVERSCAN_SETTING_USER;

    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/android/server/tv/DataBaseDeskImpl;->videopara:Lcom/android/server/tv/DataBaseDesk$T_MS_VIDEO;

    sget-object v3, Lcom/android/server/tv/DataBaseDesk$EN_DISPLAY_TVFORMAT;->DISPLAY_TVFORMAT_16TO9HD:Lcom/android/server/tv/DataBaseDesk$EN_DISPLAY_TVFORMAT;

    iput-object v3, v2, Lcom/android/server/tv/DataBaseDesk$T_MS_VIDEO;->eTvFormat:Lcom/android/server/tv/DataBaseDesk$EN_DISPLAY_TVFORMAT;

    new-instance v9, Lcom/android/server/tv/DataBaseDesk$T_MS_COLOR_TEMPEX_DATA;

    const/16 v10, 0x80

    const/16 v11, 0x80

    const/16 v12, 0x80

    const/16 v13, 0x80

    const/16 v14, 0x80

    const/16 v15, 0x80

    invoke-direct/range {v9 .. v15}, Lcom/android/server/tv/DataBaseDesk$T_MS_COLOR_TEMPEX_DATA;-><init>(IIIIII)V

    move-object/from16 v0, p0

    iput-object v9, v0, Lcom/android/server/tv/DataBaseDeskImpl;->colorParaEx:Lcom/android/server/tv/DataBaseDesk$T_MS_COLOR_TEMPEX_DATA;

    const/4 v2, 0x1

    return v2

    nop

    :array_0
    .array-data 2
        0x32s
        0x32s
        0x32s
        0x32s
        0x32s
    .end array-data

    nop

    :array_1
    .array-data 2
        0x3cs
        0x37s
        0x3cs
        0x3cs
        0x32s
    .end array-data

    nop

    :array_2
    .array-data 2
        0x28s
        0x2ds
        0x2ds
        0x28s
        0x32s
    .end array-data

    nop

    :array_3
    .array-data 2
        0x32s
        0x32s
        0x32s
        0x32s
        0x32s
    .end array-data

    nop

    :array_4
    .array-data 2
        0x32s
        0x32s
        0x32s
        0x32s
        0x32s
    .end array-data

    nop

    :array_5
    .array-data 2
        0x32s
        0x32s
        0x32s
        0x32s
        0x32s
    .end array-data

    nop

    :array_6
    .array-data 2
        0x32s
        0x32s
        0x32s
        0x32s
        0x32s
    .end array-data
.end method

.method private initVarSound()Z
    .locals 15

    const/16 v14, 0x1e

    const/4 v13, 0x1

    const/16 v12, 0x3c

    const/16 v11, 0x28

    const/16 v1, 0x32

    new-instance v0, Lcom/android/server/tv/DataBaseDesk$MS_USER_SOUND_SETTING;

    invoke-direct {v0}, Lcom/android/server/tv/DataBaseDesk$MS_USER_SOUND_SETTING;-><init>()V

    iput-object v0, p0, Lcom/android/server/tv/DataBaseDeskImpl;->soundpara:Lcom/android/server/tv/DataBaseDesk$MS_USER_SOUND_SETTING;

    iget-object v0, p0, Lcom/android/server/tv/DataBaseDeskImpl;->soundpara:Lcom/android/server/tv/DataBaseDesk$MS_USER_SOUND_SETTING;

    const v2, 0xffff

    iput v2, v0, Lcom/android/server/tv/DataBaseDesk$MS_USER_SOUND_SETTING;->u16CheckSum:I

    sget-object v0, Lcom/android/server/tv/DataBaseDesk$EN_SOUND_MODE;->SOUND_MODE_NUM:Lcom/android/server/tv/DataBaseDesk$EN_SOUND_MODE;

    invoke-virtual {v0}, Lcom/android/server/tv/DataBaseDesk$EN_SOUND_MODE;->ordinal()I

    move-result v0

    new-array v0, v0, [Lcom/android/server/tv/DataBaseDesk$SoundModeSeting;

    iput-object v0, p0, Lcom/android/server/tv/DataBaseDeskImpl;->astSoundModeSetting:[Lcom/android/server/tv/DataBaseDesk$SoundModeSeting;

    iget-object v8, p0, Lcom/android/server/tv/DataBaseDeskImpl;->astSoundModeSetting:[Lcom/android/server/tv/DataBaseDesk$SoundModeSeting;

    const/4 v9, 0x0

    new-instance v0, Lcom/android/server/tv/DataBaseDesk$SoundModeSeting;

    move v2, v1

    move v3, v1

    move v4, v1

    move v5, v1

    move v6, v1

    move v7, v1

    invoke-direct/range {v0 .. v7}, Lcom/android/server/tv/DataBaseDesk$SoundModeSeting;-><init>(SSSSSSS)V

    aput-object v0, v8, v9

    iget-object v0, p0, Lcom/android/server/tv/DataBaseDeskImpl;->astSoundModeSetting:[Lcom/android/server/tv/DataBaseDesk$SoundModeSeting;

    new-instance v2, Lcom/android/server/tv/DataBaseDesk$SoundModeSeting;

    const/16 v3, 0x46

    const/16 v5, 0x46

    move v4, v11

    move v6, v12

    move v7, v1

    move v8, v1

    move v9, v11

    invoke-direct/range {v2 .. v9}, Lcom/android/server/tv/DataBaseDesk$SoundModeSeting;-><init>(SSSSSSS)V

    aput-object v2, v0, v13

    iget-object v0, p0, Lcom/android/server/tv/DataBaseDeskImpl;->astSoundModeSetting:[Lcom/android/server/tv/DataBaseDesk$SoundModeSeting;

    const/4 v10, 0x2

    new-instance v2, Lcom/android/server/tv/DataBaseDesk$SoundModeSeting;

    const/16 v7, 0x2d

    move v3, v12

    move v4, v14

    move v5, v12

    move v6, v1

    move v8, v11

    move v9, v14

    invoke-direct/range {v2 .. v9}, Lcom/android/server/tv/DataBaseDesk$SoundModeSeting;-><init>(SSSSSSS)V

    aput-object v2, v0, v10

    iget-object v0, p0, Lcom/android/server/tv/DataBaseDeskImpl;->astSoundModeSetting:[Lcom/android/server/tv/DataBaseDesk$SoundModeSeting;

    const/4 v10, 0x3

    new-instance v2, Lcom/android/server/tv/DataBaseDesk$SoundModeSeting;

    const/16 v4, 0x50

    const/16 v6, 0x2d

    const/16 v9, 0x50

    move v3, v11

    move v5, v11

    move v7, v1

    move v8, v12

    invoke-direct/range {v2 .. v9}, Lcom/android/server/tv/DataBaseDesk$SoundModeSeting;-><init>(SSSSSSS)V

    aput-object v2, v0, v10

    iget-object v8, p0, Lcom/android/server/tv/DataBaseDeskImpl;->astSoundModeSetting:[Lcom/android/server/tv/DataBaseDesk$SoundModeSeting;

    const/4 v9, 0x4

    new-instance v0, Lcom/android/server/tv/DataBaseDesk$SoundModeSeting;

    move v2, v1

    move v3, v1

    move v4, v1

    move v5, v1

    move v6, v1

    move v7, v1

    invoke-direct/range {v0 .. v7}, Lcom/android/server/tv/DataBaseDesk$SoundModeSeting;-><init>(SSSSSSS)V

    aput-object v0, v8, v9

    iget-object v8, p0, Lcom/android/server/tv/DataBaseDeskImpl;->astSoundModeSetting:[Lcom/android/server/tv/DataBaseDesk$SoundModeSeting;

    const/4 v9, 0x5

    new-instance v0, Lcom/android/server/tv/DataBaseDesk$SoundModeSeting;

    move v2, v1

    move v3, v1

    move v4, v1

    move v5, v1

    move v6, v1

    move v7, v1

    invoke-direct/range {v0 .. v7}, Lcom/android/server/tv/DataBaseDesk$SoundModeSeting;-><init>(SSSSSSS)V

    aput-object v0, v8, v9

    iget-object v8, p0, Lcom/android/server/tv/DataBaseDeskImpl;->astSoundModeSetting:[Lcom/android/server/tv/DataBaseDesk$SoundModeSeting;

    const/4 v9, 0x6

    new-instance v0, Lcom/android/server/tv/DataBaseDesk$SoundModeSeting;

    move v2, v1

    move v3, v1

    move v4, v1

    move v5, v1

    move v6, v1

    move v7, v1

    invoke-direct/range {v0 .. v7}, Lcom/android/server/tv/DataBaseDesk$SoundModeSeting;-><init>(SSSSSSS)V

    aput-object v0, v8, v9

    return v13
.end method

.method public static setContext(Landroid/content/Context;)V
    .locals 0
    .param p0    # Landroid/content/Context;

    sput-object p0, Lcom/android/server/tv/DataBaseDeskImpl;->mContext:Landroid/content/Context;

    return-void
.end method


# virtual methods
.method public UpdateDB()V
    .locals 2

    sget-object v0, Lcom/android/server/tv/DataBaseDeskImpl;->dataBaseMgrImpl:Lcom/android/server/tv/DataBaseDeskImpl;

    invoke-direct {p0}, Lcom/android/server/tv/DataBaseDeskImpl;->getCurrentInputSource()I

    move-result v1

    invoke-virtual {v0, v1}, Lcom/android/server/tv/DataBaseDeskImpl;->queryAllVideoPara(I)Lcom/android/server/tv/DataBaseDesk$T_MS_VIDEO;

    return-void
.end method

.method public getAdcSetting()Lcom/android/server/tv/DataBaseDesk$MS_ADC_SETTING;
    .locals 1

    iget-object v0, p0, Lcom/android/server/tv/DataBaseDeskImpl;->m_stFactoryAdc:Lcom/android/server/tv/DataBaseDesk$MS_ADC_SETTING;

    return-object v0
.end method

.method public getCECVar()Lcom/android/server/tv/DataBaseDesk$MS_CEC_SETTING;
    .locals 1

    iget-object v0, p0, Lcom/android/server/tv/DataBaseDeskImpl;->stCECPara:Lcom/android/server/tv/DataBaseDesk$MS_CEC_SETTING;

    return-object v0
.end method

.method public getContentResolver()Landroid/content/ContentResolver;
    .locals 1

    iget-object v0, p0, Lcom/android/server/tv/DataBaseDeskImpl;->mContentResolver:Landroid/content/ContentResolver;

    if-nez v0, :cond_0

    sget-object v0, Lcom/android/server/tv/DataBaseDeskImpl;->mContext:Landroid/content/Context;

    invoke-virtual {v0}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v0

    iput-object v0, p0, Lcom/android/server/tv/DataBaseDeskImpl;->mContentResolver:Landroid/content/ContentResolver;

    :cond_0
    iget-object v0, p0, Lcom/android/server/tv/DataBaseDeskImpl;->mContentResolver:Landroid/content/ContentResolver;

    return-object v0
.end method

.method public getFactoryExt()Lcom/android/server/tv/DataBaseDesk$MS_FACTORY_EXTERN_SETTING;
    .locals 1

    iget-object v0, p0, Lcom/android/server/tv/DataBaseDeskImpl;->m_stFactoryExt:Lcom/android/server/tv/DataBaseDesk$MS_FACTORY_EXTERN_SETTING;

    return-object v0
.end method

.method public getLocationSet()Lcom/android/server/tv/DataBaseDesk$MS_USER_LOCATION_SETTING;
    .locals 1

    iget-object v0, p0, Lcom/android/server/tv/DataBaseDeskImpl;->stUserLocationSetting:Lcom/android/server/tv/DataBaseDesk$MS_USER_LOCATION_SETTING;

    return-object v0
.end method

.method public getNoStandSet()Lcom/android/server/tv/DataBaseDesk$MS_Factory_NS_VD_SET;
    .locals 1

    iget-object v0, p0, Lcom/android/server/tv/DataBaseDeskImpl;->mNoStandSet:Lcom/android/server/tv/DataBaseDesk$MS_Factory_NS_VD_SET;

    return-object v0
.end method

.method public getNoStandVifSet()Lcom/android/server/tv/DataBaseDesk$MS_Factory_NS_VIF_SET;
    .locals 1

    iget-object v0, p0, Lcom/android/server/tv/DataBaseDeskImpl;->mVifSet:Lcom/android/server/tv/DataBaseDesk$MS_Factory_NS_VIF_SET;

    return-object v0
.end method

.method public getSound()Lcom/android/server/tv/DataBaseDesk$MS_USER_SOUND_SETTING;
    .locals 1

    iget-object v0, p0, Lcom/android/server/tv/DataBaseDeskImpl;->soundpara:Lcom/android/server/tv/DataBaseDesk$MS_USER_SOUND_SETTING;

    return-object v0
.end method

.method public getSoundMode(Lcom/android/server/tv/DataBaseDesk$EN_SOUND_MODE;)Lcom/android/server/tv/DataBaseDesk$SoundModeSeting;
    .locals 2
    .param p1    # Lcom/android/server/tv/DataBaseDesk$EN_SOUND_MODE;

    invoke-virtual {p1}, Lcom/android/server/tv/DataBaseDesk$EN_SOUND_MODE;->ordinal()I

    move-result v0

    iget-object v1, p0, Lcom/android/server/tv/DataBaseDeskImpl;->astSoundModeSetting:[Lcom/android/server/tv/DataBaseDesk$SoundModeSeting;

    aget-object v1, v1, v0

    return-object v1
.end method

.method public getSscSet()Lcom/android/server/tv/DataBaseDesk$MS_FACTORY_SSC_SET;
    .locals 1

    iget-object v0, p0, Lcom/android/server/tv/DataBaseDeskImpl;->mSscSet:Lcom/android/server/tv/DataBaseDesk$MS_FACTORY_SSC_SET;

    return-object v0
.end method

.method public getSubtitleSet()Lcom/android/server/tv/DataBaseDesk$MS_USER_SUBTITLE_SETTING;
    .locals 1

    iget-object v0, p0, Lcom/android/server/tv/DataBaseDeskImpl;->stSubtitleSet:Lcom/android/server/tv/DataBaseDesk$MS_USER_SUBTITLE_SETTING;

    return-object v0
.end method

.method public getTableDirtyFlags(I)Ljava/lang/Boolean;
    .locals 1
    .param p1    # I

    iget-object v0, p0, Lcom/android/server/tv/DataBaseDeskImpl;->tableDirtyFlags:[Ljava/lang/Boolean;

    aget-object v0, v0, p1

    return-object v0
.end method

.method public getUsrData()Lcom/android/server/tv/DataBaseDesk$MS_USER_SYSTEM_SETTING;
    .locals 1

    iget-object v0, p0, Lcom/android/server/tv/DataBaseDeskImpl;->stUsrData:Lcom/android/server/tv/DataBaseDesk$MS_USER_SYSTEM_SETTING;

    return-object v0
.end method

.method public getVideo()Lcom/android/server/tv/DataBaseDesk$T_MS_VIDEO;
    .locals 1

    iget-object v0, p0, Lcom/android/server/tv/DataBaseDeskImpl;->videopara:Lcom/android/server/tv/DataBaseDesk$T_MS_VIDEO;

    return-object v0
.end method

.method public getVideoInfo()Lcom/mstar/android/tvapi/common/vo/VideoInfo;
    .locals 3

    const/4 v1, 0x0

    :try_start_0
    invoke-static {}, Lcom/mstar/android/tvapi/common/TvManager;->getInstance()Lcom/mstar/android/tvapi/common/TvManager;

    move-result-object v2

    if-eqz v2, :cond_0

    invoke-static {}, Lcom/mstar/android/tvapi/common/TvManager;->getInstance()Lcom/mstar/android/tvapi/common/TvManager;

    move-result-object v2

    invoke-virtual {v2}, Lcom/mstar/android/tvapi/common/TvManager;->getPlayerManager()Lcom/mstar/android/tvapi/common/TvPlayer;

    move-result-object v2

    invoke-interface {v2}, Lcom/mstar/android/tvapi/common/TvPlayer;->getVideoInfo()Lcom/mstar/android/tvapi/common/vo/VideoInfo;
    :try_end_0
    .catch Lcom/mstar/android/tvapi/common/exception/TvCommonException; {:try_start_0 .. :try_end_0} :catch_0

    move-result-object v1

    :cond_0
    :goto_0
    return-object v1

    :catch_0
    move-exception v0

    invoke-virtual {v0}, Lcom/mstar/android/tvapi/common/exception/TvCommonException;->printStackTrace()V

    goto :goto_0
.end method

.method public getVideoTemp()Lcom/android/server/tv/DataBaseDesk$T_MS_COLOR_TEMP;
    .locals 1

    iget-object v0, p0, Lcom/android/server/tv/DataBaseDeskImpl;->m_stFactoryColorTemp:Lcom/android/server/tv/DataBaseDesk$T_MS_COLOR_TEMP;

    return-object v0
.end method

.method public getVideoTempEx()Lcom/android/server/tv/DataBaseDesk$T_MS_COLOR_TEMPEX_DATA;
    .locals 1

    iget-object v0, p0, Lcom/android/server/tv/DataBaseDeskImpl;->colorParaEx:Lcom/android/server/tv/DataBaseDesk$T_MS_COLOR_TEMPEX_DATA;

    return-object v0
.end method

.method public isPCTimingNew()Z
    .locals 4

    const/4 v0, 0x0

    const/4 v0, 0x0

    :goto_0
    const/16 v1, 0x9

    if-ge v0, v1, :cond_1

    const-string v1, "DataBaseDeskImpl"

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "~method(isPCTimingNew)~~~~~~~ id is :"

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, "||||||   ModeIndex is:"

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {p0, v0}, Lcom/android/server/tv/DataBaseDeskImpl;->queryPCModeIndex(I)I

    move-result v3

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, "~~~~~~~~~~~"

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    invoke-virtual {p0, v0}, Lcom/android/server/tv/DataBaseDeskImpl;->queryPCModeIndex(I)I

    move-result v1

    int-to-short v1, v1

    invoke-virtual {p0}, Lcom/android/server/tv/DataBaseDeskImpl;->getVideoInfo()Lcom/mstar/android/tvapi/common/vo/VideoInfo;

    move-result-object v2

    iget v2, v2, Lcom/mstar/android/tvapi/common/vo/VideoInfo;->modeIndex:I

    if-ne v1, v2, :cond_0

    const-string v1, "DataBaseDeskImpl"

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "~~~~~~~~isPCTimingNew id is "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, "~~~~~~~~~~~"

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    const/4 v1, 0x1

    :goto_1
    return v1

    :cond_0
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    :cond_1
    const/4 v1, 0x0

    goto :goto_1
.end method

.method public queryADCAdjust(I)Lcom/mstar/android/tvapi/factory/vo/PqlCalibrationData;
    .locals 8
    .param p1    # I

    const/4 v2, 0x0

    invoke-virtual {p0}, Lcom/android/server/tv/DataBaseDeskImpl;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v0

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "content://mstar.tv.factory/adcadjust/sourceid/"

    invoke-virtual {v1, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v1}, Landroid/net/Uri;->parse(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v1

    move-object v3, v2

    move-object v4, v2

    move-object v5, v2

    invoke-virtual/range {v0 .. v5}, Landroid/content/ContentResolver;->query(Landroid/net/Uri;[Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;)Landroid/database/Cursor;

    move-result-object v6

    new-instance v7, Lcom/mstar/android/tvapi/factory/vo/PqlCalibrationData;

    invoke-direct {v7}, Lcom/mstar/android/tvapi/factory/vo/PqlCalibrationData;-><init>()V

    invoke-interface {v6}, Landroid/database/Cursor;->moveToNext()Z

    move-result v0

    if-eqz v0, :cond_0

    const-string v0, "u16RedGain"

    invoke-interface {v6, v0}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v0

    invoke-interface {v6, v0}, Landroid/database/Cursor;->getInt(I)I

    move-result v0

    iput v0, v7, Lcom/mstar/android/tvapi/factory/vo/PqlCalibrationData;->redGain:I

    const-string v0, "u16GreenGain"

    invoke-interface {v6, v0}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v0

    invoke-interface {v6, v0}, Landroid/database/Cursor;->getInt(I)I

    move-result v0

    iput v0, v7, Lcom/mstar/android/tvapi/factory/vo/PqlCalibrationData;->greenGain:I

    const-string v0, "u16BlueGain"

    invoke-interface {v6, v0}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v0

    invoke-interface {v6, v0}, Landroid/database/Cursor;->getInt(I)I

    move-result v0

    iput v0, v7, Lcom/mstar/android/tvapi/factory/vo/PqlCalibrationData;->blueGain:I

    const-string v0, "u16RedOffset"

    invoke-interface {v6, v0}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v0

    invoke-interface {v6, v0}, Landroid/database/Cursor;->getInt(I)I

    move-result v0

    iput v0, v7, Lcom/mstar/android/tvapi/factory/vo/PqlCalibrationData;->redOffset:I

    const-string v0, "u16GreenOffset"

    invoke-interface {v6, v0}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v0

    invoke-interface {v6, v0}, Landroid/database/Cursor;->getInt(I)I

    move-result v0

    iput v0, v7, Lcom/mstar/android/tvapi/factory/vo/PqlCalibrationData;->greenOffset:I

    const-string v0, "u16BlueOffset"

    invoke-interface {v6, v0}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v0

    invoke-interface {v6, v0}, Landroid/database/Cursor;->getInt(I)I

    move-result v0

    iput v0, v7, Lcom/mstar/android/tvapi/factory/vo/PqlCalibrationData;->blueOffset:I

    :cond_0
    invoke-interface {v6}, Landroid/database/Cursor;->close()V

    return-object v7
.end method

.method public queryADCAdjusts()Lcom/android/server/tv/DataBaseDesk$MS_ADC_SETTING;
    .locals 9

    const/4 v2, 0x0

    invoke-virtual {p0}, Lcom/android/server/tv/DataBaseDeskImpl;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v0

    const-string v1, "content://mstar.tv.factory/adcadjust"

    invoke-static {v1}, Landroid/net/Uri;->parse(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v1

    move-object v3, v2

    move-object v4, v2

    move-object v5, v2

    invoke-virtual/range {v0 .. v5}, Landroid/content/ContentResolver;->query(Landroid/net/Uri;[Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;)Landroid/database/Cursor;

    move-result-object v6

    const/4 v7, 0x0

    iget-object v0, p0, Lcom/android/server/tv/DataBaseDeskImpl;->m_stFactoryAdc:Lcom/android/server/tv/DataBaseDesk$MS_ADC_SETTING;

    iget-object v0, v0, Lcom/android/server/tv/DataBaseDesk$MS_ADC_SETTING;->stAdcGainOffsetSetting:[Lcom/android/server/tv/DataBaseDesk$T_MS_CALIBRATION_DATA;

    array-length v8, v0

    :goto_0
    invoke-interface {v6}, Landroid/database/Cursor;->moveToNext()Z

    move-result v0

    if-eqz v0, :cond_0

    add-int/lit8 v0, v8, -0x1

    if-le v7, v0, :cond_1

    :cond_0
    invoke-interface {v6}, Landroid/database/Cursor;->close()V

    iget-object v0, p0, Lcom/android/server/tv/DataBaseDeskImpl;->m_stFactoryAdc:Lcom/android/server/tv/DataBaseDesk$MS_ADC_SETTING;

    return-object v0

    :cond_1
    iget-object v0, p0, Lcom/android/server/tv/DataBaseDeskImpl;->m_stFactoryAdc:Lcom/android/server/tv/DataBaseDesk$MS_ADC_SETTING;

    iget-object v0, v0, Lcom/android/server/tv/DataBaseDesk$MS_ADC_SETTING;->stAdcGainOffsetSetting:[Lcom/android/server/tv/DataBaseDesk$T_MS_CALIBRATION_DATA;

    aget-object v0, v0, v7

    const-string v1, "u16RedGain"

    invoke-interface {v6, v1}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v1

    invoke-interface {v6, v1}, Landroid/database/Cursor;->getInt(I)I

    move-result v1

    iput v1, v0, Lcom/android/server/tv/DataBaseDesk$T_MS_CALIBRATION_DATA;->redgain:I

    iget-object v0, p0, Lcom/android/server/tv/DataBaseDeskImpl;->m_stFactoryAdc:Lcom/android/server/tv/DataBaseDesk$MS_ADC_SETTING;

    iget-object v0, v0, Lcom/android/server/tv/DataBaseDesk$MS_ADC_SETTING;->stAdcGainOffsetSetting:[Lcom/android/server/tv/DataBaseDesk$T_MS_CALIBRATION_DATA;

    aget-object v0, v0, v7

    const-string v1, "u16GreenGain"

    invoke-interface {v6, v1}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v1

    invoke-interface {v6, v1}, Landroid/database/Cursor;->getInt(I)I

    move-result v1

    iput v1, v0, Lcom/android/server/tv/DataBaseDesk$T_MS_CALIBRATION_DATA;->greengain:I

    iget-object v0, p0, Lcom/android/server/tv/DataBaseDeskImpl;->m_stFactoryAdc:Lcom/android/server/tv/DataBaseDesk$MS_ADC_SETTING;

    iget-object v0, v0, Lcom/android/server/tv/DataBaseDesk$MS_ADC_SETTING;->stAdcGainOffsetSetting:[Lcom/android/server/tv/DataBaseDesk$T_MS_CALIBRATION_DATA;

    aget-object v0, v0, v7

    const-string v1, "u16BlueGain"

    invoke-interface {v6, v1}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v1

    invoke-interface {v6, v1}, Landroid/database/Cursor;->getInt(I)I

    move-result v1

    iput v1, v0, Lcom/android/server/tv/DataBaseDesk$T_MS_CALIBRATION_DATA;->bluegain:I

    iget-object v0, p0, Lcom/android/server/tv/DataBaseDeskImpl;->m_stFactoryAdc:Lcom/android/server/tv/DataBaseDesk$MS_ADC_SETTING;

    iget-object v0, v0, Lcom/android/server/tv/DataBaseDesk$MS_ADC_SETTING;->stAdcGainOffsetSetting:[Lcom/android/server/tv/DataBaseDesk$T_MS_CALIBRATION_DATA;

    aget-object v0, v0, v7

    const-string v1, "u16RedOffset"

    invoke-interface {v6, v1}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v1

    invoke-interface {v6, v1}, Landroid/database/Cursor;->getInt(I)I

    move-result v1

    iput v1, v0, Lcom/android/server/tv/DataBaseDesk$T_MS_CALIBRATION_DATA;->redoffset:I

    iget-object v0, p0, Lcom/android/server/tv/DataBaseDeskImpl;->m_stFactoryAdc:Lcom/android/server/tv/DataBaseDesk$MS_ADC_SETTING;

    iget-object v0, v0, Lcom/android/server/tv/DataBaseDesk$MS_ADC_SETTING;->stAdcGainOffsetSetting:[Lcom/android/server/tv/DataBaseDesk$T_MS_CALIBRATION_DATA;

    aget-object v0, v0, v7

    const-string v1, "u16GreenOffset"

    invoke-interface {v6, v1}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v1

    invoke-interface {v6, v1}, Landroid/database/Cursor;->getInt(I)I

    move-result v1

    iput v1, v0, Lcom/android/server/tv/DataBaseDesk$T_MS_CALIBRATION_DATA;->greenoffset:I

    iget-object v0, p0, Lcom/android/server/tv/DataBaseDeskImpl;->m_stFactoryAdc:Lcom/android/server/tv/DataBaseDesk$MS_ADC_SETTING;

    iget-object v0, v0, Lcom/android/server/tv/DataBaseDesk$MS_ADC_SETTING;->stAdcGainOffsetSetting:[Lcom/android/server/tv/DataBaseDesk$T_MS_CALIBRATION_DATA;

    aget-object v0, v0, v7

    const-string v1, "u16BlueOffset"

    invoke-interface {v6, v1}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v1

    invoke-interface {v6, v1}, Landroid/database/Cursor;->getInt(I)I

    move-result v1

    iput v1, v0, Lcom/android/server/tv/DataBaseDesk$T_MS_CALIBRATION_DATA;->blueoffset:I

    add-int/lit8 v7, v7, 0x1

    goto :goto_0
.end method

.method public queryAllVideoPara(I)Lcom/android/server/tv/DataBaseDesk$T_MS_VIDEO;
    .locals 17
    .param p1    # I

    invoke-virtual/range {p0 .. p0}, Lcom/android/server/tv/DataBaseDeskImpl;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v1

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "content://mstar.tv.usersetting/videosetting/inputsrc/"

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    move/from16 v0, p1

    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v2}, Landroid/net/Uri;->parse(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v2

    const/4 v3, 0x0

    const/4 v4, 0x0

    const/4 v5, 0x0

    const/4 v6, 0x0

    invoke-virtual/range {v1 .. v6}, Landroid/content/ContentResolver;->query(Landroid/net/Uri;[Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;)Landroid/database/Cursor;

    move-result-object v13

    :goto_0
    invoke-interface {v13}, Landroid/database/Cursor;->moveToNext()Z

    move-result v1

    if-eqz v1, :cond_0

    move-object/from16 v0, p0

    iget-object v1, v0, Lcom/android/server/tv/DataBaseDeskImpl;->videopara:Lcom/android/server/tv/DataBaseDesk$T_MS_VIDEO;

    invoke-static {}, Lcom/android/server/tv/DataBaseDesk$EN_MS_PICTURE;->values()[Lcom/android/server/tv/DataBaseDesk$EN_MS_PICTURE;

    move-result-object v2

    const-string v3, "ePicture"

    invoke-interface {v13, v3}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v3

    invoke-interface {v13, v3}, Landroid/database/Cursor;->getInt(I)I

    move-result v3

    aget-object v2, v2, v3

    iput-object v2, v1, Lcom/android/server/tv/DataBaseDesk$T_MS_VIDEO;->ePicture:Lcom/android/server/tv/DataBaseDesk$EN_MS_PICTURE;

    move-object/from16 v0, p0

    iget-object v1, v0, Lcom/android/server/tv/DataBaseDeskImpl;->videopara:Lcom/android/server/tv/DataBaseDesk$T_MS_VIDEO;

    invoke-static {}, Lcom/android/server/tv/DataBaseDesk$MAPI_VIDEO_ARC_Type;->values()[Lcom/android/server/tv/DataBaseDesk$MAPI_VIDEO_ARC_Type;

    move-result-object v2

    const-string v3, "enARCType"

    invoke-interface {v13, v3}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v3

    invoke-interface {v13, v3}, Landroid/database/Cursor;->getInt(I)I

    move-result v3

    aget-object v2, v2, v3

    iput-object v2, v1, Lcom/android/server/tv/DataBaseDesk$T_MS_VIDEO;->enARCType:Lcom/android/server/tv/DataBaseDesk$MAPI_VIDEO_ARC_Type;

    move-object/from16 v0, p0

    iget-object v1, v0, Lcom/android/server/tv/DataBaseDeskImpl;->videopara:Lcom/android/server/tv/DataBaseDesk$T_MS_VIDEO;

    invoke-static {}, Lcom/android/server/tv/DataBaseDesk$EN_DISPLAY_RES_TYPE;->values()[Lcom/android/server/tv/DataBaseDesk$EN_DISPLAY_RES_TYPE;

    move-result-object v2

    const-string v3, "fOutput_RES"

    invoke-interface {v13, v3}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v3

    invoke-interface {v13, v3}, Landroid/database/Cursor;->getInt(I)I

    move-result v3

    aget-object v2, v2, v3

    iput-object v2, v1, Lcom/android/server/tv/DataBaseDesk$T_MS_VIDEO;->fOutput_RES:Lcom/android/server/tv/DataBaseDesk$EN_DISPLAY_RES_TYPE;

    move-object/from16 v0, p0

    iget-object v1, v0, Lcom/android/server/tv/DataBaseDeskImpl;->videopara:Lcom/android/server/tv/DataBaseDesk$T_MS_VIDEO;

    invoke-static {}, Lcom/android/server/tv/DataBaseDesk$MAPI_VIDEO_OUT_VE_SYS;->values()[Lcom/android/server/tv/DataBaseDesk$MAPI_VIDEO_OUT_VE_SYS;

    move-result-object v2

    const-string v3, "tvsys"

    invoke-interface {v13, v3}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v3

    invoke-interface {v13, v3}, Landroid/database/Cursor;->getInt(I)I

    move-result v3

    aget-object v2, v2, v3

    iput-object v2, v1, Lcom/android/server/tv/DataBaseDesk$T_MS_VIDEO;->tvsys:Lcom/android/server/tv/DataBaseDesk$MAPI_VIDEO_OUT_VE_SYS;

    move-object/from16 v0, p0

    iget-object v1, v0, Lcom/android/server/tv/DataBaseDeskImpl;->videopara:Lcom/android/server/tv/DataBaseDesk$T_MS_VIDEO;

    invoke-static {}, Lcom/android/server/tv/DataBaseDesk$MAPI_AVD_VideoStandardType;->values()[Lcom/android/server/tv/DataBaseDesk$MAPI_AVD_VideoStandardType;

    move-result-object v2

    const-string v3, "LastVideoStandardMode"

    invoke-interface {v13, v3}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v3

    invoke-interface {v13, v3}, Landroid/database/Cursor;->getInt(I)I

    move-result v3

    aget-object v2, v2, v3

    iput-object v2, v1, Lcom/android/server/tv/DataBaseDesk$T_MS_VIDEO;->LastVideoStandardMode:Lcom/android/server/tv/DataBaseDesk$MAPI_AVD_VideoStandardType;

    move-object/from16 v0, p0

    iget-object v1, v0, Lcom/android/server/tv/DataBaseDeskImpl;->videopara:Lcom/android/server/tv/DataBaseDesk$T_MS_VIDEO;

    invoke-static {}, Lcom/android/server/tv/DataBaseDesk$AUDIOMODE_TYPE_;->values()[Lcom/android/server/tv/DataBaseDesk$AUDIOMODE_TYPE_;

    move-result-object v2

    const-string v3, "LastAudioStandardMode"

    invoke-interface {v13, v3}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v3

    invoke-interface {v13, v3}, Landroid/database/Cursor;->getInt(I)I

    move-result v3

    aget-object v2, v2, v3

    iput-object v2, v1, Lcom/android/server/tv/DataBaseDesk$T_MS_VIDEO;->LastAudioStandardMode:Lcom/android/server/tv/DataBaseDesk$AUDIOMODE_TYPE_;

    move-object/from16 v0, p0

    iget-object v1, v0, Lcom/android/server/tv/DataBaseDeskImpl;->videopara:Lcom/android/server/tv/DataBaseDesk$T_MS_VIDEO;

    invoke-static {}, Lcom/android/server/tv/DataBaseDesk$EN_MS_Dynamic_Contrast;->values()[Lcom/android/server/tv/DataBaseDesk$EN_MS_Dynamic_Contrast;

    move-result-object v2

    const-string v3, "eDynamic_Contrast"

    invoke-interface {v13, v3}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v3

    invoke-interface {v13, v3}, Landroid/database/Cursor;->getInt(I)I

    move-result v3

    aget-object v2, v2, v3

    iput-object v2, v1, Lcom/android/server/tv/DataBaseDesk$T_MS_VIDEO;->eDynamic_Contrast:Lcom/android/server/tv/DataBaseDesk$EN_MS_Dynamic_Contrast;

    move-object/from16 v0, p0

    iget-object v1, v0, Lcom/android/server/tv/DataBaseDeskImpl;->videopara:Lcom/android/server/tv/DataBaseDesk$T_MS_VIDEO;

    invoke-static {}, Lcom/android/server/tv/DataBaseDesk$EN_MS_FILM;->values()[Lcom/android/server/tv/DataBaseDesk$EN_MS_FILM;

    move-result-object v2

    const-string v3, "eFilm"

    invoke-interface {v13, v3}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v3

    invoke-interface {v13, v3}, Landroid/database/Cursor;->getInt(I)I

    move-result v3

    aget-object v2, v2, v3

    iput-object v2, v1, Lcom/android/server/tv/DataBaseDesk$T_MS_VIDEO;->eFilm:Lcom/android/server/tv/DataBaseDesk$EN_MS_FILM;

    move-object/from16 v0, p0

    iget-object v1, v0, Lcom/android/server/tv/DataBaseDeskImpl;->videopara:Lcom/android/server/tv/DataBaseDesk$T_MS_VIDEO;

    invoke-static {}, Lcom/android/server/tv/DataBaseDesk$EN_DISPLAY_TVFORMAT;->values()[Lcom/android/server/tv/DataBaseDesk$EN_DISPLAY_TVFORMAT;

    move-result-object v2

    const-string v3, "eTvFormat"

    invoke-interface {v13, v3}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v3

    invoke-interface {v13, v3}, Landroid/database/Cursor;->getInt(I)I

    move-result v3

    aget-object v2, v2, v3

    iput-object v2, v1, Lcom/android/server/tv/DataBaseDesk$T_MS_VIDEO;->eTvFormat:Lcom/android/server/tv/DataBaseDesk$EN_DISPLAY_TVFORMAT;

    move-object/from16 v0, p0

    iget-object v1, v0, Lcom/android/server/tv/DataBaseDeskImpl;->videopara:Lcom/android/server/tv/DataBaseDesk$T_MS_VIDEO;

    iget-object v1, v1, Lcom/android/server/tv/DataBaseDesk$T_MS_VIDEO;->g_astSubColor:Lcom/android/server/tv/DataBaseDesk$T_MS_SUB_COLOR;

    const-string v2, "u8SubBrightness"

    invoke-interface {v13, v2}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v2

    invoke-interface {v13, v2}, Landroid/database/Cursor;->getInt(I)I

    move-result v2

    int-to-short v2, v2

    iput-short v2, v1, Lcom/android/server/tv/DataBaseDesk$T_MS_SUB_COLOR;->SubBrightness:S

    move-object/from16 v0, p0

    iget-object v1, v0, Lcom/android/server/tv/DataBaseDeskImpl;->videopara:Lcom/android/server/tv/DataBaseDesk$T_MS_VIDEO;

    iget-object v1, v1, Lcom/android/server/tv/DataBaseDesk$T_MS_VIDEO;->g_astSubColor:Lcom/android/server/tv/DataBaseDesk$T_MS_SUB_COLOR;

    const-string v2, "u8SubContrast"

    invoke-interface {v13, v2}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v2

    invoke-interface {v13, v2}, Landroid/database/Cursor;->getInt(I)I

    move-result v2

    int-to-short v2, v2

    iput-short v2, v1, Lcom/android/server/tv/DataBaseDesk$T_MS_SUB_COLOR;->SubContrast:S

    goto/16 :goto_0

    :cond_0
    invoke-interface {v13}, Landroid/database/Cursor;->close()V

    invoke-virtual/range {p0 .. p0}, Lcom/android/server/tv/DataBaseDeskImpl;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v1

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/android/server/tv/DataBaseDeskImpl;->userSettingCusSchema:Ljava/lang/String;

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, "/picmode_setting"

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v2}, Landroid/net/Uri;->parse(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v2

    const/4 v3, 0x0

    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    const-string v5, "InputSrcType = "

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    move/from16 v0, p1

    invoke-virtual {v4, v0}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    const/4 v5, 0x0

    const-string v6, "PictureModeType"

    invoke-virtual/range {v1 .. v6}, Landroid/content/ContentResolver;->query(Landroid/net/Uri;[Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;)Landroid/database/Cursor;

    move-result-object v11

    const/16 v16, 0x0

    move-object/from16 v0, p0

    iget-object v1, v0, Lcom/android/server/tv/DataBaseDeskImpl;->videopara:Lcom/android/server/tv/DataBaseDesk$T_MS_VIDEO;

    iget-object v1, v1, Lcom/android/server/tv/DataBaseDesk$T_MS_VIDEO;->astPicture:[Lcom/android/server/tv/DataBaseDesk$T_MS_PICTURE;

    array-length v14, v1

    :goto_1
    invoke-interface {v11}, Landroid/database/Cursor;->moveToNext()Z

    move-result v1

    if-eqz v1, :cond_1

    add-int/lit8 v1, v14, -0x1

    move/from16 v0, v16

    if-le v0, v1, :cond_6

    :cond_1
    invoke-interface {v11}, Landroid/database/Cursor;->close()V

    invoke-virtual/range {p0 .. p0}, Lcom/android/server/tv/DataBaseDeskImpl;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v1

    const-string v2, "content://mstar.tv.usersetting/nrmode"

    invoke-static {v2}, Landroid/net/Uri;->parse(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v2

    const/4 v3, 0x0

    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    const-string v5, "InputSrcType = "

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    move/from16 v0, p1

    invoke-virtual {v4, v0}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    const/4 v5, 0x0

    const-string v6, "NRMode"

    invoke-virtual/range {v1 .. v6}, Landroid/content/ContentResolver;->query(Landroid/net/Uri;[Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;)Landroid/database/Cursor;

    move-result-object v10

    const/4 v7, 0x0

    move-object/from16 v0, p0

    iget-object v1, v0, Lcom/android/server/tv/DataBaseDeskImpl;->videopara:Lcom/android/server/tv/DataBaseDesk$T_MS_VIDEO;

    iget-object v1, v1, Lcom/android/server/tv/DataBaseDesk$T_MS_VIDEO;->eNRMode:[Lcom/android/server/tv/DataBaseDesk$T_MS_NR_MODE;

    array-length v15, v1

    :goto_2
    invoke-interface {v10}, Landroid/database/Cursor;->moveToNext()Z

    move-result v1

    if-eqz v1, :cond_2

    add-int/lit8 v1, v15, -0x1

    if-le v7, v1, :cond_7

    :cond_2
    invoke-interface {v10}, Landroid/database/Cursor;->close()V

    invoke-virtual/range {p0 .. p0}, Lcom/android/server/tv/DataBaseDeskImpl;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v1

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "content://mstar.tv.usersetting/threedvideomode/inputsrc/"

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    move/from16 v0, p1

    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v2}, Landroid/net/Uri;->parse(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v2

    const/4 v3, 0x0

    const/4 v4, 0x0

    const/4 v5, 0x0

    const/4 v6, 0x0

    invoke-virtual/range {v1 .. v6}, Landroid/content/ContentResolver;->query(Landroid/net/Uri;[Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;)Landroid/database/Cursor;

    move-result-object v8

    invoke-interface {v8}, Landroid/database/Cursor;->moveToFirst()Z

    move-result v1

    if-eqz v1, :cond_3

    move-object/from16 v0, p0

    iget-object v1, v0, Lcom/android/server/tv/DataBaseDeskImpl;->videopara:Lcom/android/server/tv/DataBaseDesk$T_MS_VIDEO;

    iget-object v1, v1, Lcom/android/server/tv/DataBaseDesk$T_MS_VIDEO;->ThreeDVideoMode:Lcom/android/server/tv/DataBaseDesk$ThreeD_Video_MODE;

    invoke-static {}, Lcom/android/server/tv/DataBaseDesk$EN_ThreeD_Video;->values()[Lcom/android/server/tv/DataBaseDesk$EN_ThreeD_Video;

    move-result-object v2

    const-string v3, "eThreeDVideo"

    invoke-interface {v8, v3}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v3

    invoke-interface {v8, v3}, Landroid/database/Cursor;->getInt(I)I

    move-result v3

    aget-object v2, v2, v3

    iput-object v2, v1, Lcom/android/server/tv/DataBaseDesk$ThreeD_Video_MODE;->eThreeDVideo:Lcom/android/server/tv/DataBaseDesk$EN_ThreeD_Video;

    move-object/from16 v0, p0

    iget-object v1, v0, Lcom/android/server/tv/DataBaseDeskImpl;->videopara:Lcom/android/server/tv/DataBaseDesk$T_MS_VIDEO;

    iget-object v1, v1, Lcom/android/server/tv/DataBaseDesk$T_MS_VIDEO;->ThreeDVideoMode:Lcom/android/server/tv/DataBaseDesk$ThreeD_Video_MODE;

    invoke-static {}, Lcom/android/server/tv/DataBaseDesk$EN_ThreeD_Video_3DDEPTH;->values()[Lcom/android/server/tv/DataBaseDesk$EN_ThreeD_Video_3DDEPTH;

    move-result-object v2

    const-string v3, "eThreeDVideo3DDepth"

    invoke-interface {v8, v3}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v3

    invoke-interface {v8, v3}, Landroid/database/Cursor;->getInt(I)I

    move-result v3

    aget-object v2, v2, v3

    iput-object v2, v1, Lcom/android/server/tv/DataBaseDesk$ThreeD_Video_MODE;->eThreeDVideo3DDepth:Lcom/android/server/tv/DataBaseDesk$EN_ThreeD_Video_3DDEPTH;

    move-object/from16 v0, p0

    iget-object v1, v0, Lcom/android/server/tv/DataBaseDeskImpl;->videopara:Lcom/android/server/tv/DataBaseDesk$T_MS_VIDEO;

    iget-object v1, v1, Lcom/android/server/tv/DataBaseDesk$T_MS_VIDEO;->ThreeDVideoMode:Lcom/android/server/tv/DataBaseDesk$ThreeD_Video_MODE;

    invoke-static {}, Lcom/android/server/tv/DataBaseDesk$EN_ThreeD_Video_3DOFFSET;->values()[Lcom/android/server/tv/DataBaseDesk$EN_ThreeD_Video_3DOFFSET;

    move-result-object v2

    const-string v3, "eThreeDVideo3DOffset"

    invoke-interface {v8, v3}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v3

    invoke-interface {v8, v3}, Landroid/database/Cursor;->getInt(I)I

    move-result v3

    aget-object v2, v2, v3

    iput-object v2, v1, Lcom/android/server/tv/DataBaseDesk$ThreeD_Video_MODE;->eThreeDVideo3DOffset:Lcom/android/server/tv/DataBaseDesk$EN_ThreeD_Video_3DOFFSET;

    move-object/from16 v0, p0

    iget-object v1, v0, Lcom/android/server/tv/DataBaseDeskImpl;->videopara:Lcom/android/server/tv/DataBaseDesk$T_MS_VIDEO;

    iget-object v1, v1, Lcom/android/server/tv/DataBaseDesk$T_MS_VIDEO;->ThreeDVideoMode:Lcom/android/server/tv/DataBaseDesk$ThreeD_Video_MODE;

    invoke-static {}, Lcom/android/server/tv/DataBaseDesk$EN_ThreeD_Video_AUTOSTART;->values()[Lcom/android/server/tv/DataBaseDesk$EN_ThreeD_Video_AUTOSTART;

    move-result-object v2

    const-string v3, "eThreeDVideoAutoStart"

    invoke-interface {v8, v3}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v3

    invoke-interface {v8, v3}, Landroid/database/Cursor;->getInt(I)I

    move-result v3

    aget-object v2, v2, v3

    iput-object v2, v1, Lcom/android/server/tv/DataBaseDesk$ThreeD_Video_MODE;->eThreeDVideoAutoStart:Lcom/android/server/tv/DataBaseDesk$EN_ThreeD_Video_AUTOSTART;

    move-object/from16 v0, p0

    iget-object v1, v0, Lcom/android/server/tv/DataBaseDeskImpl;->videopara:Lcom/android/server/tv/DataBaseDesk$T_MS_VIDEO;

    iget-object v1, v1, Lcom/android/server/tv/DataBaseDesk$T_MS_VIDEO;->ThreeDVideoMode:Lcom/android/server/tv/DataBaseDesk$ThreeD_Video_MODE;

    invoke-static {}, Lcom/android/server/tv/DataBaseDesk$EN_ThreeD_Video_3DOUTPUTASPECT;->values()[Lcom/android/server/tv/DataBaseDesk$EN_ThreeD_Video_3DOUTPUTASPECT;

    move-result-object v2

    const-string v3, "eThreeDVideo3DOutputAspect"

    invoke-interface {v8, v3}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v3

    invoke-interface {v8, v3}, Landroid/database/Cursor;->getInt(I)I

    move-result v3

    aget-object v2, v2, v3

    iput-object v2, v1, Lcom/android/server/tv/DataBaseDesk$ThreeD_Video_MODE;->eThreeDVideo3DOutputAspect:Lcom/android/server/tv/DataBaseDesk$EN_ThreeD_Video_3DOUTPUTASPECT;

    move-object/from16 v0, p0

    iget-object v1, v0, Lcom/android/server/tv/DataBaseDeskImpl;->videopara:Lcom/android/server/tv/DataBaseDesk$T_MS_VIDEO;

    iget-object v1, v1, Lcom/android/server/tv/DataBaseDesk$T_MS_VIDEO;->ThreeDVideoMode:Lcom/android/server/tv/DataBaseDesk$ThreeD_Video_MODE;

    invoke-static {}, Lcom/android/server/tv/DataBaseDesk$EN_ThreeD_Video_LRVIEWSWITCH;->values()[Lcom/android/server/tv/DataBaseDesk$EN_ThreeD_Video_LRVIEWSWITCH;

    move-result-object v2

    const-string v3, "eThreeDVideoLRViewSwitch"

    invoke-interface {v8, v3}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v3

    invoke-interface {v8, v3}, Landroid/database/Cursor;->getInt(I)I

    move-result v3

    aget-object v2, v2, v3

    iput-object v2, v1, Lcom/android/server/tv/DataBaseDesk$ThreeD_Video_MODE;->eThreeDVideoLRViewSwitch:Lcom/android/server/tv/DataBaseDesk$EN_ThreeD_Video_LRVIEWSWITCH;

    :cond_3
    invoke-interface {v8}, Landroid/database/Cursor;->close()V

    invoke-virtual/range {p0 .. p0}, Lcom/android/server/tv/DataBaseDeskImpl;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v1

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "content://mstar.tv.usersetting/threedvideomode/inputsrc/"

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    move/from16 v0, p1

    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v2}, Landroid/net/Uri;->parse(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v2

    const/4 v3, 0x0

    const/4 v4, 0x0

    const/4 v5, 0x0

    const/4 v6, 0x0

    invoke-virtual/range {v1 .. v6}, Landroid/content/ContentResolver;->query(Landroid/net/Uri;[Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;)Landroid/database/Cursor;

    move-result-object v9

    invoke-interface {v9}, Landroid/database/Cursor;->moveToFirst()Z

    move-result v1

    if-eqz v1, :cond_4

    move-object/from16 v0, p0

    iget-object v1, v0, Lcom/android/server/tv/DataBaseDeskImpl;->videopara:Lcom/android/server/tv/DataBaseDesk$T_MS_VIDEO;

    iget-object v1, v1, Lcom/android/server/tv/DataBaseDesk$T_MS_VIDEO;->ThreeDVideoMode:Lcom/android/server/tv/DataBaseDesk$ThreeD_Video_MODE;

    invoke-static {}, Lcom/android/server/tv/DataBaseDesk$EN_ThreeD_Video_SELFADAPTIVE_DETECT;->values()[Lcom/android/server/tv/DataBaseDesk$EN_ThreeD_Video_SELFADAPTIVE_DETECT;

    move-result-object v2

    const-string v3, "eThreeDVideoSelfAdaptiveDetect"

    invoke-interface {v9, v3}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v3

    invoke-interface {v9, v3}, Landroid/database/Cursor;->getInt(I)I

    move-result v3

    aget-object v2, v2, v3

    iput-object v2, v1, Lcom/android/server/tv/DataBaseDesk$ThreeD_Video_MODE;->eThreeDVideoSelfAdaptiveDetect:Lcom/android/server/tv/DataBaseDesk$EN_ThreeD_Video_SELFADAPTIVE_DETECT;

    :cond_4
    invoke-interface {v9}, Landroid/database/Cursor;->close()V

    invoke-virtual/range {p0 .. p0}, Lcom/android/server/tv/DataBaseDeskImpl;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v1

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "content://mstar.tv.usersetting/useroverscanmode/inputsrc/"

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    move/from16 v0, p1

    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v2}, Landroid/net/Uri;->parse(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v2

    const/4 v3, 0x0

    const/4 v4, 0x0

    const/4 v5, 0x0

    const/4 v6, 0x0

    invoke-virtual/range {v1 .. v6}, Landroid/content/ContentResolver;->query(Landroid/net/Uri;[Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;)Landroid/database/Cursor;

    move-result-object v12

    invoke-interface {v12}, Landroid/database/Cursor;->moveToFirst()Z

    move-result v1

    if-eqz v1, :cond_5

    move-object/from16 v0, p0

    iget-object v1, v0, Lcom/android/server/tv/DataBaseDeskImpl;->videopara:Lcom/android/server/tv/DataBaseDesk$T_MS_VIDEO;

    iget-object v1, v1, Lcom/android/server/tv/DataBaseDesk$T_MS_VIDEO;->stUserOverScanMode:Lcom/android/server/tv/DataBaseDesk$T_MS_OVERSCAN_SETTING_USER;

    const-string v2, "OverScanHposition"

    invoke-interface {v12, v2}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v2

    invoke-interface {v12, v2}, Landroid/database/Cursor;->getInt(I)I

    move-result v2

    int-to-short v2, v2

    iput-short v2, v1, Lcom/android/server/tv/DataBaseDesk$T_MS_OVERSCAN_SETTING_USER;->OverScanHposition:S

    move-object/from16 v0, p0

    iget-object v1, v0, Lcom/android/server/tv/DataBaseDeskImpl;->videopara:Lcom/android/server/tv/DataBaseDesk$T_MS_VIDEO;

    iget-object v1, v1, Lcom/android/server/tv/DataBaseDesk$T_MS_VIDEO;->stUserOverScanMode:Lcom/android/server/tv/DataBaseDesk$T_MS_OVERSCAN_SETTING_USER;

    const-string v2, "OverScanVposition"

    invoke-interface {v12, v2}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v2

    invoke-interface {v12, v2}, Landroid/database/Cursor;->getInt(I)I

    move-result v2

    int-to-short v2, v2

    iput-short v2, v1, Lcom/android/server/tv/DataBaseDesk$T_MS_OVERSCAN_SETTING_USER;->OverScanVposition:S

    move-object/from16 v0, p0

    iget-object v1, v0, Lcom/android/server/tv/DataBaseDeskImpl;->videopara:Lcom/android/server/tv/DataBaseDesk$T_MS_VIDEO;

    iget-object v1, v1, Lcom/android/server/tv/DataBaseDesk$T_MS_VIDEO;->stUserOverScanMode:Lcom/android/server/tv/DataBaseDesk$T_MS_OVERSCAN_SETTING_USER;

    const-string v2, "OverScanHRatio"

    invoke-interface {v12, v2}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v2

    invoke-interface {v12, v2}, Landroid/database/Cursor;->getInt(I)I

    move-result v2

    int-to-short v2, v2

    iput-short v2, v1, Lcom/android/server/tv/DataBaseDesk$T_MS_OVERSCAN_SETTING_USER;->OverScanHRatio:S

    move-object/from16 v0, p0

    iget-object v1, v0, Lcom/android/server/tv/DataBaseDeskImpl;->videopara:Lcom/android/server/tv/DataBaseDesk$T_MS_VIDEO;

    iget-object v1, v1, Lcom/android/server/tv/DataBaseDesk$T_MS_VIDEO;->stUserOverScanMode:Lcom/android/server/tv/DataBaseDesk$T_MS_OVERSCAN_SETTING_USER;

    const-string v2, "OverScanVRatio"

    invoke-interface {v12, v2}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v2

    invoke-interface {v12, v2}, Landroid/database/Cursor;->getInt(I)I

    move-result v2

    int-to-short v2, v2

    iput-short v2, v1, Lcom/android/server/tv/DataBaseDesk$T_MS_OVERSCAN_SETTING_USER;->OverScanVRatio:S

    :cond_5
    invoke-interface {v12}, Landroid/database/Cursor;->close()V

    move-object/from16 v0, p0

    iget-object v1, v0, Lcom/android/server/tv/DataBaseDeskImpl;->videopara:Lcom/android/server/tv/DataBaseDesk$T_MS_VIDEO;

    return-object v1

    :cond_6
    move-object/from16 v0, p0

    iget-object v1, v0, Lcom/android/server/tv/DataBaseDeskImpl;->videopara:Lcom/android/server/tv/DataBaseDesk$T_MS_VIDEO;

    iget-object v1, v1, Lcom/android/server/tv/DataBaseDesk$T_MS_VIDEO;->astPicture:[Lcom/android/server/tv/DataBaseDesk$T_MS_PICTURE;

    aget-object v1, v1, v16

    const-string v2, "u8Backlight"

    invoke-interface {v11, v2}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v2

    invoke-interface {v11, v2}, Landroid/database/Cursor;->getInt(I)I

    move-result v2

    int-to-short v2, v2

    iput-short v2, v1, Lcom/android/server/tv/DataBaseDesk$T_MS_PICTURE;->backlight:S

    move-object/from16 v0, p0

    iget-object v1, v0, Lcom/android/server/tv/DataBaseDeskImpl;->videopara:Lcom/android/server/tv/DataBaseDesk$T_MS_VIDEO;

    iget-object v1, v1, Lcom/android/server/tv/DataBaseDesk$T_MS_VIDEO;->astPicture:[Lcom/android/server/tv/DataBaseDesk$T_MS_PICTURE;

    aget-object v1, v1, v16

    const-string v2, "u8Contrast"

    invoke-interface {v11, v2}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v2

    invoke-interface {v11, v2}, Landroid/database/Cursor;->getInt(I)I

    move-result v2

    int-to-short v2, v2

    iput-short v2, v1, Lcom/android/server/tv/DataBaseDesk$T_MS_PICTURE;->contrast:S

    move-object/from16 v0, p0

    iget-object v1, v0, Lcom/android/server/tv/DataBaseDeskImpl;->videopara:Lcom/android/server/tv/DataBaseDesk$T_MS_VIDEO;

    iget-object v1, v1, Lcom/android/server/tv/DataBaseDesk$T_MS_VIDEO;->astPicture:[Lcom/android/server/tv/DataBaseDesk$T_MS_PICTURE;

    aget-object v1, v1, v16

    const-string v2, "u8Brightness"

    invoke-interface {v11, v2}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v2

    invoke-interface {v11, v2}, Landroid/database/Cursor;->getInt(I)I

    move-result v2

    int-to-short v2, v2

    iput-short v2, v1, Lcom/android/server/tv/DataBaseDesk$T_MS_PICTURE;->brightness:S

    move-object/from16 v0, p0

    iget-object v1, v0, Lcom/android/server/tv/DataBaseDeskImpl;->videopara:Lcom/android/server/tv/DataBaseDesk$T_MS_VIDEO;

    iget-object v1, v1, Lcom/android/server/tv/DataBaseDesk$T_MS_VIDEO;->astPicture:[Lcom/android/server/tv/DataBaseDesk$T_MS_PICTURE;

    aget-object v1, v1, v16

    const-string v2, "u8Saturation"

    invoke-interface {v11, v2}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v2

    invoke-interface {v11, v2}, Landroid/database/Cursor;->getInt(I)I

    move-result v2

    int-to-short v2, v2

    iput-short v2, v1, Lcom/android/server/tv/DataBaseDesk$T_MS_PICTURE;->saturation:S

    move-object/from16 v0, p0

    iget-object v1, v0, Lcom/android/server/tv/DataBaseDeskImpl;->videopara:Lcom/android/server/tv/DataBaseDesk$T_MS_VIDEO;

    iget-object v1, v1, Lcom/android/server/tv/DataBaseDesk$T_MS_VIDEO;->astPicture:[Lcom/android/server/tv/DataBaseDesk$T_MS_PICTURE;

    aget-object v1, v1, v16

    const-string v2, "u8Sharpness"

    invoke-interface {v11, v2}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v2

    invoke-interface {v11, v2}, Landroid/database/Cursor;->getInt(I)I

    move-result v2

    int-to-short v2, v2

    iput-short v2, v1, Lcom/android/server/tv/DataBaseDesk$T_MS_PICTURE;->sharpness:S

    move-object/from16 v0, p0

    iget-object v1, v0, Lcom/android/server/tv/DataBaseDeskImpl;->videopara:Lcom/android/server/tv/DataBaseDesk$T_MS_VIDEO;

    iget-object v1, v1, Lcom/android/server/tv/DataBaseDesk$T_MS_VIDEO;->astPicture:[Lcom/android/server/tv/DataBaseDesk$T_MS_PICTURE;

    aget-object v1, v1, v16

    const-string v2, "u8Hue"

    invoke-interface {v11, v2}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v2

    invoke-interface {v11, v2}, Landroid/database/Cursor;->getInt(I)I

    move-result v2

    int-to-short v2, v2

    iput-short v2, v1, Lcom/android/server/tv/DataBaseDesk$T_MS_PICTURE;->hue:S

    move-object/from16 v0, p0

    iget-object v1, v0, Lcom/android/server/tv/DataBaseDeskImpl;->videopara:Lcom/android/server/tv/DataBaseDesk$T_MS_VIDEO;

    iget-object v1, v1, Lcom/android/server/tv/DataBaseDesk$T_MS_VIDEO;->astPicture:[Lcom/android/server/tv/DataBaseDesk$T_MS_PICTURE;

    aget-object v1, v1, v16

    invoke-static {}, Lcom/android/server/tv/DataBaseDesk$EN_MS_COLOR_TEMP;->values()[Lcom/android/server/tv/DataBaseDesk$EN_MS_COLOR_TEMP;

    move-result-object v2

    const-string v3, "eColorTemp"

    invoke-interface {v11, v3}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v3

    invoke-interface {v11, v3}, Landroid/database/Cursor;->getInt(I)I

    move-result v3

    aget-object v2, v2, v3

    iput-object v2, v1, Lcom/android/server/tv/DataBaseDesk$T_MS_PICTURE;->eColorTemp:Lcom/android/server/tv/DataBaseDesk$EN_MS_COLOR_TEMP;

    move-object/from16 v0, p0

    iget-object v1, v0, Lcom/android/server/tv/DataBaseDeskImpl;->videopara:Lcom/android/server/tv/DataBaseDesk$T_MS_VIDEO;

    iget-object v1, v1, Lcom/android/server/tv/DataBaseDesk$T_MS_VIDEO;->astPicture:[Lcom/android/server/tv/DataBaseDesk$T_MS_PICTURE;

    aget-object v1, v1, v16

    invoke-static {}, Lcom/android/server/tv/DataBaseDesk$EN_MS_PIC_ADV;->values()[Lcom/android/server/tv/DataBaseDesk$EN_MS_PIC_ADV;

    move-result-object v2

    const-string v3, "eVibrantColour"

    invoke-interface {v11, v3}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v3

    invoke-interface {v11, v3}, Landroid/database/Cursor;->getInt(I)I

    move-result v3

    aget-object v2, v2, v3

    iput-object v2, v1, Lcom/android/server/tv/DataBaseDesk$T_MS_PICTURE;->eVibrantColour:Lcom/android/server/tv/DataBaseDesk$EN_MS_PIC_ADV;

    move-object/from16 v0, p0

    iget-object v1, v0, Lcom/android/server/tv/DataBaseDeskImpl;->videopara:Lcom/android/server/tv/DataBaseDesk$T_MS_VIDEO;

    iget-object v1, v1, Lcom/android/server/tv/DataBaseDesk$T_MS_VIDEO;->astPicture:[Lcom/android/server/tv/DataBaseDesk$T_MS_PICTURE;

    aget-object v1, v1, v16

    invoke-static {}, Lcom/android/server/tv/DataBaseDesk$EN_MS_PIC_ADV;->values()[Lcom/android/server/tv/DataBaseDesk$EN_MS_PIC_ADV;

    move-result-object v2

    const-string v3, "ePerfectClear"

    invoke-interface {v11, v3}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v3

    invoke-interface {v11, v3}, Landroid/database/Cursor;->getInt(I)I

    move-result v3

    aget-object v2, v2, v3

    iput-object v2, v1, Lcom/android/server/tv/DataBaseDesk$T_MS_PICTURE;->ePerfectClear:Lcom/android/server/tv/DataBaseDesk$EN_MS_PIC_ADV;

    move-object/from16 v0, p0

    iget-object v1, v0, Lcom/android/server/tv/DataBaseDeskImpl;->videopara:Lcom/android/server/tv/DataBaseDesk$T_MS_VIDEO;

    iget-object v1, v1, Lcom/android/server/tv/DataBaseDesk$T_MS_VIDEO;->astPicture:[Lcom/android/server/tv/DataBaseDesk$T_MS_PICTURE;

    aget-object v1, v1, v16

    invoke-static {}, Lcom/android/server/tv/DataBaseDesk$EN_MS_PIC_ADV;->values()[Lcom/android/server/tv/DataBaseDesk$EN_MS_PIC_ADV;

    move-result-object v2

    const-string v3, "eDynamicContrast"

    invoke-interface {v11, v3}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v3

    invoke-interface {v11, v3}, Landroid/database/Cursor;->getInt(I)I

    move-result v3

    aget-object v2, v2, v3

    iput-object v2, v1, Lcom/android/server/tv/DataBaseDesk$T_MS_PICTURE;->eDynamicContrast:Lcom/android/server/tv/DataBaseDesk$EN_MS_PIC_ADV;

    move-object/from16 v0, p0

    iget-object v1, v0, Lcom/android/server/tv/DataBaseDeskImpl;->videopara:Lcom/android/server/tv/DataBaseDesk$T_MS_VIDEO;

    iget-object v1, v1, Lcom/android/server/tv/DataBaseDesk$T_MS_VIDEO;->astPicture:[Lcom/android/server/tv/DataBaseDesk$T_MS_PICTURE;

    aget-object v1, v1, v16

    invoke-static {}, Lcom/android/server/tv/DataBaseDesk$EN_MS_PIC_ADV;->values()[Lcom/android/server/tv/DataBaseDesk$EN_MS_PIC_ADV;

    move-result-object v2

    const-string v3, "eDynamicBacklight"

    invoke-interface {v11, v3}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v3

    invoke-interface {v11, v3}, Landroid/database/Cursor;->getInt(I)I

    move-result v3

    aget-object v2, v2, v3

    iput-object v2, v1, Lcom/android/server/tv/DataBaseDesk$T_MS_PICTURE;->eDynamicBacklight:Lcom/android/server/tv/DataBaseDesk$EN_MS_PIC_ADV;

    add-int/lit8 v16, v16, 0x1

    goto/16 :goto_1

    :cond_7
    move-object/from16 v0, p0

    iget-object v1, v0, Lcom/android/server/tv/DataBaseDeskImpl;->videopara:Lcom/android/server/tv/DataBaseDesk$T_MS_VIDEO;

    iget-object v1, v1, Lcom/android/server/tv/DataBaseDesk$T_MS_VIDEO;->eNRMode:[Lcom/android/server/tv/DataBaseDesk$T_MS_NR_MODE;

    aget-object v1, v1, v7

    invoke-static {}, Lcom/android/server/tv/DataBaseDesk$EN_MS_NR;->values()[Lcom/android/server/tv/DataBaseDesk$EN_MS_NR;

    move-result-object v2

    const-string v3, "eNR"

    invoke-interface {v10, v3}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v3

    invoke-interface {v10, v3}, Landroid/database/Cursor;->getInt(I)I

    move-result v3

    aget-object v2, v2, v3

    iput-object v2, v1, Lcom/android/server/tv/DataBaseDesk$T_MS_NR_MODE;->eNR:Lcom/android/server/tv/DataBaseDesk$EN_MS_NR;

    move-object/from16 v0, p0

    iget-object v1, v0, Lcom/android/server/tv/DataBaseDeskImpl;->videopara:Lcom/android/server/tv/DataBaseDesk$T_MS_VIDEO;

    iget-object v1, v1, Lcom/android/server/tv/DataBaseDesk$T_MS_VIDEO;->eNRMode:[Lcom/android/server/tv/DataBaseDesk$T_MS_NR_MODE;

    aget-object v1, v1, v7

    invoke-static {}, Lcom/android/server/tv/DataBaseDesk$EN_MS_MPEG_NR;->values()[Lcom/android/server/tv/DataBaseDesk$EN_MS_MPEG_NR;

    move-result-object v2

    const-string v3, "eMPEG_NR"

    invoke-interface {v10, v3}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v3

    invoke-interface {v10, v3}, Landroid/database/Cursor;->getInt(I)I

    move-result v3

    aget-object v2, v2, v3

    iput-object v2, v1, Lcom/android/server/tv/DataBaseDesk$T_MS_NR_MODE;->eMPEG_NR:Lcom/android/server/tv/DataBaseDesk$EN_MS_MPEG_NR;

    add-int/lit8 v7, v7, 0x1

    goto/16 :goto_2
.end method

.method public queryAntennaType()I
    .locals 8

    const/4 v2, 0x0

    invoke-virtual {p0}, Lcom/android/server/tv/DataBaseDeskImpl;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v0

    const-string v1, "content://mstar.tv.usersetting/mediumsetting/"

    invoke-static {v1}, Landroid/net/Uri;->parse(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v1

    move-object v3, v2

    move-object v4, v2

    move-object v5, v2

    invoke-virtual/range {v0 .. v5}, Landroid/content/ContentResolver;->query(Landroid/net/Uri;[Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;)Landroid/database/Cursor;

    move-result-object v7

    invoke-interface {v7}, Landroid/database/Cursor;->moveToFirst()Z

    const/4 v6, 0x0

    const-string v0, "AntennaType"

    invoke-interface {v7, v0}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v0

    invoke-interface {v7, v0}, Landroid/database/Cursor;->getInt(I)I

    move-result v6

    invoke-interface {v7}, Landroid/database/Cursor;->close()V

    return v6
.end method

.method public queryArcMode(I)I
    .locals 8
    .param p1    # I

    const/4 v2, 0x0

    invoke-virtual {p0}, Lcom/android/server/tv/DataBaseDeskImpl;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v0

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "content://mstar.tv.usersetting/videosetting/inputsrc/"

    invoke-virtual {v1, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v1}, Landroid/net/Uri;->parse(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v1

    move-object v3, v2

    move-object v4, v2

    move-object v5, v2

    invoke-virtual/range {v0 .. v5}, Landroid/content/ContentResolver;->query(Landroid/net/Uri;[Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;)Landroid/database/Cursor;

    move-result-object v6

    const/4 v7, 0x0

    invoke-interface {v6}, Landroid/database/Cursor;->moveToFirst()Z

    move-result v0

    if-eqz v0, :cond_0

    const-string v0, "enARCType"

    invoke-interface {v6, v0}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v0

    invoke-interface {v6, v0}, Landroid/database/Cursor;->getInt(I)I

    move-result v7

    :cond_0
    invoke-interface {v6}, Landroid/database/Cursor;->close()V

    return v7
.end method

.method public queryAudioPrescale()I
    .locals 14

    const/4 v2, 0x0

    new-instance v7, Ljava/lang/String;

    const-string v0, "0x0,0x0,0x0,0x0,0x0,0x0,0x0,0x0,0x0,0x0,0x0,0x0,0x0,0x0,0x0,0x0, 0x0,0x0,0x0,0x0,0x0,0x0,0x0,0x0,0x0,0x0,0x0,0x0,0x0,0x0,0x0,0x0, 0x0,0x0,0x0,0x0,0x0,0x0,"

    invoke-direct {v7, v0}, Ljava/lang/String;-><init>(Ljava/lang/String;)V

    const/4 v13, 0x0

    invoke-virtual {p0}, Lcom/android/server/tv/DataBaseDeskImpl;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v0

    const-string v1, "content://mstar.tv.usersetting/soundsetting"

    invoke-static {v1}, Landroid/net/Uri;->parse(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v1

    move-object v3, v2

    move-object v4, v2

    move-object v5, v2

    invoke-virtual/range {v0 .. v5}, Landroid/content/ContentResolver;->query(Landroid/net/Uri;[Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;)Landroid/database/Cursor;

    move-result-object v8

    invoke-interface {v8}, Landroid/database/Cursor;->moveToFirst()Z

    move-result v0

    if-eqz v0, :cond_0

    const-string v0, "SpeakerPreScale"

    invoke-interface {v8, v0}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v0

    invoke-interface {v8, v0}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v7

    const-string v0, ","

    invoke-virtual {v7, v0}, Ljava/lang/String;->split(Ljava/lang/String;)[Ljava/lang/String;

    move-result-object v6

    invoke-virtual {p0}, Lcom/android/server/tv/DataBaseDeskImpl;->queryCurInputSrc()I

    move-result v9

    aget-object v11, v6, v9

    const/4 v0, 0x2

    invoke-virtual {v11, v0}, Ljava/lang/String;->substring(I)Ljava/lang/String;

    move-result-object v12

    invoke-static {v12}, Ljava/lang/Integer;->parseInt(Ljava/lang/String;)I

    move-result v10

    div-int/lit8 v0, v10, 0xa

    mul-int/lit8 v0, v0, 0x10

    rem-int/lit8 v1, v10, 0xa

    add-int v13, v0, v1

    :cond_0
    invoke-interface {v8}, Landroid/database/Cursor;->close()V

    return v13
.end method

.method public queryAvc()I
    .locals 8

    const/4 v2, 0x0

    const/4 v7, 0x0

    invoke-virtual {p0}, Lcom/android/server/tv/DataBaseDeskImpl;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v0

    const-string v1, "content://mstar.tv.usersetting/soundsetting"

    invoke-static {v1}, Landroid/net/Uri;->parse(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v1

    move-object v3, v2

    move-object v4, v2

    move-object v5, v2

    invoke-virtual/range {v0 .. v5}, Landroid/content/ContentResolver;->query(Landroid/net/Uri;[Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;)Landroid/database/Cursor;

    move-result-object v6

    :goto_0
    invoke-interface {v6}, Landroid/database/Cursor;->moveToNext()Z

    move-result v0

    if-eqz v0, :cond_0

    const-string v0, "bEnableAVC"

    invoke-interface {v6, v0}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v0

    invoke-interface {v6, v0}, Landroid/database/Cursor;->getInt(I)I

    move-result v7

    goto :goto_0

    :cond_0
    invoke-interface {v6}, Landroid/database/Cursor;->close()V

    return v7
.end method

.method public queryBalance()I
    .locals 8

    const/4 v2, 0x0

    const/4 v7, 0x0

    invoke-virtual {p0}, Lcom/android/server/tv/DataBaseDeskImpl;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v0

    const-string v1, "content://mstar.tv.usersetting/soundsetting"

    invoke-static {v1}, Landroid/net/Uri;->parse(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v1

    move-object v3, v2

    move-object v4, v2

    move-object v5, v2

    invoke-virtual/range {v0 .. v5}, Landroid/content/ContentResolver;->query(Landroid/net/Uri;[Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;)Landroid/database/Cursor;

    move-result-object v6

    :goto_0
    invoke-interface {v6}, Landroid/database/Cursor;->moveToNext()Z

    move-result v0

    if-eqz v0, :cond_0

    const-string v0, "Balance"

    invoke-interface {v6, v0}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v0

    invoke-interface {v6, v0}, Landroid/database/Cursor;->getInt(I)I

    move-result v7

    goto :goto_0

    :cond_0
    invoke-interface {v6}, Landroid/database/Cursor;->close()V

    return v7
.end method

.method public queryBass(I)I
    .locals 8
    .param p1    # I

    const/4 v2, 0x0

    const/4 v7, 0x0

    invoke-virtual {p0}, Lcom/android/server/tv/DataBaseDeskImpl;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v0

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    iget-object v3, p0, Lcom/android/server/tv/DataBaseDeskImpl;->userSettingCusSchema:Ljava/lang/String;

    invoke-virtual {v1, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v3, "/soundmodesetting/"

    invoke-virtual {v1, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v1}, Landroid/net/Uri;->parse(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v1

    move-object v3, v2

    move-object v4, v2

    move-object v5, v2

    invoke-virtual/range {v0 .. v5}, Landroid/content/ContentResolver;->query(Landroid/net/Uri;[Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;)Landroid/database/Cursor;

    move-result-object v6

    :goto_0
    invoke-interface {v6}, Landroid/database/Cursor;->moveToNext()Z

    move-result v0

    if-eqz v0, :cond_0

    const-string v0, "Bass"

    invoke-interface {v6, v0}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v0

    invoke-interface {v6, v0}, Landroid/database/Cursor;->getInt(I)I

    move-result v7

    goto :goto_0

    :cond_0
    invoke-interface {v6}, Landroid/database/Cursor;->close()V

    return v7
.end method

.method public queryBassSwitch()I
    .locals 8

    const/4 v2, 0x0

    const/4 v7, 0x0

    invoke-virtual {p0}, Lcom/android/server/tv/DataBaseDeskImpl;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v0

    const-string v1, "content://mstar.tv.usersetting/soundsetting"

    invoke-static {v1}, Landroid/net/Uri;->parse(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v1

    move-object v3, v2

    move-object v4, v2

    move-object v5, v2

    invoke-virtual/range {v0 .. v5}, Landroid/content/ContentResolver;->query(Landroid/net/Uri;[Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;)Landroid/database/Cursor;

    move-result-object v6

    invoke-interface {v6}, Landroid/database/Cursor;->moveToFirst()Z

    move-result v0

    if-eqz v0, :cond_0

    const-string v0, "bEnableHeavyBass"

    invoke-interface {v6, v0}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v0

    invoke-interface {v6, v0}, Landroid/database/Cursor;->getInt(I)I

    move-result v7

    :cond_0
    invoke-interface {v6}, Landroid/database/Cursor;->close()V

    return v7
.end method

.method public queryBassVolume()I
    .locals 8

    const/4 v2, 0x0

    const/4 v7, 0x0

    invoke-virtual {p0}, Lcom/android/server/tv/DataBaseDeskImpl;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v0

    const-string v1, "content://mstar.tv.usersetting/soundsetting"

    invoke-static {v1}, Landroid/net/Uri;->parse(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v1

    move-object v3, v2

    move-object v4, v2

    move-object v5, v2

    invoke-virtual/range {v0 .. v5}, Landroid/content/ContentResolver;->query(Landroid/net/Uri;[Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;)Landroid/database/Cursor;

    move-result-object v6

    invoke-interface {v6}, Landroid/database/Cursor;->moveToFirst()Z

    move-result v0

    if-eqz v0, :cond_0

    const-string v0, "HeavyBassVolume"

    invoke-interface {v6, v0}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v0

    invoke-interface {v6, v0}, Landroid/database/Cursor;->getInt(I)I

    move-result v7

    :cond_0
    invoke-interface {v6}, Landroid/database/Cursor;->close()V

    return v7
.end method

.method public queryChSwMode()Lcom/android/server/tv/DataBaseDesk$EN_MS_CHANNEL_SWITCH_MODE;
    .locals 7

    const/4 v2, 0x0

    invoke-virtual {p0}, Lcom/android/server/tv/DataBaseDeskImpl;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v0

    const-string v1, "content://mstar.tv.usersetting/systemsetting"

    invoke-static {v1}, Landroid/net/Uri;->parse(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v1

    move-object v3, v2

    move-object v4, v2

    move-object v5, v2

    invoke-virtual/range {v0 .. v5}, Landroid/content/ContentResolver;->query(Landroid/net/Uri;[Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;)Landroid/database/Cursor;

    move-result-object v6

    invoke-interface {v6}, Landroid/database/Cursor;->moveToFirst()Z

    move-result v0

    if-eqz v0, :cond_0

    iget-object v1, p0, Lcom/android/server/tv/DataBaseDeskImpl;->stUsrData:Lcom/android/server/tv/DataBaseDesk$MS_USER_SYSTEM_SETTING;

    const-string v0, "bATVChSwitchFreeze"

    invoke-interface {v6, v0}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v0

    invoke-interface {v6, v0}, Landroid/database/Cursor;->getInt(I)I

    move-result v0

    if-nez v0, :cond_1

    sget-object v0, Lcom/android/server/tv/DataBaseDesk$EN_MS_CHANNEL_SWITCH_MODE;->MS_CHANNEL_SWM_BLACKSCREEN:Lcom/android/server/tv/DataBaseDesk$EN_MS_CHANNEL_SWITCH_MODE;

    :goto_0
    iput-object v0, v1, Lcom/android/server/tv/DataBaseDesk$MS_USER_SYSTEM_SETTING;->eChSwMode:Lcom/android/server/tv/DataBaseDesk$EN_MS_CHANNEL_SWITCH_MODE;

    :cond_0
    invoke-interface {v6}, Landroid/database/Cursor;->close()V

    iget-object v0, p0, Lcom/android/server/tv/DataBaseDeskImpl;->stUsrData:Lcom/android/server/tv/DataBaseDesk$MS_USER_SYSTEM_SETTING;

    iget-object v0, v0, Lcom/android/server/tv/DataBaseDesk$MS_USER_SYSTEM_SETTING;->eChSwMode:Lcom/android/server/tv/DataBaseDesk$EN_MS_CHANNEL_SWITCH_MODE;

    return-object v0

    :cond_1
    sget-object v0, Lcom/android/server/tv/DataBaseDesk$EN_MS_CHANNEL_SWITCH_MODE;->MS_CHANNEL_SWM_FREEZE:Lcom/android/server/tv/DataBaseDesk$EN_MS_CHANNEL_SWITCH_MODE;

    goto :goto_0
.end method

.method public queryColorTempIdx(II)Lcom/android/server/tv/DataBaseDesk$EN_MS_COLOR_TEMP;
    .locals 8
    .param p1    # I
    .param p2    # I

    const/4 v2, 0x0

    invoke-virtual {p0}, Lcom/android/server/tv/DataBaseDeskImpl;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v0

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    iget-object v3, p0, Lcom/android/server/tv/DataBaseDeskImpl;->userSettingCusSchema:Ljava/lang/String;

    invoke-virtual {v1, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v3, "/picmode_setting/inputsrc/"

    invoke-virtual {v1, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v3, "/picmode/"

    invoke-virtual {v1, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v1}, Landroid/net/Uri;->parse(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v1

    const-string v5, "PictureModeType"

    move-object v3, v2

    move-object v4, v2

    invoke-virtual/range {v0 .. v5}, Landroid/content/ContentResolver;->query(Landroid/net/Uri;[Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;)Landroid/database/Cursor;

    move-result-object v6

    invoke-interface {v6}, Landroid/database/Cursor;->moveToFirst()Z

    invoke-static {}, Lcom/android/server/tv/DataBaseDesk$EN_MS_COLOR_TEMP;->values()[Lcom/android/server/tv/DataBaseDesk$EN_MS_COLOR_TEMP;

    move-result-object v0

    const-string v1, "eColorTemp"

    invoke-interface {v6, v1}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v1

    invoke-interface {v6, v1}, Landroid/database/Cursor;->getInt(I)I

    move-result v1

    aget-object v7, v0, v1

    invoke-interface {v6}, Landroid/database/Cursor;->close()V

    return-object v7
.end method

.method public queryColorTmpIdx(II)I
    .locals 8
    .param p1    # I
    .param p2    # I

    const/4 v2, 0x0

    invoke-virtual {p0}, Lcom/android/server/tv/DataBaseDeskImpl;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v0

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    iget-object v3, p0, Lcom/android/server/tv/DataBaseDeskImpl;->userSettingCusSchema:Ljava/lang/String;

    invoke-virtual {v1, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v3, "/picmode_setting/inputsrc/"

    invoke-virtual {v1, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v3, "/picmode/"

    invoke-virtual {v1, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v1}, Landroid/net/Uri;->parse(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v1

    move-object v3, v2

    move-object v4, v2

    move-object v5, v2

    invoke-virtual/range {v0 .. v5}, Landroid/content/ContentResolver;->query(Landroid/net/Uri;[Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;)Landroid/database/Cursor;

    move-result-object v6

    const/4 v7, -0x1

    invoke-interface {v6}, Landroid/database/Cursor;->moveToNext()Z

    move-result v0

    if-eqz v0, :cond_0

    const-string v0, "eColorTemp"

    invoke-interface {v6, v0}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v0

    invoke-interface {v6, v0}, Landroid/database/Cursor;->getInt(I)I

    move-result v7

    :cond_0
    invoke-interface {v6}, Landroid/database/Cursor;->close()V

    return v7
.end method

.method public queryCountry()I
    .locals 8

    const/4 v2, 0x0

    const/4 v6, 0x0

    invoke-virtual {p0}, Lcom/android/server/tv/DataBaseDeskImpl;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v0

    const-string v1, "content://mstar.tv.usersetting/systemsetting"

    invoke-static {v1}, Landroid/net/Uri;->parse(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v1

    move-object v3, v2

    move-object v4, v2

    move-object v5, v2

    invoke-virtual/range {v0 .. v5}, Landroid/content/ContentResolver;->query(Landroid/net/Uri;[Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;)Landroid/database/Cursor;

    move-result-object v7

    invoke-interface {v7}, Landroid/database/Cursor;->moveToFirst()Z

    move-result v0

    if-eqz v0, :cond_0

    const-string v0, "Country"

    invoke-interface {v7, v0}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v0

    invoke-interface {v7, v0}, Landroid/database/Cursor;->getInt(I)I

    move-result v6

    :cond_0
    invoke-interface {v7}, Landroid/database/Cursor;->close()V

    return v6
.end method

.method public queryCurInputSrc()I
    .locals 8

    const/4 v2, 0x0

    const/4 v7, 0x0

    invoke-virtual {p0}, Lcom/android/server/tv/DataBaseDeskImpl;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v0

    const-string v1, "content://mstar.tv.usersetting/systemsetting"

    invoke-static {v1}, Landroid/net/Uri;->parse(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v1

    move-object v3, v2

    move-object v4, v2

    move-object v5, v2

    invoke-virtual/range {v0 .. v5}, Landroid/content/ContentResolver;->query(Landroid/net/Uri;[Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;)Landroid/database/Cursor;

    move-result-object v6

    invoke-interface {v6}, Landroid/database/Cursor;->moveToFirst()Z

    move-result v0

    if-eqz v0, :cond_0

    const-string v0, "enInputSourceType"

    invoke-interface {v6, v0}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v0

    invoke-interface {v6, v0}, Landroid/database/Cursor;->getInt(I)I

    move-result v7

    :cond_0
    invoke-interface {v6}, Landroid/database/Cursor;->close()V

    return v7
.end method

.method public queryDGClarity()I
    .locals 8

    const/4 v2, 0x0

    const/4 v7, 0x0

    invoke-virtual {p0}, Lcom/android/server/tv/DataBaseDeskImpl;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v0

    const-string v1, "content://mstar.tv.usersetting/soundsetting"

    invoke-static {v1}, Landroid/net/Uri;->parse(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v1

    move-object v3, v2

    move-object v4, v2

    move-object v5, v2

    invoke-virtual/range {v0 .. v5}, Landroid/content/ContentResolver;->query(Landroid/net/Uri;[Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;)Landroid/database/Cursor;

    move-result-object v6

    invoke-interface {v6}, Landroid/database/Cursor;->moveToFirst()Z

    move-result v0

    if-eqz v0, :cond_0

    const-string v0, "DGClarity"

    invoke-interface {v6, v0}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v0

    invoke-interface {v6, v0}, Landroid/database/Cursor;->getInt(I)I

    move-result v7

    :cond_0
    invoke-interface {v6}, Landroid/database/Cursor;->close()V

    return v7
.end method

.method public queryEarPhoneVolme()I
    .locals 8

    const/4 v2, 0x0

    const/4 v7, 0x0

    invoke-virtual {p0}, Lcom/android/server/tv/DataBaseDeskImpl;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v0

    const-string v1, "content://mstar.tv.usersetting/soundsetting"

    invoke-static {v1}, Landroid/net/Uri;->parse(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v1

    move-object v3, v2

    move-object v4, v2

    move-object v5, v2

    invoke-virtual/range {v0 .. v5}, Landroid/content/ContentResolver;->query(Landroid/net/Uri;[Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;)Landroid/database/Cursor;

    move-result-object v6

    invoke-interface {v6}, Landroid/database/Cursor;->moveToFirst()Z

    move-result v0

    if-eqz v0, :cond_0

    const-string v0, "HPVolume"

    invoke-interface {v6, v0}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v0

    invoke-interface {v6, v0}, Landroid/database/Cursor;->getInt(I)I

    move-result v7

    :cond_0
    invoke-interface {v6}, Landroid/database/Cursor;->close()V

    return v7
.end method

.method public queryEventName(S)Ljava/lang/String;
    .locals 8
    .param p1    # S

    const/4 v2, 0x0

    invoke-virtual {p0}, Lcom/android/server/tv/DataBaseDeskImpl;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v0

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "content://mstar.tv.usersetting/epgtimer/"

    invoke-virtual {v1, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v1}, Landroid/net/Uri;->parse(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v1

    move-object v3, v2

    move-object v4, v2

    move-object v5, v2

    invoke-virtual/range {v0 .. v5}, Landroid/content/ContentResolver;->query(Landroid/net/Uri;[Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;)Landroid/database/Cursor;

    move-result-object v6

    invoke-interface {v6}, Landroid/database/Cursor;->moveToFirst()Z

    new-instance v7, Ljava/lang/String;

    invoke-direct {v7}, Ljava/lang/String;-><init>()V

    const-string v0, "sEventName"

    invoke-interface {v6, v0}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v0

    invoke-interface {v6, v0}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v7

    sget-object v0, Ljava/lang/System;->out:Ljava/io/PrintStream;

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "\n=====>>eventName "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, " @epgTimerIndex "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/io/PrintStream;->println(Ljava/lang/String;)V

    invoke-interface {v6}, Landroid/database/Cursor;->close()V

    return-object v7
.end method

.method public queryFactoryColorTempData()Lcom/android/server/tv/DataBaseDesk$T_MS_COLOR_TEMP;
    .locals 9

    const/4 v2, 0x0

    invoke-virtual {p0}, Lcom/android/server/tv/DataBaseDeskImpl;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v0

    const-string v1, "content://mstar.tv.factory/factorycolortemp"

    invoke-static {v1}, Landroid/net/Uri;->parse(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v1

    const-string v5, "ColorTemperatureID"

    move-object v3, v2

    move-object v4, v2

    invoke-virtual/range {v0 .. v5}, Landroid/content/ContentResolver;->query(Landroid/net/Uri;[Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;)Landroid/database/Cursor;

    move-result-object v6

    const/4 v7, 0x0

    iget-object v0, p0, Lcom/android/server/tv/DataBaseDeskImpl;->m_stFactoryColorTemp:Lcom/android/server/tv/DataBaseDesk$T_MS_COLOR_TEMP;

    iget-object v0, v0, Lcom/android/server/tv/DataBaseDesk$T_MS_COLOR_TEMP;->astColorTemp:[Lcom/android/server/tv/DataBaseDesk$T_MS_COLOR_TEMP_DATA;

    array-length v8, v0

    :goto_0
    invoke-interface {v6}, Landroid/database/Cursor;->moveToNext()Z

    move-result v0

    if-eqz v0, :cond_0

    add-int/lit8 v0, v8, -0x1

    if-le v7, v0, :cond_1

    :cond_0
    invoke-interface {v6}, Landroid/database/Cursor;->close()V

    iget-object v0, p0, Lcom/android/server/tv/DataBaseDeskImpl;->m_stFactoryColorTemp:Lcom/android/server/tv/DataBaseDesk$T_MS_COLOR_TEMP;

    return-object v0

    :cond_1
    iget-object v0, p0, Lcom/android/server/tv/DataBaseDeskImpl;->m_stFactoryColorTemp:Lcom/android/server/tv/DataBaseDesk$T_MS_COLOR_TEMP;

    iget-object v0, v0, Lcom/android/server/tv/DataBaseDesk$T_MS_COLOR_TEMP;->astColorTemp:[Lcom/android/server/tv/DataBaseDesk$T_MS_COLOR_TEMP_DATA;

    aget-object v0, v0, v7

    const-string v1, "u8RedGain"

    invoke-interface {v6, v1}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v1

    invoke-interface {v6, v1}, Landroid/database/Cursor;->getInt(I)I

    move-result v1

    int-to-short v1, v1

    iput-short v1, v0, Lcom/android/server/tv/DataBaseDesk$T_MS_COLOR_TEMP_DATA;->redgain:S

    iget-object v0, p0, Lcom/android/server/tv/DataBaseDeskImpl;->m_stFactoryColorTemp:Lcom/android/server/tv/DataBaseDesk$T_MS_COLOR_TEMP;

    iget-object v0, v0, Lcom/android/server/tv/DataBaseDesk$T_MS_COLOR_TEMP;->astColorTemp:[Lcom/android/server/tv/DataBaseDesk$T_MS_COLOR_TEMP_DATA;

    aget-object v0, v0, v7

    const-string v1, "u8GreenGain"

    invoke-interface {v6, v1}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v1

    invoke-interface {v6, v1}, Landroid/database/Cursor;->getInt(I)I

    move-result v1

    int-to-short v1, v1

    iput-short v1, v0, Lcom/android/server/tv/DataBaseDesk$T_MS_COLOR_TEMP_DATA;->greengain:S

    iget-object v0, p0, Lcom/android/server/tv/DataBaseDeskImpl;->m_stFactoryColorTemp:Lcom/android/server/tv/DataBaseDesk$T_MS_COLOR_TEMP;

    iget-object v0, v0, Lcom/android/server/tv/DataBaseDesk$T_MS_COLOR_TEMP;->astColorTemp:[Lcom/android/server/tv/DataBaseDesk$T_MS_COLOR_TEMP_DATA;

    aget-object v0, v0, v7

    const-string v1, "u8BlueGain"

    invoke-interface {v6, v1}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v1

    invoke-interface {v6, v1}, Landroid/database/Cursor;->getInt(I)I

    move-result v1

    int-to-short v1, v1

    iput-short v1, v0, Lcom/android/server/tv/DataBaseDesk$T_MS_COLOR_TEMP_DATA;->bluegain:S

    iget-object v0, p0, Lcom/android/server/tv/DataBaseDeskImpl;->m_stFactoryColorTemp:Lcom/android/server/tv/DataBaseDesk$T_MS_COLOR_TEMP;

    iget-object v0, v0, Lcom/android/server/tv/DataBaseDesk$T_MS_COLOR_TEMP;->astColorTemp:[Lcom/android/server/tv/DataBaseDesk$T_MS_COLOR_TEMP_DATA;

    aget-object v0, v0, v7

    const-string v1, "u8RedOffset"

    invoke-interface {v6, v1}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v1

    invoke-interface {v6, v1}, Landroid/database/Cursor;->getInt(I)I

    move-result v1

    int-to-short v1, v1

    iput-short v1, v0, Lcom/android/server/tv/DataBaseDesk$T_MS_COLOR_TEMP_DATA;->redoffset:S

    iget-object v0, p0, Lcom/android/server/tv/DataBaseDeskImpl;->m_stFactoryColorTemp:Lcom/android/server/tv/DataBaseDesk$T_MS_COLOR_TEMP;

    iget-object v0, v0, Lcom/android/server/tv/DataBaseDesk$T_MS_COLOR_TEMP;->astColorTemp:[Lcom/android/server/tv/DataBaseDesk$T_MS_COLOR_TEMP_DATA;

    aget-object v0, v0, v7

    const-string v1, "u8GreenOffset"

    invoke-interface {v6, v1}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v1

    invoke-interface {v6, v1}, Landroid/database/Cursor;->getInt(I)I

    move-result v1

    int-to-short v1, v1

    iput-short v1, v0, Lcom/android/server/tv/DataBaseDesk$T_MS_COLOR_TEMP_DATA;->greenoffset:S

    iget-object v0, p0, Lcom/android/server/tv/DataBaseDeskImpl;->m_stFactoryColorTemp:Lcom/android/server/tv/DataBaseDesk$T_MS_COLOR_TEMP;

    iget-object v0, v0, Lcom/android/server/tv/DataBaseDesk$T_MS_COLOR_TEMP;->astColorTemp:[Lcom/android/server/tv/DataBaseDesk$T_MS_COLOR_TEMP_DATA;

    aget-object v0, v0, v7

    const-string v1, "u8BlueOffset"

    invoke-interface {v6, v1}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v1

    invoke-interface {v6, v1}, Landroid/database/Cursor;->getInt(I)I

    move-result v1

    int-to-short v1, v1

    iput-short v1, v0, Lcom/android/server/tv/DataBaseDesk$T_MS_COLOR_TEMP_DATA;->blueoffset:S

    add-int/lit8 v7, v7, 0x1

    goto/16 :goto_0
.end method

.method public queryFactoryColorTempData(I)Lcom/android/server/tv/DataBaseDesk$T_MS_COLOR_TEMP_DATA;
    .locals 9
    .param p1    # I

    const/4 v2, 0x0

    const/4 v8, 0x0

    invoke-virtual {p0}, Lcom/android/server/tv/DataBaseDeskImpl;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v0

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "content://mstar.tv.factory/factorycolortemp/colortemperatureid/"

    invoke-virtual {v1, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v1}, Landroid/net/Uri;->parse(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v1

    move-object v3, v2

    move-object v4, v2

    move-object v5, v2

    invoke-virtual/range {v0 .. v5}, Landroid/content/ContentResolver;->query(Landroid/net/Uri;[Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;)Landroid/database/Cursor;

    move-result-object v7

    new-instance v0, Lcom/android/server/tv/DataBaseDesk$T_MS_COLOR_TEMP_DATA;

    move v1, v8

    move v2, v8

    move v3, v8

    move v4, v8

    move v5, v8

    move v6, v8

    invoke-direct/range {v0 .. v6}, Lcom/android/server/tv/DataBaseDesk$T_MS_COLOR_TEMP_DATA;-><init>(SSSSSS)V

    invoke-interface {v7}, Landroid/database/Cursor;->moveToNext()Z

    move-result v1

    if-eqz v1, :cond_0

    const-string v1, "u8RedGain"

    invoke-interface {v7, v1}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v1

    invoke-interface {v7, v1}, Landroid/database/Cursor;->getInt(I)I

    move-result v1

    int-to-short v1, v1

    iput-short v1, v0, Lcom/android/server/tv/DataBaseDesk$T_MS_COLOR_TEMP_DATA;->redgain:S

    const-string v1, "u8GreenGain"

    invoke-interface {v7, v1}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v1

    invoke-interface {v7, v1}, Landroid/database/Cursor;->getInt(I)I

    move-result v1

    int-to-short v1, v1

    iput-short v1, v0, Lcom/android/server/tv/DataBaseDesk$T_MS_COLOR_TEMP_DATA;->greengain:S

    const-string v1, "u8BlueGain"

    invoke-interface {v7, v1}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v1

    invoke-interface {v7, v1}, Landroid/database/Cursor;->getInt(I)I

    move-result v1

    int-to-short v1, v1

    iput-short v1, v0, Lcom/android/server/tv/DataBaseDesk$T_MS_COLOR_TEMP_DATA;->bluegain:S

    const-string v1, "u8RedOffset"

    invoke-interface {v7, v1}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v1

    invoke-interface {v7, v1}, Landroid/database/Cursor;->getInt(I)I

    move-result v1

    int-to-short v1, v1

    iput-short v1, v0, Lcom/android/server/tv/DataBaseDesk$T_MS_COLOR_TEMP_DATA;->redoffset:S

    const-string v1, "u8GreenOffset"

    invoke-interface {v7, v1}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v1

    invoke-interface {v7, v1}, Landroid/database/Cursor;->getInt(I)I

    move-result v1

    int-to-short v1, v1

    iput-short v1, v0, Lcom/android/server/tv/DataBaseDesk$T_MS_COLOR_TEMP_DATA;->greenoffset:S

    const-string v1, "u8BlueOffset"

    invoke-interface {v7, v1}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v1

    invoke-interface {v7, v1}, Landroid/database/Cursor;->getInt(I)I

    move-result v1

    int-to-short v1, v1

    iput-short v1, v0, Lcom/android/server/tv/DataBaseDesk$T_MS_COLOR_TEMP_DATA;->blueoffset:S

    :cond_0
    invoke-interface {v7}, Landroid/database/Cursor;->close()V

    return-object v0
.end method

.method public queryFactoryColorTempExData()Lcom/android/server/tv/DataBaseDesk$T_MS_COLOR_TEMPEX;
    .locals 10

    const/4 v2, 0x0

    new-instance v8, Lcom/android/server/tv/DataBaseDesk$T_MS_COLOR_TEMPEX;

    invoke-direct {v8}, Lcom/android/server/tv/DataBaseDesk$T_MS_COLOR_TEMPEX;-><init>()V

    const/4 v9, 0x0

    :goto_0
    sget-object v0, Lcom/android/server/tv/DataBaseDesk$EN_MS_COLOR_TEMP_INPUT_SOURCE;->E_INPUT_SOURCE_NUM:Lcom/android/server/tv/DataBaseDesk$EN_MS_COLOR_TEMP_INPUT_SOURCE;

    invoke-virtual {v0}, Lcom/android/server/tv/DataBaseDesk$EN_MS_COLOR_TEMP_INPUT_SOURCE;->ordinal()I

    move-result v0

    if-ge v9, v0, :cond_3

    invoke-virtual {p0}, Lcom/android/server/tv/DataBaseDeskImpl;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v0

    const-string v1, "content://mstar.tv.factory/factorycolortempex"

    invoke-static {v1}, Landroid/net/Uri;->parse(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v1

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string v4, "InputSourceID = "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3, v9}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    const-string v5, "ColorTemperatureID"

    move-object v4, v2

    invoke-virtual/range {v0 .. v5}, Landroid/content/ContentResolver;->query(Landroid/net/Uri;[Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;)Landroid/database/Cursor;

    move-result-object v7

    const-string v1, "DataBaseDeskImpl"

    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "cursor?"

    invoke-virtual {v0, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    if-nez v7, :cond_1

    const/4 v0, 0x1

    :goto_1
    invoke-virtual {v3, v0}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-static {v1, v0}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    const/4 v6, 0x0

    :goto_2
    sget-object v0, Lcom/android/server/tv/DataBaseDesk$EN_MS_COLOR_TEMP;->MS_COLOR_TEMP_NUM:Lcom/android/server/tv/DataBaseDesk$EN_MS_COLOR_TEMP;

    invoke-virtual {v0}, Lcom/android/server/tv/DataBaseDesk$EN_MS_COLOR_TEMP;->ordinal()I

    move-result v0

    if-ge v6, v0, :cond_2

    invoke-interface {v7}, Landroid/database/Cursor;->moveToNext()Z

    move-result v0

    if-eqz v0, :cond_0

    iget-object v0, v8, Lcom/android/server/tv/DataBaseDesk$T_MS_COLOR_TEMPEX;->astColorTempEx:[[Lcom/android/server/tv/DataBaseDesk$T_MS_COLOR_TEMPEX_DATA;

    aget-object v0, v0, v6

    aget-object v0, v0, v9

    const-string v1, "u16RedGain"

    invoke-interface {v7, v1}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v1

    invoke-interface {v7, v1}, Landroid/database/Cursor;->getInt(I)I

    move-result v1

    iput v1, v0, Lcom/android/server/tv/DataBaseDesk$T_MS_COLOR_TEMPEX_DATA;->redgain:I

    iget-object v0, v8, Lcom/android/server/tv/DataBaseDesk$T_MS_COLOR_TEMPEX;->astColorTempEx:[[Lcom/android/server/tv/DataBaseDesk$T_MS_COLOR_TEMPEX_DATA;

    aget-object v0, v0, v6

    aget-object v0, v0, v9

    const-string v1, "u16GreenGain"

    invoke-interface {v7, v1}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v1

    invoke-interface {v7, v1}, Landroid/database/Cursor;->getInt(I)I

    move-result v1

    iput v1, v0, Lcom/android/server/tv/DataBaseDesk$T_MS_COLOR_TEMPEX_DATA;->greengain:I

    iget-object v0, v8, Lcom/android/server/tv/DataBaseDesk$T_MS_COLOR_TEMPEX;->astColorTempEx:[[Lcom/android/server/tv/DataBaseDesk$T_MS_COLOR_TEMPEX_DATA;

    aget-object v0, v0, v6

    aget-object v0, v0, v9

    const-string v1, "u16BlueGain"

    invoke-interface {v7, v1}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v1

    invoke-interface {v7, v1}, Landroid/database/Cursor;->getInt(I)I

    move-result v1

    iput v1, v0, Lcom/android/server/tv/DataBaseDesk$T_MS_COLOR_TEMPEX_DATA;->bluegain:I

    iget-object v0, v8, Lcom/android/server/tv/DataBaseDesk$T_MS_COLOR_TEMPEX;->astColorTempEx:[[Lcom/android/server/tv/DataBaseDesk$T_MS_COLOR_TEMPEX_DATA;

    aget-object v0, v0, v6

    aget-object v0, v0, v9

    const-string v1, "u16RedOffset"

    invoke-interface {v7, v1}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v1

    invoke-interface {v7, v1}, Landroid/database/Cursor;->getInt(I)I

    move-result v1

    iput v1, v0, Lcom/android/server/tv/DataBaseDesk$T_MS_COLOR_TEMPEX_DATA;->redoffset:I

    iget-object v0, v8, Lcom/android/server/tv/DataBaseDesk$T_MS_COLOR_TEMPEX;->astColorTempEx:[[Lcom/android/server/tv/DataBaseDesk$T_MS_COLOR_TEMPEX_DATA;

    aget-object v0, v0, v6

    aget-object v0, v0, v9

    const-string v1, "u16GreenOffset"

    invoke-interface {v7, v1}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v1

    invoke-interface {v7, v1}, Landroid/database/Cursor;->getInt(I)I

    move-result v1

    iput v1, v0, Lcom/android/server/tv/DataBaseDesk$T_MS_COLOR_TEMPEX_DATA;->greenoffset:I

    iget-object v0, v8, Lcom/android/server/tv/DataBaseDesk$T_MS_COLOR_TEMPEX;->astColorTempEx:[[Lcom/android/server/tv/DataBaseDesk$T_MS_COLOR_TEMPEX_DATA;

    aget-object v0, v0, v6

    aget-object v0, v0, v9

    const-string v1, "u16BlueOffset"

    invoke-interface {v7, v1}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v1

    invoke-interface {v7, v1}, Landroid/database/Cursor;->getInt(I)I

    move-result v1

    iput v1, v0, Lcom/android/server/tv/DataBaseDesk$T_MS_COLOR_TEMPEX_DATA;->blueoffset:I

    :cond_0
    add-int/lit8 v6, v6, 0x1

    goto :goto_2

    :cond_1
    const/4 v0, 0x0

    goto/16 :goto_1

    :cond_2
    invoke-interface {v7}, Landroid/database/Cursor;->close()V

    add-int/lit8 v9, v9, 0x1

    goto/16 :goto_0

    :cond_3
    return-object v8
.end method

.method public queryFactoryExtern()Lcom/android/server/tv/DataBaseDesk$MS_FACTORY_EXTERN_SETTING;
    .locals 8

    const/4 v2, 0x0

    invoke-virtual {p0}, Lcom/android/server/tv/DataBaseDeskImpl;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v0

    const-string v1, "content://mstar.tv.factory/factoryextern"

    invoke-static {v1}, Landroid/net/Uri;->parse(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v1

    move-object v3, v2

    move-object v4, v2

    move-object v5, v2

    invoke-virtual/range {v0 .. v5}, Landroid/content/ContentResolver;->query(Landroid/net/Uri;[Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;)Landroid/database/Cursor;

    move-result-object v6

    new-instance v7, Lcom/android/server/tv/DataBaseDesk$MS_FACTORY_EXTERN_SETTING;

    invoke-direct {v7}, Lcom/android/server/tv/DataBaseDesk$MS_FACTORY_EXTERN_SETTING;-><init>()V

    invoke-interface {v6}, Landroid/database/Cursor;->moveToFirst()Z

    move-result v0

    if-eqz v0, :cond_0

    const-string v0, "SoftWareVersion"

    invoke-interface {v6, v0}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v0

    invoke-interface {v6, v0}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v0

    sput-object v0, Lcom/android/server/tv/DataBaseDesk$MS_FACTORY_EXTERN_SETTING;->softVersion:Ljava/lang/String;

    const-string v0, "BoardType"

    invoke-interface {v6, v0}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v0

    invoke-interface {v6, v0}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v0

    sput-object v0, Lcom/android/server/tv/DataBaseDesk$MS_FACTORY_EXTERN_SETTING;->boardType:Ljava/lang/String;

    const-string v0, "PanelType"

    invoke-interface {v6, v0}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v0

    invoke-interface {v6, v0}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v0

    sput-object v0, Lcom/android/server/tv/DataBaseDesk$MS_FACTORY_EXTERN_SETTING;->panelType:Ljava/lang/String;

    const-string v0, "CompileTime"

    invoke-interface {v6, v0}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v0

    invoke-interface {v6, v0}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v0

    sput-object v0, Lcom/android/server/tv/DataBaseDesk$MS_FACTORY_EXTERN_SETTING;->dayAndTime:Ljava/lang/String;

    const-string v0, "TestPatternMode"

    invoke-interface {v6, v0}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v0

    invoke-interface {v6, v0}, Landroid/database/Cursor;->getInt(I)I

    move-result v0

    iput v0, v7, Lcom/android/server/tv/DataBaseDesk$MS_FACTORY_EXTERN_SETTING;->testPatternMode:I

    const-string v0, "stPowerMode"

    invoke-interface {v6, v0}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v0

    invoke-interface {v6, v0}, Landroid/database/Cursor;->getInt(I)I

    move-result v0

    iput v0, v7, Lcom/android/server/tv/DataBaseDesk$MS_FACTORY_EXTERN_SETTING;->stPowerMode:I

    const-string v0, "DtvAvAbnormalDelay"

    invoke-interface {v6, v0}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v0

    invoke-interface {v6, v0}, Landroid/database/Cursor;->getInt(I)I

    move-result v0

    if-nez v0, :cond_1

    const/4 v0, 0x0

    :goto_0
    iput-boolean v0, v7, Lcom/android/server/tv/DataBaseDesk$MS_FACTORY_EXTERN_SETTING;->dtvAvAbnormalDelay:Z

    const-string v0, "FactoryPreSetFeature"

    invoke-interface {v6, v0}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v0

    invoke-interface {v6, v0}, Landroid/database/Cursor;->getInt(I)I

    move-result v0

    iput v0, v7, Lcom/android/server/tv/DataBaseDesk$MS_FACTORY_EXTERN_SETTING;->factoryPreset:I

    const-string v0, "PanelSwing"

    invoke-interface {v6, v0}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v0

    invoke-interface {v6, v0}, Landroid/database/Cursor;->getInt(I)I

    move-result v0

    int-to-short v0, v0

    iput-short v0, v7, Lcom/android/server/tv/DataBaseDesk$MS_FACTORY_EXTERN_SETTING;->panelSwingVal:S

    const-string v0, "AudioPrescale"

    invoke-interface {v6, v0}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v0

    invoke-interface {v6, v0}, Landroid/database/Cursor;->getInt(I)I

    move-result v0

    int-to-short v0, v0

    iput-short v0, v7, Lcom/android/server/tv/DataBaseDesk$MS_FACTORY_EXTERN_SETTING;->audioPreScale:S

    const-string v0, "vdDspVersion"

    invoke-interface {v6, v0}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v0

    invoke-interface {v6, v0}, Landroid/database/Cursor;->getInt(I)I

    move-result v0

    int-to-short v0, v0

    iput-short v0, v7, Lcom/android/server/tv/DataBaseDesk$MS_FACTORY_EXTERN_SETTING;->vdDspVersion:S

    const-string v0, "eHidevMode"

    invoke-interface {v6, v0}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v0

    invoke-interface {v6, v0}, Landroid/database/Cursor;->getInt(I)I

    move-result v0

    int-to-short v0, v0

    iput v0, v7, Lcom/android/server/tv/DataBaseDesk$MS_FACTORY_EXTERN_SETTING;->eHidevMode:I

    const-string v0, "audioNrThr"

    invoke-interface {v6, v0}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v0

    invoke-interface {v6, v0}, Landroid/database/Cursor;->getInt(I)I

    move-result v0

    int-to-short v0, v0

    iput-short v0, v7, Lcom/android/server/tv/DataBaseDesk$MS_FACTORY_EXTERN_SETTING;->audioNrThr:S

    const-string v0, "audioSifThreshold"

    invoke-interface {v6, v0}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v0

    invoke-interface {v6, v0}, Landroid/database/Cursor;->getInt(I)I

    move-result v0

    int-to-short v0, v0

    iput-short v0, v7, Lcom/android/server/tv/DataBaseDesk$MS_FACTORY_EXTERN_SETTING;->audioSifThreshold:S

    const-string v0, "audioDspVersion"

    invoke-interface {v6, v0}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v0

    invoke-interface {v6, v0}, Landroid/database/Cursor;->getInt(I)I

    move-result v0

    int-to-short v0, v0

    iput-short v0, v7, Lcom/android/server/tv/DataBaseDesk$MS_FACTORY_EXTERN_SETTING;->audioDspVersion:S

    :cond_0
    invoke-interface {v6}, Landroid/database/Cursor;->close()V

    return-object v7

    :cond_1
    const/4 v0, 0x1

    goto :goto_0
.end method

.method public queryFilmMode(I)I
    .locals 8
    .param p1    # I

    const/4 v2, 0x0

    const/4 v7, 0x0

    invoke-virtual {p0}, Lcom/android/server/tv/DataBaseDeskImpl;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v0

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "content://mstar.tv.usersetting/videosetting/inputsrc/"

    invoke-virtual {v1, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v1}, Landroid/net/Uri;->parse(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v1

    move-object v3, v2

    move-object v4, v2

    move-object v5, v2

    invoke-virtual/range {v0 .. v5}, Landroid/content/ContentResolver;->query(Landroid/net/Uri;[Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;)Landroid/database/Cursor;

    move-result-object v6

    :goto_0
    invoke-interface {v6}, Landroid/database/Cursor;->moveToNext()Z

    move-result v0

    if-eqz v0, :cond_0

    const-string v0, "eFilm"

    invoke-interface {v6, v0}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v0

    invoke-interface {v6, v0}, Landroid/database/Cursor;->getInt(I)I

    move-result v7

    goto :goto_0

    :cond_0
    invoke-interface {v6}, Landroid/database/Cursor;->close()V

    return v7
.end method

.method public queryLastVideoStandardMode(I)I
    .locals 8
    .param p1    # I

    const/4 v2, 0x0

    const/4 v7, -0x1

    invoke-virtual {p0}, Lcom/android/server/tv/DataBaseDeskImpl;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v0

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "content://mstar.tv.usersetting/videosetting/inputsrc/"

    invoke-virtual {v1, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v1}, Landroid/net/Uri;->parse(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v1

    move-object v3, v2

    move-object v4, v2

    move-object v5, v2

    invoke-virtual/range {v0 .. v5}, Landroid/content/ContentResolver;->query(Landroid/net/Uri;[Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;)Landroid/database/Cursor;

    move-result-object v6

    invoke-interface {v6}, Landroid/database/Cursor;->moveToNext()Z

    move-result v0

    if-eqz v0, :cond_0

    const-string v0, "LastVideoStandardMode"

    invoke-interface {v6, v0}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v0

    invoke-interface {v6, v0}, Landroid/database/Cursor;->getInt(I)I

    move-result v7

    :cond_0
    invoke-interface {v6}, Landroid/database/Cursor;->close()V

    return v7
.end method

.method public queryNR(II)Lcom/android/server/tv/DataBaseDesk$EN_MS_NR;
    .locals 8
    .param p1    # I
    .param p2    # I

    const/4 v2, 0x0

    invoke-virtual {p0}, Lcom/android/server/tv/DataBaseDeskImpl;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v0

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "content://mstar.tv.usersetting/nrmode/nrmode/"

    invoke-virtual {v1, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v3, "/inputsrc/"

    invoke-virtual {v1, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v1}, Landroid/net/Uri;->parse(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v1

    move-object v3, v2

    move-object v4, v2

    move-object v5, v2

    invoke-virtual/range {v0 .. v5}, Landroid/content/ContentResolver;->query(Landroid/net/Uri;[Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;)Landroid/database/Cursor;

    move-result-object v6

    const/4 v7, 0x0

    invoke-interface {v6}, Landroid/database/Cursor;->moveToNext()Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-static {}, Lcom/android/server/tv/DataBaseDesk$EN_MS_NR;->values()[Lcom/android/server/tv/DataBaseDesk$EN_MS_NR;

    move-result-object v0

    const-string v1, "eNR"

    invoke-interface {v6, v1}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v1

    invoke-interface {v6, v1}, Landroid/database/Cursor;->getInt(I)I

    move-result v1

    aget-object v7, v0, v1

    :cond_0
    invoke-interface {v6}, Landroid/database/Cursor;->close()V

    return-object v7
.end method

.method public queryNoStandSet()Lcom/mstar/android/tvapi/factory/vo/FactoryNsVdSet;
    .locals 8

    const/4 v2, 0x0

    new-instance v7, Lcom/mstar/android/tvapi/factory/vo/FactoryNsVdSet;

    invoke-direct {v7}, Lcom/mstar/android/tvapi/factory/vo/FactoryNsVdSet;-><init>()V

    invoke-virtual {p0}, Lcom/android/server/tv/DataBaseDeskImpl;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v0

    const-string v1, "content://mstar.tv.factory/nonstandardadjust"

    invoke-static {v1}, Landroid/net/Uri;->parse(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v1

    move-object v3, v2

    move-object v4, v2

    move-object v5, v2

    invoke-virtual/range {v0 .. v5}, Landroid/content/ContentResolver;->query(Landroid/net/Uri;[Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;)Landroid/database/Cursor;

    move-result-object v6

    invoke-interface {v6}, Landroid/database/Cursor;->moveToFirst()Z

    move-result v0

    if-eqz v0, :cond_0

    const-string v0, "u8AFEC_D4"

    invoke-interface {v6, v0}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v0

    invoke-interface {v6, v0}, Landroid/database/Cursor;->getInt(I)I

    move-result v0

    int-to-short v0, v0

    iput-short v0, v7, Lcom/mstar/android/tvapi/factory/vo/FactoryNsVdSet;->aFEC_D4:S

    const-string v0, "u8AFEC_D5_Bit2"

    invoke-interface {v6, v0}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v0

    invoke-interface {v6, v0}, Landroid/database/Cursor;->getInt(I)I

    move-result v0

    int-to-short v0, v0

    iput-short v0, v7, Lcom/mstar/android/tvapi/factory/vo/FactoryNsVdSet;->aFEC_D5_Bit2:S

    const-string v0, "u8AFEC_D8_Bit3210"

    invoke-interface {v6, v0}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v0

    invoke-interface {v6, v0}, Landroid/database/Cursor;->getInt(I)I

    move-result v0

    int-to-short v0, v0

    iput-short v0, v7, Lcom/mstar/android/tvapi/factory/vo/FactoryNsVdSet;->aFEC_D8_Bit3210:S

    const-string v0, "u8AFEC_D9_Bit0"

    invoke-interface {v6, v0}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v0

    invoke-interface {v6, v0}, Landroid/database/Cursor;->getInt(I)I

    move-result v0

    int-to-short v0, v0

    iput-short v0, v7, Lcom/mstar/android/tvapi/factory/vo/FactoryNsVdSet;->aFEC_D9_Bit0:S

    const-string v0, "u8AFEC_D7_LOW_BOUND"

    invoke-interface {v6, v0}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v0

    invoke-interface {v6, v0}, Landroid/database/Cursor;->getInt(I)I

    move-result v0

    int-to-short v0, v0

    iput-short v0, v7, Lcom/mstar/android/tvapi/factory/vo/FactoryNsVdSet;->aFEC_D7_LOW_BOUND:S

    const-string v0, "u8AFEC_D7_HIGH_BOUND"

    invoke-interface {v6, v0}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v0

    invoke-interface {v6, v0}, Landroid/database/Cursor;->getInt(I)I

    move-result v0

    int-to-short v0, v0

    iput-short v0, v7, Lcom/mstar/android/tvapi/factory/vo/FactoryNsVdSet;->aFEC_D7_HIGH_BOUND:S

    const-string v0, "u8AFEC_A0"

    invoke-interface {v6, v0}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v0

    invoke-interface {v6, v0}, Landroid/database/Cursor;->getInt(I)I

    move-result v0

    int-to-short v0, v0

    iput-short v0, v7, Lcom/mstar/android/tvapi/factory/vo/FactoryNsVdSet;->aFEC_A0:S

    const-string v0, "u8AFEC_A1"

    invoke-interface {v6, v0}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v0

    invoke-interface {v6, v0}, Landroid/database/Cursor;->getInt(I)I

    move-result v0

    int-to-short v0, v0

    iput-short v0, v7, Lcom/mstar/android/tvapi/factory/vo/FactoryNsVdSet;->aFEC_A1:S

    const-string v0, "u8AFEC_66_Bit76"

    invoke-interface {v6, v0}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v0

    invoke-interface {v6, v0}, Landroid/database/Cursor;->getInt(I)I

    move-result v0

    int-to-short v0, v0

    iput-short v0, v7, Lcom/mstar/android/tvapi/factory/vo/FactoryNsVdSet;->aFEC_66_Bit76:S

    const-string v0, "u8AFEC_6E_Bit7654"

    invoke-interface {v6, v0}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v0

    invoke-interface {v6, v0}, Landroid/database/Cursor;->getInt(I)I

    move-result v0

    int-to-short v0, v0

    iput-short v0, v7, Lcom/mstar/android/tvapi/factory/vo/FactoryNsVdSet;->aFEC_6E_Bit7654:S

    const-string v0, "u8AFEC_6E_Bit3210"

    invoke-interface {v6, v0}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v0

    invoke-interface {v6, v0}, Landroid/database/Cursor;->getInt(I)I

    move-result v0

    int-to-short v0, v0

    iput-short v0, v7, Lcom/mstar/android/tvapi/factory/vo/FactoryNsVdSet;->aFEC_6E_Bit3210:S

    const-string v0, "u8AFEC_43"

    invoke-interface {v6, v0}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v0

    invoke-interface {v6, v0}, Landroid/database/Cursor;->getInt(I)I

    move-result v0

    int-to-short v0, v0

    iput-short v0, v7, Lcom/mstar/android/tvapi/factory/vo/FactoryNsVdSet;->aFEC_43:S

    const-string v0, "u8AFEC_44"

    invoke-interface {v6, v0}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v0

    invoke-interface {v6, v0}, Landroid/database/Cursor;->getInt(I)I

    move-result v0

    int-to-short v0, v0

    iput-short v0, v7, Lcom/mstar/android/tvapi/factory/vo/FactoryNsVdSet;->aFEC_44:S

    const-string v0, "u8AFEC_CB"

    invoke-interface {v6, v0}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v0

    invoke-interface {v6, v0}, Landroid/database/Cursor;->getInt(I)I

    move-result v0

    int-to-short v0, v0

    iput-short v0, v7, Lcom/mstar/android/tvapi/factory/vo/FactoryNsVdSet;->aFEC_CB:S

    const-string v0, "u8AFEC_CF_Bit2_ATV"

    invoke-interface {v6, v0}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v0

    invoke-interface {v6, v0}, Landroid/database/Cursor;->getInt(I)I

    move-result v0

    int-to-short v0, v0

    iput-short v0, v7, Lcom/mstar/android/tvapi/factory/vo/FactoryNsVdSet;->aFEC_CF_Bit2_ATV:S

    const-string v0, "u8AFEC_CF_Bit2_AV"

    invoke-interface {v6, v0}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v0

    invoke-interface {v6, v0}, Landroid/database/Cursor;->getInt(I)I

    move-result v0

    int-to-short v0, v0

    iput-short v0, v7, Lcom/mstar/android/tvapi/factory/vo/FactoryNsVdSet;->aFEC_CF_Bit2_AV:S

    :cond_0
    invoke-interface {v6}, Landroid/database/Cursor;->close()V

    return-object v7
.end method

.method public queryNoStandVifSet()Lcom/android/server/tv/DataBaseDesk$MS_Factory_NS_VIF_SET;
    .locals 10

    const/4 v9, 0x1

    const/4 v8, 0x0

    const/4 v2, 0x0

    invoke-virtual {p0}, Lcom/android/server/tv/DataBaseDeskImpl;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v0

    const-string v1, "content://mstar.tv.factory/nonstandardadjust"

    invoke-static {v1}, Landroid/net/Uri;->parse(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v1

    move-object v3, v2

    move-object v4, v2

    move-object v5, v2

    invoke-virtual/range {v0 .. v5}, Landroid/content/ContentResolver;->query(Landroid/net/Uri;[Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;)Landroid/database/Cursor;

    move-result-object v6

    new-instance v7, Lcom/android/server/tv/DataBaseDesk$MS_Factory_NS_VIF_SET;

    invoke-direct {v7}, Lcom/android/server/tv/DataBaseDesk$MS_Factory_NS_VIF_SET;-><init>()V

    invoke-interface {v6}, Landroid/database/Cursor;->moveToFirst()Z

    move-result v0

    if-eqz v0, :cond_0

    const-string v0, "VifTop"

    invoke-interface {v6, v0}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v0

    invoke-interface {v6, v0}, Landroid/database/Cursor;->getInt(I)I

    move-result v0

    int-to-short v0, v0

    iput-short v0, v7, Lcom/android/server/tv/DataBaseDesk$MS_Factory_NS_VIF_SET;->VifTop:S

    const-string v0, "VifVgaMaximum"

    invoke-interface {v6, v0}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v0

    invoke-interface {v6, v0}, Landroid/database/Cursor;->getInt(I)I

    move-result v0

    iput v0, v7, Lcom/android/server/tv/DataBaseDesk$MS_Factory_NS_VIF_SET;->VifVgaMaximum:I

    const-string v0, "VifCrKp"

    invoke-interface {v6, v0}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v0

    invoke-interface {v6, v0}, Landroid/database/Cursor;->getInt(I)I

    move-result v0

    int-to-short v0, v0

    iput-short v0, v7, Lcom/android/server/tv/DataBaseDesk$MS_Factory_NS_VIF_SET;->VifCrKp:S

    const-string v0, "VifCrKi"

    invoke-interface {v6, v0}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v0

    invoke-interface {v6, v0}, Landroid/database/Cursor;->getInt(I)I

    move-result v0

    int-to-short v0, v0

    iput-short v0, v7, Lcom/android/server/tv/DataBaseDesk$MS_Factory_NS_VIF_SET;->VifCrKi:S

    const-string v0, "VifCrKp1"

    invoke-interface {v6, v0}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v0

    invoke-interface {v6, v0}, Landroid/database/Cursor;->getInt(I)I

    move-result v0

    int-to-short v0, v0

    iput-short v0, v7, Lcom/android/server/tv/DataBaseDesk$MS_Factory_NS_VIF_SET;->VifCrKp1:S

    const-string v0, "VifCrKi1"

    invoke-interface {v6, v0}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v0

    invoke-interface {v6, v0}, Landroid/database/Cursor;->getInt(I)I

    move-result v0

    int-to-short v0, v0

    iput-short v0, v7, Lcom/android/server/tv/DataBaseDesk$MS_Factory_NS_VIF_SET;->VifCrKi1:S

    const-string v0, "VifCrKp2"

    invoke-interface {v6, v0}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v0

    invoke-interface {v6, v0}, Landroid/database/Cursor;->getInt(I)I

    move-result v0

    int-to-short v0, v0

    iput-short v0, v7, Lcom/android/server/tv/DataBaseDesk$MS_Factory_NS_VIF_SET;->VifCrKp2:S

    const-string v0, "VifCrKi2"

    invoke-interface {v6, v0}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v0

    invoke-interface {v6, v0}, Landroid/database/Cursor;->getInt(I)I

    move-result v0

    int-to-short v0, v0

    iput-short v0, v7, Lcom/android/server/tv/DataBaseDesk$MS_Factory_NS_VIF_SET;->VifCrKi2:S

    const-string v0, "VifAsiaSignalOption"

    invoke-interface {v6, v0}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v0

    invoke-interface {v6, v0}, Landroid/database/Cursor;->getInt(I)I

    move-result v0

    if-nez v0, :cond_1

    move v0, v8

    :goto_0
    iput-boolean v0, v7, Lcom/android/server/tv/DataBaseDesk$MS_Factory_NS_VIF_SET;->VifAsiaSignalOption:Z

    const-string v0, "VifCrKpKiAdjust"

    invoke-interface {v6, v0}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v0

    invoke-interface {v6, v0}, Landroid/database/Cursor;->getInt(I)I

    move-result v0

    int-to-short v0, v0

    if-nez v0, :cond_2

    move v0, v8

    :goto_1
    iput-boolean v0, v7, Lcom/android/server/tv/DataBaseDesk$MS_Factory_NS_VIF_SET;->VifCrKpKiAdjust:Z

    const-string v0, "VifOverModulation"

    invoke-interface {v6, v0}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v0

    invoke-interface {v6, v0}, Landroid/database/Cursor;->getInt(I)I

    move-result v0

    if-nez v0, :cond_3

    :goto_2
    iput-boolean v8, v7, Lcom/android/server/tv/DataBaseDesk$MS_Factory_NS_VIF_SET;->VifOverModulation:Z

    const-string v0, "VifClampgainGainOvNegative"

    invoke-interface {v6, v0}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v0

    invoke-interface {v6, v0}, Landroid/database/Cursor;->getInt(I)I

    move-result v0

    iput v0, v7, Lcom/android/server/tv/DataBaseDesk$MS_Factory_NS_VIF_SET;->VifClampgainGainOvNegative:I

    const-string v0, "ChinaDescramblerBox"

    invoke-interface {v6, v0}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v0

    invoke-interface {v6, v0}, Landroid/database/Cursor;->getInt(I)I

    move-result v0

    int-to-short v0, v0

    iput-short v0, v7, Lcom/android/server/tv/DataBaseDesk$MS_Factory_NS_VIF_SET;->ChinaDescramblerBox:S

    const-string v0, "VifDelayReduce"

    invoke-interface {v6, v0}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v0

    invoke-interface {v6, v0}, Landroid/database/Cursor;->getInt(I)I

    move-result v0

    int-to-short v0, v0

    iput-short v0, v7, Lcom/android/server/tv/DataBaseDesk$MS_Factory_NS_VIF_SET;->VifDelayReduce:S

    const-string v0, "VifCrThr"

    invoke-interface {v6, v0}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v0

    invoke-interface {v6, v0}, Landroid/database/Cursor;->getInt(I)I

    move-result v0

    int-to-short v0, v0

    iput v0, v7, Lcom/android/server/tv/DataBaseDesk$MS_Factory_NS_VIF_SET;->VifCrThr:I

    const-string v0, "VifVersion"

    invoke-interface {v6, v0}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v0

    invoke-interface {v6, v0}, Landroid/database/Cursor;->getInt(I)I

    move-result v0

    int-to-short v0, v0

    iput-short v0, v7, Lcom/android/server/tv/DataBaseDesk$MS_Factory_NS_VIF_SET;->VifVersion:S

    const-string v0, "VifACIAGCREF"

    invoke-interface {v6, v0}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v0

    invoke-interface {v6, v0}, Landroid/database/Cursor;->getInt(I)I

    move-result v0

    int-to-short v0, v0

    iput-short v0, v7, Lcom/android/server/tv/DataBaseDesk$MS_Factory_NS_VIF_SET;->VifACIAGCREF:S

    const-string v0, "VifAgcRefNegative"

    invoke-interface {v6, v0}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v0

    invoke-interface {v6, v0}, Landroid/database/Cursor;->getInt(I)I

    move-result v0

    int-to-short v0, v0

    iput-short v0, v7, Lcom/android/server/tv/DataBaseDesk$MS_Factory_NS_VIF_SET;->VifAgcRefNegative:S

    const-string v0, "GainDistributionThr"

    invoke-interface {v6, v0}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v0

    invoke-interface {v6, v0}, Landroid/database/Cursor;->getInt(I)I

    move-result v0

    iput v0, v7, Lcom/android/server/tv/DataBaseDesk$MS_Factory_NS_VIF_SET;->GainDistributionThr:I

    :cond_0
    invoke-interface {v6}, Landroid/database/Cursor;->close()V

    return-object v7

    :cond_1
    move v0, v9

    goto/16 :goto_0

    :cond_2
    move v0, v9

    goto :goto_1

    :cond_3
    move v8, v9

    goto :goto_2
.end method

.method public queryNonLinearAdjusts()Lcom/android/server/tv/DataBaseDesk$MS_NLA_SETTING;
    .locals 9

    const/4 v2, 0x0

    invoke-virtual {p0}, Lcom/android/server/tv/DataBaseDeskImpl;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v0

    const-string v1, "content://mstar.tv.factory/nonlinearadjust"

    invoke-static {v1}, Landroid/net/Uri;->parse(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v1

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string v4, "InputSrcType = "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-direct {p0}, Lcom/android/server/tv/DataBaseDeskImpl;->getCurrentInputSource()I

    move-result v4

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    const-string v5, "CurveTypeIndex"

    move-object v4, v2

    invoke-virtual/range {v0 .. v5}, Landroid/content/ContentResolver;->query(Landroid/net/Uri;[Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;)Landroid/database/Cursor;

    move-result-object v6

    const/4 v7, 0x0

    iget-object v0, p0, Lcom/android/server/tv/DataBaseDeskImpl;->m_pastNLASet:Lcom/android/server/tv/DataBaseDesk$MS_NLA_SETTING;

    iget-object v0, v0, Lcom/android/server/tv/DataBaseDesk$MS_NLA_SETTING;->stNLASetting:[Lcom/android/server/tv/DataBaseDesk$MS_NLA_POINT;

    array-length v8, v0

    :goto_0
    invoke-interface {v6}, Landroid/database/Cursor;->moveToNext()Z

    move-result v0

    if-eqz v0, :cond_0

    add-int/lit8 v0, v8, -0x1

    if-le v7, v0, :cond_1

    :cond_0
    invoke-interface {v6}, Landroid/database/Cursor;->close()V

    iget-object v0, p0, Lcom/android/server/tv/DataBaseDeskImpl;->m_pastNLASet:Lcom/android/server/tv/DataBaseDesk$MS_NLA_SETTING;

    return-object v0

    :cond_1
    iget-object v0, p0, Lcom/android/server/tv/DataBaseDeskImpl;->m_pastNLASet:Lcom/android/server/tv/DataBaseDesk$MS_NLA_SETTING;

    iget-object v0, v0, Lcom/android/server/tv/DataBaseDesk$MS_NLA_SETTING;->stNLASetting:[Lcom/android/server/tv/DataBaseDesk$MS_NLA_POINT;

    aget-object v0, v0, v7

    const-string v1, "u8OSD_V0"

    invoke-interface {v6, v1}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v1

    invoke-interface {v6, v1}, Landroid/database/Cursor;->getInt(I)I

    move-result v1

    int-to-short v1, v1

    iput-short v1, v0, Lcom/android/server/tv/DataBaseDesk$MS_NLA_POINT;->u8OSD_V0:S

    iget-object v0, p0, Lcom/android/server/tv/DataBaseDeskImpl;->m_pastNLASet:Lcom/android/server/tv/DataBaseDesk$MS_NLA_SETTING;

    iget-object v0, v0, Lcom/android/server/tv/DataBaseDesk$MS_NLA_SETTING;->stNLASetting:[Lcom/android/server/tv/DataBaseDesk$MS_NLA_POINT;

    aget-object v0, v0, v7

    const-string v1, "u8OSD_V25"

    invoke-interface {v6, v1}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v1

    invoke-interface {v6, v1}, Landroid/database/Cursor;->getInt(I)I

    move-result v1

    int-to-short v1, v1

    iput-short v1, v0, Lcom/android/server/tv/DataBaseDesk$MS_NLA_POINT;->u8OSD_V25:S

    iget-object v0, p0, Lcom/android/server/tv/DataBaseDeskImpl;->m_pastNLASet:Lcom/android/server/tv/DataBaseDesk$MS_NLA_SETTING;

    iget-object v0, v0, Lcom/android/server/tv/DataBaseDesk$MS_NLA_SETTING;->stNLASetting:[Lcom/android/server/tv/DataBaseDesk$MS_NLA_POINT;

    aget-object v0, v0, v7

    const-string v1, "u8OSD_V50"

    invoke-interface {v6, v1}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v1

    invoke-interface {v6, v1}, Landroid/database/Cursor;->getInt(I)I

    move-result v1

    int-to-short v1, v1

    iput-short v1, v0, Lcom/android/server/tv/DataBaseDesk$MS_NLA_POINT;->u8OSD_V50:S

    iget-object v0, p0, Lcom/android/server/tv/DataBaseDeskImpl;->m_pastNLASet:Lcom/android/server/tv/DataBaseDesk$MS_NLA_SETTING;

    iget-object v0, v0, Lcom/android/server/tv/DataBaseDesk$MS_NLA_SETTING;->stNLASetting:[Lcom/android/server/tv/DataBaseDesk$MS_NLA_POINT;

    aget-object v0, v0, v7

    const-string v1, "u8OSD_V75"

    invoke-interface {v6, v1}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v1

    invoke-interface {v6, v1}, Landroid/database/Cursor;->getInt(I)I

    move-result v1

    int-to-short v1, v1

    iput-short v1, v0, Lcom/android/server/tv/DataBaseDesk$MS_NLA_POINT;->u8OSD_V75:S

    iget-object v0, p0, Lcom/android/server/tv/DataBaseDeskImpl;->m_pastNLASet:Lcom/android/server/tv/DataBaseDesk$MS_NLA_SETTING;

    iget-object v0, v0, Lcom/android/server/tv/DataBaseDesk$MS_NLA_SETTING;->stNLASetting:[Lcom/android/server/tv/DataBaseDesk$MS_NLA_POINT;

    aget-object v0, v0, v7

    const-string v1, "u8OSD_V100"

    invoke-interface {v6, v1}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v1

    invoke-interface {v6, v1}, Landroid/database/Cursor;->getInt(I)I

    move-result v1

    int-to-short v1, v1

    iput-short v1, v0, Lcom/android/server/tv/DataBaseDesk$MS_NLA_POINT;->u8OSD_V100:S

    add-int/lit8 v7, v7, 0x1

    goto :goto_0
.end method

.method public queryOverscanAdjusts(I)[[Lcom/android/server/tv/DataBaseDesk$ST_MAPI_VIDEO_WINDOW_INFO;
    .locals 26
    .param p1    # I

    packed-switch p1, :pswitch_data_0

    const/4 v1, 0x0

    check-cast v1, [[Lcom/android/server/tv/DataBaseDesk$ST_MAPI_VIDEO_WINDOW_INFO;

    :goto_0
    return-object v1

    :pswitch_0
    invoke-virtual/range {p0 .. p0}, Lcom/android/server/tv/DataBaseDeskImpl;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v1

    const-string v2, "content://mstar.tv.factory/dtvoverscansetting"

    invoke-static {v2}, Landroid/net/Uri;->parse(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v2

    const/4 v3, 0x0

    const/4 v4, 0x0

    const/4 v5, 0x0

    const-string v6, "ResolutionTypeNum"

    invoke-virtual/range {v1 .. v6}, Landroid/content/ContentResolver;->query(Landroid/net/Uri;[Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;)Landroid/database/Cursor;

    move-result-object v7

    sget-object v1, Lcom/android/server/tv/DataBaseDesk$MAX_DTV_Resolution_Info;->E_DTV_MAX:Lcom/android/server/tv/DataBaseDesk$MAX_DTV_Resolution_Info;

    invoke-virtual {v1}, Lcom/android/server/tv/DataBaseDesk$MAX_DTV_Resolution_Info;->ordinal()I

    move-result v15

    sget-object v1, Lcom/android/server/tv/DataBaseDesk$MAPI_VIDEO_ARC_Type;->E_AR_MAX:Lcom/android/server/tv/DataBaseDesk$MAPI_VIDEO_ARC_Type;

    invoke-virtual {v1}, Lcom/android/server/tv/DataBaseDesk$MAPI_VIDEO_ARC_Type;->ordinal()I

    move-result v16

    filled-new-array/range {v15 .. v16}, [I

    move-result-object v1

    const-class v2, Lcom/android/server/tv/DataBaseDesk$ST_MAPI_VIDEO_WINDOW_INFO;

    invoke-static {v2, v1}, Ljava/lang/reflect/Array;->newInstance(Ljava/lang/Class;[I)Ljava/lang/Object;

    move-result-object v25

    check-cast v25, [[Lcom/android/server/tv/DataBaseDesk$ST_MAPI_VIDEO_WINDOW_INFO;

    const/4 v12, 0x0

    :goto_1
    if-ge v12, v15, :cond_2

    const/4 v14, 0x0

    :goto_2
    move/from16 v0, v16

    if-ge v14, v0, :cond_1

    new-instance v13, Lcom/android/server/tv/DataBaseDesk$ST_MAPI_VIDEO_WINDOW_INFO;

    invoke-direct {v13}, Lcom/android/server/tv/DataBaseDesk$ST_MAPI_VIDEO_WINDOW_INFO;-><init>()V

    invoke-interface {v7}, Landroid/database/Cursor;->moveToNext()Z

    move-result v1

    if-eqz v1, :cond_0

    const-string v1, "u16H_CapStart"

    invoke-interface {v7, v1}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v1

    invoke-interface {v7, v1}, Landroid/database/Cursor;->getInt(I)I

    move-result v1

    iput v1, v13, Lcom/android/server/tv/DataBaseDesk$ST_MAPI_VIDEO_WINDOW_INFO;->u16H_CapStart:I

    const-string v1, "u16V_CapStart"

    invoke-interface {v7, v1}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v1

    invoke-interface {v7, v1}, Landroid/database/Cursor;->getInt(I)I

    move-result v1

    iput v1, v13, Lcom/android/server/tv/DataBaseDesk$ST_MAPI_VIDEO_WINDOW_INFO;->u16V_CapStart:I

    const-string v1, "u8HCrop_Left"

    invoke-interface {v7, v1}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v1

    invoke-interface {v7, v1}, Landroid/database/Cursor;->getInt(I)I

    move-result v1

    int-to-short v1, v1

    iput-short v1, v13, Lcom/android/server/tv/DataBaseDesk$ST_MAPI_VIDEO_WINDOW_INFO;->u8HCrop_Left:S

    const-string v1, "u8HCrop_Right"

    invoke-interface {v7, v1}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v1

    invoke-interface {v7, v1}, Landroid/database/Cursor;->getInt(I)I

    move-result v1

    int-to-short v1, v1

    iput-short v1, v13, Lcom/android/server/tv/DataBaseDesk$ST_MAPI_VIDEO_WINDOW_INFO;->u8HCrop_Right:S

    const-string v1, "u8VCrop_Up"

    invoke-interface {v7, v1}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v1

    invoke-interface {v7, v1}, Landroid/database/Cursor;->getInt(I)I

    move-result v1

    int-to-short v1, v1

    iput-short v1, v13, Lcom/android/server/tv/DataBaseDesk$ST_MAPI_VIDEO_WINDOW_INFO;->u8VCrop_Up:S

    const-string v1, "u8VCrop_Down"

    invoke-interface {v7, v1}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v1

    invoke-interface {v7, v1}, Landroid/database/Cursor;->getInt(I)I

    move-result v1

    int-to-short v1, v1

    iput-short v1, v13, Lcom/android/server/tv/DataBaseDesk$ST_MAPI_VIDEO_WINDOW_INFO;->u8VCrop_Down:S

    :cond_0
    aget-object v1, v25, v12

    aput-object v13, v1, v14

    add-int/lit8 v14, v14, 0x1

    goto :goto_2

    :cond_1
    add-int/lit8 v12, v12, 0x1

    goto :goto_1

    :cond_2
    invoke-interface {v7}, Landroid/database/Cursor;->close()V

    :goto_3
    move-object/from16 v1, v25

    goto/16 :goto_0

    :pswitch_1
    invoke-virtual/range {p0 .. p0}, Lcom/android/server/tv/DataBaseDeskImpl;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v1

    const-string v2, "content://mstar.tv.factory/hdmioverscansetting"

    invoke-static {v2}, Landroid/net/Uri;->parse(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v2

    const/4 v3, 0x0

    const/4 v4, 0x0

    const/4 v5, 0x0

    const-string v6, "ResolutionTypeNum"

    invoke-virtual/range {v1 .. v6}, Landroid/content/ContentResolver;->query(Landroid/net/Uri;[Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;)Landroid/database/Cursor;

    move-result-object v8

    sget-object v1, Lcom/android/server/tv/DataBaseDesk$MAX_HDMI_Resolution_Info;->E_HDMI_MAX:Lcom/android/server/tv/DataBaseDesk$MAX_HDMI_Resolution_Info;

    invoke-virtual {v1}, Lcom/android/server/tv/DataBaseDesk$MAX_HDMI_Resolution_Info;->ordinal()I

    move-result v17

    sget-object v1, Lcom/android/server/tv/DataBaseDesk$MAPI_VIDEO_ARC_Type;->E_AR_MAX:Lcom/android/server/tv/DataBaseDesk$MAPI_VIDEO_ARC_Type;

    invoke-virtual {v1}, Lcom/android/server/tv/DataBaseDesk$MAPI_VIDEO_ARC_Type;->ordinal()I

    move-result v18

    filled-new-array/range {v17 .. v18}, [I

    move-result-object v1

    const-class v2, Lcom/android/server/tv/DataBaseDesk$ST_MAPI_VIDEO_WINDOW_INFO;

    invoke-static {v2, v1}, Ljava/lang/reflect/Array;->newInstance(Ljava/lang/Class;[I)Ljava/lang/Object;

    move-result-object v25

    check-cast v25, [[Lcom/android/server/tv/DataBaseDesk$ST_MAPI_VIDEO_WINDOW_INFO;

    const/4 v12, 0x0

    :goto_4
    move/from16 v0, v17

    if-ge v12, v0, :cond_5

    const/4 v14, 0x0

    :goto_5
    move/from16 v0, v18

    if-ge v14, v0, :cond_4

    new-instance v13, Lcom/android/server/tv/DataBaseDesk$ST_MAPI_VIDEO_WINDOW_INFO;

    invoke-direct {v13}, Lcom/android/server/tv/DataBaseDesk$ST_MAPI_VIDEO_WINDOW_INFO;-><init>()V

    invoke-interface {v8}, Landroid/database/Cursor;->moveToNext()Z

    move-result v1

    if-eqz v1, :cond_3

    const-string v1, "u16H_CapStart"

    invoke-interface {v8, v1}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v1

    invoke-interface {v8, v1}, Landroid/database/Cursor;->getInt(I)I

    move-result v1

    iput v1, v13, Lcom/android/server/tv/DataBaseDesk$ST_MAPI_VIDEO_WINDOW_INFO;->u16H_CapStart:I

    const-string v1, "u16V_CapStart"

    invoke-interface {v8, v1}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v1

    invoke-interface {v8, v1}, Landroid/database/Cursor;->getInt(I)I

    move-result v1

    iput v1, v13, Lcom/android/server/tv/DataBaseDesk$ST_MAPI_VIDEO_WINDOW_INFO;->u16V_CapStart:I

    const-string v1, "u8HCrop_Left"

    invoke-interface {v8, v1}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v1

    invoke-interface {v8, v1}, Landroid/database/Cursor;->getInt(I)I

    move-result v1

    int-to-short v1, v1

    iput-short v1, v13, Lcom/android/server/tv/DataBaseDesk$ST_MAPI_VIDEO_WINDOW_INFO;->u8HCrop_Left:S

    const-string v1, "u8HCrop_Right"

    invoke-interface {v8, v1}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v1

    invoke-interface {v8, v1}, Landroid/database/Cursor;->getInt(I)I

    move-result v1

    int-to-short v1, v1

    iput-short v1, v13, Lcom/android/server/tv/DataBaseDesk$ST_MAPI_VIDEO_WINDOW_INFO;->u8HCrop_Right:S

    const-string v1, "u8VCrop_Up"

    invoke-interface {v8, v1}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v1

    invoke-interface {v8, v1}, Landroid/database/Cursor;->getInt(I)I

    move-result v1

    int-to-short v1, v1

    iput-short v1, v13, Lcom/android/server/tv/DataBaseDesk$ST_MAPI_VIDEO_WINDOW_INFO;->u8VCrop_Up:S

    const-string v1, "u8VCrop_Down"

    invoke-interface {v8, v1}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v1

    invoke-interface {v8, v1}, Landroid/database/Cursor;->getInt(I)I

    move-result v1

    int-to-short v1, v1

    iput-short v1, v13, Lcom/android/server/tv/DataBaseDesk$ST_MAPI_VIDEO_WINDOW_INFO;->u8VCrop_Down:S

    :cond_3
    aget-object v1, v25, v12

    aput-object v13, v1, v14

    add-int/lit8 v14, v14, 0x1

    goto :goto_5

    :cond_4
    add-int/lit8 v12, v12, 0x1

    goto :goto_4

    :cond_5
    invoke-interface {v8}, Landroid/database/Cursor;->close()V

    goto/16 :goto_3

    :pswitch_2
    invoke-virtual/range {p0 .. p0}, Lcom/android/server/tv/DataBaseDeskImpl;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v1

    const-string v2, "content://mstar.tv.factory/ypbproverscansetting"

    invoke-static {v2}, Landroid/net/Uri;->parse(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v2

    const/4 v3, 0x0

    const/4 v4, 0x0

    const/4 v5, 0x0

    const-string v6, "ResolutionTypeNum"

    invoke-virtual/range {v1 .. v6}, Landroid/content/ContentResolver;->query(Landroid/net/Uri;[Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;)Landroid/database/Cursor;

    move-result-object v9

    sget-object v1, Lcom/android/server/tv/DataBaseDesk$MAX_YPbPr_Resolution_Info;->E_YPbPr_MAX:Lcom/android/server/tv/DataBaseDesk$MAX_YPbPr_Resolution_Info;

    invoke-virtual {v1}, Lcom/android/server/tv/DataBaseDesk$MAX_YPbPr_Resolution_Info;->ordinal()I

    move-result v23

    sget-object v1, Lcom/android/server/tv/DataBaseDesk$MAPI_VIDEO_ARC_Type;->E_AR_MAX:Lcom/android/server/tv/DataBaseDesk$MAPI_VIDEO_ARC_Type;

    invoke-virtual {v1}, Lcom/android/server/tv/DataBaseDesk$MAPI_VIDEO_ARC_Type;->ordinal()I

    move-result v24

    filled-new-array/range {v23 .. v24}, [I

    move-result-object v1

    const-class v2, Lcom/android/server/tv/DataBaseDesk$ST_MAPI_VIDEO_WINDOW_INFO;

    invoke-static {v2, v1}, Ljava/lang/reflect/Array;->newInstance(Ljava/lang/Class;[I)Ljava/lang/Object;

    move-result-object v25

    check-cast v25, [[Lcom/android/server/tv/DataBaseDesk$ST_MAPI_VIDEO_WINDOW_INFO;

    const/4 v12, 0x0

    :goto_6
    move/from16 v0, v23

    if-ge v12, v0, :cond_8

    const/4 v14, 0x0

    :goto_7
    move/from16 v0, v24

    if-ge v14, v0, :cond_7

    new-instance v13, Lcom/android/server/tv/DataBaseDesk$ST_MAPI_VIDEO_WINDOW_INFO;

    invoke-direct {v13}, Lcom/android/server/tv/DataBaseDesk$ST_MAPI_VIDEO_WINDOW_INFO;-><init>()V

    invoke-interface {v9}, Landroid/database/Cursor;->moveToNext()Z

    move-result v1

    if-eqz v1, :cond_6

    const-string v1, "u16H_CapStart"

    invoke-interface {v9, v1}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v1

    invoke-interface {v9, v1}, Landroid/database/Cursor;->getInt(I)I

    move-result v1

    iput v1, v13, Lcom/android/server/tv/DataBaseDesk$ST_MAPI_VIDEO_WINDOW_INFO;->u16H_CapStart:I

    const-string v1, "u16V_CapStart"

    invoke-interface {v9, v1}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v1

    invoke-interface {v9, v1}, Landroid/database/Cursor;->getInt(I)I

    move-result v1

    iput v1, v13, Lcom/android/server/tv/DataBaseDesk$ST_MAPI_VIDEO_WINDOW_INFO;->u16V_CapStart:I

    const-string v1, "u8HCrop_Left"

    invoke-interface {v9, v1}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v1

    invoke-interface {v9, v1}, Landroid/database/Cursor;->getInt(I)I

    move-result v1

    int-to-short v1, v1

    iput-short v1, v13, Lcom/android/server/tv/DataBaseDesk$ST_MAPI_VIDEO_WINDOW_INFO;->u8HCrop_Left:S

    const-string v1, "u8HCrop_Right"

    invoke-interface {v9, v1}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v1

    invoke-interface {v9, v1}, Landroid/database/Cursor;->getInt(I)I

    move-result v1

    int-to-short v1, v1

    iput-short v1, v13, Lcom/android/server/tv/DataBaseDesk$ST_MAPI_VIDEO_WINDOW_INFO;->u8HCrop_Right:S

    const-string v1, "u8VCrop_Up"

    invoke-interface {v9, v1}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v1

    invoke-interface {v9, v1}, Landroid/database/Cursor;->getInt(I)I

    move-result v1

    int-to-short v1, v1

    iput-short v1, v13, Lcom/android/server/tv/DataBaseDesk$ST_MAPI_VIDEO_WINDOW_INFO;->u8VCrop_Up:S

    const-string v1, "u8VCrop_Down"

    invoke-interface {v9, v1}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v1

    invoke-interface {v9, v1}, Landroid/database/Cursor;->getInt(I)I

    move-result v1

    int-to-short v1, v1

    iput-short v1, v13, Lcom/android/server/tv/DataBaseDesk$ST_MAPI_VIDEO_WINDOW_INFO;->u8VCrop_Down:S

    :cond_6
    aget-object v1, v25, v12

    aput-object v13, v1, v14

    add-int/lit8 v14, v14, 0x1

    goto :goto_7

    :cond_7
    add-int/lit8 v12, v12, 0x1

    goto :goto_6

    :cond_8
    invoke-interface {v9}, Landroid/database/Cursor;->close()V

    goto/16 :goto_3

    :pswitch_3
    invoke-virtual/range {p0 .. p0}, Lcom/android/server/tv/DataBaseDeskImpl;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v1

    const-string v2, "content://mstar.tv.factory/overscanadjust"

    invoke-static {v2}, Landroid/net/Uri;->parse(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v2

    const/4 v3, 0x0

    const/4 v4, 0x0

    const/4 v5, 0x0

    const-string v6, "FactoryOverScanType"

    invoke-virtual/range {v1 .. v6}, Landroid/content/ContentResolver;->query(Landroid/net/Uri;[Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;)Landroid/database/Cursor;

    move-result-object v10

    sget-object v1, Lcom/android/server/tv/DataBaseDesk$EN_VD_SIGNALTYPE;->SIG_NUMS:Lcom/android/server/tv/DataBaseDesk$EN_VD_SIGNALTYPE;

    invoke-virtual {v1}, Lcom/android/server/tv/DataBaseDesk$EN_VD_SIGNALTYPE;->ordinal()I

    move-result v19

    sget-object v1, Lcom/android/server/tv/DataBaseDesk$MAPI_VIDEO_ARC_Type;->E_AR_MAX:Lcom/android/server/tv/DataBaseDesk$MAPI_VIDEO_ARC_Type;

    invoke-virtual {v1}, Lcom/android/server/tv/DataBaseDesk$MAPI_VIDEO_ARC_Type;->ordinal()I

    move-result v20

    filled-new-array/range {v19 .. v20}, [I

    move-result-object v1

    const-class v2, Lcom/android/server/tv/DataBaseDesk$ST_MAPI_VIDEO_WINDOW_INFO;

    invoke-static {v2, v1}, Ljava/lang/reflect/Array;->newInstance(Ljava/lang/Class;[I)Ljava/lang/Object;

    move-result-object v25

    check-cast v25, [[Lcom/android/server/tv/DataBaseDesk$ST_MAPI_VIDEO_WINDOW_INFO;

    const/4 v12, 0x0

    :goto_8
    move/from16 v0, v19

    if-ge v12, v0, :cond_b

    const/4 v14, 0x0

    :goto_9
    move/from16 v0, v20

    if-ge v14, v0, :cond_a

    new-instance v13, Lcom/android/server/tv/DataBaseDesk$ST_MAPI_VIDEO_WINDOW_INFO;

    invoke-direct {v13}, Lcom/android/server/tv/DataBaseDesk$ST_MAPI_VIDEO_WINDOW_INFO;-><init>()V

    invoke-interface {v10}, Landroid/database/Cursor;->moveToNext()Z

    move-result v1

    if-eqz v1, :cond_9

    const-string v1, "u16H_CapStart"

    invoke-interface {v10, v1}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v1

    invoke-interface {v10, v1}, Landroid/database/Cursor;->getInt(I)I

    move-result v1

    iput v1, v13, Lcom/android/server/tv/DataBaseDesk$ST_MAPI_VIDEO_WINDOW_INFO;->u16H_CapStart:I

    const-string v1, "u16V_CapStart"

    invoke-interface {v10, v1}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v1

    invoke-interface {v10, v1}, Landroid/database/Cursor;->getInt(I)I

    move-result v1

    iput v1, v13, Lcom/android/server/tv/DataBaseDesk$ST_MAPI_VIDEO_WINDOW_INFO;->u16V_CapStart:I

    const-string v1, "u8HCrop_Left"

    invoke-interface {v10, v1}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v1

    invoke-interface {v10, v1}, Landroid/database/Cursor;->getInt(I)I

    move-result v1

    int-to-short v1, v1

    iput-short v1, v13, Lcom/android/server/tv/DataBaseDesk$ST_MAPI_VIDEO_WINDOW_INFO;->u8HCrop_Left:S

    const-string v1, "u8HCrop_Right"

    invoke-interface {v10, v1}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v1

    invoke-interface {v10, v1}, Landroid/database/Cursor;->getInt(I)I

    move-result v1

    int-to-short v1, v1

    iput-short v1, v13, Lcom/android/server/tv/DataBaseDesk$ST_MAPI_VIDEO_WINDOW_INFO;->u8HCrop_Right:S

    const-string v1, "u8VCrop_Up"

    invoke-interface {v10, v1}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v1

    invoke-interface {v10, v1}, Landroid/database/Cursor;->getInt(I)I

    move-result v1

    int-to-short v1, v1

    iput-short v1, v13, Lcom/android/server/tv/DataBaseDesk$ST_MAPI_VIDEO_WINDOW_INFO;->u8VCrop_Up:S

    const-string v1, "u8VCrop_Down"

    invoke-interface {v10, v1}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v1

    invoke-interface {v10, v1}, Landroid/database/Cursor;->getInt(I)I

    move-result v1

    int-to-short v1, v1

    iput-short v1, v13, Lcom/android/server/tv/DataBaseDesk$ST_MAPI_VIDEO_WINDOW_INFO;->u8VCrop_Down:S

    :cond_9
    aget-object v1, v25, v12

    aput-object v13, v1, v14

    add-int/lit8 v14, v14, 0x1

    goto :goto_9

    :cond_a
    add-int/lit8 v12, v12, 0x1

    goto :goto_8

    :cond_b
    invoke-interface {v10}, Landroid/database/Cursor;->close()V

    goto/16 :goto_3

    :pswitch_4
    invoke-virtual/range {p0 .. p0}, Lcom/android/server/tv/DataBaseDeskImpl;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v1

    const-string v2, "content://mstar.tv.factory/overscanadjust"

    invoke-static {v2}, Landroid/net/Uri;->parse(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v2

    const/4 v3, 0x0

    const/4 v4, 0x0

    const/4 v5, 0x0

    const-string v6, "FactoryOverScanType"

    invoke-virtual/range {v1 .. v6}, Landroid/content/ContentResolver;->query(Landroid/net/Uri;[Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;)Landroid/database/Cursor;

    move-result-object v11

    sget-object v1, Lcom/android/server/tv/DataBaseDesk$EN_VD_SIGNALTYPE;->SIG_NUMS:Lcom/android/server/tv/DataBaseDesk$EN_VD_SIGNALTYPE;

    invoke-virtual {v1}, Lcom/android/server/tv/DataBaseDesk$EN_VD_SIGNALTYPE;->ordinal()I

    move-result v21

    sget-object v1, Lcom/android/server/tv/DataBaseDesk$MAPI_VIDEO_ARC_Type;->E_AR_MAX:Lcom/android/server/tv/DataBaseDesk$MAPI_VIDEO_ARC_Type;

    invoke-virtual {v1}, Lcom/android/server/tv/DataBaseDesk$MAPI_VIDEO_ARC_Type;->ordinal()I

    move-result v22

    filled-new-array/range {v21 .. v22}, [I

    move-result-object v1

    const-class v2, Lcom/android/server/tv/DataBaseDesk$ST_MAPI_VIDEO_WINDOW_INFO;

    invoke-static {v2, v1}, Ljava/lang/reflect/Array;->newInstance(Ljava/lang/Class;[I)Ljava/lang/Object;

    move-result-object v25

    check-cast v25, [[Lcom/android/server/tv/DataBaseDesk$ST_MAPI_VIDEO_WINDOW_INFO;

    const/4 v12, 0x0

    :goto_a
    move/from16 v0, v21

    if-ge v12, v0, :cond_e

    const/4 v14, 0x0

    :goto_b
    move/from16 v0, v22

    if-ge v14, v0, :cond_d

    new-instance v13, Lcom/android/server/tv/DataBaseDesk$ST_MAPI_VIDEO_WINDOW_INFO;

    invoke-direct {v13}, Lcom/android/server/tv/DataBaseDesk$ST_MAPI_VIDEO_WINDOW_INFO;-><init>()V

    invoke-interface {v11}, Landroid/database/Cursor;->moveToNext()Z

    move-result v1

    if-eqz v1, :cond_c

    const-string v1, "u16H_CapStart"

    invoke-interface {v11, v1}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v1

    invoke-interface {v11, v1}, Landroid/database/Cursor;->getInt(I)I

    move-result v1

    iput v1, v13, Lcom/android/server/tv/DataBaseDesk$ST_MAPI_VIDEO_WINDOW_INFO;->u16H_CapStart:I

    const-string v1, "u16V_CapStart"

    invoke-interface {v11, v1}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v1

    invoke-interface {v11, v1}, Landroid/database/Cursor;->getInt(I)I

    move-result v1

    iput v1, v13, Lcom/android/server/tv/DataBaseDesk$ST_MAPI_VIDEO_WINDOW_INFO;->u16V_CapStart:I

    const-string v1, "u8HCrop_Left"

    invoke-interface {v11, v1}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v1

    invoke-interface {v11, v1}, Landroid/database/Cursor;->getInt(I)I

    move-result v1

    int-to-short v1, v1

    iput-short v1, v13, Lcom/android/server/tv/DataBaseDesk$ST_MAPI_VIDEO_WINDOW_INFO;->u8HCrop_Left:S

    const-string v1, "u8HCrop_Right"

    invoke-interface {v11, v1}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v1

    invoke-interface {v11, v1}, Landroid/database/Cursor;->getInt(I)I

    move-result v1

    int-to-short v1, v1

    iput-short v1, v13, Lcom/android/server/tv/DataBaseDesk$ST_MAPI_VIDEO_WINDOW_INFO;->u8HCrop_Right:S

    const-string v1, "u8VCrop_Up"

    invoke-interface {v11, v1}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v1

    invoke-interface {v11, v1}, Landroid/database/Cursor;->getInt(I)I

    move-result v1

    int-to-short v1, v1

    iput-short v1, v13, Lcom/android/server/tv/DataBaseDesk$ST_MAPI_VIDEO_WINDOW_INFO;->u8VCrop_Up:S

    const-string v1, "u8VCrop_Down"

    invoke-interface {v11, v1}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v1

    invoke-interface {v11, v1}, Landroid/database/Cursor;->getInt(I)I

    move-result v1

    int-to-short v1, v1

    iput-short v1, v13, Lcom/android/server/tv/DataBaseDesk$ST_MAPI_VIDEO_WINDOW_INFO;->u8VCrop_Down:S

    :cond_c
    aget-object v1, v25, v12

    aput-object v13, v1, v14

    add-int/lit8 v14, v14, 0x1

    goto :goto_b

    :cond_d
    add-int/lit8 v12, v12, 0x1

    goto :goto_a

    :cond_e
    invoke-interface {v11}, Landroid/database/Cursor;->close()V

    goto/16 :goto_3

    :pswitch_data_0
    .packed-switch 0x0
        :pswitch_0
        :pswitch_1
        :pswitch_2
        :pswitch_3
        :pswitch_4
    .end packed-switch
.end method

.method public queryPCClock()I
    .locals 10

    const/4 v2, 0x0

    const/4 v7, 0x0

    const/4 v7, 0x0

    :goto_0
    const/16 v0, 0xa

    if-ge v7, v0, :cond_0

    invoke-virtual {p0, v7}, Lcom/android/server/tv/DataBaseDeskImpl;->queryPCModeIndex(I)I

    move-result v0

    int-to-short v0, v0

    invoke-virtual {p0}, Lcom/android/server/tv/DataBaseDeskImpl;->getVideoInfo()Lcom/mstar/android/tvapi/common/vo/VideoInfo;

    move-result-object v1

    iget v1, v1, Lcom/mstar/android/tvapi/common/vo/VideoInfo;->modeIndex:I

    if-ne v0, v1, :cond_2

    const-string v0, "DataBaseDeskImpl"

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "~~~~~~~~clock id is "

    invoke-virtual {v1, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, v7}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v3, "~~~~~~~~~~~"

    invoke-virtual {v1, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    :cond_0
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v1, "content://mstar.tv.usersetting/userpcmodesetting/"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-static {v7}, Ljava/lang/String;->valueOf(I)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v8

    invoke-virtual {p0}, Lcom/android/server/tv/DataBaseDeskImpl;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v0

    invoke-static {v8}, Landroid/net/Uri;->parse(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v1

    move-object v3, v2

    move-object v4, v2

    move-object v5, v2

    invoke-virtual/range {v0 .. v5}, Landroid/content/ContentResolver;->query(Landroid/net/Uri;[Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;)Landroid/database/Cursor;

    move-result-object v6

    const/4 v9, -0x1

    invoke-interface {v6}, Landroid/database/Cursor;->moveToFirst()Z

    move-result v0

    if-eqz v0, :cond_1

    const-string v0, "u16UI_Clock"

    invoke-interface {v6, v0}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v0

    invoke-interface {v6, v0}, Landroid/database/Cursor;->getInt(I)I

    move-result v9

    :cond_1
    invoke-interface {v6}, Landroid/database/Cursor;->close()V

    return v9

    :cond_2
    add-int/lit8 v7, v7, 0x1

    goto :goto_0
.end method

.method public queryPCHPos()I
    .locals 10

    const/4 v2, 0x0

    const/4 v7, 0x0

    const/4 v7, 0x0

    :goto_0
    const/16 v0, 0xa

    if-ge v7, v0, :cond_0

    invoke-virtual {p0, v7}, Lcom/android/server/tv/DataBaseDeskImpl;->queryPCModeIndex(I)I

    move-result v0

    int-to-short v0, v0

    invoke-virtual {p0}, Lcom/android/server/tv/DataBaseDeskImpl;->getVideoInfo()Lcom/mstar/android/tvapi/common/vo/VideoInfo;

    move-result-object v1

    iget v1, v1, Lcom/mstar/android/tvapi/common/vo/VideoInfo;->modeIndex:I

    if-ne v0, v1, :cond_2

    const-string v0, "DataBaseDeskImpl"

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "~~~~~~~~Hpos id is "

    invoke-virtual {v1, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, v7}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v3, "~~~~~~~~~~~"

    invoke-virtual {v1, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    :cond_0
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v1, "content://mstar.tv.usersetting/userpcmodesetting/"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-static {v7}, Ljava/lang/String;->valueOf(I)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v8

    invoke-virtual {p0}, Lcom/android/server/tv/DataBaseDeskImpl;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v0

    invoke-static {v8}, Landroid/net/Uri;->parse(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v1

    move-object v3, v2

    move-object v4, v2

    move-object v5, v2

    invoke-virtual/range {v0 .. v5}, Landroid/content/ContentResolver;->query(Landroid/net/Uri;[Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;)Landroid/database/Cursor;

    move-result-object v6

    const/4 v9, -0x1

    invoke-interface {v6}, Landroid/database/Cursor;->moveToFirst()Z

    move-result v0

    if-eqz v0, :cond_1

    const-string v0, "u16UI_HorizontalStart"

    invoke-interface {v6, v0}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v0

    invoke-interface {v6, v0}, Landroid/database/Cursor;->getInt(I)I

    move-result v9

    :cond_1
    invoke-interface {v6}, Landroid/database/Cursor;->close()V

    return v9

    :cond_2
    add-int/lit8 v7, v7, 0x1

    goto :goto_0
.end method

.method public queryPCModeIndex(I)I
    .locals 9
    .param p1    # I

    const/4 v2, 0x0

    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v1, "content://mstar.tv.usersetting/userpcmodesetting/"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-static {p1}, Ljava/lang/String;->valueOf(I)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v7

    invoke-virtual {p0}, Lcom/android/server/tv/DataBaseDeskImpl;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v0

    invoke-static {v7}, Landroid/net/Uri;->parse(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v1

    move-object v3, v2

    move-object v4, v2

    move-object v5, v2

    invoke-virtual/range {v0 .. v5}, Landroid/content/ContentResolver;->query(Landroid/net/Uri;[Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;)Landroid/database/Cursor;

    move-result-object v6

    const/4 v8, -0x1

    invoke-interface {v6}, Landroid/database/Cursor;->moveToFirst()Z

    move-result v0

    if-eqz v0, :cond_0

    const-string v0, "u8ModeIndex"

    invoke-interface {v6, v0}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v0

    invoke-interface {v6, v0}, Landroid/database/Cursor;->getInt(I)I

    move-result v8

    :cond_0
    invoke-interface {v6}, Landroid/database/Cursor;->close()V

    return v8
.end method

.method public queryPCPhase()I
    .locals 10

    const/4 v2, 0x0

    const/4 v7, 0x0

    const/4 v7, 0x0

    :goto_0
    const/16 v0, 0xa

    if-ge v7, v0, :cond_0

    const-string v0, "DataBaseDeskImpl"

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "~~~~~~~~ id is :"

    invoke-virtual {v1, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, v7}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v3, "||||||   ModeIndex is:"

    invoke-virtual {v1, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {p0, v7}, Lcom/android/server/tv/DataBaseDeskImpl;->queryPCModeIndex(I)I

    move-result v3

    invoke-virtual {v1, v3}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v3, "~~~~~~~~~~~"

    invoke-virtual {v1, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    invoke-virtual {p0, v7}, Lcom/android/server/tv/DataBaseDeskImpl;->queryPCModeIndex(I)I

    move-result v0

    int-to-short v0, v0

    invoke-virtual {p0}, Lcom/android/server/tv/DataBaseDeskImpl;->getVideoInfo()Lcom/mstar/android/tvapi/common/vo/VideoInfo;

    move-result-object v1

    iget v1, v1, Lcom/mstar/android/tvapi/common/vo/VideoInfo;->modeIndex:I

    if-ne v0, v1, :cond_2

    const-string v0, "DataBaseDeskImpl"

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "~~~~~~~~Phase id is "

    invoke-virtual {v1, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, v7}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v3, "~~~~~~~~~~~"

    invoke-virtual {v1, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    :cond_0
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v1, "content://mstar.tv.usersetting/userpcmodesetting/"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-static {v7}, Ljava/lang/String;->valueOf(I)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v8

    invoke-virtual {p0}, Lcom/android/server/tv/DataBaseDeskImpl;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v0

    invoke-static {v8}, Landroid/net/Uri;->parse(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v1

    move-object v3, v2

    move-object v4, v2

    move-object v5, v2

    invoke-virtual/range {v0 .. v5}, Landroid/content/ContentResolver;->query(Landroid/net/Uri;[Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;)Landroid/database/Cursor;

    move-result-object v6

    const/4 v9, -0x1

    invoke-interface {v6}, Landroid/database/Cursor;->moveToFirst()Z

    move-result v0

    if-eqz v0, :cond_1

    const-string v0, "u16UI_Phase"

    invoke-interface {v6, v0}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v0

    invoke-interface {v6, v0}, Landroid/database/Cursor;->getInt(I)I

    move-result v9

    :cond_1
    invoke-interface {v6}, Landroid/database/Cursor;->close()V

    return v9

    :cond_2
    add-int/lit8 v7, v7, 0x1

    goto/16 :goto_0
.end method

.method public queryPCVPos()I
    .locals 10

    const/4 v2, 0x0

    const/4 v7, 0x0

    const/4 v7, 0x0

    :goto_0
    const/16 v0, 0xa

    if-ge v7, v0, :cond_0

    invoke-virtual {p0, v7}, Lcom/android/server/tv/DataBaseDeskImpl;->queryPCModeIndex(I)I

    move-result v0

    int-to-short v0, v0

    invoke-virtual {p0}, Lcom/android/server/tv/DataBaseDeskImpl;->getVideoInfo()Lcom/mstar/android/tvapi/common/vo/VideoInfo;

    move-result-object v1

    iget v1, v1, Lcom/mstar/android/tvapi/common/vo/VideoInfo;->modeIndex:I

    if-ne v0, v1, :cond_2

    const-string v0, "DataBaseDeskImpl"

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "~~~~~~~~Vpos id is "

    invoke-virtual {v1, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, v7}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v3, "~~~~~~~~~~~"

    invoke-virtual {v1, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    :cond_0
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v1, "content://mstar.tv.usersetting/userpcmodesetting/"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-static {v7}, Ljava/lang/String;->valueOf(I)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v8

    invoke-virtual {p0}, Lcom/android/server/tv/DataBaseDeskImpl;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v0

    invoke-static {v8}, Landroid/net/Uri;->parse(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v1

    move-object v3, v2

    move-object v4, v2

    move-object v5, v2

    invoke-virtual/range {v0 .. v5}, Landroid/content/ContentResolver;->query(Landroid/net/Uri;[Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;)Landroid/database/Cursor;

    move-result-object v6

    const/4 v9, -0x1

    invoke-interface {v6}, Landroid/database/Cursor;->moveToFirst()Z

    move-result v0

    if-eqz v0, :cond_1

    const-string v0, "u16UI_VorizontalStart"

    invoke-interface {v6, v0}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v0

    invoke-interface {v6, v0}, Landroid/database/Cursor;->getInt(I)I

    move-result v9

    :cond_1
    invoke-interface {v6}, Landroid/database/Cursor;->close()V

    return v9

    :cond_2
    add-int/lit8 v7, v7, 0x1

    goto :goto_0
.end method

.method public queryPEQAdjust(I)Lcom/android/server/tv/DataBaseDesk$AUDIO_PEQ_PARAM;
    .locals 8
    .param p1    # I

    const/4 v2, 0x0

    new-instance v7, Lcom/android/server/tv/DataBaseDesk$AUDIO_PEQ_PARAM;

    invoke-direct {v7}, Lcom/android/server/tv/DataBaseDesk$AUDIO_PEQ_PARAM;-><init>()V

    invoke-virtual {p0}, Lcom/android/server/tv/DataBaseDeskImpl;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v0

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "content://mstar.tv.factory/peqadjust/"

    invoke-virtual {v1, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v1}, Landroid/net/Uri;->parse(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v1

    move-object v3, v2

    move-object v4, v2

    move-object v5, v2

    invoke-virtual/range {v0 .. v5}, Landroid/content/ContentResolver;->query(Landroid/net/Uri;[Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;)Landroid/database/Cursor;

    move-result-object v6

    invoke-interface {v6}, Landroid/database/Cursor;->moveToFirst()Z

    move-result v0

    if-eqz v0, :cond_0

    const-string v0, "Band"

    invoke-interface {v6, v0}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v0

    invoke-interface {v6, v0}, Landroid/database/Cursor;->getInt(I)I

    move-result v0

    iput v0, v7, Lcom/android/server/tv/DataBaseDesk$AUDIO_PEQ_PARAM;->Band:I

    const-string v0, "Gain"

    invoke-interface {v6, v0}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v0

    invoke-interface {v6, v0}, Landroid/database/Cursor;->getInt(I)I

    move-result v0

    iput v0, v7, Lcom/android/server/tv/DataBaseDesk$AUDIO_PEQ_PARAM;->Gain:I

    const-string v0, "Foh"

    invoke-interface {v6, v0}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v0

    invoke-interface {v6, v0}, Landroid/database/Cursor;->getInt(I)I

    move-result v0

    iput v0, v7, Lcom/android/server/tv/DataBaseDesk$AUDIO_PEQ_PARAM;->Foh:I

    const-string v0, "Fol"

    invoke-interface {v6, v0}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v0

    invoke-interface {v6, v0}, Landroid/database/Cursor;->getInt(I)I

    move-result v0

    iput v0, v7, Lcom/android/server/tv/DataBaseDesk$AUDIO_PEQ_PARAM;->Fol:I

    const-string v0, "QValue"

    invoke-interface {v6, v0}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v0

    invoke-interface {v6, v0}, Landroid/database/Cursor;->getInt(I)I

    move-result v0

    iput v0, v7, Lcom/android/server/tv/DataBaseDesk$AUDIO_PEQ_PARAM;->QValue:I

    :cond_0
    invoke-interface {v6}, Landroid/database/Cursor;->close()V

    return-object v7
.end method

.method public queryPEQAdjusts()Lcom/android/server/tv/DataBaseDesk$ST_FACTORY_PEQ_SETTING;
    .locals 10

    const/4 v2, 0x0

    invoke-virtual {p0}, Lcom/android/server/tv/DataBaseDeskImpl;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v0

    const-string v1, "content://mstar.tv.factory/peqadjust"

    invoke-static {v1}, Landroid/net/Uri;->parse(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v1

    move-object v3, v2

    move-object v4, v2

    move-object v5, v2

    invoke-virtual/range {v0 .. v5}, Landroid/content/ContentResolver;->query(Landroid/net/Uri;[Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;)Landroid/database/Cursor;

    move-result-object v6

    new-instance v9, Lcom/android/server/tv/DataBaseDesk$ST_FACTORY_PEQ_SETTING;

    invoke-direct {v9}, Lcom/android/server/tv/DataBaseDesk$ST_FACTORY_PEQ_SETTING;-><init>()V

    const/4 v7, 0x0

    iget-object v0, v9, Lcom/android/server/tv/DataBaseDesk$ST_FACTORY_PEQ_SETTING;->stPEQParam:[Lcom/android/server/tv/DataBaseDesk$AUDIO_PEQ_PARAM;

    array-length v8, v0

    :goto_0
    invoke-interface {v6}, Landroid/database/Cursor;->moveToNext()Z

    move-result v0

    if-eqz v0, :cond_0

    add-int/lit8 v0, v8, -0x1

    if-le v7, v0, :cond_1

    :cond_0
    invoke-interface {v6}, Landroid/database/Cursor;->close()V

    return-object v9

    :cond_1
    iget-object v0, v9, Lcom/android/server/tv/DataBaseDesk$ST_FACTORY_PEQ_SETTING;->stPEQParam:[Lcom/android/server/tv/DataBaseDesk$AUDIO_PEQ_PARAM;

    aget-object v0, v0, v7

    const-string v1, "Band"

    invoke-interface {v6, v1}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v1

    invoke-interface {v6, v1}, Landroid/database/Cursor;->getInt(I)I

    move-result v1

    iput v1, v0, Lcom/android/server/tv/DataBaseDesk$AUDIO_PEQ_PARAM;->Band:I

    iget-object v0, v9, Lcom/android/server/tv/DataBaseDesk$ST_FACTORY_PEQ_SETTING;->stPEQParam:[Lcom/android/server/tv/DataBaseDesk$AUDIO_PEQ_PARAM;

    aget-object v0, v0, v7

    const-string v1, "Gain"

    invoke-interface {v6, v1}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v1

    invoke-interface {v6, v1}, Landroid/database/Cursor;->getInt(I)I

    move-result v1

    iput v1, v0, Lcom/android/server/tv/DataBaseDesk$AUDIO_PEQ_PARAM;->Gain:I

    iget-object v0, v9, Lcom/android/server/tv/DataBaseDesk$ST_FACTORY_PEQ_SETTING;->stPEQParam:[Lcom/android/server/tv/DataBaseDesk$AUDIO_PEQ_PARAM;

    aget-object v0, v0, v7

    const-string v1, "Foh"

    invoke-interface {v6, v1}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v1

    invoke-interface {v6, v1}, Landroid/database/Cursor;->getInt(I)I

    move-result v1

    iput v1, v0, Lcom/android/server/tv/DataBaseDesk$AUDIO_PEQ_PARAM;->Foh:I

    iget-object v0, v9, Lcom/android/server/tv/DataBaseDesk$ST_FACTORY_PEQ_SETTING;->stPEQParam:[Lcom/android/server/tv/DataBaseDesk$AUDIO_PEQ_PARAM;

    aget-object v0, v0, v7

    const-string v1, "Fol"

    invoke-interface {v6, v1}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v1

    invoke-interface {v6, v1}, Landroid/database/Cursor;->getInt(I)I

    move-result v1

    iput v1, v0, Lcom/android/server/tv/DataBaseDesk$AUDIO_PEQ_PARAM;->Fol:I

    iget-object v0, v9, Lcom/android/server/tv/DataBaseDesk$ST_FACTORY_PEQ_SETTING;->stPEQParam:[Lcom/android/server/tv/DataBaseDesk$AUDIO_PEQ_PARAM;

    aget-object v0, v0, v7

    const-string v1, "QValue"

    invoke-interface {v6, v1}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v1

    invoke-interface {v6, v1}, Landroid/database/Cursor;->getInt(I)I

    move-result v1

    iput v1, v0, Lcom/android/server/tv/DataBaseDesk$AUDIO_PEQ_PARAM;->QValue:I

    add-int/lit8 v7, v7, 0x1

    goto :goto_0
.end method

.method public queryPicModeSetting(Lcom/android/server/tv/DataBaseDesk$EN_MS_VIDEOITEM;II)I
    .locals 8
    .param p1    # Lcom/android/server/tv/DataBaseDesk$EN_MS_VIDEOITEM;
    .param p2    # I
    .param p3    # I

    const/4 v2, 0x0

    invoke-virtual {p0}, Lcom/android/server/tv/DataBaseDeskImpl;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v0

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    iget-object v3, p0, Lcom/android/server/tv/DataBaseDeskImpl;->userSettingCusSchema:Ljava/lang/String;

    invoke-virtual {v1, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v3, "/picmode_setting/inputsrc/"

    invoke-virtual {v1, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v3, "/picmode/"

    invoke-virtual {v1, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p3}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v1}, Landroid/net/Uri;->parse(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v1

    move-object v3, v2

    move-object v4, v2

    move-object v5, v2

    invoke-virtual/range {v0 .. v5}, Landroid/content/ContentResolver;->query(Landroid/net/Uri;[Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;)Landroid/database/Cursor;

    move-result-object v6

    invoke-interface {v6}, Landroid/database/Cursor;->moveToFirst()Z

    const/4 v7, 0x0

    sget-object v0, Lcom/android/server/tv/DataBaseDeskImpl$1;->$SwitchMap$com$android$server$tv$DataBaseDesk$EN_MS_VIDEOITEM:[I

    invoke-virtual {p1}, Lcom/android/server/tv/DataBaseDesk$EN_MS_VIDEOITEM;->ordinal()I

    move-result v1

    aget v0, v0, v1

    packed-switch v0, :pswitch_data_0

    :goto_0
    invoke-interface {v6}, Landroid/database/Cursor;->close()V

    return v7

    :pswitch_0
    const-string v0, "u8Brightness"

    invoke-interface {v6, v0}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v0

    invoke-interface {v6, v0}, Landroid/database/Cursor;->getInt(I)I

    move-result v7

    goto :goto_0

    :pswitch_1
    const-string v0, "u8Contrast"

    invoke-interface {v6, v0}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v0

    invoke-interface {v6, v0}, Landroid/database/Cursor;->getInt(I)I

    move-result v7

    goto :goto_0

    :pswitch_2
    const-string v0, "u8Hue"

    invoke-interface {v6, v0}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v0

    invoke-interface {v6, v0}, Landroid/database/Cursor;->getInt(I)I

    move-result v7

    goto :goto_0

    :pswitch_3
    const-string v0, "u8Saturation"

    invoke-interface {v6, v0}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v0

    invoke-interface {v6, v0}, Landroid/database/Cursor;->getInt(I)I

    move-result v7

    goto :goto_0

    :pswitch_4
    const-string v0, "u8Sharpness"

    invoke-interface {v6, v0}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v0

    invoke-interface {v6, v0}, Landroid/database/Cursor;->getInt(I)I

    move-result v7

    goto :goto_0

    :pswitch_5
    const-string v0, "u8Backlight"

    invoke-interface {v6, v0}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v0

    invoke-interface {v6, v0}, Landroid/database/Cursor;->getInt(I)I

    move-result v7

    goto :goto_0

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
        :pswitch_1
        :pswitch_2
        :pswitch_3
        :pswitch_4
        :pswitch_5
    .end packed-switch
.end method

.method public queryPictureMode(I)I
    .locals 8
    .param p1    # I

    const/4 v2, 0x0

    const/4 v7, -0x1

    invoke-virtual {p0}, Lcom/android/server/tv/DataBaseDeskImpl;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v0

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "content://mstar.tv.usersetting/videosetting/inputsrc/"

    invoke-virtual {v1, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v1}, Landroid/net/Uri;->parse(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v1

    move-object v3, v2

    move-object v4, v2

    move-object v5, v2

    invoke-virtual/range {v0 .. v5}, Landroid/content/ContentResolver;->query(Landroid/net/Uri;[Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;)Landroid/database/Cursor;

    move-result-object v6

    invoke-interface {v6}, Landroid/database/Cursor;->moveToFirst()Z

    move-result v0

    if-eqz v0, :cond_0

    const-string v0, "ePicture"

    invoke-interface {v6, v0}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v0

    invoke-interface {v6, v0}, Landroid/database/Cursor;->getInt(I)I

    move-result v7

    :cond_0
    invoke-interface {v6}, Landroid/database/Cursor;->close()V

    return v7
.end method

.method public queryPictureModeSettings(II)Lcom/android/server/tv/DataBaseDesk$T_MS_PICTURE;
    .locals 13
    .param p1    # I
    .param p2    # I

    new-instance v0, Lcom/android/server/tv/DataBaseDesk$T_MS_PICTURE;

    const/4 v1, 0x0

    const/4 v2, 0x0

    const/4 v3, 0x0

    const/4 v4, 0x0

    const/4 v5, 0x0

    const/4 v6, 0x0

    sget-object v7, Lcom/android/server/tv/DataBaseDesk$EN_MS_COLOR_TEMP;->MS_COLOR_TEMP_USER:Lcom/android/server/tv/DataBaseDesk$EN_MS_COLOR_TEMP;

    sget-object v8, Lcom/android/server/tv/DataBaseDesk$EN_MS_PIC_ADV;->MS_OFF:Lcom/android/server/tv/DataBaseDesk$EN_MS_PIC_ADV;

    sget-object v9, Lcom/android/server/tv/DataBaseDesk$EN_MS_PIC_ADV;->MS_OFF:Lcom/android/server/tv/DataBaseDesk$EN_MS_PIC_ADV;

    sget-object v10, Lcom/android/server/tv/DataBaseDesk$EN_MS_PIC_ADV;->MS_OFF:Lcom/android/server/tv/DataBaseDesk$EN_MS_PIC_ADV;

    sget-object v11, Lcom/android/server/tv/DataBaseDesk$EN_MS_PIC_ADV;->MS_OFF:Lcom/android/server/tv/DataBaseDesk$EN_MS_PIC_ADV;

    invoke-direct/range {v0 .. v11}, Lcom/android/server/tv/DataBaseDesk$T_MS_PICTURE;-><init>(SSSSSSLcom/android/server/tv/DataBaseDesk$EN_MS_COLOR_TEMP;Lcom/android/server/tv/DataBaseDesk$EN_MS_PIC_ADV;Lcom/android/server/tv/DataBaseDesk$EN_MS_PIC_ADV;Lcom/android/server/tv/DataBaseDesk$EN_MS_PIC_ADV;Lcom/android/server/tv/DataBaseDesk$EN_MS_PIC_ADV;)V

    invoke-virtual {p0}, Lcom/android/server/tv/DataBaseDeskImpl;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v1

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    iget-object v3, p0, Lcom/android/server/tv/DataBaseDeskImpl;->userSettingCusSchema:Ljava/lang/String;

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, "/picmode_setting/inputsrc/"

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, p2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, "/picmode/"

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, p1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v2}, Landroid/net/Uri;->parse(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v2

    const/4 v3, 0x0

    const/4 v4, 0x0

    const/4 v5, 0x0

    const/4 v6, 0x0

    invoke-virtual/range {v1 .. v6}, Landroid/content/ContentResolver;->query(Landroid/net/Uri;[Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;)Landroid/database/Cursor;

    move-result-object v12

    invoke-interface {v12}, Landroid/database/Cursor;->moveToFirst()Z

    move-result v1

    if-eqz v1, :cond_0

    const-string v1, "u8Backlight"

    invoke-interface {v12, v1}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v1

    invoke-interface {v12, v1}, Landroid/database/Cursor;->getInt(I)I

    move-result v1

    int-to-short v1, v1

    iput-short v1, v0, Lcom/android/server/tv/DataBaseDesk$T_MS_PICTURE;->backlight:S

    const-string v1, "u8Contrast"

    invoke-interface {v12, v1}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v1

    invoke-interface {v12, v1}, Landroid/database/Cursor;->getInt(I)I

    move-result v1

    int-to-short v1, v1

    iput-short v1, v0, Lcom/android/server/tv/DataBaseDesk$T_MS_PICTURE;->contrast:S

    const-string v1, "u8Brightness"

    invoke-interface {v12, v1}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v1

    invoke-interface {v12, v1}, Landroid/database/Cursor;->getInt(I)I

    move-result v1

    int-to-short v1, v1

    iput-short v1, v0, Lcom/android/server/tv/DataBaseDesk$T_MS_PICTURE;->brightness:S

    const-string v1, "u8Saturation"

    invoke-interface {v12, v1}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v1

    invoke-interface {v12, v1}, Landroid/database/Cursor;->getInt(I)I

    move-result v1

    int-to-short v1, v1

    iput-short v1, v0, Lcom/android/server/tv/DataBaseDesk$T_MS_PICTURE;->saturation:S

    const-string v1, "u8Sharpness"

    invoke-interface {v12, v1}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v1

    invoke-interface {v12, v1}, Landroid/database/Cursor;->getInt(I)I

    move-result v1

    int-to-short v1, v1

    iput-short v1, v0, Lcom/android/server/tv/DataBaseDesk$T_MS_PICTURE;->sharpness:S

    const-string v1, "u8Hue"

    invoke-interface {v12, v1}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v1

    invoke-interface {v12, v1}, Landroid/database/Cursor;->getInt(I)I

    move-result v1

    int-to-short v1, v1

    iput-short v1, v0, Lcom/android/server/tv/DataBaseDesk$T_MS_PICTURE;->hue:S

    :cond_0
    invoke-interface {v12}, Landroid/database/Cursor;->close()V

    return-object v0
.end method

.method public queryPowerOnOffMusic()I
    .locals 8

    const/4 v2, 0x0

    const/4 v7, 0x0

    invoke-virtual {p0}, Lcom/android/server/tv/DataBaseDeskImpl;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v0

    const-string v1, "content://mstar.tv.usersetting/soundsetting"

    invoke-static {v1}, Landroid/net/Uri;->parse(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v1

    move-object v3, v2

    move-object v4, v2

    move-object v5, v2

    invoke-virtual/range {v0 .. v5}, Landroid/content/ContentResolver;->query(Landroid/net/Uri;[Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;)Landroid/database/Cursor;

    move-result-object v6

    invoke-interface {v6}, Landroid/database/Cursor;->moveToFirst()Z

    move-result v0

    if-eqz v0, :cond_0

    const-string v0, "PoweronMusic"

    invoke-interface {v6, v0}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v0

    invoke-interface {v6, v0}, Landroid/database/Cursor;->getInt(I)I

    move-result v7

    :cond_0
    invoke-interface {v6}, Landroid/database/Cursor;->close()V

    return v7
.end method

.method public queryPowerOnSource()I
    .locals 8

    const/4 v2, 0x0

    const/4 v7, 0x0

    invoke-virtual {p0}, Lcom/android/server/tv/DataBaseDeskImpl;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v0

    const-string v1, "content://mstar.tv.usersetting/systemsetting"

    invoke-static {v1}, Landroid/net/Uri;->parse(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v1

    move-object v3, v2

    move-object v4, v2

    move-object v5, v2

    invoke-virtual/range {v0 .. v5}, Landroid/content/ContentResolver;->query(Landroid/net/Uri;[Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;)Landroid/database/Cursor;

    move-result-object v6

    invoke-interface {v6}, Landroid/database/Cursor;->moveToFirst()Z

    move-result v0

    if-eqz v0, :cond_0

    const-string v0, "enInputSourceType"

    invoke-interface {v6, v0}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v0

    invoke-interface {v6, v0}, Landroid/database/Cursor;->getInt(I)I

    move-result v7

    :cond_0
    invoke-interface {v6}, Landroid/database/Cursor;->close()V

    return v7
.end method

.method public querySSCAdjust()Lcom/android/server/tv/DataBaseDesk$MS_FACTORY_SSC_SET;
    .locals 10

    const/4 v9, 0x1

    const/4 v8, 0x0

    const/4 v2, 0x0

    invoke-virtual {p0}, Lcom/android/server/tv/DataBaseDeskImpl;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v0

    const-string v1, "content://mstar.tv.factory/sscadjust"

    invoke-static {v1}, Landroid/net/Uri;->parse(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v1

    move-object v3, v2

    move-object v4, v2

    move-object v5, v2

    invoke-virtual/range {v0 .. v5}, Landroid/content/ContentResolver;->query(Landroid/net/Uri;[Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;)Landroid/database/Cursor;

    move-result-object v6

    new-instance v7, Lcom/android/server/tv/DataBaseDesk$MS_FACTORY_SSC_SET;

    invoke-direct {v7}, Lcom/android/server/tv/DataBaseDesk$MS_FACTORY_SSC_SET;-><init>()V

    invoke-interface {v6}, Landroid/database/Cursor;->moveToFirst()Z

    move-result v0

    if-eqz v0, :cond_0

    const-string v0, "Miu_SscEnable"

    invoke-interface {v6, v0}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v0

    invoke-interface {v6, v0}, Landroid/database/Cursor;->getInt(I)I

    move-result v0

    if-nez v0, :cond_1

    move v0, v8

    :goto_0
    iput-boolean v0, v7, Lcom/android/server/tv/DataBaseDesk$MS_FACTORY_SSC_SET;->Miu_SscEnable:Z

    const-string v0, "Lvds_SscEnable"

    invoke-interface {v6, v0}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v0

    invoke-interface {v6, v0}, Landroid/database/Cursor;->getInt(I)I

    move-result v0

    if-nez v0, :cond_2

    :goto_1
    iput-boolean v8, v7, Lcom/android/server/tv/DataBaseDesk$MS_FACTORY_SSC_SET;->Lvds_SscEnable:Z

    const-string v0, "Lvds_SscSpan"

    invoke-interface {v6, v0}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v0

    invoke-interface {v6, v0}, Landroid/database/Cursor;->getInt(I)I

    move-result v0

    iput v0, v7, Lcom/android/server/tv/DataBaseDesk$MS_FACTORY_SSC_SET;->Lvds_SscSpan:I

    const-string v0, "Lvds_SscStep"

    invoke-interface {v6, v0}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v0

    invoke-interface {v6, v0}, Landroid/database/Cursor;->getInt(I)I

    move-result v0

    iput v0, v7, Lcom/android/server/tv/DataBaseDesk$MS_FACTORY_SSC_SET;->Lvds_SscStep:I

    const-string v0, "Miu_SscSpan"

    invoke-interface {v6, v0}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v0

    invoke-interface {v6, v0}, Landroid/database/Cursor;->getInt(I)I

    move-result v0

    iput v0, v7, Lcom/android/server/tv/DataBaseDesk$MS_FACTORY_SSC_SET;->Miu0_SscSpan:I

    const-string v0, "Miu_SscStep"

    invoke-interface {v6, v0}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v0

    invoke-interface {v6, v0}, Landroid/database/Cursor;->getInt(I)I

    move-result v0

    iput v0, v7, Lcom/android/server/tv/DataBaseDesk$MS_FACTORY_SSC_SET;->Miu0_SscStep:I

    const-string v0, "Miu1_SscSpan"

    invoke-interface {v6, v0}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v0

    invoke-interface {v6, v0}, Landroid/database/Cursor;->getInt(I)I

    move-result v0

    iput v0, v7, Lcom/android/server/tv/DataBaseDesk$MS_FACTORY_SSC_SET;->Miu1_SscSpan:I

    const-string v0, "Miu1_SscStep"

    invoke-interface {v6, v0}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v0

    invoke-interface {v6, v0}, Landroid/database/Cursor;->getInt(I)I

    move-result v0

    iput v0, v7, Lcom/android/server/tv/DataBaseDesk$MS_FACTORY_SSC_SET;->Miu1_SscStep:I

    const-string v0, "Miu2_SscSpan"

    invoke-interface {v6, v0}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v0

    invoke-interface {v6, v0}, Landroid/database/Cursor;->getInt(I)I

    move-result v0

    iput v0, v7, Lcom/android/server/tv/DataBaseDesk$MS_FACTORY_SSC_SET;->Miu2_SscSpan:I

    const-string v0, "Miu2_SscStep"

    invoke-interface {v6, v0}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v0

    invoke-interface {v6, v0}, Landroid/database/Cursor;->getInt(I)I

    move-result v0

    iput v0, v7, Lcom/android/server/tv/DataBaseDesk$MS_FACTORY_SSC_SET;->Miu2_SscStep:I

    :cond_0
    invoke-interface {v6}, Landroid/database/Cursor;->close()V

    return-object v7

    :cond_1
    move v0, v9

    goto :goto_0

    :cond_2
    move v8, v9

    goto :goto_1
.end method

.method public querySelfAdaptiveLevel(I)I
    .locals 1
    .param p1    # I

    const/4 v0, 0x0

    return v0
.end method

.method public querySeparateHear()I
    .locals 8

    const/4 v2, 0x0

    const/4 v7, 0x0

    invoke-virtual {p0}, Lcom/android/server/tv/DataBaseDeskImpl;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v0

    const-string v1, "content://mstar.tv.usersetting/soundsetting"

    invoke-static {v1}, Landroid/net/Uri;->parse(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v1

    move-object v3, v2

    move-object v4, v2

    move-object v5, v2

    invoke-virtual/range {v0 .. v5}, Landroid/content/ContentResolver;->query(Landroid/net/Uri;[Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;)Landroid/database/Cursor;

    move-result-object v6

    invoke-interface {v6}, Landroid/database/Cursor;->moveToFirst()Z

    move-result v0

    if-eqz v0, :cond_0

    const-string v0, "SeparateHearing"

    invoke-interface {v6, v0}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v0

    invoke-interface {v6, v0}, Landroid/database/Cursor;->getInt(I)I

    move-result v7

    :cond_0
    invoke-interface {v6}, Landroid/database/Cursor;->close()V

    return v7
.end method

.method public queryServiceName(S)Ljava/lang/String;
    .locals 8
    .param p1    # S

    const/4 v2, 0x0

    invoke-virtual {p0}, Lcom/android/server/tv/DataBaseDeskImpl;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v0

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "content://mstar.tv.usersetting/epgtimer/"

    invoke-virtual {v1, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v1}, Landroid/net/Uri;->parse(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v1

    move-object v3, v2

    move-object v4, v2

    move-object v5, v2

    invoke-virtual/range {v0 .. v5}, Landroid/content/ContentResolver;->query(Landroid/net/Uri;[Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;)Landroid/database/Cursor;

    move-result-object v6

    invoke-interface {v6}, Landroid/database/Cursor;->moveToFirst()Z

    new-instance v7, Ljava/lang/String;

    invoke-direct {v7}, Ljava/lang/String;-><init>()V

    const-string v0, "sServiceName"

    invoke-interface {v6, v0}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v0

    invoke-interface {v6, v0}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v7

    sget-object v0, Ljava/lang/System;->out:Ljava/io/PrintStream;

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "\n=====>>serviceName "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, " @epgTimerIndex "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/io/PrintStream;->println(Ljava/lang/String;)V

    invoke-interface {v6}, Landroid/database/Cursor;->close()V

    return-object v7
.end method

.method public querySoundMode()I
    .locals 8

    const/4 v2, 0x0

    invoke-virtual {p0}, Lcom/android/server/tv/DataBaseDeskImpl;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v0

    const-string v1, "content://mstar.tv.usersetting/soundsetting"

    invoke-static {v1}, Landroid/net/Uri;->parse(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v1

    move-object v3, v2

    move-object v4, v2

    move-object v5, v2

    invoke-virtual/range {v0 .. v5}, Landroid/content/ContentResolver;->query(Landroid/net/Uri;[Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;)Landroid/database/Cursor;

    move-result-object v6

    const/4 v7, 0x0

    invoke-interface {v6}, Landroid/database/Cursor;->moveToFirst()Z

    move-result v0

    if-eqz v0, :cond_0

    const-string v0, "SoundMode"

    invoke-interface {v6, v0}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v0

    invoke-interface {v6, v0}, Landroid/database/Cursor;->getInt(I)I

    move-result v7

    :cond_0
    invoke-interface {v6}, Landroid/database/Cursor;->close()V

    return v7
.end method

.method public querySoundModeSetting(I)Lcom/android/server/tv/DataBaseDesk$SoundModeSeting;
    .locals 10
    .param p1    # I

    const/4 v9, 0x0

    const/4 v1, 0x0

    new-instance v0, Lcom/android/server/tv/DataBaseDesk$SoundModeSeting;

    move v2, v1

    move v3, v1

    move v4, v1

    move v5, v1

    move v6, v1

    move v7, v1

    invoke-direct/range {v0 .. v7}, Lcom/android/server/tv/DataBaseDesk$SoundModeSeting;-><init>(SSSSSSS)V

    invoke-virtual {p0}, Lcom/android/server/tv/DataBaseDeskImpl;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v2

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    iget-object v4, p0, Lcom/android/server/tv/DataBaseDeskImpl;->userSettingCusSchema:Ljava/lang/String;

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    const-string v4, "/soundmodesetting/"

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3, p1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v3}, Landroid/net/Uri;->parse(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v3

    move-object v4, v9

    move-object v5, v9

    move-object v6, v9

    move-object v7, v9

    invoke-virtual/range {v2 .. v7}, Landroid/content/ContentResolver;->query(Landroid/net/Uri;[Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;)Landroid/database/Cursor;

    move-result-object v8

    :goto_0
    invoke-interface {v8}, Landroid/database/Cursor;->moveToNext()Z

    move-result v2

    if-eqz v2, :cond_1

    const-string v2, "Bass"

    invoke-interface {v8, v2}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v2

    invoke-interface {v8, v2}, Landroid/database/Cursor;->getInt(I)I

    move-result v2

    int-to-short v2, v2

    iput-short v2, v0, Lcom/android/server/tv/DataBaseDesk$SoundModeSeting;->Bass:S

    const-string v2, "Treble"

    invoke-interface {v8, v2}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v2

    invoke-interface {v8, v2}, Landroid/database/Cursor;->getInt(I)I

    move-result v2

    int-to-short v2, v2

    iput-short v2, v0, Lcom/android/server/tv/DataBaseDesk$SoundModeSeting;->Treble:S

    const-string v2, "EqBand1"

    invoke-interface {v8, v2}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v2

    invoke-interface {v8, v2}, Landroid/database/Cursor;->getInt(I)I

    move-result v2

    int-to-short v2, v2

    iput-short v2, v0, Lcom/android/server/tv/DataBaseDesk$SoundModeSeting;->EqBand1:S

    const-string v2, "EqBand2"

    invoke-interface {v8, v2}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v2

    invoke-interface {v8, v2}, Landroid/database/Cursor;->getInt(I)I

    move-result v2

    int-to-short v2, v2

    iput-short v2, v0, Lcom/android/server/tv/DataBaseDesk$SoundModeSeting;->EqBand2:S

    const-string v2, "EqBand3"

    invoke-interface {v8, v2}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v2

    invoke-interface {v8, v2}, Landroid/database/Cursor;->getInt(I)I

    move-result v2

    int-to-short v2, v2

    iput-short v2, v0, Lcom/android/server/tv/DataBaseDesk$SoundModeSeting;->EqBand3:S

    const-string v2, "EqBand4"

    invoke-interface {v8, v2}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v2

    invoke-interface {v8, v2}, Landroid/database/Cursor;->getInt(I)I

    move-result v2

    int-to-short v2, v2

    iput-short v2, v0, Lcom/android/server/tv/DataBaseDesk$SoundModeSeting;->EqBand4:S

    const-string v2, "EqBand5"

    invoke-interface {v8, v2}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v2

    invoke-interface {v8, v2}, Landroid/database/Cursor;->getInt(I)I

    move-result v2

    int-to-short v2, v2

    iput-short v2, v0, Lcom/android/server/tv/DataBaseDesk$SoundModeSeting;->EqBand5:S

    const-string v2, "EqBand6"

    invoke-interface {v8, v2}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v2

    invoke-interface {v8, v2}, Landroid/database/Cursor;->getInt(I)I

    move-result v2

    int-to-short v2, v2

    iput-short v2, v0, Lcom/android/server/tv/DataBaseDesk$SoundModeSeting;->EqBand6:S

    const-string v2, "EqBand7"

    invoke-interface {v8, v2}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v2

    invoke-interface {v8, v2}, Landroid/database/Cursor;->getInt(I)I

    move-result v2

    int-to-short v2, v2

    iput-short v2, v0, Lcom/android/server/tv/DataBaseDesk$SoundModeSeting;->EqBand7:S

    const-string v2, "UserMode"

    invoke-interface {v8, v2}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v2

    invoke-interface {v8, v2}, Landroid/database/Cursor;->getInt(I)I

    move-result v2

    if-nez v2, :cond_0

    move v2, v1

    :goto_1
    iput-boolean v2, v0, Lcom/android/server/tv/DataBaseDesk$SoundModeSeting;->UserMode:Z

    const-string v2, "Balance"

    invoke-interface {v8, v2}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v2

    invoke-interface {v8, v2}, Landroid/database/Cursor;->getInt(I)I

    move-result v2

    int-to-short v2, v2

    iput-short v2, v0, Lcom/android/server/tv/DataBaseDesk$SoundModeSeting;->Balance:S

    invoke-static {}, Lcom/android/server/tv/DataBaseDesk$EN_AUD_MODE;->values()[Lcom/android/server/tv/DataBaseDesk$EN_AUD_MODE;

    move-result-object v2

    const-string v3, "enSoundAudioChannel"

    invoke-interface {v8, v3}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v3

    invoke-interface {v8, v3}, Landroid/database/Cursor;->getInt(I)I

    move-result v3

    aget-object v2, v2, v3

    iput-object v2, v0, Lcom/android/server/tv/DataBaseDesk$SoundModeSeting;->enSoundAudioChannel:Lcom/android/server/tv/DataBaseDesk$EN_AUD_MODE;

    goto/16 :goto_0

    :cond_0
    const/4 v2, 0x1

    goto :goto_1

    :cond_1
    invoke-interface {v8}, Landroid/database/Cursor;->close()V

    return-object v0
.end method

.method public querySoundModeSettings()[Lcom/android/server/tv/DataBaseDesk$SoundModeSeting;
    .locals 9

    const/4 v2, 0x0

    invoke-virtual {p0}, Lcom/android/server/tv/DataBaseDeskImpl;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v0

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    iget-object v3, p0, Lcom/android/server/tv/DataBaseDeskImpl;->userSettingCusSchema:Ljava/lang/String;

    invoke-virtual {v1, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v3, "/soundmodesetting"

    invoke-virtual {v1, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v1}, Landroid/net/Uri;->parse(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v1

    move-object v3, v2

    move-object v4, v2

    move-object v5, v2

    invoke-virtual/range {v0 .. v5}, Landroid/content/ContentResolver;->query(Landroid/net/Uri;[Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;)Landroid/database/Cursor;

    move-result-object v6

    const/4 v7, 0x0

    iget-object v0, p0, Lcom/android/server/tv/DataBaseDeskImpl;->astSoundModeSetting:[Lcom/android/server/tv/DataBaseDesk$SoundModeSeting;

    array-length v8, v0

    :goto_0
    invoke-interface {v6}, Landroid/database/Cursor;->moveToNext()Z

    move-result v0

    if-eqz v0, :cond_0

    add-int/lit8 v0, v8, -0x1

    if-le v7, v0, :cond_1

    :cond_0
    invoke-interface {v6}, Landroid/database/Cursor;->close()V

    iget-object v0, p0, Lcom/android/server/tv/DataBaseDeskImpl;->astSoundModeSetting:[Lcom/android/server/tv/DataBaseDesk$SoundModeSeting;

    return-object v0

    :cond_1
    iget-object v0, p0, Lcom/android/server/tv/DataBaseDeskImpl;->astSoundModeSetting:[Lcom/android/server/tv/DataBaseDesk$SoundModeSeting;

    aget-object v0, v0, v7

    const-string v1, "Bass"

    invoke-interface {v6, v1}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v1

    invoke-interface {v6, v1}, Landroid/database/Cursor;->getInt(I)I

    move-result v1

    int-to-short v1, v1

    iput-short v1, v0, Lcom/android/server/tv/DataBaseDesk$SoundModeSeting;->Bass:S

    iget-object v0, p0, Lcom/android/server/tv/DataBaseDeskImpl;->astSoundModeSetting:[Lcom/android/server/tv/DataBaseDesk$SoundModeSeting;

    aget-object v0, v0, v7

    const-string v1, "Treble"

    invoke-interface {v6, v1}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v1

    invoke-interface {v6, v1}, Landroid/database/Cursor;->getInt(I)I

    move-result v1

    int-to-short v1, v1

    iput-short v1, v0, Lcom/android/server/tv/DataBaseDesk$SoundModeSeting;->Treble:S

    iget-object v0, p0, Lcom/android/server/tv/DataBaseDeskImpl;->astSoundModeSetting:[Lcom/android/server/tv/DataBaseDesk$SoundModeSeting;

    aget-object v0, v0, v7

    const-string v1, "EqBand1"

    invoke-interface {v6, v1}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v1

    invoke-interface {v6, v1}, Landroid/database/Cursor;->getInt(I)I

    move-result v1

    int-to-short v1, v1

    iput-short v1, v0, Lcom/android/server/tv/DataBaseDesk$SoundModeSeting;->EqBand1:S

    iget-object v0, p0, Lcom/android/server/tv/DataBaseDeskImpl;->astSoundModeSetting:[Lcom/android/server/tv/DataBaseDesk$SoundModeSeting;

    aget-object v0, v0, v7

    const-string v1, "EqBand2"

    invoke-interface {v6, v1}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v1

    invoke-interface {v6, v1}, Landroid/database/Cursor;->getInt(I)I

    move-result v1

    int-to-short v1, v1

    iput-short v1, v0, Lcom/android/server/tv/DataBaseDesk$SoundModeSeting;->EqBand2:S

    iget-object v0, p0, Lcom/android/server/tv/DataBaseDeskImpl;->astSoundModeSetting:[Lcom/android/server/tv/DataBaseDesk$SoundModeSeting;

    aget-object v0, v0, v7

    const-string v1, "EqBand3"

    invoke-interface {v6, v1}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v1

    invoke-interface {v6, v1}, Landroid/database/Cursor;->getInt(I)I

    move-result v1

    int-to-short v1, v1

    iput-short v1, v0, Lcom/android/server/tv/DataBaseDesk$SoundModeSeting;->EqBand3:S

    iget-object v0, p0, Lcom/android/server/tv/DataBaseDeskImpl;->astSoundModeSetting:[Lcom/android/server/tv/DataBaseDesk$SoundModeSeting;

    aget-object v0, v0, v7

    const-string v1, "EqBand4"

    invoke-interface {v6, v1}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v1

    invoke-interface {v6, v1}, Landroid/database/Cursor;->getInt(I)I

    move-result v1

    int-to-short v1, v1

    iput-short v1, v0, Lcom/android/server/tv/DataBaseDesk$SoundModeSeting;->EqBand4:S

    iget-object v0, p0, Lcom/android/server/tv/DataBaseDeskImpl;->astSoundModeSetting:[Lcom/android/server/tv/DataBaseDesk$SoundModeSeting;

    aget-object v0, v0, v7

    const-string v1, "EqBand5"

    invoke-interface {v6, v1}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v1

    invoke-interface {v6, v1}, Landroid/database/Cursor;->getInt(I)I

    move-result v1

    int-to-short v1, v1

    iput-short v1, v0, Lcom/android/server/tv/DataBaseDesk$SoundModeSeting;->EqBand5:S

    iget-object v0, p0, Lcom/android/server/tv/DataBaseDeskImpl;->astSoundModeSetting:[Lcom/android/server/tv/DataBaseDesk$SoundModeSeting;

    aget-object v0, v0, v7

    const-string v1, "EqBand6"

    invoke-interface {v6, v1}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v1

    invoke-interface {v6, v1}, Landroid/database/Cursor;->getInt(I)I

    move-result v1

    int-to-short v1, v1

    iput-short v1, v0, Lcom/android/server/tv/DataBaseDesk$SoundModeSeting;->EqBand6:S

    iget-object v0, p0, Lcom/android/server/tv/DataBaseDeskImpl;->astSoundModeSetting:[Lcom/android/server/tv/DataBaseDesk$SoundModeSeting;

    aget-object v0, v0, v7

    const-string v1, "EqBand7"

    invoke-interface {v6, v1}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v1

    invoke-interface {v6, v1}, Landroid/database/Cursor;->getInt(I)I

    move-result v1

    int-to-short v1, v1

    iput-short v1, v0, Lcom/android/server/tv/DataBaseDesk$SoundModeSeting;->EqBand7:S

    iget-object v0, p0, Lcom/android/server/tv/DataBaseDeskImpl;->astSoundModeSetting:[Lcom/android/server/tv/DataBaseDesk$SoundModeSeting;

    aget-object v1, v0, v7

    const-string v0, "UserMode"

    invoke-interface {v6, v0}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v0

    invoke-interface {v6, v0}, Landroid/database/Cursor;->getInt(I)I

    move-result v0

    if-nez v0, :cond_2

    const/4 v0, 0x0

    :goto_1
    iput-boolean v0, v1, Lcom/android/server/tv/DataBaseDesk$SoundModeSeting;->UserMode:Z

    iget-object v0, p0, Lcom/android/server/tv/DataBaseDeskImpl;->astSoundModeSetting:[Lcom/android/server/tv/DataBaseDesk$SoundModeSeting;

    aget-object v0, v0, v7

    const-string v1, "Balance"

    invoke-interface {v6, v1}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v1

    invoke-interface {v6, v1}, Landroid/database/Cursor;->getInt(I)I

    move-result v1

    int-to-short v1, v1

    iput-short v1, v0, Lcom/android/server/tv/DataBaseDesk$SoundModeSeting;->Balance:S

    iget-object v0, p0, Lcom/android/server/tv/DataBaseDeskImpl;->astSoundModeSetting:[Lcom/android/server/tv/DataBaseDesk$SoundModeSeting;

    aget-object v0, v0, v7

    invoke-static {}, Lcom/android/server/tv/DataBaseDesk$EN_AUD_MODE;->values()[Lcom/android/server/tv/DataBaseDesk$EN_AUD_MODE;

    move-result-object v1

    const-string v2, "enSoundAudioChannel"

    invoke-interface {v6, v2}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v2

    invoke-interface {v6, v2}, Landroid/database/Cursor;->getInt(I)I

    move-result v2

    aget-object v1, v1, v2

    iput-object v1, v0, Lcom/android/server/tv/DataBaseDesk$SoundModeSeting;->enSoundAudioChannel:Lcom/android/server/tv/DataBaseDesk$EN_AUD_MODE;

    add-int/lit8 v7, v7, 0x1

    goto/16 :goto_0

    :cond_2
    const/4 v0, 0x1

    goto :goto_1
.end method

.method public querySoundSetting()Lcom/android/server/tv/DataBaseDesk$MS_USER_SOUND_SETTING;
    .locals 12

    const/4 v11, 0x1

    const/4 v10, 0x0

    const/4 v2, 0x0

    invoke-virtual {p0}, Lcom/android/server/tv/DataBaseDeskImpl;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v0

    const-string v1, "content://mstar.tv.usersetting/soundsetting"

    invoke-static {v1}, Landroid/net/Uri;->parse(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v1

    move-object v3, v2

    move-object v4, v2

    move-object v5, v2

    invoke-virtual/range {v0 .. v5}, Landroid/content/ContentResolver;->query(Landroid/net/Uri;[Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;)Landroid/database/Cursor;

    move-result-object v6

    invoke-interface {v6}, Landroid/database/Cursor;->moveToFirst()Z

    move-result v0

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/android/server/tv/DataBaseDeskImpl;->soundpara:Lcom/android/server/tv/DataBaseDesk$MS_USER_SOUND_SETTING;

    invoke-static {}, Lcom/android/server/tv/DataBaseDesk$EN_SOUND_MODE;->values()[Lcom/android/server/tv/DataBaseDesk$EN_SOUND_MODE;

    move-result-object v1

    const-string v2, "SoundMode"

    invoke-interface {v6, v2}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v2

    invoke-interface {v6, v2}, Landroid/database/Cursor;->getInt(I)I

    move-result v2

    aget-object v1, v1, v2

    iput-object v1, v0, Lcom/android/server/tv/DataBaseDesk$MS_USER_SOUND_SETTING;->SoundMode:Lcom/android/server/tv/DataBaseDesk$EN_SOUND_MODE;

    iget-object v0, p0, Lcom/android/server/tv/DataBaseDeskImpl;->soundpara:Lcom/android/server/tv/DataBaseDesk$MS_USER_SOUND_SETTING;

    invoke-static {}, Lcom/android/server/tv/DataBaseDesk$EN_AUDYSSEY_DYNAMIC_VOLUME_MODE;->values()[Lcom/android/server/tv/DataBaseDesk$EN_AUDYSSEY_DYNAMIC_VOLUME_MODE;

    move-result-object v1

    const-string v2, "AudysseyDynamicVolume"

    invoke-interface {v6, v2}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v2

    invoke-interface {v6, v2}, Landroid/database/Cursor;->getInt(I)I

    move-result v2

    aget-object v1, v1, v2

    iput-object v1, v0, Lcom/android/server/tv/DataBaseDesk$MS_USER_SOUND_SETTING;->AudysseyDynamicVolume:Lcom/android/server/tv/DataBaseDesk$EN_AUDYSSEY_DYNAMIC_VOLUME_MODE;

    iget-object v0, p0, Lcom/android/server/tv/DataBaseDeskImpl;->soundpara:Lcom/android/server/tv/DataBaseDesk$MS_USER_SOUND_SETTING;

    invoke-static {}, Lcom/android/server/tv/DataBaseDesk$EN_AUDYSSEY_EQ_MODE;->values()[Lcom/android/server/tv/DataBaseDesk$EN_AUDYSSEY_EQ_MODE;

    move-result-object v1

    const-string v2, "AudysseyEQ"

    invoke-interface {v6, v2}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v2

    invoke-interface {v6, v2}, Landroid/database/Cursor;->getInt(I)I

    move-result v2

    aget-object v1, v1, v2

    iput-object v1, v0, Lcom/android/server/tv/DataBaseDesk$MS_USER_SOUND_SETTING;->AudysseyEQ:Lcom/android/server/tv/DataBaseDesk$EN_AUDYSSEY_EQ_MODE;

    iget-object v0, p0, Lcom/android/server/tv/DataBaseDeskImpl;->soundpara:Lcom/android/server/tv/DataBaseDesk$MS_USER_SOUND_SETTING;

    invoke-static {}, Lcom/android/server/tv/DataBaseDesk$EN_SURROUND_SYSTEM_TYPE;->values()[Lcom/android/server/tv/DataBaseDesk$EN_SURROUND_SYSTEM_TYPE;

    move-result-object v1

    const-string v2, "SurroundSoundMode"

    invoke-interface {v6, v2}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v2

    invoke-interface {v6, v2}, Landroid/database/Cursor;->getInt(I)I

    move-result v2

    aget-object v1, v1, v2

    iput-object v1, v0, Lcom/android/server/tv/DataBaseDesk$MS_USER_SOUND_SETTING;->SurroundSoundMode:Lcom/android/server/tv/DataBaseDesk$EN_SURROUND_SYSTEM_TYPE;

    iget-object v0, p0, Lcom/android/server/tv/DataBaseDeskImpl;->soundpara:Lcom/android/server/tv/DataBaseDesk$MS_USER_SOUND_SETTING;

    invoke-static {}, Lcom/android/server/tv/DataBaseDesk$EN_SURROUND_MODE;->values()[Lcom/android/server/tv/DataBaseDesk$EN_SURROUND_MODE;

    move-result-object v1

    const-string v2, "Surround"

    invoke-interface {v6, v2}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v2

    invoke-interface {v6, v2}, Landroid/database/Cursor;->getInt(I)I

    move-result v2

    aget-object v1, v1, v2

    iput-object v1, v0, Lcom/android/server/tv/DataBaseDesk$MS_USER_SOUND_SETTING;->SurroundMode:Lcom/android/server/tv/DataBaseDesk$EN_SURROUND_MODE;

    iget-object v1, p0, Lcom/android/server/tv/DataBaseDeskImpl;->soundpara:Lcom/android/server/tv/DataBaseDesk$MS_USER_SOUND_SETTING;

    const-string v0, "bEnableAVC"

    invoke-interface {v6, v0}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v0

    invoke-interface {v6, v0}, Landroid/database/Cursor;->getInt(I)I

    move-result v0

    if-nez v0, :cond_1

    move v0, v10

    :goto_0
    iput-boolean v0, v1, Lcom/android/server/tv/DataBaseDesk$MS_USER_SOUND_SETTING;->bEnableAVC:Z

    iget-object v0, p0, Lcom/android/server/tv/DataBaseDeskImpl;->soundpara:Lcom/android/server/tv/DataBaseDesk$MS_USER_SOUND_SETTING;

    const-string v1, "Volume"

    invoke-interface {v6, v1}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v1

    invoke-interface {v6, v1}, Landroid/database/Cursor;->getInt(I)I

    move-result v1

    int-to-short v1, v1

    iput-short v1, v0, Lcom/android/server/tv/DataBaseDesk$MS_USER_SOUND_SETTING;->Volume:S

    iget-object v0, p0, Lcom/android/server/tv/DataBaseDeskImpl;->soundpara:Lcom/android/server/tv/DataBaseDesk$MS_USER_SOUND_SETTING;

    const-string v1, "HPVolume"

    invoke-interface {v6, v1}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v1

    invoke-interface {v6, v1}, Landroid/database/Cursor;->getInt(I)I

    move-result v1

    int-to-short v1, v1

    iput-short v1, v0, Lcom/android/server/tv/DataBaseDesk$MS_USER_SOUND_SETTING;->HPVolume:S

    iget-object v0, p0, Lcom/android/server/tv/DataBaseDeskImpl;->soundpara:Lcom/android/server/tv/DataBaseDesk$MS_USER_SOUND_SETTING;

    const-string v1, "Balance"

    invoke-interface {v6, v1}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v1

    invoke-interface {v6, v1}, Landroid/database/Cursor;->getInt(I)I

    move-result v1

    int-to-short v1, v1

    iput-short v1, v0, Lcom/android/server/tv/DataBaseDesk$MS_USER_SOUND_SETTING;->Balance:S

    iget-object v0, p0, Lcom/android/server/tv/DataBaseDeskImpl;->soundpara:Lcom/android/server/tv/DataBaseDesk$MS_USER_SOUND_SETTING;

    const-string v1, "Primary_Flag"

    invoke-interface {v6, v1}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v1

    invoke-interface {v6, v1}, Landroid/database/Cursor;->getInt(I)I

    move-result v1

    int-to-short v1, v1

    iput-short v1, v0, Lcom/android/server/tv/DataBaseDesk$MS_USER_SOUND_SETTING;->Primary_Flag:S

    invoke-static {}, Lcom/mstar/android/tvapi/common/vo/TvOsType$EnumLanguage;->values()[Lcom/mstar/android/tvapi/common/vo/TvOsType$EnumLanguage;

    move-result-object v0

    const-string v1, "enSoundAudioLan1"

    invoke-interface {v6, v1}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v1

    invoke-interface {v6, v1}, Landroid/database/Cursor;->getInt(I)I

    move-result v1

    aget-object v7, v0, v1

    invoke-static {}, Lcom/mstar/android/tvapi/common/vo/TvOsType$EnumLanguage;->values()[Lcom/mstar/android/tvapi/common/vo/TvOsType$EnumLanguage;

    move-result-object v0

    const-string v1, "enSoundAudioLan2"

    invoke-interface {v6, v1}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v1

    invoke-interface {v6, v1}, Landroid/database/Cursor;->getInt(I)I

    move-result v1

    aget-object v8, v0, v1

    sget-object v0, Lcom/mstar/android/tvapi/common/vo/TvOsType$EnumLanguage;->E_ENGLISH:Lcom/mstar/android/tvapi/common/vo/TvOsType$EnumLanguage;

    if-ne v7, v0, :cond_2

    iget-object v0, p0, Lcom/android/server/tv/DataBaseDeskImpl;->soundpara:Lcom/android/server/tv/DataBaseDesk$MS_USER_SOUND_SETTING;

    sget-object v1, Lcom/mstar/android/tvapi/common/vo/TvOsType$EnumLanguage;->E_ENGLISH:Lcom/mstar/android/tvapi/common/vo/TvOsType$EnumLanguage;

    iput-object v1, v0, Lcom/android/server/tv/DataBaseDesk$MS_USER_SOUND_SETTING;->enSoundAudioLan1:Lcom/mstar/android/tvapi/common/vo/TvOsType$EnumLanguage;

    :goto_1
    sget-object v0, Lcom/mstar/android/tvapi/common/vo/TvOsType$EnumLanguage;->E_ENGLISH:Lcom/mstar/android/tvapi/common/vo/TvOsType$EnumLanguage;

    if-ne v8, v0, :cond_5

    iget-object v0, p0, Lcom/android/server/tv/DataBaseDeskImpl;->soundpara:Lcom/android/server/tv/DataBaseDesk$MS_USER_SOUND_SETTING;

    sget-object v1, Lcom/mstar/android/tvapi/common/vo/TvOsType$EnumLanguage;->E_ENGLISH:Lcom/mstar/android/tvapi/common/vo/TvOsType$EnumLanguage;

    iput-object v1, v0, Lcom/android/server/tv/DataBaseDesk$MS_USER_SOUND_SETTING;->enSoundAudioLan2:Lcom/mstar/android/tvapi/common/vo/TvOsType$EnumLanguage;

    :goto_2
    iget-object v0, p0, Lcom/android/server/tv/DataBaseDeskImpl;->soundpara:Lcom/android/server/tv/DataBaseDesk$MS_USER_SOUND_SETTING;

    const-string v1, "MUTE_Flag"

    invoke-interface {v6, v1}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v1

    invoke-interface {v6, v1}, Landroid/database/Cursor;->getInt(I)I

    move-result v1

    int-to-short v1, v1

    iput-short v1, v0, Lcom/android/server/tv/DataBaseDesk$MS_USER_SOUND_SETTING;->MUTE_Flag:S

    iget-object v0, p0, Lcom/android/server/tv/DataBaseDeskImpl;->soundpara:Lcom/android/server/tv/DataBaseDesk$MS_USER_SOUND_SETTING;

    invoke-static {}, Lcom/android/server/tv/DataBaseDesk$EN_AUD_MODE;->values()[Lcom/android/server/tv/DataBaseDesk$EN_AUD_MODE;

    move-result-object v1

    const-string v2, "enSoundAudioChannel"

    invoke-interface {v6, v2}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v2

    invoke-interface {v6, v2}, Landroid/database/Cursor;->getInt(I)I

    move-result v2

    aget-object v1, v1, v2

    iput-object v1, v0, Lcom/android/server/tv/DataBaseDesk$MS_USER_SOUND_SETTING;->enSoundAudioChannel:Lcom/android/server/tv/DataBaseDesk$EN_AUD_MODE;

    iget-object v0, p0, Lcom/android/server/tv/DataBaseDeskImpl;->soundpara:Lcom/android/server/tv/DataBaseDesk$MS_USER_SOUND_SETTING;

    const-string v1, "bEnableAD"

    invoke-interface {v6, v1}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v1

    invoke-interface {v6, v1}, Landroid/database/Cursor;->getInt(I)I

    move-result v1

    if-nez v1, :cond_8

    :goto_3
    iput-boolean v10, v0, Lcom/android/server/tv/DataBaseDesk$MS_USER_SOUND_SETTING;->bEnableAD:Z

    iget-object v0, p0, Lcom/android/server/tv/DataBaseDeskImpl;->soundpara:Lcom/android/server/tv/DataBaseDesk$MS_USER_SOUND_SETTING;

    const-string v1, "ADVolume"

    invoke-interface {v6, v1}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v1

    invoke-interface {v6, v1}, Landroid/database/Cursor;->getInt(I)I

    move-result v1

    int-to-short v1, v1

    iput-short v1, v0, Lcom/android/server/tv/DataBaseDesk$MS_USER_SOUND_SETTING;->ADVolume:S

    iget-object v0, p0, Lcom/android/server/tv/DataBaseDeskImpl;->soundpara:Lcom/android/server/tv/DataBaseDesk$MS_USER_SOUND_SETTING;

    invoke-static {}, Lcom/android/server/tv/DataBaseDesk$EN_SOUND_AD_OUTPUT;->values()[Lcom/android/server/tv/DataBaseDesk$EN_SOUND_AD_OUTPUT;

    move-result-object v1

    const-string v2, "ADOutput"

    invoke-interface {v6, v2}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v2

    invoke-interface {v6, v2}, Landroid/database/Cursor;->getInt(I)I

    move-result v2

    aget-object v1, v1, v2

    iput-object v1, v0, Lcom/android/server/tv/DataBaseDesk$MS_USER_SOUND_SETTING;->ADOutput:Lcom/android/server/tv/DataBaseDesk$EN_SOUND_AD_OUTPUT;

    iget-object v0, p0, Lcom/android/server/tv/DataBaseDeskImpl;->soundpara:Lcom/android/server/tv/DataBaseDesk$MS_USER_SOUND_SETTING;

    const-string v1, "SPDIF_Delay"

    invoke-interface {v6, v1}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v1

    invoke-interface {v6, v1}, Landroid/database/Cursor;->getInt(I)I

    move-result v1

    int-to-short v1, v1

    iput-short v1, v0, Lcom/android/server/tv/DataBaseDesk$MS_USER_SOUND_SETTING;->SPDIF_Delay:S

    iget-object v0, p0, Lcom/android/server/tv/DataBaseDeskImpl;->soundpara:Lcom/android/server/tv/DataBaseDesk$MS_USER_SOUND_SETTING;

    const-string v1, "Speaker_Delay"

    invoke-interface {v6, v1}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v1

    invoke-interface {v6, v1}, Landroid/database/Cursor;->getInt(I)I

    move-result v1

    int-to-short v1, v1

    iput-short v1, v0, Lcom/android/server/tv/DataBaseDesk$MS_USER_SOUND_SETTING;->Speaker_Delay:S

    :cond_0
    invoke-interface {v6}, Landroid/database/Cursor;->close()V

    iget-object v0, p0, Lcom/android/server/tv/DataBaseDeskImpl;->soundpara:Lcom/android/server/tv/DataBaseDesk$MS_USER_SOUND_SETTING;

    return-object v0

    :cond_1
    move v0, v11

    goto/16 :goto_0

    :cond_2
    sget-object v0, Lcom/mstar/android/tvapi/common/vo/TvOsType$EnumLanguage;->E_CHINESE:Lcom/mstar/android/tvapi/common/vo/TvOsType$EnumLanguage;

    if-ne v7, v0, :cond_3

    iget-object v0, p0, Lcom/android/server/tv/DataBaseDeskImpl;->soundpara:Lcom/android/server/tv/DataBaseDesk$MS_USER_SOUND_SETTING;

    sget-object v1, Lcom/mstar/android/tvapi/common/vo/TvOsType$EnumLanguage;->E_CHINESE:Lcom/mstar/android/tvapi/common/vo/TvOsType$EnumLanguage;

    iput-object v1, v0, Lcom/android/server/tv/DataBaseDesk$MS_USER_SOUND_SETTING;->enSoundAudioLan1:Lcom/mstar/android/tvapi/common/vo/TvOsType$EnumLanguage;

    goto/16 :goto_1

    :cond_3
    sget-object v0, Lcom/mstar/android/tvapi/common/vo/TvOsType$EnumLanguage;->E_ACHINESE:Lcom/mstar/android/tvapi/common/vo/TvOsType$EnumLanguage;

    if-ne v7, v0, :cond_4

    iget-object v0, p0, Lcom/android/server/tv/DataBaseDeskImpl;->soundpara:Lcom/android/server/tv/DataBaseDesk$MS_USER_SOUND_SETTING;

    sget-object v1, Lcom/mstar/android/tvapi/common/vo/TvOsType$EnumLanguage;->E_ACHINESE:Lcom/mstar/android/tvapi/common/vo/TvOsType$EnumLanguage;

    iput-object v1, v0, Lcom/android/server/tv/DataBaseDesk$MS_USER_SOUND_SETTING;->enSoundAudioLan1:Lcom/mstar/android/tvapi/common/vo/TvOsType$EnumLanguage;

    goto/16 :goto_1

    :cond_4
    iget-object v0, p0, Lcom/android/server/tv/DataBaseDeskImpl;->soundpara:Lcom/android/server/tv/DataBaseDesk$MS_USER_SOUND_SETTING;

    sget-object v1, Lcom/mstar/android/tvapi/common/vo/TvOsType$EnumLanguage;->E_CHINESE:Lcom/mstar/android/tvapi/common/vo/TvOsType$EnumLanguage;

    iput-object v1, v0, Lcom/android/server/tv/DataBaseDesk$MS_USER_SOUND_SETTING;->enSoundAudioLan1:Lcom/mstar/android/tvapi/common/vo/TvOsType$EnumLanguage;

    new-instance v9, Landroid/content/ContentValues;

    invoke-direct {v9}, Landroid/content/ContentValues;-><init>()V

    const-string v0, "enSoundAudioLan1"

    iget-object v1, p0, Lcom/android/server/tv/DataBaseDeskImpl;->soundpara:Lcom/android/server/tv/DataBaseDesk$MS_USER_SOUND_SETTING;

    iget-object v1, v1, Lcom/android/server/tv/DataBaseDesk$MS_USER_SOUND_SETTING;->enSoundAudioLan1:Lcom/mstar/android/tvapi/common/vo/TvOsType$EnumLanguage;

    invoke-virtual {v1}, Lcom/mstar/android/tvapi/common/vo/TvOsType$EnumLanguage;->ordinal()I

    move-result v1

    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    invoke-virtual {v9, v0, v1}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Integer;)V

    :try_start_0
    invoke-virtual {p0}, Lcom/android/server/tv/DataBaseDeskImpl;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v0

    const-string v1, "content://mstar.tv.usersetting/soundsetting"

    invoke-static {v1}, Landroid/net/Uri;->parse(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v1

    const/4 v2, 0x0

    const/4 v3, 0x0

    invoke-virtual {v0, v1, v9, v2, v3}, Landroid/content/ContentResolver;->update(Landroid/net/Uri;Landroid/content/ContentValues;Ljava/lang/String;[Ljava/lang/String;)I
    :try_end_0
    .catch Landroid/database/SQLException; {:try_start_0 .. :try_end_0} :catch_0

    goto/16 :goto_1

    :catch_0
    move-exception v0

    goto/16 :goto_1

    :cond_5
    sget-object v0, Lcom/mstar/android/tvapi/common/vo/TvOsType$EnumLanguage;->E_CHINESE:Lcom/mstar/android/tvapi/common/vo/TvOsType$EnumLanguage;

    if-ne v8, v0, :cond_6

    iget-object v0, p0, Lcom/android/server/tv/DataBaseDeskImpl;->soundpara:Lcom/android/server/tv/DataBaseDesk$MS_USER_SOUND_SETTING;

    sget-object v1, Lcom/mstar/android/tvapi/common/vo/TvOsType$EnumLanguage;->E_CHINESE:Lcom/mstar/android/tvapi/common/vo/TvOsType$EnumLanguage;

    iput-object v1, v0, Lcom/android/server/tv/DataBaseDesk$MS_USER_SOUND_SETTING;->enSoundAudioLan2:Lcom/mstar/android/tvapi/common/vo/TvOsType$EnumLanguage;

    goto/16 :goto_2

    :cond_6
    sget-object v0, Lcom/mstar/android/tvapi/common/vo/TvOsType$EnumLanguage;->E_ACHINESE:Lcom/mstar/android/tvapi/common/vo/TvOsType$EnumLanguage;

    if-ne v8, v0, :cond_7

    iget-object v0, p0, Lcom/android/server/tv/DataBaseDeskImpl;->soundpara:Lcom/android/server/tv/DataBaseDesk$MS_USER_SOUND_SETTING;

    sget-object v1, Lcom/mstar/android/tvapi/common/vo/TvOsType$EnumLanguage;->E_ACHINESE:Lcom/mstar/android/tvapi/common/vo/TvOsType$EnumLanguage;

    iput-object v1, v0, Lcom/android/server/tv/DataBaseDesk$MS_USER_SOUND_SETTING;->enSoundAudioLan2:Lcom/mstar/android/tvapi/common/vo/TvOsType$EnumLanguage;

    goto/16 :goto_2

    :cond_7
    iget-object v0, p0, Lcom/android/server/tv/DataBaseDeskImpl;->soundpara:Lcom/android/server/tv/DataBaseDesk$MS_USER_SOUND_SETTING;

    sget-object v1, Lcom/mstar/android/tvapi/common/vo/TvOsType$EnumLanguage;->E_CHINESE:Lcom/mstar/android/tvapi/common/vo/TvOsType$EnumLanguage;

    iput-object v1, v0, Lcom/android/server/tv/DataBaseDesk$MS_USER_SOUND_SETTING;->enSoundAudioLan2:Lcom/mstar/android/tvapi/common/vo/TvOsType$EnumLanguage;

    new-instance v9, Landroid/content/ContentValues;

    invoke-direct {v9}, Landroid/content/ContentValues;-><init>()V

    const-string v0, "enSoundAudioLan2"

    iget-object v1, p0, Lcom/android/server/tv/DataBaseDeskImpl;->soundpara:Lcom/android/server/tv/DataBaseDesk$MS_USER_SOUND_SETTING;

    iget-object v1, v1, Lcom/android/server/tv/DataBaseDesk$MS_USER_SOUND_SETTING;->enSoundAudioLan2:Lcom/mstar/android/tvapi/common/vo/TvOsType$EnumLanguage;

    invoke-virtual {v1}, Lcom/mstar/android/tvapi/common/vo/TvOsType$EnumLanguage;->ordinal()I

    move-result v1

    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    invoke-virtual {v9, v0, v1}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Integer;)V

    :try_start_1
    invoke-virtual {p0}, Lcom/android/server/tv/DataBaseDeskImpl;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v0

    const-string v1, "content://mstar.tv.usersetting/soundsetting"

    invoke-static {v1}, Landroid/net/Uri;->parse(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v1

    const/4 v2, 0x0

    const/4 v3, 0x0

    invoke-virtual {v0, v1, v9, v2, v3}, Landroid/content/ContentResolver;->update(Landroid/net/Uri;Landroid/content/ContentValues;Ljava/lang/String;[Ljava/lang/String;)I
    :try_end_1
    .catch Landroid/database/SQLException; {:try_start_1 .. :try_end_1} :catch_1

    goto/16 :goto_2

    :catch_1
    move-exception v0

    goto/16 :goto_2

    :cond_8
    move v10, v11

    goto/16 :goto_3
.end method

.method public querySpdifMode()I
    .locals 8

    const/4 v2, 0x0

    const/4 v7, 0x0

    invoke-virtual {p0}, Lcom/android/server/tv/DataBaseDeskImpl;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v0

    const-string v1, "content://mstar.tv.usersetting/systemsetting"

    invoke-static {v1}, Landroid/net/Uri;->parse(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v1

    move-object v3, v2

    move-object v4, v2

    move-object v5, v2

    invoke-virtual/range {v0 .. v5}, Landroid/content/ContentResolver;->query(Landroid/net/Uri;[Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;)Landroid/database/Cursor;

    move-result-object v6

    invoke-interface {v6}, Landroid/database/Cursor;->moveToFirst()Z

    move-result v0

    if-eqz v0, :cond_0

    const-string v0, "enSPDIFMODE"

    invoke-interface {v6, v0}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v0

    invoke-interface {v6, v0}, Landroid/database/Cursor;->getInt(I)I

    move-result v7

    :cond_0
    invoke-interface {v6}, Landroid/database/Cursor;->close()V

    return v7
.end method

.method public querySrr()I
    .locals 8

    const/4 v2, 0x0

    const/4 v7, 0x0

    invoke-virtual {p0}, Lcom/android/server/tv/DataBaseDeskImpl;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v0

    const-string v1, "content://mstar.tv.usersetting/soundsetting"

    invoke-static {v1}, Landroid/net/Uri;->parse(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v1

    move-object v3, v2

    move-object v4, v2

    move-object v5, v2

    invoke-virtual/range {v0 .. v5}, Landroid/content/ContentResolver;->query(Landroid/net/Uri;[Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;)Landroid/database/Cursor;

    move-result-object v6

    :goto_0
    invoke-interface {v6}, Landroid/database/Cursor;->moveToNext()Z

    move-result v0

    if-eqz v0, :cond_0

    const-string v0, "Surround"

    invoke-interface {v6, v0}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v0

    invoke-interface {v6, v0}, Landroid/database/Cursor;->getInt(I)I

    move-result v7

    goto :goto_0

    :cond_0
    invoke-interface {v6}, Landroid/database/Cursor;->close()V

    return v7
.end method

.method public queryThreeDVideoDisplayFormat(I)Lcom/android/server/tv/DataBaseDesk$EN_ThreeD_Video_DISPLAYFORMAT;
    .locals 8
    .param p1    # I

    const/4 v2, 0x0

    sget-object v7, Lcom/android/server/tv/DataBaseDesk$EN_ThreeD_Video_DISPLAYFORMAT;->DB_ThreeD_Video_DISPLAYFORMAT_NONE:Lcom/android/server/tv/DataBaseDesk$EN_ThreeD_Video_DISPLAYFORMAT;

    invoke-virtual {p0}, Lcom/android/server/tv/DataBaseDeskImpl;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v0

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "content://mstar.tv.usersetting/threedvideomode/inputsrc/"

    invoke-virtual {v1, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v1}, Landroid/net/Uri;->parse(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v1

    move-object v3, v2

    move-object v4, v2

    move-object v5, v2

    invoke-virtual/range {v0 .. v5}, Landroid/content/ContentResolver;->query(Landroid/net/Uri;[Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;)Landroid/database/Cursor;

    move-result-object v6

    invoke-interface {v6}, Landroid/database/Cursor;->moveToFirst()Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-static {}, Lcom/android/server/tv/DataBaseDesk$EN_ThreeD_Video_DISPLAYFORMAT;->values()[Lcom/android/server/tv/DataBaseDesk$EN_ThreeD_Video_DISPLAYFORMAT;

    move-result-object v0

    const-string v1, "eThreeDVideoDisplayFormat"

    invoke-interface {v6, v1}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v1

    invoke-interface {v6, v1}, Landroid/database/Cursor;->getInt(I)I

    move-result v1

    aget-object v7, v0, v1

    :cond_0
    invoke-interface {v6}, Landroid/database/Cursor;->close()V

    return-object v7
.end method

.method public queryTreble(I)I
    .locals 8
    .param p1    # I

    const/4 v2, 0x0

    const/4 v7, 0x0

    invoke-virtual {p0}, Lcom/android/server/tv/DataBaseDeskImpl;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v0

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    iget-object v3, p0, Lcom/android/server/tv/DataBaseDeskImpl;->userSettingCusSchema:Ljava/lang/String;

    invoke-virtual {v1, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v3, "/soundmodesetting/"

    invoke-virtual {v1, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v1}, Landroid/net/Uri;->parse(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v1

    move-object v3, v2

    move-object v4, v2

    move-object v5, v2

    invoke-virtual/range {v0 .. v5}, Landroid/content/ContentResolver;->query(Landroid/net/Uri;[Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;)Landroid/database/Cursor;

    move-result-object v6

    :goto_0
    invoke-interface {v6}, Landroid/database/Cursor;->moveToNext()Z

    move-result v0

    if-eqz v0, :cond_0

    const-string v0, "Treble"

    invoke-interface {v6, v0}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v0

    invoke-interface {v6, v0}, Landroid/database/Cursor;->getInt(I)I

    move-result v7

    goto :goto_0

    :cond_0
    invoke-interface {v6}, Landroid/database/Cursor;->close()V

    return v7
.end method

.method public queryTrueBass()I
    .locals 8

    const/4 v2, 0x0

    const/4 v7, 0x0

    invoke-virtual {p0}, Lcom/android/server/tv/DataBaseDeskImpl;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v0

    const-string v1, "content://mstar.tv.usersetting/soundsetting"

    invoke-static {v1}, Landroid/net/Uri;->parse(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v1

    move-object v3, v2

    move-object v4, v2

    move-object v5, v2

    invoke-virtual/range {v0 .. v5}, Landroid/content/ContentResolver;->query(Landroid/net/Uri;[Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;)Landroid/database/Cursor;

    move-result-object v6

    invoke-interface {v6}, Landroid/database/Cursor;->moveToFirst()Z

    move-result v0

    if-eqz v0, :cond_0

    const-string v0, "TrueBass"

    invoke-interface {v6, v0}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v0

    invoke-interface {v6, v0}, Landroid/database/Cursor;->getInt(I)I

    move-result v7

    :cond_0
    invoke-interface {v6}, Landroid/database/Cursor;->close()V

    return v7
.end method

.method public queryUserLocSetting()Lcom/android/server/tv/DataBaseDesk$MS_USER_LOCATION_SETTING;
    .locals 7

    const/4 v2, 0x0

    invoke-virtual {p0}, Lcom/android/server/tv/DataBaseDeskImpl;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v0

    const-string v1, "content://mstar.tv.usersetting/userlocationsetting"

    invoke-static {v1}, Landroid/net/Uri;->parse(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v1

    move-object v3, v2

    move-object v4, v2

    move-object v5, v2

    invoke-virtual/range {v0 .. v5}, Landroid/content/ContentResolver;->query(Landroid/net/Uri;[Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;)Landroid/database/Cursor;

    move-result-object v6

    :goto_0
    invoke-interface {v6}, Landroid/database/Cursor;->moveToNext()Z

    move-result v0

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/android/server/tv/DataBaseDeskImpl;->stUserLocationSetting:Lcom/android/server/tv/DataBaseDesk$MS_USER_LOCATION_SETTING;

    const-string v1, "u16LocationNo"

    invoke-interface {v6, v1}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v1

    invoke-interface {v6, v1}, Landroid/database/Cursor;->getInt(I)I

    move-result v1

    int-to-short v1, v1

    iput v1, v0, Lcom/android/server/tv/DataBaseDesk$MS_USER_LOCATION_SETTING;->mLocationNo:I

    iget-object v0, p0, Lcom/android/server/tv/DataBaseDeskImpl;->stUserLocationSetting:Lcom/android/server/tv/DataBaseDesk$MS_USER_LOCATION_SETTING;

    const-string v1, "s16ManualLongitude"

    invoke-interface {v6, v1}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v1

    invoke-interface {v6, v1}, Landroid/database/Cursor;->getInt(I)I

    move-result v1

    int-to-short v1, v1

    iput v1, v0, Lcom/android/server/tv/DataBaseDesk$MS_USER_LOCATION_SETTING;->mManualLongitude:I

    iget-object v0, p0, Lcom/android/server/tv/DataBaseDeskImpl;->stUserLocationSetting:Lcom/android/server/tv/DataBaseDesk$MS_USER_LOCATION_SETTING;

    const-string v1, "s16ManualLatitude"

    invoke-interface {v6, v1}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v1

    invoke-interface {v6, v1}, Landroid/database/Cursor;->getInt(I)I

    move-result v1

    int-to-short v1, v1

    iput v1, v0, Lcom/android/server/tv/DataBaseDesk$MS_USER_LOCATION_SETTING;->mManualLatitude:I

    goto :goto_0

    :cond_0
    invoke-interface {v6}, Landroid/database/Cursor;->close()V

    iget-object v0, p0, Lcom/android/server/tv/DataBaseDeskImpl;->stUserLocationSetting:Lcom/android/server/tv/DataBaseDesk$MS_USER_LOCATION_SETTING;

    return-object v0
.end method

.method public queryUserSubtitleSetting()Lcom/android/server/tv/DataBaseDesk$MS_USER_SUBTITLE_SETTING;
    .locals 12

    const/4 v11, 0x1

    const/4 v10, 0x0

    const/4 v2, 0x0

    invoke-virtual {p0}, Lcom/android/server/tv/DataBaseDeskImpl;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v0

    const-string v1, "content://mstar.tv.usersetting/subtitlesetting"

    invoke-static {v1}, Landroid/net/Uri;->parse(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v1

    move-object v3, v2

    move-object v4, v2

    move-object v5, v2

    invoke-virtual/range {v0 .. v5}, Landroid/content/ContentResolver;->query(Landroid/net/Uri;[Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;)Landroid/database/Cursor;

    move-result-object v6

    invoke-interface {v6}, Landroid/database/Cursor;->moveToFirst()Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-static {}, Lcom/mstar/android/tvapi/common/vo/TvOsType$EnumLanguage;->values()[Lcom/mstar/android/tvapi/common/vo/TvOsType$EnumLanguage;

    move-result-object v0

    const-string v1, "SubtitleDefaultLanguage"

    invoke-interface {v6, v1}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v1

    invoke-interface {v6, v1}, Landroid/database/Cursor;->getInt(I)I

    move-result v1

    aget-object v7, v0, v1

    invoke-static {}, Lcom/mstar/android/tvapi/common/vo/TvOsType$EnumLanguage;->values()[Lcom/mstar/android/tvapi/common/vo/TvOsType$EnumLanguage;

    move-result-object v0

    const-string v1, "SubtitleDefaultLanguage_2"

    invoke-interface {v6, v1}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v1

    invoke-interface {v6, v1}, Landroid/database/Cursor;->getInt(I)I

    move-result v1

    aget-object v8, v0, v1

    sget-object v0, Lcom/mstar/android/tvapi/common/vo/TvOsType$EnumLanguage;->E_ENGLISH:Lcom/mstar/android/tvapi/common/vo/TvOsType$EnumLanguage;

    if-ne v7, v0, :cond_1

    iget-object v0, p0, Lcom/android/server/tv/DataBaseDeskImpl;->stSubtitleSet:Lcom/android/server/tv/DataBaseDesk$MS_USER_SUBTITLE_SETTING;

    sget-object v1, Lcom/mstar/android/tvapi/common/vo/TvOsType$EnumLanguage;->E_ENGLISH:Lcom/mstar/android/tvapi/common/vo/TvOsType$EnumLanguage;

    iput-object v1, v0, Lcom/android/server/tv/DataBaseDesk$MS_USER_SUBTITLE_SETTING;->SubtitleDefaultLanguage:Lcom/mstar/android/tvapi/common/vo/TvOsType$EnumLanguage;

    :goto_0
    sget-object v0, Lcom/mstar/android/tvapi/common/vo/TvOsType$EnumLanguage;->E_ENGLISH:Lcom/mstar/android/tvapi/common/vo/TvOsType$EnumLanguage;

    if-ne v8, v0, :cond_4

    iget-object v0, p0, Lcom/android/server/tv/DataBaseDeskImpl;->stSubtitleSet:Lcom/android/server/tv/DataBaseDesk$MS_USER_SUBTITLE_SETTING;

    sget-object v1, Lcom/mstar/android/tvapi/common/vo/TvOsType$EnumLanguage;->E_ENGLISH:Lcom/mstar/android/tvapi/common/vo/TvOsType$EnumLanguage;

    iput-object v1, v0, Lcom/android/server/tv/DataBaseDesk$MS_USER_SUBTITLE_SETTING;->SubtitleDefaultLanguage_2:Lcom/mstar/android/tvapi/common/vo/TvOsType$EnumLanguage;

    :goto_1
    iget-object v1, p0, Lcom/android/server/tv/DataBaseDeskImpl;->stSubtitleSet:Lcom/android/server/tv/DataBaseDesk$MS_USER_SUBTITLE_SETTING;

    const-string v0, "fHardOfHearing"

    invoke-interface {v6, v0}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v0

    invoke-interface {v6, v0}, Landroid/database/Cursor;->getInt(I)I

    move-result v0

    if-nez v0, :cond_7

    move v0, v10

    :goto_2
    iput-boolean v0, v1, Lcom/android/server/tv/DataBaseDesk$MS_USER_SUBTITLE_SETTING;->fHardOfHearing:Z

    iget-object v0, p0, Lcom/android/server/tv/DataBaseDeskImpl;->stSubtitleSet:Lcom/android/server/tv/DataBaseDesk$MS_USER_SUBTITLE_SETTING;

    const-string v1, "fEnableSubTitle"

    invoke-interface {v6, v1}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v1

    invoke-interface {v6, v1}, Landroid/database/Cursor;->getInt(I)I

    move-result v1

    if-nez v1, :cond_8

    :goto_3
    iput-boolean v10, v0, Lcom/android/server/tv/DataBaseDesk$MS_USER_SUBTITLE_SETTING;->fEnableSubTitle:Z

    :cond_0
    invoke-interface {v6}, Landroid/database/Cursor;->close()V

    iget-object v0, p0, Lcom/android/server/tv/DataBaseDeskImpl;->stSubtitleSet:Lcom/android/server/tv/DataBaseDesk$MS_USER_SUBTITLE_SETTING;

    return-object v0

    :cond_1
    sget-object v0, Lcom/mstar/android/tvapi/common/vo/TvOsType$EnumLanguage;->E_CHINESE:Lcom/mstar/android/tvapi/common/vo/TvOsType$EnumLanguage;

    if-ne v7, v0, :cond_2

    iget-object v0, p0, Lcom/android/server/tv/DataBaseDeskImpl;->stSubtitleSet:Lcom/android/server/tv/DataBaseDesk$MS_USER_SUBTITLE_SETTING;

    sget-object v1, Lcom/mstar/android/tvapi/common/vo/TvOsType$EnumLanguage;->E_CHINESE:Lcom/mstar/android/tvapi/common/vo/TvOsType$EnumLanguage;

    iput-object v1, v0, Lcom/android/server/tv/DataBaseDesk$MS_USER_SUBTITLE_SETTING;->SubtitleDefaultLanguage:Lcom/mstar/android/tvapi/common/vo/TvOsType$EnumLanguage;

    goto :goto_0

    :cond_2
    sget-object v0, Lcom/mstar/android/tvapi/common/vo/TvOsType$EnumLanguage;->E_ACHINESE:Lcom/mstar/android/tvapi/common/vo/TvOsType$EnumLanguage;

    if-ne v7, v0, :cond_3

    iget-object v0, p0, Lcom/android/server/tv/DataBaseDeskImpl;->stSubtitleSet:Lcom/android/server/tv/DataBaseDesk$MS_USER_SUBTITLE_SETTING;

    sget-object v1, Lcom/mstar/android/tvapi/common/vo/TvOsType$EnumLanguage;->E_ACHINESE:Lcom/mstar/android/tvapi/common/vo/TvOsType$EnumLanguage;

    iput-object v1, v0, Lcom/android/server/tv/DataBaseDesk$MS_USER_SUBTITLE_SETTING;->SubtitleDefaultLanguage:Lcom/mstar/android/tvapi/common/vo/TvOsType$EnumLanguage;

    goto :goto_0

    :cond_3
    iget-object v0, p0, Lcom/android/server/tv/DataBaseDeskImpl;->stSubtitleSet:Lcom/android/server/tv/DataBaseDesk$MS_USER_SUBTITLE_SETTING;

    sget-object v1, Lcom/mstar/android/tvapi/common/vo/TvOsType$EnumLanguage;->E_CHINESE:Lcom/mstar/android/tvapi/common/vo/TvOsType$EnumLanguage;

    iput-object v1, v0, Lcom/android/server/tv/DataBaseDesk$MS_USER_SUBTITLE_SETTING;->SubtitleDefaultLanguage:Lcom/mstar/android/tvapi/common/vo/TvOsType$EnumLanguage;

    new-instance v9, Landroid/content/ContentValues;

    invoke-direct {v9}, Landroid/content/ContentValues;-><init>()V

    const-string v0, "SubtitleDefaultLanguage"

    iget-object v1, p0, Lcom/android/server/tv/DataBaseDeskImpl;->stUsrData:Lcom/android/server/tv/DataBaseDesk$MS_USER_SYSTEM_SETTING;

    iget-object v1, v1, Lcom/android/server/tv/DataBaseDesk$MS_USER_SYSTEM_SETTING;->enLanguage:Lcom/mstar/android/tvapi/common/vo/TvOsType$EnumLanguage;

    invoke-virtual {v1}, Lcom/mstar/android/tvapi/common/vo/TvOsType$EnumLanguage;->ordinal()I

    move-result v1

    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    invoke-virtual {v9, v0, v1}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Integer;)V

    :try_start_0
    invoke-virtual {p0}, Lcom/android/server/tv/DataBaseDeskImpl;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v0

    const-string v1, "content://mstar.tv.usersetting/subtitlesetting"

    invoke-static {v1}, Landroid/net/Uri;->parse(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v1

    const/4 v2, 0x0

    const/4 v3, 0x0

    invoke-virtual {v0, v1, v9, v2, v3}, Landroid/content/ContentResolver;->update(Landroid/net/Uri;Landroid/content/ContentValues;Ljava/lang/String;[Ljava/lang/String;)I
    :try_end_0
    .catch Landroid/database/SQLException; {:try_start_0 .. :try_end_0} :catch_0

    goto :goto_0

    :catch_0
    move-exception v0

    goto :goto_0

    :cond_4
    sget-object v0, Lcom/mstar/android/tvapi/common/vo/TvOsType$EnumLanguage;->E_CHINESE:Lcom/mstar/android/tvapi/common/vo/TvOsType$EnumLanguage;

    if-ne v8, v0, :cond_5

    iget-object v0, p0, Lcom/android/server/tv/DataBaseDeskImpl;->stSubtitleSet:Lcom/android/server/tv/DataBaseDesk$MS_USER_SUBTITLE_SETTING;

    sget-object v1, Lcom/mstar/android/tvapi/common/vo/TvOsType$EnumLanguage;->E_CHINESE:Lcom/mstar/android/tvapi/common/vo/TvOsType$EnumLanguage;

    iput-object v1, v0, Lcom/android/server/tv/DataBaseDesk$MS_USER_SUBTITLE_SETTING;->SubtitleDefaultLanguage_2:Lcom/mstar/android/tvapi/common/vo/TvOsType$EnumLanguage;

    goto :goto_1

    :cond_5
    sget-object v0, Lcom/mstar/android/tvapi/common/vo/TvOsType$EnumLanguage;->E_ACHINESE:Lcom/mstar/android/tvapi/common/vo/TvOsType$EnumLanguage;

    if-ne v8, v0, :cond_6

    iget-object v0, p0, Lcom/android/server/tv/DataBaseDeskImpl;->stSubtitleSet:Lcom/android/server/tv/DataBaseDesk$MS_USER_SUBTITLE_SETTING;

    sget-object v1, Lcom/mstar/android/tvapi/common/vo/TvOsType$EnumLanguage;->E_ACHINESE:Lcom/mstar/android/tvapi/common/vo/TvOsType$EnumLanguage;

    iput-object v1, v0, Lcom/android/server/tv/DataBaseDesk$MS_USER_SUBTITLE_SETTING;->SubtitleDefaultLanguage_2:Lcom/mstar/android/tvapi/common/vo/TvOsType$EnumLanguage;

    goto :goto_1

    :cond_6
    iget-object v0, p0, Lcom/android/server/tv/DataBaseDeskImpl;->stSubtitleSet:Lcom/android/server/tv/DataBaseDesk$MS_USER_SUBTITLE_SETTING;

    sget-object v1, Lcom/mstar/android/tvapi/common/vo/TvOsType$EnumLanguage;->E_CHINESE:Lcom/mstar/android/tvapi/common/vo/TvOsType$EnumLanguage;

    iput-object v1, v0, Lcom/android/server/tv/DataBaseDesk$MS_USER_SUBTITLE_SETTING;->SubtitleDefaultLanguage_2:Lcom/mstar/android/tvapi/common/vo/TvOsType$EnumLanguage;

    new-instance v9, Landroid/content/ContentValues;

    invoke-direct {v9}, Landroid/content/ContentValues;-><init>()V

    const-string v0, "SubtitleDefaultLanguage_2"

    iget-object v1, p0, Lcom/android/server/tv/DataBaseDeskImpl;->stUsrData:Lcom/android/server/tv/DataBaseDesk$MS_USER_SYSTEM_SETTING;

    iget-object v1, v1, Lcom/android/server/tv/DataBaseDesk$MS_USER_SYSTEM_SETTING;->enLanguage:Lcom/mstar/android/tvapi/common/vo/TvOsType$EnumLanguage;

    invoke-virtual {v1}, Lcom/mstar/android/tvapi/common/vo/TvOsType$EnumLanguage;->ordinal()I

    move-result v1

    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    invoke-virtual {v9, v0, v1}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Integer;)V

    :try_start_1
    invoke-virtual {p0}, Lcom/android/server/tv/DataBaseDeskImpl;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v0

    const-string v1, "content://mstar.tv.usersetting/subtitlesetting"

    invoke-static {v1}, Landroid/net/Uri;->parse(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v1

    const/4 v2, 0x0

    const/4 v3, 0x0

    invoke-virtual {v0, v1, v9, v2, v3}, Landroid/content/ContentResolver;->update(Landroid/net/Uri;Landroid/content/ContentValues;Ljava/lang/String;[Ljava/lang/String;)I
    :try_end_1
    .catch Landroid/database/SQLException; {:try_start_1 .. :try_end_1} :catch_1

    goto/16 :goto_1

    :catch_1
    move-exception v0

    goto/16 :goto_1

    :cond_7
    move v0, v11

    goto/16 :goto_2

    :cond_8
    move v10, v11

    goto/16 :goto_3
.end method

.method public queryUserSysSetting(Lcom/android/server/tv/DataBaseDeskImpl$USER_SETTING_FIELD;)I
    .locals 9
    .param p1    # Lcom/android/server/tv/DataBaseDeskImpl$USER_SETTING_FIELD;

    const/4 v2, 0x0

    const-string v7, ""

    sget-object v0, Lcom/android/server/tv/DataBaseDeskImpl$1;->$SwitchMap$com$android$server$tv$DataBaseDeskImpl$USER_SETTING_FIELD:[I

    invoke-virtual {p1}, Lcom/android/server/tv/DataBaseDeskImpl$USER_SETTING_FIELD;->ordinal()I

    move-result v1

    aget v0, v0, v1

    packed-switch v0, :pswitch_data_0

    const/4 v8, -0x1

    :goto_0
    return v8

    :pswitch_0
    const-string v7, "bEnableWDT"

    :goto_1
    invoke-virtual {p0}, Lcom/android/server/tv/DataBaseDeskImpl;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v0

    const-string v1, "content://mstar.tv.usersetting/systemsetting"

    invoke-static {v1}, Landroid/net/Uri;->parse(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v1

    move-object v3, v2

    move-object v4, v2

    move-object v5, v2

    invoke-virtual/range {v0 .. v5}, Landroid/content/ContentResolver;->query(Landroid/net/Uri;[Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;)Landroid/database/Cursor;

    move-result-object v6

    const/4 v8, -0x1

    invoke-interface {v6}, Landroid/database/Cursor;->moveToFirst()Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-interface {v6, v7}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v0

    invoke-interface {v6, v0}, Landroid/database/Cursor;->getInt(I)I

    move-result v8

    :cond_0
    invoke-interface {v6}, Landroid/database/Cursor;->close()V

    goto :goto_0

    :pswitch_1
    const-string v7, "bUartBus"

    goto :goto_1

    nop

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
        :pswitch_1
    .end packed-switch
.end method

.method public queryUserSysSetting()Lcom/android/server/tv/DataBaseDesk$MS_USER_SYSTEM_SETTING;
    .locals 11

    const/4 v2, 0x0

    const/4 v10, 0x1

    const/4 v9, 0x0

    invoke-virtual {p0}, Lcom/android/server/tv/DataBaseDeskImpl;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v0

    const-string v1, "content://mstar.tv.usersetting/systemsetting"

    invoke-static {v1}, Landroid/net/Uri;->parse(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v1

    move-object v3, v2

    move-object v4, v2

    move-object v5, v2

    invoke-virtual/range {v0 .. v5}, Landroid/content/ContentResolver;->query(Landroid/net/Uri;[Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;)Landroid/database/Cursor;

    move-result-object v6

    invoke-interface {v6}, Landroid/database/Cursor;->moveToFirst()Z

    move-result v0

    if-eqz v0, :cond_0

    iget-object v1, p0, Lcom/android/server/tv/DataBaseDeskImpl;->stUsrData:Lcom/android/server/tv/DataBaseDesk$MS_USER_SYSTEM_SETTING;

    const-string v0, "fRunInstallationGuide"

    invoke-interface {v6, v0}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v0

    invoke-interface {v6, v0}, Landroid/database/Cursor;->getInt(I)I

    move-result v0

    if-nez v0, :cond_1

    move v0, v9

    :goto_0
    iput-boolean v0, v1, Lcom/android/server/tv/DataBaseDesk$MS_USER_SYSTEM_SETTING;->fRunInstallationGuide:Z

    iget-object v1, p0, Lcom/android/server/tv/DataBaseDeskImpl;->stUsrData:Lcom/android/server/tv/DataBaseDesk$MS_USER_SYSTEM_SETTING;

    const-string v0, "fNoChannel"

    invoke-interface {v6, v0}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v0

    invoke-interface {v6, v0}, Landroid/database/Cursor;->getInt(I)I

    move-result v0

    if-nez v0, :cond_2

    move v0, v9

    :goto_1
    iput-boolean v0, v1, Lcom/android/server/tv/DataBaseDesk$MS_USER_SYSTEM_SETTING;->fNoChannel:Z

    iget-object v1, p0, Lcom/android/server/tv/DataBaseDeskImpl;->stUsrData:Lcom/android/server/tv/DataBaseDesk$MS_USER_SYSTEM_SETTING;

    const-string v0, "bDisableSiAutoUpdate"

    invoke-interface {v6, v0}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v0

    invoke-interface {v6, v0}, Landroid/database/Cursor;->getInt(I)I

    move-result v0

    if-nez v0, :cond_3

    move v0, v9

    :goto_2
    iput-boolean v0, v1, Lcom/android/server/tv/DataBaseDesk$MS_USER_SYSTEM_SETTING;->bDisableSiAutoUpdate:Z

    iget-object v0, p0, Lcom/android/server/tv/DataBaseDeskImpl;->stUsrData:Lcom/android/server/tv/DataBaseDesk$MS_USER_SYSTEM_SETTING;

    invoke-static {}, Lcom/mstar/android/tvapi/common/vo/TvOsType$EnumInputSource;->values()[Lcom/mstar/android/tvapi/common/vo/TvOsType$EnumInputSource;

    move-result-object v1

    const-string v2, "enInputSourceType"

    invoke-interface {v6, v2}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v2

    invoke-interface {v6, v2}, Landroid/database/Cursor;->getInt(I)I

    move-result v2

    aget-object v1, v1, v2

    iput-object v1, v0, Lcom/android/server/tv/DataBaseDesk$MS_USER_SYSTEM_SETTING;->enInputSourceType:Lcom/mstar/android/tvapi/common/vo/TvOsType$EnumInputSource;

    iget-object v0, p0, Lcom/android/server/tv/DataBaseDeskImpl;->stUsrData:Lcom/android/server/tv/DataBaseDesk$MS_USER_SYSTEM_SETTING;

    invoke-static {}, Lcom/android/server/tv/DataBaseDesk$MEMBER_COUNTRY;->values()[Lcom/android/server/tv/DataBaseDesk$MEMBER_COUNTRY;

    move-result-object v1

    const-string v2, "Country"

    invoke-interface {v6, v2}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v2

    invoke-interface {v6, v2}, Landroid/database/Cursor;->getInt(I)I

    move-result v2

    aget-object v1, v1, v2

    iput-object v1, v0, Lcom/android/server/tv/DataBaseDesk$MS_USER_SYSTEM_SETTING;->Country:Lcom/android/server/tv/DataBaseDesk$MEMBER_COUNTRY;

    iget-object v0, p0, Lcom/android/server/tv/DataBaseDeskImpl;->stUsrData:Lcom/android/server/tv/DataBaseDesk$MS_USER_SYSTEM_SETTING;

    invoke-static {}, Lcom/android/server/tv/DataBaseDesk$EN_CABLE_OPERATORS;->values()[Lcom/android/server/tv/DataBaseDesk$EN_CABLE_OPERATORS;

    move-result-object v1

    const-string v2, "enCableOperators"

    invoke-interface {v6, v2}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v2

    invoke-interface {v6, v2}, Landroid/database/Cursor;->getInt(I)I

    move-result v2

    aget-object v1, v1, v2

    iput-object v1, v0, Lcom/android/server/tv/DataBaseDesk$MS_USER_SYSTEM_SETTING;->enCableOperators:Lcom/android/server/tv/DataBaseDesk$EN_CABLE_OPERATORS;

    iget-object v0, p0, Lcom/android/server/tv/DataBaseDeskImpl;->stUsrData:Lcom/android/server/tv/DataBaseDesk$MS_USER_SYSTEM_SETTING;

    invoke-static {}, Lcom/android/server/tv/DataBaseDesk$EN_SATELLITE_PLATFORM;->values()[Lcom/android/server/tv/DataBaseDesk$EN_SATELLITE_PLATFORM;

    move-result-object v1

    const-string v2, "enSatellitePlatform"

    invoke-interface {v6, v2}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v2

    invoke-interface {v6, v2}, Landroid/database/Cursor;->getInt(I)I

    move-result v2

    aget-object v1, v1, v2

    iput-object v1, v0, Lcom/android/server/tv/DataBaseDesk$MS_USER_SYSTEM_SETTING;->enSatellitePlatform:Lcom/android/server/tv/DataBaseDesk$EN_SATELLITE_PLATFORM;

    invoke-static {}, Lcom/mstar/android/tvapi/common/vo/TvOsType$EnumLanguage;->values()[Lcom/mstar/android/tvapi/common/vo/TvOsType$EnumLanguage;

    move-result-object v0

    const-string v1, "Language"

    invoke-interface {v6, v1}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v1

    invoke-interface {v6, v1}, Landroid/database/Cursor;->getInt(I)I

    move-result v1

    aget-object v7, v0, v1

    sget-object v0, Lcom/mstar/android/tvapi/common/vo/TvOsType$EnumLanguage;->E_ENGLISH:Lcom/mstar/android/tvapi/common/vo/TvOsType$EnumLanguage;

    if-ne v7, v0, :cond_4

    iget-object v0, p0, Lcom/android/server/tv/DataBaseDeskImpl;->stUsrData:Lcom/android/server/tv/DataBaseDesk$MS_USER_SYSTEM_SETTING;

    sget-object v1, Lcom/mstar/android/tvapi/common/vo/TvOsType$EnumLanguage;->E_ENGLISH:Lcom/mstar/android/tvapi/common/vo/TvOsType$EnumLanguage;

    iput-object v1, v0, Lcom/android/server/tv/DataBaseDesk$MS_USER_SYSTEM_SETTING;->enLanguage:Lcom/mstar/android/tvapi/common/vo/TvOsType$EnumLanguage;

    :goto_3
    iget-object v0, p0, Lcom/android/server/tv/DataBaseDeskImpl;->stUsrData:Lcom/android/server/tv/DataBaseDesk$MS_USER_SYSTEM_SETTING;

    invoke-static {}, Lcom/android/server/tv/DataBaseDesk$SPDIF_TYPE;->values()[Lcom/android/server/tv/DataBaseDesk$SPDIF_TYPE;

    move-result-object v1

    const-string v2, "enSPDIFMODE"

    invoke-interface {v6, v2}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v2

    invoke-interface {v6, v2}, Landroid/database/Cursor;->getInt(I)I

    move-result v2

    aget-object v1, v1, v2

    iput-object v1, v0, Lcom/android/server/tv/DataBaseDesk$MS_USER_SYSTEM_SETTING;->enSPDIFMODE:Lcom/android/server/tv/DataBaseDesk$SPDIF_TYPE;

    iget-object v0, p0, Lcom/android/server/tv/DataBaseDeskImpl;->stUsrData:Lcom/android/server/tv/DataBaseDesk$MS_USER_SYSTEM_SETTING;

    const-string v1, "fSoftwareUpdate"

    invoke-interface {v6, v1}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v1

    invoke-interface {v6, v1}, Landroid/database/Cursor;->getInt(I)I

    move-result v1

    int-to-short v1, v1

    iput-short v1, v0, Lcom/android/server/tv/DataBaseDesk$MS_USER_SYSTEM_SETTING;->fSoftwareUpdate:S

    iget-object v0, p0, Lcom/android/server/tv/DataBaseDeskImpl;->stUsrData:Lcom/android/server/tv/DataBaseDesk$MS_USER_SYSTEM_SETTING;

    const-string v1, "U8OADTime"

    invoke-interface {v6, v1}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v1

    invoke-interface {v6, v1}, Landroid/database/Cursor;->getInt(I)I

    move-result v1

    int-to-short v1, v1

    iput-short v1, v0, Lcom/android/server/tv/DataBaseDesk$MS_USER_SYSTEM_SETTING;->u8OADTime:S

    iget-object v0, p0, Lcom/android/server/tv/DataBaseDeskImpl;->stUsrData:Lcom/android/server/tv/DataBaseDesk$MS_USER_SYSTEM_SETTING;

    const-string v1, "fOADScanAfterWakeup"

    invoke-interface {v6, v1}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v1

    invoke-interface {v6, v1}, Landroid/database/Cursor;->getInt(I)I

    move-result v1

    int-to-short v1, v1

    iput-short v1, v0, Lcom/android/server/tv/DataBaseDesk$MS_USER_SYSTEM_SETTING;->fOADScanAfterWakeup:S

    iget-object v0, p0, Lcom/android/server/tv/DataBaseDeskImpl;->stUsrData:Lcom/android/server/tv/DataBaseDesk$MS_USER_SYSTEM_SETTING;

    const-string v1, "fAutoVolume"

    invoke-interface {v6, v1}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v1

    invoke-interface {v6, v1}, Landroid/database/Cursor;->getInt(I)I

    move-result v1

    int-to-short v1, v1

    iput-short v1, v0, Lcom/android/server/tv/DataBaseDesk$MS_USER_SYSTEM_SETTING;->fAutoVolume:S

    iget-object v0, p0, Lcom/android/server/tv/DataBaseDeskImpl;->stUsrData:Lcom/android/server/tv/DataBaseDesk$MS_USER_SYSTEM_SETTING;

    const-string v1, "fDcPowerOFFMode"

    invoke-interface {v6, v1}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v1

    invoke-interface {v6, v1}, Landroid/database/Cursor;->getInt(I)I

    move-result v1

    int-to-short v1, v1

    iput-short v1, v0, Lcom/android/server/tv/DataBaseDesk$MS_USER_SYSTEM_SETTING;->fDcPowerOFFMode:S

    iget-object v0, p0, Lcom/android/server/tv/DataBaseDeskImpl;->stUsrData:Lcom/android/server/tv/DataBaseDesk$MS_USER_SYSTEM_SETTING;

    const-string v1, "DtvRoute"

    invoke-interface {v6, v1}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v1

    invoke-interface {v6, v1}, Landroid/database/Cursor;->getInt(I)I

    move-result v1

    int-to-short v1, v1

    iput-short v1, v0, Lcom/android/server/tv/DataBaseDesk$MS_USER_SYSTEM_SETTING;->DtvRoute:S

    iget-object v0, p0, Lcom/android/server/tv/DataBaseDeskImpl;->stUsrData:Lcom/android/server/tv/DataBaseDesk$MS_USER_SYSTEM_SETTING;

    const-string v1, "ScartOutRGB"

    invoke-interface {v6, v1}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v1

    invoke-interface {v6, v1}, Landroid/database/Cursor;->getInt(I)I

    move-result v1

    int-to-short v1, v1

    iput-short v1, v0, Lcom/android/server/tv/DataBaseDesk$MS_USER_SYSTEM_SETTING;->ScartOutRGB:S

    iget-object v0, p0, Lcom/android/server/tv/DataBaseDeskImpl;->stUsrData:Lcom/android/server/tv/DataBaseDesk$MS_USER_SYSTEM_SETTING;

    const-string v1, "U8Transparency"

    invoke-interface {v6, v1}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v1

    invoke-interface {v6, v1}, Landroid/database/Cursor;->getInt(I)I

    move-result v1

    int-to-short v1, v1

    iput-short v1, v0, Lcom/android/server/tv/DataBaseDesk$MS_USER_SYSTEM_SETTING;->U8Transparency:S

    iget-object v0, p0, Lcom/android/server/tv/DataBaseDeskImpl;->stUsrData:Lcom/android/server/tv/DataBaseDesk$MS_USER_SYSTEM_SETTING;

    const-string v1, "u32MenuTimeOut"

    invoke-interface {v6, v1}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v1

    invoke-interface {v6, v1}, Landroid/database/Cursor;->getLong(I)J

    move-result-wide v1

    iput-wide v1, v0, Lcom/android/server/tv/DataBaseDesk$MS_USER_SYSTEM_SETTING;->u32MenuTimeOut:J

    iget-object v0, p0, Lcom/android/server/tv/DataBaseDeskImpl;->stUsrData:Lcom/android/server/tv/DataBaseDesk$MS_USER_SYSTEM_SETTING;

    const-string v1, "AudioOnly"

    invoke-interface {v6, v1}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v1

    invoke-interface {v6, v1}, Landroid/database/Cursor;->getInt(I)I

    move-result v1

    int-to-short v1, v1

    iput-short v1, v0, Lcom/android/server/tv/DataBaseDesk$MS_USER_SYSTEM_SETTING;->AudioOnly:S

    iget-object v0, p0, Lcom/android/server/tv/DataBaseDeskImpl;->stUsrData:Lcom/android/server/tv/DataBaseDesk$MS_USER_SYSTEM_SETTING;

    const-string v1, "bEnableWDT"

    invoke-interface {v6, v1}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v1

    invoke-interface {v6, v1}, Landroid/database/Cursor;->getInt(I)I

    move-result v1

    int-to-short v1, v1

    iput-short v1, v0, Lcom/android/server/tv/DataBaseDesk$MS_USER_SYSTEM_SETTING;->bEnableWDT:S

    iget-object v0, p0, Lcom/android/server/tv/DataBaseDeskImpl;->stUsrData:Lcom/android/server/tv/DataBaseDesk$MS_USER_SYSTEM_SETTING;

    const-string v1, "u8FavoriteRegion"

    invoke-interface {v6, v1}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v1

    invoke-interface {v6, v1}, Landroid/database/Cursor;->getInt(I)I

    move-result v1

    int-to-short v1, v1

    iput-short v1, v0, Lcom/android/server/tv/DataBaseDesk$MS_USER_SYSTEM_SETTING;->u8FavoriteRegion:S

    iget-object v0, p0, Lcom/android/server/tv/DataBaseDeskImpl;->stUsrData:Lcom/android/server/tv/DataBaseDesk$MS_USER_SYSTEM_SETTING;

    const-string v1, "u8Bandwidth"

    invoke-interface {v6, v1}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v1

    invoke-interface {v6, v1}, Landroid/database/Cursor;->getInt(I)I

    move-result v1

    int-to-short v1, v1

    iput-short v1, v0, Lcom/android/server/tv/DataBaseDesk$MS_USER_SYSTEM_SETTING;->u8Bandwidth:S

    iget-object v0, p0, Lcom/android/server/tv/DataBaseDeskImpl;->stUsrData:Lcom/android/server/tv/DataBaseDesk$MS_USER_SYSTEM_SETTING;

    const-string v1, "u8TimeShiftSizeType"

    invoke-interface {v6, v1}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v1

    invoke-interface {v6, v1}, Landroid/database/Cursor;->getInt(I)I

    move-result v1

    int-to-short v1, v1

    iput-short v1, v0, Lcom/android/server/tv/DataBaseDesk$MS_USER_SYSTEM_SETTING;->u8TimeShiftSizeType:S

    iget-object v0, p0, Lcom/android/server/tv/DataBaseDeskImpl;->stUsrData:Lcom/android/server/tv/DataBaseDesk$MS_USER_SYSTEM_SETTING;

    const-string v1, "fOadScan"

    invoke-interface {v6, v1}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v1

    invoke-interface {v6, v1}, Landroid/database/Cursor;->getInt(I)I

    move-result v1

    int-to-short v1, v1

    iput-short v1, v0, Lcom/android/server/tv/DataBaseDesk$MS_USER_SYSTEM_SETTING;->fOadScan:S

    iget-object v0, p0, Lcom/android/server/tv/DataBaseDeskImpl;->stUsrData:Lcom/android/server/tv/DataBaseDesk$MS_USER_SYSTEM_SETTING;

    const-string v1, "bEnablePVRRecordAll"

    invoke-interface {v6, v1}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v1

    invoke-interface {v6, v1}, Landroid/database/Cursor;->getInt(I)I

    move-result v1

    int-to-short v1, v1

    iput-short v1, v0, Lcom/android/server/tv/DataBaseDesk$MS_USER_SYSTEM_SETTING;->bEnablePVRRecordAll:S

    iget-object v0, p0, Lcom/android/server/tv/DataBaseDeskImpl;->stUsrData:Lcom/android/server/tv/DataBaseDesk$MS_USER_SYSTEM_SETTING;

    const-string v1, "u8ColorRangeMode"

    invoke-interface {v6, v1}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v1

    invoke-interface {v6, v1}, Landroid/database/Cursor;->getInt(I)I

    move-result v1

    int-to-short v1, v1

    iput-short v1, v0, Lcom/android/server/tv/DataBaseDesk$MS_USER_SYSTEM_SETTING;->u8ColorRangeMode:S

    iget-object v0, p0, Lcom/android/server/tv/DataBaseDeskImpl;->stUsrData:Lcom/android/server/tv/DataBaseDesk$MS_USER_SYSTEM_SETTING;

    const-string v1, "u8HDMIAudioSource"

    invoke-interface {v6, v1}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v1

    invoke-interface {v6, v1}, Landroid/database/Cursor;->getInt(I)I

    move-result v1

    int-to-short v1, v1

    iput-short v1, v0, Lcom/android/server/tv/DataBaseDesk$MS_USER_SYSTEM_SETTING;->u8HDMIAudioSource:S

    iget-object v0, p0, Lcom/android/server/tv/DataBaseDeskImpl;->stUsrData:Lcom/android/server/tv/DataBaseDesk$MS_USER_SYSTEM_SETTING;

    const-string v1, "bEnableAlwaysTimeshift"

    invoke-interface {v6, v1}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v1

    invoke-interface {v6, v1}, Landroid/database/Cursor;->getInt(I)I

    move-result v1

    int-to-short v1, v1

    iput-short v1, v0, Lcom/android/server/tv/DataBaseDesk$MS_USER_SYSTEM_SETTING;->bEnableAlwaysTimeshift:S

    iget-object v0, p0, Lcom/android/server/tv/DataBaseDeskImpl;->stUsrData:Lcom/android/server/tv/DataBaseDesk$MS_USER_SYSTEM_SETTING;

    invoke-static {}, Lcom/android/server/tv/DataBaseDesk$EN_MS_SUPER;->values()[Lcom/android/server/tv/DataBaseDesk$EN_MS_SUPER;

    move-result-object v1

    const-string v2, "eSUPER"

    invoke-interface {v6, v2}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v2

    invoke-interface {v6, v2}, Landroid/database/Cursor;->getInt(I)I

    move-result v2

    aget-object v1, v1, v2

    iput-object v1, v0, Lcom/android/server/tv/DataBaseDesk$MS_USER_SYSTEM_SETTING;->eSUPER:Lcom/android/server/tv/DataBaseDesk$EN_MS_SUPER;

    iget-object v1, p0, Lcom/android/server/tv/DataBaseDeskImpl;->stUsrData:Lcom/android/server/tv/DataBaseDesk$MS_USER_SYSTEM_SETTING;

    const-string v0, "bUartBus"

    invoke-interface {v6, v0}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v0

    invoke-interface {v6, v0}, Landroid/database/Cursor;->getInt(I)I

    move-result v0

    if-nez v0, :cond_7

    move v0, v9

    :goto_4
    iput-boolean v0, v1, Lcom/android/server/tv/DataBaseDesk$MS_USER_SYSTEM_SETTING;->bUartBus:Z

    iget-object v0, p0, Lcom/android/server/tv/DataBaseDeskImpl;->stUsrData:Lcom/android/server/tv/DataBaseDesk$MS_USER_SYSTEM_SETTING;

    const-string v1, "m_AutoZoom"

    invoke-interface {v6, v1}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v1

    invoke-interface {v6, v1}, Landroid/database/Cursor;->getInt(I)I

    move-result v1

    int-to-short v1, v1

    iput-short v1, v0, Lcom/android/server/tv/DataBaseDesk$MS_USER_SYSTEM_SETTING;->m_AutoZoom:S

    iget-object v1, p0, Lcom/android/server/tv/DataBaseDeskImpl;->stUsrData:Lcom/android/server/tv/DataBaseDesk$MS_USER_SYSTEM_SETTING;

    const-string v0, "bOverScan"

    invoke-interface {v6, v0}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v0

    invoke-interface {v6, v0}, Landroid/database/Cursor;->getInt(I)I

    move-result v0

    if-nez v0, :cond_8

    move v0, v9

    :goto_5
    iput-boolean v0, v1, Lcom/android/server/tv/DataBaseDesk$MS_USER_SYSTEM_SETTING;->bOverScan:Z

    iget-object v0, p0, Lcom/android/server/tv/DataBaseDeskImpl;->stUsrData:Lcom/android/server/tv/DataBaseDesk$MS_USER_SYSTEM_SETTING;

    const-string v1, "m_u8BrazilVideoStandardType"

    invoke-interface {v6, v1}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v1

    invoke-interface {v6, v1}, Landroid/database/Cursor;->getInt(I)I

    move-result v1

    int-to-short v1, v1

    iput-short v1, v0, Lcom/android/server/tv/DataBaseDesk$MS_USER_SYSTEM_SETTING;->m_u8BrazilVideoStandardType:S

    iget-object v0, p0, Lcom/android/server/tv/DataBaseDeskImpl;->stUsrData:Lcom/android/server/tv/DataBaseDesk$MS_USER_SYSTEM_SETTING;

    const-string v1, "m_u8SoftwareUpdateMode"

    invoke-interface {v6, v1}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v1

    invoke-interface {v6, v1}, Landroid/database/Cursor;->getInt(I)I

    move-result v1

    int-to-short v1, v1

    iput-short v1, v0, Lcom/android/server/tv/DataBaseDesk$MS_USER_SYSTEM_SETTING;->m_u8SoftwareUpdateMode:S

    iget-object v0, p0, Lcom/android/server/tv/DataBaseDeskImpl;->stUsrData:Lcom/android/server/tv/DataBaseDesk$MS_USER_SYSTEM_SETTING;

    const-string v1, "OSD_Active_Time"

    invoke-interface {v6, v1}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v1

    invoke-interface {v6, v1}, Landroid/database/Cursor;->getLong(I)J

    move-result-wide v1

    iput-wide v1, v0, Lcom/android/server/tv/DataBaseDesk$MS_USER_SYSTEM_SETTING;->u32OSD_Active_Time:J

    iget-object v1, p0, Lcom/android/server/tv/DataBaseDeskImpl;->stUsrData:Lcom/android/server/tv/DataBaseDesk$MS_USER_SYSTEM_SETTING;

    const-string v0, "m_MessageBoxExist"

    invoke-interface {v6, v0}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v0

    invoke-interface {v6, v0}, Landroid/database/Cursor;->getInt(I)I

    move-result v0

    if-nez v0, :cond_9

    move v0, v9

    :goto_6
    iput-boolean v0, v1, Lcom/android/server/tv/DataBaseDesk$MS_USER_SYSTEM_SETTING;->m_MessageBoxExist:Z

    iget-object v0, p0, Lcom/android/server/tv/DataBaseDeskImpl;->stUsrData:Lcom/android/server/tv/DataBaseDesk$MS_USER_SYSTEM_SETTING;

    const-string v1, "u16LastOADVersion"

    invoke-interface {v6, v1}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v1

    invoke-interface {v6, v1}, Landroid/database/Cursor;->getInt(I)I

    move-result v1

    iput v1, v0, Lcom/android/server/tv/DataBaseDesk$MS_USER_SYSTEM_SETTING;->u16LastOADVersion:I

    iget-object v0, p0, Lcom/android/server/tv/DataBaseDeskImpl;->stUsrData:Lcom/android/server/tv/DataBaseDesk$MS_USER_SYSTEM_SETTING;

    const-string v1, "bEnableAutoChannelUpdate"

    invoke-interface {v6, v1}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v1

    invoke-interface {v6, v1}, Landroid/database/Cursor;->getInt(I)I

    move-result v1

    if-nez v1, :cond_a

    :goto_7
    iput-boolean v9, v0, Lcom/android/server/tv/DataBaseDesk$MS_USER_SYSTEM_SETTING;->bEnableAutoChannelUpdate:Z

    iget-object v1, p0, Lcom/android/server/tv/DataBaseDeskImpl;->stUsrData:Lcom/android/server/tv/DataBaseDesk$MS_USER_SYSTEM_SETTING;

    const-string v0, "bATVChSwitchFreeze"

    invoke-interface {v6, v0}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v0

    invoke-interface {v6, v0}, Landroid/database/Cursor;->getInt(I)I

    move-result v0

    if-nez v0, :cond_b

    sget-object v0, Lcom/android/server/tv/DataBaseDesk$EN_MS_CHANNEL_SWITCH_MODE;->MS_CHANNEL_SWM_BLACKSCREEN:Lcom/android/server/tv/DataBaseDesk$EN_MS_CHANNEL_SWITCH_MODE;

    :goto_8
    iput-object v0, v1, Lcom/android/server/tv/DataBaseDesk$MS_USER_SYSTEM_SETTING;->eChSwMode:Lcom/android/server/tv/DataBaseDesk$EN_MS_CHANNEL_SWITCH_MODE;

    :cond_0
    invoke-interface {v6}, Landroid/database/Cursor;->close()V

    iget-object v0, p0, Lcom/android/server/tv/DataBaseDeskImpl;->stUsrData:Lcom/android/server/tv/DataBaseDesk$MS_USER_SYSTEM_SETTING;

    return-object v0

    :cond_1
    move v0, v10

    goto/16 :goto_0

    :cond_2
    move v0, v10

    goto/16 :goto_1

    :cond_3
    move v0, v10

    goto/16 :goto_2

    :cond_4
    sget-object v0, Lcom/mstar/android/tvapi/common/vo/TvOsType$EnumLanguage;->E_CHINESE:Lcom/mstar/android/tvapi/common/vo/TvOsType$EnumLanguage;

    if-ne v7, v0, :cond_5

    iget-object v0, p0, Lcom/android/server/tv/DataBaseDeskImpl;->stUsrData:Lcom/android/server/tv/DataBaseDesk$MS_USER_SYSTEM_SETTING;

    sget-object v1, Lcom/mstar/android/tvapi/common/vo/TvOsType$EnumLanguage;->E_CHINESE:Lcom/mstar/android/tvapi/common/vo/TvOsType$EnumLanguage;

    iput-object v1, v0, Lcom/android/server/tv/DataBaseDesk$MS_USER_SYSTEM_SETTING;->enLanguage:Lcom/mstar/android/tvapi/common/vo/TvOsType$EnumLanguage;

    goto/16 :goto_3

    :cond_5
    sget-object v0, Lcom/mstar/android/tvapi/common/vo/TvOsType$EnumLanguage;->E_ACHINESE:Lcom/mstar/android/tvapi/common/vo/TvOsType$EnumLanguage;

    if-ne v7, v0, :cond_6

    iget-object v0, p0, Lcom/android/server/tv/DataBaseDeskImpl;->stUsrData:Lcom/android/server/tv/DataBaseDesk$MS_USER_SYSTEM_SETTING;

    sget-object v1, Lcom/mstar/android/tvapi/common/vo/TvOsType$EnumLanguage;->E_ACHINESE:Lcom/mstar/android/tvapi/common/vo/TvOsType$EnumLanguage;

    iput-object v1, v0, Lcom/android/server/tv/DataBaseDesk$MS_USER_SYSTEM_SETTING;->enLanguage:Lcom/mstar/android/tvapi/common/vo/TvOsType$EnumLanguage;

    goto/16 :goto_3

    :cond_6
    iget-object v0, p0, Lcom/android/server/tv/DataBaseDeskImpl;->stUsrData:Lcom/android/server/tv/DataBaseDesk$MS_USER_SYSTEM_SETTING;

    sget-object v1, Lcom/mstar/android/tvapi/common/vo/TvOsType$EnumLanguage;->E_CHINESE:Lcom/mstar/android/tvapi/common/vo/TvOsType$EnumLanguage;

    iput-object v1, v0, Lcom/android/server/tv/DataBaseDesk$MS_USER_SYSTEM_SETTING;->enLanguage:Lcom/mstar/android/tvapi/common/vo/TvOsType$EnumLanguage;

    new-instance v8, Landroid/content/ContentValues;

    invoke-direct {v8}, Landroid/content/ContentValues;-><init>()V

    const-string v0, "Language"

    iget-object v1, p0, Lcom/android/server/tv/DataBaseDeskImpl;->stUsrData:Lcom/android/server/tv/DataBaseDesk$MS_USER_SYSTEM_SETTING;

    iget-object v1, v1, Lcom/android/server/tv/DataBaseDesk$MS_USER_SYSTEM_SETTING;->enLanguage:Lcom/mstar/android/tvapi/common/vo/TvOsType$EnumLanguage;

    invoke-virtual {v1}, Lcom/mstar/android/tvapi/common/vo/TvOsType$EnumLanguage;->ordinal()I

    move-result v1

    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    invoke-virtual {v8, v0, v1}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Integer;)V

    :try_start_0
    invoke-virtual {p0}, Lcom/android/server/tv/DataBaseDeskImpl;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v0

    const-string v1, "content://mstar.tv.usersetting/systemsetting"

    invoke-static {v1}, Landroid/net/Uri;->parse(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v1

    const/4 v2, 0x0

    const/4 v3, 0x0

    invoke-virtual {v0, v1, v8, v2, v3}, Landroid/content/ContentResolver;->update(Landroid/net/Uri;Landroid/content/ContentValues;Ljava/lang/String;[Ljava/lang/String;)I
    :try_end_0
    .catch Landroid/database/SQLException; {:try_start_0 .. :try_end_0} :catch_0

    goto/16 :goto_3

    :catch_0
    move-exception v0

    goto/16 :goto_3

    :cond_7
    move v0, v10

    goto/16 :goto_4

    :cond_8
    move v0, v10

    goto/16 :goto_5

    :cond_9
    move v0, v10

    goto/16 :goto_6

    :cond_a
    move v9, v10

    goto :goto_7

    :cond_b
    sget-object v0, Lcom/android/server/tv/DataBaseDesk$EN_MS_CHANNEL_SWITCH_MODE;->MS_CHANNEL_SWM_FREEZE:Lcom/android/server/tv/DataBaseDesk$EN_MS_CHANNEL_SWITCH_MODE;

    goto :goto_8
.end method

.method public queryUsrColorTmpData()Lcom/android/server/tv/DataBaseDesk$T_MS_COLOR_TEMP_DATA;
    .locals 8

    const/4 v2, 0x0

    invoke-virtual {p0}, Lcom/android/server/tv/DataBaseDeskImpl;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v0

    const-string v1, "content://mstar.tv.usersetting/usercolortemp"

    invoke-static {v1}, Landroid/net/Uri;->parse(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v1

    move-object v3, v2

    move-object v4, v2

    move-object v5, v2

    invoke-virtual/range {v0 .. v5}, Landroid/content/ContentResolver;->query(Landroid/net/Uri;[Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;)Landroid/database/Cursor;

    move-result-object v6

    const/4 v7, 0x0

    :goto_0
    invoke-interface {v6}, Landroid/database/Cursor;->moveToNext()Z

    move-result v0

    if-eqz v0, :cond_0

    sget-object v0, Lcom/mstar/android/tvapi/common/vo/TvOsType$EnumInputSource;->E_INPUT_SOURCE_NUM:Lcom/mstar/android/tvapi/common/vo/TvOsType$EnumInputSource;

    invoke-virtual {v0}, Lcom/mstar/android/tvapi/common/vo/TvOsType$EnumInputSource;->ordinal()I

    move-result v0

    add-int/lit8 v0, v0, -0x1

    if-le v7, v0, :cond_1

    :cond_0
    invoke-interface {v6}, Landroid/database/Cursor;->close()V

    iget-object v0, p0, Lcom/android/server/tv/DataBaseDeskImpl;->stUsrColorTemp:Lcom/android/server/tv/DataBaseDesk$T_MS_COLOR_TEMP_DATA;

    return-object v0

    :cond_1
    iget-object v0, p0, Lcom/android/server/tv/DataBaseDeskImpl;->stUsrColorTemp:Lcom/android/server/tv/DataBaseDesk$T_MS_COLOR_TEMP_DATA;

    const-string v1, "u8RedGain"

    invoke-interface {v6, v1}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v1

    invoke-interface {v6, v1}, Landroid/database/Cursor;->getInt(I)I

    move-result v1

    int-to-short v1, v1

    iput-short v1, v0, Lcom/android/server/tv/DataBaseDesk$T_MS_COLOR_TEMP_DATA;->redgain:S

    iget-object v0, p0, Lcom/android/server/tv/DataBaseDeskImpl;->stUsrColorTemp:Lcom/android/server/tv/DataBaseDesk$T_MS_COLOR_TEMP_DATA;

    const-string v1, "u8GreenGain"

    invoke-interface {v6, v1}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v1

    invoke-interface {v6, v1}, Landroid/database/Cursor;->getInt(I)I

    move-result v1

    int-to-short v1, v1

    iput-short v1, v0, Lcom/android/server/tv/DataBaseDesk$T_MS_COLOR_TEMP_DATA;->greengain:S

    iget-object v0, p0, Lcom/android/server/tv/DataBaseDeskImpl;->stUsrColorTemp:Lcom/android/server/tv/DataBaseDesk$T_MS_COLOR_TEMP_DATA;

    const-string v1, "u8BlueGain"

    invoke-interface {v6, v1}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v1

    invoke-interface {v6, v1}, Landroid/database/Cursor;->getInt(I)I

    move-result v1

    int-to-short v1, v1

    iput-short v1, v0, Lcom/android/server/tv/DataBaseDesk$T_MS_COLOR_TEMP_DATA;->bluegain:S

    iget-object v0, p0, Lcom/android/server/tv/DataBaseDeskImpl;->stUsrColorTemp:Lcom/android/server/tv/DataBaseDesk$T_MS_COLOR_TEMP_DATA;

    const-string v1, "u8RedOffset"

    invoke-interface {v6, v1}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v1

    invoke-interface {v6, v1}, Landroid/database/Cursor;->getInt(I)I

    move-result v1

    int-to-short v1, v1

    iput-short v1, v0, Lcom/android/server/tv/DataBaseDesk$T_MS_COLOR_TEMP_DATA;->redoffset:S

    iget-object v0, p0, Lcom/android/server/tv/DataBaseDeskImpl;->stUsrColorTemp:Lcom/android/server/tv/DataBaseDesk$T_MS_COLOR_TEMP_DATA;

    const-string v1, "u8GreenOffset"

    invoke-interface {v6, v1}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v1

    invoke-interface {v6, v1}, Landroid/database/Cursor;->getInt(I)I

    move-result v1

    int-to-short v1, v1

    iput-short v1, v0, Lcom/android/server/tv/DataBaseDesk$T_MS_COLOR_TEMP_DATA;->greenoffset:S

    iget-object v0, p0, Lcom/android/server/tv/DataBaseDeskImpl;->stUsrColorTemp:Lcom/android/server/tv/DataBaseDesk$T_MS_COLOR_TEMP_DATA;

    const-string v1, "u8BlueOffset"

    invoke-interface {v6, v1}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v1

    invoke-interface {v6, v1}, Landroid/database/Cursor;->getInt(I)I

    move-result v1

    int-to-short v1, v1

    iput-short v1, v0, Lcom/android/server/tv/DataBaseDesk$T_MS_COLOR_TEMP_DATA;->blueoffset:S

    add-int/lit8 v7, v7, 0x1

    goto :goto_0
.end method

.method public queryUsrColorTmpExData(I)Lcom/mstar/android/tvapi/common/vo/ColorTemperatureExData;
    .locals 8
    .param p1    # I

    const/4 v2, 0x0

    new-instance v7, Lcom/mstar/android/tvapi/common/vo/ColorTemperatureExData;

    invoke-direct {v7}, Lcom/mstar/android/tvapi/common/vo/ColorTemperatureExData;-><init>()V

    invoke-virtual {p0}, Lcom/android/server/tv/DataBaseDeskImpl;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v0

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "content://mstar.tv.usersetting/usercolortempex/"

    invoke-virtual {v1, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v1}, Landroid/net/Uri;->parse(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v1

    move-object v3, v2

    move-object v4, v2

    move-object v5, v2

    invoke-virtual/range {v0 .. v5}, Landroid/content/ContentResolver;->query(Landroid/net/Uri;[Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;)Landroid/database/Cursor;

    move-result-object v6

    invoke-interface {v6}, Landroid/database/Cursor;->moveToNext()Z

    move-result v0

    if-eqz v0, :cond_0

    const-string v0, "u16RedGain"

    invoke-interface {v6, v0}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v0

    invoke-interface {v6, v0}, Landroid/database/Cursor;->getInt(I)I

    move-result v0

    iput v0, v7, Lcom/mstar/android/tvapi/common/vo/ColorTemperatureExData;->redGain:I

    const-string v0, "u16GreenGain"

    invoke-interface {v6, v0}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v0

    invoke-interface {v6, v0}, Landroid/database/Cursor;->getInt(I)I

    move-result v0

    iput v0, v7, Lcom/mstar/android/tvapi/common/vo/ColorTemperatureExData;->greenGain:I

    const-string v0, "u16BlueGain"

    invoke-interface {v6, v0}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v0

    invoke-interface {v6, v0}, Landroid/database/Cursor;->getInt(I)I

    move-result v0

    iput v0, v7, Lcom/mstar/android/tvapi/common/vo/ColorTemperatureExData;->blueGain:I

    const-string v0, "u16RedOffset"

    invoke-interface {v6, v0}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v0

    invoke-interface {v6, v0}, Landroid/database/Cursor;->getInt(I)I

    move-result v0

    iput v0, v7, Lcom/mstar/android/tvapi/common/vo/ColorTemperatureExData;->redOffset:I

    const-string v0, "u16GreenOffset"

    invoke-interface {v6, v0}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v0

    invoke-interface {v6, v0}, Landroid/database/Cursor;->getInt(I)I

    move-result v0

    iput v0, v7, Lcom/mstar/android/tvapi/common/vo/ColorTemperatureExData;->greenOffset:I

    const-string v0, "u16BlueOffset"

    invoke-interface {v6, v0}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v0

    invoke-interface {v6, v0}, Landroid/database/Cursor;->getInt(I)I

    move-result v0

    iput v0, v7, Lcom/mstar/android/tvapi/common/vo/ColorTemperatureExData;->blueOffset:I

    :cond_0
    invoke-interface {v6}, Landroid/database/Cursor;->close()V

    return-object v7
.end method

.method public queryUsrColorTmpExData()[Lcom/android/server/tv/DataBaseDesk$T_MS_COLOR_TEMPEX_DATA;
    .locals 8

    const/4 v2, 0x0

    invoke-virtual {p0}, Lcom/android/server/tv/DataBaseDeskImpl;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v0

    const-string v1, "content://mstar.tv.usersetting/usercolortempex"

    invoke-static {v1}, Landroid/net/Uri;->parse(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v1

    move-object v3, v2

    move-object v4, v2

    move-object v5, v2

    invoke-virtual/range {v0 .. v5}, Landroid/content/ContentResolver;->query(Landroid/net/Uri;[Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;)Landroid/database/Cursor;

    move-result-object v6

    const/4 v7, 0x0

    :goto_0
    invoke-interface {v6}, Landroid/database/Cursor;->moveToNext()Z

    move-result v0

    if-eqz v0, :cond_0

    sget-object v0, Lcom/mstar/android/tvapi/common/vo/TvOsType$EnumInputSource;->E_INPUT_SOURCE_NUM:Lcom/mstar/android/tvapi/common/vo/TvOsType$EnumInputSource;

    invoke-virtual {v0}, Lcom/mstar/android/tvapi/common/vo/TvOsType$EnumInputSource;->ordinal()I

    move-result v0

    add-int/lit8 v0, v0, -0x1

    if-le v7, v0, :cond_1

    :cond_0
    invoke-interface {v6}, Landroid/database/Cursor;->close()V

    iget-object v0, p0, Lcom/android/server/tv/DataBaseDeskImpl;->stUsrColorTempEx:[Lcom/android/server/tv/DataBaseDesk$T_MS_COLOR_TEMPEX_DATA;

    return-object v0

    :cond_1
    iget-object v0, p0, Lcom/android/server/tv/DataBaseDeskImpl;->stUsrColorTempEx:[Lcom/android/server/tv/DataBaseDesk$T_MS_COLOR_TEMPEX_DATA;

    aget-object v0, v0, v7

    const-string v1, "u16RedGain"

    invoke-interface {v6, v1}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v1

    invoke-interface {v6, v1}, Landroid/database/Cursor;->getInt(I)I

    move-result v1

    iput v1, v0, Lcom/android/server/tv/DataBaseDesk$T_MS_COLOR_TEMPEX_DATA;->redgain:I

    iget-object v0, p0, Lcom/android/server/tv/DataBaseDeskImpl;->stUsrColorTempEx:[Lcom/android/server/tv/DataBaseDesk$T_MS_COLOR_TEMPEX_DATA;

    aget-object v0, v0, v7

    const-string v1, "u16GreenGain"

    invoke-interface {v6, v1}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v1

    invoke-interface {v6, v1}, Landroid/database/Cursor;->getInt(I)I

    move-result v1

    iput v1, v0, Lcom/android/server/tv/DataBaseDesk$T_MS_COLOR_TEMPEX_DATA;->greengain:I

    iget-object v0, p0, Lcom/android/server/tv/DataBaseDeskImpl;->stUsrColorTempEx:[Lcom/android/server/tv/DataBaseDesk$T_MS_COLOR_TEMPEX_DATA;

    aget-object v0, v0, v7

    const-string v1, "u16BlueGain"

    invoke-interface {v6, v1}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v1

    invoke-interface {v6, v1}, Landroid/database/Cursor;->getInt(I)I

    move-result v1

    iput v1, v0, Lcom/android/server/tv/DataBaseDesk$T_MS_COLOR_TEMPEX_DATA;->bluegain:I

    iget-object v0, p0, Lcom/android/server/tv/DataBaseDeskImpl;->stUsrColorTempEx:[Lcom/android/server/tv/DataBaseDesk$T_MS_COLOR_TEMPEX_DATA;

    aget-object v0, v0, v7

    const-string v1, "u16RedOffset"

    invoke-interface {v6, v1}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v1

    invoke-interface {v6, v1}, Landroid/database/Cursor;->getInt(I)I

    move-result v1

    iput v1, v0, Lcom/android/server/tv/DataBaseDesk$T_MS_COLOR_TEMPEX_DATA;->redoffset:I

    iget-object v0, p0, Lcom/android/server/tv/DataBaseDeskImpl;->stUsrColorTempEx:[Lcom/android/server/tv/DataBaseDesk$T_MS_COLOR_TEMPEX_DATA;

    aget-object v0, v0, v7

    const-string v1, "u16GreenOffset"

    invoke-interface {v6, v1}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v1

    invoke-interface {v6, v1}, Landroid/database/Cursor;->getInt(I)I

    move-result v1

    iput v1, v0, Lcom/android/server/tv/DataBaseDesk$T_MS_COLOR_TEMPEX_DATA;->greenoffset:I

    iget-object v0, p0, Lcom/android/server/tv/DataBaseDeskImpl;->stUsrColorTempEx:[Lcom/android/server/tv/DataBaseDesk$T_MS_COLOR_TEMPEX_DATA;

    aget-object v0, v0, v7

    const-string v1, "u16BlueOffset"

    invoke-interface {v6, v1}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v1

    invoke-interface {v6, v1}, Landroid/database/Cursor;->getInt(I)I

    move-result v1

    iput v1, v0, Lcom/android/server/tv/DataBaseDesk$T_MS_COLOR_TEMPEX_DATA;->blueoffset:I

    add-int/lit8 v7, v7, 0x1

    goto :goto_0
.end method

.method public queryVideo3DLrSwitchMode(I)I
    .locals 8
    .param p1    # I

    const/4 v2, 0x0

    const/4 v7, 0x0

    invoke-virtual {p0}, Lcom/android/server/tv/DataBaseDeskImpl;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v0

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "content://mstar.tv.usersetting/threedvideomode/inputsrc/"

    invoke-virtual {v1, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v1}, Landroid/net/Uri;->parse(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v1

    move-object v3, v2

    move-object v4, v2

    move-object v5, v2

    invoke-virtual/range {v0 .. v5}, Landroid/content/ContentResolver;->query(Landroid/net/Uri;[Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;)Landroid/database/Cursor;

    move-result-object v6

    invoke-interface {v6}, Landroid/database/Cursor;->moveToFirst()Z

    move-result v0

    if-eqz v0, :cond_0

    const-string v0, "eThreeDVideoLRViewSwitch"

    invoke-interface {v6, v0}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v0

    invoke-interface {v6, v0}, Landroid/database/Cursor;->getInt(I)I

    move-result v7

    :cond_0
    invoke-interface {v6}, Landroid/database/Cursor;->close()V

    return v7
.end method

.method public queryVideo3DMode(Lcom/android/server/tv/DataBaseDeskImpl$S3D_TABLE_FIELD;I)I
    .locals 9
    .param p1    # Lcom/android/server/tv/DataBaseDeskImpl$S3D_TABLE_FIELD;
    .param p2    # I

    const/4 v2, 0x0

    const/4 v8, 0x0

    const-string v7, ""

    sget-object v0, Lcom/android/server/tv/DataBaseDeskImpl$1;->$SwitchMap$com$android$server$tv$DataBaseDeskImpl$S3D_TABLE_FIELD:[I

    invoke-virtual {p1}, Lcom/android/server/tv/DataBaseDeskImpl$S3D_TABLE_FIELD;->ordinal()I

    move-result v1

    aget v0, v0, v1

    packed-switch v0, :pswitch_data_0

    const/4 v0, -0x1

    :goto_0
    return v0

    :pswitch_0
    const-string v7, "eThreeDVideo"

    :goto_1
    invoke-virtual {p0}, Lcom/android/server/tv/DataBaseDeskImpl;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v0

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "content://mstar.tv.usersetting/threedvideomode/inputsrc/"

    invoke-virtual {v1, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v1}, Landroid/net/Uri;->parse(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v1

    move-object v3, v2

    move-object v4, v2

    move-object v5, v2

    invoke-virtual/range {v0 .. v5}, Landroid/content/ContentResolver;->query(Landroid/net/Uri;[Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;)Landroid/database/Cursor;

    move-result-object v6

    invoke-interface {v6}, Landroid/database/Cursor;->moveToFirst()Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-interface {v6, v7}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v0

    invoke-interface {v6, v0}, Landroid/database/Cursor;->getInt(I)I

    move-result v8

    :cond_0
    invoke-interface {v6}, Landroid/database/Cursor;->close()V

    move v0, v8

    goto :goto_0

    :pswitch_1
    const-string v7, "eThreeDVideoDisplayFormat"

    goto :goto_1

    :pswitch_2
    const-string v7, "eThreeDVideo3DDepth"

    goto :goto_1

    :pswitch_3
    const-string v7, "eThreeDVideoAutoStart"

    goto :goto_1

    :pswitch_4
    const-string v7, "eThreeDVideo3DOutputAspect"

    goto :goto_1

    :pswitch_5
    const-string v7, "eThreeDVideoLRViewSwitch"

    goto :goto_1

    :pswitch_6
    const-string v7, "eThreeDVideoSelfAdaptiveDetect"

    goto :goto_1

    :pswitch_7
    const-string v7, "eThreeDVideoSelfAdaptiveLevel"

    goto :goto_1

    :pswitch_8
    const-string v7, "eThreeDVideo3DTo2D"

    goto :goto_1

    :pswitch_9
    const-string v7, "eThreeDVideo3DOffset"

    goto :goto_1

    nop

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
        :pswitch_1
        :pswitch_2
        :pswitch_3
        :pswitch_4
        :pswitch_5
        :pswitch_6
        :pswitch_7
        :pswitch_8
        :pswitch_9
    .end packed-switch
.end method

.method public queryVolume()I
    .locals 8

    const/4 v2, 0x0

    invoke-virtual {p0}, Lcom/android/server/tv/DataBaseDeskImpl;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v0

    const-string v1, "content://mstar.tv.usersetting/soundsetting"

    invoke-static {v1}, Landroid/net/Uri;->parse(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v1

    move-object v3, v2

    move-object v4, v2

    move-object v5, v2

    invoke-virtual/range {v0 .. v5}, Landroid/content/ContentResolver;->query(Landroid/net/Uri;[Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;)Landroid/database/Cursor;

    move-result-object v6

    const/4 v7, 0x0

    invoke-interface {v6}, Landroid/database/Cursor;->moveToFirst()Z

    move-result v0

    if-eqz v0, :cond_0

    const-string v0, "Volume"

    invoke-interface {v6, v0}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v0

    invoke-interface {v6, v0}, Landroid/database/Cursor;->getInt(I)I

    move-result v0

    int-to-short v7, v0

    :cond_0
    invoke-interface {v6}, Landroid/database/Cursor;->close()V

    return v7
.end method

.method public queryWallmusic()I
    .locals 8

    const/4 v2, 0x0

    const/4 v7, 0x0

    invoke-virtual {p0}, Lcom/android/server/tv/DataBaseDeskImpl;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v0

    const-string v1, "content://mstar.tv.usersetting/soundsetting"

    invoke-static {v1}, Landroid/net/Uri;->parse(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v1

    move-object v3, v2

    move-object v4, v2

    move-object v5, v2

    invoke-virtual/range {v0 .. v5}, Landroid/content/ContentResolver;->query(Landroid/net/Uri;[Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;)Landroid/database/Cursor;

    move-result-object v6

    invoke-interface {v6}, Landroid/database/Cursor;->moveToFirst()Z

    move-result v0

    if-eqz v0, :cond_0

    const-string v0, "WallMusic"

    invoke-interface {v6, v0}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v0

    invoke-interface {v6, v0}, Landroid/database/Cursor;->getInt(I)I

    move-result v7

    :cond_0
    invoke-interface {v6}, Landroid/database/Cursor;->close()V

    return v7
.end method

.method public queryePicMode(I)I
    .locals 8
    .param p1    # I

    const/4 v2, 0x0

    invoke-virtual {p0}, Lcom/android/server/tv/DataBaseDeskImpl;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v0

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "content://mstar.tv.usersetting/videosetting/inputsrc/"

    invoke-virtual {v1, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v1}, Landroid/net/Uri;->parse(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v1

    move-object v3, v2

    move-object v4, v2

    move-object v5, v2

    invoke-virtual/range {v0 .. v5}, Landroid/content/ContentResolver;->query(Landroid/net/Uri;[Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;)Landroid/database/Cursor;

    move-result-object v6

    const/4 v7, -0x1

    invoke-interface {v6}, Landroid/database/Cursor;->moveToNext()Z

    move-result v0

    if-eqz v0, :cond_0

    const-string v0, "ePicture"

    invoke-interface {v6, v0}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v0

    invoke-interface {v6, v0}, Landroid/database/Cursor;->getInt(I)I

    move-result v7

    :cond_0
    invoke-interface {v6}, Landroid/database/Cursor;->close()V

    return v7
.end method

.method public restoreUsrDB(Lcom/android/server/tv/DataBaseDesk$EN_SYSTEM_FACTORY_DB_COMMAND;)Z
    .locals 2
    .param p1    # Lcom/android/server/tv/DataBaseDesk$EN_SYSTEM_FACTORY_DB_COMMAND;

    sget-object v0, Lcom/android/server/tv/DataBaseDeskImpl$1;->$SwitchMap$com$android$server$tv$DataBaseDesk$EN_SYSTEM_FACTORY_DB_COMMAND:[I

    invoke-virtual {p1}, Lcom/android/server/tv/DataBaseDesk$EN_SYSTEM_FACTORY_DB_COMMAND;->ordinal()I

    move-result v1

    aget v0, v0, v1

    packed-switch v0, :pswitch_data_0

    :goto_0
    :pswitch_0
    const/4 v0, 0x1

    return v0

    :pswitch_1
    invoke-direct {p0}, Lcom/android/server/tv/DataBaseDeskImpl;->initVarPicture()Z

    invoke-direct {p0}, Lcom/android/server/tv/DataBaseDeskImpl;->InitSettingVar()Z

    invoke-direct {p0}, Lcom/android/server/tv/DataBaseDeskImpl;->initCECVar()Z

    invoke-direct {p0}, Lcom/android/server/tv/DataBaseDeskImpl;->initVarSound()Z

    goto :goto_0

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
        :pswitch_0
        :pswitch_1
        :pswitch_0
    .end packed-switch
.end method

.method public setAdcSetting(Lcom/android/server/tv/DataBaseDesk$MS_ADC_SETTING;)Z
    .locals 1
    .param p1    # Lcom/android/server/tv/DataBaseDesk$MS_ADC_SETTING;

    iput-object p1, p0, Lcom/android/server/tv/DataBaseDeskImpl;->m_stFactoryAdc:Lcom/android/server/tv/DataBaseDesk$MS_ADC_SETTING;

    const/4 v0, 0x1

    return v0
.end method

.method public setCECVar(Lcom/android/server/tv/DataBaseDesk$MS_CEC_SETTING;)Z
    .locals 1
    .param p1    # Lcom/android/server/tv/DataBaseDesk$MS_CEC_SETTING;

    iput-object p1, p0, Lcom/android/server/tv/DataBaseDeskImpl;->stCECPara:Lcom/android/server/tv/DataBaseDesk$MS_CEC_SETTING;

    const/4 v0, 0x1

    return v0
.end method

.method public setFactoryExt(Lcom/android/server/tv/DataBaseDesk$MS_FACTORY_EXTERN_SETTING;)Z
    .locals 1
    .param p1    # Lcom/android/server/tv/DataBaseDesk$MS_FACTORY_EXTERN_SETTING;

    iput-object p1, p0, Lcom/android/server/tv/DataBaseDeskImpl;->m_stFactoryExt:Lcom/android/server/tv/DataBaseDesk$MS_FACTORY_EXTERN_SETTING;

    const/4 v0, 0x1

    return v0
.end method

.method public setLocationSet(Lcom/android/server/tv/DataBaseDesk$MS_USER_LOCATION_SETTING;)Z
    .locals 1
    .param p1    # Lcom/android/server/tv/DataBaseDesk$MS_USER_LOCATION_SETTING;

    iput-object p1, p0, Lcom/android/server/tv/DataBaseDeskImpl;->stUserLocationSetting:Lcom/android/server/tv/DataBaseDesk$MS_USER_LOCATION_SETTING;

    const/4 v0, 0x1

    return v0
.end method

.method public setNoStandSet(Lcom/android/server/tv/DataBaseDesk$MS_Factory_NS_VD_SET;)Z
    .locals 1
    .param p1    # Lcom/android/server/tv/DataBaseDesk$MS_Factory_NS_VD_SET;

    iput-object p1, p0, Lcom/android/server/tv/DataBaseDeskImpl;->mNoStandSet:Lcom/android/server/tv/DataBaseDesk$MS_Factory_NS_VD_SET;

    const/4 v0, 0x1

    return v0
.end method

.method public setNoStandVifSet(Lcom/android/server/tv/DataBaseDesk$MS_Factory_NS_VIF_SET;)Z
    .locals 1
    .param p1    # Lcom/android/server/tv/DataBaseDesk$MS_Factory_NS_VIF_SET;

    iput-object p1, p0, Lcom/android/server/tv/DataBaseDeskImpl;->mVifSet:Lcom/android/server/tv/DataBaseDesk$MS_Factory_NS_VIF_SET;

    const/4 v0, 0x1

    return v0
.end method

.method public setSound(Lcom/android/server/tv/DataBaseDesk$MS_USER_SOUND_SETTING;)Z
    .locals 1
    .param p1    # Lcom/android/server/tv/DataBaseDesk$MS_USER_SOUND_SETTING;

    iput-object p1, p0, Lcom/android/server/tv/DataBaseDeskImpl;->soundpara:Lcom/android/server/tv/DataBaseDesk$MS_USER_SOUND_SETTING;

    const/4 v0, 0x0

    return v0
.end method

.method public setSoundMode(Lcom/android/server/tv/DataBaseDesk$EN_SOUND_MODE;Lcom/android/server/tv/DataBaseDesk$SoundModeSeting;)Z
    .locals 2
    .param p1    # Lcom/android/server/tv/DataBaseDesk$EN_SOUND_MODE;
    .param p2    # Lcom/android/server/tv/DataBaseDesk$SoundModeSeting;

    invoke-virtual {p1}, Lcom/android/server/tv/DataBaseDesk$EN_SOUND_MODE;->ordinal()I

    move-result v0

    iget-object v1, p0, Lcom/android/server/tv/DataBaseDeskImpl;->astSoundModeSetting:[Lcom/android/server/tv/DataBaseDesk$SoundModeSeting;

    aput-object p2, v1, v0

    const/4 v1, 0x1

    return v1
.end method

.method public setSscSet(Lcom/android/server/tv/DataBaseDesk$MS_FACTORY_SSC_SET;)Z
    .locals 1
    .param p1    # Lcom/android/server/tv/DataBaseDesk$MS_FACTORY_SSC_SET;

    iput-object p1, p0, Lcom/android/server/tv/DataBaseDeskImpl;->mSscSet:Lcom/android/server/tv/DataBaseDesk$MS_FACTORY_SSC_SET;

    const/4 v0, 0x1

    return v0
.end method

.method public setSubtitleSet(Lcom/android/server/tv/DataBaseDesk$MS_USER_SUBTITLE_SETTING;)Z
    .locals 1
    .param p1    # Lcom/android/server/tv/DataBaseDesk$MS_USER_SUBTITLE_SETTING;

    iput-object p1, p0, Lcom/android/server/tv/DataBaseDeskImpl;->stSubtitleSet:Lcom/android/server/tv/DataBaseDesk$MS_USER_SUBTITLE_SETTING;

    const/4 v0, 0x1

    return v0
.end method

.method public setTableDirty(I)V
    .locals 2
    .param p1    # I

    iget-object v0, p0, Lcom/android/server/tv/DataBaseDeskImpl;->tableDirtyFlags:[Ljava/lang/Boolean;

    const/4 v1, 0x1

    invoke-static {v1}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v1

    aput-object v1, v0, p1

    return-void
.end method

.method public setUsrData(Lcom/android/server/tv/DataBaseDesk$MS_USER_SYSTEM_SETTING;)Z
    .locals 1
    .param p1    # Lcom/android/server/tv/DataBaseDesk$MS_USER_SYSTEM_SETTING;

    iput-object p1, p0, Lcom/android/server/tv/DataBaseDeskImpl;->stUsrData:Lcom/android/server/tv/DataBaseDesk$MS_USER_SYSTEM_SETTING;

    const/4 v0, 0x1

    return v0
.end method

.method public setVideo(Lcom/android/server/tv/DataBaseDesk$T_MS_VIDEO;)Z
    .locals 1
    .param p1    # Lcom/android/server/tv/DataBaseDesk$T_MS_VIDEO;

    iput-object p1, p0, Lcom/android/server/tv/DataBaseDeskImpl;->videopara:Lcom/android/server/tv/DataBaseDesk$T_MS_VIDEO;

    const/4 v0, 0x1

    return v0
.end method

.method public setVideoTemp(Lcom/android/server/tv/DataBaseDesk$T_MS_COLOR_TEMP;)Z
    .locals 1
    .param p1    # Lcom/android/server/tv/DataBaseDesk$T_MS_COLOR_TEMP;

    iput-object p1, p0, Lcom/android/server/tv/DataBaseDeskImpl;->m_stFactoryColorTemp:Lcom/android/server/tv/DataBaseDesk$T_MS_COLOR_TEMP;

    const/4 v0, 0x1

    return v0
.end method

.method public setVideoTempEx(Lcom/android/server/tv/DataBaseDesk$T_MS_COLOR_TEMPEX_DATA;)Z
    .locals 1
    .param p1    # Lcom/android/server/tv/DataBaseDesk$T_MS_COLOR_TEMPEX_DATA;

    iput-object p1, p0, Lcom/android/server/tv/DataBaseDeskImpl;->colorParaEx:Lcom/android/server/tv/DataBaseDesk$T_MS_COLOR_TEMPEX_DATA;

    const/4 v0, 0x1

    return v0
.end method

.method public updateADCAdjust(Lcom/android/server/tv/DataBaseDesk$T_MS_CALIBRATION_DATA;I)V
    .locals 8
    .param p1    # Lcom/android/server/tv/DataBaseDesk$T_MS_CALIBRATION_DATA;
    .param p2    # I

    const-wide/16 v1, -0x1

    new-instance v3, Landroid/content/ContentValues;

    invoke-direct {v3}, Landroid/content/ContentValues;-><init>()V

    const-string v4, "u16RedGain"

    iget v5, p1, Lcom/android/server/tv/DataBaseDesk$T_MS_CALIBRATION_DATA;->redgain:I

    invoke-static {v5}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v5

    invoke-virtual {v3, v4, v5}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Integer;)V

    const-string v4, "u16GreenGain"

    iget v5, p1, Lcom/android/server/tv/DataBaseDesk$T_MS_CALIBRATION_DATA;->greengain:I

    invoke-static {v5}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v5

    invoke-virtual {v3, v4, v5}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Integer;)V

    const-string v4, "u16BlueGain"

    iget v5, p1, Lcom/android/server/tv/DataBaseDesk$T_MS_CALIBRATION_DATA;->bluegain:I

    invoke-static {v5}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v5

    invoke-virtual {v3, v4, v5}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Integer;)V

    const-string v4, "u16RedOffset"

    iget v5, p1, Lcom/android/server/tv/DataBaseDesk$T_MS_CALIBRATION_DATA;->redoffset:I

    invoke-static {v5}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v5

    invoke-virtual {v3, v4, v5}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Integer;)V

    const-string v4, "u16GreenOffset"

    iget v5, p1, Lcom/android/server/tv/DataBaseDesk$T_MS_CALIBRATION_DATA;->greenoffset:I

    invoke-static {v5}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v5

    invoke-virtual {v3, v4, v5}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Integer;)V

    const-string v4, "u16BlueOffset"

    iget v5, p1, Lcom/android/server/tv/DataBaseDesk$T_MS_CALIBRATION_DATA;->blueoffset:I

    invoke-static {v5}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v5

    invoke-virtual {v3, v4, v5}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Integer;)V

    :try_start_0
    invoke-virtual {p0}, Lcom/android/server/tv/DataBaseDeskImpl;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v4

    new-instance v5, Ljava/lang/StringBuilder;

    invoke-direct {v5}, Ljava/lang/StringBuilder;-><init>()V

    const-string v6, "content://mstar.tv.factory/adcadjust/sourceid/"

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5, p2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    invoke-static {v5}, Landroid/net/Uri;->parse(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v5

    const/4 v6, 0x0

    const/4 v7, 0x0

    invoke-virtual {v4, v5, v3, v6, v7}, Landroid/content/ContentResolver;->update(Landroid/net/Uri;Landroid/content/ContentValues;Ljava/lang/String;[Ljava/lang/String;)I
    :try_end_0
    .catch Landroid/database/SQLException; {:try_start_0 .. :try_end_0} :catch_1

    move-result v4

    int-to-long v1, v4

    :goto_0
    const-wide/16 v4, -0x1

    cmp-long v4, v1, v4

    if-nez v4, :cond_0

    sget-object v4, Ljava/lang/System;->out:Ljava/io/PrintStream;

    const-string v5, "update tbl_ADCAdjust ignored"

    invoke-virtual {v4, v5}, Ljava/io/PrintStream;->println(Ljava/lang/String;)V

    :cond_0
    :try_start_1
    invoke-static {}, Lcom/mstar/android/tvapi/common/TvManager;->getInstance()Lcom/mstar/android/tvapi/common/TvManager;

    move-result-object v4

    if-eqz v4, :cond_1

    invoke-static {}, Lcom/mstar/android/tvapi/common/TvManager;->getInstance()Lcom/mstar/android/tvapi/common/TvManager;

    move-result-object v4

    invoke-virtual {v4}, Lcom/mstar/android/tvapi/common/TvManager;->getDatabaseManager()Lcom/mstar/android/tvapi/common/DatabaseManager;

    move-result-object v4

    const/16 v5, 0x24

    invoke-virtual {v4, v5}, Lcom/mstar/android/tvapi/common/DatabaseManager;->setDatabaseDirtyByApplication(S)V
    :try_end_1
    .catch Lcom/mstar/android/tvapi/common/exception/TvCommonException; {:try_start_1 .. :try_end_1} :catch_0

    :cond_1
    :goto_1
    return-void

    :catch_0
    move-exception v0

    invoke-virtual {v0}, Lcom/mstar/android/tvapi/common/exception/TvCommonException;->printStackTrace()V

    goto :goto_1

    :catch_1
    move-exception v4

    goto :goto_0
.end method

.method public updateADCAdjust(Lcom/mstar/android/tvapi/factory/vo/PqlCalibrationData;I)V
    .locals 8
    .param p1    # Lcom/mstar/android/tvapi/factory/vo/PqlCalibrationData;
    .param p2    # I

    const-wide/16 v1, -0x1

    new-instance v3, Landroid/content/ContentValues;

    invoke-direct {v3}, Landroid/content/ContentValues;-><init>()V

    const-string v4, "u16RedGain"

    iget v5, p1, Lcom/mstar/android/tvapi/factory/vo/PqlCalibrationData;->redGain:I

    invoke-static {v5}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v5

    invoke-virtual {v3, v4, v5}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Integer;)V

    const-string v4, "u16GreenGain"

    iget v5, p1, Lcom/mstar/android/tvapi/factory/vo/PqlCalibrationData;->greenGain:I

    invoke-static {v5}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v5

    invoke-virtual {v3, v4, v5}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Integer;)V

    const-string v4, "u16BlueGain"

    iget v5, p1, Lcom/mstar/android/tvapi/factory/vo/PqlCalibrationData;->blueGain:I

    invoke-static {v5}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v5

    invoke-virtual {v3, v4, v5}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Integer;)V

    const-string v4, "u16RedOffset"

    iget v5, p1, Lcom/mstar/android/tvapi/factory/vo/PqlCalibrationData;->redOffset:I

    invoke-static {v5}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v5

    invoke-virtual {v3, v4, v5}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Integer;)V

    const-string v4, "u16GreenOffset"

    iget v5, p1, Lcom/mstar/android/tvapi/factory/vo/PqlCalibrationData;->greenOffset:I

    invoke-static {v5}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v5

    invoke-virtual {v3, v4, v5}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Integer;)V

    const-string v4, "u16BlueOffset"

    iget v5, p1, Lcom/mstar/android/tvapi/factory/vo/PqlCalibrationData;->blueOffset:I

    invoke-static {v5}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v5

    invoke-virtual {v3, v4, v5}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Integer;)V

    :try_start_0
    invoke-virtual {p0}, Lcom/android/server/tv/DataBaseDeskImpl;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v4

    new-instance v5, Ljava/lang/StringBuilder;

    invoke-direct {v5}, Ljava/lang/StringBuilder;-><init>()V

    const-string v6, "content://mstar.tv.factory/adcadjust/sourceid/"

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5, p2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    invoke-static {v5}, Landroid/net/Uri;->parse(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v5

    const/4 v6, 0x0

    const/4 v7, 0x0

    invoke-virtual {v4, v5, v3, v6, v7}, Landroid/content/ContentResolver;->update(Landroid/net/Uri;Landroid/content/ContentValues;Ljava/lang/String;[Ljava/lang/String;)I
    :try_end_0
    .catch Landroid/database/SQLException; {:try_start_0 .. :try_end_0} :catch_1

    move-result v4

    int-to-long v1, v4

    :goto_0
    const-wide/16 v4, -0x1

    cmp-long v4, v1, v4

    if-nez v4, :cond_0

    sget-object v4, Ljava/lang/System;->out:Ljava/io/PrintStream;

    const-string v5, "update tbl_ADCAdjust ignored"

    invoke-virtual {v4, v5}, Ljava/io/PrintStream;->println(Ljava/lang/String;)V

    :cond_0
    :try_start_1
    invoke-static {}, Lcom/mstar/android/tvapi/common/TvManager;->getInstance()Lcom/mstar/android/tvapi/common/TvManager;

    move-result-object v4

    if-eqz v4, :cond_1

    invoke-static {}, Lcom/mstar/android/tvapi/common/TvManager;->getInstance()Lcom/mstar/android/tvapi/common/TvManager;

    move-result-object v4

    invoke-virtual {v4}, Lcom/mstar/android/tvapi/common/TvManager;->getDatabaseManager()Lcom/mstar/android/tvapi/common/DatabaseManager;

    move-result-object v4

    const/16 v5, 0x24

    invoke-virtual {v4, v5}, Lcom/mstar/android/tvapi/common/DatabaseManager;->setDatabaseDirtyByApplication(S)V
    :try_end_1
    .catch Lcom/mstar/android/tvapi/common/exception/TvCommonException; {:try_start_1 .. :try_end_1} :catch_0

    :cond_1
    :goto_1
    return-void

    :catch_0
    move-exception v0

    invoke-virtual {v0}, Lcom/mstar/android/tvapi/common/exception/TvCommonException;->printStackTrace()V

    goto :goto_1

    :catch_1
    move-exception v4

    goto :goto_0
.end method

.method public updateATVOverscanAdjust(II[[Lcom/android/server/tv/DataBaseDesk$ST_MAPI_VIDEO_WINDOW_INFO;)V
    .locals 8
    .param p1    # I
    .param p2    # I
    .param p3    # [[Lcom/android/server/tv/DataBaseDesk$ST_MAPI_VIDEO_WINDOW_INFO;

    const-wide/16 v1, -0x1

    new-instance v3, Landroid/content/ContentValues;

    invoke-direct {v3}, Landroid/content/ContentValues;-><init>()V

    const-string v4, "u16H_CapStart"

    aget-object v5, p3, p1

    aget-object v5, v5, p2

    iget v5, v5, Lcom/android/server/tv/DataBaseDesk$ST_MAPI_VIDEO_WINDOW_INFO;->u16H_CapStart:I

    invoke-static {v5}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v5

    invoke-virtual {v3, v4, v5}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Integer;)V

    const-string v4, "u16V_CapStart"

    aget-object v5, p3, p1

    aget-object v5, v5, p2

    iget v5, v5, Lcom/android/server/tv/DataBaseDesk$ST_MAPI_VIDEO_WINDOW_INFO;->u16V_CapStart:I

    invoke-static {v5}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v5

    invoke-virtual {v3, v4, v5}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Integer;)V

    const-string v4, "u8HCrop_Left"

    aget-object v5, p3, p1

    aget-object v5, v5, p2

    iget-short v5, v5, Lcom/android/server/tv/DataBaseDesk$ST_MAPI_VIDEO_WINDOW_INFO;->u8HCrop_Left:S

    invoke-static {v5}, Ljava/lang/Short;->valueOf(S)Ljava/lang/Short;

    move-result-object v5

    invoke-virtual {v3, v4, v5}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Short;)V

    const-string v4, "u8HCrop_Right"

    aget-object v5, p3, p1

    aget-object v5, v5, p2

    iget-short v5, v5, Lcom/android/server/tv/DataBaseDesk$ST_MAPI_VIDEO_WINDOW_INFO;->u8HCrop_Right:S

    invoke-static {v5}, Ljava/lang/Short;->valueOf(S)Ljava/lang/Short;

    move-result-object v5

    invoke-virtual {v3, v4, v5}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Short;)V

    const-string v4, "u8VCrop_Up"

    aget-object v5, p3, p1

    aget-object v5, v5, p2

    iget-short v5, v5, Lcom/android/server/tv/DataBaseDesk$ST_MAPI_VIDEO_WINDOW_INFO;->u8VCrop_Up:S

    invoke-static {v5}, Ljava/lang/Short;->valueOf(S)Ljava/lang/Short;

    move-result-object v5

    invoke-virtual {v3, v4, v5}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Short;)V

    const-string v4, "u8VCrop_Down"

    aget-object v5, p3, p1

    aget-object v5, v5, p2

    iget-short v5, v5, Lcom/android/server/tv/DataBaseDesk$ST_MAPI_VIDEO_WINDOW_INFO;->u8VCrop_Down:S

    invoke-static {v5}, Ljava/lang/Short;->valueOf(S)Ljava/lang/Short;

    move-result-object v5

    invoke-virtual {v3, v4, v5}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Short;)V

    :try_start_0
    invoke-virtual {p0}, Lcom/android/server/tv/DataBaseDeskImpl;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v4

    new-instance v5, Ljava/lang/StringBuilder;

    invoke-direct {v5}, Ljava/lang/StringBuilder;-><init>()V

    const-string v6, "content://mstar.tv.factory/overscanadjust/factoryoverscantype/"

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5, p1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v5

    const-string v6, "/_id/"

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5, p2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    invoke-static {v5}, Landroid/net/Uri;->parse(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v5

    const/4 v6, 0x0

    const/4 v7, 0x0

    invoke-virtual {v4, v5, v3, v6, v7}, Landroid/content/ContentResolver;->update(Landroid/net/Uri;Landroid/content/ContentValues;Ljava/lang/String;[Ljava/lang/String;)I
    :try_end_0
    .catch Landroid/database/SQLException; {:try_start_0 .. :try_end_0} :catch_1

    move-result v4

    int-to-long v1, v4

    :goto_0
    const-wide/16 v4, -0x1

    cmp-long v4, v1, v4

    if-nez v4, :cond_0

    sget-object v4, Ljava/lang/System;->out:Ljava/io/PrintStream;

    const-string v5, "update tbl_ATVOverscanSetting ignored"

    invoke-virtual {v4, v5}, Ljava/io/PrintStream;->println(Ljava/lang/String;)V

    :cond_0
    :try_start_1
    invoke-static {}, Lcom/mstar/android/tvapi/common/TvManager;->getInstance()Lcom/mstar/android/tvapi/common/TvManager;

    move-result-object v4

    if-eqz v4, :cond_1

    invoke-static {}, Lcom/mstar/android/tvapi/common/TvManager;->getInstance()Lcom/mstar/android/tvapi/common/TvManager;

    move-result-object v4

    invoke-virtual {v4}, Lcom/mstar/android/tvapi/common/TvManager;->getDatabaseManager()Lcom/mstar/android/tvapi/common/DatabaseManager;

    move-result-object v4

    const/16 v5, 0x2b

    invoke-virtual {v4, v5}, Lcom/mstar/android/tvapi/common/DatabaseManager;->setDatabaseDirtyByApplication(S)V
    :try_end_1
    .catch Lcom/mstar/android/tvapi/common/exception/TvCommonException; {:try_start_1 .. :try_end_1} :catch_0

    :cond_1
    :goto_1
    return-void

    :catch_0
    move-exception v0

    invoke-virtual {v0}, Lcom/mstar/android/tvapi/common/exception/TvCommonException;->printStackTrace()V

    goto :goto_1

    :catch_1
    move-exception v4

    goto :goto_0
.end method

.method public updateAntennaType(I)V
    .locals 8
    .param p1    # I

    const-wide/16 v1, -0x1

    new-instance v3, Landroid/content/ContentValues;

    invoke-direct {v3}, Landroid/content/ContentValues;-><init>()V

    const-string v4, "AntennaType"

    invoke-static {p1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v5

    invoke-virtual {v3, v4, v5}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Integer;)V

    :try_start_0
    invoke-virtual {p0}, Lcom/android/server/tv/DataBaseDeskImpl;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v4

    const-string v5, "content://mstar.tv.usersetting/mediumsetting"

    invoke-static {v5}, Landroid/net/Uri;->parse(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v5

    const/4 v6, 0x0

    const/4 v7, 0x0

    invoke-virtual {v4, v5, v3, v6, v7}, Landroid/content/ContentResolver;->update(Landroid/net/Uri;Landroid/content/ContentValues;Ljava/lang/String;[Ljava/lang/String;)I
    :try_end_0
    .catch Landroid/database/SQLException; {:try_start_0 .. :try_end_0} :catch_1

    move-result v4

    int-to-long v1, v4

    :goto_0
    const-wide/16 v4, -0x1

    cmp-long v4, v1, v4

    if-nez v4, :cond_0

    sget-object v4, Ljava/lang/System;->out:Ljava/io/PrintStream;

    const-string v5, "update tbl_SystemSetting ignored"

    invoke-virtual {v4, v5}, Ljava/io/PrintStream;->println(Ljava/lang/String;)V

    :cond_0
    :try_start_1
    invoke-static {}, Lcom/mstar/android/tvapi/common/TvManager;->getInstance()Lcom/mstar/android/tvapi/common/TvManager;

    move-result-object v4

    if-eqz v4, :cond_1

    invoke-static {}, Lcom/mstar/android/tvapi/common/TvManager;->getInstance()Lcom/mstar/android/tvapi/common/TvManager;

    move-result-object v4

    invoke-virtual {v4}, Lcom/mstar/android/tvapi/common/TvManager;->getDatabaseManager()Lcom/mstar/android/tvapi/common/DatabaseManager;

    move-result-object v4

    const/16 v5, 0x19

    invoke-virtual {v4, v5}, Lcom/mstar/android/tvapi/common/DatabaseManager;->setDatabaseDirtyByApplication(S)V
    :try_end_1
    .catch Lcom/mstar/android/tvapi/common/exception/TvCommonException; {:try_start_1 .. :try_end_1} :catch_0

    :cond_1
    :goto_1
    return-void

    :catch_0
    move-exception v0

    invoke-virtual {v0}, Lcom/mstar/android/tvapi/common/exception/TvCommonException;->printStackTrace()V

    goto :goto_1

    :catch_1
    move-exception v4

    goto :goto_0
.end method

.method public updateArcMode(II)V
    .locals 6
    .param p1    # I
    .param p2    # I

    new-instance v1, Landroid/content/ContentValues;

    invoke-direct {v1}, Landroid/content/ContentValues;-><init>()V

    const-string v2, "enARCType"

    invoke-static {p1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v3

    invoke-virtual {v1, v2, v3}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Integer;)V

    :try_start_0
    invoke-virtual {p0}, Lcom/android/server/tv/DataBaseDeskImpl;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v2

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string v4, "content://mstar.tv.usersetting/videosetting/inputsrc/"

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3, p2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v3}, Landroid/net/Uri;->parse(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v3

    const/4 v4, 0x0

    const/4 v5, 0x0

    invoke-virtual {v2, v3, v1, v4, v5}, Landroid/content/ContentResolver;->update(Landroid/net/Uri;Landroid/content/ContentValues;Ljava/lang/String;[Ljava/lang/String;)I
    :try_end_0
    .catch Landroid/database/SQLException; {:try_start_0 .. :try_end_0} :catch_0

    :goto_0
    return-void

    :catch_0
    move-exception v0

    const-string v2, "MiniDb"

    const-string v3, "updateArcMode failed"

    invoke-static {v2, v3}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_0
.end method

.method public updateAvc(I)V
    .locals 5
    .param p1    # I

    const/4 v1, 0x1

    new-instance v0, Landroid/content/ContentValues;

    invoke-direct {v0}, Landroid/content/ContentValues;-><init>()V

    const-string v2, "bEnableAVC"

    if-ne p1, v1, :cond_0

    :goto_0
    invoke-static {v1}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v1

    invoke-virtual {v0, v2, v1}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Boolean;)V

    :try_start_0
    invoke-virtual {p0}, Lcom/android/server/tv/DataBaseDeskImpl;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v1

    const-string v2, "content://mstar.tv.usersetting/soundsetting"

    invoke-static {v2}, Landroid/net/Uri;->parse(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v2

    const/4 v3, 0x0

    const/4 v4, 0x0

    invoke-virtual {v1, v2, v0, v3, v4}, Landroid/content/ContentResolver;->update(Landroid/net/Uri;Landroid/content/ContentValues;Ljava/lang/String;[Ljava/lang/String;)I
    :try_end_0
    .catch Landroid/database/SQLException; {:try_start_0 .. :try_end_0} :catch_0

    :goto_1
    return-void

    :cond_0
    const/4 v1, 0x0

    goto :goto_0

    :catch_0
    move-exception v1

    goto :goto_1
.end method

.method public updateBalance(I)V
    .locals 5
    .param p1    # I

    new-instance v0, Landroid/content/ContentValues;

    invoke-direct {v0}, Landroid/content/ContentValues;-><init>()V

    const-string v1, "Balance"

    invoke-static {p1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Integer;)V

    :try_start_0
    invoke-virtual {p0}, Lcom/android/server/tv/DataBaseDeskImpl;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v1

    const-string v2, "content://mstar.tv.usersetting/soundsetting"

    invoke-static {v2}, Landroid/net/Uri;->parse(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v2

    const/4 v3, 0x0

    const/4 v4, 0x0

    invoke-virtual {v1, v2, v0, v3, v4}, Landroid/content/ContentResolver;->update(Landroid/net/Uri;Landroid/content/ContentValues;Ljava/lang/String;[Ljava/lang/String;)I
    :try_end_0
    .catch Landroid/database/SQLException; {:try_start_0 .. :try_end_0} :catch_0

    :goto_0
    return-void

    :catch_0
    move-exception v1

    goto :goto_0
.end method

.method public updateBass(II)V
    .locals 5
    .param p1    # I
    .param p2    # I

    new-instance v0, Landroid/content/ContentValues;

    invoke-direct {v0}, Landroid/content/ContentValues;-><init>()V

    const-string v1, "Bass"

    invoke-static {p1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Integer;)V

    :try_start_0
    invoke-virtual {p0}, Lcom/android/server/tv/DataBaseDeskImpl;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v1

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    iget-object v3, p0, Lcom/android/server/tv/DataBaseDeskImpl;->userSettingCusSchema:Ljava/lang/String;

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, "/soundmodesetting/"

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, p2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v2}, Landroid/net/Uri;->parse(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v2

    const/4 v3, 0x0

    const/4 v4, 0x0

    invoke-virtual {v1, v2, v0, v3, v4}, Landroid/content/ContentResolver;->update(Landroid/net/Uri;Landroid/content/ContentValues;Ljava/lang/String;[Ljava/lang/String;)I
    :try_end_0
    .catch Landroid/database/SQLException; {:try_start_0 .. :try_end_0} :catch_0

    :goto_0
    return-void

    :catch_0
    move-exception v1

    goto :goto_0
.end method

.method public updateBassSwitch(I)V
    .locals 5
    .param p1    # I

    new-instance v0, Landroid/content/ContentValues;

    invoke-direct {v0}, Landroid/content/ContentValues;-><init>()V

    const-string v1, "bEnableHeavyBass"

    invoke-static {p1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Integer;)V

    :try_start_0
    invoke-virtual {p0}, Lcom/android/server/tv/DataBaseDeskImpl;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v1

    const-string v2, "content://mstar.tv.usersetting/soundsetting"

    invoke-static {v2}, Landroid/net/Uri;->parse(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v2

    const/4 v3, 0x0

    const/4 v4, 0x0

    invoke-virtual {v1, v2, v0, v3, v4}, Landroid/content/ContentResolver;->update(Landroid/net/Uri;Landroid/content/ContentValues;Ljava/lang/String;[Ljava/lang/String;)I
    :try_end_0
    .catch Landroid/database/SQLException; {:try_start_0 .. :try_end_0} :catch_0

    :goto_0
    return-void

    :catch_0
    move-exception v1

    goto :goto_0
.end method

.method public updateBassVolume(I)V
    .locals 5
    .param p1    # I

    new-instance v0, Landroid/content/ContentValues;

    invoke-direct {v0}, Landroid/content/ContentValues;-><init>()V

    const-string v1, "HeavyBassVolume"

    invoke-static {p1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Integer;)V

    :try_start_0
    invoke-virtual {p0}, Lcom/android/server/tv/DataBaseDeskImpl;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v1

    const-string v2, "content://mstar.tv.usersetting/soundsetting"

    invoke-static {v2}, Landroid/net/Uri;->parse(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v2

    const/4 v3, 0x0

    const/4 v4, 0x0

    invoke-virtual {v1, v2, v0, v3, v4}, Landroid/content/ContentResolver;->update(Landroid/net/Uri;Landroid/content/ContentValues;Ljava/lang/String;[Ljava/lang/String;)I
    :try_end_0
    .catch Landroid/database/SQLException; {:try_start_0 .. :try_end_0} :catch_0

    :goto_0
    return-void

    :catch_0
    move-exception v1

    goto :goto_0
.end method

.method public updateChSwMode(Lcom/android/server/tv/DataBaseDesk$EN_MS_CHANNEL_SWITCH_MODE;)V
    .locals 8
    .param p1    # Lcom/android/server/tv/DataBaseDesk$EN_MS_CHANNEL_SWITCH_MODE;

    const-wide/16 v1, -0x1

    new-instance v3, Landroid/content/ContentValues;

    invoke-direct {v3}, Landroid/content/ContentValues;-><init>()V

    const-string v4, "bATVChSwitchFreeze"

    invoke-virtual {p1}, Lcom/android/server/tv/DataBaseDesk$EN_MS_CHANNEL_SWITCH_MODE;->ordinal()I

    move-result v5

    invoke-static {v5}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v5

    invoke-virtual {v3, v4, v5}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Integer;)V

    :try_start_0
    invoke-virtual {p0}, Lcom/android/server/tv/DataBaseDeskImpl;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v4

    const-string v5, "content://mstar.tv.usersetting/systemsetting"

    invoke-static {v5}, Landroid/net/Uri;->parse(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v5

    const/4 v6, 0x0

    const/4 v7, 0x0

    invoke-virtual {v4, v5, v3, v6, v7}, Landroid/content/ContentResolver;->update(Landroid/net/Uri;Landroid/content/ContentValues;Ljava/lang/String;[Ljava/lang/String;)I
    :try_end_0
    .catch Landroid/database/SQLException; {:try_start_0 .. :try_end_0} :catch_1

    move-result v4

    int-to-long v1, v4

    :goto_0
    const-wide/16 v4, -0x1

    cmp-long v4, v1, v4

    if-nez v4, :cond_0

    sget-object v4, Ljava/lang/System;->out:Ljava/io/PrintStream;

    const-string v5, "update tbl_SystemSetting ignored"

    invoke-virtual {v4, v5}, Ljava/io/PrintStream;->println(Ljava/lang/String;)V

    :cond_0
    :try_start_1
    invoke-static {}, Lcom/mstar/android/tvapi/common/TvManager;->getInstance()Lcom/mstar/android/tvapi/common/TvManager;

    move-result-object v4

    if-eqz v4, :cond_1

    invoke-static {}, Lcom/mstar/android/tvapi/common/TvManager;->getInstance()Lcom/mstar/android/tvapi/common/TvManager;

    move-result-object v4

    invoke-virtual {v4}, Lcom/mstar/android/tvapi/common/TvManager;->getDatabaseManager()Lcom/mstar/android/tvapi/common/DatabaseManager;

    move-result-object v4

    const/16 v5, 0x19

    invoke-virtual {v4, v5}, Lcom/mstar/android/tvapi/common/DatabaseManager;->setDatabaseDirtyByApplication(S)V
    :try_end_1
    .catch Lcom/mstar/android/tvapi/common/exception/TvCommonException; {:try_start_1 .. :try_end_1} :catch_0

    :cond_1
    :goto_1
    return-void

    :catch_0
    move-exception v0

    invoke-virtual {v0}, Lcom/mstar/android/tvapi/common/exception/TvCommonException;->printStackTrace()V

    goto :goto_1

    :catch_1
    move-exception v4

    goto :goto_0
.end method

.method public updateColorTempIdx(IILcom/android/server/tv/DataBaseDesk$EN_MS_COLOR_TEMP;)V
    .locals 6
    .param p1    # I
    .param p2    # I
    .param p3    # Lcom/android/server/tv/DataBaseDesk$EN_MS_COLOR_TEMP;

    new-instance v1, Landroid/content/ContentValues;

    invoke-direct {v1}, Landroid/content/ContentValues;-><init>()V

    const-string v2, "eColorTemp"

    invoke-virtual {p3}, Lcom/android/server/tv/DataBaseDesk$EN_MS_COLOR_TEMP;->ordinal()I

    move-result v3

    invoke-static {v3}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v3

    invoke-virtual {v1, v2, v3}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Integer;)V

    :try_start_0
    invoke-virtual {p0}, Lcom/android/server/tv/DataBaseDeskImpl;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v2

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    iget-object v4, p0, Lcom/android/server/tv/DataBaseDeskImpl;->userSettingCusSchema:Ljava/lang/String;

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    const-string v4, "/picmode_setting/inputsrc/"

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3, p1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v3

    const-string v4, "/picmode/"

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3, p2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v3}, Landroid/net/Uri;->parse(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v3

    const/4 v4, 0x0

    const/4 v5, 0x0

    invoke-virtual {v2, v3, v1, v4, v5}, Landroid/content/ContentResolver;->update(Landroid/net/Uri;Landroid/content/ContentValues;Ljava/lang/String;[Ljava/lang/String;)I
    :try_end_0
    .catch Landroid/database/SQLException; {:try_start_0 .. :try_end_0} :catch_0

    :goto_0
    return-void

    :catch_0
    move-exception v0

    const-string v2, "MiniDb"

    const-string v3, "updateColorTempIdx failed"

    invoke-static {v2, v3}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_0
.end method

.method public updateCurInputSrc(I)V
    .locals 7
    .param p1    # I

    const-wide/16 v0, -0x1

    new-instance v2, Landroid/content/ContentValues;

    invoke-direct {v2}, Landroid/content/ContentValues;-><init>()V

    const-string v3, "enInputSourceType"

    invoke-static {p1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v4

    invoke-virtual {v2, v3, v4}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Integer;)V

    :try_start_0
    invoke-virtual {p0}, Lcom/android/server/tv/DataBaseDeskImpl;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v3

    const-string v4, "content://mstar.tv.usersetting/systemsetting"

    invoke-static {v4}, Landroid/net/Uri;->parse(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v4

    const/4 v5, 0x0

    const/4 v6, 0x0

    invoke-virtual {v3, v4, v2, v5, v6}, Landroid/content/ContentResolver;->update(Landroid/net/Uri;Landroid/content/ContentValues;Ljava/lang/String;[Ljava/lang/String;)I
    :try_end_0
    .catch Landroid/database/SQLException; {:try_start_0 .. :try_end_0} :catch_0

    move-result v3

    int-to-long v0, v3

    :goto_0
    const-wide/16 v3, -0x1

    cmp-long v3, v0, v3

    if-nez v3, :cond_0

    sget-object v3, Ljava/lang/System;->out:Ljava/io/PrintStream;

    const-string v4, "update tbl_SystemSetting field enInputSourceType ignored"

    invoke-virtual {v3, v4}, Ljava/io/PrintStream;->println(Ljava/lang/String;)V

    :cond_0
    return-void

    :catch_0
    move-exception v3

    goto :goto_0
.end method

.method public updateDGClarity(I)V
    .locals 5
    .param p1    # I

    new-instance v0, Landroid/content/ContentValues;

    invoke-direct {v0}, Landroid/content/ContentValues;-><init>()V

    const-string v1, "DGClarity"

    invoke-static {p1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Integer;)V

    :try_start_0
    invoke-virtual {p0}, Lcom/android/server/tv/DataBaseDeskImpl;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v1

    const-string v2, "content://mstar.tv.usersetting/soundsetting"

    invoke-static {v2}, Landroid/net/Uri;->parse(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v2

    const/4 v3, 0x0

    const/4 v4, 0x0

    invoke-virtual {v1, v2, v0, v3, v4}, Landroid/content/ContentResolver;->update(Landroid/net/Uri;Landroid/content/ContentValues;Ljava/lang/String;[Ljava/lang/String;)I
    :try_end_0
    .catch Landroid/database/SQLException; {:try_start_0 .. :try_end_0} :catch_0

    :goto_0
    return-void

    :catch_0
    move-exception v1

    goto :goto_0
.end method

.method public updateDTVOverscanAdjust(II[[Lcom/android/server/tv/DataBaseDesk$ST_MAPI_VIDEO_WINDOW_INFO;)V
    .locals 8
    .param p1    # I
    .param p2    # I
    .param p3    # [[Lcom/android/server/tv/DataBaseDesk$ST_MAPI_VIDEO_WINDOW_INFO;

    const-wide/16 v1, -0x1

    new-instance v3, Landroid/content/ContentValues;

    invoke-direct {v3}, Landroid/content/ContentValues;-><init>()V

    const-string v4, "u16H_CapStart"

    aget-object v5, p3, p1

    aget-object v5, v5, p2

    iget v5, v5, Lcom/android/server/tv/DataBaseDesk$ST_MAPI_VIDEO_WINDOW_INFO;->u16H_CapStart:I

    invoke-static {v5}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v5

    invoke-virtual {v3, v4, v5}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Integer;)V

    const-string v4, "u16V_CapStart"

    aget-object v5, p3, p1

    aget-object v5, v5, p2

    iget v5, v5, Lcom/android/server/tv/DataBaseDesk$ST_MAPI_VIDEO_WINDOW_INFO;->u16V_CapStart:I

    invoke-static {v5}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v5

    invoke-virtual {v3, v4, v5}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Integer;)V

    const-string v4, "u8HCrop_Left"

    aget-object v5, p3, p1

    aget-object v5, v5, p2

    iget-short v5, v5, Lcom/android/server/tv/DataBaseDesk$ST_MAPI_VIDEO_WINDOW_INFO;->u8HCrop_Left:S

    invoke-static {v5}, Ljava/lang/Short;->valueOf(S)Ljava/lang/Short;

    move-result-object v5

    invoke-virtual {v3, v4, v5}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Short;)V

    const-string v4, "u8HCrop_Right"

    aget-object v5, p3, p1

    aget-object v5, v5, p2

    iget-short v5, v5, Lcom/android/server/tv/DataBaseDesk$ST_MAPI_VIDEO_WINDOW_INFO;->u8HCrop_Right:S

    invoke-static {v5}, Ljava/lang/Short;->valueOf(S)Ljava/lang/Short;

    move-result-object v5

    invoke-virtual {v3, v4, v5}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Short;)V

    const-string v4, "u8VCrop_Up"

    aget-object v5, p3, p1

    aget-object v5, v5, p2

    iget-short v5, v5, Lcom/android/server/tv/DataBaseDesk$ST_MAPI_VIDEO_WINDOW_INFO;->u8VCrop_Up:S

    invoke-static {v5}, Ljava/lang/Short;->valueOf(S)Ljava/lang/Short;

    move-result-object v5

    invoke-virtual {v3, v4, v5}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Short;)V

    const-string v4, "u8VCrop_Down"

    aget-object v5, p3, p1

    aget-object v5, v5, p2

    iget-short v5, v5, Lcom/android/server/tv/DataBaseDesk$ST_MAPI_VIDEO_WINDOW_INFO;->u8VCrop_Down:S

    invoke-static {v5}, Ljava/lang/Short;->valueOf(S)Ljava/lang/Short;

    move-result-object v5

    invoke-virtual {v3, v4, v5}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Short;)V

    :try_start_0
    invoke-virtual {p0}, Lcom/android/server/tv/DataBaseDeskImpl;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v4

    new-instance v5, Ljava/lang/StringBuilder;

    invoke-direct {v5}, Ljava/lang/StringBuilder;-><init>()V

    const-string v6, "content://mstar.tv.factory/dtvoverscansetting/resolutiontypenum/"

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5, p1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v5

    const-string v6, "/_id/"

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5, p2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    invoke-static {v5}, Landroid/net/Uri;->parse(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v5

    const/4 v6, 0x0

    const/4 v7, 0x0

    invoke-virtual {v4, v5, v3, v6, v7}, Landroid/content/ContentResolver;->update(Landroid/net/Uri;Landroid/content/ContentValues;Ljava/lang/String;[Ljava/lang/String;)I
    :try_end_0
    .catch Landroid/database/SQLException; {:try_start_0 .. :try_end_0} :catch_1

    move-result v4

    int-to-long v1, v4

    :goto_0
    const-wide/16 v4, -0x1

    cmp-long v4, v1, v4

    if-nez v4, :cond_0

    sget-object v4, Ljava/lang/System;->out:Ljava/io/PrintStream;

    const-string v5, "update tbl_DTVOverscanSetting ignored"

    invoke-virtual {v4, v5}, Ljava/io/PrintStream;->println(Ljava/lang/String;)V

    :cond_0
    :try_start_1
    invoke-static {}, Lcom/mstar/android/tvapi/common/TvManager;->getInstance()Lcom/mstar/android/tvapi/common/TvManager;

    move-result-object v4

    if-eqz v4, :cond_1

    invoke-static {}, Lcom/mstar/android/tvapi/common/TvManager;->getInstance()Lcom/mstar/android/tvapi/common/TvManager;

    move-result-object v4

    invoke-virtual {v4}, Lcom/mstar/android/tvapi/common/TvManager;->getDatabaseManager()Lcom/mstar/android/tvapi/common/DatabaseManager;

    move-result-object v4

    const/16 v5, 0x30

    invoke-virtual {v4, v5}, Lcom/mstar/android/tvapi/common/DatabaseManager;->setDatabaseDirtyByApplication(S)V
    :try_end_1
    .catch Lcom/mstar/android/tvapi/common/exception/TvCommonException; {:try_start_1 .. :try_end_1} :catch_0

    :cond_1
    :goto_1
    return-void

    :catch_0
    move-exception v0

    invoke-virtual {v0}, Lcom/mstar/android/tvapi/common/exception/TvCommonException;->printStackTrace()V

    goto :goto_1

    :catch_1
    move-exception v4

    goto :goto_0
.end method

.method public updateEarPhoneVolume(I)V
    .locals 6
    .param p1    # I

    new-instance v1, Landroid/content/ContentValues;

    invoke-direct {v1}, Landroid/content/ContentValues;-><init>()V

    const-string v2, "HPVolume"

    invoke-static {p1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v3

    invoke-virtual {v1, v2, v3}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Integer;)V

    :try_start_0
    invoke-virtual {p0}, Lcom/android/server/tv/DataBaseDeskImpl;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v2

    const-string v3, "content://mstar.tv.usersetting/soundsetting"

    invoke-static {v3}, Landroid/net/Uri;->parse(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v3

    const/4 v4, 0x0

    const/4 v5, 0x0

    invoke-virtual {v2, v3, v1, v4, v5}, Landroid/content/ContentResolver;->update(Landroid/net/Uri;Landroid/content/ContentValues;Ljava/lang/String;[Ljava/lang/String;)I
    :try_end_0
    .catch Landroid/database/SQLException; {:try_start_0 .. :try_end_0} :catch_1

    :goto_0
    :try_start_1
    invoke-static {}, Lcom/mstar/android/tvapi/common/TvManager;->getInstance()Lcom/mstar/android/tvapi/common/TvManager;

    move-result-object v2

    if-eqz v2, :cond_0

    invoke-static {}, Lcom/mstar/android/tvapi/common/TvManager;->getInstance()Lcom/mstar/android/tvapi/common/TvManager;

    move-result-object v2

    invoke-virtual {v2}, Lcom/mstar/android/tvapi/common/TvManager;->getDatabaseManager()Lcom/mstar/android/tvapi/common/DatabaseManager;

    move-result-object v2

    const/16 v3, 0x17

    invoke-virtual {v2, v3}, Lcom/mstar/android/tvapi/common/DatabaseManager;->setDatabaseDirtyByApplication(S)V
    :try_end_1
    .catch Lcom/mstar/android/tvapi/common/exception/TvCommonException; {:try_start_1 .. :try_end_1} :catch_0

    :cond_0
    :goto_1
    return-void

    :catch_0
    move-exception v0

    invoke-virtual {v0}, Lcom/mstar/android/tvapi/common/exception/TvCommonException;->printStackTrace()V

    goto :goto_1

    :catch_1
    move-exception v2

    goto :goto_0
.end method

.method public updateEventName(Ljava/lang/String;S)V
    .locals 8
    .param p1    # Ljava/lang/String;
    .param p2    # S

    const-wide/16 v1, -0x1

    new-instance v3, Landroid/content/ContentValues;

    invoke-direct {v3}, Landroid/content/ContentValues;-><init>()V

    sget-object v4, Ljava/lang/System;->out:Ljava/io/PrintStream;

    new-instance v5, Ljava/lang/StringBuilder;

    invoke-direct {v5}, Ljava/lang/StringBuilder;-><init>()V

    const-string v6, "\n====>>updateEventName--eventName "

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    const-string v6, " @epgTimerIndex "

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5, p2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v4, v5}, Ljava/io/PrintStream;->println(Ljava/lang/String;)V

    const-string v4, "sEventName"

    invoke-virtual {v3, v4, p1}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    :try_start_0
    invoke-virtual {p0}, Lcom/android/server/tv/DataBaseDeskImpl;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v4

    new-instance v5, Ljava/lang/StringBuilder;

    invoke-direct {v5}, Ljava/lang/StringBuilder;-><init>()V

    const-string v6, "content://mstar.tv.usersetting/epgtimer/"

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5, p2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    invoke-static {v5}, Landroid/net/Uri;->parse(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v5

    const/4 v6, 0x0

    const/4 v7, 0x0

    invoke-virtual {v4, v5, v3, v6, v7}, Landroid/content/ContentResolver;->update(Landroid/net/Uri;Landroid/content/ContentValues;Ljava/lang/String;[Ljava/lang/String;)I
    :try_end_0
    .catch Landroid/database/SQLException; {:try_start_0 .. :try_end_0} :catch_1

    move-result v4

    int-to-long v1, v4

    :goto_0
    const-wide/16 v4, -0x1

    cmp-long v4, v1, v4

    if-nez v4, :cond_0

    sget-object v4, Ljava/lang/System;->out:Ljava/io/PrintStream;

    const-string v5, "update tbl_EpgTimer field sEventName ignored"

    invoke-virtual {v4, v5}, Ljava/io/PrintStream;->println(Ljava/lang/String;)V

    :cond_0
    :try_start_1
    invoke-static {}, Lcom/mstar/android/tvapi/common/TvManager;->getInstance()Lcom/mstar/android/tvapi/common/TvManager;

    move-result-object v4

    if-eqz v4, :cond_1

    invoke-static {}, Lcom/mstar/android/tvapi/common/TvManager;->getInstance()Lcom/mstar/android/tvapi/common/TvManager;

    move-result-object v4

    invoke-virtual {v4}, Lcom/mstar/android/tvapi/common/TvManager;->getDatabaseManager()Lcom/mstar/android/tvapi/common/DatabaseManager;

    move-result-object v4

    const/4 v5, 0x7

    invoke-virtual {v4, v5}, Lcom/mstar/android/tvapi/common/DatabaseManager;->setDatabaseDirtyByApplication(S)V
    :try_end_1
    .catch Lcom/mstar/android/tvapi/common/exception/TvCommonException; {:try_start_1 .. :try_end_1} :catch_0

    :cond_1
    :goto_1
    return-void

    :catch_0
    move-exception v0

    invoke-virtual {v0}, Lcom/mstar/android/tvapi/common/exception/TvCommonException;->printStackTrace()V

    goto :goto_1

    :catch_1
    move-exception v4

    goto :goto_0
.end method

.method public updateFactoryColorTempData(Lcom/android/server/tv/DataBaseDesk$T_MS_COLOR_TEMP_DATA;I)V
    .locals 7
    .param p1    # Lcom/android/server/tv/DataBaseDesk$T_MS_COLOR_TEMP_DATA;
    .param p2    # I

    const-wide/16 v0, -0x1

    new-instance v2, Landroid/content/ContentValues;

    invoke-direct {v2}, Landroid/content/ContentValues;-><init>()V

    const-string v3, "u8RedGain"

    iget-short v4, p1, Lcom/android/server/tv/DataBaseDesk$T_MS_COLOR_TEMP_DATA;->redgain:S

    invoke-static {v4}, Ljava/lang/Short;->valueOf(S)Ljava/lang/Short;

    move-result-object v4

    invoke-virtual {v2, v3, v4}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Short;)V

    const-string v3, "u8GreenGain"

    iget-short v4, p1, Lcom/android/server/tv/DataBaseDesk$T_MS_COLOR_TEMP_DATA;->greengain:S

    invoke-static {v4}, Ljava/lang/Short;->valueOf(S)Ljava/lang/Short;

    move-result-object v4

    invoke-virtual {v2, v3, v4}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Short;)V

    const-string v3, "u8BlueGain"

    iget-short v4, p1, Lcom/android/server/tv/DataBaseDesk$T_MS_COLOR_TEMP_DATA;->bluegain:S

    invoke-static {v4}, Ljava/lang/Short;->valueOf(S)Ljava/lang/Short;

    move-result-object v4

    invoke-virtual {v2, v3, v4}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Short;)V

    const-string v3, "u8RedOffset"

    iget-short v4, p1, Lcom/android/server/tv/DataBaseDesk$T_MS_COLOR_TEMP_DATA;->redoffset:S

    invoke-static {v4}, Ljava/lang/Short;->valueOf(S)Ljava/lang/Short;

    move-result-object v4

    invoke-virtual {v2, v3, v4}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Short;)V

    const-string v3, "u8GreenOffset"

    iget-short v4, p1, Lcom/android/server/tv/DataBaseDesk$T_MS_COLOR_TEMP_DATA;->greenoffset:S

    invoke-static {v4}, Ljava/lang/Short;->valueOf(S)Ljava/lang/Short;

    move-result-object v4

    invoke-virtual {v2, v3, v4}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Short;)V

    const-string v3, "u8BlueOffset"

    iget-short v4, p1, Lcom/android/server/tv/DataBaseDesk$T_MS_COLOR_TEMP_DATA;->blueoffset:S

    invoke-static {v4}, Ljava/lang/Short;->valueOf(S)Ljava/lang/Short;

    move-result-object v4

    invoke-virtual {v2, v3, v4}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Short;)V

    :try_start_0
    invoke-virtual {p0}, Lcom/android/server/tv/DataBaseDeskImpl;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v3

    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    const-string v5, "content://mstar.tv.factory/factorycolortemp/colortemperatureid/"

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4, p2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-static {v4}, Landroid/net/Uri;->parse(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v4

    const/4 v5, 0x0

    const/4 v6, 0x0

    invoke-virtual {v3, v4, v2, v5, v6}, Landroid/content/ContentResolver;->update(Landroid/net/Uri;Landroid/content/ContentValues;Ljava/lang/String;[Ljava/lang/String;)I
    :try_end_0
    .catch Landroid/database/SQLException; {:try_start_0 .. :try_end_0} :catch_0

    move-result v3

    int-to-long v0, v3

    :goto_0
    const-wide/16 v3, -0x1

    cmp-long v3, v0, v3

    if-nez v3, :cond_0

    sget-object v3, Ljava/lang/System;->out:Ljava/io/PrintStream;

    const-string v4, "update tbl_FactoryColorTemp ignored"

    invoke-virtual {v3, v4}, Ljava/io/PrintStream;->println(Ljava/lang/String;)V

    :cond_0
    return-void

    :catch_0
    move-exception v3

    goto :goto_0
.end method

.method public updateFactoryColorTempExData(Lcom/android/server/tv/DataBaseDesk$T_MS_COLOR_TEMPEX_DATA;II)V
    .locals 8
    .param p1    # Lcom/android/server/tv/DataBaseDesk$T_MS_COLOR_TEMPEX_DATA;
    .param p2    # I
    .param p3    # I

    const-wide/16 v1, -0x1

    new-instance v3, Landroid/content/ContentValues;

    invoke-direct {v3}, Landroid/content/ContentValues;-><init>()V

    const-string v4, "u16RedGain"

    iget v5, p1, Lcom/android/server/tv/DataBaseDesk$T_MS_COLOR_TEMPEX_DATA;->redgain:I

    invoke-static {v5}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v5

    invoke-virtual {v3, v4, v5}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Integer;)V

    const-string v4, "u16GreenGain"

    iget v5, p1, Lcom/android/server/tv/DataBaseDesk$T_MS_COLOR_TEMPEX_DATA;->greengain:I

    invoke-static {v5}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v5

    invoke-virtual {v3, v4, v5}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Integer;)V

    const-string v4, "u16BlueGain"

    iget v5, p1, Lcom/android/server/tv/DataBaseDesk$T_MS_COLOR_TEMPEX_DATA;->bluegain:I

    invoke-static {v5}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v5

    invoke-virtual {v3, v4, v5}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Integer;)V

    const-string v4, "u16RedOffset"

    iget v5, p1, Lcom/android/server/tv/DataBaseDesk$T_MS_COLOR_TEMPEX_DATA;->redoffset:I

    invoke-static {v5}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v5

    invoke-virtual {v3, v4, v5}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Integer;)V

    const-string v4, "u16GreenOffset"

    iget v5, p1, Lcom/android/server/tv/DataBaseDesk$T_MS_COLOR_TEMPEX_DATA;->greenoffset:I

    invoke-static {v5}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v5

    invoke-virtual {v3, v4, v5}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Integer;)V

    const-string v4, "u16BlueOffset"

    iget v5, p1, Lcom/android/server/tv/DataBaseDesk$T_MS_COLOR_TEMPEX_DATA;->blueoffset:I

    invoke-static {v5}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v5

    invoke-virtual {v3, v4, v5}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Integer;)V

    :try_start_0
    invoke-virtual {p0}, Lcom/android/server/tv/DataBaseDeskImpl;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v4

    new-instance v5, Ljava/lang/StringBuilder;

    invoke-direct {v5}, Ljava/lang/StringBuilder;-><init>()V

    const-string v6, "content://mstar.tv.factory/factorycolortempex/inputsourceid/"

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5, p2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v5

    const-string v6, "/colortemperatureid/"

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5, p3}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    invoke-static {v5}, Landroid/net/Uri;->parse(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v5

    const/4 v6, 0x0

    const/4 v7, 0x0

    invoke-virtual {v4, v5, v3, v6, v7}, Landroid/content/ContentResolver;->update(Landroid/net/Uri;Landroid/content/ContentValues;Ljava/lang/String;[Ljava/lang/String;)I
    :try_end_0
    .catch Landroid/database/SQLException; {:try_start_0 .. :try_end_0} :catch_1

    move-result v4

    int-to-long v1, v4

    :goto_0
    const-wide/16 v4, -0x1

    cmp-long v4, v1, v4

    if-nez v4, :cond_0

    sget-object v4, Ljava/lang/System;->out:Ljava/io/PrintStream;

    const-string v5, "update tbl_FactoryColorTempEx ignored"

    invoke-virtual {v4, v5}, Ljava/io/PrintStream;->println(Ljava/lang/String;)V

    :cond_0
    :try_start_1
    invoke-static {}, Lcom/mstar/android/tvapi/common/TvManager;->getInstance()Lcom/mstar/android/tvapi/common/TvManager;

    move-result-object v4

    if-eqz v4, :cond_1

    invoke-static {}, Lcom/mstar/android/tvapi/common/TvManager;->getInstance()Lcom/mstar/android/tvapi/common/TvManager;

    move-result-object v4

    invoke-virtual {v4}, Lcom/mstar/android/tvapi/common/TvManager;->getDatabaseManager()Lcom/mstar/android/tvapi/common/DatabaseManager;

    move-result-object v4

    const/16 v5, 0x26

    invoke-virtual {v4, v5}, Lcom/mstar/android/tvapi/common/DatabaseManager;->setDatabaseDirtyByApplication(S)V
    :try_end_1
    .catch Lcom/mstar/android/tvapi/common/exception/TvCommonException; {:try_start_1 .. :try_end_1} :catch_0

    :cond_1
    :goto_1
    return-void

    :catch_0
    move-exception v0

    invoke-virtual {v0}, Lcom/mstar/android/tvapi/common/exception/TvCommonException;->printStackTrace()V

    goto :goto_1

    :catch_1
    move-exception v4

    goto :goto_0
.end method

.method public updateFactoryExtern(Lcom/android/server/tv/DataBaseDesk$MS_FACTORY_EXTERN_SETTING;)V
    .locals 8
    .param p1    # Lcom/android/server/tv/DataBaseDesk$MS_FACTORY_EXTERN_SETTING;

    const-wide/16 v1, -0x1

    new-instance v3, Landroid/content/ContentValues;

    invoke-direct {v3}, Landroid/content/ContentValues;-><init>()V

    const-string v4, "SoftWareVersion"

    sget-object v5, Lcom/android/server/tv/DataBaseDesk$MS_FACTORY_EXTERN_SETTING;->softVersion:Ljava/lang/String;

    invoke-virtual {v3, v4, v5}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    const-string v4, "BoardType"

    sget-object v5, Lcom/android/server/tv/DataBaseDesk$MS_FACTORY_EXTERN_SETTING;->boardType:Ljava/lang/String;

    invoke-virtual {v3, v4, v5}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    const-string v4, "PanelType"

    sget-object v5, Lcom/android/server/tv/DataBaseDesk$MS_FACTORY_EXTERN_SETTING;->panelType:Ljava/lang/String;

    invoke-virtual {v3, v4, v5}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    const-string v4, "CompileTime"

    sget-object v5, Lcom/android/server/tv/DataBaseDesk$MS_FACTORY_EXTERN_SETTING;->dayAndTime:Ljava/lang/String;

    invoke-virtual {v3, v4, v5}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    const-string v4, "TestPatternMode"

    iget v5, p1, Lcom/android/server/tv/DataBaseDesk$MS_FACTORY_EXTERN_SETTING;->testPatternMode:I

    invoke-static {v5}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v5

    invoke-virtual {v3, v4, v5}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Integer;)V

    const-string v4, "stPowerMode"

    iget v5, p1, Lcom/android/server/tv/DataBaseDesk$MS_FACTORY_EXTERN_SETTING;->stPowerMode:I

    invoke-static {v5}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v5

    invoke-virtual {v3, v4, v5}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Integer;)V

    const-string v5, "DtvAvAbnormalDelay"

    iget-boolean v4, p1, Lcom/android/server/tv/DataBaseDesk$MS_FACTORY_EXTERN_SETTING;->dtvAvAbnormalDelay:Z

    if-eqz v4, :cond_2

    const/4 v4, 0x1

    :goto_0
    invoke-static {v4}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v4

    invoke-virtual {v3, v5, v4}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Integer;)V

    const-string v4, "FactoryPreSetFeature"

    iget v5, p1, Lcom/android/server/tv/DataBaseDesk$MS_FACTORY_EXTERN_SETTING;->factoryPreset:I

    invoke-static {v5}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v5

    invoke-virtual {v3, v4, v5}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Integer;)V

    const-string v4, "PanelSwing"

    iget-short v5, p1, Lcom/android/server/tv/DataBaseDesk$MS_FACTORY_EXTERN_SETTING;->panelSwingVal:S

    invoke-static {v5}, Ljava/lang/Short;->valueOf(S)Ljava/lang/Short;

    move-result-object v5

    invoke-virtual {v3, v4, v5}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Short;)V

    const-string v4, "AudioPrescale"

    iget-short v5, p1, Lcom/android/server/tv/DataBaseDesk$MS_FACTORY_EXTERN_SETTING;->audioPreScale:S

    invoke-static {v5}, Ljava/lang/Short;->valueOf(S)Ljava/lang/Short;

    move-result-object v5

    invoke-virtual {v3, v4, v5}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Short;)V

    const-string v4, "vdDspVersion"

    iget-short v5, p1, Lcom/android/server/tv/DataBaseDesk$MS_FACTORY_EXTERN_SETTING;->vdDspVersion:S

    invoke-static {v5}, Ljava/lang/Short;->valueOf(S)Ljava/lang/Short;

    move-result-object v5

    invoke-virtual {v3, v4, v5}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Short;)V

    const-string v4, "eHidevMode"

    iget v5, p1, Lcom/android/server/tv/DataBaseDesk$MS_FACTORY_EXTERN_SETTING;->eHidevMode:I

    invoke-static {v5}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v5

    invoke-virtual {v3, v4, v5}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Integer;)V

    const-string v4, "audioNrThr"

    iget-short v5, p1, Lcom/android/server/tv/DataBaseDesk$MS_FACTORY_EXTERN_SETTING;->audioNrThr:S

    invoke-static {v5}, Ljava/lang/Short;->valueOf(S)Ljava/lang/Short;

    move-result-object v5

    invoke-virtual {v3, v4, v5}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Short;)V

    const-string v4, "audioSifThreshold"

    iget-short v5, p1, Lcom/android/server/tv/DataBaseDesk$MS_FACTORY_EXTERN_SETTING;->audioSifThreshold:S

    invoke-static {v5}, Ljava/lang/Short;->valueOf(S)Ljava/lang/Short;

    move-result-object v5

    invoke-virtual {v3, v4, v5}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Short;)V

    const-string v4, "audioDspVersion"

    iget-short v5, p1, Lcom/android/server/tv/DataBaseDesk$MS_FACTORY_EXTERN_SETTING;->audioDspVersion:S

    invoke-static {v5}, Ljava/lang/Short;->valueOf(S)Ljava/lang/Short;

    move-result-object v5

    invoke-virtual {v3, v4, v5}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Short;)V

    :try_start_0
    invoke-virtual {p0}, Lcom/android/server/tv/DataBaseDeskImpl;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v4

    const-string v5, "content://mstar.tv.factory/factoryextern"

    invoke-static {v5}, Landroid/net/Uri;->parse(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v5

    const/4 v6, 0x0

    const/4 v7, 0x0

    invoke-virtual {v4, v5, v3, v6, v7}, Landroid/content/ContentResolver;->update(Landroid/net/Uri;Landroid/content/ContentValues;Ljava/lang/String;[Ljava/lang/String;)I
    :try_end_0
    .catch Landroid/database/SQLException; {:try_start_0 .. :try_end_0} :catch_1

    move-result v4

    int-to-long v1, v4

    :goto_1
    const-wide/16 v4, -0x1

    cmp-long v4, v1, v4

    if-nez v4, :cond_0

    sget-object v4, Ljava/lang/System;->out:Ljava/io/PrintStream;

    const-string v5, "update tbl_FactoryExtern ignored"

    invoke-virtual {v4, v5}, Ljava/io/PrintStream;->println(Ljava/lang/String;)V

    :cond_0
    :try_start_1
    invoke-static {}, Lcom/mstar/android/tvapi/common/TvManager;->getInstance()Lcom/mstar/android/tvapi/common/TvManager;

    move-result-object v4

    if-eqz v4, :cond_1

    invoke-static {}, Lcom/mstar/android/tvapi/common/TvManager;->getInstance()Lcom/mstar/android/tvapi/common/TvManager;

    move-result-object v4

    invoke-virtual {v4}, Lcom/mstar/android/tvapi/common/TvManager;->getDatabaseManager()Lcom/mstar/android/tvapi/common/DatabaseManager;

    move-result-object v4

    const/16 v5, 0x27

    invoke-virtual {v4, v5}, Lcom/mstar/android/tvapi/common/DatabaseManager;->setDatabaseDirtyByApplication(S)V
    :try_end_1
    .catch Lcom/mstar/android/tvapi/common/exception/TvCommonException; {:try_start_1 .. :try_end_1} :catch_0

    :cond_1
    :goto_2
    return-void

    :cond_2
    const/4 v4, 0x0

    goto/16 :goto_0

    :catch_0
    move-exception v0

    invoke-virtual {v0}, Lcom/mstar/android/tvapi/common/exception/TvCommonException;->printStackTrace()V

    goto :goto_2

    :catch_1
    move-exception v4

    goto :goto_1
.end method

.method public updateFilmMode(II)V
    .locals 5
    .param p1    # I
    .param p2    # I

    new-instance v0, Landroid/content/ContentValues;

    invoke-direct {v0}, Landroid/content/ContentValues;-><init>()V

    const-string v1, "eFilm"

    invoke-static {p1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Integer;)V

    :try_start_0
    invoke-virtual {p0}, Lcom/android/server/tv/DataBaseDeskImpl;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v1

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "content://mstar.tv.usersetting/videosetting/inputsrc/"

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, p2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v2}, Landroid/net/Uri;->parse(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v2

    const/4 v3, 0x0

    const/4 v4, 0x0

    invoke-virtual {v1, v2, v0, v3, v4}, Landroid/content/ContentResolver;->update(Landroid/net/Uri;Landroid/content/ContentValues;Ljava/lang/String;[Ljava/lang/String;)I
    :try_end_0
    .catch Landroid/database/SQLException; {:try_start_0 .. :try_end_0} :catch_0

    :goto_0
    return-void

    :catch_0
    move-exception v1

    goto :goto_0
.end method

.method public updateHDMIOverscanAdjust(II[[Lcom/android/server/tv/DataBaseDesk$ST_MAPI_VIDEO_WINDOW_INFO;)V
    .locals 8
    .param p1    # I
    .param p2    # I
    .param p3    # [[Lcom/android/server/tv/DataBaseDesk$ST_MAPI_VIDEO_WINDOW_INFO;

    const-wide/16 v1, -0x1

    new-instance v3, Landroid/content/ContentValues;

    invoke-direct {v3}, Landroid/content/ContentValues;-><init>()V

    const-string v4, "u16H_CapStart"

    aget-object v5, p3, p1

    aget-object v5, v5, p2

    iget v5, v5, Lcom/android/server/tv/DataBaseDesk$ST_MAPI_VIDEO_WINDOW_INFO;->u16H_CapStart:I

    invoke-static {v5}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v5

    invoke-virtual {v3, v4, v5}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Integer;)V

    const-string v4, "u16V_CapStart"

    aget-object v5, p3, p1

    aget-object v5, v5, p2

    iget v5, v5, Lcom/android/server/tv/DataBaseDesk$ST_MAPI_VIDEO_WINDOW_INFO;->u16V_CapStart:I

    invoke-static {v5}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v5

    invoke-virtual {v3, v4, v5}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Integer;)V

    const-string v4, "u8HCrop_Left"

    aget-object v5, p3, p1

    aget-object v5, v5, p2

    iget-short v5, v5, Lcom/android/server/tv/DataBaseDesk$ST_MAPI_VIDEO_WINDOW_INFO;->u8HCrop_Left:S

    invoke-static {v5}, Ljava/lang/Short;->valueOf(S)Ljava/lang/Short;

    move-result-object v5

    invoke-virtual {v3, v4, v5}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Short;)V

    const-string v4, "u8HCrop_Right"

    aget-object v5, p3, p1

    aget-object v5, v5, p2

    iget-short v5, v5, Lcom/android/server/tv/DataBaseDesk$ST_MAPI_VIDEO_WINDOW_INFO;->u8HCrop_Right:S

    invoke-static {v5}, Ljava/lang/Short;->valueOf(S)Ljava/lang/Short;

    move-result-object v5

    invoke-virtual {v3, v4, v5}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Short;)V

    const-string v4, "u8VCrop_Up"

    aget-object v5, p3, p1

    aget-object v5, v5, p2

    iget-short v5, v5, Lcom/android/server/tv/DataBaseDesk$ST_MAPI_VIDEO_WINDOW_INFO;->u8VCrop_Up:S

    invoke-static {v5}, Ljava/lang/Short;->valueOf(S)Ljava/lang/Short;

    move-result-object v5

    invoke-virtual {v3, v4, v5}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Short;)V

    const-string v4, "u8VCrop_Down"

    aget-object v5, p3, p1

    aget-object v5, v5, p2

    iget-short v5, v5, Lcom/android/server/tv/DataBaseDesk$ST_MAPI_VIDEO_WINDOW_INFO;->u8VCrop_Down:S

    invoke-static {v5}, Ljava/lang/Short;->valueOf(S)Ljava/lang/Short;

    move-result-object v5

    invoke-virtual {v3, v4, v5}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Short;)V

    :try_start_0
    invoke-virtual {p0}, Lcom/android/server/tv/DataBaseDeskImpl;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v4

    new-instance v5, Ljava/lang/StringBuilder;

    invoke-direct {v5}, Ljava/lang/StringBuilder;-><init>()V

    const-string v6, "content://mstar.tv.factory/hdmioverscansetting/resolutiontypenum/"

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5, p1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v5

    const-string v6, "/_id/"

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5, p2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    invoke-static {v5}, Landroid/net/Uri;->parse(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v5

    const/4 v6, 0x0

    const/4 v7, 0x0

    invoke-virtual {v4, v5, v3, v6, v7}, Landroid/content/ContentResolver;->update(Landroid/net/Uri;Landroid/content/ContentValues;Ljava/lang/String;[Ljava/lang/String;)I
    :try_end_0
    .catch Landroid/database/SQLException; {:try_start_0 .. :try_end_0} :catch_1

    move-result v4

    int-to-long v1, v4

    :goto_0
    const-wide/16 v4, -0x1

    cmp-long v4, v1, v4

    if-nez v4, :cond_0

    sget-object v4, Ljava/lang/System;->out:Ljava/io/PrintStream;

    const-string v5, "update tbl_HDMIOverscanSetting ignored"

    invoke-virtual {v4, v5}, Ljava/io/PrintStream;->println(Ljava/lang/String;)V

    :cond_0
    :try_start_1
    invoke-static {}, Lcom/mstar/android/tvapi/common/TvManager;->getInstance()Lcom/mstar/android/tvapi/common/TvManager;

    move-result-object v4

    if-eqz v4, :cond_1

    invoke-static {}, Lcom/mstar/android/tvapi/common/TvManager;->getInstance()Lcom/mstar/android/tvapi/common/TvManager;

    move-result-object v4

    invoke-virtual {v4}, Lcom/mstar/android/tvapi/common/TvManager;->getDatabaseManager()Lcom/mstar/android/tvapi/common/DatabaseManager;

    move-result-object v4

    const/16 v5, 0x2e

    invoke-virtual {v4, v5}, Lcom/mstar/android/tvapi/common/DatabaseManager;->setDatabaseDirtyByApplication(S)V
    :try_end_1
    .catch Lcom/mstar/android/tvapi/common/exception/TvCommonException; {:try_start_1 .. :try_end_1} :catch_0

    :cond_1
    :goto_1
    return-void

    :catch_0
    move-exception v0

    invoke-virtual {v0}, Lcom/mstar/android/tvapi/common/exception/TvCommonException;->printStackTrace()V

    goto :goto_1

    :catch_1
    move-exception v4

    goto :goto_0
.end method

.method public updateNR(IILcom/mstar/android/tvapi/common/vo/NoiseReduction$EnumNoiseReduction;)V
    .locals 6
    .param p1    # I
    .param p2    # I
    .param p3    # Lcom/mstar/android/tvapi/common/vo/NoiseReduction$EnumNoiseReduction;

    new-instance v1, Landroid/content/ContentValues;

    invoke-direct {v1}, Landroid/content/ContentValues;-><init>()V

    const-string v2, "eNR"

    invoke-virtual {p3}, Lcom/mstar/android/tvapi/common/vo/NoiseReduction$EnumNoiseReduction;->getValue()I

    move-result v3

    invoke-static {v3}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v3

    invoke-virtual {v1, v2, v3}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Integer;)V

    :try_start_0
    invoke-virtual {p0}, Lcom/android/server/tv/DataBaseDeskImpl;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v2

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string v4, "content://mstar.tv.usersetting/nrmode/nrmode/"

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3, p2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v3

    const-string v4, "/inputsrc/"

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3, p1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v3}, Landroid/net/Uri;->parse(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v3

    const/4 v4, 0x0

    const/4 v5, 0x0

    invoke-virtual {v2, v3, v1, v4, v5}, Landroid/content/ContentResolver;->update(Landroid/net/Uri;Landroid/content/ContentValues;Ljava/lang/String;[Ljava/lang/String;)I
    :try_end_0
    .catch Landroid/database/SQLException; {:try_start_0 .. :try_end_0} :catch_0

    :goto_0
    return-void

    :catch_0
    move-exception v0

    const-string v2, "MiniDb"

    const-string v3, "updateNR failed"

    invoke-static {v2, v3}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_0
.end method

.method public updateNonLinearAdjust(Lcom/android/server/tv/DataBaseDesk$MS_NLA_POINT;I)V
    .locals 8
    .param p1    # Lcom/android/server/tv/DataBaseDesk$MS_NLA_POINT;
    .param p2    # I

    const-wide/16 v1, -0x1

    new-instance v3, Landroid/content/ContentValues;

    invoke-direct {v3}, Landroid/content/ContentValues;-><init>()V

    const-string v4, "u8OSD_V0"

    iget-short v5, p1, Lcom/android/server/tv/DataBaseDesk$MS_NLA_POINT;->u8OSD_V0:S

    invoke-static {v5}, Ljava/lang/Short;->valueOf(S)Ljava/lang/Short;

    move-result-object v5

    invoke-virtual {v3, v4, v5}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Short;)V

    const-string v4, "u8OSD_V25"

    iget-short v5, p1, Lcom/android/server/tv/DataBaseDesk$MS_NLA_POINT;->u8OSD_V25:S

    invoke-static {v5}, Ljava/lang/Short;->valueOf(S)Ljava/lang/Short;

    move-result-object v5

    invoke-virtual {v3, v4, v5}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Short;)V

    const-string v4, "u8OSD_V50"

    iget-short v5, p1, Lcom/android/server/tv/DataBaseDesk$MS_NLA_POINT;->u8OSD_V50:S

    invoke-static {v5}, Ljava/lang/Short;->valueOf(S)Ljava/lang/Short;

    move-result-object v5

    invoke-virtual {v3, v4, v5}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Short;)V

    const-string v4, "u8OSD_V75"

    iget-short v5, p1, Lcom/android/server/tv/DataBaseDesk$MS_NLA_POINT;->u8OSD_V75:S

    invoke-static {v5}, Ljava/lang/Short;->valueOf(S)Ljava/lang/Short;

    move-result-object v5

    invoke-virtual {v3, v4, v5}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Short;)V

    const-string v4, "u8OSD_V100"

    iget-short v5, p1, Lcom/android/server/tv/DataBaseDesk$MS_NLA_POINT;->u8OSD_V100:S

    invoke-static {v5}, Ljava/lang/Short;->valueOf(S)Ljava/lang/Short;

    move-result-object v5

    invoke-virtual {v3, v4, v5}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Short;)V

    :try_start_0
    invoke-virtual {p0}, Lcom/android/server/tv/DataBaseDeskImpl;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v4

    new-instance v5, Ljava/lang/StringBuilder;

    invoke-direct {v5}, Ljava/lang/StringBuilder;-><init>()V

    const-string v6, "content://mstar.tv.factory/nonlinearadjust/inputsrctype/"

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {p0}, Lcom/android/server/tv/DataBaseDeskImpl;->queryCurInputSrc()I

    move-result v6

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v5

    const-string v6, "/curvetypeindex/"

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5, p2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    invoke-static {v5}, Landroid/net/Uri;->parse(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v5

    const/4 v6, 0x0

    const/4 v7, 0x0

    invoke-virtual {v4, v5, v3, v6, v7}, Landroid/content/ContentResolver;->update(Landroid/net/Uri;Landroid/content/ContentValues;Ljava/lang/String;[Ljava/lang/String;)I
    :try_end_0
    .catch Landroid/database/SQLException; {:try_start_0 .. :try_end_0} :catch_1

    move-result v4

    int-to-long v1, v4

    :goto_0
    const-wide/16 v4, -0x1

    cmp-long v4, v1, v4

    if-nez v4, :cond_0

    sget-object v4, Ljava/lang/System;->out:Ljava/io/PrintStream;

    const-string v5, "update tbl_NonLinearAdjust ignored"

    invoke-virtual {v4, v5}, Ljava/io/PrintStream;->println(Ljava/lang/String;)V

    :cond_0
    :try_start_1
    invoke-static {}, Lcom/mstar/android/tvapi/common/TvManager;->getInstance()Lcom/mstar/android/tvapi/common/TvManager;

    move-result-object v4

    if-eqz v4, :cond_1

    invoke-static {}, Lcom/mstar/android/tvapi/common/TvManager;->getInstance()Lcom/mstar/android/tvapi/common/TvManager;

    move-result-object v4

    invoke-virtual {v4}, Lcom/mstar/android/tvapi/common/TvManager;->getDatabaseManager()Lcom/mstar/android/tvapi/common/DatabaseManager;

    move-result-object v4

    const/16 v5, 0x2a

    invoke-virtual {v4, v5}, Lcom/mstar/android/tvapi/common/DatabaseManager;->setDatabaseDirtyByApplication(S)V
    :try_end_1
    .catch Lcom/mstar/android/tvapi/common/exception/TvCommonException; {:try_start_1 .. :try_end_1} :catch_0

    :cond_1
    :goto_1
    return-void

    :catch_0
    move-exception v0

    invoke-virtual {v0}, Lcom/mstar/android/tvapi/common/exception/TvCommonException;->printStackTrace()V

    goto :goto_1

    :catch_1
    move-exception v4

    goto :goto_0
.end method

.method public updateNonStandardAdjust(Lcom/android/server/tv/DataBaseDesk$MS_Factory_NS_VD_SET;)V
    .locals 8
    .param p1    # Lcom/android/server/tv/DataBaseDesk$MS_Factory_NS_VD_SET;

    const-wide/16 v1, -0x1

    new-instance v3, Landroid/content/ContentValues;

    invoke-direct {v3}, Landroid/content/ContentValues;-><init>()V

    const-string v4, "u8AFEC_D4"

    iget-short v5, p1, Lcom/android/server/tv/DataBaseDesk$MS_Factory_NS_VD_SET;->u8AFEC_D4:S

    invoke-static {v5}, Ljava/lang/Short;->valueOf(S)Ljava/lang/Short;

    move-result-object v5

    invoke-virtual {v3, v4, v5}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Short;)V

    const-string v4, "u8AFEC_D5_Bit2"

    iget-short v5, p1, Lcom/android/server/tv/DataBaseDesk$MS_Factory_NS_VD_SET;->u8AFEC_D5_Bit2:S

    invoke-static {v5}, Ljava/lang/Short;->valueOf(S)Ljava/lang/Short;

    move-result-object v5

    invoke-virtual {v3, v4, v5}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Short;)V

    const-string v4, "u8AFEC_D8_Bit3210"

    iget-short v5, p1, Lcom/android/server/tv/DataBaseDesk$MS_Factory_NS_VD_SET;->u8AFEC_D8_Bit3210:S

    invoke-static {v5}, Ljava/lang/Short;->valueOf(S)Ljava/lang/Short;

    move-result-object v5

    invoke-virtual {v3, v4, v5}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Short;)V

    const-string v4, "u8AFEC_D9_Bit0"

    iget-short v5, p1, Lcom/android/server/tv/DataBaseDesk$MS_Factory_NS_VD_SET;->u8AFEC_D9_Bit0:S

    invoke-static {v5}, Ljava/lang/Short;->valueOf(S)Ljava/lang/Short;

    move-result-object v5

    invoke-virtual {v3, v4, v5}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Short;)V

    const-string v4, "u8AFEC_D7_LOW_BOUND"

    iget-short v5, p1, Lcom/android/server/tv/DataBaseDesk$MS_Factory_NS_VD_SET;->u8AFEC_D7_LOW_BOUND:S

    invoke-static {v5}, Ljava/lang/Short;->valueOf(S)Ljava/lang/Short;

    move-result-object v5

    invoke-virtual {v3, v4, v5}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Short;)V

    const-string v4, "u8AFEC_D7_HIGH_BOUND"

    iget-short v5, p1, Lcom/android/server/tv/DataBaseDesk$MS_Factory_NS_VD_SET;->u8AFEC_D7_HIGH_BOUND:S

    invoke-static {v5}, Ljava/lang/Short;->valueOf(S)Ljava/lang/Short;

    move-result-object v5

    invoke-virtual {v3, v4, v5}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Short;)V

    const-string v4, "u8AFEC_A0"

    iget-short v5, p1, Lcom/android/server/tv/DataBaseDesk$MS_Factory_NS_VD_SET;->u8AFEC_A0:S

    invoke-static {v5}, Ljava/lang/Short;->valueOf(S)Ljava/lang/Short;

    move-result-object v5

    invoke-virtual {v3, v4, v5}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Short;)V

    const-string v4, "u8AFEC_A1"

    iget-short v5, p1, Lcom/android/server/tv/DataBaseDesk$MS_Factory_NS_VD_SET;->u8AFEC_A1:S

    invoke-static {v5}, Ljava/lang/Short;->valueOf(S)Ljava/lang/Short;

    move-result-object v5

    invoke-virtual {v3, v4, v5}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Short;)V

    const-string v4, "u8AFEC_66_Bit76"

    iget-short v5, p1, Lcom/android/server/tv/DataBaseDesk$MS_Factory_NS_VD_SET;->u8AFEC_66_Bit76:S

    invoke-static {v5}, Ljava/lang/Short;->valueOf(S)Ljava/lang/Short;

    move-result-object v5

    invoke-virtual {v3, v4, v5}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Short;)V

    const-string v4, "u8AFEC_6E_Bit7654"

    iget-short v5, p1, Lcom/android/server/tv/DataBaseDesk$MS_Factory_NS_VD_SET;->u8AFEC_6E_Bit7654:S

    invoke-static {v5}, Ljava/lang/Short;->valueOf(S)Ljava/lang/Short;

    move-result-object v5

    invoke-virtual {v3, v4, v5}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Short;)V

    const-string v4, "u8AFEC_6E_Bit3210"

    iget-short v5, p1, Lcom/android/server/tv/DataBaseDesk$MS_Factory_NS_VD_SET;->u8AFEC_6E_Bit3210:S

    invoke-static {v5}, Ljava/lang/Short;->valueOf(S)Ljava/lang/Short;

    move-result-object v5

    invoke-virtual {v3, v4, v5}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Short;)V

    const-string v4, "u8AFEC_43"

    iget-short v5, p1, Lcom/android/server/tv/DataBaseDesk$MS_Factory_NS_VD_SET;->u8AFEC_43:S

    invoke-static {v5}, Ljava/lang/Short;->valueOf(S)Ljava/lang/Short;

    move-result-object v5

    invoke-virtual {v3, v4, v5}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Short;)V

    const-string v4, "u8AFEC_44"

    iget-short v5, p1, Lcom/android/server/tv/DataBaseDesk$MS_Factory_NS_VD_SET;->u8AFEC_44:S

    invoke-static {v5}, Ljava/lang/Short;->valueOf(S)Ljava/lang/Short;

    move-result-object v5

    invoke-virtual {v3, v4, v5}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Short;)V

    const-string v4, "u8AFEC_CB"

    iget-short v5, p1, Lcom/android/server/tv/DataBaseDesk$MS_Factory_NS_VD_SET;->u8AFEC_CB:S

    invoke-static {v5}, Ljava/lang/Short;->valueOf(S)Ljava/lang/Short;

    move-result-object v5

    invoke-virtual {v3, v4, v5}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Short;)V

    :try_start_0
    invoke-virtual {p0}, Lcom/android/server/tv/DataBaseDeskImpl;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v4

    const-string v5, "content://mstar.tv.factory/nonstandardadjust"

    invoke-static {v5}, Landroid/net/Uri;->parse(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v5

    const/4 v6, 0x0

    const/4 v7, 0x0

    invoke-virtual {v4, v5, v3, v6, v7}, Landroid/content/ContentResolver;->update(Landroid/net/Uri;Landroid/content/ContentValues;Ljava/lang/String;[Ljava/lang/String;)I
    :try_end_0
    .catch Landroid/database/SQLException; {:try_start_0 .. :try_end_0} :catch_1

    move-result v4

    int-to-long v1, v4

    :goto_0
    const-wide/16 v4, -0x1

    cmp-long v4, v1, v4

    if-nez v4, :cond_0

    sget-object v4, Ljava/lang/System;->out:Ljava/io/PrintStream;

    const-string v5, "update tbl_NonStandardAdjust nonStandSet AFEC ignored"

    invoke-virtual {v4, v5}, Ljava/io/PrintStream;->println(Ljava/lang/String;)V

    :cond_0
    :try_start_1
    invoke-static {}, Lcom/mstar/android/tvapi/common/TvManager;->getInstance()Lcom/mstar/android/tvapi/common/TvManager;

    move-result-object v4

    if-eqz v4, :cond_1

    invoke-static {}, Lcom/mstar/android/tvapi/common/TvManager;->getInstance()Lcom/mstar/android/tvapi/common/TvManager;

    move-result-object v4

    invoke-virtual {v4}, Lcom/mstar/android/tvapi/common/TvManager;->getDatabaseManager()Lcom/mstar/android/tvapi/common/DatabaseManager;

    move-result-object v4

    const/16 v5, 0x28

    invoke-virtual {v4, v5}, Lcom/mstar/android/tvapi/common/DatabaseManager;->setDatabaseDirtyByApplication(S)V
    :try_end_1
    .catch Lcom/mstar/android/tvapi/common/exception/TvCommonException; {:try_start_1 .. :try_end_1} :catch_0

    :cond_1
    :goto_1
    return-void

    :catch_0
    move-exception v0

    invoke-virtual {v0}, Lcom/mstar/android/tvapi/common/exception/TvCommonException;->printStackTrace()V

    goto :goto_1

    :catch_1
    move-exception v4

    goto :goto_0
.end method

.method public updateNonStandardAdjust(Lcom/android/server/tv/DataBaseDesk$MS_Factory_NS_VIF_SET;)V
    .locals 8
    .param p1    # Lcom/android/server/tv/DataBaseDesk$MS_Factory_NS_VIF_SET;

    const/4 v5, 0x1

    const/4 v6, 0x0

    const-wide/16 v1, -0x1

    new-instance v3, Landroid/content/ContentValues;

    invoke-direct {v3}, Landroid/content/ContentValues;-><init>()V

    const-string v4, "VifTop"

    iget-short v7, p1, Lcom/android/server/tv/DataBaseDesk$MS_Factory_NS_VIF_SET;->VifTop:S

    invoke-static {v7}, Ljava/lang/Short;->valueOf(S)Ljava/lang/Short;

    move-result-object v7

    invoke-virtual {v3, v4, v7}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Short;)V

    const-string v4, "VifVgaMaximum"

    iget v7, p1, Lcom/android/server/tv/DataBaseDesk$MS_Factory_NS_VIF_SET;->VifVgaMaximum:I

    invoke-static {v7}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v7

    invoke-virtual {v3, v4, v7}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Integer;)V

    const-string v4, "VifCrKp"

    iget-short v7, p1, Lcom/android/server/tv/DataBaseDesk$MS_Factory_NS_VIF_SET;->VifCrKp:S

    invoke-static {v7}, Ljava/lang/Short;->valueOf(S)Ljava/lang/Short;

    move-result-object v7

    invoke-virtual {v3, v4, v7}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Short;)V

    const-string v4, "VifCrKi"

    iget-short v7, p1, Lcom/android/server/tv/DataBaseDesk$MS_Factory_NS_VIF_SET;->VifCrKi:S

    invoke-static {v7}, Ljava/lang/Short;->valueOf(S)Ljava/lang/Short;

    move-result-object v7

    invoke-virtual {v3, v4, v7}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Short;)V

    const-string v4, "VifCrKp1"

    iget-short v7, p1, Lcom/android/server/tv/DataBaseDesk$MS_Factory_NS_VIF_SET;->VifCrKp1:S

    invoke-static {v7}, Ljava/lang/Short;->valueOf(S)Ljava/lang/Short;

    move-result-object v7

    invoke-virtual {v3, v4, v7}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Short;)V

    const-string v4, "VifCrKi1"

    iget-short v7, p1, Lcom/android/server/tv/DataBaseDesk$MS_Factory_NS_VIF_SET;->VifCrKi1:S

    invoke-static {v7}, Ljava/lang/Short;->valueOf(S)Ljava/lang/Short;

    move-result-object v7

    invoke-virtual {v3, v4, v7}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Short;)V

    const-string v4, "VifCrKp2"

    iget-short v7, p1, Lcom/android/server/tv/DataBaseDesk$MS_Factory_NS_VIF_SET;->VifCrKp2:S

    invoke-static {v7}, Ljava/lang/Short;->valueOf(S)Ljava/lang/Short;

    move-result-object v7

    invoke-virtual {v3, v4, v7}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Short;)V

    const-string v4, "VifCrKi2"

    iget-short v7, p1, Lcom/android/server/tv/DataBaseDesk$MS_Factory_NS_VIF_SET;->VifCrKi2:S

    invoke-static {v7}, Ljava/lang/Short;->valueOf(S)Ljava/lang/Short;

    move-result-object v7

    invoke-virtual {v3, v4, v7}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Short;)V

    const-string v7, "VifAsiaSignalOption"

    iget-boolean v4, p1, Lcom/android/server/tv/DataBaseDesk$MS_Factory_NS_VIF_SET;->VifAsiaSignalOption:Z

    if-eqz v4, :cond_2

    move v4, v5

    :goto_0
    invoke-static {v4}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v4

    invoke-virtual {v3, v7, v4}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Integer;)V

    const-string v7, "VifCrKpKiAdjust"

    iget-boolean v4, p1, Lcom/android/server/tv/DataBaseDesk$MS_Factory_NS_VIF_SET;->VifCrKpKiAdjust:Z

    if-eqz v4, :cond_3

    move v4, v5

    :goto_1
    invoke-static {v4}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v4

    invoke-virtual {v3, v7, v4}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Integer;)V

    const-string v4, "VifOverModulation"

    iget-boolean v7, p1, Lcom/android/server/tv/DataBaseDesk$MS_Factory_NS_VIF_SET;->VifOverModulation:Z

    if-eqz v7, :cond_4

    :goto_2
    invoke-static {v5}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v5

    invoke-virtual {v3, v4, v5}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Integer;)V

    const-string v4, "VifClampgainGainOvNegative"

    iget v5, p1, Lcom/android/server/tv/DataBaseDesk$MS_Factory_NS_VIF_SET;->VifClampgainGainOvNegative:I

    invoke-static {v5}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v5

    invoke-virtual {v3, v4, v5}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Integer;)V

    const-string v4, "ChinaDescramblerBox"

    iget-short v5, p1, Lcom/android/server/tv/DataBaseDesk$MS_Factory_NS_VIF_SET;->ChinaDescramblerBox:S

    invoke-static {v5}, Ljava/lang/Short;->valueOf(S)Ljava/lang/Short;

    move-result-object v5

    invoke-virtual {v3, v4, v5}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Short;)V

    const-string v4, "VifDelayReduce"

    iget-short v5, p1, Lcom/android/server/tv/DataBaseDesk$MS_Factory_NS_VIF_SET;->VifDelayReduce:S

    invoke-static {v5}, Ljava/lang/Short;->valueOf(S)Ljava/lang/Short;

    move-result-object v5

    invoke-virtual {v3, v4, v5}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Short;)V

    const-string v4, "VifCrThr"

    iget v5, p1, Lcom/android/server/tv/DataBaseDesk$MS_Factory_NS_VIF_SET;->VifCrThr:I

    invoke-static {v5}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v5

    invoke-virtual {v3, v4, v5}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Integer;)V

    const-string v4, "VifVersion"

    iget-short v5, p1, Lcom/android/server/tv/DataBaseDesk$MS_Factory_NS_VIF_SET;->VifVersion:S

    invoke-static {v5}, Ljava/lang/Short;->valueOf(S)Ljava/lang/Short;

    move-result-object v5

    invoke-virtual {v3, v4, v5}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Short;)V

    const-string v4, "VifACIAGCREF"

    iget-short v5, p1, Lcom/android/server/tv/DataBaseDesk$MS_Factory_NS_VIF_SET;->VifACIAGCREF:S

    invoke-static {v5}, Ljava/lang/Short;->valueOf(S)Ljava/lang/Short;

    move-result-object v5

    invoke-virtual {v3, v4, v5}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Short;)V

    const-string v4, "VifAgcRefNegative"

    iget-short v5, p1, Lcom/android/server/tv/DataBaseDesk$MS_Factory_NS_VIF_SET;->VifAgcRefNegative:S

    invoke-static {v5}, Ljava/lang/Short;->valueOf(S)Ljava/lang/Short;

    move-result-object v5

    invoke-virtual {v3, v4, v5}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Short;)V

    const-string v4, "GainDistributionThr"

    iget v5, p1, Lcom/android/server/tv/DataBaseDesk$MS_Factory_NS_VIF_SET;->GainDistributionThr:I

    invoke-static {v5}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v5

    invoke-virtual {v3, v4, v5}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Integer;)V

    :try_start_0
    invoke-virtual {p0}, Lcom/android/server/tv/DataBaseDeskImpl;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v4

    const-string v5, "content://mstar.tv.factory/nonstandardadjust"

    invoke-static {v5}, Landroid/net/Uri;->parse(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v5

    const/4 v6, 0x0

    const/4 v7, 0x0

    invoke-virtual {v4, v5, v3, v6, v7}, Landroid/content/ContentResolver;->update(Landroid/net/Uri;Landroid/content/ContentValues;Ljava/lang/String;[Ljava/lang/String;)I
    :try_end_0
    .catch Landroid/database/SQLException; {:try_start_0 .. :try_end_0} :catch_1

    move-result v4

    int-to-long v1, v4

    :goto_3
    const-wide/16 v4, -0x1

    cmp-long v4, v1, v4

    if-nez v4, :cond_0

    sget-object v4, Ljava/lang/System;->out:Ljava/io/PrintStream;

    const-string v5, "update tbl_NonStandardAdjust Vif ignored"

    invoke-virtual {v4, v5}, Ljava/io/PrintStream;->println(Ljava/lang/String;)V

    :cond_0
    :try_start_1
    invoke-static {}, Lcom/mstar/android/tvapi/common/TvManager;->getInstance()Lcom/mstar/android/tvapi/common/TvManager;

    move-result-object v4

    if-eqz v4, :cond_1

    invoke-static {}, Lcom/mstar/android/tvapi/common/TvManager;->getInstance()Lcom/mstar/android/tvapi/common/TvManager;

    move-result-object v4

    invoke-virtual {v4}, Lcom/mstar/android/tvapi/common/TvManager;->getDatabaseManager()Lcom/mstar/android/tvapi/common/DatabaseManager;

    move-result-object v4

    const/16 v5, 0x28

    invoke-virtual {v4, v5}, Lcom/mstar/android/tvapi/common/DatabaseManager;->setDatabaseDirtyByApplication(S)V
    :try_end_1
    .catch Lcom/mstar/android/tvapi/common/exception/TvCommonException; {:try_start_1 .. :try_end_1} :catch_0

    :cond_1
    :goto_4
    return-void

    :cond_2
    move v4, v6

    goto/16 :goto_0

    :cond_3
    move v4, v6

    goto/16 :goto_1

    :cond_4
    move v5, v6

    goto/16 :goto_2

    :catch_0
    move-exception v0

    invoke-virtual {v0}, Lcom/mstar/android/tvapi/common/exception/TvCommonException;->printStackTrace()V

    goto :goto_4

    :catch_1
    move-exception v4

    goto :goto_3
.end method

.method public updateNonStandardAdjust(Lcom/mstar/android/tvapi/factory/vo/FactoryNsVdSet;)V
    .locals 8
    .param p1    # Lcom/mstar/android/tvapi/factory/vo/FactoryNsVdSet;

    const-wide/16 v1, -0x1

    new-instance v3, Landroid/content/ContentValues;

    invoke-direct {v3}, Landroid/content/ContentValues;-><init>()V

    const-string v4, "u8AFEC_D4"

    iget-short v5, p1, Lcom/mstar/android/tvapi/factory/vo/FactoryNsVdSet;->aFEC_D4:S

    invoke-static {v5}, Ljava/lang/Short;->valueOf(S)Ljava/lang/Short;

    move-result-object v5

    invoke-virtual {v3, v4, v5}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Short;)V

    const-string v4, "u8AFEC_D5_Bit2"

    iget-short v5, p1, Lcom/mstar/android/tvapi/factory/vo/FactoryNsVdSet;->aFEC_D5_Bit2:S

    invoke-static {v5}, Ljava/lang/Short;->valueOf(S)Ljava/lang/Short;

    move-result-object v5

    invoke-virtual {v3, v4, v5}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Short;)V

    const-string v4, "u8AFEC_D8_Bit3210"

    iget-short v5, p1, Lcom/mstar/android/tvapi/factory/vo/FactoryNsVdSet;->aFEC_6E_Bit3210:S

    invoke-static {v5}, Ljava/lang/Short;->valueOf(S)Ljava/lang/Short;

    move-result-object v5

    invoke-virtual {v3, v4, v5}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Short;)V

    const-string v4, "u8AFEC_D9_Bit0"

    iget-short v5, p1, Lcom/mstar/android/tvapi/factory/vo/FactoryNsVdSet;->aFEC_D9_Bit0:S

    invoke-static {v5}, Ljava/lang/Short;->valueOf(S)Ljava/lang/Short;

    move-result-object v5

    invoke-virtual {v3, v4, v5}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Short;)V

    const-string v4, "u8AFEC_D7_LOW_BOUND"

    iget-short v5, p1, Lcom/mstar/android/tvapi/factory/vo/FactoryNsVdSet;->aFEC_D7_LOW_BOUND:S

    invoke-static {v5}, Ljava/lang/Short;->valueOf(S)Ljava/lang/Short;

    move-result-object v5

    invoke-virtual {v3, v4, v5}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Short;)V

    const-string v4, "u8AFEC_D7_HIGH_BOUND"

    iget-short v5, p1, Lcom/mstar/android/tvapi/factory/vo/FactoryNsVdSet;->aFEC_D7_HIGH_BOUND:S

    invoke-static {v5}, Ljava/lang/Short;->valueOf(S)Ljava/lang/Short;

    move-result-object v5

    invoke-virtual {v3, v4, v5}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Short;)V

    const-string v4, "u8AFEC_A0"

    iget-short v5, p1, Lcom/mstar/android/tvapi/factory/vo/FactoryNsVdSet;->aFEC_A0:S

    invoke-static {v5}, Ljava/lang/Short;->valueOf(S)Ljava/lang/Short;

    move-result-object v5

    invoke-virtual {v3, v4, v5}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Short;)V

    const-string v4, "u8AFEC_A1"

    iget-short v5, p1, Lcom/mstar/android/tvapi/factory/vo/FactoryNsVdSet;->aFEC_A1:S

    invoke-static {v5}, Ljava/lang/Short;->valueOf(S)Ljava/lang/Short;

    move-result-object v5

    invoke-virtual {v3, v4, v5}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Short;)V

    const-string v4, "u8AFEC_66_Bit76"

    iget-short v5, p1, Lcom/mstar/android/tvapi/factory/vo/FactoryNsVdSet;->aFEC_66_Bit76:S

    invoke-static {v5}, Ljava/lang/Short;->valueOf(S)Ljava/lang/Short;

    move-result-object v5

    invoke-virtual {v3, v4, v5}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Short;)V

    const-string v4, "u8AFEC_6E_Bit7654"

    iget-short v5, p1, Lcom/mstar/android/tvapi/factory/vo/FactoryNsVdSet;->aFEC_6E_Bit7654:S

    invoke-static {v5}, Ljava/lang/Short;->valueOf(S)Ljava/lang/Short;

    move-result-object v5

    invoke-virtual {v3, v4, v5}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Short;)V

    const-string v4, "u8AFEC_6E_Bit3210"

    iget-short v5, p1, Lcom/mstar/android/tvapi/factory/vo/FactoryNsVdSet;->aFEC_6E_Bit3210:S

    invoke-static {v5}, Ljava/lang/Short;->valueOf(S)Ljava/lang/Short;

    move-result-object v5

    invoke-virtual {v3, v4, v5}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Short;)V

    const-string v4, "u8AFEC_43"

    iget-short v5, p1, Lcom/mstar/android/tvapi/factory/vo/FactoryNsVdSet;->aFEC_43:S

    invoke-static {v5}, Ljava/lang/Short;->valueOf(S)Ljava/lang/Short;

    move-result-object v5

    invoke-virtual {v3, v4, v5}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Short;)V

    const-string v4, "u8AFEC_44"

    iget-short v5, p1, Lcom/mstar/android/tvapi/factory/vo/FactoryNsVdSet;->aFEC_44:S

    invoke-static {v5}, Ljava/lang/Short;->valueOf(S)Ljava/lang/Short;

    move-result-object v5

    invoke-virtual {v3, v4, v5}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Short;)V

    const-string v4, "u8AFEC_CB"

    iget-short v5, p1, Lcom/mstar/android/tvapi/factory/vo/FactoryNsVdSet;->aFEC_CB:S

    invoke-static {v5}, Ljava/lang/Short;->valueOf(S)Ljava/lang/Short;

    move-result-object v5

    invoke-virtual {v3, v4, v5}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Short;)V

    const-string v4, "u8AFEC_CF_Bit2_ATV"

    iget-short v5, p1, Lcom/mstar/android/tvapi/factory/vo/FactoryNsVdSet;->aFEC_CF_Bit2_ATV:S

    invoke-static {v5}, Ljava/lang/Short;->valueOf(S)Ljava/lang/Short;

    move-result-object v5

    invoke-virtual {v3, v4, v5}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Short;)V

    const-string v4, "u8AFEC_CF_Bit2_AV"

    iget-short v5, p1, Lcom/mstar/android/tvapi/factory/vo/FactoryNsVdSet;->aFEC_CF_Bit2_AV:S

    invoke-static {v5}, Ljava/lang/Short;->valueOf(S)Ljava/lang/Short;

    move-result-object v5

    invoke-virtual {v3, v4, v5}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Short;)V

    :try_start_0
    invoke-virtual {p0}, Lcom/android/server/tv/DataBaseDeskImpl;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v4

    const-string v5, "content://mstar.tv.factory/nonstandardadjust"

    invoke-static {v5}, Landroid/net/Uri;->parse(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v5

    const/4 v6, 0x0

    const/4 v7, 0x0

    invoke-virtual {v4, v5, v3, v6, v7}, Landroid/content/ContentResolver;->update(Landroid/net/Uri;Landroid/content/ContentValues;Ljava/lang/String;[Ljava/lang/String;)I
    :try_end_0
    .catch Landroid/database/SQLException; {:try_start_0 .. :try_end_0} :catch_1

    move-result v4

    int-to-long v1, v4

    :goto_0
    const-wide/16 v4, -0x1

    cmp-long v4, v1, v4

    if-nez v4, :cond_0

    sget-object v4, Ljava/lang/System;->out:Ljava/io/PrintStream;

    const-string v5, "update tbl_NonLinearAdjust AFEC ignored"

    invoke-virtual {v4, v5}, Ljava/io/PrintStream;->println(Ljava/lang/String;)V

    :cond_0
    :try_start_1
    invoke-static {}, Lcom/mstar/android/tvapi/common/TvManager;->getInstance()Lcom/mstar/android/tvapi/common/TvManager;

    move-result-object v4

    if-eqz v4, :cond_1

    invoke-static {}, Lcom/mstar/android/tvapi/common/TvManager;->getInstance()Lcom/mstar/android/tvapi/common/TvManager;

    move-result-object v4

    invoke-virtual {v4}, Lcom/mstar/android/tvapi/common/TvManager;->getDatabaseManager()Lcom/mstar/android/tvapi/common/DatabaseManager;

    move-result-object v4

    const/16 v5, 0x28

    invoke-virtual {v4, v5}, Lcom/mstar/android/tvapi/common/DatabaseManager;->setDatabaseDirtyByApplication(S)V
    :try_end_1
    .catch Lcom/mstar/android/tvapi/common/exception/TvCommonException; {:try_start_1 .. :try_end_1} :catch_0

    :cond_1
    :goto_1
    return-void

    :catch_0
    move-exception v0

    invoke-virtual {v0}, Lcom/mstar/android/tvapi/common/exception/TvCommonException;->printStackTrace()V

    goto :goto_1

    :catch_1
    move-exception v4

    goto :goto_0
.end method

.method public updateOverscanAdjust(II[[Lcom/android/server/tv/DataBaseDesk$ST_MAPI_VIDEO_WINDOW_INFO;)V
    .locals 8
    .param p1    # I
    .param p2    # I
    .param p3    # [[Lcom/android/server/tv/DataBaseDesk$ST_MAPI_VIDEO_WINDOW_INFO;

    const-wide/16 v1, -0x1

    new-instance v3, Landroid/content/ContentValues;

    invoke-direct {v3}, Landroid/content/ContentValues;-><init>()V

    const-string v4, "u16H_CapStart"

    aget-object v5, p3, p1

    aget-object v5, v5, p2

    iget v5, v5, Lcom/android/server/tv/DataBaseDesk$ST_MAPI_VIDEO_WINDOW_INFO;->u16H_CapStart:I

    invoke-static {v5}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v5

    invoke-virtual {v3, v4, v5}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Integer;)V

    const-string v4, "u16V_CapStart"

    aget-object v5, p3, p1

    aget-object v5, v5, p2

    iget v5, v5, Lcom/android/server/tv/DataBaseDesk$ST_MAPI_VIDEO_WINDOW_INFO;->u16V_CapStart:I

    invoke-static {v5}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v5

    invoke-virtual {v3, v4, v5}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Integer;)V

    const-string v4, "u8HCrop_Left"

    aget-object v5, p3, p1

    aget-object v5, v5, p2

    iget-short v5, v5, Lcom/android/server/tv/DataBaseDesk$ST_MAPI_VIDEO_WINDOW_INFO;->u8HCrop_Left:S

    invoke-static {v5}, Ljava/lang/Short;->valueOf(S)Ljava/lang/Short;

    move-result-object v5

    invoke-virtual {v3, v4, v5}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Short;)V

    const-string v4, "u8HCrop_Right"

    aget-object v5, p3, p1

    aget-object v5, v5, p2

    iget-short v5, v5, Lcom/android/server/tv/DataBaseDesk$ST_MAPI_VIDEO_WINDOW_INFO;->u8HCrop_Right:S

    invoke-static {v5}, Ljava/lang/Short;->valueOf(S)Ljava/lang/Short;

    move-result-object v5

    invoke-virtual {v3, v4, v5}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Short;)V

    const-string v4, "u8VCrop_Up"

    aget-object v5, p3, p1

    aget-object v5, v5, p2

    iget-short v5, v5, Lcom/android/server/tv/DataBaseDesk$ST_MAPI_VIDEO_WINDOW_INFO;->u8VCrop_Up:S

    invoke-static {v5}, Ljava/lang/Short;->valueOf(S)Ljava/lang/Short;

    move-result-object v5

    invoke-virtual {v3, v4, v5}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Short;)V

    const-string v4, "u8VCrop_Down"

    aget-object v5, p3, p1

    aget-object v5, v5, p2

    iget-short v5, v5, Lcom/android/server/tv/DataBaseDesk$ST_MAPI_VIDEO_WINDOW_INFO;->u8VCrop_Down:S

    invoke-static {v5}, Ljava/lang/Short;->valueOf(S)Ljava/lang/Short;

    move-result-object v5

    invoke-virtual {v3, v4, v5}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Short;)V

    :try_start_0
    invoke-virtual {p0}, Lcom/android/server/tv/DataBaseDeskImpl;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v4

    new-instance v5, Ljava/lang/StringBuilder;

    invoke-direct {v5}, Ljava/lang/StringBuilder;-><init>()V

    const-string v6, "content://mstar.tv.factory/overscanadjust/factoryoverscantype/"

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5, p1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v5

    const-string v6, "/_id/"

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5, p2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    invoke-static {v5}, Landroid/net/Uri;->parse(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v5

    const/4 v6, 0x0

    const/4 v7, 0x0

    invoke-virtual {v4, v5, v3, v6, v7}, Landroid/content/ContentResolver;->update(Landroid/net/Uri;Landroid/content/ContentValues;Ljava/lang/String;[Ljava/lang/String;)I
    :try_end_0
    .catch Landroid/database/SQLException; {:try_start_0 .. :try_end_0} :catch_1

    move-result v4

    int-to-long v1, v4

    :goto_0
    const-wide/16 v4, -0x1

    cmp-long v4, v1, v4

    if-nez v4, :cond_0

    sget-object v4, Ljava/lang/System;->out:Ljava/io/PrintStream;

    const-string v5, "update tbl_OverscanAdjust ignored"

    invoke-virtual {v4, v5}, Ljava/io/PrintStream;->println(Ljava/lang/String;)V

    :cond_0
    :try_start_1
    invoke-static {}, Lcom/mstar/android/tvapi/common/TvManager;->getInstance()Lcom/mstar/android/tvapi/common/TvManager;

    move-result-object v4

    if-eqz v4, :cond_1

    invoke-static {}, Lcom/mstar/android/tvapi/common/TvManager;->getInstance()Lcom/mstar/android/tvapi/common/TvManager;

    move-result-object v4

    invoke-virtual {v4}, Lcom/mstar/android/tvapi/common/TvManager;->getDatabaseManager()Lcom/mstar/android/tvapi/common/DatabaseManager;

    move-result-object v4

    const/16 v5, 0x2b

    invoke-virtual {v4, v5}, Lcom/mstar/android/tvapi/common/DatabaseManager;->setDatabaseDirtyByApplication(S)V
    :try_end_1
    .catch Lcom/mstar/android/tvapi/common/exception/TvCommonException; {:try_start_1 .. :try_end_1} :catch_0

    :cond_1
    :goto_1
    return-void

    :catch_0
    move-exception v0

    invoke-virtual {v0}, Lcom/mstar/android/tvapi/common/exception/TvCommonException;->printStackTrace()V

    goto :goto_1

    :catch_1
    move-exception v4

    goto :goto_0
.end method

.method public updateOverscanAdjust(I[[Lcom/android/server/tv/DataBaseDesk$ST_MAPI_VIDEO_WINDOW_INFO;)V
    .locals 13
    .param p1    # I
    .param p2    # [[Lcom/android/server/tv/DataBaseDesk$ST_MAPI_VIDEO_WINDOW_INFO;

    const/4 v4, 0x0

    const-wide/16 v6, -0x1

    packed-switch p1, :pswitch_data_0

    :goto_0
    sget-object v9, Lcom/android/server/tv/DataBaseDesk$MAPI_VIDEO_ARC_Type;->E_AR_MAX:Lcom/android/server/tv/DataBaseDesk$MAPI_VIDEO_ARC_Type;

    invoke-virtual {v9}, Lcom/android/server/tv/DataBaseDesk$MAPI_VIDEO_ARC_Type;->ordinal()I

    move-result v5

    const/4 v0, 0x0

    const/4 v2, 0x0

    :goto_1
    if-ge v2, v4, :cond_2

    const/4 v3, 0x0

    :goto_2
    if-ge v3, v5, :cond_1

    new-instance v8, Landroid/content/ContentValues;

    invoke-direct {v8}, Landroid/content/ContentValues;-><init>()V

    const-string v9, "u16H_CapStart"

    aget-object v10, p2, v2

    aget-object v10, v10, v3

    iget v10, v10, Lcom/android/server/tv/DataBaseDesk$ST_MAPI_VIDEO_WINDOW_INFO;->u16H_CapStart:I

    invoke-static {v10}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v10

    invoke-virtual {v8, v9, v10}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Integer;)V

    const-string v9, "u16V_CapStart"

    aget-object v10, p2, v2

    aget-object v10, v10, v3

    iget v10, v10, Lcom/android/server/tv/DataBaseDesk$ST_MAPI_VIDEO_WINDOW_INFO;->u16V_CapStart:I

    invoke-static {v10}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v10

    invoke-virtual {v8, v9, v10}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Integer;)V

    const-string v9, "u8HCrop_Left"

    aget-object v10, p2, v2

    aget-object v10, v10, v3

    iget-short v10, v10, Lcom/android/server/tv/DataBaseDesk$ST_MAPI_VIDEO_WINDOW_INFO;->u8HCrop_Left:S

    invoke-static {v10}, Ljava/lang/Short;->valueOf(S)Ljava/lang/Short;

    move-result-object v10

    invoke-virtual {v8, v9, v10}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Short;)V

    const-string v9, "u8HCrop_Right"

    aget-object v10, p2, v2

    aget-object v10, v10, v3

    iget-short v10, v10, Lcom/android/server/tv/DataBaseDesk$ST_MAPI_VIDEO_WINDOW_INFO;->u8HCrop_Right:S

    invoke-static {v10}, Ljava/lang/Short;->valueOf(S)Ljava/lang/Short;

    move-result-object v10

    invoke-virtual {v8, v9, v10}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Short;)V

    const-string v9, "u8VCrop_Up"

    aget-object v10, p2, v2

    aget-object v10, v10, v3

    iget-short v10, v10, Lcom/android/server/tv/DataBaseDesk$ST_MAPI_VIDEO_WINDOW_INFO;->u8VCrop_Up:S

    invoke-static {v10}, Ljava/lang/Short;->valueOf(S)Ljava/lang/Short;

    move-result-object v10

    invoke-virtual {v8, v9, v10}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Short;)V

    const-string v9, "u8VCrop_Down"

    aget-object v10, p2, v2

    aget-object v10, v10, v3

    iget-short v10, v10, Lcom/android/server/tv/DataBaseDesk$ST_MAPI_VIDEO_WINDOW_INFO;->u8VCrop_Down:S

    invoke-static {v10}, Ljava/lang/Short;->valueOf(S)Ljava/lang/Short;

    move-result-object v10

    invoke-virtual {v8, v9, v10}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Short;)V

    mul-int v9, v2, v5

    add-int v0, v9, v3

    :try_start_0
    invoke-virtual {p0}, Lcom/android/server/tv/DataBaseDeskImpl;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v9

    new-instance v10, Ljava/lang/StringBuilder;

    invoke-direct {v10}, Ljava/lang/StringBuilder;-><init>()V

    const-string v11, "content://mstar.tv.factory/overscanadjust/factoryoverscantype/"

    invoke-virtual {v10, v11}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v10

    invoke-virtual {v10, p1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v10

    const-string v11, "/_id/"

    invoke-virtual {v10, v11}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v10

    invoke-virtual {v10, v0}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v10

    invoke-virtual {v10}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v10

    invoke-static {v10}, Landroid/net/Uri;->parse(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v10

    const/4 v11, 0x0

    const/4 v12, 0x0

    invoke-virtual {v9, v10, v8, v11, v12}, Landroid/content/ContentResolver;->update(Landroid/net/Uri;Landroid/content/ContentValues;Ljava/lang/String;[Ljava/lang/String;)I
    :try_end_0
    .catch Landroid/database/SQLException; {:try_start_0 .. :try_end_0} :catch_1

    move-result v9

    int-to-long v6, v9

    :goto_3
    const-wide/16 v9, -0x1

    cmp-long v9, v6, v9

    if-nez v9, :cond_0

    sget-object v9, Ljava/lang/System;->out:Ljava/io/PrintStream;

    const-string v10, "update tbl_OverscanAdjust ignored"

    invoke-virtual {v9, v10}, Ljava/io/PrintStream;->println(Ljava/lang/String;)V

    :cond_0
    add-int/lit8 v3, v3, 0x1

    goto/16 :goto_2

    :pswitch_0
    sget-object v9, Lcom/android/server/tv/DataBaseDesk$MAX_DTV_Resolution_Info;->E_DTV_MAX:Lcom/android/server/tv/DataBaseDesk$MAX_DTV_Resolution_Info;

    invoke-virtual {v9}, Lcom/android/server/tv/DataBaseDesk$MAX_DTV_Resolution_Info;->ordinal()I

    move-result v4

    goto/16 :goto_0

    :pswitch_1
    sget-object v9, Lcom/android/server/tv/DataBaseDesk$MAX_HDMI_Resolution_Info;->E_HDMI_MAX:Lcom/android/server/tv/DataBaseDesk$MAX_HDMI_Resolution_Info;

    invoke-virtual {v9}, Lcom/android/server/tv/DataBaseDesk$MAX_HDMI_Resolution_Info;->ordinal()I

    move-result v4

    goto/16 :goto_0

    :pswitch_2
    sget-object v9, Lcom/android/server/tv/DataBaseDesk$MAX_YPbPr_Resolution_Info;->E_YPbPr_MAX:Lcom/android/server/tv/DataBaseDesk$MAX_YPbPr_Resolution_Info;

    invoke-virtual {v9}, Lcom/android/server/tv/DataBaseDesk$MAX_YPbPr_Resolution_Info;->ordinal()I

    move-result v4

    goto/16 :goto_0

    :pswitch_3
    sget-object v9, Lcom/android/server/tv/DataBaseDesk$EN_VD_SIGNALTYPE;->SIG_NUMS:Lcom/android/server/tv/DataBaseDesk$EN_VD_SIGNALTYPE;

    invoke-virtual {v9}, Lcom/android/server/tv/DataBaseDesk$EN_VD_SIGNALTYPE;->ordinal()I

    move-result v4

    goto/16 :goto_0

    :cond_1
    add-int/lit8 v2, v2, 0x1

    goto/16 :goto_1

    :cond_2
    :try_start_1
    invoke-static {}, Lcom/mstar/android/tvapi/common/TvManager;->getInstance()Lcom/mstar/android/tvapi/common/TvManager;

    move-result-object v9

    if-eqz v9, :cond_3

    invoke-static {}, Lcom/mstar/android/tvapi/common/TvManager;->getInstance()Lcom/mstar/android/tvapi/common/TvManager;

    move-result-object v9

    invoke-virtual {v9}, Lcom/mstar/android/tvapi/common/TvManager;->getDatabaseManager()Lcom/mstar/android/tvapi/common/DatabaseManager;

    move-result-object v9

    const/16 v10, 0x2b

    invoke-virtual {v9, v10}, Lcom/mstar/android/tvapi/common/DatabaseManager;->setDatabaseDirtyByApplication(S)V
    :try_end_1
    .catch Lcom/mstar/android/tvapi/common/exception/TvCommonException; {:try_start_1 .. :try_end_1} :catch_0

    :cond_3
    :goto_4
    return-void

    :catch_0
    move-exception v1

    invoke-virtual {v1}, Lcom/mstar/android/tvapi/common/exception/TvCommonException;->printStackTrace()V

    goto :goto_4

    :catch_1
    move-exception v9

    goto :goto_3

    :pswitch_data_0
    .packed-switch 0x0
        :pswitch_0
        :pswitch_1
        :pswitch_2
        :pswitch_3
    .end packed-switch
.end method

.method public updatePEQAdjust(Lcom/android/server/tv/DataBaseDesk$AUDIO_PEQ_PARAM;I)V
    .locals 8
    .param p1    # Lcom/android/server/tv/DataBaseDesk$AUDIO_PEQ_PARAM;
    .param p2    # I

    const-wide/16 v1, -0x1

    new-instance v3, Landroid/content/ContentValues;

    invoke-direct {v3}, Landroid/content/ContentValues;-><init>()V

    const-string v4, "Band"

    iget v5, p1, Lcom/android/server/tv/DataBaseDesk$AUDIO_PEQ_PARAM;->Band:I

    invoke-static {v5}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v5

    invoke-virtual {v3, v4, v5}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Integer;)V

    const-string v4, "Gain"

    iget v5, p1, Lcom/android/server/tv/DataBaseDesk$AUDIO_PEQ_PARAM;->Gain:I

    invoke-static {v5}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v5

    invoke-virtual {v3, v4, v5}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Integer;)V

    const-string v4, "Foh"

    iget v5, p1, Lcom/android/server/tv/DataBaseDesk$AUDIO_PEQ_PARAM;->Foh:I

    invoke-static {v5}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v5

    invoke-virtual {v3, v4, v5}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Integer;)V

    const-string v4, "Fol"

    iget v5, p1, Lcom/android/server/tv/DataBaseDesk$AUDIO_PEQ_PARAM;->Fol:I

    invoke-static {v5}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v5

    invoke-virtual {v3, v4, v5}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Integer;)V

    const-string v4, "QValue"

    iget v5, p1, Lcom/android/server/tv/DataBaseDesk$AUDIO_PEQ_PARAM;->QValue:I

    invoke-static {v5}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v5

    invoke-virtual {v3, v4, v5}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Integer;)V

    :try_start_0
    invoke-virtual {p0}, Lcom/android/server/tv/DataBaseDeskImpl;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v4

    new-instance v5, Ljava/lang/StringBuilder;

    invoke-direct {v5}, Ljava/lang/StringBuilder;-><init>()V

    const-string v6, "content://mstar.tv.factory/peqadjust/"

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5, p2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    invoke-static {v5}, Landroid/net/Uri;->parse(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v5

    const/4 v6, 0x0

    const/4 v7, 0x0

    invoke-virtual {v4, v5, v3, v6, v7}, Landroid/content/ContentResolver;->update(Landroid/net/Uri;Landroid/content/ContentValues;Ljava/lang/String;[Ljava/lang/String;)I
    :try_end_0
    .catch Landroid/database/SQLException; {:try_start_0 .. :try_end_0} :catch_1

    move-result v4

    int-to-long v1, v4

    :goto_0
    const-wide/16 v4, -0x1

    cmp-long v4, v1, v4

    if-nez v4, :cond_0

    sget-object v4, Ljava/lang/System;->out:Ljava/io/PrintStream;

    const-string v5, "update tbl_PEQAdjust ignored"

    invoke-virtual {v4, v5}, Ljava/io/PrintStream;->println(Ljava/lang/String;)V

    :cond_0
    :try_start_1
    invoke-static {}, Lcom/mstar/android/tvapi/common/TvManager;->getInstance()Lcom/mstar/android/tvapi/common/TvManager;

    move-result-object v4

    if-eqz v4, :cond_1

    invoke-static {}, Lcom/mstar/android/tvapi/common/TvManager;->getInstance()Lcom/mstar/android/tvapi/common/TvManager;

    move-result-object v4

    invoke-virtual {v4}, Lcom/mstar/android/tvapi/common/TvManager;->getDatabaseManager()Lcom/mstar/android/tvapi/common/DatabaseManager;

    move-result-object v4

    const/16 v5, 0x2c

    invoke-virtual {v4, v5}, Lcom/mstar/android/tvapi/common/DatabaseManager;->setDatabaseDirtyByApplication(S)V
    :try_end_1
    .catch Lcom/mstar/android/tvapi/common/exception/TvCommonException; {:try_start_1 .. :try_end_1} :catch_0

    :cond_1
    :goto_1
    return-void

    :catch_0
    move-exception v0

    invoke-virtual {v0}, Lcom/mstar/android/tvapi/common/exception/TvCommonException;->printStackTrace()V

    goto :goto_1

    :catch_1
    move-exception v4

    goto :goto_0
.end method

.method public updatePicModeSetting(Lcom/android/server/tv/DataBaseDesk$EN_MS_VIDEOITEM;III)V
    .locals 7
    .param p1    # Lcom/android/server/tv/DataBaseDesk$EN_MS_VIDEOITEM;
    .param p2    # I
    .param p3    # I
    .param p4    # I

    const-wide/16 v0, -0x1

    new-instance v2, Landroid/content/ContentValues;

    invoke-direct {v2}, Landroid/content/ContentValues;-><init>()V

    sget-object v3, Lcom/android/server/tv/DataBaseDeskImpl$1;->$SwitchMap$com$android$server$tv$DataBaseDesk$EN_MS_VIDEOITEM:[I

    invoke-virtual {p1}, Lcom/android/server/tv/DataBaseDesk$EN_MS_VIDEOITEM;->ordinal()I

    move-result v4

    aget v3, v3, v4

    packed-switch v3, :pswitch_data_0

    :goto_0
    :try_start_0
    invoke-virtual {p0}, Lcom/android/server/tv/DataBaseDeskImpl;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v3

    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    iget-object v5, p0, Lcom/android/server/tv/DataBaseDeskImpl;->userSettingCusSchema:Ljava/lang/String;

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    const-string v5, "/picmode_setting/inputsrc/"

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4, p2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v4

    const-string v5, "/picmode/"

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4, p3}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-static {v4}, Landroid/net/Uri;->parse(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v4

    const/4 v5, 0x0

    const/4 v6, 0x0

    invoke-virtual {v3, v4, v2, v5, v6}, Landroid/content/ContentResolver;->update(Landroid/net/Uri;Landroid/content/ContentValues;Ljava/lang/String;[Ljava/lang/String;)I
    :try_end_0
    .catch Landroid/database/SQLException; {:try_start_0 .. :try_end_0} :catch_0

    move-result v3

    int-to-long v0, v3

    :goto_1
    const-wide/16 v3, -0x1

    cmp-long v3, v0, v3

    if-nez v3, :cond_0

    sget-object v3, Ljava/lang/System;->out:Ljava/io/PrintStream;

    const-string v4, "update tbl_PicMode_Setting ignored"

    invoke-virtual {v3, v4}, Ljava/io/PrintStream;->println(Ljava/lang/String;)V

    :cond_0
    return-void

    :pswitch_0
    const-string v3, "u8Brightness"

    invoke-static {p4}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v4

    invoke-virtual {v2, v3, v4}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Integer;)V

    goto :goto_0

    :pswitch_1
    const-string v3, "u8Contrast"

    invoke-static {p4}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v4

    invoke-virtual {v2, v3, v4}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Integer;)V

    goto :goto_0

    :pswitch_2
    const-string v3, "u8Hue"

    invoke-static {p4}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v4

    invoke-virtual {v2, v3, v4}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Integer;)V

    goto :goto_0

    :pswitch_3
    const-string v3, "u8Saturation"

    invoke-static {p4}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v4

    invoke-virtual {v2, v3, v4}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Integer;)V

    goto :goto_0

    :pswitch_4
    const-string v3, "u8Sharpness"

    invoke-static {p4}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v4

    invoke-virtual {v2, v3, v4}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Integer;)V

    goto :goto_0

    :pswitch_5
    const-string v3, "u8Backlight"

    invoke-static {p4}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v4

    invoke-virtual {v2, v3, v4}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Integer;)V

    goto :goto_0

    :catch_0
    move-exception v3

    goto :goto_1

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
        :pswitch_1
        :pswitch_2
        :pswitch_3
        :pswitch_4
        :pswitch_5
    .end packed-switch
.end method

.method public updatePictureMode(II)V
    .locals 6
    .param p1    # I
    .param p2    # I

    new-instance v1, Landroid/content/ContentValues;

    invoke-direct {v1}, Landroid/content/ContentValues;-><init>()V

    const-string v2, "ePicture"

    invoke-static {p1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v3

    invoke-virtual {v1, v2, v3}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Integer;)V

    :try_start_0
    invoke-virtual {p0}, Lcom/android/server/tv/DataBaseDeskImpl;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v2

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string v4, "content://mstar.tv.usersetting/videosetting/inputsrc/"

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3, p2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v3}, Landroid/net/Uri;->parse(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v3

    const/4 v4, 0x0

    const/4 v5, 0x0

    invoke-virtual {v2, v3, v1, v4, v5}, Landroid/content/ContentResolver;->update(Landroid/net/Uri;Landroid/content/ContentValues;Ljava/lang/String;[Ljava/lang/String;)I
    :try_end_0
    .catch Landroid/database/SQLException; {:try_start_0 .. :try_end_0} :catch_0

    :goto_0
    return-void

    :catch_0
    move-exception v0

    const-string v2, "MiniDb"

    const-string v3, "updatePictureMode failed"

    invoke-static {v2, v3}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_0
.end method

.method public updatePowerOnOffMusic(I)V
    .locals 5
    .param p1    # I

    new-instance v0, Landroid/content/ContentValues;

    invoke-direct {v0}, Landroid/content/ContentValues;-><init>()V

    const-string v1, "PoweronMusic"

    invoke-static {p1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Integer;)V

    :try_start_0
    invoke-virtual {p0}, Lcom/android/server/tv/DataBaseDeskImpl;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v1

    const-string v2, "content://mstar.tv.usersetting/soundsetting"

    invoke-static {v2}, Landroid/net/Uri;->parse(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v2

    const/4 v3, 0x0

    const/4 v4, 0x0

    invoke-virtual {v1, v2, v0, v3, v4}, Landroid/content/ContentResolver;->update(Landroid/net/Uri;Landroid/content/ContentValues;Ljava/lang/String;[Ljava/lang/String;)I
    :try_end_0
    .catch Landroid/database/SQLException; {:try_start_0 .. :try_end_0} :catch_0

    :goto_0
    return-void

    :catch_0
    move-exception v1

    goto :goto_0
.end method

.method public updatePowerOnSource(I)V
    .locals 6
    .param p1    # I

    new-instance v1, Landroid/content/ContentValues;

    invoke-direct {v1}, Landroid/content/ContentValues;-><init>()V

    const-string v2, "enInputSourceType"

    invoke-static {p1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v3

    invoke-virtual {v1, v2, v3}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Integer;)V

    :try_start_0
    invoke-virtual {p0}, Lcom/android/server/tv/DataBaseDeskImpl;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v2

    const-string v3, "content://mstar.tv.usersetting/systemsetting"

    invoke-static {v3}, Landroid/net/Uri;->parse(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v3

    const/4 v4, 0x0

    const/4 v5, 0x0

    invoke-virtual {v2, v3, v1, v4, v5}, Landroid/content/ContentResolver;->update(Landroid/net/Uri;Landroid/content/ContentValues;Ljava/lang/String;[Ljava/lang/String;)I
    :try_end_0
    .catch Landroid/database/SQLException; {:try_start_0 .. :try_end_0} :catch_0

    :goto_0
    return-void

    :catch_0
    move-exception v0

    const-string v2, "MiniDb"

    const-string v3, "updatePowerOnSource failed"

    invoke-static {v2, v3}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_0
.end method

.method public updateSSCAdjust(Lcom/android/server/tv/DataBaseDesk$MS_FACTORY_SSC_SET;)V
    .locals 8
    .param p1    # Lcom/android/server/tv/DataBaseDesk$MS_FACTORY_SSC_SET;

    const/4 v5, 0x1

    const/4 v6, 0x0

    const-wide/16 v1, -0x1

    new-instance v3, Landroid/content/ContentValues;

    invoke-direct {v3}, Landroid/content/ContentValues;-><init>()V

    const-string v7, "Miu_SscEnable"

    iget-boolean v4, p1, Lcom/android/server/tv/DataBaseDesk$MS_FACTORY_SSC_SET;->Miu_SscEnable:Z

    if-eqz v4, :cond_2

    move v4, v5

    :goto_0
    invoke-static {v4}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v4

    invoke-virtual {v3, v7, v4}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Integer;)V

    const-string v4, "Lvds_SscEnable"

    iget-boolean v7, p1, Lcom/android/server/tv/DataBaseDesk$MS_FACTORY_SSC_SET;->Lvds_SscEnable:Z

    if-eqz v7, :cond_3

    :goto_1
    invoke-static {v5}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v5

    invoke-virtual {v3, v4, v5}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Integer;)V

    const-string v4, "Lvds_SscSpan"

    iget v5, p1, Lcom/android/server/tv/DataBaseDesk$MS_FACTORY_SSC_SET;->Lvds_SscSpan:I

    invoke-static {v5}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v5

    invoke-virtual {v3, v4, v5}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Integer;)V

    const-string v4, "Lvds_SscStep"

    iget v5, p1, Lcom/android/server/tv/DataBaseDesk$MS_FACTORY_SSC_SET;->Lvds_SscStep:I

    invoke-static {v5}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v5

    invoke-virtual {v3, v4, v5}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Integer;)V

    const-string v4, "Miu_SscSpan"

    iget v5, p1, Lcom/android/server/tv/DataBaseDesk$MS_FACTORY_SSC_SET;->Miu0_SscSpan:I

    invoke-static {v5}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v5

    invoke-virtual {v3, v4, v5}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Integer;)V

    const-string v4, "Miu_SscStep"

    iget v5, p1, Lcom/android/server/tv/DataBaseDesk$MS_FACTORY_SSC_SET;->Miu0_SscStep:I

    invoke-static {v5}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v5

    invoke-virtual {v3, v4, v5}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Integer;)V

    const-string v4, "Miu1_SscSpan"

    iget v5, p1, Lcom/android/server/tv/DataBaseDesk$MS_FACTORY_SSC_SET;->Miu1_SscSpan:I

    invoke-static {v5}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v5

    invoke-virtual {v3, v4, v5}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Integer;)V

    const-string v4, "Miu1_SscStep"

    iget v5, p1, Lcom/android/server/tv/DataBaseDesk$MS_FACTORY_SSC_SET;->Miu1_SscStep:I

    invoke-static {v5}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v5

    invoke-virtual {v3, v4, v5}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Integer;)V

    const-string v4, "Miu2_SscSpan"

    iget v5, p1, Lcom/android/server/tv/DataBaseDesk$MS_FACTORY_SSC_SET;->Miu2_SscSpan:I

    invoke-static {v5}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v5

    invoke-virtual {v3, v4, v5}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Integer;)V

    const-string v4, "Miu2_SscStep"

    iget v5, p1, Lcom/android/server/tv/DataBaseDesk$MS_FACTORY_SSC_SET;->Miu2_SscStep:I

    invoke-static {v5}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v5

    invoke-virtual {v3, v4, v5}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Integer;)V

    :try_start_0
    invoke-virtual {p0}, Lcom/android/server/tv/DataBaseDeskImpl;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v4

    const-string v5, "content://mstar.tv.factory/sscadjust"

    invoke-static {v5}, Landroid/net/Uri;->parse(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v5

    const/4 v6, 0x0

    const/4 v7, 0x0

    invoke-virtual {v4, v5, v3, v6, v7}, Landroid/content/ContentResolver;->update(Landroid/net/Uri;Landroid/content/ContentValues;Ljava/lang/String;[Ljava/lang/String;)I
    :try_end_0
    .catch Landroid/database/SQLException; {:try_start_0 .. :try_end_0} :catch_1

    move-result v4

    int-to-long v1, v4

    :goto_2
    const-wide/16 v4, -0x1

    cmp-long v4, v1, v4

    if-nez v4, :cond_0

    sget-object v4, Ljava/lang/System;->out:Ljava/io/PrintStream;

    const-string v5, "update tbl_SSCAdjust ignored"

    invoke-virtual {v4, v5}, Ljava/io/PrintStream;->println(Ljava/lang/String;)V

    :cond_0
    :try_start_1
    invoke-static {}, Lcom/mstar/android/tvapi/common/TvManager;->getInstance()Lcom/mstar/android/tvapi/common/TvManager;

    move-result-object v4

    if-eqz v4, :cond_1

    invoke-static {}, Lcom/mstar/android/tvapi/common/TvManager;->getInstance()Lcom/mstar/android/tvapi/common/TvManager;

    move-result-object v4

    invoke-virtual {v4}, Lcom/mstar/android/tvapi/common/TvManager;->getDatabaseManager()Lcom/mstar/android/tvapi/common/DatabaseManager;

    move-result-object v4

    const/16 v5, 0x29

    invoke-virtual {v4, v5}, Lcom/mstar/android/tvapi/common/DatabaseManager;->setDatabaseDirtyByApplication(S)V
    :try_end_1
    .catch Lcom/mstar/android/tvapi/common/exception/TvCommonException; {:try_start_1 .. :try_end_1} :catch_0

    :cond_1
    :goto_3
    return-void

    :cond_2
    move v4, v6

    goto/16 :goto_0

    :cond_3
    move v5, v6

    goto/16 :goto_1

    :catch_0
    move-exception v0

    invoke-virtual {v0}, Lcom/mstar/android/tvapi/common/exception/TvCommonException;->printStackTrace()V

    goto :goto_3

    :catch_1
    move-exception v4

    goto :goto_2
.end method

.method public updateSelfAdaptiveLevel(II)V
    .locals 0
    .param p1    # I
    .param p2    # I

    return-void
.end method

.method public updateSeparateHear(I)V
    .locals 6
    .param p1    # I

    new-instance v1, Landroid/content/ContentValues;

    invoke-direct {v1}, Landroid/content/ContentValues;-><init>()V

    const-string v2, "SeparateHearing"

    invoke-static {p1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v3

    invoke-virtual {v1, v2, v3}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Integer;)V

    :try_start_0
    invoke-virtual {p0}, Lcom/android/server/tv/DataBaseDeskImpl;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v2

    const-string v3, "content://mstar.tv.usersetting/soundsetting"

    invoke-static {v3}, Landroid/net/Uri;->parse(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v3

    const/4 v4, 0x0

    const/4 v5, 0x0

    invoke-virtual {v2, v3, v1, v4, v5}, Landroid/content/ContentResolver;->update(Landroid/net/Uri;Landroid/content/ContentValues;Ljava/lang/String;[Ljava/lang/String;)I
    :try_end_0
    .catch Landroid/database/SQLException; {:try_start_0 .. :try_end_0} :catch_0

    :goto_0
    return-void

    :catch_0
    move-exception v0

    const-string v2, "MiniDb"

    const-string v3, "updateSeparateHear failed"

    invoke-static {v2, v3}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_0
.end method

.method public updateServiceName(Ljava/lang/String;S)V
    .locals 8
    .param p1    # Ljava/lang/String;
    .param p2    # S

    const-wide/16 v1, -0x1

    new-instance v3, Landroid/content/ContentValues;

    invoke-direct {v3}, Landroid/content/ContentValues;-><init>()V

    sget-object v4, Ljava/lang/System;->out:Ljava/io/PrintStream;

    new-instance v5, Ljava/lang/StringBuilder;

    invoke-direct {v5}, Ljava/lang/StringBuilder;-><init>()V

    const-string v6, "\n====>>updateServiceName--serviceName "

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    const-string v6, " @epgTimerIndex "

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5, p2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v4, v5}, Ljava/io/PrintStream;->println(Ljava/lang/String;)V

    const-string v4, "sServiceName"

    invoke-virtual {v3, v4, p1}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    :try_start_0
    invoke-virtual {p0}, Lcom/android/server/tv/DataBaseDeskImpl;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v4

    new-instance v5, Ljava/lang/StringBuilder;

    invoke-direct {v5}, Ljava/lang/StringBuilder;-><init>()V

    const-string v6, "content://mstar.tv.usersetting/epgtimer/"

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5, p2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    invoke-static {v5}, Landroid/net/Uri;->parse(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v5

    const/4 v6, 0x0

    const/4 v7, 0x0

    invoke-virtual {v4, v5, v3, v6, v7}, Landroid/content/ContentResolver;->update(Landroid/net/Uri;Landroid/content/ContentValues;Ljava/lang/String;[Ljava/lang/String;)I
    :try_end_0
    .catch Landroid/database/SQLException; {:try_start_0 .. :try_end_0} :catch_1

    move-result v4

    int-to-long v1, v4

    :goto_0
    const-wide/16 v4, -0x1

    cmp-long v4, v1, v4

    if-nez v4, :cond_0

    sget-object v4, Ljava/lang/System;->out:Ljava/io/PrintStream;

    const-string v5, "update tbl_EpgTimer field sServiceName ignored"

    invoke-virtual {v4, v5}, Ljava/io/PrintStream;->println(Ljava/lang/String;)V

    :cond_0
    :try_start_1
    invoke-static {}, Lcom/mstar/android/tvapi/common/TvManager;->getInstance()Lcom/mstar/android/tvapi/common/TvManager;

    move-result-object v4

    if-eqz v4, :cond_1

    invoke-static {}, Lcom/mstar/android/tvapi/common/TvManager;->getInstance()Lcom/mstar/android/tvapi/common/TvManager;

    move-result-object v4

    invoke-virtual {v4}, Lcom/mstar/android/tvapi/common/TvManager;->getDatabaseManager()Lcom/mstar/android/tvapi/common/DatabaseManager;

    move-result-object v4

    const/4 v5, 0x7

    invoke-virtual {v4, v5}, Lcom/mstar/android/tvapi/common/DatabaseManager;->setDatabaseDirtyByApplication(S)V
    :try_end_1
    .catch Lcom/mstar/android/tvapi/common/exception/TvCommonException; {:try_start_1 .. :try_end_1} :catch_0

    :cond_1
    :goto_1
    return-void

    :catch_0
    move-exception v0

    invoke-virtual {v0}, Lcom/mstar/android/tvapi/common/exception/TvCommonException;->printStackTrace()V

    goto :goto_1

    :catch_1
    move-exception v4

    goto :goto_0
.end method

.method public updateSoundMode(I)V
    .locals 7
    .param p1    # I

    const-wide/16 v0, -0x1

    new-instance v2, Landroid/content/ContentValues;

    invoke-direct {v2}, Landroid/content/ContentValues;-><init>()V

    const-string v3, "SoundMode"

    invoke-static {p1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v4

    invoke-virtual {v2, v3, v4}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Integer;)V

    :try_start_0
    invoke-virtual {p0}, Lcom/android/server/tv/DataBaseDeskImpl;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v3

    const-string v4, "content://mstar.tv.usersetting/soundsetting"

    invoke-static {v4}, Landroid/net/Uri;->parse(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v4

    const/4 v5, 0x0

    const/4 v6, 0x0

    invoke-virtual {v3, v4, v2, v5, v6}, Landroid/content/ContentResolver;->update(Landroid/net/Uri;Landroid/content/ContentValues;Ljava/lang/String;[Ljava/lang/String;)I
    :try_end_0
    .catch Landroid/database/SQLException; {:try_start_0 .. :try_end_0} :catch_0

    move-result v3

    int-to-long v0, v3

    :goto_0
    const-wide/16 v3, -0x1

    cmp-long v3, v0, v3

    if-nez v3, :cond_0

    sget-object v3, Ljava/lang/System;->out:Ljava/io/PrintStream;

    const-string v4, "update tbl_SoundSetting ignored"

    invoke-virtual {v3, v4}, Ljava/io/PrintStream;->println(Ljava/lang/String;)V

    :cond_0
    return-void

    :catch_0
    move-exception v3

    goto :goto_0
.end method

.method public updateSoundModeSetting(Lcom/android/server/tv/DataBaseDesk$SoundModeSeting;I)V
    .locals 8
    .param p1    # Lcom/android/server/tv/DataBaseDesk$SoundModeSeting;
    .param p2    # I

    const-wide/16 v1, -0x1

    new-instance v3, Landroid/content/ContentValues;

    invoke-direct {v3}, Landroid/content/ContentValues;-><init>()V

    const-string v4, "Bass"

    iget-short v5, p1, Lcom/android/server/tv/DataBaseDesk$SoundModeSeting;->Bass:S

    invoke-static {v5}, Ljava/lang/Short;->valueOf(S)Ljava/lang/Short;

    move-result-object v5

    invoke-virtual {v3, v4, v5}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Short;)V

    const-string v4, "Treble"

    iget-short v5, p1, Lcom/android/server/tv/DataBaseDesk$SoundModeSeting;->Treble:S

    invoke-static {v5}, Ljava/lang/Short;->valueOf(S)Ljava/lang/Short;

    move-result-object v5

    invoke-virtual {v3, v4, v5}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Short;)V

    const-string v4, "EqBand1"

    iget-short v5, p1, Lcom/android/server/tv/DataBaseDesk$SoundModeSeting;->EqBand1:S

    invoke-static {v5}, Ljava/lang/Short;->valueOf(S)Ljava/lang/Short;

    move-result-object v5

    invoke-virtual {v3, v4, v5}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Short;)V

    const-string v4, "EqBand2"

    iget-short v5, p1, Lcom/android/server/tv/DataBaseDesk$SoundModeSeting;->EqBand2:S

    invoke-static {v5}, Ljava/lang/Short;->valueOf(S)Ljava/lang/Short;

    move-result-object v5

    invoke-virtual {v3, v4, v5}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Short;)V

    const-string v4, "EqBand3"

    iget-short v5, p1, Lcom/android/server/tv/DataBaseDesk$SoundModeSeting;->EqBand3:S

    invoke-static {v5}, Ljava/lang/Short;->valueOf(S)Ljava/lang/Short;

    move-result-object v5

    invoke-virtual {v3, v4, v5}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Short;)V

    const-string v4, "EqBand4"

    iget-short v5, p1, Lcom/android/server/tv/DataBaseDesk$SoundModeSeting;->EqBand4:S

    invoke-static {v5}, Ljava/lang/Short;->valueOf(S)Ljava/lang/Short;

    move-result-object v5

    invoke-virtual {v3, v4, v5}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Short;)V

    const-string v4, "EqBand5"

    iget-short v5, p1, Lcom/android/server/tv/DataBaseDesk$SoundModeSeting;->EqBand5:S

    invoke-static {v5}, Ljava/lang/Short;->valueOf(S)Ljava/lang/Short;

    move-result-object v5

    invoke-virtual {v3, v4, v5}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Short;)V

    const-string v4, "EqBand6"

    iget-short v5, p1, Lcom/android/server/tv/DataBaseDesk$SoundModeSeting;->EqBand6:S

    invoke-static {v5}, Ljava/lang/Short;->valueOf(S)Ljava/lang/Short;

    move-result-object v5

    invoke-virtual {v3, v4, v5}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Short;)V

    const-string v4, "EqBand7"

    iget-short v5, p1, Lcom/android/server/tv/DataBaseDesk$SoundModeSeting;->EqBand7:S

    invoke-static {v5}, Ljava/lang/Short;->valueOf(S)Ljava/lang/Short;

    move-result-object v5

    invoke-virtual {v3, v4, v5}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Short;)V

    const-string v5, "UserMode"

    iget-boolean v4, p1, Lcom/android/server/tv/DataBaseDesk$SoundModeSeting;->UserMode:Z

    if-eqz v4, :cond_2

    const/4 v4, 0x1

    :goto_0
    invoke-static {v4}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v4

    invoke-virtual {v3, v5, v4}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Integer;)V

    const-string v4, "Balance"

    iget-short v5, p1, Lcom/android/server/tv/DataBaseDesk$SoundModeSeting;->Balance:S

    invoke-static {v5}, Ljava/lang/Short;->valueOf(S)Ljava/lang/Short;

    move-result-object v5

    invoke-virtual {v3, v4, v5}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Short;)V

    const-string v4, "enSoundAudioChannel"

    iget-object v5, p1, Lcom/android/server/tv/DataBaseDesk$SoundModeSeting;->enSoundAudioChannel:Lcom/android/server/tv/DataBaseDesk$EN_AUD_MODE;

    invoke-virtual {v5}, Lcom/android/server/tv/DataBaseDesk$EN_AUD_MODE;->ordinal()I

    move-result v5

    invoke-static {v5}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v5

    invoke-virtual {v3, v4, v5}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Integer;)V

    :try_start_0
    invoke-virtual {p0}, Lcom/android/server/tv/DataBaseDeskImpl;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v4

    new-instance v5, Ljava/lang/StringBuilder;

    invoke-direct {v5}, Ljava/lang/StringBuilder;-><init>()V

    iget-object v6, p0, Lcom/android/server/tv/DataBaseDeskImpl;->userSettingCusSchema:Ljava/lang/String;

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    const-string v6, "/soundmodesetting/"

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5, p2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    invoke-static {v5}, Landroid/net/Uri;->parse(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v5

    const/4 v6, 0x0

    const/4 v7, 0x0

    invoke-virtual {v4, v5, v3, v6, v7}, Landroid/content/ContentResolver;->update(Landroid/net/Uri;Landroid/content/ContentValues;Ljava/lang/String;[Ljava/lang/String;)I
    :try_end_0
    .catch Landroid/database/SQLException; {:try_start_0 .. :try_end_0} :catch_1

    move-result v4

    int-to-long v1, v4

    :goto_1
    const-wide/16 v4, -0x1

    cmp-long v4, v1, v4

    if-nez v4, :cond_0

    sget-object v4, Ljava/lang/System;->out:Ljava/io/PrintStream;

    const-string v5, "update tbl_SoundModeSetting ignored"

    invoke-virtual {v4, v5}, Ljava/io/PrintStream;->println(Ljava/lang/String;)V

    :cond_0
    :try_start_1
    invoke-static {}, Lcom/mstar/android/tvapi/common/TvManager;->getInstance()Lcom/mstar/android/tvapi/common/TvManager;

    move-result-object v4

    if-eqz v4, :cond_1

    invoke-static {}, Lcom/mstar/android/tvapi/common/TvManager;->getInstance()Lcom/mstar/android/tvapi/common/TvManager;

    move-result-object v4

    invoke-virtual {v4}, Lcom/mstar/android/tvapi/common/TvManager;->getDatabaseManager()Lcom/mstar/android/tvapi/common/DatabaseManager;

    move-result-object v4

    const/16 v5, 0x16

    invoke-virtual {v4, v5}, Lcom/mstar/android/tvapi/common/DatabaseManager;->setDatabaseDirtyByApplication(S)V
    :try_end_1
    .catch Lcom/mstar/android/tvapi/common/exception/TvCommonException; {:try_start_1 .. :try_end_1} :catch_0

    :cond_1
    :goto_2
    return-void

    :cond_2
    const/4 v4, 0x0

    goto :goto_0

    :catch_0
    move-exception v0

    invoke-virtual {v0}, Lcom/mstar/android/tvapi/common/exception/TvCommonException;->printStackTrace()V

    goto :goto_2

    :catch_1
    move-exception v4

    goto :goto_1
.end method

.method public updateSoundSetting(Lcom/android/server/tv/DataBaseDesk$MS_USER_SOUND_SETTING;)V
    .locals 10
    .param p1    # Lcom/android/server/tv/DataBaseDesk$MS_USER_SOUND_SETTING;

    const/4 v7, 0x1

    const/4 v8, 0x0

    const-wide/16 v3, -0x1

    new-instance v5, Landroid/content/ContentValues;

    invoke-direct {v5}, Landroid/content/ContentValues;-><init>()V

    const-string v6, "SoundMode"

    iget-object v9, p1, Lcom/android/server/tv/DataBaseDesk$MS_USER_SOUND_SETTING;->SoundMode:Lcom/android/server/tv/DataBaseDesk$EN_SOUND_MODE;

    invoke-virtual {v9}, Lcom/android/server/tv/DataBaseDesk$EN_SOUND_MODE;->ordinal()I

    move-result v9

    invoke-static {v9}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v9

    invoke-virtual {v5, v6, v9}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Integer;)V

    const-string v6, "AudysseyDynamicVolume"

    iget-object v9, p1, Lcom/android/server/tv/DataBaseDesk$MS_USER_SOUND_SETTING;->AudysseyDynamicVolume:Lcom/android/server/tv/DataBaseDesk$EN_AUDYSSEY_DYNAMIC_VOLUME_MODE;

    invoke-virtual {v9}, Lcom/android/server/tv/DataBaseDesk$EN_AUDYSSEY_DYNAMIC_VOLUME_MODE;->ordinal()I

    move-result v9

    invoke-static {v9}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v9

    invoke-virtual {v5, v6, v9}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Integer;)V

    const-string v6, "AudysseyEQ"

    iget-object v9, p1, Lcom/android/server/tv/DataBaseDesk$MS_USER_SOUND_SETTING;->AudysseyEQ:Lcom/android/server/tv/DataBaseDesk$EN_AUDYSSEY_EQ_MODE;

    invoke-virtual {v9}, Lcom/android/server/tv/DataBaseDesk$EN_AUDYSSEY_EQ_MODE;->ordinal()I

    move-result v9

    invoke-static {v9}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v9

    invoke-virtual {v5, v6, v9}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Integer;)V

    const-string v6, "SurroundSoundMode"

    iget-object v9, p1, Lcom/android/server/tv/DataBaseDesk$MS_USER_SOUND_SETTING;->SurroundSoundMode:Lcom/android/server/tv/DataBaseDesk$EN_SURROUND_SYSTEM_TYPE;

    invoke-virtual {v9}, Lcom/android/server/tv/DataBaseDesk$EN_SURROUND_SYSTEM_TYPE;->ordinal()I

    move-result v9

    invoke-static {v9}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v9

    invoke-virtual {v5, v6, v9}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Integer;)V

    const-string v6, "Surround"

    iget-object v9, p1, Lcom/android/server/tv/DataBaseDesk$MS_USER_SOUND_SETTING;->SurroundMode:Lcom/android/server/tv/DataBaseDesk$EN_SURROUND_MODE;

    invoke-virtual {v9}, Lcom/android/server/tv/DataBaseDesk$EN_SURROUND_MODE;->ordinal()I

    move-result v9

    invoke-static {v9}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v9

    invoke-virtual {v5, v6, v9}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Integer;)V

    const-string v9, "bEnableAVC"

    iget-boolean v6, p1, Lcom/android/server/tv/DataBaseDesk$MS_USER_SOUND_SETTING;->bEnableAVC:Z

    if-eqz v6, :cond_2

    move v6, v7

    :goto_0
    invoke-static {v6}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v6

    invoke-virtual {v5, v9, v6}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Integer;)V

    const-string v6, "Volume"

    iget-short v9, p1, Lcom/android/server/tv/DataBaseDesk$MS_USER_SOUND_SETTING;->Volume:S

    invoke-static {v9}, Ljava/lang/Short;->valueOf(S)Ljava/lang/Short;

    move-result-object v9

    invoke-virtual {v5, v6, v9}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Short;)V

    const-string v6, "HPVolume"

    iget-short v9, p1, Lcom/android/server/tv/DataBaseDesk$MS_USER_SOUND_SETTING;->HPVolume:S

    invoke-static {v9}, Ljava/lang/Short;->valueOf(S)Ljava/lang/Short;

    move-result-object v9

    invoke-virtual {v5, v6, v9}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Short;)V

    const-string v6, "Balance"

    iget-short v9, p1, Lcom/android/server/tv/DataBaseDesk$MS_USER_SOUND_SETTING;->Balance:S

    invoke-static {v9}, Ljava/lang/Short;->valueOf(S)Ljava/lang/Short;

    move-result-object v9

    invoke-virtual {v5, v6, v9}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Short;)V

    const-string v6, "Primary_Flag"

    iget-short v9, p1, Lcom/android/server/tv/DataBaseDesk$MS_USER_SOUND_SETTING;->Primary_Flag:S

    invoke-static {v9}, Ljava/lang/Short;->valueOf(S)Ljava/lang/Short;

    move-result-object v9

    invoke-virtual {v5, v6, v9}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Short;)V

    iget-object v6, p1, Lcom/android/server/tv/DataBaseDesk$MS_USER_SOUND_SETTING;->enSoundAudioLan1:Lcom/mstar/android/tvapi/common/vo/TvOsType$EnumLanguage;

    sget-object v9, Lcom/mstar/android/tvapi/common/vo/TvOsType$EnumLanguage;->E_ENGLISH:Lcom/mstar/android/tvapi/common/vo/TvOsType$EnumLanguage;

    if-ne v6, v9, :cond_3

    sget-object v1, Lcom/mstar/android/tvapi/common/vo/TvOsType$EnumLanguage;->E_ENGLISH:Lcom/mstar/android/tvapi/common/vo/TvOsType$EnumLanguage;

    :goto_1
    iget-object v6, p1, Lcom/android/server/tv/DataBaseDesk$MS_USER_SOUND_SETTING;->enSoundAudioLan2:Lcom/mstar/android/tvapi/common/vo/TvOsType$EnumLanguage;

    sget-object v9, Lcom/mstar/android/tvapi/common/vo/TvOsType$EnumLanguage;->E_ENGLISH:Lcom/mstar/android/tvapi/common/vo/TvOsType$EnumLanguage;

    if-ne v6, v9, :cond_5

    sget-object v2, Lcom/mstar/android/tvapi/common/vo/TvOsType$EnumLanguage;->E_ENGLISH:Lcom/mstar/android/tvapi/common/vo/TvOsType$EnumLanguage;

    :goto_2
    const-string v6, "enSoundAudioLan1"

    invoke-virtual {v1}, Lcom/mstar/android/tvapi/common/vo/TvOsType$EnumLanguage;->ordinal()I

    move-result v9

    invoke-static {v9}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v9

    invoke-virtual {v5, v6, v9}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Integer;)V

    const-string v6, "enSoundAudioLan2"

    invoke-virtual {v2}, Lcom/mstar/android/tvapi/common/vo/TvOsType$EnumLanguage;->ordinal()I

    move-result v9

    invoke-static {v9}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v9

    invoke-virtual {v5, v6, v9}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Integer;)V

    const-string v6, "MUTE_Flag"

    iget-short v9, p1, Lcom/android/server/tv/DataBaseDesk$MS_USER_SOUND_SETTING;->MUTE_Flag:S

    invoke-static {v9}, Ljava/lang/Short;->valueOf(S)Ljava/lang/Short;

    move-result-object v9

    invoke-virtual {v5, v6, v9}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Short;)V

    const-string v6, "enSoundAudioChannel"

    iget-object v9, p1, Lcom/android/server/tv/DataBaseDesk$MS_USER_SOUND_SETTING;->enSoundAudioChannel:Lcom/android/server/tv/DataBaseDesk$EN_AUD_MODE;

    invoke-virtual {v9}, Lcom/android/server/tv/DataBaseDesk$EN_AUD_MODE;->ordinal()I

    move-result v9

    invoke-static {v9}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v9

    invoke-virtual {v5, v6, v9}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Integer;)V

    const-string v6, "bEnableAD"

    iget-boolean v9, p1, Lcom/android/server/tv/DataBaseDesk$MS_USER_SOUND_SETTING;->bEnableAD:Z

    if-eqz v9, :cond_7

    :goto_3
    invoke-static {v7}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v7

    invoke-virtual {v5, v6, v7}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Integer;)V

    const-string v6, "ADVolume"

    iget-short v7, p1, Lcom/android/server/tv/DataBaseDesk$MS_USER_SOUND_SETTING;->ADVolume:S

    invoke-static {v7}, Ljava/lang/Short;->valueOf(S)Ljava/lang/Short;

    move-result-object v7

    invoke-virtual {v5, v6, v7}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Short;)V

    const-string v6, "ADOutput"

    iget-object v7, p1, Lcom/android/server/tv/DataBaseDesk$MS_USER_SOUND_SETTING;->ADOutput:Lcom/android/server/tv/DataBaseDesk$EN_SOUND_AD_OUTPUT;

    invoke-virtual {v7}, Lcom/android/server/tv/DataBaseDesk$EN_SOUND_AD_OUTPUT;->ordinal()I

    move-result v7

    invoke-static {v7}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v7

    invoke-virtual {v5, v6, v7}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Integer;)V

    const-string v6, "SPDIF_Delay"

    iget-short v7, p1, Lcom/android/server/tv/DataBaseDesk$MS_USER_SOUND_SETTING;->SPDIF_Delay:S

    invoke-static {v7}, Ljava/lang/Short;->valueOf(S)Ljava/lang/Short;

    move-result-object v7

    invoke-virtual {v5, v6, v7}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Short;)V

    const-string v6, "Speaker_Delay"

    iget-short v7, p1, Lcom/android/server/tv/DataBaseDesk$MS_USER_SOUND_SETTING;->Speaker_Delay:S

    invoke-static {v7}, Ljava/lang/Short;->valueOf(S)Ljava/lang/Short;

    move-result-object v7

    invoke-virtual {v5, v6, v7}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Short;)V

    :try_start_0
    invoke-virtual {p0}, Lcom/android/server/tv/DataBaseDeskImpl;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v6

    const-string v7, "content://mstar.tv.usersetting/soundsetting"

    invoke-static {v7}, Landroid/net/Uri;->parse(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v7

    const/4 v8, 0x0

    const/4 v9, 0x0

    invoke-virtual {v6, v7, v5, v8, v9}, Landroid/content/ContentResolver;->update(Landroid/net/Uri;Landroid/content/ContentValues;Ljava/lang/String;[Ljava/lang/String;)I
    :try_end_0
    .catch Landroid/database/SQLException; {:try_start_0 .. :try_end_0} :catch_1

    move-result v6

    int-to-long v3, v6

    :goto_4
    const-wide/16 v6, -0x1

    cmp-long v6, v3, v6

    if-nez v6, :cond_0

    sget-object v6, Ljava/lang/System;->out:Ljava/io/PrintStream;

    const-string v7, "update tbl_SoundSetting ignored"

    invoke-virtual {v6, v7}, Ljava/io/PrintStream;->println(Ljava/lang/String;)V

    :cond_0
    :try_start_1
    invoke-static {}, Lcom/mstar/android/tvapi/common/TvManager;->getInstance()Lcom/mstar/android/tvapi/common/TvManager;

    move-result-object v6

    if-eqz v6, :cond_1

    invoke-static {}, Lcom/mstar/android/tvapi/common/TvManager;->getInstance()Lcom/mstar/android/tvapi/common/TvManager;

    move-result-object v6

    invoke-virtual {v6}, Lcom/mstar/android/tvapi/common/TvManager;->getDatabaseManager()Lcom/mstar/android/tvapi/common/DatabaseManager;

    move-result-object v6

    const/16 v7, 0x17

    invoke-virtual {v6, v7}, Lcom/mstar/android/tvapi/common/DatabaseManager;->setDatabaseDirtyByApplication(S)V
    :try_end_1
    .catch Lcom/mstar/android/tvapi/common/exception/TvCommonException; {:try_start_1 .. :try_end_1} :catch_0

    :cond_1
    :goto_5
    return-void

    :cond_2
    move v6, v8

    goto/16 :goto_0

    :cond_3
    iget-object v6, p1, Lcom/android/server/tv/DataBaseDesk$MS_USER_SOUND_SETTING;->enSoundAudioLan1:Lcom/mstar/android/tvapi/common/vo/TvOsType$EnumLanguage;

    sget-object v9, Lcom/mstar/android/tvapi/common/vo/TvOsType$EnumLanguage;->E_ACHINESE:Lcom/mstar/android/tvapi/common/vo/TvOsType$EnumLanguage;

    if-ne v6, v9, :cond_4

    sget-object v1, Lcom/mstar/android/tvapi/common/vo/TvOsType$EnumLanguage;->E_ACHINESE:Lcom/mstar/android/tvapi/common/vo/TvOsType$EnumLanguage;

    goto/16 :goto_1

    :cond_4
    sget-object v1, Lcom/mstar/android/tvapi/common/vo/TvOsType$EnumLanguage;->E_CHINESE:Lcom/mstar/android/tvapi/common/vo/TvOsType$EnumLanguage;

    goto/16 :goto_1

    :cond_5
    iget-object v6, p1, Lcom/android/server/tv/DataBaseDesk$MS_USER_SOUND_SETTING;->enSoundAudioLan2:Lcom/mstar/android/tvapi/common/vo/TvOsType$EnumLanguage;

    sget-object v9, Lcom/mstar/android/tvapi/common/vo/TvOsType$EnumLanguage;->E_ACHINESE:Lcom/mstar/android/tvapi/common/vo/TvOsType$EnumLanguage;

    if-ne v6, v9, :cond_6

    sget-object v2, Lcom/mstar/android/tvapi/common/vo/TvOsType$EnumLanguage;->E_ACHINESE:Lcom/mstar/android/tvapi/common/vo/TvOsType$EnumLanguage;

    goto/16 :goto_2

    :cond_6
    sget-object v2, Lcom/mstar/android/tvapi/common/vo/TvOsType$EnumLanguage;->E_CHINESE:Lcom/mstar/android/tvapi/common/vo/TvOsType$EnumLanguage;

    goto/16 :goto_2

    :cond_7
    move v7, v8

    goto/16 :goto_3

    :catch_0
    move-exception v0

    invoke-virtual {v0}, Lcom/mstar/android/tvapi/common/exception/TvCommonException;->printStackTrace()V

    goto :goto_5

    :catch_1
    move-exception v6

    goto :goto_4
.end method

.method public updateSpdifMode(I)V
    .locals 5
    .param p1    # I

    new-instance v0, Landroid/content/ContentValues;

    invoke-direct {v0}, Landroid/content/ContentValues;-><init>()V

    const-string v1, "enSPDIFMODE"

    invoke-static {p1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Integer;)V

    :try_start_0
    invoke-virtual {p0}, Lcom/android/server/tv/DataBaseDeskImpl;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v1

    const-string v2, "content://mstar.tv.usersetting/systemsetting"

    invoke-static {v2}, Landroid/net/Uri;->parse(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v2

    const/4 v3, 0x0

    const/4 v4, 0x0

    invoke-virtual {v1, v2, v0, v3, v4}, Landroid/content/ContentResolver;->update(Landroid/net/Uri;Landroid/content/ContentValues;Ljava/lang/String;[Ljava/lang/String;)I
    :try_end_0
    .catch Landroid/database/SQLException; {:try_start_0 .. :try_end_0} :catch_0

    :goto_0
    return-void

    :catch_0
    move-exception v1

    goto :goto_0
.end method

.method public updateSrr(I)V
    .locals 5
    .param p1    # I

    const/4 v1, 0x1

    new-instance v0, Landroid/content/ContentValues;

    invoke-direct {v0}, Landroid/content/ContentValues;-><init>()V

    const-string v2, "Surround"

    if-ne p1, v1, :cond_0

    :goto_0
    invoke-static {v1}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v1

    invoke-virtual {v0, v2, v1}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Boolean;)V

    :try_start_0
    invoke-virtual {p0}, Lcom/android/server/tv/DataBaseDeskImpl;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v1

    const-string v2, "content://mstar.tv.usersetting/soundsetting"

    invoke-static {v2}, Landroid/net/Uri;->parse(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v2

    const/4 v3, 0x0

    const/4 v4, 0x0

    invoke-virtual {v1, v2, v0, v3, v4}, Landroid/content/ContentResolver;->update(Landroid/net/Uri;Landroid/content/ContentValues;Ljava/lang/String;[Ljava/lang/String;)I
    :try_end_0
    .catch Landroid/database/SQLException; {:try_start_0 .. :try_end_0} :catch_0

    :goto_1
    return-void

    :cond_0
    const/4 v1, 0x0

    goto :goto_0

    :catch_0
    move-exception v1

    goto :goto_1
.end method

.method public updateTreble(II)V
    .locals 5
    .param p1    # I
    .param p2    # I

    new-instance v0, Landroid/content/ContentValues;

    invoke-direct {v0}, Landroid/content/ContentValues;-><init>()V

    const-string v1, "Treble"

    invoke-static {p1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Integer;)V

    :try_start_0
    invoke-virtual {p0}, Lcom/android/server/tv/DataBaseDeskImpl;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v1

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    iget-object v3, p0, Lcom/android/server/tv/DataBaseDeskImpl;->userSettingCusSchema:Ljava/lang/String;

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, "/soundmodesetting/"

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, p2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v2}, Landroid/net/Uri;->parse(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v2

    const/4 v3, 0x0

    const/4 v4, 0x0

    invoke-virtual {v1, v2, v0, v3, v4}, Landroid/content/ContentResolver;->update(Landroid/net/Uri;Landroid/content/ContentValues;Ljava/lang/String;[Ljava/lang/String;)I
    :try_end_0
    .catch Landroid/database/SQLException; {:try_start_0 .. :try_end_0} :catch_0

    :goto_0
    return-void

    :catch_0
    move-exception v1

    goto :goto_0
.end method

.method public updateTrueBass(I)V
    .locals 5
    .param p1    # I

    new-instance v0, Landroid/content/ContentValues;

    invoke-direct {v0}, Landroid/content/ContentValues;-><init>()V

    const-string v1, "TrueBass"

    invoke-static {p1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Integer;)V

    :try_start_0
    invoke-virtual {p0}, Lcom/android/server/tv/DataBaseDeskImpl;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v1

    const-string v2, "content://mstar.tv.usersetting/soundsetting"

    invoke-static {v2}, Landroid/net/Uri;->parse(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v2

    const/4 v3, 0x0

    const/4 v4, 0x0

    invoke-virtual {v1, v2, v0, v3, v4}, Landroid/content/ContentResolver;->update(Landroid/net/Uri;Landroid/content/ContentValues;Ljava/lang/String;[Ljava/lang/String;)I
    :try_end_0
    .catch Landroid/database/SQLException; {:try_start_0 .. :try_end_0} :catch_0

    :goto_0
    return-void

    :catch_0
    move-exception v1

    goto :goto_0
.end method

.method public updateUserLocSetting(Lcom/android/server/tv/DataBaseDesk$MS_USER_LOCATION_SETTING;)V
    .locals 8
    .param p1    # Lcom/android/server/tv/DataBaseDesk$MS_USER_LOCATION_SETTING;

    const-wide/16 v1, -0x1

    new-instance v3, Landroid/content/ContentValues;

    invoke-direct {v3}, Landroid/content/ContentValues;-><init>()V

    const-string v4, "u16LocationNo"

    iget v5, p1, Lcom/android/server/tv/DataBaseDesk$MS_USER_LOCATION_SETTING;->mLocationNo:I

    invoke-static {v5}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v5

    invoke-virtual {v3, v4, v5}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Integer;)V

    const-string v4, "s16ManualLongitude"

    iget v5, p1, Lcom/android/server/tv/DataBaseDesk$MS_USER_LOCATION_SETTING;->mManualLongitude:I

    invoke-static {v5}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v5

    invoke-virtual {v3, v4, v5}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Integer;)V

    const-string v4, "s16ManualLatitude"

    iget v5, p1, Lcom/android/server/tv/DataBaseDesk$MS_USER_LOCATION_SETTING;->mManualLatitude:I

    invoke-static {v5}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v5

    invoke-virtual {v3, v4, v5}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Integer;)V

    :try_start_0
    invoke-virtual {p0}, Lcom/android/server/tv/DataBaseDeskImpl;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v4

    const-string v5, "content://mstar.tv.usersetting/userlocationsetting"

    invoke-static {v5}, Landroid/net/Uri;->parse(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v5

    const/4 v6, 0x0

    const/4 v7, 0x0

    invoke-virtual {v4, v5, v3, v6, v7}, Landroid/content/ContentResolver;->update(Landroid/net/Uri;Landroid/content/ContentValues;Ljava/lang/String;[Ljava/lang/String;)I
    :try_end_0
    .catch Landroid/database/SQLException; {:try_start_0 .. :try_end_0} :catch_1

    move-result v4

    int-to-long v1, v4

    :goto_0
    const-wide/16 v4, -0x1

    cmp-long v4, v1, v4

    if-nez v4, :cond_0

    sget-object v4, Ljava/lang/System;->out:Ljava/io/PrintStream;

    const-string v5, "update tbl_UserLocationSetting ignored"

    invoke-virtual {v4, v5}, Ljava/io/PrintStream;->println(Ljava/lang/String;)V

    :cond_0
    :try_start_1
    invoke-static {}, Lcom/mstar/android/tvapi/common/TvManager;->getInstance()Lcom/mstar/android/tvapi/common/TvManager;

    move-result-object v4

    if-eqz v4, :cond_1

    invoke-static {}, Lcom/mstar/android/tvapi/common/TvManager;->getInstance()Lcom/mstar/android/tvapi/common/TvManager;

    move-result-object v4

    invoke-virtual {v4}, Lcom/mstar/android/tvapi/common/TvManager;->getDatabaseManager()Lcom/mstar/android/tvapi/common/DatabaseManager;

    move-result-object v4

    const/16 v5, 0x1e

    invoke-virtual {v4, v5}, Lcom/mstar/android/tvapi/common/DatabaseManager;->setDatabaseDirtyByApplication(S)V
    :try_end_1
    .catch Lcom/mstar/android/tvapi/common/exception/TvCommonException; {:try_start_1 .. :try_end_1} :catch_0

    :cond_1
    :goto_1
    return-void

    :catch_0
    move-exception v0

    invoke-virtual {v0}, Lcom/mstar/android/tvapi/common/exception/TvCommonException;->printStackTrace()V

    goto :goto_1

    :catch_1
    move-exception v4

    goto :goto_0
.end method

.method public updateUserSubtitleSetting(Lcom/android/server/tv/DataBaseDesk$MS_USER_SUBTITLE_SETTING;)V
    .locals 10
    .param p1    # Lcom/android/server/tv/DataBaseDesk$MS_USER_SUBTITLE_SETTING;

    const/4 v7, 0x1

    const/4 v8, 0x0

    const-wide/16 v3, -0x1

    new-instance v5, Landroid/content/ContentValues;

    invoke-direct {v5}, Landroid/content/ContentValues;-><init>()V

    iget-object v6, p1, Lcom/android/server/tv/DataBaseDesk$MS_USER_SUBTITLE_SETTING;->SubtitleDefaultLanguage:Lcom/mstar/android/tvapi/common/vo/TvOsType$EnumLanguage;

    sget-object v9, Lcom/mstar/android/tvapi/common/vo/TvOsType$EnumLanguage;->E_ENGLISH:Lcom/mstar/android/tvapi/common/vo/TvOsType$EnumLanguage;

    if-ne v6, v9, :cond_2

    sget-object v1, Lcom/mstar/android/tvapi/common/vo/TvOsType$EnumLanguage;->E_ENGLISH:Lcom/mstar/android/tvapi/common/vo/TvOsType$EnumLanguage;

    :goto_0
    iget-object v6, p1, Lcom/android/server/tv/DataBaseDesk$MS_USER_SUBTITLE_SETTING;->SubtitleDefaultLanguage_2:Lcom/mstar/android/tvapi/common/vo/TvOsType$EnumLanguage;

    sget-object v9, Lcom/mstar/android/tvapi/common/vo/TvOsType$EnumLanguage;->E_ENGLISH:Lcom/mstar/android/tvapi/common/vo/TvOsType$EnumLanguage;

    if-ne v6, v9, :cond_4

    sget-object v2, Lcom/mstar/android/tvapi/common/vo/TvOsType$EnumLanguage;->E_ENGLISH:Lcom/mstar/android/tvapi/common/vo/TvOsType$EnumLanguage;

    :goto_1
    const-string v6, "SubtitleDefaultLanguage"

    invoke-virtual {v1}, Lcom/mstar/android/tvapi/common/vo/TvOsType$EnumLanguage;->ordinal()I

    move-result v9

    invoke-static {v9}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v9

    invoke-virtual {v5, v6, v9}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Integer;)V

    const-string v6, "SubtitleDefaultLanguage_2"

    invoke-virtual {v2}, Lcom/mstar/android/tvapi/common/vo/TvOsType$EnumLanguage;->ordinal()I

    move-result v9

    invoke-static {v9}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v9

    invoke-virtual {v5, v6, v9}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Integer;)V

    const-string v9, "fHardOfHearing"

    iget-boolean v6, p1, Lcom/android/server/tv/DataBaseDesk$MS_USER_SUBTITLE_SETTING;->fHardOfHearing:Z

    if-eqz v6, :cond_6

    move v6, v7

    :goto_2
    invoke-static {v6}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v6

    invoke-virtual {v5, v9, v6}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Integer;)V

    const-string v6, "fEnableSubTitle"

    iget-boolean v9, p1, Lcom/android/server/tv/DataBaseDesk$MS_USER_SUBTITLE_SETTING;->fEnableSubTitle:Z

    if-eqz v9, :cond_7

    :goto_3
    invoke-static {v7}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v7

    invoke-virtual {v5, v6, v7}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Integer;)V

    :try_start_0
    invoke-virtual {p0}, Lcom/android/server/tv/DataBaseDeskImpl;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v6

    const-string v7, "content://mstar.tv.usersetting/subtitlesetting"

    invoke-static {v7}, Landroid/net/Uri;->parse(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v7

    const/4 v8, 0x0

    const/4 v9, 0x0

    invoke-virtual {v6, v7, v5, v8, v9}, Landroid/content/ContentResolver;->update(Landroid/net/Uri;Landroid/content/ContentValues;Ljava/lang/String;[Ljava/lang/String;)I
    :try_end_0
    .catch Landroid/database/SQLException; {:try_start_0 .. :try_end_0} :catch_1

    move-result v6

    int-to-long v3, v6

    :goto_4
    const-wide/16 v6, -0x1

    cmp-long v6, v3, v6

    if-nez v6, :cond_0

    sget-object v6, Ljava/lang/System;->out:Ljava/io/PrintStream;

    const-string v7, "update tbl_SubtitleSetting ignored"

    invoke-virtual {v6, v7}, Ljava/io/PrintStream;->println(Ljava/lang/String;)V

    :cond_0
    :try_start_1
    invoke-static {}, Lcom/mstar/android/tvapi/common/TvManager;->getInstance()Lcom/mstar/android/tvapi/common/TvManager;

    move-result-object v6

    if-eqz v6, :cond_1

    invoke-static {}, Lcom/mstar/android/tvapi/common/TvManager;->getInstance()Lcom/mstar/android/tvapi/common/TvManager;

    move-result-object v6

    invoke-virtual {v6}, Lcom/mstar/android/tvapi/common/TvManager;->getDatabaseManager()Lcom/mstar/android/tvapi/common/DatabaseManager;

    move-result-object v6

    const/16 v7, 0x18

    invoke-virtual {v6, v7}, Lcom/mstar/android/tvapi/common/DatabaseManager;->setDatabaseDirtyByApplication(S)V
    :try_end_1
    .catch Lcom/mstar/android/tvapi/common/exception/TvCommonException; {:try_start_1 .. :try_end_1} :catch_0

    :cond_1
    :goto_5
    return-void

    :cond_2
    iget-object v6, p1, Lcom/android/server/tv/DataBaseDesk$MS_USER_SUBTITLE_SETTING;->SubtitleDefaultLanguage:Lcom/mstar/android/tvapi/common/vo/TvOsType$EnumLanguage;

    sget-object v9, Lcom/mstar/android/tvapi/common/vo/TvOsType$EnumLanguage;->E_ACHINESE:Lcom/mstar/android/tvapi/common/vo/TvOsType$EnumLanguage;

    if-ne v6, v9, :cond_3

    sget-object v1, Lcom/mstar/android/tvapi/common/vo/TvOsType$EnumLanguage;->E_ACHINESE:Lcom/mstar/android/tvapi/common/vo/TvOsType$EnumLanguage;

    goto :goto_0

    :cond_3
    sget-object v1, Lcom/mstar/android/tvapi/common/vo/TvOsType$EnumLanguage;->E_CHINESE:Lcom/mstar/android/tvapi/common/vo/TvOsType$EnumLanguage;

    goto :goto_0

    :cond_4
    iget-object v6, p1, Lcom/android/server/tv/DataBaseDesk$MS_USER_SUBTITLE_SETTING;->SubtitleDefaultLanguage_2:Lcom/mstar/android/tvapi/common/vo/TvOsType$EnumLanguage;

    sget-object v9, Lcom/mstar/android/tvapi/common/vo/TvOsType$EnumLanguage;->E_ACHINESE:Lcom/mstar/android/tvapi/common/vo/TvOsType$EnumLanguage;

    if-ne v6, v9, :cond_5

    sget-object v2, Lcom/mstar/android/tvapi/common/vo/TvOsType$EnumLanguage;->E_ACHINESE:Lcom/mstar/android/tvapi/common/vo/TvOsType$EnumLanguage;

    goto :goto_1

    :cond_5
    sget-object v2, Lcom/mstar/android/tvapi/common/vo/TvOsType$EnumLanguage;->E_CHINESE:Lcom/mstar/android/tvapi/common/vo/TvOsType$EnumLanguage;

    goto :goto_1

    :cond_6
    move v6, v8

    goto :goto_2

    :cond_7
    move v7, v8

    goto :goto_3

    :catch_0
    move-exception v0

    invoke-virtual {v0}, Lcom/mstar/android/tvapi/common/exception/TvCommonException;->printStackTrace()V

    goto :goto_5

    :catch_1
    move-exception v6

    goto :goto_4
.end method

.method public updateUserSysSetting(Lcom/android/server/tv/DataBaseDesk$MS_USER_SYSTEM_SETTING;)V
    .locals 10
    .param p1    # Lcom/android/server/tv/DataBaseDesk$MS_USER_SYSTEM_SETTING;

    const/4 v6, 0x1

    const/4 v7, 0x0

    const-wide/16 v2, -0x1

    new-instance v4, Landroid/content/ContentValues;

    invoke-direct {v4}, Landroid/content/ContentValues;-><init>()V

    const-string v8, "fRunInstallationGuide"

    iget-boolean v5, p1, Lcom/android/server/tv/DataBaseDesk$MS_USER_SYSTEM_SETTING;->fRunInstallationGuide:Z

    if-eqz v5, :cond_2

    move v5, v6

    :goto_0
    invoke-static {v5}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v5

    invoke-virtual {v4, v8, v5}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Integer;)V

    const-string v8, "fNoChannel"

    iget-boolean v5, p1, Lcom/android/server/tv/DataBaseDesk$MS_USER_SYSTEM_SETTING;->fNoChannel:Z

    if-eqz v5, :cond_3

    move v5, v6

    :goto_1
    invoke-static {v5}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v5

    invoke-virtual {v4, v8, v5}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Integer;)V

    const-string v8, "bDisableSiAutoUpdate"

    iget-boolean v5, p1, Lcom/android/server/tv/DataBaseDesk$MS_USER_SYSTEM_SETTING;->bDisableSiAutoUpdate:Z

    if-eqz v5, :cond_4

    move v5, v6

    :goto_2
    invoke-static {v5}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v5

    invoke-virtual {v4, v8, v5}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Integer;)V

    const-string v5, "Country"

    iget-object v8, p1, Lcom/android/server/tv/DataBaseDesk$MS_USER_SYSTEM_SETTING;->Country:Lcom/android/server/tv/DataBaseDesk$MEMBER_COUNTRY;

    invoke-virtual {v8}, Lcom/android/server/tv/DataBaseDesk$MEMBER_COUNTRY;->ordinal()I

    move-result v8

    invoke-static {v8}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v8

    invoke-virtual {v4, v5, v8}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Integer;)V

    const-string v5, "enCableOperators"

    iget-object v8, p1, Lcom/android/server/tv/DataBaseDesk$MS_USER_SYSTEM_SETTING;->enCableOperators:Lcom/android/server/tv/DataBaseDesk$EN_CABLE_OPERATORS;

    invoke-virtual {v8}, Lcom/android/server/tv/DataBaseDesk$EN_CABLE_OPERATORS;->ordinal()I

    move-result v8

    invoke-static {v8}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v8

    invoke-virtual {v4, v5, v8}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Integer;)V

    const-string v5, "enSatellitePlatform"

    iget-object v8, p1, Lcom/android/server/tv/DataBaseDesk$MS_USER_SYSTEM_SETTING;->enSatellitePlatform:Lcom/android/server/tv/DataBaseDesk$EN_SATELLITE_PLATFORM;

    invoke-virtual {v8}, Lcom/android/server/tv/DataBaseDesk$EN_SATELLITE_PLATFORM;->ordinal()I

    move-result v8

    invoke-static {v8}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v8

    invoke-virtual {v4, v5, v8}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Integer;)V

    iget-object v5, p1, Lcom/android/server/tv/DataBaseDesk$MS_USER_SYSTEM_SETTING;->enLanguage:Lcom/mstar/android/tvapi/common/vo/TvOsType$EnumLanguage;

    sget-object v8, Lcom/mstar/android/tvapi/common/vo/TvOsType$EnumLanguage;->E_ENGLISH:Lcom/mstar/android/tvapi/common/vo/TvOsType$EnumLanguage;

    if-ne v5, v8, :cond_5

    sget-object v1, Lcom/mstar/android/tvapi/common/vo/TvOsType$EnumLanguage;->E_ENGLISH:Lcom/mstar/android/tvapi/common/vo/TvOsType$EnumLanguage;

    :goto_3
    const-string v5, "Language"

    invoke-virtual {v1}, Lcom/mstar/android/tvapi/common/vo/TvOsType$EnumLanguage;->ordinal()I

    move-result v8

    invoke-static {v8}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v8

    invoke-virtual {v4, v5, v8}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Integer;)V

    const-string v5, "enSPDIFMODE"

    iget-object v8, p1, Lcom/android/server/tv/DataBaseDesk$MS_USER_SYSTEM_SETTING;->enSPDIFMODE:Lcom/android/server/tv/DataBaseDesk$SPDIF_TYPE;

    invoke-virtual {v8}, Lcom/android/server/tv/DataBaseDesk$SPDIF_TYPE;->ordinal()I

    move-result v8

    invoke-static {v8}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v8

    invoke-virtual {v4, v5, v8}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Integer;)V

    const-string v5, "fSoftwareUpdate"

    iget-short v8, p1, Lcom/android/server/tv/DataBaseDesk$MS_USER_SYSTEM_SETTING;->fSoftwareUpdate:S

    invoke-static {v8}, Ljava/lang/Short;->valueOf(S)Ljava/lang/Short;

    move-result-object v8

    invoke-virtual {v4, v5, v8}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Short;)V

    const-string v5, "U8OADTime"

    iget-short v8, p1, Lcom/android/server/tv/DataBaseDesk$MS_USER_SYSTEM_SETTING;->u8OADTime:S

    invoke-static {v8}, Ljava/lang/Short;->valueOf(S)Ljava/lang/Short;

    move-result-object v8

    invoke-virtual {v4, v5, v8}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Short;)V

    const-string v5, "fOADScanAfterWakeup"

    iget-short v8, p1, Lcom/android/server/tv/DataBaseDesk$MS_USER_SYSTEM_SETTING;->fOADScanAfterWakeup:S

    invoke-static {v8}, Ljava/lang/Short;->valueOf(S)Ljava/lang/Short;

    move-result-object v8

    invoke-virtual {v4, v5, v8}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Short;)V

    const-string v5, "fAutoVolume"

    iget-short v8, p1, Lcom/android/server/tv/DataBaseDesk$MS_USER_SYSTEM_SETTING;->fAutoVolume:S

    invoke-static {v8}, Ljava/lang/Short;->valueOf(S)Ljava/lang/Short;

    move-result-object v8

    invoke-virtual {v4, v5, v8}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Short;)V

    const-string v5, "fDcPowerOFFMode"

    iget-short v8, p1, Lcom/android/server/tv/DataBaseDesk$MS_USER_SYSTEM_SETTING;->fDcPowerOFFMode:S

    invoke-static {v8}, Ljava/lang/Short;->valueOf(S)Ljava/lang/Short;

    move-result-object v8

    invoke-virtual {v4, v5, v8}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Short;)V

    const-string v5, "DtvRoute"

    iget-short v8, p1, Lcom/android/server/tv/DataBaseDesk$MS_USER_SYSTEM_SETTING;->DtvRoute:S

    invoke-static {v8}, Ljava/lang/Short;->valueOf(S)Ljava/lang/Short;

    move-result-object v8

    invoke-virtual {v4, v5, v8}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Short;)V

    const-string v5, "ScartOutRGB"

    iget-short v8, p1, Lcom/android/server/tv/DataBaseDesk$MS_USER_SYSTEM_SETTING;->ScartOutRGB:S

    invoke-static {v8}, Ljava/lang/Short;->valueOf(S)Ljava/lang/Short;

    move-result-object v8

    invoke-virtual {v4, v5, v8}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Short;)V

    const-string v5, "U8Transparency"

    iget-short v8, p1, Lcom/android/server/tv/DataBaseDesk$MS_USER_SYSTEM_SETTING;->U8Transparency:S

    invoke-static {v8}, Ljava/lang/Short;->valueOf(S)Ljava/lang/Short;

    move-result-object v8

    invoke-virtual {v4, v5, v8}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Short;)V

    const-string v5, "u32MenuTimeOut"

    iget-wide v8, p1, Lcom/android/server/tv/DataBaseDesk$MS_USER_SYSTEM_SETTING;->u32MenuTimeOut:J

    invoke-static {v8, v9}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v8

    invoke-virtual {v4, v5, v8}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Long;)V

    const-string v5, "AudioOnly"

    iget-short v8, p1, Lcom/android/server/tv/DataBaseDesk$MS_USER_SYSTEM_SETTING;->AudioOnly:S

    invoke-static {v8}, Ljava/lang/Short;->valueOf(S)Ljava/lang/Short;

    move-result-object v8

    invoke-virtual {v4, v5, v8}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Short;)V

    const-string v5, "bEnableWDT"

    iget-short v8, p1, Lcom/android/server/tv/DataBaseDesk$MS_USER_SYSTEM_SETTING;->bEnableWDT:S

    invoke-static {v8}, Ljava/lang/Short;->valueOf(S)Ljava/lang/Short;

    move-result-object v8

    invoke-virtual {v4, v5, v8}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Short;)V

    const-string v5, "u8FavoriteRegion"

    iget-short v8, p1, Lcom/android/server/tv/DataBaseDesk$MS_USER_SYSTEM_SETTING;->u8FavoriteRegion:S

    invoke-static {v8}, Ljava/lang/Short;->valueOf(S)Ljava/lang/Short;

    move-result-object v8

    invoke-virtual {v4, v5, v8}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Short;)V

    const-string v5, "u8Bandwidth"

    iget-short v8, p1, Lcom/android/server/tv/DataBaseDesk$MS_USER_SYSTEM_SETTING;->u8Bandwidth:S

    invoke-static {v8}, Ljava/lang/Short;->valueOf(S)Ljava/lang/Short;

    move-result-object v8

    invoke-virtual {v4, v5, v8}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Short;)V

    const-string v5, "u8TimeShiftSizeType"

    iget-short v8, p1, Lcom/android/server/tv/DataBaseDesk$MS_USER_SYSTEM_SETTING;->u8TimeShiftSizeType:S

    invoke-static {v8}, Ljava/lang/Short;->valueOf(S)Ljava/lang/Short;

    move-result-object v8

    invoke-virtual {v4, v5, v8}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Short;)V

    const-string v5, "fOadScan"

    iget-short v8, p1, Lcom/android/server/tv/DataBaseDesk$MS_USER_SYSTEM_SETTING;->fOadScan:S

    invoke-static {v8}, Ljava/lang/Short;->valueOf(S)Ljava/lang/Short;

    move-result-object v8

    invoke-virtual {v4, v5, v8}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Short;)V

    const-string v5, "bEnablePVRRecordAll"

    iget-short v8, p1, Lcom/android/server/tv/DataBaseDesk$MS_USER_SYSTEM_SETTING;->bEnablePVRRecordAll:S

    invoke-static {v8}, Ljava/lang/Short;->valueOf(S)Ljava/lang/Short;

    move-result-object v8

    invoke-virtual {v4, v5, v8}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Short;)V

    const-string v5, "u8ColorRangeMode"

    iget-short v8, p1, Lcom/android/server/tv/DataBaseDesk$MS_USER_SYSTEM_SETTING;->u8ColorRangeMode:S

    invoke-static {v8}, Ljava/lang/Short;->valueOf(S)Ljava/lang/Short;

    move-result-object v8

    invoke-virtual {v4, v5, v8}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Short;)V

    const-string v5, "u8HDMIAudioSource"

    iget-short v8, p1, Lcom/android/server/tv/DataBaseDesk$MS_USER_SYSTEM_SETTING;->u8HDMIAudioSource:S

    invoke-static {v8}, Ljava/lang/Short;->valueOf(S)Ljava/lang/Short;

    move-result-object v8

    invoke-virtual {v4, v5, v8}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Short;)V

    const-string v5, "bEnableAlwaysTimeshift"

    iget-short v8, p1, Lcom/android/server/tv/DataBaseDesk$MS_USER_SYSTEM_SETTING;->bEnableAlwaysTimeshift:S

    invoke-static {v8}, Ljava/lang/Short;->valueOf(S)Ljava/lang/Short;

    move-result-object v8

    invoke-virtual {v4, v5, v8}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Short;)V

    const-string v5, "eSUPER"

    iget-object v8, p1, Lcom/android/server/tv/DataBaseDesk$MS_USER_SYSTEM_SETTING;->eSUPER:Lcom/android/server/tv/DataBaseDesk$EN_MS_SUPER;

    invoke-virtual {v8}, Lcom/android/server/tv/DataBaseDesk$EN_MS_SUPER;->ordinal()I

    move-result v8

    invoke-static {v8}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v8

    invoke-virtual {v4, v5, v8}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Integer;)V

    const-string v8, "bUartBus"

    iget-boolean v5, p1, Lcom/android/server/tv/DataBaseDesk$MS_USER_SYSTEM_SETTING;->bUartBus:Z

    if-eqz v5, :cond_7

    move v5, v6

    :goto_4
    invoke-static {v5}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v5

    invoke-virtual {v4, v8, v5}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Integer;)V

    const-string v5, "m_AutoZoom"

    iget-short v8, p1, Lcom/android/server/tv/DataBaseDesk$MS_USER_SYSTEM_SETTING;->m_AutoZoom:S

    invoke-static {v8}, Ljava/lang/Short;->valueOf(S)Ljava/lang/Short;

    move-result-object v8

    invoke-virtual {v4, v5, v8}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Short;)V

    const-string v8, "bOverScan"

    iget-boolean v5, p1, Lcom/android/server/tv/DataBaseDesk$MS_USER_SYSTEM_SETTING;->bOverScan:Z

    if-eqz v5, :cond_8

    move v5, v6

    :goto_5
    invoke-static {v5}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v5

    invoke-virtual {v4, v8, v5}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Integer;)V

    const-string v5, "m_u8BrazilVideoStandardType"

    iget-short v8, p1, Lcom/android/server/tv/DataBaseDesk$MS_USER_SYSTEM_SETTING;->m_u8BrazilVideoStandardType:S

    invoke-static {v8}, Ljava/lang/Short;->valueOf(S)Ljava/lang/Short;

    move-result-object v8

    invoke-virtual {v4, v5, v8}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Short;)V

    const-string v5, "m_u8SoftwareUpdateMode"

    iget-short v8, p1, Lcom/android/server/tv/DataBaseDesk$MS_USER_SYSTEM_SETTING;->m_u8SoftwareUpdateMode:S

    invoke-static {v8}, Ljava/lang/Short;->valueOf(S)Ljava/lang/Short;

    move-result-object v8

    invoke-virtual {v4, v5, v8}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Short;)V

    const-string v5, "OSD_Active_Time"

    iget-wide v8, p1, Lcom/android/server/tv/DataBaseDesk$MS_USER_SYSTEM_SETTING;->u32OSD_Active_Time:J

    invoke-static {v8, v9}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v8

    invoke-virtual {v4, v5, v8}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Long;)V

    const-string v8, "m_MessageBoxExist"

    iget-boolean v5, p1, Lcom/android/server/tv/DataBaseDesk$MS_USER_SYSTEM_SETTING;->m_MessageBoxExist:Z

    if-eqz v5, :cond_9

    move v5, v6

    :goto_6
    invoke-static {v5}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v5

    invoke-virtual {v4, v8, v5}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Integer;)V

    const-string v5, "u16LastOADVersion"

    iget v8, p1, Lcom/android/server/tv/DataBaseDesk$MS_USER_SYSTEM_SETTING;->u16LastOADVersion:I

    invoke-static {v8}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v8

    invoke-virtual {v4, v5, v8}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Integer;)V

    const-string v5, "bEnableAutoChannelUpdate"

    iget-boolean v8, p1, Lcom/android/server/tv/DataBaseDesk$MS_USER_SYSTEM_SETTING;->bEnableAutoChannelUpdate:Z

    if-eqz v8, :cond_a

    :goto_7
    invoke-static {v6}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v6

    invoke-virtual {v4, v5, v6}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Integer;)V

    const-string v5, "bATVChSwitchFreeze"

    iget-object v6, p1, Lcom/android/server/tv/DataBaseDesk$MS_USER_SYSTEM_SETTING;->eChSwMode:Lcom/android/server/tv/DataBaseDesk$EN_MS_CHANNEL_SWITCH_MODE;

    invoke-virtual {v6}, Lcom/android/server/tv/DataBaseDesk$EN_MS_CHANNEL_SWITCH_MODE;->ordinal()I

    move-result v6

    invoke-static {v6}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v6

    invoke-virtual {v4, v5, v6}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Integer;)V

    :try_start_0
    invoke-virtual {p0}, Lcom/android/server/tv/DataBaseDeskImpl;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v5

    const-string v6, "content://mstar.tv.usersetting/systemsetting"

    invoke-static {v6}, Landroid/net/Uri;->parse(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v6

    const/4 v7, 0x0

    const/4 v8, 0x0

    invoke-virtual {v5, v6, v4, v7, v8}, Landroid/content/ContentResolver;->update(Landroid/net/Uri;Landroid/content/ContentValues;Ljava/lang/String;[Ljava/lang/String;)I
    :try_end_0
    .catch Landroid/database/SQLException; {:try_start_0 .. :try_end_0} :catch_1

    move-result v5

    int-to-long v2, v5

    :goto_8
    const-wide/16 v5, -0x1

    cmp-long v5, v2, v5

    if-nez v5, :cond_0

    sget-object v5, Ljava/lang/System;->out:Ljava/io/PrintStream;

    const-string v6, "update tbl_SystemSetting ignored"

    invoke-virtual {v5, v6}, Ljava/io/PrintStream;->println(Ljava/lang/String;)V

    :cond_0
    :try_start_1
    invoke-static {}, Lcom/mstar/android/tvapi/common/TvManager;->getInstance()Lcom/mstar/android/tvapi/common/TvManager;

    move-result-object v5

    if-eqz v5, :cond_1

    invoke-static {}, Lcom/mstar/android/tvapi/common/TvManager;->getInstance()Lcom/mstar/android/tvapi/common/TvManager;

    move-result-object v5

    invoke-virtual {v5}, Lcom/mstar/android/tvapi/common/TvManager;->getDatabaseManager()Lcom/mstar/android/tvapi/common/DatabaseManager;

    move-result-object v5

    const/16 v6, 0x19

    invoke-virtual {v5, v6}, Lcom/mstar/android/tvapi/common/DatabaseManager;->setDatabaseDirtyByApplication(S)V
    :try_end_1
    .catch Lcom/mstar/android/tvapi/common/exception/TvCommonException; {:try_start_1 .. :try_end_1} :catch_0

    :cond_1
    :goto_9
    return-void

    :cond_2
    move v5, v7

    goto/16 :goto_0

    :cond_3
    move v5, v7

    goto/16 :goto_1

    :cond_4
    move v5, v7

    goto/16 :goto_2

    :cond_5
    iget-object v5, p1, Lcom/android/server/tv/DataBaseDesk$MS_USER_SYSTEM_SETTING;->enLanguage:Lcom/mstar/android/tvapi/common/vo/TvOsType$EnumLanguage;

    sget-object v8, Lcom/mstar/android/tvapi/common/vo/TvOsType$EnumLanguage;->E_ACHINESE:Lcom/mstar/android/tvapi/common/vo/TvOsType$EnumLanguage;

    if-ne v5, v8, :cond_6

    sget-object v1, Lcom/mstar/android/tvapi/common/vo/TvOsType$EnumLanguage;->E_ACHINESE:Lcom/mstar/android/tvapi/common/vo/TvOsType$EnumLanguage;

    goto/16 :goto_3

    :cond_6
    sget-object v1, Lcom/mstar/android/tvapi/common/vo/TvOsType$EnumLanguage;->E_CHINESE:Lcom/mstar/android/tvapi/common/vo/TvOsType$EnumLanguage;

    goto/16 :goto_3

    :cond_7
    move v5, v7

    goto/16 :goto_4

    :cond_8
    move v5, v7

    goto/16 :goto_5

    :cond_9
    move v5, v7

    goto :goto_6

    :cond_a
    move v6, v7

    goto :goto_7

    :catch_0
    move-exception v0

    invoke-virtual {v0}, Lcom/mstar/android/tvapi/common/exception/TvCommonException;->printStackTrace()V

    goto :goto_9

    :catch_1
    move-exception v5

    goto :goto_8
.end method

.method public updateUserSysSetting(Lcom/android/server/tv/DataBaseDeskImpl$USER_SETTING_FIELD;I)V
    .locals 9
    .param p1    # Lcom/android/server/tv/DataBaseDeskImpl$USER_SETTING_FIELD;
    .param p2    # I

    const-wide/16 v2, -0x1

    const-string v1, ""

    sget-object v5, Lcom/android/server/tv/DataBaseDeskImpl$1;->$SwitchMap$com$android$server$tv$DataBaseDeskImpl$USER_SETTING_FIELD:[I

    invoke-virtual {p1}, Lcom/android/server/tv/DataBaseDeskImpl$USER_SETTING_FIELD;->ordinal()I

    move-result v6

    aget v5, v5, v6

    packed-switch v5, :pswitch_data_0

    :cond_0
    :goto_0
    return-void

    :pswitch_0
    const-string v1, "bEnableWDT"

    :goto_1
    new-instance v4, Landroid/content/ContentValues;

    invoke-direct {v4}, Landroid/content/ContentValues;-><init>()V

    invoke-static {p2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v5

    invoke-virtual {v4, v1, v5}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Integer;)V

    :try_start_0
    invoke-virtual {p0}, Lcom/android/server/tv/DataBaseDeskImpl;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v5

    const-string v6, "content://mstar.tv.usersetting/systemsetting"

    invoke-static {v6}, Landroid/net/Uri;->parse(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v6

    const/4 v7, 0x0

    const/4 v8, 0x0

    invoke-virtual {v5, v6, v4, v7, v8}, Landroid/content/ContentResolver;->update(Landroid/net/Uri;Landroid/content/ContentValues;Ljava/lang/String;[Ljava/lang/String;)I
    :try_end_0
    .catch Landroid/database/SQLException; {:try_start_0 .. :try_end_0} :catch_1

    move-result v5

    int-to-long v2, v5

    :goto_2
    const-wide/16 v5, -0x1

    cmp-long v5, v2, v5

    if-nez v5, :cond_1

    sget-object v5, Ljava/lang/System;->out:Ljava/io/PrintStream;

    const-string v6, "update tbl_SystemSetting field bEnableWDT or bUartBus ignored"

    invoke-virtual {v5, v6}, Ljava/io/PrintStream;->println(Ljava/lang/String;)V

    :cond_1
    :try_start_1
    invoke-static {}, Lcom/mstar/android/tvapi/common/TvManager;->getInstance()Lcom/mstar/android/tvapi/common/TvManager;

    move-result-object v5

    if-eqz v5, :cond_0

    invoke-static {}, Lcom/mstar/android/tvapi/common/TvManager;->getInstance()Lcom/mstar/android/tvapi/common/TvManager;

    move-result-object v5

    invoke-virtual {v5}, Lcom/mstar/android/tvapi/common/TvManager;->getDatabaseManager()Lcom/mstar/android/tvapi/common/DatabaseManager;

    move-result-object v5

    const/16 v6, 0x19

    invoke-virtual {v5, v6}, Lcom/mstar/android/tvapi/common/DatabaseManager;->setDatabaseDirtyByApplication(S)V
    :try_end_1
    .catch Lcom/mstar/android/tvapi/common/exception/TvCommonException; {:try_start_1 .. :try_end_1} :catch_0

    goto :goto_0

    :catch_0
    move-exception v0

    invoke-virtual {v0}, Lcom/mstar/android/tvapi/common/exception/TvCommonException;->printStackTrace()V

    goto :goto_0

    :pswitch_1
    const-string v1, "bUartBus"

    goto :goto_1

    :catch_1
    move-exception v5

    goto :goto_2

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
        :pswitch_1
    .end packed-switch
.end method

.method public updateUsrColorTmpData(Lcom/android/server/tv/DataBaseDesk$T_MS_COLOR_TEMP_DATA;)V
    .locals 8
    .param p1    # Lcom/android/server/tv/DataBaseDesk$T_MS_COLOR_TEMP_DATA;

    const-wide/16 v1, -0x1

    new-instance v3, Landroid/content/ContentValues;

    invoke-direct {v3}, Landroid/content/ContentValues;-><init>()V

    const-string v4, "u8RedGain"

    iget-short v5, p1, Lcom/android/server/tv/DataBaseDesk$T_MS_COLOR_TEMP_DATA;->redgain:S

    invoke-static {v5}, Ljava/lang/Short;->valueOf(S)Ljava/lang/Short;

    move-result-object v5

    invoke-virtual {v3, v4, v5}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Short;)V

    const-string v4, "u8GreenGain"

    iget-short v5, p1, Lcom/android/server/tv/DataBaseDesk$T_MS_COLOR_TEMP_DATA;->greengain:S

    invoke-static {v5}, Ljava/lang/Short;->valueOf(S)Ljava/lang/Short;

    move-result-object v5

    invoke-virtual {v3, v4, v5}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Short;)V

    const-string v4, "u8BlueGain"

    iget-short v5, p1, Lcom/android/server/tv/DataBaseDesk$T_MS_COLOR_TEMP_DATA;->bluegain:S

    invoke-static {v5}, Ljava/lang/Short;->valueOf(S)Ljava/lang/Short;

    move-result-object v5

    invoke-virtual {v3, v4, v5}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Short;)V

    const-string v4, "u8RedOffset"

    iget-short v5, p1, Lcom/android/server/tv/DataBaseDesk$T_MS_COLOR_TEMP_DATA;->redoffset:S

    invoke-static {v5}, Ljava/lang/Short;->valueOf(S)Ljava/lang/Short;

    move-result-object v5

    invoke-virtual {v3, v4, v5}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Short;)V

    const-string v4, "u8GreenOffset"

    iget-short v5, p1, Lcom/android/server/tv/DataBaseDesk$T_MS_COLOR_TEMP_DATA;->greenoffset:S

    invoke-static {v5}, Ljava/lang/Short;->valueOf(S)Ljava/lang/Short;

    move-result-object v5

    invoke-virtual {v3, v4, v5}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Short;)V

    const-string v4, "u8BlueOffset"

    iget-short v5, p1, Lcom/android/server/tv/DataBaseDesk$T_MS_COLOR_TEMP_DATA;->blueoffset:S

    invoke-static {v5}, Ljava/lang/Short;->valueOf(S)Ljava/lang/Short;

    move-result-object v5

    invoke-virtual {v3, v4, v5}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Short;)V

    :try_start_0
    invoke-virtual {p0}, Lcom/android/server/tv/DataBaseDeskImpl;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v4

    const-string v5, "content://mstar.tv.usersetting/usercolortemp"

    invoke-static {v5}, Landroid/net/Uri;->parse(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v5

    const/4 v6, 0x0

    const/4 v7, 0x0

    invoke-virtual {v4, v5, v3, v6, v7}, Landroid/content/ContentResolver;->update(Landroid/net/Uri;Landroid/content/ContentValues;Ljava/lang/String;[Ljava/lang/String;)I
    :try_end_0
    .catch Landroid/database/SQLException; {:try_start_0 .. :try_end_0} :catch_1

    move-result v4

    int-to-long v1, v4

    :goto_0
    const-wide/16 v4, -0x1

    cmp-long v4, v1, v4

    if-nez v4, :cond_0

    sget-object v4, Ljava/lang/System;->out:Ljava/io/PrintStream;

    const-string v5, "update tbl_UserColorTemp ignored"

    invoke-virtual {v4, v5}, Ljava/io/PrintStream;->println(Ljava/lang/String;)V

    :cond_0
    :try_start_1
    invoke-static {}, Lcom/mstar/android/tvapi/common/TvManager;->getInstance()Lcom/mstar/android/tvapi/common/TvManager;

    move-result-object v4

    if-eqz v4, :cond_1

    invoke-static {}, Lcom/mstar/android/tvapi/common/TvManager;->getInstance()Lcom/mstar/android/tvapi/common/TvManager;

    move-result-object v4

    invoke-virtual {v4}, Lcom/mstar/android/tvapi/common/TvManager;->getDatabaseManager()Lcom/mstar/android/tvapi/common/DatabaseManager;

    move-result-object v4

    const/16 v5, 0x1c

    invoke-virtual {v4, v5}, Lcom/mstar/android/tvapi/common/DatabaseManager;->setDatabaseDirtyByApplication(S)V
    :try_end_1
    .catch Lcom/mstar/android/tvapi/common/exception/TvCommonException; {:try_start_1 .. :try_end_1} :catch_0

    :cond_1
    :goto_1
    return-void

    :catch_0
    move-exception v0

    invoke-virtual {v0}, Lcom/mstar/android/tvapi/common/exception/TvCommonException;->printStackTrace()V

    goto :goto_1

    :catch_1
    move-exception v4

    goto :goto_0
.end method

.method public updateUsrColorTmpExData(Lcom/mstar/android/tvapi/common/vo/ColorTemperatureExData;I)V
    .locals 7
    .param p1    # Lcom/mstar/android/tvapi/common/vo/ColorTemperatureExData;
    .param p2    # I

    const-wide/16 v0, -0x1

    new-instance v2, Landroid/content/ContentValues;

    invoke-direct {v2}, Landroid/content/ContentValues;-><init>()V

    const-string v3, "u16RedGain"

    iget v4, p1, Lcom/mstar/android/tvapi/common/vo/ColorTemperatureExData;->redGain:I

    invoke-static {v4}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v4

    invoke-virtual {v2, v3, v4}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Integer;)V

    const-string v3, "u16GreenGain"

    iget v4, p1, Lcom/mstar/android/tvapi/common/vo/ColorTemperatureExData;->greenGain:I

    invoke-static {v4}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v4

    invoke-virtual {v2, v3, v4}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Integer;)V

    const-string v3, "u16BlueGain"

    iget v4, p1, Lcom/mstar/android/tvapi/common/vo/ColorTemperatureExData;->blueGain:I

    invoke-static {v4}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v4

    invoke-virtual {v2, v3, v4}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Integer;)V

    const-string v3, "u16RedOffset"

    iget v4, p1, Lcom/mstar/android/tvapi/common/vo/ColorTemperatureExData;->redOffset:I

    invoke-static {v4}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v4

    invoke-virtual {v2, v3, v4}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Integer;)V

    const-string v3, "u16GreenOffset"

    iget v4, p1, Lcom/mstar/android/tvapi/common/vo/ColorTemperatureExData;->greenOffset:I

    invoke-static {v4}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v4

    invoke-virtual {v2, v3, v4}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Integer;)V

    const-string v3, "u16BlueOffset"

    iget v4, p1, Lcom/mstar/android/tvapi/common/vo/ColorTemperatureExData;->blueOffset:I

    invoke-static {v4}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v4

    invoke-virtual {v2, v3, v4}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Integer;)V

    :try_start_0
    invoke-virtual {p0}, Lcom/android/server/tv/DataBaseDeskImpl;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v3

    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    const-string v5, "content://mstar.tv.usersetting/usercolortempex/"

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4, p2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-static {v4}, Landroid/net/Uri;->parse(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v4

    const/4 v5, 0x0

    const/4 v6, 0x0

    invoke-virtual {v3, v4, v2, v5, v6}, Landroid/content/ContentResolver;->update(Landroid/net/Uri;Landroid/content/ContentValues;Ljava/lang/String;[Ljava/lang/String;)I
    :try_end_0
    .catch Landroid/database/SQLException; {:try_start_0 .. :try_end_0} :catch_0

    move-result v3

    int-to-long v0, v3

    :goto_0
    const-wide/16 v3, -0x1

    cmp-long v3, v0, v3

    if-nez v3, :cond_0

    sget-object v3, Ljava/lang/System;->out:Ljava/io/PrintStream;

    const-string v4, "update tbl_UserColorTempEx ignored"

    invoke-virtual {v3, v4}, Ljava/io/PrintStream;->println(Ljava/lang/String;)V

    :cond_0
    return-void

    :catch_0
    move-exception v3

    goto :goto_0
.end method

.method public updateVideo3DMode(Lcom/android/server/tv/DataBaseDesk$ThreeD_Video_MODE;I)V
    .locals 8
    .param p1    # Lcom/android/server/tv/DataBaseDesk$ThreeD_Video_MODE;
    .param p2    # I

    const-wide/16 v1, -0x1

    new-instance v3, Landroid/content/ContentValues;

    invoke-direct {v3}, Landroid/content/ContentValues;-><init>()V

    const-string v4, "eThreeDVideo"

    iget-object v5, p1, Lcom/android/server/tv/DataBaseDesk$ThreeD_Video_MODE;->eThreeDVideo:Lcom/android/server/tv/DataBaseDesk$EN_ThreeD_Video;

    invoke-virtual {v5}, Lcom/android/server/tv/DataBaseDesk$EN_ThreeD_Video;->ordinal()I

    move-result v5

    invoke-static {v5}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v5

    invoke-virtual {v3, v4, v5}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Integer;)V

    const-string v4, "eThreeDVideoSelfAdaptiveDetect"

    iget-object v5, p1, Lcom/android/server/tv/DataBaseDesk$ThreeD_Video_MODE;->eThreeDVideoSelfAdaptiveDetect:Lcom/android/server/tv/DataBaseDesk$EN_ThreeD_Video_SELFADAPTIVE_DETECT;

    invoke-virtual {v5}, Lcom/android/server/tv/DataBaseDesk$EN_ThreeD_Video_SELFADAPTIVE_DETECT;->ordinal()I

    move-result v5

    invoke-static {v5}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v5

    invoke-virtual {v3, v4, v5}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Integer;)V

    const-string v4, "eThreeDVideoDisplayFormat"

    iget-object v5, p1, Lcom/android/server/tv/DataBaseDesk$ThreeD_Video_MODE;->eThreeDVideoDisplayFormat:Lcom/android/server/tv/DataBaseDesk$EN_ThreeD_Video_DISPLAYFORMAT;

    invoke-virtual {v5}, Lcom/android/server/tv/DataBaseDesk$EN_ThreeD_Video_DISPLAYFORMAT;->ordinal()I

    move-result v5

    invoke-static {v5}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v5

    invoke-virtual {v3, v4, v5}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Integer;)V

    const-string v4, "eThreeDVideo3DTo2D"

    iget-object v5, p1, Lcom/android/server/tv/DataBaseDesk$ThreeD_Video_MODE;->eThreeDVideo3DTo2D:Lcom/android/server/tv/DataBaseDesk$EN_ThreeD_Video_3DTO2D;

    invoke-virtual {v5}, Lcom/android/server/tv/DataBaseDesk$EN_ThreeD_Video_3DTO2D;->ordinal()I

    move-result v5

    invoke-static {v5}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v5

    invoke-virtual {v3, v4, v5}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Integer;)V

    const-string v4, "eThreeDVideo3DDepth"

    iget-object v5, p1, Lcom/android/server/tv/DataBaseDesk$ThreeD_Video_MODE;->eThreeDVideo3DDepth:Lcom/android/server/tv/DataBaseDesk$EN_ThreeD_Video_3DDEPTH;

    invoke-virtual {v5}, Lcom/android/server/tv/DataBaseDesk$EN_ThreeD_Video_3DDEPTH;->ordinal()I

    move-result v5

    invoke-static {v5}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v5

    invoke-virtual {v3, v4, v5}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Integer;)V

    const-string v4, "eThreeDVideo3DOffset"

    iget-object v5, p1, Lcom/android/server/tv/DataBaseDesk$ThreeD_Video_MODE;->eThreeDVideo3DOffset:Lcom/android/server/tv/DataBaseDesk$EN_ThreeD_Video_3DOFFSET;

    invoke-virtual {v5}, Lcom/android/server/tv/DataBaseDesk$EN_ThreeD_Video_3DOFFSET;->ordinal()I

    move-result v5

    invoke-static {v5}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v5

    invoke-virtual {v3, v4, v5}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Integer;)V

    const-string v4, "eThreeDVideoAutoStart"

    iget-object v5, p1, Lcom/android/server/tv/DataBaseDesk$ThreeD_Video_MODE;->eThreeDVideoAutoStart:Lcom/android/server/tv/DataBaseDesk$EN_ThreeD_Video_AUTOSTART;

    invoke-virtual {v5}, Lcom/android/server/tv/DataBaseDesk$EN_ThreeD_Video_AUTOSTART;->ordinal()I

    move-result v5

    invoke-static {v5}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v5

    invoke-virtual {v3, v4, v5}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Integer;)V

    const-string v4, "eThreeDVideo3DOutputAspect"

    iget-object v5, p1, Lcom/android/server/tv/DataBaseDesk$ThreeD_Video_MODE;->eThreeDVideo3DOutputAspect:Lcom/android/server/tv/DataBaseDesk$EN_ThreeD_Video_3DOUTPUTASPECT;

    invoke-virtual {v5}, Lcom/android/server/tv/DataBaseDesk$EN_ThreeD_Video_3DOUTPUTASPECT;->ordinal()I

    move-result v5

    invoke-static {v5}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v5

    invoke-virtual {v3, v4, v5}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Integer;)V

    const-string v4, "eThreeDVideoLRViewSwitch"

    iget-object v5, p1, Lcom/android/server/tv/DataBaseDesk$ThreeD_Video_MODE;->eThreeDVideoLRViewSwitch:Lcom/android/server/tv/DataBaseDesk$EN_ThreeD_Video_LRVIEWSWITCH;

    invoke-virtual {v5}, Lcom/android/server/tv/DataBaseDesk$EN_ThreeD_Video_LRVIEWSWITCH;->ordinal()I

    move-result v5

    invoke-static {v5}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v5

    invoke-virtual {v3, v4, v5}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Integer;)V

    :try_start_0
    invoke-virtual {p0}, Lcom/android/server/tv/DataBaseDeskImpl;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v4

    new-instance v5, Ljava/lang/StringBuilder;

    invoke-direct {v5}, Ljava/lang/StringBuilder;-><init>()V

    const-string v6, "content://mstar.tv.usersetting/threedvideomode/inputsrc/"

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5, p2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    invoke-static {v5}, Landroid/net/Uri;->parse(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v5

    const/4 v6, 0x0

    const/4 v7, 0x0

    invoke-virtual {v4, v5, v3, v6, v7}, Landroid/content/ContentResolver;->update(Landroid/net/Uri;Landroid/content/ContentValues;Ljava/lang/String;[Ljava/lang/String;)I
    :try_end_0
    .catch Landroid/database/SQLException; {:try_start_0 .. :try_end_0} :catch_1

    move-result v4

    int-to-long v1, v4

    :goto_0
    const-wide/16 v4, -0x1

    cmp-long v4, v1, v4

    if-nez v4, :cond_0

    sget-object v4, Ljava/lang/System;->out:Ljava/io/PrintStream;

    const-string v5, "update tbl_ThreeDVideoMode ignored"

    invoke-virtual {v4, v5}, Ljava/io/PrintStream;->println(Ljava/lang/String;)V

    :cond_0
    :try_start_1
    invoke-static {}, Lcom/mstar/android/tvapi/common/TvManager;->getInstance()Lcom/mstar/android/tvapi/common/TvManager;

    move-result-object v4

    if-eqz v4, :cond_1

    invoke-static {}, Lcom/mstar/android/tvapi/common/TvManager;->getInstance()Lcom/mstar/android/tvapi/common/TvManager;

    move-result-object v4

    invoke-virtual {v4}, Lcom/mstar/android/tvapi/common/TvManager;->getDatabaseManager()Lcom/mstar/android/tvapi/common/DatabaseManager;

    move-result-object v4

    const/16 v5, 0x1a

    invoke-virtual {v4, v5}, Lcom/mstar/android/tvapi/common/DatabaseManager;->setDatabaseDirtyByApplication(S)V
    :try_end_1
    .catch Lcom/mstar/android/tvapi/common/exception/TvCommonException; {:try_start_1 .. :try_end_1} :catch_0

    :cond_1
    :goto_1
    return-void

    :catch_0
    move-exception v0

    invoke-virtual {v0}, Lcom/mstar/android/tvapi/common/exception/TvCommonException;->printStackTrace()V

    goto :goto_1

    :catch_1
    move-exception v4

    goto :goto_0
.end method

.method public updateVideo3DMode(Lcom/android/server/tv/DataBaseDeskImpl$S3D_TABLE_FIELD;II)V
    .locals 6
    .param p1    # Lcom/android/server/tv/DataBaseDeskImpl$S3D_TABLE_FIELD;
    .param p2    # I
    .param p3    # I

    new-instance v1, Landroid/content/ContentValues;

    invoke-direct {v1}, Landroid/content/ContentValues;-><init>()V

    sget-object v2, Lcom/android/server/tv/DataBaseDeskImpl$1;->$SwitchMap$com$android$server$tv$DataBaseDeskImpl$S3D_TABLE_FIELD:[I

    invoke-virtual {p1}, Lcom/android/server/tv/DataBaseDeskImpl$S3D_TABLE_FIELD;->ordinal()I

    move-result v3

    aget v2, v2, v3

    packed-switch v2, :pswitch_data_0

    :goto_0
    return-void

    :pswitch_0
    const-string v2, "eThreeDVideo"

    invoke-static {p2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v3

    invoke-virtual {v1, v2, v3}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Integer;)V

    :goto_1
    :try_start_0
    invoke-virtual {p0}, Lcom/android/server/tv/DataBaseDeskImpl;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v2

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string v4, "content://mstar.tv.usersetting/threedvideomode/inputsrc/"

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3, p3}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v3}, Landroid/net/Uri;->parse(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v3

    const/4 v4, 0x0

    const/4 v5, 0x0

    invoke-virtual {v2, v3, v1, v4, v5}, Landroid/content/ContentResolver;->update(Landroid/net/Uri;Landroid/content/ContentValues;Ljava/lang/String;[Ljava/lang/String;)I
    :try_end_0
    .catch Landroid/database/SQLException; {:try_start_0 .. :try_end_0} :catch_0

    goto :goto_0

    :catch_0
    move-exception v0

    const-string v2, "MiniDb"

    const-string v3, "updateVideo3DMode failed"

    invoke-static {v2, v3}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_0

    :pswitch_1
    const-string v2, "eThreeDVideoDisplayFormat"

    invoke-static {p2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v3

    invoke-virtual {v1, v2, v3}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Integer;)V

    goto :goto_1

    :pswitch_2
    const-string v2, "eThreeDVideo3DDepth"

    invoke-static {p2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v3

    invoke-virtual {v1, v2, v3}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Integer;)V

    goto :goto_1

    :pswitch_3
    const-string v2, "eThreeDVideoAutoStart"

    invoke-static {p2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v3

    invoke-virtual {v1, v2, v3}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Integer;)V

    goto :goto_1

    :pswitch_4
    const-string v2, "eThreeDVideo3DOutputAspect"

    invoke-static {p2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v3

    invoke-virtual {v1, v2, v3}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Integer;)V

    goto :goto_1

    :pswitch_5
    const-string v2, "eThreeDVideoLRViewSwitch"

    invoke-static {p2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v3

    invoke-virtual {v1, v2, v3}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Integer;)V

    goto :goto_1

    :pswitch_6
    const-string v2, "eThreeDVideoSelfAdaptiveDetect"

    invoke-static {p2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v3

    invoke-virtual {v1, v2, v3}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Integer;)V

    goto :goto_1

    :pswitch_7
    const-string v2, "eThreeDVideoSelfAdaptiveLevel"

    invoke-static {p2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v3

    invoke-virtual {v1, v2, v3}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Integer;)V

    goto :goto_1

    :pswitch_8
    const-string v2, "eThreeDVideo3DTo2D"

    invoke-static {p2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v3

    invoke-virtual {v1, v2, v3}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Integer;)V

    goto :goto_1

    :pswitch_9
    const-string v2, "eThreeDVideo3DOffset"

    invoke-static {p2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v3

    invoke-virtual {v1, v2, v3}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Integer;)V

    goto/16 :goto_1

    nop

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
        :pswitch_1
        :pswitch_2
        :pswitch_3
        :pswitch_4
        :pswitch_5
        :pswitch_6
        :pswitch_7
        :pswitch_8
        :pswitch_9
    .end packed-switch
.end method

.method public updateVideoAstPicture(Lcom/android/server/tv/DataBaseDesk$T_MS_PICTURE;II)V
    .locals 8
    .param p1    # Lcom/android/server/tv/DataBaseDesk$T_MS_PICTURE;
    .param p2    # I
    .param p3    # I

    const-wide/16 v1, -0x1

    new-instance v3, Landroid/content/ContentValues;

    invoke-direct {v3}, Landroid/content/ContentValues;-><init>()V

    const-string v4, "u8Backlight"

    iget-short v5, p1, Lcom/android/server/tv/DataBaseDesk$T_MS_PICTURE;->backlight:S

    invoke-static {v5}, Ljava/lang/Short;->valueOf(S)Ljava/lang/Short;

    move-result-object v5

    invoke-virtual {v3, v4, v5}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Short;)V

    const-string v4, "u8Contrast"

    iget-short v5, p1, Lcom/android/server/tv/DataBaseDesk$T_MS_PICTURE;->contrast:S

    invoke-static {v5}, Ljava/lang/Short;->valueOf(S)Ljava/lang/Short;

    move-result-object v5

    invoke-virtual {v3, v4, v5}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Short;)V

    const-string v4, "u8Brightness"

    iget-short v5, p1, Lcom/android/server/tv/DataBaseDesk$T_MS_PICTURE;->brightness:S

    invoke-static {v5}, Ljava/lang/Short;->valueOf(S)Ljava/lang/Short;

    move-result-object v5

    invoke-virtual {v3, v4, v5}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Short;)V

    const-string v4, "u8Saturation"

    iget-short v5, p1, Lcom/android/server/tv/DataBaseDesk$T_MS_PICTURE;->saturation:S

    invoke-static {v5}, Ljava/lang/Short;->valueOf(S)Ljava/lang/Short;

    move-result-object v5

    invoke-virtual {v3, v4, v5}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Short;)V

    const-string v4, "u8Sharpness"

    iget-short v5, p1, Lcom/android/server/tv/DataBaseDesk$T_MS_PICTURE;->sharpness:S

    invoke-static {v5}, Ljava/lang/Short;->valueOf(S)Ljava/lang/Short;

    move-result-object v5

    invoke-virtual {v3, v4, v5}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Short;)V

    const-string v4, "u8Hue"

    iget-short v5, p1, Lcom/android/server/tv/DataBaseDesk$T_MS_PICTURE;->hue:S

    invoke-static {v5}, Ljava/lang/Short;->valueOf(S)Ljava/lang/Short;

    move-result-object v5

    invoke-virtual {v3, v4, v5}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Short;)V

    const-string v4, "eColorTemp"

    iget-object v5, p1, Lcom/android/server/tv/DataBaseDesk$T_MS_PICTURE;->eColorTemp:Lcom/android/server/tv/DataBaseDesk$EN_MS_COLOR_TEMP;

    invoke-virtual {v5}, Lcom/android/server/tv/DataBaseDesk$EN_MS_COLOR_TEMP;->ordinal()I

    move-result v5

    invoke-static {v5}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v5

    invoke-virtual {v3, v4, v5}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Integer;)V

    const-string v4, "eVibrantColour"

    iget-object v5, p1, Lcom/android/server/tv/DataBaseDesk$T_MS_PICTURE;->eVibrantColour:Lcom/android/server/tv/DataBaseDesk$EN_MS_PIC_ADV;

    invoke-virtual {v5}, Lcom/android/server/tv/DataBaseDesk$EN_MS_PIC_ADV;->ordinal()I

    move-result v5

    invoke-static {v5}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v5

    invoke-virtual {v3, v4, v5}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Integer;)V

    const-string v4, "ePerfectClear"

    iget-object v5, p1, Lcom/android/server/tv/DataBaseDesk$T_MS_PICTURE;->ePerfectClear:Lcom/android/server/tv/DataBaseDesk$EN_MS_PIC_ADV;

    invoke-virtual {v5}, Lcom/android/server/tv/DataBaseDesk$EN_MS_PIC_ADV;->ordinal()I

    move-result v5

    invoke-static {v5}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v5

    invoke-virtual {v3, v4, v5}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Integer;)V

    const-string v4, "eDynamicContrast"

    iget-object v5, p1, Lcom/android/server/tv/DataBaseDesk$T_MS_PICTURE;->eDynamicContrast:Lcom/android/server/tv/DataBaseDesk$EN_MS_PIC_ADV;

    invoke-virtual {v5}, Lcom/android/server/tv/DataBaseDesk$EN_MS_PIC_ADV;->ordinal()I

    move-result v5

    invoke-static {v5}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v5

    invoke-virtual {v3, v4, v5}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Integer;)V

    const-string v4, "eDynamicBacklight"

    iget-object v5, p1, Lcom/android/server/tv/DataBaseDesk$T_MS_PICTURE;->eDynamicBacklight:Lcom/android/server/tv/DataBaseDesk$EN_MS_PIC_ADV;

    invoke-virtual {v5}, Lcom/android/server/tv/DataBaseDesk$EN_MS_PIC_ADV;->ordinal()I

    move-result v5

    invoke-static {v5}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v5

    invoke-virtual {v3, v4, v5}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Integer;)V

    :try_start_0
    invoke-virtual {p0}, Lcom/android/server/tv/DataBaseDeskImpl;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v4

    new-instance v5, Ljava/lang/StringBuilder;

    invoke-direct {v5}, Ljava/lang/StringBuilder;-><init>()V

    iget-object v6, p0, Lcom/android/server/tv/DataBaseDeskImpl;->userSettingCusSchema:Ljava/lang/String;

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    const-string v6, "/picmode_setting/inputsrc/"

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5, p2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v5

    const-string v6, "/picmode/"

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5, p3}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    invoke-static {v5}, Landroid/net/Uri;->parse(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v5

    const/4 v6, 0x0

    const/4 v7, 0x0

    invoke-virtual {v4, v5, v3, v6, v7}, Landroid/content/ContentResolver;->update(Landroid/net/Uri;Landroid/content/ContentValues;Ljava/lang/String;[Ljava/lang/String;)I
    :try_end_0
    .catch Landroid/database/SQLException; {:try_start_0 .. :try_end_0} :catch_1

    move-result v4

    int-to-long v1, v4

    :goto_0
    const-wide/16 v4, -0x1

    cmp-long v4, v1, v4

    if-nez v4, :cond_0

    sget-object v4, Ljava/lang/System;->out:Ljava/io/PrintStream;

    const-string v5, "update tbl_PicMode_Setting ignored"

    invoke-virtual {v4, v5}, Ljava/io/PrintStream;->println(Ljava/lang/String;)V

    :cond_0
    :try_start_1
    invoke-static {}, Lcom/mstar/android/tvapi/common/TvManager;->getInstance()Lcom/mstar/android/tvapi/common/TvManager;

    move-result-object v4

    if-eqz v4, :cond_1

    invoke-static {}, Lcom/mstar/android/tvapi/common/TvManager;->getInstance()Lcom/mstar/android/tvapi/common/TvManager;

    move-result-object v4

    invoke-virtual {v4}, Lcom/mstar/android/tvapi/common/TvManager;->getDatabaseManager()Lcom/mstar/android/tvapi/common/DatabaseManager;

    move-result-object v4

    const/16 v5, 0x14

    invoke-virtual {v4, v5}, Lcom/mstar/android/tvapi/common/DatabaseManager;->setDatabaseDirtyByApplication(S)V
    :try_end_1
    .catch Lcom/mstar/android/tvapi/common/exception/TvCommonException; {:try_start_1 .. :try_end_1} :catch_0

    :cond_1
    :goto_1
    return-void

    :catch_0
    move-exception v0

    invoke-virtual {v0}, Lcom/mstar/android/tvapi/common/exception/TvCommonException;->printStackTrace()V

    goto :goto_1

    :catch_1
    move-exception v4

    goto :goto_0
.end method

.method public updateVideoAstSubColor(Lcom/android/server/tv/DataBaseDesk$T_MS_SUB_COLOR;I)V
    .locals 8
    .param p1    # Lcom/android/server/tv/DataBaseDesk$T_MS_SUB_COLOR;
    .param p2    # I

    const-wide/16 v1, -0x1

    new-instance v3, Landroid/content/ContentValues;

    invoke-direct {v3}, Landroid/content/ContentValues;-><init>()V

    const-string v4, "u8SubBrightness"

    iget-short v5, p1, Lcom/android/server/tv/DataBaseDesk$T_MS_SUB_COLOR;->SubBrightness:S

    invoke-static {v5}, Ljava/lang/Short;->valueOf(S)Ljava/lang/Short;

    move-result-object v5

    invoke-virtual {v3, v4, v5}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Short;)V

    const-string v4, "u8SubContrast"

    iget-short v5, p1, Lcom/android/server/tv/DataBaseDesk$T_MS_SUB_COLOR;->SubContrast:S

    invoke-static {v5}, Ljava/lang/Short;->valueOf(S)Ljava/lang/Short;

    move-result-object v5

    invoke-virtual {v3, v4, v5}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Short;)V

    :try_start_0
    invoke-virtual {p0}, Lcom/android/server/tv/DataBaseDeskImpl;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v4

    new-instance v5, Ljava/lang/StringBuilder;

    invoke-direct {v5}, Ljava/lang/StringBuilder;-><init>()V

    const-string v6, "content://mstar.tv.usersetting/videosetting/inputsrc/"

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5, p2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    invoke-static {v5}, Landroid/net/Uri;->parse(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v5

    const/4 v6, 0x0

    const/4 v7, 0x0

    invoke-virtual {v4, v5, v3, v6, v7}, Landroid/content/ContentResolver;->update(Landroid/net/Uri;Landroid/content/ContentValues;Ljava/lang/String;[Ljava/lang/String;)I
    :try_end_0
    .catch Landroid/database/SQLException; {:try_start_0 .. :try_end_0} :catch_1

    move-result v4

    int-to-long v1, v4

    :goto_0
    const-wide/16 v4, -0x1

    cmp-long v4, v1, v4

    if-nez v4, :cond_0

    sget-object v4, Ljava/lang/System;->out:Ljava/io/PrintStream;

    const-string v5, "update tbl_VideoSetting ignored"

    invoke-virtual {v4, v5}, Ljava/io/PrintStream;->println(Ljava/lang/String;)V

    :cond_0
    :try_start_1
    invoke-static {}, Lcom/mstar/android/tvapi/common/TvManager;->getInstance()Lcom/mstar/android/tvapi/common/TvManager;

    move-result-object v4

    if-eqz v4, :cond_1

    invoke-static {}, Lcom/mstar/android/tvapi/common/TvManager;->getInstance()Lcom/mstar/android/tvapi/common/TvManager;

    move-result-object v4

    invoke-virtual {v4}, Lcom/mstar/android/tvapi/common/TvManager;->getDatabaseManager()Lcom/mstar/android/tvapi/common/DatabaseManager;

    move-result-object v4

    const/16 v5, 0x22

    invoke-virtual {v4, v5}, Lcom/mstar/android/tvapi/common/DatabaseManager;->setDatabaseDirtyByApplication(S)V
    :try_end_1
    .catch Lcom/mstar/android/tvapi/common/exception/TvCommonException; {:try_start_1 .. :try_end_1} :catch_0

    :cond_1
    :goto_1
    return-void

    :catch_0
    move-exception v0

    invoke-virtual {v0}, Lcom/mstar/android/tvapi/common/exception/TvCommonException;->printStackTrace()V

    goto :goto_1

    :catch_1
    move-exception v4

    goto :goto_0
.end method

.method public updateVideoBasePara(Lcom/android/server/tv/DataBaseDesk$T_MS_VIDEO;I)V
    .locals 8
    .param p1    # Lcom/android/server/tv/DataBaseDesk$T_MS_VIDEO;
    .param p2    # I

    const-wide/16 v1, -0x1

    new-instance v3, Landroid/content/ContentValues;

    invoke-direct {v3}, Landroid/content/ContentValues;-><init>()V

    const-string v4, "ePicture"

    iget-object v5, p1, Lcom/android/server/tv/DataBaseDesk$T_MS_VIDEO;->ePicture:Lcom/android/server/tv/DataBaseDesk$EN_MS_PICTURE;

    invoke-virtual {v5}, Lcom/android/server/tv/DataBaseDesk$EN_MS_PICTURE;->ordinal()I

    move-result v5

    invoke-static {v5}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v5

    invoke-virtual {v3, v4, v5}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Integer;)V

    const-string v4, "enARCType"

    iget-object v5, p1, Lcom/android/server/tv/DataBaseDesk$T_MS_VIDEO;->enARCType:Lcom/android/server/tv/DataBaseDesk$MAPI_VIDEO_ARC_Type;

    invoke-virtual {v5}, Lcom/android/server/tv/DataBaseDesk$MAPI_VIDEO_ARC_Type;->ordinal()I

    move-result v5

    invoke-static {v5}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v5

    invoke-virtual {v3, v4, v5}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Integer;)V

    const-string v4, "fOutput_RES"

    iget-object v5, p1, Lcom/android/server/tv/DataBaseDesk$T_MS_VIDEO;->fOutput_RES:Lcom/android/server/tv/DataBaseDesk$EN_DISPLAY_RES_TYPE;

    invoke-virtual {v5}, Lcom/android/server/tv/DataBaseDesk$EN_DISPLAY_RES_TYPE;->ordinal()I

    move-result v5

    invoke-static {v5}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v5

    invoke-virtual {v3, v4, v5}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Integer;)V

    const-string v4, "tvsys"

    iget-object v5, p1, Lcom/android/server/tv/DataBaseDesk$T_MS_VIDEO;->tvsys:Lcom/android/server/tv/DataBaseDesk$MAPI_VIDEO_OUT_VE_SYS;

    invoke-virtual {v5}, Lcom/android/server/tv/DataBaseDesk$MAPI_VIDEO_OUT_VE_SYS;->ordinal()I

    move-result v5

    invoke-static {v5}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v5

    invoke-virtual {v3, v4, v5}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Integer;)V

    const-string v4, "LastVideoStandardMode"

    iget-object v5, p1, Lcom/android/server/tv/DataBaseDesk$T_MS_VIDEO;->LastVideoStandardMode:Lcom/android/server/tv/DataBaseDesk$MAPI_AVD_VideoStandardType;

    invoke-virtual {v5}, Lcom/android/server/tv/DataBaseDesk$MAPI_AVD_VideoStandardType;->ordinal()I

    move-result v5

    invoke-static {v5}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v5

    invoke-virtual {v3, v4, v5}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Integer;)V

    const-string v4, "LastAudioStandardMode"

    iget-object v5, p1, Lcom/android/server/tv/DataBaseDesk$T_MS_VIDEO;->LastAudioStandardMode:Lcom/android/server/tv/DataBaseDesk$AUDIOMODE_TYPE_;

    invoke-virtual {v5}, Lcom/android/server/tv/DataBaseDesk$AUDIOMODE_TYPE_;->ordinal()I

    move-result v5

    invoke-static {v5}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v5

    invoke-virtual {v3, v4, v5}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Integer;)V

    const-string v4, "eDynamic_Contrast"

    iget-object v5, p1, Lcom/android/server/tv/DataBaseDesk$T_MS_VIDEO;->eDynamic_Contrast:Lcom/android/server/tv/DataBaseDesk$EN_MS_Dynamic_Contrast;

    invoke-virtual {v5}, Lcom/android/server/tv/DataBaseDesk$EN_MS_Dynamic_Contrast;->ordinal()I

    move-result v5

    invoke-static {v5}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v5

    invoke-virtual {v3, v4, v5}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Integer;)V

    const-string v4, "eFilm"

    iget-object v5, p1, Lcom/android/server/tv/DataBaseDesk$T_MS_VIDEO;->eFilm:Lcom/android/server/tv/DataBaseDesk$EN_MS_FILM;

    invoke-virtual {v5}, Lcom/android/server/tv/DataBaseDesk$EN_MS_FILM;->ordinal()I

    move-result v5

    invoke-static {v5}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v5

    invoke-virtual {v3, v4, v5}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Integer;)V

    const-string v4, "eTvFormat"

    iget-object v5, p1, Lcom/android/server/tv/DataBaseDesk$T_MS_VIDEO;->eTvFormat:Lcom/android/server/tv/DataBaseDesk$EN_DISPLAY_TVFORMAT;

    invoke-virtual {v5}, Lcom/android/server/tv/DataBaseDesk$EN_DISPLAY_TVFORMAT;->ordinal()I

    move-result v5

    invoke-static {v5}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v5

    invoke-virtual {v3, v4, v5}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Integer;)V

    :try_start_0
    invoke-virtual {p0}, Lcom/android/server/tv/DataBaseDeskImpl;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v4

    new-instance v5, Ljava/lang/StringBuilder;

    invoke-direct {v5}, Ljava/lang/StringBuilder;-><init>()V

    const-string v6, "content://mstar.tv.usersetting/videosetting/inputsrc/"

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5, p2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    invoke-static {v5}, Landroid/net/Uri;->parse(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v5

    const/4 v6, 0x0

    const/4 v7, 0x0

    invoke-virtual {v4, v5, v3, v6, v7}, Landroid/content/ContentResolver;->update(Landroid/net/Uri;Landroid/content/ContentValues;Ljava/lang/String;[Ljava/lang/String;)I
    :try_end_0
    .catch Landroid/database/SQLException; {:try_start_0 .. :try_end_0} :catch_1

    move-result v4

    int-to-long v1, v4

    :goto_0
    const-wide/16 v4, -0x1

    cmp-long v4, v1, v4

    if-nez v4, :cond_0

    sget-object v4, Ljava/lang/System;->out:Ljava/io/PrintStream;

    const-string v5, "update tbl_VideoSetting ignored"

    invoke-virtual {v4, v5}, Ljava/io/PrintStream;->println(Ljava/lang/String;)V

    :cond_0
    :try_start_1
    invoke-static {}, Lcom/mstar/android/tvapi/common/TvManager;->getInstance()Lcom/mstar/android/tvapi/common/TvManager;

    move-result-object v4

    if-eqz v4, :cond_1

    invoke-static {}, Lcom/mstar/android/tvapi/common/TvManager;->getInstance()Lcom/mstar/android/tvapi/common/TvManager;

    move-result-object v4

    invoke-virtual {v4}, Lcom/mstar/android/tvapi/common/TvManager;->getDatabaseManager()Lcom/mstar/android/tvapi/common/DatabaseManager;

    move-result-object v4

    const/16 v5, 0x22

    invoke-virtual {v4, v5}, Lcom/mstar/android/tvapi/common/DatabaseManager;->setDatabaseDirtyByApplication(S)V
    :try_end_1
    .catch Lcom/mstar/android/tvapi/common/exception/TvCommonException; {:try_start_1 .. :try_end_1} :catch_0

    :cond_1
    :goto_1
    return-void

    :catch_0
    move-exception v0

    invoke-virtual {v0}, Lcom/mstar/android/tvapi/common/exception/TvCommonException;->printStackTrace()V

    goto :goto_1

    :catch_1
    move-exception v4

    goto :goto_0
.end method

.method public updateVideoNRMode(Lcom/android/server/tv/DataBaseDesk$T_MS_NR_MODE;II)V
    .locals 8
    .param p1    # Lcom/android/server/tv/DataBaseDesk$T_MS_NR_MODE;
    .param p2    # I
    .param p3    # I

    const-wide/16 v1, -0x1

    new-instance v3, Landroid/content/ContentValues;

    invoke-direct {v3}, Landroid/content/ContentValues;-><init>()V

    const-string v4, "eNR"

    iget-object v5, p1, Lcom/android/server/tv/DataBaseDesk$T_MS_NR_MODE;->eNR:Lcom/android/server/tv/DataBaseDesk$EN_MS_NR;

    invoke-virtual {v5}, Lcom/android/server/tv/DataBaseDesk$EN_MS_NR;->ordinal()I

    move-result v5

    invoke-static {v5}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v5

    invoke-virtual {v3, v4, v5}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Integer;)V

    const-string v4, "eMPEG_NR"

    iget-object v5, p1, Lcom/android/server/tv/DataBaseDesk$T_MS_NR_MODE;->eMPEG_NR:Lcom/android/server/tv/DataBaseDesk$EN_MS_MPEG_NR;

    invoke-virtual {v5}, Lcom/android/server/tv/DataBaseDesk$EN_MS_MPEG_NR;->ordinal()I

    move-result v5

    invoke-static {v5}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v5

    invoke-virtual {v3, v4, v5}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Integer;)V

    :try_start_0
    invoke-virtual {p0}, Lcom/android/server/tv/DataBaseDeskImpl;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v4

    new-instance v5, Ljava/lang/StringBuilder;

    invoke-direct {v5}, Ljava/lang/StringBuilder;-><init>()V

    const-string v6, "content://mstar.tv.usersetting/nrmode/nrmode/"

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5, p3}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v5

    const-string v6, "/inputsrc/"

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5, p2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    invoke-static {v5}, Landroid/net/Uri;->parse(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v5

    const/4 v6, 0x0

    const/4 v7, 0x0

    invoke-virtual {v4, v5, v3, v6, v7}, Landroid/content/ContentResolver;->update(Landroid/net/Uri;Landroid/content/ContentValues;Ljava/lang/String;[Ljava/lang/String;)I
    :try_end_0
    .catch Landroid/database/SQLException; {:try_start_0 .. :try_end_0} :catch_1

    move-result v4

    int-to-long v1, v4

    :goto_0
    const-wide/16 v4, -0x1

    cmp-long v4, v1, v4

    if-nez v4, :cond_0

    sget-object v4, Ljava/lang/System;->out:Ljava/io/PrintStream;

    const-string v5, "update tbl_NRMode ignored"

    invoke-virtual {v4, v5}, Ljava/io/PrintStream;->println(Ljava/lang/String;)V

    :cond_0
    :try_start_1
    invoke-static {}, Lcom/mstar/android/tvapi/common/TvManager;->getInstance()Lcom/mstar/android/tvapi/common/TvManager;

    move-result-object v4

    if-eqz v4, :cond_1

    invoke-static {}, Lcom/mstar/android/tvapi/common/TvManager;->getInstance()Lcom/mstar/android/tvapi/common/TvManager;

    move-result-object v4

    invoke-virtual {v4}, Lcom/mstar/android/tvapi/common/TvManager;->getDatabaseManager()Lcom/mstar/android/tvapi/common/DatabaseManager;

    move-result-object v4

    const/16 v5, 0xe

    invoke-virtual {v4, v5}, Lcom/mstar/android/tvapi/common/DatabaseManager;->setDatabaseDirtyByApplication(S)V
    :try_end_1
    .catch Lcom/mstar/android/tvapi/common/exception/TvCommonException; {:try_start_1 .. :try_end_1} :catch_0

    :cond_1
    :goto_1
    return-void

    :catch_0
    move-exception v0

    invoke-virtual {v0}, Lcom/mstar/android/tvapi/common/exception/TvCommonException;->printStackTrace()V

    goto :goto_1

    :catch_1
    move-exception v4

    goto :goto_0
.end method

.method public updateVideoUserOverScanMode(Lcom/android/server/tv/DataBaseDesk$T_MS_OVERSCAN_SETTING_USER;I)V
    .locals 8
    .param p1    # Lcom/android/server/tv/DataBaseDesk$T_MS_OVERSCAN_SETTING_USER;
    .param p2    # I

    const-wide/16 v1, -0x1

    new-instance v3, Landroid/content/ContentValues;

    invoke-direct {v3}, Landroid/content/ContentValues;-><init>()V

    const-string v4, "OverScanHposition"

    iget-short v5, p1, Lcom/android/server/tv/DataBaseDesk$T_MS_OVERSCAN_SETTING_USER;->OverScanHposition:S

    invoke-static {v5}, Ljava/lang/Short;->valueOf(S)Ljava/lang/Short;

    move-result-object v5

    invoke-virtual {v3, v4, v5}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Short;)V

    const-string v4, "OverScanVposition"

    iget-short v5, p1, Lcom/android/server/tv/DataBaseDesk$T_MS_OVERSCAN_SETTING_USER;->OverScanVposition:S

    invoke-static {v5}, Ljava/lang/Short;->valueOf(S)Ljava/lang/Short;

    move-result-object v5

    invoke-virtual {v3, v4, v5}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Short;)V

    const-string v4, "OverScanHRatio"

    iget-short v5, p1, Lcom/android/server/tv/DataBaseDesk$T_MS_OVERSCAN_SETTING_USER;->OverScanHRatio:S

    invoke-static {v5}, Ljava/lang/Short;->valueOf(S)Ljava/lang/Short;

    move-result-object v5

    invoke-virtual {v3, v4, v5}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Short;)V

    const-string v4, "OverScanVRatio"

    iget-short v5, p1, Lcom/android/server/tv/DataBaseDesk$T_MS_OVERSCAN_SETTING_USER;->OverScanVRatio:S

    invoke-static {v5}, Ljava/lang/Short;->valueOf(S)Ljava/lang/Short;

    move-result-object v5

    invoke-virtual {v3, v4, v5}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Short;)V

    :try_start_0
    invoke-virtual {p0}, Lcom/android/server/tv/DataBaseDeskImpl;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v4

    new-instance v5, Ljava/lang/StringBuilder;

    invoke-direct {v5}, Ljava/lang/StringBuilder;-><init>()V

    const-string v6, "content://mstar.tv.usersetting/useroverscanmode/inputsrc/"

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5, p2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    invoke-static {v5}, Landroid/net/Uri;->parse(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v5

    const/4 v6, 0x0

    const/4 v7, 0x0

    invoke-virtual {v4, v5, v3, v6, v7}, Landroid/content/ContentResolver;->update(Landroid/net/Uri;Landroid/content/ContentValues;Ljava/lang/String;[Ljava/lang/String;)I
    :try_end_0
    .catch Landroid/database/SQLException; {:try_start_0 .. :try_end_0} :catch_1

    move-result v4

    int-to-long v1, v4

    :goto_0
    const-wide/16 v4, -0x1

    cmp-long v4, v1, v4

    if-nez v4, :cond_0

    sget-object v4, Ljava/lang/System;->out:Ljava/io/PrintStream;

    const-string v5, "update tbl_UserOverScanMode ignored"

    invoke-virtual {v4, v5}, Ljava/io/PrintStream;->println(Ljava/lang/String;)V

    :cond_0
    :try_start_1
    invoke-static {}, Lcom/mstar/android/tvapi/common/TvManager;->getInstance()Lcom/mstar/android/tvapi/common/TvManager;

    move-result-object v4

    if-eqz v4, :cond_1

    invoke-static {}, Lcom/mstar/android/tvapi/common/TvManager;->getInstance()Lcom/mstar/android/tvapi/common/TvManager;

    move-result-object v4

    invoke-virtual {v4}, Lcom/mstar/android/tvapi/common/TvManager;->getDatabaseManager()Lcom/mstar/android/tvapi/common/DatabaseManager;

    move-result-object v4

    const/16 v5, 0x20

    invoke-virtual {v4, v5}, Lcom/mstar/android/tvapi/common/DatabaseManager;->setDatabaseDirtyByApplication(S)V
    :try_end_1
    .catch Lcom/mstar/android/tvapi/common/exception/TvCommonException; {:try_start_1 .. :try_end_1} :catch_0

    :cond_1
    :goto_1
    return-void

    :catch_0
    move-exception v0

    invoke-virtual {v0}, Lcom/mstar/android/tvapi/common/exception/TvCommonException;->printStackTrace()V

    goto :goto_1

    :catch_1
    move-exception v4

    goto :goto_0
.end method

.method public updateVolume(I)V
    .locals 5
    .param p1    # I

    new-instance v0, Landroid/content/ContentValues;

    invoke-direct {v0}, Landroid/content/ContentValues;-><init>()V

    const-string v1, "Volume"

    invoke-static {p1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Integer;)V

    :try_start_0
    invoke-virtual {p0}, Lcom/android/server/tv/DataBaseDeskImpl;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v1

    const-string v2, "content://mstar.tv.usersetting/soundsetting"

    invoke-static {v2}, Landroid/net/Uri;->parse(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v2

    const/4 v3, 0x0

    const/4 v4, 0x0

    invoke-virtual {v1, v2, v0, v3, v4}, Landroid/content/ContentResolver;->update(Landroid/net/Uri;Landroid/content/ContentValues;Ljava/lang/String;[Ljava/lang/String;)I
    :try_end_0
    .catch Landroid/database/SQLException; {:try_start_0 .. :try_end_0} :catch_0

    :goto_0
    return-void

    :catch_0
    move-exception v1

    goto :goto_0
.end method

.method public updateWallmusic(I)V
    .locals 5
    .param p1    # I

    new-instance v0, Landroid/content/ContentValues;

    invoke-direct {v0}, Landroid/content/ContentValues;-><init>()V

    const-string v1, "WallMusic"

    invoke-static {p1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Integer;)V

    :try_start_0
    invoke-virtual {p0}, Lcom/android/server/tv/DataBaseDeskImpl;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v1

    const-string v2, "content://mstar.tv.usersetting/soundsetting"

    invoke-static {v2}, Landroid/net/Uri;->parse(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v2

    const/4 v3, 0x0

    const/4 v4, 0x0

    invoke-virtual {v1, v2, v0, v3, v4}, Landroid/content/ContentResolver;->update(Landroid/net/Uri;Landroid/content/ContentValues;Ljava/lang/String;[Ljava/lang/String;)I
    :try_end_0
    .catch Landroid/database/SQLException; {:try_start_0 .. :try_end_0} :catch_0

    :goto_0
    return-void

    :catch_0
    move-exception v1

    goto :goto_0
.end method

.method public updateYPbPrOverscanAdjust(II[[Lcom/android/server/tv/DataBaseDesk$ST_MAPI_VIDEO_WINDOW_INFO;)V
    .locals 8
    .param p1    # I
    .param p2    # I
    .param p3    # [[Lcom/android/server/tv/DataBaseDesk$ST_MAPI_VIDEO_WINDOW_INFO;

    const-wide/16 v1, -0x1

    new-instance v3, Landroid/content/ContentValues;

    invoke-direct {v3}, Landroid/content/ContentValues;-><init>()V

    const-string v4, "u16H_CapStart"

    aget-object v5, p3, p1

    aget-object v5, v5, p2

    iget v5, v5, Lcom/android/server/tv/DataBaseDesk$ST_MAPI_VIDEO_WINDOW_INFO;->u16H_CapStart:I

    invoke-static {v5}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v5

    invoke-virtual {v3, v4, v5}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Integer;)V

    const-string v4, "u16V_CapStart"

    aget-object v5, p3, p1

    aget-object v5, v5, p2

    iget v5, v5, Lcom/android/server/tv/DataBaseDesk$ST_MAPI_VIDEO_WINDOW_INFO;->u16V_CapStart:I

    invoke-static {v5}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v5

    invoke-virtual {v3, v4, v5}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Integer;)V

    const-string v4, "u8HCrop_Left"

    aget-object v5, p3, p1

    aget-object v5, v5, p2

    iget-short v5, v5, Lcom/android/server/tv/DataBaseDesk$ST_MAPI_VIDEO_WINDOW_INFO;->u8HCrop_Left:S

    invoke-static {v5}, Ljava/lang/Short;->valueOf(S)Ljava/lang/Short;

    move-result-object v5

    invoke-virtual {v3, v4, v5}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Short;)V

    const-string v4, "u8HCrop_Right"

    aget-object v5, p3, p1

    aget-object v5, v5, p2

    iget-short v5, v5, Lcom/android/server/tv/DataBaseDesk$ST_MAPI_VIDEO_WINDOW_INFO;->u8HCrop_Right:S

    invoke-static {v5}, Ljava/lang/Short;->valueOf(S)Ljava/lang/Short;

    move-result-object v5

    invoke-virtual {v3, v4, v5}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Short;)V

    const-string v4, "u8VCrop_Up"

    aget-object v5, p3, p1

    aget-object v5, v5, p2

    iget-short v5, v5, Lcom/android/server/tv/DataBaseDesk$ST_MAPI_VIDEO_WINDOW_INFO;->u8VCrop_Up:S

    invoke-static {v5}, Ljava/lang/Short;->valueOf(S)Ljava/lang/Short;

    move-result-object v5

    invoke-virtual {v3, v4, v5}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Short;)V

    const-string v4, "u8VCrop_Down"

    aget-object v5, p3, p1

    aget-object v5, v5, p2

    iget-short v5, v5, Lcom/android/server/tv/DataBaseDesk$ST_MAPI_VIDEO_WINDOW_INFO;->u8VCrop_Down:S

    invoke-static {v5}, Ljava/lang/Short;->valueOf(S)Ljava/lang/Short;

    move-result-object v5

    invoke-virtual {v3, v4, v5}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Short;)V

    :try_start_0
    invoke-virtual {p0}, Lcom/android/server/tv/DataBaseDeskImpl;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v4

    new-instance v5, Ljava/lang/StringBuilder;

    invoke-direct {v5}, Ljava/lang/StringBuilder;-><init>()V

    const-string v6, "content://mstar.tv.factory/ypbproverscansetting/resolutiontypenum/"

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5, p1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v5

    const-string v6, "/_id/"

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5, p2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    invoke-static {v5}, Landroid/net/Uri;->parse(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v5

    const/4 v6, 0x0

    const/4 v7, 0x0

    invoke-virtual {v4, v5, v3, v6, v7}, Landroid/content/ContentResolver;->update(Landroid/net/Uri;Landroid/content/ContentValues;Ljava/lang/String;[Ljava/lang/String;)I
    :try_end_0
    .catch Landroid/database/SQLException; {:try_start_0 .. :try_end_0} :catch_1

    move-result v4

    int-to-long v1, v4

    :goto_0
    const-wide/16 v4, -0x1

    cmp-long v4, v1, v4

    if-nez v4, :cond_0

    sget-object v4, Ljava/lang/System;->out:Ljava/io/PrintStream;

    const-string v5, "update tbl_YPbPrOverscanSetting ignored"

    invoke-virtual {v4, v5}, Ljava/io/PrintStream;->println(Ljava/lang/String;)V

    :cond_0
    :try_start_1
    invoke-static {}, Lcom/mstar/android/tvapi/common/TvManager;->getInstance()Lcom/mstar/android/tvapi/common/TvManager;

    move-result-object v4

    if-eqz v4, :cond_1

    invoke-static {}, Lcom/mstar/android/tvapi/common/TvManager;->getInstance()Lcom/mstar/android/tvapi/common/TvManager;

    move-result-object v4

    invoke-virtual {v4}, Lcom/mstar/android/tvapi/common/TvManager;->getDatabaseManager()Lcom/mstar/android/tvapi/common/DatabaseManager;

    move-result-object v4

    const/16 v5, 0x2f

    invoke-virtual {v4, v5}, Lcom/mstar/android/tvapi/common/DatabaseManager;->setDatabaseDirtyByApplication(S)V
    :try_end_1
    .catch Lcom/mstar/android/tvapi/common/exception/TvCommonException; {:try_start_1 .. :try_end_1} :catch_0

    :cond_1
    :goto_1
    return-void

    :catch_0
    move-exception v0

    invoke-virtual {v0}, Lcom/mstar/android/tvapi/common/exception/TvCommonException;->printStackTrace()V

    goto :goto_1

    :catch_1
    move-exception v4

    goto :goto_0
.end method
