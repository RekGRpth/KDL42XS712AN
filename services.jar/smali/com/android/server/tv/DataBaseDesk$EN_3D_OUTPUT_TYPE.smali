.class public final enum Lcom/android/server/tv/DataBaseDesk$EN_3D_OUTPUT_TYPE;
.super Ljava/lang/Enum;
.source "DataBaseDesk.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/android/server/tv/DataBaseDesk;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x4019
    name = "EN_3D_OUTPUT_TYPE"
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Enum",
        "<",
        "Lcom/android/server/tv/DataBaseDesk$EN_3D_OUTPUT_TYPE;",
        ">;"
    }
.end annotation


# static fields
.field private static final synthetic $VALUES:[Lcom/android/server/tv/DataBaseDesk$EN_3D_OUTPUT_TYPE;

.field public static final enum E_3D_OUTPUT_FRAME_ALTERNATIVE:Lcom/android/server/tv/DataBaseDesk$EN_3D_OUTPUT_TYPE;

.field public static final enum E_3D_OUTPUT_FRAME_ALTERNATIVE_NOFRC:Lcom/android/server/tv/DataBaseDesk$EN_3D_OUTPUT_TYPE;

.field public static final enum E_3D_OUTPUT_FRAME_L:Lcom/android/server/tv/DataBaseDesk$EN_3D_OUTPUT_TYPE;

.field public static final enum E_3D_OUTPUT_FRAME_R:Lcom/android/server/tv/DataBaseDesk$EN_3D_OUTPUT_TYPE;

.field public static final enum E_3D_OUTPUT_LINE_ALTERNATIVE:Lcom/android/server/tv/DataBaseDesk$EN_3D_OUTPUT_TYPE;

.field public static final enum E_3D_OUTPUT_LINE_ALTERNATIVE_HW:Lcom/android/server/tv/DataBaseDesk$EN_3D_OUTPUT_TYPE;

.field public static final enum E_3D_OUTPUT_MODE_NONE:Lcom/android/server/tv/DataBaseDesk$EN_3D_OUTPUT_TYPE;

.field public static final enum E_3D_OUTPUT_SIDE_BY_SIDE_HALF:Lcom/android/server/tv/DataBaseDesk$EN_3D_OUTPUT_TYPE;

.field public static final enum E_3D_OUTPUT_TOP_BOTTOM:Lcom/android/server/tv/DataBaseDesk$EN_3D_OUTPUT_TYPE;

.field public static final enum E_3D_OUTPUT_TYPE_NUM:Lcom/android/server/tv/DataBaseDesk$EN_3D_OUTPUT_TYPE;


# direct methods
.method static constructor <clinit>()V
    .locals 8

    const/4 v7, 0x4

    const/4 v6, 0x3

    const/4 v5, 0x2

    const/4 v4, 0x1

    const/4 v3, 0x0

    new-instance v0, Lcom/android/server/tv/DataBaseDesk$EN_3D_OUTPUT_TYPE;

    const-string v1, "E_3D_OUTPUT_MODE_NONE"

    invoke-direct {v0, v1, v3}, Lcom/android/server/tv/DataBaseDesk$EN_3D_OUTPUT_TYPE;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/android/server/tv/DataBaseDesk$EN_3D_OUTPUT_TYPE;->E_3D_OUTPUT_MODE_NONE:Lcom/android/server/tv/DataBaseDesk$EN_3D_OUTPUT_TYPE;

    new-instance v0, Lcom/android/server/tv/DataBaseDesk$EN_3D_OUTPUT_TYPE;

    const-string v1, "E_3D_OUTPUT_LINE_ALTERNATIVE"

    invoke-direct {v0, v1, v4}, Lcom/android/server/tv/DataBaseDesk$EN_3D_OUTPUT_TYPE;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/android/server/tv/DataBaseDesk$EN_3D_OUTPUT_TYPE;->E_3D_OUTPUT_LINE_ALTERNATIVE:Lcom/android/server/tv/DataBaseDesk$EN_3D_OUTPUT_TYPE;

    new-instance v0, Lcom/android/server/tv/DataBaseDesk$EN_3D_OUTPUT_TYPE;

    const-string v1, "E_3D_OUTPUT_TOP_BOTTOM"

    invoke-direct {v0, v1, v5}, Lcom/android/server/tv/DataBaseDesk$EN_3D_OUTPUT_TYPE;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/android/server/tv/DataBaseDesk$EN_3D_OUTPUT_TYPE;->E_3D_OUTPUT_TOP_BOTTOM:Lcom/android/server/tv/DataBaseDesk$EN_3D_OUTPUT_TYPE;

    new-instance v0, Lcom/android/server/tv/DataBaseDesk$EN_3D_OUTPUT_TYPE;

    const-string v1, "E_3D_OUTPUT_SIDE_BY_SIDE_HALF"

    invoke-direct {v0, v1, v6}, Lcom/android/server/tv/DataBaseDesk$EN_3D_OUTPUT_TYPE;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/android/server/tv/DataBaseDesk$EN_3D_OUTPUT_TYPE;->E_3D_OUTPUT_SIDE_BY_SIDE_HALF:Lcom/android/server/tv/DataBaseDesk$EN_3D_OUTPUT_TYPE;

    new-instance v0, Lcom/android/server/tv/DataBaseDesk$EN_3D_OUTPUT_TYPE;

    const-string v1, "E_3D_OUTPUT_FRAME_ALTERNATIVE"

    invoke-direct {v0, v1, v7}, Lcom/android/server/tv/DataBaseDesk$EN_3D_OUTPUT_TYPE;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/android/server/tv/DataBaseDesk$EN_3D_OUTPUT_TYPE;->E_3D_OUTPUT_FRAME_ALTERNATIVE:Lcom/android/server/tv/DataBaseDesk$EN_3D_OUTPUT_TYPE;

    new-instance v0, Lcom/android/server/tv/DataBaseDesk$EN_3D_OUTPUT_TYPE;

    const-string v1, "E_3D_OUTPUT_FRAME_L"

    const/4 v2, 0x5

    invoke-direct {v0, v1, v2}, Lcom/android/server/tv/DataBaseDesk$EN_3D_OUTPUT_TYPE;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/android/server/tv/DataBaseDesk$EN_3D_OUTPUT_TYPE;->E_3D_OUTPUT_FRAME_L:Lcom/android/server/tv/DataBaseDesk$EN_3D_OUTPUT_TYPE;

    new-instance v0, Lcom/android/server/tv/DataBaseDesk$EN_3D_OUTPUT_TYPE;

    const-string v1, "E_3D_OUTPUT_FRAME_R"

    const/4 v2, 0x6

    invoke-direct {v0, v1, v2}, Lcom/android/server/tv/DataBaseDesk$EN_3D_OUTPUT_TYPE;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/android/server/tv/DataBaseDesk$EN_3D_OUTPUT_TYPE;->E_3D_OUTPUT_FRAME_R:Lcom/android/server/tv/DataBaseDesk$EN_3D_OUTPUT_TYPE;

    new-instance v0, Lcom/android/server/tv/DataBaseDesk$EN_3D_OUTPUT_TYPE;

    const-string v1, "E_3D_OUTPUT_FRAME_ALTERNATIVE_NOFRC"

    const/4 v2, 0x7

    invoke-direct {v0, v1, v2}, Lcom/android/server/tv/DataBaseDesk$EN_3D_OUTPUT_TYPE;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/android/server/tv/DataBaseDesk$EN_3D_OUTPUT_TYPE;->E_3D_OUTPUT_FRAME_ALTERNATIVE_NOFRC:Lcom/android/server/tv/DataBaseDesk$EN_3D_OUTPUT_TYPE;

    new-instance v0, Lcom/android/server/tv/DataBaseDesk$EN_3D_OUTPUT_TYPE;

    const-string v1, "E_3D_OUTPUT_LINE_ALTERNATIVE_HW"

    const/16 v2, 0x8

    invoke-direct {v0, v1, v2}, Lcom/android/server/tv/DataBaseDesk$EN_3D_OUTPUT_TYPE;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/android/server/tv/DataBaseDesk$EN_3D_OUTPUT_TYPE;->E_3D_OUTPUT_LINE_ALTERNATIVE_HW:Lcom/android/server/tv/DataBaseDesk$EN_3D_OUTPUT_TYPE;

    new-instance v0, Lcom/android/server/tv/DataBaseDesk$EN_3D_OUTPUT_TYPE;

    const-string v1, "E_3D_OUTPUT_TYPE_NUM"

    const/16 v2, 0x9

    invoke-direct {v0, v1, v2}, Lcom/android/server/tv/DataBaseDesk$EN_3D_OUTPUT_TYPE;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/android/server/tv/DataBaseDesk$EN_3D_OUTPUT_TYPE;->E_3D_OUTPUT_TYPE_NUM:Lcom/android/server/tv/DataBaseDesk$EN_3D_OUTPUT_TYPE;

    const/16 v0, 0xa

    new-array v0, v0, [Lcom/android/server/tv/DataBaseDesk$EN_3D_OUTPUT_TYPE;

    sget-object v1, Lcom/android/server/tv/DataBaseDesk$EN_3D_OUTPUT_TYPE;->E_3D_OUTPUT_MODE_NONE:Lcom/android/server/tv/DataBaseDesk$EN_3D_OUTPUT_TYPE;

    aput-object v1, v0, v3

    sget-object v1, Lcom/android/server/tv/DataBaseDesk$EN_3D_OUTPUT_TYPE;->E_3D_OUTPUT_LINE_ALTERNATIVE:Lcom/android/server/tv/DataBaseDesk$EN_3D_OUTPUT_TYPE;

    aput-object v1, v0, v4

    sget-object v1, Lcom/android/server/tv/DataBaseDesk$EN_3D_OUTPUT_TYPE;->E_3D_OUTPUT_TOP_BOTTOM:Lcom/android/server/tv/DataBaseDesk$EN_3D_OUTPUT_TYPE;

    aput-object v1, v0, v5

    sget-object v1, Lcom/android/server/tv/DataBaseDesk$EN_3D_OUTPUT_TYPE;->E_3D_OUTPUT_SIDE_BY_SIDE_HALF:Lcom/android/server/tv/DataBaseDesk$EN_3D_OUTPUT_TYPE;

    aput-object v1, v0, v6

    sget-object v1, Lcom/android/server/tv/DataBaseDesk$EN_3D_OUTPUT_TYPE;->E_3D_OUTPUT_FRAME_ALTERNATIVE:Lcom/android/server/tv/DataBaseDesk$EN_3D_OUTPUT_TYPE;

    aput-object v1, v0, v7

    const/4 v1, 0x5

    sget-object v2, Lcom/android/server/tv/DataBaseDesk$EN_3D_OUTPUT_TYPE;->E_3D_OUTPUT_FRAME_L:Lcom/android/server/tv/DataBaseDesk$EN_3D_OUTPUT_TYPE;

    aput-object v2, v0, v1

    const/4 v1, 0x6

    sget-object v2, Lcom/android/server/tv/DataBaseDesk$EN_3D_OUTPUT_TYPE;->E_3D_OUTPUT_FRAME_R:Lcom/android/server/tv/DataBaseDesk$EN_3D_OUTPUT_TYPE;

    aput-object v2, v0, v1

    const/4 v1, 0x7

    sget-object v2, Lcom/android/server/tv/DataBaseDesk$EN_3D_OUTPUT_TYPE;->E_3D_OUTPUT_FRAME_ALTERNATIVE_NOFRC:Lcom/android/server/tv/DataBaseDesk$EN_3D_OUTPUT_TYPE;

    aput-object v2, v0, v1

    const/16 v1, 0x8

    sget-object v2, Lcom/android/server/tv/DataBaseDesk$EN_3D_OUTPUT_TYPE;->E_3D_OUTPUT_LINE_ALTERNATIVE_HW:Lcom/android/server/tv/DataBaseDesk$EN_3D_OUTPUT_TYPE;

    aput-object v2, v0, v1

    const/16 v1, 0x9

    sget-object v2, Lcom/android/server/tv/DataBaseDesk$EN_3D_OUTPUT_TYPE;->E_3D_OUTPUT_TYPE_NUM:Lcom/android/server/tv/DataBaseDesk$EN_3D_OUTPUT_TYPE;

    aput-object v2, v0, v1

    sput-object v0, Lcom/android/server/tv/DataBaseDesk$EN_3D_OUTPUT_TYPE;->$VALUES:[Lcom/android/server/tv/DataBaseDesk$EN_3D_OUTPUT_TYPE;

    return-void
.end method

.method private constructor <init>(Ljava/lang/String;I)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()V"
        }
    .end annotation

    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    return-void
.end method

.method public static valueOf(Ljava/lang/String;)Lcom/android/server/tv/DataBaseDesk$EN_3D_OUTPUT_TYPE;
    .locals 1
    .param p0    # Ljava/lang/String;

    const-class v0, Lcom/android/server/tv/DataBaseDesk$EN_3D_OUTPUT_TYPE;

    invoke-static {v0, p0}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;

    move-result-object v0

    check-cast v0, Lcom/android/server/tv/DataBaseDesk$EN_3D_OUTPUT_TYPE;

    return-object v0
.end method

.method public static values()[Lcom/android/server/tv/DataBaseDesk$EN_3D_OUTPUT_TYPE;
    .locals 1

    sget-object v0, Lcom/android/server/tv/DataBaseDesk$EN_3D_OUTPUT_TYPE;->$VALUES:[Lcom/android/server/tv/DataBaseDesk$EN_3D_OUTPUT_TYPE;

    invoke-virtual {v0}, [Lcom/android/server/tv/DataBaseDesk$EN_3D_OUTPUT_TYPE;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [Lcom/android/server/tv/DataBaseDesk$EN_3D_OUTPUT_TYPE;

    return-object v0
.end method
