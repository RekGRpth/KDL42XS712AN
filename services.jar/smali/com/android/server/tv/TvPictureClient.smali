.class public Lcom/android/server/tv/TvPictureClient;
.super Lcom/mstar/android/tv/ITvPicture$Stub;
.source "TvPictureClient.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/android/server/tv/TvPictureClient$1;,
        Lcom/android/server/tv/TvPictureClient$MyThread;
    }
.end annotation


# static fields
.field private static final PBLV1:I = 0x14

.field private static final PBLV2:I = 0x28

.field private static final PBLV3:I = 0x3c

.field private static final PBLV4:I = 0x50

.field private static final PBLV5:I = 0x64

.field private static final TAG:Ljava/lang/String; = "TvPictureClient"

.field private static final cmvalue1:I = 0x11

.field private static final cmvalue2:I = 0x30

.field private static final cmvalue3:I = 0x58

.field private static final cmvalue4:I = 0x70

.field private static final cmvalue5:I = 0x85

.field private static final cmvalue6:I = 0xa0

.field private static final cmvalue7:I = 0xf0

.field private static final cmvalue8:I = 0xff

.field private static mythread:Ljava/lang/Thread;

.field private static sleeptimeCnt:I


# instance fields
.field private bforceThreadSleep:Z

.field private databaseMgr:Lcom/android/server/tv/DataBaseDeskImpl;

.field private e_NR_TYPE:Lcom/mstar/android/tvapi/common/vo/NoiseReduction$EnumNoiseReduction;

.field private enableDBC:Z

.field private enableDCC:Z

.field private enableDLC:Z


# direct methods
.method static constructor <clinit>()V
    .locals 1

    const/4 v0, 0x0

    sput-object v0, Lcom/android/server/tv/TvPictureClient;->mythread:Ljava/lang/Thread;

    const/16 v0, 0x1f4

    sput v0, Lcom/android/server/tv/TvPictureClient;->sleeptimeCnt:I

    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Lcom/android/server/tv/TvHanlder;Lcom/android/server/tv/DataBaseDeskImpl;)V
    .locals 3
    .param p1    # Landroid/content/Context;
    .param p2    # Lcom/android/server/tv/TvHanlder;
    .param p3    # Lcom/android/server/tv/DataBaseDeskImpl;

    const/4 v2, 0x1

    const/4 v1, 0x0

    invoke-direct {p0}, Lcom/mstar/android/tv/ITvPicture$Stub;-><init>()V

    const/4 v0, 0x0

    iput-object v0, p0, Lcom/android/server/tv/TvPictureClient;->databaseMgr:Lcom/android/server/tv/DataBaseDeskImpl;

    iput-boolean v1, p0, Lcom/android/server/tv/TvPictureClient;->enableDBC:Z

    iput-boolean v1, p0, Lcom/android/server/tv/TvPictureClient;->enableDCC:Z

    iput-boolean v2, p0, Lcom/android/server/tv/TvPictureClient;->enableDLC:Z

    sget-object v0, Lcom/mstar/android/tvapi/common/vo/NoiseReduction$EnumNoiseReduction;->E_NR_OFF:Lcom/mstar/android/tvapi/common/vo/NoiseReduction$EnumNoiseReduction;

    iput-object v0, p0, Lcom/android/server/tv/TvPictureClient;->e_NR_TYPE:Lcom/mstar/android/tvapi/common/vo/NoiseReduction$EnumNoiseReduction;

    iput-boolean v2, p0, Lcom/android/server/tv/TvPictureClient;->bforceThreadSleep:Z

    sget-object v0, Lcom/android/server/tv/TvPictureClient;->mythread:Ljava/lang/Thread;

    if-nez v0, :cond_0

    new-instance v0, Lcom/android/server/tv/TvPictureClient$MyThread;

    invoke-direct {v0, p0}, Lcom/android/server/tv/TvPictureClient$MyThread;-><init>(Lcom/android/server/tv/TvPictureClient;)V

    sput-object v0, Lcom/android/server/tv/TvPictureClient;->mythread:Ljava/lang/Thread;

    sget-object v0, Lcom/android/server/tv/TvPictureClient;->mythread:Ljava/lang/Thread;

    invoke-virtual {v0}, Ljava/lang/Thread;->start()V

    :cond_0
    invoke-static {p1}, Lcom/android/server/tv/DataBaseDeskImpl;->getDataBaseMgrInstance(Landroid/content/Context;)Lcom/android/server/tv/DataBaseDeskImpl;

    move-result-object v0

    iput-object v0, p0, Lcom/android/server/tv/TvPictureClient;->databaseMgr:Lcom/android/server/tv/DataBaseDeskImpl;

    return-void
.end method

.method private ReCntSt()V
    .locals 2

    const-string v0, "TvPictureClient"

    const-string v1, "ReCntSt"

    invoke-static {v0, v1}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    iget-boolean v0, p0, Lcom/android/server/tv/TvPictureClient;->enableDBC:Z

    if-nez v0, :cond_0

    iget-boolean v0, p0, Lcom/android/server/tv/TvPictureClient;->enableDCC:Z

    if-nez v0, :cond_0

    const/16 v0, 0x7d0

    sput v0, Lcom/android/server/tv/TvPictureClient;->sleeptimeCnt:I

    :goto_0
    return-void

    :cond_0
    iget-boolean v0, p0, Lcom/android/server/tv/TvPictureClient;->enableDBC:Z

    if-eqz v0, :cond_1

    iget-boolean v0, p0, Lcom/android/server/tv/TvPictureClient;->enableDCC:Z

    if-eqz v0, :cond_1

    const/16 v0, 0x1f4

    sput v0, Lcom/android/server/tv/TvPictureClient;->sleeptimeCnt:I

    goto :goto_0

    :cond_1
    const/16 v0, 0x3e8

    sput v0, Lcom/android/server/tv/TvPictureClient;->sleeptimeCnt:I

    goto :goto_0
.end method

.method static synthetic access$000(Lcom/android/server/tv/TvPictureClient;)Z
    .locals 1
    .param p0    # Lcom/android/server/tv/TvPictureClient;

    iget-boolean v0, p0, Lcom/android/server/tv/TvPictureClient;->bforceThreadSleep:Z

    return v0
.end method

.method static synthetic access$100(Lcom/android/server/tv/TvPictureClient;)Z
    .locals 1
    .param p0    # Lcom/android/server/tv/TvPictureClient;

    iget-boolean v0, p0, Lcom/android/server/tv/TvPictureClient;->enableDBC:Z

    return v0
.end method

.method static synthetic access$200(Lcom/android/server/tv/TvPictureClient;)V
    .locals 0
    .param p0    # Lcom/android/server/tv/TvPictureClient;

    invoke-direct {p0}, Lcom/android/server/tv/TvPictureClient;->dbchandler()V

    return-void
.end method

.method static synthetic access$300(Lcom/android/server/tv/TvPictureClient;)Z
    .locals 1
    .param p0    # Lcom/android/server/tv/TvPictureClient;

    iget-boolean v0, p0, Lcom/android/server/tv/TvPictureClient;->enableDCC:Z

    return v0
.end method

.method static synthetic access$400(Lcom/android/server/tv/TvPictureClient;)V
    .locals 0
    .param p0    # Lcom/android/server/tv/TvPictureClient;

    invoke-direct {p0}, Lcom/android/server/tv/TvPictureClient;->dcchandler()V

    return-void
.end method

.method static synthetic access$500()I
    .locals 1

    sget v0, Lcom/android/server/tv/TvPictureClient;->sleeptimeCnt:I

    return v0
.end method

.method private dbchandler()V
    .locals 5

    const-string v2, "TvPictureClient"

    const-string v3, "dbchandler"

    invoke-static {v2, v3}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    invoke-direct {p0}, Lcom/android/server/tv/TvPictureClient;->mapi_GetImageBackLight()I

    move-result v1

    :try_start_0
    invoke-static {}, Lcom/mstar/android/tvapi/common/TvManager;->getInstance()Lcom/mstar/android/tvapi/common/TvManager;

    move-result-object v2

    if-eqz v2, :cond_0

    invoke-static {}, Lcom/mstar/android/tvapi/common/TvManager;->getInstance()Lcom/mstar/android/tvapi/common/TvManager;

    move-result-object v2

    invoke-virtual {v2}, Lcom/mstar/android/tvapi/common/TvManager;->getPictureManager()Lcom/mstar/android/tvapi/common/PictureManager;

    move-result-object v2

    invoke-virtual {v2, v1}, Lcom/mstar/android/tvapi/common/PictureManager;->setBacklight(I)V
    :try_end_0
    .catch Lcom/mstar/android/tvapi/common/exception/TvCommonException; {:try_start_0 .. :try_end_0} :catch_0

    :cond_0
    :goto_0
    const-string v2, "TvApp"

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string v4, "dbchandler:"

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v2, v3}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    return-void

    :catch_0
    move-exception v0

    invoke-virtual {v0}, Lcom/mstar/android/tvapi/common/exception/TvCommonException;->printStackTrace()V

    goto :goto_0
.end method

.method private dcchandler()V
    .locals 5

    const-string v2, "TvPictureClient"

    const-string v3, "dcchandler"

    invoke-static {v2, v3}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    invoke-direct {p0}, Lcom/android/server/tv/TvPictureClient;->mapi_GetImageBackLight()I

    move-result v1

    :try_start_0
    invoke-static {}, Lcom/mstar/android/tvapi/common/TvManager;->getInstance()Lcom/mstar/android/tvapi/common/TvManager;

    move-result-object v2

    if-eqz v2, :cond_0

    invoke-static {}, Lcom/mstar/android/tvapi/common/TvManager;->getInstance()Lcom/mstar/android/tvapi/common/TvManager;

    move-result-object v2

    invoke-virtual {v2}, Lcom/mstar/android/tvapi/common/TvManager;->getPictureManager()Lcom/mstar/android/tvapi/common/PictureManager;

    move-result-object v2

    int-to-short v3, v1

    invoke-virtual {v2, v3}, Lcom/mstar/android/tvapi/common/PictureManager;->setPictureModeContrast(S)V
    :try_end_0
    .catch Lcom/mstar/android/tvapi/common/exception/TvCommonException; {:try_start_0 .. :try_end_0} :catch_0

    :cond_0
    :goto_0
    const-string v2, "TvApp"

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string v4, "dcchandler:"

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v2, v3}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    return-void

    :catch_0
    move-exception v0

    invoke-virtual {v0}, Lcom/mstar/android/tvapi/common/exception/TvCommonException;->printStackTrace()V

    goto :goto_0
.end method

.method private getCurrentInputsource()I
    .locals 6

    const/4 v2, 0x0

    :try_start_0
    invoke-static {}, Lcom/mstar/android/tvapi/common/TvManager;->getInstance()Lcom/mstar/android/tvapi/common/TvManager;

    move-result-object v3

    if-eqz v3, :cond_0

    invoke-static {}, Lcom/mstar/android/tvapi/common/TvManager;->getInstance()Lcom/mstar/android/tvapi/common/TvManager;

    move-result-object v3

    invoke-virtual {v3}, Lcom/mstar/android/tvapi/common/TvManager;->getCurrentInputSource()Lcom/mstar/android/tvapi/common/vo/TvOsType$EnumInputSource;
    :try_end_0
    .catch Lcom/mstar/android/tvapi/common/exception/TvCommonException; {:try_start_0 .. :try_end_0} :catch_0

    move-result-object v2

    :cond_0
    :goto_0
    invoke-virtual {v2}, Lcom/mstar/android/tvapi/common/vo/TvOsType$EnumInputSource;->ordinal()I

    move-result v1

    const-string v3, "TvPictureClient"

    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    const-string v5, "getCurrentInputsource, return int "

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-static {v3, v4}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    return v1

    :catch_0
    move-exception v0

    invoke-virtual {v0}, Lcom/mstar/android/tvapi/common/exception/TvCommonException;->printStackTrace()V

    goto :goto_0
.end method

.method private getrandom()I
    .locals 5

    invoke-static {}, Ljava/lang/Math;->random()D

    move-result-wide v1

    const-wide v3, 0x4093480000000000L    # 1234.0

    mul-double/2addr v1, v3

    double-to-int v0, v1

    const-string v1, "TvPictureClient"

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "getrandom, return int "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    return v0
.end method

.method private mapi_GetImageBackLight()I
    .locals 8

    const/4 v3, 0x0

    :try_start_0
    invoke-static {}, Lcom/mstar/android/tvapi/common/TvManager;->getInstance()Lcom/mstar/android/tvapi/common/TvManager;

    move-result-object v0

    if-eqz v0, :cond_0

    invoke-static {}, Lcom/mstar/android/tvapi/common/TvManager;->getInstance()Lcom/mstar/android/tvapi/common/TvManager;

    move-result-object v0

    invoke-virtual {v0}, Lcom/mstar/android/tvapi/common/TvManager;->getPictureManager()Lcom/mstar/android/tvapi/common/PictureManager;

    move-result-object v0

    invoke-virtual {v0}, Lcom/mstar/android/tvapi/common/PictureManager;->getDlcAverageLuma()S

    move-result v3

    :cond_0
    const-string v0, "TvApp"

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "getDlcAverageLuma:"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, v3}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I
    :try_end_0
    .catch Lcom/mstar/android/tvapi/common/exception/TvCommonException; {:try_start_0 .. :try_end_0} :catch_0

    :goto_0
    const/16 v0, 0x11

    if-ge v3, v0, :cond_1

    const/16 v1, 0x28

    const/16 v2, 0x14

    const/16 v4, 0x11

    const/4 v5, 0x0

    move-object v0, p0

    invoke-direct/range {v0 .. v5}, Lcom/android/server/tv/TvPictureClient;->mapi_GetRealValue(IIIII)I

    move-result v0

    rsub-int/lit8 v7, v0, 0x14

    :goto_1
    const-string v0, "TvPictureClient"

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "mapi_GetImageBackLight, return int "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, v7}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    return v7

    :catch_0
    move-exception v6

    invoke-virtual {v6}, Lcom/mstar/android/tvapi/common/exception/TvCommonException;->printStackTrace()V

    goto :goto_0

    :cond_1
    const/16 v0, 0x30

    if-ge v3, v0, :cond_2

    const/16 v1, 0x3c

    const/16 v2, 0x28

    const/16 v4, 0x30

    const/16 v5, 0x11

    move-object v0, p0

    invoke-direct/range {v0 .. v5}, Lcom/android/server/tv/TvPictureClient;->mapi_GetRealValue(IIIII)I

    move-result v0

    rsub-int/lit8 v7, v0, 0x28

    goto :goto_1

    :cond_2
    const/16 v0, 0x58

    if-ge v3, v0, :cond_3

    const/16 v1, 0x50

    const/16 v2, 0x3c

    const/16 v4, 0x58

    const/16 v5, 0x30

    move-object v0, p0

    invoke-direct/range {v0 .. v5}, Lcom/android/server/tv/TvPictureClient;->mapi_GetRealValue(IIIII)I

    move-result v0

    rsub-int/lit8 v7, v0, 0x3c

    goto :goto_1

    :cond_3
    const/16 v0, 0x70

    if-ge v3, v0, :cond_4

    const/16 v1, 0x64

    const/16 v2, 0x50

    const/16 v4, 0x70

    const/16 v5, 0x58

    move-object v0, p0

    invoke-direct/range {v0 .. v5}, Lcom/android/server/tv/TvPictureClient;->mapi_GetRealValue(IIIII)I

    move-result v0

    rsub-int/lit8 v7, v0, 0x50

    goto :goto_1

    :cond_4
    const/16 v0, 0x85

    if-ge v3, v0, :cond_5

    const/16 v1, 0x64

    const/16 v2, 0x50

    const/16 v4, 0x85

    const/16 v5, 0x70

    move-object v0, p0

    invoke-direct/range {v0 .. v5}, Lcom/android/server/tv/TvPictureClient;->mapi_GetRealValue(IIIII)I

    move-result v0

    add-int/lit8 v7, v0, 0x64

    goto :goto_1

    :cond_5
    const/16 v0, 0xa0

    if-ge v3, v0, :cond_6

    const/16 v1, 0x50

    const/16 v2, 0x3c

    const/16 v4, 0xa0

    const/16 v5, 0xa0

    move-object v0, p0

    invoke-direct/range {v0 .. v5}, Lcom/android/server/tv/TvPictureClient;->mapi_GetRealValue(IIIII)I

    move-result v0

    add-int/lit8 v7, v0, 0x50

    goto/16 :goto_1

    :cond_6
    const/16 v0, 0xf0

    if-ge v3, v0, :cond_7

    const/16 v1, 0x3c

    const/16 v2, 0x28

    const/16 v4, 0xf0

    const/16 v5, 0xa0

    move-object v0, p0

    invoke-direct/range {v0 .. v5}, Lcom/android/server/tv/TvPictureClient;->mapi_GetRealValue(IIIII)I

    move-result v0

    add-int/lit8 v7, v0, 0x3c

    goto/16 :goto_1

    :cond_7
    const/16 v1, 0x28

    const/16 v2, 0x14

    const/16 v4, 0xff

    const/16 v5, 0xf0

    move-object v0, p0

    invoke-direct/range {v0 .. v5}, Lcom/android/server/tv/TvPictureClient;->mapi_GetRealValue(IIIII)I

    move-result v0

    add-int/lit8 v7, v0, 0x28

    goto/16 :goto_1
.end method

.method private mapi_GetRealValue(IIIII)I
    .locals 4
    .param p1    # I
    .param p2    # I
    .param p3    # I
    .param p4    # I
    .param p5    # I

    const-string v1, "TvPictureClient"

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "mapi_GetRealValue, paras MaxLVal is "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, p1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, " MinLVal is "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, p2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, " ADVal is "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, p3}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, " MaxDVal is "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, p4}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, " MinDVal is "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, p5}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    sub-int v1, p3, p5

    sub-int v2, p1, p2

    mul-int/2addr v1, v2

    sub-int v2, p4, p5

    div-int v0, v1, v2

    return v0
.end method


# virtual methods
.method public disableBacklight()V
    .locals 3
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Landroid/os/RemoteException;
        }
    .end annotation

    const-string v1, "TvPictureClient"

    const-string v2, "disableBacklight"

    invoke-static {v1, v2}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    :try_start_0
    invoke-static {}, Lcom/mstar/android/tvapi/common/TvManager;->getInstance()Lcom/mstar/android/tvapi/common/TvManager;

    move-result-object v1

    if-eqz v1, :cond_0

    invoke-static {}, Lcom/mstar/android/tvapi/common/TvManager;->getInstance()Lcom/mstar/android/tvapi/common/TvManager;

    move-result-object v1

    invoke-virtual {v1}, Lcom/mstar/android/tvapi/common/TvManager;->getPictureManager()Lcom/mstar/android/tvapi/common/PictureManager;

    move-result-object v1

    invoke-virtual {v1}, Lcom/mstar/android/tvapi/common/PictureManager;->disableBacklight()V
    :try_end_0
    .catch Lcom/mstar/android/tvapi/common/exception/TvCommonException; {:try_start_0 .. :try_end_0} :catch_0

    :cond_0
    :goto_0
    return-void

    :catch_0
    move-exception v0

    invoke-virtual {v0}, Lcom/mstar/android/tvapi/common/exception/TvCommonException;->printStackTrace()V

    goto :goto_0
.end method

.method public disableDbc()Z
    .locals 3
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Landroid/os/RemoteException;
        }
    .end annotation

    const/4 v2, 0x0

    const-string v0, "TvPictureClient"

    const-string v1, "disableDbc"

    invoke-static {v0, v1}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    iput-boolean v2, p0, Lcom/android/server/tv/TvPictureClient;->enableDBC:Z

    iput-boolean v2, p0, Lcom/android/server/tv/TvPictureClient;->bforceThreadSleep:Z

    invoke-direct {p0}, Lcom/android/server/tv/TvPictureClient;->ReCntSt()V

    return v2
.end method

.method public disableDcc()Z
    .locals 3
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Landroid/os/RemoteException;
        }
    .end annotation

    const/4 v2, 0x0

    const-string v0, "TvPictureClient"

    const-string v1, "disableDcc"

    invoke-static {v0, v1}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    iput-boolean v2, p0, Lcom/android/server/tv/TvPictureClient;->enableDCC:Z

    iput-boolean v2, p0, Lcom/android/server/tv/TvPictureClient;->bforceThreadSleep:Z

    invoke-direct {p0}, Lcom/android/server/tv/TvPictureClient;->ReCntSt()V

    const/4 v0, 0x1

    return v0
.end method

.method public disableDlc()Z
    .locals 3
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Landroid/os/RemoteException;
        }
    .end annotation

    const-string v1, "TvPictureClient"

    const-string v2, "disableDlc"

    invoke-static {v1, v2}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    const/4 v1, 0x0

    iput-boolean v1, p0, Lcom/android/server/tv/TvPictureClient;->enableDLC:Z

    :try_start_0
    invoke-static {}, Lcom/mstar/android/tvapi/common/TvManager;->getInstance()Lcom/mstar/android/tvapi/common/TvManager;

    move-result-object v1

    if-eqz v1, :cond_0

    invoke-static {}, Lcom/mstar/android/tvapi/common/TvManager;->getInstance()Lcom/mstar/android/tvapi/common/TvManager;

    move-result-object v1

    invoke-virtual {v1}, Lcom/mstar/android/tvapi/common/TvManager;->getPictureManager()Lcom/mstar/android/tvapi/common/PictureManager;

    move-result-object v1

    invoke-virtual {v1}, Lcom/mstar/android/tvapi/common/PictureManager;->disableDlc()V
    :try_end_0
    .catch Lcom/mstar/android/tvapi/common/exception/TvCommonException; {:try_start_0 .. :try_end_0} :catch_0

    :cond_0
    :goto_0
    const/4 v1, 0x1

    return v1

    :catch_0
    move-exception v0

    invoke-virtual {v0}, Lcom/mstar/android/tvapi/common/exception/TvCommonException;->printStackTrace()V

    goto :goto_0
.end method

.method public enableBacklight()V
    .locals 3
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Landroid/os/RemoteException;
        }
    .end annotation

    const-string v1, "TvPictureClient"

    const-string v2, "enableBacklight"

    invoke-static {v1, v2}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    :try_start_0
    invoke-static {}, Lcom/mstar/android/tvapi/common/TvManager;->getInstance()Lcom/mstar/android/tvapi/common/TvManager;

    move-result-object v1

    if-eqz v1, :cond_0

    invoke-static {}, Lcom/mstar/android/tvapi/common/TvManager;->getInstance()Lcom/mstar/android/tvapi/common/TvManager;

    move-result-object v1

    invoke-virtual {v1}, Lcom/mstar/android/tvapi/common/TvManager;->getPictureManager()Lcom/mstar/android/tvapi/common/PictureManager;

    move-result-object v1

    invoke-virtual {v1}, Lcom/mstar/android/tvapi/common/PictureManager;->enableBacklight()V
    :try_end_0
    .catch Lcom/mstar/android/tvapi/common/exception/TvCommonException; {:try_start_0 .. :try_end_0} :catch_0

    :cond_0
    :goto_0
    return-void

    :catch_0
    move-exception v0

    invoke-virtual {v0}, Lcom/mstar/android/tvapi/common/exception/TvCommonException;->printStackTrace()V

    goto :goto_0
.end method

.method public enableDbc()Z
    .locals 3
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Landroid/os/RemoteException;
        }
    .end annotation

    const/4 v2, 0x0

    const-string v0, "TvPictureClient"

    const-string v1, "enableDbc"

    invoke-static {v0, v1}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/android/server/tv/TvPictureClient;->enableDBC:Z

    iput-boolean v2, p0, Lcom/android/server/tv/TvPictureClient;->bforceThreadSleep:Z

    invoke-direct {p0}, Lcom/android/server/tv/TvPictureClient;->ReCntSt()V

    return v2
.end method

.method public enableDcc()Z
    .locals 3
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Landroid/os/RemoteException;
        }
    .end annotation

    const/4 v2, 0x1

    const-string v0, "TvPictureClient"

    const-string v1, "enableDcc"

    invoke-static {v0, v1}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    iput-boolean v2, p0, Lcom/android/server/tv/TvPictureClient;->enableDCC:Z

    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/android/server/tv/TvPictureClient;->bforceThreadSleep:Z

    invoke-direct {p0}, Lcom/android/server/tv/TvPictureClient;->ReCntSt()V

    return v2
.end method

.method public enableDlc()Z
    .locals 4
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Landroid/os/RemoteException;
        }
    .end annotation

    const/4 v3, 0x1

    const-string v1, "TvPictureClient"

    const-string v2, "enableDlc"

    invoke-static {v1, v2}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    iput-boolean v3, p0, Lcom/android/server/tv/TvPictureClient;->enableDLC:Z

    :try_start_0
    invoke-static {}, Lcom/mstar/android/tvapi/common/TvManager;->getInstance()Lcom/mstar/android/tvapi/common/TvManager;

    move-result-object v1

    if-eqz v1, :cond_0

    invoke-static {}, Lcom/mstar/android/tvapi/common/TvManager;->getInstance()Lcom/mstar/android/tvapi/common/TvManager;

    move-result-object v1

    invoke-virtual {v1}, Lcom/mstar/android/tvapi/common/TvManager;->getPictureManager()Lcom/mstar/android/tvapi/common/PictureManager;

    move-result-object v1

    invoke-virtual {v1}, Lcom/mstar/android/tvapi/common/PictureManager;->enableDlc()V
    :try_end_0
    .catch Lcom/mstar/android/tvapi/common/exception/TvCommonException; {:try_start_0 .. :try_end_0} :catch_0

    :cond_0
    :goto_0
    return v3

    :catch_0
    move-exception v0

    invoke-virtual {v0}, Lcom/mstar/android/tvapi/common/exception/TvCommonException;->printStackTrace()V

    goto :goto_0
.end method

.method public execAutoPc()Z
    .locals 4
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Landroid/os/RemoteException;
        }
    .end annotation

    const-string v2, "TvPictureClient"

    const-string v3, "execAutoPc"

    invoke-static {v2, v3}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    const/4 v0, 0x0

    :try_start_0
    invoke-static {}, Lcom/mstar/android/tvapi/common/TvManager;->getInstance()Lcom/mstar/android/tvapi/common/TvManager;

    move-result-object v2

    if-eqz v2, :cond_0

    invoke-static {}, Lcom/mstar/android/tvapi/common/TvManager;->getInstance()Lcom/mstar/android/tvapi/common/TvManager;

    move-result-object v2

    invoke-virtual {v2}, Lcom/mstar/android/tvapi/common/TvManager;->getPlayerManager()Lcom/mstar/android/tvapi/common/TvPlayer;

    move-result-object v2

    invoke-interface {v2}, Lcom/mstar/android/tvapi/common/TvPlayer;->startPcModeAtuoTune()Z
    :try_end_0
    .catch Lcom/mstar/android/tvapi/common/exception/TvCommonException; {:try_start_0 .. :try_end_0} :catch_0

    move-result v0

    :cond_0
    :goto_0
    return v0

    :catch_0
    move-exception v1

    invoke-virtual {v1}, Lcom/mstar/android/tvapi/common/exception/TvCommonException;->printStackTrace()V

    goto :goto_0
.end method

.method public forceThreadSleep(Z)V
    .locals 3
    .param p1    # Z

    const-string v0, "TvPictureClient"

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "forceThreadSleep, paras flag is "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    iput-boolean p1, p0, Lcom/android/server/tv/TvPictureClient;->bforceThreadSleep:Z

    return-void
.end method

.method public freezeImage()Z
    .locals 3
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Landroid/os/RemoteException;
        }
    .end annotation

    const-string v1, "TvPictureClient"

    const-string v2, "freezeImage"

    invoke-static {v1, v2}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    :try_start_0
    invoke-static {}, Lcom/mstar/android/tvapi/common/TvManager;->getInstance()Lcom/mstar/android/tvapi/common/TvManager;

    move-result-object v1

    if-eqz v1, :cond_0

    invoke-static {}, Lcom/mstar/android/tvapi/common/TvManager;->getInstance()Lcom/mstar/android/tvapi/common/TvManager;

    move-result-object v1

    invoke-virtual {v1}, Lcom/mstar/android/tvapi/common/TvManager;->getPictureManager()Lcom/mstar/android/tvapi/common/PictureManager;

    move-result-object v1

    invoke-virtual {v1}, Lcom/mstar/android/tvapi/common/PictureManager;->freezeImage()Z
    :try_end_0
    .catch Lcom/mstar/android/tvapi/common/exception/TvCommonException; {:try_start_0 .. :try_end_0} :catch_0

    move-result v1

    :goto_0
    return v1

    :catch_0
    move-exception v0

    invoke-virtual {v0}, Lcom/mstar/android/tvapi/common/exception/TvCommonException;->printStackTrace()V

    :cond_0
    const/4 v1, 0x0

    goto :goto_0
.end method

.method public getBacklight()I
    .locals 6
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Landroid/os/RemoteException;
        }
    .end annotation

    invoke-direct {p0}, Lcom/android/server/tv/TvPictureClient;->getCurrentInputsource()I

    move-result v1

    iget-object v3, p0, Lcom/android/server/tv/TvPictureClient;->databaseMgr:Lcom/android/server/tv/DataBaseDeskImpl;

    invoke-virtual {v3, v1}, Lcom/android/server/tv/DataBaseDeskImpl;->queryPictureMode(I)I

    move-result v2

    iget-object v3, p0, Lcom/android/server/tv/TvPictureClient;->databaseMgr:Lcom/android/server/tv/DataBaseDeskImpl;

    sget-object v4, Lcom/android/server/tv/DataBaseDesk$EN_MS_VIDEOITEM;->MS_VIDEOITEM_BACKLIGHT:Lcom/android/server/tv/DataBaseDesk$EN_MS_VIDEOITEM;

    invoke-virtual {v3, v4, v1, v2}, Lcom/android/server/tv/DataBaseDeskImpl;->queryPicModeSetting(Lcom/android/server/tv/DataBaseDesk$EN_MS_VIDEOITEM;II)I

    move-result v0

    const-string v3, "TvPictureClient"

    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    const-string v5, "getBacklight, return int "

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4, v0}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-static {v3, v4}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    return v0
.end method

.method public getColorRange()B
    .locals 2
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Landroid/os/RemoteException;
        }
    .end annotation

    const-string v0, "TvPictureClient"

    const-string v1, "getColorRange"

    invoke-static {v0, v1}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    const/4 v0, 0x0

    return v0
.end method

.method public getColorTempIdx()I
    .locals 6
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Landroid/os/RemoteException;
        }
    .end annotation

    invoke-direct {p0}, Lcom/android/server/tv/TvPictureClient;->getCurrentInputsource()I

    move-result v1

    iget-object v3, p0, Lcom/android/server/tv/TvPictureClient;->databaseMgr:Lcom/android/server/tv/DataBaseDeskImpl;

    invoke-virtual {v3, v1}, Lcom/android/server/tv/DataBaseDeskImpl;->queryPictureMode(I)I

    move-result v2

    iget-object v3, p0, Lcom/android/server/tv/TvPictureClient;->databaseMgr:Lcom/android/server/tv/DataBaseDeskImpl;

    invoke-virtual {v3, v1, v2}, Lcom/android/server/tv/DataBaseDeskImpl;->queryColorTempIdx(II)Lcom/android/server/tv/DataBaseDesk$EN_MS_COLOR_TEMP;

    move-result-object v3

    invoke-virtual {v3}, Lcom/android/server/tv/DataBaseDesk$EN_MS_COLOR_TEMP;->ordinal()I

    move-result v0

    const-string v3, "TvPictureClient"

    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    const-string v5, "getColorTempIdx, return int "

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4, v0}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-static {v3, v4}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    return v0
.end method

.method public getColorTempPara()Lcom/mstar/android/tvapi/common/vo/ColorTemperatureExData;
    .locals 5
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Landroid/os/RemoteException;
        }
    .end annotation

    invoke-virtual {p0}, Lcom/android/server/tv/TvPictureClient;->getColorTempIdx()I

    move-result v0

    iget-object v2, p0, Lcom/android/server/tv/TvPictureClient;->databaseMgr:Lcom/android/server/tv/DataBaseDeskImpl;

    invoke-virtual {v2, v0}, Lcom/android/server/tv/DataBaseDeskImpl;->queryUsrColorTmpExData(I)Lcom/mstar/android/tvapi/common/vo/ColorTemperatureExData;

    move-result-object v1

    const-string v2, "TvPictureClient"

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string v4, "getColorTempPara, return ColorTemperatureExData redGain = "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    iget v4, v1, Lcom/mstar/android/tvapi/common/vo/ColorTemperatureExData;->redGain:I

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v3

    const-string v4, ", blueGain = "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    iget v4, v1, Lcom/mstar/android/tvapi/common/vo/ColorTemperatureExData;->blueGain:I

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v3

    const-string v4, ", greenGain = "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    iget v4, v1, Lcom/mstar/android/tvapi/common/vo/ColorTemperatureExData;->greenGain:I

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v3

    const-string v4, ", redOffset = "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    iget v4, v1, Lcom/mstar/android/tvapi/common/vo/ColorTemperatureExData;->redOffset:I

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v3

    const-string v4, ", blueOffse = "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    iget v4, v1, Lcom/mstar/android/tvapi/common/vo/ColorTemperatureExData;->blueOffset:I

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v3

    const-string v4, ", greenOffset = "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    iget v4, v1, Lcom/mstar/android/tvapi/common/vo/ColorTemperatureExData;->greenOffset:I

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v2, v3}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    return-object v1
.end method

.method public getFilmMode()I
    .locals 5
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Landroid/os/RemoteException;
        }
    .end annotation

    invoke-direct {p0}, Lcom/android/server/tv/TvPictureClient;->getCurrentInputsource()I

    move-result v1

    iget-object v2, p0, Lcom/android/server/tv/TvPictureClient;->databaseMgr:Lcom/android/server/tv/DataBaseDeskImpl;

    invoke-virtual {v2, v1}, Lcom/android/server/tv/DataBaseDeskImpl;->queryFilmMode(I)I

    move-result v0

    const-string v2, "TvPictureClient"

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string v4, "getFilmMode, return int "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3, v0}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v2, v3}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    return v0
.end method

.method public getNR()I
    .locals 5
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Landroid/os/RemoteException;
        }
    .end annotation

    invoke-virtual {p0}, Lcom/android/server/tv/TvPictureClient;->getColorTempIdx()I

    move-result v0

    iget-object v2, p0, Lcom/android/server/tv/TvPictureClient;->databaseMgr:Lcom/android/server/tv/DataBaseDeskImpl;

    invoke-direct {p0}, Lcom/android/server/tv/TvPictureClient;->getCurrentInputsource()I

    move-result v3

    invoke-virtual {v2, v3, v0}, Lcom/android/server/tv/DataBaseDeskImpl;->queryNR(II)Lcom/android/server/tv/DataBaseDesk$EN_MS_NR;

    move-result-object v2

    invoke-virtual {v2}, Lcom/android/server/tv/DataBaseDesk$EN_MS_NR;->ordinal()I

    move-result v1

    const-string v2, "TvPictureClient"

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string v4, "getNR, return int "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v2, v3}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    return v1
.end method

.method public getPCClock()I
    .locals 4
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Landroid/os/RemoteException;
        }
    .end annotation

    iget-object v1, p0, Lcom/android/server/tv/TvPictureClient;->databaseMgr:Lcom/android/server/tv/DataBaseDeskImpl;

    invoke-virtual {v1}, Lcom/android/server/tv/DataBaseDeskImpl;->queryPCClock()I

    move-result v1

    int-to-short v0, v1

    const-string v1, "TvPictureClient"

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "getPCClock, return int "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    return v0
.end method

.method public getPCHPos()I
    .locals 4
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Landroid/os/RemoteException;
        }
    .end annotation

    iget-object v1, p0, Lcom/android/server/tv/TvPictureClient;->databaseMgr:Lcom/android/server/tv/DataBaseDeskImpl;

    invoke-virtual {v1}, Lcom/android/server/tv/DataBaseDeskImpl;->queryPCHPos()I

    move-result v1

    int-to-short v0, v1

    const-string v1, "TvPictureClient"

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "getPCHPos, return int "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    return v0
.end method

.method public getPCPhase()I
    .locals 4
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Landroid/os/RemoteException;
        }
    .end annotation

    iget-object v1, p0, Lcom/android/server/tv/TvPictureClient;->databaseMgr:Lcom/android/server/tv/DataBaseDeskImpl;

    invoke-virtual {v1}, Lcom/android/server/tv/DataBaseDeskImpl;->queryPCPhase()I

    move-result v1

    int-to-short v0, v1

    const-string v1, "TvPictureClient"

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "getPCPhase, return int "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    return v0
.end method

.method public getPCVPos()I
    .locals 4
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Landroid/os/RemoteException;
        }
    .end annotation

    iget-object v1, p0, Lcom/android/server/tv/TvPictureClient;->databaseMgr:Lcom/android/server/tv/DataBaseDeskImpl;

    invoke-virtual {v1}, Lcom/android/server/tv/DataBaseDeskImpl;->queryPCVPos()I

    move-result v1

    int-to-short v0, v1

    const-string v1, "TvPictureClient"

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "getPCVPos, return int "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    return v0
.end method

.method public getPictureModeIdx()I
    .locals 6
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Landroid/os/RemoteException;
        }
    .end annotation

    const/4 v2, 0x0

    :try_start_0
    invoke-static {}, Lcom/mstar/android/tvapi/common/TvManager;->getInstance()Lcom/mstar/android/tvapi/common/TvManager;

    move-result-object v3

    if-eqz v3, :cond_0

    invoke-static {}, Lcom/mstar/android/tvapi/common/TvManager;->getInstance()Lcom/mstar/android/tvapi/common/TvManager;

    move-result-object v3

    invoke-virtual {v3}, Lcom/mstar/android/tvapi/common/TvManager;->getCurrentInputSource()Lcom/mstar/android/tvapi/common/vo/TvOsType$EnumInputSource;
    :try_end_0
    .catch Lcom/mstar/android/tvapi/common/exception/TvCommonException; {:try_start_0 .. :try_end_0} :catch_0

    move-result-object v2

    :cond_0
    :goto_0
    iget-object v3, p0, Lcom/android/server/tv/TvPictureClient;->databaseMgr:Lcom/android/server/tv/DataBaseDeskImpl;

    invoke-virtual {v2}, Lcom/mstar/android/tvapi/common/vo/TvOsType$EnumInputSource;->ordinal()I

    move-result v4

    invoke-virtual {v3, v4}, Lcom/android/server/tv/DataBaseDeskImpl;->queryPictureMode(I)I

    move-result v1

    const-string v3, "TvPictureClient"

    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    const-string v5, "getPictureModeIdx, return int "

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-static {v3, v4}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    return v1

    :catch_0
    move-exception v0

    invoke-virtual {v0}, Lcom/mstar/android/tvapi/common/exception/TvCommonException;->printStackTrace()V

    goto :goto_0
.end method

.method public getVideoArc()I
    .locals 4
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Landroid/os/RemoteException;
        }
    .end annotation

    iget-object v1, p0, Lcom/android/server/tv/TvPictureClient;->databaseMgr:Lcom/android/server/tv/DataBaseDeskImpl;

    invoke-direct {p0}, Lcom/android/server/tv/TvPictureClient;->getCurrentInputsource()I

    move-result v2

    invoke-virtual {v1, v2}, Lcom/android/server/tv/DataBaseDeskImpl;->queryArcMode(I)I

    move-result v0

    const-string v1, "TvPictureClient"

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "getVideoArc, return int "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    return v0
.end method

.method public getVideoInfo()Lcom/mstar/android/tvapi/common/vo/VideoInfo;
    .locals 5
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Landroid/os/RemoteException;
        }
    .end annotation

    const/4 v1, 0x0

    :try_start_0
    invoke-static {}, Lcom/mstar/android/tvapi/common/TvManager;->getInstance()Lcom/mstar/android/tvapi/common/TvManager;

    move-result-object v2

    if-eqz v2, :cond_0

    invoke-static {}, Lcom/mstar/android/tvapi/common/TvManager;->getInstance()Lcom/mstar/android/tvapi/common/TvManager;

    move-result-object v2

    invoke-virtual {v2}, Lcom/mstar/android/tvapi/common/TvManager;->getPlayerManager()Lcom/mstar/android/tvapi/common/TvPlayer;

    move-result-object v2

    invoke-interface {v2}, Lcom/mstar/android/tvapi/common/TvPlayer;->getVideoInfo()Lcom/mstar/android/tvapi/common/vo/VideoInfo;
    :try_end_0
    .catch Lcom/mstar/android/tvapi/common/exception/TvCommonException; {:try_start_0 .. :try_end_0} :catch_0

    move-result-object v1

    :cond_0
    :goto_0
    const-string v2, "TvPictureClient"

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string v4, "gevideoInfoideoInfo, return VideoInfo videoInfo.frameRate = "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    iget v4, v1, Lcom/mstar/android/tvapi/common/vo/VideoInfo;->frameRate:I

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v3

    const-string v4, ", videoInfo.hResolution = "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    iget v4, v1, Lcom/mstar/android/tvapi/common/vo/VideoInfo;->hResolution:I

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v3

    const-string v4, ", videoInfo.modeIndex = "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    iget v4, v1, Lcom/mstar/android/tvapi/common/vo/VideoInfo;->modeIndex:I

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v3

    const-string v4, ", videoInfo.vResolution = "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    iget v4, v1, Lcom/mstar/android/tvapi/common/vo/VideoInfo;->vResolution:I

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v2, v3}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    return-object v1

    :catch_0
    move-exception v0

    invoke-virtual {v0}, Lcom/mstar/android/tvapi/common/exception/TvCommonException;->printStackTrace()V

    goto :goto_0
.end method

.method public getVideoItem(I)I
    .locals 6
    .param p1    # I
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Landroid/os/RemoteException;
        }
    .end annotation

    const-string v3, "TvPictureClient"

    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    const-string v5, "getVideoItem, paras eIndex is "

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4, p1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-static {v3, v4}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    invoke-direct {p0}, Lcom/android/server/tv/TvPictureClient;->getCurrentInputsource()I

    move-result v1

    iget-object v3, p0, Lcom/android/server/tv/TvPictureClient;->databaseMgr:Lcom/android/server/tv/DataBaseDeskImpl;

    invoke-virtual {v3, v1}, Lcom/android/server/tv/DataBaseDeskImpl;->queryPictureMode(I)I

    move-result v2

    iget-object v3, p0, Lcom/android/server/tv/TvPictureClient;->databaseMgr:Lcom/android/server/tv/DataBaseDeskImpl;

    invoke-static {}, Lcom/android/server/tv/DataBaseDesk$EN_MS_VIDEOITEM;->values()[Lcom/android/server/tv/DataBaseDesk$EN_MS_VIDEOITEM;

    move-result-object v4

    aget-object v4, v4, p1

    invoke-virtual {v3, v4, v1, v2}, Lcom/android/server/tv/DataBaseDeskImpl;->queryPicModeSetting(Lcom/android/server/tv/DataBaseDesk$EN_MS_VIDEOITEM;II)I

    move-result v0

    const-string v3, "TvPictureClient"

    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    const-string v5, "getVideoItem, return int "

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4, v0}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-static {v3, v4}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    return v0
.end method

.method public getVideoItemByInputSource(II)I
    .locals 5
    .param p1    # I
    .param p2    # I
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Landroid/os/RemoteException;
        }
    .end annotation

    const-string v2, "TvPictureClient"

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string v4, "getVideoItem, paras input is "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3, p2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v3

    const-string v4, " eIndex is "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3, p1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v2, v3}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    iget-object v2, p0, Lcom/android/server/tv/TvPictureClient;->databaseMgr:Lcom/android/server/tv/DataBaseDeskImpl;

    invoke-virtual {v2, p2}, Lcom/android/server/tv/DataBaseDeskImpl;->queryPictureMode(I)I

    move-result v1

    iget-object v2, p0, Lcom/android/server/tv/TvPictureClient;->databaseMgr:Lcom/android/server/tv/DataBaseDeskImpl;

    invoke-static {}, Lcom/android/server/tv/DataBaseDesk$EN_MS_VIDEOITEM;->values()[Lcom/android/server/tv/DataBaseDesk$EN_MS_VIDEOITEM;

    move-result-object v3

    aget-object v3, v3, p1

    invoke-virtual {v2, v3, p2, v1}, Lcom/android/server/tv/DataBaseDeskImpl;->queryPicModeSetting(Lcom/android/server/tv/DataBaseDesk$EN_MS_VIDEOITEM;II)I

    move-result v0

    return v0
.end method

.method public isDbcEnabled()Z
    .locals 2
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Landroid/os/RemoteException;
        }
    .end annotation

    const-string v0, "TvPictureClient"

    const-string v1, "isDbcEnabled"

    invoke-static {v0, v1}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    iget-boolean v0, p0, Lcom/android/server/tv/TvPictureClient;->enableDBC:Z

    return v0
.end method

.method public isDccEnabled()Z
    .locals 2
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Landroid/os/RemoteException;
        }
    .end annotation

    const-string v0, "TvPictureClient"

    const-string v1, "isDccEnabled"

    invoke-static {v0, v1}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    iget-boolean v0, p0, Lcom/android/server/tv/TvPictureClient;->enableDCC:Z

    return v0
.end method

.method public isDlcEnabled()Z
    .locals 2
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Landroid/os/RemoteException;
        }
    .end annotation

    const-string v0, "TvPictureClient"

    const-string v1, "isDlcEnabled"

    invoke-static {v0, v1}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    iget-boolean v0, p0, Lcom/android/server/tv/TvPictureClient;->enableDLC:Z

    return v0
.end method

.method public setBacklight(I)Z
    .locals 6
    .param p1    # I
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Landroid/os/RemoteException;
        }
    .end annotation

    const-string v3, "TvPictureClient"

    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    const-string v5, "setBacklight, paras value is "

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4, p1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-static {v3, v4}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    invoke-direct {p0}, Lcom/android/server/tv/TvPictureClient;->getCurrentInputsource()I

    move-result v1

    iget-object v3, p0, Lcom/android/server/tv/TvPictureClient;->databaseMgr:Lcom/android/server/tv/DataBaseDeskImpl;

    invoke-virtual {v3, v1}, Lcom/android/server/tv/DataBaseDeskImpl;->queryPictureMode(I)I

    move-result v2

    iget-object v3, p0, Lcom/android/server/tv/TvPictureClient;->databaseMgr:Lcom/android/server/tv/DataBaseDeskImpl;

    sget-object v4, Lcom/android/server/tv/DataBaseDesk$EN_MS_VIDEOITEM;->MS_VIDEOITEM_BACKLIGHT:Lcom/android/server/tv/DataBaseDesk$EN_MS_VIDEOITEM;

    invoke-virtual {v3, v4, v1, v2, p1}, Lcom/android/server/tv/DataBaseDeskImpl;->updatePicModeSetting(Lcom/android/server/tv/DataBaseDesk$EN_MS_VIDEOITEM;III)V

    :try_start_0
    invoke-static {}, Lcom/mstar/android/tvapi/common/TvManager;->getInstance()Lcom/mstar/android/tvapi/common/TvManager;

    move-result-object v3

    if-eqz v3, :cond_0

    invoke-static {}, Lcom/mstar/android/tvapi/common/TvManager;->getInstance()Lcom/mstar/android/tvapi/common/TvManager;

    move-result-object v3

    invoke-virtual {v3}, Lcom/mstar/android/tvapi/common/TvManager;->getPictureManager()Lcom/mstar/android/tvapi/common/PictureManager;

    move-result-object v3

    invoke-virtual {v3, p1}, Lcom/mstar/android/tvapi/common/PictureManager;->setBacklight(I)V
    :try_end_0
    .catch Lcom/mstar/android/tvapi/common/exception/TvCommonException; {:try_start_0 .. :try_end_0} :catch_0

    :cond_0
    :goto_0
    const/4 v3, 0x1

    return v3

    :catch_0
    move-exception v0

    invoke-virtual {v0}, Lcom/mstar/android/tvapi/common/exception/TvCommonException;->printStackTrace()V

    goto :goto_0
.end method

.method public setColorRange(B)Z
    .locals 5
    .param p1    # B
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Landroid/os/RemoteException;
        }
    .end annotation

    const/4 v2, 0x1

    const-string v1, "TvPictureClient"

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string v4, "setColorRange, paras value is "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3, p1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v1, v3}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    :try_start_0
    invoke-static {}, Lcom/mstar/android/tvapi/common/TvManager;->getInstance()Lcom/mstar/android/tvapi/common/TvManager;

    move-result-object v1

    if-eqz v1, :cond_0

    invoke-static {}, Lcom/mstar/android/tvapi/common/TvManager;->getInstance()Lcom/mstar/android/tvapi/common/TvManager;

    move-result-object v1

    invoke-virtual {v1}, Lcom/mstar/android/tvapi/common/TvManager;->getPictureManager()Lcom/mstar/android/tvapi/common/PictureManager;

    move-result-object v3

    if-ne p1, v2, :cond_1

    move v1, v2

    :goto_0
    invoke-virtual {v3, v1}, Lcom/mstar/android/tvapi/common/PictureManager;->setColorRange(Z)V
    :try_end_0
    .catch Lcom/mstar/android/tvapi/common/exception/TvCommonException; {:try_start_0 .. :try_end_0} :catch_0

    :cond_0
    :goto_1
    return v2

    :cond_1
    const/4 v1, 0x0

    goto :goto_0

    :catch_0
    move-exception v0

    invoke-virtual {v0}, Lcom/mstar/android/tvapi/common/exception/TvCommonException;->printStackTrace()V

    goto :goto_1
.end method

.method public setColorTempIdx(I)Z
    .locals 14
    .param p1    # I
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Landroid/os/RemoteException;
        }
    .end annotation

    const-string v0, "TvPictureClient"

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "setColorTempIdx, paras eColorTemp is "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    const/4 v8, 0x0

    :try_start_0
    invoke-static {}, Lcom/mstar/android/tvapi/common/TvManager;->getInstance()Lcom/mstar/android/tvapi/common/TvManager;

    move-result-object v0

    if-eqz v0, :cond_0

    invoke-static {}, Lcom/mstar/android/tvapi/common/TvManager;->getInstance()Lcom/mstar/android/tvapi/common/TvManager;

    move-result-object v0

    invoke-virtual {v0}, Lcom/mstar/android/tvapi/common/TvManager;->getCurrentInputSource()Lcom/mstar/android/tvapi/common/vo/TvOsType$EnumInputSource;
    :try_end_0
    .catch Lcom/mstar/android/tvapi/common/exception/TvCommonException; {:try_start_0 .. :try_end_0} :catch_0

    move-result-object v8

    :cond_0
    :goto_0
    iget-object v0, p0, Lcom/android/server/tv/TvPictureClient;->databaseMgr:Lcom/android/server/tv/DataBaseDeskImpl;

    invoke-virtual {v8}, Lcom/mstar/android/tvapi/common/vo/TvOsType$EnumInputSource;->ordinal()I

    move-result v1

    invoke-virtual {v0, v1}, Lcom/android/server/tv/DataBaseDeskImpl;->queryPictureMode(I)I

    move-result v11

    iget-object v0, p0, Lcom/android/server/tv/TvPictureClient;->databaseMgr:Lcom/android/server/tv/DataBaseDeskImpl;

    invoke-virtual {v0}, Lcom/android/server/tv/DataBaseDeskImpl;->queryFactoryColorTempExData()Lcom/android/server/tv/DataBaseDesk$T_MS_COLOR_TEMPEX;

    move-result-object v9

    new-instance v13, Lcom/mstar/android/tvapi/common/vo/ColorTemperature;

    invoke-direct {v13}, Lcom/mstar/android/tvapi/common/vo/ColorTemperature;-><init>()V

    const/4 v12, 0x0

    sget-object v0, Lcom/android/server/tv/TvPictureClient$1;->$SwitchMap$com$mstar$android$tvapi$common$vo$TvOsType$EnumInputSource:[I

    invoke-virtual {v8}, Lcom/mstar/android/tvapi/common/vo/TvOsType$EnumInputSource;->ordinal()I

    move-result v1

    aget v0, v0, v1

    packed-switch v0, :pswitch_data_0

    :goto_1
    iget v0, v12, Lcom/android/server/tv/DataBaseDesk$T_MS_COLOR_TEMPEX_DATA;->redgain:I

    int-to-short v0, v0

    iput-short v0, v13, Lcom/mstar/android/tvapi/common/vo/ColorTemperature;->redGain:S

    iget v0, v12, Lcom/android/server/tv/DataBaseDesk$T_MS_COLOR_TEMPEX_DATA;->greengain:I

    int-to-short v0, v0

    iput-short v0, v13, Lcom/mstar/android/tvapi/common/vo/ColorTemperature;->greenGain:S

    iget v0, v12, Lcom/android/server/tv/DataBaseDesk$T_MS_COLOR_TEMPEX_DATA;->bluegain:I

    int-to-short v0, v0

    iput-short v0, v13, Lcom/mstar/android/tvapi/common/vo/ColorTemperature;->buleGain:S

    iget v0, v12, Lcom/android/server/tv/DataBaseDesk$T_MS_COLOR_TEMPEX_DATA;->redoffset:I

    int-to-short v0, v0

    iput-short v0, v13, Lcom/mstar/android/tvapi/common/vo/ColorTemperature;->redOffset:S

    iget v0, v12, Lcom/android/server/tv/DataBaseDesk$T_MS_COLOR_TEMPEX_DATA;->greenoffset:I

    int-to-short v0, v0

    iput-short v0, v13, Lcom/mstar/android/tvapi/common/vo/ColorTemperature;->greenOffset:S

    iget v0, v12, Lcom/android/server/tv/DataBaseDesk$T_MS_COLOR_TEMPEX_DATA;->blueoffset:I

    int-to-short v0, v0

    iput-short v0, v13, Lcom/mstar/android/tvapi/common/vo/ColorTemperature;->blueOffset:S

    :try_start_1
    invoke-static {}, Lcom/mstar/android/tvapi/common/TvManager;->getInstance()Lcom/mstar/android/tvapi/common/TvManager;

    move-result-object v0

    if-eqz v0, :cond_1

    invoke-static {}, Lcom/mstar/android/tvapi/common/TvManager;->getInstance()Lcom/mstar/android/tvapi/common/TvManager;

    move-result-object v0

    invoke-virtual {v0}, Lcom/mstar/android/tvapi/common/TvManager;->getFactoryManager()Lcom/mstar/android/tvapi/factory/FactoryManager;

    move-result-object v0

    invoke-static {}, Lcom/mstar/android/tvapi/common/vo/EnumColorTemperature;->values()[Lcom/mstar/android/tvapi/common/vo/EnumColorTemperature;

    move-result-object v1

    add-int/lit8 v2, p1, 0x1

    aget-object v1, v1, v2

    iget-short v2, v13, Lcom/mstar/android/tvapi/common/vo/ColorTemperature;->redGain:S

    iget-short v3, v13, Lcom/mstar/android/tvapi/common/vo/ColorTemperature;->greenGain:S

    iget-short v4, v13, Lcom/mstar/android/tvapi/common/vo/ColorTemperature;->buleGain:S

    iget-short v5, v13, Lcom/mstar/android/tvapi/common/vo/ColorTemperature;->redOffset:S

    iget-short v6, v13, Lcom/mstar/android/tvapi/common/vo/ColorTemperature;->greenOffset:S

    iget-short v7, v13, Lcom/mstar/android/tvapi/common/vo/ColorTemperature;->blueOffset:S

    invoke-virtual/range {v0 .. v8}, Lcom/mstar/android/tvapi/factory/FactoryManager;->setWbGainOffsetEx(Lcom/mstar/android/tvapi/common/vo/EnumColorTemperature;IIIIIILcom/mstar/android/tvapi/common/vo/TvOsType$EnumInputSource;)V
    :try_end_1
    .catch Lcom/mstar/android/tvapi/common/exception/TvCommonException; {:try_start_1 .. :try_end_1} :catch_1

    :cond_1
    :goto_2
    iget-object v0, p0, Lcom/android/server/tv/TvPictureClient;->databaseMgr:Lcom/android/server/tv/DataBaseDeskImpl;

    invoke-virtual {v8}, Lcom/mstar/android/tvapi/common/vo/TvOsType$EnumInputSource;->ordinal()I

    move-result v1

    invoke-static {}, Lcom/android/server/tv/DataBaseDesk$EN_MS_COLOR_TEMP;->values()[Lcom/android/server/tv/DataBaseDesk$EN_MS_COLOR_TEMP;

    move-result-object v2

    aget-object v2, v2, p1

    invoke-virtual {v0, v1, v11, v2}, Lcom/android/server/tv/DataBaseDeskImpl;->updateColorTempIdx(IILcom/android/server/tv/DataBaseDesk$EN_MS_COLOR_TEMP;)V

    const/4 v0, 0x1

    return v0

    :catch_0
    move-exception v10

    invoke-virtual {v10}, Lcom/mstar/android/tvapi/common/exception/TvCommonException;->printStackTrace()V

    goto :goto_0

    :pswitch_0
    iget-object v0, v9, Lcom/android/server/tv/DataBaseDesk$T_MS_COLOR_TEMPEX;->astColorTempEx:[[Lcom/android/server/tv/DataBaseDesk$T_MS_COLOR_TEMPEX_DATA;

    aget-object v0, v0, p1

    sget-object v1, Lcom/android/server/tv/DataBaseDesk$EN_MS_COLOR_TEMP_INPUT_SOURCE;->E_INPUT_SOURCE_VGA:Lcom/android/server/tv/DataBaseDesk$EN_MS_COLOR_TEMP_INPUT_SOURCE;

    invoke-virtual {v1}, Lcom/android/server/tv/DataBaseDesk$EN_MS_COLOR_TEMP_INPUT_SOURCE;->ordinal()I

    move-result v1

    aget-object v12, v0, v1

    goto :goto_1

    :pswitch_1
    iget-object v0, v9, Lcom/android/server/tv/DataBaseDesk$T_MS_COLOR_TEMPEX;->astColorTempEx:[[Lcom/android/server/tv/DataBaseDesk$T_MS_COLOR_TEMPEX_DATA;

    aget-object v0, v0, p1

    sget-object v1, Lcom/android/server/tv/DataBaseDesk$EN_MS_COLOR_TEMP_INPUT_SOURCE;->E_INPUT_SOURCE_ATV:Lcom/android/server/tv/DataBaseDesk$EN_MS_COLOR_TEMP_INPUT_SOURCE;

    invoke-virtual {v1}, Lcom/android/server/tv/DataBaseDesk$EN_MS_COLOR_TEMP_INPUT_SOURCE;->ordinal()I

    move-result v1

    aget-object v12, v0, v1

    goto :goto_1

    :pswitch_2
    iget-object v0, v9, Lcom/android/server/tv/DataBaseDesk$T_MS_COLOR_TEMPEX;->astColorTempEx:[[Lcom/android/server/tv/DataBaseDesk$T_MS_COLOR_TEMPEX_DATA;

    aget-object v0, v0, p1

    sget-object v1, Lcom/android/server/tv/DataBaseDesk$EN_MS_COLOR_TEMP_INPUT_SOURCE;->E_INPUT_SOURCE_CVBS:Lcom/android/server/tv/DataBaseDesk$EN_MS_COLOR_TEMP_INPUT_SOURCE;

    invoke-virtual {v1}, Lcom/android/server/tv/DataBaseDesk$EN_MS_COLOR_TEMP_INPUT_SOURCE;->ordinal()I

    move-result v1

    aget-object v12, v0, v1

    goto :goto_1

    :pswitch_3
    iget-object v0, v9, Lcom/android/server/tv/DataBaseDesk$T_MS_COLOR_TEMPEX;->astColorTempEx:[[Lcom/android/server/tv/DataBaseDesk$T_MS_COLOR_TEMPEX_DATA;

    aget-object v0, v0, p1

    sget-object v1, Lcom/android/server/tv/DataBaseDesk$EN_MS_COLOR_TEMP_INPUT_SOURCE;->E_INPUT_SOURCE_SVIDEO:Lcom/android/server/tv/DataBaseDesk$EN_MS_COLOR_TEMP_INPUT_SOURCE;

    invoke-virtual {v1}, Lcom/android/server/tv/DataBaseDesk$EN_MS_COLOR_TEMP_INPUT_SOURCE;->ordinal()I

    move-result v1

    aget-object v12, v0, v1

    goto/16 :goto_1

    :pswitch_4
    iget-object v0, v9, Lcom/android/server/tv/DataBaseDesk$T_MS_COLOR_TEMPEX;->astColorTempEx:[[Lcom/android/server/tv/DataBaseDesk$T_MS_COLOR_TEMPEX_DATA;

    aget-object v0, v0, p1

    sget-object v1, Lcom/android/server/tv/DataBaseDesk$EN_MS_COLOR_TEMP_INPUT_SOURCE;->E_INPUT_SOURCE_YPBPR:Lcom/android/server/tv/DataBaseDesk$EN_MS_COLOR_TEMP_INPUT_SOURCE;

    invoke-virtual {v1}, Lcom/android/server/tv/DataBaseDesk$EN_MS_COLOR_TEMP_INPUT_SOURCE;->ordinal()I

    move-result v1

    aget-object v12, v0, v1

    goto/16 :goto_1

    :pswitch_5
    iget-object v0, v9, Lcom/android/server/tv/DataBaseDesk$T_MS_COLOR_TEMPEX;->astColorTempEx:[[Lcom/android/server/tv/DataBaseDesk$T_MS_COLOR_TEMPEX_DATA;

    aget-object v0, v0, p1

    sget-object v1, Lcom/android/server/tv/DataBaseDesk$EN_MS_COLOR_TEMP_INPUT_SOURCE;->E_INPUT_SOURCE_SCART:Lcom/android/server/tv/DataBaseDesk$EN_MS_COLOR_TEMP_INPUT_SOURCE;

    invoke-virtual {v1}, Lcom/android/server/tv/DataBaseDesk$EN_MS_COLOR_TEMP_INPUT_SOURCE;->ordinal()I

    move-result v1

    aget-object v12, v0, v1

    goto/16 :goto_1

    :pswitch_6
    iget-object v0, v9, Lcom/android/server/tv/DataBaseDesk$T_MS_COLOR_TEMPEX;->astColorTempEx:[[Lcom/android/server/tv/DataBaseDesk$T_MS_COLOR_TEMPEX_DATA;

    aget-object v0, v0, p1

    sget-object v1, Lcom/android/server/tv/DataBaseDesk$EN_MS_COLOR_TEMP_INPUT_SOURCE;->E_INPUT_SOURCE_HDMI:Lcom/android/server/tv/DataBaseDesk$EN_MS_COLOR_TEMP_INPUT_SOURCE;

    invoke-virtual {v1}, Lcom/android/server/tv/DataBaseDesk$EN_MS_COLOR_TEMP_INPUT_SOURCE;->ordinal()I

    move-result v1

    aget-object v12, v0, v1

    goto/16 :goto_1

    :pswitch_7
    iget-object v0, v9, Lcom/android/server/tv/DataBaseDesk$T_MS_COLOR_TEMPEX;->astColorTempEx:[[Lcom/android/server/tv/DataBaseDesk$T_MS_COLOR_TEMPEX_DATA;

    aget-object v0, v0, p1

    sget-object v1, Lcom/android/server/tv/DataBaseDesk$EN_MS_COLOR_TEMP_INPUT_SOURCE;->E_INPUT_SOURCE_DTV:Lcom/android/server/tv/DataBaseDesk$EN_MS_COLOR_TEMP_INPUT_SOURCE;

    invoke-virtual {v1}, Lcom/android/server/tv/DataBaseDesk$EN_MS_COLOR_TEMP_INPUT_SOURCE;->ordinal()I

    move-result v1

    aget-object v12, v0, v1

    goto/16 :goto_1

    :pswitch_8
    iget-object v0, v9, Lcom/android/server/tv/DataBaseDesk$T_MS_COLOR_TEMPEX;->astColorTempEx:[[Lcom/android/server/tv/DataBaseDesk$T_MS_COLOR_TEMPEX_DATA;

    aget-object v0, v0, p1

    sget-object v1, Lcom/android/server/tv/DataBaseDesk$EN_MS_COLOR_TEMP_INPUT_SOURCE;->E_INPUT_SOURCE_OTHERS:Lcom/android/server/tv/DataBaseDesk$EN_MS_COLOR_TEMP_INPUT_SOURCE;

    invoke-virtual {v1}, Lcom/android/server/tv/DataBaseDesk$EN_MS_COLOR_TEMP_INPUT_SOURCE;->ordinal()I

    move-result v1

    aget-object v12, v0, v1

    goto/16 :goto_1

    :catch_1
    move-exception v10

    invoke-virtual {v10}, Lcom/mstar/android/tvapi/common/exception/TvCommonException;->printStackTrace()V

    goto/16 :goto_2

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
        :pswitch_1
        :pswitch_2
        :pswitch_2
        :pswitch_2
        :pswitch_2
        :pswitch_2
        :pswitch_2
        :pswitch_2
        :pswitch_2
        :pswitch_3
        :pswitch_3
        :pswitch_3
        :pswitch_3
        :pswitch_4
        :pswitch_4
        :pswitch_4
        :pswitch_5
        :pswitch_5
        :pswitch_6
        :pswitch_6
        :pswitch_6
        :pswitch_6
        :pswitch_7
        :pswitch_7
        :pswitch_8
        :pswitch_8
        :pswitch_8
        :pswitch_8
        :pswitch_8
        :pswitch_8
        :pswitch_8
        :pswitch_8
    .end packed-switch
.end method

.method public setColorTempPara(Lcom/mstar/android/tvapi/common/vo/ColorTemperatureExData;)Z
    .locals 4
    .param p1    # Lcom/mstar/android/tvapi/common/vo/ColorTemperatureExData;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Landroid/os/RemoteException;
        }
    .end annotation

    const-string v1, "TvPictureClient"

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "setColorTempPara, paras ColorTemperatureExData stColorTemp.redGain = "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    iget v3, p1, Lcom/mstar/android/tvapi/common/vo/ColorTemperatureExData;->redGain:I

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, ", stColorTemp.blueGain = "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    iget v3, p1, Lcom/mstar/android/tvapi/common/vo/ColorTemperatureExData;->blueGain:I

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, ", stColorTemp.greenGain = "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    iget v3, p1, Lcom/mstar/android/tvapi/common/vo/ColorTemperatureExData;->greenGain:I

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, ", stColorTemp.redOffset = "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    iget v3, p1, Lcom/mstar/android/tvapi/common/vo/ColorTemperatureExData;->redOffset:I

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, ", stColorTemp.blueOffse = "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    iget v3, p1, Lcom/mstar/android/tvapi/common/vo/ColorTemperatureExData;->blueOffset:I

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, ", stColorTemp.greenOffset = "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    iget v3, p1, Lcom/mstar/android/tvapi/common/vo/ColorTemperatureExData;->greenOffset:I

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    invoke-virtual {p0}, Lcom/android/server/tv/TvPictureClient;->getColorTempIdx()I

    move-result v0

    iget-object v1, p0, Lcom/android/server/tv/TvPictureClient;->databaseMgr:Lcom/android/server/tv/DataBaseDeskImpl;

    invoke-virtual {v1, p1, v0}, Lcom/android/server/tv/DataBaseDeskImpl;->updateUsrColorTmpExData(Lcom/mstar/android/tvapi/common/vo/ColorTemperatureExData;I)V

    const/4 v1, 0x1

    return v1
.end method

.method public setDisplayWindow(Lcom/mstar/android/tvapi/common/vo/VideoWindowType;)V
    .locals 4
    .param p1    # Lcom/mstar/android/tvapi/common/vo/VideoWindowType;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Landroid/os/RemoteException;
        }
    .end annotation

    const-string v1, "TvPictureClient"

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "setDisplayWindow, paras VideoWindowType videoWindowType.x = "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    iget v3, p1, Lcom/mstar/android/tvapi/common/vo/VideoWindowType;->x:I

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, ", videoWindowType.y = "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    iget v3, p1, Lcom/mstar/android/tvapi/common/vo/VideoWindowType;->y:I

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, ", videoWindowType.width = "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    iget v3, p1, Lcom/mstar/android/tvapi/common/vo/VideoWindowType;->width:I

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, ", videoWindowType.height = "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    iget v3, p1, Lcom/mstar/android/tvapi/common/vo/VideoWindowType;->height:I

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    :try_start_0
    invoke-static {}, Lcom/mstar/android/tvapi/common/TvManager;->getInstance()Lcom/mstar/android/tvapi/common/TvManager;

    move-result-object v1

    if-eqz v1, :cond_0

    invoke-static {}, Lcom/mstar/android/tvapi/common/TvManager;->getInstance()Lcom/mstar/android/tvapi/common/TvManager;

    move-result-object v1

    invoke-virtual {v1}, Lcom/mstar/android/tvapi/common/TvManager;->getPictureManager()Lcom/mstar/android/tvapi/common/PictureManager;

    move-result-object v1

    sget-object v2, Lcom/mstar/android/tvapi/common/vo/EnumScalerWindow;->E_MAIN_WINDOW:Lcom/mstar/android/tvapi/common/vo/EnumScalerWindow;

    invoke-virtual {v1, v2}, Lcom/mstar/android/tvapi/common/PictureManager;->selectWindow(Lcom/mstar/android/tvapi/common/vo/EnumScalerWindow;)Z

    invoke-static {}, Lcom/mstar/android/tvapi/common/TvManager;->getInstance()Lcom/mstar/android/tvapi/common/TvManager;

    move-result-object v1

    invoke-virtual {v1}, Lcom/mstar/android/tvapi/common/TvManager;->getPictureManager()Lcom/mstar/android/tvapi/common/PictureManager;

    move-result-object v1

    invoke-virtual {v1, p1}, Lcom/mstar/android/tvapi/common/PictureManager;->setDisplayWindow(Lcom/mstar/android/tvapi/common/vo/VideoWindowType;)V

    invoke-static {}, Lcom/mstar/android/tvapi/common/TvManager;->getInstance()Lcom/mstar/android/tvapi/common/TvManager;

    move-result-object v1

    invoke-virtual {v1}, Lcom/mstar/android/tvapi/common/TvManager;->getPictureManager()Lcom/mstar/android/tvapi/common/PictureManager;

    move-result-object v1

    invoke-virtual {v1}, Lcom/mstar/android/tvapi/common/PictureManager;->scaleWindow()Z
    :try_end_0
    .catch Lcom/mstar/android/tvapi/common/exception/TvCommonException; {:try_start_0 .. :try_end_0} :catch_0

    :cond_0
    :goto_0
    return-void

    :catch_0
    move-exception v0

    invoke-virtual {v0}, Lcom/mstar/android/tvapi/common/exception/TvCommonException;->printStackTrace()V

    goto :goto_0
.end method

.method public setFilmMode(I)Z
    .locals 5
    .param p1    # I
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Landroid/os/RemoteException;
        }
    .end annotation

    const-string v2, "TvPictureClient"

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string v4, "setFilmMode, paras eMode is "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3, p1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v2, v3}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    invoke-direct {p0}, Lcom/android/server/tv/TvPictureClient;->getCurrentInputsource()I

    move-result v1

    iget-object v2, p0, Lcom/android/server/tv/TvPictureClient;->databaseMgr:Lcom/android/server/tv/DataBaseDeskImpl;

    invoke-virtual {v2, p1, v1}, Lcom/android/server/tv/DataBaseDeskImpl;->updateFilmMode(II)V

    :try_start_0
    invoke-static {}, Lcom/mstar/android/tvapi/common/TvManager;->getInstance()Lcom/mstar/android/tvapi/common/TvManager;

    move-result-object v2

    if-eqz v2, :cond_0

    invoke-static {}, Lcom/mstar/android/tvapi/common/TvManager;->getInstance()Lcom/mstar/android/tvapi/common/TvManager;

    move-result-object v2

    invoke-virtual {v2}, Lcom/mstar/android/tvapi/common/TvManager;->getPictureManager()Lcom/mstar/android/tvapi/common/PictureManager;

    move-result-object v2

    invoke-static {}, Lcom/mstar/android/tvapi/common/vo/Film$EnumFilm;->values()[Lcom/mstar/android/tvapi/common/vo/Film$EnumFilm;

    move-result-object v3

    invoke-static {p1}, Lcom/mstar/android/tvapi/common/vo/Film$EnumFilm;->getOrdinalThroughValue(I)I

    move-result v4

    aget-object v3, v3, v4

    invoke-virtual {v2, v3}, Lcom/mstar/android/tvapi/common/PictureManager;->setFilm(Lcom/mstar/android/tvapi/common/vo/Film$EnumFilm;)V
    :try_end_0
    .catch Lcom/mstar/android/tvapi/common/exception/TvCommonException; {:try_start_0 .. :try_end_0} :catch_0

    :cond_0
    :goto_0
    const/4 v2, 0x0

    return v2

    :catch_0
    move-exception v0

    invoke-virtual {v0}, Lcom/mstar/android/tvapi/common/exception/TvCommonException;->printStackTrace()V

    goto :goto_0
.end method

.method public setNR(I)Z
    .locals 7
    .param p1    # I
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Landroid/os/RemoteException;
        }
    .end annotation

    const-string v4, "TvPictureClient"

    new-instance v5, Ljava/lang/StringBuilder;

    invoke-direct {v5}, Ljava/lang/StringBuilder;-><init>()V

    const-string v6, "setNR, paras eNRIdx is "

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5, p1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    invoke-static {v4, v5}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    invoke-virtual {p0}, Lcom/android/server/tv/TvPictureClient;->getColorTempIdx()I

    move-result v2

    invoke-direct {p0}, Lcom/android/server/tv/TvPictureClient;->getCurrentInputsource()I

    move-result v0

    invoke-static {}, Lcom/mstar/android/tvapi/common/vo/NoiseReduction$EnumNoiseReduction;->values()[Lcom/mstar/android/tvapi/common/vo/NoiseReduction$EnumNoiseReduction;

    move-result-object v4

    invoke-static {p1}, Lcom/mstar/android/tvapi/common/vo/NoiseReduction$EnumNoiseReduction;->getOrdinalThroughValue(I)I

    move-result v5

    aget-object v3, v4, v5

    iget-object v4, p0, Lcom/android/server/tv/TvPictureClient;->databaseMgr:Lcom/android/server/tv/DataBaseDeskImpl;

    invoke-virtual {v4, v0, v2, v3}, Lcom/android/server/tv/DataBaseDeskImpl;->updateNR(IILcom/mstar/android/tvapi/common/vo/NoiseReduction$EnumNoiseReduction;)V

    :try_start_0
    invoke-static {}, Lcom/mstar/android/tvapi/common/TvManager;->getInstance()Lcom/mstar/android/tvapi/common/TvManager;

    move-result-object v4

    if-eqz v4, :cond_0

    invoke-static {}, Lcom/mstar/android/tvapi/common/TvManager;->getInstance()Lcom/mstar/android/tvapi/common/TvManager;

    move-result-object v4

    invoke-virtual {v4}, Lcom/mstar/android/tvapi/common/TvManager;->getPictureManager()Lcom/mstar/android/tvapi/common/PictureManager;

    move-result-object v4

    invoke-virtual {v4, v3}, Lcom/mstar/android/tvapi/common/PictureManager;->setNoiseReduction(Lcom/mstar/android/tvapi/common/vo/NoiseReduction$EnumNoiseReduction;)Z
    :try_end_0
    .catch Lcom/mstar/android/tvapi/common/exception/TvCommonException; {:try_start_0 .. :try_end_0} :catch_0

    :cond_0
    :goto_0
    const/4 v4, 0x1

    return v4

    :catch_0
    move-exception v1

    invoke-virtual {v1}, Lcom/mstar/android/tvapi/common/exception/TvCommonException;->printStackTrace()V

    goto :goto_0
.end method

.method public setPCClock(I)Z
    .locals 4
    .param p1    # I
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Landroid/os/RemoteException;
        }
    .end annotation

    const-string v1, "TvPictureClient"

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "setPCClock, paras clock is "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, p1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    :try_start_0
    invoke-static {}, Lcom/mstar/android/tvapi/common/TvManager;->getInstance()Lcom/mstar/android/tvapi/common/TvManager;

    move-result-object v1

    if-eqz v1, :cond_0

    invoke-static {}, Lcom/mstar/android/tvapi/common/TvManager;->getInstance()Lcom/mstar/android/tvapi/common/TvManager;

    move-result-object v1

    invoke-virtual {v1}, Lcom/mstar/android/tvapi/common/TvManager;->getPlayerManager()Lcom/mstar/android/tvapi/common/TvPlayer;

    move-result-object v1

    invoke-interface {v1, p1}, Lcom/mstar/android/tvapi/common/TvPlayer;->setSize(I)Z
    :try_end_0
    .catch Lcom/mstar/android/tvapi/common/exception/TvCommonException; {:try_start_0 .. :try_end_0} :catch_0

    :cond_0
    :goto_0
    const/4 v1, 0x1

    return v1

    :catch_0
    move-exception v0

    invoke-virtual {v0}, Lcom/mstar/android/tvapi/common/exception/TvCommonException;->printStackTrace()V

    goto :goto_0
.end method

.method public setPCHPos(I)Z
    .locals 4
    .param p1    # I
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Landroid/os/RemoteException;
        }
    .end annotation

    const-string v1, "TvPictureClient"

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "setPCHPos, paras hpos is "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, p1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    :try_start_0
    invoke-static {}, Lcom/mstar/android/tvapi/common/TvManager;->getInstance()Lcom/mstar/android/tvapi/common/TvManager;

    move-result-object v1

    if-eqz v1, :cond_0

    invoke-static {}, Lcom/mstar/android/tvapi/common/TvManager;->getInstance()Lcom/mstar/android/tvapi/common/TvManager;

    move-result-object v1

    invoke-virtual {v1}, Lcom/mstar/android/tvapi/common/TvManager;->getPlayerManager()Lcom/mstar/android/tvapi/common/TvPlayer;

    move-result-object v1

    invoke-interface {v1, p1}, Lcom/mstar/android/tvapi/common/TvPlayer;->setHPosition(I)Z
    :try_end_0
    .catch Lcom/mstar/android/tvapi/common/exception/TvCommonException; {:try_start_0 .. :try_end_0} :catch_0

    :cond_0
    :goto_0
    const/4 v1, 0x1

    return v1

    :catch_0
    move-exception v0

    invoke-virtual {v0}, Lcom/mstar/android/tvapi/common/exception/TvCommonException;->printStackTrace()V

    goto :goto_0
.end method

.method public setPCPhase(I)Z
    .locals 4
    .param p1    # I
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Landroid/os/RemoteException;
        }
    .end annotation

    const-string v1, "TvPictureClient"

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "setPCPhase, paras phase is "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, p1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    :try_start_0
    invoke-static {}, Lcom/mstar/android/tvapi/common/TvManager;->getInstance()Lcom/mstar/android/tvapi/common/TvManager;

    move-result-object v1

    if-eqz v1, :cond_0

    invoke-static {}, Lcom/mstar/android/tvapi/common/TvManager;->getInstance()Lcom/mstar/android/tvapi/common/TvManager;

    move-result-object v1

    invoke-virtual {v1}, Lcom/mstar/android/tvapi/common/TvManager;->getPlayerManager()Lcom/mstar/android/tvapi/common/TvPlayer;

    move-result-object v1

    invoke-interface {v1, p1}, Lcom/mstar/android/tvapi/common/TvPlayer;->setPhase(I)Z
    :try_end_0
    .catch Lcom/mstar/android/tvapi/common/exception/TvCommonException; {:try_start_0 .. :try_end_0} :catch_0

    :cond_0
    :goto_0
    const/4 v1, 0x1

    return v1

    :catch_0
    move-exception v0

    invoke-virtual {v0}, Lcom/mstar/android/tvapi/common/exception/TvCommonException;->printStackTrace()V

    goto :goto_0
.end method

.method public setPCVPos(I)Z
    .locals 4
    .param p1    # I
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Landroid/os/RemoteException;
        }
    .end annotation

    const-string v1, "TvPictureClient"

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "setPCVPos, paras vpos is "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, p1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    :try_start_0
    invoke-static {}, Lcom/mstar/android/tvapi/common/TvManager;->getInstance()Lcom/mstar/android/tvapi/common/TvManager;

    move-result-object v1

    if-eqz v1, :cond_0

    invoke-static {}, Lcom/mstar/android/tvapi/common/TvManager;->getInstance()Lcom/mstar/android/tvapi/common/TvManager;

    move-result-object v1

    invoke-virtual {v1}, Lcom/mstar/android/tvapi/common/TvManager;->getPlayerManager()Lcom/mstar/android/tvapi/common/TvPlayer;

    move-result-object v1

    invoke-interface {v1, p1}, Lcom/mstar/android/tvapi/common/TvPlayer;->setVPosition(I)Z
    :try_end_0
    .catch Lcom/mstar/android/tvapi/common/exception/TvCommonException; {:try_start_0 .. :try_end_0} :catch_0

    :cond_0
    :goto_0
    const/4 v1, 0x1

    return v1

    :catch_0
    move-exception v0

    invoke-virtual {v0}, Lcom/mstar/android/tvapi/common/exception/TvCommonException;->printStackTrace()V

    goto :goto_0
.end method

.method public setPictureModeIdx(I)Z
    .locals 6
    .param p1    # I
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Landroid/os/RemoteException;
        }
    .end annotation

    const-string v3, "TvPictureClient"

    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    const-string v5, "setPictureModeIdx, paras ePicMode is "

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4, p1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-static {v3, v4}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    const/4 v1, 0x0

    :try_start_0
    invoke-static {}, Lcom/mstar/android/tvapi/common/TvManager;->getInstance()Lcom/mstar/android/tvapi/common/TvManager;

    move-result-object v3

    if-eqz v3, :cond_0

    invoke-static {}, Lcom/mstar/android/tvapi/common/TvManager;->getInstance()Lcom/mstar/android/tvapi/common/TvManager;

    move-result-object v3

    invoke-virtual {v3}, Lcom/mstar/android/tvapi/common/TvManager;->getCurrentInputSource()Lcom/mstar/android/tvapi/common/vo/TvOsType$EnumInputSource;
    :try_end_0
    .catch Lcom/mstar/android/tvapi/common/exception/TvCommonException; {:try_start_0 .. :try_end_0} :catch_0

    move-result-object v1

    :cond_0
    :goto_0
    iget-object v3, p0, Lcom/android/server/tv/TvPictureClient;->databaseMgr:Lcom/android/server/tv/DataBaseDeskImpl;

    invoke-virtual {v1}, Lcom/mstar/android/tvapi/common/vo/TvOsType$EnumInputSource;->ordinal()I

    move-result v4

    invoke-virtual {v3, p1, v4}, Lcom/android/server/tv/DataBaseDeskImpl;->updatePictureMode(II)V

    iget-object v3, p0, Lcom/android/server/tv/TvPictureClient;->databaseMgr:Lcom/android/server/tv/DataBaseDeskImpl;

    invoke-virtual {v1}, Lcom/mstar/android/tvapi/common/vo/TvOsType$EnumInputSource;->ordinal()I

    move-result v4

    invoke-virtual {v3, p1, v4}, Lcom/android/server/tv/DataBaseDeskImpl;->queryPictureModeSettings(II)Lcom/android/server/tv/DataBaseDesk$T_MS_PICTURE;

    move-result-object v2

    :try_start_1
    invoke-static {}, Lcom/mstar/android/tvapi/common/TvManager;->getInstance()Lcom/mstar/android/tvapi/common/TvManager;

    move-result-object v3

    if-eqz v3, :cond_1

    invoke-static {}, Lcom/mstar/android/tvapi/common/TvManager;->getInstance()Lcom/mstar/android/tvapi/common/TvManager;

    move-result-object v3

    invoke-virtual {v3}, Lcom/mstar/android/tvapi/common/TvManager;->getPictureManager()Lcom/mstar/android/tvapi/common/PictureManager;

    move-result-object v3

    iget-short v4, v2, Lcom/android/server/tv/DataBaseDesk$T_MS_PICTURE;->brightness:S

    invoke-virtual {v3, v4}, Lcom/mstar/android/tvapi/common/PictureManager;->setPictureModeBrightness(S)V

    invoke-static {}, Lcom/mstar/android/tvapi/common/TvManager;->getInstance()Lcom/mstar/android/tvapi/common/TvManager;

    move-result-object v3

    invoke-virtual {v3}, Lcom/mstar/android/tvapi/common/TvManager;->getPictureManager()Lcom/mstar/android/tvapi/common/PictureManager;

    move-result-object v3

    iget-short v4, v2, Lcom/android/server/tv/DataBaseDesk$T_MS_PICTURE;->contrast:S

    invoke-virtual {v3, v4}, Lcom/mstar/android/tvapi/common/PictureManager;->setPictureModeContrast(S)V

    invoke-static {}, Lcom/mstar/android/tvapi/common/TvManager;->getInstance()Lcom/mstar/android/tvapi/common/TvManager;

    move-result-object v3

    invoke-virtual {v3}, Lcom/mstar/android/tvapi/common/TvManager;->getPictureManager()Lcom/mstar/android/tvapi/common/PictureManager;

    move-result-object v3

    iget-short v4, v2, Lcom/android/server/tv/DataBaseDesk$T_MS_PICTURE;->saturation:S

    invoke-virtual {v3, v4}, Lcom/mstar/android/tvapi/common/PictureManager;->setPictureModeColor(S)V

    invoke-static {}, Lcom/mstar/android/tvapi/common/TvManager;->getInstance()Lcom/mstar/android/tvapi/common/TvManager;

    move-result-object v3

    invoke-virtual {v3}, Lcom/mstar/android/tvapi/common/TvManager;->getPictureManager()Lcom/mstar/android/tvapi/common/PictureManager;

    move-result-object v3

    iget-short v4, v2, Lcom/android/server/tv/DataBaseDesk$T_MS_PICTURE;->sharpness:S

    invoke-virtual {v3, v4}, Lcom/mstar/android/tvapi/common/PictureManager;->setPictureModeSharpness(S)V

    invoke-static {}, Lcom/mstar/android/tvapi/common/TvManager;->getInstance()Lcom/mstar/android/tvapi/common/TvManager;

    move-result-object v3

    invoke-virtual {v3}, Lcom/mstar/android/tvapi/common/TvManager;->getPictureManager()Lcom/mstar/android/tvapi/common/PictureManager;

    move-result-object v3

    iget-short v4, v2, Lcom/android/server/tv/DataBaseDesk$T_MS_PICTURE;->hue:S

    invoke-virtual {v3, v4}, Lcom/mstar/android/tvapi/common/PictureManager;->setPictureModeTint(S)V
    :try_end_1
    .catch Lcom/mstar/android/tvapi/common/exception/TvCommonException; {:try_start_1 .. :try_end_1} :catch_1

    :cond_1
    :goto_1
    const/4 v3, 0x1

    return v3

    :catch_0
    move-exception v0

    invoke-virtual {v0}, Lcom/mstar/android/tvapi/common/exception/TvCommonException;->printStackTrace()V

    goto :goto_0

    :catch_1
    move-exception v0

    invoke-virtual {v0}, Lcom/mstar/android/tvapi/common/exception/TvCommonException;->printStackTrace()V

    goto :goto_1
.end method

.method public setVideoArc(I)Z
    .locals 5
    .param p1    # I
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Landroid/os/RemoteException;
        }
    .end annotation

    const-string v2, "TvPictureClient"

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string v4, "setVideoArc, paras eArcIdx is "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3, p1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v2, v3}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    sget-object v0, Lcom/mstar/android/tvapi/common/vo/EnumVideoArcType;->E_DEFAULT:Lcom/mstar/android/tvapi/common/vo/EnumVideoArcType;

    sget-object v2, Lcom/android/server/tv/DataBaseDesk$MAPI_VIDEO_ARC_Type;->E_AR_DotByDot:Lcom/android/server/tv/DataBaseDesk$MAPI_VIDEO_ARC_Type;

    invoke-virtual {v2}, Lcom/android/server/tv/DataBaseDesk$MAPI_VIDEO_ARC_Type;->ordinal()I

    move-result v2

    if-ge p1, v2, :cond_2

    invoke-static {}, Lcom/mstar/android/tvapi/common/vo/EnumVideoArcType;->values()[Lcom/mstar/android/tvapi/common/vo/EnumVideoArcType;

    move-result-object v2

    aget-object v0, v2, p1

    iget-object v2, p0, Lcom/android/server/tv/TvPictureClient;->databaseMgr:Lcom/android/server/tv/DataBaseDeskImpl;

    invoke-direct {p0}, Lcom/android/server/tv/TvPictureClient;->getCurrentInputsource()I

    move-result v3

    invoke-virtual {v2, p1, v3}, Lcom/android/server/tv/DataBaseDeskImpl;->updateArcMode(II)V

    :cond_0
    :goto_0
    :try_start_0
    invoke-static {}, Lcom/mstar/android/tvapi/common/TvManager;->getInstance()Lcom/mstar/android/tvapi/common/TvManager;

    move-result-object v2

    if-eqz v2, :cond_1

    invoke-static {}, Lcom/mstar/android/tvapi/common/TvManager;->getInstance()Lcom/mstar/android/tvapi/common/TvManager;

    move-result-object v2

    invoke-virtual {v2}, Lcom/mstar/android/tvapi/common/TvManager;->getPictureManager()Lcom/mstar/android/tvapi/common/PictureManager;

    move-result-object v2

    invoke-virtual {v2, v0}, Lcom/mstar/android/tvapi/common/PictureManager;->setAspectRatio(Lcom/mstar/android/tvapi/common/vo/EnumVideoArcType;)V
    :try_end_0
    .catch Lcom/mstar/android/tvapi/common/exception/TvCommonException; {:try_start_0 .. :try_end_0} :catch_0

    :cond_1
    :goto_1
    const/4 v2, 0x1

    return v2

    :cond_2
    sget-object v2, Lcom/android/server/tv/DataBaseDesk$MAPI_VIDEO_ARC_Type;->E_AR_DotByDot:Lcom/android/server/tv/DataBaseDesk$MAPI_VIDEO_ARC_Type;

    invoke-virtual {v2}, Lcom/android/server/tv/DataBaseDesk$MAPI_VIDEO_ARC_Type;->ordinal()I

    move-result v2

    if-ne p1, v2, :cond_0

    sget-object v0, Lcom/mstar/android/tvapi/common/vo/EnumVideoArcType;->E_16x9:Lcom/mstar/android/tvapi/common/vo/EnumVideoArcType;

    iget-object v2, p0, Lcom/android/server/tv/TvPictureClient;->databaseMgr:Lcom/android/server/tv/DataBaseDeskImpl;

    sget-object v3, Lcom/android/server/tv/DataBaseDesk$MAPI_VIDEO_ARC_Type;->E_AR_16x9:Lcom/android/server/tv/DataBaseDesk$MAPI_VIDEO_ARC_Type;

    invoke-virtual {v3}, Lcom/android/server/tv/DataBaseDesk$MAPI_VIDEO_ARC_Type;->ordinal()I

    move-result v3

    invoke-direct {p0}, Lcom/android/server/tv/TvPictureClient;->getCurrentInputsource()I

    move-result v4

    invoke-virtual {v2, v3, v4}, Lcom/android/server/tv/DataBaseDeskImpl;->updateArcMode(II)V

    goto :goto_0

    :catch_0
    move-exception v1

    invoke-virtual {v1}, Lcom/mstar/android/tvapi/common/exception/TvCommonException;->printStackTrace()V

    goto :goto_1
.end method

.method public setVideoItem(II)Z
    .locals 6
    .param p1    # I
    .param p2    # I
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Landroid/os/RemoteException;
        }
    .end annotation

    const-string v3, "TvPictureClient"

    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    const-string v5, "setVideoItem, paras eIndex is "

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4, p1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v4

    const-string v5, " value is "

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4, p2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-static {v3, v4}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    invoke-direct {p0}, Lcom/android/server/tv/TvPictureClient;->getCurrentInputsource()I

    move-result v1

    iget-object v3, p0, Lcom/android/server/tv/TvPictureClient;->databaseMgr:Lcom/android/server/tv/DataBaseDeskImpl;

    invoke-virtual {v3, v1}, Lcom/android/server/tv/DataBaseDeskImpl;->queryPictureMode(I)I

    move-result v2

    iget-object v3, p0, Lcom/android/server/tv/TvPictureClient;->databaseMgr:Lcom/android/server/tv/DataBaseDeskImpl;

    invoke-static {}, Lcom/android/server/tv/DataBaseDesk$EN_MS_VIDEOITEM;->values()[Lcom/android/server/tv/DataBaseDesk$EN_MS_VIDEOITEM;

    move-result-object v4

    aget-object v4, v4, p1

    invoke-virtual {v3, v4, v1, v2, p2}, Lcom/android/server/tv/DataBaseDeskImpl;->updatePicModeSetting(Lcom/android/server/tv/DataBaseDesk$EN_MS_VIDEOITEM;III)V

    :try_start_0
    sget-object v3, Lcom/android/server/tv/DataBaseDesk$EN_MS_VIDEOITEM;->MS_VIDEOITEM_BRIGHTNESS:Lcom/android/server/tv/DataBaseDesk$EN_MS_VIDEOITEM;

    invoke-virtual {v3}, Lcom/android/server/tv/DataBaseDesk$EN_MS_VIDEOITEM;->ordinal()I

    move-result v3

    if-ne p1, v3, :cond_1

    invoke-static {}, Lcom/mstar/android/tvapi/common/TvManager;->getInstance()Lcom/mstar/android/tvapi/common/TvManager;

    move-result-object v3

    if-eqz v3, :cond_0

    invoke-static {}, Lcom/mstar/android/tvapi/common/TvManager;->getInstance()Lcom/mstar/android/tvapi/common/TvManager;

    move-result-object v3

    invoke-virtual {v3}, Lcom/mstar/android/tvapi/common/TvManager;->getPictureManager()Lcom/mstar/android/tvapi/common/PictureManager;

    move-result-object v3

    int-to-short v4, p2

    invoke-virtual {v3, v4}, Lcom/mstar/android/tvapi/common/PictureManager;->setPictureModeBrightness(S)V

    :cond_0
    :goto_0
    const/4 v3, 0x1

    :goto_1
    return v3

    :cond_1
    sget-object v3, Lcom/android/server/tv/DataBaseDesk$EN_MS_VIDEOITEM;->MS_VIDEOITEM_CONTRAST:Lcom/android/server/tv/DataBaseDesk$EN_MS_VIDEOITEM;

    invoke-virtual {v3}, Lcom/android/server/tv/DataBaseDesk$EN_MS_VIDEOITEM;->ordinal()I

    move-result v3

    if-ne p1, v3, :cond_2

    invoke-static {}, Lcom/mstar/android/tvapi/common/TvManager;->getInstance()Lcom/mstar/android/tvapi/common/TvManager;

    move-result-object v3

    if-eqz v3, :cond_0

    invoke-static {}, Lcom/mstar/android/tvapi/common/TvManager;->getInstance()Lcom/mstar/android/tvapi/common/TvManager;

    move-result-object v3

    invoke-virtual {v3}, Lcom/mstar/android/tvapi/common/TvManager;->getPictureManager()Lcom/mstar/android/tvapi/common/PictureManager;

    move-result-object v3

    int-to-short v4, p2

    invoke-virtual {v3, v4}, Lcom/mstar/android/tvapi/common/PictureManager;->setPictureModeContrast(S)V
    :try_end_0
    .catch Lcom/mstar/android/tvapi/common/exception/TvCommonException; {:try_start_0 .. :try_end_0} :catch_0

    goto :goto_0

    :catch_0
    move-exception v0

    invoke-virtual {v0}, Lcom/mstar/android/tvapi/common/exception/TvCommonException;->printStackTrace()V

    goto :goto_0

    :cond_2
    :try_start_1
    sget-object v3, Lcom/android/server/tv/DataBaseDesk$EN_MS_VIDEOITEM;->MS_VIDEOITEM_SHARPNESS:Lcom/android/server/tv/DataBaseDesk$EN_MS_VIDEOITEM;

    invoke-virtual {v3}, Lcom/android/server/tv/DataBaseDesk$EN_MS_VIDEOITEM;->ordinal()I

    move-result v3

    if-ne p1, v3, :cond_3

    invoke-static {}, Lcom/mstar/android/tvapi/common/TvManager;->getInstance()Lcom/mstar/android/tvapi/common/TvManager;

    move-result-object v3

    if-eqz v3, :cond_0

    invoke-static {}, Lcom/mstar/android/tvapi/common/TvManager;->getInstance()Lcom/mstar/android/tvapi/common/TvManager;

    move-result-object v3

    invoke-virtual {v3}, Lcom/mstar/android/tvapi/common/TvManager;->getPictureManager()Lcom/mstar/android/tvapi/common/PictureManager;

    move-result-object v3

    int-to-short v4, p2

    invoke-virtual {v3, v4}, Lcom/mstar/android/tvapi/common/PictureManager;->setPictureModeSharpness(S)V

    goto :goto_0

    :cond_3
    sget-object v3, Lcom/android/server/tv/DataBaseDesk$EN_MS_VIDEOITEM;->MS_VIDEOITEM_SATURATION:Lcom/android/server/tv/DataBaseDesk$EN_MS_VIDEOITEM;

    invoke-virtual {v3}, Lcom/android/server/tv/DataBaseDesk$EN_MS_VIDEOITEM;->ordinal()I

    move-result v3

    if-ne p1, v3, :cond_4

    invoke-static {}, Lcom/mstar/android/tvapi/common/TvManager;->getInstance()Lcom/mstar/android/tvapi/common/TvManager;

    move-result-object v3

    if-eqz v3, :cond_0

    invoke-static {}, Lcom/mstar/android/tvapi/common/TvManager;->getInstance()Lcom/mstar/android/tvapi/common/TvManager;

    move-result-object v3

    invoke-virtual {v3}, Lcom/mstar/android/tvapi/common/TvManager;->getPictureManager()Lcom/mstar/android/tvapi/common/PictureManager;

    move-result-object v3

    int-to-short v4, p2

    invoke-virtual {v3, v4}, Lcom/mstar/android/tvapi/common/PictureManager;->setPictureModeColor(S)V

    goto :goto_0

    :cond_4
    sget-object v3, Lcom/android/server/tv/DataBaseDesk$EN_MS_VIDEOITEM;->MS_VIDEOITEM_HUE:Lcom/android/server/tv/DataBaseDesk$EN_MS_VIDEOITEM;

    invoke-virtual {v3}, Lcom/android/server/tv/DataBaseDesk$EN_MS_VIDEOITEM;->ordinal()I

    move-result v3

    if-ne p1, v3, :cond_5

    invoke-static {}, Lcom/mstar/android/tvapi/common/TvManager;->getInstance()Lcom/mstar/android/tvapi/common/TvManager;

    move-result-object v3

    if-eqz v3, :cond_0

    invoke-static {}, Lcom/mstar/android/tvapi/common/TvManager;->getInstance()Lcom/mstar/android/tvapi/common/TvManager;

    move-result-object v3

    invoke-virtual {v3}, Lcom/mstar/android/tvapi/common/TvManager;->getPictureManager()Lcom/mstar/android/tvapi/common/PictureManager;

    move-result-object v3

    int-to-short v4, p2

    invoke-virtual {v3, v4}, Lcom/mstar/android/tvapi/common/PictureManager;->setPictureModeTint(S)V
    :try_end_1
    .catch Lcom/mstar/android/tvapi/common/exception/TvCommonException; {:try_start_1 .. :try_end_1} :catch_0

    goto :goto_0

    :cond_5
    const/4 v3, 0x0

    goto :goto_1
.end method

.method public setVideoItemByInputSource(III)Z
    .locals 4
    .param p1    # I
    .param p2    # I
    .param p3    # I
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Landroid/os/RemoteException;
        }
    .end annotation

    const-string v1, "TvPictureClient"

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "setVideoItem, paras eIndex is "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, p1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, " value is "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, p2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, " input is "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, p3}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    iget-object v1, p0, Lcom/android/server/tv/TvPictureClient;->databaseMgr:Lcom/android/server/tv/DataBaseDeskImpl;

    invoke-virtual {v1, p3}, Lcom/android/server/tv/DataBaseDeskImpl;->queryPictureMode(I)I

    move-result v0

    iget-object v1, p0, Lcom/android/server/tv/TvPictureClient;->databaseMgr:Lcom/android/server/tv/DataBaseDeskImpl;

    invoke-static {}, Lcom/android/server/tv/DataBaseDesk$EN_MS_VIDEOITEM;->values()[Lcom/android/server/tv/DataBaseDesk$EN_MS_VIDEOITEM;

    move-result-object v2

    aget-object v2, v2, p1

    invoke-virtual {v1, v2, p3, v0, p2}, Lcom/android/server/tv/DataBaseDeskImpl;->updatePicModeSetting(Lcom/android/server/tv/DataBaseDesk$EN_MS_VIDEOITEM;III)V

    const/4 v1, 0x1

    return v1
.end method

.method public unFreezeImage()Z
    .locals 3
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Landroid/os/RemoteException;
        }
    .end annotation

    const-string v1, "TvPictureClient"

    const-string v2, "unFreezeImage"

    invoke-static {v1, v2}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    :try_start_0
    invoke-static {}, Lcom/mstar/android/tvapi/common/TvManager;->getInstance()Lcom/mstar/android/tvapi/common/TvManager;

    move-result-object v1

    if-eqz v1, :cond_0

    invoke-static {}, Lcom/mstar/android/tvapi/common/TvManager;->getInstance()Lcom/mstar/android/tvapi/common/TvManager;

    move-result-object v1

    invoke-virtual {v1}, Lcom/mstar/android/tvapi/common/TvManager;->getPictureManager()Lcom/mstar/android/tvapi/common/PictureManager;

    move-result-object v1

    invoke-virtual {v1}, Lcom/mstar/android/tvapi/common/PictureManager;->unFreezeImage()Z
    :try_end_0
    .catch Lcom/mstar/android/tvapi/common/exception/TvCommonException; {:try_start_0 .. :try_end_0} :catch_0

    move-result v1

    :goto_0
    return v1

    :catch_0
    move-exception v0

    invoke-virtual {v0}, Lcom/mstar/android/tvapi/common/exception/TvCommonException;->printStackTrace()V

    :cond_0
    const/4 v1, 0x0

    goto :goto_0
.end method
