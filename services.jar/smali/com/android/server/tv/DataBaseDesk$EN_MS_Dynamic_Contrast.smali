.class public final enum Lcom/android/server/tv/DataBaseDesk$EN_MS_Dynamic_Contrast;
.super Ljava/lang/Enum;
.source "DataBaseDesk.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/android/server/tv/DataBaseDesk;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x4019
    name = "EN_MS_Dynamic_Contrast"
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Enum",
        "<",
        "Lcom/android/server/tv/DataBaseDesk$EN_MS_Dynamic_Contrast;",
        ">;"
    }
.end annotation


# static fields
.field private static final synthetic $VALUES:[Lcom/android/server/tv/DataBaseDesk$EN_MS_Dynamic_Contrast;

.field public static final enum MS_Dynamic_Contrast_NUM:Lcom/android/server/tv/DataBaseDesk$EN_MS_Dynamic_Contrast;

.field public static final enum MS_Dynamic_Contrast_OFF:Lcom/android/server/tv/DataBaseDesk$EN_MS_Dynamic_Contrast;

.field public static final enum MS_Dynamic_Contrast_ON:Lcom/android/server/tv/DataBaseDesk$EN_MS_Dynamic_Contrast;


# direct methods
.method static constructor <clinit>()V
    .locals 5

    const/4 v4, 0x2

    const/4 v3, 0x1

    const/4 v2, 0x0

    new-instance v0, Lcom/android/server/tv/DataBaseDesk$EN_MS_Dynamic_Contrast;

    const-string v1, "MS_Dynamic_Contrast_OFF"

    invoke-direct {v0, v1, v2}, Lcom/android/server/tv/DataBaseDesk$EN_MS_Dynamic_Contrast;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/android/server/tv/DataBaseDesk$EN_MS_Dynamic_Contrast;->MS_Dynamic_Contrast_OFF:Lcom/android/server/tv/DataBaseDesk$EN_MS_Dynamic_Contrast;

    new-instance v0, Lcom/android/server/tv/DataBaseDesk$EN_MS_Dynamic_Contrast;

    const-string v1, "MS_Dynamic_Contrast_ON"

    invoke-direct {v0, v1, v3}, Lcom/android/server/tv/DataBaseDesk$EN_MS_Dynamic_Contrast;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/android/server/tv/DataBaseDesk$EN_MS_Dynamic_Contrast;->MS_Dynamic_Contrast_ON:Lcom/android/server/tv/DataBaseDesk$EN_MS_Dynamic_Contrast;

    new-instance v0, Lcom/android/server/tv/DataBaseDesk$EN_MS_Dynamic_Contrast;

    const-string v1, "MS_Dynamic_Contrast_NUM"

    invoke-direct {v0, v1, v4}, Lcom/android/server/tv/DataBaseDesk$EN_MS_Dynamic_Contrast;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/android/server/tv/DataBaseDesk$EN_MS_Dynamic_Contrast;->MS_Dynamic_Contrast_NUM:Lcom/android/server/tv/DataBaseDesk$EN_MS_Dynamic_Contrast;

    const/4 v0, 0x3

    new-array v0, v0, [Lcom/android/server/tv/DataBaseDesk$EN_MS_Dynamic_Contrast;

    sget-object v1, Lcom/android/server/tv/DataBaseDesk$EN_MS_Dynamic_Contrast;->MS_Dynamic_Contrast_OFF:Lcom/android/server/tv/DataBaseDesk$EN_MS_Dynamic_Contrast;

    aput-object v1, v0, v2

    sget-object v1, Lcom/android/server/tv/DataBaseDesk$EN_MS_Dynamic_Contrast;->MS_Dynamic_Contrast_ON:Lcom/android/server/tv/DataBaseDesk$EN_MS_Dynamic_Contrast;

    aput-object v1, v0, v3

    sget-object v1, Lcom/android/server/tv/DataBaseDesk$EN_MS_Dynamic_Contrast;->MS_Dynamic_Contrast_NUM:Lcom/android/server/tv/DataBaseDesk$EN_MS_Dynamic_Contrast;

    aput-object v1, v0, v4

    sput-object v0, Lcom/android/server/tv/DataBaseDesk$EN_MS_Dynamic_Contrast;->$VALUES:[Lcom/android/server/tv/DataBaseDesk$EN_MS_Dynamic_Contrast;

    return-void
.end method

.method private constructor <init>(Ljava/lang/String;I)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()V"
        }
    .end annotation

    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    return-void
.end method

.method public static valueOf(Ljava/lang/String;)Lcom/android/server/tv/DataBaseDesk$EN_MS_Dynamic_Contrast;
    .locals 1
    .param p0    # Ljava/lang/String;

    const-class v0, Lcom/android/server/tv/DataBaseDesk$EN_MS_Dynamic_Contrast;

    invoke-static {v0, p0}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;

    move-result-object v0

    check-cast v0, Lcom/android/server/tv/DataBaseDesk$EN_MS_Dynamic_Contrast;

    return-object v0
.end method

.method public static values()[Lcom/android/server/tv/DataBaseDesk$EN_MS_Dynamic_Contrast;
    .locals 1

    sget-object v0, Lcom/android/server/tv/DataBaseDesk$EN_MS_Dynamic_Contrast;->$VALUES:[Lcom/android/server/tv/DataBaseDesk$EN_MS_Dynamic_Contrast;

    invoke-virtual {v0}, [Lcom/android/server/tv/DataBaseDesk$EN_MS_Dynamic_Contrast;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [Lcom/android/server/tv/DataBaseDesk$EN_MS_Dynamic_Contrast;

    return-object v0
.end method
