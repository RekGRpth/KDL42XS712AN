.class public Lcom/android/server/tv/TvTimerClient;
.super Lcom/mstar/android/tv/ITvTimer$Stub;
.source "TvTimerClient.java"

# interfaces
.implements Lcom/mstar/android/tvapi/common/TimerManager$OnTimerEventListener;


# static fields
.field private static final TvTimerBinderTag:Ljava/lang/String; = "TvTimerClient"

.field private static powerdowntimes:I


# instance fields
.field bOffTimerFlag:Z

.field bOnTimerFlag:Z

.field private bossContext:Landroid/content/Context;

.field private curTime:Landroid/text/format/Time;

.field private eSleepMode:Lcom/mstar/android/tvapi/common/vo/EnumSleepTimeState;

.field private enSleepTimeState:Lcom/mstar/android/tvapi/common/vo/EnumSleepTimeState;

.field private offTime:Landroid/text/format/Time;

.field private onTime:Landroid/text/format/Time;

.field private powerOnConfig:Lcom/mstar/android/tvapi/common/vo/OnTimeTvDescriptor;

.field private timerPowerOffModeStatus:Lcom/mstar/android/tvapi/common/vo/TimerPowerOffModeStatus;

.field private timerPowerOn:Lcom/mstar/android/tvapi/common/vo/TimerPowerOn;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    const/4 v0, 0x0

    sput v0, Lcom/android/server/tv/TvTimerClient;->powerdowntimes:I

    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Lcom/android/server/tv/TvHanlder;)V
    .locals 1
    .param p1    # Landroid/content/Context;
    .param p2    # Lcom/android/server/tv/TvHanlder;

    invoke-direct {p0}, Lcom/mstar/android/tv/ITvTimer$Stub;-><init>()V

    sget-object v0, Lcom/mstar/android/tvapi/common/vo/EnumSleepTimeState;->E_OFF:Lcom/mstar/android/tvapi/common/vo/EnumSleepTimeState;

    iput-object v0, p0, Lcom/android/server/tv/TvTimerClient;->eSleepMode:Lcom/mstar/android/tvapi/common/vo/EnumSleepTimeState;

    iput-object p1, p0, Lcom/android/server/tv/TvTimerClient;->bossContext:Landroid/content/Context;

    invoke-virtual {p0}, Lcom/android/server/tv/TvTimerClient;->initParams()Z

    return-void
.end method


# virtual methods
.method public getClockOffset()I
    .locals 5
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Landroid/os/RemoteException;
        }
    .end annotation

    const/4 v1, -0x1

    :try_start_0
    invoke-static {}, Lcom/mstar/android/tvapi/common/TvManager;->getInstance()Lcom/mstar/android/tvapi/common/TvManager;

    move-result-object v2

    if-eqz v2, :cond_0

    invoke-static {}, Lcom/mstar/android/tvapi/common/TvManager;->getInstance()Lcom/mstar/android/tvapi/common/TvManager;

    move-result-object v2

    invoke-virtual {v2}, Lcom/mstar/android/tvapi/common/TvManager;->getTimerManager()Lcom/mstar/android/tvapi/common/TimerManager;

    move-result-object v2

    invoke-virtual {v2}, Lcom/mstar/android/tvapi/common/TimerManager;->getClockOffset()I
    :try_end_0
    .catch Lcom/mstar/android/tvapi/common/exception/TvCommonException; {:try_start_0 .. :try_end_0} :catch_0

    move-result v1

    :cond_0
    :goto_0
    const-string v2, "TvTimerClient"

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string v4, "getRtcClock, return int "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v2, v3}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    return v1

    :catch_0
    move-exception v0

    invoke-virtual {v0}, Lcom/mstar/android/tvapi/common/exception/TvCommonException;->printStackTrace()V

    goto :goto_0
.end method

.method public getCurTimer()Lcom/mstar/android/tvapi/common/vo/StandardTime;
    .locals 5
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Landroid/os/RemoteException;
        }
    .end annotation

    :try_start_0
    invoke-static {}, Lcom/mstar/android/tvapi/common/TvManager;->getInstance()Lcom/mstar/android/tvapi/common/TvManager;

    move-result-object v2

    if-eqz v2, :cond_0

    invoke-static {}, Lcom/mstar/android/tvapi/common/TvManager;->getInstance()Lcom/mstar/android/tvapi/common/TvManager;

    move-result-object v2

    invoke-virtual {v2}, Lcom/mstar/android/tvapi/common/TvManager;->getTimerManager()Lcom/mstar/android/tvapi/common/TimerManager;

    move-result-object v2

    invoke-virtual {v2}, Lcom/mstar/android/tvapi/common/TimerManager;->getClkTime()Landroid/text/format/Time;

    move-result-object v2

    iput-object v2, p0, Lcom/android/server/tv/TvTimerClient;->curTime:Landroid/text/format/Time;
    :try_end_0
    .catch Lcom/mstar/android/tvapi/common/exception/TvCommonException; {:try_start_0 .. :try_end_0} :catch_0

    :goto_0
    new-instance v0, Lcom/mstar/android/tvapi/common/vo/StandardTime;

    invoke-direct {v0}, Lcom/mstar/android/tvapi/common/vo/StandardTime;-><init>()V

    iget-object v2, p0, Lcom/android/server/tv/TvTimerClient;->curTime:Landroid/text/format/Time;

    iget v2, v2, Landroid/text/format/Time;->year:I

    add-int/lit16 v2, v2, 0x76c

    iput v2, v0, Lcom/mstar/android/tvapi/common/vo/StandardTime;->year:I

    iget-object v2, p0, Lcom/android/server/tv/TvTimerClient;->curTime:Landroid/text/format/Time;

    iget v2, v2, Landroid/text/format/Time;->month:I

    add-int/lit8 v2, v2, 0x1

    int-to-short v2, v2

    iput v2, v0, Lcom/mstar/android/tvapi/common/vo/StandardTime;->month:I

    iget-object v2, p0, Lcom/android/server/tv/TvTimerClient;->curTime:Landroid/text/format/Time;

    iget v2, v2, Landroid/text/format/Time;->hour:I

    iput v2, v0, Lcom/mstar/android/tvapi/common/vo/StandardTime;->hour:I

    iget-object v2, p0, Lcom/android/server/tv/TvTimerClient;->curTime:Landroid/text/format/Time;

    iget v2, v2, Landroid/text/format/Time;->minute:I

    iput v2, v0, Lcom/mstar/android/tvapi/common/vo/StandardTime;->minute:I

    iget-object v2, p0, Lcom/android/server/tv/TvTimerClient;->curTime:Landroid/text/format/Time;

    iget v2, v2, Landroid/text/format/Time;->second:I

    iput v2, v0, Lcom/mstar/android/tvapi/common/vo/StandardTime;->second:I

    iget-object v2, p0, Lcom/android/server/tv/TvTimerClient;->curTime:Landroid/text/format/Time;

    iget v2, v2, Landroid/text/format/Time;->monthDay:I

    iput v2, v0, Lcom/mstar/android/tvapi/common/vo/StandardTime;->monthDay:I

    const-string v2, "TvTimerClient"

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string v4, "getCurTimer:"

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    iget-object v4, p0, Lcom/android/server/tv/TvTimerClient;->curTime:Landroid/text/format/Time;

    iget v4, v4, Landroid/text/format/Time;->year:I

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v3

    const-string v4, "."

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    iget-object v4, p0, Lcom/android/server/tv/TvTimerClient;->curTime:Landroid/text/format/Time;

    iget v4, v4, Landroid/text/format/Time;->month:I

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v3

    const-string v4, "."

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    iget-object v4, p0, Lcom/android/server/tv/TvTimerClient;->curTime:Landroid/text/format/Time;

    iget v4, v4, Landroid/text/format/Time;->monthDay:I

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v3

    const-string v4, "."

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    iget-object v4, p0, Lcom/android/server/tv/TvTimerClient;->curTime:Landroid/text/format/Time;

    iget v4, v4, Landroid/text/format/Time;->hour:I

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v3

    const-string v4, "."

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    iget-object v4, p0, Lcom/android/server/tv/TvTimerClient;->curTime:Landroid/text/format/Time;

    iget v4, v4, Landroid/text/format/Time;->minute:I

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v3

    const-string v4, "."

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    iget-object v4, p0, Lcom/android/server/tv/TvTimerClient;->curTime:Landroid/text/format/Time;

    iget v4, v4, Landroid/text/format/Time;->second:I

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v2, v3}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    :goto_1
    return-object v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_1

    :catch_0
    move-exception v1

    invoke-virtual {v1}, Lcom/mstar/android/tvapi/common/exception/TvCommonException;->printStackTrace()V

    goto/16 :goto_0
.end method

.method public getOffTimer()Lcom/mstar/android/tvapi/common/vo/StandardTime;
    .locals 4
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Landroid/os/RemoteException;
        }
    .end annotation

    const-string v1, "TvTimerClient"

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "getOnTimer:"

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    iget-object v3, p0, Lcom/android/server/tv/TvTimerClient;->offTime:Landroid/text/format/Time;

    iget v3, v3, Landroid/text/format/Time;->year:I

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, "."

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    iget-object v3, p0, Lcom/android/server/tv/TvTimerClient;->offTime:Landroid/text/format/Time;

    iget v3, v3, Landroid/text/format/Time;->month:I

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, "."

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    iget-object v3, p0, Lcom/android/server/tv/TvTimerClient;->offTime:Landroid/text/format/Time;

    iget v3, v3, Landroid/text/format/Time;->monthDay:I

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, "."

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    iget-object v3, p0, Lcom/android/server/tv/TvTimerClient;->offTime:Landroid/text/format/Time;

    iget v3, v3, Landroid/text/format/Time;->hour:I

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, "."

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    iget-object v3, p0, Lcom/android/server/tv/TvTimerClient;->offTime:Landroid/text/format/Time;

    iget v3, v3, Landroid/text/format/Time;->minute:I

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, "."

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    iget-object v3, p0, Lcom/android/server/tv/TvTimerClient;->offTime:Landroid/text/format/Time;

    iget v3, v3, Landroid/text/format/Time;->second:I

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    new-instance v0, Lcom/mstar/android/tvapi/common/vo/StandardTime;

    invoke-direct {v0}, Lcom/mstar/android/tvapi/common/vo/StandardTime;-><init>()V

    iget-object v1, p0, Lcom/android/server/tv/TvTimerClient;->offTime:Landroid/text/format/Time;

    iget v1, v1, Landroid/text/format/Time;->monthDay:I

    int-to-short v1, v1

    iput v1, v0, Lcom/mstar/android/tvapi/common/vo/StandardTime;->monthDay:I

    iget-object v1, p0, Lcom/android/server/tv/TvTimerClient;->offTime:Landroid/text/format/Time;

    iget v1, v1, Landroid/text/format/Time;->month:I

    int-to-short v1, v1

    iput v1, v0, Lcom/mstar/android/tvapi/common/vo/StandardTime;->month:I

    iget-object v1, p0, Lcom/android/server/tv/TvTimerClient;->offTime:Landroid/text/format/Time;

    iget v1, v1, Landroid/text/format/Time;->year:I

    iput v1, v0, Lcom/mstar/android/tvapi/common/vo/StandardTime;->year:I

    iget-object v1, p0, Lcom/android/server/tv/TvTimerClient;->offTime:Landroid/text/format/Time;

    iget v1, v1, Landroid/text/format/Time;->hour:I

    int-to-short v1, v1

    iput v1, v0, Lcom/mstar/android/tvapi/common/vo/StandardTime;->hour:I

    iget-object v1, p0, Lcom/android/server/tv/TvTimerClient;->offTime:Landroid/text/format/Time;

    iget v1, v1, Landroid/text/format/Time;->minute:I

    int-to-short v1, v1

    iput v1, v0, Lcom/mstar/android/tvapi/common/vo/StandardTime;->minute:I

    iget-object v1, p0, Lcom/android/server/tv/TvTimerClient;->offTime:Landroid/text/format/Time;

    iget v1, v1, Landroid/text/format/Time;->second:I

    int-to-short v1, v1

    iput v1, v0, Lcom/mstar/android/tvapi/common/vo/StandardTime;->second:I

    const-string v1, "TvTimerClient"

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "getOffTimer:"

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    iget v3, v0, Lcom/mstar/android/tvapi/common/vo/StandardTime;->year:I

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, "."

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    iget v3, v0, Lcom/mstar/android/tvapi/common/vo/StandardTime;->month:I

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, "."

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    iget v3, v0, Lcom/mstar/android/tvapi/common/vo/StandardTime;->monthDay:I

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, "."

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    iget v3, v0, Lcom/mstar/android/tvapi/common/vo/StandardTime;->hour:I

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, "."

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    iget v3, v0, Lcom/mstar/android/tvapi/common/vo/StandardTime;->minute:I

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, "."

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    iget v3, v0, Lcom/mstar/android/tvapi/common/vo/StandardTime;->second:I

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    return-object v0
.end method

.method public getOnTimeEvent()Lcom/mstar/android/tvapi/common/vo/OnTimeTvDescriptor;
    .locals 3
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Landroid/os/RemoteException;
        }
    .end annotation

    const-string v0, "TvTimerClient"

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "setOnTimeEvent, return OnTimepowerOnConfigDescriptor mChNo = "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget-object v2, p0, Lcom/android/server/tv/TvTimerClient;->powerOnConfig:Lcom/mstar/android/tvapi/common/vo/OnTimeTvDescriptor;

    iget v2, v2, Lcom/mstar/android/tvapi/common/vo/OnTimeTvDescriptor;->mChNo:I

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, ", mVol = "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget-object v2, p0, Lcom/android/server/tv/TvTimerClient;->powerOnConfig:Lcom/mstar/android/tvapi/common/vo/OnTimeTvDescriptor;

    iget-short v2, v2, Lcom/mstar/android/tvapi/common/vo/OnTimeTvDescriptor;->mVol:S

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, ", enTVSrc = "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget-object v2, p0, Lcom/android/server/tv/TvTimerClient;->powerOnConfig:Lcom/mstar/android/tvapi/common/vo/OnTimeTvDescriptor;

    iget-object v2, v2, Lcom/mstar/android/tvapi/common/vo/OnTimeTvDescriptor;->enTVSrc:Lcom/mstar/android/tvapi/common/vo/EnumTimeOnTimerSource;

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    iget-object v0, p0, Lcom/android/server/tv/TvTimerClient;->powerOnConfig:Lcom/mstar/android/tvapi/common/vo/OnTimeTvDescriptor;

    return-object v0
.end method

.method public getOnTimer()Lcom/mstar/android/tvapi/common/vo/StandardTime;
    .locals 4
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Landroid/os/RemoteException;
        }
    .end annotation

    const-string v1, "TvTimerClient"

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "getOnTimer:"

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    iget-object v3, p0, Lcom/android/server/tv/TvTimerClient;->onTime:Landroid/text/format/Time;

    iget v3, v3, Landroid/text/format/Time;->year:I

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, "."

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    iget-object v3, p0, Lcom/android/server/tv/TvTimerClient;->onTime:Landroid/text/format/Time;

    iget v3, v3, Landroid/text/format/Time;->month:I

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, "."

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    iget-object v3, p0, Lcom/android/server/tv/TvTimerClient;->onTime:Landroid/text/format/Time;

    iget v3, v3, Landroid/text/format/Time;->monthDay:I

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, "."

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    iget-object v3, p0, Lcom/android/server/tv/TvTimerClient;->onTime:Landroid/text/format/Time;

    iget v3, v3, Landroid/text/format/Time;->hour:I

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, "."

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    iget-object v3, p0, Lcom/android/server/tv/TvTimerClient;->onTime:Landroid/text/format/Time;

    iget v3, v3, Landroid/text/format/Time;->minute:I

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, "."

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    iget-object v3, p0, Lcom/android/server/tv/TvTimerClient;->onTime:Landroid/text/format/Time;

    iget v3, v3, Landroid/text/format/Time;->second:I

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    new-instance v0, Lcom/mstar/android/tvapi/common/vo/StandardTime;

    invoke-direct {v0}, Lcom/mstar/android/tvapi/common/vo/StandardTime;-><init>()V

    iget-object v1, p0, Lcom/android/server/tv/TvTimerClient;->onTime:Landroid/text/format/Time;

    iget v1, v1, Landroid/text/format/Time;->monthDay:I

    int-to-short v1, v1

    iput v1, v0, Lcom/mstar/android/tvapi/common/vo/StandardTime;->monthDay:I

    iget-object v1, p0, Lcom/android/server/tv/TvTimerClient;->onTime:Landroid/text/format/Time;

    iget v1, v1, Landroid/text/format/Time;->month:I

    int-to-short v1, v1

    iput v1, v0, Lcom/mstar/android/tvapi/common/vo/StandardTime;->month:I

    iget-object v1, p0, Lcom/android/server/tv/TvTimerClient;->onTime:Landroid/text/format/Time;

    iget v1, v1, Landroid/text/format/Time;->year:I

    iput v1, v0, Lcom/mstar/android/tvapi/common/vo/StandardTime;->year:I

    iget-object v1, p0, Lcom/android/server/tv/TvTimerClient;->onTime:Landroid/text/format/Time;

    iget v1, v1, Landroid/text/format/Time;->hour:I

    int-to-short v1, v1

    iput v1, v0, Lcom/mstar/android/tvapi/common/vo/StandardTime;->hour:I

    iget-object v1, p0, Lcom/android/server/tv/TvTimerClient;->onTime:Landroid/text/format/Time;

    iget v1, v1, Landroid/text/format/Time;->minute:I

    int-to-short v1, v1

    iput v1, v0, Lcom/mstar/android/tvapi/common/vo/StandardTime;->minute:I

    iget-object v1, p0, Lcom/android/server/tv/TvTimerClient;->onTime:Landroid/text/format/Time;

    iget v1, v1, Landroid/text/format/Time;->second:I

    int-to-short v1, v1

    iput v1, v0, Lcom/mstar/android/tvapi/common/vo/StandardTime;->second:I

    return-object v0
.end method

.method public getRtcClock()I
    .locals 5
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Landroid/os/RemoteException;
        }
    .end annotation

    const/4 v1, -0x1

    :try_start_0
    invoke-static {}, Lcom/mstar/android/tvapi/common/TvManager;->getInstance()Lcom/mstar/android/tvapi/common/TvManager;

    move-result-object v2

    if-eqz v2, :cond_0

    invoke-static {}, Lcom/mstar/android/tvapi/common/TvManager;->getInstance()Lcom/mstar/android/tvapi/common/TvManager;

    move-result-object v2

    invoke-virtual {v2}, Lcom/mstar/android/tvapi/common/TvManager;->getTimerManager()Lcom/mstar/android/tvapi/common/TimerManager;

    move-result-object v2

    invoke-virtual {v2}, Lcom/mstar/android/tvapi/common/TimerManager;->getRtcClock()I
    :try_end_0
    .catch Lcom/mstar/android/tvapi/common/exception/TvCommonException; {:try_start_0 .. :try_end_0} :catch_0

    move-result v1

    :cond_0
    :goto_0
    const-string v2, "TvTimerClient"

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string v4, "getRtcClock, return int "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v2, v3}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    return v1

    :catch_0
    move-exception v0

    invoke-virtual {v0}, Lcom/mstar/android/tvapi/common/exception/TvCommonException;->printStackTrace()V

    goto :goto_0
.end method

.method public getSleepMode()I
    .locals 4
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Landroid/os/RemoteException;
        }
    .end annotation

    iget-object v1, p0, Lcom/android/server/tv/TvTimerClient;->eSleepMode:Lcom/mstar/android/tvapi/common/vo/EnumSleepTimeState;

    invoke-virtual {v1}, Lcom/mstar/android/tvapi/common/vo/EnumSleepTimeState;->ordinal()I

    move-result v0

    const-string v1, "TvTimerClient"

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "getSleepMode, return int "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    return v0
.end method

.method public getTimeZone()I
    .locals 6
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Landroid/os/RemoteException;
        }
    .end annotation

    const/4 v1, 0x0

    const/4 v2, -0x1

    :try_start_0
    invoke-static {}, Lcom/mstar/android/tvapi/common/TvManager;->getInstance()Lcom/mstar/android/tvapi/common/TvManager;

    move-result-object v3

    if-eqz v3, :cond_0

    invoke-static {}, Lcom/mstar/android/tvapi/common/TvManager;->getInstance()Lcom/mstar/android/tvapi/common/TvManager;

    move-result-object v3

    invoke-virtual {v3}, Lcom/mstar/android/tvapi/common/TvManager;->getTimerManager()Lcom/mstar/android/tvapi/common/TimerManager;

    move-result-object v3

    invoke-virtual {v3}, Lcom/mstar/android/tvapi/common/TimerManager;->getTimeZone()Lcom/mstar/android/tvapi/common/vo/TvOsType$EnumTimeZone;
    :try_end_0
    .catch Lcom/mstar/android/tvapi/common/exception/TvCommonException; {:try_start_0 .. :try_end_0} :catch_0

    move-result-object v1

    :cond_0
    :goto_0
    invoke-virtual {v1}, Lcom/mstar/android/tvapi/common/vo/TvOsType$EnumTimeZone;->getValue()I

    move-result v2

    const-string v3, "TvTimerClient"

    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    const-string v5, "getTimeZone, return int "

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-static {v3, v4}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    return v2

    :catch_0
    move-exception v0

    invoke-virtual {v0}, Lcom/mstar/android/tvapi/common/exception/TvCommonException;->printStackTrace()V

    goto :goto_0
.end method

.method public initParams()Z
    .locals 6

    const/4 v1, 0x0

    const-string v2, "TvTimerClient"

    const-string v3, "initParams"

    invoke-static {v2, v3}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    :try_start_0
    invoke-static {}, Lcom/mstar/android/tvapi/common/TvManager;->getInstance()Lcom/mstar/android/tvapi/common/TvManager;

    move-result-object v2

    if-eqz v2, :cond_0

    invoke-static {}, Lcom/mstar/android/tvapi/common/TvManager;->getInstance()Lcom/mstar/android/tvapi/common/TvManager;

    move-result-object v2

    invoke-virtual {v2}, Lcom/mstar/android/tvapi/common/TvManager;->getTimerManager()Lcom/mstar/android/tvapi/common/TimerManager;

    move-result-object v2

    invoke-virtual {v2}, Lcom/mstar/android/tvapi/common/TimerManager;->getOnTime()Lcom/mstar/android/tvapi/common/vo/TimerPowerOn;

    move-result-object v2

    iput-object v2, p0, Lcom/android/server/tv/TvTimerClient;->timerPowerOn:Lcom/mstar/android/tvapi/common/vo/TimerPowerOn;

    invoke-static {}, Lcom/mstar/android/tvapi/common/TvManager;->getInstance()Lcom/mstar/android/tvapi/common/TvManager;

    move-result-object v2

    invoke-virtual {v2}, Lcom/mstar/android/tvapi/common/TvManager;->getTimerManager()Lcom/mstar/android/tvapi/common/TimerManager;

    move-result-object v2

    invoke-virtual {v2}, Lcom/mstar/android/tvapi/common/TimerManager;->getOffModeStatus()Lcom/mstar/android/tvapi/common/vo/TimerPowerOffModeStatus;

    move-result-object v2

    iput-object v2, p0, Lcom/android/server/tv/TvTimerClient;->timerPowerOffModeStatus:Lcom/mstar/android/tvapi/common/vo/TimerPowerOffModeStatus;

    invoke-static {}, Lcom/mstar/android/tvapi/common/TvManager;->getInstance()Lcom/mstar/android/tvapi/common/TvManager;

    move-result-object v2

    invoke-virtual {v2}, Lcom/mstar/android/tvapi/common/TvManager;->getTimerManager()Lcom/mstar/android/tvapi/common/TimerManager;

    move-result-object v2

    invoke-virtual {v2}, Lcom/mstar/android/tvapi/common/TimerManager;->getSleeperState()Lcom/mstar/android/tvapi/common/vo/EnumSleepTimeState;

    move-result-object v2

    iput-object v2, p0, Lcom/android/server/tv/TvTimerClient;->enSleepTimeState:Lcom/mstar/android/tvapi/common/vo/EnumSleepTimeState;
    :try_end_0
    .catch Lcom/mstar/android/tvapi/common/exception/TvCommonException; {:try_start_0 .. :try_end_0} :catch_0

    :goto_0
    new-instance v2, Landroid/text/format/Time;

    invoke-direct {v2}, Landroid/text/format/Time;-><init>()V

    iput-object v2, p0, Lcom/android/server/tv/TvTimerClient;->curTime:Landroid/text/format/Time;

    new-instance v2, Landroid/text/format/Time;

    invoke-direct {v2}, Landroid/text/format/Time;-><init>()V

    iput-object v2, p0, Lcom/android/server/tv/TvTimerClient;->offTime:Landroid/text/format/Time;

    iget-object v2, p0, Lcom/android/server/tv/TvTimerClient;->offTime:Landroid/text/format/Time;

    iget-object v3, p0, Lcom/android/server/tv/TvTimerClient;->timerPowerOffModeStatus:Lcom/mstar/android/tvapi/common/vo/TimerPowerOffModeStatus;

    invoke-virtual {v2, v3}, Landroid/text/format/Time;->set(Landroid/text/format/Time;)V

    iget-object v2, p0, Lcom/android/server/tv/TvTimerClient;->offTime:Landroid/text/format/Time;

    iget v3, v2, Landroid/text/format/Time;->year:I

    add-int/lit16 v3, v3, 0x76c

    iput v3, v2, Landroid/text/format/Time;->year:I

    iget-object v2, p0, Lcom/android/server/tv/TvTimerClient;->offTime:Landroid/text/format/Time;

    iget v3, v2, Landroid/text/format/Time;->month:I

    add-int/lit8 v3, v3, 0x1

    iput v3, v2, Landroid/text/format/Time;->month:I

    new-instance v2, Landroid/text/format/Time;

    invoke-direct {v2}, Landroid/text/format/Time;-><init>()V

    iput-object v2, p0, Lcom/android/server/tv/TvTimerClient;->onTime:Landroid/text/format/Time;

    iget-object v2, p0, Lcom/android/server/tv/TvTimerClient;->offTime:Landroid/text/format/Time;

    iget-object v3, p0, Lcom/android/server/tv/TvTimerClient;->timerPowerOn:Lcom/mstar/android/tvapi/common/vo/TimerPowerOn;

    invoke-virtual {v2, v3}, Landroid/text/format/Time;->set(Landroid/text/format/Time;)V

    iget-object v2, p0, Lcom/android/server/tv/TvTimerClient;->onTime:Landroid/text/format/Time;

    iget v3, v2, Landroid/text/format/Time;->year:I

    add-int/lit16 v3, v3, 0x76c

    iput v3, v2, Landroid/text/format/Time;->year:I

    iget-object v2, p0, Lcom/android/server/tv/TvTimerClient;->onTime:Landroid/text/format/Time;

    iget v3, v2, Landroid/text/format/Time;->month:I

    add-int/lit8 v3, v3, 0x1

    iput v3, v2, Landroid/text/format/Time;->month:I

    new-instance v2, Lcom/mstar/android/tvapi/common/vo/OnTimeTvDescriptor;

    invoke-static {}, Lcom/mstar/android/tvapi/common/vo/EnumTimeOnTimerSource;->values()[Lcom/mstar/android/tvapi/common/vo/EnumTimeOnTimerSource;

    move-result-object v3

    iget-object v4, p0, Lcom/android/server/tv/TvTimerClient;->timerPowerOn:Lcom/mstar/android/tvapi/common/vo/TimerPowerOn;

    invoke-virtual {v4}, Lcom/mstar/android/tvapi/common/vo/TimerPowerOn;->getTvSource()Lcom/mstar/android/tvapi/common/vo/EnumTimeOnTimerSource;

    move-result-object v4

    invoke-virtual {v4}, Lcom/mstar/android/tvapi/common/vo/EnumTimeOnTimerSource;->ordinal()I

    move-result v4

    aget-object v3, v3, v4

    iget-object v4, p0, Lcom/android/server/tv/TvTimerClient;->timerPowerOn:Lcom/mstar/android/tvapi/common/vo/TimerPowerOn;

    iget v4, v4, Lcom/mstar/android/tvapi/common/vo/TimerPowerOn;->channelNumber:I

    iget-object v5, p0, Lcom/android/server/tv/TvTimerClient;->timerPowerOn:Lcom/mstar/android/tvapi/common/vo/TimerPowerOn;

    iget-short v5, v5, Lcom/mstar/android/tvapi/common/vo/TimerPowerOn;->volume:S

    invoke-direct {v2, v3, v4, v5}, Lcom/mstar/android/tvapi/common/vo/OnTimeTvDescriptor;-><init>(Lcom/mstar/android/tvapi/common/vo/EnumTimeOnTimerSource;IS)V

    iput-object v2, p0, Lcom/android/server/tv/TvTimerClient;->powerOnConfig:Lcom/mstar/android/tvapi/common/vo/OnTimeTvDescriptor;

    invoke-static {}, Lcom/mstar/android/tvapi/common/vo/EnumSleepTimeState;->values()[Lcom/mstar/android/tvapi/common/vo/EnumSleepTimeState;

    move-result-object v2

    iget-object v3, p0, Lcom/android/server/tv/TvTimerClient;->enSleepTimeState:Lcom/mstar/android/tvapi/common/vo/EnumSleepTimeState;

    invoke-virtual {v3}, Lcom/mstar/android/tvapi/common/vo/EnumSleepTimeState;->ordinal()I

    move-result v3

    aget-object v2, v2, v3

    iput-object v2, p0, Lcom/android/server/tv/TvTimerClient;->eSleepMode:Lcom/mstar/android/tvapi/common/vo/EnumSleepTimeState;

    iput-boolean v1, p0, Lcom/android/server/tv/TvTimerClient;->bOffTimerFlag:Z

    iput-boolean v1, p0, Lcom/android/server/tv/TvTimerClient;->bOnTimerFlag:Z

    sput v1, Lcom/android/server/tv/TvTimerClient;->powerdowntimes:I

    invoke-static {}, Lcom/mstar/android/tvapi/common/TvManager;->getInstance()Lcom/mstar/android/tvapi/common/TvManager;

    move-result-object v2

    if-eqz v2, :cond_0

    invoke-static {}, Lcom/mstar/android/tvapi/common/TvManager;->getInstance()Lcom/mstar/android/tvapi/common/TvManager;

    move-result-object v1

    invoke-virtual {v1}, Lcom/mstar/android/tvapi/common/TvManager;->getTimerManager()Lcom/mstar/android/tvapi/common/TimerManager;

    move-result-object v1

    invoke-virtual {v1, p0}, Lcom/mstar/android/tvapi/common/TimerManager;->setOnTimerEventListener(Lcom/mstar/android/tvapi/common/TimerManager$OnTimerEventListener;)V

    const/4 v1, 0x1

    :cond_0
    return v1

    :catch_0
    move-exception v0

    invoke-virtual {v0}, Lcom/mstar/android/tvapi/common/exception/TvCommonException;->printStackTrace()V

    goto/16 :goto_0
.end method

.method public isOffTimerEnable()Z
    .locals 2
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Landroid/os/RemoteException;
        }
    .end annotation

    const-string v0, "TvTimerClient"

    const-string v1, "isOffTimerEnable"

    invoke-static {v0, v1}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    iget-boolean v0, p0, Lcom/android/server/tv/TvTimerClient;->bOffTimerFlag:Z

    return v0
.end method

.method public isOnTimerEnable()Z
    .locals 2
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Landroid/os/RemoteException;
        }
    .end annotation

    const-string v0, "TvTimerClient"

    const-string v1, "isOnTimerEnable"

    invoke-static {v0, v1}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    iget-boolean v0, p0, Lcom/android/server/tv/TvTimerClient;->bOnTimerFlag:Z

    return v0
.end method

.method public onDestroyCountDown(Lcom/mstar/android/tvapi/common/TimerManager;III)Z
    .locals 3
    .param p1    # Lcom/mstar/android/tvapi/common/TimerManager;
    .param p2    # I
    .param p3    # I
    .param p4    # I

    new-instance v0, Landroid/content/Intent;

    invoke-direct {v0}, Landroid/content/Intent;-><init>()V

    const-string v1, "com.android.server.tv.TIME_EVENT_DESTROY_COUNT_DOWN"

    invoke-virtual {v0, v1}, Landroid/content/Intent;->setAction(Ljava/lang/String;)Landroid/content/Intent;

    const-string v1, "com.android.server.tv.TIME_EVENT_DESTROY_COUNT_DOWN"

    const-string v2, "BBBBBBBBBBBBBBBBBBBBBBBBBBB"

    invoke-static {v1, v2}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    iget-object v1, p0, Lcom/android/server/tv/TvTimerClient;->bossContext:Landroid/content/Context;

    sget-object v2, Landroid/os/UserHandle;->CURRENT:Landroid/os/UserHandle;

    invoke-virtual {v1, v0, v2}, Landroid/content/Context;->sendBroadcastAsUser(Landroid/content/Intent;Landroid/os/UserHandle;)V

    const/4 v1, 0x0

    return v1
.end method

.method public onEpgTimeUp(Lcom/mstar/android/tvapi/common/TimerManager;III)Z
    .locals 1
    .param p1    # Lcom/mstar/android/tvapi/common/TimerManager;
    .param p2    # I
    .param p3    # I
    .param p4    # I

    const/4 v0, 0x0

    return v0
.end method

.method public onEpgTimerCountDown(Lcom/mstar/android/tvapi/common/TimerManager;III)Z
    .locals 3
    .param p1    # Lcom/mstar/android/tvapi/common/TimerManager;
    .param p2    # I
    .param p3    # I
    .param p4    # I

    new-instance v0, Landroid/content/Intent;

    invoke-direct {v0}, Landroid/content/Intent;-><init>()V

    const-string v1, "konka.action.EPG_BOOKING_WARNING"

    invoke-virtual {v0, v1}, Landroid/content/Intent;->setAction(Ljava/lang/String;)Landroid/content/Intent;

    const-string v1, "leftTime"

    invoke-virtual {v0, v1, p3}, Landroid/content/Intent;->putExtra(Ljava/lang/String;I)Landroid/content/Intent;

    iget-object v1, p0, Lcom/android/server/tv/TvTimerClient;->bossContext:Landroid/content/Context;

    sget-object v2, Landroid/os/UserHandle;->CURRENT:Landroid/os/UserHandle;

    invoke-virtual {v1, v0, v2}, Landroid/content/Context;->sendBroadcastAsUser(Landroid/content/Intent;Landroid/os/UserHandle;)V

    const-string v1, "com.mstar.tv.service.EPGCOUNTDOWN"

    invoke-virtual {v0, v1}, Landroid/content/Intent;->setAction(Ljava/lang/String;)Landroid/content/Intent;

    const-string v1, "leftTime"

    invoke-virtual {v0, v1, p3}, Landroid/content/Intent;->putExtra(Ljava/lang/String;I)Landroid/content/Intent;

    iget-object v1, p0, Lcom/android/server/tv/TvTimerClient;->bossContext:Landroid/content/Context;

    sget-object v2, Landroid/os/UserHandle;->CURRENT:Landroid/os/UserHandle;

    invoke-virtual {v1, v0, v2}, Landroid/content/Context;->sendBroadcastAsUser(Landroid/content/Intent;Landroid/os/UserHandle;)V

    const-string v1, "com.mstar.tv.service.EPGCOUNTDOWN"

    const-string v2, "onEpgTimerCountDown\n"

    invoke-static {v1, v2}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    const/4 v1, 0x0

    return v1
.end method

.method public onEpgTimerRecordStart(Lcom/mstar/android/tvapi/common/TimerManager;III)Z
    .locals 3
    .param p1    # Lcom/mstar/android/tvapi/common/TimerManager;
    .param p2    # I
    .param p3    # I
    .param p4    # I

    new-instance v0, Landroid/content/Intent;

    invoke-direct {v0}, Landroid/content/Intent;-><init>()V

    const-string v1, "com.konka.tvsettings.action.PVR_START_RECORD"

    invoke-virtual {v0, v1}, Landroid/content/Intent;->setAction(Ljava/lang/String;)Landroid/content/Intent;

    iget-object v1, p0, Lcom/android/server/tv/TvTimerClient;->bossContext:Landroid/content/Context;

    sget-object v2, Landroid/os/UserHandle;->CURRENT:Landroid/os/UserHandle;

    invoke-virtual {v1, v0, v2}, Landroid/content/Context;->sendBroadcastAsUser(Landroid/content/Intent;Landroid/os/UserHandle;)V

    const-string v1, "com.konka.tvsettings.action.PVR_START_RECORD"

    const-string v2, "onEpgTimerRecordStart\n"

    invoke-static {v1, v2}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    const/4 v1, 0x0

    return v1
.end method

.method public onLastMinuteWarn(Lcom/mstar/android/tvapi/common/TimerManager;III)Z
    .locals 3
    .param p1    # Lcom/mstar/android/tvapi/common/TimerManager;
    .param p2    # I
    .param p3    # I
    .param p4    # I

    new-instance v0, Landroid/content/Intent;

    invoke-direct {v0}, Landroid/content/Intent;-><init>()V

    const-string v1, "com.android.server.tv.TIME_EVENT_LAST_MINUTE_WARN"

    invoke-virtual {v0, v1}, Landroid/content/Intent;->setAction(Ljava/lang/String;)Landroid/content/Intent;

    const-string v1, "com.android.server.tv.TIME_EVENT_LAST_MINUTE_WARN"

    const-string v2, "BBBBBBBBBBBBBBBBBBBBBBBBBBB"

    invoke-static {v1, v2}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    iget-object v1, p0, Lcom/android/server/tv/TvTimerClient;->bossContext:Landroid/content/Context;

    sget-object v2, Landroid/os/UserHandle;->CURRENT:Landroid/os/UserHandle;

    invoke-virtual {v1, v0, v2}, Landroid/content/Context;->sendBroadcastAsUser(Landroid/content/Intent;Landroid/os/UserHandle;)V

    const-string v1, "konka.action.TIMING_OFF_WARNING"

    invoke-virtual {v0, v1}, Landroid/content/Intent;->setAction(Ljava/lang/String;)Landroid/content/Intent;

    iget-object v1, p0, Lcom/android/server/tv/TvTimerClient;->bossContext:Landroid/content/Context;

    sget-object v2, Landroid/os/UserHandle;->CURRENT:Landroid/os/UserHandle;

    invoke-virtual {v1, v0, v2}, Landroid/content/Context;->sendBroadcastAsUser(Landroid/content/Intent;Landroid/os/UserHandle;)V

    const-string v1, "com.konka.tv.hotkey.TIMER_OFF_WARNING"

    invoke-virtual {v0, v1}, Landroid/content/Intent;->setAction(Ljava/lang/String;)Landroid/content/Intent;

    iget-object v1, p0, Lcom/android/server/tv/TvTimerClient;->bossContext:Landroid/content/Context;

    sget-object v2, Landroid/os/UserHandle;->CURRENT:Landroid/os/UserHandle;

    invoke-virtual {v1, v0, v2}, Landroid/content/Context;->sendBroadcastAsUser(Landroid/content/Intent;Landroid/os/UserHandle;)V

    const/4 v1, 0x0

    return v1
.end method

.method public onOadTimeScan(Lcom/mstar/android/tvapi/common/TimerManager;III)Z
    .locals 1
    .param p1    # Lcom/mstar/android/tvapi/common/TimerManager;
    .param p2    # I
    .param p3    # I
    .param p4    # I

    const/4 v0, 0x0

    return v0
.end method

.method public onOneSecondBeat(Lcom/mstar/android/tvapi/common/TimerManager;III)Z
    .locals 1
    .param p1    # Lcom/mstar/android/tvapi/common/TimerManager;
    .param p2    # I
    .param p3    # I
    .param p4    # I

    const/4 v0, 0x0

    return v0
.end method

.method public onPowerDownTime(Lcom/mstar/android/tvapi/common/TimerManager;III)Z
    .locals 5
    .param p1    # Lcom/mstar/android/tvapi/common/TimerManager;
    .param p2    # I
    .param p3    # I
    .param p4    # I

    const/4 v4, 0x0

    const-string v2, "konka.sales.area"

    const-string v3, "China"

    invoke-static {v2, v3}, Landroid/os/SystemProperties;->get(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v2

    const-string v3, "China"

    invoke-virtual {v2, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_1

    :cond_0
    :goto_0
    return v4

    :cond_1
    const-string v2, "mstar.str.enable"

    invoke-static {v2, v4}, Landroid/os/SystemProperties;->getBoolean(Ljava/lang/String;Z)Z

    move-result v0

    const/4 v2, 0x1

    if-ne v0, v2, :cond_2

    new-instance v1, Lcom/android/server/tv/TvTimerClient$1;

    invoke-direct {v1, p0}, Lcom/android/server/tv/TvTimerClient$1;-><init>(Lcom/android/server/tv/TvTimerClient;)V

    invoke-virtual {v1}, Ljava/lang/Thread;->start()V

    :cond_2
    sget v2, Lcom/android/server/tv/TvTimerClient;->powerdowntimes:I

    add-int/lit8 v2, v2, 0x1

    sput v2, Lcom/android/server/tv/TvTimerClient;->powerdowntimes:I

    sget v2, Lcom/android/server/tv/TvTimerClient;->powerdowntimes:I

    const/16 v3, 0xa

    if-le v2, v3, :cond_0

    sput v4, Lcom/android/server/tv/TvTimerClient;->powerdowntimes:I

    goto :goto_0
.end method

.method public onPvrNotifyRecordStop(Lcom/mstar/android/tvapi/common/TimerManager;III)Z
    .locals 3
    .param p1    # Lcom/mstar/android/tvapi/common/TimerManager;
    .param p2    # I
    .param p3    # I
    .param p4    # I

    new-instance v0, Landroid/content/Intent;

    invoke-direct {v0}, Landroid/content/Intent;-><init>()V

    const-string v1, "com.konka.tvsettings.action.PVR_STOP_RECORD"

    invoke-virtual {v0, v1}, Landroid/content/Intent;->setAction(Ljava/lang/String;)Landroid/content/Intent;

    iget-object v1, p0, Lcom/android/server/tv/TvTimerClient;->bossContext:Landroid/content/Context;

    sget-object v2, Landroid/os/UserHandle;->CURRENT:Landroid/os/UserHandle;

    invoke-virtual {v1, v0, v2}, Landroid/content/Context;->sendBroadcastAsUser(Landroid/content/Intent;Landroid/os/UserHandle;)V

    const-string v1, "com.konka.tvsettings.action.PVR_STOP_RECORD"

    const-string v2, "onEpgTimerRecordStop\n"

    invoke-static {v1, v2}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    const/4 v1, 0x0

    return v1
.end method

.method public onSignalLock(Lcom/mstar/android/tvapi/common/TimerManager;III)Z
    .locals 1
    .param p1    # Lcom/mstar/android/tvapi/common/TimerManager;
    .param p2    # I
    .param p3    # I
    .param p4    # I

    const/4 v0, 0x0

    return v0
.end method

.method public onSystemClkChg(Lcom/mstar/android/tvapi/common/TimerManager;III)Z
    .locals 3
    .param p1    # Lcom/mstar/android/tvapi/common/TimerManager;
    .param p2    # I
    .param p3    # I
    .param p4    # I

    new-instance v0, Landroid/content/Intent;

    invoke-direct {v0}, Landroid/content/Intent;-><init>()V

    const-string v1, "com.android.server.tv.TIME_EVENT_SYSTEM_CLOCK_CHANGE"

    invoke-virtual {v0, v1}, Landroid/content/Intent;->setAction(Ljava/lang/String;)Landroid/content/Intent;

    const-string v1, "com.android.server.tv.TIME_EVENT_SYSTEM_CLOCK_CHANGE"

    const-string v2, "DDDDDDDDDDDDDDDDDDDDDDDDDDDD"

    invoke-static {v1, v2}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    iget-object v1, p0, Lcom/android/server/tv/TvTimerClient;->bossContext:Landroid/content/Context;

    sget-object v2, Landroid/os/UserHandle;->CURRENT:Landroid/os/UserHandle;

    invoke-virtual {v1, v0, v2}, Landroid/content/Context;->sendBroadcastAsUser(Landroid/content/Intent;Landroid/os/UserHandle;)V

    const/4 v1, 0x0

    return v1
.end method

.method public onUpdateLastMinute(Lcom/mstar/android/tvapi/common/TimerManager;III)Z
    .locals 3
    .param p1    # Lcom/mstar/android/tvapi/common/TimerManager;
    .param p2    # I
    .param p3    # I
    .param p4    # I

    new-instance v0, Landroid/content/Intent;

    invoke-direct {v0}, Landroid/content/Intent;-><init>()V

    const-string v1, "com.android.server.tv.TIME_EVENT_LAST_MINUTE_UPDATE"

    invoke-virtual {v0, v1}, Landroid/content/Intent;->setAction(Ljava/lang/String;)Landroid/content/Intent;

    const-string v1, "com.android.server.tv.TIME_EVENT_LAST_MINUTE_UPDATE"

    const-string v2, "BBBBBBBBBBBBBBBBBBBBBBBBBBB"

    invoke-static {v1, v2}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    const-string v1, "LeftTime"

    invoke-virtual {v0, v1, p3}, Landroid/content/Intent;->putExtra(Ljava/lang/String;I)Landroid/content/Intent;

    const-string v1, "OffMode"

    invoke-virtual {v0, v1, p4}, Landroid/content/Intent;->putExtra(Ljava/lang/String;I)Landroid/content/Intent;

    iget-object v1, p0, Lcom/android/server/tv/TvTimerClient;->bossContext:Landroid/content/Context;

    sget-object v2, Landroid/os/UserHandle;->CURRENT:Landroid/os/UserHandle;

    invoke-virtual {v1, v0, v2}, Landroid/content/Context;->sendBroadcastAsUser(Landroid/content/Intent;Landroid/os/UserHandle;)V

    const/4 v1, 0x0

    return v1
.end method

.method public setOffTimer(Lcom/mstar/android/tvapi/common/vo/StandardTime;)Z
    .locals 3
    .param p1    # Lcom/mstar/android/tvapi/common/vo/StandardTime;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Landroid/os/RemoteException;
        }
    .end annotation

    const-string v0, "TvTimerClient"

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "setOnTimer:"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget v2, p1, Lcom/mstar/android/tvapi/common/vo/StandardTime;->year:I

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, "."

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget v2, p1, Lcom/mstar/android/tvapi/common/vo/StandardTime;->month:I

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, "."

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget v2, p1, Lcom/mstar/android/tvapi/common/vo/StandardTime;->monthDay:I

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, "."

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget v2, p1, Lcom/mstar/android/tvapi/common/vo/StandardTime;->hour:I

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, "."

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget v2, p1, Lcom/mstar/android/tvapi/common/vo/StandardTime;->minute:I

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, "."

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget v2, p1, Lcom/mstar/android/tvapi/common/vo/StandardTime;->second:I

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    iget-object v0, p0, Lcom/android/server/tv/TvTimerClient;->offTime:Landroid/text/format/Time;

    iget v1, p1, Lcom/mstar/android/tvapi/common/vo/StandardTime;->monthDay:I

    iput v1, v0, Landroid/text/format/Time;->monthDay:I

    iget-object v0, p0, Lcom/android/server/tv/TvTimerClient;->offTime:Landroid/text/format/Time;

    iget v1, p1, Lcom/mstar/android/tvapi/common/vo/StandardTime;->month:I

    iput v1, v0, Landroid/text/format/Time;->month:I

    iget-object v0, p0, Lcom/android/server/tv/TvTimerClient;->offTime:Landroid/text/format/Time;

    iget v1, p1, Lcom/mstar/android/tvapi/common/vo/StandardTime;->year:I

    iput v1, v0, Landroid/text/format/Time;->year:I

    iget-object v0, p0, Lcom/android/server/tv/TvTimerClient;->offTime:Landroid/text/format/Time;

    iget v1, p1, Lcom/mstar/android/tvapi/common/vo/StandardTime;->hour:I

    iput v1, v0, Landroid/text/format/Time;->hour:I

    iget-object v0, p0, Lcom/android/server/tv/TvTimerClient;->offTime:Landroid/text/format/Time;

    iget v1, p1, Lcom/mstar/android/tvapi/common/vo/StandardTime;->minute:I

    iput v1, v0, Landroid/text/format/Time;->minute:I

    iget-object v0, p0, Lcom/android/server/tv/TvTimerClient;->offTime:Landroid/text/format/Time;

    iget v1, p1, Lcom/mstar/android/tvapi/common/vo/StandardTime;->second:I

    iput v1, v0, Landroid/text/format/Time;->second:I

    const/4 v0, 0x1

    return v0
.end method

.method public setOffTimerEnable(Z)Z
    .locals 5
    .param p1    # Z
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Landroid/os/RemoteException;
        }
    .end annotation

    const/4 v4, 0x1

    iput-boolean p1, p0, Lcom/android/server/tv/TvTimerClient;->bOffTimerFlag:Z

    iget-boolean v1, p0, Lcom/android/server/tv/TvTimerClient;->bOffTimerFlag:Z

    if-ne v4, v1, :cond_1

    iget-object v1, p0, Lcom/android/server/tv/TvTimerClient;->timerPowerOffModeStatus:Lcom/mstar/android/tvapi/common/vo/TimerPowerOffModeStatus;

    iget-object v2, p0, Lcom/android/server/tv/TvTimerClient;->offTime:Landroid/text/format/Time;

    invoke-virtual {v1, v2}, Lcom/mstar/android/tvapi/common/vo/TimerPowerOffModeStatus;->set(Landroid/text/format/Time;)V

    iget-object v1, p0, Lcom/android/server/tv/TvTimerClient;->timerPowerOffModeStatus:Lcom/mstar/android/tvapi/common/vo/TimerPowerOffModeStatus;

    sget-object v2, Lcom/mstar/android/tvapi/common/vo/EnumTimerPeriod;->EN_Timer_Everyday:Lcom/mstar/android/tvapi/common/vo/EnumTimerPeriod;

    invoke-virtual {v1, v2}, Lcom/mstar/android/tvapi/common/vo/TimerPowerOffModeStatus;->setTimerPeriod(Lcom/mstar/android/tvapi/common/vo/EnumTimerPeriod;)V

    const-string v1, "TvTimerClient"

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "setOffTimerEnable:"

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    iget-object v3, p0, Lcom/android/server/tv/TvTimerClient;->offTime:Landroid/text/format/Time;

    iget v3, v3, Landroid/text/format/Time;->year:I

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, "."

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    iget-object v3, p0, Lcom/android/server/tv/TvTimerClient;->offTime:Landroid/text/format/Time;

    iget v3, v3, Landroid/text/format/Time;->month:I

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, "."

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    iget-object v3, p0, Lcom/android/server/tv/TvTimerClient;->offTime:Landroid/text/format/Time;

    iget v3, v3, Landroid/text/format/Time;->monthDay:I

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, "."

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    iget-object v3, p0, Lcom/android/server/tv/TvTimerClient;->offTime:Landroid/text/format/Time;

    iget v3, v3, Landroid/text/format/Time;->hour:I

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, "."

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    iget-object v3, p0, Lcom/android/server/tv/TvTimerClient;->offTime:Landroid/text/format/Time;

    iget v3, v3, Landroid/text/format/Time;->minute:I

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, "."

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    iget-object v3, p0, Lcom/android/server/tv/TvTimerClient;->offTime:Landroid/text/format/Time;

    iget v3, v3, Landroid/text/format/Time;->second:I

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    :try_start_0
    invoke-static {}, Lcom/mstar/android/tvapi/common/TvManager;->getInstance()Lcom/mstar/android/tvapi/common/TvManager;

    move-result-object v1

    if-eqz v1, :cond_0

    invoke-static {}, Lcom/mstar/android/tvapi/common/TvManager;->getInstance()Lcom/mstar/android/tvapi/common/TvManager;

    move-result-object v1

    invoke-virtual {v1}, Lcom/mstar/android/tvapi/common/TvManager;->getTimerManager()Lcom/mstar/android/tvapi/common/TimerManager;

    move-result-object v1

    iget-object v2, p0, Lcom/android/server/tv/TvTimerClient;->timerPowerOffModeStatus:Lcom/mstar/android/tvapi/common/vo/TimerPowerOffModeStatus;

    const/4 v3, 0x1

    invoke-virtual {v1, v2, v3}, Lcom/mstar/android/tvapi/common/TimerManager;->setOffModeStatus(Lcom/mstar/android/tvapi/common/vo/TimerPowerOffModeStatus;Z)V
    :try_end_0
    .catch Lcom/mstar/android/tvapi/common/exception/TvCommonException; {:try_start_0 .. :try_end_0} :catch_0

    :cond_0
    :goto_0
    return v4

    :catch_0
    move-exception v0

    invoke-virtual {v0}, Lcom/mstar/android/tvapi/common/exception/TvCommonException;->printStackTrace()V

    goto :goto_0

    :cond_1
    iget-object v1, p0, Lcom/android/server/tv/TvTimerClient;->timerPowerOffModeStatus:Lcom/mstar/android/tvapi/common/vo/TimerPowerOffModeStatus;

    iget-object v2, p0, Lcom/android/server/tv/TvTimerClient;->offTime:Landroid/text/format/Time;

    invoke-virtual {v1, v2}, Lcom/mstar/android/tvapi/common/vo/TimerPowerOffModeStatus;->set(Landroid/text/format/Time;)V

    iget-object v1, p0, Lcom/android/server/tv/TvTimerClient;->timerPowerOffModeStatus:Lcom/mstar/android/tvapi/common/vo/TimerPowerOffModeStatus;

    sget-object v2, Lcom/mstar/android/tvapi/common/vo/EnumTimerPeriod;->EN_Timer_Off:Lcom/mstar/android/tvapi/common/vo/EnumTimerPeriod;

    invoke-virtual {v1, v2}, Lcom/mstar/android/tvapi/common/vo/TimerPowerOffModeStatus;->setTimerPeriod(Lcom/mstar/android/tvapi/common/vo/EnumTimerPeriod;)V

    const-string v1, "TvTimerClient"

    const-string v2, "setOffTimerEnable:disable"

    invoke-static {v1, v2}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    :try_start_1
    invoke-static {}, Lcom/mstar/android/tvapi/common/TvManager;->getInstance()Lcom/mstar/android/tvapi/common/TvManager;

    move-result-object v1

    if-eqz v1, :cond_0

    invoke-static {}, Lcom/mstar/android/tvapi/common/TvManager;->getInstance()Lcom/mstar/android/tvapi/common/TvManager;

    move-result-object v1

    invoke-virtual {v1}, Lcom/mstar/android/tvapi/common/TvManager;->getTimerManager()Lcom/mstar/android/tvapi/common/TimerManager;

    move-result-object v1

    sget-object v2, Lcom/mstar/android/tvapi/common/vo/EnumOffTimerMode;->OFF_MODE_TIMER:Lcom/mstar/android/tvapi/common/vo/EnumOffTimerMode;

    invoke-virtual {v1, v2}, Lcom/mstar/android/tvapi/common/TimerManager;->disablePowerOffMode(Lcom/mstar/android/tvapi/common/vo/EnumOffTimerMode;)V

    invoke-static {}, Lcom/mstar/android/tvapi/common/TvManager;->getInstance()Lcom/mstar/android/tvapi/common/TvManager;

    move-result-object v1

    invoke-virtual {v1}, Lcom/mstar/android/tvapi/common/TvManager;->getTimerManager()Lcom/mstar/android/tvapi/common/TimerManager;

    move-result-object v1

    iget-object v2, p0, Lcom/android/server/tv/TvTimerClient;->timerPowerOffModeStatus:Lcom/mstar/android/tvapi/common/vo/TimerPowerOffModeStatus;

    const/4 v3, 0x1

    invoke-virtual {v1, v2, v3}, Lcom/mstar/android/tvapi/common/TimerManager;->setOffModeStatus(Lcom/mstar/android/tvapi/common/vo/TimerPowerOffModeStatus;Z)V
    :try_end_1
    .catch Lcom/mstar/android/tvapi/common/exception/TvCommonException; {:try_start_1 .. :try_end_1} :catch_1

    goto :goto_0

    :catch_1
    move-exception v0

    invoke-virtual {v0}, Lcom/mstar/android/tvapi/common/exception/TvCommonException;->printStackTrace()V

    goto :goto_0
.end method

.method public setOnTimeEvent(Lcom/mstar/android/tvapi/common/vo/OnTimeTvDescriptor;)Z
    .locals 3
    .param p1    # Lcom/mstar/android/tvapi/common/vo/OnTimeTvDescriptor;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Landroid/os/RemoteException;
        }
    .end annotation

    const-string v0, "TvTimerClient"

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "setOnTimeEvent, paras OnTimeTvDescriptor stEvent.mChNo = "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget v2, p1, Lcom/mstar/android/tvapi/common/vo/OnTimeTvDescriptor;->mChNo:I

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, ", stEvent.mVol = "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget-short v2, p1, Lcom/mstar/android/tvapi/common/vo/OnTimeTvDescriptor;->mVol:S

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, ", stEvent.enTVSrc = "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget-object v2, p1, Lcom/mstar/android/tvapi/common/vo/OnTimeTvDescriptor;->enTVSrc:Lcom/mstar/android/tvapi/common/vo/EnumTimeOnTimerSource;

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    iput-object p1, p0, Lcom/android/server/tv/TvTimerClient;->powerOnConfig:Lcom/mstar/android/tvapi/common/vo/OnTimeTvDescriptor;

    const/4 v0, 0x1

    return v0
.end method

.method public setOnTimer(Lcom/mstar/android/tvapi/common/vo/StandardTime;)Z
    .locals 3
    .param p1    # Lcom/mstar/android/tvapi/common/vo/StandardTime;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Landroid/os/RemoteException;
        }
    .end annotation

    const-string v0, "TvTimerClient"

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "settimer:"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget v2, p1, Lcom/mstar/android/tvapi/common/vo/StandardTime;->year:I

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, "."

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget v2, p1, Lcom/mstar/android/tvapi/common/vo/StandardTime;->month:I

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, "."

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget v2, p1, Lcom/mstar/android/tvapi/common/vo/StandardTime;->monthDay:I

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, "."

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget v2, p1, Lcom/mstar/android/tvapi/common/vo/StandardTime;->hour:I

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, "."

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget v2, p1, Lcom/mstar/android/tvapi/common/vo/StandardTime;->minute:I

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, "."

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget v2, p1, Lcom/mstar/android/tvapi/common/vo/StandardTime;->second:I

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    iget-object v0, p0, Lcom/android/server/tv/TvTimerClient;->onTime:Landroid/text/format/Time;

    iget v1, p1, Lcom/mstar/android/tvapi/common/vo/StandardTime;->monthDay:I

    iput v1, v0, Landroid/text/format/Time;->monthDay:I

    iget-object v0, p0, Lcom/android/server/tv/TvTimerClient;->onTime:Landroid/text/format/Time;

    iget v1, p1, Lcom/mstar/android/tvapi/common/vo/StandardTime;->month:I

    iput v1, v0, Landroid/text/format/Time;->month:I

    iget-object v0, p0, Lcom/android/server/tv/TvTimerClient;->onTime:Landroid/text/format/Time;

    iget v1, p1, Lcom/mstar/android/tvapi/common/vo/StandardTime;->year:I

    iput v1, v0, Landroid/text/format/Time;->year:I

    iget-object v0, p0, Lcom/android/server/tv/TvTimerClient;->onTime:Landroid/text/format/Time;

    iget v1, p1, Lcom/mstar/android/tvapi/common/vo/StandardTime;->hour:I

    iput v1, v0, Landroid/text/format/Time;->hour:I

    iget-object v0, p0, Lcom/android/server/tv/TvTimerClient;->onTime:Landroid/text/format/Time;

    iget v1, p1, Lcom/mstar/android/tvapi/common/vo/StandardTime;->minute:I

    iput v1, v0, Landroid/text/format/Time;->minute:I

    iget-object v0, p0, Lcom/android/server/tv/TvTimerClient;->onTime:Landroid/text/format/Time;

    iget v1, p1, Lcom/mstar/android/tvapi/common/vo/StandardTime;->second:I

    iput v1, v0, Landroid/text/format/Time;->second:I

    const/4 v0, 0x1

    return v0
.end method

.method public setOnTimerEnable(Z)Z
    .locals 7
    .param p1    # Z
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Landroid/os/RemoteException;
        }
    .end annotation

    const/4 v6, 0x1

    const-string v2, "TvTimerClient"

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string v4, "setOnTimerEnable, paras bEnable is "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3, p1}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v2, v3}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    iput-boolean p1, p0, Lcom/android/server/tv/TvTimerClient;->bOnTimerFlag:Z

    iget-boolean v2, p0, Lcom/android/server/tv/TvTimerClient;->bOnTimerFlag:Z

    if-ne v6, v2, :cond_1

    iget-object v2, p0, Lcom/android/server/tv/TvTimerClient;->timerPowerOn:Lcom/mstar/android/tvapi/common/vo/TimerPowerOn;

    iget-object v3, p0, Lcom/android/server/tv/TvTimerClient;->onTime:Landroid/text/format/Time;

    invoke-virtual {v2, v3}, Lcom/mstar/android/tvapi/common/vo/TimerPowerOn;->set(Landroid/text/format/Time;)V

    iget-object v2, p0, Lcom/android/server/tv/TvTimerClient;->timerPowerOn:Lcom/mstar/android/tvapi/common/vo/TimerPowerOn;

    iget-object v3, p0, Lcom/android/server/tv/TvTimerClient;->powerOnConfig:Lcom/mstar/android/tvapi/common/vo/OnTimeTvDescriptor;

    iget v3, v3, Lcom/mstar/android/tvapi/common/vo/OnTimeTvDescriptor;->mChNo:I

    iput v3, v2, Lcom/mstar/android/tvapi/common/vo/TimerPowerOn;->channelNumber:I

    iget-object v2, p0, Lcom/android/server/tv/TvTimerClient;->timerPowerOn:Lcom/mstar/android/tvapi/common/vo/TimerPowerOn;

    iget-object v3, p0, Lcom/android/server/tv/TvTimerClient;->powerOnConfig:Lcom/mstar/android/tvapi/common/vo/OnTimeTvDescriptor;

    iget-short v3, v3, Lcom/mstar/android/tvapi/common/vo/OnTimeTvDescriptor;->mVol:S

    iput-short v3, v2, Lcom/mstar/android/tvapi/common/vo/TimerPowerOn;->volume:S

    iget-object v2, p0, Lcom/android/server/tv/TvTimerClient;->timerPowerOn:Lcom/mstar/android/tvapi/common/vo/TimerPowerOn;

    invoke-static {}, Lcom/mstar/android/tvapi/common/vo/EnumTimeOnTimerSource;->values()[Lcom/mstar/android/tvapi/common/vo/EnumTimeOnTimerSource;

    move-result-object v3

    iget-object v4, p0, Lcom/android/server/tv/TvTimerClient;->powerOnConfig:Lcom/mstar/android/tvapi/common/vo/OnTimeTvDescriptor;

    iget-object v4, v4, Lcom/mstar/android/tvapi/common/vo/OnTimeTvDescriptor;->enTVSrc:Lcom/mstar/android/tvapi/common/vo/EnumTimeOnTimerSource;

    invoke-virtual {v4}, Lcom/mstar/android/tvapi/common/vo/EnumTimeOnTimerSource;->ordinal()I

    move-result v4

    aget-object v3, v3, v4

    invoke-virtual {v2, v3}, Lcom/mstar/android/tvapi/common/vo/TimerPowerOn;->setTvSource(Lcom/mstar/android/tvapi/common/vo/EnumTimeOnTimerSource;)V

    iget-object v2, p0, Lcom/android/server/tv/TvTimerClient;->timerPowerOn:Lcom/mstar/android/tvapi/common/vo/TimerPowerOn;

    sget-object v3, Lcom/mstar/android/tvapi/common/vo/EnumTimerBootType;->EN_TIMER_BOOT_ON_TIMER:Lcom/mstar/android/tvapi/common/vo/EnumTimerBootType;

    invoke-virtual {v2, v3}, Lcom/mstar/android/tvapi/common/vo/TimerPowerOn;->setBootMode(Lcom/mstar/android/tvapi/common/vo/EnumTimerBootType;)V

    iget-object v2, p0, Lcom/android/server/tv/TvTimerClient;->timerPowerOn:Lcom/mstar/android/tvapi/common/vo/TimerPowerOn;

    sget-object v3, Lcom/mstar/android/tvapi/common/vo/EnumTimerPeriod;->EN_Timer_Everyday:Lcom/mstar/android/tvapi/common/vo/EnumTimerPeriod;

    invoke-virtual {v2, v3}, Lcom/mstar/android/tvapi/common/vo/TimerPowerOn;->setTimerPeriod(Lcom/mstar/android/tvapi/common/vo/EnumTimerPeriod;)V

    sget-object v1, Lcom/mstar/android/tvapi/common/vo/EnumEpgTimerActType;->EN_EPGTIMER_ACT_NONE:Lcom/mstar/android/tvapi/common/vo/EnumEpgTimerActType;

    const-string v2, "TvTimerClient"

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string v4, "setOnTimerEnable:"

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    iget-object v4, p0, Lcom/android/server/tv/TvTimerClient;->onTime:Landroid/text/format/Time;

    iget v4, v4, Landroid/text/format/Time;->year:I

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v3

    const-string v4, "."

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    iget-object v4, p0, Lcom/android/server/tv/TvTimerClient;->onTime:Landroid/text/format/Time;

    iget v4, v4, Landroid/text/format/Time;->month:I

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v3

    const-string v4, "."

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    iget-object v4, p0, Lcom/android/server/tv/TvTimerClient;->onTime:Landroid/text/format/Time;

    iget v4, v4, Landroid/text/format/Time;->monthDay:I

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v3

    const-string v4, "."

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    iget-object v4, p0, Lcom/android/server/tv/TvTimerClient;->offTime:Landroid/text/format/Time;

    iget v4, v4, Landroid/text/format/Time;->hour:I

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v3

    const-string v4, "."

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    iget-object v4, p0, Lcom/android/server/tv/TvTimerClient;->onTime:Landroid/text/format/Time;

    iget v4, v4, Landroid/text/format/Time;->minute:I

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v3

    const-string v4, "."

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    iget-object v4, p0, Lcom/android/server/tv/TvTimerClient;->onTime:Landroid/text/format/Time;

    iget v4, v4, Landroid/text/format/Time;->second:I

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v2, v3}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    const-string v2, "TvTimerClient"

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string v4, "setOnTimerEnable:\tInputsource."

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    iget-object v4, p0, Lcom/android/server/tv/TvTimerClient;->powerOnConfig:Lcom/mstar/android/tvapi/common/vo/OnTimeTvDescriptor;

    iget-object v4, v4, Lcom/mstar/android/tvapi/common/vo/OnTimeTvDescriptor;->enTVSrc:Lcom/mstar/android/tvapi/common/vo/EnumTimeOnTimerSource;

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v3

    const-string v4, "\tCH."

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    iget-object v4, p0, Lcom/android/server/tv/TvTimerClient;->powerOnConfig:Lcom/mstar/android/tvapi/common/vo/OnTimeTvDescriptor;

    iget v4, v4, Lcom/mstar/android/tvapi/common/vo/OnTimeTvDescriptor;->mChNo:I

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v3

    const-string v4, "\tVolume."

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    iget-object v4, p0, Lcom/android/server/tv/TvTimerClient;->powerOnConfig:Lcom/mstar/android/tvapi/common/vo/OnTimeTvDescriptor;

    iget-short v4, v4, Lcom/mstar/android/tvapi/common/vo/OnTimeTvDescriptor;->mVol:S

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v2, v3}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    :try_start_0
    invoke-static {}, Lcom/mstar/android/tvapi/common/TvManager;->getInstance()Lcom/mstar/android/tvapi/common/TvManager;

    move-result-object v2

    if-eqz v2, :cond_0

    invoke-static {}, Lcom/mstar/android/tvapi/common/TvManager;->getInstance()Lcom/mstar/android/tvapi/common/TvManager;

    move-result-object v2

    invoke-virtual {v2}, Lcom/mstar/android/tvapi/common/TvManager;->getTimerManager()Lcom/mstar/android/tvapi/common/TimerManager;

    move-result-object v2

    iget-object v3, p0, Lcom/android/server/tv/TvTimerClient;->timerPowerOn:Lcom/mstar/android/tvapi/common/vo/TimerPowerOn;

    const/4 v4, 0x1

    const/4 v5, 0x1

    invoke-virtual {v2, v3, v4, v5, v1}, Lcom/mstar/android/tvapi/common/TimerManager;->setOnTime(Lcom/mstar/android/tvapi/common/vo/TimerPowerOn;ZZLcom/mstar/android/tvapi/common/vo/EnumEpgTimerActType;)V
    :try_end_0
    .catch Lcom/mstar/android/tvapi/common/exception/TvCommonException; {:try_start_0 .. :try_end_0} :catch_0

    :cond_0
    :goto_0
    return v6

    :catch_0
    move-exception v0

    invoke-virtual {v0}, Lcom/mstar/android/tvapi/common/exception/TvCommonException;->printStackTrace()V

    goto :goto_0

    :cond_1
    iget-object v2, p0, Lcom/android/server/tv/TvTimerClient;->timerPowerOn:Lcom/mstar/android/tvapi/common/vo/TimerPowerOn;

    iget-object v3, p0, Lcom/android/server/tv/TvTimerClient;->onTime:Landroid/text/format/Time;

    invoke-virtual {v2, v3}, Lcom/mstar/android/tvapi/common/vo/TimerPowerOn;->set(Landroid/text/format/Time;)V

    iget-object v2, p0, Lcom/android/server/tv/TvTimerClient;->timerPowerOn:Lcom/mstar/android/tvapi/common/vo/TimerPowerOn;

    iget-object v3, p0, Lcom/android/server/tv/TvTimerClient;->powerOnConfig:Lcom/mstar/android/tvapi/common/vo/OnTimeTvDescriptor;

    iget v3, v3, Lcom/mstar/android/tvapi/common/vo/OnTimeTvDescriptor;->mChNo:I

    iput v3, v2, Lcom/mstar/android/tvapi/common/vo/TimerPowerOn;->channelNumber:I

    iget-object v2, p0, Lcom/android/server/tv/TvTimerClient;->timerPowerOn:Lcom/mstar/android/tvapi/common/vo/TimerPowerOn;

    iget-object v3, p0, Lcom/android/server/tv/TvTimerClient;->powerOnConfig:Lcom/mstar/android/tvapi/common/vo/OnTimeTvDescriptor;

    iget-short v3, v3, Lcom/mstar/android/tvapi/common/vo/OnTimeTvDescriptor;->mVol:S

    iput-short v3, v2, Lcom/mstar/android/tvapi/common/vo/TimerPowerOn;->volume:S

    iget-object v2, p0, Lcom/android/server/tv/TvTimerClient;->timerPowerOn:Lcom/mstar/android/tvapi/common/vo/TimerPowerOn;

    invoke-static {}, Lcom/mstar/android/tvapi/common/vo/EnumTimeOnTimerSource;->values()[Lcom/mstar/android/tvapi/common/vo/EnumTimeOnTimerSource;

    move-result-object v3

    iget-object v4, p0, Lcom/android/server/tv/TvTimerClient;->powerOnConfig:Lcom/mstar/android/tvapi/common/vo/OnTimeTvDescriptor;

    iget-object v4, v4, Lcom/mstar/android/tvapi/common/vo/OnTimeTvDescriptor;->enTVSrc:Lcom/mstar/android/tvapi/common/vo/EnumTimeOnTimerSource;

    invoke-virtual {v4}, Lcom/mstar/android/tvapi/common/vo/EnumTimeOnTimerSource;->ordinal()I

    move-result v4

    aget-object v3, v3, v4

    invoke-virtual {v2, v3}, Lcom/mstar/android/tvapi/common/vo/TimerPowerOn;->setTvSource(Lcom/mstar/android/tvapi/common/vo/EnumTimeOnTimerSource;)V

    iget-object v2, p0, Lcom/android/server/tv/TvTimerClient;->timerPowerOn:Lcom/mstar/android/tvapi/common/vo/TimerPowerOn;

    sget-object v3, Lcom/mstar/android/tvapi/common/vo/EnumTimerBootType;->EN_TIMER_BOOT_ON_TIMER:Lcom/mstar/android/tvapi/common/vo/EnumTimerBootType;

    invoke-virtual {v2, v3}, Lcom/mstar/android/tvapi/common/vo/TimerPowerOn;->setBootMode(Lcom/mstar/android/tvapi/common/vo/EnumTimerBootType;)V

    iget-object v2, p0, Lcom/android/server/tv/TvTimerClient;->timerPowerOn:Lcom/mstar/android/tvapi/common/vo/TimerPowerOn;

    sget-object v3, Lcom/mstar/android/tvapi/common/vo/EnumTimerPeriod;->EN_Timer_Off:Lcom/mstar/android/tvapi/common/vo/EnumTimerPeriod;

    invoke-virtual {v2, v3}, Lcom/mstar/android/tvapi/common/vo/TimerPowerOn;->setTimerPeriod(Lcom/mstar/android/tvapi/common/vo/EnumTimerPeriod;)V

    sget-object v1, Lcom/mstar/android/tvapi/common/vo/EnumEpgTimerActType;->EN_EPGTIMER_ACT_NONE:Lcom/mstar/android/tvapi/common/vo/EnumEpgTimerActType;

    const-string v2, "TvTimerClient"

    const-string v3, "setOnTimerEnable:disable"

    invoke-static {v2, v3}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    :try_start_1
    invoke-static {}, Lcom/mstar/android/tvapi/common/TvManager;->getInstance()Lcom/mstar/android/tvapi/common/TvManager;

    move-result-object v2

    if-eqz v2, :cond_0

    invoke-static {}, Lcom/mstar/android/tvapi/common/TvManager;->getInstance()Lcom/mstar/android/tvapi/common/TvManager;

    move-result-object v2

    invoke-virtual {v2}, Lcom/mstar/android/tvapi/common/TvManager;->getTimerManager()Lcom/mstar/android/tvapi/common/TimerManager;

    move-result-object v2

    iget-object v3, p0, Lcom/android/server/tv/TvTimerClient;->timerPowerOn:Lcom/mstar/android/tvapi/common/vo/TimerPowerOn;

    const/4 v4, 0x1

    const/4 v5, 0x1

    invoke-virtual {v2, v3, v4, v5, v1}, Lcom/mstar/android/tvapi/common/TimerManager;->setOnTime(Lcom/mstar/android/tvapi/common/vo/TimerPowerOn;ZZLcom/mstar/android/tvapi/common/vo/EnumEpgTimerActType;)V
    :try_end_1
    .catch Lcom/mstar/android/tvapi/common/exception/TvCommonException; {:try_start_1 .. :try_end_1} :catch_1

    goto :goto_0

    :catch_1
    move-exception v0

    invoke-virtual {v0}, Lcom/mstar/android/tvapi/common/exception/TvCommonException;->printStackTrace()V

    goto :goto_0
.end method

.method public setSleepMode(I)Z
    .locals 4
    .param p1    # I
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Landroid/os/RemoteException;
        }
    .end annotation

    const-string v1, "TvTimerClient"

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "setSleepMode, paras eMode is "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, p1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    invoke-static {}, Lcom/mstar/android/tvapi/common/vo/EnumSleepTimeState;->values()[Lcom/mstar/android/tvapi/common/vo/EnumSleepTimeState;

    move-result-object v1

    aget-object v1, v1, p1

    iput-object v1, p0, Lcom/android/server/tv/TvTimerClient;->eSleepMode:Lcom/mstar/android/tvapi/common/vo/EnumSleepTimeState;

    invoke-static {}, Lcom/mstar/android/tvapi/common/vo/EnumSleepTimeState;->values()[Lcom/mstar/android/tvapi/common/vo/EnumSleepTimeState;

    move-result-object v1

    iget-object v2, p0, Lcom/android/server/tv/TvTimerClient;->eSleepMode:Lcom/mstar/android/tvapi/common/vo/EnumSleepTimeState;

    invoke-virtual {v2}, Lcom/mstar/android/tvapi/common/vo/EnumSleepTimeState;->ordinal()I

    move-result v2

    aget-object v1, v1, v2

    iput-object v1, p0, Lcom/android/server/tv/TvTimerClient;->enSleepTimeState:Lcom/mstar/android/tvapi/common/vo/EnumSleepTimeState;

    :try_start_0
    invoke-static {}, Lcom/mstar/android/tvapi/common/TvManager;->getInstance()Lcom/mstar/android/tvapi/common/TvManager;

    move-result-object v1

    if-eqz v1, :cond_0

    invoke-static {}, Lcom/mstar/android/tvapi/common/TvManager;->getInstance()Lcom/mstar/android/tvapi/common/TvManager;

    move-result-object v1

    invoke-virtual {v1}, Lcom/mstar/android/tvapi/common/TvManager;->getTimerManager()Lcom/mstar/android/tvapi/common/TimerManager;

    move-result-object v1

    iget-object v2, p0, Lcom/android/server/tv/TvTimerClient;->enSleepTimeState:Lcom/mstar/android/tvapi/common/vo/EnumSleepTimeState;

    invoke-virtual {v1, v2}, Lcom/mstar/android/tvapi/common/TimerManager;->setSleepModeTime(Lcom/mstar/android/tvapi/common/vo/EnumSleepTimeState;)V
    :try_end_0
    .catch Lcom/mstar/android/tvapi/common/exception/TvCommonException; {:try_start_0 .. :try_end_0} :catch_0

    :cond_0
    :goto_0
    const/4 v1, 0x1

    return v1

    :catch_0
    move-exception v0

    invoke-virtual {v0}, Lcom/mstar/android/tvapi/common/exception/TvCommonException;->printStackTrace()V

    goto :goto_0
.end method
