.class public Lcom/android/server/tv/DataBaseDesk$MS_FACTORY_HDMI_OVERSCAN_SETTING;
.super Ljava/lang/Object;
.source "DataBaseDesk.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/android/server/tv/DataBaseDesk;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x9
    name = "MS_FACTORY_HDMI_OVERSCAN_SETTING"
.end annotation


# instance fields
.field public stVideoWinInfo:[[Lcom/android/server/tv/DataBaseDesk$ST_MAPI_VIDEO_WINDOW_INFO;

.field public u16CheckSum:I


# direct methods
.method public constructor <init>()V
    .locals 4

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    sget-object v2, Lcom/android/server/tv/DataBaseDesk$MAX_HDMI_Resolution_Info;->E_HDMI_MAX:Lcom/android/server/tv/DataBaseDesk$MAX_HDMI_Resolution_Info;

    invoke-virtual {v2}, Lcom/android/server/tv/DataBaseDesk$MAX_HDMI_Resolution_Info;->ordinal()I

    move-result v2

    sget-object v3, Lcom/android/server/tv/DataBaseDesk$MAPI_VIDEO_ARC_Type;->E_AR_MAX:Lcom/android/server/tv/DataBaseDesk$MAPI_VIDEO_ARC_Type;

    invoke-virtual {v3}, Lcom/android/server/tv/DataBaseDesk$MAPI_VIDEO_ARC_Type;->ordinal()I

    move-result v3

    filled-new-array {v2, v3}, [I

    move-result-object v2

    const-class v3, Lcom/android/server/tv/DataBaseDesk$ST_MAPI_VIDEO_WINDOW_INFO;

    invoke-static {v3, v2}, Ljava/lang/reflect/Array;->newInstance(Ljava/lang/Class;[I)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, [[Lcom/android/server/tv/DataBaseDesk$ST_MAPI_VIDEO_WINDOW_INFO;

    iput-object v2, p0, Lcom/android/server/tv/DataBaseDesk$MS_FACTORY_HDMI_OVERSCAN_SETTING;->stVideoWinInfo:[[Lcom/android/server/tv/DataBaseDesk$ST_MAPI_VIDEO_WINDOW_INFO;

    const/4 v0, 0x0

    :goto_0
    sget-object v2, Lcom/android/server/tv/DataBaseDesk$MAX_DTV_Resolution_Info;->E_DTV_MAX:Lcom/android/server/tv/DataBaseDesk$MAX_DTV_Resolution_Info;

    invoke-virtual {v2}, Lcom/android/server/tv/DataBaseDesk$MAX_DTV_Resolution_Info;->ordinal()I

    move-result v2

    if-ge v0, v2, :cond_1

    const/4 v1, 0x0

    :goto_1
    sget-object v2, Lcom/android/server/tv/DataBaseDesk$MAPI_VIDEO_ARC_Type;->E_AR_MAX:Lcom/android/server/tv/DataBaseDesk$MAPI_VIDEO_ARC_Type;

    invoke-virtual {v2}, Lcom/android/server/tv/DataBaseDesk$MAPI_VIDEO_ARC_Type;->ordinal()I

    move-result v2

    if-ge v1, v2, :cond_0

    iget-object v2, p0, Lcom/android/server/tv/DataBaseDesk$MS_FACTORY_HDMI_OVERSCAN_SETTING;->stVideoWinInfo:[[Lcom/android/server/tv/DataBaseDesk$ST_MAPI_VIDEO_WINDOW_INFO;

    aget-object v2, v2, v0

    new-instance v3, Lcom/android/server/tv/DataBaseDesk$ST_MAPI_VIDEO_WINDOW_INFO;

    invoke-direct {v3}, Lcom/android/server/tv/DataBaseDesk$ST_MAPI_VIDEO_WINDOW_INFO;-><init>()V

    aput-object v3, v2, v1

    add-int/lit8 v1, v1, 0x1

    goto :goto_1

    :cond_0
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    :cond_1
    return-void
.end method
