.class public Lcom/android/server/tv/DeskPipEventListener;
.super Ljava/lang/Object;
.source "DeskPipEventListener.java"

# interfaces
.implements Lcom/mstar/android/tvapi/common/listener/OnPipEventListener;


# static fields
.field private static pipEventListener:Lcom/android/server/tv/DeskPipEventListener;


# instance fields
.field private iPipEventClients:Ljava/util/HashMap;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/HashMap",
            "<",
            "Ljava/lang/String;",
            "Lcom/mstar/android/tv/IPipEventClient;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method static constructor <clinit>()V
    .locals 1

    const/4 v0, 0x0

    sput-object v0, Lcom/android/server/tv/DeskPipEventListener;->pipEventListener:Lcom/android/server/tv/DeskPipEventListener;

    return-void
.end method

.method public constructor <init>()V
    .locals 1

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    new-instance v0, Ljava/util/HashMap;

    invoke-direct {v0}, Ljava/util/HashMap;-><init>()V

    iput-object v0, p0, Lcom/android/server/tv/DeskPipEventListener;->iPipEventClients:Ljava/util/HashMap;

    new-instance v0, Ljava/util/HashMap;

    invoke-direct {v0}, Ljava/util/HashMap;-><init>()V

    iput-object v0, p0, Lcom/android/server/tv/DeskPipEventListener;->iPipEventClients:Ljava/util/HashMap;

    return-void
.end method

.method public static getInstance()Lcom/android/server/tv/DeskPipEventListener;
    .locals 1

    sget-object v0, Lcom/android/server/tv/DeskPipEventListener;->pipEventListener:Lcom/android/server/tv/DeskPipEventListener;

    if-nez v0, :cond_0

    new-instance v0, Lcom/android/server/tv/DeskPipEventListener;

    invoke-direct {v0}, Lcom/android/server/tv/DeskPipEventListener;-><init>()V

    sput-object v0, Lcom/android/server/tv/DeskPipEventListener;->pipEventListener:Lcom/android/server/tv/DeskPipEventListener;

    :cond_0
    sget-object v0, Lcom/android/server/tv/DeskPipEventListener;->pipEventListener:Lcom/android/server/tv/DeskPipEventListener;

    return-object v0
.end method


# virtual methods
.method public on4k2kUnsupportPip(III)Z
    .locals 2
    .param p1    # I
    .param p2    # I
    .param p3    # I

    const-string v0, "DeskPipEventListener"

    const-string v1, "on4k2kUnsupportPip\n"

    invoke-static {v0, v1}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    const/4 v0, 0x0

    return v0
.end method

.method public on4k2kUnsupportPop(III)Z
    .locals 2
    .param p1    # I
    .param p2    # I
    .param p3    # I

    const-string v0, "DeskPipEventListener"

    const-string v1, "on4k2kUnsupportPop\n"

    invoke-static {v0, v1}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    const/4 v0, 0x0

    return v0
.end method

.method public on4k2kUnsupportTravelingMode(III)Z
    .locals 2
    .param p1    # I
    .param p2    # I
    .param p3    # I

    const-string v0, "DeskPipEventListener"

    const-string v1, "on4k2kUnsupportTravelingMode\n"

    invoke-static {v0, v1}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    const/4 v0, 0x0

    return v0
.end method

.method public onEnablePip(III)Z
    .locals 2
    .param p1    # I
    .param p2    # I
    .param p3    # I

    const-string v0, "DeskPipEventListener"

    const-string v1, "onEnablePip\n"

    invoke-static {v0, v1}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    const/4 v0, 0x0

    return v0
.end method

.method public onEnablePop(III)Z
    .locals 2
    .param p1    # I
    .param p2    # I
    .param p3    # I

    const-string v0, "DeskPipEventListener"

    const-string v1, "onEnablePop\n"

    invoke-static {v0, v1}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    const/4 v0, 0x0

    return v0
.end method

.method public releaseClient(Ljava/lang/String;)V
    .locals 1
    .param p1    # Ljava/lang/String;

    iget-object v0, p0, Lcom/android/server/tv/DeskPipEventListener;->iPipEventClients:Ljava/util/HashMap;

    invoke-virtual {v0, p1}, Ljava/util/HashMap;->remove(Ljava/lang/Object;)Ljava/lang/Object;

    return-void
.end method

.method public setClient(Ljava/lang/String;Lcom/mstar/android/tv/IPipEventClient;)V
    .locals 1
    .param p1    # Ljava/lang/String;
    .param p2    # Lcom/mstar/android/tv/IPipEventClient;

    iget-object v0, p0, Lcom/android/server/tv/DeskPipEventListener;->iPipEventClients:Ljava/util/HashMap;

    invoke-virtual {v0, p1, p2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    return-void
.end method
