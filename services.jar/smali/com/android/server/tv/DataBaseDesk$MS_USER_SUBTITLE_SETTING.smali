.class public Lcom/android/server/tv/DataBaseDesk$MS_USER_SUBTITLE_SETTING;
.super Ljava/lang/Object;
.source "DataBaseDesk.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/android/server/tv/DataBaseDesk;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x9
    name = "MS_USER_SUBTITLE_SETTING"
.end annotation


# instance fields
.field public SubtitleDefaultLanguage:Lcom/mstar/android/tvapi/common/vo/TvOsType$EnumLanguage;

.field public SubtitleDefaultLanguage_2:Lcom/mstar/android/tvapi/common/vo/TvOsType$EnumLanguage;

.field public fEnableSubTitle:Z

.field public fHardOfHearing:Z


# direct methods
.method public constructor <init>(Lcom/mstar/android/tvapi/common/vo/TvOsType$EnumLanguage;Lcom/mstar/android/tvapi/common/vo/TvOsType$EnumLanguage;ZZ)V
    .locals 0
    .param p1    # Lcom/mstar/android/tvapi/common/vo/TvOsType$EnumLanguage;
    .param p2    # Lcom/mstar/android/tvapi/common/vo/TvOsType$EnumLanguage;
    .param p3    # Z
    .param p4    # Z

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    iput-object p1, p0, Lcom/android/server/tv/DataBaseDesk$MS_USER_SUBTITLE_SETTING;->SubtitleDefaultLanguage:Lcom/mstar/android/tvapi/common/vo/TvOsType$EnumLanguage;

    iput-object p2, p0, Lcom/android/server/tv/DataBaseDesk$MS_USER_SUBTITLE_SETTING;->SubtitleDefaultLanguage_2:Lcom/mstar/android/tvapi/common/vo/TvOsType$EnumLanguage;

    iput-boolean p3, p0, Lcom/android/server/tv/DataBaseDesk$MS_USER_SUBTITLE_SETTING;->fHardOfHearing:Z

    iput-boolean p4, p0, Lcom/android/server/tv/DataBaseDesk$MS_USER_SUBTITLE_SETTING;->fEnableSubTitle:Z

    return-void
.end method
