.class public Lcom/android/server/tv/DataBaseDesk$ThreeD_Video_MODE;
.super Ljava/lang/Object;
.source "DataBaseDesk.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/android/server/tv/DataBaseDesk;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x9
    name = "ThreeD_Video_MODE"
.end annotation


# instance fields
.field public eThreeDVideo:Lcom/android/server/tv/DataBaseDesk$EN_ThreeD_Video;

.field public eThreeDVideo3DDepth:Lcom/android/server/tv/DataBaseDesk$EN_ThreeD_Video_3DDEPTH;

.field public eThreeDVideo3DOffset:Lcom/android/server/tv/DataBaseDesk$EN_ThreeD_Video_3DOFFSET;

.field public eThreeDVideo3DOutputAspect:Lcom/android/server/tv/DataBaseDesk$EN_ThreeD_Video_3DOUTPUTASPECT;

.field public eThreeDVideo3DTo2D:Lcom/android/server/tv/DataBaseDesk$EN_ThreeD_Video_3DTO2D;

.field public eThreeDVideoAutoStart:Lcom/android/server/tv/DataBaseDesk$EN_ThreeD_Video_AUTOSTART;

.field public eThreeDVideoDisplayFormat:Lcom/android/server/tv/DataBaseDesk$EN_ThreeD_Video_DISPLAYFORMAT;

.field public eThreeDVideoLRViewSwitch:Lcom/android/server/tv/DataBaseDesk$EN_ThreeD_Video_LRVIEWSWITCH;

.field public eThreeDVideoSelfAdaptiveDetect:Lcom/android/server/tv/DataBaseDesk$EN_ThreeD_Video_SELFADAPTIVE_DETECT;


# direct methods
.method public constructor <init>(Lcom/android/server/tv/DataBaseDesk$EN_ThreeD_Video;Lcom/android/server/tv/DataBaseDesk$EN_ThreeD_Video_SELFADAPTIVE_DETECT;Lcom/android/server/tv/DataBaseDesk$EN_ThreeD_Video_DISPLAYFORMAT;Lcom/android/server/tv/DataBaseDesk$EN_ThreeD_Video_3DTO2D;Lcom/android/server/tv/DataBaseDesk$EN_ThreeD_Video_3DDEPTH;Lcom/android/server/tv/DataBaseDesk$EN_ThreeD_Video_3DOFFSET;Lcom/android/server/tv/DataBaseDesk$EN_ThreeD_Video_AUTOSTART;Lcom/android/server/tv/DataBaseDesk$EN_ThreeD_Video_3DOUTPUTASPECT;Lcom/android/server/tv/DataBaseDesk$EN_ThreeD_Video_LRVIEWSWITCH;)V
    .locals 0
    .param p1    # Lcom/android/server/tv/DataBaseDesk$EN_ThreeD_Video;
    .param p2    # Lcom/android/server/tv/DataBaseDesk$EN_ThreeD_Video_SELFADAPTIVE_DETECT;
    .param p3    # Lcom/android/server/tv/DataBaseDesk$EN_ThreeD_Video_DISPLAYFORMAT;
    .param p4    # Lcom/android/server/tv/DataBaseDesk$EN_ThreeD_Video_3DTO2D;
    .param p5    # Lcom/android/server/tv/DataBaseDesk$EN_ThreeD_Video_3DDEPTH;
    .param p6    # Lcom/android/server/tv/DataBaseDesk$EN_ThreeD_Video_3DOFFSET;
    .param p7    # Lcom/android/server/tv/DataBaseDesk$EN_ThreeD_Video_AUTOSTART;
    .param p8    # Lcom/android/server/tv/DataBaseDesk$EN_ThreeD_Video_3DOUTPUTASPECT;
    .param p9    # Lcom/android/server/tv/DataBaseDesk$EN_ThreeD_Video_LRVIEWSWITCH;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    iput-object p1, p0, Lcom/android/server/tv/DataBaseDesk$ThreeD_Video_MODE;->eThreeDVideo:Lcom/android/server/tv/DataBaseDesk$EN_ThreeD_Video;

    iput-object p2, p0, Lcom/android/server/tv/DataBaseDesk$ThreeD_Video_MODE;->eThreeDVideoSelfAdaptiveDetect:Lcom/android/server/tv/DataBaseDesk$EN_ThreeD_Video_SELFADAPTIVE_DETECT;

    iput-object p3, p0, Lcom/android/server/tv/DataBaseDesk$ThreeD_Video_MODE;->eThreeDVideoDisplayFormat:Lcom/android/server/tv/DataBaseDesk$EN_ThreeD_Video_DISPLAYFORMAT;

    iput-object p4, p0, Lcom/android/server/tv/DataBaseDesk$ThreeD_Video_MODE;->eThreeDVideo3DTo2D:Lcom/android/server/tv/DataBaseDesk$EN_ThreeD_Video_3DTO2D;

    iput-object p5, p0, Lcom/android/server/tv/DataBaseDesk$ThreeD_Video_MODE;->eThreeDVideo3DDepth:Lcom/android/server/tv/DataBaseDesk$EN_ThreeD_Video_3DDEPTH;

    iput-object p6, p0, Lcom/android/server/tv/DataBaseDesk$ThreeD_Video_MODE;->eThreeDVideo3DOffset:Lcom/android/server/tv/DataBaseDesk$EN_ThreeD_Video_3DOFFSET;

    iput-object p7, p0, Lcom/android/server/tv/DataBaseDesk$ThreeD_Video_MODE;->eThreeDVideoAutoStart:Lcom/android/server/tv/DataBaseDesk$EN_ThreeD_Video_AUTOSTART;

    iput-object p8, p0, Lcom/android/server/tv/DataBaseDesk$ThreeD_Video_MODE;->eThreeDVideo3DOutputAspect:Lcom/android/server/tv/DataBaseDesk$EN_ThreeD_Video_3DOUTPUTASPECT;

    iput-object p9, p0, Lcom/android/server/tv/DataBaseDesk$ThreeD_Video_MODE;->eThreeDVideoLRViewSwitch:Lcom/android/server/tv/DataBaseDesk$EN_ThreeD_Video_LRVIEWSWITCH;

    return-void
.end method
