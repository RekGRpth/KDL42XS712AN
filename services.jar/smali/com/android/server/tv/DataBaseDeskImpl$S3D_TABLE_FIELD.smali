.class public final enum Lcom/android/server/tv/DataBaseDeskImpl$S3D_TABLE_FIELD;
.super Ljava/lang/Enum;
.source "DataBaseDeskImpl.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/android/server/tv/DataBaseDeskImpl;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x4019
    name = "S3D_TABLE_FIELD"
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Enum",
        "<",
        "Lcom/android/server/tv/DataBaseDeskImpl$S3D_TABLE_FIELD;",
        ">;"
    }
.end annotation


# static fields
.field private static final synthetic $VALUES:[Lcom/android/server/tv/DataBaseDeskImpl$S3D_TABLE_FIELD;

.field public static final enum eThreeDVideo:Lcom/android/server/tv/DataBaseDeskImpl$S3D_TABLE_FIELD;

.field public static final enum eThreeDVideo3DDepth:Lcom/android/server/tv/DataBaseDeskImpl$S3D_TABLE_FIELD;

.field public static final enum eThreeDVideo3DOffset:Lcom/android/server/tv/DataBaseDeskImpl$S3D_TABLE_FIELD;

.field public static final enum eThreeDVideo3DOutputAspect:Lcom/android/server/tv/DataBaseDeskImpl$S3D_TABLE_FIELD;

.field public static final enum eThreeDVideo3DTo2D:Lcom/android/server/tv/DataBaseDeskImpl$S3D_TABLE_FIELD;

.field public static final enum eThreeDVideoAutoStart:Lcom/android/server/tv/DataBaseDeskImpl$S3D_TABLE_FIELD;

.field public static final enum eThreeDVideoDisplayFormat:Lcom/android/server/tv/DataBaseDeskImpl$S3D_TABLE_FIELD;

.field public static final enum eThreeDVideoLRViewSwitch:Lcom/android/server/tv/DataBaseDeskImpl$S3D_TABLE_FIELD;

.field public static final enum eThreeDVideoSelfAdaptiveDetect:Lcom/android/server/tv/DataBaseDeskImpl$S3D_TABLE_FIELD;

.field public static final enum eThreeDVideoSelfAdaptiveLevel:Lcom/android/server/tv/DataBaseDeskImpl$S3D_TABLE_FIELD;


# direct methods
.method static constructor <clinit>()V
    .locals 8

    const/4 v7, 0x4

    const/4 v6, 0x3

    const/4 v5, 0x2

    const/4 v4, 0x1

    const/4 v3, 0x0

    new-instance v0, Lcom/android/server/tv/DataBaseDeskImpl$S3D_TABLE_FIELD;

    const-string v1, "eThreeDVideo"

    invoke-direct {v0, v1, v3}, Lcom/android/server/tv/DataBaseDeskImpl$S3D_TABLE_FIELD;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/android/server/tv/DataBaseDeskImpl$S3D_TABLE_FIELD;->eThreeDVideo:Lcom/android/server/tv/DataBaseDeskImpl$S3D_TABLE_FIELD;

    new-instance v0, Lcom/android/server/tv/DataBaseDeskImpl$S3D_TABLE_FIELD;

    const-string v1, "eThreeDVideoDisplayFormat"

    invoke-direct {v0, v1, v4}, Lcom/android/server/tv/DataBaseDeskImpl$S3D_TABLE_FIELD;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/android/server/tv/DataBaseDeskImpl$S3D_TABLE_FIELD;->eThreeDVideoDisplayFormat:Lcom/android/server/tv/DataBaseDeskImpl$S3D_TABLE_FIELD;

    new-instance v0, Lcom/android/server/tv/DataBaseDeskImpl$S3D_TABLE_FIELD;

    const-string v1, "eThreeDVideo3DDepth"

    invoke-direct {v0, v1, v5}, Lcom/android/server/tv/DataBaseDeskImpl$S3D_TABLE_FIELD;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/android/server/tv/DataBaseDeskImpl$S3D_TABLE_FIELD;->eThreeDVideo3DDepth:Lcom/android/server/tv/DataBaseDeskImpl$S3D_TABLE_FIELD;

    new-instance v0, Lcom/android/server/tv/DataBaseDeskImpl$S3D_TABLE_FIELD;

    const-string v1, "eThreeDVideoAutoStart"

    invoke-direct {v0, v1, v6}, Lcom/android/server/tv/DataBaseDeskImpl$S3D_TABLE_FIELD;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/android/server/tv/DataBaseDeskImpl$S3D_TABLE_FIELD;->eThreeDVideoAutoStart:Lcom/android/server/tv/DataBaseDeskImpl$S3D_TABLE_FIELD;

    new-instance v0, Lcom/android/server/tv/DataBaseDeskImpl$S3D_TABLE_FIELD;

    const-string v1, "eThreeDVideo3DOutputAspect"

    invoke-direct {v0, v1, v7}, Lcom/android/server/tv/DataBaseDeskImpl$S3D_TABLE_FIELD;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/android/server/tv/DataBaseDeskImpl$S3D_TABLE_FIELD;->eThreeDVideo3DOutputAspect:Lcom/android/server/tv/DataBaseDeskImpl$S3D_TABLE_FIELD;

    new-instance v0, Lcom/android/server/tv/DataBaseDeskImpl$S3D_TABLE_FIELD;

    const-string v1, "eThreeDVideoLRViewSwitch"

    const/4 v2, 0x5

    invoke-direct {v0, v1, v2}, Lcom/android/server/tv/DataBaseDeskImpl$S3D_TABLE_FIELD;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/android/server/tv/DataBaseDeskImpl$S3D_TABLE_FIELD;->eThreeDVideoLRViewSwitch:Lcom/android/server/tv/DataBaseDeskImpl$S3D_TABLE_FIELD;

    new-instance v0, Lcom/android/server/tv/DataBaseDeskImpl$S3D_TABLE_FIELD;

    const-string v1, "eThreeDVideoSelfAdaptiveDetect"

    const/4 v2, 0x6

    invoke-direct {v0, v1, v2}, Lcom/android/server/tv/DataBaseDeskImpl$S3D_TABLE_FIELD;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/android/server/tv/DataBaseDeskImpl$S3D_TABLE_FIELD;->eThreeDVideoSelfAdaptiveDetect:Lcom/android/server/tv/DataBaseDeskImpl$S3D_TABLE_FIELD;

    new-instance v0, Lcom/android/server/tv/DataBaseDeskImpl$S3D_TABLE_FIELD;

    const-string v1, "eThreeDVideoSelfAdaptiveLevel"

    const/4 v2, 0x7

    invoke-direct {v0, v1, v2}, Lcom/android/server/tv/DataBaseDeskImpl$S3D_TABLE_FIELD;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/android/server/tv/DataBaseDeskImpl$S3D_TABLE_FIELD;->eThreeDVideoSelfAdaptiveLevel:Lcom/android/server/tv/DataBaseDeskImpl$S3D_TABLE_FIELD;

    new-instance v0, Lcom/android/server/tv/DataBaseDeskImpl$S3D_TABLE_FIELD;

    const-string v1, "eThreeDVideo3DTo2D"

    const/16 v2, 0x8

    invoke-direct {v0, v1, v2}, Lcom/android/server/tv/DataBaseDeskImpl$S3D_TABLE_FIELD;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/android/server/tv/DataBaseDeskImpl$S3D_TABLE_FIELD;->eThreeDVideo3DTo2D:Lcom/android/server/tv/DataBaseDeskImpl$S3D_TABLE_FIELD;

    new-instance v0, Lcom/android/server/tv/DataBaseDeskImpl$S3D_TABLE_FIELD;

    const-string v1, "eThreeDVideo3DOffset"

    const/16 v2, 0x9

    invoke-direct {v0, v1, v2}, Lcom/android/server/tv/DataBaseDeskImpl$S3D_TABLE_FIELD;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/android/server/tv/DataBaseDeskImpl$S3D_TABLE_FIELD;->eThreeDVideo3DOffset:Lcom/android/server/tv/DataBaseDeskImpl$S3D_TABLE_FIELD;

    const/16 v0, 0xa

    new-array v0, v0, [Lcom/android/server/tv/DataBaseDeskImpl$S3D_TABLE_FIELD;

    sget-object v1, Lcom/android/server/tv/DataBaseDeskImpl$S3D_TABLE_FIELD;->eThreeDVideo:Lcom/android/server/tv/DataBaseDeskImpl$S3D_TABLE_FIELD;

    aput-object v1, v0, v3

    sget-object v1, Lcom/android/server/tv/DataBaseDeskImpl$S3D_TABLE_FIELD;->eThreeDVideoDisplayFormat:Lcom/android/server/tv/DataBaseDeskImpl$S3D_TABLE_FIELD;

    aput-object v1, v0, v4

    sget-object v1, Lcom/android/server/tv/DataBaseDeskImpl$S3D_TABLE_FIELD;->eThreeDVideo3DDepth:Lcom/android/server/tv/DataBaseDeskImpl$S3D_TABLE_FIELD;

    aput-object v1, v0, v5

    sget-object v1, Lcom/android/server/tv/DataBaseDeskImpl$S3D_TABLE_FIELD;->eThreeDVideoAutoStart:Lcom/android/server/tv/DataBaseDeskImpl$S3D_TABLE_FIELD;

    aput-object v1, v0, v6

    sget-object v1, Lcom/android/server/tv/DataBaseDeskImpl$S3D_TABLE_FIELD;->eThreeDVideo3DOutputAspect:Lcom/android/server/tv/DataBaseDeskImpl$S3D_TABLE_FIELD;

    aput-object v1, v0, v7

    const/4 v1, 0x5

    sget-object v2, Lcom/android/server/tv/DataBaseDeskImpl$S3D_TABLE_FIELD;->eThreeDVideoLRViewSwitch:Lcom/android/server/tv/DataBaseDeskImpl$S3D_TABLE_FIELD;

    aput-object v2, v0, v1

    const/4 v1, 0x6

    sget-object v2, Lcom/android/server/tv/DataBaseDeskImpl$S3D_TABLE_FIELD;->eThreeDVideoSelfAdaptiveDetect:Lcom/android/server/tv/DataBaseDeskImpl$S3D_TABLE_FIELD;

    aput-object v2, v0, v1

    const/4 v1, 0x7

    sget-object v2, Lcom/android/server/tv/DataBaseDeskImpl$S3D_TABLE_FIELD;->eThreeDVideoSelfAdaptiveLevel:Lcom/android/server/tv/DataBaseDeskImpl$S3D_TABLE_FIELD;

    aput-object v2, v0, v1

    const/16 v1, 0x8

    sget-object v2, Lcom/android/server/tv/DataBaseDeskImpl$S3D_TABLE_FIELD;->eThreeDVideo3DTo2D:Lcom/android/server/tv/DataBaseDeskImpl$S3D_TABLE_FIELD;

    aput-object v2, v0, v1

    const/16 v1, 0x9

    sget-object v2, Lcom/android/server/tv/DataBaseDeskImpl$S3D_TABLE_FIELD;->eThreeDVideo3DOffset:Lcom/android/server/tv/DataBaseDeskImpl$S3D_TABLE_FIELD;

    aput-object v2, v0, v1

    sput-object v0, Lcom/android/server/tv/DataBaseDeskImpl$S3D_TABLE_FIELD;->$VALUES:[Lcom/android/server/tv/DataBaseDeskImpl$S3D_TABLE_FIELD;

    return-void
.end method

.method private constructor <init>(Ljava/lang/String;I)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()V"
        }
    .end annotation

    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    return-void
.end method

.method public static valueOf(Ljava/lang/String;)Lcom/android/server/tv/DataBaseDeskImpl$S3D_TABLE_FIELD;
    .locals 1
    .param p0    # Ljava/lang/String;

    const-class v0, Lcom/android/server/tv/DataBaseDeskImpl$S3D_TABLE_FIELD;

    invoke-static {v0, p0}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;

    move-result-object v0

    check-cast v0, Lcom/android/server/tv/DataBaseDeskImpl$S3D_TABLE_FIELD;

    return-object v0
.end method

.method public static values()[Lcom/android/server/tv/DataBaseDeskImpl$S3D_TABLE_FIELD;
    .locals 1

    sget-object v0, Lcom/android/server/tv/DataBaseDeskImpl$S3D_TABLE_FIELD;->$VALUES:[Lcom/android/server/tv/DataBaseDeskImpl$S3D_TABLE_FIELD;

    invoke-virtual {v0}, [Lcom/android/server/tv/DataBaseDeskImpl$S3D_TABLE_FIELD;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [Lcom/android/server/tv/DataBaseDeskImpl$S3D_TABLE_FIELD;

    return-object v0
.end method
