.class public Lcom/android/server/tv/TvAudioClient;
.super Lcom/mstar/android/tv/ITvAudio$Stub;
.source "TvAudioClient.java"


# static fields
.field private static final TAG:Ljava/lang/String; = "TvAudioClient"


# instance fields
.field private databaseMgr:Lcom/android/server/tv/DataBaseDeskImpl;

.field private handler:Landroid/os/Handler;


# direct methods
.method public constructor <init>(Landroid/content/Context;Lcom/android/server/tv/TvHanlder;Lcom/android/server/tv/DataBaseDeskImpl;)V
    .locals 1
    .param p1    # Landroid/content/Context;
    .param p2    # Lcom/android/server/tv/TvHanlder;
    .param p3    # Lcom/android/server/tv/DataBaseDeskImpl;

    invoke-direct {p0}, Lcom/mstar/android/tv/ITvAudio$Stub;-><init>()V

    invoke-static {p1}, Lcom/android/server/tv/DataBaseDeskImpl;->getDataBaseMgrInstance(Landroid/content/Context;)Lcom/android/server/tv/DataBaseDeskImpl;

    move-result-object v0

    iput-object v0, p0, Lcom/android/server/tv/TvAudioClient;->databaseMgr:Lcom/android/server/tv/DataBaseDeskImpl;

    iput-object p2, p0, Lcom/android/server/tv/TvAudioClient;->handler:Landroid/os/Handler;

    return-void
.end method


# virtual methods
.method public getAvcMode()Z
    .locals 3
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Landroid/os/RemoteException;
        }
    .end annotation

    const/4 v0, 0x1

    const-string v1, "TvAudioClient"

    const-string v2, "getAvcMode()"

    invoke-static {v1, v2}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    iget-object v1, p0, Lcom/android/server/tv/TvAudioClient;->databaseMgr:Lcom/android/server/tv/DataBaseDeskImpl;

    invoke-virtual {v1}, Lcom/android/server/tv/DataBaseDeskImpl;->queryAvc()I

    move-result v1

    if-ne v1, v0, :cond_0

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public getBalance()I
    .locals 4
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Landroid/os/RemoteException;
        }
    .end annotation

    iget-object v1, p0, Lcom/android/server/tv/TvAudioClient;->databaseMgr:Lcom/android/server/tv/DataBaseDeskImpl;

    invoke-virtual {v1}, Lcom/android/server/tv/DataBaseDeskImpl;->queryBalance()I

    move-result v0

    const-string v1, "TvAudioClient"

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "getBalance(), return int "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    return v0
.end method

.method public getBass()I
    .locals 5
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Landroid/os/RemoteException;
        }
    .end annotation

    iget-object v2, p0, Lcom/android/server/tv/TvAudioClient;->databaseMgr:Lcom/android/server/tv/DataBaseDeskImpl;

    invoke-virtual {v2}, Lcom/android/server/tv/DataBaseDeskImpl;->querySoundMode()I

    move-result v1

    iget-object v2, p0, Lcom/android/server/tv/TvAudioClient;->databaseMgr:Lcom/android/server/tv/DataBaseDeskImpl;

    invoke-virtual {v2, v1}, Lcom/android/server/tv/DataBaseDeskImpl;->queryBass(I)I

    move-result v0

    const-string v2, "TvAudioClient"

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string v4, "getBass(), return int "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3, v0}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v2, v3}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    return v0
.end method

.method public getBassSwitch()I
    .locals 4
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Landroid/os/RemoteException;
        }
    .end annotation

    iget-object v1, p0, Lcom/android/server/tv/TvAudioClient;->databaseMgr:Lcom/android/server/tv/DataBaseDeskImpl;

    invoke-virtual {v1}, Lcom/android/server/tv/DataBaseDeskImpl;->queryBassSwitch()I

    move-result v0

    const-string v1, "TvAudioClient"

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "getBassSwitch(), return int "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    return v0
.end method

.method public getBassVolume()I
    .locals 4
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Landroid/os/RemoteException;
        }
    .end annotation

    iget-object v1, p0, Lcom/android/server/tv/TvAudioClient;->databaseMgr:Lcom/android/server/tv/DataBaseDeskImpl;

    invoke-virtual {v1}, Lcom/android/server/tv/DataBaseDeskImpl;->queryBassVolume()I

    move-result v0

    const-string v1, "TvAudioClient"

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "getBassVolume(), return int "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    return v0
.end method

.method public getDtvOutputMode()I
    .locals 6
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Landroid/os/RemoteException;
        }
    .end annotation

    const/4 v0, 0x0

    :try_start_0
    invoke-static {}, Lcom/mstar/android/tvapi/common/TvManager;->getInstance()Lcom/mstar/android/tvapi/common/TvManager;

    move-result-object v3

    if-eqz v3, :cond_0

    invoke-static {}, Lcom/mstar/android/tvapi/common/TvManager;->getInstance()Lcom/mstar/android/tvapi/common/TvManager;

    move-result-object v3

    invoke-virtual {v3}, Lcom/mstar/android/tvapi/common/TvManager;->getAudioManager()Lcom/mstar/android/tvapi/common/AudioManager;

    move-result-object v3

    invoke-virtual {v3}, Lcom/mstar/android/tvapi/common/AudioManager;->getDtvOutputMode()Lcom/mstar/android/tvapi/common/vo/EnumDtvSoundMode;
    :try_end_0
    .catch Lcom/mstar/android/tvapi/common/exception/TvCommonException; {:try_start_0 .. :try_end_0} :catch_0

    move-result-object v0

    :cond_0
    :goto_0
    invoke-virtual {v0}, Lcom/mstar/android/tvapi/common/vo/EnumDtvSoundMode;->ordinal()I

    move-result v2

    const-string v3, "TvAudioClient"

    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    const-string v5, "getDtvOutputMode(), return int "

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-static {v3, v4}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    return v2

    :catch_0
    move-exception v1

    invoke-virtual {v1}, Lcom/mstar/android/tvapi/common/exception/TvCommonException;->printStackTrace()V

    goto :goto_0
.end method

.method public getEarPhoneVolume()I
    .locals 4
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Landroid/os/RemoteException;
        }
    .end annotation

    iget-object v1, p0, Lcom/android/server/tv/TvAudioClient;->databaseMgr:Lcom/android/server/tv/DataBaseDeskImpl;

    invoke-virtual {v1}, Lcom/android/server/tv/DataBaseDeskImpl;->queryEarPhoneVolme()I

    move-result v0

    const-string v1, "TvAudioClient"

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "getEarPhoneVolume(), return int "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    return v0
.end method

.method public getEqBand10k()I
    .locals 6
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Landroid/os/RemoteException;
        }
    .end annotation

    iget-object v3, p0, Lcom/android/server/tv/TvAudioClient;->databaseMgr:Lcom/android/server/tv/DataBaseDeskImpl;

    invoke-virtual {v3}, Lcom/android/server/tv/DataBaseDeskImpl;->querySoundMode()I

    move-result v2

    iget-object v3, p0, Lcom/android/server/tv/TvAudioClient;->databaseMgr:Lcom/android/server/tv/DataBaseDeskImpl;

    invoke-virtual {v3, v2}, Lcom/android/server/tv/DataBaseDeskImpl;->querySoundModeSetting(I)Lcom/android/server/tv/DataBaseDesk$SoundModeSeting;

    move-result-object v1

    iget-short v0, v1, Lcom/android/server/tv/DataBaseDesk$SoundModeSeting;->EqBand5:S

    const-string v3, "TvAudioClient"

    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    const-string v5, "getEqBand10k(), return int "

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4, v0}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-static {v3, v4}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    return v0
.end method

.method public getEqBand120()I
    .locals 6
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Landroid/os/RemoteException;
        }
    .end annotation

    iget-object v3, p0, Lcom/android/server/tv/TvAudioClient;->databaseMgr:Lcom/android/server/tv/DataBaseDeskImpl;

    invoke-virtual {v3}, Lcom/android/server/tv/DataBaseDeskImpl;->querySoundMode()I

    move-result v2

    iget-object v3, p0, Lcom/android/server/tv/TvAudioClient;->databaseMgr:Lcom/android/server/tv/DataBaseDeskImpl;

    invoke-virtual {v3, v2}, Lcom/android/server/tv/DataBaseDeskImpl;->querySoundModeSetting(I)Lcom/android/server/tv/DataBaseDesk$SoundModeSeting;

    move-result-object v1

    iget-short v0, v1, Lcom/android/server/tv/DataBaseDesk$SoundModeSeting;->EqBand1:S

    const-string v3, "TvAudioClient"

    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    const-string v5, "getEqBand120(), return int "

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4, v0}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-static {v3, v4}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    return v0
.end method

.method public getEqBand1500()I
    .locals 6
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Landroid/os/RemoteException;
        }
    .end annotation

    iget-object v3, p0, Lcom/android/server/tv/TvAudioClient;->databaseMgr:Lcom/android/server/tv/DataBaseDeskImpl;

    invoke-virtual {v3}, Lcom/android/server/tv/DataBaseDeskImpl;->querySoundMode()I

    move-result v2

    iget-object v3, p0, Lcom/android/server/tv/TvAudioClient;->databaseMgr:Lcom/android/server/tv/DataBaseDeskImpl;

    invoke-virtual {v3, v2}, Lcom/android/server/tv/DataBaseDeskImpl;->querySoundModeSetting(I)Lcom/android/server/tv/DataBaseDesk$SoundModeSeting;

    move-result-object v1

    iget-short v0, v1, Lcom/android/server/tv/DataBaseDesk$SoundModeSeting;->EqBand3:S

    const-string v3, "TvAudioClient"

    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    const-string v5, "getEqBand1500(), return int "

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4, v0}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-static {v3, v4}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    return v0
.end method

.method public getEqBand500()I
    .locals 6
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Landroid/os/RemoteException;
        }
    .end annotation

    iget-object v3, p0, Lcom/android/server/tv/TvAudioClient;->databaseMgr:Lcom/android/server/tv/DataBaseDeskImpl;

    invoke-virtual {v3}, Lcom/android/server/tv/DataBaseDeskImpl;->querySoundMode()I

    move-result v2

    iget-object v3, p0, Lcom/android/server/tv/TvAudioClient;->databaseMgr:Lcom/android/server/tv/DataBaseDeskImpl;

    invoke-virtual {v3, v2}, Lcom/android/server/tv/DataBaseDeskImpl;->querySoundModeSetting(I)Lcom/android/server/tv/DataBaseDesk$SoundModeSeting;

    move-result-object v1

    iget-short v0, v1, Lcom/android/server/tv/DataBaseDesk$SoundModeSeting;->EqBand2:S

    const-string v3, "TvAudioClient"

    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    const-string v5, "getEqBand500(), return int "

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4, v0}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-static {v3, v4}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    return v0
.end method

.method public getEqBand5k()I
    .locals 6
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Landroid/os/RemoteException;
        }
    .end annotation

    iget-object v3, p0, Lcom/android/server/tv/TvAudioClient;->databaseMgr:Lcom/android/server/tv/DataBaseDeskImpl;

    invoke-virtual {v3}, Lcom/android/server/tv/DataBaseDeskImpl;->querySoundMode()I

    move-result v2

    iget-object v3, p0, Lcom/android/server/tv/TvAudioClient;->databaseMgr:Lcom/android/server/tv/DataBaseDeskImpl;

    invoke-virtual {v3, v2}, Lcom/android/server/tv/DataBaseDeskImpl;->querySoundModeSetting(I)Lcom/android/server/tv/DataBaseDesk$SoundModeSeting;

    move-result-object v1

    iget-short v0, v1, Lcom/android/server/tv/DataBaseDesk$SoundModeSeting;->EqBand4:S

    const-string v3, "TvAudioClient"

    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    const-string v5, "getEqBand5k(),return int "

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4, v0}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-static {v3, v4}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    return v0
.end method

.method public getPowerOnOffMusic()I
    .locals 4
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Landroid/os/RemoteException;
        }
    .end annotation

    iget-object v1, p0, Lcom/android/server/tv/TvAudioClient;->databaseMgr:Lcom/android/server/tv/DataBaseDeskImpl;

    invoke-virtual {v1}, Lcom/android/server/tv/DataBaseDeskImpl;->queryPowerOnOffMusic()I

    move-result v0

    const-string v1, "TvAudioClient"

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "getPowerOnOffMusic(), return int "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    return v0
.end method

.method public getSeparateHear()I
    .locals 4
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Landroid/os/RemoteException;
        }
    .end annotation

    iget-object v1, p0, Lcom/android/server/tv/TvAudioClient;->databaseMgr:Lcom/android/server/tv/DataBaseDeskImpl;

    invoke-virtual {v1}, Lcom/android/server/tv/DataBaseDeskImpl;->querySeparateHear()I

    move-result v0

    const-string v1, "TvAudioClient"

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "getSeparateHear(), return int "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    return v0
.end method

.method public getSoundMode()I
    .locals 4
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Landroid/os/RemoteException;
        }
    .end annotation

    iget-object v1, p0, Lcom/android/server/tv/TvAudioClient;->databaseMgr:Lcom/android/server/tv/DataBaseDeskImpl;

    invoke-virtual {v1}, Lcom/android/server/tv/DataBaseDeskImpl;->querySoundMode()I

    move-result v0

    const-string v1, "TvAudioClient"

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "getSoundMode(), return int "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    return v0
.end method

.method public getSpdifOutMode()I
    .locals 4
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Landroid/os/RemoteException;
        }
    .end annotation

    iget-object v1, p0, Lcom/android/server/tv/TvAudioClient;->databaseMgr:Lcom/android/server/tv/DataBaseDeskImpl;

    invoke-virtual {v1}, Lcom/android/server/tv/DataBaseDeskImpl;->querySpdifMode()I

    move-result v0

    const-string v1, "TvAudioClient"

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "getSpdifOutMode(), return int "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    return v0
.end method

.method public getSurroundMode()I
    .locals 4
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Landroid/os/RemoteException;
        }
    .end annotation

    iget-object v1, p0, Lcom/android/server/tv/TvAudioClient;->databaseMgr:Lcom/android/server/tv/DataBaseDeskImpl;

    invoke-virtual {v1}, Lcom/android/server/tv/DataBaseDeskImpl;->querySrr()I

    move-result v0

    const-string v1, "TvAudioClient"

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "getSurroundMode(), return int "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    return v0
.end method

.method public getTreble()I
    .locals 5
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Landroid/os/RemoteException;
        }
    .end annotation

    iget-object v2, p0, Lcom/android/server/tv/TvAudioClient;->databaseMgr:Lcom/android/server/tv/DataBaseDeskImpl;

    invoke-virtual {v2}, Lcom/android/server/tv/DataBaseDeskImpl;->querySoundMode()I

    move-result v1

    iget-object v2, p0, Lcom/android/server/tv/TvAudioClient;->databaseMgr:Lcom/android/server/tv/DataBaseDeskImpl;

    invoke-virtual {v2, v1}, Lcom/android/server/tv/DataBaseDeskImpl;->queryTreble(I)I

    move-result v0

    const-string v2, "TvAudioClient"

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string v4, "getTreble(), return int "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3, v0}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v2, v3}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    return v0
.end method

.method public getTrueBass()I
    .locals 4
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Landroid/os/RemoteException;
        }
    .end annotation

    iget-object v1, p0, Lcom/android/server/tv/TvAudioClient;->databaseMgr:Lcom/android/server/tv/DataBaseDeskImpl;

    invoke-virtual {v1}, Lcom/android/server/tv/DataBaseDeskImpl;->queryTrueBass()I

    move-result v0

    const-string v1, "TvAudioClient"

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "getTrueBass(), return int "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    return v0
.end method

.method public getWallmusic()I
    .locals 4
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Landroid/os/RemoteException;
        }
    .end annotation

    iget-object v1, p0, Lcom/android/server/tv/TvAudioClient;->databaseMgr:Lcom/android/server/tv/DataBaseDeskImpl;

    invoke-virtual {v1}, Lcom/android/server/tv/DataBaseDeskImpl;->queryWallmusic()I

    move-result v0

    const-string v1, "TvAudioClient"

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "getWallmusic(), return int "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    return v0
.end method

.method public registerOnAudioEventListener(I)V
    .locals 0
    .param p1    # I
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Landroid/os/RemoteException;
        }
    .end annotation

    return-void
.end method

.method public setAvcMode(Z)Z
    .locals 5
    .param p1    # Z
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Landroid/os/RemoteException;
        }
    .end annotation

    const/4 v2, 0x1

    const-string v1, "TvAudioClient"

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string v4, "setAvcMode(), paras isAvcEnable = "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3, p1}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v1, v3}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    iget-object v3, p0, Lcom/android/server/tv/TvAudioClient;->databaseMgr:Lcom/android/server/tv/DataBaseDeskImpl;

    if-eqz p1, :cond_1

    move v1, v2

    :goto_0
    invoke-virtual {v3, v1}, Lcom/android/server/tv/DataBaseDeskImpl;->updateAvc(I)V

    :try_start_0
    invoke-static {}, Lcom/mstar/android/tvapi/common/TvManager;->getInstance()Lcom/mstar/android/tvapi/common/TvManager;

    move-result-object v1

    if-eqz v1, :cond_0

    invoke-static {}, Lcom/mstar/android/tvapi/common/TvManager;->getInstance()Lcom/mstar/android/tvapi/common/TvManager;

    move-result-object v1

    invoke-virtual {v1}, Lcom/mstar/android/tvapi/common/TvManager;->getAudioManager()Lcom/mstar/android/tvapi/common/AudioManager;

    move-result-object v1

    sget-object v3, Lcom/mstar/android/tvapi/common/vo/EnumSoundEffectType;->E_AVC:Lcom/mstar/android/tvapi/common/vo/EnumSoundEffectType;

    invoke-virtual {v1, v3, p1}, Lcom/mstar/android/tvapi/common/AudioManager;->enableBasicSoundEffect(Lcom/mstar/android/tvapi/common/vo/EnumSoundEffectType;Z)Lcom/mstar/android/tvapi/common/vo/EnumAudioReturn;
    :try_end_0
    .catch Lcom/mstar/android/tvapi/common/exception/TvCommonException; {:try_start_0 .. :try_end_0} :catch_0

    :cond_0
    :goto_1
    return v2

    :cond_1
    const/4 v1, 0x0

    goto :goto_0

    :catch_0
    move-exception v0

    invoke-virtual {v0}, Lcom/mstar/android/tvapi/common/exception/TvCommonException;->printStackTrace()V

    goto :goto_1
.end method

.method public setBalance(I)Z
    .locals 5
    .param p1    # I
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Landroid/os/RemoteException;
        }
    .end annotation

    const-string v2, "TvAudioClient"

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string v4, "setBalance(), paras balanceValue = "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3, p1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v2, v3}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    const/4 v0, 0x0

    new-instance v0, Lcom/mstar/android/tvapi/common/vo/DtvSoundEffect;

    invoke-direct {v0}, Lcom/mstar/android/tvapi/common/vo/DtvSoundEffect;-><init>()V

    :try_start_0
    iput p1, v0, Lcom/mstar/android/tvapi/common/vo/DtvSoundEffect;->balance:I

    invoke-static {}, Lcom/mstar/android/tvapi/common/TvManager;->getInstance()Lcom/mstar/android/tvapi/common/TvManager;

    move-result-object v2

    if-eqz v2, :cond_0

    invoke-static {}, Lcom/mstar/android/tvapi/common/TvManager;->getInstance()Lcom/mstar/android/tvapi/common/TvManager;

    move-result-object v2

    invoke-virtual {v2}, Lcom/mstar/android/tvapi/common/TvManager;->getAudioManager()Lcom/mstar/android/tvapi/common/AudioManager;

    move-result-object v2

    sget-object v3, Lcom/mstar/android/tvapi/common/vo/EnumSoundEffectType;->E_BALANCE:Lcom/mstar/android/tvapi/common/vo/EnumSoundEffectType;

    invoke-virtual {v2, v3, v0}, Lcom/mstar/android/tvapi/common/AudioManager;->setBasicSoundEffect(Lcom/mstar/android/tvapi/common/vo/EnumSoundEffectType;Lcom/mstar/android/tvapi/common/vo/DtvSoundEffect;)Lcom/mstar/android/tvapi/common/vo/EnumAudioReturn;
    :try_end_0
    .catch Lcom/mstar/android/tvapi/common/exception/TvCommonException; {:try_start_0 .. :try_end_0} :catch_0

    :cond_0
    :goto_0
    iget-object v2, p0, Lcom/android/server/tv/TvAudioClient;->databaseMgr:Lcom/android/server/tv/DataBaseDeskImpl;

    invoke-virtual {v2, p1}, Lcom/android/server/tv/DataBaseDeskImpl;->updateBalance(I)V

    const/4 v2, 0x1

    return v2

    :catch_0
    move-exception v1

    invoke-virtual {v1}, Lcom/mstar/android/tvapi/common/exception/TvCommonException;->printStackTrace()V

    goto :goto_0
.end method

.method public setBass(I)Z
    .locals 8
    .param p1    # I
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Landroid/os/RemoteException;
        }
    .end annotation

    const/4 v7, 0x1

    const-string v4, "TvAudioClient"

    new-instance v5, Ljava/lang/StringBuilder;

    invoke-direct {v5}, Ljava/lang/StringBuilder;-><init>()V

    const-string v6, "setBass(), paras bassValue= "

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5, p1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    invoke-static {v4, v5}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    const/4 v0, 0x0

    new-instance v0, Lcom/mstar/android/tvapi/common/vo/DtvSoundEffect;

    invoke-direct {v0}, Lcom/mstar/android/tvapi/common/vo/DtvSoundEffect;-><init>()V

    iget-object v4, p0, Lcom/android/server/tv/TvAudioClient;->databaseMgr:Lcom/android/server/tv/DataBaseDeskImpl;

    invoke-virtual {v4}, Lcom/android/server/tv/DataBaseDeskImpl;->querySoundMode()I

    move-result v3

    iget-object v4, p0, Lcom/android/server/tv/TvAudioClient;->databaseMgr:Lcom/android/server/tv/DataBaseDeskImpl;

    invoke-virtual {v4, v3}, Lcom/android/server/tv/DataBaseDeskImpl;->querySoundModeSetting(I)Lcom/android/server/tv/DataBaseDesk$SoundModeSeting;

    move-result-object v2

    int-to-short v4, p1

    :try_start_0
    iput-short v4, v2, Lcom/android/server/tv/DataBaseDesk$SoundModeSeting;->EqBand1:S

    iget-object v4, v0, Lcom/mstar/android/tvapi/common/vo/DtvSoundEffect;->soundParameterEqs:[Lcom/mstar/android/tvapi/common/vo/SoundParameterEq;

    const/4 v5, 0x0

    aget-object v4, v4, v5

    iput p1, v4, Lcom/mstar/android/tvapi/common/vo/SoundParameterEq;->eqLevel:I

    iget-object v4, v0, Lcom/mstar/android/tvapi/common/vo/DtvSoundEffect;->soundParameterEqs:[Lcom/mstar/android/tvapi/common/vo/SoundParameterEq;

    const/4 v5, 0x1

    aget-object v4, v4, v5

    iget-short v5, v2, Lcom/android/server/tv/DataBaseDesk$SoundModeSeting;->EqBand2:S

    iput v5, v4, Lcom/mstar/android/tvapi/common/vo/SoundParameterEq;->eqLevel:I

    iget-object v4, v0, Lcom/mstar/android/tvapi/common/vo/DtvSoundEffect;->soundParameterEqs:[Lcom/mstar/android/tvapi/common/vo/SoundParameterEq;

    const/4 v5, 0x2

    aget-object v4, v4, v5

    iget-short v5, v2, Lcom/android/server/tv/DataBaseDesk$SoundModeSeting;->EqBand3:S

    iput v5, v4, Lcom/mstar/android/tvapi/common/vo/SoundParameterEq;->eqLevel:I

    iget-object v4, v0, Lcom/mstar/android/tvapi/common/vo/DtvSoundEffect;->soundParameterEqs:[Lcom/mstar/android/tvapi/common/vo/SoundParameterEq;

    const/4 v5, 0x3

    aget-object v4, v4, v5

    iget-short v5, v2, Lcom/android/server/tv/DataBaseDesk$SoundModeSeting;->EqBand4:S

    iput v5, v4, Lcom/mstar/android/tvapi/common/vo/SoundParameterEq;->eqLevel:I

    iget-object v4, v0, Lcom/mstar/android/tvapi/common/vo/DtvSoundEffect;->soundParameterEqs:[Lcom/mstar/android/tvapi/common/vo/SoundParameterEq;

    const/4 v5, 0x4

    aget-object v4, v4, v5

    iget-short v5, v2, Lcom/android/server/tv/DataBaseDesk$SoundModeSeting;->EqBand5:S

    iput v5, v4, Lcom/mstar/android/tvapi/common/vo/SoundParameterEq;->eqLevel:I

    const/4 v4, 0x5

    iput-short v4, v0, Lcom/mstar/android/tvapi/common/vo/DtvSoundEffect;->eqBandNumber:S

    invoke-static {}, Lcom/mstar/android/tvapi/common/TvManager;->getInstance()Lcom/mstar/android/tvapi/common/TvManager;

    move-result-object v4

    if-eqz v4, :cond_0

    invoke-static {}, Lcom/mstar/android/tvapi/common/TvManager;->getInstance()Lcom/mstar/android/tvapi/common/TvManager;

    move-result-object v4

    invoke-virtual {v4}, Lcom/mstar/android/tvapi/common/TvManager;->getAudioManager()Lcom/mstar/android/tvapi/common/AudioManager;

    move-result-object v4

    sget-object v5, Lcom/mstar/android/tvapi/common/vo/EnumSoundEffectType;->E_EQ:Lcom/mstar/android/tvapi/common/vo/EnumSoundEffectType;

    invoke-virtual {v4, v5, v0}, Lcom/mstar/android/tvapi/common/AudioManager;->setBasicSoundEffect(Lcom/mstar/android/tvapi/common/vo/EnumSoundEffectType;Lcom/mstar/android/tvapi/common/vo/DtvSoundEffect;)Lcom/mstar/android/tvapi/common/vo/EnumAudioReturn;
    :try_end_0
    .catch Lcom/mstar/android/tvapi/common/exception/TvCommonException; {:try_start_0 .. :try_end_0} :catch_0

    :cond_0
    :goto_0
    iget-object v4, p0, Lcom/android/server/tv/TvAudioClient;->databaseMgr:Lcom/android/server/tv/DataBaseDeskImpl;

    invoke-virtual {v4, p1, v3}, Lcom/android/server/tv/DataBaseDeskImpl;->updateBass(II)V

    return v7

    :catch_0
    move-exception v1

    invoke-virtual {v1}, Lcom/mstar/android/tvapi/common/exception/TvCommonException;->printStackTrace()V

    goto :goto_0
.end method

.method public setBassSwitch(I)Z
    .locals 3
    .param p1    # I
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Landroid/os/RemoteException;
        }
    .end annotation

    const-string v0, "TvAudioClient"

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "setBassSwitch(), paras en = "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    iget-object v0, p0, Lcom/android/server/tv/TvAudioClient;->databaseMgr:Lcom/android/server/tv/DataBaseDeskImpl;

    invoke-virtual {v0, p1}, Lcom/android/server/tv/DataBaseDeskImpl;->updateBassSwitch(I)V

    const/4 v0, 0x1

    return v0
.end method

.method public setBassVolume(I)Z
    .locals 3
    .param p1    # I
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Landroid/os/RemoteException;
        }
    .end annotation

    const-string v0, "TvAudioClient"

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "setBassVolume(), paras volume = "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    iget-object v0, p0, Lcom/android/server/tv/TvAudioClient;->databaseMgr:Lcom/android/server/tv/DataBaseDeskImpl;

    invoke-virtual {v0, p1}, Lcom/android/server/tv/DataBaseDeskImpl;->updateBassVolume(I)V

    const/4 v0, 0x1

    return v0
.end method

.method public setDtvOutputMode(I)V
    .locals 5
    .param p1    # I
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Landroid/os/RemoteException;
        }
    .end annotation

    const-string v2, "TvAudioClient"

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string v4, "setDtvOutputMode(), paras enDtvSoundMode = "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3, p1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v2, v3}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    invoke-static {}, Lcom/mstar/android/tvapi/common/vo/EnumDtvSoundMode;->values()[Lcom/mstar/android/tvapi/common/vo/EnumDtvSoundMode;

    move-result-object v2

    aget-object v0, v2, p1

    :try_start_0
    invoke-static {}, Lcom/mstar/android/tvapi/common/TvManager;->getInstance()Lcom/mstar/android/tvapi/common/TvManager;

    move-result-object v2

    if-eqz v2, :cond_0

    invoke-static {}, Lcom/mstar/android/tvapi/common/TvManager;->getInstance()Lcom/mstar/android/tvapi/common/TvManager;

    move-result-object v2

    invoke-virtual {v2}, Lcom/mstar/android/tvapi/common/TvManager;->getAudioManager()Lcom/mstar/android/tvapi/common/AudioManager;

    move-result-object v2

    invoke-virtual {v2, v0}, Lcom/mstar/android/tvapi/common/AudioManager;->setDtvOutputMode(Lcom/mstar/android/tvapi/common/vo/EnumDtvSoundMode;)V
    :try_end_0
    .catch Lcom/mstar/android/tvapi/common/exception/TvCommonException; {:try_start_0 .. :try_end_0} :catch_0

    :cond_0
    :goto_0
    return-void

    :catch_0
    move-exception v1

    invoke-virtual {v1}, Lcom/mstar/android/tvapi/common/exception/TvCommonException;->printStackTrace()V

    goto :goto_0
.end method

.method public setEarPhoneVolume(I)Z
    .locals 4
    .param p1    # I
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Landroid/os/RemoteException;
        }
    .end annotation

    const-string v1, "TvAudioClient"

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "setEarPhoneVolume(), paras volume = "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, p1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    :try_start_0
    invoke-static {}, Lcom/mstar/android/tvapi/common/TvManager;->getInstance()Lcom/mstar/android/tvapi/common/TvManager;

    move-result-object v1

    if-eqz v1, :cond_0

    invoke-static {}, Lcom/mstar/android/tvapi/common/TvManager;->getInstance()Lcom/mstar/android/tvapi/common/TvManager;

    move-result-object v1

    invoke-virtual {v1}, Lcom/mstar/android/tvapi/common/TvManager;->getAudioManager()Lcom/mstar/android/tvapi/common/AudioManager;

    move-result-object v1

    sget-object v2, Lcom/mstar/android/tvapi/common/vo/EnumAudioVolumeSourceType;->E_VOL_SOURCE_HP_OUT:Lcom/mstar/android/tvapi/common/vo/EnumAudioVolumeSourceType;

    int-to-byte v3, p1

    invoke-virtual {v1, v2, v3}, Lcom/mstar/android/tvapi/common/AudioManager;->setAudioVolume(Lcom/mstar/android/tvapi/common/vo/EnumAudioVolumeSourceType;B)V
    :try_end_0
    .catch Lcom/mstar/android/tvapi/common/exception/TvCommonException; {:try_start_0 .. :try_end_0} :catch_0

    :cond_0
    :goto_0
    iget-object v1, p0, Lcom/android/server/tv/TvAudioClient;->databaseMgr:Lcom/android/server/tv/DataBaseDeskImpl;

    invoke-virtual {v1, p1}, Lcom/android/server/tv/DataBaseDeskImpl;->updateEarPhoneVolume(I)V

    const/4 v1, 0x1

    return v1

    :catch_0
    move-exception v0

    invoke-virtual {v0}, Lcom/mstar/android/tvapi/common/exception/TvCommonException;->printStackTrace()V

    goto :goto_0
.end method

.method public setEqBand10k(I)Z
    .locals 8
    .param p1    # I
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Landroid/os/RemoteException;
        }
    .end annotation

    const/4 v7, 0x1

    const-string v4, "TvAudioClient"

    new-instance v5, Ljava/lang/StringBuilder;

    invoke-direct {v5}, Ljava/lang/StringBuilder;-><init>()V

    const-string v6, "setEqBand10k(), paras eqValue = "

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5, p1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    invoke-static {v4, v5}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    iget-object v4, p0, Lcom/android/server/tv/TvAudioClient;->databaseMgr:Lcom/android/server/tv/DataBaseDeskImpl;

    invoke-virtual {v4}, Lcom/android/server/tv/DataBaseDeskImpl;->querySoundMode()I

    move-result v3

    iget-object v4, p0, Lcom/android/server/tv/TvAudioClient;->databaseMgr:Lcom/android/server/tv/DataBaseDeskImpl;

    invoke-virtual {v4, v3}, Lcom/android/server/tv/DataBaseDeskImpl;->querySoundModeSetting(I)Lcom/android/server/tv/DataBaseDesk$SoundModeSeting;

    move-result-object v2

    int-to-short v4, p1

    iput-short v4, v2, Lcom/android/server/tv/DataBaseDesk$SoundModeSeting;->EqBand5:S

    new-instance v0, Lcom/mstar/android/tvapi/common/vo/DtvSoundEffect;

    invoke-direct {v0}, Lcom/mstar/android/tvapi/common/vo/DtvSoundEffect;-><init>()V

    sget-object v4, Lcom/android/server/tv/DataBaseDesk$EN_SOUND_MODE;->SOUND_MODE_NUM:Lcom/android/server/tv/DataBaseDesk$EN_SOUND_MODE;

    invoke-virtual {v4}, Lcom/android/server/tv/DataBaseDesk$EN_SOUND_MODE;->ordinal()I

    move-result v4

    int-to-short v4, v4

    iput-short v4, v0, Lcom/mstar/android/tvapi/common/vo/DtvSoundEffect;->eqBandNumber:S

    :try_start_0
    iget-object v4, v0, Lcom/mstar/android/tvapi/common/vo/DtvSoundEffect;->soundParameterEqs:[Lcom/mstar/android/tvapi/common/vo/SoundParameterEq;

    const/4 v5, 0x0

    aget-object v4, v4, v5

    iget-short v5, v2, Lcom/android/server/tv/DataBaseDesk$SoundModeSeting;->EqBand1:S

    iput v5, v4, Lcom/mstar/android/tvapi/common/vo/SoundParameterEq;->eqLevel:I

    iget-object v4, v0, Lcom/mstar/android/tvapi/common/vo/DtvSoundEffect;->soundParameterEqs:[Lcom/mstar/android/tvapi/common/vo/SoundParameterEq;

    const/4 v5, 0x1

    aget-object v4, v4, v5

    iget-short v5, v2, Lcom/android/server/tv/DataBaseDesk$SoundModeSeting;->EqBand2:S

    iput v5, v4, Lcom/mstar/android/tvapi/common/vo/SoundParameterEq;->eqLevel:I

    iget-object v4, v0, Lcom/mstar/android/tvapi/common/vo/DtvSoundEffect;->soundParameterEqs:[Lcom/mstar/android/tvapi/common/vo/SoundParameterEq;

    const/4 v5, 0x2

    aget-object v4, v4, v5

    iget-short v5, v2, Lcom/android/server/tv/DataBaseDesk$SoundModeSeting;->EqBand3:S

    iput v5, v4, Lcom/mstar/android/tvapi/common/vo/SoundParameterEq;->eqLevel:I

    iget-object v4, v0, Lcom/mstar/android/tvapi/common/vo/DtvSoundEffect;->soundParameterEqs:[Lcom/mstar/android/tvapi/common/vo/SoundParameterEq;

    const/4 v5, 0x3

    aget-object v4, v4, v5

    iget-short v5, v2, Lcom/android/server/tv/DataBaseDesk$SoundModeSeting;->EqBand4:S

    iput v5, v4, Lcom/mstar/android/tvapi/common/vo/SoundParameterEq;->eqLevel:I

    iget-object v4, v0, Lcom/mstar/android/tvapi/common/vo/DtvSoundEffect;->soundParameterEqs:[Lcom/mstar/android/tvapi/common/vo/SoundParameterEq;

    const/4 v5, 0x4

    aget-object v4, v4, v5

    iget-short v5, v2, Lcom/android/server/tv/DataBaseDesk$SoundModeSeting;->EqBand5:S

    iput v5, v4, Lcom/mstar/android/tvapi/common/vo/SoundParameterEq;->eqLevel:I

    const/4 v4, 0x5

    iput-short v4, v0, Lcom/mstar/android/tvapi/common/vo/DtvSoundEffect;->eqBandNumber:S

    invoke-static {}, Lcom/mstar/android/tvapi/common/TvManager;->getInstance()Lcom/mstar/android/tvapi/common/TvManager;

    move-result-object v4

    if-eqz v4, :cond_0

    invoke-static {}, Lcom/mstar/android/tvapi/common/TvManager;->getInstance()Lcom/mstar/android/tvapi/common/TvManager;

    move-result-object v4

    invoke-virtual {v4}, Lcom/mstar/android/tvapi/common/TvManager;->getAudioManager()Lcom/mstar/android/tvapi/common/AudioManager;

    move-result-object v4

    sget-object v5, Lcom/mstar/android/tvapi/common/vo/EnumSoundEffectType;->E_EQ:Lcom/mstar/android/tvapi/common/vo/EnumSoundEffectType;

    invoke-virtual {v4, v5, v0}, Lcom/mstar/android/tvapi/common/AudioManager;->setBasicSoundEffect(Lcom/mstar/android/tvapi/common/vo/EnumSoundEffectType;Lcom/mstar/android/tvapi/common/vo/DtvSoundEffect;)Lcom/mstar/android/tvapi/common/vo/EnumAudioReturn;
    :try_end_0
    .catch Lcom/mstar/android/tvapi/common/exception/TvCommonException; {:try_start_0 .. :try_end_0} :catch_0

    :cond_0
    :goto_0
    iget-object v4, p0, Lcom/android/server/tv/TvAudioClient;->databaseMgr:Lcom/android/server/tv/DataBaseDeskImpl;

    invoke-virtual {v4, v2, v3}, Lcom/android/server/tv/DataBaseDeskImpl;->updateSoundModeSetting(Lcom/android/server/tv/DataBaseDesk$SoundModeSeting;I)V

    return v7

    :catch_0
    move-exception v1

    invoke-virtual {v1}, Lcom/mstar/android/tvapi/common/exception/TvCommonException;->printStackTrace()V

    goto :goto_0
.end method

.method public setEqBand120(I)Z
    .locals 8
    .param p1    # I
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Landroid/os/RemoteException;
        }
    .end annotation

    const/4 v7, 0x1

    const-string v4, "TvAudioClient"

    new-instance v5, Ljava/lang/StringBuilder;

    invoke-direct {v5}, Ljava/lang/StringBuilder;-><init>()V

    const-string v6, "setEqBand120(), paras eqValue = "

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5, p1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    invoke-static {v4, v5}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    iget-object v4, p0, Lcom/android/server/tv/TvAudioClient;->databaseMgr:Lcom/android/server/tv/DataBaseDeskImpl;

    invoke-virtual {v4}, Lcom/android/server/tv/DataBaseDeskImpl;->querySoundMode()I

    move-result v3

    iget-object v4, p0, Lcom/android/server/tv/TvAudioClient;->databaseMgr:Lcom/android/server/tv/DataBaseDeskImpl;

    invoke-virtual {v4, v3}, Lcom/android/server/tv/DataBaseDeskImpl;->querySoundModeSetting(I)Lcom/android/server/tv/DataBaseDesk$SoundModeSeting;

    move-result-object v2

    int-to-short v4, p1

    iput-short v4, v2, Lcom/android/server/tv/DataBaseDesk$SoundModeSeting;->EqBand1:S

    new-instance v0, Lcom/mstar/android/tvapi/common/vo/DtvSoundEffect;

    invoke-direct {v0}, Lcom/mstar/android/tvapi/common/vo/DtvSoundEffect;-><init>()V

    sget-object v4, Lcom/android/server/tv/DataBaseDesk$EN_SOUND_MODE;->SOUND_MODE_NUM:Lcom/android/server/tv/DataBaseDesk$EN_SOUND_MODE;

    invoke-virtual {v4}, Lcom/android/server/tv/DataBaseDesk$EN_SOUND_MODE;->ordinal()I

    move-result v4

    int-to-short v4, v4

    iput-short v4, v0, Lcom/mstar/android/tvapi/common/vo/DtvSoundEffect;->eqBandNumber:S

    :try_start_0
    iget-object v4, v0, Lcom/mstar/android/tvapi/common/vo/DtvSoundEffect;->soundParameterEqs:[Lcom/mstar/android/tvapi/common/vo/SoundParameterEq;

    const/4 v5, 0x0

    aget-object v4, v4, v5

    iget-short v5, v2, Lcom/android/server/tv/DataBaseDesk$SoundModeSeting;->EqBand1:S

    iput v5, v4, Lcom/mstar/android/tvapi/common/vo/SoundParameterEq;->eqLevel:I

    iget-object v4, v0, Lcom/mstar/android/tvapi/common/vo/DtvSoundEffect;->soundParameterEqs:[Lcom/mstar/android/tvapi/common/vo/SoundParameterEq;

    const/4 v5, 0x1

    aget-object v4, v4, v5

    iget-short v5, v2, Lcom/android/server/tv/DataBaseDesk$SoundModeSeting;->EqBand2:S

    iput v5, v4, Lcom/mstar/android/tvapi/common/vo/SoundParameterEq;->eqLevel:I

    iget-object v4, v0, Lcom/mstar/android/tvapi/common/vo/DtvSoundEffect;->soundParameterEqs:[Lcom/mstar/android/tvapi/common/vo/SoundParameterEq;

    const/4 v5, 0x2

    aget-object v4, v4, v5

    iget-short v5, v2, Lcom/android/server/tv/DataBaseDesk$SoundModeSeting;->EqBand3:S

    iput v5, v4, Lcom/mstar/android/tvapi/common/vo/SoundParameterEq;->eqLevel:I

    iget-object v4, v0, Lcom/mstar/android/tvapi/common/vo/DtvSoundEffect;->soundParameterEqs:[Lcom/mstar/android/tvapi/common/vo/SoundParameterEq;

    const/4 v5, 0x3

    aget-object v4, v4, v5

    iget-short v5, v2, Lcom/android/server/tv/DataBaseDesk$SoundModeSeting;->EqBand4:S

    iput v5, v4, Lcom/mstar/android/tvapi/common/vo/SoundParameterEq;->eqLevel:I

    iget-object v4, v0, Lcom/mstar/android/tvapi/common/vo/DtvSoundEffect;->soundParameterEqs:[Lcom/mstar/android/tvapi/common/vo/SoundParameterEq;

    const/4 v5, 0x4

    aget-object v4, v4, v5

    iget-short v5, v2, Lcom/android/server/tv/DataBaseDesk$SoundModeSeting;->EqBand5:S

    iput v5, v4, Lcom/mstar/android/tvapi/common/vo/SoundParameterEq;->eqLevel:I

    const/4 v4, 0x5

    iput-short v4, v0, Lcom/mstar/android/tvapi/common/vo/DtvSoundEffect;->eqBandNumber:S

    invoke-static {}, Lcom/mstar/android/tvapi/common/TvManager;->getInstance()Lcom/mstar/android/tvapi/common/TvManager;

    move-result-object v4

    if-eqz v4, :cond_0

    invoke-static {}, Lcom/mstar/android/tvapi/common/TvManager;->getInstance()Lcom/mstar/android/tvapi/common/TvManager;

    move-result-object v4

    invoke-virtual {v4}, Lcom/mstar/android/tvapi/common/TvManager;->getAudioManager()Lcom/mstar/android/tvapi/common/AudioManager;

    move-result-object v4

    sget-object v5, Lcom/mstar/android/tvapi/common/vo/EnumSoundEffectType;->E_EQ:Lcom/mstar/android/tvapi/common/vo/EnumSoundEffectType;

    invoke-virtual {v4, v5, v0}, Lcom/mstar/android/tvapi/common/AudioManager;->setBasicSoundEffect(Lcom/mstar/android/tvapi/common/vo/EnumSoundEffectType;Lcom/mstar/android/tvapi/common/vo/DtvSoundEffect;)Lcom/mstar/android/tvapi/common/vo/EnumAudioReturn;
    :try_end_0
    .catch Lcom/mstar/android/tvapi/common/exception/TvCommonException; {:try_start_0 .. :try_end_0} :catch_0

    :cond_0
    :goto_0
    iget-object v4, p0, Lcom/android/server/tv/TvAudioClient;->databaseMgr:Lcom/android/server/tv/DataBaseDeskImpl;

    invoke-virtual {v4, v2, v3}, Lcom/android/server/tv/DataBaseDeskImpl;->updateSoundModeSetting(Lcom/android/server/tv/DataBaseDesk$SoundModeSeting;I)V

    return v7

    :catch_0
    move-exception v1

    invoke-virtual {v1}, Lcom/mstar/android/tvapi/common/exception/TvCommonException;->printStackTrace()V

    goto :goto_0
.end method

.method public setEqBand1500(I)Z
    .locals 8
    .param p1    # I
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Landroid/os/RemoteException;
        }
    .end annotation

    const/4 v7, 0x1

    const-string v4, "TvAudioClient"

    new-instance v5, Ljava/lang/StringBuilder;

    invoke-direct {v5}, Ljava/lang/StringBuilder;-><init>()V

    const-string v6, "setEqBand1500(), paras eqValue= "

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5, p1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    invoke-static {v4, v5}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    iget-object v4, p0, Lcom/android/server/tv/TvAudioClient;->databaseMgr:Lcom/android/server/tv/DataBaseDeskImpl;

    invoke-virtual {v4}, Lcom/android/server/tv/DataBaseDeskImpl;->querySoundMode()I

    move-result v3

    iget-object v4, p0, Lcom/android/server/tv/TvAudioClient;->databaseMgr:Lcom/android/server/tv/DataBaseDeskImpl;

    invoke-virtual {v4, v3}, Lcom/android/server/tv/DataBaseDeskImpl;->querySoundModeSetting(I)Lcom/android/server/tv/DataBaseDesk$SoundModeSeting;

    move-result-object v2

    int-to-short v4, p1

    iput-short v4, v2, Lcom/android/server/tv/DataBaseDesk$SoundModeSeting;->EqBand3:S

    new-instance v0, Lcom/mstar/android/tvapi/common/vo/DtvSoundEffect;

    invoke-direct {v0}, Lcom/mstar/android/tvapi/common/vo/DtvSoundEffect;-><init>()V

    sget-object v4, Lcom/android/server/tv/DataBaseDesk$EN_SOUND_MODE;->SOUND_MODE_NUM:Lcom/android/server/tv/DataBaseDesk$EN_SOUND_MODE;

    invoke-virtual {v4}, Lcom/android/server/tv/DataBaseDesk$EN_SOUND_MODE;->ordinal()I

    move-result v4

    int-to-short v4, v4

    iput-short v4, v0, Lcom/mstar/android/tvapi/common/vo/DtvSoundEffect;->eqBandNumber:S

    :try_start_0
    iget-object v4, v0, Lcom/mstar/android/tvapi/common/vo/DtvSoundEffect;->soundParameterEqs:[Lcom/mstar/android/tvapi/common/vo/SoundParameterEq;

    const/4 v5, 0x0

    aget-object v4, v4, v5

    iget-short v5, v2, Lcom/android/server/tv/DataBaseDesk$SoundModeSeting;->EqBand1:S

    iput v5, v4, Lcom/mstar/android/tvapi/common/vo/SoundParameterEq;->eqLevel:I

    iget-object v4, v0, Lcom/mstar/android/tvapi/common/vo/DtvSoundEffect;->soundParameterEqs:[Lcom/mstar/android/tvapi/common/vo/SoundParameterEq;

    const/4 v5, 0x1

    aget-object v4, v4, v5

    iget-short v5, v2, Lcom/android/server/tv/DataBaseDesk$SoundModeSeting;->EqBand2:S

    iput v5, v4, Lcom/mstar/android/tvapi/common/vo/SoundParameterEq;->eqLevel:I

    iget-object v4, v0, Lcom/mstar/android/tvapi/common/vo/DtvSoundEffect;->soundParameterEqs:[Lcom/mstar/android/tvapi/common/vo/SoundParameterEq;

    const/4 v5, 0x2

    aget-object v4, v4, v5

    iget-short v5, v2, Lcom/android/server/tv/DataBaseDesk$SoundModeSeting;->EqBand3:S

    iput v5, v4, Lcom/mstar/android/tvapi/common/vo/SoundParameterEq;->eqLevel:I

    iget-object v4, v0, Lcom/mstar/android/tvapi/common/vo/DtvSoundEffect;->soundParameterEqs:[Lcom/mstar/android/tvapi/common/vo/SoundParameterEq;

    const/4 v5, 0x3

    aget-object v4, v4, v5

    iget-short v5, v2, Lcom/android/server/tv/DataBaseDesk$SoundModeSeting;->EqBand4:S

    iput v5, v4, Lcom/mstar/android/tvapi/common/vo/SoundParameterEq;->eqLevel:I

    iget-object v4, v0, Lcom/mstar/android/tvapi/common/vo/DtvSoundEffect;->soundParameterEqs:[Lcom/mstar/android/tvapi/common/vo/SoundParameterEq;

    const/4 v5, 0x4

    aget-object v4, v4, v5

    iget-short v5, v2, Lcom/android/server/tv/DataBaseDesk$SoundModeSeting;->EqBand5:S

    iput v5, v4, Lcom/mstar/android/tvapi/common/vo/SoundParameterEq;->eqLevel:I

    const/4 v4, 0x5

    iput-short v4, v0, Lcom/mstar/android/tvapi/common/vo/DtvSoundEffect;->eqBandNumber:S

    invoke-static {}, Lcom/mstar/android/tvapi/common/TvManager;->getInstance()Lcom/mstar/android/tvapi/common/TvManager;

    move-result-object v4

    if-eqz v4, :cond_0

    invoke-static {}, Lcom/mstar/android/tvapi/common/TvManager;->getInstance()Lcom/mstar/android/tvapi/common/TvManager;

    move-result-object v4

    invoke-virtual {v4}, Lcom/mstar/android/tvapi/common/TvManager;->getAudioManager()Lcom/mstar/android/tvapi/common/AudioManager;

    move-result-object v4

    sget-object v5, Lcom/mstar/android/tvapi/common/vo/EnumSoundEffectType;->E_EQ:Lcom/mstar/android/tvapi/common/vo/EnumSoundEffectType;

    invoke-virtual {v4, v5, v0}, Lcom/mstar/android/tvapi/common/AudioManager;->setBasicSoundEffect(Lcom/mstar/android/tvapi/common/vo/EnumSoundEffectType;Lcom/mstar/android/tvapi/common/vo/DtvSoundEffect;)Lcom/mstar/android/tvapi/common/vo/EnumAudioReturn;
    :try_end_0
    .catch Lcom/mstar/android/tvapi/common/exception/TvCommonException; {:try_start_0 .. :try_end_0} :catch_0

    :cond_0
    :goto_0
    iget-object v4, p0, Lcom/android/server/tv/TvAudioClient;->databaseMgr:Lcom/android/server/tv/DataBaseDeskImpl;

    invoke-virtual {v4, v2, v3}, Lcom/android/server/tv/DataBaseDeskImpl;->updateSoundModeSetting(Lcom/android/server/tv/DataBaseDesk$SoundModeSeting;I)V

    return v7

    :catch_0
    move-exception v1

    invoke-virtual {v1}, Lcom/mstar/android/tvapi/common/exception/TvCommonException;->printStackTrace()V

    goto :goto_0
.end method

.method public setEqBand500(I)Z
    .locals 8
    .param p1    # I
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Landroid/os/RemoteException;
        }
    .end annotation

    const/4 v7, 0x1

    const-string v4, "TvAudioClient"

    new-instance v5, Ljava/lang/StringBuilder;

    invoke-direct {v5}, Ljava/lang/StringBuilder;-><init>()V

    const-string v6, "setEqBand500(), paras eqValue = "

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5, p1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    invoke-static {v4, v5}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    iget-object v4, p0, Lcom/android/server/tv/TvAudioClient;->databaseMgr:Lcom/android/server/tv/DataBaseDeskImpl;

    invoke-virtual {v4}, Lcom/android/server/tv/DataBaseDeskImpl;->querySoundMode()I

    move-result v3

    iget-object v4, p0, Lcom/android/server/tv/TvAudioClient;->databaseMgr:Lcom/android/server/tv/DataBaseDeskImpl;

    invoke-virtual {v4, v3}, Lcom/android/server/tv/DataBaseDeskImpl;->querySoundModeSetting(I)Lcom/android/server/tv/DataBaseDesk$SoundModeSeting;

    move-result-object v2

    int-to-short v4, p1

    iput-short v4, v2, Lcom/android/server/tv/DataBaseDesk$SoundModeSeting;->EqBand2:S

    new-instance v0, Lcom/mstar/android/tvapi/common/vo/DtvSoundEffect;

    invoke-direct {v0}, Lcom/mstar/android/tvapi/common/vo/DtvSoundEffect;-><init>()V

    sget-object v4, Lcom/android/server/tv/DataBaseDesk$EN_SOUND_MODE;->SOUND_MODE_NUM:Lcom/android/server/tv/DataBaseDesk$EN_SOUND_MODE;

    invoke-virtual {v4}, Lcom/android/server/tv/DataBaseDesk$EN_SOUND_MODE;->ordinal()I

    move-result v4

    int-to-short v4, v4

    iput-short v4, v0, Lcom/mstar/android/tvapi/common/vo/DtvSoundEffect;->eqBandNumber:S

    :try_start_0
    iget-object v4, v0, Lcom/mstar/android/tvapi/common/vo/DtvSoundEffect;->soundParameterEqs:[Lcom/mstar/android/tvapi/common/vo/SoundParameterEq;

    const/4 v5, 0x0

    aget-object v4, v4, v5

    iget-short v5, v2, Lcom/android/server/tv/DataBaseDesk$SoundModeSeting;->EqBand1:S

    iput v5, v4, Lcom/mstar/android/tvapi/common/vo/SoundParameterEq;->eqLevel:I

    iget-object v4, v0, Lcom/mstar/android/tvapi/common/vo/DtvSoundEffect;->soundParameterEqs:[Lcom/mstar/android/tvapi/common/vo/SoundParameterEq;

    const/4 v5, 0x1

    aget-object v4, v4, v5

    iget-short v5, v2, Lcom/android/server/tv/DataBaseDesk$SoundModeSeting;->EqBand2:S

    iput v5, v4, Lcom/mstar/android/tvapi/common/vo/SoundParameterEq;->eqLevel:I

    iget-object v4, v0, Lcom/mstar/android/tvapi/common/vo/DtvSoundEffect;->soundParameterEqs:[Lcom/mstar/android/tvapi/common/vo/SoundParameterEq;

    const/4 v5, 0x2

    aget-object v4, v4, v5

    iget-short v5, v2, Lcom/android/server/tv/DataBaseDesk$SoundModeSeting;->EqBand3:S

    iput v5, v4, Lcom/mstar/android/tvapi/common/vo/SoundParameterEq;->eqLevel:I

    iget-object v4, v0, Lcom/mstar/android/tvapi/common/vo/DtvSoundEffect;->soundParameterEqs:[Lcom/mstar/android/tvapi/common/vo/SoundParameterEq;

    const/4 v5, 0x3

    aget-object v4, v4, v5

    iget-short v5, v2, Lcom/android/server/tv/DataBaseDesk$SoundModeSeting;->EqBand4:S

    iput v5, v4, Lcom/mstar/android/tvapi/common/vo/SoundParameterEq;->eqLevel:I

    iget-object v4, v0, Lcom/mstar/android/tvapi/common/vo/DtvSoundEffect;->soundParameterEqs:[Lcom/mstar/android/tvapi/common/vo/SoundParameterEq;

    const/4 v5, 0x4

    aget-object v4, v4, v5

    iget-short v5, v2, Lcom/android/server/tv/DataBaseDesk$SoundModeSeting;->EqBand5:S

    iput v5, v4, Lcom/mstar/android/tvapi/common/vo/SoundParameterEq;->eqLevel:I

    const/4 v4, 0x5

    iput-short v4, v0, Lcom/mstar/android/tvapi/common/vo/DtvSoundEffect;->eqBandNumber:S

    invoke-static {}, Lcom/mstar/android/tvapi/common/TvManager;->getInstance()Lcom/mstar/android/tvapi/common/TvManager;

    move-result-object v4

    if-eqz v4, :cond_0

    invoke-static {}, Lcom/mstar/android/tvapi/common/TvManager;->getInstance()Lcom/mstar/android/tvapi/common/TvManager;

    move-result-object v4

    invoke-virtual {v4}, Lcom/mstar/android/tvapi/common/TvManager;->getAudioManager()Lcom/mstar/android/tvapi/common/AudioManager;

    move-result-object v4

    sget-object v5, Lcom/mstar/android/tvapi/common/vo/EnumSoundEffectType;->E_EQ:Lcom/mstar/android/tvapi/common/vo/EnumSoundEffectType;

    invoke-virtual {v4, v5, v0}, Lcom/mstar/android/tvapi/common/AudioManager;->setBasicSoundEffect(Lcom/mstar/android/tvapi/common/vo/EnumSoundEffectType;Lcom/mstar/android/tvapi/common/vo/DtvSoundEffect;)Lcom/mstar/android/tvapi/common/vo/EnumAudioReturn;
    :try_end_0
    .catch Lcom/mstar/android/tvapi/common/exception/TvCommonException; {:try_start_0 .. :try_end_0} :catch_0

    :cond_0
    :goto_0
    iget-object v4, p0, Lcom/android/server/tv/TvAudioClient;->databaseMgr:Lcom/android/server/tv/DataBaseDeskImpl;

    invoke-virtual {v4, v2, v3}, Lcom/android/server/tv/DataBaseDeskImpl;->updateSoundModeSetting(Lcom/android/server/tv/DataBaseDesk$SoundModeSeting;I)V

    return v7

    :catch_0
    move-exception v1

    invoke-virtual {v1}, Lcom/mstar/android/tvapi/common/exception/TvCommonException;->printStackTrace()V

    goto :goto_0
.end method

.method public setEqBand5k(I)Z
    .locals 8
    .param p1    # I
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Landroid/os/RemoteException;
        }
    .end annotation

    const/4 v7, 0x1

    const-string v4, "TvAudioClient"

    new-instance v5, Ljava/lang/StringBuilder;

    invoke-direct {v5}, Ljava/lang/StringBuilder;-><init>()V

    const-string v6, "setEqBand5k(), paras eqValue = "

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5, p1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    invoke-static {v4, v5}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    iget-object v4, p0, Lcom/android/server/tv/TvAudioClient;->databaseMgr:Lcom/android/server/tv/DataBaseDeskImpl;

    invoke-virtual {v4}, Lcom/android/server/tv/DataBaseDeskImpl;->querySoundMode()I

    move-result v3

    iget-object v4, p0, Lcom/android/server/tv/TvAudioClient;->databaseMgr:Lcom/android/server/tv/DataBaseDeskImpl;

    invoke-virtual {v4, v3}, Lcom/android/server/tv/DataBaseDeskImpl;->querySoundModeSetting(I)Lcom/android/server/tv/DataBaseDesk$SoundModeSeting;

    move-result-object v2

    int-to-short v4, p1

    iput-short v4, v2, Lcom/android/server/tv/DataBaseDesk$SoundModeSeting;->EqBand4:S

    new-instance v0, Lcom/mstar/android/tvapi/common/vo/DtvSoundEffect;

    invoke-direct {v0}, Lcom/mstar/android/tvapi/common/vo/DtvSoundEffect;-><init>()V

    sget-object v4, Lcom/android/server/tv/DataBaseDesk$EN_SOUND_MODE;->SOUND_MODE_NUM:Lcom/android/server/tv/DataBaseDesk$EN_SOUND_MODE;

    invoke-virtual {v4}, Lcom/android/server/tv/DataBaseDesk$EN_SOUND_MODE;->ordinal()I

    move-result v4

    int-to-short v4, v4

    iput-short v4, v0, Lcom/mstar/android/tvapi/common/vo/DtvSoundEffect;->eqBandNumber:S

    :try_start_0
    iget-object v4, v0, Lcom/mstar/android/tvapi/common/vo/DtvSoundEffect;->soundParameterEqs:[Lcom/mstar/android/tvapi/common/vo/SoundParameterEq;

    const/4 v5, 0x0

    aget-object v4, v4, v5

    iget-short v5, v2, Lcom/android/server/tv/DataBaseDesk$SoundModeSeting;->EqBand1:S

    iput v5, v4, Lcom/mstar/android/tvapi/common/vo/SoundParameterEq;->eqLevel:I

    iget-object v4, v0, Lcom/mstar/android/tvapi/common/vo/DtvSoundEffect;->soundParameterEqs:[Lcom/mstar/android/tvapi/common/vo/SoundParameterEq;

    const/4 v5, 0x1

    aget-object v4, v4, v5

    iget-short v5, v2, Lcom/android/server/tv/DataBaseDesk$SoundModeSeting;->EqBand2:S

    iput v5, v4, Lcom/mstar/android/tvapi/common/vo/SoundParameterEq;->eqLevel:I

    iget-object v4, v0, Lcom/mstar/android/tvapi/common/vo/DtvSoundEffect;->soundParameterEqs:[Lcom/mstar/android/tvapi/common/vo/SoundParameterEq;

    const/4 v5, 0x2

    aget-object v4, v4, v5

    iget-short v5, v2, Lcom/android/server/tv/DataBaseDesk$SoundModeSeting;->EqBand3:S

    iput v5, v4, Lcom/mstar/android/tvapi/common/vo/SoundParameterEq;->eqLevel:I

    iget-object v4, v0, Lcom/mstar/android/tvapi/common/vo/DtvSoundEffect;->soundParameterEqs:[Lcom/mstar/android/tvapi/common/vo/SoundParameterEq;

    const/4 v5, 0x3

    aget-object v4, v4, v5

    iget-short v5, v2, Lcom/android/server/tv/DataBaseDesk$SoundModeSeting;->EqBand4:S

    iput v5, v4, Lcom/mstar/android/tvapi/common/vo/SoundParameterEq;->eqLevel:I

    iget-object v4, v0, Lcom/mstar/android/tvapi/common/vo/DtvSoundEffect;->soundParameterEqs:[Lcom/mstar/android/tvapi/common/vo/SoundParameterEq;

    const/4 v5, 0x4

    aget-object v4, v4, v5

    iget-short v5, v2, Lcom/android/server/tv/DataBaseDesk$SoundModeSeting;->EqBand5:S

    iput v5, v4, Lcom/mstar/android/tvapi/common/vo/SoundParameterEq;->eqLevel:I

    const/4 v4, 0x5

    iput-short v4, v0, Lcom/mstar/android/tvapi/common/vo/DtvSoundEffect;->eqBandNumber:S

    invoke-static {}, Lcom/mstar/android/tvapi/common/TvManager;->getInstance()Lcom/mstar/android/tvapi/common/TvManager;

    move-result-object v4

    if-eqz v4, :cond_0

    invoke-static {}, Lcom/mstar/android/tvapi/common/TvManager;->getInstance()Lcom/mstar/android/tvapi/common/TvManager;

    move-result-object v4

    invoke-virtual {v4}, Lcom/mstar/android/tvapi/common/TvManager;->getAudioManager()Lcom/mstar/android/tvapi/common/AudioManager;

    move-result-object v4

    sget-object v5, Lcom/mstar/android/tvapi/common/vo/EnumSoundEffectType;->E_EQ:Lcom/mstar/android/tvapi/common/vo/EnumSoundEffectType;

    invoke-virtual {v4, v5, v0}, Lcom/mstar/android/tvapi/common/AudioManager;->setBasicSoundEffect(Lcom/mstar/android/tvapi/common/vo/EnumSoundEffectType;Lcom/mstar/android/tvapi/common/vo/DtvSoundEffect;)Lcom/mstar/android/tvapi/common/vo/EnumAudioReturn;
    :try_end_0
    .catch Lcom/mstar/android/tvapi/common/exception/TvCommonException; {:try_start_0 .. :try_end_0} :catch_0

    :cond_0
    :goto_0
    iget-object v4, p0, Lcom/android/server/tv/TvAudioClient;->databaseMgr:Lcom/android/server/tv/DataBaseDeskImpl;

    invoke-virtual {v4, v2, v3}, Lcom/android/server/tv/DataBaseDeskImpl;->updateSoundModeSetting(Lcom/android/server/tv/DataBaseDesk$SoundModeSeting;I)V

    return v7

    :catch_0
    move-exception v1

    invoke-virtual {v1}, Lcom/mstar/android/tvapi/common/exception/TvCommonException;->printStackTrace()V

    goto :goto_0
.end method

.method public setPowerOnOffMusic(I)Z
    .locals 3
    .param p1    # I
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Landroid/os/RemoteException;
        }
    .end annotation

    const-string v0, "TvAudioClient"

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "setPowerOnOffMusic(), paras en = "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    iget-object v0, p0, Lcom/android/server/tv/TvAudioClient;->databaseMgr:Lcom/android/server/tv/DataBaseDeskImpl;

    invoke-virtual {v0, p1}, Lcom/android/server/tv/DataBaseDeskImpl;->updatePowerOnOffMusic(I)V

    const/4 v0, 0x1

    return v0
.end method

.method public setSeparateHear(I)Z
    .locals 3
    .param p1    # I
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Landroid/os/RemoteException;
        }
    .end annotation

    const-string v0, "TvAudioClient"

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "setSeparateHear(), paras en= "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    iget-object v0, p0, Lcom/android/server/tv/TvAudioClient;->databaseMgr:Lcom/android/server/tv/DataBaseDeskImpl;

    invoke-virtual {v0, p1}, Lcom/android/server/tv/DataBaseDeskImpl;->updateSeparateHear(I)V

    const/4 v0, 0x1

    return v0
.end method

.method public setSoundMode(I)Z
    .locals 7
    .param p1    # I
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Landroid/os/RemoteException;
        }
    .end annotation

    const/4 v6, 0x1

    const-string v3, "TvAudioClient"

    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    const-string v5, "setSoundMode(), paras SoundMode = "

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4, p1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-static {v3, v4}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    new-instance v0, Lcom/mstar/android/tvapi/common/vo/DtvSoundEffect;

    invoke-direct {v0}, Lcom/mstar/android/tvapi/common/vo/DtvSoundEffect;-><init>()V

    iget-object v3, p0, Lcom/android/server/tv/TvAudioClient;->databaseMgr:Lcom/android/server/tv/DataBaseDeskImpl;

    invoke-virtual {v3, p1}, Lcom/android/server/tv/DataBaseDeskImpl;->querySoundModeSetting(I)Lcom/android/server/tv/DataBaseDesk$SoundModeSeting;

    move-result-object v2

    sget-object v3, Lcom/android/server/tv/DataBaseDesk$EN_SOUND_MODE;->SOUND_MODE_NUM:Lcom/android/server/tv/DataBaseDesk$EN_SOUND_MODE;

    invoke-virtual {v3}, Lcom/android/server/tv/DataBaseDesk$EN_SOUND_MODE;->ordinal()I

    move-result v3

    int-to-short v3, v3

    iput-short v3, v0, Lcom/mstar/android/tvapi/common/vo/DtvSoundEffect;->eqBandNumber:S

    :try_start_0
    iget-object v3, v0, Lcom/mstar/android/tvapi/common/vo/DtvSoundEffect;->soundParameterEqs:[Lcom/mstar/android/tvapi/common/vo/SoundParameterEq;

    const/4 v4, 0x0

    aget-object v3, v3, v4

    iget-short v4, v2, Lcom/android/server/tv/DataBaseDesk$SoundModeSeting;->EqBand1:S

    iput v4, v3, Lcom/mstar/android/tvapi/common/vo/SoundParameterEq;->eqLevel:I

    iget-object v3, v0, Lcom/mstar/android/tvapi/common/vo/DtvSoundEffect;->soundParameterEqs:[Lcom/mstar/android/tvapi/common/vo/SoundParameterEq;

    const/4 v4, 0x1

    aget-object v3, v3, v4

    iget-short v4, v2, Lcom/android/server/tv/DataBaseDesk$SoundModeSeting;->EqBand2:S

    iput v4, v3, Lcom/mstar/android/tvapi/common/vo/SoundParameterEq;->eqLevel:I

    iget-object v3, v0, Lcom/mstar/android/tvapi/common/vo/DtvSoundEffect;->soundParameterEqs:[Lcom/mstar/android/tvapi/common/vo/SoundParameterEq;

    const/4 v4, 0x2

    aget-object v3, v3, v4

    iget-short v4, v2, Lcom/android/server/tv/DataBaseDesk$SoundModeSeting;->EqBand3:S

    iput v4, v3, Lcom/mstar/android/tvapi/common/vo/SoundParameterEq;->eqLevel:I

    iget-object v3, v0, Lcom/mstar/android/tvapi/common/vo/DtvSoundEffect;->soundParameterEqs:[Lcom/mstar/android/tvapi/common/vo/SoundParameterEq;

    const/4 v4, 0x3

    aget-object v3, v3, v4

    iget-short v4, v2, Lcom/android/server/tv/DataBaseDesk$SoundModeSeting;->EqBand4:S

    iput v4, v3, Lcom/mstar/android/tvapi/common/vo/SoundParameterEq;->eqLevel:I

    iget-object v3, v0, Lcom/mstar/android/tvapi/common/vo/DtvSoundEffect;->soundParameterEqs:[Lcom/mstar/android/tvapi/common/vo/SoundParameterEq;

    const/4 v4, 0x4

    aget-object v3, v3, v4

    iget-short v4, v2, Lcom/android/server/tv/DataBaseDesk$SoundModeSeting;->EqBand5:S

    iput v4, v3, Lcom/mstar/android/tvapi/common/vo/SoundParameterEq;->eqLevel:I

    const/4 v3, 0x5

    iput-short v3, v0, Lcom/mstar/android/tvapi/common/vo/DtvSoundEffect;->eqBandNumber:S

    invoke-static {}, Lcom/mstar/android/tvapi/common/TvManager;->getInstance()Lcom/mstar/android/tvapi/common/TvManager;

    move-result-object v3

    if-eqz v3, :cond_0

    invoke-static {}, Lcom/mstar/android/tvapi/common/TvManager;->getInstance()Lcom/mstar/android/tvapi/common/TvManager;

    move-result-object v3

    invoke-virtual {v3}, Lcom/mstar/android/tvapi/common/TvManager;->getAudioManager()Lcom/mstar/android/tvapi/common/AudioManager;

    move-result-object v3

    sget-object v4, Lcom/mstar/android/tvapi/common/vo/EnumSoundEffectType;->E_EQ:Lcom/mstar/android/tvapi/common/vo/EnumSoundEffectType;

    invoke-virtual {v3, v4, v0}, Lcom/mstar/android/tvapi/common/AudioManager;->setBasicSoundEffect(Lcom/mstar/android/tvapi/common/vo/EnumSoundEffectType;Lcom/mstar/android/tvapi/common/vo/DtvSoundEffect;)Lcom/mstar/android/tvapi/common/vo/EnumAudioReturn;
    :try_end_0
    .catch Lcom/mstar/android/tvapi/common/exception/TvCommonException; {:try_start_0 .. :try_end_0} :catch_0

    :cond_0
    :goto_0
    iget-object v3, p0, Lcom/android/server/tv/TvAudioClient;->databaseMgr:Lcom/android/server/tv/DataBaseDeskImpl;

    invoke-virtual {v3, p1}, Lcom/android/server/tv/DataBaseDeskImpl;->updateSoundMode(I)V

    return v6

    :catch_0
    move-exception v1

    invoke-virtual {v1}, Lcom/mstar/android/tvapi/common/exception/TvCommonException;->printStackTrace()V

    goto :goto_0
.end method

.method public setSpdifOutMode(I)Z
    .locals 4
    .param p1    # I
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Landroid/os/RemoteException;
        }
    .end annotation

    const-string v1, "TvAudioClient"

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "setSpdifOutMode(), paras SpdifMode = "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, p1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    iget-object v1, p0, Lcom/android/server/tv/TvAudioClient;->databaseMgr:Lcom/android/server/tv/DataBaseDeskImpl;

    invoke-virtual {v1, p1}, Lcom/android/server/tv/DataBaseDeskImpl;->updateSpdifMode(I)V

    :try_start_0
    invoke-static {}, Lcom/mstar/android/tvapi/common/TvManager;->getInstance()Lcom/mstar/android/tvapi/common/TvManager;

    move-result-object v1

    if-eqz v1, :cond_0

    invoke-static {}, Lcom/mstar/android/tvapi/common/TvManager;->getInstance()Lcom/mstar/android/tvapi/common/TvManager;

    move-result-object v1

    invoke-virtual {v1}, Lcom/mstar/android/tvapi/common/TvManager;->getAudioManager()Lcom/mstar/android/tvapi/common/AudioManager;

    move-result-object v1

    invoke-static {}, Lcom/mstar/android/tvapi/common/vo/EnumSpdifType;->values()[Lcom/mstar/android/tvapi/common/vo/EnumSpdifType;

    move-result-object v2

    aget-object v2, v2, p1

    invoke-virtual {v1, v2}, Lcom/mstar/android/tvapi/common/AudioManager;->setDigitalOut(Lcom/mstar/android/tvapi/common/vo/EnumSpdifType;)V
    :try_end_0
    .catch Lcom/mstar/android/tvapi/common/exception/TvCommonException; {:try_start_0 .. :try_end_0} :catch_0

    :cond_0
    :goto_0
    const/4 v1, 0x1

    return v1

    :catch_0
    move-exception v0

    invoke-virtual {v0}, Lcom/mstar/android/tvapi/common/exception/TvCommonException;->printStackTrace()V

    goto :goto_0
.end method

.method public setSurroundMode(I)Z
    .locals 5
    .param p1    # I
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Landroid/os/RemoteException;
        }
    .end annotation

    const/4 v2, 0x1

    const-string v1, "TvAudioClient"

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string v4, "setSurroundMode(), paras surroundMode = "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3, p1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v1, v3}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    :try_start_0
    invoke-static {}, Lcom/mstar/android/tvapi/common/TvManager;->getInstance()Lcom/mstar/android/tvapi/common/TvManager;

    move-result-object v1

    if-eqz v1, :cond_0

    invoke-static {}, Lcom/mstar/android/tvapi/common/TvManager;->getInstance()Lcom/mstar/android/tvapi/common/TvManager;

    move-result-object v1

    invoke-virtual {v1}, Lcom/mstar/android/tvapi/common/TvManager;->getAudioManager()Lcom/mstar/android/tvapi/common/AudioManager;

    move-result-object v3

    sget-object v4, Lcom/mstar/android/tvapi/common/vo/EnumSoundEffectType;->E_Surround:Lcom/mstar/android/tvapi/common/vo/EnumSoundEffectType;

    if-ne p1, v2, :cond_1

    move v1, v2

    :goto_0
    invoke-virtual {v3, v4, v1}, Lcom/mstar/android/tvapi/common/AudioManager;->enableBasicSoundEffect(Lcom/mstar/android/tvapi/common/vo/EnumSoundEffectType;Z)Lcom/mstar/android/tvapi/common/vo/EnumAudioReturn;
    :try_end_0
    .catch Lcom/mstar/android/tvapi/common/exception/TvCommonException; {:try_start_0 .. :try_end_0} :catch_0

    :cond_0
    :goto_1
    iget-object v1, p0, Lcom/android/server/tv/TvAudioClient;->databaseMgr:Lcom/android/server/tv/DataBaseDeskImpl;

    invoke-virtual {v1, p1}, Lcom/android/server/tv/DataBaseDeskImpl;->updateSrr(I)V

    return v2

    :cond_1
    const/4 v1, 0x0

    goto :goto_0

    :catch_0
    move-exception v0

    invoke-virtual {v0}, Lcom/mstar/android/tvapi/common/exception/TvCommonException;->printStackTrace()V

    goto :goto_1
.end method

.method public setTreble(I)Z
    .locals 8
    .param p1    # I
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Landroid/os/RemoteException;
        }
    .end annotation

    const/4 v7, 0x1

    const-string v4, "TvAudioClient"

    new-instance v5, Ljava/lang/StringBuilder;

    invoke-direct {v5}, Ljava/lang/StringBuilder;-><init>()V

    const-string v6, "setTreble(), paras  trebleValue = "

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5, p1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    invoke-static {v4, v5}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    const/4 v0, 0x0

    new-instance v0, Lcom/mstar/android/tvapi/common/vo/DtvSoundEffect;

    invoke-direct {v0}, Lcom/mstar/android/tvapi/common/vo/DtvSoundEffect;-><init>()V

    iget-object v4, p0, Lcom/android/server/tv/TvAudioClient;->databaseMgr:Lcom/android/server/tv/DataBaseDeskImpl;

    invoke-virtual {v4}, Lcom/android/server/tv/DataBaseDeskImpl;->querySoundMode()I

    move-result v3

    iget-object v4, p0, Lcom/android/server/tv/TvAudioClient;->databaseMgr:Lcom/android/server/tv/DataBaseDeskImpl;

    invoke-virtual {v4, v3}, Lcom/android/server/tv/DataBaseDeskImpl;->querySoundModeSetting(I)Lcom/android/server/tv/DataBaseDesk$SoundModeSeting;

    move-result-object v2

    :try_start_0
    iget-object v4, v0, Lcom/mstar/android/tvapi/common/vo/DtvSoundEffect;->soundParameterEqs:[Lcom/mstar/android/tvapi/common/vo/SoundParameterEq;

    const/4 v5, 0x0

    aget-object v4, v4, v5

    iget-short v5, v2, Lcom/android/server/tv/DataBaseDesk$SoundModeSeting;->EqBand1:S

    iput v5, v4, Lcom/mstar/android/tvapi/common/vo/SoundParameterEq;->eqLevel:I

    iget-object v4, v0, Lcom/mstar/android/tvapi/common/vo/DtvSoundEffect;->soundParameterEqs:[Lcom/mstar/android/tvapi/common/vo/SoundParameterEq;

    const/4 v5, 0x1

    aget-object v4, v4, v5

    iget-short v5, v2, Lcom/android/server/tv/DataBaseDesk$SoundModeSeting;->EqBand2:S

    iput v5, v4, Lcom/mstar/android/tvapi/common/vo/SoundParameterEq;->eqLevel:I

    iget-object v4, v0, Lcom/mstar/android/tvapi/common/vo/DtvSoundEffect;->soundParameterEqs:[Lcom/mstar/android/tvapi/common/vo/SoundParameterEq;

    const/4 v5, 0x2

    aget-object v4, v4, v5

    iget-short v5, v2, Lcom/android/server/tv/DataBaseDesk$SoundModeSeting;->EqBand3:S

    iput v5, v4, Lcom/mstar/android/tvapi/common/vo/SoundParameterEq;->eqLevel:I

    iget-object v4, v0, Lcom/mstar/android/tvapi/common/vo/DtvSoundEffect;->soundParameterEqs:[Lcom/mstar/android/tvapi/common/vo/SoundParameterEq;

    const/4 v5, 0x3

    aget-object v4, v4, v5

    iget-short v5, v2, Lcom/android/server/tv/DataBaseDesk$SoundModeSeting;->EqBand4:S

    iput v5, v4, Lcom/mstar/android/tvapi/common/vo/SoundParameterEq;->eqLevel:I

    int-to-short v4, p1

    iput-short v4, v2, Lcom/android/server/tv/DataBaseDesk$SoundModeSeting;->EqBand5:S

    iget-object v4, v0, Lcom/mstar/android/tvapi/common/vo/DtvSoundEffect;->soundParameterEqs:[Lcom/mstar/android/tvapi/common/vo/SoundParameterEq;

    const/4 v5, 0x4

    aget-object v4, v4, v5

    iput p1, v4, Lcom/mstar/android/tvapi/common/vo/SoundParameterEq;->eqLevel:I

    const/4 v4, 0x5

    iput-short v4, v0, Lcom/mstar/android/tvapi/common/vo/DtvSoundEffect;->eqBandNumber:S

    invoke-static {}, Lcom/mstar/android/tvapi/common/TvManager;->getInstance()Lcom/mstar/android/tvapi/common/TvManager;

    move-result-object v4

    if-eqz v4, :cond_0

    invoke-static {}, Lcom/mstar/android/tvapi/common/TvManager;->getInstance()Lcom/mstar/android/tvapi/common/TvManager;

    move-result-object v4

    invoke-virtual {v4}, Lcom/mstar/android/tvapi/common/TvManager;->getAudioManager()Lcom/mstar/android/tvapi/common/AudioManager;

    move-result-object v4

    sget-object v5, Lcom/mstar/android/tvapi/common/vo/EnumSoundEffectType;->E_EQ:Lcom/mstar/android/tvapi/common/vo/EnumSoundEffectType;

    invoke-virtual {v4, v5, v0}, Lcom/mstar/android/tvapi/common/AudioManager;->setBasicSoundEffect(Lcom/mstar/android/tvapi/common/vo/EnumSoundEffectType;Lcom/mstar/android/tvapi/common/vo/DtvSoundEffect;)Lcom/mstar/android/tvapi/common/vo/EnumAudioReturn;
    :try_end_0
    .catch Lcom/mstar/android/tvapi/common/exception/TvCommonException; {:try_start_0 .. :try_end_0} :catch_0

    :cond_0
    :goto_0
    iget-object v4, p0, Lcom/android/server/tv/TvAudioClient;->databaseMgr:Lcom/android/server/tv/DataBaseDeskImpl;

    invoke-virtual {v4, p1, v3}, Lcom/android/server/tv/DataBaseDeskImpl;->updateTreble(II)V

    return v7

    :catch_0
    move-exception v1

    invoke-virtual {v1}, Lcom/mstar/android/tvapi/common/exception/TvCommonException;->printStackTrace()V

    goto :goto_0
.end method

.method public setTrueBass(I)Z
    .locals 3
    .param p1    # I
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Landroid/os/RemoteException;
        }
    .end annotation

    const-string v0, "TvAudioClient"

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "setTrueBass(), paras en = "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    iget-object v0, p0, Lcom/android/server/tv/TvAudioClient;->databaseMgr:Lcom/android/server/tv/DataBaseDeskImpl;

    invoke-virtual {v0, p1}, Lcom/android/server/tv/DataBaseDeskImpl;->updateTrueBass(I)V

    const/4 v0, 0x0

    return v0
.end method

.method public setWallmusic(I)Z
    .locals 3
    .param p1    # I
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Landroid/os/RemoteException;
        }
    .end annotation

    const-string v0, "TvAudioClient"

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "setWallmusic(), paras en = "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    iget-object v0, p0, Lcom/android/server/tv/TvAudioClient;->databaseMgr:Lcom/android/server/tv/DataBaseDeskImpl;

    invoke-virtual {v0, p1}, Lcom/android/server/tv/DataBaseDeskImpl;->updateWallmusic(I)V

    const/4 v0, 0x0

    return v0
.end method
