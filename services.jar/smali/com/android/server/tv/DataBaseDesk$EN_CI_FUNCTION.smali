.class public final enum Lcom/android/server/tv/DataBaseDesk$EN_CI_FUNCTION;
.super Ljava/lang/Enum;
.source "DataBaseDesk.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/android/server/tv/DataBaseDesk;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x4019
    name = "EN_CI_FUNCTION"
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Enum",
        "<",
        "Lcom/android/server/tv/DataBaseDesk$EN_CI_FUNCTION;",
        ">;"
    }
.end annotation


# static fields
.field private static final synthetic $VALUES:[Lcom/android/server/tv/DataBaseDesk$EN_CI_FUNCTION;

.field public static final enum EN_CI_FUNCTION_APPINFO:Lcom/android/server/tv/DataBaseDesk$EN_CI_FUNCTION;

.field public static final enum EN_CI_FUNCTION_APPMMI:Lcom/android/server/tv/DataBaseDesk$EN_CI_FUNCTION;

.field public static final enum EN_CI_FUNCTION_AUTH:Lcom/android/server/tv/DataBaseDesk$EN_CI_FUNCTION;

.field public static final enum EN_CI_FUNCTION_CAS:Lcom/android/server/tv/DataBaseDesk$EN_CI_FUNCTION;

.field public static final enum EN_CI_FUNCTION_CC:Lcom/android/server/tv/DataBaseDesk$EN_CI_FUNCTION;

.field public static final enum EN_CI_FUNCTION_CU:Lcom/android/server/tv/DataBaseDesk$EN_CI_FUNCTION;

.field public static final enum EN_CI_FUNCTION_DEBUG_COUNT:Lcom/android/server/tv/DataBaseDesk$EN_CI_FUNCTION;

.field public static final enum EN_CI_FUNCTION_DEFAULT:Lcom/android/server/tv/DataBaseDesk$EN_CI_FUNCTION;

.field public static final enum EN_CI_FUNCTION_DT:Lcom/android/server/tv/DataBaseDesk$EN_CI_FUNCTION;

.field public static final enum EN_CI_FUNCTION_HC:Lcom/android/server/tv/DataBaseDesk$EN_CI_FUNCTION;

.field public static final enum EN_CI_FUNCTION_HLC:Lcom/android/server/tv/DataBaseDesk$EN_CI_FUNCTION;

.field public static final enum EN_CI_FUNCTION_HSS:Lcom/android/server/tv/DataBaseDesk$EN_CI_FUNCTION;

.field public static final enum EN_CI_FUNCTION_LSC:Lcom/android/server/tv/DataBaseDesk$EN_CI_FUNCTION;

.field public static final enum EN_CI_FUNCTION_MMI:Lcom/android/server/tv/DataBaseDesk$EN_CI_FUNCTION;

.field public static final enum EN_CI_FUNCTION_OP:Lcom/android/server/tv/DataBaseDesk$EN_CI_FUNCTION;

.field public static final enum EN_CI_FUNCTION_PMT:Lcom/android/server/tv/DataBaseDesk$EN_CI_FUNCTION;

.field public static final enum EN_CI_FUNCTION_RM:Lcom/android/server/tv/DataBaseDesk$EN_CI_FUNCTION;

.field public static final enum EN_CI_FUNCTION_SAS:Lcom/android/server/tv/DataBaseDesk$EN_CI_FUNCTION;


# direct methods
.method static constructor <clinit>()V
    .locals 8

    const/4 v7, 0x4

    const/4 v6, 0x3

    const/4 v5, 0x2

    const/4 v4, 0x1

    const/4 v3, 0x0

    new-instance v0, Lcom/android/server/tv/DataBaseDesk$EN_CI_FUNCTION;

    const-string v1, "EN_CI_FUNCTION_RM"

    invoke-direct {v0, v1, v3}, Lcom/android/server/tv/DataBaseDesk$EN_CI_FUNCTION;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/android/server/tv/DataBaseDesk$EN_CI_FUNCTION;->EN_CI_FUNCTION_RM:Lcom/android/server/tv/DataBaseDesk$EN_CI_FUNCTION;

    new-instance v0, Lcom/android/server/tv/DataBaseDesk$EN_CI_FUNCTION;

    const-string v1, "EN_CI_FUNCTION_APPINFO"

    invoke-direct {v0, v1, v4}, Lcom/android/server/tv/DataBaseDesk$EN_CI_FUNCTION;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/android/server/tv/DataBaseDesk$EN_CI_FUNCTION;->EN_CI_FUNCTION_APPINFO:Lcom/android/server/tv/DataBaseDesk$EN_CI_FUNCTION;

    new-instance v0, Lcom/android/server/tv/DataBaseDesk$EN_CI_FUNCTION;

    const-string v1, "EN_CI_FUNCTION_CAS"

    invoke-direct {v0, v1, v5}, Lcom/android/server/tv/DataBaseDesk$EN_CI_FUNCTION;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/android/server/tv/DataBaseDesk$EN_CI_FUNCTION;->EN_CI_FUNCTION_CAS:Lcom/android/server/tv/DataBaseDesk$EN_CI_FUNCTION;

    new-instance v0, Lcom/android/server/tv/DataBaseDesk$EN_CI_FUNCTION;

    const-string v1, "EN_CI_FUNCTION_HC"

    invoke-direct {v0, v1, v6}, Lcom/android/server/tv/DataBaseDesk$EN_CI_FUNCTION;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/android/server/tv/DataBaseDesk$EN_CI_FUNCTION;->EN_CI_FUNCTION_HC:Lcom/android/server/tv/DataBaseDesk$EN_CI_FUNCTION;

    new-instance v0, Lcom/android/server/tv/DataBaseDesk$EN_CI_FUNCTION;

    const-string v1, "EN_CI_FUNCTION_DT"

    invoke-direct {v0, v1, v7}, Lcom/android/server/tv/DataBaseDesk$EN_CI_FUNCTION;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/android/server/tv/DataBaseDesk$EN_CI_FUNCTION;->EN_CI_FUNCTION_DT:Lcom/android/server/tv/DataBaseDesk$EN_CI_FUNCTION;

    new-instance v0, Lcom/android/server/tv/DataBaseDesk$EN_CI_FUNCTION;

    const-string v1, "EN_CI_FUNCTION_MMI"

    const/4 v2, 0x5

    invoke-direct {v0, v1, v2}, Lcom/android/server/tv/DataBaseDesk$EN_CI_FUNCTION;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/android/server/tv/DataBaseDesk$EN_CI_FUNCTION;->EN_CI_FUNCTION_MMI:Lcom/android/server/tv/DataBaseDesk$EN_CI_FUNCTION;

    new-instance v0, Lcom/android/server/tv/DataBaseDesk$EN_CI_FUNCTION;

    const-string v1, "EN_CI_FUNCTION_LSC"

    const/4 v2, 0x6

    invoke-direct {v0, v1, v2}, Lcom/android/server/tv/DataBaseDesk$EN_CI_FUNCTION;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/android/server/tv/DataBaseDesk$EN_CI_FUNCTION;->EN_CI_FUNCTION_LSC:Lcom/android/server/tv/DataBaseDesk$EN_CI_FUNCTION;

    new-instance v0, Lcom/android/server/tv/DataBaseDesk$EN_CI_FUNCTION;

    const-string v1, "EN_CI_FUNCTION_CC"

    const/4 v2, 0x7

    invoke-direct {v0, v1, v2}, Lcom/android/server/tv/DataBaseDesk$EN_CI_FUNCTION;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/android/server/tv/DataBaseDesk$EN_CI_FUNCTION;->EN_CI_FUNCTION_CC:Lcom/android/server/tv/DataBaseDesk$EN_CI_FUNCTION;

    new-instance v0, Lcom/android/server/tv/DataBaseDesk$EN_CI_FUNCTION;

    const-string v1, "EN_CI_FUNCTION_HLC"

    const/16 v2, 0x8

    invoke-direct {v0, v1, v2}, Lcom/android/server/tv/DataBaseDesk$EN_CI_FUNCTION;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/android/server/tv/DataBaseDesk$EN_CI_FUNCTION;->EN_CI_FUNCTION_HLC:Lcom/android/server/tv/DataBaseDesk$EN_CI_FUNCTION;

    new-instance v0, Lcom/android/server/tv/DataBaseDesk$EN_CI_FUNCTION;

    const-string v1, "EN_CI_FUNCTION_CU"

    const/16 v2, 0x9

    invoke-direct {v0, v1, v2}, Lcom/android/server/tv/DataBaseDesk$EN_CI_FUNCTION;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/android/server/tv/DataBaseDesk$EN_CI_FUNCTION;->EN_CI_FUNCTION_CU:Lcom/android/server/tv/DataBaseDesk$EN_CI_FUNCTION;

    new-instance v0, Lcom/android/server/tv/DataBaseDesk$EN_CI_FUNCTION;

    const-string v1, "EN_CI_FUNCTION_OP"

    const/16 v2, 0xa

    invoke-direct {v0, v1, v2}, Lcom/android/server/tv/DataBaseDesk$EN_CI_FUNCTION;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/android/server/tv/DataBaseDesk$EN_CI_FUNCTION;->EN_CI_FUNCTION_OP:Lcom/android/server/tv/DataBaseDesk$EN_CI_FUNCTION;

    new-instance v0, Lcom/android/server/tv/DataBaseDesk$EN_CI_FUNCTION;

    const-string v1, "EN_CI_FUNCTION_SAS"

    const/16 v2, 0xb

    invoke-direct {v0, v1, v2}, Lcom/android/server/tv/DataBaseDesk$EN_CI_FUNCTION;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/android/server/tv/DataBaseDesk$EN_CI_FUNCTION;->EN_CI_FUNCTION_SAS:Lcom/android/server/tv/DataBaseDesk$EN_CI_FUNCTION;

    new-instance v0, Lcom/android/server/tv/DataBaseDesk$EN_CI_FUNCTION;

    const-string v1, "EN_CI_FUNCTION_APPMMI"

    const/16 v2, 0xc

    invoke-direct {v0, v1, v2}, Lcom/android/server/tv/DataBaseDesk$EN_CI_FUNCTION;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/android/server/tv/DataBaseDesk$EN_CI_FUNCTION;->EN_CI_FUNCTION_APPMMI:Lcom/android/server/tv/DataBaseDesk$EN_CI_FUNCTION;

    new-instance v0, Lcom/android/server/tv/DataBaseDesk$EN_CI_FUNCTION;

    const-string v1, "EN_CI_FUNCTION_PMT"

    const/16 v2, 0xd

    invoke-direct {v0, v1, v2}, Lcom/android/server/tv/DataBaseDesk$EN_CI_FUNCTION;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/android/server/tv/DataBaseDesk$EN_CI_FUNCTION;->EN_CI_FUNCTION_PMT:Lcom/android/server/tv/DataBaseDesk$EN_CI_FUNCTION;

    new-instance v0, Lcom/android/server/tv/DataBaseDesk$EN_CI_FUNCTION;

    const-string v1, "EN_CI_FUNCTION_HSS"

    const/16 v2, 0xe

    invoke-direct {v0, v1, v2}, Lcom/android/server/tv/DataBaseDesk$EN_CI_FUNCTION;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/android/server/tv/DataBaseDesk$EN_CI_FUNCTION;->EN_CI_FUNCTION_HSS:Lcom/android/server/tv/DataBaseDesk$EN_CI_FUNCTION;

    new-instance v0, Lcom/android/server/tv/DataBaseDesk$EN_CI_FUNCTION;

    const-string v1, "EN_CI_FUNCTION_AUTH"

    const/16 v2, 0xf

    invoke-direct {v0, v1, v2}, Lcom/android/server/tv/DataBaseDesk$EN_CI_FUNCTION;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/android/server/tv/DataBaseDesk$EN_CI_FUNCTION;->EN_CI_FUNCTION_AUTH:Lcom/android/server/tv/DataBaseDesk$EN_CI_FUNCTION;

    new-instance v0, Lcom/android/server/tv/DataBaseDesk$EN_CI_FUNCTION;

    const-string v1, "EN_CI_FUNCTION_DEFAULT"

    const/16 v2, 0x10

    invoke-direct {v0, v1, v2}, Lcom/android/server/tv/DataBaseDesk$EN_CI_FUNCTION;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/android/server/tv/DataBaseDesk$EN_CI_FUNCTION;->EN_CI_FUNCTION_DEFAULT:Lcom/android/server/tv/DataBaseDesk$EN_CI_FUNCTION;

    new-instance v0, Lcom/android/server/tv/DataBaseDesk$EN_CI_FUNCTION;

    const-string v1, "EN_CI_FUNCTION_DEBUG_COUNT"

    const/16 v2, 0x11

    invoke-direct {v0, v1, v2}, Lcom/android/server/tv/DataBaseDesk$EN_CI_FUNCTION;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/android/server/tv/DataBaseDesk$EN_CI_FUNCTION;->EN_CI_FUNCTION_DEBUG_COUNT:Lcom/android/server/tv/DataBaseDesk$EN_CI_FUNCTION;

    const/16 v0, 0x12

    new-array v0, v0, [Lcom/android/server/tv/DataBaseDesk$EN_CI_FUNCTION;

    sget-object v1, Lcom/android/server/tv/DataBaseDesk$EN_CI_FUNCTION;->EN_CI_FUNCTION_RM:Lcom/android/server/tv/DataBaseDesk$EN_CI_FUNCTION;

    aput-object v1, v0, v3

    sget-object v1, Lcom/android/server/tv/DataBaseDesk$EN_CI_FUNCTION;->EN_CI_FUNCTION_APPINFO:Lcom/android/server/tv/DataBaseDesk$EN_CI_FUNCTION;

    aput-object v1, v0, v4

    sget-object v1, Lcom/android/server/tv/DataBaseDesk$EN_CI_FUNCTION;->EN_CI_FUNCTION_CAS:Lcom/android/server/tv/DataBaseDesk$EN_CI_FUNCTION;

    aput-object v1, v0, v5

    sget-object v1, Lcom/android/server/tv/DataBaseDesk$EN_CI_FUNCTION;->EN_CI_FUNCTION_HC:Lcom/android/server/tv/DataBaseDesk$EN_CI_FUNCTION;

    aput-object v1, v0, v6

    sget-object v1, Lcom/android/server/tv/DataBaseDesk$EN_CI_FUNCTION;->EN_CI_FUNCTION_DT:Lcom/android/server/tv/DataBaseDesk$EN_CI_FUNCTION;

    aput-object v1, v0, v7

    const/4 v1, 0x5

    sget-object v2, Lcom/android/server/tv/DataBaseDesk$EN_CI_FUNCTION;->EN_CI_FUNCTION_MMI:Lcom/android/server/tv/DataBaseDesk$EN_CI_FUNCTION;

    aput-object v2, v0, v1

    const/4 v1, 0x6

    sget-object v2, Lcom/android/server/tv/DataBaseDesk$EN_CI_FUNCTION;->EN_CI_FUNCTION_LSC:Lcom/android/server/tv/DataBaseDesk$EN_CI_FUNCTION;

    aput-object v2, v0, v1

    const/4 v1, 0x7

    sget-object v2, Lcom/android/server/tv/DataBaseDesk$EN_CI_FUNCTION;->EN_CI_FUNCTION_CC:Lcom/android/server/tv/DataBaseDesk$EN_CI_FUNCTION;

    aput-object v2, v0, v1

    const/16 v1, 0x8

    sget-object v2, Lcom/android/server/tv/DataBaseDesk$EN_CI_FUNCTION;->EN_CI_FUNCTION_HLC:Lcom/android/server/tv/DataBaseDesk$EN_CI_FUNCTION;

    aput-object v2, v0, v1

    const/16 v1, 0x9

    sget-object v2, Lcom/android/server/tv/DataBaseDesk$EN_CI_FUNCTION;->EN_CI_FUNCTION_CU:Lcom/android/server/tv/DataBaseDesk$EN_CI_FUNCTION;

    aput-object v2, v0, v1

    const/16 v1, 0xa

    sget-object v2, Lcom/android/server/tv/DataBaseDesk$EN_CI_FUNCTION;->EN_CI_FUNCTION_OP:Lcom/android/server/tv/DataBaseDesk$EN_CI_FUNCTION;

    aput-object v2, v0, v1

    const/16 v1, 0xb

    sget-object v2, Lcom/android/server/tv/DataBaseDesk$EN_CI_FUNCTION;->EN_CI_FUNCTION_SAS:Lcom/android/server/tv/DataBaseDesk$EN_CI_FUNCTION;

    aput-object v2, v0, v1

    const/16 v1, 0xc

    sget-object v2, Lcom/android/server/tv/DataBaseDesk$EN_CI_FUNCTION;->EN_CI_FUNCTION_APPMMI:Lcom/android/server/tv/DataBaseDesk$EN_CI_FUNCTION;

    aput-object v2, v0, v1

    const/16 v1, 0xd

    sget-object v2, Lcom/android/server/tv/DataBaseDesk$EN_CI_FUNCTION;->EN_CI_FUNCTION_PMT:Lcom/android/server/tv/DataBaseDesk$EN_CI_FUNCTION;

    aput-object v2, v0, v1

    const/16 v1, 0xe

    sget-object v2, Lcom/android/server/tv/DataBaseDesk$EN_CI_FUNCTION;->EN_CI_FUNCTION_HSS:Lcom/android/server/tv/DataBaseDesk$EN_CI_FUNCTION;

    aput-object v2, v0, v1

    const/16 v1, 0xf

    sget-object v2, Lcom/android/server/tv/DataBaseDesk$EN_CI_FUNCTION;->EN_CI_FUNCTION_AUTH:Lcom/android/server/tv/DataBaseDesk$EN_CI_FUNCTION;

    aput-object v2, v0, v1

    const/16 v1, 0x10

    sget-object v2, Lcom/android/server/tv/DataBaseDesk$EN_CI_FUNCTION;->EN_CI_FUNCTION_DEFAULT:Lcom/android/server/tv/DataBaseDesk$EN_CI_FUNCTION;

    aput-object v2, v0, v1

    const/16 v1, 0x11

    sget-object v2, Lcom/android/server/tv/DataBaseDesk$EN_CI_FUNCTION;->EN_CI_FUNCTION_DEBUG_COUNT:Lcom/android/server/tv/DataBaseDesk$EN_CI_FUNCTION;

    aput-object v2, v0, v1

    sput-object v0, Lcom/android/server/tv/DataBaseDesk$EN_CI_FUNCTION;->$VALUES:[Lcom/android/server/tv/DataBaseDesk$EN_CI_FUNCTION;

    return-void
.end method

.method private constructor <init>(Ljava/lang/String;I)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()V"
        }
    .end annotation

    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    return-void
.end method

.method public static valueOf(Ljava/lang/String;)Lcom/android/server/tv/DataBaseDesk$EN_CI_FUNCTION;
    .locals 1
    .param p0    # Ljava/lang/String;

    const-class v0, Lcom/android/server/tv/DataBaseDesk$EN_CI_FUNCTION;

    invoke-static {v0, p0}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;

    move-result-object v0

    check-cast v0, Lcom/android/server/tv/DataBaseDesk$EN_CI_FUNCTION;

    return-object v0
.end method

.method public static values()[Lcom/android/server/tv/DataBaseDesk$EN_CI_FUNCTION;
    .locals 1

    sget-object v0, Lcom/android/server/tv/DataBaseDesk$EN_CI_FUNCTION;->$VALUES:[Lcom/android/server/tv/DataBaseDesk$EN_CI_FUNCTION;

    invoke-virtual {v0}, [Lcom/android/server/tv/DataBaseDesk$EN_CI_FUNCTION;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [Lcom/android/server/tv/DataBaseDesk$EN_CI_FUNCTION;

    return-object v0
.end method
