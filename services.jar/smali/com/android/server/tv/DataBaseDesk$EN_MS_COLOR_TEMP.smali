.class public final enum Lcom/android/server/tv/DataBaseDesk$EN_MS_COLOR_TEMP;
.super Ljava/lang/Enum;
.source "DataBaseDesk.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/android/server/tv/DataBaseDesk;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x4019
    name = "EN_MS_COLOR_TEMP"
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Enum",
        "<",
        "Lcom/android/server/tv/DataBaseDesk$EN_MS_COLOR_TEMP;",
        ">;"
    }
.end annotation


# static fields
.field private static final synthetic $VALUES:[Lcom/android/server/tv/DataBaseDesk$EN_MS_COLOR_TEMP;

.field public static final enum MS_COLOR_TEMP_COOL:Lcom/android/server/tv/DataBaseDesk$EN_MS_COLOR_TEMP;

.field public static final enum MS_COLOR_TEMP_NATURE:Lcom/android/server/tv/DataBaseDesk$EN_MS_COLOR_TEMP;

.field public static final enum MS_COLOR_TEMP_NUM:Lcom/android/server/tv/DataBaseDesk$EN_MS_COLOR_TEMP;

.field public static final enum MS_COLOR_TEMP_USER:Lcom/android/server/tv/DataBaseDesk$EN_MS_COLOR_TEMP;

.field public static final enum MS_COLOR_TEMP_WARM:Lcom/android/server/tv/DataBaseDesk$EN_MS_COLOR_TEMP;


# direct methods
.method static constructor <clinit>()V
    .locals 7

    const/4 v6, 0x4

    const/4 v5, 0x3

    const/4 v4, 0x2

    const/4 v3, 0x1

    const/4 v2, 0x0

    new-instance v0, Lcom/android/server/tv/DataBaseDesk$EN_MS_COLOR_TEMP;

    const-string v1, "MS_COLOR_TEMP_COOL"

    invoke-direct {v0, v1, v2}, Lcom/android/server/tv/DataBaseDesk$EN_MS_COLOR_TEMP;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/android/server/tv/DataBaseDesk$EN_MS_COLOR_TEMP;->MS_COLOR_TEMP_COOL:Lcom/android/server/tv/DataBaseDesk$EN_MS_COLOR_TEMP;

    new-instance v0, Lcom/android/server/tv/DataBaseDesk$EN_MS_COLOR_TEMP;

    const-string v1, "MS_COLOR_TEMP_NATURE"

    invoke-direct {v0, v1, v3}, Lcom/android/server/tv/DataBaseDesk$EN_MS_COLOR_TEMP;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/android/server/tv/DataBaseDesk$EN_MS_COLOR_TEMP;->MS_COLOR_TEMP_NATURE:Lcom/android/server/tv/DataBaseDesk$EN_MS_COLOR_TEMP;

    new-instance v0, Lcom/android/server/tv/DataBaseDesk$EN_MS_COLOR_TEMP;

    const-string v1, "MS_COLOR_TEMP_WARM"

    invoke-direct {v0, v1, v4}, Lcom/android/server/tv/DataBaseDesk$EN_MS_COLOR_TEMP;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/android/server/tv/DataBaseDesk$EN_MS_COLOR_TEMP;->MS_COLOR_TEMP_WARM:Lcom/android/server/tv/DataBaseDesk$EN_MS_COLOR_TEMP;

    new-instance v0, Lcom/android/server/tv/DataBaseDesk$EN_MS_COLOR_TEMP;

    const-string v1, "MS_COLOR_TEMP_USER"

    invoke-direct {v0, v1, v5}, Lcom/android/server/tv/DataBaseDesk$EN_MS_COLOR_TEMP;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/android/server/tv/DataBaseDesk$EN_MS_COLOR_TEMP;->MS_COLOR_TEMP_USER:Lcom/android/server/tv/DataBaseDesk$EN_MS_COLOR_TEMP;

    new-instance v0, Lcom/android/server/tv/DataBaseDesk$EN_MS_COLOR_TEMP;

    const-string v1, "MS_COLOR_TEMP_NUM"

    invoke-direct {v0, v1, v6}, Lcom/android/server/tv/DataBaseDesk$EN_MS_COLOR_TEMP;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/android/server/tv/DataBaseDesk$EN_MS_COLOR_TEMP;->MS_COLOR_TEMP_NUM:Lcom/android/server/tv/DataBaseDesk$EN_MS_COLOR_TEMP;

    const/4 v0, 0x5

    new-array v0, v0, [Lcom/android/server/tv/DataBaseDesk$EN_MS_COLOR_TEMP;

    sget-object v1, Lcom/android/server/tv/DataBaseDesk$EN_MS_COLOR_TEMP;->MS_COLOR_TEMP_COOL:Lcom/android/server/tv/DataBaseDesk$EN_MS_COLOR_TEMP;

    aput-object v1, v0, v2

    sget-object v1, Lcom/android/server/tv/DataBaseDesk$EN_MS_COLOR_TEMP;->MS_COLOR_TEMP_NATURE:Lcom/android/server/tv/DataBaseDesk$EN_MS_COLOR_TEMP;

    aput-object v1, v0, v3

    sget-object v1, Lcom/android/server/tv/DataBaseDesk$EN_MS_COLOR_TEMP;->MS_COLOR_TEMP_WARM:Lcom/android/server/tv/DataBaseDesk$EN_MS_COLOR_TEMP;

    aput-object v1, v0, v4

    sget-object v1, Lcom/android/server/tv/DataBaseDesk$EN_MS_COLOR_TEMP;->MS_COLOR_TEMP_USER:Lcom/android/server/tv/DataBaseDesk$EN_MS_COLOR_TEMP;

    aput-object v1, v0, v5

    sget-object v1, Lcom/android/server/tv/DataBaseDesk$EN_MS_COLOR_TEMP;->MS_COLOR_TEMP_NUM:Lcom/android/server/tv/DataBaseDesk$EN_MS_COLOR_TEMP;

    aput-object v1, v0, v6

    sput-object v0, Lcom/android/server/tv/DataBaseDesk$EN_MS_COLOR_TEMP;->$VALUES:[Lcom/android/server/tv/DataBaseDesk$EN_MS_COLOR_TEMP;

    return-void
.end method

.method private constructor <init>(Ljava/lang/String;I)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()V"
        }
    .end annotation

    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    return-void
.end method

.method public static valueOf(Ljava/lang/String;)Lcom/android/server/tv/DataBaseDesk$EN_MS_COLOR_TEMP;
    .locals 1
    .param p0    # Ljava/lang/String;

    const-class v0, Lcom/android/server/tv/DataBaseDesk$EN_MS_COLOR_TEMP;

    invoke-static {v0, p0}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;

    move-result-object v0

    check-cast v0, Lcom/android/server/tv/DataBaseDesk$EN_MS_COLOR_TEMP;

    return-object v0
.end method

.method public static values()[Lcom/android/server/tv/DataBaseDesk$EN_MS_COLOR_TEMP;
    .locals 1

    sget-object v0, Lcom/android/server/tv/DataBaseDesk$EN_MS_COLOR_TEMP;->$VALUES:[Lcom/android/server/tv/DataBaseDesk$EN_MS_COLOR_TEMP;

    invoke-virtual {v0}, [Lcom/android/server/tv/DataBaseDesk$EN_MS_COLOR_TEMP;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [Lcom/android/server/tv/DataBaseDesk$EN_MS_COLOR_TEMP;

    return-object v0
.end method
