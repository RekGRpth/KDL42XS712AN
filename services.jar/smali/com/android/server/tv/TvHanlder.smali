.class public Lcom/android/server/tv/TvHanlder;
.super Landroid/os/Handler;
.source "TvHanlder.java"

# interfaces
.implements Landroid/view/SurfaceHolder$Callback;


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/android/server/tv/TvHanlder$2;,
        Lcom/android/server/tv/TvHanlder$EnumResolution;,
        Lcom/android/server/tv/TvHanlder$WriteVolToDBTask;,
        Lcom/android/server/tv/TvHanlder$HotKey;
    }
.end annotation


# static fields
.field private static final KILL_VIEW_FOR_VOL_MSG:I = 0xc

.field private static final KILL_VIEW_MSG:I = 0xb

.field private static PIPView:Landroid/view/View;

.field private static barVol:Landroid/widget/ProgressBar;

.field private static isAudioModeShowed:Z

.field private static isFreezeShowed:Z

.field private static isMuteShowed:Z

.field private static isPicModeShowed:Z

.field private static isSleepModeShowed:Z

.field private static isVolShowed:Z

.field private static isZoomShowed:Z

.field private static textView:Landroid/widget/TextView;


# instance fields
.field private PIPSurface:Landroid/view/SurfaceView;

.field private PIP_height:I

.field private PIP_width:I

.field private PIP_xPos:I

.field private PIP_yPos:I

.field private PIPwm:Landroid/view/WindowManager;

.field private final TAG:Ljava/lang/String;

.field private audioModeIdx:I

.field private audioModeVals:[Ljava/lang/String;

.field private audioModeView:Landroid/view/View;

.field private dm:Landroid/util/DisplayMetrics;

.field private eResolution:Lcom/android/server/tv/TvHanlder$EnumResolution;

.field private freezeView:Landroid/view/View;

.field private isPipSurfaceOn:Z

.field private mContext:Landroid/content/Context;

.field private mHandlerThread:Landroid/os/HandlerThread;

.field private mSurfaceHolder:Landroid/view/SurfaceHolder;

.field private mTvPlayer:Lcom/mstar/android/tvapi/common/TvPlayer;

.field private mTvService:Lcom/mstar/android/tv/ITvService;

.field private muteView:Landroid/view/View;

.field private picModeIdx:I

.field private picModeVals:[Ljava/lang/String;

.field private picModeView:Landroid/view/View;

.field private sleepModeIdx:I

.field private sleepModeVals:[Ljava/lang/String;

.field private sleepModeView:Landroid/view/View;

.field private sufaceView:Landroid/view/SurfaceView;

.field private sufaceViewFlag:Z

.field private surfaceParams:Landroid/view/WindowManager$LayoutParams;

.field protected timer:Ljava/util/Timer;

.field protected timerTask:Lcom/android/server/tv/TvHanlder$WriteVolToDBTask;

.field private vol:I

.field private volView:Landroid/view/View;

.field private wm:Landroid/view/WindowManager;

.field private zoomModeIdx:I

.field private zoomVals:[Ljava/lang/String;

.field private zoomView:Landroid/view/View;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    const/4 v0, 0x0

    sput-boolean v0, Lcom/android/server/tv/TvHanlder;->isSleepModeShowed:Z

    sput-boolean v0, Lcom/android/server/tv/TvHanlder;->isVolShowed:Z

    sput-boolean v0, Lcom/android/server/tv/TvHanlder;->isAudioModeShowed:Z

    sput-boolean v0, Lcom/android/server/tv/TvHanlder;->isPicModeShowed:Z

    sput-boolean v0, Lcom/android/server/tv/TvHanlder;->isZoomShowed:Z

    sput-boolean v0, Lcom/android/server/tv/TvHanlder;->isMuteShowed:Z

    sput-boolean v0, Lcom/android/server/tv/TvHanlder;->isFreezeShowed:Z

    return-void
.end method

.method public constructor <init>(Landroid/os/HandlerThread;Landroid/content/Context;Lcom/mstar/android/tvapi/common/TvPlayer;Lcom/mstar/android/tv/ITvService;)V
    .locals 3
    .param p1    # Landroid/os/HandlerThread;
    .param p2    # Landroid/content/Context;
    .param p3    # Lcom/mstar/android/tvapi/common/TvPlayer;
    .param p4    # Lcom/mstar/android/tv/ITvService;

    const/4 v2, 0x0

    const/4 v1, 0x0

    invoke-virtual {p1}, Landroid/os/HandlerThread;->getLooper()Landroid/os/Looper;

    move-result-object v0

    invoke-direct {p0, v0}, Landroid/os/Handler;-><init>(Landroid/os/Looper;)V

    iput-object v1, p0, Lcom/android/server/tv/TvHanlder;->wm:Landroid/view/WindowManager;

    iput-boolean v2, p0, Lcom/android/server/tv/TvHanlder;->sufaceViewFlag:Z

    iput-object v1, p0, Lcom/android/server/tv/TvHanlder;->PIPwm:Landroid/view/WindowManager;

    iput-object v1, p0, Lcom/android/server/tv/TvHanlder;->mSurfaceHolder:Landroid/view/SurfaceHolder;

    iput-boolean v2, p0, Lcom/android/server/tv/TvHanlder;->isPipSurfaceOn:Z

    iput-object v1, p0, Lcom/android/server/tv/TvHanlder;->sleepModeVals:[Ljava/lang/String;

    iput-object v1, p0, Lcom/android/server/tv/TvHanlder;->audioModeVals:[Ljava/lang/String;

    iput-object v1, p0, Lcom/android/server/tv/TvHanlder;->picModeVals:[Ljava/lang/String;

    iput-object v1, p0, Lcom/android/server/tv/TvHanlder;->zoomVals:[Ljava/lang/String;

    iput-object v1, p0, Lcom/android/server/tv/TvHanlder;->mTvPlayer:Lcom/mstar/android/tvapi/common/TvPlayer;

    iput-object v1, p0, Lcom/android/server/tv/TvHanlder;->mContext:Landroid/content/Context;

    const-string v0, "TvHanlder"

    iput-object v0, p0, Lcom/android/server/tv/TvHanlder;->TAG:Ljava/lang/String;

    iput-object p1, p0, Lcom/android/server/tv/TvHanlder;->mHandlerThread:Landroid/os/HandlerThread;

    iput-object p2, p0, Lcom/android/server/tv/TvHanlder;->mContext:Landroid/content/Context;

    iput-object p3, p0, Lcom/android/server/tv/TvHanlder;->mTvPlayer:Lcom/mstar/android/tvapi/common/TvPlayer;

    iput-object p4, p0, Lcom/android/server/tv/TvHanlder;->mTvService:Lcom/mstar/android/tv/ITvService;

    iget-object v0, p0, Lcom/android/server/tv/TvHanlder;->mContext:Landroid/content/Context;

    const-string v1, "window"

    invoke-virtual {v0, v1}, Landroid/content/Context;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/view/WindowManager;

    iput-object v0, p0, Lcom/android/server/tv/TvHanlder;->wm:Landroid/view/WindowManager;

    new-instance v0, Landroid/util/DisplayMetrics;

    invoke-direct {v0}, Landroid/util/DisplayMetrics;-><init>()V

    iput-object v0, p0, Lcom/android/server/tv/TvHanlder;->dm:Landroid/util/DisplayMetrics;

    iget-object v0, p0, Lcom/android/server/tv/TvHanlder;->wm:Landroid/view/WindowManager;

    invoke-interface {v0}, Landroid/view/WindowManager;->getDefaultDisplay()Landroid/view/Display;

    move-result-object v0

    iget-object v1, p0, Lcom/android/server/tv/TvHanlder;->dm:Landroid/util/DisplayMetrics;

    invoke-virtual {v0, v1}, Landroid/view/Display;->getMetrics(Landroid/util/DisplayMetrics;)V

    new-instance v0, Ljava/util/Timer;

    invoke-direct {v0}, Ljava/util/Timer;-><init>()V

    iput-object v0, p0, Lcom/android/server/tv/TvHanlder;->timer:Ljava/util/Timer;

    return-void
.end method

.method static synthetic access$000(Lcom/android/server/tv/TvHanlder;)Lcom/mstar/android/tvapi/common/TvPlayer;
    .locals 1
    .param p0    # Lcom/android/server/tv/TvHanlder;

    iget-object v0, p0, Lcom/android/server/tv/TvHanlder;->mTvPlayer:Lcom/mstar/android/tvapi/common/TvPlayer;

    return-object v0
.end method

.method static synthetic access$100(Lcom/android/server/tv/TvHanlder;)Landroid/view/WindowManager$LayoutParams;
    .locals 1
    .param p0    # Lcom/android/server/tv/TvHanlder;

    iget-object v0, p0, Lcom/android/server/tv/TvHanlder;->surfaceParams:Landroid/view/WindowManager$LayoutParams;

    return-object v0
.end method

.method private closeSurfaceView()V
    .locals 2
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Landroid/os/RemoteException;
        }
    .end annotation

    iget-boolean v0, p0, Lcom/android/server/tv/TvHanlder;->sufaceViewFlag:Z

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/android/server/tv/TvHanlder;->wm:Landroid/view/WindowManager;

    iget-object v1, p0, Lcom/android/server/tv/TvHanlder;->sufaceView:Landroid/view/SurfaceView;

    invoke-interface {v0, v1}, Landroid/view/WindowManager;->removeView(Landroid/view/View;)V

    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/android/server/tv/TvHanlder;->sufaceViewFlag:Z

    :cond_0
    iget-object v0, p0, Lcom/android/server/tv/TvHanlder;->mTvService:Lcom/mstar/android/tv/ITvService;

    invoke-interface {v0}, Lcom/mstar/android/tv/ITvService;->getTvPicture()Lcom/mstar/android/tv/ITvPicture;

    move-result-object v0

    iget-object v1, p0, Lcom/android/server/tv/TvHanlder;->mTvService:Lcom/mstar/android/tv/ITvService;

    invoke-interface {v1}, Lcom/mstar/android/tv/ITvService;->getTvPicture()Lcom/mstar/android/tv/ITvPicture;

    move-result-object v1

    invoke-interface {v1}, Lcom/mstar/android/tv/ITvPicture;->getVideoArc()I

    move-result v1

    invoke-interface {v0, v1}, Lcom/mstar/android/tv/ITvPicture;->setVideoArc(I)Z

    return-void
.end method

.method private openSurfaceView(IIII)V
    .locals 4
    .param p1    # I
    .param p2    # I
    .param p3    # I
    .param p4    # I

    iget-boolean v1, p0, Lcom/android/server/tv/TvHanlder;->sufaceViewFlag:Z

    if-nez v1, :cond_0

    const/4 v1, 0x1

    iput-boolean v1, p0, Lcom/android/server/tv/TvHanlder;->sufaceViewFlag:Z

    new-instance v1, Landroid/view/SurfaceView;

    iget-object v2, p0, Lcom/android/server/tv/TvHanlder;->mContext:Landroid/content/Context;

    invoke-direct {v1, v2}, Landroid/view/SurfaceView;-><init>(Landroid/content/Context;)V

    iput-object v1, p0, Lcom/android/server/tv/TvHanlder;->sufaceView:Landroid/view/SurfaceView;

    iget-object v1, p0, Lcom/android/server/tv/TvHanlder;->sufaceView:Landroid/view/SurfaceView;

    invoke-virtual {v1}, Landroid/view/SurfaceView;->getHolder()Landroid/view/SurfaceHolder;

    move-result-object v1

    const/4 v2, 0x3

    invoke-interface {v1, v2}, Landroid/view/SurfaceHolder;->setType(I)V

    new-instance v1, Landroid/view/WindowManager$LayoutParams;

    invoke-direct {v1}, Landroid/view/WindowManager$LayoutParams;-><init>()V

    iput-object v1, p0, Lcom/android/server/tv/TvHanlder;->surfaceParams:Landroid/view/WindowManager$LayoutParams;

    iget-object v1, p0, Lcom/android/server/tv/TvHanlder;->surfaceParams:Landroid/view/WindowManager$LayoutParams;

    iput p1, v1, Landroid/view/WindowManager$LayoutParams;->x:I

    iget-object v1, p0, Lcom/android/server/tv/TvHanlder;->surfaceParams:Landroid/view/WindowManager$LayoutParams;

    iput p2, v1, Landroid/view/WindowManager$LayoutParams;->y:I

    iget-object v1, p0, Lcom/android/server/tv/TvHanlder;->surfaceParams:Landroid/view/WindowManager$LayoutParams;

    iput p3, v1, Landroid/view/WindowManager$LayoutParams;->width:I

    iget-object v1, p0, Lcom/android/server/tv/TvHanlder;->surfaceParams:Landroid/view/WindowManager$LayoutParams;

    iput p4, v1, Landroid/view/WindowManager$LayoutParams;->height:I

    iget-object v1, p0, Lcom/android/server/tv/TvHanlder;->surfaceParams:Landroid/view/WindowManager$LayoutParams;

    const/16 v2, 0x7d8

    iput v2, v1, Landroid/view/WindowManager$LayoutParams;->type:I

    iget-object v1, p0, Lcom/android/server/tv/TvHanlder;->surfaceParams:Landroid/view/WindowManager$LayoutParams;

    const/16 v2, 0x8

    iput v2, v1, Landroid/view/WindowManager$LayoutParams;->flags:I

    iget-object v1, p0, Lcom/android/server/tv/TvHanlder;->surfaceParams:Landroid/view/WindowManager$LayoutParams;

    const/16 v2, 0x33

    iput v2, v1, Landroid/view/WindowManager$LayoutParams;->gravity:I

    new-instance v0, Lcom/android/server/tv/TvHanlder$1;

    invoke-direct {v0, p0}, Lcom/android/server/tv/TvHanlder$1;-><init>(Lcom/android/server/tv/TvHanlder;)V

    iget-object v1, p0, Lcom/android/server/tv/TvHanlder;->sufaceView:Landroid/view/SurfaceView;

    invoke-virtual {v1}, Landroid/view/SurfaceView;->getHolder()Landroid/view/SurfaceHolder;

    move-result-object v1

    invoke-interface {v1, v0}, Landroid/view/SurfaceHolder;->addCallback(Landroid/view/SurfaceHolder$Callback;)V

    iget-object v1, p0, Lcom/android/server/tv/TvHanlder;->mContext:Landroid/content/Context;

    const-string v2, "window"

    invoke-virtual {v1, v2}, Landroid/content/Context;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Landroid/view/WindowManager;

    iput-object v1, p0, Lcom/android/server/tv/TvHanlder;->wm:Landroid/view/WindowManager;

    iget-object v1, p0, Lcom/android/server/tv/TvHanlder;->wm:Landroid/view/WindowManager;

    iget-object v2, p0, Lcom/android/server/tv/TvHanlder;->sufaceView:Landroid/view/SurfaceView;

    iget-object v3, p0, Lcom/android/server/tv/TvHanlder;->surfaceParams:Landroid/view/WindowManager$LayoutParams;

    invoke-interface {v1, v2, v3}, Landroid/view/WindowManager;->addView(Landroid/view/View;Landroid/view/ViewGroup$LayoutParams;)V

    :cond_0
    return-void
.end method

.method private pipSurfaceInit()V
    .locals 4

    iget-object v2, p0, Lcom/android/server/tv/TvHanlder;->mContext:Landroid/content/Context;

    const-string v3, "window"

    invoke-virtual {v2, v3}, Landroid/content/Context;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Landroid/view/WindowManager;

    iput-object v2, p0, Lcom/android/server/tv/TvHanlder;->PIPwm:Landroid/view/WindowManager;

    iget-object v2, p0, Lcom/android/server/tv/TvHanlder;->mContext:Landroid/content/Context;

    const-string v3, "layout_inflater"

    invoke-virtual {v2, v3}, Landroid/content/Context;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/view/LayoutInflater;

    const v2, 0x10900a3    # android.R.layout.pipsurface

    const/4 v3, 0x0

    invoke-virtual {v0, v2, v3}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;)Landroid/view/View;

    move-result-object v2

    sput-object v2, Lcom/android/server/tv/TvHanlder;->PIPView:Landroid/view/View;

    sget-object v2, Lcom/android/server/tv/TvHanlder;->PIPView:Landroid/view/View;

    const v3, 0x1020467    # android.R.id.pip_surface

    invoke-virtual {v2, v3}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v2

    check-cast v2, Landroid/view/SurfaceView;

    iput-object v2, p0, Lcom/android/server/tv/TvHanlder;->PIPSurface:Landroid/view/SurfaceView;

    iget-object v2, p0, Lcom/android/server/tv/TvHanlder;->PIPSurface:Landroid/view/SurfaceView;

    invoke-virtual {v2}, Landroid/view/SurfaceView;->getHolder()Landroid/view/SurfaceHolder;

    move-result-object v2

    iput-object v2, p0, Lcom/android/server/tv/TvHanlder;->mSurfaceHolder:Landroid/view/SurfaceHolder;

    iget-object v2, p0, Lcom/android/server/tv/TvHanlder;->mSurfaceHolder:Landroid/view/SurfaceHolder;

    invoke-interface {v2, p0}, Landroid/view/SurfaceHolder;->addCallback(Landroid/view/SurfaceHolder$Callback;)V

    iget-object v2, p0, Lcom/android/server/tv/TvHanlder;->mSurfaceHolder:Landroid/view/SurfaceHolder;

    const/4 v3, 0x3

    invoke-interface {v2, v3}, Landroid/view/SurfaceHolder;->setType(I)V

    new-instance v1, Landroid/view/WindowManager$LayoutParams;

    invoke-direct {v1}, Landroid/view/WindowManager$LayoutParams;-><init>()V

    const/16 v2, 0x7d8

    iput v2, v1, Landroid/view/WindowManager$LayoutParams;->type:I

    const/16 v2, 0x8

    iput v2, v1, Landroid/view/WindowManager$LayoutParams;->flags:I

    iget v2, p0, Lcom/android/server/tv/TvHanlder;->PIP_xPos:I

    iput v2, v1, Landroid/view/WindowManager$LayoutParams;->x:I

    iget v2, p0, Lcom/android/server/tv/TvHanlder;->PIP_yPos:I

    iput v2, v1, Landroid/view/WindowManager$LayoutParams;->y:I

    iget v2, p0, Lcom/android/server/tv/TvHanlder;->PIP_width:I

    iput v2, v1, Landroid/view/WindowManager$LayoutParams;->width:I

    iget v2, p0, Lcom/android/server/tv/TvHanlder;->PIP_height:I

    iput v2, v1, Landroid/view/WindowManager$LayoutParams;->height:I

    const/16 v2, 0x33

    iput v2, v1, Landroid/view/WindowManager$LayoutParams;->gravity:I

    iget-object v2, p0, Lcom/android/server/tv/TvHanlder;->PIPwm:Landroid/view/WindowManager;

    sget-object v3, Lcom/android/server/tv/TvHanlder;->PIPView:Landroid/view/View;

    invoke-interface {v2, v3, v1}, Landroid/view/WindowManager;->addView(Landroid/view/View;Landroid/view/ViewGroup$LayoutParams;)V

    return-void
.end method

.method private removeOtherView(Lcom/android/server/tv/TvHanlder$HotKey;)V
    .locals 4
    .param p1    # Lcom/android/server/tv/TvHanlder$HotKey;

    const/4 v3, 0x0

    sget-object v1, Lcom/android/server/tv/TvHanlder$HotKey;->SleepMode:Lcom/android/server/tv/TvHanlder$HotKey;

    if-eq v1, p1, :cond_0

    sput-boolean v3, Lcom/android/server/tv/TvHanlder;->isSleepModeShowed:Z

    iget-object v1, p0, Lcom/android/server/tv/TvHanlder;->sleepModeView:Landroid/view/View;

    if-eqz v1, :cond_0

    :try_start_0
    iget-object v1, p0, Lcom/android/server/tv/TvHanlder;->wm:Landroid/view/WindowManager;

    iget-object v2, p0, Lcom/android/server/tv/TvHanlder;->sleepModeView:Landroid/view/View;

    invoke-interface {v1, v2}, Landroid/view/WindowManager;->removeView(Landroid/view/View;)V
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    :cond_0
    :goto_0
    sget-object v1, Lcom/android/server/tv/TvHanlder$HotKey;->AudioMode:Lcom/android/server/tv/TvHanlder$HotKey;

    if-eq v1, p1, :cond_1

    sput-boolean v3, Lcom/android/server/tv/TvHanlder;->isAudioModeShowed:Z

    iget-object v1, p0, Lcom/android/server/tv/TvHanlder;->audioModeView:Landroid/view/View;

    if-eqz v1, :cond_1

    :try_start_1
    iget-object v1, p0, Lcom/android/server/tv/TvHanlder;->wm:Landroid/view/WindowManager;

    iget-object v2, p0, Lcom/android/server/tv/TvHanlder;->audioModeView:Landroid/view/View;

    invoke-interface {v1, v2}, Landroid/view/WindowManager;->removeView(Landroid/view/View;)V
    :try_end_1
    .catch Ljava/lang/Exception; {:try_start_1 .. :try_end_1} :catch_1

    :cond_1
    :goto_1
    sget-object v1, Lcom/android/server/tv/TvHanlder$HotKey;->PicMode:Lcom/android/server/tv/TvHanlder$HotKey;

    if-eq v1, p1, :cond_2

    sput-boolean v3, Lcom/android/server/tv/TvHanlder;->isPicModeShowed:Z

    iget-object v1, p0, Lcom/android/server/tv/TvHanlder;->picModeView:Landroid/view/View;

    if-eqz v1, :cond_2

    :try_start_2
    iget-object v1, p0, Lcom/android/server/tv/TvHanlder;->wm:Landroid/view/WindowManager;

    iget-object v2, p0, Lcom/android/server/tv/TvHanlder;->picModeView:Landroid/view/View;

    invoke-interface {v1, v2}, Landroid/view/WindowManager;->removeView(Landroid/view/View;)V
    :try_end_2
    .catch Ljava/lang/Exception; {:try_start_2 .. :try_end_2} :catch_2

    :cond_2
    :goto_2
    sget-object v1, Lcom/android/server/tv/TvHanlder$HotKey;->Zoom:Lcom/android/server/tv/TvHanlder$HotKey;

    if-eq v1, p1, :cond_3

    sput-boolean v3, Lcom/android/server/tv/TvHanlder;->isZoomShowed:Z

    iget-object v1, p0, Lcom/android/server/tv/TvHanlder;->zoomView:Landroid/view/View;

    if-eqz v1, :cond_3

    :try_start_3
    iget-object v1, p0, Lcom/android/server/tv/TvHanlder;->wm:Landroid/view/WindowManager;

    iget-object v2, p0, Lcom/android/server/tv/TvHanlder;->zoomView:Landroid/view/View;

    invoke-interface {v1, v2}, Landroid/view/WindowManager;->removeView(Landroid/view/View;)V
    :try_end_3
    .catch Ljava/lang/Exception; {:try_start_3 .. :try_end_3} :catch_3

    :cond_3
    :goto_3
    return-void

    :catch_0
    move-exception v0

    const-string v1, "hotkey"

    const-string v2, "remove fail"

    invoke-static {v1, v2}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_0

    :catch_1
    move-exception v0

    const-string v1, "hotkey"

    const-string v2, "remove fail"

    invoke-static {v1, v2}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_1

    :catch_2
    move-exception v0

    const-string v1, "hotkey"

    const-string v2, "remove fail"

    invoke-static {v1, v2}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_2

    :catch_3
    move-exception v0

    const-string v1, "hotkey"

    const-string v2, "remove fail"

    invoke-static {v1, v2}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_3
.end method

.method private restorePIPSurfacePara()V
    .locals 1

    const/4 v0, 0x0

    iput v0, p0, Lcom/android/server/tv/TvHanlder;->PIP_width:I

    iput v0, p0, Lcom/android/server/tv/TvHanlder;->PIP_height:I

    iput v0, p0, Lcom/android/server/tv/TvHanlder;->PIP_xPos:I

    iput v0, p0, Lcom/android/server/tv/TvHanlder;->PIP_yPos:I

    return-void
.end method

.method private setSurfaceView(IIII)V
    .locals 5
    .param p1    # I
    .param p2    # I
    .param p3    # I
    .param p4    # I

    iget-boolean v2, p0, Lcom/android/server/tv/TvHanlder;->sufaceViewFlag:Z

    if-eqz v2, :cond_1

    iget-object v2, p0, Lcom/android/server/tv/TvHanlder;->surfaceParams:Landroid/view/WindowManager$LayoutParams;

    iput p1, v2, Landroid/view/WindowManager$LayoutParams;->x:I

    iget-object v2, p0, Lcom/android/server/tv/TvHanlder;->surfaceParams:Landroid/view/WindowManager$LayoutParams;

    iput p2, v2, Landroid/view/WindowManager$LayoutParams;->y:I

    iget-object v2, p0, Lcom/android/server/tv/TvHanlder;->surfaceParams:Landroid/view/WindowManager$LayoutParams;

    iput p3, v2, Landroid/view/WindowManager$LayoutParams;->width:I

    iget-object v2, p0, Lcom/android/server/tv/TvHanlder;->surfaceParams:Landroid/view/WindowManager$LayoutParams;

    iput p4, v2, Landroid/view/WindowManager$LayoutParams;->height:I

    new-instance v1, Lcom/mstar/android/tvapi/common/vo/VideoWindowType;

    invoke-direct {v1}, Lcom/mstar/android/tvapi/common/vo/VideoWindowType;-><init>()V

    iget-object v2, p0, Lcom/android/server/tv/TvHanlder;->surfaceParams:Landroid/view/WindowManager$LayoutParams;

    iget v2, v2, Landroid/view/WindowManager$LayoutParams;->height:I

    iput v2, v1, Lcom/mstar/android/tvapi/common/vo/VideoWindowType;->height:I

    iget-object v2, p0, Lcom/android/server/tv/TvHanlder;->surfaceParams:Landroid/view/WindowManager$LayoutParams;

    iget v2, v2, Landroid/view/WindowManager$LayoutParams;->width:I

    iput v2, v1, Lcom/mstar/android/tvapi/common/vo/VideoWindowType;->width:I

    iget-object v2, p0, Lcom/android/server/tv/TvHanlder;->surfaceParams:Landroid/view/WindowManager$LayoutParams;

    iget v2, v2, Landroid/view/WindowManager$LayoutParams;->x:I

    iput v2, v1, Lcom/mstar/android/tvapi/common/vo/VideoWindowType;->x:I

    iget-object v2, p0, Lcom/android/server/tv/TvHanlder;->surfaceParams:Landroid/view/WindowManager$LayoutParams;

    iget v2, v2, Landroid/view/WindowManager$LayoutParams;->y:I

    iput v2, v1, Lcom/mstar/android/tvapi/common/vo/VideoWindowType;->y:I

    :try_start_0
    invoke-static {}, Lcom/mstar/android/tvapi/common/TvManager;->getInstance()Lcom/mstar/android/tvapi/common/TvManager;

    move-result-object v2

    if-eqz v2, :cond_0

    invoke-static {}, Lcom/mstar/android/tvapi/common/TvManager;->getInstance()Lcom/mstar/android/tvapi/common/TvManager;

    move-result-object v2

    invoke-virtual {v2}, Lcom/mstar/android/tvapi/common/TvManager;->getPictureManager()Lcom/mstar/android/tvapi/common/PictureManager;

    move-result-object v2

    sget-object v3, Lcom/mstar/android/tvapi/common/vo/EnumScalerWindow;->E_MAIN_WINDOW:Lcom/mstar/android/tvapi/common/vo/EnumScalerWindow;

    invoke-virtual {v2, v3}, Lcom/mstar/android/tvapi/common/PictureManager;->selectWindow(Lcom/mstar/android/tvapi/common/vo/EnumScalerWindow;)Z

    invoke-static {}, Lcom/mstar/android/tvapi/common/TvManager;->getInstance()Lcom/mstar/android/tvapi/common/TvManager;

    move-result-object v2

    invoke-virtual {v2}, Lcom/mstar/android/tvapi/common/TvManager;->getPictureManager()Lcom/mstar/android/tvapi/common/PictureManager;

    move-result-object v2

    invoke-virtual {v2, v1}, Lcom/mstar/android/tvapi/common/PictureManager;->setDisplayWindow(Lcom/mstar/android/tvapi/common/vo/VideoWindowType;)V

    invoke-static {}, Lcom/mstar/android/tvapi/common/TvManager;->getInstance()Lcom/mstar/android/tvapi/common/TvManager;

    move-result-object v2

    invoke-virtual {v2}, Lcom/mstar/android/tvapi/common/TvManager;->getPictureManager()Lcom/mstar/android/tvapi/common/PictureManager;

    move-result-object v2

    invoke-virtual {v2}, Lcom/mstar/android/tvapi/common/PictureManager;->scaleWindow()Z
    :try_end_0
    .catch Ljava/lang/Throwable; {:try_start_0 .. :try_end_0} :catch_0

    :cond_0
    :goto_0
    iget-object v2, p0, Lcom/android/server/tv/TvHanlder;->wm:Landroid/view/WindowManager;

    iget-object v3, p0, Lcom/android/server/tv/TvHanlder;->sufaceView:Landroid/view/SurfaceView;

    iget-object v4, p0, Lcom/android/server/tv/TvHanlder;->surfaceParams:Landroid/view/WindowManager$LayoutParams;

    invoke-interface {v2, v3, v4}, Landroid/view/WindowManager;->updateViewLayout(Landroid/view/View;Landroid/view/ViewGroup$LayoutParams;)V

    :cond_1
    return-void

    :catch_0
    move-exception v0

    invoke-virtual {v0}, Ljava/lang/Throwable;->printStackTrace()V

    goto :goto_0
.end method


# virtual methods
.method public PIPSurface(Z)V
    .locals 5
    .param p1    # Z

    const/16 v4, 0x7d8

    const/16 v3, 0x8

    const/4 v2, 0x0

    iget-object v1, p0, Lcom/android/server/tv/TvHanlder;->PIPwm:Landroid/view/WindowManager;

    if-nez v1, :cond_0

    :goto_0
    return-void

    :cond_0
    if-eqz p1, :cond_1

    new-instance v0, Landroid/view/WindowManager$LayoutParams;

    invoke-direct {v0}, Landroid/view/WindowManager$LayoutParams;-><init>()V

    iput v4, v0, Landroid/view/WindowManager$LayoutParams;->type:I

    iput v3, v0, Landroid/view/WindowManager$LayoutParams;->flags:I

    iget v1, p0, Lcom/android/server/tv/TvHanlder;->PIP_xPos:I

    iput v1, v0, Landroid/view/WindowManager$LayoutParams;->x:I

    iget v1, p0, Lcom/android/server/tv/TvHanlder;->PIP_yPos:I

    iput v1, v0, Landroid/view/WindowManager$LayoutParams;->y:I

    iget v1, p0, Lcom/android/server/tv/TvHanlder;->PIP_width:I

    iput v1, v0, Landroid/view/WindowManager$LayoutParams;->width:I

    iget v1, p0, Lcom/android/server/tv/TvHanlder;->PIP_height:I

    iput v1, v0, Landroid/view/WindowManager$LayoutParams;->height:I

    const/16 v1, 0x33

    iput v1, v0, Landroid/view/WindowManager$LayoutParams;->gravity:I

    iget-object v1, p0, Lcom/android/server/tv/TvHanlder;->PIPwm:Landroid/view/WindowManager;

    sget-object v2, Lcom/android/server/tv/TvHanlder;->PIPView:Landroid/view/View;

    invoke-interface {v1, v2, v0}, Landroid/view/WindowManager;->updateViewLayout(Landroid/view/View;Landroid/view/ViewGroup$LayoutParams;)V

    goto :goto_0

    :cond_1
    new-instance v0, Landroid/view/WindowManager$LayoutParams;

    invoke-direct {v0}, Landroid/view/WindowManager$LayoutParams;-><init>()V

    iput v4, v0, Landroid/view/WindowManager$LayoutParams;->type:I

    iput v3, v0, Landroid/view/WindowManager$LayoutParams;->flags:I

    iput v2, v0, Landroid/view/WindowManager$LayoutParams;->x:I

    iput v2, v0, Landroid/view/WindowManager$LayoutParams;->y:I

    iput v2, v0, Landroid/view/WindowManager$LayoutParams;->width:I

    iput v2, v0, Landroid/view/WindowManager$LayoutParams;->height:I

    const/16 v1, 0x35

    iput v1, v0, Landroid/view/WindowManager$LayoutParams;->gravity:I

    iget-object v1, p0, Lcom/android/server/tv/TvHanlder;->PIPwm:Landroid/view/WindowManager;

    sget-object v2, Lcom/android/server/tv/TvHanlder;->PIPView:Landroid/view/View;

    invoke-interface {v1, v2, v0}, Landroid/view/WindowManager;->updateViewLayout(Landroid/view/View;Landroid/view/ViewGroup$LayoutParams;)V

    goto :goto_0
.end method

.method public getResolution(Landroid/content/Context;)Lcom/android/server/tv/TvHanlder$EnumResolution;
    .locals 8
    .param p1    # Landroid/content/Context;

    const/16 v7, 0x780

    iget-object v4, p0, Lcom/android/server/tv/TvHanlder;->eResolution:Lcom/android/server/tv/TvHanlder$EnumResolution;

    if-eqz v4, :cond_0

    iget-object v4, p0, Lcom/android/server/tv/TvHanlder;->eResolution:Lcom/android/server/tv/TvHanlder$EnumResolution;

    :goto_0
    return-object v4

    :cond_0
    const/4 v2, 0x0

    :try_start_0
    invoke-static {}, Lcom/mstar/android/tvapi/common/TvManager;->getInstance()Lcom/mstar/android/tvapi/common/TvManager;

    move-result-object v4

    invoke-virtual {v4}, Lcom/mstar/android/tvapi/common/TvManager;->getPictureManager()Lcom/mstar/android/tvapi/common/PictureManager;

    move-result-object v4

    invoke-virtual {v4}, Lcom/mstar/android/tvapi/common/PictureManager;->getPanelWidthHeight()Lcom/mstar/android/tvapi/common/vo/PanelProperty;
    :try_end_0
    .catch Lcom/mstar/android/tvapi/common/exception/TvCommonException; {:try_start_0 .. :try_end_0} :catch_0

    move-result-object v2

    :goto_1
    invoke-virtual {p1}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v4

    invoke-virtual {v4}, Landroid/content/res/Resources;->getDisplayMetrics()Landroid/util/DisplayMetrics;

    move-result-object v4

    iget v1, v4, Landroid/util/DisplayMetrics;->widthPixels:I

    iget v3, v2, Lcom/mstar/android/tvapi/common/vo/PanelProperty;->width:I

    const-string v4, "TvHanlder"

    new-instance v5, Ljava/lang/StringBuilder;

    invoke-direct {v5}, Ljava/lang/StringBuilder;-><init>()V

    const-string v6, "widthPixels ==== "

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v5

    const-string v6, "; panelWidth === "

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5, v3}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    invoke-static {v4, v5}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    if-ne v7, v3, :cond_2

    if-ne v7, v1, :cond_1

    sget-object v4, Lcom/android/server/tv/TvHanlder$EnumResolution;->E_1080P:Lcom/android/server/tv/TvHanlder$EnumResolution;

    iput-object v4, p0, Lcom/android/server/tv/TvHanlder;->eResolution:Lcom/android/server/tv/TvHanlder$EnumResolution;

    goto :goto_0

    :catch_0
    move-exception v0

    invoke-virtual {v0}, Lcom/mstar/android/tvapi/common/exception/TvCommonException;->printStackTrace()V

    goto :goto_1

    :cond_1
    sget-object v4, Lcom/android/server/tv/TvHanlder$EnumResolution;->E_1080P_OSD_720P:Lcom/android/server/tv/TvHanlder$EnumResolution;

    iput-object v4, p0, Lcom/android/server/tv/TvHanlder;->eResolution:Lcom/android/server/tv/TvHanlder$EnumResolution;

    goto :goto_0

    :cond_2
    sget-object v4, Lcom/android/server/tv/TvHanlder$EnumResolution;->E_720P:Lcom/android/server/tv/TvHanlder$EnumResolution;

    iput-object v4, p0, Lcom/android/server/tv/TvHanlder;->eResolution:Lcom/android/server/tv/TvHanlder$EnumResolution;

    goto :goto_0
.end method

.method public handleMessage(Landroid/os/Message;)V
    .locals 30
    .param p1    # Landroid/os/Message;

    invoke-super/range {p0 .. p1}, Landroid/os/Handler;->handleMessage(Landroid/os/Message;)V

    const-string v25, "TvService"

    new-instance v26, Ljava/lang/StringBuilder;

    invoke-direct/range {v26 .. v26}, Ljava/lang/StringBuilder;-><init>()V

    const-string v27, "TvHanlder handler msg:"

    invoke-virtual/range {v26 .. v27}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v26

    move-object/from16 v0, p1

    iget v0, v0, Landroid/os/Message;->what:I

    move/from16 v27, v0

    invoke-virtual/range {v26 .. v27}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v26

    invoke-virtual/range {v26 .. v26}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v26

    invoke-static/range {v25 .. v26}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    :try_start_0
    move-object/from16 v0, p1

    iget v0, v0, Landroid/os/Message;->what:I

    move/from16 v25, v0

    sparse-switch v25, :sswitch_data_0

    const-string v25, "TvService"

    const-string v26, "Unknown command!"

    invoke-static/range {v25 .. v26}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    :cond_0
    :goto_0
    return-void

    :sswitch_0
    const/16 v25, 0x0

    sput-boolean v25, Lcom/android/server/tv/TvHanlder;->isSleepModeShowed:Z

    const/16 v25, 0x0

    sput-boolean v25, Lcom/android/server/tv/TvHanlder;->isAudioModeShowed:Z

    const/16 v25, 0x0

    sput-boolean v25, Lcom/android/server/tv/TvHanlder;->isPicModeShowed:Z

    const/16 v25, 0x0

    sput-boolean v25, Lcom/android/server/tv/TvHanlder;->isZoomShowed:Z

    const/16 v25, 0x0

    sput-boolean v25, Lcom/android/server/tv/TvHanlder;->isMuteShowed:Z

    const/16 v25, 0x0

    sput-boolean v25, Lcom/android/server/tv/TvHanlder;->isFreezeShowed:Z

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/android/server/tv/TvHanlder;->sleepModeView:Landroid/view/View;

    move-object/from16 v25, v0
    :try_end_0
    .catch Landroid/os/RemoteException; {:try_start_0 .. :try_end_0} :catch_1

    if-eqz v25, :cond_1

    :try_start_1
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/android/server/tv/TvHanlder;->wm:Landroid/view/WindowManager;

    move-object/from16 v25, v0

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/android/server/tv/TvHanlder;->sleepModeView:Landroid/view/View;

    move-object/from16 v26, v0

    invoke-interface/range {v25 .. v26}, Landroid/view/WindowManager;->removeView(Landroid/view/View;)V
    :try_end_1
    .catch Ljava/lang/Exception; {:try_start_1 .. :try_end_1} :catch_2
    .catch Landroid/os/RemoteException; {:try_start_1 .. :try_end_1} :catch_1

    :cond_1
    :goto_1
    :try_start_2
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/android/server/tv/TvHanlder;->audioModeView:Landroid/view/View;

    move-object/from16 v25, v0
    :try_end_2
    .catch Landroid/os/RemoteException; {:try_start_2 .. :try_end_2} :catch_1

    if-eqz v25, :cond_2

    :try_start_3
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/android/server/tv/TvHanlder;->wm:Landroid/view/WindowManager;

    move-object/from16 v25, v0

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/android/server/tv/TvHanlder;->audioModeView:Landroid/view/View;

    move-object/from16 v26, v0

    invoke-interface/range {v25 .. v26}, Landroid/view/WindowManager;->removeView(Landroid/view/View;)V
    :try_end_3
    .catch Ljava/lang/Exception; {:try_start_3 .. :try_end_3} :catch_3
    .catch Landroid/os/RemoteException; {:try_start_3 .. :try_end_3} :catch_1

    :cond_2
    :goto_2
    :try_start_4
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/android/server/tv/TvHanlder;->picModeView:Landroid/view/View;

    move-object/from16 v25, v0
    :try_end_4
    .catch Landroid/os/RemoteException; {:try_start_4 .. :try_end_4} :catch_1

    if-eqz v25, :cond_3

    :try_start_5
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/android/server/tv/TvHanlder;->wm:Landroid/view/WindowManager;

    move-object/from16 v25, v0

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/android/server/tv/TvHanlder;->picModeView:Landroid/view/View;

    move-object/from16 v26, v0

    invoke-interface/range {v25 .. v26}, Landroid/view/WindowManager;->removeView(Landroid/view/View;)V
    :try_end_5
    .catch Ljava/lang/Exception; {:try_start_5 .. :try_end_5} :catch_4
    .catch Landroid/os/RemoteException; {:try_start_5 .. :try_end_5} :catch_1

    :cond_3
    :goto_3
    :try_start_6
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/android/server/tv/TvHanlder;->zoomView:Landroid/view/View;

    move-object/from16 v25, v0
    :try_end_6
    .catch Landroid/os/RemoteException; {:try_start_6 .. :try_end_6} :catch_1

    if-eqz v25, :cond_4

    :try_start_7
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/android/server/tv/TvHanlder;->wm:Landroid/view/WindowManager;

    move-object/from16 v25, v0

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/android/server/tv/TvHanlder;->zoomView:Landroid/view/View;

    move-object/from16 v26, v0

    invoke-interface/range {v25 .. v26}, Landroid/view/WindowManager;->removeView(Landroid/view/View;)V
    :try_end_7
    .catch Ljava/lang/Exception; {:try_start_7 .. :try_end_7} :catch_5
    .catch Landroid/os/RemoteException; {:try_start_7 .. :try_end_7} :catch_1

    :cond_4
    :goto_4
    :try_start_8
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/android/server/tv/TvHanlder;->muteView:Landroid/view/View;

    move-object/from16 v25, v0
    :try_end_8
    .catch Landroid/os/RemoteException; {:try_start_8 .. :try_end_8} :catch_1

    if-eqz v25, :cond_5

    :try_start_9
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/android/server/tv/TvHanlder;->wm:Landroid/view/WindowManager;

    move-object/from16 v25, v0

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/android/server/tv/TvHanlder;->muteView:Landroid/view/View;

    move-object/from16 v26, v0

    invoke-interface/range {v25 .. v26}, Landroid/view/WindowManager;->removeView(Landroid/view/View;)V
    :try_end_9
    .catch Ljava/lang/Exception; {:try_start_9 .. :try_end_9} :catch_6
    .catch Landroid/os/RemoteException; {:try_start_9 .. :try_end_9} :catch_1

    :cond_5
    :goto_5
    :try_start_a
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/android/server/tv/TvHanlder;->freezeView:Landroid/view/View;

    move-object/from16 v25, v0
    :try_end_a
    .catch Landroid/os/RemoteException; {:try_start_a .. :try_end_a} :catch_1

    if-eqz v25, :cond_0

    :try_start_b
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/android/server/tv/TvHanlder;->wm:Landroid/view/WindowManager;

    move-object/from16 v25, v0

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/android/server/tv/TvHanlder;->freezeView:Landroid/view/View;

    move-object/from16 v26, v0

    invoke-interface/range {v25 .. v26}, Landroid/view/WindowManager;->removeView(Landroid/view/View;)V
    :try_end_b
    .catch Ljava/lang/Exception; {:try_start_b .. :try_end_b} :catch_0
    .catch Landroid/os/RemoteException; {:try_start_b .. :try_end_b} :catch_1

    goto/16 :goto_0

    :catch_0
    move-exception v8

    :try_start_c
    invoke-virtual {v8}, Ljava/lang/Exception;->printStackTrace()V
    :try_end_c
    .catch Landroid/os/RemoteException; {:try_start_c .. :try_end_c} :catch_1

    goto/16 :goto_0

    :catch_1
    move-exception v8

    invoke-virtual {v8}, Landroid/os/RemoteException;->printStackTrace()V

    goto/16 :goto_0

    :catch_2
    move-exception v8

    :try_start_d
    const-string v25, "hotkey"

    const-string v26, "remove fail"

    invoke-static/range {v25 .. v26}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    goto/16 :goto_1

    :catch_3
    move-exception v8

    const-string v25, "hotkey"

    const-string v26, "remove fail"

    invoke-static/range {v25 .. v26}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_2

    :catch_4
    move-exception v8

    const-string v25, "hotkey"

    const-string v26, "remove fail"

    invoke-static/range {v25 .. v26}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_3

    :catch_5
    move-exception v8

    const-string v25, "hotkey"

    const-string v26, "remove fail"

    invoke-static/range {v25 .. v26}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_4

    :catch_6
    move-exception v8

    invoke-virtual {v8}, Ljava/lang/Exception;->printStackTrace()V

    goto :goto_5

    :sswitch_1
    const/16 v25, 0x0

    sput-boolean v25, Lcom/android/server/tv/TvHanlder;->isVolShowed:Z

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/android/server/tv/TvHanlder;->timerTask:Lcom/android/server/tv/TvHanlder$WriteVolToDBTask;

    move-object/from16 v25, v0

    if-eqz v25, :cond_6

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/android/server/tv/TvHanlder;->timerTask:Lcom/android/server/tv/TvHanlder$WriteVolToDBTask;

    move-object/from16 v25, v0

    invoke-virtual/range {v25 .. v25}, Lcom/android/server/tv/TvHanlder$WriteVolToDBTask;->cancel()Z

    :cond_6
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/android/server/tv/TvHanlder;->volView:Landroid/view/View;

    move-object/from16 v25, v0
    :try_end_d
    .catch Landroid/os/RemoteException; {:try_start_d .. :try_end_d} :catch_1

    if-eqz v25, :cond_0

    :try_start_e
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/android/server/tv/TvHanlder;->wm:Landroid/view/WindowManager;

    move-object/from16 v25, v0

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/android/server/tv/TvHanlder;->volView:Landroid/view/View;

    move-object/from16 v26, v0

    invoke-interface/range {v25 .. v26}, Landroid/view/WindowManager;->removeView(Landroid/view/View;)V
    :try_end_e
    .catch Ljava/lang/Exception; {:try_start_e .. :try_end_e} :catch_7
    .catch Landroid/os/RemoteException; {:try_start_e .. :try_end_e} :catch_1

    goto/16 :goto_0

    :catch_7
    move-exception v8

    :try_start_f
    const-string v25, "hotkey"

    const-string v26, "remove fail"

    invoke-static/range {v25 .. v26}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    goto/16 :goto_0

    :sswitch_2
    const-string v25, "TvService"

    const-string v26, "PIP_CREATE"

    invoke-static/range {v25 .. v26}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    invoke-virtual/range {p1 .. p1}, Landroid/os/Message;->getData()Landroid/os/Bundle;

    move-result-object v6

    const-string v25, "x"

    move-object/from16 v0, v25

    invoke-virtual {v6, v0}, Landroid/os/Bundle;->getInt(Ljava/lang/String;)I

    move-result v23

    const-string v25, "y"

    move-object/from16 v0, v25

    invoke-virtual {v6, v0}, Landroid/os/Bundle;->getInt(Ljava/lang/String;)I

    move-result v24

    const-string v25, "width"

    move-object/from16 v0, v25

    invoke-virtual {v6, v0}, Landroid/os/Bundle;->getInt(Ljava/lang/String;)I

    move-result v22

    const-string v25, "height"

    move-object/from16 v0, v25

    invoke-virtual {v6, v0}, Landroid/os/Bundle;->getInt(Ljava/lang/String;)I

    move-result v11

    const/4 v15, 0x0

    sget-object v25, Lcom/android/server/tv/TvHanlder$2;->$SwitchMap$com$android$server$tv$TvHanlder$EnumResolution:[I

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/android/server/tv/TvHanlder;->mContext:Landroid/content/Context;

    move-object/from16 v26, v0

    move-object/from16 v0, p0

    move-object/from16 v1, v26

    invoke-virtual {v0, v1}, Lcom/android/server/tv/TvHanlder;->getResolution(Landroid/content/Context;)Lcom/android/server/tv/TvHanlder$EnumResolution;

    move-result-object v26

    invoke-virtual/range {v26 .. v26}, Lcom/android/server/tv/TvHanlder$EnumResolution;->ordinal()I

    move-result v26

    aget v25, v25, v26

    packed-switch v25, :pswitch_data_0

    const/high16 v15, 0x3f800000    # 1.0f

    const-string v25, "TvHanlder"

    const-string v26, "=====error===== "

    invoke-static/range {v25 .. v26}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    :goto_6
    move/from16 v0, v22

    int-to-float v0, v0

    move/from16 v25, v0

    mul-float v25, v25, v15

    move/from16 v0, v25

    float-to-int v0, v0

    move/from16 v22, v0

    int-to-float v0, v11

    move/from16 v25, v0

    mul-float v25, v25, v15

    move/from16 v0, v25

    float-to-int v11, v0

    move/from16 v0, v23

    int-to-float v0, v0

    move/from16 v25, v0

    mul-float v25, v25, v15

    move/from16 v0, v25

    float-to-int v0, v0

    move/from16 v23, v0

    move/from16 v0, v24

    int-to-float v0, v0

    move/from16 v25, v0

    mul-float v25, v25, v15

    move/from16 v0, v25

    float-to-int v0, v0

    move/from16 v24, v0

    sget-object v25, Lcom/android/server/tv/TvHanlder$EnumResolution;->E_720P:Lcom/android/server/tv/TvHanlder$EnumResolution;

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/android/server/tv/TvHanlder;->mContext:Landroid/content/Context;

    move-object/from16 v26, v0

    move-object/from16 v0, p0

    move-object/from16 v1, v26

    invoke-virtual {v0, v1}, Lcom/android/server/tv/TvHanlder;->getResolution(Landroid/content/Context;)Lcom/android/server/tv/TvHanlder$EnumResolution;

    move-result-object v26

    move-object/from16 v0, v25

    move-object/from16 v1, v26

    if-ne v0, v1, :cond_7

    add-int/lit8 v25, v22, 0x2

    add-int/lit8 v26, v11, 0x2

    move-object/from16 v0, p0

    move/from16 v1, v23

    move/from16 v2, v24

    move/from16 v3, v25

    move/from16 v4, v26

    invoke-virtual {v0, v1, v2, v3, v4}, Lcom/android/server/tv/TvHanlder;->setPIPSurfacePara(IIII)V

    :goto_7
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/android/server/tv/TvHanlder;->PIPwm:Landroid/view/WindowManager;

    move-object/from16 v25, v0

    if-nez v25, :cond_8

    invoke-direct/range {p0 .. p0}, Lcom/android/server/tv/TvHanlder;->pipSurfaceInit()V

    goto/16 :goto_0

    :pswitch_0
    const/high16 v15, 0x3f800000    # 1.0f

    goto :goto_6

    :pswitch_1
    const v15, 0x3f6fe204

    goto :goto_6

    :pswitch_2
    const v15, 0x3f2aaaab

    goto :goto_6

    :cond_7
    add-int/lit8 v25, v23, -0x2

    add-int/lit8 v26, v22, 0x2

    add-int/lit8 v27, v11, 0x2

    move-object/from16 v0, p0

    move/from16 v1, v25

    move/from16 v2, v24

    move/from16 v3, v26

    move/from16 v4, v27

    invoke-virtual {v0, v1, v2, v3, v4}, Lcom/android/server/tv/TvHanlder;->setPIPSurfacePara(IIII)V

    goto :goto_7

    :cond_8
    const/16 v25, 0x1

    move-object/from16 v0, p0

    move/from16 v1, v25

    invoke-virtual {v0, v1}, Lcom/android/server/tv/TvHanlder;->PIPSurface(Z)V

    goto/16 :goto_0

    :sswitch_3
    const-string v25, "TvService"

    const-string v26, "PIP_DESTROY"

    invoke-static/range {v25 .. v26}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    const/16 v25, 0x0

    move-object/from16 v0, p0

    move/from16 v1, v25

    invoke-virtual {v0, v1}, Lcom/android/server/tv/TvHanlder;->PIPSurface(Z)V

    goto/16 :goto_0

    :sswitch_4
    const-string v25, "TvHanlder"

    new-instance v26, Ljava/lang/StringBuilder;

    invoke-direct/range {v26 .. v26}, Ljava/lang/StringBuilder;-><init>()V

    const-string v27, "TvHanlder handler msg:"

    invoke-virtual/range {v26 .. v27}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v26

    move-object/from16 v0, p1

    iget v0, v0, Landroid/os/Message;->what:I

    move/from16 v27, v0

    invoke-virtual/range {v26 .. v27}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v26

    invoke-virtual/range {v26 .. v26}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v26

    invoke-static/range {v25 .. v26}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/android/server/tv/TvHanlder;->mContext:Landroid/content/Context;

    move-object/from16 v25, v0

    invoke-virtual/range {v25 .. v25}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v25

    const v26, 0x1070039    # android.R.array.tv_sleep_mode_types

    invoke-virtual/range {v25 .. v26}, Landroid/content/res/Resources;->getStringArray(I)[Ljava/lang/String;

    move-result-object v25

    move-object/from16 v0, v25

    move-object/from16 v1, p0

    iput-object v0, v1, Lcom/android/server/tv/TvHanlder;->sleepModeVals:[Ljava/lang/String;

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/android/server/tv/TvHanlder;->mContext:Landroid/content/Context;

    move-object/from16 v25, v0

    const-string v26, "layout_inflater"

    invoke-virtual/range {v25 .. v26}, Landroid/content/Context;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v13

    check-cast v13, Landroid/view/LayoutInflater;

    new-instance v18, Landroid/view/WindowManager$LayoutParams;

    const/16 v25, 0x1f4

    const/16 v26, 0xc8

    move-object/from16 v0, v18

    move/from16 v1, v25

    move/from16 v2, v26

    invoke-direct {v0, v1, v2}, Landroid/view/WindowManager$LayoutParams;-><init>(II)V

    const/16 v25, 0x7d6

    move/from16 v0, v25

    move-object/from16 v1, v18

    iput v0, v1, Landroid/view/WindowManager$LayoutParams;->type:I

    const/16 v25, 0x1

    move/from16 v0, v25

    move-object/from16 v1, v18

    iput v0, v1, Landroid/view/WindowManager$LayoutParams;->format:I

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/android/server/tv/TvHanlder;->wm:Landroid/view/WindowManager;

    move-object/from16 v25, v0

    if-nez v25, :cond_9

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/android/server/tv/TvHanlder;->mContext:Landroid/content/Context;

    move-object/from16 v25, v0

    const-string v26, "window"

    invoke-virtual/range {v25 .. v26}, Landroid/content/Context;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v25

    check-cast v25, Landroid/view/WindowManager;

    move-object/from16 v0, v25

    move-object/from16 v1, p0

    iput-object v0, v1, Lcom/android/server/tv/TvHanlder;->wm:Landroid/view/WindowManager;

    :cond_9
    sget-boolean v25, Lcom/android/server/tv/TvHanlder;->isSleepModeShowed:Z

    if-nez v25, :cond_b

    const v25, 0x10900ec    # android.R.layout.tv_sleep_mode

    const/16 v26, 0x0

    move/from16 v0, v25

    move-object/from16 v1, v26

    invoke-virtual {v13, v0, v1}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;)Landroid/view/View;

    move-result-object v25

    move-object/from16 v0, v25

    move-object/from16 v1, p0

    iput-object v0, v1, Lcom/android/server/tv/TvHanlder;->sleepModeView:Landroid/view/View;
    :try_end_f
    .catch Landroid/os/RemoteException; {:try_start_f .. :try_end_f} :catch_1

    :try_start_10
    const-string v25, "TvHanlder"

    new-instance v26, Ljava/lang/StringBuilder;

    invoke-direct/range {v26 .. v26}, Ljava/lang/StringBuilder;-><init>()V

    const-string v27, "TvHanlder sleepModeIdx:"

    invoke-virtual/range {v26 .. v27}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v26

    move-object/from16 v0, p0

    iget v0, v0, Lcom/android/server/tv/TvHanlder;->sleepModeIdx:I

    move/from16 v27, v0

    invoke-virtual/range {v26 .. v27}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v26

    invoke-virtual/range {v26 .. v26}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v26

    invoke-static/range {v25 .. v26}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/android/server/tv/TvHanlder;->mTvService:Lcom/mstar/android/tv/ITvService;

    move-object/from16 v25, v0

    invoke-interface/range {v25 .. v25}, Lcom/mstar/android/tv/ITvService;->getTvTimer()Lcom/mstar/android/tv/ITvTimer;

    move-result-object v25

    invoke-interface/range {v25 .. v25}, Lcom/mstar/android/tv/ITvTimer;->getSleepMode()I

    move-result v25

    move/from16 v0, v25

    move-object/from16 v1, p0

    iput v0, v1, Lcom/android/server/tv/TvHanlder;->sleepModeIdx:I
    :try_end_10
    .catch Landroid/os/RemoteException; {:try_start_10 .. :try_end_10} :catch_8

    :goto_8
    :try_start_11
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/android/server/tv/TvHanlder;->sleepModeView:Landroid/view/View;

    move-object/from16 v25, v0

    const v26, 0x10204c8    # android.R.id.tv_sleep_mode_type

    invoke-virtual/range {v25 .. v26}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v19

    check-cast v19, Landroid/widget/TextView;

    move-object/from16 v0, p0

    iget v0, v0, Lcom/android/server/tv/TvHanlder;->sleepModeIdx:I

    move/from16 v25, v0

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/android/server/tv/TvHanlder;->sleepModeVals:[Ljava/lang/String;

    move-object/from16 v26, v0

    move-object/from16 v0, v26

    array-length v0, v0

    move/from16 v26, v0

    move/from16 v0, v25

    move/from16 v1, v26

    if-lt v0, v1, :cond_a

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/android/server/tv/TvHanlder;->sleepModeVals:[Ljava/lang/String;

    move-object/from16 v25, v0

    move-object/from16 v0, v25

    array-length v0, v0

    move/from16 v25, v0

    add-int/lit8 v25, v25, -0x1

    move/from16 v0, v25

    move-object/from16 v1, p0

    iput v0, v1, Lcom/android/server/tv/TvHanlder;->zoomModeIdx:I

    :cond_a
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/android/server/tv/TvHanlder;->sleepModeVals:[Ljava/lang/String;

    move-object/from16 v25, v0

    move-object/from16 v0, p0

    iget v0, v0, Lcom/android/server/tv/TvHanlder;->sleepModeIdx:I

    move/from16 v26, v0

    aget-object v25, v25, v26

    move-object/from16 v0, v19

    move-object/from16 v1, v25

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/android/server/tv/TvHanlder;->wm:Landroid/view/WindowManager;

    move-object/from16 v25, v0

    invoke-interface/range {v25 .. v25}, Landroid/view/WindowManager;->getDefaultDisplay()Landroid/view/Display;

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/android/server/tv/TvHanlder;->wm:Landroid/view/WindowManager;

    move-object/from16 v25, v0

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/android/server/tv/TvHanlder;->sleepModeView:Landroid/view/View;

    move-object/from16 v26, v0

    move-object/from16 v0, v25

    move-object/from16 v1, v26

    move-object/from16 v2, v18

    invoke-interface {v0, v1, v2}, Landroid/view/WindowManager;->addView(Landroid/view/View;Landroid/view/ViewGroup$LayoutParams;)V

    sget-object v25, Lcom/android/server/tv/TvHanlder$HotKey;->SleepMode:Lcom/android/server/tv/TvHanlder$HotKey;

    move-object/from16 v0, p0

    move-object/from16 v1, v25

    invoke-direct {v0, v1}, Lcom/android/server/tv/TvHanlder;->removeOtherView(Lcom/android/server/tv/TvHanlder$HotKey;)V

    const/16 v25, 0x1

    sput-boolean v25, Lcom/android/server/tv/TvHanlder;->isSleepModeShowed:Z

    const/16 v25, 0xb

    move-object/from16 v0, p0

    move/from16 v1, v25

    invoke-virtual {v0, v1}, Lcom/android/server/tv/TvHanlder;->removeMessages(I)V

    const/16 v25, 0xb

    const-wide/16 v26, 0x1388

    move-object/from16 v0, p0

    move/from16 v1, v25

    move-wide/from16 v2, v26

    invoke-virtual {v0, v1, v2, v3}, Lcom/android/server/tv/TvHanlder;->sendEmptyMessageDelayed(IJ)Z

    goto/16 :goto_0

    :catch_8
    move-exception v8

    invoke-virtual {v8}, Landroid/os/RemoteException;->printStackTrace()V

    goto/16 :goto_8

    :cond_b
    const/16 v25, 0xb

    move-object/from16 v0, p0

    move/from16 v1, v25

    invoke-virtual {v0, v1}, Lcom/android/server/tv/TvHanlder;->removeMessages(I)V

    const/16 v25, 0xb

    const-wide/16 v26, 0x1388

    move-object/from16 v0, p0

    move/from16 v1, v25

    move-wide/from16 v2, v26

    invoke-virtual {v0, v1, v2, v3}, Lcom/android/server/tv/TvHanlder;->sendEmptyMessageDelayed(IJ)Z

    move-object/from16 v0, p0

    iget v0, v0, Lcom/android/server/tv/TvHanlder;->sleepModeIdx:I

    move/from16 v25, v0

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/android/server/tv/TvHanlder;->sleepModeVals:[Ljava/lang/String;

    move-object/from16 v26, v0

    move-object/from16 v0, v26

    array-length v0, v0

    move/from16 v26, v0

    add-int/lit8 v26, v26, -0x1

    move/from16 v0, v25

    move/from16 v1, v26

    if-ge v0, v1, :cond_c

    move-object/from16 v0, p0

    iget v0, v0, Lcom/android/server/tv/TvHanlder;->sleepModeIdx:I

    move/from16 v25, v0

    add-int/lit8 v25, v25, 0x1

    move/from16 v0, v25

    move-object/from16 v1, p0

    iput v0, v1, Lcom/android/server/tv/TvHanlder;->sleepModeIdx:I

    :goto_9
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/android/server/tv/TvHanlder;->sleepModeView:Landroid/view/View;

    move-object/from16 v25, v0

    const v26, 0x10204c8    # android.R.id.tv_sleep_mode_type

    invoke-virtual/range {v25 .. v26}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v19

    check-cast v19, Landroid/widget/TextView;

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/android/server/tv/TvHanlder;->sleepModeVals:[Ljava/lang/String;

    move-object/from16 v25, v0

    move-object/from16 v0, p0

    iget v0, v0, Lcom/android/server/tv/TvHanlder;->sleepModeIdx:I

    move/from16 v26, v0

    aget-object v25, v25, v26

    move-object/from16 v0, v19

    move-object/from16 v1, v25

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V
    :try_end_11
    .catch Landroid/os/RemoteException; {:try_start_11 .. :try_end_11} :catch_1

    :try_start_12
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/android/server/tv/TvHanlder;->mTvService:Lcom/mstar/android/tv/ITvService;

    move-object/from16 v25, v0

    invoke-interface/range {v25 .. v25}, Lcom/mstar/android/tv/ITvService;->getTvTimer()Lcom/mstar/android/tv/ITvTimer;

    move-result-object v25

    move-object/from16 v0, p0

    iget v0, v0, Lcom/android/server/tv/TvHanlder;->sleepModeIdx:I

    move/from16 v26, v0

    invoke-interface/range {v25 .. v26}, Lcom/mstar/android/tv/ITvTimer;->setSleepMode(I)Z
    :try_end_12
    .catch Landroid/os/RemoteException; {:try_start_12 .. :try_end_12} :catch_9

    :goto_a
    :try_start_13
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/android/server/tv/TvHanlder;->wm:Landroid/view/WindowManager;

    move-object/from16 v25, v0

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/android/server/tv/TvHanlder;->sleepModeView:Landroid/view/View;

    move-object/from16 v26, v0

    move-object/from16 v0, v25

    move-object/from16 v1, v26

    move-object/from16 v2, v18

    invoke-interface {v0, v1, v2}, Landroid/view/WindowManager;->updateViewLayout(Landroid/view/View;Landroid/view/ViewGroup$LayoutParams;)V

    goto/16 :goto_0

    :cond_c
    const/16 v25, 0x0

    move/from16 v0, v25

    move-object/from16 v1, p0

    iput v0, v1, Lcom/android/server/tv/TvHanlder;->sleepModeIdx:I

    goto :goto_9

    :catch_9
    move-exception v8

    invoke-virtual {v8}, Landroid/os/RemoteException;->printStackTrace()V

    goto :goto_a

    :sswitch_5
    const-string v25, "TvHanlder"

    new-instance v26, Ljava/lang/StringBuilder;

    invoke-direct/range {v26 .. v26}, Ljava/lang/StringBuilder;-><init>()V

    const-string v27, "TvHanlder handler msg:"

    invoke-virtual/range {v26 .. v27}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v26

    move-object/from16 v0, p1

    iget v0, v0, Landroid/os/Message;->what:I

    move/from16 v27, v0

    invoke-virtual/range {v26 .. v27}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v26

    invoke-virtual/range {v26 .. v26}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v26

    invoke-static/range {v25 .. v26}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/android/server/tv/TvHanlder;->mContext:Landroid/content/Context;

    move-object/from16 v25, v0

    invoke-virtual/range {v25 .. v25}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v25

    const v26, 0x107003a    # android.R.array.tv_sound_mode_types

    invoke-virtual/range {v25 .. v26}, Landroid/content/res/Resources;->getStringArray(I)[Ljava/lang/String;

    move-result-object v25

    move-object/from16 v0, v25

    move-object/from16 v1, p0

    iput-object v0, v1, Lcom/android/server/tv/TvHanlder;->audioModeVals:[Ljava/lang/String;

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/android/server/tv/TvHanlder;->mContext:Landroid/content/Context;

    move-object/from16 v25, v0

    const-string v26, "layout_inflater"

    invoke-virtual/range {v25 .. v26}, Landroid/content/Context;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v13

    check-cast v13, Landroid/view/LayoutInflater;

    new-instance v18, Landroid/view/WindowManager$LayoutParams;

    const/16 v25, 0x1f4

    const/16 v26, 0xc8

    move-object/from16 v0, v18

    move/from16 v1, v25

    move/from16 v2, v26

    invoke-direct {v0, v1, v2}, Landroid/view/WindowManager$LayoutParams;-><init>(II)V

    const/16 v25, 0x7d6

    move/from16 v0, v25

    move-object/from16 v1, v18

    iput v0, v1, Landroid/view/WindowManager$LayoutParams;->type:I

    const/16 v25, 0x1

    move/from16 v0, v25

    move-object/from16 v1, v18

    iput v0, v1, Landroid/view/WindowManager$LayoutParams;->format:I

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/android/server/tv/TvHanlder;->wm:Landroid/view/WindowManager;

    move-object/from16 v25, v0

    if-nez v25, :cond_d

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/android/server/tv/TvHanlder;->mContext:Landroid/content/Context;

    move-object/from16 v25, v0

    const-string v26, "window"

    invoke-virtual/range {v25 .. v26}, Landroid/content/Context;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v25

    check-cast v25, Landroid/view/WindowManager;

    move-object/from16 v0, v25

    move-object/from16 v1, p0

    iput-object v0, v1, Lcom/android/server/tv/TvHanlder;->wm:Landroid/view/WindowManager;

    :cond_d
    sget-boolean v25, Lcom/android/server/tv/TvHanlder;->isAudioModeShowed:Z

    if-nez v25, :cond_e

    const v25, 0x10900ed    # android.R.layout.tv_sound_mode

    const/16 v26, 0x0

    move/from16 v0, v25

    move-object/from16 v1, v26

    invoke-virtual {v13, v0, v1}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;)Landroid/view/View;

    move-result-object v25

    move-object/from16 v0, v25

    move-object/from16 v1, p0

    iput-object v0, v1, Lcom/android/server/tv/TvHanlder;->audioModeView:Landroid/view/View;

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/android/server/tv/TvHanlder;->mTvService:Lcom/mstar/android/tv/ITvService;

    move-object/from16 v25, v0

    invoke-interface/range {v25 .. v25}, Lcom/mstar/android/tv/ITvService;->getTvAudio()Lcom/mstar/android/tv/ITvAudio;

    move-result-object v25

    invoke-interface/range {v25 .. v25}, Lcom/mstar/android/tv/ITvAudio;->getSoundMode()I

    move-result v25

    move/from16 v0, v25

    move-object/from16 v1, p0

    iput v0, v1, Lcom/android/server/tv/TvHanlder;->audioModeIdx:I

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/android/server/tv/TvHanlder;->audioModeView:Landroid/view/View;

    move-object/from16 v25, v0

    const v26, 0x10204c9    # android.R.id.tv_sound_mode_type

    invoke-virtual/range {v25 .. v26}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v19

    check-cast v19, Landroid/widget/TextView;

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/android/server/tv/TvHanlder;->audioModeVals:[Ljava/lang/String;

    move-object/from16 v25, v0

    move-object/from16 v0, p0

    iget v0, v0, Lcom/android/server/tv/TvHanlder;->audioModeIdx:I

    move/from16 v26, v0

    aget-object v25, v25, v26

    move-object/from16 v0, v19

    move-object/from16 v1, v25

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/android/server/tv/TvHanlder;->wm:Landroid/view/WindowManager;

    move-object/from16 v25, v0

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/android/server/tv/TvHanlder;->audioModeView:Landroid/view/View;

    move-object/from16 v26, v0

    move-object/from16 v0, v25

    move-object/from16 v1, v26

    move-object/from16 v2, v18

    invoke-interface {v0, v1, v2}, Landroid/view/WindowManager;->addView(Landroid/view/View;Landroid/view/ViewGroup$LayoutParams;)V

    sget-object v25, Lcom/android/server/tv/TvHanlder$HotKey;->AudioMode:Lcom/android/server/tv/TvHanlder$HotKey;

    move-object/from16 v0, p0

    move-object/from16 v1, v25

    invoke-direct {v0, v1}, Lcom/android/server/tv/TvHanlder;->removeOtherView(Lcom/android/server/tv/TvHanlder$HotKey;)V

    const/16 v25, 0x1

    sput-boolean v25, Lcom/android/server/tv/TvHanlder;->isAudioModeShowed:Z

    const/16 v25, 0xb

    move-object/from16 v0, p0

    move/from16 v1, v25

    invoke-virtual {v0, v1}, Lcom/android/server/tv/TvHanlder;->removeMessages(I)V

    const/16 v25, 0xb

    const-wide/16 v26, 0x1388

    move-object/from16 v0, p0

    move/from16 v1, v25

    move-wide/from16 v2, v26

    invoke-virtual {v0, v1, v2, v3}, Lcom/android/server/tv/TvHanlder;->sendEmptyMessageDelayed(IJ)Z

    goto/16 :goto_0

    :cond_e
    const/16 v25, 0xb

    move-object/from16 v0, p0

    move/from16 v1, v25

    invoke-virtual {v0, v1}, Lcom/android/server/tv/TvHanlder;->removeMessages(I)V

    const/16 v25, 0xb

    const-wide/16 v26, 0x1388

    move-object/from16 v0, p0

    move/from16 v1, v25

    move-wide/from16 v2, v26

    invoke-virtual {v0, v1, v2, v3}, Lcom/android/server/tv/TvHanlder;->sendEmptyMessageDelayed(IJ)Z

    move-object/from16 v0, p0

    iget v0, v0, Lcom/android/server/tv/TvHanlder;->audioModeIdx:I

    move/from16 v25, v0

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/android/server/tv/TvHanlder;->audioModeVals:[Ljava/lang/String;

    move-object/from16 v26, v0

    move-object/from16 v0, v26

    array-length v0, v0

    move/from16 v26, v0

    add-int/lit8 v26, v26, -0x1

    move/from16 v0, v25

    move/from16 v1, v26

    if-ge v0, v1, :cond_f

    move-object/from16 v0, p0

    iget v0, v0, Lcom/android/server/tv/TvHanlder;->audioModeIdx:I

    move/from16 v25, v0

    add-int/lit8 v25, v25, 0x1

    move/from16 v0, v25

    move-object/from16 v1, p0

    iput v0, v1, Lcom/android/server/tv/TvHanlder;->audioModeIdx:I

    :goto_b
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/android/server/tv/TvHanlder;->audioModeView:Landroid/view/View;

    move-object/from16 v25, v0

    const v26, 0x10204c9    # android.R.id.tv_sound_mode_type

    invoke-virtual/range {v25 .. v26}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v19

    check-cast v19, Landroid/widget/TextView;

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/android/server/tv/TvHanlder;->audioModeVals:[Ljava/lang/String;

    move-object/from16 v25, v0

    move-object/from16 v0, p0

    iget v0, v0, Lcom/android/server/tv/TvHanlder;->audioModeIdx:I

    move/from16 v26, v0

    aget-object v25, v25, v26

    move-object/from16 v0, v19

    move-object/from16 v1, v25

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/android/server/tv/TvHanlder;->mTvService:Lcom/mstar/android/tv/ITvService;

    move-object/from16 v25, v0

    invoke-interface/range {v25 .. v25}, Lcom/mstar/android/tv/ITvService;->getTvAudio()Lcom/mstar/android/tv/ITvAudio;

    move-result-object v25

    move-object/from16 v0, p0

    iget v0, v0, Lcom/android/server/tv/TvHanlder;->audioModeIdx:I

    move/from16 v26, v0

    invoke-interface/range {v25 .. v26}, Lcom/mstar/android/tv/ITvAudio;->setSoundMode(I)Z

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/android/server/tv/TvHanlder;->wm:Landroid/view/WindowManager;

    move-object/from16 v25, v0

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/android/server/tv/TvHanlder;->audioModeView:Landroid/view/View;

    move-object/from16 v26, v0

    move-object/from16 v0, v25

    move-object/from16 v1, v26

    move-object/from16 v2, v18

    invoke-interface {v0, v1, v2}, Landroid/view/WindowManager;->updateViewLayout(Landroid/view/View;Landroid/view/ViewGroup$LayoutParams;)V

    goto/16 :goto_0

    :cond_f
    const/16 v25, 0x0

    move/from16 v0, v25

    move-object/from16 v1, p0

    iput v0, v1, Lcom/android/server/tv/TvHanlder;->audioModeIdx:I

    goto :goto_b

    :sswitch_6
    const-string v25, "TvHanlder"

    new-instance v26, Ljava/lang/StringBuilder;

    invoke-direct/range {v26 .. v26}, Ljava/lang/StringBuilder;-><init>()V

    const-string v27, "TvHanlder handler msg:"

    invoke-virtual/range {v26 .. v27}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v26

    move-object/from16 v0, p1

    iget v0, v0, Landroid/os/Message;->what:I

    move/from16 v27, v0

    invoke-virtual/range {v26 .. v27}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v26

    invoke-virtual/range {v26 .. v26}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v26

    invoke-static/range {v25 .. v26}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/android/server/tv/TvHanlder;->mContext:Landroid/content/Context;

    move-object/from16 v25, v0

    invoke-virtual/range {v25 .. v25}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v25

    const v26, 0x107003b    # android.R.array.tv_zoom_mode_types

    invoke-virtual/range {v25 .. v26}, Landroid/content/res/Resources;->getStringArray(I)[Ljava/lang/String;

    move-result-object v20

    move-object/from16 v0, v20

    array-length v5, v0

    const/16 v21, 0x0

    const/4 v14, 0x0

    sget-object v7, Lcom/mstar/android/tvapi/common/vo/TvOsType$EnumInputSource;->E_INPUT_SOURCE_NONE:Lcom/mstar/android/tvapi/common/vo/TvOsType$EnumInputSource;
    :try_end_13
    .catch Landroid/os/RemoteException; {:try_start_13 .. :try_end_13} :catch_1

    :try_start_14
    invoke-static {}, Lcom/mstar/android/tvapi/common/TvManager;->getInstance()Lcom/mstar/android/tvapi/common/TvManager;

    move-result-object v25

    invoke-virtual/range {v25 .. v25}, Lcom/mstar/android/tvapi/common/TvManager;->getCurrentInputSource()Lcom/mstar/android/tvapi/common/vo/TvOsType$EnumInputSource;

    move-result-object v7

    invoke-static {}, Lcom/mstar/android/tvapi/common/TvManager;->getInstance()Lcom/mstar/android/tvapi/common/TvManager;

    move-result-object v25

    invoke-virtual/range {v25 .. v25}, Lcom/mstar/android/tvapi/common/TvManager;->getPlayerManager()Lcom/mstar/android/tvapi/common/TvPlayer;

    move-result-object v25

    invoke-interface/range {v25 .. v25}, Lcom/mstar/android/tvapi/common/TvPlayer;->getVideoInfo()Lcom/mstar/android/tvapi/common/vo/VideoInfo;

    move-result-object v21

    invoke-static {}, Lcom/mstar/android/tvapi/common/TvManager;->getInstance()Lcom/mstar/android/tvapi/common/TvManager;

    move-result-object v25

    invoke-virtual/range {v25 .. v25}, Lcom/mstar/android/tvapi/common/TvManager;->getPlayerManager()Lcom/mstar/android/tvapi/common/TvPlayer;

    move-result-object v25

    invoke-interface/range {v25 .. v25}, Lcom/mstar/android/tvapi/common/TvPlayer;->isHdmiMode()Z

    move-result v14

    invoke-virtual/range {v21 .. v21}, Lcom/mstar/android/tvapi/common/vo/VideoInfo;->getScanType()Lcom/mstar/android/tvapi/common/vo/VideoInfo$EnumScanType;

    move-result-object v25

    sget-object v26, Lcom/mstar/android/tvapi/common/vo/VideoInfo$EnumScanType;->E_INTERLACED:Lcom/mstar/android/tvapi/common/vo/VideoInfo$EnumScanType;

    move-object/from16 v0, v25

    move-object/from16 v1, v26

    if-eq v0, v1, :cond_10

    sget-object v25, Lcom/mstar/android/tvapi/common/vo/TvOsType$EnumInputSource;->E_INPUT_SOURCE_YPBPR:Lcom/mstar/android/tvapi/common/vo/TvOsType$EnumInputSource;

    move-object/from16 v0, v25

    if-eq v7, v0, :cond_10

    sget-object v25, Lcom/mstar/android/tvapi/common/vo/TvOsType$EnumInputSource;->E_INPUT_SOURCE_YPBPR2:Lcom/mstar/android/tvapi/common/vo/TvOsType$EnumInputSource;

    move-object/from16 v0, v25

    if-eq v7, v0, :cond_10

    sget-object v25, Lcom/mstar/android/tvapi/common/vo/TvOsType$EnumInputSource;->E_INPUT_SOURCE_YPBPR3:Lcom/mstar/android/tvapi/common/vo/TvOsType$EnumInputSource;

    move-object/from16 v0, v25

    if-ne v7, v0, :cond_11

    :cond_10
    sget-object v25, Lcom/android/server/tv/DataBaseDesk$MAPI_VIDEO_ARC_Type;->E_AR_DotByDot:Lcom/android/server/tv/DataBaseDesk$MAPI_VIDEO_ARC_Type;

    invoke-virtual/range {v25 .. v25}, Lcom/android/server/tv/DataBaseDesk$MAPI_VIDEO_ARC_Type;->ordinal()I

    move-result v25

    const/16 v26, 0x0

    aput-object v26, v20, v25

    add-int/lit8 v5, v5, -0x1

    :cond_11
    sget-object v25, Lcom/mstar/android/tvapi/common/vo/TvOsType$EnumInputSource;->E_INPUT_SOURCE_VGA:Lcom/mstar/android/tvapi/common/vo/TvOsType$EnumInputSource;

    move-object/from16 v0, v25

    if-eq v7, v0, :cond_13

    invoke-virtual {v7}, Lcom/mstar/android/tvapi/common/vo/TvOsType$EnumInputSource;->ordinal()I

    move-result v25

    sget-object v26, Lcom/mstar/android/tvapi/common/vo/TvOsType$EnumInputSource;->E_INPUT_SOURCE_DVI:Lcom/mstar/android/tvapi/common/vo/TvOsType$EnumInputSource;

    invoke-virtual/range {v26 .. v26}, Lcom/mstar/android/tvapi/common/vo/TvOsType$EnumInputSource;->ordinal()I

    move-result v26

    move/from16 v0, v25

    move/from16 v1, v26

    if-lt v0, v1, :cond_12

    invoke-virtual {v7}, Lcom/mstar/android/tvapi/common/vo/TvOsType$EnumInputSource;->ordinal()I

    move-result v25

    sget-object v26, Lcom/mstar/android/tvapi/common/vo/TvOsType$EnumInputSource;->E_INPUT_SOURCE_DVI_MAX:Lcom/mstar/android/tvapi/common/vo/TvOsType$EnumInputSource;

    invoke-virtual/range {v26 .. v26}, Lcom/mstar/android/tvapi/common/vo/TvOsType$EnumInputSource;->ordinal()I

    move-result v26

    move/from16 v0, v25

    move/from16 v1, v26

    if-lt v0, v1, :cond_13

    :cond_12
    invoke-virtual {v7}, Lcom/mstar/android/tvapi/common/vo/TvOsType$EnumInputSource;->ordinal()I

    move-result v25

    sget-object v26, Lcom/mstar/android/tvapi/common/vo/TvOsType$EnumInputSource;->E_INPUT_SOURCE_HDMI:Lcom/mstar/android/tvapi/common/vo/TvOsType$EnumInputSource;

    invoke-virtual/range {v26 .. v26}, Lcom/mstar/android/tvapi/common/vo/TvOsType$EnumInputSource;->ordinal()I

    move-result v26

    move/from16 v0, v25

    move/from16 v1, v26

    if-lt v0, v1, :cond_14

    invoke-virtual {v7}, Lcom/mstar/android/tvapi/common/vo/TvOsType$EnumInputSource;->ordinal()I

    move-result v25

    sget-object v26, Lcom/mstar/android/tvapi/common/vo/TvOsType$EnumInputSource;->E_INPUT_SOURCE_HDMI_MAX:Lcom/mstar/android/tvapi/common/vo/TvOsType$EnumInputSource;

    invoke-virtual/range {v26 .. v26}, Lcom/mstar/android/tvapi/common/vo/TvOsType$EnumInputSource;->ordinal()I

    move-result v26

    move/from16 v0, v25

    move/from16 v1, v26

    if-ge v0, v1, :cond_14

    if-nez v14, :cond_14

    :cond_13
    sget-object v25, Lcom/android/server/tv/DataBaseDesk$MAPI_VIDEO_ARC_Type;->E_AR_AUTO:Lcom/android/server/tv/DataBaseDesk$MAPI_VIDEO_ARC_Type;

    invoke-virtual/range {v25 .. v25}, Lcom/android/server/tv/DataBaseDesk$MAPI_VIDEO_ARC_Type;->ordinal()I

    move-result v25

    const/16 v26, 0x0

    aput-object v26, v20, v25

    sget-object v25, Lcom/android/server/tv/DataBaseDesk$MAPI_VIDEO_ARC_Type;->E_AR_JustScan:Lcom/android/server/tv/DataBaseDesk$MAPI_VIDEO_ARC_Type;

    invoke-virtual/range {v25 .. v25}, Lcom/android/server/tv/DataBaseDesk$MAPI_VIDEO_ARC_Type;->ordinal()I

    move-result v25

    const/16 v26, 0x0

    aput-object v26, v20, v25

    sget-object v25, Lcom/android/server/tv/DataBaseDesk$MAPI_VIDEO_ARC_Type;->E_AR_Panorama:Lcom/android/server/tv/DataBaseDesk$MAPI_VIDEO_ARC_Type;

    invoke-virtual/range {v25 .. v25}, Lcom/android/server/tv/DataBaseDesk$MAPI_VIDEO_ARC_Type;->ordinal()I

    move-result v25

    const/16 v26, 0x0

    aput-object v26, v20, v25

    sget-object v25, Lcom/android/server/tv/DataBaseDesk$MAPI_VIDEO_ARC_Type;->E_AR_Zoom1:Lcom/android/server/tv/DataBaseDesk$MAPI_VIDEO_ARC_Type;

    invoke-virtual/range {v25 .. v25}, Lcom/android/server/tv/DataBaseDesk$MAPI_VIDEO_ARC_Type;->ordinal()I

    move-result v25

    const/16 v26, 0x0

    aput-object v26, v20, v25

    sget-object v25, Lcom/android/server/tv/DataBaseDesk$MAPI_VIDEO_ARC_Type;->E_AR_Zoom2:Lcom/android/server/tv/DataBaseDesk$MAPI_VIDEO_ARC_Type;

    invoke-virtual/range {v25 .. v25}, Lcom/android/server/tv/DataBaseDesk$MAPI_VIDEO_ARC_Type;->ordinal()I

    move-result v25

    const/16 v26, 0x0

    aput-object v26, v20, v25
    :try_end_14
    .catch Lcom/mstar/android/tvapi/common/exception/TvCommonException; {:try_start_14 .. :try_end_14} :catch_a
    .catch Landroid/os/RemoteException; {:try_start_14 .. :try_end_14} :catch_1

    add-int/lit8 v5, v5, -0x5

    :cond_14
    :goto_c
    :try_start_15
    new-array v0, v5, [Ljava/lang/String;

    move-object/from16 v25, v0

    move-object/from16 v0, v25

    move-object/from16 v1, p0

    iput-object v0, v1, Lcom/android/server/tv/TvHanlder;->zoomVals:[Ljava/lang/String;

    move-object/from16 v0, v20

    array-length v0, v0

    move/from16 v25, v0

    add-int/lit8 v12, v25, -0x1

    :goto_d
    if-ltz v12, :cond_16

    aget-object v25, v20, v12

    if-eqz v25, :cond_15

    if-lez v5, :cond_15

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/android/server/tv/TvHanlder;->zoomVals:[Ljava/lang/String;

    move-object/from16 v25, v0

    add-int/lit8 v5, v5, -0x1

    aget-object v26, v20, v12

    aput-object v26, v25, v5

    :cond_15
    add-int/lit8 v12, v12, -0x1

    goto :goto_d

    :catch_a
    move-exception v8

    invoke-virtual {v8}, Lcom/mstar/android/tvapi/common/exception/TvCommonException;->printStackTrace()V

    goto :goto_c

    :cond_16
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/android/server/tv/TvHanlder;->mContext:Landroid/content/Context;

    move-object/from16 v25, v0

    const-string v26, "layout_inflater"

    invoke-virtual/range {v25 .. v26}, Landroid/content/Context;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v13

    check-cast v13, Landroid/view/LayoutInflater;

    new-instance v18, Landroid/view/WindowManager$LayoutParams;

    const/16 v25, 0x1f4

    const/16 v26, 0xc8

    move-object/from16 v0, v18

    move/from16 v1, v25

    move/from16 v2, v26

    invoke-direct {v0, v1, v2}, Landroid/view/WindowManager$LayoutParams;-><init>(II)V

    const/16 v25, 0x7d6

    move/from16 v0, v25

    move-object/from16 v1, v18

    iput v0, v1, Landroid/view/WindowManager$LayoutParams;->type:I

    const/16 v25, 0x1

    move/from16 v0, v25

    move-object/from16 v1, v18

    iput v0, v1, Landroid/view/WindowManager$LayoutParams;->format:I

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/android/server/tv/TvHanlder;->wm:Landroid/view/WindowManager;

    move-object/from16 v25, v0

    if-nez v25, :cond_17

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/android/server/tv/TvHanlder;->mContext:Landroid/content/Context;

    move-object/from16 v25, v0

    const-string v26, "window"

    invoke-virtual/range {v25 .. v26}, Landroid/content/Context;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v25

    check-cast v25, Landroid/view/WindowManager;

    move-object/from16 v0, v25

    move-object/from16 v1, p0

    iput-object v0, v1, Lcom/android/server/tv/TvHanlder;->wm:Landroid/view/WindowManager;

    :cond_17
    sget-boolean v25, Lcom/android/server/tv/TvHanlder;->isZoomShowed:Z

    if-nez v25, :cond_19

    const v25, 0x10900ee    # android.R.layout.tv_zoom_mode

    const/16 v26, 0x0

    move/from16 v0, v25

    move-object/from16 v1, v26

    invoke-virtual {v13, v0, v1}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;)Landroid/view/View;

    move-result-object v25

    move-object/from16 v0, v25

    move-object/from16 v1, p0

    iput-object v0, v1, Lcom/android/server/tv/TvHanlder;->zoomView:Landroid/view/View;

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/android/server/tv/TvHanlder;->mTvService:Lcom/mstar/android/tv/ITvService;

    move-object/from16 v25, v0

    invoke-interface/range {v25 .. v25}, Lcom/mstar/android/tv/ITvService;->getTvPicture()Lcom/mstar/android/tv/ITvPicture;

    move-result-object v25

    invoke-interface/range {v25 .. v25}, Lcom/mstar/android/tv/ITvPicture;->getVideoArc()I

    move-result v25

    move/from16 v0, v25

    move-object/from16 v1, p0

    iput v0, v1, Lcom/android/server/tv/TvHanlder;->zoomModeIdx:I

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/android/server/tv/TvHanlder;->zoomView:Landroid/view/View;

    move-object/from16 v25, v0

    const v26, 0x10204ca    # android.R.id.tv_zoom_mode_type

    invoke-virtual/range {v25 .. v26}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v19

    check-cast v19, Landroid/widget/TextView;

    move-object/from16 v0, p0

    iget v0, v0, Lcom/android/server/tv/TvHanlder;->zoomModeIdx:I

    move/from16 v25, v0

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/android/server/tv/TvHanlder;->zoomVals:[Ljava/lang/String;

    move-object/from16 v26, v0

    move-object/from16 v0, v26

    array-length v0, v0

    move/from16 v26, v0

    move/from16 v0, v25

    move/from16 v1, v26

    if-lt v0, v1, :cond_18

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/android/server/tv/TvHanlder;->zoomVals:[Ljava/lang/String;

    move-object/from16 v25, v0

    move-object/from16 v0, v25

    array-length v0, v0

    move/from16 v25, v0

    add-int/lit8 v25, v25, -0x1

    move/from16 v0, v25

    move-object/from16 v1, p0

    iput v0, v1, Lcom/android/server/tv/TvHanlder;->zoomModeIdx:I

    :cond_18
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/android/server/tv/TvHanlder;->zoomVals:[Ljava/lang/String;

    move-object/from16 v25, v0

    move-object/from16 v0, p0

    iget v0, v0, Lcom/android/server/tv/TvHanlder;->zoomModeIdx:I

    move/from16 v26, v0

    aget-object v25, v25, v26

    move-object/from16 v0, v19

    move-object/from16 v1, v25

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/android/server/tv/TvHanlder;->wm:Landroid/view/WindowManager;

    move-object/from16 v25, v0

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/android/server/tv/TvHanlder;->zoomView:Landroid/view/View;

    move-object/from16 v26, v0

    move-object/from16 v0, v25

    move-object/from16 v1, v26

    move-object/from16 v2, v18

    invoke-interface {v0, v1, v2}, Landroid/view/WindowManager;->addView(Landroid/view/View;Landroid/view/ViewGroup$LayoutParams;)V

    sget-object v25, Lcom/android/server/tv/TvHanlder$HotKey;->Zoom:Lcom/android/server/tv/TvHanlder$HotKey;

    move-object/from16 v0, p0

    move-object/from16 v1, v25

    invoke-direct {v0, v1}, Lcom/android/server/tv/TvHanlder;->removeOtherView(Lcom/android/server/tv/TvHanlder$HotKey;)V

    const/16 v25, 0x1

    sput-boolean v25, Lcom/android/server/tv/TvHanlder;->isZoomShowed:Z

    const/16 v25, 0xb

    move-object/from16 v0, p0

    move/from16 v1, v25

    invoke-virtual {v0, v1}, Lcom/android/server/tv/TvHanlder;->removeMessages(I)V

    const/16 v25, 0xb

    const-wide/16 v26, 0x1388

    move-object/from16 v0, p0

    move/from16 v1, v25

    move-wide/from16 v2, v26

    invoke-virtual {v0, v1, v2, v3}, Lcom/android/server/tv/TvHanlder;->sendEmptyMessageDelayed(IJ)Z

    goto/16 :goto_0

    :cond_19
    const/16 v25, 0xb

    move-object/from16 v0, p0

    move/from16 v1, v25

    invoke-virtual {v0, v1}, Lcom/android/server/tv/TvHanlder;->removeMessages(I)V

    const/16 v25, 0xb

    const-wide/16 v26, 0x1388

    move-object/from16 v0, p0

    move/from16 v1, v25

    move-wide/from16 v2, v26

    invoke-virtual {v0, v1, v2, v3}, Lcom/android/server/tv/TvHanlder;->sendEmptyMessageDelayed(IJ)Z

    move-object/from16 v0, p0

    iget v0, v0, Lcom/android/server/tv/TvHanlder;->zoomModeIdx:I

    move/from16 v25, v0

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/android/server/tv/TvHanlder;->zoomVals:[Ljava/lang/String;

    move-object/from16 v26, v0

    move-object/from16 v0, v26

    array-length v0, v0

    move/from16 v26, v0

    add-int/lit8 v26, v26, -0x1

    move/from16 v0, v25

    move/from16 v1, v26

    if-ge v0, v1, :cond_1a

    move-object/from16 v0, p0

    iget v0, v0, Lcom/android/server/tv/TvHanlder;->zoomModeIdx:I

    move/from16 v25, v0

    add-int/lit8 v25, v25, 0x1

    move/from16 v0, v25

    move-object/from16 v1, p0

    iput v0, v1, Lcom/android/server/tv/TvHanlder;->zoomModeIdx:I

    :goto_e
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/android/server/tv/TvHanlder;->zoomView:Landroid/view/View;

    move-object/from16 v25, v0

    const v26, 0x10204ca    # android.R.id.tv_zoom_mode_type

    invoke-virtual/range {v25 .. v26}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v19

    check-cast v19, Landroid/widget/TextView;

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/android/server/tv/TvHanlder;->zoomVals:[Ljava/lang/String;

    move-object/from16 v25, v0

    move-object/from16 v0, p0

    iget v0, v0, Lcom/android/server/tv/TvHanlder;->zoomModeIdx:I

    move/from16 v26, v0

    aget-object v25, v25, v26

    move-object/from16 v0, v19

    move-object/from16 v1, v25

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/android/server/tv/TvHanlder;->mTvService:Lcom/mstar/android/tv/ITvService;

    move-object/from16 v25, v0

    invoke-interface/range {v25 .. v25}, Lcom/mstar/android/tv/ITvService;->getTvPicture()Lcom/mstar/android/tv/ITvPicture;

    move-result-object v25

    move-object/from16 v0, p0

    iget v0, v0, Lcom/android/server/tv/TvHanlder;->zoomModeIdx:I

    move/from16 v26, v0

    invoke-interface/range {v25 .. v26}, Lcom/mstar/android/tv/ITvPicture;->setVideoArc(I)Z

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/android/server/tv/TvHanlder;->wm:Landroid/view/WindowManager;

    move-object/from16 v25, v0

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/android/server/tv/TvHanlder;->zoomView:Landroid/view/View;

    move-object/from16 v26, v0

    move-object/from16 v0, v25

    move-object/from16 v1, v26

    move-object/from16 v2, v18

    invoke-interface {v0, v1, v2}, Landroid/view/WindowManager;->updateViewLayout(Landroid/view/View;Landroid/view/ViewGroup$LayoutParams;)V

    goto/16 :goto_0

    :cond_1a
    const/16 v25, 0x0

    move/from16 v0, v25

    move-object/from16 v1, p0

    iput v0, v1, Lcom/android/server/tv/TvHanlder;->zoomModeIdx:I

    goto :goto_e

    :sswitch_7
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/android/server/tv/TvHanlder;->mContext:Landroid/content/Context;

    move-object/from16 v25, v0

    invoke-virtual/range {v25 .. v25}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v25

    const v26, 0x107003c    # android.R.array.tv_picture_mode_types

    invoke-virtual/range {v25 .. v26}, Landroid/content/res/Resources;->getStringArray(I)[Ljava/lang/String;

    move-result-object v25

    move-object/from16 v0, v25

    move-object/from16 v1, p0

    iput-object v0, v1, Lcom/android/server/tv/TvHanlder;->picModeVals:[Ljava/lang/String;

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/android/server/tv/TvHanlder;->mContext:Landroid/content/Context;

    move-object/from16 v25, v0

    const-string v26, "layout_inflater"

    invoke-virtual/range {v25 .. v26}, Landroid/content/Context;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v13

    check-cast v13, Landroid/view/LayoutInflater;

    new-instance v18, Landroid/view/WindowManager$LayoutParams;

    const/16 v25, 0x1f4

    const/16 v26, 0xc8

    move-object/from16 v0, v18

    move/from16 v1, v25

    move/from16 v2, v26

    invoke-direct {v0, v1, v2}, Landroid/view/WindowManager$LayoutParams;-><init>(II)V

    const/16 v25, 0x7d6

    move/from16 v0, v25

    move-object/from16 v1, v18

    iput v0, v1, Landroid/view/WindowManager$LayoutParams;->type:I

    const/16 v25, 0x1

    move/from16 v0, v25

    move-object/from16 v1, v18

    iput v0, v1, Landroid/view/WindowManager$LayoutParams;->format:I

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/android/server/tv/TvHanlder;->wm:Landroid/view/WindowManager;

    move-object/from16 v25, v0

    if-nez v25, :cond_1b

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/android/server/tv/TvHanlder;->mContext:Landroid/content/Context;

    move-object/from16 v25, v0

    const-string v26, "window"

    invoke-virtual/range {v25 .. v26}, Landroid/content/Context;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v25

    check-cast v25, Landroid/view/WindowManager;

    move-object/from16 v0, v25

    move-object/from16 v1, p0

    iput-object v0, v1, Lcom/android/server/tv/TvHanlder;->wm:Landroid/view/WindowManager;

    :cond_1b
    sget-boolean v25, Lcom/android/server/tv/TvHanlder;->isPicModeShowed:Z

    if-nez v25, :cond_1c

    const v25, 0x10900eb    # android.R.layout.tv_picture_mode

    const/16 v26, 0x0

    move/from16 v0, v25

    move-object/from16 v1, v26

    invoke-virtual {v13, v0, v1}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;)Landroid/view/View;

    move-result-object v25

    move-object/from16 v0, v25

    move-object/from16 v1, p0

    iput-object v0, v1, Lcom/android/server/tv/TvHanlder;->picModeView:Landroid/view/View;

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/android/server/tv/TvHanlder;->mTvService:Lcom/mstar/android/tv/ITvService;

    move-object/from16 v25, v0

    invoke-interface/range {v25 .. v25}, Lcom/mstar/android/tv/ITvService;->getTvPicture()Lcom/mstar/android/tv/ITvPicture;

    move-result-object v25

    invoke-interface/range {v25 .. v25}, Lcom/mstar/android/tv/ITvPicture;->getPictureModeIdx()I

    move-result v25

    move/from16 v0, v25

    move-object/from16 v1, p0

    iput v0, v1, Lcom/android/server/tv/TvHanlder;->picModeIdx:I

    const-string v25, "TvVIewHandler"

    new-instance v26, Ljava/lang/StringBuilder;

    invoke-direct/range {v26 .. v26}, Ljava/lang/StringBuilder;-><init>()V

    const-string v27, "@@@@@@_________picMode is "

    invoke-virtual/range {v26 .. v27}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v26

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/android/server/tv/TvHanlder;->mTvService:Lcom/mstar/android/tv/ITvService;

    move-object/from16 v27, v0

    invoke-interface/range {v27 .. v27}, Lcom/mstar/android/tv/ITvService;->getTvPicture()Lcom/mstar/android/tv/ITvPicture;

    move-result-object v27

    invoke-interface/range {v27 .. v27}, Lcom/mstar/android/tv/ITvPicture;->getPictureModeIdx()I

    move-result v27

    invoke-virtual/range {v26 .. v27}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v26

    invoke-virtual/range {v26 .. v26}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v26

    invoke-static/range {v25 .. v26}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    const-string v25, "TvVIewHandler"

    new-instance v26, Ljava/lang/StringBuilder;

    invoke-direct/range {v26 .. v26}, Ljava/lang/StringBuilder;-><init>()V

    const-string v27, "@@@@@@_________picModeIdx is "

    invoke-virtual/range {v26 .. v27}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v26

    move-object/from16 v0, p0

    iget v0, v0, Lcom/android/server/tv/TvHanlder;->picModeIdx:I

    move/from16 v27, v0

    invoke-virtual/range {v26 .. v27}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v26

    invoke-virtual/range {v26 .. v26}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v26

    invoke-static/range {v25 .. v26}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/android/server/tv/TvHanlder;->picModeView:Landroid/view/View;

    move-object/from16 v25, v0

    const v26, 0x10204c7    # android.R.id.tv_picture_mode_type

    invoke-virtual/range {v25 .. v26}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v19

    check-cast v19, Landroid/widget/TextView;

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/android/server/tv/TvHanlder;->picModeVals:[Ljava/lang/String;

    move-object/from16 v25, v0

    move-object/from16 v0, p0

    iget v0, v0, Lcom/android/server/tv/TvHanlder;->picModeIdx:I

    move/from16 v26, v0

    aget-object v25, v25, v26

    move-object/from16 v0, v19

    move-object/from16 v1, v25

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/android/server/tv/TvHanlder;->wm:Landroid/view/WindowManager;

    move-object/from16 v25, v0

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/android/server/tv/TvHanlder;->picModeView:Landroid/view/View;

    move-object/from16 v26, v0

    move-object/from16 v0, v25

    move-object/from16 v1, v26

    move-object/from16 v2, v18

    invoke-interface {v0, v1, v2}, Landroid/view/WindowManager;->addView(Landroid/view/View;Landroid/view/ViewGroup$LayoutParams;)V

    sget-object v25, Lcom/android/server/tv/TvHanlder$HotKey;->PicMode:Lcom/android/server/tv/TvHanlder$HotKey;

    move-object/from16 v0, p0

    move-object/from16 v1, v25

    invoke-direct {v0, v1}, Lcom/android/server/tv/TvHanlder;->removeOtherView(Lcom/android/server/tv/TvHanlder$HotKey;)V

    const/16 v25, 0x1

    sput-boolean v25, Lcom/android/server/tv/TvHanlder;->isPicModeShowed:Z

    const/16 v25, 0xb

    move-object/from16 v0, p0

    move/from16 v1, v25

    invoke-virtual {v0, v1}, Lcom/android/server/tv/TvHanlder;->removeMessages(I)V

    const/16 v25, 0xb

    const-wide/16 v26, 0x1388

    move-object/from16 v0, p0

    move/from16 v1, v25

    move-wide/from16 v2, v26

    invoke-virtual {v0, v1, v2, v3}, Lcom/android/server/tv/TvHanlder;->sendEmptyMessageDelayed(IJ)Z

    goto/16 :goto_0

    :cond_1c
    const/16 v25, 0xb

    move-object/from16 v0, p0

    move/from16 v1, v25

    invoke-virtual {v0, v1}, Lcom/android/server/tv/TvHanlder;->removeMessages(I)V

    const/16 v25, 0xb

    const-wide/16 v26, 0x1388

    move-object/from16 v0, p0

    move/from16 v1, v25

    move-wide/from16 v2, v26

    invoke-virtual {v0, v1, v2, v3}, Lcom/android/server/tv/TvHanlder;->sendEmptyMessageDelayed(IJ)Z

    move-object/from16 v0, p0

    iget v0, v0, Lcom/android/server/tv/TvHanlder;->picModeIdx:I

    move/from16 v25, v0

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/android/server/tv/TvHanlder;->picModeVals:[Ljava/lang/String;

    move-object/from16 v26, v0

    move-object/from16 v0, v26

    array-length v0, v0

    move/from16 v26, v0

    add-int/lit8 v26, v26, -0x1

    move/from16 v0, v25

    move/from16 v1, v26

    if-ge v0, v1, :cond_1d

    move-object/from16 v0, p0

    iget v0, v0, Lcom/android/server/tv/TvHanlder;->picModeIdx:I

    move/from16 v25, v0

    add-int/lit8 v25, v25, 0x1

    move/from16 v0, v25

    move-object/from16 v1, p0

    iput v0, v1, Lcom/android/server/tv/TvHanlder;->picModeIdx:I

    :goto_f
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/android/server/tv/TvHanlder;->picModeView:Landroid/view/View;

    move-object/from16 v25, v0

    const v26, 0x10204c7    # android.R.id.tv_picture_mode_type

    invoke-virtual/range {v25 .. v26}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v19

    check-cast v19, Landroid/widget/TextView;

    const-string v25, "TvVIewHandler"

    new-instance v26, Ljava/lang/StringBuilder;

    invoke-direct/range {v26 .. v26}, Ljava/lang/StringBuilder;-><init>()V

    const-string v27, "@@@@@@~~~~~~~~~~~_picModeIdx is "

    invoke-virtual/range {v26 .. v27}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v26

    move-object/from16 v0, p0

    iget v0, v0, Lcom/android/server/tv/TvHanlder;->picModeIdx:I

    move/from16 v27, v0

    invoke-virtual/range {v26 .. v27}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v26

    invoke-virtual/range {v26 .. v26}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v26

    invoke-static/range {v25 .. v26}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/android/server/tv/TvHanlder;->picModeVals:[Ljava/lang/String;

    move-object/from16 v25, v0

    move-object/from16 v0, p0

    iget v0, v0, Lcom/android/server/tv/TvHanlder;->picModeIdx:I

    move/from16 v26, v0

    aget-object v25, v25, v26

    move-object/from16 v0, v19

    move-object/from16 v1, v25

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/android/server/tv/TvHanlder;->mTvService:Lcom/mstar/android/tv/ITvService;

    move-object/from16 v25, v0

    invoke-interface/range {v25 .. v25}, Lcom/mstar/android/tv/ITvService;->getTvPicture()Lcom/mstar/android/tv/ITvPicture;

    move-result-object v25

    move-object/from16 v0, p0

    iget v0, v0, Lcom/android/server/tv/TvHanlder;->picModeIdx:I

    move/from16 v26, v0

    invoke-interface/range {v25 .. v26}, Lcom/mstar/android/tv/ITvPicture;->setPictureModeIdx(I)Z

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/android/server/tv/TvHanlder;->wm:Landroid/view/WindowManager;

    move-object/from16 v25, v0

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/android/server/tv/TvHanlder;->picModeView:Landroid/view/View;

    move-object/from16 v26, v0

    move-object/from16 v0, v25

    move-object/from16 v1, v26

    move-object/from16 v2, v18

    invoke-interface {v0, v1, v2}, Landroid/view/WindowManager;->updateViewLayout(Landroid/view/View;Landroid/view/ViewGroup$LayoutParams;)V

    goto/16 :goto_0

    :cond_1d
    const/16 v25, 0x0

    move/from16 v0, v25

    move-object/from16 v1, p0

    iput v0, v1, Lcom/android/server/tv/TvHanlder;->picModeIdx:I

    goto :goto_f

    :sswitch_8
    invoke-virtual/range {p1 .. p1}, Landroid/os/Message;->getData()Landroid/os/Bundle;

    move-result-object v6

    const-string v25, "x"

    const/16 v26, 0x0

    move-object/from16 v0, v25

    move/from16 v1, v26

    invoke-virtual {v6, v0, v1}, Landroid/os/Bundle;->getInt(Ljava/lang/String;I)I

    move-result v25

    const-string v26, "y"

    const/16 v27, 0x0

    move-object/from16 v0, v26

    move/from16 v1, v27

    invoke-virtual {v6, v0, v1}, Landroid/os/Bundle;->getInt(Ljava/lang/String;I)I

    move-result v26

    const-string v27, "width"

    const/16 v28, 0x1f4

    move-object/from16 v0, v27

    move/from16 v1, v28

    invoke-virtual {v6, v0, v1}, Landroid/os/Bundle;->getInt(Ljava/lang/String;I)I

    move-result v27

    const-string v28, "height"

    const/16 v29, 0x1f4

    move-object/from16 v0, v28

    move/from16 v1, v29

    invoke-virtual {v6, v0, v1}, Landroid/os/Bundle;->getInt(Ljava/lang/String;I)I

    move-result v28

    move-object/from16 v0, p0

    move/from16 v1, v25

    move/from16 v2, v26

    move/from16 v3, v27

    move/from16 v4, v28

    invoke-direct {v0, v1, v2, v3, v4}, Lcom/android/server/tv/TvHanlder;->openSurfaceView(IIII)V

    goto/16 :goto_0

    :sswitch_9
    invoke-virtual/range {p1 .. p1}, Landroid/os/Message;->getData()Landroid/os/Bundle;

    move-result-object v6

    const-string v25, "x"

    const/16 v26, 0x0

    move-object/from16 v0, v25

    move/from16 v1, v26

    invoke-virtual {v6, v0, v1}, Landroid/os/Bundle;->getInt(Ljava/lang/String;I)I

    move-result v25

    const-string v26, "y"

    const/16 v27, 0x0

    move-object/from16 v0, v26

    move/from16 v1, v27

    invoke-virtual {v6, v0, v1}, Landroid/os/Bundle;->getInt(Ljava/lang/String;I)I

    move-result v26

    const-string v27, "width"

    const/16 v28, 0x1f4

    move-object/from16 v0, v27

    move/from16 v1, v28

    invoke-virtual {v6, v0, v1}, Landroid/os/Bundle;->getInt(Ljava/lang/String;I)I

    move-result v27

    const-string v28, "height"

    const/16 v29, 0x1f4

    move-object/from16 v0, v28

    move/from16 v1, v29

    invoke-virtual {v6, v0, v1}, Landroid/os/Bundle;->getInt(Ljava/lang/String;I)I

    move-result v28

    move-object/from16 v0, p0

    move/from16 v1, v25

    move/from16 v2, v26

    move/from16 v3, v27

    move/from16 v4, v28

    invoke-direct {v0, v1, v2, v3, v4}, Lcom/android/server/tv/TvHanlder;->setSurfaceView(IIII)V

    goto/16 :goto_0

    :sswitch_a
    invoke-direct/range {p0 .. p0}, Lcom/android/server/tv/TvHanlder;->closeSurfaceView()V

    goto/16 :goto_0

    :sswitch_b
    new-instance v17, Landroid/view/WindowManager$LayoutParams;

    invoke-direct/range {v17 .. v17}, Landroid/view/WindowManager$LayoutParams;-><init>()V

    const/16 v25, 0x7d8

    move/from16 v0, v25

    move-object/from16 v1, v17

    iput v0, v1, Landroid/view/WindowManager$LayoutParams;->type:I

    const/16 v25, 0x8

    move/from16 v0, v25

    move-object/from16 v1, v17

    iput v0, v1, Landroid/view/WindowManager$LayoutParams;->flags:I

    const/16 v25, -0x2

    move/from16 v0, v25

    move-object/from16 v1, v17

    iput v0, v1, Landroid/view/WindowManager$LayoutParams;->width:I

    const/16 v25, -0x2

    move/from16 v0, v25

    move-object/from16 v1, v17

    iput v0, v1, Landroid/view/WindowManager$LayoutParams;->height:I

    const/16 v25, 0x1

    move/from16 v0, v25

    move-object/from16 v1, v17

    iput v0, v1, Landroid/view/WindowManager$LayoutParams;->format:I

    const/16 v25, 0x53

    move/from16 v0, v25

    move-object/from16 v1, v17

    iput v0, v1, Landroid/view/WindowManager$LayoutParams;->gravity:I

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/android/server/tv/TvHanlder;->muteView:Landroid/view/View;

    move-object/from16 v25, v0

    if-nez v25, :cond_1e

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/android/server/tv/TvHanlder;->mContext:Landroid/content/Context;

    move-object/from16 v25, v0

    const-string v26, "layout_inflater"

    invoke-virtual/range {v25 .. v26}, Landroid/content/Context;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v16

    check-cast v16, Landroid/view/LayoutInflater;

    const v25, 0x1090092    # android.R.layout.mute

    const/16 v26, 0x0

    move-object/from16 v0, v16

    move/from16 v1, v25

    move-object/from16 v2, v26

    invoke-virtual {v0, v1, v2}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;)Landroid/view/View;

    move-result-object v25

    move-object/from16 v0, v25

    move-object/from16 v1, p0

    iput-object v0, v1, Lcom/android/server/tv/TvHanlder;->muteView:Landroid/view/View;
    :try_end_15
    .catch Landroid/os/RemoteException; {:try_start_15 .. :try_end_15} :catch_1

    :cond_1e
    :try_start_16
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/android/server/tv/TvHanlder;->wm:Landroid/view/WindowManager;

    move-object/from16 v25, v0

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/android/server/tv/TvHanlder;->muteView:Landroid/view/View;

    move-object/from16 v26, v0

    invoke-interface/range {v25 .. v26}, Landroid/view/WindowManager;->removeView(Landroid/view/View;)V
    :try_end_16
    .catch Ljava/lang/Exception; {:try_start_16 .. :try_end_16} :catch_b
    .catch Landroid/os/RemoteException; {:try_start_16 .. :try_end_16} :catch_1

    :goto_10
    :try_start_17
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/android/server/tv/TvHanlder;->wm:Landroid/view/WindowManager;

    move-object/from16 v25, v0

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/android/server/tv/TvHanlder;->muteView:Landroid/view/View;

    move-object/from16 v26, v0

    move-object/from16 v0, v25

    move-object/from16 v1, v26

    move-object/from16 v2, v17

    invoke-interface {v0, v1, v2}, Landroid/view/WindowManager;->addView(Landroid/view/View;Landroid/view/ViewGroup$LayoutParams;)V

    const/16 v25, 0x1

    sput-boolean v25, Lcom/android/server/tv/TvHanlder;->isMuteShowed:Z

    goto/16 :goto_0

    :catch_b
    move-exception v8

    const-string v25, "hotkey"

    const-string v26, "remove muteView fail"

    invoke-static/range {v25 .. v26}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I
    :try_end_17
    .catch Landroid/os/RemoteException; {:try_start_17 .. :try_end_17} :catch_1

    goto :goto_10

    :sswitch_c
    :try_start_18
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/android/server/tv/TvHanlder;->wm:Landroid/view/WindowManager;

    move-object/from16 v25, v0

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/android/server/tv/TvHanlder;->muteView:Landroid/view/View;

    move-object/from16 v26, v0

    invoke-interface/range {v25 .. v26}, Landroid/view/WindowManager;->removeView(Landroid/view/View;)V

    const/16 v25, 0x0

    sput-boolean v25, Lcom/android/server/tv/TvHanlder;->isMuteShowed:Z
    :try_end_18
    .catch Ljava/lang/Exception; {:try_start_18 .. :try_end_18} :catch_c
    .catch Landroid/os/RemoteException; {:try_start_18 .. :try_end_18} :catch_1

    goto/16 :goto_0

    :catch_c
    move-exception v8

    :try_start_19
    const-string v25, "hotkey"

    const-string v26, "remove fail"

    invoke-static/range {v25 .. v26}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    goto/16 :goto_0

    :sswitch_d
    new-instance v10, Landroid/view/WindowManager$LayoutParams;

    invoke-direct {v10}, Landroid/view/WindowManager$LayoutParams;-><init>()V

    const/16 v25, 0x7d6

    move/from16 v0, v25

    iput v0, v10, Landroid/view/WindowManager$LayoutParams;->type:I

    const/16 v25, -0x2

    move/from16 v0, v25

    iput v0, v10, Landroid/view/WindowManager$LayoutParams;->width:I

    const/16 v25, -0x2

    move/from16 v0, v25

    iput v0, v10, Landroid/view/WindowManager$LayoutParams;->height:I

    const/16 v25, 0x1

    move/from16 v0, v25

    iput v0, v10, Landroid/view/WindowManager$LayoutParams;->format:I

    const/16 v25, 0x53

    move/from16 v0, v25

    iput v0, v10, Landroid/view/WindowManager$LayoutParams;->gravity:I

    const/16 v25, 0x78

    move/from16 v0, v25

    iput v0, v10, Landroid/view/WindowManager$LayoutParams;->y:I

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/android/server/tv/TvHanlder;->freezeView:Landroid/view/View;

    move-object/from16 v25, v0

    if-nez v25, :cond_1f

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/android/server/tv/TvHanlder;->mContext:Landroid/content/Context;

    move-object/from16 v25, v0

    const-string v26, "layout_inflater"

    invoke-virtual/range {v25 .. v26}, Landroid/content/Context;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v9

    check-cast v9, Landroid/view/LayoutInflater;

    const v25, 0x1090044    # android.R.layout.freeze

    const/16 v26, 0x0

    move/from16 v0, v25

    move-object/from16 v1, v26

    invoke-virtual {v9, v0, v1}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;)Landroid/view/View;

    move-result-object v25

    move-object/from16 v0, v25

    move-object/from16 v1, p0

    iput-object v0, v1, Lcom/android/server/tv/TvHanlder;->freezeView:Landroid/view/View;
    :try_end_19
    .catch Landroid/os/RemoteException; {:try_start_19 .. :try_end_19} :catch_1

    :cond_1f
    :try_start_1a
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/android/server/tv/TvHanlder;->wm:Landroid/view/WindowManager;

    move-object/from16 v25, v0

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/android/server/tv/TvHanlder;->freezeView:Landroid/view/View;

    move-object/from16 v26, v0

    invoke-interface/range {v25 .. v26}, Landroid/view/WindowManager;->removeView(Landroid/view/View;)V
    :try_end_1a
    .catch Ljava/lang/Exception; {:try_start_1a .. :try_end_1a} :catch_d
    .catch Landroid/os/RemoteException; {:try_start_1a .. :try_end_1a} :catch_1

    :goto_11
    :try_start_1b
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/android/server/tv/TvHanlder;->wm:Landroid/view/WindowManager;

    move-object/from16 v25, v0

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/android/server/tv/TvHanlder;->freezeView:Landroid/view/View;

    move-object/from16 v26, v0

    move-object/from16 v0, v25

    move-object/from16 v1, v26

    invoke-interface {v0, v1, v10}, Landroid/view/WindowManager;->addView(Landroid/view/View;Landroid/view/ViewGroup$LayoutParams;)V

    const/16 v25, 0x1

    sput-boolean v25, Lcom/android/server/tv/TvHanlder;->isFreezeShowed:Z

    goto/16 :goto_0

    :catch_d
    move-exception v8

    const-string v25, "hotkey"

    const-string v26, "remove freezeView fail"

    invoke-static/range {v25 .. v26}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I
    :try_end_1b
    .catch Landroid/os/RemoteException; {:try_start_1b .. :try_end_1b} :catch_1

    goto :goto_11

    :sswitch_e
    :try_start_1c
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/android/server/tv/TvHanlder;->wm:Landroid/view/WindowManager;

    move-object/from16 v25, v0

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/android/server/tv/TvHanlder;->freezeView:Landroid/view/View;

    move-object/from16 v26, v0

    invoke-interface/range {v25 .. v26}, Landroid/view/WindowManager;->removeView(Landroid/view/View;)V

    const/16 v25, 0x0

    sput-boolean v25, Lcom/android/server/tv/TvHanlder;->isFreezeShowed:Z
    :try_end_1c
    .catch Ljava/lang/Exception; {:try_start_1c .. :try_end_1c} :catch_e
    .catch Landroid/os/RemoteException; {:try_start_1c .. :try_end_1c} :catch_1

    goto/16 :goto_0

    :catch_e
    move-exception v8

    :try_start_1d
    const-string v25, "hotkey"

    const-string v26, "remove freezeView fail"

    invoke-static/range {v25 .. v26}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I
    :try_end_1d
    .catch Landroid/os/RemoteException; {:try_start_1d .. :try_end_1d} :catch_1

    goto/16 :goto_0

    :sswitch_data_0
    .sparse-switch
        0x1 -> :sswitch_2
        0x2 -> :sswitch_3
        0x4 -> :sswitch_8
        0x5 -> :sswitch_a
        0x6 -> :sswitch_9
        0x7 -> :sswitch_b
        0x8 -> :sswitch_c
        0x9 -> :sswitch_d
        0xa -> :sswitch_e
        0xb -> :sswitch_0
        0xc -> :sswitch_1
        0x14d -> :sswitch_7
        0x29a -> :sswitch_6
        0x309 -> :sswitch_5
        0x378 -> :sswitch_4
    .end sparse-switch

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
        :pswitch_1
        :pswitch_2
    .end packed-switch
.end method

.method public setPIPSurfacePara(IIII)V
    .locals 0
    .param p1    # I
    .param p2    # I
    .param p3    # I
    .param p4    # I

    iput p1, p0, Lcom/android/server/tv/TvHanlder;->PIP_xPos:I

    iput p2, p0, Lcom/android/server/tv/TvHanlder;->PIP_yPos:I

    iput p3, p0, Lcom/android/server/tv/TvHanlder;->PIP_width:I

    iput p4, p0, Lcom/android/server/tv/TvHanlder;->PIP_height:I

    return-void
.end method

.method public shutdown()V
    .locals 5

    const/4 v4, 0x0

    const/4 v3, 0x0

    sget-boolean v1, Lcom/android/server/tv/TvHanlder;->isVolShowed:Z

    if-eqz v1, :cond_0

    iget-object v1, p0, Lcom/android/server/tv/TvHanlder;->volView:Landroid/view/View;

    if-eqz v1, :cond_0

    :try_start_0
    iget-object v1, p0, Lcom/android/server/tv/TvHanlder;->wm:Landroid/view/WindowManager;

    iget-object v2, p0, Lcom/android/server/tv/TvHanlder;->volView:Landroid/view/View;

    invoke-interface {v1, v2}, Landroid/view/WindowManager;->removeView(Landroid/view/View;)V
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    :cond_0
    :goto_0
    sget-boolean v1, Lcom/android/server/tv/TvHanlder;->isSleepModeShowed:Z

    if-eqz v1, :cond_1

    iget-object v1, p0, Lcom/android/server/tv/TvHanlder;->sleepModeView:Landroid/view/View;

    if-eqz v1, :cond_1

    :try_start_1
    iget-object v1, p0, Lcom/android/server/tv/TvHanlder;->wm:Landroid/view/WindowManager;

    iget-object v2, p0, Lcom/android/server/tv/TvHanlder;->sleepModeView:Landroid/view/View;

    invoke-interface {v1, v2}, Landroid/view/WindowManager;->removeView(Landroid/view/View;)V
    :try_end_1
    .catch Ljava/lang/Exception; {:try_start_1 .. :try_end_1} :catch_1

    :cond_1
    :goto_1
    sget-boolean v1, Lcom/android/server/tv/TvHanlder;->isAudioModeShowed:Z

    if-eqz v1, :cond_2

    iget-object v1, p0, Lcom/android/server/tv/TvHanlder;->audioModeView:Landroid/view/View;

    if-eqz v1, :cond_2

    :try_start_2
    iget-object v1, p0, Lcom/android/server/tv/TvHanlder;->wm:Landroid/view/WindowManager;

    iget-object v2, p0, Lcom/android/server/tv/TvHanlder;->audioModeView:Landroid/view/View;

    invoke-interface {v1, v2}, Landroid/view/WindowManager;->removeView(Landroid/view/View;)V
    :try_end_2
    .catch Ljava/lang/Exception; {:try_start_2 .. :try_end_2} :catch_2

    :cond_2
    :goto_2
    sget-boolean v1, Lcom/android/server/tv/TvHanlder;->isPicModeShowed:Z

    if-eqz v1, :cond_3

    iget-object v1, p0, Lcom/android/server/tv/TvHanlder;->picModeView:Landroid/view/View;

    if-eqz v1, :cond_3

    :try_start_3
    iget-object v1, p0, Lcom/android/server/tv/TvHanlder;->wm:Landroid/view/WindowManager;

    iget-object v2, p0, Lcom/android/server/tv/TvHanlder;->picModeView:Landroid/view/View;

    invoke-interface {v1, v2}, Landroid/view/WindowManager;->removeView(Landroid/view/View;)V
    :try_end_3
    .catch Ljava/lang/Exception; {:try_start_3 .. :try_end_3} :catch_3

    :cond_3
    :goto_3
    sget-boolean v1, Lcom/android/server/tv/TvHanlder;->isZoomShowed:Z

    if-eqz v1, :cond_4

    iget-object v1, p0, Lcom/android/server/tv/TvHanlder;->zoomView:Landroid/view/View;

    if-eqz v1, :cond_4

    :try_start_4
    iget-object v1, p0, Lcom/android/server/tv/TvHanlder;->wm:Landroid/view/WindowManager;

    iget-object v2, p0, Lcom/android/server/tv/TvHanlder;->zoomView:Landroid/view/View;

    invoke-interface {v1, v2}, Landroid/view/WindowManager;->removeView(Landroid/view/View;)V
    :try_end_4
    .catch Ljava/lang/Exception; {:try_start_4 .. :try_end_4} :catch_4

    :cond_4
    :goto_4
    sget-boolean v1, Lcom/android/server/tv/TvHanlder;->isMuteShowed:Z

    if-eqz v1, :cond_5

    iget-object v1, p0, Lcom/android/server/tv/TvHanlder;->muteView:Landroid/view/View;

    if-eqz v1, :cond_5

    :try_start_5
    iget-object v1, p0, Lcom/android/server/tv/TvHanlder;->wm:Landroid/view/WindowManager;

    iget-object v2, p0, Lcom/android/server/tv/TvHanlder;->muteView:Landroid/view/View;

    invoke-interface {v1, v2}, Landroid/view/WindowManager;->removeView(Landroid/view/View;)V
    :try_end_5
    .catch Ljava/lang/Exception; {:try_start_5 .. :try_end_5} :catch_5

    :cond_5
    :goto_5
    sget-boolean v1, Lcom/android/server/tv/TvHanlder;->isFreezeShowed:Z

    if-eqz v1, :cond_6

    iget-object v1, p0, Lcom/android/server/tv/TvHanlder;->freezeView:Landroid/view/View;

    if-eqz v1, :cond_6

    :try_start_6
    iget-object v1, p0, Lcom/android/server/tv/TvHanlder;->wm:Landroid/view/WindowManager;

    iget-object v2, p0, Lcom/android/server/tv/TvHanlder;->freezeView:Landroid/view/View;

    invoke-interface {v1, v2}, Landroid/view/WindowManager;->removeView(Landroid/view/View;)V
    :try_end_6
    .catch Ljava/lang/Exception; {:try_start_6 .. :try_end_6} :catch_6

    :cond_6
    :goto_6
    sget-boolean v1, Lcom/android/server/tv/TvHanlder;->isVolShowed:Z

    if-nez v1, :cond_7

    sget-boolean v1, Lcom/android/server/tv/TvHanlder;->isSleepModeShowed:Z

    if-nez v1, :cond_7

    sget-boolean v1, Lcom/android/server/tv/TvHanlder;->isAudioModeShowed:Z

    if-nez v1, :cond_7

    sget-boolean v1, Lcom/android/server/tv/TvHanlder;->isPicModeShowed:Z

    if-nez v1, :cond_7

    sget-boolean v1, Lcom/android/server/tv/TvHanlder;->isZoomShowed:Z

    if-nez v1, :cond_7

    sget-boolean v1, Lcom/android/server/tv/TvHanlder;->isMuteShowed:Z

    if-nez v1, :cond_7

    sget-boolean v1, Lcom/android/server/tv/TvHanlder;->isFreezeShowed:Z

    if-eqz v1, :cond_8

    :cond_7
    sput-boolean v3, Lcom/android/server/tv/TvHanlder;->isPicModeShowed:Z

    sput-boolean v3, Lcom/android/server/tv/TvHanlder;->isAudioModeShowed:Z

    sput-boolean v3, Lcom/android/server/tv/TvHanlder;->isSleepModeShowed:Z

    sput-boolean v3, Lcom/android/server/tv/TvHanlder;->isVolShowed:Z

    sput-boolean v3, Lcom/android/server/tv/TvHanlder;->isZoomShowed:Z

    sput-boolean v3, Lcom/android/server/tv/TvHanlder;->isMuteShowed:Z

    sput-boolean v3, Lcom/android/server/tv/TvHanlder;->isFreezeShowed:Z

    const-wide/16 v1, 0x12c

    :try_start_7
    invoke-static {v1, v2}, Ljava/lang/Thread;->sleep(J)V
    :try_end_7
    .catch Ljava/lang/InterruptedException; {:try_start_7 .. :try_end_7} :catch_7

    :cond_8
    :goto_7
    iget-object v1, p0, Lcom/android/server/tv/TvHanlder;->mHandlerThread:Landroid/os/HandlerThread;

    invoke-virtual {v1}, Landroid/os/HandlerThread;->quit()Z

    iput-object v4, p0, Lcom/android/server/tv/TvHanlder;->mHandlerThread:Landroid/os/HandlerThread;

    iput-object v4, p0, Lcom/android/server/tv/TvHanlder;->mTvPlayer:Lcom/mstar/android/tvapi/common/TvPlayer;

    iput-object v4, p0, Lcom/android/server/tv/TvHanlder;->mTvService:Lcom/mstar/android/tv/ITvService;

    iput-object v4, p0, Lcom/android/server/tv/TvHanlder;->wm:Landroid/view/WindowManager;

    return-void

    :catch_0
    move-exception v0

    invoke-virtual {v0}, Ljava/lang/Exception;->printStackTrace()V

    goto/16 :goto_0

    :catch_1
    move-exception v0

    invoke-virtual {v0}, Ljava/lang/Exception;->printStackTrace()V

    goto/16 :goto_1

    :catch_2
    move-exception v0

    invoke-virtual {v0}, Ljava/lang/Exception;->printStackTrace()V

    goto/16 :goto_2

    :catch_3
    move-exception v0

    invoke-virtual {v0}, Ljava/lang/Exception;->printStackTrace()V

    goto :goto_3

    :catch_4
    move-exception v0

    invoke-virtual {v0}, Ljava/lang/Exception;->printStackTrace()V

    goto :goto_4

    :catch_5
    move-exception v0

    invoke-virtual {v0}, Ljava/lang/Exception;->printStackTrace()V

    goto :goto_5

    :catch_6
    move-exception v0

    invoke-virtual {v0}, Ljava/lang/Exception;->printStackTrace()V

    goto :goto_6

    :catch_7
    move-exception v0

    invoke-virtual {v0}, Ljava/lang/InterruptedException;->printStackTrace()V

    goto :goto_7
.end method

.method public surfaceChanged(Landroid/view/SurfaceHolder;III)V
    .locals 2
    .param p1    # Landroid/view/SurfaceHolder;
    .param p2    # I
    .param p3    # I
    .param p4    # I

    const-string v0, "TvService"

    const-string v1, "surfaceChanged."

    invoke-static {v0, v1}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    return-void
.end method

.method public surfaceCreated(Landroid/view/SurfaceHolder;)V
    .locals 5
    .param p1    # Landroid/view/SurfaceHolder;

    const-string v3, "TvService"

    const-string v4, "surfaceCreated."

    invoke-static {v3, v4}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    :try_start_0
    invoke-interface {p1}, Landroid/view/SurfaceHolder;->lockCanvas()Landroid/graphics/Canvas;

    move-result-object v0

    new-instance v2, Landroid/graphics/Paint;

    invoke-direct {v2}, Landroid/graphics/Paint;-><init>()V

    const/4 v3, 0x0

    invoke-virtual {v0, v3}, Landroid/graphics/Canvas;->drawColor(I)V

    const/4 v3, 0x0

    invoke-virtual {v2, v3}, Landroid/graphics/Paint;->setColor(I)V

    invoke-interface {p1, v0}, Landroid/view/SurfaceHolder;->unlockCanvasAndPost(Landroid/graphics/Canvas;)V

    iget-object v3, p0, Lcom/android/server/tv/TvHanlder;->mTvPlayer:Lcom/mstar/android/tvapi/common/TvPlayer;

    invoke-interface {v3, p1}, Lcom/mstar/android/tvapi/common/TvPlayer;->setDisplay(Landroid/view/SurfaceHolder;)V
    :try_end_0
    .catch Ljava/lang/Throwable; {:try_start_0 .. :try_end_0} :catch_0

    :goto_0
    return-void

    :catch_0
    move-exception v1

    invoke-virtual {v1}, Ljava/lang/Throwable;->printStackTrace()V

    goto :goto_0
.end method

.method public surfaceDestroyed(Landroid/view/SurfaceHolder;)V
    .locals 2
    .param p1    # Landroid/view/SurfaceHolder;

    const-string v0, "TvService"

    const-string v1, "surfaceDestroyed."

    invoke-static {v0, v1}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    return-void
.end method
