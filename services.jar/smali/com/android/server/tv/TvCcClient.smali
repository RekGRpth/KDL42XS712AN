.class public Lcom/android/server/tv/TvCcClient;
.super Lcom/mstar/android/tv/ITvCc$Stub;
.source "TvCcClient.java"


# static fields
.field private static final TAG:Ljava/lang/String; = "TvCcClient"


# direct methods
.method public constructor <init>()V
    .locals 0

    invoke-direct {p0}, Lcom/mstar/android/tv/ITvCc$Stub;-><init>()V

    return-void
.end method


# virtual methods
.method public creatPreviewCcWindow()Z
    .locals 1
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Landroid/os/RemoteException;
        }
    .end annotation

    const/4 v0, 0x0

    return v0
.end method

.method public drawPreviewCc(Lcom/mstar/android/tvapi/common/vo/CaptionOptionSetting;)Z
    .locals 1
    .param p1    # Lcom/mstar/android/tvapi/common/vo/CaptionOptionSetting;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Landroid/os/RemoteException;
        }
    .end annotation

    const/4 v0, 0x0

    return v0
.end method

.method public exitPreviewCc()Z
    .locals 1
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Landroid/os/RemoteException;
        }
    .end annotation

    const/4 v0, 0x0

    return v0
.end method

.method public startCc()Z
    .locals 1
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Landroid/os/RemoteException;
        }
    .end annotation

    const/4 v0, 0x0

    return v0
.end method

.method public stopCc()Z
    .locals 1
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Landroid/os/RemoteException;
        }
    .end annotation

    const/4 v0, 0x0

    return v0
.end method
