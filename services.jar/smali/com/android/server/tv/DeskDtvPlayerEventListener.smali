.class public Lcom/android/server/tv/DeskDtvPlayerEventListener;
.super Ljava/lang/Object;
.source "DeskDtvPlayerEventListener.java"

# interfaces
.implements Lcom/mstar/android/tvapi/dtv/listener/OnDtvPlayerEventListener;


# static fields
.field private static dtvEventListener:Lcom/android/server/tv/DeskDtvPlayerEventListener;


# instance fields
.field private clients:Ljava/util/HashMap;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/HashMap",
            "<",
            "Ljava/lang/String;",
            "Lcom/mstar/android/tv/IDtvPlayerEventClient;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method static constructor <clinit>()V
    .locals 1

    const/4 v0, 0x0

    sput-object v0, Lcom/android/server/tv/DeskDtvPlayerEventListener;->dtvEventListener:Lcom/android/server/tv/DeskDtvPlayerEventListener;

    return-void
.end method

.method public constructor <init>()V
    .locals 1

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    new-instance v0, Ljava/util/HashMap;

    invoke-direct {v0}, Ljava/util/HashMap;-><init>()V

    iput-object v0, p0, Lcom/android/server/tv/DeskDtvPlayerEventListener;->clients:Ljava/util/HashMap;

    return-void
.end method

.method public static getInstance()Lcom/android/server/tv/DeskDtvPlayerEventListener;
    .locals 1

    sget-object v0, Lcom/android/server/tv/DeskDtvPlayerEventListener;->dtvEventListener:Lcom/android/server/tv/DeskDtvPlayerEventListener;

    if-nez v0, :cond_0

    new-instance v0, Lcom/android/server/tv/DeskDtvPlayerEventListener;

    invoke-direct {v0}, Lcom/android/server/tv/DeskDtvPlayerEventListener;-><init>()V

    sput-object v0, Lcom/android/server/tv/DeskDtvPlayerEventListener;->dtvEventListener:Lcom/android/server/tv/DeskDtvPlayerEventListener;

    :cond_0
    sget-object v0, Lcom/android/server/tv/DeskDtvPlayerEventListener;->dtvEventListener:Lcom/android/server/tv/DeskDtvPlayerEventListener;

    return-object v0
.end method


# virtual methods
.method public onAudioModeChange(IZ)Z
    .locals 5
    .param p1    # I
    .param p2    # Z

    const-string v3, "TvApp"

    const-string v4, "onAudioModeChange in DeskDtvPlayerEventListener"

    invoke-static {v3, v4}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    iget-object v3, p0, Lcom/android/server/tv/DeskDtvPlayerEventListener;->clients:Ljava/util/HashMap;

    if-nez v3, :cond_0

    const/4 v3, 0x0

    :goto_0
    return v3

    :cond_0
    :try_start_0
    iget-object v3, p0, Lcom/android/server/tv/DeskDtvPlayerEventListener;->clients:Ljava/util/HashMap;

    invoke-virtual {v3}, Ljava/util/HashMap;->values()Ljava/util/Collection;

    move-result-object v3

    invoke-interface {v3}, Ljava/util/Collection;->iterator()Ljava/util/Iterator;

    move-result-object v2

    :goto_1
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    move-result v3

    if-eqz v3, :cond_1

    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/mstar/android/tv/IDtvPlayerEventClient;

    invoke-interface {v0, p1, p2}, Lcom/mstar/android/tv/IDtvPlayerEventClient;->onAudioModeChange(IZ)Z
    :try_end_0
    .catch Landroid/os/RemoteException; {:try_start_0 .. :try_end_0} :catch_0

    goto :goto_1

    :catch_0
    move-exception v1

    invoke-virtual {v1}, Landroid/os/RemoteException;->printStackTrace()V

    :cond_1
    const/4 v3, 0x1

    goto :goto_0
.end method

.method public onChangeTtxStatus(IZ)Z
    .locals 5
    .param p1    # I
    .param p2    # Z

    const-string v3, "TvApp"

    const-string v4, "onChangeTtxStatus in DeskDtvPlayerEventListener"

    invoke-static {v3, v4}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    iget-object v3, p0, Lcom/android/server/tv/DeskDtvPlayerEventListener;->clients:Ljava/util/HashMap;

    if-nez v3, :cond_0

    const/4 v3, 0x0

    :goto_0
    return v3

    :cond_0
    :try_start_0
    iget-object v3, p0, Lcom/android/server/tv/DeskDtvPlayerEventListener;->clients:Ljava/util/HashMap;

    invoke-virtual {v3}, Ljava/util/HashMap;->values()Ljava/util/Collection;

    move-result-object v3

    invoke-interface {v3}, Ljava/util/Collection;->iterator()Ljava/util/Iterator;

    move-result-object v2

    :goto_1
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    move-result v3

    if-eqz v3, :cond_1

    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/mstar/android/tv/IDtvPlayerEventClient;

    invoke-interface {v0, p1, p2}, Lcom/mstar/android/tv/IDtvPlayerEventClient;->onChangeTtxStatus(IZ)Z
    :try_end_0
    .catch Landroid/os/RemoteException; {:try_start_0 .. :try_end_0} :catch_0

    goto :goto_1

    :catch_0
    move-exception v1

    invoke-virtual {v1}, Landroid/os/RemoteException;->printStackTrace()V

    :cond_1
    const/4 v3, 0x1

    goto :goto_0
.end method

.method public onCiLoadCredentialFail(I)Z
    .locals 5
    .param p1    # I

    const-string v3, "TvApp"

    const-string v4, "onCiLoadCredentialFail in DeskDtvPlayerEventListener"

    invoke-static {v3, v4}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    iget-object v3, p0, Lcom/android/server/tv/DeskDtvPlayerEventListener;->clients:Ljava/util/HashMap;

    if-nez v3, :cond_0

    const/4 v3, 0x0

    :goto_0
    return v3

    :cond_0
    :try_start_0
    iget-object v3, p0, Lcom/android/server/tv/DeskDtvPlayerEventListener;->clients:Ljava/util/HashMap;

    invoke-virtual {v3}, Ljava/util/HashMap;->values()Ljava/util/Collection;

    move-result-object v3

    invoke-interface {v3}, Ljava/util/Collection;->iterator()Ljava/util/Iterator;

    move-result-object v2

    :goto_1
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    move-result v3

    if-eqz v3, :cond_1

    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/mstar/android/tv/IDtvPlayerEventClient;

    invoke-interface {v0, p1}, Lcom/mstar/android/tv/IDtvPlayerEventClient;->onCiLoadCredentialFail(I)Z
    :try_end_0
    .catch Landroid/os/RemoteException; {:try_start_0 .. :try_end_0} :catch_0

    goto :goto_1

    :catch_0
    move-exception v1

    invoke-virtual {v1}, Landroid/os/RemoteException;->printStackTrace()V

    :cond_1
    const/4 v3, 0x1

    goto :goto_0
.end method

.method public onDtvAutoTuningScanInfo(ILcom/mstar/android/tvapi/dtv/vo/DtvEventScan;)Z
    .locals 5
    .param p1    # I
    .param p2    # Lcom/mstar/android/tvapi/dtv/vo/DtvEventScan;

    const-string v3, "TvApp"

    const-string v4, "onDtvAutoTuningScanInfo"

    invoke-static {v3, v4}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    iget-object v3, p0, Lcom/android/server/tv/DeskDtvPlayerEventListener;->clients:Ljava/util/HashMap;

    if-nez v3, :cond_0

    const/4 v3, 0x0

    :goto_0
    return v3

    :cond_0
    :try_start_0
    iget-object v3, p0, Lcom/android/server/tv/DeskDtvPlayerEventListener;->clients:Ljava/util/HashMap;

    invoke-virtual {v3}, Ljava/util/HashMap;->values()Ljava/util/Collection;

    move-result-object v3

    invoke-interface {v3}, Ljava/util/Collection;->iterator()Ljava/util/Iterator;

    move-result-object v2

    :goto_1
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    move-result v3

    if-eqz v3, :cond_1

    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/mstar/android/tv/IDtvPlayerEventClient;

    invoke-interface {v0, p1, p2}, Lcom/mstar/android/tv/IDtvPlayerEventClient;->onDtvAutoTuningScanInfo(ILcom/mstar/android/tvapi/dtv/vo/DtvEventScan;)Z
    :try_end_0
    .catch Landroid/os/RemoteException; {:try_start_0 .. :try_end_0} :catch_0

    goto :goto_1

    :catch_0
    move-exception v1

    invoke-virtual {v1}, Landroid/os/RemoteException;->printStackTrace()V

    :cond_1
    const/4 v3, 0x1

    goto :goto_0
.end method

.method public onDtvAutoUpdateScan(I)Z
    .locals 5
    .param p1    # I

    const-string v3, "TvApp"

    const-string v4, "onDtvAutoUpdateScan in DeskDtvPlayerEventListener"

    invoke-static {v3, v4}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    iget-object v3, p0, Lcom/android/server/tv/DeskDtvPlayerEventListener;->clients:Ljava/util/HashMap;

    if-nez v3, :cond_0

    const/4 v3, 0x0

    :goto_0
    return v3

    :cond_0
    :try_start_0
    iget-object v3, p0, Lcom/android/server/tv/DeskDtvPlayerEventListener;->clients:Ljava/util/HashMap;

    invoke-virtual {v3}, Ljava/util/HashMap;->values()Ljava/util/Collection;

    move-result-object v3

    invoke-interface {v3}, Ljava/util/Collection;->iterator()Ljava/util/Iterator;

    move-result-object v2

    :goto_1
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    move-result v3

    if-eqz v3, :cond_1

    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/mstar/android/tv/IDtvPlayerEventClient;

    invoke-interface {v0, p1}, Lcom/mstar/android/tv/IDtvPlayerEventClient;->onDtvAutoUpdateScan(I)Z
    :try_end_0
    .catch Landroid/os/RemoteException; {:try_start_0 .. :try_end_0} :catch_0

    goto :goto_1

    :catch_0
    move-exception v1

    invoke-virtual {v1}, Landroid/os/RemoteException;->printStackTrace()V

    :cond_1
    const/4 v3, 0x1

    goto :goto_0
.end method

.method public onDtvChannelNameReady(I)Z
    .locals 5
    .param p1    # I

    const-string v3, "TvApp"

    const-string v4, "onDtvChannelNameReady"

    invoke-static {v3, v4}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    iget-object v3, p0, Lcom/android/server/tv/DeskDtvPlayerEventListener;->clients:Ljava/util/HashMap;

    if-nez v3, :cond_0

    const/4 v3, 0x0

    :goto_0
    return v3

    :cond_0
    :try_start_0
    iget-object v3, p0, Lcom/android/server/tv/DeskDtvPlayerEventListener;->clients:Ljava/util/HashMap;

    invoke-virtual {v3}, Ljava/util/HashMap;->values()Ljava/util/Collection;

    move-result-object v3

    invoke-interface {v3}, Ljava/util/Collection;->iterator()Ljava/util/Iterator;

    move-result-object v2

    :goto_1
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    move-result v3

    if-eqz v3, :cond_1

    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/mstar/android/tv/IDtvPlayerEventClient;

    invoke-interface {v0, p1}, Lcom/mstar/android/tv/IDtvPlayerEventClient;->onDtvChannelNameReady(I)Z
    :try_end_0
    .catch Landroid/os/RemoteException; {:try_start_0 .. :try_end_0} :catch_0

    goto :goto_1

    :catch_0
    move-exception v1

    invoke-virtual {v1}, Landroid/os/RemoteException;->printStackTrace()V

    :cond_1
    const/4 v3, 0x1

    goto :goto_0
.end method

.method public onDtvPriComponentMissing(I)Z
    .locals 5
    .param p1    # I

    const-string v3, "TvApp"

    const-string v4, "onDtvPriComponentMissing in DeskDtvPlayerEventListener"

    invoke-static {v3, v4}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    iget-object v3, p0, Lcom/android/server/tv/DeskDtvPlayerEventListener;->clients:Ljava/util/HashMap;

    if-nez v3, :cond_0

    const/4 v3, 0x0

    :goto_0
    return v3

    :cond_0
    :try_start_0
    iget-object v3, p0, Lcom/android/server/tv/DeskDtvPlayerEventListener;->clients:Ljava/util/HashMap;

    invoke-virtual {v3}, Ljava/util/HashMap;->values()Ljava/util/Collection;

    move-result-object v3

    invoke-interface {v3}, Ljava/util/Collection;->iterator()Ljava/util/Iterator;

    move-result-object v2

    :goto_1
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    move-result v3

    if-eqz v3, :cond_1

    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/mstar/android/tv/IDtvPlayerEventClient;

    invoke-interface {v0, p1}, Lcom/mstar/android/tv/IDtvPlayerEventClient;->onDtvPriComponentMissing(I)Z
    :try_end_0
    .catch Landroid/os/RemoteException; {:try_start_0 .. :try_end_0} :catch_0

    goto :goto_1

    :catch_0
    move-exception v1

    invoke-virtual {v1}, Landroid/os/RemoteException;->printStackTrace()V

    :cond_1
    const/4 v3, 0x1

    goto :goto_0
.end method

.method public onDtvProgramInfoReady(I)Z
    .locals 5
    .param p1    # I

    const-string v3, "TvApp"

    const-string v4, "onDtvProgramInfoReady"

    invoke-static {v3, v4}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    iget-object v3, p0, Lcom/android/server/tv/DeskDtvPlayerEventListener;->clients:Ljava/util/HashMap;

    if-nez v3, :cond_0

    const/4 v3, 0x0

    :goto_0
    return v3

    :cond_0
    :try_start_0
    iget-object v3, p0, Lcom/android/server/tv/DeskDtvPlayerEventListener;->clients:Ljava/util/HashMap;

    invoke-virtual {v3}, Ljava/util/HashMap;->values()Ljava/util/Collection;

    move-result-object v3

    invoke-interface {v3}, Ljava/util/Collection;->iterator()Ljava/util/Iterator;

    move-result-object v2

    :goto_1
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    move-result v3

    if-eqz v3, :cond_1

    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/mstar/android/tv/IDtvPlayerEventClient;

    invoke-interface {v0, p1}, Lcom/mstar/android/tv/IDtvPlayerEventClient;->onDtvProgramInfoReady(I)Z
    :try_end_0
    .catch Landroid/os/RemoteException; {:try_start_0 .. :try_end_0} :catch_0

    goto :goto_1

    :catch_0
    move-exception v1

    invoke-virtual {v1}, Landroid/os/RemoteException;->printStackTrace()V

    :cond_1
    const/4 v3, 0x1

    goto :goto_0
.end method

.method public onEpgTimerSimulcast(II)Z
    .locals 5
    .param p1    # I
    .param p2    # I

    const-string v3, "TvApp"

    const-string v4, "onEpgTimerSimulcast in DeskDtvPlayerEventListener"

    invoke-static {v3, v4}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    iget-object v3, p0, Lcom/android/server/tv/DeskDtvPlayerEventListener;->clients:Ljava/util/HashMap;

    if-nez v3, :cond_0

    const/4 v3, 0x0

    :goto_0
    return v3

    :cond_0
    :try_start_0
    iget-object v3, p0, Lcom/android/server/tv/DeskDtvPlayerEventListener;->clients:Ljava/util/HashMap;

    invoke-virtual {v3}, Ljava/util/HashMap;->values()Ljava/util/Collection;

    move-result-object v3

    invoke-interface {v3}, Ljava/util/Collection;->iterator()Ljava/util/Iterator;

    move-result-object v2

    :goto_1
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    move-result v3

    if-eqz v3, :cond_1

    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/mstar/android/tv/IDtvPlayerEventClient;

    invoke-interface {v0, p1, p2}, Lcom/mstar/android/tv/IDtvPlayerEventClient;->onEpgTimerSimulcast(II)Z
    :try_end_0
    .catch Landroid/os/RemoteException; {:try_start_0 .. :try_end_0} :catch_0

    goto :goto_1

    :catch_0
    move-exception v1

    invoke-virtual {v1}, Landroid/os/RemoteException;->printStackTrace()V

    :cond_1
    const/4 v3, 0x1

    goto :goto_0
.end method

.method public onGingaStatusMode(IZ)Z
    .locals 5
    .param p1    # I
    .param p2    # Z

    const-string v3, "TvApp"

    const-string v4, "onGingaStatusMode in DeskDtvPlayerEventListener"

    invoke-static {v3, v4}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    iget-object v3, p0, Lcom/android/server/tv/DeskDtvPlayerEventListener;->clients:Ljava/util/HashMap;

    if-nez v3, :cond_0

    const/4 v3, 0x0

    :goto_0
    return v3

    :cond_0
    :try_start_0
    iget-object v3, p0, Lcom/android/server/tv/DeskDtvPlayerEventListener;->clients:Ljava/util/HashMap;

    invoke-virtual {v3}, Ljava/util/HashMap;->values()Ljava/util/Collection;

    move-result-object v3

    invoke-interface {v3}, Ljava/util/Collection;->iterator()Ljava/util/Iterator;

    move-result-object v2

    :goto_1
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    move-result v3

    if-eqz v3, :cond_1

    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/mstar/android/tv/IDtvPlayerEventClient;

    invoke-interface {v0, p1, p2}, Lcom/mstar/android/tv/IDtvPlayerEventClient;->onGingaStatusMode(IZ)Z
    :try_end_0
    .catch Landroid/os/RemoteException; {:try_start_0 .. :try_end_0} :catch_0

    goto :goto_1

    :catch_0
    move-exception v1

    invoke-virtual {v1}, Landroid/os/RemoteException;->printStackTrace()V

    :cond_1
    const/4 v3, 0x1

    goto :goto_0
.end method

.method public onHbbtvStatusMode(IZ)Z
    .locals 5
    .param p1    # I
    .param p2    # Z

    const-string v3, "TvApp"

    const-string v4, "onHbbtvStatusMode in DeskDtvPlayerEventListener"

    invoke-static {v3, v4}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    iget-object v3, p0, Lcom/android/server/tv/DeskDtvPlayerEventListener;->clients:Ljava/util/HashMap;

    if-nez v3, :cond_0

    const/4 v3, 0x0

    :goto_0
    return v3

    :cond_0
    :try_start_0
    iget-object v3, p0, Lcom/android/server/tv/DeskDtvPlayerEventListener;->clients:Ljava/util/HashMap;

    invoke-virtual {v3}, Ljava/util/HashMap;->values()Ljava/util/Collection;

    move-result-object v3

    invoke-interface {v3}, Ljava/util/Collection;->iterator()Ljava/util/Iterator;

    move-result-object v2

    :goto_1
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    move-result v3

    if-eqz v3, :cond_1

    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/mstar/android/tv/IDtvPlayerEventClient;

    invoke-interface {v0, p1, p2}, Lcom/mstar/android/tv/IDtvPlayerEventClient;->onHbbtvStatusMode(IZ)Z
    :try_end_0
    .catch Landroid/os/RemoteException; {:try_start_0 .. :try_end_0} :catch_0

    goto :goto_1

    :catch_0
    move-exception v1

    invoke-virtual {v1}, Landroid/os/RemoteException;->printStackTrace()V

    :cond_1
    const/4 v3, 0x1

    goto :goto_0
.end method

.method public onMheg5EventHandler(II)Z
    .locals 5
    .param p1    # I
    .param p2    # I

    const-string v3, "TvApp"

    const-string v4, "onMheg5EventHandler in DeskDtvPlayerEventListener"

    invoke-static {v3, v4}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    iget-object v3, p0, Lcom/android/server/tv/DeskDtvPlayerEventListener;->clients:Ljava/util/HashMap;

    if-nez v3, :cond_0

    const/4 v3, 0x0

    :goto_0
    return v3

    :cond_0
    :try_start_0
    iget-object v3, p0, Lcom/android/server/tv/DeskDtvPlayerEventListener;->clients:Ljava/util/HashMap;

    invoke-virtual {v3}, Ljava/util/HashMap;->values()Ljava/util/Collection;

    move-result-object v3

    invoke-interface {v3}, Ljava/util/Collection;->iterator()Ljava/util/Iterator;

    move-result-object v2

    :goto_1
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    move-result v3

    if-eqz v3, :cond_1

    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/mstar/android/tv/IDtvPlayerEventClient;

    invoke-interface {v0, p1, p2}, Lcom/mstar/android/tv/IDtvPlayerEventClient;->onMheg5EventHandler(II)Z
    :try_end_0
    .catch Landroid/os/RemoteException; {:try_start_0 .. :try_end_0} :catch_0

    goto :goto_1

    :catch_0
    move-exception v1

    invoke-virtual {v1}, Landroid/os/RemoteException;->printStackTrace()V

    :cond_1
    const/4 v3, 0x1

    goto :goto_0
.end method

.method public onMheg5ReturnKey(II)Z
    .locals 5
    .param p1    # I
    .param p2    # I

    const-string v3, "TvApp"

    const-string v4, "onMheg5ReturnKey in DeskDtvPlayerEventListener"

    invoke-static {v3, v4}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    iget-object v3, p0, Lcom/android/server/tv/DeskDtvPlayerEventListener;->clients:Ljava/util/HashMap;

    if-nez v3, :cond_0

    const/4 v3, 0x0

    :goto_0
    return v3

    :cond_0
    :try_start_0
    iget-object v3, p0, Lcom/android/server/tv/DeskDtvPlayerEventListener;->clients:Ljava/util/HashMap;

    invoke-virtual {v3}, Ljava/util/HashMap;->values()Ljava/util/Collection;

    move-result-object v3

    invoke-interface {v3}, Ljava/util/Collection;->iterator()Ljava/util/Iterator;

    move-result-object v2

    :goto_1
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    move-result v3

    if-eqz v3, :cond_1

    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/mstar/android/tv/IDtvPlayerEventClient;

    invoke-interface {v0, p1, p2}, Lcom/mstar/android/tv/IDtvPlayerEventClient;->onMheg5ReturnKey(II)Z
    :try_end_0
    .catch Landroid/os/RemoteException; {:try_start_0 .. :try_end_0} :catch_0

    goto :goto_1

    :catch_0
    move-exception v1

    invoke-virtual {v1}, Landroid/os/RemoteException;->printStackTrace()V

    :cond_1
    const/4 v3, 0x1

    goto :goto_0
.end method

.method public onMheg5StatusMode(II)Z
    .locals 5
    .param p1    # I
    .param p2    # I

    const-string v3, "TvApp"

    const-string v4, "onMheg5StatusMode in DeskDtvPlayerEventListener"

    invoke-static {v3, v4}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    iget-object v3, p0, Lcom/android/server/tv/DeskDtvPlayerEventListener;->clients:Ljava/util/HashMap;

    if-nez v3, :cond_0

    const/4 v3, 0x0

    :goto_0
    return v3

    :cond_0
    :try_start_0
    iget-object v3, p0, Lcom/android/server/tv/DeskDtvPlayerEventListener;->clients:Ljava/util/HashMap;

    invoke-virtual {v3}, Ljava/util/HashMap;->values()Ljava/util/Collection;

    move-result-object v3

    invoke-interface {v3}, Ljava/util/Collection;->iterator()Ljava/util/Iterator;

    move-result-object v2

    :goto_1
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    move-result v3

    if-eqz v3, :cond_1

    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/mstar/android/tv/IDtvPlayerEventClient;

    invoke-interface {v0, p1, p2}, Lcom/mstar/android/tv/IDtvPlayerEventClient;->onMheg5StatusMode(II)Z
    :try_end_0
    .catch Landroid/os/RemoteException; {:try_start_0 .. :try_end_0} :catch_0

    goto :goto_1

    :catch_0
    move-exception v1

    invoke-virtual {v1}, Landroid/os/RemoteException;->printStackTrace()V

    :cond_1
    const/4 v3, 0x1

    goto :goto_0
.end method

.method public onOadDownload(II)Z
    .locals 5
    .param p1    # I
    .param p2    # I

    const-string v3, "TvApp"

    const-string v4, "onOadDownload in DeskDtvPlayerEventListener"

    invoke-static {v3, v4}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    iget-object v3, p0, Lcom/android/server/tv/DeskDtvPlayerEventListener;->clients:Ljava/util/HashMap;

    if-nez v3, :cond_0

    const/4 v3, 0x0

    :goto_0
    return v3

    :cond_0
    :try_start_0
    iget-object v3, p0, Lcom/android/server/tv/DeskDtvPlayerEventListener;->clients:Ljava/util/HashMap;

    invoke-virtual {v3}, Ljava/util/HashMap;->values()Ljava/util/Collection;

    move-result-object v3

    invoke-interface {v3}, Ljava/util/Collection;->iterator()Ljava/util/Iterator;

    move-result-object v2

    :goto_1
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    move-result v3

    if-eqz v3, :cond_1

    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/mstar/android/tv/IDtvPlayerEventClient;

    invoke-interface {v0, p1, p2}, Lcom/mstar/android/tv/IDtvPlayerEventClient;->onOadDownload(II)Z
    :try_end_0
    .catch Landroid/os/RemoteException; {:try_start_0 .. :try_end_0} :catch_0

    goto :goto_1

    :catch_0
    move-exception v1

    invoke-virtual {v1}, Landroid/os/RemoteException;->printStackTrace()V

    :cond_1
    const/4 v3, 0x1

    goto :goto_0
.end method

.method public onOadHandler(III)Z
    .locals 5
    .param p1    # I
    .param p2    # I
    .param p3    # I

    const-string v3, "TvApp"

    const-string v4, "onOadHandler in DeskDtvPlayerEventListener"

    invoke-static {v3, v4}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    iget-object v3, p0, Lcom/android/server/tv/DeskDtvPlayerEventListener;->clients:Ljava/util/HashMap;

    if-nez v3, :cond_0

    const/4 v3, 0x0

    :goto_0
    return v3

    :cond_0
    :try_start_0
    iget-object v3, p0, Lcom/android/server/tv/DeskDtvPlayerEventListener;->clients:Ljava/util/HashMap;

    invoke-virtual {v3}, Ljava/util/HashMap;->values()Ljava/util/Collection;

    move-result-object v3

    invoke-interface {v3}, Ljava/util/Collection;->iterator()Ljava/util/Iterator;

    move-result-object v2

    :goto_1
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    move-result v3

    if-eqz v3, :cond_1

    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/mstar/android/tv/IDtvPlayerEventClient;

    invoke-interface {v0, p1, p2, p3}, Lcom/mstar/android/tv/IDtvPlayerEventClient;->onOadHandler(III)Z
    :try_end_0
    .catch Landroid/os/RemoteException; {:try_start_0 .. :try_end_0} :catch_0

    goto :goto_1

    :catch_0
    move-exception v1

    invoke-virtual {v1}, Landroid/os/RemoteException;->printStackTrace()V

    :cond_1
    const/4 v3, 0x1

    goto :goto_0
.end method

.method public onOadTimeout(II)Z
    .locals 5
    .param p1    # I
    .param p2    # I

    const-string v3, "TvApp"

    const-string v4, "onOadTimeout in DeskDtvPlayerEventListener"

    invoke-static {v3, v4}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    iget-object v3, p0, Lcom/android/server/tv/DeskDtvPlayerEventListener;->clients:Ljava/util/HashMap;

    if-nez v3, :cond_0

    const/4 v3, 0x0

    :goto_0
    return v3

    :cond_0
    :try_start_0
    iget-object v3, p0, Lcom/android/server/tv/DeskDtvPlayerEventListener;->clients:Ljava/util/HashMap;

    invoke-virtual {v3}, Ljava/util/HashMap;->values()Ljava/util/Collection;

    move-result-object v3

    invoke-interface {v3}, Ljava/util/Collection;->iterator()Ljava/util/Iterator;

    move-result-object v2

    :goto_1
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    move-result v3

    if-eqz v3, :cond_1

    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/mstar/android/tv/IDtvPlayerEventClient;

    invoke-interface {v0, p1, p2}, Lcom/mstar/android/tv/IDtvPlayerEventClient;->onOadTimeout(II)Z
    :try_end_0
    .catch Landroid/os/RemoteException; {:try_start_0 .. :try_end_0} :catch_0

    goto :goto_1

    :catch_0
    move-exception v1

    invoke-virtual {v1}, Landroid/os/RemoteException;->printStackTrace()V

    :cond_1
    const/4 v3, 0x1

    goto :goto_0
.end method

.method public onPopupScanDialogFrequencyChange(I)Z
    .locals 5
    .param p1    # I

    const-string v3, "TvApp"

    const-string v4, "onPopupScanDialogFrequencyChange in DeskDtvPlayerEventListener"

    invoke-static {v3, v4}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    iget-object v3, p0, Lcom/android/server/tv/DeskDtvPlayerEventListener;->clients:Ljava/util/HashMap;

    if-nez v3, :cond_0

    const/4 v3, 0x0

    :goto_0
    return v3

    :cond_0
    :try_start_0
    iget-object v3, p0, Lcom/android/server/tv/DeskDtvPlayerEventListener;->clients:Ljava/util/HashMap;

    invoke-virtual {v3}, Ljava/util/HashMap;->values()Ljava/util/Collection;

    move-result-object v3

    invoke-interface {v3}, Ljava/util/Collection;->iterator()Ljava/util/Iterator;

    move-result-object v2

    :goto_1
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    move-result v3

    if-eqz v3, :cond_1

    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/mstar/android/tv/IDtvPlayerEventClient;

    invoke-interface {v0, p1}, Lcom/mstar/android/tv/IDtvPlayerEventClient;->onPopupScanDialogFrequencyChange(I)Z
    :try_end_0
    .catch Landroid/os/RemoteException; {:try_start_0 .. :try_end_0} :catch_0

    goto :goto_1

    :catch_0
    move-exception v1

    invoke-virtual {v1}, Landroid/os/RemoteException;->printStackTrace()V

    :cond_1
    const/4 v3, 0x1

    goto :goto_0
.end method

.method public onPopupScanDialogLossSignal(I)Z
    .locals 5
    .param p1    # I

    const-string v3, "TvApp"

    const-string v4, "onPopupScanDialogLossSignal in DeskDtvPlayerEventListener"

    invoke-static {v3, v4}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    iget-object v3, p0, Lcom/android/server/tv/DeskDtvPlayerEventListener;->clients:Ljava/util/HashMap;

    if-nez v3, :cond_0

    const/4 v3, 0x0

    :goto_0
    return v3

    :cond_0
    :try_start_0
    iget-object v3, p0, Lcom/android/server/tv/DeskDtvPlayerEventListener;->clients:Ljava/util/HashMap;

    invoke-virtual {v3}, Ljava/util/HashMap;->values()Ljava/util/Collection;

    move-result-object v3

    invoke-interface {v3}, Ljava/util/Collection;->iterator()Ljava/util/Iterator;

    move-result-object v2

    :goto_1
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    move-result v3

    if-eqz v3, :cond_1

    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/mstar/android/tv/IDtvPlayerEventClient;

    invoke-interface {v0, p1}, Lcom/mstar/android/tv/IDtvPlayerEventClient;->onPopupScanDialogLossSignal(I)Z
    :try_end_0
    .catch Landroid/os/RemoteException; {:try_start_0 .. :try_end_0} :catch_0

    goto :goto_1

    :catch_0
    move-exception v1

    invoke-virtual {v1}, Landroid/os/RemoteException;->printStackTrace()V

    :cond_1
    const/4 v3, 0x1

    goto :goto_0
.end method

.method public onPopupScanDialogNewMultiplex(I)Z
    .locals 5
    .param p1    # I

    const-string v3, "TvApp"

    const-string v4, "onPopupScanDialogNewMultiplex in DeskDtvPlayerEventListener"

    invoke-static {v3, v4}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    iget-object v3, p0, Lcom/android/server/tv/DeskDtvPlayerEventListener;->clients:Ljava/util/HashMap;

    if-nez v3, :cond_0

    const/4 v3, 0x0

    :goto_0
    return v3

    :cond_0
    :try_start_0
    iget-object v3, p0, Lcom/android/server/tv/DeskDtvPlayerEventListener;->clients:Ljava/util/HashMap;

    invoke-virtual {v3}, Ljava/util/HashMap;->values()Ljava/util/Collection;

    move-result-object v3

    invoke-interface {v3}, Ljava/util/Collection;->iterator()Ljava/util/Iterator;

    move-result-object v2

    :goto_1
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    move-result v3

    if-eqz v3, :cond_1

    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/mstar/android/tv/IDtvPlayerEventClient;

    invoke-interface {v0, p1}, Lcom/mstar/android/tv/IDtvPlayerEventClient;->onPopupScanDialogNewMultiplex(I)Z
    :try_end_0
    .catch Landroid/os/RemoteException; {:try_start_0 .. :try_end_0} :catch_0

    goto :goto_1

    :catch_0
    move-exception v1

    invoke-virtual {v1}, Landroid/os/RemoteException;->printStackTrace()V

    :cond_1
    const/4 v3, 0x1

    goto :goto_0
.end method

.method public onRctPresence(I)Z
    .locals 5
    .param p1    # I

    const-string v3, "TvApp"

    const-string v4, "onRctPresence in DeskDtvPlayerEventListener"

    invoke-static {v3, v4}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    iget-object v3, p0, Lcom/android/server/tv/DeskDtvPlayerEventListener;->clients:Ljava/util/HashMap;

    if-nez v3, :cond_0

    const/4 v3, 0x0

    :goto_0
    return v3

    :cond_0
    :try_start_0
    iget-object v3, p0, Lcom/android/server/tv/DeskDtvPlayerEventListener;->clients:Ljava/util/HashMap;

    invoke-virtual {v3}, Ljava/util/HashMap;->values()Ljava/util/Collection;

    move-result-object v3

    invoke-interface {v3}, Ljava/util/Collection;->iterator()Ljava/util/Iterator;

    move-result-object v2

    :goto_1
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    move-result v3

    if-eqz v3, :cond_1

    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/mstar/android/tv/IDtvPlayerEventClient;

    invoke-interface {v0, p1}, Lcom/mstar/android/tv/IDtvPlayerEventClient;->onRctPresence(I)Z
    :try_end_0
    .catch Landroid/os/RemoteException; {:try_start_0 .. :try_end_0} :catch_0

    goto :goto_1

    :catch_0
    move-exception v1

    invoke-virtual {v1}, Landroid/os/RemoteException;->printStackTrace()V

    :cond_1
    const/4 v3, 0x1

    goto :goto_0
.end method

.method public onSignalLock(I)Z
    .locals 5
    .param p1    # I

    const-string v3, "TvApp"

    const-string v4, "onSignalLock in DeskDtvPlayerEventListener"

    invoke-static {v3, v4}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    iget-object v3, p0, Lcom/android/server/tv/DeskDtvPlayerEventListener;->clients:Ljava/util/HashMap;

    if-nez v3, :cond_0

    const/4 v3, 0x0

    :goto_0
    return v3

    :cond_0
    :try_start_0
    iget-object v3, p0, Lcom/android/server/tv/DeskDtvPlayerEventListener;->clients:Ljava/util/HashMap;

    invoke-virtual {v3}, Ljava/util/HashMap;->values()Ljava/util/Collection;

    move-result-object v3

    invoke-interface {v3}, Ljava/util/Collection;->iterator()Ljava/util/Iterator;

    move-result-object v2

    :goto_1
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    move-result v3

    if-eqz v3, :cond_1

    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/mstar/android/tv/IDtvPlayerEventClient;

    invoke-interface {v0, p1}, Lcom/mstar/android/tv/IDtvPlayerEventClient;->onSignalLock(I)Z
    :try_end_0
    .catch Landroid/os/RemoteException; {:try_start_0 .. :try_end_0} :catch_0

    goto :goto_1

    :catch_0
    move-exception v1

    invoke-virtual {v1}, Landroid/os/RemoteException;->printStackTrace()V

    :cond_1
    const/4 v3, 0x1

    goto :goto_0
.end method

.method public onSignalUnLock(I)Z
    .locals 5
    .param p1    # I

    const-string v3, "TvApp"

    const-string v4, "onSignalUnLock in DeskDtvPlayerEventListener"

    invoke-static {v3, v4}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    iget-object v3, p0, Lcom/android/server/tv/DeskDtvPlayerEventListener;->clients:Ljava/util/HashMap;

    if-nez v3, :cond_0

    const/4 v3, 0x0

    :goto_0
    return v3

    :cond_0
    :try_start_0
    iget-object v3, p0, Lcom/android/server/tv/DeskDtvPlayerEventListener;->clients:Ljava/util/HashMap;

    invoke-virtual {v3}, Ljava/util/HashMap;->values()Ljava/util/Collection;

    move-result-object v3

    invoke-interface {v3}, Ljava/util/Collection;->iterator()Ljava/util/Iterator;

    move-result-object v2

    :goto_1
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    move-result v3

    if-eqz v3, :cond_1

    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/mstar/android/tv/IDtvPlayerEventClient;

    invoke-interface {v0, p1}, Lcom/mstar/android/tv/IDtvPlayerEventClient;->onSignalUnLock(I)Z
    :try_end_0
    .catch Landroid/os/RemoteException; {:try_start_0 .. :try_end_0} :catch_0

    goto :goto_1

    :catch_0
    move-exception v1

    invoke-virtual {v1}, Landroid/os/RemoteException;->printStackTrace()V

    :cond_1
    const/4 v3, 0x1

    goto :goto_0
.end method

.method public onTsChange(I)Z
    .locals 5
    .param p1    # I

    const-string v3, "TvApp"

    const-string v4, "onTsChange in DeskDtvPlayerEventListener"

    invoke-static {v3, v4}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    iget-object v3, p0, Lcom/android/server/tv/DeskDtvPlayerEventListener;->clients:Ljava/util/HashMap;

    if-nez v3, :cond_0

    const/4 v3, 0x0

    :goto_0
    return v3

    :cond_0
    :try_start_0
    iget-object v3, p0, Lcom/android/server/tv/DeskDtvPlayerEventListener;->clients:Ljava/util/HashMap;

    invoke-virtual {v3}, Ljava/util/HashMap;->values()Ljava/util/Collection;

    move-result-object v3

    invoke-interface {v3}, Ljava/util/Collection;->iterator()Ljava/util/Iterator;

    move-result-object v2

    :goto_1
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    move-result v3

    if-eqz v3, :cond_1

    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/mstar/android/tv/IDtvPlayerEventClient;

    invoke-interface {v0, p1}, Lcom/mstar/android/tv/IDtvPlayerEventClient;->onTsChange(I)Z
    :try_end_0
    .catch Landroid/os/RemoteException; {:try_start_0 .. :try_end_0} :catch_0

    goto :goto_1

    :catch_0
    move-exception v1

    invoke-virtual {v1}, Landroid/os/RemoteException;->printStackTrace()V

    :cond_1
    const/4 v3, 0x1

    goto :goto_0
.end method

.method public releaseClient(Ljava/lang/String;)V
    .locals 1
    .param p1    # Ljava/lang/String;

    iget-object v0, p0, Lcom/android/server/tv/DeskDtvPlayerEventListener;->clients:Ljava/util/HashMap;

    invoke-virtual {v0, p1}, Ljava/util/HashMap;->remove(Ljava/lang/Object;)Ljava/lang/Object;

    return-void
.end method

.method public setClient(Ljava/lang/String;Lcom/mstar/android/tv/IDtvPlayerEventClient;)V
    .locals 1
    .param p1    # Ljava/lang/String;
    .param p2    # Lcom/mstar/android/tv/IDtvPlayerEventClient;

    iget-object v0, p0, Lcom/android/server/tv/DeskDtvPlayerEventListener;->clients:Ljava/util/HashMap;

    invoke-virtual {v0, p1, p2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    return-void
.end method
