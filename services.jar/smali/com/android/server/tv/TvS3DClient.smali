.class public Lcom/android/server/tv/TvS3DClient;
.super Lcom/mstar/android/tv/ITvS3D$Stub;
.source "TvS3DClient.java"


# static fields
.field private static final TAG:Ljava/lang/String; = "TvS3DClient"


# instance fields
.field private db:Lcom/android/server/tv/DataBaseDeskImpl;


# direct methods
.method public constructor <init>(Lcom/android/server/tv/TvCommonClient;Landroid/content/Context;Lcom/android/server/tv/TvHanlder;Lcom/android/server/tv/DataBaseDeskImpl;)V
    .locals 1
    .param p1    # Lcom/android/server/tv/TvCommonClient;
    .param p2    # Landroid/content/Context;
    .param p3    # Lcom/android/server/tv/TvHanlder;
    .param p4    # Lcom/android/server/tv/DataBaseDeskImpl;

    invoke-direct {p0}, Lcom/mstar/android/tv/ITvS3D$Stub;-><init>()V

    invoke-static {p2}, Lcom/android/server/tv/DataBaseDeskImpl;->getDataBaseMgrInstance(Landroid/content/Context;)Lcom/android/server/tv/DataBaseDeskImpl;

    move-result-object v0

    iput-object v0, p0, Lcom/android/server/tv/TvS3DClient;->db:Lcom/android/server/tv/DataBaseDeskImpl;

    return-void
.end method

.method private getCurrentInputSource()I
    .locals 6
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Landroid/os/RemoteException;
        }
    .end annotation

    const/4 v0, 0x0

    :try_start_0
    invoke-static {}, Lcom/mstar/android/tvapi/common/TvManager;->getInstance()Lcom/mstar/android/tvapi/common/TvManager;

    move-result-object v3

    if-eqz v3, :cond_0

    invoke-static {}, Lcom/mstar/android/tvapi/common/TvManager;->getInstance()Lcom/mstar/android/tvapi/common/TvManager;

    move-result-object v3

    invoke-virtual {v3}, Lcom/mstar/android/tvapi/common/TvManager;->getCurrentInputSource()Lcom/mstar/android/tvapi/common/vo/TvOsType$EnumInputSource;
    :try_end_0
    .catch Lcom/mstar/android/tvapi/common/exception/TvCommonException; {:try_start_0 .. :try_end_0} :catch_0

    move-result-object v0

    :cond_0
    :goto_0
    invoke-virtual {v0}, Lcom/mstar/android/tvapi/common/vo/TvOsType$EnumInputSource;->ordinal()I

    move-result v2

    const-string v3, "TvS3DClient"

    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    const-string v5, "getCurrentInputSource, return int "

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-static {v3, v4}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    return v2

    :catch_0
    move-exception v1

    invoke-virtual {v1}, Lcom/mstar/android/tvapi/common/exception/TvCommonException;->printStackTrace()V

    goto :goto_0
.end method

.method private isSignalStable()Z
    .locals 4

    const-string v2, "TvS3DClient"

    const-string v3, "isSignalStable"

    invoke-static {v2, v3}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    const/4 v1, 0x0

    :try_start_0
    invoke-static {}, Lcom/mstar/android/tvapi/common/TvManager;->getInstance()Lcom/mstar/android/tvapi/common/TvManager;

    move-result-object v2

    if-eqz v2, :cond_0

    invoke-static {}, Lcom/mstar/android/tvapi/common/TvManager;->getInstance()Lcom/mstar/android/tvapi/common/TvManager;

    move-result-object v2

    invoke-virtual {v2}, Lcom/mstar/android/tvapi/common/TvManager;->getPlayerManager()Lcom/mstar/android/tvapi/common/TvPlayer;

    move-result-object v2

    invoke-interface {v2}, Lcom/mstar/android/tvapi/common/TvPlayer;->isSignalStable()Z
    :try_end_0
    .catch Lcom/mstar/android/tvapi/common/exception/TvCommonException; {:try_start_0 .. :try_end_0} :catch_0

    move-result v2

    if-eqz v2, :cond_1

    const/4 v1, 0x1

    :cond_0
    :goto_0
    return v1

    :cond_1
    const/4 v1, 0x0

    goto :goto_0

    :catch_0
    move-exception v0

    invoke-virtual {v0}, Lcom/mstar/android/tvapi/common/exception/TvCommonException;->printStackTrace()V

    goto :goto_0
.end method


# virtual methods
.method public get3DDepthMode()I
    .locals 4
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Landroid/os/RemoteException;
        }
    .end annotation

    iget-object v1, p0, Lcom/android/server/tv/TvS3DClient;->db:Lcom/android/server/tv/DataBaseDeskImpl;

    sget-object v2, Lcom/android/server/tv/DataBaseDeskImpl$S3D_TABLE_FIELD;->eThreeDVideo3DDepth:Lcom/android/server/tv/DataBaseDeskImpl$S3D_TABLE_FIELD;

    invoke-direct {p0}, Lcom/android/server/tv/TvS3DClient;->getCurrentInputSource()I

    move-result v3

    invoke-virtual {v1, v2, v3}, Lcom/android/server/tv/DataBaseDeskImpl;->queryVideo3DMode(Lcom/android/server/tv/DataBaseDeskImpl$S3D_TABLE_FIELD;I)I

    move-result v0

    const-string v1, "TvS3DClient"

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "get3DDepthMode, return int "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    return v0
.end method

.method public get3DOffsetMode()I
    .locals 4
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Landroid/os/RemoteException;
        }
    .end annotation

    iget-object v1, p0, Lcom/android/server/tv/TvS3DClient;->db:Lcom/android/server/tv/DataBaseDeskImpl;

    sget-object v2, Lcom/android/server/tv/DataBaseDeskImpl$S3D_TABLE_FIELD;->eThreeDVideo3DOffset:Lcom/android/server/tv/DataBaseDeskImpl$S3D_TABLE_FIELD;

    invoke-direct {p0}, Lcom/android/server/tv/TvS3DClient;->getCurrentInputSource()I

    move-result v3

    invoke-virtual {v1, v2, v3}, Lcom/android/server/tv/DataBaseDeskImpl;->queryVideo3DMode(Lcom/android/server/tv/DataBaseDeskImpl$S3D_TABLE_FIELD;I)I

    move-result v0

    const-string v1, "TvS3DClient"

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "get3DOffsetMode, return int "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    return v0
.end method

.method public get3DOutputAspectMode()I
    .locals 4
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Landroid/os/RemoteException;
        }
    .end annotation

    iget-object v1, p0, Lcom/android/server/tv/TvS3DClient;->db:Lcom/android/server/tv/DataBaseDeskImpl;

    sget-object v2, Lcom/android/server/tv/DataBaseDeskImpl$S3D_TABLE_FIELD;->eThreeDVideo3DOutputAspect:Lcom/android/server/tv/DataBaseDeskImpl$S3D_TABLE_FIELD;

    invoke-direct {p0}, Lcom/android/server/tv/TvS3DClient;->getCurrentInputSource()I

    move-result v3

    invoke-virtual {v1, v2, v3}, Lcom/android/server/tv/DataBaseDeskImpl;->queryVideo3DMode(Lcom/android/server/tv/DataBaseDeskImpl$S3D_TABLE_FIELD;I)I

    move-result v0

    const-string v1, "TvS3DClient"

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "get3DOutputAspectMode, return int "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    return v0
.end method

.method public getAutoStartMode()I
    .locals 4
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Landroid/os/RemoteException;
        }
    .end annotation

    iget-object v1, p0, Lcom/android/server/tv/TvS3DClient;->db:Lcom/android/server/tv/DataBaseDeskImpl;

    sget-object v2, Lcom/android/server/tv/DataBaseDeskImpl$S3D_TABLE_FIELD;->eThreeDVideoAutoStart:Lcom/android/server/tv/DataBaseDeskImpl$S3D_TABLE_FIELD;

    invoke-direct {p0}, Lcom/android/server/tv/TvS3DClient;->getCurrentInputSource()I

    move-result v3

    invoke-virtual {v1, v2, v3}, Lcom/android/server/tv/DataBaseDeskImpl;->queryVideo3DMode(Lcom/android/server/tv/DataBaseDeskImpl$S3D_TABLE_FIELD;I)I

    move-result v0

    const-string v1, "TvS3DClient"

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "getAutoStartMode, return int "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    return v0
.end method

.method public getDisplay3DTo2DMode()I
    .locals 4
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Landroid/os/RemoteException;
        }
    .end annotation

    iget-object v1, p0, Lcom/android/server/tv/TvS3DClient;->db:Lcom/android/server/tv/DataBaseDeskImpl;

    sget-object v2, Lcom/android/server/tv/DataBaseDeskImpl$S3D_TABLE_FIELD;->eThreeDVideo3DTo2D:Lcom/android/server/tv/DataBaseDeskImpl$S3D_TABLE_FIELD;

    invoke-direct {p0}, Lcom/android/server/tv/TvS3DClient;->getCurrentInputSource()I

    move-result v3

    invoke-virtual {v1, v2, v3}, Lcom/android/server/tv/DataBaseDeskImpl;->queryVideo3DMode(Lcom/android/server/tv/DataBaseDeskImpl$S3D_TABLE_FIELD;I)I

    move-result v0

    const-string v1, "TvS3DClient"

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "getDisplay3DTo2DMode, return int "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    return v0
.end method

.method public getDisplayFormat()I
    .locals 4
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Landroid/os/RemoteException;
        }
    .end annotation

    sget-object v1, Lcom/android/server/tv/DataBaseDesk$EN_ThreeD_Video_DISPLAYFORMAT;->DB_ThreeD_Video_DISPLAYFORMAT_NONE:Lcom/android/server/tv/DataBaseDesk$EN_ThreeD_Video_DISPLAYFORMAT;

    invoke-virtual {v1}, Lcom/android/server/tv/DataBaseDesk$EN_ThreeD_Video_DISPLAYFORMAT;->ordinal()I

    move-result v0

    iget-object v1, p0, Lcom/android/server/tv/TvS3DClient;->db:Lcom/android/server/tv/DataBaseDeskImpl;

    sget-object v2, Lcom/android/server/tv/DataBaseDeskImpl$S3D_TABLE_FIELD;->eThreeDVideoSelfAdaptiveDetect:Lcom/android/server/tv/DataBaseDeskImpl$S3D_TABLE_FIELD;

    sget-object v3, Lcom/mstar/android/tvapi/common/vo/TvOsType$EnumInputSource;->E_INPUT_SOURCE_HDMI:Lcom/mstar/android/tvapi/common/vo/TvOsType$EnumInputSource;

    invoke-virtual {v3}, Lcom/mstar/android/tvapi/common/vo/TvOsType$EnumInputSource;->ordinal()I

    move-result v3

    invoke-virtual {v1, v2, v3}, Lcom/android/server/tv/DataBaseDeskImpl;->queryVideo3DMode(Lcom/android/server/tv/DataBaseDeskImpl$S3D_TABLE_FIELD;I)I

    move-result v1

    sget-object v2, Lcom/android/server/tv/DataBaseDesk$EN_ThreeD_Video_SELFADAPTIVE_DETECT;->DB_ThreeD_Video_SELF_ADAPTIVE_DETECT_OFF:Lcom/android/server/tv/DataBaseDesk$EN_ThreeD_Video_SELFADAPTIVE_DETECT;

    invoke-virtual {v2}, Lcom/android/server/tv/DataBaseDesk$EN_ThreeD_Video_SELFADAPTIVE_DETECT;->ordinal()I

    move-result v2

    if-ne v1, v2, :cond_0

    iget-object v1, p0, Lcom/android/server/tv/TvS3DClient;->db:Lcom/android/server/tv/DataBaseDeskImpl;

    sget-object v2, Lcom/android/server/tv/DataBaseDeskImpl$S3D_TABLE_FIELD;->eThreeDVideoDisplayFormat:Lcom/android/server/tv/DataBaseDeskImpl$S3D_TABLE_FIELD;

    invoke-direct {p0}, Lcom/android/server/tv/TvS3DClient;->getCurrentInputSource()I

    move-result v3

    invoke-virtual {v1, v2, v3}, Lcom/android/server/tv/DataBaseDeskImpl;->queryVideo3DMode(Lcom/android/server/tv/DataBaseDeskImpl$S3D_TABLE_FIELD;I)I

    move-result v0

    :goto_0
    const-string v1, "TvS3DClient"

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "getDisplayFormat, return int "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    return v0

    :cond_0
    sget-object v1, Lcom/android/server/tv/DataBaseDesk$EN_ThreeD_Video_DISPLAYFORMAT;->DB_ThreeD_Video_DISPLAYFORMAT_AUTO:Lcom/android/server/tv/DataBaseDesk$EN_ThreeD_Video_DISPLAYFORMAT;

    invoke-virtual {v1}, Lcom/android/server/tv/DataBaseDesk$EN_ThreeD_Video_DISPLAYFORMAT;->ordinal()I

    move-result v0

    goto :goto_0
.end method

.method public getLrViewSwitch()I
    .locals 7
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Landroid/os/RemoteException;
        }
    .end annotation

    const/4 v0, 0x0

    :try_start_0
    invoke-static {}, Lcom/mstar/android/tvapi/common/TvManager;->getInstance()Lcom/mstar/android/tvapi/common/TvManager;

    move-result-object v4

    if-eqz v4, :cond_0

    invoke-static {}, Lcom/mstar/android/tvapi/common/TvManager;->getInstance()Lcom/mstar/android/tvapi/common/TvManager;

    move-result-object v4

    invoke-virtual {v4}, Lcom/mstar/android/tvapi/common/TvManager;->getThreeDimensionManager()Lcom/mstar/android/tvapi/common/ThreeDimensionManager;

    move-result-object v4

    invoke-virtual {v4}, Lcom/mstar/android/tvapi/common/ThreeDimensionManager;->get3dFormatDetectFlag()Z
    :try_end_0
    .catch Lcom/mstar/android/tvapi/common/exception/TvCommonException; {:try_start_0 .. :try_end_0} :catch_0

    move-result v0

    :cond_0
    :goto_0
    if-eqz v0, :cond_1

    sget-object v4, Lcom/android/server/tv/DataBaseDesk$EN_ThreeD_Video_LRVIEWSWITCH;->DB_ThreeD_Video_LRVIEWSWITCH_NOTEXCHANGE:Lcom/android/server/tv/DataBaseDesk$EN_ThreeD_Video_LRVIEWSWITCH;

    invoke-virtual {v4}, Lcom/android/server/tv/DataBaseDesk$EN_ThreeD_Video_LRVIEWSWITCH;->ordinal()I

    move-result v2

    const-string v4, "TvS3DClient"

    new-instance v5, Ljava/lang/StringBuilder;

    invoke-direct {v5}, Ljava/lang/StringBuilder;-><init>()V

    const-string v6, "getLrViewSwitch, return int "

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    invoke-static {v4, v5}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    move v3, v2

    :goto_1
    return v3

    :catch_0
    move-exception v1

    invoke-virtual {v1}, Lcom/mstar/android/tvapi/common/exception/TvCommonException;->printStackTrace()V

    goto :goto_0

    :cond_1
    sget-object v4, Lcom/android/server/tv/DataBaseDesk$EN_ThreeD_Video_LRVIEWSWITCH;->DB_ThreeD_Video_LRVIEWSWITCH_EXCHANGE:Lcom/android/server/tv/DataBaseDesk$EN_ThreeD_Video_LRVIEWSWITCH;

    invoke-virtual {v4}, Lcom/android/server/tv/DataBaseDesk$EN_ThreeD_Video_LRVIEWSWITCH;->ordinal()I

    move-result v2

    const-string v4, "TvS3DClient"

    new-instance v5, Ljava/lang/StringBuilder;

    invoke-direct {v5}, Ljava/lang/StringBuilder;-><init>()V

    const-string v6, "getLrViewSwitch, return int "

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    invoke-static {v4, v5}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    move v3, v2

    goto :goto_1
.end method

.method public getSelfAdaptiveDetect()I
    .locals 4
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Landroid/os/RemoteException;
        }
    .end annotation

    iget-object v1, p0, Lcom/android/server/tv/TvS3DClient;->db:Lcom/android/server/tv/DataBaseDeskImpl;

    sget-object v2, Lcom/android/server/tv/DataBaseDeskImpl$S3D_TABLE_FIELD;->eThreeDVideoSelfAdaptiveDetect:Lcom/android/server/tv/DataBaseDeskImpl$S3D_TABLE_FIELD;

    sget-object v3, Lcom/mstar/android/tvapi/common/vo/TvOsType$EnumInputSource;->E_INPUT_SOURCE_HDMI:Lcom/mstar/android/tvapi/common/vo/TvOsType$EnumInputSource;

    invoke-virtual {v3}, Lcom/mstar/android/tvapi/common/vo/TvOsType$EnumInputSource;->ordinal()I

    move-result v3

    invoke-virtual {v1, v2, v3}, Lcom/android/server/tv/DataBaseDeskImpl;->queryVideo3DMode(Lcom/android/server/tv/DataBaseDeskImpl$S3D_TABLE_FIELD;I)I

    move-result v0

    const-string v1, "TvS3DClient"

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "getSelfAdaptiveDetect, return int "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    return v0
.end method

.method public set3DDepthMode(I)Z
    .locals 4
    .param p1    # I
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Landroid/os/RemoteException;
        }
    .end annotation

    const-string v1, "TvS3DClient"

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "set3DDepthMode, paras mode3dDepth is "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, p1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    iget-object v1, p0, Lcom/android/server/tv/TvS3DClient;->db:Lcom/android/server/tv/DataBaseDeskImpl;

    sget-object v2, Lcom/android/server/tv/DataBaseDeskImpl$S3D_TABLE_FIELD;->eThreeDVideo3DDepth:Lcom/android/server/tv/DataBaseDeskImpl$S3D_TABLE_FIELD;

    invoke-direct {p0}, Lcom/android/server/tv/TvS3DClient;->getCurrentInputSource()I

    move-result v3

    invoke-virtual {v1, v2, p1, v3}, Lcom/android/server/tv/DataBaseDeskImpl;->updateVideo3DMode(Lcom/android/server/tv/DataBaseDeskImpl$S3D_TABLE_FIELD;II)V

    :try_start_0
    invoke-static {}, Lcom/mstar/android/tvapi/common/TvManager;->getInstance()Lcom/mstar/android/tvapi/common/TvManager;

    move-result-object v1

    if-eqz v1, :cond_0

    invoke-static {}, Lcom/mstar/android/tvapi/common/TvManager;->getInstance()Lcom/mstar/android/tvapi/common/TvManager;

    move-result-object v1

    invoke-virtual {v1}, Lcom/mstar/android/tvapi/common/TvManager;->getThreeDimensionManager()Lcom/mstar/android/tvapi/common/ThreeDimensionManager;

    move-result-object v1

    invoke-virtual {v1, p1}, Lcom/mstar/android/tvapi/common/ThreeDimensionManager;->set3dGain(I)Z
    :try_end_0
    .catch Lcom/mstar/android/tvapi/common/exception/TvCommonException; {:try_start_0 .. :try_end_0} :catch_0

    :cond_0
    :goto_0
    const/4 v1, 0x1

    return v1

    :catch_0
    move-exception v0

    invoke-virtual {v0}, Lcom/mstar/android/tvapi/common/exception/TvCommonException;->printStackTrace()V

    goto :goto_0
.end method

.method public set3DOffsetMode(I)Z
    .locals 4
    .param p1    # I
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Landroid/os/RemoteException;
        }
    .end annotation

    const-string v1, "TvS3DClient"

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "set3DOffsetMode, paras mode3dOffset is "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, p1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    iget-object v1, p0, Lcom/android/server/tv/TvS3DClient;->db:Lcom/android/server/tv/DataBaseDeskImpl;

    sget-object v2, Lcom/android/server/tv/DataBaseDeskImpl$S3D_TABLE_FIELD;->eThreeDVideo3DOffset:Lcom/android/server/tv/DataBaseDeskImpl$S3D_TABLE_FIELD;

    invoke-direct {p0}, Lcom/android/server/tv/TvS3DClient;->getCurrentInputSource()I

    move-result v3

    invoke-virtual {v1, v2, p1, v3}, Lcom/android/server/tv/DataBaseDeskImpl;->updateVideo3DMode(Lcom/android/server/tv/DataBaseDeskImpl$S3D_TABLE_FIELD;II)V

    :try_start_0
    invoke-static {}, Lcom/mstar/android/tvapi/common/TvManager;->getInstance()Lcom/mstar/android/tvapi/common/TvManager;

    move-result-object v1

    if-eqz v1, :cond_0

    invoke-static {}, Lcom/mstar/android/tvapi/common/TvManager;->getInstance()Lcom/mstar/android/tvapi/common/TvManager;

    move-result-object v1

    invoke-virtual {v1}, Lcom/mstar/android/tvapi/common/TvManager;->getThreeDimensionManager()Lcom/mstar/android/tvapi/common/ThreeDimensionManager;

    move-result-object v1

    invoke-virtual {v1, p1}, Lcom/mstar/android/tvapi/common/ThreeDimensionManager;->set3dOffset(I)Z
    :try_end_0
    .catch Lcom/mstar/android/tvapi/common/exception/TvCommonException; {:try_start_0 .. :try_end_0} :catch_0

    :cond_0
    :goto_0
    const/4 v1, 0x1

    return v1

    :catch_0
    move-exception v0

    invoke-virtual {v0}, Lcom/mstar/android/tvapi/common/exception/TvCommonException;->printStackTrace()V

    goto :goto_0
.end method

.method public set3DOutputAspectMode(I)Z
    .locals 9
    .param p1    # I
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Landroid/os/RemoteException;
        }
    .end annotation

    const/4 v8, 0x1

    const-string v2, "TvS3DClient"

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string v4, "set3DOutputAspectMode, paras outputAspectMode is "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3, p1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v2, v3}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    sget-object v0, Lcom/mstar/android/tvapi/common/vo/Enum3dAspectRatioType;->E_3D_ASPECTRATIO_FULL:Lcom/mstar/android/tvapi/common/vo/Enum3dAspectRatioType;

    iget-object v2, p0, Lcom/android/server/tv/TvS3DClient;->db:Lcom/android/server/tv/DataBaseDeskImpl;

    sget-object v3, Lcom/android/server/tv/DataBaseDeskImpl$S3D_TABLE_FIELD;->eThreeDVideo3DOutputAspect:Lcom/android/server/tv/DataBaseDeskImpl$S3D_TABLE_FIELD;

    invoke-direct {p0}, Lcom/android/server/tv/TvS3DClient;->getCurrentInputSource()I

    move-result v4

    invoke-virtual {v2, v3, p1, v4}, Lcom/android/server/tv/DataBaseDeskImpl;->updateVideo3DMode(Lcom/android/server/tv/DataBaseDeskImpl$S3D_TABLE_FIELD;II)V

    :try_start_0
    invoke-static {}, Lcom/mstar/android/tvapi/common/TvManager;->getInstance()Lcom/mstar/android/tvapi/common/TvManager;

    move-result-object v2

    if-eqz v2, :cond_0

    invoke-static {}, Lcom/mstar/android/tvapi/common/TvManager;->getInstance()Lcom/mstar/android/tvapi/common/TvManager;

    move-result-object v2

    const/4 v3, 0x1

    sget-object v4, Lcom/mstar/android/tvapi/common/vo/EnumScreenMuteType;->E_BLACK:Lcom/mstar/android/tvapi/common/vo/EnumScreenMuteType;

    const/4 v5, 0x0

    invoke-static {}, Lcom/mstar/android/tvapi/common/vo/TvOsType$EnumInputSource;->values()[Lcom/mstar/android/tvapi/common/vo/TvOsType$EnumInputSource;

    move-result-object v6

    invoke-direct {p0}, Lcom/android/server/tv/TvS3DClient;->getCurrentInputSource()I

    move-result v7

    aget-object v6, v6, v7

    invoke-virtual {v2, v3, v4, v5, v6}, Lcom/mstar/android/tvapi/common/TvManager;->setVideoMute(ZLcom/mstar/android/tvapi/common/vo/EnumScreenMuteType;ILcom/mstar/android/tvapi/common/vo/TvOsType$EnumInputSource;)Z
    :try_end_0
    .catch Lcom/mstar/android/tvapi/common/exception/TvCommonException; {:try_start_0 .. :try_end_0} :catch_0

    :cond_0
    :goto_0
    sget-object v2, Lcom/android/server/tv/DataBaseDesk$EN_ThreeD_Video_3DOUTPUTASPECT;->DB_ThreeD_Video_3DOUTPUTASPECT_CENTER:Lcom/android/server/tv/DataBaseDesk$EN_ThreeD_Video_3DOUTPUTASPECT;

    invoke-virtual {v2}, Lcom/android/server/tv/DataBaseDesk$EN_ThreeD_Video_3DOUTPUTASPECT;->ordinal()I

    move-result v2

    if-ne p1, v2, :cond_3

    sget-object v0, Lcom/mstar/android/tvapi/common/vo/Enum3dAspectRatioType;->E_3D_ASPECTRATIO_CENTER:Lcom/mstar/android/tvapi/common/vo/Enum3dAspectRatioType;

    :goto_1
    :try_start_1
    invoke-static {}, Lcom/mstar/android/tvapi/common/TvManager;->getInstance()Lcom/mstar/android/tvapi/common/TvManager;

    move-result-object v2

    if-eqz v2, :cond_1

    invoke-static {}, Lcom/mstar/android/tvapi/common/TvManager;->getInstance()Lcom/mstar/android/tvapi/common/TvManager;

    move-result-object v2

    invoke-virtual {v2}, Lcom/mstar/android/tvapi/common/TvManager;->getThreeDimensionManager()Lcom/mstar/android/tvapi/common/ThreeDimensionManager;

    move-result-object v2

    invoke-virtual {v2, v0}, Lcom/mstar/android/tvapi/common/ThreeDimensionManager;->set3dArc(Lcom/mstar/android/tvapi/common/vo/Enum3dAspectRatioType;)Z
    :try_end_1
    .catch Lcom/mstar/android/tvapi/common/exception/TvCommonException; {:try_start_1 .. :try_end_1} :catch_1

    :cond_1
    :goto_2
    :try_start_2
    invoke-static {}, Lcom/mstar/android/tvapi/common/TvManager;->getInstance()Lcom/mstar/android/tvapi/common/TvManager;

    move-result-object v2

    if-eqz v2, :cond_2

    invoke-static {}, Lcom/mstar/android/tvapi/common/TvManager;->getInstance()Lcom/mstar/android/tvapi/common/TvManager;

    move-result-object v2

    const/4 v3, 0x0

    sget-object v4, Lcom/mstar/android/tvapi/common/vo/EnumScreenMuteType;->E_BLACK:Lcom/mstar/android/tvapi/common/vo/EnumScreenMuteType;

    const/4 v5, 0x0

    invoke-static {}, Lcom/mstar/android/tvapi/common/vo/TvOsType$EnumInputSource;->values()[Lcom/mstar/android/tvapi/common/vo/TvOsType$EnumInputSource;

    move-result-object v6

    invoke-direct {p0}, Lcom/android/server/tv/TvS3DClient;->getCurrentInputSource()I

    move-result v7

    aget-object v6, v6, v7

    invoke-virtual {v2, v3, v4, v5, v6}, Lcom/mstar/android/tvapi/common/TvManager;->setVideoMute(ZLcom/mstar/android/tvapi/common/vo/EnumScreenMuteType;ILcom/mstar/android/tvapi/common/vo/TvOsType$EnumInputSource;)Z
    :try_end_2
    .catch Lcom/mstar/android/tvapi/common/exception/TvCommonException; {:try_start_2 .. :try_end_2} :catch_2

    :cond_2
    :goto_3
    return v8

    :catch_0
    move-exception v1

    invoke-virtual {v1}, Lcom/mstar/android/tvapi/common/exception/TvCommonException;->printStackTrace()V

    goto :goto_0

    :cond_3
    sget-object v2, Lcom/android/server/tv/DataBaseDesk$EN_ThreeD_Video_3DOUTPUTASPECT;->DB_ThreeD_Video_3DOUTPUTASPECT_AUTOADAPTED:Lcom/android/server/tv/DataBaseDesk$EN_ThreeD_Video_3DOUTPUTASPECT;

    invoke-virtual {v2}, Lcom/android/server/tv/DataBaseDesk$EN_ThreeD_Video_3DOUTPUTASPECT;->ordinal()I

    move-result v2

    if-ne p1, v2, :cond_4

    sget-object v0, Lcom/mstar/android/tvapi/common/vo/Enum3dAspectRatioType;->E_3D_ASPECTRATIO_AUTO:Lcom/mstar/android/tvapi/common/vo/Enum3dAspectRatioType;

    goto :goto_1

    :cond_4
    sget-object v0, Lcom/mstar/android/tvapi/common/vo/Enum3dAspectRatioType;->E_3D_ASPECTRATIO_FULL:Lcom/mstar/android/tvapi/common/vo/Enum3dAspectRatioType;

    goto :goto_1

    :catch_1
    move-exception v1

    invoke-virtual {v1}, Lcom/mstar/android/tvapi/common/exception/TvCommonException;->printStackTrace()V

    goto :goto_2

    :catch_2
    move-exception v1

    invoke-virtual {v1}, Lcom/mstar/android/tvapi/common/exception/TvCommonException;->printStackTrace()V

    goto :goto_3
.end method

.method public set3DTo2D(I)Z
    .locals 9
    .param p1    # I
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Landroid/os/RemoteException;
        }
    .end annotation

    const/4 v8, 0x1

    const-string v2, "TvS3DClient"

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string v4, "set3DTo2D, paras displayMode is "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3, p1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v2, v3}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    iget-object v2, p0, Lcom/android/server/tv/TvS3DClient;->db:Lcom/android/server/tv/DataBaseDeskImpl;

    sget-object v3, Lcom/android/server/tv/DataBaseDeskImpl$S3D_TABLE_FIELD;->eThreeDVideo3DTo2D:Lcom/android/server/tv/DataBaseDeskImpl$S3D_TABLE_FIELD;

    invoke-direct {p0}, Lcom/android/server/tv/TvS3DClient;->getCurrentInputSource()I

    move-result v4

    invoke-virtual {v2, v3, p1, v4}, Lcom/android/server/tv/DataBaseDeskImpl;->updateVideo3DMode(Lcom/android/server/tv/DataBaseDeskImpl$S3D_TABLE_FIELD;II)V

    :try_start_0
    invoke-static {}, Lcom/mstar/android/tvapi/common/TvManager;->getInstance()Lcom/mstar/android/tvapi/common/TvManager;

    move-result-object v2

    if-eqz v2, :cond_0

    invoke-static {}, Lcom/mstar/android/tvapi/common/TvManager;->getInstance()Lcom/mstar/android/tvapi/common/TvManager;

    move-result-object v2

    const/4 v3, 0x1

    sget-object v4, Lcom/mstar/android/tvapi/common/vo/EnumScreenMuteType;->E_BLACK:Lcom/mstar/android/tvapi/common/vo/EnumScreenMuteType;

    const/4 v5, 0x0

    invoke-static {}, Lcom/mstar/android/tvapi/common/vo/TvOsType$EnumInputSource;->values()[Lcom/mstar/android/tvapi/common/vo/TvOsType$EnumInputSource;

    move-result-object v6

    invoke-direct {p0}, Lcom/android/server/tv/TvS3DClient;->getCurrentInputSource()I

    move-result v7

    aget-object v6, v6, v7

    invoke-virtual {v2, v3, v4, v5, v6}, Lcom/mstar/android/tvapi/common/TvManager;->setVideoMute(ZLcom/mstar/android/tvapi/common/vo/EnumScreenMuteType;ILcom/mstar/android/tvapi/common/vo/TvOsType$EnumInputSource;)Z
    :try_end_0
    .catch Lcom/mstar/android/tvapi/common/exception/TvCommonException; {:try_start_0 .. :try_end_0} :catch_0

    :cond_0
    :goto_0
    sget-object v2, Lcom/android/server/tv/DataBaseDesk$EN_ThreeD_Video_3DTO2D;->DB_ThreeD_Video_3DTO2D_SIDE_BY_SIDE:Lcom/android/server/tv/DataBaseDesk$EN_ThreeD_Video_3DTO2D;

    invoke-virtual {v2}, Lcom/android/server/tv/DataBaseDesk$EN_ThreeD_Video_3DTO2D;->ordinal()I

    move-result v2

    if-ne p1, v2, :cond_3

    :try_start_1
    invoke-static {}, Lcom/mstar/android/tvapi/common/TvManager;->getInstance()Lcom/mstar/android/tvapi/common/TvManager;

    move-result-object v2

    if-eqz v2, :cond_1

    invoke-static {}, Lcom/mstar/android/tvapi/common/TvManager;->getInstance()Lcom/mstar/android/tvapi/common/TvManager;

    move-result-object v2

    invoke-virtual {v2}, Lcom/mstar/android/tvapi/common/TvManager;->getThreeDimensionManager()Lcom/mstar/android/tvapi/common/ThreeDimensionManager;

    move-result-object v2

    sget-object v3, Lcom/mstar/android/tvapi/common/vo/Enum3dType;->EN_3D_SIDE_BY_SIDE_HALF:Lcom/mstar/android/tvapi/common/vo/Enum3dType;

    invoke-virtual {v2, v3}, Lcom/mstar/android/tvapi/common/ThreeDimensionManager;->enable3dTo2d(Lcom/mstar/android/tvapi/common/vo/Enum3dType;)Z
    :try_end_1
    .catch Lcom/mstar/android/tvapi/common/exception/TvCommonException; {:try_start_1 .. :try_end_1} :catch_1

    :cond_1
    :goto_1
    :try_start_2
    invoke-static {}, Lcom/mstar/android/tvapi/common/TvManager;->getInstance()Lcom/mstar/android/tvapi/common/TvManager;

    move-result-object v2

    if-eqz v2, :cond_2

    invoke-static {}, Lcom/mstar/android/tvapi/common/TvManager;->getInstance()Lcom/mstar/android/tvapi/common/TvManager;

    move-result-object v2

    const/4 v3, 0x0

    sget-object v4, Lcom/mstar/android/tvapi/common/vo/EnumScreenMuteType;->E_BLACK:Lcom/mstar/android/tvapi/common/vo/EnumScreenMuteType;

    const/4 v5, 0x0

    invoke-static {}, Lcom/mstar/android/tvapi/common/vo/TvOsType$EnumInputSource;->values()[Lcom/mstar/android/tvapi/common/vo/TvOsType$EnumInputSource;

    move-result-object v6

    invoke-direct {p0}, Lcom/android/server/tv/TvS3DClient;->getCurrentInputSource()I

    move-result v7

    aget-object v6, v6, v7

    invoke-virtual {v2, v3, v4, v5, v6}, Lcom/mstar/android/tvapi/common/TvManager;->setVideoMute(ZLcom/mstar/android/tvapi/common/vo/EnumScreenMuteType;ILcom/mstar/android/tvapi/common/vo/TvOsType$EnumInputSource;)Z
    :try_end_2
    .catch Lcom/mstar/android/tvapi/common/exception/TvCommonException; {:try_start_2 .. :try_end_2} :catch_7

    :cond_2
    :goto_2
    return v8

    :catch_0
    move-exception v1

    invoke-virtual {v1}, Lcom/mstar/android/tvapi/common/exception/TvCommonException;->printStackTrace()V

    goto :goto_0

    :catch_1
    move-exception v1

    invoke-virtual {v1}, Lcom/mstar/android/tvapi/common/exception/TvCommonException;->printStackTrace()V

    goto :goto_1

    :cond_3
    sget-object v2, Lcom/android/server/tv/DataBaseDesk$EN_ThreeD_Video_3DTO2D;->DB_ThreeD_Video_3DTO2D_TOP_BOTTOM:Lcom/android/server/tv/DataBaseDesk$EN_ThreeD_Video_3DTO2D;

    invoke-virtual {v2}, Lcom/android/server/tv/DataBaseDesk$EN_ThreeD_Video_3DTO2D;->ordinal()I

    move-result v2

    if-ne p1, v2, :cond_4

    :try_start_3
    invoke-static {}, Lcom/mstar/android/tvapi/common/TvManager;->getInstance()Lcom/mstar/android/tvapi/common/TvManager;

    move-result-object v2

    if-eqz v2, :cond_1

    invoke-static {}, Lcom/mstar/android/tvapi/common/TvManager;->getInstance()Lcom/mstar/android/tvapi/common/TvManager;

    move-result-object v2

    invoke-virtual {v2}, Lcom/mstar/android/tvapi/common/TvManager;->getThreeDimensionManager()Lcom/mstar/android/tvapi/common/ThreeDimensionManager;

    move-result-object v2

    sget-object v3, Lcom/mstar/android/tvapi/common/vo/Enum3dType;->EN_3D_TOP_BOTTOM:Lcom/mstar/android/tvapi/common/vo/Enum3dType;

    invoke-virtual {v2, v3}, Lcom/mstar/android/tvapi/common/ThreeDimensionManager;->enable3dTo2d(Lcom/mstar/android/tvapi/common/vo/Enum3dType;)Z
    :try_end_3
    .catch Lcom/mstar/android/tvapi/common/exception/TvCommonException; {:try_start_3 .. :try_end_3} :catch_2

    goto :goto_1

    :catch_2
    move-exception v1

    invoke-virtual {v1}, Lcom/mstar/android/tvapi/common/exception/TvCommonException;->printStackTrace()V

    goto :goto_1

    :cond_4
    sget-object v2, Lcom/android/server/tv/DataBaseDesk$EN_ThreeD_Video_3DTO2D;->DB_ThreeD_Video_3DTO2D_FRAME_PACKING:Lcom/android/server/tv/DataBaseDesk$EN_ThreeD_Video_3DTO2D;

    invoke-virtual {v2}, Lcom/android/server/tv/DataBaseDesk$EN_ThreeD_Video_3DTO2D;->ordinal()I

    move-result v2

    if-ne p1, v2, :cond_5

    :try_start_4
    invoke-static {}, Lcom/mstar/android/tvapi/common/TvManager;->getInstance()Lcom/mstar/android/tvapi/common/TvManager;

    move-result-object v2

    if-eqz v2, :cond_1

    invoke-static {}, Lcom/mstar/android/tvapi/common/TvManager;->getInstance()Lcom/mstar/android/tvapi/common/TvManager;

    move-result-object v2

    invoke-virtual {v2}, Lcom/mstar/android/tvapi/common/TvManager;->getThreeDimensionManager()Lcom/mstar/android/tvapi/common/ThreeDimensionManager;

    move-result-object v2

    sget-object v3, Lcom/mstar/android/tvapi/common/vo/Enum3dType;->EN_3D_FRAME_PACKING_1080P:Lcom/mstar/android/tvapi/common/vo/Enum3dType;

    invoke-virtual {v2, v3}, Lcom/mstar/android/tvapi/common/ThreeDimensionManager;->enable3dTo2d(Lcom/mstar/android/tvapi/common/vo/Enum3dType;)Z
    :try_end_4
    .catch Lcom/mstar/android/tvapi/common/exception/TvCommonException; {:try_start_4 .. :try_end_4} :catch_3

    goto :goto_1

    :catch_3
    move-exception v1

    invoke-virtual {v1}, Lcom/mstar/android/tvapi/common/exception/TvCommonException;->printStackTrace()V

    goto :goto_1

    :cond_5
    sget-object v2, Lcom/android/server/tv/DataBaseDesk$EN_ThreeD_Video_3DTO2D;->DB_ThreeD_Video_3DTO2D_LINE_ALTERNATIVE:Lcom/android/server/tv/DataBaseDesk$EN_ThreeD_Video_3DTO2D;

    invoke-virtual {v2}, Lcom/android/server/tv/DataBaseDesk$EN_ThreeD_Video_3DTO2D;->ordinal()I

    move-result v2

    if-ne p1, v2, :cond_6

    :try_start_5
    invoke-static {}, Lcom/mstar/android/tvapi/common/TvManager;->getInstance()Lcom/mstar/android/tvapi/common/TvManager;

    move-result-object v2

    if-eqz v2, :cond_1

    invoke-static {}, Lcom/mstar/android/tvapi/common/TvManager;->getInstance()Lcom/mstar/android/tvapi/common/TvManager;

    move-result-object v2

    invoke-virtual {v2}, Lcom/mstar/android/tvapi/common/TvManager;->getThreeDimensionManager()Lcom/mstar/android/tvapi/common/ThreeDimensionManager;

    move-result-object v2

    sget-object v3, Lcom/mstar/android/tvapi/common/vo/Enum3dType;->EN_3D_LINE_ALTERNATIVE:Lcom/mstar/android/tvapi/common/vo/Enum3dType;

    invoke-virtual {v2, v3}, Lcom/mstar/android/tvapi/common/ThreeDimensionManager;->enable3dTo2d(Lcom/mstar/android/tvapi/common/vo/Enum3dType;)Z
    :try_end_5
    .catch Lcom/mstar/android/tvapi/common/exception/TvCommonException; {:try_start_5 .. :try_end_5} :catch_4

    goto/16 :goto_1

    :catch_4
    move-exception v1

    invoke-virtual {v1}, Lcom/mstar/android/tvapi/common/exception/TvCommonException;->printStackTrace()V

    goto/16 :goto_1

    :cond_6
    sget-object v2, Lcom/android/server/tv/DataBaseDesk$EN_ThreeD_Video_3DTO2D;->DB_ThreeD_Video_3DTO2D_AUTO:Lcom/android/server/tv/DataBaseDesk$EN_ThreeD_Video_3DTO2D;

    invoke-virtual {v2}, Lcom/android/server/tv/DataBaseDesk$EN_ThreeD_Video_3DTO2D;->ordinal()I

    move-result v2

    if-ne p1, v2, :cond_7

    invoke-direct {p0}, Lcom/android/server/tv/TvS3DClient;->isSignalStable()Z

    move-result v2

    if-eqz v2, :cond_1

    :try_start_6
    invoke-static {}, Lcom/mstar/android/tvapi/common/TvManager;->getInstance()Lcom/mstar/android/tvapi/common/TvManager;

    move-result-object v2

    if-eqz v2, :cond_1

    invoke-static {}, Lcom/mstar/android/tvapi/common/TvManager;->getInstance()Lcom/mstar/android/tvapi/common/TvManager;

    move-result-object v2

    invoke-virtual {v2}, Lcom/mstar/android/tvapi/common/TvManager;->getThreeDimensionManager()Lcom/mstar/android/tvapi/common/ThreeDimensionManager;

    move-result-object v2

    sget-object v3, Lcom/mstar/android/tvapi/common/vo/EnumScalerWindow;->E_MAIN_WINDOW:Lcom/mstar/android/tvapi/common/vo/EnumScalerWindow;

    invoke-virtual {v2, v3}, Lcom/mstar/android/tvapi/common/ThreeDimensionManager;->detect3dFormat(Lcom/mstar/android/tvapi/common/vo/EnumScalerWindow;)Lcom/mstar/android/tvapi/common/vo/Enum3dType;

    move-result-object v0

    invoke-static {}, Lcom/mstar/android/tvapi/common/TvManager;->getInstance()Lcom/mstar/android/tvapi/common/TvManager;

    move-result-object v2

    invoke-virtual {v2}, Lcom/mstar/android/tvapi/common/TvManager;->getThreeDimensionManager()Lcom/mstar/android/tvapi/common/ThreeDimensionManager;

    move-result-object v2

    invoke-virtual {v2, v0}, Lcom/mstar/android/tvapi/common/ThreeDimensionManager;->enable3dTo2d(Lcom/mstar/android/tvapi/common/vo/Enum3dType;)Z
    :try_end_6
    .catch Lcom/mstar/android/tvapi/common/exception/TvCommonException; {:try_start_6 .. :try_end_6} :catch_5

    goto/16 :goto_1

    :catch_5
    move-exception v1

    invoke-virtual {v1}, Lcom/mstar/android/tvapi/common/exception/TvCommonException;->printStackTrace()V

    goto/16 :goto_1

    :cond_7
    :try_start_7
    invoke-static {}, Lcom/mstar/android/tvapi/common/TvManager;->getInstance()Lcom/mstar/android/tvapi/common/TvManager;

    move-result-object v2

    if-eqz v2, :cond_1

    invoke-static {}, Lcom/mstar/android/tvapi/common/TvManager;->getInstance()Lcom/mstar/android/tvapi/common/TvManager;

    move-result-object v2

    invoke-virtual {v2}, Lcom/mstar/android/tvapi/common/TvManager;->getThreeDimensionManager()Lcom/mstar/android/tvapi/common/ThreeDimensionManager;

    move-result-object v2

    sget-object v3, Lcom/mstar/android/tvapi/common/vo/Enum3dType;->EN_3D_NONE:Lcom/mstar/android/tvapi/common/vo/Enum3dType;

    invoke-virtual {v2, v3}, Lcom/mstar/android/tvapi/common/ThreeDimensionManager;->enable3dTo2d(Lcom/mstar/android/tvapi/common/vo/Enum3dType;)Z
    :try_end_7
    .catch Lcom/mstar/android/tvapi/common/exception/TvCommonException; {:try_start_7 .. :try_end_7} :catch_6

    goto/16 :goto_1

    :catch_6
    move-exception v1

    invoke-virtual {v1}, Lcom/mstar/android/tvapi/common/exception/TvCommonException;->printStackTrace()V

    goto/16 :goto_1

    :catch_7
    move-exception v1

    invoke-virtual {v1}, Lcom/mstar/android/tvapi/common/exception/TvCommonException;->printStackTrace()V

    goto/16 :goto_2
.end method

.method public setAutoStartMode(I)Z
    .locals 3
    .param p1    # I
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Landroid/os/RemoteException;
        }
    .end annotation

    const-string v0, "TvS3DClient"

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "setAutoStartMode, paras autoStartMode is "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    iget-object v0, p0, Lcom/android/server/tv/TvS3DClient;->db:Lcom/android/server/tv/DataBaseDeskImpl;

    sget-object v1, Lcom/android/server/tv/DataBaseDeskImpl$S3D_TABLE_FIELD;->eThreeDVideoAutoStart:Lcom/android/server/tv/DataBaseDeskImpl$S3D_TABLE_FIELD;

    invoke-direct {p0}, Lcom/android/server/tv/TvS3DClient;->getCurrentInputSource()I

    move-result v2

    invoke-virtual {v0, v1, p1, v2}, Lcom/android/server/tv/DataBaseDeskImpl;->updateVideo3DMode(Lcom/android/server/tv/DataBaseDeskImpl$S3D_TABLE_FIELD;II)V

    const/4 v0, 0x1

    return v0
.end method

.method public setDisplayFormat(I)Z
    .locals 9
    .param p1    # I
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Landroid/os/RemoteException;
        }
    .end annotation

    const/4 v8, 0x1

    const-string v2, "TvS3DClient"

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string v4, "----->setDisplayFormat, paras displayFormat is "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3, p1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v2, v3}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    iget-object v2, p0, Lcom/android/server/tv/TvS3DClient;->db:Lcom/android/server/tv/DataBaseDeskImpl;

    sget-object v3, Lcom/android/server/tv/DataBaseDeskImpl$S3D_TABLE_FIELD;->eThreeDVideoDisplayFormat:Lcom/android/server/tv/DataBaseDeskImpl$S3D_TABLE_FIELD;

    invoke-direct {p0}, Lcom/android/server/tv/TvS3DClient;->getCurrentInputSource()I

    move-result v4

    invoke-virtual {v2, v3, p1, v4}, Lcom/android/server/tv/DataBaseDeskImpl;->updateVideo3DMode(Lcom/android/server/tv/DataBaseDeskImpl$S3D_TABLE_FIELD;II)V

    :try_start_0
    invoke-static {}, Lcom/mstar/android/tvapi/common/TvManager;->getInstance()Lcom/mstar/android/tvapi/common/TvManager;

    move-result-object v2

    if-eqz v2, :cond_0

    invoke-static {}, Lcom/mstar/android/tvapi/common/TvManager;->getInstance()Lcom/mstar/android/tvapi/common/TvManager;

    move-result-object v2

    const/4 v3, 0x1

    sget-object v4, Lcom/mstar/android/tvapi/common/vo/EnumScreenMuteType;->E_BLACK:Lcom/mstar/android/tvapi/common/vo/EnumScreenMuteType;

    const/4 v5, 0x0

    invoke-static {}, Lcom/mstar/android/tvapi/common/vo/TvOsType$EnumInputSource;->values()[Lcom/mstar/android/tvapi/common/vo/TvOsType$EnumInputSource;

    move-result-object v6

    invoke-direct {p0}, Lcom/android/server/tv/TvS3DClient;->getCurrentInputSource()I

    move-result v7

    aget-object v6, v6, v7

    invoke-virtual {v2, v3, v4, v5, v6}, Lcom/mstar/android/tvapi/common/TvManager;->setVideoMute(ZLcom/mstar/android/tvapi/common/vo/EnumScreenMuteType;ILcom/mstar/android/tvapi/common/vo/TvOsType$EnumInputSource;)Z
    :try_end_0
    .catch Lcom/mstar/android/tvapi/common/exception/TvCommonException; {:try_start_0 .. :try_end_0} :catch_0

    :cond_0
    :goto_0
    sget-object v2, Lcom/android/server/tv/DataBaseDesk$EN_ThreeD_Video_DISPLAYFORMAT;->DB_ThreeD_Video_DISPLAYFORMAT_SIDE_BY_SIDE:Lcom/android/server/tv/DataBaseDesk$EN_ThreeD_Video_DISPLAYFORMAT;

    invoke-virtual {v2}, Lcom/android/server/tv/DataBaseDesk$EN_ThreeD_Video_DISPLAYFORMAT;->ordinal()I

    move-result v2

    if-ne p1, v2, :cond_3

    :try_start_1
    invoke-static {}, Lcom/mstar/android/tvapi/common/TvManager;->getInstance()Lcom/mstar/android/tvapi/common/TvManager;

    move-result-object v2

    if-eqz v2, :cond_1

    invoke-static {}, Lcom/mstar/android/tvapi/common/TvManager;->getInstance()Lcom/mstar/android/tvapi/common/TvManager;

    move-result-object v2

    invoke-virtual {v2}, Lcom/mstar/android/tvapi/common/TvManager;->getThreeDimensionManager()Lcom/mstar/android/tvapi/common/ThreeDimensionManager;

    move-result-object v2

    sget-object v3, Lcom/mstar/android/tvapi/common/vo/Enum3dType;->EN_3D_SIDE_BY_SIDE_HALF:Lcom/mstar/android/tvapi/common/vo/Enum3dType;

    invoke-virtual {v2, v3}, Lcom/mstar/android/tvapi/common/ThreeDimensionManager;->enable3d(Lcom/mstar/android/tvapi/common/vo/Enum3dType;)Z
    :try_end_1
    .catch Lcom/mstar/android/tvapi/common/exception/TvCommonException; {:try_start_1 .. :try_end_1} :catch_1

    :cond_1
    :goto_1
    :try_start_2
    invoke-static {}, Lcom/mstar/android/tvapi/common/TvManager;->getInstance()Lcom/mstar/android/tvapi/common/TvManager;

    move-result-object v2

    if-eqz v2, :cond_2

    const-string v2, "TvS3DClient"

    const-string v3, "------------>EN_ThreeD_Video_DISPLAYFORMAT is E_BLACK"

    invoke-static {v2, v3}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    invoke-static {}, Lcom/mstar/android/tvapi/common/TvManager;->getInstance()Lcom/mstar/android/tvapi/common/TvManager;

    move-result-object v2

    const/4 v3, 0x0

    sget-object v4, Lcom/mstar/android/tvapi/common/vo/EnumScreenMuteType;->E_BLACK:Lcom/mstar/android/tvapi/common/vo/EnumScreenMuteType;

    const/4 v5, 0x0

    invoke-static {}, Lcom/mstar/android/tvapi/common/vo/TvOsType$EnumInputSource;->values()[Lcom/mstar/android/tvapi/common/vo/TvOsType$EnumInputSource;

    move-result-object v6

    invoke-direct {p0}, Lcom/android/server/tv/TvS3DClient;->getCurrentInputSource()I

    move-result v7

    aget-object v6, v6, v7

    invoke-virtual {v2, v3, v4, v5, v6}, Lcom/mstar/android/tvapi/common/TvManager;->setVideoMute(ZLcom/mstar/android/tvapi/common/vo/EnumScreenMuteType;ILcom/mstar/android/tvapi/common/vo/TvOsType$EnumInputSource;)Z
    :try_end_2
    .catch Lcom/mstar/android/tvapi/common/exception/TvCommonException; {:try_start_2 .. :try_end_2} :catch_b

    :cond_2
    :goto_2
    return v8

    :catch_0
    move-exception v1

    invoke-virtual {v1}, Lcom/mstar/android/tvapi/common/exception/TvCommonException;->printStackTrace()V

    goto :goto_0

    :catch_1
    move-exception v1

    invoke-virtual {v1}, Lcom/mstar/android/tvapi/common/exception/TvCommonException;->printStackTrace()V

    goto :goto_1

    :cond_3
    sget-object v2, Lcom/android/server/tv/DataBaseDesk$EN_ThreeD_Video_DISPLAYFORMAT;->DB_ThreeD_Video_DISPLAYFORMAT_TOP_BOTTOM:Lcom/android/server/tv/DataBaseDesk$EN_ThreeD_Video_DISPLAYFORMAT;

    invoke-virtual {v2}, Lcom/android/server/tv/DataBaseDesk$EN_ThreeD_Video_DISPLAYFORMAT;->ordinal()I

    move-result v2

    if-ne p1, v2, :cond_4

    :try_start_3
    invoke-static {}, Lcom/mstar/android/tvapi/common/TvManager;->getInstance()Lcom/mstar/android/tvapi/common/TvManager;

    move-result-object v2

    if-eqz v2, :cond_1

    invoke-static {}, Lcom/mstar/android/tvapi/common/TvManager;->getInstance()Lcom/mstar/android/tvapi/common/TvManager;

    move-result-object v2

    invoke-virtual {v2}, Lcom/mstar/android/tvapi/common/TvManager;->getThreeDimensionManager()Lcom/mstar/android/tvapi/common/ThreeDimensionManager;

    move-result-object v2

    sget-object v3, Lcom/mstar/android/tvapi/common/vo/Enum3dType;->EN_3D_TOP_BOTTOM:Lcom/mstar/android/tvapi/common/vo/Enum3dType;

    invoke-virtual {v2, v3}, Lcom/mstar/android/tvapi/common/ThreeDimensionManager;->enable3d(Lcom/mstar/android/tvapi/common/vo/Enum3dType;)Z
    :try_end_3
    .catch Lcom/mstar/android/tvapi/common/exception/TvCommonException; {:try_start_3 .. :try_end_3} :catch_2

    goto :goto_1

    :catch_2
    move-exception v1

    invoke-virtual {v1}, Lcom/mstar/android/tvapi/common/exception/TvCommonException;->printStackTrace()V

    goto :goto_1

    :cond_4
    sget-object v2, Lcom/android/server/tv/DataBaseDesk$EN_ThreeD_Video_DISPLAYFORMAT;->DB_ThreeD_Video_DISPLAYFORMAT_FRAME_PACKING:Lcom/android/server/tv/DataBaseDesk$EN_ThreeD_Video_DISPLAYFORMAT;

    invoke-virtual {v2}, Lcom/android/server/tv/DataBaseDesk$EN_ThreeD_Video_DISPLAYFORMAT;->ordinal()I

    move-result v2

    if-ne p1, v2, :cond_5

    :try_start_4
    invoke-static {}, Lcom/mstar/android/tvapi/common/TvManager;->getInstance()Lcom/mstar/android/tvapi/common/TvManager;

    move-result-object v2

    if-eqz v2, :cond_1

    invoke-static {}, Lcom/mstar/android/tvapi/common/TvManager;->getInstance()Lcom/mstar/android/tvapi/common/TvManager;

    move-result-object v2

    invoke-virtual {v2}, Lcom/mstar/android/tvapi/common/TvManager;->getThreeDimensionManager()Lcom/mstar/android/tvapi/common/ThreeDimensionManager;

    move-result-object v2

    sget-object v3, Lcom/mstar/android/tvapi/common/vo/Enum3dType;->EN_3D_FRAME_PACKING_1080P:Lcom/mstar/android/tvapi/common/vo/Enum3dType;

    invoke-virtual {v2, v3}, Lcom/mstar/android/tvapi/common/ThreeDimensionManager;->enable3d(Lcom/mstar/android/tvapi/common/vo/Enum3dType;)Z
    :try_end_4
    .catch Lcom/mstar/android/tvapi/common/exception/TvCommonException; {:try_start_4 .. :try_end_4} :catch_3

    goto :goto_1

    :catch_3
    move-exception v1

    invoke-virtual {v1}, Lcom/mstar/android/tvapi/common/exception/TvCommonException;->printStackTrace()V

    goto :goto_1

    :cond_5
    sget-object v2, Lcom/android/server/tv/DataBaseDesk$EN_ThreeD_Video_DISPLAYFORMAT;->DB_ThreeD_Video_DISPLAYFORMAT_LINE_ALTERNATIVE:Lcom/android/server/tv/DataBaseDesk$EN_ThreeD_Video_DISPLAYFORMAT;

    invoke-virtual {v2}, Lcom/android/server/tv/DataBaseDesk$EN_ThreeD_Video_DISPLAYFORMAT;->ordinal()I

    move-result v2

    if-ne p1, v2, :cond_6

    :try_start_5
    invoke-static {}, Lcom/mstar/android/tvapi/common/TvManager;->getInstance()Lcom/mstar/android/tvapi/common/TvManager;

    move-result-object v2

    if-eqz v2, :cond_1

    invoke-static {}, Lcom/mstar/android/tvapi/common/TvManager;->getInstance()Lcom/mstar/android/tvapi/common/TvManager;

    move-result-object v2

    invoke-virtual {v2}, Lcom/mstar/android/tvapi/common/TvManager;->getThreeDimensionManager()Lcom/mstar/android/tvapi/common/ThreeDimensionManager;

    move-result-object v2

    sget-object v3, Lcom/mstar/android/tvapi/common/vo/Enum3dType;->EN_3D_LINE_ALTERNATIVE:Lcom/mstar/android/tvapi/common/vo/Enum3dType;

    invoke-virtual {v2, v3}, Lcom/mstar/android/tvapi/common/ThreeDimensionManager;->enable3d(Lcom/mstar/android/tvapi/common/vo/Enum3dType;)Z
    :try_end_5
    .catch Lcom/mstar/android/tvapi/common/exception/TvCommonException; {:try_start_5 .. :try_end_5} :catch_4

    goto/16 :goto_1

    :catch_4
    move-exception v1

    invoke-virtual {v1}, Lcom/mstar/android/tvapi/common/exception/TvCommonException;->printStackTrace()V

    goto/16 :goto_1

    :cond_6
    sget-object v2, Lcom/android/server/tv/DataBaseDesk$EN_ThreeD_Video_DISPLAYFORMAT;->DB_ThreeD_Video_DISPLAYFORMAT_2DTO3D:Lcom/android/server/tv/DataBaseDesk$EN_ThreeD_Video_DISPLAYFORMAT;

    invoke-virtual {v2}, Lcom/android/server/tv/DataBaseDesk$EN_ThreeD_Video_DISPLAYFORMAT;->ordinal()I

    move-result v2

    if-ne p1, v2, :cond_7

    :try_start_6
    invoke-static {}, Lcom/mstar/android/tvapi/common/TvManager;->getInstance()Lcom/mstar/android/tvapi/common/TvManager;

    move-result-object v2

    if-eqz v2, :cond_1

    invoke-static {}, Lcom/mstar/android/tvapi/common/TvManager;->getInstance()Lcom/mstar/android/tvapi/common/TvManager;

    move-result-object v2

    invoke-virtual {v2}, Lcom/mstar/android/tvapi/common/TvManager;->getThreeDimensionManager()Lcom/mstar/android/tvapi/common/ThreeDimensionManager;

    move-result-object v2

    sget-object v3, Lcom/mstar/android/tvapi/common/vo/Enum3dType;->EN_3D_2DTO3D:Lcom/mstar/android/tvapi/common/vo/Enum3dType;

    invoke-virtual {v2, v3}, Lcom/mstar/android/tvapi/common/ThreeDimensionManager;->enable3d(Lcom/mstar/android/tvapi/common/vo/Enum3dType;)Z
    :try_end_6
    .catch Lcom/mstar/android/tvapi/common/exception/TvCommonException; {:try_start_6 .. :try_end_6} :catch_5

    goto/16 :goto_1

    :catch_5
    move-exception v1

    invoke-virtual {v1}, Lcom/mstar/android/tvapi/common/exception/TvCommonException;->printStackTrace()V

    goto/16 :goto_1

    :cond_7
    sget-object v2, Lcom/android/server/tv/DataBaseDesk$EN_ThreeD_Video_DISPLAYFORMAT;->DB_ThreeD_Video_DISPLAYFORMAT_AUTO:Lcom/android/server/tv/DataBaseDesk$EN_ThreeD_Video_DISPLAYFORMAT;

    invoke-virtual {v2}, Lcom/android/server/tv/DataBaseDesk$EN_ThreeD_Video_DISPLAYFORMAT;->ordinal()I

    move-result v2

    if-ne p1, v2, :cond_8

    invoke-direct {p0}, Lcom/android/server/tv/TvS3DClient;->isSignalStable()Z

    move-result v2

    if-eqz v2, :cond_1

    :try_start_7
    invoke-static {}, Lcom/mstar/android/tvapi/common/TvManager;->getInstance()Lcom/mstar/android/tvapi/common/TvManager;

    move-result-object v2

    if-eqz v2, :cond_1

    invoke-static {}, Lcom/mstar/android/tvapi/common/TvManager;->getInstance()Lcom/mstar/android/tvapi/common/TvManager;

    move-result-object v2

    invoke-virtual {v2}, Lcom/mstar/android/tvapi/common/TvManager;->getThreeDimensionManager()Lcom/mstar/android/tvapi/common/ThreeDimensionManager;

    move-result-object v2

    sget-object v3, Lcom/mstar/android/tvapi/common/vo/EnumScalerWindow;->E_MAIN_WINDOW:Lcom/mstar/android/tvapi/common/vo/EnumScalerWindow;

    invoke-virtual {v2, v3}, Lcom/mstar/android/tvapi/common/ThreeDimensionManager;->detect3dFormat(Lcom/mstar/android/tvapi/common/vo/EnumScalerWindow;)Lcom/mstar/android/tvapi/common/vo/Enum3dType;

    move-result-object v0

    invoke-static {}, Lcom/mstar/android/tvapi/common/TvManager;->getInstance()Lcom/mstar/android/tvapi/common/TvManager;

    move-result-object v2

    invoke-virtual {v2}, Lcom/mstar/android/tvapi/common/TvManager;->getThreeDimensionManager()Lcom/mstar/android/tvapi/common/ThreeDimensionManager;

    move-result-object v2

    invoke-virtual {v2, v0}, Lcom/mstar/android/tvapi/common/ThreeDimensionManager;->enable3d(Lcom/mstar/android/tvapi/common/vo/Enum3dType;)Z
    :try_end_7
    .catch Lcom/mstar/android/tvapi/common/exception/TvCommonException; {:try_start_7 .. :try_end_7} :catch_6

    goto/16 :goto_1

    :catch_6
    move-exception v1

    invoke-virtual {v1}, Lcom/mstar/android/tvapi/common/exception/TvCommonException;->printStackTrace()V

    goto/16 :goto_1

    :cond_8
    sget-object v2, Lcom/android/server/tv/DataBaseDesk$EN_ThreeD_Video_DISPLAYFORMAT;->DB_ThreeD_Video_DISPLAYFORMAT_CHECK_BOARD:Lcom/android/server/tv/DataBaseDesk$EN_ThreeD_Video_DISPLAYFORMAT;

    invoke-virtual {v2}, Lcom/android/server/tv/DataBaseDesk$EN_ThreeD_Video_DISPLAYFORMAT;->ordinal()I

    move-result v2

    if-ne p1, v2, :cond_9

    :try_start_8
    invoke-static {}, Lcom/mstar/android/tvapi/common/TvManager;->getInstance()Lcom/mstar/android/tvapi/common/TvManager;

    move-result-object v2

    if-eqz v2, :cond_1

    const-string v2, "TvS3DClient"

    const-string v3, "------------>EN_ThreeD_Video_DISPLAYFORMAT is EN_3D_CHECK_BORAD"

    invoke-static {v2, v3}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    invoke-static {}, Lcom/mstar/android/tvapi/common/TvManager;->getInstance()Lcom/mstar/android/tvapi/common/TvManager;

    move-result-object v2

    invoke-virtual {v2}, Lcom/mstar/android/tvapi/common/TvManager;->getThreeDimensionManager()Lcom/mstar/android/tvapi/common/ThreeDimensionManager;

    move-result-object v2

    sget-object v3, Lcom/mstar/android/tvapi/common/vo/Enum3dType;->EN_3D_CHECK_BORAD:Lcom/mstar/android/tvapi/common/vo/Enum3dType;

    invoke-virtual {v2, v3}, Lcom/mstar/android/tvapi/common/ThreeDimensionManager;->enable3d(Lcom/mstar/android/tvapi/common/vo/Enum3dType;)Z
    :try_end_8
    .catch Lcom/mstar/android/tvapi/common/exception/TvCommonException; {:try_start_8 .. :try_end_8} :catch_7

    goto/16 :goto_1

    :catch_7
    move-exception v1

    invoke-virtual {v1}, Lcom/mstar/android/tvapi/common/exception/TvCommonException;->printStackTrace()V

    goto/16 :goto_1

    :cond_9
    sget-object v2, Lcom/android/server/tv/DataBaseDesk$EN_ThreeD_Video_DISPLAYFORMAT;->DB_ThreeD_Video_DISPLAYFORMAT_PIXEL_ALTERNATIVE:Lcom/android/server/tv/DataBaseDesk$EN_ThreeD_Video_DISPLAYFORMAT;

    invoke-virtual {v2}, Lcom/android/server/tv/DataBaseDesk$EN_ThreeD_Video_DISPLAYFORMAT;->ordinal()I

    move-result v2

    if-ne p1, v2, :cond_a

    :try_start_9
    invoke-static {}, Lcom/mstar/android/tvapi/common/TvManager;->getInstance()Lcom/mstar/android/tvapi/common/TvManager;

    move-result-object v2

    if-eqz v2, :cond_1

    const-string v2, "TvS3DClient"

    const-string v3, "------------>EN_ThreeD_Video_DISPLAYFORMAT is EN_3D_PIXEL_ALTERNATIVE"

    invoke-static {v2, v3}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    invoke-static {}, Lcom/mstar/android/tvapi/common/TvManager;->getInstance()Lcom/mstar/android/tvapi/common/TvManager;

    move-result-object v2

    invoke-virtual {v2}, Lcom/mstar/android/tvapi/common/TvManager;->getThreeDimensionManager()Lcom/mstar/android/tvapi/common/ThreeDimensionManager;

    move-result-object v2

    sget-object v3, Lcom/mstar/android/tvapi/common/vo/Enum3dType;->EN_3D_PIXEL_ALTERNATIVE:Lcom/mstar/android/tvapi/common/vo/Enum3dType;

    invoke-virtual {v2, v3}, Lcom/mstar/android/tvapi/common/ThreeDimensionManager;->enable3d(Lcom/mstar/android/tvapi/common/vo/Enum3dType;)Z
    :try_end_9
    .catch Lcom/mstar/android/tvapi/common/exception/TvCommonException; {:try_start_9 .. :try_end_9} :catch_8

    goto/16 :goto_1

    :catch_8
    move-exception v1

    invoke-virtual {v1}, Lcom/mstar/android/tvapi/common/exception/TvCommonException;->printStackTrace()V

    goto/16 :goto_1

    :cond_a
    sget-object v2, Lcom/android/server/tv/DataBaseDesk$EN_ThreeD_Video_DISPLAYFORMAT;->DB_ThreeD_Video_DISPLAYFORMAT_FRAME_ALTERNATIVE:Lcom/android/server/tv/DataBaseDesk$EN_ThreeD_Video_DISPLAYFORMAT;

    invoke-virtual {v2}, Lcom/android/server/tv/DataBaseDesk$EN_ThreeD_Video_DISPLAYFORMAT;->ordinal()I

    move-result v2

    if-ne p1, v2, :cond_b

    :try_start_a
    invoke-static {}, Lcom/mstar/android/tvapi/common/TvManager;->getInstance()Lcom/mstar/android/tvapi/common/TvManager;

    move-result-object v2

    if-eqz v2, :cond_1

    const-string v2, "TvS3DClient"

    const-string v3, "------------>EN_ThreeD_Video_DISPLAYFORMAT is EN_3D_FRAME_ALTERNATIVE"

    invoke-static {v2, v3}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    invoke-static {}, Lcom/mstar/android/tvapi/common/TvManager;->getInstance()Lcom/mstar/android/tvapi/common/TvManager;

    move-result-object v2

    invoke-virtual {v2}, Lcom/mstar/android/tvapi/common/TvManager;->getThreeDimensionManager()Lcom/mstar/android/tvapi/common/ThreeDimensionManager;

    move-result-object v2

    sget-object v3, Lcom/mstar/android/tvapi/common/vo/Enum3dType;->EN_3D_FRAME_ALTERNATIVE:Lcom/mstar/android/tvapi/common/vo/Enum3dType;

    invoke-virtual {v2, v3}, Lcom/mstar/android/tvapi/common/ThreeDimensionManager;->enable3d(Lcom/mstar/android/tvapi/common/vo/Enum3dType;)Z
    :try_end_a
    .catch Lcom/mstar/android/tvapi/common/exception/TvCommonException; {:try_start_a .. :try_end_a} :catch_9

    goto/16 :goto_1

    :catch_9
    move-exception v1

    invoke-virtual {v1}, Lcom/mstar/android/tvapi/common/exception/TvCommonException;->printStackTrace()V

    goto/16 :goto_1

    :cond_b
    const-string v2, "TvS3DClient"

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string v4, "----->setDisplayFormat, EN_ThreeD_Video_DISPLAYFORMAT is "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    sget-object v4, Lcom/android/server/tv/DataBaseDesk$EN_ThreeD_Video_DISPLAYFORMAT;->DB_ThreeD_Video_DISPLAYFORMAT_FRAME_ALTERNATIVE:Lcom/android/server/tv/DataBaseDesk$EN_ThreeD_Video_DISPLAYFORMAT;

    invoke-virtual {v4}, Lcom/android/server/tv/DataBaseDesk$EN_ThreeD_Video_DISPLAYFORMAT;->ordinal()I

    move-result v4

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v2, v3}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    :try_start_b
    invoke-static {}, Lcom/mstar/android/tvapi/common/TvManager;->getInstance()Lcom/mstar/android/tvapi/common/TvManager;

    move-result-object v2

    if-eqz v2, :cond_1

    const-string v2, "TvS3DClient"

    const-string v3, "------------>EN_ThreeD_Video_DISPLAYFORMAT is EN_3D_NONE"

    invoke-static {v2, v3}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    invoke-static {}, Lcom/mstar/android/tvapi/common/TvManager;->getInstance()Lcom/mstar/android/tvapi/common/TvManager;

    move-result-object v2

    invoke-virtual {v2}, Lcom/mstar/android/tvapi/common/TvManager;->getThreeDimensionManager()Lcom/mstar/android/tvapi/common/ThreeDimensionManager;

    move-result-object v2

    sget-object v3, Lcom/mstar/android/tvapi/common/vo/Enum3dType;->EN_3D_NONE:Lcom/mstar/android/tvapi/common/vo/Enum3dType;

    invoke-virtual {v2, v3}, Lcom/mstar/android/tvapi/common/ThreeDimensionManager;->enable3d(Lcom/mstar/android/tvapi/common/vo/Enum3dType;)Z
    :try_end_b
    .catch Lcom/mstar/android/tvapi/common/exception/TvCommonException; {:try_start_b .. :try_end_b} :catch_a

    goto/16 :goto_1

    :catch_a
    move-exception v1

    invoke-virtual {v1}, Lcom/mstar/android/tvapi/common/exception/TvCommonException;->printStackTrace()V

    goto/16 :goto_1

    :catch_b
    move-exception v1

    invoke-virtual {v1}, Lcom/mstar/android/tvapi/common/exception/TvCommonException;->printStackTrace()V

    goto/16 :goto_2
.end method

.method public setDisplayFormatForUI(I)V
    .locals 3
    .param p1    # I
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Landroid/os/RemoteException;
        }
    .end annotation

    iget-object v0, p0, Lcom/android/server/tv/TvS3DClient;->db:Lcom/android/server/tv/DataBaseDeskImpl;

    sget-object v1, Lcom/android/server/tv/DataBaseDeskImpl$S3D_TABLE_FIELD;->eThreeDVideoDisplayFormat:Lcom/android/server/tv/DataBaseDeskImpl$S3D_TABLE_FIELD;

    invoke-direct {p0}, Lcom/android/server/tv/TvS3DClient;->getCurrentInputSource()I

    move-result v2

    invoke-virtual {v0, v1, p1, v2}, Lcom/android/server/tv/DataBaseDeskImpl;->updateVideo3DMode(Lcom/android/server/tv/DataBaseDeskImpl$S3D_TABLE_FIELD;II)V

    return-void
.end method

.method public setLrViewSwitch(I)Z
    .locals 4
    .param p1    # I
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Landroid/os/RemoteException;
        }
    .end annotation

    const-string v1, "TvS3DClient"

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "setLrViewSwitch, paras LrViewSwitchMode is "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, p1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    iget-object v1, p0, Lcom/android/server/tv/TvS3DClient;->db:Lcom/android/server/tv/DataBaseDeskImpl;

    sget-object v2, Lcom/android/server/tv/DataBaseDeskImpl$S3D_TABLE_FIELD;->eThreeDVideoLRViewSwitch:Lcom/android/server/tv/DataBaseDeskImpl$S3D_TABLE_FIELD;

    invoke-direct {p0}, Lcom/android/server/tv/TvS3DClient;->getCurrentInputSource()I

    move-result v3

    invoke-virtual {v1, v2, p1, v3}, Lcom/android/server/tv/DataBaseDeskImpl;->updateVideo3DMode(Lcom/android/server/tv/DataBaseDeskImpl$S3D_TABLE_FIELD;II)V

    :try_start_0
    invoke-static {}, Lcom/mstar/android/tvapi/common/TvManager;->getInstance()Lcom/mstar/android/tvapi/common/TvManager;

    move-result-object v1

    if-eqz v1, :cond_0

    invoke-static {}, Lcom/mstar/android/tvapi/common/TvManager;->getInstance()Lcom/mstar/android/tvapi/common/TvManager;

    move-result-object v1

    invoke-virtual {v1}, Lcom/mstar/android/tvapi/common/TvManager;->getThreeDimensionManager()Lcom/mstar/android/tvapi/common/ThreeDimensionManager;

    move-result-object v1

    invoke-virtual {v1}, Lcom/mstar/android/tvapi/common/ThreeDimensionManager;->enable3dLrSwitch()Z
    :try_end_0
    .catch Lcom/mstar/android/tvapi/common/exception/TvCommonException; {:try_start_0 .. :try_end_0} :catch_0

    :cond_0
    :goto_0
    const/4 v1, 0x1

    return v1

    :catch_0
    move-exception v0

    invoke-virtual {v0}, Lcom/mstar/android/tvapi/common/exception/TvCommonException;->printStackTrace()V

    goto :goto_0
.end method

.method public setSelfAdaptiveDetect(I)Z
    .locals 10
    .param p1    # I
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Landroid/os/RemoteException;
        }
    .end annotation

    const/4 v9, 0x1

    sget-object v1, Lcom/mstar/android/tvapi/common/vo/Enum3dType;->EN_3D_NONE:Lcom/mstar/android/tvapi/common/vo/Enum3dType;

    const-string v3, "TvS3DClient"

    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    const-string v5, "setSelfAdaptiveDetect, paras selfAdaptiveDetect is "

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4, p1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-static {v3, v4}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    :try_start_0
    invoke-static {}, Lcom/mstar/android/tvapi/common/TvManager;->getInstance()Lcom/mstar/android/tvapi/common/TvManager;

    move-result-object v3

    if-eqz v3, :cond_0

    invoke-static {}, Lcom/mstar/android/tvapi/common/TvManager;->getInstance()Lcom/mstar/android/tvapi/common/TvManager;

    move-result-object v3

    invoke-virtual {v3}, Lcom/mstar/android/tvapi/common/TvManager;->getThreeDimensionManager()Lcom/mstar/android/tvapi/common/ThreeDimensionManager;

    move-result-object v3

    invoke-virtual {v3}, Lcom/mstar/android/tvapi/common/ThreeDimensionManager;->getCurrent3dFormat()Lcom/mstar/android/tvapi/common/vo/Enum3dType;
    :try_end_0
    .catch Lcom/mstar/android/tvapi/common/exception/TvCommonException; {:try_start_0 .. :try_end_0} :catch_0

    move-result-object v1

    :cond_0
    :goto_0
    sget-object v3, Lcom/android/server/tv/DataBaseDesk$EN_ThreeD_Video_SELFADAPTIVE_DETECT;->DB_ThreeD_Video_SELF_ADAPTIVE_DETECT_OFF:Lcom/android/server/tv/DataBaseDesk$EN_ThreeD_Video_SELFADAPTIVE_DETECT;

    invoke-virtual {v3}, Lcom/android/server/tv/DataBaseDesk$EN_ThreeD_Video_SELFADAPTIVE_DETECT;->ordinal()I

    move-result v3

    if-ne p1, v3, :cond_2

    iget-object v3, p0, Lcom/android/server/tv/TvS3DClient;->db:Lcom/android/server/tv/DataBaseDeskImpl;

    sget-object v4, Lcom/android/server/tv/DataBaseDeskImpl$S3D_TABLE_FIELD;->eThreeDVideoSelfAdaptiveDetect:Lcom/android/server/tv/DataBaseDeskImpl$S3D_TABLE_FIELD;

    sget-object v5, Lcom/mstar/android/tvapi/common/vo/TvOsType$EnumInputSource;->E_INPUT_SOURCE_HDMI:Lcom/mstar/android/tvapi/common/vo/TvOsType$EnumInputSource;

    invoke-virtual {v5}, Lcom/mstar/android/tvapi/common/vo/TvOsType$EnumInputSource;->ordinal()I

    move-result v5

    invoke-virtual {v3, v4, p1, v5}, Lcom/android/server/tv/DataBaseDeskImpl;->updateVideo3DMode(Lcom/android/server/tv/DataBaseDeskImpl$S3D_TABLE_FIELD;II)V

    iget-object v3, p0, Lcom/android/server/tv/TvS3DClient;->db:Lcom/android/server/tv/DataBaseDeskImpl;

    sget-object v4, Lcom/android/server/tv/DataBaseDeskImpl$S3D_TABLE_FIELD;->eThreeDVideoDisplayFormat:Lcom/android/server/tv/DataBaseDeskImpl$S3D_TABLE_FIELD;

    sget-object v5, Lcom/android/server/tv/DataBaseDesk$EN_ThreeD_Video_DISPLAYFORMAT;->DB_ThreeD_Video_DISPLAYFORMAT_NONE:Lcom/android/server/tv/DataBaseDesk$EN_ThreeD_Video_DISPLAYFORMAT;

    invoke-virtual {v5}, Lcom/android/server/tv/DataBaseDesk$EN_ThreeD_Video_DISPLAYFORMAT;->ordinal()I

    move-result v5

    invoke-direct {p0}, Lcom/android/server/tv/TvS3DClient;->getCurrentInputSource()I

    move-result v6

    invoke-virtual {v3, v4, v5, v6}, Lcom/android/server/tv/DataBaseDeskImpl;->updateVideo3DMode(Lcom/android/server/tv/DataBaseDeskImpl$S3D_TABLE_FIELD;II)V

    :goto_1
    iget-object v3, p0, Lcom/android/server/tv/TvS3DClient;->db:Lcom/android/server/tv/DataBaseDeskImpl;

    sget-object v4, Lcom/android/server/tv/DataBaseDeskImpl$S3D_TABLE_FIELD;->eThreeDVideo3DTo2D:Lcom/android/server/tv/DataBaseDeskImpl$S3D_TABLE_FIELD;

    sget-object v5, Lcom/android/server/tv/DataBaseDesk$EN_ThreeD_Video_3DTO2D;->DB_ThreeD_Video_3DTO2D_NONE:Lcom/android/server/tv/DataBaseDesk$EN_ThreeD_Video_3DTO2D;

    invoke-virtual {v5}, Lcom/android/server/tv/DataBaseDesk$EN_ThreeD_Video_3DTO2D;->ordinal()I

    move-result v5

    invoke-direct {p0}, Lcom/android/server/tv/TvS3DClient;->getCurrentInputSource()I

    move-result v6

    invoke-virtual {v3, v4, v5, v6}, Lcom/android/server/tv/DataBaseDeskImpl;->updateVideo3DMode(Lcom/android/server/tv/DataBaseDeskImpl$S3D_TABLE_FIELD;II)V

    sget-object v3, Lcom/android/server/tv/DataBaseDesk$EN_ThreeD_Video_SELFADAPTIVE_DETECT;->DB_ThreeD_Video_SELF_ADAPTIVE_DETECT_WHEN_SOURCE_CHANGE:Lcom/android/server/tv/DataBaseDesk$EN_ThreeD_Video_SELFADAPTIVE_DETECT;

    invoke-virtual {v3}, Lcom/android/server/tv/DataBaseDesk$EN_ThreeD_Video_SELFADAPTIVE_DETECT;->ordinal()I

    move-result v3

    if-ne p1, v3, :cond_3

    :try_start_1
    invoke-static {}, Lcom/mstar/android/tvapi/common/TvManager;->getInstance()Lcom/mstar/android/tvapi/common/TvManager;

    move-result-object v3

    if-eqz v3, :cond_1

    invoke-static {}, Lcom/mstar/android/tvapi/common/TvManager;->getInstance()Lcom/mstar/android/tvapi/common/TvManager;

    move-result-object v3

    invoke-virtual {v3}, Lcom/mstar/android/tvapi/common/TvManager;->getThreeDimensionManager()Lcom/mstar/android/tvapi/common/ThreeDimensionManager;

    move-result-object v3

    const/4 v4, 0x1

    invoke-virtual {v3, v4}, Lcom/mstar/android/tvapi/common/ThreeDimensionManager;->set3dFormatDetectFlag(Z)Z
    :try_end_1
    .catch Lcom/mstar/android/tvapi/common/exception/TvCommonException; {:try_start_1 .. :try_end_1} :catch_1

    :cond_1
    :goto_2
    return v9

    :catch_0
    move-exception v2

    invoke-virtual {v2}, Lcom/mstar/android/tvapi/common/exception/TvCommonException;->printStackTrace()V

    goto :goto_0

    :cond_2
    iget-object v3, p0, Lcom/android/server/tv/TvS3DClient;->db:Lcom/android/server/tv/DataBaseDeskImpl;

    sget-object v4, Lcom/android/server/tv/DataBaseDeskImpl$S3D_TABLE_FIELD;->eThreeDVideoSelfAdaptiveDetect:Lcom/android/server/tv/DataBaseDeskImpl$S3D_TABLE_FIELD;

    sget-object v5, Lcom/android/server/tv/DataBaseDesk$EN_ThreeD_Video_SELFADAPTIVE_DETECT;->DB_ThreeD_Video_SELF_ADAPTIVE_DETECT_WHEN_SOURCE_CHANGE:Lcom/android/server/tv/DataBaseDesk$EN_ThreeD_Video_SELFADAPTIVE_DETECT;

    invoke-virtual {v5}, Lcom/android/server/tv/DataBaseDesk$EN_ThreeD_Video_SELFADAPTIVE_DETECT;->ordinal()I

    move-result v5

    sget-object v6, Lcom/mstar/android/tvapi/common/vo/TvOsType$EnumInputSource;->E_INPUT_SOURCE_HDMI:Lcom/mstar/android/tvapi/common/vo/TvOsType$EnumInputSource;

    invoke-virtual {v6}, Lcom/mstar/android/tvapi/common/vo/TvOsType$EnumInputSource;->ordinal()I

    move-result v6

    invoke-virtual {v3, v4, v5, v6}, Lcom/android/server/tv/DataBaseDeskImpl;->updateVideo3DMode(Lcom/android/server/tv/DataBaseDeskImpl$S3D_TABLE_FIELD;II)V

    iget-object v3, p0, Lcom/android/server/tv/TvS3DClient;->db:Lcom/android/server/tv/DataBaseDeskImpl;

    sget-object v4, Lcom/android/server/tv/DataBaseDeskImpl$S3D_TABLE_FIELD;->eThreeDVideoDisplayFormat:Lcom/android/server/tv/DataBaseDeskImpl$S3D_TABLE_FIELD;

    sget-object v5, Lcom/android/server/tv/DataBaseDesk$EN_ThreeD_Video_DISPLAYFORMAT;->DB_ThreeD_Video_DISPLAYFORMAT_AUTO:Lcom/android/server/tv/DataBaseDesk$EN_ThreeD_Video_DISPLAYFORMAT;

    invoke-virtual {v5}, Lcom/android/server/tv/DataBaseDesk$EN_ThreeD_Video_DISPLAYFORMAT;->ordinal()I

    move-result v5

    invoke-direct {p0}, Lcom/android/server/tv/TvS3DClient;->getCurrentInputSource()I

    move-result v6

    invoke-virtual {v3, v4, v5, v6}, Lcom/android/server/tv/DataBaseDeskImpl;->updateVideo3DMode(Lcom/android/server/tv/DataBaseDeskImpl$S3D_TABLE_FIELD;II)V

    goto :goto_1

    :catch_1
    move-exception v2

    invoke-virtual {v2}, Lcom/mstar/android/tvapi/common/exception/TvCommonException;->printStackTrace()V

    goto :goto_2

    :cond_3
    sget-object v3, Lcom/android/server/tv/DataBaseDesk$EN_ThreeD_Video_SELFADAPTIVE_DETECT;->DB_ThreeD_Video_SELF_ADAPTIVE_DETECT_RIGHT_NOW:Lcom/android/server/tv/DataBaseDesk$EN_ThreeD_Video_SELFADAPTIVE_DETECT;

    invoke-virtual {v3}, Lcom/android/server/tv/DataBaseDesk$EN_ThreeD_Video_SELFADAPTIVE_DETECT;->ordinal()I

    move-result v3

    if-ne p1, v3, :cond_7

    invoke-direct {p0}, Lcom/android/server/tv/TvS3DClient;->isSignalStable()Z

    move-result v3

    if-eqz v3, :cond_1

    invoke-direct {p0}, Lcom/android/server/tv/TvS3DClient;->getCurrentInputSource()I

    move-result v3

    sget-object v4, Lcom/mstar/android/tvapi/common/vo/TvOsType$EnumInputSource;->E_INPUT_SOURCE_STORAGE:Lcom/mstar/android/tvapi/common/vo/TvOsType$EnumInputSource;

    invoke-virtual {v4}, Lcom/mstar/android/tvapi/common/vo/TvOsType$EnumInputSource;->ordinal()I

    move-result v4

    if-ne v3, v4, :cond_5

    sget-object v3, Lcom/mstar/android/tvapi/common/vo/Enum3dType;->EN_3D_FRAME_PACKING_1080P:Lcom/mstar/android/tvapi/common/vo/Enum3dType;

    if-eq v1, v3, :cond_4

    sget-object v3, Lcom/mstar/android/tvapi/common/vo/Enum3dType;->EN_3D_FRAME_PACKING_720P:Lcom/mstar/android/tvapi/common/vo/Enum3dType;

    if-ne v1, v3, :cond_5

    :cond_4
    :try_start_2
    invoke-static {}, Lcom/mstar/android/tvapi/common/TvManager;->getInstance()Lcom/mstar/android/tvapi/common/TvManager;

    move-result-object v3

    if-eqz v3, :cond_1

    invoke-static {}, Lcom/mstar/android/tvapi/common/TvManager;->getInstance()Lcom/mstar/android/tvapi/common/TvManager;

    move-result-object v3

    const/4 v4, 0x1

    sget-object v5, Lcom/mstar/android/tvapi/common/vo/EnumScreenMuteType;->E_BLACK:Lcom/mstar/android/tvapi/common/vo/EnumScreenMuteType;

    const/4 v6, 0x0

    invoke-static {}, Lcom/mstar/android/tvapi/common/vo/TvOsType$EnumInputSource;->values()[Lcom/mstar/android/tvapi/common/vo/TvOsType$EnumInputSource;

    move-result-object v7

    invoke-direct {p0}, Lcom/android/server/tv/TvS3DClient;->getCurrentInputSource()I

    move-result v8

    aget-object v7, v7, v8

    invoke-virtual {v3, v4, v5, v6, v7}, Lcom/mstar/android/tvapi/common/TvManager;->setVideoMute(ZLcom/mstar/android/tvapi/common/vo/EnumScreenMuteType;ILcom/mstar/android/tvapi/common/vo/TvOsType$EnumInputSource;)Z

    invoke-static {}, Lcom/mstar/android/tvapi/common/TvManager;->getInstance()Lcom/mstar/android/tvapi/common/TvManager;

    move-result-object v3

    invoke-virtual {v3}, Lcom/mstar/android/tvapi/common/TvManager;->getThreeDimensionManager()Lcom/mstar/android/tvapi/common/ThreeDimensionManager;

    move-result-object v3

    invoke-virtual {v3, v1}, Lcom/mstar/android/tvapi/common/ThreeDimensionManager;->enable3d(Lcom/mstar/android/tvapi/common/vo/Enum3dType;)Z

    invoke-static {}, Lcom/mstar/android/tvapi/common/TvManager;->getInstance()Lcom/mstar/android/tvapi/common/TvManager;

    move-result-object v3

    const/4 v4, 0x0

    sget-object v5, Lcom/mstar/android/tvapi/common/vo/EnumScreenMuteType;->E_BLACK:Lcom/mstar/android/tvapi/common/vo/EnumScreenMuteType;

    const/4 v6, 0x0

    invoke-static {}, Lcom/mstar/android/tvapi/common/vo/TvOsType$EnumInputSource;->values()[Lcom/mstar/android/tvapi/common/vo/TvOsType$EnumInputSource;

    move-result-object v7

    invoke-direct {p0}, Lcom/android/server/tv/TvS3DClient;->getCurrentInputSource()I

    move-result v8

    aget-object v7, v7, v8

    invoke-virtual {v3, v4, v5, v6, v7}, Lcom/mstar/android/tvapi/common/TvManager;->setVideoMute(ZLcom/mstar/android/tvapi/common/vo/EnumScreenMuteType;ILcom/mstar/android/tvapi/common/vo/TvOsType$EnumInputSource;)Z

    invoke-static {}, Lcom/mstar/android/tvapi/common/TvManager;->getInstance()Lcom/mstar/android/tvapi/common/TvManager;

    move-result-object v3

    invoke-virtual {v3}, Lcom/mstar/android/tvapi/common/TvManager;->getThreeDimensionManager()Lcom/mstar/android/tvapi/common/ThreeDimensionManager;

    move-result-object v3

    const/4 v4, 0x1

    invoke-virtual {v3, v4}, Lcom/mstar/android/tvapi/common/ThreeDimensionManager;->set3dFormatDetectFlag(Z)Z
    :try_end_2
    .catch Lcom/mstar/android/tvapi/common/exception/TvCommonException; {:try_start_2 .. :try_end_2} :catch_2

    goto/16 :goto_2

    :catch_2
    move-exception v2

    invoke-virtual {v2}, Lcom/mstar/android/tvapi/common/exception/TvCommonException;->printStackTrace()V

    goto/16 :goto_2

    :cond_5
    :try_start_3
    invoke-static {}, Lcom/mstar/android/tvapi/common/TvManager;->getInstance()Lcom/mstar/android/tvapi/common/TvManager;

    move-result-object v3

    if-eqz v3, :cond_1

    invoke-static {}, Lcom/mstar/android/tvapi/common/TvManager;->getInstance()Lcom/mstar/android/tvapi/common/TvManager;

    move-result-object v3

    const/4 v4, 0x1

    sget-object v5, Lcom/mstar/android/tvapi/common/vo/EnumScreenMuteType;->E_BLACK:Lcom/mstar/android/tvapi/common/vo/EnumScreenMuteType;

    const/4 v6, 0x0

    invoke-static {}, Lcom/mstar/android/tvapi/common/vo/TvOsType$EnumInputSource;->values()[Lcom/mstar/android/tvapi/common/vo/TvOsType$EnumInputSource;

    move-result-object v7

    invoke-direct {p0}, Lcom/android/server/tv/TvS3DClient;->getCurrentInputSource()I

    move-result v8

    aget-object v7, v7, v8

    invoke-virtual {v3, v4, v5, v6, v7}, Lcom/mstar/android/tvapi/common/TvManager;->setVideoMute(ZLcom/mstar/android/tvapi/common/vo/EnumScreenMuteType;ILcom/mstar/android/tvapi/common/vo/TvOsType$EnumInputSource;)Z

    invoke-static {}, Lcom/mstar/android/tvapi/common/TvManager;->getInstance()Lcom/mstar/android/tvapi/common/TvManager;

    move-result-object v3

    invoke-virtual {v3}, Lcom/mstar/android/tvapi/common/TvManager;->getThreeDimensionManager()Lcom/mstar/android/tvapi/common/ThreeDimensionManager;

    move-result-object v3

    sget-object v4, Lcom/mstar/android/tvapi/common/vo/Enum3dType;->EN_3D_NONE:Lcom/mstar/android/tvapi/common/vo/Enum3dType;

    invoke-virtual {v3, v4}, Lcom/mstar/android/tvapi/common/ThreeDimensionManager;->enable3d(Lcom/mstar/android/tvapi/common/vo/Enum3dType;)Z

    invoke-static {}, Lcom/mstar/android/tvapi/common/TvManager;->getInstance()Lcom/mstar/android/tvapi/common/TvManager;

    move-result-object v3

    invoke-virtual {v3}, Lcom/mstar/android/tvapi/common/TvManager;->getThreeDimensionManager()Lcom/mstar/android/tvapi/common/ThreeDimensionManager;

    move-result-object v3

    sget-object v4, Lcom/mstar/android/tvapi/common/vo/EnumScalerWindow;->E_MAIN_WINDOW:Lcom/mstar/android/tvapi/common/vo/EnumScalerWindow;

    invoke-virtual {v3, v4}, Lcom/mstar/android/tvapi/common/ThreeDimensionManager;->detect3dFormat(Lcom/mstar/android/tvapi/common/vo/EnumScalerWindow;)Lcom/mstar/android/tvapi/common/vo/Enum3dType;

    move-result-object v0

    sget-object v3, Lcom/mstar/android/tvapi/common/vo/Enum3dType;->EN_3D_NONE:Lcom/mstar/android/tvapi/common/vo/Enum3dType;

    if-eq v0, v3, :cond_6

    invoke-static {}, Lcom/mstar/android/tvapi/common/TvManager;->getInstance()Lcom/mstar/android/tvapi/common/TvManager;

    move-result-object v3

    invoke-virtual {v3}, Lcom/mstar/android/tvapi/common/TvManager;->getThreeDimensionManager()Lcom/mstar/android/tvapi/common/ThreeDimensionManager;

    move-result-object v3

    invoke-virtual {v3, v0}, Lcom/mstar/android/tvapi/common/ThreeDimensionManager;->enable3d(Lcom/mstar/android/tvapi/common/vo/Enum3dType;)Z

    :cond_6
    invoke-static {}, Lcom/mstar/android/tvapi/common/TvManager;->getInstance()Lcom/mstar/android/tvapi/common/TvManager;

    move-result-object v3

    const/4 v4, 0x0

    sget-object v5, Lcom/mstar/android/tvapi/common/vo/EnumScreenMuteType;->E_BLACK:Lcom/mstar/android/tvapi/common/vo/EnumScreenMuteType;

    const/4 v6, 0x0

    invoke-static {}, Lcom/mstar/android/tvapi/common/vo/TvOsType$EnumInputSource;->values()[Lcom/mstar/android/tvapi/common/vo/TvOsType$EnumInputSource;

    move-result-object v7

    invoke-direct {p0}, Lcom/android/server/tv/TvS3DClient;->getCurrentInputSource()I

    move-result v8

    aget-object v7, v7, v8

    invoke-virtual {v3, v4, v5, v6, v7}, Lcom/mstar/android/tvapi/common/TvManager;->setVideoMute(ZLcom/mstar/android/tvapi/common/vo/EnumScreenMuteType;ILcom/mstar/android/tvapi/common/vo/TvOsType$EnumInputSource;)Z

    invoke-static {}, Lcom/mstar/android/tvapi/common/TvManager;->getInstance()Lcom/mstar/android/tvapi/common/TvManager;

    move-result-object v3

    invoke-virtual {v3}, Lcom/mstar/android/tvapi/common/TvManager;->getThreeDimensionManager()Lcom/mstar/android/tvapi/common/ThreeDimensionManager;

    move-result-object v3

    const/4 v4, 0x1

    invoke-virtual {v3, v4}, Lcom/mstar/android/tvapi/common/ThreeDimensionManager;->set3dFormatDetectFlag(Z)Z
    :try_end_3
    .catch Lcom/mstar/android/tvapi/common/exception/TvCommonException; {:try_start_3 .. :try_end_3} :catch_3

    goto/16 :goto_2

    :catch_3
    move-exception v2

    invoke-virtual {v2}, Lcom/mstar/android/tvapi/common/exception/TvCommonException;->printStackTrace()V

    goto/16 :goto_2

    :cond_7
    invoke-direct {p0}, Lcom/android/server/tv/TvS3DClient;->getCurrentInputSource()I

    move-result v3

    sget-object v4, Lcom/mstar/android/tvapi/common/vo/TvOsType$EnumInputSource;->E_INPUT_SOURCE_STORAGE:Lcom/mstar/android/tvapi/common/vo/TvOsType$EnumInputSource;

    invoke-virtual {v4}, Lcom/mstar/android/tvapi/common/vo/TvOsType$EnumInputSource;->ordinal()I

    move-result v4

    if-ne v3, v4, :cond_9

    sget-object v3, Lcom/mstar/android/tvapi/common/vo/Enum3dType;->EN_3D_FRAME_PACKING_1080P:Lcom/mstar/android/tvapi/common/vo/Enum3dType;

    if-eq v1, v3, :cond_8

    sget-object v3, Lcom/mstar/android/tvapi/common/vo/Enum3dType;->EN_3D_FRAME_PACKING_720P:Lcom/mstar/android/tvapi/common/vo/Enum3dType;

    if-ne v1, v3, :cond_9

    :cond_8
    :try_start_4
    invoke-static {}, Lcom/mstar/android/tvapi/common/TvManager;->getInstance()Lcom/mstar/android/tvapi/common/TvManager;

    move-result-object v3

    if-eqz v3, :cond_1

    invoke-static {}, Lcom/mstar/android/tvapi/common/TvManager;->getInstance()Lcom/mstar/android/tvapi/common/TvManager;

    move-result-object v3

    invoke-virtual {v3}, Lcom/mstar/android/tvapi/common/TvManager;->getThreeDimensionManager()Lcom/mstar/android/tvapi/common/ThreeDimensionManager;

    move-result-object v3

    const/4 v4, 0x0

    invoke-virtual {v3, v4}, Lcom/mstar/android/tvapi/common/ThreeDimensionManager;->set3dFormatDetectFlag(Z)Z

    invoke-static {}, Lcom/mstar/android/tvapi/common/TvManager;->getInstance()Lcom/mstar/android/tvapi/common/TvManager;

    move-result-object v3

    const/4 v4, 0x1

    sget-object v5, Lcom/mstar/android/tvapi/common/vo/EnumScreenMuteType;->E_BLACK:Lcom/mstar/android/tvapi/common/vo/EnumScreenMuteType;

    const/4 v6, 0x0

    invoke-static {}, Lcom/mstar/android/tvapi/common/vo/TvOsType$EnumInputSource;->values()[Lcom/mstar/android/tvapi/common/vo/TvOsType$EnumInputSource;

    move-result-object v7

    invoke-direct {p0}, Lcom/android/server/tv/TvS3DClient;->getCurrentInputSource()I

    move-result v8

    aget-object v7, v7, v8

    invoke-virtual {v3, v4, v5, v6, v7}, Lcom/mstar/android/tvapi/common/TvManager;->setVideoMute(ZLcom/mstar/android/tvapi/common/vo/EnumScreenMuteType;ILcom/mstar/android/tvapi/common/vo/TvOsType$EnumInputSource;)Z

    invoke-static {}, Lcom/mstar/android/tvapi/common/TvManager;->getInstance()Lcom/mstar/android/tvapi/common/TvManager;

    move-result-object v3

    invoke-virtual {v3}, Lcom/mstar/android/tvapi/common/TvManager;->getThreeDimensionManager()Lcom/mstar/android/tvapi/common/ThreeDimensionManager;

    move-result-object v3

    invoke-virtual {v3, v1}, Lcom/mstar/android/tvapi/common/ThreeDimensionManager;->enable3dTo2d(Lcom/mstar/android/tvapi/common/vo/Enum3dType;)Z

    invoke-static {}, Lcom/mstar/android/tvapi/common/TvManager;->getInstance()Lcom/mstar/android/tvapi/common/TvManager;

    move-result-object v3

    const/4 v4, 0x0

    sget-object v5, Lcom/mstar/android/tvapi/common/vo/EnumScreenMuteType;->E_BLACK:Lcom/mstar/android/tvapi/common/vo/EnumScreenMuteType;

    const/4 v6, 0x0

    invoke-static {}, Lcom/mstar/android/tvapi/common/vo/TvOsType$EnumInputSource;->values()[Lcom/mstar/android/tvapi/common/vo/TvOsType$EnumInputSource;

    move-result-object v7

    invoke-direct {p0}, Lcom/android/server/tv/TvS3DClient;->getCurrentInputSource()I

    move-result v8

    aget-object v7, v7, v8

    invoke-virtual {v3, v4, v5, v6, v7}, Lcom/mstar/android/tvapi/common/TvManager;->setVideoMute(ZLcom/mstar/android/tvapi/common/vo/EnumScreenMuteType;ILcom/mstar/android/tvapi/common/vo/TvOsType$EnumInputSource;)Z
    :try_end_4
    .catch Lcom/mstar/android/tvapi/common/exception/TvCommonException; {:try_start_4 .. :try_end_4} :catch_4

    goto/16 :goto_2

    :catch_4
    move-exception v2

    invoke-virtual {v2}, Lcom/mstar/android/tvapi/common/exception/TvCommonException;->printStackTrace()V

    goto/16 :goto_2

    :cond_9
    :try_start_5
    invoke-static {}, Lcom/mstar/android/tvapi/common/TvManager;->getInstance()Lcom/mstar/android/tvapi/common/TvManager;

    move-result-object v3

    if-eqz v3, :cond_1

    invoke-static {}, Lcom/mstar/android/tvapi/common/TvManager;->getInstance()Lcom/mstar/android/tvapi/common/TvManager;

    move-result-object v3

    invoke-virtual {v3}, Lcom/mstar/android/tvapi/common/TvManager;->getThreeDimensionManager()Lcom/mstar/android/tvapi/common/ThreeDimensionManager;

    move-result-object v3

    const/4 v4, 0x0

    invoke-virtual {v3, v4}, Lcom/mstar/android/tvapi/common/ThreeDimensionManager;->set3dFormatDetectFlag(Z)Z

    invoke-static {}, Lcom/mstar/android/tvapi/common/TvManager;->getInstance()Lcom/mstar/android/tvapi/common/TvManager;

    move-result-object v3

    const/4 v4, 0x1

    sget-object v5, Lcom/mstar/android/tvapi/common/vo/EnumScreenMuteType;->E_BLACK:Lcom/mstar/android/tvapi/common/vo/EnumScreenMuteType;

    const/4 v6, 0x0

    invoke-static {}, Lcom/mstar/android/tvapi/common/vo/TvOsType$EnumInputSource;->values()[Lcom/mstar/android/tvapi/common/vo/TvOsType$EnumInputSource;

    move-result-object v7

    invoke-direct {p0}, Lcom/android/server/tv/TvS3DClient;->getCurrentInputSource()I

    move-result v8

    aget-object v7, v7, v8

    invoke-virtual {v3, v4, v5, v6, v7}, Lcom/mstar/android/tvapi/common/TvManager;->setVideoMute(ZLcom/mstar/android/tvapi/common/vo/EnumScreenMuteType;ILcom/mstar/android/tvapi/common/vo/TvOsType$EnumInputSource;)Z

    invoke-static {}, Lcom/mstar/android/tvapi/common/TvManager;->getInstance()Lcom/mstar/android/tvapi/common/TvManager;

    move-result-object v3

    invoke-virtual {v3}, Lcom/mstar/android/tvapi/common/TvManager;->getThreeDimensionManager()Lcom/mstar/android/tvapi/common/ThreeDimensionManager;

    move-result-object v3

    sget-object v4, Lcom/mstar/android/tvapi/common/vo/Enum3dType;->EN_3D_NONE:Lcom/mstar/android/tvapi/common/vo/Enum3dType;

    invoke-virtual {v3, v4}, Lcom/mstar/android/tvapi/common/ThreeDimensionManager;->enable3d(Lcom/mstar/android/tvapi/common/vo/Enum3dType;)Z

    invoke-static {}, Lcom/mstar/android/tvapi/common/TvManager;->getInstance()Lcom/mstar/android/tvapi/common/TvManager;

    move-result-object v3

    const/4 v4, 0x0

    sget-object v5, Lcom/mstar/android/tvapi/common/vo/EnumScreenMuteType;->E_BLACK:Lcom/mstar/android/tvapi/common/vo/EnumScreenMuteType;

    const/4 v6, 0x0

    invoke-static {}, Lcom/mstar/android/tvapi/common/vo/TvOsType$EnumInputSource;->values()[Lcom/mstar/android/tvapi/common/vo/TvOsType$EnumInputSource;

    move-result-object v7

    invoke-direct {p0}, Lcom/android/server/tv/TvS3DClient;->getCurrentInputSource()I

    move-result v8

    aget-object v7, v7, v8

    invoke-virtual {v3, v4, v5, v6, v7}, Lcom/mstar/android/tvapi/common/TvManager;->setVideoMute(ZLcom/mstar/android/tvapi/common/vo/EnumScreenMuteType;ILcom/mstar/android/tvapi/common/vo/TvOsType$EnumInputSource;)Z
    :try_end_5
    .catch Lcom/mstar/android/tvapi/common/exception/TvCommonException; {:try_start_5 .. :try_end_5} :catch_5

    goto/16 :goto_2

    :catch_5
    move-exception v2

    invoke-virtual {v2}, Lcom/mstar/android/tvapi/common/exception/TvCommonException;->printStackTrace()V

    goto/16 :goto_2
.end method
