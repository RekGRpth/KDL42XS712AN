.class public final enum Lcom/android/server/tv/DataBaseDesk$MEMBER_COUNTRY;
.super Ljava/lang/Enum;
.source "DataBaseDesk.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/android/server/tv/DataBaseDesk;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x4019
    name = "MEMBER_COUNTRY"
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Enum",
        "<",
        "Lcom/android/server/tv/DataBaseDesk$MEMBER_COUNTRY;",
        ">;"
    }
.end annotation


# static fields
.field private static final synthetic $VALUES:[Lcom/android/server/tv/DataBaseDesk$MEMBER_COUNTRY;

.field public static final enum E_ARAB:Lcom/android/server/tv/DataBaseDesk$MEMBER_COUNTRY;

.field public static final enum E_ARGENTINA:Lcom/android/server/tv/DataBaseDesk$MEMBER_COUNTRY;

.field public static final enum E_AUSTRALIA:Lcom/android/server/tv/DataBaseDesk$MEMBER_COUNTRY;

.field public static final enum E_AUSTRIA:Lcom/android/server/tv/DataBaseDesk$MEMBER_COUNTRY;

.field public static final enum E_BELGIUM:Lcom/android/server/tv/DataBaseDesk$MEMBER_COUNTRY;

.field public static final enum E_BELIZE:Lcom/android/server/tv/DataBaseDesk$MEMBER_COUNTRY;

.field public static final enum E_BOLIVIA:Lcom/android/server/tv/DataBaseDesk$MEMBER_COUNTRY;

.field public static final enum E_BRAZIL:Lcom/android/server/tv/DataBaseDesk$MEMBER_COUNTRY;

.field public static final enum E_BULGARIA:Lcom/android/server/tv/DataBaseDesk$MEMBER_COUNTRY;

.field public static final enum E_CANADA:Lcom/android/server/tv/DataBaseDesk$MEMBER_COUNTRY;

.field public static final enum E_CHILE:Lcom/android/server/tv/DataBaseDesk$MEMBER_COUNTRY;

.field public static final enum E_CHINA:Lcom/android/server/tv/DataBaseDesk$MEMBER_COUNTRY;

.field public static final enum E_COSTARICA:Lcom/android/server/tv/DataBaseDesk$MEMBER_COUNTRY;

.field public static final enum E_COUNTRY_NUM:Lcom/android/server/tv/DataBaseDesk$MEMBER_COUNTRY;

.field public static final enum E_CROATIA:Lcom/android/server/tv/DataBaseDesk$MEMBER_COUNTRY;

.field public static final enum E_CZECH:Lcom/android/server/tv/DataBaseDesk$MEMBER_COUNTRY;

.field public static final enum E_DENMARK:Lcom/android/server/tv/DataBaseDesk$MEMBER_COUNTRY;

.field public static final enum E_ECUADOR:Lcom/android/server/tv/DataBaseDesk$MEMBER_COUNTRY;

.field public static final enum E_ESTONIA:Lcom/android/server/tv/DataBaseDesk$MEMBER_COUNTRY;

.field public static final enum E_FINLAND:Lcom/android/server/tv/DataBaseDesk$MEMBER_COUNTRY;

.field public static final enum E_FRANCE:Lcom/android/server/tv/DataBaseDesk$MEMBER_COUNTRY;

.field public static final enum E_GERMANY:Lcom/android/server/tv/DataBaseDesk$MEMBER_COUNTRY;

.field public static final enum E_GREECE:Lcom/android/server/tv/DataBaseDesk$MEMBER_COUNTRY;

.field public static final enum E_GUATEMALA:Lcom/android/server/tv/DataBaseDesk$MEMBER_COUNTRY;

.field public static final enum E_HEBREW:Lcom/android/server/tv/DataBaseDesk$MEMBER_COUNTRY;

.field public static final enum E_HUNGARY:Lcom/android/server/tv/DataBaseDesk$MEMBER_COUNTRY;

.field public static final enum E_IRELAND:Lcom/android/server/tv/DataBaseDesk$MEMBER_COUNTRY;

.field public static final enum E_ITALY:Lcom/android/server/tv/DataBaseDesk$MEMBER_COUNTRY;

.field public static final enum E_JAPAN:Lcom/android/server/tv/DataBaseDesk$MEMBER_COUNTRY;

.field public static final enum E_LATVIA:Lcom/android/server/tv/DataBaseDesk$MEMBER_COUNTRY;

.field public static final enum E_LUXEMBOURG:Lcom/android/server/tv/DataBaseDesk$MEMBER_COUNTRY;

.field public static final enum E_MALDIVES:Lcom/android/server/tv/DataBaseDesk$MEMBER_COUNTRY;

.field public static final enum E_MEXICO:Lcom/android/server/tv/DataBaseDesk$MEMBER_COUNTRY;

.field public static final enum E_NETHERLANDS:Lcom/android/server/tv/DataBaseDesk$MEMBER_COUNTRY;

.field public static final enum E_NEWZEALAND:Lcom/android/server/tv/DataBaseDesk$MEMBER_COUNTRY;

.field public static final enum E_NICARAGUA:Lcom/android/server/tv/DataBaseDesk$MEMBER_COUNTRY;

.field public static final enum E_NORWAY:Lcom/android/server/tv/DataBaseDesk$MEMBER_COUNTRY;

.field public static final enum E_OTHERS:Lcom/android/server/tv/DataBaseDesk$MEMBER_COUNTRY;

.field public static final enum E_PARAGUAY:Lcom/android/server/tv/DataBaseDesk$MEMBER_COUNTRY;

.field public static final enum E_PERU:Lcom/android/server/tv/DataBaseDesk$MEMBER_COUNTRY;

.field public static final enum E_PHILIPPINES:Lcom/android/server/tv/DataBaseDesk$MEMBER_COUNTRY;

.field public static final enum E_POLAND:Lcom/android/server/tv/DataBaseDesk$MEMBER_COUNTRY;

.field public static final enum E_PORTUGAL:Lcom/android/server/tv/DataBaseDesk$MEMBER_COUNTRY;

.field public static final enum E_RUMANIA:Lcom/android/server/tv/DataBaseDesk$MEMBER_COUNTRY;

.field public static final enum E_RUSSIA:Lcom/android/server/tv/DataBaseDesk$MEMBER_COUNTRY;

.field public static final enum E_SERBIA:Lcom/android/server/tv/DataBaseDesk$MEMBER_COUNTRY;

.field public static final enum E_SLOVAKIA:Lcom/android/server/tv/DataBaseDesk$MEMBER_COUNTRY;

.field public static final enum E_SLOVENIA:Lcom/android/server/tv/DataBaseDesk$MEMBER_COUNTRY;

.field public static final enum E_SOUTHKOREA:Lcom/android/server/tv/DataBaseDesk$MEMBER_COUNTRY;

.field public static final enum E_SPAIN:Lcom/android/server/tv/DataBaseDesk$MEMBER_COUNTRY;

.field public static final enum E_SWEDEN:Lcom/android/server/tv/DataBaseDesk$MEMBER_COUNTRY;

.field public static final enum E_SWITZERLAND:Lcom/android/server/tv/DataBaseDesk$MEMBER_COUNTRY;

.field public static final enum E_TAIWAN:Lcom/android/server/tv/DataBaseDesk$MEMBER_COUNTRY;

.field public static final enum E_THAILAND:Lcom/android/server/tv/DataBaseDesk$MEMBER_COUNTRY;

.field public static final enum E_TURKEY:Lcom/android/server/tv/DataBaseDesk$MEMBER_COUNTRY;

.field public static final enum E_UK:Lcom/android/server/tv/DataBaseDesk$MEMBER_COUNTRY;

.field public static final enum E_URUGUAY:Lcom/android/server/tv/DataBaseDesk$MEMBER_COUNTRY;

.field public static final enum E_US:Lcom/android/server/tv/DataBaseDesk$MEMBER_COUNTRY;

.field public static final enum E_VENEZUELA:Lcom/android/server/tv/DataBaseDesk$MEMBER_COUNTRY;


# direct methods
.method static constructor <clinit>()V
    .locals 8

    const/4 v7, 0x4

    const/4 v6, 0x3

    const/4 v5, 0x2

    const/4 v4, 0x1

    const/4 v3, 0x0

    new-instance v0, Lcom/android/server/tv/DataBaseDesk$MEMBER_COUNTRY;

    const-string v1, "E_AUSTRALIA"

    invoke-direct {v0, v1, v3}, Lcom/android/server/tv/DataBaseDesk$MEMBER_COUNTRY;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/android/server/tv/DataBaseDesk$MEMBER_COUNTRY;->E_AUSTRALIA:Lcom/android/server/tv/DataBaseDesk$MEMBER_COUNTRY;

    new-instance v0, Lcom/android/server/tv/DataBaseDesk$MEMBER_COUNTRY;

    const-string v1, "E_AUSTRIA"

    invoke-direct {v0, v1, v4}, Lcom/android/server/tv/DataBaseDesk$MEMBER_COUNTRY;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/android/server/tv/DataBaseDesk$MEMBER_COUNTRY;->E_AUSTRIA:Lcom/android/server/tv/DataBaseDesk$MEMBER_COUNTRY;

    new-instance v0, Lcom/android/server/tv/DataBaseDesk$MEMBER_COUNTRY;

    const-string v1, "E_BELGIUM"

    invoke-direct {v0, v1, v5}, Lcom/android/server/tv/DataBaseDesk$MEMBER_COUNTRY;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/android/server/tv/DataBaseDesk$MEMBER_COUNTRY;->E_BELGIUM:Lcom/android/server/tv/DataBaseDesk$MEMBER_COUNTRY;

    new-instance v0, Lcom/android/server/tv/DataBaseDesk$MEMBER_COUNTRY;

    const-string v1, "E_BULGARIA"

    invoke-direct {v0, v1, v6}, Lcom/android/server/tv/DataBaseDesk$MEMBER_COUNTRY;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/android/server/tv/DataBaseDesk$MEMBER_COUNTRY;->E_BULGARIA:Lcom/android/server/tv/DataBaseDesk$MEMBER_COUNTRY;

    new-instance v0, Lcom/android/server/tv/DataBaseDesk$MEMBER_COUNTRY;

    const-string v1, "E_CROATIA"

    invoke-direct {v0, v1, v7}, Lcom/android/server/tv/DataBaseDesk$MEMBER_COUNTRY;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/android/server/tv/DataBaseDesk$MEMBER_COUNTRY;->E_CROATIA:Lcom/android/server/tv/DataBaseDesk$MEMBER_COUNTRY;

    new-instance v0, Lcom/android/server/tv/DataBaseDesk$MEMBER_COUNTRY;

    const-string v1, "E_CZECH"

    const/4 v2, 0x5

    invoke-direct {v0, v1, v2}, Lcom/android/server/tv/DataBaseDesk$MEMBER_COUNTRY;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/android/server/tv/DataBaseDesk$MEMBER_COUNTRY;->E_CZECH:Lcom/android/server/tv/DataBaseDesk$MEMBER_COUNTRY;

    new-instance v0, Lcom/android/server/tv/DataBaseDesk$MEMBER_COUNTRY;

    const-string v1, "E_DENMARK"

    const/4 v2, 0x6

    invoke-direct {v0, v1, v2}, Lcom/android/server/tv/DataBaseDesk$MEMBER_COUNTRY;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/android/server/tv/DataBaseDesk$MEMBER_COUNTRY;->E_DENMARK:Lcom/android/server/tv/DataBaseDesk$MEMBER_COUNTRY;

    new-instance v0, Lcom/android/server/tv/DataBaseDesk$MEMBER_COUNTRY;

    const-string v1, "E_FINLAND"

    const/4 v2, 0x7

    invoke-direct {v0, v1, v2}, Lcom/android/server/tv/DataBaseDesk$MEMBER_COUNTRY;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/android/server/tv/DataBaseDesk$MEMBER_COUNTRY;->E_FINLAND:Lcom/android/server/tv/DataBaseDesk$MEMBER_COUNTRY;

    new-instance v0, Lcom/android/server/tv/DataBaseDesk$MEMBER_COUNTRY;

    const-string v1, "E_FRANCE"

    const/16 v2, 0x8

    invoke-direct {v0, v1, v2}, Lcom/android/server/tv/DataBaseDesk$MEMBER_COUNTRY;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/android/server/tv/DataBaseDesk$MEMBER_COUNTRY;->E_FRANCE:Lcom/android/server/tv/DataBaseDesk$MEMBER_COUNTRY;

    new-instance v0, Lcom/android/server/tv/DataBaseDesk$MEMBER_COUNTRY;

    const-string v1, "E_GERMANY"

    const/16 v2, 0x9

    invoke-direct {v0, v1, v2}, Lcom/android/server/tv/DataBaseDesk$MEMBER_COUNTRY;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/android/server/tv/DataBaseDesk$MEMBER_COUNTRY;->E_GERMANY:Lcom/android/server/tv/DataBaseDesk$MEMBER_COUNTRY;

    new-instance v0, Lcom/android/server/tv/DataBaseDesk$MEMBER_COUNTRY;

    const-string v1, "E_GREECE"

    const/16 v2, 0xa

    invoke-direct {v0, v1, v2}, Lcom/android/server/tv/DataBaseDesk$MEMBER_COUNTRY;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/android/server/tv/DataBaseDesk$MEMBER_COUNTRY;->E_GREECE:Lcom/android/server/tv/DataBaseDesk$MEMBER_COUNTRY;

    new-instance v0, Lcom/android/server/tv/DataBaseDesk$MEMBER_COUNTRY;

    const-string v1, "E_HUNGARY"

    const/16 v2, 0xb

    invoke-direct {v0, v1, v2}, Lcom/android/server/tv/DataBaseDesk$MEMBER_COUNTRY;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/android/server/tv/DataBaseDesk$MEMBER_COUNTRY;->E_HUNGARY:Lcom/android/server/tv/DataBaseDesk$MEMBER_COUNTRY;

    new-instance v0, Lcom/android/server/tv/DataBaseDesk$MEMBER_COUNTRY;

    const-string v1, "E_ITALY"

    const/16 v2, 0xc

    invoke-direct {v0, v1, v2}, Lcom/android/server/tv/DataBaseDesk$MEMBER_COUNTRY;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/android/server/tv/DataBaseDesk$MEMBER_COUNTRY;->E_ITALY:Lcom/android/server/tv/DataBaseDesk$MEMBER_COUNTRY;

    new-instance v0, Lcom/android/server/tv/DataBaseDesk$MEMBER_COUNTRY;

    const-string v1, "E_LUXEMBOURG"

    const/16 v2, 0xd

    invoke-direct {v0, v1, v2}, Lcom/android/server/tv/DataBaseDesk$MEMBER_COUNTRY;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/android/server/tv/DataBaseDesk$MEMBER_COUNTRY;->E_LUXEMBOURG:Lcom/android/server/tv/DataBaseDesk$MEMBER_COUNTRY;

    new-instance v0, Lcom/android/server/tv/DataBaseDesk$MEMBER_COUNTRY;

    const-string v1, "E_NETHERLANDS"

    const/16 v2, 0xe

    invoke-direct {v0, v1, v2}, Lcom/android/server/tv/DataBaseDesk$MEMBER_COUNTRY;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/android/server/tv/DataBaseDesk$MEMBER_COUNTRY;->E_NETHERLANDS:Lcom/android/server/tv/DataBaseDesk$MEMBER_COUNTRY;

    new-instance v0, Lcom/android/server/tv/DataBaseDesk$MEMBER_COUNTRY;

    const-string v1, "E_NORWAY"

    const/16 v2, 0xf

    invoke-direct {v0, v1, v2}, Lcom/android/server/tv/DataBaseDesk$MEMBER_COUNTRY;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/android/server/tv/DataBaseDesk$MEMBER_COUNTRY;->E_NORWAY:Lcom/android/server/tv/DataBaseDesk$MEMBER_COUNTRY;

    new-instance v0, Lcom/android/server/tv/DataBaseDesk$MEMBER_COUNTRY;

    const-string v1, "E_POLAND"

    const/16 v2, 0x10

    invoke-direct {v0, v1, v2}, Lcom/android/server/tv/DataBaseDesk$MEMBER_COUNTRY;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/android/server/tv/DataBaseDesk$MEMBER_COUNTRY;->E_POLAND:Lcom/android/server/tv/DataBaseDesk$MEMBER_COUNTRY;

    new-instance v0, Lcom/android/server/tv/DataBaseDesk$MEMBER_COUNTRY;

    const-string v1, "E_PORTUGAL"

    const/16 v2, 0x11

    invoke-direct {v0, v1, v2}, Lcom/android/server/tv/DataBaseDesk$MEMBER_COUNTRY;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/android/server/tv/DataBaseDesk$MEMBER_COUNTRY;->E_PORTUGAL:Lcom/android/server/tv/DataBaseDesk$MEMBER_COUNTRY;

    new-instance v0, Lcom/android/server/tv/DataBaseDesk$MEMBER_COUNTRY;

    const-string v1, "E_RUMANIA"

    const/16 v2, 0x12

    invoke-direct {v0, v1, v2}, Lcom/android/server/tv/DataBaseDesk$MEMBER_COUNTRY;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/android/server/tv/DataBaseDesk$MEMBER_COUNTRY;->E_RUMANIA:Lcom/android/server/tv/DataBaseDesk$MEMBER_COUNTRY;

    new-instance v0, Lcom/android/server/tv/DataBaseDesk$MEMBER_COUNTRY;

    const-string v1, "E_RUSSIA"

    const/16 v2, 0x13

    invoke-direct {v0, v1, v2}, Lcom/android/server/tv/DataBaseDesk$MEMBER_COUNTRY;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/android/server/tv/DataBaseDesk$MEMBER_COUNTRY;->E_RUSSIA:Lcom/android/server/tv/DataBaseDesk$MEMBER_COUNTRY;

    new-instance v0, Lcom/android/server/tv/DataBaseDesk$MEMBER_COUNTRY;

    const-string v1, "E_SERBIA"

    const/16 v2, 0x14

    invoke-direct {v0, v1, v2}, Lcom/android/server/tv/DataBaseDesk$MEMBER_COUNTRY;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/android/server/tv/DataBaseDesk$MEMBER_COUNTRY;->E_SERBIA:Lcom/android/server/tv/DataBaseDesk$MEMBER_COUNTRY;

    new-instance v0, Lcom/android/server/tv/DataBaseDesk$MEMBER_COUNTRY;

    const-string v1, "E_SLOVENIA"

    const/16 v2, 0x15

    invoke-direct {v0, v1, v2}, Lcom/android/server/tv/DataBaseDesk$MEMBER_COUNTRY;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/android/server/tv/DataBaseDesk$MEMBER_COUNTRY;->E_SLOVENIA:Lcom/android/server/tv/DataBaseDesk$MEMBER_COUNTRY;

    new-instance v0, Lcom/android/server/tv/DataBaseDesk$MEMBER_COUNTRY;

    const-string v1, "E_SPAIN"

    const/16 v2, 0x16

    invoke-direct {v0, v1, v2}, Lcom/android/server/tv/DataBaseDesk$MEMBER_COUNTRY;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/android/server/tv/DataBaseDesk$MEMBER_COUNTRY;->E_SPAIN:Lcom/android/server/tv/DataBaseDesk$MEMBER_COUNTRY;

    new-instance v0, Lcom/android/server/tv/DataBaseDesk$MEMBER_COUNTRY;

    const-string v1, "E_SWEDEN"

    const/16 v2, 0x17

    invoke-direct {v0, v1, v2}, Lcom/android/server/tv/DataBaseDesk$MEMBER_COUNTRY;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/android/server/tv/DataBaseDesk$MEMBER_COUNTRY;->E_SWEDEN:Lcom/android/server/tv/DataBaseDesk$MEMBER_COUNTRY;

    new-instance v0, Lcom/android/server/tv/DataBaseDesk$MEMBER_COUNTRY;

    const-string v1, "E_SWITZERLAND"

    const/16 v2, 0x18

    invoke-direct {v0, v1, v2}, Lcom/android/server/tv/DataBaseDesk$MEMBER_COUNTRY;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/android/server/tv/DataBaseDesk$MEMBER_COUNTRY;->E_SWITZERLAND:Lcom/android/server/tv/DataBaseDesk$MEMBER_COUNTRY;

    new-instance v0, Lcom/android/server/tv/DataBaseDesk$MEMBER_COUNTRY;

    const-string v1, "E_UK"

    const/16 v2, 0x19

    invoke-direct {v0, v1, v2}, Lcom/android/server/tv/DataBaseDesk$MEMBER_COUNTRY;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/android/server/tv/DataBaseDesk$MEMBER_COUNTRY;->E_UK:Lcom/android/server/tv/DataBaseDesk$MEMBER_COUNTRY;

    new-instance v0, Lcom/android/server/tv/DataBaseDesk$MEMBER_COUNTRY;

    const-string v1, "E_NEWZEALAND"

    const/16 v2, 0x1a

    invoke-direct {v0, v1, v2}, Lcom/android/server/tv/DataBaseDesk$MEMBER_COUNTRY;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/android/server/tv/DataBaseDesk$MEMBER_COUNTRY;->E_NEWZEALAND:Lcom/android/server/tv/DataBaseDesk$MEMBER_COUNTRY;

    new-instance v0, Lcom/android/server/tv/DataBaseDesk$MEMBER_COUNTRY;

    const-string v1, "E_ARAB"

    const/16 v2, 0x1b

    invoke-direct {v0, v1, v2}, Lcom/android/server/tv/DataBaseDesk$MEMBER_COUNTRY;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/android/server/tv/DataBaseDesk$MEMBER_COUNTRY;->E_ARAB:Lcom/android/server/tv/DataBaseDesk$MEMBER_COUNTRY;

    new-instance v0, Lcom/android/server/tv/DataBaseDesk$MEMBER_COUNTRY;

    const-string v1, "E_ESTONIA"

    const/16 v2, 0x1c

    invoke-direct {v0, v1, v2}, Lcom/android/server/tv/DataBaseDesk$MEMBER_COUNTRY;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/android/server/tv/DataBaseDesk$MEMBER_COUNTRY;->E_ESTONIA:Lcom/android/server/tv/DataBaseDesk$MEMBER_COUNTRY;

    new-instance v0, Lcom/android/server/tv/DataBaseDesk$MEMBER_COUNTRY;

    const-string v1, "E_HEBREW"

    const/16 v2, 0x1d

    invoke-direct {v0, v1, v2}, Lcom/android/server/tv/DataBaseDesk$MEMBER_COUNTRY;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/android/server/tv/DataBaseDesk$MEMBER_COUNTRY;->E_HEBREW:Lcom/android/server/tv/DataBaseDesk$MEMBER_COUNTRY;

    new-instance v0, Lcom/android/server/tv/DataBaseDesk$MEMBER_COUNTRY;

    const-string v1, "E_LATVIA"

    const/16 v2, 0x1e

    invoke-direct {v0, v1, v2}, Lcom/android/server/tv/DataBaseDesk$MEMBER_COUNTRY;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/android/server/tv/DataBaseDesk$MEMBER_COUNTRY;->E_LATVIA:Lcom/android/server/tv/DataBaseDesk$MEMBER_COUNTRY;

    new-instance v0, Lcom/android/server/tv/DataBaseDesk$MEMBER_COUNTRY;

    const-string v1, "E_SLOVAKIA"

    const/16 v2, 0x1f

    invoke-direct {v0, v1, v2}, Lcom/android/server/tv/DataBaseDesk$MEMBER_COUNTRY;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/android/server/tv/DataBaseDesk$MEMBER_COUNTRY;->E_SLOVAKIA:Lcom/android/server/tv/DataBaseDesk$MEMBER_COUNTRY;

    new-instance v0, Lcom/android/server/tv/DataBaseDesk$MEMBER_COUNTRY;

    const-string v1, "E_TURKEY"

    const/16 v2, 0x20

    invoke-direct {v0, v1, v2}, Lcom/android/server/tv/DataBaseDesk$MEMBER_COUNTRY;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/android/server/tv/DataBaseDesk$MEMBER_COUNTRY;->E_TURKEY:Lcom/android/server/tv/DataBaseDesk$MEMBER_COUNTRY;

    new-instance v0, Lcom/android/server/tv/DataBaseDesk$MEMBER_COUNTRY;

    const-string v1, "E_IRELAND"

    const/16 v2, 0x21

    invoke-direct {v0, v1, v2}, Lcom/android/server/tv/DataBaseDesk$MEMBER_COUNTRY;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/android/server/tv/DataBaseDesk$MEMBER_COUNTRY;->E_IRELAND:Lcom/android/server/tv/DataBaseDesk$MEMBER_COUNTRY;

    new-instance v0, Lcom/android/server/tv/DataBaseDesk$MEMBER_COUNTRY;

    const-string v1, "E_JAPAN"

    const/16 v2, 0x22

    invoke-direct {v0, v1, v2}, Lcom/android/server/tv/DataBaseDesk$MEMBER_COUNTRY;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/android/server/tv/DataBaseDesk$MEMBER_COUNTRY;->E_JAPAN:Lcom/android/server/tv/DataBaseDesk$MEMBER_COUNTRY;

    new-instance v0, Lcom/android/server/tv/DataBaseDesk$MEMBER_COUNTRY;

    const-string v1, "E_PHILIPPINES"

    const/16 v2, 0x23

    invoke-direct {v0, v1, v2}, Lcom/android/server/tv/DataBaseDesk$MEMBER_COUNTRY;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/android/server/tv/DataBaseDesk$MEMBER_COUNTRY;->E_PHILIPPINES:Lcom/android/server/tv/DataBaseDesk$MEMBER_COUNTRY;

    new-instance v0, Lcom/android/server/tv/DataBaseDesk$MEMBER_COUNTRY;

    const-string v1, "E_THAILAND"

    const/16 v2, 0x24

    invoke-direct {v0, v1, v2}, Lcom/android/server/tv/DataBaseDesk$MEMBER_COUNTRY;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/android/server/tv/DataBaseDesk$MEMBER_COUNTRY;->E_THAILAND:Lcom/android/server/tv/DataBaseDesk$MEMBER_COUNTRY;

    new-instance v0, Lcom/android/server/tv/DataBaseDesk$MEMBER_COUNTRY;

    const-string v1, "E_MALDIVES"

    const/16 v2, 0x25

    invoke-direct {v0, v1, v2}, Lcom/android/server/tv/DataBaseDesk$MEMBER_COUNTRY;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/android/server/tv/DataBaseDesk$MEMBER_COUNTRY;->E_MALDIVES:Lcom/android/server/tv/DataBaseDesk$MEMBER_COUNTRY;

    new-instance v0, Lcom/android/server/tv/DataBaseDesk$MEMBER_COUNTRY;

    const-string v1, "E_URUGUAY"

    const/16 v2, 0x26

    invoke-direct {v0, v1, v2}, Lcom/android/server/tv/DataBaseDesk$MEMBER_COUNTRY;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/android/server/tv/DataBaseDesk$MEMBER_COUNTRY;->E_URUGUAY:Lcom/android/server/tv/DataBaseDesk$MEMBER_COUNTRY;

    new-instance v0, Lcom/android/server/tv/DataBaseDesk$MEMBER_COUNTRY;

    const-string v1, "E_PERU"

    const/16 v2, 0x27

    invoke-direct {v0, v1, v2}, Lcom/android/server/tv/DataBaseDesk$MEMBER_COUNTRY;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/android/server/tv/DataBaseDesk$MEMBER_COUNTRY;->E_PERU:Lcom/android/server/tv/DataBaseDesk$MEMBER_COUNTRY;

    new-instance v0, Lcom/android/server/tv/DataBaseDesk$MEMBER_COUNTRY;

    const-string v1, "E_ARGENTINA"

    const/16 v2, 0x28

    invoke-direct {v0, v1, v2}, Lcom/android/server/tv/DataBaseDesk$MEMBER_COUNTRY;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/android/server/tv/DataBaseDesk$MEMBER_COUNTRY;->E_ARGENTINA:Lcom/android/server/tv/DataBaseDesk$MEMBER_COUNTRY;

    new-instance v0, Lcom/android/server/tv/DataBaseDesk$MEMBER_COUNTRY;

    const-string v1, "E_CHILE"

    const/16 v2, 0x29

    invoke-direct {v0, v1, v2}, Lcom/android/server/tv/DataBaseDesk$MEMBER_COUNTRY;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/android/server/tv/DataBaseDesk$MEMBER_COUNTRY;->E_CHILE:Lcom/android/server/tv/DataBaseDesk$MEMBER_COUNTRY;

    new-instance v0, Lcom/android/server/tv/DataBaseDesk$MEMBER_COUNTRY;

    const-string v1, "E_VENEZUELA"

    const/16 v2, 0x2a

    invoke-direct {v0, v1, v2}, Lcom/android/server/tv/DataBaseDesk$MEMBER_COUNTRY;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/android/server/tv/DataBaseDesk$MEMBER_COUNTRY;->E_VENEZUELA:Lcom/android/server/tv/DataBaseDesk$MEMBER_COUNTRY;

    new-instance v0, Lcom/android/server/tv/DataBaseDesk$MEMBER_COUNTRY;

    const-string v1, "E_ECUADOR"

    const/16 v2, 0x2b

    invoke-direct {v0, v1, v2}, Lcom/android/server/tv/DataBaseDesk$MEMBER_COUNTRY;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/android/server/tv/DataBaseDesk$MEMBER_COUNTRY;->E_ECUADOR:Lcom/android/server/tv/DataBaseDesk$MEMBER_COUNTRY;

    new-instance v0, Lcom/android/server/tv/DataBaseDesk$MEMBER_COUNTRY;

    const-string v1, "E_COSTARICA"

    const/16 v2, 0x2c

    invoke-direct {v0, v1, v2}, Lcom/android/server/tv/DataBaseDesk$MEMBER_COUNTRY;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/android/server/tv/DataBaseDesk$MEMBER_COUNTRY;->E_COSTARICA:Lcom/android/server/tv/DataBaseDesk$MEMBER_COUNTRY;

    new-instance v0, Lcom/android/server/tv/DataBaseDesk$MEMBER_COUNTRY;

    const-string v1, "E_PARAGUAY"

    const/16 v2, 0x2d

    invoke-direct {v0, v1, v2}, Lcom/android/server/tv/DataBaseDesk$MEMBER_COUNTRY;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/android/server/tv/DataBaseDesk$MEMBER_COUNTRY;->E_PARAGUAY:Lcom/android/server/tv/DataBaseDesk$MEMBER_COUNTRY;

    new-instance v0, Lcom/android/server/tv/DataBaseDesk$MEMBER_COUNTRY;

    const-string v1, "E_BOLIVIA"

    const/16 v2, 0x2e

    invoke-direct {v0, v1, v2}, Lcom/android/server/tv/DataBaseDesk$MEMBER_COUNTRY;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/android/server/tv/DataBaseDesk$MEMBER_COUNTRY;->E_BOLIVIA:Lcom/android/server/tv/DataBaseDesk$MEMBER_COUNTRY;

    new-instance v0, Lcom/android/server/tv/DataBaseDesk$MEMBER_COUNTRY;

    const-string v1, "E_BELIZE"

    const/16 v2, 0x2f

    invoke-direct {v0, v1, v2}, Lcom/android/server/tv/DataBaseDesk$MEMBER_COUNTRY;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/android/server/tv/DataBaseDesk$MEMBER_COUNTRY;->E_BELIZE:Lcom/android/server/tv/DataBaseDesk$MEMBER_COUNTRY;

    new-instance v0, Lcom/android/server/tv/DataBaseDesk$MEMBER_COUNTRY;

    const-string v1, "E_NICARAGUA"

    const/16 v2, 0x30

    invoke-direct {v0, v1, v2}, Lcom/android/server/tv/DataBaseDesk$MEMBER_COUNTRY;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/android/server/tv/DataBaseDesk$MEMBER_COUNTRY;->E_NICARAGUA:Lcom/android/server/tv/DataBaseDesk$MEMBER_COUNTRY;

    new-instance v0, Lcom/android/server/tv/DataBaseDesk$MEMBER_COUNTRY;

    const-string v1, "E_GUATEMALA"

    const/16 v2, 0x31

    invoke-direct {v0, v1, v2}, Lcom/android/server/tv/DataBaseDesk$MEMBER_COUNTRY;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/android/server/tv/DataBaseDesk$MEMBER_COUNTRY;->E_GUATEMALA:Lcom/android/server/tv/DataBaseDesk$MEMBER_COUNTRY;

    new-instance v0, Lcom/android/server/tv/DataBaseDesk$MEMBER_COUNTRY;

    const-string v1, "E_CHINA"

    const/16 v2, 0x32

    invoke-direct {v0, v1, v2}, Lcom/android/server/tv/DataBaseDesk$MEMBER_COUNTRY;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/android/server/tv/DataBaseDesk$MEMBER_COUNTRY;->E_CHINA:Lcom/android/server/tv/DataBaseDesk$MEMBER_COUNTRY;

    new-instance v0, Lcom/android/server/tv/DataBaseDesk$MEMBER_COUNTRY;

    const-string v1, "E_TAIWAN"

    const/16 v2, 0x33

    invoke-direct {v0, v1, v2}, Lcom/android/server/tv/DataBaseDesk$MEMBER_COUNTRY;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/android/server/tv/DataBaseDesk$MEMBER_COUNTRY;->E_TAIWAN:Lcom/android/server/tv/DataBaseDesk$MEMBER_COUNTRY;

    new-instance v0, Lcom/android/server/tv/DataBaseDesk$MEMBER_COUNTRY;

    const-string v1, "E_BRAZIL"

    const/16 v2, 0x34

    invoke-direct {v0, v1, v2}, Lcom/android/server/tv/DataBaseDesk$MEMBER_COUNTRY;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/android/server/tv/DataBaseDesk$MEMBER_COUNTRY;->E_BRAZIL:Lcom/android/server/tv/DataBaseDesk$MEMBER_COUNTRY;

    new-instance v0, Lcom/android/server/tv/DataBaseDesk$MEMBER_COUNTRY;

    const-string v1, "E_CANADA"

    const/16 v2, 0x35

    invoke-direct {v0, v1, v2}, Lcom/android/server/tv/DataBaseDesk$MEMBER_COUNTRY;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/android/server/tv/DataBaseDesk$MEMBER_COUNTRY;->E_CANADA:Lcom/android/server/tv/DataBaseDesk$MEMBER_COUNTRY;

    new-instance v0, Lcom/android/server/tv/DataBaseDesk$MEMBER_COUNTRY;

    const-string v1, "E_MEXICO"

    const/16 v2, 0x36

    invoke-direct {v0, v1, v2}, Lcom/android/server/tv/DataBaseDesk$MEMBER_COUNTRY;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/android/server/tv/DataBaseDesk$MEMBER_COUNTRY;->E_MEXICO:Lcom/android/server/tv/DataBaseDesk$MEMBER_COUNTRY;

    new-instance v0, Lcom/android/server/tv/DataBaseDesk$MEMBER_COUNTRY;

    const-string v1, "E_US"

    const/16 v2, 0x37

    invoke-direct {v0, v1, v2}, Lcom/android/server/tv/DataBaseDesk$MEMBER_COUNTRY;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/android/server/tv/DataBaseDesk$MEMBER_COUNTRY;->E_US:Lcom/android/server/tv/DataBaseDesk$MEMBER_COUNTRY;

    new-instance v0, Lcom/android/server/tv/DataBaseDesk$MEMBER_COUNTRY;

    const-string v1, "E_SOUTHKOREA"

    const/16 v2, 0x38

    invoke-direct {v0, v1, v2}, Lcom/android/server/tv/DataBaseDesk$MEMBER_COUNTRY;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/android/server/tv/DataBaseDesk$MEMBER_COUNTRY;->E_SOUTHKOREA:Lcom/android/server/tv/DataBaseDesk$MEMBER_COUNTRY;

    new-instance v0, Lcom/android/server/tv/DataBaseDesk$MEMBER_COUNTRY;

    const-string v1, "E_OTHERS"

    const/16 v2, 0x39

    invoke-direct {v0, v1, v2}, Lcom/android/server/tv/DataBaseDesk$MEMBER_COUNTRY;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/android/server/tv/DataBaseDesk$MEMBER_COUNTRY;->E_OTHERS:Lcom/android/server/tv/DataBaseDesk$MEMBER_COUNTRY;

    new-instance v0, Lcom/android/server/tv/DataBaseDesk$MEMBER_COUNTRY;

    const-string v1, "E_COUNTRY_NUM"

    const/16 v2, 0x3a

    invoke-direct {v0, v1, v2}, Lcom/android/server/tv/DataBaseDesk$MEMBER_COUNTRY;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/android/server/tv/DataBaseDesk$MEMBER_COUNTRY;->E_COUNTRY_NUM:Lcom/android/server/tv/DataBaseDesk$MEMBER_COUNTRY;

    const/16 v0, 0x3b

    new-array v0, v0, [Lcom/android/server/tv/DataBaseDesk$MEMBER_COUNTRY;

    sget-object v1, Lcom/android/server/tv/DataBaseDesk$MEMBER_COUNTRY;->E_AUSTRALIA:Lcom/android/server/tv/DataBaseDesk$MEMBER_COUNTRY;

    aput-object v1, v0, v3

    sget-object v1, Lcom/android/server/tv/DataBaseDesk$MEMBER_COUNTRY;->E_AUSTRIA:Lcom/android/server/tv/DataBaseDesk$MEMBER_COUNTRY;

    aput-object v1, v0, v4

    sget-object v1, Lcom/android/server/tv/DataBaseDesk$MEMBER_COUNTRY;->E_BELGIUM:Lcom/android/server/tv/DataBaseDesk$MEMBER_COUNTRY;

    aput-object v1, v0, v5

    sget-object v1, Lcom/android/server/tv/DataBaseDesk$MEMBER_COUNTRY;->E_BULGARIA:Lcom/android/server/tv/DataBaseDesk$MEMBER_COUNTRY;

    aput-object v1, v0, v6

    sget-object v1, Lcom/android/server/tv/DataBaseDesk$MEMBER_COUNTRY;->E_CROATIA:Lcom/android/server/tv/DataBaseDesk$MEMBER_COUNTRY;

    aput-object v1, v0, v7

    const/4 v1, 0x5

    sget-object v2, Lcom/android/server/tv/DataBaseDesk$MEMBER_COUNTRY;->E_CZECH:Lcom/android/server/tv/DataBaseDesk$MEMBER_COUNTRY;

    aput-object v2, v0, v1

    const/4 v1, 0x6

    sget-object v2, Lcom/android/server/tv/DataBaseDesk$MEMBER_COUNTRY;->E_DENMARK:Lcom/android/server/tv/DataBaseDesk$MEMBER_COUNTRY;

    aput-object v2, v0, v1

    const/4 v1, 0x7

    sget-object v2, Lcom/android/server/tv/DataBaseDesk$MEMBER_COUNTRY;->E_FINLAND:Lcom/android/server/tv/DataBaseDesk$MEMBER_COUNTRY;

    aput-object v2, v0, v1

    const/16 v1, 0x8

    sget-object v2, Lcom/android/server/tv/DataBaseDesk$MEMBER_COUNTRY;->E_FRANCE:Lcom/android/server/tv/DataBaseDesk$MEMBER_COUNTRY;

    aput-object v2, v0, v1

    const/16 v1, 0x9

    sget-object v2, Lcom/android/server/tv/DataBaseDesk$MEMBER_COUNTRY;->E_GERMANY:Lcom/android/server/tv/DataBaseDesk$MEMBER_COUNTRY;

    aput-object v2, v0, v1

    const/16 v1, 0xa

    sget-object v2, Lcom/android/server/tv/DataBaseDesk$MEMBER_COUNTRY;->E_GREECE:Lcom/android/server/tv/DataBaseDesk$MEMBER_COUNTRY;

    aput-object v2, v0, v1

    const/16 v1, 0xb

    sget-object v2, Lcom/android/server/tv/DataBaseDesk$MEMBER_COUNTRY;->E_HUNGARY:Lcom/android/server/tv/DataBaseDesk$MEMBER_COUNTRY;

    aput-object v2, v0, v1

    const/16 v1, 0xc

    sget-object v2, Lcom/android/server/tv/DataBaseDesk$MEMBER_COUNTRY;->E_ITALY:Lcom/android/server/tv/DataBaseDesk$MEMBER_COUNTRY;

    aput-object v2, v0, v1

    const/16 v1, 0xd

    sget-object v2, Lcom/android/server/tv/DataBaseDesk$MEMBER_COUNTRY;->E_LUXEMBOURG:Lcom/android/server/tv/DataBaseDesk$MEMBER_COUNTRY;

    aput-object v2, v0, v1

    const/16 v1, 0xe

    sget-object v2, Lcom/android/server/tv/DataBaseDesk$MEMBER_COUNTRY;->E_NETHERLANDS:Lcom/android/server/tv/DataBaseDesk$MEMBER_COUNTRY;

    aput-object v2, v0, v1

    const/16 v1, 0xf

    sget-object v2, Lcom/android/server/tv/DataBaseDesk$MEMBER_COUNTRY;->E_NORWAY:Lcom/android/server/tv/DataBaseDesk$MEMBER_COUNTRY;

    aput-object v2, v0, v1

    const/16 v1, 0x10

    sget-object v2, Lcom/android/server/tv/DataBaseDesk$MEMBER_COUNTRY;->E_POLAND:Lcom/android/server/tv/DataBaseDesk$MEMBER_COUNTRY;

    aput-object v2, v0, v1

    const/16 v1, 0x11

    sget-object v2, Lcom/android/server/tv/DataBaseDesk$MEMBER_COUNTRY;->E_PORTUGAL:Lcom/android/server/tv/DataBaseDesk$MEMBER_COUNTRY;

    aput-object v2, v0, v1

    const/16 v1, 0x12

    sget-object v2, Lcom/android/server/tv/DataBaseDesk$MEMBER_COUNTRY;->E_RUMANIA:Lcom/android/server/tv/DataBaseDesk$MEMBER_COUNTRY;

    aput-object v2, v0, v1

    const/16 v1, 0x13

    sget-object v2, Lcom/android/server/tv/DataBaseDesk$MEMBER_COUNTRY;->E_RUSSIA:Lcom/android/server/tv/DataBaseDesk$MEMBER_COUNTRY;

    aput-object v2, v0, v1

    const/16 v1, 0x14

    sget-object v2, Lcom/android/server/tv/DataBaseDesk$MEMBER_COUNTRY;->E_SERBIA:Lcom/android/server/tv/DataBaseDesk$MEMBER_COUNTRY;

    aput-object v2, v0, v1

    const/16 v1, 0x15

    sget-object v2, Lcom/android/server/tv/DataBaseDesk$MEMBER_COUNTRY;->E_SLOVENIA:Lcom/android/server/tv/DataBaseDesk$MEMBER_COUNTRY;

    aput-object v2, v0, v1

    const/16 v1, 0x16

    sget-object v2, Lcom/android/server/tv/DataBaseDesk$MEMBER_COUNTRY;->E_SPAIN:Lcom/android/server/tv/DataBaseDesk$MEMBER_COUNTRY;

    aput-object v2, v0, v1

    const/16 v1, 0x17

    sget-object v2, Lcom/android/server/tv/DataBaseDesk$MEMBER_COUNTRY;->E_SWEDEN:Lcom/android/server/tv/DataBaseDesk$MEMBER_COUNTRY;

    aput-object v2, v0, v1

    const/16 v1, 0x18

    sget-object v2, Lcom/android/server/tv/DataBaseDesk$MEMBER_COUNTRY;->E_SWITZERLAND:Lcom/android/server/tv/DataBaseDesk$MEMBER_COUNTRY;

    aput-object v2, v0, v1

    const/16 v1, 0x19

    sget-object v2, Lcom/android/server/tv/DataBaseDesk$MEMBER_COUNTRY;->E_UK:Lcom/android/server/tv/DataBaseDesk$MEMBER_COUNTRY;

    aput-object v2, v0, v1

    const/16 v1, 0x1a

    sget-object v2, Lcom/android/server/tv/DataBaseDesk$MEMBER_COUNTRY;->E_NEWZEALAND:Lcom/android/server/tv/DataBaseDesk$MEMBER_COUNTRY;

    aput-object v2, v0, v1

    const/16 v1, 0x1b

    sget-object v2, Lcom/android/server/tv/DataBaseDesk$MEMBER_COUNTRY;->E_ARAB:Lcom/android/server/tv/DataBaseDesk$MEMBER_COUNTRY;

    aput-object v2, v0, v1

    const/16 v1, 0x1c

    sget-object v2, Lcom/android/server/tv/DataBaseDesk$MEMBER_COUNTRY;->E_ESTONIA:Lcom/android/server/tv/DataBaseDesk$MEMBER_COUNTRY;

    aput-object v2, v0, v1

    const/16 v1, 0x1d

    sget-object v2, Lcom/android/server/tv/DataBaseDesk$MEMBER_COUNTRY;->E_HEBREW:Lcom/android/server/tv/DataBaseDesk$MEMBER_COUNTRY;

    aput-object v2, v0, v1

    const/16 v1, 0x1e

    sget-object v2, Lcom/android/server/tv/DataBaseDesk$MEMBER_COUNTRY;->E_LATVIA:Lcom/android/server/tv/DataBaseDesk$MEMBER_COUNTRY;

    aput-object v2, v0, v1

    const/16 v1, 0x1f

    sget-object v2, Lcom/android/server/tv/DataBaseDesk$MEMBER_COUNTRY;->E_SLOVAKIA:Lcom/android/server/tv/DataBaseDesk$MEMBER_COUNTRY;

    aput-object v2, v0, v1

    const/16 v1, 0x20

    sget-object v2, Lcom/android/server/tv/DataBaseDesk$MEMBER_COUNTRY;->E_TURKEY:Lcom/android/server/tv/DataBaseDesk$MEMBER_COUNTRY;

    aput-object v2, v0, v1

    const/16 v1, 0x21

    sget-object v2, Lcom/android/server/tv/DataBaseDesk$MEMBER_COUNTRY;->E_IRELAND:Lcom/android/server/tv/DataBaseDesk$MEMBER_COUNTRY;

    aput-object v2, v0, v1

    const/16 v1, 0x22

    sget-object v2, Lcom/android/server/tv/DataBaseDesk$MEMBER_COUNTRY;->E_JAPAN:Lcom/android/server/tv/DataBaseDesk$MEMBER_COUNTRY;

    aput-object v2, v0, v1

    const/16 v1, 0x23

    sget-object v2, Lcom/android/server/tv/DataBaseDesk$MEMBER_COUNTRY;->E_PHILIPPINES:Lcom/android/server/tv/DataBaseDesk$MEMBER_COUNTRY;

    aput-object v2, v0, v1

    const/16 v1, 0x24

    sget-object v2, Lcom/android/server/tv/DataBaseDesk$MEMBER_COUNTRY;->E_THAILAND:Lcom/android/server/tv/DataBaseDesk$MEMBER_COUNTRY;

    aput-object v2, v0, v1

    const/16 v1, 0x25

    sget-object v2, Lcom/android/server/tv/DataBaseDesk$MEMBER_COUNTRY;->E_MALDIVES:Lcom/android/server/tv/DataBaseDesk$MEMBER_COUNTRY;

    aput-object v2, v0, v1

    const/16 v1, 0x26

    sget-object v2, Lcom/android/server/tv/DataBaseDesk$MEMBER_COUNTRY;->E_URUGUAY:Lcom/android/server/tv/DataBaseDesk$MEMBER_COUNTRY;

    aput-object v2, v0, v1

    const/16 v1, 0x27

    sget-object v2, Lcom/android/server/tv/DataBaseDesk$MEMBER_COUNTRY;->E_PERU:Lcom/android/server/tv/DataBaseDesk$MEMBER_COUNTRY;

    aput-object v2, v0, v1

    const/16 v1, 0x28

    sget-object v2, Lcom/android/server/tv/DataBaseDesk$MEMBER_COUNTRY;->E_ARGENTINA:Lcom/android/server/tv/DataBaseDesk$MEMBER_COUNTRY;

    aput-object v2, v0, v1

    const/16 v1, 0x29

    sget-object v2, Lcom/android/server/tv/DataBaseDesk$MEMBER_COUNTRY;->E_CHILE:Lcom/android/server/tv/DataBaseDesk$MEMBER_COUNTRY;

    aput-object v2, v0, v1

    const/16 v1, 0x2a

    sget-object v2, Lcom/android/server/tv/DataBaseDesk$MEMBER_COUNTRY;->E_VENEZUELA:Lcom/android/server/tv/DataBaseDesk$MEMBER_COUNTRY;

    aput-object v2, v0, v1

    const/16 v1, 0x2b

    sget-object v2, Lcom/android/server/tv/DataBaseDesk$MEMBER_COUNTRY;->E_ECUADOR:Lcom/android/server/tv/DataBaseDesk$MEMBER_COUNTRY;

    aput-object v2, v0, v1

    const/16 v1, 0x2c

    sget-object v2, Lcom/android/server/tv/DataBaseDesk$MEMBER_COUNTRY;->E_COSTARICA:Lcom/android/server/tv/DataBaseDesk$MEMBER_COUNTRY;

    aput-object v2, v0, v1

    const/16 v1, 0x2d

    sget-object v2, Lcom/android/server/tv/DataBaseDesk$MEMBER_COUNTRY;->E_PARAGUAY:Lcom/android/server/tv/DataBaseDesk$MEMBER_COUNTRY;

    aput-object v2, v0, v1

    const/16 v1, 0x2e

    sget-object v2, Lcom/android/server/tv/DataBaseDesk$MEMBER_COUNTRY;->E_BOLIVIA:Lcom/android/server/tv/DataBaseDesk$MEMBER_COUNTRY;

    aput-object v2, v0, v1

    const/16 v1, 0x2f

    sget-object v2, Lcom/android/server/tv/DataBaseDesk$MEMBER_COUNTRY;->E_BELIZE:Lcom/android/server/tv/DataBaseDesk$MEMBER_COUNTRY;

    aput-object v2, v0, v1

    const/16 v1, 0x30

    sget-object v2, Lcom/android/server/tv/DataBaseDesk$MEMBER_COUNTRY;->E_NICARAGUA:Lcom/android/server/tv/DataBaseDesk$MEMBER_COUNTRY;

    aput-object v2, v0, v1

    const/16 v1, 0x31

    sget-object v2, Lcom/android/server/tv/DataBaseDesk$MEMBER_COUNTRY;->E_GUATEMALA:Lcom/android/server/tv/DataBaseDesk$MEMBER_COUNTRY;

    aput-object v2, v0, v1

    const/16 v1, 0x32

    sget-object v2, Lcom/android/server/tv/DataBaseDesk$MEMBER_COUNTRY;->E_CHINA:Lcom/android/server/tv/DataBaseDesk$MEMBER_COUNTRY;

    aput-object v2, v0, v1

    const/16 v1, 0x33

    sget-object v2, Lcom/android/server/tv/DataBaseDesk$MEMBER_COUNTRY;->E_TAIWAN:Lcom/android/server/tv/DataBaseDesk$MEMBER_COUNTRY;

    aput-object v2, v0, v1

    const/16 v1, 0x34

    sget-object v2, Lcom/android/server/tv/DataBaseDesk$MEMBER_COUNTRY;->E_BRAZIL:Lcom/android/server/tv/DataBaseDesk$MEMBER_COUNTRY;

    aput-object v2, v0, v1

    const/16 v1, 0x35

    sget-object v2, Lcom/android/server/tv/DataBaseDesk$MEMBER_COUNTRY;->E_CANADA:Lcom/android/server/tv/DataBaseDesk$MEMBER_COUNTRY;

    aput-object v2, v0, v1

    const/16 v1, 0x36

    sget-object v2, Lcom/android/server/tv/DataBaseDesk$MEMBER_COUNTRY;->E_MEXICO:Lcom/android/server/tv/DataBaseDesk$MEMBER_COUNTRY;

    aput-object v2, v0, v1

    const/16 v1, 0x37

    sget-object v2, Lcom/android/server/tv/DataBaseDesk$MEMBER_COUNTRY;->E_US:Lcom/android/server/tv/DataBaseDesk$MEMBER_COUNTRY;

    aput-object v2, v0, v1

    const/16 v1, 0x38

    sget-object v2, Lcom/android/server/tv/DataBaseDesk$MEMBER_COUNTRY;->E_SOUTHKOREA:Lcom/android/server/tv/DataBaseDesk$MEMBER_COUNTRY;

    aput-object v2, v0, v1

    const/16 v1, 0x39

    sget-object v2, Lcom/android/server/tv/DataBaseDesk$MEMBER_COUNTRY;->E_OTHERS:Lcom/android/server/tv/DataBaseDesk$MEMBER_COUNTRY;

    aput-object v2, v0, v1

    const/16 v1, 0x3a

    sget-object v2, Lcom/android/server/tv/DataBaseDesk$MEMBER_COUNTRY;->E_COUNTRY_NUM:Lcom/android/server/tv/DataBaseDesk$MEMBER_COUNTRY;

    aput-object v2, v0, v1

    sput-object v0, Lcom/android/server/tv/DataBaseDesk$MEMBER_COUNTRY;->$VALUES:[Lcom/android/server/tv/DataBaseDesk$MEMBER_COUNTRY;

    return-void
.end method

.method private constructor <init>(Ljava/lang/String;I)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()V"
        }
    .end annotation

    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    return-void
.end method

.method public static valueOf(Ljava/lang/String;)Lcom/android/server/tv/DataBaseDesk$MEMBER_COUNTRY;
    .locals 1
    .param p0    # Ljava/lang/String;

    const-class v0, Lcom/android/server/tv/DataBaseDesk$MEMBER_COUNTRY;

    invoke-static {v0, p0}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;

    move-result-object v0

    check-cast v0, Lcom/android/server/tv/DataBaseDesk$MEMBER_COUNTRY;

    return-object v0
.end method

.method public static values()[Lcom/android/server/tv/DataBaseDesk$MEMBER_COUNTRY;
    .locals 1

    sget-object v0, Lcom/android/server/tv/DataBaseDesk$MEMBER_COUNTRY;->$VALUES:[Lcom/android/server/tv/DataBaseDesk$MEMBER_COUNTRY;

    invoke-virtual {v0}, [Lcom/android/server/tv/DataBaseDesk$MEMBER_COUNTRY;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [Lcom/android/server/tv/DataBaseDesk$MEMBER_COUNTRY;

    return-object v0
.end method
