.class Lcom/android/server/tv/TvService$2;
.super Landroid/content/BroadcastReceiver;
.source "TvService.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/android/server/tv/TvService;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/android/server/tv/TvService;


# direct methods
.method constructor <init>(Lcom/android/server/tv/TvService;)V
    .locals 0

    iput-object p1, p0, Lcom/android/server/tv/TvService$2;->this$0:Lcom/android/server/tv/TvService;

    invoke-direct {p0}, Landroid/content/BroadcastReceiver;-><init>()V

    return-void
.end method


# virtual methods
.method public onReceive(Landroid/content/Context;Landroid/content/Intent;)V
    .locals 12
    .param p1    # Landroid/content/Context;
    .param p2    # Landroid/content/Intent;

    const/4 v7, 0x0

    const/4 v8, 0x0

    :try_start_0
    invoke-virtual {p2}, Landroid/content/Intent;->getAction()Ljava/lang/String;

    move-result-object v0

    const-string v10, "com.mstar.android.intent.action.PICTURE_MODE_BUTTON"

    invoke-virtual {v0, v10}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v10

    if-eqz v10, :cond_1

    const-string v10, "TvService"

    const-string v11, "------------HotKey------ACTION_PICTURE_MODE_BUTTON"

    invoke-static {v10, v11}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    iget-object v10, p0, Lcom/android/server/tv/TvService$2;->this$0:Lcom/android/server/tv/TvService;

    # getter for: Lcom/android/server/tv/TvService;->mViewHandler:Lcom/android/server/tv/TvHanlder;
    invoke-static {v10}, Lcom/android/server/tv/TvService;->access$400(Lcom/android/server/tv/TvService;)Lcom/android/server/tv/TvHanlder;

    move-result-object v10

    invoke-virtual {v10}, Lcom/android/server/tv/TvHanlder;->obtainMessage()Landroid/os/Message;

    move-result-object v9

    const/16 v10, 0x14d

    iput v10, v9, Landroid/os/Message;->what:I

    invoke-virtual {v9}, Landroid/os/Message;->sendToTarget()V

    const/4 v7, 0x1

    :cond_0
    :goto_0
    return-void

    :cond_1
    const-string v10, "com.mstar.android.intent.action.SOUND_MODE_BUTTON"

    invoke-virtual {v0, v10}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v10

    if-eqz v10, :cond_2

    const-string v10, "TvService"

    const-string v11, "------------HotKey------ACTION_SOUND_MODE_BUTTON"

    invoke-static {v10, v11}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    iget-object v10, p0, Lcom/android/server/tv/TvService$2;->this$0:Lcom/android/server/tv/TvService;

    # getter for: Lcom/android/server/tv/TvService;->mViewHandler:Lcom/android/server/tv/TvHanlder;
    invoke-static {v10}, Lcom/android/server/tv/TvService;->access$400(Lcom/android/server/tv/TvService;)Lcom/android/server/tv/TvHanlder;

    move-result-object v10

    invoke-virtual {v10}, Lcom/android/server/tv/TvHanlder;->obtainMessage()Landroid/os/Message;

    move-result-object v9

    const/16 v10, 0x309

    iput v10, v9, Landroid/os/Message;->what:I

    invoke-virtual {v9}, Landroid/os/Message;->sendToTarget()V

    const/4 v7, 0x1

    goto :goto_0

    :cond_2
    const-string v10, "com.mstar.android.intent.action.ASPECT_RATIO_BUTTON"

    invoke-virtual {v0, v10}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v10

    if-eqz v10, :cond_3

    const-string v10, "TvService"

    const-string v11, "------------HotKey------ACTION_ASPECT_RATIO_BUTTON"

    invoke-static {v10, v11}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    iget-object v10, p0, Lcom/android/server/tv/TvService$2;->this$0:Lcom/android/server/tv/TvService;

    # getter for: Lcom/android/server/tv/TvService;->mViewHandler:Lcom/android/server/tv/TvHanlder;
    invoke-static {v10}, Lcom/android/server/tv/TvService;->access$400(Lcom/android/server/tv/TvService;)Lcom/android/server/tv/TvHanlder;

    move-result-object v10

    invoke-virtual {v10}, Lcom/android/server/tv/TvHanlder;->obtainMessage()Landroid/os/Message;

    move-result-object v9

    const/16 v10, 0x29a

    iput v10, v9, Landroid/os/Message;->what:I

    invoke-virtual {v9}, Landroid/os/Message;->sendToTarget()V

    const/4 v7, 0x1

    goto :goto_0

    :cond_3
    const-string v10, "com.mstar.android.intent.action.SLEEP_BUTTON"

    invoke-virtual {v0, v10}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v10

    if-eqz v10, :cond_4

    const-string v10, "TvService"

    const-string v11, "------------HotKey------ACTION_SLEEP_BUTTON"

    invoke-static {v10, v11}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    iget-object v10, p0, Lcom/android/server/tv/TvService$2;->this$0:Lcom/android/server/tv/TvService;

    # getter for: Lcom/android/server/tv/TvService;->mViewHandler:Lcom/android/server/tv/TvHanlder;
    invoke-static {v10}, Lcom/android/server/tv/TvService;->access$400(Lcom/android/server/tv/TvService;)Lcom/android/server/tv/TvHanlder;

    move-result-object v10

    invoke-virtual {v10}, Lcom/android/server/tv/TvHanlder;->obtainMessage()Landroid/os/Message;

    move-result-object v9

    const/16 v10, 0x378

    iput v10, v9, Landroid/os/Message;->what:I

    invoke-virtual {v9}, Landroid/os/Message;->sendToTarget()V

    const/4 v7, 0x1

    goto :goto_0

    :cond_4
    const-string v10, "com.konka.tv.hotkey.service.MUTE"

    invoke-virtual {v0, v10}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v10

    if-eqz v10, :cond_5

    const-string v10, "TvService"

    const-string v11, "------------HotKey------MUTE"

    invoke-static {v10, v11}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    iget-object v10, p0, Lcom/android/server/tv/TvService$2;->this$0:Lcom/android/server/tv/TvService;

    # getter for: Lcom/android/server/tv/TvService;->mViewHandler:Lcom/android/server/tv/TvHanlder;
    invoke-static {v10}, Lcom/android/server/tv/TvService;->access$400(Lcom/android/server/tv/TvService;)Lcom/android/server/tv/TvHanlder;

    move-result-object v10

    invoke-virtual {v10}, Lcom/android/server/tv/TvHanlder;->obtainMessage()Landroid/os/Message;

    move-result-object v9

    const/4 v10, 0x7

    iput v10, v9, Landroid/os/Message;->what:I

    invoke-virtual {v9}, Landroid/os/Message;->sendToTarget()V

    const/4 v7, 0x1

    goto/16 :goto_0

    :cond_5
    const-string v10, "com.konka.tv.hotkey.service.UNMUTE"

    invoke-virtual {v0, v10}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v10

    if-eqz v10, :cond_6

    const-string v10, "TvService"

    const-string v11, "------------HotKey------UNMUTE"

    invoke-static {v10, v11}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    iget-object v10, p0, Lcom/android/server/tv/TvService$2;->this$0:Lcom/android/server/tv/TvService;

    # getter for: Lcom/android/server/tv/TvService;->mViewHandler:Lcom/android/server/tv/TvHanlder;
    invoke-static {v10}, Lcom/android/server/tv/TvService;->access$400(Lcom/android/server/tv/TvService;)Lcom/android/server/tv/TvHanlder;

    move-result-object v10

    invoke-virtual {v10}, Lcom/android/server/tv/TvHanlder;->obtainMessage()Landroid/os/Message;

    move-result-object v9

    const/16 v10, 0x8

    iput v10, v9, Landroid/os/Message;->what:I

    invoke-virtual {v9}, Landroid/os/Message;->sendToTarget()V

    const/4 v7, 0x1

    goto/16 :goto_0

    :cond_6
    const-string v10, "com.konka.tv.hotkey.service.FREEZE"

    invoke-virtual {v0, v10}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v10

    if-eqz v10, :cond_b

    const-string v10, "TvService"

    const-string v11, "------------HotKey------FREEZE"

    invoke-static {v10, v11}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    iget-object v10, p0, Lcom/android/server/tv/TvService$2;->this$0:Lcom/android/server/tv/TvService;

    # getter for: Lcom/android/server/tv/TvService;->mContext:Landroid/content/Context;
    invoke-static {v10}, Lcom/android/server/tv/TvService;->access$000(Lcom/android/server/tv/TvService;)Landroid/content/Context;

    move-result-object v10

    const-string v11, "activity"

    invoke-virtual {v10, v11}, Landroid/content/Context;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Landroid/app/ActivityManager;

    const/4 v10, 0x2

    invoke-virtual {v1, v10}, Landroid/app/ActivityManager;->getRunningTasks(I)Ljava/util/List;

    move-result-object v10

    const/4 v11, 0x0

    invoke-interface {v10, v11}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v10

    check-cast v10, Landroid/app/ActivityManager$RunningTaskInfo;

    iget-object v4, v10, Landroid/app/ActivityManager$RunningTaskInfo;->topActivity:Landroid/content/ComponentName;

    const-string v10, "com.konka.karaoke"

    invoke-virtual {v4}, Landroid/content/ComponentName;->getPackageName()Ljava/lang/String;

    move-result-object v11

    invoke-virtual {v10, v11}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v10

    if-nez v10, :cond_7

    const-string v10, "com.konka.movietheater"

    invoke-virtual {v4}, Landroid/content/ComponentName;->getPackageName()Ljava/lang/String;

    move-result-object v11

    invoke-virtual {v10, v11}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v10

    if-nez v10, :cond_7

    const-string v10, "com.konka.mm"

    invoke-virtual {v4}, Landroid/content/ComponentName;->getPackageName()Ljava/lang/String;

    move-result-object v11

    invoke-virtual {v10, v11}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v10

    if-nez v10, :cond_7

    const-string v10, "com.konka.vedioplayer"

    invoke-virtual {v4}, Landroid/content/ComponentName;->getPackageName()Ljava/lang/String;

    move-result-object v11

    invoke-virtual {v10, v11}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z
    :try_end_0
    .catch Ljava/lang/Throwable; {:try_start_0 .. :try_end_0} :catch_1

    move-result v10

    if-nez v10, :cond_7

    :try_start_1
    invoke-static {}, Lcom/mstar/android/tvapi/common/TvManager;->getInstance()Lcom/mstar/android/tvapi/common/TvManager;

    move-result-object v10

    invoke-virtual {v10}, Lcom/mstar/android/tvapi/common/TvManager;->getThreeDimensionManager()Lcom/mstar/android/tvapi/common/ThreeDimensionManager;

    move-result-object v10

    invoke-virtual {v10}, Lcom/mstar/android/tvapi/common/ThreeDimensionManager;->getCurrent3dFormat()Lcom/mstar/android/tvapi/common/vo/Enum3dType;

    move-result-object v6

    sget-object v10, Lcom/mstar/android/tvapi/common/vo/Enum3dType;->EN_3D_NONE:Lcom/mstar/android/tvapi/common/vo/Enum3dType;

    if-ne v6, v10, :cond_a

    invoke-static {}, Lcom/android/server/tv/TvService;->get4K2KMode()Z

    move-result v10

    if-nez v10, :cond_a

    iget-object v11, p0, Lcom/android/server/tv/TvService$2;->this$0:Lcom/android/server/tv/TvService;

    iget-object v10, p0, Lcom/android/server/tv/TvService$2;->this$0:Lcom/android/server/tv/TvService;

    # getter for: Lcom/android/server/tv/TvService;->freezeImgFlag:Z
    invoke-static {v10}, Lcom/android/server/tv/TvService;->access$500(Lcom/android/server/tv/TvService;)Z

    move-result v10

    if-nez v10, :cond_8

    const/4 v10, 0x1

    :goto_1
    # setter for: Lcom/android/server/tv/TvService;->freezeImgFlag:Z
    invoke-static {v11, v10}, Lcom/android/server/tv/TvService;->access$502(Lcom/android/server/tv/TvService;Z)Z

    iget-object v10, p0, Lcom/android/server/tv/TvService$2;->this$0:Lcom/android/server/tv/TvService;

    # getter for: Lcom/android/server/tv/TvService;->freezeImgFlag:Z
    invoke-static {v10}, Lcom/android/server/tv/TvService;->access$500(Lcom/android/server/tv/TvService;)Z

    move-result v10

    if-eqz v10, :cond_9

    iget-object v10, p0, Lcom/android/server/tv/TvService$2;->this$0:Lcom/android/server/tv/TvService;

    invoke-virtual {v10}, Lcom/android/server/tv/TvService;->getTvPicture()Lcom/mstar/android/tv/ITvPicture;

    move-result-object v10

    invoke-interface {v10}, Lcom/mstar/android/tv/ITvPicture;->freezeImage()Z

    iget-object v10, p0, Lcom/android/server/tv/TvService$2;->this$0:Lcom/android/server/tv/TvService;

    # getter for: Lcom/android/server/tv/TvService;->mViewHandler:Lcom/android/server/tv/TvHanlder;
    invoke-static {v10}, Lcom/android/server/tv/TvService;->access$400(Lcom/android/server/tv/TvService;)Lcom/android/server/tv/TvHanlder;

    move-result-object v10

    invoke-virtual {v10}, Lcom/android/server/tv/TvHanlder;->obtainMessage()Landroid/os/Message;

    move-result-object v9

    const/16 v10, 0x9

    iput v10, v9, Landroid/os/Message;->what:I

    invoke-virtual {v9}, Landroid/os/Message;->sendToTarget()V

    :cond_7
    :goto_2
    const/4 v7, 0x1

    goto/16 :goto_0

    :cond_8
    const/4 v10, 0x0

    goto :goto_1

    :cond_9
    iget-object v10, p0, Lcom/android/server/tv/TvService$2;->this$0:Lcom/android/server/tv/TvService;

    invoke-virtual {v10}, Lcom/android/server/tv/TvService;->getTvPicture()Lcom/mstar/android/tv/ITvPicture;

    move-result-object v10

    invoke-interface {v10}, Lcom/mstar/android/tv/ITvPicture;->unFreezeImage()Z

    iget-object v10, p0, Lcom/android/server/tv/TvService$2;->this$0:Lcom/android/server/tv/TvService;

    # getter for: Lcom/android/server/tv/TvService;->mViewHandler:Lcom/android/server/tv/TvHanlder;
    invoke-static {v10}, Lcom/android/server/tv/TvService;->access$400(Lcom/android/server/tv/TvService;)Lcom/android/server/tv/TvHanlder;

    move-result-object v10

    invoke-virtual {v10}, Lcom/android/server/tv/TvHanlder;->obtainMessage()Landroid/os/Message;

    move-result-object v9

    const/16 v10, 0xa

    iput v10, v9, Landroid/os/Message;->what:I

    invoke-virtual {v9}, Landroid/os/Message;->sendToTarget()V

    goto :goto_2

    :catch_0
    move-exception v10

    goto :goto_2

    :cond_a
    const-string v10, "TvService"

    const-string v11, "current is 3D or 4k2k, skip freezing action"

    invoke-static {v10, v11}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I
    :try_end_1
    .catch Lcom/mstar/android/tvapi/common/exception/TvCommonException; {:try_start_1 .. :try_end_1} :catch_0
    .catch Ljava/lang/Throwable; {:try_start_1 .. :try_end_1} :catch_1

    goto :goto_2

    :catch_1
    move-exception v5

    invoke-virtual {v5}, Ljava/lang/Throwable;->printStackTrace()V

    goto/16 :goto_0

    :cond_b
    :try_start_2
    const-string v10, "com.konka.tv.hotkey.service.UNFREEZE"

    invoke-virtual {v0, v10}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v10

    if-eqz v10, :cond_d

    const-string v10, "TvService"

    const-string v11, "------------HotKey------UNFREEZE"

    invoke-static {v10, v11}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    iget-object v10, p0, Lcom/android/server/tv/TvService$2;->this$0:Lcom/android/server/tv/TvService;

    # getter for: Lcom/android/server/tv/TvService;->freezeImgFlag:Z
    invoke-static {v10}, Lcom/android/server/tv/TvService;->access$500(Lcom/android/server/tv/TvService;)Z

    move-result v10

    if-eqz v10, :cond_c

    iget-object v10, p0, Lcom/android/server/tv/TvService$2;->this$0:Lcom/android/server/tv/TvService;

    const/4 v11, 0x0

    # setter for: Lcom/android/server/tv/TvService;->freezeImgFlag:Z
    invoke-static {v10, v11}, Lcom/android/server/tv/TvService;->access$502(Lcom/android/server/tv/TvService;Z)Z

    iget-object v10, p0, Lcom/android/server/tv/TvService$2;->this$0:Lcom/android/server/tv/TvService;

    invoke-virtual {v10}, Lcom/android/server/tv/TvService;->getTvPicture()Lcom/mstar/android/tv/ITvPicture;

    move-result-object v10

    invoke-interface {v10}, Lcom/mstar/android/tv/ITvPicture;->unFreezeImage()Z

    iget-object v10, p0, Lcom/android/server/tv/TvService$2;->this$0:Lcom/android/server/tv/TvService;

    # getter for: Lcom/android/server/tv/TvService;->mViewHandler:Lcom/android/server/tv/TvHanlder;
    invoke-static {v10}, Lcom/android/server/tv/TvService;->access$400(Lcom/android/server/tv/TvService;)Lcom/android/server/tv/TvHanlder;

    move-result-object v10

    invoke-virtual {v10}, Lcom/android/server/tv/TvHanlder;->obtainMessage()Landroid/os/Message;

    move-result-object v9

    const/16 v10, 0xa

    iput v10, v9, Landroid/os/Message;->what:I

    invoke-virtual {v9}, Landroid/os/Message;->sendToTarget()V

    :cond_c
    const/4 v7, 0x1

    goto/16 :goto_0

    :cond_d
    const-string v10, "com.konka.tv.hotkey.service.UNFREEZE.OnlyRemoveImg"

    invoke-virtual {v0, v10}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v10

    if-eqz v10, :cond_e

    const-string v10, "TvService"

    const-string v11, "------------HotKey------UNFREEZE, OnlyRemoveImg"

    invoke-static {v10, v11}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    iget-object v10, p0, Lcom/android/server/tv/TvService$2;->this$0:Lcom/android/server/tv/TvService;

    const/4 v11, 0x0

    # setter for: Lcom/android/server/tv/TvService;->freezeImgFlag:Z
    invoke-static {v10, v11}, Lcom/android/server/tv/TvService;->access$502(Lcom/android/server/tv/TvService;Z)Z

    iget-object v10, p0, Lcom/android/server/tv/TvService$2;->this$0:Lcom/android/server/tv/TvService;

    # getter for: Lcom/android/server/tv/TvService;->mViewHandler:Lcom/android/server/tv/TvHanlder;
    invoke-static {v10}, Lcom/android/server/tv/TvService;->access$400(Lcom/android/server/tv/TvService;)Lcom/android/server/tv/TvHanlder;

    move-result-object v10

    invoke-virtual {v10}, Lcom/android/server/tv/TvHanlder;->obtainMessage()Landroid/os/Message;

    move-result-object v9

    const/16 v10, 0xa

    iput v10, v9, Landroid/os/Message;->what:I

    invoke-virtual {v9}, Landroid/os/Message;->sendToTarget()V

    const/4 v7, 0x1

    goto/16 :goto_0

    :cond_e
    const-string v10, "com.konka.MOVIE_PLAY_STATUS"

    invoke-virtual {v10, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v10

    if-eqz v10, :cond_f

    invoke-virtual {v8}, Landroid/content/Intent;->getExtras()Landroid/os/Bundle;

    move-result-object v10

    if-eqz v10, :cond_0

    invoke-virtual {v8}, Landroid/content/Intent;->getExtras()Landroid/os/Bundle;

    move-result-object v3

    const/4 v10, 0x1

    const-string v11, "PlayStatus"

    invoke-virtual {v3, v11}, Landroid/os/Bundle;->getInt(Ljava/lang/String;)I

    move-result v11

    if-ne v10, v11, :cond_0

    iget-object v10, p0, Lcom/android/server/tv/TvService$2;->this$0:Lcom/android/server/tv/TvService;

    const/4 v11, 0x0

    # setter for: Lcom/android/server/tv/TvService;->freezeImgFlag:Z
    invoke-static {v10, v11}, Lcom/android/server/tv/TvService;->access$502(Lcom/android/server/tv/TvService;Z)Z

    iget-object v10, p0, Lcom/android/server/tv/TvService$2;->this$0:Lcom/android/server/tv/TvService;

    # getter for: Lcom/android/server/tv/TvService;->mViewHandler:Lcom/android/server/tv/TvHanlder;
    invoke-static {v10}, Lcom/android/server/tv/TvService;->access$400(Lcom/android/server/tv/TvService;)Lcom/android/server/tv/TvHanlder;

    move-result-object v10

    invoke-virtual {v10}, Lcom/android/server/tv/TvHanlder;->obtainMessage()Landroid/os/Message;

    move-result-object v9

    const/16 v10, 0xa

    iput v10, v9, Landroid/os/Message;->what:I

    invoke-virtual {v9}, Landroid/os/Message;->sendToTarget()V

    const/4 v7, 0x1

    goto/16 :goto_0

    :cond_f
    const-string v10, "konka.action.SET_VOLUME_UP"

    invoke-virtual {v10, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v10

    if-eqz v10, :cond_10

    const-string v10, "TvService"

    const-string v11, "TvService, volume up !"

    invoke-static {v10, v11}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    iget-object v10, p0, Lcom/android/server/tv/TvService$2;->this$0:Lcom/android/server/tv/TvService;

    # getter for: Lcom/android/server/tv/TvService;->mContext:Landroid/content/Context;
    invoke-static {v10}, Lcom/android/server/tv/TvService;->access$000(Lcom/android/server/tv/TvService;)Landroid/content/Context;

    move-result-object v10

    const-string v11, "audio"

    invoke-virtual {v10, v11}, Landroid/content/Context;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Landroid/media/AudioManager;

    const/4 v10, 0x1

    const/16 v11, 0x11

    invoke-virtual {v2, v10, v11}, Landroid/media/AudioManager;->adjustMasterVolume(II)V

    goto/16 :goto_0

    :cond_10
    const-string v10, "konka.action.SET_VOLUME_DOWN"

    invoke-virtual {v10, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v10

    if-eqz v10, :cond_0

    const-string v10, "TvService"

    const-string v11, "TvService, volume down !"

    invoke-static {v10, v11}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    iget-object v10, p0, Lcom/android/server/tv/TvService$2;->this$0:Lcom/android/server/tv/TvService;

    # getter for: Lcom/android/server/tv/TvService;->mContext:Landroid/content/Context;
    invoke-static {v10}, Lcom/android/server/tv/TvService;->access$000(Lcom/android/server/tv/TvService;)Landroid/content/Context;

    move-result-object v10

    const-string v11, "audio"

    invoke-virtual {v10, v11}, Landroid/content/Context;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Landroid/media/AudioManager;

    const/4 v10, -0x1

    const/16 v11, 0x11

    invoke-virtual {v2, v10, v11}, Landroid/media/AudioManager;->adjustMasterVolume(II)V
    :try_end_2
    .catch Ljava/lang/Throwable; {:try_start_2 .. :try_end_2} :catch_1

    goto/16 :goto_0
.end method
