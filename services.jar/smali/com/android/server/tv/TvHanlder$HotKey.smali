.class final enum Lcom/android/server/tv/TvHanlder$HotKey;
.super Ljava/lang/Enum;
.source "TvHanlder.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/android/server/tv/TvHanlder;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x4018
    name = "HotKey"
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Enum",
        "<",
        "Lcom/android/server/tv/TvHanlder$HotKey;",
        ">;"
    }
.end annotation


# static fields
.field private static final synthetic $VALUES:[Lcom/android/server/tv/TvHanlder$HotKey;

.field public static final enum AudioMode:Lcom/android/server/tv/TvHanlder$HotKey;

.field public static final enum PicMode:Lcom/android/server/tv/TvHanlder$HotKey;

.field public static final enum SleepMode:Lcom/android/server/tv/TvHanlder$HotKey;

.field public static final enum Volume:Lcom/android/server/tv/TvHanlder$HotKey;

.field public static final enum Zoom:Lcom/android/server/tv/TvHanlder$HotKey;


# direct methods
.method static constructor <clinit>()V
    .locals 7

    const/4 v6, 0x4

    const/4 v5, 0x3

    const/4 v4, 0x2

    const/4 v3, 0x1

    const/4 v2, 0x0

    new-instance v0, Lcom/android/server/tv/TvHanlder$HotKey;

    const-string v1, "SleepMode"

    invoke-direct {v0, v1, v2}, Lcom/android/server/tv/TvHanlder$HotKey;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/android/server/tv/TvHanlder$HotKey;->SleepMode:Lcom/android/server/tv/TvHanlder$HotKey;

    new-instance v0, Lcom/android/server/tv/TvHanlder$HotKey;

    const-string v1, "Volume"

    invoke-direct {v0, v1, v3}, Lcom/android/server/tv/TvHanlder$HotKey;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/android/server/tv/TvHanlder$HotKey;->Volume:Lcom/android/server/tv/TvHanlder$HotKey;

    new-instance v0, Lcom/android/server/tv/TvHanlder$HotKey;

    const-string v1, "AudioMode"

    invoke-direct {v0, v1, v4}, Lcom/android/server/tv/TvHanlder$HotKey;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/android/server/tv/TvHanlder$HotKey;->AudioMode:Lcom/android/server/tv/TvHanlder$HotKey;

    new-instance v0, Lcom/android/server/tv/TvHanlder$HotKey;

    const-string v1, "PicMode"

    invoke-direct {v0, v1, v5}, Lcom/android/server/tv/TvHanlder$HotKey;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/android/server/tv/TvHanlder$HotKey;->PicMode:Lcom/android/server/tv/TvHanlder$HotKey;

    new-instance v0, Lcom/android/server/tv/TvHanlder$HotKey;

    const-string v1, "Zoom"

    invoke-direct {v0, v1, v6}, Lcom/android/server/tv/TvHanlder$HotKey;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/android/server/tv/TvHanlder$HotKey;->Zoom:Lcom/android/server/tv/TvHanlder$HotKey;

    const/4 v0, 0x5

    new-array v0, v0, [Lcom/android/server/tv/TvHanlder$HotKey;

    sget-object v1, Lcom/android/server/tv/TvHanlder$HotKey;->SleepMode:Lcom/android/server/tv/TvHanlder$HotKey;

    aput-object v1, v0, v2

    sget-object v1, Lcom/android/server/tv/TvHanlder$HotKey;->Volume:Lcom/android/server/tv/TvHanlder$HotKey;

    aput-object v1, v0, v3

    sget-object v1, Lcom/android/server/tv/TvHanlder$HotKey;->AudioMode:Lcom/android/server/tv/TvHanlder$HotKey;

    aput-object v1, v0, v4

    sget-object v1, Lcom/android/server/tv/TvHanlder$HotKey;->PicMode:Lcom/android/server/tv/TvHanlder$HotKey;

    aput-object v1, v0, v5

    sget-object v1, Lcom/android/server/tv/TvHanlder$HotKey;->Zoom:Lcom/android/server/tv/TvHanlder$HotKey;

    aput-object v1, v0, v6

    sput-object v0, Lcom/android/server/tv/TvHanlder$HotKey;->$VALUES:[Lcom/android/server/tv/TvHanlder$HotKey;

    return-void
.end method

.method private constructor <init>(Ljava/lang/String;I)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()V"
        }
    .end annotation

    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    return-void
.end method

.method public static valueOf(Ljava/lang/String;)Lcom/android/server/tv/TvHanlder$HotKey;
    .locals 1
    .param p0    # Ljava/lang/String;

    const-class v0, Lcom/android/server/tv/TvHanlder$HotKey;

    invoke-static {v0, p0}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;

    move-result-object v0

    check-cast v0, Lcom/android/server/tv/TvHanlder$HotKey;

    return-object v0
.end method

.method public static values()[Lcom/android/server/tv/TvHanlder$HotKey;
    .locals 1

    sget-object v0, Lcom/android/server/tv/TvHanlder$HotKey;->$VALUES:[Lcom/android/server/tv/TvHanlder$HotKey;

    invoke-virtual {v0}, [Lcom/android/server/tv/TvHanlder$HotKey;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [Lcom/android/server/tv/TvHanlder$HotKey;

    return-object v0
.end method
