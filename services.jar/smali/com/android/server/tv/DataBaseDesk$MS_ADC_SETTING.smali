.class public Lcom/android/server/tv/DataBaseDesk$MS_ADC_SETTING;
.super Ljava/lang/Object;
.source "DataBaseDesk.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/android/server/tv/DataBaseDesk;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x9
    name = "MS_ADC_SETTING"
.end annotation


# instance fields
.field public stAdcGainOffsetSetting:[Lcom/android/server/tv/DataBaseDesk$T_MS_CALIBRATION_DATA;

.field public u16CheckSum:I


# direct methods
.method public constructor <init>()V
    .locals 9

    const/16 v1, 0x80

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    sget-object v0, Lcom/android/server/tv/DataBaseDesk$E_ADC_SET_INDEX;->ADC_SET_NUMS:Lcom/android/server/tv/DataBaseDesk$E_ADC_SET_INDEX;

    invoke-virtual {v0}, Lcom/android/server/tv/DataBaseDesk$E_ADC_SET_INDEX;->ordinal()I

    move-result v0

    new-array v0, v0, [Lcom/android/server/tv/DataBaseDesk$T_MS_CALIBRATION_DATA;

    iput-object v0, p0, Lcom/android/server/tv/DataBaseDesk$MS_ADC_SETTING;->stAdcGainOffsetSetting:[Lcom/android/server/tv/DataBaseDesk$T_MS_CALIBRATION_DATA;

    const/4 v7, 0x0

    :goto_0
    sget-object v0, Lcom/android/server/tv/DataBaseDesk$E_ADC_SET_INDEX;->ADC_SET_NUMS:Lcom/android/server/tv/DataBaseDesk$E_ADC_SET_INDEX;

    invoke-virtual {v0}, Lcom/android/server/tv/DataBaseDesk$E_ADC_SET_INDEX;->ordinal()I

    move-result v0

    if-ge v7, v0, :cond_0

    iget-object v8, p0, Lcom/android/server/tv/DataBaseDesk$MS_ADC_SETTING;->stAdcGainOffsetSetting:[Lcom/android/server/tv/DataBaseDesk$T_MS_CALIBRATION_DATA;

    new-instance v0, Lcom/android/server/tv/DataBaseDesk$T_MS_CALIBRATION_DATA;

    move v2, v1

    move v3, v1

    move v4, v1

    move v5, v1

    move v6, v1

    invoke-direct/range {v0 .. v6}, Lcom/android/server/tv/DataBaseDesk$T_MS_CALIBRATION_DATA;-><init>(IIIIII)V

    aput-object v0, v8, v7

    add-int/lit8 v7, v7, 0x1

    goto :goto_0

    :cond_0
    return-void
.end method
