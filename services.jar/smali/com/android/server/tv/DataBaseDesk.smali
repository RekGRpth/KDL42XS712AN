.class public interface abstract Lcom/android/server/tv/DataBaseDesk;
.super Ljava/lang/Object;
.source "DataBaseDesk.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/android/server/tv/DataBaseDesk$ST_FACTORY_CI_SETTING;,
        Lcom/android/server/tv/DataBaseDesk$EN_CI_FUNCTION;,
        Lcom/android/server/tv/DataBaseDesk$MS_FACTORY_VD_OVERSCAN_SETTING;,
        Lcom/android/server/tv/DataBaseDesk$MS_FACTORY_YPbPr_OVERSCAN_SETTING;,
        Lcom/android/server/tv/DataBaseDesk$MS_FACTORY_HDMI_OVERSCAN_SETTING;,
        Lcom/android/server/tv/DataBaseDesk$MS_FACTORY_DTV_OVERSCAN_SETTING;,
        Lcom/android/server/tv/DataBaseDesk$EN_VD_SIGNALTYPE;,
        Lcom/android/server/tv/DataBaseDesk$MS_NLA_SET_INDEX;,
        Lcom/android/server/tv/DataBaseDesk$MAX_YPbPr_Resolution_Info;,
        Lcom/android/server/tv/DataBaseDesk$MAX_HDMI_Resolution_Info;,
        Lcom/android/server/tv/DataBaseDesk$MAX_DTV_Resolution_Info;,
        Lcom/android/server/tv/DataBaseDesk$ST_MAPI_VIDEO_WINDOW_INFO;,
        Lcom/android/server/tv/DataBaseDesk$ST_FACTORY_PEQ_SETTING;,
        Lcom/android/server/tv/DataBaseDesk$AUDIO_PEQ_PARAM;,
        Lcom/android/server/tv/DataBaseDesk$MS_NLA_SETTING;,
        Lcom/android/server/tv/DataBaseDesk$MS_NLA_POINT;,
        Lcom/android/server/tv/DataBaseDesk$MS_FACTORY_EXTERN_SETTING;,
        Lcom/android/server/tv/DataBaseDesk$MS_Factory_NS_VIF_SET;,
        Lcom/android/server/tv/DataBaseDesk$MS_Factory_NS_VD_SET;,
        Lcom/android/server/tv/DataBaseDesk$MS_FACTORY_SSC_SET;,
        Lcom/android/server/tv/DataBaseDesk$EN_SYSTEM_FACTORY_DB_COMMAND;,
        Lcom/android/server/tv/DataBaseDesk$MS_CEC_SETTING;,
        Lcom/android/server/tv/DataBaseDesk$MS_USER_SOUND_SETTING;,
        Lcom/android/server/tv/DataBaseDesk$EN_SOUND_AD_OUTPUT;,
        Lcom/android/server/tv/DataBaseDesk$EN_SURROUND_TYPE;,
        Lcom/android/server/tv/DataBaseDesk$EN_SURROUND_SYSTEM_TYPE;,
        Lcom/android/server/tv/DataBaseDesk$EN_AUDYSSEY_EQ_MODE;,
        Lcom/android/server/tv/DataBaseDesk$EN_AUDYSSEY_DYNAMIC_VOLUME_MODE;,
        Lcom/android/server/tv/DataBaseDesk$SoundModeSeting;,
        Lcom/android/server/tv/DataBaseDesk$EN_AUD_MODE;,
        Lcom/android/server/tv/DataBaseDesk$EN_SPDIF_MODE;,
        Lcom/android/server/tv/DataBaseDesk$EN_SURROUND_MODE;,
        Lcom/android/server/tv/DataBaseDesk$EN_SOUND_MODE;,
        Lcom/android/server/tv/DataBaseDesk$MS_USER_SYSTEM_SETTING;,
        Lcom/android/server/tv/DataBaseDesk$EN_MS_POWERON_LOGO;,
        Lcom/android/server/tv/DataBaseDesk$EN_MS_POWERON_MUSIC;,
        Lcom/android/server/tv/DataBaseDesk$EN_MS_OFFLINE_DET_MODE;,
        Lcom/android/server/tv/DataBaseDesk$EN_MS_CHANNEL_SWITCH_MODE;,
        Lcom/android/server/tv/DataBaseDesk$EN_MS_SUPER;,
        Lcom/android/server/tv/DataBaseDesk$MS_USER_LOCATION_SETTING;,
        Lcom/android/server/tv/DataBaseDesk$SPDIF_TYPE;,
        Lcom/android/server/tv/DataBaseDesk$MS_USER_SUBTITLE_SETTING;,
        Lcom/android/server/tv/DataBaseDesk$EN_SATELLITE_PLATFORM;,
        Lcom/android/server/tv/DataBaseDesk$EN_CABLE_OPERATORS;,
        Lcom/android/server/tv/DataBaseDesk$MEMBER_COUNTRY;,
        Lcom/android/server/tv/DataBaseDesk$MFC_MODE;,
        Lcom/android/server/tv/DataBaseDesk$EN_MFC;,
        Lcom/android/server/tv/DataBaseDesk$MAPI_VIDEO_3D_ARC_Type;,
        Lcom/android/server/tv/DataBaseDesk$ST_MAPI_3D_INFO;,
        Lcom/android/server/tv/DataBaseDesk$EN_3D_OUTPUT_TYPE;,
        Lcom/android/server/tv/DataBaseDesk$EN_3D_INPUT_TYPE;,
        Lcom/android/server/tv/DataBaseDesk$MS_ADC_SETTING;,
        Lcom/android/server/tv/DataBaseDesk$T_MS_CALIBRATION_DATA;,
        Lcom/android/server/tv/DataBaseDesk$E_ADC_SET_INDEX;,
        Lcom/android/server/tv/DataBaseDesk$T_MS_COLOR_TEMPEX;,
        Lcom/android/server/tv/DataBaseDesk$T_MS_COLOR_TEMPEX_DATA;,
        Lcom/android/server/tv/DataBaseDesk$T_MS_COLOR_TEMP;,
        Lcom/android/server/tv/DataBaseDesk$T_MS_COLOR_TEMP_DATA;,
        Lcom/android/server/tv/DataBaseDesk$EN_MS_VIDEOITEM;,
        Lcom/android/server/tv/DataBaseDesk$T_MS_VIDEO;,
        Lcom/android/server/tv/DataBaseDesk$EN_DISPLAY_TVFORMAT;,
        Lcom/android/server/tv/DataBaseDesk$T_MS_OVERSCAN_SETTING_USER;,
        Lcom/android/server/tv/DataBaseDesk$T_MS_PICTURE;,
        Lcom/android/server/tv/DataBaseDesk$ThreeD_Video_MODE;,
        Lcom/android/server/tv/DataBaseDesk$EN_ThreeD_Video_LRVIEWSWITCH;,
        Lcom/android/server/tv/DataBaseDesk$EN_ThreeD_Video_3DOUTPUTASPECT;,
        Lcom/android/server/tv/DataBaseDesk$EN_ThreeD_Video_AUTOSTART;,
        Lcom/android/server/tv/DataBaseDesk$EN_ThreeD_Video_3DOFFSET;,
        Lcom/android/server/tv/DataBaseDesk$EN_ThreeD_Video_3DDEPTH;,
        Lcom/android/server/tv/DataBaseDesk$EN_ThreeD_Video_3DTO2D;,
        Lcom/android/server/tv/DataBaseDesk$EN_ThreeD_Video_DISPLAYFORMAT;,
        Lcom/android/server/tv/DataBaseDesk$EN_ThreeD_Video_SELFADAPTIVE_LEVEL;,
        Lcom/android/server/tv/DataBaseDesk$EN_ThreeD_Video_SELFADAPTIVE_DETECT;,
        Lcom/android/server/tv/DataBaseDesk$EN_ThreeD_Video;,
        Lcom/android/server/tv/DataBaseDesk$EN_MS_FILM;,
        Lcom/android/server/tv/DataBaseDesk$EN_MS_Dynamic_Contrast;,
        Lcom/android/server/tv/DataBaseDesk$AUDIOMODE_TYPE_;,
        Lcom/android/server/tv/DataBaseDesk$MAPI_AVD_VideoStandardType;,
        Lcom/android/server/tv/DataBaseDesk$MAPI_VIDEO_OUT_VE_SYS;,
        Lcom/android/server/tv/DataBaseDesk$EN_DISPLAY_RES_TYPE;,
        Lcom/android/server/tv/DataBaseDesk$MAPI_VIDEO_ARC_Type;,
        Lcom/android/server/tv/DataBaseDesk$T_MS_SUB_COLOR;,
        Lcom/android/server/tv/DataBaseDesk$T_MS_NR_MODE;,
        Lcom/android/server/tv/DataBaseDesk$EN_MS_MPEG_NR;,
        Lcom/android/server/tv/DataBaseDesk$EN_MS_NR;,
        Lcom/android/server/tv/DataBaseDesk$EN_MS_PIC_ADV;,
        Lcom/android/server/tv/DataBaseDesk$EN_MS_COLOR_TEMP_INPUT_SOURCE;,
        Lcom/android/server/tv/DataBaseDesk$EN_MS_COLOR_TEMP;,
        Lcom/android/server/tv/DataBaseDesk$EN_MS_PICTURE;
    }
.end annotation


# static fields
.field public static final EN_3D_SELFADAPTIVE_LEVEL_HIGH:I = 0x2

.field public static final EN_3D_SELFADAPTIVE_LEVEL_LOW:I = 0x0

.field public static final EN_3D_SELFADAPTIVE_LEVEL_MAX:I = 0x3

.field public static final EN_3D_SELFADAPTIVE_LEVEL_MIDDLE:I = 0x1

.field public static final EN_AUDIO_HIDEV_BW_L1:I = 0x1

.field public static final EN_AUDIO_HIDEV_BW_L2:I = 0x2

.field public static final EN_AUDIO_HIDEV_BW_L3:I = 0x3

.field public static final EN_AUDIO_HIDEV_BW_MAX:I = 0x4

.field public static final EN_AUDIO_HIDEV_OFF:I = 0x0

.field public static final EN_POWER_MODE_DIRECT:I = 0x2

.field public static final EN_POWER_MODE_MAX:I = 0x3

.field public static final EN_POWER_MODE_MEMORY:I = 0x1

.field public static final EN_POWER_MODE_SECONDARY:I = 0x0

.field public static final FACTORY_PRE_SET_ATV:I = 0x0

.field public static final FACTORY_PRE_SET_DTV:I = 0x1

.field public static final FACTORY_PRE_SET_NUM:I = 0x2

.field public static final TEST_PATTERN_MODE_BLACK:I = 0x4

.field public static final TEST_PATTERN_MODE_BLUE:I = 0x3

.field public static final TEST_PATTERN_MODE_GRAY:I = 0x0

.field public static final TEST_PATTERN_MODE_GREEN:I = 0x2

.field public static final TEST_PATTERN_MODE_OFF:I = 0x5

.field public static final TEST_PATTERN_MODE_RED:I = 0x1

.field public static final T_3DInfo_IDX:S = 0x0s

.field public static final T_3DSetting_IDX:S = 0x1s

.field public static final T_ADCAdjust_IDX:S = 0x24s

.field public static final T_ATVDefaultPrograms_IDX:S = 0x31s

.field public static final T_BlockSysSetting_IDX:S = 0x2s

.field public static final T_CECSetting_IDX:S = 0x3s

.field public static final T_CISettineUpInfo_IDX:S = 0x13s

.field public static final T_CISetting_IDX:S = 0x4s

.field public static final T_DB_VERSION_IDX:S = 0x5s

.field public static final T_DTVDefaultPrograms_IDX:S = 0x32s

.field public static final T_DTVOverscanSetting_IDX:S = 0x30s

.field public static final T_DvbtPresetting_IDX:S = 0x6s

.field public static final T_EpgTimer_IDX:S = 0x7s

.field public static final T_FacrotyColorTempEx_IDX:S = 0x26s

.field public static final T_FacrotyColorTemp_IDX:S = 0x25s

.field public static final T_FactoryExtern_IDX:S = 0x27s

.field public static final T_Factory_DB_VERSION_IDX:S = 0x2ds

.field public static final T_FavTypeName_IDX:S = 0x8s

.field public static final T_HDMIOverscanSetting_IDX:S = 0x2es

.field public static final T_InputSource_Type_IDX:S = 0x9s

.field public static final T_IsdbSysSetting_IDX:S = 0xas

.field public static final T_IsdbUserSetting_IDX:S = 0xbs

.field public static final T_MAX_IDX:S = 0x33s

.field public static final T_MediumSetting_IDX:S = 0xcs

.field public static final T_MfcMode_IDX:S = 0xds

.field public static final T_NRMode_IDX:S = 0xes

.field public static final T_NitInfo_IDX:S = 0xfs

.field public static final T_Nit_TSInfo_IDX:S = 0x10s

.field public static final T_NonLinearAdjust_IDX:S = 0x2as

.field public static final T_NonStarndardAdjust_IDX:S = 0x28s

.field public static final T_OADInfo_IDX:S = 0x11s

.field public static final T_OADInfo_UntDescriptor_IDX:S = 0x12s

.field public static final T_OverscanAdjust_IDX:S = 0x2bs

.field public static final T_PEQAdjust_IDX:S = 0x2cs

.field public static final T_PicMode_Setting_IDX:S = 0x14s

.field public static final T_PipSetting_IDX:S = 0x15s

.field public static final T_SSCAdjust_IDX:S = 0x29s

.field public static final T_SoundMode_Setting_IDX:S = 0x16s

.field public static final T_SoundSetting_IDX:S = 0x17s

.field public static final T_SubtitleSetting_IDX:S = 0x18s

.field public static final T_SystemSetting_IDX:S = 0x19s

.field public static final T_ThreeDVideoMode_IDX:S = 0x1as

.field public static final T_ThreeDVideoRouterSetting_IDX:S = 0x23s

.field public static final T_TimeSetting_IDX:S = 0x1bs

.field public static final T_USER_COLORTEMP_EX_IDX:S = 0x1ds

.field public static final T_USER_COLORTEMP_IDX:S = 0x1cs

.field public static final T_UserLocationSetting_IDX:S = 0x1es

.field public static final T_UserMMSetting_IDX:S = 0x1fs

.field public static final T_UserOverScanMode_IDX:S = 0x20s

.field public static final T_UserPCModeSetting_IDX:S = 0x21s

.field public static final T_VideoSetting_IDX:S = 0x22s

.field public static final T_YPbPrOverscanSetting_IDX:S = 0x2fs


# virtual methods
.method public abstract UpdateDB()V
.end method

.method public abstract getAdcSetting()Lcom/android/server/tv/DataBaseDesk$MS_ADC_SETTING;
.end method

.method public abstract getCECVar()Lcom/android/server/tv/DataBaseDesk$MS_CEC_SETTING;
.end method

.method public abstract getFactoryExt()Lcom/android/server/tv/DataBaseDesk$MS_FACTORY_EXTERN_SETTING;
.end method

.method public abstract getLocationSet()Lcom/android/server/tv/DataBaseDesk$MS_USER_LOCATION_SETTING;
.end method

.method public abstract getNoStandSet()Lcom/android/server/tv/DataBaseDesk$MS_Factory_NS_VD_SET;
.end method

.method public abstract getNoStandVifSet()Lcom/android/server/tv/DataBaseDesk$MS_Factory_NS_VIF_SET;
.end method

.method public abstract getSound()Lcom/android/server/tv/DataBaseDesk$MS_USER_SOUND_SETTING;
.end method

.method public abstract getSoundMode(Lcom/android/server/tv/DataBaseDesk$EN_SOUND_MODE;)Lcom/android/server/tv/DataBaseDesk$SoundModeSeting;
.end method

.method public abstract getSscSet()Lcom/android/server/tv/DataBaseDesk$MS_FACTORY_SSC_SET;
.end method

.method public abstract getSubtitleSet()Lcom/android/server/tv/DataBaseDesk$MS_USER_SUBTITLE_SETTING;
.end method

.method public abstract getUsrData()Lcom/android/server/tv/DataBaseDesk$MS_USER_SYSTEM_SETTING;
.end method

.method public abstract getVideo()Lcom/android/server/tv/DataBaseDesk$T_MS_VIDEO;
.end method

.method public abstract getVideoTemp()Lcom/android/server/tv/DataBaseDesk$T_MS_COLOR_TEMP;
.end method

.method public abstract getVideoTempEx()Lcom/android/server/tv/DataBaseDesk$T_MS_COLOR_TEMPEX_DATA;
.end method

.method public abstract queryAntennaType()I
.end method

.method public abstract queryChSwMode()Lcom/android/server/tv/DataBaseDesk$EN_MS_CHANNEL_SWITCH_MODE;
.end method

.method public abstract queryCountry()I
.end method

.method public abstract queryEventName(S)Ljava/lang/String;
.end method

.method public abstract queryServiceName(S)Ljava/lang/String;
.end method

.method public abstract querySpdifMode()I
.end method

.method public abstract restoreUsrDB(Lcom/android/server/tv/DataBaseDesk$EN_SYSTEM_FACTORY_DB_COMMAND;)Z
.end method

.method public abstract setAdcSetting(Lcom/android/server/tv/DataBaseDesk$MS_ADC_SETTING;)Z
.end method

.method public abstract setCECVar(Lcom/android/server/tv/DataBaseDesk$MS_CEC_SETTING;)Z
.end method

.method public abstract setFactoryExt(Lcom/android/server/tv/DataBaseDesk$MS_FACTORY_EXTERN_SETTING;)Z
.end method

.method public abstract setLocationSet(Lcom/android/server/tv/DataBaseDesk$MS_USER_LOCATION_SETTING;)Z
.end method

.method public abstract setNoStandSet(Lcom/android/server/tv/DataBaseDesk$MS_Factory_NS_VD_SET;)Z
.end method

.method public abstract setNoStandVifSet(Lcom/android/server/tv/DataBaseDesk$MS_Factory_NS_VIF_SET;)Z
.end method

.method public abstract setSound(Lcom/android/server/tv/DataBaseDesk$MS_USER_SOUND_SETTING;)Z
.end method

.method public abstract setSoundMode(Lcom/android/server/tv/DataBaseDesk$EN_SOUND_MODE;Lcom/android/server/tv/DataBaseDesk$SoundModeSeting;)Z
.end method

.method public abstract setSscSet(Lcom/android/server/tv/DataBaseDesk$MS_FACTORY_SSC_SET;)Z
.end method

.method public abstract setSubtitleSet(Lcom/android/server/tv/DataBaseDesk$MS_USER_SUBTITLE_SETTING;)Z
.end method

.method public abstract setUsrData(Lcom/android/server/tv/DataBaseDesk$MS_USER_SYSTEM_SETTING;)Z
.end method

.method public abstract setVideo(Lcom/android/server/tv/DataBaseDesk$T_MS_VIDEO;)Z
.end method

.method public abstract setVideoTemp(Lcom/android/server/tv/DataBaseDesk$T_MS_COLOR_TEMP;)Z
.end method

.method public abstract setVideoTempEx(Lcom/android/server/tv/DataBaseDesk$T_MS_COLOR_TEMPEX_DATA;)Z
.end method

.method public abstract updateAntennaType(I)V
.end method

.method public abstract updateChSwMode(Lcom/android/server/tv/DataBaseDesk$EN_MS_CHANNEL_SWITCH_MODE;)V
.end method

.method public abstract updateEventName(Ljava/lang/String;S)V
.end method

.method public abstract updateServiceName(Ljava/lang/String;S)V
.end method

.method public abstract updateSpdifMode(I)V
.end method
