.class public Lcom/android/server/tv/DeskCecEventListener;
.super Ljava/lang/Object;
.source "DeskCecEventListener.java"

# interfaces
.implements Lcom/mstar/android/tvapi/common/listener/OnCecEventListener;


# static fields
.field private static cecEventListener:Lcom/android/server/tv/DeskCecEventListener;


# instance fields
.field private iCecEventClients:Ljava/util/HashMap;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/HashMap",
            "<",
            "Ljava/lang/String;",
            "Lcom/mstar/android/tv/ICecEventClient;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method static constructor <clinit>()V
    .locals 1

    const/4 v0, 0x0

    sput-object v0, Lcom/android/server/tv/DeskCecEventListener;->cecEventListener:Lcom/android/server/tv/DeskCecEventListener;

    return-void
.end method

.method public constructor <init>()V
    .locals 1

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    new-instance v0, Ljava/util/HashMap;

    invoke-direct {v0}, Ljava/util/HashMap;-><init>()V

    iput-object v0, p0, Lcom/android/server/tv/DeskCecEventListener;->iCecEventClients:Ljava/util/HashMap;

    return-void
.end method

.method public static getInstance()Lcom/android/server/tv/DeskCecEventListener;
    .locals 1

    sget-object v0, Lcom/android/server/tv/DeskCecEventListener;->cecEventListener:Lcom/android/server/tv/DeskCecEventListener;

    if-nez v0, :cond_0

    new-instance v0, Lcom/android/server/tv/DeskCecEventListener;

    invoke-direct {v0}, Lcom/android/server/tv/DeskCecEventListener;-><init>()V

    sput-object v0, Lcom/android/server/tv/DeskCecEventListener;->cecEventListener:Lcom/android/server/tv/DeskCecEventListener;

    :cond_0
    sget-object v0, Lcom/android/server/tv/DeskCecEventListener;->cecEventListener:Lcom/android/server/tv/DeskCecEventListener;

    return-object v0
.end method


# virtual methods
.method public onImageViewOn(I)Z
    .locals 5
    .param p1    # I

    const/4 v4, 0x0

    iget-object v3, p0, Lcom/android/server/tv/DeskCecEventListener;->iCecEventClients:Ljava/util/HashMap;

    if-nez v3, :cond_1

    :cond_0
    :goto_0
    return v4

    :cond_1
    :try_start_0
    iget-object v3, p0, Lcom/android/server/tv/DeskCecEventListener;->iCecEventClients:Ljava/util/HashMap;

    invoke-virtual {v3}, Ljava/util/HashMap;->values()Ljava/util/Collection;

    move-result-object v3

    invoke-interface {v3}, Ljava/util/Collection;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :goto_1
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v3

    if-eqz v3, :cond_0

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lcom/mstar/android/tv/ICecEventClient;

    invoke-interface {v2, p1}, Lcom/mstar/android/tv/ICecEventClient;->onImageViewOn(I)Z
    :try_end_0
    .catch Landroid/os/RemoteException; {:try_start_0 .. :try_end_0} :catch_0

    goto :goto_1

    :catch_0
    move-exception v0

    invoke-virtual {v0}, Landroid/os/RemoteException;->printStackTrace()V

    goto :goto_0
.end method

.method public onTextViewOn(I)Z
    .locals 5
    .param p1    # I

    const/4 v4, 0x0

    iget-object v3, p0, Lcom/android/server/tv/DeskCecEventListener;->iCecEventClients:Ljava/util/HashMap;

    if-nez v3, :cond_1

    :cond_0
    :goto_0
    return v4

    :cond_1
    :try_start_0
    iget-object v3, p0, Lcom/android/server/tv/DeskCecEventListener;->iCecEventClients:Ljava/util/HashMap;

    invoke-virtual {v3}, Ljava/util/HashMap;->values()Ljava/util/Collection;

    move-result-object v3

    invoke-interface {v3}, Ljava/util/Collection;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :goto_1
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v3

    if-eqz v3, :cond_0

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lcom/mstar/android/tv/ICecEventClient;

    invoke-interface {v2, p1}, Lcom/mstar/android/tv/ICecEventClient;->onTextViewOn(I)Z
    :try_end_0
    .catch Landroid/os/RemoteException; {:try_start_0 .. :try_end_0} :catch_0

    goto :goto_1

    :catch_0
    move-exception v0

    invoke-virtual {v0}, Landroid/os/RemoteException;->printStackTrace()V

    goto :goto_0
.end method

.method public releaseClient(Ljava/lang/String;)V
    .locals 1
    .param p1    # Ljava/lang/String;

    iget-object v0, p0, Lcom/android/server/tv/DeskCecEventListener;->iCecEventClients:Ljava/util/HashMap;

    invoke-virtual {v0, p1}, Ljava/util/HashMap;->remove(Ljava/lang/Object;)Ljava/lang/Object;

    return-void
.end method

.method public setClient(Ljava/lang/String;Lcom/mstar/android/tv/ICecEventClient;)V
    .locals 1
    .param p1    # Ljava/lang/String;
    .param p2    # Lcom/mstar/android/tv/ICecEventClient;

    iget-object v0, p0, Lcom/android/server/tv/DeskCecEventListener;->iCecEventClients:Ljava/util/HashMap;

    invoke-virtual {v0, p1, p2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    return-void
.end method
