.class public Lcom/android/server/tv/DeskPvrEventListener;
.super Ljava/lang/Object;
.source "DeskPvrEventListener.java"

# interfaces
.implements Lcom/mstar/android/tvapi/common/PvrManager$OnPvrEventListener;


# static fields
.field private static instance:Lcom/android/server/tv/DeskPvrEventListener;


# instance fields
.field private clients:Ljava/util/HashMap;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/HashMap",
            "<",
            "Ljava/lang/String;",
            "Lcom/mstar/android/tv/IPvrEventClient;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method private constructor <init>()V
    .locals 1

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    const/4 v0, 0x0

    iput-object v0, p0, Lcom/android/server/tv/DeskPvrEventListener;->clients:Ljava/util/HashMap;

    new-instance v0, Ljava/util/HashMap;

    invoke-direct {v0}, Ljava/util/HashMap;-><init>()V

    iput-object v0, p0, Lcom/android/server/tv/DeskPvrEventListener;->clients:Ljava/util/HashMap;

    return-void
.end method

.method public static getInstance()Lcom/android/server/tv/DeskPvrEventListener;
    .locals 1

    sget-object v0, Lcom/android/server/tv/DeskPvrEventListener;->instance:Lcom/android/server/tv/DeskPvrEventListener;

    if-nez v0, :cond_0

    new-instance v0, Lcom/android/server/tv/DeskPvrEventListener;

    invoke-direct {v0}, Lcom/android/server/tv/DeskPvrEventListener;-><init>()V

    sput-object v0, Lcom/android/server/tv/DeskPvrEventListener;->instance:Lcom/android/server/tv/DeskPvrEventListener;

    :cond_0
    sget-object v0, Lcom/android/server/tv/DeskPvrEventListener;->instance:Lcom/android/server/tv/DeskPvrEventListener;

    return-object v0
.end method


# virtual methods
.method public onPvrNotifyFormatFinished(Lcom/mstar/android/tvapi/common/PvrManager;III)Z
    .locals 5
    .param p1    # Lcom/mstar/android/tvapi/common/PvrManager;
    .param p2    # I
    .param p3    # I
    .param p4    # I

    const/4 v4, 0x0

    iget-object v3, p0, Lcom/android/server/tv/DeskPvrEventListener;->clients:Ljava/util/HashMap;

    if-nez v3, :cond_1

    :cond_0
    :goto_0
    return v4

    :cond_1
    :try_start_0
    iget-object v3, p0, Lcom/android/server/tv/DeskPvrEventListener;->clients:Ljava/util/HashMap;

    invoke-virtual {v3}, Ljava/util/HashMap;->values()Ljava/util/Collection;

    move-result-object v3

    invoke-interface {v3}, Ljava/util/Collection;->iterator()Ljava/util/Iterator;

    move-result-object v2

    :goto_1
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    move-result v3

    if-eqz v3, :cond_0

    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/mstar/android/tv/IPvrEventClient;

    invoke-interface {v0, p2, p3, p4}, Lcom/mstar/android/tv/IPvrEventClient;->onPvrNotifyFormatFinished(III)Z
    :try_end_0
    .catch Landroid/os/RemoteException; {:try_start_0 .. :try_end_0} :catch_0

    goto :goto_1

    :catch_0
    move-exception v1

    invoke-virtual {v1}, Landroid/os/RemoteException;->printStackTrace()V

    goto :goto_0
.end method

.method public onPvrNotifyPlaybackBegin(Lcom/mstar/android/tvapi/common/PvrManager;III)Z
    .locals 5
    .param p1    # Lcom/mstar/android/tvapi/common/PvrManager;
    .param p2    # I
    .param p3    # I
    .param p4    # I

    const/4 v4, 0x0

    iget-object v3, p0, Lcom/android/server/tv/DeskPvrEventListener;->clients:Ljava/util/HashMap;

    if-nez v3, :cond_1

    :cond_0
    :goto_0
    return v4

    :cond_1
    :try_start_0
    iget-object v3, p0, Lcom/android/server/tv/DeskPvrEventListener;->clients:Ljava/util/HashMap;

    invoke-virtual {v3}, Ljava/util/HashMap;->values()Ljava/util/Collection;

    move-result-object v3

    invoke-interface {v3}, Ljava/util/Collection;->iterator()Ljava/util/Iterator;

    move-result-object v2

    :goto_1
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    move-result v3

    if-eqz v3, :cond_0

    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/mstar/android/tv/IPvrEventClient;

    invoke-interface {v0, p2, p3, p4}, Lcom/mstar/android/tv/IPvrEventClient;->onPvrNotifyPlaybackBegin(III)Z
    :try_end_0
    .catch Landroid/os/RemoteException; {:try_start_0 .. :try_end_0} :catch_0

    goto :goto_1

    :catch_0
    move-exception v1

    invoke-virtual {v1}, Landroid/os/RemoteException;->printStackTrace()V

    goto :goto_0
.end method

.method public onPvrNotifyPlaybackStop(Lcom/mstar/android/tvapi/common/PvrManager;III)Z
    .locals 5
    .param p1    # Lcom/mstar/android/tvapi/common/PvrManager;
    .param p2    # I
    .param p3    # I
    .param p4    # I

    const/4 v4, 0x0

    iget-object v3, p0, Lcom/android/server/tv/DeskPvrEventListener;->clients:Ljava/util/HashMap;

    if-nez v3, :cond_1

    :cond_0
    :goto_0
    return v4

    :cond_1
    :try_start_0
    iget-object v3, p0, Lcom/android/server/tv/DeskPvrEventListener;->clients:Ljava/util/HashMap;

    invoke-virtual {v3}, Ljava/util/HashMap;->values()Ljava/util/Collection;

    move-result-object v3

    invoke-interface {v3}, Ljava/util/Collection;->iterator()Ljava/util/Iterator;

    move-result-object v2

    :goto_1
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    move-result v3

    if-eqz v3, :cond_0

    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/mstar/android/tv/IPvrEventClient;

    invoke-interface {v0, p2, p3, p4}, Lcom/mstar/android/tv/IPvrEventClient;->onPvrNotifyPlaybackStop(III)Z
    :try_end_0
    .catch Landroid/os/RemoteException; {:try_start_0 .. :try_end_0} :catch_0

    goto :goto_1

    :catch_0
    move-exception v1

    invoke-virtual {v1}, Landroid/os/RemoteException;->printStackTrace()V

    goto :goto_0
.end method

.method public onPvrNotifyUsbInserted(Lcom/mstar/android/tvapi/common/PvrManager;III)Z
    .locals 5
    .param p1    # Lcom/mstar/android/tvapi/common/PvrManager;
    .param p2    # I
    .param p3    # I
    .param p4    # I

    const/4 v4, 0x0

    iget-object v3, p0, Lcom/android/server/tv/DeskPvrEventListener;->clients:Ljava/util/HashMap;

    if-nez v3, :cond_1

    :cond_0
    :goto_0
    return v4

    :cond_1
    :try_start_0
    iget-object v3, p0, Lcom/android/server/tv/DeskPvrEventListener;->clients:Ljava/util/HashMap;

    invoke-virtual {v3}, Ljava/util/HashMap;->values()Ljava/util/Collection;

    move-result-object v3

    invoke-interface {v3}, Ljava/util/Collection;->iterator()Ljava/util/Iterator;

    move-result-object v2

    :goto_1
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    move-result v3

    if-eqz v3, :cond_0

    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/mstar/android/tv/IPvrEventClient;

    invoke-interface {v0, p2, p3, p4}, Lcom/mstar/android/tv/IPvrEventClient;->onPvrNotifyUsbInserted(III)Z
    :try_end_0
    .catch Landroid/os/RemoteException; {:try_start_0 .. :try_end_0} :catch_0

    goto :goto_1

    :catch_0
    move-exception v1

    invoke-virtual {v1}, Landroid/os/RemoteException;->printStackTrace()V

    goto :goto_0
.end method

.method public onPvrNotifyUsbRemoved(Lcom/mstar/android/tvapi/common/PvrManager;III)Z
    .locals 5
    .param p1    # Lcom/mstar/android/tvapi/common/PvrManager;
    .param p2    # I
    .param p3    # I
    .param p4    # I

    const/4 v4, 0x0

    iget-object v3, p0, Lcom/android/server/tv/DeskPvrEventListener;->clients:Ljava/util/HashMap;

    if-nez v3, :cond_1

    :cond_0
    :goto_0
    return v4

    :cond_1
    :try_start_0
    iget-object v3, p0, Lcom/android/server/tv/DeskPvrEventListener;->clients:Ljava/util/HashMap;

    invoke-virtual {v3}, Ljava/util/HashMap;->values()Ljava/util/Collection;

    move-result-object v3

    invoke-interface {v3}, Ljava/util/Collection;->iterator()Ljava/util/Iterator;

    move-result-object v2

    :goto_1
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    move-result v3

    if-eqz v3, :cond_0

    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/mstar/android/tv/IPvrEventClient;

    invoke-interface {v0, p2, p3, p4}, Lcom/mstar/android/tv/IPvrEventClient;->onPvrNotifyUsbRemoved(III)Z
    :try_end_0
    .catch Landroid/os/RemoteException; {:try_start_0 .. :try_end_0} :catch_0

    goto :goto_1

    :catch_0
    move-exception v1

    invoke-virtual {v1}, Landroid/os/RemoteException;->printStackTrace()V

    goto :goto_0
.end method

.method public releaseClient(Ljava/lang/String;)V
    .locals 1
    .param p1    # Ljava/lang/String;

    iget-object v0, p0, Lcom/android/server/tv/DeskPvrEventListener;->clients:Ljava/util/HashMap;

    invoke-virtual {v0, p1}, Ljava/util/HashMap;->remove(Ljava/lang/Object;)Ljava/lang/Object;

    return-void
.end method

.method public setClient(Ljava/lang/String;Lcom/mstar/android/tv/IPvrEventClient;)V
    .locals 1
    .param p1    # Ljava/lang/String;
    .param p2    # Lcom/mstar/android/tv/IPvrEventClient;

    iget-object v0, p0, Lcom/android/server/tv/DeskPvrEventListener;->clients:Ljava/util/HashMap;

    invoke-virtual {v0, p1, p2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    return-void
.end method
