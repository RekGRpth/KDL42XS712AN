.class public final enum Lcom/android/server/tv/DataBaseDesk$EN_ThreeD_Video_LRVIEWSWITCH;
.super Ljava/lang/Enum;
.source "DataBaseDesk.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/android/server/tv/DataBaseDesk;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x4019
    name = "EN_ThreeD_Video_LRVIEWSWITCH"
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Enum",
        "<",
        "Lcom/android/server/tv/DataBaseDesk$EN_ThreeD_Video_LRVIEWSWITCH;",
        ">;"
    }
.end annotation


# static fields
.field private static final synthetic $VALUES:[Lcom/android/server/tv/DataBaseDesk$EN_ThreeD_Video_LRVIEWSWITCH;

.field public static final enum DB_ThreeD_Video_LRVIEWSWITCH_COUNT:Lcom/android/server/tv/DataBaseDesk$EN_ThreeD_Video_LRVIEWSWITCH;

.field public static final enum DB_ThreeD_Video_LRVIEWSWITCH_EXCHANGE:Lcom/android/server/tv/DataBaseDesk$EN_ThreeD_Video_LRVIEWSWITCH;

.field public static final enum DB_ThreeD_Video_LRVIEWSWITCH_NOTEXCHANGE:Lcom/android/server/tv/DataBaseDesk$EN_ThreeD_Video_LRVIEWSWITCH;


# direct methods
.method static constructor <clinit>()V
    .locals 5

    const/4 v4, 0x2

    const/4 v3, 0x1

    const/4 v2, 0x0

    new-instance v0, Lcom/android/server/tv/DataBaseDesk$EN_ThreeD_Video_LRVIEWSWITCH;

    const-string v1, "DB_ThreeD_Video_LRVIEWSWITCH_EXCHANGE"

    invoke-direct {v0, v1, v2}, Lcom/android/server/tv/DataBaseDesk$EN_ThreeD_Video_LRVIEWSWITCH;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/android/server/tv/DataBaseDesk$EN_ThreeD_Video_LRVIEWSWITCH;->DB_ThreeD_Video_LRVIEWSWITCH_EXCHANGE:Lcom/android/server/tv/DataBaseDesk$EN_ThreeD_Video_LRVIEWSWITCH;

    new-instance v0, Lcom/android/server/tv/DataBaseDesk$EN_ThreeD_Video_LRVIEWSWITCH;

    const-string v1, "DB_ThreeD_Video_LRVIEWSWITCH_NOTEXCHANGE"

    invoke-direct {v0, v1, v3}, Lcom/android/server/tv/DataBaseDesk$EN_ThreeD_Video_LRVIEWSWITCH;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/android/server/tv/DataBaseDesk$EN_ThreeD_Video_LRVIEWSWITCH;->DB_ThreeD_Video_LRVIEWSWITCH_NOTEXCHANGE:Lcom/android/server/tv/DataBaseDesk$EN_ThreeD_Video_LRVIEWSWITCH;

    new-instance v0, Lcom/android/server/tv/DataBaseDesk$EN_ThreeD_Video_LRVIEWSWITCH;

    const-string v1, "DB_ThreeD_Video_LRVIEWSWITCH_COUNT"

    invoke-direct {v0, v1, v4}, Lcom/android/server/tv/DataBaseDesk$EN_ThreeD_Video_LRVIEWSWITCH;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/android/server/tv/DataBaseDesk$EN_ThreeD_Video_LRVIEWSWITCH;->DB_ThreeD_Video_LRVIEWSWITCH_COUNT:Lcom/android/server/tv/DataBaseDesk$EN_ThreeD_Video_LRVIEWSWITCH;

    const/4 v0, 0x3

    new-array v0, v0, [Lcom/android/server/tv/DataBaseDesk$EN_ThreeD_Video_LRVIEWSWITCH;

    sget-object v1, Lcom/android/server/tv/DataBaseDesk$EN_ThreeD_Video_LRVIEWSWITCH;->DB_ThreeD_Video_LRVIEWSWITCH_EXCHANGE:Lcom/android/server/tv/DataBaseDesk$EN_ThreeD_Video_LRVIEWSWITCH;

    aput-object v1, v0, v2

    sget-object v1, Lcom/android/server/tv/DataBaseDesk$EN_ThreeD_Video_LRVIEWSWITCH;->DB_ThreeD_Video_LRVIEWSWITCH_NOTEXCHANGE:Lcom/android/server/tv/DataBaseDesk$EN_ThreeD_Video_LRVIEWSWITCH;

    aput-object v1, v0, v3

    sget-object v1, Lcom/android/server/tv/DataBaseDesk$EN_ThreeD_Video_LRVIEWSWITCH;->DB_ThreeD_Video_LRVIEWSWITCH_COUNT:Lcom/android/server/tv/DataBaseDesk$EN_ThreeD_Video_LRVIEWSWITCH;

    aput-object v1, v0, v4

    sput-object v0, Lcom/android/server/tv/DataBaseDesk$EN_ThreeD_Video_LRVIEWSWITCH;->$VALUES:[Lcom/android/server/tv/DataBaseDesk$EN_ThreeD_Video_LRVIEWSWITCH;

    return-void
.end method

.method private constructor <init>(Ljava/lang/String;I)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()V"
        }
    .end annotation

    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    return-void
.end method

.method public static valueOf(Ljava/lang/String;)Lcom/android/server/tv/DataBaseDesk$EN_ThreeD_Video_LRVIEWSWITCH;
    .locals 1
    .param p0    # Ljava/lang/String;

    const-class v0, Lcom/android/server/tv/DataBaseDesk$EN_ThreeD_Video_LRVIEWSWITCH;

    invoke-static {v0, p0}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;

    move-result-object v0

    check-cast v0, Lcom/android/server/tv/DataBaseDesk$EN_ThreeD_Video_LRVIEWSWITCH;

    return-object v0
.end method

.method public static values()[Lcom/android/server/tv/DataBaseDesk$EN_ThreeD_Video_LRVIEWSWITCH;
    .locals 1

    sget-object v0, Lcom/android/server/tv/DataBaseDesk$EN_ThreeD_Video_LRVIEWSWITCH;->$VALUES:[Lcom/android/server/tv/DataBaseDesk$EN_ThreeD_Video_LRVIEWSWITCH;

    invoke-virtual {v0}, [Lcom/android/server/tv/DataBaseDesk$EN_ThreeD_Video_LRVIEWSWITCH;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [Lcom/android/server/tv/DataBaseDesk$EN_ThreeD_Video_LRVIEWSWITCH;

    return-object v0
.end method
