.class public Lcom/android/server/tv/TvChannelClient;
.super Lcom/mstar/android/tv/ITvChannel$Stub;
.source "TvChannelClient.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/android/server/tv/TvChannelClient$1;,
        Lcom/android/server/tv/TvChannelClient$TV_TS_STATUS;,
        Lcom/android/server/tv/TvChannelClient$MEMBER_SERVICETYPE;
    }
.end annotation


# static fields
.field private static final TAG:Ljava/lang/String; = "TvChannelClient"

.field public static final max_atv_count:I = 0xff

.field public static final max_dtv_count:I = 0x3e8


# instance fields
.field private curChannelNumber:I

.field private curDtvRoute:S

.field private db:Lcom/android/server/tv/DataBaseDesk;

.field private prevChannelNumber:I

.field private tv_tuning_status:Lcom/android/server/tv/TvChannelClient$TV_TS_STATUS;


# direct methods
.method public constructor <init>(Landroid/content/Context;Lcom/android/server/tv/TvHanlder;Lcom/android/server/tv/DataBaseDeskImpl;)V
    .locals 2
    .param p1    # Landroid/content/Context;
    .param p2    # Lcom/android/server/tv/TvHanlder;
    .param p3    # Lcom/android/server/tv/DataBaseDeskImpl;

    const/4 v1, 0x0

    invoke-direct {p0}, Lcom/mstar/android/tv/ITvChannel$Stub;-><init>()V

    iput v1, p0, Lcom/android/server/tv/TvChannelClient;->curChannelNumber:I

    iput v1, p0, Lcom/android/server/tv/TvChannelClient;->prevChannelNumber:I

    sget-object v0, Lcom/android/server/tv/TvChannelClient$TV_TS_STATUS;->E_TS_NONE:Lcom/android/server/tv/TvChannelClient$TV_TS_STATUS;

    iput-object v0, p0, Lcom/android/server/tv/TvChannelClient;->tv_tuning_status:Lcom/android/server/tv/TvChannelClient$TV_TS_STATUS;

    iput-short v1, p0, Lcom/android/server/tv/TvChannelClient;->curDtvRoute:S

    invoke-static {p1}, Lcom/android/server/tv/DataBaseDeskImpl;->getDataBaseMgrInstance(Landroid/content/Context;)Lcom/android/server/tv/DataBaseDeskImpl;

    move-result-object v0

    iput-object v0, p0, Lcom/android/server/tv/TvChannelClient;->db:Lcom/android/server/tv/DataBaseDesk;

    return-void
.end method

.method private getCurrentInputSource()I
    .locals 6

    sget-object v2, Lcom/mstar/android/tvapi/common/vo/TvOsType$EnumInputSource;->E_INPUT_SOURCE_STORAGE:Lcom/mstar/android/tvapi/common/vo/TvOsType$EnumInputSource;

    :try_start_0
    invoke-static {}, Lcom/mstar/android/tvapi/common/TvManager;->getInstance()Lcom/mstar/android/tvapi/common/TvManager;

    move-result-object v3

    if-eqz v3, :cond_0

    invoke-static {}, Lcom/mstar/android/tvapi/common/TvManager;->getInstance()Lcom/mstar/android/tvapi/common/TvManager;

    move-result-object v3

    invoke-virtual {v3}, Lcom/mstar/android/tvapi/common/TvManager;->getCurrentInputSource()Lcom/mstar/android/tvapi/common/vo/TvOsType$EnumInputSource;
    :try_end_0
    .catch Lcom/mstar/android/tvapi/common/exception/TvCommonException; {:try_start_0 .. :try_end_0} :catch_0

    move-result-object v2

    :cond_0
    :goto_0
    invoke-virtual {v2}, Lcom/mstar/android/tvapi/common/vo/TvOsType$EnumInputSource;->ordinal()I

    move-result v1

    const-string v3, "TvChannelClient"

    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    const-string v5, "getCurrentInputSource, return int "

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-static {v3, v4}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    return v1

    :catch_0
    move-exception v0

    invoke-virtual {v0}, Lcom/mstar/android/tvapi/common/exception/TvCommonException;->printStackTrace()V

    goto :goto_0
.end method

.method private makeSourceAtv()V
    .locals 2

    invoke-direct {p0}, Lcom/android/server/tv/TvChannelClient;->getCurrentInputSource()I

    move-result v0

    sget-object v1, Lcom/mstar/android/tvapi/common/vo/TvOsType$EnumInputSource;->E_INPUT_SOURCE_ATV:Lcom/mstar/android/tvapi/common/vo/TvOsType$EnumInputSource;

    invoke-virtual {v1}, Lcom/mstar/android/tvapi/common/vo/TvOsType$EnumInputSource;->ordinal()I

    move-result v1

    if-eq v0, v1, :cond_0

    const-string v0, "Tvapp"

    const-string v1, "makeSourceAtv"

    invoke-static {v0, v1}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    sget-object v0, Lcom/mstar/android/tvapi/common/vo/TvOsType$EnumInputSource;->E_INPUT_SOURCE_ATV:Lcom/mstar/android/tvapi/common/vo/TvOsType$EnumInputSource;

    invoke-direct {p0, v0}, Lcom/android/server/tv/TvChannelClient;->setInputSource(Lcom/mstar/android/tvapi/common/vo/TvOsType$EnumInputSource;)V

    :cond_0
    return-void
.end method

.method private makeSourceDtv()V
    .locals 2

    invoke-direct {p0}, Lcom/android/server/tv/TvChannelClient;->getCurrentInputSource()I

    move-result v0

    sget-object v1, Lcom/mstar/android/tvapi/common/vo/TvOsType$EnumInputSource;->E_INPUT_SOURCE_DTV:Lcom/mstar/android/tvapi/common/vo/TvOsType$EnumInputSource;

    invoke-virtual {v1}, Lcom/mstar/android/tvapi/common/vo/TvOsType$EnumInputSource;->ordinal()I

    move-result v1

    if-eq v0, v1, :cond_0

    const-string v0, "Tvapp"

    const-string v1, "makeSourceDtv"

    invoke-static {v0, v1}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    sget-object v0, Lcom/mstar/android/tvapi/common/vo/TvOsType$EnumInputSource;->E_INPUT_SOURCE_DTV:Lcom/mstar/android/tvapi/common/vo/TvOsType$EnumInputSource;

    invoke-direct {p0, v0}, Lcom/android/server/tv/TvChannelClient;->setInputSource(Lcom/mstar/android/tvapi/common/vo/TvOsType$EnumInputSource;)V

    :cond_0
    return-void
.end method

.method private programSel(II)Z
    .locals 5
    .param p1    # I
    .param p2    # I

    const/4 v4, 0x0

    const-string v1, "TuningService"

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "programSel:"

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, p1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, "u8ServiceType"

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, p2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    :try_start_0
    invoke-static {}, Lcom/mstar/android/tvapi/common/TvManager;->getInstance()Lcom/mstar/android/tvapi/common/TvManager;

    move-result-object v1

    if-eqz v1, :cond_0

    invoke-static {}, Lcom/mstar/android/tvapi/common/TvManager;->getInstance()Lcom/mstar/android/tvapi/common/TvManager;

    move-result-object v1

    invoke-virtual {v1}, Lcom/mstar/android/tvapi/common/TvManager;->getChannelManager()Lcom/mstar/android/tvapi/common/ChannelManager;

    move-result-object v1

    int-to-short v2, p2

    const/4 v3, 0x0

    invoke-virtual {v1, p1, v2, v3}, Lcom/mstar/android/tvapi/common/ChannelManager;->selectProgram(ISI)V
    :try_end_0
    .catch Lcom/mstar/android/tvapi/common/exception/TvCommonException; {:try_start_0 .. :try_end_0} :catch_0

    :cond_0
    :goto_0
    return v4

    :catch_0
    move-exception v0

    invoke-virtual {v0}, Lcom/mstar/android/tvapi/common/exception/TvCommonException;->printStackTrace()V

    goto :goto_0
.end method

.method private setInputSource(Lcom/mstar/android/tvapi/common/vo/TvOsType$EnumInputSource;)V
    .locals 4
    .param p1    # Lcom/mstar/android/tvapi/common/vo/TvOsType$EnumInputSource;

    const-string v1, "TvChannelClient"

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "setInputSource , paras eInputSourceAtv is "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    :try_start_0
    invoke-static {}, Lcom/mstar/android/tvapi/common/TvManager;->getInstance()Lcom/mstar/android/tvapi/common/TvManager;

    move-result-object v1

    if-eqz v1, :cond_0

    invoke-static {}, Lcom/mstar/android/tvapi/common/TvManager;->getInstance()Lcom/mstar/android/tvapi/common/TvManager;

    move-result-object v1

    invoke-virtual {v1, p1}, Lcom/mstar/android/tvapi/common/TvManager;->setInputSource(Lcom/mstar/android/tvapi/common/vo/TvOsType$EnumInputSource;)Z
    :try_end_0
    .catch Lcom/mstar/android/tvapi/common/exception/TvCommonException; {:try_start_0 .. :try_end_0} :catch_0

    :cond_0
    :goto_0
    return-void

    :catch_0
    move-exception v0

    invoke-virtual {v0}, Lcom/mstar/android/tvapi/common/exception/TvCommonException;->printStackTrace()V

    goto :goto_0
.end method


# virtual methods
.method public addProgramToFavorite(IIII)V
    .locals 9
    .param p1    # I
    .param p2    # I
    .param p3    # I
    .param p4    # I
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Landroid/os/RemoteException;
        }
    .end annotation

    const-string v6, "TvChannelClient"

    new-instance v7, Ljava/lang/StringBuilder;

    invoke-direct {v7}, Ljava/lang/StringBuilder;-><init>()V

    const-string v8, "addProgramToFavorite, paras favoriteId is "

    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    invoke-virtual {v7, p1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v7

    const-string v8, " programNo is "

    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    invoke-virtual {v7, p2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v7

    const-string v8, " programType is "

    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    invoke-virtual {v7, p3}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v7

    const-string v8, " programId is "

    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    invoke-virtual {v7, p4}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v7

    invoke-virtual {v7}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v7

    invoke-static {v6, v7}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    const/4 v3, 0x0

    :try_start_0
    invoke-static {}, Lcom/mstar/android/tvapi/common/vo/EnumFavoriteId;->values()[Lcom/mstar/android/tvapi/common/vo/EnumFavoriteId;

    move-result-object v0

    array-length v5, v0

    const/4 v4, 0x0

    :goto_0
    if-ge v4, v5, :cond_0

    aget-object v2, v0, v4

    invoke-virtual {v2}, Lcom/mstar/android/tvapi/common/vo/EnumFavoriteId;->getValue()I

    move-result v6

    if-ne v6, p1, :cond_2

    move-object v3, v2

    :cond_0
    invoke-static {}, Lcom/mstar/android/tvapi/common/TvManager;->getInstance()Lcom/mstar/android/tvapi/common/TvManager;

    move-result-object v6

    if-eqz v6, :cond_1

    invoke-static {}, Lcom/mstar/android/tvapi/common/TvManager;->getInstance()Lcom/mstar/android/tvapi/common/TvManager;

    move-result-object v6

    invoke-virtual {v6}, Lcom/mstar/android/tvapi/common/TvManager;->getChannelManager()Lcom/mstar/android/tvapi/common/ChannelManager;

    move-result-object v6

    int-to-short v7, p3

    invoke-virtual {v6, v3, p2, v7, p4}, Lcom/mstar/android/tvapi/common/ChannelManager;->addProgramToFavorite(Lcom/mstar/android/tvapi/common/vo/EnumFavoriteId;ISI)V
    :try_end_0
    .catch Lcom/mstar/android/tvapi/common/exception/TvCommonException; {:try_start_0 .. :try_end_0} :catch_0

    :cond_1
    :goto_1
    return-void

    :cond_2
    add-int/lit8 v4, v4, 0x1

    goto :goto_0

    :catch_0
    move-exception v1

    invoke-virtual {v1}, Lcom/mstar/android/tvapi/common/exception/TvCommonException;->printStackTrace()V

    goto :goto_1
.end method

.method public changeToFirstService(II)Z
    .locals 5
    .param p1    # I
    .param p2    # I
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Landroid/os/RemoteException;
        }
    .end annotation

    const/4 v4, 0x0

    const-string v1, "TvChannelClient"

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "changeToFirstService, paras enInputType is "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, p1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, " enServiceType is "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, p2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    iput v4, p0, Lcom/android/server/tv/TvChannelClient;->curChannelNumber:I

    :try_start_0
    invoke-static {}, Lcom/mstar/android/tvapi/common/TvManager;->getInstance()Lcom/mstar/android/tvapi/common/TvManager;

    move-result-object v1

    if-eqz v1, :cond_0

    invoke-static {}, Lcom/mstar/android/tvapi/common/TvManager;->getInstance()Lcom/mstar/android/tvapi/common/TvManager;

    move-result-object v1

    invoke-virtual {v1}, Lcom/mstar/android/tvapi/common/TvManager;->getChannelManager()Lcom/mstar/android/tvapi/common/ChannelManager;

    move-result-object v1

    invoke-static {}, Lcom/mstar/android/tvapi/common/vo/EnumFirstServiceInputType;->values()[Lcom/mstar/android/tvapi/common/vo/EnumFirstServiceInputType;

    move-result-object v2

    aget-object v2, v2, p1

    invoke-static {}, Lcom/mstar/android/tvapi/common/vo/EnumFirstServiceType;->values()[Lcom/mstar/android/tvapi/common/vo/EnumFirstServiceType;

    move-result-object v3

    aget-object v3, v3, p2

    invoke-virtual {v1, v2, v3}, Lcom/mstar/android/tvapi/common/ChannelManager;->changeToFirstService(Lcom/mstar/android/tvapi/common/vo/EnumFirstServiceInputType;Lcom/mstar/android/tvapi/common/vo/EnumFirstServiceType;)Z
    :try_end_0
    .catch Lcom/mstar/android/tvapi/common/exception/TvCommonException; {:try_start_0 .. :try_end_0} :catch_0

    :cond_0
    :goto_0
    return v4

    :catch_0
    move-exception v0

    invoke-virtual {v0}, Lcom/mstar/android/tvapi/common/exception/TvCommonException;->printStackTrace()V

    goto :goto_0
.end method

.method public closeSubtitle()Z
    .locals 4
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Landroid/os/RemoteException;
        }
    .end annotation

    const-string v2, "TvChannelClient"

    const-string v3, "closeSubtitle"

    invoke-static {v2, v3}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    const/4 v1, 0x0

    :try_start_0
    invoke-static {}, Lcom/mstar/android/tvapi/dtv/common/DtvManager;->getSubtitleManager()Lcom/mstar/android/tvapi/dtv/common/SubtitleManager;
    :try_end_0
    .catch Lcom/mstar/android/tvapi/common/exception/TvCommonException; {:try_start_0 .. :try_end_0} :catch_0

    move-result-object v1

    :goto_0
    if-eqz v1, :cond_0

    :try_start_1
    invoke-virtual {v1}, Lcom/mstar/android/tvapi/dtv/common/SubtitleManager;->closeSubtitle()Z
    :try_end_1
    .catch Lcom/mstar/android/tvapi/common/exception/TvCommonException; {:try_start_1 .. :try_end_1} :catch_1

    move-result v2

    :goto_1
    return v2

    :catch_0
    move-exception v0

    invoke-virtual {v0}, Lcom/mstar/android/tvapi/common/exception/TvCommonException;->printStackTrace()V

    goto :goto_0

    :catch_1
    move-exception v0

    invoke-virtual {v0}, Lcom/mstar/android/tvapi/common/exception/TvCommonException;->printStackTrace()V

    :cond_0
    const/4 v2, 0x0

    goto :goto_1
.end method

.method public deleteProgramFromFavorite(IIII)V
    .locals 9
    .param p1    # I
    .param p2    # I
    .param p3    # I
    .param p4    # I
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Landroid/os/RemoteException;
        }
    .end annotation

    const-string v6, "TvChannelClient"

    new-instance v7, Ljava/lang/StringBuilder;

    invoke-direct {v7}, Ljava/lang/StringBuilder;-><init>()V

    const-string v8, "deleteProgramFromFavorite, paras favoriteId is "

    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    invoke-virtual {v7, p1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v7

    const-string v8, " programNo is "

    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    invoke-virtual {v7, p2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v7

    const-string v8, " programType is "

    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    invoke-virtual {v7, p3}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v7

    const-string v8, " programId is "

    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    invoke-virtual {v7, p4}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v7

    invoke-virtual {v7}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v7

    invoke-static {v6, v7}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    const/4 v3, 0x0

    invoke-static {}, Lcom/mstar/android/tvapi/common/vo/EnumFavoriteId;->values()[Lcom/mstar/android/tvapi/common/vo/EnumFavoriteId;

    move-result-object v0

    array-length v5, v0

    const/4 v4, 0x0

    :goto_0
    if-ge v4, v5, :cond_0

    aget-object v2, v0, v4

    invoke-virtual {v2}, Lcom/mstar/android/tvapi/common/vo/EnumFavoriteId;->getValue()I

    move-result v6

    if-ne v6, p1, :cond_2

    move-object v3, v2

    :cond_0
    :try_start_0
    invoke-static {}, Lcom/mstar/android/tvapi/common/TvManager;->getInstance()Lcom/mstar/android/tvapi/common/TvManager;

    move-result-object v6

    if-eqz v6, :cond_1

    invoke-static {}, Lcom/mstar/android/tvapi/common/TvManager;->getInstance()Lcom/mstar/android/tvapi/common/TvManager;

    move-result-object v6

    invoke-virtual {v6}, Lcom/mstar/android/tvapi/common/TvManager;->getChannelManager()Lcom/mstar/android/tvapi/common/ChannelManager;

    move-result-object v6

    int-to-short v7, p3

    invoke-virtual {v6, v3, p2, v7, p4}, Lcom/mstar/android/tvapi/common/ChannelManager;->deleteProgramFromFavorite(Lcom/mstar/android/tvapi/common/vo/EnumFavoriteId;ISI)V
    :try_end_0
    .catch Lcom/mstar/android/tvapi/common/exception/TvCommonException; {:try_start_0 .. :try_end_0} :catch_0

    :cond_1
    :goto_1
    return-void

    :cond_2
    add-int/lit8 v4, v4, 0x1

    goto :goto_0

    :catch_0
    move-exception v1

    invoke-virtual {v1}, Lcom/mstar/android/tvapi/common/exception/TvCommonException;->printStackTrace()V

    goto :goto_1
.end method

.method public getAtvCurrentFrequency()I
    .locals 3
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Landroid/os/RemoteException;
        }
    .end annotation

    const/4 v1, 0x0

    :try_start_0
    invoke-static {}, Lcom/mstar/android/tvapi/atv/AtvManager;->getAtvScanManager()Lcom/mstar/android/tvapi/atv/AtvScanManager;

    move-result-object v2

    if-eqz v2, :cond_0

    invoke-static {}, Lcom/mstar/android/tvapi/atv/AtvManager;->getAtvScanManager()Lcom/mstar/android/tvapi/atv/AtvScanManager;

    move-result-object v2

    invoke-interface {v2}, Lcom/mstar/android/tvapi/atv/AtvScanManager;->getCurrentFrequency()I
    :try_end_0
    .catch Lcom/mstar/android/tvapi/common/exception/TvCommonException; {:try_start_0 .. :try_end_0} :catch_0

    move-result v1

    :cond_0
    :goto_0
    return v1

    :catch_0
    move-exception v0

    invoke-virtual {v0}, Lcom/mstar/android/tvapi/common/exception/TvCommonException;->printStackTrace()V

    goto :goto_0
.end method

.method public getAtvProgramInfo(II)I
    .locals 5
    .param p1    # I
    .param p2    # I
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Landroid/os/RemoteException;
        }
    .end annotation

    const-string v2, "TvChannelClient"

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string v4, "getAtvProgramInfo, paras Cmd is "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3, p1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v3

    const-string v4, " u16Program is "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3, p2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v2, v3}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    const/4 v1, 0x0

    :try_start_0
    invoke-static {}, Lcom/mstar/android/tvapi/atv/AtvManager;->getAtvScanManager()Lcom/mstar/android/tvapi/atv/AtvScanManager;

    move-result-object v2

    if-eqz v2, :cond_0

    invoke-static {}, Lcom/mstar/android/tvapi/atv/AtvManager;->getAtvScanManager()Lcom/mstar/android/tvapi/atv/AtvScanManager;

    move-result-object v2

    invoke-static {}, Lcom/mstar/android/tvapi/atv/vo/EnumGetProgramInfo;->values()[Lcom/mstar/android/tvapi/atv/vo/EnumGetProgramInfo;

    move-result-object v3

    aget-object v3, v3, p1

    invoke-interface {v2, v3, p2}, Lcom/mstar/android/tvapi/atv/AtvScanManager;->getAtvProgramInfo(Lcom/mstar/android/tvapi/atv/vo/EnumGetProgramInfo;I)I
    :try_end_0
    .catch Lcom/mstar/android/tvapi/common/exception/TvCommonException; {:try_start_0 .. :try_end_0} :catch_0

    move-result v1

    :cond_0
    :goto_0
    const-string v2, "TvChannelClient"

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string v4, "getAtvProgramInfo, return int "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v2, v3}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    return v1

    :catch_0
    move-exception v0

    invoke-virtual {v0}, Lcom/mstar/android/tvapi/common/exception/TvCommonException;->printStackTrace()V

    goto :goto_0
.end method

.method public getAtvSoundSystem()I
    .locals 7
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Landroid/os/RemoteException;
        }
    .end annotation

    const/4 v3, 0x0

    const/4 v2, 0x0

    invoke-virtual {p0}, Lcom/android/server/tv/TvChannelClient;->getCurrentChannelNumber()I

    move-result v0

    :try_start_0
    invoke-static {}, Lcom/mstar/android/tvapi/atv/AtvManager;->getAtvScanManager()Lcom/mstar/android/tvapi/atv/AtvScanManager;

    move-result-object v4

    if-eqz v4, :cond_0

    invoke-static {}, Lcom/mstar/android/tvapi/atv/AtvManager;->getAtvScanManager()Lcom/mstar/android/tvapi/atv/AtvScanManager;

    move-result-object v4

    sget-object v5, Lcom/mstar/android/tvapi/atv/vo/EnumGetProgramInfo;->E_GET_AUDIO_STANDARD:Lcom/mstar/android/tvapi/atv/vo/EnumGetProgramInfo;

    invoke-interface {v4, v5, v0}, Lcom/mstar/android/tvapi/atv/AtvScanManager;->getAtvProgramInfo(Lcom/mstar/android/tvapi/atv/vo/EnumGetProgramInfo;I)I

    move-result v3

    :cond_0
    invoke-static {v3}, Lcom/mstar/android/tvapi/common/vo/AtvSystemStandard$EnumAtvSystemStandard;->getOrdinalThroughValue(I)I
    :try_end_0
    .catch Lcom/mstar/android/tvapi/common/exception/TvCommonException; {:try_start_0 .. :try_end_0} :catch_0

    move-result v2

    :goto_0
    const-string v4, "TvChannelClient"

    new-instance v5, Ljava/lang/StringBuilder;

    invoke-direct {v5}, Ljava/lang/StringBuilder;-><init>()V

    const-string v6, "getAtvSoundSystem, return int "

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    invoke-static {v4, v5}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    const/4 v4, -0x1

    if-ne v2, v4, :cond_1

    const/4 v2, 0x0

    :cond_1
    return v2

    :catch_0
    move-exception v1

    invoke-virtual {v1}, Lcom/mstar/android/tvapi/common/exception/TvCommonException;->printStackTrace()V

    goto :goto_0
.end method

.method public getAtvStationName(I)Ljava/lang/String;
    .locals 5
    .param p1    # I
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Landroid/os/RemoteException;
        }
    .end annotation

    const/4 v2, 0x0

    sget-object v3, Lcom/mstar/android/tvapi/common/vo/EnumProgramCountType;->E_COUNT_ATV:Lcom/mstar/android/tvapi/common/vo/EnumProgramCountType;

    invoke-virtual {v3}, Lcom/mstar/android/tvapi/common/vo/EnumProgramCountType;->ordinal()I

    move-result v3

    invoke-virtual {p0, v3}, Lcom/android/server/tv/TvChannelClient;->getProgramCount(I)I

    move-result v0

    if-eqz v0, :cond_0

    if-lt p1, v0, :cond_1

    :cond_0
    const-string v3, "TvApp"

    const-string v4, "getAtvStationName null"

    invoke-static {v3, v4}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    const-string v3, " "

    :goto_0
    return-object v3

    :cond_1
    :try_start_0
    invoke-static {}, Lcom/mstar/android/tvapi/atv/AtvManager;->getAtvScanManager()Lcom/mstar/android/tvapi/atv/AtvScanManager;

    move-result-object v3

    if-eqz v3, :cond_2

    invoke-static {}, Lcom/mstar/android/tvapi/atv/AtvManager;->getAtvScanManager()Lcom/mstar/android/tvapi/atv/AtvScanManager;

    move-result-object v3

    invoke-interface {v3, p1}, Lcom/mstar/android/tvapi/atv/AtvScanManager;->getAtvStationName(I)Ljava/lang/String;
    :try_end_0
    .catch Lcom/mstar/android/tvapi/common/exception/TvCommonException; {:try_start_0 .. :try_end_0} :catch_0

    move-result-object v2

    :cond_2
    :goto_1
    move-object v3, v2

    goto :goto_0

    :catch_0
    move-exception v1

    invoke-virtual {v1}, Lcom/mstar/android/tvapi/common/exception/TvCommonException;->printStackTrace()V

    goto :goto_1
.end method

.method public getAtvVideoSystem()I
    .locals 6
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Landroid/os/RemoteException;
        }
    .end annotation

    invoke-virtual {p0}, Lcom/android/server/tv/TvChannelClient;->getCurrentChannelNumber()I

    move-result v0

    const/4 v2, 0x0

    :try_start_0
    invoke-static {}, Lcom/mstar/android/tvapi/atv/AtvManager;->getAtvScanManager()Lcom/mstar/android/tvapi/atv/AtvScanManager;

    move-result-object v3

    if-eqz v3, :cond_0

    invoke-static {}, Lcom/mstar/android/tvapi/atv/AtvManager;->getAtvScanManager()Lcom/mstar/android/tvapi/atv/AtvScanManager;

    move-result-object v3

    sget-object v4, Lcom/mstar/android/tvapi/atv/vo/EnumGetProgramInfo;->E_GET_VIDEO_STANDARD_OF_PROGRAM:Lcom/mstar/android/tvapi/atv/vo/EnumGetProgramInfo;

    invoke-interface {v3, v4, v0}, Lcom/mstar/android/tvapi/atv/AtvScanManager;->getAtvProgramInfo(Lcom/mstar/android/tvapi/atv/vo/EnumGetProgramInfo;I)I
    :try_end_0
    .catch Lcom/mstar/android/tvapi/common/exception/TvCommonException; {:try_start_0 .. :try_end_0} :catch_0

    move-result v2

    :cond_0
    :goto_0
    const-string v3, "TvChannelClient"

    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    const-string v5, "getAtvVideoSystem, return int "

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-static {v3, v4}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    return v2

    :catch_0
    move-exception v1

    invoke-virtual {v1}, Lcom/mstar/android/tvapi/common/exception/TvCommonException;->printStackTrace()V

    goto :goto_0
.end method

.method public getChannelSwitchMode()I
    .locals 1
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Landroid/os/RemoteException;
        }
    .end annotation

    iget-object v0, p0, Lcom/android/server/tv/TvChannelClient;->db:Lcom/android/server/tv/DataBaseDesk;

    invoke-interface {v0}, Lcom/android/server/tv/DataBaseDesk;->queryChSwMode()Lcom/android/server/tv/DataBaseDesk$EN_MS_CHANNEL_SWITCH_MODE;

    move-result-object v0

    invoke-virtual {v0}, Lcom/android/server/tv/DataBaseDesk$EN_MS_CHANNEL_SWITCH_MODE;->ordinal()I

    move-result v0

    return v0
.end method

.method public getCurrentChannelNumber()I
    .locals 5
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Landroid/os/RemoteException;
        }
    .end annotation

    const/4 v1, 0x0

    :try_start_0
    invoke-static {}, Lcom/mstar/android/tvapi/common/TvManager;->getInstance()Lcom/mstar/android/tvapi/common/TvManager;

    move-result-object v2

    if-eqz v2, :cond_0

    invoke-static {}, Lcom/mstar/android/tvapi/common/TvManager;->getInstance()Lcom/mstar/android/tvapi/common/TvManager;

    move-result-object v2

    invoke-virtual {v2}, Lcom/mstar/android/tvapi/common/TvManager;->getChannelManager()Lcom/mstar/android/tvapi/common/ChannelManager;

    move-result-object v2

    invoke-virtual {v2}, Lcom/mstar/android/tvapi/common/ChannelManager;->getCurrChannelNumber()I

    move-result v1

    :cond_0
    invoke-direct {p0}, Lcom/android/server/tv/TvChannelClient;->getCurrentInputSource()I

    move-result v2

    sget-object v3, Lcom/mstar/android/tvapi/common/vo/TvOsType$EnumInputSource;->E_INPUT_SOURCE_ATV:Lcom/mstar/android/tvapi/common/vo/TvOsType$EnumInputSource;

    invoke-virtual {v3}, Lcom/mstar/android/tvapi/common/vo/TvOsType$EnumInputSource;->ordinal()I

    move-result v3

    if-ne v2, v3, :cond_3

    const/16 v2, 0xff

    if-gt v1, v2, :cond_1

    if-gez v1, :cond_2

    :cond_1
    const-string v2, "Mapp"

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string v4, "getatvCurrentChannelNumber error:"

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v2, v3}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I
    :try_end_0
    .catch Lcom/mstar/android/tvapi/common/exception/TvCommonException; {:try_start_0 .. :try_end_0} :catch_0

    const/16 v1, 0xff

    :cond_2
    :goto_0
    const-string v2, "TvChannelClient"

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string v4, "getCurrentChannelNumber, return int "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v2, v3}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    return v1

    :cond_3
    const/16 v2, 0x3e8

    if-gt v1, v2, :cond_4

    if-gez v1, :cond_2

    :cond_4
    :try_start_1
    const-string v2, "Mapp"

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string v4, "getdtvCurrentChannelNumber error:"

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v2, v3}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I
    :try_end_1
    .catch Lcom/mstar/android/tvapi/common/exception/TvCommonException; {:try_start_1 .. :try_end_1} :catch_0

    const/16 v1, 0x3e8

    goto :goto_0

    :catch_0
    move-exception v0

    invoke-virtual {v0}, Lcom/mstar/android/tvapi/common/exception/TvCommonException;->printStackTrace()V

    goto :goto_0
.end method

.method public getCurrentLanguageIndex(Ljava/lang/String;)I
    .locals 5
    .param p1    # Ljava/lang/String;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Landroid/os/RemoteException;
        }
    .end annotation

    const-string v2, "TvChannelClient"

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string v4, "getCurrentLanguageIndex, paras languageCode is "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v2, v3}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    const/4 v1, -0x1

    :try_start_0
    invoke-static {}, Lcom/mstar/android/tvapi/common/TvManager;->getInstance()Lcom/mstar/android/tvapi/common/TvManager;

    move-result-object v2

    if-eqz v2, :cond_0

    invoke-static {}, Lcom/mstar/android/tvapi/common/TvManager;->getInstance()Lcom/mstar/android/tvapi/common/TvManager;

    move-result-object v2

    invoke-virtual {v2, p1}, Lcom/mstar/android/tvapi/common/TvManager;->getCurrentLanguageIndex(Ljava/lang/String;)Lcom/mstar/android/tvapi/common/vo/TvOsType$EnumLanguage;

    move-result-object v2

    invoke-virtual {v2}, Lcom/mstar/android/tvapi/common/vo/TvOsType$EnumLanguage;->ordinal()I

    move-result v1

    const-string v2, "TvChannelClient"

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string v4, "getCurrentLanguageIndex, return int "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v2, v3}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I
    :try_end_0
    .catch Lcom/mstar/android/tvapi/common/exception/TvCommonException; {:try_start_0 .. :try_end_0} :catch_0

    :cond_0
    :goto_0
    return v1

    :catch_0
    move-exception v0

    invoke-virtual {v0}, Lcom/mstar/android/tvapi/common/exception/TvCommonException;->printStackTrace()V

    sget-object v2, Lcom/mstar/android/tvapi/common/vo/TvOsType$EnumLanguage;->E_CHINESE:Lcom/mstar/android/tvapi/common/vo/TvOsType$EnumLanguage;

    invoke-virtual {v2}, Lcom/mstar/android/tvapi/common/vo/TvOsType$EnumLanguage;->ordinal()I

    move-result v1

    const-string v2, "TvChannelClient"

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string v4, "getCurrentLanguageIndex, return int "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v2, v3}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_0
.end method

.method public getProgramAttribute(IIII)Z
    .locals 5
    .param p1    # I
    .param p2    # I
    .param p3    # I
    .param p4    # I
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Landroid/os/RemoteException;
        }
    .end annotation

    const-string v2, "TvChannelClient"

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string v4, "getProgramAttribute, paras enpa is "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3, p1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v3

    const-string v4, " programNo is "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3, p2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v3

    const-string v4, " programType is "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3, p3}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v3

    const-string v4, " programId is "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3, p4}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v2, v3}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    const/4 v0, 0x0

    :try_start_0
    invoke-static {}, Lcom/mstar/android/tvapi/common/TvManager;->getInstance()Lcom/mstar/android/tvapi/common/TvManager;

    move-result-object v2

    if-eqz v2, :cond_0

    invoke-static {}, Lcom/mstar/android/tvapi/common/TvManager;->getInstance()Lcom/mstar/android/tvapi/common/TvManager;

    move-result-object v2

    invoke-virtual {v2}, Lcom/mstar/android/tvapi/common/TvManager;->getChannelManager()Lcom/mstar/android/tvapi/common/ChannelManager;

    move-result-object v2

    invoke-static {}, Lcom/mstar/android/tvapi/common/vo/EnumProgramAttribute;->values()[Lcom/mstar/android/tvapi/common/vo/EnumProgramAttribute;

    move-result-object v3

    aget-object v3, v3, p1

    int-to-short v4, p3

    invoke-virtual {v2, v3, p2, v4, p4}, Lcom/mstar/android/tvapi/common/ChannelManager;->getProgramAttribute(Lcom/mstar/android/tvapi/common/vo/EnumProgramAttribute;ISI)Z
    :try_end_0
    .catch Lcom/mstar/android/tvapi/common/exception/TvCommonException; {:try_start_0 .. :try_end_0} :catch_0

    move-result v0

    :cond_0
    :goto_0
    return v0

    :catch_0
    move-exception v1

    invoke-virtual {v1}, Lcom/mstar/android/tvapi/common/exception/TvCommonException;->printStackTrace()V

    goto :goto_0
.end method

.method public getProgramCount(I)I
    .locals 5
    .param p1    # I
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Landroid/os/RemoteException;
        }
    .end annotation

    const/4 v1, 0x0

    :try_start_0
    invoke-static {}, Lcom/mstar/android/tvapi/common/TvManager;->getInstance()Lcom/mstar/android/tvapi/common/TvManager;

    move-result-object v2

    if-eqz v2, :cond_0

    invoke-static {}, Lcom/mstar/android/tvapi/common/TvManager;->getInstance()Lcom/mstar/android/tvapi/common/TvManager;

    move-result-object v2

    invoke-virtual {v2}, Lcom/mstar/android/tvapi/common/TvManager;->getChannelManager()Lcom/mstar/android/tvapi/common/ChannelManager;

    move-result-object v2

    invoke-static {}, Lcom/mstar/android/tvapi/common/vo/EnumProgramCountType;->values()[Lcom/mstar/android/tvapi/common/vo/EnumProgramCountType;

    move-result-object v3

    aget-object v3, v3, p1

    invoke-virtual {v2, v3}, Lcom/mstar/android/tvapi/common/ChannelManager;->getProgramCount(Lcom/mstar/android/tvapi/common/vo/EnumProgramCountType;)I
    :try_end_0
    .catch Lcom/mstar/android/tvapi/common/exception/TvCommonException; {:try_start_0 .. :try_end_0} :catch_0

    move-result v1

    :cond_0
    :goto_0
    const-string v2, "TuningService"

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string v4, "getProgramCount:"

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v2, v3}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    return v1

    :catch_0
    move-exception v0

    invoke-virtual {v0}, Lcom/mstar/android/tvapi/common/exception/TvCommonException;->printStackTrace()V

    goto :goto_0
.end method

.method public getProgramCtrl(III)I
    .locals 5
    .param p1    # I
    .param p2    # I
    .param p3    # I
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Landroid/os/RemoteException;
        }
    .end annotation

    const-string v2, "TvChannelClient"

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string v4, "getProgramCtrl, paras Cmd is "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3, p1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v3

    const-string v4, " u16Param2 is "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3, p2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v3

    const-string v4, " u16Param3 is "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3, p3}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v2, v3}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    const/4 v1, 0x0

    :try_start_0
    invoke-static {}, Lcom/mstar/android/tvapi/atv/AtvManager;->getAtvScanManager()Lcom/mstar/android/tvapi/atv/AtvScanManager;

    move-result-object v2

    if-eqz v2, :cond_0

    invoke-static {}, Lcom/mstar/android/tvapi/atv/AtvManager;->getAtvScanManager()Lcom/mstar/android/tvapi/atv/AtvScanManager;

    move-result-object v2

    invoke-static {}, Lcom/mstar/android/tvapi/atv/vo/EnumGetProgramCtrl;->values()[Lcom/mstar/android/tvapi/atv/vo/EnumGetProgramCtrl;

    move-result-object v3

    aget-object v3, v3, p1

    invoke-interface {v2, v3, p2, p3}, Lcom/mstar/android/tvapi/atv/AtvScanManager;->getProgramControl(Lcom/mstar/android/tvapi/atv/vo/EnumGetProgramCtrl;II)I
    :try_end_0
    .catch Lcom/mstar/android/tvapi/common/exception/TvCommonException; {:try_start_0 .. :try_end_0} :catch_0

    move-result v1

    :cond_0
    :goto_0
    const-string v2, "TvChannelClient"

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string v4, "getProgramCtrl, return int "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v2, v3}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    return v1

    :catch_0
    move-exception v0

    invoke-virtual {v0}, Lcom/mstar/android/tvapi/common/exception/TvCommonException;->printStackTrace()V

    goto :goto_0
.end method

.method public getProgramInfo(Lcom/mstar/android/tvapi/common/vo/ProgramInfoQueryCriteria;I)Lcom/mstar/android/tvapi/common/vo/ProgramInfo;
    .locals 5
    .param p1    # Lcom/mstar/android/tvapi/common/vo/ProgramInfoQueryCriteria;
    .param p2    # I
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Landroid/os/RemoteException;
        }
    .end annotation

    const-string v2, "TvChannelClient"

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string v4, "getProgramInfo, paras ProgramInfoQueryCriteria is "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    iget v4, p1, Lcom/mstar/android/tvapi/common/vo/ProgramInfoQueryCriteria;->queryIndex:I

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v3

    const-string v4, " EnumProgramInfoType is "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3, p2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v2, v3}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    const/4 v1, 0x0

    :try_start_0
    invoke-static {}, Lcom/mstar/android/tvapi/common/TvManager;->getInstance()Lcom/mstar/android/tvapi/common/TvManager;

    move-result-object v2

    if-eqz v2, :cond_0

    invoke-static {}, Lcom/mstar/android/tvapi/common/TvManager;->getInstance()Lcom/mstar/android/tvapi/common/TvManager;

    move-result-object v2

    invoke-virtual {v2}, Lcom/mstar/android/tvapi/common/TvManager;->getChannelManager()Lcom/mstar/android/tvapi/common/ChannelManager;

    move-result-object v2

    invoke-static {}, Lcom/mstar/android/tvapi/common/vo/EnumProgramInfoType;->values()[Lcom/mstar/android/tvapi/common/vo/EnumProgramInfoType;

    move-result-object v3

    aget-object v3, v3, p2

    invoke-virtual {v2, p1, v3}, Lcom/mstar/android/tvapi/common/ChannelManager;->getProgramInfo(Lcom/mstar/android/tvapi/common/vo/ProgramInfoQueryCriteria;Lcom/mstar/android/tvapi/common/vo/EnumProgramInfoType;)Lcom/mstar/android/tvapi/common/vo/ProgramInfo;
    :try_end_0
    .catch Lcom/mstar/android/tvapi/common/exception/TvCommonException; {:try_start_0 .. :try_end_0} :catch_0

    move-result-object v1

    :cond_0
    :goto_0
    return-object v1

    :catch_0
    move-exception v0

    invoke-virtual {v0}, Lcom/mstar/android/tvapi/common/exception/TvCommonException;->printStackTrace()V

    goto :goto_0
.end method

.method public getProgramName(III)Ljava/lang/String;
    .locals 5
    .param p1    # I
    .param p2    # I
    .param p3    # I
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Landroid/os/RemoteException;
        }
    .end annotation

    const-string v2, "TvChannelClient"

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string v4, "getProgramName, paras progNo is "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3, p1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v3

    const-string v4, " progType is "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3, p2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v3

    const-string v4, " progId is "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3, p3}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v2, v3}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    const-string v1, ""

    :try_start_0
    invoke-static {}, Lcom/mstar/android/tvapi/common/TvManager;->getInstance()Lcom/mstar/android/tvapi/common/TvManager;

    move-result-object v2

    if-eqz v2, :cond_0

    invoke-static {}, Lcom/mstar/android/tvapi/common/TvManager;->getInstance()Lcom/mstar/android/tvapi/common/TvManager;

    move-result-object v2

    invoke-virtual {v2}, Lcom/mstar/android/tvapi/common/TvManager;->getChannelManager()Lcom/mstar/android/tvapi/common/ChannelManager;

    move-result-object v2

    int-to-short v3, p2

    int-to-short v4, p3

    invoke-virtual {v2, p1, v3, v4}, Lcom/mstar/android/tvapi/common/ChannelManager;->getProgramName(ISS)Ljava/lang/String;
    :try_end_0
    .catch Lcom/mstar/android/tvapi/common/exception/TvCommonException; {:try_start_0 .. :try_end_0} :catch_0

    move-result-object v1

    :cond_0
    :goto_0
    return-object v1

    :catch_0
    move-exception v0

    invoke-virtual {v0}, Lcom/mstar/android/tvapi/common/exception/TvCommonException;->printStackTrace()V

    goto :goto_0
.end method

.method public getRfInfo(II)Lcom/mstar/android/tvapi/dtv/vo/RfInfo;
    .locals 5
    .param p1    # I
    .param p2    # I
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Landroid/os/RemoteException;
        }
    .end annotation

    const-string v2, "TvChannelClient"

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string v4, "getRfInfo, paras rfSignalInfoType is "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3, p1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v3

    const-string v4, " rfChNo is "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3, p2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v2, v3}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    const/4 v1, 0x0

    :try_start_0
    invoke-static {}, Lcom/mstar/android/tvapi/dtv/common/DtvManager;->getDvbPlayerManager()Lcom/mstar/android/tvapi/dtv/dvb/DvbPlayer;

    move-result-object v2

    if-eqz v2, :cond_0

    invoke-static {}, Lcom/mstar/android/tvapi/dtv/common/DtvManager;->getDvbPlayerManager()Lcom/mstar/android/tvapi/dtv/dvb/DvbPlayer;

    move-result-object v2

    invoke-static {}, Lcom/mstar/android/tvapi/dtv/vo/RfInfo$EnumInfoType;->values()[Lcom/mstar/android/tvapi/dtv/vo/RfInfo$EnumInfoType;

    move-result-object v3

    aget-object v3, v3, p1

    invoke-interface {v2, v3, p2}, Lcom/mstar/android/tvapi/dtv/dvb/DvbPlayer;->getRfInfo(Lcom/mstar/android/tvapi/dtv/vo/RfInfo$EnumInfoType;I)Lcom/mstar/android/tvapi/dtv/vo/RfInfo;

    :cond_0
    const-string v2, "TvChannelClient"

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string v4, "getRfInfo, return RfInfo frequency = "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    iget v4, v1, Lcom/mstar/android/tvapi/dtv/vo/RfInfo;->frequency:I

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v3

    const-string v4, ", isVHF = "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    iget-boolean v4, v1, Lcom/mstar/android/tvapi/dtv/vo/RfInfo;->isVHF:Z

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v3

    const-string v4, ", rfName = "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    iget-object v4, v1, Lcom/mstar/android/tvapi/dtv/vo/RfInfo;->rfName:Ljava/lang/String;

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    const-string v4, ", rfPhyNum = "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    iget-short v4, v1, Lcom/mstar/android/tvapi/dtv/vo/RfInfo;->rfPhyNum:S

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v2, v3}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I
    :try_end_0
    .catch Lcom/mstar/android/tvapi/common/exception/TvCommonException; {:try_start_0 .. :try_end_0} :catch_0

    :goto_0
    return-object v1

    :catch_0
    move-exception v0

    invoke-virtual {v0}, Lcom/mstar/android/tvapi/common/exception/TvCommonException;->printStackTrace()V

    goto :goto_0
.end method

.method public getSIFMtsMode()I
    .locals 5
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Landroid/os/RemoteException;
        }
    .end annotation

    :try_start_0
    invoke-static {}, Lcom/mstar/android/tvapi/common/TvManager;->getInstance()Lcom/mstar/android/tvapi/common/TvManager;

    move-result-object v2

    if-eqz v2, :cond_0

    invoke-static {}, Lcom/mstar/android/tvapi/common/TvManager;->getInstance()Lcom/mstar/android/tvapi/common/TvManager;

    move-result-object v2

    invoke-virtual {v2}, Lcom/mstar/android/tvapi/common/TvManager;->getAudioManager()Lcom/mstar/android/tvapi/common/AudioManager;

    move-result-object v2

    invoke-virtual {v2}, Lcom/mstar/android/tvapi/common/AudioManager;->getAtvMtsMode()Lcom/mstar/android/tvapi/common/vo/EnumAtvAudioModeType;

    move-result-object v2

    invoke-virtual {v2}, Lcom/mstar/android/tvapi/common/vo/EnumAtvAudioModeType;->ordinal()I
    :try_end_0
    .catch Lcom/mstar/android/tvapi/common/exception/TvCommonException; {:try_start_0 .. :try_end_0} :catch_0

    move-result v1

    :goto_0
    return v1

    :catch_0
    move-exception v0

    invoke-virtual {v0}, Lcom/mstar/android/tvapi/common/exception/TvCommonException;->printStackTrace()V

    :cond_0
    sget-object v2, Lcom/mstar/android/tvapi/common/vo/EnumAtvAudioModeType;->E_ATV_AUDIOMODE_MONO:Lcom/mstar/android/tvapi/common/vo/EnumAtvAudioModeType;

    invoke-virtual {v2}, Lcom/mstar/android/tvapi/common/vo/EnumAtvAudioModeType;->ordinal()I

    move-result v1

    const-string v2, "TvChannelClient"

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string v4, "getSIFMtsMode, return int "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v2, v3}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_0
.end method

.method public getSubtitleInfo()Lcom/mstar/android/tvapi/dtv/vo/DtvSubtitleInfo;
    .locals 6
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Landroid/os/RemoteException;
        }
    .end annotation

    const/4 v2, 0x0

    const/4 v1, 0x0

    :try_start_0
    invoke-static {}, Lcom/mstar/android/tvapi/dtv/common/DtvManager;->getSubtitleManager()Lcom/mstar/android/tvapi/dtv/common/SubtitleManager;
    :try_end_0
    .catch Lcom/mstar/android/tvapi/common/exception/TvCommonException; {:try_start_0 .. :try_end_0} :catch_0

    move-result-object v1

    :goto_0
    if-eqz v1, :cond_0

    :try_start_1
    invoke-virtual {v1}, Lcom/mstar/android/tvapi/dtv/common/SubtitleManager;->getSubtitleInfo()Lcom/mstar/android/tvapi/dtv/vo/DtvSubtitleInfo;

    move-result-object v2

    const-string v3, "TvChannelClient"

    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    const-string v5, "getSubtitleInfo(), return DtvSubtitleInfo currentSubtitleIndex = "

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    iget-short v5, v2, Lcom/mstar/android/tvapi/dtv/vo/DtvSubtitleInfo;->currentSubtitleIndex:S

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v4

    const-string v5, ", subtitleServiceNumber = "

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    iget-short v5, v2, Lcom/mstar/android/tvapi/dtv/vo/DtvSubtitleInfo;->subtitleServiceNumber:S

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v4

    const-string v5, ", subtitleOn = "

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    iget-boolean v5, v2, Lcom/mstar/android/tvapi/dtv/vo/DtvSubtitleInfo;->subtitleOn:Z

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-static {v3, v4}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I
    :try_end_1
    .catch Lcom/mstar/android/tvapi/common/exception/TvCommonException; {:try_start_1 .. :try_end_1} :catch_1

    move-object v3, v2

    :goto_1
    return-object v3

    :catch_0
    move-exception v0

    invoke-virtual {v0}, Lcom/mstar/android/tvapi/common/exception/TvCommonException;->printStackTrace()V

    goto :goto_0

    :catch_1
    move-exception v0

    invoke-virtual {v0}, Lcom/mstar/android/tvapi/common/exception/TvCommonException;->printStackTrace()V

    :cond_0
    const/4 v3, 0x0

    goto :goto_1
.end method

.method public getSystemCountry()I
    .locals 4
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Landroid/os/RemoteException;
        }
    .end annotation

    iget-object v1, p0, Lcom/android/server/tv/TvChannelClient;->db:Lcom/android/server/tv/DataBaseDesk;

    invoke-interface {v1}, Lcom/android/server/tv/DataBaseDesk;->queryCountry()I

    move-result v0

    const-string v1, "TvChannelClient"

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "getSystemCountry, return int "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    return v0
.end method

.method public getVideoStandard()I
    .locals 6
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Landroid/os/RemoteException;
        }
    .end annotation

    const/4 v1, -0x1

    :try_start_0
    invoke-static {}, Lcom/mstar/android/tvapi/common/TvManager;->getInstance()Lcom/mstar/android/tvapi/common/TvManager;

    move-result-object v3

    if-eqz v3, :cond_0

    invoke-static {}, Lcom/mstar/android/tvapi/common/TvManager;->getInstance()Lcom/mstar/android/tvapi/common/TvManager;

    move-result-object v3

    invoke-virtual {v3}, Lcom/mstar/android/tvapi/common/TvManager;->getPlayerManager()Lcom/mstar/android/tvapi/common/TvPlayer;

    move-result-object v3

    invoke-interface {v3}, Lcom/mstar/android/tvapi/common/TvPlayer;->getVideoStandard()Lcom/mstar/android/tvapi/common/vo/EnumAvdVideoStandardType;

    move-result-object v3

    invoke-virtual {v3}, Lcom/mstar/android/tvapi/common/vo/EnumAvdVideoStandardType;->ordinal()I

    move-result v1

    :cond_0
    const-string v3, "TvChannelClient"

    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    const-string v5, "getVideoStandard, return int "

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-static {v3, v4}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I
    :try_end_0
    .catch Lcom/mstar/android/tvapi/common/exception/TvCommonException; {:try_start_0 .. :try_end_0} :catch_0

    move v2, v1

    :goto_0
    return v2

    :catch_0
    move-exception v0

    invoke-virtual {v0}, Lcom/mstar/android/tvapi/common/exception/TvCommonException;->printStackTrace()V

    move v2, v1

    goto :goto_0
.end method

.method public isSignalStabled()Z
    .locals 4
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Landroid/os/RemoteException;
        }
    .end annotation

    const/4 v1, 0x0

    const-string v2, "TvChannelClient"

    const-string v3, "isSignalStabled"

    invoke-static {v2, v3}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    invoke-static {}, Lcom/mstar/android/tvapi/common/TvManager;->getInstance()Lcom/mstar/android/tvapi/common/TvManager;

    move-result-object v2

    if-eqz v2, :cond_0

    invoke-static {}, Lcom/mstar/android/tvapi/common/TvManager;->getInstance()Lcom/mstar/android/tvapi/common/TvManager;

    move-result-object v2

    invoke-virtual {v2}, Lcom/mstar/android/tvapi/common/TvManager;->getPlayerManager()Lcom/mstar/android/tvapi/common/TvPlayer;

    move-result-object v2

    if-eqz v2, :cond_0

    :try_start_0
    invoke-static {}, Lcom/mstar/android/tvapi/common/TvManager;->getInstance()Lcom/mstar/android/tvapi/common/TvManager;

    move-result-object v2

    invoke-virtual {v2}, Lcom/mstar/android/tvapi/common/TvManager;->getPlayerManager()Lcom/mstar/android/tvapi/common/TvPlayer;

    move-result-object v2

    invoke-interface {v2}, Lcom/mstar/android/tvapi/common/TvPlayer;->isSignalStable()Z
    :try_end_0
    .catch Lcom/mstar/android/tvapi/common/exception/TvCommonException; {:try_start_0 .. :try_end_0} :catch_0

    move-result v1

    :cond_0
    :goto_0
    return v1

    :catch_0
    move-exception v0

    invoke-virtual {v0}, Lcom/mstar/android/tvapi/common/exception/TvCommonException;->printStackTrace()V

    goto :goto_0
.end method

.method public openSubtitle(I)Z
    .locals 5
    .param p1    # I
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Landroid/os/RemoteException;
        }
    .end annotation

    const-string v2, "TvChannelClient"

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string v4, "openSubtitle , paras index is "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3, p1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v2, v3}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    const/4 v1, 0x0

    :try_start_0
    invoke-static {}, Lcom/mstar/android/tvapi/dtv/common/DtvManager;->getSubtitleManager()Lcom/mstar/android/tvapi/dtv/common/SubtitleManager;
    :try_end_0
    .catch Lcom/mstar/android/tvapi/common/exception/TvCommonException; {:try_start_0 .. :try_end_0} :catch_0

    move-result-object v1

    :goto_0
    if-eqz v1, :cond_0

    :try_start_1
    invoke-virtual {v1, p1}, Lcom/mstar/android/tvapi/dtv/common/SubtitleManager;->openSubtitle(I)Z
    :try_end_1
    .catch Lcom/mstar/android/tvapi/common/exception/TvCommonException; {:try_start_1 .. :try_end_1} :catch_1

    move-result v2

    :goto_1
    return v2

    :catch_0
    move-exception v0

    invoke-virtual {v0}, Lcom/mstar/android/tvapi/common/exception/TvCommonException;->printStackTrace()V

    goto :goto_0

    :catch_1
    move-exception v0

    invoke-virtual {v0}, Lcom/mstar/android/tvapi/common/exception/TvCommonException;->printStackTrace()V

    :cond_0
    const/4 v2, 0x0

    goto :goto_1
.end method

.method public pauseAtvAutoTuning()Z
    .locals 4
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Landroid/os/RemoteException;
        }
    .end annotation

    const-string v2, "TvChannelClient"

    const-string v3, "pauseAtvAutoTuning"

    invoke-static {v2, v3}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    const/4 v1, 0x0

    :try_start_0
    invoke-static {}, Lcom/mstar/android/tvapi/atv/AtvManager;->getAtvScanManager()Lcom/mstar/android/tvapi/atv/AtvScanManager;

    move-result-object v2

    if-eqz v2, :cond_0

    invoke-static {}, Lcom/mstar/android/tvapi/atv/AtvManager;->getAtvScanManager()Lcom/mstar/android/tvapi/atv/AtvScanManager;

    move-result-object v2

    invoke-interface {v2}, Lcom/mstar/android/tvapi/atv/AtvScanManager;->setAutoTuningPause()Z

    move-result v1

    :cond_0
    sget-object v2, Lcom/android/server/tv/TvChannelClient$TV_TS_STATUS;->E_TS_ATV_SCAN_PAUSING:Lcom/android/server/tv/TvChannelClient$TV_TS_STATUS;

    iput-object v2, p0, Lcom/android/server/tv/TvChannelClient;->tv_tuning_status:Lcom/android/server/tv/TvChannelClient$TV_TS_STATUS;
    :try_end_0
    .catch Lcom/mstar/android/tvapi/common/exception/TvCommonException; {:try_start_0 .. :try_end_0} :catch_0

    :goto_0
    return v1

    :catch_0
    move-exception v0

    invoke-virtual {v0}, Lcom/mstar/android/tvapi/common/exception/TvCommonException;->printStackTrace()V

    goto :goto_0
.end method

.method public pauseDtvScan()Z
    .locals 3
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Landroid/os/RemoteException;
        }
    .end annotation

    const-string v1, "TvChannelClient"

    const-string v2, "pauseDtvScan"

    invoke-static {v1, v2}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    :try_start_0
    invoke-static {}, Lcom/mstar/android/tvapi/dtv/common/DtvManager;->getDvbcScanManager()Lcom/mstar/android/tvapi/dtv/dvb/dvbc/DvbcScanManager;

    move-result-object v1

    if-eqz v1, :cond_0

    invoke-static {}, Lcom/mstar/android/tvapi/dtv/common/DtvManager;->getDvbcScanManager()Lcom/mstar/android/tvapi/dtv/dvb/dvbc/DvbcScanManager;

    move-result-object v1

    invoke-interface {v1}, Lcom/mstar/android/tvapi/dtv/dvb/dvbc/DvbcScanManager;->pauseScan()Z

    :cond_0
    sget-object v1, Lcom/android/server/tv/TvChannelClient$TV_TS_STATUS;->E_TS_DTV_SCAN_PAUSING:Lcom/android/server/tv/TvChannelClient$TV_TS_STATUS;

    iput-object v1, p0, Lcom/android/server/tv/TvChannelClient;->tv_tuning_status:Lcom/android/server/tv/TvChannelClient$TV_TS_STATUS;
    :try_end_0
    .catch Lcom/mstar/android/tvapi/common/exception/TvCommonException; {:try_start_0 .. :try_end_0} :catch_0

    :goto_0
    const/4 v1, 0x0

    return v1

    :catch_0
    move-exception v0

    invoke-virtual {v0}, Lcom/mstar/android/tvapi/common/exception/TvCommonException;->printStackTrace()V

    goto :goto_0
.end method

.method public playDtvCurrentProgram()Z
    .locals 3
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Landroid/os/RemoteException;
        }
    .end annotation

    const-string v1, "TvChannelClient"

    const-string v2, "playDtvCurrentProgram"

    invoke-static {v1, v2}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    :try_start_0
    invoke-static {}, Lcom/mstar/android/tvapi/dtv/common/DtvManager;->getDvbPlayerManager()Lcom/mstar/android/tvapi/dtv/dvb/DvbPlayer;

    move-result-object v1

    if-eqz v1, :cond_0

    invoke-static {}, Lcom/mstar/android/tvapi/dtv/common/DtvManager;->getDvbPlayerManager()Lcom/mstar/android/tvapi/dtv/dvb/DvbPlayer;

    move-result-object v1

    invoke-interface {v1}, Lcom/mstar/android/tvapi/dtv/dvb/DvbPlayer;->playCurrentProgram()Z
    :try_end_0
    .catch Lcom/mstar/android/tvapi/common/exception/TvCommonException; {:try_start_0 .. :try_end_0} :catch_0

    :cond_0
    :goto_0
    const/4 v1, 0x0

    return v1

    :catch_0
    move-exception v0

    invoke-virtual {v0}, Lcom/mstar/android/tvapi/common/exception/TvCommonException;->printStackTrace()V

    goto :goto_0
.end method

.method public programDown()Z
    .locals 3
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Landroid/os/RemoteException;
        }
    .end annotation

    const-string v1, "TvChannelClient"

    const-string v2, "programDown"

    invoke-static {v1, v2}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    :try_start_0
    invoke-static {}, Lcom/mstar/android/tvapi/common/TvManager;->getInstance()Lcom/mstar/android/tvapi/common/TvManager;

    move-result-object v1

    if-eqz v1, :cond_0

    invoke-static {}, Lcom/mstar/android/tvapi/common/TvManager;->getInstance()Lcom/mstar/android/tvapi/common/TvManager;

    move-result-object v1

    invoke-virtual {v1}, Lcom/mstar/android/tvapi/common/TvManager;->getChannelManager()Lcom/mstar/android/tvapi/common/ChannelManager;

    move-result-object v1

    sget-object v2, Lcom/mstar/android/tvapi/common/vo/EnumProgramLoopType;->E_PROG_LOOP_ALL:Lcom/mstar/android/tvapi/common/vo/EnumProgramLoopType;

    invoke-virtual {v1, v2}, Lcom/mstar/android/tvapi/common/ChannelManager;->programDown(Lcom/mstar/android/tvapi/common/vo/EnumProgramLoopType;)V
    :try_end_0
    .catch Lcom/mstar/android/tvapi/common/exception/TvCommonException; {:try_start_0 .. :try_end_0} :catch_0

    :cond_0
    :goto_0
    const/4 v1, 0x1

    return v1

    :catch_0
    move-exception v0

    invoke-virtual {v0}, Lcom/mstar/android/tvapi/common/exception/TvCommonException;->printStackTrace()V

    goto :goto_0
.end method

.method public programUp()Z
    .locals 3
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Landroid/os/RemoteException;
        }
    .end annotation

    const-string v1, "TvChannelClient"

    const-string v2, "programUp"

    invoke-static {v1, v2}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    :try_start_0
    invoke-static {}, Lcom/mstar/android/tvapi/common/TvManager;->getInstance()Lcom/mstar/android/tvapi/common/TvManager;

    move-result-object v1

    if-eqz v1, :cond_0

    invoke-static {}, Lcom/mstar/android/tvapi/common/TvManager;->getInstance()Lcom/mstar/android/tvapi/common/TvManager;

    move-result-object v1

    invoke-virtual {v1}, Lcom/mstar/android/tvapi/common/TvManager;->getChannelManager()Lcom/mstar/android/tvapi/common/ChannelManager;

    move-result-object v1

    sget-object v2, Lcom/mstar/android/tvapi/common/vo/EnumProgramLoopType;->E_PROG_LOOP_ALL:Lcom/mstar/android/tvapi/common/vo/EnumProgramLoopType;

    invoke-virtual {v1, v2}, Lcom/mstar/android/tvapi/common/ChannelManager;->programUp(Lcom/mstar/android/tvapi/common/vo/EnumProgramLoopType;)V
    :try_end_0
    .catch Lcom/mstar/android/tvapi/common/exception/TvCommonException; {:try_start_0 .. :try_end_0} :catch_0

    :cond_0
    :goto_0
    const/4 v1, 0x1

    return v1

    :catch_0
    move-exception v0

    invoke-virtual {v0}, Lcom/mstar/android/tvapi/common/exception/TvCommonException;->printStackTrace()V

    goto :goto_0
.end method

.method public registerOnAtvPlayerEventListener(I)Z
    .locals 1
    .param p1    # I
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Landroid/os/RemoteException;
        }
    .end annotation

    const/4 v0, 0x0

    return v0
.end method

.method public registerOnDtvPlayerEventListener(I)Z
    .locals 1
    .param p1    # I
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Landroid/os/RemoteException;
        }
    .end annotation

    const/4 v0, 0x0

    return v0
.end method

.method public registerOnTvPlayerEventListener(I)Z
    .locals 1
    .param p1    # I
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Landroid/os/RemoteException;
        }
    .end annotation

    const/4 v0, 0x0

    return v0
.end method

.method public resumeAtvAutoTuning()Z
    .locals 4
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Landroid/os/RemoteException;
        }
    .end annotation

    const-string v2, "TvChannelClient"

    const-string v3, "resumeAtvAutoTuning"

    invoke-static {v2, v3}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    const/4 v1, 0x0

    :try_start_0
    invoke-static {}, Lcom/mstar/android/tvapi/atv/AtvManager;->getAtvScanManager()Lcom/mstar/android/tvapi/atv/AtvScanManager;

    move-result-object v2

    if-eqz v2, :cond_0

    invoke-static {}, Lcom/mstar/android/tvapi/atv/AtvManager;->getAtvScanManager()Lcom/mstar/android/tvapi/atv/AtvScanManager;

    move-result-object v2

    invoke-interface {v2}, Lcom/mstar/android/tvapi/atv/AtvScanManager;->setAutoTuningResume()Z

    move-result v1

    :cond_0
    sget-object v2, Lcom/android/server/tv/TvChannelClient$TV_TS_STATUS;->E_TS_ATV_AUTO_TUNING:Lcom/android/server/tv/TvChannelClient$TV_TS_STATUS;

    iput-object v2, p0, Lcom/android/server/tv/TvChannelClient;->tv_tuning_status:Lcom/android/server/tv/TvChannelClient$TV_TS_STATUS;
    :try_end_0
    .catch Lcom/mstar/android/tvapi/common/exception/TvCommonException; {:try_start_0 .. :try_end_0} :catch_0

    :goto_0
    return v1

    :catch_0
    move-exception v0

    invoke-virtual {v0}, Lcom/mstar/android/tvapi/common/exception/TvCommonException;->printStackTrace()V

    goto :goto_0
.end method

.method public resumeDtvScan()Z
    .locals 3
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Landroid/os/RemoteException;
        }
    .end annotation

    const-string v1, "TvChannelClient"

    const-string v2, "resumeDtvScan"

    invoke-static {v1, v2}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    :try_start_0
    invoke-static {}, Lcom/mstar/android/tvapi/dtv/common/DtvManager;->getDvbcScanManager()Lcom/mstar/android/tvapi/dtv/dvb/dvbc/DvbcScanManager;

    move-result-object v1

    if-eqz v1, :cond_0

    invoke-static {}, Lcom/mstar/android/tvapi/dtv/common/DtvManager;->getDvbcScanManager()Lcom/mstar/android/tvapi/dtv/dvb/dvbc/DvbcScanManager;

    move-result-object v1

    invoke-interface {v1}, Lcom/mstar/android/tvapi/dtv/dvb/dvbc/DvbcScanManager;->resumeScan()Z

    :cond_0
    sget-object v1, Lcom/android/server/tv/TvChannelClient$TV_TS_STATUS;->E_TS_DTV_AUTO_TUNING:Lcom/android/server/tv/TvChannelClient$TV_TS_STATUS;

    iput-object v1, p0, Lcom/android/server/tv/TvChannelClient;->tv_tuning_status:Lcom/android/server/tv/TvChannelClient$TV_TS_STATUS;
    :try_end_0
    .catch Lcom/mstar/android/tvapi/common/exception/TvCommonException; {:try_start_0 .. :try_end_0} :catch_0

    :goto_0
    const/4 v1, 0x0

    return v1

    :catch_0
    move-exception v0

    invoke-virtual {v0}, Lcom/mstar/android/tvapi/common/exception/TvCommonException;->printStackTrace()V

    goto :goto_0
.end method

.method public setAtvChannel(I)I
    .locals 3
    .param p1    # I
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Landroid/os/RemoteException;
        }
    .end annotation

    const-string v0, "TvChannelClient"

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "setAtvChannel, paras ChannelNumber is "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    sget-object v0, Lcom/android/server/tv/TvChannelClient$MEMBER_SERVICETYPE;->E_SERVICETYPE_ATV:Lcom/android/server/tv/TvChannelClient$MEMBER_SERVICETYPE;

    invoke-virtual {v0}, Lcom/android/server/tv/TvChannelClient$MEMBER_SERVICETYPE;->ordinal()I

    move-result v0

    invoke-direct {p0, p1, v0}, Lcom/android/server/tv/TvChannelClient;->programSel(II)Z

    const/4 v0, 0x0

    return v0
.end method

.method public setAtvForceSoundSystem(I)Z
    .locals 4
    .param p1    # I
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Landroid/os/RemoteException;
        }
    .end annotation

    const-string v1, "TvChannelClient"

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "setAtvForceSoundSystem , paras eSoundSystem is "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, p1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    :try_start_0
    invoke-static {}, Lcom/mstar/android/tvapi/common/TvManager;->getInstance()Lcom/mstar/android/tvapi/common/TvManager;

    move-result-object v1

    if-eqz v1, :cond_0

    invoke-static {}, Lcom/mstar/android/tvapi/common/TvManager;->getInstance()Lcom/mstar/android/tvapi/common/TvManager;

    move-result-object v1

    invoke-virtual {v1}, Lcom/mstar/android/tvapi/common/TvManager;->getAudioManager()Lcom/mstar/android/tvapi/common/AudioManager;

    move-result-object v1

    invoke-static {}, Lcom/mstar/android/tvapi/common/vo/AtvSystemStandard$EnumAtvSystemStandard;->values()[Lcom/mstar/android/tvapi/common/vo/AtvSystemStandard$EnumAtvSystemStandard;

    move-result-object v2

    invoke-static {p1}, Lcom/mstar/android/tvapi/common/vo/AtvSystemStandard$EnumAtvSystemStandard;->getOrdinalThroughValue(I)I

    move-result v3

    aget-object v2, v2, v3

    invoke-virtual {v1, v2}, Lcom/mstar/android/tvapi/common/AudioManager;->setAtvSoundSystem(Lcom/mstar/android/tvapi/common/vo/AtvSystemStandard$EnumAtvSystemStandard;)Z
    :try_end_0
    .catch Lcom/mstar/android/tvapi/common/exception/TvCommonException; {:try_start_0 .. :try_end_0} :catch_0

    :cond_0
    :goto_0
    const/4 v1, 0x1

    return v1

    :catch_0
    move-exception v0

    invoke-virtual {v0}, Lcom/mstar/android/tvapi/common/exception/TvCommonException;->printStackTrace()V

    goto :goto_0
.end method

.method public setAtvForceVedioSystem(I)Z
    .locals 7
    .param p1    # I
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Landroid/os/RemoteException;
        }
    .end annotation

    const/4 v3, 0x0

    const-string v4, "TvChannelClient"

    new-instance v5, Ljava/lang/StringBuilder;

    invoke-direct {v5}, Ljava/lang/StringBuilder;-><init>()V

    const-string v6, "setAtvForceVedioSystem , paras eVideoSystem is "

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5, p1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    invoke-static {v4, v5}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    invoke-virtual {p0}, Lcom/android/server/tv/TvChannelClient;->getCurrentChannelNumber()I

    move-result v0

    move v2, p1

    :try_start_0
    invoke-static {}, Lcom/mstar/android/tvapi/atv/AtvManager;->getAtvPlayerManager()Lcom/mstar/android/tvapi/atv/AtvPlayer;

    move-result-object v4

    if-eqz v4, :cond_0

    invoke-static {}, Lcom/mstar/android/tvapi/atv/AtvManager;->getAtvPlayerManager()Lcom/mstar/android/tvapi/atv/AtvPlayer;

    move-result-object v4

    invoke-static {}, Lcom/mstar/android/tvapi/common/vo/EnumAvdVideoStandardType;->values()[Lcom/mstar/android/tvapi/common/vo/EnumAvdVideoStandardType;

    move-result-object v5

    aget-object v5, v5, p1

    invoke-interface {v4, v5}, Lcom/mstar/android/tvapi/atv/AtvPlayer;->forceVideoStandard(Lcom/mstar/android/tvapi/common/vo/EnumAvdVideoStandardType;)V

    :cond_0
    invoke-static {}, Lcom/mstar/android/tvapi/atv/AtvManager;->getAtvScanManager()Lcom/mstar/android/tvapi/atv/AtvScanManager;

    move-result-object v4

    if-eqz v4, :cond_1

    invoke-static {}, Lcom/mstar/android/tvapi/atv/AtvManager;->getAtvScanManager()Lcom/mstar/android/tvapi/atv/AtvScanManager;

    move-result-object v4

    sget-object v5, Lcom/mstar/android/tvapi/atv/vo/EnumSetProgramInfo;->E_SET_VIDEO_STANDARD_OF_PROGRAM:Lcom/mstar/android/tvapi/atv/vo/EnumSetProgramInfo;

    invoke-interface {v4, v5, v0, v2}, Lcom/mstar/android/tvapi/atv/AtvScanManager;->setAtvProgramInfo(Lcom/mstar/android/tvapi/atv/vo/EnumSetProgramInfo;II)Z
    :try_end_0
    .catch Lcom/mstar/android/tvapi/common/exception/TvCommonException; {:try_start_0 .. :try_end_0} :catch_0

    move-result v3

    :cond_1
    :goto_0
    return v3

    :catch_0
    move-exception v1

    invoke-virtual {v1}, Lcom/mstar/android/tvapi/common/exception/TvCommonException;->printStackTrace()V

    goto :goto_0
.end method

.method public setAtvProgramInfo(III)I
    .locals 5
    .param p1    # I
    .param p2    # I
    .param p3    # I
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Landroid/os/RemoteException;
        }
    .end annotation

    const-string v2, "TvChannelClient"

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string v4, "setAtvProgramInfo, paras Cmd is "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3, p1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v3

    const-string v4, " Program is "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3, p2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v3

    const-string v4, " Param2 is "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3, p3}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v2, v3}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    const/4 v1, 0x0

    :try_start_0
    invoke-static {}, Lcom/mstar/android/tvapi/atv/AtvManager;->getAtvScanManager()Lcom/mstar/android/tvapi/atv/AtvScanManager;

    move-result-object v2

    if-eqz v2, :cond_0

    invoke-static {}, Lcom/mstar/android/tvapi/atv/AtvManager;->getAtvScanManager()Lcom/mstar/android/tvapi/atv/AtvScanManager;

    move-result-object v2

    invoke-static {}, Lcom/mstar/android/tvapi/atv/vo/EnumGetProgramInfo;->values()[Lcom/mstar/android/tvapi/atv/vo/EnumGetProgramInfo;

    move-result-object v3

    aget-object v3, v3, p1

    invoke-interface {v2, v3, p2}, Lcom/mstar/android/tvapi/atv/AtvScanManager;->getAtvProgramInfo(Lcom/mstar/android/tvapi/atv/vo/EnumGetProgramInfo;I)I
    :try_end_0
    .catch Lcom/mstar/android/tvapi/common/exception/TvCommonException; {:try_start_0 .. :try_end_0} :catch_0

    move-result v1

    :cond_0
    :goto_0
    return v1

    :catch_0
    move-exception v0

    invoke-virtual {v0}, Lcom/mstar/android/tvapi/common/exception/TvCommonException;->printStackTrace()V

    goto :goto_0
.end method

.method public setChannelChangeFreezeMode(Z)V
    .locals 4
    .param p1    # Z
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Landroid/os/RemoteException;
        }
    .end annotation

    const-string v1, "TvChannelClient"

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "setChannelChangeFreezeMode, paras freezeMode is "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, p1}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    :try_start_0
    invoke-static {}, Lcom/mstar/android/tvapi/atv/AtvManager;->getAtvPlayerManager()Lcom/mstar/android/tvapi/atv/AtvPlayer;

    move-result-object v1

    if-eqz v1, :cond_0

    invoke-static {}, Lcom/mstar/android/tvapi/atv/AtvManager;->getAtvPlayerManager()Lcom/mstar/android/tvapi/atv/AtvPlayer;

    move-result-object v1

    invoke-interface {v1, p1}, Lcom/mstar/android/tvapi/atv/AtvPlayer;->setChannelChangeFreezeMode(Z)V
    :try_end_0
    .catch Lcom/mstar/android/tvapi/common/exception/TvCommonException; {:try_start_0 .. :try_end_0} :catch_0

    :cond_0
    :goto_0
    return-void

    :catch_0
    move-exception v0

    invoke-virtual {v0}, Lcom/mstar/android/tvapi/common/exception/TvCommonException;->printStackTrace()V

    goto :goto_0
.end method

.method public setChannelSwitchMode(I)Z
    .locals 4
    .param p1    # I
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Landroid/os/RemoteException;
        }
    .end annotation

    const/4 v3, 0x1

    iget-object v1, p0, Lcom/android/server/tv/TvChannelClient;->db:Lcom/android/server/tv/DataBaseDesk;

    invoke-static {}, Lcom/android/server/tv/DataBaseDesk$EN_MS_CHANNEL_SWITCH_MODE;->values()[Lcom/android/server/tv/DataBaseDesk$EN_MS_CHANNEL_SWITCH_MODE;

    move-result-object v2

    aget-object v2, v2, p1

    invoke-interface {v1, v2}, Lcom/android/server/tv/DataBaseDesk;->updateChSwMode(Lcom/android/server/tv/DataBaseDesk$EN_MS_CHANNEL_SWITCH_MODE;)V

    sget-object v1, Lcom/android/server/tv/DataBaseDesk$EN_MS_CHANNEL_SWITCH_MODE;->MS_CHANNEL_SWM_BLACKSCREEN:Lcom/android/server/tv/DataBaseDesk$EN_MS_CHANNEL_SWITCH_MODE;

    invoke-virtual {v1}, Lcom/android/server/tv/DataBaseDesk$EN_MS_CHANNEL_SWITCH_MODE;->ordinal()I

    move-result v1

    if-ne p1, v1, :cond_1

    :try_start_0
    invoke-static {}, Lcom/mstar/android/tvapi/atv/AtvManager;->getAtvPlayerManager()Lcom/mstar/android/tvapi/atv/AtvPlayer;

    move-result-object v1

    if-eqz v1, :cond_0

    invoke-static {}, Lcom/mstar/android/tvapi/atv/AtvManager;->getAtvPlayerManager()Lcom/mstar/android/tvapi/atv/AtvPlayer;

    move-result-object v1

    const/4 v2, 0x0

    invoke-interface {v1, v2}, Lcom/mstar/android/tvapi/atv/AtvPlayer;->setChannelChangeFreezeMode(Z)V
    :try_end_0
    .catch Lcom/mstar/android/tvapi/common/exception/TvCommonException; {:try_start_0 .. :try_end_0} :catch_0

    :cond_0
    :goto_0
    return v3

    :catch_0
    move-exception v0

    invoke-virtual {v0}, Lcom/mstar/android/tvapi/common/exception/TvCommonException;->printStackTrace()V

    goto :goto_0

    :cond_1
    :try_start_1
    invoke-static {}, Lcom/mstar/android/tvapi/atv/AtvManager;->getAtvPlayerManager()Lcom/mstar/android/tvapi/atv/AtvPlayer;

    move-result-object v1

    if-eqz v1, :cond_0

    invoke-static {}, Lcom/mstar/android/tvapi/atv/AtvManager;->getAtvPlayerManager()Lcom/mstar/android/tvapi/atv/AtvPlayer;

    move-result-object v1

    const/4 v2, 0x1

    invoke-interface {v1, v2}, Lcom/mstar/android/tvapi/atv/AtvPlayer;->setChannelChangeFreezeMode(Z)V
    :try_end_1
    .catch Lcom/mstar/android/tvapi/common/exception/TvCommonException; {:try_start_1 .. :try_end_1} :catch_1

    goto :goto_0

    :catch_1
    move-exception v0

    invoke-virtual {v0}, Lcom/mstar/android/tvapi/common/exception/TvCommonException;->printStackTrace()V

    goto :goto_0
.end method

.method public setDtvAntennaType(I)V
    .locals 3
    .param p1    # I
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Landroid/os/RemoteException;
        }
    .end annotation

    const-string v0, "TvChannelClient"

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "setDtvAntennaType , paras type is "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    int-to-short v0, p1

    invoke-virtual {p0, v0}, Lcom/android/server/tv/TvChannelClient;->switchMSrvDtvRouteCmd(I)Z

    iget-object v0, p0, Lcom/android/server/tv/TvChannelClient;->db:Lcom/android/server/tv/DataBaseDesk;

    invoke-interface {v0, p1}, Lcom/android/server/tv/DataBaseDesk;->updateAntennaType(I)V

    return-void
.end method

.method public setDtvManualScanByFreq(I)Z
    .locals 4
    .param p1    # I
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Landroid/os/RemoteException;
        }
    .end annotation

    invoke-direct {p0}, Lcom/android/server/tv/TvChannelClient;->makeSourceDtv()V

    const-string v1, "TvChannelClient"

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "setDtvManualScanByFreq , paras FrequencyKHz is "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, p1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    :try_start_0
    invoke-static {}, Lcom/mstar/android/tvapi/dtv/common/DtvManager;->getDvbPlayerManager()Lcom/mstar/android/tvapi/dtv/dvb/DvbPlayer;

    move-result-object v1

    if-eqz v1, :cond_0

    invoke-static {}, Lcom/mstar/android/tvapi/dtv/common/DtvManager;->getDvbPlayerManager()Lcom/mstar/android/tvapi/dtv/dvb/DvbPlayer;

    move-result-object v1

    invoke-interface {v1, p1}, Lcom/mstar/android/tvapi/dtv/dvb/DvbPlayer;->setManualTuneByFreq(I)Z
    :try_end_0
    .catch Lcom/mstar/android/tvapi/common/exception/TvCommonException; {:try_start_0 .. :try_end_0} :catch_0

    :cond_0
    :goto_0
    const/4 v1, 0x1

    return v1

    :catch_0
    move-exception v0

    invoke-virtual {v0}, Lcom/mstar/android/tvapi/common/exception/TvCommonException;->printStackTrace()V

    goto :goto_0
.end method

.method public setDtvManualScanByRF(I)Z
    .locals 4
    .param p1    # I
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Landroid/os/RemoteException;
        }
    .end annotation

    invoke-direct {p0}, Lcom/android/server/tv/TvChannelClient;->makeSourceDtv()V

    const-string v1, "TvChannelClient"

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "setDtvManualScanByRF, paras RFNum is "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, p1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    :try_start_0
    invoke-static {}, Lcom/mstar/android/tvapi/dtv/common/DtvManager;->getDvbPlayerManager()Lcom/mstar/android/tvapi/dtv/dvb/DvbPlayer;

    move-result-object v1

    if-eqz v1, :cond_0

    invoke-static {}, Lcom/mstar/android/tvapi/dtv/common/DtvManager;->getDvbPlayerManager()Lcom/mstar/android/tvapi/dtv/dvb/DvbPlayer;

    move-result-object v1

    int-to-short v2, p1

    invoke-interface {v1, v2}, Lcom/mstar/android/tvapi/dtv/dvb/DvbPlayer;->setManualTuneByRf(S)Z
    :try_end_0
    .catch Lcom/mstar/android/tvapi/common/exception/TvCommonException; {:try_start_0 .. :try_end_0} :catch_0

    :cond_0
    :goto_0
    const/4 v1, 0x1

    return v1

    :catch_0
    move-exception v0

    invoke-virtual {v0}, Lcom/mstar/android/tvapi/common/exception/TvCommonException;->printStackTrace()V

    goto :goto_0
.end method

.method public setProgramAttribute(IIII)V
    .locals 7
    .param p1    # I
    .param p2    # I
    .param p3    # I
    .param p4    # I
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Landroid/os/RemoteException;
        }
    .end annotation

    const-string v0, "TvChannelClient"

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "setProgramAttribute, paras enpa is "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, " programNo is "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, " programType is "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p3}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, " programId is "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p4}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    const/4 v5, 0x1

    :try_start_0
    invoke-static {}, Lcom/mstar/android/tvapi/common/TvManager;->getInstance()Lcom/mstar/android/tvapi/common/TvManager;

    move-result-object v0

    if-eqz v0, :cond_0

    invoke-static {}, Lcom/mstar/android/tvapi/common/TvManager;->getInstance()Lcom/mstar/android/tvapi/common/TvManager;

    move-result-object v0

    invoke-virtual {v0}, Lcom/mstar/android/tvapi/common/TvManager;->getChannelManager()Lcom/mstar/android/tvapi/common/ChannelManager;

    move-result-object v0

    invoke-static {}, Lcom/mstar/android/tvapi/common/vo/EnumProgramAttribute;->values()[Lcom/mstar/android/tvapi/common/vo/EnumProgramAttribute;

    move-result-object v1

    aget-object v1, v1, p1

    int-to-short v3, p3

    move v2, p2

    move v4, p4

    invoke-virtual/range {v0 .. v5}, Lcom/mstar/android/tvapi/common/ChannelManager;->setProgramAttribute(Lcom/mstar/android/tvapi/common/vo/EnumProgramAttribute;ISIZ)V
    :try_end_0
    .catch Lcom/mstar/android/tvapi/common/exception/TvCommonException; {:try_start_0 .. :try_end_0} :catch_0

    :cond_0
    :goto_0
    return-void

    :catch_0
    move-exception v6

    invoke-virtual {v6}, Lcom/mstar/android/tvapi/common/exception/TvCommonException;->printStackTrace()V

    goto :goto_0
.end method

.method public setProgramCtrl(III)I
    .locals 4
    .param p1    # I
    .param p2    # I
    .param p3    # I
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Landroid/os/RemoteException;
        }
    .end annotation

    const-string v1, "TvChannelClient"

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "setProgramCtrl, paras Cmd is "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, p1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, " u16Param2 is "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, p2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, " u16Param3 is "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, p3}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    :try_start_0
    invoke-static {}, Lcom/mstar/android/tvapi/atv/AtvManager;->getAtvScanManager()Lcom/mstar/android/tvapi/atv/AtvScanManager;

    move-result-object v1

    if-eqz v1, :cond_0

    invoke-static {}, Lcom/mstar/android/tvapi/atv/AtvManager;->getAtvScanManager()Lcom/mstar/android/tvapi/atv/AtvScanManager;

    move-result-object v1

    invoke-static {}, Lcom/mstar/android/tvapi/atv/vo/EnumSetProgramCtrl;->values()[Lcom/mstar/android/tvapi/atv/vo/EnumSetProgramCtrl;

    move-result-object v2

    aget-object v2, v2, p1

    invoke-interface {v1, v2, p2, p3}, Lcom/mstar/android/tvapi/atv/AtvScanManager;->setProgramControl(Lcom/mstar/android/tvapi/atv/vo/EnumSetProgramCtrl;II)Z
    :try_end_0
    .catch Lcom/mstar/android/tvapi/common/exception/TvCommonException; {:try_start_0 .. :try_end_0} :catch_0

    :cond_0
    :goto_0
    const/4 v1, 0x0

    return v1

    :catch_0
    move-exception v0

    invoke-virtual {v0}, Lcom/mstar/android/tvapi/common/exception/TvCommonException;->printStackTrace()V

    goto :goto_0
.end method

.method public setSystemCountry(I)V
    .locals 5
    .param p1    # I
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Landroid/os/RemoteException;
        }
    .end annotation

    const-string v2, "TvChannelClient"

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string v4, "setSystemCountry , paras memberCountry is "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3, p1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v2, v3}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    :try_start_0
    sget-object v1, Lcom/mstar/android/tvapi/common/vo/TvOsType$EnumCountry;->E_UK:Lcom/mstar/android/tvapi/common/vo/TvOsType$EnumCountry;

    sget-object v2, Lcom/android/server/tv/TvChannelClient$1;->$SwitchMap$com$mstar$android$tvapi$common$vo$TvOsType$EnumCountry:[I

    invoke-static {}, Lcom/mstar/android/tvapi/common/vo/TvOsType$EnumCountry;->values()[Lcom/mstar/android/tvapi/common/vo/TvOsType$EnumCountry;

    move-result-object v3

    aget-object v3, v3, p1

    invoke-virtual {v3}, Lcom/mstar/android/tvapi/common/vo/TvOsType$EnumCountry;->ordinal()I

    move-result v3

    aget v2, v2, v3

    packed-switch v2, :pswitch_data_0

    sget-object v1, Lcom/mstar/android/tvapi/common/vo/TvOsType$EnumCountry;->E_UK:Lcom/mstar/android/tvapi/common/vo/TvOsType$EnumCountry;

    :goto_0
    invoke-static {}, Lcom/mstar/android/tvapi/dtv/common/DtvManager;->getDvbPlayerManager()Lcom/mstar/android/tvapi/dtv/dvb/DvbPlayer;

    move-result-object v2

    if-eqz v2, :cond_0

    invoke-static {}, Lcom/mstar/android/tvapi/dtv/common/DtvManager;->getDvbPlayerManager()Lcom/mstar/android/tvapi/dtv/dvb/DvbPlayer;

    move-result-object v2

    invoke-static {}, Lcom/mstar/android/tvapi/common/vo/TvOsType$EnumCountry;->values()[Lcom/mstar/android/tvapi/common/vo/TvOsType$EnumCountry;

    move-result-object v3

    invoke-virtual {v1}, Lcom/mstar/android/tvapi/common/vo/TvOsType$EnumCountry;->ordinal()I

    move-result v4

    aget-object v3, v3, v4

    invoke-interface {v2, v3}, Lcom/mstar/android/tvapi/dtv/dvb/DvbPlayer;->setCountry(Lcom/mstar/android/tvapi/common/vo/TvOsType$EnumCountry;)Z

    :cond_0
    :goto_1
    return-void

    :pswitch_0
    invoke-static {}, Lcom/mstar/android/tvapi/common/vo/TvOsType$EnumCountry;->values()[Lcom/mstar/android/tvapi/common/vo/TvOsType$EnumCountry;

    move-result-object v2

    aget-object v1, v2, p1
    :try_end_0
    .catch Lcom/mstar/android/tvapi/common/exception/TvCommonException; {:try_start_0 .. :try_end_0} :catch_0

    goto :goto_0

    :catch_0
    move-exception v0

    invoke-virtual {v0}, Lcom/mstar/android/tvapi/common/exception/TvCommonException;->printStackTrace()V

    goto :goto_1

    nop

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
        :pswitch_0
    .end packed-switch
.end method

.method public startAtvAutoTuning(III)Z
    .locals 5
    .param p1    # I
    .param p2    # I
    .param p3    # I
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Landroid/os/RemoteException;
        }
    .end annotation

    invoke-direct {p0}, Lcom/android/server/tv/TvChannelClient;->makeSourceAtv()V

    const-string v2, "TvChannelClient"

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string v4, "startAtvAutoTuning, paras EventIntervalMs is "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3, p1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v3

    const-string v4, " FrequencyStart is "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3, p2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v3

    const-string v4, " FrequencyEnd is "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3, p3}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v2, v3}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    const/4 v1, 0x0

    invoke-direct {p0}, Lcom/android/server/tv/TvChannelClient;->getCurrentInputSource()I

    move-result v2

    sget-object v3, Lcom/mstar/android/tvapi/common/vo/TvOsType$EnumInputSource;->E_INPUT_SOURCE_ATV:Lcom/mstar/android/tvapi/common/vo/TvOsType$EnumInputSource;

    invoke-virtual {v3}, Lcom/mstar/android/tvapi/common/vo/TvOsType$EnumInputSource;->ordinal()I

    move-result v3

    if-eq v2, v3, :cond_0

    sget-object v2, Lcom/mstar/android/tvapi/common/vo/TvOsType$EnumInputSource;->E_INPUT_SOURCE_ATV:Lcom/mstar/android/tvapi/common/vo/TvOsType$EnumInputSource;

    invoke-direct {p0, v2}, Lcom/android/server/tv/TvChannelClient;->setInputSource(Lcom/mstar/android/tvapi/common/vo/TvOsType$EnumInputSource;)V

    const-wide/16 v2, 0x1f4

    :try_start_0
    invoke-static {v2, v3}, Ljava/lang/Thread;->sleep(J)V
    :try_end_0
    .catch Ljava/lang/InterruptedException; {:try_start_0 .. :try_end_0} :catch_0

    :cond_0
    :goto_0
    :try_start_1
    invoke-static {}, Lcom/mstar/android/tvapi/atv/AtvManager;->getAtvScanManager()Lcom/mstar/android/tvapi/atv/AtvScanManager;

    move-result-object v2

    if-eqz v2, :cond_1

    invoke-static {}, Lcom/mstar/android/tvapi/atv/AtvManager;->getAtvScanManager()Lcom/mstar/android/tvapi/atv/AtvScanManager;

    move-result-object v2

    sget-object v3, Lcom/mstar/android/tvapi/atv/vo/EnumAutoScanState;->E_NONE_NTSC_AUTO_SCAN:Lcom/mstar/android/tvapi/atv/vo/EnumAutoScanState;

    invoke-interface {v2, p1, p2, p3, v3}, Lcom/mstar/android/tvapi/atv/AtvScanManager;->setAutoTuningStart(IIILcom/mstar/android/tvapi/atv/vo/EnumAutoScanState;)Z

    move-result v1

    :cond_1
    sget-object v2, Lcom/android/server/tv/TvChannelClient$TV_TS_STATUS;->E_TS_ATV_AUTO_TUNING:Lcom/android/server/tv/TvChannelClient$TV_TS_STATUS;

    iput-object v2, p0, Lcom/android/server/tv/TvChannelClient;->tv_tuning_status:Lcom/android/server/tv/TvChannelClient$TV_TS_STATUS;
    :try_end_1
    .catch Lcom/mstar/android/tvapi/common/exception/TvCommonException; {:try_start_1 .. :try_end_1} :catch_1

    :goto_1
    return v1

    :catch_0
    move-exception v0

    invoke-virtual {v0}, Ljava/lang/InterruptedException;->printStackTrace()V

    goto :goto_0

    :catch_1
    move-exception v0

    invoke-virtual {v0}, Lcom/mstar/android/tvapi/common/exception/TvCommonException;->printStackTrace()V

    goto :goto_1
.end method

.method public startAtvManualTuning(III)Z
    .locals 5
    .param p1    # I
    .param p2    # I
    .param p3    # I
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Landroid/os/RemoteException;
        }
    .end annotation

    invoke-direct {p0}, Lcom/android/server/tv/TvChannelClient;->makeSourceAtv()V

    const-string v2, "TvChannelClient"

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string v4, "startAtvManualTuning, EventIntervalMs is "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3, p1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v3

    const-string v4, " Frequency is "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3, p2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v3

    const-string v4, " eMode is "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3, p3}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v2, v3}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    invoke-direct {p0}, Lcom/android/server/tv/TvChannelClient;->getCurrentInputSource()I

    move-result v2

    sget-object v3, Lcom/mstar/android/tvapi/common/vo/TvOsType$EnumInputSource;->E_INPUT_SOURCE_ATV:Lcom/mstar/android/tvapi/common/vo/TvOsType$EnumInputSource;

    invoke-virtual {v3}, Lcom/mstar/android/tvapi/common/vo/TvOsType$EnumInputSource;->ordinal()I

    move-result v3

    if-eq v2, v3, :cond_0

    sget-object v2, Lcom/mstar/android/tvapi/common/vo/TvOsType$EnumInputSource;->E_INPUT_SOURCE_ATV:Lcom/mstar/android/tvapi/common/vo/TvOsType$EnumInputSource;

    invoke-direct {p0, v2}, Lcom/android/server/tv/TvChannelClient;->setInputSource(Lcom/mstar/android/tvapi/common/vo/TvOsType$EnumInputSource;)V

    const-wide/16 v2, 0x3e8

    :try_start_0
    invoke-static {v2, v3}, Ljava/lang/Thread;->sleep(J)V

    sget-object v2, Lcom/mstar/android/tvapi/common/vo/EnumFirstServiceInputType;->E_FIRST_SERVICE_ATV:Lcom/mstar/android/tvapi/common/vo/EnumFirstServiceInputType;

    invoke-virtual {v2}, Lcom/mstar/android/tvapi/common/vo/EnumFirstServiceInputType;->ordinal()I

    move-result v2

    sget-object v3, Lcom/mstar/android/tvapi/common/vo/EnumFirstServiceType;->E_DEFAULT:Lcom/mstar/android/tvapi/common/vo/EnumFirstServiceType;

    invoke-virtual {v3}, Lcom/mstar/android/tvapi/common/vo/EnumFirstServiceType;->ordinal()I

    move-result v3

    invoke-virtual {p0, v2, v3}, Lcom/android/server/tv/TvChannelClient;->changeToFirstService(II)Z
    :try_end_0
    .catch Ljava/lang/InterruptedException; {:try_start_0 .. :try_end_0} :catch_0

    :cond_0
    :goto_0
    const/4 v1, 0x0

    :try_start_1
    invoke-static {}, Lcom/mstar/android/tvapi/atv/AtvManager;->getAtvScanManager()Lcom/mstar/android/tvapi/atv/AtvScanManager;

    move-result-object v2

    if-eqz v2, :cond_1

    invoke-static {}, Lcom/mstar/android/tvapi/atv/AtvManager;->getAtvScanManager()Lcom/mstar/android/tvapi/atv/AtvScanManager;

    move-result-object v2

    invoke-static {}, Lcom/mstar/android/tvapi/atv/vo/EnumAtvManualTuneMode;->values()[Lcom/mstar/android/tvapi/atv/vo/EnumAtvManualTuneMode;

    move-result-object v3

    aget-object v3, v3, p3

    invoke-interface {v2, p1, p2, v3}, Lcom/mstar/android/tvapi/atv/AtvScanManager;->setManualTuningStart(IILcom/mstar/android/tvapi/atv/vo/EnumAtvManualTuneMode;)Z

    move-result v1

    :cond_1
    sget-object v2, Lcom/mstar/android/tvapi/atv/vo/EnumAtvManualTuneMode;->E_MANUAL_TUNE_MODE_SEARCH_ONE_TO_UP:Lcom/mstar/android/tvapi/atv/vo/EnumAtvManualTuneMode;

    invoke-virtual {v2}, Lcom/mstar/android/tvapi/atv/vo/EnumAtvManualTuneMode;->ordinal()I

    move-result v2

    if-ne p3, v2, :cond_3

    sget-object v2, Lcom/android/server/tv/TvChannelClient$TV_TS_STATUS;->E_TS_ATV_MANU_TUNING_RIGHT:Lcom/android/server/tv/TvChannelClient$TV_TS_STATUS;

    iput-object v2, p0, Lcom/android/server/tv/TvChannelClient;->tv_tuning_status:Lcom/android/server/tv/TvChannelClient$TV_TS_STATUS;
    :try_end_1
    .catch Lcom/mstar/android/tvapi/common/exception/TvCommonException; {:try_start_1 .. :try_end_1} :catch_1

    :cond_2
    :goto_1
    return v1

    :catch_0
    move-exception v0

    invoke-virtual {v0}, Ljava/lang/InterruptedException;->printStackTrace()V

    goto :goto_0

    :cond_3
    :try_start_2
    sget-object v2, Lcom/mstar/android/tvapi/atv/vo/EnumAtvManualTuneMode;->E_MANUAL_TUNE_MODE_SEARCH_ONE_TO_DOWN:Lcom/mstar/android/tvapi/atv/vo/EnumAtvManualTuneMode;

    invoke-virtual {v2}, Lcom/mstar/android/tvapi/atv/vo/EnumAtvManualTuneMode;->ordinal()I

    move-result v2

    if-ne p3, v2, :cond_2

    sget-object v2, Lcom/android/server/tv/TvChannelClient$TV_TS_STATUS;->E_TS_ATV_MANU_TUNING_LEFT:Lcom/android/server/tv/TvChannelClient$TV_TS_STATUS;

    iput-object v2, p0, Lcom/android/server/tv/TvChannelClient;->tv_tuning_status:Lcom/android/server/tv/TvChannelClient$TV_TS_STATUS;
    :try_end_2
    .catch Lcom/mstar/android/tvapi/common/exception/TvCommonException; {:try_start_2 .. :try_end_2} :catch_1

    goto :goto_1

    :catch_1
    move-exception v0

    invoke-virtual {v0}, Lcom/mstar/android/tvapi/common/exception/TvCommonException;->printStackTrace()V

    goto :goto_1
.end method

.method public startDtvAutoScan()Z
    .locals 3
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Landroid/os/RemoteException;
        }
    .end annotation

    invoke-direct {p0}, Lcom/android/server/tv/TvChannelClient;->makeSourceDtv()V

    const-string v1, "TvChannelClient"

    const-string v2, "startDtvAutoScan"

    invoke-static {v1, v2}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    :try_start_0
    invoke-static {}, Lcom/mstar/android/tvapi/dtv/common/DtvManager;->getDvbcScanManager()Lcom/mstar/android/tvapi/dtv/dvb/dvbc/DvbcScanManager;

    move-result-object v1

    if-eqz v1, :cond_0

    invoke-static {}, Lcom/mstar/android/tvapi/dtv/common/DtvManager;->getDvbcScanManager()Lcom/mstar/android/tvapi/dtv/dvb/dvbc/DvbcScanManager;

    move-result-object v1

    invoke-interface {v1}, Lcom/mstar/android/tvapi/dtv/dvb/dvbc/DvbcScanManager;->startAutoScan()V

    :cond_0
    sget-object v1, Lcom/android/server/tv/TvChannelClient$TV_TS_STATUS;->E_TS_DTV_AUTO_TUNING:Lcom/android/server/tv/TvChannelClient$TV_TS_STATUS;

    iput-object v1, p0, Lcom/android/server/tv/TvChannelClient;->tv_tuning_status:Lcom/android/server/tv/TvChannelClient$TV_TS_STATUS;
    :try_end_0
    .catch Lcom/mstar/android/tvapi/common/exception/TvCommonException; {:try_start_0 .. :try_end_0} :catch_0

    :goto_0
    const/4 v1, 0x0

    return v1

    :catch_0
    move-exception v0

    invoke-virtual {v0}, Lcom/mstar/android/tvapi/common/exception/TvCommonException;->printStackTrace()V

    goto :goto_0
.end method

.method public startDtvFullScan()Z
    .locals 3
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Landroid/os/RemoteException;
        }
    .end annotation

    invoke-direct {p0}, Lcom/android/server/tv/TvChannelClient;->makeSourceDtv()V

    const-string v1, "TvChannelClient"

    const-string v2, "startDtvFullScan"

    invoke-static {v1, v2}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    :try_start_0
    invoke-static {}, Lcom/mstar/android/tvapi/dtv/common/DtvManager;->getDvbcScanManager()Lcom/mstar/android/tvapi/dtv/dvb/dvbc/DvbcScanManager;

    move-result-object v1

    if-eqz v1, :cond_0

    invoke-static {}, Lcom/mstar/android/tvapi/dtv/common/DtvManager;->getDvbcScanManager()Lcom/mstar/android/tvapi/dtv/dvb/dvbc/DvbcScanManager;

    move-result-object v1

    invoke-interface {v1}, Lcom/mstar/android/tvapi/dtv/dvb/dvbc/DvbcScanManager;->startFullScan()Z

    :cond_0
    sget-object v1, Lcom/android/server/tv/TvChannelClient$TV_TS_STATUS;->E_TS_DTV_AUTO_TUNING:Lcom/android/server/tv/TvChannelClient$TV_TS_STATUS;

    iput-object v1, p0, Lcom/android/server/tv/TvChannelClient;->tv_tuning_status:Lcom/android/server/tv/TvChannelClient$TV_TS_STATUS;
    :try_end_0
    .catch Lcom/mstar/android/tvapi/common/exception/TvCommonException; {:try_start_0 .. :try_end_0} :catch_0

    :goto_0
    const/4 v1, 0x0

    return v1

    :catch_0
    move-exception v0

    invoke-virtual {v0}, Lcom/mstar/android/tvapi/common/exception/TvCommonException;->printStackTrace()V

    goto :goto_0
.end method

.method public startDtvManualScan()Z
    .locals 3
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Landroid/os/RemoteException;
        }
    .end annotation

    invoke-direct {p0}, Lcom/android/server/tv/TvChannelClient;->makeSourceDtv()V

    const-string v1, "TvChannelClient"

    const-string v2, "startDtvManualScan"

    invoke-static {v1, v2}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    :try_start_0
    invoke-static {}, Lcom/mstar/android/tvapi/dtv/common/DtvManager;->getDvbcScanManager()Lcom/mstar/android/tvapi/dtv/dvb/dvbc/DvbcScanManager;

    move-result-object v1

    if-eqz v1, :cond_0

    invoke-static {}, Lcom/mstar/android/tvapi/dtv/common/DtvManager;->getDvbcScanManager()Lcom/mstar/android/tvapi/dtv/dvb/dvbc/DvbcScanManager;

    move-result-object v1

    invoke-interface {v1}, Lcom/mstar/android/tvapi/dtv/dvb/dvbc/DvbcScanManager;->startManualScan()Z

    :cond_0
    sget-object v1, Lcom/android/server/tv/TvChannelClient$TV_TS_STATUS;->E_TS_DTV_MANU_TUNING:Lcom/android/server/tv/TvChannelClient$TV_TS_STATUS;

    iput-object v1, p0, Lcom/android/server/tv/TvChannelClient;->tv_tuning_status:Lcom/android/server/tv/TvChannelClient$TV_TS_STATUS;
    :try_end_0
    .catch Lcom/mstar/android/tvapi/common/exception/TvCommonException; {:try_start_0 .. :try_end_0} :catch_0

    :goto_0
    const/4 v1, 0x0

    return v1

    :catch_0
    move-exception v0

    invoke-virtual {v0}, Lcom/mstar/android/tvapi/common/exception/TvCommonException;->printStackTrace()V

    goto :goto_0
.end method

.method public stopAtvAutoTuning()Z
    .locals 4
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Landroid/os/RemoteException;
        }
    .end annotation

    const-string v2, "TvChannelClient"

    const-string v3, "stopAtvAutoTuning"

    invoke-static {v2, v3}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    const/4 v1, 0x0

    :try_start_0
    invoke-static {}, Lcom/mstar/android/tvapi/atv/AtvManager;->getAtvScanManager()Lcom/mstar/android/tvapi/atv/AtvScanManager;

    move-result-object v2

    if-eqz v2, :cond_0

    invoke-static {}, Lcom/mstar/android/tvapi/atv/AtvManager;->getAtvScanManager()Lcom/mstar/android/tvapi/atv/AtvScanManager;

    move-result-object v2

    invoke-interface {v2}, Lcom/mstar/android/tvapi/atv/AtvScanManager;->setAutoTuningEnd()Z

    move-result v1

    :cond_0
    sget-object v2, Lcom/android/server/tv/TvChannelClient$TV_TS_STATUS;->E_TS_NONE:Lcom/android/server/tv/TvChannelClient$TV_TS_STATUS;

    iput-object v2, p0, Lcom/android/server/tv/TvChannelClient;->tv_tuning_status:Lcom/android/server/tv/TvChannelClient$TV_TS_STATUS;
    :try_end_0
    .catch Lcom/mstar/android/tvapi/common/exception/TvCommonException; {:try_start_0 .. :try_end_0} :catch_0

    :goto_0
    return v1

    :catch_0
    move-exception v0

    invoke-virtual {v0}, Lcom/mstar/android/tvapi/common/exception/TvCommonException;->printStackTrace()V

    goto :goto_0
.end method

.method public stopAtvManualTuning()V
    .locals 3
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Landroid/os/RemoteException;
        }
    .end annotation

    const-string v1, "TvChannelClient"

    const-string v2, "stopAtvManualTuning"

    invoke-static {v1, v2}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    sget-object v1, Lcom/android/server/tv/TvChannelClient$TV_TS_STATUS;->E_TS_NONE:Lcom/android/server/tv/TvChannelClient$TV_TS_STATUS;

    iput-object v1, p0, Lcom/android/server/tv/TvChannelClient;->tv_tuning_status:Lcom/android/server/tv/TvChannelClient$TV_TS_STATUS;

    :try_start_0
    invoke-static {}, Lcom/mstar/android/tvapi/atv/AtvManager;->getAtvScanManager()Lcom/mstar/android/tvapi/atv/AtvScanManager;

    move-result-object v1

    if-eqz v1, :cond_0

    invoke-static {}, Lcom/mstar/android/tvapi/atv/AtvManager;->getAtvScanManager()Lcom/mstar/android/tvapi/atv/AtvScanManager;

    move-result-object v1

    invoke-interface {v1}, Lcom/mstar/android/tvapi/atv/AtvScanManager;->setManualTuningEnd()V
    :try_end_0
    .catch Lcom/mstar/android/tvapi/common/exception/TvCommonException; {:try_start_0 .. :try_end_0} :catch_0

    :cond_0
    :goto_0
    return-void

    :catch_0
    move-exception v0

    invoke-virtual {v0}, Lcom/mstar/android/tvapi/common/exception/TvCommonException;->printStackTrace()V

    goto :goto_0
.end method

.method public stopDtvScan()Z
    .locals 3
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Landroid/os/RemoteException;
        }
    .end annotation

    const-string v1, "TvChannelClient"

    const-string v2, "stopDtvScan"

    invoke-static {v1, v2}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    :try_start_0
    invoke-static {}, Lcom/mstar/android/tvapi/dtv/common/DtvManager;->getDvbcScanManager()Lcom/mstar/android/tvapi/dtv/dvb/dvbc/DvbcScanManager;

    move-result-object v1

    if-eqz v1, :cond_0

    invoke-static {}, Lcom/mstar/android/tvapi/dtv/common/DtvManager;->getDvbcScanManager()Lcom/mstar/android/tvapi/dtv/dvb/dvbc/DvbcScanManager;

    move-result-object v1

    invoke-interface {v1}, Lcom/mstar/android/tvapi/dtv/dvb/dvbc/DvbcScanManager;->stopScan()Z

    :cond_0
    sget-object v1, Lcom/android/server/tv/TvChannelClient$TV_TS_STATUS;->E_TS_NONE:Lcom/android/server/tv/TvChannelClient$TV_TS_STATUS;

    iput-object v1, p0, Lcom/android/server/tv/TvChannelClient;->tv_tuning_status:Lcom/android/server/tv/TvChannelClient$TV_TS_STATUS;
    :try_end_0
    .catch Lcom/mstar/android/tvapi/common/exception/TvCommonException; {:try_start_0 .. :try_end_0} :catch_0

    :goto_0
    const/4 v1, 0x0

    return v1

    :catch_0
    move-exception v0

    invoke-virtual {v0}, Lcom/mstar/android/tvapi/common/exception/TvCommonException;->printStackTrace()V

    goto :goto_0
.end method

.method public switchAudioTrack(I)V
    .locals 4
    .param p1    # I
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Landroid/os/RemoteException;
        }
    .end annotation

    const-string v1, "TvChannelClient"

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "switchAudioTrack , paras track is "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, p1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    :try_start_0
    invoke-static {}, Lcom/mstar/android/tvapi/dtv/common/DtvManager;->getDvbPlayerManager()Lcom/mstar/android/tvapi/dtv/dvb/DvbPlayer;

    move-result-object v1

    if-eqz v1, :cond_0

    invoke-static {}, Lcom/mstar/android/tvapi/dtv/common/DtvManager;->getDvbPlayerManager()Lcom/mstar/android/tvapi/dtv/dvb/DvbPlayer;

    move-result-object v1

    invoke-interface {v1, p1}, Lcom/mstar/android/tvapi/dtv/dvb/DvbPlayer;->switchAudioTrack(I)V
    :try_end_0
    .catch Lcom/mstar/android/tvapi/common/exception/TvCommonException; {:try_start_0 .. :try_end_0} :catch_0

    :cond_0
    :goto_0
    return-void

    :catch_0
    move-exception v0

    invoke-virtual {v0}, Lcom/mstar/android/tvapi/common/exception/TvCommonException;->printStackTrace()V

    goto :goto_0
.end method

.method public switchMSrvDtvRouteCmd(I)Z
    .locals 4
    .param p1    # I
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Landroid/os/RemoteException;
        }
    .end annotation

    invoke-direct {p0}, Lcom/android/server/tv/TvChannelClient;->makeSourceDtv()V

    const-string v1, "TvChannelClient"

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "switchMSrvDtvRouteCmd, paras dtvRoute is "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, p1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    :try_start_0
    invoke-static {}, Lcom/mstar/android/tvapi/dtv/common/DtvManager;->getDvbPlayerManager()Lcom/mstar/android/tvapi/dtv/dvb/DvbPlayer;

    move-result-object v1

    if-eqz v1, :cond_0

    invoke-static {}, Lcom/mstar/android/tvapi/dtv/common/DtvManager;->getDvbPlayerManager()Lcom/mstar/android/tvapi/dtv/dvb/DvbPlayer;

    move-result-object v1

    int-to-short v2, p1

    invoke-interface {v1, v2}, Lcom/mstar/android/tvapi/dtv/dvb/DvbPlayer;->switchDtvRoute(S)Z
    :try_end_0
    .catch Lcom/mstar/android/tvapi/common/exception/TvCommonException; {:try_start_0 .. :try_end_0} :catch_0

    :cond_0
    :goto_0
    const/4 v1, 0x0

    return v1

    :catch_0
    move-exception v0

    invoke-virtual {v0}, Lcom/mstar/android/tvapi/common/exception/TvCommonException;->printStackTrace()V

    goto :goto_0
.end method
