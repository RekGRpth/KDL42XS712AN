.class public Lcom/android/server/tv/TvCommonClient;
.super Lcom/mstar/android/tv/ITvCommon$Stub;
.source "TvCommonClient.java"


# static fields
.field private static AUTOSLEEPCOMMOND:Ljava/lang/String; = null

.field private static REBOOTCOMMOND:Ljava/lang/String; = null

.field private static STANDBYCOMMOND:Ljava/lang/String; = null

.field private static final TAG:Ljava/lang/String; = "TvCommonClient"

.field private static bSrcDetResult:[Z

.field public static bThreadIsrunning:Z

.field private static isHPPortPlugged:Z

.field private static isPreviousSourceDTV:Z


# instance fields
.field private curSourceType:Lcom/mstar/android/tvapi/common/vo/TvOsType$EnumInputSource;

.field private databaseMgr:Lcom/android/server/tv/DataBaseDeskImpl;

.field private deskAtvPlayerLister:Lcom/android/server/tv/DeskAtvPlayerEventListener;

.field private deskCaLister:Lcom/android/server/tv/DeskCaEventListener;

.field private deskCiLister:Lcom/android/server/tv/DeskCiEventListener;

.field private deskDtvPlayerLister:Lcom/android/server/tv/DeskDtvPlayerEventListener;

.field private deskPictureLister:Lcom/android/server/tv/DeskPictureEventListener;

.field private deskPipLister:Lcom/android/server/tv/DeskPipEventListener;

.field private deskThreeDimensionLister:Lcom/android/server/tv/DeskThreeDimensionEventListener;

.field private deskTimerLister:Lcom/android/server/tv/DeskTimerEventListener;

.field private deskTvLister:Lcom/android/server/tv/DeskTvEventListener;

.field private deskTvPlayerLister:Lcom/android/server/tv/DeskTvPlayerEventListener;

.field private mContext:Landroid/content/Context;

.field private mHandler:Landroid/os/Handler;

.field psl:[I


# direct methods
.method static constructor <clinit>()V
    .locals 2

    const/4 v1, 0x0

    const-string v0, "reboot"

    sput-object v0, Lcom/android/server/tv/TvCommonClient;->REBOOTCOMMOND:Ljava/lang/String;

    const-string v0, "standby"

    sput-object v0, Lcom/android/server/tv/TvCommonClient;->STANDBYCOMMOND:Ljava/lang/String;

    const-string v0, "autosleep"

    sput-object v0, Lcom/android/server/tv/TvCommonClient;->AUTOSLEEPCOMMOND:Ljava/lang/String;

    const/4 v0, 0x0

    sput-object v0, Lcom/android/server/tv/TvCommonClient;->bSrcDetResult:[Z

    sput-boolean v1, Lcom/android/server/tv/TvCommonClient;->isPreviousSourceDTV:Z

    sput-boolean v1, Lcom/android/server/tv/TvCommonClient;->isHPPortPlugged:Z

    sput-boolean v1, Lcom/android/server/tv/TvCommonClient;->bThreadIsrunning:Z

    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Lcom/android/server/tv/TvHanlder;Lcom/android/server/tv/DataBaseDeskImpl;)V
    .locals 4
    .param p1    # Landroid/content/Context;
    .param p2    # Lcom/android/server/tv/TvHanlder;
    .param p3    # Lcom/android/server/tv/DataBaseDeskImpl;

    invoke-direct {p0}, Lcom/mstar/android/tv/ITvCommon$Stub;-><init>()V

    const/4 v2, 0x0

    iput-object v2, p0, Lcom/android/server/tv/TvCommonClient;->psl:[I

    sget-object v2, Lcom/mstar/android/tvapi/common/vo/TvOsType$EnumInputSource;->E_INPUT_SOURCE_NONE:Lcom/mstar/android/tvapi/common/vo/TvOsType$EnumInputSource;

    iput-object v2, p0, Lcom/android/server/tv/TvCommonClient;->curSourceType:Lcom/mstar/android/tvapi/common/vo/TvOsType$EnumInputSource;

    iput-object p1, p0, Lcom/android/server/tv/TvCommonClient;->mContext:Landroid/content/Context;

    iput-object p2, p0, Lcom/android/server/tv/TvCommonClient;->mHandler:Landroid/os/Handler;

    invoke-static {p1}, Lcom/android/server/tv/DataBaseDeskImpl;->getDataBaseMgrInstance(Landroid/content/Context;)Lcom/android/server/tv/DataBaseDeskImpl;

    move-result-object v2

    iput-object v2, p0, Lcom/android/server/tv/TvCommonClient;->databaseMgr:Lcom/android/server/tv/DataBaseDeskImpl;

    :try_start_0
    invoke-static {}, Lcom/mstar/android/tvapi/common/TvManager;->getInstance()Lcom/mstar/android/tvapi/common/TvManager;

    move-result-object v2

    if-eqz v2, :cond_0

    invoke-static {}, Lcom/mstar/android/tvapi/common/TvManager;->getInstance()Lcom/mstar/android/tvapi/common/TvManager;

    move-result-object v2

    invoke-virtual {v2}, Lcom/mstar/android/tvapi/common/TvManager;->getSourceList()[I

    move-result-object v2

    iput-object v2, p0, Lcom/android/server/tv/TvCommonClient;->psl:[I
    :try_end_0
    .catch Lcom/mstar/android/tvapi/common/exception/TvCommonException; {:try_start_0 .. :try_end_0} :catch_0

    :cond_0
    :goto_0
    invoke-static {}, Lcom/android/server/tv/DeskAtvPlayerEventListener;->getInstance()Lcom/android/server/tv/DeskAtvPlayerEventListener;

    move-result-object v2

    iput-object v2, p0, Lcom/android/server/tv/TvCommonClient;->deskAtvPlayerLister:Lcom/android/server/tv/DeskAtvPlayerEventListener;

    invoke-static {}, Lcom/android/server/tv/DeskDtvPlayerEventListener;->getInstance()Lcom/android/server/tv/DeskDtvPlayerEventListener;

    move-result-object v2

    iput-object v2, p0, Lcom/android/server/tv/TvCommonClient;->deskDtvPlayerLister:Lcom/android/server/tv/DeskDtvPlayerEventListener;

    invoke-static {}, Lcom/android/server/tv/DeskTvPlayerEventListener;->getInstance()Lcom/android/server/tv/DeskTvPlayerEventListener;

    move-result-object v2

    iput-object v2, p0, Lcom/android/server/tv/TvCommonClient;->deskTvPlayerLister:Lcom/android/server/tv/DeskTvPlayerEventListener;

    invoke-static {}, Lcom/android/server/tv/DeskTvEventListener;->getInstance()Lcom/android/server/tv/DeskTvEventListener;

    move-result-object v2

    iput-object v2, p0, Lcom/android/server/tv/TvCommonClient;->deskTvLister:Lcom/android/server/tv/DeskTvEventListener;

    invoke-static {}, Lcom/android/server/tv/DeskTvEventListener;->getInstance()Lcom/android/server/tv/DeskTvEventListener;

    move-result-object v2

    invoke-virtual {v2, p1}, Lcom/android/server/tv/DeskTvEventListener;->setContext(Landroid/content/Context;)V

    invoke-static {}, Lcom/android/server/tv/DeskTvEventListener;->getInstance()Lcom/android/server/tv/DeskTvEventListener;

    move-result-object v2

    invoke-virtual {v2, p2}, Lcom/android/server/tv/DeskTvEventListener;->setHandler(Lcom/android/server/tv/TvHanlder;)V

    invoke-static {}, Lcom/android/server/tv/DeskCiEventListener;->getInstance()Lcom/android/server/tv/DeskCiEventListener;

    move-result-object v2

    iput-object v2, p0, Lcom/android/server/tv/TvCommonClient;->deskCiLister:Lcom/android/server/tv/DeskCiEventListener;

    invoke-static {}, Lcom/android/server/tv/DeskCaEventListener;->getInstance()Lcom/android/server/tv/DeskCaEventListener;

    move-result-object v2

    iput-object v2, p0, Lcom/android/server/tv/TvCommonClient;->deskCaLister:Lcom/android/server/tv/DeskCaEventListener;

    invoke-static {}, Lcom/android/server/tv/DeskTimerEventListener;->getInstance()Lcom/android/server/tv/DeskTimerEventListener;

    move-result-object v2

    iput-object v2, p0, Lcom/android/server/tv/TvCommonClient;->deskTimerLister:Lcom/android/server/tv/DeskTimerEventListener;

    invoke-static {}, Lcom/android/server/tv/DeskPictureEventListener;->getInstance()Lcom/android/server/tv/DeskPictureEventListener;

    move-result-object v2

    iput-object v2, p0, Lcom/android/server/tv/TvCommonClient;->deskPictureLister:Lcom/android/server/tv/DeskPictureEventListener;

    invoke-static {}, Lcom/android/server/tv/DeskPipEventListener;->getInstance()Lcom/android/server/tv/DeskPipEventListener;

    move-result-object v2

    iput-object v2, p0, Lcom/android/server/tv/TvCommonClient;->deskPipLister:Lcom/android/server/tv/DeskPipEventListener;

    invoke-static {}, Lcom/android/server/tv/DeskThreeDimensionEventListener;->getInstance()Lcom/android/server/tv/DeskThreeDimensionEventListener;

    move-result-object v2

    iput-object v2, p0, Lcom/android/server/tv/TvCommonClient;->deskThreeDimensionLister:Lcom/android/server/tv/DeskThreeDimensionEventListener;

    invoke-static {}, Lcom/mstar/android/tvapi/dtv/common/DtvManager;->getDvbPlayerManager()Lcom/mstar/android/tvapi/dtv/dvb/DvbPlayer;

    move-result-object v2

    if-eqz v2, :cond_1

    invoke-static {}, Lcom/mstar/android/tvapi/dtv/common/DtvManager;->getDvbPlayerManager()Lcom/mstar/android/tvapi/dtv/dvb/DvbPlayer;

    move-result-object v2

    iget-object v3, p0, Lcom/android/server/tv/TvCommonClient;->deskDtvPlayerLister:Lcom/android/server/tv/DeskDtvPlayerEventListener;

    invoke-interface {v2, v3}, Lcom/mstar/android/tvapi/dtv/dvb/DvbPlayer;->setOnDtvPlayerEventListener(Lcom/mstar/android/tvapi/dtv/listener/OnDtvPlayerEventListener;)V

    :cond_1
    invoke-static {}, Lcom/mstar/android/tvapi/atv/AtvManager;->getAtvPlayerManager()Lcom/mstar/android/tvapi/atv/AtvPlayer;

    move-result-object v2

    if-eqz v2, :cond_2

    invoke-static {}, Lcom/mstar/android/tvapi/atv/AtvManager;->getAtvPlayerManager()Lcom/mstar/android/tvapi/atv/AtvPlayer;

    move-result-object v2

    iget-object v3, p0, Lcom/android/server/tv/TvCommonClient;->deskAtvPlayerLister:Lcom/android/server/tv/DeskAtvPlayerEventListener;

    invoke-interface {v2, v3}, Lcom/mstar/android/tvapi/atv/AtvPlayer;->setOnAtvPlayerEventListener(Lcom/mstar/android/tvapi/atv/listener/OnAtvPlayerEventListener;)V

    :cond_2
    invoke-static {}, Lcom/mstar/android/tvapi/common/TvManager;->getInstance()Lcom/mstar/android/tvapi/common/TvManager;

    move-result-object v2

    if-eqz v2, :cond_3

    invoke-static {}, Lcom/mstar/android/tvapi/common/TvManager;->getInstance()Lcom/mstar/android/tvapi/common/TvManager;

    move-result-object v2

    invoke-virtual {v2}, Lcom/mstar/android/tvapi/common/TvManager;->getPlayerManager()Lcom/mstar/android/tvapi/common/TvPlayer;

    move-result-object v2

    iget-object v3, p0, Lcom/android/server/tv/TvCommonClient;->deskTvPlayerLister:Lcom/android/server/tv/DeskTvPlayerEventListener;

    invoke-interface {v2, v3}, Lcom/mstar/android/tvapi/common/TvPlayer;->setOnTvPlayerEventListener(Lcom/mstar/android/tvapi/common/listener/OnTvPlayerEventListener;)V

    invoke-static {}, Lcom/mstar/android/tvapi/common/TvManager;->getInstance()Lcom/mstar/android/tvapi/common/TvManager;

    move-result-object v2

    iget-object v3, p0, Lcom/android/server/tv/TvCommonClient;->deskTvLister:Lcom/android/server/tv/DeskTvEventListener;

    invoke-virtual {v2, v3}, Lcom/mstar/android/tvapi/common/TvManager;->setOnTvEventListener(Lcom/mstar/android/tvapi/common/listener/OnTvEventListener;)V

    :cond_3
    const/4 v0, 0x0

    :try_start_1
    invoke-static {}, Lcom/mstar/android/tvapi/dtv/common/DtvManager;->getCiManager()Lcom/mstar/android/tvapi/dtv/common/CiManager;
    :try_end_1
    .catch Lcom/mstar/android/tvapi/common/exception/TvCommonException; {:try_start_1 .. :try_end_1} :catch_1

    move-result-object v0

    :goto_1
    if-eqz v0, :cond_4

    iget-object v2, p0, Lcom/android/server/tv/TvCommonClient;->deskCiLister:Lcom/android/server/tv/DeskCiEventListener;

    invoke-virtual {v0, v2}, Lcom/mstar/android/tvapi/dtv/common/CiManager;->setOnCiEventListener(Lcom/mstar/android/tvapi/dtv/common/CiManager$OnCiEventListener;)V

    :cond_4
    invoke-static {}, Lcom/mstar/android/tvapi/dtv/common/CaManager;->getInstance()Lcom/mstar/android/tvapi/dtv/common/CaManager;

    move-result-object v2

    if-eqz v2, :cond_5

    invoke-static {}, Lcom/mstar/android/tvapi/dtv/common/CaManager;->getInstance()Lcom/mstar/android/tvapi/dtv/common/CaManager;

    move-result-object v2

    iget-object v3, p0, Lcom/android/server/tv/TvCommonClient;->deskCaLister:Lcom/android/server/tv/DeskCaEventListener;

    invoke-virtual {v2, v3}, Lcom/mstar/android/tvapi/dtv/common/CaManager;->setOnCaEventListener(Lcom/mstar/android/tvapi/dtv/common/CaManager$OnCaEventListener;)V

    :cond_5
    invoke-static {}, Lcom/mstar/android/tvapi/common/TvManager;->getInstance()Lcom/mstar/android/tvapi/common/TvManager;

    move-result-object v2

    if-eqz v2, :cond_6

    invoke-static {}, Lcom/mstar/android/tvapi/common/TvManager;->getInstance()Lcom/mstar/android/tvapi/common/TvManager;

    move-result-object v2

    invoke-virtual {v2}, Lcom/mstar/android/tvapi/common/TvManager;->getTimerManager()Lcom/mstar/android/tvapi/common/TimerManager;

    move-result-object v2

    if-eqz v2, :cond_6

    :cond_6
    invoke-static {}, Lcom/mstar/android/tvapi/common/PictureManager;->getInstance()Lcom/mstar/android/tvapi/common/PictureManager;

    move-result-object v2

    if-eqz v2, :cond_7

    invoke-static {}, Lcom/mstar/android/tvapi/common/PictureManager;->getInstance()Lcom/mstar/android/tvapi/common/PictureManager;

    move-result-object v2

    iget-object v3, p0, Lcom/android/server/tv/TvCommonClient;->deskPictureLister:Lcom/android/server/tv/DeskPictureEventListener;

    invoke-virtual {v2, v3}, Lcom/mstar/android/tvapi/common/PictureManager;->setOnPictureEventListener(Lcom/mstar/android/tvapi/common/listener/OnPictureEventListener;)V

    :cond_7
    invoke-static {}, Lcom/mstar/android/tvapi/common/PipManager;->getInstance()Lcom/mstar/android/tvapi/common/PipManager;

    move-result-object v2

    if-eqz v2, :cond_8

    invoke-static {}, Lcom/mstar/android/tvapi/common/PipManager;->getInstance()Lcom/mstar/android/tvapi/common/PipManager;

    move-result-object v2

    iget-object v3, p0, Lcom/android/server/tv/TvCommonClient;->deskPipLister:Lcom/android/server/tv/DeskPipEventListener;

    invoke-virtual {v2, v3}, Lcom/mstar/android/tvapi/common/PipManager;->setOnPipEventListener(Lcom/mstar/android/tvapi/common/listener/OnPipEventListener;)V

    :cond_8
    invoke-static {}, Lcom/mstar/android/tvapi/common/PipManager;->getInstance()Lcom/mstar/android/tvapi/common/PipManager;

    move-result-object v2

    if-eqz v2, :cond_9

    invoke-static {}, Lcom/mstar/android/tvapi/common/PipManager;->getInstance()Lcom/mstar/android/tvapi/common/PipManager;

    move-result-object v2

    iget-object v3, p0, Lcom/android/server/tv/TvCommonClient;->deskPipLister:Lcom/android/server/tv/DeskPipEventListener;

    invoke-virtual {v2, v3}, Lcom/mstar/android/tvapi/common/PipManager;->setOnPipEventListener(Lcom/mstar/android/tvapi/common/listener/OnPipEventListener;)V

    :cond_9
    invoke-static {}, Lcom/mstar/android/tvapi/common/ThreeDimensionManager;->getInstance()Lcom/mstar/android/tvapi/common/ThreeDimensionManager;

    move-result-object v2

    if-eqz v2, :cond_a

    invoke-static {}, Lcom/mstar/android/tvapi/common/ThreeDimensionManager;->getInstance()Lcom/mstar/android/tvapi/common/ThreeDimensionManager;

    move-result-object v2

    iget-object v3, p0, Lcom/android/server/tv/TvCommonClient;->deskThreeDimensionLister:Lcom/android/server/tv/DeskThreeDimensionEventListener;

    invoke-virtual {v2, v3}, Lcom/mstar/android/tvapi/common/ThreeDimensionManager;->setOnThreeDimensionEventListener(Lcom/mstar/android/tvapi/common/listener/OnThreeDimensionEventListener;)V

    :cond_a
    return-void

    :catch_0
    move-exception v1

    invoke-virtual {v1}, Lcom/mstar/android/tvapi/common/exception/TvCommonException;->printStackTrace()V

    goto/16 :goto_0

    :catch_1
    move-exception v1

    invoke-virtual {v1}, Lcom/mstar/android/tvapi/common/exception/TvCommonException;->printStackTrace()V

    goto :goto_1
.end method

.method static synthetic access$000(Lcom/android/server/tv/TvCommonClient;)Landroid/content/Context;
    .locals 1
    .param p0    # Lcom/android/server/tv/TvCommonClient;

    iget-object v0, p0, Lcom/android/server/tv/TvCommonClient;->mContext:Landroid/content/Context;

    return-object v0
.end method


# virtual methods
.method public GetInputSourceStatus()[Z
    .locals 3

    sget-object v1, Lcom/android/server/tv/TvCommonClient;->bSrcDetResult:[Z

    if-nez v1, :cond_0

    sget-object v1, Lcom/mstar/android/tvapi/common/vo/TvOsType$EnumInputSource;->E_INPUT_SOURCE_NUM:Lcom/mstar/android/tvapi/common/vo/TvOsType$EnumInputSource;

    invoke-virtual {v1}, Lcom/mstar/android/tvapi/common/vo/TvOsType$EnumInputSource;->ordinal()I

    move-result v1

    new-array v1, v1, [Z

    sput-object v1, Lcom/android/server/tv/TvCommonClient;->bSrcDetResult:[Z

    const/4 v0, 0x0

    :goto_0
    sget-object v1, Lcom/mstar/android/tvapi/common/vo/TvOsType$EnumInputSource;->E_INPUT_SOURCE_NUM:Lcom/mstar/android/tvapi/common/vo/TvOsType$EnumInputSource;

    invoke-virtual {v1}, Lcom/mstar/android/tvapi/common/vo/TvOsType$EnumInputSource;->ordinal()I

    move-result v1

    if-ge v0, v1, :cond_0

    sget-object v1, Lcom/android/server/tv/TvCommonClient;->bSrcDetResult:[Z

    const/4 v2, 0x0

    aput-boolean v2, v1, v0

    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    :cond_0
    sget-object v1, Lcom/android/server/tv/TvCommonClient;->bSrcDetResult:[Z

    return-object v1
.end method

.method public SetInputSourceStatus([S)V
    .locals 3
    .param p1    # [S

    sget-object v1, Lcom/android/server/tv/TvCommonClient;->bSrcDetResult:[Z

    if-nez v1, :cond_0

    sget-object v1, Lcom/mstar/android/tvapi/common/vo/TvOsType$EnumInputSource;->E_INPUT_SOURCE_NUM:Lcom/mstar/android/tvapi/common/vo/TvOsType$EnumInputSource;

    invoke-virtual {v1}, Lcom/mstar/android/tvapi/common/vo/TvOsType$EnumInputSource;->ordinal()I

    move-result v1

    new-array v1, v1, [Z

    sput-object v1, Lcom/android/server/tv/TvCommonClient;->bSrcDetResult:[Z

    :cond_0
    const/4 v0, 0x0

    :goto_0
    sget-object v1, Lcom/mstar/android/tvapi/common/vo/TvOsType$EnumInputSource;->E_INPUT_SOURCE_NUM:Lcom/mstar/android/tvapi/common/vo/TvOsType$EnumInputSource;

    invoke-virtual {v1}, Lcom/mstar/android/tvapi/common/vo/TvOsType$EnumInputSource;->ordinal()I

    move-result v1

    if-ge v0, v1, :cond_2

    aget-short v1, p1, v0

    if-nez v1, :cond_1

    sget-object v1, Lcom/android/server/tv/TvCommonClient;->bSrcDetResult:[Z

    const/4 v2, 0x0

    aput-boolean v2, v1, v0

    :goto_1
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    :cond_1
    sget-object v1, Lcom/android/server/tv/TvCommonClient;->bSrcDetResult:[Z

    const/4 v2, 0x1

    aput-boolean v2, v1, v0

    goto :goto_1

    :cond_2
    return-void
.end method

.method public addClient(Ljava/lang/String;Landroid/os/IBinder;)V
    .locals 4
    .param p1    # Ljava/lang/String;
    .param p2    # Landroid/os/IBinder;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Landroid/os/RemoteException;
        }
    .end annotation

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, ""

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-static {}, Lcom/android/server/tv/TvCommonClient;->getCallingPid()I

    move-result v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    const-string v1, "TvCommonClient"

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "addClient, paras name is "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, " client is "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, p2}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, "getCallingPid()"

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    const-string v1, "DeskAtvPlayerEventListener"

    invoke-virtual {p1, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_0

    invoke-static {}, Lcom/android/server/tv/DeskAtvPlayerEventListener;->getInstance()Lcom/android/server/tv/DeskAtvPlayerEventListener;

    move-result-object v1

    invoke-static {p2}, Lcom/mstar/android/tv/IAtvPlayerEventClient$Stub;->asInterface(Landroid/os/IBinder;)Lcom/mstar/android/tv/IAtvPlayerEventClient;

    move-result-object v2

    invoke-virtual {v1, v0, v2}, Lcom/android/server/tv/DeskAtvPlayerEventListener;->setClient(Ljava/lang/String;Lcom/mstar/android/tv/IAtvPlayerEventClient;)V

    :cond_0
    const-string v1, "DeskCaEventListener"

    invoke-virtual {p1, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_1

    invoke-static {}, Lcom/android/server/tv/DeskCaEventListener;->getInstance()Lcom/android/server/tv/DeskCaEventListener;

    move-result-object v1

    invoke-static {p2}, Lcom/mstar/android/tv/ICaEventClient$Stub;->asInterface(Landroid/os/IBinder;)Lcom/mstar/android/tv/ICaEventClient;

    move-result-object v2

    invoke-virtual {v1, v0, v2}, Lcom/android/server/tv/DeskCaEventListener;->setClient(Ljava/lang/String;Lcom/mstar/android/tv/ICaEventClient;)V

    :cond_1
    const-string v1, "DeskCiEventListener"

    invoke-virtual {p1, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_2

    invoke-static {}, Lcom/android/server/tv/DeskCiEventListener;->getInstance()Lcom/android/server/tv/DeskCiEventListener;

    move-result-object v1

    invoke-static {p2}, Lcom/mstar/android/tv/ICiEventClient$Stub;->asInterface(Landroid/os/IBinder;)Lcom/mstar/android/tv/ICiEventClient;

    move-result-object v2

    invoke-virtual {v1, v0, v2}, Lcom/android/server/tv/DeskCiEventListener;->setClient(Ljava/lang/String;Lcom/mstar/android/tv/ICiEventClient;)V

    :cond_2
    const-string v1, "DeskDtvPlayerEventListener"

    invoke-virtual {p1, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_3

    invoke-static {}, Lcom/android/server/tv/DeskDtvPlayerEventListener;->getInstance()Lcom/android/server/tv/DeskDtvPlayerEventListener;

    move-result-object v1

    invoke-static {p2}, Lcom/mstar/android/tv/IDtvPlayerEventClient$Stub;->asInterface(Landroid/os/IBinder;)Lcom/mstar/android/tv/IDtvPlayerEventClient;

    move-result-object v2

    invoke-virtual {v1, v0, v2}, Lcom/android/server/tv/DeskDtvPlayerEventListener;->setClient(Ljava/lang/String;Lcom/mstar/android/tv/IDtvPlayerEventClient;)V

    :cond_3
    const-string v1, "DeskTimerEventListener"

    invoke-virtual {p1, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_4

    invoke-static {}, Lcom/android/server/tv/DeskTimerEventListener;->getInstance()Lcom/android/server/tv/DeskTimerEventListener;

    move-result-object v1

    invoke-static {p2}, Lcom/mstar/android/tv/ITimerEventClient$Stub;->asInterface(Landroid/os/IBinder;)Lcom/mstar/android/tv/ITimerEventClient;

    move-result-object v2

    invoke-virtual {v1, v0, v2}, Lcom/android/server/tv/DeskTimerEventListener;->setClient(Ljava/lang/String;Lcom/mstar/android/tv/ITimerEventClient;)V

    :cond_4
    const-string v1, "DeskTvEventListener"

    invoke-virtual {p1, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_5

    invoke-static {}, Lcom/android/server/tv/DeskTvEventListener;->getInstance()Lcom/android/server/tv/DeskTvEventListener;

    move-result-object v1

    invoke-static {p2}, Lcom/mstar/android/tv/ITvEventClient$Stub;->asInterface(Landroid/os/IBinder;)Lcom/mstar/android/tv/ITvEventClient;

    move-result-object v2

    invoke-virtual {v1, v0, v2}, Lcom/android/server/tv/DeskTvEventListener;->setClient(Ljava/lang/String;Lcom/mstar/android/tv/ITvEventClient;)V

    :cond_5
    const-string v1, "DeskTvPlayerEventListener"

    invoke-virtual {p1, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_6

    invoke-static {}, Lcom/android/server/tv/DeskTvPlayerEventListener;->getInstance()Lcom/android/server/tv/DeskTvPlayerEventListener;

    move-result-object v1

    invoke-static {p2}, Lcom/mstar/android/tv/ITvPlayerEventClient$Stub;->asInterface(Landroid/os/IBinder;)Lcom/mstar/android/tv/ITvPlayerEventClient;

    move-result-object v2

    invoke-virtual {v1, v0, v2}, Lcom/android/server/tv/DeskTvPlayerEventListener;->setClient(Ljava/lang/String;Lcom/mstar/android/tv/ITvPlayerEventClient;)V

    :cond_6
    const-string v1, "DeskPvrEventListener"

    invoke-virtual {p1, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_7

    invoke-static {}, Lcom/android/server/tv/DeskPvrEventListener;->getInstance()Lcom/android/server/tv/DeskPvrEventListener;

    move-result-object v1

    invoke-static {p2}, Lcom/mstar/android/tv/IPvrEventClient$Stub;->asInterface(Landroid/os/IBinder;)Lcom/mstar/android/tv/IPvrEventClient;

    move-result-object v2

    invoke-virtual {v1, v0, v2}, Lcom/android/server/tv/DeskPvrEventListener;->setClient(Ljava/lang/String;Lcom/mstar/android/tv/IPvrEventClient;)V

    :cond_7
    const-string v1, "DeskAudioEventListener"

    invoke-virtual {p1, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_8

    invoke-static {}, Lcom/android/server/tv/DeskAudioEventListener;->getInstance()Lcom/android/server/tv/DeskAudioEventListener;

    move-result-object v1

    invoke-static {p2}, Lcom/mstar/android/tv/IAudioEventClient$Stub;->asInterface(Landroid/os/IBinder;)Lcom/mstar/android/tv/IAudioEventClient;

    move-result-object v2

    invoke-virtual {v1, v0, v2}, Lcom/android/server/tv/DeskAudioEventListener;->setClient(Ljava/lang/String;Lcom/mstar/android/tv/IAudioEventClient;)V

    :cond_8
    const-string v1, "DeskCecEventListener"

    invoke-virtual {p1, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_9

    invoke-static {}, Lcom/android/server/tv/DeskCecEventListener;->getInstance()Lcom/android/server/tv/DeskCecEventListener;

    move-result-object v1

    invoke-static {p2}, Lcom/mstar/android/tv/ICecEventClient$Stub;->asInterface(Landroid/os/IBinder;)Lcom/mstar/android/tv/ICecEventClient;

    move-result-object v2

    invoke-virtual {v1, v0, v2}, Lcom/android/server/tv/DeskCecEventListener;->setClient(Ljava/lang/String;Lcom/mstar/android/tv/ICecEventClient;)V

    :cond_9
    return-void
.end method

.method public closeSurfaceView()V
    .locals 2
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Landroid/os/RemoteException;
        }
    .end annotation

    iget-object v0, p0, Lcom/android/server/tv/TvCommonClient;->mHandler:Landroid/os/Handler;

    const/4 v1, 0x5

    invoke-virtual {v0, v1}, Landroid/os/Handler;->sendEmptyMessage(I)Z

    return-void
.end method

.method public enterSleepMode(ZZ)V
    .locals 4
    .param p1    # Z
    .param p2    # Z
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Landroid/os/RemoteException;
        }
    .end annotation

    const-string v1, "TvCommonClient"

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "enterSleepMode, paras bMode is "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, p1}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, " bNoSignalPwDn is "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, p2}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    :try_start_0
    invoke-static {}, Lcom/mstar/android/tvapi/common/TvManager;->getInstance()Lcom/mstar/android/tvapi/common/TvManager;

    move-result-object v1

    if-eqz v1, :cond_0

    invoke-static {}, Lcom/mstar/android/tvapi/common/TvManager;->getInstance()Lcom/mstar/android/tvapi/common/TvManager;

    move-result-object v1

    invoke-virtual {v1, p1, p2}, Lcom/mstar/android/tvapi/common/TvManager;->enterSleepMode(ZZ)V
    :try_end_0
    .catch Lcom/mstar/android/tvapi/common/exception/TvCommonException; {:try_start_0 .. :try_end_0} :catch_0

    :cond_0
    :goto_0
    return-void

    :catch_0
    move-exception v0

    invoke-virtual {v0}, Lcom/mstar/android/tvapi/common/exception/TvCommonException;->printStackTrace()V

    goto :goto_0
.end method

.method public getAtvMtsMode()I
    .locals 2
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Landroid/os/RemoteException;
        }
    .end annotation

    :try_start_0
    invoke-static {}, Lcom/mstar/android/tvapi/common/TvManager;->getInstance()Lcom/mstar/android/tvapi/common/TvManager;

    move-result-object v1

    if-eqz v1, :cond_0

    invoke-static {}, Lcom/mstar/android/tvapi/common/TvManager;->getInstance()Lcom/mstar/android/tvapi/common/TvManager;

    move-result-object v1

    invoke-virtual {v1}, Lcom/mstar/android/tvapi/common/TvManager;->getAudioManager()Lcom/mstar/android/tvapi/common/AudioManager;

    move-result-object v1

    invoke-virtual {v1}, Lcom/mstar/android/tvapi/common/AudioManager;->getAtvMtsMode()Lcom/mstar/android/tvapi/common/vo/EnumAtvAudioModeType;

    move-result-object v1

    invoke-virtual {v1}, Lcom/mstar/android/tvapi/common/vo/EnumAtvAudioModeType;->ordinal()I
    :try_end_0
    .catch Lcom/mstar/android/tvapi/common/exception/TvCommonException; {:try_start_0 .. :try_end_0} :catch_0

    move-result v1

    :goto_0
    return v1

    :catch_0
    move-exception v0

    invoke-virtual {v0}, Lcom/mstar/android/tvapi/common/exception/TvCommonException;->printStackTrace()V

    :cond_0
    const/4 v1, -0x1

    goto :goto_0
.end method

.method public getAtvSoundMode()I
    .locals 2
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Landroid/os/RemoteException;
        }
    .end annotation

    :try_start_0
    invoke-static {}, Lcom/mstar/android/tvapi/common/TvManager;->getInstance()Lcom/mstar/android/tvapi/common/TvManager;

    move-result-object v1

    if-eqz v1, :cond_0

    invoke-static {}, Lcom/mstar/android/tvapi/common/TvManager;->getInstance()Lcom/mstar/android/tvapi/common/TvManager;

    move-result-object v1

    invoke-virtual {v1}, Lcom/mstar/android/tvapi/common/TvManager;->getAudioManager()Lcom/mstar/android/tvapi/common/AudioManager;

    move-result-object v1

    invoke-virtual {v1}, Lcom/mstar/android/tvapi/common/AudioManager;->getAtvSoundMode()Lcom/mstar/android/tvapi/common/vo/EnumAtvAudioModeType;

    move-result-object v1

    invoke-virtual {v1}, Lcom/mstar/android/tvapi/common/vo/EnumAtvAudioModeType;->ordinal()I
    :try_end_0
    .catch Lcom/mstar/android/tvapi/common/exception/TvCommonException; {:try_start_0 .. :try_end_0} :catch_0

    move-result v1

    :goto_0
    return v1

    :catch_0
    move-exception v0

    invoke-virtual {v0}, Lcom/mstar/android/tvapi/common/exception/TvCommonException;->printStackTrace()V

    :cond_0
    const/4 v1, -0x1

    goto :goto_0
.end method

.method public getClient(Ljava/lang/String;)Landroid/os/IBinder;
    .locals 1
    .param p1    # Ljava/lang/String;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Landroid/os/RemoteException;
        }
    .end annotation

    const/4 v0, 0x0

    return-object v0
.end method

.method public getCurrentInputSource()I
    .locals 5
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Landroid/os/RemoteException;
        }
    .end annotation

    :try_start_0
    invoke-static {}, Lcom/mstar/android/tvapi/common/TvManager;->getInstance()Lcom/mstar/android/tvapi/common/TvManager;

    move-result-object v2

    if-eqz v2, :cond_0

    invoke-static {}, Lcom/mstar/android/tvapi/common/TvManager;->getInstance()Lcom/mstar/android/tvapi/common/TvManager;

    move-result-object v2

    invoke-virtual {v2}, Lcom/mstar/android/tvapi/common/TvManager;->getCurrentInputSource()Lcom/mstar/android/tvapi/common/vo/TvOsType$EnumInputSource;

    move-result-object v2

    iput-object v2, p0, Lcom/android/server/tv/TvCommonClient;->curSourceType:Lcom/mstar/android/tvapi/common/vo/TvOsType$EnumInputSource;
    :try_end_0
    .catch Lcom/mstar/android/tvapi/common/exception/TvCommonException; {:try_start_0 .. :try_end_0} :catch_0

    :cond_0
    :goto_0
    iget-object v2, p0, Lcom/android/server/tv/TvCommonClient;->curSourceType:Lcom/mstar/android/tvapi/common/vo/TvOsType$EnumInputSource;

    invoke-virtual {v2}, Lcom/mstar/android/tvapi/common/vo/TvOsType$EnumInputSource;->ordinal()I

    move-result v1

    const-string v2, "TvCommonClient"

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string v4, "getCurrentInputSource, return int "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v2, v3}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    return v1

    :catch_0
    move-exception v0

    invoke-virtual {v0}, Lcom/mstar/android/tvapi/common/exception/TvCommonException;->printStackTrace()V

    goto :goto_0
.end method

.method public getCurrentSubInputSource()I
    .locals 5
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Landroid/os/RemoteException;
        }
    .end annotation

    :try_start_0
    invoke-static {}, Lcom/mstar/android/tvapi/common/TvManager;->getInstance()Lcom/mstar/android/tvapi/common/TvManager;

    move-result-object v2

    if-eqz v2, :cond_0

    invoke-static {}, Lcom/mstar/android/tvapi/common/TvManager;->getInstance()Lcom/mstar/android/tvapi/common/TvManager;

    move-result-object v2

    invoke-virtual {v2}, Lcom/mstar/android/tvapi/common/TvManager;->getCurrentInputSource()Lcom/mstar/android/tvapi/common/vo/TvOsType$EnumInputSource;

    move-result-object v2

    iput-object v2, p0, Lcom/android/server/tv/TvCommonClient;->curSourceType:Lcom/mstar/android/tvapi/common/vo/TvOsType$EnumInputSource;
    :try_end_0
    .catch Lcom/mstar/android/tvapi/common/exception/TvCommonException; {:try_start_0 .. :try_end_0} :catch_0

    :cond_0
    :goto_0
    iget-object v2, p0, Lcom/android/server/tv/TvCommonClient;->curSourceType:Lcom/mstar/android/tvapi/common/vo/TvOsType$EnumInputSource;

    invoke-virtual {v2}, Lcom/mstar/android/tvapi/common/vo/TvOsType$EnumInputSource;->ordinal()I

    move-result v1

    const-string v2, "TvCommonClient"

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string v4, "getCurrentSubInputSource, return int "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v2, v3}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    return v1

    :catch_0
    move-exception v0

    invoke-virtual {v0}, Lcom/mstar/android/tvapi/common/exception/TvCommonException;->printStackTrace()V

    goto :goto_0
.end method

.method public getHPPortStatus()Z
    .locals 1

    sget-boolean v0, Lcom/android/server/tv/TvCommonClient;->isHPPortPlugged:Z

    return v0
.end method

.method public getPowerOnSource()I
    .locals 4
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Landroid/os/RemoteException;
        }
    .end annotation

    iget-object v1, p0, Lcom/android/server/tv/TvCommonClient;->databaseMgr:Lcom/android/server/tv/DataBaseDeskImpl;

    invoke-virtual {v1}, Lcom/android/server/tv/DataBaseDeskImpl;->queryPowerOnSource()I

    move-result v0

    const-string v1, "TvCommonClient"

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "getPowerOnSource, return int "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    return v0
.end method

.method public getSourceList()[I
    .locals 2
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Landroid/os/RemoteException;
        }
    .end annotation

    :try_start_0
    invoke-static {}, Lcom/mstar/android/tvapi/common/TvManager;->getInstance()Lcom/mstar/android/tvapi/common/TvManager;

    move-result-object v1

    if-eqz v1, :cond_0

    invoke-static {}, Lcom/mstar/android/tvapi/common/TvManager;->getInstance()Lcom/mstar/android/tvapi/common/TvManager;

    move-result-object v1

    invoke-virtual {v1}, Lcom/mstar/android/tvapi/common/TvManager;->getSourceList()[I
    :try_end_0
    .catch Lcom/mstar/android/tvapi/common/exception/TvCommonException; {:try_start_0 .. :try_end_0} :catch_0

    move-result-object v1

    :goto_0
    return-object v1

    :catch_0
    move-exception v0

    invoke-virtual {v0}, Lcom/mstar/android/tvapi/common/exception/TvCommonException;->printStackTrace()V

    :cond_0
    const/4 v1, 0x0

    goto :goto_0
.end method

.method public isPreviousSourceDTV()Z
    .locals 1

    sget-boolean v0, Lcom/android/server/tv/TvCommonClient;->isPreviousSourceDTV:Z

    return v0
.end method

.method public openSurfaceView(IIII)V
    .locals 3
    .param p1    # I
    .param p2    # I
    .param p3    # I
    .param p4    # I
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Landroid/os/RemoteException;
        }
    .end annotation

    new-instance v1, Landroid/os/Message;

    invoke-direct {v1}, Landroid/os/Message;-><init>()V

    const/4 v2, 0x4

    iput v2, v1, Landroid/os/Message;->what:I

    new-instance v0, Landroid/os/Bundle;

    invoke-direct {v0}, Landroid/os/Bundle;-><init>()V

    const-string v2, "x"

    invoke-virtual {v0, v2, p1}, Landroid/os/Bundle;->putInt(Ljava/lang/String;I)V

    const-string v2, "y"

    invoke-virtual {v0, v2, p2}, Landroid/os/Bundle;->putInt(Ljava/lang/String;I)V

    const-string v2, "width"

    invoke-virtual {v0, v2, p3}, Landroid/os/Bundle;->putInt(Ljava/lang/String;I)V

    const-string v2, "height"

    invoke-virtual {v0, v2, p4}, Landroid/os/Bundle;->putInt(Ljava/lang/String;I)V

    invoke-virtual {v1, v0}, Landroid/os/Message;->setData(Landroid/os/Bundle;)V

    iget-object v2, p0, Lcom/android/server/tv/TvCommonClient;->mHandler:Landroid/os/Handler;

    invoke-virtual {v2, v1}, Landroid/os/Handler;->sendMessage(Landroid/os/Message;)Z

    return-void
.end method

.method public rebootSystem(Ljava/lang/String;)V
    .locals 2
    .param p1    # Ljava/lang/String;

    sget-object v1, Lcom/android/server/tv/TvCommonClient;->REBOOTCOMMOND:Ljava/lang/String;

    invoke-virtual {p1, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_0

    new-instance v0, Landroid/content/Intent;

    const-string v1, "android.intent.action.REBOOT"

    invoke-direct {v0, v1}, Landroid/content/Intent;-><init>(Ljava/lang/String;)V

    const/high16 v1, 0x10000000

    invoke-virtual {v0, v1}, Landroid/content/Intent;->setFlags(I)Landroid/content/Intent;

    iget-object v1, p0, Lcom/android/server/tv/TvCommonClient;->mContext:Landroid/content/Context;

    invoke-virtual {v1, v0}, Landroid/content/Context;->startActivity(Landroid/content/Intent;)V

    :cond_0
    return-void
.end method

.method public recoverySystem(Ljava/lang/String;)V
    .locals 3
    .param p1    # Ljava/lang/String;

    new-instance v0, Ljava/io/File;

    invoke-direct {v0, p1}, Ljava/io/File;-><init>(Ljava/lang/String;)V

    invoke-virtual {v0}, Ljava/io/File;->exists()Z

    move-result v1

    if-eqz v1, :cond_0

    new-instance v1, Ljava/lang/Thread;

    new-instance v2, Lcom/android/server/tv/TvCommonClient$1;

    invoke-direct {v2, p0, v0}, Lcom/android/server/tv/TvCommonClient$1;-><init>(Lcom/android/server/tv/TvCommonClient;Ljava/io/File;)V

    invoke-direct {v1, v2}, Ljava/lang/Thread;-><init>(Ljava/lang/Runnable;)V

    invoke-virtual {v1}, Ljava/lang/Thread;->start()V

    :cond_0
    return-void
.end method

.method public removeClient(Ljava/lang/String;)V
    .locals 4
    .param p1    # Ljava/lang/String;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Landroid/os/RemoteException;
        }
    .end annotation

    const-string v1, "TvCommonClient"

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "removeClient, paras name is "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, ""

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-static {}, Lcom/android/server/tv/TvCommonClient;->getCallingPid()I

    move-result v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    const-string v1, "DeskAtvPlayerEventListener"

    invoke-virtual {p1, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_0

    invoke-static {}, Lcom/android/server/tv/DeskAtvPlayerEventListener;->getInstance()Lcom/android/server/tv/DeskAtvPlayerEventListener;

    move-result-object v1

    invoke-virtual {v1, v0}, Lcom/android/server/tv/DeskAtvPlayerEventListener;->releaseClient(Ljava/lang/String;)V

    :cond_0
    const-string v1, "DeskCaEventListener"

    invoke-virtual {p1, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_1

    invoke-static {}, Lcom/android/server/tv/DeskCaEventListener;->getInstance()Lcom/android/server/tv/DeskCaEventListener;

    move-result-object v1

    invoke-virtual {v1, v0}, Lcom/android/server/tv/DeskCaEventListener;->releaseClient(Ljava/lang/String;)V

    :cond_1
    const-string v1, "DeskCiEventListener"

    invoke-virtual {p1, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_2

    invoke-static {}, Lcom/android/server/tv/DeskCiEventListener;->getInstance()Lcom/android/server/tv/DeskCiEventListener;

    move-result-object v1

    invoke-virtual {v1, v0}, Lcom/android/server/tv/DeskCiEventListener;->releaseClient(Ljava/lang/String;)V

    :cond_2
    const-string v1, "DeskDtvPlayerEventListener"

    invoke-virtual {p1, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_3

    invoke-static {}, Lcom/android/server/tv/DeskDtvPlayerEventListener;->getInstance()Lcom/android/server/tv/DeskDtvPlayerEventListener;

    move-result-object v1

    invoke-virtual {v1, v0}, Lcom/android/server/tv/DeskDtvPlayerEventListener;->releaseClient(Ljava/lang/String;)V

    :cond_3
    const-string v1, "DeskTimerEventListener"

    invoke-virtual {p1, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_4

    invoke-static {}, Lcom/android/server/tv/DeskTimerEventListener;->getInstance()Lcom/android/server/tv/DeskTimerEventListener;

    move-result-object v1

    invoke-virtual {v1, v0}, Lcom/android/server/tv/DeskTimerEventListener;->releaseClient(Ljava/lang/String;)V

    :cond_4
    const-string v1, "DeskTvEventListener"

    invoke-virtual {p1, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_5

    invoke-static {}, Lcom/android/server/tv/DeskTvEventListener;->getInstance()Lcom/android/server/tv/DeskTvEventListener;

    move-result-object v1

    invoke-virtual {v1, v0}, Lcom/android/server/tv/DeskTvEventListener;->releaseClient(Ljava/lang/String;)V

    :cond_5
    const-string v1, "DeskTvPlayerEventListener"

    invoke-virtual {p1, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_6

    invoke-static {}, Lcom/android/server/tv/DeskTvPlayerEventListener;->getInstance()Lcom/android/server/tv/DeskTvPlayerEventListener;

    move-result-object v1

    invoke-virtual {v1, v0}, Lcom/android/server/tv/DeskTvPlayerEventListener;->releaseClient(Ljava/lang/String;)V

    :cond_6
    const-string v1, "DeskPvrEventListener"

    invoke-virtual {p1, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_7

    invoke-static {}, Lcom/android/server/tv/DeskPvrEventListener;->getInstance()Lcom/android/server/tv/DeskPvrEventListener;

    move-result-object v1

    invoke-virtual {v1, v0}, Lcom/android/server/tv/DeskPvrEventListener;->releaseClient(Ljava/lang/String;)V

    :cond_7
    const-string v1, "DeskAudioEventListener"

    invoke-virtual {p1, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_8

    invoke-static {}, Lcom/android/server/tv/DeskAudioEventListener;->getInstance()Lcom/android/server/tv/DeskAudioEventListener;

    move-result-object v1

    invoke-virtual {v1, v0}, Lcom/android/server/tv/DeskAudioEventListener;->releaseClient(Ljava/lang/String;)V

    :cond_8
    const-string v1, "DeskCecEventListener"

    invoke-virtual {p1, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_9

    invoke-static {}, Lcom/android/server/tv/DeskCecEventListener;->getInstance()Lcom/android/server/tv/DeskCecEventListener;

    move-result-object v1

    invoke-virtual {v1, v0}, Lcom/android/server/tv/DeskCecEventListener;->releaseClient(Ljava/lang/String;)V

    :cond_9
    return-void
.end method

.method public setAtvMtsMode(I)I
    .locals 3
    .param p1    # I
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Landroid/os/RemoteException;
        }
    .end annotation

    :try_start_0
    invoke-static {}, Lcom/mstar/android/tvapi/common/TvManager;->getInstance()Lcom/mstar/android/tvapi/common/TvManager;

    move-result-object v1

    if-eqz v1, :cond_0

    invoke-static {}, Lcom/mstar/android/tvapi/common/TvManager;->getInstance()Lcom/mstar/android/tvapi/common/TvManager;

    move-result-object v1

    invoke-virtual {v1}, Lcom/mstar/android/tvapi/common/TvManager;->getAudioManager()Lcom/mstar/android/tvapi/common/AudioManager;

    move-result-object v1

    invoke-static {}, Lcom/mstar/android/tvapi/common/vo/EnumAtvAudioModeType;->values()[Lcom/mstar/android/tvapi/common/vo/EnumAtvAudioModeType;

    move-result-object v2

    aget-object v2, v2, p1

    invoke-virtual {v1, v2}, Lcom/mstar/android/tvapi/common/AudioManager;->setAtvMtsMode(Lcom/mstar/android/tvapi/common/vo/EnumAtvAudioModeType;)Lcom/mstar/android/tvapi/common/vo/EnumAudioReturn;

    move-result-object v1

    invoke-virtual {v1}, Lcom/mstar/android/tvapi/common/vo/EnumAudioReturn;->ordinal()I
    :try_end_0
    .catch Lcom/mstar/android/tvapi/common/exception/TvCommonException; {:try_start_0 .. :try_end_0} :catch_0

    move-result v1

    :goto_0
    return v1

    :catch_0
    move-exception v0

    invoke-virtual {v0}, Lcom/mstar/android/tvapi/common/exception/TvCommonException;->printStackTrace()V

    :cond_0
    const/4 v1, -0x1

    goto :goto_0
.end method

.method public setHPPortStatus(Z)V
    .locals 3
    .param p1    # Z

    sput-boolean p1, Lcom/android/server/tv/TvCommonClient;->isHPPortPlugged:Z

    const/4 v2, 0x1

    if-ne p1, v2, :cond_1

    const-string v1, "setHPPortStatusTrue"

    :goto_0
    :try_start_0
    invoke-static {}, Lcom/mstar/android/tvapi/common/TvManager;->getInstance()Lcom/mstar/android/tvapi/common/TvManager;

    move-result-object v2

    if-eqz v2, :cond_0

    invoke-static {}, Lcom/mstar/android/tvapi/common/TvManager;->getInstance()Lcom/mstar/android/tvapi/common/TvManager;

    move-result-object v2

    invoke-virtual {v2, v1}, Lcom/mstar/android/tvapi/common/TvManager;->setTvosInterfaceCommand(Ljava/lang/String;)Z
    :try_end_0
    .catch Lcom/mstar/android/tvapi/common/exception/TvCommonException; {:try_start_0 .. :try_end_0} :catch_0

    :cond_0
    :goto_1
    return-void

    :cond_1
    const-string v1, "setHPPortStatusFalse"

    goto :goto_0

    :catch_0
    move-exception v0

    invoke-virtual {v0}, Lcom/mstar/android/tvapi/common/exception/TvCommonException;->printStackTrace()V

    goto :goto_1
.end method

.method public setInputSource(I)V
    .locals 4
    .param p1    # I
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Landroid/os/RemoteException;
        }
    .end annotation

    const-string v1, "TvCommonClient"

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "setAtvChannel(), paras sourcesource = "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, p1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    invoke-static {}, Lcom/mstar/android/tvapi/common/vo/TvOsType$EnumInputSource;->values()[Lcom/mstar/android/tvapi/common/vo/TvOsType$EnumInputSource;

    move-result-object v1

    aget-object v1, v1, p1

    iput-object v1, p0, Lcom/android/server/tv/TvCommonClient;->curSourceType:Lcom/mstar/android/tvapi/common/vo/TvOsType$EnumInputSource;

    sget-object v1, Lcom/mstar/android/tvapi/common/vo/TvOsType$EnumInputSource;->E_INPUT_SOURCE_ATV:Lcom/mstar/android/tvapi/common/vo/TvOsType$EnumInputSource;

    invoke-static {}, Lcom/mstar/android/tvapi/common/vo/TvOsType$EnumInputSource;->values()[Lcom/mstar/android/tvapi/common/vo/TvOsType$EnumInputSource;

    move-result-object v2

    aget-object v2, v2, p1

    if-ne v1, v2, :cond_2

    const/4 v1, 0x0

    sput-boolean v1, Lcom/android/server/tv/TvCommonClient;->isPreviousSourceDTV:Z

    :cond_0
    :goto_0
    :try_start_0
    invoke-static {}, Lcom/mstar/android/tvapi/common/TvManager;->getInstance()Lcom/mstar/android/tvapi/common/TvManager;

    move-result-object v1

    if-eqz v1, :cond_1

    invoke-static {}, Lcom/mstar/android/tvapi/common/TvManager;->getInstance()Lcom/mstar/android/tvapi/common/TvManager;

    move-result-object v1

    invoke-static {}, Lcom/mstar/android/tvapi/common/vo/TvOsType$EnumInputSource;->values()[Lcom/mstar/android/tvapi/common/vo/TvOsType$EnumInputSource;

    move-result-object v2

    aget-object v2, v2, p1

    invoke-virtual {v1, v2}, Lcom/mstar/android/tvapi/common/TvManager;->setInputSource(Lcom/mstar/android/tvapi/common/vo/TvOsType$EnumInputSource;)Z
    :try_end_0
    .catch Lcom/mstar/android/tvapi/common/exception/TvCommonException; {:try_start_0 .. :try_end_0} :catch_0

    :cond_1
    :goto_1
    return-void

    :cond_2
    sget-object v1, Lcom/mstar/android/tvapi/common/vo/TvOsType$EnumInputSource;->E_INPUT_SOURCE_DTV:Lcom/mstar/android/tvapi/common/vo/TvOsType$EnumInputSource;

    invoke-static {}, Lcom/mstar/android/tvapi/common/vo/TvOsType$EnumInputSource;->values()[Lcom/mstar/android/tvapi/common/vo/TvOsType$EnumInputSource;

    move-result-object v2

    aget-object v2, v2, p1

    if-ne v1, v2, :cond_0

    const/4 v1, 0x1

    sput-boolean v1, Lcom/android/server/tv/TvCommonClient;->isPreviousSourceDTV:Z

    goto :goto_0

    :catch_0
    move-exception v0

    invoke-virtual {v0}, Lcom/mstar/android/tvapi/common/exception/TvCommonException;->printStackTrace()V

    goto :goto_1
.end method

.method public setPowerOnSource(I)Z
    .locals 3
    .param p1    # I
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Landroid/os/RemoteException;
        }
    .end annotation

    const-string v0, "TvCommonClient"

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "setPowerOnSource, paras eSource "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    iget-object v0, p0, Lcom/android/server/tv/TvCommonClient;->databaseMgr:Lcom/android/server/tv/DataBaseDeskImpl;

    invoke-virtual {v0, p1}, Lcom/android/server/tv/DataBaseDeskImpl;->updatePowerOnSource(I)V

    const/4 v0, 0x1

    return v0
.end method

.method public setSurfaceView(IIII)V
    .locals 3
    .param p1    # I
    .param p2    # I
    .param p3    # I
    .param p4    # I
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Landroid/os/RemoteException;
        }
    .end annotation

    new-instance v1, Landroid/os/Message;

    invoke-direct {v1}, Landroid/os/Message;-><init>()V

    const/4 v2, 0x6

    iput v2, v1, Landroid/os/Message;->what:I

    new-instance v0, Landroid/os/Bundle;

    invoke-direct {v0}, Landroid/os/Bundle;-><init>()V

    const-string v2, "x"

    invoke-virtual {v0, v2, p1}, Landroid/os/Bundle;->putInt(Ljava/lang/String;I)V

    const-string v2, "y"

    invoke-virtual {v0, v2, p2}, Landroid/os/Bundle;->putInt(Ljava/lang/String;I)V

    const-string v2, "width"

    invoke-virtual {v0, v2, p3}, Landroid/os/Bundle;->putInt(Ljava/lang/String;I)V

    const-string v2, "height"

    invoke-virtual {v0, v2, p4}, Landroid/os/Bundle;->putInt(Ljava/lang/String;I)V

    invoke-virtual {v1, v0}, Landroid/os/Message;->setData(Landroid/os/Bundle;)V

    iget-object v2, p0, Lcom/android/server/tv/TvCommonClient;->mHandler:Landroid/os/Handler;

    invoke-virtual {v2, v1}, Landroid/os/Handler;->sendMessage(Landroid/os/Message;)Z

    return-void
.end method

.method public standbySystem(Ljava/lang/String;)V
    .locals 6
    .param p1    # Ljava/lang/String;

    const/high16 v5, 0x10000000

    const/4 v4, 0x0

    const-string v3, "mstar.str.enable"

    invoke-static {v3, v4}, Landroid/os/SystemProperties;->getBoolean(Ljava/lang/String;Z)Z

    move-result v0

    const/4 v3, 0x1

    if-ne v0, v3, :cond_1

    new-instance v2, Lcom/android/server/tv/TvCommonClient$2;

    invoke-direct {v2, p0}, Lcom/android/server/tv/TvCommonClient$2;-><init>(Lcom/android/server/tv/TvCommonClient;)V

    invoke-virtual {v2}, Ljava/lang/Thread;->start()V

    :cond_0
    :goto_0
    return-void

    :cond_1
    sget-object v3, Lcom/android/server/tv/TvCommonClient;->STANDBYCOMMOND:Ljava/lang/String;

    invoke-virtual {p1, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-eqz v3, :cond_2

    new-instance v1, Landroid/content/Intent;

    const-string v3, "android.intent.action.ACTION_REQUEST_SHUTDOWN"

    invoke-direct {v1, v3}, Landroid/content/Intent;-><init>(Ljava/lang/String;)V

    const-string v3, "android.intent.extra.KEY_CONFIRM"

    invoke-virtual {v1, v3, v4}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Z)Landroid/content/Intent;

    invoke-virtual {v1, v5}, Landroid/content/Intent;->setFlags(I)Landroid/content/Intent;

    iget-object v3, p0, Lcom/android/server/tv/TvCommonClient;->mContext:Landroid/content/Context;

    invoke-virtual {v3, v1}, Landroid/content/Context;->startActivity(Landroid/content/Intent;)V

    goto :goto_0

    :cond_2
    sget-object v3, Lcom/android/server/tv/TvCommonClient;->AUTOSLEEPCOMMOND:Ljava/lang/String;

    invoke-virtual {p1, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-eqz v3, :cond_0

    new-instance v1, Landroid/content/Intent;

    const-string v3, "android.intent.action.ACTION_REQUEST_SHUTDOWN"

    invoke-direct {v1, v3}, Landroid/content/Intent;-><init>(Ljava/lang/String;)V

    const-string v3, "android.intent.extra.KEY_CONFIRM"

    invoke-virtual {v1, v3, v4}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Z)Landroid/content/Intent;

    invoke-virtual {v1, v5}, Landroid/content/Intent;->setFlags(I)Landroid/content/Intent;

    iget-object v3, p0, Lcom/android/server/tv/TvCommonClient;->mContext:Landroid/content/Context;

    invoke-virtual {v3, v1}, Landroid/content/Context;->startActivity(Landroid/content/Intent;)V

    goto :goto_0
.end method
