.class public Lcom/android/server/tv/DeskTvEventListener;
.super Ljava/lang/Object;
.source "DeskTvEventListener.java"

# interfaces
.implements Lcom/mstar/android/tvapi/common/listener/OnTvEventListener;


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/android/server/tv/DeskTvEventListener$1;
    }
.end annotation


# static fields
.field private static TAG:Ljava/lang/String;

.field private static tvEventListener:Lcom/android/server/tv/DeskTvEventListener;


# instance fields
.field final AUTO_SWITCH_INPUT_SOURCE:I

.field final CURRENT_INPUT_SOURCE_PLUG_OUT:I

.field final DISABLE_3D_FORMAT:I

.field final ENABLE_3D_FORMAT:I

.field final EXIT_SUBWIN:I

.field final GOLDEN_LEFT_EYE:I

.field final SWITCH_INPUT_SOURCE:I

.field final SYNC_TIME_ANDROID:I

.field final UPDATE_INPUT_SOURCE_STATUS:I

.field private bossContext:Landroid/content/Context;

.field private bossHandler:Lcom/android/server/tv/TvHanlder;

.field private clients:Ljava/util/HashMap;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/HashMap",
            "<",
            "Ljava/lang/String;",
            "Lcom/mstar/android/tv/ITvEventClient;",
            ">;"
        }
    .end annotation
.end field

.field private inputSource:Lcom/mstar/android/tvapi/common/vo/TvOsType$EnumInputSource;

.field private mTvCommocClient:Lcom/android/server/tv/TvCommonClient;

.field private sourceSwitchLevel:[I

.field sourcestatus:[S


# direct methods
.method static constructor <clinit>()V
    .locals 1

    const/4 v0, 0x0

    sput-object v0, Lcom/android/server/tv/DeskTvEventListener;->tvEventListener:Lcom/android/server/tv/DeskTvEventListener;

    const-string v0, "DeskTvEventListener"

    sput-object v0, Lcom/android/server/tv/DeskTvEventListener;->TAG:Ljava/lang/String;

    return-void
.end method

.method public constructor <init>()V
    .locals 2

    const/4 v1, 0x0

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    iput-object v1, p0, Lcom/android/server/tv/DeskTvEventListener;->clients:Ljava/util/HashMap;

    const/16 v0, 0x111

    iput v0, p0, Lcom/android/server/tv/DeskTvEventListener;->SWITCH_INPUT_SOURCE:I

    const/16 v0, 0x112

    iput v0, p0, Lcom/android/server/tv/DeskTvEventListener;->UPDATE_INPUT_SOURCE_STATUS:I

    const/16 v0, 0x113

    iput v0, p0, Lcom/android/server/tv/DeskTvEventListener;->AUTO_SWITCH_INPUT_SOURCE:I

    const/16 v0, 0x114

    iput v0, p0, Lcom/android/server/tv/DeskTvEventListener;->CURRENT_INPUT_SOURCE_PLUG_OUT:I

    const/16 v0, 0x767

    iput v0, p0, Lcom/android/server/tv/DeskTvEventListener;->ENABLE_3D_FORMAT:I

    const/16 v0, 0x768

    iput v0, p0, Lcom/android/server/tv/DeskTvEventListener;->DISABLE_3D_FORMAT:I

    const/16 v0, 0x789

    iput v0, p0, Lcom/android/server/tv/DeskTvEventListener;->SYNC_TIME_ANDROID:I

    const/16 v0, 0x400

    iput v0, p0, Lcom/android/server/tv/DeskTvEventListener;->GOLDEN_LEFT_EYE:I

    const/16 v0, 0x300

    iput v0, p0, Lcom/android/server/tv/DeskTvEventListener;->EXIT_SUBWIN:I

    iput-object v1, p0, Lcom/android/server/tv/DeskTvEventListener;->sourcestatus:[S

    const/16 v0, 0x27

    new-array v0, v0, [I

    fill-array-data v0, :array_0

    iput-object v0, p0, Lcom/android/server/tv/DeskTvEventListener;->sourceSwitchLevel:[I

    new-instance v0, Ljava/util/HashMap;

    invoke-direct {v0}, Ljava/util/HashMap;-><init>()V

    iput-object v0, p0, Lcom/android/server/tv/DeskTvEventListener;->clients:Ljava/util/HashMap;

    return-void

    nop

    :array_0
    .array-data 4
        0x25
        0x1
        0x2
        0x0
        0x0
        0x0
        0x0
        0x0
        0x0
        0x0
        0x0
        0x0
        0x0
        0x0
        0x0
        0x0
        0x10
        0x0
        0x0
        0x0
        0x0
        0x0
        0x0
        0x26
        0x27
        0x28
        0x0
        0x0
        0x0
        0x0
        0x0
        0x0
        0x0
        0x0
        0x0
        0x0
        0x0
        0x0
        0x0
    .end array-data
.end method

.method public static getInstance()Lcom/android/server/tv/DeskTvEventListener;
    .locals 1

    sget-object v0, Lcom/android/server/tv/DeskTvEventListener;->tvEventListener:Lcom/android/server/tv/DeskTvEventListener;

    if-nez v0, :cond_0

    new-instance v0, Lcom/android/server/tv/DeskTvEventListener;

    invoke-direct {v0}, Lcom/android/server/tv/DeskTvEventListener;-><init>()V

    sput-object v0, Lcom/android/server/tv/DeskTvEventListener;->tvEventListener:Lcom/android/server/tv/DeskTvEventListener;

    :cond_0
    sget-object v0, Lcom/android/server/tv/DeskTvEventListener;->tvEventListener:Lcom/android/server/tv/DeskTvEventListener;

    return-object v0
.end method


# virtual methods
.method public getContext()Landroid/content/Context;
    .locals 1

    iget-object v0, p0, Lcom/android/server/tv/DeskTvEventListener;->bossContext:Landroid/content/Context;

    return-object v0
.end method

.method public getHandler()Lcom/android/server/tv/TvHanlder;
    .locals 1

    iget-object v0, p0, Lcom/android/server/tv/DeskTvEventListener;->bossHandler:Lcom/android/server/tv/TvHanlder;

    return-object v0
.end method

.method public on4k2kHDMIDisableDualView(III)Z
    .locals 1
    .param p1    # I
    .param p2    # I
    .param p3    # I

    const/4 v0, 0x0

    return v0
.end method

.method public on4k2kHDMIDisablePip(III)Z
    .locals 1
    .param p1    # I
    .param p2    # I
    .param p3    # I

    const/4 v0, 0x0

    return v0
.end method

.method public on4k2kHDMIDisablePop(III)Z
    .locals 1
    .param p1    # I
    .param p2    # I
    .param p3    # I

    const/4 v0, 0x0

    return v0
.end method

.method public on4k2kHDMIDisableTravelingMode(III)Z
    .locals 1
    .param p1    # I
    .param p2    # I
    .param p3    # I

    const/4 v0, 0x0

    return v0
.end method

.method public onAtscPopupDialog(III)Z
    .locals 5
    .param p1    # I
    .param p2    # I
    .param p3    # I

    const/4 v4, 0x0

    iget-object v3, p0, Lcom/android/server/tv/DeskTvEventListener;->clients:Ljava/util/HashMap;

    if-nez v3, :cond_1

    :cond_0
    :goto_0
    return v4

    :cond_1
    :try_start_0
    iget-object v3, p0, Lcom/android/server/tv/DeskTvEventListener;->clients:Ljava/util/HashMap;

    invoke-virtual {v3}, Ljava/util/HashMap;->values()Ljava/util/Collection;

    move-result-object v3

    invoke-interface {v3}, Ljava/util/Collection;->iterator()Ljava/util/Iterator;

    move-result-object v2

    :goto_1
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    move-result v3

    if-eqz v3, :cond_0

    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/mstar/android/tv/ITvEventClient;

    invoke-interface {v0, p1, p2, p3}, Lcom/mstar/android/tv/ITvEventClient;->onAtscPopupDialog(III)Z
    :try_end_0
    .catch Landroid/os/RemoteException; {:try_start_0 .. :try_end_0} :catch_0

    goto :goto_1

    :catch_0
    move-exception v1

    invoke-virtual {v1}, Landroid/os/RemoteException;->printStackTrace()V

    goto :goto_0
.end method

.method public onDeadthEvent(III)Z
    .locals 1
    .param p1    # I
    .param p2    # I
    .param p3    # I

    const/4 v0, 0x0

    return v0
.end method

.method public onDtvReadyPopupDialog(III)Z
    .locals 5
    .param p1    # I
    .param p2    # I
    .param p3    # I

    const/4 v4, 0x0

    iget-object v3, p0, Lcom/android/server/tv/DeskTvEventListener;->clients:Ljava/util/HashMap;

    if-nez v3, :cond_1

    :cond_0
    :goto_0
    return v4

    :cond_1
    :try_start_0
    iget-object v3, p0, Lcom/android/server/tv/DeskTvEventListener;->clients:Ljava/util/HashMap;

    invoke-virtual {v3}, Ljava/util/HashMap;->values()Ljava/util/Collection;

    move-result-object v3

    invoke-interface {v3}, Ljava/util/Collection;->iterator()Ljava/util/Iterator;

    move-result-object v2

    :goto_1
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    move-result v3

    if-eqz v3, :cond_0

    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/mstar/android/tv/ITvEventClient;

    invoke-interface {v0, p1, p2, p3}, Lcom/mstar/android/tv/ITvEventClient;->onDtvReadyPopupDialog(III)Z
    :try_end_0
    .catch Landroid/os/RemoteException; {:try_start_0 .. :try_end_0} :catch_0

    goto :goto_1

    :catch_0
    move-exception v1

    invoke-virtual {v1}, Landroid/os/RemoteException;->printStackTrace()V

    goto :goto_0
.end method

.method public onScartMuteOsdMode(I)Z
    .locals 5
    .param p1    # I

    const/4 v4, 0x0

    iget-object v3, p0, Lcom/android/server/tv/DeskTvEventListener;->clients:Ljava/util/HashMap;

    if-nez v3, :cond_1

    :cond_0
    :goto_0
    return v4

    :cond_1
    :try_start_0
    iget-object v3, p0, Lcom/android/server/tv/DeskTvEventListener;->clients:Ljava/util/HashMap;

    invoke-virtual {v3}, Ljava/util/HashMap;->values()Ljava/util/Collection;

    move-result-object v3

    invoke-interface {v3}, Ljava/util/Collection;->iterator()Ljava/util/Iterator;

    move-result-object v2

    :goto_1
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    move-result v3

    if-eqz v3, :cond_0

    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/mstar/android/tv/ITvEventClient;

    invoke-interface {v0, p1}, Lcom/mstar/android/tv/ITvEventClient;->onScartMuteOsdMode(I)Z
    :try_end_0
    .catch Landroid/os/RemoteException; {:try_start_0 .. :try_end_0} :catch_0

    goto :goto_1

    :catch_0
    move-exception v1

    invoke-virtual {v1}, Landroid/os/RemoteException;->printStackTrace()V

    goto :goto_0
.end method

.method public onScreenSaverMode(III)Z
    .locals 5
    .param p1    # I
    .param p2    # I
    .param p3    # I

    const/4 v4, 0x0

    iget-object v3, p0, Lcom/android/server/tv/DeskTvEventListener;->clients:Ljava/util/HashMap;

    if-nez v3, :cond_1

    :cond_0
    :goto_0
    return v4

    :cond_1
    :try_start_0
    iget-object v3, p0, Lcom/android/server/tv/DeskTvEventListener;->clients:Ljava/util/HashMap;

    invoke-virtual {v3}, Ljava/util/HashMap;->values()Ljava/util/Collection;

    move-result-object v3

    invoke-interface {v3}, Ljava/util/Collection;->iterator()Ljava/util/Iterator;

    move-result-object v2

    :goto_1
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    move-result v3

    if-eqz v3, :cond_0

    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/mstar/android/tv/ITvEventClient;

    invoke-interface {v0, p1, p2, p3}, Lcom/mstar/android/tv/ITvEventClient;->onScreenSaverMode(III)Z
    :try_end_0
    .catch Landroid/os/RemoteException; {:try_start_0 .. :try_end_0} :catch_0

    goto :goto_1

    :catch_0
    move-exception v1

    invoke-virtual {v1}, Landroid/os/RemoteException;->printStackTrace()V

    goto :goto_0
.end method

.method public onSignalLock(I)Z
    .locals 5
    .param p1    # I

    const/4 v4, 0x0

    iget-object v3, p0, Lcom/android/server/tv/DeskTvEventListener;->clients:Ljava/util/HashMap;

    if-nez v3, :cond_1

    :cond_0
    :goto_0
    return v4

    :cond_1
    :try_start_0
    iget-object v3, p0, Lcom/android/server/tv/DeskTvEventListener;->clients:Ljava/util/HashMap;

    invoke-virtual {v3}, Ljava/util/HashMap;->values()Ljava/util/Collection;

    move-result-object v3

    invoke-interface {v3}, Ljava/util/Collection;->iterator()Ljava/util/Iterator;

    move-result-object v2

    :goto_1
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    move-result v3

    if-eqz v3, :cond_0

    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/mstar/android/tv/ITvEventClient;

    invoke-interface {v0, p1}, Lcom/mstar/android/tv/ITvEventClient;->onSignalLock(I)Z
    :try_end_0
    .catch Landroid/os/RemoteException; {:try_start_0 .. :try_end_0} :catch_0

    goto :goto_1

    :catch_0
    move-exception v1

    invoke-virtual {v1}, Landroid/os/RemoteException;->printStackTrace()V

    goto :goto_0
.end method

.method public onSignalUnlock(I)Z
    .locals 5
    .param p1    # I

    const/4 v4, 0x0

    iget-object v3, p0, Lcom/android/server/tv/DeskTvEventListener;->clients:Ljava/util/HashMap;

    if-nez v3, :cond_1

    :cond_0
    :goto_0
    return v4

    :cond_1
    :try_start_0
    iget-object v3, p0, Lcom/android/server/tv/DeskTvEventListener;->clients:Ljava/util/HashMap;

    invoke-virtual {v3}, Ljava/util/HashMap;->values()Ljava/util/Collection;

    move-result-object v3

    invoke-interface {v3}, Ljava/util/Collection;->iterator()Ljava/util/Iterator;

    move-result-object v2

    :goto_1
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    move-result v3

    if-eqz v3, :cond_0

    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/mstar/android/tv/ITvEventClient;

    invoke-interface {v0, p1}, Lcom/mstar/android/tv/ITvEventClient;->onSignalUnlock(I)Z
    :try_end_0
    .catch Landroid/os/RemoteException; {:try_start_0 .. :try_end_0} :catch_0

    goto :goto_1

    :catch_0
    move-exception v1

    invoke-virtual {v1}, Landroid/os/RemoteException;->printStackTrace()V

    goto :goto_0
.end method

.method public onUnityEvent(III)Z
    .locals 18
    .param p1    # I
    .param p2    # I
    .param p3    # I

    sget-object v14, Lcom/android/server/tv/DeskTvEventListener;->TAG:Ljava/lang/String;

    new-instance v15, Ljava/lang/StringBuilder;

    invoke-direct {v15}, Ljava/lang/StringBuilder;-><init>()V

    const-string v16, "**********arg0 is "

    invoke-virtual/range {v15 .. v16}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v15

    move/from16 v0, p1

    invoke-virtual {v15, v0}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v15

    const-string v16, "************\n"

    invoke-virtual/range {v15 .. v16}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v15

    invoke-virtual {v15}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v15

    invoke-static {v14, v15}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    sget-object v14, Lcom/android/server/tv/DeskTvEventListener;->TAG:Ljava/lang/String;

    new-instance v15, Ljava/lang/StringBuilder;

    invoke-direct {v15}, Ljava/lang/StringBuilder;-><init>()V

    const-string v16, "**********arg1 is "

    invoke-virtual/range {v15 .. v16}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v15

    move/from16 v0, p2

    invoke-virtual {v15, v0}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v15

    const-string v16, "************\n"

    invoke-virtual/range {v15 .. v16}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v15

    invoke-virtual {v15}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v15

    invoke-static {v14, v15}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    sget-object v14, Lcom/android/server/tv/DeskTvEventListener;->TAG:Ljava/lang/String;

    new-instance v15, Ljava/lang/StringBuilder;

    invoke-direct {v15}, Ljava/lang/StringBuilder;-><init>()V

    const-string v16, "**********arg2 is "

    invoke-virtual/range {v15 .. v16}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v15

    move/from16 v0, p3

    invoke-virtual {v15, v0}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v15

    const-string v16, "************\n"

    invoke-virtual/range {v15 .. v16}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v15

    invoke-virtual {v15}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v15

    invoke-static {v14, v15}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    move-object/from16 v0, p0

    iget-object v14, v0, Lcom/android/server/tv/DeskTvEventListener;->bossContext:Landroid/content/Context;

    invoke-static {v14}, Lcom/android/server/tv/DataBaseDeskImpl;->getDataBaseMgrInstance(Landroid/content/Context;)Lcom/android/server/tv/DataBaseDeskImpl;

    move-result-object v3

    new-instance v14, Lcom/android/server/tv/TvCommonClient;

    move-object/from16 v0, p0

    iget-object v15, v0, Lcom/android/server/tv/DeskTvEventListener;->bossContext:Landroid/content/Context;

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/android/server/tv/DeskTvEventListener;->bossHandler:Lcom/android/server/tv/TvHanlder;

    move-object/from16 v16, v0

    move-object/from16 v0, v16

    invoke-direct {v14, v15, v0, v3}, Lcom/android/server/tv/TvCommonClient;-><init>(Landroid/content/Context;Lcom/android/server/tv/TvHanlder;Lcom/android/server/tv/DataBaseDeskImpl;)V

    move-object/from16 v0, p0

    iput-object v14, v0, Lcom/android/server/tv/DeskTvEventListener;->mTvCommocClient:Lcom/android/server/tv/TvCommonClient;

    const/16 v14, 0x111

    move/from16 v0, p2

    if-ne v14, v0, :cond_3

    invoke-static {}, Lcom/mstar/android/tvapi/common/vo/TvOsType$EnumInputSource;->values()[Lcom/mstar/android/tvapi/common/vo/TvOsType$EnumInputSource;

    move-result-object v14

    aget-object v14, v14, p3

    move-object/from16 v0, p0

    iput-object v14, v0, Lcom/android/server/tv/DeskTvEventListener;->inputSource:Lcom/mstar/android/tvapi/common/vo/TvOsType$EnumInputSource;

    sget-object v2, Lcom/mstar/android/tvapi/common/vo/TvOsType$EnumInputSource;->E_INPUT_SOURCE_NONE:Lcom/mstar/android/tvapi/common/vo/TvOsType$EnumInputSource;

    :try_start_0
    invoke-static {}, Lcom/mstar/android/tvapi/common/TvManager;->getInstance()Lcom/mstar/android/tvapi/common/TvManager;

    move-result-object v14

    invoke-virtual {v14}, Lcom/mstar/android/tvapi/common/TvManager;->getCurrentInputSource()Lcom/mstar/android/tvapi/common/vo/TvOsType$EnumInputSource;
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    move-result-object v2

    :goto_0
    move-object/from16 v0, p0

    iget-object v14, v0, Lcom/android/server/tv/DeskTvEventListener;->inputSource:Lcom/mstar/android/tvapi/common/vo/TvOsType$EnumInputSource;

    invoke-virtual {v14}, Lcom/mstar/android/tvapi/common/vo/TvOsType$EnumInputSource;->ordinal()I

    move-result v14

    sget-object v15, Lcom/mstar/android/tvapi/common/vo/TvOsType$EnumInputSource;->E_INPUT_SOURCE_STORAGE:Lcom/mstar/android/tvapi/common/vo/TvOsType$EnumInputSource;

    invoke-virtual {v15}, Lcom/mstar/android/tvapi/common/vo/TvOsType$EnumInputSource;->ordinal()I

    move-result v15

    if-lt v14, v15, :cond_1

    new-instance v8, Landroid/content/Intent;

    invoke-direct {v8}, Landroid/content/Intent;-><init>()V

    new-instance v1, Landroid/content/ComponentName;

    const-string v14, "com.mstar.ui.activity.main"

    const-string v15, "com.mstar.ui.activity.main.MainActivity"

    invoke-direct {v1, v14, v15}, Landroid/content/ComponentName;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    invoke-virtual {v8, v1}, Landroid/content/Intent;->setComponent(Landroid/content/ComponentName;)Landroid/content/Intent;

    const-string v14, "android.intent.action.VIEW"

    invoke-virtual {v8, v14}, Landroid/content/Intent;->setAction(Ljava/lang/String;)Landroid/content/Intent;

    const/high16 v14, 0x10000000

    invoke-virtual {v8, v14}, Landroid/content/Intent;->setFlags(I)Landroid/content/Intent;

    move-object/from16 v0, p0

    iget-object v14, v0, Lcom/android/server/tv/DeskTvEventListener;->bossContext:Landroid/content/Context;

    invoke-virtual {v14, v8}, Landroid/content/Context;->startActivity(Landroid/content/Intent;)V

    :cond_0
    :goto_1
    const/4 v14, 0x0

    return v14

    :catch_0
    move-exception v4

    invoke-virtual {v4}, Ljava/lang/Exception;->printStackTrace()V

    goto :goto_0

    :cond_1
    invoke-virtual {v2}, Lcom/mstar/android/tvapi/common/vo/TvOsType$EnumInputSource;->ordinal()I

    move-result v14

    sget-object v15, Lcom/mstar/android/tvapi/common/vo/TvOsType$EnumInputSource;->E_INPUT_SOURCE_STORAGE:Lcom/mstar/android/tvapi/common/vo/TvOsType$EnumInputSource;

    invoke-virtual {v15}, Lcom/mstar/android/tvapi/common/vo/TvOsType$EnumInputSource;->ordinal()I

    move-result v15

    if-lt v14, v15, :cond_2

    new-instance v6, Landroid/content/Intent;

    const-string v14, "com.mstar.tvsetting.hotkey.intent.action.ProgressActivity"

    invoke-direct {v6, v14}, Landroid/content/Intent;-><init>(Ljava/lang/String;)V

    const-string v14, "task_tag"

    const-string v15, "input_source_changed"

    invoke-virtual {v6, v14, v15}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    const-string v14, "inputSource"

    invoke-static {}, Lcom/mstar/android/tvapi/common/vo/TvOsType$EnumInputSource;->values()[Lcom/mstar/android/tvapi/common/vo/TvOsType$EnumInputSource;

    move-result-object v15

    aget-object v15, v15, p3

    invoke-virtual {v6, v14, v15}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/io/Serializable;)Landroid/content/Intent;

    const/high16 v14, 0x10000000

    invoke-virtual {v6, v14}, Landroid/content/Intent;->setFlags(I)Landroid/content/Intent;

    :try_start_1
    move-object/from16 v0, p0

    iget-object v14, v0, Lcom/android/server/tv/DeskTvEventListener;->bossContext:Landroid/content/Context;

    invoke-virtual {v14, v6}, Landroid/content/Context;->startActivity(Landroid/content/Intent;)V
    :try_end_1
    .catch Ljava/lang/Exception; {:try_start_1 .. :try_end_1} :catch_1

    goto :goto_1

    :catch_1
    move-exception v4

    invoke-virtual {v4}, Ljava/lang/Exception;->printStackTrace()V

    goto :goto_1

    :cond_2
    :try_start_2
    invoke-static {}, Lcom/mstar/android/tvapi/common/TvManager;->getInstance()Lcom/mstar/android/tvapi/common/TvManager;

    move-result-object v14

    invoke-static {}, Lcom/mstar/android/tvapi/common/vo/TvOsType$EnumInputSource;->values()[Lcom/mstar/android/tvapi/common/vo/TvOsType$EnumInputSource;

    move-result-object v15

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/android/server/tv/DeskTvEventListener;->inputSource:Lcom/mstar/android/tvapi/common/vo/TvOsType$EnumInputSource;

    move-object/from16 v16, v0

    invoke-virtual/range {v16 .. v16}, Lcom/mstar/android/tvapi/common/vo/TvOsType$EnumInputSource;->ordinal()I

    move-result v16

    aget-object v15, v15, v16

    invoke-virtual {v14, v15}, Lcom/mstar/android/tvapi/common/TvManager;->setInputSource(Lcom/mstar/android/tvapi/common/vo/TvOsType$EnumInputSource;)Z
    :try_end_2
    .catch Ljava/lang/Exception; {:try_start_2 .. :try_end_2} :catch_3

    :goto_2
    new-instance v7, Landroid/content/Intent;

    const-string v14, "mstar.tvsetting.ui.intent.action.RootActivity"

    invoke-direct {v7, v14}, Landroid/content/Intent;-><init>(Ljava/lang/String;)V

    const/high16 v14, 0x10000000

    invoke-virtual {v7, v14}, Landroid/content/Intent;->setFlags(I)Landroid/content/Intent;

    :try_start_3
    move-object/from16 v0, p0

    iget-object v14, v0, Lcom/android/server/tv/DeskTvEventListener;->bossContext:Landroid/content/Context;

    invoke-virtual {v14, v7}, Landroid/content/Context;->startActivity(Landroid/content/Intent;)V
    :try_end_3
    .catch Ljava/lang/Exception; {:try_start_3 .. :try_end_3} :catch_2

    goto :goto_1

    :catch_2
    move-exception v4

    invoke-virtual {v4}, Ljava/lang/Exception;->printStackTrace()V

    goto :goto_1

    :catch_3
    move-exception v4

    invoke-virtual {v4}, Ljava/lang/Exception;->printStackTrace()V

    goto :goto_2

    :cond_3
    const/16 v14, 0x767

    move/from16 v0, p2

    if-ne v14, v0, :cond_4

    sget-object v14, Lcom/android/server/tv/DeskTvEventListener$1;->$SwitchMap$com$mstar$android$tvapi$common$vo$Enum3dType:[I

    invoke-static {}, Lcom/mstar/android/tvapi/common/vo/Enum3dType;->values()[Lcom/mstar/android/tvapi/common/vo/Enum3dType;

    move-result-object v15

    aget-object v15, v15, p3

    invoke-virtual {v15}, Lcom/mstar/android/tvapi/common/vo/Enum3dType;->ordinal()I

    move-result v15

    aget v14, v14, v15

    packed-switch v14, :pswitch_data_0

    const-string v14, "aaron"

    const-string v15, "default mode in\n"

    invoke-static {v14, v15}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    const-string v14, "mstar.desk-display-mode"

    const/4 v15, 0x0

    invoke-static {v15}, Ljava/lang/String;->valueOf(I)Ljava/lang/String;

    move-result-object v15

    invoke-static {v14, v15}, Landroid/os/SystemProperties;->set(Ljava/lang/String;Ljava/lang/String;)V

    goto/16 :goto_1

    :pswitch_0
    const-string v14, "aaron"

    const-string v15, "3d mode in\n"

    invoke-static {v14, v15}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    const-string v14, "mstar.desk-display-mode"

    invoke-static/range {p3 .. p3}, Ljava/lang/String;->valueOf(I)Ljava/lang/String;

    move-result-object v15

    invoke-static {v14, v15}, Landroid/os/SystemProperties;->set(Ljava/lang/String;Ljava/lang/String;)V

    goto/16 :goto_1

    :cond_4
    const/16 v14, 0x768

    move/from16 v0, p2

    if-ne v14, v0, :cond_5

    sget-object v14, Lcom/android/server/tv/DeskTvEventListener;->TAG:Ljava/lang/String;

    const-string v15, "#########Disable 3D Mode##########\n"

    invoke-static {v14, v15}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    goto/16 :goto_1

    :cond_5
    const/16 v14, 0x789

    move/from16 v0, p2

    if-ne v14, v0, :cond_6

    if-lez p3, :cond_0

    move/from16 v0, p3

    int-to-long v14, v0

    const-wide/16 v16, 0x3e8

    mul-long v9, v14, v16

    invoke-static {v9, v10}, Landroid/os/SystemClock;->setCurrentTimeMillis(J)Z

    goto/16 :goto_1

    :cond_6
    const/16 v14, 0x300

    move/from16 v0, p2

    if-ne v14, v0, :cond_7

    sget-object v14, Lcom/android/server/tv/DeskTvEventListener;->TAG:Ljava/lang/String;

    const-string v15, "#########Exit Sub Win##########\n"

    invoke-static {v14, v15}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    move-object/from16 v0, p0

    iget-object v14, v0, Lcom/android/server/tv/DeskTvEventListener;->bossHandler:Lcom/android/server/tv/TvHanlder;

    invoke-virtual {v14}, Lcom/android/server/tv/TvHanlder;->obtainMessage()Landroid/os/Message;

    move-result-object v11

    const/4 v14, 0x2

    iput v14, v11, Landroid/os/Message;->what:I

    invoke-virtual {v11}, Landroid/os/Message;->sendToTarget()V

    goto/16 :goto_1

    :cond_7
    const/16 v14, 0x112

    move/from16 v0, p2

    if-ne v14, v0, :cond_8

    :try_start_4
    invoke-static {}, Lcom/mstar/android/tvapi/common/TvManager;->getInstance()Lcom/mstar/android/tvapi/common/TvManager;

    move-result-object v14

    const-string v15, "GetInputSourceStatus"

    invoke-virtual {v14, v15}, Lcom/mstar/android/tvapi/common/TvManager;->setTvosCommonCommand(Ljava/lang/String;)[S

    move-result-object v14

    move-object/from16 v0, p0

    iput-object v14, v0, Lcom/android/server/tv/DeskTvEventListener;->sourcestatus:[S
    :try_end_4
    .catch Ljava/lang/Exception; {:try_start_4 .. :try_end_4} :catch_4

    :goto_3
    move-object/from16 v0, p0

    iget-object v14, v0, Lcom/android/server/tv/DeskTvEventListener;->mTvCommocClient:Lcom/android/server/tv/TvCommonClient;

    move-object/from16 v0, p0

    iget-object v15, v0, Lcom/android/server/tv/DeskTvEventListener;->sourcestatus:[S

    invoke-virtual {v14, v15}, Lcom/android/server/tv/TvCommonClient;->SetInputSourceStatus([S)V

    new-instance v6, Landroid/content/Intent;

    invoke-direct {v6}, Landroid/content/Intent;-><init>()V

    const-string v14, "com.mstar.tv.service.COMMON_EVENT_SIGNAL_STATUS_UPDATE"

    invoke-virtual {v6, v14}, Landroid/content/Intent;->setAction(Ljava/lang/String;)Landroid/content/Intent;

    const-string v14, "SwitchSourceIndex"

    move/from16 v0, p3

    invoke-virtual {v6, v14, v0}, Landroid/content/Intent;->putExtra(Ljava/lang/String;I)Landroid/content/Intent;

    const-string v14, "DeskTvEventListener debug"

    const-string v15, "Broadcast, input source status changes! "

    invoke-static {v14, v15}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    move-object/from16 v0, p0

    iget-object v14, v0, Lcom/android/server/tv/DeskTvEventListener;->bossContext:Landroid/content/Context;

    invoke-virtual {v14, v6}, Landroid/content/Context;->sendBroadcast(Landroid/content/Intent;)V

    goto/16 :goto_1

    :catch_4
    move-exception v4

    invoke-virtual {v4}, Ljava/lang/Exception;->printStackTrace()V

    goto :goto_3

    :cond_8
    const/16 v14, 0x113

    move/from16 v0, p2

    if-ne v14, v0, :cond_9

    :try_start_5
    invoke-static {}, Lcom/mstar/android/tvapi/common/TvManager;->getInstance()Lcom/mstar/android/tvapi/common/TvManager;

    move-result-object v14

    const-string v15, "GetInputSourceStatus"

    invoke-virtual {v14, v15}, Lcom/mstar/android/tvapi/common/TvManager;->setTvosCommonCommand(Ljava/lang/String;)[S

    move-result-object v14

    move-object/from16 v0, p0

    iput-object v14, v0, Lcom/android/server/tv/DeskTvEventListener;->sourcestatus:[S
    :try_end_5
    .catch Ljava/lang/Exception; {:try_start_5 .. :try_end_5} :catch_5

    :goto_4
    move-object/from16 v0, p0

    iget-object v14, v0, Lcom/android/server/tv/DeskTvEventListener;->mTvCommocClient:Lcom/android/server/tv/TvCommonClient;

    move-object/from16 v0, p0

    iget-object v15, v0, Lcom/android/server/tv/DeskTvEventListener;->sourcestatus:[S

    invoke-virtual {v14, v15}, Lcom/android/server/tv/TvCommonClient;->SetInputSourceStatus([S)V

    new-instance v6, Landroid/content/Intent;

    invoke-direct {v6}, Landroid/content/Intent;-><init>()V

    const-string v14, "com.mstar.tv.service.COMMON_EVENT_SIGNAL_AUTO_SWITCH"

    invoke-virtual {v6, v14}, Landroid/content/Intent;->setAction(Ljava/lang/String;)Landroid/content/Intent;

    const-string v14, "SwitchSourceIndex"

    move/from16 v0, p3

    invoke-virtual {v6, v14, v0}, Landroid/content/Intent;->putExtra(Ljava/lang/String;I)Landroid/content/Intent;

    const-string v14, "root broadcast"

    const-string v15, "Broadcast, input source status changes! "

    invoke-static {v14, v15}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    move-object/from16 v0, p0

    iget-object v14, v0, Lcom/android/server/tv/DeskTvEventListener;->bossContext:Landroid/content/Context;

    invoke-virtual {v14, v6}, Landroid/content/Context;->sendBroadcast(Landroid/content/Intent;)V

    goto/16 :goto_1

    :catch_5
    move-exception v4

    invoke-virtual {v4}, Ljava/lang/Exception;->printStackTrace()V

    goto :goto_4

    :cond_9
    const/16 v14, 0x114

    move/from16 v0, p2

    if-ne v14, v0, :cond_e

    :try_start_6
    invoke-static {}, Lcom/mstar/android/tvapi/common/TvManager;->getInstance()Lcom/mstar/android/tvapi/common/TvManager;

    move-result-object v14

    const-string v15, "GetInputSourceStatus"

    invoke-virtual {v14, v15}, Lcom/mstar/android/tvapi/common/TvManager;->setTvosCommonCommand(Ljava/lang/String;)[S

    move-result-object v14

    move-object/from16 v0, p0

    iput-object v14, v0, Lcom/android/server/tv/DeskTvEventListener;->sourcestatus:[S
    :try_end_6
    .catch Ljava/lang/Exception; {:try_start_6 .. :try_end_6} :catch_6

    :goto_5
    move-object/from16 v0, p0

    iget-object v14, v0, Lcom/android/server/tv/DeskTvEventListener;->mTvCommocClient:Lcom/android/server/tv/TvCommonClient;

    move-object/from16 v0, p0

    iget-object v15, v0, Lcom/android/server/tv/DeskTvEventListener;->sourcestatus:[S

    invoke-virtual {v14, v15}, Lcom/android/server/tv/TvCommonClient;->SetInputSourceStatus([S)V

    const/4 v12, -0x1

    const/4 v13, 0x0

    const/4 v5, 0x0

    :goto_6
    sget-object v14, Lcom/mstar/android/tvapi/common/vo/TvOsType$EnumInputSource;->E_INPUT_SOURCE_NUM:Lcom/mstar/android/tvapi/common/vo/TvOsType$EnumInputSource;

    invoke-virtual {v14}, Lcom/mstar/android/tvapi/common/vo/TvOsType$EnumInputSource;->ordinal()I

    move-result v14

    if-ge v5, v14, :cond_b

    move-object/from16 v0, p0

    iget-object v14, v0, Lcom/android/server/tv/DeskTvEventListener;->sourcestatus:[S

    aget-short v14, v14, v5

    if-eqz v14, :cond_a

    move-object/from16 v0, p0

    iget-object v14, v0, Lcom/android/server/tv/DeskTvEventListener;->sourceSwitchLevel:[I

    aget v14, v14, v5

    if-le v14, v13, :cond_a

    move v12, v5

    move-object/from16 v0, p0

    iget-object v14, v0, Lcom/android/server/tv/DeskTvEventListener;->sourceSwitchLevel:[I

    aget v13, v14, v5

    :cond_a
    add-int/lit8 v5, v5, 0x1

    goto :goto_6

    :catch_6
    move-exception v4

    invoke-virtual {v4}, Ljava/lang/Exception;->printStackTrace()V

    goto :goto_5

    :cond_b
    const/4 v14, -0x1

    if-ne v12, v14, :cond_c

    const/4 v14, 0x1

    move-object/from16 v0, p0

    iget-object v15, v0, Lcom/android/server/tv/DeskTvEventListener;->mTvCommocClient:Lcom/android/server/tv/TvCommonClient;

    invoke-virtual {v15}, Lcom/android/server/tv/TvCommonClient;->isPreviousSourceDTV()Z

    move-result v15

    if-ne v14, v15, :cond_d

    sget-object v14, Lcom/mstar/android/tvapi/common/vo/TvOsType$EnumInputSource;->E_INPUT_SOURCE_DTV:Lcom/mstar/android/tvapi/common/vo/TvOsType$EnumInputSource;

    invoke-virtual {v14}, Lcom/mstar/android/tvapi/common/vo/TvOsType$EnumInputSource;->ordinal()I

    move-result v12

    :goto_7
    const/4 v13, 0x1

    :cond_c
    new-instance v6, Landroid/content/Intent;

    invoke-direct {v6}, Landroid/content/Intent;-><init>()V

    const-string v14, "com.mstar.tv.service.COMMON_EVENT_SIGNAL_AUTO_SWITCH"

    invoke-virtual {v6, v14}, Landroid/content/Intent;->setAction(Ljava/lang/String;)Landroid/content/Intent;

    const-string v14, "SwitchSourceIndex"

    invoke-virtual {v6, v14, v12}, Landroid/content/Intent;->putExtra(Ljava/lang/String;I)Landroid/content/Intent;

    const-string v14, "root broadcast"

    const-string v15, "Broadcast, input source status changes! "

    invoke-static {v14, v15}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    move-object/from16 v0, p0

    iget-object v14, v0, Lcom/android/server/tv/DeskTvEventListener;->bossContext:Landroid/content/Context;

    invoke-virtual {v14, v6}, Landroid/content/Context;->sendBroadcast(Landroid/content/Intent;)V

    goto/16 :goto_1

    :cond_d
    sget-object v14, Lcom/mstar/android/tvapi/common/vo/TvOsType$EnumInputSource;->E_INPUT_SOURCE_ATV:Lcom/mstar/android/tvapi/common/vo/TvOsType$EnumInputSource;

    invoke-virtual {v14}, Lcom/mstar/android/tvapi/common/vo/TvOsType$EnumInputSource;->ordinal()I

    move-result v12

    goto :goto_7

    :cond_e
    const/16 v14, 0x400

    move/from16 v0, p2

    if-ne v14, v0, :cond_0

    const-string v14, "TvManagerBinder"

    new-instance v15, Ljava/lang/StringBuilder;

    invoke-direct {v15}, Ljava/lang/StringBuilder;-><init>()V

    const-string v16, "@@@___arg0 is :"

    invoke-virtual/range {v15 .. v16}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v15

    move/from16 v0, p1

    invoke-virtual {v15, v0}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v15

    const-string v16, "___arg2 is :"

    invoke-virtual/range {v15 .. v16}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v15

    move/from16 v0, p3

    invoke-virtual {v15, v0}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v15

    invoke-virtual {v15}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v15

    invoke-static {v14, v15}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    new-instance v6, Landroid/content/Intent;

    invoke-direct {v6}, Landroid/content/Intent;-><init>()V

    const-string v14, "com.mstar.tv.service.GOLDEN_LEFT_EYE"

    invoke-virtual {v6, v14}, Landroid/content/Intent;->setAction(Ljava/lang/String;)Landroid/content/Intent;

    const-string v14, "position_x"

    move/from16 v0, p3

    invoke-virtual {v6, v14, v0}, Landroid/content/Intent;->putExtra(Ljava/lang/String;I)Landroid/content/Intent;

    const-string v14, "root broadcast"

    const-string v15, "Broadcast, position x  changes! "

    invoke-static {v14, v15}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    move-object/from16 v0, p0

    iget-object v14, v0, Lcom/android/server/tv/DeskTvEventListener;->bossContext:Landroid/content/Context;

    invoke-virtual {v14, v6}, Landroid/content/Context;->sendBroadcast(Landroid/content/Intent;)V

    goto/16 :goto_1

    nop

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
        :pswitch_0
        :pswitch_0
    .end packed-switch
.end method

.method public releaseClient(Ljava/lang/String;)V
    .locals 1
    .param p1    # Ljava/lang/String;

    iget-object v0, p0, Lcom/android/server/tv/DeskTvEventListener;->clients:Ljava/util/HashMap;

    invoke-virtual {v0, p1}, Ljava/util/HashMap;->remove(Ljava/lang/Object;)Ljava/lang/Object;

    return-void
.end method

.method public setClient(Ljava/lang/String;Lcom/mstar/android/tv/ITvEventClient;)V
    .locals 1
    .param p1    # Ljava/lang/String;
    .param p2    # Lcom/mstar/android/tv/ITvEventClient;

    iget-object v0, p0, Lcom/android/server/tv/DeskTvEventListener;->clients:Ljava/util/HashMap;

    invoke-virtual {v0, p1, p2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    return-void
.end method

.method public setContext(Landroid/content/Context;)V
    .locals 0
    .param p1    # Landroid/content/Context;

    iput-object p1, p0, Lcom/android/server/tv/DeskTvEventListener;->bossContext:Landroid/content/Context;

    return-void
.end method

.method public setHandler(Lcom/android/server/tv/TvHanlder;)V
    .locals 0
    .param p1    # Lcom/android/server/tv/TvHanlder;

    iput-object p1, p0, Lcom/android/server/tv/DeskTvEventListener;->bossHandler:Lcom/android/server/tv/TvHanlder;

    return-void
.end method
