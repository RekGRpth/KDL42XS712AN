.class public final enum Lcom/android/server/tv/DataBaseDesk$EN_SURROUND_TYPE;
.super Ljava/lang/Enum;
.source "DataBaseDesk.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/android/server/tv/DataBaseDesk;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x4019
    name = "EN_SURROUND_TYPE"
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Enum",
        "<",
        "Lcom/android/server/tv/DataBaseDesk$EN_SURROUND_TYPE;",
        ">;"
    }
.end annotation


# static fields
.field private static final synthetic $VALUES:[Lcom/android/server/tv/DataBaseDesk$EN_SURROUND_TYPE;

.field public static final enum SURROUND_MODE_CHAMPAIGN:Lcom/android/server/tv/DataBaseDesk$EN_SURROUND_TYPE;

.field public static final enum SURROUND_MODE_CITY:Lcom/android/server/tv/DataBaseDesk$EN_SURROUND_TYPE;

.field public static final enum SURROUND_MODE_MOUNTAIN:Lcom/android/server/tv/DataBaseDesk$EN_SURROUND_TYPE;

.field public static final enum SURROUND_MODE_NUM:Lcom/android/server/tv/DataBaseDesk$EN_SURROUND_TYPE;

.field public static final enum SURROUND_MODE_THEATER:Lcom/android/server/tv/DataBaseDesk$EN_SURROUND_TYPE;


# direct methods
.method static constructor <clinit>()V
    .locals 7

    const/4 v6, 0x4

    const/4 v5, 0x3

    const/4 v4, 0x2

    const/4 v3, 0x1

    const/4 v2, 0x0

    new-instance v0, Lcom/android/server/tv/DataBaseDesk$EN_SURROUND_TYPE;

    const-string v1, "SURROUND_MODE_MOUNTAIN"

    invoke-direct {v0, v1, v2}, Lcom/android/server/tv/DataBaseDesk$EN_SURROUND_TYPE;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/android/server/tv/DataBaseDesk$EN_SURROUND_TYPE;->SURROUND_MODE_MOUNTAIN:Lcom/android/server/tv/DataBaseDesk$EN_SURROUND_TYPE;

    new-instance v0, Lcom/android/server/tv/DataBaseDesk$EN_SURROUND_TYPE;

    const-string v1, "SURROUND_MODE_CHAMPAIGN"

    invoke-direct {v0, v1, v3}, Lcom/android/server/tv/DataBaseDesk$EN_SURROUND_TYPE;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/android/server/tv/DataBaseDesk$EN_SURROUND_TYPE;->SURROUND_MODE_CHAMPAIGN:Lcom/android/server/tv/DataBaseDesk$EN_SURROUND_TYPE;

    new-instance v0, Lcom/android/server/tv/DataBaseDesk$EN_SURROUND_TYPE;

    const-string v1, "SURROUND_MODE_CITY"

    invoke-direct {v0, v1, v4}, Lcom/android/server/tv/DataBaseDesk$EN_SURROUND_TYPE;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/android/server/tv/DataBaseDesk$EN_SURROUND_TYPE;->SURROUND_MODE_CITY:Lcom/android/server/tv/DataBaseDesk$EN_SURROUND_TYPE;

    new-instance v0, Lcom/android/server/tv/DataBaseDesk$EN_SURROUND_TYPE;

    const-string v1, "SURROUND_MODE_THEATER"

    invoke-direct {v0, v1, v5}, Lcom/android/server/tv/DataBaseDesk$EN_SURROUND_TYPE;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/android/server/tv/DataBaseDesk$EN_SURROUND_TYPE;->SURROUND_MODE_THEATER:Lcom/android/server/tv/DataBaseDesk$EN_SURROUND_TYPE;

    new-instance v0, Lcom/android/server/tv/DataBaseDesk$EN_SURROUND_TYPE;

    const-string v1, "SURROUND_MODE_NUM"

    invoke-direct {v0, v1, v6}, Lcom/android/server/tv/DataBaseDesk$EN_SURROUND_TYPE;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/android/server/tv/DataBaseDesk$EN_SURROUND_TYPE;->SURROUND_MODE_NUM:Lcom/android/server/tv/DataBaseDesk$EN_SURROUND_TYPE;

    const/4 v0, 0x5

    new-array v0, v0, [Lcom/android/server/tv/DataBaseDesk$EN_SURROUND_TYPE;

    sget-object v1, Lcom/android/server/tv/DataBaseDesk$EN_SURROUND_TYPE;->SURROUND_MODE_MOUNTAIN:Lcom/android/server/tv/DataBaseDesk$EN_SURROUND_TYPE;

    aput-object v1, v0, v2

    sget-object v1, Lcom/android/server/tv/DataBaseDesk$EN_SURROUND_TYPE;->SURROUND_MODE_CHAMPAIGN:Lcom/android/server/tv/DataBaseDesk$EN_SURROUND_TYPE;

    aput-object v1, v0, v3

    sget-object v1, Lcom/android/server/tv/DataBaseDesk$EN_SURROUND_TYPE;->SURROUND_MODE_CITY:Lcom/android/server/tv/DataBaseDesk$EN_SURROUND_TYPE;

    aput-object v1, v0, v4

    sget-object v1, Lcom/android/server/tv/DataBaseDesk$EN_SURROUND_TYPE;->SURROUND_MODE_THEATER:Lcom/android/server/tv/DataBaseDesk$EN_SURROUND_TYPE;

    aput-object v1, v0, v5

    sget-object v1, Lcom/android/server/tv/DataBaseDesk$EN_SURROUND_TYPE;->SURROUND_MODE_NUM:Lcom/android/server/tv/DataBaseDesk$EN_SURROUND_TYPE;

    aput-object v1, v0, v6

    sput-object v0, Lcom/android/server/tv/DataBaseDesk$EN_SURROUND_TYPE;->$VALUES:[Lcom/android/server/tv/DataBaseDesk$EN_SURROUND_TYPE;

    return-void
.end method

.method private constructor <init>(Ljava/lang/String;I)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()V"
        }
    .end annotation

    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    return-void
.end method

.method public static valueOf(Ljava/lang/String;)Lcom/android/server/tv/DataBaseDesk$EN_SURROUND_TYPE;
    .locals 1
    .param p0    # Ljava/lang/String;

    const-class v0, Lcom/android/server/tv/DataBaseDesk$EN_SURROUND_TYPE;

    invoke-static {v0, p0}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;

    move-result-object v0

    check-cast v0, Lcom/android/server/tv/DataBaseDesk$EN_SURROUND_TYPE;

    return-object v0
.end method

.method public static values()[Lcom/android/server/tv/DataBaseDesk$EN_SURROUND_TYPE;
    .locals 1

    sget-object v0, Lcom/android/server/tv/DataBaseDesk$EN_SURROUND_TYPE;->$VALUES:[Lcom/android/server/tv/DataBaseDesk$EN_SURROUND_TYPE;

    invoke-virtual {v0}, [Lcom/android/server/tv/DataBaseDesk$EN_SURROUND_TYPE;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [Lcom/android/server/tv/DataBaseDesk$EN_SURROUND_TYPE;

    return-object v0
.end method
