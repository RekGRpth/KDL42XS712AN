.class public Lcom/android/server/dongle/DongleService;
.super Lcom/mstar/android/tv/IDongleService$Stub;
.source "DongleService.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/android/server/dongle/DongleService$1;,
        Lcom/android/server/dongle/DongleService$AppRequestToDongleReceiver;,
        Lcom/android/server/dongle/DongleService$DongleAckReceiver;
    }
.end annotation


# static fields
.field private static final KONKA_ACTION_REMOTE_DONGLE_VALID_ACK:Ljava/lang/String; = "konka.dongle.REMOTE_DONGLE_VALID_ACK"

.field static final TAG:Ljava/lang/String; = "DongleService"

.field private static mAppRequestToDongleReceiver:Lcom/android/server/dongle/DongleService$AppRequestToDongleReceiver;

.field private static mDongleAckReceiver:Lcom/android/server/dongle/DongleService$DongleAckReceiver;

.field private static mDongleService:Lcom/android/server/dongle/DongleService;


# instance fields
.field private mDongleManager:Lcom/mstar/android/tvapi/common/DongleManager;

.field private mUpdateHandler:Landroid/os/Handler;

.field private packageArray:[B


# direct methods
.method static constructor <clinit>()V
    .locals 1

    const/4 v0, 0x0

    sput-object v0, Lcom/android/server/dongle/DongleService;->mDongleService:Lcom/android/server/dongle/DongleService;

    sput-object v0, Lcom/android/server/dongle/DongleService;->mDongleAckReceiver:Lcom/android/server/dongle/DongleService$DongleAckReceiver;

    sput-object v0, Lcom/android/server/dongle/DongleService;->mAppRequestToDongleReceiver:Lcom/android/server/dongle/DongleService$AppRequestToDongleReceiver;

    return-void
.end method

.method private constructor <init>(Landroid/content/Context;)V
    .locals 5
    .param p1    # Landroid/content/Context;

    const/4 v4, 0x0

    invoke-direct {p0}, Lcom/mstar/android/tv/IDongleService$Stub;-><init>()V

    iput-object v4, p0, Lcom/android/server/dongle/DongleService;->mUpdateHandler:Landroid/os/Handler;

    iput-object v4, p0, Lcom/android/server/dongle/DongleService;->mDongleManager:Lcom/mstar/android/tvapi/common/DongleManager;

    iput-object v4, p0, Lcom/android/server/dongle/DongleService;->packageArray:[B

    const-string v2, "DongleService"

    const-string v3, "new dongle service start!!"

    invoke-static {v2, v3}, Landroid/util/Slog;->d(Ljava/lang/String;Ljava/lang/String;)I

    invoke-static {p1}, Lcom/mstar/android/tvapi/common/DongleManager;->getInstance(Landroid/content/Context;)Lcom/mstar/android/tvapi/common/DongleManager;

    move-result-object v2

    iput-object v2, p0, Lcom/android/server/dongle/DongleService;->mDongleManager:Lcom/mstar/android/tvapi/common/DongleManager;

    iget-object v2, p0, Lcom/android/server/dongle/DongleService;->mDongleManager:Lcom/mstar/android/tvapi/common/DongleManager;

    if-nez v2, :cond_2

    const-string v2, "DongleService"

    const-string v3, "new dongle manager error!!"

    invoke-static {v2, v3}, Landroid/util/Slog;->d(Ljava/lang/String;Ljava/lang/String;)I

    :goto_0
    iget-object v2, p0, Lcom/android/server/dongle/DongleService;->mDongleManager:Lcom/mstar/android/tvapi/common/DongleManager;

    invoke-virtual {v2}, Lcom/mstar/android/tvapi/common/DongleManager;->startMonitor()Z

    sget-object v2, Lcom/android/server/dongle/DongleService;->mDongleAckReceiver:Lcom/android/server/dongle/DongleService$DongleAckReceiver;

    if-nez v2, :cond_0

    new-instance v2, Lcom/android/server/dongle/DongleService$DongleAckReceiver;

    invoke-direct {v2, p0}, Lcom/android/server/dongle/DongleService$DongleAckReceiver;-><init>(Lcom/android/server/dongle/DongleService;)V

    sput-object v2, Lcom/android/server/dongle/DongleService;->mDongleAckReceiver:Lcom/android/server/dongle/DongleService$DongleAckReceiver;

    new-instance v1, Landroid/content/IntentFilter;

    invoke-direct {v1}, Landroid/content/IntentFilter;-><init>()V

    const-string v2, "konka.dongle.REMOTE_DONGLE_VALID_ACK"

    invoke-virtual {v1, v2}, Landroid/content/IntentFilter;->addAction(Ljava/lang/String;)V

    sget-object v2, Lcom/android/server/dongle/DongleService;->mDongleAckReceiver:Lcom/android/server/dongle/DongleService$DongleAckReceiver;

    invoke-virtual {p1, v2, v1}, Landroid/content/Context;->registerReceiver(Landroid/content/BroadcastReceiver;Landroid/content/IntentFilter;)Landroid/content/Intent;

    :cond_0
    sget-object v2, Lcom/android/server/dongle/DongleService;->mAppRequestToDongleReceiver:Lcom/android/server/dongle/DongleService$AppRequestToDongleReceiver;

    if-nez v2, :cond_1

    new-instance v2, Lcom/android/server/dongle/DongleService$AppRequestToDongleReceiver;

    invoke-direct {v2, p0, v4}, Lcom/android/server/dongle/DongleService$AppRequestToDongleReceiver;-><init>(Lcom/android/server/dongle/DongleService;Lcom/android/server/dongle/DongleService$1;)V

    sput-object v2, Lcom/android/server/dongle/DongleService;->mAppRequestToDongleReceiver:Lcom/android/server/dongle/DongleService$AppRequestToDongleReceiver;

    new-instance v0, Landroid/content/IntentFilter;

    invoke-direct {v0}, Landroid/content/IntentFilter;-><init>()V

    const-string v2, "konka.action.SWITCH_KEHWIN_DEVICE_TO_INFRARED"

    invoke-virtual {v0, v2}, Landroid/content/IntentFilter;->addAction(Ljava/lang/String;)V

    const-string v2, "konka.action.SWITCH_KEHWIN_DEVICE_TO_24G"

    invoke-virtual {v0, v2}, Landroid/content/IntentFilter;->addAction(Ljava/lang/String;)V

    sget-object v2, Lcom/android/server/dongle/DongleService;->mAppRequestToDongleReceiver:Lcom/android/server/dongle/DongleService$AppRequestToDongleReceiver;

    invoke-virtual {p1, v2, v0}, Landroid/content/Context;->registerReceiver(Landroid/content/BroadcastReceiver;Landroid/content/IntentFilter;)Landroid/content/Intent;

    :cond_1
    const-string v2, "DongleService"

    const-string v3, "new dongle service end!!"

    invoke-static {v2, v3}, Landroid/util/Slog;->d(Ljava/lang/String;Ljava/lang/String;)I

    return-void

    :cond_2
    const-string v2, "DongleService"

    const-string v3, "new dongle manager OK!!"

    invoke-static {v2, v3}, Landroid/util/Slog;->d(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_0
.end method

.method public static main(Landroid/content/Context;)Lcom/android/server/dongle/DongleService;
    .locals 1
    .param p0    # Landroid/content/Context;

    sget-object v0, Lcom/android/server/dongle/DongleService;->mDongleService:Lcom/android/server/dongle/DongleService;

    if-nez v0, :cond_0

    new-instance v0, Lcom/android/server/dongle/DongleService;

    invoke-direct {v0, p0}, Lcom/android/server/dongle/DongleService;-><init>(Landroid/content/Context;)V

    sput-object v0, Lcom/android/server/dongle/DongleService;->mDongleService:Lcom/android/server/dongle/DongleService;

    :cond_0
    sget-object v0, Lcom/android/server/dongle/DongleService;->mDongleService:Lcom/android/server/dongle/DongleService;

    return-object v0
.end method


# virtual methods
.method public bAgreeCnDevice(ZIII)Z
    .locals 1
    .param p1    # Z
    .param p2    # I
    .param p3    # I
    .param p4    # I
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Landroid/os/RemoteException;
        }
    .end annotation

    iget-object v0, p0, Lcom/android/server/dongle/DongleService;->mDongleManager:Lcom/mstar/android/tvapi/common/DongleManager;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/android/server/dongle/DongleService;->mDongleManager:Lcom/mstar/android/tvapi/common/DongleManager;

    invoke-virtual {v0, p1, p2, p3, p4}, Lcom/mstar/android/tvapi/common/DongleManager;->dM_bAgreeCnDevice(ZIII)Z

    move-result v0

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public getCnDeviceInfo(I)Z
    .locals 1
    .param p1    # I
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Landroid/os/RemoteException;
        }
    .end annotation

    const/4 v0, 0x0

    return v0
.end method

.method public getDongleSwVersion(I)Z
    .locals 1
    .param p1    # I
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Landroid/os/RemoteException;
        }
    .end annotation

    const/4 v0, 0x0

    return v0
.end method

.method public resetAllSystem()Z
    .locals 1
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Landroid/os/RemoteException;
        }
    .end annotation

    const/4 v0, 0x0

    return v0
.end method

.method public searchCnDevice(II)Z
    .locals 1
    .param p1    # I
    .param p2    # I
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Landroid/os/RemoteException;
        }
    .end annotation

    const/4 v0, 0x0

    return v0
.end method

.method public sendUpgradePackage(Lcom/mstar/android/tv/PackageParcel;)Z
    .locals 3
    .param p1    # Lcom/mstar/android/tv/PackageParcel;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Landroid/os/RemoteException;
        }
    .end annotation

    const-string v0, "DongleService"

    const-string v1, "service : send Upgrade Package"

    invoke-static {v0, v1}, Landroid/util/Slog;->d(Ljava/lang/String;Ljava/lang/String;)I

    iget-object v0, p0, Lcom/android/server/dongle/DongleService;->mDongleManager:Lcom/mstar/android/tvapi/common/DongleManager;

    if-eqz v0, :cond_1

    if-nez p1, :cond_0

    const-string v0, "DongleService"

    const-string v1, "PackageParcel is null"

    invoke-static {v0, v1}, Landroid/util/Slog;->d(Ljava/lang/String;Ljava/lang/String;)I

    :cond_0
    invoke-virtual {p1}, Lcom/mstar/android/tv/PackageParcel;->getPackageArray()[B

    move-result-object v0

    iput-object v0, p0, Lcom/android/server/dongle/DongleService;->packageArray:[B

    const-string v0, "DongleService"

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "access packageArray.length ===="

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget-object v2, p0, Lcom/android/server/dongle/DongleService;->packageArray:[B

    array-length v2, v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Landroid/util/Slog;->d(Ljava/lang/String;Ljava/lang/String;)I

    iget-object v0, p0, Lcom/android/server/dongle/DongleService;->mDongleManager:Lcom/mstar/android/tvapi/common/DongleManager;

    iget-object v1, p0, Lcom/android/server/dongle/DongleService;->packageArray:[B

    invoke-virtual {v0, v1}, Lcom/mstar/android/tvapi/common/DongleManager;->dM_SendUpgradePackage([B)Z

    move-result v0

    :goto_0
    return v0

    :cond_1
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public setAudioTrack(II)Z
    .locals 1
    .param p1    # I
    .param p2    # I
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Landroid/os/RemoteException;
        }
    .end annotation

    const/4 v0, 0x0

    return v0
.end method

.method public setAudioVolume(II)Z
    .locals 1
    .param p1    # I
    .param p2    # I
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Landroid/os/RemoteException;
        }
    .end annotation

    const/4 v0, 0x0

    return v0
.end method

.method public setPowerOffMode(I)Z
    .locals 5
    .param p1    # I
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Landroid/os/RemoteException;
        }
    .end annotation

    const-string v3, "DongleService"

    const-string v4, "service : set power off mode"

    invoke-static {v3, v4}, Landroid/util/Slog;->d(Ljava/lang/String;Ljava/lang/String;)I

    const/4 v2, 0x1

    iget-object v3, p0, Lcom/android/server/dongle/DongleService;->mDongleManager:Lcom/mstar/android/tvapi/common/DongleManager;

    if-eqz v3, :cond_2

    iget-object v3, p0, Lcom/android/server/dongle/DongleService;->mDongleManager:Lcom/mstar/android/tvapi/common/DongleManager;

    invoke-virtual {v3, p1}, Lcom/mstar/android/tvapi/common/DongleManager;->dM_SetPowerOffMode(I)Z

    move-result v2

    const/4 v0, 0x5

    :goto_0
    if-lez v0, :cond_1

    const-wide/16 v3, 0xc8

    :try_start_0
    invoke-static {v3, v4}, Ljava/lang/Thread;->sleep(J)V
    :try_end_0
    .catch Ljava/lang/InterruptedException; {:try_start_0 .. :try_end_0} :catch_0

    :goto_1
    sget-object v3, Lcom/android/server/dongle/DongleService;->mDongleAckReceiver:Lcom/android/server/dongle/DongleService$DongleAckReceiver;

    invoke-virtual {v3}, Lcom/android/server/dongle/DongleService$DongleAckReceiver;->getPowerModeSettingAck()Z

    move-result v3

    const/4 v4, 0x1

    if-ne v3, v4, :cond_0

    sget-object v3, Lcom/android/server/dongle/DongleService;->mDongleAckReceiver:Lcom/android/server/dongle/DongleService$DongleAckReceiver;

    invoke-virtual {v3}, Lcom/android/server/dongle/DongleService$DongleAckReceiver;->clearPowerModeSettingAck()V

    move v3, v2

    :goto_2
    return v3

    :catch_0
    move-exception v1

    invoke-virtual {v1}, Ljava/lang/InterruptedException;->printStackTrace()V

    goto :goto_1

    :cond_0
    iget-object v3, p0, Lcom/android/server/dongle/DongleService;->mDongleManager:Lcom/mstar/android/tvapi/common/DongleManager;

    invoke-virtual {v3, p1}, Lcom/mstar/android/tvapi/common/DongleManager;->dM_SetPowerOffMode(I)Z

    move-result v2

    add-int/lit8 v0, v0, -0x1

    goto :goto_0

    :cond_1
    move v3, v2

    goto :goto_2

    :cond_2
    const/4 v3, 0x0

    goto :goto_2
.end method

.method public setPowerOnTime(J)Z
    .locals 1
    .param p1    # J
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Landroid/os/RemoteException;
        }
    .end annotation

    iget-object v0, p0, Lcom/android/server/dongle/DongleService;->mDongleManager:Lcom/mstar/android/tvapi/common/DongleManager;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/android/server/dongle/DongleService;->mDongleManager:Lcom/mstar/android/tvapi/common/DongleManager;

    invoke-virtual {v0, p1, p2}, Lcom/mstar/android/tvapi/common/DongleManager;->dM_SetPowerOnTime(J)Z

    move-result v0

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public startUpgradeDongle()Z
    .locals 2
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Landroid/os/RemoteException;
        }
    .end annotation

    const-string v0, "DongleService"

    const-string v1, "service : send Upgrade dongle"

    invoke-static {v0, v1}, Landroid/util/Slog;->d(Ljava/lang/String;Ljava/lang/String;)I

    iget-object v0, p0, Lcom/android/server/dongle/DongleService;->mDongleManager:Lcom/mstar/android/tvapi/common/DongleManager;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/android/server/dongle/DongleService;->mDongleManager:Lcom/mstar/android/tvapi/common/DongleManager;

    invoke-virtual {v0}, Lcom/mstar/android/tvapi/common/DongleManager;->dM_StartUpgradeDongle()Z

    move-result v0

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public switchRemoteCtrMode(I)Z
    .locals 5
    .param p1    # I
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Landroid/os/RemoteException;
        }
    .end annotation

    const-string v3, "DongleService"

    const-string v4, "service : switchRemoteCtrMode"

    invoke-static {v3, v4}, Landroid/util/Slog;->d(Ljava/lang/String;Ljava/lang/String;)I

    iget-object v3, p0, Lcom/android/server/dongle/DongleService;->mDongleManager:Lcom/mstar/android/tvapi/common/DongleManager;

    if-eqz v3, :cond_2

    const/4 v2, 0x1

    iget-object v3, p0, Lcom/android/server/dongle/DongleService;->mDongleManager:Lcom/mstar/android/tvapi/common/DongleManager;

    invoke-virtual {v3, p1}, Lcom/mstar/android/tvapi/common/DongleManager;->dM_switchRemoteCtrMode(I)Z

    move-result v2

    const/4 v0, 0x5

    :goto_0
    if-lez v0, :cond_0

    const-wide/16 v3, 0xc8

    :try_start_0
    invoke-static {v3, v4}, Ljava/lang/Thread;->sleep(J)V
    :try_end_0
    .catch Ljava/lang/InterruptedException; {:try_start_0 .. :try_end_0} :catch_0

    :goto_1
    sget-object v3, Lcom/android/server/dongle/DongleService;->mDongleAckReceiver:Lcom/android/server/dongle/DongleService$DongleAckReceiver;

    invoke-virtual {v3}, Lcom/android/server/dongle/DongleService$DongleAckReceiver;->getCtrlSwitchModeSettingAck()Z

    move-result v3

    const/4 v4, 0x1

    if-ne v3, v4, :cond_1

    sget-object v3, Lcom/android/server/dongle/DongleService;->mDongleAckReceiver:Lcom/android/server/dongle/DongleService$DongleAckReceiver;

    invoke-virtual {v3}, Lcom/android/server/dongle/DongleService$DongleAckReceiver;->clearCtrlSwitchModeSettingAck()V

    :cond_0
    :goto_2
    return v2

    :catch_0
    move-exception v1

    invoke-virtual {v1}, Ljava/lang/InterruptedException;->printStackTrace()V

    goto :goto_1

    :cond_1
    iget-object v3, p0, Lcom/android/server/dongle/DongleService;->mDongleManager:Lcom/mstar/android/tvapi/common/DongleManager;

    invoke-virtual {v3, p1}, Lcom/mstar/android/tvapi/common/DongleManager;->dM_switchRemoteCtrMode(I)Z

    move-result v2

    add-int/lit8 v0, v0, -0x1

    goto :goto_0

    :cond_2
    const/4 v2, 0x0

    goto :goto_2
.end method

.method public upgradeDongleSw(I)Z
    .locals 1
    .param p1    # I
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Landroid/os/RemoteException;
        }
    .end annotation

    const/4 v0, 0x0

    return v0
.end method
