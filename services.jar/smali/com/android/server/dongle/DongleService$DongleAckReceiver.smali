.class Lcom/android/server/dongle/DongleService$DongleAckReceiver;
.super Landroid/content/BroadcastReceiver;
.source "DongleService.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/android/server/dongle/DongleService;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x2
    name = "DongleAckReceiver"
.end annotation


# static fields
.field private static final DEVICE_PACKAGE_FROM_DONGLE_TO_TV:I = 0x4

.field private static final DEVICE_PACKAGE_FROM_TV_TO_DONGLE:I = 0x3

.field private static final PM_PACKAGE_FROM_DONGLE_TO_TV:I = 0x6

.field private static final PM_PACKAGE_FROM_TV_TO_DONGLE:I = 0x5

.field private static final SOFTWARE_PACKAGE_FROM_DONGLE_TO_TV:I = 0x8

.field private static final SOFTWARE_PACKAGE_FROM_TV_TO_DONGLE:I = 0x7

.field private static final VOLUME_PACKAGE_FROM_DONGLE_TO_TV:I = 0x2

.field private static final VOLUME_PACKAGE_FROM_TV_TO_DONGLE:I = 0x1


# instance fields
.field private CtrlSwitchModeSettingAck:Z

.field private PowerModeSettingAck:Z

.field final synthetic this$0:Lcom/android/server/dongle/DongleService;


# direct methods
.method public constructor <init>(Lcom/android/server/dongle/DongleService;)V
    .locals 0

    iput-object p1, p0, Lcom/android/server/dongle/DongleService$DongleAckReceiver;->this$0:Lcom/android/server/dongle/DongleService;

    invoke-direct {p0}, Landroid/content/BroadcastReceiver;-><init>()V

    invoke-virtual {p0}, Lcom/android/server/dongle/DongleService$DongleAckReceiver;->clearPowerModeSettingAck()V

    invoke-virtual {p0}, Lcom/android/server/dongle/DongleService$DongleAckReceiver;->clearCtrlSwitchModeSettingAck()V

    return-void
.end method

.method private dongleAckAnalysis([B)V
    .locals 10
    .param p1    # [B

    const/4 v9, 0x5

    const/4 v8, 0x1

    const/4 v7, 0x0

    const/4 v6, 0x4

    const/4 v0, 0x0

    const/4 v1, 0x0

    const/4 v2, 0x0

    :goto_0
    array-length v3, p1

    if-ge v2, v3, :cond_0

    const-string v3, "xieshujia"

    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    const-string v5, "DongleAckData ["

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v4

    const-string v5, "] = "

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    aget-byte v5, p1, v2

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-static {v3, v4}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    add-int/lit8 v2, v2, 0x1

    goto :goto_0

    :cond_0
    aget-byte v3, p1, v7

    and-int/lit8 v0, v3, 0x1f

    aget-byte v3, p1, v7

    and-int/lit8 v3, v3, 0x60

    shr-int/lit8 v1, v3, 0x5

    const-string v3, "xieshujia"

    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    const-string v5, "ackPackageType = "

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4, v0}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-static {v3, v4}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    const-string v3, "xieshujia"

    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    const-string v5, "ackValue = "

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-static {v3, v4}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    packed-switch v0, :pswitch_data_0

    :goto_1
    :pswitch_0
    return-void

    :pswitch_1
    invoke-virtual {p0}, Lcom/android/server/dongle/DongleService$DongleAckReceiver;->clearPowerModeSettingAck()V

    aget-byte v3, p1, v6

    if-ne v3, v6, :cond_1

    aget-byte v3, p1, v9

    and-int/lit8 v3, v3, 0x4

    if-ne v3, v6, :cond_1

    invoke-direct {p0, v8}, Lcom/android/server/dongle/DongleService$DongleAckReceiver;->setPowerModeSettingAck(Z)V

    goto :goto_1

    :cond_1
    invoke-direct {p0, v7}, Lcom/android/server/dongle/DongleService$DongleAckReceiver;->setPowerModeSettingAck(Z)V

    goto :goto_1

    :pswitch_2
    invoke-virtual {p0}, Lcom/android/server/dongle/DongleService$DongleAckReceiver;->clearCtrlSwitchModeSettingAck()V

    aget-byte v3, p1, v6

    const/16 v4, 0x9

    if-ne v3, v4, :cond_2

    aget-byte v3, p1, v9

    and-int/lit8 v3, v3, 0x4

    if-ne v3, v6, :cond_2

    const/4 v3, 0x6

    aget-byte v3, p1, v3

    const/4 v4, 0x2

    if-ne v3, v4, :cond_2

    invoke-direct {p0, v8}, Lcom/android/server/dongle/DongleService$DongleAckReceiver;->setCtrlSwitchModeSettingAck(Z)V

    goto :goto_1

    :cond_2
    invoke-direct {p0, v7}, Lcom/android/server/dongle/DongleService$DongleAckReceiver;->setCtrlSwitchModeSettingAck(Z)V

    goto :goto_1

    nop

    :pswitch_data_0
    .packed-switch 0x4
        :pswitch_2
        :pswitch_0
        :pswitch_1
    .end packed-switch
.end method

.method private setCtrlSwitchModeSettingAck(Z)V
    .locals 0
    .param p1    # Z

    iput-boolean p1, p0, Lcom/android/server/dongle/DongleService$DongleAckReceiver;->CtrlSwitchModeSettingAck:Z

    return-void
.end method

.method private setPowerModeSettingAck(Z)V
    .locals 0
    .param p1    # Z

    iput-boolean p1, p0, Lcom/android/server/dongle/DongleService$DongleAckReceiver;->PowerModeSettingAck:Z

    return-void
.end method


# virtual methods
.method public clearCtrlSwitchModeSettingAck()V
    .locals 1

    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/android/server/dongle/DongleService$DongleAckReceiver;->CtrlSwitchModeSettingAck:Z

    return-void
.end method

.method public clearPowerModeSettingAck()V
    .locals 1

    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/android/server/dongle/DongleService$DongleAckReceiver;->PowerModeSettingAck:Z

    return-void
.end method

.method public getCtrlSwitchModeSettingAck()Z
    .locals 1

    iget-boolean v0, p0, Lcom/android/server/dongle/DongleService$DongleAckReceiver;->CtrlSwitchModeSettingAck:Z

    return v0
.end method

.method public getPowerModeSettingAck()Z
    .locals 1

    iget-boolean v0, p0, Lcom/android/server/dongle/DongleService$DongleAckReceiver;->PowerModeSettingAck:Z

    return v0
.end method

.method public onReceive(Landroid/content/Context;Landroid/content/Intent;)V
    .locals 4
    .param p1    # Landroid/content/Context;
    .param p2    # Landroid/content/Intent;

    const-string v2, "xieshujia"

    const-string v3, "DongleAckReceiver"

    invoke-static {v2, v3}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    invoke-virtual {p2}, Landroid/content/Intent;->getExtras()Landroid/os/Bundle;

    move-result-object v1

    const-string v2, "DongleValidAck"

    invoke-virtual {v1, v2}, Landroid/os/Bundle;->getByteArray(Ljava/lang/String;)[B

    move-result-object v0

    invoke-direct {p0, v0}, Lcom/android/server/dongle/DongleService$DongleAckReceiver;->dongleAckAnalysis([B)V

    return-void
.end method
