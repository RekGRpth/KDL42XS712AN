.class Lcom/android/server/dongle/DongleService$AppRequestToDongleReceiver;
.super Landroid/content/BroadcastReceiver;
.source "DongleService.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/android/server/dongle/DongleService;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x2
    name = "AppRequestToDongleReceiver"
.end annotation


# instance fields
.field final synthetic this$0:Lcom/android/server/dongle/DongleService;


# direct methods
.method private constructor <init>(Lcom/android/server/dongle/DongleService;)V
    .locals 0

    iput-object p1, p0, Lcom/android/server/dongle/DongleService$AppRequestToDongleReceiver;->this$0:Lcom/android/server/dongle/DongleService;

    invoke-direct {p0}, Landroid/content/BroadcastReceiver;-><init>()V

    return-void
.end method

.method synthetic constructor <init>(Lcom/android/server/dongle/DongleService;Lcom/android/server/dongle/DongleService$1;)V
    .locals 0
    .param p1    # Lcom/android/server/dongle/DongleService;
    .param p2    # Lcom/android/server/dongle/DongleService$1;

    invoke-direct {p0, p1}, Lcom/android/server/dongle/DongleService$AppRequestToDongleReceiver;-><init>(Lcom/android/server/dongle/DongleService;)V

    return-void
.end method


# virtual methods
.method public onReceive(Landroid/content/Context;Landroid/content/Intent;)V
    .locals 3
    .param p1    # Landroid/content/Context;
    .param p2    # Landroid/content/Intent;

    const-string v1, "xieshujia"

    const-string v2, "AppRequesterToDongleReceiver"

    invoke-static {v1, v2}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    invoke-virtual {p2}, Landroid/content/Intent;->getAction()Ljava/lang/String;

    move-result-object v1

    const-string v2, "konka.action.SWITCH_KEHWIN_DEVICE_TO_24G"

    invoke-virtual {v1, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_0

    :try_start_0
    iget-object v1, p0, Lcom/android/server/dongle/DongleService$AppRequestToDongleReceiver;->this$0:Lcom/android/server/dongle/DongleService;

    const/4 v2, 0x0

    invoke-virtual {v1, v2}, Lcom/android/server/dongle/DongleService;->switchRemoteCtrMode(I)Z
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    :cond_0
    :goto_0
    invoke-virtual {p2}, Landroid/content/Intent;->getAction()Ljava/lang/String;

    move-result-object v1

    const-string v2, "konka.action.SWITCH_KEHWIN_DEVICE_TO_INFRARED"

    invoke-virtual {v1, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_1

    :try_start_1
    iget-object v1, p0, Lcom/android/server/dongle/DongleService$AppRequestToDongleReceiver;->this$0:Lcom/android/server/dongle/DongleService;

    const/4 v2, 0x1

    invoke-virtual {v1, v2}, Lcom/android/server/dongle/DongleService;->switchRemoteCtrMode(I)Z
    :try_end_1
    .catch Ljava/lang/Exception; {:try_start_1 .. :try_end_1} :catch_1

    :cond_1
    :goto_1
    return-void

    :catch_0
    move-exception v0

    invoke-virtual {v0}, Ljava/lang/Exception;->printStackTrace()V

    goto :goto_0

    :catch_1
    move-exception v0

    invoke-virtual {v0}, Ljava/lang/Exception;->printStackTrace()V

    goto :goto_1
.end method
