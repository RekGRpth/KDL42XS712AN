.class Lcom/android/server/MountService$ISOFileMountInfo;
.super Ljava/lang/Object;
.source "MountService.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/android/server/MountService;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = "ISOFileMountInfo"
.end annotation


# instance fields
.field private isoFileMountPath:Ljava/lang/String;

.field private isoFilePath:Ljava/lang/String;

.field private nonce:I

.field private observer:Landroid/os/storage/IISOActionListener;

.field final synthetic this$0:Lcom/android/server/MountService;


# direct methods
.method public constructor <init>(Lcom/android/server/MountService;Ljava/lang/String;Ljava/lang/String;Landroid/os/storage/IISOActionListener;I)V
    .locals 0
    .param p2    # Ljava/lang/String;
    .param p3    # Ljava/lang/String;
    .param p4    # Landroid/os/storage/IISOActionListener;
    .param p5    # I

    iput-object p1, p0, Lcom/android/server/MountService$ISOFileMountInfo;->this$0:Lcom/android/server/MountService;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    iput-object p2, p0, Lcom/android/server/MountService$ISOFileMountInfo;->isoFilePath:Ljava/lang/String;

    iput-object p3, p0, Lcom/android/server/MountService$ISOFileMountInfo;->isoFileMountPath:Ljava/lang/String;

    iput-object p4, p0, Lcom/android/server/MountService$ISOFileMountInfo;->observer:Landroid/os/storage/IISOActionListener;

    iput p5, p0, Lcom/android/server/MountService$ISOFileMountInfo;->nonce:I

    return-void
.end method


# virtual methods
.method public getFileMountPath()Ljava/lang/String;
    .locals 1

    iget-object v0, p0, Lcom/android/server/MountService$ISOFileMountInfo;->isoFileMountPath:Ljava/lang/String;

    return-object v0
.end method

.method public getISOActionListener()Landroid/os/storage/IISOActionListener;
    .locals 1

    iget-object v0, p0, Lcom/android/server/MountService$ISOFileMountInfo;->observer:Landroid/os/storage/IISOActionListener;

    return-object v0
.end method

.method public getISOFilePath()Ljava/lang/String;
    .locals 1

    iget-object v0, p0, Lcom/android/server/MountService$ISOFileMountInfo;->isoFilePath:Ljava/lang/String;

    return-object v0
.end method

.method public getNonce()I
    .locals 1

    iget v0, p0, Lcom/android/server/MountService$ISOFileMountInfo;->nonce:I

    return v0
.end method
