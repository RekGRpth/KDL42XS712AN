.class public Lcom/android/musicvis/vis5/ScriptC_many;
.super Landroid/renderscript/ScriptC;
.source "ScriptC_many.java"


# instance fields
.field private __ALLOCATION:Landroid/renderscript/Element;

.field private __F32:Landroid/renderscript/Element;

.field private __I32:Landroid/renderscript/Element;

.field private __MESH:Landroid/renderscript/Element;

.field private __PROGRAM_FRAGMENT:Landroid/renderscript/Element;

.field private __PROGRAM_RASTER:Landroid/renderscript/Element;

.field private __PROGRAM_STORE:Landroid/renderscript/Element;

.field private __PROGRAM_VERTEX:Landroid/renderscript/Element;

.field private mExportVar_autorotation:F

.field private mExportVar_fadeincounter:I

.field private mExportVar_fadeoutcounter:I

.field private mExportVar_gAngle:F

.field private mExportVar_gCubeMesh:Landroid/renderscript/Mesh;

.field private mExportVar_gIdle:I

.field private mExportVar_gPFBackgroundMip:Landroid/renderscript/ProgramFragment;

.field private mExportVar_gPFBackgroundNoMip:Landroid/renderscript/ProgramFragment;

.field private mExportVar_gPFSBackground:Landroid/renderscript/ProgramStore;

.field private mExportVar_gPR:Landroid/renderscript/ProgramRaster;

.field private mExportVar_gPVBackground:Landroid/renderscript/ProgramVertex;

.field private mExportVar_gPeak:I

.field private mExportVar_gPointBuffer:Landroid/renderscript/Allocation;

.field private mExportVar_gPoints:Lcom/android/musicvis/vis5/ScriptField_Vertex;

.field private mExportVar_gRotate:F

.field private mExportVar_gTilt:F

.field private mExportVar_gTlinetexture:Landroid/renderscript/Allocation;

.field private mExportVar_gTvumeter_album:Landroid/renderscript/Allocation;

.field private mExportVar_gTvumeter_background:Landroid/renderscript/Allocation;

.field private mExportVar_gTvumeter_black:Landroid/renderscript/Allocation;

.field private mExportVar_gTvumeter_frame:Landroid/renderscript/Allocation;

.field private mExportVar_gTvumeter_needle:Landroid/renderscript/Allocation;

.field private mExportVar_gTvumeter_peak_off:Landroid/renderscript/Allocation;

.field private mExportVar_gTvumeter_peak_on:Landroid/renderscript/Allocation;

.field private mExportVar_gWaveCounter:I

.field private mExportVar_lastuptime:I

.field private mExportVar_wave1amp:I

.field private mExportVar_wave1pos:I

.field private mExportVar_wave2amp:I

.field private mExportVar_wave2pos:I

.field private mExportVar_wave3amp:I

.field private mExportVar_wave3pos:I

.field private mExportVar_wave4amp:I

.field private mExportVar_wave4pos:I

.field private mExportVar_waveCounter:I


# direct methods
.method public constructor <init>(Landroid/renderscript/RenderScript;Landroid/content/res/Resources;I)V
    .locals 2
    .param p1    # Landroid/renderscript/RenderScript;
    .param p2    # Landroid/content/res/Resources;
    .param p3    # I

    const/4 v1, 0x0

    invoke-direct {p0, p1, p2, p3}, Landroid/renderscript/ScriptC;-><init>(Landroid/renderscript/RenderScript;Landroid/content/res/Resources;I)V

    invoke-static {p1}, Landroid/renderscript/Element;->F32(Landroid/renderscript/RenderScript;)Landroid/renderscript/Element;

    move-result-object v0

    iput-object v0, p0, Lcom/android/musicvis/vis5/ScriptC_many;->__F32:Landroid/renderscript/Element;

    invoke-static {p1}, Landroid/renderscript/Element;->I32(Landroid/renderscript/RenderScript;)Landroid/renderscript/Element;

    move-result-object v0

    iput-object v0, p0, Lcom/android/musicvis/vis5/ScriptC_many;->__I32:Landroid/renderscript/Element;

    invoke-static {p1}, Landroid/renderscript/Element;->PROGRAM_VERTEX(Landroid/renderscript/RenderScript;)Landroid/renderscript/Element;

    move-result-object v0

    iput-object v0, p0, Lcom/android/musicvis/vis5/ScriptC_many;->__PROGRAM_VERTEX:Landroid/renderscript/Element;

    invoke-static {p1}, Landroid/renderscript/Element;->PROGRAM_FRAGMENT(Landroid/renderscript/RenderScript;)Landroid/renderscript/Element;

    move-result-object v0

    iput-object v0, p0, Lcom/android/musicvis/vis5/ScriptC_many;->__PROGRAM_FRAGMENT:Landroid/renderscript/Element;

    invoke-static {p1}, Landroid/renderscript/Element;->PROGRAM_RASTER(Landroid/renderscript/RenderScript;)Landroid/renderscript/Element;

    move-result-object v0

    iput-object v0, p0, Lcom/android/musicvis/vis5/ScriptC_many;->__PROGRAM_RASTER:Landroid/renderscript/Element;

    invoke-static {p1}, Landroid/renderscript/Element;->ALLOCATION(Landroid/renderscript/RenderScript;)Landroid/renderscript/Element;

    move-result-object v0

    iput-object v0, p0, Lcom/android/musicvis/vis5/ScriptC_many;->__ALLOCATION:Landroid/renderscript/Element;

    invoke-static {p1}, Landroid/renderscript/Element;->PROGRAM_STORE(Landroid/renderscript/RenderScript;)Landroid/renderscript/Element;

    move-result-object v0

    iput-object v0, p0, Lcom/android/musicvis/vis5/ScriptC_many;->__PROGRAM_STORE:Landroid/renderscript/Element;

    invoke-static {p1}, Landroid/renderscript/Element;->MESH(Landroid/renderscript/RenderScript;)Landroid/renderscript/Element;

    move-result-object v0

    iput-object v0, p0, Lcom/android/musicvis/vis5/ScriptC_many;->__MESH:Landroid/renderscript/Element;

    iput v1, p0, Lcom/android/musicvis/vis5/ScriptC_many;->mExportVar_fadeoutcounter:I

    iput v1, p0, Lcom/android/musicvis/vis5/ScriptC_many;->mExportVar_fadeincounter:I

    iput v1, p0, Lcom/android/musicvis/vis5/ScriptC_many;->mExportVar_wave1pos:I

    iput v1, p0, Lcom/android/musicvis/vis5/ScriptC_many;->mExportVar_wave1amp:I

    iput v1, p0, Lcom/android/musicvis/vis5/ScriptC_many;->mExportVar_wave2pos:I

    iput v1, p0, Lcom/android/musicvis/vis5/ScriptC_many;->mExportVar_wave2amp:I

    iput v1, p0, Lcom/android/musicvis/vis5/ScriptC_many;->mExportVar_wave3pos:I

    iput v1, p0, Lcom/android/musicvis/vis5/ScriptC_many;->mExportVar_wave3amp:I

    iput v1, p0, Lcom/android/musicvis/vis5/ScriptC_many;->mExportVar_wave4pos:I

    iput v1, p0, Lcom/android/musicvis/vis5/ScriptC_many;->mExportVar_wave4amp:I

    iput v1, p0, Lcom/android/musicvis/vis5/ScriptC_many;->mExportVar_waveCounter:I

    iput v1, p0, Lcom/android/musicvis/vis5/ScriptC_many;->mExportVar_lastuptime:I

    const/4 v0, 0x0

    iput v0, p0, Lcom/android/musicvis/vis5/ScriptC_many;->mExportVar_autorotation:F

    return-void
.end method


# virtual methods
.method public bind_gPoints(Lcom/android/musicvis/vis5/ScriptField_Vertex;)V
    .locals 2
    .param p1    # Lcom/android/musicvis/vis5/ScriptField_Vertex;

    const/16 v1, 0x12

    iput-object p1, p0, Lcom/android/musicvis/vis5/ScriptC_many;->mExportVar_gPoints:Lcom/android/musicvis/vis5/ScriptField_Vertex;

    if-nez p1, :cond_0

    const/4 v0, 0x0

    invoke-virtual {p0, v0, v1}, Lcom/android/musicvis/vis5/ScriptC_many;->bindAllocation(Landroid/renderscript/Allocation;I)V

    :goto_0
    return-void

    :cond_0
    invoke-virtual {p1}, Lcom/android/musicvis/vis5/ScriptField_Vertex;->getAllocation()Landroid/renderscript/Allocation;

    move-result-object v0

    invoke-virtual {p0, v0, v1}, Lcom/android/musicvis/vis5/ScriptC_many;->bindAllocation(Landroid/renderscript/Allocation;I)V

    goto :goto_0
.end method

.method public declared-synchronized set_gAngle(F)V
    .locals 1
    .param p1    # F

    monitor-enter p0

    const/4 v0, 0x0

    :try_start_0
    invoke-virtual {p0, v0, p1}, Lcom/android/musicvis/vis5/ScriptC_many;->setVar(IF)V

    iput p1, p0, Lcom/android/musicvis/vis5/ScriptC_many;->mExportVar_gAngle:F
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    monitor-exit p0

    return-void

    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method public declared-synchronized set_gCubeMesh(Landroid/renderscript/Mesh;)V
    .locals 1
    .param p1    # Landroid/renderscript/Mesh;

    monitor-enter p0

    const/16 v0, 0x15

    :try_start_0
    invoke-virtual {p0, v0, p1}, Lcom/android/musicvis/vis5/ScriptC_many;->setVar(ILandroid/renderscript/BaseObj;)V

    iput-object p1, p0, Lcom/android/musicvis/vis5/ScriptC_many;->mExportVar_gCubeMesh:Landroid/renderscript/Mesh;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    monitor-exit p0

    return-void

    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method public declared-synchronized set_gIdle(I)V
    .locals 1
    .param p1    # I

    monitor-enter p0

    const/4 v0, 0x4

    :try_start_0
    invoke-virtual {p0, v0, p1}, Lcom/android/musicvis/vis5/ScriptC_many;->setVar(II)V

    iput p1, p0, Lcom/android/musicvis/vis5/ScriptC_many;->mExportVar_gIdle:I
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    monitor-exit p0

    return-void

    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method public declared-synchronized set_gPFBackgroundMip(Landroid/renderscript/ProgramFragment;)V
    .locals 1
    .param p1    # Landroid/renderscript/ProgramFragment;

    monitor-enter p0

    const/4 v0, 0x7

    :try_start_0
    invoke-virtual {p0, v0, p1}, Lcom/android/musicvis/vis5/ScriptC_many;->setVar(ILandroid/renderscript/BaseObj;)V

    iput-object p1, p0, Lcom/android/musicvis/vis5/ScriptC_many;->mExportVar_gPFBackgroundMip:Landroid/renderscript/ProgramFragment;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    monitor-exit p0

    return-void

    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method public declared-synchronized set_gPFBackgroundNoMip(Landroid/renderscript/ProgramFragment;)V
    .locals 1
    .param p1    # Landroid/renderscript/ProgramFragment;

    monitor-enter p0

    const/16 v0, 0x8

    :try_start_0
    invoke-virtual {p0, v0, p1}, Lcom/android/musicvis/vis5/ScriptC_many;->setVar(ILandroid/renderscript/BaseObj;)V

    iput-object p1, p0, Lcom/android/musicvis/vis5/ScriptC_many;->mExportVar_gPFBackgroundNoMip:Landroid/renderscript/ProgramFragment;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    monitor-exit p0

    return-void

    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method public declared-synchronized set_gPFSBackground(Landroid/renderscript/ProgramStore;)V
    .locals 1
    .param p1    # Landroid/renderscript/ProgramStore;

    monitor-enter p0

    const/16 v0, 0x11

    :try_start_0
    invoke-virtual {p0, v0, p1}, Lcom/android/musicvis/vis5/ScriptC_many;->setVar(ILandroid/renderscript/BaseObj;)V

    iput-object p1, p0, Lcom/android/musicvis/vis5/ScriptC_many;->mExportVar_gPFSBackground:Landroid/renderscript/ProgramStore;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    monitor-exit p0

    return-void

    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method public declared-synchronized set_gPR(Landroid/renderscript/ProgramRaster;)V
    .locals 1
    .param p1    # Landroid/renderscript/ProgramRaster;

    monitor-enter p0

    const/16 v0, 0x9

    :try_start_0
    invoke-virtual {p0, v0, p1}, Lcom/android/musicvis/vis5/ScriptC_many;->setVar(ILandroid/renderscript/BaseObj;)V

    iput-object p1, p0, Lcom/android/musicvis/vis5/ScriptC_many;->mExportVar_gPR:Landroid/renderscript/ProgramRaster;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    monitor-exit p0

    return-void

    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method public declared-synchronized set_gPVBackground(Landroid/renderscript/ProgramVertex;)V
    .locals 1
    .param p1    # Landroid/renderscript/ProgramVertex;

    monitor-enter p0

    const/4 v0, 0x6

    :try_start_0
    invoke-virtual {p0, v0, p1}, Lcom/android/musicvis/vis5/ScriptC_many;->setVar(ILandroid/renderscript/BaseObj;)V

    iput-object p1, p0, Lcom/android/musicvis/vis5/ScriptC_many;->mExportVar_gPVBackground:Landroid/renderscript/ProgramVertex;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    monitor-exit p0

    return-void

    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method public declared-synchronized set_gPeak(I)V
    .locals 1
    .param p1    # I

    monitor-enter p0

    const/4 v0, 0x1

    :try_start_0
    invoke-virtual {p0, v0, p1}, Lcom/android/musicvis/vis5/ScriptC_many;->setVar(II)V

    iput p1, p0, Lcom/android/musicvis/vis5/ScriptC_many;->mExportVar_gPeak:I
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    monitor-exit p0

    return-void

    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method public declared-synchronized set_gPointBuffer(Landroid/renderscript/Allocation;)V
    .locals 1
    .param p1    # Landroid/renderscript/Allocation;

    monitor-enter p0

    const/16 v0, 0x13

    :try_start_0
    invoke-virtual {p0, v0, p1}, Lcom/android/musicvis/vis5/ScriptC_many;->setVar(ILandroid/renderscript/BaseObj;)V

    iput-object p1, p0, Lcom/android/musicvis/vis5/ScriptC_many;->mExportVar_gPointBuffer:Landroid/renderscript/Allocation;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    monitor-exit p0

    return-void

    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method public declared-synchronized set_gRotate(F)V
    .locals 1
    .param p1    # F

    monitor-enter p0

    const/4 v0, 0x2

    :try_start_0
    invoke-virtual {p0, v0, p1}, Lcom/android/musicvis/vis5/ScriptC_many;->setVar(IF)V

    iput p1, p0, Lcom/android/musicvis/vis5/ScriptC_many;->mExportVar_gRotate:F
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    monitor-exit p0

    return-void

    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method public declared-synchronized set_gTilt(F)V
    .locals 1
    .param p1    # F

    monitor-enter p0

    const/4 v0, 0x3

    :try_start_0
    invoke-virtual {p0, v0, p1}, Lcom/android/musicvis/vis5/ScriptC_many;->setVar(IF)V

    iput p1, p0, Lcom/android/musicvis/vis5/ScriptC_many;->mExportVar_gTilt:F
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    monitor-exit p0

    return-void

    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method public declared-synchronized set_gTlinetexture(Landroid/renderscript/Allocation;)V
    .locals 1
    .param p1    # Landroid/renderscript/Allocation;

    monitor-enter p0

    const/16 v0, 0x14

    :try_start_0
    invoke-virtual {p0, v0, p1}, Lcom/android/musicvis/vis5/ScriptC_many;->setVar(ILandroid/renderscript/BaseObj;)V

    iput-object p1, p0, Lcom/android/musicvis/vis5/ScriptC_many;->mExportVar_gTlinetexture:Landroid/renderscript/Allocation;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    monitor-exit p0

    return-void

    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method public declared-synchronized set_gTvumeter_album(Landroid/renderscript/Allocation;)V
    .locals 1
    .param p1    # Landroid/renderscript/Allocation;

    monitor-enter p0

    const/16 v0, 0x10

    :try_start_0
    invoke-virtual {p0, v0, p1}, Lcom/android/musicvis/vis5/ScriptC_many;->setVar(ILandroid/renderscript/BaseObj;)V

    iput-object p1, p0, Lcom/android/musicvis/vis5/ScriptC_many;->mExportVar_gTvumeter_album:Landroid/renderscript/Allocation;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    monitor-exit p0

    return-void

    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method public declared-synchronized set_gTvumeter_background(Landroid/renderscript/Allocation;)V
    .locals 1
    .param p1    # Landroid/renderscript/Allocation;

    monitor-enter p0

    const/16 v0, 0xa

    :try_start_0
    invoke-virtual {p0, v0, p1}, Lcom/android/musicvis/vis5/ScriptC_many;->setVar(ILandroid/renderscript/BaseObj;)V

    iput-object p1, p0, Lcom/android/musicvis/vis5/ScriptC_many;->mExportVar_gTvumeter_background:Landroid/renderscript/Allocation;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    monitor-exit p0

    return-void

    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method public declared-synchronized set_gTvumeter_black(Landroid/renderscript/Allocation;)V
    .locals 1
    .param p1    # Landroid/renderscript/Allocation;

    monitor-enter p0

    const/16 v0, 0xe

    :try_start_0
    invoke-virtual {p0, v0, p1}, Lcom/android/musicvis/vis5/ScriptC_many;->setVar(ILandroid/renderscript/BaseObj;)V

    iput-object p1, p0, Lcom/android/musicvis/vis5/ScriptC_many;->mExportVar_gTvumeter_black:Landroid/renderscript/Allocation;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    monitor-exit p0

    return-void

    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method public declared-synchronized set_gTvumeter_frame(Landroid/renderscript/Allocation;)V
    .locals 1
    .param p1    # Landroid/renderscript/Allocation;

    monitor-enter p0

    const/16 v0, 0xf

    :try_start_0
    invoke-virtual {p0, v0, p1}, Lcom/android/musicvis/vis5/ScriptC_many;->setVar(ILandroid/renderscript/BaseObj;)V

    iput-object p1, p0, Lcom/android/musicvis/vis5/ScriptC_many;->mExportVar_gTvumeter_frame:Landroid/renderscript/Allocation;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    monitor-exit p0

    return-void

    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method public declared-synchronized set_gTvumeter_needle(Landroid/renderscript/Allocation;)V
    .locals 1
    .param p1    # Landroid/renderscript/Allocation;

    monitor-enter p0

    const/16 v0, 0xd

    :try_start_0
    invoke-virtual {p0, v0, p1}, Lcom/android/musicvis/vis5/ScriptC_many;->setVar(ILandroid/renderscript/BaseObj;)V

    iput-object p1, p0, Lcom/android/musicvis/vis5/ScriptC_many;->mExportVar_gTvumeter_needle:Landroid/renderscript/Allocation;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    monitor-exit p0

    return-void

    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method public declared-synchronized set_gTvumeter_peak_off(Landroid/renderscript/Allocation;)V
    .locals 1
    .param p1    # Landroid/renderscript/Allocation;

    monitor-enter p0

    const/16 v0, 0xc

    :try_start_0
    invoke-virtual {p0, v0, p1}, Lcom/android/musicvis/vis5/ScriptC_many;->setVar(ILandroid/renderscript/BaseObj;)V

    iput-object p1, p0, Lcom/android/musicvis/vis5/ScriptC_many;->mExportVar_gTvumeter_peak_off:Landroid/renderscript/Allocation;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    monitor-exit p0

    return-void

    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method public declared-synchronized set_gTvumeter_peak_on(Landroid/renderscript/Allocation;)V
    .locals 1
    .param p1    # Landroid/renderscript/Allocation;

    monitor-enter p0

    const/16 v0, 0xb

    :try_start_0
    invoke-virtual {p0, v0, p1}, Lcom/android/musicvis/vis5/ScriptC_many;->setVar(ILandroid/renderscript/BaseObj;)V

    iput-object p1, p0, Lcom/android/musicvis/vis5/ScriptC_many;->mExportVar_gTvumeter_peak_on:Landroid/renderscript/Allocation;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    monitor-exit p0

    return-void

    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method public declared-synchronized set_gWaveCounter(I)V
    .locals 1
    .param p1    # I

    monitor-enter p0

    const/4 v0, 0x5

    :try_start_0
    invoke-virtual {p0, v0, p1}, Lcom/android/musicvis/vis5/ScriptC_many;->setVar(II)V

    iput p1, p0, Lcom/android/musicvis/vis5/ScriptC_many;->mExportVar_gWaveCounter:I
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    monitor-exit p0

    return-void

    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method
