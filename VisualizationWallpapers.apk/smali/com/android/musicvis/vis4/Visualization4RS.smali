.class Lcom/android/musicvis/vis4/Visualization4RS;
.super Lcom/android/musicvis/RenderScriptScene;
.source "Visualization4RS.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/android/musicvis/vis4/Visualization4RS$WorldState;
    }
.end annotation


# instance fields
.field private mAudioCapture:Lcom/android/musicvis/AudioCapture;

.field private final mDrawCube:Ljava/lang/Runnable;

.field private final mHandler:Landroid/os/Handler;

.field private mNeedleMass:I

.field private mNeedlePos:I

.field private mNeedleSpeed:I

.field private mPVAlloc:Landroid/renderscript/ProgramVertexFixedFunction$Constants;

.field private mPVBackground:Landroid/renderscript/ProgramVertex;

.field private mPfBackground:Landroid/renderscript/ProgramFragment;

.field private mPfsBackground:Landroid/renderscript/ProgramStore;

.field private mSampler:Landroid/renderscript/Sampler;

.field mScript:Lcom/android/musicvis/vis4/ScriptC_vu;

.field private mSpringForceAtOrigin:I

.field private mTextures:[Landroid/renderscript/Allocation;

.field private mVisible:Z

.field private mVizData:[I

.field mWorldState:Lcom/android/musicvis/vis4/Visualization4RS$WorldState;


# direct methods
.method constructor <init>(II)V
    .locals 2
    .param p1    # I
    .param p2    # I

    const/4 v1, 0x0

    invoke-direct {p0, p1, p2}, Lcom/android/musicvis/RenderScriptScene;-><init>(II)V

    new-instance v0, Landroid/os/Handler;

    invoke-direct {v0}, Landroid/os/Handler;-><init>()V

    iput-object v0, p0, Lcom/android/musicvis/vis4/Visualization4RS;->mHandler:Landroid/os/Handler;

    new-instance v0, Lcom/android/musicvis/vis4/Visualization4RS$1;

    invoke-direct {v0, p0}, Lcom/android/musicvis/vis4/Visualization4RS$1;-><init>(Lcom/android/musicvis/vis4/Visualization4RS;)V

    iput-object v0, p0, Lcom/android/musicvis/vis4/Visualization4RS;->mDrawCube:Ljava/lang/Runnable;

    iput v1, p0, Lcom/android/musicvis/vis4/Visualization4RS;->mNeedlePos:I

    iput v1, p0, Lcom/android/musicvis/vis4/Visualization4RS;->mNeedleSpeed:I

    const/16 v0, 0xa

    iput v0, p0, Lcom/android/musicvis/vis4/Visualization4RS;->mNeedleMass:I

    const/16 v0, 0xc8

    iput v0, p0, Lcom/android/musicvis/vis4/Visualization4RS;->mSpringForceAtOrigin:I

    new-instance v0, Lcom/android/musicvis/vis4/Visualization4RS$WorldState;

    invoke-direct {v0}, Lcom/android/musicvis/vis4/Visualization4RS$WorldState;-><init>()V

    iput-object v0, p0, Lcom/android/musicvis/vis4/Visualization4RS;->mWorldState:Lcom/android/musicvis/vis4/Visualization4RS$WorldState;

    const/4 v0, 0x0

    iput-object v0, p0, Lcom/android/musicvis/vis4/Visualization4RS;->mAudioCapture:Lcom/android/musicvis/AudioCapture;

    const/16 v0, 0x400

    new-array v0, v0, [I

    iput-object v0, p0, Lcom/android/musicvis/vis4/Visualization4RS;->mVizData:[I

    iput p1, p0, Lcom/android/musicvis/vis4/Visualization4RS;->mWidth:I

    iput p2, p0, Lcom/android/musicvis/vis4/Visualization4RS;->mHeight:I

    return-void
.end method


# virtual methods
.method protected createScript()Landroid/renderscript/ScriptC;
    .locals 13

    const/4 v12, 0x4

    const/4 v11, 0x3

    const/4 v8, 0x2

    const/4 v10, 0x1

    const/4 v9, 0x0

    new-instance v4, Lcom/android/musicvis/vis4/ScriptC_vu;

    iget-object v5, p0, Lcom/android/musicvis/vis4/Visualization4RS;->mRS:Landroid/renderscript/RenderScriptGL;

    iget-object v6, p0, Lcom/android/musicvis/vis4/Visualization4RS;->mResources:Landroid/content/res/Resources;

    const v7, 0x7f040001    # com.android.musicvis.R.raw.vu

    invoke-direct {v4, v5, v6, v7}, Lcom/android/musicvis/vis4/ScriptC_vu;-><init>(Landroid/renderscript/RenderScript;Landroid/content/res/Resources;I)V

    iput-object v4, p0, Lcom/android/musicvis/vis4/Visualization4RS;->mScript:Lcom/android/musicvis/vis4/ScriptC_vu;

    new-instance v2, Landroid/renderscript/ProgramVertexFixedFunction$Builder;

    iget-object v4, p0, Lcom/android/musicvis/vis4/Visualization4RS;->mRS:Landroid/renderscript/RenderScriptGL;

    invoke-direct {v2, v4}, Landroid/renderscript/ProgramVertexFixedFunction$Builder;-><init>(Landroid/renderscript/RenderScript;)V

    invoke-virtual {v2}, Landroid/renderscript/ProgramVertexFixedFunction$Builder;->create()Landroid/renderscript/ProgramVertexFixedFunction;

    move-result-object v4

    iput-object v4, p0, Lcom/android/musicvis/vis4/Visualization4RS;->mPVBackground:Landroid/renderscript/ProgramVertex;

    new-instance v4, Landroid/renderscript/ProgramVertexFixedFunction$Constants;

    iget-object v5, p0, Lcom/android/musicvis/vis4/Visualization4RS;->mRS:Landroid/renderscript/RenderScriptGL;

    invoke-direct {v4, v5}, Landroid/renderscript/ProgramVertexFixedFunction$Constants;-><init>(Landroid/renderscript/RenderScript;)V

    iput-object v4, p0, Lcom/android/musicvis/vis4/Visualization4RS;->mPVAlloc:Landroid/renderscript/ProgramVertexFixedFunction$Constants;

    iget-object v4, p0, Lcom/android/musicvis/vis4/Visualization4RS;->mPVBackground:Landroid/renderscript/ProgramVertex;

    check-cast v4, Landroid/renderscript/ProgramVertexFixedFunction;

    iget-object v5, p0, Lcom/android/musicvis/vis4/Visualization4RS;->mPVAlloc:Landroid/renderscript/ProgramVertexFixedFunction$Constants;

    invoke-virtual {v4, v5}, Landroid/renderscript/ProgramVertexFixedFunction;->bindConstants(Landroid/renderscript/ProgramVertexFixedFunction$Constants;)V

    new-instance v1, Landroid/renderscript/Matrix4f;

    invoke-direct {v1}, Landroid/renderscript/Matrix4f;-><init>()V

    iget v4, p0, Lcom/android/musicvis/vis4/Visualization4RS;->mWidth:I

    iget v5, p0, Lcom/android/musicvis/vis4/Visualization4RS;->mHeight:I

    invoke-virtual {v1, v4, v5}, Landroid/renderscript/Matrix4f;->loadProjectionNormalized(II)V

    iget-object v4, p0, Lcom/android/musicvis/vis4/Visualization4RS;->mPVAlloc:Landroid/renderscript/ProgramVertexFixedFunction$Constants;

    invoke-virtual {v4, v1}, Landroid/renderscript/ProgramVertexFixedFunction$Constants;->setProjection(Landroid/renderscript/Matrix4f;)V

    iget-object v4, p0, Lcom/android/musicvis/vis4/Visualization4RS;->mScript:Lcom/android/musicvis/vis4/ScriptC_vu;

    iget-object v5, p0, Lcom/android/musicvis/vis4/Visualization4RS;->mPVBackground:Landroid/renderscript/ProgramVertex;

    invoke-virtual {v4, v5}, Lcom/android/musicvis/vis4/ScriptC_vu;->set_gPVBackground(Landroid/renderscript/ProgramVertex;)V

    invoke-virtual {p0}, Lcom/android/musicvis/vis4/Visualization4RS;->updateWave()V

    const/4 v4, 0x6

    new-array v4, v4, [Landroid/renderscript/Allocation;

    iput-object v4, p0, Lcom/android/musicvis/vis4/Visualization4RS;->mTextures:[Landroid/renderscript/Allocation;

    iget-object v4, p0, Lcom/android/musicvis/vis4/Visualization4RS;->mTextures:[Landroid/renderscript/Allocation;

    iget-object v5, p0, Lcom/android/musicvis/vis4/Visualization4RS;->mRS:Landroid/renderscript/RenderScriptGL;

    iget-object v6, p0, Lcom/android/musicvis/vis4/Visualization4RS;->mResources:Landroid/content/res/Resources;

    const v7, 0x7f020001    # com.android.musicvis.R.drawable.background

    invoke-static {v5, v6, v7}, Landroid/renderscript/Allocation;->createFromBitmapResource(Landroid/renderscript/RenderScript;Landroid/content/res/Resources;I)Landroid/renderscript/Allocation;

    move-result-object v5

    aput-object v5, v4, v9

    iget-object v4, p0, Lcom/android/musicvis/vis4/Visualization4RS;->mScript:Lcom/android/musicvis/vis4/ScriptC_vu;

    iget-object v5, p0, Lcom/android/musicvis/vis4/Visualization4RS;->mTextures:[Landroid/renderscript/Allocation;

    aget-object v5, v5, v9

    invoke-virtual {v4, v5}, Lcom/android/musicvis/vis4/ScriptC_vu;->set_gTvumeter_background(Landroid/renderscript/Allocation;)V

    iget-object v4, p0, Lcom/android/musicvis/vis4/Visualization4RS;->mTextures:[Landroid/renderscript/Allocation;

    iget-object v5, p0, Lcom/android/musicvis/vis4/Visualization4RS;->mRS:Landroid/renderscript/RenderScriptGL;

    iget-object v6, p0, Lcom/android/musicvis/vis4/Visualization4RS;->mResources:Landroid/content/res/Resources;

    const v7, 0x7f020004    # com.android.musicvis.R.drawable.frame

    invoke-static {v5, v6, v7}, Landroid/renderscript/Allocation;->createFromBitmapResource(Landroid/renderscript/RenderScript;Landroid/content/res/Resources;I)Landroid/renderscript/Allocation;

    move-result-object v5

    aput-object v5, v4, v10

    iget-object v4, p0, Lcom/android/musicvis/vis4/Visualization4RS;->mScript:Lcom/android/musicvis/vis4/ScriptC_vu;

    iget-object v5, p0, Lcom/android/musicvis/vis4/Visualization4RS;->mTextures:[Landroid/renderscript/Allocation;

    aget-object v5, v5, v10

    invoke-virtual {v4, v5}, Lcom/android/musicvis/vis4/ScriptC_vu;->set_gTvumeter_frame(Landroid/renderscript/Allocation;)V

    iget-object v4, p0, Lcom/android/musicvis/vis4/Visualization4RS;->mTextures:[Landroid/renderscript/Allocation;

    iget-object v5, p0, Lcom/android/musicvis/vis4/Visualization4RS;->mRS:Landroid/renderscript/RenderScriptGL;

    iget-object v6, p0, Lcom/android/musicvis/vis4/Visualization4RS;->mResources:Landroid/content/res/Resources;

    const v7, 0x7f020009    # com.android.musicvis.R.drawable.peak_on

    invoke-static {v5, v6, v7}, Landroid/renderscript/Allocation;->createFromBitmapResource(Landroid/renderscript/RenderScript;Landroid/content/res/Resources;I)Landroid/renderscript/Allocation;

    move-result-object v5

    aput-object v5, v4, v8

    iget-object v4, p0, Lcom/android/musicvis/vis4/Visualization4RS;->mScript:Lcom/android/musicvis/vis4/ScriptC_vu;

    iget-object v5, p0, Lcom/android/musicvis/vis4/Visualization4RS;->mTextures:[Landroid/renderscript/Allocation;

    aget-object v5, v5, v8

    invoke-virtual {v4, v5}, Lcom/android/musicvis/vis4/ScriptC_vu;->set_gTvumeter_peak_on(Landroid/renderscript/Allocation;)V

    iget-object v4, p0, Lcom/android/musicvis/vis4/Visualization4RS;->mTextures:[Landroid/renderscript/Allocation;

    iget-object v5, p0, Lcom/android/musicvis/vis4/Visualization4RS;->mRS:Landroid/renderscript/RenderScriptGL;

    iget-object v6, p0, Lcom/android/musicvis/vis4/Visualization4RS;->mResources:Landroid/content/res/Resources;

    const v7, 0x7f020008    # com.android.musicvis.R.drawable.peak_off

    invoke-static {v5, v6, v7}, Landroid/renderscript/Allocation;->createFromBitmapResource(Landroid/renderscript/RenderScript;Landroid/content/res/Resources;I)Landroid/renderscript/Allocation;

    move-result-object v5

    aput-object v5, v4, v11

    iget-object v4, p0, Lcom/android/musicvis/vis4/Visualization4RS;->mScript:Lcom/android/musicvis/vis4/ScriptC_vu;

    iget-object v5, p0, Lcom/android/musicvis/vis4/Visualization4RS;->mTextures:[Landroid/renderscript/Allocation;

    aget-object v5, v5, v11

    invoke-virtual {v4, v5}, Lcom/android/musicvis/vis4/ScriptC_vu;->set_gTvumeter_peak_off(Landroid/renderscript/Allocation;)V

    iget-object v4, p0, Lcom/android/musicvis/vis4/Visualization4RS;->mTextures:[Landroid/renderscript/Allocation;

    iget-object v5, p0, Lcom/android/musicvis/vis4/Visualization4RS;->mRS:Landroid/renderscript/RenderScriptGL;

    iget-object v6, p0, Lcom/android/musicvis/vis4/Visualization4RS;->mResources:Landroid/content/res/Resources;

    const v7, 0x7f020007    # com.android.musicvis.R.drawable.needle

    invoke-static {v5, v6, v7}, Landroid/renderscript/Allocation;->createFromBitmapResource(Landroid/renderscript/RenderScript;Landroid/content/res/Resources;I)Landroid/renderscript/Allocation;

    move-result-object v5

    aput-object v5, v4, v12

    iget-object v4, p0, Lcom/android/musicvis/vis4/Visualization4RS;->mScript:Lcom/android/musicvis/vis4/ScriptC_vu;

    iget-object v5, p0, Lcom/android/musicvis/vis4/Visualization4RS;->mTextures:[Landroid/renderscript/Allocation;

    aget-object v5, v5, v12

    invoke-virtual {v4, v5}, Lcom/android/musicvis/vis4/ScriptC_vu;->set_gTvumeter_needle(Landroid/renderscript/Allocation;)V

    iget-object v4, p0, Lcom/android/musicvis/vis4/Visualization4RS;->mTextures:[Landroid/renderscript/Allocation;

    const/4 v5, 0x5

    iget-object v6, p0, Lcom/android/musicvis/vis4/Visualization4RS;->mRS:Landroid/renderscript/RenderScriptGL;

    iget-object v7, p0, Lcom/android/musicvis/vis4/Visualization4RS;->mResources:Landroid/content/res/Resources;

    const v8, 0x7f020002    # com.android.musicvis.R.drawable.black

    invoke-static {v6, v7, v8}, Landroid/renderscript/Allocation;->createFromBitmapResource(Landroid/renderscript/RenderScript;Landroid/content/res/Resources;I)Landroid/renderscript/Allocation;

    move-result-object v6

    aput-object v6, v4, v5

    iget-object v4, p0, Lcom/android/musicvis/vis4/Visualization4RS;->mScript:Lcom/android/musicvis/vis4/ScriptC_vu;

    iget-object v5, p0, Lcom/android/musicvis/vis4/Visualization4RS;->mTextures:[Landroid/renderscript/Allocation;

    const/4 v6, 0x5

    aget-object v5, v5, v6

    invoke-virtual {v4, v5}, Lcom/android/musicvis/vis4/ScriptC_vu;->set_gTvumeter_black(Landroid/renderscript/Allocation;)V

    new-instance v3, Landroid/renderscript/Sampler$Builder;

    iget-object v4, p0, Lcom/android/musicvis/vis4/Visualization4RS;->mRS:Landroid/renderscript/RenderScriptGL;

    invoke-direct {v3, v4}, Landroid/renderscript/Sampler$Builder;-><init>(Landroid/renderscript/RenderScript;)V

    sget-object v4, Landroid/renderscript/Sampler$Value;->LINEAR:Landroid/renderscript/Sampler$Value;

    invoke-virtual {v3, v4}, Landroid/renderscript/Sampler$Builder;->setMinification(Landroid/renderscript/Sampler$Value;)V

    sget-object v4, Landroid/renderscript/Sampler$Value;->LINEAR:Landroid/renderscript/Sampler$Value;

    invoke-virtual {v3, v4}, Landroid/renderscript/Sampler$Builder;->setMagnification(Landroid/renderscript/Sampler$Value;)V

    sget-object v4, Landroid/renderscript/Sampler$Value;->WRAP:Landroid/renderscript/Sampler$Value;

    invoke-virtual {v3, v4}, Landroid/renderscript/Sampler$Builder;->setWrapS(Landroid/renderscript/Sampler$Value;)V

    sget-object v4, Landroid/renderscript/Sampler$Value;->WRAP:Landroid/renderscript/Sampler$Value;

    invoke-virtual {v3, v4}, Landroid/renderscript/Sampler$Builder;->setWrapT(Landroid/renderscript/Sampler$Value;)V

    invoke-virtual {v3}, Landroid/renderscript/Sampler$Builder;->create()Landroid/renderscript/Sampler;

    move-result-object v4

    iput-object v4, p0, Lcom/android/musicvis/vis4/Visualization4RS;->mSampler:Landroid/renderscript/Sampler;

    new-instance v0, Landroid/renderscript/ProgramFragmentFixedFunction$Builder;

    iget-object v4, p0, Lcom/android/musicvis/vis4/Visualization4RS;->mRS:Landroid/renderscript/RenderScriptGL;

    invoke-direct {v0, v4}, Landroid/renderscript/ProgramFragmentFixedFunction$Builder;-><init>(Landroid/renderscript/RenderScript;)V

    sget-object v4, Landroid/renderscript/ProgramFragmentFixedFunction$Builder$EnvMode;->REPLACE:Landroid/renderscript/ProgramFragmentFixedFunction$Builder$EnvMode;

    sget-object v5, Landroid/renderscript/ProgramFragmentFixedFunction$Builder$Format;->RGBA:Landroid/renderscript/ProgramFragmentFixedFunction$Builder$Format;

    invoke-virtual {v0, v4, v5, v9}, Landroid/renderscript/ProgramFragmentFixedFunction$Builder;->setTexture(Landroid/renderscript/ProgramFragmentFixedFunction$Builder$EnvMode;Landroid/renderscript/ProgramFragmentFixedFunction$Builder$Format;I)Landroid/renderscript/ProgramFragmentFixedFunction$Builder;

    invoke-virtual {v0}, Landroid/renderscript/ProgramFragmentFixedFunction$Builder;->create()Landroid/renderscript/ProgramFragmentFixedFunction;

    move-result-object v4

    iput-object v4, p0, Lcom/android/musicvis/vis4/Visualization4RS;->mPfBackground:Landroid/renderscript/ProgramFragment;

    iget-object v4, p0, Lcom/android/musicvis/vis4/Visualization4RS;->mPfBackground:Landroid/renderscript/ProgramFragment;

    iget-object v5, p0, Lcom/android/musicvis/vis4/Visualization4RS;->mSampler:Landroid/renderscript/Sampler;

    invoke-virtual {v4, v5, v9}, Landroid/renderscript/ProgramFragment;->bindSampler(Landroid/renderscript/Sampler;I)V

    iget-object v4, p0, Lcom/android/musicvis/vis4/Visualization4RS;->mScript:Lcom/android/musicvis/vis4/ScriptC_vu;

    iget-object v5, p0, Lcom/android/musicvis/vis4/Visualization4RS;->mPfBackground:Landroid/renderscript/ProgramFragment;

    invoke-virtual {v4, v5}, Lcom/android/musicvis/vis4/ScriptC_vu;->set_gPFBackground(Landroid/renderscript/ProgramFragment;)V

    new-instance v0, Landroid/renderscript/ProgramStore$Builder;

    iget-object v4, p0, Lcom/android/musicvis/vis4/Visualization4RS;->mRS:Landroid/renderscript/RenderScriptGL;

    invoke-direct {v0, v4}, Landroid/renderscript/ProgramStore$Builder;-><init>(Landroid/renderscript/RenderScript;)V

    sget-object v4, Landroid/renderscript/ProgramStore$DepthFunc;->ALWAYS:Landroid/renderscript/ProgramStore$DepthFunc;

    invoke-virtual {v0, v4}, Landroid/renderscript/ProgramStore$Builder;->setDepthFunc(Landroid/renderscript/ProgramStore$DepthFunc;)Landroid/renderscript/ProgramStore$Builder;

    sget-object v4, Landroid/renderscript/ProgramStore$BlendSrcFunc;->ONE:Landroid/renderscript/ProgramStore$BlendSrcFunc;

    sget-object v5, Landroid/renderscript/ProgramStore$BlendDstFunc;->ONE_MINUS_SRC_ALPHA:Landroid/renderscript/ProgramStore$BlendDstFunc;

    invoke-virtual {v0, v4, v5}, Landroid/renderscript/ProgramStore$Builder;->setBlendFunc(Landroid/renderscript/ProgramStore$BlendSrcFunc;Landroid/renderscript/ProgramStore$BlendDstFunc;)Landroid/renderscript/ProgramStore$Builder;

    invoke-virtual {v0, v10}, Landroid/renderscript/ProgramStore$Builder;->setDitherEnabled(Z)Landroid/renderscript/ProgramStore$Builder;

    invoke-virtual {v0, v9}, Landroid/renderscript/ProgramStore$Builder;->setDepthMaskEnabled(Z)Landroid/renderscript/ProgramStore$Builder;

    invoke-virtual {v0}, Landroid/renderscript/ProgramStore$Builder;->create()Landroid/renderscript/ProgramStore;

    move-result-object v4

    iput-object v4, p0, Lcom/android/musicvis/vis4/Visualization4RS;->mPfsBackground:Landroid/renderscript/ProgramStore;

    iget-object v4, p0, Lcom/android/musicvis/vis4/Visualization4RS;->mScript:Lcom/android/musicvis/vis4/ScriptC_vu;

    iget-object v5, p0, Lcom/android/musicvis/vis4/Visualization4RS;->mPfsBackground:Landroid/renderscript/ProgramStore;

    invoke-virtual {v4, v5}, Lcom/android/musicvis/vis4/ScriptC_vu;->set_gPFSBackground(Landroid/renderscript/ProgramStore;)V

    iget-object v4, p0, Lcom/android/musicvis/vis4/Visualization4RS;->mScript:Lcom/android/musicvis/vis4/ScriptC_vu;

    invoke-static {}, Ljava/util/TimeZone;->getDefault()Ljava/util/TimeZone;

    move-result-object v5

    invoke-virtual {v5}, Ljava/util/TimeZone;->getID()Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v4, v5}, Lcom/android/musicvis/vis4/ScriptC_vu;->setTimeZone(Ljava/lang/String;)V

    iget-object v4, p0, Lcom/android/musicvis/vis4/Visualization4RS;->mScript:Lcom/android/musicvis/vis4/ScriptC_vu;

    return-object v4
.end method

.method public resize(II)V
    .locals 2
    .param p1    # I
    .param p2    # I

    invoke-super {p0, p1, p2}, Lcom/android/musicvis/RenderScriptScene;->resize(II)V

    iget-object v1, p0, Lcom/android/musicvis/vis4/Visualization4RS;->mPVAlloc:Landroid/renderscript/ProgramVertexFixedFunction$Constants;

    if-eqz v1, :cond_0

    new-instance v0, Landroid/renderscript/Matrix4f;

    invoke-direct {v0}, Landroid/renderscript/Matrix4f;-><init>()V

    invoke-virtual {v0, p1, p2}, Landroid/renderscript/Matrix4f;->loadProjectionNormalized(II)V

    iget-object v1, p0, Lcom/android/musicvis/vis4/Visualization4RS;->mPVAlloc:Landroid/renderscript/ProgramVertexFixedFunction$Constants;

    invoke-virtual {v1, v0}, Landroid/renderscript/ProgramVertexFixedFunction$Constants;->setProjection(Landroid/renderscript/Matrix4f;)V

    :cond_0
    return-void
.end method

.method public start()V
    .locals 3

    invoke-super {p0}, Lcom/android/musicvis/RenderScriptScene;->start()V

    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/android/musicvis/vis4/Visualization4RS;->mVisible:Z

    iget-object v0, p0, Lcom/android/musicvis/vis4/Visualization4RS;->mAudioCapture:Lcom/android/musicvis/AudioCapture;

    if-nez v0, :cond_0

    new-instance v0, Lcom/android/musicvis/AudioCapture;

    const/4 v1, 0x0

    const/16 v2, 0x400

    invoke-direct {v0, v1, v2}, Lcom/android/musicvis/AudioCapture;-><init>(II)V

    iput-object v0, p0, Lcom/android/musicvis/vis4/Visualization4RS;->mAudioCapture:Lcom/android/musicvis/AudioCapture;

    :cond_0
    iget-object v0, p0, Lcom/android/musicvis/vis4/Visualization4RS;->mAudioCapture:Lcom/android/musicvis/AudioCapture;

    invoke-virtual {v0}, Lcom/android/musicvis/AudioCapture;->start()V

    invoke-virtual {p0}, Lcom/android/musicvis/vis4/Visualization4RS;->updateWave()V

    return-void
.end method

.method public stop()V
    .locals 1

    invoke-super {p0}, Lcom/android/musicvis/RenderScriptScene;->stop()V

    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/android/musicvis/vis4/Visualization4RS;->mVisible:Z

    iget-object v0, p0, Lcom/android/musicvis/vis4/Visualization4RS;->mAudioCapture:Lcom/android/musicvis/AudioCapture;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/android/musicvis/vis4/Visualization4RS;->mAudioCapture:Lcom/android/musicvis/AudioCapture;

    invoke-virtual {v0}, Lcom/android/musicvis/AudioCapture;->stop()V

    iget-object v0, p0, Lcom/android/musicvis/vis4/Visualization4RS;->mAudioCapture:Lcom/android/musicvis/AudioCapture;

    invoke-virtual {v0}, Lcom/android/musicvis/AudioCapture;->release()V

    const/4 v0, 0x0

    iput-object v0, p0, Lcom/android/musicvis/vis4/Visualization4RS;->mAudioCapture:Lcom/android/musicvis/AudioCapture;

    :cond_0
    return-void
.end method

.method updateWave()V
    .locals 12

    const/16 v11, 0x7fff

    const/4 v10, 0x0

    iget-object v6, p0, Lcom/android/musicvis/vis4/Visualization4RS;->mHandler:Landroid/os/Handler;

    iget-object v7, p0, Lcom/android/musicvis/vis4/Visualization4RS;->mDrawCube:Ljava/lang/Runnable;

    invoke-virtual {v6, v7}, Landroid/os/Handler;->removeCallbacks(Ljava/lang/Runnable;)V

    iget-boolean v6, p0, Lcom/android/musicvis/vis4/Visualization4RS;->mVisible:Z

    if-nez v6, :cond_0

    :goto_0
    return-void

    :cond_0
    iget-object v6, p0, Lcom/android/musicvis/vis4/Visualization4RS;->mHandler:Landroid/os/Handler;

    iget-object v7, p0, Lcom/android/musicvis/vis4/Visualization4RS;->mDrawCube:Ljava/lang/Runnable;

    const-wide/16 v8, 0x14

    invoke-virtual {v6, v7, v8, v9}, Landroid/os/Handler;->postDelayed(Ljava/lang/Runnable;J)Z

    const/4 v2, 0x0

    iget-object v6, p0, Lcom/android/musicvis/vis4/Visualization4RS;->mAudioCapture:Lcom/android/musicvis/AudioCapture;

    if-eqz v6, :cond_1

    iget-object v6, p0, Lcom/android/musicvis/vis4/Visualization4RS;->mAudioCapture:Lcom/android/musicvis/AudioCapture;

    const/16 v7, 0x200

    const/4 v8, 0x1

    invoke-virtual {v6, v7, v8}, Lcom/android/musicvis/AudioCapture;->getFormattedData(II)[I

    move-result-object v6

    iput-object v6, p0, Lcom/android/musicvis/vis4/Visualization4RS;->mVizData:[I

    iget-object v6, p0, Lcom/android/musicvis/vis4/Visualization4RS;->mVizData:[I

    array-length v2, v6

    :cond_1
    const/4 v5, 0x0

    if-lez v2, :cond_4

    const/4 v1, 0x0

    :goto_1
    if-ge v1, v2, :cond_3

    iget-object v6, p0, Lcom/android/musicvis/vis4/Visualization4RS;->mVizData:[I

    aget v4, v6, v1

    if-gez v4, :cond_2

    neg-int v4, v4

    :cond_2
    add-int/2addr v5, v4

    add-int/lit8 v1, v1, 0x1

    goto :goto_1

    :cond_3
    div-int/2addr v5, v2

    :cond_4
    iget v6, p0, Lcom/android/musicvis/vis4/Visualization4RS;->mNeedleSpeed:I

    mul-int/lit8 v6, v6, 0x3

    sub-int v6, v5, v6

    iget v7, p0, Lcom/android/musicvis/vis4/Visualization4RS;->mNeedlePos:I

    iget v8, p0, Lcom/android/musicvis/vis4/Visualization4RS;->mSpringForceAtOrigin:I

    add-int/2addr v7, v8

    sub-int v3, v6, v7

    iget v6, p0, Lcom/android/musicvis/vis4/Visualization4RS;->mNeedleMass:I

    div-int v0, v3, v6

    iget v6, p0, Lcom/android/musicvis/vis4/Visualization4RS;->mNeedleSpeed:I

    add-int/2addr v6, v0

    iput v6, p0, Lcom/android/musicvis/vis4/Visualization4RS;->mNeedleSpeed:I

    iget v6, p0, Lcom/android/musicvis/vis4/Visualization4RS;->mNeedlePos:I

    iget v7, p0, Lcom/android/musicvis/vis4/Visualization4RS;->mNeedleSpeed:I

    add-int/2addr v6, v7

    iput v6, p0, Lcom/android/musicvis/vis4/Visualization4RS;->mNeedlePos:I

    iget v6, p0, Lcom/android/musicvis/vis4/Visualization4RS;->mNeedlePos:I

    if-gez v6, :cond_7

    iput v10, p0, Lcom/android/musicvis/vis4/Visualization4RS;->mNeedlePos:I

    iput v10, p0, Lcom/android/musicvis/vis4/Visualization4RS;->mNeedleSpeed:I

    :cond_5
    :goto_2
    iget-object v6, p0, Lcom/android/musicvis/vis4/Visualization4RS;->mWorldState:Lcom/android/musicvis/vis4/Visualization4RS$WorldState;

    iget v6, v6, Lcom/android/musicvis/vis4/Visualization4RS$WorldState;->mPeak:I

    if-lez v6, :cond_6

    iget-object v6, p0, Lcom/android/musicvis/vis4/Visualization4RS;->mWorldState:Lcom/android/musicvis/vis4/Visualization4RS$WorldState;

    iget v7, v6, Lcom/android/musicvis/vis4/Visualization4RS$WorldState;->mPeak:I

    add-int/lit8 v7, v7, -0x1

    iput v7, v6, Lcom/android/musicvis/vis4/Visualization4RS$WorldState;->mPeak:I

    :cond_6
    iget-object v6, p0, Lcom/android/musicvis/vis4/Visualization4RS;->mWorldState:Lcom/android/musicvis/vis4/Visualization4RS$WorldState;

    const/high16 v7, 0x43030000    # 131.0f

    iget v8, p0, Lcom/android/musicvis/vis4/Visualization4RS;->mNeedlePos:I

    int-to-float v8, v8

    const/high16 v9, 0x43cd0000    # 410.0f

    div-float/2addr v8, v9

    sub-float/2addr v7, v8

    iput v7, v6, Lcom/android/musicvis/vis4/Visualization4RS$WorldState;->mAngle:F

    iget-object v6, p0, Lcom/android/musicvis/vis4/Visualization4RS;->mScript:Lcom/android/musicvis/vis4/ScriptC_vu;

    iget-object v7, p0, Lcom/android/musicvis/vis4/Visualization4RS;->mWorldState:Lcom/android/musicvis/vis4/Visualization4RS$WorldState;

    iget v7, v7, Lcom/android/musicvis/vis4/Visualization4RS$WorldState;->mAngle:F

    invoke-virtual {v6, v7}, Lcom/android/musicvis/vis4/ScriptC_vu;->set_gAngle(F)V

    iget-object v6, p0, Lcom/android/musicvis/vis4/Visualization4RS;->mScript:Lcom/android/musicvis/vis4/ScriptC_vu;

    iget-object v7, p0, Lcom/android/musicvis/vis4/Visualization4RS;->mWorldState:Lcom/android/musicvis/vis4/Visualization4RS$WorldState;

    iget v7, v7, Lcom/android/musicvis/vis4/Visualization4RS$WorldState;->mPeak:I

    invoke-virtual {v6, v7}, Lcom/android/musicvis/vis4/ScriptC_vu;->set_gPeak(I)V

    goto/16 :goto_0

    :cond_7
    iget v6, p0, Lcom/android/musicvis/vis4/Visualization4RS;->mNeedlePos:I

    if-le v6, v11, :cond_5

    iget v6, p0, Lcom/android/musicvis/vis4/Visualization4RS;->mNeedlePos:I

    const v7, 0x8235

    if-le v6, v7, :cond_8

    iget-object v6, p0, Lcom/android/musicvis/vis4/Visualization4RS;->mWorldState:Lcom/android/musicvis/vis4/Visualization4RS$WorldState;

    const/16 v7, 0xa

    iput v7, v6, Lcom/android/musicvis/vis4/Visualization4RS$WorldState;->mPeak:I

    :cond_8
    iput v11, p0, Lcom/android/musicvis/vis4/Visualization4RS;->mNeedlePos:I

    iput v10, p0, Lcom/android/musicvis/vis4/Visualization4RS;->mNeedleSpeed:I

    goto :goto_2
.end method
