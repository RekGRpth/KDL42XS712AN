.class public final enum Lcom/konka/mediaSharePlayer/mediaType;
.super Ljava/lang/Enum;
.source "mediaType.java"


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Enum",
        "<",
        "Lcom/konka/mediaSharePlayer/mediaType;",
        ">;"
    }
.end annotation


# static fields
.field private static final synthetic ENUM$VALUES:[Lcom/konka/mediaSharePlayer/mediaType;

.field public static final enum multiscreen_bestv:Lcom/konka/mediaSharePlayer/mediaType;

.field public static final enum multiscreen_music:Lcom/konka/mediaSharePlayer/mediaType;

.field public static final enum multiscreen_null:Lcom/konka/mediaSharePlayer/mediaType;

.field public static final enum multiscreen_num:Lcom/konka/mediaSharePlayer/mediaType;

.field public static final enum multiscreen_photo:Lcom/konka/mediaSharePlayer/mediaType;

.field public static final enum multiscreen_pplive:Lcom/konka/mediaSharePlayer/mediaType;

.field public static final enum multiscreen_video:Lcom/konka/mediaSharePlayer/mediaType;


# direct methods
.method static constructor <clinit>()V
    .locals 8

    const/4 v7, 0x4

    const/4 v6, 0x3

    const/4 v5, 0x2

    const/4 v4, 0x1

    const/4 v3, 0x0

    new-instance v0, Lcom/konka/mediaSharePlayer/mediaType;

    const-string v1, "multiscreen_null"

    invoke-direct {v0, v1, v3}, Lcom/konka/mediaSharePlayer/mediaType;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/konka/mediaSharePlayer/mediaType;->multiscreen_null:Lcom/konka/mediaSharePlayer/mediaType;

    new-instance v0, Lcom/konka/mediaSharePlayer/mediaType;

    const-string v1, "multiscreen_photo"

    invoke-direct {v0, v1, v4}, Lcom/konka/mediaSharePlayer/mediaType;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/konka/mediaSharePlayer/mediaType;->multiscreen_photo:Lcom/konka/mediaSharePlayer/mediaType;

    new-instance v0, Lcom/konka/mediaSharePlayer/mediaType;

    const-string v1, "multiscreen_music"

    invoke-direct {v0, v1, v5}, Lcom/konka/mediaSharePlayer/mediaType;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/konka/mediaSharePlayer/mediaType;->multiscreen_music:Lcom/konka/mediaSharePlayer/mediaType;

    new-instance v0, Lcom/konka/mediaSharePlayer/mediaType;

    const-string v1, "multiscreen_video"

    invoke-direct {v0, v1, v6}, Lcom/konka/mediaSharePlayer/mediaType;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/konka/mediaSharePlayer/mediaType;->multiscreen_video:Lcom/konka/mediaSharePlayer/mediaType;

    new-instance v0, Lcom/konka/mediaSharePlayer/mediaType;

    const-string v1, "multiscreen_bestv"

    invoke-direct {v0, v1, v7}, Lcom/konka/mediaSharePlayer/mediaType;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/konka/mediaSharePlayer/mediaType;->multiscreen_bestv:Lcom/konka/mediaSharePlayer/mediaType;

    new-instance v0, Lcom/konka/mediaSharePlayer/mediaType;

    const-string v1, "multiscreen_pplive"

    const/4 v2, 0x5

    invoke-direct {v0, v1, v2}, Lcom/konka/mediaSharePlayer/mediaType;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/konka/mediaSharePlayer/mediaType;->multiscreen_pplive:Lcom/konka/mediaSharePlayer/mediaType;

    new-instance v0, Lcom/konka/mediaSharePlayer/mediaType;

    const-string v1, "multiscreen_num"

    const/4 v2, 0x6

    invoke-direct {v0, v1, v2}, Lcom/konka/mediaSharePlayer/mediaType;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/konka/mediaSharePlayer/mediaType;->multiscreen_num:Lcom/konka/mediaSharePlayer/mediaType;

    const/4 v0, 0x7

    new-array v0, v0, [Lcom/konka/mediaSharePlayer/mediaType;

    sget-object v1, Lcom/konka/mediaSharePlayer/mediaType;->multiscreen_null:Lcom/konka/mediaSharePlayer/mediaType;

    aput-object v1, v0, v3

    sget-object v1, Lcom/konka/mediaSharePlayer/mediaType;->multiscreen_photo:Lcom/konka/mediaSharePlayer/mediaType;

    aput-object v1, v0, v4

    sget-object v1, Lcom/konka/mediaSharePlayer/mediaType;->multiscreen_music:Lcom/konka/mediaSharePlayer/mediaType;

    aput-object v1, v0, v5

    sget-object v1, Lcom/konka/mediaSharePlayer/mediaType;->multiscreen_video:Lcom/konka/mediaSharePlayer/mediaType;

    aput-object v1, v0, v6

    sget-object v1, Lcom/konka/mediaSharePlayer/mediaType;->multiscreen_bestv:Lcom/konka/mediaSharePlayer/mediaType;

    aput-object v1, v0, v7

    const/4 v1, 0x5

    sget-object v2, Lcom/konka/mediaSharePlayer/mediaType;->multiscreen_pplive:Lcom/konka/mediaSharePlayer/mediaType;

    aput-object v2, v0, v1

    const/4 v1, 0x6

    sget-object v2, Lcom/konka/mediaSharePlayer/mediaType;->multiscreen_num:Lcom/konka/mediaSharePlayer/mediaType;

    aput-object v2, v0, v1

    sput-object v0, Lcom/konka/mediaSharePlayer/mediaType;->ENUM$VALUES:[Lcom/konka/mediaSharePlayer/mediaType;

    return-void
.end method

.method private constructor <init>(Ljava/lang/String;I)V
    .locals 0

    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    return-void
.end method

.method public static valueOf(Ljava/lang/String;)Lcom/konka/mediaSharePlayer/mediaType;
    .locals 1

    const-class v0, Lcom/konka/mediaSharePlayer/mediaType;

    invoke-static {v0, p0}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;

    move-result-object v0

    check-cast v0, Lcom/konka/mediaSharePlayer/mediaType;

    return-object v0
.end method

.method public static values()[Lcom/konka/mediaSharePlayer/mediaType;
    .locals 4

    const/4 v3, 0x0

    sget-object v0, Lcom/konka/mediaSharePlayer/mediaType;->ENUM$VALUES:[Lcom/konka/mediaSharePlayer/mediaType;

    array-length v1, v0

    new-array v2, v1, [Lcom/konka/mediaSharePlayer/mediaType;

    invoke-static {v0, v3, v2, v3, v1}, Ljava/lang/System;->arraycopy(Ljava/lang/Object;ILjava/lang/Object;II)V

    return-object v2
.end method
