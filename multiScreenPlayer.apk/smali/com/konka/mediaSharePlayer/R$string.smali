.class public final Lcom/konka/mediaSharePlayer/R$string;
.super Ljava/lang/Object;
.source "R.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/konka/mediaSharePlayer/R;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x19
    name = "string"
.end annotation


# static fields
.field public static final DELETE_ALL_FILE:I = 0x7f090059

.field public static final DELETE_FILE:I = 0x7f090058

.field public static final FAILE:I = 0x7f09005d

.field public static final FILE:I = 0x7f090052

.field public static final FILE_DELETE:I = 0x7f090053

.field public static final FILE_DELETE_ALL:I = 0x7f090054

.field public static final FILE_LOADING:I = 0x7f09005f

.field public static final FILE_MENU_CANCEL:I = 0x7f090056

.field public static final FILE_MENU_SURE:I = 0x7f090055

.field public static final FILE_TITLE:I = 0x7f09005e

.field public static final Format_not_support:I = 0x7f090041

.field public static final LaunchAudio:I = 0x7f090032

.field public static final LaunchFileManager:I = 0x7f090035

.field public static final LaunchImage:I = 0x7f090033

.field public static final LaunchShare:I = 0x7f090034

.field public static final LaunchVideo:I = 0x7f090031

.field public static final MEDIA_LOADING:I = 0x7f090043

.field public static final MM_AUDIO:I = 0x7f090064

.field public static final MM_CANCLE:I = 0x7f090038

.field public static final MM_DISK_TXT:I = 0x7f090077

.field public static final MM_EDIT:I = 0x7f090036

.field public static final MM_FILE_MANAGER:I = 0x7f090067

.field public static final MM_FSM_CANCEL:I = 0x7f09006b

.field public static final MM_FSM_DEFAULT_HOST_NAME:I = 0x7f09006d

.field public static final MM_FSM_MODULE_NAME:I = 0x7f09006c

.field public static final MM_FSM_SEARCH:I = 0x7f09006a

.field public static final MM_IMAGE:I = 0x7f090066

.field public static final MM_IS_QUIT_APP:I = 0x7f09003a

.field public static final MM_QUIE:I = 0x7f090037

.field public static final MM_QUIT_APP:I = 0x7f090039

.field public static final MM_SDCARD_NOT_FOUND:I = 0x7f09003b

.field public static final MM_SELECT_DISK_C:I = 0x7f09006f

.field public static final MM_SELECT_DISK_D:I = 0x7f090070

.field public static final MM_SELECT_DISK_DEFAULT_STATUS:I = 0x7f090074

.field public static final MM_SELECT_DISK_E:I = 0x7f090071

.field public static final MM_SELECT_DISK_F:I = 0x7f090072

.field public static final MM_SELECT_DISK_G:I = 0x7f090073

.field public static final MM_SELECT_DISK_PATH:I = 0x7f09006e

.field public static final MM_SHARE:I = 0x7f090068

.field public static final MM_VIDEO:I = 0x7f090065

.field public static final MM_WIDGET:I = 0x7f090069

.field public static final NOLRC:I = 0x7f09003e

.field public static final NOT_STORAGE:I = 0x7f090063

.field public static final PosProfile:I = 0x7f09008d

.field public static final SELECT_ONE:I = 0x7f090057

.field public static final SORT:I = 0x7f090040

.field public static final SORT_TITLE:I = 0x7f09003f

.field public static final SUCCESS:I = 0x7f09005c

.field public static final SURE_DELETE:I = 0x7f09005a

.field public static final SURE_DELETE_ALL:I = 0x7f09005b

.field public static final Your_File_Format_not_support:I = 0x7f090042

.field public static final appInit:I = 0x7f0900a6

.field public static final app_ime:I = 0x7f090005

.field public static final app_music:I = 0x7f090003

.field public static final app_name:I = 0x7f090001

.field public static final app_photo:I = 0x7f090002

.field public static final app_video:I = 0x7f090004

.field public static final back:I = 0x7f09008b

.field public static final buffering:I = 0x7f090082

.field public static final cancel:I = 0x7f09008a

.field public static final cycleTxt:I = 0x7f09004c

.field public static final descale:I = 0x7f09009b

.field public static final directory_empty:I = 0x7f090060

.field public static final disk_eject:I = 0x7f090076

.field public static final down_load_title:I = 0x7f09009f

.field public static final down_loading:I = 0x7f09008e

.field public static final down_loading_err:I = 0x7f09008f

.field public static final end:I = 0x7f09002e

.field public static final enlarge:I = 0x7f09009a

.field public static final file_downLoad_error:I = 0x7f0900a7

.field public static final first_photo:I = 0x7f090084

.field public static final forbidoption:I = 0x7f0900a5

.field public static final fullScreen:I = 0x7f09009c

.field public static final hello:I = 0x7f090000

.field public static final ime_close:I = 0x7f090008

.field public static final ime_settings_activity_name:I = 0x7f090006

.field public static final ime_start:I = 0x7f090007

.field public static final last_photo:I = 0x7f090085

.field public static final leftrotion:I = 0x7f090098

.field public static final listPic:I = 0x7f090092

.field public static final loader:I = 0x7f09009d

.field public static final localip:I = 0x7f090030

.field public static final localserial:I = 0x7f09002f

.field public static final login:I = 0x7f090089

.field public static final lrcTxt:I = 0x7f090045

.field public static final mediafile_buffering:I = 0x7f0900a2

.field public static final mediafile_end:I = 0x7f0900a3

.field public static final musicListTxt:I = 0x7f090044

.field public static final music_buffering:I = 0x7f09009e

.field public static final next:I = 0x7f09002c

.field public static final nextPic:I = 0x7f090096

.field public static final nextTxt:I = 0x7f090049

.field public static final ok:I = 0x7f090083

.field public static final pause:I = 0x7f09007e

.field public static final pausePic:I = 0x7f090095

.field public static final pauseTxt:I = 0x7f090048

.field public static final pic_setting_defaule_sort:I = 0x7f090086

.field public static final pic_setting_name_sort:I = 0x7f090087

.field public static final pic_setting_size_sort:I = 0x7f090088

.field public static final picture_too_lage:I = 0x7f090090

.field public static final playPic:I = 0x7f090094

.field public static final playTxt:I = 0x7f090047

.field public static final playing:I = 0x7f09007d

.field public static final prePic:I = 0x7f090093

.field public static final preTxt:I = 0x7f090046

.field public static final previous:I = 0x7f090091

.field public static final prompt:I = 0x7f0900a4

.field public static final radioBtn1:I = 0x7f0900a0

.field public static final radioBtn2:I = 0x7f0900a1

.field public static final remove:I = 0x7f090062

.field public static final repeatTxt:I = 0x7f09004b

.field public static final review:I = 0x7f09002d

.field public static final rightrotion:I = 0x7f090099

.field public static final sdcarderror:I = 0x7f090075

.field public static final sequenceTxt:I = 0x7f09004a

.field public static final setting:I = 0x7f09001f

.field public static final setting_about:I = 0x7f090029

.field public static final setting_advanced_key:I = 0x7f09001e

.field public static final setting_directions:I = 0x7f09002a

.field public static final setting_disabled:I = 0x7f090026

.field public static final setting_enabled:I = 0x7f090025

.field public static final setting_others:I = 0x7f090027

.field public static final setting_others_summary:I = 0x7f090028

.field public static final setting_prediction_key:I = 0x7f09001c

.field public static final setting_prediction_title:I = 0x7f090022

.field public static final setting_sound_key:I = 0x7f09001a

.field public static final setting_sound_key_title:I = 0x7f090020

.field public static final setting_switch_key:I = 0x7f09001d

.field public static final setting_switch_shift_space_title:I = 0x7f090024

.field public static final setting_switch_title:I = 0x7f090023

.field public static final setting_vibrate_key:I = 0x7f09001b

.field public static final setting_vibrate_title:I = 0x7f090021

.field public static final show_info:I = 0x7f090081

.field public static final shuffleTxt:I = 0x7f09004d

.field public static final stop:I = 0x7f09007f

.field public static final stopPic:I = 0x7f090097

.field public static final storage:I = 0x7f090061

.field public static final str_cirmode:I = 0x7f09007b

.field public static final str_lanmode:I = 0x7f09007c

.field public static final str_picmode:I = 0x7f09007a

.field public static final str_showmode:I = 0x7f090079

.field public static final str_soundmod:I = 0x7f090078

.field public static final tips_title:I = 0x7f0900a9

.field public static final toggle_cn:I = 0x7f090009

.field public static final toggle_cn_cand:I = 0x7f09000a

.field public static final toggle_en_lower:I = 0x7f09000b

.field public static final toggle_en_sym1:I = 0x7f09000d

.field public static final toggle_en_sym2:I = 0x7f09000e

.field public static final toggle_en_upper:I = 0x7f09000c

.field public static final toggle_enter_done:I = 0x7f090014

.field public static final toggle_enter_go:I = 0x7f090010

.field public static final toggle_enter_next:I = 0x7f090013

.field public static final toggle_enter_search:I = 0x7f090011

.field public static final toggle_enter_send:I = 0x7f090012

.field public static final toggle_phone_sym:I = 0x7f090019

.field public static final toggle_row_cn:I = 0x7f090015

.field public static final toggle_row_emailaddress:I = 0x7f090018

.field public static final toggle_row_en:I = 0x7f090016

.field public static final toggle_row_uri:I = 0x7f090017

.field public static final toggle_smiley:I = 0x7f09000f

.field public static final version:I = 0x7f09002b

.field public static final versionFormatter:I = 0x7f09003c

.field public static final version_no:I = 0x7f09003d

.field public static final video_file_error:I = 0x7f0900a8

.field public static final videofile:I = 0x7f09008c

.field public static final videoformaterror:I = 0x7f090080

.field public static final voiceDown:I = 0x7f090050

.field public static final voiceNoTxt:I = 0x7f09004f

.field public static final voiceTxt:I = 0x7f09004e

.field public static final voiceUp:I = 0x7f090051


# direct methods
.method public constructor <init>()V
    .locals 0

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method
