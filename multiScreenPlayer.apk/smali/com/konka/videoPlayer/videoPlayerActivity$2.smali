.class Lcom/konka/videoPlayer/videoPlayerActivity$2;
.super Landroid/os/Handler;
.source "videoPlayerActivity.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/konka/videoPlayer/videoPlayerActivity;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/konka/videoPlayer/videoPlayerActivity;


# direct methods
.method constructor <init>(Lcom/konka/videoPlayer/videoPlayerActivity;)V
    .locals 0

    iput-object p1, p0, Lcom/konka/videoPlayer/videoPlayerActivity$2;->this$0:Lcom/konka/videoPlayer/videoPlayerActivity;

    invoke-direct {p0}, Landroid/os/Handler;-><init>()V

    return-void
.end method


# virtual methods
.method public handleMessage(Landroid/os/Message;)V
    .locals 3
    .param p1    # Landroid/os/Message;

    const-string v0, "VideoPlayer"

    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "handler:------------"

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    iget v2, p1, Landroid/os/Message;->what:I

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    iget v0, p1, Landroid/os/Message;->what:I

    packed-switch v0, :pswitch_data_0

    :cond_0
    :goto_0
    return-void

    :pswitch_0
    iget-object v0, p0, Lcom/konka/videoPlayer/videoPlayerActivity$2;->this$0:Lcom/konka/videoPlayer/videoPlayerActivity;

    # invokes: Lcom/konka/videoPlayer/videoPlayerActivity;->videoPlayerStop()V
    invoke-static {v0}, Lcom/konka/videoPlayer/videoPlayerActivity;->access$9(Lcom/konka/videoPlayer/videoPlayerActivity;)V

    goto :goto_0

    :pswitch_1
    iget-object v0, p0, Lcom/konka/videoPlayer/videoPlayerActivity$2;->this$0:Lcom/konka/videoPlayer/videoPlayerActivity;

    # getter for: Lcom/konka/videoPlayer/videoPlayerActivity;->sourcePath:Ljava/lang/String;
    invoke-static {v0}, Lcom/konka/videoPlayer/videoPlayerActivity;->access$8(Lcom/konka/videoPlayer/videoPlayerActivity;)Ljava/lang/String;

    move-result-object v0

    if-eqz v0, :cond_0

    const-string v0, "weikan"

    const-string v1, "playMediaSource!!!"

    invoke-static {v0, v1}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    iget-object v0, p0, Lcom/konka/videoPlayer/videoPlayerActivity$2;->this$0:Lcom/konka/videoPlayer/videoPlayerActivity;

    # invokes: Lcom/konka/videoPlayer/videoPlayerActivity;->playMediaSource()V
    invoke-static {v0}, Lcom/konka/videoPlayer/videoPlayerActivity;->access$10(Lcom/konka/videoPlayer/videoPlayerActivity;)V

    goto :goto_0

    :pswitch_2
    iget-object v0, p0, Lcom/konka/videoPlayer/videoPlayerActivity$2;->this$0:Lcom/konka/videoPlayer/videoPlayerActivity;

    # getter for: Lcom/konka/videoPlayer/videoPlayerActivity;->sourcePath:Ljava/lang/String;
    invoke-static {v0}, Lcom/konka/videoPlayer/videoPlayerActivity;->access$8(Lcom/konka/videoPlayer/videoPlayerActivity;)Ljava/lang/String;

    move-result-object v0

    if-eqz v0, :cond_1

    iget-object v0, p0, Lcom/konka/videoPlayer/videoPlayerActivity$2;->this$0:Lcom/konka/videoPlayer/videoPlayerActivity;

    # invokes: Lcom/konka/videoPlayer/videoPlayerActivity;->playMediaSource()V
    invoke-static {v0}, Lcom/konka/videoPlayer/videoPlayerActivity;->access$10(Lcom/konka/videoPlayer/videoPlayerActivity;)V

    :cond_1
    :pswitch_3
    iget-object v0, p0, Lcom/konka/videoPlayer/videoPlayerActivity$2;->this$0:Lcom/konka/videoPlayer/videoPlayerActivity;

    invoke-virtual {v0}, Lcom/konka/videoPlayer/videoPlayerActivity;->hideController()V

    goto :goto_0

    :pswitch_4
    iget-object v0, p0, Lcom/konka/videoPlayer/videoPlayerActivity$2;->this$0:Lcom/konka/videoPlayer/videoPlayerActivity;

    const/4 v1, 0x0

    invoke-static {v0, v1}, Lcom/konka/videoPlayer/videoPlayerActivity;->access$11(Lcom/konka/videoPlayer/videoPlayerActivity;Z)V

    iget-object v0, p0, Lcom/konka/videoPlayer/videoPlayerActivity$2;->this$0:Lcom/konka/videoPlayer/videoPlayerActivity;

    invoke-virtual {v0}, Lcom/konka/videoPlayer/videoPlayerActivity;->cancelProgressDlg()V

    :pswitch_5
    const-string v0, "weikan"

    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "paly Segment video mcurrentpos = "

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    iget-object v2, p0, Lcom/konka/videoPlayer/videoPlayerActivity$2;->this$0:Lcom/konka/videoPlayer/videoPlayerActivity;

    # getter for: Lcom/konka/videoPlayer/videoPlayerActivity;->mCurrentPosOfUrlList:I
    invoke-static {v2}, Lcom/konka/videoPlayer/videoPlayerActivity;->access$12(Lcom/konka/videoPlayer/videoPlayerActivity;)I

    move-result v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, " total length = "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget-object v2, p0, Lcom/konka/videoPlayer/videoPlayerActivity$2;->this$0:Lcom/konka/videoPlayer/videoPlayerActivity;

    # getter for: Lcom/konka/videoPlayer/videoPlayerActivity;->mLengOfUrlList:I
    invoke-static {v2}, Lcom/konka/videoPlayer/videoPlayerActivity;->access$13(Lcom/konka/videoPlayer/videoPlayerActivity;)I

    move-result v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    iget-object v0, p0, Lcom/konka/videoPlayer/videoPlayerActivity$2;->this$0:Lcom/konka/videoPlayer/videoPlayerActivity;

    # getter for: Lcom/konka/videoPlayer/videoPlayerActivity;->mCurrentPosOfUrlList:I
    invoke-static {v0}, Lcom/konka/videoPlayer/videoPlayerActivity;->access$12(Lcom/konka/videoPlayer/videoPlayerActivity;)I

    move-result v0

    iget-object v1, p0, Lcom/konka/videoPlayer/videoPlayerActivity$2;->this$0:Lcom/konka/videoPlayer/videoPlayerActivity;

    # getter for: Lcom/konka/videoPlayer/videoPlayerActivity;->mLengOfUrlList:I
    invoke-static {v1}, Lcom/konka/videoPlayer/videoPlayerActivity;->access$13(Lcom/konka/videoPlayer/videoPlayerActivity;)I

    move-result v1

    if-ge v0, v1, :cond_0

    iget-object v0, p0, Lcom/konka/videoPlayer/videoPlayerActivity$2;->this$0:Lcom/konka/videoPlayer/videoPlayerActivity;

    # invokes: Lcom/konka/videoPlayer/videoPlayerActivity;->playSegmentMediaSource()V
    invoke-static {v0}, Lcom/konka/videoPlayer/videoPlayerActivity;->access$14(Lcom/konka/videoPlayer/videoPlayerActivity;)V

    goto :goto_0

    nop

    :pswitch_data_0
    .packed-switch 0x0
        :pswitch_0
        :pswitch_1
        :pswitch_2
        :pswitch_3
        :pswitch_4
        :pswitch_5
    .end packed-switch
.end method
