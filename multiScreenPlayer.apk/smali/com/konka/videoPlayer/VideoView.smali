.class public Lcom/konka/videoPlayer/VideoView;
.super Landroid/view/SurfaceView;
.source "VideoView.java"

# interfaces
.implements Landroid/widget/MediaController$MediaPlayerControl;


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/konka/videoPlayer/VideoView$MySizeChangeLinstener;
    }
.end annotation


# instance fields
.field private TAG:Ljava/lang/String;

.field private mBufferingUpdateListener:Landroid/media/MediaPlayer$OnBufferingUpdateListener;

.field private mCompletionListener:Landroid/media/MediaPlayer$OnCompletionListener;

.field private mContext:Landroid/content/Context;

.field private mCurrentBufferPercentage:I

.field private mDuration:I

.field private mErrorListener:Landroid/media/MediaPlayer$OnErrorListener;

.field private mIsPrepared:Z

.field private mMediaController:Landroid/widget/MediaController;

.field public mMediaPlayer:Landroid/media/MediaPlayer;

.field private mMyChangeLinstener:Lcom/konka/videoPlayer/VideoView$MySizeChangeLinstener;

.field private mOnCompletionListener:Landroid/media/MediaPlayer$OnCompletionListener;

.field private mOnErrorListener:Landroid/media/MediaPlayer$OnErrorListener;

.field private mOnInfoListener:Landroid/media/MediaPlayer$OnInfoListener;

.field private mOnPreparedListener:Landroid/media/MediaPlayer$OnPreparedListener;

.field mPreparedListener:Landroid/media/MediaPlayer$OnPreparedListener;

.field mSHCallback:Landroid/view/SurfaceHolder$Callback;

.field private mSeekCompleteListener:Landroid/media/MediaPlayer$OnSeekCompleteListener;

.field private mSeekWhenPrepared:I

.field mSizeChangedListener:Landroid/media/MediaPlayer$OnVideoSizeChangedListener;

.field private mStartWhenPrepared:Z

.field private mSurfaceHeight:I

.field private mSurfaceHolder:Landroid/view/SurfaceHolder;

.field private mSurfaceWidth:I

.field private mUri:Landroid/net/Uri;

.field private mVideoHeight:I

.field private mVideoWidth:I

.field private pg:Landroid/app/ProgressDialog;


# direct methods
.method public constructor <init>(Landroid/content/Context;)V
    .locals 2
    .param p1    # Landroid/content/Context;

    const/4 v1, 0x0

    invoke-direct {p0, p1}, Landroid/view/SurfaceView;-><init>(Landroid/content/Context;)V

    const-string v0, "VideoView"

    iput-object v0, p0, Lcom/konka/videoPlayer/VideoView;->TAG:Ljava/lang/String;

    iput-object v1, p0, Lcom/konka/videoPlayer/VideoView;->mSurfaceHolder:Landroid/view/SurfaceHolder;

    iput-object v1, p0, Lcom/konka/videoPlayer/VideoView;->mMediaPlayer:Landroid/media/MediaPlayer;

    iput-object v1, p0, Lcom/konka/videoPlayer/VideoView;->pg:Landroid/app/ProgressDialog;

    new-instance v0, Lcom/konka/videoPlayer/VideoView$1;

    invoke-direct {v0, p0}, Lcom/konka/videoPlayer/VideoView$1;-><init>(Lcom/konka/videoPlayer/VideoView;)V

    iput-object v0, p0, Lcom/konka/videoPlayer/VideoView;->mSizeChangedListener:Landroid/media/MediaPlayer$OnVideoSizeChangedListener;

    new-instance v0, Lcom/konka/videoPlayer/VideoView$2;

    invoke-direct {v0, p0}, Lcom/konka/videoPlayer/VideoView$2;-><init>(Lcom/konka/videoPlayer/VideoView;)V

    iput-object v0, p0, Lcom/konka/videoPlayer/VideoView;->mPreparedListener:Landroid/media/MediaPlayer$OnPreparedListener;

    new-instance v0, Lcom/konka/videoPlayer/VideoView$3;

    invoke-direct {v0, p0}, Lcom/konka/videoPlayer/VideoView$3;-><init>(Lcom/konka/videoPlayer/VideoView;)V

    iput-object v0, p0, Lcom/konka/videoPlayer/VideoView;->mCompletionListener:Landroid/media/MediaPlayer$OnCompletionListener;

    new-instance v0, Lcom/konka/videoPlayer/VideoView$4;

    invoke-direct {v0, p0}, Lcom/konka/videoPlayer/VideoView$4;-><init>(Lcom/konka/videoPlayer/VideoView;)V

    iput-object v0, p0, Lcom/konka/videoPlayer/VideoView;->mErrorListener:Landroid/media/MediaPlayer$OnErrorListener;

    new-instance v0, Lcom/konka/videoPlayer/VideoView$5;

    invoke-direct {v0, p0}, Lcom/konka/videoPlayer/VideoView$5;-><init>(Lcom/konka/videoPlayer/VideoView;)V

    iput-object v0, p0, Lcom/konka/videoPlayer/VideoView;->mSeekCompleteListener:Landroid/media/MediaPlayer$OnSeekCompleteListener;

    new-instance v0, Lcom/konka/videoPlayer/VideoView$6;

    invoke-direct {v0, p0}, Lcom/konka/videoPlayer/VideoView$6;-><init>(Lcom/konka/videoPlayer/VideoView;)V

    iput-object v0, p0, Lcom/konka/videoPlayer/VideoView;->mBufferingUpdateListener:Landroid/media/MediaPlayer$OnBufferingUpdateListener;

    new-instance v0, Lcom/konka/videoPlayer/VideoView$7;

    invoke-direct {v0, p0}, Lcom/konka/videoPlayer/VideoView$7;-><init>(Lcom/konka/videoPlayer/VideoView;)V

    iput-object v0, p0, Lcom/konka/videoPlayer/VideoView;->mSHCallback:Landroid/view/SurfaceHolder$Callback;

    iput-object p1, p0, Lcom/konka/videoPlayer/VideoView;->mContext:Landroid/content/Context;

    invoke-direct {p0}, Lcom/konka/videoPlayer/VideoView;->initVideoView()V

    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;)V
    .locals 1
    .param p1    # Landroid/content/Context;
    .param p2    # Landroid/util/AttributeSet;

    const/4 v0, 0x0

    invoke-direct {p0, p1, p2, v0}, Lcom/konka/videoPlayer/VideoView;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V

    iput-object p1, p0, Lcom/konka/videoPlayer/VideoView;->mContext:Landroid/content/Context;

    invoke-direct {p0}, Lcom/konka/videoPlayer/VideoView;->initVideoView()V

    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V
    .locals 2
    .param p1    # Landroid/content/Context;
    .param p2    # Landroid/util/AttributeSet;
    .param p3    # I

    const/4 v1, 0x0

    invoke-direct {p0, p1, p2, p3}, Landroid/view/SurfaceView;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V

    const-string v0, "VideoView"

    iput-object v0, p0, Lcom/konka/videoPlayer/VideoView;->TAG:Ljava/lang/String;

    iput-object v1, p0, Lcom/konka/videoPlayer/VideoView;->mSurfaceHolder:Landroid/view/SurfaceHolder;

    iput-object v1, p0, Lcom/konka/videoPlayer/VideoView;->mMediaPlayer:Landroid/media/MediaPlayer;

    iput-object v1, p0, Lcom/konka/videoPlayer/VideoView;->pg:Landroid/app/ProgressDialog;

    new-instance v0, Lcom/konka/videoPlayer/VideoView$1;

    invoke-direct {v0, p0}, Lcom/konka/videoPlayer/VideoView$1;-><init>(Lcom/konka/videoPlayer/VideoView;)V

    iput-object v0, p0, Lcom/konka/videoPlayer/VideoView;->mSizeChangedListener:Landroid/media/MediaPlayer$OnVideoSizeChangedListener;

    new-instance v0, Lcom/konka/videoPlayer/VideoView$2;

    invoke-direct {v0, p0}, Lcom/konka/videoPlayer/VideoView$2;-><init>(Lcom/konka/videoPlayer/VideoView;)V

    iput-object v0, p0, Lcom/konka/videoPlayer/VideoView;->mPreparedListener:Landroid/media/MediaPlayer$OnPreparedListener;

    new-instance v0, Lcom/konka/videoPlayer/VideoView$3;

    invoke-direct {v0, p0}, Lcom/konka/videoPlayer/VideoView$3;-><init>(Lcom/konka/videoPlayer/VideoView;)V

    iput-object v0, p0, Lcom/konka/videoPlayer/VideoView;->mCompletionListener:Landroid/media/MediaPlayer$OnCompletionListener;

    new-instance v0, Lcom/konka/videoPlayer/VideoView$4;

    invoke-direct {v0, p0}, Lcom/konka/videoPlayer/VideoView$4;-><init>(Lcom/konka/videoPlayer/VideoView;)V

    iput-object v0, p0, Lcom/konka/videoPlayer/VideoView;->mErrorListener:Landroid/media/MediaPlayer$OnErrorListener;

    new-instance v0, Lcom/konka/videoPlayer/VideoView$5;

    invoke-direct {v0, p0}, Lcom/konka/videoPlayer/VideoView$5;-><init>(Lcom/konka/videoPlayer/VideoView;)V

    iput-object v0, p0, Lcom/konka/videoPlayer/VideoView;->mSeekCompleteListener:Landroid/media/MediaPlayer$OnSeekCompleteListener;

    new-instance v0, Lcom/konka/videoPlayer/VideoView$6;

    invoke-direct {v0, p0}, Lcom/konka/videoPlayer/VideoView$6;-><init>(Lcom/konka/videoPlayer/VideoView;)V

    iput-object v0, p0, Lcom/konka/videoPlayer/VideoView;->mBufferingUpdateListener:Landroid/media/MediaPlayer$OnBufferingUpdateListener;

    new-instance v0, Lcom/konka/videoPlayer/VideoView$7;

    invoke-direct {v0, p0}, Lcom/konka/videoPlayer/VideoView$7;-><init>(Lcom/konka/videoPlayer/VideoView;)V

    iput-object v0, p0, Lcom/konka/videoPlayer/VideoView;->mSHCallback:Landroid/view/SurfaceHolder$Callback;

    iput-object p1, p0, Lcom/konka/videoPlayer/VideoView;->mContext:Landroid/content/Context;

    invoke-direct {p0}, Lcom/konka/videoPlayer/VideoView;->initVideoView()V

    return-void
.end method

.method static synthetic access$0(Lcom/konka/videoPlayer/VideoView;I)V
    .locals 0

    iput p1, p0, Lcom/konka/videoPlayer/VideoView;->mVideoWidth:I

    return-void
.end method

.method static synthetic access$1(Lcom/konka/videoPlayer/VideoView;I)V
    .locals 0

    iput p1, p0, Lcom/konka/videoPlayer/VideoView;->mVideoHeight:I

    return-void
.end method

.method static synthetic access$10(Lcom/konka/videoPlayer/VideoView;)I
    .locals 1

    iget v0, p0, Lcom/konka/videoPlayer/VideoView;->mSeekWhenPrepared:I

    return v0
.end method

.method static synthetic access$11(Lcom/konka/videoPlayer/VideoView;I)V
    .locals 0

    iput p1, p0, Lcom/konka/videoPlayer/VideoView;->mSeekWhenPrepared:I

    return-void
.end method

.method static synthetic access$12(Lcom/konka/videoPlayer/VideoView;)Z
    .locals 1

    iget-boolean v0, p0, Lcom/konka/videoPlayer/VideoView;->mStartWhenPrepared:Z

    return v0
.end method

.method static synthetic access$13(Lcom/konka/videoPlayer/VideoView;Z)V
    .locals 0

    iput-boolean p1, p0, Lcom/konka/videoPlayer/VideoView;->mStartWhenPrepared:Z

    return-void
.end method

.method static synthetic access$14(Lcom/konka/videoPlayer/VideoView;)Ljava/lang/String;
    .locals 1

    iget-object v0, p0, Lcom/konka/videoPlayer/VideoView;->TAG:Ljava/lang/String;

    return-object v0
.end method

.method static synthetic access$15(Lcom/konka/videoPlayer/VideoView;)Landroid/media/MediaPlayer$OnCompletionListener;
    .locals 1

    iget-object v0, p0, Lcom/konka/videoPlayer/VideoView;->mOnCompletionListener:Landroid/media/MediaPlayer$OnCompletionListener;

    return-object v0
.end method

.method static synthetic access$16(Lcom/konka/videoPlayer/VideoView;)Landroid/media/MediaPlayer$OnErrorListener;
    .locals 1

    iget-object v0, p0, Lcom/konka/videoPlayer/VideoView;->mOnErrorListener:Landroid/media/MediaPlayer$OnErrorListener;

    return-object v0
.end method

.method static synthetic access$17(Lcom/konka/videoPlayer/VideoView;)Landroid/app/ProgressDialog;
    .locals 1

    iget-object v0, p0, Lcom/konka/videoPlayer/VideoView;->pg:Landroid/app/ProgressDialog;

    return-object v0
.end method

.method static synthetic access$18(Lcom/konka/videoPlayer/VideoView;I)V
    .locals 0

    iput p1, p0, Lcom/konka/videoPlayer/VideoView;->mCurrentBufferPercentage:I

    return-void
.end method

.method static synthetic access$19(Lcom/konka/videoPlayer/VideoView;I)V
    .locals 0

    iput p1, p0, Lcom/konka/videoPlayer/VideoView;->mSurfaceWidth:I

    return-void
.end method

.method static synthetic access$2(Lcom/konka/videoPlayer/VideoView;)Lcom/konka/videoPlayer/VideoView$MySizeChangeLinstener;
    .locals 1

    iget-object v0, p0, Lcom/konka/videoPlayer/VideoView;->mMyChangeLinstener:Lcom/konka/videoPlayer/VideoView$MySizeChangeLinstener;

    return-object v0
.end method

.method static synthetic access$20(Lcom/konka/videoPlayer/VideoView;I)V
    .locals 0

    iput p1, p0, Lcom/konka/videoPlayer/VideoView;->mSurfaceHeight:I

    return-void
.end method

.method static synthetic access$21(Lcom/konka/videoPlayer/VideoView;)Z
    .locals 1

    iget-boolean v0, p0, Lcom/konka/videoPlayer/VideoView;->mIsPrepared:Z

    return v0
.end method

.method static synthetic access$22(Lcom/konka/videoPlayer/VideoView;Landroid/view/SurfaceHolder;)V
    .locals 0

    iput-object p1, p0, Lcom/konka/videoPlayer/VideoView;->mSurfaceHolder:Landroid/view/SurfaceHolder;

    return-void
.end method

.method static synthetic access$23(Lcom/konka/videoPlayer/VideoView;)V
    .locals 0

    invoke-direct {p0}, Lcom/konka/videoPlayer/VideoView;->openVideo()V

    return-void
.end method

.method static synthetic access$3(Lcom/konka/videoPlayer/VideoView;)I
    .locals 1

    iget v0, p0, Lcom/konka/videoPlayer/VideoView;->mVideoWidth:I

    return v0
.end method

.method static synthetic access$4(Lcom/konka/videoPlayer/VideoView;)I
    .locals 1

    iget v0, p0, Lcom/konka/videoPlayer/VideoView;->mVideoHeight:I

    return v0
.end method

.method static synthetic access$5(Lcom/konka/videoPlayer/VideoView;Z)V
    .locals 0

    iput-boolean p1, p0, Lcom/konka/videoPlayer/VideoView;->mIsPrepared:Z

    return-void
.end method

.method static synthetic access$6(Lcom/konka/videoPlayer/VideoView;)Landroid/media/MediaPlayer$OnPreparedListener;
    .locals 1

    iget-object v0, p0, Lcom/konka/videoPlayer/VideoView;->mOnPreparedListener:Landroid/media/MediaPlayer$OnPreparedListener;

    return-object v0
.end method

.method static synthetic access$7(Lcom/konka/videoPlayer/VideoView;)Landroid/widget/MediaController;
    .locals 1

    iget-object v0, p0, Lcom/konka/videoPlayer/VideoView;->mMediaController:Landroid/widget/MediaController;

    return-object v0
.end method

.method static synthetic access$8(Lcom/konka/videoPlayer/VideoView;)I
    .locals 1

    iget v0, p0, Lcom/konka/videoPlayer/VideoView;->mSurfaceWidth:I

    return v0
.end method

.method static synthetic access$9(Lcom/konka/videoPlayer/VideoView;)I
    .locals 1

    iget v0, p0, Lcom/konka/videoPlayer/VideoView;->mSurfaceHeight:I

    return v0
.end method

.method private attachMediaController()V
    .locals 3

    iget-object v1, p0, Lcom/konka/videoPlayer/VideoView;->mMediaPlayer:Landroid/media/MediaPlayer;

    if-eqz v1, :cond_0

    iget-object v1, p0, Lcom/konka/videoPlayer/VideoView;->mMediaController:Landroid/widget/MediaController;

    if-eqz v1, :cond_0

    iget-object v1, p0, Lcom/konka/videoPlayer/VideoView;->mMediaController:Landroid/widget/MediaController;

    invoke-virtual {v1, p0}, Landroid/widget/MediaController;->setMediaPlayer(Landroid/widget/MediaController$MediaPlayerControl;)V

    invoke-virtual {p0}, Lcom/konka/videoPlayer/VideoView;->getParent()Landroid/view/ViewParent;

    move-result-object v1

    instance-of v1, v1, Landroid/view/View;

    if-eqz v1, :cond_1

    invoke-virtual {p0}, Lcom/konka/videoPlayer/VideoView;->getParent()Landroid/view/ViewParent;

    move-result-object v1

    check-cast v1, Landroid/view/View;

    move-object v0, v1

    :goto_0
    iget-object v1, p0, Lcom/konka/videoPlayer/VideoView;->mMediaController:Landroid/widget/MediaController;

    invoke-virtual {v1, v0}, Landroid/widget/MediaController;->setAnchorView(Landroid/view/View;)V

    iget-object v1, p0, Lcom/konka/videoPlayer/VideoView;->mMediaController:Landroid/widget/MediaController;

    iget-boolean v2, p0, Lcom/konka/videoPlayer/VideoView;->mIsPrepared:Z

    invoke-virtual {v1, v2}, Landroid/widget/MediaController;->setEnabled(Z)V

    :cond_0
    return-void

    :cond_1
    move-object v0, p0

    goto :goto_0
.end method

.method private initVideoView()V
    .locals 3

    const/4 v2, 0x1

    const/4 v0, 0x0

    iput v0, p0, Lcom/konka/videoPlayer/VideoView;->mVideoWidth:I

    iput v0, p0, Lcom/konka/videoPlayer/VideoView;->mVideoHeight:I

    invoke-virtual {p0}, Lcom/konka/videoPlayer/VideoView;->getHolder()Landroid/view/SurfaceHolder;

    move-result-object v0

    iget-object v1, p0, Lcom/konka/videoPlayer/VideoView;->mSHCallback:Landroid/view/SurfaceHolder$Callback;

    invoke-interface {v0, v1}, Landroid/view/SurfaceHolder;->addCallback(Landroid/view/SurfaceHolder$Callback;)V

    invoke-virtual {p0}, Lcom/konka/videoPlayer/VideoView;->getHolder()Landroid/view/SurfaceHolder;

    move-result-object v0

    const/4 v1, 0x3

    invoke-interface {v0, v1}, Landroid/view/SurfaceHolder;->setType(I)V

    invoke-virtual {p0, v2}, Lcom/konka/videoPlayer/VideoView;->setFocusable(Z)V

    invoke-virtual {p0, v2}, Lcom/konka/videoPlayer/VideoView;->setFocusableInTouchMode(Z)V

    invoke-virtual {p0}, Lcom/konka/videoPlayer/VideoView;->requestFocus()Z

    return-void
.end method

.method private openSegVideo(I)V
    .locals 5
    .param p1    # I

    iget-object v2, p0, Lcom/konka/videoPlayer/VideoView;->mUri:Landroid/net/Uri;

    if-eqz v2, :cond_0

    iget-object v2, p0, Lcom/konka/videoPlayer/VideoView;->mSurfaceHolder:Landroid/view/SurfaceHolder;

    if-nez v2, :cond_1

    :cond_0
    :goto_0
    return-void

    :cond_1
    new-instance v1, Landroid/content/Intent;

    const-string v2, "com.android.music.musicservicecommand"

    invoke-direct {v1, v2}, Landroid/content/Intent;-><init>(Ljava/lang/String;)V

    const-string v2, "command"

    const-string v3, "pause"

    invoke-virtual {v1, v2, v3}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    iget-object v2, p0, Lcom/konka/videoPlayer/VideoView;->mContext:Landroid/content/Context;

    invoke-virtual {v2, v1}, Landroid/content/Context;->sendBroadcast(Landroid/content/Intent;)V

    :try_start_0
    iget-object v2, p0, Lcom/konka/videoPlayer/VideoView;->mMediaPlayer:Landroid/media/MediaPlayer;

    if-nez v2, :cond_2

    new-instance v2, Landroid/media/MediaPlayer;

    invoke-direct {v2}, Landroid/media/MediaPlayer;-><init>()V

    iput-object v2, p0, Lcom/konka/videoPlayer/VideoView;->mMediaPlayer:Landroid/media/MediaPlayer;

    :cond_2
    iget-object v2, p0, Lcom/konka/videoPlayer/VideoView;->mMediaPlayer:Landroid/media/MediaPlayer;

    invoke-virtual {v2}, Landroid/media/MediaPlayer;->reset()V

    iget-object v2, p0, Lcom/konka/videoPlayer/VideoView;->mMediaPlayer:Landroid/media/MediaPlayer;

    iget-object v3, p0, Lcom/konka/videoPlayer/VideoView;->mPreparedListener:Landroid/media/MediaPlayer$OnPreparedListener;

    invoke-virtual {v2, v3}, Landroid/media/MediaPlayer;->setOnPreparedListener(Landroid/media/MediaPlayer$OnPreparedListener;)V

    iget-object v2, p0, Lcom/konka/videoPlayer/VideoView;->mMediaPlayer:Landroid/media/MediaPlayer;

    iget-object v3, p0, Lcom/konka/videoPlayer/VideoView;->mSizeChangedListener:Landroid/media/MediaPlayer$OnVideoSizeChangedListener;

    invoke-virtual {v2, v3}, Landroid/media/MediaPlayer;->setOnVideoSizeChangedListener(Landroid/media/MediaPlayer$OnVideoSizeChangedListener;)V

    const/4 v2, 0x0

    iput-boolean v2, p0, Lcom/konka/videoPlayer/VideoView;->mIsPrepared:Z

    const-string v2, "weikan"

    const-string v3, "reset duration to -1 in openVideo"

    invoke-static {v2, v3}, Landroid/util/Log;->v(Ljava/lang/String;Ljava/lang/String;)I

    const/4 v2, -0x1

    iput v2, p0, Lcom/konka/videoPlayer/VideoView;->mDuration:I

    iget-object v2, p0, Lcom/konka/videoPlayer/VideoView;->mMediaPlayer:Landroid/media/MediaPlayer;

    iget-object v3, p0, Lcom/konka/videoPlayer/VideoView;->mCompletionListener:Landroid/media/MediaPlayer$OnCompletionListener;

    invoke-virtual {v2, v3}, Landroid/media/MediaPlayer;->setOnCompletionListener(Landroid/media/MediaPlayer$OnCompletionListener;)V

    iget-object v2, p0, Lcom/konka/videoPlayer/VideoView;->mMediaPlayer:Landroid/media/MediaPlayer;

    iget-object v3, p0, Lcom/konka/videoPlayer/VideoView;->mErrorListener:Landroid/media/MediaPlayer$OnErrorListener;

    invoke-virtual {v2, v3}, Landroid/media/MediaPlayer;->setOnErrorListener(Landroid/media/MediaPlayer$OnErrorListener;)V

    iget-object v2, p0, Lcom/konka/videoPlayer/VideoView;->mMediaPlayer:Landroid/media/MediaPlayer;

    iget-object v3, p0, Lcom/konka/videoPlayer/VideoView;->mBufferingUpdateListener:Landroid/media/MediaPlayer$OnBufferingUpdateListener;

    invoke-virtual {v2, v3}, Landroid/media/MediaPlayer;->setOnBufferingUpdateListener(Landroid/media/MediaPlayer$OnBufferingUpdateListener;)V

    iget-object v2, p0, Lcom/konka/videoPlayer/VideoView;->mMediaPlayer:Landroid/media/MediaPlayer;

    iget-object v3, p0, Lcom/konka/videoPlayer/VideoView;->mSeekCompleteListener:Landroid/media/MediaPlayer$OnSeekCompleteListener;

    invoke-virtual {v2, v3}, Landroid/media/MediaPlayer;->setOnSeekCompleteListener(Landroid/media/MediaPlayer$OnSeekCompleteListener;)V

    iget-object v2, p0, Lcom/konka/videoPlayer/VideoView;->mMediaPlayer:Landroid/media/MediaPlayer;

    iget-object v3, p0, Lcom/konka/videoPlayer/VideoView;->mOnInfoListener:Landroid/media/MediaPlayer$OnInfoListener;

    invoke-virtual {v2, v3}, Landroid/media/MediaPlayer;->setOnInfoListener(Landroid/media/MediaPlayer$OnInfoListener;)V

    const/4 v2, 0x0

    iput v2, p0, Lcom/konka/videoPlayer/VideoView;->mCurrentBufferPercentage:I

    iget-object v2, p0, Lcom/konka/videoPlayer/VideoView;->mMediaPlayer:Landroid/media/MediaPlayer;

    iget-object v3, p0, Lcom/konka/videoPlayer/VideoView;->mContext:Landroid/content/Context;

    iget-object v4, p0, Lcom/konka/videoPlayer/VideoView;->mUri:Landroid/net/Uri;

    invoke-virtual {v2, v3, v4}, Landroid/media/MediaPlayer;->setDataSource(Landroid/content/Context;Landroid/net/Uri;)V

    iget-object v2, p0, Lcom/konka/videoPlayer/VideoView;->mMediaPlayer:Landroid/media/MediaPlayer;

    iget-object v3, p0, Lcom/konka/videoPlayer/VideoView;->mSurfaceHolder:Landroid/view/SurfaceHolder;

    invoke-virtual {v2, v3}, Landroid/media/MediaPlayer;->setDisplay(Landroid/view/SurfaceHolder;)V

    iget-object v2, p0, Lcom/konka/videoPlayer/VideoView;->mMediaPlayer:Landroid/media/MediaPlayer;

    const/4 v3, 0x3

    invoke-virtual {v2, v3}, Landroid/media/MediaPlayer;->setAudioStreamType(I)V

    iget-object v2, p0, Lcom/konka/videoPlayer/VideoView;->mMediaPlayer:Landroid/media/MediaPlayer;

    const/4 v3, 0x1

    invoke-virtual {v2, v3}, Landroid/media/MediaPlayer;->setScreenOnWhilePlaying(Z)V

    iget-object v2, p0, Lcom/konka/videoPlayer/VideoView;->mMediaPlayer:Landroid/media/MediaPlayer;

    invoke-virtual {v2}, Landroid/media/MediaPlayer;->prepareAsync()V

    invoke-direct {p0}, Lcom/konka/videoPlayer/VideoView;->attachMediaController()V
    :try_end_0
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_0
    .catch Ljava/lang/IllegalArgumentException; {:try_start_0 .. :try_end_0} :catch_1

    goto/16 :goto_0

    :catch_0
    move-exception v0

    iget-object v2, p0, Lcom/konka/videoPlayer/VideoView;->TAG:Ljava/lang/String;

    new-instance v3, Ljava/lang/StringBuilder;

    const-string v4, "Unable to open content: "

    invoke-direct {v3, v4}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    iget-object v4, p0, Lcom/konka/videoPlayer/VideoView;->mUri:Landroid/net/Uri;

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v2, v3, v0}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I

    goto/16 :goto_0

    :catch_1
    move-exception v0

    iget-object v2, p0, Lcom/konka/videoPlayer/VideoView;->TAG:Ljava/lang/String;

    new-instance v3, Ljava/lang/StringBuilder;

    const-string v4, "Unable to open content: "

    invoke-direct {v3, v4}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    iget-object v4, p0, Lcom/konka/videoPlayer/VideoView;->mUri:Landroid/net/Uri;

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v2, v3, v0}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I

    goto/16 :goto_0
.end method

.method private openVideo()V
    .locals 5

    iget-object v2, p0, Lcom/konka/videoPlayer/VideoView;->mUri:Landroid/net/Uri;

    if-eqz v2, :cond_0

    iget-object v2, p0, Lcom/konka/videoPlayer/VideoView;->mSurfaceHolder:Landroid/view/SurfaceHolder;

    if-nez v2, :cond_1

    :cond_0
    :goto_0
    return-void

    :cond_1
    new-instance v1, Landroid/content/Intent;

    const-string v2, "com.android.music.musicservicecommand"

    invoke-direct {v1, v2}, Landroid/content/Intent;-><init>(Ljava/lang/String;)V

    const-string v2, "command"

    const-string v3, "pause"

    invoke-virtual {v1, v2, v3}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    iget-object v2, p0, Lcom/konka/videoPlayer/VideoView;->mContext:Landroid/content/Context;

    invoke-virtual {v2, v1}, Landroid/content/Context;->sendBroadcast(Landroid/content/Intent;)V

    iget-object v2, p0, Lcom/konka/videoPlayer/VideoView;->mMediaPlayer:Landroid/media/MediaPlayer;

    if-eqz v2, :cond_2

    iget-object v2, p0, Lcom/konka/videoPlayer/VideoView;->mMediaPlayer:Landroid/media/MediaPlayer;

    invoke-virtual {v2}, Landroid/media/MediaPlayer;->reset()V

    iget-object v2, p0, Lcom/konka/videoPlayer/VideoView;->mMediaPlayer:Landroid/media/MediaPlayer;

    invoke-virtual {v2}, Landroid/media/MediaPlayer;->release()V

    const/4 v2, 0x0

    iput-object v2, p0, Lcom/konka/videoPlayer/VideoView;->mMediaPlayer:Landroid/media/MediaPlayer;

    :cond_2
    :try_start_0
    new-instance v2, Landroid/media/MediaPlayer;

    invoke-direct {v2}, Landroid/media/MediaPlayer;-><init>()V

    iput-object v2, p0, Lcom/konka/videoPlayer/VideoView;->mMediaPlayer:Landroid/media/MediaPlayer;

    iget-object v2, p0, Lcom/konka/videoPlayer/VideoView;->mMediaPlayer:Landroid/media/MediaPlayer;

    iget-object v3, p0, Lcom/konka/videoPlayer/VideoView;->mPreparedListener:Landroid/media/MediaPlayer$OnPreparedListener;

    invoke-virtual {v2, v3}, Landroid/media/MediaPlayer;->setOnPreparedListener(Landroid/media/MediaPlayer$OnPreparedListener;)V

    iget-object v2, p0, Lcom/konka/videoPlayer/VideoView;->mMediaPlayer:Landroid/media/MediaPlayer;

    iget-object v3, p0, Lcom/konka/videoPlayer/VideoView;->mSizeChangedListener:Landroid/media/MediaPlayer$OnVideoSizeChangedListener;

    invoke-virtual {v2, v3}, Landroid/media/MediaPlayer;->setOnVideoSizeChangedListener(Landroid/media/MediaPlayer$OnVideoSizeChangedListener;)V

    const/4 v2, 0x0

    iput-boolean v2, p0, Lcom/konka/videoPlayer/VideoView;->mIsPrepared:Z

    const-string v2, "weikan"

    const-string v3, "reset duration to -1 in openVideo"

    invoke-static {v2, v3}, Landroid/util/Log;->v(Ljava/lang/String;Ljava/lang/String;)I

    const/4 v2, -0x1

    iput v2, p0, Lcom/konka/videoPlayer/VideoView;->mDuration:I

    iget-object v2, p0, Lcom/konka/videoPlayer/VideoView;->mMediaPlayer:Landroid/media/MediaPlayer;

    iget-object v3, p0, Lcom/konka/videoPlayer/VideoView;->mCompletionListener:Landroid/media/MediaPlayer$OnCompletionListener;

    invoke-virtual {v2, v3}, Landroid/media/MediaPlayer;->setOnCompletionListener(Landroid/media/MediaPlayer$OnCompletionListener;)V

    iget-object v2, p0, Lcom/konka/videoPlayer/VideoView;->mMediaPlayer:Landroid/media/MediaPlayer;

    iget-object v3, p0, Lcom/konka/videoPlayer/VideoView;->mErrorListener:Landroid/media/MediaPlayer$OnErrorListener;

    invoke-virtual {v2, v3}, Landroid/media/MediaPlayer;->setOnErrorListener(Landroid/media/MediaPlayer$OnErrorListener;)V

    iget-object v2, p0, Lcom/konka/videoPlayer/VideoView;->mMediaPlayer:Landroid/media/MediaPlayer;

    iget-object v3, p0, Lcom/konka/videoPlayer/VideoView;->mBufferingUpdateListener:Landroid/media/MediaPlayer$OnBufferingUpdateListener;

    invoke-virtual {v2, v3}, Landroid/media/MediaPlayer;->setOnBufferingUpdateListener(Landroid/media/MediaPlayer$OnBufferingUpdateListener;)V

    iget-object v2, p0, Lcom/konka/videoPlayer/VideoView;->mMediaPlayer:Landroid/media/MediaPlayer;

    iget-object v3, p0, Lcom/konka/videoPlayer/VideoView;->mSeekCompleteListener:Landroid/media/MediaPlayer$OnSeekCompleteListener;

    invoke-virtual {v2, v3}, Landroid/media/MediaPlayer;->setOnSeekCompleteListener(Landroid/media/MediaPlayer$OnSeekCompleteListener;)V

    iget-object v2, p0, Lcom/konka/videoPlayer/VideoView;->mMediaPlayer:Landroid/media/MediaPlayer;

    iget-object v3, p0, Lcom/konka/videoPlayer/VideoView;->mOnInfoListener:Landroid/media/MediaPlayer$OnInfoListener;

    invoke-virtual {v2, v3}, Landroid/media/MediaPlayer;->setOnInfoListener(Landroid/media/MediaPlayer$OnInfoListener;)V

    const/4 v2, 0x0

    iput v2, p0, Lcom/konka/videoPlayer/VideoView;->mCurrentBufferPercentage:I

    iget-object v2, p0, Lcom/konka/videoPlayer/VideoView;->mMediaPlayer:Landroid/media/MediaPlayer;

    iget-object v3, p0, Lcom/konka/videoPlayer/VideoView;->mContext:Landroid/content/Context;

    iget-object v4, p0, Lcom/konka/videoPlayer/VideoView;->mUri:Landroid/net/Uri;

    invoke-virtual {v2, v3, v4}, Landroid/media/MediaPlayer;->setDataSource(Landroid/content/Context;Landroid/net/Uri;)V

    iget-object v2, p0, Lcom/konka/videoPlayer/VideoView;->mMediaPlayer:Landroid/media/MediaPlayer;

    iget-object v3, p0, Lcom/konka/videoPlayer/VideoView;->mSurfaceHolder:Landroid/view/SurfaceHolder;

    invoke-virtual {v2, v3}, Landroid/media/MediaPlayer;->setDisplay(Landroid/view/SurfaceHolder;)V

    iget-object v2, p0, Lcom/konka/videoPlayer/VideoView;->mMediaPlayer:Landroid/media/MediaPlayer;

    const/4 v3, 0x3

    invoke-virtual {v2, v3}, Landroid/media/MediaPlayer;->setAudioStreamType(I)V

    iget-object v2, p0, Lcom/konka/videoPlayer/VideoView;->mMediaPlayer:Landroid/media/MediaPlayer;

    const/4 v3, 0x1

    invoke-virtual {v2, v3}, Landroid/media/MediaPlayer;->setScreenOnWhilePlaying(Z)V

    iget-object v2, p0, Lcom/konka/videoPlayer/VideoView;->mMediaPlayer:Landroid/media/MediaPlayer;

    invoke-virtual {v2}, Landroid/media/MediaPlayer;->prepareAsync()V

    invoke-direct {p0}, Lcom/konka/videoPlayer/VideoView;->attachMediaController()V
    :try_end_0
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_0
    .catch Ljava/lang/IllegalArgumentException; {:try_start_0 .. :try_end_0} :catch_1

    goto/16 :goto_0

    :catch_0
    move-exception v0

    iget-object v2, p0, Lcom/konka/videoPlayer/VideoView;->TAG:Ljava/lang/String;

    new-instance v3, Ljava/lang/StringBuilder;

    const-string v4, "Unable to open content: "

    invoke-direct {v3, v4}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    iget-object v4, p0, Lcom/konka/videoPlayer/VideoView;->mUri:Landroid/net/Uri;

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v2, v3, v0}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I

    goto/16 :goto_0

    :catch_1
    move-exception v0

    iget-object v2, p0, Lcom/konka/videoPlayer/VideoView;->TAG:Ljava/lang/String;

    new-instance v3, Ljava/lang/StringBuilder;

    const-string v4, "Unable to open content: "

    invoke-direct {v3, v4}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    iget-object v4, p0, Lcom/konka/videoPlayer/VideoView;->mUri:Landroid/net/Uri;

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v2, v3, v0}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I

    goto/16 :goto_0
.end method

.method private toggleMediaControlsVisiblity()V
    .locals 1

    iget-object v0, p0, Lcom/konka/videoPlayer/VideoView;->mMediaController:Landroid/widget/MediaController;

    invoke-virtual {v0}, Landroid/widget/MediaController;->isShowing()Z

    move-result v0

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/konka/videoPlayer/VideoView;->mMediaController:Landroid/widget/MediaController;

    invoke-virtual {v0}, Landroid/widget/MediaController;->hide()V

    :goto_0
    return-void

    :cond_0
    iget-object v0, p0, Lcom/konka/videoPlayer/VideoView;->mMediaController:Landroid/widget/MediaController;

    invoke-virtual {v0}, Landroid/widget/MediaController;->show()V

    goto :goto_0
.end method


# virtual methods
.method public calculateZoom(DDDD)D
    .locals 9
    .param p1    # D
    .param p3    # D
    .param p5    # D
    .param p7    # D

    const-wide/high16 v0, 0x3fe0000000000000L    # 0.5

    const-string v6, ">>======="

    const-string v7, "33333333"

    invoke-static {v6, v7}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    cmpl-double v6, p1, p5

    if-ltz v6, :cond_0

    cmpl-double v6, p3, p7

    if-lez v6, :cond_0

    div-double v4, p1, p5

    div-double v2, p3, p7

    cmpl-double v6, v4, v2

    if-lez v6, :cond_1

    move-wide v0, v2

    :cond_0
    :goto_0
    const-string v6, ">>========="

    new-instance v7, Ljava/lang/StringBuilder;

    const-string v8, "dRet======="

    invoke-direct {v7, v8}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v7, v0, v1}, Ljava/lang/StringBuilder;->append(D)Ljava/lang/StringBuilder;

    move-result-object v7

    invoke-virtual {v7}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v7

    invoke-static {v6, v7}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    return-wide v0

    :cond_1
    move-wide v0, v4

    goto :goto_0
.end method

.method public canPause()Z
    .locals 1

    const/4 v0, 0x0

    return v0
.end method

.method public canSeekBackward()Z
    .locals 1

    const/4 v0, 0x0

    return v0
.end method

.method public canSeekForward()Z
    .locals 1

    const/4 v0, 0x0

    return v0
.end method

.method public getAudioTrackInfo(Z)Lcom/mstar/android/media/AudioTrackInfo;
    .locals 1
    .param p1    # Z

    const/4 v0, 0x0

    return-object v0
.end method

.method public getBufferPercentage()I
    .locals 1

    iget-object v0, p0, Lcom/konka/videoPlayer/VideoView;->mMediaPlayer:Landroid/media/MediaPlayer;

    if-eqz v0, :cond_0

    iget v0, p0, Lcom/konka/videoPlayer/VideoView;->mCurrentBufferPercentage:I

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public getCurrentPosition()I
    .locals 1

    iget-object v0, p0, Lcom/konka/videoPlayer/VideoView;->mMediaPlayer:Landroid/media/MediaPlayer;

    if-eqz v0, :cond_0

    iget-boolean v0, p0, Lcom/konka/videoPlayer/VideoView;->mIsPrepared:Z

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/konka/videoPlayer/VideoView;->mMediaPlayer:Landroid/media/MediaPlayer;

    invoke-virtual {v0}, Landroid/media/MediaPlayer;->getCurrentPosition()I

    move-result v0

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public getDuration()I
    .locals 1

    iget-object v0, p0, Lcom/konka/videoPlayer/VideoView;->mMediaPlayer:Landroid/media/MediaPlayer;

    if-eqz v0, :cond_1

    iget-boolean v0, p0, Lcom/konka/videoPlayer/VideoView;->mIsPrepared:Z

    if-eqz v0, :cond_1

    iget v0, p0, Lcom/konka/videoPlayer/VideoView;->mDuration:I

    if-lez v0, :cond_0

    iget v0, p0, Lcom/konka/videoPlayer/VideoView;->mDuration:I

    :goto_0
    return v0

    :cond_0
    iget-object v0, p0, Lcom/konka/videoPlayer/VideoView;->mMediaPlayer:Landroid/media/MediaPlayer;

    invoke-virtual {v0}, Landroid/media/MediaPlayer;->getDuration()I

    move-result v0

    iput v0, p0, Lcom/konka/videoPlayer/VideoView;->mDuration:I

    iget v0, p0, Lcom/konka/videoPlayer/VideoView;->mDuration:I

    goto :goto_0

    :cond_1
    const/4 v0, -0x1

    iput v0, p0, Lcom/konka/videoPlayer/VideoView;->mDuration:I

    iget v0, p0, Lcom/konka/videoPlayer/VideoView;->mDuration:I

    goto :goto_0
.end method

.method public getSubtitleData()Ljava/lang/String;
    .locals 1

    const-string v0, ""

    return-object v0
.end method

.method public getVideoHeight()I
    .locals 1

    iget v0, p0, Lcom/konka/videoPlayer/VideoView;->mVideoHeight:I

    return v0
.end method

.method public getVideoPath()Ljava/lang/String;
    .locals 1

    iget-object v0, p0, Lcom/konka/videoPlayer/VideoView;->mUri:Landroid/net/Uri;

    invoke-virtual {v0}, Landroid/net/Uri;->getPath()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public getVideoWidth()I
    .locals 1

    iget v0, p0, Lcom/konka/videoPlayer/VideoView;->mVideoWidth:I

    return v0
.end method

.method public isPlayerSane()Z
    .locals 1

    iget-object v0, p0, Lcom/konka/videoPlayer/VideoView;->mMediaPlayer:Landroid/media/MediaPlayer;

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public isPlaying()Z
    .locals 1

    iget-object v0, p0, Lcom/konka/videoPlayer/VideoView;->mMediaPlayer:Landroid/media/MediaPlayer;

    if-eqz v0, :cond_0

    iget-boolean v0, p0, Lcom/konka/videoPlayer/VideoView;->mIsPrepared:Z

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/konka/videoPlayer/VideoView;->mMediaPlayer:Landroid/media/MediaPlayer;

    invoke-virtual {v0}, Landroid/media/MediaPlayer;->isPlaying()Z

    move-result v0

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public offSubtitleTrack()V
    .locals 0

    return-void
.end method

.method public onKeyDown(ILandroid/view/KeyEvent;)Z
    .locals 1
    .param p1    # I
    .param p2    # Landroid/view/KeyEvent;

    iget-boolean v0, p0, Lcom/konka/videoPlayer/VideoView;->mIsPrepared:Z

    if-eqz v0, :cond_3

    const/4 v0, 0x4

    if-eq p1, v0, :cond_3

    const/16 v0, 0x18

    if-eq p1, v0, :cond_3

    const/16 v0, 0x19

    if-eq p1, v0, :cond_3

    const/16 v0, 0x52

    if-eq p1, v0, :cond_3

    const/4 v0, 0x5

    if-eq p1, v0, :cond_3

    const/4 v0, 0x6

    if-eq p1, v0, :cond_3

    iget-object v0, p0, Lcom/konka/videoPlayer/VideoView;->mMediaPlayer:Landroid/media/MediaPlayer;

    if-eqz v0, :cond_3

    iget-object v0, p0, Lcom/konka/videoPlayer/VideoView;->mMediaController:Landroid/widget/MediaController;

    if-eqz v0, :cond_3

    const/16 v0, 0x4f

    if-eq p1, v0, :cond_0

    const/16 v0, 0x55

    if-ne p1, v0, :cond_2

    :cond_0
    iget-object v0, p0, Lcom/konka/videoPlayer/VideoView;->mMediaPlayer:Landroid/media/MediaPlayer;

    invoke-virtual {v0}, Landroid/media/MediaPlayer;->isPlaying()Z

    move-result v0

    if-eqz v0, :cond_1

    invoke-virtual {p0}, Lcom/konka/videoPlayer/VideoView;->pause()V

    iget-object v0, p0, Lcom/konka/videoPlayer/VideoView;->mMediaController:Landroid/widget/MediaController;

    invoke-virtual {v0}, Landroid/widget/MediaController;->show()V

    :goto_0
    const/4 v0, 0x1

    :goto_1
    return v0

    :cond_1
    invoke-virtual {p0}, Lcom/konka/videoPlayer/VideoView;->start()V

    iget-object v0, p0, Lcom/konka/videoPlayer/VideoView;->mMediaController:Landroid/widget/MediaController;

    invoke-virtual {v0}, Landroid/widget/MediaController;->hide()V

    goto :goto_0

    :cond_2
    const/16 v0, 0x56

    if-ne p1, v0, :cond_4

    iget-object v0, p0, Lcom/konka/videoPlayer/VideoView;->mMediaPlayer:Landroid/media/MediaPlayer;

    invoke-virtual {v0}, Landroid/media/MediaPlayer;->isPlaying()Z

    move-result v0

    if-eqz v0, :cond_4

    invoke-virtual {p0}, Lcom/konka/videoPlayer/VideoView;->pause()V

    iget-object v0, p0, Lcom/konka/videoPlayer/VideoView;->mMediaController:Landroid/widget/MediaController;

    invoke-virtual {v0}, Landroid/widget/MediaController;->show()V

    :cond_3
    :goto_2
    invoke-super {p0, p1, p2}, Landroid/view/SurfaceView;->onKeyDown(ILandroid/view/KeyEvent;)Z

    move-result v0

    goto :goto_1

    :cond_4
    invoke-direct {p0}, Lcom/konka/videoPlayer/VideoView;->toggleMediaControlsVisiblity()V

    goto :goto_2
.end method

.method protected onMeasure(II)V
    .locals 3
    .param p1    # I
    .param p2    # I

    iget v2, p0, Lcom/konka/videoPlayer/VideoView;->mVideoWidth:I

    invoke-static {v2, p1}, Lcom/konka/videoPlayer/VideoView;->getDefaultSize(II)I

    move-result v1

    iget v2, p0, Lcom/konka/videoPlayer/VideoView;->mVideoHeight:I

    invoke-static {v2, p2}, Lcom/konka/videoPlayer/VideoView;->getDefaultSize(II)I

    move-result v0

    invoke-virtual {p0, v1, v0}, Lcom/konka/videoPlayer/VideoView;->setMeasuredDimension(II)V

    return-void
.end method

.method public onSubtitleTrack()V
    .locals 0

    return-void
.end method

.method public onTouchEvent(Landroid/view/MotionEvent;)Z
    .locals 1
    .param p1    # Landroid/view/MotionEvent;

    iget-boolean v0, p0, Lcom/konka/videoPlayer/VideoView;->mIsPrepared:Z

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/konka/videoPlayer/VideoView;->mMediaPlayer:Landroid/media/MediaPlayer;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/konka/videoPlayer/VideoView;->mMediaController:Landroid/widget/MediaController;

    if-eqz v0, :cond_0

    invoke-direct {p0}, Lcom/konka/videoPlayer/VideoView;->toggleMediaControlsVisiblity()V

    :cond_0
    const/4 v0, 0x0

    return v0
.end method

.method public onTrackballEvent(Landroid/view/MotionEvent;)Z
    .locals 1
    .param p1    # Landroid/view/MotionEvent;

    iget-boolean v0, p0, Lcom/konka/videoPlayer/VideoView;->mIsPrepared:Z

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/konka/videoPlayer/VideoView;->mMediaPlayer:Landroid/media/MediaPlayer;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/konka/videoPlayer/VideoView;->mMediaController:Landroid/widget/MediaController;

    if-eqz v0, :cond_0

    invoke-direct {p0}, Lcom/konka/videoPlayer/VideoView;->toggleMediaControlsVisiblity()V

    :cond_0
    const/4 v0, 0x0

    return v0
.end method

.method public pause()V
    .locals 1

    iget-object v0, p0, Lcom/konka/videoPlayer/VideoView;->mMediaPlayer:Landroid/media/MediaPlayer;

    if-eqz v0, :cond_0

    iget-boolean v0, p0, Lcom/konka/videoPlayer/VideoView;->mIsPrepared:Z

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/konka/videoPlayer/VideoView;->mMediaPlayer:Landroid/media/MediaPlayer;

    invoke-virtual {v0}, Landroid/media/MediaPlayer;->isPlaying()Z

    move-result v0

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/konka/videoPlayer/VideoView;->mMediaPlayer:Landroid/media/MediaPlayer;

    invoke-virtual {v0}, Landroid/media/MediaPlayer;->pause()V

    :cond_0
    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/konka/videoPlayer/VideoView;->mStartWhenPrepared:Z

    return-void
.end method

.method public reset()V
    .locals 1

    iget-object v0, p0, Lcom/konka/videoPlayer/VideoView;->mMediaPlayer:Landroid/media/MediaPlayer;

    if-eqz v0, :cond_0

    iget-boolean v0, p0, Lcom/konka/videoPlayer/VideoView;->mIsPrepared:Z

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/konka/videoPlayer/VideoView;->mMediaPlayer:Landroid/media/MediaPlayer;

    invoke-virtual {v0}, Landroid/media/MediaPlayer;->reset()V

    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/konka/videoPlayer/VideoView;->mStartWhenPrepared:Z

    :goto_0
    return-void

    :cond_0
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/konka/videoPlayer/VideoView;->mStartWhenPrepared:Z

    goto :goto_0
.end method

.method public resolveAdjustedSize(II)I
    .locals 3
    .param p1    # I
    .param p2    # I

    move v0, p1

    invoke-static {p2}, Landroid/view/View$MeasureSpec;->getMode(I)I

    move-result v1

    invoke-static {p2}, Landroid/view/View$MeasureSpec;->getSize(I)I

    move-result v2

    sparse-switch v1, :sswitch_data_0

    :goto_0
    return v0

    :sswitch_0
    move v0, p1

    goto :goto_0

    :sswitch_1
    invoke-static {p1, v2}, Ljava/lang/Math;->min(II)I

    move-result v0

    goto :goto_0

    :sswitch_2
    move v0, v2

    goto :goto_0

    :sswitch_data_0
    .sparse-switch
        -0x80000000 -> :sswitch_1
        0x0 -> :sswitch_0
        0x40000000 -> :sswitch_2
    .end sparse-switch
.end method

.method public seekTo(I)V
    .locals 1
    .param p1    # I

    iget-object v0, p0, Lcom/konka/videoPlayer/VideoView;->mMediaPlayer:Landroid/media/MediaPlayer;

    if-eqz v0, :cond_0

    iget-boolean v0, p0, Lcom/konka/videoPlayer/VideoView;->mIsPrepared:Z

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/konka/videoPlayer/VideoView;->mMediaPlayer:Landroid/media/MediaPlayer;

    invoke-virtual {v0, p1}, Landroid/media/MediaPlayer;->seekTo(I)V

    :goto_0
    return-void

    :cond_0
    iput p1, p0, Lcom/konka/videoPlayer/VideoView;->mSeekWhenPrepared:I

    goto :goto_0
.end method

.method public setAudioTrack(I)V
    .locals 0
    .param p1    # I

    return-void
.end method

.method public setMediaController(Landroid/widget/MediaController;)V
    .locals 1
    .param p1    # Landroid/widget/MediaController;

    iget-object v0, p0, Lcom/konka/videoPlayer/VideoView;->mMediaController:Landroid/widget/MediaController;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/konka/videoPlayer/VideoView;->mMediaController:Landroid/widget/MediaController;

    invoke-virtual {v0}, Landroid/widget/MediaController;->hide()V

    :cond_0
    iput-object p1, p0, Lcom/konka/videoPlayer/VideoView;->mMediaController:Landroid/widget/MediaController;

    invoke-direct {p0}, Lcom/konka/videoPlayer/VideoView;->attachMediaController()V

    return-void
.end method

.method public setMySizeChangeLinstener(Lcom/konka/videoPlayer/VideoView$MySizeChangeLinstener;)V
    .locals 0
    .param p1    # Lcom/konka/videoPlayer/VideoView$MySizeChangeLinstener;

    iput-object p1, p0, Lcom/konka/videoPlayer/VideoView;->mMyChangeLinstener:Lcom/konka/videoPlayer/VideoView$MySizeChangeLinstener;

    return-void
.end method

.method public setOnCompletionListener(Landroid/media/MediaPlayer$OnCompletionListener;)V
    .locals 0
    .param p1    # Landroid/media/MediaPlayer$OnCompletionListener;

    iput-object p1, p0, Lcom/konka/videoPlayer/VideoView;->mOnCompletionListener:Landroid/media/MediaPlayer$OnCompletionListener;

    return-void
.end method

.method public setOnErrorListener(Landroid/media/MediaPlayer$OnErrorListener;)V
    .locals 0
    .param p1    # Landroid/media/MediaPlayer$OnErrorListener;

    iput-object p1, p0, Lcom/konka/videoPlayer/VideoView;->mOnErrorListener:Landroid/media/MediaPlayer$OnErrorListener;

    return-void
.end method

.method public setOnInfoListener(Landroid/media/MediaPlayer$OnInfoListener;)V
    .locals 0
    .param p1    # Landroid/media/MediaPlayer$OnInfoListener;

    iput-object p1, p0, Lcom/konka/videoPlayer/VideoView;->mOnInfoListener:Landroid/media/MediaPlayer$OnInfoListener;

    return-void
.end method

.method public setOnPreparedListener(Landroid/media/MediaPlayer$OnPreparedListener;)V
    .locals 0
    .param p1    # Landroid/media/MediaPlayer$OnPreparedListener;

    iput-object p1, p0, Lcom/konka/videoPlayer/VideoView;->mOnPreparedListener:Landroid/media/MediaPlayer$OnPreparedListener;

    return-void
.end method

.method public setPG(Landroid/app/ProgressDialog;)V
    .locals 0
    .param p1    # Landroid/app/ProgressDialog;

    iput-object p1, p0, Lcom/konka/videoPlayer/VideoView;->pg:Landroid/app/ProgressDialog;

    return-void
.end method

.method public setSegVideoURI(Landroid/net/Uri;I)V
    .locals 1
    .param p1    # Landroid/net/Uri;
    .param p2    # I

    const/4 v0, 0x0

    iput-object p1, p0, Lcom/konka/videoPlayer/VideoView;->mUri:Landroid/net/Uri;

    iput-boolean v0, p0, Lcom/konka/videoPlayer/VideoView;->mStartWhenPrepared:Z

    iput v0, p0, Lcom/konka/videoPlayer/VideoView;->mSeekWhenPrepared:I

    invoke-direct {p0, p2}, Lcom/konka/videoPlayer/VideoView;->openSegVideo(I)V

    invoke-virtual {p0}, Lcom/konka/videoPlayer/VideoView;->requestLayout()V

    invoke-virtual {p0}, Lcom/konka/videoPlayer/VideoView;->invalidate()V

    return-void
.end method

.method public setSubtitleDataSource(Ljava/lang/String;)V
    .locals 0
    .param p1    # Ljava/lang/String;

    return-void
.end method

.method public setSubtitleDisplay(Landroid/view/SurfaceHolder;)V
    .locals 0
    .param p1    # Landroid/view/SurfaceHolder;

    return-void
.end method

.method public setSubtitleTrack(I)V
    .locals 0
    .param p1    # I

    return-void
.end method

.method public setVideoPath(Ljava/lang/String;)V
    .locals 1
    .param p1    # Ljava/lang/String;

    if-eqz p1, :cond_0

    invoke-static {p1}, Landroid/net/Uri;->parse(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v0

    invoke-virtual {p0, v0}, Lcom/konka/videoPlayer/VideoView;->setVideoURI(Landroid/net/Uri;)V

    :cond_0
    return-void
.end method

.method public setVideoScale(II)V
    .locals 3
    .param p1    # I
    .param p2    # I

    const-string v1, ">>======="

    const-string v2, "2222222"

    invoke-static {v1, v2}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    invoke-virtual {p0}, Lcom/konka/videoPlayer/VideoView;->getLayoutParams()Landroid/view/ViewGroup$LayoutParams;

    move-result-object v0

    iput p2, v0, Landroid/view/ViewGroup$LayoutParams;->height:I

    iput p1, v0, Landroid/view/ViewGroup$LayoutParams;->width:I

    invoke-virtual {p0, v0}, Lcom/konka/videoPlayer/VideoView;->setLayoutParams(Landroid/view/ViewGroup$LayoutParams;)V

    return-void
.end method

.method public setVideoScaleFrameLayout(IIII)V
    .locals 4
    .param p1    # I
    .param p2    # I
    .param p3    # I
    .param p4    # I

    const-string v2, ">>======="

    const-string v3, "11111111"

    invoke-static {v2, v3}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    invoke-virtual {p0}, Lcom/konka/videoPlayer/VideoView;->getLayoutParams()Landroid/view/ViewGroup$LayoutParams;

    move-result-object v0

    move-object v1, v0

    check-cast v1, Landroid/widget/RelativeLayout$LayoutParams;

    iput p1, v1, Landroid/widget/RelativeLayout$LayoutParams;->leftMargin:I

    iput p1, v1, Landroid/widget/RelativeLayout$LayoutParams;->rightMargin:I

    iput p2, v1, Landroid/widget/RelativeLayout$LayoutParams;->topMargin:I

    iput p2, v1, Landroid/widget/RelativeLayout$LayoutParams;->bottomMargin:I

    iput p3, v1, Landroid/widget/RelativeLayout$LayoutParams;->width:I

    iput p4, v1, Landroid/widget/RelativeLayout$LayoutParams;->height:I

    invoke-virtual {p0, v1}, Lcom/konka/videoPlayer/VideoView;->setLayoutParams(Landroid/view/ViewGroup$LayoutParams;)V

    return-void
.end method

.method public setVideoURI(Landroid/net/Uri;)V
    .locals 1
    .param p1    # Landroid/net/Uri;

    const/4 v0, 0x0

    iput-object p1, p0, Lcom/konka/videoPlayer/VideoView;->mUri:Landroid/net/Uri;

    iput-boolean v0, p0, Lcom/konka/videoPlayer/VideoView;->mStartWhenPrepared:Z

    iput v0, p0, Lcom/konka/videoPlayer/VideoView;->mSeekWhenPrepared:I

    invoke-direct {p0}, Lcom/konka/videoPlayer/VideoView;->openVideo()V

    invoke-virtual {p0}, Lcom/konka/videoPlayer/VideoView;->requestLayout()V

    invoke-virtual {p0}, Lcom/konka/videoPlayer/VideoView;->invalidate()V

    return-void
.end method

.method public start()V
    .locals 2

    new-instance v0, Ljava/lang/Thread;

    new-instance v1, Lcom/konka/videoPlayer/VideoView$8;

    invoke-direct {v1, p0}, Lcom/konka/videoPlayer/VideoView$8;-><init>(Lcom/konka/videoPlayer/VideoView;)V

    invoke-direct {v0, v1}, Ljava/lang/Thread;-><init>(Ljava/lang/Runnable;)V

    invoke-virtual {v0}, Ljava/lang/Thread;->start()V

    return-void
.end method

.method public stopPlayback()V
    .locals 1

    iget-object v0, p0, Lcom/konka/videoPlayer/VideoView;->mMediaPlayer:Landroid/media/MediaPlayer;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/konka/videoPlayer/VideoView;->mMediaPlayer:Landroid/media/MediaPlayer;

    invoke-virtual {v0}, Landroid/media/MediaPlayer;->stop()V

    iget-object v0, p0, Lcom/konka/videoPlayer/VideoView;->mMediaPlayer:Landroid/media/MediaPlayer;

    invoke-virtual {v0}, Landroid/media/MediaPlayer;->release()V

    const/4 v0, 0x0

    iput-object v0, p0, Lcom/konka/videoPlayer/VideoView;->mMediaPlayer:Landroid/media/MediaPlayer;

    :cond_0
    return-void
.end method
