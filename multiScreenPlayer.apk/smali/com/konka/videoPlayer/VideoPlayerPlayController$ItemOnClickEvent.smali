.class public Lcom/konka/videoPlayer/VideoPlayerPlayController$ItemOnClickEvent;
.super Ljava/lang/Object;
.source "VideoPlayerPlayController.java"

# interfaces
.implements Landroid/view/View$OnClickListener;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/konka/videoPlayer/VideoPlayerPlayController;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x1
    name = "ItemOnClickEvent"
.end annotation


# instance fields
.field final synthetic this$0:Lcom/konka/videoPlayer/VideoPlayerPlayController;


# direct methods
.method public constructor <init>(Lcom/konka/videoPlayer/VideoPlayerPlayController;)V
    .locals 0

    iput-object p1, p0, Lcom/konka/videoPlayer/VideoPlayerPlayController$ItemOnClickEvent;->this$0:Lcom/konka/videoPlayer/VideoPlayerPlayController;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onClick(Landroid/view/View;)V
    .locals 4
    .param p1    # Landroid/view/View;

    const/16 v3, 0x8

    const/4 v2, 0x0

    invoke-virtual {p1}, Landroid/view/View;->getId()I

    move-result v1

    packed-switch v1, :pswitch_data_0

    :goto_0
    :pswitch_0
    return-void

    :pswitch_1
    iget-object v1, p0, Lcom/konka/videoPlayer/VideoPlayerPlayController$ItemOnClickEvent;->this$0:Lcom/konka/videoPlayer/VideoPlayerPlayController;

    # getter for: Lcom/konka/videoPlayer/VideoPlayerPlayController;->mVideoPlayer:Lcom/konka/videoPlayer/videoPlayerActivity;
    invoke-static {v1}, Lcom/konka/videoPlayer/VideoPlayerPlayController;->access$0(Lcom/konka/videoPlayer/VideoPlayerPlayController;)Lcom/konka/videoPlayer/videoPlayerActivity;

    move-result-object v1

    invoke-virtual {v1}, Lcom/konka/videoPlayer/videoPlayerActivity;->setPlayState()V

    iget-object v1, p0, Lcom/konka/videoPlayer/VideoPlayerPlayController$ItemOnClickEvent;->this$0:Lcom/konka/videoPlayer/VideoPlayerPlayController;

    # getter for: Lcom/konka/videoPlayer/VideoPlayerPlayController;->buttonPlay:Landroid/widget/Button;
    invoke-static {v1}, Lcom/konka/videoPlayer/VideoPlayerPlayController;->access$1(Lcom/konka/videoPlayer/VideoPlayerPlayController;)Landroid/widget/Button;

    move-result-object v1

    invoke-virtual {v1, v3}, Landroid/widget/Button;->setVisibility(I)V

    iget-object v1, p0, Lcom/konka/videoPlayer/VideoPlayerPlayController$ItemOnClickEvent;->this$0:Lcom/konka/videoPlayer/VideoPlayerPlayController;

    # getter for: Lcom/konka/videoPlayer/VideoPlayerPlayController;->buttonPause:Landroid/widget/Button;
    invoke-static {v1}, Lcom/konka/videoPlayer/VideoPlayerPlayController;->access$2(Lcom/konka/videoPlayer/VideoPlayerPlayController;)Landroid/widget/Button;

    move-result-object v1

    invoke-virtual {v1, v2}, Landroid/widget/Button;->setVisibility(I)V

    goto :goto_0

    :pswitch_2
    iget-object v1, p0, Lcom/konka/videoPlayer/VideoPlayerPlayController$ItemOnClickEvent;->this$0:Lcom/konka/videoPlayer/VideoPlayerPlayController;

    # getter for: Lcom/konka/videoPlayer/VideoPlayerPlayController;->mVideoPlayer:Lcom/konka/videoPlayer/videoPlayerActivity;
    invoke-static {v1}, Lcom/konka/videoPlayer/VideoPlayerPlayController;->access$0(Lcom/konka/videoPlayer/VideoPlayerPlayController;)Lcom/konka/videoPlayer/videoPlayerActivity;

    move-result-object v1

    invoke-virtual {v1}, Lcom/konka/videoPlayer/videoPlayerActivity;->setPlayState()V

    iget-object v1, p0, Lcom/konka/videoPlayer/VideoPlayerPlayController$ItemOnClickEvent;->this$0:Lcom/konka/videoPlayer/VideoPlayerPlayController;

    # getter for: Lcom/konka/videoPlayer/VideoPlayerPlayController;->buttonPlay:Landroid/widget/Button;
    invoke-static {v1}, Lcom/konka/videoPlayer/VideoPlayerPlayController;->access$1(Lcom/konka/videoPlayer/VideoPlayerPlayController;)Landroid/widget/Button;

    move-result-object v1

    invoke-virtual {v1, v2}, Landroid/widget/Button;->setVisibility(I)V

    iget-object v1, p0, Lcom/konka/videoPlayer/VideoPlayerPlayController$ItemOnClickEvent;->this$0:Lcom/konka/videoPlayer/VideoPlayerPlayController;

    # getter for: Lcom/konka/videoPlayer/VideoPlayerPlayController;->buttonPause:Landroid/widget/Button;
    invoke-static {v1}, Lcom/konka/videoPlayer/VideoPlayerPlayController;->access$2(Lcom/konka/videoPlayer/VideoPlayerPlayController;)Landroid/widget/Button;

    move-result-object v1

    invoke-virtual {v1, v3}, Landroid/widget/Button;->setVisibility(I)V

    goto :goto_0

    :pswitch_3
    iget-object v1, p0, Lcom/konka/videoPlayer/VideoPlayerPlayController$ItemOnClickEvent;->this$0:Lcom/konka/videoPlayer/VideoPlayerPlayController;

    # getter for: Lcom/konka/videoPlayer/VideoPlayerPlayController;->mVideoPlayer:Lcom/konka/videoPlayer/videoPlayerActivity;
    invoke-static {v1}, Lcom/konka/videoPlayer/VideoPlayerPlayController;->access$0(Lcom/konka/videoPlayer/VideoPlayerPlayController;)Lcom/konka/videoPlayer/videoPlayerActivity;

    move-result-object v1

    const v2, 0x7f0900a5    # com.konka.mediaSharePlayer.R.string.forbidoption

    invoke-virtual {v1, v2}, Lcom/konka/videoPlayer/videoPlayerActivity;->toastMessage(I)V

    goto :goto_0

    :pswitch_4
    iget-object v1, p0, Lcom/konka/videoPlayer/VideoPlayerPlayController$ItemOnClickEvent;->this$0:Lcom/konka/videoPlayer/VideoPlayerPlayController;

    # getter for: Lcom/konka/videoPlayer/VideoPlayerPlayController;->seekbar:Landroid/widget/SeekBar;
    invoke-static {v1}, Lcom/konka/videoPlayer/VideoPlayerPlayController;->access$3(Lcom/konka/videoPlayer/VideoPlayerPlayController;)Landroid/widget/SeekBar;

    move-result-object v1

    invoke-virtual {v1}, Landroid/widget/SeekBar;->getProgress()I

    move-result v0

    iget-object v1, p0, Lcom/konka/videoPlayer/VideoPlayerPlayController$ItemOnClickEvent;->this$0:Lcom/konka/videoPlayer/VideoPlayerPlayController;

    # getter for: Lcom/konka/videoPlayer/VideoPlayerPlayController;->mVideoPlayer:Lcom/konka/videoPlayer/videoPlayerActivity;
    invoke-static {v1}, Lcom/konka/videoPlayer/VideoPlayerPlayController;->access$0(Lcom/konka/videoPlayer/VideoPlayerPlayController;)Lcom/konka/videoPlayer/videoPlayerActivity;

    move-result-object v1

    const/4 v2, 0x1

    invoke-virtual {v1, v0, v2}, Lcom/konka/videoPlayer/videoPlayerActivity;->videoPlayerSeek(II)V

    goto :goto_0

    nop

    :pswitch_data_0
    .packed-switch 0x7f0b0021
        :pswitch_3    # com.konka.mediaSharePlayer.R.id.video_btn_seekback
        :pswitch_1    # com.konka.mediaSharePlayer.R.id.video_btn_play
        :pswitch_2    # com.konka.mediaSharePlayer.R.id.video_btn_pause
        :pswitch_3    # com.konka.mediaSharePlayer.R.id.video_btn_seekforward
        :pswitch_0    # com.konka.mediaSharePlayer.R.id.video_playbar_time
        :pswitch_4    # com.konka.mediaSharePlayer.R.id.video_seekbar
    .end packed-switch
.end method
