.class public Lcom/konka/kkinterface/tv/DataBaseDesk$MS_USER_SOUND_SETTING;
.super Ljava/lang/Object;
.source "DataBaseDesk.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/konka/kkinterface/tv/DataBaseDesk;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x9
    name = "MS_USER_SOUND_SETTING"
.end annotation


# instance fields
.field public ADOutput:Lcom/konka/kkinterface/tv/DataBaseDesk$EN_SOUND_AD_OUTPUT;

.field public ADVolume:S

.field public AudysseyDynamicVolume:Lcom/konka/kkinterface/tv/DataBaseDesk$EN_AUDYSSEY_DYNAMIC_VOLUME_MODE;

.field public AudysseyEQ:Lcom/konka/kkinterface/tv/DataBaseDesk$EN_AUDYSSEY_EQ_MODE;

.field public Balance:S

.field public CH1PreScale:S

.field public HPVolume:S

.field public MUTE_Flag:S

.field public Primary_Flag:S

.field public SPDIF_Delay:S

.field public SoundMode:Lcom/konka/kkinterface/tv/DataBaseDesk$EN_SOUND_MODE;

.field public Speaker_Delay:S

.field public Surround:Lcom/konka/kkinterface/tv/DataBaseDesk$EN_SURROUND_TYPE;

.field public SurroundMode:Lcom/konka/kkinterface/tv/DataBaseDesk$EN_SURROUND_MODE;

.field public SurroundSoundMode:Lcom/konka/kkinterface/tv/DataBaseDesk$EN_SURROUND_SYSTEM_TYPE;

.field public Volume:S

.field public bEnableAD:Z

.field public bEnableAVC:Z

.field public enSoundAudioChannel:Lcom/konka/kkinterface/tv/DataBaseDesk$EN_AUD_MODE;

.field public enSoundAudioLan1:Lcom/mstar/android/tvapi/common/vo/TvOsType$EnumLanguage;

.field public enSoundAudioLan2:Lcom/mstar/android/tvapi/common/vo/TvOsType$EnumLanguage;

.field public hdmi1AudioSource:Lcom/konka/kkinterface/tv/DataBaseDesk$HdmiAudioSource;

.field public hdmi2AudioSource:Lcom/konka/kkinterface/tv/DataBaseDesk$HdmiAudioSource;

.field public hdmi3AudioSource:Lcom/konka/kkinterface/tv/DataBaseDesk$HdmiAudioSource;

.field public hdmi4AudioSource:Lcom/konka/kkinterface/tv/DataBaseDesk$HdmiAudioSource;

.field public spdifMode:Lcom/konka/kkinterface/tv/DataBaseDesk$EN_SPDIF_MODE;

.field public u16CheckSum:I


# direct methods
.method public constructor <init>()V
    .locals 1

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    sget-object v0, Lcom/konka/kkinterface/tv/DataBaseDesk$EN_SOUND_MODE;->SOUND_MODE_STANDARD:Lcom/konka/kkinterface/tv/DataBaseDesk$EN_SOUND_MODE;

    iput-object v0, p0, Lcom/konka/kkinterface/tv/DataBaseDesk$MS_USER_SOUND_SETTING;->SoundMode:Lcom/konka/kkinterface/tv/DataBaseDesk$EN_SOUND_MODE;

    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/konka/kkinterface/tv/DataBaseDesk$MS_USER_SOUND_SETTING;->bEnableAVC:Z

    sget-object v0, Lcom/konka/kkinterface/tv/DataBaseDesk$EN_SPDIF_MODE;->PDIF_MODE_RAW:Lcom/konka/kkinterface/tv/DataBaseDesk$EN_SPDIF_MODE;

    iput-object v0, p0, Lcom/konka/kkinterface/tv/DataBaseDesk$MS_USER_SOUND_SETTING;->spdifMode:Lcom/konka/kkinterface/tv/DataBaseDesk$EN_SPDIF_MODE;

    sget-object v0, Lcom/konka/kkinterface/tv/DataBaseDesk$EN_SURROUND_MODE;->E_SURROUND_MODE_OFF:Lcom/konka/kkinterface/tv/DataBaseDesk$EN_SURROUND_MODE;

    iput-object v0, p0, Lcom/konka/kkinterface/tv/DataBaseDesk$MS_USER_SOUND_SETTING;->SurroundMode:Lcom/konka/kkinterface/tv/DataBaseDesk$EN_SURROUND_MODE;

    sget-object v0, Lcom/konka/kkinterface/tv/DataBaseDesk$EN_SOUND_MODE;->SOUND_MODE_STANDARD:Lcom/konka/kkinterface/tv/DataBaseDesk$EN_SOUND_MODE;

    iput-object v0, p0, Lcom/konka/kkinterface/tv/DataBaseDesk$MS_USER_SOUND_SETTING;->SoundMode:Lcom/konka/kkinterface/tv/DataBaseDesk$EN_SOUND_MODE;

    const/16 v0, 0x32

    iput-short v0, p0, Lcom/konka/kkinterface/tv/DataBaseDesk$MS_USER_SOUND_SETTING;->Balance:S

    return-void
.end method
