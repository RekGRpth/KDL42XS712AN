.class public interface abstract Lcom/konka/kkinterface/tv/FactoryDesk;
.super Ljava/lang/Object;
.source "FactoryDesk.java"

# interfaces
.implements Lcom/konka/kkinterface/tv/BaseDesk;


# static fields
.field public static final AUTOTUNE_END_FAILED:I = 0x4e91

.field public static final AUTOTUNE_END_SUCESSED:I = 0x4e90

.field public static final AUTOTUNE_START:I = 0x4e8f


# virtual methods
.method public abstract ExecAutoADC()Z
.end method

.method public abstract changeWBParaWhenSourceChange()Z
.end method

.method public abstract get3DSelfAdaptiveLevel()I
.end method

.method public abstract getADCBlueGain()I
.end method

.method public abstract getADCBlueOffset()I
.end method

.method public abstract getADCGreenGain()I
.end method

.method public abstract getADCGreenOffset()I
.end method

.method public abstract getADCRedGain()I
.end method

.method public abstract getADCRedOffset()I
.end method

.method public abstract getAEFC_43()S
.end method

.method public abstract getAEFC_44()S
.end method

.method public abstract getAEFC_66Bit76()S
.end method

.method public abstract getAEFC_6EBit3210()S
.end method

.method public abstract getAEFC_6EBit7654()S
.end method

.method public abstract getAEFC_A0()S
.end method

.method public abstract getAEFC_A1()S
.end method

.method public abstract getAEFC_CB()S
.end method

.method public abstract getAEFC_D4()S
.end method

.method public abstract getAEFC_D5Bit2()S
.end method

.method public abstract getAEFC_D7LowBoun()S
.end method

.method public abstract getAEFC_D8Bit3210()S
.end method

.method public abstract getAEFC_D9Bit0()S
.end method

.method public abstract getAdcIdx()Lcom/konka/kkinterface/tv/DataBaseDesk$E_ADC_SET_INDEX;
.end method

.method public abstract getAudioDspVersion()S
.end method

.method public abstract getAudioHiDevMode()I
.end method

.method public abstract getAudioNrThr()S
.end method

.method public abstract getAudioPrescale()S
.end method

.method public abstract getAudioSifThreshold()S
.end method

.method public abstract getBoardType()Ljava/lang/String;
.end method

.method public abstract getChinaDescramblerBox()S
.end method

.method public abstract getCompileTime()Ljava/lang/String;
.end method

.method public abstract getCurveType()Lcom/konka/kkinterface/tv/DataBaseDesk$MS_NLA_SET_INDEX;
.end method

.method public abstract getDelayReduce()S
.end method

.method public abstract getDtvAvAbnormalDelay()Z
.end method

.method public abstract getFactoryPreSetFeature()I
.end method

.method public abstract getGainDistributionThr()I
.end method

.method public abstract getLVDSenalbe()Z
.end method

.method public abstract getLVDSmodulation()I
.end method

.method public abstract getLVDSpercentage()I
.end method

.method public abstract getMIUenalbe()Z
.end method

.method public abstract getMIUmodulation()I
.end method

.method public abstract getMIUpercentage()I
.end method

.method public abstract getOsdV0Nonlinear()S
.end method

.method public abstract getOsdV100Nonlinear()S
.end method

.method public abstract getOsdV25Nonlinear()S
.end method

.method public abstract getOsdV50Nonlinear()S
.end method

.method public abstract getOsdV75Nonlinear()S
.end method

.method public abstract getOverScanHposition()S
.end method

.method public abstract getOverScanHsize()S
.end method

.method public abstract getOverScanSourceType()Lcom/mstar/android/tvapi/common/vo/TvOsType$EnumInputSource;
.end method

.method public abstract getOverScanVposition()S
.end method

.method public abstract getOverScanVsize()S
.end method

.method public abstract getPanelSwing()S
.end method

.method public abstract getPanelType()Ljava/lang/String;
.end method

.method public abstract getPeqFoCoarse(I)I
.end method

.method public abstract getPeqFoFine(I)I
.end method

.method public abstract getPeqGain(I)I
.end method

.method public abstract getPeqQ(I)I
.end method

.method public abstract getPowerOnMode()I
.end method

.method public abstract getSoftWareVersion()Ljava/lang/String;
.end method

.method public abstract getTestPattern()I
.end method

.method public abstract getUartOnOff()Z
.end method

.method public abstract getVdDspVersion()S
.end method

.method public abstract getVifAgcRef()S
.end method

.method public abstract getVifAsiaSignalOption()Z
.end method

.method public abstract getVifClampGainOvNegative()I
.end method

.method public abstract getVifCrKi()S
.end method

.method public abstract getVifCrKp()S
.end method

.method public abstract getVifCrKpKiAdjust()Z
.end method

.method public abstract getVifCrThr()I
.end method

.method public abstract getVifOverModulation()Z
.end method

.method public abstract getVifTop()S
.end method

.method public abstract getVifVersion()S
.end method

.method public abstract getVifVgaMaximum()I
.end method

.method public abstract getWatchDogMode()S
.end method

.method public abstract getWbBlueGain()S
.end method

.method public abstract getWbBlueOffset()S
.end method

.method public abstract getWbGreenGain()S
.end method

.method public abstract getWbGreenOffset()S
.end method

.method public abstract getWbRedGain()S
.end method

.method public abstract getWbRedOffset()S
.end method

.method public abstract restoreToDefault()Z
.end method

.method public abstract set3DSelfAdaptiveLevel(I)Z
.end method

.method public abstract setADCBlueGain(I)Z
.end method

.method public abstract setADCBlueOffset(I)Z
.end method

.method public abstract setADCGreenGain(I)Z
.end method

.method public abstract setADCGreenOffset(I)Z
.end method

.method public abstract setADCRedGain(I)Z
.end method

.method public abstract setADCRedOffset(I)Z
.end method

.method public abstract setAEFC_43(S)Z
.end method

.method public abstract setAEFC_44(S)Z
.end method

.method public abstract setAEFC_66Bit76(S)Z
.end method

.method public abstract setAEFC_6EBit3210(S)Z
.end method

.method public abstract setAEFC_6EBit7654(S)Z
.end method

.method public abstract setAEFC_A0(S)Z
.end method

.method public abstract setAEFC_A1(S)Z
.end method

.method public abstract setAEFC_CB(S)Z
.end method

.method public abstract setAEFC_D4(S)Z
.end method

.method public abstract setAEFC_D5Bit2(S)Z
.end method

.method public abstract setAEFC_D7LowBoun(S)Z
.end method

.method public abstract setAEFC_D8Bit3210(S)Z
.end method

.method public abstract setAEFC_D9Bit0(S)Z
.end method

.method public abstract setAdcIdx(Lcom/konka/kkinterface/tv/DataBaseDesk$E_ADC_SET_INDEX;)Z
.end method

.method public abstract setAudioDspVersion(S)Z
.end method

.method public abstract setAudioHiDevMode(I)Z
.end method

.method public abstract setAudioNrThr(S)Z
.end method

.method public abstract setAudioPrescale(S)Z
.end method

.method public abstract setAudioSifThreshold(S)Z
.end method

.method public abstract setChinaDescramblerBox(S)Z
.end method

.method public abstract setCurveType(Lcom/konka/kkinterface/tv/DataBaseDesk$MS_NLA_SET_INDEX;)Z
.end method

.method public abstract setDelayReduce(S)Z
.end method

.method public abstract setDtvAvAbnormalDelay(Z)Z
.end method

.method public abstract setFactoryPreSetFeature(I)Z
.end method

.method public abstract setGainDistributionThr(I)Z
.end method

.method public abstract setLVDSenable(Z)Z
.end method

.method public abstract setLVDSmodulation(I)Z
.end method

.method public abstract setLVDSpercentage(I)Z
.end method

.method public abstract setMIUenable(Z)Z
.end method

.method public abstract setMIUmodulation(I)Z
.end method

.method public abstract setMIUpercentage(I)Z
.end method

.method public abstract setOsdV0Nonlinear(S)Z
.end method

.method public abstract setOsdV100Nonlinear(S)Z
.end method

.method public abstract setOsdV25Nonlinear(S)Z
.end method

.method public abstract setOsdV50Nonlinear(S)Z
.end method

.method public abstract setOsdV75Nonlinear(S)Z
.end method

.method public abstract setOverScanHposition(S)Z
.end method

.method public abstract setOverScanHsize(S)Z
.end method

.method public abstract setOverScanSourceType(Lcom/mstar/android/tvapi/common/vo/TvOsType$EnumInputSource;)Z
.end method

.method public abstract setOverScanVposition(S)Z
.end method

.method public abstract setOverScanVsize(S)Z
.end method

.method public abstract setPEQ()Z
.end method

.method public abstract setPanelSwing(S)Z
.end method

.method public abstract setPeqFoCoarse(II)Z
.end method

.method public abstract setPeqFoFine(II)Z
.end method

.method public abstract setPeqGain(II)Z
.end method

.method public abstract setPeqQ(II)Z
.end method

.method public abstract setPowerOnMode(I)Z
.end method

.method public abstract setTestPattern(I)Z
.end method

.method public abstract setUartOnOff(Z)Z
.end method

.method public abstract setVdDspVersion(S)Z
.end method

.method public abstract setVifAgcRef(S)Z
.end method

.method public abstract setVifAsiaSignalOption(Z)Z
.end method

.method public abstract setVifClampGainOvNegative(I)Z
.end method

.method public abstract setVifCrKi(S)Z
.end method

.method public abstract setVifCrKp(S)Z
.end method

.method public abstract setVifCrKpKiAdjust(Z)Z
.end method

.method public abstract setVifCrThr(I)Z
.end method

.method public abstract setVifOverModulation(Z)Z
.end method

.method public abstract setVifTop(S)Z
.end method

.method public abstract setVifVersion(S)Z
.end method

.method public abstract setVifVgaMaximum(I)Z
.end method

.method public abstract setWatchDogMode(S)Z
.end method

.method public abstract setWbBlueGain(S)Z
.end method

.method public abstract setWbBlueOffset(S)Z
.end method

.method public abstract setWbGreenGain(S)Z
.end method

.method public abstract setWbGreenOffset(S)Z
.end method

.method public abstract setWbRedGain(S)Z
.end method

.method public abstract setWbRedOffset(S)Z
.end method
