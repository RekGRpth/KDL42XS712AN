.class public interface abstract Lcom/konka/kkinterface/tv/S3DDesk;
.super Ljava/lang/Object;
.source "S3DDesk.java"

# interfaces
.implements Lcom/konka/kkinterface/tv/BaseDesk;


# virtual methods
.method public abstract disableLow3dQuality()V
.end method

.method public abstract enableLow3dQuality()V
.end method

.method public abstract get3DDepthMode()I
.end method

.method public abstract get3DOffsetMode()I
.end method

.method public abstract get3DOutputAspectMode()Lcom/konka/kkinterface/tv/DataBaseDesk$EN_ThreeD_Video_3DOUTPUTASPECT;
.end method

.method public abstract getAutoStartMode()Lcom/konka/kkinterface/tv/DataBaseDesk$EN_ThreeD_Video_AUTOSTART;
.end method

.method public abstract getDisplay3DTo2DMode()Lcom/konka/kkinterface/tv/DataBaseDesk$EN_ThreeD_Video_3DTO2D;
.end method

.method public abstract getDisplayFormat()Lcom/konka/kkinterface/tv/DataBaseDesk$EN_ThreeD_Video_DISPLAYFORMAT;
.end method

.method public abstract getLRViewSwitch()Lcom/konka/kkinterface/tv/DataBaseDesk$EN_ThreeD_Video_LRVIEWSWITCH;
.end method

.method public abstract getSelfAdaptiveDetect()Lcom/konka/kkinterface/tv/DataBaseDesk$EN_ThreeD_Video_SELFADAPTIVE_DETECT;
.end method

.method public abstract set3DDepthMode(I)Z
.end method

.method public abstract set3DOffsetMode(I)Z
.end method

.method public abstract set3DOutputAspectMode(Lcom/konka/kkinterface/tv/DataBaseDesk$EN_ThreeD_Video_3DOUTPUTASPECT;)Z
.end method

.method public abstract set3DTo2D(Lcom/konka/kkinterface/tv/DataBaseDesk$EN_ThreeD_Video_3DTO2D;)Z
.end method

.method public abstract setAutoStartMode(Lcom/konka/kkinterface/tv/DataBaseDesk$EN_ThreeD_Video_AUTOSTART;)Z
.end method

.method public abstract setDisplayFormat(Lcom/konka/kkinterface/tv/DataBaseDesk$EN_ThreeD_Video_DISPLAYFORMAT;)Z
.end method

.method public abstract setLRViewSwitch(Lcom/konka/kkinterface/tv/DataBaseDesk$EN_ThreeD_Video_LRVIEWSWITCH;)Z
.end method

.method public abstract setSelfAdaptiveDetect(Lcom/konka/kkinterface/tv/DataBaseDesk$EN_ThreeD_Video_SELFADAPTIVE_DETECT;)Z
.end method

.method public abstract setVideoScaleforKTV()Z
.end method
