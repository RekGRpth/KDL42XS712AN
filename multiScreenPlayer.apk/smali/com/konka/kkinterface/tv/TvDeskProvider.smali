.class public interface abstract Lcom/konka/kkinterface/tv/TvDeskProvider;
.super Ljava/lang/Object;
.source "TvDeskProvider.java"


# virtual methods
.method public abstract getCaManagerInstance()Lcom/konka/kkinterface/tv/CaDesk;
.end method

.method public abstract getChannelManagerInstance()Lcom/konka/kkinterface/tv/ChannelDesk;
.end method

.method public abstract getCiManagerInstance()Lcom/konka/kkinterface/tv/CiDesk;
.end method

.method public abstract getCommonManagerInstance()Lcom/konka/kkinterface/tv/CommonDesk;
.end method

.method public abstract getDataBaseManagerInstance()Lcom/konka/kkinterface/tv/DataBaseDesk;
.end method

.method public abstract getDemoManagerInstance()Lcom/konka/kkinterface/tv/DemoDesk;
.end method

.method public abstract getEpgManagerInstance()Lcom/konka/kkinterface/tv/EpgDesk;
.end method

.method public abstract getPictureManagerInstance()Lcom/konka/kkinterface/tv/PictureDesk;
.end method

.method public abstract getPvrManagerInstance()Lcom/konka/kkinterface/tv/PvrDesk;
.end method

.method public abstract getS3DManagerInstance()Lcom/konka/kkinterface/tv/S3DDesk;
.end method

.method public abstract getSettingManagerInstance()Lcom/konka/kkinterface/tv/SettingDesk;
.end method

.method public abstract getSoundManagerInstance()Lcom/konka/kkinterface/tv/SoundDesk;
.end method

.method public abstract initTvSrvProvider()V
.end method
