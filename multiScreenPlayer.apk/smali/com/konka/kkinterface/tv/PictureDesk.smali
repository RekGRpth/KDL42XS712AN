.class public interface abstract Lcom/konka/kkinterface/tv/PictureDesk;
.super Ljava/lang/Object;
.source "PictureDesk.java"

# interfaces
.implements Lcom/konka/kkinterface/tv/BaseDesk;


# static fields
.field public static final AUTOPC_END_FAILED:I = 0x3

.field public static final AUTOPC_END_SUCESSED:I = 0x2

.field public static final AUTOPC_START:I = 0x1


# virtual methods
.method public abstract ExecAutoPc()Z
.end method

.method public abstract ExecVideoItem(Lcom/konka/kkinterface/tv/DataBaseDesk$EN_MS_VIDEOITEM;S)Z
.end method

.method public abstract GetBacklight()S
.end method

.method public abstract GetBacklightOfPicMode(Lcom/konka/kkinterface/tv/DataBaseDesk$EN_MS_PICTURE;)S
.end method

.method public abstract GetColorTempIdx()Lcom/konka/kkinterface/tv/DataBaseDesk$EN_MS_COLOR_TEMP;
.end method

.method public abstract GetColorTempPara()Lcom/konka/kkinterface/tv/DataBaseDesk$T_MS_COLOR_TEMPEX_DATA;
.end method

.method public abstract GetMpegNR()Lcom/konka/kkinterface/tv/DataBaseDesk$EN_MS_MPEG_NR;
.end method

.method public abstract GetNR()Lcom/konka/kkinterface/tv/DataBaseDesk$EN_MS_NR;
.end method

.method public abstract GetPCClock()S
.end method

.method public abstract GetPCHPos()S
.end method

.method public abstract GetPCModeIndex(I)S
.end method

.method public abstract GetPCPhase()S
.end method

.method public abstract GetPCVPos()S
.end method

.method public abstract GetPictureModeIdx()Lcom/konka/kkinterface/tv/DataBaseDesk$EN_MS_PICTURE;
.end method

.method public abstract GetVideoArc()Lcom/konka/kkinterface/tv/DataBaseDesk$MAPI_VIDEO_ARC_Type;
.end method

.method public abstract GetVideoItem(Lcom/konka/kkinterface/tv/DataBaseDesk$EN_MS_VIDEOITEM;)S
.end method

.method public abstract SetBacklight(S)Z
.end method

.method public abstract SetColorTempIdx(Lcom/konka/kkinterface/tv/DataBaseDesk$EN_MS_COLOR_TEMP;)Z
.end method

.method public abstract SetColorTempPara(Lcom/konka/kkinterface/tv/DataBaseDesk$T_MS_COLOR_TEMPEX_DATA;)Z
.end method

.method public abstract SetKTVVideoArc()Z
.end method

.method public abstract SetMpegNR(Lcom/konka/kkinterface/tv/DataBaseDesk$EN_MS_MPEG_NR;)Z
.end method

.method public abstract SetNR(Lcom/konka/kkinterface/tv/DataBaseDesk$EN_MS_NR;)Z
.end method

.method public abstract SetPCClock(S)Z
.end method

.method public abstract SetPCHPos(S)Z
.end method

.method public abstract SetPCPhase(S)Z
.end method

.method public abstract SetPCVPos(S)Z
.end method

.method public abstract SetPictureModeIdx(Lcom/konka/kkinterface/tv/DataBaseDesk$EN_MS_PICTURE;)Z
.end method

.method public abstract SetVideoArc(Lcom/konka/kkinterface/tv/DataBaseDesk$MAPI_VIDEO_ARC_Type;)Z
.end method

.method public abstract disableBacklight()V
.end method

.method public abstract enableBacklight()V
.end method

.method public abstract enableSetBacklight(Z)Z
.end method

.method public abstract getCustomerPqRuleNumber()I
.end method

.method public abstract getDNR()Lcom/konka/kkinterface/tv/DataBaseDesk$EN_MS_NR;
.end method

.method public abstract getDetailEnhance()Z
.end method

.method public abstract getDlcAverageLuma()S
.end method

.method public abstract getDlcLumArray(I)[I
.end method

.method public abstract getDynamicBLModeIdx()I
.end method

.method public abstract getDynamicContrast()Lcom/konka/kkinterface/tv/DataBaseDesk$EN_MS_Dynamic_Contrast;
.end method

.method public abstract getDynamicContrastCurve()[I
.end method

.method public abstract getEnergyEnable()Z
.end method

.method public abstract getEnergyPercent()S
.end method

.method public abstract getSkinToneMode()Lcom/konka/kkinterface/tv/DataBaseDesk$SkinToneMode;
.end method

.method public abstract getStatusNumberByCustomerPqRule(I)I
.end method

.method public abstract refreshVideoPara()Z
.end method

.method public abstract setDNR(Lcom/konka/kkinterface/tv/DataBaseDesk$EN_MS_NR;)Z
.end method

.method public abstract setDetailEnhance(Z)Z
.end method

.method public abstract setDisplayWindow(Lcom/mstar/android/tvapi/common/vo/VideoWindowType;)V
.end method

.method public abstract setDynamicBLModeIdx(Lcom/konka/kkinterface/tv/DataBaseDesk$EN_MS_LOCALDIMMING;)Z
.end method

.method public abstract setDynamicContrast(Lcom/konka/kkinterface/tv/DataBaseDesk$EN_MS_Dynamic_Contrast;)Z
.end method

.method public abstract setEnergyEnable(Z)Z
.end method

.method public abstract setEnergyPercent(S)Z
.end method

.method public abstract setSkinToneMode(Lcom/konka/kkinterface/tv/DataBaseDesk$SkinToneMode;)Z
.end method

.method public abstract setStatusByCustomerPqRule(II)Z
.end method
