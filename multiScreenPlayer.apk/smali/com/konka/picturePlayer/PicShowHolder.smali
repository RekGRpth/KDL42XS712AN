.class public Lcom/konka/picturePlayer/PicShowHolder;
.super Ljava/lang/Object;
.source "PicShowHolder.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/konka/picturePlayer/PicShowHolder$ItemOnClickEvent;
    }
.end annotation


# static fields
.field public static final IMG_BIG_BTN_STATE:I = 0x7f020005

.field public static final IMG_FULL_BTN_STATE:I = 0x7f02004e

.field public static final IMG_GALLERY_SHOW_BTN_STATE:I = 0x7f020006

.field public static final IMG_LEFT_BTN_STATE:I = 0x7f02005b

.field public static final IMG_NEXT_BTN_STATE:I = 0x7f02008c

.field public static final IMG_PLAY_BTN_STATE:I = 0x7f0200a8

.field public static final IMG_PRE_BTN_STATE:I = 0x7f020004

.field public static final IMG_RIGHT_BTN_STATE:I = 0x7f0200b5

.field public static final IMG_SMALL_BTN_STATE:I = 0x7f0200c8

.field public static final IMG_STOP_BTN_STATE:I = 0x7f0200ce

.field public static final OPTION_STATE_BACK:I = 0x2

.field public static final OPTION_STATE_BIG:I = 0x8

.field public static final OPTION_STATE_FULLSCREEN:I = 0x10

.field public static final OPTION_STATE_GALLERY:I = 0x1

.field public static final OPTION_STATE_NEXT:I = 0x4

.field public static final OPTION_STATE_PLAY:I = 0x3

.field public static final OPTION_STATE_SMALL:I = 0x9

.field public static final OPTION_STATE_STOP:I = 0x5

.field public static final OPTION_STATE_TURNLEFT:I = 0x6

.field public static final OPTION_STATE_TURNRIGHT:I = 0x7

.field public static bt_big:Landroid/widget/Button;

.field public static bt_leftRotion:Landroid/widget/Button;

.field public static bt_rightRotion:Landroid/widget/Button;

.field public static bt_small:Landroid/widget/Button;


# instance fields
.field public bt_autoPlay:Landroid/widget/Button;

.field public bt_fullScreen:Landroid/widget/Button;

.field public bt_galleryShow:Landroid/widget/Button;

.field public bt_next:Landroid/widget/Button;

.field public bt_pre:Landroid/widget/Button;

.field public bt_stop:Landroid/widget/Button;

.field private mPicShowActivity:Lcom/konka/picturePlayer/picturePlayerActivity;

.field private onFocusChange:Landroid/view/View$OnFocusChangeListener;

.field protected pic_bts_layout:Landroid/widget/RelativeLayout;

.field public text_autoplay:Landroid/widget/TextView;

.field public text_descale:Landroid/widget/TextView;

.field public text_enlarge:Landroid/widget/TextView;

.field public text_fullscreen:Landroid/widget/TextView;

.field public text_leferotion:Landroid/widget/TextView;

.field public text_list:Landroid/widget/TextView;

.field public text_next:Landroid/widget/TextView;

.field public text_pre:Landroid/widget/TextView;

.field public text_rightrotion:Landroid/widget/TextView;

.field public text_stop:Landroid/widget/TextView;

.field protected toast_first_image:Landroid/widget/Toast;

.field protected toast_last_image:Landroid/widget/Toast;


# direct methods
.method public constructor <init>(Lcom/konka/picturePlayer/picturePlayerActivity;)V
    .locals 3
    .param p1    # Lcom/konka/picturePlayer/picturePlayerActivity;

    const/4 v0, 0x0

    const/4 v2, 0x0

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    iput-object v0, p0, Lcom/konka/picturePlayer/PicShowHolder;->toast_first_image:Landroid/widget/Toast;

    iput-object v0, p0, Lcom/konka/picturePlayer/PicShowHolder;->toast_last_image:Landroid/widget/Toast;

    new-instance v0, Lcom/konka/picturePlayer/PicShowHolder$1;

    invoke-direct {v0, p0}, Lcom/konka/picturePlayer/PicShowHolder$1;-><init>(Lcom/konka/picturePlayer/PicShowHolder;)V

    iput-object v0, p0, Lcom/konka/picturePlayer/PicShowHolder;->onFocusChange:Landroid/view/View$OnFocusChangeListener;

    iput-object p1, p0, Lcom/konka/picturePlayer/PicShowHolder;->mPicShowActivity:Lcom/konka/picturePlayer/picturePlayerActivity;

    invoke-virtual {p1}, Lcom/konka/picturePlayer/picturePlayerActivity;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    const v1, 0x7f090084    # com.konka.mediaSharePlayer.R.string.first_photo

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v0

    invoke-static {p1, v0, v2}, Landroid/widget/Toast;->makeText(Landroid/content/Context;Ljava/lang/CharSequence;I)Landroid/widget/Toast;

    move-result-object v0

    iput-object v0, p0, Lcom/konka/picturePlayer/PicShowHolder;->toast_first_image:Landroid/widget/Toast;

    invoke-virtual {p1}, Lcom/konka/picturePlayer/picturePlayerActivity;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    const v1, 0x7f090085    # com.konka.mediaSharePlayer.R.string.last_photo

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v0

    invoke-static {p1, v0, v2}, Landroid/widget/Toast;->makeText(Landroid/content/Context;Ljava/lang/CharSequence;I)Landroid/widget/Toast;

    move-result-object v0

    iput-object v0, p0, Lcom/konka/picturePlayer/PicShowHolder;->toast_last_image:Landroid/widget/Toast;

    return-void
.end method

.method static synthetic access$0(Lcom/konka/picturePlayer/PicShowHolder;Landroid/widget/TextView;Z)V
    .locals 0

    invoke-direct {p0, p1, p2}, Lcom/konka/picturePlayer/PicShowHolder;->setTextViewVisible(Landroid/widget/TextView;Z)V

    return-void
.end method

.method static synthetic access$1(Lcom/konka/picturePlayer/PicShowHolder;)Lcom/konka/picturePlayer/picturePlayerActivity;
    .locals 1

    iget-object v0, p0, Lcom/konka/picturePlayer/PicShowHolder;->mPicShowActivity:Lcom/konka/picturePlayer/picturePlayerActivity;

    return-object v0
.end method

.method static synthetic access$2(Lcom/konka/picturePlayer/PicShowHolder;I)V
    .locals 0

    invoke-direct {p0, p1}, Lcom/konka/picturePlayer/PicShowHolder;->setTextViewVisible(I)V

    return-void
.end method

.method private setTextViewVisible(I)V
    .locals 3
    .param p1    # I

    const/4 v2, 0x0

    const/4 v1, 0x4

    packed-switch p1, :pswitch_data_0

    :goto_0
    :pswitch_0
    return-void

    :pswitch_1
    iget-object v0, p0, Lcom/konka/picturePlayer/PicShowHolder;->text_leferotion:Landroid/widget/TextView;

    invoke-virtual {v0, v2}, Landroid/widget/TextView;->setVisibility(I)V

    iget-object v0, p0, Lcom/konka/picturePlayer/PicShowHolder;->text_rightrotion:Landroid/widget/TextView;

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setVisibility(I)V

    iget-object v0, p0, Lcom/konka/picturePlayer/PicShowHolder;->text_enlarge:Landroid/widget/TextView;

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setVisibility(I)V

    iget-object v0, p0, Lcom/konka/picturePlayer/PicShowHolder;->text_descale:Landroid/widget/TextView;

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setVisibility(I)V

    iget-object v0, p0, Lcom/konka/picturePlayer/PicShowHolder;->text_fullscreen:Landroid/widget/TextView;

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setVisibility(I)V

    goto :goto_0

    :pswitch_2
    iget-object v0, p0, Lcom/konka/picturePlayer/PicShowHolder;->text_leferotion:Landroid/widget/TextView;

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setVisibility(I)V

    iget-object v0, p0, Lcom/konka/picturePlayer/PicShowHolder;->text_rightrotion:Landroid/widget/TextView;

    invoke-virtual {v0, v2}, Landroid/widget/TextView;->setVisibility(I)V

    iget-object v0, p0, Lcom/konka/picturePlayer/PicShowHolder;->text_enlarge:Landroid/widget/TextView;

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setVisibility(I)V

    iget-object v0, p0, Lcom/konka/picturePlayer/PicShowHolder;->text_descale:Landroid/widget/TextView;

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setVisibility(I)V

    iget-object v0, p0, Lcom/konka/picturePlayer/PicShowHolder;->text_fullscreen:Landroid/widget/TextView;

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setVisibility(I)V

    goto :goto_0

    :pswitch_3
    iget-object v0, p0, Lcom/konka/picturePlayer/PicShowHolder;->text_leferotion:Landroid/widget/TextView;

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setVisibility(I)V

    iget-object v0, p0, Lcom/konka/picturePlayer/PicShowHolder;->text_rightrotion:Landroid/widget/TextView;

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setVisibility(I)V

    iget-object v0, p0, Lcom/konka/picturePlayer/PicShowHolder;->text_enlarge:Landroid/widget/TextView;

    invoke-virtual {v0, v2}, Landroid/widget/TextView;->setVisibility(I)V

    iget-object v0, p0, Lcom/konka/picturePlayer/PicShowHolder;->text_descale:Landroid/widget/TextView;

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setVisibility(I)V

    iget-object v0, p0, Lcom/konka/picturePlayer/PicShowHolder;->text_fullscreen:Landroid/widget/TextView;

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setVisibility(I)V

    goto :goto_0

    :pswitch_4
    iget-object v0, p0, Lcom/konka/picturePlayer/PicShowHolder;->text_leferotion:Landroid/widget/TextView;

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setVisibility(I)V

    iget-object v0, p0, Lcom/konka/picturePlayer/PicShowHolder;->text_rightrotion:Landroid/widget/TextView;

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setVisibility(I)V

    iget-object v0, p0, Lcom/konka/picturePlayer/PicShowHolder;->text_enlarge:Landroid/widget/TextView;

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setVisibility(I)V

    iget-object v0, p0, Lcom/konka/picturePlayer/PicShowHolder;->text_descale:Landroid/widget/TextView;

    invoke-virtual {v0, v2}, Landroid/widget/TextView;->setVisibility(I)V

    iget-object v0, p0, Lcom/konka/picturePlayer/PicShowHolder;->text_fullscreen:Landroid/widget/TextView;

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setVisibility(I)V

    goto :goto_0

    :pswitch_data_0
    .packed-switch 0x7f0b0015
        :pswitch_1    # com.konka.mediaSharePlayer.R.id.textview_item6
        :pswitch_0    # com.konka.mediaSharePlayer.R.id.buttomitem7
        :pswitch_2    # com.konka.mediaSharePlayer.R.id.textview_item7
        :pswitch_0    # com.konka.mediaSharePlayer.R.id.buttomitem8
        :pswitch_3    # com.konka.mediaSharePlayer.R.id.textview_item8
        :pswitch_0    # com.konka.mediaSharePlayer.R.id.buttomitem9
        :pswitch_4    # com.konka.mediaSharePlayer.R.id.textview_item9
    .end packed-switch
.end method

.method private setTextViewVisible(Landroid/widget/TextView;Z)V
    .locals 1
    .param p1    # Landroid/widget/TextView;
    .param p2    # Z

    if-eqz p2, :cond_0

    const/4 v0, 0x0

    invoke-virtual {p1, v0}, Landroid/widget/TextView;->setVisibility(I)V

    :goto_0
    return-void

    :cond_0
    const/4 v0, 0x4

    invoke-virtual {p1, v0}, Landroid/widget/TextView;->setVisibility(I)V

    goto :goto_0
.end method


# virtual methods
.method public chageUnFouceBgImg()V
    .locals 0

    return-void
.end method

.method public findBtnView()V
    .locals 2

    iget-object v0, p0, Lcom/konka/picturePlayer/PicShowHolder;->mPicShowActivity:Lcom/konka/picturePlayer/picturePlayerActivity;

    const v1, 0x7f0b0014    # com.konka.mediaSharePlayer.R.id.buttomitem6

    invoke-virtual {v0, v1}, Lcom/konka/picturePlayer/picturePlayerActivity;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/Button;

    sput-object v0, Lcom/konka/picturePlayer/PicShowHolder;->bt_leftRotion:Landroid/widget/Button;

    iget-object v0, p0, Lcom/konka/picturePlayer/PicShowHolder;->mPicShowActivity:Lcom/konka/picturePlayer/picturePlayerActivity;

    const v1, 0x7f0b0016    # com.konka.mediaSharePlayer.R.id.buttomitem7

    invoke-virtual {v0, v1}, Lcom/konka/picturePlayer/picturePlayerActivity;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/Button;

    sput-object v0, Lcom/konka/picturePlayer/PicShowHolder;->bt_rightRotion:Landroid/widget/Button;

    iget-object v0, p0, Lcom/konka/picturePlayer/PicShowHolder;->mPicShowActivity:Lcom/konka/picturePlayer/picturePlayerActivity;

    const v1, 0x7f0b0018    # com.konka.mediaSharePlayer.R.id.buttomitem8

    invoke-virtual {v0, v1}, Lcom/konka/picturePlayer/picturePlayerActivity;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/Button;

    sput-object v0, Lcom/konka/picturePlayer/PicShowHolder;->bt_small:Landroid/widget/Button;

    iget-object v0, p0, Lcom/konka/picturePlayer/PicShowHolder;->mPicShowActivity:Lcom/konka/picturePlayer/picturePlayerActivity;

    const v1, 0x7f0b001a    # com.konka.mediaSharePlayer.R.id.buttomitem9

    invoke-virtual {v0, v1}, Lcom/konka/picturePlayer/picturePlayerActivity;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/Button;

    sput-object v0, Lcom/konka/picturePlayer/PicShowHolder;->bt_big:Landroid/widget/Button;

    iget-object v0, p0, Lcom/konka/picturePlayer/PicShowHolder;->mPicShowActivity:Lcom/konka/picturePlayer/picturePlayerActivity;

    const v1, 0x7f0b0015    # com.konka.mediaSharePlayer.R.id.textview_item6

    invoke-virtual {v0, v1}, Lcom/konka/picturePlayer/picturePlayerActivity;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    iput-object v0, p0, Lcom/konka/picturePlayer/PicShowHolder;->text_leferotion:Landroid/widget/TextView;

    iget-object v0, p0, Lcom/konka/picturePlayer/PicShowHolder;->mPicShowActivity:Lcom/konka/picturePlayer/picturePlayerActivity;

    const v1, 0x7f0b0017    # com.konka.mediaSharePlayer.R.id.textview_item7

    invoke-virtual {v0, v1}, Lcom/konka/picturePlayer/picturePlayerActivity;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    iput-object v0, p0, Lcom/konka/picturePlayer/PicShowHolder;->text_rightrotion:Landroid/widget/TextView;

    iget-object v0, p0, Lcom/konka/picturePlayer/PicShowHolder;->mPicShowActivity:Lcom/konka/picturePlayer/picturePlayerActivity;

    const v1, 0x7f0b0019    # com.konka.mediaSharePlayer.R.id.textview_item8

    invoke-virtual {v0, v1}, Lcom/konka/picturePlayer/picturePlayerActivity;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    iput-object v0, p0, Lcom/konka/picturePlayer/PicShowHolder;->text_descale:Landroid/widget/TextView;

    iget-object v0, p0, Lcom/konka/picturePlayer/PicShowHolder;->mPicShowActivity:Lcom/konka/picturePlayer/picturePlayerActivity;

    const v1, 0x7f0b001b    # com.konka.mediaSharePlayer.R.id.textview_item9

    invoke-virtual {v0, v1}, Lcom/konka/picturePlayer/picturePlayerActivity;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    iput-object v0, p0, Lcom/konka/picturePlayer/PicShowHolder;->text_enlarge:Landroid/widget/TextView;

    iget-object v0, p0, Lcom/konka/picturePlayer/PicShowHolder;->mPicShowActivity:Lcom/konka/picturePlayer/picturePlayerActivity;

    const v1, 0x7f0b0012    # com.konka.mediaSharePlayer.R.id.buttoms_set

    invoke-virtual {v0, v1}, Lcom/konka/picturePlayer/picturePlayerActivity;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/RelativeLayout;

    iput-object v0, p0, Lcom/konka/picturePlayer/PicShowHolder;->pic_bts_layout:Landroid/widget/RelativeLayout;

    sget-object v0, Lcom/konka/picturePlayer/PicShowHolder;->bt_leftRotion:Landroid/widget/Button;

    new-instance v1, Lcom/konka/picturePlayer/PicShowHolder$ItemOnClickEvent;

    invoke-direct {v1, p0}, Lcom/konka/picturePlayer/PicShowHolder$ItemOnClickEvent;-><init>(Lcom/konka/picturePlayer/PicShowHolder;)V

    invoke-virtual {v0, v1}, Landroid/widget/Button;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    sget-object v0, Lcom/konka/picturePlayer/PicShowHolder;->bt_rightRotion:Landroid/widget/Button;

    new-instance v1, Lcom/konka/picturePlayer/PicShowHolder$ItemOnClickEvent;

    invoke-direct {v1, p0}, Lcom/konka/picturePlayer/PicShowHolder$ItemOnClickEvent;-><init>(Lcom/konka/picturePlayer/PicShowHolder;)V

    invoke-virtual {v0, v1}, Landroid/widget/Button;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    sget-object v0, Lcom/konka/picturePlayer/PicShowHolder;->bt_big:Landroid/widget/Button;

    new-instance v1, Lcom/konka/picturePlayer/PicShowHolder$ItemOnClickEvent;

    invoke-direct {v1, p0}, Lcom/konka/picturePlayer/PicShowHolder$ItemOnClickEvent;-><init>(Lcom/konka/picturePlayer/PicShowHolder;)V

    invoke-virtual {v0, v1}, Landroid/widget/Button;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    sget-object v0, Lcom/konka/picturePlayer/PicShowHolder;->bt_small:Landroid/widget/Button;

    new-instance v1, Lcom/konka/picturePlayer/PicShowHolder$ItemOnClickEvent;

    invoke-direct {v1, p0}, Lcom/konka/picturePlayer/PicShowHolder$ItemOnClickEvent;-><init>(Lcom/konka/picturePlayer/PicShowHolder;)V

    invoke-virtual {v0, v1}, Landroid/widget/Button;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    sget-object v0, Lcom/konka/picturePlayer/PicShowHolder;->bt_leftRotion:Landroid/widget/Button;

    iget-object v1, p0, Lcom/konka/picturePlayer/PicShowHolder;->onFocusChange:Landroid/view/View$OnFocusChangeListener;

    invoke-virtual {v0, v1}, Landroid/widget/Button;->setOnFocusChangeListener(Landroid/view/View$OnFocusChangeListener;)V

    sget-object v0, Lcom/konka/picturePlayer/PicShowHolder;->bt_rightRotion:Landroid/widget/Button;

    iget-object v1, p0, Lcom/konka/picturePlayer/PicShowHolder;->onFocusChange:Landroid/view/View$OnFocusChangeListener;

    invoke-virtual {v0, v1}, Landroid/widget/Button;->setOnFocusChangeListener(Landroid/view/View$OnFocusChangeListener;)V

    sget-object v0, Lcom/konka/picturePlayer/PicShowHolder;->bt_big:Landroid/widget/Button;

    iget-object v1, p0, Lcom/konka/picturePlayer/PicShowHolder;->onFocusChange:Landroid/view/View$OnFocusChangeListener;

    invoke-virtual {v0, v1}, Landroid/widget/Button;->setOnFocusChangeListener(Landroid/view/View$OnFocusChangeListener;)V

    sget-object v0, Lcom/konka/picturePlayer/PicShowHolder;->bt_small:Landroid/widget/Button;

    iget-object v1, p0, Lcom/konka/picturePlayer/PicShowHolder;->onFocusChange:Landroid/view/View$OnFocusChangeListener;

    invoke-virtual {v0, v1}, Landroid/widget/Button;->setOnFocusChangeListener(Landroid/view/View$OnFocusChangeListener;)V

    return-void
.end method
