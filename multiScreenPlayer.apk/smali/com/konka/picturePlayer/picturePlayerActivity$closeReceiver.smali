.class Lcom/konka/picturePlayer/picturePlayerActivity$closeReceiver;
.super Landroid/content/BroadcastReceiver;
.source "picturePlayerActivity.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/konka/picturePlayer/picturePlayerActivity;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x2
    name = "closeReceiver"
.end annotation


# instance fields
.field final synthetic this$0:Lcom/konka/picturePlayer/picturePlayerActivity;


# direct methods
.method private constructor <init>(Lcom/konka/picturePlayer/picturePlayerActivity;)V
    .locals 0

    iput-object p1, p0, Lcom/konka/picturePlayer/picturePlayerActivity$closeReceiver;->this$0:Lcom/konka/picturePlayer/picturePlayerActivity;

    invoke-direct {p0}, Landroid/content/BroadcastReceiver;-><init>()V

    return-void
.end method

.method synthetic constructor <init>(Lcom/konka/picturePlayer/picturePlayerActivity;Lcom/konka/picturePlayer/picturePlayerActivity$closeReceiver;)V
    .locals 0

    invoke-direct {p0, p1}, Lcom/konka/picturePlayer/picturePlayerActivity$closeReceiver;-><init>(Lcom/konka/picturePlayer/picturePlayerActivity;)V

    return-void
.end method


# virtual methods
.method public onReceive(Landroid/content/Context;Landroid/content/Intent;)V
    .locals 8
    .param p1    # Landroid/content/Context;
    .param p2    # Landroid/content/Intent;

    const/4 v2, 0x1

    const/4 v3, 0x0

    invoke-virtual {p2}, Landroid/content/Intent;->getExtras()Landroid/os/Bundle;

    move-result-object v6

    const-string v0, "closePlayer"

    invoke-virtual {v6, v0, v3}, Landroid/os/Bundle;->getInt(Ljava/lang/String;I)I

    move-result v7

    iget-object v0, p0, Lcom/konka/picturePlayer/picturePlayerActivity$closeReceiver;->this$0:Lcom/konka/picturePlayer/picturePlayerActivity;

    const-string v1, "mediastop"

    invoke-virtual {v6, v1, v2}, Landroid/os/Bundle;->getInt(Ljava/lang/String;I)I

    move-result v1

    iput v1, v0, Lcom/konka/picturePlayer/picturePlayerActivity;->mediastop:I

    if-ne v7, v2, :cond_0

    const-string v0, "media stop"

    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "closePlayer: --------------pic"

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v1, v7}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    iget-object v0, p0, Lcom/konka/picturePlayer/picturePlayerActivity$closeReceiver;->this$0:Lcom/konka/picturePlayer/picturePlayerActivity;

    iget-object v1, p0, Lcom/konka/picturePlayer/picturePlayerActivity$closeReceiver;->this$0:Lcom/konka/picturePlayer/picturePlayerActivity;

    sget-object v2, Lcom/konka/mediaSharePlayer/PlayerState;->PLAY_STATUS_EXIT:Lcom/konka/mediaSharePlayer/PlayerState;

    invoke-virtual {v2}, Lcom/konka/mediaSharePlayer/PlayerState;->ordinal()I

    move-result v2

    new-instance v5, Ljava/lang/StringBuilder;

    iget-object v4, p0, Lcom/konka/picturePlayer/picturePlayerActivity$closeReceiver;->this$0:Lcom/konka/picturePlayer/picturePlayerActivity;

    iget-object v4, v4, Lcom/konka/picturePlayer/picturePlayerActivity;->picPaths:Ljava/util/List;

    invoke-interface {v4, v3}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v4

    check-cast v4, Ljava/lang/String;

    invoke-static {v4}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v4

    invoke-direct {v5, v4}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    const-string v4, "&mp"

    invoke-virtual {v5, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    move v4, v3

    invoke-virtual/range {v0 .. v5}, Lcom/konka/picturePlayer/picturePlayerActivity;->sendPlayerStateToPlayerService(Landroid/content/Context;IIILjava/lang/String;)V

    iget-object v0, p0, Lcom/konka/picturePlayer/picturePlayerActivity$closeReceiver;->this$0:Lcom/konka/picturePlayer/picturePlayerActivity;

    iput-boolean v3, v0, Lcom/konka/picturePlayer/picturePlayerActivity;->isrun:Z

    iget-object v0, p0, Lcom/konka/picturePlayer/picturePlayerActivity$closeReceiver;->this$0:Lcom/konka/picturePlayer/picturePlayerActivity;

    invoke-static {v0, v3, v3, v3}, Lcom/konka/mediaSharePlayer/sendPlayerState;->MediaPlayerActionSendBroadcastIsCreat(Landroid/content/Context;III)V

    invoke-static {v3}, Ljava/lang/System;->exit(I)V

    :cond_0
    iget-object v0, p0, Lcom/konka/picturePlayer/picturePlayerActivity$closeReceiver;->this$0:Lcom/konka/picturePlayer/picturePlayerActivity;

    iget v0, v0, Lcom/konka/picturePlayer/picturePlayerActivity;->mediastop:I

    if-nez v0, :cond_1

    const-string v0, "media stop"

    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "mediastop: --------------pic"

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    iget-object v2, p0, Lcom/konka/picturePlayer/picturePlayerActivity$closeReceiver;->this$0:Lcom/konka/picturePlayer/picturePlayerActivity;

    iget v2, v2, Lcom/konka/picturePlayer/picturePlayerActivity;->mediastop:I

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    iget-object v0, p0, Lcom/konka/picturePlayer/picturePlayerActivity$closeReceiver;->this$0:Lcom/konka/picturePlayer/picturePlayerActivity;

    iget-object v1, p0, Lcom/konka/picturePlayer/picturePlayerActivity$closeReceiver;->this$0:Lcom/konka/picturePlayer/picturePlayerActivity;

    sget-object v2, Lcom/konka/mediaSharePlayer/PlayerState;->PLAY_STATUS_STOP:Lcom/konka/mediaSharePlayer/PlayerState;

    invoke-virtual {v2}, Lcom/konka/mediaSharePlayer/PlayerState;->ordinal()I

    move-result v2

    new-instance v5, Ljava/lang/StringBuilder;

    iget-object v4, p0, Lcom/konka/picturePlayer/picturePlayerActivity$closeReceiver;->this$0:Lcom/konka/picturePlayer/picturePlayerActivity;

    iget-object v4, v4, Lcom/konka/picturePlayer/picturePlayerActivity;->picPaths:Ljava/util/List;

    invoke-interface {v4, v3}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v4

    check-cast v4, Ljava/lang/String;

    invoke-static {v4}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v4

    invoke-direct {v5, v4}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    const-string v4, "&mp"

    invoke-virtual {v5, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    move v4, v3

    invoke-virtual/range {v0 .. v5}, Lcom/konka/picturePlayer/picturePlayerActivity;->sendPlayerStateToPlayerService(Landroid/content/Context;IIILjava/lang/String;)V

    iget-object v0, p0, Lcom/konka/picturePlayer/picturePlayerActivity$closeReceiver;->this$0:Lcom/konka/picturePlayer/picturePlayerActivity;

    iput-boolean v3, v0, Lcom/konka/picturePlayer/picturePlayerActivity;->isrun:Z

    iget-object v0, p0, Lcom/konka/picturePlayer/picturePlayerActivity$closeReceiver;->this$0:Lcom/konka/picturePlayer/picturePlayerActivity;

    invoke-static {v0, v3, v3, v3}, Lcom/konka/mediaSharePlayer/sendPlayerState;->MediaPlayerActionSendBroadcastIsCreat(Landroid/content/Context;III)V

    invoke-static {v3}, Ljava/lang/System;->exit(I)V

    :cond_1
    return-void
.end method
