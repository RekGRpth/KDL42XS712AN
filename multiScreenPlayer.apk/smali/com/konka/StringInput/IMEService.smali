.class public Lcom/konka/StringInput/IMEService;
.super Landroid/inputmethodservice/InputMethodService;
.source "IMEService.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/konka/StringInput/IMEService$StringReceiver;
    }
.end annotation


# instance fields
.field curInputMethodId:Ljava/lang/String;

.field private mComposing:Ljava/lang/StringBuilder;

.field private mEnvironment:Lcom/konka/StringInput/Environment;

.field private mInputMethodManager:Landroid/view/inputmethod/InputMethodManager;

.field resultText:Ljava/lang/String;

.field stringreceiver:Lcom/konka/StringInput/IMEService$StringReceiver;


# direct methods
.method public constructor <init>()V
    .locals 2

    const/4 v1, 0x0

    invoke-direct {p0}, Landroid/inputmethodservice/InputMethodService;-><init>()V

    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    iput-object v0, p0, Lcom/konka/StringInput/IMEService;->mComposing:Ljava/lang/StringBuilder;

    iput-object v1, p0, Lcom/konka/StringInput/IMEService;->resultText:Ljava/lang/String;

    iput-object v1, p0, Lcom/konka/StringInput/IMEService;->curInputMethodId:Ljava/lang/String;

    return-void
.end method

.method static synthetic access$0(Lcom/konka/StringInput/IMEService;)Ljava/lang/StringBuilder;
    .locals 1

    iget-object v0, p0, Lcom/konka/StringInput/IMEService;->mComposing:Ljava/lang/StringBuilder;

    return-object v0
.end method

.method static synthetic access$1(Lcom/konka/StringInput/IMEService;Landroid/view/inputmethod/InputConnection;)V
    .locals 0

    invoke-direct {p0, p1}, Lcom/konka/StringInput/IMEService;->commitTyped(Landroid/view/inputmethod/InputConnection;)V

    return-void
.end method

.method private commitTyped(Landroid/view/inputmethod/InputConnection;)V
    .locals 2
    .param p1    # Landroid/view/inputmethod/InputConnection;

    iget-object v0, p0, Lcom/konka/StringInput/IMEService;->mComposing:Ljava/lang/StringBuilder;

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->length()I

    move-result v0

    if-lez v0, :cond_0

    iget-object v0, p0, Lcom/konka/StringInput/IMEService;->mComposing:Ljava/lang/StringBuilder;

    iget-object v1, p0, Lcom/konka/StringInput/IMEService;->mComposing:Ljava/lang/StringBuilder;

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->length()I

    move-result v1

    invoke-interface {p1, v0, v1}, Landroid/view/inputmethod/InputConnection;->commitText(Ljava/lang/CharSequence;I)Z

    iget-object v0, p0, Lcom/konka/StringInput/IMEService;->mComposing:Ljava/lang/StringBuilder;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->setLength(I)V

    :cond_0
    const-string v0, "com.konka.StringInput"

    const-string v1, "result = \ufffd\ufffd\ufffd{\ufffd\ufffd\ufffd\ufffd\ufffd\ufffd\ubde8___________commitTyped"

    invoke-static {v0, v1}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    return-void
.end method


# virtual methods
.method public onCreate()V
    .locals 3

    invoke-static {}, Lcom/konka/StringInput/Environment;->getInstance()Lcom/konka/StringInput/Environment;

    move-result-object v0

    iput-object v0, p0, Lcom/konka/StringInput/IMEService;->mEnvironment:Lcom/konka/StringInput/Environment;

    iget-object v0, p0, Lcom/konka/StringInput/IMEService;->mEnvironment:Lcom/konka/StringInput/Environment;

    invoke-virtual {v0}, Lcom/konka/StringInput/Environment;->needDebug()Z

    move-result v0

    if-eqz v0, :cond_0

    const-string v0, "IMEService"

    const-string v1, "onCreate."

    invoke-static {v0, v1}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    :cond_0
    const-string v0, "input_method"

    invoke-virtual {p0, v0}, Lcom/konka/StringInput/IMEService;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/view/inputmethod/InputMethodManager;

    iput-object v0, p0, Lcom/konka/StringInput/IMEService;->mInputMethodManager:Landroid/view/inputmethod/InputMethodManager;

    invoke-virtual {p0}, Lcom/konka/StringInput/IMEService;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v0

    const-string v1, "default_input_method"

    invoke-static {v0, v1}, Landroid/provider/Settings$Secure;->getString(Landroid/content/ContentResolver;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/konka/StringInput/IMEService;->curInputMethodId:Ljava/lang/String;

    const-string v0, "Demo Error"

    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "CURRENT IME3: "

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    iget-object v2, p0, Lcom/konka/StringInput/IMEService;->curInputMethodId:Ljava/lang/String;

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    invoke-virtual {p0}, Lcom/konka/StringInput/IMEService;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v0

    const-string v1, "default_input_method"

    const-string v2, "com.konka.mediaSharePlayer/com.konka.StringInput.IMEService"

    invoke-static {v0, v1, v2}, Landroid/provider/Settings$Secure;->putString(Landroid/content/ContentResolver;Ljava/lang/String;Ljava/lang/String;)Z

    const v0, 0x7f090007    # com.konka.mediaSharePlayer.R.string.ime_start

    invoke-virtual {p0, v0}, Lcom/konka/StringInput/IMEService;->getText(I)Ljava/lang/CharSequence;

    move-result-object v0

    const/4 v1, 0x0

    invoke-static {p0, v0, v1}, Landroid/widget/Toast;->makeText(Landroid/content/Context;Ljava/lang/CharSequence;I)Landroid/widget/Toast;

    move-result-object v0

    invoke-virtual {v0}, Landroid/widget/Toast;->show()V

    invoke-virtual {p0}, Lcom/konka/StringInput/IMEService;->regiseterStringReceiver()V

    invoke-super {p0}, Landroid/inputmethodservice/InputMethodService;->onCreate()V

    return-void
.end method

.method public onCreateCandidatesView()Landroid/view/View;
    .locals 2

    const-string v0, "com.konka.StringInput"

    const-string v1, "result = \ufffd\ufffd\ufffd{\ufffd\ufffd\ufffd\ufffd\ufffd\ufffd\ubde8___________5"

    invoke-static {v0, v1}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    const/4 v0, 0x0

    return-object v0
.end method

.method public onCreateInputView()Landroid/view/View;
    .locals 2

    const-string v0, "com.konka.StringInput"

    const-string v1, "result = \ufffd\ufffd\ufffd{\ufffd\ufffd\ufffd\ufffd\ufffd\ufffd\ubde8___________4"

    invoke-static {v0, v1}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    const/4 v0, 0x0

    return-object v0
.end method

.method public onCurrentInputMethodSubtypeChanged(Landroid/view/inputmethod/InputMethodSubtype;)V
    .locals 2
    .param p1    # Landroid/view/inputmethod/InputMethodSubtype;

    const-string v0, "com.konka.StringInput"

    const-string v1, "result = \ufffd\ufffd\ufffd{\ufffd\ufffd\ufffd\ufffd\ufffd\ufffd\ubde8___________onCurrentInputMethodSubtypeChanged"

    invoke-static {v0, v1}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    return-void
.end method

.method public onDestroy()V
    .locals 2

    const-string v0, "com.konka.StringInput"

    const-string v1, "onDestroy()---------------------"

    invoke-static {v0, v1}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    iget-object v0, p0, Lcom/konka/StringInput/IMEService;->stringreceiver:Lcom/konka/StringInput/IMEService$StringReceiver;

    invoke-virtual {p0, v0}, Lcom/konka/StringInput/IMEService;->unregisterReceiver(Landroid/content/BroadcastReceiver;)V

    invoke-virtual {p0}, Lcom/konka/StringInput/IMEService;->stopSelf()V

    invoke-super {p0}, Landroid/inputmethodservice/InputMethodService;->onDestroy()V

    return-void
.end method

.method public onEvaluateInputViewShown()Z
    .locals 1

    const/4 v0, 0x0

    return v0
.end method

.method public onFinishInput()V
    .locals 2

    invoke-super {p0}, Landroid/inputmethodservice/InputMethodService;->onFinishInput()V

    const-string v0, "com.konka.StringInput"

    const-string v1, "result = \ufffd\ufffd\ufffd{\ufffd\ufffd\ufffd\ufffd\ufffd\ufffd\ubde8___________onFinishInput"

    invoke-static {v0, v1}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    iget-object v0, p0, Lcom/konka/StringInput/IMEService;->mComposing:Ljava/lang/StringBuilder;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->setLength(I)V

    return-void
.end method

.method public onInitializeInterface()V
    .locals 2

    const-string v0, "com.konka.StringInput"

    const-string v1, "result = \ufffd\ufffd\ufffd{\ufffd\ufffd\ufffd\ufffd\ufffd\ufffd\ubde8___________3"

    invoke-static {v0, v1}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    return-void
.end method

.method public onStartInput(Landroid/view/inputmethod/EditorInfo;Z)V
    .locals 2
    .param p1    # Landroid/view/inputmethod/EditorInfo;
    .param p2    # Z

    invoke-super {p0, p1, p2}, Landroid/inputmethodservice/InputMethodService;->onStartInput(Landroid/view/inputmethod/EditorInfo;Z)V

    const-string v0, "com.konka.StringInput"

    const-string v1, "result = \ufffd\ufffd\ufffd{\ufffd\ufffd\ufffd\ufffd\ufffd\ufffd\ubde8___________onStartInput"

    invoke-static {v0, v1}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    iget-object v0, p0, Lcom/konka/StringInput/IMEService;->mComposing:Ljava/lang/StringBuilder;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->setLength(I)V

    return-void
.end method

.method public onStartInputView(Landroid/view/inputmethod/EditorInfo;Z)V
    .locals 4
    .param p1    # Landroid/view/inputmethod/EditorInfo;
    .param p2    # Z

    invoke-super {p0, p1, p2}, Landroid/inputmethodservice/InputMethodService;->onStartInputView(Landroid/view/inputmethod/EditorInfo;Z)V

    iget-object v1, p0, Lcom/konka/StringInput/IMEService;->mInputMethodManager:Landroid/view/inputmethod/InputMethodManager;

    invoke-virtual {v1}, Landroid/view/inputmethod/InputMethodManager;->getCurrentInputMethodSubtype()Landroid/view/inputmethod/InputMethodSubtype;

    move-result-object v0

    const-string v1, "com.konka.StringInput"

    new-instance v2, Ljava/lang/StringBuilder;

    const-string v3, "result = \ufffd\ufffd\ufffd{\ufffd\ufffd\ufffd\ufffd\ufffd\ufffd\ubde8___________onStartInputView"

    invoke-direct {v2, v3}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    return-void
.end method

.method public onText(Ljava/lang/CharSequence;)V
    .locals 2
    .param p1    # Ljava/lang/CharSequence;

    invoke-virtual {p0}, Lcom/konka/StringInput/IMEService;->getCurrentInputConnection()Landroid/view/inputmethod/InputConnection;

    move-result-object v0

    if-nez v0, :cond_0

    :goto_0
    return-void

    :cond_0
    invoke-interface {v0}, Landroid/view/inputmethod/InputConnection;->beginBatchEdit()Z

    iget-object v1, p0, Lcom/konka/StringInput/IMEService;->mComposing:Ljava/lang/StringBuilder;

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->length()I

    move-result v1

    if-lez v1, :cond_1

    invoke-direct {p0, v0}, Lcom/konka/StringInput/IMEService;->commitTyped(Landroid/view/inputmethod/InputConnection;)V

    :cond_1
    const/4 v1, 0x0

    invoke-interface {v0, p1, v1}, Landroid/view/inputmethod/InputConnection;->commitText(Ljava/lang/CharSequence;I)Z

    invoke-interface {v0}, Landroid/view/inputmethod/InputConnection;->endBatchEdit()Z

    goto :goto_0
.end method

.method public regiseterStringReceiver()V
    .locals 3

    const-string v1, "com.konka.StringInput"

    const-string v2, "result = \ufffd\ufffd\ufffd{\ufffd\ufffd\ufffd\ufffd\ufffd\ufffd\ubde8___________regiseterStringReceiver"

    invoke-static {v1, v2}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    new-instance v1, Lcom/konka/StringInput/IMEService$StringReceiver;

    const/4 v2, 0x0

    invoke-direct {v1, p0, v2}, Lcom/konka/StringInput/IMEService$StringReceiver;-><init>(Lcom/konka/StringInput/IMEService;Lcom/konka/StringInput/IMEService$StringReceiver;)V

    iput-object v1, p0, Lcom/konka/StringInput/IMEService;->stringreceiver:Lcom/konka/StringInput/IMEService$StringReceiver;

    new-instance v0, Landroid/content/IntentFilter;

    invoke-direct {v0}, Landroid/content/IntentFilter;-><init>()V

    const-string v1, "com.konka.multiScreen.InputMethod"

    invoke-virtual {v0, v1}, Landroid/content/IntentFilter;->addAction(Ljava/lang/String;)V

    iget-object v1, p0, Lcom/konka/StringInput/IMEService;->stringreceiver:Lcom/konka/StringInput/IMEService$StringReceiver;

    invoke-virtual {p0, v1, v0}, Lcom/konka/StringInput/IMEService;->registerReceiver(Landroid/content/BroadcastReceiver;Landroid/content/IntentFilter;)Landroid/content/Intent;

    return-void
.end method
