.class public Lcom/konka/kkimplements/tv/mstar/DemoDeskImpl;
.super Ljava/lang/Object;
.source "DemoDeskImpl.java"

# interfaces
.implements Lcom/konka/kkinterface/tv/DemoDesk;


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/konka/kkimplements/tv/mstar/DemoDeskImpl$MyThread;
    }
.end annotation


# static fields
.field private static final PBLV1:I = 0x14

.field private static final PBLV2:I = 0x28

.field private static final PBLV3:I = 0x3c

.field private static final PBLV4:I = 0x50

.field private static final PBLV5:I = 0x64

.field private static final cmvalue1:I = 0x11

.field private static final cmvalue2:I = 0x30

.field private static final cmvalue3:I = 0x58

.field private static final cmvalue4:I = 0x70

.field private static final cmvalue5:I = 0x85

.field private static final cmvalue6:I = 0xa0

.field private static final cmvalue7:I = 0xf0

.field private static final cmvalue8:I = 0xff

.field private static demoMgrImpl:Lcom/konka/kkimplements/tv/mstar/DemoDeskImpl;

.field private static mythread:Ljava/lang/Thread;

.field private static sleeptimeCnt:I


# instance fields
.field private bforceThreadSleep:Z

.field private context:Landroid/content/Context;

.field private e_DBC_TYPE:Lcom/konka/kkinterface/tv/DemoDesk$EN_MS_DBC_TYPE;

.field private e_DCC_TYPE:Lcom/konka/kkinterface/tv/DemoDesk$EN_MS_DCC_TYPE;

.field private e_DLC_TYPE:Lcom/konka/kkinterface/tv/DemoDesk$EN_MS_DLC_TYPE;

.field private e_MWE_TYPE:Lcom/mstar/android/tvapi/common/vo/MweType$EnumMweType;

.field private e_NR_TYPE:Lcom/mstar/android/tvapi/common/vo/NoiseReduction$EnumNoiseReduction;

.field private e_UCLEAR_TYPE:Lcom/konka/kkinterface/tv/DemoDesk$EN_MS_UCLEAR_TYPE;

.field pm:Lcom/mstar/android/tvapi/common/PictureManager;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    const/4 v0, 0x0

    sput-object v0, Lcom/konka/kkimplements/tv/mstar/DemoDeskImpl;->mythread:Ljava/lang/Thread;

    const/16 v0, 0x1f4

    sput v0, Lcom/konka/kkimplements/tv/mstar/DemoDeskImpl;->sleeptimeCnt:I

    return-void
.end method

.method private constructor <init>(Landroid/content/Context;)V
    .locals 1
    .param p1    # Landroid/content/Context;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    sget-object v0, Lcom/mstar/android/tvapi/common/vo/MweType$EnumMweType;->E_OFF:Lcom/mstar/android/tvapi/common/vo/MweType$EnumMweType;

    iput-object v0, p0, Lcom/konka/kkimplements/tv/mstar/DemoDeskImpl;->e_MWE_TYPE:Lcom/mstar/android/tvapi/common/vo/MweType$EnumMweType;

    sget-object v0, Lcom/konka/kkinterface/tv/DemoDesk$EN_MS_DBC_TYPE;->E_MS_DBC_OFF:Lcom/konka/kkinterface/tv/DemoDesk$EN_MS_DBC_TYPE;

    iput-object v0, p0, Lcom/konka/kkimplements/tv/mstar/DemoDeskImpl;->e_DBC_TYPE:Lcom/konka/kkinterface/tv/DemoDesk$EN_MS_DBC_TYPE;

    sget-object v0, Lcom/konka/kkinterface/tv/DemoDesk$EN_MS_DCC_TYPE;->E_MS_DCC_OFF:Lcom/konka/kkinterface/tv/DemoDesk$EN_MS_DCC_TYPE;

    iput-object v0, p0, Lcom/konka/kkimplements/tv/mstar/DemoDeskImpl;->e_DCC_TYPE:Lcom/konka/kkinterface/tv/DemoDesk$EN_MS_DCC_TYPE;

    sget-object v0, Lcom/konka/kkinterface/tv/DemoDesk$EN_MS_DLC_TYPE;->E_MS_DLC_ON:Lcom/konka/kkinterface/tv/DemoDesk$EN_MS_DLC_TYPE;

    iput-object v0, p0, Lcom/konka/kkimplements/tv/mstar/DemoDeskImpl;->e_DLC_TYPE:Lcom/konka/kkinterface/tv/DemoDesk$EN_MS_DLC_TYPE;

    sget-object v0, Lcom/konka/kkinterface/tv/DemoDesk$EN_MS_UCLEAR_TYPE;->E_MS_UCLEAR_ON:Lcom/konka/kkinterface/tv/DemoDesk$EN_MS_UCLEAR_TYPE;

    iput-object v0, p0, Lcom/konka/kkimplements/tv/mstar/DemoDeskImpl;->e_UCLEAR_TYPE:Lcom/konka/kkinterface/tv/DemoDesk$EN_MS_UCLEAR_TYPE;

    sget-object v0, Lcom/mstar/android/tvapi/common/vo/NoiseReduction$EnumNoiseReduction;->E_NR_OFF:Lcom/mstar/android/tvapi/common/vo/NoiseReduction$EnumNoiseReduction;

    iput-object v0, p0, Lcom/konka/kkimplements/tv/mstar/DemoDeskImpl;->e_NR_TYPE:Lcom/mstar/android/tvapi/common/vo/NoiseReduction$EnumNoiseReduction;

    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/konka/kkimplements/tv/mstar/DemoDeskImpl;->bforceThreadSleep:Z

    const/4 v0, 0x0

    iput-object v0, p0, Lcom/konka/kkimplements/tv/mstar/DemoDeskImpl;->pm:Lcom/mstar/android/tvapi/common/PictureManager;

    iput-object p1, p0, Lcom/konka/kkimplements/tv/mstar/DemoDeskImpl;->context:Landroid/content/Context;

    invoke-static {}, Lcom/mstar/android/tvapi/common/TvManager;->getInstance()Lcom/mstar/android/tvapi/common/TvManager;

    move-result-object v0

    if-eqz v0, :cond_0

    invoke-static {}, Lcom/mstar/android/tvapi/common/TvManager;->getInstance()Lcom/mstar/android/tvapi/common/TvManager;

    move-result-object v0

    invoke-virtual {v0}, Lcom/mstar/android/tvapi/common/TvManager;->getPictureManager()Lcom/mstar/android/tvapi/common/PictureManager;

    move-result-object v0

    if-eqz v0, :cond_0

    invoke-static {}, Lcom/mstar/android/tvapi/common/TvManager;->getInstance()Lcom/mstar/android/tvapi/common/TvManager;

    move-result-object v0

    invoke-virtual {v0}, Lcom/mstar/android/tvapi/common/TvManager;->getPictureManager()Lcom/mstar/android/tvapi/common/PictureManager;

    move-result-object v0

    iput-object v0, p0, Lcom/konka/kkimplements/tv/mstar/DemoDeskImpl;->pm:Lcom/mstar/android/tvapi/common/PictureManager;

    :cond_0
    sget-object v0, Lcom/mstar/android/tvapi/common/vo/MweType$EnumMweType;->E_OFF:Lcom/mstar/android/tvapi/common/vo/MweType$EnumMweType;

    iput-object v0, p0, Lcom/konka/kkimplements/tv/mstar/DemoDeskImpl;->e_MWE_TYPE:Lcom/mstar/android/tvapi/common/vo/MweType$EnumMweType;

    sget-object v0, Lcom/konka/kkinterface/tv/DemoDesk$EN_MS_DBC_TYPE;->E_MS_DBC_OFF:Lcom/konka/kkinterface/tv/DemoDesk$EN_MS_DBC_TYPE;

    iput-object v0, p0, Lcom/konka/kkimplements/tv/mstar/DemoDeskImpl;->e_DBC_TYPE:Lcom/konka/kkinterface/tv/DemoDesk$EN_MS_DBC_TYPE;

    sget-object v0, Lcom/konka/kkinterface/tv/DemoDesk$EN_MS_DCC_TYPE;->E_MS_DCC_OFF:Lcom/konka/kkinterface/tv/DemoDesk$EN_MS_DCC_TYPE;

    iput-object v0, p0, Lcom/konka/kkimplements/tv/mstar/DemoDeskImpl;->e_DCC_TYPE:Lcom/konka/kkinterface/tv/DemoDesk$EN_MS_DCC_TYPE;

    sget-object v0, Lcom/konka/kkinterface/tv/DemoDesk$EN_MS_DLC_TYPE;->E_MS_DLC_ON:Lcom/konka/kkinterface/tv/DemoDesk$EN_MS_DLC_TYPE;

    iput-object v0, p0, Lcom/konka/kkimplements/tv/mstar/DemoDeskImpl;->e_DLC_TYPE:Lcom/konka/kkinterface/tv/DemoDesk$EN_MS_DLC_TYPE;

    sget-object v0, Lcom/konka/kkinterface/tv/DemoDesk$EN_MS_UCLEAR_TYPE;->E_MS_UCLEAR_ON:Lcom/konka/kkinterface/tv/DemoDesk$EN_MS_UCLEAR_TYPE;

    iput-object v0, p0, Lcom/konka/kkimplements/tv/mstar/DemoDeskImpl;->e_UCLEAR_TYPE:Lcom/konka/kkinterface/tv/DemoDesk$EN_MS_UCLEAR_TYPE;

    sget-object v0, Lcom/mstar/android/tvapi/common/vo/NoiseReduction$EnumNoiseReduction;->E_NR_OFF:Lcom/mstar/android/tvapi/common/vo/NoiseReduction$EnumNoiseReduction;

    iput-object v0, p0, Lcom/konka/kkimplements/tv/mstar/DemoDeskImpl;->e_NR_TYPE:Lcom/mstar/android/tvapi/common/vo/NoiseReduction$EnumNoiseReduction;

    sget-object v0, Lcom/konka/kkimplements/tv/mstar/DemoDeskImpl;->mythread:Ljava/lang/Thread;

    if-nez v0, :cond_1

    new-instance v0, Lcom/konka/kkimplements/tv/mstar/DemoDeskImpl$MyThread;

    invoke-direct {v0, p0}, Lcom/konka/kkimplements/tv/mstar/DemoDeskImpl$MyThread;-><init>(Lcom/konka/kkimplements/tv/mstar/DemoDeskImpl;)V

    sput-object v0, Lcom/konka/kkimplements/tv/mstar/DemoDeskImpl;->mythread:Ljava/lang/Thread;

    sget-object v0, Lcom/konka/kkimplements/tv/mstar/DemoDeskImpl;->mythread:Ljava/lang/Thread;

    invoke-virtual {v0}, Ljava/lang/Thread;->start()V

    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/konka/kkimplements/tv/mstar/DemoDeskImpl;->bforceThreadSleep:Z

    :cond_1
    return-void
.end method

.method private ReCntSt()V
    .locals 2

    iget-object v0, p0, Lcom/konka/kkimplements/tv/mstar/DemoDeskImpl;->e_DBC_TYPE:Lcom/konka/kkinterface/tv/DemoDesk$EN_MS_DBC_TYPE;

    sget-object v1, Lcom/konka/kkinterface/tv/DemoDesk$EN_MS_DBC_TYPE;->E_MS_DBC_OFF:Lcom/konka/kkinterface/tv/DemoDesk$EN_MS_DBC_TYPE;

    if-ne v0, v1, :cond_0

    iget-object v0, p0, Lcom/konka/kkimplements/tv/mstar/DemoDeskImpl;->e_DCC_TYPE:Lcom/konka/kkinterface/tv/DemoDesk$EN_MS_DCC_TYPE;

    sget-object v1, Lcom/konka/kkinterface/tv/DemoDesk$EN_MS_DCC_TYPE;->E_MS_DCC_OFF:Lcom/konka/kkinterface/tv/DemoDesk$EN_MS_DCC_TYPE;

    if-ne v0, v1, :cond_0

    const/16 v0, 0x7d0

    sput v0, Lcom/konka/kkimplements/tv/mstar/DemoDeskImpl;->sleeptimeCnt:I

    :goto_0
    return-void

    :cond_0
    iget-object v0, p0, Lcom/konka/kkimplements/tv/mstar/DemoDeskImpl;->e_DBC_TYPE:Lcom/konka/kkinterface/tv/DemoDesk$EN_MS_DBC_TYPE;

    sget-object v1, Lcom/konka/kkinterface/tv/DemoDesk$EN_MS_DBC_TYPE;->E_MS_DBC_ON:Lcom/konka/kkinterface/tv/DemoDesk$EN_MS_DBC_TYPE;

    if-ne v0, v1, :cond_1

    iget-object v0, p0, Lcom/konka/kkimplements/tv/mstar/DemoDeskImpl;->e_DCC_TYPE:Lcom/konka/kkinterface/tv/DemoDesk$EN_MS_DCC_TYPE;

    sget-object v1, Lcom/konka/kkinterface/tv/DemoDesk$EN_MS_DCC_TYPE;->E_MS_DCC_ON:Lcom/konka/kkinterface/tv/DemoDesk$EN_MS_DCC_TYPE;

    if-ne v0, v1, :cond_1

    const/16 v0, 0x1f4

    sput v0, Lcom/konka/kkimplements/tv/mstar/DemoDeskImpl;->sleeptimeCnt:I

    goto :goto_0

    :cond_1
    const/16 v0, 0x3e8

    sput v0, Lcom/konka/kkimplements/tv/mstar/DemoDeskImpl;->sleeptimeCnt:I

    goto :goto_0
.end method

.method static synthetic access$0(Lcom/konka/kkimplements/tv/mstar/DemoDeskImpl;)Z
    .locals 1

    iget-boolean v0, p0, Lcom/konka/kkimplements/tv/mstar/DemoDeskImpl;->bforceThreadSleep:Z

    return v0
.end method

.method static synthetic access$1(Lcom/konka/kkimplements/tv/mstar/DemoDeskImpl;)Lcom/konka/kkinterface/tv/DemoDesk$EN_MS_DBC_TYPE;
    .locals 1

    iget-object v0, p0, Lcom/konka/kkimplements/tv/mstar/DemoDeskImpl;->e_DBC_TYPE:Lcom/konka/kkinterface/tv/DemoDesk$EN_MS_DBC_TYPE;

    return-object v0
.end method

.method static synthetic access$2(Lcom/konka/kkimplements/tv/mstar/DemoDeskImpl;)V
    .locals 0

    invoke-direct {p0}, Lcom/konka/kkimplements/tv/mstar/DemoDeskImpl;->dbchandler()V

    return-void
.end method

.method static synthetic access$3(Lcom/konka/kkimplements/tv/mstar/DemoDeskImpl;)Lcom/konka/kkinterface/tv/DemoDesk$EN_MS_DCC_TYPE;
    .locals 1

    iget-object v0, p0, Lcom/konka/kkimplements/tv/mstar/DemoDeskImpl;->e_DCC_TYPE:Lcom/konka/kkinterface/tv/DemoDesk$EN_MS_DCC_TYPE;

    return-object v0
.end method

.method static synthetic access$4(Lcom/konka/kkimplements/tv/mstar/DemoDeskImpl;)V
    .locals 0

    invoke-direct {p0}, Lcom/konka/kkimplements/tv/mstar/DemoDeskImpl;->dcchandler()V

    return-void
.end method

.method static synthetic access$5()I
    .locals 1

    sget v0, Lcom/konka/kkimplements/tv/mstar/DemoDeskImpl;->sleeptimeCnt:I

    return v0
.end method

.method private dbchandler()V
    .locals 5

    invoke-direct {p0}, Lcom/konka/kkimplements/tv/mstar/DemoDeskImpl;->mapi_GetImageBackLight()I

    move-result v1

    :try_start_0
    invoke-static {}, Lcom/mstar/android/tvapi/common/TvManager;->getInstance()Lcom/mstar/android/tvapi/common/TvManager;

    move-result-object v2

    if-eqz v2, :cond_0

    invoke-static {}, Lcom/mstar/android/tvapi/common/TvManager;->getInstance()Lcom/mstar/android/tvapi/common/TvManager;

    move-result-object v2

    invoke-virtual {v2}, Lcom/mstar/android/tvapi/common/TvManager;->getPictureManager()Lcom/mstar/android/tvapi/common/PictureManager;

    move-result-object v2

    if-eqz v2, :cond_0

    invoke-static {}, Lcom/mstar/android/tvapi/common/TvManager;->getInstance()Lcom/mstar/android/tvapi/common/TvManager;

    move-result-object v2

    invoke-virtual {v2}, Lcom/mstar/android/tvapi/common/TvManager;->getPictureManager()Lcom/mstar/android/tvapi/common/PictureManager;

    move-result-object v2

    invoke-virtual {v2, v1}, Lcom/mstar/android/tvapi/common/PictureManager;->setBacklight(I)V
    :try_end_0
    .catch Lcom/mstar/android/tvapi/common/exception/TvCommonException; {:try_start_0 .. :try_end_0} :catch_0

    :cond_0
    :goto_0
    const-string v2, "TvApp"

    new-instance v3, Ljava/lang/StringBuilder;

    const-string v4, "dbchandler:"

    invoke-direct {v3, v4}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v3, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v2, v3}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    return-void

    :catch_0
    move-exception v0

    invoke-virtual {v0}, Lcom/mstar/android/tvapi/common/exception/TvCommonException;->printStackTrace()V

    goto :goto_0
.end method

.method private dcchandler()V
    .locals 5

    invoke-direct {p0}, Lcom/konka/kkimplements/tv/mstar/DemoDeskImpl;->mapi_GetImageBackLight()I

    move-result v1

    :try_start_0
    invoke-static {}, Lcom/mstar/android/tvapi/common/TvManager;->getInstance()Lcom/mstar/android/tvapi/common/TvManager;

    move-result-object v2

    if-eqz v2, :cond_0

    invoke-static {}, Lcom/mstar/android/tvapi/common/TvManager;->getInstance()Lcom/mstar/android/tvapi/common/TvManager;

    move-result-object v2

    invoke-virtual {v2}, Lcom/mstar/android/tvapi/common/TvManager;->getPictureManager()Lcom/mstar/android/tvapi/common/PictureManager;

    move-result-object v2

    if-eqz v2, :cond_0

    invoke-static {}, Lcom/mstar/android/tvapi/common/TvManager;->getInstance()Lcom/mstar/android/tvapi/common/TvManager;

    move-result-object v2

    invoke-virtual {v2}, Lcom/mstar/android/tvapi/common/TvManager;->getPictureManager()Lcom/mstar/android/tvapi/common/PictureManager;

    move-result-object v2

    int-to-short v3, v1

    invoke-virtual {v2, v3}, Lcom/mstar/android/tvapi/common/PictureManager;->setPictureModeContrast(S)V
    :try_end_0
    .catch Lcom/mstar/android/tvapi/common/exception/TvCommonException; {:try_start_0 .. :try_end_0} :catch_0

    :cond_0
    :goto_0
    const-string v2, "TvApp"

    new-instance v3, Ljava/lang/StringBuilder;

    const-string v4, "dcchandler:"

    invoke-direct {v3, v4}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v3, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v2, v3}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    return-void

    :catch_0
    move-exception v0

    invoke-virtual {v0}, Lcom/mstar/android/tvapi/common/exception/TvCommonException;->printStackTrace()V

    goto :goto_0
.end method

.method public static getDemoMgrInstance(Landroid/content/Context;)Lcom/konka/kkimplements/tv/mstar/DemoDeskImpl;
    .locals 1
    .param p0    # Landroid/content/Context;

    sget-object v0, Lcom/konka/kkimplements/tv/mstar/DemoDeskImpl;->demoMgrImpl:Lcom/konka/kkimplements/tv/mstar/DemoDeskImpl;

    if-nez v0, :cond_0

    new-instance v0, Lcom/konka/kkimplements/tv/mstar/DemoDeskImpl;

    invoke-direct {v0, p0}, Lcom/konka/kkimplements/tv/mstar/DemoDeskImpl;-><init>(Landroid/content/Context;)V

    sput-object v0, Lcom/konka/kkimplements/tv/mstar/DemoDeskImpl;->demoMgrImpl:Lcom/konka/kkimplements/tv/mstar/DemoDeskImpl;

    :cond_0
    sget-object v0, Lcom/konka/kkimplements/tv/mstar/DemoDeskImpl;->demoMgrImpl:Lcom/konka/kkimplements/tv/mstar/DemoDeskImpl;

    return-object v0
.end method

.method private getrandom()I
    .locals 4

    invoke-static {}, Ljava/lang/Math;->random()D

    move-result-wide v0

    const-wide v2, 0x4093480000000000L    # 1234.0

    mul-double/2addr v0, v2

    double-to-int v0, v0

    return v0
.end method

.method private mapi_GetImageBackLight()I
    .locals 8

    const/4 v3, 0x0

    :try_start_0
    invoke-static {}, Lcom/mstar/android/tvapi/common/TvManager;->getInstance()Lcom/mstar/android/tvapi/common/TvManager;

    move-result-object v0

    if-eqz v0, :cond_0

    invoke-static {}, Lcom/mstar/android/tvapi/common/TvManager;->getInstance()Lcom/mstar/android/tvapi/common/TvManager;

    move-result-object v0

    invoke-virtual {v0}, Lcom/mstar/android/tvapi/common/TvManager;->getPictureManager()Lcom/mstar/android/tvapi/common/PictureManager;

    move-result-object v0

    if-eqz v0, :cond_0

    invoke-static {}, Lcom/mstar/android/tvapi/common/TvManager;->getInstance()Lcom/mstar/android/tvapi/common/TvManager;

    move-result-object v0

    invoke-virtual {v0}, Lcom/mstar/android/tvapi/common/TvManager;->getPictureManager()Lcom/mstar/android/tvapi/common/PictureManager;

    move-result-object v0

    invoke-virtual {v0}, Lcom/mstar/android/tvapi/common/PictureManager;->getDlcAverageLuma()S

    move-result v3

    :cond_0
    const-string v0, "TvApp"

    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "getDlcAverageLuma:"

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v1, v3}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I
    :try_end_0
    .catch Lcom/mstar/android/tvapi/common/exception/TvCommonException; {:try_start_0 .. :try_end_0} :catch_0

    :goto_0
    const/16 v0, 0x11

    if-ge v3, v0, :cond_1

    const/16 v1, 0x28

    const/16 v2, 0x14

    const/16 v4, 0x11

    const/4 v5, 0x0

    move-object v0, p0

    invoke-direct/range {v0 .. v5}, Lcom/konka/kkimplements/tv/mstar/DemoDeskImpl;->mapi_GetRealValue(IIIII)I

    move-result v0

    rsub-int/lit8 v7, v0, 0x14

    :goto_1
    return v7

    :catch_0
    move-exception v6

    invoke-virtual {v6}, Lcom/mstar/android/tvapi/common/exception/TvCommonException;->printStackTrace()V

    goto :goto_0

    :cond_1
    const/16 v0, 0x30

    if-ge v3, v0, :cond_2

    const/16 v1, 0x3c

    const/16 v2, 0x28

    const/16 v4, 0x30

    const/16 v5, 0x11

    move-object v0, p0

    invoke-direct/range {v0 .. v5}, Lcom/konka/kkimplements/tv/mstar/DemoDeskImpl;->mapi_GetRealValue(IIIII)I

    move-result v0

    rsub-int/lit8 v7, v0, 0x28

    goto :goto_1

    :cond_2
    const/16 v0, 0x58

    if-ge v3, v0, :cond_3

    const/16 v1, 0x50

    const/16 v2, 0x3c

    const/16 v4, 0x58

    const/16 v5, 0x30

    move-object v0, p0

    invoke-direct/range {v0 .. v5}, Lcom/konka/kkimplements/tv/mstar/DemoDeskImpl;->mapi_GetRealValue(IIIII)I

    move-result v0

    rsub-int/lit8 v7, v0, 0x3c

    goto :goto_1

    :cond_3
    const/16 v0, 0x70

    if-ge v3, v0, :cond_4

    const/16 v1, 0x64

    const/16 v2, 0x50

    const/16 v4, 0x70

    const/16 v5, 0x58

    move-object v0, p0

    invoke-direct/range {v0 .. v5}, Lcom/konka/kkimplements/tv/mstar/DemoDeskImpl;->mapi_GetRealValue(IIIII)I

    move-result v0

    rsub-int/lit8 v7, v0, 0x50

    goto :goto_1

    :cond_4
    const/16 v0, 0x85

    if-ge v3, v0, :cond_5

    const/16 v1, 0x64

    const/16 v2, 0x50

    const/16 v4, 0x85

    const/16 v5, 0x70

    move-object v0, p0

    invoke-direct/range {v0 .. v5}, Lcom/konka/kkimplements/tv/mstar/DemoDeskImpl;->mapi_GetRealValue(IIIII)I

    move-result v0

    add-int/lit8 v7, v0, 0x64

    goto :goto_1

    :cond_5
    const/16 v0, 0xa0

    if-ge v3, v0, :cond_6

    const/16 v1, 0x50

    const/16 v2, 0x3c

    const/16 v4, 0xa0

    const/16 v5, 0xa0

    move-object v0, p0

    invoke-direct/range {v0 .. v5}, Lcom/konka/kkimplements/tv/mstar/DemoDeskImpl;->mapi_GetRealValue(IIIII)I

    move-result v0

    add-int/lit8 v7, v0, 0x50

    goto :goto_1

    :cond_6
    const/16 v0, 0xf0

    if-ge v3, v0, :cond_7

    const/16 v1, 0x3c

    const/16 v2, 0x28

    const/16 v4, 0xf0

    const/16 v5, 0xa0

    move-object v0, p0

    invoke-direct/range {v0 .. v5}, Lcom/konka/kkimplements/tv/mstar/DemoDeskImpl;->mapi_GetRealValue(IIIII)I

    move-result v0

    add-int/lit8 v7, v0, 0x3c

    goto :goto_1

    :cond_7
    const/16 v1, 0x28

    const/16 v2, 0x14

    const/16 v4, 0xff

    const/16 v5, 0xf0

    move-object v0, p0

    invoke-direct/range {v0 .. v5}, Lcom/konka/kkimplements/tv/mstar/DemoDeskImpl;->mapi_GetRealValue(IIIII)I

    move-result v0

    add-int/lit8 v7, v0, 0x28

    goto/16 :goto_1
.end method

.method private mapi_GetRealValue(IIIII)I
    .locals 4
    .param p1    # I
    .param p2    # I
    .param p3    # I
    .param p4    # I
    .param p5    # I

    const/4 v1, 0x0

    sub-int v0, p4, p5

    if-nez v0, :cond_0

    const/4 v0, 0x1

    :cond_0
    sub-int v2, p3, p5

    sub-int v3, p1, p2

    mul-int/2addr v2, v3

    div-int v1, v2, v0

    return v1
.end method


# virtual methods
.method public forceThreadSleep(Z)V
    .locals 0
    .param p1    # Z

    iput-boolean p1, p0, Lcom/konka/kkimplements/tv/mstar/DemoDeskImpl;->bforceThreadSleep:Z

    return-void
.end method

.method public get3DNRStatus()Lcom/mstar/android/tvapi/common/vo/NoiseReduction$EnumNoiseReduction;
    .locals 6

    iget-object v3, p0, Lcom/konka/kkimplements/tv/mstar/DemoDeskImpl;->context:Landroid/content/Context;

    invoke-static {v3}, Lcom/konka/kkimplements/tv/mstar/DataBaseDeskImpl;->getDataBaseMgrInstance(Landroid/content/Context;)Lcom/konka/kkimplements/tv/mstar/DataBaseDeskImpl;

    move-result-object v3

    invoke-virtual {v3}, Lcom/konka/kkimplements/tv/mstar/DataBaseDeskImpl;->getVideo()Lcom/konka/kkinterface/tv/DataBaseDesk$T_MS_VIDEO;

    move-result-object v2

    iget-object v3, v2, Lcom/konka/kkinterface/tv/DataBaseDesk$T_MS_VIDEO;->astPicture:[Lcom/konka/kkinterface/tv/DataBaseDesk$T_MS_PICTURE;

    iget-object v4, v2, Lcom/konka/kkinterface/tv/DataBaseDesk$T_MS_VIDEO;->ePicture:Lcom/konka/kkinterface/tv/DataBaseDesk$EN_MS_PICTURE;

    invoke-virtual {v4}, Lcom/konka/kkinterface/tv/DataBaseDesk$EN_MS_PICTURE;->ordinal()I

    move-result v4

    aget-object v3, v3, v4

    iget-object v3, v3, Lcom/konka/kkinterface/tv/DataBaseDesk$T_MS_PICTURE;->eColorTemp:Lcom/konka/kkinterface/tv/DataBaseDesk$EN_MS_COLOR_TEMP;

    invoke-virtual {v3}, Lcom/konka/kkinterface/tv/DataBaseDesk$EN_MS_COLOR_TEMP;->ordinal()I

    move-result v0

    iget-object v3, v2, Lcom/konka/kkinterface/tv/DataBaseDesk$T_MS_VIDEO;->eNRMode:[Lcom/konka/kkinterface/tv/DataBaseDesk$T_MS_NR_MODE;

    aget-object v3, v3, v0

    iget-object v3, v3, Lcom/konka/kkinterface/tv/DataBaseDesk$T_MS_NR_MODE;->eNR:Lcom/konka/kkinterface/tv/DataBaseDesk$EN_MS_NR;

    invoke-virtual {v3}, Lcom/konka/kkinterface/tv/DataBaseDesk$EN_MS_NR;->ordinal()I

    move-result v1

    sget-object v3, Lcom/mstar/android/tvapi/common/vo/NoiseReduction$EnumNoiseReduction;->E_NR_NUM:Lcom/mstar/android/tvapi/common/vo/NoiseReduction$EnumNoiseReduction;

    invoke-virtual {v3}, Lcom/mstar/android/tvapi/common/vo/NoiseReduction$EnumNoiseReduction;->getValue()I

    move-result v3

    if-ge v1, v3, :cond_0

    invoke-static {}, Lcom/mstar/android/tvapi/common/vo/NoiseReduction$EnumNoiseReduction;->values()[Lcom/mstar/android/tvapi/common/vo/NoiseReduction$EnumNoiseReduction;

    move-result-object v3

    aget-object v3, v3, v1

    iput-object v3, p0, Lcom/konka/kkimplements/tv/mstar/DemoDeskImpl;->e_NR_TYPE:Lcom/mstar/android/tvapi/common/vo/NoiseReduction$EnumNoiseReduction;

    :goto_0
    iget-object v3, p0, Lcom/konka/kkimplements/tv/mstar/DemoDeskImpl;->e_NR_TYPE:Lcom/mstar/android/tvapi/common/vo/NoiseReduction$EnumNoiseReduction;

    return-object v3

    :cond_0
    const-string v3, "Tvapp"

    new-instance v4, Ljava/lang/StringBuilder;

    const-string v5, "get3DNRStatus error:"

    invoke-direct {v4, v5}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v4, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-static {v3, v4}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_0
.end method

.method public getDBCStatus()Lcom/konka/kkinterface/tv/DemoDesk$EN_MS_DBC_TYPE;
    .locals 1

    iget-object v0, p0, Lcom/konka/kkimplements/tv/mstar/DemoDeskImpl;->e_DBC_TYPE:Lcom/konka/kkinterface/tv/DemoDesk$EN_MS_DBC_TYPE;

    return-object v0
.end method

.method public getDCCStatus()Lcom/konka/kkinterface/tv/DemoDesk$EN_MS_DCC_TYPE;
    .locals 1

    iget-object v0, p0, Lcom/konka/kkimplements/tv/mstar/DemoDeskImpl;->e_DCC_TYPE:Lcom/konka/kkinterface/tv/DemoDesk$EN_MS_DCC_TYPE;

    return-object v0
.end method

.method public getDLCStatus()Lcom/konka/kkinterface/tv/DemoDesk$EN_MS_DLC_TYPE;
    .locals 1

    iget-object v0, p0, Lcom/konka/kkimplements/tv/mstar/DemoDeskImpl;->e_DLC_TYPE:Lcom/konka/kkinterface/tv/DemoDesk$EN_MS_DLC_TYPE;

    return-object v0
.end method

.method public getMWEStatus()Lcom/mstar/android/tvapi/common/vo/MweType$EnumMweType;
    .locals 2

    :try_start_0
    invoke-static {}, Lcom/mstar/android/tvapi/common/TvManager;->getInstance()Lcom/mstar/android/tvapi/common/TvManager;

    move-result-object v1

    if-eqz v1, :cond_0

    invoke-static {}, Lcom/mstar/android/tvapi/common/TvManager;->getInstance()Lcom/mstar/android/tvapi/common/TvManager;

    move-result-object v1

    invoke-virtual {v1}, Lcom/mstar/android/tvapi/common/TvManager;->getPictureManager()Lcom/mstar/android/tvapi/common/PictureManager;

    move-result-object v1

    if-eqz v1, :cond_0

    invoke-static {}, Lcom/mstar/android/tvapi/common/TvManager;->getInstance()Lcom/mstar/android/tvapi/common/TvManager;

    move-result-object v1

    invoke-virtual {v1}, Lcom/mstar/android/tvapi/common/TvManager;->getPictureManager()Lcom/mstar/android/tvapi/common/PictureManager;

    move-result-object v1

    invoke-virtual {v1}, Lcom/mstar/android/tvapi/common/PictureManager;->getDemoMode()Lcom/mstar/android/tvapi/common/vo/MweType$EnumMweType;

    move-result-object v1

    iput-object v1, p0, Lcom/konka/kkimplements/tv/mstar/DemoDeskImpl;->e_MWE_TYPE:Lcom/mstar/android/tvapi/common/vo/MweType$EnumMweType;
    :try_end_0
    .catch Lcom/mstar/android/tvapi/common/exception/TvCommonException; {:try_start_0 .. :try_end_0} :catch_0

    :cond_0
    :goto_0
    iget-object v1, p0, Lcom/konka/kkimplements/tv/mstar/DemoDeskImpl;->e_MWE_TYPE:Lcom/mstar/android/tvapi/common/vo/MweType$EnumMweType;

    return-object v1

    :catch_0
    move-exception v0

    invoke-virtual {v0}, Lcom/mstar/android/tvapi/common/exception/TvCommonException;->printStackTrace()V

    goto :goto_0
.end method

.method public getUClearStatus()Lcom/konka/kkinterface/tv/DemoDesk$EN_MS_UCLEAR_TYPE;
    .locals 1

    iget-object v0, p0, Lcom/konka/kkimplements/tv/mstar/DemoDeskImpl;->e_UCLEAR_TYPE:Lcom/konka/kkinterface/tv/DemoDesk$EN_MS_UCLEAR_TYPE;

    return-object v0
.end method

.method public set3DNR(Lcom/mstar/android/tvapi/common/vo/NoiseReduction$EnumNoiseReduction;)V
    .locals 6
    .param p1    # Lcom/mstar/android/tvapi/common/vo/NoiseReduction$EnumNoiseReduction;

    :try_start_0
    iget-object v4, p0, Lcom/konka/kkimplements/tv/mstar/DemoDeskImpl;->context:Landroid/content/Context;

    invoke-static {v4}, Lcom/konka/kkimplements/tv/mstar/DataBaseDeskImpl;->getDataBaseMgrInstance(Landroid/content/Context;)Lcom/konka/kkimplements/tv/mstar/DataBaseDeskImpl;

    move-result-object v4

    invoke-virtual {v4}, Lcom/konka/kkimplements/tv/mstar/DataBaseDeskImpl;->getVideo()Lcom/konka/kkinterface/tv/DataBaseDesk$T_MS_VIDEO;

    move-result-object v3

    iget-object v4, v3, Lcom/konka/kkinterface/tv/DataBaseDesk$T_MS_VIDEO;->astPicture:[Lcom/konka/kkinterface/tv/DataBaseDesk$T_MS_PICTURE;

    iget-object v5, v3, Lcom/konka/kkinterface/tv/DataBaseDesk$T_MS_VIDEO;->ePicture:Lcom/konka/kkinterface/tv/DataBaseDesk$EN_MS_PICTURE;

    invoke-virtual {v5}, Lcom/konka/kkinterface/tv/DataBaseDesk$EN_MS_PICTURE;->ordinal()I

    move-result v5

    aget-object v4, v4, v5

    iget-object v4, v4, Lcom/konka/kkinterface/tv/DataBaseDesk$T_MS_PICTURE;->eColorTemp:Lcom/konka/kkinterface/tv/DataBaseDesk$EN_MS_COLOR_TEMP;

    invoke-virtual {v4}, Lcom/konka/kkinterface/tv/DataBaseDesk$EN_MS_COLOR_TEMP;->ordinal()I

    move-result v0

    iget-object v4, v3, Lcom/konka/kkinterface/tv/DataBaseDesk$T_MS_VIDEO;->eNRMode:[Lcom/konka/kkinterface/tv/DataBaseDesk$T_MS_NR_MODE;

    aget-object v4, v4, v0

    invoke-static {}, Lcom/konka/kkinterface/tv/DataBaseDesk$EN_MS_NR;->values()[Lcom/konka/kkinterface/tv/DataBaseDesk$EN_MS_NR;

    move-result-object v5

    aget-object v5, v5, v0

    iput-object v5, v4, Lcom/konka/kkinterface/tv/DataBaseDesk$T_MS_NR_MODE;->eNR:Lcom/konka/kkinterface/tv/DataBaseDesk$EN_MS_NR;

    invoke-static {}, Lcom/mstar/android/tvapi/common/TvManager;->getInstance()Lcom/mstar/android/tvapi/common/TvManager;

    move-result-object v4

    if-eqz v4, :cond_0

    invoke-static {}, Lcom/mstar/android/tvapi/common/TvManager;->getInstance()Lcom/mstar/android/tvapi/common/TvManager;

    move-result-object v4

    invoke-virtual {v4}, Lcom/mstar/android/tvapi/common/TvManager;->getPictureManager()Lcom/mstar/android/tvapi/common/PictureManager;

    move-result-object v4

    if-eqz v4, :cond_0

    invoke-static {}, Lcom/mstar/android/tvapi/common/TvManager;->getInstance()Lcom/mstar/android/tvapi/common/TvManager;

    move-result-object v4

    invoke-virtual {v4}, Lcom/mstar/android/tvapi/common/TvManager;->getPictureManager()Lcom/mstar/android/tvapi/common/PictureManager;

    move-result-object v4

    invoke-virtual {v4, p1}, Lcom/mstar/android/tvapi/common/PictureManager;->setNoiseReduction(Lcom/mstar/android/tvapi/common/vo/NoiseReduction$EnumNoiseReduction;)Z

    :cond_0
    iget-object v4, p0, Lcom/konka/kkimplements/tv/mstar/DemoDeskImpl;->context:Landroid/content/Context;

    invoke-static {v4}, Lcom/konka/kkimplements/tv/mstar/CommonDeskImpl;->getInstance(Landroid/content/Context;)Lcom/konka/kkinterface/tv/CommonDesk;

    move-result-object v4

    invoke-interface {v4}, Lcom/konka/kkinterface/tv/CommonDesk;->GetCurrentInputSource()Lcom/mstar/android/tvapi/common/vo/TvOsType$EnumInputSource;

    move-result-object v4

    invoke-virtual {v4}, Lcom/mstar/android/tvapi/common/vo/TvOsType$EnumInputSource;->ordinal()I

    move-result v2

    iget-object v4, p0, Lcom/konka/kkimplements/tv/mstar/DemoDeskImpl;->context:Landroid/content/Context;

    invoke-static {v4}, Lcom/konka/kkimplements/tv/mstar/DataBaseDeskImpl;->getDataBaseMgrInstance(Landroid/content/Context;)Lcom/konka/kkimplements/tv/mstar/DataBaseDeskImpl;

    move-result-object v4

    iget-object v5, v3, Lcom/konka/kkinterface/tv/DataBaseDesk$T_MS_VIDEO;->eNRMode:[Lcom/konka/kkinterface/tv/DataBaseDesk$T_MS_NR_MODE;

    aget-object v5, v5, v0

    invoke-virtual {v4, v5, v2, v0}, Lcom/konka/kkimplements/tv/mstar/DataBaseDeskImpl;->updateVideoNRMode(Lcom/konka/kkinterface/tv/DataBaseDesk$T_MS_NR_MODE;II)V
    :try_end_0
    .catch Lcom/mstar/android/tvapi/common/exception/TvCommonException; {:try_start_0 .. :try_end_0} :catch_0

    :goto_0
    iput-object p1, p0, Lcom/konka/kkimplements/tv/mstar/DemoDeskImpl;->e_NR_TYPE:Lcom/mstar/android/tvapi/common/vo/NoiseReduction$EnumNoiseReduction;

    return-void

    :catch_0
    move-exception v1

    invoke-virtual {v1}, Lcom/mstar/android/tvapi/common/exception/TvCommonException;->printStackTrace()V

    goto :goto_0
.end method

.method public setDBCStatus(Lcom/konka/kkinterface/tv/DemoDesk$EN_MS_DBC_TYPE;)V
    .locals 1
    .param p1    # Lcom/konka/kkinterface/tv/DemoDesk$EN_MS_DBC_TYPE;

    iput-object p1, p0, Lcom/konka/kkimplements/tv/mstar/DemoDeskImpl;->e_DBC_TYPE:Lcom/konka/kkinterface/tv/DemoDesk$EN_MS_DBC_TYPE;

    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/konka/kkimplements/tv/mstar/DemoDeskImpl;->bforceThreadSleep:Z

    invoke-direct {p0}, Lcom/konka/kkimplements/tv/mstar/DemoDeskImpl;->ReCntSt()V

    return-void
.end method

.method public setDCCStatus(Lcom/konka/kkinterface/tv/DemoDesk$EN_MS_DCC_TYPE;)V
    .locals 3
    .param p1    # Lcom/konka/kkinterface/tv/DemoDesk$EN_MS_DCC_TYPE;

    const-string v0, "TvApp"

    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "setDCCStatus:"

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    iput-object p1, p0, Lcom/konka/kkimplements/tv/mstar/DemoDeskImpl;->e_DCC_TYPE:Lcom/konka/kkinterface/tv/DemoDesk$EN_MS_DCC_TYPE;

    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/konka/kkimplements/tv/mstar/DemoDeskImpl;->bforceThreadSleep:Z

    invoke-direct {p0}, Lcom/konka/kkimplements/tv/mstar/DemoDeskImpl;->ReCntSt()V

    return-void
.end method

.method public setDLCStatus(Lcom/konka/kkinterface/tv/DemoDesk$EN_MS_DLC_TYPE;)V
    .locals 2
    .param p1    # Lcom/konka/kkinterface/tv/DemoDesk$EN_MS_DLC_TYPE;

    sget-object v1, Lcom/konka/kkinterface/tv/DemoDesk$EN_MS_DLC_TYPE;->E_MS_DLC_ON:Lcom/konka/kkinterface/tv/DemoDesk$EN_MS_DLC_TYPE;

    if-ne p1, v1, :cond_1

    :try_start_0
    invoke-static {}, Lcom/mstar/android/tvapi/common/TvManager;->getInstance()Lcom/mstar/android/tvapi/common/TvManager;

    move-result-object v1

    if-eqz v1, :cond_0

    invoke-static {}, Lcom/mstar/android/tvapi/common/TvManager;->getInstance()Lcom/mstar/android/tvapi/common/TvManager;

    move-result-object v1

    invoke-virtual {v1}, Lcom/mstar/android/tvapi/common/TvManager;->getPictureManager()Lcom/mstar/android/tvapi/common/PictureManager;

    move-result-object v1

    if-eqz v1, :cond_0

    invoke-static {}, Lcom/mstar/android/tvapi/common/TvManager;->getInstance()Lcom/mstar/android/tvapi/common/TvManager;

    move-result-object v1

    invoke-virtual {v1}, Lcom/mstar/android/tvapi/common/TvManager;->getPictureManager()Lcom/mstar/android/tvapi/common/PictureManager;

    move-result-object v1

    invoke-virtual {v1}, Lcom/mstar/android/tvapi/common/PictureManager;->enableDlc()V
    :try_end_0
    .catch Lcom/mstar/android/tvapi/common/exception/TvCommonException; {:try_start_0 .. :try_end_0} :catch_0

    :cond_0
    :goto_0
    iput-object p1, p0, Lcom/konka/kkimplements/tv/mstar/DemoDeskImpl;->e_DLC_TYPE:Lcom/konka/kkinterface/tv/DemoDesk$EN_MS_DLC_TYPE;

    return-void

    :catch_0
    move-exception v0

    invoke-virtual {v0}, Lcom/mstar/android/tvapi/common/exception/TvCommonException;->printStackTrace()V

    goto :goto_0

    :cond_1
    :try_start_1
    invoke-static {}, Lcom/mstar/android/tvapi/common/TvManager;->getInstance()Lcom/mstar/android/tvapi/common/TvManager;

    move-result-object v1

    if-eqz v1, :cond_0

    invoke-static {}, Lcom/mstar/android/tvapi/common/TvManager;->getInstance()Lcom/mstar/android/tvapi/common/TvManager;

    move-result-object v1

    invoke-virtual {v1}, Lcom/mstar/android/tvapi/common/TvManager;->getPictureManager()Lcom/mstar/android/tvapi/common/PictureManager;

    move-result-object v1

    if-eqz v1, :cond_0

    invoke-static {}, Lcom/mstar/android/tvapi/common/TvManager;->getInstance()Lcom/mstar/android/tvapi/common/TvManager;

    move-result-object v1

    invoke-virtual {v1}, Lcom/mstar/android/tvapi/common/TvManager;->getPictureManager()Lcom/mstar/android/tvapi/common/PictureManager;

    move-result-object v1

    invoke-virtual {v1}, Lcom/mstar/android/tvapi/common/PictureManager;->disableDlc()V
    :try_end_1
    .catch Lcom/mstar/android/tvapi/common/exception/TvCommonException; {:try_start_1 .. :try_end_1} :catch_1

    goto :goto_0

    :catch_1
    move-exception v0

    invoke-virtual {v0}, Lcom/mstar/android/tvapi/common/exception/TvCommonException;->printStackTrace()V

    goto :goto_0
.end method

.method public setMWEStatus(Lcom/mstar/android/tvapi/common/vo/MweType$EnumMweType;)V
    .locals 2
    .param p1    # Lcom/mstar/android/tvapi/common/vo/MweType$EnumMweType;

    iput-object p1, p0, Lcom/konka/kkimplements/tv/mstar/DemoDeskImpl;->e_MWE_TYPE:Lcom/mstar/android/tvapi/common/vo/MweType$EnumMweType;

    :try_start_0
    invoke-static {}, Lcom/mstar/android/tvapi/common/TvManager;->getInstance()Lcom/mstar/android/tvapi/common/TvManager;

    move-result-object v1

    if-eqz v1, :cond_0

    invoke-static {}, Lcom/mstar/android/tvapi/common/TvManager;->getInstance()Lcom/mstar/android/tvapi/common/TvManager;

    move-result-object v1

    invoke-virtual {v1}, Lcom/mstar/android/tvapi/common/TvManager;->getPictureManager()Lcom/mstar/android/tvapi/common/PictureManager;

    move-result-object v1

    if-eqz v1, :cond_0

    invoke-static {}, Lcom/mstar/android/tvapi/common/TvManager;->getInstance()Lcom/mstar/android/tvapi/common/TvManager;

    move-result-object v1

    invoke-virtual {v1}, Lcom/mstar/android/tvapi/common/TvManager;->getPictureManager()Lcom/mstar/android/tvapi/common/PictureManager;

    move-result-object v1

    invoke-virtual {v1, p1}, Lcom/mstar/android/tvapi/common/PictureManager;->setDemoMode(Lcom/mstar/android/tvapi/common/vo/MweType$EnumMweType;)V
    :try_end_0
    .catch Lcom/mstar/android/tvapi/common/exception/TvCommonException; {:try_start_0 .. :try_end_0} :catch_0

    :cond_0
    :goto_0
    return-void

    :catch_0
    move-exception v0

    invoke-virtual {v0}, Lcom/mstar/android/tvapi/common/exception/TvCommonException;->printStackTrace()V

    goto :goto_0
.end method

.method public setUClearStatus(Lcom/konka/kkinterface/tv/DemoDesk$EN_MS_UCLEAR_TYPE;)V
    .locals 3
    .param p1    # Lcom/konka/kkinterface/tv/DemoDesk$EN_MS_UCLEAR_TYPE;

    sget-object v1, Lcom/konka/kkinterface/tv/DemoDesk$EN_MS_UCLEAR_TYPE;->E_MS_UCLEAR_ON:Lcom/konka/kkinterface/tv/DemoDesk$EN_MS_UCLEAR_TYPE;

    if-ne p1, v1, :cond_1

    :try_start_0
    invoke-static {}, Lcom/mstar/android/tvapi/common/TvManager;->getInstance()Lcom/mstar/android/tvapi/common/TvManager;

    move-result-object v1

    if-eqz v1, :cond_0

    invoke-static {}, Lcom/mstar/android/tvapi/common/TvManager;->getInstance()Lcom/mstar/android/tvapi/common/TvManager;

    move-result-object v1

    invoke-virtual {v1}, Lcom/mstar/android/tvapi/common/TvManager;->getPictureManager()Lcom/mstar/android/tvapi/common/PictureManager;

    move-result-object v1

    if-eqz v1, :cond_0

    invoke-static {}, Lcom/mstar/android/tvapi/common/TvManager;->getInstance()Lcom/mstar/android/tvapi/common/TvManager;

    move-result-object v1

    invoke-virtual {v1}, Lcom/mstar/android/tvapi/common/TvManager;->getPictureManager()Lcom/mstar/android/tvapi/common/PictureManager;

    move-result-object v1

    const/4 v2, 0x1

    invoke-virtual {v1, v2}, Lcom/mstar/android/tvapi/common/PictureManager;->setUltraClear(Z)Z
    :try_end_0
    .catch Lcom/mstar/android/tvapi/common/exception/TvCommonException; {:try_start_0 .. :try_end_0} :catch_0

    :cond_0
    :goto_0
    iput-object p1, p0, Lcom/konka/kkimplements/tv/mstar/DemoDeskImpl;->e_UCLEAR_TYPE:Lcom/konka/kkinterface/tv/DemoDesk$EN_MS_UCLEAR_TYPE;

    return-void

    :catch_0
    move-exception v0

    invoke-virtual {v0}, Lcom/mstar/android/tvapi/common/exception/TvCommonException;->printStackTrace()V

    goto :goto_0

    :cond_1
    :try_start_1
    invoke-static {}, Lcom/mstar/android/tvapi/common/TvManager;->getInstance()Lcom/mstar/android/tvapi/common/TvManager;

    move-result-object v1

    if-eqz v1, :cond_0

    invoke-static {}, Lcom/mstar/android/tvapi/common/TvManager;->getInstance()Lcom/mstar/android/tvapi/common/TvManager;

    move-result-object v1

    invoke-virtual {v1}, Lcom/mstar/android/tvapi/common/TvManager;->getPictureManager()Lcom/mstar/android/tvapi/common/PictureManager;

    move-result-object v1

    if-eqz v1, :cond_0

    invoke-static {}, Lcom/mstar/android/tvapi/common/TvManager;->getInstance()Lcom/mstar/android/tvapi/common/TvManager;

    move-result-object v1

    invoke-virtual {v1}, Lcom/mstar/android/tvapi/common/TvManager;->getPictureManager()Lcom/mstar/android/tvapi/common/PictureManager;

    move-result-object v1

    const/4 v2, 0x0

    invoke-virtual {v1, v2}, Lcom/mstar/android/tvapi/common/PictureManager;->setUltraClear(Z)Z
    :try_end_1
    .catch Lcom/mstar/android/tvapi/common/exception/TvCommonException; {:try_start_1 .. :try_end_1} :catch_1

    goto :goto_0

    :catch_1
    move-exception v0

    invoke-virtual {v0}, Lcom/mstar/android/tvapi/common/exception/TvCommonException;->printStackTrace()V

    goto :goto_0
.end method
