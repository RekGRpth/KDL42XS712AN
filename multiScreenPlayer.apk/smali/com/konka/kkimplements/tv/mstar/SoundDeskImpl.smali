.class public Lcom/konka/kkimplements/tv/mstar/SoundDeskImpl;
.super Lcom/konka/kkimplements/tv/mstar/BaseDeskImpl;
.source "SoundDeskImpl.java"

# interfaces
.implements Lcom/konka/kkinterface/tv/SoundDesk;


# static fields
.field private static synthetic $SWITCH_TABLE$com$konka$kkinterface$tv$DataBaseDesk$SPDIF_TYPE:[I

.field private static synthetic $SWITCH_TABLE$com$mstar$android$tvapi$common$vo$TvOsType$EnumInputSource:[I

.field private static soundMgrImpl:Lcom/konka/kkimplements/tv/mstar/SoundDeskImpl;


# instance fields
.field private com:Lcom/konka/kkinterface/tv/CommonDesk;

.field private context:Landroid/content/Context;

.field private databaseMgr:Lcom/konka/kkimplements/tv/mstar/DataBaseDeskImpl;

.field private userSoundSetting:Lcom/konka/kkinterface/tv/DataBaseDesk$MS_USER_SOUND_SETTING;


# direct methods
.method static synthetic $SWITCH_TABLE$com$konka$kkinterface$tv$DataBaseDesk$SPDIF_TYPE()[I
    .locals 3

    sget-object v0, Lcom/konka/kkimplements/tv/mstar/SoundDeskImpl;->$SWITCH_TABLE$com$konka$kkinterface$tv$DataBaseDesk$SPDIF_TYPE:[I

    if-eqz v0, :cond_0

    :goto_0
    return-object v0

    :cond_0
    invoke-static {}, Lcom/konka/kkinterface/tv/DataBaseDesk$SPDIF_TYPE;->values()[Lcom/konka/kkinterface/tv/DataBaseDesk$SPDIF_TYPE;

    move-result-object v0

    array-length v0, v0

    new-array v0, v0, [I

    :try_start_0
    sget-object v1, Lcom/konka/kkinterface/tv/DataBaseDesk$SPDIF_TYPE;->MSAPI_AUD_SPDIF_NONPCM_:Lcom/konka/kkinterface/tv/DataBaseDesk$SPDIF_TYPE;

    invoke-virtual {v1}, Lcom/konka/kkinterface/tv/DataBaseDesk$SPDIF_TYPE;->ordinal()I

    move-result v1

    const/4 v2, 0x3

    aput v2, v0, v1
    :try_end_0
    .catch Ljava/lang/NoSuchFieldError; {:try_start_0 .. :try_end_0} :catch_2

    :goto_1
    :try_start_1
    sget-object v1, Lcom/konka/kkinterface/tv/DataBaseDesk$SPDIF_TYPE;->MSAPI_AUD_SPDIF_OFF_:Lcom/konka/kkinterface/tv/DataBaseDesk$SPDIF_TYPE;

    invoke-virtual {v1}, Lcom/konka/kkinterface/tv/DataBaseDesk$SPDIF_TYPE;->ordinal()I

    move-result v1

    const/4 v2, 0x2

    aput v2, v0, v1
    :try_end_1
    .catch Ljava/lang/NoSuchFieldError; {:try_start_1 .. :try_end_1} :catch_1

    :goto_2
    :try_start_2
    sget-object v1, Lcom/konka/kkinterface/tv/DataBaseDesk$SPDIF_TYPE;->MSAPI_AUD_SPDIF_PCM_:Lcom/konka/kkinterface/tv/DataBaseDesk$SPDIF_TYPE;

    invoke-virtual {v1}, Lcom/konka/kkinterface/tv/DataBaseDesk$SPDIF_TYPE;->ordinal()I

    move-result v1

    const/4 v2, 0x1

    aput v2, v0, v1
    :try_end_2
    .catch Ljava/lang/NoSuchFieldError; {:try_start_2 .. :try_end_2} :catch_0

    :goto_3
    sput-object v0, Lcom/konka/kkimplements/tv/mstar/SoundDeskImpl;->$SWITCH_TABLE$com$konka$kkinterface$tv$DataBaseDesk$SPDIF_TYPE:[I

    goto :goto_0

    :catch_0
    move-exception v1

    goto :goto_3

    :catch_1
    move-exception v1

    goto :goto_2

    :catch_2
    move-exception v1

    goto :goto_1
.end method

.method static synthetic $SWITCH_TABLE$com$mstar$android$tvapi$common$vo$TvOsType$EnumInputSource()[I
    .locals 3

    sget-object v0, Lcom/konka/kkimplements/tv/mstar/SoundDeskImpl;->$SWITCH_TABLE$com$mstar$android$tvapi$common$vo$TvOsType$EnumInputSource:[I

    if-eqz v0, :cond_0

    :goto_0
    return-object v0

    :cond_0
    invoke-static {}, Lcom/mstar/android/tvapi/common/vo/TvOsType$EnumInputSource;->values()[Lcom/mstar/android/tvapi/common/vo/TvOsType$EnumInputSource;

    move-result-object v0

    array-length v0, v0

    new-array v0, v0, [I

    :try_start_0
    sget-object v1, Lcom/mstar/android/tvapi/common/vo/TvOsType$EnumInputSource;->E_INPUT_SOURCE_ATV:Lcom/mstar/android/tvapi/common/vo/TvOsType$EnumInputSource;

    invoke-virtual {v1}, Lcom/mstar/android/tvapi/common/vo/TvOsType$EnumInputSource;->ordinal()I

    move-result v1

    const/4 v2, 0x2

    aput v2, v0, v1
    :try_end_0
    .catch Ljava/lang/NoSuchFieldError; {:try_start_0 .. :try_end_0} :catch_28

    :goto_1
    :try_start_1
    sget-object v1, Lcom/mstar/android/tvapi/common/vo/TvOsType$EnumInputSource;->E_INPUT_SOURCE_CVBS:Lcom/mstar/android/tvapi/common/vo/TvOsType$EnumInputSource;

    invoke-virtual {v1}, Lcom/mstar/android/tvapi/common/vo/TvOsType$EnumInputSource;->ordinal()I

    move-result v1

    const/4 v2, 0x3

    aput v2, v0, v1
    :try_end_1
    .catch Ljava/lang/NoSuchFieldError; {:try_start_1 .. :try_end_1} :catch_27

    :goto_2
    :try_start_2
    sget-object v1, Lcom/mstar/android/tvapi/common/vo/TvOsType$EnumInputSource;->E_INPUT_SOURCE_CVBS2:Lcom/mstar/android/tvapi/common/vo/TvOsType$EnumInputSource;

    invoke-virtual {v1}, Lcom/mstar/android/tvapi/common/vo/TvOsType$EnumInputSource;->ordinal()I

    move-result v1

    const/4 v2, 0x4

    aput v2, v0, v1
    :try_end_2
    .catch Ljava/lang/NoSuchFieldError; {:try_start_2 .. :try_end_2} :catch_26

    :goto_3
    :try_start_3
    sget-object v1, Lcom/mstar/android/tvapi/common/vo/TvOsType$EnumInputSource;->E_INPUT_SOURCE_CVBS3:Lcom/mstar/android/tvapi/common/vo/TvOsType$EnumInputSource;

    invoke-virtual {v1}, Lcom/mstar/android/tvapi/common/vo/TvOsType$EnumInputSource;->ordinal()I

    move-result v1

    const/4 v2, 0x5

    aput v2, v0, v1
    :try_end_3
    .catch Ljava/lang/NoSuchFieldError; {:try_start_3 .. :try_end_3} :catch_25

    :goto_4
    :try_start_4
    sget-object v1, Lcom/mstar/android/tvapi/common/vo/TvOsType$EnumInputSource;->E_INPUT_SOURCE_CVBS4:Lcom/mstar/android/tvapi/common/vo/TvOsType$EnumInputSource;

    invoke-virtual {v1}, Lcom/mstar/android/tvapi/common/vo/TvOsType$EnumInputSource;->ordinal()I

    move-result v1

    const/4 v2, 0x6

    aput v2, v0, v1
    :try_end_4
    .catch Ljava/lang/NoSuchFieldError; {:try_start_4 .. :try_end_4} :catch_24

    :goto_5
    :try_start_5
    sget-object v1, Lcom/mstar/android/tvapi/common/vo/TvOsType$EnumInputSource;->E_INPUT_SOURCE_CVBS5:Lcom/mstar/android/tvapi/common/vo/TvOsType$EnumInputSource;

    invoke-virtual {v1}, Lcom/mstar/android/tvapi/common/vo/TvOsType$EnumInputSource;->ordinal()I

    move-result v1

    const/4 v2, 0x7

    aput v2, v0, v1
    :try_end_5
    .catch Ljava/lang/NoSuchFieldError; {:try_start_5 .. :try_end_5} :catch_23

    :goto_6
    :try_start_6
    sget-object v1, Lcom/mstar/android/tvapi/common/vo/TvOsType$EnumInputSource;->E_INPUT_SOURCE_CVBS6:Lcom/mstar/android/tvapi/common/vo/TvOsType$EnumInputSource;

    invoke-virtual {v1}, Lcom/mstar/android/tvapi/common/vo/TvOsType$EnumInputSource;->ordinal()I

    move-result v1

    const/16 v2, 0x8

    aput v2, v0, v1
    :try_end_6
    .catch Ljava/lang/NoSuchFieldError; {:try_start_6 .. :try_end_6} :catch_22

    :goto_7
    :try_start_7
    sget-object v1, Lcom/mstar/android/tvapi/common/vo/TvOsType$EnumInputSource;->E_INPUT_SOURCE_CVBS7:Lcom/mstar/android/tvapi/common/vo/TvOsType$EnumInputSource;

    invoke-virtual {v1}, Lcom/mstar/android/tvapi/common/vo/TvOsType$EnumInputSource;->ordinal()I

    move-result v1

    const/16 v2, 0x9

    aput v2, v0, v1
    :try_end_7
    .catch Ljava/lang/NoSuchFieldError; {:try_start_7 .. :try_end_7} :catch_21

    :goto_8
    :try_start_8
    sget-object v1, Lcom/mstar/android/tvapi/common/vo/TvOsType$EnumInputSource;->E_INPUT_SOURCE_CVBS8:Lcom/mstar/android/tvapi/common/vo/TvOsType$EnumInputSource;

    invoke-virtual {v1}, Lcom/mstar/android/tvapi/common/vo/TvOsType$EnumInputSource;->ordinal()I

    move-result v1

    const/16 v2, 0xa

    aput v2, v0, v1
    :try_end_8
    .catch Ljava/lang/NoSuchFieldError; {:try_start_8 .. :try_end_8} :catch_20

    :goto_9
    :try_start_9
    sget-object v1, Lcom/mstar/android/tvapi/common/vo/TvOsType$EnumInputSource;->E_INPUT_SOURCE_CVBS_MAX:Lcom/mstar/android/tvapi/common/vo/TvOsType$EnumInputSource;

    invoke-virtual {v1}, Lcom/mstar/android/tvapi/common/vo/TvOsType$EnumInputSource;->ordinal()I

    move-result v1

    const/16 v2, 0xb

    aput v2, v0, v1
    :try_end_9
    .catch Ljava/lang/NoSuchFieldError; {:try_start_9 .. :try_end_9} :catch_1f

    :goto_a
    :try_start_a
    sget-object v1, Lcom/mstar/android/tvapi/common/vo/TvOsType$EnumInputSource;->E_INPUT_SOURCE_DTV:Lcom/mstar/android/tvapi/common/vo/TvOsType$EnumInputSource;

    invoke-virtual {v1}, Lcom/mstar/android/tvapi/common/vo/TvOsType$EnumInputSource;->ordinal()I

    move-result v1

    const/16 v2, 0x1d

    aput v2, v0, v1
    :try_end_a
    .catch Ljava/lang/NoSuchFieldError; {:try_start_a .. :try_end_a} :catch_1e

    :goto_b
    :try_start_b
    sget-object v1, Lcom/mstar/android/tvapi/common/vo/TvOsType$EnumInputSource;->E_INPUT_SOURCE_DTV2:Lcom/mstar/android/tvapi/common/vo/TvOsType$EnumInputSource;

    invoke-virtual {v1}, Lcom/mstar/android/tvapi/common/vo/TvOsType$EnumInputSource;->ordinal()I

    move-result v1

    const/16 v2, 0x26

    aput v2, v0, v1
    :try_end_b
    .catch Ljava/lang/NoSuchFieldError; {:try_start_b .. :try_end_b} :catch_1d

    :goto_c
    :try_start_c
    sget-object v1, Lcom/mstar/android/tvapi/common/vo/TvOsType$EnumInputSource;->E_INPUT_SOURCE_DVI:Lcom/mstar/android/tvapi/common/vo/TvOsType$EnumInputSource;

    invoke-virtual {v1}, Lcom/mstar/android/tvapi/common/vo/TvOsType$EnumInputSource;->ordinal()I

    move-result v1

    const/16 v2, 0x1e

    aput v2, v0, v1
    :try_end_c
    .catch Ljava/lang/NoSuchFieldError; {:try_start_c .. :try_end_c} :catch_1c

    :goto_d
    :try_start_d
    sget-object v1, Lcom/mstar/android/tvapi/common/vo/TvOsType$EnumInputSource;->E_INPUT_SOURCE_DVI2:Lcom/mstar/android/tvapi/common/vo/TvOsType$EnumInputSource;

    invoke-virtual {v1}, Lcom/mstar/android/tvapi/common/vo/TvOsType$EnumInputSource;->ordinal()I

    move-result v1

    const/16 v2, 0x1f

    aput v2, v0, v1
    :try_end_d
    .catch Ljava/lang/NoSuchFieldError; {:try_start_d .. :try_end_d} :catch_1b

    :goto_e
    :try_start_e
    sget-object v1, Lcom/mstar/android/tvapi/common/vo/TvOsType$EnumInputSource;->E_INPUT_SOURCE_DVI3:Lcom/mstar/android/tvapi/common/vo/TvOsType$EnumInputSource;

    invoke-virtual {v1}, Lcom/mstar/android/tvapi/common/vo/TvOsType$EnumInputSource;->ordinal()I

    move-result v1

    const/16 v2, 0x20

    aput v2, v0, v1
    :try_end_e
    .catch Ljava/lang/NoSuchFieldError; {:try_start_e .. :try_end_e} :catch_1a

    :goto_f
    :try_start_f
    sget-object v1, Lcom/mstar/android/tvapi/common/vo/TvOsType$EnumInputSource;->E_INPUT_SOURCE_DVI4:Lcom/mstar/android/tvapi/common/vo/TvOsType$EnumInputSource;

    invoke-virtual {v1}, Lcom/mstar/android/tvapi/common/vo/TvOsType$EnumInputSource;->ordinal()I

    move-result v1

    const/16 v2, 0x21

    aput v2, v0, v1
    :try_end_f
    .catch Ljava/lang/NoSuchFieldError; {:try_start_f .. :try_end_f} :catch_19

    :goto_10
    :try_start_10
    sget-object v1, Lcom/mstar/android/tvapi/common/vo/TvOsType$EnumInputSource;->E_INPUT_SOURCE_DVI_MAX:Lcom/mstar/android/tvapi/common/vo/TvOsType$EnumInputSource;

    invoke-virtual {v1}, Lcom/mstar/android/tvapi/common/vo/TvOsType$EnumInputSource;->ordinal()I

    move-result v1

    const/16 v2, 0x22

    aput v2, v0, v1
    :try_end_10
    .catch Ljava/lang/NoSuchFieldError; {:try_start_10 .. :try_end_10} :catch_18

    :goto_11
    :try_start_11
    sget-object v1, Lcom/mstar/android/tvapi/common/vo/TvOsType$EnumInputSource;->E_INPUT_SOURCE_HDMI:Lcom/mstar/android/tvapi/common/vo/TvOsType$EnumInputSource;

    invoke-virtual {v1}, Lcom/mstar/android/tvapi/common/vo/TvOsType$EnumInputSource;->ordinal()I

    move-result v1

    const/16 v2, 0x18

    aput v2, v0, v1
    :try_end_11
    .catch Ljava/lang/NoSuchFieldError; {:try_start_11 .. :try_end_11} :catch_17

    :goto_12
    :try_start_12
    sget-object v1, Lcom/mstar/android/tvapi/common/vo/TvOsType$EnumInputSource;->E_INPUT_SOURCE_HDMI2:Lcom/mstar/android/tvapi/common/vo/TvOsType$EnumInputSource;

    invoke-virtual {v1}, Lcom/mstar/android/tvapi/common/vo/TvOsType$EnumInputSource;->ordinal()I

    move-result v1

    const/16 v2, 0x19

    aput v2, v0, v1
    :try_end_12
    .catch Ljava/lang/NoSuchFieldError; {:try_start_12 .. :try_end_12} :catch_16

    :goto_13
    :try_start_13
    sget-object v1, Lcom/mstar/android/tvapi/common/vo/TvOsType$EnumInputSource;->E_INPUT_SOURCE_HDMI3:Lcom/mstar/android/tvapi/common/vo/TvOsType$EnumInputSource;

    invoke-virtual {v1}, Lcom/mstar/android/tvapi/common/vo/TvOsType$EnumInputSource;->ordinal()I

    move-result v1

    const/16 v2, 0x1a

    aput v2, v0, v1
    :try_end_13
    .catch Ljava/lang/NoSuchFieldError; {:try_start_13 .. :try_end_13} :catch_15

    :goto_14
    :try_start_14
    sget-object v1, Lcom/mstar/android/tvapi/common/vo/TvOsType$EnumInputSource;->E_INPUT_SOURCE_HDMI4:Lcom/mstar/android/tvapi/common/vo/TvOsType$EnumInputSource;

    invoke-virtual {v1}, Lcom/mstar/android/tvapi/common/vo/TvOsType$EnumInputSource;->ordinal()I

    move-result v1

    const/16 v2, 0x1b

    aput v2, v0, v1
    :try_end_14
    .catch Ljava/lang/NoSuchFieldError; {:try_start_14 .. :try_end_14} :catch_14

    :goto_15
    :try_start_15
    sget-object v1, Lcom/mstar/android/tvapi/common/vo/TvOsType$EnumInputSource;->E_INPUT_SOURCE_HDMI_MAX:Lcom/mstar/android/tvapi/common/vo/TvOsType$EnumInputSource;

    invoke-virtual {v1}, Lcom/mstar/android/tvapi/common/vo/TvOsType$EnumInputSource;->ordinal()I

    move-result v1

    const/16 v2, 0x1c

    aput v2, v0, v1
    :try_end_15
    .catch Ljava/lang/NoSuchFieldError; {:try_start_15 .. :try_end_15} :catch_13

    :goto_16
    :try_start_16
    sget-object v1, Lcom/mstar/android/tvapi/common/vo/TvOsType$EnumInputSource;->E_INPUT_SOURCE_JPEG:Lcom/mstar/android/tvapi/common/vo/TvOsType$EnumInputSource;

    invoke-virtual {v1}, Lcom/mstar/android/tvapi/common/vo/TvOsType$EnumInputSource;->ordinal()I

    move-result v1

    const/16 v2, 0x25

    aput v2, v0, v1
    :try_end_16
    .catch Ljava/lang/NoSuchFieldError; {:try_start_16 .. :try_end_16} :catch_12

    :goto_17
    :try_start_17
    sget-object v1, Lcom/mstar/android/tvapi/common/vo/TvOsType$EnumInputSource;->E_INPUT_SOURCE_KTV:Lcom/mstar/android/tvapi/common/vo/TvOsType$EnumInputSource;

    invoke-virtual {v1}, Lcom/mstar/android/tvapi/common/vo/TvOsType$EnumInputSource;->ordinal()I

    move-result v1

    const/16 v2, 0x24

    aput v2, v0, v1
    :try_end_17
    .catch Ljava/lang/NoSuchFieldError; {:try_start_17 .. :try_end_17} :catch_11

    :goto_18
    :try_start_18
    sget-object v1, Lcom/mstar/android/tvapi/common/vo/TvOsType$EnumInputSource;->E_INPUT_SOURCE_NONE:Lcom/mstar/android/tvapi/common/vo/TvOsType$EnumInputSource;

    invoke-virtual {v1}, Lcom/mstar/android/tvapi/common/vo/TvOsType$EnumInputSource;->ordinal()I

    move-result v1

    const/16 v2, 0x29

    aput v2, v0, v1
    :try_end_18
    .catch Ljava/lang/NoSuchFieldError; {:try_start_18 .. :try_end_18} :catch_10

    :goto_19
    :try_start_19
    sget-object v1, Lcom/mstar/android/tvapi/common/vo/TvOsType$EnumInputSource;->E_INPUT_SOURCE_NUM:Lcom/mstar/android/tvapi/common/vo/TvOsType$EnumInputSource;

    invoke-virtual {v1}, Lcom/mstar/android/tvapi/common/vo/TvOsType$EnumInputSource;->ordinal()I

    move-result v1

    const/16 v2, 0x28

    aput v2, v0, v1
    :try_end_19
    .catch Ljava/lang/NoSuchFieldError; {:try_start_19 .. :try_end_19} :catch_f

    :goto_1a
    :try_start_1a
    sget-object v1, Lcom/mstar/android/tvapi/common/vo/TvOsType$EnumInputSource;->E_INPUT_SOURCE_SCART:Lcom/mstar/android/tvapi/common/vo/TvOsType$EnumInputSource;

    invoke-virtual {v1}, Lcom/mstar/android/tvapi/common/vo/TvOsType$EnumInputSource;->ordinal()I

    move-result v1

    const/16 v2, 0x15

    aput v2, v0, v1
    :try_end_1a
    .catch Ljava/lang/NoSuchFieldError; {:try_start_1a .. :try_end_1a} :catch_e

    :goto_1b
    :try_start_1b
    sget-object v1, Lcom/mstar/android/tvapi/common/vo/TvOsType$EnumInputSource;->E_INPUT_SOURCE_SCART2:Lcom/mstar/android/tvapi/common/vo/TvOsType$EnumInputSource;

    invoke-virtual {v1}, Lcom/mstar/android/tvapi/common/vo/TvOsType$EnumInputSource;->ordinal()I

    move-result v1

    const/16 v2, 0x16

    aput v2, v0, v1
    :try_end_1b
    .catch Ljava/lang/NoSuchFieldError; {:try_start_1b .. :try_end_1b} :catch_d

    :goto_1c
    :try_start_1c
    sget-object v1, Lcom/mstar/android/tvapi/common/vo/TvOsType$EnumInputSource;->E_INPUT_SOURCE_SCART_MAX:Lcom/mstar/android/tvapi/common/vo/TvOsType$EnumInputSource;

    invoke-virtual {v1}, Lcom/mstar/android/tvapi/common/vo/TvOsType$EnumInputSource;->ordinal()I

    move-result v1

    const/16 v2, 0x17

    aput v2, v0, v1
    :try_end_1c
    .catch Ljava/lang/NoSuchFieldError; {:try_start_1c .. :try_end_1c} :catch_c

    :goto_1d
    :try_start_1d
    sget-object v1, Lcom/mstar/android/tvapi/common/vo/TvOsType$EnumInputSource;->E_INPUT_SOURCE_STORAGE:Lcom/mstar/android/tvapi/common/vo/TvOsType$EnumInputSource;

    invoke-virtual {v1}, Lcom/mstar/android/tvapi/common/vo/TvOsType$EnumInputSource;->ordinal()I

    move-result v1

    const/16 v2, 0x23

    aput v2, v0, v1
    :try_end_1d
    .catch Ljava/lang/NoSuchFieldError; {:try_start_1d .. :try_end_1d} :catch_b

    :goto_1e
    :try_start_1e
    sget-object v1, Lcom/mstar/android/tvapi/common/vo/TvOsType$EnumInputSource;->E_INPUT_SOURCE_STORAGE2:Lcom/mstar/android/tvapi/common/vo/TvOsType$EnumInputSource;

    invoke-virtual {v1}, Lcom/mstar/android/tvapi/common/vo/TvOsType$EnumInputSource;->ordinal()I

    move-result v1

    const/16 v2, 0x27

    aput v2, v0, v1
    :try_end_1e
    .catch Ljava/lang/NoSuchFieldError; {:try_start_1e .. :try_end_1e} :catch_a

    :goto_1f
    :try_start_1f
    sget-object v1, Lcom/mstar/android/tvapi/common/vo/TvOsType$EnumInputSource;->E_INPUT_SOURCE_SVIDEO:Lcom/mstar/android/tvapi/common/vo/TvOsType$EnumInputSource;

    invoke-virtual {v1}, Lcom/mstar/android/tvapi/common/vo/TvOsType$EnumInputSource;->ordinal()I

    move-result v1

    const/16 v2, 0xc

    aput v2, v0, v1
    :try_end_1f
    .catch Ljava/lang/NoSuchFieldError; {:try_start_1f .. :try_end_1f} :catch_9

    :goto_20
    :try_start_20
    sget-object v1, Lcom/mstar/android/tvapi/common/vo/TvOsType$EnumInputSource;->E_INPUT_SOURCE_SVIDEO2:Lcom/mstar/android/tvapi/common/vo/TvOsType$EnumInputSource;

    invoke-virtual {v1}, Lcom/mstar/android/tvapi/common/vo/TvOsType$EnumInputSource;->ordinal()I

    move-result v1

    const/16 v2, 0xd

    aput v2, v0, v1
    :try_end_20
    .catch Ljava/lang/NoSuchFieldError; {:try_start_20 .. :try_end_20} :catch_8

    :goto_21
    :try_start_21
    sget-object v1, Lcom/mstar/android/tvapi/common/vo/TvOsType$EnumInputSource;->E_INPUT_SOURCE_SVIDEO3:Lcom/mstar/android/tvapi/common/vo/TvOsType$EnumInputSource;

    invoke-virtual {v1}, Lcom/mstar/android/tvapi/common/vo/TvOsType$EnumInputSource;->ordinal()I

    move-result v1

    const/16 v2, 0xe

    aput v2, v0, v1
    :try_end_21
    .catch Ljava/lang/NoSuchFieldError; {:try_start_21 .. :try_end_21} :catch_7

    :goto_22
    :try_start_22
    sget-object v1, Lcom/mstar/android/tvapi/common/vo/TvOsType$EnumInputSource;->E_INPUT_SOURCE_SVIDEO4:Lcom/mstar/android/tvapi/common/vo/TvOsType$EnumInputSource;

    invoke-virtual {v1}, Lcom/mstar/android/tvapi/common/vo/TvOsType$EnumInputSource;->ordinal()I

    move-result v1

    const/16 v2, 0xf

    aput v2, v0, v1
    :try_end_22
    .catch Ljava/lang/NoSuchFieldError; {:try_start_22 .. :try_end_22} :catch_6

    :goto_23
    :try_start_23
    sget-object v1, Lcom/mstar/android/tvapi/common/vo/TvOsType$EnumInputSource;->E_INPUT_SOURCE_SVIDEO_MAX:Lcom/mstar/android/tvapi/common/vo/TvOsType$EnumInputSource;

    invoke-virtual {v1}, Lcom/mstar/android/tvapi/common/vo/TvOsType$EnumInputSource;->ordinal()I

    move-result v1

    const/16 v2, 0x10

    aput v2, v0, v1
    :try_end_23
    .catch Ljava/lang/NoSuchFieldError; {:try_start_23 .. :try_end_23} :catch_5

    :goto_24
    :try_start_24
    sget-object v1, Lcom/mstar/android/tvapi/common/vo/TvOsType$EnumInputSource;->E_INPUT_SOURCE_VGA:Lcom/mstar/android/tvapi/common/vo/TvOsType$EnumInputSource;

    invoke-virtual {v1}, Lcom/mstar/android/tvapi/common/vo/TvOsType$EnumInputSource;->ordinal()I

    move-result v1

    const/4 v2, 0x1

    aput v2, v0, v1
    :try_end_24
    .catch Ljava/lang/NoSuchFieldError; {:try_start_24 .. :try_end_24} :catch_4

    :goto_25
    :try_start_25
    sget-object v1, Lcom/mstar/android/tvapi/common/vo/TvOsType$EnumInputSource;->E_INPUT_SOURCE_YPBPR:Lcom/mstar/android/tvapi/common/vo/TvOsType$EnumInputSource;

    invoke-virtual {v1}, Lcom/mstar/android/tvapi/common/vo/TvOsType$EnumInputSource;->ordinal()I

    move-result v1

    const/16 v2, 0x11

    aput v2, v0, v1
    :try_end_25
    .catch Ljava/lang/NoSuchFieldError; {:try_start_25 .. :try_end_25} :catch_3

    :goto_26
    :try_start_26
    sget-object v1, Lcom/mstar/android/tvapi/common/vo/TvOsType$EnumInputSource;->E_INPUT_SOURCE_YPBPR2:Lcom/mstar/android/tvapi/common/vo/TvOsType$EnumInputSource;

    invoke-virtual {v1}, Lcom/mstar/android/tvapi/common/vo/TvOsType$EnumInputSource;->ordinal()I

    move-result v1

    const/16 v2, 0x12

    aput v2, v0, v1
    :try_end_26
    .catch Ljava/lang/NoSuchFieldError; {:try_start_26 .. :try_end_26} :catch_2

    :goto_27
    :try_start_27
    sget-object v1, Lcom/mstar/android/tvapi/common/vo/TvOsType$EnumInputSource;->E_INPUT_SOURCE_YPBPR3:Lcom/mstar/android/tvapi/common/vo/TvOsType$EnumInputSource;

    invoke-virtual {v1}, Lcom/mstar/android/tvapi/common/vo/TvOsType$EnumInputSource;->ordinal()I

    move-result v1

    const/16 v2, 0x13

    aput v2, v0, v1
    :try_end_27
    .catch Ljava/lang/NoSuchFieldError; {:try_start_27 .. :try_end_27} :catch_1

    :goto_28
    :try_start_28
    sget-object v1, Lcom/mstar/android/tvapi/common/vo/TvOsType$EnumInputSource;->E_INPUT_SOURCE_YPBPR_MAX:Lcom/mstar/android/tvapi/common/vo/TvOsType$EnumInputSource;

    invoke-virtual {v1}, Lcom/mstar/android/tvapi/common/vo/TvOsType$EnumInputSource;->ordinal()I

    move-result v1

    const/16 v2, 0x14

    aput v2, v0, v1
    :try_end_28
    .catch Ljava/lang/NoSuchFieldError; {:try_start_28 .. :try_end_28} :catch_0

    :goto_29
    sput-object v0, Lcom/konka/kkimplements/tv/mstar/SoundDeskImpl;->$SWITCH_TABLE$com$mstar$android$tvapi$common$vo$TvOsType$EnumInputSource:[I

    goto/16 :goto_0

    :catch_0
    move-exception v1

    goto :goto_29

    :catch_1
    move-exception v1

    goto :goto_28

    :catch_2
    move-exception v1

    goto :goto_27

    :catch_3
    move-exception v1

    goto :goto_26

    :catch_4
    move-exception v1

    goto :goto_25

    :catch_5
    move-exception v1

    goto :goto_24

    :catch_6
    move-exception v1

    goto :goto_23

    :catch_7
    move-exception v1

    goto :goto_22

    :catch_8
    move-exception v1

    goto :goto_21

    :catch_9
    move-exception v1

    goto :goto_20

    :catch_a
    move-exception v1

    goto :goto_1f

    :catch_b
    move-exception v1

    goto/16 :goto_1e

    :catch_c
    move-exception v1

    goto/16 :goto_1d

    :catch_d
    move-exception v1

    goto/16 :goto_1c

    :catch_e
    move-exception v1

    goto/16 :goto_1b

    :catch_f
    move-exception v1

    goto/16 :goto_1a

    :catch_10
    move-exception v1

    goto/16 :goto_19

    :catch_11
    move-exception v1

    goto/16 :goto_18

    :catch_12
    move-exception v1

    goto/16 :goto_17

    :catch_13
    move-exception v1

    goto/16 :goto_16

    :catch_14
    move-exception v1

    goto/16 :goto_15

    :catch_15
    move-exception v1

    goto/16 :goto_14

    :catch_16
    move-exception v1

    goto/16 :goto_13

    :catch_17
    move-exception v1

    goto/16 :goto_12

    :catch_18
    move-exception v1

    goto/16 :goto_11

    :catch_19
    move-exception v1

    goto/16 :goto_10

    :catch_1a
    move-exception v1

    goto/16 :goto_f

    :catch_1b
    move-exception v1

    goto/16 :goto_e

    :catch_1c
    move-exception v1

    goto/16 :goto_d

    :catch_1d
    move-exception v1

    goto/16 :goto_c

    :catch_1e
    move-exception v1

    goto/16 :goto_b

    :catch_1f
    move-exception v1

    goto/16 :goto_a

    :catch_20
    move-exception v1

    goto/16 :goto_9

    :catch_21
    move-exception v1

    goto/16 :goto_8

    :catch_22
    move-exception v1

    goto/16 :goto_7

    :catch_23
    move-exception v1

    goto/16 :goto_6

    :catch_24
    move-exception v1

    goto/16 :goto_5

    :catch_25
    move-exception v1

    goto/16 :goto_4

    :catch_26
    move-exception v1

    goto/16 :goto_3

    :catch_27
    move-exception v1

    goto/16 :goto_2

    :catch_28
    move-exception v1

    goto/16 :goto_1
.end method

.method static constructor <clinit>()V
    .locals 1

    const/4 v0, 0x0

    sput-object v0, Lcom/konka/kkimplements/tv/mstar/SoundDeskImpl;->soundMgrImpl:Lcom/konka/kkimplements/tv/mstar/SoundDeskImpl;

    return-void
.end method

.method private constructor <init>(Landroid/content/Context;)V
    .locals 3
    .param p1    # Landroid/content/Context;

    const/4 v0, 0x0

    invoke-direct {p0}, Lcom/konka/kkimplements/tv/mstar/BaseDeskImpl;-><init>()V

    iput-object v0, p0, Lcom/konka/kkimplements/tv/mstar/SoundDeskImpl;->databaseMgr:Lcom/konka/kkimplements/tv/mstar/DataBaseDeskImpl;

    iput-object v0, p0, Lcom/konka/kkimplements/tv/mstar/SoundDeskImpl;->userSoundSetting:Lcom/konka/kkinterface/tv/DataBaseDesk$MS_USER_SOUND_SETTING;

    iput-object v0, p0, Lcom/konka/kkimplements/tv/mstar/SoundDeskImpl;->com:Lcom/konka/kkinterface/tv/CommonDesk;

    iput-object p1, p0, Lcom/konka/kkimplements/tv/mstar/SoundDeskImpl;->context:Landroid/content/Context;

    invoke-static {p1}, Lcom/konka/kkimplements/tv/mstar/CommonDeskImpl;->getInstance(Landroid/content/Context;)Lcom/konka/kkinterface/tv/CommonDesk;

    move-result-object v0

    iput-object v0, p0, Lcom/konka/kkimplements/tv/mstar/SoundDeskImpl;->com:Lcom/konka/kkinterface/tv/CommonDesk;

    iget-object v0, p0, Lcom/konka/kkimplements/tv/mstar/SoundDeskImpl;->com:Lcom/konka/kkinterface/tv/CommonDesk;

    const-string v1, "TvService"

    const-string v2, "SoundManagerImpl constructor!!"

    invoke-interface {v0, v1, v2}, Lcom/konka/kkinterface/tv/CommonDesk;->printfI(Ljava/lang/String;Ljava/lang/String;)V

    invoke-static {p1}, Lcom/konka/kkimplements/tv/mstar/DataBaseDeskImpl;->getDataBaseMgrInstance(Landroid/content/Context;)Lcom/konka/kkimplements/tv/mstar/DataBaseDeskImpl;

    move-result-object v0

    iput-object v0, p0, Lcom/konka/kkimplements/tv/mstar/SoundDeskImpl;->databaseMgr:Lcom/konka/kkimplements/tv/mstar/DataBaseDeskImpl;

    iget-object v0, p0, Lcom/konka/kkimplements/tv/mstar/SoundDeskImpl;->databaseMgr:Lcom/konka/kkimplements/tv/mstar/DataBaseDeskImpl;

    invoke-virtual {v0}, Lcom/konka/kkimplements/tv/mstar/DataBaseDeskImpl;->getSound()Lcom/konka/kkinterface/tv/DataBaseDesk$MS_USER_SOUND_SETTING;

    move-result-object v0

    iput-object v0, p0, Lcom/konka/kkimplements/tv/mstar/SoundDeskImpl;->userSoundSetting:Lcom/konka/kkinterface/tv/DataBaseDesk$MS_USER_SOUND_SETTING;

    return-void
.end method

.method public static getSoundMgrInstance(Landroid/content/Context;)Lcom/konka/kkimplements/tv/mstar/SoundDeskImpl;
    .locals 1
    .param p0    # Landroid/content/Context;

    sget-object v0, Lcom/konka/kkimplements/tv/mstar/SoundDeskImpl;->soundMgrImpl:Lcom/konka/kkimplements/tv/mstar/SoundDeskImpl;

    if-nez v0, :cond_0

    new-instance v0, Lcom/konka/kkimplements/tv/mstar/SoundDeskImpl;

    invoke-direct {v0, p0}, Lcom/konka/kkimplements/tv/mstar/SoundDeskImpl;-><init>(Landroid/content/Context;)V

    sput-object v0, Lcom/konka/kkimplements/tv/mstar/SoundDeskImpl;->soundMgrImpl:Lcom/konka/kkimplements/tv/mstar/SoundDeskImpl;

    :cond_0
    sget-object v0, Lcom/konka/kkimplements/tv/mstar/SoundDeskImpl;->soundMgrImpl:Lcom/konka/kkimplements/tv/mstar/SoundDeskImpl;

    return-object v0
.end method


# virtual methods
.method public getAVCMode()Z
    .locals 1

    iget-object v0, p0, Lcom/konka/kkimplements/tv/mstar/SoundDeskImpl;->userSoundSetting:Lcom/konka/kkinterface/tv/DataBaseDesk$MS_USER_SOUND_SETTING;

    iget-boolean v0, v0, Lcom/konka/kkinterface/tv/DataBaseDesk$MS_USER_SOUND_SETTING;->bEnableAVC:Z

    return v0
.end method

.method public getAtvMtsMode()Lcom/mstar/android/tvapi/common/vo/EnumAtvAudioModeType;
    .locals 3

    sget-object v1, Lcom/mstar/android/tvapi/common/vo/EnumAtvAudioModeType;->E_ATV_AUDIOMODE_INVALID:Lcom/mstar/android/tvapi/common/vo/EnumAtvAudioModeType;

    :try_start_0
    invoke-static {}, Lcom/mstar/android/tvapi/common/TvManager;->getInstance()Lcom/mstar/android/tvapi/common/TvManager;

    move-result-object v2

    if-eqz v2, :cond_0

    invoke-static {}, Lcom/mstar/android/tvapi/common/TvManager;->getInstance()Lcom/mstar/android/tvapi/common/TvManager;

    move-result-object v2

    invoke-virtual {v2}, Lcom/mstar/android/tvapi/common/TvManager;->getAudioManager()Lcom/mstar/android/tvapi/common/AudioManager;

    move-result-object v2

    invoke-virtual {v2}, Lcom/mstar/android/tvapi/common/AudioManager;->getAtvMtsMode()Lcom/mstar/android/tvapi/common/vo/EnumAtvAudioModeType;
    :try_end_0
    .catch Lcom/mstar/android/tvapi/common/exception/TvCommonException; {:try_start_0 .. :try_end_0} :catch_0

    move-result-object v1

    :cond_0
    :goto_0
    return-object v1

    :catch_0
    move-exception v0

    invoke-virtual {v0}, Lcom/mstar/android/tvapi/common/exception/TvCommonException;->printStackTrace()V

    goto :goto_0
.end method

.method public getBalance()S
    .locals 4

    iget-object v0, p0, Lcom/konka/kkimplements/tv/mstar/SoundDeskImpl;->com:Lcom/konka/kkinterface/tv/CommonDesk;

    const-string v1, "TvSoundServiceImpl"

    new-instance v2, Ljava/lang/StringBuilder;

    const-string v3, "Get Balance is"

    invoke-direct {v2, v3}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    iget-object v3, p0, Lcom/konka/kkimplements/tv/mstar/SoundDeskImpl;->userSoundSetting:Lcom/konka/kkinterface/tv/DataBaseDesk$MS_USER_SOUND_SETTING;

    iget-short v3, v3, Lcom/konka/kkinterface/tv/DataBaseDesk$MS_USER_SOUND_SETTING;->Balance:S

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-interface {v0, v1, v2}, Lcom/konka/kkinterface/tv/CommonDesk;->printfI(Ljava/lang/String;Ljava/lang/String;)V

    iget-object v0, p0, Lcom/konka/kkimplements/tv/mstar/SoundDeskImpl;->userSoundSetting:Lcom/konka/kkinterface/tv/DataBaseDesk$MS_USER_SOUND_SETTING;

    iget-short v0, v0, Lcom/konka/kkinterface/tv/DataBaseDesk$MS_USER_SOUND_SETTING;->Balance:S

    return v0
.end method

.method public getBass()S
    .locals 2

    iget-object v0, p0, Lcom/konka/kkimplements/tv/mstar/SoundDeskImpl;->databaseMgr:Lcom/konka/kkimplements/tv/mstar/DataBaseDeskImpl;

    iget-object v1, p0, Lcom/konka/kkimplements/tv/mstar/SoundDeskImpl;->userSoundSetting:Lcom/konka/kkinterface/tv/DataBaseDesk$MS_USER_SOUND_SETTING;

    iget-object v1, v1, Lcom/konka/kkinterface/tv/DataBaseDesk$MS_USER_SOUND_SETTING;->SoundMode:Lcom/konka/kkinterface/tv/DataBaseDesk$EN_SOUND_MODE;

    invoke-virtual {v0, v1}, Lcom/konka/kkimplements/tv/mstar/DataBaseDeskImpl;->getSoundMode(Lcom/konka/kkinterface/tv/DataBaseDesk$EN_SOUND_MODE;)Lcom/konka/kkinterface/tv/DataBaseDesk$SoundModeSeting;

    move-result-object v0

    iget-short v0, v0, Lcom/konka/kkinterface/tv/DataBaseDesk$SoundModeSeting;->Bass:S

    return v0
.end method

.method public getEqBand10k()S
    .locals 2

    iget-object v0, p0, Lcom/konka/kkimplements/tv/mstar/SoundDeskImpl;->databaseMgr:Lcom/konka/kkimplements/tv/mstar/DataBaseDeskImpl;

    iget-object v1, p0, Lcom/konka/kkimplements/tv/mstar/SoundDeskImpl;->userSoundSetting:Lcom/konka/kkinterface/tv/DataBaseDesk$MS_USER_SOUND_SETTING;

    iget-object v1, v1, Lcom/konka/kkinterface/tv/DataBaseDesk$MS_USER_SOUND_SETTING;->SoundMode:Lcom/konka/kkinterface/tv/DataBaseDesk$EN_SOUND_MODE;

    invoke-virtual {v0, v1}, Lcom/konka/kkimplements/tv/mstar/DataBaseDeskImpl;->getSoundMode(Lcom/konka/kkinterface/tv/DataBaseDesk$EN_SOUND_MODE;)Lcom/konka/kkinterface/tv/DataBaseDesk$SoundModeSeting;

    move-result-object v0

    iget-short v0, v0, Lcom/konka/kkinterface/tv/DataBaseDesk$SoundModeSeting;->EqBand5:S

    return v0
.end method

.method public getEqBand120()S
    .locals 2

    iget-object v0, p0, Lcom/konka/kkimplements/tv/mstar/SoundDeskImpl;->databaseMgr:Lcom/konka/kkimplements/tv/mstar/DataBaseDeskImpl;

    iget-object v1, p0, Lcom/konka/kkimplements/tv/mstar/SoundDeskImpl;->userSoundSetting:Lcom/konka/kkinterface/tv/DataBaseDesk$MS_USER_SOUND_SETTING;

    iget-object v1, v1, Lcom/konka/kkinterface/tv/DataBaseDesk$MS_USER_SOUND_SETTING;->SoundMode:Lcom/konka/kkinterface/tv/DataBaseDesk$EN_SOUND_MODE;

    invoke-virtual {v0, v1}, Lcom/konka/kkimplements/tv/mstar/DataBaseDeskImpl;->getSoundMode(Lcom/konka/kkinterface/tv/DataBaseDesk$EN_SOUND_MODE;)Lcom/konka/kkinterface/tv/DataBaseDesk$SoundModeSeting;

    move-result-object v0

    iget-short v0, v0, Lcom/konka/kkinterface/tv/DataBaseDesk$SoundModeSeting;->EqBand1:S

    return v0
.end method

.method public getEqBand1500()S
    .locals 2

    iget-object v0, p0, Lcom/konka/kkimplements/tv/mstar/SoundDeskImpl;->databaseMgr:Lcom/konka/kkimplements/tv/mstar/DataBaseDeskImpl;

    iget-object v1, p0, Lcom/konka/kkimplements/tv/mstar/SoundDeskImpl;->userSoundSetting:Lcom/konka/kkinterface/tv/DataBaseDesk$MS_USER_SOUND_SETTING;

    iget-object v1, v1, Lcom/konka/kkinterface/tv/DataBaseDesk$MS_USER_SOUND_SETTING;->SoundMode:Lcom/konka/kkinterface/tv/DataBaseDesk$EN_SOUND_MODE;

    invoke-virtual {v0, v1}, Lcom/konka/kkimplements/tv/mstar/DataBaseDeskImpl;->getSoundMode(Lcom/konka/kkinterface/tv/DataBaseDesk$EN_SOUND_MODE;)Lcom/konka/kkinterface/tv/DataBaseDesk$SoundModeSeting;

    move-result-object v0

    iget-short v0, v0, Lcom/konka/kkinterface/tv/DataBaseDesk$SoundModeSeting;->EqBand3:S

    return v0
.end method

.method public getEqBand500()S
    .locals 2

    iget-object v0, p0, Lcom/konka/kkimplements/tv/mstar/SoundDeskImpl;->databaseMgr:Lcom/konka/kkimplements/tv/mstar/DataBaseDeskImpl;

    iget-object v1, p0, Lcom/konka/kkimplements/tv/mstar/SoundDeskImpl;->userSoundSetting:Lcom/konka/kkinterface/tv/DataBaseDesk$MS_USER_SOUND_SETTING;

    iget-object v1, v1, Lcom/konka/kkinterface/tv/DataBaseDesk$MS_USER_SOUND_SETTING;->SoundMode:Lcom/konka/kkinterface/tv/DataBaseDesk$EN_SOUND_MODE;

    invoke-virtual {v0, v1}, Lcom/konka/kkimplements/tv/mstar/DataBaseDeskImpl;->getSoundMode(Lcom/konka/kkinterface/tv/DataBaseDesk$EN_SOUND_MODE;)Lcom/konka/kkinterface/tv/DataBaseDesk$SoundModeSeting;

    move-result-object v0

    iget-short v0, v0, Lcom/konka/kkinterface/tv/DataBaseDesk$SoundModeSeting;->EqBand2:S

    return v0
.end method

.method public getEqBand5k()S
    .locals 2

    iget-object v0, p0, Lcom/konka/kkimplements/tv/mstar/SoundDeskImpl;->databaseMgr:Lcom/konka/kkimplements/tv/mstar/DataBaseDeskImpl;

    iget-object v1, p0, Lcom/konka/kkimplements/tv/mstar/SoundDeskImpl;->userSoundSetting:Lcom/konka/kkinterface/tv/DataBaseDesk$MS_USER_SOUND_SETTING;

    iget-object v1, v1, Lcom/konka/kkinterface/tv/DataBaseDesk$MS_USER_SOUND_SETTING;->SoundMode:Lcom/konka/kkinterface/tv/DataBaseDesk$EN_SOUND_MODE;

    invoke-virtual {v0, v1}, Lcom/konka/kkimplements/tv/mstar/DataBaseDeskImpl;->getSoundMode(Lcom/konka/kkinterface/tv/DataBaseDesk$EN_SOUND_MODE;)Lcom/konka/kkinterface/tv/DataBaseDesk$SoundModeSeting;

    move-result-object v0

    iget-short v0, v0, Lcom/konka/kkinterface/tv/DataBaseDesk$SoundModeSeting;->EqBand4:S

    return v0
.end method

.method public getHdmiAudioSource(Lcom/mstar/android/tvapi/common/vo/TvOsType$EnumInputSource;)Lcom/konka/kkinterface/tv/DataBaseDesk$HdmiAudioSource;
    .locals 3
    .param p1    # Lcom/mstar/android/tvapi/common/vo/TvOsType$EnumInputSource;

    invoke-static {}, Lcom/konka/kkimplements/tv/mstar/SoundDeskImpl;->$SWITCH_TABLE$com$mstar$android$tvapi$common$vo$TvOsType$EnumInputSource()[I

    move-result-object v1

    invoke-virtual {p1}, Lcom/mstar/android/tvapi/common/vo/TvOsType$EnumInputSource;->ordinal()I

    move-result v2

    aget v1, v1, v2

    packed-switch v1, :pswitch_data_0

    sget-object v0, Lcom/konka/kkinterface/tv/DataBaseDesk$HdmiAudioSource;->AUDIO_SOURCE_HDMI:Lcom/konka/kkinterface/tv/DataBaseDesk$HdmiAudioSource;

    :goto_0
    return-object v0

    :pswitch_0
    iget-object v1, p0, Lcom/konka/kkimplements/tv/mstar/SoundDeskImpl;->userSoundSetting:Lcom/konka/kkinterface/tv/DataBaseDesk$MS_USER_SOUND_SETTING;

    iget-object v0, v1, Lcom/konka/kkinterface/tv/DataBaseDesk$MS_USER_SOUND_SETTING;->hdmi1AudioSource:Lcom/konka/kkinterface/tv/DataBaseDesk$HdmiAudioSource;

    goto :goto_0

    :pswitch_1
    iget-object v1, p0, Lcom/konka/kkimplements/tv/mstar/SoundDeskImpl;->userSoundSetting:Lcom/konka/kkinterface/tv/DataBaseDesk$MS_USER_SOUND_SETTING;

    iget-object v0, v1, Lcom/konka/kkinterface/tv/DataBaseDesk$MS_USER_SOUND_SETTING;->hdmi2AudioSource:Lcom/konka/kkinterface/tv/DataBaseDesk$HdmiAudioSource;

    goto :goto_0

    :pswitch_2
    iget-object v1, p0, Lcom/konka/kkimplements/tv/mstar/SoundDeskImpl;->userSoundSetting:Lcom/konka/kkinterface/tv/DataBaseDesk$MS_USER_SOUND_SETTING;

    iget-object v0, v1, Lcom/konka/kkinterface/tv/DataBaseDesk$MS_USER_SOUND_SETTING;->hdmi3AudioSource:Lcom/konka/kkinterface/tv/DataBaseDesk$HdmiAudioSource;

    goto :goto_0

    :pswitch_3
    iget-object v1, p0, Lcom/konka/kkimplements/tv/mstar/SoundDeskImpl;->userSoundSetting:Lcom/konka/kkinterface/tv/DataBaseDesk$MS_USER_SOUND_SETTING;

    iget-object v0, v1, Lcom/konka/kkinterface/tv/DataBaseDesk$MS_USER_SOUND_SETTING;->hdmi4AudioSource:Lcom/konka/kkinterface/tv/DataBaseDesk$HdmiAudioSource;

    goto :goto_0

    :pswitch_data_0
    .packed-switch 0x18
        :pswitch_0
        :pswitch_1
        :pswitch_2
        :pswitch_3
    .end packed-switch
.end method

.method public getMuteFlag()Z
    .locals 3

    :try_start_0
    invoke-static {}, Lcom/mstar/android/tvapi/common/TvManager;->getInstance()Lcom/mstar/android/tvapi/common/TvManager;

    move-result-object v1

    if-eqz v1, :cond_0

    invoke-static {}, Lcom/mstar/android/tvapi/common/TvManager;->getInstance()Lcom/mstar/android/tvapi/common/TvManager;

    move-result-object v1

    invoke-virtual {v1}, Lcom/mstar/android/tvapi/common/TvManager;->getAudioManager()Lcom/mstar/android/tvapi/common/AudioManager;

    move-result-object v1

    sget-object v2, Lcom/mstar/android/tvapi/common/vo/EnumMuteStatusType;->MUTE_STATUS_BBYUSERAUDIOMUTE:Lcom/mstar/android/tvapi/common/vo/EnumMuteStatusType;

    invoke-virtual {v1, v2}, Lcom/mstar/android/tvapi/common/AudioManager;->isMuteEnabled(Lcom/mstar/android/tvapi/common/vo/EnumMuteStatusType;)Z
    :try_end_0
    .catch Lcom/mstar/android/tvapi/common/exception/TvCommonException; {:try_start_0 .. :try_end_0} :catch_0

    move-result v1

    :goto_0
    return v1

    :catch_0
    move-exception v0

    invoke-virtual {v0}, Lcom/mstar/android/tvapi/common/exception/TvCommonException;->printStackTrace()V

    :cond_0
    const/4 v1, 0x0

    goto :goto_0
.end method

.method public getSoundMode()Lcom/konka/kkinterface/tv/DataBaseDesk$EN_SOUND_MODE;
    .locals 3

    iget-object v0, p0, Lcom/konka/kkimplements/tv/mstar/SoundDeskImpl;->userSoundSetting:Lcom/konka/kkinterface/tv/DataBaseDesk$MS_USER_SOUND_SETTING;

    invoke-static {}, Lcom/konka/kkinterface/tv/DataBaseDesk$EN_SOUND_MODE;->values()[Lcom/konka/kkinterface/tv/DataBaseDesk$EN_SOUND_MODE;

    move-result-object v1

    iget-object v2, p0, Lcom/konka/kkimplements/tv/mstar/SoundDeskImpl;->databaseMgr:Lcom/konka/kkimplements/tv/mstar/DataBaseDeskImpl;

    invoke-virtual {v2}, Lcom/konka/kkimplements/tv/mstar/DataBaseDeskImpl;->querySoundMode()I

    move-result v2

    aget-object v1, v1, v2

    iput-object v1, v0, Lcom/konka/kkinterface/tv/DataBaseDesk$MS_USER_SOUND_SETTING;->SoundMode:Lcom/konka/kkinterface/tv/DataBaseDesk$EN_SOUND_MODE;

    iget-object v0, p0, Lcom/konka/kkimplements/tv/mstar/SoundDeskImpl;->userSoundSetting:Lcom/konka/kkinterface/tv/DataBaseDesk$MS_USER_SOUND_SETTING;

    iget-object v0, v0, Lcom/konka/kkinterface/tv/DataBaseDesk$MS_USER_SOUND_SETTING;->SoundMode:Lcom/konka/kkinterface/tv/DataBaseDesk$EN_SOUND_MODE;

    return-object v0
.end method

.method public getSoundSpeakerDelay()I
    .locals 3

    const/4 v0, 0x0

    invoke-static {}, Lcom/mstar/android/tvapi/common/TvManager;->getInstance()Lcom/mstar/android/tvapi/common/TvManager;

    move-result-object v1

    if-eqz v1, :cond_0

    invoke-static {}, Lcom/mstar/android/tvapi/common/TvManager;->getInstance()Lcom/mstar/android/tvapi/common/TvManager;

    move-result-object v1

    invoke-virtual {v1}, Lcom/mstar/android/tvapi/common/TvManager;->getAudioManager()Lcom/mstar/android/tvapi/common/AudioManager;

    move-result-object v1

    sget-object v2, Lcom/mstar/android/tvapi/common/vo/EnumSoundSetParamType;->E_SOUND_SET_PARAM_AUDIODELAY_:Lcom/mstar/android/tvapi/common/vo/EnumSoundSetParamType;

    invoke-virtual {v1, v2, v0}, Lcom/mstar/android/tvapi/common/AudioManager;->getSoundParameter(Lcom/mstar/android/tvapi/common/vo/EnumSoundSetParamType;I)I

    :cond_0
    return v0
.end method

.method public getSpdifOutMode()Lcom/konka/kkinterface/tv/DataBaseDesk$EN_SPDIF_MODE;
    .locals 4

    sget-object v0, Lcom/konka/kkinterface/tv/DataBaseDesk$EN_SPDIF_MODE;->PDIF_MODE_OFF:Lcom/konka/kkinterface/tv/DataBaseDesk$EN_SPDIF_MODE;

    iget-object v2, p0, Lcom/konka/kkimplements/tv/mstar/SoundDeskImpl;->databaseMgr:Lcom/konka/kkimplements/tv/mstar/DataBaseDeskImpl;

    invoke-virtual {v2}, Lcom/konka/kkimplements/tv/mstar/DataBaseDeskImpl;->getSpdifMode()Lcom/konka/kkinterface/tv/DataBaseDesk$SPDIF_TYPE;

    move-result-object v1

    invoke-static {}, Lcom/konka/kkimplements/tv/mstar/SoundDeskImpl;->$SWITCH_TABLE$com$konka$kkinterface$tv$DataBaseDesk$SPDIF_TYPE()[I

    move-result-object v2

    invoke-virtual {v1}, Lcom/konka/kkinterface/tv/DataBaseDesk$SPDIF_TYPE;->ordinal()I

    move-result v3

    aget v2, v2, v3

    packed-switch v2, :pswitch_data_0

    :goto_0
    return-object v0

    :pswitch_0
    sget-object v0, Lcom/konka/kkinterface/tv/DataBaseDesk$EN_SPDIF_MODE;->PDIF_MODE_PCM:Lcom/konka/kkinterface/tv/DataBaseDesk$EN_SPDIF_MODE;

    goto :goto_0

    :pswitch_1
    sget-object v0, Lcom/konka/kkinterface/tv/DataBaseDesk$EN_SPDIF_MODE;->PDIF_MODE_RAW:Lcom/konka/kkinterface/tv/DataBaseDesk$EN_SPDIF_MODE;

    goto :goto_0

    :pswitch_2
    sget-object v0, Lcom/konka/kkinterface/tv/DataBaseDesk$EN_SPDIF_MODE;->PDIF_MODE_OFF:Lcom/konka/kkinterface/tv/DataBaseDesk$EN_SPDIF_MODE;

    goto :goto_0

    nop

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
        :pswitch_2
        :pswitch_1
    .end packed-switch
.end method

.method public getSurroundMode()Lcom/konka/kkinterface/tv/DataBaseDesk$EN_SURROUND_MODE;
    .locals 1

    iget-object v0, p0, Lcom/konka/kkimplements/tv/mstar/SoundDeskImpl;->userSoundSetting:Lcom/konka/kkinterface/tv/DataBaseDesk$MS_USER_SOUND_SETTING;

    iget-object v0, v0, Lcom/konka/kkinterface/tv/DataBaseDesk$MS_USER_SOUND_SETTING;->SurroundMode:Lcom/konka/kkinterface/tv/DataBaseDesk$EN_SURROUND_MODE;

    return-object v0
.end method

.method public getTreble()S
    .locals 2

    iget-object v0, p0, Lcom/konka/kkimplements/tv/mstar/SoundDeskImpl;->databaseMgr:Lcom/konka/kkimplements/tv/mstar/DataBaseDeskImpl;

    iget-object v1, p0, Lcom/konka/kkimplements/tv/mstar/SoundDeskImpl;->userSoundSetting:Lcom/konka/kkinterface/tv/DataBaseDesk$MS_USER_SOUND_SETTING;

    iget-object v1, v1, Lcom/konka/kkinterface/tv/DataBaseDesk$MS_USER_SOUND_SETTING;->SoundMode:Lcom/konka/kkinterface/tv/DataBaseDesk$EN_SOUND_MODE;

    invoke-virtual {v0, v1}, Lcom/konka/kkimplements/tv/mstar/DataBaseDeskImpl;->getSoundMode(Lcom/konka/kkinterface/tv/DataBaseDesk$EN_SOUND_MODE;)Lcom/konka/kkinterface/tv/DataBaseDesk$SoundModeSeting;

    move-result-object v0

    iget-short v0, v0, Lcom/konka/kkinterface/tv/DataBaseDesk$SoundModeSeting;->Treble:S

    return v0
.end method

.method public getVolume()S
    .locals 3

    iget-object v0, p0, Lcom/konka/kkimplements/tv/mstar/SoundDeskImpl;->databaseMgr:Lcom/konka/kkimplements/tv/mstar/DataBaseDeskImpl;

    invoke-virtual {v0}, Lcom/konka/kkimplements/tv/mstar/DataBaseDeskImpl;->querySoundSetting()Lcom/konka/kkinterface/tv/DataBaseDesk$MS_USER_SOUND_SETTING;

    move-result-object v0

    iput-object v0, p0, Lcom/konka/kkimplements/tv/mstar/SoundDeskImpl;->userSoundSetting:Lcom/konka/kkinterface/tv/DataBaseDesk$MS_USER_SOUND_SETTING;

    const-string v0, "TvSoundServiceImpl"

    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "getVolume is"

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    iget-object v2, p0, Lcom/konka/kkimplements/tv/mstar/SoundDeskImpl;->userSoundSetting:Lcom/konka/kkinterface/tv/DataBaseDesk$MS_USER_SOUND_SETTING;

    iget-short v2, v2, Lcom/konka/kkinterface/tv/DataBaseDesk$MS_USER_SOUND_SETTING;->Volume:S

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    iget-object v0, p0, Lcom/konka/kkimplements/tv/mstar/SoundDeskImpl;->userSoundSetting:Lcom/konka/kkinterface/tv/DataBaseDesk$MS_USER_SOUND_SETTING;

    iget-short v0, v0, Lcom/konka/kkinterface/tv/DataBaseDesk$MS_USER_SOUND_SETTING;->Volume:S

    return v0
.end method

.method public setAVCMode(Z)Z
    .locals 5
    .param p1    # Z

    iget-object v1, p0, Lcom/konka/kkimplements/tv/mstar/SoundDeskImpl;->databaseMgr:Lcom/konka/kkimplements/tv/mstar/DataBaseDeskImpl;

    invoke-virtual {v1}, Lcom/konka/kkimplements/tv/mstar/DataBaseDeskImpl;->querySoundSetting()Lcom/konka/kkinterface/tv/DataBaseDesk$MS_USER_SOUND_SETTING;

    iget-object v1, p0, Lcom/konka/kkimplements/tv/mstar/SoundDeskImpl;->databaseMgr:Lcom/konka/kkimplements/tv/mstar/DataBaseDeskImpl;

    invoke-virtual {v1}, Lcom/konka/kkimplements/tv/mstar/DataBaseDeskImpl;->getSound()Lcom/konka/kkinterface/tv/DataBaseDesk$MS_USER_SOUND_SETTING;

    move-result-object v1

    iput-object v1, p0, Lcom/konka/kkimplements/tv/mstar/SoundDeskImpl;->userSoundSetting:Lcom/konka/kkinterface/tv/DataBaseDesk$MS_USER_SOUND_SETTING;

    iget-object v1, p0, Lcom/konka/kkimplements/tv/mstar/SoundDeskImpl;->userSoundSetting:Lcom/konka/kkinterface/tv/DataBaseDesk$MS_USER_SOUND_SETTING;

    iput-boolean p1, v1, Lcom/konka/kkinterface/tv/DataBaseDesk$MS_USER_SOUND_SETTING;->bEnableAVC:Z

    iget-object v1, p0, Lcom/konka/kkimplements/tv/mstar/SoundDeskImpl;->com:Lcom/konka/kkinterface/tv/CommonDesk;

    const-string v2, "TvSoundServiceImpl"

    new-instance v3, Ljava/lang/StringBuilder;

    const-string v4, "bEnableAVC is"

    invoke-direct {v3, v4}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    iget-object v4, p0, Lcom/konka/kkimplements/tv/mstar/SoundDeskImpl;->userSoundSetting:Lcom/konka/kkinterface/tv/DataBaseDesk$MS_USER_SOUND_SETTING;

    iget-boolean v4, v4, Lcom/konka/kkinterface/tv/DataBaseDesk$MS_USER_SOUND_SETTING;->bEnableAVC:Z

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-interface {v1, v2, v3}, Lcom/konka/kkinterface/tv/CommonDesk;->printfI(Ljava/lang/String;Ljava/lang/String;)V

    iget-object v1, p0, Lcom/konka/kkimplements/tv/mstar/SoundDeskImpl;->databaseMgr:Lcom/konka/kkimplements/tv/mstar/DataBaseDeskImpl;

    iget-object v2, p0, Lcom/konka/kkimplements/tv/mstar/SoundDeskImpl;->userSoundSetting:Lcom/konka/kkinterface/tv/DataBaseDesk$MS_USER_SOUND_SETTING;

    invoke-virtual {v1, v2}, Lcom/konka/kkimplements/tv/mstar/DataBaseDeskImpl;->updateSoundSetting(Lcom/konka/kkinterface/tv/DataBaseDesk$MS_USER_SOUND_SETTING;)V

    :try_start_0
    invoke-static {}, Lcom/mstar/android/tvapi/common/TvManager;->getInstance()Lcom/mstar/android/tvapi/common/TvManager;

    move-result-object v1

    if-eqz v1, :cond_0

    invoke-static {}, Lcom/mstar/android/tvapi/common/TvManager;->getInstance()Lcom/mstar/android/tvapi/common/TvManager;

    move-result-object v1

    invoke-virtual {v1}, Lcom/mstar/android/tvapi/common/TvManager;->getAudioManager()Lcom/mstar/android/tvapi/common/AudioManager;

    move-result-object v1

    sget-object v2, Lcom/mstar/android/tvapi/common/vo/EnumSoundEffectType;->E_AVC:Lcom/mstar/android/tvapi/common/vo/EnumSoundEffectType;

    invoke-virtual {v1, v2, p1}, Lcom/mstar/android/tvapi/common/AudioManager;->enableBasicSoundEffect(Lcom/mstar/android/tvapi/common/vo/EnumSoundEffectType;Z)Lcom/mstar/android/tvapi/common/vo/EnumAudioReturn;
    :try_end_0
    .catch Lcom/mstar/android/tvapi/common/exception/TvCommonException; {:try_start_0 .. :try_end_0} :catch_0

    :cond_0
    :goto_0
    const/4 v1, 0x1

    return v1

    :catch_0
    move-exception v0

    iget-object v1, p0, Lcom/konka/kkimplements/tv/mstar/SoundDeskImpl;->com:Lcom/konka/kkinterface/tv/CommonDesk;

    const-string v2, "TvSoundServiceImpl"

    const-string v3, "Set Balance Exception"

    invoke-interface {v1, v2, v3}, Lcom/konka/kkinterface/tv/CommonDesk;->printfE(Ljava/lang/String;Ljava/lang/String;)V

    invoke-virtual {v0}, Lcom/mstar/android/tvapi/common/exception/TvCommonException;->printStackTrace()V

    goto :goto_0
.end method

.method public setAtvMtsMode(Lcom/mstar/android/tvapi/common/vo/EnumAtvAudioModeType;)Lcom/mstar/android/tvapi/common/vo/EnumAudioReturn;
    .locals 3
    .param p1    # Lcom/mstar/android/tvapi/common/vo/EnumAtvAudioModeType;

    sget-object v1, Lcom/mstar/android/tvapi/common/vo/EnumAudioReturn;->E_RETURN_NOTOK:Lcom/mstar/android/tvapi/common/vo/EnumAudioReturn;

    :try_start_0
    invoke-static {}, Lcom/mstar/android/tvapi/common/TvManager;->getInstance()Lcom/mstar/android/tvapi/common/TvManager;

    move-result-object v2

    if-eqz v2, :cond_0

    invoke-static {}, Lcom/mstar/android/tvapi/common/TvManager;->getInstance()Lcom/mstar/android/tvapi/common/TvManager;

    move-result-object v2

    invoke-virtual {v2}, Lcom/mstar/android/tvapi/common/TvManager;->getAudioManager()Lcom/mstar/android/tvapi/common/AudioManager;

    move-result-object v2

    invoke-virtual {v2, p1}, Lcom/mstar/android/tvapi/common/AudioManager;->setAtvMtsMode(Lcom/mstar/android/tvapi/common/vo/EnumAtvAudioModeType;)Lcom/mstar/android/tvapi/common/vo/EnumAudioReturn;
    :try_end_0
    .catch Lcom/mstar/android/tvapi/common/exception/TvCommonException; {:try_start_0 .. :try_end_0} :catch_0

    move-result-object v1

    :cond_0
    :goto_0
    return-object v1

    :catch_0
    move-exception v0

    invoke-virtual {v0}, Lcom/mstar/android/tvapi/common/exception/TvCommonException;->printStackTrace()V

    goto :goto_0
.end method

.method public setAudioInputSource(Lcom/mstar/android/tvapi/common/vo/TvOsType$EnumInputSource;)Z
    .locals 4
    .param p1    # Lcom/mstar/android/tvapi/common/vo/TvOsType$EnumInputSource;

    const-string v1, "setAudioInputSource"

    new-instance v2, Ljava/lang/StringBuilder;

    const-string v3, "source is"

    invoke-direct {v2, v3}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v2, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    :try_start_0
    invoke-static {}, Lcom/mstar/android/tvapi/common/TvManager;->getInstance()Lcom/mstar/android/tvapi/common/TvManager;

    move-result-object v1

    if-eqz v1, :cond_0

    invoke-static {}, Lcom/mstar/android/tvapi/common/TvManager;->getInstance()Lcom/mstar/android/tvapi/common/TvManager;

    move-result-object v1

    invoke-virtual {v1}, Lcom/mstar/android/tvapi/common/TvManager;->getAudioManager()Lcom/mstar/android/tvapi/common/AudioManager;

    move-result-object v1

    invoke-virtual {v1, p1}, Lcom/mstar/android/tvapi/common/AudioManager;->setInputSource(Lcom/mstar/android/tvapi/common/vo/TvOsType$EnumInputSource;)V
    :try_end_0
    .catch Lcom/mstar/android/tvapi/common/exception/TvCommonException; {:try_start_0 .. :try_end_0} :catch_0

    :cond_0
    :goto_0
    const/4 v1, 0x1

    return v1

    :catch_0
    move-exception v0

    invoke-virtual {v0}, Lcom/mstar/android/tvapi/common/exception/TvCommonException;->printStackTrace()V

    goto :goto_0
.end method

.method public setBalance(S)Z
    .locals 6
    .param p1    # S

    iget-object v2, p0, Lcom/konka/kkimplements/tv/mstar/SoundDeskImpl;->databaseMgr:Lcom/konka/kkimplements/tv/mstar/DataBaseDeskImpl;

    invoke-virtual {v2}, Lcom/konka/kkimplements/tv/mstar/DataBaseDeskImpl;->querySoundSetting()Lcom/konka/kkinterface/tv/DataBaseDesk$MS_USER_SOUND_SETTING;

    iget-object v2, p0, Lcom/konka/kkimplements/tv/mstar/SoundDeskImpl;->databaseMgr:Lcom/konka/kkimplements/tv/mstar/DataBaseDeskImpl;

    invoke-virtual {v2}, Lcom/konka/kkimplements/tv/mstar/DataBaseDeskImpl;->getSound()Lcom/konka/kkinterface/tv/DataBaseDesk$MS_USER_SOUND_SETTING;

    move-result-object v2

    iput-object v2, p0, Lcom/konka/kkimplements/tv/mstar/SoundDeskImpl;->userSoundSetting:Lcom/konka/kkinterface/tv/DataBaseDesk$MS_USER_SOUND_SETTING;

    const/4 v0, 0x0

    new-instance v0, Lcom/mstar/android/tvapi/common/vo/DtvSoundEffect;

    invoke-direct {v0}, Lcom/mstar/android/tvapi/common/vo/DtvSoundEffect;-><init>()V

    iget-object v2, p0, Lcom/konka/kkimplements/tv/mstar/SoundDeskImpl;->userSoundSetting:Lcom/konka/kkinterface/tv/DataBaseDesk$MS_USER_SOUND_SETTING;

    iput-short p1, v2, Lcom/konka/kkinterface/tv/DataBaseDesk$MS_USER_SOUND_SETTING;->Balance:S

    iget-object v2, p0, Lcom/konka/kkimplements/tv/mstar/SoundDeskImpl;->com:Lcom/konka/kkinterface/tv/CommonDesk;

    const-string v3, "TvSoundServiceImpl"

    new-instance v4, Ljava/lang/StringBuilder;

    const-string v5, "Set Balance is"

    invoke-direct {v4, v5}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    iget-object v5, p0, Lcom/konka/kkimplements/tv/mstar/SoundDeskImpl;->userSoundSetting:Lcom/konka/kkinterface/tv/DataBaseDesk$MS_USER_SOUND_SETTING;

    iget-short v5, v5, Lcom/konka/kkinterface/tv/DataBaseDesk$MS_USER_SOUND_SETTING;->Balance:S

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-interface {v2, v3, v4}, Lcom/konka/kkinterface/tv/CommonDesk;->printfI(Ljava/lang/String;Ljava/lang/String;)V

    :try_start_0
    iput p1, v0, Lcom/mstar/android/tvapi/common/vo/DtvSoundEffect;->balance:I

    invoke-static {}, Lcom/mstar/android/tvapi/common/TvManager;->getInstance()Lcom/mstar/android/tvapi/common/TvManager;

    move-result-object v2

    if-eqz v2, :cond_0

    invoke-static {}, Lcom/mstar/android/tvapi/common/TvManager;->getInstance()Lcom/mstar/android/tvapi/common/TvManager;

    move-result-object v2

    invoke-virtual {v2}, Lcom/mstar/android/tvapi/common/TvManager;->getAudioManager()Lcom/mstar/android/tvapi/common/AudioManager;

    move-result-object v2

    sget-object v3, Lcom/mstar/android/tvapi/common/vo/EnumSoundEffectType;->E_BALANCE:Lcom/mstar/android/tvapi/common/vo/EnumSoundEffectType;

    invoke-virtual {v2, v3, v0}, Lcom/mstar/android/tvapi/common/AudioManager;->setBasicSoundEffect(Lcom/mstar/android/tvapi/common/vo/EnumSoundEffectType;Lcom/mstar/android/tvapi/common/vo/DtvSoundEffect;)Lcom/mstar/android/tvapi/common/vo/EnumAudioReturn;
    :try_end_0
    .catch Lcom/mstar/android/tvapi/common/exception/TvCommonException; {:try_start_0 .. :try_end_0} :catch_0

    :cond_0
    :goto_0
    iget-object v2, p0, Lcom/konka/kkimplements/tv/mstar/SoundDeskImpl;->databaseMgr:Lcom/konka/kkimplements/tv/mstar/DataBaseDeskImpl;

    iget-object v3, p0, Lcom/konka/kkimplements/tv/mstar/SoundDeskImpl;->userSoundSetting:Lcom/konka/kkinterface/tv/DataBaseDesk$MS_USER_SOUND_SETTING;

    invoke-virtual {v2, v3}, Lcom/konka/kkimplements/tv/mstar/DataBaseDeskImpl;->updateSoundSetting(Lcom/konka/kkinterface/tv/DataBaseDesk$MS_USER_SOUND_SETTING;)V

    const/4 v2, 0x1

    return v2

    :catch_0
    move-exception v1

    iget-object v2, p0, Lcom/konka/kkimplements/tv/mstar/SoundDeskImpl;->com:Lcom/konka/kkinterface/tv/CommonDesk;

    const-string v3, "TvSoundServiceImpl"

    const-string v4, "Set Balance Exception"

    invoke-interface {v2, v3, v4}, Lcom/konka/kkinterface/tv/CommonDesk;->printfE(Ljava/lang/String;Ljava/lang/String;)V

    invoke-virtual {v1}, Lcom/mstar/android/tvapi/common/exception/TvCommonException;->printStackTrace()V

    goto :goto_0
.end method

.method public setBass(S)Z
    .locals 8
    .param p1    # S

    const/4 v7, 0x1

    iget-object v2, p0, Lcom/konka/kkimplements/tv/mstar/SoundDeskImpl;->databaseMgr:Lcom/konka/kkimplements/tv/mstar/DataBaseDeskImpl;

    invoke-virtual {v2}, Lcom/konka/kkimplements/tv/mstar/DataBaseDeskImpl;->querySoundSetting()Lcom/konka/kkinterface/tv/DataBaseDesk$MS_USER_SOUND_SETTING;

    iget-object v2, p0, Lcom/konka/kkimplements/tv/mstar/SoundDeskImpl;->databaseMgr:Lcom/konka/kkimplements/tv/mstar/DataBaseDeskImpl;

    invoke-virtual {v2}, Lcom/konka/kkimplements/tv/mstar/DataBaseDeskImpl;->getSound()Lcom/konka/kkinterface/tv/DataBaseDesk$MS_USER_SOUND_SETTING;

    move-result-object v2

    iput-object v2, p0, Lcom/konka/kkimplements/tv/mstar/SoundDeskImpl;->userSoundSetting:Lcom/konka/kkinterface/tv/DataBaseDesk$MS_USER_SOUND_SETTING;

    const/4 v0, 0x0

    new-instance v0, Lcom/mstar/android/tvapi/common/vo/DtvSoundEffect;

    invoke-direct {v0}, Lcom/mstar/android/tvapi/common/vo/DtvSoundEffect;-><init>()V

    iget-object v2, p0, Lcom/konka/kkimplements/tv/mstar/SoundDeskImpl;->databaseMgr:Lcom/konka/kkimplements/tv/mstar/DataBaseDeskImpl;

    iget-object v3, p0, Lcom/konka/kkimplements/tv/mstar/SoundDeskImpl;->userSoundSetting:Lcom/konka/kkinterface/tv/DataBaseDesk$MS_USER_SOUND_SETTING;

    iget-object v3, v3, Lcom/konka/kkinterface/tv/DataBaseDesk$MS_USER_SOUND_SETTING;->SoundMode:Lcom/konka/kkinterface/tv/DataBaseDesk$EN_SOUND_MODE;

    invoke-virtual {v2, v3}, Lcom/konka/kkimplements/tv/mstar/DataBaseDeskImpl;->getSoundMode(Lcom/konka/kkinterface/tv/DataBaseDesk$EN_SOUND_MODE;)Lcom/konka/kkinterface/tv/DataBaseDesk$SoundModeSeting;

    move-result-object v2

    iput-short p1, v2, Lcom/konka/kkinterface/tv/DataBaseDesk$SoundModeSeting;->Bass:S

    iget-object v2, p0, Lcom/konka/kkimplements/tv/mstar/SoundDeskImpl;->com:Lcom/konka/kkinterface/tv/CommonDesk;

    const-string v3, "TvSoundServiceImpl"

    new-instance v4, Ljava/lang/StringBuilder;

    const-string v5, "Bass is"

    invoke-direct {v4, v5}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    iget-object v5, p0, Lcom/konka/kkimplements/tv/mstar/SoundDeskImpl;->databaseMgr:Lcom/konka/kkimplements/tv/mstar/DataBaseDeskImpl;

    iget-object v6, p0, Lcom/konka/kkimplements/tv/mstar/SoundDeskImpl;->userSoundSetting:Lcom/konka/kkinterface/tv/DataBaseDesk$MS_USER_SOUND_SETTING;

    iget-object v6, v6, Lcom/konka/kkinterface/tv/DataBaseDesk$MS_USER_SOUND_SETTING;->SoundMode:Lcom/konka/kkinterface/tv/DataBaseDesk$EN_SOUND_MODE;

    invoke-virtual {v5, v6}, Lcom/konka/kkimplements/tv/mstar/DataBaseDeskImpl;->getSoundMode(Lcom/konka/kkinterface/tv/DataBaseDesk$EN_SOUND_MODE;)Lcom/konka/kkinterface/tv/DataBaseDesk$SoundModeSeting;

    move-result-object v5

    iget-short v5, v5, Lcom/konka/kkinterface/tv/DataBaseDesk$SoundModeSeting;->Bass:S

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-interface {v2, v3, v4}, Lcom/konka/kkinterface/tv/CommonDesk;->printfI(Ljava/lang/String;Ljava/lang/String;)V

    :try_start_0
    iget-object v2, p0, Lcom/konka/kkimplements/tv/mstar/SoundDeskImpl;->com:Lcom/konka/kkinterface/tv/CommonDesk;

    const-string v3, "TvSoundServiceImpl"

    new-instance v4, Ljava/lang/StringBuilder;

    const-string v5, "Bass is$$$$$$$$"

    invoke-direct {v4, v5}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v4, p1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-interface {v2, v3, v4}, Lcom/konka/kkinterface/tv/CommonDesk;->printfI(Ljava/lang/String;Ljava/lang/String;)V

    iget-object v2, p0, Lcom/konka/kkimplements/tv/mstar/SoundDeskImpl;->databaseMgr:Lcom/konka/kkimplements/tv/mstar/DataBaseDeskImpl;

    iget-object v3, p0, Lcom/konka/kkimplements/tv/mstar/SoundDeskImpl;->userSoundSetting:Lcom/konka/kkinterface/tv/DataBaseDesk$MS_USER_SOUND_SETTING;

    iget-object v3, v3, Lcom/konka/kkinterface/tv/DataBaseDesk$MS_USER_SOUND_SETTING;->SoundMode:Lcom/konka/kkinterface/tv/DataBaseDesk$EN_SOUND_MODE;

    invoke-virtual {v2, v3}, Lcom/konka/kkimplements/tv/mstar/DataBaseDeskImpl;->getSoundMode(Lcom/konka/kkinterface/tv/DataBaseDesk$EN_SOUND_MODE;)Lcom/konka/kkinterface/tv/DataBaseDesk$SoundModeSeting;

    move-result-object v2

    iput-short p1, v2, Lcom/konka/kkinterface/tv/DataBaseDesk$SoundModeSeting;->EqBand1:S

    iget-object v2, v0, Lcom/mstar/android/tvapi/common/vo/DtvSoundEffect;->soundParameterEqs:[Lcom/mstar/android/tvapi/common/vo/SoundParameterEq;

    const/4 v3, 0x0

    aget-object v2, v2, v3

    iput p1, v2, Lcom/mstar/android/tvapi/common/vo/SoundParameterEq;->eqLevel:I

    iget-object v2, v0, Lcom/mstar/android/tvapi/common/vo/DtvSoundEffect;->soundParameterEqs:[Lcom/mstar/android/tvapi/common/vo/SoundParameterEq;

    const/4 v3, 0x1

    aget-object v2, v2, v3

    iget-object v3, p0, Lcom/konka/kkimplements/tv/mstar/SoundDeskImpl;->databaseMgr:Lcom/konka/kkimplements/tv/mstar/DataBaseDeskImpl;

    iget-object v4, p0, Lcom/konka/kkimplements/tv/mstar/SoundDeskImpl;->userSoundSetting:Lcom/konka/kkinterface/tv/DataBaseDesk$MS_USER_SOUND_SETTING;

    iget-object v4, v4, Lcom/konka/kkinterface/tv/DataBaseDesk$MS_USER_SOUND_SETTING;->SoundMode:Lcom/konka/kkinterface/tv/DataBaseDesk$EN_SOUND_MODE;

    invoke-virtual {v3, v4}, Lcom/konka/kkimplements/tv/mstar/DataBaseDeskImpl;->getSoundMode(Lcom/konka/kkinterface/tv/DataBaseDesk$EN_SOUND_MODE;)Lcom/konka/kkinterface/tv/DataBaseDesk$SoundModeSeting;

    move-result-object v3

    iget-short v3, v3, Lcom/konka/kkinterface/tv/DataBaseDesk$SoundModeSeting;->EqBand2:S

    iput v3, v2, Lcom/mstar/android/tvapi/common/vo/SoundParameterEq;->eqLevel:I

    iget-object v2, v0, Lcom/mstar/android/tvapi/common/vo/DtvSoundEffect;->soundParameterEqs:[Lcom/mstar/android/tvapi/common/vo/SoundParameterEq;

    const/4 v3, 0x2

    aget-object v2, v2, v3

    iget-object v3, p0, Lcom/konka/kkimplements/tv/mstar/SoundDeskImpl;->databaseMgr:Lcom/konka/kkimplements/tv/mstar/DataBaseDeskImpl;

    iget-object v4, p0, Lcom/konka/kkimplements/tv/mstar/SoundDeskImpl;->userSoundSetting:Lcom/konka/kkinterface/tv/DataBaseDesk$MS_USER_SOUND_SETTING;

    iget-object v4, v4, Lcom/konka/kkinterface/tv/DataBaseDesk$MS_USER_SOUND_SETTING;->SoundMode:Lcom/konka/kkinterface/tv/DataBaseDesk$EN_SOUND_MODE;

    invoke-virtual {v3, v4}, Lcom/konka/kkimplements/tv/mstar/DataBaseDeskImpl;->getSoundMode(Lcom/konka/kkinterface/tv/DataBaseDesk$EN_SOUND_MODE;)Lcom/konka/kkinterface/tv/DataBaseDesk$SoundModeSeting;

    move-result-object v3

    iget-short v3, v3, Lcom/konka/kkinterface/tv/DataBaseDesk$SoundModeSeting;->EqBand3:S

    iput v3, v2, Lcom/mstar/android/tvapi/common/vo/SoundParameterEq;->eqLevel:I

    iget-object v2, v0, Lcom/mstar/android/tvapi/common/vo/DtvSoundEffect;->soundParameterEqs:[Lcom/mstar/android/tvapi/common/vo/SoundParameterEq;

    const/4 v3, 0x3

    aget-object v2, v2, v3

    iget-object v3, p0, Lcom/konka/kkimplements/tv/mstar/SoundDeskImpl;->databaseMgr:Lcom/konka/kkimplements/tv/mstar/DataBaseDeskImpl;

    iget-object v4, p0, Lcom/konka/kkimplements/tv/mstar/SoundDeskImpl;->userSoundSetting:Lcom/konka/kkinterface/tv/DataBaseDesk$MS_USER_SOUND_SETTING;

    iget-object v4, v4, Lcom/konka/kkinterface/tv/DataBaseDesk$MS_USER_SOUND_SETTING;->SoundMode:Lcom/konka/kkinterface/tv/DataBaseDesk$EN_SOUND_MODE;

    invoke-virtual {v3, v4}, Lcom/konka/kkimplements/tv/mstar/DataBaseDeskImpl;->getSoundMode(Lcom/konka/kkinterface/tv/DataBaseDesk$EN_SOUND_MODE;)Lcom/konka/kkinterface/tv/DataBaseDesk$SoundModeSeting;

    move-result-object v3

    iget-short v3, v3, Lcom/konka/kkinterface/tv/DataBaseDesk$SoundModeSeting;->EqBand4:S

    iput v3, v2, Lcom/mstar/android/tvapi/common/vo/SoundParameterEq;->eqLevel:I

    iget-object v2, v0, Lcom/mstar/android/tvapi/common/vo/DtvSoundEffect;->soundParameterEqs:[Lcom/mstar/android/tvapi/common/vo/SoundParameterEq;

    const/4 v3, 0x4

    aget-object v2, v2, v3

    iget-object v3, p0, Lcom/konka/kkimplements/tv/mstar/SoundDeskImpl;->databaseMgr:Lcom/konka/kkimplements/tv/mstar/DataBaseDeskImpl;

    iget-object v4, p0, Lcom/konka/kkimplements/tv/mstar/SoundDeskImpl;->userSoundSetting:Lcom/konka/kkinterface/tv/DataBaseDesk$MS_USER_SOUND_SETTING;

    iget-object v4, v4, Lcom/konka/kkinterface/tv/DataBaseDesk$MS_USER_SOUND_SETTING;->SoundMode:Lcom/konka/kkinterface/tv/DataBaseDesk$EN_SOUND_MODE;

    invoke-virtual {v3, v4}, Lcom/konka/kkimplements/tv/mstar/DataBaseDeskImpl;->getSoundMode(Lcom/konka/kkinterface/tv/DataBaseDesk$EN_SOUND_MODE;)Lcom/konka/kkinterface/tv/DataBaseDesk$SoundModeSeting;

    move-result-object v3

    iget-short v3, v3, Lcom/konka/kkinterface/tv/DataBaseDesk$SoundModeSeting;->EqBand5:S

    iput v3, v2, Lcom/mstar/android/tvapi/common/vo/SoundParameterEq;->eqLevel:I

    const/4 v2, 0x5

    iput-short v2, v0, Lcom/mstar/android/tvapi/common/vo/DtvSoundEffect;->eqBandNumber:S

    invoke-static {}, Lcom/mstar/android/tvapi/common/TvManager;->getInstance()Lcom/mstar/android/tvapi/common/TvManager;

    move-result-object v2

    if-eqz v2, :cond_0

    invoke-static {}, Lcom/mstar/android/tvapi/common/TvManager;->getInstance()Lcom/mstar/android/tvapi/common/TvManager;

    move-result-object v2

    invoke-virtual {v2}, Lcom/mstar/android/tvapi/common/TvManager;->getAudioManager()Lcom/mstar/android/tvapi/common/AudioManager;

    move-result-object v2

    sget-object v3, Lcom/mstar/android/tvapi/common/vo/EnumSoundEffectType;->E_EQ:Lcom/mstar/android/tvapi/common/vo/EnumSoundEffectType;

    invoke-virtual {v2, v3, v0}, Lcom/mstar/android/tvapi/common/AudioManager;->setBasicSoundEffect(Lcom/mstar/android/tvapi/common/vo/EnumSoundEffectType;Lcom/mstar/android/tvapi/common/vo/DtvSoundEffect;)Lcom/mstar/android/tvapi/common/vo/EnumAudioReturn;
    :try_end_0
    .catch Lcom/mstar/android/tvapi/common/exception/TvCommonException; {:try_start_0 .. :try_end_0} :catch_0

    :cond_0
    :goto_0
    iget-object v2, p0, Lcom/konka/kkimplements/tv/mstar/SoundDeskImpl;->databaseMgr:Lcom/konka/kkimplements/tv/mstar/DataBaseDeskImpl;

    iget-object v3, p0, Lcom/konka/kkimplements/tv/mstar/SoundDeskImpl;->databaseMgr:Lcom/konka/kkimplements/tv/mstar/DataBaseDeskImpl;

    iget-object v4, p0, Lcom/konka/kkimplements/tv/mstar/SoundDeskImpl;->userSoundSetting:Lcom/konka/kkinterface/tv/DataBaseDesk$MS_USER_SOUND_SETTING;

    iget-object v4, v4, Lcom/konka/kkinterface/tv/DataBaseDesk$MS_USER_SOUND_SETTING;->SoundMode:Lcom/konka/kkinterface/tv/DataBaseDesk$EN_SOUND_MODE;

    invoke-virtual {v3, v4}, Lcom/konka/kkimplements/tv/mstar/DataBaseDeskImpl;->getSoundMode(Lcom/konka/kkinterface/tv/DataBaseDesk$EN_SOUND_MODE;)Lcom/konka/kkinterface/tv/DataBaseDesk$SoundModeSeting;

    move-result-object v3

    iget-object v4, p0, Lcom/konka/kkimplements/tv/mstar/SoundDeskImpl;->userSoundSetting:Lcom/konka/kkinterface/tv/DataBaseDesk$MS_USER_SOUND_SETTING;

    iget-object v4, v4, Lcom/konka/kkinterface/tv/DataBaseDesk$MS_USER_SOUND_SETTING;->SoundMode:Lcom/konka/kkinterface/tv/DataBaseDesk$EN_SOUND_MODE;

    invoke-virtual {v4}, Lcom/konka/kkinterface/tv/DataBaseDesk$EN_SOUND_MODE;->ordinal()I

    move-result v4

    invoke-virtual {v2, v3, v4}, Lcom/konka/kkimplements/tv/mstar/DataBaseDeskImpl;->updateSoundModeSetting(Lcom/konka/kkinterface/tv/DataBaseDesk$SoundModeSeting;I)V

    return v7

    :catch_0
    move-exception v1

    iget-object v2, p0, Lcom/konka/kkimplements/tv/mstar/SoundDeskImpl;->com:Lcom/konka/kkinterface/tv/CommonDesk;

    const-string v3, "TvSoundServiceImpl"

    const-string v4, "Set Sound Mode Exception"

    invoke-interface {v2, v3, v4}, Lcom/konka/kkinterface/tv/CommonDesk;->printfE(Ljava/lang/String;Ljava/lang/String;)V

    invoke-virtual {v1}, Lcom/mstar/android/tvapi/common/exception/TvCommonException;->printStackTrace()V

    goto :goto_0
.end method

.method public setEqBand10k(S)Z
    .locals 5
    .param p1    # S

    iget-object v0, p0, Lcom/konka/kkimplements/tv/mstar/SoundDeskImpl;->databaseMgr:Lcom/konka/kkimplements/tv/mstar/DataBaseDeskImpl;

    invoke-virtual {v0}, Lcom/konka/kkimplements/tv/mstar/DataBaseDeskImpl;->querySoundSetting()Lcom/konka/kkinterface/tv/DataBaseDesk$MS_USER_SOUND_SETTING;

    iget-object v0, p0, Lcom/konka/kkimplements/tv/mstar/SoundDeskImpl;->databaseMgr:Lcom/konka/kkimplements/tv/mstar/DataBaseDeskImpl;

    invoke-virtual {v0}, Lcom/konka/kkimplements/tv/mstar/DataBaseDeskImpl;->getSound()Lcom/konka/kkinterface/tv/DataBaseDesk$MS_USER_SOUND_SETTING;

    move-result-object v0

    iput-object v0, p0, Lcom/konka/kkimplements/tv/mstar/SoundDeskImpl;->userSoundSetting:Lcom/konka/kkinterface/tv/DataBaseDesk$MS_USER_SOUND_SETTING;

    iget-object v0, p0, Lcom/konka/kkimplements/tv/mstar/SoundDeskImpl;->databaseMgr:Lcom/konka/kkimplements/tv/mstar/DataBaseDeskImpl;

    iget-object v1, p0, Lcom/konka/kkimplements/tv/mstar/SoundDeskImpl;->userSoundSetting:Lcom/konka/kkinterface/tv/DataBaseDesk$MS_USER_SOUND_SETTING;

    iget-object v1, v1, Lcom/konka/kkinterface/tv/DataBaseDesk$MS_USER_SOUND_SETTING;->SoundMode:Lcom/konka/kkinterface/tv/DataBaseDesk$EN_SOUND_MODE;

    invoke-virtual {v0, v1}, Lcom/konka/kkimplements/tv/mstar/DataBaseDeskImpl;->getSoundMode(Lcom/konka/kkinterface/tv/DataBaseDesk$EN_SOUND_MODE;)Lcom/konka/kkinterface/tv/DataBaseDesk$SoundModeSeting;

    move-result-object v0

    iput-short p1, v0, Lcom/konka/kkinterface/tv/DataBaseDesk$SoundModeSeting;->EqBand5:S

    iget-object v0, p0, Lcom/konka/kkimplements/tv/mstar/SoundDeskImpl;->com:Lcom/konka/kkinterface/tv/CommonDesk;

    const-string v1, "TvSoundServiceImpl"

    new-instance v2, Ljava/lang/StringBuilder;

    const-string v3, "EqBand5 is"

    invoke-direct {v2, v3}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    iget-object v3, p0, Lcom/konka/kkimplements/tv/mstar/SoundDeskImpl;->databaseMgr:Lcom/konka/kkimplements/tv/mstar/DataBaseDeskImpl;

    iget-object v4, p0, Lcom/konka/kkimplements/tv/mstar/SoundDeskImpl;->userSoundSetting:Lcom/konka/kkinterface/tv/DataBaseDesk$MS_USER_SOUND_SETTING;

    iget-object v4, v4, Lcom/konka/kkinterface/tv/DataBaseDesk$MS_USER_SOUND_SETTING;->SoundMode:Lcom/konka/kkinterface/tv/DataBaseDesk$EN_SOUND_MODE;

    invoke-virtual {v3, v4}, Lcom/konka/kkimplements/tv/mstar/DataBaseDeskImpl;->getSoundMode(Lcom/konka/kkinterface/tv/DataBaseDesk$EN_SOUND_MODE;)Lcom/konka/kkinterface/tv/DataBaseDesk$SoundModeSeting;

    move-result-object v3

    iget-short v3, v3, Lcom/konka/kkinterface/tv/DataBaseDesk$SoundModeSeting;->EqBand5:S

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-interface {v0, v1, v2}, Lcom/konka/kkinterface/tv/CommonDesk;->printfI(Ljava/lang/String;Ljava/lang/String;)V

    iget-object v0, p0, Lcom/konka/kkimplements/tv/mstar/SoundDeskImpl;->userSoundSetting:Lcom/konka/kkinterface/tv/DataBaseDesk$MS_USER_SOUND_SETTING;

    iget-object v0, v0, Lcom/konka/kkinterface/tv/DataBaseDesk$MS_USER_SOUND_SETTING;->SoundMode:Lcom/konka/kkinterface/tv/DataBaseDesk$EN_SOUND_MODE;

    invoke-virtual {p0, v0}, Lcom/konka/kkimplements/tv/mstar/SoundDeskImpl;->setSoundMode(Lcom/konka/kkinterface/tv/DataBaseDesk$EN_SOUND_MODE;)Z

    iget-object v0, p0, Lcom/konka/kkimplements/tv/mstar/SoundDeskImpl;->databaseMgr:Lcom/konka/kkimplements/tv/mstar/DataBaseDeskImpl;

    iget-object v1, p0, Lcom/konka/kkimplements/tv/mstar/SoundDeskImpl;->databaseMgr:Lcom/konka/kkimplements/tv/mstar/DataBaseDeskImpl;

    iget-object v2, p0, Lcom/konka/kkimplements/tv/mstar/SoundDeskImpl;->userSoundSetting:Lcom/konka/kkinterface/tv/DataBaseDesk$MS_USER_SOUND_SETTING;

    iget-object v2, v2, Lcom/konka/kkinterface/tv/DataBaseDesk$MS_USER_SOUND_SETTING;->SoundMode:Lcom/konka/kkinterface/tv/DataBaseDesk$EN_SOUND_MODE;

    invoke-virtual {v1, v2}, Lcom/konka/kkimplements/tv/mstar/DataBaseDeskImpl;->getSoundMode(Lcom/konka/kkinterface/tv/DataBaseDesk$EN_SOUND_MODE;)Lcom/konka/kkinterface/tv/DataBaseDesk$SoundModeSeting;

    move-result-object v1

    iget-object v2, p0, Lcom/konka/kkimplements/tv/mstar/SoundDeskImpl;->userSoundSetting:Lcom/konka/kkinterface/tv/DataBaseDesk$MS_USER_SOUND_SETTING;

    iget-object v2, v2, Lcom/konka/kkinterface/tv/DataBaseDesk$MS_USER_SOUND_SETTING;->SoundMode:Lcom/konka/kkinterface/tv/DataBaseDesk$EN_SOUND_MODE;

    invoke-virtual {v2}, Lcom/konka/kkinterface/tv/DataBaseDesk$EN_SOUND_MODE;->ordinal()I

    move-result v2

    invoke-virtual {v0, v1, v2}, Lcom/konka/kkimplements/tv/mstar/DataBaseDeskImpl;->updateSoundModeSetting(Lcom/konka/kkinterface/tv/DataBaseDesk$SoundModeSeting;I)V

    const/4 v0, 0x1

    return v0
.end method

.method public setEqBand120(S)Z
    .locals 5
    .param p1    # S

    iget-object v0, p0, Lcom/konka/kkimplements/tv/mstar/SoundDeskImpl;->databaseMgr:Lcom/konka/kkimplements/tv/mstar/DataBaseDeskImpl;

    invoke-virtual {v0}, Lcom/konka/kkimplements/tv/mstar/DataBaseDeskImpl;->querySoundSetting()Lcom/konka/kkinterface/tv/DataBaseDesk$MS_USER_SOUND_SETTING;

    iget-object v0, p0, Lcom/konka/kkimplements/tv/mstar/SoundDeskImpl;->databaseMgr:Lcom/konka/kkimplements/tv/mstar/DataBaseDeskImpl;

    invoke-virtual {v0}, Lcom/konka/kkimplements/tv/mstar/DataBaseDeskImpl;->getSound()Lcom/konka/kkinterface/tv/DataBaseDesk$MS_USER_SOUND_SETTING;

    move-result-object v0

    iput-object v0, p0, Lcom/konka/kkimplements/tv/mstar/SoundDeskImpl;->userSoundSetting:Lcom/konka/kkinterface/tv/DataBaseDesk$MS_USER_SOUND_SETTING;

    iget-object v0, p0, Lcom/konka/kkimplements/tv/mstar/SoundDeskImpl;->databaseMgr:Lcom/konka/kkimplements/tv/mstar/DataBaseDeskImpl;

    iget-object v1, p0, Lcom/konka/kkimplements/tv/mstar/SoundDeskImpl;->userSoundSetting:Lcom/konka/kkinterface/tv/DataBaseDesk$MS_USER_SOUND_SETTING;

    iget-object v1, v1, Lcom/konka/kkinterface/tv/DataBaseDesk$MS_USER_SOUND_SETTING;->SoundMode:Lcom/konka/kkinterface/tv/DataBaseDesk$EN_SOUND_MODE;

    invoke-virtual {v0, v1}, Lcom/konka/kkimplements/tv/mstar/DataBaseDeskImpl;->getSoundMode(Lcom/konka/kkinterface/tv/DataBaseDesk$EN_SOUND_MODE;)Lcom/konka/kkinterface/tv/DataBaseDesk$SoundModeSeting;

    move-result-object v0

    iput-short p1, v0, Lcom/konka/kkinterface/tv/DataBaseDesk$SoundModeSeting;->EqBand1:S

    iget-object v0, p0, Lcom/konka/kkimplements/tv/mstar/SoundDeskImpl;->com:Lcom/konka/kkinterface/tv/CommonDesk;

    const-string v1, "TvSoundServiceImpl"

    new-instance v2, Ljava/lang/StringBuilder;

    const-string v3, "EqBand1 is"

    invoke-direct {v2, v3}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    iget-object v3, p0, Lcom/konka/kkimplements/tv/mstar/SoundDeskImpl;->databaseMgr:Lcom/konka/kkimplements/tv/mstar/DataBaseDeskImpl;

    iget-object v4, p0, Lcom/konka/kkimplements/tv/mstar/SoundDeskImpl;->userSoundSetting:Lcom/konka/kkinterface/tv/DataBaseDesk$MS_USER_SOUND_SETTING;

    iget-object v4, v4, Lcom/konka/kkinterface/tv/DataBaseDesk$MS_USER_SOUND_SETTING;->SoundMode:Lcom/konka/kkinterface/tv/DataBaseDesk$EN_SOUND_MODE;

    invoke-virtual {v3, v4}, Lcom/konka/kkimplements/tv/mstar/DataBaseDeskImpl;->getSoundMode(Lcom/konka/kkinterface/tv/DataBaseDesk$EN_SOUND_MODE;)Lcom/konka/kkinterface/tv/DataBaseDesk$SoundModeSeting;

    move-result-object v3

    iget-short v3, v3, Lcom/konka/kkinterface/tv/DataBaseDesk$SoundModeSeting;->EqBand1:S

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-interface {v0, v1, v2}, Lcom/konka/kkinterface/tv/CommonDesk;->printfI(Ljava/lang/String;Ljava/lang/String;)V

    iget-object v0, p0, Lcom/konka/kkimplements/tv/mstar/SoundDeskImpl;->userSoundSetting:Lcom/konka/kkinterface/tv/DataBaseDesk$MS_USER_SOUND_SETTING;

    iget-object v0, v0, Lcom/konka/kkinterface/tv/DataBaseDesk$MS_USER_SOUND_SETTING;->SoundMode:Lcom/konka/kkinterface/tv/DataBaseDesk$EN_SOUND_MODE;

    invoke-virtual {p0, v0}, Lcom/konka/kkimplements/tv/mstar/SoundDeskImpl;->setSoundMode(Lcom/konka/kkinterface/tv/DataBaseDesk$EN_SOUND_MODE;)Z

    iget-object v0, p0, Lcom/konka/kkimplements/tv/mstar/SoundDeskImpl;->databaseMgr:Lcom/konka/kkimplements/tv/mstar/DataBaseDeskImpl;

    iget-object v1, p0, Lcom/konka/kkimplements/tv/mstar/SoundDeskImpl;->databaseMgr:Lcom/konka/kkimplements/tv/mstar/DataBaseDeskImpl;

    iget-object v2, p0, Lcom/konka/kkimplements/tv/mstar/SoundDeskImpl;->userSoundSetting:Lcom/konka/kkinterface/tv/DataBaseDesk$MS_USER_SOUND_SETTING;

    iget-object v2, v2, Lcom/konka/kkinterface/tv/DataBaseDesk$MS_USER_SOUND_SETTING;->SoundMode:Lcom/konka/kkinterface/tv/DataBaseDesk$EN_SOUND_MODE;

    invoke-virtual {v1, v2}, Lcom/konka/kkimplements/tv/mstar/DataBaseDeskImpl;->getSoundMode(Lcom/konka/kkinterface/tv/DataBaseDesk$EN_SOUND_MODE;)Lcom/konka/kkinterface/tv/DataBaseDesk$SoundModeSeting;

    move-result-object v1

    iget-object v2, p0, Lcom/konka/kkimplements/tv/mstar/SoundDeskImpl;->userSoundSetting:Lcom/konka/kkinterface/tv/DataBaseDesk$MS_USER_SOUND_SETTING;

    iget-object v2, v2, Lcom/konka/kkinterface/tv/DataBaseDesk$MS_USER_SOUND_SETTING;->SoundMode:Lcom/konka/kkinterface/tv/DataBaseDesk$EN_SOUND_MODE;

    invoke-virtual {v2}, Lcom/konka/kkinterface/tv/DataBaseDesk$EN_SOUND_MODE;->ordinal()I

    move-result v2

    invoke-virtual {v0, v1, v2}, Lcom/konka/kkimplements/tv/mstar/DataBaseDeskImpl;->updateSoundModeSetting(Lcom/konka/kkinterface/tv/DataBaseDesk$SoundModeSeting;I)V

    const/4 v0, 0x1

    return v0
.end method

.method public setEqBand1500(S)Z
    .locals 5
    .param p1    # S

    iget-object v0, p0, Lcom/konka/kkimplements/tv/mstar/SoundDeskImpl;->databaseMgr:Lcom/konka/kkimplements/tv/mstar/DataBaseDeskImpl;

    invoke-virtual {v0}, Lcom/konka/kkimplements/tv/mstar/DataBaseDeskImpl;->querySoundSetting()Lcom/konka/kkinterface/tv/DataBaseDesk$MS_USER_SOUND_SETTING;

    iget-object v0, p0, Lcom/konka/kkimplements/tv/mstar/SoundDeskImpl;->databaseMgr:Lcom/konka/kkimplements/tv/mstar/DataBaseDeskImpl;

    invoke-virtual {v0}, Lcom/konka/kkimplements/tv/mstar/DataBaseDeskImpl;->getSound()Lcom/konka/kkinterface/tv/DataBaseDesk$MS_USER_SOUND_SETTING;

    move-result-object v0

    iput-object v0, p0, Lcom/konka/kkimplements/tv/mstar/SoundDeskImpl;->userSoundSetting:Lcom/konka/kkinterface/tv/DataBaseDesk$MS_USER_SOUND_SETTING;

    iget-object v0, p0, Lcom/konka/kkimplements/tv/mstar/SoundDeskImpl;->databaseMgr:Lcom/konka/kkimplements/tv/mstar/DataBaseDeskImpl;

    iget-object v1, p0, Lcom/konka/kkimplements/tv/mstar/SoundDeskImpl;->userSoundSetting:Lcom/konka/kkinterface/tv/DataBaseDesk$MS_USER_SOUND_SETTING;

    iget-object v1, v1, Lcom/konka/kkinterface/tv/DataBaseDesk$MS_USER_SOUND_SETTING;->SoundMode:Lcom/konka/kkinterface/tv/DataBaseDesk$EN_SOUND_MODE;

    invoke-virtual {v0, v1}, Lcom/konka/kkimplements/tv/mstar/DataBaseDeskImpl;->getSoundMode(Lcom/konka/kkinterface/tv/DataBaseDesk$EN_SOUND_MODE;)Lcom/konka/kkinterface/tv/DataBaseDesk$SoundModeSeting;

    move-result-object v0

    iput-short p1, v0, Lcom/konka/kkinterface/tv/DataBaseDesk$SoundModeSeting;->EqBand3:S

    iget-object v0, p0, Lcom/konka/kkimplements/tv/mstar/SoundDeskImpl;->com:Lcom/konka/kkinterface/tv/CommonDesk;

    const-string v1, "TvSoundServiceImpl"

    new-instance v2, Ljava/lang/StringBuilder;

    const-string v3, "EqBand3 is"

    invoke-direct {v2, v3}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    iget-object v3, p0, Lcom/konka/kkimplements/tv/mstar/SoundDeskImpl;->databaseMgr:Lcom/konka/kkimplements/tv/mstar/DataBaseDeskImpl;

    iget-object v4, p0, Lcom/konka/kkimplements/tv/mstar/SoundDeskImpl;->userSoundSetting:Lcom/konka/kkinterface/tv/DataBaseDesk$MS_USER_SOUND_SETTING;

    iget-object v4, v4, Lcom/konka/kkinterface/tv/DataBaseDesk$MS_USER_SOUND_SETTING;->SoundMode:Lcom/konka/kkinterface/tv/DataBaseDesk$EN_SOUND_MODE;

    invoke-virtual {v3, v4}, Lcom/konka/kkimplements/tv/mstar/DataBaseDeskImpl;->getSoundMode(Lcom/konka/kkinterface/tv/DataBaseDesk$EN_SOUND_MODE;)Lcom/konka/kkinterface/tv/DataBaseDesk$SoundModeSeting;

    move-result-object v3

    iget-short v3, v3, Lcom/konka/kkinterface/tv/DataBaseDesk$SoundModeSeting;->EqBand3:S

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-interface {v0, v1, v2}, Lcom/konka/kkinterface/tv/CommonDesk;->printfI(Ljava/lang/String;Ljava/lang/String;)V

    iget-object v0, p0, Lcom/konka/kkimplements/tv/mstar/SoundDeskImpl;->userSoundSetting:Lcom/konka/kkinterface/tv/DataBaseDesk$MS_USER_SOUND_SETTING;

    iget-object v0, v0, Lcom/konka/kkinterface/tv/DataBaseDesk$MS_USER_SOUND_SETTING;->SoundMode:Lcom/konka/kkinterface/tv/DataBaseDesk$EN_SOUND_MODE;

    invoke-virtual {p0, v0}, Lcom/konka/kkimplements/tv/mstar/SoundDeskImpl;->setSoundMode(Lcom/konka/kkinterface/tv/DataBaseDesk$EN_SOUND_MODE;)Z

    iget-object v0, p0, Lcom/konka/kkimplements/tv/mstar/SoundDeskImpl;->databaseMgr:Lcom/konka/kkimplements/tv/mstar/DataBaseDeskImpl;

    iget-object v1, p0, Lcom/konka/kkimplements/tv/mstar/SoundDeskImpl;->databaseMgr:Lcom/konka/kkimplements/tv/mstar/DataBaseDeskImpl;

    iget-object v2, p0, Lcom/konka/kkimplements/tv/mstar/SoundDeskImpl;->userSoundSetting:Lcom/konka/kkinterface/tv/DataBaseDesk$MS_USER_SOUND_SETTING;

    iget-object v2, v2, Lcom/konka/kkinterface/tv/DataBaseDesk$MS_USER_SOUND_SETTING;->SoundMode:Lcom/konka/kkinterface/tv/DataBaseDesk$EN_SOUND_MODE;

    invoke-virtual {v1, v2}, Lcom/konka/kkimplements/tv/mstar/DataBaseDeskImpl;->getSoundMode(Lcom/konka/kkinterface/tv/DataBaseDesk$EN_SOUND_MODE;)Lcom/konka/kkinterface/tv/DataBaseDesk$SoundModeSeting;

    move-result-object v1

    iget-object v2, p0, Lcom/konka/kkimplements/tv/mstar/SoundDeskImpl;->userSoundSetting:Lcom/konka/kkinterface/tv/DataBaseDesk$MS_USER_SOUND_SETTING;

    iget-object v2, v2, Lcom/konka/kkinterface/tv/DataBaseDesk$MS_USER_SOUND_SETTING;->SoundMode:Lcom/konka/kkinterface/tv/DataBaseDesk$EN_SOUND_MODE;

    invoke-virtual {v2}, Lcom/konka/kkinterface/tv/DataBaseDesk$EN_SOUND_MODE;->ordinal()I

    move-result v2

    invoke-virtual {v0, v1, v2}, Lcom/konka/kkimplements/tv/mstar/DataBaseDeskImpl;->updateSoundModeSetting(Lcom/konka/kkinterface/tv/DataBaseDesk$SoundModeSeting;I)V

    const/4 v0, 0x1

    return v0
.end method

.method public setEqBand500(S)Z
    .locals 5
    .param p1    # S

    iget-object v0, p0, Lcom/konka/kkimplements/tv/mstar/SoundDeskImpl;->databaseMgr:Lcom/konka/kkimplements/tv/mstar/DataBaseDeskImpl;

    invoke-virtual {v0}, Lcom/konka/kkimplements/tv/mstar/DataBaseDeskImpl;->querySoundSetting()Lcom/konka/kkinterface/tv/DataBaseDesk$MS_USER_SOUND_SETTING;

    iget-object v0, p0, Lcom/konka/kkimplements/tv/mstar/SoundDeskImpl;->databaseMgr:Lcom/konka/kkimplements/tv/mstar/DataBaseDeskImpl;

    invoke-virtual {v0}, Lcom/konka/kkimplements/tv/mstar/DataBaseDeskImpl;->getSound()Lcom/konka/kkinterface/tv/DataBaseDesk$MS_USER_SOUND_SETTING;

    move-result-object v0

    iput-object v0, p0, Lcom/konka/kkimplements/tv/mstar/SoundDeskImpl;->userSoundSetting:Lcom/konka/kkinterface/tv/DataBaseDesk$MS_USER_SOUND_SETTING;

    iget-object v0, p0, Lcom/konka/kkimplements/tv/mstar/SoundDeskImpl;->databaseMgr:Lcom/konka/kkimplements/tv/mstar/DataBaseDeskImpl;

    iget-object v1, p0, Lcom/konka/kkimplements/tv/mstar/SoundDeskImpl;->userSoundSetting:Lcom/konka/kkinterface/tv/DataBaseDesk$MS_USER_SOUND_SETTING;

    iget-object v1, v1, Lcom/konka/kkinterface/tv/DataBaseDesk$MS_USER_SOUND_SETTING;->SoundMode:Lcom/konka/kkinterface/tv/DataBaseDesk$EN_SOUND_MODE;

    invoke-virtual {v0, v1}, Lcom/konka/kkimplements/tv/mstar/DataBaseDeskImpl;->getSoundMode(Lcom/konka/kkinterface/tv/DataBaseDesk$EN_SOUND_MODE;)Lcom/konka/kkinterface/tv/DataBaseDesk$SoundModeSeting;

    move-result-object v0

    iput-short p1, v0, Lcom/konka/kkinterface/tv/DataBaseDesk$SoundModeSeting;->EqBand2:S

    iget-object v0, p0, Lcom/konka/kkimplements/tv/mstar/SoundDeskImpl;->com:Lcom/konka/kkinterface/tv/CommonDesk;

    const-string v1, "TvSoundServiceImpl"

    new-instance v2, Ljava/lang/StringBuilder;

    const-string v3, "EqBand2 is"

    invoke-direct {v2, v3}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    iget-object v3, p0, Lcom/konka/kkimplements/tv/mstar/SoundDeskImpl;->databaseMgr:Lcom/konka/kkimplements/tv/mstar/DataBaseDeskImpl;

    iget-object v4, p0, Lcom/konka/kkimplements/tv/mstar/SoundDeskImpl;->userSoundSetting:Lcom/konka/kkinterface/tv/DataBaseDesk$MS_USER_SOUND_SETTING;

    iget-object v4, v4, Lcom/konka/kkinterface/tv/DataBaseDesk$MS_USER_SOUND_SETTING;->SoundMode:Lcom/konka/kkinterface/tv/DataBaseDesk$EN_SOUND_MODE;

    invoke-virtual {v3, v4}, Lcom/konka/kkimplements/tv/mstar/DataBaseDeskImpl;->getSoundMode(Lcom/konka/kkinterface/tv/DataBaseDesk$EN_SOUND_MODE;)Lcom/konka/kkinterface/tv/DataBaseDesk$SoundModeSeting;

    move-result-object v3

    iget-short v3, v3, Lcom/konka/kkinterface/tv/DataBaseDesk$SoundModeSeting;->EqBand2:S

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-interface {v0, v1, v2}, Lcom/konka/kkinterface/tv/CommonDesk;->printfI(Ljava/lang/String;Ljava/lang/String;)V

    iget-object v0, p0, Lcom/konka/kkimplements/tv/mstar/SoundDeskImpl;->userSoundSetting:Lcom/konka/kkinterface/tv/DataBaseDesk$MS_USER_SOUND_SETTING;

    iget-object v0, v0, Lcom/konka/kkinterface/tv/DataBaseDesk$MS_USER_SOUND_SETTING;->SoundMode:Lcom/konka/kkinterface/tv/DataBaseDesk$EN_SOUND_MODE;

    invoke-virtual {p0, v0}, Lcom/konka/kkimplements/tv/mstar/SoundDeskImpl;->setSoundMode(Lcom/konka/kkinterface/tv/DataBaseDesk$EN_SOUND_MODE;)Z

    iget-object v0, p0, Lcom/konka/kkimplements/tv/mstar/SoundDeskImpl;->databaseMgr:Lcom/konka/kkimplements/tv/mstar/DataBaseDeskImpl;

    iget-object v1, p0, Lcom/konka/kkimplements/tv/mstar/SoundDeskImpl;->databaseMgr:Lcom/konka/kkimplements/tv/mstar/DataBaseDeskImpl;

    iget-object v2, p0, Lcom/konka/kkimplements/tv/mstar/SoundDeskImpl;->userSoundSetting:Lcom/konka/kkinterface/tv/DataBaseDesk$MS_USER_SOUND_SETTING;

    iget-object v2, v2, Lcom/konka/kkinterface/tv/DataBaseDesk$MS_USER_SOUND_SETTING;->SoundMode:Lcom/konka/kkinterface/tv/DataBaseDesk$EN_SOUND_MODE;

    invoke-virtual {v1, v2}, Lcom/konka/kkimplements/tv/mstar/DataBaseDeskImpl;->getSoundMode(Lcom/konka/kkinterface/tv/DataBaseDesk$EN_SOUND_MODE;)Lcom/konka/kkinterface/tv/DataBaseDesk$SoundModeSeting;

    move-result-object v1

    iget-object v2, p0, Lcom/konka/kkimplements/tv/mstar/SoundDeskImpl;->userSoundSetting:Lcom/konka/kkinterface/tv/DataBaseDesk$MS_USER_SOUND_SETTING;

    iget-object v2, v2, Lcom/konka/kkinterface/tv/DataBaseDesk$MS_USER_SOUND_SETTING;->SoundMode:Lcom/konka/kkinterface/tv/DataBaseDesk$EN_SOUND_MODE;

    invoke-virtual {v2}, Lcom/konka/kkinterface/tv/DataBaseDesk$EN_SOUND_MODE;->ordinal()I

    move-result v2

    invoke-virtual {v0, v1, v2}, Lcom/konka/kkimplements/tv/mstar/DataBaseDeskImpl;->updateSoundModeSetting(Lcom/konka/kkinterface/tv/DataBaseDesk$SoundModeSeting;I)V

    const/4 v0, 0x1

    return v0
.end method

.method public setEqBand5k(S)Z
    .locals 5
    .param p1    # S

    iget-object v0, p0, Lcom/konka/kkimplements/tv/mstar/SoundDeskImpl;->databaseMgr:Lcom/konka/kkimplements/tv/mstar/DataBaseDeskImpl;

    invoke-virtual {v0}, Lcom/konka/kkimplements/tv/mstar/DataBaseDeskImpl;->querySoundSetting()Lcom/konka/kkinterface/tv/DataBaseDesk$MS_USER_SOUND_SETTING;

    iget-object v0, p0, Lcom/konka/kkimplements/tv/mstar/SoundDeskImpl;->databaseMgr:Lcom/konka/kkimplements/tv/mstar/DataBaseDeskImpl;

    invoke-virtual {v0}, Lcom/konka/kkimplements/tv/mstar/DataBaseDeskImpl;->getSound()Lcom/konka/kkinterface/tv/DataBaseDesk$MS_USER_SOUND_SETTING;

    move-result-object v0

    iput-object v0, p0, Lcom/konka/kkimplements/tv/mstar/SoundDeskImpl;->userSoundSetting:Lcom/konka/kkinterface/tv/DataBaseDesk$MS_USER_SOUND_SETTING;

    iget-object v0, p0, Lcom/konka/kkimplements/tv/mstar/SoundDeskImpl;->databaseMgr:Lcom/konka/kkimplements/tv/mstar/DataBaseDeskImpl;

    iget-object v1, p0, Lcom/konka/kkimplements/tv/mstar/SoundDeskImpl;->userSoundSetting:Lcom/konka/kkinterface/tv/DataBaseDesk$MS_USER_SOUND_SETTING;

    iget-object v1, v1, Lcom/konka/kkinterface/tv/DataBaseDesk$MS_USER_SOUND_SETTING;->SoundMode:Lcom/konka/kkinterface/tv/DataBaseDesk$EN_SOUND_MODE;

    invoke-virtual {v0, v1}, Lcom/konka/kkimplements/tv/mstar/DataBaseDeskImpl;->getSoundMode(Lcom/konka/kkinterface/tv/DataBaseDesk$EN_SOUND_MODE;)Lcom/konka/kkinterface/tv/DataBaseDesk$SoundModeSeting;

    move-result-object v0

    iput-short p1, v0, Lcom/konka/kkinterface/tv/DataBaseDesk$SoundModeSeting;->EqBand4:S

    iget-object v0, p0, Lcom/konka/kkimplements/tv/mstar/SoundDeskImpl;->com:Lcom/konka/kkinterface/tv/CommonDesk;

    const-string v1, "TvSoundServiceImpl"

    new-instance v2, Ljava/lang/StringBuilder;

    const-string v3, "EqBand4 is"

    invoke-direct {v2, v3}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    iget-object v3, p0, Lcom/konka/kkimplements/tv/mstar/SoundDeskImpl;->databaseMgr:Lcom/konka/kkimplements/tv/mstar/DataBaseDeskImpl;

    iget-object v4, p0, Lcom/konka/kkimplements/tv/mstar/SoundDeskImpl;->userSoundSetting:Lcom/konka/kkinterface/tv/DataBaseDesk$MS_USER_SOUND_SETTING;

    iget-object v4, v4, Lcom/konka/kkinterface/tv/DataBaseDesk$MS_USER_SOUND_SETTING;->SoundMode:Lcom/konka/kkinterface/tv/DataBaseDesk$EN_SOUND_MODE;

    invoke-virtual {v3, v4}, Lcom/konka/kkimplements/tv/mstar/DataBaseDeskImpl;->getSoundMode(Lcom/konka/kkinterface/tv/DataBaseDesk$EN_SOUND_MODE;)Lcom/konka/kkinterface/tv/DataBaseDesk$SoundModeSeting;

    move-result-object v3

    iget-short v3, v3, Lcom/konka/kkinterface/tv/DataBaseDesk$SoundModeSeting;->EqBand4:S

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-interface {v0, v1, v2}, Lcom/konka/kkinterface/tv/CommonDesk;->printfI(Ljava/lang/String;Ljava/lang/String;)V

    iget-object v0, p0, Lcom/konka/kkimplements/tv/mstar/SoundDeskImpl;->userSoundSetting:Lcom/konka/kkinterface/tv/DataBaseDesk$MS_USER_SOUND_SETTING;

    iget-object v0, v0, Lcom/konka/kkinterface/tv/DataBaseDesk$MS_USER_SOUND_SETTING;->SoundMode:Lcom/konka/kkinterface/tv/DataBaseDesk$EN_SOUND_MODE;

    invoke-virtual {p0, v0}, Lcom/konka/kkimplements/tv/mstar/SoundDeskImpl;->setSoundMode(Lcom/konka/kkinterface/tv/DataBaseDesk$EN_SOUND_MODE;)Z

    iget-object v0, p0, Lcom/konka/kkimplements/tv/mstar/SoundDeskImpl;->databaseMgr:Lcom/konka/kkimplements/tv/mstar/DataBaseDeskImpl;

    iget-object v1, p0, Lcom/konka/kkimplements/tv/mstar/SoundDeskImpl;->databaseMgr:Lcom/konka/kkimplements/tv/mstar/DataBaseDeskImpl;

    iget-object v2, p0, Lcom/konka/kkimplements/tv/mstar/SoundDeskImpl;->userSoundSetting:Lcom/konka/kkinterface/tv/DataBaseDesk$MS_USER_SOUND_SETTING;

    iget-object v2, v2, Lcom/konka/kkinterface/tv/DataBaseDesk$MS_USER_SOUND_SETTING;->SoundMode:Lcom/konka/kkinterface/tv/DataBaseDesk$EN_SOUND_MODE;

    invoke-virtual {v1, v2}, Lcom/konka/kkimplements/tv/mstar/DataBaseDeskImpl;->getSoundMode(Lcom/konka/kkinterface/tv/DataBaseDesk$EN_SOUND_MODE;)Lcom/konka/kkinterface/tv/DataBaseDesk$SoundModeSeting;

    move-result-object v1

    iget-object v2, p0, Lcom/konka/kkimplements/tv/mstar/SoundDeskImpl;->userSoundSetting:Lcom/konka/kkinterface/tv/DataBaseDesk$MS_USER_SOUND_SETTING;

    iget-object v2, v2, Lcom/konka/kkinterface/tv/DataBaseDesk$MS_USER_SOUND_SETTING;->SoundMode:Lcom/konka/kkinterface/tv/DataBaseDesk$EN_SOUND_MODE;

    invoke-virtual {v2}, Lcom/konka/kkinterface/tv/DataBaseDesk$EN_SOUND_MODE;->ordinal()I

    move-result v2

    invoke-virtual {v0, v1, v2}, Lcom/konka/kkimplements/tv/mstar/DataBaseDeskImpl;->updateSoundModeSetting(Lcom/konka/kkinterface/tv/DataBaseDesk$SoundModeSeting;I)V

    const/4 v0, 0x1

    return v0
.end method

.method public setHdmiAudioSource(Lcom/mstar/android/tvapi/common/vo/TvOsType$EnumInputSource;Lcom/konka/kkinterface/tv/DataBaseDesk$HdmiAudioSource;)Z
    .locals 6
    .param p1    # Lcom/mstar/android/tvapi/common/vo/TvOsType$EnumInputSource;
    .param p2    # Lcom/konka/kkinterface/tv/DataBaseDesk$HdmiAudioSource;

    iget-object v2, p0, Lcom/konka/kkimplements/tv/mstar/SoundDeskImpl;->databaseMgr:Lcom/konka/kkimplements/tv/mstar/DataBaseDeskImpl;

    invoke-virtual {v2}, Lcom/konka/kkimplements/tv/mstar/DataBaseDeskImpl;->querySoundSetting()Lcom/konka/kkinterface/tv/DataBaseDesk$MS_USER_SOUND_SETTING;

    iget-object v2, p0, Lcom/konka/kkimplements/tv/mstar/SoundDeskImpl;->databaseMgr:Lcom/konka/kkimplements/tv/mstar/DataBaseDeskImpl;

    invoke-virtual {v2}, Lcom/konka/kkimplements/tv/mstar/DataBaseDeskImpl;->getSound()Lcom/konka/kkinterface/tv/DataBaseDesk$MS_USER_SOUND_SETTING;

    move-result-object v2

    iput-object v2, p0, Lcom/konka/kkimplements/tv/mstar/SoundDeskImpl;->userSoundSetting:Lcom/konka/kkinterface/tv/DataBaseDesk$MS_USER_SOUND_SETTING;

    invoke-static {}, Lcom/konka/kkimplements/tv/mstar/SoundDeskImpl;->$SWITCH_TABLE$com$mstar$android$tvapi$common$vo$TvOsType$EnumInputSource()[I

    move-result-object v2

    invoke-virtual {p1}, Lcom/mstar/android/tvapi/common/vo/TvOsType$EnumInputSource;->ordinal()I

    move-result v3

    aget v2, v2, v3

    packed-switch v2, :pswitch_data_0

    const/4 v2, 0x0

    :goto_0
    return v2

    :pswitch_0
    iget-object v2, p0, Lcom/konka/kkimplements/tv/mstar/SoundDeskImpl;->userSoundSetting:Lcom/konka/kkinterface/tv/DataBaseDesk$MS_USER_SOUND_SETTING;

    iput-object p2, v2, Lcom/konka/kkinterface/tv/DataBaseDesk$MS_USER_SOUND_SETTING;->hdmi1AudioSource:Lcom/konka/kkinterface/tv/DataBaseDesk$HdmiAudioSource;

    :goto_1
    iget-object v2, p0, Lcom/konka/kkimplements/tv/mstar/SoundDeskImpl;->com:Lcom/konka/kkinterface/tv/CommonDesk;

    const-string v3, "TvSoundServiceImpl"

    new-instance v4, Ljava/lang/StringBuilder;

    const-string v5, "hdmiAudioSource is"

    invoke-direct {v4, v5}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v4, p2}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-interface {v2, v3, v4}, Lcom/konka/kkinterface/tv/CommonDesk;->printfI(Ljava/lang/String;Ljava/lang/String;)V

    iget-object v2, p0, Lcom/konka/kkimplements/tv/mstar/SoundDeskImpl;->databaseMgr:Lcom/konka/kkimplements/tv/mstar/DataBaseDeskImpl;

    iget-object v3, p0, Lcom/konka/kkimplements/tv/mstar/SoundDeskImpl;->userSoundSetting:Lcom/konka/kkinterface/tv/DataBaseDesk$MS_USER_SOUND_SETTING;

    invoke-virtual {v2, v3}, Lcom/konka/kkimplements/tv/mstar/DataBaseDeskImpl;->updateSoundSetting(Lcom/konka/kkinterface/tv/DataBaseDesk$MS_USER_SOUND_SETTING;)V

    :try_start_0
    sget-object v2, Lcom/konka/kkinterface/tv/DataBaseDesk$HdmiAudioSource;->AUDIO_SOURCE_VGA:Lcom/konka/kkinterface/tv/DataBaseDesk$HdmiAudioSource;

    if-ne p2, v2, :cond_1

    sget-object v0, Lcom/mstar/android/tvapi/common/vo/TvOsType$EnumInputSource;->E_INPUT_SOURCE_VGA:Lcom/mstar/android/tvapi/common/vo/TvOsType$EnumInputSource;

    :goto_2
    invoke-static {}, Lcom/mstar/android/tvapi/common/TvManager;->getInstance()Lcom/mstar/android/tvapi/common/TvManager;

    move-result-object v2

    if-eqz v2, :cond_0

    invoke-static {}, Lcom/mstar/android/tvapi/common/TvManager;->getInstance()Lcom/mstar/android/tvapi/common/TvManager;

    move-result-object v2

    invoke-virtual {v2}, Lcom/mstar/android/tvapi/common/TvManager;->getAudioManager()Lcom/mstar/android/tvapi/common/AudioManager;

    move-result-object v2

    invoke-virtual {v2, v0}, Lcom/mstar/android/tvapi/common/AudioManager;->setInputSource(Lcom/mstar/android/tvapi/common/vo/TvOsType$EnumInputSource;)V
    :try_end_0
    .catch Lcom/mstar/android/tvapi/common/exception/TvCommonException; {:try_start_0 .. :try_end_0} :catch_0

    :cond_0
    :goto_3
    const/4 v2, 0x1

    goto :goto_0

    :pswitch_1
    iget-object v2, p0, Lcom/konka/kkimplements/tv/mstar/SoundDeskImpl;->userSoundSetting:Lcom/konka/kkinterface/tv/DataBaseDesk$MS_USER_SOUND_SETTING;

    iput-object p2, v2, Lcom/konka/kkinterface/tv/DataBaseDesk$MS_USER_SOUND_SETTING;->hdmi2AudioSource:Lcom/konka/kkinterface/tv/DataBaseDesk$HdmiAudioSource;

    goto :goto_1

    :pswitch_2
    iget-object v2, p0, Lcom/konka/kkimplements/tv/mstar/SoundDeskImpl;->userSoundSetting:Lcom/konka/kkinterface/tv/DataBaseDesk$MS_USER_SOUND_SETTING;

    iput-object p2, v2, Lcom/konka/kkinterface/tv/DataBaseDesk$MS_USER_SOUND_SETTING;->hdmi3AudioSource:Lcom/konka/kkinterface/tv/DataBaseDesk$HdmiAudioSource;

    goto :goto_1

    :pswitch_3
    iget-object v2, p0, Lcom/konka/kkimplements/tv/mstar/SoundDeskImpl;->userSoundSetting:Lcom/konka/kkinterface/tv/DataBaseDesk$MS_USER_SOUND_SETTING;

    iput-object p2, v2, Lcom/konka/kkinterface/tv/DataBaseDesk$MS_USER_SOUND_SETTING;->hdmi4AudioSource:Lcom/konka/kkinterface/tv/DataBaseDesk$HdmiAudioSource;

    goto :goto_1

    :cond_1
    move-object v0, p1

    goto :goto_2

    :catch_0
    move-exception v1

    iget-object v2, p0, Lcom/konka/kkimplements/tv/mstar/SoundDeskImpl;->com:Lcom/konka/kkinterface/tv/CommonDesk;

    const-string v3, "TvSoundServiceImpl"

    const-string v4, "Set hdmiAudioSource Exception"

    invoke-interface {v2, v3, v4}, Lcom/konka/kkinterface/tv/CommonDesk;->printfE(Ljava/lang/String;Ljava/lang/String;)V

    invoke-virtual {v1}, Lcom/mstar/android/tvapi/common/exception/TvCommonException;->printStackTrace()V

    goto :goto_3

    nop

    :pswitch_data_0
    .packed-switch 0x18
        :pswitch_0
        :pswitch_1
        :pswitch_2
        :pswitch_3
    .end packed-switch
.end method

.method public setKaraADC()Z
    .locals 5

    :try_start_0
    sget-object v1, Ljava/lang/System;->out:Ljava/io/PrintStream;

    const-string v2, "setKaraMicNR"

    const/4 v3, 0x0

    new-array v3, v3, [Ljava/lang/Object;

    invoke-virtual {v1, v2, v3}, Ljava/io/PrintStream;->printf(Ljava/lang/String;[Ljava/lang/Object;)Ljava/io/PrintStream;

    invoke-static {}, Lcom/mstar/android/tvapi/common/TvManager;->getInstance()Lcom/mstar/android/tvapi/common/TvManager;

    move-result-object v1

    if-eqz v1, :cond_0

    invoke-static {}, Lcom/mstar/android/tvapi/common/TvManager;->getInstance()Lcom/mstar/android/tvapi/common/TvManager;

    move-result-object v1

    invoke-virtual {v1}, Lcom/mstar/android/tvapi/common/TvManager;->getAudioManager()Lcom/mstar/android/tvapi/common/AudioManager;

    move-result-object v1

    sget-object v2, Lcom/mstar/android/tvapi/common/vo/KtvInfoType$EnumKtvInfoType;->E_KTV_SETINFO_ADC_GAIN:Lcom/mstar/android/tvapi/common/vo/KtvInfoType$EnumKtvInfoType;

    const/4 v3, 0x6

    const/4 v4, 0x0

    invoke-virtual {v1, v2, v3, v4}, Lcom/mstar/android/tvapi/common/AudioManager;->setKtvSoundInfo(Lcom/mstar/android/tvapi/common/vo/KtvInfoType$EnumKtvInfoType;II)S

    invoke-static {}, Lcom/mstar/android/tvapi/common/TvManager;->getInstance()Lcom/mstar/android/tvapi/common/TvManager;

    move-result-object v1

    invoke-virtual {v1}, Lcom/mstar/android/tvapi/common/TvManager;->getAudioManager()Lcom/mstar/android/tvapi/common/AudioManager;

    move-result-object v1

    sget-object v2, Lcom/mstar/android/tvapi/common/vo/KtvInfoType$EnumKtvInfoType;->E_KTV_SETINFO_ADC1_GAIN:Lcom/mstar/android/tvapi/common/vo/KtvInfoType$EnumKtvInfoType;

    const/4 v3, 0x6

    const/4 v4, 0x0

    invoke-virtual {v1, v2, v3, v4}, Lcom/mstar/android/tvapi/common/AudioManager;->setKtvSoundInfo(Lcom/mstar/android/tvapi/common/vo/KtvInfoType$EnumKtvInfoType;II)S
    :try_end_0
    .catch Lcom/mstar/android/tvapi/common/exception/TvCommonException; {:try_start_0 .. :try_end_0} :catch_0

    :cond_0
    :goto_0
    const/4 v1, 0x1

    return v1

    :catch_0
    move-exception v0

    invoke-virtual {v0}, Lcom/mstar/android/tvapi/common/exception/TvCommonException;->printStackTrace()V

    goto :goto_0
.end method

.method public setKaraAudioTrack(I)Z
    .locals 4
    .param p1    # I

    sget-object v1, Ljava/lang/System;->out:Ljava/io/PrintStream;

    const-string v2, "setKaraAudioMode SoundDeskImpl"

    invoke-virtual {v1, v2}, Ljava/io/PrintStream;->println(Ljava/lang/String;)V

    :try_start_0
    invoke-static {}, Lcom/mstar/android/tvapi/common/TvManager;->getInstance()Lcom/mstar/android/tvapi/common/TvManager;

    move-result-object v1

    if-eqz v1, :cond_0

    invoke-static {}, Lcom/mstar/android/tvapi/common/TvManager;->getInstance()Lcom/mstar/android/tvapi/common/TvManager;

    move-result-object v1

    invoke-virtual {v1}, Lcom/mstar/android/tvapi/common/TvManager;->getAudioManager()Lcom/mstar/android/tvapi/common/AudioManager;

    move-result-object v1

    sget-object v2, Lcom/mstar/android/tvapi/common/vo/KtvInfoType$EnumKtvInfoType;->E_KTV_SETINFO_BG_MUSIC_SOUNDMODE:Lcom/mstar/android/tvapi/common/vo/KtvInfoType$EnumKtvInfoType;

    const/4 v3, 0x0

    invoke-virtual {v1, v2, p1, v3}, Lcom/mstar/android/tvapi/common/AudioManager;->setKtvSoundInfo(Lcom/mstar/android/tvapi/common/vo/KtvInfoType$EnumKtvInfoType;II)S
    :try_end_0
    .catch Lcom/mstar/android/tvapi/common/exception/TvCommonException; {:try_start_0 .. :try_end_0} :catch_0

    :cond_0
    :goto_0
    const/4 v1, 0x1

    return v1

    :catch_0
    move-exception v0

    invoke-virtual {v0}, Lcom/mstar/android/tvapi/common/exception/TvCommonException;->printStackTrace()V

    goto :goto_0
.end method

.method public setKaraMicNR(I)Z
    .locals 4
    .param p1    # I

    const/4 v3, 0x0

    sget-object v0, Ljava/lang/System;->out:Ljava/io/PrintStream;

    const-string v1, "setKaraMicNR"

    new-array v2, v3, [Ljava/lang/Object;

    invoke-virtual {v0, v1, v2}, Ljava/io/PrintStream;->printf(Ljava/lang/String;[Ljava/lang/Object;)Ljava/io/PrintStream;

    invoke-static {}, Lcom/mstar/android/tvapi/common/TvManager;->getInstance()Lcom/mstar/android/tvapi/common/TvManager;

    move-result-object v0

    if-eqz v0, :cond_0

    invoke-static {}, Lcom/mstar/android/tvapi/common/TvManager;->getInstance()Lcom/mstar/android/tvapi/common/TvManager;

    move-result-object v0

    invoke-virtual {v0}, Lcom/mstar/android/tvapi/common/TvManager;->getAudioManager()Lcom/mstar/android/tvapi/common/AudioManager;

    move-result-object v0

    sget-object v1, Lcom/mstar/android/tvapi/common/vo/EnumSoundSetParamType;->E_SOUND_SET_PARAM_NR_THRESHOLD_:Lcom/mstar/android/tvapi/common/vo/EnumSoundSetParamType;

    invoke-virtual {v0, v1, p1, v3}, Lcom/mstar/android/tvapi/common/AudioManager;->setSoundParameter(Lcom/mstar/android/tvapi/common/vo/EnumSoundSetParamType;II)S

    :cond_0
    const/4 v0, 0x1

    return v0
.end method

.method public setKaraMicVolume(S)Z
    .locals 4
    .param p1    # S

    sget-object v1, Ljava/lang/System;->out:Ljava/io/PrintStream;

    const-string v2, "setKaraMicVolume SoundDeskImpl before try"

    invoke-virtual {v1, v2}, Ljava/io/PrintStream;->println(Ljava/lang/String;)V

    :try_start_0
    sget-object v1, Ljava/lang/System;->out:Ljava/io/PrintStream;

    new-instance v2, Ljava/lang/StringBuilder;

    const-string v3, "setKaraVolume SoundDeskImpl micvolume = "

    invoke-direct {v2, v3}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v2, p1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/io/PrintStream;->println(Ljava/lang/String;)V

    invoke-static {}, Lcom/mstar/android/tvapi/common/TvManager;->getInstance()Lcom/mstar/android/tvapi/common/TvManager;

    move-result-object v1

    if-eqz v1, :cond_0

    invoke-static {}, Lcom/mstar/android/tvapi/common/TvManager;->getInstance()Lcom/mstar/android/tvapi/common/TvManager;

    move-result-object v1

    invoke-virtual {v1}, Lcom/mstar/android/tvapi/common/TvManager;->getAudioManager()Lcom/mstar/android/tvapi/common/AudioManager;

    move-result-object v1

    sget-object v2, Lcom/mstar/android/tvapi/common/vo/EnumAudioInputLevelSourceType;->E_VOL_SOURCE_PREMIXER_KTV_MIC_IN:Lcom/mstar/android/tvapi/common/vo/EnumAudioInputLevelSourceType;

    invoke-virtual {v1, v2, p1}, Lcom/mstar/android/tvapi/common/AudioManager;->setInputLevel(Lcom/mstar/android/tvapi/common/vo/EnumAudioInputLevelSourceType;S)V
    :try_end_0
    .catch Lcom/mstar/android/tvapi/common/exception/TvCommonException; {:try_start_0 .. :try_end_0} :catch_0

    :cond_0
    :goto_0
    const/4 v1, 0x1

    return v1

    :catch_0
    move-exception v0

    sget-object v1, Ljava/lang/System;->out:Ljava/io/PrintStream;

    const-string v2, "setKaraVolume SoundDeskImpl Error"

    invoke-virtual {v1, v2}, Ljava/io/PrintStream;->println(Ljava/lang/String;)V

    iget-object v1, p0, Lcom/konka/kkimplements/tv/mstar/SoundDeskImpl;->com:Lcom/konka/kkinterface/tv/CommonDesk;

    const-string v2, "TvSoundServiceImpl"

    const-string v3, "Set Volume Exception"

    invoke-interface {v1, v2, v3}, Lcom/konka/kkinterface/tv/CommonDesk;->printfE(Ljava/lang/String;Ljava/lang/String;)V

    invoke-virtual {v0}, Lcom/mstar/android/tvapi/common/exception/TvCommonException;->printStackTrace()V

    goto :goto_0
.end method

.method public setKaraMixEffect(Z)Z
    .locals 4
    .param p1    # Z

    :try_start_0
    sget-object v1, Ljava/lang/System;->out:Ljava/io/PrintStream;

    const-string v2, "setKaraAudioMode SoundDeskImpl"

    invoke-virtual {v1, v2}, Ljava/io/PrintStream;->println(Ljava/lang/String;)V

    invoke-static {}, Lcom/mstar/android/tvapi/common/TvManager;->getInstance()Lcom/mstar/android/tvapi/common/TvManager;

    move-result-object v1

    if-eqz v1, :cond_0

    invoke-static {}, Lcom/mstar/android/tvapi/common/TvManager;->getInstance()Lcom/mstar/android/tvapi/common/TvManager;

    move-result-object v1

    invoke-virtual {v1}, Lcom/mstar/android/tvapi/common/TvManager;->getAudioManager()Lcom/mstar/android/tvapi/common/AudioManager;

    move-result-object v1

    sget-object v2, Lcom/mstar/android/tvapi/common/vo/EnumSoundEffectType;->E_ECHO:Lcom/mstar/android/tvapi/common/vo/EnumSoundEffectType;

    invoke-virtual {v1, v2, p1}, Lcom/mstar/android/tvapi/common/AudioManager;->enableBasicSoundEffect(Lcom/mstar/android/tvapi/common/vo/EnumSoundEffectType;Z)Lcom/mstar/android/tvapi/common/vo/EnumAudioReturn;
    :try_end_0
    .catch Lcom/mstar/android/tvapi/common/exception/TvCommonException; {:try_start_0 .. :try_end_0} :catch_0

    :cond_0
    :goto_0
    const/4 v1, 0x1

    return v1

    :catch_0
    move-exception v0

    iget-object v1, p0, Lcom/konka/kkimplements/tv/mstar/SoundDeskImpl;->com:Lcom/konka/kkinterface/tv/CommonDesk;

    const-string v2, "TvSoundServiceImpl"

    const-string v3, "Set Volume Exception"

    invoke-interface {v1, v2, v3}, Lcom/konka/kkinterface/tv/CommonDesk;->printfE(Ljava/lang/String;Ljava/lang/String;)V

    invoke-virtual {v0}, Lcom/mstar/android/tvapi/common/exception/TvCommonException;->printStackTrace()V

    goto :goto_0
.end method

.method public setKaraMixVolume(S)Z
    .locals 4
    .param p1    # S

    :try_start_0
    invoke-static {}, Lcom/mstar/android/tvapi/common/TvManager;->getInstance()Lcom/mstar/android/tvapi/common/TvManager;

    move-result-object v1

    if-eqz v1, :cond_0

    invoke-static {}, Lcom/mstar/android/tvapi/common/TvManager;->getInstance()Lcom/mstar/android/tvapi/common/TvManager;

    move-result-object v1

    invoke-virtual {v1}, Lcom/mstar/android/tvapi/common/TvManager;->getAudioManager()Lcom/mstar/android/tvapi/common/AudioManager;

    move-result-object v1

    sget-object v2, Lcom/mstar/android/tvapi/common/vo/EnumAudioInputLevelSourceType;->E_VOL_SOURCE_PREMIXER_ECHO1_IN:Lcom/mstar/android/tvapi/common/vo/EnumAudioInputLevelSourceType;

    invoke-virtual {v1, v2, p1}, Lcom/mstar/android/tvapi/common/AudioManager;->setInputLevel(Lcom/mstar/android/tvapi/common/vo/EnumAudioInputLevelSourceType;S)V
    :try_end_0
    .catch Lcom/mstar/android/tvapi/common/exception/TvCommonException; {:try_start_0 .. :try_end_0} :catch_0

    :cond_0
    :goto_0
    const/4 v1, 0x1

    return v1

    :catch_0
    move-exception v0

    iget-object v1, p0, Lcom/konka/kkimplements/tv/mstar/SoundDeskImpl;->com:Lcom/konka/kkinterface/tv/CommonDesk;

    const-string v2, "TvSoundServiceImpl"

    const-string v3, "Set Volume Exception"

    invoke-interface {v1, v2, v3}, Lcom/konka/kkinterface/tv/CommonDesk;->printfE(Ljava/lang/String;Ljava/lang/String;)V

    invoke-virtual {v0}, Lcom/mstar/android/tvapi/common/exception/TvCommonException;->printStackTrace()V

    goto :goto_0
.end method

.method public setKaraSystemVolume(S)Z
    .locals 4
    .param p1    # S

    sget-object v1, Ljava/lang/System;->out:Ljava/io/PrintStream;

    const-string v2, "setKaraSystemVolume SoundDeskImpl before try"

    invoke-virtual {v1, v2}, Ljava/io/PrintStream;->println(Ljava/lang/String;)V

    :try_start_0
    sget-object v1, Ljava/lang/System;->out:Ljava/io/PrintStream;

    new-instance v2, Ljava/lang/StringBuilder;

    const-string v3, "setKaraSystemVolume SoundDeskImpl accvolume = "

    invoke-direct {v2, v3}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v2, p1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/io/PrintStream;->println(Ljava/lang/String;)V

    invoke-static {}, Lcom/mstar/android/tvapi/common/TvManager;->getInstance()Lcom/mstar/android/tvapi/common/TvManager;

    move-result-object v1

    if-eqz v1, :cond_0

    invoke-static {}, Lcom/mstar/android/tvapi/common/TvManager;->getInstance()Lcom/mstar/android/tvapi/common/TvManager;

    move-result-object v1

    invoke-virtual {v1}, Lcom/mstar/android/tvapi/common/TvManager;->getAudioManager()Lcom/mstar/android/tvapi/common/AudioManager;

    move-result-object v1

    sget-object v2, Lcom/mstar/android/tvapi/common/vo/EnumAudioVolumeSourceType;->E_VOL_SOURCE_SPEAKER_OUT:Lcom/mstar/android/tvapi/common/vo/EnumAudioVolumeSourceType;

    int-to-byte v3, p1

    invoke-virtual {v1, v2, v3}, Lcom/mstar/android/tvapi/common/AudioManager;->setAudioVolume(Lcom/mstar/android/tvapi/common/vo/EnumAudioVolumeSourceType;B)V
    :try_end_0
    .catch Lcom/mstar/android/tvapi/common/exception/TvCommonException; {:try_start_0 .. :try_end_0} :catch_0

    :cond_0
    :goto_0
    const/4 v1, 0x1

    return v1

    :catch_0
    move-exception v0

    sget-object v1, Ljava/lang/System;->out:Ljava/io/PrintStream;

    const-string v2, "setKaraSystemVolume SoundDeskImpl Error"

    invoke-virtual {v1, v2}, Ljava/io/PrintStream;->println(Ljava/lang/String;)V

    iget-object v1, p0, Lcom/konka/kkimplements/tv/mstar/SoundDeskImpl;->com:Lcom/konka/kkinterface/tv/CommonDesk;

    const-string v2, "TvSoundServiceImpl"

    const-string v3, "Set Volume Exception"

    invoke-interface {v1, v2, v3}, Lcom/konka/kkinterface/tv/CommonDesk;->printfE(Ljava/lang/String;Ljava/lang/String;)V

    invoke-virtual {v0}, Lcom/mstar/android/tvapi/common/exception/TvCommonException;->printStackTrace()V

    goto :goto_0
.end method

.method public setKaraVolume(S)Z
    .locals 4
    .param p1    # S

    sget-object v1, Ljava/lang/System;->out:Ljava/io/PrintStream;

    const-string v2, "setKaraVolume SoundDeskImpl before try"

    invoke-virtual {v1, v2}, Ljava/io/PrintStream;->println(Ljava/lang/String;)V

    :try_start_0
    sget-object v1, Ljava/lang/System;->out:Ljava/io/PrintStream;

    new-instance v2, Ljava/lang/StringBuilder;

    const-string v3, "setKaraVolume SoundDeskImpl accvolume = "

    invoke-direct {v2, v3}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v2, p1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/io/PrintStream;->println(Ljava/lang/String;)V

    invoke-static {}, Lcom/mstar/android/tvapi/common/TvManager;->getInstance()Lcom/mstar/android/tvapi/common/TvManager;

    move-result-object v1

    if-eqz v1, :cond_0

    invoke-static {}, Lcom/mstar/android/tvapi/common/TvManager;->getInstance()Lcom/mstar/android/tvapi/common/TvManager;

    move-result-object v1

    invoke-virtual {v1}, Lcom/mstar/android/tvapi/common/TvManager;->getAudioManager()Lcom/mstar/android/tvapi/common/AudioManager;

    move-result-object v1

    sget-object v2, Lcom/mstar/android/tvapi/common/vo/EnumAudioInputLevelSourceType;->E_VOL_SOURCE_PREMIXER_KTV_MP3_IN:Lcom/mstar/android/tvapi/common/vo/EnumAudioInputLevelSourceType;

    invoke-virtual {v1, v2, p1}, Lcom/mstar/android/tvapi/common/AudioManager;->setInputLevel(Lcom/mstar/android/tvapi/common/vo/EnumAudioInputLevelSourceType;S)V
    :try_end_0
    .catch Lcom/mstar/android/tvapi/common/exception/TvCommonException; {:try_start_0 .. :try_end_0} :catch_0

    :cond_0
    :goto_0
    const/4 v1, 0x1

    return v1

    :catch_0
    move-exception v0

    sget-object v1, Ljava/lang/System;->out:Ljava/io/PrintStream;

    const-string v2, "setKaraVolume SoundDeskImpl Error"

    invoke-virtual {v1, v2}, Ljava/io/PrintStream;->println(Ljava/lang/String;)V

    iget-object v1, p0, Lcom/konka/kkimplements/tv/mstar/SoundDeskImpl;->com:Lcom/konka/kkinterface/tv/CommonDesk;

    const-string v2, "TvSoundServiceImpl"

    const-string v3, "Set Volume Exception"

    invoke-interface {v1, v2, v3}, Lcom/konka/kkinterface/tv/CommonDesk;->printfE(Ljava/lang/String;Ljava/lang/String;)V

    invoke-virtual {v0}, Lcom/mstar/android/tvapi/common/exception/TvCommonException;->printStackTrace()V

    goto :goto_0
.end method

.method public setMuteFlag(Z)Z
    .locals 4
    .param p1    # Z

    if-eqz p1, :cond_1

    :try_start_0
    invoke-static {}, Lcom/mstar/android/tvapi/common/TvManager;->getInstance()Lcom/mstar/android/tvapi/common/TvManager;

    move-result-object v1

    if-eqz v1, :cond_0

    invoke-static {}, Lcom/mstar/android/tvapi/common/TvManager;->getInstance()Lcom/mstar/android/tvapi/common/TvManager;

    move-result-object v1

    invoke-virtual {v1}, Lcom/mstar/android/tvapi/common/TvManager;->getAudioManager()Lcom/mstar/android/tvapi/common/AudioManager;

    move-result-object v1

    sget-object v2, Lcom/mstar/android/tvapi/common/vo/MuteType$EnumMuteType;->E_BYUSER:Lcom/mstar/android/tvapi/common/vo/MuteType$EnumMuteType;

    invoke-virtual {v1, v2}, Lcom/mstar/android/tvapi/common/AudioManager;->enableMute(Lcom/mstar/android/tvapi/common/vo/MuteType$EnumMuteType;)Lcom/mstar/android/tvapi/common/vo/EnumAudioReturn;
    :try_end_0
    .catch Lcom/mstar/android/tvapi/common/exception/TvCommonException; {:try_start_0 .. :try_end_0} :catch_0

    :cond_0
    :goto_0
    const-string v1, "SoundDeskImpl"

    new-instance v2, Ljava/lang/StringBuilder;

    const-string v3, "set muteFlag is"

    invoke-direct {v2, v3}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v2, p1}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    const/4 v1, 0x1

    return v1

    :catch_0
    move-exception v0

    invoke-virtual {v0}, Lcom/mstar/android/tvapi/common/exception/TvCommonException;->printStackTrace()V

    goto :goto_0

    :cond_1
    :try_start_1
    invoke-static {}, Lcom/mstar/android/tvapi/common/TvManager;->getInstance()Lcom/mstar/android/tvapi/common/TvManager;

    move-result-object v1

    if-eqz v1, :cond_0

    invoke-static {}, Lcom/mstar/android/tvapi/common/TvManager;->getInstance()Lcom/mstar/android/tvapi/common/TvManager;

    move-result-object v1

    invoke-virtual {v1}, Lcom/mstar/android/tvapi/common/TvManager;->getAudioManager()Lcom/mstar/android/tvapi/common/AudioManager;

    move-result-object v1

    sget-object v2, Lcom/mstar/android/tvapi/common/vo/MuteType$EnumMuteType;->E_BYUSER:Lcom/mstar/android/tvapi/common/vo/MuteType$EnumMuteType;

    invoke-virtual {v1, v2}, Lcom/mstar/android/tvapi/common/AudioManager;->disableMute(Lcom/mstar/android/tvapi/common/vo/MuteType$EnumMuteType;)Lcom/mstar/android/tvapi/common/vo/EnumAudioReturn;
    :try_end_1
    .catch Lcom/mstar/android/tvapi/common/exception/TvCommonException; {:try_start_1 .. :try_end_1} :catch_1

    goto :goto_0

    :catch_1
    move-exception v0

    invoke-virtual {v0}, Lcom/mstar/android/tvapi/common/exception/TvCommonException;->printStackTrace()V

    goto :goto_0
.end method

.method public setSRSDefination(Z)V
    .locals 5
    .param p1    # Z

    if-eqz p1, :cond_2

    :try_start_0
    const-string v2, "KKJAVAAPI"

    const-string v3, "set SRS Defination on!"

    invoke-static {v2, v3}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    invoke-static {}, Lcom/mstar/android/tvapi/common/TvManager;->getInstance()Lcom/mstar/android/tvapi/common/TvManager;

    move-result-object v2

    if-eqz v2, :cond_0

    invoke-static {}, Lcom/mstar/android/tvapi/common/TvManager;->getInstance()Lcom/mstar/android/tvapi/common/TvManager;

    move-result-object v2

    invoke-virtual {v2}, Lcom/mstar/android/tvapi/common/TvManager;->getAudioManager()Lcom/mstar/android/tvapi/common/AudioManager;

    move-result-object v2

    sget-object v3, Lcom/mstar/android/tvapi/common/vo/EnumAdvancedSoundType;->E_SRS_TSHD:Lcom/mstar/android/tvapi/common/vo/EnumAdvancedSoundType;

    sget-object v4, Lcom/mstar/android/tvapi/common/vo/EnumAdvancedSoundSubProcessType;->E_SRS_THEATERSOUND_DEFINITION_ON:Lcom/mstar/android/tvapi/common/vo/EnumAdvancedSoundSubProcessType;

    invoke-virtual {v2, v3, v4}, Lcom/mstar/android/tvapi/common/AudioManager;->enableAdvancedSoundEffect(Lcom/mstar/android/tvapi/common/vo/EnumAdvancedSoundType;Lcom/mstar/android/tvapi/common/vo/EnumAdvancedSoundSubProcessType;)Lcom/mstar/android/tvapi/common/vo/EnumAudioReturn;

    :cond_0
    iget-object v2, p0, Lcom/konka/kkimplements/tv/mstar/SoundDeskImpl;->databaseMgr:Lcom/konka/kkimplements/tv/mstar/DataBaseDeskImpl;

    sget-object v3, Lcom/konka/kkinterface/tv/DataBaseDesk$EnumSwitchOnOff;->SWITCH_ON:Lcom/konka/kkinterface/tv/DataBaseDesk$EnumSwitchOnOff;

    invoke-virtual {v2, v3}, Lcom/konka/kkimplements/tv/mstar/DataBaseDeskImpl;->setDefinitionOnOff(Lcom/konka/kkinterface/tv/DataBaseDesk$EnumSwitchOnOff;)V

    new-instance v1, Lcom/mstar/android/tvapi/common/vo/AdvancedSoundParameter;

    invoke-direct {v1}, Lcom/mstar/android/tvapi/common/vo/AdvancedSoundParameter;-><init>()V

    const/16 v2, 0x12c

    iput v2, v1, Lcom/mstar/android/tvapi/common/vo/AdvancedSoundParameter;->paramSrsTheaterSoundDefinitionControl:I

    invoke-static {}, Lcom/mstar/android/tvapi/common/TvManager;->getInstance()Lcom/mstar/android/tvapi/common/TvManager;

    move-result-object v2

    if-eqz v2, :cond_1

    invoke-static {}, Lcom/mstar/android/tvapi/common/TvManager;->getInstance()Lcom/mstar/android/tvapi/common/TvManager;

    move-result-object v2

    invoke-virtual {v2}, Lcom/mstar/android/tvapi/common/TvManager;->getAudioManager()Lcom/mstar/android/tvapi/common/AudioManager;

    move-result-object v2

    sget-object v3, Lcom/mstar/android/tvapi/common/vo/EnumAdvancedSoundParameterType;->E_ADVSND_SRS_THEATERSOUND_DEFINITION_CONTROL:Lcom/mstar/android/tvapi/common/vo/EnumAdvancedSoundParameterType;

    invoke-virtual {v2, v3, v1}, Lcom/mstar/android/tvapi/common/AudioManager;->setAdvancedSoundEffect(Lcom/mstar/android/tvapi/common/vo/EnumAdvancedSoundParameterType;Lcom/mstar/android/tvapi/common/vo/AdvancedSoundParameter;)Lcom/mstar/android/tvapi/common/vo/EnumAudioReturn;

    :cond_1
    :goto_0
    return-void

    :cond_2
    const-string v2, "KKJAVAAPI"

    const-string v3, "set SRS Defination off"

    invoke-static {v2, v3}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    invoke-static {}, Lcom/mstar/android/tvapi/common/TvManager;->getInstance()Lcom/mstar/android/tvapi/common/TvManager;

    move-result-object v2

    if-eqz v2, :cond_3

    invoke-static {}, Lcom/mstar/android/tvapi/common/TvManager;->getInstance()Lcom/mstar/android/tvapi/common/TvManager;

    move-result-object v2

    invoke-virtual {v2}, Lcom/mstar/android/tvapi/common/TvManager;->getAudioManager()Lcom/mstar/android/tvapi/common/AudioManager;

    move-result-object v2

    sget-object v3, Lcom/mstar/android/tvapi/common/vo/EnumAdvancedSoundType;->E_SRS_TSHD:Lcom/mstar/android/tvapi/common/vo/EnumAdvancedSoundType;

    sget-object v4, Lcom/mstar/android/tvapi/common/vo/EnumAdvancedSoundSubProcessType;->E_SRS_THEATERSOUND_DEFINITION_OFF:Lcom/mstar/android/tvapi/common/vo/EnumAdvancedSoundSubProcessType;

    invoke-virtual {v2, v3, v4}, Lcom/mstar/android/tvapi/common/AudioManager;->enableAdvancedSoundEffect(Lcom/mstar/android/tvapi/common/vo/EnumAdvancedSoundType;Lcom/mstar/android/tvapi/common/vo/EnumAdvancedSoundSubProcessType;)Lcom/mstar/android/tvapi/common/vo/EnumAudioReturn;

    :cond_3
    iget-object v2, p0, Lcom/konka/kkimplements/tv/mstar/SoundDeskImpl;->databaseMgr:Lcom/konka/kkimplements/tv/mstar/DataBaseDeskImpl;

    sget-object v3, Lcom/konka/kkinterface/tv/DataBaseDesk$EnumSwitchOnOff;->SWITCH_OFF:Lcom/konka/kkinterface/tv/DataBaseDesk$EnumSwitchOnOff;

    invoke-virtual {v2, v3}, Lcom/konka/kkimplements/tv/mstar/DataBaseDeskImpl;->setDefinitionOnOff(Lcom/konka/kkinterface/tv/DataBaseDesk$EnumSwitchOnOff;)V
    :try_end_0
    .catch Lcom/mstar/android/tvapi/common/exception/TvCommonException; {:try_start_0 .. :try_end_0} :catch_0

    goto :goto_0

    :catch_0
    move-exception v0

    invoke-virtual {v0}, Lcom/mstar/android/tvapi/common/exception/TvCommonException;->printStackTrace()V

    goto :goto_0
.end method

.method public setSRSDynamicClarity(Z)V
    .locals 5
    .param p1    # Z

    if-eqz p1, :cond_2

    :try_start_0
    const-string v2, "KKJAVAAPI"

    const-string v3, "set SRS DynamicClarity on"

    invoke-static {v2, v3}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    invoke-static {}, Lcom/mstar/android/tvapi/common/TvManager;->getInstance()Lcom/mstar/android/tvapi/common/TvManager;

    move-result-object v2

    if-eqz v2, :cond_0

    invoke-static {}, Lcom/mstar/android/tvapi/common/TvManager;->getInstance()Lcom/mstar/android/tvapi/common/TvManager;

    move-result-object v2

    invoke-virtual {v2}, Lcom/mstar/android/tvapi/common/TvManager;->getAudioManager()Lcom/mstar/android/tvapi/common/AudioManager;

    move-result-object v2

    sget-object v3, Lcom/mstar/android/tvapi/common/vo/EnumAdvancedSoundType;->E_SRS_TSHD:Lcom/mstar/android/tvapi/common/vo/EnumAdvancedSoundType;

    sget-object v4, Lcom/mstar/android/tvapi/common/vo/EnumAdvancedSoundSubProcessType;->E_SRS_THEATERSOUND_DYNAMIC_CLARITY_ON:Lcom/mstar/android/tvapi/common/vo/EnumAdvancedSoundSubProcessType;

    invoke-virtual {v2, v3, v4}, Lcom/mstar/android/tvapi/common/AudioManager;->enableAdvancedSoundEffect(Lcom/mstar/android/tvapi/common/vo/EnumAdvancedSoundType;Lcom/mstar/android/tvapi/common/vo/EnumAdvancedSoundSubProcessType;)Lcom/mstar/android/tvapi/common/vo/EnumAudioReturn;

    :cond_0
    iget-object v2, p0, Lcom/konka/kkimplements/tv/mstar/SoundDeskImpl;->databaseMgr:Lcom/konka/kkimplements/tv/mstar/DataBaseDeskImpl;

    sget-object v3, Lcom/konka/kkinterface/tv/DataBaseDesk$EnumSwitchOnOff;->SWITCH_ON:Lcom/konka/kkinterface/tv/DataBaseDesk$EnumSwitchOnOff;

    invoke-virtual {v2, v3}, Lcom/konka/kkimplements/tv/mstar/DataBaseDeskImpl;->setDialogClarityOnOff(Lcom/konka/kkinterface/tv/DataBaseDesk$EnumSwitchOnOff;)V

    new-instance v1, Lcom/mstar/android/tvapi/common/vo/AdvancedSoundParameter;

    invoke-direct {v1}, Lcom/mstar/android/tvapi/common/vo/AdvancedSoundParameter;-><init>()V

    const/16 v2, 0x2bc

    iput v2, v1, Lcom/mstar/android/tvapi/common/vo/AdvancedSoundParameter;->paramSrsTheaterSoundDcControl:I

    invoke-static {}, Lcom/mstar/android/tvapi/common/TvManager;->getInstance()Lcom/mstar/android/tvapi/common/TvManager;

    move-result-object v2

    if-eqz v2, :cond_1

    invoke-static {}, Lcom/mstar/android/tvapi/common/TvManager;->getInstance()Lcom/mstar/android/tvapi/common/TvManager;

    move-result-object v2

    invoke-virtual {v2}, Lcom/mstar/android/tvapi/common/TvManager;->getAudioManager()Lcom/mstar/android/tvapi/common/AudioManager;

    move-result-object v2

    sget-object v3, Lcom/mstar/android/tvapi/common/vo/EnumAdvancedSoundParameterType;->E_ADVSND_SRS_THEATERSOUND_DC_CONTROL:Lcom/mstar/android/tvapi/common/vo/EnumAdvancedSoundParameterType;

    invoke-virtual {v2, v3, v1}, Lcom/mstar/android/tvapi/common/AudioManager;->setAdvancedSoundEffect(Lcom/mstar/android/tvapi/common/vo/EnumAdvancedSoundParameterType;Lcom/mstar/android/tvapi/common/vo/AdvancedSoundParameter;)Lcom/mstar/android/tvapi/common/vo/EnumAudioReturn;

    :cond_1
    :goto_0
    return-void

    :cond_2
    const-string v2, "KKJAVAAPI"

    const-string v3, "set SRS DynamicClarity off"

    invoke-static {v2, v3}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    invoke-static {}, Lcom/mstar/android/tvapi/common/TvManager;->getInstance()Lcom/mstar/android/tvapi/common/TvManager;

    move-result-object v2

    if-eqz v2, :cond_3

    invoke-static {}, Lcom/mstar/android/tvapi/common/TvManager;->getInstance()Lcom/mstar/android/tvapi/common/TvManager;

    move-result-object v2

    invoke-virtual {v2}, Lcom/mstar/android/tvapi/common/TvManager;->getAudioManager()Lcom/mstar/android/tvapi/common/AudioManager;

    move-result-object v2

    sget-object v3, Lcom/mstar/android/tvapi/common/vo/EnumAdvancedSoundType;->E_SRS_TSHD:Lcom/mstar/android/tvapi/common/vo/EnumAdvancedSoundType;

    sget-object v4, Lcom/mstar/android/tvapi/common/vo/EnumAdvancedSoundSubProcessType;->E_SRS_THEATERSOUND_DYNAMIC_CLARITY_OFF:Lcom/mstar/android/tvapi/common/vo/EnumAdvancedSoundSubProcessType;

    invoke-virtual {v2, v3, v4}, Lcom/mstar/android/tvapi/common/AudioManager;->enableAdvancedSoundEffect(Lcom/mstar/android/tvapi/common/vo/EnumAdvancedSoundType;Lcom/mstar/android/tvapi/common/vo/EnumAdvancedSoundSubProcessType;)Lcom/mstar/android/tvapi/common/vo/EnumAudioReturn;

    :cond_3
    iget-object v2, p0, Lcom/konka/kkimplements/tv/mstar/SoundDeskImpl;->databaseMgr:Lcom/konka/kkimplements/tv/mstar/DataBaseDeskImpl;

    sget-object v3, Lcom/konka/kkinterface/tv/DataBaseDesk$EnumSwitchOnOff;->SWITCH_OFF:Lcom/konka/kkinterface/tv/DataBaseDesk$EnumSwitchOnOff;

    invoke-virtual {v2, v3}, Lcom/konka/kkimplements/tv/mstar/DataBaseDeskImpl;->setDialogClarityOnOff(Lcom/konka/kkinterface/tv/DataBaseDesk$EnumSwitchOnOff;)V
    :try_end_0
    .catch Lcom/mstar/android/tvapi/common/exception/TvCommonException; {:try_start_0 .. :try_end_0} :catch_0

    goto :goto_0

    :catch_0
    move-exception v0

    invoke-virtual {v0}, Lcom/mstar/android/tvapi/common/exception/TvCommonException;->printStackTrace()V

    goto :goto_0
.end method

.method public setSRSPara(Lcom/konka/kkinterface/tv/DataBaseDesk$EN_SRS_SET_TYPE;I)V
    .locals 5
    .param p1    # Lcom/konka/kkinterface/tv/DataBaseDesk$EN_SRS_SET_TYPE;
    .param p2    # I

    new-instance v1, Lcom/mstar/android/tvapi/common/vo/AdvancedSoundParameter;

    invoke-direct {v1}, Lcom/mstar/android/tvapi/common/vo/AdvancedSoundParameter;-><init>()V

    new-instance v2, Lcom/konka/kkinterface/tv/DataBaseDesk$KK_SRS_SET;

    invoke-direct {v2}, Lcom/konka/kkinterface/tv/DataBaseDesk$KK_SRS_SET;-><init>()V

    sget-object v3, Lcom/konka/kkinterface/tv/DataBaseDesk$EN_SRS_SET_TYPE;->E_SRS_INPUTGAIN:Lcom/konka/kkinterface/tv/DataBaseDesk$EN_SRS_SET_TYPE;

    if-ne p1, v3, :cond_2

    iget-object v3, p0, Lcom/konka/kkimplements/tv/mstar/SoundDeskImpl;->databaseMgr:Lcom/konka/kkimplements/tv/mstar/DataBaseDeskImpl;

    invoke-virtual {v3}, Lcom/konka/kkimplements/tv/mstar/DataBaseDeskImpl;->getSRSSet()Lcom/konka/kkinterface/tv/DataBaseDesk$KK_SRS_SET;

    move-result-object v3

    iget v3, v3, Lcom/konka/kkinterface/tv/DataBaseDesk$KK_SRS_SET;->srs_InputGain:I

    mul-int/lit8 v3, v3, 0x64

    iput v3, v1, Lcom/mstar/android/tvapi/common/vo/AdvancedSoundParameter;->paramSrsTheaterSoundInputGain:I

    :try_start_0
    invoke-static {}, Lcom/mstar/android/tvapi/common/TvManager;->getInstance()Lcom/mstar/android/tvapi/common/TvManager;

    move-result-object v3

    if-eqz v3, :cond_0

    invoke-static {}, Lcom/mstar/android/tvapi/common/TvManager;->getInstance()Lcom/mstar/android/tvapi/common/TvManager;

    move-result-object v3

    invoke-virtual {v3}, Lcom/mstar/android/tvapi/common/TvManager;->getAudioManager()Lcom/mstar/android/tvapi/common/AudioManager;

    move-result-object v3

    sget-object v4, Lcom/mstar/android/tvapi/common/vo/EnumAdvancedSoundParameterType;->E_ADVSND_SRS_THEATERSOUND_INPUT_GAIN:Lcom/mstar/android/tvapi/common/vo/EnumAdvancedSoundParameterType;

    invoke-virtual {v3, v4, v1}, Lcom/mstar/android/tvapi/common/AudioManager;->setAdvancedSoundEffect(Lcom/mstar/android/tvapi/common/vo/EnumAdvancedSoundParameterType;Lcom/mstar/android/tvapi/common/vo/AdvancedSoundParameter;)Lcom/mstar/android/tvapi/common/vo/EnumAudioReturn;
    :try_end_0
    .catch Lcom/mstar/android/tvapi/common/exception/TvCommonException; {:try_start_0 .. :try_end_0} :catch_0

    :cond_0
    :goto_0
    iput p2, v2, Lcom/konka/kkinterface/tv/DataBaseDesk$KK_SRS_SET;->srs_InputGain:I

    iget-object v3, p0, Lcom/konka/kkimplements/tv/mstar/SoundDeskImpl;->databaseMgr:Lcom/konka/kkimplements/tv/mstar/DataBaseDeskImpl;

    invoke-virtual {v3, v2, p1}, Lcom/konka/kkimplements/tv/mstar/DataBaseDeskImpl;->setSRSSet(Lcom/konka/kkinterface/tv/DataBaseDesk$KK_SRS_SET;Lcom/konka/kkinterface/tv/DataBaseDesk$EN_SRS_SET_TYPE;)Z

    :cond_1
    :goto_1
    return-void

    :catch_0
    move-exception v0

    invoke-virtual {v0}, Lcom/mstar/android/tvapi/common/exception/TvCommonException;->printStackTrace()V

    goto :goto_0

    :cond_2
    sget-object v3, Lcom/konka/kkinterface/tv/DataBaseDesk$EN_SRS_SET_TYPE;->E_SRS_SURRLEVEL_CONTROL:Lcom/konka/kkinterface/tv/DataBaseDesk$EN_SRS_SET_TYPE;

    if-ne p1, v3, :cond_4

    iget-object v3, p0, Lcom/konka/kkimplements/tv/mstar/SoundDeskImpl;->databaseMgr:Lcom/konka/kkimplements/tv/mstar/DataBaseDeskImpl;

    invoke-virtual {v3}, Lcom/konka/kkimplements/tv/mstar/DataBaseDeskImpl;->getSRSSet()Lcom/konka/kkinterface/tv/DataBaseDesk$KK_SRS_SET;

    move-result-object v3

    iget v3, v3, Lcom/konka/kkinterface/tv/DataBaseDesk$KK_SRS_SET;->srs_SurrLevelControl:I

    mul-int/lit8 v3, v3, 0x64

    iput v3, v1, Lcom/mstar/android/tvapi/common/vo/AdvancedSoundParameter;->paramSrsTheaterasoundSurrLevelControl:I

    :try_start_1
    invoke-static {}, Lcom/mstar/android/tvapi/common/TvManager;->getInstance()Lcom/mstar/android/tvapi/common/TvManager;

    move-result-object v3

    if-eqz v3, :cond_3

    invoke-static {}, Lcom/mstar/android/tvapi/common/TvManager;->getInstance()Lcom/mstar/android/tvapi/common/TvManager;

    move-result-object v3

    invoke-virtual {v3}, Lcom/mstar/android/tvapi/common/TvManager;->getAudioManager()Lcom/mstar/android/tvapi/common/AudioManager;

    move-result-object v3

    sget-object v4, Lcom/mstar/android/tvapi/common/vo/EnumAdvancedSoundParameterType;->ADVSND_SRS_THEATERSOUND_SURR_LEVEL_CONTROL:Lcom/mstar/android/tvapi/common/vo/EnumAdvancedSoundParameterType;

    invoke-virtual {v3, v4, v1}, Lcom/mstar/android/tvapi/common/AudioManager;->setAdvancedSoundEffect(Lcom/mstar/android/tvapi/common/vo/EnumAdvancedSoundParameterType;Lcom/mstar/android/tvapi/common/vo/AdvancedSoundParameter;)Lcom/mstar/android/tvapi/common/vo/EnumAudioReturn;
    :try_end_1
    .catch Lcom/mstar/android/tvapi/common/exception/TvCommonException; {:try_start_1 .. :try_end_1} :catch_1

    :cond_3
    :goto_2
    iput p2, v2, Lcom/konka/kkinterface/tv/DataBaseDesk$KK_SRS_SET;->srs_SurrLevelControl:I

    iget-object v3, p0, Lcom/konka/kkimplements/tv/mstar/SoundDeskImpl;->databaseMgr:Lcom/konka/kkimplements/tv/mstar/DataBaseDeskImpl;

    invoke-virtual {v3, v2, p1}, Lcom/konka/kkimplements/tv/mstar/DataBaseDeskImpl;->setSRSSet(Lcom/konka/kkinterface/tv/DataBaseDesk$KK_SRS_SET;Lcom/konka/kkinterface/tv/DataBaseDesk$EN_SRS_SET_TYPE;)Z

    goto :goto_1

    :catch_1
    move-exception v0

    invoke-virtual {v0}, Lcom/mstar/android/tvapi/common/exception/TvCommonException;->printStackTrace()V

    goto :goto_2

    :cond_4
    sget-object v3, Lcom/konka/kkinterface/tv/DataBaseDesk$EN_SRS_SET_TYPE;->E_SRS_SPEAKERAUDIO:Lcom/konka/kkinterface/tv/DataBaseDesk$EN_SRS_SET_TYPE;

    if-ne p1, v3, :cond_6

    iget-object v3, p0, Lcom/konka/kkimplements/tv/mstar/SoundDeskImpl;->databaseMgr:Lcom/konka/kkimplements/tv/mstar/DataBaseDeskImpl;

    invoke-virtual {v3}, Lcom/konka/kkimplements/tv/mstar/DataBaseDeskImpl;->getSRSSet()Lcom/konka/kkinterface/tv/DataBaseDesk$KK_SRS_SET;

    move-result-object v3

    iget v3, v3, Lcom/konka/kkinterface/tv/DataBaseDesk$KK_SRS_SET;->srs_SpeakerAudio:I

    iput v3, v1, Lcom/mstar/android/tvapi/common/vo/AdvancedSoundParameter;->paramSrsTheaterasoundTrubassSpeakerAudio:I

    :try_start_2
    invoke-static {}, Lcom/mstar/android/tvapi/common/TvManager;->getInstance()Lcom/mstar/android/tvapi/common/TvManager;

    move-result-object v3

    if-eqz v3, :cond_5

    invoke-static {}, Lcom/mstar/android/tvapi/common/TvManager;->getInstance()Lcom/mstar/android/tvapi/common/TvManager;

    move-result-object v3

    invoke-virtual {v3}, Lcom/mstar/android/tvapi/common/TvManager;->getAudioManager()Lcom/mstar/android/tvapi/common/AudioManager;

    move-result-object v3

    sget-object v4, Lcom/mstar/android/tvapi/common/vo/EnumAdvancedSoundParameterType;->ADVSND_SRS_THEATERSOUND_TRUBASS_SPEAKER_AUDIO:Lcom/mstar/android/tvapi/common/vo/EnumAdvancedSoundParameterType;

    invoke-virtual {v3, v4, v1}, Lcom/mstar/android/tvapi/common/AudioManager;->setAdvancedSoundEffect(Lcom/mstar/android/tvapi/common/vo/EnumAdvancedSoundParameterType;Lcom/mstar/android/tvapi/common/vo/AdvancedSoundParameter;)Lcom/mstar/android/tvapi/common/vo/EnumAudioReturn;
    :try_end_2
    .catch Lcom/mstar/android/tvapi/common/exception/TvCommonException; {:try_start_2 .. :try_end_2} :catch_2

    :cond_5
    :goto_3
    iput p2, v2, Lcom/konka/kkinterface/tv/DataBaseDesk$KK_SRS_SET;->srs_SpeakerAudio:I

    iget-object v3, p0, Lcom/konka/kkimplements/tv/mstar/SoundDeskImpl;->databaseMgr:Lcom/konka/kkimplements/tv/mstar/DataBaseDeskImpl;

    invoke-virtual {v3, v2, p1}, Lcom/konka/kkimplements/tv/mstar/DataBaseDeskImpl;->setSRSSet(Lcom/konka/kkinterface/tv/DataBaseDesk$KK_SRS_SET;Lcom/konka/kkinterface/tv/DataBaseDesk$EN_SRS_SET_TYPE;)Z

    goto :goto_1

    :catch_2
    move-exception v0

    invoke-virtual {v0}, Lcom/mstar/android/tvapi/common/exception/TvCommonException;->printStackTrace()V

    goto :goto_3

    :cond_6
    sget-object v3, Lcom/konka/kkinterface/tv/DataBaseDesk$EN_SRS_SET_TYPE;->E_SRS_SPEAKERANALYSIS:Lcom/konka/kkinterface/tv/DataBaseDesk$EN_SRS_SET_TYPE;

    if-ne p1, v3, :cond_8

    iget-object v3, p0, Lcom/konka/kkimplements/tv/mstar/SoundDeskImpl;->databaseMgr:Lcom/konka/kkimplements/tv/mstar/DataBaseDeskImpl;

    invoke-virtual {v3}, Lcom/konka/kkimplements/tv/mstar/DataBaseDeskImpl;->getSRSSet()Lcom/konka/kkinterface/tv/DataBaseDesk$KK_SRS_SET;

    move-result-object v3

    iget v3, v3, Lcom/konka/kkinterface/tv/DataBaseDesk$KK_SRS_SET;->srs_SpeakerAnalysis:I

    iput v3, v1, Lcom/mstar/android/tvapi/common/vo/AdvancedSoundParameter;->paramSrsTheaterasoundTrubassSpeakerAnalysis:I

    :try_start_3
    invoke-static {}, Lcom/mstar/android/tvapi/common/TvManager;->getInstance()Lcom/mstar/android/tvapi/common/TvManager;

    move-result-object v3

    if-eqz v3, :cond_7

    invoke-static {}, Lcom/mstar/android/tvapi/common/TvManager;->getInstance()Lcom/mstar/android/tvapi/common/TvManager;

    move-result-object v3

    invoke-virtual {v3}, Lcom/mstar/android/tvapi/common/TvManager;->getAudioManager()Lcom/mstar/android/tvapi/common/AudioManager;

    move-result-object v3

    sget-object v4, Lcom/mstar/android/tvapi/common/vo/EnumAdvancedSoundParameterType;->ADVSND_SRS_THEATERSOUND_SPEAKER_ANALYSIS:Lcom/mstar/android/tvapi/common/vo/EnumAdvancedSoundParameterType;

    invoke-virtual {v3, v4, v1}, Lcom/mstar/android/tvapi/common/AudioManager;->setAdvancedSoundEffect(Lcom/mstar/android/tvapi/common/vo/EnumAdvancedSoundParameterType;Lcom/mstar/android/tvapi/common/vo/AdvancedSoundParameter;)Lcom/mstar/android/tvapi/common/vo/EnumAudioReturn;
    :try_end_3
    .catch Lcom/mstar/android/tvapi/common/exception/TvCommonException; {:try_start_3 .. :try_end_3} :catch_3

    :cond_7
    :goto_4
    iput p2, v2, Lcom/konka/kkinterface/tv/DataBaseDesk$KK_SRS_SET;->srs_SpeakerAnalysis:I

    iget-object v3, p0, Lcom/konka/kkimplements/tv/mstar/SoundDeskImpl;->databaseMgr:Lcom/konka/kkimplements/tv/mstar/DataBaseDeskImpl;

    invoke-virtual {v3, v2, p1}, Lcom/konka/kkimplements/tv/mstar/DataBaseDeskImpl;->setSRSSet(Lcom/konka/kkinterface/tv/DataBaseDesk$KK_SRS_SET;Lcom/konka/kkinterface/tv/DataBaseDesk$EN_SRS_SET_TYPE;)Z

    goto/16 :goto_1

    :catch_3
    move-exception v0

    invoke-virtual {v0}, Lcom/mstar/android/tvapi/common/exception/TvCommonException;->printStackTrace()V

    goto :goto_4

    :cond_8
    sget-object v3, Lcom/konka/kkinterface/tv/DataBaseDesk$EN_SRS_SET_TYPE;->E_SRS_TRUBASS_CONTROL:Lcom/konka/kkinterface/tv/DataBaseDesk$EN_SRS_SET_TYPE;

    if-ne p1, v3, :cond_a

    iget-object v3, p0, Lcom/konka/kkimplements/tv/mstar/SoundDeskImpl;->databaseMgr:Lcom/konka/kkimplements/tv/mstar/DataBaseDeskImpl;

    invoke-virtual {v3}, Lcom/konka/kkimplements/tv/mstar/DataBaseDeskImpl;->getSRSSet()Lcom/konka/kkinterface/tv/DataBaseDesk$KK_SRS_SET;

    move-result-object v3

    iget v3, v3, Lcom/konka/kkinterface/tv/DataBaseDesk$KK_SRS_SET;->srs_TrubassControl:I

    mul-int/lit8 v3, v3, 0x64

    iput v3, v1, Lcom/mstar/android/tvapi/common/vo/AdvancedSoundParameter;->paramSrsTheaterSoundTrubassControl:I

    :try_start_4
    invoke-static {}, Lcom/mstar/android/tvapi/common/TvManager;->getInstance()Lcom/mstar/android/tvapi/common/TvManager;

    move-result-object v3

    if-eqz v3, :cond_9

    invoke-static {}, Lcom/mstar/android/tvapi/common/TvManager;->getInstance()Lcom/mstar/android/tvapi/common/TvManager;

    move-result-object v3

    invoke-virtual {v3}, Lcom/mstar/android/tvapi/common/TvManager;->getAudioManager()Lcom/mstar/android/tvapi/common/AudioManager;

    move-result-object v3

    sget-object v4, Lcom/mstar/android/tvapi/common/vo/EnumAdvancedSoundParameterType;->E_ADVSND_SRS_THEATERSOUND_TRUBASS_CONTROL:Lcom/mstar/android/tvapi/common/vo/EnumAdvancedSoundParameterType;

    invoke-virtual {v3, v4, v1}, Lcom/mstar/android/tvapi/common/AudioManager;->setAdvancedSoundEffect(Lcom/mstar/android/tvapi/common/vo/EnumAdvancedSoundParameterType;Lcom/mstar/android/tvapi/common/vo/AdvancedSoundParameter;)Lcom/mstar/android/tvapi/common/vo/EnumAudioReturn;
    :try_end_4
    .catch Lcom/mstar/android/tvapi/common/exception/TvCommonException; {:try_start_4 .. :try_end_4} :catch_4

    :cond_9
    :goto_5
    iput p2, v2, Lcom/konka/kkinterface/tv/DataBaseDesk$KK_SRS_SET;->srs_TrubassControl:I

    iget-object v3, p0, Lcom/konka/kkimplements/tv/mstar/SoundDeskImpl;->databaseMgr:Lcom/konka/kkimplements/tv/mstar/DataBaseDeskImpl;

    invoke-virtual {v3, v2, p1}, Lcom/konka/kkimplements/tv/mstar/DataBaseDeskImpl;->setSRSSet(Lcom/konka/kkinterface/tv/DataBaseDesk$KK_SRS_SET;Lcom/konka/kkinterface/tv/DataBaseDesk$EN_SRS_SET_TYPE;)Z

    goto/16 :goto_1

    :catch_4
    move-exception v0

    invoke-virtual {v0}, Lcom/mstar/android/tvapi/common/exception/TvCommonException;->printStackTrace()V

    goto :goto_5

    :cond_a
    sget-object v3, Lcom/konka/kkinterface/tv/DataBaseDesk$EN_SRS_SET_TYPE;->E_SRS_DC_CONTROL:Lcom/konka/kkinterface/tv/DataBaseDesk$EN_SRS_SET_TYPE;

    if-ne p1, v3, :cond_c

    iget-object v3, p0, Lcom/konka/kkimplements/tv/mstar/SoundDeskImpl;->databaseMgr:Lcom/konka/kkimplements/tv/mstar/DataBaseDeskImpl;

    invoke-virtual {v3}, Lcom/konka/kkimplements/tv/mstar/DataBaseDeskImpl;->getSRSSet()Lcom/konka/kkinterface/tv/DataBaseDesk$KK_SRS_SET;

    move-result-object v3

    iget v3, v3, Lcom/konka/kkinterface/tv/DataBaseDesk$KK_SRS_SET;->srs_DCControl:I

    mul-int/lit8 v3, v3, 0x64

    iput v3, v1, Lcom/mstar/android/tvapi/common/vo/AdvancedSoundParameter;->paramSrsTheaterSoundDcControl:I

    :try_start_5
    invoke-static {}, Lcom/mstar/android/tvapi/common/TvManager;->getInstance()Lcom/mstar/android/tvapi/common/TvManager;

    move-result-object v3

    if-eqz v3, :cond_b

    invoke-static {}, Lcom/mstar/android/tvapi/common/TvManager;->getInstance()Lcom/mstar/android/tvapi/common/TvManager;

    move-result-object v3

    invoke-virtual {v3}, Lcom/mstar/android/tvapi/common/TvManager;->getAudioManager()Lcom/mstar/android/tvapi/common/AudioManager;

    move-result-object v3

    sget-object v4, Lcom/mstar/android/tvapi/common/vo/EnumAdvancedSoundParameterType;->E_ADVSND_SRS_THEATERSOUND_DC_CONTROL:Lcom/mstar/android/tvapi/common/vo/EnumAdvancedSoundParameterType;

    invoke-virtual {v3, v4, v1}, Lcom/mstar/android/tvapi/common/AudioManager;->setAdvancedSoundEffect(Lcom/mstar/android/tvapi/common/vo/EnumAdvancedSoundParameterType;Lcom/mstar/android/tvapi/common/vo/AdvancedSoundParameter;)Lcom/mstar/android/tvapi/common/vo/EnumAudioReturn;
    :try_end_5
    .catch Lcom/mstar/android/tvapi/common/exception/TvCommonException; {:try_start_5 .. :try_end_5} :catch_5

    :cond_b
    :goto_6
    iput p2, v2, Lcom/konka/kkinterface/tv/DataBaseDesk$KK_SRS_SET;->srs_DCControl:I

    iget-object v3, p0, Lcom/konka/kkimplements/tv/mstar/SoundDeskImpl;->databaseMgr:Lcom/konka/kkimplements/tv/mstar/DataBaseDeskImpl;

    invoke-virtual {v3, v2, p1}, Lcom/konka/kkimplements/tv/mstar/DataBaseDeskImpl;->setSRSSet(Lcom/konka/kkinterface/tv/DataBaseDesk$KK_SRS_SET;Lcom/konka/kkinterface/tv/DataBaseDesk$EN_SRS_SET_TYPE;)Z

    goto/16 :goto_1

    :catch_5
    move-exception v0

    invoke-virtual {v0}, Lcom/mstar/android/tvapi/common/exception/TvCommonException;->printStackTrace()V

    goto :goto_6

    :cond_c
    sget-object v3, Lcom/konka/kkinterface/tv/DataBaseDesk$EN_SRS_SET_TYPE;->E_SRS_DEFINITION_CONTROL:Lcom/konka/kkinterface/tv/DataBaseDesk$EN_SRS_SET_TYPE;

    if-ne p1, v3, :cond_1

    iget-object v3, p0, Lcom/konka/kkimplements/tv/mstar/SoundDeskImpl;->databaseMgr:Lcom/konka/kkimplements/tv/mstar/DataBaseDeskImpl;

    invoke-virtual {v3}, Lcom/konka/kkimplements/tv/mstar/DataBaseDeskImpl;->getSRSSet()Lcom/konka/kkinterface/tv/DataBaseDesk$KK_SRS_SET;

    move-result-object v3

    iget v3, v3, Lcom/konka/kkinterface/tv/DataBaseDesk$KK_SRS_SET;->srs_DefinitionControl:I

    mul-int/lit8 v3, v3, 0x64

    iput v3, v1, Lcom/mstar/android/tvapi/common/vo/AdvancedSoundParameter;->paramSrsTheaterSoundDefinitionControl:I

    :try_start_6
    invoke-static {}, Lcom/mstar/android/tvapi/common/TvManager;->getInstance()Lcom/mstar/android/tvapi/common/TvManager;

    move-result-object v3

    if-eqz v3, :cond_d

    invoke-static {}, Lcom/mstar/android/tvapi/common/TvManager;->getInstance()Lcom/mstar/android/tvapi/common/TvManager;

    move-result-object v3

    invoke-virtual {v3}, Lcom/mstar/android/tvapi/common/TvManager;->getAudioManager()Lcom/mstar/android/tvapi/common/AudioManager;

    move-result-object v3

    sget-object v4, Lcom/mstar/android/tvapi/common/vo/EnumAdvancedSoundParameterType;->E_ADVSND_SRS_THEATERSOUND_DEFINITION_CONTROL:Lcom/mstar/android/tvapi/common/vo/EnumAdvancedSoundParameterType;

    invoke-virtual {v3, v4, v1}, Lcom/mstar/android/tvapi/common/AudioManager;->setAdvancedSoundEffect(Lcom/mstar/android/tvapi/common/vo/EnumAdvancedSoundParameterType;Lcom/mstar/android/tvapi/common/vo/AdvancedSoundParameter;)Lcom/mstar/android/tvapi/common/vo/EnumAudioReturn;
    :try_end_6
    .catch Lcom/mstar/android/tvapi/common/exception/TvCommonException; {:try_start_6 .. :try_end_6} :catch_6

    :cond_d
    :goto_7
    iput p2, v2, Lcom/konka/kkinterface/tv/DataBaseDesk$KK_SRS_SET;->srs_DefinitionControl:I

    iget-object v3, p0, Lcom/konka/kkimplements/tv/mstar/SoundDeskImpl;->databaseMgr:Lcom/konka/kkimplements/tv/mstar/DataBaseDeskImpl;

    invoke-virtual {v3, v2, p1}, Lcom/konka/kkimplements/tv/mstar/DataBaseDeskImpl;->setSRSSet(Lcom/konka/kkinterface/tv/DataBaseDesk$KK_SRS_SET;Lcom/konka/kkinterface/tv/DataBaseDesk$EN_SRS_SET_TYPE;)Z

    goto/16 :goto_1

    :catch_6
    move-exception v0

    invoke-virtual {v0}, Lcom/mstar/android/tvapi/common/exception/TvCommonException;->printStackTrace()V

    goto :goto_7
.end method

.method public setSRSTSHD(Z)V
    .locals 5
    .param p1    # Z

    if-eqz p1, :cond_1

    :try_start_0
    const-string v2, "KKJAVAAPI"

    const-string v3, "set SRS TSHD on"

    invoke-static {v2, v3}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    sget-object v2, Lcom/konka/kkinterface/tv/DataBaseDesk$EN_SOUND_MODE;->SOUND_MODE_STANDARD:Lcom/konka/kkinterface/tv/DataBaseDesk$EN_SOUND_MODE;

    const/4 v3, 0x0

    invoke-virtual {p0, v2, v3}, Lcom/konka/kkimplements/tv/mstar/SoundDeskImpl;->setSoundMode(Lcom/konka/kkinterface/tv/DataBaseDesk$EN_SOUND_MODE;Z)Z

    invoke-static {}, Lcom/mstar/android/tvapi/common/TvManager;->getInstance()Lcom/mstar/android/tvapi/common/TvManager;

    move-result-object v2

    if-eqz v2, :cond_0

    invoke-static {}, Lcom/mstar/android/tvapi/common/TvManager;->getInstance()Lcom/mstar/android/tvapi/common/TvManager;

    move-result-object v2

    invoke-virtual {v2}, Lcom/mstar/android/tvapi/common/TvManager;->getAudioManager()Lcom/mstar/android/tvapi/common/AudioManager;

    move-result-object v2

    sget-object v3, Lcom/mstar/android/tvapi/common/vo/EnumAdvancedSoundType;->E_SRS_TSHD:Lcom/mstar/android/tvapi/common/vo/EnumAdvancedSoundType;

    sget-object v4, Lcom/mstar/android/tvapi/common/vo/EnumAdvancedSoundSubProcessType;->E_SRS_THEATERSOUND_TSHD_ON:Lcom/mstar/android/tvapi/common/vo/EnumAdvancedSoundSubProcessType;

    invoke-virtual {v2, v3, v4}, Lcom/mstar/android/tvapi/common/AudioManager;->enableAdvancedSoundEffect(Lcom/mstar/android/tvapi/common/vo/EnumAdvancedSoundType;Lcom/mstar/android/tvapi/common/vo/EnumAdvancedSoundSubProcessType;)Lcom/mstar/android/tvapi/common/vo/EnumAudioReturn;

    :cond_0
    iget-object v2, p0, Lcom/konka/kkimplements/tv/mstar/SoundDeskImpl;->databaseMgr:Lcom/konka/kkimplements/tv/mstar/DataBaseDeskImpl;

    sget-object v3, Lcom/konka/kkinterface/tv/DataBaseDesk$EnumSwitchOnOff;->SWITCH_ON:Lcom/konka/kkinterface/tv/DataBaseDesk$EnumSwitchOnOff;

    invoke-virtual {v2, v3}, Lcom/konka/kkimplements/tv/mstar/DataBaseDeskImpl;->setSRSOnOff(Lcom/konka/kkinterface/tv/DataBaseDesk$EnumSwitchOnOff;)V

    :goto_0
    return-void

    :cond_1
    const-string v2, "KKJAVAAPI"

    const-string v3, "set SRS TSHD off"

    invoke-static {v2, v3}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    invoke-static {}, Lcom/mstar/android/tvapi/common/TvManager;->getInstance()Lcom/mstar/android/tvapi/common/TvManager;

    move-result-object v2

    if-eqz v2, :cond_2

    invoke-static {}, Lcom/mstar/android/tvapi/common/TvManager;->getInstance()Lcom/mstar/android/tvapi/common/TvManager;

    move-result-object v2

    invoke-virtual {v2}, Lcom/mstar/android/tvapi/common/TvManager;->getAudioManager()Lcom/mstar/android/tvapi/common/AudioManager;

    move-result-object v2

    sget-object v3, Lcom/mstar/android/tvapi/common/vo/EnumAdvancedSoundType;->E_SRS_TSHD:Lcom/mstar/android/tvapi/common/vo/EnumAdvancedSoundType;

    sget-object v4, Lcom/mstar/android/tvapi/common/vo/EnumAdvancedSoundSubProcessType;->E_SRS_THEATERSOUND_TSHD_OFF:Lcom/mstar/android/tvapi/common/vo/EnumAdvancedSoundSubProcessType;

    invoke-virtual {v2, v3, v4}, Lcom/mstar/android/tvapi/common/AudioManager;->enableAdvancedSoundEffect(Lcom/mstar/android/tvapi/common/vo/EnumAdvancedSoundType;Lcom/mstar/android/tvapi/common/vo/EnumAdvancedSoundSubProcessType;)Lcom/mstar/android/tvapi/common/vo/EnumAudioReturn;

    :cond_2
    iget-object v2, p0, Lcom/konka/kkimplements/tv/mstar/SoundDeskImpl;->databaseMgr:Lcom/konka/kkimplements/tv/mstar/DataBaseDeskImpl;

    sget-object v3, Lcom/konka/kkinterface/tv/DataBaseDesk$EnumSwitchOnOff;->SWITCH_OFF:Lcom/konka/kkinterface/tv/DataBaseDesk$EnumSwitchOnOff;

    invoke-virtual {v2, v3}, Lcom/konka/kkimplements/tv/mstar/DataBaseDeskImpl;->setSRSOnOff(Lcom/konka/kkinterface/tv/DataBaseDesk$EnumSwitchOnOff;)V

    invoke-virtual {p0}, Lcom/konka/kkimplements/tv/mstar/SoundDeskImpl;->getSoundMode()Lcom/konka/kkinterface/tv/DataBaseDesk$EN_SOUND_MODE;

    move-result-object v1

    invoke-virtual {p0, v1}, Lcom/konka/kkimplements/tv/mstar/SoundDeskImpl;->setSoundMode(Lcom/konka/kkinterface/tv/DataBaseDesk$EN_SOUND_MODE;)Z
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    goto :goto_0

    :catch_0
    move-exception v0

    invoke-virtual {v0}, Ljava/lang/Exception;->printStackTrace()V

    goto :goto_0
.end method

.method public setSRSTrueBass(Z)V
    .locals 6
    .param p1    # Z

    if-eqz p1, :cond_3

    :try_start_0
    const-string v3, "KKJAVAAPI"

    const-string v4, "set SRS TrueBass on"

    invoke-static {v3, v4}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    invoke-static {}, Lcom/mstar/android/tvapi/common/TvManager;->getInstance()Lcom/mstar/android/tvapi/common/TvManager;

    move-result-object v3

    if-eqz v3, :cond_0

    invoke-static {}, Lcom/mstar/android/tvapi/common/TvManager;->getInstance()Lcom/mstar/android/tvapi/common/TvManager;

    move-result-object v3

    invoke-virtual {v3}, Lcom/mstar/android/tvapi/common/TvManager;->getAudioManager()Lcom/mstar/android/tvapi/common/AudioManager;

    move-result-object v3

    sget-object v4, Lcom/mstar/android/tvapi/common/vo/EnumAdvancedSoundType;->E_SRS_TSHD:Lcom/mstar/android/tvapi/common/vo/EnumAdvancedSoundType;

    sget-object v5, Lcom/mstar/android/tvapi/common/vo/EnumAdvancedSoundSubProcessType;->E_SRS_THEATERSOUND_TRUEBASS_ON:Lcom/mstar/android/tvapi/common/vo/EnumAdvancedSoundSubProcessType;

    invoke-virtual {v3, v4, v5}, Lcom/mstar/android/tvapi/common/AudioManager;->enableAdvancedSoundEffect(Lcom/mstar/android/tvapi/common/vo/EnumAdvancedSoundType;Lcom/mstar/android/tvapi/common/vo/EnumAdvancedSoundSubProcessType;)Lcom/mstar/android/tvapi/common/vo/EnumAudioReturn;

    :cond_0
    iget-object v3, p0, Lcom/konka/kkimplements/tv/mstar/SoundDeskImpl;->databaseMgr:Lcom/konka/kkimplements/tv/mstar/DataBaseDeskImpl;

    sget-object v4, Lcom/konka/kkinterface/tv/DataBaseDesk$EnumSwitchOnOff;->SWITCH_ON:Lcom/konka/kkinterface/tv/DataBaseDesk$EnumSwitchOnOff;

    invoke-virtual {v3, v4}, Lcom/konka/kkimplements/tv/mstar/DataBaseDeskImpl;->setTruebaseOnOff(Lcom/konka/kkinterface/tv/DataBaseDesk$EnumSwitchOnOff;)V

    new-instance v2, Lcom/mstar/android/tvapi/common/vo/AdvancedSoundParameter;

    invoke-direct {v2}, Lcom/mstar/android/tvapi/common/vo/AdvancedSoundParameter;-><init>()V

    const/16 v3, 0x1f4

    iput v3, v2, Lcom/mstar/android/tvapi/common/vo/AdvancedSoundParameter;->paramSrsTheaterSoundTrubassControl:I

    invoke-static {}, Lcom/mstar/android/tvapi/common/TvManager;->getInstance()Lcom/mstar/android/tvapi/common/TvManager;

    move-result-object v3

    if-eqz v3, :cond_1

    invoke-static {}, Lcom/mstar/android/tvapi/common/TvManager;->getInstance()Lcom/mstar/android/tvapi/common/TvManager;

    move-result-object v3

    invoke-virtual {v3}, Lcom/mstar/android/tvapi/common/TvManager;->getAudioManager()Lcom/mstar/android/tvapi/common/AudioManager;

    move-result-object v3

    sget-object v4, Lcom/mstar/android/tvapi/common/vo/EnumAdvancedSoundParameterType;->E_ADVSND_SRS_THEATERSOUND_TRUBASS_CONTROL:Lcom/mstar/android/tvapi/common/vo/EnumAdvancedSoundParameterType;

    invoke-virtual {v3, v4, v2}, Lcom/mstar/android/tvapi/common/AudioManager;->setAdvancedSoundEffect(Lcom/mstar/android/tvapi/common/vo/EnumAdvancedSoundParameterType;Lcom/mstar/android/tvapi/common/vo/AdvancedSoundParameter;)Lcom/mstar/android/tvapi/common/vo/EnumAudioReturn;

    :cond_1
    const/4 v1, 0x0

    invoke-static {}, Lcom/mstar/android/tvapi/common/TvManager;->getInstance()Lcom/mstar/android/tvapi/common/TvManager;

    move-result-object v3

    if-eqz v3, :cond_2

    invoke-static {}, Lcom/mstar/android/tvapi/common/TvManager;->getInstance()Lcom/mstar/android/tvapi/common/TvManager;

    move-result-object v3

    invoke-virtual {v3}, Lcom/mstar/android/tvapi/common/TvManager;->getAudioManager()Lcom/mstar/android/tvapi/common/AudioManager;

    move-result-object v3

    sget-object v4, Lcom/mstar/android/tvapi/common/vo/EnumAdvancedSoundParameterType;->E_ADVSND_SRS_THEATERSOUND_TRUBASS_CONTROL:Lcom/mstar/android/tvapi/common/vo/EnumAdvancedSoundParameterType;

    invoke-virtual {v3, v4}, Lcom/mstar/android/tvapi/common/AudioManager;->getAdvancedSoundEffect(Lcom/mstar/android/tvapi/common/vo/EnumAdvancedSoundParameterType;)I

    move-result v1

    :cond_2
    const-string v3, "KKJAVAAPI"

    new-instance v4, Ljava/lang/StringBuilder;

    const-string v5, "the iTrubass=="

    invoke-direct {v4, v5}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v4, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-static {v3, v4}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    :goto_0
    return-void

    :cond_3
    const-string v3, "KKJAVAAPI"

    const-string v4, "set SRS TrueBass off!"

    invoke-static {v3, v4}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    invoke-static {}, Lcom/mstar/android/tvapi/common/TvManager;->getInstance()Lcom/mstar/android/tvapi/common/TvManager;

    move-result-object v3

    if-eqz v3, :cond_4

    invoke-static {}, Lcom/mstar/android/tvapi/common/TvManager;->getInstance()Lcom/mstar/android/tvapi/common/TvManager;

    move-result-object v3

    invoke-virtual {v3}, Lcom/mstar/android/tvapi/common/TvManager;->getAudioManager()Lcom/mstar/android/tvapi/common/AudioManager;

    move-result-object v3

    sget-object v4, Lcom/mstar/android/tvapi/common/vo/EnumAdvancedSoundType;->E_SRS_TSHD:Lcom/mstar/android/tvapi/common/vo/EnumAdvancedSoundType;

    sget-object v5, Lcom/mstar/android/tvapi/common/vo/EnumAdvancedSoundSubProcessType;->E_SRS_THEATERSOUND_TRUEBASS_OFF:Lcom/mstar/android/tvapi/common/vo/EnumAdvancedSoundSubProcessType;

    invoke-virtual {v3, v4, v5}, Lcom/mstar/android/tvapi/common/AudioManager;->enableAdvancedSoundEffect(Lcom/mstar/android/tvapi/common/vo/EnumAdvancedSoundType;Lcom/mstar/android/tvapi/common/vo/EnumAdvancedSoundSubProcessType;)Lcom/mstar/android/tvapi/common/vo/EnumAudioReturn;

    :cond_4
    iget-object v3, p0, Lcom/konka/kkimplements/tv/mstar/SoundDeskImpl;->databaseMgr:Lcom/konka/kkimplements/tv/mstar/DataBaseDeskImpl;

    sget-object v4, Lcom/konka/kkinterface/tv/DataBaseDesk$EnumSwitchOnOff;->SWITCH_OFF:Lcom/konka/kkinterface/tv/DataBaseDesk$EnumSwitchOnOff;

    invoke-virtual {v3, v4}, Lcom/konka/kkimplements/tv/mstar/DataBaseDeskImpl;->setTruebaseOnOff(Lcom/konka/kkinterface/tv/DataBaseDesk$EnumSwitchOnOff;)V
    :try_end_0
    .catch Lcom/mstar/android/tvapi/common/exception/TvCommonException; {:try_start_0 .. :try_end_0} :catch_0

    goto :goto_0

    :catch_0
    move-exception v0

    invoke-virtual {v0}, Lcom/mstar/android/tvapi/common/exception/TvCommonException;->printStackTrace()V

    goto :goto_0
.end method

.method public setSoundMode(Lcom/konka/kkinterface/tv/DataBaseDesk$EN_SOUND_MODE;)Z
    .locals 1
    .param p1    # Lcom/konka/kkinterface/tv/DataBaseDesk$EN_SOUND_MODE;

    const/4 v0, 0x1

    invoke-virtual {p0, p1, v0}, Lcom/konka/kkimplements/tv/mstar/SoundDeskImpl;->setSoundMode(Lcom/konka/kkinterface/tv/DataBaseDesk$EN_SOUND_MODE;Z)Z

    move-result v0

    return v0
.end method

.method public setSoundMode(Lcom/konka/kkinterface/tv/DataBaseDesk$EN_SOUND_MODE;Z)Z
    .locals 7
    .param p1    # Lcom/konka/kkinterface/tv/DataBaseDesk$EN_SOUND_MODE;
    .param p2    # Z

    const/4 v2, 0x1

    const/4 v0, 0x0

    iget-object v3, p0, Lcom/konka/kkimplements/tv/mstar/SoundDeskImpl;->databaseMgr:Lcom/konka/kkimplements/tv/mstar/DataBaseDeskImpl;

    invoke-virtual {v3}, Lcom/konka/kkimplements/tv/mstar/DataBaseDeskImpl;->querySoundSetting()Lcom/konka/kkinterface/tv/DataBaseDesk$MS_USER_SOUND_SETTING;

    iget-object v3, p0, Lcom/konka/kkimplements/tv/mstar/SoundDeskImpl;->databaseMgr:Lcom/konka/kkimplements/tv/mstar/DataBaseDeskImpl;

    invoke-virtual {v3}, Lcom/konka/kkimplements/tv/mstar/DataBaseDeskImpl;->getSound()Lcom/konka/kkinterface/tv/DataBaseDesk$MS_USER_SOUND_SETTING;

    move-result-object v3

    iput-object v3, p0, Lcom/konka/kkimplements/tv/mstar/SoundDeskImpl;->userSoundSetting:Lcom/konka/kkinterface/tv/DataBaseDesk$MS_USER_SOUND_SETTING;

    new-instance v0, Lcom/mstar/android/tvapi/common/vo/DtvSoundEffect;

    invoke-direct {v0}, Lcom/mstar/android/tvapi/common/vo/DtvSoundEffect;-><init>()V

    iget-object v3, p0, Lcom/konka/kkimplements/tv/mstar/SoundDeskImpl;->userSoundSetting:Lcom/konka/kkinterface/tv/DataBaseDesk$MS_USER_SOUND_SETTING;

    iput-object p1, v3, Lcom/konka/kkinterface/tv/DataBaseDesk$MS_USER_SOUND_SETTING;->SoundMode:Lcom/konka/kkinterface/tv/DataBaseDesk$EN_SOUND_MODE;

    sget-object v3, Lcom/konka/kkinterface/tv/DataBaseDesk$EN_SOUND_MODE;->SOUND_MODE_NUM:Lcom/konka/kkinterface/tv/DataBaseDesk$EN_SOUND_MODE;

    invoke-virtual {v3}, Lcom/konka/kkinterface/tv/DataBaseDesk$EN_SOUND_MODE;->ordinal()I

    move-result v3

    int-to-short v3, v3

    iput-short v3, v0, Lcom/mstar/android/tvapi/common/vo/DtvSoundEffect;->eqBandNumber:S

    iget-object v3, p0, Lcom/konka/kkimplements/tv/mstar/SoundDeskImpl;->com:Lcom/konka/kkinterface/tv/CommonDesk;

    const-string v4, "TvSoundServiceImpl"

    new-instance v5, Ljava/lang/StringBuilder;

    const-string v6, "SoundMode is"

    invoke-direct {v5, v6}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    iget-object v6, p0, Lcom/konka/kkimplements/tv/mstar/SoundDeskImpl;->userSoundSetting:Lcom/konka/kkinterface/tv/DataBaseDesk$MS_USER_SOUND_SETTING;

    iget-object v6, v6, Lcom/konka/kkinterface/tv/DataBaseDesk$MS_USER_SOUND_SETTING;->SoundMode:Lcom/konka/kkinterface/tv/DataBaseDesk$EN_SOUND_MODE;

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    invoke-interface {v3, v4, v5}, Lcom/konka/kkinterface/tv/CommonDesk;->printfI(Ljava/lang/String;Ljava/lang/String;)V

    iget-object v3, p0, Lcom/konka/kkimplements/tv/mstar/SoundDeskImpl;->com:Lcom/konka/kkinterface/tv/CommonDesk;

    const-string v4, "TvSoundServiceImpl"

    new-instance v5, Ljava/lang/StringBuilder;

    const-string v6, "Value:"

    invoke-direct {v5, v6}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    iget-object v6, p0, Lcom/konka/kkimplements/tv/mstar/SoundDeskImpl;->databaseMgr:Lcom/konka/kkimplements/tv/mstar/DataBaseDeskImpl;

    invoke-virtual {v6, p1}, Lcom/konka/kkimplements/tv/mstar/DataBaseDeskImpl;->getSoundMode(Lcom/konka/kkinterface/tv/DataBaseDesk$EN_SOUND_MODE;)Lcom/konka/kkinterface/tv/DataBaseDesk$SoundModeSeting;

    move-result-object v6

    iget-short v6, v6, Lcom/konka/kkinterface/tv/DataBaseDesk$SoundModeSeting;->EqBand1:S

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v5

    const-string v6, ":"

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    iget-object v6, p0, Lcom/konka/kkimplements/tv/mstar/SoundDeskImpl;->databaseMgr:Lcom/konka/kkimplements/tv/mstar/DataBaseDeskImpl;

    invoke-virtual {v6, p1}, Lcom/konka/kkimplements/tv/mstar/DataBaseDeskImpl;->getSoundMode(Lcom/konka/kkinterface/tv/DataBaseDesk$EN_SOUND_MODE;)Lcom/konka/kkinterface/tv/DataBaseDesk$SoundModeSeting;

    move-result-object v6

    iget-short v6, v6, Lcom/konka/kkinterface/tv/DataBaseDesk$SoundModeSeting;->EqBand2:S

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v5

    const-string v6, ":"

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    iget-object v6, p0, Lcom/konka/kkimplements/tv/mstar/SoundDeskImpl;->databaseMgr:Lcom/konka/kkimplements/tv/mstar/DataBaseDeskImpl;

    invoke-virtual {v6, p1}, Lcom/konka/kkimplements/tv/mstar/DataBaseDeskImpl;->getSoundMode(Lcom/konka/kkinterface/tv/DataBaseDesk$EN_SOUND_MODE;)Lcom/konka/kkinterface/tv/DataBaseDesk$SoundModeSeting;

    move-result-object v6

    iget-short v6, v6, Lcom/konka/kkinterface/tv/DataBaseDesk$SoundModeSeting;->EqBand3:S

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v5

    const-string v6, ":"

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    iget-object v6, p0, Lcom/konka/kkimplements/tv/mstar/SoundDeskImpl;->databaseMgr:Lcom/konka/kkimplements/tv/mstar/DataBaseDeskImpl;

    invoke-virtual {v6, p1}, Lcom/konka/kkimplements/tv/mstar/DataBaseDeskImpl;->getSoundMode(Lcom/konka/kkinterface/tv/DataBaseDesk$EN_SOUND_MODE;)Lcom/konka/kkinterface/tv/DataBaseDesk$SoundModeSeting;

    move-result-object v6

    iget-short v6, v6, Lcom/konka/kkinterface/tv/DataBaseDesk$SoundModeSeting;->EqBand4:S

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v5

    const-string v6, ":"

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    iget-object v6, p0, Lcom/konka/kkimplements/tv/mstar/SoundDeskImpl;->databaseMgr:Lcom/konka/kkimplements/tv/mstar/DataBaseDeskImpl;

    invoke-virtual {v6, p1}, Lcom/konka/kkimplements/tv/mstar/DataBaseDeskImpl;->getSoundMode(Lcom/konka/kkinterface/tv/DataBaseDesk$EN_SOUND_MODE;)Lcom/konka/kkinterface/tv/DataBaseDesk$SoundModeSeting;

    move-result-object v6

    iget-short v6, v6, Lcom/konka/kkinterface/tv/DataBaseDesk$SoundModeSeting;->EqBand5:S

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    invoke-interface {v3, v4, v5}, Lcom/konka/kkinterface/tv/CommonDesk;->printfI(Ljava/lang/String;Ljava/lang/String;)V

    :try_start_0
    iget-object v3, v0, Lcom/mstar/android/tvapi/common/vo/DtvSoundEffect;->soundParameterEqs:[Lcom/mstar/android/tvapi/common/vo/SoundParameterEq;

    const/4 v4, 0x0

    aget-object v3, v3, v4

    iget-object v4, p0, Lcom/konka/kkimplements/tv/mstar/SoundDeskImpl;->databaseMgr:Lcom/konka/kkimplements/tv/mstar/DataBaseDeskImpl;

    invoke-virtual {v4, p1}, Lcom/konka/kkimplements/tv/mstar/DataBaseDeskImpl;->getSoundMode(Lcom/konka/kkinterface/tv/DataBaseDesk$EN_SOUND_MODE;)Lcom/konka/kkinterface/tv/DataBaseDesk$SoundModeSeting;

    move-result-object v4

    iget-short v4, v4, Lcom/konka/kkinterface/tv/DataBaseDesk$SoundModeSeting;->EqBand1:S

    iput v4, v3, Lcom/mstar/android/tvapi/common/vo/SoundParameterEq;->eqLevel:I

    iget-object v3, v0, Lcom/mstar/android/tvapi/common/vo/DtvSoundEffect;->soundParameterEqs:[Lcom/mstar/android/tvapi/common/vo/SoundParameterEq;

    const/4 v4, 0x1

    aget-object v3, v3, v4

    iget-object v4, p0, Lcom/konka/kkimplements/tv/mstar/SoundDeskImpl;->databaseMgr:Lcom/konka/kkimplements/tv/mstar/DataBaseDeskImpl;

    invoke-virtual {v4, p1}, Lcom/konka/kkimplements/tv/mstar/DataBaseDeskImpl;->getSoundMode(Lcom/konka/kkinterface/tv/DataBaseDesk$EN_SOUND_MODE;)Lcom/konka/kkinterface/tv/DataBaseDesk$SoundModeSeting;

    move-result-object v4

    iget-short v4, v4, Lcom/konka/kkinterface/tv/DataBaseDesk$SoundModeSeting;->EqBand2:S

    iput v4, v3, Lcom/mstar/android/tvapi/common/vo/SoundParameterEq;->eqLevel:I

    iget-object v3, v0, Lcom/mstar/android/tvapi/common/vo/DtvSoundEffect;->soundParameterEqs:[Lcom/mstar/android/tvapi/common/vo/SoundParameterEq;

    const/4 v4, 0x2

    aget-object v3, v3, v4

    iget-object v4, p0, Lcom/konka/kkimplements/tv/mstar/SoundDeskImpl;->databaseMgr:Lcom/konka/kkimplements/tv/mstar/DataBaseDeskImpl;

    invoke-virtual {v4, p1}, Lcom/konka/kkimplements/tv/mstar/DataBaseDeskImpl;->getSoundMode(Lcom/konka/kkinterface/tv/DataBaseDesk$EN_SOUND_MODE;)Lcom/konka/kkinterface/tv/DataBaseDesk$SoundModeSeting;

    move-result-object v4

    iget-short v4, v4, Lcom/konka/kkinterface/tv/DataBaseDesk$SoundModeSeting;->EqBand3:S

    iput v4, v3, Lcom/mstar/android/tvapi/common/vo/SoundParameterEq;->eqLevel:I

    iget-object v3, v0, Lcom/mstar/android/tvapi/common/vo/DtvSoundEffect;->soundParameterEqs:[Lcom/mstar/android/tvapi/common/vo/SoundParameterEq;

    const/4 v4, 0x3

    aget-object v3, v3, v4

    iget-object v4, p0, Lcom/konka/kkimplements/tv/mstar/SoundDeskImpl;->databaseMgr:Lcom/konka/kkimplements/tv/mstar/DataBaseDeskImpl;

    invoke-virtual {v4, p1}, Lcom/konka/kkimplements/tv/mstar/DataBaseDeskImpl;->getSoundMode(Lcom/konka/kkinterface/tv/DataBaseDesk$EN_SOUND_MODE;)Lcom/konka/kkinterface/tv/DataBaseDesk$SoundModeSeting;

    move-result-object v4

    iget-short v4, v4, Lcom/konka/kkinterface/tv/DataBaseDesk$SoundModeSeting;->EqBand4:S

    iput v4, v3, Lcom/mstar/android/tvapi/common/vo/SoundParameterEq;->eqLevel:I

    iget-object v3, v0, Lcom/mstar/android/tvapi/common/vo/DtvSoundEffect;->soundParameterEqs:[Lcom/mstar/android/tvapi/common/vo/SoundParameterEq;

    const/4 v4, 0x4

    aget-object v3, v3, v4

    iget-object v4, p0, Lcom/konka/kkimplements/tv/mstar/SoundDeskImpl;->databaseMgr:Lcom/konka/kkimplements/tv/mstar/DataBaseDeskImpl;

    invoke-virtual {v4, p1}, Lcom/konka/kkimplements/tv/mstar/DataBaseDeskImpl;->getSoundMode(Lcom/konka/kkinterface/tv/DataBaseDesk$EN_SOUND_MODE;)Lcom/konka/kkinterface/tv/DataBaseDesk$SoundModeSeting;

    move-result-object v4

    iget-short v4, v4, Lcom/konka/kkinterface/tv/DataBaseDesk$SoundModeSeting;->EqBand5:S

    iput v4, v3, Lcom/mstar/android/tvapi/common/vo/SoundParameterEq;->eqLevel:I

    const/4 v3, 0x5

    iput-short v3, v0, Lcom/mstar/android/tvapi/common/vo/DtvSoundEffect;->eqBandNumber:S

    invoke-static {}, Lcom/mstar/android/tvapi/common/TvManager;->getInstance()Lcom/mstar/android/tvapi/common/TvManager;

    move-result-object v3

    if-eqz v3, :cond_0

    invoke-static {}, Lcom/mstar/android/tvapi/common/TvManager;->getInstance()Lcom/mstar/android/tvapi/common/TvManager;

    move-result-object v3

    invoke-virtual {v3}, Lcom/mstar/android/tvapi/common/TvManager;->getAudioManager()Lcom/mstar/android/tvapi/common/AudioManager;

    move-result-object v3

    sget-object v4, Lcom/mstar/android/tvapi/common/vo/EnumSoundEffectType;->E_EQ:Lcom/mstar/android/tvapi/common/vo/EnumSoundEffectType;

    invoke-virtual {v3, v4, v0}, Lcom/mstar/android/tvapi/common/AudioManager;->setBasicSoundEffect(Lcom/mstar/android/tvapi/common/vo/EnumSoundEffectType;Lcom/mstar/android/tvapi/common/vo/DtvSoundEffect;)Lcom/mstar/android/tvapi/common/vo/EnumAudioReturn;
    :try_end_0
    .catch Lcom/mstar/android/tvapi/common/exception/TvCommonException; {:try_start_0 .. :try_end_0} :catch_0

    :cond_0
    :goto_0
    if-eqz p2, :cond_1

    iget-object v2, p0, Lcom/konka/kkimplements/tv/mstar/SoundDeskImpl;->databaseMgr:Lcom/konka/kkimplements/tv/mstar/DataBaseDeskImpl;

    iget-object v3, p0, Lcom/konka/kkimplements/tv/mstar/SoundDeskImpl;->userSoundSetting:Lcom/konka/kkinterface/tv/DataBaseDesk$MS_USER_SOUND_SETTING;

    iget-object v3, v3, Lcom/konka/kkinterface/tv/DataBaseDesk$MS_USER_SOUND_SETTING;->SoundMode:Lcom/konka/kkinterface/tv/DataBaseDesk$EN_SOUND_MODE;

    invoke-virtual {v2, v3}, Lcom/konka/kkimplements/tv/mstar/DataBaseDeskImpl;->setSoundMode(Lcom/konka/kkinterface/tv/DataBaseDesk$EN_SOUND_MODE;)Z

    move-result v2

    :cond_1
    return v2

    :catch_0
    move-exception v1

    iget-object v3, p0, Lcom/konka/kkimplements/tv/mstar/SoundDeskImpl;->com:Lcom/konka/kkinterface/tv/CommonDesk;

    const-string v4, "TvSoundServiceImpl"

    const-string v5, "Set Sound Mode Exception"

    invoke-interface {v3, v4, v5}, Lcom/konka/kkinterface/tv/CommonDesk;->printfE(Ljava/lang/String;Ljava/lang/String;)V

    invoke-virtual {v1}, Lcom/mstar/android/tvapi/common/exception/TvCommonException;->printStackTrace()V

    goto :goto_0
.end method

.method public setSoundSpeakerDelay(I)Z
    .locals 1
    .param p1    # I

    invoke-static {}, Lcom/mstar/android/tvapi/common/TvManager;->getInstance()Lcom/mstar/android/tvapi/common/TvManager;

    move-result-object v0

    if-eqz v0, :cond_0

    invoke-static {}, Lcom/mstar/android/tvapi/common/TvManager;->getInstance()Lcom/mstar/android/tvapi/common/TvManager;

    move-result-object v0

    invoke-virtual {v0}, Lcom/mstar/android/tvapi/common/TvManager;->getAudioManager()Lcom/mstar/android/tvapi/common/AudioManager;

    move-result-object v0

    invoke-virtual {v0, p1}, Lcom/mstar/android/tvapi/common/AudioManager;->setSoundSpeakerDelay(I)S

    :cond_0
    const/4 v0, 0x1

    return v0
.end method

.method public setSpdifOutMode(Lcom/konka/kkinterface/tv/DataBaseDesk$EN_SPDIF_MODE;)Z
    .locals 7
    .param p1    # Lcom/konka/kkinterface/tv/DataBaseDesk$EN_SPDIF_MODE;

    iget-object v4, p0, Lcom/konka/kkimplements/tv/mstar/SoundDeskImpl;->databaseMgr:Lcom/konka/kkimplements/tv/mstar/DataBaseDeskImpl;

    invoke-virtual {v4}, Lcom/konka/kkimplements/tv/mstar/DataBaseDeskImpl;->querySoundSetting()Lcom/konka/kkinterface/tv/DataBaseDesk$MS_USER_SOUND_SETTING;

    iget-object v4, p0, Lcom/konka/kkimplements/tv/mstar/SoundDeskImpl;->databaseMgr:Lcom/konka/kkimplements/tv/mstar/DataBaseDeskImpl;

    invoke-virtual {v4}, Lcom/konka/kkimplements/tv/mstar/DataBaseDeskImpl;->getSound()Lcom/konka/kkinterface/tv/DataBaseDesk$MS_USER_SOUND_SETTING;

    move-result-object v4

    iput-object v4, p0, Lcom/konka/kkimplements/tv/mstar/SoundDeskImpl;->userSoundSetting:Lcom/konka/kkinterface/tv/DataBaseDesk$MS_USER_SOUND_SETTING;

    iget-object v4, p0, Lcom/konka/kkimplements/tv/mstar/SoundDeskImpl;->userSoundSetting:Lcom/konka/kkinterface/tv/DataBaseDesk$MS_USER_SOUND_SETTING;

    iput-object p1, v4, Lcom/konka/kkinterface/tv/DataBaseDesk$MS_USER_SOUND_SETTING;->spdifMode:Lcom/konka/kkinterface/tv/DataBaseDesk$EN_SPDIF_MODE;

    new-instance v0, Lcom/mstar/android/tvapi/common/vo/AudioOutParameter;

    invoke-direct {v0}, Lcom/mstar/android/tvapi/common/vo/AudioOutParameter;-><init>()V

    invoke-static {}, Lcom/mstar/android/tvapi/common/vo/EnumSpdifType;->values()[Lcom/mstar/android/tvapi/common/vo/EnumSpdifType;

    move-result-object v4

    invoke-virtual {p1}, Lcom/konka/kkinterface/tv/DataBaseDesk$EN_SPDIF_MODE;->ordinal()I

    move-result v5

    aget-object v4, v4, v5

    invoke-virtual {v0, v4}, Lcom/mstar/android/tvapi/common/vo/AudioOutParameter;->setspdifOutModeInUi(Lcom/mstar/android/tvapi/common/vo/EnumSpdifType;)V

    sget-object v4, Lcom/konka/kkinterface/tv/DataBaseDesk$EN_SPDIF_MODE;->PDIF_MODE_PCM:Lcom/konka/kkinterface/tv/DataBaseDesk$EN_SPDIF_MODE;

    if-ne p1, v4, :cond_1

    sget-object v2, Lcom/mstar/android/tvapi/common/vo/EnumSpdifType;->E_PCM:Lcom/mstar/android/tvapi/common/vo/EnumSpdifType;

    sget-object v3, Lcom/konka/kkinterface/tv/DataBaseDesk$SPDIF_TYPE;->MSAPI_AUD_SPDIF_PCM_:Lcom/konka/kkinterface/tv/DataBaseDesk$SPDIF_TYPE;

    :goto_0
    iget-object v4, p0, Lcom/konka/kkimplements/tv/mstar/SoundDeskImpl;->databaseMgr:Lcom/konka/kkimplements/tv/mstar/DataBaseDeskImpl;

    invoke-virtual {v4, v3}, Lcom/konka/kkimplements/tv/mstar/DataBaseDeskImpl;->setSpdifMode(Lcom/konka/kkinterface/tv/DataBaseDesk$SPDIF_TYPE;)V

    :try_start_0
    invoke-static {}, Lcom/mstar/android/tvapi/common/TvManager;->getInstance()Lcom/mstar/android/tvapi/common/TvManager;

    move-result-object v4

    if-eqz v4, :cond_0

    invoke-static {}, Lcom/mstar/android/tvapi/common/TvManager;->getInstance()Lcom/mstar/android/tvapi/common/TvManager;

    move-result-object v4

    invoke-virtual {v4}, Lcom/mstar/android/tvapi/common/TvManager;->getAudioManager()Lcom/mstar/android/tvapi/common/AudioManager;

    move-result-object v4

    invoke-virtual {v4, v2}, Lcom/mstar/android/tvapi/common/AudioManager;->setDigitalOut(Lcom/mstar/android/tvapi/common/vo/EnumSpdifType;)V
    :try_end_0
    .catch Lcom/mstar/android/tvapi/common/exception/TvCommonException; {:try_start_0 .. :try_end_0} :catch_0

    :cond_0
    :goto_1
    const/4 v4, 0x1

    return v4

    :cond_1
    sget-object v4, Lcom/konka/kkinterface/tv/DataBaseDesk$EN_SPDIF_MODE;->PDIF_MODE_RAW:Lcom/konka/kkinterface/tv/DataBaseDesk$EN_SPDIF_MODE;

    if-ne p1, v4, :cond_2

    sget-object v2, Lcom/mstar/android/tvapi/common/vo/EnumSpdifType;->E_NONPCM:Lcom/mstar/android/tvapi/common/vo/EnumSpdifType;

    sget-object v3, Lcom/konka/kkinterface/tv/DataBaseDesk$SPDIF_TYPE;->MSAPI_AUD_SPDIF_NONPCM_:Lcom/konka/kkinterface/tv/DataBaseDesk$SPDIF_TYPE;

    goto :goto_0

    :cond_2
    sget-object v2, Lcom/mstar/android/tvapi/common/vo/EnumSpdifType;->E_OFF:Lcom/mstar/android/tvapi/common/vo/EnumSpdifType;

    sget-object v3, Lcom/konka/kkinterface/tv/DataBaseDesk$SPDIF_TYPE;->MSAPI_AUD_SPDIF_OFF_:Lcom/konka/kkinterface/tv/DataBaseDesk$SPDIF_TYPE;

    goto :goto_0

    :catch_0
    move-exception v1

    iget-object v4, p0, Lcom/konka/kkimplements/tv/mstar/SoundDeskImpl;->com:Lcom/konka/kkinterface/tv/CommonDesk;

    const-string v5, "TvSoundServiceImpl"

    const-string v6, "Set Volume Exception"

    invoke-interface {v4, v5, v6}, Lcom/konka/kkinterface/tv/CommonDesk;->printfE(Ljava/lang/String;Ljava/lang/String;)V

    invoke-virtual {v1}, Lcom/mstar/android/tvapi/common/exception/TvCommonException;->printStackTrace()V

    goto :goto_1
.end method

.method public setSurroundMode(Lcom/konka/kkinterface/tv/DataBaseDesk$EN_SURROUND_MODE;)Z
    .locals 6
    .param p1    # Lcom/konka/kkinterface/tv/DataBaseDesk$EN_SURROUND_MODE;

    iget-object v2, p0, Lcom/konka/kkimplements/tv/mstar/SoundDeskImpl;->databaseMgr:Lcom/konka/kkimplements/tv/mstar/DataBaseDeskImpl;

    invoke-virtual {v2}, Lcom/konka/kkimplements/tv/mstar/DataBaseDeskImpl;->querySoundSetting()Lcom/konka/kkinterface/tv/DataBaseDesk$MS_USER_SOUND_SETTING;

    iget-object v2, p0, Lcom/konka/kkimplements/tv/mstar/SoundDeskImpl;->databaseMgr:Lcom/konka/kkimplements/tv/mstar/DataBaseDeskImpl;

    invoke-virtual {v2}, Lcom/konka/kkimplements/tv/mstar/DataBaseDeskImpl;->getSound()Lcom/konka/kkinterface/tv/DataBaseDesk$MS_USER_SOUND_SETTING;

    move-result-object v2

    iput-object v2, p0, Lcom/konka/kkimplements/tv/mstar/SoundDeskImpl;->userSoundSetting:Lcom/konka/kkinterface/tv/DataBaseDesk$MS_USER_SOUND_SETTING;

    const/4 v0, 0x0

    iget-object v2, p0, Lcom/konka/kkimplements/tv/mstar/SoundDeskImpl;->userSoundSetting:Lcom/konka/kkinterface/tv/DataBaseDesk$MS_USER_SOUND_SETTING;

    iput-object p1, v2, Lcom/konka/kkinterface/tv/DataBaseDesk$MS_USER_SOUND_SETTING;->SurroundMode:Lcom/konka/kkinterface/tv/DataBaseDesk$EN_SURROUND_MODE;

    iget-object v2, p0, Lcom/konka/kkimplements/tv/mstar/SoundDeskImpl;->com:Lcom/konka/kkinterface/tv/CommonDesk;

    const-string v3, "TvSoundServiceImpl"

    new-instance v4, Ljava/lang/StringBuilder;

    const-string v5, "SurroundMode is"

    invoke-direct {v4, v5}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    iget-object v5, p0, Lcom/konka/kkimplements/tv/mstar/SoundDeskImpl;->userSoundSetting:Lcom/konka/kkinterface/tv/DataBaseDesk$MS_USER_SOUND_SETTING;

    iget-object v5, v5, Lcom/konka/kkinterface/tv/DataBaseDesk$MS_USER_SOUND_SETTING;->SurroundMode:Lcom/konka/kkinterface/tv/DataBaseDesk$EN_SURROUND_MODE;

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-interface {v2, v3, v4}, Lcom/konka/kkinterface/tv/CommonDesk;->printfI(Ljava/lang/String;Ljava/lang/String;)V

    sget-object v2, Lcom/konka/kkinterface/tv/DataBaseDesk$EN_SURROUND_MODE;->E_SURROUND_MODE_OFF:Lcom/konka/kkinterface/tv/DataBaseDesk$EN_SURROUND_MODE;

    if-ne p1, v2, :cond_1

    const/4 v0, 0x0

    :goto_0
    :try_start_0
    invoke-static {}, Lcom/mstar/android/tvapi/common/TvManager;->getInstance()Lcom/mstar/android/tvapi/common/TvManager;

    move-result-object v2

    if-eqz v2, :cond_0

    invoke-static {}, Lcom/mstar/android/tvapi/common/TvManager;->getInstance()Lcom/mstar/android/tvapi/common/TvManager;

    move-result-object v2

    invoke-virtual {v2}, Lcom/mstar/android/tvapi/common/TvManager;->getAudioManager()Lcom/mstar/android/tvapi/common/AudioManager;

    move-result-object v2

    sget-object v3, Lcom/mstar/android/tvapi/common/vo/EnumSoundEffectType;->E_Surround:Lcom/mstar/android/tvapi/common/vo/EnumSoundEffectType;

    invoke-virtual {v2, v3, v0}, Lcom/mstar/android/tvapi/common/AudioManager;->enableBasicSoundEffect(Lcom/mstar/android/tvapi/common/vo/EnumSoundEffectType;Z)Lcom/mstar/android/tvapi/common/vo/EnumAudioReturn;
    :try_end_0
    .catch Lcom/mstar/android/tvapi/common/exception/TvCommonException; {:try_start_0 .. :try_end_0} :catch_0

    :cond_0
    :goto_1
    iget-object v2, p0, Lcom/konka/kkimplements/tv/mstar/SoundDeskImpl;->databaseMgr:Lcom/konka/kkimplements/tv/mstar/DataBaseDeskImpl;

    iget-object v3, p0, Lcom/konka/kkimplements/tv/mstar/SoundDeskImpl;->userSoundSetting:Lcom/konka/kkinterface/tv/DataBaseDesk$MS_USER_SOUND_SETTING;

    invoke-virtual {v2, v3}, Lcom/konka/kkimplements/tv/mstar/DataBaseDeskImpl;->updateSoundSetting(Lcom/konka/kkinterface/tv/DataBaseDesk$MS_USER_SOUND_SETTING;)V

    const/4 v2, 0x1

    return v2

    :cond_1
    const/4 v0, 0x1

    goto :goto_0

    :catch_0
    move-exception v1

    iget-object v2, p0, Lcom/konka/kkimplements/tv/mstar/SoundDeskImpl;->com:Lcom/konka/kkinterface/tv/CommonDesk;

    const-string v3, "TvSoundServiceImpl"

    const-string v4, "Set Balance Exception"

    invoke-interface {v2, v3, v4}, Lcom/konka/kkinterface/tv/CommonDesk;->printfE(Ljava/lang/String;Ljava/lang/String;)V

    invoke-virtual {v1}, Lcom/mstar/android/tvapi/common/exception/TvCommonException;->printStackTrace()V

    goto :goto_1
.end method

.method public setToNextAtvMtsMode()Lcom/mstar/android/tvapi/common/vo/EnumAudioReturn;
    .locals 3

    sget-object v1, Lcom/mstar/android/tvapi/common/vo/EnumAudioReturn;->E_RETURN_NOTOK:Lcom/mstar/android/tvapi/common/vo/EnumAudioReturn;

    :try_start_0
    invoke-static {}, Lcom/mstar/android/tvapi/common/TvManager;->getInstance()Lcom/mstar/android/tvapi/common/TvManager;

    move-result-object v2

    if-eqz v2, :cond_0

    invoke-static {}, Lcom/mstar/android/tvapi/common/TvManager;->getInstance()Lcom/mstar/android/tvapi/common/TvManager;

    move-result-object v2

    invoke-virtual {v2}, Lcom/mstar/android/tvapi/common/TvManager;->getAudioManager()Lcom/mstar/android/tvapi/common/AudioManager;

    move-result-object v2

    invoke-virtual {v2}, Lcom/mstar/android/tvapi/common/AudioManager;->setToNextAtvMtsMode()Lcom/mstar/android/tvapi/common/vo/EnumAudioReturn;
    :try_end_0
    .catch Lcom/mstar/android/tvapi/common/exception/TvCommonException; {:try_start_0 .. :try_end_0} :catch_0

    move-result-object v1

    :cond_0
    :goto_0
    return-object v1

    :catch_0
    move-exception v0

    invoke-virtual {v0}, Lcom/mstar/android/tvapi/common/exception/TvCommonException;->printStackTrace()V

    goto :goto_0
.end method

.method public setTreble(S)Z
    .locals 8
    .param p1    # S

    const/4 v7, 0x1

    iget-object v2, p0, Lcom/konka/kkimplements/tv/mstar/SoundDeskImpl;->databaseMgr:Lcom/konka/kkimplements/tv/mstar/DataBaseDeskImpl;

    invoke-virtual {v2}, Lcom/konka/kkimplements/tv/mstar/DataBaseDeskImpl;->querySoundSetting()Lcom/konka/kkinterface/tv/DataBaseDesk$MS_USER_SOUND_SETTING;

    iget-object v2, p0, Lcom/konka/kkimplements/tv/mstar/SoundDeskImpl;->databaseMgr:Lcom/konka/kkimplements/tv/mstar/DataBaseDeskImpl;

    invoke-virtual {v2}, Lcom/konka/kkimplements/tv/mstar/DataBaseDeskImpl;->getSound()Lcom/konka/kkinterface/tv/DataBaseDesk$MS_USER_SOUND_SETTING;

    move-result-object v2

    iput-object v2, p0, Lcom/konka/kkimplements/tv/mstar/SoundDeskImpl;->userSoundSetting:Lcom/konka/kkinterface/tv/DataBaseDesk$MS_USER_SOUND_SETTING;

    const/4 v0, 0x0

    new-instance v0, Lcom/mstar/android/tvapi/common/vo/DtvSoundEffect;

    invoke-direct {v0}, Lcom/mstar/android/tvapi/common/vo/DtvSoundEffect;-><init>()V

    iget-object v2, p0, Lcom/konka/kkimplements/tv/mstar/SoundDeskImpl;->databaseMgr:Lcom/konka/kkimplements/tv/mstar/DataBaseDeskImpl;

    iget-object v3, p0, Lcom/konka/kkimplements/tv/mstar/SoundDeskImpl;->userSoundSetting:Lcom/konka/kkinterface/tv/DataBaseDesk$MS_USER_SOUND_SETTING;

    iget-object v3, v3, Lcom/konka/kkinterface/tv/DataBaseDesk$MS_USER_SOUND_SETTING;->SoundMode:Lcom/konka/kkinterface/tv/DataBaseDesk$EN_SOUND_MODE;

    invoke-virtual {v2, v3}, Lcom/konka/kkimplements/tv/mstar/DataBaseDeskImpl;->getSoundMode(Lcom/konka/kkinterface/tv/DataBaseDesk$EN_SOUND_MODE;)Lcom/konka/kkinterface/tv/DataBaseDesk$SoundModeSeting;

    move-result-object v2

    iput-short p1, v2, Lcom/konka/kkinterface/tv/DataBaseDesk$SoundModeSeting;->Treble:S

    iget-object v2, p0, Lcom/konka/kkimplements/tv/mstar/SoundDeskImpl;->com:Lcom/konka/kkinterface/tv/CommonDesk;

    const-string v3, "TvSoundServiceImpl"

    new-instance v4, Ljava/lang/StringBuilder;

    const-string v5, "Treble is"

    invoke-direct {v4, v5}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    iget-object v5, p0, Lcom/konka/kkimplements/tv/mstar/SoundDeskImpl;->databaseMgr:Lcom/konka/kkimplements/tv/mstar/DataBaseDeskImpl;

    iget-object v6, p0, Lcom/konka/kkimplements/tv/mstar/SoundDeskImpl;->userSoundSetting:Lcom/konka/kkinterface/tv/DataBaseDesk$MS_USER_SOUND_SETTING;

    iget-object v6, v6, Lcom/konka/kkinterface/tv/DataBaseDesk$MS_USER_SOUND_SETTING;->SoundMode:Lcom/konka/kkinterface/tv/DataBaseDesk$EN_SOUND_MODE;

    invoke-virtual {v5, v6}, Lcom/konka/kkimplements/tv/mstar/DataBaseDeskImpl;->getSoundMode(Lcom/konka/kkinterface/tv/DataBaseDesk$EN_SOUND_MODE;)Lcom/konka/kkinterface/tv/DataBaseDesk$SoundModeSeting;

    move-result-object v5

    iget-short v5, v5, Lcom/konka/kkinterface/tv/DataBaseDesk$SoundModeSeting;->Treble:S

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-interface {v2, v3, v4}, Lcom/konka/kkinterface/tv/CommonDesk;->printfI(Ljava/lang/String;Ljava/lang/String;)V

    :try_start_0
    iget-object v2, v0, Lcom/mstar/android/tvapi/common/vo/DtvSoundEffect;->soundParameterEqs:[Lcom/mstar/android/tvapi/common/vo/SoundParameterEq;

    const/4 v3, 0x0

    aget-object v2, v2, v3

    iget-object v3, p0, Lcom/konka/kkimplements/tv/mstar/SoundDeskImpl;->databaseMgr:Lcom/konka/kkimplements/tv/mstar/DataBaseDeskImpl;

    iget-object v4, p0, Lcom/konka/kkimplements/tv/mstar/SoundDeskImpl;->userSoundSetting:Lcom/konka/kkinterface/tv/DataBaseDesk$MS_USER_SOUND_SETTING;

    iget-object v4, v4, Lcom/konka/kkinterface/tv/DataBaseDesk$MS_USER_SOUND_SETTING;->SoundMode:Lcom/konka/kkinterface/tv/DataBaseDesk$EN_SOUND_MODE;

    invoke-virtual {v3, v4}, Lcom/konka/kkimplements/tv/mstar/DataBaseDeskImpl;->getSoundMode(Lcom/konka/kkinterface/tv/DataBaseDesk$EN_SOUND_MODE;)Lcom/konka/kkinterface/tv/DataBaseDesk$SoundModeSeting;

    move-result-object v3

    iget-short v3, v3, Lcom/konka/kkinterface/tv/DataBaseDesk$SoundModeSeting;->EqBand1:S

    iput v3, v2, Lcom/mstar/android/tvapi/common/vo/SoundParameterEq;->eqLevel:I

    iget-object v2, v0, Lcom/mstar/android/tvapi/common/vo/DtvSoundEffect;->soundParameterEqs:[Lcom/mstar/android/tvapi/common/vo/SoundParameterEq;

    const/4 v3, 0x1

    aget-object v2, v2, v3

    iget-object v3, p0, Lcom/konka/kkimplements/tv/mstar/SoundDeskImpl;->databaseMgr:Lcom/konka/kkimplements/tv/mstar/DataBaseDeskImpl;

    iget-object v4, p0, Lcom/konka/kkimplements/tv/mstar/SoundDeskImpl;->userSoundSetting:Lcom/konka/kkinterface/tv/DataBaseDesk$MS_USER_SOUND_SETTING;

    iget-object v4, v4, Lcom/konka/kkinterface/tv/DataBaseDesk$MS_USER_SOUND_SETTING;->SoundMode:Lcom/konka/kkinterface/tv/DataBaseDesk$EN_SOUND_MODE;

    invoke-virtual {v3, v4}, Lcom/konka/kkimplements/tv/mstar/DataBaseDeskImpl;->getSoundMode(Lcom/konka/kkinterface/tv/DataBaseDesk$EN_SOUND_MODE;)Lcom/konka/kkinterface/tv/DataBaseDesk$SoundModeSeting;

    move-result-object v3

    iget-short v3, v3, Lcom/konka/kkinterface/tv/DataBaseDesk$SoundModeSeting;->EqBand2:S

    iput v3, v2, Lcom/mstar/android/tvapi/common/vo/SoundParameterEq;->eqLevel:I

    iget-object v2, v0, Lcom/mstar/android/tvapi/common/vo/DtvSoundEffect;->soundParameterEqs:[Lcom/mstar/android/tvapi/common/vo/SoundParameterEq;

    const/4 v3, 0x2

    aget-object v2, v2, v3

    iget-object v3, p0, Lcom/konka/kkimplements/tv/mstar/SoundDeskImpl;->databaseMgr:Lcom/konka/kkimplements/tv/mstar/DataBaseDeskImpl;

    iget-object v4, p0, Lcom/konka/kkimplements/tv/mstar/SoundDeskImpl;->userSoundSetting:Lcom/konka/kkinterface/tv/DataBaseDesk$MS_USER_SOUND_SETTING;

    iget-object v4, v4, Lcom/konka/kkinterface/tv/DataBaseDesk$MS_USER_SOUND_SETTING;->SoundMode:Lcom/konka/kkinterface/tv/DataBaseDesk$EN_SOUND_MODE;

    invoke-virtual {v3, v4}, Lcom/konka/kkimplements/tv/mstar/DataBaseDeskImpl;->getSoundMode(Lcom/konka/kkinterface/tv/DataBaseDesk$EN_SOUND_MODE;)Lcom/konka/kkinterface/tv/DataBaseDesk$SoundModeSeting;

    move-result-object v3

    iget-short v3, v3, Lcom/konka/kkinterface/tv/DataBaseDesk$SoundModeSeting;->EqBand3:S

    iput v3, v2, Lcom/mstar/android/tvapi/common/vo/SoundParameterEq;->eqLevel:I

    iget-object v2, v0, Lcom/mstar/android/tvapi/common/vo/DtvSoundEffect;->soundParameterEqs:[Lcom/mstar/android/tvapi/common/vo/SoundParameterEq;

    const/4 v3, 0x3

    aget-object v2, v2, v3

    iget-object v3, p0, Lcom/konka/kkimplements/tv/mstar/SoundDeskImpl;->databaseMgr:Lcom/konka/kkimplements/tv/mstar/DataBaseDeskImpl;

    iget-object v4, p0, Lcom/konka/kkimplements/tv/mstar/SoundDeskImpl;->userSoundSetting:Lcom/konka/kkinterface/tv/DataBaseDesk$MS_USER_SOUND_SETTING;

    iget-object v4, v4, Lcom/konka/kkinterface/tv/DataBaseDesk$MS_USER_SOUND_SETTING;->SoundMode:Lcom/konka/kkinterface/tv/DataBaseDesk$EN_SOUND_MODE;

    invoke-virtual {v3, v4}, Lcom/konka/kkimplements/tv/mstar/DataBaseDeskImpl;->getSoundMode(Lcom/konka/kkinterface/tv/DataBaseDesk$EN_SOUND_MODE;)Lcom/konka/kkinterface/tv/DataBaseDesk$SoundModeSeting;

    move-result-object v3

    iget-short v3, v3, Lcom/konka/kkinterface/tv/DataBaseDesk$SoundModeSeting;->EqBand4:S

    iput v3, v2, Lcom/mstar/android/tvapi/common/vo/SoundParameterEq;->eqLevel:I

    iget-object v2, p0, Lcom/konka/kkimplements/tv/mstar/SoundDeskImpl;->databaseMgr:Lcom/konka/kkimplements/tv/mstar/DataBaseDeskImpl;

    iget-object v3, p0, Lcom/konka/kkimplements/tv/mstar/SoundDeskImpl;->userSoundSetting:Lcom/konka/kkinterface/tv/DataBaseDesk$MS_USER_SOUND_SETTING;

    iget-object v3, v3, Lcom/konka/kkinterface/tv/DataBaseDesk$MS_USER_SOUND_SETTING;->SoundMode:Lcom/konka/kkinterface/tv/DataBaseDesk$EN_SOUND_MODE;

    invoke-virtual {v2, v3}, Lcom/konka/kkimplements/tv/mstar/DataBaseDeskImpl;->getSoundMode(Lcom/konka/kkinterface/tv/DataBaseDesk$EN_SOUND_MODE;)Lcom/konka/kkinterface/tv/DataBaseDesk$SoundModeSeting;

    move-result-object v2

    iput-short p1, v2, Lcom/konka/kkinterface/tv/DataBaseDesk$SoundModeSeting;->EqBand5:S

    iget-object v2, v0, Lcom/mstar/android/tvapi/common/vo/DtvSoundEffect;->soundParameterEqs:[Lcom/mstar/android/tvapi/common/vo/SoundParameterEq;

    const/4 v3, 0x4

    aget-object v2, v2, v3

    iput p1, v2, Lcom/mstar/android/tvapi/common/vo/SoundParameterEq;->eqLevel:I

    const/4 v2, 0x5

    iput-short v2, v0, Lcom/mstar/android/tvapi/common/vo/DtvSoundEffect;->eqBandNumber:S

    invoke-static {}, Lcom/mstar/android/tvapi/common/TvManager;->getInstance()Lcom/mstar/android/tvapi/common/TvManager;

    move-result-object v2

    if-eqz v2, :cond_0

    invoke-static {}, Lcom/mstar/android/tvapi/common/TvManager;->getInstance()Lcom/mstar/android/tvapi/common/TvManager;

    move-result-object v2

    invoke-virtual {v2}, Lcom/mstar/android/tvapi/common/TvManager;->getAudioManager()Lcom/mstar/android/tvapi/common/AudioManager;

    move-result-object v2

    sget-object v3, Lcom/mstar/android/tvapi/common/vo/EnumSoundEffectType;->E_EQ:Lcom/mstar/android/tvapi/common/vo/EnumSoundEffectType;

    invoke-virtual {v2, v3, v0}, Lcom/mstar/android/tvapi/common/AudioManager;->setBasicSoundEffect(Lcom/mstar/android/tvapi/common/vo/EnumSoundEffectType;Lcom/mstar/android/tvapi/common/vo/DtvSoundEffect;)Lcom/mstar/android/tvapi/common/vo/EnumAudioReturn;
    :try_end_0
    .catch Lcom/mstar/android/tvapi/common/exception/TvCommonException; {:try_start_0 .. :try_end_0} :catch_0

    :cond_0
    :goto_0
    iget-object v2, p0, Lcom/konka/kkimplements/tv/mstar/SoundDeskImpl;->databaseMgr:Lcom/konka/kkimplements/tv/mstar/DataBaseDeskImpl;

    iget-object v3, p0, Lcom/konka/kkimplements/tv/mstar/SoundDeskImpl;->databaseMgr:Lcom/konka/kkimplements/tv/mstar/DataBaseDeskImpl;

    iget-object v4, p0, Lcom/konka/kkimplements/tv/mstar/SoundDeskImpl;->userSoundSetting:Lcom/konka/kkinterface/tv/DataBaseDesk$MS_USER_SOUND_SETTING;

    iget-object v4, v4, Lcom/konka/kkinterface/tv/DataBaseDesk$MS_USER_SOUND_SETTING;->SoundMode:Lcom/konka/kkinterface/tv/DataBaseDesk$EN_SOUND_MODE;

    invoke-virtual {v3, v4}, Lcom/konka/kkimplements/tv/mstar/DataBaseDeskImpl;->getSoundMode(Lcom/konka/kkinterface/tv/DataBaseDesk$EN_SOUND_MODE;)Lcom/konka/kkinterface/tv/DataBaseDesk$SoundModeSeting;

    move-result-object v3

    iget-object v4, p0, Lcom/konka/kkimplements/tv/mstar/SoundDeskImpl;->userSoundSetting:Lcom/konka/kkinterface/tv/DataBaseDesk$MS_USER_SOUND_SETTING;

    iget-object v4, v4, Lcom/konka/kkinterface/tv/DataBaseDesk$MS_USER_SOUND_SETTING;->SoundMode:Lcom/konka/kkinterface/tv/DataBaseDesk$EN_SOUND_MODE;

    invoke-virtual {v4}, Lcom/konka/kkinterface/tv/DataBaseDesk$EN_SOUND_MODE;->ordinal()I

    move-result v4

    invoke-virtual {v2, v3, v4}, Lcom/konka/kkimplements/tv/mstar/DataBaseDeskImpl;->updateSoundModeSetting(Lcom/konka/kkinterface/tv/DataBaseDesk$SoundModeSeting;I)V

    return v7

    :catch_0
    move-exception v1

    iget-object v2, p0, Lcom/konka/kkimplements/tv/mstar/SoundDeskImpl;->com:Lcom/konka/kkinterface/tv/CommonDesk;

    const-string v3, "TvSoundServiceImpl"

    const-string v4, "Set Sound Mode Exception"

    invoke-interface {v2, v3, v4}, Lcom/konka/kkinterface/tv/CommonDesk;->printfE(Ljava/lang/String;Ljava/lang/String;)V

    invoke-virtual {v1}, Lcom/mstar/android/tvapi/common/exception/TvCommonException;->printStackTrace()V

    goto :goto_0
.end method

.method public setVolume(S)Z
    .locals 5
    .param p1    # S

    iget-object v1, p0, Lcom/konka/kkimplements/tv/mstar/SoundDeskImpl;->databaseMgr:Lcom/konka/kkimplements/tv/mstar/DataBaseDeskImpl;

    invoke-virtual {v1}, Lcom/konka/kkimplements/tv/mstar/DataBaseDeskImpl;->querySoundSetting()Lcom/konka/kkinterface/tv/DataBaseDesk$MS_USER_SOUND_SETTING;

    iget-object v1, p0, Lcom/konka/kkimplements/tv/mstar/SoundDeskImpl;->databaseMgr:Lcom/konka/kkimplements/tv/mstar/DataBaseDeskImpl;

    invoke-virtual {v1}, Lcom/konka/kkimplements/tv/mstar/DataBaseDeskImpl;->getSound()Lcom/konka/kkinterface/tv/DataBaseDesk$MS_USER_SOUND_SETTING;

    move-result-object v1

    iput-object v1, p0, Lcom/konka/kkimplements/tv/mstar/SoundDeskImpl;->userSoundSetting:Lcom/konka/kkinterface/tv/DataBaseDesk$MS_USER_SOUND_SETTING;

    iget-object v1, p0, Lcom/konka/kkimplements/tv/mstar/SoundDeskImpl;->userSoundSetting:Lcom/konka/kkinterface/tv/DataBaseDesk$MS_USER_SOUND_SETTING;

    iput-short p1, v1, Lcom/konka/kkinterface/tv/DataBaseDesk$MS_USER_SOUND_SETTING;->Volume:S

    iget-object v1, p0, Lcom/konka/kkimplements/tv/mstar/SoundDeskImpl;->com:Lcom/konka/kkinterface/tv/CommonDesk;

    const-string v2, "TvSoundServiceImpl"

    new-instance v3, Ljava/lang/StringBuilder;

    const-string v4, "setVolume is"

    invoke-direct {v3, v4}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    iget-object v4, p0, Lcom/konka/kkimplements/tv/mstar/SoundDeskImpl;->userSoundSetting:Lcom/konka/kkinterface/tv/DataBaseDesk$MS_USER_SOUND_SETTING;

    iget-short v4, v4, Lcom/konka/kkinterface/tv/DataBaseDesk$MS_USER_SOUND_SETTING;->Volume:S

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-interface {v1, v2, v3}, Lcom/konka/kkinterface/tv/CommonDesk;->printfI(Ljava/lang/String;Ljava/lang/String;)V

    :try_start_0
    invoke-static {}, Lcom/mstar/android/tvapi/common/TvManager;->getInstance()Lcom/mstar/android/tvapi/common/TvManager;

    move-result-object v1

    if-eqz v1, :cond_0

    invoke-static {}, Lcom/mstar/android/tvapi/common/TvManager;->getInstance()Lcom/mstar/android/tvapi/common/TvManager;

    move-result-object v1

    invoke-virtual {v1}, Lcom/mstar/android/tvapi/common/TvManager;->getAudioManager()Lcom/mstar/android/tvapi/common/AudioManager;

    move-result-object v1

    sget-object v2, Lcom/mstar/android/tvapi/common/vo/EnumAudioVolumeSourceType;->E_VOL_SOURCE_SPEAKER_OUT:Lcom/mstar/android/tvapi/common/vo/EnumAudioVolumeSourceType;

    int-to-byte v3, p1

    invoke-virtual {v1, v2, v3}, Lcom/mstar/android/tvapi/common/AudioManager;->setAudioVolume(Lcom/mstar/android/tvapi/common/vo/EnumAudioVolumeSourceType;B)V

    invoke-static {}, Lcom/mstar/android/tvapi/common/TvManager;->getInstance()Lcom/mstar/android/tvapi/common/TvManager;

    move-result-object v1

    invoke-virtual {v1}, Lcom/mstar/android/tvapi/common/TvManager;->getAudioManager()Lcom/mstar/android/tvapi/common/AudioManager;

    move-result-object v1

    sget-object v2, Lcom/mstar/android/tvapi/common/vo/EnumAudioVolumeSourceType;->E_VOL_SOURCE_HP_OUT:Lcom/mstar/android/tvapi/common/vo/EnumAudioVolumeSourceType;

    int-to-byte v3, p1

    invoke-virtual {v1, v2, v3}, Lcom/mstar/android/tvapi/common/AudioManager;->setAudioVolume(Lcom/mstar/android/tvapi/common/vo/EnumAudioVolumeSourceType;B)V
    :try_end_0
    .catch Lcom/mstar/android/tvapi/common/exception/TvCommonException; {:try_start_0 .. :try_end_0} :catch_0

    :cond_0
    :goto_0
    iget-object v1, p0, Lcom/konka/kkimplements/tv/mstar/SoundDeskImpl;->databaseMgr:Lcom/konka/kkimplements/tv/mstar/DataBaseDeskImpl;

    iget-object v2, p0, Lcom/konka/kkimplements/tv/mstar/SoundDeskImpl;->userSoundSetting:Lcom/konka/kkinterface/tv/DataBaseDesk$MS_USER_SOUND_SETTING;

    invoke-virtual {v1, v2}, Lcom/konka/kkimplements/tv/mstar/DataBaseDeskImpl;->updateSoundSetting(Lcom/konka/kkinterface/tv/DataBaseDesk$MS_USER_SOUND_SETTING;)V

    const/4 v1, 0x1

    return v1

    :catch_0
    move-exception v0

    iget-object v1, p0, Lcom/konka/kkimplements/tv/mstar/SoundDeskImpl;->com:Lcom/konka/kkinterface/tv/CommonDesk;

    const-string v2, "TvSoundServiceImpl"

    const-string v3, "Set Volume Exception"

    invoke-interface {v1, v2, v3}, Lcom/konka/kkinterface/tv/CommonDesk;->printfE(Ljava/lang/String;Ljava/lang/String;)V

    invoke-virtual {v0}, Lcom/mstar/android/tvapi/common/exception/TvCommonException;->printStackTrace()V

    goto :goto_0
.end method
