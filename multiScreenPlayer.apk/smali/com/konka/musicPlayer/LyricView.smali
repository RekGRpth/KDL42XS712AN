.class public Lcom/konka/musicPlayer/LyricView;
.super Landroid/widget/TextView;
.source "LyricView.java"


# instance fields
.field private CurrentPaint:Landroid/graphics/Paint;

.field private Index:I

.field private NotCurrentPaint:Landroid/graphics/Paint;

.field private TextHigh:F

.field private TextSize:F

.field private high:F

.field private mSentenceEntities:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Lcom/konka/musicPlayer/LyricContent;",
            ">;"
        }
    .end annotation
.end field

.field private width:F


# direct methods
.method public constructor <init>(Landroid/content/Context;)V
    .locals 1
    .param p1    # Landroid/content/Context;

    invoke-direct {p0, p1}, Landroid/widget/TextView;-><init>(Landroid/content/Context;)V

    const/high16 v0, 0x42200000    # 40.0f

    iput v0, p0, Lcom/konka/musicPlayer/LyricView;->TextHigh:F

    const/high16 v0, 0x41a00000    # 20.0f

    iput v0, p0, Lcom/konka/musicPlayer/LyricView;->TextSize:F

    const/4 v0, 0x0

    iput v0, p0, Lcom/konka/musicPlayer/LyricView;->Index:I

    const/4 v0, 0x0

    iput-object v0, p0, Lcom/konka/musicPlayer/LyricView;->mSentenceEntities:Ljava/util/List;

    invoke-direct {p0}, Lcom/konka/musicPlayer/LyricView;->init()V

    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;)V
    .locals 1
    .param p1    # Landroid/content/Context;
    .param p2    # Landroid/util/AttributeSet;

    invoke-direct {p0, p1, p2}, Landroid/widget/TextView;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;)V

    const/high16 v0, 0x42200000    # 40.0f

    iput v0, p0, Lcom/konka/musicPlayer/LyricView;->TextHigh:F

    const/high16 v0, 0x41a00000    # 20.0f

    iput v0, p0, Lcom/konka/musicPlayer/LyricView;->TextSize:F

    const/4 v0, 0x0

    iput v0, p0, Lcom/konka/musicPlayer/LyricView;->Index:I

    const/4 v0, 0x0

    iput-object v0, p0, Lcom/konka/musicPlayer/LyricView;->mSentenceEntities:Ljava/util/List;

    invoke-direct {p0}, Lcom/konka/musicPlayer/LyricView;->init()V

    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V
    .locals 1
    .param p1    # Landroid/content/Context;
    .param p2    # Landroid/util/AttributeSet;
    .param p3    # I

    invoke-direct {p0, p1, p2, p3}, Landroid/widget/TextView;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V

    const/high16 v0, 0x42200000    # 40.0f

    iput v0, p0, Lcom/konka/musicPlayer/LyricView;->TextHigh:F

    const/high16 v0, 0x41a00000    # 20.0f

    iput v0, p0, Lcom/konka/musicPlayer/LyricView;->TextSize:F

    const/4 v0, 0x0

    iput v0, p0, Lcom/konka/musicPlayer/LyricView;->Index:I

    const/4 v0, 0x0

    iput-object v0, p0, Lcom/konka/musicPlayer/LyricView;->mSentenceEntities:Ljava/util/List;

    invoke-direct {p0}, Lcom/konka/musicPlayer/LyricView;->init()V

    return-void
.end method

.method private init()V
    .locals 3

    const/4 v2, 0x1

    invoke-virtual {p0, v2}, Lcom/konka/musicPlayer/LyricView;->setFocusable(Z)V

    new-instance v0, Landroid/graphics/Paint;

    invoke-direct {v0}, Landroid/graphics/Paint;-><init>()V

    iput-object v0, p0, Lcom/konka/musicPlayer/LyricView;->CurrentPaint:Landroid/graphics/Paint;

    iget-object v0, p0, Lcom/konka/musicPlayer/LyricView;->CurrentPaint:Landroid/graphics/Paint;

    invoke-virtual {v0, v2}, Landroid/graphics/Paint;->setAntiAlias(Z)V

    iget-object v0, p0, Lcom/konka/musicPlayer/LyricView;->CurrentPaint:Landroid/graphics/Paint;

    sget-object v1, Landroid/graphics/Paint$Align;->CENTER:Landroid/graphics/Paint$Align;

    invoke-virtual {v0, v1}, Landroid/graphics/Paint;->setTextAlign(Landroid/graphics/Paint$Align;)V

    new-instance v0, Landroid/graphics/Paint;

    invoke-direct {v0}, Landroid/graphics/Paint;-><init>()V

    iput-object v0, p0, Lcom/konka/musicPlayer/LyricView;->NotCurrentPaint:Landroid/graphics/Paint;

    iget-object v0, p0, Lcom/konka/musicPlayer/LyricView;->NotCurrentPaint:Landroid/graphics/Paint;

    invoke-virtual {v0, v2}, Landroid/graphics/Paint;->setAntiAlias(Z)V

    iget-object v0, p0, Lcom/konka/musicPlayer/LyricView;->NotCurrentPaint:Landroid/graphics/Paint;

    sget-object v1, Landroid/graphics/Paint$Align;->CENTER:Landroid/graphics/Paint$Align;

    invoke-virtual {v0, v1}, Landroid/graphics/Paint;->setTextAlign(Landroid/graphics/Paint$Align;)V

    return-void
.end method


# virtual methods
.method public SetIndex(I)V
    .locals 0
    .param p1    # I

    iput p1, p0, Lcom/konka/musicPlayer/LyricView;->Index:I

    return-void
.end method

.method protected onDraw(Landroid/graphics/Canvas;)V
    .locals 7
    .param p1    # Landroid/graphics/Canvas;

    const/high16 v6, 0x40000000    # 2.0f

    invoke-super {p0, p1}, Landroid/widget/TextView;->onDraw(Landroid/graphics/Canvas;)V

    if-nez p1, :cond_1

    :cond_0
    :goto_0
    return-void

    :cond_1
    iget-object v2, p0, Lcom/konka/musicPlayer/LyricView;->CurrentPaint:Landroid/graphics/Paint;

    invoke-virtual {p0}, Lcom/konka/musicPlayer/LyricView;->getResources()Landroid/content/res/Resources;

    move-result-object v3

    const v4, 0x7f07000b    # com.konka.mediaSharePlayer.R.color.music_lrc_yellow

    invoke-virtual {v3, v4}, Landroid/content/res/Resources;->getColor(I)I

    move-result v3

    invoke-virtual {v2, v3}, Landroid/graphics/Paint;->setColor(I)V

    iget-object v2, p0, Lcom/konka/musicPlayer/LyricView;->NotCurrentPaint:Landroid/graphics/Paint;

    invoke-virtual {p0}, Lcom/konka/musicPlayer/LyricView;->getResources()Landroid/content/res/Resources;

    move-result-object v3

    const v4, 0x7f070004    # com.konka.mediaSharePlayer.R.color.white

    invoke-virtual {v3, v4}, Landroid/content/res/Resources;->getColor(I)I

    move-result v3

    invoke-virtual {v2, v3}, Landroid/graphics/Paint;->setColor(I)V

    iget-object v2, p0, Lcom/konka/musicPlayer/LyricView;->CurrentPaint:Landroid/graphics/Paint;

    iget v3, p0, Lcom/konka/musicPlayer/LyricView;->TextSize:F

    const/high16 v4, 0x40a00000    # 5.0f

    add-float/2addr v3, v4

    invoke-virtual {v2, v3}, Landroid/graphics/Paint;->setTextSize(F)V

    iget-object v2, p0, Lcom/konka/musicPlayer/LyricView;->CurrentPaint:Landroid/graphics/Paint;

    sget-object v3, Landroid/graphics/Typeface;->SERIF:Landroid/graphics/Typeface;

    invoke-virtual {v2, v3}, Landroid/graphics/Paint;->setTypeface(Landroid/graphics/Typeface;)Landroid/graphics/Typeface;

    iget-object v2, p0, Lcom/konka/musicPlayer/LyricView;->NotCurrentPaint:Landroid/graphics/Paint;

    iget v3, p0, Lcom/konka/musicPlayer/LyricView;->TextSize:F

    invoke-virtual {v2, v3}, Landroid/graphics/Paint;->setTextSize(F)V

    iget-object v2, p0, Lcom/konka/musicPlayer/LyricView;->NotCurrentPaint:Landroid/graphics/Paint;

    sget-object v3, Landroid/graphics/Typeface;->SERIF:Landroid/graphics/Typeface;

    invoke-virtual {v2, v3}, Landroid/graphics/Paint;->setTypeface(Landroid/graphics/Typeface;)Landroid/graphics/Typeface;

    :try_start_0
    iget-object v2, p0, Lcom/konka/musicPlayer/LyricView;->mSentenceEntities:Ljava/util/List;

    iget v3, p0, Lcom/konka/musicPlayer/LyricView;->Index:I

    invoke-interface {v2, v3}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lcom/konka/musicPlayer/LyricContent;

    invoke-virtual {v2}, Lcom/konka/musicPlayer/LyricContent;->getLyric()Ljava/lang/String;

    move-result-object v2

    iget v3, p0, Lcom/konka/musicPlayer/LyricView;->width:F

    div-float/2addr v3, v6

    iget v4, p0, Lcom/konka/musicPlayer/LyricView;->high:F

    div-float/2addr v4, v6

    iget-object v5, p0, Lcom/konka/musicPlayer/LyricView;->CurrentPaint:Landroid/graphics/Paint;

    invoke-virtual {p1, v2, v3, v4, v5}, Landroid/graphics/Canvas;->drawText(Ljava/lang/String;FFLandroid/graphics/Paint;)V

    iget v2, p0, Lcom/konka/musicPlayer/LyricView;->high:F

    div-float v1, v2, v6

    iget v2, p0, Lcom/konka/musicPlayer/LyricView;->Index:I

    add-int/lit8 v0, v2, -0x1

    :goto_1
    if-gez v0, :cond_2

    iget v2, p0, Lcom/konka/musicPlayer/LyricView;->high:F

    div-float v1, v2, v6

    iget v2, p0, Lcom/konka/musicPlayer/LyricView;->Index:I

    add-int/lit8 v0, v2, 0x1

    :goto_2
    iget-object v2, p0, Lcom/konka/musicPlayer/LyricView;->mSentenceEntities:Ljava/util/List;

    invoke-interface {v2}, Ljava/util/List;->size()I

    move-result v2

    if-ge v0, v2, :cond_0

    iget v2, p0, Lcom/konka/musicPlayer/LyricView;->TextHigh:F

    add-float/2addr v1, v2

    iget-object v2, p0, Lcom/konka/musicPlayer/LyricView;->mSentenceEntities:Ljava/util/List;

    invoke-interface {v2, v0}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lcom/konka/musicPlayer/LyricContent;

    invoke-virtual {v2}, Lcom/konka/musicPlayer/LyricContent;->getLyric()Ljava/lang/String;

    move-result-object v2

    iget v3, p0, Lcom/konka/musicPlayer/LyricView;->width:F

    div-float/2addr v3, v6

    iget-object v4, p0, Lcom/konka/musicPlayer/LyricView;->NotCurrentPaint:Landroid/graphics/Paint;

    invoke-virtual {p1, v2, v3, v1, v4}, Landroid/graphics/Canvas;->drawText(Ljava/lang/String;FFLandroid/graphics/Paint;)V

    add-int/lit8 v0, v0, 0x1

    goto :goto_2

    :cond_2
    iget v2, p0, Lcom/konka/musicPlayer/LyricView;->TextHigh:F

    sub-float/2addr v1, v2

    iget-object v2, p0, Lcom/konka/musicPlayer/LyricView;->mSentenceEntities:Ljava/util/List;

    invoke-interface {v2, v0}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lcom/konka/musicPlayer/LyricContent;

    invoke-virtual {v2}, Lcom/konka/musicPlayer/LyricContent;->getLyric()Ljava/lang/String;

    move-result-object v2

    iget v3, p0, Lcom/konka/musicPlayer/LyricView;->width:F

    div-float/2addr v3, v6

    iget-object v4, p0, Lcom/konka/musicPlayer/LyricView;->NotCurrentPaint:Landroid/graphics/Paint;

    invoke-virtual {p1, v2, v3, v1, v4}, Landroid/graphics/Canvas;->drawText(Ljava/lang/String;FFLandroid/graphics/Paint;)V
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    add-int/lit8 v0, v0, -0x1

    goto :goto_1

    :catch_0
    move-exception v2

    goto/16 :goto_0
.end method

.method protected onSizeChanged(IIII)V
    .locals 1
    .param p1    # I
    .param p2    # I
    .param p3    # I
    .param p4    # I

    invoke-super {p0, p1, p2, p3, p4}, Landroid/widget/TextView;->onSizeChanged(IIII)V

    int-to-float v0, p2

    iput v0, p0, Lcom/konka/musicPlayer/LyricView;->high:F

    int-to-float v0, p1

    iput v0, p0, Lcom/konka/musicPlayer/LyricView;->width:F

    return-void
.end method

.method public setSentenceEntities(Ljava/util/List;)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List",
            "<",
            "Lcom/konka/musicPlayer/LyricContent;",
            ">;)V"
        }
    .end annotation

    iput-object p1, p0, Lcom/konka/musicPlayer/LyricView;->mSentenceEntities:Ljava/util/List;

    return-void
.end method
