.class Lcom/konka/musicPlayer/musicPlayerActivity$8;
.super Ljava/lang/Object;
.source "musicPlayerActivity.java"

# interfaces
.implements Landroid/media/MediaPlayer$OnErrorListener;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/konka/musicPlayer/musicPlayerActivity;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/konka/musicPlayer/musicPlayerActivity;


# direct methods
.method constructor <init>(Lcom/konka/musicPlayer/musicPlayerActivity;)V
    .locals 0

    iput-object p1, p0, Lcom/konka/musicPlayer/musicPlayerActivity$8;->this$0:Lcom/konka/musicPlayer/musicPlayerActivity;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onError(Landroid/media/MediaPlayer;II)Z
    .locals 9
    .param p1    # Landroid/media/MediaPlayer;
    .param p2    # I
    .param p3    # I

    const/4 v2, 0x0

    iget-object v0, p0, Lcom/konka/musicPlayer/musicPlayerActivity$8;->this$0:Lcom/konka/musicPlayer/musicPlayerActivity;

    iput-boolean v2, v0, Lcom/konka/musicPlayer/musicPlayerActivity;->isSend:Z

    iget-object v0, p0, Lcom/konka/musicPlayer/musicPlayerActivity$8;->this$0:Lcom/konka/musicPlayer/musicPlayerActivity;

    iput v2, v0, Lcom/konka/musicPlayer/musicPlayerActivity;->relayTimer:I

    const-string v0, "testmusic"

    new-instance v1, Ljava/lang/StringBuilder;

    const-string v3, "media player is error!what = "

    invoke-direct {v1, v3}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v1, p2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v3, " extra = "

    invoke-virtual {v1, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p3}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    iget-object v0, p0, Lcom/konka/musicPlayer/musicPlayerActivity$8;->this$0:Lcom/konka/musicPlayer/musicPlayerActivity;

    invoke-virtual {v0}, Lcom/konka/musicPlayer/musicPlayerActivity;->getApplicationContext()Landroid/content/Context;

    move-result-object v0

    const v1, 0x7f0900a7    # com.konka.mediaSharePlayer.R.string.file_downLoad_error

    const/4 v3, 0x1

    invoke-static {v0, v1, v3}, Landroid/widget/Toast;->makeText(Landroid/content/Context;II)Landroid/widget/Toast;

    move-result-object v0

    invoke-virtual {v0}, Landroid/widget/Toast;->show()V

    iget-object v0, p0, Lcom/konka/musicPlayer/musicPlayerActivity$8;->this$0:Lcom/konka/musicPlayer/musicPlayerActivity;

    iget-object v1, p0, Lcom/konka/musicPlayer/musicPlayerActivity$8;->this$0:Lcom/konka/musicPlayer/musicPlayerActivity;

    iget-object v3, p0, Lcom/konka/musicPlayer/musicPlayerActivity$8;->this$0:Lcom/konka/musicPlayer/musicPlayerActivity;

    # getter for: Lcom/konka/musicPlayer/musicPlayerActivity;->player:Landroid/media/MediaPlayer;
    invoke-static {v3}, Lcom/konka/musicPlayer/musicPlayerActivity;->access$7(Lcom/konka/musicPlayer/musicPlayerActivity;)Landroid/media/MediaPlayer;

    move-result-object v3

    invoke-virtual {v3}, Landroid/media/MediaPlayer;->getCurrentPosition()I

    move-result v3

    iget-object v4, p0, Lcom/konka/musicPlayer/musicPlayerActivity$8;->this$0:Lcom/konka/musicPlayer/musicPlayerActivity;

    # getter for: Lcom/konka/musicPlayer/musicPlayerActivity;->player:Landroid/media/MediaPlayer;
    invoke-static {v4}, Lcom/konka/musicPlayer/musicPlayerActivity;->access$7(Lcom/konka/musicPlayer/musicPlayerActivity;)Landroid/media/MediaPlayer;

    move-result-object v4

    invoke-virtual {v4}, Landroid/media/MediaPlayer;->getDuration()I

    move-result v4

    new-instance v5, Ljava/lang/StringBuilder;

    iget-object v6, p0, Lcom/konka/musicPlayer/musicPlayerActivity$8;->this$0:Lcom/konka/musicPlayer/musicPlayerActivity;

    # getter for: Lcom/konka/musicPlayer/musicPlayerActivity;->info:Lcom/konka/musicPlayer/MusicInfo;
    invoke-static {v6}, Lcom/konka/musicPlayer/musicPlayerActivity;->access$0(Lcom/konka/musicPlayer/musicPlayerActivity;)Lcom/konka/musicPlayer/MusicInfo;

    move-result-object v6

    invoke-virtual {v6}, Lcom/konka/musicPlayer/MusicInfo;->getData()Ljava/lang/String;

    move-result-object v6

    invoke-static {v6}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v6

    invoke-direct {v5, v6}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    const-string v6, "&ma"

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    invoke-virtual/range {v0 .. v5}, Lcom/konka/musicPlayer/musicPlayerActivity;->sendPlayerStateToPlayerService(Landroid/content/Context;IIILjava/lang/String;)V

    iget-object v3, p0, Lcom/konka/musicPlayer/musicPlayerActivity$8;->this$0:Lcom/konka/musicPlayer/musicPlayerActivity;

    iget-object v4, p0, Lcom/konka/musicPlayer/musicPlayerActivity$8;->this$0:Lcom/konka/musicPlayer/musicPlayerActivity;

    sget-object v0, Lcom/konka/mediaSharePlayer/PlayerState;->PLAY_STATUS_EXIT:Lcom/konka/mediaSharePlayer/PlayerState;

    invoke-virtual {v0}, Lcom/konka/mediaSharePlayer/PlayerState;->ordinal()I

    move-result v5

    iget-object v0, p0, Lcom/konka/musicPlayer/musicPlayerActivity$8;->this$0:Lcom/konka/musicPlayer/musicPlayerActivity;

    # getter for: Lcom/konka/musicPlayer/musicPlayerActivity;->player:Landroid/media/MediaPlayer;
    invoke-static {v0}, Lcom/konka/musicPlayer/musicPlayerActivity;->access$7(Lcom/konka/musicPlayer/musicPlayerActivity;)Landroid/media/MediaPlayer;

    move-result-object v0

    invoke-virtual {v0}, Landroid/media/MediaPlayer;->getCurrentPosition()I

    move-result v6

    iget-object v0, p0, Lcom/konka/musicPlayer/musicPlayerActivity$8;->this$0:Lcom/konka/musicPlayer/musicPlayerActivity;

    # getter for: Lcom/konka/musicPlayer/musicPlayerActivity;->player:Landroid/media/MediaPlayer;
    invoke-static {v0}, Lcom/konka/musicPlayer/musicPlayerActivity;->access$7(Lcom/konka/musicPlayer/musicPlayerActivity;)Landroid/media/MediaPlayer;

    move-result-object v0

    invoke-virtual {v0}, Landroid/media/MediaPlayer;->getDuration()I

    move-result v7

    new-instance v0, Ljava/lang/StringBuilder;

    iget-object v1, p0, Lcom/konka/musicPlayer/musicPlayerActivity$8;->this$0:Lcom/konka/musicPlayer/musicPlayerActivity;

    # getter for: Lcom/konka/musicPlayer/musicPlayerActivity;->info:Lcom/konka/musicPlayer/MusicInfo;
    invoke-static {v1}, Lcom/konka/musicPlayer/musicPlayerActivity;->access$0(Lcom/konka/musicPlayer/musicPlayerActivity;)Lcom/konka/musicPlayer/MusicInfo;

    move-result-object v1

    invoke-virtual {v1}, Lcom/konka/musicPlayer/MusicInfo;->getData()Ljava/lang/String;

    move-result-object v1

    invoke-static {v1}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    const-string v1, "&ma"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v8

    invoke-virtual/range {v3 .. v8}, Lcom/konka/musicPlayer/musicPlayerActivity;->sendPlayerStateToPlayerService(Landroid/content/Context;IIILjava/lang/String;)V

    iget-object v0, p0, Lcom/konka/musicPlayer/musicPlayerActivity$8;->this$0:Lcom/konka/musicPlayer/musicPlayerActivity;

    invoke-static {v0, v2, v2, v2}, Lcom/konka/mediaSharePlayer/sendPlayerState;->MediaPlayerActionSendBroadcastIsCreat(Landroid/content/Context;III)V

    iget-object v0, p0, Lcom/konka/musicPlayer/musicPlayerActivity$8;->this$0:Lcom/konka/musicPlayer/musicPlayerActivity;

    invoke-virtual {v0}, Lcom/konka/musicPlayer/musicPlayerActivity;->musicPlayerStop()V

    return v2
.end method
