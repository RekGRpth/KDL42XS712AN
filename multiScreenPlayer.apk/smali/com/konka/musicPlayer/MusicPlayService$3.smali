.class Lcom/konka/musicPlayer/MusicPlayService$3;
.super Ljava/lang/Object;
.source "MusicPlayService.java"

# interfaces
.implements Landroid/media/MediaPlayer$OnPreparedListener;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/konka/musicPlayer/MusicPlayService;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/konka/musicPlayer/MusicPlayService;


# direct methods
.method constructor <init>(Lcom/konka/musicPlayer/MusicPlayService;)V
    .locals 0

    iput-object p1, p0, Lcom/konka/musicPlayer/MusicPlayService$3;->this$0:Lcom/konka/musicPlayer/MusicPlayService;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onPrepared(Landroid/media/MediaPlayer;)V
    .locals 3
    .param p1    # Landroid/media/MediaPlayer;

    const-string v0, "playerOnPrepared"

    const-string v1, "PLAYER_PREPARE_END----------------"

    invoke-static {v0, v1}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    const-string v0, "noob"

    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "onprepared:"

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    iget-object v2, p0, Lcom/konka/musicPlayer/MusicPlayService$3;->this$0:Lcom/konka/musicPlayer/MusicPlayService;

    # getter for: Lcom/konka/musicPlayer/MusicPlayService;->currentTime:I
    invoke-static {v2}, Lcom/konka/musicPlayer/MusicPlayService;->access$3(Lcom/konka/musicPlayer/MusicPlayService;)I

    move-result v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    invoke-virtual {p1}, Landroid/media/MediaPlayer;->start()V

    iget-object v0, p0, Lcom/konka/musicPlayer/MusicPlayService$3;->this$0:Lcom/konka/musicPlayer/MusicPlayService;

    # getter for: Lcom/konka/musicPlayer/MusicPlayService;->currentTime:I
    invoke-static {v0}, Lcom/konka/musicPlayer/MusicPlayService;->access$3(Lcom/konka/musicPlayer/MusicPlayService;)I

    move-result v0

    if-lez v0, :cond_0

    iget-object v0, p0, Lcom/konka/musicPlayer/MusicPlayService$3;->this$0:Lcom/konka/musicPlayer/MusicPlayService;

    # getter for: Lcom/konka/musicPlayer/MusicPlayService;->player:Landroid/media/MediaPlayer;
    invoke-static {v0}, Lcom/konka/musicPlayer/MusicPlayService;->access$1(Lcom/konka/musicPlayer/MusicPlayService;)Landroid/media/MediaPlayer;

    move-result-object v0

    iget-object v1, p0, Lcom/konka/musicPlayer/MusicPlayService$3;->this$0:Lcom/konka/musicPlayer/MusicPlayService;

    # getter for: Lcom/konka/musicPlayer/MusicPlayService;->currentTime:I
    invoke-static {v1}, Lcom/konka/musicPlayer/MusicPlayService;->access$3(Lcom/konka/musicPlayer/MusicPlayService;)I

    move-result v1

    invoke-virtual {v0, v1}, Landroid/media/MediaPlayer;->seekTo(I)V

    :cond_0
    return-void
.end method
