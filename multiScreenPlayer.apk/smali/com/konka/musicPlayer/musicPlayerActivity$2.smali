.class Lcom/konka/musicPlayer/musicPlayerActivity$2;
.super Ljava/lang/Object;
.source "musicPlayerActivity.java"

# interfaces
.implements Landroid/view/View$OnClickListener;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/konka/musicPlayer/musicPlayerActivity;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/konka/musicPlayer/musicPlayerActivity;


# direct methods
.method constructor <init>(Lcom/konka/musicPlayer/musicPlayerActivity;)V
    .locals 0

    iput-object p1, p0, Lcom/konka/musicPlayer/musicPlayerActivity$2;->this$0:Lcom/konka/musicPlayer/musicPlayerActivity;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onClick(Landroid/view/View;)V
    .locals 8
    .param p1    # Landroid/view/View;

    const/4 v7, 0x1

    const/4 v6, 0x0

    const-string v0, "MultiScreeenMusicActivity:"

    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "getid: "

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {p1}, Landroid/view/View;->getId()I

    move-result v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    invoke-virtual {p1}, Landroid/view/View;->getId()I

    move-result v0

    packed-switch v0, :pswitch_data_0

    :goto_0
    :pswitch_0
    return-void

    :pswitch_1
    iget-object v0, p0, Lcom/konka/musicPlayer/musicPlayerActivity$2;->this$0:Lcom/konka/musicPlayer/musicPlayerActivity;

    iget-object v1, p0, Lcom/konka/musicPlayer/musicPlayerActivity$2;->this$0:Lcom/konka/musicPlayer/musicPlayerActivity;

    # getter for: Lcom/konka/musicPlayer/musicPlayerActivity;->info:Lcom/konka/musicPlayer/MusicInfo;
    invoke-static {v1}, Lcom/konka/musicPlayer/musicPlayerActivity;->access$0(Lcom/konka/musicPlayer/musicPlayerActivity;)Lcom/konka/musicPlayer/MusicInfo;

    move-result-object v1

    # invokes: Lcom/konka/musicPlayer/musicPlayerActivity;->playMusicService(ILcom/konka/musicPlayer/MusicInfo;)V
    invoke-static {v0, v7, v1}, Lcom/konka/musicPlayer/musicPlayerActivity;->access$3(Lcom/konka/musicPlayer/musicPlayerActivity;ILcom/konka/musicPlayer/MusicInfo;)V

    iget-object v0, p0, Lcom/konka/musicPlayer/musicPlayerActivity$2;->this$0:Lcom/konka/musicPlayer/musicPlayerActivity;

    iget-object v1, p0, Lcom/konka/musicPlayer/musicPlayerActivity$2;->this$0:Lcom/konka/musicPlayer/musicPlayerActivity;

    # getter for: Lcom/konka/musicPlayer/musicPlayerActivity;->playBtn:Landroid/widget/Button;
    invoke-static {v1}, Lcom/konka/musicPlayer/musicPlayerActivity;->access$5(Lcom/konka/musicPlayer/musicPlayerActivity;)Landroid/widget/Button;

    move-result-object v1

    iget-object v2, p0, Lcom/konka/musicPlayer/musicPlayerActivity$2;->this$0:Lcom/konka/musicPlayer/musicPlayerActivity;

    # getter for: Lcom/konka/musicPlayer/musicPlayerActivity;->pauseBtn:Landroid/widget/Button;
    invoke-static {v2}, Lcom/konka/musicPlayer/musicPlayerActivity;->access$6(Lcom/konka/musicPlayer/musicPlayerActivity;)Landroid/widget/Button;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Lcom/konka/musicPlayer/musicPlayerActivity;->setBtnShowHide(Landroid/widget/Button;Landroid/widget/Button;)V

    iget-object v0, p0, Lcom/konka/musicPlayer/musicPlayerActivity$2;->this$0:Lcom/konka/musicPlayer/musicPlayerActivity;

    # getter for: Lcom/konka/musicPlayer/musicPlayerActivity;->pauseBtn:Landroid/widget/Button;
    invoke-static {v0}, Lcom/konka/musicPlayer/musicPlayerActivity;->access$6(Lcom/konka/musicPlayer/musicPlayerActivity;)Landroid/widget/Button;

    move-result-object v0

    const v1, 0x7f020015    # com.konka.mediaSharePlayer.R.drawable.com_pause_sel

    invoke-virtual {v0, v1}, Landroid/widget/Button;->setBackgroundResource(I)V

    iget-object v0, p0, Lcom/konka/musicPlayer/musicPlayerActivity$2;->this$0:Lcom/konka/musicPlayer/musicPlayerActivity;

    iput-boolean v6, v0, Lcom/konka/musicPlayer/musicPlayerActivity;->isPause:Z

    iget-object v0, p0, Lcom/konka/musicPlayer/musicPlayerActivity$2;->this$0:Lcom/konka/musicPlayer/musicPlayerActivity;

    iget-boolean v0, v0, Lcom/konka/musicPlayer/musicPlayerActivity;->isPlayerPrepare:Z

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/konka/musicPlayer/musicPlayerActivity$2;->this$0:Lcom/konka/musicPlayer/musicPlayerActivity;

    iget-object v1, p0, Lcom/konka/musicPlayer/musicPlayerActivity$2;->this$0:Lcom/konka/musicPlayer/musicPlayerActivity;

    sget-object v2, Lcom/konka/mediaSharePlayer/PlayerState;->PLAY_STATUS_PLAY:Lcom/konka/mediaSharePlayer/PlayerState;

    invoke-virtual {v2}, Lcom/konka/mediaSharePlayer/PlayerState;->ordinal()I

    move-result v2

    iget-object v3, p0, Lcom/konka/musicPlayer/musicPlayerActivity$2;->this$0:Lcom/konka/musicPlayer/musicPlayerActivity;

    # getter for: Lcom/konka/musicPlayer/musicPlayerActivity;->player:Landroid/media/MediaPlayer;
    invoke-static {v3}, Lcom/konka/musicPlayer/musicPlayerActivity;->access$7(Lcom/konka/musicPlayer/musicPlayerActivity;)Landroid/media/MediaPlayer;

    move-result-object v3

    invoke-virtual {v3}, Landroid/media/MediaPlayer;->getCurrentPosition()I

    move-result v3

    iget-object v4, p0, Lcom/konka/musicPlayer/musicPlayerActivity$2;->this$0:Lcom/konka/musicPlayer/musicPlayerActivity;

    # getter for: Lcom/konka/musicPlayer/musicPlayerActivity;->player:Landroid/media/MediaPlayer;
    invoke-static {v4}, Lcom/konka/musicPlayer/musicPlayerActivity;->access$7(Lcom/konka/musicPlayer/musicPlayerActivity;)Landroid/media/MediaPlayer;

    move-result-object v4

    invoke-virtual {v4}, Landroid/media/MediaPlayer;->getDuration()I

    move-result v4

    iget-object v5, p0, Lcom/konka/musicPlayer/musicPlayerActivity$2;->this$0:Lcom/konka/musicPlayer/musicPlayerActivity;

    # getter for: Lcom/konka/musicPlayer/musicPlayerActivity;->info:Lcom/konka/musicPlayer/MusicInfo;
    invoke-static {v5}, Lcom/konka/musicPlayer/musicPlayerActivity;->access$0(Lcom/konka/musicPlayer/musicPlayerActivity;)Lcom/konka/musicPlayer/MusicInfo;

    move-result-object v5

    invoke-virtual {v5}, Lcom/konka/musicPlayer/MusicInfo;->getData()Ljava/lang/String;

    move-result-object v5

    invoke-virtual/range {v0 .. v5}, Lcom/konka/musicPlayer/musicPlayerActivity;->sendPlayerStateToPlayerService(Landroid/content/Context;IIILjava/lang/String;)V

    :cond_0
    iget-object v0, p0, Lcom/konka/musicPlayer/musicPlayerActivity$2;->this$0:Lcom/konka/musicPlayer/musicPlayerActivity;

    invoke-static {v0, v7}, Lcom/konka/musicPlayer/musicPlayerActivity;->access$8(Lcom/konka/musicPlayer/musicPlayerActivity;I)V

    goto :goto_0

    :pswitch_2
    iget-object v0, p0, Lcom/konka/musicPlayer/musicPlayerActivity$2;->this$0:Lcom/konka/musicPlayer/musicPlayerActivity;

    iget-object v1, p0, Lcom/konka/musicPlayer/musicPlayerActivity$2;->this$0:Lcom/konka/musicPlayer/musicPlayerActivity;

    # getter for: Lcom/konka/musicPlayer/musicPlayerActivity;->info:Lcom/konka/musicPlayer/MusicInfo;
    invoke-static {v1}, Lcom/konka/musicPlayer/musicPlayerActivity;->access$0(Lcom/konka/musicPlayer/musicPlayerActivity;)Lcom/konka/musicPlayer/MusicInfo;

    move-result-object v1

    # invokes: Lcom/konka/musicPlayer/musicPlayerActivity;->playMusicService(ILcom/konka/musicPlayer/MusicInfo;)V
    invoke-static {v0, v6, v1}, Lcom/konka/musicPlayer/musicPlayerActivity;->access$3(Lcom/konka/musicPlayer/musicPlayerActivity;ILcom/konka/musicPlayer/MusicInfo;)V

    iget-object v0, p0, Lcom/konka/musicPlayer/musicPlayerActivity$2;->this$0:Lcom/konka/musicPlayer/musicPlayerActivity;

    iget-object v1, p0, Lcom/konka/musicPlayer/musicPlayerActivity$2;->this$0:Lcom/konka/musicPlayer/musicPlayerActivity;

    # getter for: Lcom/konka/musicPlayer/musicPlayerActivity;->pauseBtn:Landroid/widget/Button;
    invoke-static {v1}, Lcom/konka/musicPlayer/musicPlayerActivity;->access$6(Lcom/konka/musicPlayer/musicPlayerActivity;)Landroid/widget/Button;

    move-result-object v1

    iget-object v2, p0, Lcom/konka/musicPlayer/musicPlayerActivity$2;->this$0:Lcom/konka/musicPlayer/musicPlayerActivity;

    # getter for: Lcom/konka/musicPlayer/musicPlayerActivity;->playBtn:Landroid/widget/Button;
    invoke-static {v2}, Lcom/konka/musicPlayer/musicPlayerActivity;->access$5(Lcom/konka/musicPlayer/musicPlayerActivity;)Landroid/widget/Button;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Lcom/konka/musicPlayer/musicPlayerActivity;->setBtnShowHide(Landroid/widget/Button;Landroid/widget/Button;)V

    iget-object v0, p0, Lcom/konka/musicPlayer/musicPlayerActivity$2;->this$0:Lcom/konka/musicPlayer/musicPlayerActivity;

    # getter for: Lcom/konka/musicPlayer/musicPlayerActivity;->playBtn:Landroid/widget/Button;
    invoke-static {v0}, Lcom/konka/musicPlayer/musicPlayerActivity;->access$5(Lcom/konka/musicPlayer/musicPlayerActivity;)Landroid/widget/Button;

    move-result-object v0

    const v1, 0x7f02001e    # com.konka.mediaSharePlayer.R.drawable.com_play_sel

    invoke-virtual {v0, v1}, Landroid/widget/Button;->setBackgroundResource(I)V

    iget-object v0, p0, Lcom/konka/musicPlayer/musicPlayerActivity$2;->this$0:Lcom/konka/musicPlayer/musicPlayerActivity;

    iput-boolean v7, v0, Lcom/konka/musicPlayer/musicPlayerActivity;->isPause:Z

    iget-object v0, p0, Lcom/konka/musicPlayer/musicPlayerActivity$2;->this$0:Lcom/konka/musicPlayer/musicPlayerActivity;

    iget-boolean v0, v0, Lcom/konka/musicPlayer/musicPlayerActivity;->isPlayerPrepare:Z

    if-eqz v0, :cond_1

    iget-object v0, p0, Lcom/konka/musicPlayer/musicPlayerActivity$2;->this$0:Lcom/konka/musicPlayer/musicPlayerActivity;

    iget-object v1, p0, Lcom/konka/musicPlayer/musicPlayerActivity$2;->this$0:Lcom/konka/musicPlayer/musicPlayerActivity;

    sget-object v2, Lcom/konka/mediaSharePlayer/PlayerState;->PLAY_STATUS_PAUSE:Lcom/konka/mediaSharePlayer/PlayerState;

    invoke-virtual {v2}, Lcom/konka/mediaSharePlayer/PlayerState;->ordinal()I

    move-result v2

    iget-object v3, p0, Lcom/konka/musicPlayer/musicPlayerActivity$2;->this$0:Lcom/konka/musicPlayer/musicPlayerActivity;

    # getter for: Lcom/konka/musicPlayer/musicPlayerActivity;->player:Landroid/media/MediaPlayer;
    invoke-static {v3}, Lcom/konka/musicPlayer/musicPlayerActivity;->access$7(Lcom/konka/musicPlayer/musicPlayerActivity;)Landroid/media/MediaPlayer;

    move-result-object v3

    invoke-virtual {v3}, Landroid/media/MediaPlayer;->getCurrentPosition()I

    move-result v3

    iget-object v4, p0, Lcom/konka/musicPlayer/musicPlayerActivity$2;->this$0:Lcom/konka/musicPlayer/musicPlayerActivity;

    # getter for: Lcom/konka/musicPlayer/musicPlayerActivity;->player:Landroid/media/MediaPlayer;
    invoke-static {v4}, Lcom/konka/musicPlayer/musicPlayerActivity;->access$7(Lcom/konka/musicPlayer/musicPlayerActivity;)Landroid/media/MediaPlayer;

    move-result-object v4

    invoke-virtual {v4}, Landroid/media/MediaPlayer;->getDuration()I

    move-result v4

    iget-object v5, p0, Lcom/konka/musicPlayer/musicPlayerActivity$2;->this$0:Lcom/konka/musicPlayer/musicPlayerActivity;

    # getter for: Lcom/konka/musicPlayer/musicPlayerActivity;->info:Lcom/konka/musicPlayer/MusicInfo;
    invoke-static {v5}, Lcom/konka/musicPlayer/musicPlayerActivity;->access$0(Lcom/konka/musicPlayer/musicPlayerActivity;)Lcom/konka/musicPlayer/MusicInfo;

    move-result-object v5

    invoke-virtual {v5}, Lcom/konka/musicPlayer/MusicInfo;->getData()Ljava/lang/String;

    move-result-object v5

    invoke-virtual/range {v0 .. v5}, Lcom/konka/musicPlayer/musicPlayerActivity;->sendPlayerStateToPlayerService(Landroid/content/Context;IIILjava/lang/String;)V

    :cond_1
    iget-object v0, p0, Lcom/konka/musicPlayer/musicPlayerActivity$2;->this$0:Lcom/konka/musicPlayer/musicPlayerActivity;

    invoke-static {v0, v6}, Lcom/konka/musicPlayer/musicPlayerActivity;->access$8(Lcom/konka/musicPlayer/musicPlayerActivity;I)V

    goto/16 :goto_0

    nop

    :pswitch_data_0
    .packed-switch 0x7f0b0004
        :pswitch_0    # com.konka.mediaSharePlayer.R.id.btn_seekback
        :pswitch_1    # com.konka.mediaSharePlayer.R.id.btn_music_play
        :pswitch_2    # com.konka.mediaSharePlayer.R.id.btn_music_pause
        :pswitch_0    # com.konka.mediaSharePlayer.R.id.btn_seekforward
    .end packed-switch
.end method
