.class Lcom/konka/musicPlayer/musicPlayerActivity$SeekToReceiver;
.super Landroid/content/BroadcastReceiver;
.source "musicPlayerActivity.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/konka/musicPlayer/musicPlayerActivity;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x2
    name = "SeekToReceiver"
.end annotation


# instance fields
.field final synthetic this$0:Lcom/konka/musicPlayer/musicPlayerActivity;


# direct methods
.method private constructor <init>(Lcom/konka/musicPlayer/musicPlayerActivity;)V
    .locals 0

    iput-object p1, p0, Lcom/konka/musicPlayer/musicPlayerActivity$SeekToReceiver;->this$0:Lcom/konka/musicPlayer/musicPlayerActivity;

    invoke-direct {p0}, Landroid/content/BroadcastReceiver;-><init>()V

    return-void
.end method

.method synthetic constructor <init>(Lcom/konka/musicPlayer/musicPlayerActivity;Lcom/konka/musicPlayer/musicPlayerActivity$SeekToReceiver;)V
    .locals 0

    invoke-direct {p0, p1}, Lcom/konka/musicPlayer/musicPlayerActivity$SeekToReceiver;-><init>(Lcom/konka/musicPlayer/musicPlayerActivity;)V

    return-void
.end method


# virtual methods
.method public onReceive(Landroid/content/Context;Landroid/content/Intent;)V
    .locals 4
    .param p1    # Landroid/content/Context;
    .param p2    # Landroid/content/Intent;

    const-string v1, "seekto"

    const/4 v2, 0x0

    invoke-virtual {p2, v1, v2}, Landroid/content/Intent;->getIntExtra(Ljava/lang/String;I)I

    move-result v0

    const-string v1, "MultiScreeenMusicActivity:"

    new-instance v2, Ljava/lang/StringBuilder;

    const-string v3, "richard seekto000: "

    invoke-direct {v2, v3}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    if-ltz v0, :cond_0

    iget-object v1, p0, Lcom/konka/musicPlayer/musicPlayerActivity$SeekToReceiver;->this$0:Lcom/konka/musicPlayer/musicPlayerActivity;

    # getter for: Lcom/konka/musicPlayer/musicPlayerActivity;->player:Landroid/media/MediaPlayer;
    invoke-static {v1}, Lcom/konka/musicPlayer/musicPlayerActivity;->access$7(Lcom/konka/musicPlayer/musicPlayerActivity;)Landroid/media/MediaPlayer;

    move-result-object v1

    invoke-virtual {v1}, Landroid/media/MediaPlayer;->getDuration()I

    move-result v1

    if-lt v1, v0, :cond_0

    const-string v1, "MultiScreeenMusicActivity:"

    new-instance v2, Ljava/lang/StringBuilder;

    const-string v3, "richard seekto111: "

    invoke-direct {v2, v3}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    iget-object v1, p0, Lcom/konka/musicPlayer/musicPlayerActivity$SeekToReceiver;->this$0:Lcom/konka/musicPlayer/musicPlayerActivity;

    # invokes: Lcom/konka/musicPlayer/musicPlayerActivity;->playPos(I)V
    invoke-static {v1, v0}, Lcom/konka/musicPlayer/musicPlayerActivity;->access$16(Lcom/konka/musicPlayer/musicPlayerActivity;I)V

    :cond_0
    return-void
.end method
