.class Lcom/konka/musicPlayer/musicPlayerActivity$6;
.super Ljava/lang/Object;
.source "musicPlayerActivity.java"

# interfaces
.implements Landroid/media/MediaPlayer$OnSeekCompleteListener;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/konka/musicPlayer/musicPlayerActivity;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/konka/musicPlayer/musicPlayerActivity;


# direct methods
.method constructor <init>(Lcom/konka/musicPlayer/musicPlayerActivity;)V
    .locals 0

    iput-object p1, p0, Lcom/konka/musicPlayer/musicPlayerActivity$6;->this$0:Lcom/konka/musicPlayer/musicPlayerActivity;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onSeekComplete(Landroid/media/MediaPlayer;)V
    .locals 9
    .param p1    # Landroid/media/MediaPlayer;

    const v8, 0x7f020015    # com.konka.mediaSharePlayer.R.drawable.com_pause_sel

    const/4 v7, 0x0

    const-string v0, "MultiScreeenMusicActivity:"

    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "media player is seek "

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {p1}, Landroid/media/MediaPlayer;->getCurrentPosition()I

    move-result v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, "----"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {p1}, Landroid/media/MediaPlayer;->getDuration()I

    move-result v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    iget-object v0, p0, Lcom/konka/musicPlayer/musicPlayerActivity$6;->this$0:Lcom/konka/musicPlayer/musicPlayerActivity;

    # getter for: Lcom/konka/musicPlayer/musicPlayerActivity;->resumePos:I
    invoke-static {v0}, Lcom/konka/musicPlayer/musicPlayerActivity;->access$4(Lcom/konka/musicPlayer/musicPlayerActivity;)I

    move-result v0

    if-lez v0, :cond_0

    iget-object v0, p0, Lcom/konka/musicPlayer/musicPlayerActivity$6;->this$0:Lcom/konka/musicPlayer/musicPlayerActivity;

    iget-object v1, p0, Lcom/konka/musicPlayer/musicPlayerActivity$6;->this$0:Lcom/konka/musicPlayer/musicPlayerActivity;

    sget-object v2, Lcom/konka/mediaSharePlayer/PlayerState;->PLAY_STATUS_RESUMEPLAY:Lcom/konka/mediaSharePlayer/PlayerState;

    invoke-virtual {v2}, Lcom/konka/mediaSharePlayer/PlayerState;->ordinal()I

    move-result v2

    iget-object v3, p0, Lcom/konka/musicPlayer/musicPlayerActivity$6;->this$0:Lcom/konka/musicPlayer/musicPlayerActivity;

    # getter for: Lcom/konka/musicPlayer/musicPlayerActivity;->player:Landroid/media/MediaPlayer;
    invoke-static {v3}, Lcom/konka/musicPlayer/musicPlayerActivity;->access$7(Lcom/konka/musicPlayer/musicPlayerActivity;)Landroid/media/MediaPlayer;

    move-result-object v3

    invoke-virtual {v3}, Landroid/media/MediaPlayer;->getCurrentPosition()I

    move-result v3

    iget-object v4, p0, Lcom/konka/musicPlayer/musicPlayerActivity$6;->this$0:Lcom/konka/musicPlayer/musicPlayerActivity;

    # getter for: Lcom/konka/musicPlayer/musicPlayerActivity;->player:Landroid/media/MediaPlayer;
    invoke-static {v4}, Lcom/konka/musicPlayer/musicPlayerActivity;->access$7(Lcom/konka/musicPlayer/musicPlayerActivity;)Landroid/media/MediaPlayer;

    move-result-object v4

    invoke-virtual {v4}, Landroid/media/MediaPlayer;->getDuration()I

    move-result v4

    new-instance v5, Ljava/lang/StringBuilder;

    iget-object v6, p0, Lcom/konka/musicPlayer/musicPlayerActivity$6;->this$0:Lcom/konka/musicPlayer/musicPlayerActivity;

    # getter for: Lcom/konka/musicPlayer/musicPlayerActivity;->info:Lcom/konka/musicPlayer/MusicInfo;
    invoke-static {v6}, Lcom/konka/musicPlayer/musicPlayerActivity;->access$0(Lcom/konka/musicPlayer/musicPlayerActivity;)Lcom/konka/musicPlayer/MusicInfo;

    move-result-object v6

    invoke-virtual {v6}, Lcom/konka/musicPlayer/MusicInfo;->getData()Ljava/lang/String;

    move-result-object v6

    invoke-static {v6}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v6

    invoke-direct {v5, v6}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    const-string v6, "&ma"

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    invoke-virtual/range {v0 .. v5}, Lcom/konka/musicPlayer/musicPlayerActivity;->sendPlayerStateToPlayerService(Landroid/content/Context;IIILjava/lang/String;)V

    iget-object v0, p0, Lcom/konka/musicPlayer/musicPlayerActivity$6;->this$0:Lcom/konka/musicPlayer/musicPlayerActivity;

    invoke-static {v0, v7}, Lcom/konka/musicPlayer/musicPlayerActivity;->access$22(Lcom/konka/musicPlayer/musicPlayerActivity;I)V

    :goto_0
    iget-object v0, p0, Lcom/konka/musicPlayer/musicPlayerActivity$6;->this$0:Lcom/konka/musicPlayer/musicPlayerActivity;

    iget-object v1, p0, Lcom/konka/musicPlayer/musicPlayerActivity$6;->this$0:Lcom/konka/musicPlayer/musicPlayerActivity;

    # getter for: Lcom/konka/musicPlayer/musicPlayerActivity;->playBtn:Landroid/widget/Button;
    invoke-static {v1}, Lcom/konka/musicPlayer/musicPlayerActivity;->access$5(Lcom/konka/musicPlayer/musicPlayerActivity;)Landroid/widget/Button;

    move-result-object v1

    iget-object v2, p0, Lcom/konka/musicPlayer/musicPlayerActivity$6;->this$0:Lcom/konka/musicPlayer/musicPlayerActivity;

    # getter for: Lcom/konka/musicPlayer/musicPlayerActivity;->pauseBtn:Landroid/widget/Button;
    invoke-static {v2}, Lcom/konka/musicPlayer/musicPlayerActivity;->access$6(Lcom/konka/musicPlayer/musicPlayerActivity;)Landroid/widget/Button;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Lcom/konka/musicPlayer/musicPlayerActivity;->setBtnShowHide(Landroid/widget/Button;Landroid/widget/Button;)V

    iget-object v0, p0, Lcom/konka/musicPlayer/musicPlayerActivity$6;->this$0:Lcom/konka/musicPlayer/musicPlayerActivity;

    # getter for: Lcom/konka/musicPlayer/musicPlayerActivity;->playBtn:Landroid/widget/Button;
    invoke-static {v0}, Lcom/konka/musicPlayer/musicPlayerActivity;->access$5(Lcom/konka/musicPlayer/musicPlayerActivity;)Landroid/widget/Button;

    move-result-object v0

    invoke-virtual {v0, v8}, Landroid/widget/Button;->setBackgroundResource(I)V

    iget-object v0, p0, Lcom/konka/musicPlayer/musicPlayerActivity$6;->this$0:Lcom/konka/musicPlayer/musicPlayerActivity;

    # getter for: Lcom/konka/musicPlayer/musicPlayerActivity;->pauseBtn:Landroid/widget/Button;
    invoke-static {v0}, Lcom/konka/musicPlayer/musicPlayerActivity;->access$6(Lcom/konka/musicPlayer/musicPlayerActivity;)Landroid/widget/Button;

    move-result-object v0

    invoke-virtual {v0, v8}, Landroid/widget/Button;->setBackgroundResource(I)V

    iget-object v0, p0, Lcom/konka/musicPlayer/musicPlayerActivity$6;->this$0:Lcom/konka/musicPlayer/musicPlayerActivity;

    iput-boolean v7, v0, Lcom/konka/musicPlayer/musicPlayerActivity;->isPause:Z

    iget-object v0, p0, Lcom/konka/musicPlayer/musicPlayerActivity$6;->this$0:Lcom/konka/musicPlayer/musicPlayerActivity;

    const/4 v1, 0x1

    invoke-static {v0, v1}, Lcom/konka/musicPlayer/musicPlayerActivity;->access$8(Lcom/konka/musicPlayer/musicPlayerActivity;I)V

    return-void

    :cond_0
    iget-object v0, p0, Lcom/konka/musicPlayer/musicPlayerActivity$6;->this$0:Lcom/konka/musicPlayer/musicPlayerActivity;

    iget-object v1, p0, Lcom/konka/musicPlayer/musicPlayerActivity$6;->this$0:Lcom/konka/musicPlayer/musicPlayerActivity;

    sget-object v2, Lcom/konka/mediaSharePlayer/PlayerState;->PLAY_STATUS_SEEK:Lcom/konka/mediaSharePlayer/PlayerState;

    invoke-virtual {v2}, Lcom/konka/mediaSharePlayer/PlayerState;->ordinal()I

    move-result v2

    iget-object v3, p0, Lcom/konka/musicPlayer/musicPlayerActivity$6;->this$0:Lcom/konka/musicPlayer/musicPlayerActivity;

    # getter for: Lcom/konka/musicPlayer/musicPlayerActivity;->player:Landroid/media/MediaPlayer;
    invoke-static {v3}, Lcom/konka/musicPlayer/musicPlayerActivity;->access$7(Lcom/konka/musicPlayer/musicPlayerActivity;)Landroid/media/MediaPlayer;

    move-result-object v3

    invoke-virtual {v3}, Landroid/media/MediaPlayer;->getCurrentPosition()I

    move-result v3

    iget-object v4, p0, Lcom/konka/musicPlayer/musicPlayerActivity$6;->this$0:Lcom/konka/musicPlayer/musicPlayerActivity;

    # getter for: Lcom/konka/musicPlayer/musicPlayerActivity;->player:Landroid/media/MediaPlayer;
    invoke-static {v4}, Lcom/konka/musicPlayer/musicPlayerActivity;->access$7(Lcom/konka/musicPlayer/musicPlayerActivity;)Landroid/media/MediaPlayer;

    move-result-object v4

    invoke-virtual {v4}, Landroid/media/MediaPlayer;->getDuration()I

    move-result v4

    new-instance v5, Ljava/lang/StringBuilder;

    iget-object v6, p0, Lcom/konka/musicPlayer/musicPlayerActivity$6;->this$0:Lcom/konka/musicPlayer/musicPlayerActivity;

    # getter for: Lcom/konka/musicPlayer/musicPlayerActivity;->info:Lcom/konka/musicPlayer/MusicInfo;
    invoke-static {v6}, Lcom/konka/musicPlayer/musicPlayerActivity;->access$0(Lcom/konka/musicPlayer/musicPlayerActivity;)Lcom/konka/musicPlayer/MusicInfo;

    move-result-object v6

    invoke-virtual {v6}, Lcom/konka/musicPlayer/MusicInfo;->getData()Ljava/lang/String;

    move-result-object v6

    invoke-static {v6}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v6

    invoke-direct {v5, v6}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    const-string v6, "&ma"

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    invoke-virtual/range {v0 .. v5}, Lcom/konka/musicPlayer/musicPlayerActivity;->sendPlayerStateToPlayerService(Landroid/content/Context;IIILjava/lang/String;)V

    goto :goto_0
.end method
