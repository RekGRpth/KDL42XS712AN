.class public final Lorg/tukaani/xz/lz/LZDecoder;
.super Ljava/lang/Object;


# instance fields
.field private final a:[B

.field private b:I

.field private c:I

.field private d:I

.field private e:I

.field private f:I

.field private g:I


# direct methods
.method public constructor <init>(I[B)V
    .locals 1

    const/4 v0, 0x0

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    iput v0, p0, Lorg/tukaani/xz/lz/LZDecoder;->b:I

    iput v0, p0, Lorg/tukaani/xz/lz/LZDecoder;->c:I

    iput v0, p0, Lorg/tukaani/xz/lz/LZDecoder;->d:I

    iput v0, p0, Lorg/tukaani/xz/lz/LZDecoder;->e:I

    iput v0, p0, Lorg/tukaani/xz/lz/LZDecoder;->f:I

    iput v0, p0, Lorg/tukaani/xz/lz/LZDecoder;->g:I

    new-array v0, p1, [B

    iput-object v0, p0, Lorg/tukaani/xz/lz/LZDecoder;->a:[B

    return-void
.end method


# virtual methods
.method public final a([BI)I
    .locals 3

    iget v0, p0, Lorg/tukaani/xz/lz/LZDecoder;->c:I

    iget v1, p0, Lorg/tukaani/xz/lz/LZDecoder;->b:I

    sub-int/2addr v0, v1

    iget v1, p0, Lorg/tukaani/xz/lz/LZDecoder;->c:I

    iget-object v2, p0, Lorg/tukaani/xz/lz/LZDecoder;->a:[B

    array-length v2, v2

    if-ne v1, v2, :cond_0

    const/4 v1, 0x0

    iput v1, p0, Lorg/tukaani/xz/lz/LZDecoder;->c:I

    :cond_0
    iget-object v1, p0, Lorg/tukaani/xz/lz/LZDecoder;->a:[B

    iget v2, p0, Lorg/tukaani/xz/lz/LZDecoder;->b:I

    invoke-static {v1, v2, p1, p2, v0}, Ljava/lang/System;->arraycopy(Ljava/lang/Object;ILjava/lang/Object;II)V

    iget v1, p0, Lorg/tukaani/xz/lz/LZDecoder;->c:I

    iput v1, p0, Lorg/tukaani/xz/lz/LZDecoder;->b:I

    return v0
.end method

.method public final a()V
    .locals 3

    const/4 v2, 0x0

    iput v2, p0, Lorg/tukaani/xz/lz/LZDecoder;->b:I

    iput v2, p0, Lorg/tukaani/xz/lz/LZDecoder;->c:I

    iput v2, p0, Lorg/tukaani/xz/lz/LZDecoder;->d:I

    iput v2, p0, Lorg/tukaani/xz/lz/LZDecoder;->e:I

    iget-object v0, p0, Lorg/tukaani/xz/lz/LZDecoder;->a:[B

    iget-object v1, p0, Lorg/tukaani/xz/lz/LZDecoder;->a:[B

    array-length v1, v1

    add-int/lit8 v1, v1, -0x1

    aput-byte v2, v0, v1

    return-void
.end method

.method public final a(B)V
    .locals 3

    iget-object v0, p0, Lorg/tukaani/xz/lz/LZDecoder;->a:[B

    iget v1, p0, Lorg/tukaani/xz/lz/LZDecoder;->c:I

    add-int/lit8 v2, v1, 0x1

    iput v2, p0, Lorg/tukaani/xz/lz/LZDecoder;->c:I

    aput-byte p1, v0, v1

    iget v0, p0, Lorg/tukaani/xz/lz/LZDecoder;->d:I

    iget v1, p0, Lorg/tukaani/xz/lz/LZDecoder;->c:I

    if-ge v0, v1, :cond_0

    iget v0, p0, Lorg/tukaani/xz/lz/LZDecoder;->c:I

    iput v0, p0, Lorg/tukaani/xz/lz/LZDecoder;->d:I

    :cond_0
    return-void
.end method

.method public final a(I)V
    .locals 2

    iget-object v0, p0, Lorg/tukaani/xz/lz/LZDecoder;->a:[B

    array-length v0, v0

    iget v1, p0, Lorg/tukaani/xz/lz/LZDecoder;->c:I

    sub-int/2addr v0, v1

    if-gt v0, p1, :cond_0

    iget-object v0, p0, Lorg/tukaani/xz/lz/LZDecoder;->a:[B

    array-length v0, v0

    iput v0, p0, Lorg/tukaani/xz/lz/LZDecoder;->e:I

    :goto_0
    return-void

    :cond_0
    iget v0, p0, Lorg/tukaani/xz/lz/LZDecoder;->c:I

    add-int/2addr v0, p1

    iput v0, p0, Lorg/tukaani/xz/lz/LZDecoder;->e:I

    goto :goto_0
.end method

.method public final a(II)V
    .locals 6

    if-ltz p1, :cond_0

    iget v0, p0, Lorg/tukaani/xz/lz/LZDecoder;->d:I

    if-lt p1, v0, :cond_1

    :cond_0
    new-instance v0, Lorg/tukaani/xz/CorruptedInputException;

    invoke-direct {v0}, Lorg/tukaani/xz/CorruptedInputException;-><init>()V

    throw v0

    :cond_1
    iget v0, p0, Lorg/tukaani/xz/lz/LZDecoder;->e:I

    iget v1, p0, Lorg/tukaani/xz/lz/LZDecoder;->c:I

    sub-int/2addr v0, v1

    invoke-static {v0, p2}, Ljava/lang/Math;->min(II)I

    move-result v1

    sub-int v0, p2, v1

    iput v0, p0, Lorg/tukaani/xz/lz/LZDecoder;->f:I

    iput p1, p0, Lorg/tukaani/xz/lz/LZDecoder;->g:I

    iget v0, p0, Lorg/tukaani/xz/lz/LZDecoder;->c:I

    sub-int/2addr v0, p1

    add-int/lit8 v0, v0, -0x1

    iget v2, p0, Lorg/tukaani/xz/lz/LZDecoder;->c:I

    if-lt p1, v2, :cond_2

    iget-object v2, p0, Lorg/tukaani/xz/lz/LZDecoder;->a:[B

    array-length v2, v2

    add-int/2addr v0, v2

    :cond_2
    iget-object v3, p0, Lorg/tukaani/xz/lz/LZDecoder;->a:[B

    iget v4, p0, Lorg/tukaani/xz/lz/LZDecoder;->c:I

    add-int/lit8 v2, v4, 0x1

    iput v2, p0, Lorg/tukaani/xz/lz/LZDecoder;->c:I

    iget-object v5, p0, Lorg/tukaani/xz/lz/LZDecoder;->a:[B

    add-int/lit8 v2, v0, 0x1

    aget-byte v0, v5, v0

    aput-byte v0, v3, v4

    iget-object v0, p0, Lorg/tukaani/xz/lz/LZDecoder;->a:[B

    array-length v0, v0

    if-ne v2, v0, :cond_4

    const/4 v0, 0x0

    :goto_0
    add-int/lit8 v1, v1, -0x1

    if-gtz v1, :cond_2

    iget v0, p0, Lorg/tukaani/xz/lz/LZDecoder;->d:I

    iget v1, p0, Lorg/tukaani/xz/lz/LZDecoder;->c:I

    if-ge v0, v1, :cond_3

    iget v0, p0, Lorg/tukaani/xz/lz/LZDecoder;->c:I

    iput v0, p0, Lorg/tukaani/xz/lz/LZDecoder;->d:I

    :cond_3
    return-void

    :cond_4
    move v0, v2

    goto :goto_0
.end method

.method public final a(Ljava/io/DataInputStream;I)V
    .locals 3

    iget-object v0, p0, Lorg/tukaani/xz/lz/LZDecoder;->a:[B

    array-length v0, v0

    iget v1, p0, Lorg/tukaani/xz/lz/LZDecoder;->c:I

    sub-int/2addr v0, v1

    invoke-static {v0, p2}, Ljava/lang/Math;->min(II)I

    move-result v0

    iget-object v1, p0, Lorg/tukaani/xz/lz/LZDecoder;->a:[B

    iget v2, p0, Lorg/tukaani/xz/lz/LZDecoder;->c:I

    invoke-virtual {p1, v1, v2, v0}, Ljava/io/DataInputStream;->readFully([BII)V

    iget v1, p0, Lorg/tukaani/xz/lz/LZDecoder;->c:I

    add-int/2addr v0, v1

    iput v0, p0, Lorg/tukaani/xz/lz/LZDecoder;->c:I

    iget v0, p0, Lorg/tukaani/xz/lz/LZDecoder;->d:I

    iget v1, p0, Lorg/tukaani/xz/lz/LZDecoder;->c:I

    if-ge v0, v1, :cond_0

    iget v0, p0, Lorg/tukaani/xz/lz/LZDecoder;->c:I

    iput v0, p0, Lorg/tukaani/xz/lz/LZDecoder;->d:I

    :cond_0
    return-void
.end method

.method public final b(I)I
    .locals 2

    iget v0, p0, Lorg/tukaani/xz/lz/LZDecoder;->c:I

    sub-int/2addr v0, p1

    add-int/lit8 v0, v0, -0x1

    iget v1, p0, Lorg/tukaani/xz/lz/LZDecoder;->c:I

    if-lt p1, v1, :cond_0

    iget-object v1, p0, Lorg/tukaani/xz/lz/LZDecoder;->a:[B

    array-length v1, v1

    add-int/2addr v0, v1

    :cond_0
    iget-object v1, p0, Lorg/tukaani/xz/lz/LZDecoder;->a:[B

    aget-byte v0, v1, v0

    and-int/lit16 v0, v0, 0xff

    return v0
.end method

.method public final b()Z
    .locals 2

    iget v0, p0, Lorg/tukaani/xz/lz/LZDecoder;->c:I

    iget v1, p0, Lorg/tukaani/xz/lz/LZDecoder;->e:I

    if-ge v0, v1, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public final c()Z
    .locals 1

    iget v0, p0, Lorg/tukaani/xz/lz/LZDecoder;->f:I

    if-lez v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public final d()I
    .locals 1

    iget v0, p0, Lorg/tukaani/xz/lz/LZDecoder;->c:I

    return v0
.end method

.method public final e()V
    .locals 2

    iget v0, p0, Lorg/tukaani/xz/lz/LZDecoder;->f:I

    if-lez v0, :cond_0

    iget v0, p0, Lorg/tukaani/xz/lz/LZDecoder;->g:I

    iget v1, p0, Lorg/tukaani/xz/lz/LZDecoder;->f:I

    invoke-virtual {p0, v0, v1}, Lorg/tukaani/xz/lz/LZDecoder;->a(II)V

    :cond_0
    return-void
.end method
