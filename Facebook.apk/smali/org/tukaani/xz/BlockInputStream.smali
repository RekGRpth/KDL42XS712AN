.class Lorg/tukaani/xz/BlockInputStream;
.super Ljava/io/InputStream;


# instance fields
.field private final a:Ljava/io/InputStream;

.field private final b:Ljava/io/DataInputStream;

.field private final c:Lorg/tukaani/xz/CountingInputStream;

.field private d:Ljava/io/InputStream;

.field private final e:Lorg/tukaani/xz/check/Check;

.field private f:J

.field private g:J

.field private h:J

.field private final i:I

.field private j:J

.field private k:Z


# direct methods
.method public constructor <init>(Ljava/io/InputStream;Lorg/tukaani/xz/check/Check;I)V
    .locals 10

    invoke-direct {p0}, Ljava/io/InputStream;-><init>()V

    const-wide/16 v0, -0x1

    iput-wide v0, p0, Lorg/tukaani/xz/BlockInputStream;->f:J

    const-wide/16 v0, -0x1

    iput-wide v0, p0, Lorg/tukaani/xz/BlockInputStream;->g:J

    const-wide/16 v0, 0x0

    iput-wide v0, p0, Lorg/tukaani/xz/BlockInputStream;->j:J

    const/4 v0, 0x0

    iput-boolean v0, p0, Lorg/tukaani/xz/BlockInputStream;->k:Z

    iput-object p1, p0, Lorg/tukaani/xz/BlockInputStream;->a:Ljava/io/InputStream;

    iput-object p2, p0, Lorg/tukaani/xz/BlockInputStream;->e:Lorg/tukaani/xz/check/Check;

    new-instance v0, Ljava/io/DataInputStream;

    invoke-direct {v0, p1}, Ljava/io/DataInputStream;-><init>(Ljava/io/InputStream;)V

    iput-object v0, p0, Lorg/tukaani/xz/BlockInputStream;->b:Ljava/io/DataInputStream;

    const/16 v0, 0x400

    new-array v0, v0, [B

    iget-object v1, p0, Lorg/tukaani/xz/BlockInputStream;->b:Ljava/io/DataInputStream;

    const/4 v2, 0x0

    const/4 v3, 0x1

    invoke-virtual {v1, v0, v2, v3}, Ljava/io/DataInputStream;->readFully([BII)V

    const/4 v1, 0x0

    aget-byte v1, v0, v1

    if-nez v1, :cond_0

    new-instance v0, Lorg/tukaani/xz/IndexIndicatorException;

    invoke-direct {v0}, Lorg/tukaani/xz/IndexIndicatorException;-><init>()V

    throw v0

    :cond_0
    const/4 v1, 0x0

    aget-byte v1, v0, v1

    and-int/lit16 v1, v1, 0xff

    add-int/lit8 v1, v1, 0x1

    mul-int/lit8 v1, v1, 0x4

    iput v1, p0, Lorg/tukaani/xz/BlockInputStream;->i:I

    iget-object v1, p0, Lorg/tukaani/xz/BlockInputStream;->b:Ljava/io/DataInputStream;

    const/4 v2, 0x1

    iget v3, p0, Lorg/tukaani/xz/BlockInputStream;->i:I

    add-int/lit8 v3, v3, -0x1

    invoke-virtual {v1, v0, v2, v3}, Ljava/io/DataInputStream;->readFully([BII)V

    const/4 v1, 0x0

    iget v2, p0, Lorg/tukaani/xz/BlockInputStream;->i:I

    add-int/lit8 v2, v2, -0x4

    iget v3, p0, Lorg/tukaani/xz/BlockInputStream;->i:I

    add-int/lit8 v3, v3, -0x4

    invoke-static {v0, v1, v2, v3}, Lorg/tukaani/xz/common/DecoderUtil;->a([BIII)Z

    move-result v1

    if-nez v1, :cond_1

    new-instance v0, Lorg/tukaani/xz/CorruptedInputException;

    const-string v1, "XZ Block Header is corrupt"

    invoke-direct {v0, v1}, Lorg/tukaani/xz/CorruptedInputException;-><init>(Ljava/lang/String;)V

    throw v0

    :cond_1
    const/4 v1, 0x1

    aget-byte v1, v0, v1

    and-int/lit8 v1, v1, 0x3c

    if-eqz v1, :cond_2

    new-instance v0, Lorg/tukaani/xz/UnsupportedOptionsException;

    const-string v1, "Unsupported options in XZ Block Header"

    invoke-direct {v0, v1}, Lorg/tukaani/xz/UnsupportedOptionsException;-><init>(Ljava/lang/String;)V

    throw v0

    :cond_2
    const/4 v1, 0x1

    aget-byte v1, v0, v1

    and-int/lit8 v1, v1, 0x3

    add-int/lit8 v2, v1, 0x1

    new-array v1, v2, [J

    new-array v3, v2, [[B

    new-instance v4, Ljava/io/ByteArrayInputStream;

    const/4 v5, 0x2

    iget v6, p0, Lorg/tukaani/xz/BlockInputStream;->i:I

    add-int/lit8 v6, v6, -0x6

    invoke-direct {v4, v0, v5, v6}, Ljava/io/ByteArrayInputStream;-><init>([BII)V

    const-wide v5, 0x7ffffffffffffffcL

    :try_start_0
    iget v7, p0, Lorg/tukaani/xz/BlockInputStream;->i:I

    int-to-long v7, v7

    sub-long/2addr v5, v7

    invoke-virtual {p2}, Lorg/tukaani/xz/check/Check;->b()I

    move-result v7

    int-to-long v7, v7

    sub-long/2addr v5, v7

    iput-wide v5, p0, Lorg/tukaani/xz/BlockInputStream;->h:J

    const/4 v5, 0x1

    aget-byte v5, v0, v5

    and-int/lit8 v5, v5, 0x40

    if-eqz v5, :cond_5

    invoke-static {v4}, Lorg/tukaani/xz/common/DecoderUtil;->a(Ljava/io/InputStream;)J

    move-result-wide v5

    iput-wide v5, p0, Lorg/tukaani/xz/BlockInputStream;->g:J

    iget-wide v5, p0, Lorg/tukaani/xz/BlockInputStream;->g:J

    const-wide/16 v7, 0x0

    cmp-long v5, v5, v7

    if-eqz v5, :cond_3

    iget-wide v5, p0, Lorg/tukaani/xz/BlockInputStream;->g:J

    iget-wide v7, p0, Lorg/tukaani/xz/BlockInputStream;->h:J

    cmp-long v5, v5, v7

    if-lez v5, :cond_4

    :cond_3
    new-instance v0, Lorg/tukaani/xz/CorruptedInputException;

    invoke-direct {v0}, Lorg/tukaani/xz/CorruptedInputException;-><init>()V

    throw v0
    :try_end_0
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_0

    :catch_0
    move-exception v0

    new-instance v0, Lorg/tukaani/xz/CorruptedInputException;

    const-string v1, "XZ Block Header is corrupt"

    invoke-direct {v0, v1}, Lorg/tukaani/xz/CorruptedInputException;-><init>(Ljava/lang/String;)V

    throw v0

    :cond_4
    :try_start_1
    iget-wide v5, p0, Lorg/tukaani/xz/BlockInputStream;->g:J

    iput-wide v5, p0, Lorg/tukaani/xz/BlockInputStream;->h:J

    :cond_5
    const/4 v5, 0x1

    aget-byte v0, v0, v5

    and-int/lit16 v0, v0, 0x80

    if-eqz v0, :cond_6

    invoke-static {v4}, Lorg/tukaani/xz/common/DecoderUtil;->a(Ljava/io/InputStream;)J

    move-result-wide v5

    iput-wide v5, p0, Lorg/tukaani/xz/BlockInputStream;->f:J

    :cond_6
    const/4 v0, 0x0

    :goto_0
    if-ge v0, v2, :cond_8

    invoke-static {v4}, Lorg/tukaani/xz/common/DecoderUtil;->a(Ljava/io/InputStream;)J

    move-result-wide v5

    aput-wide v5, v1, v0

    invoke-static {v4}, Lorg/tukaani/xz/common/DecoderUtil;->a(Ljava/io/InputStream;)J

    move-result-wide v5

    invoke-virtual {v4}, Ljava/io/ByteArrayInputStream;->available()I

    move-result v7

    int-to-long v7, v7

    cmp-long v7, v5, v7

    if-lez v7, :cond_7

    new-instance v0, Lorg/tukaani/xz/CorruptedInputException;

    invoke-direct {v0}, Lorg/tukaani/xz/CorruptedInputException;-><init>()V

    throw v0

    :cond_7
    long-to-int v5, v5

    new-array v5, v5, [B

    aput-object v5, v3, v0

    aget-object v5, v3, v0

    invoke-virtual {v4, v5}, Ljava/io/ByteArrayInputStream;->read([B)I
    :try_end_1
    .catch Ljava/io/IOException; {:try_start_1 .. :try_end_1} :catch_0

    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    :cond_8
    invoke-virtual {v4}, Ljava/io/ByteArrayInputStream;->available()I

    move-result v0

    :goto_1
    if-lez v0, :cond_a

    invoke-virtual {v4}, Ljava/io/ByteArrayInputStream;->read()I

    move-result v5

    if-eqz v5, :cond_9

    new-instance v0, Lorg/tukaani/xz/UnsupportedOptionsException;

    const-string v1, "Unsupported options in XZ Block Header"

    invoke-direct {v0, v1}, Lorg/tukaani/xz/UnsupportedOptionsException;-><init>(Ljava/lang/String;)V

    throw v0

    :cond_9
    add-int/lit8 v0, v0, -0x1

    goto :goto_1

    :cond_a
    const-wide/16 v4, -0x1

    const-wide/16 v6, -0x1

    cmp-long v0, v4, v6

    if-eqz v0, :cond_f

    iget v0, p0, Lorg/tukaani/xz/BlockInputStream;->i:I

    invoke-virtual {p2}, Lorg/tukaani/xz/check/Check;->b()I

    move-result v4

    add-int/2addr v0, v4

    int-to-long v4, v0

    const-wide/16 v6, -0x1

    cmp-long v4, v4, v6

    if-ltz v4, :cond_b

    new-instance v0, Lorg/tukaani/xz/CorruptedInputException;

    const-string v1, "XZ Index does not match a Block Header"

    invoke-direct {v0, v1}, Lorg/tukaani/xz/CorruptedInputException;-><init>(Ljava/lang/String;)V

    throw v0

    :cond_b
    const-wide/16 v4, -0x1

    int-to-long v6, v0

    sub-long/2addr v4, v6

    iget-wide v6, p0, Lorg/tukaani/xz/BlockInputStream;->h:J

    cmp-long v0, v4, v6

    if-gtz v0, :cond_c

    iget-wide v6, p0, Lorg/tukaani/xz/BlockInputStream;->g:J

    const-wide/16 v8, -0x1

    cmp-long v0, v6, v8

    if-eqz v0, :cond_d

    iget-wide v6, p0, Lorg/tukaani/xz/BlockInputStream;->g:J

    cmp-long v0, v6, v4

    if-eqz v0, :cond_d

    :cond_c
    new-instance v0, Lorg/tukaani/xz/CorruptedInputException;

    const-string v1, "XZ Index does not match a Block Header"

    invoke-direct {v0, v1}, Lorg/tukaani/xz/CorruptedInputException;-><init>(Ljava/lang/String;)V

    throw v0

    :cond_d
    iget-wide v6, p0, Lorg/tukaani/xz/BlockInputStream;->f:J

    const-wide/16 v8, -0x1

    cmp-long v0, v6, v8

    if-eqz v0, :cond_e

    iget-wide v6, p0, Lorg/tukaani/xz/BlockInputStream;->f:J

    const-wide/16 v8, -0x1

    cmp-long v0, v6, v8

    if-eqz v0, :cond_e

    new-instance v0, Lorg/tukaani/xz/CorruptedInputException;

    const-string v1, "XZ Index does not match a Block Header"

    invoke-direct {v0, v1}, Lorg/tukaani/xz/CorruptedInputException;-><init>(Ljava/lang/String;)V

    throw v0

    :cond_e
    iput-wide v4, p0, Lorg/tukaani/xz/BlockInputStream;->h:J

    iput-wide v4, p0, Lorg/tukaani/xz/BlockInputStream;->g:J

    const-wide/16 v4, -0x1

    iput-wide v4, p0, Lorg/tukaani/xz/BlockInputStream;->f:J

    :cond_f
    new-array v4, v2, [Lorg/tukaani/xz/FilterDecoder;

    const/4 v0, 0x0

    :goto_2
    if-ge v0, v2, :cond_13

    aget-wide v5, v1, v0

    const-wide/16 v7, 0x21

    cmp-long v5, v5, v7

    if-nez v5, :cond_10

    new-instance v5, Lorg/tukaani/xz/LZMA2Decoder;

    aget-object v6, v3, v0

    invoke-direct {v5, v6}, Lorg/tukaani/xz/LZMA2Decoder;-><init>([B)V

    aput-object v5, v4, v0

    :goto_3
    add-int/lit8 v0, v0, 0x1

    goto :goto_2

    :cond_10
    aget-wide v5, v1, v0

    const-wide/16 v7, 0x3

    cmp-long v5, v5, v7

    if-nez v5, :cond_11

    new-instance v5, Lorg/tukaani/xz/DeltaDecoder;

    aget-object v6, v3, v0

    invoke-direct {v5, v6}, Lorg/tukaani/xz/DeltaDecoder;-><init>([B)V

    aput-object v5, v4, v0

    goto :goto_3

    :cond_11
    aget-wide v5, v1, v0

    invoke-static {v5, v6}, Lorg/tukaani/xz/BCJDecoder;->a(J)Z

    move-result v5

    if-eqz v5, :cond_12

    new-instance v5, Lorg/tukaani/xz/BCJDecoder;

    aget-wide v6, v1, v0

    aget-object v8, v3, v0

    invoke-direct {v5, v6, v7, v8}, Lorg/tukaani/xz/BCJDecoder;-><init>(J[B)V

    aput-object v5, v4, v0

    goto :goto_3

    :cond_12
    new-instance v2, Lorg/tukaani/xz/UnsupportedOptionsException;

    new-instance v3, Ljava/lang/StringBuffer;

    const-string v4, "Unknown Filter ID "

    invoke-direct {v3, v4}, Ljava/lang/StringBuffer;-><init>(Ljava/lang/String;)V

    aget-wide v0, v1, v0

    invoke-virtual {v3, v0, v1}, Ljava/lang/StringBuffer;->append(J)Ljava/lang/StringBuffer;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuffer;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-direct {v2, v0}, Lorg/tukaani/xz/UnsupportedOptionsException;-><init>(Ljava/lang/String;)V

    throw v2

    :cond_13
    invoke-static {v4}, Lorg/tukaani/xz/RawCoder;->a([Lorg/tukaani/xz/FilterCoder;)V

    if-ltz p3, :cond_15

    const/4 v1, 0x0

    const/4 v0, 0x0

    :goto_4
    if-ge v0, v2, :cond_14

    aget-object v3, v4, v0

    invoke-interface {v3}, Lorg/tukaani/xz/FilterDecoder;->d()I

    move-result v3

    add-int/2addr v1, v3

    add-int/lit8 v0, v0, 0x1

    goto :goto_4

    :cond_14
    if-le v1, p3, :cond_15

    new-instance v0, Lorg/tukaani/xz/MemoryLimitException;

    invoke-direct {v0, v1, p3}, Lorg/tukaani/xz/MemoryLimitException;-><init>(II)V

    throw v0

    :cond_15
    new-instance v0, Lorg/tukaani/xz/CountingInputStream;

    invoke-direct {v0, p1}, Lorg/tukaani/xz/CountingInputStream;-><init>(Ljava/io/InputStream;)V

    iput-object v0, p0, Lorg/tukaani/xz/BlockInputStream;->c:Lorg/tukaani/xz/CountingInputStream;

    iget-object v0, p0, Lorg/tukaani/xz/BlockInputStream;->c:Lorg/tukaani/xz/CountingInputStream;

    iput-object v0, p0, Lorg/tukaani/xz/BlockInputStream;->d:Ljava/io/InputStream;

    add-int/lit8 v0, v2, -0x1

    :goto_5
    if-ltz v0, :cond_16

    aget-object v1, v4, v0

    iget-object v2, p0, Lorg/tukaani/xz/BlockInputStream;->d:Ljava/io/InputStream;

    invoke-interface {v1, v2}, Lorg/tukaani/xz/FilterDecoder;->a(Ljava/io/InputStream;)Ljava/io/InputStream;

    move-result-object v1

    iput-object v1, p0, Lorg/tukaani/xz/BlockInputStream;->d:Ljava/io/InputStream;

    add-int/lit8 v0, v0, -0x1

    goto :goto_5

    :cond_16
    return-void
.end method

.method private c()V
    .locals 6

    const-wide/16 v4, -0x1

    iget-object v0, p0, Lorg/tukaani/xz/BlockInputStream;->c:Lorg/tukaani/xz/CountingInputStream;

    invoke-virtual {v0}, Lorg/tukaani/xz/CountingInputStream;->a()J

    move-result-wide v0

    iget-wide v2, p0, Lorg/tukaani/xz/BlockInputStream;->g:J

    cmp-long v2, v2, v4

    if-eqz v2, :cond_0

    iget-wide v2, p0, Lorg/tukaani/xz/BlockInputStream;->g:J

    cmp-long v2, v2, v0

    if-nez v2, :cond_1

    :cond_0
    iget-wide v2, p0, Lorg/tukaani/xz/BlockInputStream;->f:J

    cmp-long v2, v2, v4

    if-eqz v2, :cond_3

    iget-wide v2, p0, Lorg/tukaani/xz/BlockInputStream;->f:J

    iget-wide v4, p0, Lorg/tukaani/xz/BlockInputStream;->j:J

    cmp-long v2, v2, v4

    if-eqz v2, :cond_3

    :cond_1
    new-instance v0, Lorg/tukaani/xz/CorruptedInputException;

    invoke-direct {v0}, Lorg/tukaani/xz/CorruptedInputException;-><init>()V

    throw v0

    :cond_2
    move-wide v0, v2

    :cond_3
    const-wide/16 v2, 0x1

    add-long/2addr v2, v0

    const-wide/16 v4, 0x3

    and-long/2addr v0, v4

    const-wide/16 v4, 0x0

    cmp-long v0, v0, v4

    if-eqz v0, :cond_4

    iget-object v0, p0, Lorg/tukaani/xz/BlockInputStream;->b:Ljava/io/DataInputStream;

    invoke-virtual {v0}, Ljava/io/DataInputStream;->readUnsignedByte()I

    move-result v0

    if-eqz v0, :cond_2

    new-instance v0, Lorg/tukaani/xz/CorruptedInputException;

    invoke-direct {v0}, Lorg/tukaani/xz/CorruptedInputException;-><init>()V

    throw v0

    :cond_4
    iget-object v0, p0, Lorg/tukaani/xz/BlockInputStream;->e:Lorg/tukaani/xz/check/Check;

    invoke-virtual {v0}, Lorg/tukaani/xz/check/Check;->b()I

    move-result v0

    new-array v0, v0, [B

    iget-object v1, p0, Lorg/tukaani/xz/BlockInputStream;->b:Ljava/io/DataInputStream;

    invoke-virtual {v1, v0}, Ljava/io/DataInputStream;->readFully([B)V

    iget-object v1, p0, Lorg/tukaani/xz/BlockInputStream;->e:Lorg/tukaani/xz/check/Check;

    invoke-virtual {v1}, Lorg/tukaani/xz/check/Check;->a()[B

    move-result-object v1

    invoke-static {v1, v0}, Ljava/util/Arrays;->equals([B[B)Z

    move-result v0

    if-nez v0, :cond_5

    new-instance v0, Lorg/tukaani/xz/CorruptedInputException;

    new-instance v1, Ljava/lang/StringBuffer;

    const-string v2, "Integrity check ("

    invoke-direct {v1, v2}, Ljava/lang/StringBuffer;-><init>(Ljava/lang/String;)V

    iget-object v2, p0, Lorg/tukaani/xz/BlockInputStream;->e:Lorg/tukaani/xz/check/Check;

    invoke-virtual {v2}, Lorg/tukaani/xz/check/Check;->c()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    move-result-object v1

    const-string v2, ") does not match"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuffer;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Lorg/tukaani/xz/CorruptedInputException;-><init>(Ljava/lang/String;)V

    throw v0

    :cond_5
    return-void
.end method


# virtual methods
.method public final a()J
    .locals 4

    iget v0, p0, Lorg/tukaani/xz/BlockInputStream;->i:I

    int-to-long v0, v0

    iget-object v2, p0, Lorg/tukaani/xz/BlockInputStream;->c:Lorg/tukaani/xz/CountingInputStream;

    invoke-virtual {v2}, Lorg/tukaani/xz/CountingInputStream;->a()J

    move-result-wide v2

    add-long/2addr v0, v2

    iget-object v2, p0, Lorg/tukaani/xz/BlockInputStream;->e:Lorg/tukaani/xz/check/Check;

    invoke-virtual {v2}, Lorg/tukaani/xz/check/Check;->b()I

    move-result v2

    int-to-long v2, v2

    add-long/2addr v0, v2

    return-wide v0
.end method

.method public available()I
    .locals 1

    iget-object v0, p0, Lorg/tukaani/xz/BlockInputStream;->d:Ljava/io/InputStream;

    invoke-virtual {v0}, Ljava/io/InputStream;->available()I

    move-result v0

    return v0
.end method

.method public final b()J
    .locals 2

    iget-wide v0, p0, Lorg/tukaani/xz/BlockInputStream;->j:J

    return-wide v0
.end method

.method public read()I
    .locals 4

    const/4 v2, 0x1

    const/4 v3, 0x0

    const/4 v0, -0x1

    new-array v1, v2, [B

    invoke-virtual {p0, v1, v3, v2}, Lorg/tukaani/xz/BlockInputStream;->read([BII)I

    move-result v2

    if-ne v2, v0, :cond_0

    :goto_0
    return v0

    :cond_0
    aget-byte v0, v1, v3

    and-int/lit16 v0, v0, 0xff

    goto :goto_0
.end method

.method public read([BII)I
    .locals 9

    const-wide/16 v7, 0x0

    const/4 v6, 0x1

    const/4 v0, -0x1

    iget-boolean v1, p0, Lorg/tukaani/xz/BlockInputStream;->k:Z

    if-eqz v1, :cond_0

    :goto_0
    return v0

    :cond_0
    iget-object v1, p0, Lorg/tukaani/xz/BlockInputStream;->d:Ljava/io/InputStream;

    invoke-virtual {v1, p1, p2, p3}, Ljava/io/InputStream;->read([BII)I

    move-result v1

    if-lez v1, :cond_6

    iget-object v2, p0, Lorg/tukaani/xz/BlockInputStream;->e:Lorg/tukaani/xz/check/Check;

    invoke-virtual {v2, p1, p2, v1}, Lorg/tukaani/xz/check/Check;->a([BII)V

    iget-wide v2, p0, Lorg/tukaani/xz/BlockInputStream;->j:J

    int-to-long v4, v1

    add-long/2addr v2, v4

    iput-wide v2, p0, Lorg/tukaani/xz/BlockInputStream;->j:J

    iget-object v2, p0, Lorg/tukaani/xz/BlockInputStream;->c:Lorg/tukaani/xz/CountingInputStream;

    invoke-virtual {v2}, Lorg/tukaani/xz/CountingInputStream;->a()J

    move-result-wide v2

    cmp-long v4, v2, v7

    if-ltz v4, :cond_1

    iget-wide v4, p0, Lorg/tukaani/xz/BlockInputStream;->h:J

    cmp-long v2, v2, v4

    if-gtz v2, :cond_1

    iget-wide v2, p0, Lorg/tukaani/xz/BlockInputStream;->j:J

    cmp-long v2, v2, v7

    if-ltz v2, :cond_1

    iget-wide v2, p0, Lorg/tukaani/xz/BlockInputStream;->f:J

    const-wide/16 v4, -0x1

    cmp-long v2, v2, v4

    if-eqz v2, :cond_2

    iget-wide v2, p0, Lorg/tukaani/xz/BlockInputStream;->j:J

    iget-wide v4, p0, Lorg/tukaani/xz/BlockInputStream;->f:J

    cmp-long v2, v2, v4

    if-lez v2, :cond_2

    :cond_1
    new-instance v0, Lorg/tukaani/xz/CorruptedInputException;

    invoke-direct {v0}, Lorg/tukaani/xz/CorruptedInputException;-><init>()V

    throw v0

    :cond_2
    if-lt v1, p3, :cond_3

    iget-wide v2, p0, Lorg/tukaani/xz/BlockInputStream;->j:J

    iget-wide v4, p0, Lorg/tukaani/xz/BlockInputStream;->f:J

    cmp-long v2, v2, v4

    if-nez v2, :cond_5

    :cond_3
    iget-object v2, p0, Lorg/tukaani/xz/BlockInputStream;->d:Ljava/io/InputStream;

    invoke-virtual {v2}, Ljava/io/InputStream;->read()I

    move-result v2

    if-eq v2, v0, :cond_4

    new-instance v0, Lorg/tukaani/xz/CorruptedInputException;

    invoke-direct {v0}, Lorg/tukaani/xz/CorruptedInputException;-><init>()V

    throw v0

    :cond_4
    invoke-direct {p0}, Lorg/tukaani/xz/BlockInputStream;->c()V

    iput-boolean v6, p0, Lorg/tukaani/xz/BlockInputStream;->k:Z

    :cond_5
    :goto_1
    move v0, v1

    goto :goto_0

    :cond_6
    if-ne v1, v0, :cond_5

    invoke-direct {p0}, Lorg/tukaani/xz/BlockInputStream;->c()V

    iput-boolean v6, p0, Lorg/tukaani/xz/BlockInputStream;->k:Z

    goto :goto_1
.end method
