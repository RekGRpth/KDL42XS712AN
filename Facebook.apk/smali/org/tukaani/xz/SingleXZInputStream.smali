.class public Lorg/tukaani/xz/SingleXZInputStream;
.super Ljava/io/InputStream;


# instance fields
.field private a:Ljava/io/InputStream;

.field private b:I

.field private c:Lorg/tukaani/xz/common/StreamFlags;

.field private d:Lorg/tukaani/xz/check/Check;

.field private e:Lorg/tukaani/xz/BlockInputStream;

.field private final f:Lorg/tukaani/xz/index/IndexHash;

.field private g:Z

.field private h:Ljava/io/IOException;


# direct methods
.method public constructor <init>(Ljava/io/InputStream;)V
    .locals 2

    const/4 v1, 0x0

    invoke-direct {p0}, Ljava/io/InputStream;-><init>()V

    iput-object v1, p0, Lorg/tukaani/xz/SingleXZInputStream;->e:Lorg/tukaani/xz/BlockInputStream;

    new-instance v0, Lorg/tukaani/xz/index/IndexHash;

    invoke-direct {v0}, Lorg/tukaani/xz/index/IndexHash;-><init>()V

    iput-object v0, p0, Lorg/tukaani/xz/SingleXZInputStream;->f:Lorg/tukaani/xz/index/IndexHash;

    const/4 v0, 0x0

    iput-boolean v0, p0, Lorg/tukaani/xz/SingleXZInputStream;->g:Z

    iput-object v1, p0, Lorg/tukaani/xz/SingleXZInputStream;->h:Ljava/io/IOException;

    invoke-direct {p0, p1}, Lorg/tukaani/xz/SingleXZInputStream;->a(Ljava/io/InputStream;)V

    return-void
.end method

.method private a()V
    .locals 5

    const/16 v0, 0xc

    new-array v0, v0, [B

    new-instance v1, Ljava/io/DataInputStream;

    iget-object v2, p0, Lorg/tukaani/xz/SingleXZInputStream;->a:Ljava/io/InputStream;

    invoke-direct {v1, v2}, Ljava/io/DataInputStream;-><init>(Ljava/io/InputStream;)V

    invoke-virtual {v1, v0}, Ljava/io/DataInputStream;->readFully([B)V

    invoke-static {v0}, Lorg/tukaani/xz/common/DecoderUtil;->b([B)Lorg/tukaani/xz/common/StreamFlags;

    move-result-object v0

    iget-object v1, p0, Lorg/tukaani/xz/SingleXZInputStream;->c:Lorg/tukaani/xz/common/StreamFlags;

    invoke-static {v1, v0}, Lorg/tukaani/xz/common/DecoderUtil;->a(Lorg/tukaani/xz/common/StreamFlags;Lorg/tukaani/xz/common/StreamFlags;)Z

    move-result v1

    if-eqz v1, :cond_0

    iget-object v1, p0, Lorg/tukaani/xz/SingleXZInputStream;->f:Lorg/tukaani/xz/index/IndexHash;

    invoke-virtual {v1}, Lorg/tukaani/xz/index/IndexHash;->a()J

    move-result-wide v1

    iget-wide v3, v0, Lorg/tukaani/xz/common/StreamFlags;->b:J

    cmp-long v0, v1, v3

    if-eqz v0, :cond_1

    :cond_0
    new-instance v0, Lorg/tukaani/xz/CorruptedInputException;

    const-string v1, "XZ Stream Footer does not match Stream Header"

    invoke-direct {v0, v1}, Lorg/tukaani/xz/CorruptedInputException;-><init>(Ljava/lang/String;)V

    throw v0

    :cond_1
    return-void
.end method

.method private a(Ljava/io/InputStream;)V
    .locals 2

    const/16 v0, 0xc

    new-array v0, v0, [B

    new-instance v1, Ljava/io/DataInputStream;

    invoke-direct {v1, p1}, Ljava/io/DataInputStream;-><init>(Ljava/io/InputStream;)V

    invoke-virtual {v1, v0}, Ljava/io/DataInputStream;->readFully([B)V

    const/4 v1, -0x1

    invoke-direct {p0, p1, v1, v0}, Lorg/tukaani/xz/SingleXZInputStream;->a(Ljava/io/InputStream;I[B)V

    return-void
.end method

.method private a(Ljava/io/InputStream;I[B)V
    .locals 1

    iput-object p1, p0, Lorg/tukaani/xz/SingleXZInputStream;->a:Ljava/io/InputStream;

    const/4 v0, -0x1

    iput v0, p0, Lorg/tukaani/xz/SingleXZInputStream;->b:I

    invoke-static {p3}, Lorg/tukaani/xz/common/DecoderUtil;->a([B)Lorg/tukaani/xz/common/StreamFlags;

    move-result-object v0

    iput-object v0, p0, Lorg/tukaani/xz/SingleXZInputStream;->c:Lorg/tukaani/xz/common/StreamFlags;

    iget-object v0, p0, Lorg/tukaani/xz/SingleXZInputStream;->c:Lorg/tukaani/xz/common/StreamFlags;

    iget v0, v0, Lorg/tukaani/xz/common/StreamFlags;->a:I

    invoke-static {v0}, Lorg/tukaani/xz/check/Check;->a(I)Lorg/tukaani/xz/check/Check;

    move-result-object v0

    iput-object v0, p0, Lorg/tukaani/xz/SingleXZInputStream;->d:Lorg/tukaani/xz/check/Check;

    return-void
.end method


# virtual methods
.method public available()I
    .locals 2

    iget-object v0, p0, Lorg/tukaani/xz/SingleXZInputStream;->a:Ljava/io/InputStream;

    if-nez v0, :cond_0

    new-instance v0, Lorg/tukaani/xz/XZIOException;

    const-string v1, "Stream closed"

    invoke-direct {v0, v1}, Lorg/tukaani/xz/XZIOException;-><init>(Ljava/lang/String;)V

    throw v0

    :cond_0
    iget-object v0, p0, Lorg/tukaani/xz/SingleXZInputStream;->h:Ljava/io/IOException;

    if-eqz v0, :cond_1

    iget-object v0, p0, Lorg/tukaani/xz/SingleXZInputStream;->h:Ljava/io/IOException;

    throw v0

    :cond_1
    iget-object v0, p0, Lorg/tukaani/xz/SingleXZInputStream;->e:Lorg/tukaani/xz/BlockInputStream;

    if-nez v0, :cond_2

    const/4 v0, 0x0

    :goto_0
    return v0

    :cond_2
    iget-object v0, p0, Lorg/tukaani/xz/SingleXZInputStream;->e:Lorg/tukaani/xz/BlockInputStream;

    invoke-virtual {v0}, Lorg/tukaani/xz/BlockInputStream;->available()I

    move-result v0

    goto :goto_0
.end method

.method public close()V
    .locals 2

    const/4 v1, 0x0

    iget-object v0, p0, Lorg/tukaani/xz/SingleXZInputStream;->a:Ljava/io/InputStream;

    if-eqz v0, :cond_0

    :try_start_0
    iget-object v0, p0, Lorg/tukaani/xz/SingleXZInputStream;->a:Ljava/io/InputStream;

    invoke-virtual {v0}, Ljava/io/InputStream;->close()V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    iput-object v1, p0, Lorg/tukaani/xz/SingleXZInputStream;->a:Ljava/io/InputStream;

    :cond_0
    return-void

    :catchall_0
    move-exception v0

    iput-object v1, p0, Lorg/tukaani/xz/SingleXZInputStream;->a:Ljava/io/InputStream;

    throw v0
.end method

.method public read()I
    .locals 4

    const/4 v2, 0x1

    const/4 v3, 0x0

    const/4 v0, -0x1

    new-array v1, v2, [B

    invoke-virtual {p0, v1, v3, v2}, Lorg/tukaani/xz/SingleXZInputStream;->read([BII)I

    move-result v2

    if-ne v2, v0, :cond_0

    :goto_0
    return v0

    :cond_0
    aget-byte v0, v1, v3

    and-int/lit16 v0, v0, 0xff

    goto :goto_0
.end method

.method public read([BII)I
    .locals 7

    const/4 v0, 0x0

    const/4 v1, -0x1

    if-ltz p2, :cond_0

    if-ltz p3, :cond_0

    add-int v2, p2, p3

    if-ltz v2, :cond_0

    add-int v2, p2, p3

    array-length v3, p1

    if-le v2, v3, :cond_1

    :cond_0
    new-instance v0, Ljava/lang/IndexOutOfBoundsException;

    invoke-direct {v0}, Ljava/lang/IndexOutOfBoundsException;-><init>()V

    throw v0

    :cond_1
    if-nez p3, :cond_3

    :cond_2
    :goto_0
    return v0

    :cond_3
    iget-object v2, p0, Lorg/tukaani/xz/SingleXZInputStream;->a:Ljava/io/InputStream;

    if-nez v2, :cond_4

    new-instance v0, Lorg/tukaani/xz/XZIOException;

    const-string v1, "Stream closed"

    invoke-direct {v0, v1}, Lorg/tukaani/xz/XZIOException;-><init>(Ljava/lang/String;)V

    throw v0

    :cond_4
    iget-object v2, p0, Lorg/tukaani/xz/SingleXZInputStream;->h:Ljava/io/IOException;

    if-eqz v2, :cond_5

    iget-object v0, p0, Lorg/tukaani/xz/SingleXZInputStream;->h:Ljava/io/IOException;

    throw v0

    :cond_5
    iget-boolean v2, p0, Lorg/tukaani/xz/SingleXZInputStream;->g:Z

    if-eqz v2, :cond_6

    move v0, v1

    goto :goto_0

    :cond_6
    :goto_1
    if-lez p3, :cond_2

    :try_start_0
    iget-object v2, p0, Lorg/tukaani/xz/SingleXZInputStream;->e:Lorg/tukaani/xz/BlockInputStream;
    :try_end_0
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_1

    if-nez v2, :cond_7

    :try_start_1
    new-instance v2, Lorg/tukaani/xz/BlockInputStream;

    iget-object v3, p0, Lorg/tukaani/xz/SingleXZInputStream;->a:Ljava/io/InputStream;

    iget-object v4, p0, Lorg/tukaani/xz/SingleXZInputStream;->d:Lorg/tukaani/xz/check/Check;

    iget v5, p0, Lorg/tukaani/xz/SingleXZInputStream;->b:I

    invoke-direct {v2, v3, v4, v5}, Lorg/tukaani/xz/BlockInputStream;-><init>(Ljava/io/InputStream;Lorg/tukaani/xz/check/Check;I)V

    iput-object v2, p0, Lorg/tukaani/xz/SingleXZInputStream;->e:Lorg/tukaani/xz/BlockInputStream;
    :try_end_1
    .catch Lorg/tukaani/xz/IndexIndicatorException; {:try_start_1 .. :try_end_1} :catch_0
    .catch Ljava/io/IOException; {:try_start_1 .. :try_end_1} :catch_1

    :cond_7
    :try_start_2
    iget-object v2, p0, Lorg/tukaani/xz/SingleXZInputStream;->e:Lorg/tukaani/xz/BlockInputStream;

    invoke-virtual {v2, p1, p2, p3}, Lorg/tukaani/xz/BlockInputStream;->read([BII)I

    move-result v2

    if-lez v2, :cond_8

    add-int/2addr v0, v2

    add-int/2addr p2, v2

    sub-int/2addr p3, v2

    goto :goto_1

    :catch_0
    move-exception v2

    iget-object v2, p0, Lorg/tukaani/xz/SingleXZInputStream;->f:Lorg/tukaani/xz/index/IndexHash;

    iget-object v3, p0, Lorg/tukaani/xz/SingleXZInputStream;->a:Ljava/io/InputStream;

    invoke-virtual {v2, v3}, Lorg/tukaani/xz/index/IndexHash;->a(Ljava/io/InputStream;)V

    invoke-direct {p0}, Lorg/tukaani/xz/SingleXZInputStream;->a()V

    const/4 v2, 0x1

    iput-boolean v2, p0, Lorg/tukaani/xz/SingleXZInputStream;->g:Z

    if-gtz v0, :cond_2

    move v0, v1

    goto :goto_0

    :cond_8
    if-ne v2, v1, :cond_6

    iget-object v2, p0, Lorg/tukaani/xz/SingleXZInputStream;->f:Lorg/tukaani/xz/index/IndexHash;

    iget-object v3, p0, Lorg/tukaani/xz/SingleXZInputStream;->e:Lorg/tukaani/xz/BlockInputStream;

    invoke-virtual {v3}, Lorg/tukaani/xz/BlockInputStream;->a()J

    move-result-wide v3

    iget-object v5, p0, Lorg/tukaani/xz/SingleXZInputStream;->e:Lorg/tukaani/xz/BlockInputStream;

    invoke-virtual {v5}, Lorg/tukaani/xz/BlockInputStream;->b()J

    move-result-wide v5

    invoke-virtual {v2, v3, v4, v5, v6}, Lorg/tukaani/xz/index/IndexHash;->a(JJ)V

    const/4 v2, 0x0

    iput-object v2, p0, Lorg/tukaani/xz/SingleXZInputStream;->e:Lorg/tukaani/xz/BlockInputStream;
    :try_end_2
    .catch Ljava/io/IOException; {:try_start_2 .. :try_end_2} :catch_1

    goto :goto_1

    :catch_1
    move-exception v1

    iput-object v1, p0, Lorg/tukaani/xz/SingleXZInputStream;->h:Ljava/io/IOException;

    if-nez v0, :cond_2

    throw v1
.end method
