.class public Lcom/facebook/R$string;
.super Ljava/lang/Object;
.source "R.java"


# static fields
.field public static final SSL_date_error:I = 0x7f080053

.field public static final aaa_tux_tooltip_audience_friends:I = 0x7f0808b6

.field public static final aaa_tux_tooltip_audience_public:I = 0x7f0808b5

.field public static final aaa_tux_tooltip_more_options:I = 0x7f0808b7

.field public static final aaa_tux_tooltip_skipped:I = 0x7f0808b8

.field public static final abbreviated_active_x_days_ago:I = 0x7f080387

.field public static final abbreviated_active_x_days_ago_uppercase:I = 0x7f080395

.field public static final abbreviated_active_x_hours_ago:I = 0x7f080386

.field public static final abbreviated_active_x_hours_ago_uppercase:I = 0x7f080394

.field public static final abbreviated_active_x_mins_ago:I = 0x7f080385

.field public static final abbreviated_active_x_mins_ago_uppercase:I = 0x7f080393

.field public static final abbreviated_count_million:I = 0x7f080710

.field public static final abbreviated_count_thousand:I = 0x7f08070f

.field public static final abc_action_bar_home_description:I = 0x7f0800c6

.field public static final abc_action_bar_home_description_format:I = 0x7f0800c9

.field public static final abc_action_bar_home_subtitle_description_format:I = 0x7f0800ca

.field public static final abc_action_bar_up_description:I = 0x7f0800c7

.field public static final abc_action_menu_overflow_description:I = 0x7f0800c8

.field public static final abc_action_mode_done:I = 0x7f0800c5

.field public static final abc_activity_chooser_view_see_all:I = 0x7f0800d1

.field public static final abc_activitychooserview_choose_application:I = 0x7f0800d0

.field public static final abc_searchview_description_clear:I = 0x7f0800cd

.field public static final abc_searchview_description_query:I = 0x7f0800cc

.field public static final abc_searchview_description_search:I = 0x7f0800cb

.field public static final abc_searchview_description_submit:I = 0x7f0800ce

.field public static final abc_searchview_description_voice:I = 0x7f0800cf

.field public static final abc_shareactionprovider_share_with:I = 0x7f0800d3

.field public static final abc_shareactionprovider_share_with_application:I = 0x7f0800d2

.field public static final about:I = 0x7f080a34

.field public static final accept_friend_request_button_description:I = 0x7f080187

.field public static final accessibility_action_menu:I = 0x7f080b51

.field public static final accessibility_albums_create_album:I = 0x7f080754

.field public static final accessibility_attachment_add_more:I = 0x7f0807f5

.field public static final accessibility_back:I = 0x7f0807a7

.field public static final accessibility_back_to_composer:I = 0x7f0807a9

.field public static final accessibility_back_to_picker:I = 0x7f0807a8

.field public static final accessibility_background_location_settings_sharing_privacy_option_not_selected:I = 0x7f080f75

.field public static final accessibility_background_location_settings_sharing_privacy_option_selected:I = 0x7f080f74

.field public static final accessibility_background_location_settings_sharing_switch:I = 0x7f080f73

.field public static final accessibility_bookmark_hide_button:I = 0x7f080e76

.field public static final accessibility_bookmark_messages_settings:I = 0x7f080e73

.field public static final accessibility_bookmark_newsfeed_settings:I = 0x7f080e72

.field public static final accessibility_bookmark_sponsored_app_install:I = 0x7f080e74

.field public static final accessibility_bookmark_sponsored_creative_image:I = 0x7f080e77

.field public static final accessibility_bookmark_x_button:I = 0x7f080e75

.field public static final accessibility_call_to_action:I = 0x7f080482

.field public static final accessibility_can_subscribe:I = 0x7f080b54

.field public static final accessibility_collection_curate:I = 0x7f0811ea

.field public static final accessibility_composer_add_photo:I = 0x7f08085e

.field public static final accessibility_composer_add_photo_to_album:I = 0x7f08085f

.field public static final accessibility_composer_minutiae:I = 0x7f080860

.field public static final accessibility_composer_minutiae_delete:I = 0x7f080861

.field public static final accessibility_composer_publish_mode:I = 0x7f080862

.field public static final accessibility_composer_tag_friends:I = 0x7f08085c

.field public static final accessibility_composer_tag_location:I = 0x7f08085d

.field public static final accessibility_contacts:I = 0x7f080f5b

.field public static final accessibility_cover_photo:I = 0x7f080b50

.field public static final accessibility_feed_app_collection_add:I = 0x7f0805ec

.field public static final accessibility_feed_app_collection_added:I = 0x7f0805ed

.field public static final accessibility_feed_app_collection_member:I = 0x7f0805ee

.field public static final accessibility_feed_liked_page:I = 0x7f08058b

.field public static final accessibility_feed_unit_menu:I = 0x7f0805ef

.field public static final accessibility_friending_are_friends:I = 0x7f08054f

.field public static final accessibility_friending_outgoing_request:I = 0x7f080550

.field public static final accessibility_friends_nearby_invite_button:I = 0x7f080aa0

.field public static final accessibility_friends_nearby_ping_button:I = 0x7f080a9f

.field public static final accessibility_friends_nearby_refresh_button:I = 0x7f080a9e

.field public static final accessibility_friends_nearby_settings_button:I = 0x7f080a9d

.field public static final accessibility_groups_can_request:I = 0x7f080b53

.field public static final accessibility_groups_member:I = 0x7f080b56

.field public static final accessibility_groups_outgoing_request:I = 0x7f080b52

.field public static final accessibility_image_camera:I = 0x7f0807a5

.field public static final accessibility_loading_more_results:I = 0x7f08005d

.field public static final accessibility_menu:I = 0x7f080f5c

.field public static final accessibility_minutiae_select_an_icon:I = 0x7f080879

.field public static final accessibility_more_items:I = 0x7f080b58

.field public static final accessibility_photo_set_add_photo:I = 0x7f080753

.field public static final accessibility_photo_title:I = 0x7f080b5a

.field public static final accessibility_places_category_remove:I = 0x7f0809a5

.field public static final accessibility_privacy_choose_audience:I = 0x7f080816

.field public static final accessibility_privacy_option_checkmark:I = 0x7f0809d7

.field public static final accessibility_privacy_selected_audience:I = 0x7f080817

.field public static final accessibility_profile_picture:I = 0x7f080b4f

.field public static final accessibility_profile_questions:I = 0x7f080b57

.field public static final accessibility_refresh_photos_button:I = 0x7f080745

.field public static final accessibility_rotate_photo_button:I = 0x7f0807aa

.field public static final accessibility_search:I = 0x7f08005e

.field public static final accessibility_show_cover_photo:I = 0x7f080b5b

.field public static final accessibility_subscribed:I = 0x7f080b55

.field public static final accessibility_tag_button:I = 0x7f0807ab

.field public static final accessibility_video_camera:I = 0x7f0807a6

.field public static final accessibility_video_end_screen:I = 0x7f080481

.field public static final accessibility_video_preview_accept:I = 0x7f080947

.field public static final accessibility_video_preview_play:I = 0x7f080948

.field public static final accessibility_video_preview_reject:I = 0x7f080946

.field public static final accessibility_who_cancel:I = 0x7f080ad0

.field public static final account_confirmation:I = 0x7f080f85

.field public static final account_settings:I = 0x7f080e69

.field public static final action_bar_cancelled:I = 0x7f080fda

.field public static final action_bar_choose_friends:I = 0x7f080fe2

.field public static final action_bar_copy_link:I = 0x7f080fe6

.field public static final action_bar_create:I = 0x7f080fe8

.field public static final action_bar_decline:I = 0x7f080fd7

.field public static final action_bar_delete:I = 0x7f080fe7

.field public static final action_bar_edit:I = 0x7f080fe4

.field public static final action_bar_going:I = 0x7f080fd4

.field public static final action_bar_hosting:I = 0x7f080fd9

.field public static final action_bar_invite:I = 0x7f080fe1

.field public static final action_bar_join:I = 0x7f080fd3

.field public static final action_bar_maybe:I = 0x7f080fd5

.field public static final action_bar_maybed:I = 0x7f080fd6

.field public static final action_bar_not_going:I = 0x7f080fd8

.field public static final action_bar_post:I = 0x7f080fdf

.field public static final action_bar_remove_me:I = 0x7f080fe3

.field public static final action_bar_report_event:I = 0x7f080fe5

.field public static final action_bar_save:I = 0x7f080fdb

.field public static final action_bar_saved:I = 0x7f080fdd

.field public static final action_bar_saving:I = 0x7f080fdc

.field public static final action_bar_share:I = 0x7f080fe0

.field public static final action_bar_unsaving:I = 0x7f080fde

.field public static final activity_log:I = 0x7f080e6b

.field public static final add_content:I = 0x7f080e86

.field public static final add_credit_card_billing_zip_hint:I = 0x7f080d2e

.field public static final add_credit_card_button_add_card:I = 0x7f080d2f

.field public static final add_credit_card_card_number_hint:I = 0x7f080d29

.field public static final add_credit_card_error_billing_zip:I = 0x7f080d36

.field public static final add_credit_card_error_cc_number:I = 0x7f080d32

.field public static final add_credit_card_error_expiration_date_invalid:I = 0x7f080d33

.field public static final add_credit_card_error_general_internal:I = 0x7f080d34

.field public static final add_credit_card_error_security_code:I = 0x7f080d35

.field public static final add_credit_card_exp_month_hint:I = 0x7f080d2b

.field public static final add_credit_card_exp_year_hint:I = 0x7f080d2c

.field public static final add_credit_card_expiration_hint:I = 0x7f080d2a

.field public static final add_credit_card_header:I = 0x7f080d31

.field public static final add_credit_card_info:I = 0x7f080d11

.field public static final add_credit_card_security_code_hint:I = 0x7f080d2d

.field public static final add_credit_card_terms:I = 0x7f080d30

.field public static final add_friend:I = 0x7f080169

.field public static final add_friend_button_description:I = 0x7f080171

.field public static final add_members_button_text:I = 0x7f08024c

.field public static final add_members_missing_people:I = 0x7f08024d

.field public static final add_members_progress:I = 0x7f08024e

.field public static final add_members_warning:I = 0x7f08024b

.field public static final add_new_address:I = 0x7f080d09

.field public static final add_payment:I = 0x7f0806e3

.field public static final add_people_action:I = 0x7f0802eb

.field public static final add_people_to_group_action:I = 0x7f0802ec

.field public static final add_phone_number:I = 0x7f080dcf

.field public static final add_photo_overlay:I = 0x7f08095e

.field public static final add_photos:I = 0x7f0808aa

.field public static final add_to_album_text:I = 0x7f0807a2

.field public static final add_to_favorites_description:I = 0x7f0801d6

.field public static final address_city_placeholder:I = 0x7f080d0e

.field public static final address_name_placeholder:I = 0x7f080d0b

.field public static final address_save_error_text:I = 0x7f080ca7

.field public static final address_state_placeholder:I = 0x7f080d0f

.field public static final address_street2_placeholder:I = 0x7f080d0d

.field public static final address_street_placeholder:I = 0x7f080d0c

.field public static final address_zipcode_placeholder:I = 0x7f080d10

.field public static final album:I = 0x7f081140

.field public static final album_art_bitmap_view_description:I = 0x7f080a84

.field public static final album_creating_dialog_text:I = 0x7f080849

.field public static final album_creator_cancellation_dialog_text:I = 0x7f080848

.field public static final album_creator_create_button_text:I = 0x7f080841

.field public static final album_creator_missing_title:I = 0x7f080846

.field public static final album_creator_save_button_text:I = 0x7f080840

.field public static final album_creator_title_text:I = 0x7f08083f

.field public static final album_creator_untitled_album:I = 0x7f080847

.field public static final albums_add_contributor_action:I = 0x7f080767

.field public static final albums_allow_contributor_action:I = 0x7f080768

.field public static final albums_contributor_intro:I = 0x7f080769

.field public static final albums_create_new:I = 0x7f080e6c

.field public static final albums_create_new_album:I = 0x7f08074e

.field public static final albums_delete_action:I = 0x7f08076e

.field public static final albums_delete_cancel:I = 0x7f080771

.field public static final albums_delete_message:I = 0x7f080770

.field public static final albums_delete_title:I = 0x7f08076f

.field public static final albums_deleting_progress:I = 0x7f080772

.field public static final albums_discard_change_action:I = 0x7f08076d

.field public static final albums_discard_change_prompt:I = 0x7f08076c

.field public static final albums_edit:I = 0x7f080e6d

.field public static final albums_edit_about:I = 0x7f08075e

.field public static final albums_edit_action:I = 0x7f08075b

.field public static final albums_edit_audience:I = 0x7f080766

.field public static final albums_edit_complete:I = 0x7f08075d

.field public static final albums_edit_contributor_action:I = 0x7f08076a

.field public static final albums_edit_contributor_lower_case:I = 0x7f080760

.field public static final albums_edit_contributor_upper_case:I = 0x7f08075f

.field public static final albums_edit_description:I = 0x7f080762

.field public static final albums_edit_location:I = 0x7f080763

.field public static final albums_edit_location_action:I = 0x7f080764

.field public static final albums_edit_name:I = 0x7f080761

.field public static final albums_edit_privacy:I = 0x7f080765

.field public static final albums_edit_title:I = 0x7f08075c

.field public static final albums_empty_view_text:I = 0x7f080e6e

.field public static final albums_rename_action:I = 0x7f080755

.field public static final albums_rename_cancel:I = 0x7f080758

.field public static final albums_rename_edit_text_hint:I = 0x7f080757

.field public static final albums_rename_empty:I = 0x7f08075a

.field public static final albums_rename_title:I = 0x7f080756

.field public static final albums_renaming_progress:I = 0x7f080759

.field public static final albums_update_progress:I = 0x7f08076b

.field public static final albums_view:I = 0x7f080659

.field public static final alcohol_address_section_header:I = 0x7f080d05

.field public static final alcohol_age_section_header:I = 0x7f080d03

.field public static final alcohol_birthdate_sentence:I = 0x7f080d04

.field public static final alcohol_form_header:I = 0x7f080d02

.field public static final alcohol_help_section_header:I = 0x7f080d06

.field public static final alcohol_help_text:I = 0x7f080d08

.field public static final alcohol_info_privacy_text:I = 0x7f080d07

.field public static final alert_dialog_title:I = 0x7f080019

.field public static final alerts_are_off:I = 0x7f080f09

.field public static final alerts_are_on:I = 0x7f080f0a

.field public static final all:I = 0x7f080781

.field public static final all_friends_tag_text:I = 0x7f0806af

.field public static final all_transactions_title:I = 0x7f0801c9

.field public static final app_error_dialog_title:I = 0x7f080015

.field public static final app_feeds_connect_cover_feed_nux_dialog_description:I = 0x7f080147

.field public static final app_feeds_connect_cover_feed_nux_dialog_title:I = 0x7f080146

.field public static final app_feeds_upsell_body:I = 0x7f080164

.field public static final app_feeds_upsell_connect_apps_label:I = 0x7f080165

.field public static final app_feeds_upsell_title:I = 0x7f080163

.field public static final app_invites_title:I = 0x7f080f61

.field public static final app_name:I = 0x7f080014

.field public static final app_name_long:I = 0x7f0801e7

.field public static final app_name_short:I = 0x7f0801e6

.field public static final app_name_text_view_description:I = 0x7f080a83

.field public static final app_settings:I = 0x7f080e6a

.field public static final app_widget_name:I = 0x7f080e6f

.field public static final appirater_cancel_button:I = 0x7f080c5e

.field public static final appirater_dialog_title:I = 0x7f080c5c

.field public static final appirater_ise_dismiss_button:I = 0x7f080c60

.field public static final appirater_ise_feedback_cancel_button:I = 0x7f080c67

.field public static final appirater_ise_feedback_message:I = 0x7f080c65

.field public static final appirater_ise_feedback_submit_button:I = 0x7f080c68

.field public static final appirater_ise_feedback_textbox_hint:I = 0x7f080c66

.field public static final appirater_ise_five_star_description:I = 0x7f080c72

.field public static final appirater_ise_four_star_description:I = 0x7f080c71

.field public static final appirater_ise_one_star_description:I = 0x7f080c6e

.field public static final appirater_ise_stars_message:I = 0x7f080c64

.field public static final appirater_ise_stars_notnow_button:I = 0x7f080c62

.field public static final appirater_ise_stars_submit_button:I = 0x7f080c61

.field public static final appirater_ise_stars_title:I = 0x7f080c63

.field public static final appirater_ise_thanks_norate_message:I = 0x7f080c69

.field public static final appirater_ise_thanks_norate_okay_button:I = 0x7f080c6a

.field public static final appirater_ise_thanks_rate_goto_play_store_button:I = 0x7f080c6d

.field public static final appirater_ise_thanks_rate_message:I = 0x7f080c6b

.field public static final appirater_ise_thanks_rate_nothanks_button:I = 0x7f080c6c

.field public static final appirater_ise_three_star_description:I = 0x7f080c70

.field public static final appirater_ise_two_star_description:I = 0x7f080c6f

.field public static final appirater_message:I = 0x7f080c5b

.field public static final appirater_rate_button:I = 0x7f080c5d

.field public static final appirater_rate_later:I = 0x7f080c5f

.field public static final artist_title_text_view_description:I = 0x7f080a86

.field public static final attach_image_content_description:I = 0x7f0802a1

.field public static final attach_image_or_video_content_description:I = 0x7f0802a2

.field public static final attached_too_many_photos:I = 0x7f0807fa

.field public static final attachment_download:I = 0x7f0803a1

.field public static final attachment_download_dialog_download:I = 0x7f0803fd

.field public static final attachment_download_dialog_unknown_attachment:I = 0x7f0803fc

.field public static final attachment_download_error:I = 0x7f0803a2

.field public static final attachment_preview_cancel:I = 0x7f0803f6

.field public static final attachment_preview_continue:I = 0x7f0803f3

.field public static final attachment_preview_edit:I = 0x7f0803f5

.field public static final attachment_preview_send_video:I = 0x7f0803f4

.field public static final attachment_upload_not_complete:I = 0x7f0803a3

.field public static final audience_alignment_call_to_action:I = 0x7f0808ba

.field public static final audience_alignment_description_text:I = 0x7f0808b9

.field public static final audience_alignment_learn_more_body:I = 0x7f0808bc

.field public static final audience_alignment_learn_more_get_help:I = 0x7f0808bd

.field public static final audience_alignment_learn_more_title:I = 0x7f0808bb

.field public static final audience_educator_greeting:I = 0x7f0808ae

.field public static final audience_educator_learn_more:I = 0x7f0808b2

.field public static final audience_educator_learn_more_close_content_descriptor:I = 0x7f0808b4

.field public static final audience_educator_more_options:I = 0x7f0808b1

.field public static final audience_educator_share_friends:I = 0x7f0808b0

.field public static final audience_educator_share_public:I = 0x7f0808af

.field public static final audience_educator_skip:I = 0x7f0808b3

.field public static final audio_cancel_record_instruction:I = 0x7f0803aa

.field public static final audio_player_duration_error:I = 0x7f0803a6

.field public static final audio_player_duration_minute:I = 0x7f0803a5

.field public static final audio_player_error_message:I = 0x7f0803a4

.field public static final audio_record_button_text:I = 0x7f0803b1

.field public static final audio_record_cancel_instructions:I = 0x7f0803b0

.field public static final audio_record_cancelled_message:I = 0x7f0803ab

.field public static final audio_record_done:I = 0x7f0803ac

.field public static final audio_record_nux:I = 0x7f0803ae

.field public static final audio_record_swipe_instructions:I = 0x7f0803af

.field public static final audio_record_tooltip_hint:I = 0x7f0803ad

.field public static final audio_recorder_maximum_length_notification:I = 0x7f0803a9

.field public static final audio_recording_attachment_error:I = 0x7f0803a8

.field public static final audio_recording_clock_zero:I = 0x7f0803b2

.field public static final audio_recording_error_recording:I = 0x7f0803a7

.field public static final audio_recording_first_time_press:I = 0x7f0803b4

.field public static final audio_recording_nux_string:I = 0x7f0803b3

.field public static final auth_session_expired_dialog_body:I = 0x7f08007e

.field public static final auth_session_expired_dialog_ok_button:I = 0x7f08007f

.field public static final auth_session_expired_dialog_title:I = 0x7f08007d

.field public static final auto_login_messenger:I = 0x7f080077

.field public static final auto_logout_messenger:I = 0x7f080078

.field public static final back_button_accessibility:I = 0x7f0807f6

.field public static final back_button_description:I = 0x7f0801d3

.field public static final background_button_description:I = 0x7f0801dd

.field public static final backgroundlocation_inline_upsell_content:I = 0x7f080f72

.field public static final backgroundlocation_inline_upsell_title:I = 0x7f080f71

.field public static final backgroundlocation_nux_intro_friend_nearby:I = 0x7f080f64

.field public static final backgroundlocation_nux_intro_message1:I = 0x7f080f65

.field public static final backgroundlocation_nux_intro_message2:I = 0x7f080f66

.field public static final backgroundlocation_nux_intro_subtitle:I = 0x7f080f63

.field public static final backgroundlocation_nux_intro_title:I = 0x7f080f62

.field public static final backgroundlocation_nux_notifications_subtitle:I = 0x7f080f6b

.field public static final backgroundlocation_nux_notifications_title:I = 0x7f080f6a

.field public static final backgroundlocation_nux_privacy_accept:I = 0x7f080f6f

.field public static final backgroundlocation_nux_privacy_card_title:I = 0x7f080f6e

.field public static final backgroundlocation_nux_privacy_decline:I = 0x7f080f70

.field public static final backgroundlocation_nux_privacy_subtitle:I = 0x7f080f6d

.field public static final backgroundlocation_nux_privacy_title:I = 0x7f080f6c

.field public static final backgroundlocation_nux_timeline_current_location:I = 0x7f080f69

.field public static final backgroundlocation_nux_timeline_subtitle:I = 0x7f080f68

.field public static final backgroundlocation_nux_timeline_title:I = 0x7f080f67

.field public static final backgroundlocation_settings_history_off:I = 0x7f080f7f

.field public static final backgroundlocation_settings_history_on:I = 0x7f080f7e

.field public static final backgroundlocation_settings_invite:I = 0x7f080f82

.field public static final backgroundlocation_settings_location_manage:I = 0x7f080f7d

.field public static final backgroundlocation_settings_location_off:I = 0x7f080f7c

.field public static final backgroundlocation_settings_location_settings:I = 0x7f080f80

.field public static final backgroundlocation_settings_report_bug:I = 0x7f080f81

.field public static final backgroundlocation_settings_sharing_audience_description:I = 0x7f080f78

.field public static final backgroundlocation_settings_sharing_audience_header:I = 0x7f080f76

.field public static final backgroundlocation_settings_sharing_audience_title:I = 0x7f080f77

.field public static final backgroundlocation_settings_sharing_off:I = 0x7f080f7b

.field public static final backgroundlocation_settings_sharing_on:I = 0x7f080f7a

.field public static final backgroundlocation_settings_title:I = 0x7f080f79

.field public static final backspace_button_description:I = 0x7f0801d5

.field public static final badge_count_more:I = 0x7f080b68

.field public static final bauble_apps_button_label:I = 0x7f0800f4

.field public static final bauble_back_button_label:I = 0x7f0800f5

.field public static final bauble_camera_button_label:I = 0x7f0800f3

.field public static final bauble_messages_button_label:I = 0x7f0800f2

.field public static final bauble_profile_image_view_description:I = 0x7f0800e6

.field public static final better_switch_off:I = 0x7f0803eb

.field public static final better_switch_on:I = 0x7f0803ea

.field public static final birthday_annotation_content_description:I = 0x7f080434

.field public static final birthday_banner_text_plural:I = 0x7f080433

.field public static final birthday_banner_text_singular:I = 0x7f080431

.field public static final birthday_banner_title_plural:I = 0x7f080432

.field public static final birthday_banner_title_singular:I = 0x7f080430

.field public static final birthday_default_description_text:I = 0x7f080877

.field public static final birthday_menu_gift_option:I = 0x7f080876

.field public static final birthday_menu_message_option:I = 0x7f080875

.field public static final birthday_menu_timeline_option:I = 0x7f080874

.field public static final birthday_nux_text_plural:I = 0x7f08042c

.field public static final birthday_nux_text_singular:I = 0x7f08042a

.field public static final birthday_nux_title_plural:I = 0x7f08042b

.field public static final birthday_nux_title_singular:I = 0x7f080429

.field public static final blue_check_desc:I = 0x7f0808f9

.field public static final bookmark_add_favorites:I = 0x7f080e82

.field public static final bookmark_cancel_edit:I = 0x7f080e80

.field public static final bookmark_edit:I = 0x7f080e7e

.field public static final bookmark_events:I = 0x7f080e7a

.field public static final bookmark_finish_edit:I = 0x7f080e7f

.field public static final bookmark_friends:I = 0x7f080e7b

.field public static final bookmark_group_favorites:I = 0x7f080e7d

.field public static final bookmark_hide:I = 0x7f080e78

.field public static final bookmark_messages:I = 0x7f080e79

.field public static final bookmark_nearby:I = 0x7f080e7c

.field public static final bookmark_newsfeed:I = 0x7f080e71

.field public static final bookmark_remove_favorites:I = 0x7f080e81

.field public static final bookmark_settings_group_title:I = 0x7f080e68

.field public static final bookmark_sync_error:I = 0x7f080e83

.field public static final bookmarks:I = 0x7f080e67

.field public static final boost_post_duration:I = 0x7f08070e

.field public static final browser_menu_nux:I = 0x7f08119c

.field public static final browser_opt_out_setting_summary_off:I = 0x7f08119b

.field public static final browser_opt_out_setting_summary_on:I = 0x7f08119a

.field public static final browser_opt_out_setting_title:I = 0x7f081199

.field public static final bug_report_acknowledgement_body:I = 0x7f080675

.field public static final bug_report_acknowledgement_close:I = 0x7f080676

.field public static final bug_report_acknowledgement_title:I = 0x7f080674

.field public static final bug_report_add_screen_shot:I = 0x7f08066c

.field public static final bug_report_button_title:I = 0x7f080677

.field public static final bug_report_category_ads:I = 0x7f080688

.field public static final bug_report_category_aura:I = 0x7f080697

.field public static final bug_report_category_bookmarks:I = 0x7f080689

.field public static final bug_report_category_chat_heads:I = 0x7f0806a6

.field public static final bug_report_category_comments:I = 0x7f08068d

.field public static final bug_report_category_composer_employee:I = 0x7f08068e

.field public static final bug_report_category_composer_non_employee:I = 0x7f08068f

.field public static final bug_report_category_events:I = 0x7f080690

.field public static final bug_report_category_gifts:I = 0x7f080693

.field public static final bug_report_category_groups:I = 0x7f080694

.field public static final bug_report_category_growth_employee:I = 0x7f0806a1

.field public static final bug_report_category_growth_non_employee:I = 0x7f0806a2

.field public static final bug_report_category_i18n:I = 0x7f080695

.field public static final bug_report_category_launcher:I = 0x7f0806a5

.field public static final bug_report_category_likes_comments:I = 0x7f0806a8

.field public static final bug_report_category_list_title:I = 0x7f080681

.field public static final bug_report_category_location_employee:I = 0x7f08068b

.field public static final bug_report_category_location_non_employee:I = 0x7f08068c

.field public static final bug_report_category_lock_screen:I = 0x7f0806a7

.field public static final bug_report_category_login:I = 0x7f080696

.field public static final bug_report_category_messenger:I = 0x7f08068a

.field public static final bug_report_category_news_feed:I = 0x7f080698

.field public static final bug_report_category_notifications:I = 0x7f080699

.field public static final bug_report_category_open_graph_employee:I = 0x7f080691

.field public static final bug_report_category_open_graph_non_employee:I = 0x7f080692

.field public static final bug_report_category_other:I = 0x7f0806a4

.field public static final bug_report_category_pages:I = 0x7f08069a

.field public static final bug_report_category_photos:I = 0x7f08069b

.field public static final bug_report_category_privacy:I = 0x7f08069d

.field public static final bug_report_category_public_content:I = 0x7f0806a3

.field public static final bug_report_category_reaction:I = 0x7f0806a9

.field public static final bug_report_category_search:I = 0x7f0806a0

.field public static final bug_report_category_timeline_employee:I = 0x7f08069e

.field public static final bug_report_category_timeline_non_employee:I = 0x7f08069f

.field public static final bug_report_category_videos:I = 0x7f08069c

.field public static final bug_report_chooser_report_abuse:I = 0x7f080686

.field public static final bug_report_chooser_report_abuse_subtitle:I = 0x7f080687

.field public static final bug_report_chooser_report_problem:I = 0x7f080684

.field public static final bug_report_chooser_report_problem_subtitle:I = 0x7f080685

.field public static final bug_report_chooser_send_feedback:I = 0x7f080682

.field public static final bug_report_chooser_send_feedback_subtitle:I = 0x7f080683

.field public static final bug_report_disclaimer:I = 0x7f080668

.field public static final bug_report_disclaimer_data_use_link:I = 0x7f080669

.field public static final bug_report_fail_text:I = 0x7f08066f

.field public static final bug_report_fail_ticker:I = 0x7f08066d

.field public static final bug_report_fail_title:I = 0x7f08066e

.field public static final bug_report_hint:I = 0x7f08066a

.field public static final bug_report_image_picker_thumbnail_copy_error:I = 0x7f0806aa

.field public static final bug_report_image_picker_thumbnail_create_error:I = 0x7f0806ab

.field public static final bug_report_intern_settings_button:I = 0x7f08067d

.field public static final bug_report_lint_ui_button:I = 0x7f08067b

.field public static final bug_report_old_version_text:I = 0x7f08067f

.field public static final bug_report_old_version_title:I = 0x7f08067e

.field public static final bug_report_please_enter_text:I = 0x7f08066b

.field public static final bug_report_please_log_in:I = 0x7f080673

.field public static final bug_report_progress:I = 0x7f080680

.field public static final bug_report_prompt:I = 0x7f080667

.field public static final bug_report_question:I = 0x7f080679

.field public static final bug_report_rageshake_setting_summary:I = 0x7f080671

.field public static final bug_report_rageshake_setting_title:I = 0x7f080670

.field public static final bug_report_save_view_hierarchy:I = 0x7f08067c

.field public static final bug_report_send:I = 0x7f080678

.field public static final bug_report_send_report_button:I = 0x7f08067a

.field public static final bug_report_thank_you_toast:I = 0x7f080672

.field public static final bug_report_title:I = 0x7f080666

.field public static final button_clear:I = 0x7f08084e

.field public static final button_draft:I = 0x7f080856

.field public static final button_schedule:I = 0x7f08084f

.field public static final button_set_date:I = 0x7f080852

.field public static final byte_size_bytes:I = 0x7f080616

.field public static final byte_size_kb:I = 0x7f080617

.field public static final byte_size_mb:I = 0x7f080618

.field public static final c2dm_registration_error_account_missing:I = 0x7f080f30

.field public static final c2dm_registration_error_account_missing_add:I = 0x7f080f31

.field public static final c2dm_registration_error_account_missing_title:I = 0x7f080f2f

.field public static final c2dm_registration_error_authentication_failed:I = 0x7f080f33

.field public static final c2dm_registration_error_authentication_failed_title:I = 0x7f080f32

.field public static final c2dm_registration_error_phone_registration_error:I = 0x7f080f35

.field public static final c2dm_registration_error_phone_registration_error_title:I = 0x7f080f34

.field public static final c2dm_registration_error_too_many_registrations:I = 0x7f080f37

.field public static final c2dm_registration_error_too_many_registrations_title:I = 0x7f080f36

.field public static final calendar_display_name:I = 0x7f0811c2

.field public static final camera_already_open_error:I = 0x7f080347

.field public static final camera_gallery_dialog_title:I = 0x7f080711

.field public static final camera_gallery_photo_option:I = 0x7f08041a

.field public static final camera_gallery_video_option:I = 0x7f080712

.field public static final camera_no_storage:I = 0x7f08033d

.field public static final camera_not_enough_space:I = 0x7f08033e

.field public static final camera_preparing_sd:I = 0x7f08033c

.field public static final camera_roll_text:I = 0x7f080795

.field public static final camera_rotation_error:I = 0x7f08033f

.field public static final cancel:I = 0x7f080a37

.field public static final cancel_button_default:I = 0x7f080ca1

.field public static final cancel_favorites_button_label:I = 0x7f080282

.field public static final cancel_friend_request_button_description:I = 0x7f080186

.field public static final cant_connect:I = 0x7f08005c

.field public static final cant_load_while_offline:I = 0x7f08114a

.field public static final cant_show_photos:I = 0x7f080778

.field public static final cant_show_videos:I = 0x7f08077b

.field public static final card_front_header:I = 0x7f080cdf

.field public static final change_email:I = 0x7f080f97

.field public static final change_phone:I = 0x7f080f98

.field public static final change_schdule_time:I = 0x7f08084c

.field public static final change_type_email:I = 0x7f080f99

.field public static final change_type_phone:I = 0x7f080f9a

.field public static final chat_head_description:I = 0x7f0803ba

.field public static final chat_head_with_name_description:I = 0x7f0803bb

.field public static final chat_heads_close_nux:I = 0x7f0801f0

.field public static final chat_heads_enable_contact_card:I = 0x7f080529

.field public static final chat_heads_first_message_nux:I = 0x7f0801ef

.field public static final chat_heads_inbox_description:I = 0x7f0803bd

.field public static final chat_heads_open_messenger_description:I = 0x7f0803bc

.field public static final chat_heads_popup_menu_description:I = 0x7f0803b8

.field public static final chat_heads_stack_description:I = 0x7f0803b9

.field public static final chat_heads_translucent_when_inactive:I = 0x7f080528

.field public static final checkin_composer_title_text:I = 0x7f08087a

.field public static final chevron_desc:I = 0x7f0808fb

.field public static final clearing_image_cache_message:I = 0x7f08052b

.field public static final clearing_image_cache_title:I = 0x7f08052a

.field public static final clearing_stickers_cache_message:I = 0x7f08052d

.field public static final clearing_stickers_cache_title:I = 0x7f08052c

.field public static final clearing_thread_cache_message:I = 0x7f08052f

.field public static final clearing_thread_cache_title:I = 0x7f08052e

.field public static final close:I = 0x7f080a3a

.field public static final close_button_description:I = 0x7f0801de

.field public static final cm_current_promo:I = 0x7f0811f3

.field public static final cm_default_title:I = 0x7f0811ed

.field public static final cm_error_creating_shortcut:I = 0x7f0811fa

.field public static final cm_error_message:I = 0x7f0811ec

.field public static final cm_error_updating:I = 0x7f0811f8

.field public static final cm_expiration:I = 0x7f0811f5

.field public static final cm_facebook_free_campaign:I = 0x7f0811f7

.field public static final cm_last_updated:I = 0x7f0811f2

.field public static final cm_loan_repayment:I = 0x7f0811f6

.field public static final cm_mb_balance:I = 0x7f0811f4

.field public static final cm_purchase_button_text:I = 0x7f0811ef

.field public static final cm_sim_balance:I = 0x7f0811f1

.field public static final cm_suggested_promo_text:I = 0x7f0811ee

.field public static final cm_suggested_promos_title:I = 0x7f0811f0

.field public static final cm_top_up_required:I = 0x7f0811f9

.field public static final code_generator:I = 0x7f080f41

.field public static final code_generator_about:I = 0x7f080f42

.field public static final code_generator_activate_button:I = 0x7f080f48

.field public static final code_generator_activating:I = 0x7f080f49

.field public static final code_generator_code_not_working_content:I = 0x7f080f45

.field public static final code_generator_code_not_working_restart_button:I = 0x7f080f47

.field public static final code_generator_code_not_working_title:I = 0x7f080f44

.field public static final code_generator_code_not_working_try_again_button:I = 0x7f080f46

.field public static final code_generator_code_refresh:I = 0x7f080f43

.field public static final code_generator_connection_error_content:I = 0x7f080f50

.field public static final code_generator_connection_error_title:I = 0x7f080f4f

.field public static final code_generator_enter_key_button:I = 0x7f080f4d

.field public static final code_generator_generic_error_content:I = 0x7f080f51

.field public static final code_generator_generic_error_title:I = 0x7f080f4e

.field public static final code_generator_provision_manual:I = 0x7f080f4a

.field public static final code_generator_provision_manual_error_content:I = 0x7f080f4c

.field public static final code_generator_provision_manual_error_title:I = 0x7f080f4b

.field public static final code_resent_email:I = 0x7f080f87

.field public static final code_resent_phone:I = 0x7f080f88

.field public static final collections_cancel_friend_request_failed:I = 0x7f080b21

.field public static final collections_cancel_groups_request_failed:I = 0x7f080b26

.field public static final collections_confirm_leave_group:I = 0x7f080b27

.field public static final collections_friend_removed:I = 0x7f080b1f

.field public static final collections_friend_request_cancelled:I = 0x7f080b1e

.field public static final collections_friend_request_failed:I = 0x7f080b20

.field public static final collections_friend_request_sent:I = 0x7f080b1d

.field public static final collections_groups_request_cancelled:I = 0x7f080b24

.field public static final collections_groups_request_failed:I = 0x7f080b25

.field public static final collections_groups_request_sent:I = 0x7f080b23

.field public static final collections_leave_group:I = 0x7f080b28

.field public static final collections_leave_group_failed:I = 0x7f080b2a

.field public static final collections_left_group:I = 0x7f080b29

.field public static final collections_nothing_to_show:I = 0x7f0811eb

.field public static final collections_remove_friend_failed:I = 0x7f080b22

.field public static final comma_delimited_value:I = 0x7f08113a

.field public static final comment_post_fail_ticker:I = 0x7f080f11

.field public static final comment_post_fail_title:I = 0x7f080f12

.field public static final commenter_post:I = 0x7f080554

.field public static final commenter_write_comment_in_page_voice:I = 0x7f080559

.field public static final commenter_write_reply_in_page_voice:I = 0x7f08055a

.field public static final comments_permalink_fragment_title:I = 0x7f0805df

.field public static final common_google_play_services_enable_button:I = 0x7f0806c0

.field public static final common_google_play_services_enable_text:I = 0x7f0806bf

.field public static final common_google_play_services_enable_title:I = 0x7f0806be

.field public static final common_google_play_services_error_notification_requested_by_msg:I = 0x7f0806b9

.field public static final common_google_play_services_install_button:I = 0x7f0806bd

.field public static final common_google_play_services_install_text_phone:I = 0x7f0806bb

.field public static final common_google_play_services_install_text_tablet:I = 0x7f0806bc

.field public static final common_google_play_services_install_title:I = 0x7f0806ba

.field public static final common_google_play_services_invalid_account_text:I = 0x7f0806c6

.field public static final common_google_play_services_invalid_account_title:I = 0x7f0806c5

.field public static final common_google_play_services_needs_enabling_title:I = 0x7f0806b8

.field public static final common_google_play_services_network_error_text:I = 0x7f0806c4

.field public static final common_google_play_services_network_error_title:I = 0x7f0806c3

.field public static final common_google_play_services_notification_needs_installation_title:I = 0x7f0806b6

.field public static final common_google_play_services_notification_needs_update_title:I = 0x7f0806b7

.field public static final common_google_play_services_notification_ticker:I = 0x7f0806b5

.field public static final common_google_play_services_unknown_issue:I = 0x7f0806c7

.field public static final common_google_play_services_unsupported_date_text:I = 0x7f0806ca

.field public static final common_google_play_services_unsupported_text:I = 0x7f0806c9

.field public static final common_google_play_services_unsupported_title:I = 0x7f0806c8

.field public static final common_google_play_services_update_button:I = 0x7f0806cb

.field public static final common_google_play_services_update_text:I = 0x7f0806c2

.field public static final common_google_play_services_update_title:I = 0x7f0806c1

.field public static final common_last_name_prefixes:I = 0x7f080196

.field public static final common_name_conjunctions:I = 0x7f080197

.field public static final common_name_prefixes:I = 0x7f080194

.field public static final common_name_suffixes:I = 0x7f080195

.field public static final common_signin_button_text:I = 0x7f0806cc

.field public static final common_signin_button_text_long:I = 0x7f0806cd

.field public static final compose_button_location_description:I = 0x7f08029e

.field public static final compose_button_stickers_description:I = 0x7f08029d

.field public static final compose_discard_dialog_discard_button:I = 0x7f08029a

.field public static final compose_discard_dialog_message:I = 0x7f080298

.field public static final compose_discard_dialog_no_button:I = 0x7f080299

.field public static final compose_discard_dialog_title:I = 0x7f080297

.field public static final compose_emoji_attachments_description:I = 0x7f08029c

.field public static final compose_location_disabled_nux_heading:I = 0x7f0802cb

.field public static final compose_location_disabled_nux_link:I = 0x7f0802ce

.field public static final compose_location_disabled_nux_text:I = 0x7f0802cc

.field public static final compose_location_disabled_nux_text_neue:I = 0x7f0802cd

.field public static final compose_location_nux:I = 0x7f0802c7

.field public static final compose_location_nux1:I = 0x7f0802c5

.field public static final compose_location_nux2:I = 0x7f0802c6

.field public static final compose_location_nux_neue:I = 0x7f0802ca

.field public static final compose_location_nux_off:I = 0x7f0802c9

.field public static final compose_location_nux_on:I = 0x7f0802c8

.field public static final compose_location_services_disabled_title:I = 0x7f0802cf

.field public static final compose_location_services_phone_disabled_description:I = 0x7f0802d0

.field public static final compose_location_services_phone_disabled_description2:I = 0x7f0802d2

.field public static final compose_location_services_phone_disabled_go_to_settings_button:I = 0x7f0802d1

.field public static final compose_location_services_setting_disabled_description:I = 0x7f0802d3

.field public static final compose_send:I = 0x7f080296

.field public static final composer_album_privacy_msg:I = 0x7f080819

.field public static final composer_album_privacy_title:I = 0x7f080818

.field public static final composer_audience:I = 0x7f080e88

.field public static final composer_audience_fixed_target:I = 0x7f080813

.field public static final composer_audience_fixed_tip_album:I = 0x7f080826

.field public static final composer_audience_fixed_tip_closed_group:I = 0x7f080828

.field public static final composer_audience_fixed_tip_friends_event:I = 0x7f08082c

.field public static final composer_audience_fixed_tip_open_child:I = 0x7f080829

.field public static final composer_audience_fixed_tip_open_event:I = 0x7f08082d

.field public static final composer_audience_fixed_tip_open_group:I = 0x7f08082a

.field public static final composer_audience_fixed_tip_page:I = 0x7f08082e

.field public static final composer_audience_fixed_tip_private_event:I = 0x7f08082b

.field public static final composer_audience_fixed_tip_timeline:I = 0x7f080827

.field public static final composer_audience_more_text:I = 0x7f08081c

.field public static final composer_audience_selector_education_text:I = 0x7f080812

.field public static final composer_audience_selector_title_text:I = 0x7f080811

.field public static final composer_audience_user_limit_message:I = 0x7f080815

.field public static final composer_audience_user_limit_title:I = 0x7f080814

.field public static final composer_checkin_title_text:I = 0x7f0807d0

.field public static final composer_done_button_text:I = 0x7f0807c8

.field public static final composer_edit_button_text:I = 0x7f0807cb

.field public static final composer_edit_title_text:I = 0x7f0807cc

.field public static final composer_education_page_review:I = 0x7f08085b

.field public static final composer_exit_dialog_better_checkin_title:I = 0x7f080807

.field public static final composer_exit_dialog_better_discard_privacy_message:I = 0x7f080810

.field public static final composer_exit_dialog_better_message:I = 0x7f080809

.field public static final composer_exit_dialog_better_title:I = 0x7f080804

.field public static final composer_exit_dialog_cancel:I = 0x7f08080c

.field public static final composer_exit_dialog_discard:I = 0x7f080801

.field public static final composer_exit_dialog_discard_privacy_message:I = 0x7f0807ff

.field public static final composer_exit_dialog_message:I = 0x7f0807fd

.field public static final composer_exit_dialog_message_offline:I = 0x7f080808

.field public static final composer_exit_dialog_ok:I = 0x7f08080e

.field public static final composer_exit_edit_dialog_better_message:I = 0x7f08080a

.field public static final composer_exit_edit_dialog_message:I = 0x7f080800

.field public static final composer_fixed_target_custom:I = 0x7f080821

.field public static final composer_fixed_target_event:I = 0x7f080824

.field public static final composer_fixed_target_event_friends:I = 0x7f080823

.field public static final composer_fixed_target_friends:I = 0x7f08081f

.field public static final composer_fixed_target_friends_of_friends:I = 0x7f080820

.field public static final composer_fixed_target_group_members:I = 0x7f08081e

.field public static final composer_fixed_target_members_of:I = 0x7f080825

.field public static final composer_fixed_target_public:I = 0x7f080822

.field public static final composer_friend_lists:I = 0x7f080e89

.field public static final composer_friends_of_tagged:I = 0x7f08082f

.field public static final composer_hint_album_creator_description:I = 0x7f080843

.field public static final composer_hint_album_title:I = 0x7f080842

.field public static final composer_hint_new_thread:I = 0x7f08029b

.field public static final composer_hint_tag_people:I = 0x7f080859

.field public static final composer_hint_tag_place_to_photo:I = 0x7f08085a

.field public static final composer_hint_tag_place_to_photos:I = 0x7f08089e

.field public static final composer_hint_tag_place_to_video:I = 0x7f0808a1

.field public static final composer_hint_upload_photo:I = 0x7f0807f7

.field public static final composer_hint_upload_photos:I = 0x7f0807f8

.field public static final composer_hint_upload_video:I = 0x7f0807f9

.field public static final composer_loading_token:I = 0x7f08081a

.field public static final composer_minutiae_education_text:I = 0x7f0807f1

.field public static final composer_minutiae_nux_accessibility:I = 0x7f0807f2

.field public static final composer_mixed_media_types_error:I = 0x7f0807df

.field public static final composer_og_action_hint_text:I = 0x7f0807ce

.field public static final composer_page_admin_post_title:I = 0x7f08081d

.field public static final composer_people_tagged:I = 0x7f080830

.field public static final composer_photo_attach_failed:I = 0x7f0807d1

.field public static final composer_plus_sign:I = 0x7f0807d2

.field public static final composer_publish_button_text:I = 0x7f0807c9

.field public static final composer_publish_error_general:I = 0x7f0807cf

.field public static final composer_publish_hint_text:I = 0x7f0807cd

.field public static final composer_publish_title_text:I = 0x7f0807ca

.field public static final composer_retry_initial:I = 0x7f08089a

.field public static final composer_retry_success:I = 0x7f08089b

.field public static final composer_share_nux_accessibility:I = 0x7f0807f4

.field public static final composer_share_nux_text:I = 0x7f0807f3

.field public static final composer_stream_publishing:I = 0x7f0807fb

.field public static final composer_suggestion_tag_place_to_photo:I = 0x7f08089c

.field public static final composer_suggestion_tag_place_to_photo_with_nux:I = 0x7f08089d

.field public static final composer_suggestion_tag_place_to_photos:I = 0x7f08089f

.field public static final composer_suggestion_tag_place_to_photos_with_nux:I = 0x7f0808a0

.field public static final composer_suggestion_tag_place_to_video:I = 0x7f0808a2

.field public static final composer_suggestion_tag_place_to_video_with_nux:I = 0x7f0808a3

.field public static final composer_tag_expansion_disabled_tip:I = 0x7f080831

.field public static final composer_target_selector_title:I = 0x7f08029f

.field public static final composer_title_tap_to_change:I = 0x7f080802

.field public static final composer_user_section_title:I = 0x7f08081b

.field public static final composer_write_recommendation_hint:I = 0x7f08086a

.field public static final confirm:I = 0x7f080e87

.field public static final confirm_city:I = 0x7f0809be

.field public static final confirm_request:I = 0x7f08016d

.field public static final confirmation_string:I = 0x7f080957

.field public static final confirmation_success:I = 0x7f080f9b

.field public static final confirming:I = 0x7f080f95

.field public static final connected:I = 0x7f08005a

.field public static final connected_captive_portal:I = 0x7f0803e2

.field public static final connected_captive_portal_cta:I = 0x7f0803e3

.field public static final connected_captive_portal_notification_title:I = 0x7f0803e4

.field public static final connecting:I = 0x7f080059

.field public static final consumption_photo_delete_dialog_no:I = 0x7f080641

.field public static final consumption_photo_delete_dialog_yes:I = 0x7f080640

.field public static final contact_card_cant_dail_number:I = 0x7f0803d4

.field public static final contact_card_device_unable_to_make_call:I = 0x7f0803d5

.field public static final contact_card_loading:I = 0x7f080222

.field public static final contact_card_view_page:I = 0x7f080223

.field public static final contact_delete_confirm_msg:I = 0x7f0803ed

.field public static final contact_delete_confirm_ok_button:I = 0x7f0803ee

.field public static final contact_delete_confirm_title:I = 0x7f0803ec

.field public static final contact_delete_progress:I = 0x7f0803ef

.field public static final contact_multipicker_top_friends_header:I = 0x7f08028d

.field public static final contact_notifications_dialog_label:I = 0x7f080241

.field public static final contact_notifications_disabled:I = 0x7f080239

.field public static final contact_notifications_enabled:I = 0x7f080238

.field public static final contact_notifications_label:I = 0x7f080240

.field public static final contact_notifications_muted_eight_hours:I = 0x7f08023c

.field public static final contact_notifications_muted_fifteen_minutes:I = 0x7f08023a

.field public static final contact_notifications_muted_one_hour:I = 0x7f08023b

.field public static final contact_notifications_muted_twenty_four_hours:I = 0x7f08023d

.field public static final contact_notifications_muted_until:I = 0x7f08023e

.field public static final contact_notifications_muted_until_alarm:I = 0x7f08023f

.field public static final contact_notifications_voice_reply_disabled:I = 0x7f080249

.field public static final contact_notifications_voice_reply_muted_eight_hours:I = 0x7f080245

.field public static final contact_notifications_voice_reply_muted_fifteen_minutes:I = 0x7f080243

.field public static final contact_notifications_voice_reply_muted_one_hour:I = 0x7f080244

.field public static final contact_notifications_voice_reply_muted_twenty_four_hours:I = 0x7f080246

.field public static final contact_notifications_voice_reply_muted_until_alarm:I = 0x7f080248

.field public static final contact_notifications_voice_reply_muted_until_morning:I = 0x7f080247

.field public static final contact_notifications_voice_reply_prompt:I = 0x7f080242

.field public static final contact_picker_active_now_header:I = 0x7f08028e

.field public static final contact_picker_add_heading:I = 0x7f080277

.field public static final contact_picker_chat_availability_off_banner_text:I = 0x7f08028f

.field public static final contact_picker_more_friends_header:I = 0x7f08028b

.field public static final contact_picker_no_results:I = 0x7f080275

.field public static final contact_picker_other_contacts_header:I = 0x7f08028c

.field public static final contact_picker_search_messages:I = 0x7f080278

.field public static final contacts_chat_action_detail:I = 0x7f080d4e

.field public static final contacts_chat_action_title:I = 0x7f080d4f

.field public static final contacts_loading:I = 0x7f080276

.field public static final contacts_profile_action:I = 0x7f080d4d

.field public static final contacts_sync:I = 0x7f080e8c

.field public static final continue_button_text:I = 0x7f080f94

.field public static final contributors:I = 0x7f081141

.field public static final control_widget_name:I = 0x7f080e70

.field public static final cookies:I = 0x7f080107

.field public static final corrupt_photo:I = 0x7f080790

.field public static final couldnt_load:I = 0x7f08003b

.field public static final cover_feed_nux_dialog_description:I = 0x7f080145

.field public static final cover_feed_nux_dialog_title:I = 0x7f080144

.field public static final cover_feed_shortcut_name:I = 0x7f080143

.field public static final cover_photo_view:I = 0x7f080b4c

.field public static final coverfeed_dialog_button_label:I = 0x7f080141

.field public static final coverfeed_dialog_text:I = 0x7f080140

.field public static final coverfeed_notification_updating:I = 0x7f080a72

.field public static final coverphoto_edit:I = 0x7f080775

.field public static final coworkers_section_header:I = 0x7f08027c

.field public static final crash:I = 0x7f080e85

.field public static final crash_dash:I = 0x7f0800ea

.field public static final create_album_create_error:I = 0x7f080844

.field public static final create_album_create_success:I = 0x7f080845

.field public static final create_album_create_title:I = 0x7f080e8d

.field public static final create_album_creating:I = 0x7f080e8e

.field public static final create_album_description_hint:I = 0x7f080e8f

.field public static final create_album_edit_error:I = 0x7f080e90

.field public static final create_album_edit_success:I = 0x7f080e91

.field public static final create_album_edit_title:I = 0x7f080e92

.field public static final create_album_editing:I = 0x7f080e93

.field public static final create_album_location_hint:I = 0x7f080e94

.field public static final create_album_missing_name:I = 0x7f080e95

.field public static final create_album_name_hint:I = 0x7f080e96

.field public static final create_album_no_changes:I = 0x7f080e97

.field public static final create_album_privacy:I = 0x7f080e98

.field public static final create_album_quit_question:I = 0x7f080e99

.field public static final create_group_thread_progress:I = 0x7f08026d

.field public static final create_thread_no_recipients_name:I = 0x7f080291

.field public static final create_thread_progress:I = 0x7f080290

.field public static final credit_cards_list_button_add_new_card:I = 0x7f080d38

.field public static final credit_cards_list_header:I = 0x7f080d37

.field public static final credit_cards_list_item_info:I = 0x7f080d39

.field public static final crop_picture_hint:I = 0x7f080949

.field public static final custom_card_button_save_card:I = 0x7f080cd9

.field public static final custom_card_header:I = 0x7f080cda

.field public static final custom_card_header_info:I = 0x7f080cdb

.field public static final custom_card_must_have_message:I = 0x7f080cde

.field public static final custom_card_must_have_recipient_name:I = 0x7f080cdc

.field public static final custom_card_must_have_sender_name:I = 0x7f080cdd

.field public static final custom_card_sender_name_hint:I = 0x7f080cd8

.field public static final custom_menu_more:I = 0x7f080944

.field public static final dash_added_cover_feed_shortcut_toast:I = 0x7f080a56

.field public static final dash_caption_profile_pic:I = 0x7f0800f1

.field public static final dash_cover_feed_shortcut_exists_toast:I = 0x7f080a57

.field public static final dash_empty_stories_text:I = 0x7f080135

.field public static final dash_facebook_story_source:I = 0x7f0800e7

.field public static final dash_fail_to_upload_app_feed_config:I = 0x7f0809e3

.field public static final dash_fb4a_bookmarks_group_title:I = 0x7f08014a

.field public static final dash_fb4a_bookmarks_group_title_new:I = 0x7f080148

.field public static final dash_fb4a_bookmarks_settings:I = 0x7f08014b

.field public static final dash_fb4a_bookmarks_settings_new:I = 0x7f080149

.field public static final dash_fb4a_bookmarks_start_dash:I = 0x7f08014c

.field public static final dash_feed_fetch_connection_fail:I = 0x7f0800fb

.field public static final dash_feed_fetch_network_fail:I = 0x7f0800fc

.field public static final dash_feed_instagram_story_title:I = 0x7f0800fd

.field public static final dash_gating_button:I = 0x7f08013e

.field public static final dash_gating_device_not_officially_supported_main:I = 0x7f08013c

.field public static final dash_gating_device_unsupported_main:I = 0x7f080139

.field public static final dash_gating_use_home_button:I = 0x7f08013d

.field public static final dash_gating_waitlist_footer:I = 0x7f08013b

.field public static final dash_gating_waitlist_main:I = 0x7f08013a

.field public static final dash_help_center:I = 0x7f0800e8

.field public static final dash_home_upgrade_error_text:I = 0x7f080152

.field public static final dash_ignore_button_text:I = 0x7f080154

.field public static final dash_lockcard_news_from_eight_entities:I = 0x7f080092

.field public static final dash_lockcard_news_from_five_entities:I = 0x7f08008f

.field public static final dash_lockcard_news_from_four_entities:I = 0x7f08008e

.field public static final dash_lockcard_news_from_more_than_eight_entities:I = 0x7f080093

.field public static final dash_lockcard_news_from_one_entity:I = 0x7f08008b

.field public static final dash_lockcard_news_from_seven_entities:I = 0x7f080091

.field public static final dash_lockcard_news_from_six_entities:I = 0x7f080090

.field public static final dash_lockcard_news_from_three_entities:I = 0x7f08008d

.field public static final dash_lockcard_news_from_two_entities:I = 0x7f08008c

.field public static final dash_lockcard_swipe_down_to_unlock:I = 0x7f08008a

.field public static final dash_logging_out_progress:I = 0x7f0800f0

.field public static final dash_login_button_label:I = 0x7f0800f7

.field public static final dash_login_disclosure_text1:I = 0x7f080100

.field public static final dash_login_disclosure_text1_lock_first:I = 0x7f080102

.field public static final dash_login_disclosure_text1_no_system_notifications:I = 0x7f080101

.field public static final dash_login_disclosure_text2:I = 0x7f080103

.field public static final dash_login_disclosure_text3:I = 0x7f080104

.field public static final dash_login_logo_description:I = 0x7f0800f8

.field public static final dash_logout:I = 0x7f0800fe

.field public static final dash_logout_question:I = 0x7f0800ff

.field public static final dash_ood_descrip_first:I = 0x7f080136

.field public static final dash_ood_descrip_second:I = 0x7f080137

.field public static final dash_ood_view_settings:I = 0x7f080138

.field public static final dash_preferences_account_category:I = 0x7f080130

.field public static final dash_preferences_app_feed_dialog_connected_disabled:I = 0x7f08014f

.field public static final dash_preferences_app_feed_dialog_connected_enabled:I = 0x7f08014e

.field public static final dash_preferences_app_feed_dialog_disconnected:I = 0x7f080150

.field public static final dash_preferences_app_feed_dialog_expired:I = 0x7f080151

.field public static final dash_preferences_app_feed_dialog_title:I = 0x7f08014d

.field public static final dash_preferences_data_use:I = 0x7f08011e

.field public static final dash_preferences_data_use_dialog_high:I = 0x7f080120

.field public static final dash_preferences_data_use_dialog_low:I = 0x7f080122

.field public static final dash_preferences_data_use_dialog_medium:I = 0x7f080121

.field public static final dash_preferences_data_use_dialog_title:I = 0x7f080124

.field public static final dash_preferences_data_use_summary:I = 0x7f08011f

.field public static final dash_preferences_data_use_wifi_only:I = 0x7f080123

.field public static final dash_preferences_deactivate_home_screen_toast:I = 0x7f080110

.field public static final dash_preferences_dialog_message_install_messenger:I = 0x7f08012f

.field public static final dash_preferences_dialog_message_update_messenger:I = 0x7f08012e

.field public static final dash_preferences_disable_facebook_home:I = 0x7f08010a

.field public static final dash_preferences_disable_facebook_home_dialog_confirm_button:I = 0x7f08010c

.field public static final dash_preferences_disable_facebook_home_dialog_message:I = 0x7f08010b

.field public static final dash_preferences_facebook_disabled_dialog_feedback_button:I = 0x7f080119

.field public static final dash_preferences_facebook_disabled_dialog_feedback_message:I = 0x7f080118

.field public static final dash_preferences_facebook_disabled_dialog_message:I = 0x7f080117

.field public static final dash_preferences_facebook_disabled_dialog_title:I = 0x7f080116

.field public static final dash_preferences_facebook_settings:I = 0x7f080131

.field public static final dash_preferences_get_home_dialog_install_now:I = 0x7f080114

.field public static final dash_preferences_get_home_dialog_instructions:I = 0x7f080111

.field public static final dash_preferences_get_home_dialog_instructions_step_one:I = 0x7f080112

.field public static final dash_preferences_get_home_dialog_instructions_step_two:I = 0x7f080113

.field public static final dash_preferences_help_center:I = 0x7f080133

.field public static final dash_preferences_home_as_home_screen:I = 0x7f08010f

.field public static final dash_preferences_home_as_screen_lock:I = 0x7f080115

.field public static final dash_preferences_keep_running_summary:I = 0x7f08011c

.field public static final dash_preferences_keep_running_title:I = 0x7f08011b

.field public static final dash_preferences_lock_screen_category:I = 0x7f08011d

.field public static final dash_preferences_logout:I = 0x7f080134

.field public static final dash_preferences_messages_category:I = 0x7f08012c

.field public static final dash_preferences_messages_settings:I = 0x7f08012d

.field public static final dash_preferences_open_app_feed_store:I = 0x7f08010d

.field public static final dash_preferences_open_app_feed_store_summary:I = 0x7f08010e

.field public static final dash_preferences_temperature_summary:I = 0x7f08012b

.field public static final dash_preferences_temperature_unit_celsius_short:I = 0x7f080128

.field public static final dash_preferences_temperature_unit_dialog_automatic:I = 0x7f080126

.field public static final dash_preferences_temperature_unit_dialog_celsius:I = 0x7f080127

.field public static final dash_preferences_temperature_unit_dialog_fahrenheit:I = 0x7f080129

.field public static final dash_preferences_temperature_unit_dialog_title:I = 0x7f080125

.field public static final dash_preferences_temperature_unit_fahrenheit_short:I = 0x7f08012a

.field public static final dash_preferences_terms:I = 0x7f080132

.field public static final dash_preferences_title:I = 0x7f080109

.field public static final dash_upgrade_button_text:I = 0x7f080153

.field public static final dashcard_message_attachment_text_description:I = 0x7f080a97

.field public static final dashcard_message_subject_description:I = 0x7f080a95

.field public static final dashcard_message_text_description:I = 0x7f080a96

.field public static final dashcard_notification_appfeed_reconnect_text:I = 0x7f080a8f

.field public static final dashcard_notification_appfeed_reconnect_title:I = 0x7f080a8e

.field public static final dashcard_notification_blank_state_notifications_text:I = 0x7f080a94

.field public static final dashcard_notification_dash_v2_nux_text:I = 0x7f080a9a

.field public static final dashcard_notification_dash_v2_nux_title:I = 0x7f080a99

.field public static final dashcard_notification_sender_description:I = 0x7f080a8a

.field public static final dashcard_notification_setup_notifications_music_text:I = 0x7f080a93

.field public static final dashcard_notification_setup_notifications_music_title:I = 0x7f080a92

.field public static final dashcard_notification_setup_notifications_text:I = 0x7f080a91

.field public static final dashcard_notification_setup_notifications_title:I = 0x7f080a90

.field public static final dashcard_notification_tap_to_view:I = 0x7f080a98

.field public static final dashcard_notification_text_description:I = 0x7f080a8c

.field public static final dashcard_notification_thumbnail_description:I = 0x7f080a8d

.field public static final dashcard_notification_title_description:I = 0x7f080a8b

.field public static final dashcard_tap_again_to_open:I = 0x7f080a76

.field public static final dashclock_extension_description:I = 0x7f0808db

.field public static final dashclock_extension_title:I = 0x7f0808da

.field public static final data_budget_expiration_confirm_close:I = 0x7f0801a7

.field public static final data_budget_expiration_continue_offline:I = 0x7f0801a9

.field public static final data_budget_expiration_continue_online:I = 0x7f0801a8

.field public static final data_budget_expiration_message:I = 0x7f0801a6

.field public static final data_budget_expiration_title:I = 0x7f0801a5

.field public static final data_budget_reached_notification_text:I = 0x7f0801a4

.field public static final data_budget_reached_notification_ticker:I = 0x7f0801a3

.field public static final data_settings_title:I = 0x7f080199

.field public static final data_usage_title:I = 0x7f080198

.field public static final data_use_policy:I = 0x7f080106

.field public static final date_time_ago_hours:I = 0x7f0802d7

.field public static final date_time_ago_just_now:I = 0x7f0802d4

.field public static final date_time_ago_minutes:I = 0x7f0802d6

.field public static final date_time_ago_seconds:I = 0x7f0802d5

.field public static final dbl_account_settings_title:I = 0x7f080d63

.field public static final dbl_add_passcode:I = 0x7f080d55

.field public static final dbl_change_passcode:I = 0x7f080d5f

.field public static final dbl_enter_passcode:I = 0x7f080d5a

.field public static final dbl_enter_passcode_title:I = 0x7f080d64

.field public static final dbl_enter_password:I = 0x7f080d5d

.field public static final dbl_forgot_password_link:I = 0x7f080d6a

.field public static final dbl_generic_error_message:I = 0x7f080d6c

.field public static final dbl_incorrect_login_approvals_code:I = 0x7f080d6e

.field public static final dbl_incorrect_passcode_error:I = 0x7f080d6d

.field public static final dbl_incorrect_password_error:I = 0x7f080d5c

.field public static final dbl_invalid_passcode_error_message:I = 0x7f080d5e

.field public static final dbl_invalid_password_error_message:I = 0x7f080d6f

.field public static final dbl_log_into_another_account:I = 0x7f080d58

.field public static final dbl_login_settings:I = 0x7f080d57

.field public static final dbl_login_signin:I = 0x7f080d69

.field public static final dbl_nux_add_passcode_message:I = 0x7f080d56

.field public static final dbl_nux_error_message:I = 0x7f080d59

.field public static final dbl_nux_message:I = 0x7f080d51

.field public static final dbl_nux_title:I = 0x7f080d50

.field public static final dbl_off:I = 0x7f080d54

.field public static final dbl_on:I = 0x7f080d53

.field public static final dbl_passcode_prompt:I = 0x7f080d52

.field public static final dbl_password_placeholder:I = 0x7f080d6b

.field public static final dbl_remove_account:I = 0x7f080d61

.field public static final dbl_remove_account_message:I = 0x7f080d62

.field public static final dbl_remove_passcode:I = 0x7f080d60

.field public static final dbl_subtitle_create_new_passcode:I = 0x7f080d67

.field public static final dbl_subtitle_create_passcode:I = 0x7f080d65

.field public static final dbl_subtitle_enter_old_passcode:I = 0x7f080d66

.field public static final dbl_subtitle_incorrect_passcode_flow:I = 0x7f080d68

.field public static final dbl_verify_passcode_error_message:I = 0x7f080d5b

.field public static final debug_droidinspector_enabled_summary:I = 0x7f080f9d

.field public static final debug_droidinspector_enabled_title:I = 0x7f080f9c

.field public static final debug_fb_tracing:I = 0x7f0804bb

.field public static final debug_fb_tracing_description:I = 0x7f0804bc

.field public static final debug_force_crash:I = 0x7f0804b4

.field public static final debug_force_fb4a_look_and_feel:I = 0x7f0804bd

.field public static final debug_force_fb4a_look_and_feel_description:I = 0x7f0804be

.field public static final debug_fps_enabled_summary:I = 0x7f080952

.field public static final debug_fps_enabled_title:I = 0x7f080951

.field public static final debug_fps_logging_enabled_title:I = 0x7f080953

.field public static final debug_goto_contacts_list:I = 0x7f08050b

.field public static final debug_http_proxy_dialog_title:I = 0x7f080011

.field public static final debug_http_proxy_hint:I = 0x7f080012

.field public static final debug_http_proxy_summary:I = 0x7f080010

.field public static final debug_http_proxy_title:I = 0x7f08000f

.field public static final debug_log_description:I = 0x7f0804b3

.field public static final debug_log_enable:I = 0x7f0804b2

.field public static final debug_log_level:I = 0x7f080013

.field public static final debug_log_send:I = 0x7f0804b1

.field public static final debug_mqtt_sandbox_description:I = 0x7f0804c7

.field public static final debug_mqtt_sandbox_hint:I = 0x7f0804c8

.field public static final debug_mqtt_sandbox_title:I = 0x7f0804c6

.field public static final debug_mqtt_server_tier_description:I = 0x7f0804c5

.field public static final debug_mqtt_server_tier_title:I = 0x7f0804c4

.field public static final debug_net_log_access:I = 0x7f08050c

.field public static final debug_php_profiling:I = 0x7f0804b5

.field public static final debug_php_profiling_description:I = 0x7f0804b6

.field public static final debug_push_notif_device_id_title:I = 0x7f080506

.field public static final debug_push_notif_last_register_title:I = 0x7f080509

.field public static final debug_push_notif_last_request_title:I = 0x7f080508

.field public static final debug_push_notif_register_unregister_title:I = 0x7f08050a

.field public static final debug_push_notif_token_title:I = 0x7f080507

.field public static final debug_ssl_cert_check_summary:I = 0x7f08000e

.field public static final debug_ssl_cert_check_title:I = 0x7f08000d

.field public static final debug_teak_profiling:I = 0x7f0804b7

.field public static final debug_teak_profiling_description:I = 0x7f0804b8

.field public static final debug_toggle_neue:I = 0x7f0804b0

.field public static final debug_ui_thread_watchdog_pref_summary:I = 0x7f080a9c

.field public static final debug_ui_thread_watchdog_pref_title:I = 0x7f080a9b

.field public static final debug_upload_contacts_batch_size_description:I = 0x7f0804ca

.field public static final debug_upload_contacts_batch_size_title:I = 0x7f0804c9

.field public static final debug_upload_contacts_continuous_import_title:I = 0x7f0804cd

.field public static final debug_upload_contacts_description:I = 0x7f0804cc

.field public static final debug_upload_contacts_title:I = 0x7f0804cb

.field public static final debug_viewserver_enabled_summary:I = 0x7f080f9f

.field public static final debug_viewserver_enabled_title:I = 0x7f080f9e

.field public static final debug_voip_adaptive_fec_turn_on_bwe_threshold_desc:I = 0x7f0804dc

.field public static final debug_voip_adaptive_fec_turn_on_bwe_threshold_title:I = 0x7f0804db

.field public static final debug_voip_aecm_mode_title:I = 0x7f0804d0

.field public static final debug_voip_audio_mode_description:I = 0x7f0804cf

.field public static final debug_voip_audio_mode_title:I = 0x7f0804ce

.field public static final debug_voip_auto_answer_title:I = 0x7f0804ed

.field public static final debug_voip_automated_test_support_title:I = 0x7f0804ec

.field public static final debug_voip_bwe_logging_description:I = 0x7f0804d7

.field public static final debug_voip_bwe_logging_title:I = 0x7f0804d6

.field public static final debug_voip_codec_override_mode_description:I = 0x7f0804d3

.field public static final debug_voip_codec_override_mode_title:I = 0x7f0804d2

.field public static final debug_voip_codec_override_rate_description:I = 0x7f0804d5

.field public static final debug_voip_codec_override_rate_title:I = 0x7f0804d4

.field public static final debug_voip_disable_relay:I = 0x7f0804fa

.field public static final debug_voip_dtls_description:I = 0x7f0804e7

.field public static final debug_voip_dtls_title:I = 0x7f0804e6

.field public static final debug_voip_fec_description:I = 0x7f0804da

.field public static final debug_voip_fec_title:I = 0x7f0804d9

.field public static final debug_voip_isac_excess_jitter_description:I = 0x7f0804e2

.field public static final debug_voip_isac_excess_jitter_title:I = 0x7f0804e1

.field public static final debug_voip_logging_mode:I = 0x7f0804f9

.field public static final debug_voip_no_voice_communication_title:I = 0x7f0804d1

.field public static final debug_voip_opus_bw_description:I = 0x7f0804de

.field public static final debug_voip_opus_bw_title:I = 0x7f0804dd

.field public static final debug_voip_opus_complexity_description:I = 0x7f0804e0

.field public static final debug_voip_opus_complexity_title:I = 0x7f0804df

.field public static final debug_voip_opus_excess_jitter_description:I = 0x7f0804e8

.field public static final debug_voip_opus_excess_jitter_title:I = 0x7f0804e5

.field public static final debug_voip_play_sample_title:I = 0x7f0804f4

.field public static final debug_voip_record_directory_description:I = 0x7f0804f2

.field public static final debug_voip_record_directory_hint:I = 0x7f0804f3

.field public static final debug_voip_record_directory_title:I = 0x7f0804f1

.field public static final debug_voip_record_mic_title:I = 0x7f0804ee

.field public static final debug_voip_record_playout_title:I = 0x7f0804f0

.field public static final debug_voip_record_raw_mic_title:I = 0x7f0804ef

.field public static final debug_voip_record_sampling_rate:I = 0x7f0804f8

.field public static final debug_voip_sample_file_description:I = 0x7f0804f6

.field public static final debug_voip_sample_file_hint:I = 0x7f0804f7

.field public static final debug_voip_sample_file_title:I = 0x7f0804f5

.field public static final debug_voip_speex_excess_jitter_description:I = 0x7f0804e4

.field public static final debug_voip_speex_excess_jitter_title:I = 0x7f0804e3

.field public static final debug_voip_use_jni_audio_description:I = 0x7f0804eb

.field public static final debug_voip_use_jni_audio_title:I = 0x7f0804ea

.field public static final debug_voip_volume_stream_type_description:I = 0x7f0804e9

.field public static final debug_voip_volume_stream_type_title:I = 0x7f0804d8

.field public static final debug_voip_webrtc_aecm_mode_title:I = 0x7f080502

.field public static final debug_voip_webrtc_agc_mode_title:I = 0x7f080503

.field public static final debug_voip_webrtc_agc_title:I = 0x7f0804fc

.field public static final debug_voip_webrtc_cng_title:I = 0x7f0804ff

.field public static final debug_voip_webrtc_eagc_title:I = 0x7f080500

.field public static final debug_voip_webrtc_ec_mode_title:I = 0x7f080501

.field public static final debug_voip_webrtc_ec_title:I = 0x7f0804fb

.field public static final debug_voip_webrtc_hpf_title:I = 0x7f0804fe

.field public static final debug_voip_webrtc_lafns_mode_title:I = 0x7f080505

.field public static final debug_voip_webrtc_ns_mode_title:I = 0x7f080504

.field public static final debug_voip_webrtc_ns_title:I = 0x7f0804fd

.field public static final debug_web_sandbox_description:I = 0x7f0804c2

.field public static final debug_web_sandbox_hint:I = 0x7f0804c3

.field public static final debug_web_sandbox_title:I = 0x7f0804c1

.field public static final debug_web_server_tier_description:I = 0x7f0804c0

.field public static final debug_web_server_tier_title:I = 0x7f0804bf

.field public static final debug_wirehog_profiling:I = 0x7f0804b9

.field public static final debug_wirehog_profiling_description:I = 0x7f0804ba

.field public static final delete_event_message:I = 0x7f080fea

.field public static final delete_event_negative_button:I = 0x7f080fec

.field public static final delete_event_positive_button:I = 0x7f080feb

.field public static final delete_event_progress_message:I = 0x7f080fed

.field public static final delete_event_title:I = 0x7f080fe9

.field public static final delete_request:I = 0x7f08016c

.field public static final dialog_cancel:I = 0x7f08001b

.field public static final dialog_close:I = 0x7f08011a

.field public static final dialog_confirm:I = 0x7f08001c

.field public static final dialog_confirm_block:I = 0x7f080174

.field public static final dialog_confirm_unfollow:I = 0x7f080173

.field public static final dialog_confirm_unfriend:I = 0x7f080172

.field public static final dialog_continue:I = 0x7f08001d

.field public static final dialog_delete:I = 0x7f08001e

.field public static final dialog_done:I = 0x7f08001f

.field public static final dialog_edit:I = 0x7f080020

.field public static final dialog_enable:I = 0x7f0801e8

.field public static final dialog_no:I = 0x7f080022

.field public static final dialog_not_now:I = 0x7f0801e9

.field public static final dialog_ok:I = 0x7f08001a

.field public static final dialog_yes:I = 0x7f080021

.field public static final diode_content_install_later:I = 0x7f080005

.field public static final diode_content_install_now:I = 0x7f080006

.field public static final diode_content_new_user:I = 0x7f080007

.field public static final diode_get_app:I = 0x7f080001

.field public static final diode_learn_more:I = 0x7f08000b

.field public static final diode_promo_header_install_later:I = 0x7f080002

.field public static final diode_promo_header_install_now:I = 0x7f080003

.field public static final diode_promo_header_new_user:I = 0x7f080004

.field public static final diode_promo_social_context_1_friend:I = 0x7f080008

.field public static final diode_promo_social_context_2_friends:I = 0x7f080009

.field public static final diode_promo_social_context_3_friends_or_more:I = 0x7f08000a

.field public static final diode_remind_later:I = 0x7f080000

.field public static final discard_draft:I = 0x7f080854

.field public static final disturbing_warning_label:I = 0x7f080487

.field public static final disturbing_warning_message:I = 0x7f080489

.field public static final disturbing_warning_question:I = 0x7f08048a

.field public static final disturbing_warning_title:I = 0x7f080488

.field public static final divebar_availability_disabled_warning_button:I = 0x7f0803c4

.field public static final divebar_availability_disabled_warning_line:I = 0x7f0803c3

.field public static final divebar_favorites_unsaved_changes_message:I = 0x7f0801f4

.field public static final divebar_favorites_unsaved_changes_negative:I = 0x7f0801f6

.field public static final divebar_favorites_unsaved_changes_positive:I = 0x7f0801f5

.field public static final divebar_nearby_friends_row_subtitle_empty:I = 0x7f0801f3

.field public static final divebar_nearby_friends_row_subtitle_upsell:I = 0x7f0801f2

.field public static final divebar_nearby_friends_row_title:I = 0x7f0801f1

.field public static final done:I = 0x7f08094a

.field public static final done_button_text:I = 0x7f0806ae

.field public static final done_favorites_button_label:I = 0x7f08027f

.field public static final done_picking_friends:I = 0x7f080c74

.field public static final done_picking_groups:I = 0x7f080c75

.field public static final dont_ask_again:I = 0x7f080c56

.field public static final doodle_discard_dialog_discard_button:I = 0x7f080424

.field public static final doodle_discard_dialog_message:I = 0x7f080422

.field public static final doodle_discard_dialog_no_button:I = 0x7f080423

.field public static final doodle_discard_dialog_title:I = 0x7f080421

.field public static final download_new_build:I = 0x7f080c8d

.field public static final downloading_profile_pic:I = 0x7f08094e

.field public static final edit_album_size_error_message:I = 0x7f08003d

.field public static final edit_caption_cancel_dialog_no:I = 0x7f08063b

.field public static final edit_caption_cancel_dialog_text:I = 0x7f080639

.field public static final edit_caption_cancel_dialog_title:I = 0x7f080638

.field public static final edit_caption_cancel_dialog_yes:I = 0x7f08063a

.field public static final edit_favorites_button_label:I = 0x7f080280

.field public static final edit_photo_too_small_error_message:I = 0x7f08003e

.field public static final edit_privacy_set_story_privacy_failed:I = 0x7f0809d6

.field public static final edit_privacy_title_text:I = 0x7f0809d3

.field public static final edit_story_promotion:I = 0x7f0805cc

.field public static final edit_video:I = 0x7f080428

.field public static final education_collapse_content_description:I = 0x7f0809db

.field public static final education_expand_content_description:I = 0x7f0809dc

.field public static final elapsed_time_today:I = 0x7f0800ee

.field public static final elapsed_time_yesterday:I = 0x7f0800ef

.field public static final ellipses:I = 0x7f080553

.field public static final emoji:I = 0x7f08047a

.field public static final emoji_emotions:I = 0x7f08047b

.field public static final emoji_nature:I = 0x7f08047c

.field public static final emoji_numbers:I = 0x7f08047f

.field public static final emoji_objects:I = 0x7f08047d

.field public static final emoji_vehicles:I = 0x7f08047e

.field public static final enable_dash:I = 0x7f0800eb

.field public static final enable_dash_as_lock:I = 0x7f0800ec

.field public static final enable_dash_as_lock_summary:I = 0x7f0800ed

.field public static final end_call_button_description:I = 0x7f0801d8

.field public static final enter_email:I = 0x7f080f8b

.field public static final enter_new_email:I = 0x7f080f8d

.field public static final enter_new_phone:I = 0x7f080f8e

.field public static final enter_phone:I = 0x7f080f8c

.field public static final enter_your_phone_number:I = 0x7f080dd0

.field public static final entitycards_fetch_error_occurred:I = 0x7f08105e

.field public static final entitycards_tap_to_retry:I = 0x7f08105f

.field public static final entitycardsplugins_person_view_profile:I = 0x7f081060

.field public static final error:I = 0x7f080e9a

.field public static final error_default:I = 0x7f080ca4

.field public static final error_facebook:I = 0x7f080ca5

.field public static final error_loading:I = 0x7f080e9b

.field public static final error_loading_media:I = 0x7f080418

.field public static final error_network:I = 0x7f080ca2

.field public static final error_no_connection:I = 0x7f080ca6

.field public static final error_opening_third_party_application:I = 0x7f08000c

.field public static final error_unknown:I = 0x7f080ca3

.field public static final error_with_code_and_reason:I = 0x7f0808d9

.field public static final error_with_reason:I = 0x7f080e9c

.field public static final event_about_title:I = 0x7f080fcf

.field public static final event_accessibility_create_event:I = 0x7f080eac

.field public static final event_admin_posts_only:I = 0x7f080fa0

.field public static final event_artist_subtitle:I = 0x7f080fd0

.field public static final event_artist_viewer_likes:I = 0x7f080fd1

.field public static final event_automatically_validated:I = 0x7f080fa1

.field public static final event_clear_time:I = 0x7f080e9d

.field public static final event_create_error:I = 0x7f080e9e

.field public static final event_creation_discard_no:I = 0x7f080ea2

.field public static final event_creation_discard_prompt_message:I = 0x7f080ea0

.field public static final event_creation_discard_prompt_title:I = 0x7f080e9f

.field public static final event_creation_discard_yes:I = 0x7f080ea1

.field public static final event_creation_processing:I = 0x7f080fa3

.field public static final event_description_hint:I = 0x7f080ea3

.field public static final event_duration_too_long:I = 0x7f080fa2

.field public static final event_edit_progress_message:I = 0x7f080fb5

.field public static final event_end_date:I = 0x7f080ea4

.field public static final event_end_time:I = 0x7f080ea5

.field public static final event_free_tickets_available:I = 0x7f080fc7

.field public static final event_invite_friends:I = 0x7f080ea6

.field public static final event_location:I = 0x7f080ea7

.field public static final event_location_as_text_only:I = 0x7f080fa6

.field public static final event_location_copy_address:I = 0x7f080fc3

.field public static final event_location_copy_location:I = 0x7f080fc4

.field public static final event_location_open_in_maps:I = 0x7f080fc2

.field public static final event_location_view_page:I = 0x7f080fc1

.field public static final event_more_details:I = 0x7f0811c3

.field public static final event_name_hint:I = 0x7f080ea8

.field public static final event_no_name_error:I = 0x7f080ea9

.field public static final event_post:I = 0x7f080fa4

.field public static final event_privacy:I = 0x7f080eaa

.field public static final event_start_time:I = 0x7f080eab

.field public static final event_tickets_available:I = 0x7f080fcc

.field public static final event_tickets_available_on:I = 0x7f080fc9

.field public static final event_tickets_find_tickets:I = 0x7f080fce

.field public static final event_tickets_for_free:I = 0x7f080fc6

.field public static final event_tickets_from_min_price:I = 0x7f080fc0

.field public static final event_tickets_price_range:I = 0x7f080fc5

.field public static final event_tickets_sold_out:I = 0x7f080fcb

.field public static final event_tickets_via_vendor:I = 0x7f080fc8

.field public static final event_tickets_with_price:I = 0x7f080fca

.field public static final event_time_timezone_template:I = 0x7f080fa5

.field public static final events_create_button_nux:I = 0x7f08103c

.field public static final events_dashboard_birthdays_this_month_header:I = 0x7f08102d

.field public static final events_dashboard_create_button:I = 0x7f081010

.field public static final events_dashboard_event_for_group:I = 0x7f08101e

.field public static final events_dashboard_hosting_filter_header:I = 0x7f08100f

.field public static final events_dashboard_invited_by:I = 0x7f08101b

.field public static final events_dashboard_invited_by_group:I = 0x7f08101c

.field public static final events_dashboard_invited_filter_header:I = 0x7f08100e

.field public static final events_dashboard_no_more_hosting:I = 0x7f081013

.field public static final events_dashboard_no_more_invited:I = 0x7f081012

.field public static final events_dashboard_no_more_past:I = 0x7f081015

.field public static final events_dashboard_no_more_saved:I = 0x7f081014

.field public static final events_dashboard_no_more_upcoming:I = 0x7f081011

.field public static final events_dashboard_options_button_content_description_going:I = 0x7f081016

.field public static final events_dashboard_options_button_content_description_hosting:I = 0x7f08101a

.field public static final events_dashboard_options_button_content_description_maybe:I = 0x7f081017

.field public static final events_dashboard_options_button_content_description_not_going:I = 0x7f081018

.field public static final events_dashboard_options_button_content_description_saved:I = 0x7f081019

.field public static final events_dashboard_past_filter_header:I = 0x7f08100c

.field public static final events_dashboard_saved_filter_header:I = 0x7f08100d

.field public static final events_dashboard_social_context_friends_going_singular:I = 0x7f08101f

.field public static final events_dashboard_social_context_friends_maybe_singular:I = 0x7f081020

.field public static final events_dashboard_suggestions_button_content_description_null_state:I = 0x7f081031

.field public static final events_dashboard_suggestions_card_category_and_time:I = 0x7f081030

.field public static final events_dashboard_suggestions_card_title_default:I = 0x7f08102f

.field public static final events_dashboard_suggestions_card_view_all_template:I = 0x7f081032

.field public static final events_dashboard_time_summary_date_with_time_template:I = 0x7f081022

.field public static final events_dashboard_time_summary_later:I = 0x7f081029

.field public static final events_dashboard_time_summary_later_in_month:I = 0x7f08102a

.field public static final events_dashboard_time_summary_multi_day_template:I = 0x7f081023

.field public static final events_dashboard_time_summary_next_week:I = 0x7f081028

.field public static final events_dashboard_time_summary_this_week:I = 0x7f081027

.field public static final events_dashboard_time_summary_today:I = 0x7f081025

.field public static final events_dashboard_time_summary_tomorrow:I = 0x7f081026

.field public static final events_dashboard_time_summary_yesterday:I = 0x7f081024

.field public static final events_dashboard_title:I = 0x7f08100a

.field public static final events_dashboard_upcoming_birthdays_header:I = 0x7f08102b

.field public static final events_dashboard_upcoming_filter_header:I = 0x7f08100b

.field public static final events_dashboard_user_saved:I = 0x7f08101d

.field public static final events_dashboard_view_all:I = 0x7f081021

.field public static final events_dashboard_view_all_birthdays:I = 0x7f08102c

.field public static final events_feed_decline_stories_button_text:I = 0x7f081040

.field public static final events_feed_loading_failed:I = 0x7f081041

.field public static final events_feed_stories_button_text:I = 0x7f08103f

.field public static final events_feed_title:I = 0x7f08103e

.field public static final events_guestlist_friends_actor_type_friends:I = 0x7f080ffd

.field public static final events_guestlist_friends_actor_type_others:I = 0x7f080ffe

.field public static final events_guestlist_message_button_description:I = 0x7f08102e

.field public static final events_guestlist_no_friends:I = 0x7f080fff

.field public static final events_guestlist_no_friends_going:I = 0x7f081000

.field public static final events_guestlist_no_friends_invited:I = 0x7f081002

.field public static final events_guestlist_no_friends_maybe:I = 0x7f081001

.field public static final events_guestlist_no_others:I = 0x7f081003

.field public static final events_guestlist_no_others_going:I = 0x7f081004

.field public static final events_guestlist_no_others_invited:I = 0x7f081006

.field public static final events_guestlist_no_others_maybe:I = 0x7f081005

.field public static final events_guestlist_title:I = 0x7f080ff9

.field public static final events_guestlist_title_going:I = 0x7f080ffa

.field public static final events_guestlist_title_invited:I = 0x7f080ffc

.field public static final events_guestlist_title_maybe:I = 0x7f080ffb

.field public static final events_hosts_title:I = 0x7f080ff8

.field public static final events_invite_friends_title:I = 0x7f080fb1

.field public static final events_invite_friends_who_else:I = 0x7f080fb2

.field public static final events_launcher_megaphone_accepting_toast:I = 0x7f081038

.field public static final events_launcher_megaphone_primary_button:I = 0x7f081036

.field public static final events_launcher_megaphone_secondary_button:I = 0x7f081037

.field public static final events_launcher_megaphone_subtitle:I = 0x7f081035

.field public static final events_launcher_megaphone_title:I = 0x7f081034

.field public static final events_launcher_shortcut_name:I = 0x7f081033

.field public static final events_num_people_invited:I = 0x7f080fb0

.field public static final events_permalink_address_single_line_formatter:I = 0x7f080fbd

.field public static final events_permalink_event_approx_location_with_distance:I = 0x7f080fbe

.field public static final events_permalink_event_for_privacy_label:I = 0x7f080fb6

.field public static final events_permalink_event_location_city_state:I = 0x7f080fbf

.field public static final events_permalink_event_with:I = 0x7f080fb8

.field public static final events_permalink_failed_to_load:I = 0x7f081009

.field public static final events_permalink_going_count_title:I = 0x7f080ff5

.field public static final events_permalink_guest_summary_going_long_one:I = 0x7f080ff1

.field public static final events_permalink_guest_summary_going_long_two:I = 0x7f080ff2

.field public static final events_permalink_guest_summary_maybe_long_one:I = 0x7f080ff3

.field public static final events_permalink_guest_summary_maybe_long_two:I = 0x7f080ff4

.field public static final events_permalink_guest_summary_user_going:I = 0x7f080fee

.field public static final events_permalink_guest_summary_user_hosting:I = 0x7f080ff0

.field public static final events_permalink_guest_summary_user_maybe:I = 0x7f080fef

.field public static final events_permalink_happening_now:I = 0x7f080fbc

.field public static final events_permalink_hosted_by:I = 0x7f080fb7

.field public static final events_permalink_invited_by:I = 0x7f080fb4

.field public static final events_permalink_invited_count_title:I = 0x7f080ff7

.field public static final events_permalink_maybe_count_title:I = 0x7f080ff6

.field public static final events_permalink_starts_in_few_seconds:I = 0x7f080fbb

.field public static final events_permalink_subtitle_and_other:I = 0x7f080fb9

.field public static final events_permalink_subtitle_and_others:I = 0x7f080fba

.field public static final events_permalink_ticket_subtitle_with_others:I = 0x7f080fcd

.field public static final events_permalink_title:I = 0x7f080fb3

.field public static final events_posting_progress:I = 0x7f081007

.field public static final events_privacy_guests_and_friends:I = 0x7f080fac

.field public static final events_privacy_guests_and_friends_details:I = 0x7f080fad

.field public static final events_privacy_invite_only:I = 0x7f080fae

.field public static final events_privacy_invite_only_details:I = 0x7f080faf

.field public static final events_privacy_open_invite:I = 0x7f080faa

.field public static final events_privacy_open_invite_details:I = 0x7f080fab

.field public static final events_privacy_picker_title:I = 0x7f080fa7

.field public static final events_privacy_public:I = 0x7f080fa8

.field public static final events_privacy_public_details:I = 0x7f080fa9

.field public static final events_view_all_posts:I = 0x7f081008

.field public static final external_media_unsupported:I = 0x7f08078d

.field public static final external_storage_unavailable:I = 0x7f08078e

.field public static final facebook_user:I = 0x7f0806ac

.field public static final facepile_unshown_faces:I = 0x7f0800d4

.field public static final faceweb_accessibility_like:I = 0x7f080f3a

.field public static final faceweb_accessibility_unlike:I = 0x7f080f3b

.field public static final faceweb_contextmenu_copy:I = 0x7f080f38

.field public static final faceweb_contextmenu_open_in_browser:I = 0x7f080f39

.field public static final failure_loading_events:I = 0x7f080050

.field public static final favorites_delete_contact_prompt:I = 0x7f080285

.field public static final favorites_editor_title:I = 0x7f08028a

.field public static final favorites_section_title:I = 0x7f080288

.field public static final fb4a_short_product_name:I = 0x7f08049b

.field public static final feed_add_place_to_story:I = 0x7f080605

.field public static final feed_afro_confirmation_title:I = 0x7f08058f

.field public static final feed_app_collection_add_action:I = 0x7f0805e8

.field public static final feed_app_collection_add_error:I = 0x7f0805ea

.field public static final feed_app_collection_remove_action:I = 0x7f0805e9

.field public static final feed_app_collection_remove_error:I = 0x7f0805eb

.field public static final feed_banning_user:I = 0x7f080599

.field public static final feed_browser_cannot_load_page:I = 0x7f081195

.field public static final feed_browser_header_shares_context:I = 0x7f081194

.field public static final feed_browser_menu_item_copy_link:I = 0x7f08118f

.field public static final feed_browser_menu_item_copy_link_acknowledgement:I = 0x7f081196

.field public static final feed_browser_menu_item_open_with:I = 0x7f081191

.field public static final feed_browser_menu_item_open_with_specific_app:I = 0x7f081190

.field public static final feed_browser_menu_item_save_link:I = 0x7f081193

.field public static final feed_browser_menu_item_save_link_acknowledgement:I = 0x7f081197

.field public static final feed_browser_menu_item_save_link_failure:I = 0x7f081198

.field public static final feed_browser_menu_item_share_link:I = 0x7f081192

.field public static final feed_celebrations_message:I = 0x7f0805a3

.field public static final feed_celebrations_post:I = 0x7f0805a4

.field public static final feed_confirm_delete:I = 0x7f080594

.field public static final feed_continue_reading:I = 0x7f080584

.field public static final feed_copy_story_link:I = 0x7f0805ae

.field public static final feed_copy_story_text:I = 0x7f0805ad

.field public static final feed_coupon_claim_failed:I = 0x7f080573

.field public static final feed_coupon_expired:I = 0x7f08056f

.field public static final feed_coupon_get_offer:I = 0x7f08056d

.field public static final feed_coupon_resend_failed:I = 0x7f080574

.field public static final feed_coupon_resend_offer:I = 0x7f08056e

.field public static final feed_coupon_subtitle_instore:I = 0x7f080571

.field public static final feed_coupon_subtitle_online:I = 0x7f080570

.field public static final feed_coupon_subtitle_online_instores:I = 0x7f080572

.field public static final feed_delete:I = 0x7f080597

.field public static final feed_delete_story:I = 0x7f080593

.field public static final feed_delete_story_failed:I = 0x7f080595

.field public static final feed_deleting_comment:I = 0x7f080596

.field public static final feed_disable_notifications:I = 0x7f08059d

.field public static final feed_edit_privacy:I = 0x7f08059b

.field public static final feed_edit_story:I = 0x7f08059e

.field public static final feed_edited:I = 0x7f0805f1

.field public static final feed_enable_notifications:I = 0x7f08059c

.field public static final feed_end_story_symbol:I = 0x7f0805a0

.field public static final feed_explanation_header_trending_topic:I = 0x7f08055d

.field public static final feed_explanation_ncpp:I = 0x7f08055b

.field public static final feed_explanation_neko:I = 0x7f08055c

.field public static final feed_explanation_see_photo_chaining:I = 0x7f080564

.field public static final feed_feedback_comment_container_content_description:I = 0x7f0805fd

.field public static final feed_feedback_comments_many:I = 0x7f0805d6

.field public static final feed_feedback_comments_one:I = 0x7f0805d5

.field public static final feed_feedback_like_container_content_description:I = 0x7f0805fb

.field public static final feed_feedback_like_container_content_description_pressed:I = 0x7f0805fc

.field public static final feed_feedback_likes_many:I = 0x7f0805d4

.field public static final feed_feedback_likes_one:I = 0x7f0805d3

.field public static final feed_feedback_seen_by_many:I = 0x7f0805d8

.field public static final feed_feedback_seen_by_one:I = 0x7f0805d7

.field public static final feed_feedback_share_container_content_description:I = 0x7f0805fe

.field public static final feed_fetch_liker_list_failed:I = 0x7f0805e3

.field public static final feed_filter_recent_stories:I = 0x7f080f3e

.field public static final feed_filter_top_stories:I = 0x7f080f3d

.field public static final feed_find_friends_subtitle:I = 0x7f0805c5

.field public static final feed_find_friends_title:I = 0x7f0805c4

.field public static final feed_find_pages_button_text:I = 0x7f0805c9

.field public static final feed_find_pages_default_name:I = 0x7f0805c6

.field public static final feed_find_pages_subtitle:I = 0x7f0805c8

.field public static final feed_find_pages_title:I = 0x7f0805c7

.field public static final feed_flyout_no_comments:I = 0x7f0805e7

.field public static final feed_friends_nearby_ping_succeeded:I = 0x7f080c33

.field public static final feed_friends_nearby_see_all:I = 0x7f0805ac

.field public static final feed_hidden_story_afro_text:I = 0x7f080591

.field public static final feed_hidden_story_untag_afro_explanation:I = 0x7f080590

.field public static final feed_hidden_story_untag_afro_text:I = 0x7f080592

.field public static final feed_hide_story:I = 0x7f08058d

.field public static final feed_hide_story_error:I = 0x7f08058e

.field public static final feed_image_gallery_app_attribution:I = 0x7f0805f0

.field public static final feed_inline_video_pivot:I = 0x7f080566

.field public static final feed_instagram_photos_from_friends:I = 0x7f0805dd

.field public static final feed_install_instagram:I = 0x7f0805dc

.field public static final feed_install_now:I = 0x7f0805b4

.field public static final feed_is_up_to_date:I = 0x7f0805ff

.field public static final feed_keep_trying:I = 0x7f080598

.field public static final feed_like_or_comment:I = 0x7f080581

.field public static final feed_like_page:I = 0x7f08058a

.field public static final feed_loading_comments:I = 0x7f08056c

.field public static final feed_music_list_button:I = 0x7f080c36

.field public static final feed_music_list_install_application:I = 0x7f080c35

.field public static final feed_music_list_options_nux_text:I = 0x7f080c37

.field public static final feed_music_list_play_in_application:I = 0x7f080c34

.field public static final feed_new_stories:I = 0x7f080585

.field public static final feed_new_stories_badge:I = 0x7f080588

.field public static final feed_new_stories_badge_after_max_limit:I = 0x7f080589

.field public static final feed_no_content:I = 0x7f0805a1

.field public static final feed_offline_comment_posting:I = 0x7f080607

.field public static final feed_older_stories:I = 0x7f080587

.field public static final feed_page_review_survey_blacklist_item_failure:I = 0x7f0805a5

.field public static final feed_page_story_admin_attribution_nux_text:I = 0x7f0805d2

.field public static final feed_page_story_posted_by_admin:I = 0x7f0805d1

.field public static final feed_permalink_comment_fetching_failed:I = 0x7f0805e0

.field public static final feed_permalink_comment_optimistic_status_failed:I = 0x7f0805e6

.field public static final feed_permalink_comment_optimistic_status_posting:I = 0x7f0805e5

.field public static final feed_permalink_comment_post:I = 0x7f0805e4

.field public static final feed_permalink_comment_posting_failed:I = 0x7f0805e1

.field public static final feed_permalink_report:I = 0x7f08059a

.field public static final feed_permalink_story_fetching_failed:I = 0x7f0805e2

.field public static final feed_photo_chaining_photo:I = 0x7f080565

.field public static final feed_place_star_survey_unit_title:I = 0x7f080c31

.field public static final feed_post_search_no_content:I = 0x7f0805a2

.field public static final feed_presence_story_see_all:I = 0x7f080c2f

.field public static final feed_presence_story_see_all_with_num:I = 0x7f080c2e

.field public static final feed_presence_title:I = 0x7f080c30

.field public static final feed_publisher_check_in:I = 0x7f080580

.field public static final feed_publisher_photo:I = 0x7f08057e

.field public static final feed_publisher_photo_video:I = 0x7f08057f

.field public static final feed_publisher_status:I = 0x7f08057d

.field public static final feed_pymk_canceled:I = 0x7f0805a8

.field public static final feed_pymk_load_more_friends:I = 0x7f080c32

.field public static final feed_pymk_see_all:I = 0x7f0805a6

.field public static final feed_pymk_see_all_description:I = 0x7f0805a7

.field public static final feed_pyml_see_all:I = 0x7f0805a9

.field public static final feed_pyml_see_all_description:I = 0x7f0805aa

.field public static final feed_quick_cam_bottom_bar_button_pause:I = 0x7f081044

.field public static final feed_quick_cam_bottom_bar_button_play:I = 0x7f081043

.field public static final feed_quick_cam_bottom_bar_button_processing:I = 0x7f081047

.field public static final feed_quick_cam_bottom_bar_button_record:I = 0x7f081042

.field public static final feed_quick_cam_bottom_bar_button_recording:I = 0x7f081046

.field public static final feed_quick_cam_bottom_bar_button_stop:I = 0x7f081045

.field public static final feed_quick_cam_bottom_bar_play_message:I = 0x7f08104b

.field public static final feed_quick_cam_bottom_bar_processing_message:I = 0x7f08104a

.field public static final feed_quick_cam_bottom_bar_record_message:I = 0x7f081048

.field public static final feed_quick_cam_bottom_bar_recording_message:I = 0x7f081049

.field public static final feed_quick_cam_bundle_error_message:I = 0x7f08104c

.field public static final feed_quick_cam_freeze_screen_error_message:I = 0x7f08104d

.field public static final feed_quick_cam_top_bar_post:I = 0x7f08104e

.field public static final feed_read_card:I = 0x7f080609

.field public static final feed_remove_place_from_story:I = 0x7f080606

.field public static final feed_research_poll:I = 0x7f080c3c

.field public static final feed_research_poll_error_message:I = 0x7f080c40

.field public static final feed_research_poll_mark_all_that_apply:I = 0x7f080c3f

.field public static final feed_research_poll_thank_you:I = 0x7f080c41

.field public static final feed_research_poll_vote:I = 0x7f080c3e

.field public static final feed_research_poll_vote_call_to_action:I = 0x7f080c3d

.field public static final feed_retrigger_reaction:I = 0x7f0805af

.field public static final feed_see_more:I = 0x7f080582

.field public static final feed_see_more_at:I = 0x7f080583

.field public static final feed_seen_by_people:I = 0x7f0805db

.field public static final feed_settings_bookmark_nux:I = 0x7f080c82

.field public static final feed_settings_tab_nux:I = 0x7f080c81

.field public static final feed_sort_switcher_chronological_button_text:I = 0x7f0805f5

.field public static final feed_sort_switcher_description_text_chronological:I = 0x7f0805f7

.field public static final feed_sort_switcher_description_text_personalized:I = 0x7f0805f6

.field public static final feed_sort_switcher_done_button_text:I = 0x7f0805f3

.field public static final feed_sort_switcher_heading_text:I = 0x7f0805f2

.field public static final feed_sort_switcher_personalized_button_text:I = 0x7f0805f4

.field public static final feed_sort_switcher_update_failed:I = 0x7f0805f8

.field public static final feed_sponsored:I = 0x7f080567

.field public static final feed_sponsored_context:I = 0x7f0805b5

.field public static final feed_sponsored_demo:I = 0x7f080568

.field public static final feed_sports_stories_finish_marker:I = 0x7f080c3a

.field public static final feed_sports_stories_start_and_finish_marker:I = 0x7f080c3b

.field public static final feed_sports_stories_start_marker:I = 0x7f080c39

.field public static final feed_sports_stories_versus_text:I = 0x7f080c38

.field public static final feed_store_access_token_expire:I = 0x7f0809e4

.field public static final feed_store_card_connect_account:I = 0x7f0809e7

.field public static final feed_store_card_connect_sso:I = 0x7f0809e8

.field public static final feed_store_card_connect_without_sso:I = 0x7f0809e9

.field public static final feed_store_card_toggle:I = 0x7f0809eb

.field public static final feed_store_card_toggle_front:I = 0x7f0809ea

.field public static final feed_store_done_button:I = 0x7f0809e1

.field public static final feed_store_log_out_button:I = 0x7f0809e2

.field public static final feed_store_no_app_enable:I = 0x7f0809ec

.field public static final feed_store_problem_connecting:I = 0x7f0809ee

.field public static final feed_store_problem_connecting_to_app:I = 0x7f0809ed

.field public static final feed_store_problem_loading_feed_config:I = 0x7f0809ef

.field public static final feed_store_session_expire:I = 0x7f0809e6

.field public static final feed_store_tap_to_reconnect:I = 0x7f0809e5

.field public static final feed_store_title:I = 0x7f0809de

.field public static final feed_store_title_subtext:I = 0x7f0809e0

.field public static final feed_store_title_subtext_v2:I = 0x7f0809df

.field public static final feed_stories_loaded_in_background:I = 0x7f080608

.field public static final feed_story_actor1_via_actor2:I = 0x7f080575

.field public static final feed_story_actor_added_x_new_photos:I = 0x7f080576

.field public static final feed_story_cancel:I = 0x7f0805d9

.field public static final feed_story_commerce_attachment_buy_on_fb_badge:I = 0x7f080604

.field public static final feed_story_commerce_attachment_price_discount:I = 0x7f080603

.field public static final feed_story_confirm:I = 0x7f0805da

.field public static final feed_story_fallback_warning:I = 0x7f08060d

.field public static final feed_story_info_near:I = 0x7f080578

.field public static final feed_story_info_time_near:I = 0x7f08057b

.field public static final feed_story_info_time_via:I = 0x7f08057a

.field public static final feed_story_info_time_via_near:I = 0x7f08057c

.field public static final feed_story_info_via:I = 0x7f080577

.field public static final feed_story_info_via_near:I = 0x7f080579

.field public static final feed_story_location_section_more_checkins:I = 0x7f0805ab

.field public static final feed_story_postpost_badge_bubble:I = 0x7f080602

.field public static final feed_story_video_optimistic_text:I = 0x7f080601

.field public static final feed_story_video_optimistic_title:I = 0x7f080600

.field public static final feed_survey_completed_message:I = 0x7f080569

.field public static final feed_survey_error_message:I = 0x7f08056a

.field public static final feed_survey_progress:I = 0x7f08056b

.field public static final feed_target_actor_spacer:I = 0x7f080556

.field public static final feed_unlike_page:I = 0x7f08058c

.field public static final feed_unseen_stories:I = 0x7f080586

.field public static final feed_view_history:I = 0x7f08059f

.field public static final filter_stories_menu:I = 0x7f080f3c

.field public static final find_friends:I = 0x7f080175

.field public static final find_friends_add_all:I = 0x7f080da6

.field public static final find_friends_add_all_dialog_continue:I = 0x7f080da9

.field public static final find_friends_add_all_dialog_many_friends:I = 0x7f080da8

.field public static final find_friends_add_all_dialog_one_friend:I = 0x7f080da7

.field public static final find_friends_add_friends_contact_found:I = 0x7f080da3

.field public static final find_friends_add_friends_contacts_found:I = 0x7f080da4

.field public static final find_friends_add_friends_progress:I = 0x7f080da1

.field public static final find_friends_add_friends_title:I = 0x7f080da2

.field public static final find_friends_add_selected:I = 0x7f080da5

.field public static final find_friends_are_you_sure_add_friends:I = 0x7f080d96

.field public static final find_friends_are_you_sure_dont_add_friends:I = 0x7f080d97

.field public static final find_friends_are_you_sure_prompt:I = 0x7f080d95

.field public static final find_friends_dialog_text:I = 0x7f080d90

.field public static final find_friends_dialog_title:I = 0x7f080d8f

.field public static final find_friends_how_it_works_manage_contacts:I = 0x7f080d9d

.field public static final find_friends_how_it_works_settings:I = 0x7f080d9e

.field public static final find_friends_how_it_works_text_continuous:I = 0x7f080d9f

.field public static final find_friends_how_it_works_text_continuous_phone_number:I = 0x7f080da0

.field public static final find_friends_how_it_works_title:I = 0x7f080d9c

.field public static final find_friends_intro_continuous_footer:I = 0x7f080d99

.field public static final find_friends_intro_continuous_phone_number_footer:I = 0x7f080d9a

.field public static final find_friends_intro_header:I = 0x7f080d98

.field public static final find_friends_intro_how_it_works_link:I = 0x7f080d9b

.field public static final find_friends_invite_action:I = 0x7f080daa

.field public static final find_friends_invite_all:I = 0x7f080dab

.field public static final find_friends_invite_all_are_you_sure_prompt:I = 0x7f080dad

.field public static final find_friends_invite_friends_button:I = 0x7f080db0

.field public static final find_friends_invite_friends_footer:I = 0x7f080db1

.field public static final find_friends_invite_friends_header:I = 0x7f080daf

.field public static final find_friends_invite_friends_title:I = 0x7f080dae

.field public static final find_friends_invite_sent:I = 0x7f080dac

.field public static final find_friends_kddi_show_you:I = 0x7f080d93

.field public static final find_friends_no_contacts:I = 0x7f080d8e

.field public static final find_friends_send_invitations:I = 0x7f080db2

.field public static final find_friends_show_you:I = 0x7f080d92

.field public static final find_friends_skip_dialog_message:I = 0x7f080db3

.field public static final find_friends_store_contacts:I = 0x7f080d94

.field public static final find_friends_two_reminders:I = 0x7f080d91

.field public static final find_friends_undo:I = 0x7f080d8d

.field public static final flash_posts_bullet_separated:I = 0x7f08060a

.field public static final flash_posts_combined_explanation_string:I = 0x7f08060c

.field public static final flash_posts_standalone_explanation_string:I = 0x7f08060b

.field public static final friend_finder_cancel_dialog_message:I = 0x7f080dc3

.field public static final friend_finder_cancel_dialog_negative:I = 0x7f080dc4

.field public static final friend_finder_cancel_dialog_positive:I = 0x7f080dc5

.field public static final friend_finder_cancel_dialog_title:I = 0x7f080dc2

.field public static final friend_finder_contacts_header:I = 0x7f080dbb

.field public static final friend_finder_contacts_importing:I = 0x7f080dbf

.field public static final friend_finder_few_friends_dialog_message:I = 0x7f080dcb

.field public static final friend_finder_few_friends_dialog_negative:I = 0x7f080dcc

.field public static final friend_finder_few_friends_dialog_positive:I = 0x7f080dcd

.field public static final friend_finder_few_friends_dialog_title:I = 0x7f080dca

.field public static final friend_finder_get_started:I = 0x7f080db6

.field public static final friend_finder_learn_more:I = 0x7f080db9

.field public static final friend_finder_learn_more_how_to:I = 0x7f080dba

.field public static final friend_finder_manage_imported_contacts:I = 0x7f080dce

.field public static final friend_finder_manage_or_delete:I = 0x7f080db8

.field public static final friend_finder_no_contacts:I = 0x7f080dc0

.field public static final friend_finder_no_friends_dialog_message:I = 0x7f080dc7

.field public static final friend_finder_no_friends_dialog_negative:I = 0x7f080dc8

.field public static final friend_finder_no_friends_dialog_positive:I = 0x7f080dc9

.field public static final friend_finder_no_friends_dialog_title:I = 0x7f080dc6

.field public static final friend_finder_nux_content:I = 0x7f080db5

.field public static final friend_finder_nux_legal_note:I = 0x7f080db7

.field public static final friend_finder_nux_title:I = 0x7f080db4

.field public static final friend_finder_pymk_header:I = 0x7f080dbe

.field public static final friend_finder_search_hint:I = 0x7f080dc1

.field public static final friend_finder_your_contacts_header:I = 0x7f080dbc

.field public static final friend_finder_your_phone_contacts_header:I = 0x7f080dbd

.field public static final friend_request_hidden:I = 0x7f08107d

.field public static final friend_requests_title:I = 0x7f080166

.field public static final friend_selector_list_empty:I = 0x7f0810aa

.field public static final friend_selector_search_hint:I = 0x7f0810a9

.field public static final friend_selector_section_friends:I = 0x7f0810ab

.field public static final friend_selector_section_suggestion:I = 0x7f080aca

.field public static final friending_codes_about_to_expire:I = 0x7f08109a

.field public static final friending_codes_empty_code:I = 0x7f08109d

.field public static final friending_codes_expiration_time:I = 0x7f081099

.field public static final friending_codes_header:I = 0x7f081094

.field public static final friending_codes_input:I = 0x7f081096

.field public static final friending_codes_internal_error:I = 0x7f08109c

.field public static final friending_codes_invalid_code:I = 0x7f08109e

.field public static final friending_codes_second_header:I = 0x7f081095

.field public static final friending_codes_share:I = 0x7f081097

.field public static final friending_codes_try_again:I = 0x7f08109b

.field public static final friending_codes_use_code:I = 0x7f081098

.field public static final friending_radar_close:I = 0x7f081091

.field public static final friending_radar_codes_nux_motivation:I = 0x7f081093

.field public static final friending_radar_codes_nux_title:I = 0x7f081092

.field public static final friending_radar_connection_error_header:I = 0x7f081088

.field public static final friending_radar_connection_error_subject:I = 0x7f081089

.field public static final friending_radar_entry_point:I = 0x7f081087

.field public static final friending_radar_entry_point_title:I = 0x7f081080

.field public static final friending_radar_finding_location:I = 0x7f081085

.field public static final friending_radar_location_error_header:I = 0x7f08108a

.field public static final friending_radar_location_error_subject:I = 0x7f08108b

.field public static final friending_radar_location_settings:I = 0x7f08108f

.field public static final friending_radar_no_one_found_error_header:I = 0x7f08108c

.field public static final friending_radar_no_one_found_error_subject:I = 0x7f08108d

.field public static final friending_radar_nux_how_to:I = 0x7f081083

.field public static final friending_radar_nux_motivation:I = 0x7f081081

.field public static final friending_radar_nux_title:I = 0x7f08107f

.field public static final friending_radar_search_again:I = 0x7f081090

.field public static final friending_radar_search_start:I = 0x7f081082

.field public static final friending_radar_searching_nearby:I = 0x7f081086

.field public static final friending_radar_start:I = 0x7f081084

.field public static final friending_radar_title_text:I = 0x7f08107e

.field public static final friending_radar_unknown_error_header:I = 0x7f08108e

.field public static final friendlist_friends:I = 0x7f0811e6

.field public static final friendlist_hint:I = 0x7f0811e8

.field public static final friendlist_mutual_friends:I = 0x7f0811e7

.field public static final friends:I = 0x7f080167

.field public static final friends_already_friends_error_message:I = 0x7f080045

.field public static final friends_cannot_add_more_error_message:I = 0x7f080046

.field public static final friends_cannot_add_user_error_message:I = 0x7f080044

.field public static final friends_cannot_add_user_now_error_message:I = 0x7f080049

.field public static final friends_cannot_update_error_message:I = 0x7f08004a

.field public static final friends_center_browse:I = 0x7f08105b

.field public static final friends_center_contacts:I = 0x7f08105a

.field public static final friends_center_friends:I = 0x7f08105c

.field public static final friends_center_no_friends_text:I = 0x7f08105d

.field public static final friends_center_requests:I = 0x7f081059

.field public static final friends_center_search:I = 0x7f081058

.field public static final friends_center_suggestions:I = 0x7f081057

.field public static final friends_nearby_empty_invite_button:I = 0x7f080ab6

.field public static final friends_nearby_empty_invite_subtitle:I = 0x7f080ab5

.field public static final friends_nearby_empty_invite_title:I = 0x7f080ab4

.field public static final friends_nearby_empty_list_text:I = 0x7f080aa2

.field public static final friends_nearby_error:I = 0x7f080aa4

.field public static final friends_nearby_error_subtitle:I = 0x7f080aa5

.field public static final friends_nearby_finding_location:I = 0x7f080aa3

.field public static final friends_nearby_invite_invited:I = 0x7f080ac4

.field public static final friends_nearby_invite_section_header:I = 0x7f080ac7

.field public static final friends_nearby_invite_send_hint:I = 0x7f080ac8

.field public static final friends_nearby_invite_send_invite:I = 0x7f080ac5

.field public static final friends_nearby_invite_send_success_content:I = 0x7f080ac9

.field public static final friends_nearby_invite_title:I = 0x7f080ac6

.field public static final friends_nearby_location_disabled:I = 0x7f080aa6

.field public static final friends_nearby_location_disabled_subtitle:I = 0x7f080aa7

.field public static final friends_nearby_location_error:I = 0x7f080aa9

.field public static final friends_nearby_location_settings:I = 0x7f080aa8

.field public static final friends_nearby_map_marker_label:I = 0x7f080ab7

.field public static final friends_nearby_open_in_maps_ping_action:I = 0x7f080ac2

.field public static final friends_nearby_open_in_uber_ping_action:I = 0x7f080ac3

.field public static final friends_nearby_ping_actions_dialog_header:I = 0x7f080ac1

.field public static final friends_nearby_search:I = 0x7f080aab

.field public static final friends_nearby_search_invite:I = 0x7f080aac

.field public static final friends_nearby_search_invite_error:I = 0x7f080ab1

.field public static final friends_nearby_search_invite_undo:I = 0x7f080ab0

.field public static final friends_nearby_search_invited:I = 0x7f080aaf

.field public static final friends_nearby_search_inviting:I = 0x7f080aad

.field public static final friends_nearby_search_no_results:I = 0x7f080ab3

.field public static final friends_nearby_search_uninvite_error:I = 0x7f080ab2

.field public static final friends_nearby_search_uninviting:I = 0x7f080aae

.field public static final friends_nearby_see_more:I = 0x7f080aaa

.field public static final friends_nearby_title:I = 0x7f080aa1

.field public static final friends_nearby_uber_dropoff_nickname:I = 0x7f080ac0

.field public static final friends_no_content:I = 0x7f0808a6

.field public static final friends_other_over_friend_limit_error_message:I = 0x7f080048

.field public static final friends_pending_request_error_message:I = 0x7f080043

.field public static final friends_privacy_description:I = 0x7f08083a

.field public static final friends_self_over_friend_limit_error_message:I = 0x7f080047

.field public static final friends_suggestions:I = 0x7f0806b1

.field public static final friends_tab:I = 0x7f080c7e

.field public static final gallery_select_button_text:I = 0x7f08079c

.field public static final gallery_view_photo_many:I = 0x7f0807a0

.field public static final gallery_view_photo_one:I = 0x7f08079f

.field public static final gallery_view_video_one:I = 0x7f08079e

.field public static final gdp_dialog_retry:I = 0x7f0810ec

.field public static final gdp_nux_public_permissions:I = 0x7f0810ed

.field public static final generic_action_fail:I = 0x7f080035

.field public static final generic_back:I = 0x7f080026

.field public static final generic_connection_lost:I = 0x7f080033

.field public static final generic_continue:I = 0x7f080029

.field public static final generic_done:I = 0x7f080030

.field public static final generic_error_message:I = 0x7f080031

.field public static final generic_install:I = 0x7f08002a

.field public static final generic_learn_more:I = 0x7f08002b

.field public static final generic_loading:I = 0x7f080023

.field public static final generic_next:I = 0x7f080028

.field public static final generic_off:I = 0x7f08002e

.field public static final generic_on:I = 0x7f08002d

.field public static final generic_previous:I = 0x7f080027

.field public static final generic_retry:I = 0x7f080038

.field public static final generic_sending:I = 0x7f080024

.field public static final generic_skip:I = 0x7f08002f

.field public static final generic_something_went_wrong:I = 0x7f080034

.field public static final generic_tap_to_retry:I = 0x7f080037

.field public static final generic_turn_on:I = 0x7f08002c

.field public static final generic_updating:I = 0x7f080025

.field public static final geo_permissions_allow:I = 0x7f080eae

.field public static final geo_permissions_deny:I = 0x7f080eaf

.field public static final geo_permissions_dialog:I = 0x7f080eb0

.field public static final geo_permissions_remember:I = 0x7f080eb1

.field public static final gift_card_mall_continue_button:I = 0x7f080d3a

.field public static final gift_card_mall_more_gift_cards_header:I = 0x7f080d3f

.field public static final gift_card_mall_more_information_option:I = 0x7f080d3b

.field public static final gift_card_mall_more_products_header_info:I = 0x7f080d3d

.field public static final gift_card_mall_more_products_header_see_all:I = 0x7f080d3c

.field public static final gift_card_mall_recommended_header:I = 0x7f080d40

.field public static final gift_card_mall_see_all_gifts_button:I = 0x7f080d3e

.field public static final global_notifications_disabled:I = 0x7f08025a

.field public static final global_notifications_disabled_msg:I = 0x7f08025f

.field public static final global_notifications_goto_settings:I = 0x7f080260

.field public static final global_notifications_muted_msg:I = 0x7f08025e

.field public static final global_notifications_muted_until:I = 0x7f08025b

.field public static final grab_bag_buy_button:I = 0x7f080d45

.field public static final grab_bag_header_info_no_recipient:I = 0x7f080d43

.field public static final grab_bag_header_info_with_recipient:I = 0x7f080d44

.field public static final grab_bag_header_no_recipient:I = 0x7f080d41

.field public static final grab_bag_header_with_recipient:I = 0x7f080d42

.field public static final graph_search_activity_log_dialog_button_text:I = 0x7f080e4f

.field public static final graph_search_activity_log_dialog_title:I = 0x7f080e4e

.field public static final graph_search_clear_query_text:I = 0x7f080e35

.field public static final graph_search_ellipsis_suggestions_title:I = 0x7f080e2e

.field public static final graph_search_filter:I = 0x7f080e29

.field public static final graph_search_filter_results:I = 0x7f080e2a

.field public static final graph_search_filter_value_example:I = 0x7f080e2d

.field public static final graph_search_filters_done:I = 0x7f080e2b

.field public static final graph_search_placeholder_text:I = 0x7f080e25

.field public static final graph_search_post_search_total_failure_loading:I = 0x7f080e32

.field public static final graph_search_recently_searched:I = 0x7f080e30

.field public static final graph_search_remove_filter_button_desc:I = 0x7f080e2c

.field public static final graph_search_result_title_verified:I = 0x7f080e33

.field public static final graph_search_results_fetch_failure:I = 0x7f080e26

.field public static final graph_search_results_load_more:I = 0x7f080e27

.field public static final graph_search_results_not_found:I = 0x7f080e28

.field public static final graph_search_see_more:I = 0x7f080e34

.field public static final graph_search_see_more_all_empty:I = 0x7f080e36

.field public static final graph_search_see_more_events_empty:I = 0x7f080e38

.field public static final graph_search_see_more_groups_empty:I = 0x7f080e39

.field public static final graph_search_see_more_pages_empty:I = 0x7f080e3a

.field public static final graph_search_see_more_people_empty:I = 0x7f080e37

.field public static final graph_search_trending:I = 0x7f080e2f

.field public static final graph_search_typeahead_error:I = 0x7f080e31

.field public static final green_check_desc:I = 0x7f0808fa

.field public static final grey_alert_desc:I = 0x7f0808f8

.field public static final grey_circle_desc:I = 0x7f0808f6

.field public static final group_closed_visibility_text:I = 0x7f0811c5

.field public static final group_members_no_match:I = 0x7f0808a8

.field public static final group_members_type_name:I = 0x7f0808a7

.field public static final group_open_visibility_text:I = 0x7f0811c6

.field public static final group_open_visibility_text_community:I = 0x7f0811c7

.field public static final group_presence_active_now:I = 0x7f08037a

.field public static final group_presence_active_now_short:I = 0x7f08037b

.field public static final group_presence_active_one_day_ago:I = 0x7f080380

.field public static final group_presence_active_one_hour_ago:I = 0x7f08037e

.field public static final group_presence_active_one_min_ago:I = 0x7f08037c

.field public static final group_presence_active_x_days_ago:I = 0x7f080381

.field public static final group_presence_active_x_hours_ago:I = 0x7f08037f

.field public static final group_presence_active_x_mins_ago:I = 0x7f08037d

.field public static final group_secret_visibility_text:I = 0x7f0811c8

.field public static final group_selector_title:I = 0x7f080c78

.field public static final groups_cancel_join_request:I = 0x7f0811cb

.field public static final groups_edit_cover_photo:I = 0x7f0803f7

.field public static final groups_feed_action_create_shortcut:I = 0x7f0811d1

.field public static final groups_feed_action_info:I = 0x7f0811ce

.field public static final groups_feed_action_share:I = 0x7f0811cf

.field public static final groups_feed_approve_failed_message:I = 0x7f0811e3

.field public static final groups_feed_approve_post_button:I = 0x7f0811e1

.field public static final groups_feed_approve_success_message:I = 0x7f0811e4

.field public static final groups_feed_delete_success_message:I = 0x7f0811e5

.field public static final groups_feed_loading_failed:I = 0x7f0811de

.field public static final groups_feed_no_pending_posts:I = 0x7f0811dd

.field public static final groups_feed_no_pinned_posts:I = 0x7f0811dc

.field public static final groups_feed_notification_copy_link:I = 0x7f0811d0

.field public static final groups_feed_notification_create_shortcut:I = 0x7f0811d2

.field public static final groups_feed_pending_loading_failed:I = 0x7f0811df

.field public static final groups_feed_pin_failed:I = 0x7f0811d5

.field public static final groups_feed_pin_failed_too_many:I = 0x7f0811d7

.field public static final groups_feed_pin_post:I = 0x7f0811d3

.field public static final groups_feed_pinned_loading_failed:I = 0x7f0811e0

.field public static final groups_feed_publisher_photo:I = 0x7f0811cd

.field public static final groups_feed_publisher_post:I = 0x7f0811cc

.field public static final groups_feed_remove_post_button:I = 0x7f0811e2

.field public static final groups_feed_unpin_failed:I = 0x7f0811d6

.field public static final groups_feed_unpin_post:I = 0x7f0811d4

.field public static final groups_feed_view_pinned_post_bar_text:I = 0x7f0811c4

.field public static final groups_get_error:I = 0x7f080c76

.field public static final groups_gysj_requested:I = 0x7f0811c9

.field public static final groups_join_group:I = 0x7f0811ca

.field public static final groups_no_content:I = 0x7f080c77

.field public static final groups_pending_posts:I = 0x7f0811db

.field public static final groups_post_approval_ok_button:I = 0x7f0811d8

.field public static final groups_post_waiting_approval_message:I = 0x7f0811d9

.field public static final groups_post_waiting_approval_title:I = 0x7f0811da

.field public static final groups_section_header:I = 0x7f080274

.field public static final groups_tab:I = 0x7f080c80

.field public static final header_action_filter:I = 0x7f080c9b

.field public static final header_action_next:I = 0x7f080c9c

.field public static final header_action_skip:I = 0x7f080c9d

.field public static final help_center:I = 0x7f0809d2

.field public static final hide:I = 0x7f0809bd

.field public static final hide_vs_delete:I = 0x7f080af9

.field public static final home_app_reenable_toast:I = 0x7f08013f

.field public static final home_chat:I = 0x7f080eb2

.field public static final home_logout:I = 0x7f080eb3

.field public static final home_settings:I = 0x7f080eb4

.field public static final homeintent_nux_clue_text_ics_and_before:I = 0x7f080094

.field public static final homeintent_nux_clue_text_jb_and_above:I = 0x7f080095

.field public static final homeintent_nux_dialog_body:I = 0x7f080097

.field public static final homeintent_nux_dialog_button:I = 0x7f080098

.field public static final homeintent_nux_dialog_header:I = 0x7f080096

.field public static final homesettings_add_cover_feed_shortcut_row_detail:I = 0x7f080a5f

.field public static final homesettings_add_cover_feed_shortcut_row_label:I = 0x7f080a5e

.field public static final homesettings_app_info_group_title:I = 0x7f080a67

.field public static final homesettings_cancel_lockscreen_disable_button_label:I = 0x7f080a69

.field public static final homesettings_cancel_lockscreen_disable_survey_button_label:I = 0x7f080a6e

.field public static final homesettings_change_wallpaper_row_detail:I = 0x7f080a5d

.field public static final homesettings_change_wallpaper_row_label:I = 0x7f080a5c

.field public static final homesettings_confirm_lockscreen_disable_button_label:I = 0x7f080a6a

.field public static final homesettings_confirm_lockscreen_disable_survey_button_label:I = 0x7f080a6f

.field public static final homesettings_connect_apps_row_detail:I = 0x7f080a5a

.field public static final homesettings_connect_apps_row_label:I = 0x7f080a59

.field public static final homesettings_general_settings_group_title:I = 0x7f080a66

.field public static final homesettings_internal_settings_group_title:I = 0x7f080a68

.field public static final homesettings_lockscreen_disable_dialog_text:I = 0x7f080a6b

.field public static final homesettings_lockscreen_disable_survey_dialog_text:I = 0x7f080a70

.field public static final homesettings_lockscreen_group_title:I = 0x7f080a62

.field public static final homesettings_more_settings_row_detail:I = 0x7f080a61

.field public static final homesettings_more_settings_row_label:I = 0x7f080a60

.field public static final homesettings_reenable_lock_screen_for_ongoing_notification_toast:I = 0x7f080a6d

.field public static final homesettings_reenable_lock_screen_for_wallpaper_toast:I = 0x7f080a6c

.field public static final homesettings_root_title:I = 0x7f080a58

.field public static final homesettings_system_notifications_label:I = 0x7f080a63

.field public static final homesettings_system_notifications_summary_off:I = 0x7f080a65

.field public static final homesettings_system_notifications_summary_on:I = 0x7f080a64

.field public static final homesettings_wallpaper_title:I = 0x7f080a5b

.field public static final homesetup_continue:I = 0x7f080a45

.field public static final homesetup_description_text:I = 0x7f080a2d

.field public static final homesetup_description_text_appfeeds_enabled:I = 0x7f080a2e

.field public static final homesetup_done:I = 0x7f080a46

.field public static final homesetup_enable_launcher_button_label:I = 0x7f080a51

.field public static final homesetup_launcher_off_toast:I = 0x7f080a3c

.field public static final homesetup_launcher_on_toast:I = 0x7f080a3b

.field public static final homesetup_launcher_option_label:I = 0x7f080a48

.field public static final homesetup_lockscreen_launcher_description_format:I = 0x7f080a71

.field public static final homesetup_lockscreen_launcher_step_launcher_description:I = 0x7f080a41

.field public static final homesetup_lockscreen_launcher_step_launcher_header:I = 0x7f080a3f

.field public static final homesetup_lockscreen_launcher_step_lockscreen_description:I = 0x7f080a40

.field public static final homesetup_lockscreen_launcher_step_lockscreen_header:I = 0x7f080a3e

.field public static final homesetup_lockscreen_launcher_step_lockscreen_only_description:I = 0x7f080a42

.field public static final homesetup_lockscreen_launcher_step_title_lockscreen_only:I = 0x7f080a3d

.field public static final homesetup_lockscreen_option_label:I = 0x7f080a47

.field public static final homesetup_not_now:I = 0x7f080a44

.field public static final homesetup_setup_app_feeds_title:I = 0x7f080a54

.field public static final homesetup_setup_cover_feed_shortcut_title:I = 0x7f080a55

.field public static final homesetup_setup_wallpaper_button_label:I = 0x7f080a53

.field public static final homesetup_setup_wallpaper_title:I = 0x7f080a52

.field public static final homesetup_step_1_of_2:I = 0x7f080a4f

.field public static final homesetup_step_1_of_3:I = 0x7f080a4c

.field public static final homesetup_step_2_of_2:I = 0x7f080a50

.field public static final homesetup_step_2_of_3:I = 0x7f080a4d

.field public static final homesetup_step_3_of_3:I = 0x7f080a4e

.field public static final homesetup_title_text_active:I = 0x7f080a2b

.field public static final homesetup_title_text_inactive:I = 0x7f080a2c

.field public static final homesetup_title_text_nux:I = 0x7f080a2a

.field public static final homesetup_turn_on_lockscreen_only_lockscreen_available:I = 0x7f080a43

.field public static final homesetup_welcome_continue_button_label:I = 0x7f080a4b

.field public static final homesetup_welcome_description:I = 0x7f080a4a

.field public static final homesetup_welcome_title:I = 0x7f080a49

.field public static final identity_dialog_skip:I = 0x7f080956

.field public static final ignore_request:I = 0x7f08016b

.field public static final image_attachment_failed_attach_type:I = 0x7f080295

.field public static final image_browser_title:I = 0x7f080ead

.field public static final image_crop_discard_text:I = 0x7f080342

.field public static final image_crop_multiface_crop_help:I = 0x7f080345

.field public static final image_crop_running_face_detection:I = 0x7f080340

.field public static final image_crop_save_text:I = 0x7f080341

.field public static final image_crop_saving_image:I = 0x7f080344

.field public static final image_crop_wallpaper:I = 0x7f080343

.field public static final image_search_button:I = 0x7f080354

.field public static final image_search_failed_to_load_images:I = 0x7f080351

.field public static final image_search_hint:I = 0x7f080353

.field public static final image_search_loading:I = 0x7f080350

.field public static final image_search_no_results:I = 0x7f080352

.field public static final image_search_range:I = 0x7f08034d

.field public static final image_search_range_next:I = 0x7f08034f

.field public static final image_search_range_previous:I = 0x7f08034e

.field public static final image_search_title:I = 0x7f08034c

.field public static final incall_answer_call:I = 0x7f080436

.field public static final incall_decline_call:I = 0x7f080437

.field public static final incoming_transactions_title:I = 0x7f0801cb

.field public static final info_button_description:I = 0x7f0801df

.field public static final inline_rating_title:I = 0x7f0807c5

.field public static final inline_rating_title_null_state:I = 0x7f0807c6

.field public static final inline_video_pivots_comments_count_k:I = 0x7f080563

.field public static final inline_video_pivots_comments_count_multiple:I = 0x7f080562

.field public static final inline_video_pivots_comments_count_one:I = 0x7f080561

.field public static final inline_video_pivots_likes_count_k:I = 0x7f080560

.field public static final inline_video_pivots_likes_count_multiple:I = 0x7f08055f

.field public static final inline_video_pivots_likes_count_one:I = 0x7f08055e

.field public static final inline_video_player_status:I = 0x7f0803fe

.field public static final inlineactionbar_accessibility_more:I = 0x7f080acb

.field public static final input_code:I = 0x7f080f8f

.field public static final input_email:I = 0x7f080f90

.field public static final input_new_email:I = 0x7f080f92

.field public static final input_new_phone:I = 0x7f080f93

.field public static final input_phone:I = 0x7f080f91

.field public static final input_phone_number:I = 0x7f080dd1

.field public static final install_new_build_app:I = 0x7f080c86

.field public static final install_new_build_confirm:I = 0x7f080c8b

.field public static final install_new_build_notes_default_text:I = 0x7f080c8a

.field public static final install_new_build_notes_text:I = 0x7f080c89

.field public static final install_new_build_notes_title:I = 0x7f080c88

.field public static final install_new_build_remind:I = 0x7f080c8c

.field public static final install_new_build_text:I = 0x7f080c85

.field public static final install_new_build_title:I = 0x7f080c84

.field public static final install_new_build_version:I = 0x7f080c87

.field public static final instructions_email:I = 0x7f080f89

.field public static final instructions_phone:I = 0x7f080f8a

.field public static final internal_pref_caches_qe_desc:I = 0x7f080518

.field public static final internal_pref_caches_qp_desc:I = 0x7f080519

.field public static final internal_pref_caches_sandbox_desc:I = 0x7f08051b

.field public static final internal_pref_caches_screen_desc:I = 0x7f08051a

.field public static final internal_pref_caches_screen_title:I = 0x7f08050e

.field public static final internal_pref_category_chat_heads:I = 0x7f080520

.field public static final internal_pref_category_cpu:I = 0x7f080523

.field public static final internal_pref_category_logging:I = 0x7f08051d

.field public static final internal_pref_category_misc:I = 0x7f08051f

.field public static final internal_pref_category_request_memory:I = 0x7f080525

.field public static final internal_pref_category_request_profiling:I = 0x7f080524

.field public static final internal_pref_category_rolodex:I = 0x7f080521

.field public static final internal_pref_category_subsystems:I = 0x7f08051e

.field public static final internal_pref_category_voip:I = 0x7f080522

.field public static final internal_pref_category_zero_rating:I = 0x7f0804a5

.field public static final internal_pref_debug_overlay:I = 0x7f080530

.field public static final internal_pref_features_screen_desc:I = 0x7f080517

.field public static final internal_pref_features_screen_title:I = 0x7f080513

.field public static final internal_pref_performance_screen_title:I = 0x7f08050f

.field public static final internal_pref_push_notification_desc:I = 0x7f08051c

.field public static final internal_pref_push_notification_title:I = 0x7f080516

.field public static final internal_pref_qe_screen_title:I = 0x7f080510

.field public static final internal_pref_qp_screen_title:I = 0x7f080511

.field public static final internal_pref_root_screen_title:I = 0x7f08050d

.field public static final internal_pref_sandbox_screen_title:I = 0x7f080512

.field public static final internal_pref_version:I = 0x7f080526

.field public static final internal_pref_voip_screen_title:I = 0x7f080515

.field public static final internal_pref_webrtc_screen_title:I = 0x7f080514

.field public static final internal_settings:I = 0x7f080527

.field public static final invite_banner_content:I = 0x7f08022c

.field public static final invite_banner_title:I = 0x7f08022b

.field public static final invite_button_url:I = 0x7f080435

.field public static final invite_dialog_cancel_button:I = 0x7f080230

.field public static final invite_dialog_invite_button:I = 0x7f08022f

.field public static final invite_dialog_message:I = 0x7f08022e

.field public static final invite_dialog_title:I = 0x7f08022d

.field public static final invite_failed:I = 0x7f080232

.field public static final invite_friends_button:I = 0x7f080228

.field public static final invite_friends_neue:I = 0x7f080229

.field public static final invite_recipient_button:I = 0x7f08022a

.field public static final invite_successful:I = 0x7f080231

.field public static final iorg_about_legal_text:I = 0x7f0810c2

.field public static final iorg_app_install:I = 0x7f0810ba

.field public static final iorg_cancel_button:I = 0x7f0810be

.field public static final iorg_close_button:I = 0x7f0810bd

.field public static final iorg_continue_button:I = 0x7f0810bf

.field public static final iorg_dialog_download_app_content:I = 0x7f0810bc

.field public static final iorg_dialog_external_url_content:I = 0x7f0810bb

.field public static final iorg_fb4a_about_text:I = 0x7f0810d5

.field public static final iorg_fb4a_about_title:I = 0x7f0810d4

.field public static final iorg_fb4a_dialog_close:I = 0x7f0810d2

.field public static final iorg_fb4a_dialog_not_now:I = 0x7f0810d1

.field public static final iorg_fb4a_menu_icon_desc:I = 0x7f0810ce

.field public static final iorg_fb4a_not_eligible_title:I = 0x7f0810d3

.field public static final iorg_fb4a_nux_legal:I = 0x7f0810d0

.field public static final iorg_fb4a_nux_title:I = 0x7f0810cf

.field public static final iorg_fb4a_title:I = 0x7f0810cd

.field public static final iorg_free_services:I = 0x7f0810ae

.field public static final iorg_help_center:I = 0x7f0810c8

.field public static final iorg_legal_cookie_use:I = 0x7f0810c5

.field public static final iorg_legal_data_use_policy:I = 0x7f0810c4

.field public static final iorg_legal_facebook_terms:I = 0x7f0810c3

.field public static final iorg_legal_learn_more:I = 0x7f0810c6

.field public static final iorg_menu_about_button:I = 0x7f0810b7

.field public static final iorg_menu_back_button:I = 0x7f0810af

.field public static final iorg_menu_bookmark_set_button:I = 0x7f0810b1

.field public static final iorg_menu_bookmark_unset_button:I = 0x7f0810b2

.field public static final iorg_menu_bookmarks_button:I = 0x7f0810b6

.field public static final iorg_menu_forward_button:I = 0x7f0810b0

.field public static final iorg_menu_history_button:I = 0x7f0810b5

.field public static final iorg_menu_refresh_button:I = 0x7f0810b3

.field public static final iorg_menu_share_button:I = 0x7f0810b4

.field public static final iorg_no_internet:I = 0x7f0810ca

.field public static final iorg_not_eligible_content:I = 0x7f0810c9

.field public static final iorg_nux_legal_text:I = 0x7f0810c1

.field public static final iorg_nux_main_text:I = 0x7f0810c0

.field public static final iorg_questions:I = 0x7f0810c7

.field public static final iorg_share_url_subject:I = 0x7f0810b9

.field public static final iorg_share_url_title:I = 0x7f0810b8

.field public static final iorg_something_went_wrong:I = 0x7f0810cb

.field public static final iorg_tap_to_retry:I = 0x7f0810cc

.field public static final itunes_recommended_details_section_header:I = 0x7f080cf8

.field public static final itunes_recommended_header_albums:I = 0x7f080cf1

.field public static final itunes_recommended_header_ebooks:I = 0x7f080cf6

.field public static final itunes_recommended_header_ios_apps:I = 0x7f080cf4

.field public static final itunes_recommended_header_mac_apps:I = 0x7f080cf5

.field public static final itunes_recommended_header_movies:I = 0x7f080cf2

.field public static final itunes_recommended_header_songs:I = 0x7f080cf7

.field public static final itunes_recommended_header_tv:I = 0x7f080cf3

.field public static final itunes_recommended_no_results_skip:I = 0x7f080d01

.field public static final itunes_recommended_no_search_results:I = 0x7f080d00

.field public static final itunes_recommended_search_hint_albums:I = 0x7f080cf9

.field public static final itunes_recommended_search_hint_ebooks:I = 0x7f080cfd

.field public static final itunes_recommended_search_hint_ios_apps:I = 0x7f080cfc

.field public static final itunes_recommended_search_hint_mac_apps:I = 0x7f080cff

.field public static final itunes_recommended_search_hint_movies:I = 0x7f080cfa

.field public static final itunes_recommended_search_hint_songs:I = 0x7f080cfe

.field public static final itunes_recommended_search_hint_tv:I = 0x7f080cfb

.field public static final itunes_sku_selector_amount_header:I = 0x7f080ce7

.field public static final itunes_sku_selector_recommend_list_header:I = 0x7f080ce8

.field public static final itunes_sku_selector_recommend_list_item_albums:I = 0x7f080ce9

.field public static final itunes_sku_selector_recommend_list_item_ebooks:I = 0x7f080ced

.field public static final itunes_sku_selector_recommend_list_item_ios_apps:I = 0x7f080cec

.field public static final itunes_sku_selector_recommend_list_item_mac_apps:I = 0x7f080cef

.field public static final itunes_sku_selector_recommend_list_item_movies:I = 0x7f080cea

.field public static final itunes_sku_selector_recommend_list_item_skip:I = 0x7f080cf0

.field public static final itunes_sku_selector_recommend_list_item_songs:I = 0x7f080cee

.field public static final itunes_sku_selector_recommend_list_item_tv_shows:I = 0x7f080ceb

.field public static final jewel_badge_count_more:I = 0x7f080f2e

.field public static final jewel_divebar_upsell_1_friend:I = 0x7f080f5e

.field public static final jewel_divebar_upsell_2_friend:I = 0x7f080f5f

.field public static final jewel_divebar_upsell_3_friend:I = 0x7f080f60

.field public static final jewel_divebar_upsell_title:I = 0x7f080f5d

.field public static final keyword_search_wikipedia_module_title:I = 0x7f080e50

.field public static final languages:I = 0x7f08113b

.field public static final launch_camera_failed:I = 0x7f08071b

.field public static final launch_video_failed:I = 0x7f08071c

.field public static final learn_more:I = 0x7f080108

.field public static final leave_group_action:I = 0x7f0802ea

.field public static final like_action_sending:I = 0x7f08042d

.field public static final like_action_sent:I = 0x7f08042e

.field public static final like_button_description:I = 0x7f0801d2

.field public static final link:I = 0x7f0807de

.field public static final load_review_failed:I = 0x7f0807c7

.field public static final load_review_progress_dialog_message:I = 0x7f0807c4

.field public static final loading:I = 0x7f080555

.field public static final loading_indicator_text:I = 0x7f0807a1

.field public static final loading_media:I = 0x7f080417

.field public static final loading_text:I = 0x7f080c9e

.field public static final loading_timeline_message:I = 0x7f080224

.field public static final location_ping_dialog_current_time_label:I = 0x7f080ab8

.field public static final location_ping_dialog_delete:I = 0x7f080abb

.field public static final location_ping_dialog_message_hint:I = 0x7f080abc

.field public static final location_ping_dialog_message_length:I = 0x7f080abd

.field public static final location_ping_dialog_new_time_label:I = 0x7f080ab9

.field public static final location_ping_dialog_share:I = 0x7f080abe

.field public static final location_ping_dialog_time_forever:I = 0x7f080aba

.field public static final location_ping_dialog_update:I = 0x7f080abf

.field public static final location_settings_device:I = 0x7f0810a3

.field public static final location_settings_fb:I = 0x7f0810a0

.field public static final location_settings_google_play_title:I = 0x7f0810a6

.field public static final location_settings_history_desc:I = 0x7f0810a2

.field public static final location_settings_history_title:I = 0x7f0810a1

.field public static final location_settings_location_services_off_desc:I = 0x7f0810a7

.field public static final location_settings_location_services_off_manage:I = 0x7f0810a8

.field public static final location_settings_location_services_title:I = 0x7f0810a4

.field public static final location_settings_title:I = 0x7f08109f

.field public static final location_settings_wifi_title:I = 0x7f0810a5

.field public static final location_sources_turn_on_gps_message:I = 0x7f080c55

.field public static final location_sources_turn_on_gps_title:I = 0x7f080c54

.field public static final location_sources_turn_on_message:I = 0x7f080c51

.field public static final location_sources_turn_on_network_message:I = 0x7f080c53

.field public static final location_sources_turn_on_network_title:I = 0x7f080c52

.field public static final location_sources_turn_on_title:I = 0x7f080c50

.field public static final lockscreen_fb4a_short_product_name:I = 0x7f0808f4

.field public static final lockscreen_info_master_switch_caption:I = 0x7f0808e8

.field public static final lockscreen_info_text:I = 0x7f0808e7

.field public static final lockscreen_info_title:I = 0x7f0808e6

.field public static final lockscreen_inform_title_line:I = 0x7f0808de

.field public static final lockscreen_keep_on:I = 0x7f0808f0

.field public static final lockscreen_messenger_short_product_name:I = 0x7f0808f5

.field public static final lockscreen_notifications_banner_subtitle:I = 0x7f0808e5

.field public static final lockscreen_notifications_banner_title:I = 0x7f0808e4

.field public static final lockscreen_notifications_dismiss_notifications_text:I = 0x7f0808f3

.field public static final lockscreen_notifications_dismiss_text:I = 0x7f0808f2

.field public static final lockscreen_notifications_from_facebook:I = 0x7f0808e9

.field public static final lockscreen_notifications_from_messenger:I = 0x7f0808ea

.field public static final lockscreen_notifications_light_up_screen:I = 0x7f0808eb

.field public static final lockscreen_notifications_light_up_screen_subtitle:I = 0x7f0808ec

.field public static final lockscreen_notifications_new_notification:I = 0x7f0808e2

.field public static final lockscreen_notifications_new_notification_from:I = 0x7f0808df

.field public static final lockscreen_notifications_new_notifications:I = 0x7f0808e3

.field public static final lockscreen_notifications_new_notifications_from:I = 0x7f0808e0

.field public static final lockscreen_notifications_new_notifications_from_same_actor:I = 0x7f0808e1

.field public static final lockscreen_notifications_opt_in_not_now:I = 0x7f0808dd

.field public static final lockscreen_notifications_opt_in_title:I = 0x7f0808dc

.field public static final lockscreen_see_notifications:I = 0x7f0808f1

.field public static final lockscreen_turn_off:I = 0x7f0808ef

.field public static final lockscreen_turn_off_text:I = 0x7f0808ee

.field public static final lockscreen_turn_off_title:I = 0x7f0808ed

.field public static final log_out:I = 0x7f080f86

.field public static final logging_out_progress:I = 0x7f0801ee

.field public static final login_about:I = 0x7f0800e9

.field public static final login_account_exists:I = 0x7f080eb5

.field public static final login_another_logged_in_message:I = 0x7f080eb7

.field public static final login_another_logged_in_title:I = 0x7f080eb6

.field public static final login_approval_code_hint:I = 0x7f080075

.field public static final login_approval_instructions:I = 0x7f080142

.field public static final login_approval_instructions_1:I = 0x7f080073

.field public static final login_approval_instructions_2:I = 0x7f080074

.field public static final login_approval_login_button:I = 0x7f080076

.field public static final login_approvals_code:I = 0x7f080ed2

.field public static final login_approvals_continue:I = 0x7f080ed3

.field public static final login_approvals_prompt:I = 0x7f080ed4

.field public static final login_email_or_phone_title:I = 0x7f080eb8

.field public static final login_error_approvals_message:I = 0x7f080ed6

.field public static final login_error_approvals_title:I = 0x7f080ed5

.field public static final login_error_checkpoint_message:I = 0x7f080ec1

.field public static final login_error_checkpoint_title:I = 0x7f080ec0

.field public static final login_error_email_message:I = 0x7f080eba

.field public static final login_error_email_title:I = 0x7f080eb9

.field public static final login_error_generic:I = 0x7f080ec9

.field public static final login_error_network_error_message:I = 0x7f080ec8

.field public static final login_error_password_message:I = 0x7f080ebc

.field public static final login_error_password_reset_button:I = 0x7f080ebe

.field public static final login_error_password_reset_not_now_button:I = 0x7f080ebf

.field public static final login_error_password_suggest_reset_message:I = 0x7f080ebd

.field public static final login_error_password_title:I = 0x7f080ebb

.field public static final login_error_service_message:I = 0x7f080ec5

.field public static final login_error_service_title:I = 0x7f080ec4

.field public static final login_error_ticker:I = 0x7f080eca

.field public static final login_error_unconfirmed_message:I = 0x7f080ec3

.field public static final login_error_unconfirmed_title:I = 0x7f080ec2

.field public static final login_error_unexpected_message:I = 0x7f080ec7

.field public static final login_error_unexpected_title:I = 0x7f080ec6

.field public static final login_failed_message:I = 0x7f08006f

.field public static final login_forgot_password:I = 0x7f080ed0

.field public static final login_help_center_button_description:I = 0x7f080ed7

.field public static final login_password:I = 0x7f080ecb

.field public static final login_password_empty_prompt:I = 0x7f080ecc

.field public static final login_screen_login_button:I = 0x7f08007b

.field public static final login_screen_login_progress:I = 0x7f080070

.field public static final login_screen_password_hint:I = 0x7f08007a

.field public static final login_screen_signup_button:I = 0x7f08007c

.field public static final login_screen_user_hint:I = 0x7f080079

.field public static final login_signin:I = 0x7f080ecd

.field public static final login_signing_in:I = 0x7f080ece

.field public static final login_signup_fb:I = 0x7f080ecf

.field public static final login_terms:I = 0x7f0809dd

.field public static final login_username_empty_prompt:I = 0x7f080ed1

.field public static final logout_dialog_message:I = 0x7f080018

.field public static final logout_dialog_title:I = 0x7f080017

.field public static final long_press_nux_text_press:I = 0x7f0808ab

.field public static final long_press_nux_text_try:I = 0x7f0808ac

.field public static final low_data_mode_active:I = 0x7f08019d

.field public static final low_data_mode_daily_limit_summary:I = 0x7f0801a0

.field public static final low_data_mode_daily_limit_title:I = 0x7f08019f

.field public static final low_data_mode_inactive:I = 0x7f08019e

.field public static final low_data_mode_indicator:I = 0x7f0801a1

.field public static final low_data_mode_indicator_with_bytes:I = 0x7f0801a2

.field public static final low_data_mode_settings:I = 0x7f08019b

.field public static final low_data_mode_title:I = 0x7f08019c

.field public static final low_disk_space_warning_message:I = 0x7f08060e

.field public static final mailbox_sending:I = 0x7f080edb

.field public static final map_dialog_no_google_maps:I = 0x7f080226

.field public static final map_dialog_no_locations:I = 0x7f080227

.field public static final maps_show_map_button:I = 0x7f08018c

.field public static final max_new_pack_count_text:I = 0x7f0803f2

.field public static final max_photos_selected_toast:I = 0x7f08079b

.field public static final me_tab_confirm_phone:I = 0x7f0803e5

.field public static final me_tab_contacts_learn_more:I = 0x7f0803e9

.field public static final me_tab_contacts_title:I = 0x7f0803e8

.field public static final me_tab_location_summary:I = 0x7f0803e6

.field public static final me_tab_save_photos_summary:I = 0x7f0803e7

.field public static final media_attachment_failed:I = 0x7f080294

.field public static final media_capture_photo:I = 0x7f080713

.field public static final media_capture_video:I = 0x7f080714

.field public static final media_file_missing:I = 0x7f08078f

.field public static final media_photo_from_gallery:I = 0x7f080715

.field public static final media_pick_photo_title:I = 0x7f080779

.field public static final media_switch_camera:I = 0x7f080717

.field public static final media_tray_edit_button:I = 0x7f08041b

.field public static final media_type_conflict_toast:I = 0x7f08079a

.field public static final media_video_from_gallery:I = 0x7f080716

.field public static final meme_activity_title:I = 0x7f081052

.field public static final meme_bottom_text:I = 0x7f081051

.field public static final meme_send_button:I = 0x7f08104f

.field public static final meme_top_text:I = 0x7f081050

.field public static final menu_about:I = 0x7f0802dc

.field public static final menu_archive:I = 0x7f0802df

.field public static final menu_delete:I = 0x7f0802e2

.field public static final menu_group_settings:I = 0x7f0802ef

.field public static final menu_help:I = 0x7f0802dd

.field public static final menu_mark_as_spam:I = 0x7f0802e0

.field public static final menu_mark_as_spam_confirm:I = 0x7f0802e1

.field public static final menu_more_options:I = 0x7f0801ea

.field public static final menu_mute:I = 0x7f0802e6

.field public static final menu_open_full_view:I = 0x7f0803bf

.field public static final menu_open_full_view_messenger:I = 0x7f0803be

.field public static final menu_preferences:I = 0x7f0802db

.field public static final menu_refresh:I = 0x7f0802da

.field public static final menu_switch_accounts:I = 0x7f0802de

.field public static final menu_unmute:I = 0x7f0802e7

.field public static final message_attachment_text:I = 0x7f0809f0

.field public static final message_context_menu_copy_message:I = 0x7f08020d

.field public static final message_context_menu_delete_message:I = 0x7f08020f

.field public static final message_context_menu_forward_message:I = 0x7f08020e

.field public static final message_context_menu_message_details:I = 0x7f080210

.field public static final message_context_menu_save_image:I = 0x7f0803cd

.field public static final message_context_menu_save_video:I = 0x7f0803d0

.field public static final message_context_menu_share_image:I = 0x7f080211

.field public static final message_context_menu_title:I = 0x7f08020c

.field public static final message_context_menu_view_fullscreen:I = 0x7f080213

.field public static final message_context_menu_view_sticker_pack:I = 0x7f080212

.field public static final message_date_no_year:I = 0x7f08039c

.field public static final message_date_time_order:I = 0x7f08039e

.field public static final message_date_with_year:I = 0x7f08039d

.field public static final message_delivered_receipt:I = 0x7f080369

.field public static final message_seen_receipt:I = 0x7f08036a

.field public static final message_seen_receipt_group_everyone:I = 0x7f08036c

.field public static final message_seen_receipt_group_header:I = 0x7f08036b

.field public static final message_seen_receipt_group_multiple:I = 0x7f08036e

.field public static final message_seen_receipt_group_multiple_more:I = 0x7f08036f

.field public static final message_seen_receipt_group_single:I = 0x7f08036d

.field public static final message_sent_receipt:I = 0x7f0803d3

.field public static final message_sent_x:I = 0x7f080370

.field public static final message_sent_x_y:I = 0x7f080371

.field public static final messenger_about_copyright:I = 0x7f0803c8

.field public static final messenger_about_licenses:I = 0x7f0803ca

.field public static final messenger_about_privacy_and_terms:I = 0x7f0803cb

.field public static final messenger_about_terms_of_service:I = 0x7f0803cc

.field public static final messenger_about_title:I = 0x7f0803c7

.field public static final messenger_about_trademarks:I = 0x7f0803c9

.field public static final messenger_composer_explicit_share_location:I = 0x7f0803fb

.field public static final messenger_explicit_location_sharing_my_loc_button:I = 0x7f0803fa

.field public static final messenger_image_already_saved:I = 0x7f0803cf

.field public static final messenger_image_saved:I = 0x7f0803ce

.field public static final messenger_image_saved_title:I = 0x7f0803d6

.field public static final messenger_jewel_promotion_button:I = 0x7f080f0d

.field public static final messenger_jewel_promotion_message:I = 0x7f080f0c

.field public static final messenger_jewel_promotion_title:I = 0x7f080f0b

.field public static final messenger_location_services:I = 0x7f080f08

.field public static final messenger_photo_auto_download_upsell:I = 0x7f0803d7

.field public static final messenger_short_product_name:I = 0x7f08049a

.field public static final messenger_video_already_saved:I = 0x7f0803d2

.field public static final messenger_video_saved:I = 0x7f0803d1

.field public static final middle_of_string_selected:I = 0x7f080632

.field public static final minutiae_attachment_remove_button_label:I = 0x7f08087b

.field public static final minutiae_attachment_remove_button_label_default:I = 0x7f08087c

.field public static final minutiae_composer_search_hint:I = 0x7f0807ea

.field public static final minutiae_composer_title_text:I = 0x7f0807e9

.field public static final minutiae_error_msg:I = 0x7f0807ef

.field public static final minutiae_nux_text:I = 0x7f0807f0

.field public static final minutiae_place_picker_loading_text:I = 0x7f080880

.field public static final minutiae_place_picker_location_error:I = 0x7f08087e

.field public static final minutiae_place_picker_location_settings_button_text:I = 0x7f080881

.field public static final minutiae_place_picker_location_settings_text:I = 0x7f08087f

.field public static final minutiae_place_picker_network_error:I = 0x7f08087d

.field public static final minutiae_place_picker_search_text:I = 0x7f080882

.field public static final minutiae_place_picker_title:I = 0x7f080883

.field public static final minutiae_select_an_icon:I = 0x7f080878

.field public static final minutiae_taggable_object_subtitle:I = 0x7f0807ed

.field public static final minutiae_taggable_object_subtitle_with_page_name:I = 0x7f0807ee

.field public static final minutiae_taggable_object_subtitle_with_page_name_without_likes:I = 0x7f0807ec

.field public static final more_people_section_header:I = 0x7f080273

.field public static final more_settings:I = 0x7f080a31

.field public static final move_and_scale:I = 0x7f080057

.field public static final multi_friend_selector_title:I = 0x7f0808a4

.field public static final multi_group_member_selector_title:I = 0x7f0808a5

.field public static final music_error_text:I = 0x7f080a81

.field public static final music_error_unknown_app:I = 0x7f080a80

.field public static final music_no_metadata_text:I = 0x7f080a82

.field public static final mute_action_description:I = 0x7f08042f

.field public static final mute_off_button_description:I = 0x7f0801db

.field public static final mute_on_button_description:I = 0x7f0801dc

.field public static final mute_warning_button:I = 0x7f08035d

.field public static final mute_warning_button_caps:I = 0x7f08035e

.field public static final mute_warning_global:I = 0x7f080359

.field public static final mute_warning_global_snooze:I = 0x7f08035a

.field public static final mute_warning_thread:I = 0x7f08035b

.field public static final mute_warning_thread_snooze:I = 0x7f08035c

.field public static final name_or_phone_search_hint:I = 0x7f08027b

.field public static final name_search_hint:I = 0x7f08027a

.field public static final name_thread:I = 0x7f0802e8

.field public static final native_chooser_install:I = 0x7f080064

.field public static final native_chooser_install_default:I = 0x7f080065

.field public static final native_chooser_open_in_app:I = 0x7f080062

.field public static final native_chooser_open_in_app_default:I = 0x7f080063

.field public static final native_chooser_open_in_web:I = 0x7f080061

.field public static final native_chooser_title:I = 0x7f080060

.field public static final native_direct_link_cancel:I = 0x7f080066

.field public static final native_direct_link_install_button:I = 0x7f08006c

.field public static final native_direct_link_install_message:I = 0x7f08006d

.field public static final native_direct_link_install_message_default:I = 0x7f08006e

.field public static final native_direct_link_install_title:I = 0x7f08006b

.field public static final native_direct_link_open_button:I = 0x7f080068

.field public static final native_direct_link_open_message:I = 0x7f080069

.field public static final native_direct_link_open_message_default:I = 0x7f08006a

.field public static final native_direct_link_open_title:I = 0x7f080067

.field public static final nearby_category_all_art:I = 0x7f080df7

.field public static final nearby_category_all_restaurant:I = 0x7f080df6

.field public static final nearby_category_all_shopping:I = 0x7f080df8

.field public static final nearby_category_art:I = 0x7f080df2

.field public static final nearby_category_coffee:I = 0x7f080def

.field public static final nearby_category_history:I = 0x7f080df5

.field public static final nearby_category_hotel:I = 0x7f080df3

.field public static final nearby_category_nearby:I = 0x7f080ded

.field public static final nearby_category_nightlife:I = 0x7f080df0

.field public static final nearby_category_outdoor:I = 0x7f080df1

.field public static final nearby_category_restaurant:I = 0x7f080dee

.field public static final nearby_category_shopping:I = 0x7f080df4

.field public static final nearby_detect_location:I = 0x7f080de8

.field public static final nearby_find_a_place:I = 0x7f080dd6

.field public static final nearby_find_location_error_caption:I = 0x7f080de1

.field public static final nearby_find_location_error_description:I = 0x7f080de2

.field public static final nearby_global_context_separator:I = 0x7f080de9

.field public static final nearby_loading:I = 0x7f080de7

.field public static final nearby_location_setting_error:I = 0x7f080de3

.field public static final nearby_location_sources_turn_on_message:I = 0x7f080de5

.field public static final nearby_location_sources_turn_on_message_short:I = 0x7f080de6

.field public static final nearby_location_sources_turn_on_title:I = 0x7f080de4

.field public static final nearby_network_error:I = 0x7f080ddc

.field public static final nearby_network_error_caption:I = 0x7f080ddb

.field public static final nearby_no_location_error:I = 0x7f080dd8

.field public static final nearby_no_results_error:I = 0x7f080dda

.field public static final nearby_no_results_error_caption:I = 0x7f080dd9

.field public static final nearby_no_typeahead_results:I = 0x7f080ddd

.field public static final nearby_nux_restore_list_button:I = 0x7f080e22

.field public static final nearby_nux_search_button:I = 0x7f080e21

.field public static final nearby_places_facepile:I = 0x7f080dec

.field public static final nearby_places_fallback_description:I = 0x7f080e24

.field public static final nearby_places_fallback_title:I = 0x7f080e23

.field public static final nearby_places_settings:I = 0x7f080dea

.field public static final nearby_places_settings_skip:I = 0x7f080deb

.field public static final nearby_search_this_area:I = 0x7f080dd4

.field public static final nearby_search_tip:I = 0x7f080dd7

.field public static final nearby_subcategory_american:I = 0x7f080df9

.field public static final nearby_subcategory_antiques:I = 0x7f080e0c

.field public static final nearby_subcategory_art_gallery:I = 0x7f080e1d

.field public static final nearby_subcategory_bakery:I = 0x7f080dfa

.field public static final nearby_subcategory_bbq:I = 0x7f080dfb

.field public static final nearby_subcategory_books:I = 0x7f080e0d

.field public static final nearby_subcategory_breakfast:I = 0x7f080dfc

.field public static final nearby_subcategory_cafe:I = 0x7f080dfd

.field public static final nearby_subcategory_chinese:I = 0x7f080dfe

.field public static final nearby_subcategory_clothing:I = 0x7f080e0e

.field public static final nearby_subcategory_computers:I = 0x7f080e0f

.field public static final nearby_subcategory_convenience:I = 0x7f080e10

.field public static final nearby_subcategory_deli:I = 0x7f080dff

.field public static final nearby_subcategory_department:I = 0x7f080e11

.field public static final nearby_subcategory_food:I = 0x7f080e12

.field public static final nearby_subcategory_french:I = 0x7f080e00

.field public static final nearby_subcategory_furniture:I = 0x7f080e13

.field public static final nearby_subcategory_home_improvement:I = 0x7f080e14

.field public static final nearby_subcategory_ice_cream:I = 0x7f080e01

.field public static final nearby_subcategory_indian:I = 0x7f080e02

.field public static final nearby_subcategory_italian:I = 0x7f080e03

.field public static final nearby_subcategory_japanese:I = 0x7f080e04

.field public static final nearby_subcategory_jewelry:I = 0x7f080e15

.field public static final nearby_subcategory_korean:I = 0x7f080e05

.field public static final nearby_subcategory_meditterranean:I = 0x7f080e06

.field public static final nearby_subcategory_mexican:I = 0x7f080e07

.field public static final nearby_subcategory_movie:I = 0x7f080e1e

.field public static final nearby_subcategory_museum:I = 0x7f080e1f

.field public static final nearby_subcategory_music:I = 0x7f080e16

.field public static final nearby_subcategory_music_venues:I = 0x7f080e20

.field public static final nearby_subcategory_office:I = 0x7f080e17

.field public static final nearby_subcategory_pizza:I = 0x7f080e08

.field public static final nearby_subcategory_shopping_mall:I = 0x7f080e18

.field public static final nearby_subcategory_spa:I = 0x7f080e19

.field public static final nearby_subcategory_sports:I = 0x7f080e09

.field public static final nearby_subcategory_sports_rec:I = 0x7f080e1a

.field public static final nearby_subcategory_steakhouse:I = 0x7f080e0a

.field public static final nearby_subcategory_toys:I = 0x7f080e1b

.field public static final nearby_subcategory_vietnamese:I = 0x7f080e0b

.field public static final nearby_subcategory_wholesale:I = 0x7f080e1c

.field public static final nearby_title:I = 0x7f080dd5

.field public static final nearby_wifi_see_all:I = 0x7f080c43

.field public static final nearby_wifi_title:I = 0x7f080c42

.field public static final needle_search_filter_dialog_close:I = 0x7f080e47

.field public static final needle_search_filter_friends:I = 0x7f080e3c

.field public static final needle_search_filter_location:I = 0x7f080e3b

.field public static final needle_search_filter_posted_by:I = 0x7f080e40

.field public static final needle_search_filter_school:I = 0x7f080e3d

.field public static final needle_search_filter_search_hint_location:I = 0x7f080e44

.field public static final needle_search_filter_search_hint_name:I = 0x7f080e43

.field public static final needle_search_filter_search_hint_school:I = 0x7f080e46

.field public static final needle_search_filter_search_hint_work:I = 0x7f080e45

.field public static final needle_search_filter_tagged:I = 0x7f080e3f

.field public static final needle_search_filter_work:I = 0x7f080e3e

.field public static final needle_search_pivot_group_people:I = 0x7f080e4a

.field public static final needle_search_pivot_group_photos:I = 0x7f080e4b

.field public static final needle_search_pivot_page_people:I = 0x7f080e4c

.field public static final needle_search_pivot_page_photos:I = 0x7f080e4d

.field public static final needle_search_pivot_timeline_people:I = 0x7f080e48

.field public static final needle_search_pivot_timeline_photos:I = 0x7f080e49

.field public static final needle_search_title_people:I = 0x7f080e41

.field public static final needle_search_title_photos:I = 0x7f080e42

.field public static final network_error_message:I = 0x7f080032

.field public static final neue_presence_mobile:I = 0x7f08039f

.field public static final neue_presence_web:I = 0x7f0803a0

.field public static final new_address_dialog_title:I = 0x7f080d0a

.field public static final new_bookmark_item_logout:I = 0x7f0810d8

.field public static final new_bookmark_item_user_subtitle:I = 0x7f0810d7

.field public static final new_bookmark_section_name_facebook_apps:I = 0x7f0810dc

.field public static final new_bookmark_section_name_favorites:I = 0x7f0810da

.field public static final new_bookmark_section_name_groups:I = 0x7f0810de

.field public static final new_bookmark_section_name_lists:I = 0x7f0810e0

.field public static final new_bookmark_section_name_pages:I = 0x7f0810df

.field public static final new_bookmark_section_name_platform_apps:I = 0x7f0810dd

.field public static final new_bookmark_section_name_relevant_now:I = 0x7f0810db

.field public static final new_bookmark_section_name_settings:I = 0x7f0810d9

.field public static final new_bookmark_settings_name_account_settings:I = 0x7f0810e3

.field public static final new_bookmark_settings_name_activity_log:I = 0x7f0810e8

.field public static final new_bookmark_settings_name_app_settings:I = 0x7f0810e1

.field public static final new_bookmark_settings_name_code_generator:I = 0x7f0810e5

.field public static final new_bookmark_settings_name_help_center:I = 0x7f0810e4

.field public static final new_bookmark_settings_name_logout:I = 0x7f0810e9

.field public static final new_bookmark_settings_name_manage_news_feed:I = 0x7f0810e2

.field public static final new_bookmark_settings_name_privacy_shortcuts:I = 0x7f0810e6

.field public static final new_bookmark_settings_name_terms_and_policies:I = 0x7f0810e7

.field public static final new_bookmark_top_level_title:I = 0x7f0810d6

.field public static final new_bookmarks_edit_button_start:I = 0x7f0810ea

.field public static final new_bookmarks_edit_button_stop:I = 0x7f0810eb

.field public static final new_contacts_section_title:I = 0x7f080286

.field public static final new_event_selector_get_together:I = 0x7f081039

.field public static final new_event_selector_host_an_event:I = 0x7f08103a

.field public static final new_event_selector_title:I = 0x7f08103b

.field public static final newcomer_audience_call_to_action:I = 0x7f0808c4

.field public static final newcomer_audience_description_text:I = 0x7f0808c3

.field public static final newcomer_audience_learn_more_friends:I = 0x7f0808c5

.field public static final newcomer_audience_learn_more_friends_body:I = 0x7f0808c6

.field public static final newcomer_audience_learn_more_get_help:I = 0x7f0808cc

.field public static final newcomer_audience_learn_more_more_options_body:I = 0x7f0808ca

.field public static final newcomer_audience_learn_more_more_options_title:I = 0x7f0808c9

.field public static final newcomer_audience_learn_more_only_me:I = 0x7f0808cb

.field public static final newcomer_audience_learn_more_public:I = 0x7f0808c7

.field public static final newcomer_audience_learn_more_public_body:I = 0x7f0808c8

.field public static final newcomer_audience_tooltip_audience_friends_title:I = 0x7f0808bf

.field public static final newcomer_audience_tooltip_audience_public_title:I = 0x7f0808be

.field public static final newcomer_audience_tooltip_made_choice_body:I = 0x7f0808c2

.field public static final newcomer_audience_tooltip_more_options_body:I = 0x7f0808c0

.field public static final newcomer_audience_tooltip_skipped_title:I = 0x7f0808c1

.field public static final news_feed:I = 0x7f080551

.field public static final news_feed_most_recent:I = 0x7f080552

.field public static final newsfeed_settings:I = 0x7f080c7d

.field public static final next_button_default:I = 0x7f080c9f

.field public static final next_button_description:I = 0x7f080a89

.field public static final no:I = 0x7f080edc

.field public static final no_favorite_people_message:I = 0x7f08027e

.field public static final no_favorites_message:I = 0x7f08027d

.field public static final no_images_selected:I = 0x7f08077e

.field public static final no_images_selected_instructions:I = 0x7f08077f

.field public static final no_internet_connection:I = 0x7f080036

.field public static final no_media_found:I = 0x7f080419

.field public static final no_network_caption:I = 0x7f080de0

.field public static final no_new_requests:I = 0x7f08107c

.field public static final no_photos_to_show:I = 0x7f081145

.field public static final no_places_caption:I = 0x7f080dde

.field public static final no_places_description:I = 0x7f080ddf

.field public static final no_retry_default_heading:I = 0x7f080271

.field public static final nodex_app_settings_string:I = 0x7f080a09

.field public static final nodex_cannot_launch_fb_app_string:I = 0x7f0809ff

.field public static final nodex_cannot_run_fb_app_string:I = 0x7f080a00

.field public static final nodex_main_app_loading_message_string:I = 0x7f080a0b

.field public static final nodex_not_enough_space_promote_sd_move_string:I = 0x7f080a06

.field public static final nodex_not_enough_space_runtime_string:I = 0x7f080a07

.field public static final nodex_not_enough_space_string:I = 0x7f080a05

.field public static final nodex_reboot_string:I = 0x7f080a03

.field public static final nodex_uninstall_message_store_string:I = 0x7f080a01

.field public static final nodex_uninstall_message_string:I = 0x7f080a02

.field public static final nodex_uninstall_string:I = 0x7f080a08

.field public static final nodex_unsupported_android_version_string:I = 0x7f080a04

.field public static final nodex_upgrading_message_string:I = 0x7f080a0a

.field public static final normal_data_mode:I = 0x7f08019a

.field public static final not_now_request:I = 0x7f08016f

.field public static final notification_appfeed_reconnect_text:I = 0x7f0809f4

.field public static final notification_appfeed_reconnect_title:I = 0x7f0809f3

.field public static final notification_friend_request:I = 0x7f0809f8

.field public static final notification_music_error_text:I = 0x7f0809f6

.field public static final notification_music_error_unknown_app:I = 0x7f0809f5

.field public static final notification_music_no_metadata_text:I = 0x7f0809f7

.field public static final notification_reconnect_facebook_text:I = 0x7f0809fa

.field public static final notification_reconnect_facebook_title:I = 0x7f0809f9

.field public static final notification_tap_to_view:I = 0x7f0809f1

.field public static final notification_unknown_application_name:I = 0x7f0809f2

.field public static final notifications_center_label:I = 0x7f0808d7

.field public static final notifications_center_summary:I = 0x7f0808d8

.field public static final notifications_get_error:I = 0x7f0808d4

.field public static final notifications_group_ticker_message:I = 0x7f08033a

.field public static final notifications_login_reminder_description:I = 0x7f080339

.field public static final notifications_no_content:I = 0x7f0808d5

.field public static final notifications_settings_apps:I = 0x7f080f5a

.field public static final notifications_settings_cfa:I = 0x7f080f57

.field public static final notifications_settings_email:I = 0x7f080f56

.field public static final notifications_settings_groups:I = 0x7f080f59

.field public static final notifications_settings_how_you_get:I = 0x7f080f52

.field public static final notifications_settings_mobile_push:I = 0x7f080f54

.field public static final notifications_settings_sms:I = 0x7f080f55

.field public static final notifications_settings_subscribers:I = 0x7f080f58

.field public static final notifications_settings_you_get:I = 0x7f080f53

.field public static final notifications_title_label:I = 0x7f0808d6

.field public static final notify_new_build_text:I = 0x7f080c8f

.field public static final notify_new_build_ticker:I = 0x7f080c90

.field public static final notify_new_build_title:I = 0x7f080c8e

.field public static final num_billion_short:I = 0x7f080613

.field public static final num_hundred_million_long:I = 0x7f080615

.field public static final num_hundred_million_short:I = 0x7f080612

.field public static final num_million_short:I = 0x7f080611

.field public static final num_ten_thousand_long:I = 0x7f080614

.field public static final num_ten_thousand_short:I = 0x7f080610

.field public static final num_thousand_short:I = 0x7f08060f

.field public static final nux_image_content_description:I = 0x7f0810ac

.field public static final nux_inner_content_text_view_content_description:I = 0x7f0810ad

.field public static final nux_tabs_main_bubble_body:I = 0x7f080176

.field public static final nux_tabs_main_bubble_title:I = 0x7f080177

.field public static final nux_tabs_more_bubble_body:I = 0x7f080178

.field public static final nux_tabs_more_bubble_title:I = 0x7f080179

.field public static final offline:I = 0x7f08005b

.field public static final offline_post_delete_confirmation_message:I = 0x7f0805bf

.field public static final offline_post_failed:I = 0x7f0805bc

.field public static final offline_post_failed_popup_default_message:I = 0x7f0805be

.field public static final offline_post_failed_popup_title:I = 0x7f0805bd

.field public static final offline_post_will_retry:I = 0x7f0805bb

.field public static final offline_posting_accessibility_cancel:I = 0x7f0805b8

.field public static final offline_posting_accessibility_failed:I = 0x7f0805ba

.field public static final offline_posting_accessibility_retry:I = 0x7f0805b9

.field public static final offline_share_failed_message:I = 0x7f080803

.field public static final ok:I = 0x7f08077d

.field public static final on_messenger_section_title:I = 0x7f080289

.field public static final one_tap_login_label:I = 0x7f080eda

.field public static final ongoing_notification_text:I = 0x7f0809fe

.field public static final ongoing_notification_title:I = 0x7f0809fd

.field public static final open_camera:I = 0x7f080783

.field public static final open_photo_grid:I = 0x7f08078a

.field public static final optimistic_shared_post_default_title:I = 0x7f080873

.field public static final options_menu_schedule_time:I = 0x7f080858

.field public static final options_menu_title:I = 0x7f080857

.field public static final optionsbar_install_launcher:I = 0x7f08103d

.field public static final orca_action_add_contact:I = 0x7f0803de

.field public static final orca_action_contact_info:I = 0x7f0803df

.field public static final orca_action_new_message:I = 0x7f0803dc

.field public static final orca_action_reply:I = 0x7f0803e1

.field public static final orca_action_search:I = 0x7f0803dd

.field public static final orca_chat_heads_foreground_notif_dlg_text:I = 0x7f0803c2

.field public static final orca_chat_heads_foreground_notif_dlg_title:I = 0x7f0803c1

.field public static final orca_chat_heads_notification_active_title:I = 0x7f0803b6

.field public static final orca_chat_heads_notification_text_start_conversation:I = 0x7f0803b7

.field public static final orca_chat_heads_notification_title:I = 0x7f0803b5

.field public static final orca_chat_heads_system_tray_confirmation_dlg_title:I = 0x7f0803c0

.field public static final orca_compose_hot_like_nux_bubble:I = 0x7f0802a4

.field public static final orca_compose_text_button:I = 0x7f0802a3

.field public static final orca_generate_meme_menu:I = 0x7f080533

.field public static final orca_meme_popup_header:I = 0x7f080531

.field public static final orca_new_messenger_contact_status:I = 0x7f0801e5

.field public static final orca_photo_dialog_meme:I = 0x7f080532

.field public static final order_confirmation_button_pay_now:I = 0x7f080cd6

.field public static final order_confirmation_button_view_profile:I = 0x7f080cd7

.field public static final order_confirmation_gift_info:I = 0x7f080cd5

.field public static final order_confirmation_header:I = 0x7f080cd4

.field public static final order_overview_button_pay_now:I = 0x7f080cca

.field public static final order_overview_button_share_gift:I = 0x7f080cc9

.field public static final order_overview_custom_card_no_card:I = 0x7f080cc7

.field public static final order_overview_has_gifts:I = 0x7f080cc5

.field public static final order_overview_itunes_recommend_sku_option:I = 0x7f080cd2

.field public static final order_overview_no_sku_chosen_known_recipient:I = 0x7f080cc8

.field public static final order_overview_order_grand_total:I = 0x7f080cc3

.field public static final order_overview_order_shipping_total:I = 0x7f080cc2

.field public static final order_overview_order_sub_total_singular:I = 0x7f080ccc

.field public static final order_overview_order_tax_info:I = 0x7f080cc4

.field public static final order_overview_payment_details:I = 0x7f080ccf

.field public static final order_overview_payment_details_header:I = 0x7f080ccd

.field public static final order_overview_payment_details_prepay:I = 0x7f080cd0

.field public static final order_overview_payment_details_prepay_header:I = 0x7f080cce

.field public static final order_overview_personalization_header:I = 0x7f080cc6

.field public static final order_overview_tag_gift_for_header:I = 0x7f080ccb

.field public static final order_overview_terms_apply:I = 0x7f080cd1

.field public static final order_review_add_card_subinfo:I = 0x7f080d20

.field public static final order_review_add_card_title:I = 0x7f080d1f

.field public static final order_review_bought_confirmation:I = 0x7f080d27

.field public static final order_review_button_buy_gift:I = 0x7f080d1d

.field public static final order_review_button_checkout:I = 0x7f080d1c

.field public static final order_review_button_close:I = 0x7f080d1e

.field public static final order_review_cost_totals_discount:I = 0x7f080d16

.field public static final order_review_cost_totals_processing:I = 0x7f080d15

.field public static final order_review_cost_totals_shipping:I = 0x7f080d14

.field public static final order_review_cost_totals_subtotal:I = 0x7f080d13

.field public static final order_review_cost_totals_total:I = 0x7f080d12

.field public static final order_review_details_header:I = 0x7f080cd3

.field public static final order_review_payment_header:I = 0x7f080d26

.field public static final order_review_payment_info_title:I = 0x7f080d21

.field public static final order_review_share_on_timeline_disabled:I = 0x7f080d25

.field public static final order_review_share_on_timeline_female:I = 0x7f080d23

.field public static final order_review_share_on_timeline_male:I = 0x7f080d22

.field public static final order_review_share_on_timeline_unknown:I = 0x7f080d24

.field public static final order_review_terms:I = 0x7f080d18

.field public static final order_review_terms_apply_link:I = 0x7f080d17

.field public static final order_review_terms_post_info_female:I = 0x7f080d1a

.field public static final order_review_terms_post_info_male:I = 0x7f080d19

.field public static final order_review_terms_post_info_unknown:I = 0x7f080d1b

.field public static final other_sent_a_message_generic_admin_message:I = 0x7f0802c1

.field public static final other_sent_a_sticker_admin_message:I = 0x7f0802b9

.field public static final other_sent_a_video_admin_message:I = 0x7f0802bd

.field public static final other_sent_a_voice_clip_admin_message:I = 0x7f0802bf

.field public static final other_sent_an_image_admin_message:I = 0x7f0802bb

.field public static final outgoing_transactions_title:I = 0x7f0801ca

.field public static final page_admin_action_edit_admin:I = 0x7f080b83

.field public static final page_admin_action_edit_cover_photo:I = 0x7f080b80

.field public static final page_admin_action_edit_page:I = 0x7f080b82

.field public static final page_admin_action_edit_profile_pic:I = 0x7f080b81

.field public static final page_admin_action_view_admin:I = 0x7f080b84

.field public static final page_hours_closed:I = 0x7f081070

.field public static final page_hours_closed_today:I = 0x7f08106f

.field public static final page_hours_closed_until_day_status:I = 0x7f08106d

.field public static final page_hours_closed_until_later_today_status:I = 0x7f08106b

.field public static final page_hours_closed_until_next_day_status:I = 0x7f08106e

.field public static final page_hours_closed_until_tomorrow_status:I = 0x7f08106c

.field public static final page_hours_days_range:I = 0x7f081071

.field public static final page_hours_open_status:I = 0x7f08106a

.field public static final page_hours_range:I = 0x7f081072

.field public static final page_identity_action_call:I = 0x7f080b8c

.field public static final page_identity_action_checkin:I = 0x7f080b94

.field public static final page_identity_action_closed:I = 0x7f080ba1

.field public static final page_identity_action_copy_link:I = 0x7f080bad

.field public static final page_identity_action_create_page:I = 0x7f080bb0

.field public static final page_identity_action_email:I = 0x7f080b92

.field public static final page_identity_action_follow:I = 0x7f080b96

.field public static final page_identity_action_hide_from_newsfeed:I = 0x7f080b88

.field public static final page_identity_action_inappropriate_content:I = 0x7f080b9f

.field public static final page_identity_action_info_incorrect:I = 0x7f080ba7

.field public static final page_identity_action_like:I = 0x7f080b85

.field public static final page_identity_action_liked:I = 0x7f080b86

.field public static final page_identity_action_message:I = 0x7f080b95

.field public static final page_identity_action_not_closed:I = 0x7f080ba2

.field public static final page_identity_action_not_public_place:I = 0x7f080ba0

.field public static final page_identity_action_notify_off:I = 0x7f080b99

.field public static final page_identity_action_notify_off_fail:I = 0x7f080b9d

.field public static final page_identity_action_notify_off_success:I = 0x7f080b9b

.field public static final page_identity_action_notify_on:I = 0x7f080b98

.field public static final page_identity_action_notify_on_fail:I = 0x7f080b9c

.field public static final page_identity_action_notify_on_success:I = 0x7f080b9a

.field public static final page_identity_action_place_claim:I = 0x7f080bb1

.field public static final page_identity_action_report:I = 0x7f080b9e

.field public static final page_identity_action_report_failure:I = 0x7f080ba9

.field public static final page_identity_action_report_problem:I = 0x7f080bab

.field public static final page_identity_action_report_success:I = 0x7f080ba8

.field public static final page_identity_action_reported_closed:I = 0x7f080ba3

.field public static final page_identity_action_reported_closed_false:I = 0x7f080ba6

.field public static final page_identity_action_reported_closed_prompt:I = 0x7f080ba4

.field public static final page_identity_action_reported_closed_true:I = 0x7f080ba5

.field public static final page_identity_action_save:I = 0x7f080b8d

.field public static final page_identity_action_save_error:I = 0x7f080b90

.field public static final page_identity_action_saved:I = 0x7f080b8e

.field public static final page_identity_action_saved_unsave:I = 0x7f080b8f

.field public static final page_identity_action_share:I = 0x7f080bac

.field public static final page_identity_action_share_as_post:I = 0x7f080bae

.field public static final page_identity_action_sheet_more:I = 0x7f080baf

.field public static final page_identity_action_show_in_newsfeed:I = 0x7f080b89

.field public static final page_identity_action_suggest_edit:I = 0x7f080baa

.field public static final page_identity_action_unfollow:I = 0x7f080b97

.field public static final page_identity_action_unlike:I = 0x7f080b87

.field public static final page_identity_action_unsave_error:I = 0x7f080b91

.field public static final page_identity_action_view_website:I = 0x7f080b93

.field public static final page_identity_admin_switcher_tab_admin:I = 0x7f080bf0

.field public static final page_identity_admin_switcher_tab_public:I = 0x7f080bef

.field public static final page_identity_admin_tab_activity:I = 0x7f080c03

.field public static final page_identity_admin_tab_insights:I = 0x7f080c04

.field public static final page_identity_admin_tab_page:I = 0x7f080c02

.field public static final page_identity_average_rating_header:I = 0x7f080bd8

.field public static final page_identity_book_review_composer_heading:I = 0x7f080866

.field public static final page_identity_checkin_error:I = 0x7f080bdb

.field public static final page_identity_checkin_progress:I = 0x7f080bda

.field public static final page_identity_composer_action_post:I = 0x7f080bbc

.field public static final page_identity_consumption_button_description:I = 0x7f080bfb

.field public static final page_identity_count_format:I = 0x7f080bb2

.field public static final page_identity_data_fetch_error:I = 0x7f080bd0

.field public static final page_identity_data_fetch_error_see_more:I = 0x7f080bd2

.field public static final page_identity_data_fetch_error_tap_to_retry:I = 0x7f080bd1

.field public static final page_identity_event_going_text:I = 0x7f080bfa

.field public static final page_identity_event_join_text:I = 0x7f080bf9

.field public static final page_identity_event_rsvp_error:I = 0x7f080bf8

.field public static final page_identity_event_time_format_for_today:I = 0x7f080bed

.field public static final page_identity_event_time_format_with_day:I = 0x7f080bec

.field public static final page_identity_events_all_day:I = 0x7f080bf4

.field public static final page_identity_events_today:I = 0x7f080bf3

.field public static final page_identity_follow_error:I = 0x7f080bd5

.field public static final page_identity_follow_success:I = 0x7f080bdf

.field public static final page_identity_friends_here_now:I = 0x7f080bc8

.field public static final page_identity_friends_who_like:I = 0x7f080bc7

.field public static final page_identity_friends_who_like_local:I = 0x7f080bc6

.field public static final page_identity_friends_who_visited:I = 0x7f080bc5

.field public static final page_identity_get_directions:I = 0x7f080bb3

.field public static final page_identity_header_about_text:I = 0x7f080be2

.field public static final page_identity_hide_from_newsfeed_error:I = 0x7f080b8a

.field public static final page_identity_hours:I = 0x7f080bb4

.field public static final page_identity_hours_always_open:I = 0x7f080bb8

.field public static final page_identity_hours_closed:I = 0x7f080bb7

.field public static final page_identity_hours_open:I = 0x7f080bb6

.field public static final page_identity_hours_range_divider:I = 0x7f080bb9

.field public static final page_identity_hours_text:I = 0x7f080bb5

.field public static final page_identity_info_card_about:I = 0x7f080bbd

.field public static final page_identity_invite_friends:I = 0x7f080bc2

.field public static final page_identity_like_error:I = 0x7f080bd3

.field public static final page_identity_loading:I = 0x7f080bcf

.field public static final page_identity_menu:I = 0x7f080bbe

.field public static final page_identity_more_info:I = 0x7f080bbf

.field public static final page_identity_movie_review_composer_heading:I = 0x7f080867

.field public static final page_identity_new_likes:I = 0x7f080c01

.field public static final page_identity_opentable_back_button:I = 0x7f080c12

.field public static final page_identity_opentable_confirm_reservation_text:I = 0x7f080c17

.field public static final page_identity_opentable_confirm_reservation_title:I = 0x7f080c16

.field public static final page_identity_opentable_contact_information_disclaimer:I = 0x7f080c0e

.field public static final page_identity_opentable_contact_information_disclaimer_sent_data:I = 0x7f080c0f

.field public static final page_identity_opentable_contact_information_disclaimer_title:I = 0x7f080c0d

.field public static final page_identity_opentable_contact_information_email:I = 0x7f080c11

.field public static final page_identity_opentable_contact_information_phone_number:I = 0x7f080c10

.field public static final page_identity_opentable_contact_information_title:I = 0x7f080c0c

.field public static final page_identity_opentable_invalid_email:I = 0x7f080c14

.field public static final page_identity_opentable_invalid_phone:I = 0x7f080c15

.field public static final page_identity_opentable_reservation_cancel_button:I = 0x7f080c1c

.field public static final page_identity_opentable_reservation_cancel_generic_error:I = 0x7f080c1d

.field public static final page_identity_opentable_reservation_cancel_generic_error_title:I = 0x7f080c1e

.field public static final page_identity_opentable_reservation_default_restaurant_message:I = 0x7f080c1b

.field public static final page_identity_opentable_reservation_make_generic_error:I = 0x7f080c1f

.field public static final page_identity_opentable_reservation_make_generic_error_title:I = 0x7f080c20

.field public static final page_identity_opentable_reservation_restaurant_message:I = 0x7f080c1a

.field public static final page_identity_opentable_reservation_secondary_title:I = 0x7f080c19

.field public static final page_identity_opentable_reservation_title:I = 0x7f080c18

.field public static final page_identity_opentable_reserve_button:I = 0x7f080c13

.field public static final page_identity_opentable_search_change_party_size:I = 0x7f080c06

.field public static final page_identity_opentable_search_change_time:I = 0x7f080c05

.field public static final page_identity_opentable_search_date_time_subtitle:I = 0x7f080c09

.field public static final page_identity_opentable_search_default_search_message:I = 0x7f080c0a

.field public static final page_identity_opentable_search_default_search_no_tables:I = 0x7f080c0b

.field public static final page_identity_opentable_search_header_default:I = 0x7f080c07

.field public static final page_identity_opentable_search_header_with_party_size:I = 0x7f080c08

.field public static final page_identity_page_info_payment_option_cash_only:I = 0x7f080be3

.field public static final page_identity_page_link_copied:I = 0x7f080be1

.field public static final page_identity_photos:I = 0x7f080be4

.field public static final page_identity_photos_at_place_text:I = 0x7f080be5

.field public static final page_identity_photos_by_page_heading_text:I = 0x7f080be8

.field public static final page_identity_photos_by_page_tab_text:I = 0x7f080be6

.field public static final page_identity_photos_by_public_tab_text:I = 0x7f080be7

.field public static final page_identity_place_review_composer_heading:I = 0x7f080868

.field public static final page_identity_please_wait:I = 0x7f080bcc

.field public static final page_identity_pma:I = 0x7f080bfc

.field public static final page_identity_pma_desc:I = 0x7f080bfd

.field public static final page_identity_pma_messages:I = 0x7f080bff

.field public static final page_identity_pma_notifications:I = 0x7f080bfe

.field public static final page_identity_pma_num_new:I = 0x7f080c00

.field public static final page_identity_posts_by_others:I = 0x7f080bc4

.field public static final page_identity_posts_by_others_button_accessibility:I = 0x7f080c22

.field public static final page_identity_posts_by_others_generic:I = 0x7f080c23

.field public static final page_identity_posts_by_page:I = 0x7f080bc3

.field public static final page_identity_price_range:I = 0x7f080bc0

.field public static final page_identity_privacy_selector_accessibility_text:I = 0x7f080c2d

.field public static final page_identity_related_photos_heading:I = 0x7f080beb

.field public static final page_identity_review_composer_heading:I = 0x7f080865

.field public static final page_identity_reviews_fetch_error:I = 0x7f080bd7

.field public static final page_identity_scroll_back_to_top:I = 0x7f080bbb

.field public static final page_identity_see_all_child_locations:I = 0x7f080bee

.field public static final page_identity_see_all_photos_text:I = 0x7f080bea

.field public static final page_identity_share_error:I = 0x7f080bde

.field public static final page_identity_share_progress:I = 0x7f080bdc

.field public static final page_identity_share_success:I = 0x7f080bdd

.field public static final page_identity_show_in_newsfeed_error:I = 0x7f080b8b

.field public static final page_identity_similar_pages_heading_default:I = 0x7f080bc9

.field public static final page_identity_similar_pages_heading_nonregion:I = 0x7f080bca

.field public static final page_identity_similar_pages_heading_region:I = 0x7f080bcb

.field public static final page_identity_suggest_edits_success:I = 0x7f080bd9

.field public static final page_identity_talking_about_this:I = 0x7f080bce

.field public static final page_identity_timezone_caption:I = 0x7f080bba

.field public static final page_identity_tv_airings_cancel_reminder_button:I = 0x7f080c29

.field public static final page_identity_tv_airings_cancel_reminder_error:I = 0x7f080c2c

.field public static final page_identity_tv_airings_channel:I = 0x7f080c27

.field public static final page_identity_tv_airings_date:I = 0x7f080c25

.field public static final page_identity_tv_airings_episode_id:I = 0x7f080c2a

.field public static final page_identity_tv_airings_set_reminder_button:I = 0x7f080c28

.field public static final page_identity_tv_airings_set_reminder_error:I = 0x7f080c2b

.field public static final page_identity_tv_airings_time:I = 0x7f080c26

.field public static final page_identity_tv_airings_title:I = 0x7f080c24

.field public static final page_identity_tv_show_review_composer_heading:I = 0x7f080869

.field public static final page_identity_unfollow_error:I = 0x7f080bd6

.field public static final page_identity_unfollow_success:I = 0x7f080be0

.field public static final page_identity_unlike_error:I = 0x7f080bd4

.field public static final page_identity_upcoming_events:I = 0x7f080bf2

.field public static final page_identity_vertex_attribution_disclaimer:I = 0x7f080bf1

.field public static final page_identity_videos_by_page_heading_text:I = 0x7f080be9

.field public static final page_identity_view_one_rating_header:I = 0x7f080bcd

.field public static final page_identity_view_timeline:I = 0x7f080bc1

.field public static final page_identity_viewer_declined:I = 0x7f080bf7

.field public static final page_identity_viewer_going:I = 0x7f080bf5

.field public static final page_identity_viewer_might_go:I = 0x7f080bf6

.field public static final page_identity_viewer_post_success:I = 0x7f080c21

.field public static final page_inline_friend_inviter_failure_message:I = 0x7f08113d

.field public static final page_inline_friend_inviter_invite_sent:I = 0x7f08113f

.field public static final page_inline_friend_inviter_invite_text:I = 0x7f08113e

.field public static final page_selector_title:I = 0x7f080c7a

.field public static final page_topic_header_children:I = 0x7f080964

.field public static final page_topic_header_parent:I = 0x7f080963

.field public static final page_topic_title:I = 0x7f080962

.field public static final page_ui_composer_label_event:I = 0x7f080b7f

.field public static final page_ui_composer_label_photo:I = 0x7f080b7d

.field public static final page_ui_composer_label_text:I = 0x7f080b7c

.field public static final page_ui_composer_label_video:I = 0x7f080b7e

.field public static final page_ui_friend_inviter_title:I = 0x7f080b66

.field public static final page_ui_insights_weekly_post_reach:I = 0x7f080b69

.field public static final page_ui_invite_more_friends:I = 0x7f080b67

.field public static final page_ui_uni_active_page_ad:I = 0x7f080b6c

.field public static final page_ui_uni_daily_budget:I = 0x7f080b6f

.field public static final page_ui_uni_default_budget:I = 0x7f080b72

.field public static final page_ui_uni_duration_label:I = 0x7f080b73

.field public static final page_ui_uni_duration_start_to_end:I = 0x7f080b74

.field public static final page_ui_uni_duration_start_to_now:I = 0x7f080b75

.field public static final page_ui_uni_lifetime_budget:I = 0x7f080b71

.field public static final page_ui_uni_monthly_budget:I = 0x7f080b70

.field public static final page_ui_uni_page_likes:I = 0x7f080b6a

.field public static final page_ui_uni_page_likes_description:I = 0x7f080b6b

.field public static final page_ui_uni_people_reached:I = 0x7f080b6d

.field public static final page_ui_uni_status_active:I = 0x7f080b77

.field public static final page_ui_uni_status_creating:I = 0x7f080b7b

.field public static final page_ui_uni_status_finished:I = 0x7f080b79

.field public static final page_ui_uni_status_label:I = 0x7f080b76

.field public static final page_ui_uni_status_paused:I = 0x7f080b78

.field public static final page_ui_uni_status_pending:I = 0x7f080b7a

.field public static final page_ui_uni_total_spent:I = 0x7f080b6e

.field public static final page_voice_bar_acting_as:I = 0x7f080b00

.field public static final pages_browser:I = 0x7f08114b

.field public static final pages_browser_invited_by:I = 0x7f08114f

.field public static final pages_browser_list_explore_pages_subtitle:I = 0x7f08114e

.field public static final pages_browser_list_explore_pages_title:I = 0x7f08114d

.field public static final pages_browser_section_see_more:I = 0x7f08114c

.field public static final pages_no_search_results:I = 0x7f080c79

.field public static final pages_tab:I = 0x7f080c7f

.field public static final partial_failure_loading_event:I = 0x7f08004f

.field public static final partial_failure_loading_feed:I = 0x7f0805b6

.field public static final partial_failure_loading_page:I = 0x7f08004d

.field public static final partial_failure_loading_timeline:I = 0x7f08004b

.field public static final payment_add_payment_methods_header_string:I = 0x7f0801ab

.field public static final payment_add_payment_methods_header_string_no_name:I = 0x7f0801ac

.field public static final payment_add_payment_methods_title:I = 0x7f0801aa

.field public static final payment_billing_zip:I = 0x7f0801b0

.field public static final payment_button_description:I = 0x7f080258

.field public static final payment_card_number:I = 0x7f0801ad

.field public static final payment_expiration_date:I = 0x7f0801ae

.field public static final payment_pin_change:I = 0x7f0801bd

.field public static final payment_pin_change_title:I = 0x7f0801c3

.field public static final payment_pin_changed_toast:I = 0x7f0801c7

.field public static final payment_pin_confirm_header:I = 0x7f0801c1

.field public static final payment_pin_confirm_new_header:I = 0x7f0801c6

.field public static final payment_pin_created_toast:I = 0x7f0801c2

.field public static final payment_pin_creation_header:I = 0x7f0801c0

.field public static final payment_pin_creation_title:I = 0x7f0801bf

.field public static final payment_pin_deleted_toast:I = 0x7f0801c8

.field public static final payment_pin_enter_current_header:I = 0x7f0801c4

.field public static final payment_pin_enter_new_header:I = 0x7f0801c5

.field public static final payment_pin_preferences_title:I = 0x7f0801bc

.field public static final payment_pin_turn_off:I = 0x7f0801be

.field public static final payment_security_code:I = 0x7f0801af

.field public static final payment_transactions:I = 0x7f0801b1

.field public static final people_filter_text_hint:I = 0x7f0806b0

.field public static final people_in_your_photos:I = 0x7f0806b3

.field public static final people_tab_description:I = 0x7f0801e2

.field public static final people_you_may_know_title:I = 0x7f080168

.field public static final perm_ads_management:I = 0x7f081130

.field public static final perm_anonymous_publish_actions:I = 0x7f08112d

.field public static final perm_app_request:I = 0x7f0810ef

.field public static final perm_app_request_known_unknown:I = 0x7f0810ee

.field public static final perm_basic_info:I = 0x7f0810f6

.field public static final perm_basic_info_friend_list:I = 0x7f0810f7

.field public static final perm_create_event:I = 0x7f081131

.field public static final perm_csl_and_part:I = 0x7f0810f5

.field public static final perm_email:I = 0x7f0810f8

.field public static final perm_friends_about_me:I = 0x7f0810ff

.field public static final perm_friends_access:I = 0x7f0810f1

.field public static final perm_friends_activities:I = 0x7f081101

.field public static final perm_friends_birthday:I = 0x7f081103

.field public static final perm_friends_checkins:I = 0x7f081105

.field public static final perm_friends_education_history:I = 0x7f081107

.field public static final perm_friends_events:I = 0x7f081109

.field public static final perm_friends_groups:I = 0x7f08110b

.field public static final perm_friends_hometown:I = 0x7f08110d

.field public static final perm_friends_interests:I = 0x7f08110f

.field public static final perm_friends_likes:I = 0x7f081111

.field public static final perm_friends_location:I = 0x7f081113

.field public static final perm_friends_notes:I = 0x7f081115

.field public static final perm_friends_online_presence:I = 0x7f081117

.field public static final perm_friends_photos:I = 0x7f081119

.field public static final perm_friends_questions:I = 0x7f08111b

.field public static final perm_friends_relationship_details:I = 0x7f08111f

.field public static final perm_friends_relationships:I = 0x7f08111d

.field public static final perm_friends_religion_politics:I = 0x7f081121

.field public static final perm_friends_status:I = 0x7f081123

.field public static final perm_friends_subscriptions:I = 0x7f081125

.field public static final perm_friends_videos:I = 0x7f081127

.field public static final perm_friends_website:I = 0x7f081129

.field public static final perm_friends_work_history:I = 0x7f08112b

.field public static final perm_manage_friendlists:I = 0x7f081132

.field public static final perm_manage_notifications:I = 0x7f081133

.field public static final perm_manage_pages:I = 0x7f081134

.field public static final perm_publish_actions:I = 0x7f08112c

.field public static final perm_publish_checkins:I = 0x7f08112e

.field public static final perm_publish_stream:I = 0x7f08112f

.field public static final perm_read_friendlists:I = 0x7f0810f9

.field public static final perm_read_insights:I = 0x7f0810fa

.field public static final perm_read_mailbox:I = 0x7f0810fb

.field public static final perm_read_requests:I = 0x7f0810fc

.field public static final perm_read_stream:I = 0x7f0810fd

.field public static final perm_rsvp_event:I = 0x7f081135

.field public static final perm_user_about_me:I = 0x7f0810fe

.field public static final perm_user_access:I = 0x7f0810f0

.field public static final perm_user_activities:I = 0x7f081100

.field public static final perm_user_and_friends_access:I = 0x7f0810f2

.field public static final perm_user_birthday:I = 0x7f081102

.field public static final perm_user_checkins:I = 0x7f081104

.field public static final perm_user_education_history:I = 0x7f081106

.field public static final perm_user_events:I = 0x7f081108

.field public static final perm_user_groups:I = 0x7f08110a

.field public static final perm_user_hometown:I = 0x7f08110c

.field public static final perm_user_interests:I = 0x7f08110e

.field public static final perm_user_likes:I = 0x7f081110

.field public static final perm_user_location:I = 0x7f081112

.field public static final perm_user_manage:I = 0x7f0810f4

.field public static final perm_user_notes:I = 0x7f081114

.field public static final perm_user_online_presence:I = 0x7f081116

.field public static final perm_user_photos:I = 0x7f081118

.field public static final perm_user_questions:I = 0x7f08111a

.field public static final perm_user_relationship_details:I = 0x7f08111e

.field public static final perm_user_relationships:I = 0x7f08111c

.field public static final perm_user_religion_politics:I = 0x7f081120

.field public static final perm_user_status:I = 0x7f081122

.field public static final perm_user_subscriptions:I = 0x7f081124

.field public static final perm_user_videos:I = 0x7f081126

.field public static final perm_user_website:I = 0x7f081128

.field public static final perm_user_work_history:I = 0x7f08112a

.field public static final perm_user_write:I = 0x7f0810f3

.field public static final perm_write_everyone:I = 0x7f081137

.field public static final perm_write_friends:I = 0x7f081138

.field public static final perm_write_self:I = 0x7f081139

.field public static final perm_xmpp_login:I = 0x7f081136

.field public static final permission_error_message:I = 0x7f08003f

.field public static final photo_cancel_tagging:I = 0x7f08064f

.field public static final photo_comment:I = 0x7f08064b

.field public static final photo_description:I = 0x7f080785

.field public static final photo_dialog_choose_photo:I = 0x7f080252

.field public static final photo_dialog_choose_photo_or_video:I = 0x7f080253

.field public static final photo_dialog_image_search:I = 0x7f080254

.field public static final photo_dialog_record_audio:I = 0x7f080256

.field public static final photo_dialog_record_video:I = 0x7f080255

.field public static final photo_dialog_remove:I = 0x7f080257

.field public static final photo_dialog_take_photo:I = 0x7f080251

.field public static final photo_drawing_cancel_button:I = 0x7f08041e

.field public static final photo_drawing_caption_button:I = 0x7f080420

.field public static final photo_drawing_draw_button:I = 0x7f08041f

.field public static final photo_drawing_send_button:I = 0x7f08041d

.field public static final photo_drawing_undo_button:I = 0x7f08041c

.field public static final photo_like:I = 0x7f080649

.field public static final photo_more:I = 0x7f08064e

.field public static final photo_save_error:I = 0x7f08063c

.field public static final photo_save_success:I = 0x7f08063d

.field public static final photo_set_add_photos:I = 0x7f08074f

.field public static final photo_set_downloading_cover_photo:I = 0x7f080752

.field public static final photo_set_upload_cover_photo:I = 0x7f080751

.field public static final photo_set_upload_profile_picture:I = 0x7f080750

.field public static final photo_share:I = 0x7f08064c

.field public static final photo_stories:I = 0x7f080edd

.field public static final photo_tag:I = 0x7f08064d

.field public static final photo_tag_friend_instructions:I = 0x7f080634

.field public static final photo_tag_instruction:I = 0x7f080056

.field public static final photo_tag_max_limit:I = 0x7f080665

.field public static final photo_tag_suggestion:I = 0x7f080664

.field public static final photo_tag_typeahead_prompt:I = 0x7f080633

.field public static final photo_toggle_button_selected_state:I = 0x7f080055

.field public static final photo_toggle_button_unselected_state:I = 0x7f080054

.field public static final photo_unlike:I = 0x7f08064a

.field public static final photo_upload_review_notification_complete:I = 0x7f08090b

.field public static final photo_upload_tagging_notification_complete:I = 0x7f08090a

.field public static final photo_viewer_message_count:I = 0x7f08024a

.field public static final photos_I_like:I = 0x7f080a77

.field public static final photos_I_took:I = 0x7f080a78

.field public static final photos_album_select:I = 0x7f0807fc

.field public static final photos_delete:I = 0x7f08063e

.field public static final photos_delete_photo_error:I = 0x7f080643

.field public static final photos_delete_photo_question:I = 0x7f08063f

.field public static final photos_deleting_photo:I = 0x7f080642

.field public static final photos_get_error:I = 0x7f080773

.field public static final photos_no_photos:I = 0x7f080774

.field public static final photos_of_close_friends:I = 0x7f080a7e

.field public static final photos_of_friends:I = 0x7f080a7f

.field public static final photos_of_me:I = 0x7f080a79

.field public static final photos_of_me_and_signoth:I = 0x7f080a7b

.field public static final photos_of_sigoth:I = 0x7f080a7a

.field public static final photos_of_target:I = 0x7f081143

.field public static final photos_of_target_and_mutual_friends:I = 0x7f081150

.field public static final photos_of_you:I = 0x7f081142

.field public static final photos_of_you_and_target:I = 0x7f081144

.field public static final photos_recommended_for_me:I = 0x7f080a7c

.field public static final photos_taken_by_friends:I = 0x7f080a7d

.field public static final pinned_groups_tab_description:I = 0x7f0801e3

.field public static final place_address_street_type_does_have_physical_address:I = 0x7f080965

.field public static final place_address_street_type_no_physical_address:I = 0x7f080966

.field public static final place_choose_another_picture:I = 0x7f080960

.field public static final place_photo_uploading_toast:I = 0x7f08095f

.field public static final place_remove_picture:I = 0x7f080961

.field public static final places_add:I = 0x7f080967

.field public static final places_add_content:I = 0x7f0809ac

.field public static final places_add_home_title:I = 0x7f08096a

.field public static final places_add_in_city:I = 0x7f080968

.field public static final places_add_location_title:I = 0x7f08096c

.field public static final places_add_new:I = 0x7f08098e

.field public static final places_add_title:I = 0x7f080969

.field public static final places_address:I = 0x7f0809a0

.field public static final places_address_hint:I = 0x7f080991

.field public static final places_categories:I = 0x7f0809a4

.field public static final places_categories_emptyadd:I = 0x7f08099f

.field public static final places_categories_toomany:I = 0x7f08099e

.field public static final places_category_hint:I = 0x7f080992

.field public static final places_checkin_card_friends_were_here_name_and_count:I = 0x7f0809b9

.field public static final places_checkin_card_friends_were_here_names:I = 0x7f0809ba

.field public static final places_checkin_error:I = 0x7f08096d

.field public static final places_checkin_title:I = 0x7f08096b

.field public static final places_city:I = 0x7f0809a3

.field public static final places_city_hint:I = 0x7f080990

.field public static final places_close_button_description:I = 0x7f0809b8

.field public static final places_create_error:I = 0x7f080976

.field public static final places_create_location_inaccurate:I = 0x7f080977

.field public static final places_create_location_off:I = 0x7f080978

.field public static final places_distance_feet:I = 0x7f080189

.field public static final places_distance_kilometers:I = 0x7f08018b

.field public static final places_distance_meters:I = 0x7f08018a

.field public static final places_distance_miles:I = 0x7f080188

.field public static final places_event_at:I = 0x7f0809bb

.field public static final places_first_time_image_view_item_1_address:I = 0x7f0809c5

.field public static final places_first_time_image_view_item_1_title:I = 0x7f0809c4

.field public static final places_first_time_image_view_item_2_address:I = 0x7f0809c7

.field public static final places_first_time_image_view_item_2_title:I = 0x7f0809c6

.field public static final places_first_time_image_view_item_3_address:I = 0x7f0809c9

.field public static final places_first_time_image_view_item_3_title:I = 0x7f0809c8

.field public static final places_first_time_view_details_1:I = 0x7f0809c1

.field public static final places_first_time_view_details_2:I = 0x7f0809c2

.field public static final places_first_time_view_details_3:I = 0x7f0809c3

.field public static final places_home_creation_notif:I = 0x7f080974

.field public static final places_home_privacy:I = 0x7f080973

.field public static final places_inappropriate:I = 0x7f080999

.field public static final places_invalid_name_blacklist_error:I = 0x7f080980

.field public static final places_invalid_name_blacklist_suggestion_error:I = 0x7f080981

.field public static final places_invalid_name_caps_error:I = 0x7f080982

.field public static final places_invalid_name_caps_suggestion_error:I = 0x7f080983

.field public static final places_invalid_name_error:I = 0x7f08097c

.field public static final places_invalid_name_invalid_chars_error:I = 0x7f08097e

.field public static final places_invalid_name_invalid_chars_suggestion_error:I = 0x7f08097f

.field public static final places_invalid_name_suggestion_error:I = 0x7f08097d

.field public static final places_invalid_phone_error:I = 0x7f080958

.field public static final places_invalid_website_error:I = 0x7f080959

.field public static final places_just_use_text_as_location:I = 0x7f0809a7

.field public static final places_location_at:I = 0x7f0809ae

.field public static final places_location_error_message:I = 0x7f0809af

.field public static final places_location_keep_trying:I = 0x7f0809b1

.field public static final places_location_settings:I = 0x7f08098c

.field public static final places_location_skip:I = 0x7f0809b0

.field public static final places_location_sources_turn_on_message:I = 0x7f0809ad

.field public static final places_location_unknown_type_to_search:I = 0x7f08096e

.field public static final places_map_hint:I = 0x7f08095d

.field public static final places_mark_duplicate:I = 0x7f080997

.field public static final places_mark_duplicates:I = 0x7f080998

.field public static final places_most_visited_places:I = 0x7f080971

.field public static final places_name_hint:I = 0x7f08098f

.field public static final places_nearby:I = 0x7f080970

.field public static final places_nearby_same_error:I = 0x7f08097a

.field public static final places_nearby_similar_warning:I = 0x7f08097b

.field public static final places_no_category_error:I = 0x7f080979

.field public static final places_no_places_found:I = 0x7f080984

.field public static final places_no_places_found_type_to_search:I = 0x7f080985

.field public static final places_no_recent_checkins:I = 0x7f080986

.field public static final places_not_now:I = 0x7f080987

.field public static final places_not_public_place:I = 0x7f08099a

.field public static final places_not_public_places:I = 0x7f08099b

.field public static final places_phone_hint:I = 0x7f080993

.field public static final places_phone_number:I = 0x7f0809a1

.field public static final places_picture_suggestion:I = 0x7f0809ab

.field public static final places_picture_suggestion_cancel:I = 0x7f0809aa

.field public static final places_picture_suggestion_confirmation:I = 0x7f0809a8

.field public static final places_picture_suggestion_continue:I = 0x7f0809a9

.field public static final places_place_creation_notif:I = 0x7f080975

.field public static final places_recent_places:I = 0x7f080972

.field public static final places_remove_tag:I = 0x7f080988

.field public static final places_report_duplicates:I = 0x7f080f84

.field public static final places_search_for_a_place:I = 0x7f080989

.field public static final places_search_for_a_place_near_city:I = 0x7f08098a

.field public static final places_see_nearby_places:I = 0x7f0809c0

.field public static final places_select_duplicates:I = 0x7f08099c

.field public static final places_select_duplicates_short:I = 0x7f080f83

.field public static final places_select_places_edit:I = 0x7f080996

.field public static final places_settings:I = 0x7f08098b

.field public static final places_suggest_checkin_composer_close_out_description:I = 0x7f0809b7

.field public static final places_suggest_checkin_composer_image_description:I = 0x7f0809b6

.field public static final places_suggest_checkin_composer_info:I = 0x7f0809b4

.field public static final places_suggest_checkin_composer_info_description:I = 0x7f0809b5

.field public static final places_suggest_checkin_composer_prompt:I = 0x7f0809b2

.field public static final places_suggest_checkin_composer_prompt_description:I = 0x7f0809b3

.field public static final places_suggest_edits:I = 0x7f080995

.field public static final places_suggestions_error:I = 0x7f0809a6

.field public static final places_suggestions_submitted:I = 0x7f08099d

.field public static final places_top_result:I = 0x7f08096f

.field public static final places_use:I = 0x7f08098d

.field public static final places_website:I = 0x7f0809a2

.field public static final places_website_hint:I = 0x7f080994

.field public static final platform_sending_post:I = 0x7f080ede

.field public static final play_button_description:I = 0x7f080a88

.field public static final please_edit_video_text:I = 0x7f080427

.field public static final plutonium_context_subtitle_application:I = 0x7f080acd

.field public static final plutonium_context_subtitle_datetime_application:I = 0x7f080acc

.field public static final plutonium_pivot_all_albums:I = 0x7f080b5e

.field public static final plutonium_pivot_all_photos:I = 0x7f080b5d

.field public static final plutonium_pivot_tagged_photos:I = 0x7f080b5c

.field public static final post_failed:I = 0x7f080c57

.field public static final post_num_photos_many:I = 0x7f080788

.field public static final post_num_photos_one:I = 0x7f080787

.field public static final post_photos:I = 0x7f080784

.field public static final post_post_desc1:I = 0x7f080654

.field public static final post_post_header:I = 0x7f080653

.field public static final post_post_photo_count:I = 0x7f080655

.field public static final posting_in_progress:I = 0x7f0807e0

.field public static final posts_permalink_fragment_title:I = 0x7f0805de

.field public static final preference_audio_configurator:I = 0x7f080d49

.field public static final preference_audio_configurator_off_summary:I = 0x7f080d4b

.field public static final preference_audio_configurator_on_summary:I = 0x7f080d4a

.field public static final preference_autoplay:I = 0x7f0805c0

.field public static final preference_autoplay_off:I = 0x7f0805c3

.field public static final preference_autoplay_on:I = 0x7f0805c1

.field public static final preference_autoplay_wifi_only:I = 0x7f0805c2

.field public static final preference_category_settings_title:I = 0x7f0802f6

.field public static final preference_clear_image_cache_description:I = 0x7f080310

.field public static final preference_clear_image_cache_title:I = 0x7f08030f

.field public static final preference_clear_stickers_cache_description:I = 0x7f080312

.field public static final preference_clear_stickers_cache_title:I = 0x7f080311

.field public static final preference_clear_thread_cache_description:I = 0x7f080314

.field public static final preference_clear_thread_cache_title:I = 0x7f080313

.field public static final preference_contacts_delete_contacts_deleted:I = 0x7f080320

.field public static final preference_contacts_delete_contacts_deleting:I = 0x7f08031f

.field public static final preference_contacts_delete_contacts_dialog_message:I = 0x7f08031d

.field public static final preference_contacts_delete_contacts_error:I = 0x7f080321

.field public static final preference_contacts_delete_contacts_title:I = 0x7f08031e

.field public static final preference_contacts_start_contacts_syncing_dialog_message:I = 0x7f080328

.field public static final preference_contacts_stop_contacts_syncing_dialog_message:I = 0x7f080326

.field public static final preference_contacts_stop_contacts_syncing_in_progress:I = 0x7f080327

.field public static final preference_contacts_sync_contacts_error:I = 0x7f080325

.field public static final preference_contacts_sync_contacts_synced:I = 0x7f080324

.field public static final preference_contacts_sync_contacts_syncing:I = 0x7f080323

.field public static final preference_contacts_sync_contacts_title:I = 0x7f080322

.field public static final preference_contacts_title:I = 0x7f08031c

.field public static final preference_location_services_dialog_checkbox:I = 0x7f080329

.field public static final preference_location_services_dialog_description1:I = 0x7f08031a

.field public static final preference_location_services_dialog_description2:I = 0x7f08031b

.field public static final preference_location_services_dialog_title:I = 0x7f080319

.field public static final preference_location_services_disabled_summary:I = 0x7f080318

.field public static final preference_location_services_enabled_summary:I = 0x7f080317

.field public static final preference_location_services_title:I = 0x7f080316

.field public static final preference_mobile_chat_availability_off_summary:I = 0x7f080335

.field public static final preference_mobile_chat_availability_on_summary:I = 0x7f080334

.field public static final preference_mobile_chat_availability_title:I = 0x7f080333

.field public static final preference_my_wallet_title:I = 0x7f08032a

.field public static final preference_notification_preview:I = 0x7f08032b

.field public static final preference_notification_preview_name_and_message:I = 0x7f08032c

.field public static final preference_notifications_chat_heads_disabled:I = 0x7f080305

.field public static final preference_notifications_chat_heads_enabled:I = 0x7f080304

.field public static final preference_notifications_chat_heads_hide_full_screen:I = 0x7f08030b

.field public static final preference_notifications_chat_heads_title:I = 0x7f080302

.field public static final preference_notifications_chat_heads_toggle:I = 0x7f080303

.field public static final preference_notifications_dialog_label:I = 0x7f0802f8

.field public static final preference_notifications_disabled:I = 0x7f0802fa

.field public static final preference_notifications_dive_head_shortcut_notif:I = 0x7f080306

.field public static final preference_notifications_dive_head_shortcut_notif_summary:I = 0x7f080307

.field public static final preference_notifications_enabled:I = 0x7f0802f9

.field public static final preference_notifications_enabled_title:I = 0x7f0802fc

.field public static final preference_notifications_group_title:I = 0x7f0802f7

.field public static final preference_notifications_in_app_sounds_enabled_title:I = 0x7f0802fe

.field public static final preference_notifications_led_enabled_title:I = 0x7f080300

.field public static final preference_notifications_messenger_ringtone_title:I = 0x7f08030d

.field public static final preference_notifications_muted_until:I = 0x7f0802fb

.field public static final preference_notifications_open_chat_head_settings:I = 0x7f08030c

.field public static final preference_notifications_ringtone_title:I = 0x7f080308

.field public static final preference_notifications_silent_sound_text:I = 0x7f080301

.field public static final preference_notifications_sound_enabled_title:I = 0x7f0802fd

.field public static final preference_notifications_system_default_ringtone_title:I = 0x7f08030e

.field public static final preference_notifications_vibrate_enabled_title:I = 0x7f0802ff

.field public static final preference_notifications_voip_ringtone_title:I = 0x7f080309

.field public static final preference_notifications_voip_vibrate_title:I = 0x7f08030a

.field public static final preference_photos_auto_download_dialog_checkbox:I = 0x7f080330

.field public static final preference_photos_auto_download_dialog_description:I = 0x7f080331

.field public static final preference_photos_auto_download_disabled_summary:I = 0x7f08032f

.field public static final preference_photos_auto_download_enabled_summary:I = 0x7f08032e

.field public static final preference_photos_auto_download_title:I = 0x7f08032d

.field public static final preference_reset_nux_title:I = 0x7f0804af

.field public static final preference_sounds:I = 0x7f080d46

.field public static final preference_sounds_off_summary:I = 0x7f080d48

.field public static final preference_sounds_on_summary:I = 0x7f080d47

.field public static final preference_switch_account_summary:I = 0x7f080332

.field public static final preference_title:I = 0x7f0802f5

.field public static final preference_version_description:I = 0x7f080315

.field public static final preference_zero_rating_allow_on_wifi_title:I = 0x7f0804ad

.field public static final preference_zero_rating_available_no:I = 0x7f0804a8

.field public static final preference_zero_rating_available_title:I = 0x7f0804a6

.field public static final preference_zero_rating_available_yes:I = 0x7f0804a7

.field public static final preference_zero_rating_clear_preferences:I = 0x7f0804ae

.field public static final preference_zero_rating_current_status_title:I = 0x7f0804a9

.field public static final preference_zero_rating_disabled:I = 0x7f0804ab

.field public static final preference_zero_rating_enabled:I = 0x7f0804aa

.field public static final preference_zero_rating_unknown:I = 0x7f0804ac

.field public static final presence_active_now:I = 0x7f080372

.field public static final presence_active_now_short:I = 0x7f080373

.field public static final presence_active_now_short_uppercase:I = 0x7f080389

.field public static final presence_active_now_uppercase:I = 0x7f080388

.field public static final presence_active_one_day_ago:I = 0x7f080378

.field public static final presence_active_one_day_ago_uppercase:I = 0x7f08038e

.field public static final presence_active_one_hour_ago:I = 0x7f080376

.field public static final presence_active_one_hour_ago_uppercase:I = 0x7f08038c

.field public static final presence_active_one_min_ago:I = 0x7f080374

.field public static final presence_active_one_min_ago_uppercase:I = 0x7f08038a

.field public static final presence_active_x_days_ago:I = 0x7f080379

.field public static final presence_active_x_days_ago_uppercase:I = 0x7f08038f

.field public static final presence_active_x_hours_ago:I = 0x7f080377

.field public static final presence_active_x_hours_ago_uppercase:I = 0x7f08038d

.field public static final presence_active_x_mins_ago:I = 0x7f080375

.field public static final presence_active_x_mins_ago_uppercase:I = 0x7f08038b

.field public static final presence_mobile:I = 0x7f080396

.field public static final presence_typing:I = 0x7f080397

.field public static final presence_typing_description:I = 0x7f080398

.field public static final prev_button_description:I = 0x7f080a87

.field public static final preview_will_be_added:I = 0x7f0807dd

.field public static final privacy_custom:I = 0x7f0809d1

.field public static final privacy_friends:I = 0x7f0809cd

.field public static final privacy_friends_of_friends:I = 0x7f0809cb

.field public static final privacy_friends_of_guests:I = 0x7f0809ce

.field public static final privacy_invite_only:I = 0x7f0809cf

.field public static final privacy_network_and_friends:I = 0x7f0809cc

.field public static final privacy_only_me:I = 0x7f0809d0

.field public static final privacy_options_section_audience:I = 0x7f0809d4

.field public static final privacy_options_section_friend_lists:I = 0x7f0809d5

.field public static final privacy_public:I = 0x7f0809ca

.field public static final privacy_shortcuts:I = 0x7f080edf

.field public static final processing:I = 0x7f08077a

.field public static final product_details_description_section_header:I = 0x7f080cbe

.field public static final product_details_let_them_choose_known_recipient_sku_title:I = 0x7f080cbc

.field public static final product_details_let_them_choose_unknown_recipient_sku_title:I = 0x7f080cbd

.field public static final product_details_recommended_gifts_header:I = 0x7f080cc0

.field public static final product_details_see_all_gifts_button:I = 0x7f080cc1

.field public static final product_details_view_all_products_header:I = 0x7f080cbf

.field public static final product_details_view_give_gift_button_text:I = 0x7f080cba

.field public static final product_order_details_remove_product:I = 0x7f080cbb

.field public static final product_sku_choose_sentence:I = 0x7f080cb7

.field public static final product_sku_let_them_choose_header_known_recipient:I = 0x7f080cb5

.field public static final product_sku_let_them_choose_header_unknown_recipient:I = 0x7f080cb6

.field public static final product_sku_let_them_choose_sentence:I = 0x7f080cb8

.field public static final product_sku_let_them_choose_sentence_no_recipient:I = 0x7f080cb9

.field public static final products_a_gift_for_header:I = 0x7f080cac

.field public static final products_cant_buy_alcohol_button_learn_more:I = 0x7f080cb3

.field public static final products_cant_buy_alcohol_button_ok:I = 0x7f080cb4

.field public static final products_cant_buy_alcohol_dialog_body:I = 0x7f080cb2

.field public static final products_cant_buy_alcohol_dialog_title:I = 0x7f080cb1

.field public static final products_filter_all_gifts_option:I = 0x7f080cb0

.field public static final products_filter_recommended_option:I = 0x7f080cad

.field public static final products_filter_recommended_option_no_recipient:I = 0x7f080caf

.field public static final products_filter_recommended_option_unknown_recipient:I = 0x7f080cae

.field public static final products_no_products_found:I = 0x7f080cab

.field public static final profile_add_as_friend:I = 0x7f080ee1

.field public static final profile_albums_tab_title:I = 0x7f080ee4

.field public static final profile_completeness_percentage_string:I = 0x7f080954

.field public static final profile_info_location:I = 0x7f080ee2

.field public static final profile_info_request_button_text:I = 0x7f081064

.field public static final profile_info_request_edit_text_hint:I = 0x7f081063

.field public static final profile_info_request_fail_message:I = 0x7f081067

.field public static final profile_info_request_footer_text:I = 0x7f081068

.field public static final profile_info_request_footer_text_with_name:I = 0x7f081069

.field public static final profile_info_request_process_text:I = 0x7f081065

.field public static final profile_info_request_success_message:I = 0x7f081066

.field public static final profile_info_request_title:I = 0x7f081061

.field public static final profile_info_request_title_with_name:I = 0x7f081062

.field public static final profile_list_clear_accessibility:I = 0x7f0808d3

.field public static final profile_list_search_accessibility:I = 0x7f0808d2

.field public static final profile_photos_tab_title:I = 0x7f080ee3

.field public static final profile_pic_cover_photo_edit_choose_photo:I = 0x7f080b4e

.field public static final profile_pic_cover_photo_upload:I = 0x7f080b4d

.field public static final profile_pic_load_error:I = 0x7f08094d

.field public static final profile_pic_min_size_error:I = 0x7f08094b

.field public static final profile_pic_update_error:I = 0x7f08094c

.field public static final profile_pic_update_success:I = 0x7f08094f

.field public static final profile_pic_updating:I = 0x7f080950

.field public static final profile_picture_view:I = 0x7f080b4b

.field public static final profile_self_photos_tab_title:I = 0x7f081152

.field public static final profile_self_uploads_tab_title:I = 0x7f081153

.field public static final profile_sync_settings_button:I = 0x7f080ee6

.field public static final profile_sync_tab_title:I = 0x7f080ee5

.field public static final profile_target_user_photos_tab_title:I = 0x7f081154

.field public static final profile_target_user_uploads_tab_title:I = 0x7f081155

.field public static final profile_title_label:I = 0x7f080e84

.field public static final profile_uploads_tab_title:I = 0x7f081151

.field public static final profile_view_profile:I = 0x7f080d4c

.field public static final profilelist_search:I = 0x7f080c7c

.field public static final profilephoto_edit:I = 0x7f080777

.field public static final promote_story:I = 0x7f0806e2

.field public static final prompt_city:I = 0x7f0809bf

.field public static final public_privacy_description:I = 0x7f080839

.field public static final publish_duplicate_error_message:I = 0x7f080040

.field public static final publish_invalid_tag_error_message:I = 0x7f080042

.field public static final publish_mode_selector_draft_nux_text:I = 0x7f080864

.field public static final publish_mode_selector_nux_text:I = 0x7f080863

.field public static final publish_sentry_fail:I = 0x7f080041

.field public static final publisher_create_event:I = 0x7f0805b3

.field public static final publisher_page_status:I = 0x7f0807d3

.field public static final publisher_share_photo:I = 0x7f0805b0

.field public static final publisher_status:I = 0x7f0805b1

.field public static final publisher_write_post:I = 0x7f0805b2

.field public static final pull_to_refresh_last_updated_date:I = 0x7f080495

.field public static final pull_to_refresh_last_updated_time_ago:I = 0x7f080496

.field public static final pull_to_refresh_pull_label:I = 0x7f080491

.field public static final pull_to_refresh_push_label:I = 0x7f080492

.field public static final pull_to_refresh_refreshing_label:I = 0x7f080494

.field public static final pull_to_refresh_release_label:I = 0x7f080493

.field public static final qr_code_accessibility_code_description:I = 0x7f081158

.field public static final qr_code_error_detected:I = 0x7f08115c

.field public static final qr_code_error_loading_camera:I = 0x7f08115d

.field public static final qr_code_error_not_facebook_code:I = 0x7f08115e

.field public static final qr_code_error_saving:I = 0x7f08115f

.field public static final qr_code_error_sharing:I = 0x7f081160

.field public static final qr_code_error_sharing_file:I = 0x7f081161

.field public static final qr_code_error_write_file:I = 0x7f081162

.field public static final qr_code_import_button:I = 0x7f081159

.field public static final qr_code_login_approve_button:I = 0x7f080085

.field public static final qr_code_login_cancel_button:I = 0x7f080083

.field public static final qr_code_login_fetching_info:I = 0x7f080086

.field public static final qr_code_login_malformed_qr_code:I = 0x7f080084

.field public static final qr_code_login_question:I = 0x7f080087

.field public static final qr_code_login_service_exception:I = 0x7f080088

.field public static final qr_code_login_service_exception_dialog_title:I = 0x7f080089

.field public static final qr_code_login_success:I = 0x7f080081

.field public static final qr_code_login_success_dialog_title:I = 0x7f080082

.field public static final qr_code_login_title:I = 0x7f080080

.field public static final qr_code_msg_saved:I = 0x7f08115a

.field public static final qr_code_or_divider:I = 0x7f08115b

.field public static final qr_code_promo_description:I = 0x7f081157

.field public static final qr_code_promo_title:I = 0x7f081156

.field public static final qr_code_save_button:I = 0x7f081163

.field public static final qr_code_scan_button:I = 0x7f081164

.field public static final qr_code_scan_description:I = 0x7f081165

.field public static final qr_code_select:I = 0x7f081166

.field public static final qr_code_share_button:I = 0x7f081167

.field public static final qr_code_share_description:I = 0x7f081168

.field public static final qr_code_show_button:I = 0x7f081169

.field public static final qr_code_title:I = 0x7f08116a

.field public static final quick_tips_dialog_title:I = 0x7f080155

.field public static final quick_tips_finish_button_label:I = 0x7f080157

.field public static final quick_tips_swipe_down_text:I = 0x7f080156

.field public static final quickcam_nux_release_instructions:I = 0x7f08034a

.field public static final quickcam_nux_swipe_instructions:I = 0x7f080349

.field public static final quickcam_nux_tap_instructions:I = 0x7f080348

.field public static final quickcam_promotional_banner:I = 0x7f08034b

.field public static final rating_section_card_question:I = 0x7f081073

.field public static final rating_section_change_privacy_error:I = 0x7f081075

.field public static final rating_section_fetch_error:I = 0x7f081074

.field public static final rating_section_items_list_end:I = 0x7f081078

.field public static final rating_section_nux_instructions:I = 0x7f081077

.field public static final rating_section_update_privacy_error:I = 0x7f081076

.field public static final reaction_done:I = 0x7f080c58

.field public static final reaction_posting:I = 0x7f080c59

.field public static final reaction_story_posted:I = 0x7f080c5a

.field public static final receipt_payment_id:I = 0x7f0801ce

.field public static final receipt_sent_to:I = 0x7f0801cd

.field public static final recent_stickers:I = 0x7f0803f1

.field public static final recents_tab_description:I = 0x7f0801e1

.field public static final recipient_picker_search_hint:I = 0x7f080d28

.field public static final recording_indicator_text:I = 0x7f080272

.field public static final red_alert_desc:I = 0x7f0808f7

.field public static final reflex_debug_preference_apply_layer_snapping:I = 0x7f0800a7

.field public static final reflex_debug_preference_apply_layer_snapping_description:I = 0x7f0800a8

.field public static final reflex_debug_preference_clear_opaque_quads:I = 0x7f0800b3

.field public static final reflex_debug_preference_clear_opaque_quads_description:I = 0x7f0800b4

.field public static final reflex_debug_preference_clear_to_red:I = 0x7f0800c3

.field public static final reflex_debug_preference_clear_to_red_description:I = 0x7f0800c4

.field public static final reflex_debug_preference_compose_opacity:I = 0x7f0800c1

.field public static final reflex_debug_preference_compose_opacity_description:I = 0x7f0800c2

.field public static final reflex_debug_preference_content_expiration:I = 0x7f0800ad

.field public static final reflex_debug_preference_content_expiration_description:I = 0x7f0800ae

.field public static final reflex_debug_preference_debug_quad_borders:I = 0x7f0800b5

.field public static final reflex_debug_preference_debug_quad_borders_description:I = 0x7f0800b6

.field public static final reflex_debug_preference_debug_repaint_borders:I = 0x7f0800b7

.field public static final reflex_debug_preference_debug_repaint_borders_description:I = 0x7f0800b8

.field public static final reflex_debug_preference_dirty_method:I = 0x7f0800a5

.field public static final reflex_debug_preference_dirty_method_description:I = 0x7f0800a6

.field public static final reflex_debug_preference_explode_layers:I = 0x7f0800bb

.field public static final reflex_debug_preference_explode_layers_description:I = 0x7f0800bc

.field public static final reflex_debug_preference_explode_layers_scale:I = 0x7f0800bd

.field public static final reflex_debug_preference_explode_layers_scale_description:I = 0x7f0800be

.field public static final reflex_debug_preference_explode_layers_transition:I = 0x7f0800bf

.field public static final reflex_debug_preference_explode_layers_transition_description:I = 0x7f0800c0

.field public static final reflex_debug_preference_input_move_off_ui_thread:I = 0x7f0800b1

.field public static final reflex_debug_preference_input_move_off_ui_thread_description:I = 0x7f0800b2

.field public static final reflex_debug_preference_layer_overlay_mode:I = 0x7f0800b9

.field public static final reflex_debug_preference_layer_overlay_mode_description:I = 0x7f0800ba

.field public static final reflex_debug_preference_render_clip_to_visible:I = 0x7f0800a1

.field public static final reflex_debug_preference_render_clip_to_visible_description:I = 0x7f0800a2

.field public static final reflex_debug_preference_render_method:I = 0x7f08009b

.field public static final reflex_debug_preference_render_method_description:I = 0x7f08009c

.field public static final reflex_debug_preference_render_pooling_mode:I = 0x7f08009f

.field public static final reflex_debug_preference_render_pooling_mode_description:I = 0x7f0800a0

.field public static final reflex_debug_preference_render_subcontent_clipping:I = 0x7f0800a3

.field public static final reflex_debug_preference_render_subcontent_clipping_description:I = 0x7f0800a4

.field public static final reflex_debug_preference_render_use_pooling:I = 0x7f08009d

.field public static final reflex_debug_preference_render_use_pooling_description:I = 0x7f08009e

.field public static final reflex_debug_preference_show_framerate_monitor:I = 0x7f0800af

.field public static final reflex_debug_preference_show_framerate_monitor_description:I = 0x7f0800b0

.field public static final reflex_debug_preference_tile_height_bits:I = 0x7f0800ab

.field public static final reflex_debug_preference_tile_height_bits_description:I = 0x7f0800ac

.field public static final reflex_debug_preference_tile_width_bits:I = 0x7f0800a9

.field public static final reflex_debug_preference_tile_width_bits_description:I = 0x7f0800aa

.field public static final reflex_debug_preferences_expert_title:I = 0x7f08009a

.field public static final reflex_debug_preferences_title:I = 0x7f080099

.field public static final registration_step_birthday_description:I = 0x7f081185

.field public static final registration_step_birthday_error:I = 0x7f081187

.field public static final registration_step_birthday_hint:I = 0x7f081186

.field public static final registration_step_birthday_title:I = 0x7f081184

.field public static final registration_step_contact_email_description:I = 0x7f08117c

.field public static final registration_step_contact_email_error:I = 0x7f08117e

.field public static final registration_step_contact_email_input:I = 0x7f08117d

.field public static final registration_step_contact_email_switch:I = 0x7f08117f

.field public static final registration_step_contact_email_title:I = 0x7f08117b

.field public static final registration_step_contact_phone_description:I = 0x7f081177

.field public static final registration_step_contact_phone_error:I = 0x7f081179

.field public static final registration_step_contact_phone_input:I = 0x7f081178

.field public static final registration_step_contact_phone_switch:I = 0x7f08117a

.field public static final registration_step_contact_phone_title:I = 0x7f081176

.field public static final registration_step_create_account_error:I = 0x7f08118d

.field public static final registration_step_create_account_success:I = 0x7f08118e

.field public static final registration_step_create_account_title:I = 0x7f08118c

.field public static final registration_step_gender_description:I = 0x7f081189

.field public static final registration_step_gender_female:I = 0x7f08118b

.field public static final registration_step_gender_male:I = 0x7f08118a

.field public static final registration_step_gender_title:I = 0x7f081188

.field public static final registration_step_name_description:I = 0x7f081174

.field public static final registration_step_name_empty_field:I = 0x7f081175

.field public static final registration_step_name_field_first_name:I = 0x7f081172

.field public static final registration_step_name_field_last_name:I = 0x7f081173

.field public static final registration_step_name_title:I = 0x7f081171

.field public static final registration_step_password_description:I = 0x7f081182

.field public static final registration_step_password_input_hint:I = 0x7f081181

.field public static final registration_step_password_len_error:I = 0x7f081183

.field public static final registration_step_password_title:I = 0x7f081180

.field public static final registration_terms_dialog_title:I = 0x7f08116c

.field public static final registration_terms_text_1:I = 0x7f08116d

.field public static final registration_verifying:I = 0x7f08116b

.field public static final registration_words_cookie_use:I = 0x7f081170

.field public static final registration_words_fb_terms:I = 0x7f08116e

.field public static final registration_words_privacy_policy:I = 0x7f08116f

.field public static final related_video_like_content_description:I = 0x7f080c96

.field public static final related_video_play_button_content_description:I = 0x7f080c97

.field public static final related_videos_carousel_collapse:I = 0x7f080c9a

.field public static final related_videos_carousel_expand:I = 0x7f080c99

.field public static final related_videos_title:I = 0x7f080c98

.field public static final remove_friend_button_description:I = 0x7f080185

.field public static final remove_from_favorites_description:I = 0x7f0801d7

.field public static final remove_member_action:I = 0x7f0803f8

.field public static final remove_member_progress:I = 0x7f0803f9

.field public static final remove_members_button_text:I = 0x7f08024f

.field public static final remove_people_action:I = 0x7f0802ed

.field public static final report_a_problem:I = 0x7f080a33

.field public static final report_error_button:I = 0x7f08003c

.field public static final request_sent:I = 0x7f08016e

.field public static final requests_plus_button_content_description:I = 0x7f080ee7

.field public static final requests_request_canceled:I = 0x7f080182

.field public static final requests_suggested_by:I = 0x7f080184

.field public static final requests_suggestion_ignored:I = 0x7f080183

.field public static final research_poll_feed_unit_title:I = 0x7f08005f

.field public static final reshare_education_title:I = 0x7f0809d8

.field public static final respond:I = 0x7f08016a

.field public static final retry_send_delete_button:I = 0x7f080270

.field public static final retry_send_heading:I = 0x7f08026e

.field public static final retry_send_positive_button:I = 0x7f08026f

.field public static final review_action:I = 0x7f0807bd

.field public static final review_action_more:I = 0x7f0807be

.field public static final review_button:I = 0x7f0807ae

.field public static final review_delete_confirm:I = 0x7f0807b4

.field public static final review_delete_failure:I = 0x7f0807b5

.field public static final review_edit_failure:I = 0x7f0807b3

.field public static final review_edit_prompt:I = 0x7f0807b2

.field public static final review_exit_dialog_cancel:I = 0x7f08080d

.field public static final review_exit_dialog_message:I = 0x7f0807fe

.field public static final review_exit_dialog_ok:I = 0x7f08080f

.field public static final review_exit_dialog_title:I = 0x7f080805

.field public static final review_exit_edit_dialog_message:I = 0x7f08080b

.field public static final review_exit_edit_dialog_title:I = 0x7f080806

.field public static final review_fetch_data_error:I = 0x7f0807c1

.field public static final review_fragment_title:I = 0x7f0807c0

.field public static final review_list_empty:I = 0x7f0807c3

.field public static final review_post_failure:I = 0x7f0807b1

.field public static final review_post_progress:I = 0x7f0807af

.field public static final review_post_success:I = 0x7f0807b0

.field public static final review_privacy_selector_text:I = 0x7f0807bf

.field public static final review_prompt:I = 0x7f0807ac

.field public static final review_prompt_short:I = 0x7f0807ad

.field public static final review_report:I = 0x7f0807c2

.field public static final review_section_title:I = 0x7f0807b8

.field public static final review_section_title_from_everyone:I = 0x7f0807bc

.field public static final review_section_title_from_friends:I = 0x7f0807bb

.field public static final review_section_title_ratings_summary:I = 0x7f0807b9

.field public static final review_section_title_viewer_review:I = 0x7f0807ba

.field public static final review_write_a_review:I = 0x7f0807b6

.field public static final review_write_a_review_ellipsized:I = 0x7f0807b7

.field public static final ridge_identifying_accessibility_string:I = 0x7f080899

.field public static final ridge_interstitial_explanation:I = 0x7f080885

.field public static final ridge_interstitial_title:I = 0x7f080884

.field public static final ridge_media_explanation_description:I = 0x7f080889

.field public static final ridge_media_explanation_title:I = 0x7f080888

.field public static final ridge_megaphone_more_text:I = 0x7f080886

.field public static final ridge_megaphone_no_text:I = 0x7f080887

.field public static final ridge_nux_bubble_text:I = 0x7f080895

.field public static final ridge_post_optin_nux_bubble_text:I = 0x7f080896

.field public static final ridge_privacy_explanation_description:I = 0x7f08088d

.field public static final ridge_privacy_explanation_title:I = 0x7f08088c

.field public static final ridge_ptr_commerical_text:I = 0x7f08088f

.field public static final ridge_ptr_disabled:I = 0x7f080891

.field public static final ridge_ptr_failure_text:I = 0x7f08088e

.field public static final ridge_ptr_internal_error:I = 0x7f080890

.field public static final ridge_ptr_loading:I = 0x7f080892

.field public static final ridge_ptr_pull:I = 0x7f080893

.field public static final ridge_ptr_release:I = 0x7f080894

.field public static final ridge_turned_off_accessibility_string:I = 0x7f080897

.field public static final ridge_turned_on_accessibility_string:I = 0x7f080898

.field public static final ridge_working_explanation_description:I = 0x7f08088b

.field public static final ridge_working_explanation_title:I = 0x7f08088a

.field public static final rotate_photo:I = 0x7f080789

.field public static final save:I = 0x7f080a39

.field public static final save_as_draft:I = 0x7f080855

.field public static final save_button_default:I = 0x7f080ca0

.field public static final save_draft:I = 0x7f080853

.field public static final save_favorites_button_label:I = 0x7f080281

.field public static final save_favorites_failed:I = 0x7f080284

.field public static final save_place:I = 0x7f0805f9

.field public static final save_tagging:I = 0x7f080652

.field public static final saved_archive_failed:I = 0x7f0811b7

.field public static final saved_context_menu_archive_title:I = 0x7f080c47

.field public static final saved_context_menu_delete_title:I = 0x7f080c4e

.field public static final saved_context_menu_review_subtitle:I = 0x7f080c4d

.field public static final saved_context_menu_review_title:I = 0x7f080c4b

.field public static final saved_context_menu_share_title:I = 0x7f080c49

.field public static final saved_context_menu_unarchive_title:I = 0x7f080c48

.field public static final saved_context_menu_view_post_subtitle:I = 0x7f080c4c

.field public static final saved_context_menu_view_post_title:I = 0x7f080c4a

.field public static final saved_dashboard_action_button_label:I = 0x7f0811be

.field public static final saved_dashboard_interstitial_getting_back_dashboard:I = 0x7f0811c1

.field public static final saved_dashboard_interstitial_saved_usage:I = 0x7f0811c0

.field public static final saved_dashboard_interstitial_title:I = 0x7f0811bf

.field public static final saved_dashboard_load_more_failed:I = 0x7f0811bd

.field public static final saved_delete_failed:I = 0x7f0811b9

.field public static final saved_filter_action_cancel_text:I = 0x7f0811a0

.field public static final saved_filter_action_clear_text:I = 0x7f0811a1

.field public static final saved_filter_by_title:I = 0x7f08119f

.field public static final saved_filter_by_type_title:I = 0x7f08119e

.field public static final saved_filter_chevron_accessibility_text:I = 0x7f0811a2

.field public static final saved_fragment_title:I = 0x7f08119d

.field public static final saved_item_deleted:I = 0x7f0811b6

.field public static final saved_items_null_state_message_for_all:I = 0x7f0811a4

.field public static final saved_items_null_state_message_for_archived:I = 0x7f0811ac

.field public static final saved_items_null_state_message_for_book:I = 0x7f0811a8

.field public static final saved_items_null_state_message_for_event:I = 0x7f0811ab

.field public static final saved_items_null_state_message_for_link:I = 0x7f0811a5

.field public static final saved_items_null_state_message_for_movie:I = 0x7f0811a9

.field public static final saved_items_null_state_message_for_music:I = 0x7f0811a7

.field public static final saved_items_null_state_message_for_place:I = 0x7f0811a6

.field public static final saved_items_null_state_message_for_tv_show:I = 0x7f0811aa

.field public static final saved_list_load_failed:I = 0x7f0811ba

.field public static final saved_moved_back_to_collection_for_all:I = 0x7f0811ad

.field public static final saved_moved_back_to_collection_for_book:I = 0x7f0811b1

.field public static final saved_moved_back_to_collection_for_event:I = 0x7f0811b4

.field public static final saved_moved_back_to_collection_for_link:I = 0x7f0811ae

.field public static final saved_moved_back_to_collection_for_movie:I = 0x7f0811b2

.field public static final saved_moved_back_to_collection_for_music:I = 0x7f0811b0

.field public static final saved_moved_back_to_collection_for_place:I = 0x7f0811af

.field public static final saved_moved_back_to_collection_for_tv_show:I = 0x7f0811b3

.field public static final saved_moved_to_archive:I = 0x7f0811b5

.field public static final saved_open_save_dashboard_failed:I = 0x7f0811bc

.field public static final saved_place:I = 0x7f0805fa

.field public static final saved_saved_confirmation:I = 0x7f080c44

.field public static final saved_saved_item:I = 0x7f080c46

.field public static final saved_unarchive_failed:I = 0x7f0811b8

.field public static final saved_undo_button_label:I = 0x7f0811a3

.field public static final saved_undo_failed:I = 0x7f0811bb

.field public static final saved_unsaved_confirmation:I = 0x7f080c45

.field public static final schedule_post_reminder:I = 0x7f08084d

.field public static final schedule_post_succeed:I = 0x7f080850

.field public static final schedule_post_succeed_refresh:I = 0x7f080851

.field public static final schedule_post_too_early:I = 0x7f08084a

.field public static final schedule_post_too_late:I = 0x7f08084b

.field public static final search_all_tab:I = 0x7f080e51

.field public static final search_apps_tab:I = 0x7f080e52

.field public static final search_events_tab:I = 0x7f080e53

.field public static final search_groups_tab:I = 0x7f080e54

.field public static final search_icon_name:I = 0x7f080279

.field public static final search_messages_loading:I = 0x7f0803d8

.field public static final search_pages_tab:I = 0x7f080e55

.field public static final search_people_tab:I = 0x7f080e56

.field public static final search_photo_pivots_related_photos:I = 0x7f080c73

.field public static final search_places_tab:I = 0x7f080e57

.field public static final search_posts_tab:I = 0x7f080e58

.field public static final search_title_bar:I = 0x7f0803d9

.field public static final search_top_tab:I = 0x7f080e59

.field public static final search_view_load_more_messages:I = 0x7f0803db

.field public static final search_view_load_more_threads:I = 0x7f0803da

.field public static final see_all:I = 0x7f080f2d

.field public static final see_all_new_contacts_button_label:I = 0x7f080287

.field public static final see_more_emoji_button_description:I = 0x7f0801e0

.field public static final select_external_photo:I = 0x7f08078b

.field public static final select_external_video:I = 0x7f08078c

.field public static final select_page:I = 0x7f0807e5

.field public static final selected:I = 0x7f080780

.field public static final selected_feed_filter:I = 0x7f080f3f

.field public static final self_update_button_title:I = 0x7f080c83

.field public static final send_button_description:I = 0x7f0801d4

.field public static final send_code_again:I = 0x7f080f96

.field public static final send_failed_error:I = 0x7f080292

.field public static final send_money_change_card:I = 0x7f0801d0

.field public static final send_money_no_value_entered:I = 0x7f0801d1

.field public static final send_si_error_title:I = 0x7f080293

.field public static final sent_receipt_pending_message:I = 0x7f0801cf

.field public static final sent_you_message:I = 0x7f0803e0

.field public static final sentry_block_title:I = 0x7f080052

.field public static final set_coverphoto_failed:I = 0x7f080776

.field public static final set_photo:I = 0x7f0802e9

.field public static final settings_add_card:I = 0x7f0801b7

.field public static final settings_app_requests_title:I = 0x7f080f07

.field public static final settings_button_content_description:I = 0x7f080a75

.field public static final settings_card_info:I = 0x7f0801b8

.field public static final settings_category_dash:I = 0x7f0800d8

.field public static final settings_category_dash_link:I = 0x7f0800d9

.field public static final settings_check_time_0:I = 0x7f080ee8

.field public static final settings_check_time_1:I = 0x7f080ee9

.field public static final settings_check_time_2:I = 0x7f080eea

.field public static final settings_check_time_3:I = 0x7f080eeb

.field public static final settings_check_time_4:I = 0x7f080eec

.field public static final settings_comments_title:I = 0x7f080f04

.field public static final settings_dash:I = 0x7f0800d7

.field public static final settings_event_invites_title:I = 0x7f080eed

.field public static final settings_friend_confirmations_title:I = 0x7f080f02

.field public static final settings_friend_requests_title:I = 0x7f080eee

.field public static final settings_general_settings:I = 0x7f080eef

.field public static final settings_messages_title:I = 0x7f080ef0

.field public static final settings_my_cards_title:I = 0x7f0801b2

.field public static final settings_nearby_friends_title:I = 0x7f080f05

.field public static final settings_notif_groups_title:I = 0x7f080f06

.field public static final settings_notification_advanced:I = 0x7f080ef2

.field public static final settings_notification_settings:I = 0x7f080ef1

.field public static final settings_notifications_active:I = 0x7f080ef3

.field public static final settings_notifications_inactive:I = 0x7f080ef4

.field public static final settings_notifications_title:I = 0x7f080ef5

.field public static final settings_other:I = 0x7f080ef6

.field public static final settings_payments_history_title:I = 0x7f0801b3

.field public static final settings_phone_acquisition:I = 0x7f080dd2

.field public static final settings_phone_acquisition_add:I = 0x7f080dd3

.field public static final settings_photo_tags_title:I = 0x7f080f03

.field public static final settings_pin:I = 0x7f0801b5

.field public static final settings_pin_disabled:I = 0x7f0801bb

.field public static final settings_pin_enabled:I = 0x7f0801ba

.field public static final settings_polling_interval_title:I = 0x7f080ef7

.field public static final settings_primary_card:I = 0x7f0801b9

.field public static final settings_ringtone_summary:I = 0x7f080ef8

.field public static final settings_ringtone_title:I = 0x7f080ef9

.field public static final settings_security_title:I = 0x7f0801b4

.field public static final settings_see_payments_history:I = 0x7f0801b6

.field public static final settings_sticky_sys_tray_notifications_summary:I = 0x7f080eff

.field public static final settings_sticky_sys_tray_notifications_title:I = 0x7f080f00

.field public static final settings_sync_contacts:I = 0x7f080efa

.field public static final settings_tab_description:I = 0x7f0801e4

.field public static final settings_use_led_summary:I = 0x7f080efb

.field public static final settings_use_led_title:I = 0x7f080efc

.field public static final settings_vault_sync:I = 0x7f080726

.field public static final settings_vault_sync_always_on:I = 0x7f080727

.field public static final settings_vault_sync_off:I = 0x7f080729

.field public static final settings_vault_sync_wifi:I = 0x7f080728

.field public static final settings_vibrate_summary:I = 0x7f080efd

.field public static final settings_vibrate_title:I = 0x7f080efe

.field public static final settings_wall_posts_title:I = 0x7f080f01

.field public static final share_choice_button:I = 0x7f080ce0

.field public static final share_choice_header:I = 0x7f080ce5

.field public static final share_choice_post_header:I = 0x7f080ce2

.field public static final share_choice_post_minute_ago:I = 0x7f080ce1

.field public static final share_choice_preview_description:I = 0x7f080ce6

.field public static final share_choice_tag_for:I = 0x7f080ce3

.field public static final share_choice_tag_from:I = 0x7f080ce4

.field public static final share_composer_initial_title:I = 0x7f0807d4

.field public static final share_composer_preview_failed:I = 0x7f080479

.field public static final share_composer_prompt:I = 0x7f0807dc

.field public static final share_composer_title_album:I = 0x7f0807d7

.field public static final share_composer_title_link:I = 0x7f0807d5

.field public static final share_composer_title_page:I = 0x7f0807da

.field public static final share_composer_title_photo:I = 0x7f0807d6

.field public static final share_composer_title_post:I = 0x7f0807db

.field public static final share_composer_title_status:I = 0x7f0807d9

.field public static final share_composer_title_video:I = 0x7f0807d8

.field public static final share_property:I = 0x7f0802d8

.field public static final share_succeed:I = 0x7f0807e6

.field public static final share_via:I = 0x7f0802d9

.field public static final shared_album_privacy_contributors_only:I = 0x7f081149

.field public static final shared_album_privacy_facebook:I = 0x7f081147

.field public static final shared_album_privacy_friends_of_contributors:I = 0x7f081148

.field public static final shared_album_privacy_public:I = 0x7f081146

.field public static final short_active_x_days_ago:I = 0x7f080384

.field public static final short_active_x_days_ago_uppercase:I = 0x7f080392

.field public static final short_active_x_hours_ago:I = 0x7f080383

.field public static final short_active_x_hours_ago_uppercase:I = 0x7f080391

.field public static final short_active_x_mins_ago:I = 0x7f080382

.field public static final short_active_x_mins_ago_uppercase:I = 0x7f080390

.field public static final shortcut_banner_content:I = 0x7f080234

.field public static final shortcut_banner_title:I = 0x7f080233

.field public static final show_more_string:I = 0x7f080955

.field public static final sign_in_title:I = 0x7f080016

.field public static final simple_picker_video_too_large_toast:I = 0x7f0807a3

.field public static final simple_picker_video_too_small_toast:I = 0x7f0807a4

.field public static final simplepicker_done_button_text:I = 0x7f080797

.field public static final simplepicker_next_button_text:I = 0x7f080798

.field public static final simplepicker_no_photo_text:I = 0x7f080799

.field public static final simultaneous_video_error:I = 0x7f080425

.field public static final single_friend_selector_title:I = 0x7f080c7b

.field public static final single_sign_on_failed_t1118578:I = 0x7f080ed8

.field public static final single_sign_on_failed_t1732910:I = 0x7f080ed9

.field public static final site_error:I = 0x7f080e8a

.field public static final skip:I = 0x7f0809bc

.field public static final skip_step:I = 0x7f080c4f

.field public static final skip_tagging:I = 0x7f080651

.field public static final snowflake_metadata_text:I = 0x7f080648

.field public static final snowflake_owner_text:I = 0x7f080647

.field public static final source_from_email:I = 0x7f080366

.field public static final source_from_messenger:I = 0x7f080363

.field public static final source_from_mobile:I = 0x7f080364

.field public static final source_from_quickcam_photo:I = 0x7f080368

.field public static final source_from_quickcam_video:I = 0x7f080367

.field public static final source_from_web:I = 0x7f080365

.field public static final speakerphone_off_button_description:I = 0x7f0801d9

.field public static final speakerphone_on_button_description:I = 0x7f0801da

.field public static final ssl_error:I = 0x7f080e8b

.field public static final ssl_error_beta:I = 0x7f0809fc

.field public static final ssl_error_webview:I = 0x7f0809fb

.field public static final start_screen_sso_text:I = 0x7f080071

.field public static final start_screen_sso_text_not_you_link:I = 0x7f080072

.field public static final status_post_fail_text:I = 0x7f080f0e

.field public static final status_post_fail_ticker:I = 0x7f080f10

.field public static final status_post_fail_title:I = 0x7f080f0f

.field public static final status_tagged_at:I = 0x7f08086e

.field public static final status_tagged_title:I = 0x7f080870

.field public static final status_tagged_with:I = 0x7f08086d

.field public static final status_tagged_with_at:I = 0x7f08086f

.field public static final status_tagged_x_and_others:I = 0x7f08086c

.field public static final status_tagged_x_and_y:I = 0x7f08086b

.field public static final status_update:I = 0x7f0807eb

.field public static final status_video_added_tagged_title:I = 0x7f080872

.field public static final status_video_added_title:I = 0x7f080871

.field public static final sticker:I = 0x7f0803f0

.field public static final sticker_pack_not_available:I = 0x7f080214

.field public static final sticker_popup_empty:I = 0x7f0802a5

.field public static final sticker_popup_pack_name_desc:I = 0x7f0802c2

.field public static final sticker_popup_store_desc:I = 0x7f0802b4

.field public static final sticker_search_empty_result:I = 0x7f0802c4

.field public static final sticker_store_category_empty:I = 0x7f0802aa

.field public static final sticker_store_connection_error:I = 0x7f0802b3

.field public static final sticker_store_download:I = 0x7f0802ad

.field public static final sticker_store_downloaded:I = 0x7f0802af

.field public static final sticker_store_downloading:I = 0x7f0802ae

.field public static final sticker_store_price_free:I = 0x7f0802ab

.field public static final sticker_store_remove_pack:I = 0x7f0802ac

.field public static final sticker_store_tab_available:I = 0x7f0802a8

.field public static final sticker_store_tab_featured:I = 0x7f0802a7

.field public static final sticker_store_tab_owned:I = 0x7f0802a9

.field public static final sticker_store_title:I = 0x7f0802a6

.field public static final sticker_store_title_done:I = 0x7f0802b2

.field public static final sticker_store_title_edit:I = 0x7f0802b1

.field public static final sticker_store_view:I = 0x7f0802b0

.field public static final sticker_tags_search_bar_hint:I = 0x7f0802c3

.field public static final sticky_guardrail_accept:I = 0x7f0808cf

.field public static final sticky_guardrail_body:I = 0x7f0808ce

.field public static final sticky_guardrail_close_content_descriptor:I = 0x7f0808d1

.field public static final sticky_guardrail_decline:I = 0x7f0808d0

.field public static final sticky_guardrail_title:I = 0x7f0808cd

.field public static final storage_not_writable:I = 0x7f080718

.field public static final store_tab_page_button:I = 0x7f0802b7

.field public static final store_tab_page_description:I = 0x7f0802b6

.field public static final store_tab_page_headline:I = 0x7f0802b5

.field public static final story_insights_actions:I = 0x7f0806d6

.field public static final story_insights_clicks:I = 0x7f0806d8

.field public static final story_insights_comment:I = 0x7f0806e0

.field public static final story_insights_like:I = 0x7f0806df

.field public static final story_insights_link_clicks:I = 0x7f0806db

.field public static final story_insights_organic_reach:I = 0x7f0806d9

.field public static final story_insights_other_clicks:I = 0x7f0806dc

.field public static final story_insights_paid_reach:I = 0x7f0806da

.field public static final story_insights_photo_views:I = 0x7f0806dd

.field public static final story_insights_reach:I = 0x7f0806d7

.field public static final story_insights_share:I = 0x7f0806e1

.field public static final story_insights_video_plays:I = 0x7f0806de

.field public static final story_promotion_account:I = 0x7f0806fb

.field public static final story_promotion_age:I = 0x7f08070d

.field public static final story_promotion_audience:I = 0x7f0806fc

.field public static final story_promotion_audience_fof:I = 0x7f0806fd

.field public static final story_promotion_audience_ncpp:I = 0x7f0806fe

.field public static final story_promotion_bm_no_ad_account:I = 0x7f08070c

.field public static final story_promotion_cancel_promotion:I = 0x7f0806f1

.field public static final story_promotion_complete:I = 0x7f0805cd

.field public static final story_promotion_creating_campaign:I = 0x7f0806f3

.field public static final story_promotion_error_title:I = 0x7f0806f5

.field public static final story_promotion_estimated_reach:I = 0x7f0806ed

.field public static final story_promotion_estimated_reach_range:I = 0x7f0806ee

.field public static final story_promotion_failed:I = 0x7f0805ce

.field public static final story_promotion_gender:I = 0x7f080708

.field public static final story_promotion_gender_both:I = 0x7f080709

.field public static final story_promotion_gender_female:I = 0x7f08070a

.field public static final story_promotion_gender_male:I = 0x7f08070b

.field public static final story_promotion_guidelines:I = 0x7f0806f8

.field public static final story_promotion_header_inactive:I = 0x7f0806e6

.field public static final story_promotion_header_pending:I = 0x7f0806e7

.field public static final story_promotion_header_rejected:I = 0x7f0806e8

.field public static final story_promotion_legal_terms:I = 0x7f0806f7

.field public static final story_promotion_location:I = 0x7f0806ff

.field public static final story_promotion_location_selector_cities:I = 0x7f080707

.field public static final story_promotion_location_selector_countries:I = 0x7f080705

.field public static final story_promotion_location_selector_message:I = 0x7f080704

.field public static final story_promotion_location_selector_regions:I = 0x7f080706

.field public static final story_promotion_location_selector_title:I = 0x7f080703

.field public static final story_promotion_max_budget:I = 0x7f0806eb

.field public static final story_promotion_min_budget_error_title:I = 0x7f0806ec

.field public static final story_promotion_paid_reach:I = 0x7f0806e9

.field public static final story_promotion_paused:I = 0x7f0805cf

.field public static final story_promotion_payment_method:I = 0x7f0806fa

.field public static final story_promotion_promoted:I = 0x7f0805d0

.field public static final story_promotion_promoted_for:I = 0x7f0806e4

.field public static final story_promotion_rejected:I = 0x7f0806e5

.field public static final story_promotion_resume_promotion:I = 0x7f0806f2

.field public static final story_promotion_selector_list_empty:I = 0x7f080702

.field public static final story_promotion_selector_list_hint:I = 0x7f080701

.field public static final story_promotion_selector_search_hint:I = 0x7f080700

.field public static final story_promotion_spent:I = 0x7f0806ea

.field public static final story_promotion_stop_promotion:I = 0x7f0806f0

.field public static final story_promotion_succeed:I = 0x7f0806f9

.field public static final story_promotion_update_budget:I = 0x7f0806ef

.field public static final story_promotion_updating_campaign:I = 0x7f0806f4

.field public static final story_promotion_warning:I = 0x7f0806f6

.field public static final stream_add_comment_error:I = 0x7f080f13

.field public static final stream_adding_comment:I = 0x7f080f14

.field public static final stream_comment:I = 0x7f080f15

.field public static final stream_comment_hint:I = 0x7f080f16

.field public static final stream_logging_out:I = 0x7f080f18

.field public static final stream_logout_message:I = 0x7f080f1d

.field public static final stream_logout_message_with_pending_stories:I = 0x7f080f1e

.field public static final stream_logout_question:I = 0x7f080f19

.field public static final stream_optional_comment_hint:I = 0x7f080f17

.field public static final stream_publish_connection_error:I = 0x7f080f1a

.field public static final stream_publishing:I = 0x7f080f1b

.field public static final stream_quick_share_with_everyone:I = 0x7f0807e4

.field public static final stream_quick_share_with_friends:I = 0x7f0807e3

.field public static final stream_refresh:I = 0x7f080f1c

.field public static final stream_share:I = 0x7f0806d0

.field public static final stream_share_hint:I = 0x7f0806cf

.field public static final stream_share_to_message:I = 0x7f0807e1

.field public static final stream_share_to_timelines:I = 0x7f0807e2

.field public static final suggested_video_likes_count_k:I = 0x7f080c95

.field public static final suggested_video_views:I = 0x7f080c93

.field public static final suggested_video_views_count_k:I = 0x7f080c94

.field public static final suggested_videos_title:I = 0x7f080c92

.field public static final switch_camera:I = 0x7f080346

.field public static final sync:I = 0x7f080f1f

.field public static final sync_contacts_choice_dont_sync:I = 0x7f080f20

.field public static final sync_contacts_choice_dont_sync_description:I = 0x7f080f21

.field public static final sync_contacts_choice_remove_sync:I = 0x7f080f22

.field public static final sync_contacts_choice_remove_sync_description:I = 0x7f080f23

.field public static final sync_contacts_choice_sync_all:I = 0x7f080f24

.field public static final sync_contacts_choice_sync_all_description:I = 0x7f080f25

.field public static final sync_contacts_choice_sync_existing:I = 0x7f080f26

.field public static final sync_contacts_choice_sync_existing_description:I = 0x7f080f27

.field public static final sync_contacts_footer:I = 0x7f080f28

.field public static final sync_contacts_header:I = 0x7f080f29

.field public static final sync_settings_go_to_settings_button:I = 0x7f080361

.field public static final sync_settings_go_to_settings_button_caps:I = 0x7f080362

.field public static final sync_settings_warning_setting_background_data:I = 0x7f080360

.field public static final sync_settings_warning_template:I = 0x7f08035f

.field public static final system_language:I = 0x7f08113c

.field public static final system_notifications_music_prompt_dialog_text:I = 0x7f080161

.field public static final system_notifications_music_prompt_dialog_title:I = 0x7f080160

.field public static final system_notifications_music_settings_toast_message:I = 0x7f08015f

.field public static final system_notifications_prompt_dialog_text:I = 0x7f08015e

.field public static final system_notifications_prompt_dialog_title:I = 0x7f08015d

.field public static final system_notifications_prompt_finish_button_label:I = 0x7f080162

.field public static final system_notifications_settings_toast_message:I = 0x7f08015c

.field public static final tab_badge_count_more:I = 0x7f08017a

.field public static final tab_title_bookmarks:I = 0x7f08017e

.field public static final tab_title_messages:I = 0x7f08017c

.field public static final tab_title_nearby:I = 0x7f08017f

.field public static final tab_title_news_feed:I = 0x7f08017b

.field public static final tab_title_notifications:I = 0x7f08017d

.field public static final tab_title_unread_messages:I = 0x7f080181

.field public static final tab_title_unread_messages_ten_or_more:I = 0x7f080180

.field public static final tag_expansion_education_action_link_name:I = 0x7f0809da

.field public static final tag_expansion_education_title:I = 0x7f0809d9

.field public static final tag_friends:I = 0x7f0806ad

.field public static final tag_more_friends:I = 0x7f080650

.field public static final target_closed_group:I = 0x7f08083d

.field public static final target_friend_timeline:I = 0x7f080833

.field public static final target_friend_timeline_selected:I = 0x7f080834

.field public static final target_group:I = 0x7f080835

.field public static final target_group_selected:I = 0x7f080836

.field public static final target_in_group:I = 0x7f08083b

.field public static final target_other_page_selected:I = 0x7f08083c

.field public static final target_own_page:I = 0x7f080837

.field public static final target_own_page_selected:I = 0x7f080838

.field public static final target_own_timeline:I = 0x7f080832

.field public static final target_secret_group:I = 0x7f08083e

.field public static final terms_and_policies:I = 0x7f080ee0

.field public static final terms_and_privacy:I = 0x7f080a32

.field public static final terms_of_service:I = 0x7f080105

.field public static final thread_add_pin_confirm_msg:I = 0x7f08026a

.field public static final thread_add_pin_confirm_ok_button:I = 0x7f08026b

.field public static final thread_add_pin_confirm_title:I = 0x7f080269

.field public static final thread_add_pin_progress:I = 0x7f08026c

.field public static final thread_context_menu_archive_conversation:I = 0x7f080204

.field public static final thread_context_menu_create_shortcut:I = 0x7f080207

.field public static final thread_context_menu_delete_conversation:I = 0x7f080203

.field public static final thread_context_menu_group_settings:I = 0x7f08020b

.field public static final thread_context_menu_mark_as_read:I = 0x7f080208

.field public static final thread_context_menu_mark_as_unread:I = 0x7f080209

.field public static final thread_context_menu_mark_conversation_as_spam:I = 0x7f080205

.field public static final thread_context_menu_pin_thread:I = 0x7f08020a

.field public static final thread_context_menu_show_chat_head:I = 0x7f080206

.field public static final thread_context_menu_title:I = 0x7f080202

.field public static final thread_date_full:I = 0x7f08039b

.field public static final thread_date_short:I = 0x7f08039a

.field public static final thread_date_short_day_of_week:I = 0x7f080399

.field public static final thread_delete_confirm_msg:I = 0x7f080262

.field public static final thread_delete_confirm_ok_button:I = 0x7f080263

.field public static final thread_delete_confirm_title:I = 0x7f080261

.field public static final thread_delete_progress:I = 0x7f080264

.field public static final thread_icon_picker_progress:I = 0x7f08033b

.field public static final thread_leave_confirm_msg:I = 0x7f080266

.field public static final thread_leave_confirm_ok_button:I = 0x7f080267

.field public static final thread_leave_confirm_title:I = 0x7f080265

.field public static final thread_leave_progress:I = 0x7f080268

.field public static final thread_list_contacts_button_description:I = 0x7f0801f8

.field public static final thread_list_empty_title:I = 0x7f080358

.field public static final thread_list_loading:I = 0x7f0801fa

.field public static final thread_list_new_group_button:I = 0x7f0801ff

.field public static final thread_list_new_message_button:I = 0x7f0801fe

.field public static final thread_list_no_threads:I = 0x7f0801fd

.field public static final thread_list_shortcut_created:I = 0x7f080201

.field public static final thread_list_snippet_with_short_name:I = 0x7f080200

.field public static final thread_list_start_conversation_text:I = 0x7f0801fc

.field public static final thread_list_title:I = 0x7f0801f7

.field public static final thread_list_view_updating:I = 0x7f080221

.field public static final thread_loading:I = 0x7f080217

.field public static final thread_name_dialog_hint:I = 0x7f080338

.field public static final thread_name_dialog_remove_button:I = 0x7f080337

.field public static final thread_name_dialog_set_button:I = 0x7f080336

.field public static final thread_name_n_more:I = 0x7f080216

.field public static final thread_name_with_only_you:I = 0x7f080215

.field public static final thread_new_conversation_title:I = 0x7f0801fb

.field public static final thread_no_messages_start_conversation:I = 0x7f080219

.field public static final thread_no_messages_start_conversation_on_messenger:I = 0x7f08021a

.field public static final thread_no_updates:I = 0x7f080218

.field public static final thread_notifications_dialog_title:I = 0x7f08025c

.field public static final thread_notifications_global_disabled:I = 0x7f08025d

.field public static final thread_start_thread_button:I = 0x7f0801f9

.field public static final thread_view_facebook_app:I = 0x7f08021d

.field public static final thread_view_forward_button_content_description:I = 0x7f08021f

.field public static final thread_view_load_more:I = 0x7f08021b

.field public static final thread_view_message_send_error:I = 0x7f08021e

.field public static final thread_view_messenger_app:I = 0x7f08021c

.field public static final thread_view_saving_progress:I = 0x7f080250

.field public static final thread_view_set_thread_name_hint:I = 0x7f080259

.field public static final time_date:I = 0x7f080619

.field public static final time_happening_now:I = 0x7f08061a

.field public static final time_hours_ago_many:I = 0x7f08061d

.field public static final time_hours_ago_many_short:I = 0x7f08061e

.field public static final time_hours_ago_one:I = 0x7f08061b

.field public static final time_hours_ago_one_short:I = 0x7f08061c

.field public static final time_in_x_hours_many:I = 0x7f080624

.field public static final time_in_x_hours_one:I = 0x7f080623

.field public static final time_in_x_minutes_many:I = 0x7f080626

.field public static final time_in_x_minutes_one:I = 0x7f080625

.field public static final time_just_now:I = 0x7f080627

.field public static final time_minutes_ago_many:I = 0x7f080620

.field public static final time_minutes_ago_many_short:I = 0x7f080622

.field public static final time_minutes_ago_one:I = 0x7f08061f

.field public static final time_minutes_ago_one_short:I = 0x7f080621

.field public static final time_ongoing:I = 0x7f080628

.field public static final time_this_month:I = 0x7f080629

.field public static final time_this_week:I = 0x7f08062a

.field public static final time_today:I = 0x7f08062b

.field public static final time_today_at:I = 0x7f08062c

.field public static final time_tomorrow:I = 0x7f08062d

.field public static final time_tomorrow_at:I = 0x7f08062e

.field public static final time_tomorrow_at_lower:I = 0x7f08062f

.field public static final time_week_day_at_time:I = 0x7f080630

.field public static final time_yesterday_at:I = 0x7f080631

.field public static final timeline_about:I = 0x7f080aed

.field public static final timeline_actionbar_activity_log:I = 0x7f080ae9

.field public static final timeline_actionbar_add_life_event:I = 0x7f080ae4

.field public static final timeline_actionbar_add_moment:I = 0x7f080ae3

.field public static final timeline_actionbar_block:I = 0x7f080adf

.field public static final timeline_actionbar_call:I = 0x7f080ada

.field public static final timeline_actionbar_cancel_request:I = 0x7f080ad6

.field public static final timeline_actionbar_check_in:I = 0x7f080ae2

.field public static final timeline_actionbar_confirm:I = 0x7f080ad5

.field public static final timeline_actionbar_edit_cover_photo:I = 0x7f080ae7

.field public static final timeline_actionbar_edit_profile_photo:I = 0x7f080ae6

.field public static final timeline_actionbar_follow:I = 0x7f080ad7

.field public static final timeline_actionbar_followed:I = 0x7f080ad8

.field public static final timeline_actionbar_friends:I = 0x7f080ad4

.field public static final timeline_actionbar_give_gift:I = 0x7f080adc

.field public static final timeline_actionbar_message:I = 0x7f080ad9

.field public static final timeline_actionbar_poke:I = 0x7f080adb

.field public static final timeline_actionbar_report:I = 0x7f080ae0

.field public static final timeline_actionbar_see_friendship:I = 0x7f080add

.field public static final timeline_actionbar_update_info:I = 0x7f080ae5

.field public static final timeline_actionbar_update_status:I = 0x7f080ae1

.field public static final timeline_actionbar_view_privacy_shortcuts:I = 0x7f080ae8

.field public static final timeline_actionbar_write_post:I = 0x7f080ade

.field public static final timeline_add_profile_photo:I = 0x7f080af1

.field public static final timeline_ban_user:I = 0x7f080afb

.field public static final timeline_block_confirm_message:I = 0x7f080b2b

.field public static final timeline_block_confirm_message_conversation:I = 0x7f080b2f

.field public static final timeline_block_confirm_message_event:I = 0x7f080b2e

.field public static final timeline_block_confirm_message_friend:I = 0x7f080b30

.field public static final timeline_block_confirm_message_friends:I = 0x7f080b31

.field public static final timeline_block_confirm_message_see:I = 0x7f080b2c

.field public static final timeline_block_confirm_message_tag:I = 0x7f080b2d

.field public static final timeline_block_failed:I = 0x7f080b32

.field public static final timeline_call_button:I = 0x7f080b45

.field public static final timeline_cancel:I = 0x7f080b33

.field public static final timeline_cancel_friend_request:I = 0x7f080b0f

.field public static final timeline_cancel_friend_request_failed:I = 0x7f080b1b

.field public static final timeline_cancel_phone_number_request:I = 0x7f080b4a

.field public static final timeline_cell_number_call_button:I = 0x7f080b48

.field public static final timeline_collections_suggestions:I = 0x7f0811e9

.field public static final timeline_confirm_ban_user:I = 0x7f080afc

.field public static final timeline_confirm_delete:I = 0x7f080b17

.field public static final timeline_confirm_hide:I = 0x7f080b18

.field public static final timeline_content_not_available:I = 0x7f080051

.field public static final timeline_cover_photo_upload:I = 0x7f080af2

.field public static final timeline_coverphoto_drag:I = 0x7f080b3c

.field public static final timeline_coverphoto_edit:I = 0x7f080b3f

.field public static final timeline_coverphoto_preview:I = 0x7f080b3d

.field public static final timeline_coverphoto_save:I = 0x7f080b3e

.field public static final timeline_delete:I = 0x7f080af5

.field public static final timeline_delete_friend_request:I = 0x7f080b10

.field public static final timeline_delete_friend_request_short:I = 0x7f080b11

.field public static final timeline_delete_story_failed:I = 0x7f080b13

.field public static final timeline_delete_verbose:I = 0x7f080af6

.field public static final timeline_disable_notifications:I = 0x7f080b09

.field public static final timeline_edit_friend_list:I = 0x7f080b0e

.field public static final timeline_employee_only_error:I = 0x7f080b1c

.field public static final timeline_error_min_size:I = 0x7f080b40

.field public static final timeline_follow_button:I = 0x7f080aec

.field public static final timeline_following:I = 0x7f080b04

.field public static final timeline_friend_request_failed:I = 0x7f080b12

.field public static final timeline_friend_request_sent:I = 0x7f080aff

.field public static final timeline_friend_request_title:I = 0x7f080b44

.field public static final timeline_friend_respond_to_request:I = 0x7f080afe

.field public static final timeline_friends:I = 0x7f080aef

.field public static final timeline_friends_button:I = 0x7f080aea

.field public static final timeline_get_notifications:I = 0x7f080b08

.field public static final timeline_get_notifications_change_failed:I = 0x7f080b0c

.field public static final timeline_hide:I = 0x7f080af7

.field public static final timeline_hide_story_failed:I = 0x7f080b14

.field public static final timeline_home_number_call_button:I = 0x7f080b47

.field public static final timeline_more:I = 0x7f080af0

.field public static final timeline_no_stories:I = 0x7f080b35

.field public static final timeline_notifications_disabled:I = 0x7f080b0b

.field public static final timeline_notifications_enabled:I = 0x7f080b0a

.field public static final timeline_page_post_by_others:I = 0x7f080af4

.field public static final timeline_page_scheduled_posts:I = 0x7f080af3

.field public static final timeline_pending_posts:I = 0x7f080b34

.field public static final timeline_photo:I = 0x7f080b59

.field public static final timeline_photo_uploading:I = 0x7f08107a

.field public static final timeline_photo_view:I = 0x7f080b41

.field public static final timeline_photos:I = 0x7f080aee

.field public static final timeline_poke_failed:I = 0x7f080b37

.field public static final timeline_poke_outstanding:I = 0x7f080b38

.field public static final timeline_poke_success:I = 0x7f080b36

.field public static final timeline_profile_questions_unit_title:I = 0x7f080b43

.field public static final timeline_profilephoto_edit:I = 0x7f080b42

.field public static final timeline_remove_friend_failed:I = 0x7f080b1a

.field public static final timeline_remove_from_friends:I = 0x7f080b16

.field public static final timeline_request_phone_number:I = 0x7f080b49

.field public static final timeline_respond_to_friend_request_failed:I = 0x7f080b19

.field public static final timeline_respond_to_friend_requests:I = 0x7f080b39

.field public static final timeline_scale_coverphoto_failed:I = 0x7f080b3a

.field public static final timeline_see_more:I = 0x7f080afd

.field public static final timeline_set_coverphoto_failed:I = 0x7f080b3b

.field public static final timeline_set_profilephoto_failed:I = 0x7f081079

.field public static final timeline_story_hidden:I = 0x7f080af8

.field public static final timeline_subscribe:I = 0x7f080b01

.field public static final timeline_subscribe_button:I = 0x7f080aeb

.field public static final timeline_subscribe_failed:I = 0x7f080b06

.field public static final timeline_subscribed:I = 0x7f080b03

.field public static final timeline_unfriend:I = 0x7f080b0d

.field public static final timeline_unhide_story:I = 0x7f080afa

.field public static final timeline_unhide_story_failed:I = 0x7f080b15

.field public static final timeline_unsubscribe:I = 0x7f080b02

.field public static final timeline_unsubscribe_failed:I = 0x7f080b07

.field public static final timeline_unsubscribed:I = 0x7f080b05

.field public static final timeline_upload_coverphoto_failed:I = 0x7f08107b

.field public static final timeline_work_number_call_button:I = 0x7f080b46

.field public static final timeline_year_overview_add_event:I = 0x7f080b60

.field public static final timeline_year_overview_add_moment:I = 0x7f080b61

.field public static final timeline_year_overview_photo_title:I = 0x7f080b64

.field public static final timeline_year_overview_show_more_posts:I = 0x7f080b63

.field public static final timeline_year_overview_show_posts:I = 0x7f080b5f

.field public static final timeline_year_overview_snowflake_photo_title:I = 0x7f080b65

.field public static final timeline_year_show_all:I = 0x7f080b62

.field public static final title_activity_dash:I = 0x7f0800d5

.field public static final title_bar_preview:I = 0x7f08079d

.field public static final title_listener_dash:I = 0x7f0800d6

.field public static final title_new_event:I = 0x7f080f2a

.field public static final token_hint_text:I = 0x7f080191

.field public static final too_many_photos:I = 0x7f080782

.field public static final too_many_user_warning_message:I = 0x7f0803c6

.field public static final too_many_user_warning_title:I = 0x7f0803c5

.field public static final total_failure_loading_feed:I = 0x7f0805b7

.field public static final total_failure_loading_page:I = 0x7f08004e

.field public static final total_failure_loading_timeline:I = 0x7f08004c

.field public static final total_reach_text_description:I = 0x7f0805cb

.field public static final track_title_text_view_description:I = 0x7f080a85

.field public static final transaction_yesterday_format:I = 0x7f0801cc

.field public static final try_again:I = 0x7f08003a

.field public static final turn_off_home:I = 0x7f080a36

.field public static final turn_off_home_confirm:I = 0x7f080a35

.field public static final turn_on_home:I = 0x7f080a38

.field public static final type_name_hint:I = 0x7f0806b2

.field public static final typeahead_icon_description:I = 0x7f08018f

.field public static final typeahead_to_heading:I = 0x7f08018d

.field public static final typeahead_to_heading_description:I = 0x7f08018e

.field public static final typeahead_token_with_comma:I = 0x7f080193

.field public static final typeahead_view_more_description:I = 0x7f080190

.field public static final typeahead_view_more_text:I = 0x7f080192

.field public static final uberbar_no_results:I = 0x7f080e5a

.field public static final uberbar_searchbox_hint:I = 0x7f080e5b

.field public static final ubersearch_clear_button_description:I = 0x7f080e66

.field public static final ubersearch_close_button_description:I = 0x7f080e65

.field public static final ubersearch_friend_request_sent_description:I = 0x7f080e62

.field public static final ubersearch_initiate_call_description:I = 0x7f080e63

.field public static final ubersearch_like_description:I = 0x7f080e60

.field public static final ubersearch_liked_description:I = 0x7f080e61

.field public static final ubersearch_result_details_separator:I = 0x7f080e5c

.field public static final ubersearch_result_title_verified:I = 0x7f080e64

.field public static final ubersearch_type_event:I = 0x7f080e5d

.field public static final ubersearch_type_group:I = 0x7f080e5e

.field public static final ubersearch_type_page:I = 0x7f080e5f

.field public static final ufi_comment_button_description:I = 0x7f0800de

.field public static final ufi_favorite_button_description:I = 0x7f0800dc

.field public static final ufi_first_of_friends:I = 0x7f0800f9

.field public static final ufi_like_button_description:I = 0x7f0800da

.field public static final ufi_more_options_button_description:I = 0x7f0800df

.field public static final ufi_more_options_delete_post:I = 0x7f0800e1

.field public static final ufi_more_options_hide_post:I = 0x7f0800e0

.field public static final ufi_more_options_prompt_cancel:I = 0x7f0800e5

.field public static final ufi_more_options_prompt_confirm:I = 0x7f0800e4

.field public static final ufi_more_options_prompt_header:I = 0x7f0800e3

.field public static final ufi_more_options_share:I = 0x7f0800e2

.field public static final ufi_unfavorite_button_description:I = 0x7f0800dd

.field public static final ufi_unlike_button_description:I = 0x7f0800db

.field public static final ufi_view_on_app_label:I = 0x7f0800f6

.field public static final ufi_you_like_this:I = 0x7f0800fa

.field public static final ufiservices_actor_to_target:I = 0x7f080548

.field public static final ufiservices_ban_user:I = 0x7f080541

.field public static final ufiservices_cancel:I = 0x7f080546

.field public static final ufiservices_comment:I = 0x7f08053b

.field public static final ufiservices_delete:I = 0x7f080545

.field public static final ufiservices_delete_comment:I = 0x7f08053d

.field public static final ufiservices_edit_comment:I = 0x7f08053e

.field public static final ufiservices_edit_history_title:I = 0x7f080549

.field public static final ufiservices_feedback_copy_comment_title:I = 0x7f08053c

.field public static final ufiservices_first_to_like:I = 0x7f080537

.field public static final ufiservices_go_back_description:I = 0x7f08054b

.field public static final ufiservices_like:I = 0x7f080539

.field public static final ufiservices_load_more_comments:I = 0x7f080536

.field public static final ufiservices_load_previous_comments:I = 0x7f080534

.field public static final ufiservices_load_previous_replies:I = 0x7f080535

.field public static final ufiservices_people_who_like_this:I = 0x7f08054a

.field public static final ufiservices_photo_comment:I = 0x7f08053f

.field public static final ufiservices_remove_photo_comment:I = 0x7f080540

.field public static final ufiservices_replied:I = 0x7f08054d

.field public static final ufiservices_replies:I = 0x7f08054e

.field public static final ufiservices_reply:I = 0x7f08054c

.field public static final ufiservices_separator:I = 0x7f080538

.field public static final ufiservices_share:I = 0x7f080543

.field public static final ufiservices_try_again:I = 0x7f080544

.field public static final ufiservices_unlike:I = 0x7f08053a

.field public static final ufiservices_update:I = 0x7f080547

.field public static final ufiservices_user_banned:I = 0x7f080542

.field public static final undo:I = 0x7f080f2b

.field public static final unified_composer_hint:I = 0x7f0802a0

.field public static final unread_message_pill_text:I = 0x7f080220

.field public static final unselected_feed_filter:I = 0x7f080f40

.field public static final update_installation_failure_message:I = 0x7f080c91

.field public static final update_required_button:I = 0x7f080caa

.field public static final update_required_text:I = 0x7f080ca9

.field public static final update_required_title:I = 0x7f080ca8

.field public static final updating_action_bar:I = 0x7f080fd2

.field public static final updating_favorites:I = 0x7f080283

.field public static final upgrade_button:I = 0x7f0801ec

.field public static final upgrade_header:I = 0x7f0801eb

.field public static final upgrade_log_out_button:I = 0x7f0801ed

.field public static final upload_application_name:I = 0x7f080904

.field public static final upload_cancel_survey_accidentally_clicked:I = 0x7f08093f

.field public static final upload_cancel_survey_change_post:I = 0x7f08093e

.field public static final upload_cancel_survey_close:I = 0x7f080941

.field public static final upload_cancel_survey_do_not_post:I = 0x7f08093d

.field public static final upload_cancel_survey_other:I = 0x7f080940

.field public static final upload_cancel_survey_question:I = 0x7f080943

.field public static final upload_cancel_survey_question_header:I = 0x7f080942

.field public static final upload_cancel_survey_too_long:I = 0x7f08093b

.field public static final upload_cancel_survey_upload_error:I = 0x7f08093c

.field public static final upload_complete_notification_title:I = 0x7f08090c

.field public static final upload_dialog_cancel_message:I = 0x7f08091b

.field public static final upload_dialog_cancel_upload_button:I = 0x7f080919

.field public static final upload_dialog_cancel_video_message:I = 0x7f08091c

.field public static final upload_dialog_cant_find_album_photo:I = 0x7f08092b

.field public static final upload_dialog_cant_upload_to_album:I = 0x7f08092a

.field public static final upload_dialog_close_button:I = 0x7f080926

.field public static final upload_dialog_complete_message:I = 0x7f08091f

.field public static final upload_dialog_complete_title:I = 0x7f08091e

.field public static final upload_dialog_continue_upload_button:I = 0x7f08091a

.field public static final upload_dialog_didnt_upload_multi_photo_message:I = 0x7f080936

.field public static final upload_dialog_didnt_upload_single_photo_message:I = 0x7f080935

.field public static final upload_dialog_didnt_upload_video_message:I = 0x7f080937

.field public static final upload_dialog_failure_multiphoto_message:I = 0x7f080929

.field public static final upload_dialog_failure_photo_message:I = 0x7f080927

.field public static final upload_dialog_failure_video_message:I = 0x7f080928

.field public static final upload_dialog_fatal_failure_title:I = 0x7f080923

.field public static final upload_dialog_interrupted_too_many_times:I = 0x7f080932

.field public static final upload_dialog_invalid_request_server_says:I = 0x7f08092c

.field public static final upload_dialog_keep_trying_button:I = 0x7f080925

.field public static final upload_dialog_keep_trying_message:I = 0x7f080920

.field public static final upload_dialog_local_processing_error_dont_retry:I = 0x7f08092f

.field public static final upload_dialog_missing_photo:I = 0x7f080933

.field public static final upload_dialog_missing_video:I = 0x7f080934

.field public static final upload_dialog_multiphoto_message:I = 0x7f08091d

.field public static final upload_dialog_network_error_retry_multi_photo:I = 0x7f080939

.field public static final upload_dialog_network_error_retry_single_photo:I = 0x7f080938

.field public static final upload_dialog_network_error_retry_video:I = 0x7f08093a

.field public static final upload_dialog_no_disk_space:I = 0x7f080931

.field public static final upload_dialog_ok_button:I = 0x7f080921

.field public static final upload_dialog_policy_violation_error_dont_retry:I = 0x7f080930

.field public static final upload_dialog_title:I = 0x7f080918

.field public static final upload_dialog_transient_failure_title:I = 0x7f080922

.field public static final upload_dialog_try_again_button:I = 0x7f080924

.field public static final upload_dialog_unspecified_error_dont_retry:I = 0x7f08092e

.field public static final upload_dialog_unspecified_error_may_retry:I = 0x7f08092d

.field public static final upload_load_media_no_permission:I = 0x7f08071d

.field public static final upload_load_photo_error:I = 0x7f08071e

.field public static final upload_notification_complete:I = 0x7f080909

.field public static final upload_notification_failed:I = 0x7f080913

.field public static final upload_notification_progress_m_of_n:I = 0x7f080906

.field public static final upload_notification_progress_title_format:I = 0x7f080907

.field public static final upload_notification_progress_title_merging_format:I = 0x7f080908

.field public static final upload_notification_start:I = 0x7f080905

.field public static final upload_notification_title:I = 0x7f08090d

.field public static final upload_notification_title_photo:I = 0x7f08090f

.field public static final upload_notification_title_photos:I = 0x7f080910

.field public static final upload_notification_title_review:I = 0x7f08090e

.field public static final upload_notification_title_video:I = 0x7f080911

.field public static final upload_notification_title_video_processing:I = 0x7f080912

.field public static final upload_place_pic_notification_completed:I = 0x7f08095b

.field public static final upload_place_pic_notification_failed:I = 0x7f08095c

.field public static final upload_place_pic_notification_title:I = 0x7f08095a

.field public static final upload_retrying:I = 0x7f080916

.field public static final upload_tap_for_options_notification:I = 0x7f080914

.field public static final upload_tap_to_cancel:I = 0x7f080915

.field public static final upload_uploading_photo:I = 0x7f0807e7

.field public static final upload_uploading_video:I = 0x7f0807e8

.field public static final upload_will_retry:I = 0x7f080917

.field public static final upsell_action_row_text:I = 0x7f080903

.field public static final upsell_close_button_text:I = 0x7f0808fe

.field public static final upsell_confirm_button:I = 0x7f080902

.field public static final upsell_current_promo_title:I = 0x7f080900

.field public static final upsell_did_not_receive_promo_text:I = 0x7f0808fd

.field public static final upsell_error_title:I = 0x7f0808fc

.field public static final upsell_plan_selected_title:I = 0x7f080901

.field public static final upsell_processing_button_text:I = 0x7f0808ff

.field public static final usb_error:I = 0x7f08077c

.field public static final use_also_as_launcher:I = 0x7f080a30

.field public static final use_as_lockscreen:I = 0x7f080a2f

.field public static final use_button_text:I = 0x7f080796

.field public static final user_account_nux_button_done:I = 0x7f080d78

.field public static final user_account_nux_button_next:I = 0x7f080d79

.field public static final user_account_nux_button_skip:I = 0x7f080d7a

.field public static final user_account_nux_step_contact_importer_title:I = 0x7f080d76

.field public static final user_account_nux_step_native_name_continue:I = 0x7f080d82

.field public static final user_account_nux_step_native_name_extra_name:I = 0x7f080d81

.field public static final user_account_nux_step_native_name_extra_surname:I = 0x7f080d80

.field public static final user_account_nux_step_native_name_intro1:I = 0x7f080d7c

.field public static final user_account_nux_step_native_name_intro2:I = 0x7f080d7d

.field public static final user_account_nux_step_native_name_name:I = 0x7f080d7f

.field public static final user_account_nux_step_native_name_surname:I = 0x7f080d7e

.field public static final user_account_nux_step_native_name_title:I = 0x7f080d7b

.field public static final user_account_nux_step_profile_information_bottom_instructions:I = 0x7f080d85

.field public static final user_account_nux_step_profile_information_clear_icon:I = 0x7f080d87

.field public static final user_account_nux_step_profile_information_college_text:I = 0x7f080d89

.field public static final user_account_nux_step_profile_information_current_city_text:I = 0x7f080d8b

.field public static final user_account_nux_step_profile_information_high_school_text:I = 0x7f080d88

.field public static final user_account_nux_step_profile_information_hometown_text:I = 0x7f080d8c

.field public static final user_account_nux_step_profile_information_save_button:I = 0x7f080d86

.field public static final user_account_nux_step_profile_information_title:I = 0x7f080d83

.field public static final user_account_nux_step_profile_information_top_instructions:I = 0x7f080d84

.field public static final user_account_nux_step_profile_information_work_text:I = 0x7f080d8a

.field public static final user_account_nux_step_profile_photo_choose_button:I = 0x7f080d73

.field public static final user_account_nux_step_profile_photo_instructions:I = 0x7f080d71

.field public static final user_account_nux_step_profile_photo_take_button:I = 0x7f080d72

.field public static final user_account_nux_step_profile_photo_title:I = 0x7f080d70

.field public static final user_account_nux_step_profile_photo_upload_failed:I = 0x7f080d75

.field public static final user_account_nux_step_profile_photo_uploading:I = 0x7f080d74

.field public static final user_account_nux_step_pymk_title:I = 0x7f080d77

.field public static final util_bytes:I = 0x7f080746

.field public static final util_exabytes:I = 0x7f08074c

.field public static final util_gigabytes:I = 0x7f080749

.field public static final util_kilobytes:I = 0x7f080747

.field public static final util_megabytes:I = 0x7f080748

.field public static final util_petabytes:I = 0x7f08074b

.field public static final util_terrabytes:I = 0x7f08074a

.field public static final uw_caption_box_hint:I = 0x7f0808a9

.field public static final uw_more:I = 0x7f0808ad

.field public static final vault_error_low_battery:I = 0x7f080740

.field public static final vault_error_no_internet:I = 0x7f08073d

.field public static final vault_error_requires_wifi:I = 0x7f08073e

.field public static final vault_error_sync_is_off:I = 0x7f08073f

.field public static final vault_nux_flyout:I = 0x7f080721

.field public static final vault_optin_desc:I = 0x7f08073a

.field public static final vault_optin_footer:I = 0x7f080738

.field public static final vault_optin_private_emphasized:I = 0x7f08073b

.field public static final vault_optin_sync_photos_btn:I = 0x7f08073c

.field public static final vault_optin_title:I = 0x7f080739

.field public static final vault_photo_delete_dialog_no:I = 0x7f080744

.field public static final vault_photo_delete_dialog_text:I = 0x7f080742

.field public static final vault_photo_delete_dialog_title:I = 0x7f080741

.field public static final vault_photo_delete_dialog_yes:I = 0x7f080743

.field public static final vault_remove:I = 0x7f080725

.field public static final vault_settings_device_title:I = 0x7f080732

.field public static final vault_settings_disable_dialog_no:I = 0x7f080735

.field public static final vault_settings_disable_dialog_text:I = 0x7f080733

.field public static final vault_settings_disable_dialog_yes:I = 0x7f080734

.field public static final vault_settings_header:I = 0x7f080731

.field public static final vault_settings_storage_description:I = 0x7f08072d

.field public static final vault_settings_storage_title:I = 0x7f08072e

.field public static final vault_settings_storage_total:I = 0x7f080730

.field public static final vault_settings_storage_used:I = 0x7f08072f

.field public static final vault_sync_older_photos_label:I = 0x7f08072a

.field public static final vault_sync_older_photos_off_state:I = 0x7f08072b

.field public static final vault_sync_older_photos_on_state:I = 0x7f08072c

.field public static final vault_sync_privacy_header_text:I = 0x7f080724

.field public static final vault_sync_settings_unknown:I = 0x7f080737

.field public static final vault_sync_settings_used_formatter:I = 0x7f080736

.field public static final vault_try_again:I = 0x7f080723

.field public static final vault_unable_to_delete_photo:I = 0x7f080720

.field public static final vault_unable_to_delete_photo_title:I = 0x7f08071f

.field public static final vault_unable_to_sync_photo:I = 0x7f080722

.field public static final version_promo_button:I = 0x7f080356

.field public static final version_promo_button_caps:I = 0x7f080357

.field public static final version_promo_text:I = 0x7f080355

.field public static final video_album_title:I = 0x7f08074d

.field public static final video_description:I = 0x7f080786

.field public static final video_failed_to_locate:I = 0x7f080945

.field public static final video_not_supported:I = 0x7f080400

.field public static final video_not_supported_title:I = 0x7f0803ff

.field public static final video_off_button_description:I = 0x7f080438

.field public static final video_on_button_description:I = 0x7f080439

.field public static final video_pause_button_description:I = 0x7f080486

.field public static final video_picking_error_desc_file_size_too_large:I = 0x7f080402

.field public static final video_picking_error_desc_video_and_photo:I = 0x7f080409

.field public static final video_picking_error_desc_video_too_long:I = 0x7f080404

.field public static final video_picking_error_desc_video_too_short:I = 0x7f080406

.field public static final video_picking_error_title_file_size_too_large:I = 0x7f080401

.field public static final video_picking_error_title_video_too_long:I = 0x7f080403

.field public static final video_picking_error_title_video_too_short:I = 0x7f080405

.field public static final video_picking_warning_desc_video_very_long:I = 0x7f080408

.field public static final video_picking_warning_title_video_very_long:I = 0x7f080407

.field public static final video_play_button_description:I = 0x7f080485

.field public static final video_play_error:I = 0x7f080484

.field public static final video_play_error_title:I = 0x7f080483

.field public static final video_playback_error_desc_corrupt:I = 0x7f080416

.field public static final video_playback_error_desc_disk_space:I = 0x7f080414

.field public static final video_playback_error_desc_generic:I = 0x7f080412

.field public static final video_playback_error_title_corrupt:I = 0x7f080415

.field public static final video_playback_error_title_disk_space:I = 0x7f080413

.field public static final video_playback_error_title_download:I = 0x7f080411

.field public static final video_playback_error_title_generic:I = 0x7f080410

.field public static final video_player_duration_hours:I = 0x7f081053

.field public static final video_player_duration_minutes:I = 0x7f081054

.field public static final video_player_filesize_mb:I = 0x7f081055

.field public static final video_recording_failed:I = 0x7f080719

.field public static final video_replay:I = 0x7f080480

.field public static final video_resizing_error_generic:I = 0x7f08040f

.field public static final video_saving_failed:I = 0x7f08071a

.field public static final video_too_big_title:I = 0x7f080426

.field public static final video_too_large_dialog_message:I = 0x7f080792

.field public static final video_too_large_dialog_title:I = 0x7f080791

.field public static final video_too_small_dialog_message:I = 0x7f080794

.field public static final video_too_small_dialog_title:I = 0x7f080793

.field public static final video_trim_handle_left:I = 0x7f08048f

.field public static final video_trim_handle_right:I = 0x7f080490

.field public static final video_trim_scrubber:I = 0x7f08048e

.field public static final video_trimming_approximate_size:I = 0x7f08048d

.field public static final video_trimming_metadata_original:I = 0x7f08048b

.field public static final video_trimming_metadata_ready:I = 0x7f08048c

.field public static final video_uploading_error_corrupted:I = 0x7f08040d

.field public static final video_uploading_error_disk_space:I = 0x7f08040e

.field public static final video_uploading_error_generic:I = 0x7f08040b

.field public static final video_uploading_error_timeout:I = 0x7f08040c

.field public static final video_uploading_error_title:I = 0x7f08040a

.field public static final video_width_height:I = 0x7f081056

.field public static final view_archived_messages:I = 0x7f0802e3

.field public static final view_map_action:I = 0x7f080225

.field public static final view_other_messages:I = 0x7f0802e4

.field public static final view_people_action:I = 0x7f0802ee

.field public static final view_photo_caption_hint:I = 0x7f080635

.field public static final view_photo_clear:I = 0x7f080637

.field public static final view_photo_delete:I = 0x7f08065b

.field public static final view_photo_dont_like:I = 0x7f080660

.field public static final view_photo_edit:I = 0x7f08065a

.field public static final view_photo_hide_from_page:I = 0x7f080644

.field public static final view_photo_hide_from_page_dialog_yes:I = 0x7f080646

.field public static final view_photo_hide_from_page_question:I = 0x7f080645

.field public static final view_photo_make_cover_photo:I = 0x7f08065d

.field public static final view_photo_make_profile_pic:I = 0x7f08065c

.field public static final view_photo_no_way_to_share_image:I = 0x7f080657

.field public static final view_photo_remove_tag:I = 0x7f080661

.field public static final view_photo_remove_tag_confirmation_dialog_body:I = 0x7f080663

.field public static final view_photo_remove_tag_confirmation_dialog_title:I = 0x7f080662

.field public static final view_photo_report:I = 0x7f08065f

.field public static final view_photo_save:I = 0x7f08065e

.field public static final view_photo_set_as:I = 0x7f080656

.field public static final view_photo_share:I = 0x7f080658

.field public static final view_photo_update:I = 0x7f080636

.field public static final view_profile_action:I = 0x7f0802e5

.field public static final view_shared_image_history_action:I = 0x7f0802f1

.field public static final view_shared_image_history_activity_title:I = 0x7f0802f0

.field public static final view_shared_image_history_download_error:I = 0x7f0802f4

.field public static final view_shared_image_history_loading:I = 0x7f0802f2

.field public static final view_shared_image_history_no_results:I = 0x7f0802f3

.field public static final view_story_insights:I = 0x7f0805ca

.field public static final voip_nux_banner_content:I = 0x7f080236

.field public static final voip_nux_banner_cta:I = 0x7f080237

.field public static final voip_nux_banner_title:I = 0x7f080235

.field public static final waiting_to_connect:I = 0x7f080058

.field public static final wallet_buy_button_place_holder:I = 0x7f0806ce

.field public static final wallpaper_dialog_continue_button_label:I = 0x7f08015b

.field public static final wallpaper_dialog_defer_button_label:I = 0x7f08015a

.field public static final wallpaper_dialog_text:I = 0x7f080159

.field public static final wallpaper_dialog_title:I = 0x7f080158

.field public static final weather_formatted:I = 0x7f080a74

.field public static final weather_updating:I = 0x7f080a73

.field public static final webrtc_call_again:I = 0x7f080459

.field public static final webrtc_call_again_interrupt_message:I = 0x7f08045f

.field public static final webrtc_call_back:I = 0x7f080458

.field public static final webrtc_call_back_interrupt_message:I = 0x7f080465

.field public static final webrtc_call_back_message:I = 0x7f080464

.field public static final webrtc_call_reminder:I = 0x7f080475

.field public static final webrtc_call_reminder_1h:I = 0x7f080470

.field public static final webrtc_call_reminder_30m:I = 0x7f08046f

.field public static final webrtc_call_reminder_8h:I = 0x7f080471

.field public static final webrtc_call_status_bar_description:I = 0x7f080456

.field public static final webrtc_call_status_bar_text:I = 0x7f080455

.field public static final webrtc_cancel:I = 0x7f08045d

.field public static final webrtc_feedback_dismiss:I = 0x7f080468

.field public static final webrtc_feedback_hint:I = 0x7f080466

.field public static final webrtc_feedback_submit:I = 0x7f080467

.field public static final webrtc_feedback_title:I = 0x7f080469

.field public static final webrtc_free_call_call:I = 0x7f080454

.field public static final webrtc_free_call_notification_body:I = 0x7f080453

.field public static final webrtc_free_call_notification_title:I = 0x7f080452

.field public static final webrtc_incall_connection_duration_long:I = 0x7f080449

.field public static final webrtc_incall_connection_duration_short:I = 0x7f080448

.field public static final webrtc_incall_status_call_ended:I = 0x7f080440

.field public static final webrtc_incall_status_call_failed:I = 0x7f080442

.field public static final webrtc_incall_status_call_interrupted:I = 0x7f080441

.field public static final webrtc_incall_status_carrier_blocked:I = 0x7f080447

.field public static final webrtc_incall_status_connecting:I = 0x7f08043f

.field public static final webrtc_incall_status_connection_lost:I = 0x7f080446

.field public static final webrtc_incall_status_contacting:I = 0x7f08043d

.field public static final webrtc_incall_status_did_not_answer:I = 0x7f080444

.field public static final webrtc_incall_status_in_another_call:I = 0x7f080445

.field public static final webrtc_incall_status_incoming:I = 0x7f08043b

.field public static final webrtc_incall_status_incoming_messenger:I = 0x7f08043c

.field public static final webrtc_incall_status_not_reachable:I = 0x7f080443

.field public static final webrtc_incall_status_ringing:I = 0x7f08043e

.field public static final webrtc_incoming_upgrade_alert_message:I = 0x7f08044d

.field public static final webrtc_incoming_upgrade_alert_title:I = 0x7f08044c

.field public static final webrtc_interrupted_call:I = 0x7f08045b

.field public static final webrtc_missed_call:I = 0x7f08045a

.field public static final webrtc_muted:I = 0x7f08045c

.field public static final webrtc_notif_warning:I = 0x7f080476

.field public static final webrtc_notification_incall_text:I = 0x7f08043a

.field public static final webrtc_outgoing_upgrade_alert_message:I = 0x7f08044b

.field public static final webrtc_outgoing_upgrade_alert_title:I = 0x7f08044a

.field public static final webrtc_qr_busy_msg:I = 0x7f080472

.field public static final webrtc_qr_call_back:I = 0x7f080473

.field public static final webrtc_qr_call_later:I = 0x7f080474

.field public static final webrtc_quality_survey_text:I = 0x7f080451

.field public static final webrtc_rating_excellent:I = 0x7f08046e

.field public static final webrtc_rating_fair:I = 0x7f08046b

.field public static final webrtc_rating_good:I = 0x7f08046c

.field public static final webrtc_rating_poor:I = 0x7f08046a

.field public static final webrtc_rating_very_good:I = 0x7f08046d

.field public static final webrtc_reconnected:I = 0x7f080462

.field public static final webrtc_reconnecting:I = 0x7f080461

.field public static final webrtc_redial_call_text:I = 0x7f08045e

.field public static final webrtc_ringtone_toast:I = 0x7f080463

.field public static final webrtc_start_call_title:I = 0x7f080457

.field public static final webrtc_unable_call_generic_message:I = 0x7f08044f

.field public static final webrtc_unable_call_ongoing_call:I = 0x7f080450

.field public static final webrtc_unable_call_title:I = 0x7f08044e

.field public static final webrtc_weak_connection:I = 0x7f080460

.field public static final who_ask:I = 0x7f080ace

.field public static final who_cancel:I = 0x7f080acf

.field public static final who_cancel_failure:I = 0x7f080ad1

.field public static final who_copy_to_clipboard:I = 0x7f080ad2

.field public static final who_is_this:I = 0x7f0806b4

.field public static final who_text_copied:I = 0x7f080ad3

.field public static final widget_error_title:I = 0x7f0806d1

.field public static final widget_logged_out_message:I = 0x7f0806d2

.field public static final widget_logged_out_title:I = 0x7f0806d3

.field public static final widget_no_content_title:I = 0x7f0806d5

.field public static final widget_refreshing_title:I = 0x7f0806d4

.field public static final wp_change_every_24_hours:I = 0x7f080a26

.field public static final wp_change_every_hour:I = 0x7f080a25

.field public static final wp_change_never:I = 0x7f080a23

.field public static final wp_change_screen_on:I = 0x7f080a24

.field public static final wp_change_wallpaper:I = 0x7f080a22

.field public static final wp_desc:I = 0x7f080a0d

.field public static final wp_icon_calendar:I = 0x7f080a15

.field public static final wp_icon_camera:I = 0x7f080a16

.field public static final wp_icon_contacts:I = 0x7f080a13

.field public static final wp_icon_email:I = 0x7f080a14

.field public static final wp_icon_photos:I = 0x7f080a1a

.field public static final wp_icon_rss:I = 0x7f080a18

.field public static final wp_icon_settings:I = 0x7f080a19

.field public static final wp_icon_videos:I = 0x7f080a17

.field public static final wp_lock_and_launcher:I = 0x7f080a29

.field public static final wp_lock_screen_only:I = 0x7f080a28

.field public static final wp_logged_out_line:I = 0x7f080a20

.field public static final wp_name:I = 0x7f080a0c

.field public static final wp_no_internet_line:I = 0x7f080a1e

.field public static final wp_no_internet_setup_flow_line:I = 0x7f080a1f

.field public static final wp_no_photo_line:I = 0x7f080a21

.field public static final wp_no_selection:I = 0x7f080a11

.field public static final wp_photos_from_facebook:I = 0x7f080a1d

.field public static final wp_show_wallpaper_on:I = 0x7f080a27

.field public static final wp_swipe_down:I = 0x7f080a12

.field public static final wp_use_current_wallpaper:I = 0x7f080a1b

.field public static final wp_use_home_wallpaper:I = 0x7f080a1c

.field public static final wpnux_desc:I = 0x7f080a0f

.field public static final wpnux_set_wallpaper:I = 0x7f080a10

.field public static final wpnux_title:I = 0x7f080a0e

.field public static final write_comment:I = 0x7f080557

.field public static final write_reply:I = 0x7f080558

.field public static final yes:I = 0x7f080f2c

.field public static final you_are_now_friends:I = 0x7f080170

.field public static final you_can_still_post:I = 0x7f080039

.field public static final you_sent_a_message_generic_admin_message:I = 0x7f0802c0

.field public static final you_sent_a_sticker_admin_message:I = 0x7f0802b8

.field public static final you_sent_a_video_admin_message:I = 0x7f0802bc

.field public static final you_sent_a_voice_clip_admin_message:I = 0x7f0802be

.field public static final you_sent_an_image_admin_message:I = 0x7f0802ba

.field public static final zero_bottom_banner_content:I = 0x7f0804a0

.field public static final zero_download_video_dialog_content:I = 0x7f0804a4

.field public static final zero_external_url_dialog_content:I = 0x7f080499

.field public static final zero_generic_do_not_remind_me_again:I = 0x7f080498

.field public static final zero_generic_extra_data_charges_dialog_title:I = 0x7f080497

.field public static final zero_image_search_dialog_content:I = 0x7f08049d

.field public static final zero_location_services_content:I = 0x7f0804a1

.field public static final zero_play_video_dialog_content:I = 0x7f0804a3

.field public static final zero_show_map_button_title:I = 0x7f08049e

.field public static final zero_show_map_dialog_content:I = 0x7f08049f

.field public static final zero_upload_video_dialog_content:I = 0x7f0804a2

.field public static final zero_view_timeline_dialog_content:I = 0x7f08049c

.field public static final zero_voip_call_dialog_content:I = 0x7f080477

.field public static final zero_voip_call_incoming_dialog_content:I = 0x7f080478


# direct methods
.method public constructor <init>()V
    .locals 0

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method
