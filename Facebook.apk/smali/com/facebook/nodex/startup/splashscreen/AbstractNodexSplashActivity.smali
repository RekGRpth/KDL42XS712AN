.class public abstract Lcom/facebook/nodex/startup/splashscreen/AbstractNodexSplashActivity;
.super Lcom/facebook/nodex/startup/splashscreen/NodexBaseActivity;
.source "AbstractNodexSplashActivity.java"


# instance fields
.field private a:Landroid/content/Context;

.field private b:Lcom/facebook/nodex/startup/crashloop/CrashLoop;

.field private c:Landroid/content/ContentResolver;

.field private d:Lcom/facebook/nodex/startup/splashscreen/NodexInitializer;

.field private e:Lcom/facebook/nodex/resources/NodexResources;

.field private f:Landroid/os/Handler;

.field private g:Ljava/util/concurrent/ExecutorService;

.field private h:Z

.field private i:Z

.field private j:Ljava/lang/String;

.field private k:Landroid/view/View;

.field private l:Ljava/lang/Runnable;

.field private m:Landroid/widget/TextView;

.field private n:Ljava/lang/Runnable;


# direct methods
.method public constructor <init>()V
    .locals 0

    invoke-direct {p0}, Lcom/facebook/nodex/startup/splashscreen/NodexBaseActivity;-><init>()V

    return-void
.end method

.method static synthetic a(Lcom/facebook/nodex/startup/splashscreen/AbstractNodexSplashActivity;)Landroid/view/View;
    .locals 1

    iget-object v0, p0, Lcom/facebook/nodex/startup/splashscreen/AbstractNodexSplashActivity;->k:Landroid/view/View;

    return-object v0
.end method

.method static synthetic a(Lcom/facebook/nodex/startup/splashscreen/AbstractNodexSplashActivity;Lcom/facebook/nodex/startup/splashscreen/NodexError;)V
    .locals 0

    invoke-direct {p0, p1}, Lcom/facebook/nodex/startup/splashscreen/AbstractNodexSplashActivity;->a(Lcom/facebook/nodex/startup/splashscreen/NodexError;)V

    return-void
.end method

.method static synthetic a(Lcom/facebook/nodex/startup/splashscreen/AbstractNodexSplashActivity;Ljava/lang/String;)V
    .locals 0

    invoke-direct {p0, p1}, Lcom/facebook/nodex/startup/splashscreen/AbstractNodexSplashActivity;->a(Ljava/lang/String;)V

    return-void
.end method

.method private a(Lcom/facebook/nodex/startup/splashscreen/NodexError;)V
    .locals 1
    .annotation build Landroid/annotation/SuppressLint;
        value = {
            "BadMethodUse-android.content.Context.startActivity"
        }
    .end annotation

    invoke-virtual {p1, p0}, Lcom/facebook/nodex/startup/splashscreen/NodexError;->a(Landroid/content/Context;)Landroid/content/Intent;

    move-result-object v0

    invoke-virtual {p0, v0}, Lcom/facebook/nodex/startup/splashscreen/AbstractNodexSplashActivity;->startActivity(Landroid/content/Intent;)V

    invoke-virtual {p0}, Lcom/facebook/nodex/startup/splashscreen/AbstractNodexSplashActivity;->finish()V

    return-void
.end method

.method private a(Ljava/lang/String;)V
    .locals 2

    iget-object v0, p0, Lcom/facebook/nodex/startup/splashscreen/AbstractNodexSplashActivity;->m:Landroid/widget/TextView;

    if-eqz v0, :cond_0

    if-nez p1, :cond_1

    :cond_0
    :goto_0
    return-void

    :cond_1
    iget-object v0, p0, Lcom/facebook/nodex/startup/splashscreen/AbstractNodexSplashActivity;->f:Landroid/os/Handler;

    new-instance v1, Lcom/facebook/nodex/startup/splashscreen/AbstractNodexSplashActivity$3;

    invoke-direct {v1, p0, p1}, Lcom/facebook/nodex/startup/splashscreen/AbstractNodexSplashActivity$3;-><init>(Lcom/facebook/nodex/startup/splashscreen/AbstractNodexSplashActivity;Ljava/lang/String;)V

    invoke-virtual {v0, v1}, Landroid/os/Handler;->post(Ljava/lang/Runnable;)Z

    goto :goto_0
.end method

.method static synthetic b(Lcom/facebook/nodex/startup/splashscreen/AbstractNodexSplashActivity;)Landroid/widget/TextView;
    .locals 1

    iget-object v0, p0, Lcom/facebook/nodex/startup/splashscreen/AbstractNodexSplashActivity;->m:Landroid/widget/TextView;

    return-object v0
.end method

.method static synthetic c(Lcom/facebook/nodex/startup/splashscreen/AbstractNodexSplashActivity;)Lcom/facebook/nodex/resources/NodexResources;
    .locals 1

    iget-object v0, p0, Lcom/facebook/nodex/startup/splashscreen/AbstractNodexSplashActivity;->e:Lcom/facebook/nodex/resources/NodexResources;

    return-object v0
.end method

.method static synthetic d(Lcom/facebook/nodex/startup/splashscreen/AbstractNodexSplashActivity;)Ljava/lang/String;
    .locals 1

    iget-object v0, p0, Lcom/facebook/nodex/startup/splashscreen/AbstractNodexSplashActivity;->j:Ljava/lang/String;

    return-object v0
.end method

.method static synthetic e(Lcom/facebook/nodex/startup/splashscreen/AbstractNodexSplashActivity;)Ljava/lang/Runnable;
    .locals 1

    iget-object v0, p0, Lcom/facebook/nodex/startup/splashscreen/AbstractNodexSplashActivity;->l:Ljava/lang/Runnable;

    return-object v0
.end method

.method static synthetic f(Lcom/facebook/nodex/startup/splashscreen/AbstractNodexSplashActivity;)Landroid/os/Handler;
    .locals 1

    iget-object v0, p0, Lcom/facebook/nodex/startup/splashscreen/AbstractNodexSplashActivity;->f:Landroid/os/Handler;

    return-object v0
.end method

.method static synthetic g(Lcom/facebook/nodex/startup/splashscreen/AbstractNodexSplashActivity;)Ljava/lang/Runnable;
    .locals 1

    iget-object v0, p0, Lcom/facebook/nodex/startup/splashscreen/AbstractNodexSplashActivity;->n:Ljava/lang/Runnable;

    return-object v0
.end method

.method static synthetic h(Lcom/facebook/nodex/startup/splashscreen/AbstractNodexSplashActivity;)Lcom/facebook/nodex/startup/splashscreen/NodexInitializer;
    .locals 1

    iget-object v0, p0, Lcom/facebook/nodex/startup/splashscreen/AbstractNodexSplashActivity;->d:Lcom/facebook/nodex/startup/splashscreen/NodexInitializer;

    return-object v0
.end method

.method static synthetic i(Lcom/facebook/nodex/startup/splashscreen/AbstractNodexSplashActivity;)Lcom/facebook/nodex/startup/crashloop/CrashLoop;
    .locals 1

    iget-object v0, p0, Lcom/facebook/nodex/startup/splashscreen/AbstractNodexSplashActivity;->b:Lcom/facebook/nodex/startup/crashloop/CrashLoop;

    return-object v0
.end method

.method static synthetic j(Lcom/facebook/nodex/startup/splashscreen/AbstractNodexSplashActivity;)Landroid/content/ContentResolver;
    .locals 1

    iget-object v0, p0, Lcom/facebook/nodex/startup/splashscreen/AbstractNodexSplashActivity;->c:Landroid/content/ContentResolver;

    return-object v0
.end method

.method private j()V
    .locals 2

    invoke-virtual {p0}, Lcom/facebook/nodex/startup/splashscreen/AbstractNodexSplashActivity;->b()Ljava/lang/String;

    move-result-object v0

    if-nez v0, :cond_1

    :cond_0
    :goto_0
    return-void

    :cond_1
    iget-object v1, p0, Lcom/facebook/nodex/startup/splashscreen/AbstractNodexSplashActivity;->e:Lcom/facebook/nodex/resources/NodexResources;

    invoke-virtual {v1, v0}, Lcom/facebook/nodex/resources/NodexResources;->b(Ljava/lang/String;)I

    move-result v0

    invoke-virtual {p0, v0}, Lcom/facebook/nodex/startup/splashscreen/AbstractNodexSplashActivity;->findViewById(I)Landroid/view/View;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/nodex/startup/splashscreen/AbstractNodexSplashActivity;->k:Landroid/view/View;

    new-instance v0, Lcom/facebook/nodex/startup/splashscreen/AbstractNodexSplashActivity$1;

    invoke-direct {v0, p0}, Lcom/facebook/nodex/startup/splashscreen/AbstractNodexSplashActivity$1;-><init>(Lcom/facebook/nodex/startup/splashscreen/AbstractNodexSplashActivity;)V

    iput-object v0, p0, Lcom/facebook/nodex/startup/splashscreen/AbstractNodexSplashActivity;->l:Ljava/lang/Runnable;

    invoke-virtual {p0}, Lcom/facebook/nodex/startup/splashscreen/AbstractNodexSplashActivity;->c()Ljava/lang/String;

    move-result-object v0

    if-eqz v0, :cond_0

    iget-object v1, p0, Lcom/facebook/nodex/startup/splashscreen/AbstractNodexSplashActivity;->e:Lcom/facebook/nodex/resources/NodexResources;

    invoke-virtual {v1, v0}, Lcom/facebook/nodex/resources/NodexResources;->b(Ljava/lang/String;)I

    move-result v0

    invoke-virtual {p0, v0}, Lcom/facebook/nodex/startup/splashscreen/AbstractNodexSplashActivity;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    iput-object v0, p0, Lcom/facebook/nodex/startup/splashscreen/AbstractNodexSplashActivity;->m:Landroid/widget/TextView;

    new-instance v0, Lcom/facebook/nodex/startup/splashscreen/AbstractNodexSplashActivity$2;

    invoke-direct {v0, p0}, Lcom/facebook/nodex/startup/splashscreen/AbstractNodexSplashActivity$2;-><init>(Lcom/facebook/nodex/startup/splashscreen/AbstractNodexSplashActivity;)V

    iput-object v0, p0, Lcom/facebook/nodex/startup/splashscreen/AbstractNodexSplashActivity;->n:Ljava/lang/Runnable;

    goto :goto_0
.end method

.method static synthetic k(Lcom/facebook/nodex/startup/splashscreen/AbstractNodexSplashActivity;)V
    .locals 0

    invoke-direct {p0}, Lcom/facebook/nodex/startup/splashscreen/AbstractNodexSplashActivity;->l()V

    return-void
.end method

.method private k()Z
    .locals 4

    const/4 v0, 0x0

    invoke-virtual {p0}, Lcom/facebook/nodex/startup/splashscreen/AbstractNodexSplashActivity;->isTaskRoot()Z

    move-result v1

    if-eqz v1, :cond_1

    :cond_0
    :goto_0
    return v0

    :cond_1
    invoke-virtual {p0}, Lcom/facebook/nodex/startup/splashscreen/AbstractNodexSplashActivity;->getIntent()Landroid/content/Intent;

    move-result-object v1

    if-eqz v1, :cond_0

    invoke-virtual {v1}, Landroid/content/Intent;->getAction()Ljava/lang/String;

    move-result-object v2

    const-string v3, "android.intent.category.LAUNCHER"

    invoke-virtual {v1, v3}, Landroid/content/Intent;->hasCategory(Ljava/lang/String;)Z

    move-result v1

    if-eqz v1, :cond_0

    const-string v1, "android.intent.action.MAIN"

    invoke-virtual {v1, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_0

    const/4 v0, 0x1

    goto :goto_0
.end method

.method private l()V
    .locals 2

    iget-object v0, p0, Lcom/facebook/nodex/startup/splashscreen/AbstractNodexSplashActivity;->j:Ljava/lang/String;

    const-string v1, "onRemoteProcessInitialized()"

    invoke-static {v0, v1}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    invoke-virtual {p0}, Lcom/facebook/nodex/startup/splashscreen/AbstractNodexSplashActivity;->isFinishing()Z

    move-result v0

    if-nez v0, :cond_0

    invoke-direct {p0}, Lcom/facebook/nodex/startup/splashscreen/AbstractNodexSplashActivity;->m()V

    :cond_0
    return-void
.end method

.method private m()V
    .locals 4

    const/4 v3, 0x1

    const/4 v2, 0x0

    iget-object v0, p0, Lcom/facebook/nodex/startup/splashscreen/AbstractNodexSplashActivity;->j:Ljava/lang/String;

    const-string v1, "startRemoteProcessTargetActivity()"

    invoke-static {v0, v1}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    iput-boolean v3, p0, Lcom/facebook/nodex/startup/splashscreen/AbstractNodexSplashActivity;->h:Z

    iput-boolean v3, p0, Lcom/facebook/nodex/startup/splashscreen/AbstractNodexSplashActivity;->i:Z

    invoke-direct {p0}, Lcom/facebook/nodex/startup/splashscreen/AbstractNodexSplashActivity;->o()V

    invoke-virtual {p0, v2, v2}, Lcom/facebook/nodex/startup/splashscreen/AbstractNodexSplashActivity;->overridePendingTransition(II)V

    return-void
.end method

.method private n()Z
    .locals 3

    iget-object v0, p0, Lcom/facebook/nodex/startup/splashscreen/AbstractNodexSplashActivity;->a:Landroid/content/Context;

    const-string v1, "activity"

    invoke-virtual {v0, v1}, Landroid/content/Context;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/app/ActivityManager;

    invoke-virtual {p0}, Lcom/facebook/nodex/startup/splashscreen/AbstractNodexSplashActivity;->f()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0}, Landroid/app/ActivityManager;->getRunningAppProcesses()Ljava/util/List;

    move-result-object v0

    if-eqz v0, :cond_1

    invoke-interface {v0}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v2

    :cond_0
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_1

    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/app/ActivityManager$RunningAppProcessInfo;

    iget-object v0, v0, Landroid/app/ActivityManager$RunningAppProcessInfo;->processName:Ljava/lang/String;

    invoke-virtual {v1, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_1
    const/4 v0, 0x0

    goto :goto_0
.end method

.method private o()V
    .locals 3
    .annotation build Landroid/annotation/SuppressLint;
        value = {
            "BadMethodUse-android.content.Context.startActivity"
        }
    .end annotation

    new-instance v1, Landroid/content/Intent;

    invoke-direct {v1}, Landroid/content/Intent;-><init>()V

    invoke-virtual {p0}, Lcom/facebook/nodex/startup/splashscreen/AbstractNodexSplashActivity;->i()Ljava/util/Set;

    move-result-object v0

    invoke-interface {v0}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    move-result-object v2

    :goto_0
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/String;

    invoke-virtual {v1, v0}, Landroid/content/Intent;->addCategory(Ljava/lang/String;)Landroid/content/Intent;

    goto :goto_0

    :cond_0
    invoke-virtual {p0}, Lcom/facebook/nodex/startup/splashscreen/AbstractNodexSplashActivity;->h()Landroid/content/ComponentName;

    move-result-object v0

    invoke-virtual {v1, v0}, Landroid/content/Intent;->setComponent(Landroid/content/ComponentName;)Landroid/content/Intent;

    const/high16 v0, 0x4000000

    invoke-virtual {v1, v0}, Landroid/content/Intent;->addFlags(I)Landroid/content/Intent;

    invoke-virtual {p0, v1}, Lcom/facebook/nodex/startup/splashscreen/AbstractNodexSplashActivity;->startActivity(Landroid/content/Intent;)V

    return-void
.end method


# virtual methods
.method protected abstract a()Ljava/lang/String;
.end method

.method protected abstract b()Ljava/lang/String;
.end method

.method protected abstract c()Ljava/lang/String;
.end method

.method protected abstract d()Ljava/lang/String;
.end method

.method protected abstract e()Ljava/lang/String;
.end method

.method protected abstract f()Ljava/lang/String;
.end method

.method protected abstract g()Landroid/net/Uri;
.end method

.method protected abstract h()Landroid/content/ComponentName;
.end method

.method protected abstract i()Ljava/util/Set;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/Set",
            "<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation
.end method

.method protected onCreate(Landroid/os/Bundle;)V
    .locals 3

    invoke-virtual {p0}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/Class;->getSimpleName()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/nodex/startup/splashscreen/AbstractNodexSplashActivity;->j:Ljava/lang/String;

    iget-object v0, p0, Lcom/facebook/nodex/startup/splashscreen/AbstractNodexSplashActivity;->j:Ljava/lang/String;

    const-string v1, "onCreate()"

    invoke-static {v0, v1}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    invoke-super {p0, p1}, Lcom/facebook/nodex/startup/splashscreen/NodexBaseActivity;->onCreate(Landroid/os/Bundle;)V

    invoke-virtual {p0}, Lcom/facebook/nodex/startup/splashscreen/AbstractNodexSplashActivity;->getApplicationContext()Landroid/content/Context;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/nodex/startup/splashscreen/AbstractNodexSplashActivity;->a:Landroid/content/Context;

    new-instance v0, Lcom/facebook/nodex/startup/crashloop/CrashLoop;

    iget-object v1, p0, Lcom/facebook/nodex/startup/splashscreen/AbstractNodexSplashActivity;->a:Landroid/content/Context;

    invoke-direct {v0, v1}, Lcom/facebook/nodex/startup/crashloop/CrashLoop;-><init>(Landroid/content/Context;)V

    iput-object v0, p0, Lcom/facebook/nodex/startup/splashscreen/AbstractNodexSplashActivity;->b:Lcom/facebook/nodex/startup/crashloop/CrashLoop;

    iget-object v0, p0, Lcom/facebook/nodex/startup/splashscreen/AbstractNodexSplashActivity;->a:Landroid/content/Context;

    invoke-virtual {v0}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/nodex/startup/splashscreen/AbstractNodexSplashActivity;->c:Landroid/content/ContentResolver;

    new-instance v0, Lcom/facebook/nodex/startup/splashscreen/NodexInitializer;

    invoke-direct {v0, p0}, Lcom/facebook/nodex/startup/splashscreen/NodexInitializer;-><init>(Landroid/content/Context;)V

    iput-object v0, p0, Lcom/facebook/nodex/startup/splashscreen/AbstractNodexSplashActivity;->d:Lcom/facebook/nodex/startup/splashscreen/NodexInitializer;

    new-instance v0, Landroid/os/Handler;

    invoke-direct {v0}, Landroid/os/Handler;-><init>()V

    iput-object v0, p0, Lcom/facebook/nodex/startup/splashscreen/AbstractNodexSplashActivity;->f:Landroid/os/Handler;

    invoke-static {}, Ljava/util/concurrent/Executors;->newSingleThreadExecutor()Ljava/util/concurrent/ExecutorService;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/nodex/startup/splashscreen/AbstractNodexSplashActivity;->g:Ljava/util/concurrent/ExecutorService;

    new-instance v0, Lcom/facebook/nodex/resources/NodexResources;

    invoke-direct {v0, p0}, Lcom/facebook/nodex/resources/NodexResources;-><init>(Landroid/content/Context;)V

    iput-object v0, p0, Lcom/facebook/nodex/startup/splashscreen/AbstractNodexSplashActivity;->e:Lcom/facebook/nodex/resources/NodexResources;

    iget-object v0, p0, Lcom/facebook/nodex/startup/splashscreen/AbstractNodexSplashActivity;->e:Lcom/facebook/nodex/resources/NodexResources;

    invoke-virtual {p0}, Lcom/facebook/nodex/startup/splashscreen/AbstractNodexSplashActivity;->a()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/facebook/nodex/resources/NodexResources;->a(Ljava/lang/String;)I

    move-result v0

    invoke-virtual {p0, v0}, Lcom/facebook/nodex/startup/splashscreen/AbstractNodexSplashActivity;->setContentView(I)V

    invoke-direct {p0}, Lcom/facebook/nodex/startup/splashscreen/AbstractNodexSplashActivity;->j()V

    invoke-direct {p0}, Lcom/facebook/nodex/startup/splashscreen/AbstractNodexSplashActivity;->k()Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-virtual {p0}, Lcom/facebook/nodex/startup/splashscreen/AbstractNodexSplashActivity;->finish()V

    :goto_0
    return-void

    :cond_0
    invoke-direct {p0}, Lcom/facebook/nodex/startup/splashscreen/AbstractNodexSplashActivity;->n()Z

    move-result v0

    if-eqz v0, :cond_1

    invoke-direct {p0}, Lcom/facebook/nodex/startup/splashscreen/AbstractNodexSplashActivity;->o()V

    invoke-virtual {p0}, Lcom/facebook/nodex/startup/splashscreen/AbstractNodexSplashActivity;->finish()V

    goto :goto_0

    :cond_1
    iget-object v0, p0, Lcom/facebook/nodex/startup/splashscreen/AbstractNodexSplashActivity;->g:Ljava/util/concurrent/ExecutorService;

    new-instance v1, Lcom/facebook/nodex/startup/splashscreen/AbstractNodexSplashActivity$InitRunnable;

    const/4 v2, 0x0

    invoke-direct {v1, p0, v2}, Lcom/facebook/nodex/startup/splashscreen/AbstractNodexSplashActivity$InitRunnable;-><init>(Lcom/facebook/nodex/startup/splashscreen/AbstractNodexSplashActivity;B)V

    invoke-interface {v0, v1}, Ljava/util/concurrent/ExecutorService;->execute(Ljava/lang/Runnable;)V

    goto :goto_0
.end method

.method protected onDestroy()V
    .locals 2

    iget-object v0, p0, Lcom/facebook/nodex/startup/splashscreen/AbstractNodexSplashActivity;->j:Ljava/lang/String;

    const-string v1, "onDestroy()"

    invoke-static {v0, v1}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    invoke-super {p0}, Lcom/facebook/nodex/startup/splashscreen/NodexBaseActivity;->onDestroy()V

    iget-object v0, p0, Lcom/facebook/nodex/startup/splashscreen/AbstractNodexSplashActivity;->l:Ljava/lang/Runnable;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/facebook/nodex/startup/splashscreen/AbstractNodexSplashActivity;->f:Landroid/os/Handler;

    iget-object v1, p0, Lcom/facebook/nodex/startup/splashscreen/AbstractNodexSplashActivity;->l:Ljava/lang/Runnable;

    invoke-virtual {v0, v1}, Landroid/os/Handler;->removeCallbacks(Ljava/lang/Runnable;)V

    :cond_0
    iget-object v0, p0, Lcom/facebook/nodex/startup/splashscreen/AbstractNodexSplashActivity;->n:Ljava/lang/Runnable;

    if-eqz v0, :cond_1

    iget-object v0, p0, Lcom/facebook/nodex/startup/splashscreen/AbstractNodexSplashActivity;->f:Landroid/os/Handler;

    iget-object v1, p0, Lcom/facebook/nodex/startup/splashscreen/AbstractNodexSplashActivity;->n:Ljava/lang/Runnable;

    invoke-virtual {v0, v1}, Landroid/os/Handler;->removeCallbacks(Ljava/lang/Runnable;)V

    :cond_1
    iget-object v0, p0, Lcom/facebook/nodex/startup/splashscreen/AbstractNodexSplashActivity;->g:Ljava/util/concurrent/ExecutorService;

    invoke-interface {v0}, Ljava/util/concurrent/ExecutorService;->shutdown()V

    return-void
.end method

.method protected onPause()V
    .locals 4

    iget-object v0, p0, Lcom/facebook/nodex/startup/splashscreen/AbstractNodexSplashActivity;->j:Ljava/lang/String;

    const-string v1, "onPause()"

    invoke-static {v0, v1}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    invoke-super {p0}, Lcom/facebook/nodex/startup/splashscreen/NodexBaseActivity;->onPause()V

    iget-boolean v0, p0, Lcom/facebook/nodex/startup/splashscreen/AbstractNodexSplashActivity;->h:Z

    if-eqz v0, :cond_0

    new-instance v0, Lcom/facebook/nodex/startup/splashscreen/AbstractNodexSplashActivity$4;

    invoke-direct {v0, p0}, Lcom/facebook/nodex/startup/splashscreen/AbstractNodexSplashActivity$4;-><init>(Lcom/facebook/nodex/startup/splashscreen/AbstractNodexSplashActivity;)V

    iget-object v1, p0, Lcom/facebook/nodex/startup/splashscreen/AbstractNodexSplashActivity;->f:Landroid/os/Handler;

    const-wide/16 v2, 0xbb8

    invoke-virtual {v1, v0, v2, v3}, Landroid/os/Handler;->postDelayed(Ljava/lang/Runnable;J)Z

    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/facebook/nodex/startup/splashscreen/AbstractNodexSplashActivity;->i:Z

    :cond_0
    return-void
.end method

.method protected onResume()V
    .locals 2

    iget-object v0, p0, Lcom/facebook/nodex/startup/splashscreen/AbstractNodexSplashActivity;->j:Ljava/lang/String;

    const-string v1, "onResume()"

    invoke-static {v0, v1}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    invoke-super {p0}, Lcom/facebook/nodex/startup/splashscreen/NodexBaseActivity;->onResume()V

    iget-boolean v0, p0, Lcom/facebook/nodex/startup/splashscreen/AbstractNodexSplashActivity;->i:Z

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/facebook/nodex/startup/splashscreen/AbstractNodexSplashActivity;->j:Ljava/lang/String;

    const-string v1, "onResume() - finishing the activity."

    invoke-static {v0, v1}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    invoke-virtual {p0}, Lcom/facebook/nodex/startup/splashscreen/AbstractNodexSplashActivity;->finish()V

    :cond_0
    return-void
.end method
