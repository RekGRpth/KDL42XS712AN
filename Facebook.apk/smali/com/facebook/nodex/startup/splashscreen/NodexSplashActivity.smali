.class public Lcom/facebook/nodex/startup/splashscreen/NodexSplashActivity;
.super Lcom/facebook/nodex/startup/splashscreen/AbstractNodexSplashActivity;
.source "NodexSplashActivity.java"


# direct methods
.method public constructor <init>()V
    .locals 0

    invoke-direct {p0}, Lcom/facebook/nodex/startup/splashscreen/AbstractNodexSplashActivity;-><init>()V

    return-void
.end method


# virtual methods
.method protected final a()Ljava/lang/String;
    .locals 1

    const-string v0, "nodex_splashscreen"

    return-object v0
.end method

.method protected final b()Ljava/lang/String;
    .locals 1

    const-string v0, "progress_bar"

    return-object v0
.end method

.method protected final c()Ljava/lang/String;
    .locals 1

    const-string v0, "progress_message"

    return-object v0
.end method

.method protected final d()Ljava/lang/String;
    .locals 1

    const-string v0, "nodex_upgrading_message_string"

    return-object v0
.end method

.method protected final e()Ljava/lang/String;
    .locals 1

    const-string v0, "nodex_main_app_loading_message_string"

    return-object v0
.end method

.method protected final f()Ljava/lang/String;
    .locals 1

    invoke-virtual {p0}, Lcom/facebook/nodex/startup/splashscreen/NodexSplashActivity;->getPackageName()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method protected final g()Landroid/net/Uri;
    .locals 1

    sget-object v0, Lcom/facebook/nodex/startup/warmup/NodexWarmupContentProvider;->b:Landroid/net/Uri;

    return-object v0
.end method

.method protected final h()Landroid/content/ComponentName;
    .locals 2

    new-instance v0, Landroid/content/ComponentName;

    const-string v1, "com.facebook.katana.ProtectedLoginActivity"

    invoke-direct {v0, p0, v1}, Landroid/content/ComponentName;-><init>(Landroid/content/Context;Ljava/lang/String;)V

    return-object v0
.end method

.method protected final i()Ljava/util/Set;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/Set",
            "<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation

    new-instance v0, Ljava/util/HashSet;

    invoke-direct {v0}, Ljava/util/HashSet;-><init>()V

    return-object v0
.end method
