.class Lcom/facebook/nodex/startup/splashscreen/AbstractNodexSplashActivity$InitRunnable;
.super Ljava/lang/Object;
.source "AbstractNodexSplashActivity.java"

# interfaces
.implements Ljava/lang/Runnable;


# instance fields
.field final synthetic a:Lcom/facebook/nodex/startup/splashscreen/AbstractNodexSplashActivity;


# direct methods
.method private constructor <init>(Lcom/facebook/nodex/startup/splashscreen/AbstractNodexSplashActivity;)V
    .locals 0

    iput-object p1, p0, Lcom/facebook/nodex/startup/splashscreen/AbstractNodexSplashActivity$InitRunnable;->a:Lcom/facebook/nodex/startup/splashscreen/AbstractNodexSplashActivity;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method synthetic constructor <init>(Lcom/facebook/nodex/startup/splashscreen/AbstractNodexSplashActivity;B)V
    .locals 0

    invoke-direct {p0, p1}, Lcom/facebook/nodex/startup/splashscreen/AbstractNodexSplashActivity$InitRunnable;-><init>(Lcom/facebook/nodex/startup/splashscreen/AbstractNodexSplashActivity;)V

    return-void
.end method


# virtual methods
.method public run()V
    .locals 4

    iget-object v0, p0, Lcom/facebook/nodex/startup/splashscreen/AbstractNodexSplashActivity$InitRunnable;->a:Lcom/facebook/nodex/startup/splashscreen/AbstractNodexSplashActivity;

    invoke-static {v0}, Lcom/facebook/nodex/startup/splashscreen/AbstractNodexSplashActivity;->d(Lcom/facebook/nodex/startup/splashscreen/AbstractNodexSplashActivity;)Ljava/lang/String;

    move-result-object v0

    const-string v1, "InitRunnable.run()"

    invoke-static {v0, v1}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    iget-object v0, p0, Lcom/facebook/nodex/startup/splashscreen/AbstractNodexSplashActivity$InitRunnable;->a:Lcom/facebook/nodex/startup/splashscreen/AbstractNodexSplashActivity;

    invoke-static {v0}, Lcom/facebook/nodex/startup/splashscreen/AbstractNodexSplashActivity;->e(Lcom/facebook/nodex/startup/splashscreen/AbstractNodexSplashActivity;)Ljava/lang/Runnable;

    move-result-object v0

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/facebook/nodex/startup/splashscreen/AbstractNodexSplashActivity$InitRunnable;->a:Lcom/facebook/nodex/startup/splashscreen/AbstractNodexSplashActivity;

    invoke-static {v0}, Lcom/facebook/nodex/startup/splashscreen/AbstractNodexSplashActivity;->f(Lcom/facebook/nodex/startup/splashscreen/AbstractNodexSplashActivity;)Landroid/os/Handler;

    move-result-object v0

    iget-object v1, p0, Lcom/facebook/nodex/startup/splashscreen/AbstractNodexSplashActivity$InitRunnable;->a:Lcom/facebook/nodex/startup/splashscreen/AbstractNodexSplashActivity;

    invoke-static {v1}, Lcom/facebook/nodex/startup/splashscreen/AbstractNodexSplashActivity;->e(Lcom/facebook/nodex/startup/splashscreen/AbstractNodexSplashActivity;)Ljava/lang/Runnable;

    move-result-object v1

    const-wide/16 v2, 0xbb8

    invoke-virtual {v0, v1, v2, v3}, Landroid/os/Handler;->postDelayed(Ljava/lang/Runnable;J)Z

    :cond_0
    iget-object v0, p0, Lcom/facebook/nodex/startup/splashscreen/AbstractNodexSplashActivity$InitRunnable;->a:Lcom/facebook/nodex/startup/splashscreen/AbstractNodexSplashActivity;

    invoke-static {v0}, Lcom/facebook/nodex/startup/splashscreen/AbstractNodexSplashActivity;->g(Lcom/facebook/nodex/startup/splashscreen/AbstractNodexSplashActivity;)Ljava/lang/Runnable;

    move-result-object v0

    if-eqz v0, :cond_1

    iget-object v0, p0, Lcom/facebook/nodex/startup/splashscreen/AbstractNodexSplashActivity$InitRunnable;->a:Lcom/facebook/nodex/startup/splashscreen/AbstractNodexSplashActivity;

    invoke-static {v0}, Lcom/facebook/nodex/startup/splashscreen/AbstractNodexSplashActivity;->f(Lcom/facebook/nodex/startup/splashscreen/AbstractNodexSplashActivity;)Landroid/os/Handler;

    move-result-object v0

    iget-object v1, p0, Lcom/facebook/nodex/startup/splashscreen/AbstractNodexSplashActivity$InitRunnable;->a:Lcom/facebook/nodex/startup/splashscreen/AbstractNodexSplashActivity;

    invoke-static {v1}, Lcom/facebook/nodex/startup/splashscreen/AbstractNodexSplashActivity;->g(Lcom/facebook/nodex/startup/splashscreen/AbstractNodexSplashActivity;)Ljava/lang/Runnable;

    move-result-object v1

    const-wide/16 v2, 0x2710

    invoke-virtual {v0, v1, v2, v3}, Landroid/os/Handler;->postDelayed(Ljava/lang/Runnable;J)Z

    :cond_1
    iget-object v0, p0, Lcom/facebook/nodex/startup/splashscreen/AbstractNodexSplashActivity$InitRunnable;->a:Lcom/facebook/nodex/startup/splashscreen/AbstractNodexSplashActivity;

    iget-object v1, p0, Lcom/facebook/nodex/startup/splashscreen/AbstractNodexSplashActivity$InitRunnable;->a:Lcom/facebook/nodex/startup/splashscreen/AbstractNodexSplashActivity;

    invoke-virtual {v1}, Lcom/facebook/nodex/startup/splashscreen/AbstractNodexSplashActivity;->d()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/facebook/nodex/startup/splashscreen/AbstractNodexSplashActivity;->a(Lcom/facebook/nodex/startup/splashscreen/AbstractNodexSplashActivity;Ljava/lang/String;)V

    iget-object v0, p0, Lcom/facebook/nodex/startup/splashscreen/AbstractNodexSplashActivity$InitRunnable;->a:Lcom/facebook/nodex/startup/splashscreen/AbstractNodexSplashActivity;

    invoke-static {v0}, Lcom/facebook/nodex/startup/splashscreen/AbstractNodexSplashActivity;->h(Lcom/facebook/nodex/startup/splashscreen/AbstractNodexSplashActivity;)Lcom/facebook/nodex/startup/splashscreen/NodexInitializer;

    move-result-object v0

    invoke-virtual {v0}, Lcom/facebook/nodex/startup/splashscreen/NodexInitializer;->a()Lcom/facebook/nodex/startup/splashscreen/NodexError;

    move-result-object v0

    if-eqz v0, :cond_2

    iget-object v1, p0, Lcom/facebook/nodex/startup/splashscreen/AbstractNodexSplashActivity$InitRunnable;->a:Lcom/facebook/nodex/startup/splashscreen/AbstractNodexSplashActivity;

    invoke-static {v1, v0}, Lcom/facebook/nodex/startup/splashscreen/AbstractNodexSplashActivity;->a(Lcom/facebook/nodex/startup/splashscreen/AbstractNodexSplashActivity;Lcom/facebook/nodex/startup/splashscreen/NodexError;)V

    :goto_0
    return-void

    :cond_2
    iget-object v0, p0, Lcom/facebook/nodex/startup/splashscreen/AbstractNodexSplashActivity$InitRunnable;->a:Lcom/facebook/nodex/startup/splashscreen/AbstractNodexSplashActivity;

    iget-object v1, p0, Lcom/facebook/nodex/startup/splashscreen/AbstractNodexSplashActivity$InitRunnable;->a:Lcom/facebook/nodex/startup/splashscreen/AbstractNodexSplashActivity;

    invoke-virtual {v1}, Lcom/facebook/nodex/startup/splashscreen/AbstractNodexSplashActivity;->e()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/facebook/nodex/startup/splashscreen/AbstractNodexSplashActivity;->a(Lcom/facebook/nodex/startup/splashscreen/AbstractNodexSplashActivity;Ljava/lang/String;)V

    iget-object v0, p0, Lcom/facebook/nodex/startup/splashscreen/AbstractNodexSplashActivity$InitRunnable;->a:Lcom/facebook/nodex/startup/splashscreen/AbstractNodexSplashActivity;

    invoke-static {v0}, Lcom/facebook/nodex/startup/splashscreen/AbstractNodexSplashActivity;->i(Lcom/facebook/nodex/startup/splashscreen/AbstractNodexSplashActivity;)Lcom/facebook/nodex/startup/crashloop/CrashLoop;

    move-result-object v0

    invoke-virtual {v0}, Lcom/facebook/nodex/startup/crashloop/CrashLoop;->d()Z

    move-result v0

    if-eqz v0, :cond_3

    iget-object v0, p0, Lcom/facebook/nodex/startup/splashscreen/AbstractNodexSplashActivity$InitRunnable;->a:Lcom/facebook/nodex/startup/splashscreen/AbstractNodexSplashActivity;

    invoke-static {v0}, Lcom/facebook/nodex/startup/splashscreen/AbstractNodexSplashActivity;->i(Lcom/facebook/nodex/startup/splashscreen/AbstractNodexSplashActivity;)Lcom/facebook/nodex/startup/crashloop/CrashLoop;

    move-result-object v0

    invoke-virtual {v0}, Lcom/facebook/nodex/startup/crashloop/CrashLoop;->c()Z

    move-result v0

    if-eqz v0, :cond_4

    :cond_3
    iget-object v0, p0, Lcom/facebook/nodex/startup/splashscreen/AbstractNodexSplashActivity$InitRunnable;->a:Lcom/facebook/nodex/startup/splashscreen/AbstractNodexSplashActivity;

    invoke-static {v0}, Lcom/facebook/nodex/startup/splashscreen/AbstractNodexSplashActivity;->i(Lcom/facebook/nodex/startup/splashscreen/AbstractNodexSplashActivity;)Lcom/facebook/nodex/startup/crashloop/CrashLoop;

    move-result-object v0

    invoke-virtual {v0}, Lcom/facebook/nodex/startup/crashloop/CrashLoop;->b()V

    :cond_4
    iget-object v0, p0, Lcom/facebook/nodex/startup/splashscreen/AbstractNodexSplashActivity$InitRunnable;->a:Lcom/facebook/nodex/startup/splashscreen/AbstractNodexSplashActivity;

    invoke-static {v0}, Lcom/facebook/nodex/startup/splashscreen/AbstractNodexSplashActivity;->j(Lcom/facebook/nodex/startup/splashscreen/AbstractNodexSplashActivity;)Landroid/content/ContentResolver;

    move-result-object v0

    iget-object v1, p0, Lcom/facebook/nodex/startup/splashscreen/AbstractNodexSplashActivity$InitRunnable;->a:Lcom/facebook/nodex/startup/splashscreen/AbstractNodexSplashActivity;

    invoke-virtual {v1}, Lcom/facebook/nodex/startup/splashscreen/AbstractNodexSplashActivity;->g()Landroid/net/Uri;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/content/ContentResolver;->acquireContentProviderClient(Landroid/net/Uri;)Landroid/content/ContentProviderClient;

    move-result-object v1

    if-eqz v1, :cond_5

    sget-object v0, Lcom/facebook/nodex/startup/warmup/NodexWarmupContentProvider$Action;->WaitForInit:Lcom/facebook/nodex/startup/warmup/NodexWarmupContentProvider$Action;

    invoke-static {v0}, Lcom/facebook/nodex/startup/warmup/NodexWarmupContentProvider;->a(Lcom/facebook/nodex/startup/warmup/NodexWarmupContentProvider$Action;)Landroid/content/ContentValues;

    move-result-object v0

    :try_start_0
    iget-object v2, p0, Lcom/facebook/nodex/startup/splashscreen/AbstractNodexSplashActivity$InitRunnable;->a:Lcom/facebook/nodex/startup/splashscreen/AbstractNodexSplashActivity;

    invoke-virtual {v2}, Lcom/facebook/nodex/startup/splashscreen/AbstractNodexSplashActivity;->g()Landroid/net/Uri;

    move-result-object v2

    invoke-virtual {v1, v2, v0}, Landroid/content/ContentProviderClient;->insert(Landroid/net/Uri;Landroid/content/ContentValues;)Landroid/net/Uri;
    :try_end_0
    .catch Ljava/lang/Throwable; {:try_start_0 .. :try_end_0} :catch_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    invoke-virtual {v1}, Landroid/content/ContentProviderClient;->release()Z

    :goto_1
    iget-object v0, p0, Lcom/facebook/nodex/startup/splashscreen/AbstractNodexSplashActivity$InitRunnable;->a:Lcom/facebook/nodex/startup/splashscreen/AbstractNodexSplashActivity;

    invoke-static {v0}, Lcom/facebook/nodex/startup/splashscreen/AbstractNodexSplashActivity;->f(Lcom/facebook/nodex/startup/splashscreen/AbstractNodexSplashActivity;)Landroid/os/Handler;

    move-result-object v0

    new-instance v1, Lcom/facebook/nodex/startup/splashscreen/AbstractNodexSplashActivity$InitRunnable$1;

    invoke-direct {v1, p0}, Lcom/facebook/nodex/startup/splashscreen/AbstractNodexSplashActivity$InitRunnable$1;-><init>(Lcom/facebook/nodex/startup/splashscreen/AbstractNodexSplashActivity$InitRunnable;)V

    invoke-virtual {v0, v1}, Landroid/os/Handler;->post(Ljava/lang/Runnable;)Z

    goto :goto_0

    :catch_0
    move-exception v0

    :try_start_1
    iget-object v2, p0, Lcom/facebook/nodex/startup/splashscreen/AbstractNodexSplashActivity$InitRunnable;->a:Lcom/facebook/nodex/startup/splashscreen/AbstractNodexSplashActivity;

    invoke-static {v2}, Lcom/facebook/nodex/startup/splashscreen/AbstractNodexSplashActivity;->d(Lcom/facebook/nodex/startup/splashscreen/AbstractNodexSplashActivity;)Ljava/lang/String;

    move-result-object v2

    const-string v3, "Remote exception from main process."

    invoke-static {v2, v3, v0}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    invoke-virtual {v1}, Landroid/content/ContentProviderClient;->release()Z

    goto :goto_1

    :catchall_0
    move-exception v0

    invoke-virtual {v1}, Landroid/content/ContentProviderClient;->release()Z

    throw v0

    :cond_5
    iget-object v0, p0, Lcom/facebook/nodex/startup/splashscreen/AbstractNodexSplashActivity$InitRunnable;->a:Lcom/facebook/nodex/startup/splashscreen/AbstractNodexSplashActivity;

    invoke-static {v0}, Lcom/facebook/nodex/startup/splashscreen/AbstractNodexSplashActivity;->d(Lcom/facebook/nodex/startup/splashscreen/AbstractNodexSplashActivity;)Ljava/lang/String;

    move-result-object v0

    const-string v1, "Failed to acquire provider to warmup the main process."

    invoke-static {v0, v1}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_1
.end method
