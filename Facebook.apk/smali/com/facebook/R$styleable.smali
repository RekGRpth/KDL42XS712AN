.class public Lcom/facebook/R$styleable;
.super Ljava/lang/Object;
.source "R.java"


# static fields
.field public static final ActionBar:[I

.field public static final ActionBarImageView:[I

.field public static final ActionBarImageView_splitBarBackground:I = 0x1

.field public static final ActionBarImageView_splitBarSrc:I = 0x0

.field public static final ActionBarLayout:[I

.field public static final ActionBarLayout_android_layout_gravity:I = 0x0

.field public static final ActionBarWindow:[I

.field public static final ActionBarWindow_windowActionBar:I = 0x0

.field public static final ActionBarWindow_windowActionBarOverlay:I = 0x1

.field public static final ActionBarWindow_windowSplitActionBar:I = 0x2

.field public static final ActionBar_background:I = 0xa

.field public static final ActionBar_backgroundSplit:I = 0xc

.field public static final ActionBar_backgroundStacked:I = 0xb

.field public static final ActionBar_customNavigationLayout:I = 0xd

.field public static final ActionBar_displayOptions:I = 0x5

.field public static final ActionBar_divider:I = 0x2

.field public static final ActionBar_height:I = 0x3

.field public static final ActionBar_homeLayout:I = 0xe

.field public static final ActionBar_icon:I = 0x8

.field public static final ActionBar_indeterminateProgressStyle:I = 0x10

.field public static final ActionBar_itemPadding:I = 0x12

.field public static final ActionBar_logo:I = 0x9

.field public static final ActionBar_navigationMode:I = 0x4

.field public static final ActionBar_progressBarPadding:I = 0x11

.field public static final ActionBar_progressBarStyle:I = 0xf

.field public static final ActionBar_subtitle:I = 0x1

.field public static final ActionBar_subtitleTextStyle:I = 0x7

.field public static final ActionBar_title:I = 0x0

.field public static final ActionBar_titleTextStyle:I = 0x6

.field public static final ActionMenuItemView:[I

.field public static final ActionMenuItemView_android_minWidth:I = 0x0

.field public static final ActionMenuView:[I

.field public static final ActionMode:[I

.field public static final ActionMode_background:I = 0x3

.field public static final ActionMode_backgroundSplit:I = 0x4

.field public static final ActionMode_height:I = 0x0

.field public static final ActionMode_subtitleTextStyle:I = 0x2

.field public static final ActionMode_titleTextStyle:I = 0x1

.field public static final ActivityChooserView:[I

.field public static final ActivityChooserView_expandActivityOverflowButtonDrawable:I = 0x1

.field public static final ActivityChooserView_initialActivityCount:I = 0x0

.field public static final AdsAttrs:[I

.field public static final AdsAttrs_adSize:I = 0x0

.field public static final AdsAttrs_adSizes:I = 0x1

.field public static final AdsAttrs_adUnitId:I = 0x2

.field public static final AdvancedVerticalLinearLayout:[I

.field public static final AdvancedVerticalLinearLayout_Layout:[I

.field public static final AdvancedVerticalLinearLayout_Layout_layout_isOptional:I = 0x0

.field public static final AdvancedVerticalLinearLayout_Layout_layout_overlapWithPrevious:I = 0x1

.field public static final AdvancedVerticalLinearLayout_spaceSavingThreshold:I = 0x0

.field public static final BadgeTextView:[I

.field public static final BadgeTextViewBadgeAppearance:[I

.field public static final BadgeTextViewBadgeAppearance_android_shadowColor:I = 0x3

.field public static final BadgeTextViewBadgeAppearance_android_shadowDx:I = 0x4

.field public static final BadgeTextViewBadgeAppearance_android_shadowDy:I = 0x5

.field public static final BadgeTextViewBadgeAppearance_android_shadowRadius:I = 0x6

.field public static final BadgeTextViewBadgeAppearance_android_textColor:I = 0x2

.field public static final BadgeTextViewBadgeAppearance_android_textSize:I = 0x0

.field public static final BadgeTextViewBadgeAppearance_android_textStyle:I = 0x1

.field public static final BadgeTextViewStyle:[I

.field public static final BadgeTextViewStyle_badgeTextViewStyle:I = 0x0

.field public static final BadgeTextView_badgeBackground:I = 0x3

.field public static final BadgeTextView_badgePadding:I = 0x2

.field public static final BadgeTextView_badgePlacement:I = 0x4

.field public static final BadgeTextView_badgeText:I = 0x0

.field public static final BadgeTextView_badgeTextAppearance:I = 0x1

.field public static final BadgeableFooterButton:[I

.field public static final BadgeableFooterButton_active_src:I = 0x0

.field public static final BadgeableFooterButton_badge_layout:I = 0x2

.field public static final BadgeableFooterButton_inactive_src:I = 0x1

.field public static final BadgedView:[I

.field public static final BadgedView_facebookBadge:I = 0x0

.field public static final BadgedView_friendPhoneBadge:I = 0x3

.field public static final BadgedView_messengerBadge:I = 0x1

.field public static final BadgedView_phoneBadge:I = 0x2

.field public static final BetterButton:[I

.field public static final BetterButton_allCaps:I = 0x0

.field public static final BetterButton_fontFamily:I = 0x1

.field public static final BetterEditTextView:[I

.field public static final BetterEditTextView_allowImeActionsWithMultiLine:I = 0x2

.field public static final BetterEditTextView_allowPastingSpans:I = 0x3

.field public static final BetterEditTextView_clearTextDrawable:I = 0x1

.field public static final BetterEditTextView_fontFamily:I = 0x0

.field public static final BetterListView:[I

.field public static final BetterListView_enableTranscriptModeWorkaround:I = 0x0

.field public static final BetterRatingBar:[I

.field public static final BetterRatingBar_activeStarDrawable:I = 0x0

.field public static final BetterRatingBar_disableDragToRate:I = 0x4

.field public static final BetterRatingBar_horizontalStarPadding:I = 0x3

.field public static final BetterRatingBar_inactiveStarDrawable:I = 0x1

.field public static final BetterRatingBar_numStars:I = 0x2

.field public static final BetterSwitch:[I

.field public static final BetterSwitch_switchMinHeight:I = 0xb

.field public static final BetterSwitch_switchMinWidth:I = 0xa

.field public static final BetterSwitch_switchPadding:I = 0xc

.field public static final BetterSwitch_switchTextAllCaps:I = 0x7

.field public static final BetterSwitch_switchTextAppearance:I = 0x9

.field public static final BetterSwitch_textColorOff:I = 0x6

.field public static final BetterSwitch_textColorOn:I = 0x5

.field public static final BetterSwitch_textOff:I = 0x4

.field public static final BetterSwitch_textOn:I = 0x3

.field public static final BetterSwitch_thumb:I = 0x0

.field public static final BetterSwitch_thumbTextPadding:I = 0x8

.field public static final BetterSwitch_trackOff:I = 0x2

.field public static final BetterSwitch_trackOn:I = 0x1

.field public static final BetterTextView:[I

.field public static final BetterTextView_adjustLRGravityByTextDirectionCompat:I = 0x5

.field public static final BetterTextView_allCaps:I = 0x0

.field public static final BetterTextView_fontFamily:I = 0x1

.field public static final BetterTextView_fontWeight:I = 0x2

.field public static final BetterTextView_maximallyWideThreshold:I = 0x4

.field public static final BetterTextView_minimallyWide:I = 0x3

.field public static final CardToggleButton:[I

.field public static final CardToggleButton_checkOffColour:I = 0x2

.field public static final CardToggleButton_checkOnColour:I = 0x1

.field public static final CardToggleButton_emptyCircleColour:I = 0x5

.field public static final CardToggleButton_maxCheckSize:I = 0x0

.field public static final CardToggleButton_pressedCheckOffColour:I = 0x4

.field public static final CardToggleButton_pressedCheckOnColour:I = 0x3

.field public static final CheckedContentView:[I

.field public static final CheckedContentView_android_checkMark:I = 0x1

.field public static final CheckedContentView_android_checked:I = 0x0

.field public static final CheckedContentView_checkMarkPosition:I = 0x2

.field public static final CirclePageIndicator:[I

.field public static final CirclePageIndicator_android_orientation:I = 0x0

.field public static final CirclePageIndicator_centered:I = 0x1

.field public static final CirclePageIndicator_fillColor:I = 0x3

.field public static final CirclePageIndicator_pageColor:I = 0x5

.field public static final CirclePageIndicator_radius:I = 0x6

.field public static final CirclePageIndicator_snap:I = 0x7

.field public static final CirclePageIndicator_strokeColor:I = 0x4

.field public static final CirclePageIndicator_strokeWidth:I = 0x2

.field public static final ColourIndicator:[I

.field public static final ColourIndicator_colour_radius:I = 0x0

.field public static final ColourPicker:[I

.field public static final ColourPicker_default_colour:I = 0x0

.field public static final ColourPicker_default_stroke_width:I = 0x1

.field public static final ColourPicker_max_stroke_width:I = 0x3

.field public static final ColourPicker_min_stroke_width:I = 0x2

.field public static final CompatFastScroller:[I

.field public static final CompatFastScroller_fastScrollOverlayPosition:I = 0x5

.field public static final CompatFastScroller_fastScrollPreviewBackgroundLeft:I = 0x3

.field public static final CompatFastScroller_fastScrollPreviewBackgroundRight:I = 0x4

.field public static final CompatFastScroller_fastScrollTextColor:I = 0x0

.field public static final CompatFastScroller_fastScrollThumbDrawable:I = 0x1

.field public static final CompatFastScroller_fastScrollTrackDrawable:I = 0x2

.field public static final CompatTextView:[I

.field public static final CompatTextView_textAllCaps:I = 0x0

.field public static final ComposerActionLayout:[I

.field public static final ComposerActionLayout_Layout:[I

.field public static final ComposerActionLayout_Layout_layout_position:I = 0x0

.field public static final ComposerActionLayout_useFade:I = 0x0

.field public static final CompoundButton:[I

.field public static final CompoundButton_android_button:I = 0x0

.field public static final ConfirmationView:[I

.field public static final ConfirmationView_message:I = 0x0

.field public static final ConfirmationView_negativeButtonTitle:I = 0x2

.field public static final ConfirmationView_positiveButtonTitle:I = 0x1

.field public static final ContainerView:[I

.field public static final ContainerView_dispatchAndroidTouchEvents:I = 0x0

.field public static final ContainerView_reflexAndroidTouchMode:I = 0x1

.field public static final ContentView:[I

.field public static final ContentViewTW1L:[I

.field public static final ContentViewTW1L_imageSrc:I = 0x0

.field public static final ContentViewTW1L_simpleImageView:I = 0x2

.field public static final ContentViewTW1L_titleText:I = 0x1

.field public static final ContentViewTW2L:[I

.field public static final ContentViewTW2L_subtitleText:I = 0x0

.field public static final ContentViewTW3L:[I

.field public static final ContentViewTW3L_metaText:I = 0x0

.field public static final ContentViewWithButton:[I

.field public static final ContentViewWithButton_actionButtonBackground:I = 0x5

.field public static final ContentViewWithButton_actionButtonDrawable:I = 0x3

.field public static final ContentViewWithButton_actionButtonPadding:I = 0x6

.field public static final ContentViewWithButton_actionButtonText:I = 0x4

.field public static final ContentViewWithButton_actionButtonTextAppearance:I = 0x7

.field public static final ContentViewWithButton_actionButtonTheme:I = 0x8

.field public static final ContentViewWithButton_divider:I = 0x0

.field public static final ContentViewWithButton_dividerPadding:I = 0x1

.field public static final ContentViewWithButton_dividerThickness:I = 0x2

.field public static final ContentView_LayoutParams:[I

.field public static final ContentView_LayoutParams_layout_useViewAs:I = 0x0

.field public static final ContentView_metaText:I = 0x2

.field public static final ContentView_metaTextAppearance:I = 0x5

.field public static final ContentView_subtitleText:I = 0x1

.field public static final ContentView_subtitleTextAppearance:I = 0x4

.field public static final ContentView_thumbnailSize:I = 0x6

.field public static final ContentView_titleText:I = 0x0

.field public static final ContentView_titleTextAppearance:I = 0x3

.field public static final ControlledView:[I

.field public static final ControlledView_controller:I = 0x0

.field public static final CountBadge:[I

.field public static final CountBadge_badgeTextColor:I = 0x2

.field public static final CountBadge_checkedCountBadge:I = 0x1

.field public static final CountBadge_uncheckedCountBadge:I = 0x0

.field public static final CustomFrameLayout:[I

.field public static final CustomFrameLayout_traceAs:I = 0x0

.field public static final CustomImageButton:[I

.field public static final CustomImageButton_bottomText:I = 0x1

.field public static final CustomImageButton_fontColor:I = 0x4

.field public static final CustomImageButton_fontSize:I = 0x2

.field public static final CustomImageButton_fontStyle:I = 0x3

.field public static final CustomImageButton_imageSrc:I = 0x0

.field public static final CustomImageButton_textLabelGravity:I = 0x7

.field public static final CustomImageButton_textMaxLines:I = 0x6

.field public static final CustomImageButton_textTopPadding:I = 0x5

.field public static final CustomKeyboardLayout:[I

.field public static final CustomKeyboardLayout_minContentHeight:I = 0x0

.field public static final CustomLinearLayout:[I

.field public static final CustomLinearLayout_traceAs:I = 0x0

.field public static final CustomRelativeLayout:[I

.field public static final CustomRelativeLayout_traceAs:I = 0x0

.field public static final CustomViewPager:[I

.field public static final CustomViewPager_allowDpadPaging:I = 0x2

.field public static final CustomViewPager_initializeHeightToFirstItem:I = 0x1

.field public static final CustomViewPager_isSwipingEnabled:I = 0x0

.field public static final CustomViewStub:[I

.field public static final CustomViewStub_inflatedLayoutAndroidId:I = 0x0

.field public static final DashCustomDialog:[I

.field public static final DashCustomDialog_dialog_background_alpha:I = 0x0

.field public static final DashCustomDialog_dialog_padding:I = 0x1

.field public static final DismissibleFrameLayout:[I

.field public static final DismissibleFrameLayout_dragDirections:I = 0x0

.field public static final DismissibleFrameLayout_swipeAxis:I = 0x1

.field public static final DragSortGridView:[I

.field public static final DragSortGridView_columnWidthCompat:I = 0x1

.field public static final DragSortGridView_minNumColumns:I = 0x0

.field public static final DragSortListView:[I

.field public static final DragSortListView_dragndropBackground:I = 0x3

.field public static final DragSortListView_dragndropImageBackground:I = 0x4

.field public static final DragSortListView_grabberId:I = 0x1

.field public static final DragSortListView_normalHeight:I = 0x0

.field public static final DragSortListView_viewToHideWhileDragging:I = 0x2

.field public static final DrawingView:[I

.field public static final DrawingView_stroke_colour:I = 0x1

.field public static final DrawingView_stroke_width:I = 0x0

.field public static final EllipsizingTextView:[I

.field public static final EllipsizingTextView_ellipsizeLineBreaks:I = 0x0

.field public static final EmojiCategoryPageIndicator:[I

.field public static final EmojiCategoryPageIndicator_track_color:I = 0x3

.field public static final EmojiCategoryPageIndicator_track_indicator_color:I = 0x4

.field public static final EmojiCategoryPageIndicator_track_indicator_width:I = 0x2

.field public static final EmojiCategoryPageIndicator_track_left_padding:I = 0x0

.field public static final EmojiCategoryPageIndicator_track_right_padding:I = 0x1

.field public static final EmptyListViewItem:[I

.field public static final EmptyListViewItem_textColor:I = 0x0

.field public static final EventGuestTileRowView:[I

.field public static final EventGuestTileRowView_guestTileSize:I = 0x0

.field public static final EventGuestTileRowView_guestTitleSpacing:I = 0x1

.field public static final ExpandableHeaderListView:[I

.field public static final ExpandableHeaderListView_expandable:I = 0x0

.field public static final ExpandableText:[I

.field public static final ExpandableText_animateHeightChanges:I = 0x2

.field public static final ExpandableText_canCollapse:I = 0x3

.field public static final ExpandableText_collapsedStateMaxLines:I = 0x4

.field public static final ExpandableText_fadingGradientEndColor:I = 0x1

.field public static final ExpandableText_fadingGradientLength:I = 0x0

.field public static final ExpandingBackgroundEditText:[I

.field public static final ExpandingBackgroundEditText_expandingBackground:I = 0x0

.field public static final FacebookProgressCircleView:[I

.field public static final FacebookProgressCircleView_progressCircleBaseAlpha:I = 0x0

.field public static final FacebookProgressCircleView_progressCircleBaseColor:I = 0x1

.field public static final FacebookProgressCircleView_progressCircleDrawnAlpha:I = 0x2

.field public static final FacebookProgressCircleView_progressCircleDrawnColor:I = 0x3

.field public static final FacebookProgressCircleView_progressCircleEnableFadeIn:I = 0x4

.field public static final FacebookProgressCircleView_progressCircleStrokeWidth:I = 0x5

.field public static final FacebookProgressCircleView_progressCircleWidth:I = 0x6

.field public static final FacepileView:[I

.field public static final FacepileView_circleFaces:I = 0x2

.field public static final FacepileView_faceSize:I = 0x0

.field public static final FacepileView_paddingBetweenFaces:I = 0x1

.field public static final FavoritesDragSortListView:[I

.field public static final FavoritesDragSortListView_bottom_divider:I = 0x1

.field public static final FavoritesDragSortListView_top_divider:I = 0x0

.field public static final Fb4aTitleBar:[I

.field public static final Fb4aTitleBar_hasJewel:I = 0x1

.field public static final Fb4aTitleBar_hasLauncherButton:I = 0x2

.field public static final Fb4aTitleBar_title:I = 0x0

.field public static final FbAutoCompleteTextView:[I

.field public static final FbAutoCompleteTextView_android_hint:I = 0x0

.field public static final FbButton:[I

.field public static final FbButton_android_text:I = 0x0

.field public static final FbEditText:[I

.field public static final FbEditText_android_hint:I = 0x0

.field public static final FbTextView:[I

.field public static final FbTextView_android_text:I = 0x0

.field public static final FeedTheme:[I

.field public static final FeedbackActionButtonBar:[I

.field public static final FeedbackActionButtonBar_downstateType:I = 0x0

.field public static final FlippableView:[I

.field public static final FlippableView_backView:I = 0x1

.field public static final FlippableView_frontView:I = 0x0

.field public static final FlippableView_pressedView:I = 0x2

.field public static final FlowLayout:[I

.field public static final FlowLayout_horizontalSpacing:I = 0x0

.field public static final FlowLayout_verticalSpacing:I = 0x1

.field public static final FooterButton:[I

.field public static final FooterButton_active_src:I = 0x0

.field public static final FooterButton_inactive_src:I = 0x1

.field public static final FractionalRatingBar:[I

.field public static final FractionalRatingBar_emptyStarDrawable:I = 0x2

.field public static final FractionalRatingBar_fullStarDrawable:I = 0x0

.field public static final FractionalRatingBar_halfStarDrawable:I = 0x1

.field public static final FriendingButton:[I

.field public static final FriendingButton_activeSrc:I = 0x0

.field public static final FriendingButton_inactiveSrc:I = 0x1

.field public static final FullScreenVideoPlayer:[I

.field public static final FullScreenVideoPlayer_allowTextureView:I = 0x0

.field public static final Gallery:[I

.field public static final Gallery_android_gravity:I = 0x0

.field public static final Gallery_animationDurationKS:I = 0x1

.field public static final Gallery_spacingKS:I = 0x2

.field public static final Gallery_unselectedAlphaKS:I = 0x3

.field public static final GridLayout:[I

.field public static final GridLayout_Layout:[I

.field public static final GridLayout_Layout_android_layout_height:I = 0x1

.field public static final GridLayout_Layout_android_layout_margin:I = 0x2

.field public static final GridLayout_Layout_android_layout_marginBottom:I = 0x6

.field public static final GridLayout_Layout_android_layout_marginLeft:I = 0x3

.field public static final GridLayout_Layout_android_layout_marginRight:I = 0x5

.field public static final GridLayout_Layout_android_layout_marginTop:I = 0x4

.field public static final GridLayout_Layout_android_layout_width:I = 0x0

.field public static final GridLayout_Layout_layout_column:I = 0x9

.field public static final GridLayout_Layout_layout_columnSpan:I = 0xa

.field public static final GridLayout_Layout_layout_gravity:I = 0xb

.field public static final GridLayout_Layout_layout_row:I = 0x7

.field public static final GridLayout_Layout_layout_rowSpan:I = 0x8

.field public static final GridLayout_alignmentMode:I = 0x4

.field public static final GridLayout_columnCount:I = 0x2

.field public static final GridLayout_columnOrderPreserved:I = 0x6

.field public static final GridLayout_orientation:I = 0x0

.field public static final GridLayout_rowCount:I = 0x1

.field public static final GridLayout_rowOrderPreserved:I = 0x5

.field public static final GridLayout_useDefaultMargins:I = 0x3

.field public static final GridView:[I

.field public static final GridView_columnWidth:I = 0x2

.field public static final GridView_numColumns:I = 0x3

.field public static final GridView_spacingHorizontal:I = 0x0

.field public static final GridView_spacingVertical:I = 0x1

.field public static final GridView_stretchMode:I = 0x4

.field public static final HorizontalImageGallery:[I

.field public static final HorizontalImageGalleryItemIndicator:[I

.field public static final HorizontalImageGalleryItemIndicator_indicator_active_color:I = 0x0

.field public static final HorizontalImageGalleryItemIndicator_indicator_inactive_color:I = 0x1

.field public static final HorizontalImageGallery_left_item_width_percentage:I = 0x0

.field public static final HorizontalImageGallery_support_vertical_scrolling:I = 0x1

.field public static final HorizontalOrVerticalViewGroup:[I

.field public static final HorizontalOrVerticalViewGroup_childMargin:I = 0x0

.field public static final ImageBlockLayout:[I

.field public static final ImageBlockLayout_LayoutParams:[I

.field public static final ImageBlockLayout_LayoutParams_android_layout_gravity:I = 0x0

.field public static final ImageBlockLayout_LayoutParams_layout_useAsAuxView:I = 0x2

.field public static final ImageBlockLayout_LayoutParams_layout_useAsThumbnail:I = 0x1

.field public static final ImageBlockLayout_android_gravity:I = 0x0

.field public static final ImageBlockLayout_android_layout:I = 0x6

.field public static final ImageBlockLayout_android_padding:I = 0x1

.field public static final ImageBlockLayout_android_paddingBottom:I = 0x5

.field public static final ImageBlockLayout_android_paddingLeft:I = 0x2

.field public static final ImageBlockLayout_android_paddingRight:I = 0x4

.field public static final ImageBlockLayout_android_paddingTop:I = 0x3

.field public static final ImageBlockLayout_auxViewPadding:I = 0xc

.field public static final ImageBlockLayout_border:I = 0xe

.field public static final ImageBlockLayout_borderBottom:I = 0x10

.field public static final ImageBlockLayout_borderColor:I = 0xd

.field public static final ImageBlockLayout_borderLeft:I = 0x11

.field public static final ImageBlockLayout_borderRight:I = 0x12

.field public static final ImageBlockLayout_borderTop:I = 0xf

.field public static final ImageBlockLayout_clipBorderToPadding:I = 0x13

.field public static final ImageBlockLayout_overlayDrawable:I = 0x8

.field public static final ImageBlockLayout_thumbnailDrawable:I = 0x7

.field public static final ImageBlockLayout_thumbnailHeight:I = 0xa

.field public static final ImageBlockLayout_thumbnailPadding:I = 0xb

.field public static final ImageBlockLayout_thumbnailWidth:I = 0x9

.field public static final ImageWithTextView:[I

.field public static final ImageWithTextView_drawable:I = 0x0

.field public static final ImageWithTextView_drawableOrientation:I = 0x1

.field public static final InlineActionBar:[I

.field public static final InlineActionBar_ButtonBackgroundStyle:[I

.field public static final InlineActionBar_ButtonBackgroundStyle_buttonCenterBackground:I = 0x1

.field public static final InlineActionBar_ButtonBackgroundStyle_buttonLeftBackground:I = 0x0

.field public static final InlineActionBar_ButtonBackgroundStyle_buttonRightBackground:I = 0x2

.field public static final InlineActionBar_ButtonBackgroundStyle_buttonWholeBackground:I = 0x3

.field public static final InlineActionBar_buttonBackgroundStyle:I = 0x4

.field public static final InlineActionBar_buttonOrientation:I = 0x1

.field public static final InlineActionBar_inlineActionBarOverflowButtonStyle:I = 0x6

.field public static final InlineActionBar_maxNumOfVisibleButtons:I = 0x3

.field public static final InlineActionBar_menu:I = 0x0

.field public static final InlineActionBar_overflowStyle:I = 0x2

.field public static final InlineActionBar_textAppearance:I = 0x5

.field public static final InlineVideoPlayer:[I

.field public static final InlineVideoPlayer_playButtonLayout:I = 0x0

.field public static final IorgTextView:[I

.field public static final IorgTextView_android_text:I = 0x0

.field public static final LastMessageViewHelper:[I

.field public static final LastMessageViewHelper_iconAlignment:I = 0x0

.field public static final LinearLayoutICS:[I

.field public static final LinearLayoutICS_divider:I = 0x0

.field public static final LinearLayoutICS_dividerPadding:I = 0x2

.field public static final LinearLayoutICS_showDividers:I = 0x1

.field public static final LinearLayoutListView:[I

.field public static final LinearLayoutListView_innerDividerDrawable:I = 0x2

.field public static final LinearLayoutListView_outerDividerDrawableBottom:I = 0x1

.field public static final LinearLayoutListView_outerDividerDrawableTop:I = 0x0

.field public static final ListView:[I

.field public static final ListView_longpressSelectorColor:I = 0x1

.field public static final ListView_reflexListviewOverscrollEnabled:I = 0x2

.field public static final ListView_selectorColor:I = 0x0

.field public static final LoadingIndicatorView:[I

.field public static final LoadingIndicatorView_contentLayout:I = 0x6

.field public static final LoadingIndicatorView_errorOrientation:I = 0x1

.field public static final LoadingIndicatorView_errorPaddingBottom:I = 0x5

.field public static final LoadingIndicatorView_errorPaddingTop:I = 0x4

.field public static final LoadingIndicatorView_imageHeight:I = 0x3

.field public static final LoadingIndicatorView_imageSize:I = 0x0

.field public static final LoadingIndicatorView_imageWidth:I = 0x2

.field public static final LocalActivityFragment:[I

.field public static final LocalActivityFragment_activityClass:I = 0x0

.field public static final MapAttrs:[I

.field public static final MapAttrs_cameraBearing:I = 0x1

.field public static final MapAttrs_cameraTargetLat:I = 0x2

.field public static final MapAttrs_cameraTargetLng:I = 0x3

.field public static final MapAttrs_cameraTilt:I = 0x4

.field public static final MapAttrs_cameraZoom:I = 0x5

.field public static final MapAttrs_mapType:I = 0x0

.field public static final MapAttrs_uiCompass:I = 0x6

.field public static final MapAttrs_uiRotateGestures:I = 0x7

.field public static final MapAttrs_uiScrollGestures:I = 0x8

.field public static final MapAttrs_uiTiltGestures:I = 0x9

.field public static final MapAttrs_uiZoomControls:I = 0xa

.field public static final MapAttrs_uiZoomGestures:I = 0xb

.field public static final MapAttrs_useViewLifecycle:I = 0xc

.field public static final MapAttrs_zOrderOnTop:I = 0xd

.field public static final MapImage:[I

.field public static final MapImage_keepMarkerAtCenter:I = 0x2

.field public static final MapImage_markerColor:I = 0x1

.field public static final MapImage_overrideZeroRating:I = 0x4

.field public static final MapImage_retainMapDuringUpdate:I = 0x3

.field public static final MapImage_zoom:I = 0x0

.field public static final MaskedFrameLayout:[I

.field public static final MaskedFrameLayout_foreground:I = 0x1

.field public static final MaskedFrameLayout_mask:I = 0x0

.field public static final MaskedFrameLayout_usesFboToMask:I = 0x2

.field public static final MaxWidthFrameLayout:[I

.field public static final MaxWidthFrameLayout_maximumWidth:I = 0x0

.field public static final Megaphone:[I

.field public static final Megaphone_megaphoneImage:I = 0x4

.field public static final Megaphone_primaryButtonText:I = 0x2

.field public static final Megaphone_secondaryButtonText:I = 0x3

.field public static final Megaphone_subtitleText:I = 0x1

.field public static final Megaphone_titleText:I = 0x0

.field public static final MemberBarAttrs:[I

.field public static final MemberBarAttrs_maxMembersToDisplay:I = 0x2

.field public static final MemberBarAttrs_paddingBetweenPogs:I = 0x3

.field public static final MemberBarAttrs_showAddMemberPog:I = 0x0

.field public static final MemberBarAttrs_showMemberCountPog:I = 0x1

.field public static final MenuGroup:[I

.field public static final MenuGroup_android_checkableBehavior:I = 0x5

.field public static final MenuGroup_android_enabled:I = 0x0

.field public static final MenuGroup_android_id:I = 0x1

.field public static final MenuGroup_android_menuCategory:I = 0x3

.field public static final MenuGroup_android_orderInCategory:I = 0x4

.field public static final MenuGroup_android_visible:I = 0x2

.field public static final MenuItem:[I

.field public static final MenuItem_actionLayout:I = 0xe

.field public static final MenuItem_actionProviderClass:I = 0x10

.field public static final MenuItem_actionViewClass:I = 0xf

.field public static final MenuItem_android_alphabeticShortcut:I = 0x9

.field public static final MenuItem_android_checkable:I = 0xb

.field public static final MenuItem_android_checked:I = 0x3

.field public static final MenuItem_android_enabled:I = 0x1

.field public static final MenuItem_android_icon:I = 0x0

.field public static final MenuItem_android_id:I = 0x2

.field public static final MenuItem_android_menuCategory:I = 0x5

.field public static final MenuItem_android_numericShortcut:I = 0xa

.field public static final MenuItem_android_onClick:I = 0xc

.field public static final MenuItem_android_orderInCategory:I = 0x6

.field public static final MenuItem_android_title:I = 0x7

.field public static final MenuItem_android_titleCondensed:I = 0x8

.field public static final MenuItem_android_visible:I = 0x4

.field public static final MenuItem_showAsAction:I = 0xd

.field public static final MenuView:[I

.field public static final MenuView_android_headerBackground:I = 0x4

.field public static final MenuView_android_horizontalDivider:I = 0x2

.field public static final MenuView_android_itemBackground:I = 0x5

.field public static final MenuView_android_itemIconDisabledAlpha:I = 0x6

.field public static final MenuView_android_itemTextAppearance:I = 0x1

.field public static final MenuView_android_preserveIconSpacing:I = 0x7

.field public static final MenuView_android_verticalDivider:I = 0x3

.field public static final MenuView_android_windowAnimationStyle:I = 0x0

.field public static final MessengerNotificationProfileView:[I

.field public static final MessengerNotificationProfileView_dividerColor:I = 0x1

.field public static final MessengerNotificationProfileView_dividerSize:I = 0x0

.field public static final MinutiaeTextView:[I

.field public static final MinutiaeTextView_android_autoLink:I = 0x2

.field public static final MinutiaeTextView_android_maxLines:I = 0x3

.field public static final MinutiaeTextView_android_textColor:I = 0x1

.field public static final MinutiaeTextView_android_textSize:I = 0x0

.field public static final MultilineEllipsizeTextView:[I

.field public static final MultilineEllipsizeTextView_includeFontPadding:I = 0x8

.field public static final MultilineEllipsizeTextView_maxLines:I = 0x1

.field public static final MultilineEllipsizeTextView_maxWidth:I = 0x6

.field public static final MultilineEllipsizeTextView_minHeight:I = 0x7

.field public static final MultilineEllipsizeTextView_minLines:I = 0x2

.field public static final MultilineEllipsizeTextView_shadowColor:I = 0xc

.field public static final MultilineEllipsizeTextView_shadowDx:I = 0x9

.field public static final MultilineEllipsizeTextView_shadowDy:I = 0xa

.field public static final MultilineEllipsizeTextView_shadowRadius:I = 0xb

.field public static final MultilineEllipsizeTextView_textColor:I = 0x0

.field public static final MultilineEllipsizeTextView_textSize:I = 0x5

.field public static final MultilineEllipsizeTextView_textStyle:I = 0x4

.field public static final MultilineEllipsizeTextView_typeface:I = 0x3

.field public static final NavigationButtonView:[I

.field public static final NavigationButtonView_navigation_button_view_theme:I = 0x2

.field public static final NavigationButtonView_primary_button_text:I = 0x1

.field public static final NavigationButtonView_secondary_button_text:I = 0x0

.field public static final NotificationTextSwitcher:[I

.field public static final NotificationTextSwitcher_textColor:I = 0x0

.field public static final NotificationTextSwitcher_textSize:I = 0x2

.field public static final NotificationTextSwitcher_textStyle:I = 0x1

.field public static final NuxBubbleView:[I

.field public static final NuxBubbleView_bodyBackground:I = 0x0

.field public static final NuxBubbleView_bodyText:I = 0x1

.field public static final NuxBubbleView_nubAlign:I = 0x3

.field public static final NuxBubbleView_nubMargin:I = 0x4

.field public static final NuxBubbleView_nubPosition:I = 0x2

.field public static final OrcaTabWidget:[I

.field public static final OrcaTabWidget_divider:I = 0x0

.field public static final OrcaTabWidget_tabStripEnabled:I = 0x1

.field public static final OrcaTabWidget_tabStripLeft:I = 0x2

.field public static final OrcaTabWidget_tabStripRight:I = 0x3

.field public static final OverlayLayout_Layout:[I

.field public static final OverlayLayout_Layout_layout_anchorPosition:I = 0x0

.field public static final OverlayLayout_Layout_layout_anchoredTo:I = 0x2

.field public static final OverlayLayout_Layout_layout_isOverlay:I = 0x1

.field public static final OverlayLayout_Layout_layout_xOffset:I = 0x3

.field public static final OverlayLayout_Layout_layout_yOffset:I = 0x4

.field public static final PageViewPlaceholder:[I

.field public static final PageViewPlaceholder_transferBackground:I = 0x1

.field public static final PageViewPlaceholder_transferPadding:I = 0x0

.field public static final PhotoToggleButton:[I

.field public static final PhotoToggleButton_checkedContentDescription:I = 0x3

.field public static final PhotoToggleButton_checkedImage:I = 0x1

.field public static final PhotoToggleButton_shouldBounce:I = 0x4

.field public static final PhotoToggleButton_uncheckedContentDescription:I = 0x2

.field public static final PhotoToggleButton_uncheckedImage:I = 0x0

.field public static final PopoverMenuItem:[I

.field public static final PopoverMenuItem_android_checkable:I = 0x8

.field public static final PopoverMenuItem_android_checked:I = 0x4

.field public static final PopoverMenuItem_android_description:I = 0x2

.field public static final PopoverMenuItem_android_enabled:I = 0x1

.field public static final PopoverMenuItem_android_icon:I = 0x0

.field public static final PopoverMenuItem_android_id:I = 0x3

.field public static final PopoverMenuItem_android_orderInCategory:I = 0x6

.field public static final PopoverMenuItem_android_title:I = 0x7

.field public static final PopoverMenuItem_android_visible:I = 0x5

.field public static final PopoverMenuItem_badgeText:I = 0x9

.field public static final PresenceIndicatorView:[I

.field public static final PresenceIndicatorView_alignment:I = 0x1

.field public static final PresenceIndicatorView_presenceIndicatorIconStyle:I = 0x2

.field public static final PresenceIndicatorView_textColor:I = 0x0

.field public static final ProgressCircleAnimation:[I

.field public static final ProgressCircleAnimation_circleStrokeWidth:I = 0x0

.field public static final PullToRefreshListView:[I

.field public static final PullToRefreshListView_refreshDirection:I = 0x0

.field public static final RadioButtonWithSubtitle:[I

.field public static final RadioButtonWithSubtitle_android_text:I = 0x0

.field public static final RadioButtonWithSubtitle_subtitle:I = 0x1

.field public static final ReceiptItemView:[I

.field public static final ReceiptItemView_for_me_user:I = 0x0

.field public static final RefreshableListViewContainer:[I

.field public static final RefreshableListViewContainer_overflowAndListOverlap:I = 0x0

.field public static final RefreshableViewItem:[I

.field public static final RefreshableViewItem_enablePtrMask:I = 0x0

.field public static final RoundedBitmapView:[I

.field public static final RoundedBitmapView_foregroundDrawable:I = 0x0

.field public static final RoundedBitmapView_scaling:I = 0x1

.field public static final RoundedCornerSelectorButton:[I

.field public static final RoundedCornerSelectorButton_showChevronOnRight:I = 0x6

.field public static final RoundedCornerSelectorButton_text:I = 0x3

.field public static final RoundedCornerSelectorButton_textColor:I = 0x0

.field public static final RoundedCornerSelectorButton_textGravity:I = 0x5

.field public static final RoundedCornerSelectorButton_textSize:I = 0x4

.field public static final RoundedCornerSelectorButton_textStyle:I = 0x2

.field public static final RoundedCornerSelectorButton_typeface:I = 0x1

.field public static final RoundedView:[I

.field public static final RoundedView_asCircle:I = 0x0

.field public static final RoundedView_cornerRadius:I = 0x5

.field public static final RoundedView_isBottomLeftRounded:I = 0x4

.field public static final RoundedView_isBottomRightRounded:I = 0x3

.field public static final RoundedView_isTopLeftRounded:I = 0x1

.field public static final RoundedView_isTopRightRounded:I = 0x2

.field public static final RoundedView_roundBorderColor:I = 0x8

.field public static final RoundedView_roundBorderWidth:I = 0x7

.field public static final RoundedView_roundByOverlayingColor:I = 0x6

.field public static final RowView:[I

.field public static final RowView_detail_text:I = 0x1

.field public static final RowView_primary_text:I = 0x0

.field public static final SearchView:[I

.field public static final SearchView_android_imeOptions:I = 0x2

.field public static final SearchView_android_inputType:I = 0x1

.field public static final SearchView_android_maxWidth:I = 0x0

.field public static final SearchView_iconifiedByDefault:I = 0x3

.field public static final SearchView_queryHint:I = 0x4

.field public static final SearchView_searchViewHintMinFontSize:I = 0x5

.field public static final SegmentedLinearLayout:[I

.field public static final SegmentedLinearLayout_divider:I = 0x0

.field public static final SegmentedLinearLayout_dividerPadding:I = 0x2

.field public static final SegmentedLinearLayout_dividerPaddingEnd:I = 0x5

.field public static final SegmentedLinearLayout_dividerPaddingStart:I = 0x4

.field public static final SegmentedLinearLayout_dividerThickness:I = 0x3

.field public static final SegmentedLinearLayout_showDividers:I = 0x1

.field public static final SharePreviewAttributes:[I

.field public static final SharePreviewAttributes_suppressBackground:I = 0x0

.field public static final ShimmerFrameLayout:[I

.field public static final ShimmerFrameLayout_angle:I = 0x6

.field public static final ShimmerFrameLayout_auto_start:I = 0x0

.field public static final ShimmerFrameLayout_base_alpha:I = 0x1

.field public static final ShimmerFrameLayout_dropoff:I = 0x7

.field public static final ShimmerFrameLayout_duration:I = 0x2

.field public static final ShimmerFrameLayout_fixed_height:I = 0x9

.field public static final ShimmerFrameLayout_fixed_width:I = 0x8

.field public static final ShimmerFrameLayout_intensity:I = 0xa

.field public static final ShimmerFrameLayout_relative_height:I = 0xc

.field public static final ShimmerFrameLayout_relative_width:I = 0xb

.field public static final ShimmerFrameLayout_repeat_count:I = 0x3

.field public static final ShimmerFrameLayout_repeat_delay:I = 0x4

.field public static final ShimmerFrameLayout_repeat_mode:I = 0x5

.field public static final ShimmerFrameLayout_shape:I = 0xd

.field public static final ShimmerFrameLayout_tilt:I = 0xe

.field public static final SimpleVariableTextLayoutView:[I

.field public static final SimpleVariableTextLayoutView_text:I = 0x0

.field public static final SlidingOutSuggestionView:[I

.field public static final SlidingOutSuggestionView_animateOutDirection:I = 0x1

.field public static final SlidingOutSuggestionView_buttonText:I = 0x3

.field public static final SlidingOutSuggestionView_dividerPosition:I = 0x0

.field public static final SlidingOutSuggestionView_suggestionText:I = 0x2

.field public static final SoundWaveView:[I

.field public static final SoundWaveView_barColor:I = 0x1

.field public static final SoundWaveView_distanceBetweenBars:I = 0x0

.field public static final SoundWaveView_numberOfBars:I = 0x2

.field public static final Spinner:[I

.field public static final Spinner_android_dropDownHorizontalOffset:I = 0x4

.field public static final Spinner_android_dropDownSelector:I = 0x1

.field public static final Spinner_android_dropDownVerticalOffset:I = 0x5

.field public static final Spinner_android_dropDownWidth:I = 0x3

.field public static final Spinner_android_gravity:I = 0x0

.field public static final Spinner_android_popupBackground:I = 0x2

.field public static final Spinner_disableChildrenWhenDisabled:I = 0x9

.field public static final Spinner_popupPromptView:I = 0x8

.field public static final Spinner_prompt:I = 0x6

.field public static final Spinner_spinnerMode:I = 0x7

.field public static final SplitHideableListView:[I

.field public static final SplitHideableListView_disableScrollHideList:I = 0x4

.field public static final SplitHideableListView_headerHideThreshold:I = 0x3

.field public static final SplitHideableListView_headerStartHeight:I = 0x2

.field public static final SplitHideableListView_listHideThreshold:I = 0x1

.field public static final SplitHideableListView_listStartHeight:I = 0x0

.field public static final StickerStoreListView:[I

.field public static final StickerStoreListView_stickerStoreItemBottomDiv:I = 0x1

.field public static final StickerStoreListView_stickerStoreItemBottomSlot:I = 0x3

.field public static final StickerStoreListView_stickerStoreItemTopDiv:I = 0x0

.field public static final StickerStoreListView_stickerStoreItemTopSlot:I = 0x2

.field public static final SwitchCompat:[I

.field public static final SwitchCompat_android_switchMinWidth:I = 0x5

.field public static final SwitchCompat_android_switchPadding:I = 0x6

.field public static final SwitchCompat_android_switchTextAppearance:I = 0x3

.field public static final SwitchCompat_android_textOff:I = 0x1

.field public static final SwitchCompat_android_textOn:I = 0x0

.field public static final SwitchCompat_android_thumb:I = 0x2

.field public static final SwitchCompat_android_thumbTextPadding:I = 0x7

.field public static final SwitchCompat_android_track:I = 0x4

.field public static final TabbedPageView:[I

.field public static final TabbedPageView_emptyMessage:I = 0x0

.field public static final TabbedViewPagerIndicator:[I

.field public static final TabbedViewPagerIndicator_divider:I = 0x0

.field public static final TabbedViewPagerIndicator_dividerPadding:I = 0x1

.field public static final TabbedViewPagerIndicator_dividerThickness:I = 0x2

.field public static final TabbedViewPagerIndicator_tabLayout:I = 0x5

.field public static final TabbedViewPagerIndicator_underlineColor:I = 0x3

.field public static final TabbedViewPagerIndicator_underlineHeight:I = 0x4

.field public static final TextAppearanceBetterSwitch:[I

.field public static final TextAppearanceBetterSwitch_switchTextColor:I = 0x0

.field public static final TextAppearanceBetterSwitch_switchTextSize:I = 0x1

.field public static final TextAppearanceBetterSwitch_switchTextStyle:I = 0x2

.field public static final TextAppearanceBetterSwitch_switchTypeface:I = 0x3

.field public static final TextViewWithTextWrappingDrawables:[I

.field public static final TextViewWithTextWrappingDrawables_textWrappingDrawableBottom:I = 0x3

.field public static final TextViewWithTextWrappingDrawables_textWrappingDrawableLeft:I = 0x4

.field public static final TextViewWithTextWrappingDrawables_textWrappingDrawablePadding:I = 0x0

.field public static final TextViewWithTextWrappingDrawables_textWrappingDrawableRight:I = 0x2

.field public static final TextViewWithTextWrappingDrawables_textWrappingDrawableTop:I = 0x1

.field public static final TextViewWithTruncationContainer:[I

.field public static final TextViewWithTruncationContainer_maxLines:I = 0x0

.field public static final TextWithEntitiesView:[I

.field public static final TextWithEntitiesView_highlightColor:I = 0x0

.field public static final TextWithEntitiesView_highlightStyle:I = 0x1

.field public static final Theme:[I

.field public static final Theme_actionDropDownStyle:I = 0x0

.field public static final Theme_dropdownListPreferredItemHeight:I = 0x1

.field public static final Theme_listChoiceBackgroundIndicator:I = 0x5

.field public static final Theme_panelMenuListTheme:I = 0x4

.field public static final Theme_panelMenuListWidth:I = 0x3

.field public static final Theme_popupMenuStyle:I = 0x2

.field public static final ThemedLayout:[I

.field public static final ThemedLayout_themed_layout:I = 0x0

.field public static final ThreadNameView:[I

.field public static final ThreadNameView_alignment:I = 0x6

.field public static final ThreadNameView_maxLines:I = 0x1

.field public static final ThreadNameView_maxScaledTextSize:I = 0x5

.field public static final ThreadNameView_minScaledTextSize:I = 0x4

.field public static final ThreadNameView_nameOption:I = 0x7

.field public static final ThreadNameView_textColor:I = 0x0

.field public static final ThreadNameView_textStyle:I = 0x3

.field public static final ThreadNameView_typeface:I = 0x2

.field public static final ThreadTileView:[I

.field public static final ThreadTileView_bigImageLocation:I = 0x2

.field public static final ThreadTileView_bigImageWidthPercent:I = 0x1

.field public static final ThreadTileView_facebookBadge:I = 0x4

.field public static final ThreadTileView_friendPhoneBadge:I = 0x7

.field public static final ThreadTileView_messengerBadge:I = 0x5

.field public static final ThreadTileView_overlayDivider:I = 0x3

.field public static final ThreadTileView_phoneBadge:I = 0x6

.field public static final ThreadTileView_threadTileSize:I = 0x0

.field public static final ThreadTitleView:[I

.field public static final ThreadTitleView_chatStyle:I = 0x0

.field public static final ThreadViewDetailsItem:[I

.field public static final ThreadViewDetailsItem_hintText:I = 0x2

.field public static final ThreadViewDetailsItem_iconSrc:I = 0x0

.field public static final ThreadViewDetailsItem_itemTitle:I = 0x1

.field public static final ThreadViewImageAttachmentView:[I

.field public static final ThreadViewImageAttachmentView_scaleType:I = 0x0

.field public static final TiledView:[I

.field public static final TiledView_bitmapSrc:I = 0x0

.field public static final TiledView_fromRight:I = 0x1

.field public static final TiledView_mode_vertical:I = 0x2

.field public static final TimelineFriendMenuItem:[I

.field public static final TimelineFriendMenuItem_checked:I = 0x2

.field public static final TimelineFriendMenuItem_hasCheckBox:I = 0x1

.field public static final TimelineFriendMenuItem_text:I = 0x0

.field public static final TitleBar:[I

.field public static final TitleBarButton:[I

.field public static final TitleBarButton_dividerPosition:I = 0x0

.field public static final TitleBarButton_src:I = 0x1

.field public static final TitleBarView:[I

.field public static final TitleBarViewStub:[I

.field public static final TitleBarViewStub_centerTitle:I = 0x3

.field public static final TitleBarViewStub_hasBackButton:I = 0x1

.field public static final TitleBarViewStub_navless:I = 0x4

.field public static final TitleBarViewStub_title:I = 0x0

.field public static final TitleBarViewStub_useActionBar:I = 0x2

.field public static final TitleBarView_accessory_text:I = 0x1

.field public static final TitleBarView_show_back_chevron:I = 0x2

.field public static final TitleBarView_title_text:I = 0x0

.field public static final TitleBar_centerTitle:I = 0x2

.field public static final TitleBar_hasBackButton:I = 0x1

.field public static final TitleBar_title:I = 0x0

.field public static final TokenizedAutoCompleteTextView:[I

.field public static final TokenizedAutoCompleteTextView_tokenBackgroundDrawable:I = 0x2

.field public static final TokenizedAutoCompleteTextView_tokenTextColor:I = 0x0

.field public static final TokenizedAutoCompleteTextView_tokenTextSize:I = 0x1

.field public static final UrlImage:[I

.field public static final UrlImage_adjustViewBounds:I = 0xa

.field public static final UrlImage_isShownInGallery:I = 0x3

.field public static final UrlImage_isUsedWithUploadProgress:I = 0x6

.field public static final UrlImage_placeHolderScaleType:I = 0xc

.field public static final UrlImage_placeholderSrc:I = 0x1

.field public static final UrlImage_pressedOverlayColor:I = 0xb

.field public static final UrlImage_retainImageDuringUpdate:I = 0x8

.field public static final UrlImage_scaleType:I = 0xd

.field public static final UrlImage_shouldShowLoadingAnimation:I = 0x9

.field public static final UrlImage_showProgressBar:I = 0x2

.field public static final UrlImage_url:I = 0x0

.field public static final UrlImage_useCoverView:I = 0x7

.field public static final UrlImage_useQuickContactBadge:I = 0x5

.field public static final UrlImage_useZoomableImageView:I = 0x4

.field public static final UserTileRowView:[I

.field public static final UserTileRowView_childMargin:I = 0x1

.field public static final UserTileRowView_tileSize:I = 0x0

.field public static final UserTileView:[I

.field public static final UserTileView_defaultUserTileColor:I = 0x2

.field public static final UserTileView_defaultUserTileSrc:I = 0x1

.field public static final UserTileView_tileSize:I = 0x0

.field public static final ValueBasedSoundWaveView:[I

.field public static final ValueBasedSoundWaveView_barColor:I = 0x1

.field public static final ValueBasedSoundWaveView_distanceBetweenBars:I = 0x0

.field public static final ValueBasedSoundWaveView_numberOfBars:I = 0x2

.field public static final VariableTextLayoutView:[I

.field public static final VariableTextLayoutView_alignment:I = 0x6

.field public static final VariableTextLayoutView_fontFamily:I = 0x7

.field public static final VariableTextLayoutView_maxLines:I = 0x1

.field public static final VariableTextLayoutView_maxScaledTextSize:I = 0x5

.field public static final VariableTextLayoutView_minScaledTextSize:I = 0x4

.field public static final VariableTextLayoutView_textColor:I = 0x0

.field public static final VariableTextLayoutView_textStyle:I = 0x3

.field public static final VariableTextLayoutView_typeface:I = 0x2

.field public static final VideoTrimmingMetadataView:[I

.field public static final VideoTrimmingMetadataView_bodyTextColor:I = 0x2

.field public static final VideoTrimmingMetadataView_label:I = 0x0

.field public static final VideoTrimmingMetadataView_labelTextColor:I = 0x1

.field public static final View:[I

.field public static final ViewPager:[I

.field public static final ViewPagerIndicator:[I

.field public static final ViewPagerIndicator_vpiCirclePageIndicatorStyle:I = 0x0

.field public static final ViewPagerIndicator_vpiIconPageIndicatorStyle:I = 0x2

.field public static final ViewPagerIndicator_vpiTabPageIndicatorStyle:I = 0x1

.field public static final ViewPager_reflexViewPagerOverscrollEnabled:I = 0x0

.field public static final View_android_focusable:I = 0x0

.field public static final View_paddingEnd:I = 0x2

.field public static final View_paddingStart:I = 0x1

.field public static final WalletFragmentOptions:[I

.field public static final WalletFragmentOptions_environment:I = 0x1

.field public static final WalletFragmentOptions_fragmentMode:I = 0x3

.field public static final WalletFragmentOptions_fragmentStyle:I = 0x2

.field public static final WalletFragmentOptions_theme:I = 0x0

.field public static final WalletFragmentStyle:[I

.field public static final WalletFragmentStyle_buyButtonAppearance:I = 0x3

.field public static final WalletFragmentStyle_buyButtonHeight:I = 0x0

.field public static final WalletFragmentStyle_buyButtonText:I = 0x2

.field public static final WalletFragmentStyle_buyButtonWidth:I = 0x1

.field public static final WalletFragmentStyle_maskedWalletDetailsBackground:I = 0x6

.field public static final WalletFragmentStyle_maskedWalletDetailsButtonBackground:I = 0x8

.field public static final WalletFragmentStyle_maskedWalletDetailsButtonTextAppearance:I = 0x7

.field public static final WalletFragmentStyle_maskedWalletDetailsHeaderTextAppearance:I = 0x5

.field public static final WalletFragmentStyle_maskedWalletDetailsLogoImageType:I = 0xa

.field public static final WalletFragmentStyle_maskedWalletDetailsLogoTextColor:I = 0x9

.field public static final WalletFragmentStyle_maskedWalletDetailsTextAppearance:I = 0x4

.field public static final ZigzagImageView:[I

.field public static final ZigzagImageView_foregroundColor:I


# direct methods
.method static constructor <clinit>()V
    .locals 7

    const/4 v6, 0x5

    const/4 v5, 0x2

    const/4 v4, 0x3

    const/4 v3, 0x1

    const/4 v2, 0x0

    const/16 v0, 0x13

    new-array v0, v0, [I

    fill-array-data v0, :array_0

    sput-object v0, Lcom/facebook/R$styleable;->ActionBar:[I

    new-array v0, v5, [I

    fill-array-data v0, :array_1

    sput-object v0, Lcom/facebook/R$styleable;->ActionBarImageView:[I

    new-array v0, v3, [I

    const v1, 0x10100b3    # android.R.attr.layout_gravity

    aput v1, v0, v2

    sput-object v0, Lcom/facebook/R$styleable;->ActionBarLayout:[I

    new-array v0, v4, [I

    fill-array-data v0, :array_2

    sput-object v0, Lcom/facebook/R$styleable;->ActionBarWindow:[I

    new-array v0, v3, [I

    const v1, 0x101013f    # android.R.attr.minWidth

    aput v1, v0, v2

    sput-object v0, Lcom/facebook/R$styleable;->ActionMenuItemView:[I

    new-array v0, v2, [I

    sput-object v0, Lcom/facebook/R$styleable;->ActionMenuView:[I

    new-array v0, v6, [I

    fill-array-data v0, :array_3

    sput-object v0, Lcom/facebook/R$styleable;->ActionMode:[I

    new-array v0, v5, [I

    fill-array-data v0, :array_4

    sput-object v0, Lcom/facebook/R$styleable;->ActivityChooserView:[I

    new-array v0, v4, [I

    fill-array-data v0, :array_5

    sput-object v0, Lcom/facebook/R$styleable;->AdsAttrs:[I

    new-array v0, v3, [I

    const v1, 0x7f010155    # com.facebook.katana.R.attr.spaceSavingThreshold

    aput v1, v0, v2

    sput-object v0, Lcom/facebook/R$styleable;->AdvancedVerticalLinearLayout:[I

    new-array v0, v5, [I

    fill-array-data v0, :array_6

    sput-object v0, Lcom/facebook/R$styleable;->AdvancedVerticalLinearLayout_Layout:[I

    new-array v0, v6, [I

    fill-array-data v0, :array_7

    sput-object v0, Lcom/facebook/R$styleable;->BadgeTextView:[I

    const/4 v0, 0x7

    new-array v0, v0, [I

    fill-array-data v0, :array_8

    sput-object v0, Lcom/facebook/R$styleable;->BadgeTextViewBadgeAppearance:[I

    new-array v0, v3, [I

    const v1, 0x7f0102d7    # com.facebook.katana.R.attr.badgeTextViewStyle

    aput v1, v0, v2

    sput-object v0, Lcom/facebook/R$styleable;->BadgeTextViewStyle:[I

    new-array v0, v4, [I

    fill-array-data v0, :array_9

    sput-object v0, Lcom/facebook/R$styleable;->BadgeableFooterButton:[I

    const/4 v0, 0x4

    new-array v0, v0, [I

    fill-array-data v0, :array_a

    sput-object v0, Lcom/facebook/R$styleable;->BadgedView:[I

    new-array v0, v5, [I

    fill-array-data v0, :array_b

    sput-object v0, Lcom/facebook/R$styleable;->BetterButton:[I

    const/4 v0, 0x4

    new-array v0, v0, [I

    fill-array-data v0, :array_c

    sput-object v0, Lcom/facebook/R$styleable;->BetterEditTextView:[I

    new-array v0, v3, [I

    const v1, 0x7f010161    # com.facebook.katana.R.attr.enableTranscriptModeWorkaround

    aput v1, v0, v2

    sput-object v0, Lcom/facebook/R$styleable;->BetterListView:[I

    new-array v0, v6, [I

    fill-array-data v0, :array_d

    sput-object v0, Lcom/facebook/R$styleable;->BetterRatingBar:[I

    const/16 v0, 0xd

    new-array v0, v0, [I

    fill-array-data v0, :array_e

    sput-object v0, Lcom/facebook/R$styleable;->BetterSwitch:[I

    const/4 v0, 0x6

    new-array v0, v0, [I

    fill-array-data v0, :array_f

    sput-object v0, Lcom/facebook/R$styleable;->BetterTextView:[I

    const/4 v0, 0x6

    new-array v0, v0, [I

    fill-array-data v0, :array_10

    sput-object v0, Lcom/facebook/R$styleable;->CardToggleButton:[I

    new-array v0, v4, [I

    fill-array-data v0, :array_11

    sput-object v0, Lcom/facebook/R$styleable;->CheckedContentView:[I

    const/16 v0, 0x8

    new-array v0, v0, [I

    fill-array-data v0, :array_12

    sput-object v0, Lcom/facebook/R$styleable;->CirclePageIndicator:[I

    new-array v0, v3, [I

    const v1, 0x7f01029c    # com.facebook.katana.R.attr.colour_radius

    aput v1, v0, v2

    sput-object v0, Lcom/facebook/R$styleable;->ColourIndicator:[I

    const/4 v0, 0x4

    new-array v0, v0, [I

    fill-array-data v0, :array_13

    sput-object v0, Lcom/facebook/R$styleable;->ColourPicker:[I

    const/4 v0, 0x6

    new-array v0, v0, [I

    fill-array-data v0, :array_14

    sput-object v0, Lcom/facebook/R$styleable;->CompatFastScroller:[I

    new-array v0, v3, [I

    const v1, 0x7f010108    # com.facebook.katana.R.attr.textAllCaps

    aput v1, v0, v2

    sput-object v0, Lcom/facebook/R$styleable;->CompatTextView:[I

    new-array v0, v3, [I

    const v1, 0x7f010290    # com.facebook.katana.R.attr.useFade

    aput v1, v0, v2

    sput-object v0, Lcom/facebook/R$styleable;->ComposerActionLayout:[I

    new-array v0, v3, [I

    const v1, 0x7f010291    # com.facebook.katana.R.attr.layout_position

    aput v1, v0, v2

    sput-object v0, Lcom/facebook/R$styleable;->ComposerActionLayout_Layout:[I

    new-array v0, v3, [I

    const v1, 0x1010107    # android.R.attr.button

    aput v1, v0, v2

    sput-object v0, Lcom/facebook/R$styleable;->CompoundButton:[I

    new-array v0, v4, [I

    fill-array-data v0, :array_15

    sput-object v0, Lcom/facebook/R$styleable;->ConfirmationView:[I

    new-array v0, v5, [I

    fill-array-data v0, :array_16

    sput-object v0, Lcom/facebook/R$styleable;->ContainerView:[I

    const/4 v0, 0x7

    new-array v0, v0, [I

    fill-array-data v0, :array_17

    sput-object v0, Lcom/facebook/R$styleable;->ContentView:[I

    new-array v0, v4, [I

    fill-array-data v0, :array_18

    sput-object v0, Lcom/facebook/R$styleable;->ContentViewTW1L:[I

    new-array v0, v3, [I

    const v1, 0x7f01010c    # com.facebook.katana.R.attr.subtitleText

    aput v1, v0, v2

    sput-object v0, Lcom/facebook/R$styleable;->ContentViewTW2L:[I

    new-array v0, v3, [I

    const v1, 0x7f01010d    # com.facebook.katana.R.attr.metaText

    aput v1, v0, v2

    sput-object v0, Lcom/facebook/R$styleable;->ContentViewTW3L:[I

    const/16 v0, 0x9

    new-array v0, v0, [I

    fill-array-data v0, :array_19

    sput-object v0, Lcom/facebook/R$styleable;->ContentViewWithButton:[I

    new-array v0, v3, [I

    const v1, 0x7f010128    # com.facebook.katana.R.attr.layout_useViewAs

    aput v1, v0, v2

    sput-object v0, Lcom/facebook/R$styleable;->ContentView_LayoutParams:[I

    new-array v0, v3, [I

    const v1, 0x7f0102e3    # com.facebook.katana.R.attr.controller

    aput v1, v0, v2

    sput-object v0, Lcom/facebook/R$styleable;->ControlledView:[I

    new-array v0, v4, [I

    fill-array-data v0, :array_1a

    sput-object v0, Lcom/facebook/R$styleable;->CountBadge:[I

    new-array v0, v3, [I

    const v1, 0x7f010136    # com.facebook.katana.R.attr.traceAs

    aput v1, v0, v2

    sput-object v0, Lcom/facebook/R$styleable;->CustomFrameLayout:[I

    const/16 v0, 0x8

    new-array v0, v0, [I

    fill-array-data v0, :array_1b

    sput-object v0, Lcom/facebook/R$styleable;->CustomImageButton:[I

    new-array v0, v3, [I

    const v1, 0x7f01016c    # com.facebook.katana.R.attr.minContentHeight

    aput v1, v0, v2

    sput-object v0, Lcom/facebook/R$styleable;->CustomKeyboardLayout:[I

    new-array v0, v3, [I

    const v1, 0x7f010136    # com.facebook.katana.R.attr.traceAs

    aput v1, v0, v2

    sput-object v0, Lcom/facebook/R$styleable;->CustomLinearLayout:[I

    new-array v0, v3, [I

    const v1, 0x7f010136    # com.facebook.katana.R.attr.traceAs

    aput v1, v0, v2

    sput-object v0, Lcom/facebook/R$styleable;->CustomRelativeLayout:[I

    new-array v0, v4, [I

    fill-array-data v0, :array_1c

    sput-object v0, Lcom/facebook/R$styleable;->CustomViewPager:[I

    new-array v0, v3, [I

    const v1, 0x7f010058    # com.facebook.katana.R.attr.inflatedLayoutAndroidId

    aput v1, v0, v2

    sput-object v0, Lcom/facebook/R$styleable;->CustomViewStub:[I

    new-array v0, v5, [I

    fill-array-data v0, :array_1d

    sput-object v0, Lcom/facebook/R$styleable;->DashCustomDialog:[I

    new-array v0, v5, [I

    fill-array-data v0, :array_1e

    sput-object v0, Lcom/facebook/R$styleable;->DismissibleFrameLayout:[I

    new-array v0, v5, [I

    fill-array-data v0, :array_1f

    sput-object v0, Lcom/facebook/R$styleable;->DragSortGridView:[I

    new-array v0, v6, [I

    fill-array-data v0, :array_20

    sput-object v0, Lcom/facebook/R$styleable;->DragSortListView:[I

    new-array v0, v5, [I

    fill-array-data v0, :array_21

    sput-object v0, Lcom/facebook/R$styleable;->DrawingView:[I

    new-array v0, v3, [I

    const v1, 0x7f010071    # com.facebook.katana.R.attr.ellipsizeLineBreaks

    aput v1, v0, v2

    sput-object v0, Lcom/facebook/R$styleable;->EllipsizingTextView:[I

    new-array v0, v6, [I

    fill-array-data v0, :array_22

    sput-object v0, Lcom/facebook/R$styleable;->EmojiCategoryPageIndicator:[I

    new-array v0, v3, [I

    const v1, 0x7f010001    # com.facebook.katana.R.attr.textColor

    aput v1, v0, v2

    sput-object v0, Lcom/facebook/R$styleable;->EmptyListViewItem:[I

    new-array v0, v5, [I

    fill-array-data v0, :array_23

    sput-object v0, Lcom/facebook/R$styleable;->EventGuestTileRowView:[I

    new-array v0, v3, [I

    const v1, 0x7f0102e4    # com.facebook.katana.R.attr.expandable

    aput v1, v0, v2

    sput-object v0, Lcom/facebook/R$styleable;->ExpandableHeaderListView:[I

    new-array v0, v6, [I

    fill-array-data v0, :array_24

    sput-object v0, Lcom/facebook/R$styleable;->ExpandableText:[I

    new-array v0, v3, [I

    const v1, 0x7f01028f    # com.facebook.katana.R.attr.expandingBackground

    aput v1, v0, v2

    sput-object v0, Lcom/facebook/R$styleable;->ExpandingBackgroundEditText:[I

    const/4 v0, 0x7

    new-array v0, v0, [I

    fill-array-data v0, :array_25

    sput-object v0, Lcom/facebook/R$styleable;->FacebookProgressCircleView:[I

    new-array v0, v4, [I

    fill-array-data v0, :array_26

    sput-object v0, Lcom/facebook/R$styleable;->FacepileView:[I

    new-array v0, v5, [I

    fill-array-data v0, :array_27

    sput-object v0, Lcom/facebook/R$styleable;->FavoritesDragSortListView:[I

    new-array v0, v4, [I

    fill-array-data v0, :array_28

    sput-object v0, Lcom/facebook/R$styleable;->Fb4aTitleBar:[I

    new-array v0, v3, [I

    const v1, 0x1010150    # android.R.attr.hint

    aput v1, v0, v2

    sput-object v0, Lcom/facebook/R$styleable;->FbAutoCompleteTextView:[I

    new-array v0, v3, [I

    const v1, 0x101014f    # android.R.attr.text

    aput v1, v0, v2

    sput-object v0, Lcom/facebook/R$styleable;->FbButton:[I

    new-array v0, v3, [I

    const v1, 0x1010150    # android.R.attr.hint

    aput v1, v0, v2

    sput-object v0, Lcom/facebook/R$styleable;->FbEditText:[I

    new-array v0, v3, [I

    const v1, 0x101014f    # android.R.attr.text

    aput v1, v0, v2

    sput-object v0, Lcom/facebook/R$styleable;->FbTextView:[I

    new-array v0, v2, [I

    sput-object v0, Lcom/facebook/R$styleable;->FeedTheme:[I

    new-array v0, v3, [I

    const v1, 0x7f0102a5    # com.facebook.katana.R.attr.downstateType

    aput v1, v0, v2

    sput-object v0, Lcom/facebook/R$styleable;->FeedbackActionButtonBar:[I

    new-array v0, v4, [I

    fill-array-data v0, :array_29

    sput-object v0, Lcom/facebook/R$styleable;->FlippableView:[I

    new-array v0, v5, [I

    fill-array-data v0, :array_2a

    sput-object v0, Lcom/facebook/R$styleable;->FlowLayout:[I

    new-array v0, v5, [I

    fill-array-data v0, :array_2b

    sput-object v0, Lcom/facebook/R$styleable;->FooterButton:[I

    new-array v0, v4, [I

    fill-array-data v0, :array_2c

    sput-object v0, Lcom/facebook/R$styleable;->FractionalRatingBar:[I

    new-array v0, v5, [I

    fill-array-data v0, :array_2d

    sput-object v0, Lcom/facebook/R$styleable;->FriendingButton:[I

    new-array v0, v3, [I

    const v1, 0x7f010176    # com.facebook.katana.R.attr.allowTextureView

    aput v1, v0, v2

    sput-object v0, Lcom/facebook/R$styleable;->FullScreenVideoPlayer:[I

    const/4 v0, 0x4

    new-array v0, v0, [I

    fill-array-data v0, :array_2e

    sput-object v0, Lcom/facebook/R$styleable;->Gallery:[I

    const/4 v0, 0x7

    new-array v0, v0, [I

    fill-array-data v0, :array_2f

    sput-object v0, Lcom/facebook/R$styleable;->GridLayout:[I

    const/16 v0, 0xc

    new-array v0, v0, [I

    fill-array-data v0, :array_30

    sput-object v0, Lcom/facebook/R$styleable;->GridLayout_Layout:[I

    new-array v0, v6, [I

    fill-array-data v0, :array_31

    sput-object v0, Lcom/facebook/R$styleable;->GridView:[I

    new-array v0, v5, [I

    fill-array-data v0, :array_32

    sput-object v0, Lcom/facebook/R$styleable;->HorizontalImageGallery:[I

    new-array v0, v5, [I

    fill-array-data v0, :array_33

    sput-object v0, Lcom/facebook/R$styleable;->HorizontalImageGalleryItemIndicator:[I

    new-array v0, v3, [I

    const v1, 0x7f010061    # com.facebook.katana.R.attr.childMargin

    aput v1, v0, v2

    sput-object v0, Lcom/facebook/R$styleable;->HorizontalOrVerticalViewGroup:[I

    const/16 v0, 0x14

    new-array v0, v0, [I

    fill-array-data v0, :array_34

    sput-object v0, Lcom/facebook/R$styleable;->ImageBlockLayout:[I

    new-array v0, v4, [I

    fill-array-data v0, :array_35

    sput-object v0, Lcom/facebook/R$styleable;->ImageBlockLayout_LayoutParams:[I

    new-array v0, v5, [I

    fill-array-data v0, :array_36

    sput-object v0, Lcom/facebook/R$styleable;->ImageWithTextView:[I

    const/4 v0, 0x7

    new-array v0, v0, [I

    fill-array-data v0, :array_37

    sput-object v0, Lcom/facebook/R$styleable;->InlineActionBar:[I

    const/4 v0, 0x4

    new-array v0, v0, [I

    fill-array-data v0, :array_38

    sput-object v0, Lcom/facebook/R$styleable;->InlineActionBar_ButtonBackgroundStyle:[I

    new-array v0, v3, [I

    const v1, 0x7f010177    # com.facebook.katana.R.attr.playButtonLayout

    aput v1, v0, v2

    sput-object v0, Lcom/facebook/R$styleable;->InlineVideoPlayer:[I

    new-array v0, v3, [I

    const v1, 0x101014f    # android.R.attr.text

    aput v1, v0, v2

    sput-object v0, Lcom/facebook/R$styleable;->IorgTextView:[I

    new-array v0, v3, [I

    const v1, 0x7f01018a    # com.facebook.katana.R.attr.iconAlignment

    aput v1, v0, v2

    sput-object v0, Lcom/facebook/R$styleable;->LastMessageViewHelper:[I

    new-array v0, v4, [I

    fill-array-data v0, :array_39

    sput-object v0, Lcom/facebook/R$styleable;->LinearLayoutICS:[I

    new-array v0, v4, [I

    fill-array-data v0, :array_3a

    sput-object v0, Lcom/facebook/R$styleable;->LinearLayoutListView:[I

    new-array v0, v4, [I

    fill-array-data v0, :array_3b

    sput-object v0, Lcom/facebook/R$styleable;->ListView:[I

    const/4 v0, 0x7

    new-array v0, v0, [I

    fill-array-data v0, :array_3c

    sput-object v0, Lcom/facebook/R$styleable;->LoadingIndicatorView:[I

    new-array v0, v3, [I

    const/high16 v1, 0x7f010000    # com.facebook.katana.R.attr.activityClass

    aput v1, v0, v2

    sput-object v0, Lcom/facebook/R$styleable;->LocalActivityFragment:[I

    const/16 v0, 0xe

    new-array v0, v0, [I

    fill-array-data v0, :array_3d

    sput-object v0, Lcom/facebook/R$styleable;->MapAttrs:[I

    new-array v0, v6, [I

    fill-array-data v0, :array_3e

    sput-object v0, Lcom/facebook/R$styleable;->MapImage:[I

    new-array v0, v4, [I

    fill-array-data v0, :array_3f

    sput-object v0, Lcom/facebook/R$styleable;->MaskedFrameLayout:[I

    new-array v0, v3, [I

    const v1, 0x7f010068    # com.facebook.katana.R.attr.maximumWidth

    aput v1, v0, v2

    sput-object v0, Lcom/facebook/R$styleable;->MaxWidthFrameLayout:[I

    new-array v0, v6, [I

    fill-array-data v0, :array_40

    sput-object v0, Lcom/facebook/R$styleable;->Megaphone:[I

    const/4 v0, 0x4

    new-array v0, v0, [I

    fill-array-data v0, :array_41

    sput-object v0, Lcom/facebook/R$styleable;->MemberBarAttrs:[I

    const/4 v0, 0x6

    new-array v0, v0, [I

    fill-array-data v0, :array_42

    sput-object v0, Lcom/facebook/R$styleable;->MenuGroup:[I

    const/16 v0, 0x11

    new-array v0, v0, [I

    fill-array-data v0, :array_43

    sput-object v0, Lcom/facebook/R$styleable;->MenuItem:[I

    const/16 v0, 0x8

    new-array v0, v0, [I

    fill-array-data v0, :array_44

    sput-object v0, Lcom/facebook/R$styleable;->MenuView:[I

    new-array v0, v5, [I

    fill-array-data v0, :array_45

    sput-object v0, Lcom/facebook/R$styleable;->MessengerNotificationProfileView:[I

    const/4 v0, 0x4

    new-array v0, v0, [I

    fill-array-data v0, :array_46

    sput-object v0, Lcom/facebook/R$styleable;->MinutiaeTextView:[I

    const/16 v0, 0xd

    new-array v0, v0, [I

    fill-array-data v0, :array_47

    sput-object v0, Lcom/facebook/R$styleable;->MultilineEllipsizeTextView:[I

    new-array v0, v4, [I

    fill-array-data v0, :array_48

    sput-object v0, Lcom/facebook/R$styleable;->NavigationButtonView:[I

    new-array v0, v4, [I

    fill-array-data v0, :array_49

    sput-object v0, Lcom/facebook/R$styleable;->NotificationTextSwitcher:[I

    new-array v0, v6, [I

    fill-array-data v0, :array_4a

    sput-object v0, Lcom/facebook/R$styleable;->NuxBubbleView:[I

    const/4 v0, 0x4

    new-array v0, v0, [I

    fill-array-data v0, :array_4b

    sput-object v0, Lcom/facebook/R$styleable;->OrcaTabWidget:[I

    new-array v0, v6, [I

    fill-array-data v0, :array_4c

    sput-object v0, Lcom/facebook/R$styleable;->OverlayLayout_Layout:[I

    new-array v0, v5, [I

    fill-array-data v0, :array_4d

    sput-object v0, Lcom/facebook/R$styleable;->PageViewPlaceholder:[I

    new-array v0, v6, [I

    fill-array-data v0, :array_4e

    sput-object v0, Lcom/facebook/R$styleable;->PhotoToggleButton:[I

    const/16 v0, 0xa

    new-array v0, v0, [I

    fill-array-data v0, :array_4f

    sput-object v0, Lcom/facebook/R$styleable;->PopoverMenuItem:[I

    new-array v0, v4, [I

    fill-array-data v0, :array_50

    sput-object v0, Lcom/facebook/R$styleable;->PresenceIndicatorView:[I

    new-array v0, v3, [I

    const v1, 0x7f0102d5    # com.facebook.katana.R.attr.circleStrokeWidth

    aput v1, v0, v2

    sput-object v0, Lcom/facebook/R$styleable;->ProgressCircleAnimation:[I

    new-array v0, v3, [I

    const v1, 0x7f010033    # com.facebook.katana.R.attr.refreshDirection

    aput v1, v0, v2

    sput-object v0, Lcom/facebook/R$styleable;->PullToRefreshListView:[I

    new-array v0, v5, [I

    fill-array-data v0, :array_51

    sput-object v0, Lcom/facebook/R$styleable;->RadioButtonWithSubtitle:[I

    new-array v0, v3, [I

    const v1, 0x7f01019e    # com.facebook.katana.R.attr.for_me_user

    aput v1, v0, v2

    sput-object v0, Lcom/facebook/R$styleable;->ReceiptItemView:[I

    new-array v0, v3, [I

    const v1, 0x7f010048    # com.facebook.katana.R.attr.overflowAndListOverlap

    aput v1, v0, v2

    sput-object v0, Lcom/facebook/R$styleable;->RefreshableListViewContainer:[I

    new-array v0, v3, [I

    const v1, 0x7f010183    # com.facebook.katana.R.attr.enablePtrMask

    aput v1, v0, v2

    sput-object v0, Lcom/facebook/R$styleable;->RefreshableViewItem:[I

    new-array v0, v5, [I

    fill-array-data v0, :array_52

    sput-object v0, Lcom/facebook/R$styleable;->RoundedBitmapView:[I

    const/4 v0, 0x7

    new-array v0, v0, [I

    fill-array-data v0, :array_53

    sput-object v0, Lcom/facebook/R$styleable;->RoundedCornerSelectorButton:[I

    const/16 v0, 0x9

    new-array v0, v0, [I

    fill-array-data v0, :array_54

    sput-object v0, Lcom/facebook/R$styleable;->RoundedView:[I

    new-array v0, v5, [I

    fill-array-data v0, :array_55

    sput-object v0, Lcom/facebook/R$styleable;->RowView:[I

    const/4 v0, 0x6

    new-array v0, v0, [I

    fill-array-data v0, :array_56

    sput-object v0, Lcom/facebook/R$styleable;->SearchView:[I

    const/4 v0, 0x6

    new-array v0, v0, [I

    fill-array-data v0, :array_57

    sput-object v0, Lcom/facebook/R$styleable;->SegmentedLinearLayout:[I

    new-array v0, v3, [I

    const v1, 0x7f010172    # com.facebook.katana.R.attr.suppressBackground

    aput v1, v0, v2

    sput-object v0, Lcom/facebook/R$styleable;->SharePreviewAttributes:[I

    const/16 v0, 0xf

    new-array v0, v0, [I

    fill-array-data v0, :array_58

    sput-object v0, Lcom/facebook/R$styleable;->ShimmerFrameLayout:[I

    new-array v0, v3, [I

    const v1, 0x7f010010    # com.facebook.katana.R.attr.text

    aput v1, v0, v2

    sput-object v0, Lcom/facebook/R$styleable;->SimpleVariableTextLayoutView:[I

    const/4 v0, 0x4

    new-array v0, v0, [I

    fill-array-data v0, :array_59

    sput-object v0, Lcom/facebook/R$styleable;->SlidingOutSuggestionView:[I

    new-array v0, v4, [I

    fill-array-data v0, :array_5a

    sput-object v0, Lcom/facebook/R$styleable;->SoundWaveView:[I

    const/16 v0, 0xa

    new-array v0, v0, [I

    fill-array-data v0, :array_5b

    sput-object v0, Lcom/facebook/R$styleable;->Spinner:[I

    new-array v0, v6, [I

    fill-array-data v0, :array_5c

    sput-object v0, Lcom/facebook/R$styleable;->SplitHideableListView:[I

    const/4 v0, 0x4

    new-array v0, v0, [I

    fill-array-data v0, :array_5d

    sput-object v0, Lcom/facebook/R$styleable;->StickerStoreListView:[I

    const/16 v0, 0x8

    new-array v0, v0, [I

    fill-array-data v0, :array_5e

    sput-object v0, Lcom/facebook/R$styleable;->SwitchCompat:[I

    new-array v0, v3, [I

    const v1, 0x7f010042    # com.facebook.katana.R.attr.emptyMessage

    aput v1, v0, v2

    sput-object v0, Lcom/facebook/R$styleable;->TabbedPageView:[I

    const/4 v0, 0x6

    new-array v0, v0, [I

    fill-array-data v0, :array_5f

    sput-object v0, Lcom/facebook/R$styleable;->TabbedViewPagerIndicator:[I

    const/4 v0, 0x4

    new-array v0, v0, [I

    fill-array-data v0, :array_60

    sput-object v0, Lcom/facebook/R$styleable;->TextAppearanceBetterSwitch:[I

    new-array v0, v6, [I

    fill-array-data v0, :array_61

    sput-object v0, Lcom/facebook/R$styleable;->TextViewWithTextWrappingDrawables:[I

    new-array v0, v3, [I

    const v1, 0x7f010008    # com.facebook.katana.R.attr.maxLines

    aput v1, v0, v2

    sput-object v0, Lcom/facebook/R$styleable;->TextViewWithTruncationContainer:[I

    new-array v0, v5, [I

    fill-array-data v0, :array_62

    sput-object v0, Lcom/facebook/R$styleable;->TextWithEntitiesView:[I

    const/4 v0, 0x6

    new-array v0, v0, [I

    fill-array-data v0, :array_63

    sput-object v0, Lcom/facebook/R$styleable;->Theme:[I

    new-array v0, v3, [I

    const v1, 0x7f0102a6    # com.facebook.katana.R.attr.themed_layout

    aput v1, v0, v2

    sput-object v0, Lcom/facebook/R$styleable;->ThemedLayout:[I

    const/16 v0, 0x8

    new-array v0, v0, [I

    fill-array-data v0, :array_64

    sput-object v0, Lcom/facebook/R$styleable;->ThreadNameView:[I

    const/16 v0, 0x8

    new-array v0, v0, [I

    fill-array-data v0, :array_65

    sput-object v0, Lcom/facebook/R$styleable;->ThreadTileView:[I

    new-array v0, v3, [I

    const v1, 0x7f010188    # com.facebook.katana.R.attr.chatStyle

    aput v1, v0, v2

    sput-object v0, Lcom/facebook/R$styleable;->ThreadTitleView:[I

    new-array v0, v4, [I

    fill-array-data v0, :array_66

    sput-object v0, Lcom/facebook/R$styleable;->ThreadViewDetailsItem:[I

    new-array v0, v3, [I

    const v1, 0x7f010041    # com.facebook.katana.R.attr.scaleType

    aput v1, v0, v2

    sput-object v0, Lcom/facebook/R$styleable;->ThreadViewImageAttachmentView:[I

    new-array v0, v4, [I

    fill-array-data v0, :array_67

    sput-object v0, Lcom/facebook/R$styleable;->TiledView:[I

    new-array v0, v4, [I

    fill-array-data v0, :array_68

    sput-object v0, Lcom/facebook/R$styleable;->TimelineFriendMenuItem:[I

    new-array v0, v4, [I

    fill-array-data v0, :array_69

    sput-object v0, Lcom/facebook/R$styleable;->TitleBar:[I

    new-array v0, v5, [I

    fill-array-data v0, :array_6a

    sput-object v0, Lcom/facebook/R$styleable;->TitleBarButton:[I

    new-array v0, v4, [I

    fill-array-data v0, :array_6b

    sput-object v0, Lcom/facebook/R$styleable;->TitleBarView:[I

    new-array v0, v6, [I

    fill-array-data v0, :array_6c

    sput-object v0, Lcom/facebook/R$styleable;->TitleBarViewStub:[I

    new-array v0, v4, [I

    fill-array-data v0, :array_6d

    sput-object v0, Lcom/facebook/R$styleable;->TokenizedAutoCompleteTextView:[I

    const/16 v0, 0xe

    new-array v0, v0, [I

    fill-array-data v0, :array_6e

    sput-object v0, Lcom/facebook/R$styleable;->UrlImage:[I

    new-array v0, v5, [I

    fill-array-data v0, :array_6f

    sput-object v0, Lcom/facebook/R$styleable;->UserTileRowView:[I

    new-array v0, v4, [I

    fill-array-data v0, :array_70

    sput-object v0, Lcom/facebook/R$styleable;->UserTileView:[I

    new-array v0, v4, [I

    fill-array-data v0, :array_71

    sput-object v0, Lcom/facebook/R$styleable;->ValueBasedSoundWaveView:[I

    const/16 v0, 0x8

    new-array v0, v0, [I

    fill-array-data v0, :array_72

    sput-object v0, Lcom/facebook/R$styleable;->VariableTextLayoutView:[I

    new-array v0, v4, [I

    fill-array-data v0, :array_73

    sput-object v0, Lcom/facebook/R$styleable;->VideoTrimmingMetadataView:[I

    new-array v0, v4, [I

    fill-array-data v0, :array_74

    sput-object v0, Lcom/facebook/R$styleable;->View:[I

    new-array v0, v3, [I

    const v1, 0x7f01009b    # com.facebook.katana.R.attr.reflexViewPagerOverscrollEnabled

    aput v1, v0, v2

    sput-object v0, Lcom/facebook/R$styleable;->ViewPager:[I

    new-array v0, v4, [I

    fill-array-data v0, :array_75

    sput-object v0, Lcom/facebook/R$styleable;->ViewPagerIndicator:[I

    const/4 v0, 0x4

    new-array v0, v0, [I

    fill-array-data v0, :array_76

    sput-object v0, Lcom/facebook/R$styleable;->WalletFragmentOptions:[I

    const/16 v0, 0xb

    new-array v0, v0, [I

    fill-array-data v0, :array_77

    sput-object v0, Lcom/facebook/R$styleable;->WalletFragmentStyle:[I

    new-array v0, v3, [I

    const v1, 0x7f010065    # com.facebook.katana.R.attr.foregroundColor

    aput v1, v0, v2

    sput-object v0, Lcom/facebook/R$styleable;->ZigzagImageView:[I

    return-void

    nop

    :array_0
    .array-data 4
        0x7f010059    # com.facebook.katana.R.attr.title
        0x7f01005a    # com.facebook.katana.R.attr.subtitle
        0x7f010060    # com.facebook.katana.R.attr.divider
        0x7f0100c2    # com.facebook.katana.R.attr.height
        0x7f0100c3    # com.facebook.katana.R.attr.navigationMode
        0x7f0100c4    # com.facebook.katana.R.attr.displayOptions
        0x7f0100c5    # com.facebook.katana.R.attr.titleTextStyle
        0x7f0100c6    # com.facebook.katana.R.attr.subtitleTextStyle
        0x7f0100c7    # com.facebook.katana.R.attr.icon
        0x7f0100c8    # com.facebook.katana.R.attr.logo
        0x7f0100c9    # com.facebook.katana.R.attr.background
        0x7f0100ca    # com.facebook.katana.R.attr.backgroundStacked
        0x7f0100cb    # com.facebook.katana.R.attr.backgroundSplit
        0x7f0100cc    # com.facebook.katana.R.attr.customNavigationLayout
        0x7f0100cd    # com.facebook.katana.R.attr.homeLayout
        0x7f0100ce    # com.facebook.katana.R.attr.progressBarStyle
        0x7f0100cf    # com.facebook.katana.R.attr.indeterminateProgressStyle
        0x7f0100d0    # com.facebook.katana.R.attr.progressBarPadding
        0x7f0100d1    # com.facebook.katana.R.attr.itemPadding
    .end array-data

    :array_1
    .array-data 4
        0x7f01009f    # com.facebook.katana.R.attr.splitBarSrc
        0x7f0100a0    # com.facebook.katana.R.attr.splitBarBackground
    .end array-data

    :array_2
    .array-data 4
        0x7f01009c    # com.facebook.katana.R.attr.windowActionBar
        0x7f01009d    # com.facebook.katana.R.attr.windowActionBarOverlay
        0x7f01009e    # com.facebook.katana.R.attr.windowSplitActionBar
    .end array-data

    :array_3
    .array-data 4
        0x7f0100c2    # com.facebook.katana.R.attr.height
        0x7f0100c5    # com.facebook.katana.R.attr.titleTextStyle
        0x7f0100c6    # com.facebook.katana.R.attr.subtitleTextStyle
        0x7f0100c9    # com.facebook.katana.R.attr.background
        0x7f0100cb    # com.facebook.katana.R.attr.backgroundSplit
    .end array-data

    :array_4
    .array-data 4
        0x7f010105    # com.facebook.katana.R.attr.initialActivityCount
        0x7f010106    # com.facebook.katana.R.attr.expandActivityOverflowButtonDrawable
    .end array-data

    :array_5
    .array-data 4
        0x7f0102ac    # com.facebook.katana.R.attr.adSize
        0x7f0102ad    # com.facebook.katana.R.attr.adSizes
        0x7f0102ae    # com.facebook.katana.R.attr.adUnitId
    .end array-data

    :array_6
    .array-data 4
        0x7f010156    # com.facebook.katana.R.attr.layout_isOptional
        0x7f010157    # com.facebook.katana.R.attr.layout_overlapWithPrevious
    .end array-data

    :array_7
    .array-data 4
        0x7f0102d8    # com.facebook.katana.R.attr.badgeText
        0x7f0102d9    # com.facebook.katana.R.attr.badgeTextAppearance
        0x7f0102da    # com.facebook.katana.R.attr.badgePadding
        0x7f0102db    # com.facebook.katana.R.attr.badgeBackground
        0x7f0102dc    # com.facebook.katana.R.attr.badgePlacement
    .end array-data

    :array_8
    .array-data 4
        0x1010095    # android.R.attr.textSize
        0x1010097    # android.R.attr.textStyle
        0x1010098    # android.R.attr.textColor
        0x1010161    # android.R.attr.shadowColor
        0x1010162    # android.R.attr.shadowDx
        0x1010163    # android.R.attr.shadowDy
        0x1010164    # android.R.attr.shadowRadius
    .end array-data

    :array_9
    .array-data 4
        0x7f0102d2    # com.facebook.katana.R.attr.active_src
        0x7f0102d3    # com.facebook.katana.R.attr.inactive_src
        0x7f0102d4    # com.facebook.katana.R.attr.badge_layout
    .end array-data

    :array_a
    .array-data 4
        0x7f01006d    # com.facebook.katana.R.attr.facebookBadge
        0x7f01006e    # com.facebook.katana.R.attr.messengerBadge
        0x7f01006f    # com.facebook.katana.R.attr.phoneBadge
        0x7f010070    # com.facebook.katana.R.attr.friendPhoneBadge
    .end array-data

    :array_b
    .array-data 4
        0x7f01000c    # com.facebook.katana.R.attr.allCaps
        0x7f010049    # com.facebook.katana.R.attr.fontFamily
    .end array-data

    :array_c
    .array-data 4
        0x7f010049    # com.facebook.katana.R.attr.fontFamily
        0x7f01004b    # com.facebook.katana.R.attr.clearTextDrawable
        0x7f01004c    # com.facebook.katana.R.attr.allowImeActionsWithMultiLine
        0x7f01004d    # com.facebook.katana.R.attr.allowPastingSpans
    .end array-data

    :array_d
    .array-data 4
        0x7f0102a0    # com.facebook.katana.R.attr.activeStarDrawable
        0x7f0102a1    # com.facebook.katana.R.attr.inactiveStarDrawable
        0x7f0102a2    # com.facebook.katana.R.attr.numStars
        0x7f0102a3    # com.facebook.katana.R.attr.horizontalStarPadding
        0x7f0102a4    # com.facebook.katana.R.attr.disableDragToRate
    .end array-data

    :array_e
    .array-data 4
        0x7f010017    # com.facebook.katana.R.attr.thumb
        0x7f010018    # com.facebook.katana.R.attr.trackOn
        0x7f010019    # com.facebook.katana.R.attr.trackOff
        0x7f01001a    # com.facebook.katana.R.attr.textOn
        0x7f01001b    # com.facebook.katana.R.attr.textOff
        0x7f01001c    # com.facebook.katana.R.attr.textColorOn
        0x7f01001d    # com.facebook.katana.R.attr.textColorOff
        0x7f01001e    # com.facebook.katana.R.attr.switchTextAllCaps
        0x7f01001f    # com.facebook.katana.R.attr.thumbTextPadding
        0x7f010020    # com.facebook.katana.R.attr.switchTextAppearance
        0x7f010021    # com.facebook.katana.R.attr.switchMinWidth
        0x7f010022    # com.facebook.katana.R.attr.switchMinHeight
        0x7f010023    # com.facebook.katana.R.attr.switchPadding
    .end array-data

    :array_f
    .array-data 4
        0x7f01000c    # com.facebook.katana.R.attr.allCaps
        0x7f010049    # com.facebook.katana.R.attr.fontFamily
        0x7f01004a    # com.facebook.katana.R.attr.fontWeight
        0x7f01004e    # com.facebook.katana.R.attr.minimallyWide
        0x7f01004f    # com.facebook.katana.R.attr.maximallyWideThreshold
        0x7f010050    # com.facebook.katana.R.attr.adjustLRGravityByTextDirectionCompat
    .end array-data

    :array_10
    .array-data 4
        0x7f0102e5    # com.facebook.katana.R.attr.maxCheckSize
        0x7f0102e6    # com.facebook.katana.R.attr.checkOnColour
        0x7f0102e7    # com.facebook.katana.R.attr.checkOffColour
        0x7f0102e8    # com.facebook.katana.R.attr.pressedCheckOnColour
        0x7f0102e9    # com.facebook.katana.R.attr.pressedCheckOffColour
        0x7f0102ea    # com.facebook.katana.R.attr.emptyCircleColour
    .end array-data

    :array_11
    .array-data 4
        0x1010106    # android.R.attr.checked
        0x1010108    # android.R.attr.checkMark
        0x7f010131    # com.facebook.katana.R.attr.checkMarkPosition
    .end array-data

    :array_12
    .array-data 4
        0x10100c4    # android.R.attr.orientation
        0x7f010002    # com.facebook.katana.R.attr.centered
        0x7f010004    # com.facebook.katana.R.attr.strokeWidth
        0x7f010006    # com.facebook.katana.R.attr.fillColor
        0x7f010007    # com.facebook.katana.R.attr.strokeColor
        0x7f0102e0    # com.facebook.katana.R.attr.pageColor
        0x7f0102e1    # com.facebook.katana.R.attr.radius
        0x7f0102e2    # com.facebook.katana.R.attr.snap
    .end array-data

    :array_13
    .array-data 4
        0x7f010298    # com.facebook.katana.R.attr.default_colour
        0x7f010299    # com.facebook.katana.R.attr.default_stroke_width
        0x7f01029a    # com.facebook.katana.R.attr.min_stroke_width
        0x7f01029b    # com.facebook.katana.R.attr.max_stroke_width
    .end array-data

    :array_14
    .array-data 4
        0x7f01014a    # com.facebook.katana.R.attr.fastScrollTextColor
        0x7f01014b    # com.facebook.katana.R.attr.fastScrollThumbDrawable
        0x7f01014c    # com.facebook.katana.R.attr.fastScrollTrackDrawable
        0x7f01014d    # com.facebook.katana.R.attr.fastScrollPreviewBackgroundLeft
        0x7f01014e    # com.facebook.katana.R.attr.fastScrollPreviewBackgroundRight
        0x7f01014f    # com.facebook.katana.R.attr.fastScrollOverlayPosition
    .end array-data

    :array_15
    .array-data 4
        0x7f010062    # com.facebook.katana.R.attr.message
        0x7f010063    # com.facebook.katana.R.attr.positiveButtonTitle
        0x7f010064    # com.facebook.katana.R.attr.negativeButtonTitle
    .end array-data

    :array_16
    .array-data 4
        0x7f010091    # com.facebook.katana.R.attr.dispatchAndroidTouchEvents
        0x7f010092    # com.facebook.katana.R.attr.reflexAndroidTouchMode
    .end array-data

    :array_17
    .array-data 4
        0x7f01010b    # com.facebook.katana.R.attr.titleText
        0x7f01010c    # com.facebook.katana.R.attr.subtitleText
        0x7f01010d    # com.facebook.katana.R.attr.metaText
        0x7f010124    # com.facebook.katana.R.attr.titleTextAppearance
        0x7f010125    # com.facebook.katana.R.attr.subtitleTextAppearance
        0x7f010126    # com.facebook.katana.R.attr.metaTextAppearance
        0x7f010127    # com.facebook.katana.R.attr.thumbnailSize
    .end array-data

    :array_18
    .array-data 4
        0x7f01010a    # com.facebook.katana.R.attr.imageSrc
        0x7f01010b    # com.facebook.katana.R.attr.titleText
        0x7f0102a7    # com.facebook.katana.R.attr.simpleImageView
    .end array-data

    :array_19
    .array-data 4
        0x7f010060    # com.facebook.katana.R.attr.divider
        0x7f0100f0    # com.facebook.katana.R.attr.dividerPadding
        0x7f010110    # com.facebook.katana.R.attr.dividerThickness
        0x7f01012a    # com.facebook.katana.R.attr.actionButtonDrawable
        0x7f01012b    # com.facebook.katana.R.attr.actionButtonText
        0x7f01012c    # com.facebook.katana.R.attr.actionButtonBackground
        0x7f01012d    # com.facebook.katana.R.attr.actionButtonPadding
        0x7f01012e    # com.facebook.katana.R.attr.actionButtonTextAppearance
        0x7f01012f    # com.facebook.katana.R.attr.actionButtonTheme
    .end array-data

    :array_1a
    .array-data 4
        0x7f010030    # com.facebook.katana.R.attr.uncheckedCountBadge
        0x7f010031    # com.facebook.katana.R.attr.checkedCountBadge
        0x7f010032    # com.facebook.katana.R.attr.badgeTextColor
    .end array-data

    :array_1b
    .array-data 4
        0x7f01010a    # com.facebook.katana.R.attr.imageSrc
        0x7f010304    # com.facebook.katana.R.attr.bottomText
        0x7f010305    # com.facebook.katana.R.attr.fontSize
        0x7f010306    # com.facebook.katana.R.attr.fontStyle
        0x7f010307    # com.facebook.katana.R.attr.fontColor
        0x7f010308    # com.facebook.katana.R.attr.textTopPadding
        0x7f010309    # com.facebook.katana.R.attr.textMaxLines
        0x7f01030a    # com.facebook.katana.R.attr.textLabelGravity
    .end array-data

    :array_1c
    .array-data 4
        0x7f010152    # com.facebook.katana.R.attr.isSwipingEnabled
        0x7f010153    # com.facebook.katana.R.attr.initializeHeightToFirstItem
        0x7f010154    # com.facebook.katana.R.attr.allowDpadPaging
    .end array-data

    :array_1d
    .array-data 4
        0x7f0102cc    # com.facebook.katana.R.attr.dialog_background_alpha
        0x7f0102cd    # com.facebook.katana.R.attr.dialog_padding
    .end array-data

    :array_1e
    .array-data 4
        0x7f01010e    # com.facebook.katana.R.attr.dragDirections
        0x7f01010f    # com.facebook.katana.R.attr.swipeAxis
    .end array-data

    :array_1f
    .array-data 4
        0x7f010158    # com.facebook.katana.R.attr.minNumColumns
        0x7f010159    # com.facebook.katana.R.attr.columnWidthCompat
    .end array-data

    :array_20
    .array-data 4
        0x7f010043    # com.facebook.katana.R.attr.normalHeight
        0x7f010044    # com.facebook.katana.R.attr.grabberId
        0x7f010045    # com.facebook.katana.R.attr.viewToHideWhileDragging
        0x7f010046    # com.facebook.katana.R.attr.dragndropBackground
        0x7f010047    # com.facebook.katana.R.attr.dragndropImageBackground
    .end array-data

    :array_21
    .array-data 4
        0x7f010296    # com.facebook.katana.R.attr.stroke_width
        0x7f010297    # com.facebook.katana.R.attr.stroke_colour
    .end array-data

    :array_22
    .array-data 4
        0x7f01019f    # com.facebook.katana.R.attr.track_left_padding
        0x7f0101a0    # com.facebook.katana.R.attr.track_right_padding
        0x7f0101a1    # com.facebook.katana.R.attr.track_indicator_width
        0x7f0101a2    # com.facebook.katana.R.attr.track_color
        0x7f0101a3    # com.facebook.katana.R.attr.track_indicator_color
    .end array-data

    :array_23
    .array-data 4
        0x7f01031a    # com.facebook.katana.R.attr.guestTileSize
        0x7f01031b    # com.facebook.katana.R.attr.guestTitleSpacing
    .end array-data

    :array_24
    .array-data 4
        0x7f0102fd    # com.facebook.katana.R.attr.fadingGradientLength
        0x7f0102fe    # com.facebook.katana.R.attr.fadingGradientEndColor
        0x7f0102ff    # com.facebook.katana.R.attr.animateHeightChanges
        0x7f010300    # com.facebook.katana.R.attr.canCollapse
        0x7f010301    # com.facebook.katana.R.attr.collapsedStateMaxLines
    .end array-data

    :array_25
    .array-data 4
        0x7f010072    # com.facebook.katana.R.attr.progressCircleBaseAlpha
        0x7f010073    # com.facebook.katana.R.attr.progressCircleBaseColor
        0x7f010074    # com.facebook.katana.R.attr.progressCircleDrawnAlpha
        0x7f010075    # com.facebook.katana.R.attr.progressCircleDrawnColor
        0x7f010076    # com.facebook.katana.R.attr.progressCircleEnableFadeIn
        0x7f010077    # com.facebook.katana.R.attr.progressCircleStrokeWidth
        0x7f010078    # com.facebook.katana.R.attr.progressCircleWidth
    .end array-data

    :array_26
    .array-data 4
        0x7f010184    # com.facebook.katana.R.attr.faceSize
        0x7f010185    # com.facebook.katana.R.attr.paddingBetweenFaces
        0x7f010186    # com.facebook.katana.R.attr.circleFaces
    .end array-data

    :array_27
    .array-data 4
        0x7f01019c    # com.facebook.katana.R.attr.top_divider
        0x7f01019d    # com.facebook.katana.R.attr.bottom_divider
    .end array-data

    :array_28
    .array-data 4
        0x7f010059    # com.facebook.katana.R.attr.title
        0x7f010318    # com.facebook.katana.R.attr.hasJewel
        0x7f010319    # com.facebook.katana.R.attr.hasLauncherButton
    .end array-data

    :array_29
    .array-data 4
        0x7f0102eb    # com.facebook.katana.R.attr.frontView
        0x7f0102ec    # com.facebook.katana.R.attr.backView
        0x7f0102ed    # com.facebook.katana.R.attr.pressedView
    .end array-data

    :array_2a
    .array-data 4
        0x7f010066    # com.facebook.katana.R.attr.horizontalSpacing
        0x7f010067    # com.facebook.katana.R.attr.verticalSpacing
    .end array-data

    :array_2b
    .array-data 4
        0x7f0102d2    # com.facebook.katana.R.attr.active_src
        0x7f0102d3    # com.facebook.katana.R.attr.inactive_src
    .end array-data

    :array_2c
    .array-data 4
        0x7f01029d    # com.facebook.katana.R.attr.fullStarDrawable
        0x7f01029e    # com.facebook.katana.R.attr.halfStarDrawable
        0x7f01029f    # com.facebook.katana.R.attr.emptyStarDrawable
    .end array-data

    :array_2d
    .array-data 4
        0x7f01015a    # com.facebook.katana.R.attr.activeSrc
        0x7f01015b    # com.facebook.katana.R.attr.inactiveSrc
    .end array-data

    :array_2e
    .array-data 4
        0x10100af    # android.R.attr.gravity
        0x7f01030f    # com.facebook.katana.R.attr.animationDurationKS
        0x7f010310    # com.facebook.katana.R.attr.spacingKS
        0x7f010311    # com.facebook.katana.R.attr.unselectedAlphaKS
    .end array-data

    :array_2f
    .array-data 4
        0x7f010331    # com.facebook.katana.R.attr.orientation
        0x7f010332    # com.facebook.katana.R.attr.rowCount
        0x7f010333    # com.facebook.katana.R.attr.columnCount
        0x7f010334    # com.facebook.katana.R.attr.useDefaultMargins
        0x7f010335    # com.facebook.katana.R.attr.alignmentMode
        0x7f010336    # com.facebook.katana.R.attr.rowOrderPreserved
        0x7f010337    # com.facebook.katana.R.attr.columnOrderPreserved
    .end array-data

    :array_30
    .array-data 4
        0x10100f4    # android.R.attr.layout_width
        0x10100f5    # android.R.attr.layout_height
        0x10100f6    # android.R.attr.layout_margin
        0x10100f7    # android.R.attr.layout_marginLeft
        0x10100f8    # android.R.attr.layout_marginTop
        0x10100f9    # android.R.attr.layout_marginRight
        0x10100fa    # android.R.attr.layout_marginBottom
        0x7f010338    # com.facebook.katana.R.attr.layout_row
        0x7f010339    # com.facebook.katana.R.attr.layout_rowSpan
        0x7f01033a    # com.facebook.katana.R.attr.layout_column
        0x7f01033b    # com.facebook.katana.R.attr.layout_columnSpan
        0x7f01033c    # com.facebook.katana.R.attr.layout_gravity
    .end array-data

    :array_31
    .array-data 4
        0x7f010096    # com.facebook.katana.R.attr.spacingHorizontal
        0x7f010097    # com.facebook.katana.R.attr.spacingVertical
        0x7f010098    # com.facebook.katana.R.attr.columnWidth
        0x7f010099    # com.facebook.katana.R.attr.numColumns
        0x7f01009a    # com.facebook.katana.R.attr.stretchMode
    .end array-data

    :array_32
    .array-data 4
        0x7f010148    # com.facebook.katana.R.attr.left_item_width_percentage
        0x7f010149    # com.facebook.katana.R.attr.support_vertical_scrolling
    .end array-data

    :array_33
    .array-data 4
        0x7f010150    # com.facebook.katana.R.attr.indicator_active_color
        0x7f010151    # com.facebook.katana.R.attr.indicator_inactive_color
    .end array-data

    :array_34
    .array-data 4
        0x10100af    # android.R.attr.gravity
        0x10100d5    # android.R.attr.padding
        0x10100d6    # android.R.attr.paddingLeft
        0x10100d7    # android.R.attr.paddingTop
        0x10100d8    # android.R.attr.paddingRight
        0x10100d9    # android.R.attr.paddingBottom
        0x10100f2    # android.R.attr.layout
        0x7f010114    # com.facebook.katana.R.attr.thumbnailDrawable
        0x7f010115    # com.facebook.katana.R.attr.overlayDrawable
        0x7f010116    # com.facebook.katana.R.attr.thumbnailWidth
        0x7f010117    # com.facebook.katana.R.attr.thumbnailHeight
        0x7f010118    # com.facebook.katana.R.attr.thumbnailPadding
        0x7f010119    # com.facebook.katana.R.attr.auxViewPadding
        0x7f01011a    # com.facebook.katana.R.attr.borderColor
        0x7f01011b    # com.facebook.katana.R.attr.border
        0x7f01011c    # com.facebook.katana.R.attr.borderTop
        0x7f01011d    # com.facebook.katana.R.attr.borderBottom
        0x7f01011e    # com.facebook.katana.R.attr.borderLeft
        0x7f01011f    # com.facebook.katana.R.attr.borderRight
        0x7f010120    # com.facebook.katana.R.attr.clipBorderToPadding
    .end array-data

    :array_35
    .array-data 4
        0x10100b3    # android.R.attr.layout_gravity
        0x7f010121    # com.facebook.katana.R.attr.layout_useAsThumbnail
        0x7f010122    # com.facebook.katana.R.attr.layout_useAsAuxView
    .end array-data

    :array_36
    .array-data 4
        0x7f010056    # com.facebook.katana.R.attr.drawable
        0x7f010057    # com.facebook.katana.R.attr.drawableOrientation
    .end array-data

    :array_37
    .array-data 4
        0x7f0102f0    # com.facebook.katana.R.attr.menu
        0x7f0102f1    # com.facebook.katana.R.attr.buttonOrientation
        0x7f0102f2    # com.facebook.katana.R.attr.overflowStyle
        0x7f0102f3    # com.facebook.katana.R.attr.maxNumOfVisibleButtons
        0x7f0102f4    # com.facebook.katana.R.attr.buttonBackgroundStyle
        0x7f0102f5    # com.facebook.katana.R.attr.textAppearance
        0x7f0102f6    # com.facebook.katana.R.attr.inlineActionBarOverflowButtonStyle
    .end array-data

    :array_38
    .array-data 4
        0x7f0102f7    # com.facebook.katana.R.attr.buttonLeftBackground
        0x7f0102f8    # com.facebook.katana.R.attr.buttonCenterBackground
        0x7f0102f9    # com.facebook.katana.R.attr.buttonRightBackground
        0x7f0102fa    # com.facebook.katana.R.attr.buttonWholeBackground
    .end array-data

    :array_39
    .array-data 4
        0x7f010060    # com.facebook.katana.R.attr.divider
        0x7f0100ef    # com.facebook.katana.R.attr.showDividers
        0x7f0100f0    # com.facebook.katana.R.attr.dividerPadding
    .end array-data

    :array_3a
    .array-data 4
        0x7f010315    # com.facebook.katana.R.attr.outerDividerDrawableTop
        0x7f010316    # com.facebook.katana.R.attr.outerDividerDrawableBottom
        0x7f010317    # com.facebook.katana.R.attr.innerDividerDrawable
    .end array-data

    :array_3b
    .array-data 4
        0x7f010093    # com.facebook.katana.R.attr.selectorColor
        0x7f010094    # com.facebook.katana.R.attr.longpressSelectorColor
        0x7f010095    # com.facebook.katana.R.attr.reflexListviewOverscrollEnabled
    .end array-data

    :array_3c
    .array-data 4
        0x7f01017b    # com.facebook.katana.R.attr.imageSize
        0x7f01017c    # com.facebook.katana.R.attr.errorOrientation
        0x7f01017d    # com.facebook.katana.R.attr.imageWidth
        0x7f01017e    # com.facebook.katana.R.attr.imageHeight
        0x7f01017f    # com.facebook.katana.R.attr.errorPaddingTop
        0x7f010180    # com.facebook.katana.R.attr.errorPaddingBottom
        0x7f010181    # com.facebook.katana.R.attr.contentLayout
    .end array-data

    :array_3d
    .array-data 4
        0x7f0102af    # com.facebook.katana.R.attr.mapType
        0x7f0102b0    # com.facebook.katana.R.attr.cameraBearing
        0x7f0102b1    # com.facebook.katana.R.attr.cameraTargetLat
        0x7f0102b2    # com.facebook.katana.R.attr.cameraTargetLng
        0x7f0102b3    # com.facebook.katana.R.attr.cameraTilt
        0x7f0102b4    # com.facebook.katana.R.attr.cameraZoom
        0x7f0102b5    # com.facebook.katana.R.attr.uiCompass
        0x7f0102b6    # com.facebook.katana.R.attr.uiRotateGestures
        0x7f0102b7    # com.facebook.katana.R.attr.uiScrollGestures
        0x7f0102b8    # com.facebook.katana.R.attr.uiTiltGestures
        0x7f0102b9    # com.facebook.katana.R.attr.uiZoomControls
        0x7f0102ba    # com.facebook.katana.R.attr.uiZoomGestures
        0x7f0102bb    # com.facebook.katana.R.attr.useViewLifecycle
        0x7f0102bc    # com.facebook.katana.R.attr.zOrderOnTop
    .end array-data

    :array_3e
    .array-data 4
        0x7f010162    # com.facebook.katana.R.attr.zoom
        0x7f010163    # com.facebook.katana.R.attr.markerColor
        0x7f010164    # com.facebook.katana.R.attr.keepMarkerAtCenter
        0x7f010165    # com.facebook.katana.R.attr.retainMapDuringUpdate
        0x7f010166    # com.facebook.katana.R.attr.overrideZeroRating
    .end array-data

    :array_3f
    .array-data 4
        0x7f010028    # com.facebook.katana.R.attr.mask
        0x7f010029    # com.facebook.katana.R.attr.foreground
        0x7f01002a    # com.facebook.katana.R.attr.usesFboToMask
    .end array-data

    :array_40
    .array-data 4
        0x7f01010b    # com.facebook.katana.R.attr.titleText
        0x7f01010c    # com.facebook.katana.R.attr.subtitleText
        0x7f0102cf    # com.facebook.katana.R.attr.primaryButtonText
        0x7f0102d0    # com.facebook.katana.R.attr.secondaryButtonText
        0x7f0102d1    # com.facebook.katana.R.attr.megaphoneImage
    .end array-data

    :array_41
    .array-data 4
        0x7f01033d    # com.facebook.katana.R.attr.showAddMemberPog
        0x7f01033e    # com.facebook.katana.R.attr.showMemberCountPog
        0x7f01033f    # com.facebook.katana.R.attr.maxMembersToDisplay
        0x7f010340    # com.facebook.katana.R.attr.paddingBetweenPogs
    .end array-data

    :array_42
    .array-data 4
        0x101000e    # android.R.attr.enabled
        0x10100d0    # android.R.attr.id
        0x1010194    # android.R.attr.visible
        0x10101de    # android.R.attr.menuCategory
        0x10101df    # android.R.attr.orderInCategory
        0x10101e0    # android.R.attr.checkableBehavior
    .end array-data

    :array_43
    .array-data 4
        0x1010002    # android.R.attr.icon
        0x101000e    # android.R.attr.enabled
        0x10100d0    # android.R.attr.id
        0x1010106    # android.R.attr.checked
        0x1010194    # android.R.attr.visible
        0x10101de    # android.R.attr.menuCategory
        0x10101df    # android.R.attr.orderInCategory
        0x10101e1    # android.R.attr.title
        0x10101e2    # android.R.attr.titleCondensed
        0x10101e3    # android.R.attr.alphabeticShortcut
        0x10101e4    # android.R.attr.numericShortcut
        0x10101e5    # android.R.attr.checkable
        0x101026f    # android.R.attr.onClick
        0x7f0100e7    # com.facebook.katana.R.attr.showAsAction
        0x7f0100e8    # com.facebook.katana.R.attr.actionLayout
        0x7f0100e9    # com.facebook.katana.R.attr.actionViewClass
        0x7f0100ea    # com.facebook.katana.R.attr.actionProviderClass
    .end array-data

    :array_44
    .array-data 4
        0x10100ae    # android.R.attr.windowAnimationStyle
        0x101012c    # android.R.attr.itemTextAppearance
        0x101012d    # android.R.attr.horizontalDivider
        0x101012e    # android.R.attr.verticalDivider
        0x101012f    # android.R.attr.headerBackground
        0x1010130    # android.R.attr.itemBackground
        0x1010131    # android.R.attr.itemIconDisabledAlpha
        0x1010435    # android.R.attr.snapMargin
    .end array-data

    :array_45
    .array-data 4
        0x7f0102ee    # com.facebook.katana.R.attr.dividerSize
        0x7f0102ef    # com.facebook.katana.R.attr.dividerColor
    .end array-data

    :array_46
    .array-data 4
        0x1010095    # android.R.attr.textSize
        0x1010098    # android.R.attr.textColor
        0x10100b0    # android.R.attr.autoLink
        0x1010153    # android.R.attr.maxLines
    .end array-data

    :array_47
    .array-data 4
        0x7f010001    # com.facebook.katana.R.attr.textColor
        0x7f010008    # com.facebook.katana.R.attr.maxLines
        0x7f010009    # com.facebook.katana.R.attr.minLines
        0x7f01000a    # com.facebook.katana.R.attr.typeface
        0x7f01000b    # com.facebook.katana.R.attr.textStyle
        0x7f010011    # com.facebook.katana.R.attr.textSize
        0x7f01018f    # com.facebook.katana.R.attr.maxWidth
        0x7f010190    # com.facebook.katana.R.attr.minHeight
        0x7f010191    # com.facebook.katana.R.attr.includeFontPadding
        0x7f010192    # com.facebook.katana.R.attr.shadowDx
        0x7f010193    # com.facebook.katana.R.attr.shadowDy
        0x7f010194    # com.facebook.katana.R.attr.shadowRadius
        0x7f010195    # com.facebook.katana.R.attr.shadowColor
    .end array-data

    :array_48
    .array-data 4
        0x7f010088    # com.facebook.katana.R.attr.secondary_button_text
        0x7f010089    # com.facebook.katana.R.attr.primary_button_text
        0x7f01008a    # com.facebook.katana.R.attr.navigation_button_view_theme
    .end array-data

    :array_49
    .array-data 4
        0x7f010001    # com.facebook.katana.R.attr.textColor
        0x7f01000b    # com.facebook.katana.R.attr.textStyle
        0x7f010011    # com.facebook.katana.R.attr.textSize
    .end array-data

    :array_4a
    .array-data 4
        0x7f010167    # com.facebook.katana.R.attr.bodyBackground
        0x7f010168    # com.facebook.katana.R.attr.bodyText
        0x7f010169    # com.facebook.katana.R.attr.nubPosition
        0x7f01016a    # com.facebook.katana.R.attr.nubAlign
        0x7f01016b    # com.facebook.katana.R.attr.nubMargin
    .end array-data

    :array_4b
    .array-data 4
        0x7f010060    # com.facebook.katana.R.attr.divider
        0x7f010196    # com.facebook.katana.R.attr.tabStripEnabled
        0x7f010197    # com.facebook.katana.R.attr.tabStripLeft
        0x7f010198    # com.facebook.katana.R.attr.tabStripRight
    .end array-data

    :array_4c
    .array-data 4
        0x7f010143    # com.facebook.katana.R.attr.layout_anchorPosition
        0x7f010144    # com.facebook.katana.R.attr.layout_isOverlay
        0x7f010145    # com.facebook.katana.R.attr.layout_anchoredTo
        0x7f010146    # com.facebook.katana.R.attr.layout_xOffset
        0x7f010147    # com.facebook.katana.R.attr.layout_yOffset
    .end array-data

    :array_4d
    .array-data 4
        0x7f01030b    # com.facebook.katana.R.attr.transferPadding
        0x7f01030c    # com.facebook.katana.R.attr.transferBackground
    .end array-data

    :array_4e
    .array-data 4
        0x7f01002b    # com.facebook.katana.R.attr.uncheckedImage
        0x7f01002c    # com.facebook.katana.R.attr.checkedImage
        0x7f01002d    # com.facebook.katana.R.attr.uncheckedContentDescription
        0x7f01002e    # com.facebook.katana.R.attr.checkedContentDescription
        0x7f01002f    # com.facebook.katana.R.attr.shouldBounce
    .end array-data

    :array_4f
    .array-data 4
        0x1010002    # android.R.attr.icon
        0x101000e    # android.R.attr.enabled
        0x1010020    # android.R.attr.description
        0x10100d0    # android.R.attr.id
        0x1010106    # android.R.attr.checked
        0x1010194    # android.R.attr.visible
        0x10101df    # android.R.attr.orderInCategory
        0x10101e1    # android.R.attr.title
        0x10101e5    # android.R.attr.checkable
        0x7f0102d8    # com.facebook.katana.R.attr.badgeText
    .end array-data

    :array_50
    .array-data 4
        0x7f010001    # com.facebook.katana.R.attr.textColor
        0x7f01000f    # com.facebook.katana.R.attr.alignment
        0x7f0101da    # com.facebook.katana.R.attr.presenceIndicatorIconStyle
    .end array-data

    :array_51
    .array-data 4
        0x101014f    # android.R.attr.text
        0x7f01005a    # com.facebook.katana.R.attr.subtitle
    .end array-data

    :array_52
    .array-data 4
        0x7f010141    # com.facebook.katana.R.attr.foregroundDrawable
        0x7f010142    # com.facebook.katana.R.attr.scaling
    .end array-data

    :array_53
    .array-data 4
        0x7f010001    # com.facebook.katana.R.attr.textColor
        0x7f01000a    # com.facebook.katana.R.attr.typeface
        0x7f01000b    # com.facebook.katana.R.attr.textStyle
        0x7f010010    # com.facebook.katana.R.attr.text
        0x7f010011    # com.facebook.katana.R.attr.textSize
        0x7f010013    # com.facebook.katana.R.attr.textGravity
        0x7f010137    # com.facebook.katana.R.attr.showChevronOnRight
    .end array-data

    :array_54
    .array-data 4
        0x7f010138    # com.facebook.katana.R.attr.asCircle
        0x7f010139    # com.facebook.katana.R.attr.isTopLeftRounded
        0x7f01013a    # com.facebook.katana.R.attr.isTopRightRounded
        0x7f01013b    # com.facebook.katana.R.attr.isBottomRightRounded
        0x7f01013c    # com.facebook.katana.R.attr.isBottomLeftRounded
        0x7f01013d    # com.facebook.katana.R.attr.cornerRadius
        0x7f01013e    # com.facebook.katana.R.attr.roundByOverlayingColor
        0x7f01013f    # com.facebook.katana.R.attr.roundBorderWidth
        0x7f010140    # com.facebook.katana.R.attr.roundBorderColor
    .end array-data

    :array_55
    .array-data 4
        0x7f01008d    # com.facebook.katana.R.attr.primary_text
        0x7f01008e    # com.facebook.katana.R.attr.detail_text
    .end array-data

    :array_56
    .array-data 4
        0x101011f    # android.R.attr.maxWidth
        0x1010220    # android.R.attr.inputType
        0x1010264    # android.R.attr.imeOptions
        0x7f0100f4    # com.facebook.katana.R.attr.iconifiedByDefault
        0x7f0100f5    # com.facebook.katana.R.attr.queryHint
        0x7f0100f6    # com.facebook.katana.R.attr.searchViewHintMinFontSize
    .end array-data

    :array_57
    .array-data 4
        0x7f010060    # com.facebook.katana.R.attr.divider
        0x7f0100ef    # com.facebook.katana.R.attr.showDividers
        0x7f0100f0    # com.facebook.katana.R.attr.dividerPadding
        0x7f010110    # com.facebook.katana.R.attr.dividerThickness
        0x7f010111    # com.facebook.katana.R.attr.dividerPaddingStart
        0x7f010112    # com.facebook.katana.R.attr.dividerPaddingEnd
    .end array-data

    :array_58
    .array-data 4
        0x7f010079    # com.facebook.katana.R.attr.auto_start
        0x7f01007a    # com.facebook.katana.R.attr.base_alpha
        0x7f01007b    # com.facebook.katana.R.attr.duration
        0x7f01007c    # com.facebook.katana.R.attr.repeat_count
        0x7f01007d    # com.facebook.katana.R.attr.repeat_delay
        0x7f01007e    # com.facebook.katana.R.attr.repeat_mode
        0x7f01007f    # com.facebook.katana.R.attr.angle
        0x7f010080    # com.facebook.katana.R.attr.dropoff
        0x7f010081    # com.facebook.katana.R.attr.fixed_width
        0x7f010082    # com.facebook.katana.R.attr.fixed_height
        0x7f010083    # com.facebook.katana.R.attr.intensity
        0x7f010084    # com.facebook.katana.R.attr.relative_width
        0x7f010085    # com.facebook.katana.R.attr.relative_height
        0x7f010086    # com.facebook.katana.R.attr.shape
        0x7f010087    # com.facebook.katana.R.attr.tilt
    .end array-data

    :array_59
    .array-data 4
        0x7f010187    # com.facebook.katana.R.attr.dividerPosition
        0x7f010199    # com.facebook.katana.R.attr.animateOutDirection
        0x7f01019a    # com.facebook.katana.R.attr.suggestionText
        0x7f01019b    # com.facebook.katana.R.attr.buttonText
    .end array-data

    :array_5a
    .array-data 4
        0x7f010173    # com.facebook.katana.R.attr.distanceBetweenBars
        0x7f010174    # com.facebook.katana.R.attr.barColor
        0x7f010175    # com.facebook.katana.R.attr.numberOfBars
    .end array-data

    :array_5b
    .array-data 4
        0x10100af    # android.R.attr.gravity
        0x1010175    # android.R.attr.dropDownSelector
        0x1010176    # android.R.attr.popupBackground
        0x1010262    # android.R.attr.dropDownWidth
        0x10102ac    # android.R.attr.dropDownHorizontalOffset
        0x10102ad    # android.R.attr.dropDownVerticalOffset
        0x7f0100eb    # com.facebook.katana.R.attr.prompt
        0x7f0100ec    # com.facebook.katana.R.attr.spinnerMode
        0x7f0100ed    # com.facebook.katana.R.attr.popupPromptView
        0x7f0100ee    # com.facebook.katana.R.attr.disableChildrenWhenDisabled
    .end array-data

    :array_5c
    .array-data 4
        0x7f01015c    # com.facebook.katana.R.attr.listStartHeight
        0x7f01015d    # com.facebook.katana.R.attr.listHideThreshold
        0x7f01015e    # com.facebook.katana.R.attr.headerStartHeight
        0x7f01015f    # com.facebook.katana.R.attr.headerHideThreshold
        0x7f010160    # com.facebook.katana.R.attr.disableScrollHideList
    .end array-data

    :array_5d
    .array-data 4
        0x7f010292    # com.facebook.katana.R.attr.stickerStoreItemTopDiv
        0x7f010293    # com.facebook.katana.R.attr.stickerStoreItemBottomDiv
        0x7f010294    # com.facebook.katana.R.attr.stickerStoreItemTopSlot
        0x7f010295    # com.facebook.katana.R.attr.stickerStoreItemBottomSlot
    .end array-data

    :array_5e
    .array-data 4
        0x1010124    # android.R.attr.textOn
        0x1010125    # android.R.attr.textOff
        0x1010142    # android.R.attr.thumb
        0x101036e    # android.R.attr.switchTextAppearance
        0x101036f    # android.R.attr.track
        0x1010370    # android.R.attr.switchMinWidth
        0x1010371    # android.R.attr.switchPadding
        0x1010372    # android.R.attr.thumbTextPadding
    .end array-data

    :array_5f
    .array-data 4
        0x7f010060    # com.facebook.katana.R.attr.divider
        0x7f0100f0    # com.facebook.katana.R.attr.dividerPadding
        0x7f010110    # com.facebook.katana.R.attr.dividerThickness
        0x7f010133    # com.facebook.katana.R.attr.underlineColor
        0x7f010134    # com.facebook.katana.R.attr.underlineHeight
        0x7f010135    # com.facebook.katana.R.attr.tabLayout
    .end array-data

    :array_60
    .array-data 4
        0x7f010024    # com.facebook.katana.R.attr.switchTextColor
        0x7f010025    # com.facebook.katana.R.attr.switchTextSize
        0x7f010026    # com.facebook.katana.R.attr.switchTextStyle
        0x7f010027    # com.facebook.katana.R.attr.switchTypeface
    .end array-data

    :array_61
    .array-data 4
        0x7f010051    # com.facebook.katana.R.attr.textWrappingDrawablePadding
        0x7f010052    # com.facebook.katana.R.attr.textWrappingDrawableTop
        0x7f010053    # com.facebook.katana.R.attr.textWrappingDrawableRight
        0x7f010054    # com.facebook.katana.R.attr.textWrappingDrawableBottom
        0x7f010055    # com.facebook.katana.R.attr.textWrappingDrawableLeft
    .end array-data

    :array_62
    .array-data 4
        0x7f010302    # com.facebook.katana.R.attr.highlightColor
        0x7f010303    # com.facebook.katana.R.attr.highlightStyle
    .end array-data

    :array_63
    .array-data 4
        0x7f0100e1    # com.facebook.katana.R.attr.actionDropDownStyle
        0x7f0100e2    # com.facebook.katana.R.attr.dropdownListPreferredItemHeight
        0x7f0100e3    # com.facebook.katana.R.attr.popupMenuStyle
        0x7f0100e4    # com.facebook.katana.R.attr.panelMenuListWidth
        0x7f0100e5    # com.facebook.katana.R.attr.panelMenuListTheme
        0x7f0100e6    # com.facebook.katana.R.attr.listChoiceBackgroundIndicator
    .end array-data

    :array_64
    .array-data 4
        0x7f010001    # com.facebook.katana.R.attr.textColor
        0x7f010008    # com.facebook.katana.R.attr.maxLines
        0x7f01000a    # com.facebook.katana.R.attr.typeface
        0x7f01000b    # com.facebook.katana.R.attr.textStyle
        0x7f01000d    # com.facebook.katana.R.attr.minScaledTextSize
        0x7f01000e    # com.facebook.katana.R.attr.maxScaledTextSize
        0x7f01000f    # com.facebook.katana.R.attr.alignment
        0x7f010171    # com.facebook.katana.R.attr.nameOption
    .end array-data

    :array_65
    .array-data 4
        0x7f010069    # com.facebook.katana.R.attr.threadTileSize
        0x7f01006a    # com.facebook.katana.R.attr.bigImageWidthPercent
        0x7f01006b    # com.facebook.katana.R.attr.bigImageLocation
        0x7f01006c    # com.facebook.katana.R.attr.overlayDivider
        0x7f01006d    # com.facebook.katana.R.attr.facebookBadge
        0x7f01006e    # com.facebook.katana.R.attr.messengerBadge
        0x7f01006f    # com.facebook.katana.R.attr.phoneBadge
        0x7f010070    # com.facebook.katana.R.attr.friendPhoneBadge
    .end array-data

    :array_66
    .array-data 4
        0x7f01018c    # com.facebook.katana.R.attr.iconSrc
        0x7f01018d    # com.facebook.katana.R.attr.itemTitle
        0x7f01018e    # com.facebook.katana.R.attr.hintText
    .end array-data

    :array_67
    .array-data 4
        0x7f010312    # com.facebook.katana.R.attr.bitmapSrc
        0x7f010313    # com.facebook.katana.R.attr.fromRight
        0x7f010314    # com.facebook.katana.R.attr.mode_vertical
    .end array-data

    :array_68
    .array-data 4
        0x7f010010    # com.facebook.katana.R.attr.text
        0x7f0102fb    # com.facebook.katana.R.attr.hasCheckBox
        0x7f0102fc    # com.facebook.katana.R.attr.checked
    .end array-data

    :array_69
    .array-data 4
        0x7f010059    # com.facebook.katana.R.attr.title
        0x7f01005c    # com.facebook.katana.R.attr.hasBackButton
        0x7f01005e    # com.facebook.katana.R.attr.centerTitle
    .end array-data

    :array_6a
    .array-data 4
        0x7f010187    # com.facebook.katana.R.attr.dividerPosition
        0x7f01018b    # com.facebook.katana.R.attr.src
    .end array-data

    :array_6b
    .array-data 4
        0x7f01008b    # com.facebook.katana.R.attr.title_text
        0x7f01008c    # com.facebook.katana.R.attr.accessory_text
        0x7f01008f    # com.facebook.katana.R.attr.show_back_chevron
    .end array-data

    :array_6c
    .array-data 4
        0x7f010059    # com.facebook.katana.R.attr.title
        0x7f01005c    # com.facebook.katana.R.attr.hasBackButton
        0x7f01005d    # com.facebook.katana.R.attr.useActionBar
        0x7f01005e    # com.facebook.katana.R.attr.centerTitle
        0x7f01005f    # com.facebook.katana.R.attr.navless
    .end array-data

    :array_6d
    .array-data 4
        0x7f01016d    # com.facebook.katana.R.attr.tokenTextColor
        0x7f01016e    # com.facebook.katana.R.attr.tokenTextSize
        0x7f01016f    # com.facebook.katana.R.attr.tokenBackgroundDrawable
    .end array-data

    :array_6e
    .array-data 4
        0x7f010034    # com.facebook.katana.R.attr.url
        0x7f010035    # com.facebook.katana.R.attr.placeholderSrc
        0x7f010036    # com.facebook.katana.R.attr.showProgressBar
        0x7f010037    # com.facebook.katana.R.attr.isShownInGallery
        0x7f010038    # com.facebook.katana.R.attr.useZoomableImageView
        0x7f010039    # com.facebook.katana.R.attr.useQuickContactBadge
        0x7f01003a    # com.facebook.katana.R.attr.isUsedWithUploadProgress
        0x7f01003b    # com.facebook.katana.R.attr.useCoverView
        0x7f01003c    # com.facebook.katana.R.attr.retainImageDuringUpdate
        0x7f01003d    # com.facebook.katana.R.attr.shouldShowLoadingAnimation
        0x7f01003e    # com.facebook.katana.R.attr.adjustViewBounds
        0x7f01003f    # com.facebook.katana.R.attr.pressedOverlayColor
        0x7f010040    # com.facebook.katana.R.attr.placeHolderScaleType
        0x7f010041    # com.facebook.katana.R.attr.scaleType
    .end array-data

    :array_6f
    .array-data 4
        0x7f010014    # com.facebook.katana.R.attr.tileSize
        0x7f010061    # com.facebook.katana.R.attr.childMargin
    .end array-data

    :array_70
    .array-data 4
        0x7f010014    # com.facebook.katana.R.attr.tileSize
        0x7f010015    # com.facebook.katana.R.attr.defaultUserTileSrc
        0x7f010016    # com.facebook.katana.R.attr.defaultUserTileColor
    .end array-data

    :array_71
    .array-data 4
        0x7f010173    # com.facebook.katana.R.attr.distanceBetweenBars
        0x7f010174    # com.facebook.katana.R.attr.barColor
        0x7f010175    # com.facebook.katana.R.attr.numberOfBars
    .end array-data

    :array_72
    .array-data 4
        0x7f010001    # com.facebook.katana.R.attr.textColor
        0x7f010008    # com.facebook.katana.R.attr.maxLines
        0x7f01000a    # com.facebook.katana.R.attr.typeface
        0x7f01000b    # com.facebook.katana.R.attr.textStyle
        0x7f01000d    # com.facebook.katana.R.attr.minScaledTextSize
        0x7f01000e    # com.facebook.katana.R.attr.maxScaledTextSize
        0x7f01000f    # com.facebook.katana.R.attr.alignment
        0x7f010049    # com.facebook.katana.R.attr.fontFamily
    .end array-data

    :array_73
    .array-data 4
        0x7f010178    # com.facebook.katana.R.attr.label
        0x7f010179    # com.facebook.katana.R.attr.labelTextColor
        0x7f01017a    # com.facebook.katana.R.attr.bodyTextColor
    .end array-data

    :array_74
    .array-data 4
        0x10100da    # android.R.attr.focusable
        0x7f0100d2    # com.facebook.katana.R.attr.paddingStart
        0x7f0100d3    # com.facebook.katana.R.attr.paddingEnd
    .end array-data

    :array_75
    .array-data 4
        0x7f0102dd    # com.facebook.katana.R.attr.vpiCirclePageIndicatorStyle
        0x7f0102de    # com.facebook.katana.R.attr.vpiTabPageIndicatorStyle
        0x7f0102df    # com.facebook.katana.R.attr.vpiIconPageIndicatorStyle
    .end array-data

    :array_76
    .array-data 4
        0x7f0102bd    # com.facebook.katana.R.attr.theme
        0x7f0102be    # com.facebook.katana.R.attr.environment
        0x7f0102bf    # com.facebook.katana.R.attr.fragmentStyle
        0x7f0102c0    # com.facebook.katana.R.attr.fragmentMode
    .end array-data

    :array_77
    .array-data 4
        0x7f0102c1    # com.facebook.katana.R.attr.buyButtonHeight
        0x7f0102c2    # com.facebook.katana.R.attr.buyButtonWidth
        0x7f0102c3    # com.facebook.katana.R.attr.buyButtonText
        0x7f0102c4    # com.facebook.katana.R.attr.buyButtonAppearance
        0x7f0102c5    # com.facebook.katana.R.attr.maskedWalletDetailsTextAppearance
        0x7f0102c6    # com.facebook.katana.R.attr.maskedWalletDetailsHeaderTextAppearance
        0x7f0102c7    # com.facebook.katana.R.attr.maskedWalletDetailsBackground
        0x7f0102c8    # com.facebook.katana.R.attr.maskedWalletDetailsButtonTextAppearance
        0x7f0102c9    # com.facebook.katana.R.attr.maskedWalletDetailsButtonBackground
        0x7f0102ca    # com.facebook.katana.R.attr.maskedWalletDetailsLogoTextColor
        0x7f0102cb    # com.facebook.katana.R.attr.maskedWalletDetailsLogoImageType
    .end array-data
.end method

.method public constructor <init>()V
    .locals 0

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method
