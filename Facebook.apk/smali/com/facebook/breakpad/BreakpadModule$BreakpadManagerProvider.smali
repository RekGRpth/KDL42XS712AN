.class public Lcom/facebook/breakpad/BreakpadModule$BreakpadManagerProvider;
.super Lcom/facebook/inject/AbstractProvider;
.source "BreakpadModule.java"


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/facebook/inject/AbstractProvider",
        "<",
        "Lcom/facebook/breakpad/BreakpadManager;",
        ">;"
    }
.end annotation


# static fields
.field private static a:Lcom/facebook/breakpad/BreakpadManager;


# direct methods
.method private constructor <init>()V
    .locals 0

    invoke-direct {p0}, Lcom/facebook/inject/AbstractProvider;-><init>()V

    return-void
.end method

.method private a()Lcom/facebook/breakpad/BreakpadManager;
    .locals 2

    invoke-virtual {p0}, Lcom/facebook/breakpad/BreakpadModule$BreakpadManagerProvider;->getApplicationInjector()Lcom/facebook/inject/FbInjector;

    move-result-object v0

    const-class v1, Landroid/content/Context;

    invoke-virtual {v0, v1}, Lcom/facebook/inject/FbInjector;->getInstance(Ljava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/content/Context;

    invoke-static {v0}, Lcom/facebook/breakpad/BreakpadManager;->a(Landroid/content/Context;)Lcom/facebook/breakpad/BreakpadManager;

    move-result-object v0

    return-object v0
.end method

.method public static a(Lcom/facebook/inject/InjectorLike;)Lcom/facebook/breakpad/BreakpadManager;
    .locals 4

    const-class v2, Lcom/facebook/breakpad/BreakpadModule$BreakpadManagerProvider;

    monitor-enter v2

    :try_start_0
    sget-object v0, Lcom/facebook/breakpad/BreakpadModule$BreakpadManagerProvider;->a:Lcom/facebook/breakpad/BreakpadManager;

    if-nez v0, :cond_0

    invoke-static {}, Lcom/facebook/inject/ScopeStack;->a()Lcom/facebook/inject/ScopeStack;

    move-result-object v3

    const-class v0, Ljavax/inject/Singleton;

    invoke-virtual {v3, v0}, Lcom/facebook/inject/ScopeStack;->a(Ljava/lang/Class;)V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_2

    :try_start_1
    const-class v0, Lcom/facebook/inject/ContextScope;

    invoke-interface {p0, v0}, Lcom/facebook/inject/InjectorLike;->getInstance(Ljava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/inject/ContextScope;

    invoke-virtual {v0}, Lcom/facebook/inject/ContextScope;->a()V
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_1

    :try_start_2
    invoke-interface {p0}, Lcom/facebook/inject/InjectorLike;->getApplicationInjector()Lcom/facebook/inject/FbInjector;

    move-result-object v1

    invoke-static {v1}, Lcom/facebook/breakpad/BreakpadModule$BreakpadManagerProvider;->b(Lcom/facebook/inject/InjectorLike;)Lcom/facebook/breakpad/BreakpadManager;

    move-result-object v1

    sput-object v1, Lcom/facebook/breakpad/BreakpadModule$BreakpadManagerProvider;->a:Lcom/facebook/breakpad/BreakpadManager;
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    :try_start_3
    invoke-virtual {v0}, Lcom/facebook/inject/ContextScope;->b()V
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_1

    :try_start_4
    const-class v0, Ljavax/inject/Singleton;

    invoke-virtual {v3, v0}, Lcom/facebook/inject/ScopeStack;->b(Ljava/lang/Class;)V

    :cond_0
    monitor-exit v2
    :try_end_4
    .catchall {:try_start_4 .. :try_end_4} :catchall_2

    sget-object v0, Lcom/facebook/breakpad/BreakpadModule$BreakpadManagerProvider;->a:Lcom/facebook/breakpad/BreakpadManager;

    return-object v0

    :catchall_0
    move-exception v1

    :try_start_5
    invoke-virtual {v0}, Lcom/facebook/inject/ContextScope;->b()V

    throw v1
    :try_end_5
    .catchall {:try_start_5 .. :try_end_5} :catchall_1

    :catchall_1
    move-exception v0

    :try_start_6
    const-class v1, Ljavax/inject/Singleton;

    invoke-virtual {v3, v1}, Lcom/facebook/inject/ScopeStack;->b(Ljava/lang/Class;)V

    throw v0
    :try_end_6
    .catchall {:try_start_6 .. :try_end_6} :catchall_2

    :catchall_2
    move-exception v0

    monitor-exit v2

    throw v0
.end method

.method private static b(Lcom/facebook/inject/InjectorLike;)Lcom/facebook/breakpad/BreakpadManager;
    .locals 2

    invoke-interface {p0}, Lcom/facebook/inject/InjectorLike;->getApplicationInjector()Lcom/facebook/inject/FbInjector;

    move-result-object v0

    const-class v1, Landroid/content/Context;

    invoke-virtual {v0, v1}, Lcom/facebook/inject/FbInjector;->getInstance(Ljava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/content/Context;

    invoke-static {v0}, Lcom/facebook/breakpad/BreakpadManager;->a(Landroid/content/Context;)Lcom/facebook/breakpad/BreakpadManager;

    move-result-object v0

    return-object v0
.end method


# virtual methods
.method public synthetic get()Ljava/lang/Object;
    .locals 1

    invoke-direct {p0}, Lcom/facebook/breakpad/BreakpadModule$BreakpadManagerProvider;->a()Lcom/facebook/breakpad/BreakpadManager;

    move-result-object v0

    return-object v0
.end method
