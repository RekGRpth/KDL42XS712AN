.class public Lcom/facebook/breakpad/BreakpadManager;
.super Ljava/lang/Object;
.source "BreakpadManager.java"


# static fields
.field private static a:Lcom/facebook/breakpad/BreakpadManager;


# instance fields
.field private b:Ljava/io/File;


# direct methods
.method private constructor <init>(Ljava/io/File;)V
    .locals 0

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    iput-object p1, p0, Lcom/facebook/breakpad/BreakpadManager;->b:Ljava/io/File;

    return-void
.end method

.method public static declared-synchronized a(Landroid/content/Context;)Lcom/facebook/breakpad/BreakpadManager;
    .locals 3

    const-class v1, Lcom/facebook/breakpad/BreakpadManager;

    monitor-enter v1

    :try_start_0
    sget-object v0, Lcom/facebook/breakpad/BreakpadManager;->a:Lcom/facebook/breakpad/BreakpadManager;

    if-nez v0, :cond_0

    const-string v0, "BreakpadManager"

    const-string v2, "Breakpad not initialized properly, should fix if not in unittest"

    invoke-static {v0, v2}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    invoke-static {p0}, Lcom/facebook/breakpad/BreakpadManager;->b(Landroid/content/Context;)V

    :cond_0
    sget-object v0, Lcom/facebook/breakpad/BreakpadManager;->a:Lcom/facebook/breakpad/BreakpadManager;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    monitor-exit v1

    return-object v0

    :catchall_0
    move-exception v0

    monitor-exit v1

    throw v0
.end method

.method private a()Ljava/lang/String;
    .locals 1

    iget-object v0, p0, Lcom/facebook/breakpad/BreakpadManager;->b:Ljava/io/File;

    invoke-virtual {v0}, Ljava/io/File;->getAbsolutePath()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public static declared-synchronized b(Landroid/content/Context;)V
    .locals 4

    const-class v1, Lcom/facebook/breakpad/BreakpadManager;

    monitor-enter v1

    :try_start_0
    sget-object v0, Lcom/facebook/breakpad/BreakpadManager;->a:Lcom/facebook/breakpad/BreakpadManager;

    if-nez v0, :cond_0

    const-string v0, "minidumps"

    const/4 v2, 0x0

    invoke-virtual {p0, v0, v2}, Landroid/content/Context;->getDir(Ljava/lang/String;I)Ljava/io/File;

    move-result-object v0

    new-instance v2, Lcom/facebook/breakpad/BreakpadManager;

    invoke-direct {v2, v0}, Lcom/facebook/breakpad/BreakpadManager;-><init>(Ljava/io/File;)V

    sput-object v2, Lcom/facebook/breakpad/BreakpadManager;->a:Lcom/facebook/breakpad/BreakpadManager;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    :try_start_1
    const-string v0, "fb_stl_shared"

    invoke-static {v0}, Lcom/facebook/soloader/SoLoader;->a(Ljava/lang/String;)V

    const-string v0, "fb_breakpad_client"

    invoke-static {v0}, Lcom/facebook/soloader/SoLoader;->a(Ljava/lang/String;)V

    invoke-static {}, Lcom/facebook/common/build/BuildConstants;->a()Z

    move-result v0

    if-nez v0, :cond_0

    sget-object v0, Lcom/facebook/breakpad/BreakpadManager;->a:Lcom/facebook/breakpad/BreakpadManager;

    invoke-direct {v0}, Lcom/facebook/breakpad/BreakpadManager;->a()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Lcom/facebook/breakpad/BreakpadManager;->install(Ljava/lang/String;)V
    :try_end_1
    .catch Ljava/lang/UnsatisfiedLinkError; {:try_start_1 .. :try_end_1} :catch_0
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    :cond_0
    :goto_0
    monitor-exit v1

    return-void

    :catch_0
    move-exception v0

    :try_start_2
    const-string v2, "BreakpadManager"

    const-string v3, "Breakpad init failed: "

    invoke-static {v2, v3, v0}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    goto :goto_0

    :catchall_0
    move-exception v0

    monitor-exit v1

    throw v0
.end method

.method private static native install(Ljava/lang/String;)V
.end method

.method public static nativeCrashed(Ljava/lang/String;)V
    .locals 3
    .annotation build Lcom/facebook/proguard/annotations/DoNotStrip;
    .end annotation

    const-string v0, "BreakpadManager"

    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "Native crash reported: "

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v1, p0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    return-void
.end method
