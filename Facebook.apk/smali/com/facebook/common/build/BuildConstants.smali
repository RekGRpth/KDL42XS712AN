.class public Lcom/facebook/common/build/BuildConstants;
.super Ljava/lang/Object;
.source "BuildConstants.java"


# static fields
.field private static a:Z

.field private static b:Z


# direct methods
.method static constructor <clinit>()V
    .locals 1

    sget-boolean v0, Lcom/facebook/common/build/config/BuildConfig;->a:Z

    sput-boolean v0, Lcom/facebook/common/build/BuildConstants;->a:Z

    sget-boolean v0, Lcom/facebook/common/build/config/BuildConfig;->b:Z

    sput-boolean v0, Lcom/facebook/common/build/BuildConstants;->b:Z

    return-void
.end method

.method public constructor <init>()V
    .locals 0

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method public static final a()Z
    .locals 1

    sget-boolean v0, Lcom/facebook/common/build/BuildConstants;->a:Z

    return v0
.end method

.method public static final b()Z
    .locals 1

    sget-boolean v0, Lcom/facebook/common/build/BuildConstants;->b:Z

    return v0
.end method

.method public static final c()Ljava/lang/String;
    .locals 1

    invoke-static {}, Lcom/facebook/common/build/BuildConstants;->b()Z

    move-result v0

    if-eqz v0, :cond_0

    const-string v0, "com.facebook.work"

    :goto_0
    return-object v0

    :cond_0
    invoke-static {}, Lcom/facebook/common/build/BuildConstants;->a()Z

    move-result v0

    if-eqz v0, :cond_1

    const-string v0, "com.facebook.wakizashi"

    goto :goto_0

    :cond_1
    const-string v0, "com.facebook.katana"

    goto :goto_0
.end method

.method public static final d()Ljava/lang/String;
    .locals 1

    invoke-static {}, Lcom/facebook/common/build/BuildConstants;->b()Z

    move-result v0

    if-eqz v0, :cond_0

    const-string v0, "work"

    :goto_0
    return-object v0

    :cond_0
    invoke-static {}, Lcom/facebook/common/build/BuildConstants;->a()Z

    move-result v0

    if-eqz v0, :cond_1

    const-string v0, "wakizashi"

    goto :goto_0

    :cond_1
    const-string v0, "katana"

    goto :goto_0
.end method

.method public static final e()Ljava/lang/String;
    .locals 1

    invoke-static {}, Lcom/facebook/common/build/BuildConstants;->a()Z

    move-result v0

    if-eqz v0, :cond_0

    const-string v0, "https://m.facebook.com/mobile_builds"

    :goto_0
    return-object v0

    :cond_0
    const-string v0, "https://market.android.com/details?id=com.facebook.katana"

    goto :goto_0
.end method
