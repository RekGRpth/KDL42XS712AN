.class final Lcom/facebook/common/dextricks/DexLibLoader$ResourceResProvider;
.super Ljava/lang/Object;
.source "DexLibLoader.java"

# interfaces
.implements Lcom/facebook/common/dextricks/DexLibLoader$ResProvider;


# instance fields
.field private final a:Landroid/content/Context;


# direct methods
.method private constructor <init>(Landroid/content/Context;)V
    .locals 0

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    iput-object p1, p0, Lcom/facebook/common/dextricks/DexLibLoader$ResourceResProvider;->a:Landroid/content/Context;

    return-void
.end method

.method synthetic constructor <init>(Landroid/content/Context;B)V
    .locals 0

    invoke-direct {p0, p1}, Lcom/facebook/common/dextricks/DexLibLoader$ResourceResProvider;-><init>(Landroid/content/Context;)V

    return-void
.end method


# virtual methods
.method public final a(Ljava/lang/String;)Ljava/io/InputStream;
    .locals 3

    iget-object v0, p0, Lcom/facebook/common/dextricks/DexLibLoader$ResourceResProvider;->a:Landroid/content/Context;

    invoke-virtual {v0}, Landroid/content/Context;->getAssets()Landroid/content/res/AssetManager;

    move-result-object v0

    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "secondary-program-dex-jars/"

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/content/res/AssetManager;->open(Ljava/lang/String;)Ljava/io/InputStream;

    move-result-object v0

    return-object v0
.end method

.method public final a(Ljava/lang/String;Ljava/io/File;)V
    .locals 5

    const/4 v2, 0x0

    :try_start_0
    invoke-virtual {p0, p1}, Lcom/facebook/common/dextricks/DexLibLoader$ResourceResProvider;->a(Ljava/lang/String;)Ljava/io/InputStream;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    move-result-object v3

    :try_start_1
    new-instance v1, Ljava/io/FileOutputStream;

    invoke-direct {v1, p2}, Ljava/io/FileOutputStream;-><init>(Ljava/io/File;)V
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_2

    const/16 v0, 0x2000

    :try_start_2
    new-array v0, v0, [B

    :goto_0
    const/4 v2, 0x0

    const/16 v4, 0x2000

    invoke-virtual {v3, v0, v2, v4}, Ljava/io/InputStream;->read([BII)I

    move-result v2

    if-lez v2, :cond_0

    const/4 v4, 0x0

    invoke-virtual {v1, v0, v4, v2}, Ljava/io/OutputStream;->write([BII)V
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    goto :goto_0

    :catchall_0
    move-exception v0

    move-object v2, v3

    :goto_1
    invoke-static {v2}, Lcom/facebook/common/dextricks/DexLibLoader;->a(Ljava/io/Closeable;)V

    invoke-static {v1}, Lcom/facebook/common/dextricks/DexLibLoader;->a(Ljava/io/Closeable;)V

    throw v0

    :cond_0
    :try_start_3
    invoke-virtual {v1}, Ljava/io/OutputStream;->flush()V
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_0

    invoke-static {v3}, Lcom/facebook/common/dextricks/DexLibLoader;->a(Ljava/io/Closeable;)V

    invoke-static {v1}, Lcom/facebook/common/dextricks/DexLibLoader;->a(Ljava/io/Closeable;)V

    return-void

    :catchall_1
    move-exception v0

    move-object v1, v2

    goto :goto_1

    :catchall_2
    move-exception v0

    move-object v1, v2

    move-object v2, v3

    goto :goto_1
.end method

.method public final a()Z
    .locals 1

    const/4 v0, 0x1

    return v0
.end method

.method public final b(Ljava/lang/String;Ljava/io/File;)V
    .locals 3

    new-instance v1, Lcom/facebook/xzdecoder/XzDecoder;

    invoke-direct {v1}, Lcom/facebook/xzdecoder/XzDecoder;-><init>()V

    :try_start_0
    new-instance v0, Ljava/lang/StringBuilder;

    const-string v2, "secondary-program-dex-jars/"

    invoke-direct {v0, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v0, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    iget-object v2, p0, Lcom/facebook/common/dextricks/DexLibLoader$ResourceResProvider;->a:Landroid/content/Context;

    invoke-virtual {v1, v2, v0, p2}, Lcom/facebook/xzdecoder/XzDecoder;->a(Landroid/content/Context;Ljava/lang/String;Ljava/io/File;)V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    invoke-virtual {v1}, Lcom/facebook/xzdecoder/XzDecoder;->a()V

    return-void

    :catchall_0
    move-exception v0

    invoke-virtual {v1}, Lcom/facebook/xzdecoder/XzDecoder;->a()V

    throw v0
.end method

.method public final b()Z
    .locals 1

    const/4 v0, 0x1

    return v0
.end method
