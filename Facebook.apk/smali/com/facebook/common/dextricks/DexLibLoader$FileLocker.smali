.class Lcom/facebook/common/dextricks/DexLibLoader$FileLocker;
.super Ljava/lang/Object;
.source "DexLibLoader.java"


# instance fields
.field private final a:Ljava/io/FileOutputStream;

.field private final b:Ljava/nio/channels/FileLock;


# direct methods
.method private constructor <init>(Ljava/io/File;)V
    .locals 6

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    new-instance v0, Ljava/io/FileOutputStream;

    invoke-direct {v0, p1}, Ljava/io/FileOutputStream;-><init>(Ljava/io/File;)V

    iput-object v0, p0, Lcom/facebook/common/dextricks/DexLibLoader$FileLocker;->a:Ljava/io/FileOutputStream;

    const/4 v1, 0x0

    :try_start_0
    invoke-static {}, Landroid/os/SystemClock;->elapsedRealtime()J

    move-result-wide v2

    const-string v0, "DexLibLoader"

    const-string v4, "about to lock odex store"

    invoke-static {v0, v4}, Landroid/util/Log;->v(Ljava/lang/String;Ljava/lang/String;)I

    iget-object v0, p0, Lcom/facebook/common/dextricks/DexLibLoader$FileLocker;->a:Ljava/io/FileOutputStream;

    invoke-virtual {v0}, Ljava/io/FileOutputStream;->getChannel()Ljava/nio/channels/FileChannel;

    move-result-object v0

    invoke-virtual {v0}, Ljava/nio/channels/FileChannel;->lock()Ljava/nio/channels/FileLock;

    move-result-object v1

    invoke-static {}, Landroid/os/SystemClock;->elapsedRealtime()J

    move-result-wide v4

    sub-long v2, v4, v2

    const-string v0, "DexLibLoader"

    new-instance v4, Ljava/lang/StringBuilder;

    const-string v5, "locking odex store took "

    invoke-direct {v4, v5}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v4, v2, v3}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, " ms"

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v0, v2}, Landroid/util/Log;->v(Ljava/lang/String;Ljava/lang/String;)I
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    if-nez v1, :cond_0

    iget-object v0, p0, Lcom/facebook/common/dextricks/DexLibLoader$FileLocker;->a:Ljava/io/FileOutputStream;

    invoke-static {v0}, Lcom/facebook/common/dextricks/DexLibLoader;->a(Ljava/io/Closeable;)V

    :cond_0
    iput-object v1, p0, Lcom/facebook/common/dextricks/DexLibLoader$FileLocker;->b:Ljava/nio/channels/FileLock;

    return-void

    :catchall_0
    move-exception v0

    if-nez v1, :cond_1

    iget-object v1, p0, Lcom/facebook/common/dextricks/DexLibLoader$FileLocker;->a:Ljava/io/FileOutputStream;

    invoke-static {v1}, Lcom/facebook/common/dextricks/DexLibLoader;->a(Ljava/io/Closeable;)V

    :cond_1
    throw v0
.end method

.method public static a(Ljava/io/File;)Lcom/facebook/common/dextricks/DexLibLoader$FileLocker;
    .locals 1

    new-instance v0, Lcom/facebook/common/dextricks/DexLibLoader$FileLocker;

    invoke-direct {v0, p0}, Lcom/facebook/common/dextricks/DexLibLoader$FileLocker;-><init>(Ljava/io/File;)V

    return-object v0
.end method


# virtual methods
.method final a()V
    .locals 2

    :try_start_0
    iget-object v0, p0, Lcom/facebook/common/dextricks/DexLibLoader$FileLocker;->b:Ljava/nio/channels/FileLock;

    invoke-virtual {v0}, Ljava/nio/channels/FileLock;->release()V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    iget-object v0, p0, Lcom/facebook/common/dextricks/DexLibLoader$FileLocker;->a:Ljava/io/FileOutputStream;

    invoke-static {v0}, Lcom/facebook/common/dextricks/DexLibLoader;->a(Ljava/io/Closeable;)V

    const-string v0, "DexLibLoader"

    const-string v1, "released odex store lock"

    invoke-static {v0, v1}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    return-void

    :catchall_0
    move-exception v0

    iget-object v1, p0, Lcom/facebook/common/dextricks/DexLibLoader$FileLocker;->a:Ljava/io/FileOutputStream;

    invoke-static {v1}, Lcom/facebook/common/dextricks/DexLibLoader;->a(Ljava/io/Closeable;)V

    throw v0
.end method
