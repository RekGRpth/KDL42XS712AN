.class Lcom/facebook/common/gcinitopt/GcOptimizer$GcSentinel;
.super Ljava/lang/Object;
.source "GcOptimizer.java"


# direct methods
.method private constructor <init>()V
    .locals 0

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method synthetic constructor <init>(B)V
    .locals 0

    invoke-direct {p0}, Lcom/facebook/common/gcinitopt/GcOptimizer$GcSentinel;-><init>()V

    return-void
.end method


# virtual methods
.method protected finalize()V
    .locals 4

    invoke-static {}, Lcom/facebook/common/gcinitopt/GcOptimizer;->b()Lcom/facebook/common/references/OOMSoftReference;

    move-result-object v0

    if-eqz v0, :cond_0

    invoke-static {}, Lcom/facebook/common/gcinitopt/GcOptimizer;->b()Lcom/facebook/common/references/OOMSoftReference;

    move-result-object v0

    invoke-virtual {v0}, Lcom/facebook/common/references/OOMSoftReference;->a()Ljava/lang/Object;

    move-result-object v0

    if-nez v0, :cond_1

    :cond_0
    invoke-static {}, Lcom/facebook/common/gcinitopt/GcOptimizer;->a()Ljava/lang/Class;

    move-result-object v0

    const-string v1, "Detected that GC optimization was cleared."

    invoke-static {v0, v1}, Lcom/facebook/debug/log/BLog;->d(Ljava/lang/Class;Ljava/lang/String;)V

    :goto_0
    return-void

    :cond_1
    invoke-static {}, Lcom/facebook/common/gcinitopt/GcOptimizer;->d()J

    move-result-wide v0

    const-wide/32 v2, 0x2400000

    cmp-long v0, v0, v2

    if-ltz v0, :cond_2

    invoke-static {}, Lcom/facebook/common/gcinitopt/GcOptimizer;->a()Ljava/lang/Class;

    move-result-object v0

    const-string v1, "Private heap grown to target size - clearing GC optimization."

    invoke-static {v0, v1}, Lcom/facebook/debug/log/BLog;->b(Ljava/lang/Class;Ljava/lang/String;)V

    invoke-static {}, Lcom/facebook/common/gcinitopt/GcOptimizer;->c()V

    goto :goto_0

    :cond_2
    invoke-static {}, Lcom/facebook/common/gcinitopt/GcOptimizer;->e()Z

    move-result v0

    if-eqz v0, :cond_3

    invoke-static {}, Lcom/facebook/common/gcinitopt/GcOptimizer;->a()Ljava/lang/Class;

    move-result-object v0

    const-string v1, "OutOfMemory slack exceeded - clearing soft references to avoid expensive double garbage collection."

    invoke-static {v0, v1}, Lcom/facebook/debug/log/BLog;->d(Ljava/lang/Class;Ljava/lang/String;)V

    invoke-static {}, Lcom/facebook/common/gcinitopt/GcOptimizer;->c()V

    goto :goto_0

    :cond_3
    invoke-static {}, Lcom/facebook/common/gcinitopt/GcOptimizer;->a()Ljava/lang/Class;

    move-result-object v0

    const-string v1, "GcSentinel recycled"

    invoke-static {v0, v1}, Lcom/facebook/debug/log/BLog;->b(Ljava/lang/Class;Ljava/lang/String;)V

    new-instance v0, Lcom/facebook/common/gcinitopt/GcOptimizer$GcSentinel;

    invoke-direct {v0}, Lcom/facebook/common/gcinitopt/GcOptimizer$GcSentinel;-><init>()V

    goto :goto_0
.end method
