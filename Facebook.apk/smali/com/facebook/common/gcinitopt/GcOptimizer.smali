.class public Lcom/facebook/common/gcinitopt/GcOptimizer;
.super Ljava/lang/Object;
.source "GcOptimizer.java"


# static fields
.field private static final a:Ljava/lang/Class;

.field private static final b:J

.field private static c:J

.field private static d:Lcom/facebook/common/references/OOMSoftReference;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/facebook/common/references/OOMSoftReference",
            "<[B>;"
        }
    .end annotation
.end field


# direct methods
.method static constructor <clinit>()V
    .locals 3

    const-class v0, Lcom/facebook/common/gcinitopt/GcOptimizer;

    sput-object v0, Lcom/facebook/common/gcinitopt/GcOptimizer;->a:Ljava/lang/Class;

    sget-object v0, Ljava/util/concurrent/TimeUnit;->SECONDS:Ljava/util/concurrent/TimeUnit;

    const-wide/16 v1, 0x1e

    invoke-virtual {v0, v1, v2}, Ljava/util/concurrent/TimeUnit;->toMillis(J)J

    move-result-wide v0

    sput-wide v0, Lcom/facebook/common/gcinitopt/GcOptimizer;->b:J

    const-wide/16 v0, 0x0

    sput-wide v0, Lcom/facebook/common/gcinitopt/GcOptimizer;->c:J

    return-void
.end method

.method public constructor <init>()V
    .locals 0

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method static synthetic a()Ljava/lang/Class;
    .locals 1

    sget-object v0, Lcom/facebook/common/gcinitopt/GcOptimizer;->a:Ljava/lang/Class;

    return-object v0
.end method

.method public static a(Landroid/content/Context;)V
    .locals 4

    if-nez p0, :cond_0

    sget-object v0, Lcom/facebook/common/gcinitopt/GcOptimizer;->a:Ljava/lang/Class;

    const-string v1, "Attempting to enable with null context"

    invoke-static {v0, v1}, Lcom/facebook/debug/log/BLog;->e(Ljava/lang/Class;Ljava/lang/String;)V

    :goto_0
    return-void

    :cond_0
    invoke-static {p0}, Lcom/facebook/common/gcinitopt/GcOptimizer;->d(Landroid/content/Context;)Z

    move-result v0

    if-nez v0, :cond_1

    sget-object v0, Lcom/facebook/common/gcinitopt/GcOptimizer;->a:Ljava/lang/Class;

    const-string v1, "Disabled: Not configured"

    invoke-static {v0, v1}, Lcom/facebook/debug/log/BLog;->b(Ljava/lang/Class;Ljava/lang/String;)V

    goto :goto_0

    :cond_1
    sget-wide v0, Lcom/facebook/common/gcinitopt/GcOptimizer;->c:J

    const-wide/16 v2, 0x0

    cmp-long v0, v0, v2

    if-eqz v0, :cond_2

    sget-object v0, Lcom/facebook/common/gcinitopt/GcOptimizer;->a:Ljava/lang/Class;

    const-string v1, "Duplicate policy call detected - aborting"

    invoke-static {v0, v1}, Lcom/facebook/debug/log/BLog;->e(Ljava/lang/Class;Ljava/lang/String;)V

    goto :goto_0

    :cond_2
    invoke-static {}, Ljava/lang/Runtime;->getRuntime()Ljava/lang/Runtime;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/Runtime;->maxMemory()J

    move-result-wide v0

    const-wide/32 v2, 0x3f00000

    cmp-long v0, v0, v2

    if-gez v0, :cond_3

    sget-object v0, Lcom/facebook/common/gcinitopt/GcOptimizer;->a:Ljava/lang/Class;

    const-string v1, "Max heap too small - aborting GC policy"

    invoke-static {v0, v1}, Lcom/facebook/debug/log/BLog;->d(Ljava/lang/Class;Ljava/lang/String;)V

    goto :goto_0

    :cond_3
    sget-object v0, Lcom/facebook/common/gcinitopt/GcOptimizer;->a:Ljava/lang/Class;

    const-string v1, "Enable"

    invoke-static {v0, v1}, Lcom/facebook/debug/log/BLog;->b(Ljava/lang/Class;Ljava/lang/String;)V

    new-instance v0, Lcom/facebook/common/gcinitopt/GcOptimizer$1;

    invoke-direct {v0, p0}, Lcom/facebook/common/gcinitopt/GcOptimizer$1;-><init>(Landroid/content/Context;)V

    invoke-virtual {v0}, Lcom/facebook/common/gcinitopt/GcOptimizer$1;->start()V

    invoke-static {}, Lcom/facebook/common/gcinitopt/GcOptimizer;->h()J

    move-result-wide v0

    sput-wide v0, Lcom/facebook/common/gcinitopt/GcOptimizer;->c:J

    const/high16 v0, 0x1800000

    new-array v0, v0, [B

    new-instance v1, Lcom/facebook/common/references/OOMSoftReference;

    invoke-direct {v1}, Lcom/facebook/common/references/OOMSoftReference;-><init>()V

    sput-object v1, Lcom/facebook/common/gcinitopt/GcOptimizer;->d:Lcom/facebook/common/references/OOMSoftReference;

    invoke-virtual {v1, v0}, Lcom/facebook/common/references/OOMSoftReference;->a(Ljava/lang/Object;)V

    new-instance v0, Lcom/facebook/common/gcinitopt/GcOptimizer$GcSentinel;

    const/4 v1, 0x0

    invoke-direct {v0, v1}, Lcom/facebook/common/gcinitopt/GcOptimizer$GcSentinel;-><init>(B)V

    new-instance v0, Landroid/os/Handler;

    invoke-direct {v0}, Landroid/os/Handler;-><init>()V

    new-instance v1, Lcom/facebook/common/gcinitopt/GcOptimizer$2;

    invoke-direct {v1}, Lcom/facebook/common/gcinitopt/GcOptimizer$2;-><init>()V

    sget-wide v2, Lcom/facebook/common/gcinitopt/GcOptimizer;->b:J

    invoke-virtual {v0, v1, v2, v3}, Landroid/os/Handler;->postDelayed(Ljava/lang/Runnable;J)Z

    goto :goto_0
.end method

.method static a(Landroid/content/Context;Z)V
    .locals 3
    .param p0    # Landroid/content/Context;
        .annotation runtime Ljavax/annotation/Nonnull;
        .end annotation
    .end param

    invoke-static {p0}, Lcom/facebook/common/gcinitopt/GcOptimizer;->c(Landroid/content/Context;)Ljava/io/File;

    move-result-object v0

    if-eqz p1, :cond_1

    sget-object v1, Lcom/facebook/common/gcinitopt/GcOptimizer;->a:Ljava/lang/Class;

    const-string v2, "Configure for next cold start: enable"

    invoke-static {v1, v2}, Lcom/facebook/debug/log/BLog;->b(Ljava/lang/Class;Ljava/lang/String;)V

    :try_start_0
    invoke-virtual {v0}, Ljava/io/File;->exists()Z

    move-result v1

    if-nez v1, :cond_0

    invoke-virtual {v0}, Ljava/io/File;->createNewFile()Z

    move-result v0

    if-nez v0, :cond_0

    sget-object v0, Lcom/facebook/common/gcinitopt/GcOptimizer;->a:Ljava/lang/Class;

    const-string v1, "Can\'t create file"

    invoke-static {v0, v1}, Lcom/facebook/debug/log/BLog;->d(Ljava/lang/Class;Ljava/lang/String;)V
    :try_end_0
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_0

    :cond_0
    :goto_0
    return-void

    :catch_0
    move-exception v0

    sget-object v1, Lcom/facebook/common/gcinitopt/GcOptimizer;->a:Ljava/lang/Class;

    const-string v2, "Can\'t create file"

    invoke-static {v1, v2, v0}, Lcom/facebook/debug/log/BLog;->d(Ljava/lang/Class;Ljava/lang/String;Ljava/lang/Throwable;)V

    goto :goto_0

    :cond_1
    sget-object v1, Lcom/facebook/common/gcinitopt/GcOptimizer;->a:Ljava/lang/Class;

    const-string v2, "Configure for next cold start: disable"

    invoke-static {v1, v2}, Lcom/facebook/debug/log/BLog;->b(Ljava/lang/Class;Ljava/lang/String;)V

    invoke-virtual {v0}, Ljava/io/File;->exists()Z

    move-result v1

    if-eqz v1, :cond_0

    invoke-virtual {v0}, Ljava/io/File;->delete()Z

    move-result v0

    if-nez v0, :cond_0

    sget-object v0, Lcom/facebook/common/gcinitopt/GcOptimizer;->a:Ljava/lang/Class;

    const-string v1, "Can\'t delete"

    invoke-static {v0, v1}, Lcom/facebook/debug/log/BLog;->d(Ljava/lang/Class;Ljava/lang/String;)V

    goto :goto_0
.end method

.method static synthetic b()Lcom/facebook/common/references/OOMSoftReference;
    .locals 1

    sget-object v0, Lcom/facebook/common/gcinitopt/GcOptimizer;->d:Lcom/facebook/common/references/OOMSoftReference;

    return-object v0
.end method

.method static synthetic b(Landroid/content/Context;)Ljava/io/File;
    .locals 1

    invoke-static {p0}, Lcom/facebook/common/gcinitopt/GcOptimizer;->c(Landroid/content/Context;)Ljava/io/File;

    move-result-object v0

    return-object v0
.end method

.method private static c(Landroid/content/Context;)Ljava/io/File;
    .locals 3
    .param p0    # Landroid/content/Context;
        .annotation runtime Ljavax/annotation/Nonnull;
        .end annotation
    .end param

    new-instance v0, Ljava/io/File;

    invoke-virtual {p0}, Landroid/content/Context;->getFilesDir()Ljava/io/File;

    move-result-object v1

    const-string v2, "gcinitopt"

    invoke-direct {v0, v1, v2}, Ljava/io/File;-><init>(Ljava/io/File;Ljava/lang/String;)V

    return-object v0
.end method

.method static synthetic c()V
    .locals 0

    invoke-static {}, Lcom/facebook/common/gcinitopt/GcOptimizer;->i()V

    return-void
.end method

.method static synthetic d()J
    .locals 2

    invoke-static {}, Lcom/facebook/common/gcinitopt/GcOptimizer;->f()J

    move-result-wide v0

    return-wide v0
.end method

.method private static d(Landroid/content/Context;)Z
    .locals 1
    .param p0    # Landroid/content/Context;
        .annotation runtime Ljavax/annotation/Nonnull;
        .end annotation
    .end param

    invoke-static {p0}, Lcom/facebook/common/gcinitopt/GcOptimizer;->c(Landroid/content/Context;)Ljava/io/File;

    move-result-object v0

    invoke-virtual {v0}, Ljava/io/File;->exists()Z

    move-result v0

    return v0
.end method

.method static synthetic e()Z
    .locals 1

    invoke-static {}, Lcom/facebook/common/gcinitopt/GcOptimizer;->g()Z

    move-result v0

    return v0
.end method

.method private static f()J
    .locals 4

    const-wide/16 v0, 0x0

    sget-wide v2, Lcom/facebook/common/gcinitopt/GcOptimizer;->c:J

    cmp-long v2, v2, v0

    if-nez v2, :cond_0

    :goto_0
    return-wide v0

    :cond_0
    invoke-static {}, Lcom/facebook/common/gcinitopt/GcOptimizer;->h()J

    move-result-wide v0

    sget-wide v2, Lcom/facebook/common/gcinitopt/GcOptimizer;->c:J

    sub-long/2addr v0, v2

    goto :goto_0
.end method

.method private static g()Z
    .locals 4

    invoke-static {}, Lcom/facebook/common/gcinitopt/GcOptimizer;->h()J

    move-result-wide v0

    const-wide/32 v2, 0x800000

    add-long/2addr v0, v2

    invoke-static {}, Ljava/lang/Runtime;->getRuntime()Ljava/lang/Runtime;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/Runtime;->maxMemory()J

    move-result-wide v2

    cmp-long v0, v0, v2

    if-ltz v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method private static h()J
    .locals 5

    invoke-static {}, Ljava/lang/Runtime;->getRuntime()Ljava/lang/Runtime;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/Runtime;->totalMemory()J

    move-result-wide v1

    invoke-virtual {v0}, Ljava/lang/Runtime;->freeMemory()J

    move-result-wide v3

    sub-long v0, v1, v3

    return-wide v0
.end method

.method private static i()V
    .locals 1

    const/4 v0, 0x0

    sput-object v0, Lcom/facebook/common/gcinitopt/GcOptimizer;->d:Lcom/facebook/common/references/OOMSoftReference;

    return-void
.end method
