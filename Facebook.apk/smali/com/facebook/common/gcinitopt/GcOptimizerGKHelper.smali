.class public Lcom/facebook/common/gcinitopt/GcOptimizerGKHelper;
.super Ljava/lang/Object;
.source "GcOptimizerGKHelper.java"

# interfaces
.implements Lcom/facebook/common/init/INeedInit;


# instance fields
.field private final a:Landroid/content/Context;

.field private final b:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider",
            "<",
            "Lcom/facebook/common/util/TriState;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method public constructor <init>(Landroid/content/Context;Ljavax/inject/Provider;)V
    .locals 0
    .param p2    # Ljavax/inject/Provider;
        .annotation runtime Lcom/facebook/common/gcinitopt/IsGcOptimizationEnabled;
        .end annotation
    .end param
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Landroid/content/Context;",
            "Ljavax/inject/Provider",
            "<",
            "Lcom/facebook/common/util/TriState;",
            ">;)V"
        }
    .end annotation

    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    iput-object p1, p0, Lcom/facebook/common/gcinitopt/GcOptimizerGKHelper;->a:Landroid/content/Context;

    iput-object p2, p0, Lcom/facebook/common/gcinitopt/GcOptimizerGKHelper;->b:Ljavax/inject/Provider;

    return-void
.end method


# virtual methods
.method public final a()V
    .locals 3

    iget-object v1, p0, Lcom/facebook/common/gcinitopt/GcOptimizerGKHelper;->a:Landroid/content/Context;

    iget-object v0, p0, Lcom/facebook/common/gcinitopt/GcOptimizerGKHelper;->b:Ljavax/inject/Provider;

    invoke-interface {v0}, Ljavax/inject/Provider;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/common/util/TriState;

    const/4 v2, 0x0

    invoke-virtual {v0, v2}, Lcom/facebook/common/util/TriState;->asBoolean(Z)Z

    move-result v0

    invoke-static {v1, v0}, Lcom/facebook/common/gcinitopt/GcOptimizer;->a(Landroid/content/Context;Z)V

    return-void
.end method
