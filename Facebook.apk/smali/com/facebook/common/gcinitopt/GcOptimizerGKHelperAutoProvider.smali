.class public final Lcom/facebook/common/gcinitopt/GcOptimizerGKHelperAutoProvider;
.super Lcom/facebook/inject/AbstractProvider;
.source "GcOptimizerGKHelperAutoProvider.java"


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/facebook/inject/AbstractProvider",
        "<",
        "Lcom/facebook/common/gcinitopt/GcOptimizerGKHelper;",
        ">;"
    }
.end annotation


# direct methods
.method public constructor <init>()V
    .locals 0

    invoke-direct {p0}, Lcom/facebook/inject/AbstractProvider;-><init>()V

    return-void
.end method

.method private a()Lcom/facebook/common/gcinitopt/GcOptimizerGKHelper;
    .locals 4

    new-instance v1, Lcom/facebook/common/gcinitopt/GcOptimizerGKHelper;

    invoke-virtual {p0}, Lcom/facebook/common/gcinitopt/GcOptimizerGKHelperAutoProvider;->getApplicationInjector()Lcom/facebook/inject/FbInjector;

    move-result-object v0

    const-class v2, Landroid/content/Context;

    invoke-virtual {v0, v2}, Lcom/facebook/inject/FbInjector;->getInstance(Ljava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/content/Context;

    const-class v2, Lcom/facebook/common/util/TriState;

    const-class v3, Lcom/facebook/common/gcinitopt/IsGcOptimizationEnabled;

    invoke-virtual {p0, v2, v3}, Lcom/facebook/common/gcinitopt/GcOptimizerGKHelperAutoProvider;->getProvider(Ljava/lang/Class;Ljava/lang/Class;)Ljavax/inject/Provider;

    move-result-object v2

    invoke-direct {v1, v0, v2}, Lcom/facebook/common/gcinitopt/GcOptimizerGKHelper;-><init>(Landroid/content/Context;Ljavax/inject/Provider;)V

    return-object v1
.end method


# virtual methods
.method public final synthetic get()Ljava/lang/Object;
    .locals 1

    invoke-direct {p0}, Lcom/facebook/common/gcinitopt/GcOptimizerGKHelperAutoProvider;->a()Lcom/facebook/common/gcinitopt/GcOptimizerGKHelper;

    move-result-object v0

    return-object v0
.end method
