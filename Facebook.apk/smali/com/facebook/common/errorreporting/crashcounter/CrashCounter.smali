.class public Lcom/facebook/common/errorreporting/crashcounter/CrashCounter;
.super Ljava/lang/Object;
.source "CrashCounter.java"


# instance fields
.field private final a:Landroid/content/Context;

.field private final b:Lcom/facebook/common/errorreporting/crashcounter/CrashCounter$CalendarProvider;

.field private final c:Ljava/lang/String;

.field private d:Z

.field private e:I

.field private f:[J

.field private g:[I

.field private h:Ljava/util/HashMap;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/HashMap",
            "<",
            "Ljava/lang/String;",
            "Ljava/lang/Long;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method public constructor <init>(Landroid/content/Context;)V
    .locals 2

    new-instance v0, Lcom/facebook/common/errorreporting/crashcounter/CrashCounter$DefaultCalendarProvider;

    const/4 v1, 0x0

    invoke-direct {v0, v1}, Lcom/facebook/common/errorreporting/crashcounter/CrashCounter$DefaultCalendarProvider;-><init>(B)V

    invoke-direct {p0, p1, v0}, Lcom/facebook/common/errorreporting/crashcounter/CrashCounter;-><init>(Landroid/content/Context;Lcom/facebook/common/errorreporting/crashcounter/CrashCounter$CalendarProvider;)V

    return-void
.end method

.method private constructor <init>(Landroid/content/Context;Lcom/facebook/common/errorreporting/crashcounter/CrashCounter$CalendarProvider;)V
    .locals 3

    const/16 v2, 0x18

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    iput-object p1, p0, Lcom/facebook/common/errorreporting/crashcounter/CrashCounter;->a:Landroid/content/Context;

    iput-object p2, p0, Lcom/facebook/common/errorreporting/crashcounter/CrashCounter;->b:Lcom/facebook/common/errorreporting/crashcounter/CrashCounter$CalendarProvider;

    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {p1}, Landroid/content/Context;->getPackageName()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, ".crash.count"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/common/errorreporting/crashcounter/CrashCounter;->c:Ljava/lang/String;

    new-array v0, v2, [J

    iput-object v0, p0, Lcom/facebook/common/errorreporting/crashcounter/CrashCounter;->f:[J

    new-array v0, v2, [I

    iput-object v0, p0, Lcom/facebook/common/errorreporting/crashcounter/CrashCounter;->g:[I

    new-instance v0, Ljava/util/HashMap;

    invoke-direct {v0}, Ljava/util/HashMap;-><init>()V

    iput-object v0, p0, Lcom/facebook/common/errorreporting/crashcounter/CrashCounter;->h:Ljava/util/HashMap;

    return-void
.end method

.method private a(Landroid/content/Intent;)V
    .locals 7

    const-wide/16 v5, 0x0

    const/4 v4, 0x0

    const-string v0, "crash_count"

    invoke-virtual {p1, v0, v4}, Landroid/content/Intent;->getIntExtra(Ljava/lang/String;I)I

    move-result v0

    const-string v1, "crash_count_24h"

    invoke-virtual {p1, v1}, Landroid/content/Intent;->getIntArrayExtra(Ljava/lang/String;)[I

    move-result-object v1

    const-string v2, "crash_count_timestamp_24h"

    invoke-virtual {p1, v2}, Landroid/content/Intent;->getLongArrayExtra(Ljava/lang/String;)[J

    move-result-object v2

    iput v0, p0, Lcom/facebook/common/errorreporting/crashcounter/CrashCounter;->e:I

    if-eqz v1, :cond_1

    array-length v0, v1

    iget-object v3, p0, Lcom/facebook/common/errorreporting/crashcounter/CrashCounter;->g:[I

    array-length v3, v3

    if-ne v0, v3, :cond_1

    iget-object v0, p0, Lcom/facebook/common/errorreporting/crashcounter/CrashCounter;->g:[I

    iget-object v3, p0, Lcom/facebook/common/errorreporting/crashcounter/CrashCounter;->g:[I

    array-length v3, v3

    invoke-static {v1, v4, v0, v4, v3}, Ljava/lang/System;->arraycopy(Ljava/lang/Object;ILjava/lang/Object;II)V

    :goto_0
    if-eqz v2, :cond_2

    array-length v0, v2

    iget-object v1, p0, Lcom/facebook/common/errorreporting/crashcounter/CrashCounter;->f:[J

    array-length v1, v1

    if-ne v0, v1, :cond_2

    iget-object v0, p0, Lcom/facebook/common/errorreporting/crashcounter/CrashCounter;->f:[J

    iget-object v1, p0, Lcom/facebook/common/errorreporting/crashcounter/CrashCounter;->f:[J

    array-length v1, v1

    invoke-static {v2, v4, v0, v4, v1}, Ljava/lang/System;->arraycopy(Ljava/lang/Object;ILjava/lang/Object;II)V

    :goto_1
    iget-object v0, p0, Lcom/facebook/common/errorreporting/crashcounter/CrashCounter;->h:Ljava/util/HashMap;

    invoke-virtual {v0}, Ljava/util/HashMap;->clear()V

    invoke-virtual {p1}, Landroid/content/Intent;->getExtras()Landroid/os/Bundle;

    move-result-object v0

    if-eqz v0, :cond_3

    invoke-virtual {p1}, Landroid/content/Intent;->getExtras()Landroid/os/Bundle;

    move-result-object v0

    invoke-virtual {v0}, Landroid/os/Bundle;->keySet()Ljava/util/Set;

    move-result-object v0

    invoke-interface {v0}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :cond_0
    :goto_2
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_3

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/String;

    const-string v2, "flag_"

    invoke-virtual {v0, v2}, Ljava/lang/String;->startsWith(Ljava/lang/String;)Z

    move-result v2

    if-eqz v2, :cond_0

    const/4 v2, 0x5

    invoke-virtual {v0, v2}, Ljava/lang/String;->substring(I)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {p1, v0, v5, v6}, Landroid/content/Intent;->getLongExtra(Ljava/lang/String;J)J

    move-result-wide v3

    iget-object v0, p0, Lcom/facebook/common/errorreporting/crashcounter/CrashCounter;->h:Ljava/util/HashMap;

    invoke-static {v3, v4}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v3

    invoke-virtual {v0, v2, v3}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    goto :goto_2

    :cond_1
    iget-object v0, p0, Lcom/facebook/common/errorreporting/crashcounter/CrashCounter;->g:[I

    invoke-static {v0, v4}, Ljava/util/Arrays;->fill([II)V

    goto :goto_0

    :cond_2
    iget-object v0, p0, Lcom/facebook/common/errorreporting/crashcounter/CrashCounter;->f:[J

    invoke-static {v0, v5, v6}, Ljava/util/Arrays;->fill([JJ)V

    goto :goto_1

    :cond_3
    return-void
.end method

.method private b(Landroid/content/Intent;)V
    .locals 4

    const-string v0, "crash_count"

    iget v1, p0, Lcom/facebook/common/errorreporting/crashcounter/CrashCounter;->e:I

    invoke-virtual {p1, v0, v1}, Landroid/content/Intent;->putExtra(Ljava/lang/String;I)Landroid/content/Intent;

    const-string v0, "crash_count_24h"

    iget-object v1, p0, Lcom/facebook/common/errorreporting/crashcounter/CrashCounter;->g:[I

    invoke-virtual {p1, v0, v1}, Landroid/content/Intent;->putExtra(Ljava/lang/String;[I)Landroid/content/Intent;

    const-string v0, "crash_count_timestamp_24h"

    iget-object v1, p0, Lcom/facebook/common/errorreporting/crashcounter/CrashCounter;->f:[J

    invoke-virtual {p1, v0, v1}, Landroid/content/Intent;->putExtra(Ljava/lang/String;[J)Landroid/content/Intent;

    iget-object v0, p0, Lcom/facebook/common/errorreporting/crashcounter/CrashCounter;->h:Ljava/util/HashMap;

    invoke-virtual {v0}, Ljava/util/HashMap;->entrySet()Ljava/util/Set;

    move-result-object v0

    invoke-interface {v0}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    move-result-object v2

    :goto_0
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/util/Map$Entry;

    new-instance v3, Ljava/lang/StringBuilder;

    const-string v1, "flag_"

    invoke-direct {v3, v1}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-interface {v0}, Ljava/util/Map$Entry;->getKey()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Ljava/lang/String;

    invoke-virtual {v3, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-interface {v0}, Ljava/util/Map$Entry;->getValue()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/io/Serializable;

    invoke-virtual {p1, v1, v0}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/io/Serializable;)Landroid/content/Intent;

    goto :goto_0

    :cond_0
    return-void
.end method

.method private h()V
    .locals 2

    iget-boolean v0, p0, Lcom/facebook/common/errorreporting/crashcounter/CrashCounter;->d:Z

    if-nez v0, :cond_0

    new-instance v0, Ljava/lang/IllegalStateException;

    const-string v1, "State is not restored."

    invoke-direct {v0, v1}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    throw v0

    :cond_0
    return-void
.end method

.method private i()Ljava/util/Calendar;
    .locals 3

    const/4 v2, 0x0

    iget-object v0, p0, Lcom/facebook/common/errorreporting/crashcounter/CrashCounter;->b:Lcom/facebook/common/errorreporting/crashcounter/CrashCounter$CalendarProvider;

    invoke-interface {v0}, Lcom/facebook/common/errorreporting/crashcounter/CrashCounter$CalendarProvider;->a()Ljava/util/Calendar;

    move-result-object v0

    const/16 v1, 0xc

    invoke-virtual {v0, v1, v2}, Ljava/util/Calendar;->set(II)V

    const/16 v1, 0xd

    invoke-virtual {v0, v1, v2}, Ljava/util/Calendar;->set(II)V

    const/16 v1, 0xe

    invoke-virtual {v0, v1, v2}, Ljava/util/Calendar;->set(II)V

    return-object v0
.end method


# virtual methods
.method public final a()V
    .locals 4

    iget-object v0, p0, Lcom/facebook/common/errorreporting/crashcounter/CrashCounter;->a:Landroid/content/Context;

    const/4 v1, 0x0

    new-instance v2, Landroid/content/IntentFilter;

    iget-object v3, p0, Lcom/facebook/common/errorreporting/crashcounter/CrashCounter;->c:Ljava/lang/String;

    invoke-direct {v2, v3}, Landroid/content/IntentFilter;-><init>(Ljava/lang/String;)V

    invoke-virtual {v0, v1, v2}, Landroid/content/Context;->registerReceiver(Landroid/content/BroadcastReceiver;Landroid/content/IntentFilter;)Landroid/content/Intent;

    move-result-object v0

    if-nez v0, :cond_0

    new-instance v0, Landroid/content/Intent;

    invoke-direct {v0}, Landroid/content/Intent;-><init>()V

    :cond_0
    invoke-direct {p0, v0}, Lcom/facebook/common/errorreporting/crashcounter/CrashCounter;->a(Landroid/content/Intent;)V

    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/facebook/common/errorreporting/crashcounter/CrashCounter;->d:Z

    return-void
.end method

.method public final a(Ljava/lang/String;)V
    .locals 4

    invoke-direct {p0}, Lcom/facebook/common/errorreporting/crashcounter/CrashCounter;->h()V

    invoke-direct {p0}, Lcom/facebook/common/errorreporting/crashcounter/CrashCounter;->i()Ljava/util/Calendar;

    move-result-object v0

    iget-object v1, p0, Lcom/facebook/common/errorreporting/crashcounter/CrashCounter;->h:Ljava/util/HashMap;

    invoke-virtual {v0}, Ljava/util/Calendar;->getTimeInMillis()J

    move-result-wide v2

    invoke-static {v2, v3}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v0

    invoke-virtual {v1, p1, v0}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    return-void
.end method

.method public final b()V
    .locals 2

    new-instance v0, Landroid/content/Intent;

    iget-object v1, p0, Lcom/facebook/common/errorreporting/crashcounter/CrashCounter;->c:Ljava/lang/String;

    invoke-direct {v0, v1}, Landroid/content/Intent;-><init>(Ljava/lang/String;)V

    invoke-direct {p0, v0}, Lcom/facebook/common/errorreporting/crashcounter/CrashCounter;->b(Landroid/content/Intent;)V

    iget-object v1, p0, Lcom/facebook/common/errorreporting/crashcounter/CrashCounter;->a:Landroid/content/Context;

    invoke-virtual {v1, v0}, Landroid/content/Context;->sendStickyBroadcast(Landroid/content/Intent;)V

    return-void
.end method

.method public final b(Ljava/lang/String;)Z
    .locals 6

    const/4 v1, 0x0

    invoke-direct {p0}, Lcom/facebook/common/errorreporting/crashcounter/CrashCounter;->h()V

    invoke-direct {p0}, Lcom/facebook/common/errorreporting/crashcounter/CrashCounter;->i()Ljava/util/Calendar;

    move-result-object v2

    iget-object v0, p0, Lcom/facebook/common/errorreporting/crashcounter/CrashCounter;->h:Ljava/util/HashMap;

    invoke-virtual {v0, p1}, Ljava/util/HashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/Long;

    if-eqz v0, :cond_1

    invoke-virtual {v2}, Ljava/util/Calendar;->getTimeInMillis()J

    move-result-wide v2

    invoke-virtual {v0}, Ljava/lang/Long;->longValue()J

    move-result-wide v4

    sub-long/2addr v2, v4

    const-wide/32 v4, 0x5265c00

    cmp-long v0, v2, v4

    if-gez v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    move v0, v1

    goto :goto_0

    :cond_1
    move v0, v1

    goto :goto_0
.end method

.method public final c()V
    .locals 8

    invoke-direct {p0}, Lcom/facebook/common/errorreporting/crashcounter/CrashCounter;->h()V

    iget v0, p0, Lcom/facebook/common/errorreporting/crashcounter/CrashCounter;->e:I

    add-int/lit8 v0, v0, 0x1

    iput v0, p0, Lcom/facebook/common/errorreporting/crashcounter/CrashCounter;->e:I

    invoke-direct {p0}, Lcom/facebook/common/errorreporting/crashcounter/CrashCounter;->i()Ljava/util/Calendar;

    move-result-object v0

    const/16 v1, 0xb

    invoke-virtual {v0, v1}, Ljava/util/Calendar;->get(I)I

    move-result v1

    iget-object v2, p0, Lcom/facebook/common/errorreporting/crashcounter/CrashCounter;->f:[J

    aget-wide v2, v2, v1

    invoke-virtual {v0}, Ljava/util/Calendar;->getTimeInMillis()J

    move-result-wide v4

    sub-long v2, v4, v2

    const-wide/16 v6, 0x0

    cmp-long v0, v2, v6

    if-ltz v0, :cond_0

    const-wide/32 v6, 0x36ee80

    cmp-long v0, v2, v6

    if-gez v0, :cond_0

    iget-object v0, p0, Lcom/facebook/common/errorreporting/crashcounter/CrashCounter;->g:[I

    aget v2, v0, v1

    add-int/lit8 v2, v2, 0x1

    aput v2, v0, v1

    :goto_0
    iget-object v0, p0, Lcom/facebook/common/errorreporting/crashcounter/CrashCounter;->f:[J

    aput-wide v4, v0, v1

    return-void

    :cond_0
    iget-object v0, p0, Lcom/facebook/common/errorreporting/crashcounter/CrashCounter;->g:[I

    const/4 v2, 0x1

    aput v2, v0, v1

    goto :goto_0
.end method

.method public final d()I
    .locals 1

    invoke-direct {p0}, Lcom/facebook/common/errorreporting/crashcounter/CrashCounter;->h()V

    iget v0, p0, Lcom/facebook/common/errorreporting/crashcounter/CrashCounter;->e:I

    return v0
.end method

.method public final e()I
    .locals 8

    const/4 v0, 0x0

    invoke-direct {p0}, Lcom/facebook/common/errorreporting/crashcounter/CrashCounter;->h()V

    invoke-direct {p0}, Lcom/facebook/common/errorreporting/crashcounter/CrashCounter;->i()Ljava/util/Calendar;

    move-result-object v1

    invoke-virtual {v1}, Ljava/util/Calendar;->getTimeInMillis()J

    move-result-wide v2

    move v1, v0

    :goto_0
    const/16 v4, 0x18

    if-ge v1, v4, :cond_1

    iget-object v4, p0, Lcom/facebook/common/errorreporting/crashcounter/CrashCounter;->f:[J

    aget-wide v4, v4, v1

    sub-long v4, v2, v4

    const-wide/16 v6, 0x0

    cmp-long v6, v4, v6

    if-ltz v6, :cond_0

    const-wide/32 v6, 0x5265c00

    cmp-long v4, v4, v6

    if-gez v4, :cond_0

    iget-object v4, p0, Lcom/facebook/common/errorreporting/crashcounter/CrashCounter;->g:[I

    aget v4, v4, v1

    add-int/2addr v0, v4

    :cond_0
    add-int/lit8 v1, v1, 0x1

    goto :goto_0

    :cond_1
    return v0
.end method

.method public final f()Lcom/facebook/acra/CustomReportDataSupplier;
    .locals 1

    new-instance v0, Lcom/facebook/common/errorreporting/crashcounter/CrashCounter$1;

    invoke-direct {v0, p0}, Lcom/facebook/common/errorreporting/crashcounter/CrashCounter$1;-><init>(Lcom/facebook/common/errorreporting/crashcounter/CrashCounter;)V

    return-object v0
.end method

.method public final g()Lcom/facebook/acra/CustomReportDataSupplier;
    .locals 1

    new-instance v0, Lcom/facebook/common/errorreporting/crashcounter/CrashCounter$2;

    invoke-direct {v0, p0}, Lcom/facebook/common/errorreporting/crashcounter/CrashCounter$2;-><init>(Lcom/facebook/common/errorreporting/crashcounter/CrashCounter;)V

    return-object v0
.end method
