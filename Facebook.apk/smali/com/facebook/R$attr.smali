.class public Lcom/facebook/R$attr;
.super Ljava/lang/Object;
.source "R.java"


# static fields
.field public static final accessory_text:I = 0x7f01008c

.field public static final actionBarDivider:I = 0x7f0100ab

.field public static final actionBarForceSupport:I = 0x7f0100a1

.field public static final actionBarItemBackground:I = 0x7f0100ac

.field public static final actionBarSize:I = 0x7f0100aa

.field public static final actionBarSplitStyle:I = 0x7f0100a8

.field public static final actionBarStyle:I = 0x7f0100a7

.field public static final actionBarTabBarSelectionDrawable:I = 0x7f0100a4

.field public static final actionBarTabBarStyle:I = 0x7f0100a3

.field public static final actionBarTabStyle:I = 0x7f0100a2

.field public static final actionBarTabTextStyle:I = 0x7f0100a5

.field public static final actionBarWidgetTheme:I = 0x7f0100a9

.field public static final actionButtonBackground:I = 0x7f01012c

.field public static final actionButtonDrawable:I = 0x7f01012a

.field public static final actionButtonPadding:I = 0x7f01012d

.field public static final actionButtonSplitBarStyle:I = 0x7f0100b3

.field public static final actionButtonStyle:I = 0x7f0100b2

.field public static final actionButtonText:I = 0x7f01012b

.field public static final actionButtonTextAppearance:I = 0x7f01012e

.field public static final actionButtonTheme:I = 0x7f01012f

.field public static final actionDropDownStyle:I = 0x7f0100e1

.field public static final actionLayout:I = 0x7f0100e8

.field public static final actionMenuTextAppearance:I = 0x7f0100ad

.field public static final actionMenuTextColor:I = 0x7f0100ae

.field public static final actionModeBackground:I = 0x7f0100d6

.field public static final actionModeCloseButtonStyle:I = 0x7f0100d5

.field public static final actionModeCloseDrawable:I = 0x7f0100d8

.field public static final actionModeCopyDrawable:I = 0x7f0100da

.field public static final actionModeCutDrawable:I = 0x7f0100d9

.field public static final actionModeFindDrawable:I = 0x7f0100de

.field public static final actionModePasteDrawable:I = 0x7f0100db

.field public static final actionModePopupWindowStyle:I = 0x7f0100e0

.field public static final actionModeSelectAllDrawable:I = 0x7f0100dc

.field public static final actionModeShareDrawable:I = 0x7f0100dd

.field public static final actionModeSplitBackground:I = 0x7f0100d7

.field public static final actionModeStyle:I = 0x7f0100d4

.field public static final actionModeWebSearchDrawable:I = 0x7f0100df

.field public static final actionOverflowButtonStyle:I = 0x7f0100a6

.field public static final actionProviderClass:I = 0x7f0100ea

.field public static final actionViewClass:I = 0x7f0100e9

.field public static final activeSrc:I = 0x7f01015a

.field public static final activeStarDrawable:I = 0x7f0102a0

.field public static final active_src:I = 0x7f0102d2

.field public static final activityChooserViewStyle:I = 0x7f010107

.field public static final activityClass:I = 0x7f010000

.field public static final adSize:I = 0x7f0102ac

.field public static final adSizes:I = 0x7f0102ad

.field public static final adUnitId:I = 0x7f0102ae

.field public static final addMembersButtonStyle:I = 0x7f01026c

.field public static final addMembersCreateThreadStyle:I = 0x7f010269

.field public static final addMembersDividerStyle:I = 0x7f01026a

.field public static final addMembersFooterStyle:I = 0x7f01026b

.field public static final addMembersListSelector:I = 0x7f01026e

.field public static final addMembersRowPadding:I = 0x7f01026d

.field public static final adjustLRGravityByTextDirectionCompat:I = 0x7f010050

.field public static final adjustViewBounds:I = 0x7f01003e

.field public static final adminMessageAddPeopleDrawable:I = 0x7f010252

.field public static final adminMessageChangePictureDrawable:I = 0x7f010255

.field public static final adminMessageEditNameDrawable:I = 0x7f010254

.field public static final adminMessageIncomingCallDrawable:I = 0x7f010258

.field public static final adminMessageItemStyle:I = 0x7f01024e

.field public static final adminMessageItemViewAttachmentViewStyle:I = 0x7f010251

.field public static final adminMessageItemViewDividerVisibility:I = 0x7f01024f

.field public static final adminMessageItemViewTextStyle:I = 0x7f010250

.field public static final adminMessageLeaveConversationDrawable:I = 0x7f010253

.field public static final adminMessageMissedCallDrawable:I = 0x7f010257

.field public static final adminMessageOutgoingCallDrawable:I = 0x7f010259

.field public static final adminMessageVideoCallDrawable:I = 0x7f010256

.field public static final alignment:I = 0x7f01000f

.field public static final alignmentMode:I = 0x7f010335

.field public static final allCaps:I = 0x7f01000c

.field public static final allowDpadPaging:I = 0x7f010154

.field public static final allowImeActionsWithMultiLine:I = 0x7f01004c

.field public static final allowPastingSpans:I = 0x7f01004d

.field public static final allowTextureView:I = 0x7f010176

.field public static final angle:I = 0x7f01007f

.field public static final animateHeightChanges:I = 0x7f0102ff

.field public static final animateOutDirection:I = 0x7f010199

.field public static final animationDurationKS:I = 0x7f01030f

.field public static final asCircle:I = 0x7f010138

.field public static final audioRecordBanner:I = 0x7f010276

.field public static final audioRecordButtonNormal:I = 0x7f010270

.field public static final audioRecordButtonPressed:I = 0x7f010272

.field public static final audioRecordButtonSelected:I = 0x7f010271

.field public static final audioRecordDoneButton:I = 0x7f010275

.field public static final audioRecordNormalPopup:I = 0x7f010273

.field public static final audioRecordPatternLeftNormal:I = 0x7f01027b

.field public static final audioRecordPatternLeftSelected:I = 0x7f01027c

.field public static final audioRecordPatternRightNormal:I = 0x7f010279

.field public static final audioRecordPatternRightSelected:I = 0x7f01027a

.field public static final audioRecordPauseLeftNormal:I = 0x7f01027d

.field public static final audioRecordPauseLeftSelected:I = 0x7f01027e

.field public static final audioRecordPauseRightNormal:I = 0x7f01027f

.field public static final audioRecordPauseRightSelected:I = 0x7f010280

.field public static final audioRecordPlayLeftNormal:I = 0x7f010281

.field public static final audioRecordPlayLeftSelected:I = 0x7f010282

.field public static final audioRecordPlayRightNormal:I = 0x7f010283

.field public static final audioRecordPlayRightSelected:I = 0x7f010284

.field public static final audioRecordPressedPopup:I = 0x7f010274

.field public static final audioRecordSpinnerLeftDrawable:I = 0x7f010277

.field public static final audioRecordSpinnerRightDrawable:I = 0x7f010278

.field public static final audioRecordTimerBgLeftNormal:I = 0x7f010285

.field public static final audioRecordTimerBgLeftSelected:I = 0x7f010286

.field public static final audioRecordTimerBgRightNormal:I = 0x7f010287

.field public static final audioRecordTimerBgRightSelected:I = 0x7f010288

.field public static final audioRecordTimerTextColorLeftNormal:I = 0x7f010289

.field public static final audioRecordTimerTextColorLeftSelected:I = 0x7f01028a

.field public static final audioRecordTimerTextColorRightNormal:I = 0x7f01028b

.field public static final audioRecordTimerTextColorRightSelected:I = 0x7f01028c

.field public static final audioRecordVolume:I = 0x7f01026f

.field public static final auto_start:I = 0x7f010079

.field public static final auxViewPadding:I = 0x7f010119

.field public static final backView:I = 0x7f0102ec

.field public static final background:I = 0x7f0100c9

.field public static final backgroundSplit:I = 0x7f0100cb

.field public static final backgroundStacked:I = 0x7f0100ca

.field public static final badgeBackground:I = 0x7f0102db

.field public static final badgePadding:I = 0x7f0102da

.field public static final badgePlacement:I = 0x7f0102dc

.field public static final badgeText:I = 0x7f0102d8

.field public static final badgeTextAppearance:I = 0x7f0102d9

.field public static final badgeTextColor:I = 0x7f010032

.field public static final badgeTextViewStyle:I = 0x7f0102d7

.field public static final badge_layout:I = 0x7f0102d4

.field public static final bannerNotificationViewNeue:I = 0x7f010265

.field public static final barColor:I = 0x7f010174

.field public static final base_alpha:I = 0x7f01007a

.field public static final bigImageLocation:I = 0x7f01006b

.field public static final bigImageWidthPercent:I = 0x7f01006a

.field public static final bitmapSrc:I = 0x7f010312

.field public static final bodyBackground:I = 0x7f010167

.field public static final bodyText:I = 0x7f010168

.field public static final bodyTextColor:I = 0x7f01017a

.field public static final border:I = 0x7f01011b

.field public static final borderBottom:I = 0x7f01011d

.field public static final borderColor:I = 0x7f01011a

.field public static final borderLeft:I = 0x7f01011e

.field public static final borderRight:I = 0x7f01011f

.field public static final borderTop:I = 0x7f01011c

.field public static final bottomText:I = 0x7f010304

.field public static final bottom_divider:I = 0x7f01019d

.field public static final buttonBackgroundStyle:I = 0x7f0102f4

.field public static final buttonBarButtonStyle:I = 0x7f0100b5

.field public static final buttonBarStyle:I = 0x7f0100b4

.field public static final buttonCenterBackground:I = 0x7f0102f8

.field public static final buttonLeftBackground:I = 0x7f0102f7

.field public static final buttonOrientation:I = 0x7f0102f1

.field public static final buttonRightBackground:I = 0x7f0102f9

.field public static final buttonText:I = 0x7f01019b

.field public static final buttonWholeBackground:I = 0x7f0102fa

.field public static final buyButtonAppearance:I = 0x7f0102c4

.field public static final buyButtonHeight:I = 0x7f0102c1

.field public static final buyButtonText:I = 0x7f0102c3

.field public static final buyButtonWidth:I = 0x7f0102c2

.field public static final cameraBearing:I = 0x7f0102b0

.field public static final cameraTargetLat:I = 0x7f0102b1

.field public static final cameraTargetLng:I = 0x7f0102b2

.field public static final cameraTilt:I = 0x7f0102b3

.field public static final cameraZoom:I = 0x7f0102b4

.field public static final canCollapse:I = 0x7f010300

.field public static final centerTitle:I = 0x7f01005e

.field public static final centered:I = 0x7f010002

.field public static final chatHeadTextBubbleLeftOriginMaskStyle:I = 0x7f01020f

.field public static final chatHeadTextBubbleLeftOriginTextStyle:I = 0x7f010210

.field public static final chatHeadTextBubbleRightOriginMaskStyle:I = 0x7f010211

.field public static final chatHeadTextBubbleRightOriginTextStyle:I = 0x7f010212

.field public static final chatStyle:I = 0x7f010188

.field public static final checkMarkPosition:I = 0x7f010131

.field public static final checkOffColour:I = 0x7f0102e7

.field public static final checkOnColour:I = 0x7f0102e6

.field public static final checked:I = 0x7f0102fc

.field public static final checkedContentDescription:I = 0x7f01002e

.field public static final checkedContentViewStyle:I = 0x7f010130

.field public static final checkedCountBadge:I = 0x7f010031

.field public static final checkedImage:I = 0x7f01002c

.field public static final childMargin:I = 0x7f010061

.field public static final circleFaces:I = 0x7f010186

.field public static final circleStrokeWidth:I = 0x7f0102d5

.field public static final clearTextDrawable:I = 0x7f01004b

.field public static final clipBorderToPadding:I = 0x7f010120

.field public static final collapsedStateMaxLines:I = 0x7f010301

.field public static final colour_radius:I = 0x7f01029c

.field public static final columnCount:I = 0x7f010333

.field public static final columnOrderPreserved:I = 0x7f010337

.field public static final columnWidth:I = 0x7f010098

.field public static final columnWidthCompat:I = 0x7f010159

.field public static final composeDrawerLeftButtonStyle:I = 0x7f01023a

.field public static final composeDrawerRightButtonStyle:I = 0x7f01023b

.field public static final contactMultipickerContainerStyle:I = 0x7f0101c5

.field public static final contactMultipickerCover:I = 0x7f0101c7

.field public static final contactMultipickerListContainerStyle:I = 0x7f0101c6

.field public static final contactPickerAutocompleteContainerStyle:I = 0x7f0101c8

.field public static final contactPickerCheckboxStyle:I = 0x7f0101ac

.field public static final contactPickerEmptyListItemStyle:I = 0x7f0101bc

.field public static final contactPickerFragmentTheme:I = 0x7f0101a6

.field public static final contactPickerFriendsListMaskStyle:I = 0x7f0101c2

.field public static final contactPickerHeadingText:I = 0x7f0101c9

.field public static final contactPickerItemDividerStyle:I = 0x7f0101b6

.field public static final contactPickerItemMenuAnchorStyle:I = 0x7f0101b4

.field public static final contactPickerItemNameStyle:I = 0x7f0101b0

.field public static final contactPickerItemPresenceIndicatorStyle:I = 0x7f0101b3

.field public static final contactPickerItemStatusStyle:I = 0x7f0101b2

.field public static final contactPickerItemStyle:I = 0x7f0101b5

.field public static final contactPickerItemTextContainerStyle:I = 0x7f0101af

.field public static final contactPickerListStyle:I = 0x7f0101aa

.field public static final contactPickerListTopShadowExtent:I = 0x7f0101bf

.field public static final contactPickerListTopShadowStyle:I = 0x7f0101be

.field public static final contactPickerMembersDividerStyle:I = 0x7f0101ca

.field public static final contactPickerOnFacebookRes:I = 0x7f0101c4

.field public static final contactPickerPushableOnMessengerRes:I = 0x7f0101c3

.field public static final contactPickerRowHeight:I = 0x7f0101c0

.field public static final contactPickerSearchDividerStyle:I = 0x7f0101ab

.field public static final contactPickerSearchIcon:I = 0x7f0101c1

.field public static final contactPickerSecondaryCheckboxStyle:I = 0x7f0101ad

.field public static final contactPickerSectionHeaderButtonStyle:I = 0x7f0101b9

.field public static final contactPickerSectionHeaderDividerStyle:I = 0x7f0101ba

.field public static final contactPickerSectionHeaderStyle:I = 0x7f0101b7

.field public static final contactPickerSectionHeaderTextStyle:I = 0x7f0101b8

.field public static final contactPickerSectionSplitterStyle:I = 0x7f0101bb

.field public static final contactPickerThreadTileViewStyle:I = 0x7f0101ae

.field public static final contactPickerTokenizedAutoCompleteTextViewStyle:I = 0x7f010267

.field public static final contactPickerViewMoreStyle:I = 0x7f0101bd

.field public static final contactPickerWarningStyle:I = 0x7f0101cb

.field public static final contentLayout:I = 0x7f010181

.field public static final contentViewStyle:I = 0x7f010123

.field public static final contentViewWithButtonStyle:I = 0x7f010129

.field public static final controller:I = 0x7f0102e3

.field public static final cornerRadius:I = 0x7f01013d

.field public static final createThreadCustomLayoutStyle:I = 0x7f010266

.field public static final customNavigationLayout:I = 0x7f0100cc

.field public static final debug:I = 0x7f010109

.field public static final defaultUserTileColor:I = 0x7f010016

.field public static final defaultUserTileSrc:I = 0x7f010015

.field public static final default_colour:I = 0x7f010298

.field public static final default_stroke_width:I = 0x7f010299

.field public static final detail_text:I = 0x7f01008e

.field public static final dialogBorderlessButtonStyle:I = 0x7f0102ab

.field public static final dialogButtonBarStyle:I = 0x7f0102aa

.field public static final dialogMessageStyle:I = 0x7f0102a9

.field public static final dialog_background_alpha:I = 0x7f0102cc

.field public static final dialog_padding:I = 0x7f0102cd

.field public static final disableChildrenWhenDisabled:I = 0x7f0100ee

.field public static final disableDragToRate:I = 0x7f0102a4

.field public static final disableScrollHideList:I = 0x7f010160

.field public static final dispatchAndroidTouchEvents:I = 0x7f010091

.field public static final displayOptions:I = 0x7f0100c4

.field public static final distanceBetweenBars:I = 0x7f010173

.field public static final divebarContainerStyle:I = 0x7f0101cd

.field public static final divebarFragmentTheme:I = 0x7f0101a4

.field public static final divebarSearchContainerStyle:I = 0x7f0101cf

.field public static final divebarSearchEditTextStyle:I = 0x7f0101d0

.field public static final divebarSearchHeaderStyle:I = 0x7f0101ce

.field public static final divider:I = 0x7f010060

.field public static final dividerColor:I = 0x7f0102ef

.field public static final dividerHorizontal:I = 0x7f0100b8

.field public static final dividerPadding:I = 0x7f0100f0

.field public static final dividerPaddingEnd:I = 0x7f010112

.field public static final dividerPaddingStart:I = 0x7f010111

.field public static final dividerPosition:I = 0x7f010187

.field public static final dividerSize:I = 0x7f0102ee

.field public static final dividerThickness:I = 0x7f010110

.field public static final dividerVertical:I = 0x7f0100b7

.field public static final downstateType:I = 0x7f0102a5

.field public static final dragDirections:I = 0x7f01010e

.field public static final dragndropBackground:I = 0x7f010046

.field public static final dragndropImageBackground:I = 0x7f010047

.field public static final drawable:I = 0x7f010056

.field public static final drawableOrientation:I = 0x7f010057

.field public static final dropDownListViewStyle:I = 0x7f0100be

.field public static final dropdownListPreferredItemHeight:I = 0x7f0100e2

.field public static final dropoff:I = 0x7f010080

.field public static final duration:I = 0x7f01007b

.field public static final editFavoriteDividerStyle:I = 0x7f0101d6

.field public static final editFavoriteListStyle:I = 0x7f0101d7

.field public static final editFavoriteSectionSplitterStyle:I = 0x7f0101d8

.field public static final editFavoritesActionBarVisibility:I = 0x7f0101d5

.field public static final editFavoritesContactPickerSearchBarStyle:I = 0x7f0101d4

.field public static final editFavoritesEmptyText:I = 0x7f0101b1

.field public static final editFavoritesItemAddButtonStyle:I = 0x7f0101d3

.field public static final editFavoritesItemDeleteButtonStyle:I = 0x7f0101d2

.field public static final editFavoritesItemDragStyle:I = 0x7f0101d1

.field public static final editFavoritesUserTileViewStyle:I = 0x7f0101d9

.field public static final ellipsizeLineBreaks:I = 0x7f010071

.field public static final emojiAttachmentsBackgroundColor:I = 0x7f010234

.field public static final emojiAttachmentsBackspaceButtonStyle:I = 0x7f010238

.field public static final emojiAttachmentsEmojiRowBottomPadding:I = 0x7f010239

.field public static final emojiAttachmentsPopupAttachmentsDividerStyle:I = 0x7f010236

.field public static final emojiAttachmentsPopupAttachmentsSectionLayout:I = 0x7f010235

.field public static final emojiAttachmentsPopupTheme:I = 0x7f010233

.field public static final emojiAttachmentsShowMoreButtonStyle:I = 0x7f010237

.field public static final emojiCategoryPageIndicatorStyle:I = 0x7f0101cc

.field public static final emptyCircleColour:I = 0x7f0102ea

.field public static final emptyMessage:I = 0x7f010042

.field public static final emptyStarDrawable:I = 0x7f01029f

.field public static final enablePtrMask:I = 0x7f010183

.field public static final enableTranscriptModeWorkaround:I = 0x7f010161

.field public static final environment:I = 0x7f0102be

.field public static final errorOrientation:I = 0x7f01017c

.field public static final errorPaddingBottom:I = 0x7f010180

.field public static final errorPaddingTop:I = 0x7f01017f

.field public static final expandActivityOverflowButtonDrawable:I = 0x7f010106

.field public static final expandable:I = 0x7f0102e4

.field public static final expandingBackground:I = 0x7f01028f

.field public static final faceSize:I = 0x7f010184

.field public static final facebookBadge:I = 0x7f01006d

.field public static final fadingGradientEndColor:I = 0x7f0102fe

.field public static final fadingGradientLength:I = 0x7f0102fd

.field public static final fastScrollOverlayPosition:I = 0x7f01014f

.field public static final fastScrollPreviewBackgroundLeft:I = 0x7f01014d

.field public static final fastScrollPreviewBackgroundRight:I = 0x7f01014e

.field public static final fastScrollTextColor:I = 0x7f01014a

.field public static final fastScrollThumbDrawable:I = 0x7f01014b

.field public static final fastScrollTrackDrawable:I = 0x7f01014c

.field public static final fillColor:I = 0x7f010006

.field public static final fixed_height:I = 0x7f010082

.field public static final fixed_width:I = 0x7f010081

.field public static final fontColor:I = 0x7f010307

.field public static final fontFamily:I = 0x7f010049

.field public static final fontSize:I = 0x7f010305

.field public static final fontStyle:I = 0x7f010306

.field public static final fontWeight:I = 0x7f01004a

.field public static final for_me_user:I = 0x7f01019e

.field public static final foreground:I = 0x7f010029

.field public static final foregroundColor:I = 0x7f010065

.field public static final foregroundDrawable:I = 0x7f010141

.field public static final fragmentMode:I = 0x7f0102c0

.field public static final fragmentStyle:I = 0x7f0102bf

.field public static final friendPhoneBadge:I = 0x7f010070

.field public static final fromRight:I = 0x7f010313

.field public static final frontView:I = 0x7f0102eb

.field public static final fullStarDrawable:I = 0x7f01029d

.field public static final grabberId:I = 0x7f010044

.field public static final guestTileSize:I = 0x7f01031a

.field public static final guestTitleSpacing:I = 0x7f01031b

.field public static final halfStarDrawable:I = 0x7f01029e

.field public static final hasBackButton:I = 0x7f01005c

.field public static final hasCheckBox:I = 0x7f0102fb

.field public static final hasJewel:I = 0x7f010318

.field public static final hasLauncherButton:I = 0x7f010319

.field public static final hasProgressBar:I = 0x7f01005b

.field public static final headerHideThreshold:I = 0x7f01015f

.field public static final headerStartHeight:I = 0x7f01015e

.field public static final height:I = 0x7f0100c2

.field public static final highlightColor:I = 0x7f010302

.field public static final highlightStyle:I = 0x7f010303

.field public static final hintText:I = 0x7f01018e

.field public static final homeAsUpIndicator:I = 0x7f0100af

.field public static final homeLayout:I = 0x7f0100cd

.field public static final horizontalSpacing:I = 0x7f010066

.field public static final horizontalStarPadding:I = 0x7f0102a3

.field public static final icon:I = 0x7f0100c7

.field public static final iconAlignment:I = 0x7f01018a

.field public static final iconSrc:I = 0x7f01018c

.field public static final iconifiedByDefault:I = 0x7f0100f4

.field public static final imageBlockLayoutStyle:I = 0x7f010113

.field public static final imageHeight:I = 0x7f01017e

.field public static final imageSize:I = 0x7f01017b

.field public static final imageSrc:I = 0x7f01010a

.field public static final imageWidth:I = 0x7f01017d

.field public static final inactiveSrc:I = 0x7f01015b

.field public static final inactiveStarDrawable:I = 0x7f0102a1

.field public static final inactive_src:I = 0x7f0102d3

.field public static final includeFontPadding:I = 0x7f010191

.field public static final indeterminateProgressStyle:I = 0x7f0100cf

.field public static final indicator_active_color:I = 0x7f010150

.field public static final indicator_inactive_color:I = 0x7f010151

.field public static final inflatedLayoutAndroidId:I = 0x7f010058

.field public static final initialActivityCount:I = 0x7f010105

.field public static final initializeHeightToFirstItem:I = 0x7f010153

.field public static final inlineActionBarOverflowButtonStyle:I = 0x7f0102f6

.field public static final innerDividerDrawable:I = 0x7f010317

.field public static final intensity:I = 0x7f010083

.field public static final isBottomLeftRounded:I = 0x7f01013c

.field public static final isBottomRightRounded:I = 0x7f01013b

.field public static final isLightTheme:I = 0x7f0100f3

.field public static final isShownInGallery:I = 0x7f010037

.field public static final isSwipingEnabled:I = 0x7f010152

.field public static final isTopLeftRounded:I = 0x7f010139

.field public static final isTopRightRounded:I = 0x7f01013a

.field public static final isUsedWithUploadProgress:I = 0x7f01003a

.field public static final itemPadding:I = 0x7f0100d1

.field public static final itemTitle:I = 0x7f01018d

.field public static final keepMarkerAtCenter:I = 0x7f010164

.field public static final label:I = 0x7f010178

.field public static final labelTextColor:I = 0x7f010179

.field public static final layout_anchorPosition:I = 0x7f010143

.field public static final layout_anchoredTo:I = 0x7f010145

.field public static final layout_column:I = 0x7f01033a

.field public static final layout_columnSpan:I = 0x7f01033b

.field public static final layout_gravity:I = 0x7f01033c

.field public static final layout_isOptional:I = 0x7f010156

.field public static final layout_isOverlay:I = 0x7f010144

.field public static final layout_overlapWithPrevious:I = 0x7f010157

.field public static final layout_position:I = 0x7f010291

.field public static final layout_row:I = 0x7f010338

.field public static final layout_rowSpan:I = 0x7f010339

.field public static final layout_useAsAuxView:I = 0x7f010122

.field public static final layout_useAsThumbnail:I = 0x7f010121

.field public static final layout_useViewAs:I = 0x7f010128

.field public static final layout_xOffset:I = 0x7f010146

.field public static final layout_yOffset:I = 0x7f010147

.field public static final left_item_width_percentage:I = 0x7f010148

.field public static final listChoiceBackgroundIndicator:I = 0x7f0100e6

.field public static final listHideThreshold:I = 0x7f01015d

.field public static final listPopupWindowStyle:I = 0x7f0100bf

.field public static final listPreferredItemHeight:I = 0x7f0100b9

.field public static final listPreferredItemHeightLarge:I = 0x7f0100bb

.field public static final listPreferredItemHeightSmall:I = 0x7f0100ba

.field public static final listPreferredItemPaddingLeft:I = 0x7f0100bc

.field public static final listPreferredItemPaddingRight:I = 0x7f0100bd

.field public static final listStartHeight:I = 0x7f01015c

.field public static final locationDisabledNuxLayout:I = 0x7f010221

.field public static final locationDisabledNuxText:I = 0x7f010222

.field public static final locationNuxLayout:I = 0x7f010220

.field public static final locationNuxViewTheme:I = 0x7f01021f

.field public static final locationPreferencesLocationIconDrawable:I = 0x7f010264

.field public static final logo:I = 0x7f0100c8

.field public static final longpressSelectorColor:I = 0x7f010094

.field public static final mapType:I = 0x7f0102af

.field public static final markerColor:I = 0x7f010163

.field public static final mask:I = 0x7f010028

.field public static final maskedWalletDetailsBackground:I = 0x7f0102c7

.field public static final maskedWalletDetailsButtonBackground:I = 0x7f0102c9

.field public static final maskedWalletDetailsButtonTextAppearance:I = 0x7f0102c8

.field public static final maskedWalletDetailsHeaderTextAppearance:I = 0x7f0102c6

.field public static final maskedWalletDetailsLogoImageType:I = 0x7f0102cb

.field public static final maskedWalletDetailsLogoTextColor:I = 0x7f0102ca

.field public static final maskedWalletDetailsTextAppearance:I = 0x7f0102c5

.field public static final maxCheckSize:I = 0x7f0102e5

.field public static final maxLines:I = 0x7f010008

.field public static final maxMembersToDisplay:I = 0x7f01033f

.field public static final maxNumOfVisibleButtons:I = 0x7f0102f3

.field public static final maxScaledTextSize:I = 0x7f01000e

.field public static final maxWidth:I = 0x7f01018f

.field public static final max_stroke_width:I = 0x7f01029b

.field public static final maximallyWideThreshold:I = 0x7f01004f

.field public static final maximumWidth:I = 0x7f010068

.field public static final mediaChoiceDialogTheme:I = 0x7f0101a9

.field public static final megaphoneImage:I = 0x7f0102d1

.field public static final megaphoneStyle:I = 0x7f0102ce

.field public static final menu:I = 0x7f0102f0

.field public static final message:I = 0x7f010062

.field public static final messageComposerAttachmentsButtonBackground:I = 0x7f01020a

.field public static final messageComposerAttachmentsButtonBackgroundTop:I = 0x7f01020b

.field public static final messageComposerAttachmentsButtonDrawable:I = 0x7f010209

.field public static final messageComposerAudioComposerBackground:I = 0x7f01021e

.field public static final messageComposerBackground:I = 0x7f010214

.field public static final messageComposerDividerBackground:I = 0x7f01021d

.field public static final messageComposerDividerHeight:I = 0x7f01021c

.field public static final messageComposerDividerStyle:I = 0x7f01021a

.field public static final messageComposerEditContainerNoSendButtonRightPadding:I = 0x7f010219

.field public static final messageComposerEditContainerWithSendButtonRightPadding:I = 0x7f010218

.field public static final messageComposerLocationButtonStyle:I = 0x7f01021b

.field public static final messageComposerSendButton:I = 0x7f010217

.field public static final messageComposerStickersButtonBackground:I = 0x7f01020d

.field public static final messageComposerStickersButtonBackgroundTop:I = 0x7f01020e

.field public static final messageComposerStickersButtonDrawable:I = 0x7f01020c

.field public static final messageComposerTheme:I = 0x7f010213

.field public static final messageDividerBarStyle:I = 0x7f010263

.field public static final messageDividerStyle:I = 0x7f010261

.field public static final messageDividerViewStyle:I = 0x7f010262

.field public static final messageItemBottomSpacerHeight:I = 0x7f010249

.field public static final messageItemViewContainerMeUserRightMargin:I = 0x7f01025e

.field public static final messageItemViewContainerMeUserStyle:I = 0x7f010240

.field public static final messageItemViewFontSize:I = 0x7f01024d

.field public static final messageItemViewLeftMargin:I = 0x7f01023c

.field public static final messageItemViewLeftTextMargin:I = 0x7f01023e

.field public static final messageItemViewMarginTopGrouped:I = 0x7f010248

.field public static final messageItemViewMarginTopUngrouped:I = 0x7f010247

.field public static final messageItemViewMeUserStyle:I = 0x7f010242

.field public static final messageItemViewNameTextGroupedVisibility:I = 0x7f010246

.field public static final messageItemViewOtherUserStyle:I = 0x7f010241

.field public static final messageItemViewRightMargin:I = 0x7f01023d

.field public static final messageItemViewRightTextMargin:I = 0x7f01023f

.field public static final messageItemViewRowReceiptLayout:I = 0x7f01025a

.field public static final messageItemViewRowReceiptMarginLeft:I = 0x7f01025c

.field public static final messageItemViewRowReceiptMarginRight:I = 0x7f01025d

.field public static final messageItemViewRowReceiptTextViewStyle:I = 0x7f01025b

.field public static final messageItemViewTileGroupedVisibility:I = 0x7f010245

.field public static final messageItemViewTypingDotAnimationVisibility:I = 0x7f01024c

.field public static final messageItemViewTypingItemStyle:I = 0x7f010244

.field public static final messageItemViewTypingTextVisibility:I = 0x7f01024b

.field public static final messageItemViewUserTileStyle:I = 0x7f010243

.field public static final messageReadReceiptDrawable:I = 0x7f01025f

.field public static final messageReplyDrawable:I = 0x7f010260

.field public static final messageTwoLineComposerBackgroundWithBorder:I = 0x7f010215

.field public static final messageTwoLineComposerBackgroundWithoutBorder:I = 0x7f010216

.field public static final messengerBadge:I = 0x7f01006e

.field public static final metaText:I = 0x7f01010d

.field public static final metaTextAppearance:I = 0x7f010126

.field public static final minContentHeight:I = 0x7f01016c

.field public static final minHeight:I = 0x7f010190

.field public static final minLines:I = 0x7f010009

.field public static final minNumColumns:I = 0x7f010158

.field public static final minScaledTextSize:I = 0x7f01000d

.field public static final min_stroke_width:I = 0x7f01029a

.field public static final minimallyWide:I = 0x7f01004e

.field public static final mode_vertical:I = 0x7f010314

.field public static final nameOption:I = 0x7f010171

.field public static final navigationMode:I = 0x7f0100c3

.field public static final navigation_button_view_theme:I = 0x7f01008a

.field public static final navless:I = 0x7f01005f

.field public static final negativeButtonTitle:I = 0x7f010064

.field public static final normalHeight:I = 0x7f010043

.field public static final nubAlign:I = 0x7f01016a

.field public static final nubMargin:I = 0x7f01016b

.field public static final nubPosition:I = 0x7f010169

.field public static final numColumns:I = 0x7f010099

.field public static final numStars:I = 0x7f0102a2

.field public static final numberOfBars:I = 0x7f010175

.field public static final orientation:I = 0x7f010331

.field public static final outerDividerDrawableBottom:I = 0x7f010316

.field public static final outerDividerDrawableTop:I = 0x7f010315

.field public static final overflowAndListOverlap:I = 0x7f010048

.field public static final overflowStyle:I = 0x7f0102f2

.field public static final overlayDivider:I = 0x7f01006c

.field public static final overlayDrawable:I = 0x7f010115

.field public static final overrideZeroRating:I = 0x7f010166

.field public static final paddingBetweenFaces:I = 0x7f010185

.field public static final paddingBetweenPogs:I = 0x7f010340

.field public static final paddingEnd:I = 0x7f0100d3

.field public static final paddingStart:I = 0x7f0100d2

.field public static final pageColor:I = 0x7f0102e0

.field public static final pageIdentityCardStyle:I = 0x7f01030d

.field public static final pageIdentityFeedStoryCardStyle:I = 0x7f01030e

.field public static final panelMenuListTheme:I = 0x7f0100e5

.field public static final panelMenuListWidth:I = 0x7f0100e4

.field public static final peopleTabContactMenu:I = 0x7f010268

.field public static final phoneBadge:I = 0x7f01006f

.field public static final pickMediaButton:I = 0x7f01028e

.field public static final pickMediaLayout:I = 0x7f01028d

.field public static final pickerSearchBoxDefaultHeight:I = 0x7f010170

.field public static final placeHolderScaleType:I = 0x7f010040

.field public static final placeholderSrc:I = 0x7f010035

.field public static final playButtonLayout:I = 0x7f010177

.field public static final popupMenuStyle:I = 0x7f0100e3

.field public static final popupPromptView:I = 0x7f0100ed

.field public static final positiveButtonTitle:I = 0x7f010063

.field public static final presenceIndicatorIconStyle:I = 0x7f0101da

.field public static final presenceIndicatorTextStyle:I = 0x7f0101db

.field public static final pressedCheckOffColour:I = 0x7f0102e9

.field public static final pressedCheckOnColour:I = 0x7f0102e8

.field public static final pressedOverlayColor:I = 0x7f01003f

.field public static final pressedView:I = 0x7f0102ed

.field public static final primaryButtonText:I = 0x7f0102cf

.field public static final primary_button_text:I = 0x7f010089

.field public static final primary_text:I = 0x7f01008d

.field public static final progressBarPadding:I = 0x7f0100d0

.field public static final progressBarStyle:I = 0x7f0100ce

.field public static final progressBarStyleSmallInverse:I = 0x7f010012

.field public static final progressCircleBaseAlpha:I = 0x7f010072

.field public static final progressCircleBaseColor:I = 0x7f010073

.field public static final progressCircleDrawnAlpha:I = 0x7f010074

.field public static final progressCircleDrawnColor:I = 0x7f010075

.field public static final progressCircleEnableFadeIn:I = 0x7f010076

.field public static final progressCircleStrokeWidth:I = 0x7f010077

.field public static final progressCircleWidth:I = 0x7f010078

.field public static final prompt:I = 0x7f0100eb

.field public static final queryHint:I = 0x7f0100f5

.field public static final radius:I = 0x7f0102e1

.field public static final receiptItemTopMargin:I = 0x7f01024a

.field public static final reflexAndroidTouchMode:I = 0x7f010092

.field public static final reflexAware:I = 0x7f010090

.field public static final reflexListviewOverscrollEnabled:I = 0x7f010095

.field public static final reflexViewPagerOverscrollEnabled:I = 0x7f01009b

.field public static final refreshDirection:I = 0x7f010033

.field public static final refreshableViewItemLayout:I = 0x7f010182

.field public static final relative_height:I = 0x7f010085

.field public static final relative_width:I = 0x7f010084

.field public static final repeat_count:I = 0x7f01007c

.field public static final repeat_delay:I = 0x7f01007d

.field public static final repeat_mode:I = 0x7f01007e

.field public static final retainImageDuringUpdate:I = 0x7f01003c

.field public static final retainMapDuringUpdate:I = 0x7f010165

.field public static final roundBorderColor:I = 0x7f010140

.field public static final roundBorderWidth:I = 0x7f01013f

.field public static final roundByOverlayingColor:I = 0x7f01013e

.field public static final rowCount:I = 0x7f010332

.field public static final rowOrderPreserved:I = 0x7f010336

.field public static final scaleType:I = 0x7f010041

.field public static final scaling:I = 0x7f010142

.field public static final searchDropdownBackground:I = 0x7f0100f7

.field public static final searchResultListItemHeight:I = 0x7f010100

.field public static final searchViewAutoCompleteTextView:I = 0x7f010104

.field public static final searchViewCloseIcon:I = 0x7f0100f8

.field public static final searchViewEditQuery:I = 0x7f0100fc

.field public static final searchViewEditQueryBackground:I = 0x7f0100fd

.field public static final searchViewGoIcon:I = 0x7f0100f9

.field public static final searchViewHintMinFontSize:I = 0x7f0100f6

.field public static final searchViewSearchIcon:I = 0x7f0100fa

.field public static final searchViewTextField:I = 0x7f0100fe

.field public static final searchViewTextFieldRight:I = 0x7f0100ff

.field public static final searchViewVoiceIcon:I = 0x7f0100fb

.field public static final secondaryButtonText:I = 0x7f0102d0

.field public static final secondary_button_text:I = 0x7f010088

.field public static final selectableItemBackground:I = 0x7f0100b6

.field public static final selectedColor:I = 0x7f010003

.field public static final selectorColor:I = 0x7f010093

.field public static final shadowColor:I = 0x7f010195

.field public static final shadowDx:I = 0x7f010192

.field public static final shadowDy:I = 0x7f010193

.field public static final shadowRadius:I = 0x7f010194

.field public static final shape:I = 0x7f010086

.field public static final shouldBounce:I = 0x7f01002f

.field public static final shouldShowLoadingAnimation:I = 0x7f01003d

.field public static final showAddMemberPog:I = 0x7f01033d

.field public static final showAsAction:I = 0x7f0100e7

.field public static final showChevronOnRight:I = 0x7f010137

.field public static final showDividers:I = 0x7f0100ef

.field public static final showMemberCountPog:I = 0x7f01033e

.field public static final showProgressBar:I = 0x7f010036

.field public static final show_back_chevron:I = 0x7f01008f

.field public static final simpleImageView:I = 0x7f0102a7

.field public static final snap:I = 0x7f0102e2

.field public static final spaceSavingThreshold:I = 0x7f010155

.field public static final spacingHorizontal:I = 0x7f010096

.field public static final spacingKS:I = 0x7f010310

.field public static final spacingVertical:I = 0x7f010097

.field public static final spinnerDropDownItemStyle:I = 0x7f0100f2

.field public static final spinnerMode:I = 0x7f0100ec

.field public static final spinnerStyle:I = 0x7f0100f1

.field public static final splitBarBackground:I = 0x7f0100a0

.field public static final splitBarSrc:I = 0x7f01009f

.field public static final src:I = 0x7f01018b

.field public static final state_deleting:I = 0x7f010189

.field public static final stickerPopupBackgroundColor:I = 0x7f010224

.field public static final stickerPopupPagerFillColor:I = 0x7f010228

.field public static final stickerPopupPagerPageColor:I = 0x7f010227

.field public static final stickerPopupPlaceholderDrawable:I = 0x7f01022b

.field public static final stickerPopupTabDividerColor:I = 0x7f010225

.field public static final stickerPopupTheme:I = 0x7f010223

.field public static final stickerStoreItemBottomDiv:I = 0x7f010293

.field public static final stickerStoreItemBottomSlot:I = 0x7f010295

.field public static final stickerStoreItemDividerStyle:I = 0x7f0101dc

.field public static final stickerStoreItemDragStyle:I = 0x7f0101dd

.field public static final stickerStoreItemTopDiv:I = 0x7f010292

.field public static final stickerStoreItemTopSlot:I = 0x7f010294

.field public static final stickerStorePackDeleteIconDrawable:I = 0x7f01022d

.field public static final stickerStorePackDownloadButtonDrawable:I = 0x7f010231

.field public static final stickerStorePackDownloadIconDrawable:I = 0x7f01022e

.field public static final stickerStorePackDownloadedIconDrawable:I = 0x7f01022f

.field public static final stickerStorePackReorderGrabberDrawable:I = 0x7f010230

.field public static final stickerStorePlaceholderDrawable:I = 0x7f010232

.field public static final stickerStoreTabBackgroundDrawable:I = 0x7f010229

.field public static final stickerStoreTabTextColor:I = 0x7f010226

.field public static final stickerStoreTheme:I = 0x7f01022c

.field public static final stickerTabPromotedIcon:I = 0x7f01022a

.field public static final stretchMode:I = 0x7f01009a

.field public static final strokeColor:I = 0x7f010007

.field public static final strokeWidth:I = 0x7f010004

.field public static final stroke_colour:I = 0x7f010297

.field public static final stroke_width:I = 0x7f010296

.field public static final subtitle:I = 0x7f01005a

.field public static final subtitleText:I = 0x7f01010c

.field public static final subtitleTextAppearance:I = 0x7f010125

.field public static final subtitleTextStyle:I = 0x7f0100c6

.field public static final suggestionText:I = 0x7f01019a

.field public static final support_vertical_scrolling:I = 0x7f010149

.field public static final suppressBackground:I = 0x7f010172

.field public static final swipeAxis:I = 0x7f01010f

.field public static final switchCompatStyle:I = 0x7f0102d6

.field public static final switchMinHeight:I = 0x7f010022

.field public static final switchMinWidth:I = 0x7f010021

.field public static final switchPadding:I = 0x7f010023

.field public static final switchTextAllCaps:I = 0x7f01001e

.field public static final switchTextAppearance:I = 0x7f010020

.field public static final switchTextColor:I = 0x7f010024

.field public static final switchTextSize:I = 0x7f010025

.field public static final switchTextStyle:I = 0x7f010026

.field public static final switchTypeface:I = 0x7f010027

.field public static final tabLayout:I = 0x7f010135

.field public static final tabStripEnabled:I = 0x7f010196

.field public static final tabStripLeft:I = 0x7f010197

.field public static final tabStripRight:I = 0x7f010198

.field public static final tabbedViewPagerIndicatorStyle:I = 0x7f010132

.field public static final text:I = 0x7f010010

.field public static final textAllCaps:I = 0x7f010108

.field public static final textAppearance:I = 0x7f0102f5

.field public static final textAppearanceLargePopupMenu:I = 0x7f0100b0

.field public static final textAppearanceListItem:I = 0x7f0100c0

.field public static final textAppearanceListItemSmall:I = 0x7f0100c1

.field public static final textAppearanceSearchResultSubtitle:I = 0x7f010102

.field public static final textAppearanceSearchResultTitle:I = 0x7f010101

.field public static final textAppearanceSmallPopupMenu:I = 0x7f0100b1

.field public static final textColor:I = 0x7f010001

.field public static final textColorOff:I = 0x7f01001d

.field public static final textColorOn:I = 0x7f01001c

.field public static final textColorSearchUrl:I = 0x7f010103

.field public static final textGravity:I = 0x7f010013

.field public static final textLabelGravity:I = 0x7f01030a

.field public static final textMaxLines:I = 0x7f010309

.field public static final textOff:I = 0x7f01001b

.field public static final textOn:I = 0x7f01001a

.field public static final textSize:I = 0x7f010011

.field public static final textStyle:I = 0x7f01000b

.field public static final textTopPadding:I = 0x7f010308

.field public static final textWrappingDrawableBottom:I = 0x7f010054

.field public static final textWrappingDrawableLeft:I = 0x7f010055

.field public static final textWrappingDrawablePadding:I = 0x7f010051

.field public static final textWrappingDrawableRight:I = 0x7f010053

.field public static final textWrappingDrawableTop:I = 0x7f010052

.field public static final theme:I = 0x7f0102bd

.field public static final themed_layout:I = 0x7f0102a6

.field public static final threadListBackground:I = 0x7f0101e0

.field public static final threadListErrorDrawable:I = 0x7f0101fb

.field public static final threadListFragmentTheme:I = 0x7f0101a7

.field public static final threadListHarrison:I = 0x7f0101e1

.field public static final threadListItemDividerHeight:I = 0x7f0101f9

.field public static final threadListItemDividerStyle:I = 0x7f0101f8

.field public static final threadListItemLastMessageLayout:I = 0x7f0101fa

.field public static final threadListItemMobilePresenceDrawable:I = 0x7f0101e7

.field public static final threadListItemNamePresenceStyle:I = 0x7f0101e5

.field public static final threadListItemNameReadColor:I = 0x7f0101ee

.field public static final threadListItemNameReadTypefaceStyle:I = 0x7f0101f6

.field public static final threadListItemNameStyle:I = 0x7f0101ed

.field public static final threadListItemNameUnreadColor:I = 0x7f0101ef

.field public static final threadListItemNameUnreadTypefaceStyle:I = 0x7f0101f7

.field public static final threadListItemOnlinePresenceDrawable:I = 0x7f0101e6

.field public static final threadListItemOverlayDivider:I = 0x7f0101e4

.field public static final threadListItemReadBackground:I = 0x7f0101e8

.field public static final threadListItemSnippetReadColor:I = 0x7f0101f0

.field public static final threadListItemSnippetStyle:I = 0x7f0101ec

.field public static final threadListItemSnippetUnreadColor:I = 0x7f0101f1

.field public static final threadListItemStyle:I = 0x7f0101e2

.field public static final threadListItemThreadHeight:I = 0x7f0101ea

.field public static final threadListItemThreadTileStyle:I = 0x7f0101e3

.field public static final threadListItemTimeReadColor:I = 0x7f0101f2

.field public static final threadListItemTimeReadFontFamily:I = 0x7f0101f4

.field public static final threadListItemTimeStyle:I = 0x7f0101eb

.field public static final threadListItemTimeUnreadColor:I = 0x7f0101f3

.field public static final threadListItemTimeUnreadFontFamily:I = 0x7f0101f5

.field public static final threadListItemUnreadBackground:I = 0x7f0101e9

.field public static final threadListReflexLongpressSelectorColor:I = 0x7f0101df

.field public static final threadListReflexSelectorColor:I = 0x7f0101de

.field public static final threadListSendInProgressIndeterminateDrawable:I = 0x7f0101fc

.field public static final threadSettingsFragmentTheme:I = 0x7f0101a5

.field public static final threadTileSize:I = 0x7f010069

.field public static final threadTitleLeftPadding:I = 0x7f010200

.field public static final threadTitleNameStyle:I = 0x7f0101fe

.field public static final threadTitleRightPadding:I = 0x7f010201

.field public static final threadTitleStatusStyle:I = 0x7f0101ff

.field public static final threadTitleTheme:I = 0x7f0101fd

.field public static final threadViewBackground:I = 0x7f010202

.field public static final threadViewBottomShadowColor:I = 0x7f010206

.field public static final threadViewBottomShadowVisibility:I = 0x7f010207

.field public static final threadViewFragmentTheme:I = 0x7f0101a8

.field public static final threadViewInfoIcon:I = 0x7f010208

.field public static final threadViewMessageListColor:I = 0x7f010203

.field public static final threadViewTopShadowColor:I = 0x7f010204

.field public static final threadViewTopShadowVisibility:I = 0x7f010205

.field public static final thumb:I = 0x7f010017

.field public static final thumbTextPadding:I = 0x7f01001f

.field public static final thumbnailDrawable:I = 0x7f010114

.field public static final thumbnailHeight:I = 0x7f010117

.field public static final thumbnailPadding:I = 0x7f010118

.field public static final thumbnailSize:I = 0x7f010127

.field public static final thumbnailWidth:I = 0x7f010116

.field public static final tileSize:I = 0x7f010014

.field public static final tilt:I = 0x7f010087

.field public static final title:I = 0x7f010059

.field public static final titleText:I = 0x7f01010b

.field public static final titleTextAppearance:I = 0x7f010124

.field public static final titleTextStyle:I = 0x7f0100c5

.field public static final title_text:I = 0x7f01008b

.field public static final tokenBackgroundDrawable:I = 0x7f01016f

.field public static final tokenTextColor:I = 0x7f01016d

.field public static final tokenTextSize:I = 0x7f01016e

.field public static final top_divider:I = 0x7f01019c

.field public static final traceAs:I = 0x7f010136

.field public static final trackOff:I = 0x7f010019

.field public static final trackOn:I = 0x7f010018

.field public static final track_color:I = 0x7f0101a2

.field public static final track_indicator_color:I = 0x7f0101a3

.field public static final track_indicator_width:I = 0x7f0101a1

.field public static final track_left_padding:I = 0x7f01019f

.field public static final track_right_padding:I = 0x7f0101a0

.field public static final transferBackground:I = 0x7f01030c

.field public static final transferPadding:I = 0x7f01030b

.field public static final typeface:I = 0x7f01000a

.field public static final uiCompass:I = 0x7f0102b5

.field public static final uiRotateGestures:I = 0x7f0102b6

.field public static final uiScrollGestures:I = 0x7f0102b7

.field public static final uiTiltGestures:I = 0x7f0102b8

.field public static final uiZoomControls:I = 0x7f0102b9

.field public static final uiZoomGestures:I = 0x7f0102ba

.field public static final uncheckedContentDescription:I = 0x7f01002d

.field public static final uncheckedCountBadge:I = 0x7f010030

.field public static final uncheckedImage:I = 0x7f01002b

.field public static final underlineColor:I = 0x7f010133

.field public static final underlineHeight:I = 0x7f010134

.field public static final unselectedAlphaKS:I = 0x7f010311

.field public static final unselectedColor:I = 0x7f010005

.field public static final url:I = 0x7f010034

.field public static final useActionBar:I = 0x7f01005d

.field public static final useCoverView:I = 0x7f01003b

.field public static final useDefaultMargins:I = 0x7f010334

.field public static final useFade:I = 0x7f010290

.field public static final useQuickContactBadge:I = 0x7f010039

.field public static final useViewLifecycle:I = 0x7f0102bb

.field public static final useZoomableImageView:I = 0x7f010038

.field public static final usesFboToMask:I = 0x7f01002a

.field public static final verticalSpacing:I = 0x7f010067

.field public static final viewToHideWhileDragging:I = 0x7f010045

.field public static final vpiCirclePageIndicatorStyle:I = 0x7f0102dd

.field public static final vpiIconPageIndicatorStyle:I = 0x7f0102df

.field public static final vpiTabPageIndicatorStyle:I = 0x7f0102de

.field public static final windowActionBar:I = 0x7f01009c

.field public static final windowActionBarOverlay:I = 0x7f01009d

.field public static final windowSplitActionBar:I = 0x7f01009e

.field public static final windowTitleContainerStyle:I = 0x7f0102a8

.field public static final zOrderOnTop:I = 0x7f0102bc

.field public static final zoom:I = 0x7f010162


# direct methods
.method public constructor <init>()V
    .locals 0

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method
