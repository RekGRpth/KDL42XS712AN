.class public Lcom/facebook/dalvik/DalvikReplaceBuffer;
.super Ljava/lang/Object;
.source "DalvikReplaceBuffer.java"


# static fields
.field private static a:Ljava/lang/String;

.field private static b:Lcom/facebook/dalvik/DalvikReplaceBuffer$Result;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    const-string v0, ""

    sput-object v0, Lcom/facebook/dalvik/DalvikReplaceBuffer;->a:Ljava/lang/String;

    sget-object v0, Lcom/facebook/dalvik/DalvikReplaceBuffer$Result;->NOT_ATTEMPTED:Lcom/facebook/dalvik/DalvikReplaceBuffer$Result;

    sput-object v0, Lcom/facebook/dalvik/DalvikReplaceBuffer;->b:Lcom/facebook/dalvik/DalvikReplaceBuffer$Result;

    return-void
.end method

.method public constructor <init>()V
    .locals 0

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method public static a()Lcom/facebook/dalvik/DalvikReplaceBuffer$Result;
    .locals 1

    sget-object v0, Lcom/facebook/dalvik/DalvikReplaceBuffer;->b:Lcom/facebook/dalvik/DalvikReplaceBuffer$Result;

    return-object v0
.end method

.method public static a(Lcom/facebook/dalvik/DalvikLinearAllocType;)Z
    .locals 2

    invoke-static {}, Lcom/facebook/dalvik/DalvikReplaceBuffer;->c()I

    move-result v0

    iget v1, p0, Lcom/facebook/dalvik/DalvikLinearAllocType;->bufferSizeBytes:I

    if-ge v0, v1, :cond_0

    invoke-static {p0}, Lcom/facebook/dalvik/DalvikReplaceBuffer;->b(Lcom/facebook/dalvik/DalvikLinearAllocType;)V

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public static b()Ljava/lang/String;
    .locals 2

    invoke-static {}, Lcom/facebook/dalvik/DalvikReplaceBuffer;->a()Lcom/facebook/dalvik/DalvikReplaceBuffer$Result;

    move-result-object v0

    sget-object v1, Lcom/facebook/dalvik/DalvikReplaceBuffer$Result;->FAILURE:Lcom/facebook/dalvik/DalvikReplaceBuffer$Result;

    if-ne v0, v1, :cond_0

    sget-object v0, Lcom/facebook/dalvik/DalvikReplaceBuffer;->a:Ljava/lang/String;

    return-object v0

    :cond_0
    new-instance v0, Ljava/lang/IllegalStateException;

    const-string v1, "No failure string is provided when the operation did not fail."

    invoke-direct {v0, v1}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    throw v0
.end method

.method public static b(Lcom/facebook/dalvik/DalvikLinearAllocType;)V
    .locals 6

    sget-object v0, Lcom/facebook/dalvik/DalvikReplaceBuffer;->b:Lcom/facebook/dalvik/DalvikReplaceBuffer$Result;

    sget-object v1, Lcom/facebook/dalvik/DalvikReplaceBuffer$Result;->NOT_ATTEMPTED:Lcom/facebook/dalvik/DalvikReplaceBuffer$Result;

    if-eq v0, v1, :cond_0

    const-string v0, "DalvikReplaceBuffer"

    const-string v1, "Multiple attempts to replace the buffer detected!"

    invoke-static {v0, v1}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    :goto_0
    return-void

    :cond_0
    iget v1, p0, Lcom/facebook/dalvik/DalvikLinearAllocType;->bufferSizeBytes:I

    :try_start_0
    invoke-static {}, Lcom/facebook/dalvik/DalvikInternals;->a()J

    move-result-wide v2

    const-wide/16 v4, 0x0

    cmp-long v0, v2, v4

    if-nez v0, :cond_1

    new-instance v0, Ljava/io/IOException;

    const-string v2, "Failed to find LinearAllocHdr."

    invoke-direct {v0, v2}, Ljava/io/IOException;-><init>(Ljava/lang/String;)V

    throw v0
    :try_end_0
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_0

    :catch_0
    move-exception v0

    sget-object v2, Lcom/facebook/dalvik/DalvikReplaceBuffer$Result;->FAILURE:Lcom/facebook/dalvik/DalvikReplaceBuffer$Result;

    sput-object v2, Lcom/facebook/dalvik/DalvikReplaceBuffer;->b:Lcom/facebook/dalvik/DalvikReplaceBuffer$Result;

    invoke-virtual {v0}, Ljava/io/IOException;->getMessage()Ljava/lang/String;

    move-result-object v2

    sput-object v2, Lcom/facebook/dalvik/DalvikReplaceBuffer;->a:Ljava/lang/String;

    const-string v2, "DalvikReplaceBuffer"

    new-instance v3, Ljava/lang/StringBuilder;

    const-string v4, "Failed to replace LinearAlloc buffer (at size "

    invoke-direct {v3, v4}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v3, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v3, "). Continuing with standard buffer."

    invoke-virtual {v1, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v2, v1, v0}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I

    goto :goto_0

    :cond_1
    const/16 v0, 0x1000

    :try_start_1
    invoke-static {v2, v3, v1, v0}, Lcom/facebook/dalvik/DalvikInternals;->replaceLinearAllocBuffer(JII)V

    sget-object v0, Lcom/facebook/dalvik/DalvikReplaceBuffer$Result;->SUCCESS:Lcom/facebook/dalvik/DalvikReplaceBuffer$Result;

    sput-object v0, Lcom/facebook/dalvik/DalvikReplaceBuffer;->b:Lcom/facebook/dalvik/DalvikReplaceBuffer$Result;
    :try_end_1
    .catch Ljava/io/IOException; {:try_start_1 .. :try_end_1} :catch_0

    goto :goto_0
.end method

.method private static c()I
    .locals 2

    sget v0, Landroid/os/Build$VERSION;->SDK_INT:I

    const/16 v1, 0xb

    if-ge v0, v1, :cond_0

    const/high16 v0, 0x500000

    :goto_0
    return v0

    :cond_0
    const/16 v1, 0x10

    if-ge v0, v1, :cond_1

    const/high16 v0, 0x800000

    goto :goto_0

    :cond_1
    const/high16 v0, 0x1000000

    goto :goto_0
.end method
