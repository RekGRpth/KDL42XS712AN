.class public Lcom/facebook/R$drawable;
.super Ljava/lang/Object;
.source "R.java"


# static fields
.field public static final ab_transparent_light_holo:I = 0x7f020000

.field public static final abc_ab_bottom_solid_dark_holo:I = 0x7f020001

.field public static final abc_ab_bottom_solid_light_holo:I = 0x7f020002

.field public static final abc_ab_bottom_transparent_dark_holo:I = 0x7f020003

.field public static final abc_ab_bottom_transparent_light_holo:I = 0x7f020004

.field public static final abc_ab_share_pack_holo_dark:I = 0x7f020005

.field public static final abc_ab_share_pack_holo_light:I = 0x7f020006

.field public static final abc_ab_solid_dark_holo:I = 0x7f020007

.field public static final abc_ab_solid_light_holo:I = 0x7f020008

.field public static final abc_ab_stacked_solid_dark_holo:I = 0x7f020009

.field public static final abc_ab_stacked_solid_light_holo:I = 0x7f02000a

.field public static final abc_ab_stacked_transparent_dark_holo:I = 0x7f02000b

.field public static final abc_ab_stacked_transparent_light_holo:I = 0x7f02000c

.field public static final abc_ab_transparent_dark_holo:I = 0x7f02000d

.field public static final abc_ab_transparent_light_holo:I = 0x7f02000e

.field public static final abc_cab_background_bottom_holo_dark:I = 0x7f02000f

.field public static final abc_cab_background_bottom_holo_light:I = 0x7f020010

.field public static final abc_cab_background_top_holo_dark:I = 0x7f020011

.field public static final abc_cab_background_top_holo_light:I = 0x7f020012

.field public static final abc_ic_ab_back_holo_dark:I = 0x7f020013

.field public static final abc_ic_ab_back_holo_light:I = 0x7f020014

.field public static final abc_ic_cab_done_holo_dark:I = 0x7f020015

.field public static final abc_ic_cab_done_holo_light:I = 0x7f020016

.field public static final abc_ic_clear:I = 0x7f020017

.field public static final abc_ic_clear_disabled:I = 0x7f020018

.field public static final abc_ic_clear_holo_light:I = 0x7f020019

.field public static final abc_ic_clear_normal:I = 0x7f02001a

.field public static final abc_ic_clear_search_api_disabled_holo_light:I = 0x7f02001b

.field public static final abc_ic_clear_search_api_holo_light:I = 0x7f02001c

.field public static final abc_ic_commit_search_api_holo_dark:I = 0x7f02001d

.field public static final abc_ic_commit_search_api_holo_light:I = 0x7f02001e

.field public static final abc_ic_go:I = 0x7f02001f

.field public static final abc_ic_go_search_api_holo_light:I = 0x7f020020

.field public static final abc_ic_menu_moreoverflow_normal_holo_dark:I = 0x7f020021

.field public static final abc_ic_menu_moreoverflow_normal_holo_light:I = 0x7f020022

.field public static final abc_ic_menu_share_holo_dark:I = 0x7f020023

.field public static final abc_ic_menu_share_holo_light:I = 0x7f020024

.field public static final abc_ic_search:I = 0x7f020025

.field public static final abc_ic_search_api_holo_light:I = 0x7f020026

.field public static final abc_ic_voice_search:I = 0x7f020027

.field public static final abc_ic_voice_search_api_holo_light:I = 0x7f020028

.field public static final abc_item_background_holo_dark:I = 0x7f020029

.field public static final abc_item_background_holo_light:I = 0x7f02002a

.field public static final abc_list_divider_holo_dark:I = 0x7f02002b

.field public static final abc_list_divider_holo_light:I = 0x7f02002c

.field public static final abc_list_focused_holo:I = 0x7f02002d

.field public static final abc_list_longpressed_holo:I = 0x7f02002e

.field public static final abc_list_pressed_holo_dark:I = 0x7f02002f

.field public static final abc_list_pressed_holo_light:I = 0x7f020030

.field public static final abc_list_selector_background_transition_holo_dark:I = 0x7f020031

.field public static final abc_list_selector_background_transition_holo_light:I = 0x7f020032

.field public static final abc_list_selector_disabled_holo_dark:I = 0x7f020033

.field public static final abc_list_selector_disabled_holo_light:I = 0x7f020034

.field public static final abc_list_selector_holo_dark:I = 0x7f020035

.field public static final abc_list_selector_holo_light:I = 0x7f020036

.field public static final abc_menu_dropdown_panel_holo_dark:I = 0x7f020037

.field public static final abc_menu_dropdown_panel_holo_light:I = 0x7f020038

.field public static final abc_menu_hardkey_panel_holo_dark:I = 0x7f020039

.field public static final abc_menu_hardkey_panel_holo_light:I = 0x7f02003a

.field public static final abc_search_dropdown_dark:I = 0x7f02003b

.field public static final abc_search_dropdown_light:I = 0x7f02003c

.field public static final abc_spinner_ab_default_holo_dark:I = 0x7f02003d

.field public static final abc_spinner_ab_default_holo_light:I = 0x7f02003e

.field public static final abc_spinner_ab_disabled_holo_dark:I = 0x7f02003f

.field public static final abc_spinner_ab_disabled_holo_light:I = 0x7f020040

.field public static final abc_spinner_ab_focused_holo_dark:I = 0x7f020041

.field public static final abc_spinner_ab_focused_holo_light:I = 0x7f020042

.field public static final abc_spinner_ab_holo_dark:I = 0x7f020043

.field public static final abc_spinner_ab_holo_light:I = 0x7f020044

.field public static final abc_spinner_ab_pressed_holo_dark:I = 0x7f020045

.field public static final abc_spinner_ab_pressed_holo_light:I = 0x7f020046

.field public static final abc_tab_indicator_ab_holo:I = 0x7f020047

.field public static final abc_tab_selected_focused_holo:I = 0x7f020048

.field public static final abc_tab_selected_holo:I = 0x7f020049

.field public static final abc_tab_selected_pressed_holo:I = 0x7f02004a

.field public static final abc_tab_unselected_pressed_holo:I = 0x7f02004b

.field public static final abc_textfield_search_default_holo_dark:I = 0x7f02004c

.field public static final abc_textfield_search_default_holo_light:I = 0x7f02004d

.field public static final abc_textfield_search_right_default_holo_dark:I = 0x7f02004e

.field public static final abc_textfield_search_right_default_holo_light:I = 0x7f02004f

.field public static final abc_textfield_search_right_selected_holo_dark:I = 0x7f020050

.field public static final abc_textfield_search_right_selected_holo_light:I = 0x7f020051

.field public static final abc_textfield_search_selected_holo_dark:I = 0x7f020052

.field public static final abc_textfield_search_selected_holo_light:I = 0x7f020053

.field public static final abc_textfield_searchview_holo_dark:I = 0x7f020054

.field public static final abc_textfield_searchview_holo_light:I = 0x7f020055

.field public static final abc_textfield_searchview_right_holo_dark:I = 0x7f020056

.field public static final abc_textfield_searchview_right_holo_light:I = 0x7f020057

.field public static final accounts_gear:I = 0x7f020058

.field public static final action_bar_cancelled:I = 0x7f020059

.field public static final action_bar_decline:I = 0x7f02005a

.field public static final action_bar_going:I = 0x7f02005b

.field public static final action_bar_hosting:I = 0x7f02005c

.field public static final action_bar_invite:I = 0x7f02005d

.field public static final action_bar_join:I = 0x7f02005e

.field public static final action_bar_maybe:I = 0x7f02005f

.field public static final action_bar_maybed:I = 0x7f020060

.field public static final action_bar_not_going:I = 0x7f020061

.field public static final action_bar_post:I = 0x7f020062

.field public static final action_bar_save:I = 0x7f020063

.field public static final action_bar_saved:I = 0x7f020064

.field public static final action_bar_saving:I = 0x7f020065

.field public static final action_bar_share:I = 0x7f020066

.field public static final activity_log:I = 0x7f020067

.field public static final add_attachment:I = 0x7f020068

.field public static final add_button_press_state:I = 0x7f020069

.field public static final add_contributor:I = 0x7f02006a

.field public static final add_contributor_button_press_state:I = 0x7f02006b

.field public static final add_friend_blue_l:I = 0x7f02006c

.field public static final add_friend_blue_m:I = 0x7f02006d

.field public static final add_friend_button_fullscreen:I = 0x7f02006e

.field public static final add_friend_dark_grey_l:I = 0x7f02006f

.field public static final add_friend_dark_grey_m:I = 0x7f020070

.field public static final add_friend_icon:I = 0x7f020071

.field public static final add_friend_icon_pressed:I = 0x7f020072

.field public static final add_friend_icon_state:I = 0x7f020073

.field public static final add_friend_light_grey_l:I = 0x7f020074

.field public static final add_friend_light_grey_m:I = 0x7f020075

.field public static final add_friend_white_l:I = 0x7f020076

.field public static final add_icon:I = 0x7f020077

.field public static final add_photo_button:I = 0x7f020078

.field public static final add_photos_plus_sign:I = 0x7f020079

.field public static final add_to_album:I = 0x7f02007a

.field public static final album:I = 0x7f02007b

.field public static final album_gradient:I = 0x7f02007c

.field public static final album_header:I = 0x7f02007d

.field public static final album_permalink_ufi_comment_button:I = 0x7f02007e

.field public static final album_permalink_ufi_field_shadow:I = 0x7f02007f

.field public static final album_permalink_ufi_like_button:I = 0x7f020080

.field public static final album_permalink_ufi_like_button_active:I = 0x7f020081

.field public static final album_permalink_ufi_share_button:I = 0x7f020082

.field public static final albumgradient:I = 0x7f020083

.field public static final alert_x:I = 0x7f020084

.field public static final app_center_rating_bar:I = 0x7f020085

.field public static final app_center_star_empty:I = 0x7f020086

.field public static final app_center_star_full:I = 0x7f020087

.field public static final app_feeds_upsell_background_gradient:I = 0x7f020088

.field public static final appfeed_icon_flickr:I = 0x7f020089

.field public static final appfeed_icon_instagram:I = 0x7f02008a

.field public static final appfeed_icon_pinterest:I = 0x7f02008b

.field public static final appfeed_icon_tumblr:I = 0x7f02008c

.field public static final appirater_dialog_title_bg:I = 0x7f02008d

.field public static final appirater_ise_like_big:I = 0x7f02008e

.field public static final appwidget_background:I = 0x7f02008f

.field public static final appwidget_bg:I = 0x7f020090

.field public static final appwidget_bg_focus:I = 0x7f020091

.field public static final appwidget_bg_press:I = 0x7f020092

.field public static final appwidget_top_white:I = 0x7f020093

.field public static final arrow:I = 0x7f020094

.field public static final arrow_collapse:I = 0x7f020095

.field public static final arrow_down:I = 0x7f020096

.field public static final arrow_expand:I = 0x7f020097

.field public static final arrow_left:I = 0x7f020098

.field public static final arrow_right:I = 0x7f020099

.field public static final attachment_action_button_divider:I = 0x7f02009a

.field public static final attachment_background:I = 0x7f02009b

.field public static final attachment_event_join_icon_fallback:I = 0x7f02009c

.field public static final attachment_icon_background:I = 0x7f02009d

.field public static final attachment_thumbnail_shadow:I = 0x7f02009e

.field public static final attribution_amex:I = 0x7f02009f

.field public static final attribution_freebase:I = 0x7f0200a0

.field public static final attribution_harvard:I = 0x7f0200a1

.field public static final attribution_ingram:I = 0x7f0200a2

.field public static final attribution_wikipedia:I = 0x7f0200a3

.field public static final audience_acquaintances:I = 0x7f0200a4

.field public static final audience_acquaintances_blue:I = 0x7f0200a5

.field public static final audience_acquaintances_footer:I = 0x7f0200a6

.field public static final audience_close_friends:I = 0x7f0200a7

.field public static final audience_close_friends_footer:I = 0x7f0200a8

.field public static final audience_closefriends_blue:I = 0x7f0200a9

.field public static final audience_custom:I = 0x7f0200aa

.field public static final audience_custom_blue:I = 0x7f0200ab

.field public static final audience_custom_footer:I = 0x7f0200ac

.field public static final audience_educator_icon:I = 0x7f0200ad

.field public static final audience_everyone:I = 0x7f0200ae

.field public static final audience_everyone_footer:I = 0x7f0200af

.field public static final audience_facebook:I = 0x7f0200b0

.field public static final audience_facebook_blue:I = 0x7f0200b1

.field public static final audience_facebook_footer:I = 0x7f0200b2

.field public static final audience_family_blue:I = 0x7f0200b3

.field public static final audience_family_footer:I = 0x7f0200b4

.field public static final audience_family_list:I = 0x7f0200b5

.field public static final audience_fofs:I = 0x7f0200b6

.field public static final audience_fofs_footer:I = 0x7f0200b7

.field public static final audience_friendlist_blue:I = 0x7f0200b8

.field public static final audience_friends:I = 0x7f0200b9

.field public static final audience_friends_blue:I = 0x7f0200ba

.field public static final audience_friends_except_acquaintances:I = 0x7f0200bb

.field public static final audience_friends_except_acquaintances_footer:I = 0x7f0200bc

.field public static final audience_friends_footer:I = 0x7f0200bd

.field public static final audience_friendsexcept_blue:I = 0x7f0200be

.field public static final audience_friendsoffriends_blue:I = 0x7f0200bf

.field public static final audience_friendsoftagged_blue:I = 0x7f0200c0

.field public static final audience_friendsoftagged_expansion:I = 0x7f0200c1

.field public static final audience_friendsoftagged_gray:I = 0x7f0200c2

.field public static final audience_friendstagged_blue:I = 0x7f0200c3

.field public static final audience_friendstagged_expansion:I = 0x7f0200c4

.field public static final audience_friendstagged_gray:I = 0x7f0200c5

.field public static final audience_friendstagged_light_gray:I = 0x7f0200c6

.field public static final audience_lists:I = 0x7f0200c7

.field public static final audience_lists_footer:I = 0x7f0200c8

.field public static final audience_location_blue:I = 0x7f0200c9

.field public static final audience_location_footer:I = 0x7f0200ca

.field public static final audience_location_list:I = 0x7f0200cb

.field public static final audience_only_me:I = 0x7f0200cc

.field public static final audience_only_me_footer:I = 0x7f0200cd

.field public static final audience_onlyme_blue:I = 0x7f0200ce

.field public static final audience_public_blue:I = 0x7f0200cf

.field public static final audience_school_blue:I = 0x7f0200d0

.field public static final audience_school_footer:I = 0x7f0200d1

.field public static final audience_school_list:I = 0x7f0200d2

.field public static final audience_token_custom:I = 0x7f0200d3

.field public static final audience_token_friends_of_friends:I = 0x7f0200d4

.field public static final audience_token_public:I = 0x7f0200d5

.field public static final audience_work_blue:I = 0x7f0200d6

.field public static final audience_work_footer:I = 0x7f0200d7

.field public static final audience_work_list:I = 0x7f0200d8

.field public static final audio_compose_button_circle_gray:I = 0x7f0200d9

.field public static final audio_compose_button_circle_red:I = 0x7f0200da

.field public static final audio_compose_button_circle_white:I = 0x7f0200db

.field public static final audio_player_timer_text_left_normal:I = 0x7f020e53

.field public static final audio_player_timer_text_left_normal_neue:I = 0x7f020e57

.field public static final audio_player_timer_text_left_selected:I = 0x7f020e54

.field public static final audio_player_timer_text_left_selected_neue:I = 0x7f020e58

.field public static final audio_player_timer_text_right_normal:I = 0x7f020e55

.field public static final audio_player_timer_text_right_normal_neue:I = 0x7f020e59

.field public static final audio_player_timer_text_right_selected:I = 0x7f020e56

.field public static final audio_player_timer_text_right_selected_neue:I = 0x7f020e5a

.field public static final back:I = 0x7f0200dc

.field public static final back_chevron:I = 0x7f0200dd

.field public static final background_location_nux_intro_chat_head_mask:I = 0x7f0200de

.field public static final background_location_nux_intro_chathead:I = 0x7f0200df

.field public static final background_location_nux_intro_message_bubble_1:I = 0x7f0200e0

.field public static final background_location_nux_intro_message_bubble_2:I = 0x7f0200e1

.field public static final background_location_nux_intro_notification_bubble:I = 0x7f0200e2

.field public static final background_location_nux_notifications_phone_overlay:I = 0x7f0200e3

.field public static final background_location_nux_notifications_pocket:I = 0x7f0200e4

.field public static final background_location_nux_phone:I = 0x7f0200e5

.field public static final background_location_nux_privacy_card:I = 0x7f0200e6

.field public static final background_location_nux_privacy_checkmark:I = 0x7f0200e7

.field public static final background_location_nux_spinner:I = 0x7f0200e8

.field public static final background_location_nux_spinner_normal:I = 0x7f0200e9

.field public static final background_location_nux_spinner_pressed:I = 0x7f0200ea

.field public static final background_location_nux_timeline_card:I = 0x7f0200eb

.field public static final background_location_nux_timeline_orb:I = 0x7f0200ec

.field public static final background_location_nux_timeline_profile:I = 0x7f0200ed

.field public static final background_location_privacy_settings_checkmark:I = 0x7f0200ee

.field public static final background_location_settings_chevron:I = 0x7f0200ef

.field public static final background_location_settings_icon_off:I = 0x7f0200f0

.field public static final background_location_settings_icon_on:I = 0x7f0200f1

.field public static final background_location_settings_invite:I = 0x7f0200f2

.field public static final background_location_settings_location_settings_icon:I = 0x7f0200f3

.field public static final background_location_settings_report_bug:I = 0x7f0200f4

.field public static final badge_going:I = 0x7f0200f5

.field public static final badge_maybe:I = 0x7f0200f6

.field public static final banner_notif_button_background:I = 0x7f0200f7

.field public static final bar_chart_light_grey:I = 0x7f0200f8

.field public static final beeper_arrow:I = 0x7f0200f9

.field public static final beta_top_right_corner:I = 0x7f0200fa

.field public static final better_switch_thumb:I = 0x7f0200fb

.field public static final better_switch_thumb_checked:I = 0x7f0200fc

.field public static final better_switch_thumb_checked_disabled:I = 0x7f0200fd

.field public static final better_switch_thumb_unchecked:I = 0x7f0200fe

.field public static final better_switch_thumb_unchecked_disabled:I = 0x7f0200ff

.field public static final better_switch_track:I = 0x7f020100

.field public static final better_switch_track_disabled:I = 0x7f020101

.field public static final better_switch_track_enabled:I = 0x7f020102

.field public static final bg_left:I = 0x7f020103

.field public static final bg_left_pressed:I = 0x7f020104

.field public static final bg_left_selector:I = 0x7f020105

.field public static final bg_middle:I = 0x7f020106

.field public static final bg_middle_pressed:I = 0x7f020107

.field public static final bg_middle_selector:I = 0x7f020108

.field public static final bg_right:I = 0x7f020109

.field public static final bg_right_pressed:I = 0x7f02010a

.field public static final bg_right_selector:I = 0x7f02010b

.field public static final black_cursor:I = 0x7f02010c

.field public static final black_px:I = 0x7f02010d

.field public static final black_tiled:I = 0x7f02010e

.field public static final blank_slate:I = 0x7f02010f

.field public static final blank_state_notifications:I = 0x7f020110

.field public static final bling_bar_pressable_rounded_rect:I = 0x7f020111

.field public static final block_light_grey_l:I = 0x7f020112

.field public static final blue_background:I = 0x7f020113

.field public static final blue_button_background:I = 0x7f020114

.field public static final blue_button_bar_button_background:I = 0x7f020115

.field public static final blue_checkmark:I = 0x7f020116

.field public static final blue_like_circle:I = 0x7f020117

.field public static final blue_rating_star:I = 0x7f020118

.field public static final blue_tab_background:I = 0x7f020119

.field public static final blue_text_color:I = 0x7f020e5d

.field public static final blue_touch_target:I = 0x7f02011a

.field public static final blue_touch_target_shadow:I = 0x7f02011b

.field public static final blues_clue_bar:I = 0x7f02011c

.field public static final bookmark_bg:I = 0x7f02011d

.field public static final bookmark_drag_bg:I = 0x7f02011e

.field public static final bookmark_drag_bottom_bg:I = 0x7f02011f

.field public static final bookmark_drag_handle:I = 0x7f020120

.field public static final bookmark_menu_shadow:I = 0x7f020121

.field public static final bookmark_search:I = 0x7f020122

.field public static final bookmark_search_field:I = 0x7f020123

.field public static final bookmark_search_pressed:I = 0x7f020124

.field public static final bookmark_tab_edit_done_button_bg:I = 0x7f020125

.field public static final bookmark_tab_in_favorites:I = 0x7f020126

.field public static final bookmark_tab_item_bg:I = 0x7f020127

.field public static final bookmark_tab_item_count_background:I = 0x7f020128

.field public static final bookmark_tab_not_in_favorites:I = 0x7f020129

.field public static final bookmark_tab_profile_item_bg:I = 0x7f02012a

.field public static final bookmarks_badge:I = 0x7f02012b

.field public static final bookmarks_edit_gear:I = 0x7f02012c

.field public static final bookmarks_menu_back_button_bg:I = 0x7f02012d

.field public static final bookmarks_menu_back_button_bg_normal:I = 0x7f02012e

.field public static final bookmarks_menu_back_button_bg_pressed:I = 0x7f02012f

.field public static final bottom_banner_close:I = 0x7f020130

.field public static final bottom_button_background:I = 0x7f020131

.field public static final bottom_button_background_normal:I = 0x7f020132

.field public static final bottom_button_background_pressed:I = 0x7f020133

.field public static final broadcast_chevron:I = 0x7f020134

.field public static final broadcast_request_action_link_bg:I = 0x7f020135

.field public static final broadcast_request_arrow_right:I = 0x7f020136

.field public static final browser_back_active:I = 0x7f020137

.field public static final browser_back_inactive:I = 0x7f020138

.field public static final browser_chrome_overflow:I = 0x7f020139

.field public static final browser_chrome_up_caret:I = 0x7f02013a

.field public static final browser_copy:I = 0x7f02013b

.field public static final browser_forward_active:I = 0x7f02013c

.field public static final browser_forward_inactive:I = 0x7f02013d

.field public static final browser_menu_bg:I = 0x7f02013e

.field public static final browser_menu_overflow_for_dim:I = 0x7f02013f

.field public static final browser_message:I = 0x7f020140

.field public static final browser_new_post:I = 0x7f020141

.field public static final browser_open_with_chrome:I = 0x7f020142

.field public static final browser_open_with_x:I = 0x7f020143

.field public static final browser_pivots_shadow:I = 0x7f020144

.field public static final browser_save:I = 0x7f020145

.field public static final btn_blue_normal:I = 0x7f020146

.field public static final btn_blue_pressed:I = 0x7f020147

.field public static final btn_blue_selected:I = 0x7f020148

.field public static final btn_check:I = 0x7f020149

.field public static final btn_check_off:I = 0x7f02014a

.field public static final btn_check_on:I = 0x7f02014b

.field public static final btn_global_fb_dark:I = 0x7f02014c

.field public static final btn_global_fb_focus_dark:I = 0x7f02014d

.field public static final btn_global_fb_press_dark:I = 0x7f02014e

.field public static final btn_play_sm:I = 0x7f02014f

.field public static final btn_tag:I = 0x7f020150

.field public static final bubble_left:I = 0x7f020151

.field public static final bubble_right:I = 0x7f020152

.field public static final button_bar_button_background:I = 0x7f020153

.field public static final button_blue:I = 0x7f020154

.field public static final button_blue_background:I = 0x7f020155

.field public static final button_blue_border:I = 0x7f020156

.field public static final button_blue_border_normal:I = 0x7f020157

.field public static final button_blue_border_pressed:I = 0x7f020158

.field public static final button_blue_focused:I = 0x7f020159

.field public static final button_blue_gifts_normal:I = 0x7f02015a

.field public static final button_blue_gifts_pressed:I = 0x7f02015b

.field public static final button_blue_normal:I = 0x7f02015c

.field public static final button_blue_pressed:I = 0x7f02015d

.field public static final button_check:I = 0x7f02015e

.field public static final button_check_off:I = 0x7f02015f

.field public static final button_check_on:I = 0x7f020160

.field public static final button_friend_request_blue:I = 0x7f020161

.field public static final button_friend_request_grey:I = 0x7f020162

.field public static final button_gray_gifts_normal:I = 0x7f020163

.field public static final button_gray_gifts_pressed:I = 0x7f020164

.field public static final button_green:I = 0x7f020165

.field public static final button_green_background:I = 0x7f020166

.field public static final button_green_focused:I = 0x7f020167

.field public static final button_green_gifts_normal:I = 0x7f020168

.field public static final button_green_gifts_pressed:I = 0x7f020169

.field public static final button_green_normal:I = 0x7f02016a

.field public static final button_green_pressed:I = 0x7f02016b

.field public static final button_grey_background:I = 0x7f02016c

.field public static final button_grey_focused:I = 0x7f02016d

.field public static final button_grey_normal:I = 0x7f02016e

.field public static final button_grey_pressed:I = 0x7f02016f

.field public static final button_light:I = 0x7f020170

.field public static final button_nav_gray_disabled:I = 0x7f020171

.field public static final button_nav_red:I = 0x7f020172

.field public static final button_nav_red_hit:I = 0x7f020173

.field public static final button_nav_red_normal:I = 0x7f020174

.field public static final button_new_blue:I = 0x7f020175

.field public static final button_new_blue_focused:I = 0x7f020176

.field public static final button_new_blue_normal:I = 0x7f020177

.field public static final button_new_blue_pressed:I = 0x7f020178

.field public static final button_new_green:I = 0x7f020179

.field public static final button_new_green_focused:I = 0x7f02017a

.field public static final button_new_green_normal:I = 0x7f02017b

.field public static final button_new_green_pressed:I = 0x7f02017c

.field public static final button_new_grey:I = 0x7f02017d

.field public static final button_new_grey_focused:I = 0x7f02017e

.field public static final button_new_grey_normal:I = 0x7f02017f

.field public static final button_new_grey_pressed:I = 0x7f020180

.field public static final button_white_border_normal:I = 0x7f020181

.field public static final button_white_border_pressed:I = 0x7f020182

.field public static final buttonbar_divider:I = 0x7f020183

.field public static final call:I = 0x7f020184

.field public static final call_blue_l:I = 0x7f020185

.field public static final call_dark_grey_l:I = 0x7f020186

.field public static final call_dark_grey_m:I = 0x7f020187

.field public static final call_dark_grey_s:I = 0x7f020188

.field public static final call_glyph_with_state:I = 0x7f020189

.field public static final call_light_grey_l:I = 0x7f02018a

.field public static final call_red_l:I = 0x7f02018b

.field public static final call_to_action_book_travel:I = 0x7f02018c

.field public static final call_to_action_download:I = 0x7f02018d

.field public static final call_to_action_learn_more:I = 0x7f02018e

.field public static final call_to_action_replay:I = 0x7f02018f

.field public static final call_to_action_shop_now:I = 0x7f020190

.field public static final call_to_action_sign_up:I = 0x7f020191

.field public static final camera_button:I = 0x7f020192

.field public static final camera_button_pressed:I = 0x7f020193

.field public static final camera_button_states:I = 0x7f020194

.field public static final camera_dark_grey_l:I = 0x7f020195

.field public static final camera_flash:I = 0x7f020196

.field public static final camera_flash_auto:I = 0x7f020197

.field public static final camera_flash_auto_pressed:I = 0x7f020198

.field public static final camera_flash_inactive:I = 0x7f020199

.field public static final camera_flash_inactive_pressed:I = 0x7f02019a

.field public static final camera_flash_pressed:I = 0x7f02019b

.field public static final camera_flash_selector:I = 0x7f02019c

.field public static final camera_focus_box:I = 0x7f02019d

.field public static final camera_gallery_icon_inactive:I = 0x7f02019e

.field public static final camera_icon:I = 0x7f02019f

.field public static final camera_icon_active:I = 0x7f0201a0

.field public static final camera_icon_album_permalink:I = 0x7f0201a1

.field public static final camera_menu_button_background:I = 0x7f0201a2

.field public static final camera_photo_button:I = 0x7f0201a3

.field public static final camera_photo_button_normal:I = 0x7f0201a4

.field public static final camera_photo_button_pressed:I = 0x7f0201a5

.field public static final camera_photo_icon_active:I = 0x7f0201a6

.field public static final camera_review_accept_button:I = 0x7f0201a7

.field public static final camera_review_cancel:I = 0x7f0201a8

.field public static final camera_review_cancel_inactive:I = 0x7f0201a9

.field public static final camera_review_checkmark:I = 0x7f0201aa

.field public static final camera_review_checkmark_inactive:I = 0x7f0201ab

.field public static final camera_review_play_button:I = 0x7f0201ac

.field public static final camera_review_play_button_pressed:I = 0x7f0201ad

.field public static final camera_review_reject_button:I = 0x7f0201ae

.field public static final camera_review_video_play_button:I = 0x7f0201af

.field public static final camera_switch:I = 0x7f0201b0

.field public static final camera_switch_button_selector:I = 0x7f0201b1

.field public static final camera_switch_pressed:I = 0x7f0201b2

.field public static final camera_video_button_active:I = 0x7f0201b3

.field public static final camera_video_button_normal:I = 0x7f0201b4

.field public static final cancel_button_states:I = 0x7f0201b5

.field public static final caption_shadow:I = 0x7f0201b6

.field public static final card:I = 0x7f0201b7

.field public static final card_bg:I = 0x7f0201b8

.field public static final card_button:I = 0x7f0201b9

.field public static final card_button_hit:I = 0x7f0201ba

.field public static final card_button_normal:I = 0x7f0201bb

.field public static final card_field:I = 0x7f0201bc

.field public static final card_placeholder_thumb:I = 0x7f0201bd

.field public static final cards_card_bottom:I = 0x7f0201be

.field public static final cards_card_full:I = 0x7f0201bf

.field public static final cards_card_middle:I = 0x7f0201c0

.field public static final cards_card_top:I = 0x7f0201c1

.field public static final carousel_swipe_arrow:I = 0x7f0201c2

.field public static final cc_amex:I = 0x7f0201c3

.field public static final cc_discover:I = 0x7f0201c4

.field public static final cc_mc:I = 0x7f0201c5

.field public static final cc_placeholder:I = 0x7f0201c6

.field public static final cc_visa:I = 0x7f0201c7

.field public static final celebrations_gift_glyph:I = 0x7f0201c8

.field public static final celebrations_message_glyph:I = 0x7f0201c9

.field public static final celebrations_post_glyph:I = 0x7f0201ca

.field public static final celebrations_unit_cover_photo_overlay_background:I = 0x7f0201cb

.field public static final cell:I = 0x7f0201cc

.field public static final chaining_section_background:I = 0x7f0201cd

.field public static final chat_availability_switch_thumb:I = 0x7f0201ce

.field public static final chat_availability_switch_track_off:I = 0x7f0201cf

.field public static final chat_availability_switch_track_on:I = 0x7f0201d0

.field public static final chat_heads_notif_info:I = 0x7f0201d1

.field public static final check:I = 0x7f0201d2

.field public static final check_button:I = 0x7f0201d3

.field public static final check_icon:I = 0x7f0201d4

.field public static final checkbox_radio_button:I = 0x7f0201d5

.field public static final checkin_corner_triangle:I = 0x7f0201d6

.field public static final checkmark:I = 0x7f0201d7

.field public static final checkmark_blue:I = 0x7f0201d8

.field public static final checkmark_blue_off:I = 0x7f0201d9

.field public static final checkmark_blue_on:I = 0x7f0201da

.field public static final checkmark_dark_icon:I = 0x7f0201db

.field public static final checkmark_default:I = 0x7f0201dc

.field public static final checkmark_green:I = 0x7f0201dd

.field public static final checkmark_green_off:I = 0x7f0201de

.field public static final checkmark_green_on:I = 0x7f0201df

.field public static final checkmark_neue:I = 0x7f0201e0

.field public static final checkmark_selected:I = 0x7f0201e1

.field public static final checkmark_states:I = 0x7f0201e2

.field public static final checkmark_toggle:I = 0x7f0201e3

.field public static final checkmark_white_default:I = 0x7f0201e4

.field public static final checkmark_white_disabled:I = 0x7f0201e5

.field public static final checkmark_white_selected:I = 0x7f0201e6

.field public static final chevron:I = 0x7f0201e7

.field public static final chevron_dark:I = 0x7f0201e8

.field public static final chevron_down_light_grey_m:I = 0x7f0201e9

.field public static final chevron_grey:I = 0x7f0201ea

.field public static final chevron_left_dark_grey_l:I = 0x7f0201eb

.field public static final chevron_left_light_grey_l:I = 0x7f0201ec

.field public static final chevron_right_dark_grey_s:I = 0x7f0201ed

.field public static final chevron_right_light_grey_m:I = 0x7f0201ee

.field public static final chevron_right_white_s:I = 0x7f0201ef

.field public static final choose_profile_picture_placeholder:I = 0x7f0201f0

.field public static final chrome_badge_count:I = 0x7f0201f1

.field public static final chrome_bottom_bar:I = 0x7f0201f2

.field public static final chrome_facebox:I = 0x7f0201f3

.field public static final chrome_gradient:I = 0x7f0201f4

.field public static final circle_indicator:I = 0x7f0201f5

.field public static final circle_progress_bg_person_icon:I = 0x7f0201f6

.field public static final circled_cross:I = 0x7f0201f7

.field public static final clear:I = 0x7f020e5f

.field public static final clear_text_box:I = 0x7f0201f8

.field public static final clickable_item_bg:I = 0x7f0201f9

.field public static final clock_dark_grey_l:I = 0x7f0201fa

.field public static final clock_dark_grey_s:I = 0x7f0201fb

.field public static final clock_light_grey_l:I = 0x7f0201fc

.field public static final close:I = 0x7f0201fd

.field public static final close_icon:I = 0x7f0201fe

.field public static final close_icon_selected:I = 0x7f0201ff

.field public static final close_x:I = 0x7f020200

.field public static final code_generator:I = 0x7f020201

.field public static final collection_image_translucent_border:I = 0x7f020202

.field public static final collection_list_item_bg:I = 0x7f020203

.field public static final collection_list_item_recently_added:I = 0x7f020204

.field public static final collection_list_item_standard:I = 0x7f020205

.field public static final collection_list_item_suggestion:I = 0x7f020206

.field public static final collection_subscribe:I = 0x7f020207

.field public static final collection_subscribed:I = 0x7f020208

.field public static final collections_find_friends_action:I = 0x7f020209

.field public static final com_facebook_newsfeed_inline_player_play_button:I = 0x7f02020a

.field public static final com_facebook_video_inline_player_play_button:I = 0x7f02020b

.field public static final com_facebook_video_pivots_play_button:I = 0x7f02020c

.field public static final combo_large:I = 0x7f02020d

.field public static final combo_small:I = 0x7f02020e

.field public static final comment_icon:I = 0x7f02020f

.field public static final comment_icon_new:I = 0x7f020210

.field public static final comment_light_grey_icon:I = 0x7f020211

.field public static final comment_retry:I = 0x7f020212

.field public static final comment_retry_pressed:I = 0x7f020213

.field public static final common_signin_btn_icon_dark:I = 0x7f020214

.field public static final common_signin_btn_icon_disabled_dark:I = 0x7f020215

.field public static final common_signin_btn_icon_disabled_focus_dark:I = 0x7f020216

.field public static final common_signin_btn_icon_disabled_focus_light:I = 0x7f020217

.field public static final common_signin_btn_icon_disabled_light:I = 0x7f020218

.field public static final common_signin_btn_icon_focus_dark:I = 0x7f020219

.field public static final common_signin_btn_icon_focus_light:I = 0x7f02021a

.field public static final common_signin_btn_icon_light:I = 0x7f02021b

.field public static final common_signin_btn_icon_normal_dark:I = 0x7f02021c

.field public static final common_signin_btn_icon_normal_light:I = 0x7f02021d

.field public static final common_signin_btn_icon_pressed_dark:I = 0x7f02021e

.field public static final common_signin_btn_icon_pressed_light:I = 0x7f02021f

.field public static final common_signin_btn_text_dark:I = 0x7f020220

.field public static final common_signin_btn_text_disabled_dark:I = 0x7f020221

.field public static final common_signin_btn_text_disabled_focus_dark:I = 0x7f020222

.field public static final common_signin_btn_text_disabled_focus_light:I = 0x7f020223

.field public static final common_signin_btn_text_disabled_light:I = 0x7f020224

.field public static final common_signin_btn_text_focus_dark:I = 0x7f020225

.field public static final common_signin_btn_text_focus_light:I = 0x7f020226

.field public static final common_signin_btn_text_light:I = 0x7f020227

.field public static final common_signin_btn_text_normal_dark:I = 0x7f020228

.field public static final common_signin_btn_text_normal_light:I = 0x7f020229

.field public static final common_signin_btn_text_pressed_dark:I = 0x7f02022a

.field public static final common_signin_btn_text_pressed_light:I = 0x7f02022b

.field public static final compose_blue:I = 0x7f02022c

.field public static final compose_button_states:I = 0x7f02022d

.field public static final compose_dark_grey_l:I = 0x7f02022e

.field public static final compose_dark_grey_m:I = 0x7f02022f

.field public static final compose_light_grey_l:I = 0x7f020230

.field public static final composer_album_active:I = 0x7f020231

.field public static final composer_album_button:I = 0x7f020232

.field public static final composer_album_normal:I = 0x7f020233

.field public static final composer_attachments_button_background_selected:I = 0x7f020234

.field public static final composer_attachments_button_background_selected_top:I = 0x7f020235

.field public static final composer_audience_bar_background:I = 0x7f020236

.field public static final composer_audience_dotted_divider:I = 0x7f020237

.field public static final composer_audience_dotted_segment:I = 0x7f020238

.field public static final composer_auto_tag_active:I = 0x7f020239

.field public static final composer_auto_tag_inactive:I = 0x7f02023a

.field public static final composer_balloons:I = 0x7f02023b

.field public static final composer_footer_bar_background:I = 0x7f02023c

.field public static final composer_group:I = 0x7f02023d

.field public static final composer_left:I = 0x7f02023e

.field public static final composer_left_normal:I = 0x7f02023f

.field public static final composer_left_pressed:I = 0x7f020240

.field public static final composer_like_button:I = 0x7f020241

.field public static final composer_location_button:I = 0x7f020242

.field public static final composer_location_button_neue:I = 0x7f020243

.field public static final composer_location_button_neue_off:I = 0x7f020244

.field public static final composer_location_button_neue_on:I = 0x7f020245

.field public static final composer_middle:I = 0x7f020246

.field public static final composer_middle_normal:I = 0x7f020247

.field public static final composer_middle_pressed:I = 0x7f020248

.field public static final composer_minutiae_active:I = 0x7f020249

.field public static final composer_minutiae_button:I = 0x7f02024a

.field public static final composer_minutiae_free_form_bg:I = 0x7f02024b

.field public static final composer_minutiae_icon_placeholder:I = 0x7f02024c

.field public static final composer_minutiae_normal:I = 0x7f02024d

.field public static final composer_minutiae_search_bg:I = 0x7f02024e

.field public static final composer_minutiae_view_bg:I = 0x7f02024f

.field public static final composer_people_active:I = 0x7f020250

.field public static final composer_people_normal:I = 0x7f020251

.field public static final composer_photo_active:I = 0x7f020252

.field public static final composer_photo_button:I = 0x7f020253

.field public static final composer_photo_normal:I = 0x7f020254

.field public static final composer_place_active:I = 0x7f020255

.field public static final composer_place_button:I = 0x7f020256

.field public static final composer_place_normal:I = 0x7f020257

.field public static final composer_plus_icon_border:I = 0x7f020258

.field public static final composer_right:I = 0x7f020259

.field public static final composer_right_normal:I = 0x7f02025a

.field public static final composer_right_pressed:I = 0x7f02025b

.field public static final composer_single:I = 0x7f02025c

.field public static final composer_single_normal:I = 0x7f02025d

.field public static final composer_single_pressed:I = 0x7f02025e

.field public static final composer_stickers_button_background_selected:I = 0x7f02025f

.field public static final composer_stickers_button_background_selected_top:I = 0x7f020260

.field public static final composer_tagging_badge:I = 0x7f020261

.field public static final composer_title_back:I = 0x7f020262

.field public static final composerbar:I = 0x7f020263

.field public static final confirm_button_states:I = 0x7f020264

.field public static final contact_icon:I = 0x7f020265

.field public static final contacts_profile_photo_background:I = 0x7f020266

.field public static final contacts_section_overlay:I = 0x7f020267

.field public static final content_separator_dot:I = 0x7f020268

.field public static final context_chevron:I = 0x7f020269

.field public static final context_items_divider:I = 0x7f02026a

.field public static final control_widget_icon:I = 0x7f02026b

.field public static final corner_triangle:I = 0x7f02026c

.field public static final corrupted_image_placeholder:I = 0x7f02026d

.field public static final counter:I = 0x7f02026e

.field public static final counter_active:I = 0x7f02026f

.field public static final cover_feed_icon:I = 0x7f020270

.field public static final cover_feed_nux_dialog_shadow:I = 0x7f020271

.field public static final cover_feed_nux_dialog_x_button:I = 0x7f020272

.field public static final cover_feed_upsell_x:I = 0x7f020273

.field public static final cover_photo_placeholder:I = 0x7f020274

.field public static final create_album_button:I = 0x7f020275

.field public static final create_event:I = 0x7f020276

.field public static final crop_image_button_bg_selector:I = 0x7f020277

.field public static final cross_dark_grey_l:I = 0x7f020278

.field public static final cross_red_l:I = 0x7f020279

.field public static final cross_white_m:I = 0x7f02027a

.field public static final custom_progress_bar:I = 0x7f02027b

.field public static final custom_progress_bg:I = 0x7f02027c

.field public static final custom_progress_loader:I = 0x7f02027d

.field public static final dark_gray_text_color:I = 0x7f020e5e

.field public static final dash_comment_icon:I = 0x7f02027e

.field public static final dash_edge_square:I = 0x7f02027f

.field public static final dash_flyout_like_icon:I = 0x7f020280

.field public static final dash_flyout_like_icon_pressed:I = 0x7f020281

.field public static final dash_flyout_load_more_icon:I = 0x7f020282

.field public static final dash_flyout_no_comments_placeholder:I = 0x7f020283

.field public static final dash_gating_background:I = 0x7f020284

.field public static final dash_help_center:I = 0x7f020285

.field public static final dash_login_background:I = 0x7f020286

.field public static final dash_more_options_icon_grey:I = 0x7f020287

.field public static final dash_more_options_icon_white:I = 0x7f020288

.field public static final dash_ood_background:I = 0x7f020289

.field public static final dash_ood_no_data_icon:I = 0x7f02028a

.field public static final dash_ood_view_setting_button:I = 0x7f02028b

.field public static final dash_ood_view_setting_button_pressed:I = 0x7f02028c

.field public static final dash_out_of_data_background:I = 0x7f02028d

.field public static final dash_out_of_data_button_press_state:I = 0x7f02028e

.field public static final dash_preference_category_background:I = 0x7f02028f

.field public static final dash_preference_checkbox:I = 0x7f020290

.field public static final dash_preference_checkbox_off:I = 0x7f020291

.field public static final dash_preference_checkbox_on:I = 0x7f020292

.field public static final dash_preference_dialog_background:I = 0x7f020293

.field public static final dash_preference_dialog_button_pressed:I = 0x7f020294

.field public static final dash_preference_dialog_button_selector:I = 0x7f020295

.field public static final dash_preference_dialog_title:I = 0x7f020296

.field public static final dash_preference_list_separator:I = 0x7f020297

.field public static final dash_preference_panel_top:I = 0x7f020298

.field public static final dash_preference_switch_background:I = 0x7f020299

.field public static final dash_preference_switch_thumb:I = 0x7f02029a

.field public static final dash_preference_switch_thumb_off:I = 0x7f02029b

.field public static final dash_preference_switch_thumb_on:I = 0x7f02029c

.field public static final dash_preference_switch_thumb_pressed_off:I = 0x7f02029d

.field public static final dash_preference_switch_thumb_pressed_on:I = 0x7f02029e

.field public static final dash_preferences_close_button:I = 0x7f02029f

.field public static final dash_preferences_list_selector:I = 0x7f0202a0

.field public static final dash_v2_nux_icon:I = 0x7f0202a1

.field public static final dashboard_decline:I = 0x7f0202a2

.field public static final dashboard_decline_selected:I = 0x7f0202a3

.field public static final dashboard_delete:I = 0x7f0202a4

.field public static final dashboard_edit:I = 0x7f0202a5

.field public static final dashboard_filter_hosting:I = 0x7f0202a6

.field public static final dashboard_filter_hosting_selected:I = 0x7f0202a7

.field public static final dashboard_filter_invited:I = 0x7f0202a8

.field public static final dashboard_filter_invited_selected:I = 0x7f0202a9

.field public static final dashboard_filter_past:I = 0x7f0202aa

.field public static final dashboard_filter_past_selected:I = 0x7f0202ab

.field public static final dashboard_filter_saved:I = 0x7f0202ac

.field public static final dashboard_filter_saved_selected:I = 0x7f0202ad

.field public static final dashboard_filter_upcoming:I = 0x7f0202ae

.field public static final dashboard_filter_upcoming_selected:I = 0x7f0202af

.field public static final dashboard_going:I = 0x7f0202b0

.field public static final dashboard_going_button:I = 0x7f0202b1

.field public static final dashboard_going_selected:I = 0x7f0202b2

.field public static final dashboard_hosting_button:I = 0x7f0202b3

.field public static final dashboard_maybe:I = 0x7f0202b4

.field public static final dashboard_maybe_button:I = 0x7f0202b5

.field public static final dashboard_maybe_selected:I = 0x7f0202b6

.field public static final dashboard_not_going_button:I = 0x7f0202b7

.field public static final dashboard_saved_button:I = 0x7f0202b8

.field public static final dashcard_image_shadow:I = 0x7f0202b9

.field public static final dashclock_notifications_icon:I = 0x7f0202ba

.field public static final data_large:I = 0x7f0202bb

.field public static final data_small:I = 0x7f0202bc

.field public static final dbl_delete_icon:I = 0x7f0202bd

.field public static final dbl_nux_divider_vertical:I = 0x7f0202be

.field public static final dbl_passcode_bullets:I = 0x7f0202bf

.field public static final dbl_profile_pic_border:I = 0x7f0202c0

.field public static final default_event_target_icon:I = 0x7f0202c1

.field public static final default_group_target_icon:I = 0x7f0202c2

.field public static final default_scrollbar:I = 0x7f0202c3

.field public static final default_video_icon:I = 0x7f0202c4

.field public static final dialog_button_blue:I = 0x7f0202c5

.field public static final dialog_button_blue_pressed:I = 0x7f0202c6

.field public static final dialog_button_divider:I = 0x7f0202c7

.field public static final dialog_button_gray:I = 0x7f0202c8

.field public static final dialog_button_gray_pressed:I = 0x7f0202c9

.field public static final dialog_check_icon:I = 0x7f0202ca

.field public static final dialog_checkbox:I = 0x7f0202cb

.field public static final dialog_checkbox_off:I = 0x7f0202cc

.field public static final dialog_checkbox_on:I = 0x7f0202cd

.field public static final dialog_close_button:I = 0x7f0202ce

.field public static final dialog_close_icon:I = 0x7f0202cf

.field public static final dialog_close_icon_selected:I = 0x7f0202d0

.field public static final dialog_full_holo_light:I = 0x7f0202d1

.field public static final dialog_item_category_background:I = 0x7f0202d2

.field public static final dialog_list_selector:I = 0x7f0202d3

.field public static final dialog_list_separator_background:I = 0x7f0202d4

.field public static final dialog_panel:I = 0x7f0202d5

.field public static final dialog_panel_background:I = 0x7f0202d6

.field public static final dialog_panel_top:I = 0x7f0202d7

.field public static final dialog_row_background:I = 0x7f0202d8

.field public static final dialog_row_rounded_bottom_background:I = 0x7f0202d9

.field public static final diode_mandatory:I = 0x7f0202da

.field public static final diode_promo_sticker:I = 0x7f0202db

.field public static final diode_warning:I = 0x7f0202dc

.field public static final directions_dark_grey_s:I = 0x7f0202dd

.field public static final disclosure_arrow:I = 0x7f0202de

.field public static final disclosure_arrow_down:I = 0x7f0202df

.field public static final disclosure_arrow_up:I = 0x7f0202e0

.field public static final dismiss_button:I = 0x7f0202e1

.field public static final divebar_contact_bg:I = 0x7f0202e2

.field public static final dollar_gray:I = 0x7f0202e3

.field public static final doodle_photo_edit_draw_button:I = 0x7f0202e4

.field public static final doodle_photo_edit_text_button:I = 0x7f0202e5

.field public static final dot:I = 0x7f0202e6

.field public static final dot_selected:I = 0x7f0202e7

.field public static final double_tap_heart:I = 0x7f0202e8

.field public static final double_tap_like_thumb:I = 0x7f0202e9

.field public static final double_tap_star:I = 0x7f0202ea

.field public static final double_tap_thumb:I = 0x7f0202eb

.field public static final down_gradient:I = 0x7f0202ec

.field public static final draganddropbookmark:I = 0x7f0202ed

.field public static final drawer_left_shadow:I = 0x7f0202ee

.field public static final drawer_right_shadow:I = 0x7f0202ef

.field public static final drop_down:I = 0x7f0202f0

.field public static final drop_shadow:I = 0x7f0202f1

.field public static final dropdown_arrow:I = 0x7f0202f2

.field public static final dropdown_dropshadow:I = 0x7f0202f3

.field public static final edit_dark_grey_l:I = 0x7f0202f4

.field public static final edit_dark_grey_m:I = 0x7f0202f5

.field public static final edit_light_grey_l:I = 0x7f0202f6

.field public static final edit_privacy_row_background:I = 0x7f0202f7

.field public static final edit_s:I = 0x7f0202f8

.field public static final edit_text_facebookholo_light:I = 0x7f0202f9

.field public static final education_reshare_tip_collapsed:I = 0x7f0202fa

.field public static final education_reshare_tip_expanded:I = 0x7f0202fb

.field public static final education_tag_tip_collapsed:I = 0x7f0202fc

.field public static final education_tag_tip_expanded:I = 0x7f0202fd

.field public static final education_tip_close:I = 0x7f0202fe

.field public static final emoji_1f004_32:I = 0x7f0202ff

.field public static final emoji_1f192_32:I = 0x7f020300

.field public static final emoji_1f199_32:I = 0x7f020301

.field public static final emoji_1f19a_32:I = 0x7f020302

.field public static final emoji_1f1e8_1f1f3_32:I = 0x7f020303

.field public static final emoji_1f1e9_1f1ea_32:I = 0x7f020304

.field public static final emoji_1f1ea_1f1f8_32:I = 0x7f020305

.field public static final emoji_1f1eb_1f1f7_32:I = 0x7f020306

.field public static final emoji_1f1ec_1f1e7_32:I = 0x7f020307

.field public static final emoji_1f1ee_1f1f9_32:I = 0x7f020308

.field public static final emoji_1f1ef_1f1f5_32:I = 0x7f020309

.field public static final emoji_1f1f0_1f1f7_32:I = 0x7f02030a

.field public static final emoji_1f1f7_1f1fa_32:I = 0x7f02030b

.field public static final emoji_1f1fa_1f1f8_32:I = 0x7f02030c

.field public static final emoji_1f201_32:I = 0x7f02030d

.field public static final emoji_1f202_32:I = 0x7f02030e

.field public static final emoji_1f21a_32:I = 0x7f02030f

.field public static final emoji_1f22f_32:I = 0x7f020310

.field public static final emoji_1f233_32:I = 0x7f020311

.field public static final emoji_1f235_32:I = 0x7f020312

.field public static final emoji_1f236_32:I = 0x7f020313

.field public static final emoji_1f237_32:I = 0x7f020314

.field public static final emoji_1f238_32:I = 0x7f020315

.field public static final emoji_1f239_32:I = 0x7f020316

.field public static final emoji_1f23a_32:I = 0x7f020317

.field public static final emoji_1f250_32:I = 0x7f020318

.field public static final emoji_1f300_32:I = 0x7f020319

.field public static final emoji_1f302_32:I = 0x7f02031a

.field public static final emoji_1f303_32:I = 0x7f02031b

.field public static final emoji_1f304_32:I = 0x7f02031c

.field public static final emoji_1f305_32:I = 0x7f02031d

.field public static final emoji_1f306_32:I = 0x7f02031e

.field public static final emoji_1f307_32:I = 0x7f02031f

.field public static final emoji_1f308_32:I = 0x7f020320

.field public static final emoji_1f30a_32:I = 0x7f020321

.field public static final emoji_1f319_32:I = 0x7f020322

.field public static final emoji_1f31f_32:I = 0x7f020323

.field public static final emoji_1f331_32:I = 0x7f020324

.field public static final emoji_1f334_32:I = 0x7f020325

.field public static final emoji_1f335_32:I = 0x7f020326

.field public static final emoji_1f337_32:I = 0x7f020327

.field public static final emoji_1f338_32:I = 0x7f020328

.field public static final emoji_1f339_32:I = 0x7f020329

.field public static final emoji_1f33a_32:I = 0x7f02032a

.field public static final emoji_1f33b_32:I = 0x7f02032b

.field public static final emoji_1f33e_32:I = 0x7f02032c

.field public static final emoji_1f340_32:I = 0x7f02032d

.field public static final emoji_1f341_32:I = 0x7f02032e

.field public static final emoji_1f342_32:I = 0x7f02032f

.field public static final emoji_1f343_32:I = 0x7f020330

.field public static final emoji_1f345_32:I = 0x7f020331

.field public static final emoji_1f346_32:I = 0x7f020332

.field public static final emoji_1f349_32:I = 0x7f020333

.field public static final emoji_1f34a_32:I = 0x7f020334

.field public static final emoji_1f34e_32:I = 0x7f020335

.field public static final emoji_1f353_32:I = 0x7f020336

.field public static final emoji_1f354_32:I = 0x7f020337

.field public static final emoji_1f358_32:I = 0x7f020338

.field public static final emoji_1f359_32:I = 0x7f020339

.field public static final emoji_1f35a_32:I = 0x7f02033a

.field public static final emoji_1f35b_32:I = 0x7f02033b

.field public static final emoji_1f35c_32:I = 0x7f02033c

.field public static final emoji_1f35d_32:I = 0x7f02033d

.field public static final emoji_1f35e_32:I = 0x7f02033e

.field public static final emoji_1f35f_32:I = 0x7f02033f

.field public static final emoji_1f361_32:I = 0x7f020340

.field public static final emoji_1f362_32:I = 0x7f020341

.field public static final emoji_1f363_32:I = 0x7f020342

.field public static final emoji_1f366_32:I = 0x7f020343

.field public static final emoji_1f367_32:I = 0x7f020344

.field public static final emoji_1f370_32:I = 0x7f020345

.field public static final emoji_1f371_32:I = 0x7f020346

.field public static final emoji_1f372_32:I = 0x7f020347

.field public static final emoji_1f373_32:I = 0x7f020348

.field public static final emoji_1f374_32:I = 0x7f020349

.field public static final emoji_1f375_32:I = 0x7f02034a

.field public static final emoji_1f376_32:I = 0x7f02034b

.field public static final emoji_1f378_32:I = 0x7f02034c

.field public static final emoji_1f37a_32:I = 0x7f02034d

.field public static final emoji_1f37b_32:I = 0x7f02034e

.field public static final emoji_1f380_32:I = 0x7f02034f

.field public static final emoji_1f381_32:I = 0x7f020350

.field public static final emoji_1f383_32:I = 0x7f020351

.field public static final emoji_1f384_32:I = 0x7f020352

.field public static final emoji_1f385_32:I = 0x7f020353

.field public static final emoji_1f388_32:I = 0x7f020354

.field public static final emoji_1f389_32:I = 0x7f020355

.field public static final emoji_1f38c_32:I = 0x7f020356

.field public static final emoji_1f38d_32:I = 0x7f020357

.field public static final emoji_1f38e_32:I = 0x7f020358

.field public static final emoji_1f38f_32:I = 0x7f020359

.field public static final emoji_1f390_32:I = 0x7f02035a

.field public static final emoji_1f392_32:I = 0x7f02035b

.field public static final emoji_1f393_32:I = 0x7f02035c

.field public static final emoji_1f3a1_32:I = 0x7f02035d

.field public static final emoji_1f3a2_32:I = 0x7f02035e

.field public static final emoji_1f3a4_32:I = 0x7f02035f

.field public static final emoji_1f3a5_32:I = 0x7f020360

.field public static final emoji_1f3a6_32:I = 0x7f020361

.field public static final emoji_1f3a7_32:I = 0x7f020362

.field public static final emoji_1f3a8_32:I = 0x7f020363

.field public static final emoji_1f3a9_32:I = 0x7f020364

.field public static final emoji_1f3ab_32:I = 0x7f020365

.field public static final emoji_1f3ac_32:I = 0x7f020366

.field public static final emoji_1f3ad_32:I = 0x7f020367

.field public static final emoji_1f3af_32:I = 0x7f020368

.field public static final emoji_1f3b1_32:I = 0x7f020369

.field public static final emoji_1f3b5_32:I = 0x7f02036a

.field public static final emoji_1f3b6_32:I = 0x7f02036b

.field public static final emoji_1f3b7_32:I = 0x7f02036c

.field public static final emoji_1f3b8_32:I = 0x7f02036d

.field public static final emoji_1f3ba_32:I = 0x7f02036e

.field public static final emoji_1f3bc_32:I = 0x7f02036f

.field public static final emoji_1f3be_32:I = 0x7f020370

.field public static final emoji_1f3bf_32:I = 0x7f020371

.field public static final emoji_1f3c0_32:I = 0x7f020372

.field public static final emoji_1f3c1_32:I = 0x7f020373

.field public static final emoji_1f3c6_32:I = 0x7f020374

.field public static final emoji_1f3c8_32:I = 0x7f020375

.field public static final emoji_1f3e0_32:I = 0x7f020376

.field public static final emoji_1f3e1_32:I = 0x7f020377

.field public static final emoji_1f3e2_32:I = 0x7f020378

.field public static final emoji_1f3e3_32:I = 0x7f020379

.field public static final emoji_1f3e5_32:I = 0x7f02037a

.field public static final emoji_1f3e6_32:I = 0x7f02037b

.field public static final emoji_1f3e7_32:I = 0x7f02037c

.field public static final emoji_1f3e8_32:I = 0x7f02037d

.field public static final emoji_1f3e9_32:I = 0x7f02037e

.field public static final emoji_1f3ea_32:I = 0x7f02037f

.field public static final emoji_1f3eb_32:I = 0x7f020380

.field public static final emoji_1f3ec_32:I = 0x7f020381

.field public static final emoji_1f3ed_32:I = 0x7f020382

.field public static final emoji_1f3ef_32:I = 0x7f020383

.field public static final emoji_1f3f0_32:I = 0x7f020384

.field public static final emoji_1f40d_32:I = 0x7f020385

.field public static final emoji_1f40e_32:I = 0x7f020386

.field public static final emoji_1f411_32:I = 0x7f020387

.field public static final emoji_1f412_32:I = 0x7f020388

.field public static final emoji_1f414_32:I = 0x7f020389

.field public static final emoji_1f417_32:I = 0x7f02038a

.field public static final emoji_1f418_32:I = 0x7f02038b

.field public static final emoji_1f419_32:I = 0x7f02038c

.field public static final emoji_1f41a_32:I = 0x7f02038d

.field public static final emoji_1f41b_32:I = 0x7f02038e

.field public static final emoji_1f41f_32:I = 0x7f02038f

.field public static final emoji_1f420_32:I = 0x7f020390

.field public static final emoji_1f421_32:I = 0x7f020391

.field public static final emoji_1f425_32:I = 0x7f020392

.field public static final emoji_1f426_32:I = 0x7f020393

.field public static final emoji_1f427_32:I = 0x7f020394

.field public static final emoji_1f428_32:I = 0x7f020395

.field public static final emoji_1f429_32:I = 0x7f020396

.field public static final emoji_1f42b_32:I = 0x7f020397

.field public static final emoji_1f42c_32:I = 0x7f020398

.field public static final emoji_1f42d_32:I = 0x7f020399

.field public static final emoji_1f42e_32:I = 0x7f02039a

.field public static final emoji_1f42f_32:I = 0x7f02039b

.field public static final emoji_1f430_32:I = 0x7f02039c

.field public static final emoji_1f431_32:I = 0x7f02039d

.field public static final emoji_1f433_32:I = 0x7f02039e

.field public static final emoji_1f434_32:I = 0x7f02039f

.field public static final emoji_1f435_32:I = 0x7f0203a0

.field public static final emoji_1f436_32:I = 0x7f0203a1

.field public static final emoji_1f437_32:I = 0x7f0203a2

.field public static final emoji_1f438_32:I = 0x7f0203a3

.field public static final emoji_1f439_32:I = 0x7f0203a4

.field public static final emoji_1f43a_32:I = 0x7f0203a5

.field public static final emoji_1f43b_32:I = 0x7f0203a6

.field public static final emoji_1f43e_32:I = 0x7f0203a7

.field public static final emoji_1f440_32:I = 0x7f0203a8

.field public static final emoji_1f442_32:I = 0x7f0203a9

.field public static final emoji_1f443_32:I = 0x7f0203aa

.field public static final emoji_1f444_32:I = 0x7f0203ab

.field public static final emoji_1f445_32:I = 0x7f0203ac

.field public static final emoji_1f446_32:I = 0x7f0203ad

.field public static final emoji_1f447_32:I = 0x7f0203ae

.field public static final emoji_1f448_32:I = 0x7f0203af

.field public static final emoji_1f449_32:I = 0x7f0203b0

.field public static final emoji_1f44a_32:I = 0x7f0203b1

.field public static final emoji_1f44b_32:I = 0x7f0203b2

.field public static final emoji_1f44c_32:I = 0x7f0203b3

.field public static final emoji_1f44d_32:I = 0x7f0203b4

.field public static final emoji_1f44e_32:I = 0x7f0203b5

.field public static final emoji_1f44f_32:I = 0x7f0203b6

.field public static final emoji_1f450_32:I = 0x7f0203b7

.field public static final emoji_1f451_32:I = 0x7f0203b8

.field public static final emoji_1f452_32:I = 0x7f0203b9

.field public static final emoji_1f454_32:I = 0x7f0203ba

.field public static final emoji_1f455_32:I = 0x7f0203bb

.field public static final emoji_1f457_32:I = 0x7f0203bc

.field public static final emoji_1f458_32:I = 0x7f0203bd

.field public static final emoji_1f459_32:I = 0x7f0203be

.field public static final emoji_1f45c_32:I = 0x7f0203bf

.field public static final emoji_1f45f_32:I = 0x7f0203c0

.field public static final emoji_1f460_32:I = 0x7f0203c1

.field public static final emoji_1f461_32:I = 0x7f0203c2

.field public static final emoji_1f462_32:I = 0x7f0203c3

.field public static final emoji_1f466_32:I = 0x7f0203c4

.field public static final emoji_1f467_32:I = 0x7f0203c5

.field public static final emoji_1f468_32:I = 0x7f0203c6

.field public static final emoji_1f469_32:I = 0x7f0203c7

.field public static final emoji_1f46b_32:I = 0x7f0203c8

.field public static final emoji_1f46e_32:I = 0x7f0203c9

.field public static final emoji_1f46f_32:I = 0x7f0203ca

.field public static final emoji_1f471_32:I = 0x7f0203cb

.field public static final emoji_1f472_32:I = 0x7f0203cc

.field public static final emoji_1f473_32:I = 0x7f0203cd

.field public static final emoji_1f474_32:I = 0x7f0203ce

.field public static final emoji_1f475_32:I = 0x7f0203cf

.field public static final emoji_1f476_32:I = 0x7f0203d0

.field public static final emoji_1f477_32:I = 0x7f0203d1

.field public static final emoji_1f478_32:I = 0x7f0203d2

.field public static final emoji_1f47b_32:I = 0x7f0203d3

.field public static final emoji_1f47c_32:I = 0x7f0203d4

.field public static final emoji_1f47d_32:I = 0x7f0203d5

.field public static final emoji_1f47e_32:I = 0x7f0203d6

.field public static final emoji_1f47f_32:I = 0x7f0203d7

.field public static final emoji_1f480_32:I = 0x7f0203d8

.field public static final emoji_1f482_32:I = 0x7f0203d9

.field public static final emoji_1f483_32:I = 0x7f0203da

.field public static final emoji_1f484_32:I = 0x7f0203db

.field public static final emoji_1f485_32:I = 0x7f0203dc

.field public static final emoji_1f488_32:I = 0x7f0203dd

.field public static final emoji_1f489_32:I = 0x7f0203de

.field public static final emoji_1f48a_32:I = 0x7f0203df

.field public static final emoji_1f48b_32:I = 0x7f0203e0

.field public static final emoji_1f48c_32:I = 0x7f0203e1

.field public static final emoji_1f48d_32:I = 0x7f0203e2

.field public static final emoji_1f48e_32:I = 0x7f0203e3

.field public static final emoji_1f48f_32:I = 0x7f0203e4

.field public static final emoji_1f490_32:I = 0x7f0203e5

.field public static final emoji_1f491_32:I = 0x7f0203e6

.field public static final emoji_1f492_32:I = 0x7f0203e7

.field public static final emoji_1f493_32:I = 0x7f0203e8

.field public static final emoji_1f494_32:I = 0x7f0203e9

.field public static final emoji_1f496_32:I = 0x7f0203ea

.field public static final emoji_1f497_32:I = 0x7f0203eb

.field public static final emoji_1f498_32:I = 0x7f0203ec

.field public static final emoji_1f499_32:I = 0x7f0203ed

.field public static final emoji_1f49a_32:I = 0x7f0203ee

.field public static final emoji_1f49b_32:I = 0x7f0203ef

.field public static final emoji_1f49c_32:I = 0x7f0203f0

.field public static final emoji_1f49d_32:I = 0x7f0203f1

.field public static final emoji_1f49e_32:I = 0x7f0203f2

.field public static final emoji_1f49f_32:I = 0x7f0203f3

.field public static final emoji_1f4a0_32:I = 0x7f0203f4

.field public static final emoji_1f4a1_32:I = 0x7f0203f5

.field public static final emoji_1f4a2_32:I = 0x7f0203f6

.field public static final emoji_1f4a3_32:I = 0x7f0203f7

.field public static final emoji_1f4a4_32:I = 0x7f0203f8

.field public static final emoji_1f4a6_32:I = 0x7f0203f9

.field public static final emoji_1f4a7_32:I = 0x7f0203fa

.field public static final emoji_1f4a8_32:I = 0x7f0203fb

.field public static final emoji_1f4a9_32:I = 0x7f0203fc

.field public static final emoji_1f4aa_32:I = 0x7f0203fd

.field public static final emoji_1f4b0_32:I = 0x7f0203fe

.field public static final emoji_1f4b2_32:I = 0x7f0203ff

.field public static final emoji_1f4b4_32:I = 0x7f020400

.field public static final emoji_1f4b5_32:I = 0x7f020401

.field public static final emoji_1f4ba_32:I = 0x7f020402

.field public static final emoji_1f4bb_32:I = 0x7f020403

.field public static final emoji_1f4bc_32:I = 0x7f020404

.field public static final emoji_1f4bd_32:I = 0x7f020405

.field public static final emoji_1f4be_32:I = 0x7f020406

.field public static final emoji_1f4bf_32:I = 0x7f020407

.field public static final emoji_1f4c0_32:I = 0x7f020408

.field public static final emoji_1f4d6_32:I = 0x7f020409

.field public static final emoji_1f4dd_32:I = 0x7f02040a

.field public static final emoji_1f4de_32:I = 0x7f02040b

.field public static final emoji_1f4e0_32:I = 0x7f02040c

.field public static final emoji_1f4e1_32:I = 0x7f02040d

.field public static final emoji_1f4e2_32:I = 0x7f02040e

.field public static final emoji_1f4e3_32:I = 0x7f02040f

.field public static final emoji_1f4e8_32:I = 0x7f020410

.field public static final emoji_1f4e9_32:I = 0x7f020411

.field public static final emoji_1f4ea_32:I = 0x7f020412

.field public static final emoji_1f4eb_32:I = 0x7f020413

.field public static final emoji_1f4ec_32:I = 0x7f020414

.field public static final emoji_1f4ed_32:I = 0x7f020415

.field public static final emoji_1f4ee_32:I = 0x7f020416

.field public static final emoji_1f4f1_32:I = 0x7f020417

.field public static final emoji_1f4f2_32:I = 0x7f020418

.field public static final emoji_1f4f3_32:I = 0x7f020419

.field public static final emoji_1f4f4_32:I = 0x7f02041a

.field public static final emoji_1f4f6_32:I = 0x7f02041b

.field public static final emoji_1f4f7_32:I = 0x7f02041c

.field public static final emoji_1f4fa_32:I = 0x7f02041d

.field public static final emoji_1f4fb_32:I = 0x7f02041e

.field public static final emoji_1f4fc_32:I = 0x7f02041f

.field public static final emoji_1f508_32:I = 0x7f020420

.field public static final emoji_1f50d_32:I = 0x7f020421

.field public static final emoji_1f50e_32:I = 0x7f020422

.field public static final emoji_1f50f_32:I = 0x7f020423

.field public static final emoji_1f510_32:I = 0x7f020424

.field public static final emoji_1f511_32:I = 0x7f020425

.field public static final emoji_1f512_32:I = 0x7f020426

.field public static final emoji_1f513_32:I = 0x7f020427

.field public static final emoji_1f514_32:I = 0x7f020428

.field public static final emoji_1f51e_32:I = 0x7f020429

.field public static final emoji_1f525_32:I = 0x7f02042a

.field public static final emoji_1f528_32:I = 0x7f02042b

.field public static final emoji_1f52b_32:I = 0x7f02042c

.field public static final emoji_1f530_32:I = 0x7f02042d

.field public static final emoji_1f531_32:I = 0x7f02042e

.field public static final emoji_1f532_32:I = 0x7f02042f

.field public static final emoji_1f533_32:I = 0x7f020430

.field public static final emoji_1f534_32:I = 0x7f020431

.field public static final emoji_1f535_32:I = 0x7f020432

.field public static final emoji_1f536_32:I = 0x7f020433

.field public static final emoji_1f537_32:I = 0x7f020434

.field public static final emoji_1f538_32:I = 0x7f020435

.field public static final emoji_1f539_32:I = 0x7f020436

.field public static final emoji_1f5fb_32:I = 0x7f020437

.field public static final emoji_1f5fc_32:I = 0x7f020438

.field public static final emoji_1f5fd_32:I = 0x7f020439

.field public static final emoji_1f601_32:I = 0x7f02043a

.field public static final emoji_1f602_32:I = 0x7f02043b

.field public static final emoji_1f603_32:I = 0x7f02043c

.field public static final emoji_1f604_32:I = 0x7f02043d

.field public static final emoji_1f606_32:I = 0x7f02043e

.field public static final emoji_1f609_32:I = 0x7f02043f

.field public static final emoji_1f60a_32:I = 0x7f020440

.field public static final emoji_1f60b_32:I = 0x7f020441

.field public static final emoji_1f60c_32:I = 0x7f020442

.field public static final emoji_1f60d_32:I = 0x7f020443

.field public static final emoji_1f60f_32:I = 0x7f020444

.field public static final emoji_1f612_32:I = 0x7f020445

.field public static final emoji_1f613_32:I = 0x7f020446

.field public static final emoji_1f614_32:I = 0x7f020447

.field public static final emoji_1f615_32:I = 0x7f020448

.field public static final emoji_1f616_32:I = 0x7f020449

.field public static final emoji_1f618_32:I = 0x7f02044a

.field public static final emoji_1f61a_32:I = 0x7f02044b

.field public static final emoji_1f61c_32:I = 0x7f02044c

.field public static final emoji_1f61d_32:I = 0x7f02044d

.field public static final emoji_1f61e_32:I = 0x7f02044e

.field public static final emoji_1f620_32:I = 0x7f02044f

.field public static final emoji_1f621_32:I = 0x7f020450

.field public static final emoji_1f622_32:I = 0x7f020451

.field public static final emoji_1f623_32:I = 0x7f020452

.field public static final emoji_1f624_32:I = 0x7f020453

.field public static final emoji_1f625_32:I = 0x7f020454

.field public static final emoji_1f628_32:I = 0x7f020455

.field public static final emoji_1f629_32:I = 0x7f020456

.field public static final emoji_1f62a_32:I = 0x7f020457

.field public static final emoji_1f62b_32:I = 0x7f020458

.field public static final emoji_1f62d_32:I = 0x7f020459

.field public static final emoji_1f630_32:I = 0x7f02045a

.field public static final emoji_1f631_32:I = 0x7f02045b

.field public static final emoji_1f632_32:I = 0x7f02045c

.field public static final emoji_1f633_32:I = 0x7f02045d

.field public static final emoji_1f635_32:I = 0x7f02045e

.field public static final emoji_1f637_32:I = 0x7f02045f

.field public static final emoji_1f638_32:I = 0x7f020460

.field public static final emoji_1f639_32:I = 0x7f020461

.field public static final emoji_1f63a_32:I = 0x7f020462

.field public static final emoji_1f63b_32:I = 0x7f020463

.field public static final emoji_1f63c_32:I = 0x7f020464

.field public static final emoji_1f63d_32:I = 0x7f020465

.field public static final emoji_1f63f_32:I = 0x7f020466

.field public static final emoji_1f640_32:I = 0x7f020467

.field public static final emoji_1f64b_32:I = 0x7f020468

.field public static final emoji_1f64c_32:I = 0x7f020469

.field public static final emoji_1f64d_32:I = 0x7f02046a

.field public static final emoji_1f64f_32:I = 0x7f02046b

.field public static final emoji_1f680_32:I = 0x7f02046c

.field public static final emoji_1f683_32:I = 0x7f02046d

.field public static final emoji_1f684_32:I = 0x7f02046e

.field public static final emoji_1f685_32:I = 0x7f02046f

.field public static final emoji_1f689_32:I = 0x7f020470

.field public static final emoji_1f68c_32:I = 0x7f020471

.field public static final emoji_1f68f_32:I = 0x7f020472

.field public static final emoji_1f691_32:I = 0x7f020473

.field public static final emoji_1f692_32:I = 0x7f020474

.field public static final emoji_1f693_32:I = 0x7f020475

.field public static final emoji_1f695_32:I = 0x7f020476

.field public static final emoji_1f697_32:I = 0x7f020477

.field public static final emoji_1f699_32:I = 0x7f020478

.field public static final emoji_1f69a_32:I = 0x7f020479

.field public static final emoji_1f6a2_32:I = 0x7f02047a

.field public static final emoji_1f6a4_32:I = 0x7f02047b

.field public static final emoji_1f6a5_32:I = 0x7f02047c

.field public static final emoji_1f6a7_32:I = 0x7f02047d

.field public static final emoji_1f6ac_32:I = 0x7f02047e

.field public static final emoji_1f6ad_32:I = 0x7f02047f

.field public static final emoji_1f6b2_32:I = 0x7f020480

.field public static final emoji_1f6b9_32:I = 0x7f020481

.field public static final emoji_1f6ba_32:I = 0x7f020482

.field public static final emoji_1f6bb_32:I = 0x7f020483

.field public static final emoji_1f6bc_32:I = 0x7f020484

.field public static final emoji_1f6bd_32:I = 0x7f020485

.field public static final emoji_1f6be_32:I = 0x7f020486

.field public static final emoji_1f6c0_32:I = 0x7f020487

.field public static final emoji_2196_32:I = 0x7f020488

.field public static final emoji_2197_32:I = 0x7f020489

.field public static final emoji_2198_32:I = 0x7f02048a

.field public static final emoji_2199_32:I = 0x7f02048b

.field public static final emoji_23_20e3_32:I = 0x7f02048c

.field public static final emoji_25aa_32:I = 0x7f02048d

.field public static final emoji_25ab_32:I = 0x7f02048e

.field public static final emoji_25fb_32:I = 0x7f02048f

.field public static final emoji_25fc_32:I = 0x7f020490

.field public static final emoji_25fd_32:I = 0x7f020491

.field public static final emoji_25fe_32:I = 0x7f020492

.field public static final emoji_2600_32:I = 0x7f020493

.field public static final emoji_2601_32:I = 0x7f020494

.field public static final emoji_260e_32:I = 0x7f020495

.field public static final emoji_2614_32:I = 0x7f020496

.field public static final emoji_2615_32:I = 0x7f020497

.field public static final emoji_261d_32:I = 0x7f020498

.field public static final emoji_263a_32:I = 0x7f020499

.field public static final emoji_2660_32:I = 0x7f02049a

.field public static final emoji_2663_32:I = 0x7f02049b

.field public static final emoji_2665_32:I = 0x7f02049c

.field public static final emoji_2666_32:I = 0x7f02049d

.field public static final emoji_2668_32:I = 0x7f02049e

.field public static final emoji_267f_32:I = 0x7f02049f

.field public static final emoji_26a0_32:I = 0x7f0204a0

.field public static final emoji_26a1_32:I = 0x7f0204a1

.field public static final emoji_26aa_32:I = 0x7f0204a2

.field public static final emoji_26ab_32:I = 0x7f0204a3

.field public static final emoji_26bd_32:I = 0x7f0204a4

.field public static final emoji_26be_32:I = 0x7f0204a5

.field public static final emoji_26c4_32:I = 0x7f0204a6

.field public static final emoji_26d4_32:I = 0x7f0204a7

.field public static final emoji_26ea_32:I = 0x7f0204a8

.field public static final emoji_26f2_32:I = 0x7f0204a9

.field public static final emoji_26f3_32:I = 0x7f0204aa

.field public static final emoji_26f5_32:I = 0x7f0204ab

.field public static final emoji_26fa_32:I = 0x7f0204ac

.field public static final emoji_26fd_32:I = 0x7f0204ad

.field public static final emoji_2702_32:I = 0x7f0204ae

.field public static final emoji_2708_32:I = 0x7f0204af

.field public static final emoji_2709_32:I = 0x7f0204b0

.field public static final emoji_270a_32:I = 0x7f0204b1

.field public static final emoji_270b_32:I = 0x7f0204b2

.field public static final emoji_270c_32:I = 0x7f0204b3

.field public static final emoji_2716_32:I = 0x7f0204b4

.field public static final emoji_2728_32:I = 0x7f0204b5

.field public static final emoji_2733_32:I = 0x7f0204b6

.field public static final emoji_2734_32:I = 0x7f0204b7

.field public static final emoji_274c_32:I = 0x7f0204b8

.field public static final emoji_274e_32:I = 0x7f0204b9

.field public static final emoji_2753_32:I = 0x7f0204ba

.field public static final emoji_2754_32:I = 0x7f0204bb

.field public static final emoji_2755_32:I = 0x7f0204bc

.field public static final emoji_2757_32:I = 0x7f0204bd

.field public static final emoji_2764_32:I = 0x7f0204be

.field public static final emoji_27a1_32:I = 0x7f0204bf

.field public static final emoji_27bf_32:I = 0x7f0204c0

.field public static final emoji_2934_32:I = 0x7f0204c1

.field public static final emoji_2935_32:I = 0x7f0204c2

.field public static final emoji_2b05_32:I = 0x7f0204c3

.field public static final emoji_2b06_32:I = 0x7f0204c4

.field public static final emoji_2b07_32:I = 0x7f0204c5

.field public static final emoji_2b1b_32:I = 0x7f0204c6

.field public static final emoji_2b1c_32:I = 0x7f0204c7

.field public static final emoji_2b50_32:I = 0x7f0204c8

.field public static final emoji_2b55_32:I = 0x7f0204c9

.field public static final emoji_303d_32:I = 0x7f0204ca

.field public static final emoji_30_20e3_32:I = 0x7f0204cb

.field public static final emoji_31_20e3_32:I = 0x7f0204cc

.field public static final emoji_3297_32:I = 0x7f0204cd

.field public static final emoji_3299_32:I = 0x7f0204ce

.field public static final emoji_32_20e3_32:I = 0x7f0204cf

.field public static final emoji_33_20e3_32:I = 0x7f0204d0

.field public static final emoji_34_20e3_32:I = 0x7f0204d1

.field public static final emoji_35_20e3_32:I = 0x7f0204d2

.field public static final emoji_36_20e3_32:I = 0x7f0204d3

.field public static final emoji_37_20e3_32:I = 0x7f0204d4

.field public static final emoji_38_20e3_32:I = 0x7f0204d5

.field public static final emoji_39_20e3_32:I = 0x7f0204d6

.field public static final emoji_bell_neue_off:I = 0x7f0204d7

.field public static final emoji_bell_neue_on:I = 0x7f0204d8

.field public static final emoji_bell_off:I = 0x7f0204d9

.field public static final emoji_bell_on:I = 0x7f0204da

.field public static final emoji_blank_32:I = 0x7f0204db

.field public static final emoji_car_neue_off:I = 0x7f0204dc

.field public static final emoji_car_neue_on:I = 0x7f0204dd

.field public static final emoji_car_off:I = 0x7f0204de

.field public static final emoji_car_on:I = 0x7f0204df

.field public static final emoji_category_cars:I = 0x7f0204e0

.field public static final emoji_category_cars_neue:I = 0x7f0204e1

.field public static final emoji_category_nature:I = 0x7f0204e2

.field public static final emoji_category_nature_neue:I = 0x7f0204e3

.field public static final emoji_category_objects:I = 0x7f0204e4

.field public static final emoji_category_objects_neue:I = 0x7f0204e5

.field public static final emoji_category_people:I = 0x7f0204e6

.field public static final emoji_category_people_neue:I = 0x7f0204e7

.field public static final emoji_category_punctuation:I = 0x7f0204e8

.field public static final emoji_category_punctuation_neue:I = 0x7f0204e9

.field public static final emoji_emoticon_confused:I = 0x7f0204ea

.field public static final emoji_emoticon_confused_rev:I = 0x7f0204eb

.field public static final emoji_f0000_32:I = 0x7f0204ec

.field public static final emoji_flower_neue_off:I = 0x7f0204ed

.field public static final emoji_flower_neue_on:I = 0x7f0204ee

.field public static final emoji_flower_off:I = 0x7f0204ef

.field public static final emoji_flower_on:I = 0x7f0204f0

.field public static final emoji_smiley_neue_off:I = 0x7f0204f1

.field public static final emoji_smiley_neue_on:I = 0x7f0204f2

.field public static final emoji_smiley_off:I = 0x7f0204f3

.field public static final emoji_smiley_on:I = 0x7f0204f4

.field public static final emoji_symbol_neue_off:I = 0x7f0204f5

.field public static final emoji_symbol_neue_on:I = 0x7f0204f6

.field public static final emoji_symbol_off:I = 0x7f0204f7

.field public static final emoji_symbol_on:I = 0x7f0204f8

.field public static final empty:I = 0x7f0204f9

.field public static final empty_circle:I = 0x7f0204fa

.field public static final entitycards_card_background:I = 0x7f0204fb

.field public static final envelope_blue_l:I = 0x7f0204fc

.field public static final envelope_dark_grey_l:I = 0x7f0204fd

.field public static final envelope_light_grey_l:I = 0x7f0204fe

.field public static final event:I = 0x7f0204ff

.field public static final event_artist_picture_background:I = 0x7f020500

.field public static final event_card_action_button:I = 0x7f020501

.field public static final event_card_going_selected:I = 0x7f020502

.field public static final event_card_maybe_selected:I = 0x7f020503

.field public static final event_card_notgoing_selected:I = 0x7f020504

.field public static final event_card_null_guest_status:I = 0x7f020505

.field public static final event_coverphoto_vignette_bg:I = 0x7f020506

.field public static final event_empty_cover_photo_bg:I = 0x7f020507

.field public static final event_feed_type_button_bg:I = 0x7f020508

.field public static final event_glyph_invite:I = 0x7f020509

.field public static final event_glyph_placepin:I = 0x7f02050a

.field public static final event_glyph_tickets:I = 0x7f02050b

.field public static final event_glyph_time:I = 0x7f02050c

.field public static final event_permalink_headershadow:I = 0x7f02050d

.field public static final event_profile_overlay_bg:I = 0x7f02050e

.field public static final events_dashboard_empty_hosting:I = 0x7f02050f

.field public static final events_dashboard_empty_invited:I = 0x7f020510

.field public static final events_dashboard_empty_past:I = 0x7f020511

.field public static final events_dashboard_empty_saved:I = 0x7f020512

.field public static final events_dashboard_empty_upcoming:I = 0x7f020513

.field public static final events_dashboard_no_events_image:I = 0x7f020514

.field public static final events_dashboard_options_button:I = 0x7f020515

.field public static final events_dashboard_row_inline_rsvp:I = 0x7f020516

.field public static final events_dashboard_row_inline_rsvp_view_background:I = 0x7f020517

.field public static final events_dashboard_row_inline_rsvp_view_text_color:I = 0x7f020518

.field public static final events_dashboard_row_top_border_background:I = 0x7f020519

.field public static final events_home_launcher_icon:I = 0x7f02051a

.field public static final facebook_back:I = 0x7f02051b

.field public static final facebook_back_icon:I = 0x7f02051c

.field public static final facebook_back_pressed:I = 0x7f02051d

.field public static final facebook_badge:I = 0x7f02051e

.field public static final facebook_button_blue:I = 0x7f02051f

.field public static final facebook_button_blue_disabled:I = 0x7f020520

.field public static final facebook_button_blue_focused:I = 0x7f020521

.field public static final facebook_button_blue_focused_disabled:I = 0x7f020522

.field public static final facebook_button_blue_normal:I = 0x7f020523

.field public static final facebook_button_blue_pressed:I = 0x7f020524

.field public static final facebook_button_grey:I = 0x7f020525

.field public static final facebook_button_grey_focused:I = 0x7f020526

.field public static final facebook_button_grey_normal:I = 0x7f020527

.field public static final facebook_button_grey_pressed:I = 0x7f020528

.field public static final facebook_icon:I = 0x7f020529

.field public static final facebook_light_grey_l:I = 0x7f02052a

.field public static final facebook_white:I = 0x7f02052b

.field public static final failure:I = 0x7f02052c

.field public static final fastscroll_thumb:I = 0x7f02052d

.field public static final fastscroll_thumb_default:I = 0x7f02052e

.field public static final fastscroll_thumb_pressed:I = 0x7f02052f

.field public static final fb_app_chat:I = 0x7f020530

.field public static final fb_app_events:I = 0x7f020531

.field public static final fb_app_friends:I = 0x7f020532

.field public static final fb_app_generator:I = 0x7f020533

.field public static final fb_app_help:I = 0x7f020534

.field public static final fb_app_messages:I = 0x7f020535

.field public static final fb_app_mobile_data:I = 0x7f020536

.field public static final fb_app_nearby:I = 0x7f020537

.field public static final fb_app_newsfeed:I = 0x7f020538

.field public static final fb_app_photos:I = 0x7f020539

.field public static final fb_icon:I = 0x7f02053a

.field public static final fb_logo_large:I = 0x7f02053b

.field public static final fb_progress_horizontal:I = 0x7f02053c

.field public static final fb_progress_horizontal_inverse:I = 0x7f02053d

.field public static final fb_progress_indeterminate_horizontal:I = 0x7f02053e

.field public static final fb_progress_indeterminate_horizontal_inverse:I = 0x7f02053f

.field public static final fb_video_seekbar_indeterminate_horizontal:I = 0x7f020540

.field public static final fb_video_seekbar_progress_horizontal:I = 0x7f020541

.field public static final fb_white_icon:I = 0x7f020542

.field public static final fbc_actionbar_44_button:I = 0x7f020543

.field public static final fbc_actionbar_44_button_highlighted:I = 0x7f020544

.field public static final fbc_actionbar_44_centerbutton:I = 0x7f020545

.field public static final fbc_actionbar_44_centerbutton_highlighted:I = 0x7f020546

.field public static final fbc_actionbar_44_leftbutton:I = 0x7f020547

.field public static final fbc_actionbar_44_leftbutton_highlighted:I = 0x7f020548

.field public static final fbc_actionbar_44_rightbutton:I = 0x7f020549

.field public static final fbc_actionbar_44_rightbutton_highlighted:I = 0x7f02054a

.field public static final fbc_actionbar_52_button:I = 0x7f02054b

.field public static final fbc_actionbar_52_button_highlighted:I = 0x7f02054c

.field public static final fbc_actionbar_52_centerbutton:I = 0x7f02054d

.field public static final fbc_actionbar_52_centerbutton_highlighted:I = 0x7f02054e

.field public static final fbc_actionbar_52_leftbutton:I = 0x7f02054f

.field public static final fbc_actionbar_52_leftbutton_highlighted:I = 0x7f020550

.field public static final fbc_actionbar_52_rightbutton:I = 0x7f020551

.field public static final fbc_actionbar_52_rightbutton_highlighted:I = 0x7f020552

.field public static final fbc_regularbutton28:I = 0x7f020553

.field public static final fbhome_icon:I = 0x7f020554

.field public static final fblogo_normal:I = 0x7f020555

.field public static final fbui_btn_dark_primary_large_bg:I = 0x7f020556

.field public static final fbui_btn_dark_primary_medium_bg:I = 0x7f020557

.field public static final fbui_btn_dark_primary_small_bg:I = 0x7f020558

.field public static final fbui_btn_dark_regular_large_bg:I = 0x7f020559

.field public static final fbui_btn_dark_regular_medium_bg:I = 0x7f02055a

.field public static final fbui_btn_dark_regular_small_bg:I = 0x7f02055b

.field public static final fbui_btn_dark_special_large_bg:I = 0x7f02055c

.field public static final fbui_btn_dark_special_medium_bg:I = 0x7f02055d

.field public static final fbui_btn_dark_special_small_bg:I = 0x7f02055e

.field public static final fbui_btn_light_primary_large_bg:I = 0x7f02055f

.field public static final fbui_btn_light_primary_medium_bg:I = 0x7f020560

.field public static final fbui_btn_light_primary_small_bg:I = 0x7f020561

.field public static final fbui_btn_light_regular_large_bg:I = 0x7f020562

.field public static final fbui_btn_light_regular_medium_bg:I = 0x7f020563

.field public static final fbui_btn_light_regular_small_bg:I = 0x7f020564

.field public static final fbui_btn_light_special_large_bg:I = 0x7f020565

.field public static final fbui_btn_light_special_medium_bg:I = 0x7f020566

.field public static final fbui_btn_light_special_small_bg:I = 0x7f020567

.field public static final fbui_card_fg:I = 0x7f020568

.field public static final fbui_check_off_dark:I = 0x7f020569

.field public static final fbui_check_off_disabled_dark:I = 0x7f02056a

.field public static final fbui_check_off_disabled_light:I = 0x7f02056b

.field public static final fbui_check_off_light:I = 0x7f02056c

.field public static final fbui_check_off_pressed_dark:I = 0x7f02056d

.field public static final fbui_check_off_pressed_light:I = 0x7f02056e

.field public static final fbui_check_on:I = 0x7f02056f

.field public static final fbui_check_on_disabled_dark:I = 0x7f020570

.field public static final fbui_check_on_disabled_light:I = 0x7f020571

.field public static final fbui_check_on_pressed_dark:I = 0x7f020572

.field public static final fbui_check_on_pressed_light:I = 0x7f020573

.field public static final fbui_checkbox_dark:I = 0x7f020574

.field public static final fbui_checkbox_light:I = 0x7f020575

.field public static final fbui_clickable_list_item_bg:I = 0x7f020576

.field public static final fbui_clickable_list_item_bg_opaque:I = 0x7f020577

.field public static final fbui_divider_horizontal:I = 0x7f020578

.field public static final fbui_divider_vertical:I = 0x7f020579

.field public static final fbui_list_divider:I = 0x7f02057a

.field public static final fbui_list_header_bg:I = 0x7f02057b

.field public static final fbui_megaphone_background:I = 0x7f02057c

.field public static final fbui_megaphone_button_background:I = 0x7f02057d

.field public static final fbui_megaphone_cross:I = 0x7f02057e

.field public static final fbui_menu_album_dark_grey_l:I = 0x7f02057f

.field public static final fbui_menu_block_dark_grey_l:I = 0x7f020580

.field public static final fbui_menu_call_dark_grey_l:I = 0x7f020581

.field public static final fbui_menu_camera_dark_grey_l:I = 0x7f020582

.field public static final fbui_menu_compose_dark_grey_l:I = 0x7f020583

.field public static final fbui_menu_copylink_dark_grey_l:I = 0x7f020584

.field public static final fbui_menu_coverphoto_dark_grey_l:I = 0x7f020585

.field public static final fbui_menu_create_dark_grey_l:I = 0x7f020586

.field public static final fbui_menu_cross_dark_grey_l:I = 0x7f020587

.field public static final fbui_menu_delete_dark_grey_l:I = 0x7f020588

.field public static final fbui_menu_download_dark_grey_l:I = 0x7f020589

.field public static final fbui_menu_edit_dark_grey_l:I = 0x7f02058a

.field public static final fbui_menu_gift_dark_grey_l:I = 0x7f02058b

.field public static final fbui_menu_hide_dark_grey_l:I = 0x7f02058c

.field public static final fbui_menu_history_dark_grey_l:I = 0x7f02058d

.field public static final fbui_menu_mail_dark_grey_l:I = 0x7f02058e

.field public static final fbui_menu_message_dark_grey_l:I = 0x7f02058f

.field public static final fbui_menu_more_dark_grey_l:I = 0x7f020590

.field public static final fbui_menu_photo_dark_grey_l:I = 0x7f020591

.field public static final fbui_menu_poke_dark_grey_l:I = 0x7f020592

.field public static final fbui_menu_privacy_acquaintances_dark_grey_l:I = 0x7f020593

.field public static final fbui_menu_privacy_closefriends_dark_grey_l:I = 0x7f020594

.field public static final fbui_menu_privacy_custom_dark_grey_l:I = 0x7f020595

.field public static final fbui_menu_privacy_facebook_dark_grey_l:I = 0x7f020596

.field public static final fbui_menu_privacy_family_dark_grey_l:I = 0x7f020597

.field public static final fbui_menu_privacy_friends_dark_grey_l:I = 0x7f020598

.field public static final fbui_menu_privacy_friends_of_friends_dark_grey_l:I = 0x7f020599

.field public static final fbui_menu_privacy_friendsexcept_dark_grey_l:I = 0x7f02059a

.field public static final fbui_menu_privacy_group_dark_grey_l:I = 0x7f02059b

.field public static final fbui_menu_privacy_location_dark_grey_l:I = 0x7f02059c

.field public static final fbui_menu_privacy_onlyme_dark_grey_l:I = 0x7f02059d

.field public static final fbui_menu_privacy_public_dark_grey_l:I = 0x7f02059e

.field public static final fbui_menu_privacy_school_dark_grey_l:I = 0x7f02059f

.field public static final fbui_menu_privacy_taggee_dark_grey_l:I = 0x7f0205a0

.field public static final fbui_menu_privacy_work_dark_grey_l:I = 0x7f0205a1

.field public static final fbui_menu_profilephoto_dark_grey_l:I = 0x7f0205a2

.field public static final fbui_menu_rss_dark_grey_l:I = 0x7f0205a3

.field public static final fbui_menu_save_dark_grey_l:I = 0x7f0205a4

.field public static final fbui_menu_share_dark_grey_l:I = 0x7f0205a5

.field public static final fbui_menu_tag_dark_grey_l:I = 0x7f0205a6

.field public static final fbui_menu_unfollow_dark_grey_l:I = 0x7f0205a7

.field public static final fbui_menu_unfriend_dark_grey_l:I = 0x7f0205a8

.field public static final fbui_menu_video_dark_grey_l:I = 0x7f0205a9

.field public static final fbui_pivotcard_photo_mask:I = 0x7f0205aa

.field public static final fbui_pivotcard_shadow:I = 0x7f0205ab

.field public static final fbui_popover_checkbox:I = 0x7f0205ac

.field public static final fbui_popover_checkmark:I = 0x7f0205ad

.field public static final fbui_popover_list_header_bg:I = 0x7f0205ae

.field public static final fbui_popover_list_view_divider:I = 0x7f0205af

.field public static final fbui_popover_list_view_selector:I = 0x7f0205b0

.field public static final fbui_pressed_list_item_bg:I = 0x7f0205b1

.field public static final fbui_progress_comet:I = 0x7f0205b2

.field public static final fbui_progress_spinner:I = 0x7f0205b3

.field public static final fbui_radio_dark:I = 0x7f0205b4

.field public static final fbui_radio_light:I = 0x7f0205b5

.field public static final fbui_radio_on_dark:I = 0x7f0205b6

.field public static final fbui_radio_on_disabled_dark:I = 0x7f0205b7

.field public static final fbui_radio_on_disabled_light:I = 0x7f0205b8

.field public static final fbui_radio_on_light:I = 0x7f0205b9

.field public static final fbui_radio_on_pressed_dark:I = 0x7f0205ba

.field public static final fbui_radio_on_pressed_light:I = 0x7f0205bb

.field public static final fbui_tabbed_view_pager_indicator_bg:I = 0x7f0205bc

.field public static final fbui_tooltip_bg:I = 0x7f0205bd

.field public static final fbui_tooltip_nub_above:I = 0x7f0205be

.field public static final fbui_tooltip_nub_below:I = 0x7f0205bf

.field public static final feed_add_photo_button_background_pressed:I = 0x7f0205c0

.field public static final feed_app_collection_button_bg:I = 0x7f0205c1

.field public static final feed_app_collection_button_checkmark:I = 0x7f0205c2

.field public static final feed_app_collection_button_checkmark_active:I = 0x7f0205c3

.field public static final feed_app_collection_button_checkmark_selector:I = 0x7f0205c4

.field public static final feed_app_collection_button_plus:I = 0x7f0205c5

.field public static final feed_app_collection_button_plus_active:I = 0x7f0205c6

.field public static final feed_app_collection_button_plus_selector:I = 0x7f0205c7

.field public static final feed_app_collection_button_selector:I = 0x7f0205c8

.field public static final feed_app_collection_pressed_button_bg:I = 0x7f0205c9

.field public static final feed_app_item_background:I = 0x7f0205ca

.field public static final feed_app_item_card_background:I = 0x7f0205cb

.field public static final feed_app_item_card_media_background:I = 0x7f0205cc

.field public static final feed_attached_story_view_background:I = 0x7f0205cd

.field public static final feed_attachment_background:I = 0x7f0205ce

.field public static final feed_attachment_background_box:I = 0x7f0205cf

.field public static final feed_attachment_background_box_pressed:I = 0x7f0205d0

.field public static final feed_call_to_action_background:I = 0x7f0205d1

.field public static final feed_call_to_action_background_pressed:I = 0x7f0205d2

.field public static final feed_call_to_action_button:I = 0x7f0205d3

.field public static final feed_card:I = 0x7f0205d4

.field public static final feed_card_placeholder:I = 0x7f0205d5

.field public static final feed_check_icon:I = 0x7f0205d6

.field public static final feed_checkin_bubble:I = 0x7f0205d7

.field public static final feed_coupon_background:I = 0x7f0205d8

.field public static final feed_coupon_background_pressed:I = 0x7f0205d9

.field public static final feed_coupon_button:I = 0x7f0205da

.field public static final feed_end:I = 0x7f0205db

.field public static final feed_error_banner:I = 0x7f0205dc

.field public static final feed_error_banner_icon:I = 0x7f0205dd

.field public static final feed_error_banner_pressed:I = 0x7f0205de

.field public static final feed_event_attachment_button_background:I = 0x7f0205df

.field public static final feed_event_attachment_button_plus:I = 0x7f0205e0

.field public static final feed_feedback_background:I = 0x7f0205e1

.field public static final feed_feedback_background_pressed:I = 0x7f0205e2

.field public static final feed_feedback_bg_left_pressed:I = 0x7f0205e3

.field public static final feed_feedback_bg_middle_pressed:I = 0x7f0205e4

.field public static final feed_feedback_bg_right_pressed:I = 0x7f0205e5

.field public static final feed_feedback_whole_button_bg_selector:I = 0x7f0205e6

.field public static final feed_flyout_comment_button_background:I = 0x7f0205e7

.field public static final feed_flyout_load_more_icon:I = 0x7f0205e8

.field public static final feed_flyout_no_comments_placeholder:I = 0x7f0205e9

.field public static final feed_friends_nearby_upsell_icon:I = 0x7f0205ea

.field public static final feed_generic_press_state_background_rounded:I = 0x7f0205eb

.field public static final feed_generic_press_state_background_squared:I = 0x7f0205ec

.field public static final feed_hidden_story_background_selector:I = 0x7f0205ed

.field public static final feed_hidden_story_generic_background:I = 0x7f0205ee

.field public static final feed_history_bg_bottom:I = 0x7f0205ef

.field public static final feed_history_bg_bottom_border:I = 0x7f0205f0

.field public static final feed_history_bg_middle:I = 0x7f0205f1

.field public static final feed_history_bg_top:I = 0x7f0205f2

.field public static final feed_history_bg_top_border:I = 0x7f0205f3

.field public static final feed_icon_exit:I = 0x7f0205f4

.field public static final feed_image_shadow:I = 0x7f0205f5

.field public static final feed_item_generic_bg_bottom:I = 0x7f0205f6

.field public static final feed_item_generic_bg_bottom_with_followup_section:I = 0x7f0205f7

.field public static final feed_item_generic_bg_feed_middle_with_top_divider:I = 0x7f0205f8

.field public static final feed_item_generic_bg_inner_bottom:I = 0x7f0205f9

.field public static final feed_item_generic_bg_inner_bottom_with_followup_section:I = 0x7f0205fa

.field public static final feed_item_generic_bg_inner_middle:I = 0x7f0205fb

.field public static final feed_item_generic_bg_inner_top:I = 0x7f0205fc

.field public static final feed_item_generic_bg_inner_whole:I = 0x7f0205fd

.field public static final feed_item_generic_bg_middle:I = 0x7f0205fe

.field public static final feed_item_generic_bg_top:I = 0x7f0205ff

.field public static final feed_like_icon:I = 0x7f020600

.field public static final feed_like_icon_angora:I = 0x7f020601

.field public static final feed_like_icon_pressed:I = 0x7f020602

.field public static final feed_like_icon_pressed_angora:I = 0x7f020603

.field public static final feed_location_text_selector:I = 0x7f020604

.field public static final feed_menu_ad_choices:I = 0x7f020605

.field public static final feed_new_stories_arrow:I = 0x7f020606

.field public static final feed_new_stories_background:I = 0x7f020607

.field public static final feed_new_stories_background_normal:I = 0x7f020608

.field public static final feed_new_stories_background_pressed:I = 0x7f020609

.field public static final feed_no_connection_background:I = 0x7f02060a

.field public static final feed_offline_header_background:I = 0x7f02060b

.field public static final feed_page_review_suggestion_blacklist:I = 0x7f02060c

.field public static final feed_permalink_bg_bottom:I = 0x7f02060d

.field public static final feed_permalink_bg_bottom_pressed:I = 0x7f02060e

.field public static final feed_permalink_bg_bottom_with_content_offsets:I = 0x7f02060f

.field public static final feed_permalink_bg_bottom_with_press_state:I = 0x7f020610

.field public static final feed_permalink_bg_middle:I = 0x7f020611

.field public static final feed_permalink_bg_middle_pressed:I = 0x7f020612

.field public static final feed_permalink_bg_middle_with_content_offsets:I = 0x7f020613

.field public static final feed_permalink_bg_middle_with_press_state:I = 0x7f020614

.field public static final feed_permalink_bg_top:I = 0x7f020615

.field public static final feed_permalink_comment_like_icon:I = 0x7f020616

.field public static final feed_permalink_feedback_with_content_bg_angora:I = 0x7f020617

.field public static final feed_permalink_feedback_without_content_bg_angora:I = 0x7f020618

.field public static final feed_permalink_middle_row_press_state:I = 0x7f020619

.field public static final feed_permalink_reply_bg_bottom:I = 0x7f02061a

.field public static final feed_permalink_reply_bg_middle:I = 0x7f02061b

.field public static final feed_permalink_story_background:I = 0x7f02061c

.field public static final feed_photo_grid_shadow:I = 0x7f02061d

.field public static final feed_plus_icon:I = 0x7f02061e

.field public static final feed_postpost_badge_bubble:I = 0x7f02061f

.field public static final feed_postpost_tagging_circle:I = 0x7f020620

.field public static final feed_postpost_tagging_icon:I = 0x7f020621

.field public static final feed_presence_icon_active:I = 0x7f020622

.field public static final feed_press_state_rounded_rect:I = 0x7f020623

.field public static final feed_product_buy_icon:I = 0x7f020624

.field public static final feed_quick_cam_progress_drawable:I = 0x7f020625

.field public static final feed_scissor:I = 0x7f020626

.field public static final feed_settings_item_background:I = 0x7f020627

.field public static final feed_story_attachment_base_bg_beta:I = 0x7f020628

.field public static final feed_story_bg:I = 0x7f020629

.field public static final feed_story_bg_bottom:I = 0x7f02062a

.field public static final feed_story_bg_bottom_with_followup_section:I = 0x7f02062b

.field public static final feed_story_bg_grey:I = 0x7f02062c

.field public static final feed_story_bg_grey_down:I = 0x7f02062d

.field public static final feed_story_bg_middle:I = 0x7f02062e

.field public static final feed_story_bg_middle_with_top_divider:I = 0x7f02062f

.field public static final feed_story_bg_top:I = 0x7f020630

.field public static final feed_story_chevron:I = 0x7f020631

.field public static final feed_story_coupon_background:I = 0x7f020632

.field public static final feed_story_item_generic_background:I = 0x7f020633

.field public static final feed_storyset_item_background_box:I = 0x7f020634

.field public static final feed_substory_feedback_background:I = 0x7f020635

.field public static final feed_survey_completed:I = 0x7f020636

.field public static final feed_swipe_pymk_end_card:I = 0x7f020637

.field public static final feed_tiled_scissor:I = 0x7f020638

.field public static final feed_topic_header_background:I = 0x7f020639

.field public static final feed_topic_header_background_pressed:I = 0x7f02063a

.field public static final feed_topic_header_background_selector:I = 0x7f02063b

.field public static final feed_topic_multi_row:I = 0x7f02063c

.field public static final feed_topic_multi_row_pressed:I = 0x7f02063d

.field public static final feed_topic_multi_row_selector:I = 0x7f02063e

.field public static final feed_transparent_background_selector:I = 0x7f02063f

.field public static final feed_upsell_footer_background:I = 0x7f020640

.field public static final feed_video_attachment_play_button:I = 0x7f020641

.field public static final feed_white_button:I = 0x7f020642

.field public static final feed_white_button_pressed:I = 0x7f020643

.field public static final filter_content_background:I = 0x7f020644

.field public static final filter_stories_menu_icon:I = 0x7f020645

.field public static final filter_types_events_icon:I = 0x7f020646

.field public static final filter_types_friend_list_icon:I = 0x7f020647

.field public static final filter_types_links_icon:I = 0x7f020648

.field public static final filter_types_most_recent:I = 0x7f020649

.field public static final filter_types_pages_icon:I = 0x7f02064a

.field public static final filter_types_photos_icon:I = 0x7f02064b

.field public static final filter_types_status_updates_icon:I = 0x7f02064c

.field public static final filter_types_top_news_icon:I = 0x7f02064d

.field public static final filter_types_videos_icon:I = 0x7f02064e

.field public static final find_friends:I = 0x7f02064f

.field public static final find_friends_action_default:I = 0x7f020650

.field public static final find_friends_action_pressed:I = 0x7f020651

.field public static final find_friends_action_selected:I = 0x7f020652

.field public static final find_friends_action_selector:I = 0x7f020653

.field public static final find_friends_add_icon:I = 0x7f020654

.field public static final find_friends_bottom_bar_icon:I = 0x7f020655

.field public static final find_friends_button:I = 0x7f020656

.field public static final find_friends_checkmark_icon:I = 0x7f020657

.field public static final find_friends_envelope_image:I = 0x7f020658

.field public static final find_friends_gray_text_color:I = 0x7f020e60

.field public static final find_friends_titlebar_icon:I = 0x7f020659

.field public static final find_friends_titlebar_icon_pressed:I = 0x7f02065a

.field public static final find_friends_titlebar_icon_state:I = 0x7f02065b

.field public static final first_time_view_button_background:I = 0x7f02065c

.field public static final first_time_view_image:I = 0x7f02065d

.field public static final fixed_divider_horizontal_bright:I = 0x7f02065e

.field public static final flickr_logo:I = 0x7f02065f

.field public static final flickr_white:I = 0x7f020660

.field public static final flyout_body_bg:I = 0x7f020661

.field public static final flyout_comment_box_top_shadow:I = 0x7f020662

.field public static final flyout_dialog_top_divider:I = 0x7f020663

.field public static final flyout_header_bottom_shadow:I = 0x7f020664

.field public static final followup_section_bg:I = 0x7f020665

.field public static final followup_section_bg_whole:I = 0x7f020666

.field public static final footer_bg_with_border:I = 0x7f020667

.field public static final footer_border:I = 0x7f020668

.field public static final friend_added_blue_l:I = 0x7f020669

.field public static final friend_added_blue_m:I = 0x7f02066a

.field public static final friend_added_dark_grey_l:I = 0x7f02066b

.field public static final friend_added_dark_grey_m:I = 0x7f02066c

.field public static final friend_added_white_l:I = 0x7f02066d

.field public static final friend_dialog_item_background:I = 0x7f02066e

.field public static final friend_dialog_item_button_background:I = 0x7f02066f

.field public static final friend_dialog_item_group_background:I = 0x7f020670

.field public static final friend_finder_progress:I = 0x7f020671

.field public static final friend_finder_search_icon:I = 0x7f020672

.field public static final friend_guy:I = 0x7f020673

.field public static final friend_request_button_blue:I = 0x7f020674

.field public static final friend_request_button_blue_pressed:I = 0x7f020675

.field public static final friend_request_button_grey:I = 0x7f020676

.field public static final friend_request_button_grey_pressed:I = 0x7f020677

.field public static final friend_request_unread_background:I = 0x7f020678

.field public static final friend_selector_search:I = 0x7f020679

.field public static final friending_button:I = 0x7f02067a

.field public static final friending_button_background:I = 0x7f02067b

.field public static final friending_button_bg:I = 0x7f02067c

.field public static final friending_button_bg_pressed:I = 0x7f02067d

.field public static final friending_button_pressed:I = 0x7f02067e

.field public static final friending_codes_cover_pic_overlay:I = 0x7f02067f

.field public static final friending_codes_try_again_button:I = 0x7f020680

.field public static final friending_glyph_add_friend:I = 0x7f020681

.field public static final friending_glyph_friends:I = 0x7f020682

.field public static final friending_radar_cover_pic_overlay:I = 0x7f020683

.field public static final friending_radar_error_dialog_bg:I = 0x7f020684

.field public static final friending_radar_nux_gradient:I = 0x7f020685

.field public static final friending_radar_progress_bar_drawable:I = 0x7f020686

.field public static final friending_radar_start_button:I = 0x7f020687

.field public static final friends_icon:I = 0x7f020688

.field public static final friends_icon_pressed:I = 0x7f020689

.field public static final friends_icon_state:I = 0x7f02068a

.field public static final friends_nearby_invite_button:I = 0x7f02068b

.field public static final friends_nearby_invite_button_normal:I = 0x7f02068c

.field public static final friends_nearby_invite_button_pressed:I = 0x7f02068d

.field public static final friends_nearby_invite_toast_card:I = 0x7f02068e

.field public static final friends_nearby_map_arrow_left:I = 0x7f02068f

.field public static final friends_nearby_map_arrow_right:I = 0x7f020690

.field public static final friends_nearby_map_button:I = 0x7f020691

.field public static final friends_nearby_map_button_normal:I = 0x7f020692

.field public static final friends_nearby_map_button_pressed:I = 0x7f020693

.field public static final friends_nearby_map_marker_border:I = 0x7f020694

.field public static final friends_nearby_map_marker_placeholder:I = 0x7f020695

.field public static final friends_nearby_map_my_location:I = 0x7f020696

.field public static final friends_nearby_ping_button_off:I = 0x7f020697

.field public static final friends_nearby_ping_button_off_normal:I = 0x7f020698

.field public static final friends_nearby_ping_button_off_pressed:I = 0x7f020699

.field public static final friends_nearby_ping_button_on:I = 0x7f02069a

.field public static final friends_nearby_ping_button_on_normal:I = 0x7f02069b

.field public static final friends_nearby_ping_button_on_pressed:I = 0x7f02069c

.field public static final friends_nearby_refresh_harrison:I = 0x7f02069d

.field public static final friends_nearby_refresh_harrison_normal:I = 0x7f02069e

.field public static final friends_nearby_refresh_harrison_pressed:I = 0x7f02069f

.field public static final friends_nearby_search:I = 0x7f0206a0

.field public static final friends_nearby_search_invite_button:I = 0x7f0206a1

.field public static final friends_nearby_search_invite_button_normal:I = 0x7f0206a2

.field public static final friends_nearby_search_invite_button_pressed:I = 0x7f0206a3

.field public static final friends_nearby_settings_harrison:I = 0x7f0206a4

.field public static final friends_nearby_settings_harrison_normal:I = 0x7f0206a5

.field public static final friends_nearby_settings_harrison_pressed:I = 0x7f0206a6

.field public static final frowncloud:I = 0x7f0206a7

.field public static final frowncloudbig:I = 0x7f0206a8

.field public static final fullscreen_pause_icon:I = 0x7f0206a9

.field public static final fullscreen_play_icon:I = 0x7f0206aa

.field public static final fullscreen_popup:I = 0x7f0206ab

.field public static final fullscreen_video_menu_button:I = 0x7f0206ac

.field public static final fullscreen_video_ufi_background:I = 0x7f0206ad

.field public static final gallery_rotate_icon:I = 0x7f0206ae

.field public static final gallery_tag:I = 0x7f0206af

.field public static final gallery_tag_active:I = 0x7f0206b0

.field public static final gallery_tag_active_pressed:I = 0x7f0206b1

.field public static final gallery_tag_icon:I = 0x7f0206b2

.field public static final gallery_tag_icon_active:I = 0x7f0206b3

.field public static final gallery_tag_pressed:I = 0x7f0206b4

.field public static final gear:I = 0x7f0206b5

.field public static final gender_button_bg_normal:I = 0x7f0206b6

.field public static final gender_button_bg_pressed:I = 0x7f0206b7

.field public static final gender_button_bg_selector:I = 0x7f0206b8

.field public static final gender_female_normal:I = 0x7f0206b9

.field public static final gender_female_pressed:I = 0x7f0206ba

.field public static final gender_female_selector:I = 0x7f0206bb

.field public static final gender_male_normal:I = 0x7f0206bc

.field public static final gender_male_pressed:I = 0x7f0206bd

.field public static final gender_male_selector:I = 0x7f0206be

.field public static final generic_pressed_state_background:I = 0x7f0206bf

.field public static final gift_card_mall_selector_text_color:I = 0x7f0206c0

.field public static final gifts_shadow_down:I = 0x7f0206c1

.field public static final gifts_shadow_up:I = 0x7f0206c2

.field public static final giftwrap_pattern:I = 0x7f0206c3

.field public static final giftwrap_tiled:I = 0x7f0206c4

.field public static final global_button_top_new:I = 0x7f0206c5

.field public static final global_top_button_left_new:I = 0x7f0206c6

.field public static final global_top_button_right:I = 0x7f0206c7

.field public static final glyph_chevron_right_light_grey_s:I = 0x7f0206c8

.field public static final grab_bag_selector_text_color:I = 0x7f0206c9

.field public static final grabbag_center_selector:I = 0x7f0206ca

.field public static final grabbag_side_selector:I = 0x7f0206cb

.field public static final graph_search_apps_icon:I = 0x7f0206cc

.field public static final graph_search_blue_plus_icon:I = 0x7f0206cd

.field public static final graph_search_books_icon:I = 0x7f0206ce

.field public static final graph_search_box_bg:I = 0x7f0206cf

.field public static final graph_search_box_icon:I = 0x7f0206d0

.field public static final graph_search_box_offline_bg:I = 0x7f0206d1

.field public static final graph_search_brown_plus_icon:I = 0x7f0206d2

.field public static final graph_search_button_pressed:I = 0x7f0206d3

.field public static final graph_search_button_unpressed:I = 0x7f0206d4

.field public static final graph_search_clear_click_target:I = 0x7f0206d5

.field public static final graph_search_edit_gear:I = 0x7f0206d6

.field public static final graph_search_edit_text_activated:I = 0x7f0206d7

.field public static final graph_search_edit_text_bg_selector:I = 0x7f0206d8

.field public static final graph_search_edit_text_default:I = 0x7f0206d9

.field public static final graph_search_edit_text_disabled:I = 0x7f0206da

.field public static final graph_search_edit_text_disabled_focused:I = 0x7f0206db

.field public static final graph_search_edit_text_focused:I = 0x7f0206dc

.field public static final graph_search_entry_button:I = 0x7f0206dd

.field public static final graph_search_filter_remove:I = 0x7f0206de

.field public static final graph_search_games_icon:I = 0x7f0206df

.field public static final graph_search_gray_plus_icon:I = 0x7f0206e0

.field public static final graph_search_green_plus_icon:I = 0x7f0206e1

.field public static final graph_search_groups_icon:I = 0x7f0206e2

.field public static final graph_search_keyword_icon:I = 0x7f0206e3

.field public static final graph_search_list_divider:I = 0x7f0206e4

.field public static final graph_search_list_divider_2dp:I = 0x7f0206e5

.field public static final graph_search_list_header_divider:I = 0x7f0206e6

.field public static final graph_search_movies_icon:I = 0x7f0206e7

.field public static final graph_search_music_icon:I = 0x7f0206e8

.field public static final graph_search_needle_people_entry:I = 0x7f0206e9

.field public static final graph_search_needle_photos_entry:I = 0x7f0206ea

.field public static final graph_search_noresults_all:I = 0x7f0206eb

.field public static final graph_search_noresults_events:I = 0x7f0206ec

.field public static final graph_search_noresults_groups:I = 0x7f0206ed

.field public static final graph_search_noresults_pages:I = 0x7f0206ee

.field public static final graph_search_noresults_people:I = 0x7f0206ef

.field public static final graph_search_orange_plus_icon:I = 0x7f0206f0

.field public static final graph_search_pages_icon:I = 0x7f0206f1

.field public static final graph_search_people_icon:I = 0x7f0206f2

.field public static final graph_search_photos_icon:I = 0x7f0206f3

.field public static final graph_search_pink_plus_icon:I = 0x7f0206f4

.field public static final graph_search_places_icon:I = 0x7f0206f5

.field public static final graph_search_posts_icon:I = 0x7f0206f6

.field public static final graph_search_qr_code_icon:I = 0x7f0206f7

.field public static final graph_search_search_edit_text_icon:I = 0x7f0206f8

.field public static final graph_search_see_more_icon:I = 0x7f0206f9

.field public static final graph_search_trending_icon:I = 0x7f0206fa

.field public static final graph_search_videos_icon:I = 0x7f0206fb

.field public static final graph_search_yellow_plus_icon:I = 0x7f0206fc

.field public static final gray_rating_star:I = 0x7f0206fd

.field public static final gray_wash_color:I = 0x7f020e52

.field public static final green_checkmark:I = 0x7f0206fe

.field public static final grey_circle:I = 0x7f0206ff

.field public static final grey_exclamation_mark:I = 0x7f020700

.field public static final grid_back:I = 0x7f020701

.field public static final grid_back_icon:I = 0x7f020702

.field public static final grid_back_pressed:I = 0x7f020703

.field public static final group:I = 0x7f020704

.field public static final group_default_icon:I = 0x7f020705

.field public static final groups_feed_header_chevron_white:I = 0x7f020706

.field public static final guest_tile_badge:I = 0x7f020707

.field public static final happy_birthday_feed_unit_balloons:I = 0x7f020708

.field public static final header_gradient:I = 0x7f020709

.field public static final header_shadow:I = 0x7f02070a

.field public static final heart_like_button_states:I = 0x7f02070b

.field public static final helpcenter_button:I = 0x7f02070c

.field public static final helpcenter_icon:I = 0x7f02070d

.field public static final helpcenter_icon_pressed:I = 0x7f02070e

.field public static final home_loading_indicator:I = 0x7f020711

.field public static final home_notification_icon:I = 0x7f020712

.field public static final home_notification_information:I = 0x7f020713

.field public static final home_settings_header:I = 0x7f020714

.field public static final home_settings_menu_icon:I = 0x7f020715

.field public static final homesetup_checkbox:I = 0x7f020716

.field public static final homesetup_checkbox_bg:I = 0x7f020717

.field public static final homesetup_checkbox_fg:I = 0x7f020718

.field public static final homesetup_checkbox_off:I = 0x7f020719

.field public static final homesetup_checkbox_on:I = 0x7f02071a

.field public static final homesetup_link_background:I = 0x7f02071b

.field public static final hscroll_white_border_filled:I = 0x7f02071c

.field public static final iab_44_center_button:I = 0x7f02071d

.field public static final iab_44_left_button:I = 0x7f02071e

.field public static final iab_44_right_button:I = 0x7f02071f

.field public static final iab_44_whole_button:I = 0x7f020720

.field public static final iab_52_center_button:I = 0x7f020721

.field public static final iab_52_left_button:I = 0x7f020722

.field public static final iab_52_right_button:I = 0x7f020723

.field public static final iab_52_whole_button:I = 0x7f020724

.field public static final ic_checkin:I = 0x7f020725

.field public static final ic_dialog_alert:I = 0x7f020726

.field public static final ic_dialog_alert_holo_light:I = 0x7f020727

.field public static final ic_dialog_info:I = 0x7f020728

.field public static final ic_edit_photo:I = 0x7f020729

.field public static final ic_facebook_selected:I = 0x7f02072a

.field public static final ic_fb_minitab_selected:I = 0x7f02072b

.field public static final ic_menu_emoticons:I = 0x7f02072c

.field public static final ic_menu_messages_archive:I = 0x7f02072d

.field public static final ic_menu_messages_delete:I = 0x7f02072e

.field public static final ic_menu_messages_forward:I = 0x7f02072f

.field public static final ic_menu_messages_move:I = 0x7f020730

.field public static final ic_menu_messages_spam:I = 0x7f020731

.field public static final ic_menu_messages_unarchive:I = 0x7f020732

.field public static final ic_menu_messages_unread:I = 0x7f020733

.field public static final ic_next_widget_default:I = 0x7f020734

.field public static final ic_next_widget_disabled:I = 0x7f020735

.field public static final ic_next_widget_pressed:I = 0x7f020736

.field public static final ic_next_widget_selected:I = 0x7f020737

.field public static final ic_photo:I = 0x7f020738

.field public static final ic_plusone_medium_off_client:I = 0x7f020739

.field public static final ic_plusone_small_off_client:I = 0x7f02073a

.field public static final ic_plusone_standard_off_client:I = 0x7f02073b

.field public static final ic_plusone_tall_off_client:I = 0x7f02073c

.field public static final ic_prev_widget_default:I = 0x7f02073d

.field public static final ic_prev_widget_disabled:I = 0x7f02073e

.field public static final ic_prev_widget_pressed:I = 0x7f02073f

.field public static final ic_prev_widget_selected:I = 0x7f020740

.field public static final ic_search_text:I = 0x7f020741

.field public static final ic_status:I = 0x7f020742

.field public static final icon_add_album:I = 0x7f020743

.field public static final icon_add_photo:I = 0x7f020744

.field public static final icon_add_tag_highlight:I = 0x7f020745

.field public static final icon_add_tag_normal:I = 0x7f020746

.field public static final icon_back_highlighted:I = 0x7f020747

.field public static final icon_back_normal:I = 0x7f020748

.field public static final icon_camera_highlight:I = 0x7f020749

.field public static final icon_camera_normal:I = 0x7f02074a

.field public static final icon_checkmark_highlight:I = 0x7f02074b

.field public static final icon_checkmark_normal:I = 0x7f02074c

.field public static final icon_checkmark_small_highlight:I = 0x7f02074d

.field public static final icon_checkmark_small_normal:I = 0x7f02074e

.field public static final icon_compose_highlight:I = 0x7f02074f

.field public static final icon_compose_normal:I = 0x7f020750

.field public static final icon_facebook:I = 0x7f020751

.field public static final icon_indicator_off:I = 0x7f020752

.field public static final icon_indicator_on:I = 0x7f020753

.field public static final icon_katana:I = 0x7f020754

.field public static final icon_library_highlight:I = 0x7f020755

.field public static final icon_library_normal:I = 0x7f020756

.field public static final icon_lock:I = 0x7f020757

.field public static final icon_mediapicker_send_highlight:I = 0x7f020758

.field public static final icon_mediapicker_send_normal:I = 0x7f020759

.field public static final icon_microphone:I = 0x7f02075a

.field public static final icon_multi_tagged:I = 0x7f02075b

.field public static final icon_one_tagged:I = 0x7f02075c

.field public static final icon_pages:I = 0x7f02075d

.field public static final icon_participant:I = 0x7f02075e

.field public static final icon_remove_tag_normal:I = 0x7f02075f

.field public static final icon_rotate_highlight:I = 0x7f020760

.field public static final icon_rotate_normal:I = 0x7f020761

.field public static final icon_schedule:I = 0x7f020762

.field public static final icon_schedule_selected:I = 0x7f020763

.field public static final icon_search:I = 0x7f020764

.field public static final icon_table_cell_action:I = 0x7f020765

.field public static final icon_table_cell_action_highlighted:I = 0x7f020766

.field public static final icon_table_cell_action_normal:I = 0x7f020767

.field public static final icon_two_tagged:I = 0x7f020768

.field public static final icon_video_normal:I = 0x7f020769

.field public static final icon_wakizashi:I = 0x7f02076a

.field public static final igcamera_sm:I = 0x7f02076b

.field public static final image_camera_button:I = 0x7f02076c

.field public static final info_icon:I = 0x7f02076d

.field public static final info_icon_pressed_states:I = 0x7f02076e

.field public static final info_solid_light_gray_m:I = 0x7f02076f

.field public static final instagram:I = 0x7f020770

.field public static final instagram_white:I = 0x7f020771

.field public static final installbutton:I = 0x7f020772

.field public static final invite_banner_sticker:I = 0x7f020773

.field public static final invite_blue:I = 0x7f020774

.field public static final iorg_dialog_background:I = 0x7f020775

.field public static final iorg_dialog_button_background:I = 0x7f020776

.field public static final iorg_dialog_button_pressed:I = 0x7f020777

.field public static final iorg_dialog_logo:I = 0x7f020778

.field public static final iorg_dialog_logo_grey:I = 0x7f020779

.field public static final iorg_dialog_nux_logo:I = 0x7f02077a

.field public static final iorg_fb4a_menu_icon:I = 0x7f02077b

.field public static final iorg_fb4a_menu_overflow:I = 0x7f02077c

.field public static final iorg_fb4a_menu_overflow_selected:I = 0x7f02077d

.field public static final iorg_free_services_row_background:I = 0x7f02077e

.field public static final iorg_generic_row_background:I = 0x7f02077f

.field public static final iorg_menu_about_icon:I = 0x7f020780

.field public static final iorg_menu_back_disabled_icon:I = 0x7f020781

.field public static final iorg_menu_back_icon:I = 0x7f020782

.field public static final iorg_menu_background:I = 0x7f020783

.field public static final iorg_menu_bookmark_set_icon:I = 0x7f020784

.field public static final iorg_menu_bookmark_unset_icon:I = 0x7f020785

.field public static final iorg_menu_forward_disabled_icon:I = 0x7f020786

.field public static final iorg_menu_forward_icon:I = 0x7f020787

.field public static final iorg_menu_history_icon:I = 0x7f020788

.field public static final iorg_menu_refresh_icon:I = 0x7f020789

.field public static final iorg_menu_row_background_bottom:I = 0x7f02078a

.field public static final iorg_menu_row_background_top:I = 0x7f02078b

.field public static final iorg_menu_row_background_top_left:I = 0x7f02078c

.field public static final iorg_menu_row_background_top_right:I = 0x7f02078d

.field public static final iorg_menu_share_icon:I = 0x7f02078e

.field public static final iorg_nux_dialog_header_background:I = 0x7f02078f

.field public static final iorg_row_item_x:I = 0x7f020790

.field public static final iorg_webview_progress_bar:I = 0x7f020791

.field public static final iorg_x_icon:I = 0x7f020792

.field public static final iorg_x_icon_pressed:I = 0x7f020793

.field public static final item_background_holo_light:I = 0x7f020794

.field public static final item_list_bg:I = 0x7f020795

.field public static final item_list_border:I = 0x7f020796

.field public static final jewel_promo_green_dot:I = 0x7f020797

.field public static final jewel_pymk_header_bar:I = 0x7f020798

.field public static final jewel_pymk_header_shadow:I = 0x7f020799

.field public static final keyword_search_info:I = 0x7f02079a

.field public static final keyword_search_people:I = 0x7f02079b

.field public static final keyword_search_photos:I = 0x7f02079c

.field public static final launcher_messenger_icon_katana:I = 0x7f02079d

.field public static final launcher_option:I = 0x7f02079e

.field public static final launcher_overlay:I = 0x7f02079f

.field public static final launcher_wf:I = 0x7f0207a0

.field public static final layout_border_left_button:I = 0x7f0207a1

.field public static final layout_border_option:I = 0x7f0207a2

.field public static final life_event_icon:I = 0x7f0207a3

.field public static final light_border:I = 0x7f0207a4

.field public static final like:I = 0x7f0207a5

.field public static final like_blue_icon:I = 0x7f0207a6

.field public static final like_dark_grey_l:I = 0x7f0207a7

.field public static final like_icon:I = 0x7f0207a8

.field public static final like_icon_active:I = 0x7f0207a9

.field public static final like_icon_active_new:I = 0x7f0207aa

.field public static final like_icon_new:I = 0x7f0207ab

.field public static final like_light_grey_icon:I = 0x7f0207ac

.field public static final like_light_grey_l:I = 0x7f0207ad

.field public static final link_background:I = 0x7f0207ae

.field public static final list_chevron:I = 0x7f0207af

.field public static final list_divider_holo_light:I = 0x7f0207b0

.field public static final list_focused_holo:I = 0x7f0207b1

.field public static final list_longpressed_holo:I = 0x7f0207b2

.field public static final list_pressed_holo_light:I = 0x7f0207b3

.field public static final list_section_header_background:I = 0x7f0207b4

.field public static final list_selector_background_transition_holo_light:I = 0x7f0207b5

.field public static final list_selector_disabled_holo_light:I = 0x7f0207b6

.field public static final loading:I = 0x7f0207b7

.field public static final loading0:I = 0x7f0207b8

.field public static final loading1:I = 0x7f0207b9

.field public static final loading2:I = 0x7f0207ba

.field public static final loading3:I = 0x7f0207bb

.field public static final loading_text_color:I = 0x7f020e5c

.field public static final location_ping_dialog_checkmark:I = 0x7f0207bc

.field public static final location_ping_dialog_pressable:I = 0x7f0207bd

.field public static final location_ping_dialog_spinner:I = 0x7f0207be

.field public static final location_ping_dialog_spinner_normal:I = 0x7f0207bf

.field public static final location_ping_dialog_spinner_pressed:I = 0x7f0207c0

.field public static final location_settings_history_off:I = 0x7f0207c1

.field public static final location_settings_history_on:I = 0x7f0207c2

.field public static final lock_overlay:I = 0x7f0207c3

.field public static final lock_screen_only_option:I = 0x7f0207c4

.field public static final lock_screen_option:I = 0x7f0207c5

.field public static final lock_wf:I = 0x7f0207c6

.field public static final lockscreen_checkbox_selector:I = 0x7f0207c7

.field public static final lockscreen_notification_dialog_background:I = 0x7f0207c8

.field public static final lockscreen_switch_thumb_selector:I = 0x7f0207c9

.field public static final lockscreen_switch_track_selector:I = 0x7f0207ca

.field public static final login_background:I = 0x7f0207cb

.field public static final login_button:I = 0x7f0207cc

.field public static final login_button_background:I = 0x7f0207cd

.field public static final login_button_pressed:I = 0x7f0207ce

.field public static final login_inputfield:I = 0x7f0207cf

.field public static final login_inputfield_background:I = 0x7f0207d0

.field public static final login_inputfield_pressed:I = 0x7f0207d1

.field public static final logo_facebook_home_text:I = 0x7f0207d2

.field public static final logo_facebook_text:I = 0x7f0207d3

.field public static final logout_menu_icon:I = 0x7f0207d4

.field public static final map_dot:I = 0x7f0207d5

.field public static final map_placeholder:I = 0x7f0207d6

.field public static final map_placeholder_background_repeat:I = 0x7f0207d7

.field public static final me_logo:I = 0x7f0207d8

.field public static final media_edit_cancel_btn:I = 0x7f0207d9

.field public static final media_edit_send_btn:I = 0x7f0207da

.field public static final media_gallery_comment_icon:I = 0x7f0207db

.field public static final media_gallery_counter:I = 0x7f0207dc

.field public static final media_gallery_counter_active:I = 0x7f0207dd

.field public static final media_gallery_like_icon:I = 0x7f0207de

.field public static final media_gallery_like_icon_active:I = 0x7f0207df

.field public static final media_gallery_menu_icon:I = 0x7f0207e0

.field public static final media_gallery_share_icon:I = 0x7f0207e1

.field public static final media_gallery_tag_icon:I = 0x7f0207e2

.field public static final media_gallery_tag_icon_active:I = 0x7f0207e3

.field public static final media_gallery_tag_icon_active_for_counter:I = 0x7f0207e4

.field public static final media_gallery_tag_icon_for_counter:I = 0x7f0207e5

.field public static final mediapicker_send_button_states:I = 0x7f0207e6

.field public static final megaphone_image_bg:I = 0x7f0207e7

.field public static final megaphone_story_x_button:I = 0x7f0207e8

.field public static final members_bar_add_member:I = 0x7f0207e9

.field public static final members_bar_add_member_background:I = 0x7f0207ea

.field public static final members_divider:I = 0x7f0207eb

.field public static final menu_icon:I = 0x7f0207ec

.field public static final menu_icon_more:I = 0x7f0207ed

.field public static final menu_item_bg:I = 0x7f0207ee

.field public static final message_notification_panel:I = 0x7f0207ef

.field public static final message_panel:I = 0x7f0207f0

.field public static final message_panel_press_state:I = 0x7f0207f1

.field public static final messages_dark_grey_l:I = 0x7f0207f2

.field public static final messages_dark_grey_m:I = 0x7f0207f3

.field public static final messages_glyph_with_state:I = 0x7f0207f4

.field public static final messages_light_grey_l:I = 0x7f0207f5

.field public static final messages_light_grey_m:I = 0x7f0207f6

.field public static final messenger_badge_s:I = 0x7f0207f7

.field public static final minutiae_chevron_right:I = 0x7f0207f8

.field public static final minutiae_ridge_button_background:I = 0x7f0207f9

.field public static final minutiae_ridge_disabled:I = 0x7f0207fa

.field public static final minutiae_section_header_background:I = 0x7f0207fb

.field public static final mondobar_icon_action:I = 0x7f0207fc

.field public static final mondobar_icon_add:I = 0x7f0207fd

.field public static final mondobar_icon_add_normal:I = 0x7f0207fe

.field public static final mondobar_icon_add_pressed:I = 0x7f0207ff

.field public static final mondobar_icon_launcher:I = 0x7f020800

.field public static final mondobar_icon_like:I = 0x7f020801

.field public static final mondobar_icon_new:I = 0x7f020802

.field public static final mondobar_jewel_badge:I = 0x7f020803

.field public static final mondobar_jewel_friends_inactive:I = 0x7f020804

.field public static final mondobar_jewel_friends_off:I = 0x7f020805

.field public static final mondobar_jewel_friends_off_pressed:I = 0x7f020806

.field public static final mondobar_jewel_friends_on:I = 0x7f020807

.field public static final mondobar_jewel_messages_inactive:I = 0x7f020808

.field public static final mondobar_jewel_messages_off:I = 0x7f020809

.field public static final mondobar_jewel_messages_off_pressed:I = 0x7f02080a

.field public static final mondobar_jewel_messages_on:I = 0x7f02080b

.field public static final mondobar_jewel_notifications_inactive:I = 0x7f02080c

.field public static final mondobar_jewel_notifications_off:I = 0x7f02080d

.field public static final mondobar_jewel_notifications_off_pressed:I = 0x7f02080e

.field public static final mondobar_jewel_notifications_on:I = 0x7f02080f

.field public static final multipicker_cover_color:I = 0x7f020e5b

.field public static final multipicker_cover_transition:I = 0x7f020810

.field public static final music_buffering_spinner:I = 0x7f020811

.field public static final music_next_button:I = 0x7f020812

.field public static final music_notification_back_overlay:I = 0x7f020813

.field public static final music_notification_back_panel:I = 0x7f020814

.field public static final music_notification_buffering_spinner:I = 0x7f020815

.field public static final music_notification_front_overlay:I = 0x7f020816

.field public static final music_notification_highlights:I = 0x7f020817

.field public static final music_notification_next_button:I = 0x7f020818

.field public static final music_notification_next_button_disabled:I = 0x7f020819

.field public static final music_notification_next_button_pressed:I = 0x7f02081a

.field public static final music_notification_next_button_unpressed:I = 0x7f02081b

.field public static final music_notification_next_glyph:I = 0x7f02081c

.field public static final music_notification_pause_button:I = 0x7f02081d

.field public static final music_notification_pause_button_disabled:I = 0x7f02081e

.field public static final music_notification_pause_button_pressed:I = 0x7f02081f

.field public static final music_notification_pause_button_unpressed:I = 0x7f020820

.field public static final music_notification_previous_button:I = 0x7f020821

.field public static final music_notification_previous_button_disabled:I = 0x7f020822

.field public static final music_notification_previous_button_pressed:I = 0x7f020823

.field public static final music_notification_previous_button_unpressed:I = 0x7f020824

.field public static final music_notification_previous_glyph:I = 0x7f020825

.field public static final music_notification_shadow:I = 0x7f020826

.field public static final music_notification_spinner_indeterminate:I = 0x7f020827

.field public static final music_pause_button:I = 0x7f020828

.field public static final music_play_button:I = 0x7f020829

.field public static final music_preview_card_cover_art_placeholder:I = 0x7f02082a

.field public static final music_previous_button:I = 0x7f02082b

.field public static final music_spinner_indeterminate:I = 0x7f02082c

.field public static final music_story_icon:I = 0x7f02082d

.field public static final native_optin_interstitial_background:I = 0x7f02082e

.field public static final nav_friend_requests:I = 0x7f02082f

.field public static final nav_messages:I = 0x7f020830

.field public static final nav_more:I = 0x7f020831

.field public static final nav_news_feed:I = 0x7f020832

.field public static final nav_notifications:I = 0x7f020833

.field public static final navigation_back:I = 0x7f020834

.field public static final navigation_forward:I = 0x7f020835

.field public static final nearby_bookmark:I = 0x7f020836

.field public static final nearby_category_cell:I = 0x7f020837

.field public static final nearby_category_cell_bottom_left:I = 0x7f020838

.field public static final nearby_category_cell_bottom_right:I = 0x7f020839

.field public static final nearby_category_cell_top_left:I = 0x7f02083a

.field public static final nearby_category_cell_top_right:I = 0x7f02083b

.field public static final nearby_default_place:I = 0x7f02083c

.field public static final nearby_icon_arts:I = 0x7f02083d

.field public static final nearby_icon_arts_pressed:I = 0x7f02083e

.field public static final nearby_icon_arts_selector:I = 0x7f02083f

.field public static final nearby_icon_coffee:I = 0x7f020840

.field public static final nearby_icon_coffee_pressed:I = 0x7f020841

.field public static final nearby_icon_coffee_selector:I = 0x7f020842

.field public static final nearby_icon_default:I = 0x7f020843

.field public static final nearby_icon_default_pressed:I = 0x7f020844

.field public static final nearby_icon_default_selector:I = 0x7f020845

.field public static final nearby_icon_hotels:I = 0x7f020846

.field public static final nearby_icon_hotels_pressed:I = 0x7f020847

.field public static final nearby_icon_hotels_selector:I = 0x7f020848

.field public static final nearby_icon_list:I = 0x7f020849

.field public static final nearby_icon_location:I = 0x7f02084a

.field public static final nearby_icon_nightlife:I = 0x7f02084b

.field public static final nearby_icon_nightlife_pressed:I = 0x7f02084c

.field public static final nearby_icon_nightlife_selector:I = 0x7f02084d

.field public static final nearby_icon_outdoors:I = 0x7f02084e

.field public static final nearby_icon_outdoors_pressed:I = 0x7f02084f

.field public static final nearby_icon_outdoors_selector:I = 0x7f020850

.field public static final nearby_icon_restaurants:I = 0x7f020851

.field public static final nearby_icon_restaurants_pressed:I = 0x7f020852

.field public static final nearby_icon_restaurants_selector:I = 0x7f020853

.field public static final nearby_icon_search_input:I = 0x7f020854

.field public static final nearby_icon_shopping:I = 0x7f020855

.field public static final nearby_icon_shopping_pressed:I = 0x7f020856

.field public static final nearby_icon_shopping_selector:I = 0x7f020857

.field public static final nearby_map_baby_pin_mask:I = 0x7f020858

.field public static final nearby_map_baby_pin_mask_regular:I = 0x7f020859

.field public static final nearby_map_baby_pin_mask_selected:I = 0x7f02085a

.field public static final nearby_map_badge:I = 0x7f02085b

.field public static final nearby_map_button:I = 0x7f02085c

.field public static final nearby_map_button_pressed:I = 0x7f02085d

.field public static final nearby_map_button_selector:I = 0x7f02085e

.field public static final nearby_map_pin:I = 0x7f02085f

.field public static final nearby_map_pin_mask:I = 0x7f020860

.field public static final nearby_map_pin_mask_regular:I = 0x7f020861

.field public static final nearby_map_pin_mask_selected:I = 0x7f020862

.field public static final nearby_no_gps:I = 0x7f020863

.field public static final nearby_no_network:I = 0x7f020864

.field public static final nearby_nux_dialog_point_down_left:I = 0x7f020865

.field public static final nearby_nux_dialog_point_up_center:I = 0x7f020866

.field public static final nearby_places_row_view_background:I = 0x7f020867

.field public static final nearby_progress_background:I = 0x7f020868

.field public static final nearby_search_bar_background:I = 0x7f020869

.field public static final nearby_search_row_bg:I = 0x7f02086a

.field public static final nearby_search_row_bottom_rounded_corners_bg:I = 0x7f02086b

.field public static final nearby_search_row_top_rounded_corners_bg:I = 0x7f02086c

.field public static final nearby_stacked_pin_mask:I = 0x7f02086d

.field public static final nearby_stacked_pin_mask_regular:I = 0x7f02086e

.field public static final nearby_stacked_pin_mask_selected:I = 0x7f02086f

.field public static final nearby_stacked_pin_with_badge:I = 0x7f020870

.field public static final nearby_stacked_pin_without_badge:I = 0x7f020871

.field public static final nearby_topic:I = 0x7f020872

.field public static final needle_search_filter_friends:I = 0x7f020873

.field public static final needle_search_filter_friends_active:I = 0x7f020874

.field public static final needle_search_filter_location:I = 0x7f020875

.field public static final needle_search_filter_location_active:I = 0x7f020876

.field public static final needle_search_filter_posted_by:I = 0x7f020877

.field public static final needle_search_filter_posted_by_active:I = 0x7f020878

.field public static final needle_search_filter_school:I = 0x7f020879

.field public static final needle_search_filter_school_active:I = 0x7f02087a

.field public static final needle_search_filter_sort_by_name:I = 0x7f02087b

.field public static final needle_search_filter_sort_by_name_active:I = 0x7f02087c

.field public static final needle_search_filter_tagged:I = 0x7f02087d

.field public static final needle_search_filter_tagged_active:I = 0x7f02087e

.field public static final needle_search_filter_work:I = 0x7f02087f

.field public static final needle_search_filter_work_active:I = 0x7f020880

.field public static final nekov_install_button:I = 0x7f020881

.field public static final nekov_mute_button:I = 0x7f020882

.field public static final nekov_pause_button:I = 0x7f020883

.field public static final nekov_play_button:I = 0x7f020884

.field public static final neue_action_button_rounded:I = 0x7f020885

.field public static final neue_action_button_rounded_normal:I = 0x7f020886

.field public static final neue_action_button_rounded_pressed:I = 0x7f020887

.field public static final neue_action_selector:I = 0x7f020888

.field public static final neue_orca_panel_button_background:I = 0x7f020889

.field public static final neue_orca_panel_button_background_normal:I = 0x7f02088a

.field public static final neue_orca_panel_button_background_pressed:I = 0x7f02088b

.field public static final neue_orca_small_spinner_clockwise:I = 0x7f02088c

.field public static final neue_orca_small_spinner_clockwise_indeterminate:I = 0x7f02088d

.field public static final neue_orca_small_spinner_clockwise_indeterminate_light:I = 0x7f02088e

.field public static final neue_orca_small_spinner_clockwise_light:I = 0x7f02088f

.field public static final neue_orca_small_spinner_clockwise_orange:I = 0x7f020890

.field public static final neue_orca_small_spinner_clockwise_orange_indeterminate:I = 0x7f020891

.field public static final neue_orca_small_spinner_counterclockwise:I = 0x7f020892

.field public static final neue_orca_small_spinner_counterclockwise_indeterminate:I = 0x7f020893

.field public static final neue_orca_small_spinner_counterclockwise_indeterminate_light:I = 0x7f020894

.field public static final neue_orca_small_spinner_counterclockwise_light:I = 0x7f020895

.field public static final neue_orca_small_spinner_counterclockwise_orange:I = 0x7f020896

.field public static final neue_orca_small_spinner_counterclockwise_orange_indeterminate:I = 0x7f020897

.field public static final neue_orca_small_spinner_indeterminate:I = 0x7f020898

.field public static final neue_orca_small_spinner_indeterminate_light:I = 0x7f020899

.field public static final neue_orca_small_spinner_orange_indeterminate:I = 0x7f02089a

.field public static final neue_orca_spinner_clockwise:I = 0x7f02089b

.field public static final neue_orca_spinner_clockwise_indeterminate:I = 0x7f02089c

.field public static final neue_orca_spinner_clockwise_indeterminate_light:I = 0x7f02089d

.field public static final neue_orca_spinner_clockwise_light:I = 0x7f02089e

.field public static final neue_orca_spinner_clockwise_orange:I = 0x7f02089f

.field public static final neue_orca_spinner_clockwise_orange_indeterminate:I = 0x7f0208a0

.field public static final neue_orca_spinner_counterclockwise:I = 0x7f0208a1

.field public static final neue_orca_spinner_counterclockwise_indeterminate:I = 0x7f0208a2

.field public static final neue_orca_spinner_counterclockwise_indeterminate_light:I = 0x7f0208a3

.field public static final neue_orca_spinner_counterclockwise_light:I = 0x7f0208a4

.field public static final neue_orca_spinner_counterclockwise_orange:I = 0x7f0208a5

.field public static final neue_orca_spinner_counterclockwise_orange_indeterminate:I = 0x7f0208a6

.field public static final neue_orca_spinner_indeterminate:I = 0x7f0208a7

.field public static final neue_orca_spinner_indeterminate_light:I = 0x7f0208a8

.field public static final neue_orca_spinner_orange_indeterminate:I = 0x7f0208a9

.field public static final neue_typing_dot:I = 0x7f0208aa

.field public static final new_bookmark_in_favorites:I = 0x7f0208ab

.field public static final new_bookmark_item_bg:I = 0x7f0208ac

.field public static final new_bookmark_section_apps:I = 0x7f0208ad

.field public static final new_bookmark_section_events:I = 0x7f0208ae

.field public static final new_bookmark_section_facebook:I = 0x7f0208af

.field public static final new_bookmark_section_groups:I = 0x7f0208b0

.field public static final new_bookmark_section_link_indicator:I = 0x7f0208b1

.field public static final new_bookmark_section_lists:I = 0x7f0208b2

.field public static final new_bookmark_section_pages:I = 0x7f0208b3

.field public static final new_bookmark_section_settings:I = 0x7f0208b4

.field public static final new_bookmark_settings_about:I = 0x7f0208b5

.field public static final new_bookmark_settings_account_settings:I = 0x7f0208b6

.field public static final new_bookmark_settings_activity_log:I = 0x7f0208b7

.field public static final new_bookmark_settings_app_settings:I = 0x7f0208b8

.field public static final new_bookmark_settings_code_generator:I = 0x7f0208b9

.field public static final new_bookmark_settings_default:I = 0x7f0208ba

.field public static final new_bookmark_settings_edit_favorites:I = 0x7f0208bb

.field public static final new_bookmark_settings_help_center:I = 0x7f0208bc

.field public static final new_bookmark_settings_logout:I = 0x7f0208bd

.field public static final new_bookmark_settings_manage_news_feed:I = 0x7f0208be

.field public static final new_bookmark_settings_mobile_data:I = 0x7f0208bf

.field public static final new_bookmark_settings_privacy_shortcuts:I = 0x7f0208c0

.field public static final new_bookmark_settings_report_problem:I = 0x7f0208c1

.field public static final new_bookmark_settings_terms_and_policies:I = 0x7f0208c2

.field public static final new_event_selector_bg:I = 0x7f0208c3

.field public static final new_event_selector_bg_normal:I = 0x7f0208c4

.field public static final new_event_selector_bg_pressed:I = 0x7f0208c5

.field public static final new_message:I = 0x7f0208c6

.field public static final new_message_pill_background:I = 0x7f0208c7

.field public static final newsfeed_light_grey_l:I = 0x7f0208c8

.field public static final newsfeed_settings:I = 0x7f0208c9

.field public static final no_avatar:I = 0x7f0208ca

.field public static final no_connection_connect_icon:I = 0x7f0208cb

.field public static final no_connection_retry_icon:I = 0x7f0208cc

.field public static final no_shortcut_icon:I = 0x7f0208cd

.field public static final notif_placeholder:I = 0x7f0208ce

.field public static final notif_profile_photo_highlights:I = 0x7f0208cf

.field public static final notif_x:I = 0x7f0208d0

.field public static final notification_lockscreen_background:I = 0x7f0208d1

.field public static final notification_not_load:I = 0x7f0208d2

.field public static final notification_panel:I = 0x7f0208d3

.field public static final notification_panel_background:I = 0x7f0208d4

.field public static final notification_panel_press_state:I = 0x7f0208d5

.field public static final notificationbar_jewel_friends_off:I = 0x7f0208d6

.field public static final notificationbar_jewel_messages_off:I = 0x7f0208d7

.field public static final notificationbar_jewel_notifications_off:I = 0x7f0208d8

.field public static final notifications_dots_icon:I = 0x7f0208d9

.field public static final notifications_facebook_icon:I = 0x7f0208da

.field public static final notifications_light_grey_l:I = 0x7f0208db

.field public static final notifications_messenger_icon:I = 0x7f0208dc

.field public static final notifications_toggle_bg:I = 0x7f0208dd

.field public static final notifications_toggle_off:I = 0x7f0208de

.field public static final notifications_toggle_on:I = 0x7f0208df

.field public static final notifications_x_to_close_button:I = 0x7f0208e0

.field public static final num_pad_divider:I = 0x7f0208e1

.field public static final nux_background:I = 0x7f0208e2

.field public static final nux_background_photo:I = 0x7f0208e3

.field public static final nux_button_states:I = 0x7f0208e4

.field public static final nux_callout:I = 0x7f0208e5

.field public static final nux_contact_importer:I = 0x7f0208e6

.field public static final nux_dialog_background:I = 0x7f0208e7

.field public static final nux_dialog_button_background:I = 0x7f0208e8

.field public static final nux_dialog_button_background_down:I = 0x7f0208e9

.field public static final nux_fade_gradient_bottom:I = 0x7f0208ea

.field public static final nux_fade_gradient_top:I = 0x7f0208eb

.field public static final nux_location_close:I = 0x7f0208ec

.field public static final offline_posting_cancel:I = 0x7f0208ed

.field public static final offline_posting_cancel_initial:I = 0x7f0208ee

.field public static final offline_posting_cap:I = 0x7f0208ef

.field public static final offline_posting_error:I = 0x7f0208f0

.field public static final offline_posting_line:I = 0x7f0208f1

.field public static final offline_posting_retry:I = 0x7f0208f2

.field public static final opentable_ticket:I = 0x7f0208f3

.field public static final orca_ab_menu_moreoverflow_white:I = 0x7f0208f4

.field public static final orca_ab_split_background:I = 0x7f0208f5

.field public static final orca_ab_stacked_solid_light:I = 0x7f0208f6

.field public static final orca_ab_stacked_solid_light_chat_heads:I = 0x7f0208f7

.field public static final orca_ab_tab_divider:I = 0x7f0208f8

.field public static final orca_action_bar_cancel:I = 0x7f0208f9

.field public static final orca_action_bar_save:I = 0x7f0208fa

.field public static final orca_action_overflow_button:I = 0x7f0208fb

.field public static final orca_active_now:I = 0x7f0208fc

.field public static final orca_add_favorite_button:I = 0x7f0208fd

.field public static final orca_admin_add_people:I = 0x7f0208fe

.field public static final orca_admin_add_people_neue:I = 0x7f0208ff

.field public static final orca_admin_change_picture:I = 0x7f020900

.field public static final orca_admin_change_picture_neue:I = 0x7f020901

.field public static final orca_admin_edit_name:I = 0x7f020902

.field public static final orca_admin_edit_name_neue:I = 0x7f020903

.field public static final orca_admin_incoming_call:I = 0x7f020904

.field public static final orca_admin_incoming_call_neue:I = 0x7f020905

.field public static final orca_admin_leave_conversation:I = 0x7f020906

.field public static final orca_admin_leave_conversation_neue:I = 0x7f020907

.field public static final orca_admin_message_user_badge:I = 0x7f020908

.field public static final orca_admin_missed_call:I = 0x7f020909

.field public static final orca_admin_missed_call_neue:I = 0x7f02090a

.field public static final orca_admin_outgoing_call:I = 0x7f02090b

.field public static final orca_admin_outgoing_call_neue:I = 0x7f02090c

.field public static final orca_admin_video_call:I = 0x7f02090d

.field public static final orca_admin_video_call_neue:I = 0x7f02090e

.field public static final orca_anchorable_toast_background:I = 0x7f02090f

.field public static final orca_attach_audio_normal:I = 0x7f020910

.field public static final orca_attach_camera_normal:I = 0x7f020911

.field public static final orca_attach_camera_pressed:I = 0x7f020912

.field public static final orca_attach_photo_normal:I = 0x7f020913

.field public static final orca_attach_photo_pressed:I = 0x7f020914

.field public static final orca_attach_search_normal:I = 0x7f020915

.field public static final orca_attach_search_pressed:I = 0x7f020916

.field public static final orca_attachment_file_generic:I = 0x7f020917

.field public static final orca_attachment_file_movie:I = 0x7f020918

.field public static final orca_attachment_file_music:I = 0x7f020919

.field public static final orca_attachment_file_richtext:I = 0x7f02091a

.field public static final orca_attachment_file_text:I = 0x7f02091b

.field public static final orca_attachment_remove_normal:I = 0x7f02091c

.field public static final orca_attachment_remove_pressed:I = 0x7f02091d

.field public static final orca_attachments_arrow:I = 0x7f02091e

.field public static final orca_attachments_arrow_reversed:I = 0x7f02091f

.field public static final orca_attachments_choose_media:I = 0x7f020920

.field public static final orca_attachments_emoji_del:I = 0x7f020921

.field public static final orca_attachments_emoji_del_neue:I = 0x7f020922

.field public static final orca_attachments_emoji_del_neue_pressed:I = 0x7f020923

.field public static final orca_attachments_emoji_del_pressed:I = 0x7f020924

.field public static final orca_attachments_emoji_del_selector:I = 0x7f020925

.field public static final orca_attachments_emoji_del_selector_neue:I = 0x7f020926

.field public static final orca_attachments_emoji_more:I = 0x7f020927

.field public static final orca_attachments_emoji_more_neue:I = 0x7f020928

.field public static final orca_attachments_emoji_more_neue_pressed:I = 0x7f020929

.field public static final orca_attachments_emoji_more_pressed:I = 0x7f02092a

.field public static final orca_attachments_emoji_more_selector:I = 0x7f02092b

.field public static final orca_attachments_emoji_more_selector_neue:I = 0x7f02092c

.field public static final orca_attachments_image_search:I = 0x7f02092d

.field public static final orca_attachments_record_voice:I = 0x7f02092e

.field public static final orca_attachments_take_photo:I = 0x7f02092f

.field public static final orca_audio_loading_spinner_left:I = 0x7f020930

.field public static final orca_audio_loading_spinner_neue:I = 0x7f020931

.field public static final orca_audio_loading_spinner_right:I = 0x7f020932

.field public static final orca_audio_record_volume:I = 0x7f020933

.field public static final orca_audio_record_volume_neue:I = 0x7f020934

.field public static final orca_audio_spinner_1_neue:I = 0x7f020935

.field public static final orca_audio_spinner_2_neue:I = 0x7f020936

.field public static final orca_audio_spinner_3_neue:I = 0x7f020937

.field public static final orca_audio_spinner_4_neue:I = 0x7f020938

.field public static final orca_audio_spinner_5_neue:I = 0x7f020939

.field public static final orca_audio_spinner_6_neue:I = 0x7f02093a

.field public static final orca_audio_spinner_7_neue:I = 0x7f02093b

.field public static final orca_audio_spinner_8_neue:I = 0x7f02093c

.field public static final orca_audio_spinner_left_1:I = 0x7f02093d

.field public static final orca_audio_spinner_left_2:I = 0x7f02093e

.field public static final orca_audio_spinner_left_3:I = 0x7f02093f

.field public static final orca_audio_spinner_left_4:I = 0x7f020940

.field public static final orca_audio_spinner_left_5:I = 0x7f020941

.field public static final orca_audio_spinner_left_6:I = 0x7f020942

.field public static final orca_audio_spinner_left_7:I = 0x7f020943

.field public static final orca_audio_spinner_left_8:I = 0x7f020944

.field public static final orca_audio_spinner_right_1:I = 0x7f020945

.field public static final orca_audio_spinner_right_2:I = 0x7f020946

.field public static final orca_audio_spinner_right_3:I = 0x7f020947

.field public static final orca_audio_spinner_right_4:I = 0x7f020948

.field public static final orca_audio_spinner_right_5:I = 0x7f020949

.field public static final orca_audio_spinner_right_6:I = 0x7f02094a

.field public static final orca_audio_spinner_right_7:I = 0x7f02094b

.field public static final orca_audio_spinner_right_8:I = 0x7f02094c

.field public static final orca_back:I = 0x7f02094d

.field public static final orca_bing_logo:I = 0x7f02094e

.field public static final orca_blank_state_logo:I = 0x7f02094f

.field public static final orca_blue_nux_bubble:I = 0x7f020950

.field public static final orca_blue_nux_bubble_bottom:I = 0x7f020951

.field public static final orca_blue_nux_bubble_bottom_left:I = 0x7f020952

.field public static final orca_blue_nux_bubble_bottom_right:I = 0x7f020953

.field public static final orca_blue_nux_bubble_left:I = 0x7f020954

.field public static final orca_blue_nux_bubble_right:I = 0x7f020955

.field public static final orca_btn_check_light:I = 0x7f020956

.field public static final orca_btn_check_off_disabled_focused_light:I = 0x7f020957

.field public static final orca_btn_check_off_disabled_light:I = 0x7f020958

.field public static final orca_btn_check_off_focused_light:I = 0x7f020959

.field public static final orca_btn_check_off_light:I = 0x7f02095a

.field public static final orca_btn_check_off_pressed_light:I = 0x7f02095b

.field public static final orca_btn_check_on_disabled_focused_light:I = 0x7f02095c

.field public static final orca_btn_check_on_disabled_light:I = 0x7f02095d

.field public static final orca_btn_check_on_focused_light:I = 0x7f02095e

.field public static final orca_btn_check_on_light:I = 0x7f02095f

.field public static final orca_btn_check_on_pressed_light:I = 0x7f020960

.field public static final orca_button_blue:I = 0x7f020961

.field public static final orca_button_blue_disabled:I = 0x7f020962

.field public static final orca_button_blue_focused:I = 0x7f020963

.field public static final orca_button_blue_focused_disabled:I = 0x7f020964

.field public static final orca_button_blue_normal:I = 0x7f020965

.field public static final orca_button_blue_pressed:I = 0x7f020966

.field public static final orca_button_dark_background:I = 0x7f020967

.field public static final orca_button_dark_focused:I = 0x7f020968

.field public static final orca_button_dark_normal:I = 0x7f020969

.field public static final orca_button_dark_pressed:I = 0x7f02096a

.field public static final orca_button_grey:I = 0x7f02096b

.field public static final orca_button_grey_focused:I = 0x7f02096c

.field public static final orca_button_grey_normal:I = 0x7f02096d

.field public static final orca_button_grey_pressed:I = 0x7f02096e

.field public static final orca_camera_crop_height:I = 0x7f02096f

.field public static final orca_camera_crop_width:I = 0x7f020970

.field public static final orca_chat_actions_background:I = 0x7f020971

.field public static final orca_chat_availability_turn_on_button:I = 0x7f020972

.field public static final orca_chat_availability_turn_on_normal:I = 0x7f020973

.field public static final orca_chat_availability_turn_on_pressed:I = 0x7f020974

.field public static final orca_chat_badge:I = 0x7f020975

.field public static final orca_chat_close_base:I = 0x7f020976

.field public static final orca_chat_close_icon:I = 0x7f020977

.field public static final orca_chat_head_inbox:I = 0x7f020978

.field public static final orca_chat_head_overlay:I = 0x7f020979

.field public static final orca_chat_heads_bubble_view_bottom:I = 0x7f02097a

.field public static final orca_chat_heads_bubble_view_top:I = 0x7f02097b

.field public static final orca_chat_heads_popup_menu:I = 0x7f02097c

.field public static final orca_chat_heads_vertical_divider:I = 0x7f02097d

.field public static final orca_chat_notification_bubble:I = 0x7f02097e

.field public static final orca_chat_notification_bubble_gradient:I = 0x7f02097f

.field public static final orca_chat_notification_bubble_mask:I = 0x7f020980

.field public static final orca_chat_notification_bubble_mask_right:I = 0x7f020981

.field public static final orca_chat_notification_bubble_right:I = 0x7f020982

.field public static final orca_chat_nub:I = 0x7f020983

.field public static final orca_chat_nub_right:I = 0x7f020984

.field public static final orca_compose_edit_text_spacing:I = 0x7f020985

.field public static final orca_composer_attach_camera_button:I = 0x7f020986

.field public static final orca_composer_attach_photo_button:I = 0x7f020987

.field public static final orca_composer_attach_recorder_button:I = 0x7f020988

.field public static final orca_composer_attach_search_button:I = 0x7f020989

.field public static final orca_composer_attachment_remove_button:I = 0x7f02098a

.field public static final orca_composer_attachments_button:I = 0x7f02098b

.field public static final orca_composer_attachments_button_background:I = 0x7f02098c

.field public static final orca_composer_attachments_button_background_top:I = 0x7f02098d

.field public static final orca_composer_attachments_button_neue:I = 0x7f02098e

.field public static final orca_composer_attachments_off:I = 0x7f02098f

.field public static final orca_composer_attachments_on:I = 0x7f020990

.field public static final orca_composer_background:I = 0x7f020991

.field public static final orca_composer_background_neue:I = 0x7f020992

.field public static final orca_composer_button_background:I = 0x7f020993

.field public static final orca_composer_camera_button:I = 0x7f020994

.field public static final orca_composer_camera_button_normal:I = 0x7f020995

.field public static final orca_composer_camera_button_selected:I = 0x7f020996

.field public static final orca_composer_camera_button_with_promo:I = 0x7f020997

.field public static final orca_composer_camera_contract_button:I = 0x7f020998

.field public static final orca_composer_camera_edit:I = 0x7f020999

.field public static final orca_composer_camera_edit_button:I = 0x7f02099a

.field public static final orca_composer_camera_edit_button_pressed:I = 0x7f02099b

.field public static final orca_composer_camera_expand_button:I = 0x7f02099c

.field public static final orca_composer_camera_flip:I = 0x7f02099d

.field public static final orca_composer_camera_fullscreen_button:I = 0x7f02099e

.field public static final orca_composer_camera_send:I = 0x7f02099f

.field public static final orca_composer_camera_send_button:I = 0x7f0209a0

.field public static final orca_composer_camera_send_button_pressed:I = 0x7f0209a1

.field public static final orca_composer_chat_head_inbox_search:I = 0x7f0209a2

.field public static final orca_composer_chat_head_location_off:I = 0x7f0209a3

.field public static final orca_composer_chat_head_location_on:I = 0x7f0209a4

.field public static final orca_composer_chat_head_popup_button:I = 0x7f0209a5

.field public static final orca_composer_chat_head_popup_button_active:I = 0x7f0209a6

.field public static final orca_composer_chat_head_send_button:I = 0x7f0209a7

.field public static final orca_composer_chat_head_send_button_disabled:I = 0x7f0209a8

.field public static final orca_composer_chat_head_send_button_normal:I = 0x7f0209a9

.field public static final orca_composer_chat_head_send_button_pressed:I = 0x7f0209aa

.field public static final orca_composer_chat_head_vertical_divider:I = 0x7f0209ab

.field public static final orca_composer_divider_horizontal:I = 0x7f0209ac

.field public static final orca_composer_divider_vertical:I = 0x7f0209ad

.field public static final orca_composer_emoji_button:I = 0x7f0209ae

.field public static final orca_composer_emoji_button_normal:I = 0x7f0209af

.field public static final orca_composer_emoji_button_selected:I = 0x7f0209b0

.field public static final orca_composer_gallery_button:I = 0x7f0209b1

.field public static final orca_composer_gallery_button_normal:I = 0x7f0209b2

.field public static final orca_composer_gallery_button_selected:I = 0x7f0209b3

.field public static final orca_composer_like:I = 0x7f0209b4

.field public static final orca_composer_like_button:I = 0x7f0209b5

.field public static final orca_composer_like_button_disabled:I = 0x7f0209b6

.field public static final orca_composer_like_button_normal:I = 0x7f0209b7

.field public static final orca_composer_like_button_nux:I = 0x7f0209b8

.field public static final orca_composer_location_button:I = 0x7f0209b9

.field public static final orca_composer_location_button_off:I = 0x7f0209ba

.field public static final orca_composer_location_button_on:I = 0x7f0209bb

.field public static final orca_composer_location_off_normal:I = 0x7f0209bc

.field public static final orca_composer_location_on_normal:I = 0x7f0209bd

.field public static final orca_composer_location_on_normal_cropped:I = 0x7f0209be

.field public static final orca_composer_payment_button:I = 0x7f0209bf

.field public static final orca_composer_payment_button_normal:I = 0x7f0209c0

.field public static final orca_composer_payment_button_selected:I = 0x7f0209c1

.field public static final orca_composer_plus_button:I = 0x7f0209c2

.field public static final orca_composer_send:I = 0x7f0209c3

.field public static final orca_composer_send_button:I = 0x7f0209c4

.field public static final orca_composer_send_disabled:I = 0x7f0209c5

.field public static final orca_composer_sticker_button:I = 0x7f0209c6

.field public static final orca_composer_stickers_button:I = 0x7f0209c7

.field public static final orca_composer_stickers_button_background:I = 0x7f0209c8

.field public static final orca_composer_stickers_button_background_top:I = 0x7f0209c9

.field public static final orca_composer_stickers_button_neue:I = 0x7f0209ca

.field public static final orca_composer_stickers_button_normal:I = 0x7f0209cb

.field public static final orca_composer_stickers_button_selected:I = 0x7f0209cc

.field public static final orca_composer_stickers_off:I = 0x7f0209cd

.field public static final orca_composer_stickers_on:I = 0x7f0209ce

.field public static final orca_composer_tab_selected:I = 0x7f0209cf

.field public static final orca_composer_text_button:I = 0x7f0209d0

.field public static final orca_composer_text_button_normal:I = 0x7f0209d1

.field public static final orca_composer_text_button_selected:I = 0x7f0209d2

.field public static final orca_composer_top_border_background:I = 0x7f0209d3

.field public static final orca_composer_voiceclip_button:I = 0x7f0209d4

.field public static final orca_composer_voiceclip_button_normal:I = 0x7f0209d5

.field public static final orca_composer_voiceclip_button_selected:I = 0x7f0209d6

.field public static final orca_contact_picker_warning_background:I = 0x7f0209d7

.field public static final orca_contacts_fast_scroll_overlay:I = 0x7f0209d8

.field public static final orca_contacts_settings:I = 0x7f0209d9

.field public static final orca_convo_bubble_button_classic:I = 0x7f0209da

.field public static final orca_convo_bubble_me_user_button_classic:I = 0x7f0209db

.field public static final orca_convo_bubble_me_user_normal_classic:I = 0x7f0209dc

.field public static final orca_convo_bubble_me_user_pressed_classic:I = 0x7f0209dd

.field public static final orca_convo_bubble_normal_classic:I = 0x7f0209de

.field public static final orca_convo_bubble_pending_button_classic:I = 0x7f0209df

.field public static final orca_convo_bubble_pending_normal_classic:I = 0x7f0209e0

.field public static final orca_convo_bubble_pending_pressed_classic:I = 0x7f0209e1

.field public static final orca_convo_bubble_pressed_classic:I = 0x7f0209e2

.field public static final orca_convo_profile_overlay:I = 0x7f0209e3

.field public static final orca_dark_add_contact_action:I = 0x7f0209e4

.field public static final orca_dark_new_message_action:I = 0x7f0209e5

.field public static final orca_dark_search_action:I = 0x7f0209e6

.field public static final orca_divebar_icon:I = 0x7f0209e7

.field public static final orca_divebar_icon_angora:I = 0x7f0209e8

.field public static final orca_divebar_icon_harrison:I = 0x7f0209e9

.field public static final orca_divebar_icon_normal_angora:I = 0x7f0209ea

.field public static final orca_divebar_icon_pressed_angora:I = 0x7f0209eb

.field public static final orca_divebar_icon_pressed_harrison:I = 0x7f0209ec

.field public static final orca_divebar_icon_unpressed_harrison:I = 0x7f0209ed

.field public static final orca_divebar_search:I = 0x7f0209ee

.field public static final orca_divebar_section_button:I = 0x7f0209ef

.field public static final orca_divebar_shadow:I = 0x7f0209f0

.field public static final orca_facebook_user_badge:I = 0x7f0209f1

.field public static final orca_facebook_user_badge_list:I = 0x7f0209f2

.field public static final orca_facebook_user_badge_thread:I = 0x7f0209f3

.field public static final orca_facebook_user_badge_thread_title:I = 0x7f0209f4

.field public static final orca_favorite_drag_shadow:I = 0x7f0209f5

.field public static final orca_favorites_add:I = 0x7f0209f6

.field public static final orca_favorites_add_pressed:I = 0x7f0209f7

.field public static final orca_favorites_drag_handle:I = 0x7f0209f8

.field public static final orca_favorites_drag_handle_neue:I = 0x7f0209f9

.field public static final orca_favorites_remove:I = 0x7f0209fa

.field public static final orca_favorites_remove_neue:I = 0x7f0209fb

.field public static final orca_favorites_remove_pressed:I = 0x7f0209fc

.field public static final orca_forward_arrow:I = 0x7f0209fd

.field public static final orca_forward_check_icon:I = 0x7f0209fe

.field public static final orca_forward_send:I = 0x7f0209ff

.field public static final orca_gallery_menu_icon:I = 0x7f020a00

.field public static final orca_groups_settings_row_background:I = 0x7f020a01

.field public static final orca_groups_text_field:I = 0x7f020a02

.field public static final orca_ic_like_action:I = 0x7f020a03

.field public static final orca_ic_like_action_big:I = 0x7f020a04

.field public static final orca_ic_menu_archive:I = 0x7f020a05

.field public static final orca_ic_menu_bug:I = 0x7f020a06

.field public static final orca_ic_menu_delete:I = 0x7f020a07

.field public static final orca_ic_menu_messages_archived:I = 0x7f020a08

.field public static final orca_ic_menu_messages_other:I = 0x7f020a09

.field public static final orca_ic_menu_preferences:I = 0x7f020a0a

.field public static final orca_ic_menu_spam:I = 0x7f020a0b

.field public static final orca_ic_mute_action:I = 0x7f020a0c

.field public static final orca_ic_mute_action_big:I = 0x7f020a0d

.field public static final orca_ic_reply_action_big:I = 0x7f020a0e

.field public static final orca_image_attachment_background:I = 0x7f020a0f

.field public static final orca_image_attachment_background_focused:I = 0x7f020a10

.field public static final orca_image_attachment_background_normal:I = 0x7f020a11

.field public static final orca_image_attachment_background_pressed:I = 0x7f020a12

.field public static final orca_in_app_notification_background:I = 0x7f020a13

.field public static final orca_indicator_autocrop:I = 0x7f020a14

.field public static final orca_info_action:I = 0x7f020a15

.field public static final orca_item_background_holo_light:I = 0x7f020a16

.field public static final orca_light_add_contact_action:I = 0x7f020a17

.field public static final orca_light_search_action:I = 0x7f020a18

.field public static final orca_list_focused_holo:I = 0x7f020a19

.field public static final orca_list_longpressed_holo:I = 0x7f020a1a

.field public static final orca_list_pressed_holo_light:I = 0x7f020a1b

.field public static final orca_list_selector_background_transition_holo_light:I = 0x7f020a1c

.field public static final orca_list_selector_disabled_holo_light:I = 0x7f020a1d

.field public static final orca_list_selector_holo_light:I = 0x7f020a1e

.field public static final orca_location_nux_bubble_neue:I = 0x7f020a1f

.field public static final orca_login_background:I = 0x7f020a20

.field public static final orca_login_button:I = 0x7f020a21

.field public static final orca_login_button_background:I = 0x7f020a22

.field public static final orca_login_button_pressed:I = 0x7f020a23

.field public static final orca_login_help_button:I = 0x7f020a24

.field public static final orca_login_help_icon:I = 0x7f020a25

.field public static final orca_login_help_icon_pressed:I = 0x7f020a26

.field public static final orca_login_inputfield:I = 0x7f020a27

.field public static final orca_login_inputfield_background:I = 0x7f020a28

.field public static final orca_login_inputfield_pressed:I = 0x7f020a29

.field public static final orca_login_primary_button:I = 0x7f020a2a

.field public static final orca_login_primary_button_normal:I = 0x7f020a2b

.field public static final orca_login_primary_button_pressed:I = 0x7f020a2c

.field public static final orca_login_secondary_button:I = 0x7f020a2d

.field public static final orca_login_secondary_button_normal:I = 0x7f020a2e

.field public static final orca_login_secondary_button_pressed:I = 0x7f020a2f

.field public static final orca_magnifier:I = 0x7f020a30

.field public static final orca_map_background:I = 0x7f020a31

.field public static final orca_marker_red:I = 0x7f020a32

.field public static final orca_media_edit_dialog_background:I = 0x7f020a33

.field public static final orca_media_preview_button:I = 0x7f020a34

.field public static final orca_media_tray_error_icon:I = 0x7f020a35

.field public static final orca_media_tray_gallery_button:I = 0x7f020a36

.field public static final orca_media_tray_gallery_button_background:I = 0x7f020a37

.field public static final orca_media_tray_gallery_button_background_active:I = 0x7f020a38

.field public static final orca_media_tray_gallery_button_background_inactive:I = 0x7f020a39

.field public static final orca_media_tray_video_timestamp_bg:I = 0x7f020a3a

.field public static final orca_message_error:I = 0x7f020a3b

.field public static final orca_message_error_normal:I = 0x7f020a3c

.field public static final orca_message_error_pressed:I = 0x7f020a3d

.field public static final orca_message_item_map_image_background:I = 0x7f020a3e

.field public static final orca_message_si_error:I = 0x7f020a3f

.field public static final orca_message_si_error_normal:I = 0x7f020a40

.field public static final orca_message_si_error_pressed:I = 0x7f020a41

.field public static final orca_message_type_chat:I = 0x7f020a42

.field public static final orca_messenger_delivered_badge:I = 0x7f020a43

.field public static final orca_messenger_user_badge:I = 0x7f020a44

.field public static final orca_messenger_user_badge_large:I = 0x7f020a45

.field public static final orca_messenger_user_badge_list:I = 0x7f020a46

.field public static final orca_messenger_user_badge_thread:I = 0x7f020a47

.field public static final orca_messenger_user_badge_thread_title:I = 0x7f020a48

.field public static final orca_mobile_icon:I = 0x7f020a49

.field public static final orca_mobile_icon_chat_head_title:I = 0x7f020a4a

.field public static final orca_mobile_icon_thread:I = 0x7f020a4b

.field public static final orca_mute_icon:I = 0x7f020a4c

.field public static final orca_mylocation:I = 0x7f020a4d

.field public static final orca_nearby_icon:I = 0x7f020a4e

.field public static final orca_nearby_icon_large:I = 0x7f020a4f

.field public static final orca_neue_ab_back_light:I = 0x7f020a50

.field public static final orca_neue_ab_menu_moreoverflow_dark:I = 0x7f020a51

.field public static final orca_neue_ab_menu_moreoverflow_light:I = 0x7f020a52

.field public static final orca_neue_add_contact_not_found:I = 0x7f020a53

.field public static final orca_neue_background_focused:I = 0x7f020a54

.field public static final orca_neue_background_focused_dark:I = 0x7f020a55

.field public static final orca_neue_background_focused_pressed:I = 0x7f020a56

.field public static final orca_neue_background_focused_pressed_dark:I = 0x7f020a57

.field public static final orca_neue_background_longpress:I = 0x7f020a58

.field public static final orca_neue_background_pressed:I = 0x7f020a59

.field public static final orca_neue_background_pressed_dark:I = 0x7f020a5a

.field public static final orca_neue_button:I = 0x7f020a5b

.field public static final orca_neue_button_disabled:I = 0x7f020a5c

.field public static final orca_neue_button_enabled:I = 0x7f020a5d

.field public static final orca_neue_button_pressed:I = 0x7f020a5e

.field public static final orca_neue_checkmark:I = 0x7f020a5f

.field public static final orca_neue_contact_added_profile_checkmark:I = 0x7f020a60

.field public static final orca_neue_edit_text:I = 0x7f020a61

.field public static final orca_neue_edit_text_activated:I = 0x7f020a62

.field public static final orca_neue_edit_text_default:I = 0x7f020a63

.field public static final orca_neue_edit_text_disabled:I = 0x7f020a64

.field public static final orca_neue_edit_text_disabled_focused:I = 0x7f020a65

.field public static final orca_neue_edit_text_focused:I = 0x7f020a66

.field public static final orca_neue_item_background:I = 0x7f020a67

.field public static final orca_neue_item_background_dark:I = 0x7f020a68

.field public static final orca_neue_list_selector_background:I = 0x7f020a69

.field public static final orca_neue_list_selector_background_transition:I = 0x7f020a6a

.field public static final orca_neue_me_preferences_divider:I = 0x7f020a6b

.field public static final orca_neue_menu_text_color:I = 0x7f020a6c

.field public static final orca_neue_progress_bg:I = 0x7f020a6d

.field public static final orca_neue_progress_horizontal:I = 0x7f020a6e

.field public static final orca_neue_progress_indeterminate1:I = 0x7f020a6f

.field public static final orca_neue_progress_indeterminate2:I = 0x7f020a70

.field public static final orca_neue_progress_indeterminate3:I = 0x7f020a71

.field public static final orca_neue_progress_indeterminate4:I = 0x7f020a72

.field public static final orca_neue_progress_indeterminate5:I = 0x7f020a73

.field public static final orca_neue_progress_indeterminate6:I = 0x7f020a74

.field public static final orca_neue_progress_indeterminate7:I = 0x7f020a75

.field public static final orca_neue_progress_indeterminate8:I = 0x7f020a76

.field public static final orca_neue_progress_indeterminate_horizontal:I = 0x7f020a77

.field public static final orca_neue_progress_primary:I = 0x7f020a78

.field public static final orca_neue_progress_secondary:I = 0x7f020a79

.field public static final orca_neue_pull_to_refresh_arrow:I = 0x7f020a7a

.field public static final orca_neue_receipt_checkmark_icon:I = 0x7f020a7b

.field public static final orca_neue_reflex_listitem_background:I = 0x7f020a7c

.field public static final orca_neue_reply_arrow:I = 0x7f020a7d

.field public static final orca_neue_secondary_button:I = 0x7f020a7e

.field public static final orca_neue_secondary_button_enabled:I = 0x7f020a7f

.field public static final orca_neue_secondary_button_pressed:I = 0x7f020a80

.field public static final orca_neue_sticker_store_tab:I = 0x7f020a81

.field public static final orca_neue_stickers_popup_placeholder:I = 0x7f020a82

.field public static final orca_neue_stickers_promoted_tab_icon:I = 0x7f020a83

.field public static final orca_neue_stickers_store_pack_delete:I = 0x7f020a84

.field public static final orca_neue_stickers_store_pack_delete_normal:I = 0x7f020a85

.field public static final orca_neue_stickers_store_pack_delete_pressed:I = 0x7f020a86

.field public static final orca_neue_stickers_store_pack_download:I = 0x7f020a87

.field public static final orca_neue_stickers_store_pack_download_disabled:I = 0x7f020a88

.field public static final orca_neue_stickers_store_pack_download_normal:I = 0x7f020a89

.field public static final orca_neue_stickers_store_pack_download_pressed:I = 0x7f020a8a

.field public static final orca_neue_stickers_store_pack_downloaded:I = 0x7f020a8b

.field public static final orca_neue_stickers_store_pack_reorder:I = 0x7f020a8c

.field public static final orca_neue_stickers_store_placeholder:I = 0x7f020a8d

.field public static final orca_new_group_tile:I = 0x7f020a8e

.field public static final orca_new_message_pill_arrow:I = 0x7f020a8f

.field public static final orca_normal_sheet_background:I = 0x7f020a90

.field public static final orca_notification_icon:I = 0x7f020a91

.field public static final orca_notification_icon_chathead:I = 0x7f020a92

.field public static final orca_nux_close:I = 0x7f020a93

.field public static final orca_nux_close_normal:I = 0x7f020a94

.field public static final orca_nux_close_pressed:I = 0x7f020a95

.field public static final orca_nux_location:I = 0x7f020a96

.field public static final orca_nux_location_nub:I = 0x7f020a97

.field public static final orca_nux_location_popover:I = 0x7f020a98

.field public static final orca_online_icon:I = 0x7f020a99

.field public static final orca_online_icon_chat_head_title:I = 0x7f020a9a

.field public static final orca_online_icon_thread:I = 0x7f020a9b

.field public static final orca_panel_background:I = 0x7f020a9c

.field public static final orca_people_birthday:I = 0x7f020a9d

.field public static final orca_phone_unknown_contact:I = 0x7f020a9e

.field public static final orca_photo_downloading:I = 0x7f020a9f

.field public static final orca_photo_placeholder_dark:I = 0x7f020aa0

.field public static final orca_quick_cam_progress_cancel_drawable:I = 0x7f020aa1

.field public static final orca_quick_cam_progress_drawable:I = 0x7f020aa2

.field public static final orca_quick_cam_tooltip_video_icon:I = 0x7f020aa3

.field public static final orca_quick_cam_video_dot:I = 0x7f020aa4

.field public static final orca_receipt_checkmark_icon:I = 0x7f020aa5

.field public static final orca_record_banner:I = 0x7f020aa6

.field public static final orca_record_banner_neue:I = 0x7f020aa7

.field public static final orca_record_button_normal:I = 0x7f020aa8

.field public static final orca_record_button_normal_neue:I = 0x7f020aa9

.field public static final orca_record_button_pressed:I = 0x7f020aaa

.field public static final orca_record_button_pressed_neue:I = 0x7f020aab

.field public static final orca_record_button_selected:I = 0x7f020aac

.field public static final orca_record_button_selected_neue:I = 0x7f020aad

.field public static final orca_record_done_button:I = 0x7f020aae

.field public static final orca_record_done_button_neue:I = 0x7f020aaf

.field public static final orca_record_done_button_normal:I = 0x7f020ab0

.field public static final orca_record_done_button_normal_neue:I = 0x7f020ab1

.field public static final orca_record_done_button_pressed:I = 0x7f020ab2

.field public static final orca_record_done_button_pressed_neue:I = 0x7f020ab3

.field public static final orca_recorder_popup_red:I = 0x7f020ab4

.field public static final orca_recorder_popup_red_neue:I = 0x7f020ab5

.field public static final orca_recorder_popup_white:I = 0x7f020ab6

.field public static final orca_recorder_popup_white_neue:I = 0x7f020ab7

.field public static final orca_remove_favorite_button:I = 0x7f020ab8

.field public static final orca_reply_arrow:I = 0x7f020ab9

.field public static final orca_share_attachment_link_background:I = 0x7f020aba

.field public static final orca_share_attachment_link_background_focused:I = 0x7f020abb

.field public static final orca_share_attachment_link_background_normal:I = 0x7f020abc

.field public static final orca_share_attachment_link_background_pressed:I = 0x7f020abd

.field public static final orca_sheet_button:I = 0x7f020abe

.field public static final orca_sheet_button_normal:I = 0x7f020abf

.field public static final orca_sheet_button_pressed:I = 0x7f020ac0

.field public static final orca_sticker_store_progress_bar:I = 0x7f020ac1

.field public static final orca_sticker_store_tab:I = 0x7f020ac2

.field public static final orca_sticker_tag_item_bubble:I = 0x7f020ac3

.field public static final orca_stickers_arrow:I = 0x7f020ac4

.field public static final orca_stickers_arrow_reversed:I = 0x7f020ac5

.field public static final orca_stickers_off:I = 0x7f020ac6

.field public static final orca_stickers_on:I = 0x7f020ac7

.field public static final orca_stickers_popup_placeholder:I = 0x7f020ac8

.field public static final orca_stickers_promoted_tab_icon:I = 0x7f020ac9

.field public static final orca_stickers_recent_tab:I = 0x7f020aca

.field public static final orca_stickers_search_tab:I = 0x7f020acb

.field public static final orca_stickers_store_pack_delete:I = 0x7f020acc

.field public static final orca_stickers_store_pack_delete_normal:I = 0x7f020acd

.field public static final orca_stickers_store_pack_delete_pressed:I = 0x7f020ace

.field public static final orca_stickers_store_pack_download:I = 0x7f020acf

.field public static final orca_stickers_store_pack_download_disabled:I = 0x7f020ad0

.field public static final orca_stickers_store_pack_download_normal:I = 0x7f020ad1

.field public static final orca_stickers_store_pack_download_pressed:I = 0x7f020ad2

.field public static final orca_stickers_store_pack_downloaded:I = 0x7f020ad3

.field public static final orca_stickers_store_pack_reorder:I = 0x7f020ad4

.field public static final orca_stickers_store_placeholder:I = 0x7f020ad5

.field public static final orca_stickers_store_tab:I = 0x7f020ad6

.field public static final orca_stickers_store_tab_new:I = 0x7f020ad7

.field public static final orca_tab_selected:I = 0x7f020ad8

.field public static final orca_textfield_search_right_selected:I = 0x7f020ad9

.field public static final orca_textfield_search_selected:I = 0x7f020ada

.field public static final orca_textfield_searchview:I = 0x7f020adb

.field public static final orca_textfield_searchview_right:I = 0x7f020adc

.field public static final orca_thread_error:I = 0x7f020add

.field public static final orca_thread_error_neue:I = 0x7f020ade

.field public static final orca_thread_list_item_unread_background:I = 0x7f020adf

.field public static final orca_thread_list_read_item_background:I = 0x7f020ae0

.field public static final orca_title_bar_foreground:I = 0x7f020ae1

.field public static final orca_title_bar_top_button:I = 0x7f020ae2

.field public static final orca_title_bar_top_button_focused:I = 0x7f020ae3

.field public static final orca_title_bar_top_button_normal:I = 0x7f020ae4

.field public static final orca_title_bar_top_button_pressed:I = 0x7f020ae5

.field public static final orca_two_line_composer_background_chat_heads_without_border:I = 0x7f020ae6

.field public static final orca_two_line_composer_covering_background:I = 0x7f020ae7

.field public static final orca_two_line_composer_covering_background_chat_heads:I = 0x7f020ae8

.field public static final orca_unread_message_pill_arrow:I = 0x7f020ae9

.field public static final orca_video_message_item_background:I = 0x7f020aea

.field public static final orca_video_message_item_cancel:I = 0x7f020aeb

.field public static final orca_video_message_item_play:I = 0x7f020aec

.field public static final orca_video_message_item_retry:I = 0x7f020aed

.field public static final orca_voice_bubble_left_normal:I = 0x7f020aee

.field public static final orca_voice_bubble_left_normal_button:I = 0x7f020aef

.field public static final orca_voice_bubble_left_normal_down:I = 0x7f020af0

.field public static final orca_voice_bubble_left_selected:I = 0x7f020af1

.field public static final orca_voice_bubble_left_selected_button:I = 0x7f020af2

.field public static final orca_voice_bubble_left_selected_down:I = 0x7f020af3

.field public static final orca_voice_bubble_right_normal:I = 0x7f020af4

.field public static final orca_voice_bubble_right_normal_button:I = 0x7f020af5

.field public static final orca_voice_bubble_right_normal_down:I = 0x7f020af6

.field public static final orca_voice_bubble_right_selected:I = 0x7f020af7

.field public static final orca_voice_bubble_right_selected_button:I = 0x7f020af8

.field public static final orca_voice_bubble_right_selected_down:I = 0x7f020af9

.field public static final orca_voice_pattern_left_normal:I = 0x7f020afa

.field public static final orca_voice_pattern_left_selected:I = 0x7f020afb

.field public static final orca_voice_pattern_normal_neue:I = 0x7f020afc

.field public static final orca_voice_pattern_right_normal:I = 0x7f020afd

.field public static final orca_voice_pattern_right_selected:I = 0x7f020afe

.field public static final orca_voice_pattern_selected_neue:I = 0x7f020aff

.field public static final orca_voice_pause_left_normal:I = 0x7f020b00

.field public static final orca_voice_pause_left_selected:I = 0x7f020b01

.field public static final orca_voice_pause_normal_neue:I = 0x7f020b02

.field public static final orca_voice_pause_right_normal:I = 0x7f020b03

.field public static final orca_voice_pause_right_selected:I = 0x7f020b04

.field public static final orca_voice_pause_selected_neue:I = 0x7f020b05

.field public static final orca_voice_play_left_normal:I = 0x7f020b06

.field public static final orca_voice_play_left_selected:I = 0x7f020b07

.field public static final orca_voice_play_normal_neue:I = 0x7f020b08

.field public static final orca_voice_play_right_normal:I = 0x7f020b09

.field public static final orca_voice_play_right_selected:I = 0x7f020b0a

.field public static final orca_voice_play_selected_neue:I = 0x7f020b0b

.field public static final orca_voice_timerbg_left_normal:I = 0x7f020b0c

.field public static final orca_voice_timerbg_left_selected:I = 0x7f020b0d

.field public static final orca_voice_timerbg_normal_neue:I = 0x7f020b0e

.field public static final orca_voice_timerbg_right_normal:I = 0x7f020b0f

.field public static final orca_voice_timerbg_right_selected:I = 0x7f020b10

.field public static final orca_voice_timerbg_selected_neue:I = 0x7f020b11

.field public static final orca_volume_0:I = 0x7f020b12

.field public static final orca_volume_0_neue:I = 0x7f020b13

.field public static final orca_volume_1:I = 0x7f020b14

.field public static final orca_volume_1_neue:I = 0x7f020b15

.field public static final orca_volume_2:I = 0x7f020b16

.field public static final orca_volume_2_neue:I = 0x7f020b17

.field public static final orca_volume_3:I = 0x7f020b18

.field public static final orca_volume_3_neue:I = 0x7f020b19

.field public static final orca_volume_4:I = 0x7f020b1a

.field public static final orca_volume_4_neue:I = 0x7f020b1b

.field public static final orca_warning_sheet_background:I = 0x7f020b1c

.field public static final orca_warning_sheet_shadow:I = 0x7f020b1d

.field public static final order_review_order_totals_border:I = 0x7f020b1e

.field public static final pa_comment:I = 0x7f020b1f

.field public static final pa_find_friends:I = 0x7f020b20

.field public static final pa_find_friends_kddi:I = 0x7f020b21

.field public static final page_activity_uni_status_active_icon:I = 0x7f020b22

.field public static final page_activity_uni_status_paused_icon:I = 0x7f020b23

.field public static final page_admin_upsell_leav_app_icon:I = 0x7f020b24

.field public static final page_chevron_background:I = 0x7f020b25

.field public static final page_chevron_fg:I = 0x7f020b26

.field public static final page_collection_chevron:I = 0x7f020b27

.field public static final page_dot_active:I = 0x7f020b28

.field public static final page_dot_inactive:I = 0x7f020b29

.field public static final page_identity_action_sheet_button_bg:I = 0x7f020b2a

.field public static final page_identity_action_sheet_button_refresh_bg:I = 0x7f020b2b

.field public static final page_identity_action_sheet_like_icon_selector:I = 0x7f020b2c

.field public static final page_identity_action_sheet_save_icon_selector:I = 0x7f020b2d

.field public static final page_identity_action_sheet_separator:I = 0x7f020b2e

.field public static final page_identity_card_clickable_highlighted_unit_background:I = 0x7f020b2f

.field public static final page_identity_carousel_image_gradient:I = 0x7f020b30

.field public static final page_identity_checkin_icon:I = 0x7f020b31

.field public static final page_identity_chevron_header_bg_inset:I = 0x7f020b32

.field public static final page_identity_chevron_header_gradient_clickable_background:I = 0x7f020b33

.field public static final page_identity_context_items_container_border:I = 0x7f020b34

.field public static final page_identity_cover_gradient:I = 0x7f020b35

.field public static final page_identity_error_icon:I = 0x7f020b36

.field public static final page_identity_full_width_card_background:I = 0x7f020b37

.field public static final page_identity_generic_cover_placeholder:I = 0x7f020b38

.field public static final page_identity_header_profile_pic_border:I = 0x7f020b39

.field public static final page_identity_impressum_icon:I = 0x7f020b3a

.field public static final page_identity_like_icon:I = 0x7f020b3b

.field public static final page_identity_like_icon_selected:I = 0x7f020b3c

.field public static final page_identity_list_view_header:I = 0x7f020b3d

.field public static final page_identity_local_cover_placeholder:I = 0x7f020b3e

.field public static final page_identity_map_location:I = 0x7f020b3f

.field public static final page_identity_message_icon:I = 0x7f020b40

.field public static final page_identity_more_icon:I = 0x7f020b41

.field public static final page_identity_overall_blue_rating_pill:I = 0x7f020b42

.field public static final page_identity_photo_pressed_state:I = 0x7f020b43

.field public static final page_identity_pinned_icon:I = 0x7f020b44

.field public static final page_identity_recommendation_border:I = 0x7f020b45

.field public static final page_identity_save_icon:I = 0x7f020b46

.field public static final page_identity_save_icon_selected:I = 0x7f020b47

.field public static final page_identity_saved_check_icon:I = 0x7f020b48

.field public static final page_identity_saved_lock_icon:I = 0x7f020b49

.field public static final page_identity_share_icon:I = 0x7f020b4a

.field public static final page_identity_structured_content_divider:I = 0x7f020b4b

.field public static final page_identity_structured_content_genre:I = 0x7f020b4c

.field public static final page_identity_structured_content_people:I = 0x7f020b4d

.field public static final page_identity_structured_content_person:I = 0x7f020b4e

.field public static final page_identity_timeline_card_background:I = 0x7f020b4f

.field public static final page_identity_tv_airings_vertical_separator:I = 0x7f020b50

.field public static final page_identity_vertex_header_about_text_view:I = 0x7f020b51

.field public static final page_information_payment_option_icon_amex:I = 0x7f020b52

.field public static final page_information_payment_option_icon_discover:I = 0x7f020b53

.field public static final page_information_payment_option_icon_mastercard:I = 0x7f020b54

.field public static final page_information_payment_option_icon_visa:I = 0x7f020b55

.field public static final page_information_send_email_icon:I = 0x7f020b56

.field public static final page_information_service_checkmark:I = 0x7f020b57

.field public static final page_information_visit_website_icon:I = 0x7f020b58

.field public static final page_review_cover_photo_gradient:I = 0x7f020b59

.field public static final page_review_profile_picture_border:I = 0x7f020b5a

.field public static final page_ui_card_clickable_unit_background:I = 0x7f020b5b

.field public static final page_ui_card_clickable_unit_background_refresh:I = 0x7f020b5c

.field public static final page_ui_chevron:I = 0x7f020b5d

.field public static final page_verified:I = 0x7f020b5e

.field public static final pages_browser_clickable_unit_background:I = 0x7f020b5f

.field public static final pages_manager_invite_friends:I = 0x7f020b60

.field public static final pages_manager_posts_by_others:I = 0x7f020b61

.field public static final pages_roe_footer_text_background:I = 0x7f020b62

.field public static final pages_silhouette_100:I = 0x7f020b63

.field public static final payment_help:I = 0x7f020b64

.field public static final payment_pin_dots_layer:I = 0x7f020b65

.field public static final payment_pin_preferences_top_border:I = 0x7f020b66

.field public static final payment_pin_underline_shape:I = 0x7f020b67

.field public static final payment_preference_divider:I = 0x7f020b68

.field public static final payment_receipt_overline_border:I = 0x7f020b69

.field public static final payment_receipt_underline_border:I = 0x7f020b6a

.field public static final payment_send_money_icon:I = 0x7f020b6b

.field public static final payment_send_money_receiver_border:I = 0x7f020b6c

.field public static final pencil_default:I = 0x7f020b6d

.field public static final pencil_icon:I = 0x7f020b6e

.field public static final pencil_image:I = 0x7f020b6f

.field public static final pencil_pressed:I = 0x7f020b70

.field public static final pending:I = 0x7f020b71

.field public static final permalink_add_reply_button_disabled:I = 0x7f020b72

.field public static final permalink_add_reply_button_enabled:I = 0x7f020b73

.field public static final permalink_add_reply_button_pressed:I = 0x7f020b74

.field public static final permalink_add_reply_entry_field:I = 0x7f020b75

.field public static final permalink_comment_button_background:I = 0x7f020b76

.field public static final person:I = 0x7f020b77

.field public static final person_card_footer_bg_normal:I = 0x7f020b78

.field public static final person_card_footer_bg_pressed:I = 0x7f020b79

.field public static final person_card_footer_bg_with_state:I = 0x7f020b7a

.field public static final person_card_header_bg_with_state:I = 0x7f020b7b

.field public static final phone_large:I = 0x7f020b7c

.field public static final phone_small:I = 0x7f020b7d

.field public static final photo_action_icon_album:I = 0x7f020b7e

.field public static final photo_action_icon_bug:I = 0x7f020b7f

.field public static final photo_action_icon_delete:I = 0x7f020b80

.field public static final photo_action_icon_info:I = 0x7f020b81

.field public static final photo_action_icon_logout:I = 0x7f020b82

.field public static final photo_action_icon_message:I = 0x7f020b83

.field public static final photo_action_icon_more:I = 0x7f020b84

.field public static final photo_action_icon_profile:I = 0x7f020b85

.field public static final photo_action_icon_refresh:I = 0x7f020b86

.field public static final photo_action_icon_settings:I = 0x7f020b87

.field public static final photo_action_icon_share:I = 0x7f020b88

.field public static final photo_button_selector:I = 0x7f020b89

.field public static final photo_downloading:I = 0x7f020b8a

.field public static final photo_gradient:I = 0x7f020b8b

.field public static final photo_overlay_btn_left:I = 0x7f020b8c

.field public static final photo_overlay_btn_pressed:I = 0x7f020b8d

.field public static final photo_overlay_btn_right:I = 0x7f020b8e

.field public static final photo_placeholder_dark:I = 0x7f020b8f

.field public static final photo_tag_suggestion_bg:I = 0x7f020b90

.field public static final picker_checkmark_hint:I = 0x7f020b91

.field public static final picker_grid_icon_states:I = 0x7f020b92

.field public static final picker_no_images_hint:I = 0x7f020b93

.field public static final picker_no_photos:I = 0x7f020b94

.field public static final picker_play_video:I = 0x7f020b95

.field public static final pin_blue_l:I = 0x7f020b96

.field public static final pin_dark_grey_l:I = 0x7f020b97

.field public static final pin_dark_grey_m:I = 0x7f020b98

.field public static final pin_dot:I = 0x7f020b99

.field public static final pin_light_grey_l:I = 0x7f020b9a

.field public static final pinned_post:I = 0x7f020b9b

.field public static final pinterest:I = 0x7f020b9c

.field public static final pinterest_white:I = 0x7f020b9d

.field public static final pivot_feed_popover_content_background:I = 0x7f020b9e

.field public static final pivot_pager_dot_shape:I = 0x7f020b9f

.field public static final place_card_info:I = 0x7f020ba0

.field public static final place_dark_grey_l:I = 0x7f020ba1

.field public static final place_dark_grey_m:I = 0x7f020ba2

.field public static final place_default_icon:I = 0x7f020ba3

.field public static final placeholder_video_image:I = 0x7f020ba4

.field public static final places_checkin_clear_search:I = 0x7f020ba5

.field public static final places_checkin_suggestion_close_out:I = 0x7f020ba6

.field public static final places_checkin_suggestion_pink_icon:I = 0x7f020ba7

.field public static final places_glyph_checkin:I = 0x7f020ba8

.field public static final places_glyph_placepin:I = 0x7f020ba9

.field public static final plus_icon:I = 0x7f020baa

.field public static final plus_light_grey_m:I = 0x7f020bab

.field public static final plus_white_s:I = 0x7f020bac

.field public static final plutonium_context_item_badge_bg:I = 0x7f020bad

.field public static final plutonium_context_item_bg:I = 0x7f020bae

.field public static final plutonium_coverphoto_vignette_bg:I = 0x7f020baf

.field public static final plutonium_item_bg:I = 0x7f020bb0

.field public static final plutonium_profile_pic_bg:I = 0x7f020bb1

.field public static final plutonium_timeline_covermedia_vignette_bg:I = 0x7f020bb2

.field public static final plutonium_timeline_navitem:I = 0x7f020bb3

.field public static final plutonium_timeline_navitem_pressed:I = 0x7f020bb4

.field public static final plutonium_timeline_navtile_placeholder_about:I = 0x7f020bb5

.field public static final plutonium_timeline_navtile_placeholder_friends:I = 0x7f020bb6

.field public static final plutonium_timeline_navtile_placeholder_photos:I = 0x7f020bb7

.field public static final popover_arrow_down:I = 0x7f020bb8

.field public static final popover_arrow_fullscreen:I = 0x7f020bb9

.field public static final popover_arrow_up:I = 0x7f020bba

.field public static final popover_content_background:I = 0x7f020bbb

.field public static final popover_footer_background:I = 0x7f020bbc

.field public static final popover_outside:I = 0x7f020bbd

.field public static final post_dark_grey_l:I = 0x7f020bbe

.field public static final post_date_color:I = 0x7f020e61

.field public static final post_play_like_icon:I = 0x7f020bbf

.field public static final post_post_background:I = 0x7f020bc0

.field public static final postplay_play_icon:I = 0x7f020bc1

.field public static final powered_by_google_dark:I = 0x7f020bc2

.field public static final powered_by_google_light:I = 0x7f020bc3

.field public static final premium_videos_item_blingbar_bg:I = 0x7f020bc4

.field public static final pressable_text_color_states:I = 0x7f020bc5

.field public static final preview_camera_button_states:I = 0x7f020bc6

.field public static final price_dark_grey_s:I = 0x7f020bc7

.field public static final price_tag:I = 0x7f020bc8

.field public static final price_tag_corner:I = 0x7f020bc9

.field public static final privacy_acquaintance:I = 0x7f020bca

.field public static final privacy_blank:I = 0x7f020bcb

.field public static final privacy_checkmark:I = 0x7f020bcc

.field public static final privacy_close_friends:I = 0x7f020bcd

.field public static final privacy_custom:I = 0x7f020bce

.field public static final privacy_event:I = 0x7f020bcf

.field public static final privacy_everyone:I = 0x7f020bd0

.field public static final privacy_facebook:I = 0x7f020bd1

.field public static final privacy_family:I = 0x7f020bd2

.field public static final privacy_friends:I = 0x7f020bd3

.field public static final privacy_friends_of_friends:I = 0x7f020bd4

.field public static final privacy_groups:I = 0x7f020bd5

.field public static final privacy_list:I = 0x7f020bd6

.field public static final privacy_location:I = 0x7f020bd7

.field public static final privacy_only_me:I = 0x7f020bd8

.field public static final privacy_school:I = 0x7f020bd9

.field public static final privacy_scope_acquaintances:I = 0x7f020bda

.field public static final privacy_scope_close_friends:I = 0x7f020bdb

.field public static final privacy_scope_custom:I = 0x7f020bdc

.field public static final privacy_scope_event:I = 0x7f020bdd

.field public static final privacy_scope_everyone:I = 0x7f020bde

.field public static final privacy_scope_facebook:I = 0x7f020bdf

.field public static final privacy_scope_family:I = 0x7f020be0

.field public static final privacy_scope_friends:I = 0x7f020be1

.field public static final privacy_scope_friends_except_acquaintances:I = 0x7f020be2

.field public static final privacy_scope_friends_of_friends:I = 0x7f020be3

.field public static final privacy_scope_group:I = 0x7f020be4

.field public static final privacy_scope_list:I = 0x7f020be5

.field public static final privacy_scope_location:I = 0x7f020be6

.field public static final privacy_scope_only_me:I = 0x7f020be7

.field public static final privacy_scope_school:I = 0x7f020be8

.field public static final privacy_scope_work:I = 0x7f020be9

.field public static final privacy_shortcuts:I = 0x7f020bea

.field public static final privacy_work:I = 0x7f020beb

.field public static final processing:I = 0x7f020bec

.field public static final products_filter_check:I = 0x7f020bed

.field public static final profile_crop_border:I = 0x7f020bee

.field public static final profile_crop_square:I = 0x7f020bef

.field public static final profile_info_clear_icon:I = 0x7f020bf0

.field public static final profile_info_item_background_bottom:I = 0x7f020bf1

.field public static final profile_info_item_background_center:I = 0x7f020bf2

.field public static final profile_info_item_background_top:I = 0x7f020bf3

.field public static final profile_info_request_button_text:I = 0x7f020bf4

.field public static final profile_photo_background_color:I = 0x7f020e63

.field public static final profile_pic_edit_icon:I = 0x7f020bf5

.field public static final profile_pic_highlight_no_shadow:I = 0x7f020bf6

.field public static final profile_picture_background:I = 0x7f020bf7

.field public static final profilepic_overlay:I = 0x7f020bf8

.field public static final progress_bar:I = 0x7f020bf9

.field public static final progress_indeterminate_horizontal:I = 0x7f020bfa

.field public static final progress_knob:I = 0x7f020bfb

.field public static final progress_large_holo:I = 0x7f020bfc

.field public static final progress_medium_holo:I = 0x7f020bfd

.field public static final progress_shadow:I = 0x7f020bfe

.field public static final progress_small_holo:I = 0x7f020bff

.field public static final promotion_budget_knob:I = 0x7f020c00

.field public static final promotion_budget_setter_background:I = 0x7f020c01

.field public static final promotion_budget_slider:I = 0x7f020c02

.field public static final publisher_btn_background:I = 0x7f020c03

.field public static final publisher_button_background:I = 0x7f020c04

.field public static final publisher_checkin_icon:I = 0x7f020c05

.field public static final publisher_checkin_icon_harrison:I = 0x7f020c06

.field public static final publisher_gradient_background:I = 0x7f020c07

.field public static final publisher_gradient_background_pressed:I = 0x7f020c08

.field public static final publisher_gradient_bg:I = 0x7f020c09

.field public static final publisher_groupchat_icon_harrison:I = 0x7f020c0a

.field public static final publisher_photo_icon:I = 0x7f020c0b

.field public static final publisher_photo_icon_harrison:I = 0x7f020c0c

.field public static final publisher_status_icon:I = 0x7f020c0d

.field public static final publisher_status_icon_harrison:I = 0x7f020c0e

.field public static final pull_to_refresh_arrow_angora:I = 0x7f020c0f

.field public static final push_notification_item_background:I = 0x7f020c10

.field public static final push_notification_item_background_shadow:I = 0x7f020c11

.field public static final qp_card_background:I = 0x7f020c12

.field public static final qp_dialog_card_x:I = 0x7f020c13

.field public static final qp_divebar_x:I = 0x7f020c14

.field public static final qp_interstitial_x:I = 0x7f020c15

.field public static final qp_messenger_mask_100:I = 0x7f020c16

.field public static final qp_voip_card_x:I = 0x7f020c17

.field public static final qr_code_background:I = 0x7f020c18

.field public static final qr_code_scanner_background:I = 0x7f020c19

.field public static final qr_scanner_guide:I = 0x7f020c1a

.field public static final qr_scanner_guide_success:I = 0x7f020c1b

.field public static final query_list_checkmark:I = 0x7f020c1c

.field public static final query_list_radio:I = 0x7f020c1d

.field public static final query_list_selector:I = 0x7f020c1e

.field public static final questionmark:I = 0x7f020c1f

.field public static final radio_white_selected:I = 0x7f020c20

.field public static final rating_pill_1:I = 0x7f020c21

.field public static final rating_pill_2:I = 0x7f020c22

.field public static final rating_pill_3:I = 0x7f020c23

.field public static final rating_pill_4:I = 0x7f020c24

.field public static final rating_pill_5:I = 0x7f020c25

.field public static final reaction_card_bottom_wash:I = 0x7f020c26

.field public static final reaction_card_bottom_white:I = 0x7f020c27

.field public static final reaction_card_footer_background:I = 0x7f020c28

.field public static final reaction_card_separator_background:I = 0x7f020c29

.field public static final reaction_card_top_primary_color:I = 0x7f020c2a

.field public static final reaction_card_white_background:I = 0x7f020c2b

.field public static final reaction_cover_photo_gradient:I = 0x7f020c2c

.field public static final reaction_glyph_checkmark_white_m:I = 0x7f020c2d

.field public static final reaction_header_gradient:I = 0x7f020c2e

.field public static final reaction_photo_overlay:I = 0x7f020c2f

.field public static final reaction_single_photo_text_gradient:I = 0x7f020c30

.field public static final reconnect_to_facebook:I = 0x7f020c31

.field public static final record_button_shape:I = 0x7f020c32

.field public static final recording_button_shape:I = 0x7f020c33

.field public static final red_exclamation_mark:I = 0x7f020c34

.field public static final redirect_to_messenger_background:I = 0x7f020c35

.field public static final reflex_transparent_buffer:I = 0x7f020c36

.field public static final refresh_button:I = 0x7f020c37

.field public static final refresh_button_pressed:I = 0x7f020c38

.field public static final refresh_circle_button:I = 0x7f020c39

.field public static final regular_search_button:I = 0x7f020c3a

.field public static final regular_search_button_pressed:I = 0x7f020c3b

.field public static final regular_search_button_unpressed:I = 0x7f020c3c

.field public static final related_video_big_play_icon:I = 0x7f020c3d

.field public static final related_video_play_icon:I = 0x7f020c3e

.field public static final related_videos_bg:I = 0x7f020c3f

.field public static final related_videos_carousel_loading_shimmer:I = 0x7f020c40

.field public static final remove_icon:I = 0x7f020c41

.field public static final remove_tags_icon:I = 0x7f020c42

.field public static final restaurant_dark_grey_s:I = 0x7f020c43

.field public static final retry_icon:I = 0x7f020c44

.field public static final retry_icon_pressed:I = 0x7f020c45

.field public static final review_compose_button_bg:I = 0x7f020c46

.field public static final review_press_state_rounded_bg:I = 0x7f020c47

.field public static final review_press_state_rounded_rect:I = 0x7f020c48

.field public static final ridge_interstitial_tv_contents:I = 0x7f020c49

.field public static final ridge_interstitial_tv_frame:I = 0x7f020c4a

.field public static final ridge_nux_grayed_out_result_bg:I = 0x7f020c4b

.field public static final ridge_nux_media_icon:I = 0x7f020c4c

.field public static final ridge_nux_phone_body:I = 0x7f020c4d

.field public static final ridge_nux_phone_gradient:I = 0x7f020c4e

.field public static final ridge_nux_phone_top:I = 0x7f020c4f

.field public static final ridge_nux_privacy_icon:I = 0x7f020c50

.field public static final ridge_nux_result_profile_pic:I = 0x7f020c51

.field public static final ridge_nux_working_icon:I = 0x7f020c52

.field public static final ridge_widget_spin_animation:I = 0x7f020c53

.field public static final rotate_button_states:I = 0x7f020c54

.field public static final rotate_icon:I = 0x7f020c55

.field public static final rotate_icon_pressed:I = 0x7f020c56

.field public static final rounded_bottom_row_highlight:I = 0x7f020c57

.field public static final rounded_button_blue_border:I = 0x7f020c58

.field public static final rounded_button_grey_border:I = 0x7f020c59

.field public static final rounded_corner_selector_overlay:I = 0x7f020c5a

.field public static final rounded_list_overlay:I = 0x7f020c5b

.field public static final save_button_selected:I = 0x7f020c5c

.field public static final save_sash_flat_off:I = 0x7f020c5d

.field public static final save_sash_flat_on:I = 0x7f020c5e

.field public static final save_sash_off:I = 0x7f020c5f

.field public static final saved_dashboard_action_button:I = 0x7f020c60

.field public static final saved_dashboard_bookmark_introduction:I = 0x7f020c61

.field public static final saved_dashboard_caret_introduction:I = 0x7f020c62

.field public static final saved_dashboard_icon_all:I = 0x7f020c63

.field public static final saved_dashboard_icon_archive:I = 0x7f020c64

.field public static final saved_dashboard_icon_books:I = 0x7f020c65

.field public static final saved_dashboard_icon_events:I = 0x7f020c66

.field public static final saved_dashboard_icon_links:I = 0x7f020c67

.field public static final saved_dashboard_icon_movies:I = 0x7f020c68

.field public static final saved_dashboard_icon_music:I = 0x7f020c69

.field public static final saved_dashboard_icon_places:I = 0x7f020c6a

.field public static final saved_dashboard_icon_tv:I = 0x7f020c6b

.field public static final saved_dashboard_large_saved_sash:I = 0x7f020c6c

.field public static final saved_dashboard_load_more:I = 0x7f020c6d

.field public static final saved_dashboard_menu_icon_archive:I = 0x7f020c6e

.field public static final saved_dashboard_menu_icon_delete:I = 0x7f020c6f

.field public static final saved_dashboard_menu_icon_review:I = 0x7f020c70

.field public static final saved_dashboard_menu_icon_share:I = 0x7f020c71

.field public static final saved_dashboard_menu_icon_view_post:I = 0x7f020c72

.field public static final saved_dashboard_undo_button:I = 0x7f020c73

.field public static final saved_small_sash:I = 0x7f020c74

.field public static final saved_small_sash_inset_tw2l:I = 0x7f020c75

.field public static final saved_small_sash_inset_tw3l:I = 0x7f020c76

.field public static final search:I = 0x7f020c77

.field public static final search_light_grey_s:I = 0x7f020c78

.field public static final secondary_actions_menu_bg:I = 0x7f020c79

.field public static final segment_left:I = 0x7f020c7a

.field public static final segment_left_normal:I = 0x7f020c7b

.field public static final segment_left_pressed:I = 0x7f020c7c

.field public static final segment_middle:I = 0x7f020c7d

.field public static final segment_middle_normal:I = 0x7f020c7e

.field public static final segment_middle_pressed:I = 0x7f020c7f

.field public static final segment_right:I = 0x7f020c80

.field public static final segment_right_normal:I = 0x7f020c81

.field public static final segment_right_pressed:I = 0x7f020c82

.field public static final select_home:I = 0x7f020c83

.field public static final selection_mark:I = 0x7f020c84

.field public static final selector_spinner_background:I = 0x7f020c85

.field public static final selfie_cam_hscroll:I = 0x7f020c86

.field public static final send_button_background:I = 0x7f020c87

.field public static final send_button_background_normal:I = 0x7f020c88

.field public static final send_button_background_pressed:I = 0x7f020c89

.field public static final send_money:I = 0x7f020c8a

.field public static final send_money_disabled:I = 0x7f020c8b

.field public static final settings_icon:I = 0x7f020c8c

.field public static final setup_system_notifications:I = 0x7f020c8d

.field public static final shadow_divider:I = 0x7f020c8e

.field public static final shadow_down:I = 0x7f020c8f

.field public static final share_background:I = 0x7f020c90

.field public static final share_icon:I = 0x7f020c91

.field public static final show_button_clickable_bg:I = 0x7f020c92

.field public static final show_cover_photo_icon:I = 0x7f020c93

.field public static final show_map_button_background:I = 0x7f020c94

.field public static final side_gradient:I = 0x7f020c95

.field public static final simple_picker_facebook_back:I = 0x7f020c96

.field public static final single_selection_mark:I = 0x7f020c97

.field public static final snowflake_card_shadow:I = 0x7f020c98

.field public static final snowflake_comment_button:I = 0x7f020c99

.field public static final snowflake_like_button:I = 0x7f020c9a

.field public static final snowflake_like_button_active:I = 0x7f020c9b

.field public static final snowflake_more_button:I = 0x7f020c9c

.field public static final snowflake_photo_loading_icon:I = 0x7f020c9d

.field public static final snowflake_photo_mask:I = 0x7f020c9e

.field public static final snowflake_photo_placeholder:I = 0x7f020c9f

.field public static final snowflake_share_button:I = 0x7f020ca0

.field public static final snowflake_tag_button:I = 0x7f020ca1

.field public static final sparkline:I = 0x7f020ca2

.field public static final spinner:I = 0x7f020ca3

.field public static final spinner_16_inner_holo:I = 0x7f020ca4

.field public static final spinner_48_inner_holo:I = 0x7f020ca5

.field public static final spinner_76_inner_holo:I = 0x7f020ca6

.field public static final standard_e2e2e0_border:I = 0x7f020ca7

.field public static final standard_white_bg_with_hit:I = 0x7f020ca8

.field public static final star_blue:I = 0x7f020ca9

.field public static final star_full_blue:I = 0x7f020caa

.field public static final star_full_empty:I = 0x7f020cab

.field public static final star_grey:I = 0x7f020cac

.field public static final star_icon:I = 0x7f020cad

.field public static final star_large_blue:I = 0x7f020cae

.field public static final star_large_empty:I = 0x7f020caf

.field public static final star_like_button_states:I = 0x7f020cb0

.field public static final star_medium_blue:I = 0x7f020cb1

.field public static final star_medium_blue_composite:I = 0x7f020cb2

.field public static final star_medium_blue_half:I = 0x7f020cb3

.field public static final star_medium_empty:I = 0x7f020cb4

.field public static final star_small_black:I = 0x7f020cb5

.field public static final star_small_blue:I = 0x7f020cb6

.field public static final star_small_blue_composite:I = 0x7f020cb7

.field public static final star_small_blue_half:I = 0x7f020cb8

.field public static final star_small_empty:I = 0x7f020cb9

.field public static final star_xlarge_blue:I = 0x7f020cba

.field public static final star_xlarge_empty:I = 0x7f020cbb

.field public static final status_card:I = 0x7f020cbc

.field public static final status_no_internet:I = 0x7f020cbd

.field public static final status_no_photo:I = 0x7f020cbe

.field public static final status_thumb_like_button_states:I = 0x7f020cbf

.field public static final sticker_hot_like_large:I = 0x7f020cc0

.field public static final sticker_hot_like_medium:I = 0x7f020cc1

.field public static final sticker_hot_like_small:I = 0x7f020cc2

.field public static final sticker_like:I = 0x7f020cc3

.field public static final sticker_popup_background:I = 0x7f020cc4

.field public static final sticker_store_tab_background:I = 0x7f020cc5

.field public static final sticky_favicon_f_active:I = 0x7f020cc6

.field public static final sticky_sys_notification_cancel:I = 0x7f020cc7

.field public static final subscriptions_blue_l:I = 0x7f020cc8

.field public static final subscriptions_blue_m:I = 0x7f020cc9

.field public static final subscriptions_dark_grey_l:I = 0x7f020cca

.field public static final subscriptions_dark_grey_m:I = 0x7f020ccb

.field public static final subscriptions_glyph_with_state:I = 0x7f020ccc

.field public static final subscriptions_light_grey_l:I = 0x7f020ccd

.field public static final subscriptions_light_grey_m:I = 0x7f020cce

.field public static final substory_feedback_bg_left_pressed:I = 0x7f020ccf

.field public static final substory_feedback_bg_middle_pressed:I = 0x7f020cd0

.field public static final substory_feedback_bg_right_pressed:I = 0x7f020cd1

.field public static final substory_feedback_bg_whole_pressed:I = 0x7f020cd2

.field public static final suggest_checkin_composer_background:I = 0x7f020cd3

.field public static final swipe_arrow_left:I = 0x7f020cd4

.field public static final swipe_arrow_right:I = 0x7f020cd5

.field public static final swipedown:I = 0x7f020cd6

.field public static final switch_thumb_selector:I = 0x7f020cd7

.field public static final switch_track_selector:I = 0x7f020cd8

.field public static final sysnotif_complete:I = 0x7f020cd9

.field public static final sysnotif_facebook:I = 0x7f020cda

.field public static final sysnotif_friend_request:I = 0x7f020cdb

.field public static final sysnotif_invite:I = 0x7f020cdc

.field public static final sysnotif_loading:I = 0x7f020cdd

.field public static final sysnotif_message:I = 0x7f020cde

.field public static final tab_background:I = 0x7f020cdf

.field public static final tab_badge_background:I = 0x7f020ce0

.field public static final tab_icon_bookmarks_white:I = 0x7f020ce1

.field public static final tab_icon_feed_white:I = 0x7f020ce2

.field public static final tab_icon_friends_white:I = 0x7f020ce3

.field public static final tab_icon_messages_white:I = 0x7f020ce4

.field public static final tab_icon_notifications_east_white:I = 0x7f020ce5

.field public static final tab_icon_notifications_white:I = 0x7f020ce6

.field public static final tab_icon_press_state:I = 0x7f020ce7

.field public static final tab_left:I = 0x7f020ce8

.field public static final tab_right:I = 0x7f020ce9

.field public static final tabbar_badge_background:I = 0x7f020cea

.field public static final tabbar_badge_background_blue:I = 0x7f020ceb

.field public static final tabbar_icon_bookmarks:I = 0x7f020cec

.field public static final tabbar_icon_bookmarks_pressed:I = 0x7f020ced

.field public static final tabbar_icon_bookmarks_pressed_white:I = 0x7f020cee

.field public static final tabbar_icon_bookmarks_white:I = 0x7f020cef

.field public static final tabbar_icon_feed:I = 0x7f020cf0

.field public static final tabbar_icon_feed_pressed:I = 0x7f020cf1

.field public static final tabbar_icon_feed_pressed_white:I = 0x7f020cf2

.field public static final tabbar_icon_feed_white:I = 0x7f020cf3

.field public static final tabbar_icon_friends:I = 0x7f020cf4

.field public static final tabbar_icon_friends_pressed:I = 0x7f020cf5

.field public static final tabbar_icon_friends_pressed_white:I = 0x7f020cf6

.field public static final tabbar_icon_friends_white:I = 0x7f020cf7

.field public static final tabbar_icon_messages:I = 0x7f020cf8

.field public static final tabbar_icon_messages_pressed:I = 0x7f020cf9

.field public static final tabbar_icon_messages_pressed_white:I = 0x7f020cfa

.field public static final tabbar_icon_messages_white:I = 0x7f020cfb

.field public static final tabbar_icon_notifications:I = 0x7f020cfc

.field public static final tabbar_icon_notifications_east_pressed_white:I = 0x7f020cfd

.field public static final tabbar_icon_notifications_east_white:I = 0x7f020cfe

.field public static final tabbar_icon_notifications_pressed:I = 0x7f020cff

.field public static final tabbar_icon_notifications_pressed_white:I = 0x7f020d00

.field public static final tabbar_icon_notifications_white:I = 0x7f020d01

.field public static final tabbar_icon_people_pressed_white:I = 0x7f020d02

.field public static final tabbar_icon_people_white:I = 0x7f020d03

.field public static final tabbar_newstories_glow:I = 0x7f020d04

.field public static final tabbar_publisher_btn_background:I = 0x7f020d05

.field public static final tag_bubble:I = 0x7f020d06

.field public static final tag_bubble_pressed:I = 0x7f020d07

.field public static final tag_icon:I = 0x7f020d08

.field public static final tag_icon_active_for_counter:I = 0x7f020d09

.field public static final tag_icon_active_new:I = 0x7f020d0a

.field public static final tag_icon_for_counter:I = 0x7f020d0b

.field public static final tag_icon_new:I = 0x7f020d0c

.field public static final tag_instruction_bg:I = 0x7f020d0d

.field public static final tag_touch_point:I = 0x7f020d0e

.field public static final tag_typeahead_x:I = 0x7f020d0f

.field public static final tag_vertical:I = 0x7f020d10

.field public static final tappable_transparent_background:I = 0x7f020d11

.field public static final tappable_white_background:I = 0x7f020d12

.field public static final terms_and_policies:I = 0x7f020d13

.field public static final text_color:I = 0x7f020e62

.field public static final text_color_left_button:I = 0x7f020d14

.field public static final text_color_right_button:I = 0x7f020d15

.field public static final text_marker:I = 0x7f020d16

.field public static final textfield_activated_holo_light:I = 0x7f020d17

.field public static final textfield_default_holo_light:I = 0x7f020d18

.field public static final textfield_disabled_focused_holo_light:I = 0x7f020d19

.field public static final textfield_disabled_holo_light:I = 0x7f020d1a

.field public static final textfield_focused_holo_light:I = 0x7f020d1b

.field public static final textfield_multiline_activated_holo_light:I = 0x7f020d1c

.field public static final textfield_multiline_default_holo_light:I = 0x7f020d1d

.field public static final textfield_multiline_disabled_focused_holo_light:I = 0x7f020d1e

.field public static final textfield_multiline_disabled_holo_light:I = 0x7f020d1f

.field public static final textfield_multiline_focused_holo_light:I = 0x7f020d20

.field public static final thread_item_unread_background_color:I = 0x7f020e51

.field public static final three_dots_dark_grey_l:I = 0x7f020d21

.field public static final three_dots_dark_grey_m:I = 0x7f020d22

.field public static final three_dots_dark_grey_s:I = 0x7f020d23

.field public static final three_dots_light_grey_l:I = 0x7f020d24

.field public static final three_dots_vertical_dark_grey_l:I = 0x7f020d25

.field public static final three_dots_vertical_light_grey_l:I = 0x7f020d26

.field public static final three_dots_vertical_white_l:I = 0x7f020d27

.field public static final three_dots_vertical_white_normal_l:I = 0x7f020d28

.field public static final three_dots_vertical_white_pressed_l:I = 0x7f020d29

.field public static final three_dots_white_l:I = 0x7f020d2a

.field public static final three_dots_white_m:I = 0x7f020d2b

.field public static final three_dots_white_s:I = 0x7f020d2c

.field public static final thumb_like_button_states:I = 0x7f020d2d

.field public static final tiled_card_background:I = 0x7f020d2e

.field public static final timeline_action_button_icon_check:I = 0x7f020d2f

.field public static final timeline_activity_log:I = 0x7f020d30

.field public static final timeline_activitylog_tab:I = 0x7f020d31

.field public static final timeline_btn_background:I = 0x7f020d32

.field public static final timeline_btn_normal:I = 0x7f020d33

.field public static final timeline_btn_pressed:I = 0x7f020d34

.field public static final timeline_check:I = 0x7f020d35

.field public static final timeline_checkbox:I = 0x7f020d36

.field public static final timeline_chevron:I = 0x7f020d37

.field public static final timeline_empty_cover_photo_bg:I = 0x7f020d38

.field public static final timeline_feed_item_bg_normal:I = 0x7f020d39

.field public static final timeline_feed_item_bg_pressed:I = 0x7f020d3a

.field public static final timeline_headershadow:I = 0x7f020d3b

.field public static final timeline_info_request_bgr:I = 0x7f020d3c

.field public static final timeline_list_item_bg:I = 0x7f020d3d

.field public static final timeline_more_context_items:I = 0x7f020d3e

.field public static final timeline_nav_item:I = 0x7f020d3f

.field public static final timeline_navtile:I = 0x7f020d40

.field public static final timeline_navtile_mask:I = 0x7f020d41

.field public static final timeline_pressable_feed_item_bg:I = 0x7f020d42

.field public static final timeline_profile:I = 0x7f020d43

.field public static final timeline_profile_question_bottom:I = 0x7f020d44

.field public static final timeline_profile_question_bottom_normal:I = 0x7f020d45

.field public static final timeline_profile_question_bottom_pressed:I = 0x7f020d46

.field public static final timeline_profile_question_checkmark:I = 0x7f020d47

.field public static final timeline_profile_question_middle:I = 0x7f020d48

.field public static final timeline_profile_question_middle_normal:I = 0x7f020d49

.field public static final timeline_profile_question_middle_pressed:I = 0x7f020d4a

.field public static final timeline_profile_question_option_background_bottom:I = 0x7f020d4b

.field public static final timeline_profile_question_option_background_center:I = 0x7f020d4c

.field public static final timeline_profile_question_option_background_top:I = 0x7f020d4d

.field public static final timeline_profile_question_single:I = 0x7f020d4e

.field public static final timeline_profile_question_single_normal:I = 0x7f020d4f

.field public static final timeline_profile_question_single_pressed:I = 0x7f020d50

.field public static final timeline_profile_question_top:I = 0x7f020d51

.field public static final timeline_profile_question_top_normal:I = 0x7f020d52

.field public static final timeline_profile_question_top_pressed:I = 0x7f020d53

.field public static final timeline_profile_questions_close_button:I = 0x7f020d54

.field public static final timeline_profile_silhouette:I = 0x7f020d55

.field public static final timeline_prompt_count_bg:I = 0x7f020d56

.field public static final timeline_publish_left:I = 0x7f020d57

.field public static final timeline_publish_left_normal:I = 0x7f020d58

.field public static final timeline_publish_left_pressed:I = 0x7f020d59

.field public static final timeline_publish_middle:I = 0x7f020d5a

.field public static final timeline_publish_middle_normal:I = 0x7f020d5b

.field public static final timeline_publish_middle_pressed:I = 0x7f020d5c

.field public static final timeline_publish_right:I = 0x7f020d5d

.field public static final timeline_publish_right_normal:I = 0x7f020d5e

.field public static final timeline_publish_right_pressed:I = 0x7f020d5f

.field public static final timeline_publisher_more_icon:I = 0x7f020d60

.field public static final timeline_section_header:I = 0x7f020d61

.field public static final timeline_year_overview_background:I = 0x7f020d62

.field public static final timeline_year_overview_see_more_bg:I = 0x7f020d63

.field public static final timeline_year_overview_see_more_bg_pressed:I = 0x7f020d64

.field public static final timeline_year_overview_see_more_button:I = 0x7f020d65

.field public static final timeline_year_overview_show_posts_bg:I = 0x7f020d66

.field public static final title_bar_background:I = 0x7f020d67

.field public static final title_bar_divider_gradient:I = 0x7f020d68

.field public static final title_open_menu_down:I = 0x7f020d69

.field public static final title_open_menu_icon_blue:I = 0x7f020d6a

.field public static final title_open_menu_icon_dark:I = 0x7f020d6b

.field public static final titlebar_pressable_button_bg_selector:I = 0x7f020d6c

.field public static final toggle_left_highlight:I = 0x7f020d6d

.field public static final toggle_left_normal:I = 0x7f020d6e

.field public static final toggle_left_pressed:I = 0x7f020d6f

.field public static final toggle_left_states:I = 0x7f020d70

.field public static final toggle_off:I = 0x7f020d71

.field public static final toggle_off_bg:I = 0x7f020d72

.field public static final toggle_on:I = 0x7f020d73

.field public static final toggle_on_bg:I = 0x7f020d74

.field public static final toggle_right_highlight:I = 0x7f020d75

.field public static final toggle_right_normal:I = 0x7f020d76

.field public static final toggle_right_pressed:I = 0x7f020d77

.field public static final toggle_right_states:I = 0x7f020d78

.field public static final token_field:I = 0x7f020d79

.field public static final token_field_blue:I = 0x7f020d7a

.field public static final token_field_selected_blue:I = 0x7f020d7b

.field public static final token_field_transparent:I = 0x7f020d7c

.field public static final tokenized_autocomplete_popup_background:I = 0x7f020d7d

.field public static final tools_color_scrubber:I = 0x7f020d7e

.field public static final tools_draw_icon:I = 0x7f020d7f

.field public static final tools_draw_icon_active:I = 0x7f020d80

.field public static final tools_draw_undo:I = 0x7f020d81

.field public static final tools_text_icon:I = 0x7f020d82

.field public static final tools_text_icon_active:I = 0x7f020d83

.field public static final tooltip_black_background:I = 0x7f020d84

.field public static final tooltip_black_bottomnub:I = 0x7f020d85

.field public static final tooltip_black_topnub:I = 0x7f020d86

.field public static final tooltip_blue_background:I = 0x7f020d87

.field public static final tooltip_blue_bottomnub:I = 0x7f020d88

.field public static final tooltip_blue_topnub:I = 0x7f020d89

.field public static final transparent_black_border:I = 0x7f020d8a

.field public static final transparent_drawable:I = 0x7f020d8b

.field public static final trash_icon:I = 0x7f020d8c

.field public static final travel_welcome_cover_photo_gradient:I = 0x7f020d8d

.field public static final triangle_corner_pressed_states:I = 0x7f020d8e

.field public static final tumblr:I = 0x7f020d8f

.field public static final tumblr_white:I = 0x7f020d90

.field public static final typeahead_divider:I = 0x7f020d91

.field public static final uberbar_add_friend_button:I = 0x7f020d92

.field public static final uberbar_add_friend_button_normal:I = 0x7f020d93

.field public static final uberbar_add_friend_button_pressed:I = 0x7f020d94

.field public static final uberbar_add_friend_checkmark:I = 0x7f020d95

.field public static final uberbar_call_button:I = 0x7f020d96

.field public static final uberbar_call_button_normal:I = 0x7f020d97

.field public static final uberbar_call_button_pressed:I = 0x7f020d98

.field public static final uberbar_like_button_normal:I = 0x7f020d99

.field public static final uberbar_like_button_pressed:I = 0x7f020d9a

.field public static final uberbar_like_page_button:I = 0x7f020d9b

.field public static final uberbar_like_page_checkmark:I = 0x7f020d9c

.field public static final uberbar_search_x:I = 0x7f020d9d

.field public static final uberbar_verified_badge:I = 0x7f020d9e

.field public static final ufi_attachment_remove_badge:I = 0x7f020d9f

.field public static final ufi_button_comment_status:I = 0x7f020da0

.field public static final ufi_button_comment_story:I = 0x7f020da1

.field public static final ufi_button_like_status:I = 0x7f020da2

.field public static final ufi_button_like_story:I = 0x7f020da3

.field public static final ufi_button_liked_status:I = 0x7f020da4

.field public static final ufi_button_liked_story:I = 0x7f020da5

.field public static final ufi_camera_icon:I = 0x7f020da6

.field public static final ufi_camera_icon_disabled:I = 0x7f020da7

.field public static final ufi_camera_icon_normal:I = 0x7f020da8

.field public static final ufi_comment_box_top_shadow:I = 0x7f020da9

.field public static final ufi_flyout_drag_handle:I = 0x7f020daa

.field public static final ufi_heart:I = 0x7f020dab

.field public static final ufi_heart_blue:I = 0x7f020dac

.field public static final ufi_icon_comment:I = 0x7f020dad

.field public static final ufi_icon_like:I = 0x7f020dae

.field public static final ufi_icon_like_pressed:I = 0x7f020daf

.field public static final ufi_icon_share:I = 0x7f020db0

.field public static final ufi_popover:I = 0x7f020db1

.field public static final ufi_popover_arrow:I = 0x7f020db2

.field public static final ufi_star:I = 0x7f020db3

.field public static final ufi_star_blue:I = 0x7f020db4

.field public static final ufiservices_comment_like_icon:I = 0x7f020db5

.field public static final ufiservices_comment_retry_press_state:I = 0x7f020db6

.field public static final ufiservices_flyout_action_button:I = 0x7f020db7

.field public static final ufiservices_flyout_action_button_background:I = 0x7f020db8

.field public static final ufiservices_flyout_action_button_pressed:I = 0x7f020db9

.field public static final ufiservices_flyout_edit_input:I = 0x7f020dba

.field public static final ufiservices_flyout_like_icon:I = 0x7f020dbb

.field public static final ufiservices_flyout_like_icon_pressed:I = 0x7f020dbc

.field public static final ufiservices_flyout_load_more_icon:I = 0x7f020dbd

.field public static final ufiservices_flyout_threaded_comment_background:I = 0x7f020dbe

.field public static final ufiservices_generic_press_state_background_rounded:I = 0x7f020dbf

.field public static final ufiservices_press_state_rounded_rect:I = 0x7f020dc0

.field public static final ufiservices_transparent_background_selector:I = 0x7f020dc1

.field public static final ui_flyout_body_bg:I = 0x7f020dc2

.field public static final underwood_add_photo_button_border:I = 0x7f020dc3

.field public static final underwood_more_arrow:I = 0x7f020dc4

.field public static final underwood_more_indicator_bg:I = 0x7f020dc5

.field public static final unread_message_pill_background:I = 0x7f020dc6

.field public static final unread_notification_background:I = 0x7f020dc7

.field public static final up_button_fblogo_normal:I = 0x7f020dc8

.field public static final up_button_fblogo_pressed:I = 0x7f020dc9

.field public static final up_gradient:I = 0x7f020dca

.field public static final updateinfo_dark_grey_l:I = 0x7f020dcb

.field public static final upload_progress_progress_repeat_horizontal0:I = 0x7f020dcc

.field public static final upload_progress_progress_repeat_horizontal1:I = 0x7f020dcd

.field public static final upload_progress_progress_repeat_horizontal2:I = 0x7f020dce

.field public static final upload_progress_progress_repeat_horizontal3:I = 0x7f020dcf

.field public static final upload_progress_progress_repeat_horizontal4:I = 0x7f020dd0

.field public static final upload_progress_progress_repeat_horizontal5:I = 0x7f020dd1

.field public static final upload_progress_progress_repeat_horizontal6:I = 0x7f020dd2

.field public static final upload_progress_progress_repeat_horizontal7:I = 0x7f020dd3

.field public static final url_image_darken_pressed_state:I = 0x7f020dd4

.field public static final url_image_progress_horizontal:I = 0x7f020dd5

.field public static final url_image_progress_indeterminate_horizontal:I = 0x7f020dd6

.field public static final url_image_upload_cover_color:I = 0x7f020e50

.field public static final url_image_upload_cover_transition:I = 0x7f020dd7

.field public static final urlimage_retry_button_states:I = 0x7f020dd8

.field public static final use_button_bg:I = 0x7f020dd9

.field public static final user_account_nux_step_profile_photo_button_list:I = 0x7f020dda

.field public static final uw_attachment_tag:I = 0x7f020ddb

.field public static final uw_attachment_tag_counter:I = 0x7f020ddc

.field public static final uw_nux_circle:I = 0x7f020ddd

.field public static final uw_remove_icon:I = 0x7f020dde

.field public static final vault_close_x:I = 0x7f020ddf

.field public static final vault_delete_progress:I = 0x7f020de0

.field public static final vault_fail_icon:I = 0x7f020de1

.field public static final vault_lock:I = 0x7f020de2

.field public static final vault_low_battery_icon:I = 0x7f020de3

.field public static final vault_nux_flyout:I = 0x7f020de4

.field public static final vault_nux_flyout_lock:I = 0x7f020de5

.field public static final vault_nux_help_button:I = 0x7f020de6

.field public static final vault_nux_help_icon:I = 0x7f020de7

.field public static final vault_nux_help_icon_pressed:I = 0x7f020de8

.field public static final vault_off_icon:I = 0x7f020de9

.field public static final vault_optin_computer:I = 0x7f020dea

.field public static final vault_optin_network:I = 0x7f020deb

.field public static final vault_optin_phone:I = 0x7f020dec

.field public static final vault_refresh:I = 0x7f020ded

.field public static final vault_refresh_button:I = 0x7f020dee

.field public static final vault_refresh_pressed:I = 0x7f020def

.field public static final vault_spinner:I = 0x7f020df0

.field public static final vault_wifi_icon:I = 0x7f020df1

.field public static final verified_badge_m:I = 0x7f020df2

.field public static final verified_badge_s:I = 0x7f020df3

.field public static final veyron_feed_item_bg_bottom:I = 0x7f020df4

.field public static final veyron_feed_item_bg_middle:I = 0x7f020df5

.field public static final veyron_feed_item_bg_middle_with_top_divider:I = 0x7f020df6

.field public static final veyron_feed_item_bg_top:I = 0x7f020df7

.field public static final veyron_feed_story_bg_bottom:I = 0x7f020df8

.field public static final veyron_feed_story_bg_middle:I = 0x7f020df9

.field public static final veyron_feed_story_bg_middle_with_top_divider:I = 0x7f020dfa

.field public static final veyron_feed_story_bg_top:I = 0x7f020dfb

.field public static final veyron_feed_story_chevron:I = 0x7f020dfc

.field public static final video_album_thumbnail_play_icon:I = 0x7f020dfd

.field public static final video_button:I = 0x7f020dfe

.field public static final video_button_pressed:I = 0x7f020dff

.field public static final video_camera_button:I = 0x7f020e00

.field public static final video_controls_pause:I = 0x7f020e01

.field public static final video_controls_play:I = 0x7f020e02

.field public static final video_cta_button_border:I = 0x7f020e03

.field public static final video_grid_icon:I = 0x7f020e04

.field public static final video_like_icon:I = 0x7f020e05

.field public static final video_trim_handle_left:I = 0x7f020e06

.field public static final video_trim_handle_left_normal:I = 0x7f020e07

.field public static final video_trim_handle_left_pressed:I = 0x7f020e08

.field public static final video_trim_handle_right:I = 0x7f020e09

.field public static final video_trim_handle_right_normal:I = 0x7f020e0a

.field public static final video_trim_handle_right_pressed:I = 0x7f020e0b

.field public static final video_trim_play_button:I = 0x7f020e0c

.field public static final video_trim_play_button_normal:I = 0x7f020e0d

.field public static final video_trim_play_button_pressed:I = 0x7f020e0e

.field public static final video_trim_scrubber:I = 0x7f020e0f

.field public static final video_trim_scrubber_normal:I = 0x7f020e10

.field public static final video_trim_scrubber_pressed:I = 0x7f020e11

.field public static final view_more_arrow_icon:I = 0x7f020e12

.field public static final view_photo_background:I = 0x7f020e13

.field public static final voip_answer_icon:I = 0x7f020e14

.field public static final voip_button_background:I = 0x7f020e15

.field public static final voip_button_toggled_background:I = 0x7f020e16

.field public static final voip_call_status_bar:I = 0x7f020e17

.field public static final voip_call_status_bar_background:I = 0x7f020e18

.field public static final voip_call_status_bar_pressed:I = 0x7f020e19

.field public static final voip_green_button:I = 0x7f020e1a

.field public static final voip_green_button_enabled:I = 0x7f020e1b

.field public static final voip_green_button_pressed:I = 0x7f020e1c

.field public static final voip_grey_button:I = 0x7f020e1d

.field public static final voip_grey_button_enabled:I = 0x7f020e1e

.field public static final voip_grey_button_pressed:I = 0x7f020e1f

.field public static final voip_in_call_background_icon:I = 0x7f020e20

.field public static final voip_in_call_end_icon:I = 0x7f020e21

.field public static final voip_in_call_mute_icon:I = 0x7f020e22

.field public static final voip_in_call_speaker_icon:I = 0x7f020e23

.field public static final voip_in_call_video_icon:I = 0x7f020e24

.field public static final voip_nux_banner_sticker:I = 0x7f020e25

.field public static final voip_red_button:I = 0x7f020e26

.field public static final voip_red_button_enabled:I = 0x7f020e27

.field public static final voip_red_button_pressed:I = 0x7f020e28

.field public static final voip_star:I = 0x7f020e29

.field public static final voip_star_pressed:I = 0x7f020e2a

.field public static final voip_threadview_button_with_nux_nub:I = 0x7f020e2b

.field public static final voip_titlebar_button_disabled:I = 0x7f020e2c

.field public static final voip_titlebar_button_icon:I = 0x7f020e2d

.field public static final voip_titlebar_button_icon_missed:I = 0x7f020e2e

.field public static final voip_titlebar_button_white_icon:I = 0x7f020e2f

.field public static final voip_titlebar_nux_nub:I = 0x7f020e30

.field public static final voip_usertile_background:I = 0x7f020e31

.field public static final wallpaper_figure:I = 0x7f020e32

.field public static final white_button:I = 0x7f020e33

.field public static final white_button_normal:I = 0x7f020e34

.field public static final white_button_pressed:I = 0x7f020e35

.field public static final white_gradient:I = 0x7f020e36

.field public static final white_line_rounded_rectangle:I = 0x7f020e37

.field public static final white_spinner:I = 0x7f020e38

.field public static final white_spinner_indeterminate:I = 0x7f020e39

.field public static final white_x:I = 0x7f020e3a

.field public static final widget_button_background:I = 0x7f020e3b

.field public static final widget_icon_background:I = 0x7f020e3c

.field public static final widget_next_button:I = 0x7f020e3d

.field public static final widget_prev_button:I = 0x7f020e3e

.field public static final widgetbknd_noborder:I = 0x7f020e3f

.field public static final with_tag_chevron:I = 0x7f020e40

.field public static final wp_muzei_icon:I = 0x7f020e41

.field public static final x:I = 0x7f020e42

.field public static final x_button_normal:I = 0x7f020e43

.field public static final x_button_pressed:I = 0x7f020e44

.field public static final zero_button_blue:I = 0x7f020e45

.field public static final zero_button_blue_normal:I = 0x7f020e46

.field public static final zero_button_blue_pressed:I = 0x7f020e47

.field public static final zero_button_dark:I = 0x7f020e48

.field public static final zero_button_dark_normal:I = 0x7f020e49

.field public static final zero_button_dark_pressed:I = 0x7f020e4a

.field public static final zero_checkbox:I = 0x7f020e4b

.field public static final zero_checkbox_off:I = 0x7f020e4c

.field public static final zero_checkbox_on:I = 0x7f020e4d

.field public static final zero_modal_background:I = 0x7f020e4e

.field public static final zero_optin_celltower:I = 0x7f020e4f


# direct methods
.method public constructor <init>()V
    .locals 0

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method
