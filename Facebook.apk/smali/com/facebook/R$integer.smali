.class public Lcom/facebook/R$integer;
.super Ljava/lang/Object;
.source "R.java"


# static fields
.field public static final abc_max_action_buttons:I = 0x7f0f0002

.field public static final activity_transition_duration:I = 0x7f0f0004

.field public static final appirater_dialog_animation_time:I = 0x7f0f002d

.field public static final beeper_text_line_limits:I = 0x7f0f0011

.field public static final config_activityHorizontalTransitionDuration:I = 0x7f0f0008

.field public static final config_activityTransitionDuration:I = 0x7f0f0007

.field public static final default_circle_indicator_orientation:I = 0x7f0f0014

.field public static final dialog_enter_anim_duration:I = 0x7f0f000e

.field public static final dialog_exit_anim_duration:I = 0x7f0f000f

.field public static final error_anim_time:I = 0x7f0f002f

.field public static final event_rsvp_going:I = 0x7f0f003a

.field public static final event_rsvp_maybe:I = 0x7f0f003b

.field public static final events_card_action_button_going_state:I = 0x7f0f003d

.field public static final events_card_action_button_maybe_state:I = 0x7f0f003e

.field public static final events_card_action_button_not_going_state:I = 0x7f0f003f

.field public static final events_card_action_button_null_state:I = 0x7f0f003c

.field public static final events_dashboard_empty_hosting:I = 0x7f0f0030

.field public static final events_dashboard_empty_invited:I = 0x7f0f0031

.field public static final events_dashboard_empty_past:I = 0x7f0f0034

.field public static final events_dashboard_empty_saved:I = 0x7f0f0032

.field public static final events_dashboard_empty_upcoming:I = 0x7f0f0033

.field public static final events_dashboard_row_inline_rsvp_going:I = 0x7f0f0035

.field public static final events_dashboard_row_inline_rsvp_hosting:I = 0x7f0f0038

.field public static final events_dashboard_row_inline_rsvp_maybe:I = 0x7f0f0036

.field public static final events_dashboard_row_inline_rsvp_not_going:I = 0x7f0f0037

.field public static final events_dashboard_row_inline_rsvp_saved:I = 0x7f0f0039

.field public static final fbui_popover_window_anim_enter_duration:I = 0x7f0f0012

.field public static final fbui_popover_window_anim_exit_duration:I = 0x7f0f0013

.field public static final feed_quick_cam_max_recording_time_ms:I = 0x7f0f0040

.field public static final feed_quick_cam_min_recording_time_ms:I = 0x7f0f0041

.field public static final feed_store_activity_transition_duration:I = 0x7f0f001b

.field public static final google_play_services_version:I = 0x7f0f000d

.field public static final groups_feed_max_members_to_display:I = 0x7f0f0042

.field public static final location_ping_dialog_message_max_length:I = 0x7f0f001c

.field public static final maximum_status_length:I = 0x7f0f000c

.field public static final menu_free_call_order:I = 0x7f0f0009

.field public static final neue_login_logo_threshold:I = 0x7f0f0000

.field public static final neue_login_text_size_threshold:I = 0x7f0f0001

.field public static final notification_row_anim_duration:I = 0x7f0f0003

.field public static final orca_audio_composer_text_size:I = 0x7f0f000b

.field public static final orca_two_line_composer_edit_text_max_lines:I = 0x7f0f000a

.field public static final page_animation_time:I = 0x7f0f002e

.field public static final page_identity_events_max_num:I = 0x7f0f0026

.field public static final page_identity_friends_info_fragment_max_friends:I = 0x7f0f0022

.field public static final page_identity_max_child_locations:I = 0x7f0f0028

.field public static final page_identity_max_context_item_info_cards:I = 0x7f0f002a

.field public static final page_identity_max_context_item_rows:I = 0x7f0f0029

.field public static final page_identity_num_initial_child_locations:I = 0x7f0f0027

.field public static final page_identity_num_recent_posters:I = 0x7f0f0021

.field public static final page_identity_num_top_reviews:I = 0x7f0f001d

.field public static final page_identity_num_top_reviews_with_composer:I = 0x7f0f001e

.field public static final page_identity_photo_grid_fetch_size:I = 0x7f0f0025

.field public static final page_identity_photos_max_num:I = 0x7f0f0023

.field public static final page_identity_structured_content_fetch_genres_max_num:I = 0x7f0f001f

.field public static final page_identity_structured_content_fetch_people_max_num:I = 0x7f0f0020

.field public static final page_identity_videos_max_num:I = 0x7f0f0024

.field public static final preferences_enter_anim_duration:I = 0x7f0f0005

.field public static final preferences_exit_anim_duration:I = 0x7f0f0006

.field public static final qp_interstitial_button_max_lines:I = 0x7f0f001a

.field public static final qp_interstitial_content_with_image_max_lines:I = 0x7f0f0018

.field public static final qp_interstitial_social_context_max_lines:I = 0x7f0f0019

.field public static final qp_interstitial_title_max_lines:I = 0x7f0f0015

.field public static final qp_interstitial_title_no_image_max_lines:I = 0x7f0f0016

.field public static final qp_interstitial_title_no_image_or_social_context_max_lines:I = 0x7f0f0017

.field public static final reaction_medium_anim_time:I = 0x7f0f002c

.field public static final reaction_short_anim_time:I = 0x7f0f002b

.field public static final review_composer_advice_character_threshold:I = 0x7f0f0010


# direct methods
.method public constructor <init>()V
    .locals 0

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method
