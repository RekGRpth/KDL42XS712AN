.class public Lcom/facebook/systrace/Systrace;
.super Ljava/lang/Object;
.source "Systrace.java"


# direct methods
.method static constructor <clinit>()V
    .locals 0

    invoke-static {}, Lcom/facebook/systrace/Systrace;->c()V

    return-void
.end method

.method public constructor <init>()V
    .locals 0

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method public static a()V
    .locals 1
    .annotation build Landroid/annotation/TargetApi;
        value = 0x12
    .end annotation

    invoke-static {}, Lcom/facebook/systrace/Systrace;->b()Z

    move-result v0

    if-nez v0, :cond_0

    :goto_0
    return-void

    :cond_0
    invoke-static {}, Landroid/os/Trace;->endSection()V

    goto :goto_0
.end method

.method public static a(Ljava/lang/String;)V
    .locals 1
    .annotation build Landroid/annotation/TargetApi;
        value = 0x12
    .end annotation

    invoke-static {}, Lcom/facebook/systrace/Systrace;->b()Z

    move-result v0

    if-nez v0, :cond_0

    :goto_0
    return-void

    :cond_0
    invoke-static {p0}, Lcom/facebook/systrace/FbSystrace;->a(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Landroid/os/Trace;->beginSection(Ljava/lang/String;)V

    goto :goto_0
.end method

.method public static a(Ljava/lang/String;I)V
    .locals 3

    invoke-static {}, Lcom/facebook/systrace/Systrace;->b()Z

    move-result v0

    if-nez v0, :cond_0

    :goto_0
    return-void

    :cond_0
    invoke-static {p0}, Lcom/facebook/systrace/FbSystrace;->b(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    sget-wide v1, Lcom/facebook/systrace/TraceInternal;->b:J

    invoke-static {v1, v2, v0, p1}, Lcom/facebook/systrace/TraceInternal;->b(JLjava/lang/String;I)V

    goto :goto_0
.end method

.method public static a(Ljava/lang/String;IJ)V
    .locals 3

    invoke-static {}, Lcom/facebook/systrace/Systrace;->b()Z

    move-result v0

    if-nez v0, :cond_0

    :goto_0
    return-void

    :cond_0
    invoke-static {p0, p2, p3}, Lcom/facebook/systrace/FbSystrace;->a(Ljava/lang/String;J)Ljava/lang/String;

    move-result-object v0

    sget-wide v1, Lcom/facebook/systrace/TraceInternal;->b:J

    invoke-static {v1, v2, v0, p1}, Lcom/facebook/systrace/TraceInternal;->b(JLjava/lang/String;I)V

    goto :goto_0
.end method

.method public static a(Ljava/lang/String;ILjava/lang/String;)V
    .locals 2

    invoke-static {}, Lcom/facebook/systrace/Systrace;->b()Z

    move-result v0

    if-nez v0, :cond_0

    :goto_0
    return-void

    :cond_0
    invoke-static {p0}, Lcom/facebook/systrace/FbSystrace;->a(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    invoke-static {p2}, Lcom/facebook/systrace/FbSystrace;->a(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, p1, v1}, Lcom/facebook/systrace/TraceDirect;->a(Ljava/lang/String;ILjava/lang/String;)V

    goto :goto_0
.end method

.method public static a(Ljava/lang/String;Ljava/lang/String;I)V
    .locals 4

    invoke-static {}, Lcom/facebook/systrace/Systrace;->b()Z

    move-result v0

    if-nez v0, :cond_0

    :goto_0
    return-void

    :cond_0
    invoke-static {p0}, Lcom/facebook/systrace/FbSystrace;->d(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    invoke-static {p1}, Lcom/facebook/systrace/FbSystrace;->d(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    sget-wide v2, Lcom/facebook/systrace/TraceInternal;->b:J

    invoke-static {v2, v3, v0, p2}, Lcom/facebook/systrace/TraceInternal;->b(JLjava/lang/String;I)V

    sget-wide v2, Lcom/facebook/systrace/TraceInternal;->b:J

    invoke-static {v2, v3, v1, p2}, Lcom/facebook/systrace/TraceInternal;->c(JLjava/lang/String;I)V

    goto :goto_0
.end method

.method public static b(Ljava/lang/String;)V
    .locals 4

    invoke-static {}, Lcom/facebook/systrace/Systrace;->b()Z

    move-result v0

    if-nez v0, :cond_0

    :goto_0
    return-void

    :cond_0
    sget-wide v0, Lcom/facebook/systrace/TraceInternal;->b:J

    invoke-static {p0}, Lcom/facebook/systrace/Systrace;->c(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v2

    const/16 v3, 0x3e8

    invoke-static {v0, v1, v2, v3}, Lcom/facebook/systrace/TraceInternal;->a(JLjava/lang/String;I)V

    goto :goto_0
.end method

.method public static b(Ljava/lang/String;I)V
    .locals 3

    invoke-static {}, Lcom/facebook/systrace/Systrace;->b()Z

    move-result v0

    if-nez v0, :cond_0

    :goto_0
    return-void

    :cond_0
    invoke-static {p0}, Lcom/facebook/systrace/FbSystrace;->a(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    sget-wide v1, Lcom/facebook/systrace/TraceInternal;->b:J

    invoke-static {v1, v2, v0, p1}, Lcom/facebook/systrace/TraceInternal;->c(JLjava/lang/String;I)V

    goto :goto_0
.end method

.method public static b(Ljava/lang/String;IJ)V
    .locals 3

    invoke-static {}, Lcom/facebook/systrace/Systrace;->b()Z

    move-result v0

    if-nez v0, :cond_0

    :goto_0
    return-void

    :cond_0
    invoke-static {p0, p2, p3}, Lcom/facebook/systrace/FbSystrace;->a(Ljava/lang/String;J)Ljava/lang/String;

    move-result-object v0

    sget-wide v1, Lcom/facebook/systrace/TraceInternal;->b:J

    invoke-static {v1, v2, v0, p1}, Lcom/facebook/systrace/TraceInternal;->c(JLjava/lang/String;I)V

    goto :goto_0
.end method

.method public static b()Z
    .locals 2

    sget-wide v0, Lcom/facebook/systrace/TraceInternal;->b:J

    invoke-static {v0, v1}, Lcom/facebook/systrace/TraceInternal;->a(J)Z

    move-result v0

    return v0
.end method

.method private static c(Ljava/lang/String;)Ljava/lang/String;
    .locals 2
    .annotation build Landroid/annotation/SuppressLint;
        value = {
            "BadMethodUse-java.lang.String.length"
        }
    .end annotation

    const/16 v1, 0x14

    invoke-virtual {p0}, Ljava/lang/String;->length()I

    move-result v0

    if-gt v0, v1, :cond_0

    :goto_0
    return-object p0

    :cond_0
    const/4 v0, 0x0

    invoke-virtual {p0, v0, v1}, Ljava/lang/String;->substring(II)Ljava/lang/String;

    move-result-object p0

    goto :goto_0
.end method

.method private static c()V
    .locals 1

    const/4 v0, 0x1

    invoke-static {v0}, Lcom/facebook/systrace/TraceInternal;->a(Z)V

    return-void
.end method

.method public static c(Ljava/lang/String;I)V
    .locals 3

    invoke-static {}, Lcom/facebook/systrace/Systrace;->b()Z

    move-result v0

    if-nez v0, :cond_0

    :goto_0
    return-void

    :cond_0
    invoke-static {p0}, Lcom/facebook/systrace/FbSystrace;->c(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    sget-wide v1, Lcom/facebook/systrace/TraceInternal;->b:J

    invoke-static {v1, v2, v0, p1}, Lcom/facebook/systrace/TraceInternal;->c(JLjava/lang/String;I)V

    goto :goto_0
.end method
