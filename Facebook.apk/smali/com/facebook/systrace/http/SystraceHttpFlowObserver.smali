.class public Lcom/facebook/systrace/http/SystraceHttpFlowObserver;
.super Lcom/facebook/http/observer/AbstractFbHttpFlowObserver;
.source "SystraceHttpFlowObserver.java"


# direct methods
.method public constructor <init>()V
    .locals 0
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    invoke-direct {p0}, Lcom/facebook/http/observer/AbstractFbHttpFlowObserver;-><init>()V

    return-void
.end method


# virtual methods
.method public final a(Lcom/facebook/http/observer/Stage;Lorg/apache/http/HttpRequest;Lorg/apache/http/HttpResponse;Lorg/apache/http/protocol/HttpContext;Ljava/io/IOException;)V
    .locals 5
    .param p3    # Lorg/apache/http/HttpResponse;
        .annotation runtime Ljavax/annotation/Nullable;
        .end annotation
    .end param

    invoke-super/range {p0 .. p5}, Lcom/facebook/http/observer/AbstractFbHttpFlowObserver;->a(Lcom/facebook/http/observer/Stage;Lorg/apache/http/HttpRequest;Lorg/apache/http/HttpResponse;Lorg/apache/http/protocol/HttpContext;Ljava/io/IOException;)V

    invoke-interface {p2}, Lorg/apache/http/HttpRequest;->getRequestLine()Lorg/apache/http/RequestLine;

    move-result-object v0

    invoke-interface {v0}, Lorg/apache/http/RequestLine;->getUri()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {p2}, Ljava/lang/Object;->hashCode()I

    move-result v2

    if-eqz p3, :cond_0

    invoke-interface {p3}, Lorg/apache/http/HttpResponse;->getStatusLine()Lorg/apache/http/StatusLine;

    move-result-object v0

    invoke-interface {v0}, Lorg/apache/http/StatusLine;->getStatusCode()I

    move-result v0

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v3, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    const-string v4, "(FAILED: "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3, v0}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v3, ")"

    invoke-virtual {v0, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    :goto_0
    invoke-static {v1, v2}, Lcom/facebook/systrace/Systrace;->b(Ljava/lang/String;I)V

    invoke-static {v1, v0, v2}, Lcom/facebook/systrace/Systrace;->a(Ljava/lang/String;Ljava/lang/String;I)V

    return-void

    :cond_0
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v3, "(FAILED)"

    invoke-virtual {v0, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    goto :goto_0
.end method

.method public final a(Lorg/apache/http/HttpRequest;Lorg/apache/http/protocol/HttpContext;Lcom/facebook/http/observer/HttpFlowStatistics;)V
    .locals 3

    invoke-super {p0, p1, p2, p3}, Lcom/facebook/http/observer/AbstractFbHttpFlowObserver;->a(Lorg/apache/http/HttpRequest;Lorg/apache/http/protocol/HttpContext;Lcom/facebook/http/observer/HttpFlowStatistics;)V

    invoke-interface {p1}, Lorg/apache/http/HttpRequest;->getRequestLine()Lorg/apache/http/RequestLine;

    move-result-object v0

    invoke-interface {v0}, Lorg/apache/http/RequestLine;->getUri()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p1}, Ljava/lang/Object;->hashCode()I

    move-result v1

    invoke-static {v0, v1}, Lcom/facebook/systrace/Systrace;->a(Ljava/lang/String;I)V

    const-string v2, "Latency"

    invoke-static {v0, v1, v2}, Lcom/facebook/systrace/Systrace;->a(Ljava/lang/String;ILjava/lang/String;)V

    return-void
.end method

.method public final a(Lorg/apache/http/HttpResponse;Lorg/apache/http/protocol/HttpContext;)V
    .locals 2

    invoke-super {p0, p1, p2}, Lcom/facebook/http/observer/AbstractFbHttpFlowObserver;->a(Lorg/apache/http/HttpResponse;Lorg/apache/http/protocol/HttpContext;)V

    invoke-virtual {p0}, Lcom/facebook/systrace/http/SystraceHttpFlowObserver;->b()Lorg/apache/http/HttpRequest;

    move-result-object v0

    invoke-interface {v0}, Lorg/apache/http/HttpRequest;->getRequestLine()Lorg/apache/http/RequestLine;

    move-result-object v0

    invoke-interface {v0}, Lorg/apache/http/RequestLine;->getUri()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p0}, Lcom/facebook/systrace/http/SystraceHttpFlowObserver;->b()Lorg/apache/http/HttpRequest;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/Object;->hashCode()I

    move-result v1

    invoke-static {v0, v1}, Lcom/facebook/systrace/Systrace;->b(Ljava/lang/String;I)V

    return-void
.end method

.method public final b(Lorg/apache/http/HttpResponse;Lorg/apache/http/protocol/HttpContext;)V
    .locals 3

    invoke-super {p0, p1, p2}, Lcom/facebook/http/observer/AbstractFbHttpFlowObserver;->b(Lorg/apache/http/HttpResponse;Lorg/apache/http/protocol/HttpContext;)V

    invoke-virtual {p0}, Lcom/facebook/systrace/http/SystraceHttpFlowObserver;->b()Lorg/apache/http/HttpRequest;

    move-result-object v0

    invoke-interface {v0}, Lorg/apache/http/HttpRequest;->getRequestLine()Lorg/apache/http/RequestLine;

    move-result-object v0

    invoke-interface {v0}, Lorg/apache/http/RequestLine;->getUri()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p0}, Lcom/facebook/systrace/http/SystraceHttpFlowObserver;->b()Lorg/apache/http/HttpRequest;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/Object;->hashCode()I

    move-result v1

    const-string v2, "Download"

    invoke-static {v0, v1, v2}, Lcom/facebook/systrace/Systrace;->a(Ljava/lang/String;ILjava/lang/String;)V

    return-void
.end method
