.class public Lcom/facebook/R$plurals;
.super Ljava/lang/Object;
.source "R.java"


# static fields
.field public static final accessibility_jewel_friends:I = 0x7f0e0057

.field public static final accessibility_jewel_inbox:I = 0x7f0e0058

.field public static final accessibility_jewel_notifications:I = 0x7f0e0059

.field public static final album_contributor_digest:I = 0x7f0e0028

.field public static final album_num_photos:I = 0x7f0e0026

.field public static final album_num_videos:I = 0x7f0e0027

.field public static final audio_player_duration_seconds:I = 0x7f0e000c

.field public static final backgroundlocation_inline_upsell_facepile:I = 0x7f0e005b

.field public static final backgroundlocation_nux_privacy_facepile:I = 0x7f0e005a

.field public static final birthday_count:I = 0x7f0e0053

.field public static final chat_head_conversations:I = 0x7f0e000d

.field public static final chat_head_num_online_friends:I = 0x7f0e000e

.field public static final dashclock_expanded_title:I = 0x7f0e002f

.field public static final elapsed_time_hr:I = 0x7f0e0003

.field public static final elapsed_time_min:I = 0x7f0e0002

.field public static final event_artist_num_likes:I = 0x7f0e005f

.field public static final event_ticket_providers_other:I = 0x7f0e005e

.field public static final events_dashboard_birthday_friend_age:I = 0x7f0e0067

.field public static final events_dashboard_social_context_friends_going_plural:I = 0x7f0e0065

.field public static final events_dashboard_social_context_friends_maybe_plural:I = 0x7f0e0066

.field public static final events_guestlist_mutual_friends:I = 0x7f0e0064

.field public static final events_permalink_guest_summary_going_long_with_others:I = 0x7f0e0060

.field public static final events_permalink_guest_summary_going_short:I = 0x7f0e0061

.field public static final events_permalink_guest_summary_maybe_long_with_others:I = 0x7f0e0062

.field public static final events_permalink_guest_summary_maybe_short:I = 0x7f0e0063

.field public static final events_permalink_starts_in_hr:I = 0x7f0e005d

.field public static final events_permalink_starts_in_min:I = 0x7f0e005c

.field public static final fake_plural:I = 0x7f0e0000

.field public static final feed_browser_pivots_show_button_text:I = 0x7f0e0069

.field public static final feed_explanation_article_chaining:I = 0x7f0e0014

.field public static final feed_explanation_inline_video_pivots:I = 0x7f0e0016

.field public static final feed_explanation_photo_chaining:I = 0x7f0e0015

.field public static final feed_explanation_pyml:I = 0x7f0e0013

.field public static final feed_plural_likes:I = 0x7f0e0017

.field public static final feed_storyset_fallback_social_context:I = 0x7f0e004a

.field public static final feed_video_view_count:I = 0x7f0e0019

.field public static final flash_posts_expire_days:I = 0x7f0e001e

.field public static final flash_posts_expire_hours:I = 0x7f0e001d

.field public static final flash_posts_expire_minutes:I = 0x7f0e001c

.field public static final friend_finder_all_done:I = 0x7f0e004d

.field public static final friends_count:I = 0x7f0e0006

.field public static final friends_nearby_invite_send_success_title:I = 0x7f0e0038

.field public static final friends_visited:I = 0x7f0e001b

.field public static final group_members:I = 0x7f0e004b

.field public static final inboxstyle_notification_title:I = 0x7f0e0030

.field public static final itunes_recommended_search_results:I = 0x7f0e004c

.field public static final keyword_search_friends_with_number:I = 0x7f0e0051

.field public static final keyword_search_likes_with_number:I = 0x7f0e0050

.field public static final keyword_search_members_with_number:I = 0x7f0e0052

.field public static final lockscreen_dialog_title:I = 0x7f0e0031

.field public static final message_delete_confirm_msg:I = 0x7f0e0009

.field public static final message_delete_confirm_ok_button:I = 0x7f0e000a

.field public static final message_delete_confirm_title:I = 0x7f0e0008

.field public static final message_delete_progress:I = 0x7f0e000b

.field public static final mutual_friends:I = 0x7f0e0007

.field public static final nearby_likes:I = 0x7f0e004e

.field public static final nearby_visits:I = 0x7f0e004f

.field public static final notification_aggregated_count:I = 0x7f0e0037

.field public static final num_billion_long:I = 0x7f0e0021

.field public static final num_million_long:I = 0x7f0e0020

.field public static final num_thousand_long:I = 0x7f0e001f

.field public static final orca_more_messages:I = 0x7f0e0010

.field public static final orca_new_message:I = 0x7f0e000f

.field public static final page_identity_events_friends_going:I = 0x7f0e0046

.field public static final page_identity_events_people_going:I = 0x7f0e0045

.field public static final page_identity_friends_like_phrase:I = 0x7f0e003e

.field public static final page_identity_friends_visited_phrase:I = 0x7f0e003f

.field public static final page_identity_likes:I = 0x7f0e0040

.field public static final page_identity_likes_with_number:I = 0x7f0e0049

.field public static final page_identity_posts_by_others_num:I = 0x7f0e0047

.field public static final page_identity_posts_by_others_viewer_friend:I = 0x7f0e0048

.field public static final page_identity_rating_bar_chart_label:I = 0x7f0e0029

.field public static final page_identity_total_raters:I = 0x7f0e0041

.field public static final page_identity_view_reviews_header:I = 0x7f0e0043

.field public static final page_identity_viewer_visits_amount:I = 0x7f0e0044

.field public static final page_identity_visits:I = 0x7f0e0042

.field public static final page_ui_insights_weekly_new_likes:I = 0x7f0e003c

.field public static final page_ui_uni_new_likes:I = 0x7f0e003d

.field public static final pages_browser_likes_with_number:I = 0x7f0e0068

.field public static final photo_num_comments:I = 0x7f0e0023

.field public static final photo_num_likes:I = 0x7f0e0022

.field public static final place_visits:I = 0x7f0e001a

.field public static final places_checkin_card_friends_were_here:I = 0x7f0e0033

.field public static final places_checkin_were_here:I = 0x7f0e0032

.field public static final places_event_going:I = 0x7f0e0034

.field public static final places_tag_place_to_photo:I = 0x7f0e0035

.field public static final profile_info_num_checkins:I = 0x7f0e0055

.field public static final profile_info_num_likes:I = 0x7f0e0054

.field public static final rating_bar_accessibility_text:I = 0x7f0e0001

.field public static final rating_decimal_number:I = 0x7f0e002a

.field public static final small_audience_privacy_education_text:I = 0x7f0e0036

.field public static final snowflake_num_photos:I = 0x7f0e0024

.field public static final status_photo_added_tagged_title:I = 0x7f0e002d

.field public static final status_photo_added_title:I = 0x7f0e002c

.field public static final status_photo_added_to_album_title:I = 0x7f0e002e

.field public static final status_tagged_others:I = 0x7f0e002b

.field public static final story_promotion_duration:I = 0x7f0e0025

.field public static final timeline_page_likes:I = 0x7f0e0039

.field public static final timeline_page_talking_about:I = 0x7f0e003b

.field public static final timeline_page_visits:I = 0x7f0e003a

.field public static final total_reach:I = 0x7f0e0018

.field public static final ufi_num_comments:I = 0x7f0e0005

.field public static final ufi_num_likes:I = 0x7f0e0004

.field public static final ufiservices_replys:I = 0x7f0e0012

.field public static final video_play_count_text:I = 0x7f0e0011

.field public static final x_people:I = 0x7f0e0056


# direct methods
.method public constructor <init>()V
    .locals 0

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method
