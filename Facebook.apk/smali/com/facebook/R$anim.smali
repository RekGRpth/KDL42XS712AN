.class public Lcom/facebook/R$anim;
.super Ljava/lang/Object;
.source "R.java"


# static fields
.field public static final abc_fade_in:I = 0x7f040000

.field public static final abc_fade_out:I = 0x7f040001

.field public static final abc_slide_in_bottom:I = 0x7f040002

.field public static final abc_slide_in_top:I = 0x7f040003

.field public static final abc_slide_out_bottom:I = 0x7f040004

.field public static final abc_slide_out_top:I = 0x7f040005

.field public static final activity_close_enter:I = 0x7f040006

.field public static final activity_close_exit:I = 0x7f040007

.field public static final browser_in:I = 0x7f040008

.field public static final camera_auto_focus_finished:I = 0x7f040009

.field public static final composer_overlay_in:I = 0x7f04000a

.field public static final continuous_rotation:I = 0x7f04000b

.field public static final decelerate_interpolator:I = 0x7f04000c

.field public static final default_fade_in:I = 0x7f04000d

.field public static final default_fade_out:I = 0x7f04000e

.field public static final dialog_enter:I = 0x7f04000f

.field public static final dialog_exit:I = 0x7f040010

.field public static final down_from_top:I = 0x7f040011

.field public static final down_from_top_rev:I = 0x7f040012

.field public static final drop_out_fade:I = 0x7f040013

.field public static final fade_in:I = 0x7f040014

.field public static final fade_in_fast:I = 0x7f040015

.field public static final fade_in_medium:I = 0x7f040016

.field public static final fade_in_super_fast:I = 0x7f040017

.field public static final fade_in_thumbnail:I = 0x7f040018

.field public static final fade_out:I = 0x7f040019

.field public static final fade_out_fast:I = 0x7f04001a

.field public static final fade_out_medium:I = 0x7f04001b

.field public static final fade_out_super_fast:I = 0x7f04001c

.field public static final fast_decelerate_interpolator:I = 0x7f04001d

.field public static final fbui_popover_window_above_enter:I = 0x7f04001e

.field public static final fbui_popover_window_above_exit:I = 0x7f04001f

.field public static final fbui_popover_window_below_enter:I = 0x7f040020

.field public static final fbui_popover_window_below_exit:I = 0x7f040021

.field public static final fbui_popover_window_enter:I = 0x7f040022

.field public static final fbui_popover_window_exit:I = 0x7f040023

.field public static final fbui_popover_window_overshoot_interpolator:I = 0x7f040024

.field public static final feedstore_out:I = 0x7f040025

.field public static final hold:I = 0x7f040026

.field public static final in_from_left:I = 0x7f040027

.field public static final in_from_right:I = 0x7f040028

.field public static final in_from_right_animation:I = 0x7f040029

.field public static final keep_still_bookmark:I = 0x7f04002a

.field public static final map_buttons_fade_in:I = 0x7f04002b

.field public static final map_buttons_fade_out:I = 0x7f04002c

.field public static final menu_dim_entry:I = 0x7f04002d

.field public static final menu_dim_exit:I = 0x7f04002e

.field public static final nearby_nux_appear_from_bottom:I = 0x7f04002f

.field public static final nearby_nux_appear_from_top:I = 0x7f040030

.field public static final nearby_nux_fade_out:I = 0x7f040031

.field public static final nearby_overshoot_interpolator:I = 0x7f040032

.field public static final none:I = 0x7f040033

.field public static final orca_enter_from_bottom:I = 0x7f040034

.field public static final orca_enter_from_right:I = 0x7f040035

.field public static final orca_fading_enter:I = 0x7f040036

.field public static final orca_fading_exit:I = 0x7f040037

.field public static final orca_fragment_fade_in:I = 0x7f040038

.field public static final orca_fragment_fade_out:I = 0x7f040039

.field public static final orca_leave_to_bottom:I = 0x7f04003a

.field public static final orca_leave_to_right:I = 0x7f04003b

.field public static final orca_main_fragment_enter:I = 0x7f04003c

.field public static final orca_main_fragment_exit:I = 0x7f04003d

.field public static final orca_new_message_pill_hide:I = 0x7f04003e

.field public static final orca_new_message_pill_show:I = 0x7f04003f

.field public static final orca_unread_message_pill_hide:I = 0x7f040040

.field public static final orca_unread_message_pill_show:I = 0x7f040041

.field public static final out_to_left:I = 0x7f040042

.field public static final out_to_right:I = 0x7f040043

.field public static final out_to_right_animation:I = 0x7f040044

.field public static final page_identity_rating_bar_star_selected:I = 0x7f040045

.field public static final page_identity_rating_bar_star_unselected:I = 0x7f040046

.field public static final page_identity_rating_bar_star_wave:I = 0x7f040047

.field public static final page_identity_rating_bar_star_wave_interpolator:I = 0x7f040048

.field public static final payment_pin_confirm_cycle:I = 0x7f040049

.field public static final payment_pin_confirm_shake:I = 0x7f04004a

.field public static final places_down_from_top_rev:I = 0x7f04004b

.field public static final places_footer_slide_in:I = 0x7f04004c

.field public static final places_footer_slide_out:I = 0x7f04004d

.field public static final places_footer_slide_out_for_replace:I = 0x7f04004e

.field public static final preferences_enter:I = 0x7f04004f

.field public static final preferences_exit:I = 0x7f040050

.field public static final pull_in:I = 0x7f040051

.field public static final pull_in_animation:I = 0x7f040052

.field public static final pull_out_animation:I = 0x7f040053

.field public static final push_back:I = 0x7f040054

.field public static final reaction_card_fade_in:I = 0x7f040055

.field public static final reaction_card_fade_out:I = 0x7f040056

.field public static final reaction_fade_out:I = 0x7f040057

.field public static final reaction_header_fade_in:I = 0x7f040058

.field public static final reaction_overlay_fade_in:I = 0x7f040059

.field public static final reaction_overshoot_interpolator:I = 0x7f04005a

.field public static final rise_in_fade:I = 0x7f04005b

.field public static final scale_down:I = 0x7f04005c

.field public static final scale_up:I = 0x7f04005d

.field public static final slide_in:I = 0x7f04005e

.field public static final slide_in_bookmark:I = 0x7f04005f

.field public static final slide_in_bottom:I = 0x7f040060

.field public static final slide_left_in:I = 0x7f040061

.field public static final slide_left_in_bookmark_fragment:I = 0x7f040062

.field public static final slide_left_out:I = 0x7f040063

.field public static final slide_left_out_bookmark_fragment:I = 0x7f040064

.field public static final slide_out_bottom:I = 0x7f040065

.field public static final slide_right_in:I = 0x7f040066

.field public static final slide_right_in_bookmark_fragment:I = 0x7f040067

.field public static final slide_right_out:I = 0x7f040068

.field public static final slide_right_out_bookmark_fragment:I = 0x7f040069

.field public static final slide_up:I = 0x7f04006a

.field public static final slow_fade_in:I = 0x7f04006b

.field public static final slow_fade_out:I = 0x7f04006c

.field public static final subtle_fade_in:I = 0x7f04006d

.field public static final subtle_fade_out:I = 0x7f04006e

.field public static final thread_location_nux_show:I = 0x7f04006f

.field public static final tip_fade_in:I = 0x7f040070

.field public static final tip_fade_in_out:I = 0x7f040071

.field public static final tip_fade_out:I = 0x7f040072

.field public static final vault_spinner_rotate:I = 0x7f040073


# direct methods
.method public constructor <init>()V
    .locals 0

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method
