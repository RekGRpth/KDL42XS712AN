.class public Lcom/facebook/dash/nobreak/DashResetHandler;
.super Ljava/lang/Object;
.source "DashResetHandler.java"

# interfaces
.implements Lcom/facebook/nobreak/ResetHandler;


# instance fields
.field private final a:Landroid/content/Context;

.field private final b:Ljava/lang/String;

.field private final c:Lcom/facebook/content/FacebookOnlyIntentActionFactory;


# direct methods
.method public constructor <init>(Landroid/content/Context;Lcom/facebook/common/build/SignatureType;Lcom/facebook/content/FacebookOnlyIntentActionFactory;)V
    .locals 1
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    invoke-static {p1}, Lcom/google/common/base/Preconditions;->checkNotNull(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/content/Context;

    iput-object v0, p0, Lcom/facebook/dash/nobreak/DashResetHandler;->a:Landroid/content/Context;

    invoke-virtual {p2}, Lcom/facebook/common/build/SignatureType;->getPermission()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Lcom/google/common/base/Preconditions;->checkNotNull(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/String;

    iput-object v0, p0, Lcom/facebook/dash/nobreak/DashResetHandler;->b:Ljava/lang/String;

    invoke-static {p3}, Lcom/google/common/base/Preconditions;->checkNotNull(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/content/FacebookOnlyIntentActionFactory;

    iput-object v0, p0, Lcom/facebook/dash/nobreak/DashResetHandler;->c:Lcom/facebook/content/FacebookOnlyIntentActionFactory;

    return-void
.end method

.method public static a(Lcom/facebook/inject/InjectorLike;)Lcom/facebook/dash/nobreak/DashResetHandler;
    .locals 1

    invoke-static {p0}, Lcom/facebook/dash/nobreak/DashResetHandler;->b(Lcom/facebook/inject/InjectorLike;)Lcom/facebook/dash/nobreak/DashResetHandler;

    move-result-object v0

    return-object v0
.end method

.method private static b(Lcom/facebook/inject/InjectorLike;)Lcom/facebook/dash/nobreak/DashResetHandler;
    .locals 4

    new-instance v3, Lcom/facebook/dash/nobreak/DashResetHandler;

    const-class v0, Landroid/content/Context;

    invoke-interface {p0, v0}, Lcom/facebook/inject/InjectorLike;->getInstance(Ljava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/content/Context;

    const-class v1, Lcom/facebook/common/build/SignatureType;

    invoke-interface {p0, v1}, Lcom/facebook/inject/InjectorLike;->getInstance(Ljava/lang/Class;)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/facebook/common/build/SignatureType;

    const-class v2, Lcom/facebook/content/FacebookOnlyIntentActionFactory;

    invoke-interface {p0, v2}, Lcom/facebook/inject/InjectorLike;->getInstance(Ljava/lang/Class;)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lcom/facebook/content/FacebookOnlyIntentActionFactory;

    invoke-direct {v3, v0, v1, v2}, Lcom/facebook/dash/nobreak/DashResetHandler;-><init>(Landroid/content/Context;Lcom/facebook/common/build/SignatureType;Lcom/facebook/content/FacebookOnlyIntentActionFactory;)V

    return-object v3
.end method


# virtual methods
.method public final a()V
    .locals 3

    new-instance v0, Landroid/content/Intent;

    const-string v1, "com.facebook.intent.action.ACTION_RELEASE_HOME_INTENT"

    invoke-direct {v0, v1}, Landroid/content/Intent;-><init>(Ljava/lang/String;)V

    iget-object v1, p0, Lcom/facebook/dash/nobreak/DashResetHandler;->a:Landroid/content/Context;

    iget-object v2, p0, Lcom/facebook/dash/nobreak/DashResetHandler;->b:Ljava/lang/String;

    invoke-virtual {v1, v0, v2}, Landroid/content/Context;->sendBroadcast(Landroid/content/Intent;Ljava/lang/String;)V

    new-instance v0, Landroid/content/Intent;

    iget-object v1, p0, Lcom/facebook/dash/nobreak/DashResetHandler;->c:Lcom/facebook/content/FacebookOnlyIntentActionFactory;

    const-string v2, "ACTION_DISABLE_DASH"

    invoke-virtual {v1, v2}, Lcom/facebook/content/FacebookOnlyIntentActionFactory;->a(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Landroid/content/Intent;-><init>(Ljava/lang/String;)V

    iget-object v1, p0, Lcom/facebook/dash/nobreak/DashResetHandler;->a:Landroid/content/Context;

    iget-object v2, p0, Lcom/facebook/dash/nobreak/DashResetHandler;->b:Ljava/lang/String;

    invoke-virtual {v1, v0, v2}, Landroid/content/Context;->sendBroadcast(Landroid/content/Intent;Ljava/lang/String;)V

    return-void
.end method
