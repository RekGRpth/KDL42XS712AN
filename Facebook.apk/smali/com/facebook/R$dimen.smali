.class public Lcom/facebook/R$dimen;
.super Ljava/lang/Object;
.source "R.java"


# static fields
.field public static final abc_action_bar_default_height:I = 0x7f0c0048

.field public static final abc_action_bar_icon_vertical_padding:I = 0x7f0c0049

.field public static final abc_action_bar_stacked_max_height:I = 0x7f0c004f

.field public static final abc_action_bar_stacked_tab_max_width:I = 0x7f0c0047

.field public static final abc_action_bar_subtitle_bottom_margin:I = 0x7f0c004d

.field public static final abc_action_bar_subtitle_text_size:I = 0x7f0c004b

.field public static final abc_action_bar_subtitle_top_margin:I = 0x7f0c004c

.field public static final abc_action_bar_title_text_size:I = 0x7f0c004a

.field public static final abc_action_button_min_width:I = 0x7f0c004e

.field public static final abc_config_prefDialogWidth:I = 0x7f0c0046

.field public static final abc_dropdownitem_icon_width:I = 0x7f0c0055

.field public static final abc_dropdownitem_text_padding_left:I = 0x7f0c0053

.field public static final abc_dropdownitem_text_padding_right:I = 0x7f0c0054

.field public static final abc_panel_menu_list_width:I = 0x7f0c0050

.field public static final abc_search_view_preferred_width:I = 0x7f0c0052

.field public static final abc_search_view_text_min_width:I = 0x7f0c0051

.field public static final activity_transition_full_fade_alpha:I = 0x7f0c0096

.field public static final activity_transition_min_zoom:I = 0x7f0c0097

.field public static final activity_transition_partial_fade_alpha:I = 0x7f0c0095

.field public static final add_credit_card_input_text_size:I = 0x7f0c0650

.field public static final album_cover_photo_hd_height:I = 0x7f0c0357

.field public static final album_cover_photo_hd_text_width:I = 0x7f0c0358

.field public static final album_cover_photo_hd_width:I = 0x7f0c0356

.field public static final album_cover_photo_view_size:I = 0x7f0c0355

.field public static final album_edit_about_above_margin:I = 0x7f0c035c

.field public static final album_edit_about_text_size:I = 0x7f0c0369

.field public static final album_edit_add_contributor_bar_height:I = 0x7f0c0370

.field public static final album_edit_add_contributor_btn_left_margin:I = 0x7f0c0371

.field public static final album_edit_add_contributor_btn_right_margin:I = 0x7f0c0372

.field public static final album_edit_add_contributor_textview_left_margin:I = 0x7f0c036f

.field public static final album_edit_blue_titlebar_vertical_divider_bottom_margin:I = 0x7f0c0324

.field public static final album_edit_blue_titlebar_vertical_divider_right_margin:I = 0x7f0c0322

.field public static final album_edit_blue_titlebar_vertical_divider_top_margin:I = 0x7f0c0323

.field public static final album_edit_blue_titlebar_vertical_divider_width:I = 0x7f0c0321

.field public static final album_edit_description_bottom_margin:I = 0x7f0c0362

.field public static final album_edit_description_text_size:I = 0x7f0c036b

.field public static final album_edit_edge_margin:I = 0x7f0c035f

.field public static final album_edit_edittext_spacing:I = 0x7f0c0360

.field public static final album_edit_ineditable_content_text_size:I = 0x7f0c036d

.field public static final album_edit_ineditable_textview_bottom_margin:I = 0x7f0c0368

.field public static final album_edit_ineditable_textview_inside_margin:I = 0x7f0c0367

.field public static final album_edit_ineditable_textview_top_margin:I = 0x7f0c0366

.field public static final album_edit_ineditable_title_text_size:I = 0x7f0c036c

.field public static final album_edit_section_bottom_margin:I = 0x7f0c0365

.field public static final album_edit_section_title_divider_margin:I = 0x7f0c0364

.field public static final album_edit_section_title_text_size:I = 0x7f0c036e

.field public static final album_edit_section_title_top_margin:I = 0x7f0c0363

.field public static final album_edit_thick_divider_height:I = 0x7f0c035e

.field public static final album_edit_thin_divider_height:I = 0x7f0c035d

.field public static final album_edit_title_text_size:I = 0x7f0c036a

.field public static final album_edit_title_top_margin:I = 0x7f0c0361

.field public static final album_extra_size:I = 0x7f0c0758

.field public static final album_header_margin_bottom:I = 0x7f0c0353

.field public static final album_header_margin_top:I = 0x7f0c0352

.field public static final album_rename_dialog_text_edit_padding:I = 0x7f0c0354

.field public static final album_text_size:I = 0x7f0c0754

.field public static final album_title_size:I = 0x7f0c0755

.field public static final alertdialog_item_padding:I = 0x7f0c0008

.field public static final alertdialog_item_vertical_size:I = 0x7f0c0007

.field public static final alertdialog_text_horizontal_padding:I = 0x7f0c0009

.field public static final alertdialog_title_text_size:I = 0x7f0c000a

.field public static final all_selected_padding:I = 0x7f0c0375

.field public static final app_source_button_height:I = 0x7f0c00c0

.field public static final app_source_button_padding_right:I = 0x7f0c00be

.field public static final app_source_button_padding_top:I = 0x7f0c00bd

.field public static final app_source_button_width:I = 0x7f0c00bf

.field public static final apptab_nux_top_margin:I = 0x7f0c00e7

.field public static final article_chaining_item_first_left_margin:I = 0x7f0c01ff

.field public static final article_chaining_item_image_padding:I = 0x7f0c01fa

.field public static final article_chaining_item_image_size:I = 0x7f0c01f9

.field public static final article_chaining_item_spacing:I = 0x7f0c0200

.field public static final article_chaining_item_subtitle_text:I = 0x7f0c01fe

.field public static final article_chaining_item_text_right_padding:I = 0x7f0c01fb

.field public static final article_chaining_item_text_top_padding:I = 0x7f0c01fc

.field public static final article_chaining_item_title_text:I = 0x7f0c01fd

.field public static final article_chaining_item_width:I = 0x7f0c01f8

.field public static final article_chaining_larger_item_text_height:I = 0x7f0c0201

.field public static final article_chaining_larger_item_top_margin:I = 0x7f0c0202

.field public static final attachment_bottom_view_background_padding:I = 0x7f0c02c5

.field public static final attachment_bottom_view_background_padding_with_cta_extended:I = 0x7f0c02c6

.field public static final audience_educator_body_text_top_margin:I = 0x7f0c03d2

.field public static final audience_educator_button_height:I = 0x7f0c03cd

.field public static final audience_educator_button_horizontal_padding:I = 0x7f0c03ce

.field public static final audience_educator_button_width:I = 0x7f0c03cc

.field public static final audience_educator_element_vertical_margin:I = 0x7f0c03d1

.field public static final audience_educator_horizontal_padding:I = 0x7f0c03ca

.field public static final audience_educator_learn_more_horizontal_padding:I = 0x7f0c03d3

.field public static final audience_educator_learn_more_vertical_margin:I = 0x7f0c03d4

.field public static final audience_educator_link_separator_margin:I = 0x7f0c03cf

.field public static final audience_educator_link_top_padding:I = 0x7f0c03d0

.field public static final audience_educator_vertical_padding:I = 0x7f0c03cb

.field public static final audience_picker_fixed_icon_size:I = 0x7f0c03ac

.field public static final bar_chart_bar_height:I = 0x7f0c0093

.field public static final bar_chart_bar_spacing:I = 0x7f0c0094

.field public static final bar_chart_label_padding:I = 0x7f0c0092

.field public static final bar_chart_label_right_margin:I = 0x7f0c0091

.field public static final bar_chart_label_text_size:I = 0x7f0c0090

.field public static final bauble_bump_up_distance:I = 0x7f0c009e

.field public static final bauble_button_label_offset:I = 0x7f0c00a1

.field public static final bauble_extra_top_section_range_for_center_button:I = 0x7f0c009c

.field public static final bauble_highlight_y_threshold:I = 0x7f0c009f

.field public static final bauble_label_normal_font_size:I = 0x7f0c00a4

.field public static final bauble_label_small_font_size:I = 0x7f0c00a5

.field public static final bauble_min_edge_offset:I = 0x7f0c00a3

.field public static final bauble_radius:I = 0x7f0c00a2

.field public static final bauble_selection_locking_threshold:I = 0x7f0c009d

.field public static final bauble_selection_threshold:I = 0x7f0c009b

.field public static final bauble_stash_y_translate:I = 0x7f0c00a0

.field public static final beeper_container_padding_size:I = 0x7f0c03fc

.field public static final beeper_cross_button_padding:I = 0x7f0c03fd

.field public static final beeper_minimum_container_height:I = 0x7f0c03fa

.field public static final beeper_minimum_padding:I = 0x7f0c0400

.field public static final beeper_picture_size:I = 0x7f0c03fb

.field public static final beeper_text_container_margin:I = 0x7f0c03ff

.field public static final beeper_text_size:I = 0x7f0c03fe

.field public static final birthday_swipe_feed_unit_bottom_description_text_size:I = 0x7f0c023c

.field public static final birthday_swipe_feed_unit_bottom_name_text_size:I = 0x7f0c023b

.field public static final birthday_swipe_feed_unit_bottom_section_size:I = 0x7f0c023a

.field public static final bookmark_bottom_padding:I = 0x7f0c06b2

.field public static final bookmark_bottom_title_margin:I = 0x7f0c06b0

.field public static final bookmark_creative_width:I = 0x7f0c06ae

.field public static final bookmark_height:I = 0x7f0c0469

.field public static final bookmark_icon_size:I = 0x7f0c046b

.field public static final bookmark_max_width:I = 0x7f0c06a8

.field public static final bookmark_padding_top:I = 0x7f0c046c

.field public static final bookmark_right_title_margin:I = 0x7f0c06b1

.field public static final bookmark_tab_bottom_divider_height:I = 0x7f0c0477

.field public static final bookmark_tab_divider_height:I = 0x7f0c0476

.field public static final bookmark_tab_item_count_padding:I = 0x7f0c0475

.field public static final bookmark_tab_item_icon_size:I = 0x7f0c046e

.field public static final bookmark_tab_item_profile_icon_size:I = 0x7f0c046f

.field public static final bookmark_tab_item_profile_size:I = 0x7f0c0470

.field public static final bookmark_tab_item_size:I = 0x7f0c046d

.field public static final bookmark_tab_profile_and_divider_vertical_margins:I = 0x7f0c0474

.field public static final bookmark_tab_standard_margins:I = 0x7f0c0473

.field public static final bookmark_tab_x_container_height:I = 0x7f0c06ad

.field public static final bookmark_top_title_margin:I = 0x7f0c06af

.field public static final bookmark_total_height:I = 0x7f0c046a

.field public static final bookmark_x_container_additional_height:I = 0x7f0c06b3

.field public static final bookmark_x_container_height:I = 0x7f0c06ac

.field public static final bookmark_x_container_width:I = 0x7f0c06ab

.field public static final bookmark_x_out_height:I = 0x7f0c06aa

.field public static final bookmark_x_out_width:I = 0x7f0c06a9

.field public static final border_padding:I = 0x7f0c04bb

.field public static final bottom_border_height:I = 0x7f0c05ab

.field public static final bottom_border_padding:I = 0x7f0c04bd

.field public static final broadcast_request_add_recommendation_icon_margin:I = 0x7f0c05a8

.field public static final broadcast_request_add_recommendation_icon_size:I = 0x7f0c05a7

.field public static final broadcast_request_add_recommendation_link_padding:I = 0x7f0c05a9

.field public static final broadcast_request_images_inside_margin:I = 0x7f0c05aa

.field public static final broadcast_request_recommendations_padding:I = 0x7f0c05a6

.field public static final browser_chrome_height:I = 0x7f0c0772

.field public static final browser_chrome_overflow_width:I = 0x7f0c0773

.field public static final browser_menu_divider_height:I = 0x7f0c0777

.field public static final browser_menu_horizontal_padding:I = 0x7f0c0775

.field public static final browser_menu_popup_custom_width:I = 0x7f0c0774

.field public static final browser_pivots_shadow_height:I = 0x7f0c0778

.field public static final browser_progress_overlay_height:I = 0x7f0c0776

.field public static final bubble_popup_arrow_width:I = 0x7f0c063b

.field public static final bubble_popup_row_height:I = 0x7f0c0639

.field public static final bubble_popup_scroll_view_max_height:I = 0x7f0c063a

.field public static final camera_roll_text_size:I = 0x7f0c0384

.field public static final caption_gradient_fade_length:I = 0x7f0c00bc

.field public static final card_entry_info_icon_size:I = 0x7f0c0451

.field public static final card_face_size:I = 0x7f0c0450

.field public static final card_front_card_separation_width:I = 0x7f0c0635

.field public static final card_front_category_gallery_height:I = 0x7f0c0633

.field public static final card_front_category_item_width:I = 0x7f0c0634

.field public static final card_front_gallery_peek_width:I = 0x7f0c0636

.field public static final card_front_item_padding:I = 0x7f0c0638

.field public static final card_front_pager_margin:I = 0x7f0c0637

.field public static final card_glyph_size:I = 0x7f0c044e

.field public static final card_map_height:I = 0x7f0c044d

.field public static final card_profile_place_image_size:I = 0x7f0c044f

.field public static final cards_frame_horizontal_padding:I = 0x7f0c07af

.field public static final cards_frame_vertical_padding:I = 0x7f0c07b0

.field public static final carrier_manager_alert_height:I = 0x7f0c07c1

.field public static final carrier_manager_alert_margin:I = 0x7f0c07c2

.field public static final carrier_row_background_horizontal_padding:I = 0x7f0c07bf

.field public static final carrier_row_background_vertical_padding:I = 0x7f0c07be

.field public static final carrier_row_padding:I = 0x7f0c07c0

.field public static final category_header_padding_top:I = 0x7f0c0622

.field public static final celebrations_feed_unit_footer_button_height:I = 0x7f0c024b

.field public static final celebrations_feed_unit_profile_image_size:I = 0x7f0c024a

.field public static final chaining_section_chevron_padding_leftright:I = 0x7f0c02a7

.field public static final chaining_section_chevron_padding_top:I = 0x7f0c02a6

.field public static final chaining_section_gallery_margin_top:I = 0x7f0c02a2

.field public static final chaining_section_negative_margin_top:I = 0x7f0c02a5

.field public static final chaining_section_padding_bottom:I = 0x7f0c02a4

.field public static final chaining_section_title_padding_bottom:I = 0x7f0c02a3

.field public static final chambray_padding_for_photos_unit:I = 0x7f0c053d

.field public static final chambray_single_row_min_height:I = 0x7f0c053b

.field public static final chambray_standard_padding:I = 0x7f0c053c

.field public static final chambray_text_large:I = 0x7f0c053a

.field public static final chambray_text_medium:I = 0x7f0c0539

.field public static final chambray_text_small:I = 0x7f0c0537

.field public static final chambray_text_small_unscaled:I = 0x7f0c0538

.field public static final chat_bubble_tab_right_offset:I = 0x7f0c016e

.field public static final chat_bubble_tab_shadow_width:I = 0x7f0c016f

.field public static final chat_bubble_tab_top_offset:I = 0x7f0c016d

.field public static final chat_close_area_height:I = 0x7f0c018f

.field public static final chat_close_area_width:I = 0x7f0c018e

.field public static final chat_close_fling_area_width:I = 0x7f0c0190

.field public static final chat_close_max_attract_y:I = 0x7f0c0192

.field public static final chat_close_min_distance:I = 0x7f0c0191

.field public static final chat_close_stashed_y:I = 0x7f0c018d

.field public static final chat_close_unstashed_y:I = 0x7f0c018c

.field public static final chat_head_dismiss_velocity_threshold:I = 0x7f0c0183

.field public static final chat_head_dock_overshoot_x:I = 0x7f0c017e

.field public static final chat_head_first_stack_offset:I = 0x7f0c016c

.field public static final chat_head_height:I = 0x7f0c0166

.field public static final chat_head_line_item_max_x_offset:I = 0x7f0c017c

.field public static final chat_head_line_item_max_y_offset:I = 0x7f0c017d

.field public static final chat_head_line_item_min_x_offset:I = 0x7f0c017a

.field public static final chat_head_line_item_min_y_offset:I = 0x7f0c017b

.field public static final chat_head_notification_left_margin:I = 0x7f0c019b

.field public static final chat_head_notification_title_top_padding:I = 0x7f0c019c

.field public static final chat_head_nub_base_height:I = 0x7f0c0185

.field public static final chat_head_nub_base_width:I = 0x7f0c0184

.field public static final chat_head_nux_bubble_max_width:I = 0x7f0c0176

.field public static final chat_head_nux_bubble_text_shadow_dx:I = 0x7f0c0177

.field public static final chat_head_nux_bubble_text_shadow_dy:I = 0x7f0c0178

.field public static final chat_head_nux_bubble_text_shadow_radius:I = 0x7f0c0179

.field public static final chat_head_nux_bubble_x:I = 0x7f0c0175

.field public static final chat_head_popup_pivot_x_offset:I = 0x7f0c0189

.field public static final chat_head_popup_pivot_y_offset:I = 0x7f0c018a

.field public static final chat_head_side_spring_in_initial_velocity_x:I = 0x7f0c0181

.field public static final chat_head_side_spring_in_initial_velocity_y:I = 0x7f0c0182

.field public static final chat_head_side_spring_in_start_offset_x:I = 0x7f0c017f

.field public static final chat_head_side_spring_in_start_offset_y:I = 0x7f0c0180

.field public static final chat_head_text_bubble_min_content_height:I = 0x7f0c016b

.field public static final chat_head_text_bubble_x:I = 0x7f0c0169

.field public static final chat_head_text_bubble_y:I = 0x7f0c016a

.field public static final chat_head_tile_size:I = 0x7f0c0168

.field public static final chat_head_width:I = 0x7f0c0165

.field public static final chat_heads_custom_keyboard_right_margin:I = 0x7f0c0167

.field public static final chat_heads_mini_window_height:I = 0x7f0c0188

.field public static final chat_heads_mini_window_width:I = 0x7f0c0187

.field public static final chat_heads_minimum_portrait_height:I = 0x7f0c0171

.field public static final chat_heads_minimum_portrait_width:I = 0x7f0c0170

.field public static final chat_heads_space_saving_height:I = 0x7f0c0172

.field public static final chat_heads_space_saving_height_with_two_line_composer:I = 0x7f0c0174

.field public static final chat_heads_space_saving_insets:I = 0x7f0c0173

.field public static final chat_list_shadows_height:I = 0x7f0c019d

.field public static final chat_thread_compose_button_height:I = 0x7f0c0194

.field public static final chat_thread_compose_button_margin_left:I = 0x7f0c0197

.field public static final chat_thread_compose_button_margin_right:I = 0x7f0c0198

.field public static final chat_thread_compose_button_text:I = 0x7f0c0195

.field public static final chat_thread_compose_divider_width:I = 0x7f0c0199

.field public static final chat_thread_compose_text:I = 0x7f0c0196

.field public static final chat_thread_emoji_arrow_margin:I = 0x7f0c019a

.field public static final chat_thread_menu_button_width:I = 0x7f0c0193

.field public static final chat_title_bar_height:I = 0x7f0c018b

.field public static final checkable_row_height:I = 0x7f0c03ba

.field public static final circle_indicator_horizontal_margin:I = 0x7f0c0621

.field public static final clock_card_touch_slop:I = 0x7f0c0029

.field public static final close_button_pad_single_face:I = 0x7f0c0303

.field public static final collection_about_item_icon_size:I = 0x7f0c0536

.field public static final collection_app_icon_size:I = 0x7f0c07b5

.field public static final collection_item_horizontal_margin:I = 0x7f0c07b7

.field public static final collection_item_start_middle_section_margin:I = 0x7f0c07b9

.field public static final collection_item_vertical_margin:I = 0x7f0c07b8

.field public static final collection_list_divider_height:I = 0x7f0c07bb

.field public static final collection_list_item_button_padding:I = 0x7f0c07bd

.field public static final collection_list_item_facepile_icon_margin:I = 0x7f0c0535

.field public static final collection_list_item_facepile_icon_size:I = 0x7f0c0534

.field public static final collection_list_item_icon_size:I = 0x7f0c0533

.field public static final collection_section_header_top_padding:I = 0x7f0c07ba

.field public static final collection_section_header_vertical_padding_when_divider:I = 0x7f0c07bc

.field public static final collection_table_item_margin:I = 0x7f0c07b3

.field public static final collection_table_padding:I = 0x7f0c07b4

.field public static final collections_collection_header_height:I = 0x7f0c07b1

.field public static final collections_list_button_height:I = 0x7f0c07b2

.field public static final collections_section_header_icon_size:I = 0x7f0c07b6

.field public static final comment_photo_attachment_leftright_padding:I = 0x7f0c0036

.field public static final composer_attachment_add_photo_button_margin_top:I = 0x7f0c039d

.field public static final composer_attachment_large_thumbnails_margin_top:I = 0x7f0c039c

.field public static final composer_attachment_large_thumbnails_size:I = 0x7f0c0399

.field public static final composer_attachment_large_thumbnails_spacing:I = 0x7f0c039e

.field public static final composer_attachment_plus_icon_width:I = 0x7f0c039a

.field public static final composer_attachment_size:I = 0x7f0c0398

.field public static final composer_attachment_spacing:I = 0x7f0c039b

.field public static final composer_audience_education_overlap:I = 0x7f0c03a5

.field public static final composer_footer_bar_button_min_width:I = 0x7f0c0394

.field public static final composer_footer_bar_button_width:I = 0x7f0c0393

.field public static final composer_footer_bar_height:I = 0x7f0c0397

.field public static final composer_harrison_event_date_text_padding_top:I = 0x7f0c031b

.field public static final composer_harrison_event_date_text_size:I = 0x7f0c031c

.field public static final composer_harrison_right_button_padding_left:I = 0x7f0c0320

.field public static final composer_harrison_right_button_padding_right:I = 0x7f0c031f

.field public static final composer_harrison_round_profile_height:I = 0x7f0c031e

.field public static final composer_harrison_round_profile_width:I = 0x7f0c031d

.field public static final composer_impl_loc_max_width:I = 0x7f0c0396

.field public static final composer_impl_loc_min_width:I = 0x7f0c0395

.field public static final composer_minimum_height:I = 0x7f0c039f

.field public static final composer_minutiae_icon_size:I = 0x7f0c03a1

.field public static final composer_minutiae_profile_pic_size:I = 0x7f0c03a0

.field public static final composer_profile_pic_size:I = 0x7f0c0110

.field public static final composer_rated_rating_view_size:I = 0x7f0c03a3

.field public static final composer_rating_view_minimum_height:I = 0x7f0c03a2

.field public static final composer_rating_view_size:I = 0x7f0c03a4

.field public static final composer_status_wrapper_padding:I = 0x7f0c0111

.field public static final composer_sub_title_text_size:I = 0x7f0c0318

.field public static final composer_title_bottom_padding:I = 0x7f0c0316

.field public static final composer_title_horiz_divider_height:I = 0x7f0c0319

.field public static final composer_title_text_size:I = 0x7f0c0317

.field public static final composer_title_top_padding:I = 0x7f0c0315

.field public static final contact_multipicker_container_list_offset:I = 0x7f0c01b2

.field public static final contact_picker_sticky_header_height:I = 0x7f0c01b1

.field public static final content_text_line_spacing:I = 0x7f0c05a1

.field public static final context_items_container_divider_left_inset:I = 0x7f0c04dc

.field public static final context_items_container_left_margin:I = 0x7f0c04db

.field public static final context_items_icon_horizontal_margin:I = 0x7f0c04da

.field public static final context_items_icon_size:I = 0x7f0c04d9

.field public static final context_items_row_height:I = 0x7f0c04d8

.field public static final context_items_subtitle_text_size:I = 0x7f0c04de

.field public static final context_items_title_text_size:I = 0x7f0c04dd

.field public static final contributors_image_size:I = 0x7f0c0756

.field public static final control_widget_separator:I = 0x7f0c06b7

.field public static final cover_feed_nux_dialog_extra_top_padding:I = 0x7f0c00df

.field public static final creative_pyml_attachment_height:I = 0x7f0c0243

.field public static final crop_size_suggestion_bitmap:I = 0x7f0c0310

.field public static final custom_keyboard_layout_default_height:I = 0x7f0c00fd

.field public static final custom_keyboard_layout_max_height:I = 0x7f0c00fc

.field public static final custom_keyboard_layout_min_height:I = 0x7f0c00fb

.field public static final dash_double_tap_like_animation_y_offset:I = 0x7f0c00a9

.field public static final dash_double_tap_like_gesture_recognition_edge_offset:I = 0x7f0c00ab

.field public static final dash_double_tap_like_rendering_edge_offset:I = 0x7f0c00aa

.field public static final dash_page_margin:I = 0x7f0c00a8

.field public static final dash_status_swipe_gutter_height:I = 0x7f0c00a7

.field public static final dash_story_content_padding:I = 0x7f0c009a

.field public static final dash_story_gutter:I = 0x7f0c0099

.field public static final dash_swipe_x_slop:I = 0x7f0c00a6

.field public static final dashcard_content_vertical_gap:I = 0x7f0c04cc

.field public static final dashcard_large_icon_content_gap:I = 0x7f0c04cb

.field public static final dashcard_large_icon_height:I = 0x7f0c04ca

.field public static final dashcard_large_icon_width:I = 0x7f0c04c9

.field public static final dashcard_small_icon_content_gap:I = 0x7f0c04d0

.field public static final dashcard_small_icon_corner_radius:I = 0x7f0c04cf

.field public static final dashcard_small_icon_height:I = 0x7f0c04ce

.field public static final dashcard_small_icon_width:I = 0x7f0c04cd

.field public static final date_text_size_multiplier:I = 0x7f0c0098

.field public static final default_circle_indicator_radius:I = 0x7f0c0423

.field public static final default_circle_indicator_stroke_width:I = 0x7f0c0424

.field public static final default_fling_velocity_threshold_pps:I = 0x7f0c00de

.field public static final default_gap:I = 0x7f0c074c

.field public static final default_padding_between_pogs:I = 0x7f0c079e

.field public static final default_place_image_size:I = 0x7f0c0447

.field public static final default_right_padding:I = 0x7f0c0044

.field public static final default_scrollbar_bottom:I = 0x7f0c0043

.field public static final default_scrollbar_left:I = 0x7f0c0040

.field public static final default_scrollbar_right:I = 0x7f0c0042

.field public static final default_scrollbar_top:I = 0x7f0c0041

.field public static final default_scrollbar_width:I = 0x7f0c0045

.field public static final dialog_min_width:I = 0x7f0c0058

.field public static final dialog_min_width_major:I = 0x7f0c0056

.field public static final dialog_min_width_minor:I = 0x7f0c0057

.field public static final disturbing_video_message_margin:I = 0x7f0c0123

.field public static final disturbing_video_message_text_size:I = 0x7f0c0125

.field public static final disturbing_video_message_top_padding:I = 0x7f0c0124

.field public static final dive_head_search_box_height:I = 0x7f0c019e

.field public static final divebar_left_margin_width:I = 0x7f0c0109

.field public static final divebar_max_width:I = 0x7f0c010f

.field public static final divebar_swipe__orthogonal_threshold:I = 0x7f0c010e

.field public static final divebar_swipe__to_close_horizontal_threshold:I = 0x7f0c010c

.field public static final divebar_swipe__to_open_horizontal_threshold:I = 0x7f0c010b

.field public static final divebar_swipe__vertical_threshold:I = 0x7f0c010d

.field public static final divebar_swipe_horizontal_threshold:I = 0x7f0c010a

.field public static final divider_height:I = 0x7f0c04c3

.field public static final divider_width:I = 0x7f0c0004

.field public static final double_tap_like_image_height:I = 0x7f0c008b

.field public static final double_tap_like_image_width:I = 0x7f0c008a

.field public static final drag_sort_list_view_fast_scroll:I = 0x7f0c00f4

.field public static final drag_sort_list_view_slow_scroll:I = 0x7f0c00f5

.field public static final drawers_block_other_drawer_drag_threshold:I = 0x7f0c0427

.field public static final drawers_min_handle_width:I = 0x7f0c0426

.field public static final drawers_shadow_width:I = 0x7f0c0425

.field public static final dropshadow_height:I = 0x7f0c060f

.field public static final dttl_min_photo_height:I = 0x7f0c02bb

.field public static final dttl_min_photo_top:I = 0x7f0c02bc

.field public static final edge_offset:I = 0x7f0c0028

.field public static final edit_history_padding_bottom:I = 0x7f0c0274

.field public static final edit_history_padding_top:I = 0x7f0c0273

.field public static final ego_item_viewpager_extra_peak:I = 0x7f0c02b5

.field public static final emoji_attachments_drawer_row_bottom_padding:I = 0x7f0c0152

.field public static final emoji_attachments_drawer_row_bottom_padding_neue:I = 0x7f0c0153

.field public static final emoji_attachments_drawer_row_height:I = 0x7f0c0151

.field public static final emoji_attachments_pager_indicator_padding_right:I = 0x7f0c0154

.field public static final emoji_category_height_dp:I = 0x7f0c01ec

.field public static final end_screen_video_cta_icon_and_label_margin:I = 0x7f0c012a

.field public static final end_screen_video_fullscreen_cta_icon_size:I = 0x7f0c0131

.field public static final end_screen_video_fullscreen_cta_label_text_size:I = 0x7f0c012d

.field public static final end_screen_video_fullscreen_cta_replay_and_action_spacing:I = 0x7f0c012b

.field public static final end_screen_video_fullscreen_cta_source_text_size:I = 0x7f0c012e

.field public static final end_screen_video_inline_cta_icon_size:I = 0x7f0c0132

.field public static final end_screen_video_inline_cta_label_text_size:I = 0x7f0c012f

.field public static final end_screen_video_inline_cta_replay_and_action_spacing:I = 0x7f0c012c

.field public static final end_screen_video_inline_cta_source_text_size:I = 0x7f0c0130

.field public static final entity_card_pager_custom_vertical_margin:I = 0x7f0c0710

.field public static final entity_card_pager_page_padding:I = 0x7f0c070f

.field public static final entity_cards_pager_page_negative_offset:I = 0x7f0c070e

.field public static final entity_cards_title_height_min:I = 0x7f0c0712

.field public static final entity_cards_title_vertical_margin_min:I = 0x7f0c0711

.field public static final event_card_action_button_right_margin:I = 0x7f0c06ff

.field public static final event_card_action_button_separator_height:I = 0x7f0c06fd

.field public static final event_card_action_button_separator_right_margin:I = 0x7f0c06fe

.field public static final event_card_bottom_view_height:I = 0x7f0c06f9

.field public static final event_card_bottom_view_left_margin:I = 0x7f0c06fa

.field public static final event_card_bottom_view_padding:I = 0x7f0c06fc

.field public static final event_card_bottom_view_right_margin:I = 0x7f0c06fb

.field public static final event_card_cover_photo_height:I = 0x7f0c0700

.field public static final event_card_title_padding_bottom:I = 0x7f0c06f7

.field public static final event_card_title_padding_left:I = 0x7f0c06f5

.field public static final event_card_title_padding_right:I = 0x7f0c06f6

.field public static final event_card_title_text_size:I = 0x7f0c06f8

.field public static final event_date_text:I = 0x7f0c0547

.field public static final event_feed_list_item_header_height:I = 0x7f0c06dc

.field public static final event_guest_tile_picture_width:I = 0x7f0c06d0

.field public static final event_guest_tile_spacing:I = 0x7f0c06d2

.field public static final event_guest_tile_width:I = 0x7f0c06d1

.field public static final event_permalink_action_bar_card_height:I = 0x7f0c06d7

.field public static final event_permalink_details_artist_divider_margin:I = 0x7f0c06d4

.field public static final event_permalink_details_artist_picture_width:I = 0x7f0c06d3

.field public static final event_permalink_details_text_gradient_default_length:I = 0x7f0c06db

.field public static final event_permalink_header_text_left_padding:I = 0x7f0c06d5

.field public static final event_permalink_header_text_right_padding:I = 0x7f0c06d6

.field public static final event_permalink_page_profile_pic_size:I = 0x7f0c06cf

.field public static final event_permalink_summary_view_card_divider_margin:I = 0x7f0c06d9

.field public static final event_permalink_summary_view_card_left_icon_width:I = 0x7f0c06da

.field public static final event_summary_view_card_min_height:I = 0x7f0c06d8

.field public static final events_dashboard_birthday_button_dim:I = 0x7f0c06f3

.field public static final events_dashboard_birthday_button_padding_dim:I = 0x7f0c06f4

.field public static final events_dashboard_birthday_header_height:I = 0x7f0c06e4

.field public static final events_dashboard_card_horizontal_padding:I = 0x7f0c06ec

.field public static final events_dashboard_card_inter_page_margin:I = 0x7f0c06ee

.field public static final events_dashboard_card_page_margin_and_padding:I = 0x7f0c06ed

.field public static final events_dashboard_card_title_height:I = 0x7f0c06ef

.field public static final events_dashboard_card_view_all_height:I = 0x7f0c06f0

.field public static final events_dashboard_divider_width:I = 0x7f0c06e5

.field public static final events_dashboard_empty_list_left_padding:I = 0x7f0c06ea

.field public static final events_dashboard_empty_list_right_padding:I = 0x7f0c06eb

.field public static final events_dashboard_filter_row_height:I = 0x7f0c06dd

.field public static final events_dashboard_header_divider_width:I = 0x7f0c06e6

.field public static final events_dashboard_profile_photo_size:I = 0x7f0c06de

.field public static final events_dashboard_row_horizontal_margin:I = 0x7f0c06e7

.field public static final events_dashboard_row_inline_rsvp_border_width:I = 0x7f0c06e0

.field public static final events_dashboard_row_inline_rsvp_corner_radius:I = 0x7f0c06e1

.field public static final events_dashboard_row_inline_rsvp_height:I = 0x7f0c06df

.field public static final events_dashboard_row_vertical_margin:I = 0x7f0c06e8

.field public static final events_dashboard_separator_row_height:I = 0x7f0c06e3

.field public static final events_dashboard_view_all_row_height:I = 0x7f0c06e9

.field public static final events_row_padding:I = 0x7f0c06e2

.field public static final experimental_search_bar_height:I = 0x7f0c044c

.field public static final facebox_margin:I = 0x7f0c03b8

.field public static final facebox_offset_bottom:I = 0x7f0c03c8

.field public static final facebox_offset_left:I = 0x7f0c03c5

.field public static final facebox_offset_right:I = 0x7f0c03c7

.field public static final facebox_offset_top:I = 0x7f0c03c6

.field public static final facebox_remove_icon_margin:I = 0x7f0c03b9

.field public static final facebox_shadow_width:I = 0x7f0c03b7

.field public static final facepile_count_text_size:I = 0x7f0c008f

.field public static final fastscroll_overlay_size:I = 0x7f0c008c

.field public static final fastscroll_thumb_height:I = 0x7f0c008e

.field public static final fastscroll_thumb_width:I = 0x7f0c008d

.field public static final fb4a_login_widget_padding:I = 0x7f0c00d6

.field public static final fb_home_logo_botton_margin:I = 0x7f0c00d8

.field public static final fb_home_logo_scaled_height:I = 0x7f0c00d7

.field public static final fb_logo_badge_left_margin:I = 0x7f0c06cc

.field public static final fb_logo_badge_top_margin:I = 0x7f0c06cb

.field public static final fb_range_seek_bar_thumb_bg_radius:I = 0x7f0c0422

.field public static final fb_range_seek_bar_thumb_border_radius:I = 0x7f0c0421

.field public static final fb_range_seek_bar_thumb_radius:I = 0x7f0c0420

.field public static final fb_range_seek_bar_width:I = 0x7f0c041f

.field public static final fbui_button_padding_bottom_large:I = 0x7f0c006d

.field public static final fbui_button_padding_bottom_medium:I = 0x7f0c006c

.field public static final fbui_button_padding_bottom_small:I = 0x7f0c006b

.field public static final fbui_button_padding_left_large:I = 0x7f0c0070

.field public static final fbui_button_padding_left_medium:I = 0x7f0c006f

.field public static final fbui_button_padding_left_small:I = 0x7f0c006e

.field public static final fbui_button_padding_right_large:I = 0x7f0c0073

.field public static final fbui_button_padding_right_medium:I = 0x7f0c0072

.field public static final fbui_button_padding_right_small:I = 0x7f0c0071

.field public static final fbui_button_padding_top_large:I = 0x7f0c006a

.field public static final fbui_button_padding_top_medium:I = 0x7f0c0069

.field public static final fbui_button_padding_top_small:I = 0x7f0c0068

.field public static final fbui_card_padding_bottom:I = 0x7f0c005a

.field public static final fbui_card_padding_left:I = 0x7f0c005b

.field public static final fbui_card_padding_right:I = 0x7f0c005c

.field public static final fbui_card_padding_top:I = 0x7f0c0059

.field public static final fbui_check_inner_ring_inset:I = 0x7f0c007a

.field public static final fbui_check_outer_ring_inset:I = 0x7f0c0079

.field public static final fbui_check_outer_ring_stroke_size:I = 0x7f0c0078

.field public static final fbui_check_size:I = 0x7f0c0077

.field public static final fbui_content_view_action_button_width:I = 0x7f0c007e

.field public static final fbui_content_view_attachment_padding:I = 0x7f0c007d

.field public static final fbui_content_view_horizontal_padding:I = 0x7f0c007c

.field public static final fbui_content_view_thumbnail_margin_right:I = 0x7f0c0081

.field public static final fbui_content_view_tw1l_thumbnail_width_height:I = 0x7f0c0082

.field public static final fbui_content_view_tw2l_expandable_padding:I = 0x7f0c0080

.field public static final fbui_content_view_tw2l_expandable_thumbnail_width_height:I = 0x7f0c0084

.field public static final fbui_content_view_tw2l_thumbnail_width_height:I = 0x7f0c0083

.field public static final fbui_content_view_tw3l_thumbnail_width_height:I = 0x7f0c0085

.field public static final fbui_content_view_tw4l_thumbnail_width_height:I = 0x7f0c0086

.field public static final fbui_content_view_vertical_padding:I = 0x7f0c007b

.field public static final fbui_content_view_with_image_button_divider_padding:I = 0x7f0c007f

.field public static final fbui_dialog_icon_size:I = 0x7f0c030f

.field public static final fbui_drawable_padding:I = 0x7f0c0067

.field public static final fbui_list_divider_padding:I = 0x7f0c0074

.field public static final fbui_list_header_padding_bottom:I = 0x7f0c0076

.field public static final fbui_list_header_padding_top:I = 0x7f0c0075

.field public static final fbui_megaphone_button_side_padding:I = 0x7f0c0329

.field public static final fbui_megaphone_default_padding:I = 0x7f0c0326

.field public static final fbui_megaphone_image_size:I = 0x7f0c0328

.field public static final fbui_megaphone_top_padding:I = 0x7f0c0327

.field public static final fbui_padding_half_standard:I = 0x7f0c0064

.field public static final fbui_padding_half_text:I = 0x7f0c0065

.field public static final fbui_padding_one_fourth_standard:I = 0x7f0c0066

.field public static final fbui_padding_standard:I = 0x7f0c0062

.field public static final fbui_padding_text:I = 0x7f0c0063

.field public static final fbui_popover_list_header_height:I = 0x7f0c0414

.field public static final fbui_popover_list_item_view_height:I = 0x7f0c0411

.field public static final fbui_popover_list_item_view_horizontal_padding:I = 0x7f0c0412

.field public static final fbui_popover_list_item_view_vertical_padding:I = 0x7f0c0413

.field public static final fbui_popover_list_view_corner_radius:I = 0x7f0c0415

.field public static final fbui_popover_window_anim_enter_min_alpha:I = 0x7f0c0419

.field public static final fbui_popover_window_anim_exit_min_alpha:I = 0x7f0c041a

.field public static final fbui_popover_window_anim_max_alpha:I = 0x7f0c041b

.field public static final fbui_popover_window_anim_max_scale:I = 0x7f0c0418

.field public static final fbui_popover_window_anim_min_scale:I = 0x7f0c0417

.field public static final fbui_popover_window_anim_pivot_bottom:I = 0x7f0c041e

.field public static final fbui_popover_window_anim_pivot_center:I = 0x7f0c041d

.field public static final fbui_popover_window_anim_pivot_top:I = 0x7f0c041c

.field public static final fbui_popover_window_interpolator_tension:I = 0x7f0c0416

.field public static final fbui_section_header_padding:I = 0x7f0c070d

.field public static final fbui_text_size_all_caps:I = 0x7f0c005d

.field public static final fbui_text_size_large:I = 0x7f0c0060

.field public static final fbui_text_size_medium:I = 0x7f0c005f

.field public static final fbui_text_size_small:I = 0x7f0c005e

.field public static final fbui_text_size_xlarge:I = 0x7f0c0061

.field public static final fbui_tooltip_above_overlap:I = 0x7f0c05b1

.field public static final fbui_tooltip_arrow_width:I = 0x7f0c05b3

.field public static final fbui_tooltip_below_overlap:I = 0x7f0c05b2

.field public static final feed_action_icon_padding_left:I = 0x7f0c0224

.field public static final feed_action_icon_padding_right:I = 0x7f0c0223

.field public static final feed_attachment_action_container_padding:I = 0x7f0c029e

.field public static final feed_attachment_action_icon_width:I = 0x7f0c029d

.field public static final feed_attachment_coupon_padding_top:I = 0x7f0c021c

.field public static final feed_attachment_cover_image_size:I = 0x7f0c029b

.field public static final feed_attachment_default_height:I = 0x7f0c0038

.field public static final feed_attachment_divider_length:I = 0x7f0c003d

.field public static final feed_attachment_divider_margin:I = 0x7f0c003e

.field public static final feed_attachment_divider_width:I = 0x7f0c003f

.field public static final feed_attachment_event_height:I = 0x7f0c003b

.field public static final feed_attachment_event_line_spacing:I = 0x7f0c003c

.field public static final feed_attachment_height_for_three_line_title:I = 0x7f0c0039

.field public static final feed_attachment_height_for_two_line_context:I = 0x7f0c003a

.field public static final feed_attachment_image_padding:I = 0x7f0c002b

.field public static final feed_attachment_image_size:I = 0x7f0c002a

.field public static final feed_attachment_image_size_angora:I = 0x7f0c0037

.field public static final feed_attachment_image_size_padded:I = 0x7f0c002c

.field public static final feed_attachment_large_image_padding:I = 0x7f0c0295

.field public static final feed_attachment_large_image_size:I = 0x7f0c0294

.field public static final feed_attachment_leftright_margins:I = 0x7f0c002e

.field public static final feed_attachment_profile_image_size:I = 0x7f0c029c

.field public static final feed_attachment_rounding:I = 0x7f0c021b

.field public static final feed_attachment_video_pager_ufi_bottom_shadow_margin:I = 0x7f0c0032

.field public static final feed_attachment_video_pager_ufi_leftright_margins:I = 0x7f0c0031

.field public static final feed_bling_bar_height:I = 0x7f0c0268

.field public static final feed_bling_bar_height_multi_row:I = 0x7f0c0269

.field public static final feed_bling_bar_padding:I = 0x7f0c026a

.field public static final feed_bling_bar_spacing:I = 0x7f0c0275

.field public static final feed_card_content_padding:I = 0x7f0c0226

.field public static final feed_checkin_story_save_button_horizontal_margin:I = 0x7f0c02d5

.field public static final feed_cover_photo_height:I = 0x7f0c0259

.field public static final feed_default_message_font_size:I = 0x7f0c00c1

.field public static final feed_digital_goods_description_height:I = 0x7f0c02ae

.field public static final feed_digital_goods_description_margin:I = 0x7f0c02af

.field public static final feed_digital_goods_footer_margin:I = 0x7f0c02b0

.field public static final feed_digital_goods_item_border_width:I = 0x7f0c02b1

.field public static final feed_digital_goods_padding_bottom:I = 0x7f0c02ab

.field public static final feed_digital_goods_padding_left:I = 0x7f0c02a8

.field public static final feed_digital_goods_padding_right:I = 0x7f0c02aa

.field public static final feed_digital_goods_padding_top:I = 0x7f0c02a9

.field public static final feed_digital_goods_unit_padding_bottom:I = 0x7f0c02ad

.field public static final feed_digital_goods_unit_padding_top:I = 0x7f0c02ac

.field public static final feed_ego_unit_x_out_margin:I = 0x7f0c02d0

.field public static final feed_ego_unit_x_out_padding:I = 0x7f0c02d1

.field public static final feed_feedback_button_spacer:I = 0x7f0c0277

.field public static final feed_feedback_button_text_size:I = 0x7f0c0278

.field public static final feed_feedback_press_state_padding:I = 0x7f0c0276

.field public static final feed_feedback_spring_touch_slop:I = 0x7f0c0279

.field public static final feed_flyout_comment_list_bottom_padding_height:I = 0x7f0c028e

.field public static final feed_flyout_drag_handle_height:I = 0x7f0c028d

.field public static final feed_friending_button_padding_left:I = 0x7f0c02e0

.field public static final feed_friending_button_padding_right:I = 0x7f0c02e1

.field public static final feed_friending_button_padding_top:I = 0x7f0c02df

.field public static final feed_hidden_margin:I = 0x7f0c0285

.field public static final feed_hidden_padding:I = 0x7f0c0284

.field public static final feed_hidden_story_divider_margin_bottom:I = 0x7f0c028c

.field public static final feed_hidden_story_divider_margin_horizontal:I = 0x7f0c028b

.field public static final feed_hidden_story_divider_size:I = 0x7f0c028a

.field public static final feed_hidden_story_horizontal_padding:I = 0x7f0c0288

.field public static final feed_hidden_story_margin_horizontal:I = 0x7f0c0286

.field public static final feed_hidden_story_margin_vertical:I = 0x7f0c0287

.field public static final feed_hidden_story_text_size:I = 0x7f0c0289

.field public static final feed_inline_btn_margin:I = 0x7f0c011e

.field public static final feed_inline_clickable_area_height:I = 0x7f0c0120

.field public static final feed_inline_clickable_area_width:I = 0x7f0c011f

.field public static final feed_inline_video_bottom_margin:I = 0x7f0c02d4

.field public static final feed_inline_video_control_height:I = 0x7f0c011d

.field public static final feed_inline_video_control_margin:I = 0x7f0c011c

.field public static final feed_inline_video_control_padding:I = 0x7f0c011b

.field public static final feed_inline_video_left_margin:I = 0x7f0c02d2

.field public static final feed_inline_video_top_margin:I = 0x7f0c02d3

.field public static final feed_legacy_attachment_save_button_bottom_margin:I = 0x7f0c02d9

.field public static final feed_legacy_attachment_save_button_left_margin:I = 0x7f0c02d7

.field public static final feed_legacy_attachment_save_button_right_margin:I = 0x7f0c02d6

.field public static final feed_legacy_attachment_save_button_top_margin:I = 0x7f0c02d8

.field public static final feed_likable_story_title_right_margin:I = 0x7f0c0214

.field public static final feed_likers_profile_image_size:I = 0x7f0c00da

.field public static final feed_location_bubble_leftright_padding:I = 0x7f0c0217

.field public static final feed_location_bubble_margins:I = 0x7f0c0216

.field public static final feed_location_horizontal_margins:I = 0x7f0c0218

.field public static final feed_location_image_size:I = 0x7f0c0215

.field public static final feed_location_more_text_large:I = 0x7f0c021a

.field public static final feed_location_more_text_small:I = 0x7f0c0219

.field public static final feed_location_nux_y_offset:I = 0x7f0c0229

.field public static final feed_location_story_margin:I = 0x7f0c0228

.field public static final feed_med_message_font_size:I = 0x7f0c00c2

.field public static final feed_offline_posting_padding_height:I = 0x7f0c0222

.field public static final feed_photo_grid_shadow_inside_col_margin:I = 0x7f0c025e

.field public static final feed_photo_grid_shadow_inside_row_margin:I = 0x7f0c025f

.field public static final feed_photo_grid_shadow_outside_margin:I = 0x7f0c025d

.field public static final feed_photo_story_space_above_message:I = 0x7f0c00c6

.field public static final feed_photo_story_space_above_timestamp:I = 0x7f0c00c7

.field public static final feed_place_collection_link_height:I = 0x7f0c025c

.field public static final feed_postpost_badge_bubble_height:I = 0x7f0c029a

.field public static final feed_postpost_badge_bubble_width:I = 0x7f0c0299

.field public static final feed_postpost_badge_icon_size:I = 0x7f0c0298

.field public static final feed_postpost_badge_margin_bottom:I = 0x7f0c0297

.field public static final feed_postpost_badge_margin_left:I = 0x7f0c0296

.field public static final feed_profile_image_size:I = 0x7f0c00d9

.field public static final feed_profile_list_image_size:I = 0x7f0c0263

.field public static final feed_profile_photo_shadow_margin:I = 0x7f0c00c4

.field public static final feed_ptr_error_padding:I = 0x7f0c0220

.field public static final feed_quick_cam_record_button_dimx:I = 0x7f0c0701

.field public static final feed_quick_cam_record_button_dimy:I = 0x7f0c0702

.field public static final feed_quick_cam_record_button_padding:I = 0x7f0c0703

.field public static final feed_quick_cam_record_button_text_size:I = 0x7f0c0704

.field public static final feed_quick_cam_top_action_bar_text_size:I = 0x7f0c0705

.field public static final feed_quick_cam_top_bar_layout_margin:I = 0x7f0c0707

.field public static final feed_quick_cam_top_title_bar_profile_image_dimX:I = 0x7f0c0708

.field public static final feed_quick_cam_top_title_bar_profile_image_dimY:I = 0x7f0c0709

.field public static final feed_quick_cam_top_title_bar_text_size:I = 0x7f0c0706

.field public static final feed_quick_cam_video_control_height:I = 0x7f0c070a

.field public static final feed_quick_cam_video_control_padding:I = 0x7f0c070b

.field public static final feed_quick_cam_video_control_play_button_margin:I = 0x7f0c070c

.field public static final feed_research_poll_answer_height:I = 0x7f0c02b8

.field public static final feed_research_poll_answer_text:I = 0x7f0c02b9

.field public static final feed_settings_header_item_height:I = 0x7f0c05d4

.field public static final feed_settings_item_horizontal_margin:I = 0x7f0c05d5

.field public static final feed_settings_item_icon_size:I = 0x7f0c05dc

.field public static final feed_settings_item_separator_height:I = 0x7f0c05d6

.field public static final feed_settings_item_separator_height_thick:I = 0x7f0c05d7

.field public static final feed_settings_item_vertical_margin:I = 0x7f0c05d8

.field public static final feed_settings_secondary_action_button_touch_margin:I = 0x7f0c05d9

.field public static final feed_settings_see_more_item_height:I = 0x7f0c05da

.field public static final feed_settings_tabs_spinner_text_size:I = 0x7f0c05db

.field public static final feed_short_message_font_size:I = 0x7f0c00c3

.field public static final feed_status_default_space_above_actor:I = 0x7f0c00c8

.field public static final feed_status_large_actor_to_message_space:I = 0x7f0c00c9

.field public static final feed_status_large_message_to_suffix_space:I = 0x7f0c00ca

.field public static final feed_status_medium_actor_to_message_space:I = 0x7f0c00cb

.field public static final feed_status_medium_message_to_suffix_space:I = 0x7f0c00cc

.field public static final feed_status_profile_pic_height:I = 0x7f0c00d0

.field public static final feed_status_profile_pic_margin_left:I = 0x7f0c00d1

.field public static final feed_status_profile_pic_margin_top:I = 0x7f0c00d2

.field public static final feed_status_profile_pic_width:I = 0x7f0c00cf

.field public static final feed_status_small_actor_to_message_space:I = 0x7f0c00cd

.field public static final feed_status_small_message_to_suffix_space:I = 0x7f0c00ce

.field public static final feed_store_back_check_button:I = 0x7f0c0498

.field public static final feed_store_back_margin_sides:I = 0x7f0c0489

.field public static final feed_store_back_margin_top:I = 0x7f0c048a

.field public static final feed_store_back_row_height:I = 0x7f0c0497

.field public static final feed_store_back_service_name_font_size:I = 0x7f0c0484

.field public static final feed_store_back_settings_font_size:I = 0x7f0c0485

.field public static final feed_store_back_username_font_size:I = 0x7f0c0486

.field public static final feed_store_card_toggle_button_size:I = 0x7f0c048c

.field public static final feed_store_done_button_margin:I = 0x7f0c0493

.field public static final feed_store_expiration_text_margin_bottom:I = 0x7f0c0496

.field public static final feed_store_expiration_text_margin_sides:I = 0x7f0c0494

.field public static final feed_store_expiration_text_margin_top:I = 0x7f0c0495

.field public static final feed_store_facebook_back_header_font_size:I = 0x7f0c048b

.field public static final feed_store_facebook_back_username_font_size:I = 0x7f0c048d

.field public static final feed_store_front_error_font_size:I = 0x7f0c047e

.field public static final feed_store_front_margin_sides:I = 0x7f0c0487

.field public static final feed_store_front_margin_top:I = 0x7f0c0488

.field public static final feed_store_front_reconnect_font_size:I = 0x7f0c047f

.field public static final feed_store_log_out_font_size:I = 0x7f0c0480

.field public static final feed_store_log_out_margin_bottom:I = 0x7f0c0481

.field public static final feed_store_logo_size:I = 0x7f0c0483

.field public static final feed_store_min_pull_down_translation:I = 0x7f0c00db

.field public static final feed_store_pager_container_width:I = 0x7f0c0499

.field public static final feed_store_profile_photo_size:I = 0x7f0c0482

.field public static final feed_store_rounded_corner_radius:I = 0x7f0c047b

.field public static final feed_store_service_name_font_size:I = 0x7f0c047c

.field public static final feed_store_subtext_font_size:I = 0x7f0c047d

.field public static final feed_store_summary_text_font_size:I = 0x7f0c0492

.field public static final feed_store_text_container_margin_sides:I = 0x7f0c048f

.field public static final feed_store_text_container_margin_top:I = 0x7f0c048e

.field public static final feed_store_title_text_font_size:I = 0x7f0c0491

.field public static final feed_store_title_text_margin_bottom:I = 0x7f0c0490

.field public static final feed_story_app_name_padding:I = 0x7f0c0030

.field public static final feed_story_attachment_border_width:I = 0x7f0c0283

.field public static final feed_story_attachment_call_to_action_icon_size:I = 0x7f0c02c4

.field public static final feed_story_attachment_call_to_action_margin:I = 0x7f0c02c7

.field public static final feed_story_attachment_gallery_spacing:I = 0x7f0c0282

.field public static final feed_story_attachment_padding:I = 0x7f0c002f

.field public static final feed_story_bg_inset_top_bottom:I = 0x7f0c019f

.field public static final feed_story_bg_shadow_size:I = 0x7f0c0254

.field public static final feed_story_element_separation_photo_bottom:I = 0x7f0c027f

.field public static final feed_story_element_separation_photo_top:I = 0x7f0c027e

.field public static final feed_story_element_separation_with_shadow:I = 0x7f0c002d

.field public static final feed_story_element_separation_without_shadow:I = 0x7f0c027d

.field public static final feed_story_feedback_height:I = 0x7f0c0255

.field public static final feed_story_header_info_font_size:I = 0x7f0c020b

.field public static final feed_story_header_line_spacing:I = 0x7f0c020c

.field public static final feed_story_header_margin_right:I = 0x7f0c0208

.field public static final feed_story_header_margin_top:I = 0x7f0c0207

.field public static final feed_story_header_menu_button_size:I = 0x7f0c020e

.field public static final feed_story_header_padding_bottom:I = 0x7f0c020d

.field public static final feed_story_header_profile_image_margin_right:I = 0x7f0c0209

.field public static final feed_story_header_title_font_size:I = 0x7f0c020a

.field public static final feed_story_inline_video_leftright_margins:I = 0x7f0c0281

.field public static final feed_story_loading_spinner_dim:I = 0x7f0c00d3

.field public static final feed_story_location_place_info_padding:I = 0x7f0c022a

.field public static final feed_story_location_section_height:I = 0x7f0c027b

.field public static final feed_story_location_section_map_height:I = 0x7f0c027c

.field public static final feed_story_margin:I = 0x7f0c0225

.field public static final feed_story_menu_button_size:I = 0x7f0c0210

.field public static final feed_story_menu_button_size_multirow:I = 0x7f0c0211

.field public static final feed_story_menu_padding_left:I = 0x7f0c0256

.field public static final feed_story_padding:I = 0x7f0c027a

.field public static final feed_story_postpost_badge_margin:I = 0x7f0c020f

.field public static final feed_story_premium_videos_menu_padding_left:I = 0x7f0c0257

.field public static final feed_story_pyml_divider_height:I = 0x7f0c0227

.field public static final feed_story_secondary_actions_top_offset:I = 0x7f0c0262

.field public static final feed_story_title_info_privacy_scope_width:I = 0x7f0c0267

.field public static final feed_story_topic_pivot_header_icon_height:I = 0x7f0c02c8

.field public static final feed_story_topic_pivot_header_icon_padding_right:I = 0x7f0c02ca

.field public static final feed_story_topic_pivot_header_icon_width:I = 0x7f0c02c9

.field public static final feed_story_topic_pivot_header_subtext:I = 0x7f0c02cb

.field public static final feed_story_topic_pivot_header_text:I = 0x7f0c02cc

.field public static final feed_story_topic_pivot_text_padding_between:I = 0x7f0c02ce

.field public static final feed_story_topic_pivot_text_padding_top_bottom:I = 0x7f0c02cd

.field public static final feed_story_upper_right_button_padding:I = 0x7f0c0212

.field public static final feed_story_upper_right_button_padding_top:I = 0x7f0c0213

.field public static final feed_story_video_shadow_bottom_margin:I = 0x7f0c0280

.field public static final feed_substory_bottom_padding:I = 0x7f0c0266

.field public static final feed_substory_feedback_border_width:I = 0x7f0c0265

.field public static final feed_suffix_line_height_fix:I = 0x7f0c00d4

.field public static final feed_survey_answer_height:I = 0x7f0c02b6

.field public static final feed_survey_answer_text:I = 0x7f0c02b7

.field public static final feed_timeline_app_collection_button_margin:I = 0x7f0c0292

.field public static final feed_timeline_app_collection_button_size:I = 0x7f0c0291

.field public static final feed_title_font_size:I = 0x7f0c00c5

.field public static final feed_title_line_height_fix:I = 0x7f0c00d5

.field public static final feed_top_bar_height:I = 0x7f0c021e

.field public static final feed_top_no_ptr_margin:I = 0x7f0c0221

.field public static final feed_top_ptr_margin:I = 0x7f0c021f

.field public static final feed_travel_welcome_cover_padding:I = 0x7f0c02de

.field public static final film_strip_handle_left_right_padding:I = 0x7f0c0137

.field public static final film_strip_handle_right_left_padding:I = 0x7f0c0138

.field public static final film_strip_separator_width:I = 0x7f0c0136

.field public static final find_friend_bar_height:I = 0x7f0c073a

.field public static final fitness_card_marker_text_size:I = 0x7f0c0592

.field public static final fitness_card_route_circular_pin_border_radius:I = 0x7f0c058e

.field public static final fitness_card_route_circular_pin_inner_radius:I = 0x7f0c058f

.field public static final fitness_card_route_circular_pin_radius:I = 0x7f0c058d

.field public static final fitness_card_route_line_border_width:I = 0x7f0c058c

.field public static final fitness_card_route_line_width:I = 0x7f0c058b

.field public static final fitness_card_route_map_padding_horizontal:I = 0x7f0c0590

.field public static final fitness_card_route_map_padding_vertical:I = 0x7f0c0591

.field public static final flyout_tablet_padding:I = 0x7f0c0290

.field public static final flyout_tablet_width:I = 0x7f0c028f

.field public static final footer_height:I = 0x7f0c0452

.field public static final form_item_height:I = 0x7f0c0647

.field public static final form_item_horizontal_padding:I = 0x7f0c064a

.field public static final form_item_vertical_padding:I = 0x7f0c0648

.field public static final form_item_vertical_padding_and_half:I = 0x7f0c0649

.field public static final friend_finder_avatar_width:I = 0x7f0c0668

.field public static final friend_finder_header_divider_height:I = 0x7f0c0665

.field public static final friend_finder_header_section_height:I = 0x7f0c0666

.field public static final friend_finder_row_padding:I = 0x7f0c0667

.field public static final friend_inviter_horizontal_padding:I = 0x7f0c0751

.field public static final friend_inviter_vertical_padding:I = 0x7f0c0752

.field public static final friend_name_top_margin:I = 0x7f0c00f6

.field public static final friend_request_image_size:I = 0x7f0c00f7

.field public static final friend_request_reply_button_height:I = 0x7f0c00e0

.field public static final friend_request_reply_button_width:I = 0x7f0c00e1

.field public static final friending_radar_extra_tapping_area:I = 0x7f0c073c

.field public static final friending_radar_nux_horizontal_margin:I = 0x7f0c073b

.field public static final friending_radar_ping_anim_size:I = 0x7f0c073d

.field public static final friending_radar_profile_pic_size:I = 0x7f0c073e

.field public static final friending_radar_vertical_padding:I = 0x7f0c073f

.field public static final friendlist_pic_size:I = 0x7f0c07ae

.field public static final friends_nearby_dashboard_image_size:I = 0x7f0c04d5

.field public static final friends_nearby_feed_unit_image_size:I = 0x7f0c0250

.field public static final friends_nearby_feed_unit_row_padding_bottom:I = 0x7f0c0252

.field public static final friends_nearby_feed_unit_row_padding_top:I = 0x7f0c0251

.field public static final friends_nearby_map_start_height:I = 0x7f0c04d6

.field public static final full_screen_video_call_to_action_left_margin:I = 0x7f0c0126

.field public static final full_screen_video_call_to_action_right_margin:I = 0x7f0c0127

.field public static final full_screen_video_call_to_action_title_and_source_spacing:I = 0x7f0c0128

.field public static final full_screen_video_call_to_action_top_margin:I = 0x7f0c0129

.field public static final fullscreen_video_control_height:I = 0x7f0c0122

.field public static final fullscreen_video_control_padding:I = 0x7f0c0121

.field public static final gallery_privacy_icon_size:I = 0x7f0c02f7

.field public static final gallery_text_size:I = 0x7f0c02f5

.field public static final gallery_time_text_size:I = 0x7f0c02f6

.field public static final gap_section_height:I = 0x7f0c05ac

.field public static final gift_card_call_gift_card_product_image_dimens:I = 0x7f0c0653

.field public static final gift_card_mall_horizontal_button_height:I = 0x7f0c0658

.field public static final gift_card_mall_horizontal_selector_height:I = 0x7f0c0657

.field public static final gift_card_mall_other_gifts_card_list_header_height:I = 0x7f0c0655

.field public static final gift_card_mall_other_products_list_header_height:I = 0x7f0c0656

.field public static final gift_card_mall_product_image_height:I = 0x7f0c0651

.field public static final gift_card_mall_product_image_width:I = 0x7f0c0652

.field public static final gift_card_mall_profile_photo_dimens:I = 0x7f0c0654

.field public static final grab_bag_header_height:I = 0x7f0c065b

.field public static final grab_bag_option_picker_height:I = 0x7f0c065c

.field public static final grab_bag_product_item_dimens:I = 0x7f0c0659

.field public static final grab_bag_product_list_horizontal_padding:I = 0x7f0c065a

.field public static final grab_bag_profile_pic_dimens:I = 0x7f0c065d

.field public static final grab_bag_selector_divider_width:I = 0x7f0c065e

.field public static final graph_search_activity_log_dialog_padding:I = 0x7f0c069f

.field public static final graph_search_box_corner_radius:I = 0x7f0c067c

.field public static final graph_search_box_height:I = 0x7f0c0677

.field public static final graph_search_box_icon_padding:I = 0x7f0c0679

.field public static final graph_search_box_margin:I = 0x7f0c067b

.field public static final graph_search_box_padding:I = 0x7f0c067a

.field public static final graph_search_custom_value_list_row_height:I = 0x7f0c0685

.field public static final graph_search_error_banner_height:I = 0x7f0c06a6

.field public static final graph_search_filter_image_left_padding:I = 0x7f0c0687

.field public static final graph_search_filter_image_right_padding:I = 0x7f0c0688

.field public static final graph_search_filter_image_width_height:I = 0x7f0c0686

.field public static final graph_search_filter_list_padding:I = 0x7f0c0684

.field public static final graph_search_group_header_action_image_padding:I = 0x7f0c069e

.field public static final graph_search_list_divider_height:I = 0x7f0c0675

.field public static final graph_search_list_divider_indent:I = 0x7f0c0674

.field public static final graph_search_list_view_photo_divider:I = 0x7f0c0682

.field public static final graph_search_padding_small:I = 0x7f0c0676

.field public static final graph_search_results_load_more_height_dp:I = 0x7f0c0683

.field public static final graph_search_see_more_empty_text_size:I = 0x7f0c068a

.field public static final graph_search_see_more_empty_text_top_padding:I = 0x7f0c0689

.field public static final graph_search_see_more_row_vertical_margin:I = 0x7f0c068b

.field public static final graph_search_suggestions_header_padding_top:I = 0x7f0c067f

.field public static final graph_search_suggestions_header_padding_top_first:I = 0x7f0c067e

.field public static final graph_search_suggestions_loading_spinner_height:I = 0x7f0c0680

.field public static final graph_search_suggestions_loading_spinner_margin:I = 0x7f0c0681

.field public static final graph_search_suggestions_typeahead_padding_left_right:I = 0x7f0c067d

.field public static final graph_search_title_bar_text_size:I = 0x7f0c0678

.field public static final group_cover_photo_crop_height:I = 0x7f0c01cf

.field public static final group_cover_photo_crop_width:I = 0x7f0c01ce

.field public static final group_icon_facepile_image_size:I = 0x7f0c0242

.field public static final groups_feed_approval_bar_bottom_padding:I = 0x7f0c07a8

.field public static final groups_feed_bar_margin:I = 0x7f0c079f

.field public static final groups_feed_header_action_bar_height:I = 0x7f0c07a5

.field public static final groups_feed_header_action_bar_separator_size:I = 0x7f0c07a6

.field public static final groups_feed_header_action_bar_spacing_after:I = 0x7f0c07a7

.field public static final groups_feed_header_chevron_bottom_padding:I = 0x7f0c07a2

.field public static final groups_feed_header_height:I = 0x7f0c07a0

.field public static final groups_feed_header_spacing_after:I = 0x7f0c07a4

.field public static final groups_feed_header_title_bottom_padding:I = 0x7f0c07a1

.field public static final groups_feed_header_visibility_description_bottom_padding:I = 0x7f0c07a3

.field public static final groups_feed_members_bar_bottom_padding:I = 0x7f0c07ab

.field public static final groups_feed_members_bar_left_padding:I = 0x7f0c07ac

.field public static final groups_feed_members_bar_right_padding:I = 0x7f0c07ad

.field public static final groups_feed_members_bar_top_padding:I = 0x7f0c07aa

.field public static final groups_feed_padding_between_pogs:I = 0x7f0c07a9

.field public static final groups_settings_arrow_size:I = 0x7f0c01ba

.field public static final groups_settings_cancel_button_size:I = 0x7f0c01b8

.field public static final groups_settings_color_square_list_size:I = 0x7f0c01b7

.field public static final groups_settings_color_square_margin:I = 0x7f0c01b6

.field public static final groups_settings_color_square_size:I = 0x7f0c01b5

.field public static final groups_settings_first_row_height:I = 0x7f0c01b9

.field public static final groups_settings_row_height:I = 0x7f0c01bb

.field public static final groups_settings_search_friends_height:I = 0x7f0c01bf

.field public static final groups_settings_separator_height:I = 0x7f0c01bd

.field public static final groups_settings_separator_margin:I = 0x7f0c01be

.field public static final groups_settings_separator_padding:I = 0x7f0c01bc

.field public static final groups_settings_switch_min_height:I = 0x7f0c01c0

.field public static final happy_birthday_feed_unit_line_height:I = 0x7f0c05ad

.field public static final happy_birthday_feed_unit_profile_pic_size:I = 0x7f0c05ae

.field public static final happy_birthday_feed_unit_profile_pic_spacing:I = 0x7f0c05af

.field public static final harrison_overlap_padding:I = 0x7f0c01ed

.field public static final harrison_tab_bar_above_indicator_height:I = 0x7f0c00f0

.field public static final harrison_tab_bar_above_indicator_padding_bottom:I = 0x7f0c00f2

.field public static final harrison_tab_bar_above_indicator_padding_top:I = 0x7f0c00f1

.field public static final harrison_tab_bar_height:I = 0x7f0c00ed

.field public static final harrison_tab_bar_icon_margin_bottom:I = 0x7f0c00ef

.field public static final harrison_tab_bar_icon_margin_top:I = 0x7f0c00ee

.field public static final harrison_tab_bar_indicator_height:I = 0x7f0c00f3

.field public static final harrison_tile_bar_plus_tab_bar_height:I = 0x7f0c06ce

.field public static final harrison_title_bar_height:I = 0x7f0c031a

.field public static final harrison_title_bar_text_size:I = 0x7f0c0325

.field public static final harrison_title_margin_right:I = 0x7f0c06b4

.field public static final home_double_standard_padding:I = 0x7f0c0019

.field public static final home_half_standard_padding:I = 0x7f0c001b

.field public static final home_standard_padding:I = 0x7f0c001a

.field public static final homesetup_footer_height:I = 0x7f0c04c7

.field public static final homesetup_title_bar_height:I = 0x7f0c04c6

.field public static final horizontal_divider_height:I = 0x7f0c060e

.field public static final horizontal_dotted_line_divider_height:I = 0x7f0c065f

.field public static final hscroll_feed_unit_view_padding_bottom:I = 0x7f0c02a0

.field public static final hscroll_feed_unit_view_padding_top:I = 0x7f0c029f

.field public static final hscroll_item_view_margins:I = 0x7f0c02a1

.field public static final identity_growth_button_min_height:I = 0x7f0c0438

.field public static final identity_growth_button_min_width:I = 0x7f0c0437

.field public static final identity_growth_large_spacing:I = 0x7f0c0431

.field public static final identity_growth_loading_indicator_size:I = 0x7f0c0435

.field public static final identity_growth_medium_spacing:I = 0x7f0c0432

.field public static final identity_growth_privacy_image_size:I = 0x7f0c0436

.field public static final identity_growth_small_spacing:I = 0x7f0c0433

.field public static final identity_growth_xsmall_spacing:I = 0x7f0c0434

.field public static final image_picker_thumbnail_height:I = 0x7f0c030c

.field public static final image_picker_thumbnail_margin:I = 0x7f0c030e

.field public static final image_picker_thumbnail_width:I = 0x7f0c030d

.field public static final image_reorder_bottom_padding:I = 0x7f0c0391

.field public static final image_reorder_in_between_padding:I = 0x7f0c0392

.field public static final image_reorder_top_padding:I = 0x7f0c0390

.field public static final in_app_notification_font_size_dp:I = 0x7f0c01eb

.field public static final info_text_size:I = 0x7f0c076a

.field public static final inline_action_button_default_min_width:I = 0x7f0c04e0

.field public static final inline_action_button_image_margin:I = 0x7f0c04e2

.field public static final inline_action_button_min_width:I = 0x7f0c04df

.field public static final inline_action_button_overflow_min_width:I = 0x7f0c04e1

.field public static final instagram_logo_padding:I = 0x7f0c04d4

.field public static final iorg_dialog_corner_radius:I = 0x7f0c074d

.field public static final iorg_dialog_horizontal_padding:I = 0x7f0c074e

.field public static final iorg_dialog_outside_padding:I = 0x7f0c0750

.field public static final iorg_header_text_size:I = 0x7f0c074b

.field public static final iorg_list_row_divider_height:I = 0x7f0c074a

.field public static final iorg_menu_divider_thickness:I = 0x7f0c0749

.field public static final iorg_menu_drawable_padding:I = 0x7f0c0741

.field public static final iorg_menu_horizontal_divider_spacing:I = 0x7f0c0744

.field public static final iorg_menu_horizontal_row_padding:I = 0x7f0c0743

.field public static final iorg_menu_radius:I = 0x7f0c0746

.field public static final iorg_menu_row_height:I = 0x7f0c0740

.field public static final iorg_menu_text_size:I = 0x7f0c0742

.field public static final iorg_menu_vertical_divider_spacing:I = 0x7f0c0745

.field public static final iorg_nux_dialog_horizontal_padding:I = 0x7f0c074f

.field public static final iorg_services_list_horizontal_margin:I = 0x7f0c0748

.field public static final iorg_services_list_icon_size:I = 0x7f0c0747

.field public static final isr_stars_min_height:I = 0x7f0c05c9

.field public static final itunes_content_icon:I = 0x7f0c0646

.field public static final itunes_item_padding:I = 0x7f0c0642

.field public static final itunes_item_padding_and_a_half:I = 0x7f0c0643

.field public static final itunes_item_padding_double:I = 0x7f0c0644

.field public static final itunes_item_padding_half:I = 0x7f0c0641

.field public static final itunes_section_divider_height:I = 0x7f0c0645

.field public static final itunes_section_header_height:I = 0x7f0c0640

.field public static final jewel_arrow_width:I = 0x7f0c06c2

.field public static final jewel_avatar_width:I = 0x7f0c00e2

.field public static final jewel_button_count_right_margin_angora:I = 0x7f0c06c4

.field public static final jewel_button_count_top_margin_angora:I = 0x7f0c06c3

.field public static final jewel_divebar_upsell_height:I = 0x7f0c06c1

.field public static final jewel_divider_height:I = 0x7f0c06c5

.field public static final jewel_messenger_promotion_height:I = 0x7f0c06c0

.field public static final jewel_row_padding:I = 0x7f0c00e3

.field public static final keyword_search_central_entity_action_image_padding:I = 0x7f0c06a3

.field public static final keyword_search_central_entity_left_right_padding:I = 0x7f0c06a1

.field public static final keyword_search_central_entity_photo_height:I = 0x7f0c06a0

.field public static final keyword_search_central_entity_top_padding:I = 0x7f0c06a2

.field public static final keyword_search_photos_module_height:I = 0x7f0c06a5

.field public static final keyword_search_title_drawable_padding:I = 0x7f0c06a4

.field public static final large_contributors_image_size:I = 0x7f0c0757

.field public static final left_error_msg_padding:I = 0x7f0c04c4

.field public static final list_view_ego_feed_unit_image_size:I = 0x7f0c0240

.field public static final list_view_ego_unit_padding:I = 0x7f0c024d

.field public static final list_view_ego_unit_topbottom_margins:I = 0x7f0c024c

.field public static final loading_indicator_error_view_horizontal_padding:I = 0x7f0c013f

.field public static final loading_indicator_error_view_vertical_padding:I = 0x7f0c013e

.field public static final loading_indicator_spinner_size:I = 0x7f0c013c

.field public static final loading_indicator_spinner_vertical_padding:I = 0x7f0c013d

.field public static final location_ping_dialog_item_padding:I = 0x7f0c04d7

.field public static final lock_card_fling_velocity_threshold_pps:I = 0x7f0c00dd

.field public static final lockscreen_settings_inner_padding:I = 0x7f0c03f4

.field public static final lockscreen_settings_padding_vertical:I = 0x7f0c03f5

.field public static final lockscreen_switch_min_width:I = 0x7f0c0401

.field public static final lockscreen_switch_padding:I = 0x7f0c0403

.field public static final lockscreen_thumb_text_padding:I = 0x7f0c0402

.field public static final map_overlay_bubble_offset:I = 0x7f0c00f9

.field public static final map_overlay_padding:I = 0x7f0c00f8

.field public static final megaphone_button_height:I = 0x7f0c043d

.field public static final megaphone_button_horiz_padding:I = 0x7f0c043c

.field public static final megaphone_default_spacing:I = 0x7f0c0439

.field public static final megaphone_default_text_size:I = 0x7f0c043b

.field public static final megaphone_facepile_container_margin:I = 0x7f0c0441

.field public static final megaphone_facepile_container_top_margin:I = 0x7f0c0442

.field public static final megaphone_facepile_height:I = 0x7f0c043f

.field public static final megaphone_facepile_left_margin:I = 0x7f0c0443

.field public static final megaphone_facepile_margin_width:I = 0x7f0c0440

.field public static final megaphone_facepile_width:I = 0x7f0c043e

.field public static final megaphone_right_spacing:I = 0x7f0c043a

.field public static final members_bar_extra_pog_h_padding:I = 0x7f0c079c

.field public static final members_bar_extra_pog_inner_text_padding:I = 0x7f0c079a

.field public static final members_bar_extra_pog_text_size:I = 0x7f0c079b

.field public static final members_bar_inner_padding:I = 0x7f0c0799

.field public static final members_bar_member_circle_diam:I = 0x7f0c0798

.field public static final members_divider_height:I = 0x7f0c079d

.field public static final menu_divider_size:I = 0x7f0c076b

.field public static final menu_height:I = 0x7f0c076c

.field public static final menu_text_size:I = 0x7f0c076d

.field public static final message_font_size:I = 0x7f0c01e9

.field public static final message_font_size_classic:I = 0x7f0c01ea

.field public static final minutiae_icon_picker_header_icon_padding:I = 0x7f0c03ae

.field public static final minutiae_icon_picker_header_icon_size:I = 0x7f0c03ad

.field public static final minutiae_icon_picker_image_padding:I = 0x7f0c03b0

.field public static final minutiae_icon_picker_image_size:I = 0x7f0c03af

.field public static final minutiae_picker_glyph_left_margin:I = 0x7f0c03b1

.field public static final minutiae_ridge_button_touch_area_extension_length:I = 0x7f0c03b4

.field public static final minutiae_ridge_nux_offset:I = 0x7f0c03b5

.field public static final minutiae_ridge_nux_result_height:I = 0x7f0c03b6

.field public static final minutiae_ridge_refreshable_header_height:I = 0x7f0c03b2

.field public static final minutiae_search_progress_indicator_size:I = 0x7f0c03b3

.field public static final mobile_zero_upsell_feed_unit_image_size:I = 0x7f0c024f

.field public static final multi_share_attachment_end_item_image_size:I = 0x7f0c02c2

.field public static final multi_share_attachment_hscroller_height:I = 0x7f0c02c3

.field public static final multi_share_attachment_item_border_thickness:I = 0x7f0c02c0

.field public static final multi_share_attachment_item_bottom_section_height:I = 0x7f0c02c1

.field public static final multi_share_attachment_item_image_size:I = 0x7f0c02be

.field public static final multi_share_attachment_item_left_right_margin:I = 0x7f0c02bf

.field public static final multi_share_attachment_top_bottom_margin:I = 0x7f0c02bd

.field public static final multipicker_grid_size:I = 0x7f0c0376

.field public static final multipicker_spacing:I = 0x7f0c0377

.field public static final music_notification_app_icon_padding:I = 0x7f0c04b9

.field public static final music_notification_app_icon_size:I = 0x7f0c04b8

.field public static final music_notification_app_label_text_size:I = 0x7f0c04b5

.field public static final music_notification_buffering_spinner_size:I = 0x7f0c04ba

.field public static final music_notification_controls_height:I = 0x7f0c04b7

.field public static final music_notification_front_text_padding:I = 0x7f0c04b2

.field public static final music_notification_height:I = 0x7f0c04b6

.field public static final music_notification_panel_padding:I = 0x7f0c04b3

.field public static final music_notification_text_size:I = 0x7f0c04b4

.field public static final music_preview_card_provider_action_padding:I = 0x7f0c0596

.field public static final music_preview_card_provider_logo_right_margin:I = 0x7f0c0598

.field public static final music_preview_card_provider_logo_size:I = 0x7f0c0597

.field public static final music_preview_card_square_footer_padding:I = 0x7f0c0593

.field public static final music_preview_card_wide_text_padding_horizontal:I = 0x7f0c0594

.field public static final music_preview_card_wide_text_padding_vertical:I = 0x7f0c0595

.field public static final music_preview_pager_margin:I = 0x7f0c0599

.field public static final nag_layout_padding:I = 0x7f0c00dc

.field public static final navigation_footer_height:I = 0x7f0c0025

.field public static final navigation_popup_menu_row_height:I = 0x7f0c0771

.field public static final navigation_row_height:I = 0x7f0c0026

.field public static final navigation_row_height_with_description:I = 0x7f0c0027

.field public static final navigation_title_bar_height:I = 0x7f0c0024

.field public static final nearby_category_search_bar_bottom_shadow_height:I = 0x7f0c0671

.field public static final nearby_category_search_bar_height:I = 0x7f0c0670

.field public static final nearby_facepile_size:I = 0x7f0c066e

.field public static final nearby_list_hide_threshold:I = 0x7f0c0673

.field public static final nearby_list_start_height:I = 0x7f0c0672

.field public static final nearby_map_button_size:I = 0x7f0c066f

.field public static final nearby_place_image_size:I = 0x7f0c066d

.field public static final needle_search_filter_divider_height:I = 0x7f0c0694

.field public static final needle_search_filter_image_border:I = 0x7f0c069d

.field public static final needle_search_filter_left_right_padding:I = 0x7f0c068f

.field public static final needle_search_filter_loading_spinner_height:I = 0x7f0c0698

.field public static final needle_search_filter_name_divider_height:I = 0x7f0c069c

.field public static final needle_search_filter_name_filter_icon_padding:I = 0x7f0c0699

.field public static final needle_search_filter_name_filter_left_right_bottom_margin:I = 0x7f0c069b

.field public static final needle_search_filter_name_filter_left_right_margin:I = 0x7f0c069a

.field public static final needle_search_filter_remove_row_height:I = 0x7f0c0693

.field public static final needle_search_filter_tag_compound_padding:I = 0x7f0c0696

.field public static final needle_search_filter_tag_container_padding:I = 0x7f0c0695

.field public static final needle_search_filter_tag_horizontal_vertical_spacing:I = 0x7f0c0697

.field public static final needle_search_filter_title_bottom_margin:I = 0x7f0c0691

.field public static final needle_search_filter_title_row_height:I = 0x7f0c0692

.field public static final needle_search_filter_title_top_margin:I = 0x7f0c0690

.field public static final needle_search_null_state_image_height:I = 0x7f0c068d

.field public static final needle_search_null_state_text_padding:I = 0x7f0c068c

.field public static final needle_search_null_state_top_bottom_padding:I = 0x7f0c068e

.field public static final neue_login_button_text_size:I = 0x7f0c0012

.field public static final neue_login_desc_text_size:I = 0x7f0c000f

.field public static final neue_login_desc_text_size_small:I = 0x7f0c0010

.field public static final neue_login_field_text_size:I = 0x7f0c0011

.field public static final neue_login_secondary_button_text_size:I = 0x7f0c0013

.field public static final neue_login_title_text_size:I = 0x7f0c000d

.field public static final neue_login_title_text_size_small:I = 0x7f0c000e

.field public static final neue_thread_tile_size:I = 0x7f0c01e8

.field public static final new_event_selector_button_height:I = 0x7f0c06f2

.field public static final new_event_selector_button_margin:I = 0x7f0c06f1

.field public static final new_story_pill_scrollaway_height:I = 0x7f0c02cf

.field public static final next_button_height:I = 0x7f0c0610

.field public static final no_photo_text_size:I = 0x7f0c0386

.field public static final notification_avatar_width:I = 0x7f0c06bf

.field public static final notification_chat_head_size:I = 0x7f0c04a1

.field public static final notification_corner_radius:I = 0x7f0c04ae

.field public static final notification_dismiss_button_size:I = 0x7f0c04d3

.field public static final notification_front_view_bottom_padding:I = 0x7f0c04b1

.field public static final notification_front_view_left_padding:I = 0x7f0c04af

.field public static final notification_front_view_right_padding:I = 0x7f0c04b0

.field public static final notification_list_edge_padding:I = 0x7f0c04a8

.field public static final notification_message_bottom_padding:I = 0x7f0c04ab

.field public static final notification_message_left_padding:I = 0x7f0c04ad

.field public static final notification_message_min_height:I = 0x7f0c04a9

.field public static final notification_message_right_padding:I = 0x7f0c04ac

.field public static final notification_message_top_padding:I = 0x7f0c04aa

.field public static final notification_panel_margin_bottom:I = 0x7f0c04a5

.field public static final notification_profile_pic_height:I = 0x7f0c049f

.field public static final notification_profile_pic_width:I = 0x7f0c049e

.field public static final notification_text_size:I = 0x7f0c04a0

.field public static final notification_thread_tile_corner_radius:I = 0x7f0c04a3

.field public static final notification_thread_tile_divider:I = 0x7f0c04a4

.field public static final notification_thread_tile_size:I = 0x7f0c04a2

.field public static final notification_thumbnail_height:I = 0x7f0c04a7

.field public static final notification_thumbnail_width:I = 0x7f0c04a6

.field public static final notification_upsell_icon_padding:I = 0x7f0c04d2

.field public static final notification_view_top_padding:I = 0x7f0c04d1

.field public static final notifications_container_padding_bottom:I = 0x7f0c049c

.field public static final notifications_container_padding_top:I = 0x7f0c049b

.field public static final notifications_list_padding_bottom:I = 0x7f0c049d

.field public static final nub_vertical_offset:I = 0x7f0c0186

.field public static final nux_bubble_top_margin:I = 0x7f0c00fa

.field public static final nux_clue_view_overshoot_threshold:I = 0x7f0c00ac

.field public static final one_dp:I = 0x7f0c0002

.field public static final one_px:I = 0x7f0c0001

.field public static final open_menu_background_margin:I = 0x7f0c06c6

.field public static final open_menu_side_margin:I = 0x7f0c06c7

.field public static final orca_audio_composer_record_button_size:I = 0x7f0c01ae

.field public static final orca_audio_composer_record_button_size_shrunken:I = 0x7f0c01af

.field public static final orca_audio_composer_record_button_text_size:I = 0x7f0c01ad

.field public static final orca_audio_composer_text_size:I = 0x7f0c01ac

.field public static final orca_compose_attachment_item_height:I = 0x7f0c01aa

.field public static final orca_compose_attachment_item_image_width_height:I = 0x7f0c01ab

.field public static final orca_compose_attachment_item_width:I = 0x7f0c01a9

.field public static final orca_compose_force_2line_min_height:I = 0x7f0c015d

.field public static final orca_compose_min_height_for_two_lines:I = 0x7f0c015e

.field public static final orca_compose_min_text_input_container_height:I = 0x7f0c0162

.field public static final orca_compose_single_line_location_nux_nub_right:I = 0x7f0c0164

.field public static final orca_compose_two_line_adapted_location_width:I = 0x7f0c015f

.field public static final orca_compose_two_line_normal_location_width:I = 0x7f0c0160

.field public static final orca_composer_height_to_force_text_visible:I = 0x7f0c015c

.field public static final orca_contact_upload_view_top_padding:I = 0x7f0c01b0

.field public static final orca_group_image_history_image_size:I = 0x7f0c01b3

.field public static final orca_location_nux_nub_bottom:I = 0x7f0c0163

.field public static final orca_media_tray_divider_width:I = 0x7f0c0161

.field public static final orca_message_bubble_mask_stroke_width:I = 0x7f0c01d9

.field public static final orca_message_bubble_min_height:I = 0x7f0c01d8

.field public static final orca_message_bubble_padding_horizontal:I = 0x7f0c01d7

.field public static final orca_message_bubble_padding_vertical:I = 0x7f0c01d6

.field public static final orca_message_bubble_round_radius:I = 0x7f0c01d4

.field public static final orca_message_bubble_square_radius:I = 0x7f0c01d5

.field public static final orca_message_inline_video_player_max_size:I = 0x7f0c0156

.field public static final orca_message_share_button_padding:I = 0x7f0c0157

.field public static final orca_message_share_button_right_padding_me_user:I = 0x7f0c0158

.field public static final orca_message_text_size:I = 0x7f0c014f

.field public static final orca_message_text_size_neue:I = 0x7f0c0150

.field public static final orca_panel_bg_margin:I = 0x7f0c0003

.field public static final orca_pull_to_refresh_spinner_dimen:I = 0x7f0c01d1

.field public static final orca_pull_to_refresh_view_height:I = 0x7f0c01d0

.field public static final orca_quick_cam_pop_out_offset_y:I = 0x7f0c01d2

.field public static final orca_quick_cam_progress_bar_height:I = 0x7f0c01d3

.field public static final orca_title_left_right_padding:I = 0x7f0c015a

.field public static final order_overview_avatar_dimensions:I = 0x7f0c062d

.field public static final order_overview_card_icon_width:I = 0x7f0c0631

.field public static final order_overview_product_image_height:I = 0x7f0c0630

.field public static final order_overview_product_image_width_full:I = 0x7f0c062f

.field public static final order_overview_product_image_width_thumb:I = 0x7f0c062e

.field public static final order_review_cell_subinfo_text_size:I = 0x7f0c064d

.field public static final order_review_cell_title_text_size:I = 0x7f0c064c

.field public static final order_review_gift_details_image_size:I = 0x7f0c064b

.field public static final order_review_terms_text_size:I = 0x7f0c064e

.field public static final overlay_divider_text_size:I = 0x7f0c076e

.field public static final padding_bottom:I = 0x7f0c06bc

.field public static final padding_left_dd:I = 0x7f0c06bd

.field public static final padding_left_sd:I = 0x7f0c03bb

.field public static final page_activity_card_padding_large:I = 0x7f0c0578

.field public static final page_activity_text_large:I = 0x7f0c0540

.field public static final page_activity_uni_button_margin:I = 0x7f0c053f

.field public static final page_activity_uni_status_icon_margin:I = 0x7f0c057b

.field public static final page_identity_action_sheet_bottom_border_height:I = 0x7f0c0571

.field public static final page_identity_action_sheet_bottom_margin:I = 0x7f0c0570

.field public static final page_identity_action_sheet_height:I = 0x7f0c0572

.field public static final page_identity_admin_switcher_height:I = 0x7f0c0559

.field public static final page_identity_attributions_icon_size:I = 0x7f0c055a

.field public static final page_identity_bar_chart_margin:I = 0x7f0c0579

.field public static final page_identity_button_height:I = 0x7f0c055d

.field public static final page_identity_card_content_padding:I = 0x7f0c0542

.field public static final page_identity_card_margin:I = 0x7f0c0541

.field public static final page_identity_card_vertical_margin:I = 0x7f0c0548

.field public static final page_identity_carousel_item_text_padding:I = 0x7f0c0549

.field public static final page_identity_cover_padding:I = 0x7f0c054a

.field public static final page_identity_cover_padding_refresh:I = 0x7f0c054d

.field public static final page_identity_cover_photo_height:I = 0x7f0c054c

.field public static final page_identity_custom_image_button_top_padding:I = 0x7f0c056f

.field public static final page_identity_event_bylines_icon_size:I = 0x7f0c0557

.field public static final page_identity_event_cover_photo_height:I = 0x7f0c0558

.field public static final page_identity_event_join_button_width:I = 0x7f0c0560

.field public static final page_identity_events_text_minimum_height:I = 0x7f0c0546

.field public static final page_identity_events_vertical_padding:I = 0x7f0c055f

.field public static final page_identity_expandable_text_gradient_default_length:I = 0x7f0c0555

.field public static final page_identity_feed_story_footer_padding:I = 0x7f0c0554

.field public static final page_identity_feedback_text_size:I = 0x7f0c0577

.field public static final page_identity_header_about_text_size:I = 0x7f0c056e

.field public static final page_identity_header_category_text_size:I = 0x7f0c056d

.field public static final page_identity_header_count_chevron_padding:I = 0x7f0c0545

.field public static final page_identity_header_name_small_text_size:I = 0x7f0c056c

.field public static final page_identity_header_name_text_size:I = 0x7f0c056b

.field public static final page_identity_link_badge_right_padding:I = 0x7f0c0575

.field public static final page_identity_link_text_left_padding:I = 0x7f0c0574

.field public static final page_identity_loading_spinner_size:I = 0x7f0c055c

.field public static final page_identity_local_card_divider_margin:I = 0x7f0c0544

.field public static final page_identity_map_view_height:I = 0x7f0c055b

.field public static final page_identity_mini_facepile_profile_pic_size:I = 0x7f0c0552

.field public static final page_identity_photo_gallery_size:I = 0x7f0c056a

.field public static final page_identity_photos_by_friends_tile_size:I = 0x7f0c0553

.field public static final page_identity_popular_menu_item_padding:I = 0x7f0c055e

.field public static final page_identity_product_pic_size:I = 0x7f0c0551

.field public static final page_identity_profile_pic_bottom_margin:I = 0x7f0c054f

.field public static final page_identity_profile_pic_left_margin:I = 0x7f0c054e

.field public static final page_identity_profile_pic_size:I = 0x7f0c054b

.field public static final page_identity_ratings_summary_margin:I = 0x7f0c057a

.field public static final page_identity_review_feedback_buttons_touch_margin:I = 0x7f0c0563

.field public static final page_identity_review_photo_height:I = 0x7f0c0562

.field public static final page_identity_review_photo_width:I = 0x7f0c0561

.field public static final page_identity_review_secondary_action_touch_margin:I = 0x7f0c0564

.field public static final page_identity_save_place_nux_margin_top:I = 0x7f0c0566

.field public static final page_identity_save_place_nux_translation_y:I = 0x7f0c0567

.field public static final page_identity_save_place_nux_width:I = 0x7f0c0568

.field public static final page_identity_segmented_button_corner_radius:I = 0x7f0c0565

.field public static final page_identity_similar_pages_unit_width:I = 0x7f0c0569

.field public static final page_identity_structured_content_left_icon_width:I = 0x7f0c0543

.field public static final page_identity_structured_content_row_height:I = 0x7f0c0573

.field public static final page_identity_timeline_footer_margin:I = 0x7f0c0556

.field public static final page_identity_vertical_separator_width:I = 0x7f0c0550

.field public static final page_identity_video_card_ufi_padding:I = 0x7f0c0576

.field public static final page_indicator_icon_size:I = 0x7f0c0479

.field public static final page_indicator_spacer_width:I = 0x7f0c047a

.field public static final page_margin:I = 0x7f0c0017

.field public static final page_review_survey_feed_unit_cover_photo_size:I = 0x7f0c022d

.field public static final page_review_survey_feed_unit_image_size:I = 0x7f0c022c

.field public static final page_review_survey_feed_unit_item_height:I = 0x7f0c025a

.field public static final page_ui_user_pic_size:I = 0x7f0c053e

.field public static final pager_frame_layout_height_landscape:I = 0x7f0c02fc

.field public static final pager_frame_layout_height_portrait:I = 0x7f0c02fb

.field public static final pager_margin_landscape:I = 0x7f0c0301

.field public static final pager_spacing_landscape:I = 0x7f0c0300

.field public static final pager_spacing_portrait:I = 0x7f0c02ff

.field public static final pager_width_landscape:I = 0x7f0c02fd

.field public static final pager_width_portrait:I = 0x7f0c02fe

.field public static final pages_browser_item_like_icon_size:I = 0x7f0c075e

.field public static final pages_browser_item_profile_pic_size:I = 0x7f0c075c

.field public static final pages_browser_list_item_profile_pic_size:I = 0x7f0c075d

.field public static final pages_browser_section_header_min_height:I = 0x7f0c075b

.field public static final pages_browser_section_inside_horizontal_margin:I = 0x7f0c075a

.field public static final pages_browser_section_vertical_margin:I = 0x7f0c0759

.field public static final pages_indicator_bottom_margin:I = 0x7f0c049a

.field public static final pages_indicator_height:I = 0x7f0c0478

.field public static final pandora_album_name_margin:I = 0x7f0c0374

.field public static final pandora_album_spacing:I = 0x7f0c0373

.field public static final pandora_benny_banner_no_photos:I = 0x7f0c0769

.field public static final pandora_expanded_details_max_height:I = 0x7f0c0764

.field public static final pandora_feed_row_margin:I = 0x7f0c075f

.field public static final pandora_list_padding:I = 0x7f0c0762

.field public static final pandora_profile_pic_width:I = 0x7f0c0761

.field public static final pandora_story_delimiter:I = 0x7f0c0763

.field public static final pandora_thumbnail_margin:I = 0x7f0c0766

.field public static final pandora_thumbnail_spacing_size:I = 0x7f0c0760

.field public static final pandora_title_height:I = 0x7f0c0768

.field public static final pandora_title_padding:I = 0x7f0c0767

.field public static final pandora_title_size:I = 0x7f0c0765

.field public static final permalink_add_reply_horizontal_padding:I = 0x7f0c0270

.field public static final permalink_add_reply_vertical_padding:I = 0x7f0c0271

.field public static final permalink_comment_edit_padding:I = 0x7f0c026f

.field public static final permalink_comments_metadata_separator_padding:I = 0x7f0c026e

.field public static final permalink_comments_metadata_separator_top:I = 0x7f0c026d

.field public static final permalink_comments_padding_bottom:I = 0x7f0c026c

.field public static final permalink_comments_padding_top:I = 0x7f0c026b

.field public static final permalink_contents_margin:I = 0x7f0c0253

.field public static final permalink_top_ptr_margin:I = 0x7f0c0272

.field public static final person_card_actionbar_height:I = 0x7f0c0721

.field public static final person_card_context_item_image_margin_horizontal:I = 0x7f0c0724

.field public static final person_card_context_item_image_margin_vertical:I = 0x7f0c0725

.field public static final person_card_context_item_image_size:I = 0x7f0c0723

.field public static final person_card_context_item_label_container_left_margin:I = 0x7f0c0726

.field public static final person_card_context_item_label_container_margin_vertical:I = 0x7f0c0727

.field public static final person_card_contextual_items_error_text_size:I = 0x7f0c0729

.field public static final person_card_footer_font_size:I = 0x7f0c0728

.field public static final person_card_info_row_height:I = 0x7f0c0722

.field public static final person_card_name_height:I = 0x7f0c071d

.field public static final person_card_name_margin_bottom:I = 0x7f0c071b

.field public static final person_card_name_margin_left:I = 0x7f0c071e

.field public static final person_card_name_margin_right:I = 0x7f0c071c

.field public static final person_card_name_size_large:I = 0x7f0c071f

.field public static final person_card_name_size_small:I = 0x7f0c0720

.field public static final person_card_profile_container_height:I = 0x7f0c0714

.field public static final person_card_profile_container_margin_bottom:I = 0x7f0c0718

.field public static final person_card_profile_container_margin_left:I = 0x7f0c0719

.field public static final person_card_profile_container_width:I = 0x7f0c0713

.field public static final person_card_profile_pic_padding:I = 0x7f0c0716

.field public static final person_card_profile_pic_padding_bottom:I = 0x7f0c0717

.field public static final person_card_profile_pic_size:I = 0x7f0c0715

.field public static final person_card_verified_badge_margin:I = 0x7f0c071a

.field public static final person_icon_inset:I = 0x7f0c0430

.field public static final photo_chaining_item_first_left_margin:I = 0x7f0c0204

.field public static final photo_chaining_item_image_size:I = 0x7f0c0203

.field public static final photo_chaining_item_padding:I = 0x7f0c0205

.field public static final photo_chaining_item_spacing:I = 0x7f0c0206

.field public static final photo_height:I = 0x7f0c04bf

.field public static final photo_preview_thumb:I = 0x7f0c042f

.field public static final photo_preview_thumb_upload:I = 0x7f0c06b8

.field public static final photo_size:I = 0x7f0c0359

.field public static final photo_spacing_size:I = 0x7f0c035a

.field public static final photo_width:I = 0x7f0c04c0

.field public static final photoset_thumbnail_size:I = 0x7f0c0753

.field public static final pin_padding:I = 0x7f0c0444

.field public static final pinned_groups_add_attachment_circle_image_size:I = 0x7f0c01ca

.field public static final pinned_groups_add_attachment_circle_size:I = 0x7f0c01c9

.field public static final pinned_groups_contacts_list_margin_top:I = 0x7f0c01cd

.field public static final pinned_groups_container_margin:I = 0x7f0c01cc

.field public static final pinned_groups_continue_button_circle_size:I = 0x7f0c01c7

.field public static final pinned_groups_continue_button_height:I = 0x7f0c01c5

.field public static final pinned_groups_continue_button_margin_bottom:I = 0x7f0c01c6

.field public static final pinned_groups_continue_button_width:I = 0x7f0c01c4

.field public static final pinned_groups_create_button_height:I = 0x7f0c01cb

.field public static final pinned_groups_input_text_size:I = 0x7f0c01c8

.field public static final pinned_groups_subtitle_text_size:I = 0x7f0c01c3

.field public static final pinned_groups_title_text_size:I = 0x7f0c01c2

.field public static final pivot_card_image_set_height:I = 0x7f0c05ca

.field public static final pivot_card_text_padding:I = 0x7f0c05cc

.field public static final pivot_card_text_size:I = 0x7f0c05cd

.field public static final pivot_card_text_view_height:I = 0x7f0c05cb

.field public static final pivot_pager_dots_diameter:I = 0x7f0c02dc

.field public static final pivot_pager_dots_spacing:I = 0x7f0c02dd

.field public static final pivot_title_line_min_width:I = 0x7f0c02db

.field public static final pivot_title_padding:I = 0x7f0c02da

.field public static final place_image_size:I = 0x7f0c05b0

.field public static final plutonium_action_bar_with_divider_height:I = 0x7f0c0502

.field public static final plutonium_carousel_arrow_padding:I = 0x7f0c04ff

.field public static final plutonium_context_item_badge_horizontal_padding:I = 0x7f0c04e9

.field public static final plutonium_context_item_badge_right_margin:I = 0x7f0c04ea

.field public static final plutonium_context_item_badge_text_size:I = 0x7f0c04e7

.field public static final plutonium_context_item_badge_vertical_padding:I = 0x7f0c04e8

.field public static final plutonium_context_item_image_size:I = 0x7f0c04e6

.field public static final plutonium_context_item_info_left_margin:I = 0x7f0c04e5

.field public static final plutonium_context_item_padding_horizontal:I = 0x7f0c04e3

.field public static final plutonium_context_item_padding_vertical:I = 0x7f0c04e4

.field public static final plutonium_friend_request_content_padding:I = 0x7f0c0503

.field public static final plutonium_friend_request_reply_button_height:I = 0x7f0c0504

.field public static final plutonium_friend_request_reply_button_right_margin:I = 0x7f0c0505

.field public static final plutonium_name_size_large:I = 0x7f0c04eb

.field public static final plutonium_name_size_small:I = 0x7f0c04ec

.field public static final plutonium_pp_tip_min_cover_height:I = 0x7f0c04fa

.field public static final plutonium_profile_photo_tip_left_margin:I = 0x7f0c04f9

.field public static final plutonium_timeline_covermedia_pivot_label_vertical_padding:I = 0x7f0c04fe

.field public static final plutonium_timeline_covermedia_pivot_padding_label_horizontal:I = 0x7f0c04fc

.field public static final plutonium_timeline_covermedia_pivot_separator_horizontal:I = 0x7f0c04fd

.field public static final plutonium_timeline_covermedia_vignette_height:I = 0x7f0c04fb

.field public static final plutonium_timeline_friend_request_progress_size:I = 0x7f0c0506

.field public static final plutonium_timeline_more_context_items_size:I = 0x7f0c0507

.field public static final plutonium_timeline_more_context_items_size_with_padding:I = 0x7f0c0508

.field public static final plutonium_timeline_navitem_bottom_padding:I = 0x7f0c0517

.field public static final plutonium_timeline_navitem_text_height:I = 0x7f0c0515

.field public static final plutonium_timeline_navitem_tile_padding:I = 0x7f0c0514

.field public static final plutonium_timeline_navitem_top_padding:I = 0x7f0c0516

.field public static final plutonium_timeline_navtile_margin_max:I = 0x7f0c0519

.field public static final plutonium_timeline_navtile_margin_min:I = 0x7f0c0518

.field public static final plutonium_timeline_navtile_width:I = 0x7f0c0513

.field public static final plutonium_timeline_navtiles_padding:I = 0x7f0c051a

.field public static final plutonium_timeline_profile_image_size:I = 0x7f0c050c

.field public static final plutonium_timeline_profile_pic_padding:I = 0x7f0c04f7

.field public static final plutonium_timeline_profile_pic_padding_bottom:I = 0x7f0c04f8

.field public static final plutonium_timeline_profile_pic_size_with_padding:I = 0x7f0c04f6

.field public static final poke_travel_distance:I = 0x7f0c04c8

.field public static final popover_arrow_gap:I = 0x7f0c0662

.field public static final popover_outter_gap_bottom:I = 0x7f0c0661

.field public static final popover_outter_gap_top:I = 0x7f0c0660

.field public static final popup_subview_margin:I = 0x7f0c06be

.field public static final post_play_suggestions_video_height:I = 0x7f0c0133

.field public static final post_post_face_crop_size:I = 0x7f0c0306

.field public static final post_post_minimum_height:I = 0x7f0c02f8

.field public static final post_post_minimum_width:I = 0x7f0c02f9

.field public static final post_post_photo_padding:I = 0x7f0c0307

.field public static final post_post_tag_typeahead_width:I = 0x7f0c02fa

.field public static final premium_video_btn_margin:I = 0x7f0c0244

.field public static final premium_video_clickable_area_height:I = 0x7f0c0246

.field public static final premium_video_clickable_area_width:I = 0x7f0c0245

.field public static final premium_videos_bling_bar_left_right_padding:I = 0x7f0c0249

.field public static final premium_videos_padding:I = 0x7f0c0247

.field public static final premium_videos_play_button_size:I = 0x7f0c0248

.field public static final presence_feed_unit_image_size:I = 0x7f0c024e

.field public static final preview_image_default_size:I = 0x7f0c01a5

.field public static final preview_image_hot_like_sticker_medium:I = 0x7f0c01a7

.field public static final preview_image_hot_like_sticker_small:I = 0x7f0c01a6

.field public static final price_tag_corner_negative_left_margin:I = 0x7f0c0664

.field public static final privacy_icon_height:I = 0x7f0c0472

.field public static final privacy_icon_width:I = 0x7f0c0471

.field public static final privacy_option_text_size:I = 0x7f0c035b

.field public static final product_details_itunes_recommended_icon:I = 0x7f0c062a

.field public static final product_details_price_tag_negative_left_margin:I = 0x7f0c0663

.field public static final product_details_recommended_gift_icon:I = 0x7f0c062b

.field public static final product_details_shrunk_description_height:I = 0x7f0c062c

.field public static final product_sku_let_them_choose_icon_dimens:I = 0x7f0c0632

.field public static final production_gallery_footer_height:I = 0x7f0c02f0

.field public static final production_gallery_photo_offset:I = 0x7f0c02f1

.field public static final products_arrow_corner_bottom_padding:I = 0x7f0c0623

.field public static final products_arrow_corner_left_padding:I = 0x7f0c0624

.field public static final products_cell_left_gap:I = 0x7f0c0628

.field public static final products_filter_check_width:I = 0x7f0c0627

.field public static final products_image_border_width:I = 0x7f0c0629

.field public static final products_recipient_avatar_height:I = 0x7f0c0626

.field public static final products_recipient_avatar_width:I = 0x7f0c0625

.field public static final profile_image_size:I = 0x7f0c05dd

.field public static final profile_info_request_button_height:I = 0x7f0c072b

.field public static final profile_info_request_padding:I = 0x7f0c072a

.field public static final profile_info_request_progress_bar_height:I = 0x7f0c072c

.field public static final profile_list_divider_height:I = 0x7f0c0264

.field public static final profile_pic_bottom_margin:I = 0x7f0c0449

.field public static final profile_pic_icon_height:I = 0x7f0c044b

.field public static final profile_pic_icon_width:I = 0x7f0c044a

.field public static final profile_pic_right_margin:I = 0x7f0c0448

.field public static final profile_pic_swipe_feed_unit_image_size:I = 0x7f0c023e

.field public static final profile_pic_swipe_feed_unit_width:I = 0x7f0c023f

.field public static final ptr_list_gradient_height:I = 0x7f0c0142

.field public static final ptr_stub_view_height_margin:I = 0x7f0c06bb

.field public static final publish_mode_options_divider:I = 0x7f0c03da

.field public static final publish_mode_options_horizontal_padding:I = 0x7f0c03db

.field public static final publish_mode_options_text_size:I = 0x7f0c03dc

.field public static final publish_mode_options_vertical_padding:I = 0x7f0c03dd

.field public static final publish_mode_selector_nux_bubble_bottom_margin:I = 0x7f0c03c9

.field public static final publisher_height:I = 0x7f0c0139

.field public static final publisher_height_of_page:I = 0x7f0c01a8

.field public static final publisher_height_with_offset:I = 0x7f0c013a

.field public static final publisher_text_padding_left:I = 0x7f0c021d

.field public static final pull_to_refresh_distance:I = 0x7f0c06b9

.field public static final pull_to_refresh_threshold:I = 0x7f0c06ba

.field public static final push_notification_banner_default_padding:I = 0x7f0c03f7

.field public static final push_notification_banner_topdown_padding:I = 0x7f0c03f6

.field public static final push_notification_bounce_offset:I = 0x7f0c03f9

.field public static final push_notification_corner_radius:I = 0x7f0c03ee

.field public static final push_notification_drawable_padding:I = 0x7f0c03f3

.field public static final push_notification_gray_bar_height:I = 0x7f0c03ef

.field public static final push_notification_inner_padding:I = 0x7f0c03ed

.field public static final push_notification_inner_text_horizontal_padding:I = 0x7f0c03f1

.field public static final push_notification_inner_text_vertical_padding:I = 0x7f0c03f2

.field public static final push_notification_item_divider_height:I = 0x7f0c03ec

.field public static final push_notification_item_height:I = 0x7f0c03eb

.field public static final push_notification_opt_in_button_height:I = 0x7f0c03f0

.field public static final push_notification_preview_item_height:I = 0x7f0c03ea

.field public static final push_notification_x_to_close_padding:I = 0x7f0c03f8

.field public static final push_notifications_title_height:I = 0x7f0c03e9

.field public static final pymk_feed_unit_blacklist_icon_margin:I = 0x7f0c0584

.field public static final pymk_feed_unit_image_size:I = 0x7f0c022b

.field public static final pymk_feed_unit_name_background_height:I = 0x7f0c0583

.field public static final pymk_feed_unit_name_mutual_friends_background_height:I = 0x7f0c0581

.field public static final pymk_feed_unit_name_mutual_friends_background_padding_bottom:I = 0x7f0c0582

.field public static final pymk_swipe_feed_unit_bottom_section_size:I = 0x7f0c0239

.field public static final pymk_swipe_feed_unit_size:I = 0x7f0c0237

.field public static final pymk_view_pager_border_thickness:I = 0x7f0c0238

.field public static final pyml_creative_image_high:I = 0x7f0c02b4

.field public static final pyml_creative_image_low:I = 0x7f0c02b2

.field public static final pyml_creative_image_medium:I = 0x7f0c02b3

.field public static final pyml_feed_unit_image_size:I = 0x7f0c023d

.field public static final qp_dialog_card_image_max_height:I = 0x7f0c0464

.field public static final qp_divebar_medium_image_max_height:I = 0x7f0c0461

.field public static final qp_divebar_medium_image_max_width:I = 0x7f0c0460

.field public static final qp_interstitial_button_divider_height:I = 0x7f0c045c

.field public static final qp_interstitial_button_height:I = 0x7f0c045b

.field public static final qp_interstitial_card_margin:I = 0x7f0c0459

.field public static final qp_interstitial_image_height:I = 0x7f0c045d

.field public static final qp_interstitial_image_max_height:I = 0x7f0c0467

.field public static final qp_interstitial_image_max_width:I = 0x7f0c0468

.field public static final qp_interstitial_inside_margin:I = 0x7f0c0456

.field public static final qp_interstitial_inside_margin_large:I = 0x7f0c0457

.field public static final qp_interstitial_margin_bottom:I = 0x7f0c0454

.field public static final qp_interstitial_margin_horizontal:I = 0x7f0c0455

.field public static final qp_interstitial_margin_top:I = 0x7f0c0453

.field public static final qp_interstitial_social_context_padding_top:I = 0x7f0c045e

.field public static final qp_interstitial_title_margin:I = 0x7f0c0458

.field public static final qp_interstitial_x_button_padding:I = 0x7f0c045f

.field public static final qp_interstitial_x_button_top:I = 0x7f0c045a

.field public static final qp_megaphone_standard_image_max_height:I = 0x7f0c0463

.field public static final qp_megaphone_standard_image_max_width:I = 0x7f0c0462

.field public static final qp_messenger_dialog_card_image_max_width:I = 0x7f0c0465

.field public static final qp_messenger_dialog_card_no_badge_image_max_width:I = 0x7f0c0466

.field public static final qrcode_container_size:I = 0x7f0c076f

.field public static final qrcode_size:I = 0x7f0c0770

.field public static final query_list_divider_height:I = 0x7f0c04c2

.field public static final query_list_item_height:I = 0x7f0c04c1

.field public static final rating_container_height:I = 0x7f0c03de

.field public static final rating_default_vertical_padding:I = 0x7f0c03df

.field public static final rating_rating_bar_bottom_padding:I = 0x7f0c03e0

.field public static final rating_section_card_privacy_icon_size:I = 0x7f0c0730

.field public static final rating_section_card_rating_bar_vertical_padding:I = 0x7f0c0731

.field public static final rating_section_card_review_button_height:I = 0x7f0c0733

.field public static final rating_section_card_star_horizontal_padding:I = 0x7f0c0732

.field public static final rating_section_card_view_picture_height:I = 0x7f0c072f

.field public static final rating_section_card_view_picture_width:I = 0x7f0c072e

.field public static final rating_section_image_max_translation_x:I = 0x7f0c0737

.field public static final rating_section_image_min_translation_x:I = 0x7f0c0736

.field public static final rating_section_items_card_image_stack_height:I = 0x7f0c0739

.field public static final rating_section_nux_arrow_lateral_margin:I = 0x7f0c0734

.field public static final rating_section_nux_instruction_text_lateral_padding:I = 0x7f0c0735

.field public static final rating_section_standard_padding:I = 0x7f0c072d

.field public static final rating_section_swipe_progress_radius:I = 0x7f0c0738

.field public static final rating_star_bounce_height:I = 0x7f0c03e1

.field public static final reaction_attachment_profile_pic_size:I = 0x7f0c05b4

.field public static final reaction_card_corner_size:I = 0x7f0c05b5

.field public static final reaction_card_padding:I = 0x7f0c05b6

.field public static final reaction_gradient_height:I = 0x7f0c05b7

.field public static final reaction_hscroll_button_height:I = 0x7f0c05b8

.field public static final reaction_hscroll_text_container:I = 0x7f0c05b9

.field public static final reaction_hscroll_text_margin:I = 0x7f0c05ba

.field public static final reaction_icon_size:I = 0x7f0c05bb

.field public static final reaction_image_size:I = 0x7f0c05bc

.field public static final reaction_padding_image:I = 0x7f0c05bd

.field public static final reaction_padding_large:I = 0x7f0c05be

.field public static final reaction_padding_medium:I = 0x7f0c05bf

.field public static final reaction_padding_medium_large:I = 0x7f0c05c0

.field public static final reaction_padding_small:I = 0x7f0c05c1

.field public static final reaction_photo_size:I = 0x7f0c05c2

.field public static final reaction_profile_pic_border:I = 0x7f0c05c5

.field public static final reaction_profile_pic_size:I = 0x7f0c05c3

.field public static final reaction_profile_pic_size_small:I = 0x7f0c05c4

.field public static final reaction_right_left_padding:I = 0x7f0c05c6

.field public static final reaction_scroll_padding:I = 0x7f0c05c7

.field public static final reaction_text_header:I = 0x7f0c05c8

.field public static final recipient_picker_avatar_dimens:I = 0x7f0c064f

.field public static final recommended_link_banner_margin_bottom:I = 0x7f0c0779

.field public static final recommended_link_border_height:I = 0x7f0c077b

.field public static final recommended_link_border_width:I = 0x7f0c077a

.field public static final recommended_link_dismiss_bar_height:I = 0x7f0c077c

.field public static final recommended_link_divider_margin_bottom:I = 0x7f0c077d

.field public static final recommended_link_divider_width:I = 0x7f0c077e

.field public static final recommended_link_drag_handle_height:I = 0x7f0c077f

.field public static final recommended_link_drag_handle_width:I = 0x7f0c0780

.field public static final recommended_link_image_height:I = 0x7f0c0781

.field public static final recommended_link_image_margin_bottom:I = 0x7f0c0783

.field public static final recommended_link_image_margin_left:I = 0x7f0c0784

.field public static final recommended_link_image_margin_right:I = 0x7f0c0785

.field public static final recommended_link_image_width:I = 0x7f0c0782

.field public static final recommended_link_show_button_margin:I = 0x7f0c0786

.field public static final recommended_link_show_button_padding:I = 0x7f0c0787

.field public static final recommended_link_text_padding:I = 0x7f0c0788

.field public static final recommended_link_viewpager_height:I = 0x7f0c0789

.field public static final recommended_link_viewpager_margin:I = 0x7f0c078a

.field public static final refreshable_list_view_image_padding_angora:I = 0x7f0c0140

.field public static final refreshable_list_view_spring_k:I = 0x7f0c0141

.field public static final related_videos_card_cover_image_height:I = 0x7f0c05f2

.field public static final related_videos_card_like_count_size:I = 0x7f0c05f4

.field public static final related_videos_card_like_icon_height:I = 0x7f0c05f5

.field public static final related_videos_card_line_spacing:I = 0x7f0c05f6

.field public static final related_videos_card_title_size:I = 0x7f0c05f3

.field public static final related_videos_cards_height:I = 0x7f0c05f0

.field public static final related_videos_cards_title_size:I = 0x7f0c05f1

.field public static final related_videos_carousel_bottom_spacing:I = 0x7f0c0605

.field public static final related_videos_carousel_horizontal_spacing:I = 0x7f0c0600

.field public static final related_videos_carousel_item_cover_image_height:I = 0x7f0c05fb

.field public static final related_videos_carousel_item_cover_image_width:I = 0x7f0c05fa

.field public static final related_videos_carousel_item_like_count_size:I = 0x7f0c05fe

.field public static final related_videos_carousel_item_like_image_height:I = 0x7f0c05f8

.field public static final related_videos_carousel_item_like_image_width:I = 0x7f0c05f7

.field public static final related_videos_carousel_item_placeholder_bottom_spacing:I = 0x7f0c060d

.field public static final related_videos_carousel_item_placeholder_likes_line_height:I = 0x7f0c060c

.field public static final related_videos_carousel_item_placeholder_title_likes_space:I = 0x7f0c060b

.field public static final related_videos_carousel_item_placeholder_title_line_height:I = 0x7f0c0609

.field public static final related_videos_carousel_item_placeholder_title_line_space:I = 0x7f0c060a

.field public static final related_videos_carousel_item_placeholder_vertical_spacing:I = 0x7f0c0608

.field public static final related_videos_carousel_item_spacing:I = 0x7f0c05ff

.field public static final related_videos_carousel_item_title_height:I = 0x7f0c05fd

.field public static final related_videos_carousel_item_title_size:I = 0x7f0c05fc

.field public static final related_videos_carousel_scroll_height:I = 0x7f0c0606

.field public static final related_videos_carousel_shimmer_width:I = 0x7f0c0607

.field public static final related_videos_carousel_title_left_spacing:I = 0x7f0c0601

.field public static final related_videos_carousel_title_likes_space:I = 0x7f0c0604

.field public static final related_videos_carousel_title_padding:I = 0x7f0c0602

.field public static final related_videos_carousel_title_size:I = 0x7f0c05f9

.field public static final related_videos_carousel_vertical_spacing:I = 0x7f0c0603

.field public static final removable_tag_left_padding:I = 0x7f0c042b

.field public static final removable_tag_middle_padding:I = 0x7f0c042c

.field public static final removable_tag_right_padding:I = 0x7f0c042d

.field public static final removable_tag_vertical_padding:I = 0x7f0c042a

.field public static final review_compose_button_padding:I = 0x7f0c0387

.field public static final review_feeback_extra_touch_margin:I = 0x7f0c038d

.field public static final review_feedback_separator_margin:I = 0x7f0c038c

.field public static final review_row_view_rating_bottom_padding:I = 0x7f0c038f

.field public static final review_secondary_action_extra_touch_margin:I = 0x7f0c038e

.field public static final review_unit_header_height:I = 0x7f0c0580

.field public static final review_unit_privacy_selector_arrow_height:I = 0x7f0c057f

.field public static final review_unit_privacy_selector_arrow_width:I = 0x7f0c057e

.field public static final review_unit_privacy_selector_icon_height:I = 0x7f0c057d

.field public static final review_unit_privacy_selector_icon_width:I = 0x7f0c057c

.field public static final review_view_standard_padding:I = 0x7f0c038b

.field public static final review_view_top_padding:I = 0x7f0c038a

.field public static final reviews_list_item_padding:I = 0x7f0c0389

.field public static final reviews_list_item_profile_picture_size:I = 0x7f0c0388

.field public static final ridge_widget_dot_gap:I = 0x7f0c03e7

.field public static final ridge_widget_dot_grid_margin:I = 0x7f0c03e8

.field public static final ridge_widget_dot_width:I = 0x7f0c03e6

.field public static final ridge_widget_height:I = 0x7f0c03e2

.field public static final ridge_widget_icon_column_width:I = 0x7f0c03e5

.field public static final ridge_widget_icon_size:I = 0x7f0c03e4

.field public static final ridge_widget_width:I = 0x7f0c03e3

.field public static final right_error_msg_padding:I = 0x7f0c04c5

.field public static final roe_footer_button_padding:I = 0x7f0c02ba

.field public static final saved_collection_bottom_section_margin:I = 0x7f0c0235

.field public static final saved_collection_bottom_section_size:I = 0x7f0c0234

.field public static final saved_collection_feed_unit_item_height:I = 0x7f0c025b

.field public static final saved_collection_inter_page_margin:I = 0x7f0c0233

.field public static final saved_collection_item_downloaded_image_height:I = 0x7f0c0231

.field public static final saved_collection_item_downloaded_image_width:I = 0x7f0c0230

.field public static final saved_collection_item_image_height:I = 0x7f0c022f

.field public static final saved_collection_item_image_width:I = 0x7f0c022e

.field public static final saved_collection_save_button_margin:I = 0x7f0c0236

.field public static final saved_collection_view_pager_border_thickness:I = 0x7f0c0232

.field public static final saved_dashboard_action_button_vertical_extra_tap_area:I = 0x7f0c0795

.field public static final saved_dashboard_divider_height:I = 0x7f0c0792

.field public static final saved_dashboard_header_bottom_padding:I = 0x7f0c0791

.field public static final saved_dashboard_header_height:I = 0x7f0c078f

.field public static final saved_dashboard_interstitial_button_width:I = 0x7f0c0796

.field public static final saved_dashboard_interstitial_margin:I = 0x7f0c0797

.field public static final saved_dashboard_item_height:I = 0x7f0c0790

.field public static final saved_dashboard_list_item_padding:I = 0x7f0c078c

.field public static final saved_dashboard_load_more_row_height:I = 0x7f0c0793

.field public static final saved_dashboard_profile_picture_size:I = 0x7f0c078e

.field public static final saved_dashboard_saved_item_padding:I = 0x7f0c078d

.field public static final saved_dashboard_standard_padding:I = 0x7f0c078b

.field public static final saved_dashboard_undo_button_size:I = 0x7f0c0794

.field public static final search_verified_badge_margin_left:I = 0x7f0c06a7

.field public static final secondary_tab_bottom_border:I = 0x7f0c00e6

.field public static final secondary_tab_height:I = 0x7f0c00e4

.field public static final secondary_tab_page_indicator_height:I = 0x7f0c00e5

.field public static final see_all_footer_height:I = 0x7f0c0258

.field public static final selected_face_box_screen_size:I = 0x7f0c02e4

.field public static final selected_face_box_top_margin:I = 0x7f0c02e3

.field public static final separator_margin:I = 0x7f0c0018

.field public static final share_gift_choice_avatar_dimensions:I = 0x7f0c063c

.field public static final share_gift_choice_gift_height:I = 0x7f0c063f

.field public static final share_gift_choice_tag_content_top_padding:I = 0x7f0c063e

.field public static final share_gift_choice_tag_top_padding:I = 0x7f0c063d

.field public static final share_negative_left_right_margin:I = 0x7f0c03a7

.field public static final share_negative_top_bottom_margin:I = 0x7f0c03a8

.field public static final share_nux_top_margin:I = 0x7f0c03aa

.field public static final share_separator_height:I = 0x7f0c03a9

.field public static final share_status_top_bottom_margin:I = 0x7f0c03a6

.field public static final share_title_left_padding:I = 0x7f0c06c9

.field public static final share_title_right_padding:I = 0x7f0c06c8

.field public static final share_title_top_padding:I = 0x7f0c06ca

.field public static final share_view_url_image_margin_bottom:I = 0x7f0c01a2

.field public static final share_view_url_image_margin_left:I = 0x7f0c01a3

.field public static final share_view_url_image_margin_right:I = 0x7f0c01a4

.field public static final share_view_url_image_margin_top:I = 0x7f0c01a1

.field public static final shift_keyboard_size:I = 0x7f0c0302

.field public static final shortcut_banner_notification_icon:I = 0x7f0c01c1

.field public static final simplegallery_bottom_bar_height:I = 0x7f0c0383

.field public static final simplepicker_divider_height:I = 0x7f0c037d

.field public static final simplepicker_divider_width:I = 0x7f0c037e

.field public static final simplepicker_no_photo_image_size:I = 0x7f0c0381

.field public static final simplepicker_no_photo_text_line_spacing:I = 0x7f0c0382

.field public static final simplepicker_no_photo_view_width:I = 0x7f0c0380

.field public static final simplepicker_progress_bar_height:I = 0x7f0c037a

.field public static final simplepicker_progress_bar_margin_top:I = 0x7f0c037b

.field public static final simplepicker_thumbnail_size:I = 0x7f0c037f

.field public static final simplepicker_title_bar_height:I = 0x7f0c0378

.field public static final simplepicker_title_bar_height_landscape:I = 0x7f0c0379

.field public static final simplepicker_use_button_padding:I = 0x7f0c037c

.field public static final snowflake_photo_default_height:I = 0x7f0c02f4

.field public static final snowflake_photo_view_bottom_margin:I = 0x7f0c02f2

.field public static final snowflake_photo_view_bottom_shadow_offset:I = 0x7f0c030a

.field public static final snowflake_photo_view_left_shadow_offset:I = 0x7f0c030b

.field public static final snowflake_photo_view_right_shadow_offset:I = 0x7f0c0309

.field public static final snowflake_photo_view_side_margin:I = 0x7f0c02f3

.field public static final snowflake_photo_view_top_shadow_offset:I = 0x7f0c0308

.field public static final social_wifi_feed_unit_image_size:I = 0x7f0c0241

.field public static final sound_wave_view_default_spacing:I = 0x7f0c011a

.field public static final space_saving_height:I = 0x7f0c015b

.field public static final spinner_horizontal_margin:I = 0x7f0c05d3

.field public static final spinner_icon_margin_right:I = 0x7f0c05d2

.field public static final spinner_icon_size:I = 0x7f0c05d1

.field public static final spinner_text_padding_bottom:I = 0x7f0c05d0

.field public static final spinner_text_padding_top:I = 0x7f0c05cf

.field public static final spinner_text_size:I = 0x7f0c05ce

.field public static final sports_stories_height:I = 0x7f0c059a

.field public static final sports_stories_padding:I = 0x7f0c059b

.field public static final sports_stories_team_logo_size:I = 0x7f0c059c

.field public static final sports_stories_with_scores_padding_between_lines:I = 0x7f0c059d

.field public static final sports_stories_with_scores_team_name_paddingLeft:I = 0x7f0c059e

.field public static final spring_configurator_reveal_px:I = 0x7f0c000b

.field public static final spring_configurator_stash_px:I = 0x7f0c000c

.field public static final standard_10pt_text_size:I = 0x7f0c0618

.field public static final standard_11pt_text_size:I = 0x7f0c0619

.field public static final standard_12pt_text_size:I = 0x7f0c061a

.field public static final standard_13pt_text_size:I = 0x7f0c061b

.field public static final standard_14pt_text_size:I = 0x7f0c061c

.field public static final standard_15pt_text_size:I = 0x7f0c061d

.field public static final standard_16pt_text_size:I = 0x7f0c061e

.field public static final standard_18pt_text_size:I = 0x7f0c061f

.field public static final standard_button_height:I = 0x7f0c0611

.field public static final standard_button_text_size:I = 0x7f0c0620

.field public static final standard_double_padding:I = 0x7f0c0615

.field public static final standard_half_padding:I = 0x7f0c0617

.field public static final standard_half_padding_and_a_half:I = 0x7f0c0614

.field public static final standard_padding:I = 0x7f0c0612

.field public static final standard_padding_and_a_half:I = 0x7f0c0613

.field public static final standard_tripple_padding:I = 0x7f0c0616

.field public static final sticker_popup_tab_width:I = 0x7f0c0155

.field public static final sticker_search_edittext_height:I = 0x7f0c01dd

.field public static final sticker_search_edittext_icon_size:I = 0x7f0c01de

.field public static final sticker_search_left_padding:I = 0x7f0c01da

.field public static final sticker_search_right_padding:I = 0x7f0c01db

.field public static final sticker_search_tag_item_height:I = 0x7f0c01e3

.field public static final sticker_search_tag_item_left_padding:I = 0x7f0c01e4

.field public static final sticker_search_tag_item_right_padding:I = 0x7f0c01e6

.field public static final sticker_search_tag_item_text_left_padding:I = 0x7f0c01e5

.field public static final sticker_search_tag_item_thumbnail_size:I = 0x7f0c01e7

.field public static final sticker_search_tag_item_width:I = 0x7f0c01e2

.field public static final sticker_search_tags_grid_horizontal_spacing:I = 0x7f0c01df

.field public static final sticker_search_tags_grid_top_padding:I = 0x7f0c01e1

.field public static final sticker_search_tags_grid_vertical_spacing:I = 0x7f0c01e0

.field public static final sticker_search_text_size:I = 0x7f0c01dc

.field public static final sticker_store_page_btn:I = 0x7f0c0016

.field public static final sticker_store_page_description:I = 0x7f0c0015

.field public static final sticker_store_page_headline:I = 0x7f0c0014

.field public static final sticky_guardrail_button_height:I = 0x7f0c03d8

.field public static final sticky_guardrail_close_button_vertical_padding:I = 0x7f0c03d9

.field public static final sticky_guardrail_dialog_horizontal_padding:I = 0x7f0c03d7

.field public static final sticky_guardrail_dialog_vertical_padding:I = 0x7f0c03d6

.field public static final sticky_guardrail_element_vertical_padding:I = 0x7f0c03d5

.field public static final story_background_inner_padding:I = 0x7f0c05a3

.field public static final story_background_outer_padding:I = 0x7f0c05a2

.field public static final story_bottom_frame_padding:I = 0x7f0c05a0

.field public static final story_frame_padding:I = 0x7f0c059f

.field public static final story_insights_bar_height:I = 0x7f0c033c

.field public static final story_insights_bar_padding:I = 0x7f0c033b

.field public static final story_insights_header_padding:I = 0x7f0c0339

.field public static final story_insights_header_text_size:I = 0x7f0c033a

.field public static final story_insights_list_padding:I = 0x7f0c0338

.field public static final story_promotion_budget_bar_height:I = 0x7f0c032a

.field public static final story_promotion_budget_label_text_size:I = 0x7f0c032e

.field public static final story_promotion_budget_thumb_bg:I = 0x7f0c032b

.field public static final story_promotion_budget_thumb_border_padding:I = 0x7f0c032c

.field public static final story_promotion_budget_thumb_padding:I = 0x7f0c032d

.field public static final story_promotion_default_padding:I = 0x7f0c0331

.field public static final story_promotion_default_padding_top:I = 0x7f0c0332

.field public static final story_promotion_divider_height:I = 0x7f0c032f

.field public static final story_promotion_divider_padding:I = 0x7f0c0330

.field public static final story_promotion_selector_input_height:I = 0x7f0c0333

.field public static final story_promotion_spinner_item_padding:I = 0x7f0c0335

.field public static final story_promotion_spinner_top_margin:I = 0x7f0c0337

.field public static final story_promotion_spinner_top_padding:I = 0x7f0c0336

.field public static final story_promotion_text_size:I = 0x7f0c0334

.field public static final tab_segments_height:I = 0x7f0c06cd

.field public static final tag_padding:I = 0x7f0c02ef

.field public static final tag_people_badge_size:I = 0x7f0c03ab

.field public static final tag_remove_button_size:I = 0x7f0c02ee

.field public static final tag_remove_x_image_height:I = 0x7f0c0429

.field public static final tag_remove_x_image_width:I = 0x7f0c0428

.field public static final tag_suggestion_face_box_size:I = 0x7f0c042e

.field public static final tag_touch_target_padding:I = 0x7f0c02ed

.field public static final tag_typeahead_bubble_arrow_length:I = 0x7f0c02ea

.field public static final tag_typeahead_bubble_arrow_margin:I = 0x7f0c02ec

.field public static final tag_typeahead_bubble_corner_radius:I = 0x7f0c02e9

.field public static final tag_typeahead_bubble_stroke_width:I = 0x7f0c02eb

.field public static final tag_typeahead_max_height:I = 0x7f0c02e7

.field public static final tag_typeahead_max_width:I = 0x7f0c02e6

.field public static final tag_typeahead_min_height:I = 0x7f0c02e8

.field public static final tag_typeahead_offset_x_landscape:I = 0x7f0c0305

.field public static final tag_typeahead_offset_x_portrait:I = 0x7f0c0304

.field public static final tag_typeahead_side_margin:I = 0x7f0c02e5

.field public static final text_billboard:I = 0x7f0c001c

.field public static final text_cover_feed_large:I = 0x7f0c001f

.field public static final text_cover_feed_medium:I = 0x7f0c0021

.field public static final text_large:I = 0x7f0c001e

.field public static final text_medium:I = 0x7f0c0020

.field public static final text_small:I = 0x7f0c0022

.field public static final text_xlarge:I = 0x7f0c001d

.field public static final text_xsmall:I = 0x7f0c0023

.field public static final thread_tile_badge_shadow_dx:I = 0x7f0c0087

.field public static final thread_tile_badge_shadow_dy:I = 0x7f0c0088

.field public static final thread_tile_badge_shadow_radius:I = 0x7f0c0089

.field public static final three_grid_size:I = 0x7f0c0260

.field public static final thumbnail_image_attachment_message_size:I = 0x7f0c01a0

.field public static final thumbnail_image_hot_like_sticker_large:I = 0x7f0c0119

.field public static final thumbnail_image_hot_like_sticker_medium:I = 0x7f0c0118

.field public static final thumbnail_image_hot_like_sticker_small:I = 0x7f0c0117

.field public static final thumbnail_image_like_sticker_size:I = 0x7f0c0113

.field public static final thumbnail_image_like_sticker_size_neue:I = 0x7f0c0114

.field public static final thumbnail_image_share_preview_size:I = 0x7f0c0112

.field public static final thumbnail_image_sticker_size:I = 0x7f0c0115

.field public static final thumbnail_media_tray_item_size:I = 0x7f0c0116

.field public static final timeline_app_collection_button_hit_expansion:I = 0x7f0c0293

.field public static final timeline_byline_fragment_icon_size:I = 0x7f0c0509

.field public static final timeline_byline_fragment_text_size:I = 0x7f0c050a

.field public static final timeline_header_margin_left:I = 0x7f0c04ed

.field public static final timeline_moments_unit_product_image_size:I = 0x7f0c051e

.field public static final timeline_moments_unit_product_pager_spacing:I = 0x7f0c051f

.field public static final timeline_name_size_large:I = 0x7f0c04ee

.field public static final timeline_name_size_small:I = 0x7f0c04ef

.field public static final timeline_navtile_item_text_size:I = 0x7f0c0512

.field public static final timeline_navtile_margin:I = 0x7f0c050e

.field public static final timeline_navtile_margin_max:I = 0x7f0c0511

.field public static final timeline_navtile_margin_min:I = 0x7f0c0510

.field public static final timeline_navtile_tile_padding:I = 0x7f0c050f

.field public static final timeline_navtile_width:I = 0x7f0c050d

.field public static final timeline_profile_height_with_padding:I = 0x7f0c04f2

.field public static final timeline_profile_image_inset:I = 0x7f0c0500

.field public static final timeline_profile_image_size:I = 0x7f0c050b

.field public static final timeline_profile_name_height:I = 0x7f0c0501

.field public static final timeline_profile_padding:I = 0x7f0c04f3

.field public static final timeline_profile_padding_top:I = 0x7f0c04f4

.field public static final timeline_profile_pic_drawable_padding:I = 0x7f0c04f5

.field public static final timeline_profile_question_disabled_item_icon_size:I = 0x7f0c0527

.field public static final timeline_profile_question_disabled_item_text_size:I = 0x7f0c0526

.field public static final timeline_profile_question_item_icon_size:I = 0x7f0c0524

.field public static final timeline_profile_question_item_text_icon_padding:I = 0x7f0c0525

.field public static final timeline_profile_question_item_text_size:I = 0x7f0c0523

.field public static final timeline_profile_questions_margin:I = 0x7f0c0522

.field public static final timeline_profile_questions_title_text_size:I = 0x7f0c0521

.field public static final timeline_profile_width_with_padding:I = 0x7f0c04f1

.field public static final timeline_prompt_drawable_padding:I = 0x7f0c051d

.field public static final timeline_prompt_horizontal_padding:I = 0x7f0c051c

.field public static final timeline_prompt_vertical_padding:I = 0x7f0c051b

.field public static final timeline_publish_bar_height:I = 0x7f0c0520

.field public static final timeline_sections_dot_separator_padding:I = 0x7f0c0532

.field public static final timeline_verified_badge_margin:I = 0x7f0c04f0

.field public static final timeline_year_overview_container_padding:I = 0x7f0c0529

.field public static final timeline_year_overview_elements_padding:I = 0x7f0c0528

.field public static final timeline_year_overview_item_image_margin_top:I = 0x7f0c052e

.field public static final timeline_year_overview_item_image_size:I = 0x7f0c052d

.field public static final timeline_year_overview_item_margin:I = 0x7f0c052f

.field public static final timeline_year_overview_photo_inside_margin:I = 0x7f0c0531

.field public static final timeline_year_overview_photo_outside_margin:I = 0x7f0c0530

.field public static final timeline_year_overview_title_margin_bottom:I = 0x7f0c052c

.field public static final timeline_year_overview_title_margin_side:I = 0x7f0c052b

.field public static final timeline_year_overview_title_margin_top:I = 0x7f0c052a

.field public static final title_bar_action_button_width:I = 0x7f0c06b5

.field public static final title_bar_back_button_width:I = 0x7f0c06b6

.field public static final title_bar_height:I = 0x7f0c0005

.field public static final titlebar_button_width:I = 0x7f0c0159

.field public static final tokenizedtypeahead_fig_check_margin_left:I = 0x7f0c0104

.field public static final tokenizedtypeahead_fig_check_margin_right:I = 0x7f0c0105

.field public static final tokenizedtypeahead_fig_check_margin_y:I = 0x7f0c0106

.field public static final tokenizedtypeahead_fig_check_size:I = 0x7f0c0108

.field public static final tokenizedtypeahead_fig_icon_margin:I = 0x7f0c0102

.field public static final tokenizedtypeahead_fig_text_right_margin:I = 0x7f0c0103

.field public static final tokenizedtypeahead_fig_text_size:I = 0x7f0c0107

.field public static final tokenizedtypeahead_icon_margin_x:I = 0x7f0c0100

.field public static final tokenizedtypeahead_item_row_text_height:I = 0x7f0c0101

.field public static final tokenizedtypeahead_span_margin_x:I = 0x7f0c00fe

.field public static final tokenizedtypeahead_span_margin_y:I = 0x7f0c00ff

.field public static final top_border_padding:I = 0x7f0c04bc

.field public static final top_map_height:I = 0x7f0c0445

.field public static final top_map_possible_height:I = 0x7f0c0446

.field public static final top_photos_padding:I = 0x7f0c04be

.field public static final trending_topic_bottom_radius:I = 0x7f0c05a5

.field public static final trending_topic_top_radius:I = 0x7f0c05a4

.field public static final trimming_portrait_film_strip_height:I = 0x7f0c0135

.field public static final trimming_preview_margin:I = 0x7f0c0134

.field public static final two_dp:I = 0x7f0c01f7

.field public static final two_grid_size:I = 0x7f0c0261

.field public static final two_px:I = 0x7f0c0000

.field public static final typing_animation_user_tile_slop_height:I = 0x7f0c01b4

.field public static final ufi_bar_height:I = 0x7f0c00b3

.field public static final ufi_button_touch_slop:I = 0x7f0c00b8

.field public static final ufi_comment_button_margin_left:I = 0x7f0c00b0

.field public static final ufi_comment_button_margin_top:I = 0x7f0c00af

.field public static final ufi_flyout_dash_vertical_origin:I = 0x7f0c0035

.field public static final ufi_flyout_horizontal_offset:I = 0x7f0c0034

.field public static final ufi_flyout_nub_margin_bottom:I = 0x7f0c0033

.field public static final ufi_like_button_extra_hit_target_padding_dp:I = 0x7f0c00ad

.field public static final ufi_like_button_margin_left:I = 0x7f0c00ae

.field public static final ufi_like_comments_text_size:I = 0x7f0c00b5

.field public static final ufi_mini_names_text_size:I = 0x7f0c00b6

.field public static final ufi_more_options_button_margin_left:I = 0x7f0c00b2

.field public static final ufi_more_options_button_margin_top:I = 0x7f0c00b1

.field public static final ufi_more_options_flyout_horizontal_offset:I = 0x7f0c00b9

.field public static final ufi_popover_prompt_dialogue_height:I = 0x7f0c00bb

.field public static final ufi_popover_vertical_origin:I = 0x7f0c00ba

.field public static final ufi_third_party_link_text_size:I = 0x7f0c00b7

.field public static final ufi_view_height:I = 0x7f0c00b4

.field public static final ufiservice_flyout_margin_left_right:I = 0x7f0c01f0

.field public static final ufiservice_flyout_padding_top_bottom:I = 0x7f0c01f1

.field public static final ufiservices_comment_retry_size:I = 0x7f0c01ef

.field public static final ufiservices_flyout_comment_edit_button_height:I = 0x7f0c01f2

.field public static final ufiservices_flyout_comment_edit_button_margin:I = 0x7f0c01f3

.field public static final ufiservices_flyout_comment_edit_button_padding:I = 0x7f0c01f4

.field public static final ufiservices_flyout_comment_feedback_text_size:I = 0x7f0c01f5

.field public static final ufiservices_flyout_comment_feedback_text_vertical_adjustment:I = 0x7f0c01f6

.field public static final ufiservices_likers_profile_image_size:I = 0x7f0c01ee

.field public static final underwood_add_photo_side_padding:I = 0x7f0c03bd

.field public static final underwood_attachment_side_padding:I = 0x7f0c03bc

.field public static final underwood_nux_inner_circle_size:I = 0x7f0c03c0

.field public static final underwood_nux_outer_ring_size:I = 0x7f0c03c1

.field public static final underwood_nux_text_offset:I = 0x7f0c03bf

.field public static final underwood_nux_text_translation:I = 0x7f0c03be

.field public static final underwood_progress_ring_stroke_width:I = 0x7f0c03c2

.field public static final underwood_status_text_side_padding:I = 0x7f0c03c3

.field public static final underwood_status_text_top_padding:I = 0x7f0c03c4

.field public static final unlock_affordance_origin_y:I = 0x7f0c0311

.field public static final unlock_affordance_peek_height:I = 0x7f0c0314

.field public static final unlock_affordance_vertical_fade_region:I = 0x7f0c0313

.field public static final unlock_affordance_y_to_start_animating_in_cover_feed:I = 0x7f0c0312

.field public static final unread_count_margin_right:I = 0x7f0c00e8

.field public static final unread_count_offset_x:I = 0x7f0c00e9

.field public static final unread_count_offset_y:I = 0x7f0c00ea

.field public static final unread_count_radius:I = 0x7f0c00eb

.field public static final unread_count_text_size:I = 0x7f0c00ec

.field public static final upsell_alert_icon_size:I = 0x7f0c0410

.field public static final upsell_interstitial_button_height:I = 0x7f0c040c

.field public static final upsell_interstitial_content_margin:I = 0x7f0c0406

.field public static final upsell_interstitial_dialog_horizontal_margin:I = 0x7f0c0404

.field public static final upsell_interstitial_dialog_vertical_margin:I = 0x7f0c0405

.field public static final upsell_interstitial_keyline_margin:I = 0x7f0c0407

.field public static final upsell_interstitial_min_row_height:I = 0x7f0c0409

.field public static final upsell_interstitial_progress_spacing:I = 0x7f0c040b

.field public static final upsell_interstitial_row_padding:I = 0x7f0c0408

.field public static final upsell_interstitial_title_padding:I = 0x7f0c040e

.field public static final upsell_interstitial_title_text_size:I = 0x7f0c040d

.field public static final upsell_interstitial_view_spacing:I = 0x7f0c040a

.field public static final upsell_shortcut_icon_size:I = 0x7f0c040f

.field public static final use_button_text_size:I = 0x7f0c0385

.field public static final user_account_nux_step_profile_info_big_padding:I = 0x7f0c066c

.field public static final user_account_nux_step_profile_info_item_privacy_icon_size:I = 0x7f0c0669

.field public static final user_account_nux_step_profile_info_medium_padding:I = 0x7f0c066b

.field public static final user_account_nux_step_profile_info_small_padding:I = 0x7f0c066a

.field public static final vault_older_photos_btn_height:I = 0x7f0c0351

.field public static final vault_optin_btn_horizontal_padding:I = 0x7f0c0341

.field public static final vault_optin_btn_text_size:I = 0x7f0c033f

.field public static final vault_optin_btn_vertical_padding:I = 0x7f0c0340

.field public static final vault_optin_horiz_border:I = 0x7f0c033e

.field public static final vault_optin_radio_padding:I = 0x7f0c0342

.field public static final vault_optin_radio_padding_left:I = 0x7f0c0343

.field public static final vault_optin_spacing_small:I = 0x7f0c033d

.field public static final vault_photo_option_line_width:I = 0x7f0c034f

.field public static final vault_photo_option_padding:I = 0x7f0c0350

.field public static final vault_sync_list_item_height:I = 0x7f0c034e

.field public static final vault_sync_screen_header_image_spacing:I = 0x7f0c0347

.field public static final vault_sync_screen_header_size:I = 0x7f0c0345

.field public static final vault_sync_screen_header_text_spacing:I = 0x7f0c0348

.field public static final vault_sync_screen_image_size:I = 0x7f0c0346

.field public static final vault_sync_screen_spacing:I = 0x7f0c0344

.field public static final vault_sync_settings_margin_top:I = 0x7f0c034c

.field public static final vault_sync_settings_outer_margin:I = 0x7f0c0349

.field public static final vault_sync_settings_outer_padding:I = 0x7f0c034a

.field public static final vault_sync_settings_text_size:I = 0x7f0c034d

.field public static final vault_sync_settings_total_margin:I = 0x7f0c034b

.field public static final video_chaining_item_first_left_margin:I = 0x7f0c0587

.field public static final video_chaining_item_image_height:I = 0x7f0c0586

.field public static final video_chaining_item_image_width:I = 0x7f0c0585

.field public static final video_chaining_item_spacing:I = 0x7f0c0588

.field public static final video_chaining_item_text_side_padding:I = 0x7f0c0589

.field public static final video_chaining_item_text_vertical_padding:I = 0x7f0c058a

.field public static final video_postplay_suggestions_spacing:I = 0x7f0c05ed

.field public static final video_postplay_suggestions_title_shadow:I = 0x7f0c05ef

.field public static final video_postplay_suggestions_title_size:I = 0x7f0c05ee

.field public static final video_suggestion_author_shadow:I = 0x7f0c05ec

.field public static final video_suggestion_left_padding:I = 0x7f0c05e4

.field public static final video_suggestion_like_icon_margin:I = 0x7f0c05e0

.field public static final video_suggestion_likes_views_margin:I = 0x7f0c05e1

.field public static final video_suggestion_likes_views_shadow:I = 0x7f0c05eb

.field public static final video_suggestion_likes_views_size:I = 0x7f0c05e8

.field public static final video_suggestion_margin:I = 0x7f0c05e6

.field public static final video_suggestion_right_padding:I = 0x7f0c05e5

.field public static final video_suggestion_title_shadow:I = 0x7f0c05ea

.field public static final video_suggestion_title_size:I = 0x7f0c05e7

.field public static final video_suggestion_vertical_margin:I = 0x7f0c05e2

.field public static final video_suggestion_video_height:I = 0x7f0c05de

.field public static final video_suggestion_video_width:I = 0x7f0c05df

.field public static final video_suggestions_author_size:I = 0x7f0c05e9

.field public static final video_suggestions_author_stats_margin:I = 0x7f0c05e3

.field public static final view_swipe_min_dismiss_velocity:I = 0x7f0c02e2

.field public static final zero:I = 0x7f0c013b

.field public static final zero_interstitial_bottom_padding:I = 0x7f0c0146

.field public static final zero_interstitial_button_section_top_margin:I = 0x7f0c014e

.field public static final zero_interstitial_content_section_between_component_margin:I = 0x7f0c014c

.field public static final zero_interstitial_content_section_bottom_margin:I = 0x7f0c014b

.field public static final zero_interstitial_content_section_text_size:I = 0x7f0c014d

.field public static final zero_interstitial_header_section_between_component_margin:I = 0x7f0c0148

.field public static final zero_interstitial_header_section_bottom_margin:I = 0x7f0c0147

.field public static final zero_interstitial_header_section_description_text_size:I = 0x7f0c014a

.field public static final zero_interstitial_header_section_title_text_size:I = 0x7f0c0149

.field public static final zero_interstitial_left_padding:I = 0x7f0c0145

.field public static final zero_interstitial_right_padding:I = 0x7f0c0144

.field public static final zero_interstitial_top_padding:I = 0x7f0c0143

.field public static final zero_rating_top_banner_height:I = 0x7f0c0006


# direct methods
.method public constructor <init>()V
    .locals 0

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method
