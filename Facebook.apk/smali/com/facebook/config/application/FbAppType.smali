.class public Lcom/facebook/config/application/FbAppType;
.super Ljava/lang/Object;
.source "FbAppType.java"

# interfaces
.implements Lcom/facebook/config/application/PlatformAppConfig;


# annotations
.annotation build Ljavax/annotation/concurrent/Immutable;
.end annotation


# instance fields
.field private final a:Ljava/lang/String;

.field private final b:Ljava/lang/String;

.field private final c:Ljava/lang/String;

.field private final d:Ljava/lang/String;

.field private final e:Ljava/lang/String;

.field private final f:Ljava/lang/String;

.field private final g:Ljava/lang/String;

.field private final h:Lcom/facebook/config/application/IntendedAudience;

.field private final i:Lcom/facebook/config/application/Product;

.field private final j:Lcom/facebook/common/build/SignatureType;


# direct methods
.method public constructor <init>(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Lcom/facebook/config/application/IntendedAudience;Lcom/facebook/config/application/Product;Lcom/facebook/common/build/SignatureType;)V
    .locals 0

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    iput-object p1, p0, Lcom/facebook/config/application/FbAppType;->a:Ljava/lang/String;

    iput-object p2, p0, Lcom/facebook/config/application/FbAppType;->b:Ljava/lang/String;

    iput-object p3, p0, Lcom/facebook/config/application/FbAppType;->c:Ljava/lang/String;

    iput-object p4, p0, Lcom/facebook/config/application/FbAppType;->d:Ljava/lang/String;

    iput-object p5, p0, Lcom/facebook/config/application/FbAppType;->e:Ljava/lang/String;

    iput-object p6, p0, Lcom/facebook/config/application/FbAppType;->f:Ljava/lang/String;

    iput-object p7, p0, Lcom/facebook/config/application/FbAppType;->g:Ljava/lang/String;

    iput-object p8, p0, Lcom/facebook/config/application/FbAppType;->h:Lcom/facebook/config/application/IntendedAudience;

    iput-object p9, p0, Lcom/facebook/config/application/FbAppType;->i:Lcom/facebook/config/application/Product;

    iput-object p10, p0, Lcom/facebook/config/application/FbAppType;->j:Lcom/facebook/common/build/SignatureType;

    return-void
.end method


# virtual methods
.method public final a()Ljava/lang/String;
    .locals 1

    iget-object v0, p0, Lcom/facebook/config/application/FbAppType;->a:Ljava/lang/String;

    return-object v0
.end method

.method public final b()Ljava/lang/String;
    .locals 1

    iget-object v0, p0, Lcom/facebook/config/application/FbAppType;->b:Ljava/lang/String;

    return-object v0
.end method

.method public final c()Ljava/lang/String;
    .locals 1

    iget-object v0, p0, Lcom/facebook/config/application/FbAppType;->c:Ljava/lang/String;

    return-object v0
.end method

.method public final d()Ljava/lang/String;
    .locals 1

    iget-object v0, p0, Lcom/facebook/config/application/FbAppType;->d:Ljava/lang/String;

    return-object v0
.end method

.method public final e()Ljava/lang/String;
    .locals 1

    iget-object v0, p0, Lcom/facebook/config/application/FbAppType;->e:Ljava/lang/String;

    return-object v0
.end method

.method public final f()Ljava/lang/String;
    .locals 1

    iget-object v0, p0, Lcom/facebook/config/application/FbAppType;->f:Ljava/lang/String;

    return-object v0
.end method

.method public final g()Ljava/lang/String;
    .locals 1

    iget-object v0, p0, Lcom/facebook/config/application/FbAppType;->g:Ljava/lang/String;

    return-object v0
.end method

.method public final h()Lcom/facebook/config/application/IntendedAudience;
    .locals 1

    iget-object v0, p0, Lcom/facebook/config/application/FbAppType;->h:Lcom/facebook/config/application/IntendedAudience;

    return-object v0
.end method

.method public final i()Lcom/facebook/config/application/Product;
    .locals 1

    iget-object v0, p0, Lcom/facebook/config/application/FbAppType;->i:Lcom/facebook/config/application/Product;

    return-object v0
.end method

.method public final j()Lcom/facebook/common/build/SignatureType;
    .locals 1

    iget-object v0, p0, Lcom/facebook/config/application/FbAppType;->j:Lcom/facebook/common/build/SignatureType;

    return-object v0
.end method
