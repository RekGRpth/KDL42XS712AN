.class public abstract Lcom/facebook/base/app/AbstractApplicationLike;
.super Ljava/lang/Object;
.source "AbstractApplicationLike.java"

# interfaces
.implements Lcom/facebook/base/app/ApplicationLike;


# static fields
.field private static final b:Ljava/lang/String;


# instance fields
.field protected final a:Landroid/app/Application;

.field private final c:Ljava/util/concurrent/atomic/AtomicInteger;

.field private final d:Lcom/facebook/config/application/FbAppType;

.field private e:Lcom/facebook/base/app/LightweightPerfEvents;

.field private f:Lcom/facebook/inject/FbInjector;

.field private g:Lcom/facebook/common/init/impl/FbAppInitializer;

.field private h:Lcom/facebook/debug/activitytracer/ActivityTracer;

.field private i:Ljava/util/concurrent/Executor;

.field private volatile j:Lcom/facebook/resources/FbResources;

.field private k:Lcom/facebook/common/memory/MemoryManager;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    const-class v0, Lcom/facebook/base/app/AbstractApplicationLike;

    invoke-virtual {v0}, Ljava/lang/Class;->getName()Ljava/lang/String;

    move-result-object v0

    sput-object v0, Lcom/facebook/base/app/AbstractApplicationLike;->b:Ljava/lang/String;

    return-void
.end method

.method public constructor <init>(Landroid/app/Application;Lcom/facebook/config/application/FbAppType;Lcom/facebook/base/app/LightweightPerfEvents;)V
    .locals 1

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    new-instance v0, Ljava/util/concurrent/atomic/AtomicInteger;

    invoke-direct {v0}, Ljava/util/concurrent/atomic/AtomicInteger;-><init>()V

    iput-object v0, p0, Lcom/facebook/base/app/AbstractApplicationLike;->c:Ljava/util/concurrent/atomic/AtomicInteger;

    iput-object p1, p0, Lcom/facebook/base/app/AbstractApplicationLike;->a:Landroid/app/Application;

    iput-object p2, p0, Lcom/facebook/base/app/AbstractApplicationLike;->d:Lcom/facebook/config/application/FbAppType;

    iput-object p3, p0, Lcom/facebook/base/app/AbstractApplicationLike;->e:Lcom/facebook/base/app/LightweightPerfEvents;

    return-void
.end method

.method private static e(Lcom/facebook/common/process/ProcessName;)V
    .locals 3
    .param p0    # Lcom/facebook/common/process/ProcessName;
        .annotation runtime Ljavax/annotation/Nullable;
        .end annotation
    .end param

    if-nez p0, :cond_0

    const-string v0, "null"

    :goto_0
    invoke-static {}, Lcom/facebook/acra/ErrorReporter;->getInstance()Lcom/facebook/acra/ErrorReporter;

    move-result-object v1

    const-string v2, "process_name_on_start"

    invoke-virtual {v1, v2, v0}, Lcom/facebook/acra/ErrorReporter;->putCustomData(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    return-void

    :cond_0
    invoke-virtual {p0}, Lcom/facebook/common/process/ProcessName;->b()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v0

    if-eqz v0, :cond_1

    const-string v0, "empty"

    goto :goto_0

    :cond_1
    invoke-virtual {p0}, Lcom/facebook/common/process/ProcessName;->b()Ljava/lang/String;

    move-result-object v0

    goto :goto_0
.end method

.method private h()V
    .locals 2

    sget-object v0, Lcom/facebook/base/app/AbstractApplicationLike$2;->a:[I

    iget-object v1, p0, Lcom/facebook/base/app/AbstractApplicationLike;->d:Lcom/facebook/config/application/FbAppType;

    invoke-virtual {v1}, Lcom/facebook/config/application/FbAppType;->h()Lcom/facebook/config/application/IntendedAudience;

    move-result-object v1

    invoke-virtual {v1}, Lcom/facebook/config/application/IntendedAudience;->ordinal()I

    move-result v1

    aget v0, v0, v1

    packed-switch v0, :pswitch_data_0

    const/4 v0, 0x5

    :goto_0
    invoke-static {v0}, Lcom/facebook/debug/log/BLog;->a(I)V

    return-void

    :pswitch_0
    const/4 v0, 0x3

    goto :goto_0

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
    .end packed-switch
.end method

.method private i()Lcom/facebook/common/process/ProcessName;
    .locals 4
    .annotation runtime Ljavax/annotation/Nullable;
    .end annotation

    iget-object v0, p0, Lcom/facebook/base/app/AbstractApplicationLike;->a:Landroid/app/Application;

    const-string v1, "activity"

    invoke-virtual {v0, v1}, Landroid/app/Application;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/app/ActivityManager;

    invoke-static {}, Landroid/os/Process;->myPid()I

    move-result v1

    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    invoke-static {v1}, Lcom/google/inject/util/Providers;->a(Ljava/lang/Object;)Ljavax/inject/Provider;

    move-result-object v1

    new-instance v2, Lcom/facebook/common/process/DefaultProcessUtil;

    iget-object v3, p0, Lcom/facebook/base/app/AbstractApplicationLike;->a:Landroid/app/Application;

    invoke-direct {v2, v3, v0, v1}, Lcom/facebook/common/process/DefaultProcessUtil;-><init>(Landroid/content/Context;Landroid/app/ActivityManager;Ljavax/inject/Provider;)V

    invoke-interface {v2}, Lcom/facebook/common/process/ProcessUtil;->a()Lcom/facebook/common/process/ProcessName;

    move-result-object v0

    return-object v0
.end method

.method private j()V
    .locals 3

    invoke-static {}, Lcom/facebook/acra/ErrorReporter;->getInstance()Lcom/facebook/acra/ErrorReporter;

    move-result-object v0

    iget-object v1, p0, Lcom/facebook/base/app/AbstractApplicationLike;->c:Ljava/util/concurrent/atomic/AtomicInteger;

    invoke-virtual {v1}, Ljava/util/concurrent/atomic/AtomicInteger;->incrementAndGet()I

    move-result v1

    const-string v2, "app_on_create_count"

    invoke-static {v1}, Ljava/lang/Integer;->toString(I)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v2, v1}, Lcom/facebook/acra/ErrorReporter;->putCustomData(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    return-void
.end method

.method private k()V
    .locals 0

    invoke-static {}, Lcom/facebook/base/app/AbstractApplicationLike;->l()V

    invoke-direct {p0}, Lcom/facebook/base/app/AbstractApplicationLike;->m()V

    return-void
.end method

.method private static l()V
    .locals 3

    sget-object v0, Lcom/facebook/base/app/AbstractApplicationLike;->b:Ljava/lang/String;

    const-string v1, "Forcing initialization of AsyncTask"

    invoke-static {v0, v1}, Lcom/facebook/debug/log/BLog;->a(Ljava/lang/String;Ljava/lang/String;)V

    :try_start_0
    const-string v0, "android.os.AsyncTask"

    invoke-static {v0}, Ljava/lang/Class;->forName(Ljava/lang/String;)Ljava/lang/Class;
    :try_end_0
    .catch Ljava/lang/ClassNotFoundException; {:try_start_0 .. :try_end_0} :catch_0

    :goto_0
    return-void

    :catch_0
    move-exception v0

    sget-object v1, Lcom/facebook/base/app/AbstractApplicationLike;->b:Ljava/lang/String;

    const-string v2, "Exception trying to initialize AsyncTask"

    invoke-static {v1, v2, v0}, Lcom/facebook/debug/log/BLog;->c(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)V

    goto :goto_0
.end method

.method private m()V
    .locals 2

    :try_start_0
    iget-object v0, p0, Lcom/facebook/base/app/AbstractApplicationLike;->a:Landroid/app/Application;

    const-string v1, "audio"

    invoke-virtual {v0, v1}, Landroid/app/Application;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;
    :try_end_0
    .catch Landroid/content/res/Resources$NotFoundException; {:try_start_0 .. :try_end_0} :catch_1
    .catch Ljava/lang/NullPointerException; {:try_start_0 .. :try_end_0} :catch_0

    :goto_0
    return-void

    :catch_0
    move-exception v0

    goto :goto_0

    :catch_1
    move-exception v0

    goto :goto_0
.end method


# virtual methods
.method public final a()Landroid/app/Application;
    .locals 1

    iget-object v0, p0, Lcom/facebook/base/app/AbstractApplicationLike;->a:Landroid/app/Application;

    return-object v0
.end method

.method public final a(I)V
    .locals 1

    iget-object v0, p0, Lcom/facebook/base/app/AbstractApplicationLike;->k:Lcom/facebook/common/memory/MemoryManager;

    invoke-virtual {v0, p1}, Lcom/facebook/common/memory/MemoryManager;->a(I)V

    return-void
.end method

.method public final a(Lcom/facebook/common/init/impl/FbAppInitializer;Ljava/util/concurrent/Executor;Lcom/facebook/debug/activitytracer/ActivityTracer;Lcom/facebook/resources/FbResources;Lcom/facebook/common/memory/MemoryManager;)V
    .locals 0
    .param p2    # Ljava/util/concurrent/Executor;
        .annotation runtime Lcom/facebook/common/executors/ForUiThread;
        .end annotation
    .end param
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    iput-object p1, p0, Lcom/facebook/base/app/AbstractApplicationLike;->g:Lcom/facebook/common/init/impl/FbAppInitializer;

    iput-object p2, p0, Lcom/facebook/base/app/AbstractApplicationLike;->i:Ljava/util/concurrent/Executor;

    iput-object p3, p0, Lcom/facebook/base/app/AbstractApplicationLike;->h:Lcom/facebook/debug/activitytracer/ActivityTracer;

    iput-object p4, p0, Lcom/facebook/base/app/AbstractApplicationLike;->j:Lcom/facebook/resources/FbResources;

    iput-object p5, p0, Lcom/facebook/base/app/AbstractApplicationLike;->k:Lcom/facebook/common/memory/MemoryManager;

    return-void
.end method

.method protected a(Lcom/facebook/multiprocess/experiment/config/MultiprocessConfigRegistry;)V
    .locals 0

    return-void
.end method

.method protected a(Lcom/facebook/common/process/ProcessName;)Z
    .locals 1

    const/4 v0, 0x0

    return v0
.end method

.method public final b()Lcom/facebook/config/application/FbAppType;
    .locals 1

    iget-object v0, p0, Lcom/facebook/base/app/AbstractApplicationLike;->d:Lcom/facebook/config/application/FbAppType;

    return-object v0
.end method

.method protected b(Lcom/facebook/common/process/ProcessName;)V
    .locals 0

    return-void
.end method

.method protected abstract c(Lcom/facebook/common/process/ProcessName;)Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/facebook/common/process/ProcessName;",
            ")",
            "Ljava/util/List",
            "<",
            "Lcom/facebook/inject/Module;",
            ">;"
        }
    .end annotation
.end method

.method public c()V
    .locals 5

    const-class v0, Lcom/google/common/base/FinalizableReferenceQueue;

    invoke-virtual {v0}, Ljava/lang/Class;->getName()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Ljava/util/logging/Logger;->getLogger(Ljava/lang/String;)Ljava/util/logging/Logger;

    move-result-object v0

    sget-object v1, Ljava/util/logging/Level;->SEVERE:Ljava/util/logging/Level;

    invoke-virtual {v0, v1}, Ljava/util/logging/Logger;->setLevel(Ljava/util/logging/Level;)V

    invoke-direct {p0}, Lcom/facebook/base/app/AbstractApplicationLike;->h()V

    const-wide/16 v0, 0x5

    invoke-static {v0, v1}, Lcom/facebook/debug/tracer/Tracer;->b(J)V

    const-string v0, "Application startup"

    invoke-static {v0}, Lcom/facebook/debug/tracer/Tracer;->b(Ljava/lang/String;)Lcom/facebook/debug/tracer/Tracer;

    move-result-object v0

    invoke-direct {p0}, Lcom/facebook/base/app/AbstractApplicationLike;->k()V

    invoke-direct {p0}, Lcom/facebook/base/app/AbstractApplicationLike;->j()V

    invoke-direct {p0}, Lcom/facebook/base/app/AbstractApplicationLike;->i()Lcom/facebook/common/process/ProcessName;

    move-result-object v1

    invoke-static {v1}, Lcom/facebook/base/app/AbstractApplicationLike;->e(Lcom/facebook/common/process/ProcessName;)V

    monitor-enter p0

    :try_start_0
    invoke-virtual {p0, v1}, Lcom/facebook/base/app/AbstractApplicationLike;->a(Lcom/facebook/common/process/ProcessName;)Z

    move-result v2

    if-eqz v2, :cond_0

    invoke-virtual {p0}, Lcom/facebook/base/app/AbstractApplicationLike;->d()V

    const/4 v2, 0x0

    invoke-static {v2}, Ljava/lang/System;->exit(I)V

    :cond_0
    invoke-static {}, Lcom/facebook/multiprocess/experiment/config/MultiprocessConfigRegistry;->a()Lcom/facebook/multiprocess/experiment/config/MultiprocessConfigRegistry;

    move-result-object v2

    invoke-virtual {p0, v2}, Lcom/facebook/base/app/AbstractApplicationLike;->a(Lcom/facebook/multiprocess/experiment/config/MultiprocessConfigRegistry;)V

    invoke-virtual {v2}, Lcom/facebook/multiprocess/experiment/config/MultiprocessConfigRegistry;->b()V

    invoke-virtual {p0, v1}, Lcom/facebook/base/app/AbstractApplicationLike;->b(Lcom/facebook/common/process/ProcessName;)V

    invoke-virtual {p0, v1}, Lcom/facebook/base/app/AbstractApplicationLike;->c(Lcom/facebook/common/process/ProcessName;)Ljava/util/List;

    move-result-object v2

    invoke-static {}, Lcom/google/common/collect/ImmutableList;->i()Lcom/google/common/collect/ImmutableList$Builder;

    move-result-object v3

    new-instance v4, Lcom/facebook/base/app/ApplicationLikeModule;

    invoke-direct {v4}, Lcom/facebook/base/app/ApplicationLikeModule;-><init>()V

    invoke-virtual {v3, v4}, Lcom/google/common/collect/ImmutableList$Builder;->b(Ljava/lang/Object;)Lcom/google/common/collect/ImmutableList$Builder;

    move-result-object v3

    new-instance v4, Lcom/facebook/inject/manifest/ManifestModuleLoaderModule;

    invoke-direct {v4, v1}, Lcom/facebook/inject/manifest/ManifestModuleLoaderModule;-><init>(Lcom/facebook/common/process/ProcessName;)V

    invoke-virtual {v3, v4}, Lcom/google/common/collect/ImmutableList$Builder;->b(Ljava/lang/Object;)Lcom/google/common/collect/ImmutableList$Builder;

    move-result-object v3

    invoke-virtual {v3, v2}, Lcom/google/common/collect/ImmutableList$Builder;->b(Ljava/lang/Iterable;)Lcom/google/common/collect/ImmutableList$Builder;

    move-result-object v2

    invoke-virtual {v2}, Lcom/google/common/collect/ImmutableList$Builder;->b()Lcom/google/common/collect/ImmutableList;

    move-result-object v2

    iget-object v3, p0, Lcom/facebook/base/app/AbstractApplicationLike;->e:Lcom/facebook/base/app/LightweightPerfEvents;

    const-string v4, "ColdStart/FBInjector.create"

    invoke-virtual {v3, v4}, Lcom/facebook/base/app/LightweightPerfEvents;->a(Ljava/lang/String;)V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    :try_start_1
    invoke-virtual {p0}, Lcom/facebook/base/app/AbstractApplicationLike;->a()Landroid/app/Application;

    move-result-object v3

    invoke-static {v3, v2}, Lcom/facebook/inject/FbInjector;->a(Landroid/content/Context;Ljava/util/List;)Lcom/facebook/inject/FbInjector;

    move-result-object v2

    iput-object v2, p0, Lcom/facebook/base/app/AbstractApplicationLike;->f:Lcom/facebook/inject/FbInjector;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    :try_start_2
    iget-object v2, p0, Lcom/facebook/base/app/AbstractApplicationLike;->e:Lcom/facebook/base/app/LightweightPerfEvents;

    const-string v3, "ColdStart/FBInjector.create"

    invoke-virtual {v2, v3}, Lcom/facebook/base/app/LightweightPerfEvents;->b(Ljava/lang/String;)V

    const/4 v2, 0x0

    iput-object v2, p0, Lcom/facebook/base/app/AbstractApplicationLike;->e:Lcom/facebook/base/app/LightweightPerfEvents;

    const-string v2, "ApplicationLike.onCreate#notifyAll"

    invoke-static {v2}, Lcom/facebook/debug/tracer/Tracer;->a(Ljava/lang/String;)V
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    :try_start_3
    invoke-virtual {p0}, Ljava/lang/Object;->notifyAll()V
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_2

    :try_start_4
    invoke-static {}, Lcom/facebook/debug/tracer/Tracer;->a()V

    monitor-exit p0
    :try_end_4
    .catchall {:try_start_4 .. :try_end_4} :catchall_1

    iget-object v2, p0, Lcom/facebook/base/app/AbstractApplicationLike;->f:Lcom/facebook/inject/FbInjector;

    const-class v3, Lcom/facebook/base/app/AbstractApplicationLike;

    invoke-virtual {v2, v3, p0}, Lcom/facebook/inject/FbInjector;->a(Ljava/lang/Class;Ljava/lang/Object;)V

    invoke-virtual {p0, v1}, Lcom/facebook/base/app/AbstractApplicationLike;->d(Lcom/facebook/common/process/ProcessName;)V

    iget-object v1, p0, Lcom/facebook/base/app/AbstractApplicationLike;->g:Lcom/facebook/common/init/impl/FbAppInitializer;

    invoke-virtual {v1}, Lcom/facebook/common/init/impl/FbAppInitializer;->a()Lcom/google/common/util/concurrent/ListenableFuture;

    move-result-object v1

    iget-object v2, p0, Lcom/facebook/base/app/AbstractApplicationLike;->h:Lcom/facebook/debug/activitytracer/ActivityTracer;

    const-string v3, "cold_start"

    invoke-virtual {v2, v0, v3}, Lcom/facebook/debug/activitytracer/ActivityTracer;->a(Lcom/facebook/debug/tracer/Tracer;Ljava/lang/String;)Lcom/facebook/debug/activitytracer/ActivityTrace;

    iget-object v0, p0, Lcom/facebook/base/app/AbstractApplicationLike;->h:Lcom/facebook/debug/activitytracer/ActivityTracer;

    invoke-virtual {v0}, Lcom/facebook/debug/activitytracer/ActivityTracer;->a()Lcom/facebook/debug/activitytracer/ActivityTrace;

    move-result-object v0

    if-eqz v0, :cond_1

    sget-object v2, Lcom/facebook/debug/activitytracer/AppStartupType;->COLD_START:Lcom/facebook/debug/activitytracer/AppStartupType;

    invoke-virtual {v0, v2}, Lcom/facebook/debug/activitytracer/ActivityTrace;->a(Lcom/facebook/debug/activitytracer/AppStartupType;)V

    :cond_1
    new-instance v0, Lcom/facebook/base/app/AbstractApplicationLike$1;

    invoke-direct {v0, p0}, Lcom/facebook/base/app/AbstractApplicationLike$1;-><init>(Lcom/facebook/base/app/AbstractApplicationLike;)V

    iget-object v2, p0, Lcom/facebook/base/app/AbstractApplicationLike;->i:Ljava/util/concurrent/Executor;

    invoke-static {v1, v0, v2}, Lcom/google/common/util/concurrent/Futures;->a(Lcom/google/common/util/concurrent/ListenableFuture;Lcom/google/common/util/concurrent/FutureCallback;Ljava/util/concurrent/Executor;)V

    return-void

    :catchall_0
    move-exception v0

    :try_start_5
    iget-object v1, p0, Lcom/facebook/base/app/AbstractApplicationLike;->e:Lcom/facebook/base/app/LightweightPerfEvents;

    const-string v2, "ColdStart/FBInjector.create"

    invoke-virtual {v1, v2}, Lcom/facebook/base/app/LightweightPerfEvents;->b(Ljava/lang/String;)V

    const/4 v1, 0x0

    iput-object v1, p0, Lcom/facebook/base/app/AbstractApplicationLike;->e:Lcom/facebook/base/app/LightweightPerfEvents;

    throw v0
    :try_end_5
    .catchall {:try_start_5 .. :try_end_5} :catchall_1

    :catchall_1
    move-exception v0

    monitor-exit p0

    throw v0

    :catchall_2
    move-exception v0

    :try_start_6
    invoke-static {}, Lcom/facebook/debug/tracer/Tracer;->a()V

    throw v0
    :try_end_6
    .catchall {:try_start_6 .. :try_end_6} :catchall_1
.end method

.method protected d()V
    .locals 0

    return-void
.end method

.method protected d(Lcom/facebook/common/process/ProcessName;)V
    .locals 0

    return-void
.end method

.method public final e()V
    .locals 1

    iget-object v0, p0, Lcom/facebook/base/app/AbstractApplicationLike;->k:Lcom/facebook/common/memory/MemoryManager;

    invoke-virtual {v0}, Lcom/facebook/common/memory/MemoryManager;->b()V

    return-void
.end method

.method public final declared-synchronized f()Lcom/facebook/inject/FbInjector;
    .locals 2

    monitor-enter p0

    :goto_0
    :try_start_0
    iget-object v0, p0, Lcom/facebook/base/app/AbstractApplicationLike;->f:Lcom/facebook/inject/FbInjector;

    if-nez v0, :cond_0

    invoke-virtual {p0}, Ljava/lang/Object;->wait()V
    :try_end_0
    .catch Ljava/lang/InterruptedException; {:try_start_0 .. :try_end_0} :catch_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    goto :goto_0

    :catch_0
    move-exception v0

    :try_start_1
    new-instance v1, Ljava/lang/RuntimeException;

    invoke-direct {v1, v0}, Ljava/lang/RuntimeException;-><init>(Ljava/lang/Throwable;)V

    throw v1
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0

    :cond_0
    :try_start_2
    iget-object v0, p0, Lcom/facebook/base/app/AbstractApplicationLike;->f:Lcom/facebook/inject/FbInjector;
    :try_end_2
    .catch Ljava/lang/InterruptedException; {:try_start_2 .. :try_end_2} :catch_0
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    monitor-exit p0

    return-object v0
.end method

.method public final g()Landroid/content/res/Resources;
    .locals 1

    iget-object v0, p0, Lcom/facebook/base/app/AbstractApplicationLike;->j:Lcom/facebook/resources/FbResources;

    return-object v0
.end method
