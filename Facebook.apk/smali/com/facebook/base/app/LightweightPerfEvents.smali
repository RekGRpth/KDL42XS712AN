.class public Lcom/facebook/base/app/LightweightPerfEvents;
.super Ljava/lang/Object;
.source "LightweightPerfEvents.java"


# instance fields
.field private final a:Ljava/util/Stack;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Stack",
            "<",
            "Lcom/facebook/base/app/LightweightPerfEvents$Event;",
            ">;"
        }
    .end annotation
.end field

.field private final b:Ljava/util/Map;
    .annotation build Landroid/annotation/SuppressLint;
        value = {
            "BadMethodUse-java.util.HashMap._Constructor"
        }
    .end annotation

    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Map",
            "<",
            "Ljava/lang/String;",
            "Lcom/facebook/base/app/LightweightPerfEvents$Event;",
            ">;"
        }
    .end annotation
.end field

.field private final c:Ljava/util/List;
    .annotation build Landroid/annotation/SuppressLint;
        value = {
            "BadMethodUse-java.util.ArrayList._Constructor"
        }
    .end annotation

    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Lcom/facebook/base/app/LightweightPerfEvents$Event;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method public constructor <init>()V
    .locals 2

    const/16 v1, 0x8

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    new-instance v0, Ljava/util/Stack;

    invoke-direct {v0}, Ljava/util/Stack;-><init>()V

    iput-object v0, p0, Lcom/facebook/base/app/LightweightPerfEvents;->a:Ljava/util/Stack;

    new-instance v0, Ljava/util/HashMap;

    invoke-direct {v0, v1}, Ljava/util/HashMap;-><init>(I)V

    iput-object v0, p0, Lcom/facebook/base/app/LightweightPerfEvents;->b:Ljava/util/Map;

    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0, v1}, Ljava/util/ArrayList;-><init>(I)V

    iput-object v0, p0, Lcom/facebook/base/app/LightweightPerfEvents;->c:Ljava/util/List;

    return-void
.end method

.method private static a(Lcom/facebook/base/app/LightweightPerfEvents$Event;)V
    .locals 2

    invoke-static {}, Landroid/os/SystemClock;->elapsedRealtime()J

    move-result-wide v0

    iput-wide v0, p0, Lcom/facebook/base/app/LightweightPerfEvents$Event;->c:J

    return-void
.end method

.method private e(Ljava/lang/String;)Lcom/facebook/base/app/LightweightPerfEvents$Event;
    .locals 3

    new-instance v0, Lcom/facebook/base/app/LightweightPerfEvents$Event;

    const/4 v1, 0x0

    invoke-direct {v0, v1}, Lcom/facebook/base/app/LightweightPerfEvents$Event;-><init>(B)V

    iput-object p1, v0, Lcom/facebook/base/app/LightweightPerfEvents$Event;->a:Ljava/lang/String;

    invoke-static {}, Landroid/os/SystemClock;->elapsedRealtime()J

    move-result-wide v1

    iput-wide v1, v0, Lcom/facebook/base/app/LightweightPerfEvents$Event;->b:J

    iget-object v1, p0, Lcom/facebook/base/app/LightweightPerfEvents;->c:Ljava/util/List;

    invoke-interface {v1, v0}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    return-object v0
.end method


# virtual methods
.method public final a(Lcom/facebook/base/app/LightweightPerfEvents$Marker;)V
    .locals 7

    iget-object v0, p0, Lcom/facebook/base/app/LightweightPerfEvents;->c:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v6

    :goto_0
    invoke-interface {v6}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-interface {v6}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/base/app/LightweightPerfEvents$Event;

    iget-object v1, v0, Lcom/facebook/base/app/LightweightPerfEvents$Event;->a:Ljava/lang/String;

    iget-wide v2, v0, Lcom/facebook/base/app/LightweightPerfEvents$Event;->b:J

    iget-wide v4, v0, Lcom/facebook/base/app/LightweightPerfEvents$Event;->c:J

    move-object v0, p1

    invoke-interface/range {v0 .. v5}, Lcom/facebook/base/app/LightweightPerfEvents$Marker;->a(Ljava/lang/String;JJ)V

    goto :goto_0

    :cond_0
    iget-object v0, p0, Lcom/facebook/base/app/LightweightPerfEvents;->c:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->clear()V

    return-void
.end method

.method public final a(Ljava/lang/String;)V
    .locals 2

    invoke-direct {p0, p1}, Lcom/facebook/base/app/LightweightPerfEvents;->e(Ljava/lang/String;)Lcom/facebook/base/app/LightweightPerfEvents$Event;

    move-result-object v0

    iget-object v1, p0, Lcom/facebook/base/app/LightweightPerfEvents;->a:Ljava/util/Stack;

    invoke-virtual {v1, v0}, Ljava/util/Stack;->push(Ljava/lang/Object;)Ljava/lang/Object;

    return-void
.end method

.method public final b(Ljava/lang/String;)V
    .locals 5

    iget-object v0, p0, Lcom/facebook/base/app/LightweightPerfEvents;->a:Ljava/util/Stack;

    invoke-virtual {v0}, Ljava/util/Stack;->pop()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/base/app/LightweightPerfEvents$Event;

    iget-object v1, v0, Lcom/facebook/base/app/LightweightPerfEvents$Event;->a:Ljava/lang/String;

    invoke-virtual {v1, p1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-nez v1, :cond_0

    new-instance v1, Ljava/lang/IllegalStateException;

    const-string v2, "Unbalanced LightweightPerfEvents.stop(). Expected: %s Actual: %s"

    const/4 v3, 0x2

    new-array v3, v3, [Ljava/lang/Object;

    const/4 v4, 0x0

    iget-object v0, v0, Lcom/facebook/base/app/LightweightPerfEvents$Event;->a:Ljava/lang/String;

    aput-object v0, v3, v4

    const/4 v0, 0x1

    aput-object p1, v3, v0

    invoke-static {v2, v3}, Lcom/facebook/common/util/StringLocaleUtil;->a(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    invoke-direct {v1, v0}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    throw v1

    :cond_0
    invoke-static {v0}, Lcom/facebook/base/app/LightweightPerfEvents;->a(Lcom/facebook/base/app/LightweightPerfEvents$Event;)V

    return-void
.end method

.method public final c(Ljava/lang/String;)V
    .locals 2

    iget-object v0, p0, Lcom/facebook/base/app/LightweightPerfEvents;->b:Ljava/util/Map;

    invoke-interface {v0, p1}, Ljava/util/Map;->containsKey(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    new-instance v0, Ljava/lang/IllegalArgumentException;

    invoke-direct {v0}, Ljava/lang/IllegalArgumentException;-><init>()V

    throw v0

    :cond_0
    invoke-direct {p0, p1}, Lcom/facebook/base/app/LightweightPerfEvents;->e(Ljava/lang/String;)Lcom/facebook/base/app/LightweightPerfEvents$Event;

    move-result-object v0

    iget-object v1, p0, Lcom/facebook/base/app/LightweightPerfEvents;->b:Ljava/util/Map;

    invoke-interface {v1, p1, v0}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    return-void
.end method

.method public final d(Ljava/lang/String;)V
    .locals 4

    iget-object v0, p0, Lcom/facebook/base/app/LightweightPerfEvents;->b:Ljava/util/Map;

    invoke-interface {v0, p1}, Ljava/util/Map;->remove(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/base/app/LightweightPerfEvents$Event;

    if-nez v0, :cond_0

    new-instance v0, Ljava/lang/IllegalStateException;

    const-string v1, "LightweightPerfEvents.stopAsync() called for %s without first calling startAsync()."

    const/4 v2, 0x1

    new-array v2, v2, [Ljava/lang/Object;

    const/4 v3, 0x0

    aput-object p1, v2, v3

    invoke-static {v1, v2}, Lcom/facebook/common/util/StringLocaleUtil;->a(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    throw v0

    :cond_0
    invoke-static {v0}, Lcom/facebook/base/app/LightweightPerfEvents;->a(Lcom/facebook/base/app/LightweightPerfEvents$Event;)V

    return-void
.end method
