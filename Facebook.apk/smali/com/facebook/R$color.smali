.class public Lcom/facebook/R$color;
.super Ljava/lang/Object;
.source "R.java"


# static fields
.field public static final QPButtonLinesColor:I = 0x7f0b02ec

.field public static final QPInterstitialPrimaryButtonTextColor:I = 0x7f0b02ed

.field public static final abc_search_url_text_holo:I = 0x7f0b04c7

.field public static final abc_search_url_text_normal:I = 0x7f0b0034

.field public static final abc_search_url_text_pressed:I = 0x7f0b0036

.field public static final abc_search_url_text_selected:I = 0x7f0b0035

.field public static final action_bar_text:I = 0x7f0b04c8

.field public static final active_blue:I = 0x7f0b002c

.field public static final add_credit_card_error_text_color:I = 0x7f0b03cd

.field public static final add_credit_card_text_color:I = 0x7f0b03ce

.field public static final admin_message_background:I = 0x7f0b0192

.field public static final album_add_photos_background:I = 0x7f0b0483

.field public static final album_add_photos_background_pressed:I = 0x7f0b0484

.field public static final album_add_photos_text:I = 0x7f0b0482

.field public static final album_delimiter:I = 0x7f0b0481

.field public static final album_edit_add_contributor_background:I = 0x7f0b0487

.field public static final album_edit_add_contributor_background_pressed:I = 0x7f0b0488

.field public static final album_edit_blue_titlebar_complete_text_color:I = 0x7f0b024a

.field public static final album_edit_blue_titlebar_dividercolor:I = 0x7f0b0249

.field public static final album_edit_blue_titlebar_pressed_color:I = 0x7f0b024b

.field public static final album_edit_content_black:I = 0x7f0b027f

.field public static final album_edit_ineditable_gray_text_color:I = 0x7f0b0280

.field public static final album_edit_section_title_text_color:I = 0x7f0b027e

.field public static final album_edit_thick_divider_color:I = 0x7f0b0281

.field public static final album_edit_thin_divider_color:I = 0x7f0b0282

.field public static final album_list_album_name_color:I = 0x7f0b027b

.field public static final album_list_album_name_shadow_color:I = 0x7f0b027c

.field public static final album_permalink_feedback_text:I = 0x7f0b0486

.field public static final album_permalink_thumbnail_pressed_state:I = 0x7f0b027d

.field public static final album_permalink_ufi_separator:I = 0x7f0b0485

.field public static final album_photo_count_grey:I = 0x7f0b0279

.field public static final album_primary_text:I = 0x7f0b0480

.field public static final album_secondary_text:I = 0x7f0b047f

.field public static final album_selector_background:I = 0x7f0b028b

.field public static final album_selector_text_color:I = 0x7f0b028c

.field public static final albums_list_divider_color:I = 0x7f0b047e

.field public static final audience_picker_button_bar_bg_color:I = 0x7f0b0245

.field public static final audience_picker_button_bar_pressed_color:I = 0x7f0b0246

.field public static final audio_compose_window_gray:I = 0x7f0b016c

.field public static final audio_compose_window_red:I = 0x7f0b016b

.field public static final avatar_placeholder_color:I = 0x7f0b0018

.field public static final background:I = 0x7f0b0445

.field public static final background_facebookholo_dark:I = 0x7f0b0040

.field public static final background_facebookholo_light:I = 0x7f0b0041

.field public static final background_selected_gray:I = 0x7f0b00e2

.field public static final bar_chart_bar_color:I = 0x7f0b00b5

.field public static final bar_chart_label_color:I = 0x7f0b00b6

.field public static final base_button_text_color:I = 0x7f0b04c9

.field public static final black:I = 0x7f0b0003

.field public static final black25a:I = 0x7f0b0001

.field public static final black50a:I = 0x7f0b0002

.field public static final black70a:I = 0x7f0b0000

.field public static final black_alpha_20:I = 0x7f0b0132

.field public static final black_alpha_48:I = 0x7f0b0133

.field public static final black_alpha_67:I = 0x7f0b0134

.field public static final black_partial_transparent:I = 0x7f0b01ba

.field public static final blue_button:I = 0x7f0b03e2

.field public static final blue_button_pressed:I = 0x7f0b03e3

.field public static final blue_button_selected:I = 0x7f0b03e4

.field public static final blue_tab_default:I = 0x7f0b0439

.field public static final blue_tab_pressed:I = 0x7f0b043b

.field public static final blue_tab_selected:I = 0x7f0b043a

.field public static final bookmark_background:I = 0x7f0b0420

.field public static final bookmark_background_transparent:I = 0x7f0b0421

.field public static final bookmark_divider_thin:I = 0x7f0b02f0

.field public static final bookmark_item_count_color:I = 0x7f0b02f1

.field public static final bookmark_item_highlight:I = 0x7f0b02ef

.field public static final bookmark_menu_button_bottom_highlight:I = 0x7f0b0426

.field public static final bookmark_menu_button_top_highlight:I = 0x7f0b0425

.field public static final bookmark_rounded_corners_color:I = 0x7f0b02d3

.field public static final bookmark_tab_background_dragdrop:I = 0x7f0b0422

.field public static final bookmark_tab_divider_item_text:I = 0x7f0b02f3

.field public static final bookmark_tab_divider_thin:I = 0x7f0b02f4

.field public static final bookmark_tab_item_bg_highlight:I = 0x7f0b02f7

.field public static final bookmark_tab_item_count_color:I = 0x7f0b02f5

.field public static final bookmark_tab_item_text:I = 0x7f0b02f2

.field public static final bookmark_tab_profile_item_bg_color:I = 0x7f0b02f6

.field public static final bookmark_text:I = 0x7f0b02ee

.field public static final bookmark_text_background:I = 0x7f0b0424

.field public static final bookmark_text_hide:I = 0x7f0b0427

.field public static final bookmark_text_transparent:I = 0x7f0b0423

.field public static final bottom_bar_grey:I = 0x7f0b0217

.field public static final bottom_bar_grey_pressed:I = 0x7f0b0218

.field public static final bright_foreground_disabled_facebookholo_dark:I = 0x7f0b0044

.field public static final bright_foreground_disabled_facebookholo_light:I = 0x7f0b0045

.field public static final bright_foreground_facebookholo_dark:I = 0x7f0b0042

.field public static final bright_foreground_facebookholo_light:I = 0x7f0b0043

.field public static final bright_foreground_inverse_facebookholo_dark:I = 0x7f0b0046

.field public static final bright_foreground_inverse_facebookholo_light:I = 0x7f0b0047

.field public static final bright_red_warning_color:I = 0x7f0b013b

.field public static final broadcast_button_text_color:I = 0x7f0b0194

.field public static final broadcast_request_action_link_bg:I = 0x7f0b0370

.field public static final broadcast_request_action_link_pressed_bg:I = 0x7f0b0371

.field public static final broken_white:I = 0x7f0b0156

.field public static final browser_chrome_subtitle_text_color:I = 0x7f0b049b

.field public static final browser_chrome_title_text_color:I = 0x7f0b049a

.field public static final browser_loading_bar_color:I = 0x7f0b0499

.field public static final browser_menu_divider_color:I = 0x7f0b049d

.field public static final browser_pivots_background_color:I = 0x7f0b049f

.field public static final browser_pivots_divider_color:I = 0x7f0b04a0

.field public static final browser_pivots_drag_handle_color:I = 0x7f0b04a1

.field public static final browser_pivots_drag_handle_press_color:I = 0x7f0b04a2

.field public static final browser_pivots_image_background_color:I = 0x7f0b04a3

.field public static final browser_pivots_main_text_color:I = 0x7f0b04a4

.field public static final browser_pivots_shadow_end:I = 0x7f0b04ac

.field public static final browser_pivots_shadow_start:I = 0x7f0b04ab

.field public static final browser_pivots_show_button_background:I = 0x7f0b04a5

.field public static final browser_pivots_show_button_text_color:I = 0x7f0b04a6

.field public static final browser_pivots_sub_text_color:I = 0x7f0b04a7

.field public static final browser_pivots_top_border_color:I = 0x7f0b04a8

.field public static final bubble_dark_background:I = 0x7f0b0414

.field public static final camera_borders_and_dividers:I = 0x7f0b020d

.field public static final camera_crop_overlay:I = 0x7f0b026f

.field public static final camera_gray_bar:I = 0x7f0b020b

.field public static final camera_menu_button_down_state:I = 0x7f0b026d

.field public static final camera_review_typeahead_suggests:I = 0x7f0b020c

.field public static final camera_shutter:I = 0x7f0b0270

.field public static final camera_typeahead_selection_color:I = 0x7f0b02cf

.field public static final card_front_category_text_color:I = 0x7f0b04ca

.field public static final card_front_category_text_color_selected:I = 0x7f0b03ba

.field public static final carrier_alert_background_color:I = 0x7f0b04c6

.field public static final carrier_banner_end_color:I = 0x7f0b04c4

.field public static final carrier_banner_start_color:I = 0x7f0b04c3

.field public static final carrier_blue_text_color:I = 0x7f0b04c5

.field public static final carrier_red_color:I = 0x7f0b04c1

.field public static final carrier_row_divider_color:I = 0x7f0b04c2

.field public static final cell_divider_color:I = 0x7f0b03c7

.field public static final chambray_text_red:I = 0x7f0b0351

.field public static final chat_heads_nux_bubble_shadow:I = 0x7f0b017f

.field public static final chat_heads_popup_background:I = 0x7f0b017e

.field public static final chat_heads_row_separator:I = 0x7f0b0188

.field public static final chat_thread_composer_background:I = 0x7f0b0184

.field public static final chat_thread_composer_text_color:I = 0x7f0b0186

.field public static final chat_thread_composer_text_hint_color:I = 0x7f0b0187

.field public static final chat_thread_view_divider:I = 0x7f0b0185

.field public static final chat_thread_view_message_background:I = 0x7f0b0183

.field public static final checkbox_gradient_bottom:I = 0x7f0b0321

.field public static final checkbox_gradient_inner_shadow:I = 0x7f0b0322

.field public static final checkbox_gradient_stroke:I = 0x7f0b0323

.field public static final checkbox_gradient_top:I = 0x7f0b0320

.field public static final checkbox_selector:I = 0x7f0b031f

.field public static final clear:I = 0x7f0b0015

.field public static final click_background:I = 0x7f0b049c

.field public static final clickable_span_highlight_color:I = 0x7f0b0350

.field public static final collection_about_item_title:I = 0x7f0b04bc

.field public static final collection_grid_item_label:I = 0x7f0b04b7

.field public static final collection_grid_item_label_shadow:I = 0x7f0b04b8

.field public static final collection_item_image_background_color_angora:I = 0x7f0b04c0

.field public static final collection_item_subtitle:I = 0x7f0b04bb

.field public static final collection_item_title:I = 0x7f0b04ba

.field public static final collection_no_data_text:I = 0x7f0b04bf

.field public static final collection_recently_added_item_background_color:I = 0x7f0b04bd

.field public static final collection_suggestions_background_color:I = 0x7f0b04be

.field public static final collections_frame:I = 0x7f0b032d

.field public static final collections_frame_pressed:I = 0x7f0b032e

.field public static final collections_notes_title:I = 0x7f0b04b9

.field public static final comment_background:I = 0x7f0b01bb

.field public static final commenter_divider:I = 0x7f0b01bc

.field public static final commenter_send_button:I = 0x7f0b01bd

.field public static final commenter_send_button_disabled:I = 0x7f0b01be

.field public static final common_action_bar_splitter:I = 0x7f0b0225

.field public static final common_signin_btn_dark_text_default:I = 0x7f0b021c

.field public static final common_signin_btn_dark_text_disabled:I = 0x7f0b021e

.field public static final common_signin_btn_dark_text_focused:I = 0x7f0b021f

.field public static final common_signin_btn_dark_text_pressed:I = 0x7f0b021d

.field public static final common_signin_btn_default_background:I = 0x7f0b0224

.field public static final common_signin_btn_light_text_default:I = 0x7f0b0220

.field public static final common_signin_btn_light_text_disabled:I = 0x7f0b0222

.field public static final common_signin_btn_light_text_focused:I = 0x7f0b0223

.field public static final common_signin_btn_light_text_pressed:I = 0x7f0b0221

.field public static final common_signin_btn_text_dark:I = 0x7f0b04cb

.field public static final common_signin_btn_text_light:I = 0x7f0b04cc

.field public static final compose_button_chat_heads_disabled:I = 0x7f0b0180

.field public static final compose_button_chat_heads_enabled:I = 0x7f0b0181

.field public static final composer_background:I = 0x7f0b0157

.field public static final composer_button_shadow:I = 0x7f0b0158

.field public static final composer_title_bar_text_color:I = 0x7f0b0244

.field public static final confirmation_background:I = 0x7f0b00ae

.field public static final confirmation_divider:I = 0x7f0b00af

.field public static final confirmation_negative_text_color:I = 0x7f0b00b1

.field public static final confirmation_text_color:I = 0x7f0b00b0

.field public static final connection_status_captive_portal:I = 0x7f0b019f

.field public static final connection_status_connected:I = 0x7f0b019e

.field public static final connection_status_connecting:I = 0x7f0b019d

.field public static final connection_status_no_internet:I = 0x7f0b019b

.field public static final connection_status_waiting_to_connect:I = 0x7f0b019c

.field public static final contact_sync_header_background_color:I = 0x7f0b042e

.field public static final contact_sync_header_text_color:I = 0x7f0b042d

.field public static final context_items_row_border:I = 0x7f0b032b

.field public static final context_items_row_color:I = 0x7f0b032a

.field public static final control_title_bar_primary_named_button_divider:I = 0x7f0b0247

.field public static final coupon_requests_responded:I = 0x7f0b01bf

.field public static final crop_blackout_color:I = 0x7f0b02d1

.field public static final crop_corner_color:I = 0x7f0b02d0

.field public static final crop_title_bar_grey:I = 0x7f0b02d4

.field public static final crop_title_bar_grey_pressed:I = 0x7f0b02d5

.field public static final dark_gray:I = 0x7f0b0438

.field public static final dark_gray_text_color:I = 0x7f0b026e

.field public static final dark_green:I = 0x7f0b0430

.field public static final dark_grey:I = 0x7f0b012b

.field public static final dark_orange_focused_color:I = 0x7f0b0138

.field public static final dark_orange_pressed_color:I = 0x7f0b0139

.field public static final dark_orca_blue:I = 0x7f0b0142

.field public static final dark_red:I = 0x7f0b042f

.field public static final dark_yellow:I = 0x7f0b0431

.field public static final dash_status_card_gray:I = 0x7f0b002a

.field public static final dash_status_card_separator_bar_gray:I = 0x7f0b002b

.field public static final date_text_color:I = 0x7f0b00b8

.field public static final dbl_blue_background:I = 0x7f0b03dc

.field public static final dbl_dark_text:I = 0x7f0b03d7

.field public static final dbl_divider_thin:I = 0x7f0b03d9

.field public static final dbl_light_blue:I = 0x7f0b03db

.field public static final dbl_light_text:I = 0x7f0b03d8

.field public static final dbl_nux_divider:I = 0x7f0b03da

.field public static final default_banner_background:I = 0x7f0b01a0

.field public static final default_circle_indicator_fill_color:I = 0x7f0b02c7

.field public static final default_circle_indicator_page_color:I = 0x7f0b02c8

.field public static final default_circle_indicator_stroke_color:I = 0x7f0b02c9

.field public static final default_contacts_contact_background:I = 0x7f0b0170

.field public static final default_contacts_contact_name_text:I = 0x7f0b0171

.field public static final default_contacts_contact_status_text:I = 0x7f0b0172

.field public static final default_contacts_section_background:I = 0x7f0b016e

.field public static final default_contacts_section_text:I = 0x7f0b016f

.field public static final default_indicator_active_color:I = 0x7f0b00ac

.field public static final default_indicator_inactive_color:I = 0x7f0b00ad

.field public static final default_list_bg_color:I = 0x7f0b0292

.field public static final default_overlay_color:I = 0x7f0b02d2

.field public static final default_separator_color:I = 0x7f0b0010

.field public static final destructive_red:I = 0x7f0b002d

.field public static final dialog_button_divider_left:I = 0x7f0b0234

.field public static final dialog_button_divider_right:I = 0x7f0b0235

.field public static final dialog_category_gradient_bottom:I = 0x7f0b023b

.field public static final dialog_category_gradient_top:I = 0x7f0b023a

.field public static final dialog_category_text:I = 0x7f0b0239

.field public static final dialog_checkbox_gradient_bottom:I = 0x7f0b023e

.field public static final dialog_checkbox_gradient_inner_shadow:I = 0x7f0b023f

.field public static final dialog_checkbox_gradient_stroke:I = 0x7f0b0240

.field public static final dialog_checkbox_gradient_top:I = 0x7f0b023d

.field public static final dialog_dialog_background:I = 0x7f0b0242

.field public static final dialog_panel_background:I = 0x7f0b023c

.field public static final dialog_panel_background_stroke:I = 0x7f0b0243

.field public static final dialog_selector:I = 0x7f0b0238

.field public static final dialog_separator_bottom:I = 0x7f0b0237

.field public static final dialog_separator_top:I = 0x7f0b0236

.field public static final dialog_text_color:I = 0x7f0b0413

.field public static final dialog_text_shadow:I = 0x7f0b0241

.field public static final dialog_white_text_shadow_color:I = 0x7f0b0233

.field public static final dim_foreground_disabled_facebookholo_dark:I = 0x7f0b0049

.field public static final dim_foreground_disabled_facebookholo_light:I = 0x7f0b004e

.field public static final dim_foreground_facebookholo_dark:I = 0x7f0b0048

.field public static final dim_foreground_facebookholo_light:I = 0x7f0b004d

.field public static final dim_foreground_inverse_disabled_facebookholo_dark:I = 0x7f0b004b

.field public static final dim_foreground_inverse_disabled_facebookholo_light:I = 0x7f0b0050

.field public static final dim_foreground_inverse_facebookholo_dark:I = 0x7f0b004a

.field public static final dim_foreground_inverse_facebookholo_light:I = 0x7f0b004f

.field public static final disabled_overlay_color:I = 0x7f0b001d

.field public static final dive_head_last_msg_time_color:I = 0x7f0b0189

.field public static final divebar_background:I = 0x7f0b00ec

.field public static final divebar_contact_background:I = 0x7f0b0175

.field public static final divebar_contact_background_pressed:I = 0x7f0b0176

.field public static final divebar_contact_divider:I = 0x7f0b0179

.field public static final divebar_contact_name_text:I = 0x7f0b0177

.field public static final divebar_contact_status_text:I = 0x7f0b0178

.field public static final divebar_header_separator:I = 0x7f0b017b

.field public static final divebar_header_text:I = 0x7f0b017a

.field public static final divebar_importing_header_text:I = 0x7f0b017c

.field public static final divebar_new_message_row_backgroup:I = 0x7f0b017d

.field public static final divebar_section_background:I = 0x7f0b0173

.field public static final divebar_section_text:I = 0x7f0b0174

.field public static final divebar_text:I = 0x7f0b00ed

.field public static final divider:I = 0x7f0b0446

.field public static final divider_bg:I = 0x7f0b0285

.field public static final drawers_default_background:I = 0x7f0b02ce

.field public static final drawers_main_content_fallback_background:I = 0x7f0b02cd

.field public static final duplicates_bg_color:I = 0x7f0b0444

.field public static final edit_coverphoto_headerdisable:I = 0x7f0b034f

.field public static final eight_percent_alpha_white:I = 0x7f0b0029

.field public static final eighty_percent_alpha_black:I = 0x7f0b001e

.field public static final emoji_attachment_popup_background_color:I = 0x7f0b0169

.field public static final emoji_attachment_popup_background_color_neue:I = 0x7f0b016d

.field public static final emoji_selector_classic_background:I = 0x7f0b0153

.field public static final emoji_selector_classic_gray:I = 0x7f0b0154

.field public static final emoji_view_pager_indicator_unselected_color:I = 0x7f0b016a

.field public static final entity_cards_fragment_bg:I = 0x7f0b0463

.field public static final error_message_details:I = 0x7f0b001a

.field public static final error_message_title:I = 0x7f0b0019

.field public static final error_shadow_color:I = 0x7f0b00d2

.field public static final error_text_color:I = 0x7f0b00d3

.field public static final event_action_bar_disable_cover_view_background:I = 0x7f0b0450

.field public static final event_action_button_text_normal:I = 0x7f0b044f

.field public static final event_actionlink_text:I = 0x7f0b044d

.field public static final event_actionlink_text_disabled:I = 0x7f0b044e

.field public static final event_cover_photo_border:I = 0x7f0b0449

.field public static final event_create_section_blue:I = 0x7f0b0448

.field public static final event_darken_pressed_state:I = 0x7f0b044a

.field public static final event_dashboard_profile_placeholder_color:I = 0x7f0b045c

.field public static final event_date_title_text_shadow_color:I = 0x7f0b0452

.field public static final event_feed_list_item_bg_color:I = 0x7f0b044b

.field public static final event_feed_list_item_bg_pressed_state:I = 0x7f0b044c

.field public static final event_guestlist_segmented_control_bg_color:I = 0x7f0b045a

.field public static final event_guestlist_segmented_control_divider_color:I = 0x7f0b045b

.field public static final event_join_button_divider_color:I = 0x7f0b035c

.field public static final event_join_icon_text_color:I = 0x7f0b035b

.field public static final event_permalink_card_background_pressed:I = 0x7f0b0458

.field public static final event_permalink_card_divider_color:I = 0x7f0b0453

.field public static final event_permalink_details_text_gradient_default_color:I = 0x7f0b0457

.field public static final event_title_text_shadow_color:I = 0x7f0b0451

.field public static final events_card_month:I = 0x7f0b035a

.field public static final events_dasbhoard_separator_border:I = 0x7f0b045d

.field public static final events_subtext_color:I = 0x7f0b041d

.field public static final events_suggestions_overlay:I = 0x7f0b045e

.field public static final events_title_color:I = 0x7f0b041c

.field public static final facebook_blue_twenty_percent_opacity:I = 0x7f0b0012

.field public static final facebook_grey:I = 0x7f0b0126

.field public static final facebook_greyish_blue:I = 0x7f0b0127

.field public static final facebook_light_blue:I = 0x7f0b0124

.field public static final facebook_segments_blue:I = 0x7f0b0125

.field public static final facebookholo_background_dark:I = 0x7f0b0038

.field public static final facebookholo_background_light:I = 0x7f0b0039

.field public static final facebookholo_blue_light:I = 0x7f0b0037

.field public static final facebookholo_bright_foreground_dark:I = 0x7f0b003a

.field public static final facebookholo_bright_foreground_dark_disabled:I = 0x7f0b003c

.field public static final facebookholo_bright_foreground_dark_inverse:I = 0x7f0b003e

.field public static final facebookholo_bright_foreground_light:I = 0x7f0b003b

.field public static final facebookholo_bright_foreground_light_disabled:I = 0x7f0b003d

.field public static final facebookholo_bright_foreground_light_inverse:I = 0x7f0b003f

.field public static final facebookholo_dim_foreground_dark:I = 0x7f0b0056

.field public static final facebookholo_dim_foreground_dark_inverse:I = 0x7f0b0057

.field public static final facepile_count_background_color:I = 0x7f0b00b2

.field public static final facepile_count_text_color:I = 0x7f0b00b3

.field public static final favorites_dragndrop_background:I = 0x7f0b018f

.field public static final fb_blue:I = 0x7f0b041a

.field public static final fb_blue_sdk:I = 0x7f0b041b

.field public static final fbui_accent_blue:I = 0x7f0b0059

.field public static final fbui_accent_blue_20:I = 0x7f0b006e

.field public static final fbui_accent_blue_50:I = 0x7f0b006d

.field public static final fbui_bg_dark:I = 0x7f0b0078

.field public static final fbui_bg_light:I = 0x7f0b007a

.field public static final fbui_bg_medium:I = 0x7f0b0079

.field public static final fbui_bluegrey_10:I = 0x7f0b0066

.field public static final fbui_bluegrey_2:I = 0x7f0b0068

.field public static final fbui_bluegrey_20:I = 0x7f0b0065

.field public static final fbui_bluegrey_30:I = 0x7f0b0064

.field public static final fbui_bluegrey_40:I = 0x7f0b0063

.field public static final fbui_bluegrey_5:I = 0x7f0b0067

.field public static final fbui_bluegrey_50:I = 0x7f0b0062

.field public static final fbui_bluegrey_60:I = 0x7f0b0061

.field public static final fbui_bluegrey_70:I = 0x7f0b0060

.field public static final fbui_bluegrey_80:I = 0x7f0b005f

.field public static final fbui_bluegrey_90:I = 0x7f0b005e

.field public static final fbui_border_dark:I = 0x7f0b007b

.field public static final fbui_border_inverse_dark:I = 0x7f0b007e

.field public static final fbui_border_light:I = 0x7f0b007d

.field public static final fbui_border_medium:I = 0x7f0b007c

.field public static final fbui_btn_dark_primary_bg_default:I = 0x7f0b009d

.field public static final fbui_btn_dark_primary_bg_pressed:I = 0x7f0b009c

.field public static final fbui_btn_dark_primary_stroke_disabled:I = 0x7f0b009f

.field public static final fbui_btn_dark_primary_stroke_enabled:I = 0x7f0b009e

.field public static final fbui_btn_dark_regular_bg_default:I = 0x7f0b0099

.field public static final fbui_btn_dark_regular_bg_pressed:I = 0x7f0b0098

.field public static final fbui_btn_dark_regular_stroke_disabled:I = 0x7f0b009b

.field public static final fbui_btn_dark_regular_stroke_enabled:I = 0x7f0b009a

.field public static final fbui_btn_dark_special_bg_disabled:I = 0x7f0b00a2

.field public static final fbui_btn_dark_special_bg_enabled:I = 0x7f0b00a1

.field public static final fbui_btn_dark_special_bg_pressed:I = 0x7f0b00a0

.field public static final fbui_btn_dark_text:I = 0x7f0b04cd

.field public static final fbui_btn_dark_text_disabled:I = 0x7f0b008b

.field public static final fbui_btn_dark_text_enabled:I = 0x7f0b008a

.field public static final fbui_btn_light_primary_bg_default:I = 0x7f0b0092

.field public static final fbui_btn_light_primary_bg_pressed:I = 0x7f0b0090

.field public static final fbui_btn_light_primary_stroke_disabled:I = 0x7f0b0094

.field public static final fbui_btn_light_primary_stroke_enabled:I = 0x7f0b0093

.field public static final fbui_btn_light_primary_stroke_pressed:I = 0x7f0b0091

.field public static final fbui_btn_light_primary_text:I = 0x7f0b04ce

.field public static final fbui_btn_light_primary_text_disabled:I = 0x7f0b0087

.field public static final fbui_btn_light_primary_text_enabled:I = 0x7f0b0086

.field public static final fbui_btn_light_regular_bg_default:I = 0x7f0b008e

.field public static final fbui_btn_light_regular_bg_pressed:I = 0x7f0b008c

.field public static final fbui_btn_light_regular_stroke_default:I = 0x7f0b008f

.field public static final fbui_btn_light_regular_stroke_pressed:I = 0x7f0b008d

.field public static final fbui_btn_light_regular_text:I = 0x7f0b04cf

.field public static final fbui_btn_light_regular_text_disabled:I = 0x7f0b0085

.field public static final fbui_btn_light_regular_text_enabled:I = 0x7f0b0084

.field public static final fbui_btn_light_special_bg_disabled:I = 0x7f0b0097

.field public static final fbui_btn_light_special_bg_enabled:I = 0x7f0b0096

.field public static final fbui_btn_light_special_bg_pressed:I = 0x7f0b0095

.field public static final fbui_btn_light_special_text:I = 0x7f0b04d0

.field public static final fbui_btn_light_special_text_disabled:I = 0x7f0b0089

.field public static final fbui_btn_light_special_text_enabled:I = 0x7f0b0088

.field public static final fbui_card_separator_gray:I = 0x7f0b0080

.field public static final fbui_check_disabled_dark:I = 0x7f0b00a3

.field public static final fbui_check_disabled_light:I = 0x7f0b00a4

.field public static final fbui_check_pressed_dark:I = 0x7f0b00a5

.field public static final fbui_check_pressed_light:I = 0x7f0b00a6

.field public static final fbui_divider:I = 0x7f0b007f

.field public static final fbui_facebook_blue:I = 0x7f0b0058

.field public static final fbui_green:I = 0x7f0b005b

.field public static final fbui_grey_30:I = 0x7f0b006c

.field public static final fbui_grey_40:I = 0x7f0b006b

.field public static final fbui_grey_50:I = 0x7f0b006a

.field public static final fbui_grey_80:I = 0x7f0b0069

.field public static final fbui_iab_text_grey:I = 0x7f0b032c

.field public static final fbui_light_blue:I = 0x7f0b005d

.field public static final fbui_list_item_bg:I = 0x7f0b0083

.field public static final fbui_popover_list_header:I = 0x7f0b00ab

.field public static final fbui_popover_list_item_view_description:I = 0x7f0b00aa

.field public static final fbui_popover_list_item_view_title:I = 0x7f0b00a9

.field public static final fbui_popover_list_view_divider:I = 0x7f0b00a7

.field public static final fbui_popover_list_view_selector:I = 0x7f0b00a8

.field public static final fbui_pressed_list_item_bg:I = 0x7f0b0081

.field public static final fbui_pressed_list_item_bg_opaque:I = 0x7f0b0082

.field public static final fbui_red:I = 0x7f0b005a

.field public static final fbui_tabbed_view_pager_indicator_text_color:I = 0x7f0b04d1

.field public static final fbui_text_dark:I = 0x7f0b0071

.field public static final fbui_text_inverse_dark:I = 0x7f0b0075

.field public static final fbui_text_inverse_light:I = 0x7f0b0077

.field public static final fbui_text_inverse_medium:I = 0x7f0b0076

.field public static final fbui_text_light:I = 0x7f0b0073

.field public static final fbui_text_link:I = 0x7f0b0074

.field public static final fbui_text_medium:I = 0x7f0b0072

.field public static final fbui_wash_mobile:I = 0x7f0b006f

.field public static final fbui_wash_mobile_60:I = 0x7f0b0070

.field public static final fbui_white:I = 0x7f0b005c

.field public static final feed_angora_attachment_event_month:I = 0x7f0b0203

.field public static final feed_app_ad_card_background_color:I = 0x7f0b01c0

.field public static final feed_app_ad_cta_divider_color:I = 0x7f0b01c1

.field public static final feed_app_ad_description_color:I = 0x7f0b01c2

.field public static final feed_app_ad_social_context_color:I = 0x7f0b01c3

.field public static final feed_attachment_context_color:I = 0x7f0b0033

.field public static final feed_attachment_title_color:I = 0x7f0b0032

.field public static final feed_cover_photo_overlay:I = 0x7f0b01c4

.field public static final feed_cover_photo_overlay_pressed:I = 0x7f0b01c5

.field public static final feed_cover_photo_placeholder:I = 0x7f0b01c6

.field public static final feed_cta_text_color:I = 0x7f0b01c7

.field public static final feed_digital_goods_item_border:I = 0x7f0b01c8

.field public static final feed_digital_goods_item_footer_background:I = 0x7f0b01c9

.field public static final feed_ego_unit_light_gray:I = 0x7f0b0367

.field public static final feed_feedback_action_button_bar_divider_color:I = 0x7f0b01ca

.field public static final feed_feedback_background_color:I = 0x7f0b01cb

.field public static final feed_feedback_background_pressed_start_color:I = 0x7f0b01cc

.field public static final feed_feedback_blue_text_color:I = 0x7f0b01cd

.field public static final feed_feedback_divider_color:I = 0x7f0b01ce

.field public static final feed_feedback_text_color:I = 0x7f0b01cf

.field public static final feed_flyout_background_color:I = 0x7f0b01d0

.field public static final feed_flyout_divider_color:I = 0x7f0b01d1

.field public static final feed_flyout_no_comments_text_view_color:I = 0x7f0b01d2

.field public static final feed_hidden_story_divider_color:I = 0x7f0b01d3

.field public static final feed_hidden_story_text_color:I = 0x7f0b01d4

.field public static final feed_list_item_bg_color:I = 0x7f0b0196

.field public static final feed_location_save_text_color:I = 0x7f0b01d5

.field public static final feed_new_stories_text_color:I = 0x7f0b01d6

.field public static final feed_popup_replybox_text:I = 0x7f0b0437

.field public static final feed_press_state_background_color:I = 0x7f0b01d7

.field public static final feed_press_state_solid_background_color:I = 0x7f0b01d8

.field public static final feed_saved_clickable_toast_confirmation_bg_color:I = 0x7f0b01d9

.field public static final feed_settings_additional_description:I = 0x7f0b0383

.field public static final feed_settings_background:I = 0x7f0b0380

.field public static final feed_settings_border:I = 0x7f0b0384

.field public static final feed_settings_border_bottom:I = 0x7f0b0386

.field public static final feed_settings_border_top:I = 0x7f0b0385

.field public static final feed_settings_description:I = 0x7f0b0382

.field public static final feed_settings_item_pressed:I = 0x7f0b0389

.field public static final feed_settings_list_background:I = 0x7f0b0388

.field public static final feed_settings_name:I = 0x7f0b0381

.field public static final feed_settings_profile_pic_loading:I = 0x7f0b038a

.field public static final feed_settings_title_text:I = 0x7f0b0387

.field public static final feed_sort_button_background_color:I = 0x7f0b01da

.field public static final feed_sort_button_unselected_color:I = 0x7f0b01db

.field public static final feed_sort_container_background:I = 0x7f0b01dc

.field public static final feed_sort_description_text_color:I = 0x7f0b01dd

.field public static final feed_sort_divider_color:I = 0x7f0b01de

.field public static final feed_sort_done_button_background_end:I = 0x7f0b01df

.field public static final feed_sort_done_button_background_start:I = 0x7f0b01e0

.field public static final feed_sort_done_button_text_color:I = 0x7f0b01e1

.field public static final feed_sort_heading_color:I = 0x7f0b01e2

.field public static final feed_store_card_front_subtext_dark:I = 0x7f0b02fd

.field public static final feed_store_card_front_subtext_light:I = 0x7f0b02fc

.field public static final feed_store_lines_back:I = 0x7f0b02f9

.field public static final feed_store_subtext:I = 0x7f0b02fb

.field public static final feed_store_text_back:I = 0x7f0b02fa

.field public static final feed_store_username_back:I = 0x7f0b02f8

.field public static final feed_story_attachment_background_color:I = 0x7f0b01e3

.field public static final feed_story_attachment_base_bg_color:I = 0x7f0b01e4

.field public static final feed_story_attachment_border_color:I = 0x7f0b01e5

.field public static final feed_story_attachment_feedback_divider_color:I = 0x7f0b01e6

.field public static final feed_story_blue_text_color:I = 0x7f0b01b0

.field public static final feed_story_bright_blue_like_text_color:I = 0x7f0b01e7

.field public static final feed_story_chaining_section_title_color:I = 0x7f0b01e8

.field public static final feed_story_coupon_white:I = 0x7f0b01e9

.field public static final feed_story_dark_gray_text_color:I = 0x7f0b002f

.field public static final feed_story_divider_color:I = 0x7f0b0031

.field public static final feed_story_freshness_background_new:I = 0x7f0b01ea

.field public static final feed_story_freshness_background_updated:I = 0x7f0b01eb

.field public static final feed_story_light_gray_text_color:I = 0x7f0b0030

.field public static final feed_story_link_text_color:I = 0x7f0b01b1

.field public static final feed_story_photo_placeholder_color:I = 0x7f0b01ec

.field public static final feed_story_secondary_actions_menu_divider:I = 0x7f0b01ed

.field public static final feed_subattachment_divider_color:I = 0x7f0b01ee

.field public static final feed_substory_divider_color:I = 0x7f0b01ef

.field public static final feed_substory_feedback_border_color:I = 0x7f0b01f0

.field public static final feed_text_body_color:I = 0x7f0b01f1

.field public static final feed_tombstone_text_color:I = 0x7f0b01f2

.field public static final fifty_percent_alpha_black:I = 0x7f0b001f

.field public static final fifty_percent_alpha_white:I = 0x7f0b0024

.field public static final fifty_percent_transparent_black:I = 0x7f0b00bb

.field public static final fifty_percent_transparent_white:I = 0x7f0b031e

.field public static final find_friends_add_friend_black:I = 0x7f0b03ec

.field public static final find_friends_add_friend_green:I = 0x7f0b03ed

.field public static final find_friends_chrome_border:I = 0x7f0b03e6

.field public static final find_friends_chrome_text:I = 0x7f0b03e7

.field public static final find_friends_dark_blue_text_color:I = 0x7f0b03e8

.field public static final find_friends_fb_blue:I = 0x7f0b03e5

.field public static final find_friends_footer_background_color:I = 0x7f0b03e9

.field public static final find_friends_link_color:I = 0x7f0b03eb

.field public static final find_friends_text_color:I = 0x7f0b0118

.field public static final find_friends_title_color:I = 0x7f0b03ea

.field public static final fitness_card_endpoint_circular_pin_inner_color:I = 0x7f0b036b

.field public static final fitness_card_route_line_border_color:I = 0x7f0b036a

.field public static final fitness_card_route_line_color:I = 0x7f0b0369

.field public static final five_percent_transparent_black:I = 0x7f0b00bc

.field public static final flyout_background_mask:I = 0x7f0b02be

.field public static final flyout_blue_text_color:I = 0x7f0b01b5

.field public static final flyout_comment_edit_cancel_button_text_color:I = 0x7f0b01b6

.field public static final flyout_comment_threaded_reply_background:I = 0x7f0b01b7

.field public static final flyout_light_gray_text_color:I = 0x7f0b01b4

.field public static final flyout_press_state_background_color:I = 0x7f0b01b3

.field public static final flyout_text_body_color:I = 0x7f0b01b2

.field public static final focused_background_color_dark:I = 0x7f0b0129

.field public static final focused_background_color_light:I = 0x7f0b012a

.field public static final footer_background_color:I = 0x7f0b02ea

.field public static final footer_grey:I = 0x7f0b0128

.field public static final form_hint_text:I = 0x7f0b03b9

.field public static final forty_percent_alpha_black:I = 0x7f0b0020

.field public static final forty_percent_alpha_white:I = 0x7f0b0025

.field public static final fqc_progress_bar_bkground:I = 0x7f0b045f

.field public static final friend_finder_avatar_background_color:I = 0x7f0b03f5

.field public static final friend_finder_background_color:I = 0x7f0b03f0

.field public static final friend_finder_header_section:I = 0x7f0b03ee

.field public static final friend_finder_horizontal_divider:I = 0x7f0b03ef

.field public static final friend_finder_progress_bar_background:I = 0x7f0b03f1

.field public static final friend_finder_progress_bar_container_background:I = 0x7f0b03f2

.field public static final friend_finder_row_subheading_color:I = 0x7f0b03f4

.field public static final friend_finder_row_title_color:I = 0x7f0b03f3

.field public static final friend_request_accepted_background:I = 0x7f0b046a

.field public static final friend_request_button_text_color_grey:I = 0x7f0b00d6

.field public static final friend_request_button_text_color_white:I = 0x7f0b00d7

.field public static final friend_request_grey_background:I = 0x7f0b046d

.field public static final friend_request_ignored_background:I = 0x7f0b046b

.field public static final friend_request_ignored_text:I = 0x7f0b046c

.field public static final friend_request_no_requests_text:I = 0x7f0b0469

.field public static final friending_list_background_color:I = 0x7f0b0460

.field public static final friending_list_subtext_color:I = 0x7f0b0461

.field public static final friending_list_title_color:I = 0x7f0b0462

.field public static final friends_nearby_list_divider:I = 0x7f0b0328

.field public static final friends_nearby_map_accuracy_fill:I = 0x7f0b0327

.field public static final friends_nearby_map_accuracy_stroke:I = 0x7f0b0326

.field public static final generic_background:I = 0x7f0b0397

.field public static final generic_black:I = 0x7f0b0398

.field public static final generic_error:I = 0x7f0b038b

.field public static final generic_pressed_state_background_color:I = 0x7f0b00b7

.field public static final generic_transparent:I = 0x7f0b039a

.field public static final generic_white:I = 0x7f0b0399

.field public static final gift_card_mall_divider:I = 0x7f0b03cf

.field public static final gift_card_mall_gift_card_options_background:I = 0x7f0b03d0

.field public static final gift_card_mall_option_selector_text_selected:I = 0x7f0b03d2

.field public static final gift_card_mall_option_selector_text_unselected:I = 0x7f0b03d1

.field public static final gift_card_mall_square_image_border:I = 0x7f0b03d3

.field public static final grab_bag_header_background:I = 0x7f0b03d4

.field public static final grab_bag_selector_divider:I = 0x7f0b03d5

.field public static final grab_bag_selector_selected_text:I = 0x7f0b03d6

.field public static final graph_search_filter_list_bg:I = 0x7f0b0409

.field public static final graph_search_generic_typeahead_bg:I = 0x7f0b0407

.field public static final graph_search_list_divider_color:I = 0x7f0b0403

.field public static final graph_search_match_words_background:I = 0x7f0b040a

.field public static final graph_search_photo_results_placeholder:I = 0x7f0b0408

.field public static final graph_search_search_box_bg:I = 0x7f0b0404

.field public static final graph_search_search_box_offline_bg:I = 0x7f0b0405

.field public static final graph_search_search_box_placeholder_text:I = 0x7f0b0406

.field public static final grey20:I = 0x7f0b0004

.field public static final grey27:I = 0x7f0b0005

.field public static final grey33:I = 0x7f0b0006

.field public static final grey40:I = 0x7f0b0007

.field public static final grey47:I = 0x7f0b0008

.field public static final grey50:I = 0x7f0b0009

.field public static final grey53:I = 0x7f0b000a

.field public static final grey60:I = 0x7f0b000b

.field public static final grey68:I = 0x7f0b000c

.field public static final grey80:I = 0x7f0b000d

.field public static final grey93:I = 0x7f0b000e

.field public static final grey94:I = 0x7f0b000f

.field public static final grid_placeholder:I = 0x7f0b0289

.field public static final groups_feed_header_action_bar_separator_color:I = 0x7f0b04b6

.field public static final groups_feed_header_placeholder_color:I = 0x7f0b04b5

.field public static final groups_feed_header_title_background_color:I = 0x7f0b04b4

.field public static final harrison_button_color:I = 0x7f0b0248

.field public static final harrison_button_composer:I = 0x7f0b04d2

.field public static final harrison_publisher_bar_background_color:I = 0x7f0b00dc

.field public static final harrison_publisher_bar_offline_background_color:I = 0x7f0b00dd

.field public static final harrison_publisher_bar_press_state:I = 0x7f0b00de

.field public static final harrison_up_button_press_state:I = 0x7f0b0219

.field public static final header_background_color:I = 0x7f0b0411

.field public static final header_divider_color:I = 0x7f0b03c6

.field public static final header_text_color:I = 0x7f0b03c5

.field public static final highlighted_text_facebookholo_dark:I = 0x7f0b0052

.field public static final highlighted_text_facebookholo_light:I = 0x7f0b0053

.field public static final hint_color:I = 0x7f0b040e

.field public static final hint_foreground_facebookholo_dark:I = 0x7f0b004c

.field public static final hint_foreground_facebookholo_light:I = 0x7f0b0051

.field public static final identity_growth_megaphone_accent_blue:I = 0x7f0b02da

.field public static final identity_growth_megaphone_background:I = 0x7f0b02d7

.field public static final identity_growth_megaphone_dark_grey_border:I = 0x7f0b02d8

.field public static final identity_growth_megaphone_dark_grey_text:I = 0x7f0b02db

.field public static final identity_growth_megaphone_disabled_overlay:I = 0x7f0b02d9

.field public static final identity_growth_megaphone_light_grey_text:I = 0x7f0b02dc

.field public static final image_reorder_view_bg_color:I = 0x7f0b029f

.field public static final implicit_location_text_color:I = 0x7f0b029e

.field public static final inline_video_placeholder_color:I = 0x7f0b010c

.field public static final input_field_background:I = 0x7f0b0433

.field public static final input_field_horizontal_bar:I = 0x7f0b0432

.field public static final input_field_text_hint:I = 0x7f0b0434

.field public static final input_text:I = 0x7f0b0435

.field public static final iorg_dialog_button_regular_text:I = 0x7f0b047a

.field public static final iorg_dialog_dark_text:I = 0x7f0b046f

.field public static final iorg_dialog_divider:I = 0x7f0b0477

.field public static final iorg_dialog_light_text:I = 0x7f0b0478

.field public static final iorg_dialog_link:I = 0x7f0b0479

.field public static final iorg_list_item_dark:I = 0x7f0b0476

.field public static final iorg_menu_divider:I = 0x7f0b0470

.field public static final iorg_menu_text:I = 0x7f0b0471

.field public static final iorg_pink:I = 0x7f0b0474

.field public static final iorg_progress_bar:I = 0x7f0b0475

.field public static final iorg_purple:I = 0x7f0b0473

.field public static final iorg_row_highlight:I = 0x7f0b0472

.field public static final itunes_content_divider:I = 0x7f0b03c3

.field public static final itunes_content_subtitle:I = 0x7f0b03c2

.field public static final itunes_recommendation_search_hint_text:I = 0x7f0b03c4

.field public static final jewel_background_color:I = 0x7f0b043f

.field public static final jewel_divebar_promo_text_color:I = 0x7f0b0441

.field public static final jewel_divebar_promo_title_color:I = 0x7f0b0440

.field public static final jewel_row_subheading_color:I = 0x7f0b043e

.field public static final jewel_row_title_color:I = 0x7f0b043d

.field public static final jewel_title_color:I = 0x7f0b043c

.field public static final light_gray_text_color:I = 0x7f0b0272

.field public static final light_grey:I = 0x7f0b002e

.field public static final light_orange_focused_color:I = 0x7f0b0136

.field public static final light_orange_pressed_color:I = 0x7f0b0137

.field public static final light_orca_blue:I = 0x7f0b0140

.field public static final light_separator_color:I = 0x7f0b0016

.field public static final link_highlight_color:I = 0x7f0b00be

.field public static final link_share_description_grey_me:I = 0x7f0b012c

.field public static final link_share_description_grey_other:I = 0x7f0b012d

.field public static final link_share_dividing_line_me:I = 0x7f0b012e

.field public static final link_share_dividing_line_other:I = 0x7f0b012f

.field public static final link_text:I = 0x7f0b039f

.field public static final link_text_facebookholo_dark:I = 0x7f0b0054

.field public static final link_text_facebookholo_light:I = 0x7f0b0055

.field public static final list_unselected_background:I = 0x7f0b0436

.field public static final loading_indicator_error_view_message_text_color:I = 0x7f0b0119

.field public static final loading_indicator_error_view_retry_text_color:I = 0x7f0b011a

.field public static final location_ping_dialog_pressed:I = 0x7f0b0329

.field public static final lockscreen_notification_opt_in_button_background_pressed:I = 0x7f0b02b0

.field public static final lockscreen_settings_divider:I = 0x7f0b02b8

.field public static final lockscreen_settings_subtitle:I = 0x7f0b02b7

.field public static final lockscreen_settings_title:I = 0x7f0b02b6

.field public static final login_font_color:I = 0x7f0b040f

.field public static final maintab_background_color:I = 0x7f0b0443

.field public static final map_dialog_background:I = 0x7f0b01a3

.field public static final media_edit_background_color:I = 0x7f0b010e

.field public static final media_edit_divider_color:I = 0x7f0b010f

.field public static final media_tray_background_color:I = 0x7f0b0159

.field public static final media_tray_error_state_background_color:I = 0x7f0b015b

.field public static final media_tray_photo_action_bar_color:I = 0x7f0b015c

.field public static final media_tray_placeholder_color:I = 0x7f0b015a

.field public static final medium_orca_blue:I = 0x7f0b0141

.field public static final megaphone_bg_color:I = 0x7f0b02dd

.field public static final megaphone_dark_border:I = 0x7f0b02e1

.field public static final megaphone_dark_grey_text:I = 0x7f0b02de

.field public static final megaphone_grey_button_text:I = 0x7f0b02df

.field public static final megaphone_light_grey_text:I = 0x7f0b02e0

.field public static final members_bar_extra_pog_fill_color:I = 0x7f0b04b3

.field public static final members_bar_extra_pog_outline_color:I = 0x7f0b04b2

.field public static final mentions_background:I = 0x7f0b021a

.field public static final menu_background_transparent_black:I = 0x7f0b02cc

.field public static final menu_item_pressed:I = 0x7f0b02cb

.field public static final menu_item_text_disabled:I = 0x7f0b02ca

.field public static final message_list_view_background:I = 0x7f0b0191

.field public static final message_notification_tile_divider:I = 0x7f0b0302

.field public static final message_text_grey:I = 0x7f0b0190

.field public static final meta_text_default_color:I = 0x7f0b029c

.field public static final meta_text_disabled_color:I = 0x7f0b029d

.field public static final minutiae_footer_bg_color:I = 0x7f0b0298

.field public static final minutiae_free_form_bg_color:I = 0x7f0b0296

.field public static final minutiae_free_form_pressed_bg_color:I = 0x7f0b0297

.field public static final minutiae_nux_bg_color:I = 0x7f0b0299

.field public static final minutiae_pressed_state_bg_color:I = 0x7f0b0295

.field public static final minutiae_subtitle_text_color:I = 0x7f0b0294

.field public static final minutiae_title_text_color:I = 0x7f0b0293

.field public static final music_notification_control_panel:I = 0x7f0b0307

.field public static final music_notification_overlay_end_shade:I = 0x7f0b0305

.field public static final music_notification_overlay_middle_shade:I = 0x7f0b0304

.field public static final music_notification_overlay_start_shade:I = 0x7f0b0303

.field public static final music_notification_text_shadow:I = 0x7f0b0306

.field public static final music_preview_card_gradient_end:I = 0x7f0b036f

.field public static final music_preview_card_gradient_start:I = 0x7f0b036e

.field public static final music_preview_card_music_button_background:I = 0x7f0b036c

.field public static final music_preview_card_music_button_progress:I = 0x7f0b036d

.field public static final muzei_background:I = 0x7f0b031d

.field public static final nav_badge_background_color:I = 0x7f0b00e1

.field public static final nav_glyph_off_state:I = 0x7f0b00df

.field public static final nav_glyph_on_state:I = 0x7f0b00e0

.field public static final nearby_blue_text_color:I = 0x7f0b03fd

.field public static final nearby_category_cell_default:I = 0x7f0b03fa

.field public static final nearby_category_cell_pressed:I = 0x7f0b03fb

.field public static final nearby_category_cell_title:I = 0x7f0b04d3

.field public static final nearby_dark_gray_text_color:I = 0x7f0b0401

.field public static final nearby_icon_title_default:I = 0x7f0b03fe

.field public static final nearby_icon_title_pressed:I = 0x7f0b03ff

.field public static final nearby_learn_more_background:I = 0x7f0b018c

.field public static final nearby_learn_more_text:I = 0x7f0b018e

.field public static final nearby_learn_more_title:I = 0x7f0b018d

.field public static final nearby_light_gray_text_color:I = 0x7f0b0400

.field public static final nearby_listview_default:I = 0x7f0b03f6

.field public static final nearby_listview_pressed:I = 0x7f0b03f7

.field public static final nearby_search_results_background:I = 0x7f0b03fc

.field public static final nearby_search_row_default:I = 0x7f0b03f8

.field public static final nearby_search_row_pressed:I = 0x7f0b03f9

.field public static final nearby_text_dark:I = 0x7f0b0402

.field public static final needle_search_find_photo_text:I = 0x7f0b040b

.field public static final neue_mainactivity_background:I = 0x7f0b01a1

.field public static final neue_presence_text_color:I = 0x7f0b0199

.field public static final new_bookmark_item_bg_highlight:I = 0x7f0b047b

.field public static final no_photo_text_color:I = 0x7f0b028a

.field public static final notification_background_color:I = 0x7f0b02aa

.field public static final notification_greyish_dark:I = 0x7f0b0130

.field public static final notification_greyish_light:I = 0x7f0b0131

.field public static final notification_lockscreen_background_color:I = 0x7f0b02ac

.field public static final notification_opt_in_background_color:I = 0x7f0b02ad

.field public static final notification_opt_in_button_background_color:I = 0x7f0b02ae

.field public static final notification_opt_in_buttons_divider_color:I = 0x7f0b02af

.field public static final notification_panel_end_color:I = 0x7f0b0300

.field public static final notification_panel_start_color:I = 0x7f0b02ff

.field public static final notification_rear_text:I = 0x7f0b0301

.field public static final notification_subheading_color:I = 0x7f0b02a8

.field public static final notification_system_background_color:I = 0x7f0b02fe

.field public static final notification_text_color:I = 0x7f0b02a9

.field public static final notification_title_color:I = 0x7f0b02a7

.field public static final notifications_background_overlay_full_blur:I = 0x7f0b00d5

.field public static final notifications_background_overlay_full_noblur:I = 0x7f0b00d4

.field public static final num_pad_key_color:I = 0x7f0b04d4

.field public static final nux_bg_color:I = 0x7f0b00e3

.field public static final nux_bg_color_transparent:I = 0x7f0b00e4

.field public static final nux_learn_more_background:I = 0x7f0b018a

.field public static final nux_text:I = 0x7f0b018b

.field public static final off_white:I = 0x7f0b0109

.field public static final offline_story_header_background_gradient_end:I = 0x7f0b0374

.field public static final offline_story_header_background_gradient_start:I = 0x7f0b0373

.field public static final offline_story_header_divider:I = 0x7f0b0372

.field public static final ood_text_color:I = 0x7f0b00d0

.field public static final ood_view_setting:I = 0x7f0b00d1

.field public static final orca_chat_thread_list_shadow_color:I = 0x7f0b0182

.field public static final orca_convo_bubble_audio_me_normal_highlighted:I = 0x7f0b0151

.field public static final orca_convo_bubble_audio_me_selected_highlighted:I = 0x7f0b0152

.field public static final orca_convo_bubble_audio_normal_highlighted:I = 0x7f0b014f

.field public static final orca_convo_bubble_audio_selected_highlighted:I = 0x7f0b0150

.field public static final orca_convo_bubble_mask_stroke:I = 0x7f0b014e

.field public static final orca_convo_bubble_me_normal:I = 0x7f0b014a

.field public static final orca_convo_bubble_me_selected:I = 0x7f0b014b

.field public static final orca_convo_bubble_normal:I = 0x7f0b0148

.field public static final orca_convo_bubble_pending_normal:I = 0x7f0b014c

.field public static final orca_convo_bubble_pending_selected:I = 0x7f0b014d

.field public static final orca_convo_bubble_selected:I = 0x7f0b0149

.field public static final orca_full_screen_video_pop_out_background:I = 0x7f0b01af

.field public static final orca_media_tray_cancel_progress_bar_color:I = 0x7f0b015d

.field public static final orca_neue_button_press_overlay:I = 0x7f0b019a

.field public static final orca_neue_gray:I = 0x7f0b0143

.field public static final orca_neue_light_gray:I = 0x7f0b0144

.field public static final orca_neue_menu_disabled_text_color:I = 0x7f0b0147

.field public static final orca_neue_menu_text_color:I = 0x7f0b0146

.field public static final orca_neue_nux_neutral:I = 0x7f0b0155

.field public static final orca_neue_preference_category_background:I = 0x7f0b01ac

.field public static final orca_neue_preference_category_separator:I = 0x7f0b01ae

.field public static final orca_neue_preference_category_text_color:I = 0x7f0b01ad

.field public static final orca_neue_primary:I = 0x7f0b0106

.field public static final orca_neue_primary_selected:I = 0x7f0b0107

.field public static final orca_neue_text_grey:I = 0x7f0b0145

.field public static final orca_quick_cam_container_background:I = 0x7f0b01aa

.field public static final orca_quick_cam_nux_drop_shadow_color:I = 0x7f0b013e

.field public static final orca_quick_cam_nux_text_color:I = 0x7f0b013f

.field public static final orca_quick_cam_pop_out_background:I = 0x7f0b01ab

.field public static final orca_titlebar_button:I = 0x7f0b04d5

.field public static final orca_white:I = 0x7f0b0108

.field public static final order_confirmation_header_background:I = 0x7f0b03bc

.field public static final order_confirmation_header_divider:I = 0x7f0b03bd

.field public static final order_confirmation_view_profile:I = 0x7f0b03be

.field public static final order_review_cell_subinfo_color:I = 0x7f0b03c9

.field public static final order_review_cell_title_color:I = 0x7f0b03c8

.field public static final order_review_terms_apply_link_text_color:I = 0x7f0b03ca

.field public static final order_review_terms_text_color:I = 0x7f0b03cb

.field public static final overlay_divider:I = 0x7f0b0498

.field public static final padding_bar:I = 0x7f0b0271

.field public static final page_activity_insights_uni_divider:I = 0x7f0b0360

.field public static final page_activity_uni_running_insights_description:I = 0x7f0b0363

.field public static final page_activity_uni_running_status_active:I = 0x7f0b0361

.field public static final page_activity_uni_running_status_default:I = 0x7f0b0362

.field public static final page_album_fragment_blue_tab_default:I = 0x7f0b0364

.field public static final page_album_fragment_blue_tab_selected:I = 0x7f0b0365

.field public static final page_friend_inviter_title_background:I = 0x7f0b047d

.field public static final page_identity_action_sheet_bottom_border:I = 0x7f0b035f

.field public static final page_identity_expandable_text_gradient_default_color:I = 0x7f0b0356

.field public static final page_identity_header_loading_bg:I = 0x7f0b0359

.field public static final page_identity_highlighted_unit_bg:I = 0x7f0b0352

.field public static final page_identity_hours_closed:I = 0x7f0b0354

.field public static final page_identity_hours_open:I = 0x7f0b0353

.field public static final page_identity_separator_color:I = 0x7f0b035d

.field public static final page_identity_text_friend_name:I = 0x7f0b0355

.field public static final page_identity_upsell_background:I = 0x7f0b0357

.field public static final page_identity_viewer_review_unit_background:I = 0x7f0b035e

.field public static final page_inline_friend_inviter_invite_button:I = 0x7f0b047c

.field public static final page_menu_section_header_bg:I = 0x7f0b0358

.field public static final page_placeholder_color:I = 0x7f0b0459

.field public static final page_timeline_chevron_pressed_bg:I = 0x7f0b0346

.field public static final page_timeline_summary_stats_text:I = 0x7f0b0342

.field public static final page_topic_section_header_fg:I = 0x7f0b02e2

.field public static final pandora_album_cover_placeholder:I = 0x7f0b0283

.field public static final pandora_benny_background:I = 0x7f0b0494

.field public static final pandora_benny_banner_no_photos:I = 0x7f0b0497

.field public static final pandora_benny_image_placeholder:I = 0x7f0b0495

.field public static final pandora_benny_images_pressed_state:I = 0x7f0b0496

.field public static final pandora_benny_text_color:I = 0x7f0b0491

.field public static final pandora_benny_title_background:I = 0x7f0b0492

.field public static final pandora_benny_title_border:I = 0x7f0b0493

.field public static final pandora_card_background:I = 0x7f0b048e

.field public static final pandora_fragment_background:I = 0x7f0b048f

.field public static final pandora_header_background:I = 0x7f0b048d

.field public static final pandora_header_date:I = 0x7f0b048c

.field public static final pandora_images_pressed_state:I = 0x7f0b0489

.field public static final pandora_story_details:I = 0x7f0b0490

.field public static final pandora_transparent_black_wash:I = 0x7f0b048a

.field public static final pandora_transparent_highlight_wash:I = 0x7f0b048b

.field public static final payment_pin_underline_color:I = 0x7f0b00ee

.field public static final people_filter_text_hint:I = 0x7f0b021b

.field public static final permalink_add_reply_background:I = 0x7f0b01f3

.field public static final permalink_add_reply_button_text:I = 0x7f0b01f4

.field public static final permalink_add_reply_text_hint:I = 0x7f0b01f5

.field public static final permalink_divider:I = 0x7f0b01f6

.field public static final person_card_footer_text:I = 0x7f0b0464

.field public static final photos_overlay_background:I = 0x7f0b0428

.field public static final photos_overlay_border:I = 0x7f0b042a

.field public static final photos_overlay_highlight:I = 0x7f0b0429

.field public static final pinned_groups_button_text_color:I = 0x7f0b01a4

.field public static final pinned_groups_input_hint_text_color:I = 0x7f0b01a5

.field public static final pivot_pager_dots_selected:I = 0x7f0b0208

.field public static final pivot_pager_dots_unselected:I = 0x7f0b0209

.field public static final places_border_color:I = 0x7f0b02e5

.field public static final places_dark_grey:I = 0x7f0b02e3

.field public static final places_footer_bg:I = 0x7f0b02e6

.field public static final places_placeholder_background:I = 0x7f0b02e4

.field public static final places_profile_photo_pressed_color:I = 0x7f0b02e7

.field public static final plutonium_context_item_count:I = 0x7f0b0333

.field public static final plutonium_context_item_subtitle:I = 0x7f0b0332

.field public static final plutonium_context_item_title:I = 0x7f0b0331

.field public static final plutonium_cover_photo_placeholder:I = 0x7f0b032f

.field public static final plutonium_darken_pressed_state:I = 0x7f0b0330

.field public static final popover_background_mask:I = 0x7f0b02d6

.field public static final popup_background_color:I = 0x7f0b042b

.field public static final popup_stroke_color:I = 0x7f0b042c

.field public static final post_check_in_background_gray:I = 0x7f0b01a6

.field public static final post_check_in_seperator_color:I = 0x7f0b01a8

.field public static final post_insights_blue_description:I = 0x7f0b024e

.field public static final post_insights_blue_first_bar:I = 0x7f0b024f

.field public static final post_insights_blue_second_bar:I = 0x7f0b0250

.field public static final post_insights_blue_third_bar:I = 0x7f0b0251

.field public static final post_insights_blue_title:I = 0x7f0b024d

.field public static final post_insights_gray_text_color:I = 0x7f0b025d

.field public static final post_insights_green_description:I = 0x7f0b0253

.field public static final post_insights_green_first_bar:I = 0x7f0b0254

.field public static final post_insights_green_second_bar:I = 0x7f0b0255

.field public static final post_insights_green_third_bar:I = 0x7f0b0256

.field public static final post_insights_green_title:I = 0x7f0b0252

.field public static final post_insights_half_white:I = 0x7f0b024c

.field public static final post_insights_lighter_gray:I = 0x7f0b025c

.field public static final post_insights_pager_indicator_selected:I = 0x7f0b025f

.field public static final post_insights_pager_indicator_unselected:I = 0x7f0b025e

.field public static final post_insights_red_description:I = 0x7f0b0258

.field public static final post_insights_red_first_bar:I = 0x7f0b0259

.field public static final post_insights_red_second_bar:I = 0x7f0b025a

.field public static final post_insights_red_third_bar:I = 0x7f0b025b

.field public static final post_insights_red_title:I = 0x7f0b0257

.field public static final post_post_description:I = 0x7f0b0211

.field public static final post_post_title:I = 0x7f0b0212

.field public static final preferences_category_gradient_bottom:I = 0x7f0b00c4

.field public static final preferences_category_gradient_top:I = 0x7f0b00c3

.field public static final preferences_category_text:I = 0x7f0b00c2

.field public static final preferences_checkbox_gradient_bottom:I = 0x7f0b00cb

.field public static final preferences_checkbox_gradient_inner_shadow:I = 0x7f0b00cc

.field public static final preferences_checkbox_gradient_stroke:I = 0x7f0b00cd

.field public static final preferences_checkbox_gradient_top:I = 0x7f0b00ca

.field public static final preferences_dialog_background:I = 0x7f0b00cf

.field public static final preferences_panel_background:I = 0x7f0b00c5

.field public static final preferences_panel_bottom_bevel_highlight:I = 0x7f0b00c9

.field public static final preferences_selector:I = 0x7f0b00c1

.field public static final preferences_separator_bottom:I = 0x7f0b00c0

.field public static final preferences_separator_top:I = 0x7f0b00bf

.field public static final preferences_text_shadow:I = 0x7f0b00ce

.field public static final preferences_title_bevel_highlight:I = 0x7f0b00c8

.field public static final preferences_title_gradient_bottom:I = 0x7f0b00c7

.field public static final preferences_title_gradient_top:I = 0x7f0b00c6

.field public static final premium_videos_ufi_bg_end_color:I = 0x7f0b0202

.field public static final premium_videos_ufi_bg_start_color:I = 0x7f0b0201

.field public static final presence_feed_unit_text_color:I = 0x7f0b0366

.field public static final preview_background:I = 0x7f0b031c

.field public static final primary_divider:I = 0x7f0b0316

.field public static final primary_named_button:I = 0x7f0b04d6

.field public static final primary_named_button_composer:I = 0x7f0b04d7

.field public static final primary_named_button_composer_control:I = 0x7f0b04d8

.field public static final primary_text_disable_only_facebookholo_dark:I = 0x7f0b04d9

.field public static final primary_text_disable_only_facebookholo_light:I = 0x7f0b04da

.field public static final primary_text_facebookholo_dark:I = 0x7f0b04db

.field public static final primary_text_facebookholo_light:I = 0x7f0b04dc

.field public static final primary_text_nodisable_facebookholo_dark:I = 0x7f0b04dd

.field public static final primary_text_nodisable_facebookholo_light:I = 0x7f0b04de

.field public static final privacy_background:I = 0x7f0b02e8

.field public static final privacy_background_color:I = 0x7f0b0412

.field public static final privacy_background_pressed:I = 0x7f0b02e9

.field public static final product_detail_text:I = 0x7f0b039e

.field public static final product_details_background:I = 0x7f0b03cc

.field public static final product_name_text:I = 0x7f0b039d

.field public static final products_filter_child_divider_color:I = 0x7f0b03c1

.field public static final products_filter_child_text_color:I = 0x7f0b03bf

.field public static final products_filter_group_divider_color:I = 0x7f0b03c0

.field public static final profile_info_request_button_text_disabled:I = 0x7f0b0465

.field public static final profile_list_name_color:I = 0x7f0b01f7

.field public static final publish_mode_options_text_color:I = 0x7f0b02a3

.field public static final publisher_text_color:I = 0x7f0b01f8

.field public static final publisher_text_color_blue:I = 0x7f0b01f9

.field public static final publisher_vertical_divider:I = 0x7f0b0195

.field public static final publisher_vertical_divider_harrison:I = 0x7f0b01fa

.field public static final pull_to_refresh_default_color:I = 0x7f0b011c

.field public static final push_notification_active_blue:I = 0x7f0b02b1

.field public static final push_notification_banner_background:I = 0x7f0b02b2

.field public static final push_notification_banner_subtitle:I = 0x7f0b02b4

.field public static final push_notification_banner_title:I = 0x7f0b02b3

.field public static final push_notification_gray_bar:I = 0x7f0b02ab

.field public static final pymk_feed_unit_divider_color:I = 0x7f0b01fb

.field public static final pymk_feed_unit_name_mutual_friends_background:I = 0x7f0b0368

.field public static final pymk_feed_unit_swipe_footer_divider_color:I = 0x7f0b01fc

.field public static final pymk_row_title_color:I = 0x7f0b046e

.field public static final qp_megaphone_bg_color:I = 0x7f0b02eb

.field public static final qrcode_state_switcher:I = 0x7f0b04df

.field public static final query_detail_list_item_color:I = 0x7f0b04e0

.field public static final query_list_caption_item_color:I = 0x7f0b04e1

.field public static final query_list_item_color:I = 0x7f0b04e2

.field public static final rating_section_black_80a:I = 0x7f0b0466

.field public static final rating_section_dark_gray:I = 0x7f0b0467

.field public static final rating_section_medium_gray:I = 0x7f0b0468

.field public static final reaction_base_wash:I = 0x7f0b0376

.field public static final reaction_cover_photo_action_overlay:I = 0x7f0b0377

.field public static final reaction_cover_photo_no_action_overlay:I = 0x7f0b0378

.field public static final reaction_hscroll_subtitle:I = 0x7f0b037a

.field public static final reaction_hscroll_title:I = 0x7f0b0379

.field public static final reaction_overlay_bg:I = 0x7f0b037b

.field public static final reaction_photo_overlay:I = 0x7f0b037c

.field public static final reaction_separator:I = 0x7f0b037d

.field public static final reaction_subtitle_text:I = 0x7f0b037e

.field public static final receipt_divider_color:I = 0x7f0b00f7

.field public static final receipt_send_to_receive_from_color:I = 0x7f0b00f8

.field public static final receipt_sent_money_color:I = 0x7f0b00f9

.field public static final red:I = 0x7f0b00bd

.field public static final red_highlight_text:I = 0x7f0b0454

.field public static final red_warning_color:I = 0x7f0b013a

.field public static final refreshable_list_view_text_color_angora:I = 0x7f0b011b

.field public static final related_videos_card_title_color:I = 0x7f0b0391

.field public static final related_videos_cards_title_color:I = 0x7f0b0390

.field public static final related_videos_carousel_dim_color:I = 0x7f0b038c

.field public static final related_videos_carousel_item_like_count_color:I = 0x7f0b038f

.field public static final related_videos_carousel_item_placeholder_color:I = 0x7f0b0393

.field public static final related_videos_carousel_item_placeholder_content:I = 0x7f0b0395

.field public static final related_videos_carousel_item_placeholder_space:I = 0x7f0b0394

.field public static final related_videos_carousel_item_placeholder_text_content:I = 0x7f0b0396

.field public static final related_videos_carousel_item_title_color:I = 0x7f0b038e

.field public static final related_videos_carousel_shimmer_center_color:I = 0x7f0b0392

.field public static final related_videos_carousel_title_color:I = 0x7f0b038d

.field public static final requests_mutual_friends:I = 0x7f0b041f

.field public static final requests_responded:I = 0x7f0b041e

.field public static final result_background:I = 0x7f0b040c

.field public static final result_divider:I = 0x7f0b040d

.field public static final return_to_feed_text_color:I = 0x7f0b01a7

.field public static final review_press_state_background_color:I = 0x7f0b028d

.field public static final review_text_dark:I = 0x7f0b028e

.field public static final review_text_light:I = 0x7f0b0290

.field public static final review_text_medium:I = 0x7f0b028f

.field public static final ridge_nux_item_bg_color:I = 0x7f0b029a

.field public static final ridge_widget_blue_dot:I = 0x7f0b02a5

.field public static final ridge_widget_grey_dot:I = 0x7f0b02a4

.field public static final roe_footer_text:I = 0x7f0b01fd

.field public static final roe_footer_text_background:I = 0x7f0b01fe

.field public static final roe_footer_text_background_border:I = 0x7f0b01ff

.field public static final rowview_detail_text_color:I = 0x7f0b0319

.field public static final rowview_disabled_text_color:I = 0x7f0b031a

.field public static final rowview_primary_text_color:I = 0x7f0b0318

.field public static final saved_dashboard_item_profile_picture_border:I = 0x7f0b04b0

.field public static final saved_dashboard_undo_button_divider:I = 0x7f0b04b1

.field public static final saved_item_subtitle_text_color:I = 0x7f0b04e3

.field public static final saved_item_title_text_color:I = 0x7f0b04e4

.field public static final saved_section_header_background:I = 0x7f0b04ad

.field public static final saved_section_header_divider:I = 0x7f0b04af

.field public static final saved_section_header_title:I = 0x7f0b04ae

.field public static final schedule_post_time_text_color:I = 0x7f0b0291

.field public static final scrim:I = 0x7f0b015e

.field public static final search_bar_background:I = 0x7f0b001b

.field public static final search_bar_text:I = 0x7f0b001c

.field public static final secondary_button_text:I = 0x7f0b0447

.field public static final secondary_divider:I = 0x7f0b0317

.field public static final secondary_tab_background:I = 0x7f0b00d8

.field public static final secondary_tab_bottom_border:I = 0x7f0b00db

.field public static final secondary_tab_text:I = 0x7f0b00d9

.field public static final secondary_tab_vertical_divider:I = 0x7f0b00da

.field public static final secondary_text_facebookholo_dark:I = 0x7f0b04e5

.field public static final secondary_text_facebookholo_light:I = 0x7f0b04e6

.field public static final secondary_text_nodisable_facebookholo_dark:I = 0x7f0b04e7

.field public static final secondary_text_nodisable_facebookholo_light:I = 0x7f0b04e8

.field public static final section_blue:I = 0x7f0b0410

.field public static final section_divider:I = 0x7f0b03a0

.field public static final section_header_text_color:I = 0x7f0b0207

.field public static final send_money_payment_gray_text:I = 0x7f0b00f3

.field public static final send_money_payment_value_text_color:I = 0x7f0b00f2

.field public static final send_money_receiver_bottom_border_color:I = 0x7f0b00f4

.field public static final separator_bottom:I = 0x7f0b0325

.field public static final separator_color:I = 0x7f0b029b

.field public static final separator_top:I = 0x7f0b0324

.field public static final settings_category_text_color:I = 0x7f0b00ef

.field public static final settings_preference_divider_color:I = 0x7f0b00f0

.field public static final settings_preference_summary_text_color:I = 0x7f0b00f1

.field public static final seventyfive_percent_alpha_white:I = 0x7f0b0022

.field public static final shadow_overlay:I = 0x7f0b031b

.field public static final share_launcher_divider:I = 0x7f0b01a9

.field public static final show_pivots_button_pressed:I = 0x7f0b04aa

.field public static final show_pivots_button_unpressed:I = 0x7f0b04a9

.field public static final sixty_percent_alpha_white:I = 0x7f0b0023

.field public static final sixty_percent_transparent_black:I = 0x7f0b00ba

.field public static final snowflake_background:I = 0x7f0b0213

.field public static final snowflake_dark_text:I = 0x7f0b0204

.field public static final snowflake_feed_text:I = 0x7f0b0205

.field public static final snowflake_feedback_text:I = 0x7f0b0215

.field public static final snowflake_light_text:I = 0x7f0b0206

.field public static final snowflake_link:I = 0x7f0b0216

.field public static final snowflake_ufi_separator:I = 0x7f0b0214

.field public static final solid_black:I = 0x7f0b0014

.field public static final solid_white:I = 0x7f0b0013

.field public static final span_color:I = 0x7f0b0415

.field public static final splash_screen_gradient_end:I = 0x7f0b030a

.field public static final splash_screen_gradient_start:I = 0x7f0b0309

.field public static final splash_screen_text_color:I = 0x7f0b0308

.field public static final standard_10_percent_black:I = 0x7f0b01a2

.field public static final standard_20_percent_black:I = 0x7f0b0200

.field public static final standard_40_percent_black:I = 0x7f0b049e

.field public static final standard_blue_3B5998:I = 0x7f0b03a9

.field public static final standard_blue_selected_text:I = 0x7f0b03b8

.field public static final standard_gray_141823:I = 0x7f0b03b4

.field public static final standard_gray_3e4350:I = 0x7f0b03a7

.field public static final standard_gray_424240:I = 0x7f0b03a6

.field public static final standard_gray_464c59:I = 0x7f0b03af

.field public static final standard_gray_4e5665:I = 0x7f0b03b6

.field public static final standard_gray_6a7180:I = 0x7f0b03b5

.field public static final standard_gray_6b6b68:I = 0x7f0b03a5

.field public static final standard_gray_787878:I = 0x7f0b03ab

.field public static final standard_gray_808080:I = 0x7f0b03a8

.field public static final standard_gray_999999:I = 0x7f0b03a4

.field public static final standard_gray_9c9c99:I = 0x7f0b03a3

.field public static final standard_gray_a0a3a8:I = 0x7f0b03b2

.field public static final standard_gray_adb2bb:I = 0x7f0b03b7

.field public static final standard_gray_b2b4b8:I = 0x7f0b03ad

.field public static final standard_gray_c3c4c7:I = 0x7f0b03b1

.field public static final standard_gray_cccccc:I = 0x7f0b03aa

.field public static final standard_gray_d3d6db:I = 0x7f0b03b3

.field public static final standard_gray_dadbde:I = 0x7f0b03ae

.field public static final standard_gray_divider_dadada:I = 0x7f0b03bb

.field public static final standard_gray_e2e2e0:I = 0x7f0b03a2

.field public static final standard_gray_e9e9e9:I = 0x7f0b03ac

.field public static final standard_light_text:I = 0x7f0b0456

.field public static final standard_medium_text:I = 0x7f0b0455

.field public static final standard_off_white_background:I = 0x7f0b039b

.field public static final standard_purple_text:I = 0x7f0b039c

.field public static final standard_section_header_text:I = 0x7f0b03b0

.field public static final star_rating_description_text_color:I = 0x7f0b037f

.field public static final sticker_popup_background_color:I = 0x7f0b015f

.field public static final sticker_popup_background_color_neue:I = 0x7f0b0164

.field public static final sticker_popup_pager_fill_color:I = 0x7f0b0163

.field public static final sticker_popup_pager_fill_color_neue:I = 0x7f0b0168

.field public static final sticker_popup_pager_page_color:I = 0x7f0b0162

.field public static final sticker_popup_pager_page_color_neue:I = 0x7f0b0167

.field public static final sticker_popup_tab_divider_color:I = 0x7f0b0160

.field public static final sticker_popup_tab_divider_color_neue:I = 0x7f0b0165

.field public static final sticker_store_tab_text_color:I = 0x7f0b0161

.field public static final sticker_store_tab_text_color_neue:I = 0x7f0b0166

.field public static final story_promotion_banner_background_normal:I = 0x7f0b0260

.field public static final story_promotion_banner_background_red:I = 0x7f0b0261

.field public static final story_promotion_banner_divider:I = 0x7f0b0262

.field public static final story_promotion_banner_text:I = 0x7f0b0263

.field public static final story_promotion_body_background:I = 0x7f0b0268

.field public static final story_promotion_budget_label_text:I = 0x7f0b0269

.field public static final story_promotion_estimate_reach_text:I = 0x7f0b026a

.field public static final story_promotion_grey_button_text:I = 0x7f0b026c

.field public static final story_promotion_header_text:I = 0x7f0b0264

.field public static final story_promotion_payment_info_divider:I = 0x7f0b026b

.field public static final story_promotion_progress_number:I = 0x7f0b0266

.field public static final story_promotion_progress_text:I = 0x7f0b0267

.field public static final story_promotion_text:I = 0x7f0b0265

.field public static final table_divider:I = 0x7f0b03a1

.field public static final tag_typeahead_divider:I = 0x7f0b020e

.field public static final tag_typeahead_hint_focused:I = 0x7f0b0210

.field public static final tag_typeahead_hint_not_focused:I = 0x7f0b020f

.field public static final ten_percent_alpha_black:I = 0x7f0b0021

.field public static final tertiary_text_facebookholo_dark:I = 0x7f0b04e9

.field public static final tertiary_text_facebookholo_light:I = 0x7f0b04ea

.field public static final text_link_color:I = 0x7f0b0135

.field public static final thirty_percent_alpha_white:I = 0x7f0b0026

.field public static final thread_tile_badge_shadow:I = 0x7f0b00b4

.field public static final thread_view_background:I = 0x7f0b0193

.field public static final timeline_action_button_text_normal:I = 0x7f0b033e

.field public static final timeline_actionlink_text:I = 0x7f0b033c

.field public static final timeline_actionlink_text_disabled:I = 0x7f0b033d

.field public static final timeline_add_year_overview_text_color:I = 0x7f0b034e

.field public static final timeline_alternate_name_text:I = 0x7f0b0337

.field public static final timeline_cover_photo_bg:I = 0x7f0b0338

.field public static final timeline_cover_photo_border:I = 0x7f0b033a

.field public static final timeline_cover_photo_upload_text:I = 0x7f0b0339

.field public static final timeline_darken_pressed_state:I = 0x7f0b0335

.field public static final timeline_dialog_background:I = 0x7f0b0343

.field public static final timeline_dialog_border:I = 0x7f0b0344

.field public static final timeline_dialog_item_background:I = 0x7f0b0345

.field public static final timeline_feed_list_item_bg_pressed_state:I = 0x7f0b0336

.field public static final timeline_name_text:I = 0x7f0b0334

.field public static final timeline_navtile_background_color:I = 0x7f0b0347

.field public static final timeline_navtile_image_background_color_angora:I = 0x7f0b0348

.field public static final timeline_profile_header_text:I = 0x7f0b033b

.field public static final timeline_profile_questions_background:I = 0x7f0b0349

.field public static final timeline_profile_questions_subtitle:I = 0x7f0b034a

.field public static final timeline_profile_questions_title:I = 0x7f0b034b

.field public static final timeline_prompt_count_border:I = 0x7f0b034d

.field public static final timeline_prompt_text:I = 0x7f0b034c

.field public static final timeline_publisher_divider:I = 0x7f0b033f

.field public static final timeline_section_header_shadow:I = 0x7f0b0341

.field public static final timeline_section_header_text:I = 0x7f0b0340

.field public static final title_bar_background:I = 0x7f0b0416

.field public static final title_bar_background_offline:I = 0x7f0b0417

.field public static final title_bar_bg:I = 0x7f0b0284

.field public static final title_bar_blue_background:I = 0x7f0b0375

.field public static final title_bar_shadow:I = 0x7f0b0419

.field public static final title_bar_text_overlay_color:I = 0x7f0b0418

.field public static final titlebar_button_pressed_overlay:I = 0x7f0b0442

.field public static final toolbar_button_label:I = 0x7f0b0197

.field public static final toolbar_button_label_disabled:I = 0x7f0b0198

.field public static final transparent:I = 0x7f0b0017

.field public static final transparent_black_border:I = 0x7f0b0278

.field public static final transparent_black_wash_on_album_cover:I = 0x7f0b027a

.field public static final twelve_percent_alpha_white:I = 0x7f0b0028

.field public static final twenty_percent_alpha_white:I = 0x7f0b0027

.field public static final txn_hist_payment_value_text_color_receiver:I = 0x7f0b00f6

.field public static final txn_hist_payment_value_text_color_sender:I = 0x7f0b00f5

.field public static final typeahead_disabled_text:I = 0x7f0b00eb

.field public static final typeahead_divider_background:I = 0x7f0b00e5

.field public static final typeahead_divider_background_fig:I = 0x7f0b00e6

.field public static final typeahead_header_background:I = 0x7f0b00e7

.field public static final typeahead_header_text:I = 0x7f0b00e8

.field public static final typeahead_item_background:I = 0x7f0b00e9

.field public static final typeahead_item_text:I = 0x7f0b00ea

.field public static final ufiservices_flyout_divider_color:I = 0x7f0b01b8

.field public static final ufiservices_story_dark_gray_text_color:I = 0x7f0b01b9

.field public static final underwood_attachment_bg_color:I = 0x7f0b02a2

.field public static final underwood_nux_bg_color:I = 0x7f0b02a0

.field public static final underwood_nux_circle_bg_color:I = 0x7f0b02a1

.field public static final unread_notification_background_color:I = 0x7f0b02a6

.field public static final upsell_interstitial_dark_text:I = 0x7f0b02b9

.field public static final upsell_interstitial_grey_background:I = 0x7f0b02bc

.field public static final upsell_interstitial_highlight_color:I = 0x7f0b02bd

.field public static final upsell_interstitial_keyline:I = 0x7f0b02bb

.field public static final upsell_interstitial_light_text:I = 0x7f0b02ba

.field public static final use_button_bg:I = 0x7f0b0288

.field public static final use_button_text:I = 0x7f0b0287

.field public static final use_button_text_pressed:I = 0x7f0b0286

.field public static final user_account_nux_step_profile_info_background_color:I = 0x7f0b03dd

.field public static final user_account_nux_step_profile_info_bottom_text_color:I = 0x7f0b03e1

.field public static final user_account_nux_step_profile_info_question_item_placeholder_text_color:I = 0x7f0b03df

.field public static final user_account_nux_step_profile_info_question_item_saved_info_text_color:I = 0x7f0b03e0

.field public static final user_account_nux_step_profile_info_top_text_color:I = 0x7f0b03de

.field public static final vault_light_blue_background:I = 0x7f0b0273

.field public static final vault_sync_blue:I = 0x7f0b0275

.field public static final vault_sync_settings_background:I = 0x7f0b0277

.field public static final vault_sync_settings_grey_text_color:I = 0x7f0b0276

.field public static final vault_sync_status_bar_text_color:I = 0x7f0b0274

.field public static final very_light_orca_blue:I = 0x7f0b013d

.field public static final video_cancel_screen_color:I = 0x7f0b013c

.field public static final video_end_screen_mask_color:I = 0x7f0b010d

.field public static final video_progress_buffered:I = 0x7f0b010b

.field public static final video_progress_dark:I = 0x7f0b010a

.field public static final video_trimming_film_strip_clip_mask_color:I = 0x7f0b0117

.field public static final video_trimming_film_strip_selected_bar_color:I = 0x7f0b0115

.field public static final video_trimming_film_strip_unselected_bar_color:I = 0x7f0b0116

.field public static final video_trimming_metadata_background_color:I = 0x7f0b0110

.field public static final video_trimming_metadata_edited_body_text_color:I = 0x7f0b0114

.field public static final video_trimming_metadata_edited_label_text_color:I = 0x7f0b0112

.field public static final video_trimming_metadata_original_body_text_color:I = 0x7f0b0113

.field public static final video_trimming_metadata_original_label_text_color:I = 0x7f0b0111

.field public static final vignette_overlay:I = 0x7f0b020a

.field public static final voip_button_pressed:I = 0x7f0b00fc

.field public static final voip_cancel_button:I = 0x7f0b0101

.field public static final voip_cancel_button_pressed:I = 0x7f0b0102

.field public static final voip_end_button:I = 0x7f0b00fd

.field public static final voip_end_button_pressed:I = 0x7f0b00fe

.field public static final voip_reconnected:I = 0x7f0b0105

.field public static final voip_reconnecting:I = 0x7f0b0104

.field public static final voip_redial_button:I = 0x7f0b00ff

.field public static final voip_redial_button_pressed:I = 0x7f0b0100

.field public static final voip_section_background_normal:I = 0x7f0b00fa

.field public static final voip_section_background_unavailable:I = 0x7f0b00fb

.field public static final voip_weak_connection:I = 0x7f0b0103

.field public static final vpi__background_holo_dark:I = 0x7f0b02bf

.field public static final vpi__background_holo_light:I = 0x7f0b02c0

.field public static final vpi__bright_foreground_disabled_holo_dark:I = 0x7f0b02c3

.field public static final vpi__bright_foreground_disabled_holo_light:I = 0x7f0b02c4

.field public static final vpi__bright_foreground_holo_dark:I = 0x7f0b02c1

.field public static final vpi__bright_foreground_holo_light:I = 0x7f0b02c2

.field public static final vpi__bright_foreground_inverse_holo_dark:I = 0x7f0b02c5

.field public static final vpi__bright_foreground_inverse_holo_light:I = 0x7f0b02c6

.field public static final wallet_bright_foreground_disabled_holo_light:I = 0x7f0b022b

.field public static final wallet_bright_foreground_holo_dark:I = 0x7f0b0226

.field public static final wallet_bright_foreground_holo_light:I = 0x7f0b022c

.field public static final wallet_dim_foreground_disabled_holo_dark:I = 0x7f0b0228

.field public static final wallet_dim_foreground_holo_dark:I = 0x7f0b0227

.field public static final wallet_dim_foreground_inverse_disabled_holo_dark:I = 0x7f0b022a

.field public static final wallet_dim_foreground_inverse_holo_dark:I = 0x7f0b0229

.field public static final wallet_highlighted_text_holo_dark:I = 0x7f0b0230

.field public static final wallet_highlighted_text_holo_light:I = 0x7f0b022f

.field public static final wallet_hint_foreground_holo_dark:I = 0x7f0b022e

.field public static final wallet_hint_foreground_holo_light:I = 0x7f0b022d

.field public static final wallet_holo_blue_light:I = 0x7f0b0231

.field public static final wallet_link_text_light:I = 0x7f0b0232

.field public static final wallet_primary_text_holo_light:I = 0x7f0b04eb

.field public static final wallet_secondary_text_holo_dark:I = 0x7f0b04ec

.field public static final white:I = 0x7f0b0011

.field public static final white_10pct:I = 0x7f0b030c

.field public static final white_15pct:I = 0x7f0b030d

.field public static final white_20pct:I = 0x7f0b030e

.field public static final white_25pct:I = 0x7f0b030f

.field public static final white_30pct:I = 0x7f0b0310

.field public static final white_40pct:I = 0x7f0b0311

.field public static final white_50pct:I = 0x7f0b0312

.field public static final white_5pct:I = 0x7f0b030b

.field public static final white_60pct:I = 0x7f0b02b5

.field public static final white_70pct:I = 0x7f0b0313

.field public static final white_80pct:I = 0x7f0b0314

.field public static final white_90pct:I = 0x7f0b0315

.field public static final white_text_shadow_color:I = 0x7f0b00b9

.field public static final zero_checkbox_gradient_bottom:I = 0x7f0b011e

.field public static final zero_checkbox_gradient_inner_shadow:I = 0x7f0b011f

.field public static final zero_checkbox_gradient_stroke:I = 0x7f0b0120

.field public static final zero_checkbox_gradient_top:I = 0x7f0b011d

.field public static final zero_interstitial_content_text_color:I = 0x7f0b0123

.field public static final zero_interstitial_description_text_color:I = 0x7f0b0122

.field public static final zero_interstitial_title_text_color:I = 0x7f0b0121


# direct methods
.method public constructor <init>()V
    .locals 0

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method
