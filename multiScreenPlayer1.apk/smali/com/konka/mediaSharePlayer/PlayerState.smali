.class public final enum Lcom/konka/mediaSharePlayer/PlayerState;
.super Ljava/lang/Enum;
.source "PlayerState.java"


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Enum",
        "<",
        "Lcom/konka/mediaSharePlayer/PlayerState;",
        ">;"
    }
.end annotation


# static fields
.field private static final synthetic ENUM$VALUES:[Lcom/konka/mediaSharePlayer/PlayerState;

.field public static final enum PLAY_STATUS_ERROR:Lcom/konka/mediaSharePlayer/PlayerState;

.field public static final enum PLAY_STATUS_EXIT:Lcom/konka/mediaSharePlayer/PlayerState;

.field public static final enum PLAY_STATUS_NUM:Lcom/konka/mediaSharePlayer/PlayerState;

.field public static final enum PLAY_STATUS_PAUSE:Lcom/konka/mediaSharePlayer/PlayerState;

.field public static final enum PLAY_STATUS_PLAY:Lcom/konka/mediaSharePlayer/PlayerState;

.field public static final enum PLAY_STATUS_RESUME:Lcom/konka/mediaSharePlayer/PlayerState;

.field public static final enum PLAY_STATUS_RESUMEPLAY:Lcom/konka/mediaSharePlayer/PlayerState;

.field public static final enum PLAY_STATUS_SEEK:Lcom/konka/mediaSharePlayer/PlayerState;

.field public static final enum PLAY_STATUS_STOP:Lcom/konka/mediaSharePlayer/PlayerState;

.field public static final enum PLAY_STATUS_SWITCH_PLAYER:Lcom/konka/mediaSharePlayer/PlayerState;

.field public static final enum PLAY_STATUS_SYNC:Lcom/konka/mediaSharePlayer/PlayerState;


# direct methods
.method static constructor <clinit>()V
    .locals 8

    const/4 v7, 0x4

    const/4 v6, 0x3

    const/4 v5, 0x2

    const/4 v4, 0x1

    const/4 v3, 0x0

    new-instance v0, Lcom/konka/mediaSharePlayer/PlayerState;

    const-string v1, "PLAY_STATUS_ERROR"

    invoke-direct {v0, v1, v3}, Lcom/konka/mediaSharePlayer/PlayerState;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/konka/mediaSharePlayer/PlayerState;->PLAY_STATUS_ERROR:Lcom/konka/mediaSharePlayer/PlayerState;

    new-instance v0, Lcom/konka/mediaSharePlayer/PlayerState;

    const-string v1, "PLAY_STATUS_PLAY"

    invoke-direct {v0, v1, v4}, Lcom/konka/mediaSharePlayer/PlayerState;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/konka/mediaSharePlayer/PlayerState;->PLAY_STATUS_PLAY:Lcom/konka/mediaSharePlayer/PlayerState;

    new-instance v0, Lcom/konka/mediaSharePlayer/PlayerState;

    const-string v1, "PLAY_STATUS_STOP"

    invoke-direct {v0, v1, v5}, Lcom/konka/mediaSharePlayer/PlayerState;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/konka/mediaSharePlayer/PlayerState;->PLAY_STATUS_STOP:Lcom/konka/mediaSharePlayer/PlayerState;

    new-instance v0, Lcom/konka/mediaSharePlayer/PlayerState;

    const-string v1, "PLAY_STATUS_PAUSE"

    invoke-direct {v0, v1, v6}, Lcom/konka/mediaSharePlayer/PlayerState;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/konka/mediaSharePlayer/PlayerState;->PLAY_STATUS_PAUSE:Lcom/konka/mediaSharePlayer/PlayerState;

    new-instance v0, Lcom/konka/mediaSharePlayer/PlayerState;

    const-string v1, "PLAY_STATUS_SEEK"

    invoke-direct {v0, v1, v7}, Lcom/konka/mediaSharePlayer/PlayerState;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/konka/mediaSharePlayer/PlayerState;->PLAY_STATUS_SEEK:Lcom/konka/mediaSharePlayer/PlayerState;

    new-instance v0, Lcom/konka/mediaSharePlayer/PlayerState;

    const-string v1, "PLAY_STATUS_EXIT"

    const/4 v2, 0x5

    invoke-direct {v0, v1, v2}, Lcom/konka/mediaSharePlayer/PlayerState;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/konka/mediaSharePlayer/PlayerState;->PLAY_STATUS_EXIT:Lcom/konka/mediaSharePlayer/PlayerState;

    new-instance v0, Lcom/konka/mediaSharePlayer/PlayerState;

    const-string v1, "PLAY_STATUS_RESUME"

    const/4 v2, 0x6

    invoke-direct {v0, v1, v2}, Lcom/konka/mediaSharePlayer/PlayerState;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/konka/mediaSharePlayer/PlayerState;->PLAY_STATUS_RESUME:Lcom/konka/mediaSharePlayer/PlayerState;

    new-instance v0, Lcom/konka/mediaSharePlayer/PlayerState;

    const-string v1, "PLAY_STATUS_SYNC"

    const/4 v2, 0x7

    invoke-direct {v0, v1, v2}, Lcom/konka/mediaSharePlayer/PlayerState;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/konka/mediaSharePlayer/PlayerState;->PLAY_STATUS_SYNC:Lcom/konka/mediaSharePlayer/PlayerState;

    new-instance v0, Lcom/konka/mediaSharePlayer/PlayerState;

    const-string v1, "PLAY_STATUS_RESUMEPLAY"

    const/16 v2, 0x8

    invoke-direct {v0, v1, v2}, Lcom/konka/mediaSharePlayer/PlayerState;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/konka/mediaSharePlayer/PlayerState;->PLAY_STATUS_RESUMEPLAY:Lcom/konka/mediaSharePlayer/PlayerState;

    new-instance v0, Lcom/konka/mediaSharePlayer/PlayerState;

    const-string v1, "PLAY_STATUS_SWITCH_PLAYER"

    const/16 v2, 0x9

    invoke-direct {v0, v1, v2}, Lcom/konka/mediaSharePlayer/PlayerState;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/konka/mediaSharePlayer/PlayerState;->PLAY_STATUS_SWITCH_PLAYER:Lcom/konka/mediaSharePlayer/PlayerState;

    new-instance v0, Lcom/konka/mediaSharePlayer/PlayerState;

    const-string v1, "PLAY_STATUS_NUM"

    const/16 v2, 0xa

    invoke-direct {v0, v1, v2}, Lcom/konka/mediaSharePlayer/PlayerState;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/konka/mediaSharePlayer/PlayerState;->PLAY_STATUS_NUM:Lcom/konka/mediaSharePlayer/PlayerState;

    const/16 v0, 0xb

    new-array v0, v0, [Lcom/konka/mediaSharePlayer/PlayerState;

    sget-object v1, Lcom/konka/mediaSharePlayer/PlayerState;->PLAY_STATUS_ERROR:Lcom/konka/mediaSharePlayer/PlayerState;

    aput-object v1, v0, v3

    sget-object v1, Lcom/konka/mediaSharePlayer/PlayerState;->PLAY_STATUS_PLAY:Lcom/konka/mediaSharePlayer/PlayerState;

    aput-object v1, v0, v4

    sget-object v1, Lcom/konka/mediaSharePlayer/PlayerState;->PLAY_STATUS_STOP:Lcom/konka/mediaSharePlayer/PlayerState;

    aput-object v1, v0, v5

    sget-object v1, Lcom/konka/mediaSharePlayer/PlayerState;->PLAY_STATUS_PAUSE:Lcom/konka/mediaSharePlayer/PlayerState;

    aput-object v1, v0, v6

    sget-object v1, Lcom/konka/mediaSharePlayer/PlayerState;->PLAY_STATUS_SEEK:Lcom/konka/mediaSharePlayer/PlayerState;

    aput-object v1, v0, v7

    const/4 v1, 0x5

    sget-object v2, Lcom/konka/mediaSharePlayer/PlayerState;->PLAY_STATUS_EXIT:Lcom/konka/mediaSharePlayer/PlayerState;

    aput-object v2, v0, v1

    const/4 v1, 0x6

    sget-object v2, Lcom/konka/mediaSharePlayer/PlayerState;->PLAY_STATUS_RESUME:Lcom/konka/mediaSharePlayer/PlayerState;

    aput-object v2, v0, v1

    const/4 v1, 0x7

    sget-object v2, Lcom/konka/mediaSharePlayer/PlayerState;->PLAY_STATUS_SYNC:Lcom/konka/mediaSharePlayer/PlayerState;

    aput-object v2, v0, v1

    const/16 v1, 0x8

    sget-object v2, Lcom/konka/mediaSharePlayer/PlayerState;->PLAY_STATUS_RESUMEPLAY:Lcom/konka/mediaSharePlayer/PlayerState;

    aput-object v2, v0, v1

    const/16 v1, 0x9

    sget-object v2, Lcom/konka/mediaSharePlayer/PlayerState;->PLAY_STATUS_SWITCH_PLAYER:Lcom/konka/mediaSharePlayer/PlayerState;

    aput-object v2, v0, v1

    const/16 v1, 0xa

    sget-object v2, Lcom/konka/mediaSharePlayer/PlayerState;->PLAY_STATUS_NUM:Lcom/konka/mediaSharePlayer/PlayerState;

    aput-object v2, v0, v1

    sput-object v0, Lcom/konka/mediaSharePlayer/PlayerState;->ENUM$VALUES:[Lcom/konka/mediaSharePlayer/PlayerState;

    return-void
.end method

.method private constructor <init>(Ljava/lang/String;I)V
    .locals 0

    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    return-void
.end method

.method public static valueOf(Ljava/lang/String;)Lcom/konka/mediaSharePlayer/PlayerState;
    .locals 1

    const-class v0, Lcom/konka/mediaSharePlayer/PlayerState;

    invoke-static {v0, p0}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;

    move-result-object v0

    check-cast v0, Lcom/konka/mediaSharePlayer/PlayerState;

    return-object v0
.end method

.method public static values()[Lcom/konka/mediaSharePlayer/PlayerState;
    .locals 4

    const/4 v3, 0x0

    sget-object v0, Lcom/konka/mediaSharePlayer/PlayerState;->ENUM$VALUES:[Lcom/konka/mediaSharePlayer/PlayerState;

    array-length v1, v0

    new-array v2, v1, [Lcom/konka/mediaSharePlayer/PlayerState;

    invoke-static {v0, v3, v2, v3, v1}, Ljava/lang/System;->arraycopy(Ljava/lang/Object;ILjava/lang/Object;II)V

    return-object v2
.end method
