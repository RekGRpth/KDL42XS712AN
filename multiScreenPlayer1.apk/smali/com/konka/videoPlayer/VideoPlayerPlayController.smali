.class public Lcom/konka/videoPlayer/VideoPlayerPlayController;
.super Ljava/lang/Object;
.source "VideoPlayerPlayController.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/konka/videoPlayer/VideoPlayerPlayController$ItemOnClickEvent;
    }
.end annotation


# instance fields
.field private TAG:Ljava/lang/String;

.field private buttonNext:Landroid/widget/Button;

.field private buttonPause:Landroid/widget/Button;

.field private buttonPlay:Landroid/widget/Button;

.field private buttonPre:Landroid/widget/Button;

.field current:I

.field private curtime:Landroid/widget/TextView;

.field duration:I

.field private mVideoPlayer:Lcom/konka/videoPlayer/videoPlayerActivity;

.field private seekbar:Landroid/widget/SeekBar;

.field private totalTime:Landroid/widget/TextView;

.field protected videoControl:Landroid/widget/LinearLayout;


# direct methods
.method public constructor <init>(Lcom/konka/videoPlayer/videoPlayerActivity;)V
    .locals 1
    .param p1    # Lcom/konka/videoPlayer/videoPlayerActivity;

    const/4 v0, 0x0

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    iput v0, p0, Lcom/konka/videoPlayer/VideoPlayerPlayController;->current:I

    iput v0, p0, Lcom/konka/videoPlayer/VideoPlayerPlayController;->duration:I

    const-string v0, "VideoPlayerPlayController"

    iput-object v0, p0, Lcom/konka/videoPlayer/VideoPlayerPlayController;->TAG:Ljava/lang/String;

    iput-object p1, p0, Lcom/konka/videoPlayer/VideoPlayerPlayController;->mVideoPlayer:Lcom/konka/videoPlayer/videoPlayerActivity;

    return-void
.end method

.method static synthetic access$0(Lcom/konka/videoPlayer/VideoPlayerPlayController;)Lcom/konka/videoPlayer/videoPlayerActivity;
    .locals 1

    iget-object v0, p0, Lcom/konka/videoPlayer/VideoPlayerPlayController;->mVideoPlayer:Lcom/konka/videoPlayer/videoPlayerActivity;

    return-object v0
.end method

.method static synthetic access$1(Lcom/konka/videoPlayer/VideoPlayerPlayController;)Landroid/widget/Button;
    .locals 1

    iget-object v0, p0, Lcom/konka/videoPlayer/VideoPlayerPlayController;->buttonPlay:Landroid/widget/Button;

    return-object v0
.end method

.method static synthetic access$2(Lcom/konka/videoPlayer/VideoPlayerPlayController;)Landroid/widget/Button;
    .locals 1

    iget-object v0, p0, Lcom/konka/videoPlayer/VideoPlayerPlayController;->buttonPause:Landroid/widget/Button;

    return-object v0
.end method

.method static synthetic access$3(Lcom/konka/videoPlayer/VideoPlayerPlayController;)Landroid/widget/SeekBar;
    .locals 1

    iget-object v0, p0, Lcom/konka/videoPlayer/VideoPlayerPlayController;->seekbar:Landroid/widget/SeekBar;

    return-object v0
.end method

.method static synthetic access$4(Lcom/konka/videoPlayer/VideoPlayerPlayController;)Ljava/lang/String;
    .locals 1

    iget-object v0, p0, Lcom/konka/videoPlayer/VideoPlayerPlayController;->TAG:Ljava/lang/String;

    return-object v0
.end method


# virtual methods
.method public findBtnView()V
    .locals 2

    iget-object v0, p0, Lcom/konka/videoPlayer/VideoPlayerPlayController;->mVideoPlayer:Lcom/konka/videoPlayer/videoPlayerActivity;

    const v1, 0x7f0b0021    # com.konka.mediaSharePlayer.R.id.video_btn_seekback

    invoke-virtual {v0, v1}, Lcom/konka/videoPlayer/videoPlayerActivity;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/Button;

    iput-object v0, p0, Lcom/konka/videoPlayer/VideoPlayerPlayController;->buttonPre:Landroid/widget/Button;

    iget-object v0, p0, Lcom/konka/videoPlayer/VideoPlayerPlayController;->mVideoPlayer:Lcom/konka/videoPlayer/videoPlayerActivity;

    const v1, 0x7f0b0022    # com.konka.mediaSharePlayer.R.id.video_btn_play

    invoke-virtual {v0, v1}, Lcom/konka/videoPlayer/videoPlayerActivity;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/Button;

    iput-object v0, p0, Lcom/konka/videoPlayer/VideoPlayerPlayController;->buttonPlay:Landroid/widget/Button;

    iget-object v0, p0, Lcom/konka/videoPlayer/VideoPlayerPlayController;->mVideoPlayer:Lcom/konka/videoPlayer/videoPlayerActivity;

    const v1, 0x7f0b0023    # com.konka.mediaSharePlayer.R.id.video_btn_pause

    invoke-virtual {v0, v1}, Lcom/konka/videoPlayer/videoPlayerActivity;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/Button;

    iput-object v0, p0, Lcom/konka/videoPlayer/VideoPlayerPlayController;->buttonPause:Landroid/widget/Button;

    iget-object v0, p0, Lcom/konka/videoPlayer/VideoPlayerPlayController;->mVideoPlayer:Lcom/konka/videoPlayer/videoPlayerActivity;

    const v1, 0x7f0b0024    # com.konka.mediaSharePlayer.R.id.video_btn_seekforward

    invoke-virtual {v0, v1}, Lcom/konka/videoPlayer/videoPlayerActivity;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/Button;

    iput-object v0, p0, Lcom/konka/videoPlayer/VideoPlayerPlayController;->buttonNext:Landroid/widget/Button;

    iget-object v0, p0, Lcom/konka/videoPlayer/VideoPlayerPlayController;->mVideoPlayer:Lcom/konka/videoPlayer/videoPlayerActivity;

    const v1, 0x7f0b0025    # com.konka.mediaSharePlayer.R.id.video_playbar_time

    invoke-virtual {v0, v1}, Lcom/konka/videoPlayer/videoPlayerActivity;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    iput-object v0, p0, Lcom/konka/videoPlayer/VideoPlayerPlayController;->curtime:Landroid/widget/TextView;

    iget-object v0, p0, Lcom/konka/videoPlayer/VideoPlayerPlayController;->mVideoPlayer:Lcom/konka/videoPlayer/videoPlayerActivity;

    const v1, 0x7f0b0027    # com.konka.mediaSharePlayer.R.id.video_playbar_time_all

    invoke-virtual {v0, v1}, Lcom/konka/videoPlayer/videoPlayerActivity;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    iput-object v0, p0, Lcom/konka/videoPlayer/VideoPlayerPlayController;->totalTime:Landroid/widget/TextView;

    iget-object v0, p0, Lcom/konka/videoPlayer/VideoPlayerPlayController;->mVideoPlayer:Lcom/konka/videoPlayer/videoPlayerActivity;

    const v1, 0x7f0b0026    # com.konka.mediaSharePlayer.R.id.video_seekbar

    invoke-virtual {v0, v1}, Lcom/konka/videoPlayer/videoPlayerActivity;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/SeekBar;

    iput-object v0, p0, Lcom/konka/videoPlayer/VideoPlayerPlayController;->seekbar:Landroid/widget/SeekBar;

    iget-object v0, p0, Lcom/konka/videoPlayer/VideoPlayerPlayController;->mVideoPlayer:Lcom/konka/videoPlayer/videoPlayerActivity;

    const v1, 0x7f0b0020    # com.konka.mediaSharePlayer.R.id.video_control

    invoke-virtual {v0, v1}, Lcom/konka/videoPlayer/videoPlayerActivity;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/LinearLayout;

    iput-object v0, p0, Lcom/konka/videoPlayer/VideoPlayerPlayController;->videoControl:Landroid/widget/LinearLayout;

    iget-object v0, p0, Lcom/konka/videoPlayer/VideoPlayerPlayController;->buttonPlay:Landroid/widget/Button;

    new-instance v1, Lcom/konka/videoPlayer/VideoPlayerPlayController$ItemOnClickEvent;

    invoke-direct {v1, p0}, Lcom/konka/videoPlayer/VideoPlayerPlayController$ItemOnClickEvent;-><init>(Lcom/konka/videoPlayer/VideoPlayerPlayController;)V

    invoke-virtual {v0, v1}, Landroid/widget/Button;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    iget-object v0, p0, Lcom/konka/videoPlayer/VideoPlayerPlayController;->buttonPause:Landroid/widget/Button;

    new-instance v1, Lcom/konka/videoPlayer/VideoPlayerPlayController$ItemOnClickEvent;

    invoke-direct {v1, p0}, Lcom/konka/videoPlayer/VideoPlayerPlayController$ItemOnClickEvent;-><init>(Lcom/konka/videoPlayer/VideoPlayerPlayController;)V

    invoke-virtual {v0, v1}, Landroid/widget/Button;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    iget-object v0, p0, Lcom/konka/videoPlayer/VideoPlayerPlayController;->buttonPre:Landroid/widget/Button;

    new-instance v1, Lcom/konka/videoPlayer/VideoPlayerPlayController$ItemOnClickEvent;

    invoke-direct {v1, p0}, Lcom/konka/videoPlayer/VideoPlayerPlayController$ItemOnClickEvent;-><init>(Lcom/konka/videoPlayer/VideoPlayerPlayController;)V

    invoke-virtual {v0, v1}, Landroid/widget/Button;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    iget-object v0, p0, Lcom/konka/videoPlayer/VideoPlayerPlayController;->buttonNext:Landroid/widget/Button;

    new-instance v1, Lcom/konka/videoPlayer/VideoPlayerPlayController$ItemOnClickEvent;

    invoke-direct {v1, p0}, Lcom/konka/videoPlayer/VideoPlayerPlayController$ItemOnClickEvent;-><init>(Lcom/konka/videoPlayer/VideoPlayerPlayController;)V

    invoke-virtual {v0, v1}, Landroid/widget/Button;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    iget-object v0, p0, Lcom/konka/videoPlayer/VideoPlayerPlayController;->seekbar:Landroid/widget/SeekBar;

    new-instance v1, Lcom/konka/videoPlayer/VideoPlayerPlayController$ItemOnClickEvent;

    invoke-direct {v1, p0}, Lcom/konka/videoPlayer/VideoPlayerPlayController$ItemOnClickEvent;-><init>(Lcom/konka/videoPlayer/VideoPlayerPlayController;)V

    invoke-virtual {v0, v1}, Landroid/widget/SeekBar;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    iget-object v0, p0, Lcom/konka/videoPlayer/VideoPlayerPlayController;->seekbar:Landroid/widget/SeekBar;

    new-instance v1, Lcom/konka/videoPlayer/VideoPlayerPlayController$1;

    invoke-direct {v1, p0}, Lcom/konka/videoPlayer/VideoPlayerPlayController$1;-><init>(Lcom/konka/videoPlayer/VideoPlayerPlayController;)V

    invoke-virtual {v0, v1}, Landroid/widget/SeekBar;->setOnSeekBarChangeListener(Landroid/widget/SeekBar$OnSeekBarChangeListener;)V

    return-void
.end method

.method public initSeekBar()V
    .locals 0

    return-void
.end method

.method public refresh()V
    .locals 4

    :try_start_0
    iget-object v1, p0, Lcom/konka/videoPlayer/VideoPlayerPlayController;->TAG:Ljava/lang/String;

    new-instance v2, Ljava/lang/StringBuilder;

    const-string v3, "current: "

    invoke-direct {v2, v3}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    iget v3, p0, Lcom/konka/videoPlayer/VideoPlayerPlayController;->current:I

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    iget-object v1, p0, Lcom/konka/videoPlayer/VideoPlayerPlayController;->TAG:Ljava/lang/String;

    new-instance v2, Ljava/lang/StringBuilder;

    const-string v3, "duration: "

    invoke-direct {v2, v3}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    iget v3, p0, Lcom/konka/videoPlayer/VideoPlayerPlayController;->duration:I

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    iget-object v1, p0, Lcom/konka/videoPlayer/VideoPlayerPlayController;->seekbar:Landroid/widget/SeekBar;

    iget v2, p0, Lcom/konka/videoPlayer/VideoPlayerPlayController;->duration:I

    invoke-virtual {v1, v2}, Landroid/widget/SeekBar;->setMax(I)V

    iget-object v1, p0, Lcom/konka/videoPlayer/VideoPlayerPlayController;->seekbar:Landroid/widget/SeekBar;

    iget v2, p0, Lcom/konka/videoPlayer/VideoPlayerPlayController;->current:I

    invoke-virtual {v1, v2}, Landroid/widget/SeekBar;->setProgress(I)V

    iget-object v1, p0, Lcom/konka/videoPlayer/VideoPlayerPlayController;->seekbar:Landroid/widget/SeekBar;

    invoke-virtual {v1}, Landroid/widget/SeekBar;->postInvalidate()V
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    :goto_0
    return-void

    :catch_0
    move-exception v0

    invoke-virtual {p0}, Lcom/konka/videoPlayer/VideoPlayerPlayController;->initSeekBar()V

    invoke-virtual {v0}, Ljava/lang/Exception;->printStackTrace()V

    goto :goto_0
.end method
