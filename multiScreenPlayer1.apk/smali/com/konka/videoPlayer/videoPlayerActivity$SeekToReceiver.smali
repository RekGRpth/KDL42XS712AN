.class public Lcom/konka/videoPlayer/videoPlayerActivity$SeekToReceiver;
.super Landroid/content/BroadcastReceiver;
.source "videoPlayerActivity.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/konka/videoPlayer/videoPlayerActivity;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x1
    name = "SeekToReceiver"
.end annotation


# instance fields
.field final synthetic this$0:Lcom/konka/videoPlayer/videoPlayerActivity;


# direct methods
.method public constructor <init>(Lcom/konka/videoPlayer/videoPlayerActivity;)V
    .locals 0

    iput-object p1, p0, Lcom/konka/videoPlayer/videoPlayerActivity$SeekToReceiver;->this$0:Lcom/konka/videoPlayer/videoPlayerActivity;

    invoke-direct {p0}, Landroid/content/BroadcastReceiver;-><init>()V

    return-void
.end method


# virtual methods
.method public onReceive(Landroid/content/Context;Landroid/content/Intent;)V
    .locals 12
    .param p1    # Landroid/content/Context;
    .param p2    # Landroid/content/Intent;

    const/4 v11, -0x1

    const/4 v10, -0x2

    const/4 v9, 0x0

    const/4 v8, 0x1

    const-string v4, "seekto"

    invoke-virtual {p2, v4, v9}, Landroid/content/Intent;->getIntExtra(Ljava/lang/String;I)I

    move-result v3

    const-string v4, "musicTag"

    new-instance v5, Ljava/lang/StringBuilder;

    const-string v6, "seekto: "

    invoke-direct {v5, v6}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v5, v3}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    invoke-static {v4, v5}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    const-string v4, "pressStatus"

    invoke-virtual {p2, v4, v9}, Landroid/content/Intent;->getIntExtra(Ljava/lang/String;I)I

    move-result v0

    const-string v4, "wangjinxin"

    new-instance v5, Ljava/lang/StringBuilder;

    const-string v6, "PressStatus == "

    invoke-direct {v5, v6}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v5, v0}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    invoke-static {v4, v5}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    if-nez v0, :cond_1

    if-ltz v3, :cond_0

    iget-object v4, p0, Lcom/konka/videoPlayer/videoPlayerActivity$SeekToReceiver;->this$0:Lcom/konka/videoPlayer/videoPlayerActivity;

    # getter for: Lcom/konka/videoPlayer/videoPlayerActivity;->mVideoView:Lcom/konka/videoPlayer/VideoView;
    invoke-static {v4}, Lcom/konka/videoPlayer/videoPlayerActivity;->access$7(Lcom/konka/videoPlayer/videoPlayerActivity;)Lcom/konka/videoPlayer/VideoView;

    move-result-object v4

    invoke-virtual {v4}, Lcom/konka/videoPlayer/VideoView;->getDuration()I

    move-result v4

    if-lt v4, v3, :cond_0

    iget-object v4, p0, Lcom/konka/videoPlayer/videoPlayerActivity$SeekToReceiver;->this$0:Lcom/konka/videoPlayer/videoPlayerActivity;

    invoke-virtual {v4, v3, v8}, Lcom/konka/videoPlayer/videoPlayerActivity;->videoPlayerSeek(II)V

    :cond_0
    :goto_0
    return-void

    :cond_1
    const/4 v4, 0x2

    if-ne v0, v4, :cond_6

    if-ne v3, v11, :cond_5

    invoke-static {v8}, Lcom/konka/videoPlayer/videoPlayerActivity;->access$37(Z)V

    :cond_2
    :goto_1
    iget-object v4, p0, Lcom/konka/videoPlayer/videoPlayerActivity$SeekToReceiver;->this$0:Lcom/konka/videoPlayer/videoPlayerActivity;

    # getter for: Lcom/konka/videoPlayer/videoPlayerActivity;->mVideoView:Lcom/konka/videoPlayer/VideoView;
    invoke-static {v4}, Lcom/konka/videoPlayer/videoPlayerActivity;->access$7(Lcom/konka/videoPlayer/videoPlayerActivity;)Lcom/konka/videoPlayer/VideoView;

    move-result-object v4

    invoke-virtual {v4}, Lcom/konka/videoPlayer/VideoView;->isPlaying()Z

    move-result v4

    if-eqz v4, :cond_3

    iget-object v4, p0, Lcom/konka/videoPlayer/videoPlayerActivity$SeekToReceiver;->this$0:Lcom/konka/videoPlayer/videoPlayerActivity;

    # getter for: Lcom/konka/videoPlayer/videoPlayerActivity;->mVideoView:Lcom/konka/videoPlayer/VideoView;
    invoke-static {v4}, Lcom/konka/videoPlayer/videoPlayerActivity;->access$7(Lcom/konka/videoPlayer/videoPlayerActivity;)Lcom/konka/videoPlayer/VideoView;

    move-result-object v4

    invoke-virtual {v4}, Lcom/konka/videoPlayer/VideoView;->pause()V

    iget-object v4, p0, Lcom/konka/videoPlayer/videoPlayerActivity$SeekToReceiver;->this$0:Lcom/konka/videoPlayer/videoPlayerActivity;

    iput-boolean v8, v4, Lcom/konka/videoPlayer/videoPlayerActivity;->isPause:Z

    :cond_3
    iget-object v4, p0, Lcom/konka/videoPlayer/videoPlayerActivity$SeekToReceiver;->this$0:Lcom/konka/videoPlayer/videoPlayerActivity;

    # getter for: Lcom/konka/videoPlayer/videoPlayerActivity;->isControllerShow:Z
    invoke-static {v4}, Lcom/konka/videoPlayer/videoPlayerActivity;->access$38(Lcom/konka/videoPlayer/videoPlayerActivity;)Z

    move-result v4

    if-nez v4, :cond_4

    iget-object v4, p0, Lcom/konka/videoPlayer/videoPlayerActivity$SeekToReceiver;->this$0:Lcom/konka/videoPlayer/videoPlayerActivity;

    invoke-virtual {v4}, Lcom/konka/videoPlayer/videoPlayerActivity;->offController()V

    :cond_4
    # getter for: Lcom/konka/videoPlayer/videoPlayerActivity;->mLongPressTimes:I
    invoke-static {}, Lcom/konka/videoPlayer/videoPlayerActivity;->access$0()I

    move-result v4

    add-int/lit8 v4, v4, 0x1

    invoke-static {v4}, Lcom/konka/videoPlayer/videoPlayerActivity;->access$39(I)V

    goto :goto_0

    :cond_5
    if-ne v3, v10, :cond_2

    invoke-static {v9}, Lcom/konka/videoPlayer/videoPlayerActivity;->access$37(Z)V

    goto :goto_1

    :cond_6
    if-ne v0, v8, :cond_0

    iget-object v4, p0, Lcom/konka/videoPlayer/videoPlayerActivity$SeekToReceiver;->this$0:Lcom/konka/videoPlayer/videoPlayerActivity;

    # getter for: Lcom/konka/videoPlayer/videoPlayerActivity;->isControllerShow:Z
    invoke-static {v4}, Lcom/konka/videoPlayer/videoPlayerActivity;->access$38(Lcom/konka/videoPlayer/videoPlayerActivity;)Z

    move-result v4

    if-nez v4, :cond_7

    iget-object v4, p0, Lcom/konka/videoPlayer/videoPlayerActivity$SeekToReceiver;->this$0:Lcom/konka/videoPlayer/videoPlayerActivity;

    invoke-virtual {v4}, Lcom/konka/videoPlayer/videoPlayerActivity;->offController()V

    :cond_7
    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v1

    const-string v4, "wangjinxin"

    new-instance v5, Ljava/lang/StringBuilder;

    const-string v6, "now time = "

    invoke-direct {v5, v6}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v5, v1, v2}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v5

    const-string v6, "preTime = "

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    # getter for: Lcom/konka/videoPlayer/videoPlayerActivity;->preTime:J
    invoke-static {}, Lcom/konka/videoPlayer/videoPlayerActivity;->access$40()J

    move-result-wide v6

    invoke-virtual {v5, v6, v7}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    invoke-static {v4, v5}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    # getter for: Lcom/konka/videoPlayer/videoPlayerActivity;->preTime:J
    invoke-static {}, Lcom/konka/videoPlayer/videoPlayerActivity;->access$40()J

    move-result-wide v4

    sub-long v4, v1, v4

    const-wide/16 v6, 0x3e8

    cmp-long v4, v4, v6

    if-gez v4, :cond_8

    const-string v4, "wangjinxin"

    const-string v5, "click too fast!!!"

    invoke-static {v4, v5}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    goto/16 :goto_0

    :cond_8
    invoke-static {v1, v2}, Lcom/konka/videoPlayer/videoPlayerActivity;->access$41(J)V

    if-ne v3, v11, :cond_a

    iget-object v4, p0, Lcom/konka/videoPlayer/videoPlayerActivity$SeekToReceiver;->this$0:Lcom/konka/videoPlayer/videoPlayerActivity;

    # getter for: Lcom/konka/videoPlayer/videoPlayerActivity;->mLongPressTimes:I
    invoke-static {}, Lcom/konka/videoPlayer/videoPlayerActivity;->access$0()I

    move-result v5

    invoke-virtual {v4, v11, v5}, Lcom/konka/videoPlayer/videoPlayerActivity;->videoPlayerSeek(II)V

    :cond_9
    :goto_2
    invoke-static {v9}, Lcom/konka/videoPlayer/videoPlayerActivity;->access$39(I)V

    goto/16 :goto_0

    :cond_a
    if-ne v3, v10, :cond_b

    iget-object v4, p0, Lcom/konka/videoPlayer/videoPlayerActivity$SeekToReceiver;->this$0:Lcom/konka/videoPlayer/videoPlayerActivity;

    # getter for: Lcom/konka/videoPlayer/videoPlayerActivity;->mLongPressTimes:I
    invoke-static {}, Lcom/konka/videoPlayer/videoPlayerActivity;->access$0()I

    move-result v5

    invoke-virtual {v4, v10, v5}, Lcom/konka/videoPlayer/videoPlayerActivity;->videoPlayerSeek(II)V

    goto :goto_2

    :cond_b
    if-ltz v3, :cond_9

    iget-object v4, p0, Lcom/konka/videoPlayer/videoPlayerActivity$SeekToReceiver;->this$0:Lcom/konka/videoPlayer/videoPlayerActivity;

    # getter for: Lcom/konka/videoPlayer/videoPlayerActivity;->mVideoView:Lcom/konka/videoPlayer/VideoView;
    invoke-static {v4}, Lcom/konka/videoPlayer/videoPlayerActivity;->access$7(Lcom/konka/videoPlayer/videoPlayerActivity;)Lcom/konka/videoPlayer/VideoView;

    move-result-object v4

    invoke-virtual {v4}, Lcom/konka/videoPlayer/VideoView;->getDuration()I

    move-result v4

    if-lt v4, v3, :cond_9

    iget-object v4, p0, Lcom/konka/videoPlayer/videoPlayerActivity$SeekToReceiver;->this$0:Lcom/konka/videoPlayer/videoPlayerActivity;

    invoke-virtual {v4, v3, v8}, Lcom/konka/videoPlayer/videoPlayerActivity;->videoPlayerSeek(II)V

    goto :goto_2
.end method
