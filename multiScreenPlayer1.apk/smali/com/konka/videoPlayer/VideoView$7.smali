.class Lcom/konka/videoPlayer/VideoView$7;
.super Ljava/lang/Object;
.source "VideoView.java"

# interfaces
.implements Landroid/view/SurfaceHolder$Callback;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/konka/videoPlayer/VideoView;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/konka/videoPlayer/VideoView;


# direct methods
.method constructor <init>(Lcom/konka/videoPlayer/VideoView;)V
    .locals 0

    iput-object p1, p0, Lcom/konka/videoPlayer/VideoView$7;->this$0:Lcom/konka/videoPlayer/VideoView;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public surfaceChanged(Landroid/view/SurfaceHolder;III)V
    .locals 2
    .param p1    # Landroid/view/SurfaceHolder;
    .param p2    # I
    .param p3    # I
    .param p4    # I

    iget-object v0, p0, Lcom/konka/videoPlayer/VideoView$7;->this$0:Lcom/konka/videoPlayer/VideoView;

    invoke-static {v0, p3}, Lcom/konka/videoPlayer/VideoView;->access$19(Lcom/konka/videoPlayer/VideoView;I)V

    iget-object v0, p0, Lcom/konka/videoPlayer/VideoView$7;->this$0:Lcom/konka/videoPlayer/VideoView;

    invoke-static {v0, p4}, Lcom/konka/videoPlayer/VideoView;->access$20(Lcom/konka/videoPlayer/VideoView;I)V

    iget-object v0, p0, Lcom/konka/videoPlayer/VideoView$7;->this$0:Lcom/konka/videoPlayer/VideoView;

    iget-object v0, v0, Lcom/konka/videoPlayer/VideoView;->mMediaPlayer:Landroid/media/MediaPlayer;

    if-eqz v0, :cond_1

    iget-object v0, p0, Lcom/konka/videoPlayer/VideoView$7;->this$0:Lcom/konka/videoPlayer/VideoView;

    # getter for: Lcom/konka/videoPlayer/VideoView;->mIsPrepared:Z
    invoke-static {v0}, Lcom/konka/videoPlayer/VideoView;->access$21(Lcom/konka/videoPlayer/VideoView;)Z

    move-result v0

    if-eqz v0, :cond_1

    iget-object v0, p0, Lcom/konka/videoPlayer/VideoView$7;->this$0:Lcom/konka/videoPlayer/VideoView;

    # getter for: Lcom/konka/videoPlayer/VideoView;->mVideoWidth:I
    invoke-static {v0}, Lcom/konka/videoPlayer/VideoView;->access$3(Lcom/konka/videoPlayer/VideoView;)I

    move-result v0

    if-ne v0, p3, :cond_1

    iget-object v0, p0, Lcom/konka/videoPlayer/VideoView$7;->this$0:Lcom/konka/videoPlayer/VideoView;

    # getter for: Lcom/konka/videoPlayer/VideoView;->mVideoHeight:I
    invoke-static {v0}, Lcom/konka/videoPlayer/VideoView;->access$4(Lcom/konka/videoPlayer/VideoView;)I

    move-result v0

    if-ne v0, p4, :cond_1

    iget-object v0, p0, Lcom/konka/videoPlayer/VideoView$7;->this$0:Lcom/konka/videoPlayer/VideoView;

    # getter for: Lcom/konka/videoPlayer/VideoView;->mSeekWhenPrepared:I
    invoke-static {v0}, Lcom/konka/videoPlayer/VideoView;->access$10(Lcom/konka/videoPlayer/VideoView;)I

    move-result v0

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/konka/videoPlayer/VideoView$7;->this$0:Lcom/konka/videoPlayer/VideoView;

    iget-object v0, v0, Lcom/konka/videoPlayer/VideoView;->mMediaPlayer:Landroid/media/MediaPlayer;

    iget-object v1, p0, Lcom/konka/videoPlayer/VideoView$7;->this$0:Lcom/konka/videoPlayer/VideoView;

    # getter for: Lcom/konka/videoPlayer/VideoView;->mSeekWhenPrepared:I
    invoke-static {v1}, Lcom/konka/videoPlayer/VideoView;->access$10(Lcom/konka/videoPlayer/VideoView;)I

    move-result v1

    invoke-virtual {v0, v1}, Landroid/media/MediaPlayer;->seekTo(I)V

    iget-object v0, p0, Lcom/konka/videoPlayer/VideoView$7;->this$0:Lcom/konka/videoPlayer/VideoView;

    const/4 v1, 0x0

    invoke-static {v0, v1}, Lcom/konka/videoPlayer/VideoView;->access$11(Lcom/konka/videoPlayer/VideoView;I)V

    :cond_0
    iget-object v0, p0, Lcom/konka/videoPlayer/VideoView$7;->this$0:Lcom/konka/videoPlayer/VideoView;

    iget-object v0, v0, Lcom/konka/videoPlayer/VideoView;->mMediaPlayer:Landroid/media/MediaPlayer;

    invoke-virtual {v0}, Landroid/media/MediaPlayer;->start()V

    iget-object v0, p0, Lcom/konka/videoPlayer/VideoView$7;->this$0:Lcom/konka/videoPlayer/VideoView;

    # getter for: Lcom/konka/videoPlayer/VideoView;->mMediaController:Landroid/widget/MediaController;
    invoke-static {v0}, Lcom/konka/videoPlayer/VideoView;->access$7(Lcom/konka/videoPlayer/VideoView;)Landroid/widget/MediaController;

    move-result-object v0

    if-eqz v0, :cond_1

    iget-object v0, p0, Lcom/konka/videoPlayer/VideoView$7;->this$0:Lcom/konka/videoPlayer/VideoView;

    # getter for: Lcom/konka/videoPlayer/VideoView;->mMediaController:Landroid/widget/MediaController;
    invoke-static {v0}, Lcom/konka/videoPlayer/VideoView;->access$7(Lcom/konka/videoPlayer/VideoView;)Landroid/widget/MediaController;

    move-result-object v0

    invoke-virtual {v0}, Landroid/widget/MediaController;->show()V

    :cond_1
    return-void
.end method

.method public surfaceCreated(Landroid/view/SurfaceHolder;)V
    .locals 1
    .param p1    # Landroid/view/SurfaceHolder;

    iget-object v0, p0, Lcom/konka/videoPlayer/VideoView$7;->this$0:Lcom/konka/videoPlayer/VideoView;

    invoke-static {v0, p1}, Lcom/konka/videoPlayer/VideoView;->access$22(Lcom/konka/videoPlayer/VideoView;Landroid/view/SurfaceHolder;)V

    iget-object v0, p0, Lcom/konka/videoPlayer/VideoView$7;->this$0:Lcom/konka/videoPlayer/VideoView;

    # invokes: Lcom/konka/videoPlayer/VideoView;->openVideo()V
    invoke-static {v0}, Lcom/konka/videoPlayer/VideoView;->access$23(Lcom/konka/videoPlayer/VideoView;)V

    return-void
.end method

.method public surfaceDestroyed(Landroid/view/SurfaceHolder;)V
    .locals 2
    .param p1    # Landroid/view/SurfaceHolder;

    const/4 v1, 0x0

    iget-object v0, p0, Lcom/konka/videoPlayer/VideoView$7;->this$0:Lcom/konka/videoPlayer/VideoView;

    invoke-static {v0, v1}, Lcom/konka/videoPlayer/VideoView;->access$22(Lcom/konka/videoPlayer/VideoView;Landroid/view/SurfaceHolder;)V

    iget-object v0, p0, Lcom/konka/videoPlayer/VideoView$7;->this$0:Lcom/konka/videoPlayer/VideoView;

    # getter for: Lcom/konka/videoPlayer/VideoView;->mMediaController:Landroid/widget/MediaController;
    invoke-static {v0}, Lcom/konka/videoPlayer/VideoView;->access$7(Lcom/konka/videoPlayer/VideoView;)Landroid/widget/MediaController;

    move-result-object v0

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/konka/videoPlayer/VideoView$7;->this$0:Lcom/konka/videoPlayer/VideoView;

    # getter for: Lcom/konka/videoPlayer/VideoView;->mMediaController:Landroid/widget/MediaController;
    invoke-static {v0}, Lcom/konka/videoPlayer/VideoView;->access$7(Lcom/konka/videoPlayer/VideoView;)Landroid/widget/MediaController;

    move-result-object v0

    invoke-virtual {v0}, Landroid/widget/MediaController;->hide()V

    :cond_0
    iget-object v0, p0, Lcom/konka/videoPlayer/VideoView$7;->this$0:Lcom/konka/videoPlayer/VideoView;

    iget-object v0, v0, Lcom/konka/videoPlayer/VideoView;->mMediaPlayer:Landroid/media/MediaPlayer;

    if-eqz v0, :cond_1

    iget-object v0, p0, Lcom/konka/videoPlayer/VideoView$7;->this$0:Lcom/konka/videoPlayer/VideoView;

    iget-object v0, v0, Lcom/konka/videoPlayer/VideoView;->mMediaPlayer:Landroid/media/MediaPlayer;

    invoke-virtual {v0}, Landroid/media/MediaPlayer;->reset()V

    iget-object v0, p0, Lcom/konka/videoPlayer/VideoView$7;->this$0:Lcom/konka/videoPlayer/VideoView;

    iget-object v0, v0, Lcom/konka/videoPlayer/VideoView;->mMediaPlayer:Landroid/media/MediaPlayer;

    invoke-virtual {v0}, Landroid/media/MediaPlayer;->release()V

    iget-object v0, p0, Lcom/konka/videoPlayer/VideoView$7;->this$0:Lcom/konka/videoPlayer/VideoView;

    iput-object v1, v0, Lcom/konka/videoPlayer/VideoView;->mMediaPlayer:Landroid/media/MediaPlayer;

    :cond_1
    return-void
.end method
