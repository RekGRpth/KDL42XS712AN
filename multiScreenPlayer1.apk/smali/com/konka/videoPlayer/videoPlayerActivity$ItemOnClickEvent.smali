.class public Lcom/konka/videoPlayer/videoPlayerActivity$ItemOnClickEvent;
.super Ljava/lang/Object;
.source "videoPlayerActivity.java"

# interfaces
.implements Landroid/view/View$OnClickListener;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/konka/videoPlayer/videoPlayerActivity;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x1
    name = "ItemOnClickEvent"
.end annotation


# instance fields
.field final synthetic this$0:Lcom/konka/videoPlayer/videoPlayerActivity;


# direct methods
.method public constructor <init>(Lcom/konka/videoPlayer/videoPlayerActivity;)V
    .locals 0

    iput-object p1, p0, Lcom/konka/videoPlayer/videoPlayerActivity$ItemOnClickEvent;->this$0:Lcom/konka/videoPlayer/videoPlayerActivity;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onClick(Landroid/view/View;)V
    .locals 3
    .param p1    # Landroid/view/View;

    const/16 v2, 0x8

    const/4 v1, 0x0

    invoke-virtual {p1}, Landroid/view/View;->getId()I

    move-result v0

    packed-switch v0, :pswitch_data_0

    :goto_0
    return-void

    :pswitch_0
    iget-object v0, p0, Lcom/konka/videoPlayer/videoPlayerActivity$ItemOnClickEvent;->this$0:Lcom/konka/videoPlayer/videoPlayerActivity;

    invoke-virtual {v0}, Lcom/konka/videoPlayer/videoPlayerActivity;->setPlayState()V

    iget-object v0, p0, Lcom/konka/videoPlayer/videoPlayerActivity$ItemOnClickEvent;->this$0:Lcom/konka/videoPlayer/videoPlayerActivity;

    # getter for: Lcom/konka/videoPlayer/videoPlayerActivity;->buttonPlay:Landroid/widget/Button;
    invoke-static {v0}, Lcom/konka/videoPlayer/videoPlayerActivity;->access$15(Lcom/konka/videoPlayer/videoPlayerActivity;)Landroid/widget/Button;

    move-result-object v0

    invoke-virtual {v0, v2}, Landroid/widget/Button;->setVisibility(I)V

    iget-object v0, p0, Lcom/konka/videoPlayer/videoPlayerActivity$ItemOnClickEvent;->this$0:Lcom/konka/videoPlayer/videoPlayerActivity;

    # getter for: Lcom/konka/videoPlayer/videoPlayerActivity;->buttonPause:Landroid/widget/Button;
    invoke-static {v0}, Lcom/konka/videoPlayer/videoPlayerActivity;->access$16(Lcom/konka/videoPlayer/videoPlayerActivity;)Landroid/widget/Button;

    move-result-object v0

    invoke-virtual {v0, v1}, Landroid/widget/Button;->setVisibility(I)V

    goto :goto_0

    :pswitch_1
    iget-object v0, p0, Lcom/konka/videoPlayer/videoPlayerActivity$ItemOnClickEvent;->this$0:Lcom/konka/videoPlayer/videoPlayerActivity;

    invoke-virtual {v0}, Lcom/konka/videoPlayer/videoPlayerActivity;->setPlayState()V

    iget-object v0, p0, Lcom/konka/videoPlayer/videoPlayerActivity$ItemOnClickEvent;->this$0:Lcom/konka/videoPlayer/videoPlayerActivity;

    # getter for: Lcom/konka/videoPlayer/videoPlayerActivity;->buttonPlay:Landroid/widget/Button;
    invoke-static {v0}, Lcom/konka/videoPlayer/videoPlayerActivity;->access$15(Lcom/konka/videoPlayer/videoPlayerActivity;)Landroid/widget/Button;

    move-result-object v0

    invoke-virtual {v0, v1}, Landroid/widget/Button;->setVisibility(I)V

    iget-object v0, p0, Lcom/konka/videoPlayer/videoPlayerActivity$ItemOnClickEvent;->this$0:Lcom/konka/videoPlayer/videoPlayerActivity;

    # getter for: Lcom/konka/videoPlayer/videoPlayerActivity;->buttonPause:Landroid/widget/Button;
    invoke-static {v0}, Lcom/konka/videoPlayer/videoPlayerActivity;->access$16(Lcom/konka/videoPlayer/videoPlayerActivity;)Landroid/widget/Button;

    move-result-object v0

    invoke-virtual {v0, v2}, Landroid/widget/Button;->setVisibility(I)V

    goto :goto_0

    :pswitch_2
    iget-object v0, p0, Lcom/konka/videoPlayer/videoPlayerActivity$ItemOnClickEvent;->this$0:Lcom/konka/videoPlayer/videoPlayerActivity;

    const v1, 0x7f0900a5    # com.konka.mediaSharePlayer.R.string.forbidoption

    invoke-virtual {v0, v1}, Lcom/konka/videoPlayer/videoPlayerActivity;->toastMessage(I)V

    goto :goto_0

    :pswitch_data_0
    .packed-switch 0x7f0b0021
        :pswitch_2    # com.konka.mediaSharePlayer.R.id.video_btn_seekback
        :pswitch_0    # com.konka.mediaSharePlayer.R.id.video_btn_play
        :pswitch_1    # com.konka.mediaSharePlayer.R.id.video_btn_pause
        :pswitch_2    # com.konka.mediaSharePlayer.R.id.video_btn_seekforward
    .end packed-switch
.end method
