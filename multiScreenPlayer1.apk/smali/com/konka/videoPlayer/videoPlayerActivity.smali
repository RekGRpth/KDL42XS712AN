.class public Lcom/konka/videoPlayer/videoPlayerActivity;
.super Landroid/app/Activity;
.source "videoPlayerActivity.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/konka/videoPlayer/videoPlayerActivity$ItemOnClickEvent;,
        Lcom/konka/videoPlayer/videoPlayerActivity$SeekToReceiver;,
        Lcom/konka/videoPlayer/videoPlayerActivity$closeReceiver;,
        Lcom/konka/videoPlayer/videoPlayerActivity$playerDataReceiver;,
        Lcom/konka/videoPlayer/videoPlayerActivity$switchReceiver;
    }
.end annotation


# static fields
.field public static final TAG:Ljava/lang/String; = "VideoPlayer"

.field protected static isCanMove:Z

.field private static mLongPressTimes:I

.field private static mPressForward:Z

.field private static preTime:J


# instance fields
.field private buttonNext:Landroid/widget/Button;

.field private buttonPause:Landroid/widget/Button;

.field private buttonPlay:Landroid/widget/Button;

.field private buttonPre:Landroid/widget/Button;

.field protected classVideoSubtitle:Lcom/konka/videoPlayer/VideoPlayerSubTitle;

.field clockThread:Ljava/lang/Thread;

.field closePlayer:I

.field comefrom:I

.field private comfrom:I

.field private currentTime:I

.field private curtime:Landroid/widget/TextView;

.field dataReceiver:Lcom/konka/videoPlayer/videoPlayerActivity$playerDataReceiver;

.field private durTime:Landroid/widget/TextView;

.field private durationList:Ljava/util/ArrayList;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/ArrayList",
            "<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation
.end field

.field private handlerRefresh:Landroid/os/Handler;

.field private haveNextFlag:I

.field hideThread:Ljava/lang/Thread;

.field private isControllerShow:Z

.field isPause:Z

.field isResume:Z

.field isSeekTo:Z

.field private isSegVideo:Ljava/lang/Boolean;

.field isSend:Z

.field isplaying:Z

.field private isrun:Z

.field isshow:Z

.field private mAlterDialog:Landroid/app/AlertDialog;

.field private mCurrentPosOfUrlList:I

.field private mCurrentTimeSegVideo:J

.field private mHideController:Z

.field private mLengOfUrlList:I

.field private mPositionWhenPaused:I

.field private mSeekPoint:Z

.field private mSeekPosOfUrlList:I

.field private mTimesError:I

.field private mTotalTimeSegVideo:J

.field private mUri:Landroid/net/Uri;

.field private mVideoName:Ljava/lang/String;

.field private mVideoPlayer:Lcom/konka/videoPlayer/videoPlayerActivity;

.field private mVideoView:Lcom/konka/videoPlayer/VideoView;

.field mediaPlayerSwitch:Lcom/konka/videoPlayer/videoPlayerActivity$switchReceiver;

.field private mediaPlayerTag:Ljava/lang/String;

.field mediaSwitch:I

.field mediastop:I

.field private mobileIp:Ljava/lang/String;

.field private proDlg:Landroid/app/ProgressDialog;

.field refreshThread:Ljava/lang/Thread;

.field relayThread:Ljava/lang/Thread;

.field relayTimer:I

.field resumeAsk:I

.field resumePos:I

.field seekReceiver:Lcom/konka/videoPlayer/videoPlayerActivity$SeekToReceiver;

.field private seekbar:Landroid/widget/SeekBar;

.field private sendState:Lcom/konka/mediaSharePlayer/sendPlayerState;

.field private sourcePath:Ljava/lang/String;

.field private totalTime:I

.field private urlList:Ljava/util/ArrayList;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/ArrayList",
            "<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation
.end field

.field videoClose:Lcom/konka/videoPlayer/videoPlayerActivity$closeReceiver;

.field protected videoControl:Landroid/widget/LinearLayout;

.field private videoHandler:Landroid/os/Handler;


# direct methods
.method static constructor <clinit>()V
    .locals 2

    const/4 v0, 0x0

    sput-boolean v0, Lcom/konka/videoPlayer/videoPlayerActivity;->isCanMove:Z

    sput v0, Lcom/konka/videoPlayer/videoPlayerActivity;->mLongPressTimes:I

    sput-boolean v0, Lcom/konka/videoPlayer/videoPlayerActivity;->mPressForward:Z

    const-wide/16 v0, 0x0

    sput-wide v0, Lcom/konka/videoPlayer/videoPlayerActivity;->preTime:J

    return-void
.end method

.method public constructor <init>()V
    .locals 6

    const-wide/16 v4, 0x0

    const/4 v3, 0x1

    const/4 v2, 0x0

    const/4 v1, 0x0

    invoke-direct {p0}, Landroid/app/Activity;-><init>()V

    iput-object v2, p0, Lcom/konka/videoPlayer/videoPlayerActivity;->sourcePath:Ljava/lang/String;

    iput-boolean v1, p0, Lcom/konka/videoPlayer/videoPlayerActivity;->mHideController:Z

    iput v1, p0, Lcom/konka/videoPlayer/videoPlayerActivity;->haveNextFlag:I

    iput-object v2, p0, Lcom/konka/videoPlayer/videoPlayerActivity;->mobileIp:Ljava/lang/String;

    const/4 v0, -0x1

    iput v0, p0, Lcom/konka/videoPlayer/videoPlayerActivity;->mPositionWhenPaused:I

    iput-boolean v1, p0, Lcom/konka/videoPlayer/videoPlayerActivity;->isrun:Z

    iput-boolean v3, p0, Lcom/konka/videoPlayer/videoPlayerActivity;->isControllerShow:Z

    const-string v0, "VideoPlayerActivity"

    iput-object v0, p0, Lcom/konka/videoPlayer/videoPlayerActivity;->mediaPlayerTag:Ljava/lang/String;

    iput v1, p0, Lcom/konka/videoPlayer/videoPlayerActivity;->currentTime:I

    iput v1, p0, Lcom/konka/videoPlayer/videoPlayerActivity;->totalTime:I

    iput v1, p0, Lcom/konka/videoPlayer/videoPlayerActivity;->comefrom:I

    iput v1, p0, Lcom/konka/videoPlayer/videoPlayerActivity;->closePlayer:I

    iput-boolean v1, p0, Lcom/konka/videoPlayer/videoPlayerActivity;->isplaying:Z

    iput-boolean v3, p0, Lcom/konka/videoPlayer/videoPlayerActivity;->isshow:Z

    iput-boolean v3, p0, Lcom/konka/videoPlayer/videoPlayerActivity;->isResume:Z

    iput-object v2, p0, Lcom/konka/videoPlayer/videoPlayerActivity;->refreshThread:Ljava/lang/Thread;

    iput-object v2, p0, Lcom/konka/videoPlayer/videoPlayerActivity;->clockThread:Ljava/lang/Thread;

    iput-object v2, p0, Lcom/konka/videoPlayer/videoPlayerActivity;->hideThread:Ljava/lang/Thread;

    iput-object v2, p0, Lcom/konka/videoPlayer/videoPlayerActivity;->relayThread:Ljava/lang/Thread;

    iput v3, p0, Lcom/konka/videoPlayer/videoPlayerActivity;->mediastop:I

    iput v1, p0, Lcom/konka/videoPlayer/videoPlayerActivity;->resumePos:I

    iput v1, p0, Lcom/konka/videoPlayer/videoPlayerActivity;->resumeAsk:I

    iput-boolean v1, p0, Lcom/konka/videoPlayer/videoPlayerActivity;->isPause:Z

    iput v1, p0, Lcom/konka/videoPlayer/videoPlayerActivity;->relayTimer:I

    iput-boolean v1, p0, Lcom/konka/videoPlayer/videoPlayerActivity;->isSend:Z

    iput v3, p0, Lcom/konka/videoPlayer/videoPlayerActivity;->mediaSwitch:I

    iput-boolean v1, p0, Lcom/konka/videoPlayer/videoPlayerActivity;->isSeekTo:Z

    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, Lcom/konka/videoPlayer/videoPlayerActivity;->urlList:Ljava/util/ArrayList;

    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, Lcom/konka/videoPlayer/videoPlayerActivity;->durationList:Ljava/util/ArrayList;

    iput v1, p0, Lcom/konka/videoPlayer/videoPlayerActivity;->mLengOfUrlList:I

    iput v1, p0, Lcom/konka/videoPlayer/videoPlayerActivity;->mCurrentPosOfUrlList:I

    iput v1, p0, Lcom/konka/videoPlayer/videoPlayerActivity;->mSeekPosOfUrlList:I

    invoke-static {v1}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v0

    iput-object v0, p0, Lcom/konka/videoPlayer/videoPlayerActivity;->isSegVideo:Ljava/lang/Boolean;

    iput-wide v4, p0, Lcom/konka/videoPlayer/videoPlayerActivity;->mTotalTimeSegVideo:J

    iput-wide v4, p0, Lcom/konka/videoPlayer/videoPlayerActivity;->mCurrentTimeSegVideo:J

    iput-boolean v1, p0, Lcom/konka/videoPlayer/videoPlayerActivity;->mSeekPoint:Z

    const-string v0, ""

    iput-object v0, p0, Lcom/konka/videoPlayer/videoPlayerActivity;->mVideoName:Ljava/lang/String;

    iput v1, p0, Lcom/konka/videoPlayer/videoPlayerActivity;->comfrom:I

    iput v1, p0, Lcom/konka/videoPlayer/videoPlayerActivity;->mTimesError:I

    new-instance v0, Lcom/konka/videoPlayer/videoPlayerActivity$1;

    invoke-direct {v0, p0}, Lcom/konka/videoPlayer/videoPlayerActivity$1;-><init>(Lcom/konka/videoPlayer/videoPlayerActivity;)V

    iput-object v0, p0, Lcom/konka/videoPlayer/videoPlayerActivity;->handlerRefresh:Landroid/os/Handler;

    new-instance v0, Lcom/konka/videoPlayer/videoPlayerActivity$2;

    invoke-direct {v0, p0}, Lcom/konka/videoPlayer/videoPlayerActivity$2;-><init>(Lcom/konka/videoPlayer/videoPlayerActivity;)V

    iput-object v0, p0, Lcom/konka/videoPlayer/videoPlayerActivity;->videoHandler:Landroid/os/Handler;

    return-void
.end method

.method static synthetic access$0()I
    .locals 1

    sget v0, Lcom/konka/videoPlayer/videoPlayerActivity;->mLongPressTimes:I

    return v0
.end method

.method static synthetic access$1(Lcom/konka/videoPlayer/videoPlayerActivity;)Ljava/lang/Boolean;
    .locals 1

    iget-object v0, p0, Lcom/konka/videoPlayer/videoPlayerActivity;->isSegVideo:Ljava/lang/Boolean;

    return-object v0
.end method

.method static synthetic access$10(Lcom/konka/videoPlayer/videoPlayerActivity;)V
    .locals 0

    invoke-direct {p0}, Lcom/konka/videoPlayer/videoPlayerActivity;->playMediaSource()V

    return-void
.end method

.method static synthetic access$11(Lcom/konka/videoPlayer/videoPlayerActivity;Z)V
    .locals 0

    iput-boolean p1, p0, Lcom/konka/videoPlayer/videoPlayerActivity;->isrun:Z

    return-void
.end method

.method static synthetic access$12(Lcom/konka/videoPlayer/videoPlayerActivity;)I
    .locals 1

    iget v0, p0, Lcom/konka/videoPlayer/videoPlayerActivity;->mCurrentPosOfUrlList:I

    return v0
.end method

.method static synthetic access$13(Lcom/konka/videoPlayer/videoPlayerActivity;)I
    .locals 1

    iget v0, p0, Lcom/konka/videoPlayer/videoPlayerActivity;->mLengOfUrlList:I

    return v0
.end method

.method static synthetic access$14(Lcom/konka/videoPlayer/videoPlayerActivity;)V
    .locals 0

    invoke-direct {p0}, Lcom/konka/videoPlayer/videoPlayerActivity;->playSegmentMediaSource()V

    return-void
.end method

.method static synthetic access$15(Lcom/konka/videoPlayer/videoPlayerActivity;)Landroid/widget/Button;
    .locals 1

    iget-object v0, p0, Lcom/konka/videoPlayer/videoPlayerActivity;->buttonPlay:Landroid/widget/Button;

    return-object v0
.end method

.method static synthetic access$16(Lcom/konka/videoPlayer/videoPlayerActivity;)Landroid/widget/Button;
    .locals 1

    iget-object v0, p0, Lcom/konka/videoPlayer/videoPlayerActivity;->buttonPause:Landroid/widget/Button;

    return-object v0
.end method

.method static synthetic access$17(Lcom/konka/videoPlayer/videoPlayerActivity;)Landroid/app/AlertDialog;
    .locals 1

    iget-object v0, p0, Lcom/konka/videoPlayer/videoPlayerActivity;->mAlterDialog:Landroid/app/AlertDialog;

    return-object v0
.end method

.method static synthetic access$18(Lcom/konka/videoPlayer/videoPlayerActivity;I)V
    .locals 0

    iput p1, p0, Lcom/konka/videoPlayer/videoPlayerActivity;->comfrom:I

    return-void
.end method

.method static synthetic access$19(Lcom/konka/videoPlayer/videoPlayerActivity;)I
    .locals 1

    iget v0, p0, Lcom/konka/videoPlayer/videoPlayerActivity;->comfrom:I

    return v0
.end method

.method static synthetic access$2(Lcom/konka/videoPlayer/videoPlayerActivity;)I
    .locals 1

    invoke-direct {p0}, Lcom/konka/videoPlayer/videoPlayerActivity;->getCurrentPos()I

    move-result v0

    return v0
.end method

.method static synthetic access$20(Lcom/konka/videoPlayer/videoPlayerActivity;Z)V
    .locals 0

    iput-boolean p1, p0, Lcom/konka/videoPlayer/videoPlayerActivity;->mHideController:Z

    return-void
.end method

.method static synthetic access$21(Lcom/konka/videoPlayer/videoPlayerActivity;Ljava/util/ArrayList;)V
    .locals 0

    iput-object p1, p0, Lcom/konka/videoPlayer/videoPlayerActivity;->urlList:Ljava/util/ArrayList;

    return-void
.end method

.method static synthetic access$22(Lcom/konka/videoPlayer/videoPlayerActivity;)Ljava/util/ArrayList;
    .locals 1

    iget-object v0, p0, Lcom/konka/videoPlayer/videoPlayerActivity;->urlList:Ljava/util/ArrayList;

    return-object v0
.end method

.method static synthetic access$23(Lcom/konka/videoPlayer/videoPlayerActivity;I)V
    .locals 0

    iput p1, p0, Lcom/konka/videoPlayer/videoPlayerActivity;->mLengOfUrlList:I

    return-void
.end method

.method static synthetic access$24(Lcom/konka/videoPlayer/videoPlayerActivity;I)V
    .locals 0

    iput p1, p0, Lcom/konka/videoPlayer/videoPlayerActivity;->mCurrentPosOfUrlList:I

    return-void
.end method

.method static synthetic access$25(Lcom/konka/videoPlayer/videoPlayerActivity;Ljava/util/ArrayList;)V
    .locals 0

    iput-object p1, p0, Lcom/konka/videoPlayer/videoPlayerActivity;->durationList:Ljava/util/ArrayList;

    return-void
.end method

.method static synthetic access$26(Lcom/konka/videoPlayer/videoPlayerActivity;)Ljava/util/ArrayList;
    .locals 1

    iget-object v0, p0, Lcom/konka/videoPlayer/videoPlayerActivity;->durationList:Ljava/util/ArrayList;

    return-object v0
.end method

.method static synthetic access$27(Lcom/konka/videoPlayer/videoPlayerActivity;)J
    .locals 2

    iget-wide v0, p0, Lcom/konka/videoPlayer/videoPlayerActivity;->mTotalTimeSegVideo:J

    return-wide v0
.end method

.method static synthetic access$28(Lcom/konka/videoPlayer/videoPlayerActivity;J)V
    .locals 0

    iput-wide p1, p0, Lcom/konka/videoPlayer/videoPlayerActivity;->mTotalTimeSegVideo:J

    return-void
.end method

.method static synthetic access$29(Lcom/konka/videoPlayer/videoPlayerActivity;Ljava/lang/String;)V
    .locals 0

    iput-object p1, p0, Lcom/konka/videoPlayer/videoPlayerActivity;->sourcePath:Ljava/lang/String;

    return-void
.end method

.method static synthetic access$3(Lcom/konka/videoPlayer/videoPlayerActivity;)I
    .locals 1

    invoke-direct {p0}, Lcom/konka/videoPlayer/videoPlayerActivity;->getTotalDuration()I

    move-result v0

    return v0
.end method

.method static synthetic access$30(Lcom/konka/videoPlayer/videoPlayerActivity;Ljava/lang/String;)V
    .locals 0

    iput-object p1, p0, Lcom/konka/videoPlayer/videoPlayerActivity;->mVideoName:Ljava/lang/String;

    return-void
.end method

.method static synthetic access$31(Lcom/konka/videoPlayer/videoPlayerActivity;I)V
    .locals 0

    iput p1, p0, Lcom/konka/videoPlayer/videoPlayerActivity;->haveNextFlag:I

    return-void
.end method

.method static synthetic access$32(Lcom/konka/videoPlayer/videoPlayerActivity;)I
    .locals 1

    iget v0, p0, Lcom/konka/videoPlayer/videoPlayerActivity;->haveNextFlag:I

    return v0
.end method

.method static synthetic access$33(Lcom/konka/videoPlayer/videoPlayerActivity;Ljava/lang/Boolean;)V
    .locals 0

    iput-object p1, p0, Lcom/konka/videoPlayer/videoPlayerActivity;->isSegVideo:Ljava/lang/Boolean;

    return-void
.end method

.method static synthetic access$34(Lcom/konka/videoPlayer/videoPlayerActivity;)Ljava/lang/String;
    .locals 1

    iget-object v0, p0, Lcom/konka/videoPlayer/videoPlayerActivity;->mVideoName:Ljava/lang/String;

    return-object v0
.end method

.method static synthetic access$35(Lcom/konka/videoPlayer/videoPlayerActivity;Z)V
    .locals 0

    iput-boolean p1, p0, Lcom/konka/videoPlayer/videoPlayerActivity;->mSeekPoint:Z

    return-void
.end method

.method static synthetic access$36(Lcom/konka/videoPlayer/videoPlayerActivity;)Landroid/os/Handler;
    .locals 1

    iget-object v0, p0, Lcom/konka/videoPlayer/videoPlayerActivity;->videoHandler:Landroid/os/Handler;

    return-object v0
.end method

.method static synthetic access$37(Z)V
    .locals 0

    sput-boolean p0, Lcom/konka/videoPlayer/videoPlayerActivity;->mPressForward:Z

    return-void
.end method

.method static synthetic access$38(Lcom/konka/videoPlayer/videoPlayerActivity;)Z
    .locals 1

    iget-boolean v0, p0, Lcom/konka/videoPlayer/videoPlayerActivity;->isControllerShow:Z

    return v0
.end method

.method static synthetic access$39(I)V
    .locals 0

    sput p0, Lcom/konka/videoPlayer/videoPlayerActivity;->mLongPressTimes:I

    return-void
.end method

.method static synthetic access$4()Z
    .locals 1

    sget-boolean v0, Lcom/konka/videoPlayer/videoPlayerActivity;->mPressForward:Z

    return v0
.end method

.method static synthetic access$40()J
    .locals 2

    sget-wide v0, Lcom/konka/videoPlayer/videoPlayerActivity;->preTime:J

    return-wide v0
.end method

.method static synthetic access$41(J)V
    .locals 0

    sput-wide p0, Lcom/konka/videoPlayer/videoPlayerActivity;->preTime:J

    return-void
.end method

.method static synthetic access$42(Lcom/konka/videoPlayer/videoPlayerActivity;)V
    .locals 0

    invoke-direct {p0}, Lcom/konka/videoPlayer/videoPlayerActivity;->registerDataReceiver()V

    return-void
.end method

.method static synthetic access$43(Lcom/konka/videoPlayer/videoPlayerActivity;)Z
    .locals 1

    iget-boolean v0, p0, Lcom/konka/videoPlayer/videoPlayerActivity;->isrun:Z

    return v0
.end method

.method static synthetic access$44(Lcom/konka/videoPlayer/videoPlayerActivity;)Landroid/os/Handler;
    .locals 1

    iget-object v0, p0, Lcom/konka/videoPlayer/videoPlayerActivity;->handlerRefresh:Landroid/os/Handler;

    return-object v0
.end method

.method static synthetic access$45(Lcom/konka/videoPlayer/videoPlayerActivity;)I
    .locals 1

    iget v0, p0, Lcom/konka/videoPlayer/videoPlayerActivity;->mTimesError:I

    return v0
.end method

.method static synthetic access$46(Lcom/konka/videoPlayer/videoPlayerActivity;I)V
    .locals 0

    iput p1, p0, Lcom/konka/videoPlayer/videoPlayerActivity;->mTimesError:I

    return-void
.end method

.method static synthetic access$47(Lcom/konka/videoPlayer/videoPlayerActivity;II)V
    .locals 0

    invoke-direct {p0, p1, p2}, Lcom/konka/videoPlayer/videoPlayerActivity;->showProgressDialog(II)V

    return-void
.end method

.method static synthetic access$48(Lcom/konka/videoPlayer/videoPlayerActivity;Landroid/app/AlertDialog;)V
    .locals 0

    iput-object p1, p0, Lcom/konka/videoPlayer/videoPlayerActivity;->mAlterDialog:Landroid/app/AlertDialog;

    return-void
.end method

.method static synthetic access$49(Lcom/konka/videoPlayer/videoPlayerActivity;)Ljava/lang/String;
    .locals 1

    iget-object v0, p0, Lcom/konka/videoPlayer/videoPlayerActivity;->mediaPlayerTag:Ljava/lang/String;

    return-object v0
.end method

.method static synthetic access$5(Lcom/konka/videoPlayer/videoPlayerActivity;)Landroid/widget/SeekBar;
    .locals 1

    iget-object v0, p0, Lcom/konka/videoPlayer/videoPlayerActivity;->seekbar:Landroid/widget/SeekBar;

    return-object v0
.end method

.method static synthetic access$50(Lcom/konka/videoPlayer/videoPlayerActivity;)Z
    .locals 1

    iget-boolean v0, p0, Lcom/konka/videoPlayer/videoPlayerActivity;->mSeekPoint:Z

    return v0
.end method

.method static synthetic access$51(Lcom/konka/videoPlayer/videoPlayerActivity;)J
    .locals 2

    iget-wide v0, p0, Lcom/konka/videoPlayer/videoPlayerActivity;->mCurrentTimeSegVideo:J

    return-wide v0
.end method

.method static synthetic access$52(Lcom/konka/videoPlayer/videoPlayerActivity;)V
    .locals 0

    invoke-direct {p0}, Lcom/konka/videoPlayer/videoPlayerActivity;->showController()V

    return-void
.end method

.method static synthetic access$6(Lcom/konka/videoPlayer/videoPlayerActivity;)Landroid/widget/TextView;
    .locals 1

    iget-object v0, p0, Lcom/konka/videoPlayer/videoPlayerActivity;->curtime:Landroid/widget/TextView;

    return-object v0
.end method

.method static synthetic access$7(Lcom/konka/videoPlayer/videoPlayerActivity;)Lcom/konka/videoPlayer/VideoView;
    .locals 1

    iget-object v0, p0, Lcom/konka/videoPlayer/videoPlayerActivity;->mVideoView:Lcom/konka/videoPlayer/VideoView;

    return-object v0
.end method

.method static synthetic access$8(Lcom/konka/videoPlayer/videoPlayerActivity;)Ljava/lang/String;
    .locals 1

    iget-object v0, p0, Lcom/konka/videoPlayer/videoPlayerActivity;->sourcePath:Ljava/lang/String;

    return-object v0
.end method

.method static synthetic access$9(Lcom/konka/videoPlayer/videoPlayerActivity;)V
    .locals 0

    invoke-direct {p0}, Lcom/konka/videoPlayer/videoPlayerActivity;->videoPlayerStop()V

    return-void
.end method

.method private calculationSeekPos(I)I
    .locals 8
    .param p1    # I

    const-wide/16 v6, 0x64

    int-to-long v1, p1

    iput-wide v1, p0, Lcom/konka/videoPlayer/videoPlayerActivity;->mCurrentTimeSegVideo:J

    const/4 v0, 0x0

    :goto_0
    iget-object v1, p0, Lcom/konka/videoPlayer/videoPlayerActivity;->durationList:Ljava/util/ArrayList;

    invoke-virtual {v1}, Ljava/util/ArrayList;->size()I

    move-result v1

    if-lt v0, v1, :cond_1

    :cond_0
    return v0

    :cond_1
    const-string v2, "weikan"

    new-instance v1, Ljava/lang/StringBuilder;

    const-string v3, "i = "

    invoke-direct {v1, v3}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v3, "mCurrentTimeSegVideo = !!!!"

    invoke-virtual {v1, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget-wide v3, p0, Lcom/konka/videoPlayer/videoPlayerActivity;->mCurrentTimeSegVideo:J

    invoke-virtual {v1, v3, v4}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v3, "Long.parseLong(durationList.get(i)) = "

    invoke-virtual {v1, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    iget-object v1, p0, Lcom/konka/videoPlayer/videoPlayerActivity;->durationList:Ljava/util/ArrayList;

    invoke-virtual {v1, v0}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Ljava/lang/String;

    invoke-static {v1}, Ljava/lang/Long;->parseLong(Ljava/lang/String;)J

    move-result-wide v4

    invoke-virtual {v3, v4, v5}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v2, v1}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    iget-wide v2, p0, Lcom/konka/videoPlayer/videoPlayerActivity;->mCurrentTimeSegVideo:J

    iget-object v1, p0, Lcom/konka/videoPlayer/videoPlayerActivity;->durationList:Ljava/util/ArrayList;

    invoke-virtual {v1, v0}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Ljava/lang/String;

    invoke-static {v1}, Ljava/lang/Long;->parseLong(Ljava/lang/String;)J

    move-result-wide v4

    div-long/2addr v4, v6

    cmp-long v1, v2, v4

    if-lez v1, :cond_0

    iget-wide v2, p0, Lcom/konka/videoPlayer/videoPlayerActivity;->mCurrentTimeSegVideo:J

    iget-object v1, p0, Lcom/konka/videoPlayer/videoPlayerActivity;->durationList:Ljava/util/ArrayList;

    invoke-virtual {v1, v0}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Ljava/lang/String;

    invoke-static {v1}, Ljava/lang/Long;->parseLong(Ljava/lang/String;)J

    move-result-wide v4

    div-long/2addr v4, v6

    sub-long v1, v2, v4

    iput-wide v1, p0, Lcom/konka/videoPlayer/videoPlayerActivity;->mCurrentTimeSegVideo:J

    add-int/lit8 v0, v0, 0x1

    goto :goto_0
.end method

.method private getCurrentPos()I
    .locals 6

    const-wide/16 v0, 0x0

    iget v3, p0, Lcom/konka/videoPlayer/videoPlayerActivity;->mCurrentPosOfUrlList:I

    if-nez v3, :cond_0

    const-string v3, "weikan"

    new-instance v4, Ljava/lang/StringBuilder;

    const-string v5, "getCurrentPosition:"

    invoke-direct {v4, v5}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    iget-object v5, p0, Lcom/konka/videoPlayer/videoPlayerActivity;->mVideoView:Lcom/konka/videoPlayer/VideoView;

    invoke-virtual {v5}, Lcom/konka/videoPlayer/VideoView;->getCurrentPosition()I

    move-result v5

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-static {v3, v4}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    iget-object v3, p0, Lcom/konka/videoPlayer/videoPlayerActivity;->mVideoView:Lcom/konka/videoPlayer/VideoView;

    invoke-virtual {v3}, Lcom/konka/videoPlayer/VideoView;->getCurrentPosition()I

    move-result v3

    div-int/lit8 v3, v3, 0x64

    :goto_0
    return v3

    :cond_0
    const/4 v2, 0x0

    :goto_1
    iget v3, p0, Lcom/konka/videoPlayer/videoPlayerActivity;->mCurrentPosOfUrlList:I

    if-lt v2, v3, :cond_1

    iget-object v3, p0, Lcom/konka/videoPlayer/videoPlayerActivity;->mVideoView:Lcom/konka/videoPlayer/VideoView;

    invoke-virtual {v3}, Lcom/konka/videoPlayer/VideoView;->getCurrentPosition()I

    move-result v3

    int-to-long v3, v3

    add-long/2addr v0, v3

    const-wide/16 v3, 0x64

    div-long v3, v0, v3

    long-to-int v3, v3

    goto :goto_0

    :cond_1
    iget-object v3, p0, Lcom/konka/videoPlayer/videoPlayerActivity;->durationList:Ljava/util/ArrayList;

    invoke-virtual {v3, v2}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Ljava/lang/String;

    invoke-static {v3}, Ljava/lang/Long;->parseLong(Ljava/lang/String;)J

    move-result-wide v3

    add-long/2addr v0, v3

    add-int/lit8 v2, v2, 0x1

    goto :goto_1
.end method

.method private getTotalDuration()I
    .locals 4

    iget-wide v0, p0, Lcom/konka/videoPlayer/videoPlayerActivity;->mTotalTimeSegVideo:J

    const-wide/16 v2, 0x64

    div-long/2addr v0, v2

    long-to-int v0, v0

    return v0
.end method

.method private initVideoPlayer()V
    .locals 2

    const v0, 0x7f0b001c    # com.konka.mediaSharePlayer.R.id.video_view

    invoke-virtual {p0, v0}, Lcom/konka/videoPlayer/videoPlayerActivity;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/konka/videoPlayer/VideoView;

    iput-object v0, p0, Lcom/konka/videoPlayer/videoPlayerActivity;->mVideoView:Lcom/konka/videoPlayer/VideoView;

    iget-object v0, p0, Lcom/konka/videoPlayer/videoPlayerActivity;->mVideoView:Lcom/konka/videoPlayer/VideoView;

    const/high16 v1, -0x1000000

    invoke-virtual {v0, v1}, Lcom/konka/videoPlayer/VideoView;->setBackgroundColor(I)V

    new-instance v0, Lcom/konka/videoPlayer/VideoPlayerSubTitle;

    invoke-direct {v0, p0}, Lcom/konka/videoPlayer/VideoPlayerSubTitle;-><init>(Lcom/konka/videoPlayer/videoPlayerActivity;)V

    iput-object v0, p0, Lcom/konka/videoPlayer/videoPlayerActivity;->classVideoSubtitle:Lcom/konka/videoPlayer/VideoPlayerSubTitle;

    const-string v0, "-----------------------"

    const-string v1, "---------------------------------3"

    invoke-static {v0, v1}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    iget-object v0, p0, Lcom/konka/videoPlayer/videoPlayerActivity;->classVideoSubtitle:Lcom/konka/videoPlayer/VideoPlayerSubTitle;

    invoke-virtual {v0}, Lcom/konka/videoPlayer/VideoPlayerSubTitle;->findBtnView()V

    invoke-virtual {p0}, Lcom/konka/videoPlayer/videoPlayerActivity;->findBtnView()V

    iget-object v0, p0, Lcom/konka/videoPlayer/videoPlayerActivity;->mVideoView:Lcom/konka/videoPlayer/VideoView;

    new-instance v1, Lcom/konka/videoPlayer/videoPlayerActivity$5;

    invoke-direct {v1, p0}, Lcom/konka/videoPlayer/videoPlayerActivity$5;-><init>(Lcom/konka/videoPlayer/videoPlayerActivity;)V

    invoke-virtual {v0, v1}, Lcom/konka/videoPlayer/VideoView;->addOnLayoutChangeListener(Landroid/view/View$OnLayoutChangeListener;)V

    iget-object v0, p0, Lcom/konka/videoPlayer/videoPlayerActivity;->mVideoView:Lcom/konka/videoPlayer/VideoView;

    new-instance v1, Lcom/konka/videoPlayer/videoPlayerActivity$6;

    invoke-direct {v1, p0}, Lcom/konka/videoPlayer/videoPlayerActivity$6;-><init>(Lcom/konka/videoPlayer/videoPlayerActivity;)V

    invoke-virtual {v0, v1}, Lcom/konka/videoPlayer/VideoView;->addOnAttachStateChangeListener(Landroid/view/View$OnAttachStateChangeListener;)V

    iget-object v0, p0, Lcom/konka/videoPlayer/videoPlayerActivity;->mVideoView:Lcom/konka/videoPlayer/VideoView;

    new-instance v1, Lcom/konka/videoPlayer/videoPlayerActivity$7;

    invoke-direct {v1, p0}, Lcom/konka/videoPlayer/videoPlayerActivity$7;-><init>(Lcom/konka/videoPlayer/videoPlayerActivity;)V

    invoke-virtual {v0, v1}, Lcom/konka/videoPlayer/VideoView;->setOnErrorListener(Landroid/media/MediaPlayer$OnErrorListener;)V

    iget-object v0, p0, Lcom/konka/videoPlayer/videoPlayerActivity;->mVideoView:Lcom/konka/videoPlayer/VideoView;

    new-instance v1, Lcom/konka/videoPlayer/videoPlayerActivity$8;

    invoke-direct {v1, p0}, Lcom/konka/videoPlayer/videoPlayerActivity$8;-><init>(Lcom/konka/videoPlayer/videoPlayerActivity;)V

    invoke-virtual {v0, v1}, Lcom/konka/videoPlayer/VideoView;->setOnPreparedListener(Landroid/media/MediaPlayer$OnPreparedListener;)V

    iget-object v0, p0, Lcom/konka/videoPlayer/videoPlayerActivity;->mVideoView:Lcom/konka/videoPlayer/VideoView;

    new-instance v1, Lcom/konka/videoPlayer/videoPlayerActivity$9;

    invoke-direct {v1, p0}, Lcom/konka/videoPlayer/videoPlayerActivity$9;-><init>(Lcom/konka/videoPlayer/videoPlayerActivity;)V

    invoke-virtual {v0, v1}, Lcom/konka/videoPlayer/VideoView;->setOnDragListener(Landroid/view/View$OnDragListener;)V

    iget-object v0, p0, Lcom/konka/videoPlayer/videoPlayerActivity;->mVideoView:Lcom/konka/videoPlayer/VideoView;

    new-instance v1, Lcom/konka/videoPlayer/videoPlayerActivity$10;

    invoke-direct {v1, p0}, Lcom/konka/videoPlayer/videoPlayerActivity$10;-><init>(Lcom/konka/videoPlayer/videoPlayerActivity;)V

    invoke-virtual {v0, v1}, Lcom/konka/videoPlayer/VideoView;->setOnCompletionListener(Landroid/media/MediaPlayer$OnCompletionListener;)V

    iget-object v0, p0, Lcom/konka/videoPlayer/videoPlayerActivity;->mVideoView:Lcom/konka/videoPlayer/VideoView;

    new-instance v1, Lcom/konka/videoPlayer/videoPlayerActivity$11;

    invoke-direct {v1, p0}, Lcom/konka/videoPlayer/videoPlayerActivity$11;-><init>(Lcom/konka/videoPlayer/videoPlayerActivity;)V

    invoke-virtual {v0, v1}, Lcom/konka/videoPlayer/VideoView;->setOnTouchListener(Landroid/view/View$OnTouchListener;)V

    return-void
.end method

.method private playMediaSource()V
    .locals 6

    const v5, 0x7f0900a2    # com.konka.mediaSharePlayer.R.string.mediafile_buffering

    const/16 v4, 0x400

    const/4 v3, 0x0

    iget-object v0, p0, Lcom/konka/videoPlayer/videoPlayerActivity;->mVideoView:Lcom/konka/videoPlayer/VideoView;

    invoke-virtual {v0}, Lcom/konka/videoPlayer/VideoView;->stopPlayback()V

    iget-boolean v0, p0, Lcom/konka/videoPlayer/videoPlayerActivity;->mHideController:Z

    if-nez v0, :cond_1

    const-string v0, "weikan"

    const-string v1, "showController!!"

    invoke-static {v0, v1}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    invoke-direct {p0}, Lcom/konka/videoPlayer/videoPlayerActivity;->showController()V

    :goto_0
    const-string v0, "weikan"

    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "set title "

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    iget-object v2, p0, Lcom/konka/videoPlayer/videoPlayerActivity;->mVideoName:Ljava/lang/String;

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    iget v0, p0, Lcom/konka/videoPlayer/videoPlayerActivity;->comfrom:I

    if-nez v0, :cond_2

    iget-object v0, p0, Lcom/konka/videoPlayer/videoPlayerActivity;->classVideoSubtitle:Lcom/konka/videoPlayer/VideoPlayerSubTitle;

    iget-object v1, p0, Lcom/konka/videoPlayer/videoPlayerActivity;->sourcePath:Ljava/lang/String;

    invoke-virtual {v0, v1}, Lcom/konka/videoPlayer/VideoPlayerSubTitle;->setTextViewName(Ljava/lang/String;)V

    :goto_1
    invoke-direct {p0, v5, v5}, Lcom/konka/videoPlayer/videoPlayerActivity;->showProgressDialog(II)V

    invoke-virtual {p0}, Lcom/konka/videoPlayer/videoPlayerActivity;->getApplicationContext()Landroid/content/Context;

    move-result-object v0

    invoke-virtual {p0, v0}, Lcom/konka/videoPlayer/videoPlayerActivity;->is1280x720(Landroid/content/Context;)Z

    move-result v0

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/konka/videoPlayer/videoPlayerActivity;->mVideoView:Lcom/konka/videoPlayer/VideoView;

    const/16 v1, 0x556

    const/16 v2, 0x2d0

    invoke-virtual {v0, v3, v3, v1, v2}, Lcom/konka/videoPlayer/VideoView;->setVideoScaleFrameLayout(IIII)V

    :cond_0
    invoke-virtual {p0}, Lcom/konka/videoPlayer/videoPlayerActivity;->getWindow()Landroid/view/Window;

    move-result-object v0

    invoke-virtual {v0, v4}, Landroid/view/Window;->addFlags(I)V

    invoke-virtual {p0}, Lcom/konka/videoPlayer/videoPlayerActivity;->getWindow()Landroid/view/Window;

    move-result-object v0

    invoke-virtual {v0, v4}, Landroid/view/Window;->clearFlags(I)V

    iget-object v0, p0, Lcom/konka/videoPlayer/videoPlayerActivity;->sourcePath:Ljava/lang/String;

    invoke-static {v0}, Landroid/net/Uri;->parse(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v0

    iput-object v0, p0, Lcom/konka/videoPlayer/videoPlayerActivity;->mUri:Landroid/net/Uri;

    iget-object v0, p0, Lcom/konka/videoPlayer/videoPlayerActivity;->mVideoView:Lcom/konka/videoPlayer/VideoView;

    iget-object v1, p0, Lcom/konka/videoPlayer/videoPlayerActivity;->mUri:Landroid/net/Uri;

    invoke-virtual {v0, v1}, Lcom/konka/videoPlayer/VideoView;->setVideoURI(Landroid/net/Uri;)V

    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/konka/videoPlayer/videoPlayerActivity;->isResume:Z

    return-void

    :cond_1
    invoke-virtual {p0}, Lcom/konka/videoPlayer/videoPlayerActivity;->hideController()V

    goto :goto_0

    :cond_2
    iget-object v0, p0, Lcom/konka/videoPlayer/videoPlayerActivity;->classVideoSubtitle:Lcom/konka/videoPlayer/VideoPlayerSubTitle;

    iget-object v1, p0, Lcom/konka/videoPlayer/videoPlayerActivity;->mVideoName:Ljava/lang/String;

    invoke-virtual {v0, v1}, Lcom/konka/videoPlayer/VideoPlayerSubTitle;->setTextViewGivenName(Ljava/lang/String;)V

    invoke-virtual {p0}, Lcom/konka/videoPlayer/videoPlayerActivity;->setTextUrl()V

    goto :goto_1
.end method

.method private playSegmentMediaSource()V
    .locals 6

    const v5, 0x7f0900a2    # com.konka.mediaSharePlayer.R.string.mediafile_buffering

    const/16 v4, 0x400

    iget v0, p0, Lcom/konka/videoPlayer/videoPlayerActivity;->mCurrentPosOfUrlList:I

    if-nez v0, :cond_0

    const-string v0, "weikan"

    const-string v1, "stop play back!!"

    invoke-static {v0, v1}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    iget-object v0, p0, Lcom/konka/videoPlayer/videoPlayerActivity;->mVideoView:Lcom/konka/videoPlayer/VideoView;

    invoke-virtual {v0}, Lcom/konka/videoPlayer/VideoView;->stopPlayback()V

    iget-boolean v0, p0, Lcom/konka/videoPlayer/videoPlayerActivity;->mHideController:Z

    if-nez v0, :cond_1

    invoke-direct {p0}, Lcom/konka/videoPlayer/videoPlayerActivity;->showController()V

    :cond_0
    :goto_0
    const-string v1, "weikan"

    new-instance v2, Ljava/lang/StringBuilder;

    const-string v0, "now play weikan segment url = "

    invoke-direct {v2, v0}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    iget-object v0, p0, Lcom/konka/videoPlayer/videoPlayerActivity;->urlList:Ljava/util/ArrayList;

    iget v3, p0, Lcom/konka/videoPlayer/videoPlayerActivity;->mCurrentPosOfUrlList:I

    invoke-virtual {v0, v3}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/String;

    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-static {v1, v0}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    iget-object v0, p0, Lcom/konka/videoPlayer/videoPlayerActivity;->classVideoSubtitle:Lcom/konka/videoPlayer/VideoPlayerSubTitle;

    iget-object v1, p0, Lcom/konka/videoPlayer/videoPlayerActivity;->mVideoName:Ljava/lang/String;

    invoke-virtual {v0, v1}, Lcom/konka/videoPlayer/VideoPlayerSubTitle;->setTextViewGivenName(Ljava/lang/String;)V

    invoke-virtual {p0}, Lcom/konka/videoPlayer/videoPlayerActivity;->setTextUrl()V

    invoke-direct {p0, v5, v5}, Lcom/konka/videoPlayer/videoPlayerActivity;->showProgressDialog(II)V

    invoke-virtual {p0}, Lcom/konka/videoPlayer/videoPlayerActivity;->getWindow()Landroid/view/Window;

    move-result-object v0

    invoke-virtual {v0, v4}, Landroid/view/Window;->addFlags(I)V

    invoke-virtual {p0}, Lcom/konka/videoPlayer/videoPlayerActivity;->getWindow()Landroid/view/Window;

    move-result-object v0

    invoke-virtual {v0, v4}, Landroid/view/Window;->clearFlags(I)V

    iget-object v0, p0, Lcom/konka/videoPlayer/videoPlayerActivity;->urlList:Ljava/util/ArrayList;

    iget v1, p0, Lcom/konka/videoPlayer/videoPlayerActivity;->mCurrentPosOfUrlList:I

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/String;

    invoke-static {v0}, Landroid/net/Uri;->parse(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v0

    iput-object v0, p0, Lcom/konka/videoPlayer/videoPlayerActivity;->mUri:Landroid/net/Uri;

    iget-object v0, p0, Lcom/konka/videoPlayer/videoPlayerActivity;->mVideoView:Lcom/konka/videoPlayer/VideoView;

    iget-object v1, p0, Lcom/konka/videoPlayer/videoPlayerActivity;->mUri:Landroid/net/Uri;

    iget v2, p0, Lcom/konka/videoPlayer/videoPlayerActivity;->mCurrentPosOfUrlList:I

    invoke-virtual {v0, v1, v2}, Lcom/konka/videoPlayer/VideoView;->setSegVideoURI(Landroid/net/Uri;I)V

    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/konka/videoPlayer/videoPlayerActivity;->isResume:Z

    return-void

    :cond_1
    invoke-virtual {p0}, Lcom/konka/videoPlayer/videoPlayerActivity;->hideController()V

    goto :goto_0
.end method

.method private registerDataReceiver()V
    .locals 2

    new-instance v1, Lcom/konka/videoPlayer/videoPlayerActivity$playerDataReceiver;

    invoke-direct {v1, p0}, Lcom/konka/videoPlayer/videoPlayerActivity$playerDataReceiver;-><init>(Lcom/konka/videoPlayer/videoPlayerActivity;)V

    iput-object v1, p0, Lcom/konka/videoPlayer/videoPlayerActivity;->dataReceiver:Lcom/konka/videoPlayer/videoPlayerActivity$playerDataReceiver;

    new-instance v0, Landroid/content/IntentFilter;

    invoke-direct {v0}, Landroid/content/IntentFilter;-><init>()V

    const-string v1, "com.konka.MultiScreenPlayer.videoPlayer"

    invoke-virtual {v0, v1}, Landroid/content/IntentFilter;->addAction(Ljava/lang/String;)V

    iget-object v1, p0, Lcom/konka/videoPlayer/videoPlayerActivity;->dataReceiver:Lcom/konka/videoPlayer/videoPlayerActivity$playerDataReceiver;

    invoke-virtual {p0, v1, v0}, Lcom/konka/videoPlayer/videoPlayerActivity;->registerReceiver(Landroid/content/BroadcastReceiver;Landroid/content/IntentFilter;)Landroid/content/Intent;

    return-void
.end method

.method private setDialogText(Landroid/view/View;)V
    .locals 5
    .param p1    # Landroid/view/View;

    instance-of v4, p1, Landroid/view/ViewGroup;

    if-eqz v4, :cond_2

    move-object v3, p1

    check-cast v3, Landroid/view/ViewGroup;

    const/high16 v4, -0x1000000

    invoke-virtual {p1, v4}, Landroid/view/View;->setBackgroundColor(I)V

    invoke-virtual {v3}, Landroid/view/ViewGroup;->getChildCount()I

    move-result v1

    const/4 v2, 0x0

    :goto_0
    if-lt v2, v1, :cond_1

    :cond_0
    :goto_1
    return-void

    :cond_1
    invoke-virtual {v3, v2}, Landroid/view/ViewGroup;->getChildAt(I)Landroid/view/View;

    move-result-object v0

    invoke-direct {p0, v0}, Lcom/konka/videoPlayer/videoPlayerActivity;->setDialogText(Landroid/view/View;)V

    add-int/lit8 v2, v2, 0x1

    goto :goto_0

    :cond_2
    instance-of v4, p1, Landroid/widget/TextView;

    if-eqz v4, :cond_0

    check-cast p1, Landroid/widget/TextView;

    const/high16 v4, 0x41700000    # 15.0f

    invoke-virtual {p1, v4}, Landroid/widget/TextView;->setTextSize(F)V

    goto :goto_1
.end method

.method private showController()V
    .locals 2

    const/4 v1, 0x0

    iget-object v0, p0, Lcom/konka/videoPlayer/videoPlayerActivity;->classVideoSubtitle:Lcom/konka/videoPlayer/VideoPlayerSubTitle;

    iget-object v0, v0, Lcom/konka/videoPlayer/VideoPlayerSubTitle;->videoSubtitle:Landroid/widget/LinearLayout;

    invoke-virtual {v0, v1}, Landroid/widget/LinearLayout;->setVisibility(I)V

    iget-object v0, p0, Lcom/konka/videoPlayer/videoPlayerActivity;->videoControl:Landroid/widget/LinearLayout;

    invoke-virtual {v0, v1}, Landroid/widget/LinearLayout;->setVisibility(I)V

    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/konka/videoPlayer/videoPlayerActivity;->isControllerShow:Z

    iget-boolean v0, p0, Lcom/konka/videoPlayer/videoPlayerActivity;->isControllerShow:Z

    invoke-virtual {p0, v0}, Lcom/konka/videoPlayer/videoPlayerActivity;->videoViewSetFocusable(Z)V

    return-void
.end method

.method private showProgressDialog(II)V
    .locals 3
    .param p1    # I
    .param p2    # I

    iget-object v1, p0, Lcom/konka/videoPlayer/videoPlayerActivity;->proDlg:Landroid/app/ProgressDialog;

    if-nez v1, :cond_0

    :try_start_0
    new-instance v1, Landroid/app/ProgressDialog;

    invoke-direct {v1, p0}, Landroid/app/ProgressDialog;-><init>(Landroid/content/Context;)V

    iput-object v1, p0, Lcom/konka/videoPlayer/videoPlayerActivity;->proDlg:Landroid/app/ProgressDialog;
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    :cond_0
    :goto_0
    iget-object v1, p0, Lcom/konka/videoPlayer/videoPlayerActivity;->proDlg:Landroid/app/ProgressDialog;

    if-eqz v1, :cond_1

    iget-object v1, p0, Lcom/konka/videoPlayer/videoPlayerActivity;->proDlg:Landroid/app/ProgressDialog;

    invoke-virtual {p0}, Lcom/konka/videoPlayer/videoPlayerActivity;->getResources()Landroid/content/res/Resources;

    move-result-object v2

    invoke-virtual {v2, p2}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Landroid/app/ProgressDialog;->setMessage(Ljava/lang/CharSequence;)V

    iget-object v1, p0, Lcom/konka/videoPlayer/videoPlayerActivity;->proDlg:Landroid/app/ProgressDialog;

    invoke-virtual {v1}, Landroid/app/ProgressDialog;->show()V

    iget-object v1, p0, Lcom/konka/videoPlayer/videoPlayerActivity;->proDlg:Landroid/app/ProgressDialog;

    invoke-virtual {v1}, Landroid/app/ProgressDialog;->getWindow()Landroid/view/Window;

    move-result-object v1

    invoke-virtual {v1}, Landroid/view/Window;->getDecorView()Landroid/view/View;

    move-result-object v0

    invoke-direct {p0, v0}, Lcom/konka/videoPlayer/videoPlayerActivity;->setDialogText(Landroid/view/View;)V

    iget-object v1, p0, Lcom/konka/videoPlayer/videoPlayerActivity;->mVideoView:Lcom/konka/videoPlayer/VideoView;

    iget-object v2, p0, Lcom/konka/videoPlayer/videoPlayerActivity;->proDlg:Landroid/app/ProgressDialog;

    invoke-virtual {v1, v2}, Lcom/konka/videoPlayer/VideoView;->setPG(Landroid/app/ProgressDialog;)V

    :cond_1
    return-void

    :catch_0
    move-exception v1

    goto :goto_0
.end method

.method private startPlayMedia()V
    .locals 14

    const/4 v13, 0x1

    const/4 v12, 0x0

    invoke-virtual {p0}, Lcom/konka/videoPlayer/videoPlayerActivity;->getIntent()Landroid/content/Intent;

    move-result-object v3

    invoke-virtual {v3}, Landroid/content/Intent;->getExtras()Landroid/os/Bundle;

    move-result-object v0

    const-string v7, "weikan"

    const-string v8, "now this is player!!"

    invoke-static {v7, v8}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    const-string v7, "comfrom"

    invoke-virtual {v0, v7, v12}, Landroid/os/Bundle;->getInt(Ljava/lang/String;I)I

    move-result v7

    iput v7, p0, Lcom/konka/videoPlayer/videoPlayerActivity;->comfrom:I

    const-string v7, "videotitle"

    const-string v8, ""

    invoke-virtual {v0, v7, v8}, Landroid/os/Bundle;->getString(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v7

    iput-object v7, p0, Lcom/konka/videoPlayer/videoPlayerActivity;->mVideoName:Ljava/lang/String;

    const-string v7, "weikan"

    new-instance v8, Ljava/lang/StringBuilder;

    const-string v9, "video name is "

    invoke-direct {v8, v9}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    iget-object v9, p0, Lcom/konka/videoPlayer/videoPlayerActivity;->mVideoName:Ljava/lang/String;

    invoke-virtual {v8, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v8

    invoke-virtual {v8}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v8

    invoke-static {v7, v8}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    const-string v7, "isStringArrayList"

    invoke-virtual {v0, v7}, Landroid/os/Bundle;->getBoolean(Ljava/lang/String;)Z

    move-result v7

    invoke-static {v7}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v7

    iput-object v7, p0, Lcom/konka/videoPlayer/videoPlayerActivity;->isSegVideo:Ljava/lang/Boolean;

    const-string v7, "weikan"

    new-instance v8, Ljava/lang/StringBuilder;

    const-string v9, "is seg video!!! = "

    invoke-direct {v8, v9}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    iget-object v9, p0, Lcom/konka/videoPlayer/videoPlayerActivity;->isSegVideo:Ljava/lang/Boolean;

    invoke-virtual {v8, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v8

    invoke-virtual {v8}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v8

    invoke-static {v7, v8}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    iget v7, p0, Lcom/konka/videoPlayer/videoPlayerActivity;->comfrom:I

    if-ne v7, v13, :cond_3

    const-string v7, "hideController"

    invoke-virtual {v0, v7, v12}, Landroid/os/Bundle;->getBoolean(Ljava/lang/String;Z)Z

    move-result v7

    iput-boolean v7, p0, Lcom/konka/videoPlayer/videoPlayerActivity;->mHideController:Z

    iget-object v7, p0, Lcom/konka/videoPlayer/videoPlayerActivity;->isSegVideo:Ljava/lang/Boolean;

    invoke-virtual {v7}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v7

    if-eqz v7, :cond_2

    const-string v7, "urllist"

    invoke-virtual {v0, v7}, Landroid/os/Bundle;->getStringArrayList(Ljava/lang/String;)Ljava/util/ArrayList;

    move-result-object v7

    iput-object v7, p0, Lcom/konka/videoPlayer/videoPlayerActivity;->urlList:Ljava/util/ArrayList;

    iget-object v7, p0, Lcom/konka/videoPlayer/videoPlayerActivity;->urlList:Ljava/util/ArrayList;

    invoke-virtual {v7}, Ljava/util/ArrayList;->size()I

    move-result v7

    iput v7, p0, Lcom/konka/videoPlayer/videoPlayerActivity;->mLengOfUrlList:I

    iput v12, p0, Lcom/konka/videoPlayer/videoPlayerActivity;->mCurrentPosOfUrlList:I

    const-string v7, "durationtime"

    invoke-virtual {v0, v7}, Landroid/os/Bundle;->getStringArrayList(Ljava/lang/String;)Ljava/util/ArrayList;

    move-result-object v7

    iput-object v7, p0, Lcom/konka/videoPlayer/videoPlayerActivity;->durationList:Ljava/util/ArrayList;

    const/4 v2, 0x0

    :goto_0
    iget-object v7, p0, Lcom/konka/videoPlayer/videoPlayerActivity;->urlList:Ljava/util/ArrayList;

    invoke-virtual {v7}, Ljava/util/ArrayList;->size()I

    move-result v7

    if-lt v2, v7, :cond_1

    const-string v7, "weikan"

    new-instance v8, Ljava/lang/StringBuilder;

    const-string v9, "palyer total duration = "

    invoke-direct {v8, v9}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    iget-wide v9, p0, Lcom/konka/videoPlayer/videoPlayerActivity;->mTotalTimeSegVideo:J

    invoke-virtual {v8, v9, v10}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v8

    invoke-virtual {v8}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v8

    invoke-static {v7, v8}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    :goto_1
    const-string v7, "haveNextFlag"

    invoke-virtual {v0, v7, v12}, Landroid/os/Bundle;->getInt(Ljava/lang/String;I)I

    move-result v7

    iput v7, p0, Lcom/konka/videoPlayer/videoPlayerActivity;->haveNextFlag:I

    const-string v7, "mediastop"

    invoke-virtual {v0, v7, v12}, Landroid/os/Bundle;->getInt(Ljava/lang/String;I)I

    move-result v7

    iput v7, p0, Lcom/konka/videoPlayer/videoPlayerActivity;->mediastop:I

    const-string v7, "comefrom"

    invoke-virtual {v0, v7, v12}, Landroid/os/Bundle;->getInt(Ljava/lang/String;I)I

    move-result v7

    iput v7, p0, Lcom/konka/videoPlayer/videoPlayerActivity;->comefrom:I

    const-string v7, "weikan"

    new-instance v8, Ljava/lang/StringBuilder;

    const-string v9, "haveNextFlag ----------2:"

    invoke-direct {v8, v9}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    iget v9, p0, Lcom/konka/videoPlayer/videoPlayerActivity;->haveNextFlag:I

    invoke-virtual {v8, v9}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v8

    invoke-virtual {v8}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v8

    invoke-static {v7, v8}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    const-string v7, "mediastop"

    invoke-virtual {v0, v7, v13}, Landroid/os/Bundle;->getInt(Ljava/lang/String;I)I

    move-result v7

    iput v7, p0, Lcom/konka/videoPlayer/videoPlayerActivity;->mediastop:I

    const-string v7, "resumePos"

    invoke-virtual {v3, v7, v12}, Landroid/content/Intent;->getIntExtra(Ljava/lang/String;I)I

    move-result v7

    iput v7, p0, Lcom/konka/videoPlayer/videoPlayerActivity;->resumePos:I

    iget-object v7, p0, Lcom/konka/videoPlayer/videoPlayerActivity;->isSegVideo:Ljava/lang/Boolean;

    invoke-virtual {v7}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v7

    if-eqz v7, :cond_4

    iget-object v7, p0, Lcom/konka/videoPlayer/videoPlayerActivity;->classVideoSubtitle:Lcom/konka/videoPlayer/VideoPlayerSubTitle;

    iget-object v8, p0, Lcom/konka/videoPlayer/videoPlayerActivity;->mVideoName:Ljava/lang/String;

    invoke-virtual {v7, v8}, Lcom/konka/videoPlayer/VideoPlayerSubTitle;->setTextViewGivenName(Ljava/lang/String;)V

    invoke-virtual {p0}, Lcom/konka/videoPlayer/videoPlayerActivity;->setTextUrl()V

    iput-boolean v12, p0, Lcom/konka/videoPlayer/videoPlayerActivity;->mSeekPoint:Z

    iget-object v7, p0, Lcom/konka/videoPlayer/videoPlayerActivity;->videoHandler:Landroid/os/Handler;

    const/4 v8, 0x5

    invoke-virtual {v7, v8}, Landroid/os/Handler;->sendEmptyMessage(I)Z

    :cond_0
    :goto_2
    return-void

    :cond_1
    const-string v8, "weikan"

    new-instance v9, Ljava/lang/StringBuilder;

    const-string v7, "palyer url = "

    invoke-direct {v9, v7}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    iget-object v7, p0, Lcom/konka/videoPlayer/videoPlayerActivity;->urlList:Ljava/util/ArrayList;

    invoke-virtual {v7, v2}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v7

    check-cast v7, Ljava/lang/String;

    invoke-virtual {v9, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    invoke-virtual {v7}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v7

    invoke-static {v8, v7}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    const-string v8, "weikan"

    new-instance v9, Ljava/lang/StringBuilder;

    const-string v7, "palyer duration = "

    invoke-direct {v9, v7}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    iget-object v7, p0, Lcom/konka/videoPlayer/videoPlayerActivity;->durationList:Ljava/util/ArrayList;

    invoke-virtual {v7, v2}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v7

    check-cast v7, Ljava/lang/String;

    invoke-virtual {v9, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    invoke-virtual {v7}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v7

    invoke-static {v8, v7}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    iget-wide v8, p0, Lcom/konka/videoPlayer/videoPlayerActivity;->mTotalTimeSegVideo:J

    iget-object v7, p0, Lcom/konka/videoPlayer/videoPlayerActivity;->durationList:Ljava/util/ArrayList;

    invoke-virtual {v7, v2}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v7

    check-cast v7, Ljava/lang/String;

    invoke-static {v7}, Ljava/lang/Long;->parseLong(Ljava/lang/String;)J

    move-result-wide v10

    add-long v7, v8, v10

    iput-wide v7, p0, Lcom/konka/videoPlayer/videoPlayerActivity;->mTotalTimeSegVideo:J

    add-int/lit8 v2, v2, 0x1

    goto/16 :goto_0

    :cond_2
    const-string v7, "videofile"

    invoke-virtual {v0, v7}, Landroid/os/Bundle;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v7

    iput-object v7, p0, Lcom/konka/videoPlayer/videoPlayerActivity;->sourcePath:Ljava/lang/String;

    goto/16 :goto_1

    :cond_3
    const-string v7, "videofile"

    invoke-virtual {v0, v7}, Landroid/os/Bundle;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v5

    const-string v7, "8085/"

    invoke-virtual {v5, v7}, Ljava/lang/String;->indexOf(Ljava/lang/String;)I

    move-result v7

    add-int/lit8 v7, v7, 0x5

    invoke-virtual {v5}, Ljava/lang/String;->length()I

    move-result v8

    invoke-virtual {v5, v7, v8}, Ljava/lang/String;->substring(II)Ljava/lang/String;

    move-result-object v6

    const-string v7, "8085/"

    invoke-virtual {v5, v7}, Ljava/lang/String;->indexOf(Ljava/lang/String;)I

    move-result v7

    add-int/lit8 v7, v7, 0x5

    invoke-virtual {v5, v12, v7}, Ljava/lang/String;->substring(II)Ljava/lang/String;

    move-result-object v1

    invoke-static {v6}, Lcom/konka/mediaSharePlayer/encodeUrl;->encodeUrl(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v4

    new-instance v7, Ljava/lang/StringBuilder;

    invoke-static {v1}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v8

    invoke-direct {v7, v8}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v7, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    invoke-virtual {v7}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v7

    iput-object v7, p0, Lcom/konka/videoPlayer/videoPlayerActivity;->sourcePath:Ljava/lang/String;

    goto/16 :goto_1

    :cond_4
    iget-object v7, p0, Lcom/konka/videoPlayer/videoPlayerActivity;->sourcePath:Ljava/lang/String;

    if-eqz v7, :cond_0

    iget-object v7, p0, Lcom/konka/videoPlayer/videoPlayerActivity;->classVideoSubtitle:Lcom/konka/videoPlayer/VideoPlayerSubTitle;

    iget-object v8, p0, Lcom/konka/videoPlayer/videoPlayerActivity;->sourcePath:Ljava/lang/String;

    invoke-virtual {v7, v8}, Lcom/konka/videoPlayer/VideoPlayerSubTitle;->setTextViewName(Ljava/lang/String;)V

    iget-object v7, p0, Lcom/konka/videoPlayer/videoPlayerActivity;->videoHandler:Landroid/os/Handler;

    invoke-virtual {v7, v13}, Landroid/os/Handler;->sendEmptyMessage(I)Z

    goto/16 :goto_2
.end method

.method private videoPlayerStop()V
    .locals 1

    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/konka/videoPlayer/videoPlayerActivity;->isrun:Z

    invoke-virtual {p0}, Lcom/konka/videoPlayer/videoPlayerActivity;->cancelProgressDlg()V

    iget-object v0, p0, Lcom/konka/videoPlayer/videoPlayerActivity;->mVideoView:Lcom/konka/videoPlayer/VideoView;

    invoke-virtual {v0}, Lcom/konka/videoPlayer/VideoView;->stopPlayback()V

    invoke-virtual {p0}, Lcom/konka/videoPlayer/videoPlayerActivity;->finish()V

    return-void
.end method


# virtual methods
.method public cancelProgressDlg()V
    .locals 1

    iget-object v0, p0, Lcom/konka/videoPlayer/videoPlayerActivity;->proDlg:Landroid/app/ProgressDialog;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/konka/videoPlayer/videoPlayerActivity;->proDlg:Landroid/app/ProgressDialog;

    invoke-virtual {v0}, Landroid/app/ProgressDialog;->dismiss()V

    const/4 v0, 0x0

    iput-object v0, p0, Lcom/konka/videoPlayer/videoPlayerActivity;->proDlg:Landroid/app/ProgressDialog;

    :cond_0
    return-void
.end method

.method public findBtnView()V
    .locals 2

    const v0, 0x7f0b0021    # com.konka.mediaSharePlayer.R.id.video_btn_seekback

    invoke-virtual {p0, v0}, Lcom/konka/videoPlayer/videoPlayerActivity;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/Button;

    iput-object v0, p0, Lcom/konka/videoPlayer/videoPlayerActivity;->buttonPre:Landroid/widget/Button;

    const v0, 0x7f0b0022    # com.konka.mediaSharePlayer.R.id.video_btn_play

    invoke-virtual {p0, v0}, Lcom/konka/videoPlayer/videoPlayerActivity;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/Button;

    iput-object v0, p0, Lcom/konka/videoPlayer/videoPlayerActivity;->buttonPlay:Landroid/widget/Button;

    const v0, 0x7f0b0023    # com.konka.mediaSharePlayer.R.id.video_btn_pause

    invoke-virtual {p0, v0}, Lcom/konka/videoPlayer/videoPlayerActivity;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/Button;

    iput-object v0, p0, Lcom/konka/videoPlayer/videoPlayerActivity;->buttonPause:Landroid/widget/Button;

    const v0, 0x7f0b0024    # com.konka.mediaSharePlayer.R.id.video_btn_seekforward

    invoke-virtual {p0, v0}, Lcom/konka/videoPlayer/videoPlayerActivity;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/Button;

    iput-object v0, p0, Lcom/konka/videoPlayer/videoPlayerActivity;->buttonNext:Landroid/widget/Button;

    const v0, 0x7f0b0025    # com.konka.mediaSharePlayer.R.id.video_playbar_time

    invoke-virtual {p0, v0}, Lcom/konka/videoPlayer/videoPlayerActivity;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    iput-object v0, p0, Lcom/konka/videoPlayer/videoPlayerActivity;->curtime:Landroid/widget/TextView;

    const v0, 0x7f0b0027    # com.konka.mediaSharePlayer.R.id.video_playbar_time_all

    invoke-virtual {p0, v0}, Lcom/konka/videoPlayer/videoPlayerActivity;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    iput-object v0, p0, Lcom/konka/videoPlayer/videoPlayerActivity;->durTime:Landroid/widget/TextView;

    const v0, 0x7f0b0026    # com.konka.mediaSharePlayer.R.id.video_seekbar

    invoke-virtual {p0, v0}, Lcom/konka/videoPlayer/videoPlayerActivity;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/SeekBar;

    iput-object v0, p0, Lcom/konka/videoPlayer/videoPlayerActivity;->seekbar:Landroid/widget/SeekBar;

    const v0, 0x7f0b0020    # com.konka.mediaSharePlayer.R.id.video_control

    invoke-virtual {p0, v0}, Lcom/konka/videoPlayer/videoPlayerActivity;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/LinearLayout;

    iput-object v0, p0, Lcom/konka/videoPlayer/videoPlayerActivity;->videoControl:Landroid/widget/LinearLayout;

    iget-object v0, p0, Lcom/konka/videoPlayer/videoPlayerActivity;->buttonPlay:Landroid/widget/Button;

    new-instance v1, Lcom/konka/videoPlayer/videoPlayerActivity$ItemOnClickEvent;

    invoke-direct {v1, p0}, Lcom/konka/videoPlayer/videoPlayerActivity$ItemOnClickEvent;-><init>(Lcom/konka/videoPlayer/videoPlayerActivity;)V

    invoke-virtual {v0, v1}, Landroid/widget/Button;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    iget-object v0, p0, Lcom/konka/videoPlayer/videoPlayerActivity;->buttonPause:Landroid/widget/Button;

    new-instance v1, Lcom/konka/videoPlayer/videoPlayerActivity$ItemOnClickEvent;

    invoke-direct {v1, p0}, Lcom/konka/videoPlayer/videoPlayerActivity$ItemOnClickEvent;-><init>(Lcom/konka/videoPlayer/videoPlayerActivity;)V

    invoke-virtual {v0, v1}, Landroid/widget/Button;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    iget-object v0, p0, Lcom/konka/videoPlayer/videoPlayerActivity;->buttonPre:Landroid/widget/Button;

    new-instance v1, Lcom/konka/videoPlayer/videoPlayerActivity$ItemOnClickEvent;

    invoke-direct {v1, p0}, Lcom/konka/videoPlayer/videoPlayerActivity$ItemOnClickEvent;-><init>(Lcom/konka/videoPlayer/videoPlayerActivity;)V

    invoke-virtual {v0, v1}, Landroid/widget/Button;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    iget-object v0, p0, Lcom/konka/videoPlayer/videoPlayerActivity;->buttonNext:Landroid/widget/Button;

    new-instance v1, Lcom/konka/videoPlayer/videoPlayerActivity$ItemOnClickEvent;

    invoke-direct {v1, p0}, Lcom/konka/videoPlayer/videoPlayerActivity$ItemOnClickEvent;-><init>(Lcom/konka/videoPlayer/videoPlayerActivity;)V

    invoke-virtual {v0, v1}, Landroid/widget/Button;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    new-instance v0, Landroid/app/ProgressDialog;

    invoke-direct {v0, p0}, Landroid/app/ProgressDialog;-><init>(Landroid/content/Context;)V

    iput-object v0, p0, Lcom/konka/videoPlayer/videoPlayerActivity;->proDlg:Landroid/app/ProgressDialog;

    iget-boolean v0, p0, Lcom/konka/videoPlayer/videoPlayerActivity;->isControllerShow:Z

    invoke-virtual {p0, v0}, Lcom/konka/videoPlayer/videoPlayerActivity;->videoViewSetFocusable(Z)V

    iget-object v0, p0, Lcom/konka/videoPlayer/videoPlayerActivity;->seekbar:Landroid/widget/SeekBar;

    invoke-virtual {p0, v0}, Lcom/konka/videoPlayer/videoPlayerActivity;->videoPlayerViewSetFocus(Landroid/view/View;)V

    iget-object v0, p0, Lcom/konka/videoPlayer/videoPlayerActivity;->seekbar:Landroid/widget/SeekBar;

    new-instance v1, Lcom/konka/videoPlayer/videoPlayerActivity$12;

    invoke-direct {v1, p0}, Lcom/konka/videoPlayer/videoPlayerActivity$12;-><init>(Lcom/konka/videoPlayer/videoPlayerActivity;)V

    invoke-virtual {v0, v1}, Landroid/widget/SeekBar;->setOnSeekBarChangeListener(Landroid/widget/SeekBar$OnSeekBarChangeListener;)V

    return-void
.end method

.method public hideController()V
    .locals 2

    const/16 v1, 0x8

    iget-object v0, p0, Lcom/konka/videoPlayer/videoPlayerActivity;->classVideoSubtitle:Lcom/konka/videoPlayer/VideoPlayerSubTitle;

    iget-object v0, v0, Lcom/konka/videoPlayer/VideoPlayerSubTitle;->videoSubtitle:Landroid/widget/LinearLayout;

    invoke-virtual {v0, v1}, Landroid/widget/LinearLayout;->setVisibility(I)V

    iget-object v0, p0, Lcom/konka/videoPlayer/videoPlayerActivity;->videoControl:Landroid/widget/LinearLayout;

    invoke-virtual {v0, v1}, Landroid/widget/LinearLayout;->setVisibility(I)V

    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/konka/videoPlayer/videoPlayerActivity;->isControllerShow:Z

    iget-boolean v0, p0, Lcom/konka/videoPlayer/videoPlayerActivity;->isControllerShow:Z

    invoke-virtual {p0, v0}, Lcom/konka/videoPlayer/videoPlayerActivity;->videoViewSetFocusable(Z)V

    return-void
.end method

.method public initSeekBar()V
    .locals 3

    const/4 v2, 0x0

    iget-object v0, p0, Lcom/konka/videoPlayer/videoPlayerActivity;->seekbar:Landroid/widget/SeekBar;

    invoke-virtual {v0, v2}, Landroid/widget/SeekBar;->setProgress(I)V

    iget-object v0, p0, Lcom/konka/videoPlayer/videoPlayerActivity;->curtime:Landroid/widget/TextView;

    invoke-static {v2}, Lcom/konka/musicPlayer/MusicTool;->calculateDuration(I)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    iget-object v0, p0, Lcom/konka/videoPlayer/videoPlayerActivity;->durTime:Landroid/widget/TextView;

    invoke-static {v2}, Lcom/konka/musicPlayer/MusicTool;->calculateDuration(I)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    return-void
.end method

.method public is1280x720(Landroid/content/Context;)Z
    .locals 4
    .param p1    # Landroid/content/Context;

    const-string v2, "window"

    invoke-virtual {p1, v2}, Landroid/content/Context;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Landroid/view/WindowManager;

    new-instance v0, Landroid/util/DisplayMetrics;

    invoke-direct {v0}, Landroid/util/DisplayMetrics;-><init>()V

    invoke-interface {v1}, Landroid/view/WindowManager;->getDefaultDisplay()Landroid/view/Display;

    move-result-object v2

    invoke-virtual {v2, v0}, Landroid/view/Display;->getMetrics(Landroid/util/DisplayMetrics;)V

    iget v2, v0, Landroid/util/DisplayMetrics;->widthPixels:I

    const/16 v3, 0x500

    if-ne v2, v3, :cond_0

    iget v2, v0, Landroid/util/DisplayMetrics;->heightPixels:I

    const/16 v3, 0x2d0

    if-ne v2, v3, :cond_0

    const/4 v2, 0x1

    :goto_0
    return v2

    :cond_0
    const/4 v2, 0x0

    goto :goto_0
.end method

.method public offController()V
    .locals 2

    invoke-direct {p0}, Lcom/konka/videoPlayer/videoPlayerActivity;->showController()V

    new-instance v0, Ljava/lang/Thread;

    new-instance v1, Lcom/konka/videoPlayer/videoPlayerActivity$14;

    invoke-direct {v1, p0}, Lcom/konka/videoPlayer/videoPlayerActivity$14;-><init>(Lcom/konka/videoPlayer/videoPlayerActivity;)V

    invoke-direct {v0, v1}, Ljava/lang/Thread;-><init>(Ljava/lang/Runnable;)V

    iput-object v0, p0, Lcom/konka/videoPlayer/videoPlayerActivity;->clockThread:Ljava/lang/Thread;

    iget-object v0, p0, Lcom/konka/videoPlayer/videoPlayerActivity;->clockThread:Ljava/lang/Thread;

    invoke-virtual {v0}, Ljava/lang/Thread;->start()V

    return-void
.end method

.method public onCreate(Landroid/os/Bundle;)V
    .locals 5
    .param p1    # Landroid/os/Bundle;

    const/16 v4, 0x400

    const/4 v3, 0x1

    const/4 v2, 0x0

    invoke-super {p0, p1}, Landroid/app/Activity;->onCreate(Landroid/os/Bundle;)V

    const-string v0, "VideoPlayer"

    const-string v1, "video view creat"

    invoke-static {v0, v1}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    invoke-virtual {p0, v3}, Lcom/konka/videoPlayer/videoPlayerActivity;->requestWindowFeature(I)Z

    invoke-static {p0, v2, v2, v3}, Lcom/konka/mediaSharePlayer/sendPlayerState;->MediaPlayerActionSendBroadcastIsCreat(Landroid/content/Context;III)V

    invoke-virtual {p0}, Lcom/konka/videoPlayer/videoPlayerActivity;->getWindow()Landroid/view/Window;

    move-result-object v0

    invoke-virtual {v0, v4, v4}, Landroid/view/Window;->setFlags(II)V

    invoke-virtual {p0, v2}, Lcom/konka/videoPlayer/videoPlayerActivity;->setRequestedOrientation(I)V

    const v0, 0x7f030004    # com.konka.mediaSharePlayer.R.layout.video_1280x720

    invoke-virtual {p0, v0}, Lcom/konka/videoPlayer/videoPlayerActivity;->setContentView(I)V

    new-instance v0, Ljava/lang/Thread;

    new-instance v1, Lcom/konka/videoPlayer/videoPlayerActivity$3;

    invoke-direct {v1, p0}, Lcom/konka/videoPlayer/videoPlayerActivity$3;-><init>(Lcom/konka/videoPlayer/videoPlayerActivity;)V

    invoke-direct {v0, v1}, Ljava/lang/Thread;-><init>(Ljava/lang/Runnable;)V

    invoke-virtual {v0}, Ljava/lang/Thread;->start()V

    iput-boolean v3, p0, Lcom/konka/videoPlayer/videoPlayerActivity;->isrun:Z

    new-instance v0, Ljava/lang/Thread;

    new-instance v1, Lcom/konka/videoPlayer/videoPlayerActivity$4;

    invoke-direct {v1, p0}, Lcom/konka/videoPlayer/videoPlayerActivity$4;-><init>(Lcom/konka/videoPlayer/videoPlayerActivity;)V

    invoke-direct {v0, v1}, Ljava/lang/Thread;-><init>(Ljava/lang/Runnable;)V

    iput-object v0, p0, Lcom/konka/videoPlayer/videoPlayerActivity;->refreshThread:Ljava/lang/Thread;

    iget-object v0, p0, Lcom/konka/videoPlayer/videoPlayerActivity;->refreshThread:Ljava/lang/Thread;

    invoke-virtual {v0}, Ljava/lang/Thread;->start()V

    invoke-direct {p0}, Lcom/konka/videoPlayer/videoPlayerActivity;->initVideoPlayer()V

    invoke-direct {p0}, Lcom/konka/videoPlayer/videoPlayerActivity;->startPlayMedia()V

    return-void
.end method

.method protected onDestroy()V
    .locals 2

    const-string v0, "onDestroy"

    const-string v1, " video onDestroy()"

    invoke-static {v0, v1}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    iget-object v0, p0, Lcom/konka/videoPlayer/videoPlayerActivity;->dataReceiver:Lcom/konka/videoPlayer/videoPlayerActivity$playerDataReceiver;

    invoke-virtual {p0, v0}, Lcom/konka/videoPlayer/videoPlayerActivity;->unregisterReceiver(Landroid/content/BroadcastReceiver;)V

    iget-object v0, p0, Lcom/konka/videoPlayer/videoPlayerActivity;->seekReceiver:Lcom/konka/videoPlayer/videoPlayerActivity$SeekToReceiver;

    invoke-virtual {p0, v0}, Lcom/konka/videoPlayer/videoPlayerActivity;->unregisterReceiver(Landroid/content/BroadcastReceiver;)V

    iget-object v0, p0, Lcom/konka/videoPlayer/videoPlayerActivity;->videoClose:Lcom/konka/videoPlayer/videoPlayerActivity$closeReceiver;

    invoke-virtual {p0, v0}, Lcom/konka/videoPlayer/videoPlayerActivity;->unregisterReceiver(Landroid/content/BroadcastReceiver;)V

    iget-object v0, p0, Lcom/konka/videoPlayer/videoPlayerActivity;->mediaPlayerSwitch:Lcom/konka/videoPlayer/videoPlayerActivity$switchReceiver;

    invoke-virtual {p0, v0}, Lcom/konka/videoPlayer/videoPlayerActivity;->unregisterReceiver(Landroid/content/BroadcastReceiver;)V

    invoke-super {p0}, Landroid/app/Activity;->onDestroy()V

    return-void
.end method

.method public onKeyDown(ILandroid/view/KeyEvent;)Z
    .locals 8
    .param p1    # I
    .param p2    # Landroid/view/KeyEvent;

    const v7, 0x7f02001e    # com.konka.mediaSharePlayer.R.drawable.com_play_sel

    const v6, 0x7f020015    # com.konka.mediaSharePlayer.R.drawable.com_pause_sel

    const/16 v5, 0x8

    const/4 v4, 0x0

    const/4 v3, 0x1

    const-string v0, "VideoPlayer"

    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "getKeyCode : "

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    invoke-virtual {p2}, Landroid/view/KeyEvent;->getKeyCode()I

    move-result v0

    sparse-switch v0, :sswitch_data_0

    :cond_0
    :goto_0
    :sswitch_0
    invoke-super {p0, p1, p2}, Landroid/app/Activity;->onKeyDown(ILandroid/view/KeyEvent;)Z

    move-result v0

    return v0

    :sswitch_1
    iget-boolean v0, p0, Lcom/konka/videoPlayer/videoPlayerActivity;->isControllerShow:Z

    if-eqz v0, :cond_1

    invoke-virtual {p0}, Lcom/konka/videoPlayer/videoPlayerActivity;->hideController()V

    goto :goto_0

    :cond_1
    invoke-direct {p0}, Lcom/konka/videoPlayer/videoPlayerActivity;->showController()V

    new-instance v0, Ljava/lang/Thread;

    new-instance v1, Lcom/konka/videoPlayer/videoPlayerActivity$13;

    invoke-direct {v1, p0}, Lcom/konka/videoPlayer/videoPlayerActivity$13;-><init>(Lcom/konka/videoPlayer/videoPlayerActivity;)V

    invoke-direct {v0, v1}, Ljava/lang/Thread;-><init>(Ljava/lang/Runnable;)V

    iput-object v0, p0, Lcom/konka/videoPlayer/videoPlayerActivity;->clockThread:Ljava/lang/Thread;

    iget-object v0, p0, Lcom/konka/videoPlayer/videoPlayerActivity;->clockThread:Ljava/lang/Thread;

    invoke-virtual {v0}, Ljava/lang/Thread;->start()V

    goto :goto_0

    :sswitch_2
    const-string v0, "VideoPlayer"

    const-string v1, "KeyEvent.KEYCODE_ENTER"

    invoke-static {v0, v1}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    iget-object v0, p0, Lcom/konka/videoPlayer/videoPlayerActivity;->mVideoView:Lcom/konka/videoPlayer/VideoView;

    invoke-virtual {v0}, Lcom/konka/videoPlayer/VideoView;->isPlaying()Z

    move-result v0

    if-eqz v0, :cond_2

    invoke-virtual {p0}, Lcom/konka/videoPlayer/videoPlayerActivity;->setPlayState()V

    invoke-direct {p0}, Lcom/konka/videoPlayer/videoPlayerActivity;->showController()V

    iget-object v0, p0, Lcom/konka/videoPlayer/videoPlayerActivity;->buttonPlay:Landroid/widget/Button;

    invoke-virtual {v0, v4}, Landroid/widget/Button;->setVisibility(I)V

    iget-object v0, p0, Lcom/konka/videoPlayer/videoPlayerActivity;->buttonPause:Landroid/widget/Button;

    invoke-virtual {v0, v5}, Landroid/widget/Button;->setVisibility(I)V

    iget-object v0, p0, Lcom/konka/videoPlayer/videoPlayerActivity;->buttonPlay:Landroid/widget/Button;

    invoke-virtual {v0, v3}, Landroid/widget/Button;->setFocusable(Z)V

    iget-object v0, p0, Lcom/konka/videoPlayer/videoPlayerActivity;->buttonPlay:Landroid/widget/Button;

    invoke-virtual {v0}, Landroid/widget/Button;->requestFocus()Z

    iget-object v0, p0, Lcom/konka/videoPlayer/videoPlayerActivity;->buttonPlay:Landroid/widget/Button;

    invoke-virtual {v0, v7}, Landroid/widget/Button;->setBackgroundResource(I)V

    goto :goto_0

    :cond_2
    invoke-virtual {p0}, Lcom/konka/videoPlayer/videoPlayerActivity;->setPlayState()V

    invoke-virtual {p0}, Lcom/konka/videoPlayer/videoPlayerActivity;->hideController()V

    iget-object v0, p0, Lcom/konka/videoPlayer/videoPlayerActivity;->buttonPause:Landroid/widget/Button;

    invoke-virtual {v0, v4}, Landroid/widget/Button;->setVisibility(I)V

    iget-object v0, p0, Lcom/konka/videoPlayer/videoPlayerActivity;->buttonPlay:Landroid/widget/Button;

    invoke-virtual {v0, v5}, Landroid/widget/Button;->setVisibility(I)V

    iget-object v0, p0, Lcom/konka/videoPlayer/videoPlayerActivity;->buttonPause:Landroid/widget/Button;

    invoke-virtual {v0, v3}, Landroid/widget/Button;->setFocusable(Z)V

    iget-object v0, p0, Lcom/konka/videoPlayer/videoPlayerActivity;->buttonPause:Landroid/widget/Button;

    invoke-virtual {v0}, Landroid/widget/Button;->requestFocus()Z

    iget-object v0, p0, Lcom/konka/videoPlayer/videoPlayerActivity;->buttonPause:Landroid/widget/Button;

    invoke-virtual {v0, v6}, Landroid/widget/Button;->setBackgroundResource(I)V

    goto :goto_0

    :sswitch_3
    iget-object v0, p0, Lcom/konka/videoPlayer/videoPlayerActivity;->mVideoView:Lcom/konka/videoPlayer/VideoView;

    invoke-virtual {v0}, Lcom/konka/videoPlayer/VideoView;->getCurrentPosition()I

    move-result v0

    iput v0, p0, Lcom/konka/videoPlayer/videoPlayerActivity;->currentTime:I

    const-string v0, "VideoPlayer"

    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "KeyEvent.KEYCODE_PROG_BLUE"

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    iget v2, p0, Lcom/konka/videoPlayer/videoPlayerActivity;->currentTime:I

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    iget-boolean v0, p0, Lcom/konka/videoPlayer/videoPlayerActivity;->isControllerShow:Z

    if-nez v0, :cond_3

    invoke-direct {p0}, Lcom/konka/videoPlayer/videoPlayerActivity;->showController()V

    :cond_3
    iget-boolean v0, p0, Lcom/konka/videoPlayer/videoPlayerActivity;->isPause:Z

    if-eqz v0, :cond_4

    iget-object v0, p0, Lcom/konka/videoPlayer/videoPlayerActivity;->mVideoView:Lcom/konka/videoPlayer/VideoView;

    invoke-virtual {v0}, Lcom/konka/videoPlayer/VideoView;->start()V

    iget-object v0, p0, Lcom/konka/videoPlayer/videoPlayerActivity;->buttonPlay:Landroid/widget/Button;

    invoke-virtual {v0, v4}, Landroid/widget/Button;->setVisibility(I)V

    iget-object v0, p0, Lcom/konka/videoPlayer/videoPlayerActivity;->buttonPause:Landroid/widget/Button;

    invoke-virtual {v0, v5}, Landroid/widget/Button;->setVisibility(I)V

    iput-boolean v4, p0, Lcom/konka/videoPlayer/videoPlayerActivity;->isPause:Z

    :cond_4
    iget v0, p0, Lcom/konka/videoPlayer/videoPlayerActivity;->currentTime:I

    add-int/lit16 v0, v0, 0x2710

    invoke-virtual {p0, v0, v3}, Lcom/konka/videoPlayer/videoPlayerActivity;->videoPlayerSeek(II)V

    goto/16 :goto_0

    :sswitch_4
    iget-object v0, p0, Lcom/konka/videoPlayer/videoPlayerActivity;->mVideoView:Lcom/konka/videoPlayer/VideoView;

    invoke-virtual {v0}, Lcom/konka/videoPlayer/VideoView;->getCurrentPosition()I

    move-result v0

    iput v0, p0, Lcom/konka/videoPlayer/videoPlayerActivity;->currentTime:I

    iget-boolean v0, p0, Lcom/konka/videoPlayer/videoPlayerActivity;->isControllerShow:Z

    if-nez v0, :cond_5

    invoke-direct {p0}, Lcom/konka/videoPlayer/videoPlayerActivity;->showController()V

    :cond_5
    const-string v0, "VideoPlayer"

    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "KeyEvent.KEYCODE_PROG_BLUE"

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    iget v2, p0, Lcom/konka/videoPlayer/videoPlayerActivity;->currentTime:I

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    iget-boolean v0, p0, Lcom/konka/videoPlayer/videoPlayerActivity;->isPause:Z

    if-eqz v0, :cond_6

    iget-object v0, p0, Lcom/konka/videoPlayer/videoPlayerActivity;->mVideoView:Lcom/konka/videoPlayer/VideoView;

    invoke-virtual {v0}, Lcom/konka/videoPlayer/VideoView;->start()V

    iget-object v0, p0, Lcom/konka/videoPlayer/videoPlayerActivity;->buttonPlay:Landroid/widget/Button;

    invoke-virtual {v0, v4}, Landroid/widget/Button;->setVisibility(I)V

    iget-object v0, p0, Lcom/konka/videoPlayer/videoPlayerActivity;->buttonPause:Landroid/widget/Button;

    invoke-virtual {v0, v5}, Landroid/widget/Button;->setVisibility(I)V

    iput-boolean v4, p0, Lcom/konka/videoPlayer/videoPlayerActivity;->isPause:Z

    :cond_6
    iget v0, p0, Lcom/konka/videoPlayer/videoPlayerActivity;->currentTime:I

    add-int/lit16 v0, v0, -0x2710

    invoke-virtual {p0, v0, v3}, Lcom/konka/videoPlayer/videoPlayerActivity;->videoPlayerSeek(II)V

    goto/16 :goto_0

    :sswitch_5
    iget-boolean v0, p0, Lcom/konka/videoPlayer/videoPlayerActivity;->isControllerShow:Z

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/konka/videoPlayer/videoPlayerActivity;->seekbar:Landroid/widget/SeekBar;

    invoke-virtual {v0, v3}, Landroid/widget/SeekBar;->setFocusable(Z)V

    iget-object v0, p0, Lcom/konka/videoPlayer/videoPlayerActivity;->seekbar:Landroid/widget/SeekBar;

    invoke-virtual {v0}, Landroid/widget/SeekBar;->requestFocus()Z

    iget-object v0, p0, Lcom/konka/videoPlayer/videoPlayerActivity;->mVideoView:Lcom/konka/videoPlayer/VideoView;

    invoke-virtual {v0}, Lcom/konka/videoPlayer/VideoView;->isPlaying()Z

    move-result v0

    if-eqz v0, :cond_7

    iget-object v0, p0, Lcom/konka/videoPlayer/videoPlayerActivity;->buttonPause:Landroid/widget/Button;

    const v1, 0x7f020016    # com.konka.mediaSharePlayer.R.drawable.com_pause_uns

    invoke-virtual {v0, v1}, Landroid/widget/Button;->setBackgroundResource(I)V

    goto/16 :goto_0

    :cond_7
    iget-object v0, p0, Lcom/konka/videoPlayer/videoPlayerActivity;->buttonPlay:Landroid/widget/Button;

    const v1, 0x7f020021    # com.konka.mediaSharePlayer.R.drawable.com_play_uns

    invoke-virtual {v0, v1}, Landroid/widget/Button;->setBackgroundResource(I)V

    goto/16 :goto_0

    :sswitch_6
    iget-boolean v0, p0, Lcom/konka/videoPlayer/videoPlayerActivity;->isControllerShow:Z

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/konka/videoPlayer/videoPlayerActivity;->mVideoView:Lcom/konka/videoPlayer/VideoView;

    invoke-virtual {v0}, Lcom/konka/videoPlayer/VideoView;->isPlaying()Z

    move-result v0

    if-eqz v0, :cond_8

    iget-object v0, p0, Lcom/konka/videoPlayer/videoPlayerActivity;->buttonPause:Landroid/widget/Button;

    invoke-virtual {v0, v3}, Landroid/widget/Button;->setFocusable(Z)V

    iget-object v0, p0, Lcom/konka/videoPlayer/videoPlayerActivity;->buttonPause:Landroid/widget/Button;

    invoke-virtual {v0}, Landroid/widget/Button;->requestFocus()Z

    iget-object v0, p0, Lcom/konka/videoPlayer/videoPlayerActivity;->buttonPause:Landroid/widget/Button;

    invoke-virtual {v0, v6}, Landroid/widget/Button;->setBackgroundResource(I)V

    goto/16 :goto_0

    :cond_8
    iget-object v0, p0, Lcom/konka/videoPlayer/videoPlayerActivity;->buttonPlay:Landroid/widget/Button;

    invoke-virtual {v0, v3}, Landroid/widget/Button;->setFocusable(Z)V

    iget-object v0, p0, Lcom/konka/videoPlayer/videoPlayerActivity;->buttonPlay:Landroid/widget/Button;

    invoke-virtual {v0}, Landroid/widget/Button;->requestFocus()Z

    iget-object v0, p0, Lcom/konka/videoPlayer/videoPlayerActivity;->buttonPlay:Landroid/widget/Button;

    invoke-virtual {v0, v7}, Landroid/widget/Button;->setBackgroundResource(I)V

    goto/16 :goto_0

    :sswitch_7
    iget-boolean v0, p0, Lcom/konka/videoPlayer/videoPlayerActivity;->isControllerShow:Z

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/konka/videoPlayer/videoPlayerActivity;->seekbar:Landroid/widget/SeekBar;

    invoke-virtual {v0}, Landroid/widget/SeekBar;->isFocused()Z

    move-result v0

    if-eqz v0, :cond_a

    iget-boolean v0, p0, Lcom/konka/videoPlayer/videoPlayerActivity;->isPause:Z

    if-eqz v0, :cond_9

    iget-object v0, p0, Lcom/konka/videoPlayer/videoPlayerActivity;->mVideoView:Lcom/konka/videoPlayer/VideoView;

    invoke-virtual {v0}, Lcom/konka/videoPlayer/VideoView;->start()V

    iget-object v0, p0, Lcom/konka/videoPlayer/videoPlayerActivity;->buttonPlay:Landroid/widget/Button;

    invoke-virtual {v0, v5}, Landroid/widget/Button;->setVisibility(I)V

    iget-object v0, p0, Lcom/konka/videoPlayer/videoPlayerActivity;->buttonPause:Landroid/widget/Button;

    invoke-virtual {v0, v4}, Landroid/widget/Button;->setVisibility(I)V

    iput-boolean v4, p0, Lcom/konka/videoPlayer/videoPlayerActivity;->isPause:Z

    :cond_9
    iget-object v0, p0, Lcom/konka/videoPlayer/videoPlayerActivity;->mVideoView:Lcom/konka/videoPlayer/VideoView;

    invoke-virtual {v0}, Lcom/konka/videoPlayer/VideoView;->getCurrentPosition()I

    move-result v0

    iput v0, p0, Lcom/konka/videoPlayer/videoPlayerActivity;->currentTime:I

    const-string v0, "VideoPlayer"

    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "KeyEvent.KEYCODE_PROG_BLUE"

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    iget v2, p0, Lcom/konka/videoPlayer/videoPlayerActivity;->currentTime:I

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    iget v0, p0, Lcom/konka/videoPlayer/videoPlayerActivity;->currentTime:I

    add-int/lit16 v0, v0, -0x2710

    invoke-virtual {p0, v0, v3}, Lcom/konka/videoPlayer/videoPlayerActivity;->videoPlayerSeek(II)V

    goto/16 :goto_0

    :cond_a
    invoke-virtual {p0}, Lcom/konka/videoPlayer/videoPlayerActivity;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    const v1, 0x7f0900a5    # com.konka.mediaSharePlayer.R.string.forbidoption

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v0

    invoke-static {p0, v0, v3}, Landroid/widget/Toast;->makeText(Landroid/content/Context;Ljava/lang/CharSequence;I)Landroid/widget/Toast;

    move-result-object v0

    invoke-virtual {v0}, Landroid/widget/Toast;->show()V

    goto/16 :goto_0

    :sswitch_8
    iget-boolean v0, p0, Lcom/konka/videoPlayer/videoPlayerActivity;->isControllerShow:Z

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/konka/videoPlayer/videoPlayerActivity;->seekbar:Landroid/widget/SeekBar;

    invoke-virtual {v0}, Landroid/widget/SeekBar;->isFocused()Z

    move-result v0

    if-eqz v0, :cond_c

    iget-boolean v0, p0, Lcom/konka/videoPlayer/videoPlayerActivity;->isPause:Z

    if-eqz v0, :cond_b

    iget-object v0, p0, Lcom/konka/videoPlayer/videoPlayerActivity;->mVideoView:Lcom/konka/videoPlayer/VideoView;

    invoke-virtual {v0}, Lcom/konka/videoPlayer/VideoView;->start()V

    iget-object v0, p0, Lcom/konka/videoPlayer/videoPlayerActivity;->buttonPlay:Landroid/widget/Button;

    invoke-virtual {v0, v5}, Landroid/widget/Button;->setVisibility(I)V

    iget-object v0, p0, Lcom/konka/videoPlayer/videoPlayerActivity;->buttonPause:Landroid/widget/Button;

    invoke-virtual {v0, v4}, Landroid/widget/Button;->setVisibility(I)V

    iput-boolean v4, p0, Lcom/konka/videoPlayer/videoPlayerActivity;->isPause:Z

    :cond_b
    iget-object v0, p0, Lcom/konka/videoPlayer/videoPlayerActivity;->mVideoView:Lcom/konka/videoPlayer/VideoView;

    invoke-virtual {v0}, Lcom/konka/videoPlayer/VideoView;->getCurrentPosition()I

    move-result v0

    iput v0, p0, Lcom/konka/videoPlayer/videoPlayerActivity;->currentTime:I

    const-string v0, "VideoPlayer"

    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "KeyEvent.KEYCODE_PROG_BLUE"

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    iget v2, p0, Lcom/konka/videoPlayer/videoPlayerActivity;->currentTime:I

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    iget v0, p0, Lcom/konka/videoPlayer/videoPlayerActivity;->currentTime:I

    add-int/lit16 v0, v0, 0x2710

    invoke-virtual {p0, v0, v3}, Lcom/konka/videoPlayer/videoPlayerActivity;->videoPlayerSeek(II)V

    goto/16 :goto_0

    :cond_c
    invoke-virtual {p0}, Lcom/konka/videoPlayer/videoPlayerActivity;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    const v1, 0x7f0900a5    # com.konka.mediaSharePlayer.R.string.forbidoption

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v0

    invoke-static {p0, v0, v3}, Landroid/widget/Toast;->makeText(Landroid/content/Context;Ljava/lang/CharSequence;I)Landroid/widget/Toast;

    move-result-object v0

    invoke-virtual {v0}, Landroid/widget/Toast;->show()V

    goto/16 :goto_0

    :sswitch_data_0
    .sparse-switch
        0x4 -> :sswitch_0
        0x13 -> :sswitch_6
        0x14 -> :sswitch_5
        0x15 -> :sswitch_7
        0x16 -> :sswitch_8
        0x42 -> :sswitch_2
        0x52 -> :sswitch_1
        0x6f -> :sswitch_0
        0x8a -> :sswitch_2
        0xb9 -> :sswitch_3
        0xba -> :sswitch_4
    .end sparse-switch
.end method

.method public onStart()V
    .locals 2

    const-string v0, "VideoPlayer"

    const-string v1, "video view start"

    invoke-static {v0, v1}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    invoke-super {p0}, Landroid/app/Activity;->onStart()V

    return-void
.end method

.method protected onStop()V
    .locals 6

    const/4 v4, 0x1

    const/4 v3, 0x0

    iget-object v0, p0, Lcom/konka/videoPlayer/videoPlayerActivity;->mediaPlayerTag:Ljava/lang/String;

    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "onStop()---mediastop:"

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    iget v2, p0, Lcom/konka/videoPlayer/videoPlayerActivity;->mediastop:I

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    iget v0, p0, Lcom/konka/videoPlayer/videoPlayerActivity;->mediastop:I

    if-ne v0, v4, :cond_0

    iget v0, p0, Lcom/konka/videoPlayer/videoPlayerActivity;->mediaSwitch:I

    if-ne v0, v4, :cond_0

    invoke-static {p0, v3, v3, v3}, Lcom/konka/mediaSharePlayer/sendPlayerState;->MediaPlayerActionSendBroadcastIsCreat(Landroid/content/Context;III)V

    sget-object v0, Lcom/konka/mediaSharePlayer/PlayerState;->PLAY_STATUS_EXIT:Lcom/konka/mediaSharePlayer/PlayerState;

    invoke-virtual {v0}, Lcom/konka/mediaSharePlayer/PlayerState;->ordinal()I

    move-result v2

    iget-object v0, p0, Lcom/konka/videoPlayer/videoPlayerActivity;->mVideoView:Lcom/konka/videoPlayer/VideoView;

    invoke-virtual {v0}, Lcom/konka/videoPlayer/VideoView;->getCurrentPosition()I

    move-result v3

    iget-object v0, p0, Lcom/konka/videoPlayer/videoPlayerActivity;->mVideoView:Lcom/konka/videoPlayer/VideoView;

    invoke-virtual {v0}, Lcom/konka/videoPlayer/VideoView;->getDuration()I

    move-result v4

    new-instance v0, Ljava/lang/StringBuilder;

    iget-object v1, p0, Lcom/konka/videoPlayer/videoPlayerActivity;->sourcePath:Ljava/lang/String;

    invoke-static {v1}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    const-string v1, "&mv"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    move-object v0, p0

    move-object v1, p0

    invoke-virtual/range {v0 .. v5}, Lcom/konka/videoPlayer/videoPlayerActivity;->sendPlayerStateToPlayerService(Landroid/content/Context;IIILjava/lang/String;)V

    :cond_0
    invoke-direct {p0}, Lcom/konka/videoPlayer/videoPlayerActivity;->videoPlayerStop()V

    invoke-super {p0}, Landroid/app/Activity;->onStop()V

    return-void
.end method

.method public refresh(II)V
    .locals 7
    .param p1    # I
    .param p2    # I

    :try_start_0
    iget-object v0, p0, Lcom/konka/videoPlayer/videoPlayerActivity;->mVideoView:Lcom/konka/videoPlayer/VideoView;

    invoke-virtual {v0}, Lcom/konka/videoPlayer/VideoView;->getCurrentPosition()I

    move-result p1

    iget-object v0, p0, Lcom/konka/videoPlayer/videoPlayerActivity;->mVideoView:Lcom/konka/videoPlayer/VideoView;

    invoke-virtual {v0}, Lcom/konka/videoPlayer/VideoView;->getDuration()I

    move-result p2

    const-string v0, "VideoPlayer"

    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "current: "

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    const-string v0, "VideoPlayer"

    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "duration: "

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v1, p2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    iget-object v0, p0, Lcom/konka/videoPlayer/videoPlayerActivity;->seekbar:Landroid/widget/SeekBar;

    invoke-virtual {v0, p2}, Landroid/widget/SeekBar;->setMax(I)V

    iget-object v0, p0, Lcom/konka/videoPlayer/videoPlayerActivity;->seekbar:Landroid/widget/SeekBar;

    invoke-virtual {v0, p1}, Landroid/widget/SeekBar;->setProgress(I)V

    iget-object v0, p0, Lcom/konka/videoPlayer/videoPlayerActivity;->seekbar:Landroid/widget/SeekBar;

    invoke-virtual {v0}, Landroid/widget/SeekBar;->postInvalidate()V

    iget-object v0, p0, Lcom/konka/videoPlayer/videoPlayerActivity;->curtime:Landroid/widget/TextView;

    invoke-static {p1}, Lcom/konka/musicPlayer/MusicTool;->calculateDuration(I)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    iget-object v0, p0, Lcom/konka/videoPlayer/videoPlayerActivity;->durTime:Landroid/widget/TextView;

    invoke-static {p2}, Lcom/konka/musicPlayer/MusicTool;->calculateDuration(I)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    :goto_0
    iget-boolean v0, p0, Lcom/konka/videoPlayer/videoPlayerActivity;->isplaying:Z

    if-eqz v0, :cond_0

    iget-boolean v0, p0, Lcom/konka/videoPlayer/videoPlayerActivity;->isPause:Z

    if-nez v0, :cond_0

    sget-object v0, Lcom/konka/mediaSharePlayer/PlayerState;->PLAY_STATUS_SYNC:Lcom/konka/mediaSharePlayer/PlayerState;

    invoke-virtual {v0}, Lcom/konka/mediaSharePlayer/PlayerState;->ordinal()I

    move-result v2

    iget-object v0, p0, Lcom/konka/videoPlayer/videoPlayerActivity;->mVideoView:Lcom/konka/videoPlayer/VideoView;

    invoke-virtual {v0}, Lcom/konka/videoPlayer/VideoView;->getCurrentPosition()I

    move-result v3

    iget-object v0, p0, Lcom/konka/videoPlayer/videoPlayerActivity;->mVideoView:Lcom/konka/videoPlayer/VideoView;

    invoke-virtual {v0}, Lcom/konka/videoPlayer/VideoView;->getDuration()I

    move-result v4

    new-instance v0, Ljava/lang/StringBuilder;

    iget-object v1, p0, Lcom/konka/videoPlayer/videoPlayerActivity;->sourcePath:Ljava/lang/String;

    invoke-static {v1}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    const-string v1, "&mv"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    move-object v0, p0

    move-object v1, p0

    invoke-virtual/range {v0 .. v5}, Lcom/konka/videoPlayer/videoPlayerActivity;->sendPlayerStateToPlayerService(Landroid/content/Context;IIILjava/lang/String;)V

    :cond_0
    return-void

    :catch_0
    move-exception v6

    invoke-virtual {p0}, Lcom/konka/videoPlayer/videoPlayerActivity;->initSeekBar()V

    invoke-virtual {v6}, Ljava/lang/Exception;->printStackTrace()V

    goto :goto_0
.end method

.method public refreshSegVideoBar(II)V
    .locals 4
    .param p1    # I
    .param p2    # I

    :try_start_0
    invoke-direct {p0}, Lcom/konka/videoPlayer/videoPlayerActivity;->getCurrentPos()I

    move-result p1

    invoke-direct {p0}, Lcom/konka/videoPlayer/videoPlayerActivity;->getTotalDuration()I

    move-result p2

    const-string v1, "VideoPlayer"

    new-instance v2, Ljava/lang/StringBuilder;

    const-string v3, "current: "

    invoke-direct {v2, v3}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v2, p1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    const-string v1, "VideoPlayer"

    new-instance v2, Ljava/lang/StringBuilder;

    const-string v3, "duration: "

    invoke-direct {v2, v3}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v2, p2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    const-string v1, "VideoPlayer"

    new-instance v2, Ljava/lang/StringBuilder;

    const-string v3, "isPause: "

    invoke-direct {v2, v3}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    iget-boolean v3, p0, Lcom/konka/videoPlayer/videoPlayerActivity;->isPause:Z

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    iget-object v1, p0, Lcom/konka/videoPlayer/videoPlayerActivity;->seekbar:Landroid/widget/SeekBar;

    invoke-virtual {v1, p2}, Landroid/widget/SeekBar;->setMax(I)V

    iget-object v1, p0, Lcom/konka/videoPlayer/videoPlayerActivity;->seekbar:Landroid/widget/SeekBar;

    invoke-virtual {v1, p1}, Landroid/widget/SeekBar;->setProgress(I)V

    iget-object v1, p0, Lcom/konka/videoPlayer/videoPlayerActivity;->seekbar:Landroid/widget/SeekBar;

    invoke-virtual {v1}, Landroid/widget/SeekBar;->postInvalidate()V

    iget-object v1, p0, Lcom/konka/videoPlayer/videoPlayerActivity;->curtime:Landroid/widget/TextView;

    invoke-static {p1}, Lcom/konka/musicPlayer/MusicTool;->calculateSegDuration(I)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    iget-object v1, p0, Lcom/konka/videoPlayer/videoPlayerActivity;->durTime:Landroid/widget/TextView;

    invoke-static {p2}, Lcom/konka/musicPlayer/MusicTool;->calculateSegDuration(I)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    :goto_0
    return-void

    :catch_0
    move-exception v0

    invoke-virtual {p0}, Lcom/konka/videoPlayer/videoPlayerActivity;->initSeekBar()V

    invoke-virtual {v0}, Ljava/lang/Exception;->printStackTrace()V

    goto :goto_0
.end method

.method public registerCloseReceiver()V
    .locals 2

    new-instance v1, Lcom/konka/videoPlayer/videoPlayerActivity$closeReceiver;

    invoke-direct {v1, p0}, Lcom/konka/videoPlayer/videoPlayerActivity$closeReceiver;-><init>(Lcom/konka/videoPlayer/videoPlayerActivity;)V

    iput-object v1, p0, Lcom/konka/videoPlayer/videoPlayerActivity;->videoClose:Lcom/konka/videoPlayer/videoPlayerActivity$closeReceiver;

    new-instance v0, Landroid/content/IntentFilter;

    invoke-direct {v0}, Landroid/content/IntentFilter;-><init>()V

    const-string v1, "com.konka.playerService.MultiScreenPlayerService"

    invoke-virtual {v0, v1}, Landroid/content/IntentFilter;->addAction(Ljava/lang/String;)V

    iget-object v1, p0, Lcom/konka/videoPlayer/videoPlayerActivity;->videoClose:Lcom/konka/videoPlayer/videoPlayerActivity$closeReceiver;

    invoke-virtual {p0, v1, v0}, Lcom/konka/videoPlayer/videoPlayerActivity;->registerReceiver(Landroid/content/BroadcastReceiver;Landroid/content/IntentFilter;)Landroid/content/Intent;

    return-void
.end method

.method public registerSeekReceiver()V
    .locals 3

    const-string v1, "VideoPlayer"

    const-string v2, "registerSeekReceiver--------"

    invoke-static {v1, v2}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    new-instance v1, Lcom/konka/videoPlayer/videoPlayerActivity$SeekToReceiver;

    invoke-direct {v1, p0}, Lcom/konka/videoPlayer/videoPlayerActivity$SeekToReceiver;-><init>(Lcom/konka/videoPlayer/videoPlayerActivity;)V

    iput-object v1, p0, Lcom/konka/videoPlayer/videoPlayerActivity;->seekReceiver:Lcom/konka/videoPlayer/videoPlayerActivity$SeekToReceiver;

    new-instance v0, Landroid/content/IntentFilter;

    invoke-direct {v0}, Landroid/content/IntentFilter;-><init>()V

    const-string v1, "com.konka.MultiScreeenPlayer.playerseek"

    invoke-virtual {v0, v1}, Landroid/content/IntentFilter;->addAction(Ljava/lang/String;)V

    iget-object v1, p0, Lcom/konka/videoPlayer/videoPlayerActivity;->seekReceiver:Lcom/konka/videoPlayer/videoPlayerActivity$SeekToReceiver;

    invoke-virtual {p0, v1, v0}, Lcom/konka/videoPlayer/videoPlayerActivity;->registerReceiver(Landroid/content/BroadcastReceiver;Landroid/content/IntentFilter;)Landroid/content/Intent;

    return-void
.end method

.method public registerSwitchReceiver()V
    .locals 3

    new-instance v1, Lcom/konka/videoPlayer/videoPlayerActivity$switchReceiver;

    const/4 v2, 0x0

    invoke-direct {v1, p0, v2}, Lcom/konka/videoPlayer/videoPlayerActivity$switchReceiver;-><init>(Lcom/konka/videoPlayer/videoPlayerActivity;Lcom/konka/videoPlayer/videoPlayerActivity$switchReceiver;)V

    iput-object v1, p0, Lcom/konka/videoPlayer/videoPlayerActivity;->mediaPlayerSwitch:Lcom/konka/videoPlayer/videoPlayerActivity$switchReceiver;

    new-instance v0, Landroid/content/IntentFilter;

    invoke-direct {v0}, Landroid/content/IntentFilter;-><init>()V

    const-string v1, "com.konka.playerService.MultiScreenPlayerSwtich"

    invoke-virtual {v0, v1}, Landroid/content/IntentFilter;->addAction(Ljava/lang/String;)V

    iget-object v1, p0, Lcom/konka/videoPlayer/videoPlayerActivity;->mediaPlayerSwitch:Lcom/konka/videoPlayer/videoPlayerActivity$switchReceiver;

    invoke-virtual {p0, v1, v0}, Lcom/konka/videoPlayer/videoPlayerActivity;->registerReceiver(Landroid/content/BroadcastReceiver;Landroid/content/IntentFilter;)Landroid/content/Intent;

    return-void
.end method

.method public sendPlayerStateToPlayerService(Landroid/content/Context;IIILjava/lang/String;)V
    .locals 5
    .param p1    # Landroid/content/Context;
    .param p2    # I
    .param p3    # I
    .param p4    # I
    .param p5    # Ljava/lang/String;

    new-instance v1, Landroid/content/Intent;

    invoke-direct {v1}, Landroid/content/Intent;-><init>()V

    const-string v2, "com.konka.MultiScreeenPlayer.PlayerState"

    invoke-virtual {v1, v2}, Landroid/content/Intent;->setAction(Ljava/lang/String;)Landroid/content/Intent;

    new-instance v0, Landroid/os/Bundle;

    invoke-direct {v0}, Landroid/os/Bundle;-><init>()V

    const-string v2, "playerState"

    invoke-virtual {v0, v2, p2}, Landroid/os/Bundle;->putInt(Ljava/lang/String;I)V

    const-string v2, "currentTime"

    invoke-virtual {v0, v2, p3}, Landroid/os/Bundle;->putInt(Ljava/lang/String;I)V

    const-string v2, "DurationTime"

    invoke-virtual {v0, v2, p4}, Landroid/os/Bundle;->putInt(Ljava/lang/String;I)V

    const-string v2, "fileUrl"

    invoke-virtual {v0, v2, p5}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V

    const-string v2, "mobileIp"

    iget-object v3, p0, Lcom/konka/videoPlayer/videoPlayerActivity;->mobileIp:Ljava/lang/String;

    invoke-virtual {v0, v2, v3}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V

    invoke-virtual {v1, v0}, Landroid/content/Intent;->putExtras(Landroid/os/Bundle;)Landroid/content/Intent;

    invoke-virtual {p1, v1}, Landroid/content/Context;->sendBroadcast(Landroid/content/Intent;)V

    const-string v2, "sendBroadcast"

    new-instance v3, Ljava/lang/StringBuilder;

    const-string v4, "sendPlayerStateToPlayerService:"

    invoke-direct {v3, v4}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v3, p2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v3

    const-string v4, ","

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3, p3}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v3

    const-string v4, ","

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3, p4}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v3

    const-string v4, ","

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3, p5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v2, v3}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    return-void
.end method

.method public setPlayState()V
    .locals 6

    const-string v0, "VideoPlayer"

    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "setPlayState():"

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    iget-object v2, p0, Lcom/konka/videoPlayer/videoPlayerActivity;->mVideoView:Lcom/konka/videoPlayer/VideoView;

    invoke-virtual {v2}, Lcom/konka/videoPlayer/VideoView;->isPlaying()Z

    move-result v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    iget-object v0, p0, Lcom/konka/videoPlayer/videoPlayerActivity;->mVideoView:Lcom/konka/videoPlayer/VideoView;

    invoke-virtual {v0}, Lcom/konka/videoPlayer/VideoView;->isPlaying()Z

    move-result v0

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/konka/videoPlayer/videoPlayerActivity;->mVideoView:Lcom/konka/videoPlayer/VideoView;

    invoke-virtual {v0}, Lcom/konka/videoPlayer/VideoView;->pause()V

    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/konka/videoPlayer/videoPlayerActivity;->isPause:Z

    sget-object v0, Lcom/konka/mediaSharePlayer/PlayerState;->PLAY_STATUS_PAUSE:Lcom/konka/mediaSharePlayer/PlayerState;

    invoke-virtual {v0}, Lcom/konka/mediaSharePlayer/PlayerState;->ordinal()I

    move-result v2

    iget-object v0, p0, Lcom/konka/videoPlayer/videoPlayerActivity;->mVideoView:Lcom/konka/videoPlayer/VideoView;

    invoke-virtual {v0}, Lcom/konka/videoPlayer/VideoView;->getCurrentPosition()I

    move-result v3

    iget-object v0, p0, Lcom/konka/videoPlayer/videoPlayerActivity;->mVideoView:Lcom/konka/videoPlayer/VideoView;

    invoke-virtual {v0}, Lcom/konka/videoPlayer/VideoView;->getDuration()I

    move-result v4

    new-instance v0, Ljava/lang/StringBuilder;

    iget-object v1, p0, Lcom/konka/videoPlayer/videoPlayerActivity;->sourcePath:Ljava/lang/String;

    invoke-static {v1}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    const-string v1, "&mv"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    move-object v0, p0

    move-object v1, p0

    invoke-virtual/range {v0 .. v5}, Lcom/konka/videoPlayer/videoPlayerActivity;->sendPlayerStateToPlayerService(Landroid/content/Context;IIILjava/lang/String;)V

    :goto_0
    return-void

    :cond_0
    iget-object v0, p0, Lcom/konka/videoPlayer/videoPlayerActivity;->mVideoView:Lcom/konka/videoPlayer/VideoView;

    invoke-virtual {v0}, Lcom/konka/videoPlayer/VideoView;->start()V

    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/konka/videoPlayer/videoPlayerActivity;->isPause:Z

    sget-object v0, Lcom/konka/mediaSharePlayer/PlayerState;->PLAY_STATUS_PLAY:Lcom/konka/mediaSharePlayer/PlayerState;

    invoke-virtual {v0}, Lcom/konka/mediaSharePlayer/PlayerState;->ordinal()I

    move-result v2

    iget-object v0, p0, Lcom/konka/videoPlayer/videoPlayerActivity;->mVideoView:Lcom/konka/videoPlayer/VideoView;

    invoke-virtual {v0}, Lcom/konka/videoPlayer/VideoView;->getCurrentPosition()I

    move-result v3

    iget-object v0, p0, Lcom/konka/videoPlayer/videoPlayerActivity;->mVideoView:Lcom/konka/videoPlayer/VideoView;

    invoke-virtual {v0}, Lcom/konka/videoPlayer/VideoView;->getDuration()I

    move-result v4

    new-instance v0, Ljava/lang/StringBuilder;

    iget-object v1, p0, Lcom/konka/videoPlayer/videoPlayerActivity;->sourcePath:Ljava/lang/String;

    invoke-static {v1}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    const-string v1, "&mv"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    move-object v0, p0

    move-object v1, p0

    invoke-virtual/range {v0 .. v5}, Lcom/konka/videoPlayer/videoPlayerActivity;->sendPlayerStateToPlayerService(Landroid/content/Context;IIILjava/lang/String;)V

    goto :goto_0
.end method

.method public setTextUrl()V
    .locals 4

    const/16 v3, 0x50

    const-string v0, ""

    iget-object v1, p0, Lcom/konka/videoPlayer/videoPlayerActivity;->isSegVideo:Ljava/lang/Boolean;

    invoke-virtual {v1}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v1

    if-eqz v1, :cond_1

    iget-object v1, p0, Lcom/konka/videoPlayer/videoPlayerActivity;->urlList:Ljava/util/ArrayList;

    iget v2, p0, Lcom/konka/videoPlayer/videoPlayerActivity;->mCurrentPosOfUrlList:I

    invoke-virtual {v1, v2}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/String;

    :goto_0
    invoke-virtual {v0}, Ljava/lang/String;->length()I

    move-result v1

    if-le v1, v3, :cond_0

    new-instance v1, Ljava/lang/StringBuilder;

    const/4 v2, 0x0

    invoke-virtual {v0, v2, v3}, Ljava/lang/String;->substring(II)Ljava/lang/String;

    move-result-object v2

    invoke-static {v2}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v2

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    const-string v2, "..."

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    :cond_0
    iget-object v1, p0, Lcom/konka/videoPlayer/videoPlayerActivity;->classVideoSubtitle:Lcom/konka/videoPlayer/VideoPlayerSubTitle;

    invoke-virtual {v1, v0}, Lcom/konka/videoPlayer/VideoPlayerSubTitle;->setTextViewUrl(Ljava/lang/String;)V

    return-void

    :cond_1
    iget-object v0, p0, Lcom/konka/videoPlayer/videoPlayerActivity;->sourcePath:Ljava/lang/String;

    goto :goto_0
.end method

.method public toastMessage(I)V
    .locals 2
    .param p1    # I

    invoke-virtual {p0}, Lcom/konka/videoPlayer/videoPlayerActivity;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    invoke-virtual {v1, p1}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v0

    const/4 v1, 0x1

    invoke-static {p0, v0, v1}, Landroid/widget/Toast;->makeText(Landroid/content/Context;Ljava/lang/CharSequence;I)Landroid/widget/Toast;

    move-result-object v1

    invoke-virtual {v1}, Landroid/widget/Toast;->show()V

    return-void
.end method

.method public videoPlayerSeek(II)V
    .locals 9
    .param p1    # I
    .param p2    # I

    const/4 v5, -0x1

    const/4 v4, -0x2

    const/16 v8, 0x8

    const/4 v7, 0x1

    const/4 v3, 0x0

    const-string v0, "VideoPlayer"

    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "seek to: "

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, "is playing !!!"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget-boolean v2, p0, Lcom/konka/videoPlayer/videoPlayerActivity;->isplaying:Z

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    const-string v0, "wangjinxin"

    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "PressTimes = "

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v1, p2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    const v0, 0x7f0900a2    # com.konka.mediaSharePlayer.R.string.mediafile_buffering

    const v1, 0x7f0900a2    # com.konka.mediaSharePlayer.R.string.mediafile_buffering

    invoke-direct {p0, v0, v1}, Lcom/konka/videoPlayer/videoPlayerActivity;->showProgressDialog(II)V

    iput-boolean v7, p0, Lcom/konka/videoPlayer/videoPlayerActivity;->isSend:Z

    iput v3, p0, Lcom/konka/videoPlayer/videoPlayerActivity;->relayTimer:I

    iget v0, p0, Lcom/konka/videoPlayer/videoPlayerActivity;->comfrom:I

    if-ne v0, v7, :cond_c

    if-ne p1, v5, :cond_2

    iget-object v0, p0, Lcom/konka/videoPlayer/videoPlayerActivity;->mVideoView:Lcom/konka/videoPlayer/VideoView;

    invoke-virtual {v0}, Lcom/konka/videoPlayer/VideoView;->getCurrentPosition()I

    move-result v0

    div-int/lit8 v0, v0, 0x64

    mul-int/lit16 v1, p2, 0x12c

    add-int p1, v0, v1

    iget-object v0, p0, Lcom/konka/videoPlayer/videoPlayerActivity;->isSegVideo:Ljava/lang/Boolean;

    invoke-virtual {v0}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-direct {p0}, Lcom/konka/videoPlayer/videoPlayerActivity;->getCurrentPos()I

    move-result v6

    sget v0, Lcom/konka/videoPlayer/videoPlayerActivity;->mLongPressTimes:I

    mul-int/lit16 v0, v0, 0x12c

    add-int p1, v6, v0

    const-string v0, "wangjinxin"

    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "mForwardCurrent current = "

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v1, v6}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, "mLongPressTimes*300 = "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    sget v2, Lcom/konka/videoPlayer/videoPlayerActivity;->mLongPressTimes:I

    mul-int/lit16 v2, v2, 0x12c

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, " seekPos = "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    :cond_0
    :goto_0
    iget-object v0, p0, Lcom/konka/videoPlayer/videoPlayerActivity;->isSegVideo:Ljava/lang/Boolean;

    invoke-virtual {v0}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v0

    if-eqz v0, :cond_8

    if-gtz p1, :cond_3

    if-eq p1, v5, :cond_3

    if-eq p1, v4, :cond_3

    iget-object v0, p0, Lcom/konka/videoPlayer/videoPlayerActivity;->mVideoView:Lcom/konka/videoPlayer/VideoView;

    invoke-virtual {v0, v3}, Lcom/konka/videoPlayer/VideoView;->seekTo(I)V

    iget-object v0, p0, Lcom/konka/videoPlayer/videoPlayerActivity;->mediaPlayerTag:Ljava/lang/String;

    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "getCurrentPosition time====00000===="

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    iget-object v2, p0, Lcom/konka/videoPlayer/videoPlayerActivity;->mVideoView:Lcom/konka/videoPlayer/VideoView;

    invoke-virtual {v2}, Lcom/konka/videoPlayer/VideoView;->getCurrentPosition()I

    move-result v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    sget-object v0, Lcom/konka/mediaSharePlayer/PlayerState;->PLAY_STATUS_SEEK:Lcom/konka/mediaSharePlayer/PlayerState;

    invoke-virtual {v0}, Lcom/konka/mediaSharePlayer/PlayerState;->ordinal()I

    move-result v2

    iget-object v0, p0, Lcom/konka/videoPlayer/videoPlayerActivity;->mVideoView:Lcom/konka/videoPlayer/VideoView;

    invoke-virtual {v0}, Lcom/konka/videoPlayer/VideoView;->getDuration()I

    move-result v4

    new-instance v0, Ljava/lang/StringBuilder;

    iget-object v1, p0, Lcom/konka/videoPlayer/videoPlayerActivity;->sourcePath:Ljava/lang/String;

    invoke-static {v1}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    const-string v1, "&mv"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    move-object v0, p0

    move-object v1, p0

    invoke-virtual/range {v0 .. v5}, Lcom/konka/videoPlayer/videoPlayerActivity;->sendPlayerStateToPlayerService(Landroid/content/Context;IIILjava/lang/String;)V

    :cond_1
    :goto_1
    return-void

    :cond_2
    if-ne p1, v4, :cond_0

    iget-object v0, p0, Lcom/konka/videoPlayer/videoPlayerActivity;->mVideoView:Lcom/konka/videoPlayer/VideoView;

    invoke-virtual {v0}, Lcom/konka/videoPlayer/VideoView;->getCurrentPosition()I

    move-result v0

    div-int/lit8 v0, v0, 0x64

    mul-int/lit16 v1, p2, 0x12c

    sub-int p1, v0, v1

    iget-object v0, p0, Lcom/konka/videoPlayer/videoPlayerActivity;->isSegVideo:Ljava/lang/Boolean;

    invoke-virtual {v0}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-direct {p0}, Lcom/konka/videoPlayer/videoPlayerActivity;->getCurrentPos()I

    move-result v6

    sget v0, Lcom/konka/videoPlayer/videoPlayerActivity;->mLongPressTimes:I

    mul-int/lit16 v0, v0, 0x12c

    sub-int p1, v6, v0

    goto :goto_0

    :cond_3
    int-to-long v0, p1

    iget-wide v4, p0, Lcom/konka/videoPlayer/videoPlayerActivity;->mTotalTimeSegVideo:J

    cmp-long v0, v0, v4

    if-ltz v0, :cond_4

    iget-object v0, p0, Lcom/konka/videoPlayer/videoPlayerActivity;->mVideoView:Lcom/konka/videoPlayer/VideoView;

    iget-object v1, p0, Lcom/konka/videoPlayer/videoPlayerActivity;->mVideoView:Lcom/konka/videoPlayer/VideoView;

    invoke-virtual {v1}, Lcom/konka/videoPlayer/VideoView;->getDuration()I

    move-result v1

    invoke-virtual {v0, v1}, Lcom/konka/videoPlayer/VideoView;->seekTo(I)V

    goto :goto_1

    :cond_4
    invoke-direct {p0, p1}, Lcom/konka/videoPlayer/videoPlayerActivity;->calculationSeekPos(I)I

    move-result v0

    iput v0, p0, Lcom/konka/videoPlayer/videoPlayerActivity;->mSeekPosOfUrlList:I

    const-string v0, "weikan"

    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "mSeekPosOfUrlList = "

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    iget v2, p0, Lcom/konka/videoPlayer/videoPlayerActivity;->mSeekPosOfUrlList:I

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, "mCurrentPosOfUrlList = "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget v2, p0, Lcom/konka/videoPlayer/videoPlayerActivity;->mCurrentPosOfUrlList:I

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    const-string v0, "wangjinxin"

    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "mCurrentTimeSegVideo = "

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    iget-wide v4, p0, Lcom/konka/videoPlayer/videoPlayerActivity;->mCurrentTimeSegVideo:J

    invoke-virtual {v1, v4, v5}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    iget v0, p0, Lcom/konka/videoPlayer/videoPlayerActivity;->mSeekPosOfUrlList:I

    iget v1, p0, Lcom/konka/videoPlayer/videoPlayerActivity;->mCurrentPosOfUrlList:I

    if-ne v0, v1, :cond_7

    const-string v0, "wangjinxin"

    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "(int)mCurrentTimeSegVideo *100 = "

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    iget-wide v4, p0, Lcom/konka/videoPlayer/videoPlayerActivity;->mCurrentTimeSegVideo:J

    long-to-int v2, v4

    mul-int/lit8 v2, v2, 0x64

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    iget-object v0, p0, Lcom/konka/videoPlayer/videoPlayerActivity;->mVideoView:Lcom/konka/videoPlayer/VideoView;

    iget-wide v1, p0, Lcom/konka/videoPlayer/videoPlayerActivity;->mCurrentTimeSegVideo:J

    long-to-int v1, v1

    mul-int/lit8 v1, v1, 0x64

    invoke-virtual {v0, v1}, Lcom/konka/videoPlayer/VideoView;->seekTo(I)V

    :cond_5
    :goto_2
    iget-boolean v0, p0, Lcom/konka/videoPlayer/videoPlayerActivity;->isPause:Z

    if-eqz v0, :cond_6

    iget-object v0, p0, Lcom/konka/videoPlayer/videoPlayerActivity;->mVideoView:Lcom/konka/videoPlayer/VideoView;

    invoke-virtual {v0}, Lcom/konka/videoPlayer/VideoView;->start()V

    iput-boolean v3, p0, Lcom/konka/videoPlayer/videoPlayerActivity;->isPause:Z

    iget-object v0, p0, Lcom/konka/videoPlayer/videoPlayerActivity;->buttonPlay:Landroid/widget/Button;

    invoke-virtual {v0, v8}, Landroid/widget/Button;->setVisibility(I)V

    iget-object v0, p0, Lcom/konka/videoPlayer/videoPlayerActivity;->buttonPause:Landroid/widget/Button;

    invoke-virtual {v0, v3}, Landroid/widget/Button;->setVisibility(I)V

    :cond_6
    iget-object v0, p0, Lcom/konka/videoPlayer/videoPlayerActivity;->mVideoView:Lcom/konka/videoPlayer/VideoView;

    invoke-virtual {v0}, Lcom/konka/videoPlayer/VideoView;->isPlaying()Z

    move-result v0

    if-eqz v0, :cond_1

    iget-object v0, p0, Lcom/konka/videoPlayer/videoPlayerActivity;->mediaPlayerTag:Ljava/lang/String;

    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "getCurrentPosition time===1111====="

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    iget-object v2, p0, Lcom/konka/videoPlayer/videoPlayerActivity;->mVideoView:Lcom/konka/videoPlayer/VideoView;

    invoke-virtual {v2}, Lcom/konka/videoPlayer/VideoView;->getCurrentPosition()I

    move-result v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    iput-boolean v7, p0, Lcom/konka/videoPlayer/videoPlayerActivity;->isSeekTo:Z

    goto/16 :goto_1

    :cond_7
    iget v0, p0, Lcom/konka/videoPlayer/videoPlayerActivity;->mSeekPosOfUrlList:I

    iput v0, p0, Lcom/konka/videoPlayer/videoPlayerActivity;->mCurrentPosOfUrlList:I

    iget v0, p0, Lcom/konka/videoPlayer/videoPlayerActivity;->mCurrentPosOfUrlList:I

    iget v1, p0, Lcom/konka/videoPlayer/videoPlayerActivity;->mLengOfUrlList:I

    if-ge v0, v1, :cond_5

    const-string v0, "weikan"

    const-string v1, "VideoView is begin seek one!!!"

    invoke-static {v0, v1}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    iput-boolean v3, p0, Lcom/konka/videoPlayer/videoPlayerActivity;->isplaying:Z

    iput-boolean v7, p0, Lcom/konka/videoPlayer/videoPlayerActivity;->mSeekPoint:Z

    const-string v0, "VideoPlayer"

    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "videoHandler.sendEmptyMessage(5) "

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    iget-boolean v2, p0, Lcom/konka/videoPlayer/videoPlayerActivity;->isplaying:Z

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    iget-object v0, p0, Lcom/konka/videoPlayer/videoPlayerActivity;->videoHandler:Landroid/os/Handler;

    const/4 v1, 0x5

    invoke-virtual {v0, v1}, Landroid/os/Handler;->sendEmptyMessage(I)Z

    goto :goto_2

    :cond_8
    mul-int/lit8 p1, p1, 0x64

    if-gtz p1, :cond_9

    iget-object v0, p0, Lcom/konka/videoPlayer/videoPlayerActivity;->mVideoView:Lcom/konka/videoPlayer/VideoView;

    invoke-virtual {v0, v3}, Lcom/konka/videoPlayer/VideoView;->seekTo(I)V

    iget-object v0, p0, Lcom/konka/videoPlayer/videoPlayerActivity;->mediaPlayerTag:Ljava/lang/String;

    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "getCurrentPosition time====00000===="

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    iget-object v2, p0, Lcom/konka/videoPlayer/videoPlayerActivity;->mVideoView:Lcom/konka/videoPlayer/VideoView;

    invoke-virtual {v2}, Lcom/konka/videoPlayer/VideoView;->getCurrentPosition()I

    move-result v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    sget-object v0, Lcom/konka/mediaSharePlayer/PlayerState;->PLAY_STATUS_SEEK:Lcom/konka/mediaSharePlayer/PlayerState;

    invoke-virtual {v0}, Lcom/konka/mediaSharePlayer/PlayerState;->ordinal()I

    move-result v2

    iget-object v0, p0, Lcom/konka/videoPlayer/videoPlayerActivity;->mVideoView:Lcom/konka/videoPlayer/VideoView;

    invoke-virtual {v0}, Lcom/konka/videoPlayer/VideoView;->getDuration()I

    move-result v4

    new-instance v0, Ljava/lang/StringBuilder;

    iget-object v1, p0, Lcom/konka/videoPlayer/videoPlayerActivity;->sourcePath:Ljava/lang/String;

    invoke-static {v1}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    const-string v1, "&mv"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    move-object v0, p0

    move-object v1, p0

    invoke-virtual/range {v0 .. v5}, Lcom/konka/videoPlayer/videoPlayerActivity;->sendPlayerStateToPlayerService(Landroid/content/Context;IIILjava/lang/String;)V

    goto/16 :goto_1

    :cond_9
    iget-object v0, p0, Lcom/konka/videoPlayer/videoPlayerActivity;->mVideoView:Lcom/konka/videoPlayer/VideoView;

    invoke-virtual {v0}, Lcom/konka/videoPlayer/VideoView;->getDuration()I

    move-result v0

    if-lt p1, v0, :cond_a

    iget-object v0, p0, Lcom/konka/videoPlayer/videoPlayerActivity;->mVideoView:Lcom/konka/videoPlayer/VideoView;

    iget-object v1, p0, Lcom/konka/videoPlayer/videoPlayerActivity;->mVideoView:Lcom/konka/videoPlayer/VideoView;

    invoke-virtual {v1}, Lcom/konka/videoPlayer/VideoView;->getDuration()I

    move-result v1

    invoke-virtual {v0, v1}, Lcom/konka/videoPlayer/VideoView;->seekTo(I)V

    goto/16 :goto_1

    :cond_a
    iget-boolean v0, p0, Lcom/konka/videoPlayer/videoPlayerActivity;->isPause:Z

    if-eqz v0, :cond_b

    iget-object v0, p0, Lcom/konka/videoPlayer/videoPlayerActivity;->mVideoView:Lcom/konka/videoPlayer/VideoView;

    invoke-virtual {v0}, Lcom/konka/videoPlayer/VideoView;->start()V

    iput-boolean v3, p0, Lcom/konka/videoPlayer/videoPlayerActivity;->isPause:Z

    iget-object v0, p0, Lcom/konka/videoPlayer/videoPlayerActivity;->buttonPlay:Landroid/widget/Button;

    invoke-virtual {v0, v8}, Landroid/widget/Button;->setVisibility(I)V

    iget-object v0, p0, Lcom/konka/videoPlayer/videoPlayerActivity;->buttonPause:Landroid/widget/Button;

    invoke-virtual {v0, v3}, Landroid/widget/Button;->setVisibility(I)V

    :cond_b
    iget-object v0, p0, Lcom/konka/videoPlayer/videoPlayerActivity;->mVideoView:Lcom/konka/videoPlayer/VideoView;

    invoke-virtual {v0, p1}, Lcom/konka/videoPlayer/VideoView;->seekTo(I)V

    iget-object v0, p0, Lcom/konka/videoPlayer/videoPlayerActivity;->mediaPlayerTag:Ljava/lang/String;

    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "getCurrentPosition time========"

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    iget-object v0, p0, Lcom/konka/videoPlayer/videoPlayerActivity;->mVideoView:Lcom/konka/videoPlayer/VideoView;

    invoke-virtual {v0}, Lcom/konka/videoPlayer/VideoView;->isPlaying()Z

    move-result v0

    if-eqz v0, :cond_1

    iget-object v0, p0, Lcom/konka/videoPlayer/videoPlayerActivity;->mediaPlayerTag:Ljava/lang/String;

    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "getCurrentPosition time===1111====="

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    iget-object v2, p0, Lcom/konka/videoPlayer/videoPlayerActivity;->mVideoView:Lcom/konka/videoPlayer/VideoView;

    invoke-virtual {v2}, Lcom/konka/videoPlayer/VideoView;->getCurrentPosition()I

    move-result v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    iput-boolean v7, p0, Lcom/konka/videoPlayer/videoPlayerActivity;->isSeekTo:Z

    goto/16 :goto_1

    :cond_c
    if-gtz p1, :cond_d

    iget-object v0, p0, Lcom/konka/videoPlayer/videoPlayerActivity;->mVideoView:Lcom/konka/videoPlayer/VideoView;

    invoke-virtual {v0, v3}, Lcom/konka/videoPlayer/VideoView;->seekTo(I)V

    iget-object v0, p0, Lcom/konka/videoPlayer/videoPlayerActivity;->mediaPlayerTag:Ljava/lang/String;

    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "getCurrentPosition time====00000===="

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    iget-object v2, p0, Lcom/konka/videoPlayer/videoPlayerActivity;->mVideoView:Lcom/konka/videoPlayer/VideoView;

    invoke-virtual {v2}, Lcom/konka/videoPlayer/VideoView;->getCurrentPosition()I

    move-result v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    sget-object v0, Lcom/konka/mediaSharePlayer/PlayerState;->PLAY_STATUS_SEEK:Lcom/konka/mediaSharePlayer/PlayerState;

    invoke-virtual {v0}, Lcom/konka/mediaSharePlayer/PlayerState;->ordinal()I

    move-result v2

    iget-object v0, p0, Lcom/konka/videoPlayer/videoPlayerActivity;->mVideoView:Lcom/konka/videoPlayer/VideoView;

    invoke-virtual {v0}, Lcom/konka/videoPlayer/VideoView;->getDuration()I

    move-result v4

    new-instance v0, Ljava/lang/StringBuilder;

    iget-object v1, p0, Lcom/konka/videoPlayer/videoPlayerActivity;->sourcePath:Ljava/lang/String;

    invoke-static {v1}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    const-string v1, "&mv"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    move-object v0, p0

    move-object v1, p0

    invoke-virtual/range {v0 .. v5}, Lcom/konka/videoPlayer/videoPlayerActivity;->sendPlayerStateToPlayerService(Landroid/content/Context;IIILjava/lang/String;)V

    goto/16 :goto_1

    :cond_d
    iget-object v0, p0, Lcom/konka/videoPlayer/videoPlayerActivity;->mVideoView:Lcom/konka/videoPlayer/VideoView;

    invoke-virtual {v0}, Lcom/konka/videoPlayer/VideoView;->getDuration()I

    move-result v0

    if-lt p1, v0, :cond_e

    iget-object v0, p0, Lcom/konka/videoPlayer/videoPlayerActivity;->mVideoView:Lcom/konka/videoPlayer/VideoView;

    iget-object v1, p0, Lcom/konka/videoPlayer/videoPlayerActivity;->mVideoView:Lcom/konka/videoPlayer/VideoView;

    invoke-virtual {v1}, Lcom/konka/videoPlayer/VideoView;->getDuration()I

    move-result v1

    invoke-virtual {v0, v1}, Lcom/konka/videoPlayer/VideoView;->seekTo(I)V

    goto/16 :goto_1

    :cond_e
    iget-boolean v0, p0, Lcom/konka/videoPlayer/videoPlayerActivity;->isPause:Z

    if-eqz v0, :cond_f

    iget-object v0, p0, Lcom/konka/videoPlayer/videoPlayerActivity;->mVideoView:Lcom/konka/videoPlayer/VideoView;

    invoke-virtual {v0}, Lcom/konka/videoPlayer/VideoView;->start()V

    iput-boolean v3, p0, Lcom/konka/videoPlayer/videoPlayerActivity;->isPause:Z

    iget-object v0, p0, Lcom/konka/videoPlayer/videoPlayerActivity;->buttonPlay:Landroid/widget/Button;

    invoke-virtual {v0, v8}, Landroid/widget/Button;->setVisibility(I)V

    iget-object v0, p0, Lcom/konka/videoPlayer/videoPlayerActivity;->buttonPause:Landroid/widget/Button;

    invoke-virtual {v0, v3}, Landroid/widget/Button;->setVisibility(I)V

    :cond_f
    iget-object v0, p0, Lcom/konka/videoPlayer/videoPlayerActivity;->mVideoView:Lcom/konka/videoPlayer/VideoView;

    invoke-virtual {v0, p1}, Lcom/konka/videoPlayer/VideoView;->seekTo(I)V

    iget-object v0, p0, Lcom/konka/videoPlayer/videoPlayerActivity;->mediaPlayerTag:Ljava/lang/String;

    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "getCurrentPosition time========"

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    iget-object v0, p0, Lcom/konka/videoPlayer/videoPlayerActivity;->mVideoView:Lcom/konka/videoPlayer/VideoView;

    invoke-virtual {v0}, Lcom/konka/videoPlayer/VideoView;->isPlaying()Z

    move-result v0

    if-eqz v0, :cond_1

    iget-object v0, p0, Lcom/konka/videoPlayer/videoPlayerActivity;->mediaPlayerTag:Ljava/lang/String;

    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "getCurrentPosition time===1111====="

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    iget-object v2, p0, Lcom/konka/videoPlayer/videoPlayerActivity;->mVideoView:Lcom/konka/videoPlayer/VideoView;

    invoke-virtual {v2}, Lcom/konka/videoPlayer/VideoView;->getCurrentPosition()I

    move-result v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    iput-boolean v7, p0, Lcom/konka/videoPlayer/videoPlayerActivity;->isSeekTo:Z

    goto/16 :goto_1
.end method

.method public videoPlayerViewSetFocus(Landroid/view/View;)V
    .locals 1
    .param p1    # Landroid/view/View;

    const/4 v0, 0x1

    invoke-virtual {p1, v0}, Landroid/view/View;->setFocusable(Z)V

    invoke-virtual {p1}, Landroid/view/View;->requestFocus()Z

    return-void
.end method

.method public videoViewSetFocusable(Z)V
    .locals 1
    .param p1    # Z

    iget-object v0, p0, Lcom/konka/videoPlayer/videoPlayerActivity;->buttonNext:Landroid/widget/Button;

    invoke-virtual {v0, p1}, Landroid/widget/Button;->setFocusable(Z)V

    iget-object v0, p0, Lcom/konka/videoPlayer/videoPlayerActivity;->buttonPause:Landroid/widget/Button;

    invoke-virtual {v0, p1}, Landroid/widget/Button;->setFocusable(Z)V

    iget-object v0, p0, Lcom/konka/videoPlayer/videoPlayerActivity;->buttonPlay:Landroid/widget/Button;

    invoke-virtual {v0, p1}, Landroid/widget/Button;->setFocusable(Z)V

    iget-object v0, p0, Lcom/konka/videoPlayer/videoPlayerActivity;->buttonPre:Landroid/widget/Button;

    invoke-virtual {v0, p1}, Landroid/widget/Button;->setFocusable(Z)V

    iget-object v0, p0, Lcom/konka/videoPlayer/videoPlayerActivity;->seekbar:Landroid/widget/SeekBar;

    invoke-virtual {v0, p1}, Landroid/widget/SeekBar;->setFocusable(Z)V

    return-void
.end method
