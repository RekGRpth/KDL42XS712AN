.class public Lcom/konka/videoPlayer/VideoPlayerSubTitle;
.super Ljava/lang/Object;
.source "VideoPlayerSubTitle.java"


# instance fields
.field private mVideoName:Landroid/widget/TextView;

.field private mVideoPlayer:Lcom/konka/videoPlayer/videoPlayerActivity;

.field private mVideoUrl:Landroid/widget/TextView;

.field protected videoSubtitle:Landroid/widget/LinearLayout;


# direct methods
.method public constructor <init>(Lcom/konka/videoPlayer/videoPlayerActivity;)V
    .locals 0
    .param p1    # Lcom/konka/videoPlayer/videoPlayerActivity;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    iput-object p1, p0, Lcom/konka/videoPlayer/VideoPlayerSubTitle;->mVideoPlayer:Lcom/konka/videoPlayer/videoPlayerActivity;

    return-void
.end method


# virtual methods
.method public findBtnView()V
    .locals 2

    iget-object v0, p0, Lcom/konka/videoPlayer/VideoPlayerSubTitle;->mVideoPlayer:Lcom/konka/videoPlayer/videoPlayerActivity;

    const v1, 0x7f0b001f    # com.konka.mediaSharePlayer.R.id.playbar_videoname

    invoke-virtual {v0, v1}, Lcom/konka/videoPlayer/videoPlayerActivity;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    iput-object v0, p0, Lcom/konka/videoPlayer/VideoPlayerSubTitle;->mVideoName:Landroid/widget/TextView;

    iget-object v0, p0, Lcom/konka/videoPlayer/VideoPlayerSubTitle;->mVideoPlayer:Lcom/konka/videoPlayer/videoPlayerActivity;

    const v1, 0x7f0b001e    # com.konka.mediaSharePlayer.R.id.playbar_videourl

    invoke-virtual {v0, v1}, Lcom/konka/videoPlayer/videoPlayerActivity;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    iput-object v0, p0, Lcom/konka/videoPlayer/VideoPlayerSubTitle;->mVideoUrl:Landroid/widget/TextView;

    iget-object v0, p0, Lcom/konka/videoPlayer/VideoPlayerSubTitle;->mVideoPlayer:Lcom/konka/videoPlayer/videoPlayerActivity;

    const v1, 0x7f0b001d    # com.konka.mediaSharePlayer.R.id.video_subtitle

    invoke-virtual {v0, v1}, Lcom/konka/videoPlayer/videoPlayerActivity;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/LinearLayout;

    iput-object v0, p0, Lcom/konka/videoPlayer/VideoPlayerSubTitle;->videoSubtitle:Landroid/widget/LinearLayout;

    return-void
.end method

.method public setTextViewGivenName(Ljava/lang/String;)V
    .locals 2
    .param p1    # Ljava/lang/String;

    invoke-static {}, Ljava/lang/System;->gc()V

    iget-object v0, p0, Lcom/konka/videoPlayer/VideoPlayerSubTitle;->mVideoName:Landroid/widget/TextView;

    invoke-virtual {v0, p1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    iget-object v0, p0, Lcom/konka/videoPlayer/VideoPlayerSubTitle;->mVideoName:Landroid/widget/TextView;

    const/high16 v1, 0x41c80000    # 25.0f

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setTextSize(F)V

    return-void
.end method

.method public setTextViewName(Ljava/lang/String;)V
    .locals 3
    .param p1    # Ljava/lang/String;

    iget-object v1, p0, Lcom/konka/videoPlayer/VideoPlayerSubTitle;->mVideoUrl:Landroid/widget/TextView;

    const/16 v2, 0x8

    invoke-virtual {v1, v2}, Landroid/widget/TextView;->setVisibility(I)V

    invoke-static {}, Ljava/lang/System;->gc()V

    const-string v1, "/"

    invoke-virtual {p1, v1}, Ljava/lang/String;->lastIndexOf(Ljava/lang/String;)I

    move-result v1

    add-int/lit8 v1, v1, 0x1

    invoke-virtual {p1}, Ljava/lang/String;->length()I

    move-result v2

    invoke-virtual {p1, v1, v2}, Ljava/lang/String;->substring(II)Ljava/lang/String;

    move-result-object v1

    invoke-static {v1}, Ljava/net/URLDecoder;->decode(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    iget-object v1, p0, Lcom/konka/videoPlayer/VideoPlayerSubTitle;->mVideoName:Landroid/widget/TextView;

    invoke-virtual {v1, v0}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    iget-object v1, p0, Lcom/konka/videoPlayer/VideoPlayerSubTitle;->mVideoName:Landroid/widget/TextView;

    const/high16 v2, 0x41c80000    # 25.0f

    invoke-virtual {v1, v2}, Landroid/widget/TextView;->setTextSize(F)V

    return-void
.end method

.method public setTextViewUrl(Ljava/lang/String;)V
    .locals 2
    .param p1    # Ljava/lang/String;

    iget-object v0, p0, Lcom/konka/videoPlayer/VideoPlayerSubTitle;->mVideoUrl:Landroid/widget/TextView;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setVisibility(I)V

    invoke-static {}, Ljava/lang/System;->gc()V

    iget-object v0, p0, Lcom/konka/videoPlayer/VideoPlayerSubTitle;->mVideoUrl:Landroid/widget/TextView;

    invoke-virtual {v0, p1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    iget-object v0, p0, Lcom/konka/videoPlayer/VideoPlayerSubTitle;->mVideoUrl:Landroid/widget/TextView;

    const/high16 v1, 0x41400000    # 12.0f

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setTextSize(F)V

    return-void
.end method
