.class Lcom/konka/videoPlayer/videoPlayerActivity$1;
.super Landroid/os/Handler;
.source "videoPlayerActivity.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/konka/videoPlayer/videoPlayerActivity;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/konka/videoPlayer/videoPlayerActivity;


# direct methods
.method constructor <init>(Lcom/konka/videoPlayer/videoPlayerActivity;)V
    .locals 0

    iput-object p1, p0, Lcom/konka/videoPlayer/videoPlayerActivity$1;->this$0:Lcom/konka/videoPlayer/videoPlayerActivity;

    invoke-direct {p0}, Landroid/os/Handler;-><init>()V

    return-void
.end method


# virtual methods
.method public handleMessage(Landroid/os/Message;)V
    .locals 12
    .param p1    # Landroid/os/Message;

    const/4 v11, 0x0

    # getter for: Lcom/konka/videoPlayer/videoPlayerActivity;->mLongPressTimes:I
    invoke-static {}, Lcom/konka/videoPlayer/videoPlayerActivity;->access$0()I

    move-result v0

    if-lez v0, :cond_8

    iget-object v0, p0, Lcom/konka/videoPlayer/videoPlayerActivity$1;->this$0:Lcom/konka/videoPlayer/videoPlayerActivity;

    # getter for: Lcom/konka/videoPlayer/videoPlayerActivity;->isSegVideo:Ljava/lang/Boolean;
    invoke-static {v0}, Lcom/konka/videoPlayer/videoPlayerActivity;->access$1(Lcom/konka/videoPlayer/videoPlayerActivity;)Ljava/lang/Boolean;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v0

    if-eqz v0, :cond_4

    iget-object v0, p0, Lcom/konka/videoPlayer/videoPlayerActivity$1;->this$0:Lcom/konka/videoPlayer/videoPlayerActivity;

    # invokes: Lcom/konka/videoPlayer/videoPlayerActivity;->getCurrentPos()I
    invoke-static {v0}, Lcom/konka/videoPlayer/videoPlayerActivity;->access$2(Lcom/konka/videoPlayer/videoPlayerActivity;)I

    move-result v6

    iget-object v0, p0, Lcom/konka/videoPlayer/videoPlayerActivity$1;->this$0:Lcom/konka/videoPlayer/videoPlayerActivity;

    # invokes: Lcom/konka/videoPlayer/videoPlayerActivity;->getTotalDuration()I
    invoke-static {v0}, Lcom/konka/videoPlayer/videoPlayerActivity;->access$3(Lcom/konka/videoPlayer/videoPlayerActivity;)I

    move-result v7

    # getter for: Lcom/konka/videoPlayer/videoPlayerActivity;->mLongPressTimes:I
    invoke-static {}, Lcom/konka/videoPlayer/videoPlayerActivity;->access$0()I

    move-result v0

    mul-int/lit16 v0, v0, 0x12c

    add-int v9, v6, v0

    # getter for: Lcom/konka/videoPlayer/videoPlayerActivity;->mLongPressTimes:I
    invoke-static {}, Lcom/konka/videoPlayer/videoPlayerActivity;->access$0()I

    move-result v0

    mul-int/lit16 v0, v0, 0x12c

    sub-int v8, v6, v0

    const-string v0, "wangjinxin"

    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "mForwardCurrent progress bar current = "

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v1, v6}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, "mLongPressTimes*300 = "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    # getter for: Lcom/konka/videoPlayer/videoPlayerActivity;->mLongPressTimes:I
    invoke-static {}, Lcom/konka/videoPlayer/videoPlayerActivity;->access$0()I

    move-result v2

    mul-int/lit16 v2, v2, 0x12c

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, " total = "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, v9}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    # getter for: Lcom/konka/videoPlayer/videoPlayerActivity;->mPressForward:Z
    invoke-static {}, Lcom/konka/videoPlayer/videoPlayerActivity;->access$4()Z

    move-result v0

    if-eqz v0, :cond_2

    if-le v9, v7, :cond_1

    iget-object v0, p0, Lcom/konka/videoPlayer/videoPlayerActivity$1;->this$0:Lcom/konka/videoPlayer/videoPlayerActivity;

    # getter for: Lcom/konka/videoPlayer/videoPlayerActivity;->seekbar:Landroid/widget/SeekBar;
    invoke-static {v0}, Lcom/konka/videoPlayer/videoPlayerActivity;->access$5(Lcom/konka/videoPlayer/videoPlayerActivity;)Landroid/widget/SeekBar;

    move-result-object v0

    invoke-virtual {v0, v7}, Landroid/widget/SeekBar;->setProgress(I)V

    iget-object v0, p0, Lcom/konka/videoPlayer/videoPlayerActivity$1;->this$0:Lcom/konka/videoPlayer/videoPlayerActivity;

    # getter for: Lcom/konka/videoPlayer/videoPlayerActivity;->curtime:Landroid/widget/TextView;
    invoke-static {v0}, Lcom/konka/videoPlayer/videoPlayerActivity;->access$6(Lcom/konka/videoPlayer/videoPlayerActivity;)Landroid/widget/TextView;

    move-result-object v0

    invoke-static {v7}, Lcom/konka/musicPlayer/MusicTool;->calculateSegDuration(I)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    :cond_0
    :goto_0
    return-void

    :cond_1
    iget-object v0, p0, Lcom/konka/videoPlayer/videoPlayerActivity$1;->this$0:Lcom/konka/videoPlayer/videoPlayerActivity;

    # getter for: Lcom/konka/videoPlayer/videoPlayerActivity;->seekbar:Landroid/widget/SeekBar;
    invoke-static {v0}, Lcom/konka/videoPlayer/videoPlayerActivity;->access$5(Lcom/konka/videoPlayer/videoPlayerActivity;)Landroid/widget/SeekBar;

    move-result-object v0

    invoke-virtual {v0, v9}, Landroid/widget/SeekBar;->setProgress(I)V

    iget-object v0, p0, Lcom/konka/videoPlayer/videoPlayerActivity$1;->this$0:Lcom/konka/videoPlayer/videoPlayerActivity;

    # getter for: Lcom/konka/videoPlayer/videoPlayerActivity;->curtime:Landroid/widget/TextView;
    invoke-static {v0}, Lcom/konka/videoPlayer/videoPlayerActivity;->access$6(Lcom/konka/videoPlayer/videoPlayerActivity;)Landroid/widget/TextView;

    move-result-object v0

    invoke-static {v9}, Lcom/konka/musicPlayer/MusicTool;->calculateSegDuration(I)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    goto :goto_0

    :cond_2
    if-gez v8, :cond_3

    iget-object v0, p0, Lcom/konka/videoPlayer/videoPlayerActivity$1;->this$0:Lcom/konka/videoPlayer/videoPlayerActivity;

    # getter for: Lcom/konka/videoPlayer/videoPlayerActivity;->seekbar:Landroid/widget/SeekBar;
    invoke-static {v0}, Lcom/konka/videoPlayer/videoPlayerActivity;->access$5(Lcom/konka/videoPlayer/videoPlayerActivity;)Landroid/widget/SeekBar;

    move-result-object v0

    invoke-virtual {v0, v11}, Landroid/widget/SeekBar;->setProgress(I)V

    iget-object v0, p0, Lcom/konka/videoPlayer/videoPlayerActivity$1;->this$0:Lcom/konka/videoPlayer/videoPlayerActivity;

    # getter for: Lcom/konka/videoPlayer/videoPlayerActivity;->curtime:Landroid/widget/TextView;
    invoke-static {v0}, Lcom/konka/videoPlayer/videoPlayerActivity;->access$6(Lcom/konka/videoPlayer/videoPlayerActivity;)Landroid/widget/TextView;

    move-result-object v0

    invoke-static {v11}, Lcom/konka/musicPlayer/MusicTool;->calculateSegDuration(I)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    goto :goto_0

    :cond_3
    iget-object v0, p0, Lcom/konka/videoPlayer/videoPlayerActivity$1;->this$0:Lcom/konka/videoPlayer/videoPlayerActivity;

    # getter for: Lcom/konka/videoPlayer/videoPlayerActivity;->seekbar:Landroid/widget/SeekBar;
    invoke-static {v0}, Lcom/konka/videoPlayer/videoPlayerActivity;->access$5(Lcom/konka/videoPlayer/videoPlayerActivity;)Landroid/widget/SeekBar;

    move-result-object v0

    invoke-virtual {v0, v8}, Landroid/widget/SeekBar;->setProgress(I)V

    iget-object v0, p0, Lcom/konka/videoPlayer/videoPlayerActivity$1;->this$0:Lcom/konka/videoPlayer/videoPlayerActivity;

    # getter for: Lcom/konka/videoPlayer/videoPlayerActivity;->curtime:Landroid/widget/TextView;
    invoke-static {v0}, Lcom/konka/videoPlayer/videoPlayerActivity;->access$6(Lcom/konka/videoPlayer/videoPlayerActivity;)Landroid/widget/TextView;

    move-result-object v0

    invoke-static {v8}, Lcom/konka/musicPlayer/MusicTool;->calculateSegDuration(I)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    goto :goto_0

    :cond_4
    iget-object v0, p0, Lcom/konka/videoPlayer/videoPlayerActivity$1;->this$0:Lcom/konka/videoPlayer/videoPlayerActivity;

    # getter for: Lcom/konka/videoPlayer/videoPlayerActivity;->mVideoView:Lcom/konka/videoPlayer/VideoView;
    invoke-static {v0}, Lcom/konka/videoPlayer/videoPlayerActivity;->access$7(Lcom/konka/videoPlayer/videoPlayerActivity;)Lcom/konka/videoPlayer/VideoView;

    move-result-object v0

    invoke-virtual {v0}, Lcom/konka/videoPlayer/VideoView;->getDuration()I

    move-result v7

    iget-object v0, p0, Lcom/konka/videoPlayer/videoPlayerActivity$1;->this$0:Lcom/konka/videoPlayer/videoPlayerActivity;

    # getter for: Lcom/konka/videoPlayer/videoPlayerActivity;->mVideoView:Lcom/konka/videoPlayer/VideoView;
    invoke-static {v0}, Lcom/konka/videoPlayer/videoPlayerActivity;->access$7(Lcom/konka/videoPlayer/videoPlayerActivity;)Lcom/konka/videoPlayer/VideoView;

    move-result-object v0

    invoke-virtual {v0}, Lcom/konka/videoPlayer/VideoView;->getCurrentPosition()I

    move-result v0

    # getter for: Lcom/konka/videoPlayer/videoPlayerActivity;->mLongPressTimes:I
    invoke-static {}, Lcom/konka/videoPlayer/videoPlayerActivity;->access$0()I

    move-result v1

    mul-int/lit16 v1, v1, 0x7530

    add-int v9, v0, v1

    iget-object v0, p0, Lcom/konka/videoPlayer/videoPlayerActivity$1;->this$0:Lcom/konka/videoPlayer/videoPlayerActivity;

    # getter for: Lcom/konka/videoPlayer/videoPlayerActivity;->mVideoView:Lcom/konka/videoPlayer/VideoView;
    invoke-static {v0}, Lcom/konka/videoPlayer/videoPlayerActivity;->access$7(Lcom/konka/videoPlayer/videoPlayerActivity;)Lcom/konka/videoPlayer/VideoView;

    move-result-object v0

    invoke-virtual {v0}, Lcom/konka/videoPlayer/VideoView;->getCurrentPosition()I

    move-result v0

    # getter for: Lcom/konka/videoPlayer/videoPlayerActivity;->mLongPressTimes:I
    invoke-static {}, Lcom/konka/videoPlayer/videoPlayerActivity;->access$0()I

    move-result v1

    mul-int/lit16 v1, v1, 0x7530

    sub-int v8, v0, v1

    # getter for: Lcom/konka/videoPlayer/videoPlayerActivity;->mPressForward:Z
    invoke-static {}, Lcom/konka/videoPlayer/videoPlayerActivity;->access$4()Z

    move-result v0

    if-eqz v0, :cond_6

    if-le v9, v7, :cond_5

    iget-object v0, p0, Lcom/konka/videoPlayer/videoPlayerActivity$1;->this$0:Lcom/konka/videoPlayer/videoPlayerActivity;

    # getter for: Lcom/konka/videoPlayer/videoPlayerActivity;->seekbar:Landroid/widget/SeekBar;
    invoke-static {v0}, Lcom/konka/videoPlayer/videoPlayerActivity;->access$5(Lcom/konka/videoPlayer/videoPlayerActivity;)Landroid/widget/SeekBar;

    move-result-object v0

    invoke-virtual {v0, v7}, Landroid/widget/SeekBar;->setProgress(I)V

    iget-object v0, p0, Lcom/konka/videoPlayer/videoPlayerActivity$1;->this$0:Lcom/konka/videoPlayer/videoPlayerActivity;

    # getter for: Lcom/konka/videoPlayer/videoPlayerActivity;->curtime:Landroid/widget/TextView;
    invoke-static {v0}, Lcom/konka/videoPlayer/videoPlayerActivity;->access$6(Lcom/konka/videoPlayer/videoPlayerActivity;)Landroid/widget/TextView;

    move-result-object v0

    invoke-static {v7}, Lcom/konka/musicPlayer/MusicTool;->calculateDuration(I)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    goto/16 :goto_0

    :cond_5
    iget-object v0, p0, Lcom/konka/videoPlayer/videoPlayerActivity$1;->this$0:Lcom/konka/videoPlayer/videoPlayerActivity;

    # getter for: Lcom/konka/videoPlayer/videoPlayerActivity;->seekbar:Landroid/widget/SeekBar;
    invoke-static {v0}, Lcom/konka/videoPlayer/videoPlayerActivity;->access$5(Lcom/konka/videoPlayer/videoPlayerActivity;)Landroid/widget/SeekBar;

    move-result-object v0

    invoke-virtual {v0, v9}, Landroid/widget/SeekBar;->setProgress(I)V

    iget-object v0, p0, Lcom/konka/videoPlayer/videoPlayerActivity$1;->this$0:Lcom/konka/videoPlayer/videoPlayerActivity;

    # getter for: Lcom/konka/videoPlayer/videoPlayerActivity;->curtime:Landroid/widget/TextView;
    invoke-static {v0}, Lcom/konka/videoPlayer/videoPlayerActivity;->access$6(Lcom/konka/videoPlayer/videoPlayerActivity;)Landroid/widget/TextView;

    move-result-object v0

    invoke-static {v9}, Lcom/konka/musicPlayer/MusicTool;->calculateDuration(I)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    goto/16 :goto_0

    :cond_6
    if-gez v8, :cond_7

    iget-object v0, p0, Lcom/konka/videoPlayer/videoPlayerActivity$1;->this$0:Lcom/konka/videoPlayer/videoPlayerActivity;

    # getter for: Lcom/konka/videoPlayer/videoPlayerActivity;->seekbar:Landroid/widget/SeekBar;
    invoke-static {v0}, Lcom/konka/videoPlayer/videoPlayerActivity;->access$5(Lcom/konka/videoPlayer/videoPlayerActivity;)Landroid/widget/SeekBar;

    move-result-object v0

    invoke-virtual {v0, v11}, Landroid/widget/SeekBar;->setProgress(I)V

    iget-object v0, p0, Lcom/konka/videoPlayer/videoPlayerActivity$1;->this$0:Lcom/konka/videoPlayer/videoPlayerActivity;

    # getter for: Lcom/konka/videoPlayer/videoPlayerActivity;->curtime:Landroid/widget/TextView;
    invoke-static {v0}, Lcom/konka/videoPlayer/videoPlayerActivity;->access$6(Lcom/konka/videoPlayer/videoPlayerActivity;)Landroid/widget/TextView;

    move-result-object v0

    invoke-static {v11}, Lcom/konka/musicPlayer/MusicTool;->calculateDuration(I)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    goto/16 :goto_0

    :cond_7
    iget-object v0, p0, Lcom/konka/videoPlayer/videoPlayerActivity$1;->this$0:Lcom/konka/videoPlayer/videoPlayerActivity;

    # getter for: Lcom/konka/videoPlayer/videoPlayerActivity;->seekbar:Landroid/widget/SeekBar;
    invoke-static {v0}, Lcom/konka/videoPlayer/videoPlayerActivity;->access$5(Lcom/konka/videoPlayer/videoPlayerActivity;)Landroid/widget/SeekBar;

    move-result-object v0

    invoke-virtual {v0, v8}, Landroid/widget/SeekBar;->setProgress(I)V

    iget-object v0, p0, Lcom/konka/videoPlayer/videoPlayerActivity$1;->this$0:Lcom/konka/videoPlayer/videoPlayerActivity;

    # getter for: Lcom/konka/videoPlayer/videoPlayerActivity;->curtime:Landroid/widget/TextView;
    invoke-static {v0}, Lcom/konka/videoPlayer/videoPlayerActivity;->access$6(Lcom/konka/videoPlayer/videoPlayerActivity;)Landroid/widget/TextView;

    move-result-object v0

    invoke-static {v8}, Lcom/konka/musicPlayer/MusicTool;->calculateDuration(I)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    goto/16 :goto_0

    :cond_8
    const-string v0, "VideoPlayer"

    const-string v1, "Handler()"

    invoke-static {v0, v1}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    iget-object v0, p0, Lcom/konka/videoPlayer/videoPlayerActivity$1;->this$0:Lcom/konka/videoPlayer/videoPlayerActivity;

    # getter for: Lcom/konka/videoPlayer/videoPlayerActivity;->isSegVideo:Ljava/lang/Boolean;
    invoke-static {v0}, Lcom/konka/videoPlayer/videoPlayerActivity;->access$1(Lcom/konka/videoPlayer/videoPlayerActivity;)Ljava/lang/Boolean;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v0

    if-eqz v0, :cond_b

    iget-object v0, p0, Lcom/konka/videoPlayer/videoPlayerActivity$1;->this$0:Lcom/konka/videoPlayer/videoPlayerActivity;

    iget-boolean v0, v0, Lcom/konka/videoPlayer/videoPlayerActivity;->isplaying:Z

    if-eqz v0, :cond_a

    iget-object v0, p0, Lcom/konka/videoPlayer/videoPlayerActivity$1;->this$0:Lcom/konka/videoPlayer/videoPlayerActivity;

    iget-object v1, p0, Lcom/konka/videoPlayer/videoPlayerActivity$1;->this$0:Lcom/konka/videoPlayer/videoPlayerActivity;

    # getter for: Lcom/konka/videoPlayer/videoPlayerActivity;->mVideoView:Lcom/konka/videoPlayer/VideoView;
    invoke-static {v1}, Lcom/konka/videoPlayer/videoPlayerActivity;->access$7(Lcom/konka/videoPlayer/videoPlayerActivity;)Lcom/konka/videoPlayer/VideoView;

    move-result-object v1

    invoke-virtual {v1}, Lcom/konka/videoPlayer/VideoView;->getCurrentPosition()I

    move-result v1

    iget-object v2, p0, Lcom/konka/videoPlayer/videoPlayerActivity$1;->this$0:Lcom/konka/videoPlayer/videoPlayerActivity;

    # getter for: Lcom/konka/videoPlayer/videoPlayerActivity;->mVideoView:Lcom/konka/videoPlayer/VideoView;
    invoke-static {v2}, Lcom/konka/videoPlayer/videoPlayerActivity;->access$7(Lcom/konka/videoPlayer/videoPlayerActivity;)Lcom/konka/videoPlayer/VideoView;

    move-result-object v2

    invoke-virtual {v2}, Lcom/konka/videoPlayer/VideoView;->getDuration()I

    move-result v2

    invoke-virtual {v0, v1, v2}, Lcom/konka/videoPlayer/videoPlayerActivity;->refreshSegVideoBar(II)V

    const-string v0, "VideoPlayer"

    const-string v1, "is playing!!!!!"

    invoke-static {v0, v1}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    :goto_1
    iget-object v0, p0, Lcom/konka/videoPlayer/videoPlayerActivity$1;->this$0:Lcom/konka/videoPlayer/videoPlayerActivity;

    iget-boolean v0, v0, Lcom/konka/videoPlayer/videoPlayerActivity;->isSeekTo:Z

    if-eqz v0, :cond_c

    new-instance v0, Ljava/lang/StringBuilder;

    const-string v1, "mVideoView.getCurrentPosition() = "

    invoke-direct {v0, v1}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    iget-object v1, p0, Lcom/konka/videoPlayer/videoPlayerActivity$1;->this$0:Lcom/konka/videoPlayer/videoPlayerActivity;

    # getter for: Lcom/konka/videoPlayer/videoPlayerActivity;->mVideoView:Lcom/konka/videoPlayer/VideoView;
    invoke-static {v1}, Lcom/konka/videoPlayer/videoPlayerActivity;->access$7(Lcom/konka/videoPlayer/videoPlayerActivity;)Lcom/konka/videoPlayer/VideoView;

    move-result-object v1

    invoke-virtual {v1}, Lcom/konka/videoPlayer/VideoView;->getCurrentPosition()I

    move-result v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "mVideoView.getDuration() = "

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    iget-object v2, p0, Lcom/konka/videoPlayer/videoPlayerActivity$1;->this$0:Lcom/konka/videoPlayer/videoPlayerActivity;

    # getter for: Lcom/konka/videoPlayer/videoPlayerActivity;->mVideoView:Lcom/konka/videoPlayer/VideoView;
    invoke-static {v2}, Lcom/konka/videoPlayer/videoPlayerActivity;->access$7(Lcom/konka/videoPlayer/videoPlayerActivity;)Lcom/konka/videoPlayer/VideoView;

    move-result-object v2

    invoke-virtual {v2}, Lcom/konka/videoPlayer/VideoView;->getDuration()I

    move-result v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    iget-object v0, p0, Lcom/konka/videoPlayer/videoPlayerActivity$1;->this$0:Lcom/konka/videoPlayer/videoPlayerActivity;

    # getter for: Lcom/konka/videoPlayer/videoPlayerActivity;->mVideoView:Lcom/konka/videoPlayer/VideoView;
    invoke-static {v0}, Lcom/konka/videoPlayer/videoPlayerActivity;->access$7(Lcom/konka/videoPlayer/videoPlayerActivity;)Lcom/konka/videoPlayer/VideoView;

    move-result-object v0

    invoke-virtual {v0}, Lcom/konka/videoPlayer/VideoView;->isPlaying()Z

    move-result v0

    if-eqz v0, :cond_9

    iget-object v0, p0, Lcom/konka/videoPlayer/videoPlayerActivity$1;->this$0:Lcom/konka/videoPlayer/videoPlayerActivity;

    iget-object v1, p0, Lcom/konka/videoPlayer/videoPlayerActivity$1;->this$0:Lcom/konka/videoPlayer/videoPlayerActivity;

    sget-object v2, Lcom/konka/mediaSharePlayer/PlayerState;->PLAY_STATUS_SEEK:Lcom/konka/mediaSharePlayer/PlayerState;

    invoke-virtual {v2}, Lcom/konka/mediaSharePlayer/PlayerState;->ordinal()I

    move-result v2

    iget-object v3, p0, Lcom/konka/videoPlayer/videoPlayerActivity$1;->this$0:Lcom/konka/videoPlayer/videoPlayerActivity;

    # getter for: Lcom/konka/videoPlayer/videoPlayerActivity;->mVideoView:Lcom/konka/videoPlayer/VideoView;
    invoke-static {v3}, Lcom/konka/videoPlayer/videoPlayerActivity;->access$7(Lcom/konka/videoPlayer/videoPlayerActivity;)Lcom/konka/videoPlayer/VideoView;

    move-result-object v3

    invoke-virtual {v3}, Lcom/konka/videoPlayer/VideoView;->getCurrentPosition()I

    move-result v3

    iget-object v4, p0, Lcom/konka/videoPlayer/videoPlayerActivity$1;->this$0:Lcom/konka/videoPlayer/videoPlayerActivity;

    # getter for: Lcom/konka/videoPlayer/videoPlayerActivity;->mVideoView:Lcom/konka/videoPlayer/VideoView;
    invoke-static {v4}, Lcom/konka/videoPlayer/videoPlayerActivity;->access$7(Lcom/konka/videoPlayer/videoPlayerActivity;)Lcom/konka/videoPlayer/VideoView;

    move-result-object v4

    invoke-virtual {v4}, Lcom/konka/videoPlayer/VideoView;->getDuration()I

    move-result v4

    new-instance v5, Ljava/lang/StringBuilder;

    iget-object v10, p0, Lcom/konka/videoPlayer/videoPlayerActivity$1;->this$0:Lcom/konka/videoPlayer/videoPlayerActivity;

    # getter for: Lcom/konka/videoPlayer/videoPlayerActivity;->sourcePath:Ljava/lang/String;
    invoke-static {v10}, Lcom/konka/videoPlayer/videoPlayerActivity;->access$8(Lcom/konka/videoPlayer/videoPlayerActivity;)Ljava/lang/String;

    move-result-object v10

    invoke-static {v10}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v10

    invoke-direct {v5, v10}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    const-string v10, "&mv"

    invoke-virtual {v5, v10}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    invoke-virtual/range {v0 .. v5}, Lcom/konka/videoPlayer/videoPlayerActivity;->sendPlayerStateToPlayerService(Landroid/content/Context;IIILjava/lang/String;)V

    :cond_9
    iget-object v0, p0, Lcom/konka/videoPlayer/videoPlayerActivity$1;->this$0:Lcom/konka/videoPlayer/videoPlayerActivity;

    iput-boolean v11, v0, Lcom/konka/videoPlayer/videoPlayerActivity;->isSeekTo:Z

    goto/16 :goto_0

    :cond_a
    const-string v0, "VideoPlayer"

    const-string v1, "no playing!!!!!"

    invoke-static {v0, v1}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    goto/16 :goto_1

    :cond_b
    iget-object v0, p0, Lcom/konka/videoPlayer/videoPlayerActivity$1;->this$0:Lcom/konka/videoPlayer/videoPlayerActivity;

    iget-object v1, p0, Lcom/konka/videoPlayer/videoPlayerActivity$1;->this$0:Lcom/konka/videoPlayer/videoPlayerActivity;

    # getter for: Lcom/konka/videoPlayer/videoPlayerActivity;->mVideoView:Lcom/konka/videoPlayer/VideoView;
    invoke-static {v1}, Lcom/konka/videoPlayer/videoPlayerActivity;->access$7(Lcom/konka/videoPlayer/videoPlayerActivity;)Lcom/konka/videoPlayer/VideoView;

    move-result-object v1

    invoke-virtual {v1}, Lcom/konka/videoPlayer/VideoView;->getCurrentPosition()I

    move-result v1

    iget-object v2, p0, Lcom/konka/videoPlayer/videoPlayerActivity$1;->this$0:Lcom/konka/videoPlayer/videoPlayerActivity;

    # getter for: Lcom/konka/videoPlayer/videoPlayerActivity;->mVideoView:Lcom/konka/videoPlayer/VideoView;
    invoke-static {v2}, Lcom/konka/videoPlayer/videoPlayerActivity;->access$7(Lcom/konka/videoPlayer/videoPlayerActivity;)Lcom/konka/videoPlayer/VideoView;

    move-result-object v2

    invoke-virtual {v2}, Lcom/konka/videoPlayer/VideoView;->getDuration()I

    move-result v2

    invoke-virtual {v0, v1, v2}, Lcom/konka/videoPlayer/videoPlayerActivity;->refresh(II)V

    goto/16 :goto_1

    :cond_c
    iget-object v0, p0, Lcom/konka/videoPlayer/videoPlayerActivity$1;->this$0:Lcom/konka/videoPlayer/videoPlayerActivity;

    iget-boolean v0, v0, Lcom/konka/videoPlayer/videoPlayerActivity;->isSend:Z

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/konka/videoPlayer/videoPlayerActivity$1;->this$0:Lcom/konka/videoPlayer/videoPlayerActivity;

    iget v0, v0, Lcom/konka/videoPlayer/videoPlayerActivity;->relayTimer:I

    const/4 v1, 0x5

    if-gt v0, v1, :cond_d

    iget-object v0, p0, Lcom/konka/videoPlayer/videoPlayerActivity$1;->this$0:Lcom/konka/videoPlayer/videoPlayerActivity;

    iget v1, v0, Lcom/konka/videoPlayer/videoPlayerActivity;->relayTimer:I

    add-int/lit8 v1, v1, 0x1

    iput v1, v0, Lcom/konka/videoPlayer/videoPlayerActivity;->relayTimer:I

    iget-object v0, p0, Lcom/konka/videoPlayer/videoPlayerActivity$1;->this$0:Lcom/konka/videoPlayer/videoPlayerActivity;

    iget-object v1, p0, Lcom/konka/videoPlayer/videoPlayerActivity$1;->this$0:Lcom/konka/videoPlayer/videoPlayerActivity;

    sget-object v2, Lcom/konka/mediaSharePlayer/PlayerState;->PLAY_STATUS_SYNC:Lcom/konka/mediaSharePlayer/PlayerState;

    invoke-virtual {v2}, Lcom/konka/mediaSharePlayer/PlayerState;->ordinal()I

    move-result v2

    iget-object v3, p0, Lcom/konka/videoPlayer/videoPlayerActivity$1;->this$0:Lcom/konka/videoPlayer/videoPlayerActivity;

    # getter for: Lcom/konka/videoPlayer/videoPlayerActivity;->mVideoView:Lcom/konka/videoPlayer/VideoView;
    invoke-static {v3}, Lcom/konka/videoPlayer/videoPlayerActivity;->access$7(Lcom/konka/videoPlayer/videoPlayerActivity;)Lcom/konka/videoPlayer/VideoView;

    move-result-object v3

    invoke-virtual {v3}, Lcom/konka/videoPlayer/VideoView;->getCurrentPosition()I

    move-result v3

    iget-object v4, p0, Lcom/konka/videoPlayer/videoPlayerActivity$1;->this$0:Lcom/konka/videoPlayer/videoPlayerActivity;

    # getter for: Lcom/konka/videoPlayer/videoPlayerActivity;->mVideoView:Lcom/konka/videoPlayer/VideoView;
    invoke-static {v4}, Lcom/konka/videoPlayer/videoPlayerActivity;->access$7(Lcom/konka/videoPlayer/videoPlayerActivity;)Lcom/konka/videoPlayer/VideoView;

    move-result-object v4

    invoke-virtual {v4}, Lcom/konka/videoPlayer/VideoView;->getDuration()I

    move-result v4

    new-instance v5, Ljava/lang/StringBuilder;

    iget-object v10, p0, Lcom/konka/videoPlayer/videoPlayerActivity$1;->this$0:Lcom/konka/videoPlayer/videoPlayerActivity;

    # getter for: Lcom/konka/videoPlayer/videoPlayerActivity;->sourcePath:Ljava/lang/String;
    invoke-static {v10}, Lcom/konka/videoPlayer/videoPlayerActivity;->access$8(Lcom/konka/videoPlayer/videoPlayerActivity;)Ljava/lang/String;

    move-result-object v10

    invoke-static {v10}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v10

    invoke-direct {v5, v10}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    const-string v10, "&mv"

    invoke-virtual {v5, v10}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    invoke-virtual/range {v0 .. v5}, Lcom/konka/videoPlayer/videoPlayerActivity;->sendPlayerStateToPlayerService(Landroid/content/Context;IIILjava/lang/String;)V

    goto/16 :goto_0

    :cond_d
    iget-object v0, p0, Lcom/konka/videoPlayer/videoPlayerActivity$1;->this$0:Lcom/konka/videoPlayer/videoPlayerActivity;

    iput v11, v0, Lcom/konka/videoPlayer/videoPlayerActivity;->relayTimer:I

    iget-object v0, p0, Lcom/konka/videoPlayer/videoPlayerActivity$1;->this$0:Lcom/konka/videoPlayer/videoPlayerActivity;

    iput-boolean v11, v0, Lcom/konka/videoPlayer/videoPlayerActivity;->isSend:Z

    goto/16 :goto_0
.end method
