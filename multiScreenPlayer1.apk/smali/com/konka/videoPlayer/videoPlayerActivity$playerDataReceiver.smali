.class public Lcom/konka/videoPlayer/videoPlayerActivity$playerDataReceiver;
.super Landroid/content/BroadcastReceiver;
.source "videoPlayerActivity.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/konka/videoPlayer/videoPlayerActivity;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x1
    name = "playerDataReceiver"
.end annotation


# instance fields
.field final synthetic this$0:Lcom/konka/videoPlayer/videoPlayerActivity;


# direct methods
.method public constructor <init>(Lcom/konka/videoPlayer/videoPlayerActivity;)V
    .locals 0

    iput-object p1, p0, Lcom/konka/videoPlayer/videoPlayerActivity$playerDataReceiver;->this$0:Lcom/konka/videoPlayer/videoPlayerActivity;

    invoke-direct {p0}, Landroid/content/BroadcastReceiver;-><init>()V

    return-void
.end method


# virtual methods
.method public onReceive(Landroid/content/Context;Landroid/content/Intent;)V
    .locals 12
    .param p1    # Landroid/content/Context;
    .param p2    # Landroid/content/Intent;

    iget-object v6, p0, Lcom/konka/videoPlayer/videoPlayerActivity$playerDataReceiver;->this$0:Lcom/konka/videoPlayer/videoPlayerActivity;

    # getter for: Lcom/konka/videoPlayer/videoPlayerActivity;->mAlterDialog:Landroid/app/AlertDialog;
    invoke-static {v6}, Lcom/konka/videoPlayer/videoPlayerActivity;->access$17(Lcom/konka/videoPlayer/videoPlayerActivity;)Landroid/app/AlertDialog;

    move-result-object v6

    if-eqz v6, :cond_0

    iget-object v6, p0, Lcom/konka/videoPlayer/videoPlayerActivity$playerDataReceiver;->this$0:Lcom/konka/videoPlayer/videoPlayerActivity;

    # getter for: Lcom/konka/videoPlayer/videoPlayerActivity;->mAlterDialog:Landroid/app/AlertDialog;
    invoke-static {v6}, Lcom/konka/videoPlayer/videoPlayerActivity;->access$17(Lcom/konka/videoPlayer/videoPlayerActivity;)Landroid/app/AlertDialog;

    move-result-object v6

    invoke-virtual {v6}, Landroid/app/AlertDialog;->dismiss()V

    :cond_0
    invoke-virtual {p2}, Landroid/content/Intent;->getExtras()Landroid/os/Bundle;

    move-result-object v0

    iget-object v6, p0, Lcom/konka/videoPlayer/videoPlayerActivity$playerDataReceiver;->this$0:Lcom/konka/videoPlayer/videoPlayerActivity;

    const-string v7, "comfrom"

    const/4 v8, 0x0

    invoke-virtual {v0, v7, v8}, Landroid/os/Bundle;->getInt(Ljava/lang/String;I)I

    move-result v7

    invoke-static {v6, v7}, Lcom/konka/videoPlayer/videoPlayerActivity;->access$18(Lcom/konka/videoPlayer/videoPlayerActivity;I)V

    iget-object v6, p0, Lcom/konka/videoPlayer/videoPlayerActivity$playerDataReceiver;->this$0:Lcom/konka/videoPlayer/videoPlayerActivity;

    # getter for: Lcom/konka/videoPlayer/videoPlayerActivity;->comfrom:I
    invoke-static {v6}, Lcom/konka/videoPlayer/videoPlayerActivity;->access$19(Lcom/konka/videoPlayer/videoPlayerActivity;)I

    move-result v6

    const/4 v7, 0x1

    if-ne v6, v7, :cond_4

    iget-object v6, p0, Lcom/konka/videoPlayer/videoPlayerActivity$playerDataReceiver;->this$0:Lcom/konka/videoPlayer/videoPlayerActivity;

    const-string v7, "hideController"

    const/4 v8, 0x0

    invoke-virtual {v0, v7, v8}, Landroid/os/Bundle;->getBoolean(Ljava/lang/String;Z)Z

    move-result v7

    invoke-static {v6, v7}, Lcom/konka/videoPlayer/videoPlayerActivity;->access$20(Lcom/konka/videoPlayer/videoPlayerActivity;Z)V

    const-string v6, "isStringArrayList"

    invoke-virtual {v0, v6}, Landroid/os/Bundle;->getBoolean(Ljava/lang/String;)Z

    move-result v6

    if-eqz v6, :cond_3

    iget-object v6, p0, Lcom/konka/videoPlayer/videoPlayerActivity$playerDataReceiver;->this$0:Lcom/konka/videoPlayer/videoPlayerActivity;

    const-string v7, "urllist"

    invoke-virtual {v0, v7}, Landroid/os/Bundle;->getStringArrayList(Ljava/lang/String;)Ljava/util/ArrayList;

    move-result-object v7

    invoke-static {v6, v7}, Lcom/konka/videoPlayer/videoPlayerActivity;->access$21(Lcom/konka/videoPlayer/videoPlayerActivity;Ljava/util/ArrayList;)V

    iget-object v6, p0, Lcom/konka/videoPlayer/videoPlayerActivity$playerDataReceiver;->this$0:Lcom/konka/videoPlayer/videoPlayerActivity;

    iget-object v7, p0, Lcom/konka/videoPlayer/videoPlayerActivity$playerDataReceiver;->this$0:Lcom/konka/videoPlayer/videoPlayerActivity;

    # getter for: Lcom/konka/videoPlayer/videoPlayerActivity;->urlList:Ljava/util/ArrayList;
    invoke-static {v7}, Lcom/konka/videoPlayer/videoPlayerActivity;->access$22(Lcom/konka/videoPlayer/videoPlayerActivity;)Ljava/util/ArrayList;

    move-result-object v7

    invoke-virtual {v7}, Ljava/util/ArrayList;->size()I

    move-result v7

    invoke-static {v6, v7}, Lcom/konka/videoPlayer/videoPlayerActivity;->access$23(Lcom/konka/videoPlayer/videoPlayerActivity;I)V

    iget-object v6, p0, Lcom/konka/videoPlayer/videoPlayerActivity$playerDataReceiver;->this$0:Lcom/konka/videoPlayer/videoPlayerActivity;

    const/4 v7, 0x0

    invoke-static {v6, v7}, Lcom/konka/videoPlayer/videoPlayerActivity;->access$24(Lcom/konka/videoPlayer/videoPlayerActivity;I)V

    iget-object v6, p0, Lcom/konka/videoPlayer/videoPlayerActivity$playerDataReceiver;->this$0:Lcom/konka/videoPlayer/videoPlayerActivity;

    const-string v7, "durationtime"

    invoke-virtual {v0, v7}, Landroid/os/Bundle;->getStringArrayList(Ljava/lang/String;)Ljava/util/ArrayList;

    move-result-object v7

    invoke-static {v6, v7}, Lcom/konka/videoPlayer/videoPlayerActivity;->access$25(Lcom/konka/videoPlayer/videoPlayerActivity;Ljava/util/ArrayList;)V

    const/4 v2, 0x0

    :goto_0
    iget-object v6, p0, Lcom/konka/videoPlayer/videoPlayerActivity$playerDataReceiver;->this$0:Lcom/konka/videoPlayer/videoPlayerActivity;

    # getter for: Lcom/konka/videoPlayer/videoPlayerActivity;->urlList:Ljava/util/ArrayList;
    invoke-static {v6}, Lcom/konka/videoPlayer/videoPlayerActivity;->access$22(Lcom/konka/videoPlayer/videoPlayerActivity;)Ljava/util/ArrayList;

    move-result-object v6

    invoke-virtual {v6}, Ljava/util/ArrayList;->size()I

    move-result v6

    if-lt v2, v6, :cond_2

    const-string v6, "weikan"

    new-instance v7, Ljava/lang/StringBuilder;

    const-string v8, "palyer total duration = "

    invoke-direct {v7, v8}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    iget-object v8, p0, Lcom/konka/videoPlayer/videoPlayerActivity$playerDataReceiver;->this$0:Lcom/konka/videoPlayer/videoPlayerActivity;

    # getter for: Lcom/konka/videoPlayer/videoPlayerActivity;->mTotalTimeSegVideo:J
    invoke-static {v8}, Lcom/konka/videoPlayer/videoPlayerActivity;->access$27(Lcom/konka/videoPlayer/videoPlayerActivity;)J

    move-result-wide v8

    invoke-virtual {v7, v8, v9}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v7

    invoke-virtual {v7}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v7

    invoke-static {v6, v7}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    :goto_1
    const-string v6, "VideoPlayer"

    new-instance v7, Ljava/lang/StringBuilder;

    const-string v8, "sourcePath ----------:"

    invoke-direct {v7, v8}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    iget-object v8, p0, Lcom/konka/videoPlayer/videoPlayerActivity$playerDataReceiver;->this$0:Lcom/konka/videoPlayer/videoPlayerActivity;

    # getter for: Lcom/konka/videoPlayer/videoPlayerActivity;->sourcePath:Ljava/lang/String;
    invoke-static {v8}, Lcom/konka/videoPlayer/videoPlayerActivity;->access$8(Lcom/konka/videoPlayer/videoPlayerActivity;)Ljava/lang/String;

    move-result-object v8

    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    invoke-virtual {v7}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v7

    invoke-static {v6, v7}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    iget-object v6, p0, Lcom/konka/videoPlayer/videoPlayerActivity$playerDataReceiver;->this$0:Lcom/konka/videoPlayer/videoPlayerActivity;

    const-string v7, "videotitle"

    const-string v8, ""

    invoke-virtual {v0, v7, v8}, Landroid/os/Bundle;->getString(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v7

    invoke-static {v6, v7}, Lcom/konka/videoPlayer/videoPlayerActivity;->access$30(Lcom/konka/videoPlayer/videoPlayerActivity;Ljava/lang/String;)V

    iget-object v6, p0, Lcom/konka/videoPlayer/videoPlayerActivity$playerDataReceiver;->this$0:Lcom/konka/videoPlayer/videoPlayerActivity;

    const-string v7, "haveNextFlag"

    const/4 v8, 0x0

    invoke-virtual {v0, v7, v8}, Landroid/os/Bundle;->getInt(Ljava/lang/String;I)I

    move-result v7

    invoke-static {v6, v7}, Lcom/konka/videoPlayer/videoPlayerActivity;->access$31(Lcom/konka/videoPlayer/videoPlayerActivity;I)V

    iget-object v6, p0, Lcom/konka/videoPlayer/videoPlayerActivity$playerDataReceiver;->this$0:Lcom/konka/videoPlayer/videoPlayerActivity;

    const-string v7, "mediastop"

    const/4 v8, 0x0

    invoke-virtual {v0, v7, v8}, Landroid/os/Bundle;->getInt(Ljava/lang/String;I)I

    move-result v7

    iput v7, v6, Lcom/konka/videoPlayer/videoPlayerActivity;->mediastop:I

    iget-object v6, p0, Lcom/konka/videoPlayer/videoPlayerActivity$playerDataReceiver;->this$0:Lcom/konka/videoPlayer/videoPlayerActivity;

    const-string v7, "resumePos"

    const/4 v8, 0x0

    invoke-virtual {v0, v7, v8}, Landroid/os/Bundle;->getInt(Ljava/lang/String;I)I

    move-result v7

    iput v7, v6, Lcom/konka/videoPlayer/videoPlayerActivity;->resumePos:I

    iget-object v6, p0, Lcom/konka/videoPlayer/videoPlayerActivity$playerDataReceiver;->this$0:Lcom/konka/videoPlayer/videoPlayerActivity;

    const-string v7, "comefrom"

    const/4 v8, 0x0

    invoke-virtual {v0, v7, v8}, Landroid/os/Bundle;->getInt(Ljava/lang/String;I)I

    move-result v7

    iput v7, v6, Lcom/konka/videoPlayer/videoPlayerActivity;->comefrom:I

    const-string v6, "VideoPlayer"

    new-instance v7, Ljava/lang/StringBuilder;

    const-string v8, "haveNextFlag ----------2:"

    invoke-direct {v7, v8}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    iget-object v8, p0, Lcom/konka/videoPlayer/videoPlayerActivity$playerDataReceiver;->this$0:Lcom/konka/videoPlayer/videoPlayerActivity;

    # getter for: Lcom/konka/videoPlayer/videoPlayerActivity;->haveNextFlag:I
    invoke-static {v8}, Lcom/konka/videoPlayer/videoPlayerActivity;->access$32(Lcom/konka/videoPlayer/videoPlayerActivity;)I

    move-result v8

    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v7

    invoke-virtual {v7}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v7

    invoke-static {v6, v7}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    iget-object v6, p0, Lcom/konka/videoPlayer/videoPlayerActivity$playerDataReceiver;->this$0:Lcom/konka/videoPlayer/videoPlayerActivity;

    const-string v7, "mediastop"

    const/4 v8, 0x1

    invoke-virtual {v0, v7, v8}, Landroid/os/Bundle;->getInt(Ljava/lang/String;I)I

    move-result v7

    iput v7, v6, Lcom/konka/videoPlayer/videoPlayerActivity;->mediastop:I

    iget-object v6, p0, Lcom/konka/videoPlayer/videoPlayerActivity$playerDataReceiver;->this$0:Lcom/konka/videoPlayer/videoPlayerActivity;

    const-string v7, "isStringArrayList"

    invoke-virtual {v0, v7}, Landroid/os/Bundle;->getBoolean(Ljava/lang/String;)Z

    move-result v7

    invoke-static {v7}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v7

    invoke-static {v6, v7}, Lcom/konka/videoPlayer/videoPlayerActivity;->access$33(Lcom/konka/videoPlayer/videoPlayerActivity;Ljava/lang/Boolean;)V

    iget-object v6, p0, Lcom/konka/videoPlayer/videoPlayerActivity$playerDataReceiver;->this$0:Lcom/konka/videoPlayer/videoPlayerActivity;

    # getter for: Lcom/konka/videoPlayer/videoPlayerActivity;->isSegVideo:Ljava/lang/Boolean;
    invoke-static {v6}, Lcom/konka/videoPlayer/videoPlayerActivity;->access$1(Lcom/konka/videoPlayer/videoPlayerActivity;)Ljava/lang/Boolean;

    move-result-object v6

    invoke-virtual {v6}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v6

    if-eqz v6, :cond_5

    iget-object v6, p0, Lcom/konka/videoPlayer/videoPlayerActivity$playerDataReceiver;->this$0:Lcom/konka/videoPlayer/videoPlayerActivity;

    iget-object v6, v6, Lcom/konka/videoPlayer/videoPlayerActivity;->classVideoSubtitle:Lcom/konka/videoPlayer/VideoPlayerSubTitle;

    iget-object v7, p0, Lcom/konka/videoPlayer/videoPlayerActivity$playerDataReceiver;->this$0:Lcom/konka/videoPlayer/videoPlayerActivity;

    # getter for: Lcom/konka/videoPlayer/videoPlayerActivity;->mVideoName:Ljava/lang/String;
    invoke-static {v7}, Lcom/konka/videoPlayer/videoPlayerActivity;->access$34(Lcom/konka/videoPlayer/videoPlayerActivity;)Ljava/lang/String;

    move-result-object v7

    invoke-virtual {v6, v7}, Lcom/konka/videoPlayer/VideoPlayerSubTitle;->setTextViewGivenName(Ljava/lang/String;)V

    iget-object v6, p0, Lcom/konka/videoPlayer/videoPlayerActivity$playerDataReceiver;->this$0:Lcom/konka/videoPlayer/videoPlayerActivity;

    invoke-virtual {v6}, Lcom/konka/videoPlayer/videoPlayerActivity;->setTextUrl()V

    iget-object v6, p0, Lcom/konka/videoPlayer/videoPlayerActivity$playerDataReceiver;->this$0:Lcom/konka/videoPlayer/videoPlayerActivity;

    const/4 v7, 0x0

    invoke-static {v6, v7}, Lcom/konka/videoPlayer/videoPlayerActivity;->access$35(Lcom/konka/videoPlayer/videoPlayerActivity;Z)V

    iget-object v6, p0, Lcom/konka/videoPlayer/videoPlayerActivity$playerDataReceiver;->this$0:Lcom/konka/videoPlayer/videoPlayerActivity;

    # getter for: Lcom/konka/videoPlayer/videoPlayerActivity;->videoHandler:Landroid/os/Handler;
    invoke-static {v6}, Lcom/konka/videoPlayer/videoPlayerActivity;->access$36(Lcom/konka/videoPlayer/videoPlayerActivity;)Landroid/os/Handler;

    move-result-object v6

    const/4 v7, 0x5

    invoke-virtual {v6, v7}, Landroid/os/Handler;->sendEmptyMessage(I)Z

    :cond_1
    :goto_2
    return-void

    :cond_2
    const-string v7, "weikan"

    new-instance v8, Ljava/lang/StringBuilder;

    const-string v6, "palyer url = "

    invoke-direct {v8, v6}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    iget-object v6, p0, Lcom/konka/videoPlayer/videoPlayerActivity$playerDataReceiver;->this$0:Lcom/konka/videoPlayer/videoPlayerActivity;

    # getter for: Lcom/konka/videoPlayer/videoPlayerActivity;->urlList:Ljava/util/ArrayList;
    invoke-static {v6}, Lcom/konka/videoPlayer/videoPlayerActivity;->access$22(Lcom/konka/videoPlayer/videoPlayerActivity;)Ljava/util/ArrayList;

    move-result-object v6

    invoke-virtual {v6, v2}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v6

    check-cast v6, Ljava/lang/String;

    invoke-virtual {v8, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v6}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v6

    invoke-static {v7, v6}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    const-string v7, "weikan"

    new-instance v8, Ljava/lang/StringBuilder;

    const-string v6, "palyer duration = "

    invoke-direct {v8, v6}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    iget-object v6, p0, Lcom/konka/videoPlayer/videoPlayerActivity$playerDataReceiver;->this$0:Lcom/konka/videoPlayer/videoPlayerActivity;

    # getter for: Lcom/konka/videoPlayer/videoPlayerActivity;->durationList:Ljava/util/ArrayList;
    invoke-static {v6}, Lcom/konka/videoPlayer/videoPlayerActivity;->access$26(Lcom/konka/videoPlayer/videoPlayerActivity;)Ljava/util/ArrayList;

    move-result-object v6

    invoke-virtual {v6, v2}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v6

    check-cast v6, Ljava/lang/String;

    invoke-virtual {v8, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v6}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v6

    invoke-static {v7, v6}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    iget-object v7, p0, Lcom/konka/videoPlayer/videoPlayerActivity$playerDataReceiver;->this$0:Lcom/konka/videoPlayer/videoPlayerActivity;

    # getter for: Lcom/konka/videoPlayer/videoPlayerActivity;->mTotalTimeSegVideo:J
    invoke-static {v7}, Lcom/konka/videoPlayer/videoPlayerActivity;->access$27(Lcom/konka/videoPlayer/videoPlayerActivity;)J

    move-result-wide v8

    iget-object v6, p0, Lcom/konka/videoPlayer/videoPlayerActivity$playerDataReceiver;->this$0:Lcom/konka/videoPlayer/videoPlayerActivity;

    # getter for: Lcom/konka/videoPlayer/videoPlayerActivity;->durationList:Ljava/util/ArrayList;
    invoke-static {v6}, Lcom/konka/videoPlayer/videoPlayerActivity;->access$26(Lcom/konka/videoPlayer/videoPlayerActivity;)Ljava/util/ArrayList;

    move-result-object v6

    invoke-virtual {v6, v2}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v6

    check-cast v6, Ljava/lang/String;

    invoke-static {v6}, Ljava/lang/Long;->parseLong(Ljava/lang/String;)J

    move-result-wide v10

    add-long/2addr v8, v10

    invoke-static {v7, v8, v9}, Lcom/konka/videoPlayer/videoPlayerActivity;->access$28(Lcom/konka/videoPlayer/videoPlayerActivity;J)V

    add-int/lit8 v2, v2, 0x1

    goto/16 :goto_0

    :cond_3
    iget-object v6, p0, Lcom/konka/videoPlayer/videoPlayerActivity$playerDataReceiver;->this$0:Lcom/konka/videoPlayer/videoPlayerActivity;

    const-string v7, "videofile"

    invoke-virtual {v0, v7}, Landroid/os/Bundle;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v7

    invoke-static {v6, v7}, Lcom/konka/videoPlayer/videoPlayerActivity;->access$29(Lcom/konka/videoPlayer/videoPlayerActivity;Ljava/lang/String;)V

    goto/16 :goto_1

    :cond_4
    const-string v6, "videofile"

    invoke-virtual {v0, v6}, Landroid/os/Bundle;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v4

    const-string v6, "8085/"

    invoke-virtual {v4, v6}, Ljava/lang/String;->indexOf(Ljava/lang/String;)I

    move-result v6

    add-int/lit8 v6, v6, 0x5

    invoke-virtual {v4}, Ljava/lang/String;->length()I

    move-result v7

    invoke-virtual {v4, v6, v7}, Ljava/lang/String;->substring(II)Ljava/lang/String;

    move-result-object v5

    const/4 v6, 0x0

    const-string v7, "8085/"

    invoke-virtual {v4, v7}, Ljava/lang/String;->indexOf(Ljava/lang/String;)I

    move-result v7

    add-int/lit8 v7, v7, 0x5

    invoke-virtual {v4, v6, v7}, Ljava/lang/String;->substring(II)Ljava/lang/String;

    move-result-object v1

    invoke-static {v5}, Lcom/konka/mediaSharePlayer/encodeUrl;->encodeUrl(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v3

    iget-object v6, p0, Lcom/konka/videoPlayer/videoPlayerActivity$playerDataReceiver;->this$0:Lcom/konka/videoPlayer/videoPlayerActivity;

    new-instance v7, Ljava/lang/StringBuilder;

    invoke-static {v1}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v8

    invoke-direct {v7, v8}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v7, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    invoke-virtual {v7}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v7

    invoke-static {v6, v7}, Lcom/konka/videoPlayer/videoPlayerActivity;->access$29(Lcom/konka/videoPlayer/videoPlayerActivity;Ljava/lang/String;)V

    iget-object v6, p0, Lcom/konka/videoPlayer/videoPlayerActivity$playerDataReceiver;->this$0:Lcom/konka/videoPlayer/videoPlayerActivity;

    # getter for: Lcom/konka/videoPlayer/videoPlayerActivity;->buttonPause:Landroid/widget/Button;
    invoke-static {v6}, Lcom/konka/videoPlayer/videoPlayerActivity;->access$16(Lcom/konka/videoPlayer/videoPlayerActivity;)Landroid/widget/Button;

    move-result-object v6

    const/4 v7, 0x0

    invoke-virtual {v6, v7}, Landroid/widget/Button;->setVisibility(I)V

    iget-object v6, p0, Lcom/konka/videoPlayer/videoPlayerActivity$playerDataReceiver;->this$0:Lcom/konka/videoPlayer/videoPlayerActivity;

    # getter for: Lcom/konka/videoPlayer/videoPlayerActivity;->buttonPlay:Landroid/widget/Button;
    invoke-static {v6}, Lcom/konka/videoPlayer/videoPlayerActivity;->access$15(Lcom/konka/videoPlayer/videoPlayerActivity;)Landroid/widget/Button;

    move-result-object v6

    const/16 v7, 0x8

    invoke-virtual {v6, v7}, Landroid/widget/Button;->setVisibility(I)V

    goto/16 :goto_1

    :cond_5
    iget-object v6, p0, Lcom/konka/videoPlayer/videoPlayerActivity$playerDataReceiver;->this$0:Lcom/konka/videoPlayer/videoPlayerActivity;

    # getter for: Lcom/konka/videoPlayer/videoPlayerActivity;->sourcePath:Ljava/lang/String;
    invoke-static {v6}, Lcom/konka/videoPlayer/videoPlayerActivity;->access$8(Lcom/konka/videoPlayer/videoPlayerActivity;)Ljava/lang/String;

    move-result-object v6

    if-eqz v6, :cond_1

    iget-object v6, p0, Lcom/konka/videoPlayer/videoPlayerActivity$playerDataReceiver;->this$0:Lcom/konka/videoPlayer/videoPlayerActivity;

    iget-object v6, v6, Lcom/konka/videoPlayer/videoPlayerActivity;->classVideoSubtitle:Lcom/konka/videoPlayer/VideoPlayerSubTitle;

    iget-object v7, p0, Lcom/konka/videoPlayer/videoPlayerActivity$playerDataReceiver;->this$0:Lcom/konka/videoPlayer/videoPlayerActivity;

    # getter for: Lcom/konka/videoPlayer/videoPlayerActivity;->sourcePath:Ljava/lang/String;
    invoke-static {v7}, Lcom/konka/videoPlayer/videoPlayerActivity;->access$8(Lcom/konka/videoPlayer/videoPlayerActivity;)Ljava/lang/String;

    move-result-object v7

    invoke-virtual {v6, v7}, Lcom/konka/videoPlayer/VideoPlayerSubTitle;->setTextViewName(Ljava/lang/String;)V

    iget-object v6, p0, Lcom/konka/videoPlayer/videoPlayerActivity$playerDataReceiver;->this$0:Lcom/konka/videoPlayer/videoPlayerActivity;

    # getter for: Lcom/konka/videoPlayer/videoPlayerActivity;->videoHandler:Landroid/os/Handler;
    invoke-static {v6}, Lcom/konka/videoPlayer/videoPlayerActivity;->access$36(Lcom/konka/videoPlayer/videoPlayerActivity;)Landroid/os/Handler;

    move-result-object v6

    const/4 v7, 0x1

    invoke-virtual {v6, v7}, Landroid/os/Handler;->sendEmptyMessage(I)Z

    goto/16 :goto_2
.end method
