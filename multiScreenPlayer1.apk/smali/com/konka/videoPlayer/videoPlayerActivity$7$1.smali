.class Lcom/konka/videoPlayer/videoPlayerActivity$7$1;
.super Ljava/lang/Object;
.source "videoPlayerActivity.java"

# interfaces
.implements Landroid/content/DialogInterface$OnClickListener;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/konka/videoPlayer/videoPlayerActivity$7;->onError(Landroid/media/MediaPlayer;II)Z
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$1:Lcom/konka/videoPlayer/videoPlayerActivity$7;


# direct methods
.method constructor <init>(Lcom/konka/videoPlayer/videoPlayerActivity$7;)V
    .locals 0

    iput-object p1, p0, Lcom/konka/videoPlayer/videoPlayerActivity$7$1;->this$1:Lcom/konka/videoPlayer/videoPlayerActivity$7;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onClick(Landroid/content/DialogInterface;I)V
    .locals 8
    .param p1    # Landroid/content/DialogInterface;
    .param p2    # I

    const/4 v7, 0x0

    iget-object v0, p0, Lcom/konka/videoPlayer/videoPlayerActivity$7$1;->this$1:Lcom/konka/videoPlayer/videoPlayerActivity$7;

    # getter for: Lcom/konka/videoPlayer/videoPlayerActivity$7;->this$0:Lcom/konka/videoPlayer/videoPlayerActivity;
    invoke-static {v0}, Lcom/konka/videoPlayer/videoPlayerActivity$7;->access$0(Lcom/konka/videoPlayer/videoPlayerActivity$7;)Lcom/konka/videoPlayer/videoPlayerActivity;

    move-result-object v0

    # getter for: Lcom/konka/videoPlayer/videoPlayerActivity;->mAlterDialog:Landroid/app/AlertDialog;
    invoke-static {v0}, Lcom/konka/videoPlayer/videoPlayerActivity;->access$17(Lcom/konka/videoPlayer/videoPlayerActivity;)Landroid/app/AlertDialog;

    move-result-object v0

    invoke-virtual {v0}, Landroid/app/AlertDialog;->dismiss()V

    iget-object v0, p0, Lcom/konka/videoPlayer/videoPlayerActivity$7$1;->this$1:Lcom/konka/videoPlayer/videoPlayerActivity$7;

    # getter for: Lcom/konka/videoPlayer/videoPlayerActivity$7;->this$0:Lcom/konka/videoPlayer/videoPlayerActivity;
    invoke-static {v0}, Lcom/konka/videoPlayer/videoPlayerActivity$7;->access$0(Lcom/konka/videoPlayer/videoPlayerActivity$7;)Lcom/konka/videoPlayer/videoPlayerActivity;

    move-result-object v0

    invoke-static {v0, v7}, Lcom/konka/videoPlayer/videoPlayerActivity;->access$46(Lcom/konka/videoPlayer/videoPlayerActivity;I)V

    iget-object v0, p0, Lcom/konka/videoPlayer/videoPlayerActivity$7$1;->this$1:Lcom/konka/videoPlayer/videoPlayerActivity$7;

    # getter for: Lcom/konka/videoPlayer/videoPlayerActivity$7;->this$0:Lcom/konka/videoPlayer/videoPlayerActivity;
    invoke-static {v0}, Lcom/konka/videoPlayer/videoPlayerActivity$7;->access$0(Lcom/konka/videoPlayer/videoPlayerActivity$7;)Lcom/konka/videoPlayer/videoPlayerActivity;

    move-result-object v0

    # getter for: Lcom/konka/videoPlayer/videoPlayerActivity;->mediaPlayerTag:Ljava/lang/String;
    invoke-static {v0}, Lcom/konka/videoPlayer/videoPlayerActivity;->access$49(Lcom/konka/videoPlayer/videoPlayerActivity;)Ljava/lang/String;

    move-result-object v0

    const-string v1, "have no next file!"

    invoke-static {v0, v1}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    iget-object v0, p0, Lcom/konka/videoPlayer/videoPlayerActivity$7$1;->this$1:Lcom/konka/videoPlayer/videoPlayerActivity$7;

    # getter for: Lcom/konka/videoPlayer/videoPlayerActivity$7;->this$0:Lcom/konka/videoPlayer/videoPlayerActivity;
    invoke-static {v0}, Lcom/konka/videoPlayer/videoPlayerActivity$7;->access$0(Lcom/konka/videoPlayer/videoPlayerActivity$7;)Lcom/konka/videoPlayer/videoPlayerActivity;

    move-result-object v0

    iget-object v1, p0, Lcom/konka/videoPlayer/videoPlayerActivity$7$1;->this$1:Lcom/konka/videoPlayer/videoPlayerActivity$7;

    # getter for: Lcom/konka/videoPlayer/videoPlayerActivity$7;->this$0:Lcom/konka/videoPlayer/videoPlayerActivity;
    invoke-static {v1}, Lcom/konka/videoPlayer/videoPlayerActivity$7;->access$0(Lcom/konka/videoPlayer/videoPlayerActivity$7;)Lcom/konka/videoPlayer/videoPlayerActivity;

    move-result-object v1

    sget-object v2, Lcom/konka/mediaSharePlayer/PlayerState;->PLAY_STATUS_EXIT:Lcom/konka/mediaSharePlayer/PlayerState;

    invoke-virtual {v2}, Lcom/konka/mediaSharePlayer/PlayerState;->ordinal()I

    move-result v2

    iget-object v3, p0, Lcom/konka/videoPlayer/videoPlayerActivity$7$1;->this$1:Lcom/konka/videoPlayer/videoPlayerActivity$7;

    # getter for: Lcom/konka/videoPlayer/videoPlayerActivity$7;->this$0:Lcom/konka/videoPlayer/videoPlayerActivity;
    invoke-static {v3}, Lcom/konka/videoPlayer/videoPlayerActivity$7;->access$0(Lcom/konka/videoPlayer/videoPlayerActivity$7;)Lcom/konka/videoPlayer/videoPlayerActivity;

    move-result-object v3

    # getter for: Lcom/konka/videoPlayer/videoPlayerActivity;->mVideoView:Lcom/konka/videoPlayer/VideoView;
    invoke-static {v3}, Lcom/konka/videoPlayer/videoPlayerActivity;->access$7(Lcom/konka/videoPlayer/videoPlayerActivity;)Lcom/konka/videoPlayer/VideoView;

    move-result-object v3

    invoke-virtual {v3}, Lcom/konka/videoPlayer/VideoView;->getCurrentPosition()I

    move-result v3

    iget-object v4, p0, Lcom/konka/videoPlayer/videoPlayerActivity$7$1;->this$1:Lcom/konka/videoPlayer/videoPlayerActivity$7;

    # getter for: Lcom/konka/videoPlayer/videoPlayerActivity$7;->this$0:Lcom/konka/videoPlayer/videoPlayerActivity;
    invoke-static {v4}, Lcom/konka/videoPlayer/videoPlayerActivity$7;->access$0(Lcom/konka/videoPlayer/videoPlayerActivity$7;)Lcom/konka/videoPlayer/videoPlayerActivity;

    move-result-object v4

    # getter for: Lcom/konka/videoPlayer/videoPlayerActivity;->mVideoView:Lcom/konka/videoPlayer/VideoView;
    invoke-static {v4}, Lcom/konka/videoPlayer/videoPlayerActivity;->access$7(Lcom/konka/videoPlayer/videoPlayerActivity;)Lcom/konka/videoPlayer/VideoView;

    move-result-object v4

    invoke-virtual {v4}, Lcom/konka/videoPlayer/VideoView;->getDuration()I

    move-result v4

    new-instance v5, Ljava/lang/StringBuilder;

    iget-object v6, p0, Lcom/konka/videoPlayer/videoPlayerActivity$7$1;->this$1:Lcom/konka/videoPlayer/videoPlayerActivity$7;

    # getter for: Lcom/konka/videoPlayer/videoPlayerActivity$7;->this$0:Lcom/konka/videoPlayer/videoPlayerActivity;
    invoke-static {v6}, Lcom/konka/videoPlayer/videoPlayerActivity$7;->access$0(Lcom/konka/videoPlayer/videoPlayerActivity$7;)Lcom/konka/videoPlayer/videoPlayerActivity;

    move-result-object v6

    # getter for: Lcom/konka/videoPlayer/videoPlayerActivity;->sourcePath:Ljava/lang/String;
    invoke-static {v6}, Lcom/konka/videoPlayer/videoPlayerActivity;->access$8(Lcom/konka/videoPlayer/videoPlayerActivity;)Ljava/lang/String;

    move-result-object v6

    invoke-static {v6}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v6

    invoke-direct {v5, v6}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    const-string v6, "&mv"

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    invoke-virtual/range {v0 .. v5}, Lcom/konka/videoPlayer/videoPlayerActivity;->sendPlayerStateToPlayerService(Landroid/content/Context;IIILjava/lang/String;)V

    iget-object v0, p0, Lcom/konka/videoPlayer/videoPlayerActivity$7$1;->this$1:Lcom/konka/videoPlayer/videoPlayerActivity$7;

    # getter for: Lcom/konka/videoPlayer/videoPlayerActivity$7;->this$0:Lcom/konka/videoPlayer/videoPlayerActivity;
    invoke-static {v0}, Lcom/konka/videoPlayer/videoPlayerActivity$7;->access$0(Lcom/konka/videoPlayer/videoPlayerActivity$7;)Lcom/konka/videoPlayer/videoPlayerActivity;

    move-result-object v0

    invoke-static {v0, v7}, Lcom/konka/videoPlayer/videoPlayerActivity;->access$11(Lcom/konka/videoPlayer/videoPlayerActivity;Z)V

    iget-object v0, p0, Lcom/konka/videoPlayer/videoPlayerActivity$7$1;->this$1:Lcom/konka/videoPlayer/videoPlayerActivity$7;

    # getter for: Lcom/konka/videoPlayer/videoPlayerActivity$7;->this$0:Lcom/konka/videoPlayer/videoPlayerActivity;
    invoke-static {v0}, Lcom/konka/videoPlayer/videoPlayerActivity$7;->access$0(Lcom/konka/videoPlayer/videoPlayerActivity$7;)Lcom/konka/videoPlayer/videoPlayerActivity;

    move-result-object v0

    # getter for: Lcom/konka/videoPlayer/videoPlayerActivity;->mVideoView:Lcom/konka/videoPlayer/VideoView;
    invoke-static {v0}, Lcom/konka/videoPlayer/videoPlayerActivity;->access$7(Lcom/konka/videoPlayer/videoPlayerActivity;)Lcom/konka/videoPlayer/VideoView;

    move-result-object v0

    invoke-virtual {v0}, Lcom/konka/videoPlayer/VideoView;->stopPlayback()V

    iget-object v0, p0, Lcom/konka/videoPlayer/videoPlayerActivity$7$1;->this$1:Lcom/konka/videoPlayer/videoPlayerActivity$7;

    # getter for: Lcom/konka/videoPlayer/videoPlayerActivity$7;->this$0:Lcom/konka/videoPlayer/videoPlayerActivity;
    invoke-static {v0}, Lcom/konka/videoPlayer/videoPlayerActivity$7;->access$0(Lcom/konka/videoPlayer/videoPlayerActivity$7;)Lcom/konka/videoPlayer/videoPlayerActivity;

    move-result-object v0

    invoke-static {v0, v7, v7, v7}, Lcom/konka/mediaSharePlayer/sendPlayerState;->MediaPlayerActionSendBroadcastIsCreat(Landroid/content/Context;III)V

    invoke-static {v7}, Ljava/lang/System;->exit(I)V

    return-void
.end method
