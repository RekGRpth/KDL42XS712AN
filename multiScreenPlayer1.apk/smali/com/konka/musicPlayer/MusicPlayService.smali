.class public Lcom/konka/musicPlayer/MusicPlayService;
.super Landroid/app/Service;
.source "MusicPlayService.java"

# interfaces
.implements Ljava/lang/Runnable;


# static fields
.field private static activity:Lcom/konka/musicPlayer/musicPlayerActivity; = null

.field private static final musicplayService:Ljava/lang/String; = "musicplayService..."


# instance fields
.field private countTime:I

.field private currentTime:I

.field fileUrl:[B

.field private handler:Landroid/os/Handler;

.field private index:I

.field private info:Lcom/konka/musicPlayer/MusicInfo;

.field private isPause:Z

.field private isPlay:Z

.field private isRepeat:Z

.field private isRun:Z

.field isSend:Z

.field private lrcHandler:Landroid/os/Handler;

.field private lrcRead:Lcom/konka/musicPlayer/LrcRead;

.field private lyricList:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Lcom/konka/musicPlayer/LyricContent;",
            ">;"
        }
    .end annotation
.end field

.field mRunnable:Ljava/lang/Runnable;

.field public musicPath:Ljava/lang/String;

.field private musichandler:Landroid/os/Handler;

.field private player:Landroid/media/MediaPlayer;

.field playerOnPrepared:Landroid/media/MediaPlayer$OnPreparedListener;

.field relayTimer:I


# direct methods
.method static constructor <clinit>()V
    .locals 1

    const/4 v0, 0x0

    sput-object v0, Lcom/konka/musicPlayer/MusicPlayService;->activity:Lcom/konka/musicPlayer/musicPlayerActivity;

    return-void
.end method

.method public constructor <init>()V
    .locals 2

    const/4 v0, 0x0

    const/4 v1, 0x0

    invoke-direct {p0}, Landroid/app/Service;-><init>()V

    iput-object v0, p0, Lcom/konka/musicPlayer/MusicPlayService;->player:Landroid/media/MediaPlayer;

    iput-boolean v1, p0, Lcom/konka/musicPlayer/MusicPlayService;->isPlay:Z

    iput-boolean v1, p0, Lcom/konka/musicPlayer/MusicPlayService;->isPause:Z

    iput-object v0, p0, Lcom/konka/musicPlayer/MusicPlayService;->info:Lcom/konka/musicPlayer/MusicInfo;

    iput-boolean v1, p0, Lcom/konka/musicPlayer/MusicPlayService;->isRepeat:Z

    iput-object v0, p0, Lcom/konka/musicPlayer/MusicPlayService;->lrcRead:Lcom/konka/musicPlayer/LrcRead;

    iput-object v0, p0, Lcom/konka/musicPlayer/MusicPlayService;->lyricList:Ljava/util/List;

    iput v1, p0, Lcom/konka/musicPlayer/MusicPlayService;->index:I

    iput v1, p0, Lcom/konka/musicPlayer/MusicPlayService;->currentTime:I

    iput v1, p0, Lcom/konka/musicPlayer/MusicPlayService;->countTime:I

    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/konka/musicPlayer/MusicPlayService;->isRun:Z

    const-string v0, ""

    iput-object v0, p0, Lcom/konka/musicPlayer/MusicPlayService;->musicPath:Ljava/lang/String;

    const/16 v0, 0x190

    new-array v0, v0, [B

    iput-object v0, p0, Lcom/konka/musicPlayer/MusicPlayService;->fileUrl:[B

    iput v1, p0, Lcom/konka/musicPlayer/MusicPlayService;->relayTimer:I

    iput-boolean v1, p0, Lcom/konka/musicPlayer/MusicPlayService;->isSend:Z

    new-instance v0, Lcom/konka/musicPlayer/MusicPlayService$1;

    invoke-direct {v0, p0}, Lcom/konka/musicPlayer/MusicPlayService$1;-><init>(Lcom/konka/musicPlayer/MusicPlayService;)V

    iput-object v0, p0, Lcom/konka/musicPlayer/MusicPlayService;->handler:Landroid/os/Handler;

    new-instance v0, Lcom/konka/musicPlayer/MusicPlayService$2;

    invoke-direct {v0, p0}, Lcom/konka/musicPlayer/MusicPlayService$2;-><init>(Lcom/konka/musicPlayer/MusicPlayService;)V

    iput-object v0, p0, Lcom/konka/musicPlayer/MusicPlayService;->musichandler:Landroid/os/Handler;

    new-instance v0, Lcom/konka/musicPlayer/MusicPlayService$3;

    invoke-direct {v0, p0}, Lcom/konka/musicPlayer/MusicPlayService$3;-><init>(Lcom/konka/musicPlayer/MusicPlayService;)V

    iput-object v0, p0, Lcom/konka/musicPlayer/MusicPlayService;->playerOnPrepared:Landroid/media/MediaPlayer$OnPreparedListener;

    new-instance v0, Landroid/os/Handler;

    invoke-direct {v0}, Landroid/os/Handler;-><init>()V

    iput-object v0, p0, Lcom/konka/musicPlayer/MusicPlayService;->lrcHandler:Landroid/os/Handler;

    new-instance v0, Lcom/konka/musicPlayer/MusicPlayService$4;

    invoke-direct {v0, p0}, Lcom/konka/musicPlayer/MusicPlayService$4;-><init>(Lcom/konka/musicPlayer/MusicPlayService;)V

    iput-object v0, p0, Lcom/konka/musicPlayer/MusicPlayService;->mRunnable:Ljava/lang/Runnable;

    return-void
.end method

.method static synthetic access$0()Lcom/konka/musicPlayer/musicPlayerActivity;
    .locals 1

    sget-object v0, Lcom/konka/musicPlayer/MusicPlayService;->activity:Lcom/konka/musicPlayer/musicPlayerActivity;

    return-object v0
.end method

.method static synthetic access$1(Lcom/konka/musicPlayer/MusicPlayService;)Landroid/media/MediaPlayer;
    .locals 1

    iget-object v0, p0, Lcom/konka/musicPlayer/MusicPlayService;->player:Landroid/media/MediaPlayer;

    return-object v0
.end method

.method static synthetic access$2(Lcom/konka/musicPlayer/MusicPlayService;)Lcom/konka/musicPlayer/MusicInfo;
    .locals 1

    iget-object v0, p0, Lcom/konka/musicPlayer/MusicPlayService;->info:Lcom/konka/musicPlayer/MusicInfo;

    return-object v0
.end method

.method static synthetic access$3(Lcom/konka/musicPlayer/MusicPlayService;)I
    .locals 1

    iget v0, p0, Lcom/konka/musicPlayer/MusicPlayService;->currentTime:I

    return v0
.end method

.method static synthetic access$4(Lcom/konka/musicPlayer/MusicPlayService;)Landroid/os/Handler;
    .locals 1

    iget-object v0, p0, Lcom/konka/musicPlayer/MusicPlayService;->lrcHandler:Landroid/os/Handler;

    return-object v0
.end method

.method public static addActivity(Lcom/konka/musicPlayer/musicPlayerActivity;)V
    .locals 3
    .param p0    # Lcom/konka/musicPlayer/musicPlayerActivity;

    sput-object p0, Lcom/konka/musicPlayer/MusicPlayService;->activity:Lcom/konka/musicPlayer/musicPlayerActivity;

    const-string v0, "musicplayService..."

    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "addActivity----------:"

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    sget-object v2, Lcom/konka/musicPlayer/MusicPlayService;->activity:Lcom/konka/musicPlayer/musicPlayerActivity;

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    return-void
.end method

.method private initPlay()V
    .locals 3

    const/4 v2, 0x0

    const-string v0, "musicplayService..."

    const-string v1, "initPlay()"

    invoke-static {v0, v1}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    iput v2, p0, Lcom/konka/musicPlayer/MusicPlayService;->index:I

    iput-boolean v2, p0, Lcom/konka/musicPlayer/MusicPlayService;->isPlay:Z

    iput-boolean v2, p0, Lcom/konka/musicPlayer/MusicPlayService;->isPause:Z

    return-void
.end method

.method private playMusic(Lcom/konka/musicPlayer/MusicInfo;)V
    .locals 7
    .param p1    # Lcom/konka/musicPlayer/MusicInfo;

    const/4 v4, 0x1

    const/4 v3, 0x0

    const-string v0, "musicplayService..."

    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "initPlay()"

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    iget-boolean v2, p0, Lcom/konka/musicPlayer/MusicPlayService;->isPlay:Z

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, "----"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, "ispause: "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget-boolean v2, p0, Lcom/konka/musicPlayer/MusicPlayService;->isPause:Z

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, "----"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, "info: "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {p1}, Lcom/konka/musicPlayer/MusicInfo;->getData()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    sget-object v0, Lcom/konka/musicPlayer/MusicPlayService;->activity:Lcom/konka/musicPlayer/musicPlayerActivity;

    if-eqz v0, :cond_0

    sget-object v0, Lcom/konka/musicPlayer/MusicPlayService;->activity:Lcom/konka/musicPlayer/musicPlayerActivity;

    invoke-virtual {v0}, Lcom/konka/musicPlayer/musicPlayerActivity;->encodeMusicUri()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/konka/musicPlayer/MusicPlayService;->musicPath:Ljava/lang/String;

    :cond_0
    iget-boolean v0, p0, Lcom/konka/musicPlayer/MusicPlayService;->isPlay:Z

    if-nez v0, :cond_3

    iget-boolean v0, p0, Lcom/konka/musicPlayer/MusicPlayService;->isPause:Z

    if-nez v0, :cond_2

    iget-object v0, p0, Lcom/konka/musicPlayer/MusicPlayService;->musicPath:Ljava/lang/String;

    if-nez v0, :cond_1

    iget-object v0, p0, Lcom/konka/musicPlayer/MusicPlayService;->musichandler:Landroid/os/Handler;

    invoke-virtual {v0, v3}, Landroid/os/Handler;->sendEmptyMessage(I)Z

    :goto_0
    iput-boolean v4, p0, Lcom/konka/musicPlayer/MusicPlayService;->isPlay:Z

    iput-boolean v3, p0, Lcom/konka/musicPlayer/MusicPlayService;->isPause:Z

    :goto_1
    return-void

    :cond_1
    const-string v0, "noob"

    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "play start--------------"

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    iget v2, p0, Lcom/konka/musicPlayer/MusicPlayService;->currentTime:I

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    iget-object v0, p0, Lcom/konka/musicPlayer/MusicPlayService;->musichandler:Landroid/os/Handler;

    invoke-virtual {v0, v4}, Landroid/os/Handler;->sendEmptyMessage(I)Z

    goto :goto_0

    :cond_2
    iput-boolean v4, p0, Lcom/konka/musicPlayer/MusicPlayService;->isPlay:Z

    iput-boolean v3, p0, Lcom/konka/musicPlayer/MusicPlayService;->isPause:Z

    iget-object v0, p0, Lcom/konka/musicPlayer/MusicPlayService;->player:Landroid/media/MediaPlayer;

    invoke-virtual {v0}, Landroid/media/MediaPlayer;->start()V

    sget-object v0, Lcom/konka/musicPlayer/MusicPlayService;->activity:Lcom/konka/musicPlayer/musicPlayerActivity;

    sget-object v1, Lcom/konka/musicPlayer/MusicPlayService;->activity:Lcom/konka/musicPlayer/musicPlayerActivity;

    sget-object v2, Lcom/konka/mediaSharePlayer/PlayerState;->PLAY_STATUS_PLAY:Lcom/konka/mediaSharePlayer/PlayerState;

    invoke-virtual {v2}, Lcom/konka/mediaSharePlayer/PlayerState;->ordinal()I

    move-result v2

    iget-object v3, p0, Lcom/konka/musicPlayer/MusicPlayService;->player:Landroid/media/MediaPlayer;

    invoke-virtual {v3}, Landroid/media/MediaPlayer;->getCurrentPosition()I

    move-result v3

    iget-object v4, p0, Lcom/konka/musicPlayer/MusicPlayService;->player:Landroid/media/MediaPlayer;

    invoke-virtual {v4}, Landroid/media/MediaPlayer;->getDuration()I

    move-result v4

    new-instance v5, Ljava/lang/StringBuilder;

    invoke-virtual {p1}, Lcom/konka/musicPlayer/MusicInfo;->getData()Ljava/lang/String;

    move-result-object v6

    invoke-static {v6}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v6

    invoke-direct {v5, v6}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    const-string v6, "&ma"

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    invoke-virtual/range {v0 .. v5}, Lcom/konka/musicPlayer/musicPlayerActivity;->sendPlayerStateToPlayerService(Landroid/content/Context;IIILjava/lang/String;)V

    goto :goto_1

    :cond_3
    iput-boolean v3, p0, Lcom/konka/musicPlayer/MusicPlayService;->isPlay:Z

    iput-boolean v4, p0, Lcom/konka/musicPlayer/MusicPlayService;->isPause:Z

    iget-object v0, p0, Lcom/konka/musicPlayer/MusicPlayService;->player:Landroid/media/MediaPlayer;

    invoke-virtual {v0}, Landroid/media/MediaPlayer;->pause()V

    sget-object v0, Lcom/konka/musicPlayer/MusicPlayService;->activity:Lcom/konka/musicPlayer/musicPlayerActivity;

    sget-object v1, Lcom/konka/musicPlayer/MusicPlayService;->activity:Lcom/konka/musicPlayer/musicPlayerActivity;

    sget-object v2, Lcom/konka/mediaSharePlayer/PlayerState;->PLAY_STATUS_PAUSE:Lcom/konka/mediaSharePlayer/PlayerState;

    invoke-virtual {v2}, Lcom/konka/mediaSharePlayer/PlayerState;->ordinal()I

    move-result v2

    iget-object v3, p0, Lcom/konka/musicPlayer/MusicPlayService;->player:Landroid/media/MediaPlayer;

    invoke-virtual {v3}, Landroid/media/MediaPlayer;->getCurrentPosition()I

    move-result v3

    iget-object v4, p0, Lcom/konka/musicPlayer/MusicPlayService;->player:Landroid/media/MediaPlayer;

    invoke-virtual {v4}, Landroid/media/MediaPlayer;->getDuration()I

    move-result v4

    new-instance v5, Ljava/lang/StringBuilder;

    invoke-virtual {p1}, Lcom/konka/musicPlayer/MusicInfo;->getData()Ljava/lang/String;

    move-result-object v6

    invoke-static {v6}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v6

    invoke-direct {v5, v6}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    const-string v6, "&ma"

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    invoke-virtual/range {v0 .. v5}, Lcom/konka/musicPlayer/musicPlayerActivity;->sendPlayerStateToPlayerService(Landroid/content/Context;IIILjava/lang/String;)V

    goto/16 :goto_1
.end method

.method private sendLrcBroadcast(Ljava/lang/String;)V
    .locals 2
    .param p1    # Ljava/lang/String;

    new-instance v0, Landroid/content/Intent;

    invoke-direct {v0}, Landroid/content/Intent;-><init>()V

    const-string v1, "konka.mm.lrc.action"

    invoke-virtual {v0, v1}, Landroid/content/Intent;->setAction(Ljava/lang/String;)Landroid/content/Intent;

    const-string v1, "message"

    invoke-virtual {v0, v1, p1}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    invoke-virtual {p0, v0}, Lcom/konka/musicPlayer/MusicPlayService;->sendBroadcast(Landroid/content/Intent;)V

    return-void
.end method


# virtual methods
.method public Index()I
    .locals 5

    const-string v2, "noob"

    new-instance v3, Ljava/lang/StringBuilder;

    const-string v4, "index current time = "

    invoke-direct {v3, v4}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    iget v4, p0, Lcom/konka/musicPlayer/MusicPlayService;->currentTime:I

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v2, v3}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    :try_start_0
    iget-object v2, p0, Lcom/konka/musicPlayer/MusicPlayService;->player:Landroid/media/MediaPlayer;

    if-eqz v2, :cond_0

    iget-object v2, p0, Lcom/konka/musicPlayer/MusicPlayService;->player:Landroid/media/MediaPlayer;

    invoke-virtual {v2}, Landroid/media/MediaPlayer;->isPlaying()Z

    move-result v2

    if-eqz v2, :cond_0

    const-string v2, "noob"

    new-instance v3, Ljava/lang/StringBuilder;

    const-string v4, "index current time1 = "

    invoke-direct {v3, v4}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    iget v4, p0, Lcom/konka/musicPlayer/MusicPlayService;->currentTime:I

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v2, v3}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    iget-object v2, p0, Lcom/konka/musicPlayer/MusicPlayService;->player:Landroid/media/MediaPlayer;

    invoke-virtual {v2}, Landroid/media/MediaPlayer;->getCurrentPosition()I

    move-result v2

    iput v2, p0, Lcom/konka/musicPlayer/MusicPlayService;->currentTime:I

    iget-object v2, p0, Lcom/konka/musicPlayer/MusicPlayService;->player:Landroid/media/MediaPlayer;

    invoke-virtual {v2}, Landroid/media/MediaPlayer;->getDuration()I

    move-result v2

    iput v2, p0, Lcom/konka/musicPlayer/MusicPlayService;->countTime:I

    :cond_0
    iget v2, p0, Lcom/konka/musicPlayer/MusicPlayService;->currentTime:I

    iget v3, p0, Lcom/konka/musicPlayer/MusicPlayService;->countTime:I

    if-ge v2, v3, :cond_1

    const-string v2, "noob"

    new-instance v3, Ljava/lang/StringBuilder;

    const-string v4, "index current time2 = "

    invoke-direct {v3, v4}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    iget v4, p0, Lcom/konka/musicPlayer/MusicPlayService;->currentTime:I

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v2, v3}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    const/4 v1, 0x0

    :goto_0
    iget-object v2, p0, Lcom/konka/musicPlayer/MusicPlayService;->lyricList:Ljava/util/List;

    invoke-interface {v2}, Ljava/util/List;->size()I
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    move-result v2

    if-lt v1, v2, :cond_2

    :cond_1
    :goto_1
    const-string v2, "noob"

    new-instance v3, Ljava/lang/StringBuilder;

    const-string v4, "index current time3 = "

    invoke-direct {v3, v4}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    iget v4, p0, Lcom/konka/musicPlayer/MusicPlayService;->currentTime:I

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v2, v3}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    iget v2, p0, Lcom/konka/musicPlayer/MusicPlayService;->index:I

    return v2

    :cond_2
    :try_start_1
    iget-object v2, p0, Lcom/konka/musicPlayer/MusicPlayService;->lyricList:Ljava/util/List;

    invoke-interface {v2}, Ljava/util/List;->size()I

    move-result v2

    add-int/lit8 v2, v2, -0x1

    if-ge v1, v2, :cond_4

    iget v3, p0, Lcom/konka/musicPlayer/MusicPlayService;->currentTime:I

    iget-object v2, p0, Lcom/konka/musicPlayer/MusicPlayService;->lyricList:Ljava/util/List;

    invoke-interface {v2, v1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lcom/konka/musicPlayer/LyricContent;

    invoke-virtual {v2}, Lcom/konka/musicPlayer/LyricContent;->getLyricTime()I

    move-result v2

    if-ge v3, v2, :cond_3

    if-nez v1, :cond_3

    iput v1, p0, Lcom/konka/musicPlayer/MusicPlayService;->index:I

    :cond_3
    iget v3, p0, Lcom/konka/musicPlayer/MusicPlayService;->currentTime:I

    iget-object v2, p0, Lcom/konka/musicPlayer/MusicPlayService;->lyricList:Ljava/util/List;

    invoke-interface {v2, v1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lcom/konka/musicPlayer/LyricContent;

    invoke-virtual {v2}, Lcom/konka/musicPlayer/LyricContent;->getLyricTime()I

    move-result v2

    if-le v3, v2, :cond_4

    iget v3, p0, Lcom/konka/musicPlayer/MusicPlayService;->currentTime:I

    iget-object v2, p0, Lcom/konka/musicPlayer/MusicPlayService;->lyricList:Ljava/util/List;

    add-int/lit8 v4, v1, 0x1

    invoke-interface {v2, v4}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lcom/konka/musicPlayer/LyricContent;

    invoke-virtual {v2}, Lcom/konka/musicPlayer/LyricContent;->getLyricTime()I

    move-result v2

    if-ge v3, v2, :cond_4

    iput v1, p0, Lcom/konka/musicPlayer/MusicPlayService;->index:I

    :cond_4
    iget-object v2, p0, Lcom/konka/musicPlayer/MusicPlayService;->lyricList:Ljava/util/List;

    invoke-interface {v2}, Ljava/util/List;->size()I

    move-result v2

    add-int/lit8 v2, v2, -0x1

    if-ne v1, v2, :cond_5

    iget v3, p0, Lcom/konka/musicPlayer/MusicPlayService;->currentTime:I

    iget-object v2, p0, Lcom/konka/musicPlayer/MusicPlayService;->lyricList:Ljava/util/List;

    invoke-interface {v2, v1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lcom/konka/musicPlayer/LyricContent;

    invoke-virtual {v2}, Lcom/konka/musicPlayer/LyricContent;->getLyricTime()I

    move-result v2

    if-le v3, v2, :cond_5

    iput v1, p0, Lcom/konka/musicPlayer/MusicPlayService;->index:I
    :try_end_1
    .catch Ljava/lang/Exception; {:try_start_1 .. :try_end_1} :catch_0

    :cond_5
    add-int/lit8 v1, v1, 0x1

    goto/16 :goto_0

    :catch_0
    move-exception v0

    invoke-virtual {v0}, Ljava/lang/Exception;->printStackTrace()V

    goto :goto_1
.end method

.method public onBind(Landroid/content/Intent;)Landroid/os/IBinder;
    .locals 1
    .param p1    # Landroid/content/Intent;

    const/4 v0, 0x0

    return-object v0
.end method

.method public onCreate()V
    .locals 2

    const-string v0, "musicplayService..."

    const-string v1, "onCreate()"

    invoke-static {v0, v1}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/konka/musicPlayer/MusicPlayService;->isRun:Z

    new-instance v0, Landroid/media/MediaPlayer;

    invoke-direct {v0}, Landroid/media/MediaPlayer;-><init>()V

    iput-object v0, p0, Lcom/konka/musicPlayer/MusicPlayService;->player:Landroid/media/MediaPlayer;

    new-instance v0, Ljava/lang/Thread;

    invoke-direct {v0, p0}, Ljava/lang/Thread;-><init>(Ljava/lang/Runnable;)V

    invoke-virtual {v0}, Ljava/lang/Thread;->start()V

    invoke-super {p0}, Landroid/app/Service;->onCreate()V

    return-void
.end method

.method public onDestroy()V
    .locals 2

    const-string v0, "musicplayService..."

    const-string v1, "onDestroy()"

    invoke-static {v0, v1}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    invoke-super {p0}, Landroid/app/Service;->onDestroy()V

    iget-object v0, p0, Lcom/konka/musicPlayer/MusicPlayService;->player:Landroid/media/MediaPlayer;

    invoke-virtual {v0}, Landroid/media/MediaPlayer;->release()V

    const/4 v0, 0x0

    iput-object v0, p0, Lcom/konka/musicPlayer/MusicPlayService;->player:Landroid/media/MediaPlayer;

    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/konka/musicPlayer/MusicPlayService;->isRun:Z

    invoke-virtual {p0}, Lcom/konka/musicPlayer/MusicPlayService;->stopSelf()V

    return-void
.end method

.method public onStartCommand(Landroid/content/Intent;II)I
    .locals 6
    .param p1    # Landroid/content/Intent;
    .param p2    # I
    .param p3    # I

    const/4 v5, 0x0

    const-string v2, "musicplayService..."

    new-instance v3, Ljava/lang/StringBuilder;

    const-string v4, "onStartCommand()"

    invoke-direct {v3, v4}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v3, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v2, v3}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    const-string v2, "CurrentTime"

    invoke-virtual {p1, v2, v5}, Landroid/content/Intent;->getIntExtra(Ljava/lang/String;I)I

    move-result v2

    iput v2, p0, Lcom/konka/musicPlayer/MusicPlayService;->currentTime:I

    const-string v2, "MSG"

    invoke-virtual {p1, v2, v5}, Landroid/content/Intent;->getIntExtra(Ljava/lang/String;I)I

    move-result v0

    const-string v2, "noob"

    new-instance v3, Ljava/lang/StringBuilder;

    const-string v4, "this is service : current time = "

    invoke-direct {v3, v4}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    iget v4, p0, Lcom/konka/musicPlayer/MusicPlayService;->currentTime:I

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v3

    const-string v4, " msgflag = "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3, v0}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v2, v3}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    packed-switch v0, :pswitch_data_0

    :cond_0
    :goto_0
    invoke-super {p0, p1, p2, p3}, Landroid/app/Service;->onStartCommand(Landroid/content/Intent;II)I

    move-result v2

    return v2

    :pswitch_0
    iget-object v2, p0, Lcom/konka/musicPlayer/MusicPlayService;->player:Landroid/media/MediaPlayer;

    if-eqz v2, :cond_0

    iget-object v2, p0, Lcom/konka/musicPlayer/MusicPlayService;->player:Landroid/media/MediaPlayer;

    invoke-virtual {v2}, Landroid/media/MediaPlayer;->isLooping()Z

    move-result v2

    if-nez v2, :cond_1

    const/4 v2, 0x1

    iput-boolean v2, p0, Lcom/konka/musicPlayer/MusicPlayService;->isRepeat:Z

    iget-object v2, p0, Lcom/konka/musicPlayer/MusicPlayService;->player:Landroid/media/MediaPlayer;

    iget-boolean v3, p0, Lcom/konka/musicPlayer/MusicPlayService;->isRepeat:Z

    invoke-virtual {v2, v3}, Landroid/media/MediaPlayer;->setLooping(Z)V

    goto :goto_0

    :cond_1
    iput-boolean v5, p0, Lcom/konka/musicPlayer/MusicPlayService;->isRepeat:Z

    iget-object v2, p0, Lcom/konka/musicPlayer/MusicPlayService;->player:Landroid/media/MediaPlayer;

    iget-boolean v3, p0, Lcom/konka/musicPlayer/MusicPlayService;->isRepeat:Z

    invoke-virtual {v2, v3}, Landroid/media/MediaPlayer;->setLooping(Z)V

    goto :goto_0

    :pswitch_1
    const-string v2, "info"

    invoke-virtual {p1, v2}, Landroid/content/Intent;->getSerializableExtra(Ljava/lang/String;)Ljava/io/Serializable;

    move-result-object v2

    check-cast v2, Lcom/konka/musicPlayer/MusicInfo;

    iput-object v2, p0, Lcom/konka/musicPlayer/MusicPlayService;->info:Lcom/konka/musicPlayer/MusicInfo;

    invoke-direct {p0}, Lcom/konka/musicPlayer/MusicPlayService;->initPlay()V

    iget-object v2, p0, Lcom/konka/musicPlayer/MusicPlayService;->info:Lcom/konka/musicPlayer/MusicInfo;

    invoke-direct {p0, v2}, Lcom/konka/musicPlayer/MusicPlayService;->playMusic(Lcom/konka/musicPlayer/MusicInfo;)V

    goto :goto_0

    :pswitch_2
    const-string v2, "info"

    invoke-virtual {p1, v2}, Landroid/content/Intent;->getSerializableExtra(Ljava/lang/String;)Ljava/io/Serializable;

    move-result-object v2

    check-cast v2, Lcom/konka/musicPlayer/MusicInfo;

    iput-object v2, p0, Lcom/konka/musicPlayer/MusicPlayService;->info:Lcom/konka/musicPlayer/MusicInfo;

    const-string v2, "CurrentTime"

    invoke-virtual {p1, v2, v5}, Landroid/content/Intent;->getIntExtra(Ljava/lang/String;I)I

    move-result v2

    iput v2, p0, Lcom/konka/musicPlayer/MusicPlayService;->currentTime:I

    invoke-direct {p0}, Lcom/konka/musicPlayer/MusicPlayService;->initPlay()V

    iget-object v2, p0, Lcom/konka/musicPlayer/MusicPlayService;->info:Lcom/konka/musicPlayer/MusicInfo;

    invoke-direct {p0, v2}, Lcom/konka/musicPlayer/MusicPlayService;->playMusic(Lcom/konka/musicPlayer/MusicInfo;)V

    goto :goto_0

    :pswitch_3
    const-string v2, "info"

    invoke-virtual {p1, v2}, Landroid/content/Intent;->getSerializableExtra(Ljava/lang/String;)Ljava/io/Serializable;

    move-result-object v2

    check-cast v2, Lcom/konka/musicPlayer/MusicInfo;

    iput-object v2, p0, Lcom/konka/musicPlayer/MusicPlayService;->info:Lcom/konka/musicPlayer/MusicInfo;

    const-string v2, "CurrentTime"

    invoke-virtual {p1, v2, v5}, Landroid/content/Intent;->getIntExtra(Ljava/lang/String;I)I

    move-result v2

    iput v2, p0, Lcom/konka/musicPlayer/MusicPlayService;->currentTime:I

    const-string v2, "noob"

    new-instance v3, Ljava/lang/StringBuilder;

    const-string v4, "MusicFinals.PLAY: current time = "

    invoke-direct {v3, v4}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    iget v4, p0, Lcom/konka/musicPlayer/MusicPlayService;->currentTime:I

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v2, v3}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    iget-object v2, p0, Lcom/konka/musicPlayer/MusicPlayService;->info:Lcom/konka/musicPlayer/MusicInfo;

    invoke-direct {p0, v2}, Lcom/konka/musicPlayer/MusicPlayService;->playMusic(Lcom/konka/musicPlayer/MusicInfo;)V

    goto/16 :goto_0

    :pswitch_4
    const-string v2, "info"

    invoke-virtual {p1, v2}, Landroid/content/Intent;->getSerializableExtra(Ljava/lang/String;)Ljava/io/Serializable;

    move-result-object v2

    check-cast v2, Lcom/konka/musicPlayer/MusicInfo;

    iput-object v2, p0, Lcom/konka/musicPlayer/MusicPlayService;->info:Lcom/konka/musicPlayer/MusicInfo;

    const-string v2, "musicplayService..."

    new-instance v3, Ljava/lang/StringBuilder;

    const-string v4, "pause info: "

    invoke-direct {v3, v4}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    iget-object v4, p0, Lcom/konka/musicPlayer/MusicPlayService;->info:Lcom/konka/musicPlayer/MusicInfo;

    invoke-virtual {v4}, Lcom/konka/musicPlayer/MusicInfo;->getData()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v2, v3}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    iget-object v2, p0, Lcom/konka/musicPlayer/MusicPlayService;->info:Lcom/konka/musicPlayer/MusicInfo;

    invoke-direct {p0, v2}, Lcom/konka/musicPlayer/MusicPlayService;->playMusic(Lcom/konka/musicPlayer/MusicInfo;)V

    goto/16 :goto_0

    :pswitch_5
    const-string v2, "info"

    invoke-virtual {p1, v2}, Landroid/content/Intent;->getSerializableExtra(Ljava/lang/String;)Ljava/io/Serializable;

    move-result-object v2

    check-cast v2, Lcom/konka/musicPlayer/MusicInfo;

    iput-object v2, p0, Lcom/konka/musicPlayer/MusicPlayService;->info:Lcom/konka/musicPlayer/MusicInfo;

    const-string v2, "musicplayService..."

    new-instance v3, Ljava/lang/StringBuilder;

    const-string v4, "pause info------------0: "

    invoke-direct {v3, v4}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    iget-object v4, p0, Lcom/konka/musicPlayer/MusicPlayService;->info:Lcom/konka/musicPlayer/MusicInfo;

    invoke-virtual {v4}, Lcom/konka/musicPlayer/MusicInfo;->getData()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v2, v3}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    invoke-direct {p0}, Lcom/konka/musicPlayer/MusicPlayService;->initPlay()V

    iget-object v2, p0, Lcom/konka/musicPlayer/MusicPlayService;->info:Lcom/konka/musicPlayer/MusicInfo;

    invoke-direct {p0, v2}, Lcom/konka/musicPlayer/MusicPlayService;->playMusic(Lcom/konka/musicPlayer/MusicInfo;)V

    goto/16 :goto_0

    :pswitch_6
    const-string v2, "progress"

    invoke-virtual {p1, v2, v5}, Landroid/content/Intent;->getIntExtra(Ljava/lang/String;I)I

    move-result v1

    const-string v2, "musicplayService..."

    new-instance v3, Ljava/lang/StringBuilder;

    const-string v4, "pause info------------1: "

    invoke-direct {v3, v4}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v3, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v2, v3}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    iget-object v2, p0, Lcom/konka/musicPlayer/MusicPlayService;->player:Landroid/media/MediaPlayer;

    if-eqz v2, :cond_0

    iget-boolean v2, p0, Lcom/konka/musicPlayer/MusicPlayService;->isPause:Z

    if-eqz v2, :cond_2

    iget-object v2, p0, Lcom/konka/musicPlayer/MusicPlayService;->player:Landroid/media/MediaPlayer;

    invoke-virtual {v2}, Landroid/media/MediaPlayer;->start()V

    :cond_2
    iget-object v2, p0, Lcom/konka/musicPlayer/MusicPlayService;->player:Landroid/media/MediaPlayer;

    invoke-virtual {v2, v1}, Landroid/media/MediaPlayer;->seekTo(I)V

    const-string v2, "musicplayService..."

    new-instance v3, Ljava/lang/StringBuilder;

    const-string v4, "pause info------------2: "

    invoke-direct {v3, v4}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v3, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v2, v3}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    goto/16 :goto_0

    :pswitch_data_0
    .packed-switch 0x0
        :pswitch_3
        :pswitch_4
        :pswitch_1
        :pswitch_2
        :pswitch_0
        :pswitch_5
        :pswitch_6
    .end packed-switch
.end method

.method public run()V
    .locals 4

    const-string v2, "musicplayService..."

    const-string v3, "run()"

    invoke-static {v2, v3}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    :goto_0
    iget-boolean v2, p0, Lcom/konka/musicPlayer/MusicPlayService;->isRun:Z

    if-nez v2, :cond_0

    return-void

    :cond_0
    :try_start_0
    iget-object v2, p0, Lcom/konka/musicPlayer/MusicPlayService;->handler:Landroid/os/Handler;

    invoke-virtual {v2}, Landroid/os/Handler;->obtainMessage()Landroid/os/Message;

    move-result-object v1

    iget-object v2, p0, Lcom/konka/musicPlayer/MusicPlayService;->player:Landroid/media/MediaPlayer;

    iput-object v2, v1, Landroid/os/Message;->obj:Ljava/lang/Object;

    iget-object v2, p0, Lcom/konka/musicPlayer/MusicPlayService;->handler:Landroid/os/Handler;

    invoke-virtual {v2, v1}, Landroid/os/Handler;->sendMessage(Landroid/os/Message;)Z

    const-wide/16 v2, 0x3e8

    invoke-static {v2, v3}, Ljava/lang/Thread;->sleep(J)V
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    goto :goto_0

    :catch_0
    move-exception v0

    invoke-virtual {v0}, Ljava/lang/Exception;->printStackTrace()V

    goto :goto_0
.end method
