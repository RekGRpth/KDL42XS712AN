.class Lcom/konka/musicPlayer/MusicPlayService$1;
.super Landroid/os/Handler;
.source "MusicPlayService.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/konka/musicPlayer/MusicPlayService;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/konka/musicPlayer/MusicPlayService;


# direct methods
.method constructor <init>(Lcom/konka/musicPlayer/MusicPlayService;)V
    .locals 0

    iput-object p1, p0, Lcom/konka/musicPlayer/MusicPlayService$1;->this$0:Lcom/konka/musicPlayer/MusicPlayService;

    invoke-direct {p0}, Landroid/os/Handler;-><init>()V

    return-void
.end method


# virtual methods
.method public handleMessage(Landroid/os/Message;)V
    .locals 7
    .param p1    # Landroid/os/Message;

    const/4 v3, 0x0

    const-string v0, "musicplayService..."

    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "Handler()"

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    # getter for: Lcom/konka/musicPlayer/MusicPlayService;->activity:Lcom/konka/musicPlayer/musicPlayerActivity;
    invoke-static {}, Lcom/konka/musicPlayer/MusicPlayService;->access$0()Lcom/konka/musicPlayer/musicPlayerActivity;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    # getter for: Lcom/konka/musicPlayer/MusicPlayService;->activity:Lcom/konka/musicPlayer/musicPlayerActivity;
    invoke-static {}, Lcom/konka/musicPlayer/MusicPlayService;->access$0()Lcom/konka/musicPlayer/musicPlayerActivity;

    move-result-object v0

    if-eqz v0, :cond_0

    # getter for: Lcom/konka/musicPlayer/MusicPlayService;->activity:Lcom/konka/musicPlayer/musicPlayerActivity;
    invoke-static {}, Lcom/konka/musicPlayer/MusicPlayService;->access$0()Lcom/konka/musicPlayer/musicPlayerActivity;

    move-result-object v0

    const/4 v1, 0x1

    new-array v1, v1, [Ljava/lang/Object;

    iget-object v2, p1, Landroid/os/Message;->obj:Ljava/lang/Object;

    aput-object v2, v1, v3

    invoke-virtual {v0, v1}, Lcom/konka/musicPlayer/musicPlayerActivity;->refresh([Ljava/lang/Object;)V

    :cond_0
    iget-object v0, p0, Lcom/konka/musicPlayer/MusicPlayService$1;->this$0:Lcom/konka/musicPlayer/MusicPlayService;

    iget-boolean v0, v0, Lcom/konka/musicPlayer/MusicPlayService;->isSend:Z

    if-eqz v0, :cond_1

    iget-object v0, p0, Lcom/konka/musicPlayer/MusicPlayService$1;->this$0:Lcom/konka/musicPlayer/MusicPlayService;

    iget v0, v0, Lcom/konka/musicPlayer/MusicPlayService;->relayTimer:I

    const/4 v1, 0x3

    if-gt v0, v1, :cond_2

    iget-object v0, p0, Lcom/konka/musicPlayer/MusicPlayService$1;->this$0:Lcom/konka/musicPlayer/MusicPlayService;

    iget v1, v0, Lcom/konka/musicPlayer/MusicPlayService;->relayTimer:I

    add-int/lit8 v1, v1, 0x1

    iput v1, v0, Lcom/konka/musicPlayer/MusicPlayService;->relayTimer:I

    # getter for: Lcom/konka/musicPlayer/MusicPlayService;->activity:Lcom/konka/musicPlayer/musicPlayerActivity;
    invoke-static {}, Lcom/konka/musicPlayer/MusicPlayService;->access$0()Lcom/konka/musicPlayer/musicPlayerActivity;

    move-result-object v0

    # getter for: Lcom/konka/musicPlayer/MusicPlayService;->activity:Lcom/konka/musicPlayer/musicPlayerActivity;
    invoke-static {}, Lcom/konka/musicPlayer/MusicPlayService;->access$0()Lcom/konka/musicPlayer/musicPlayerActivity;

    move-result-object v1

    sget-object v2, Lcom/konka/mediaSharePlayer/PlayerState;->PLAY_STATUS_SYNC:Lcom/konka/mediaSharePlayer/PlayerState;

    invoke-virtual {v2}, Lcom/konka/mediaSharePlayer/PlayerState;->ordinal()I

    move-result v2

    iget-object v3, p0, Lcom/konka/musicPlayer/MusicPlayService$1;->this$0:Lcom/konka/musicPlayer/MusicPlayService;

    # getter for: Lcom/konka/musicPlayer/MusicPlayService;->player:Landroid/media/MediaPlayer;
    invoke-static {v3}, Lcom/konka/musicPlayer/MusicPlayService;->access$1(Lcom/konka/musicPlayer/MusicPlayService;)Landroid/media/MediaPlayer;

    move-result-object v3

    invoke-virtual {v3}, Landroid/media/MediaPlayer;->getCurrentPosition()I

    move-result v3

    iget-object v4, p0, Lcom/konka/musicPlayer/MusicPlayService$1;->this$0:Lcom/konka/musicPlayer/MusicPlayService;

    # getter for: Lcom/konka/musicPlayer/MusicPlayService;->player:Landroid/media/MediaPlayer;
    invoke-static {v4}, Lcom/konka/musicPlayer/MusicPlayService;->access$1(Lcom/konka/musicPlayer/MusicPlayService;)Landroid/media/MediaPlayer;

    move-result-object v4

    invoke-virtual {v4}, Landroid/media/MediaPlayer;->getDuration()I

    move-result v4

    new-instance v5, Ljava/lang/StringBuilder;

    iget-object v6, p0, Lcom/konka/musicPlayer/MusicPlayService$1;->this$0:Lcom/konka/musicPlayer/MusicPlayService;

    # getter for: Lcom/konka/musicPlayer/MusicPlayService;->info:Lcom/konka/musicPlayer/MusicInfo;
    invoke-static {v6}, Lcom/konka/musicPlayer/MusicPlayService;->access$2(Lcom/konka/musicPlayer/MusicPlayService;)Lcom/konka/musicPlayer/MusicInfo;

    move-result-object v6

    invoke-virtual {v6}, Lcom/konka/musicPlayer/MusicInfo;->getData()Ljava/lang/String;

    move-result-object v6

    invoke-static {v6}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v6

    invoke-direct {v5, v6}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    const-string v6, "&ma"

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    invoke-virtual/range {v0 .. v5}, Lcom/konka/musicPlayer/musicPlayerActivity;->sendPlayerStateToPlayerService(Landroid/content/Context;IIILjava/lang/String;)V

    :cond_1
    :goto_0
    return-void

    :cond_2
    iget-object v0, p0, Lcom/konka/musicPlayer/MusicPlayService$1;->this$0:Lcom/konka/musicPlayer/MusicPlayService;

    iput v3, v0, Lcom/konka/musicPlayer/MusicPlayService;->relayTimer:I

    iget-object v0, p0, Lcom/konka/musicPlayer/MusicPlayService$1;->this$0:Lcom/konka/musicPlayer/MusicPlayService;

    iput-boolean v3, v0, Lcom/konka/musicPlayer/MusicPlayService;->isSend:Z

    goto :goto_0
.end method
