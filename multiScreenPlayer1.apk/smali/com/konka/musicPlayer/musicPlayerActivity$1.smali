.class Lcom/konka/musicPlayer/musicPlayerActivity$1;
.super Landroid/os/Handler;
.source "musicPlayerActivity.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/konka/musicPlayer/musicPlayerActivity;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/konka/musicPlayer/musicPlayerActivity;


# direct methods
.method constructor <init>(Lcom/konka/musicPlayer/musicPlayerActivity;)V
    .locals 0

    iput-object p1, p0, Lcom/konka/musicPlayer/musicPlayerActivity$1;->this$0:Lcom/konka/musicPlayer/musicPlayerActivity;

    invoke-direct {p0}, Landroid/os/Handler;-><init>()V

    return-void
.end method


# virtual methods
.method public handleMessage(Landroid/os/Message;)V
    .locals 5
    .param p1    # Landroid/os/Message;

    const v4, 0x7f0900a2    # com.konka.mediaSharePlayer.R.string.mediafile_buffering

    const/4 v3, 0x0

    const-string v0, "MultiScreeenMusicActivity:"

    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "handler:------------"

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    iget v2, p1, Landroid/os/Message;->what:I

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    iget v0, p1, Landroid/os/Message;->what:I

    packed-switch v0, :pswitch_data_0

    :cond_0
    :goto_0
    :pswitch_0
    return-void

    :pswitch_1
    iget-object v0, p0, Lcom/konka/musicPlayer/musicPlayerActivity$1;->this$0:Lcom/konka/musicPlayer/musicPlayerActivity;

    # getter for: Lcom/konka/musicPlayer/musicPlayerActivity;->info:Lcom/konka/musicPlayer/MusicInfo;
    invoke-static {v0}, Lcom/konka/musicPlayer/musicPlayerActivity;->access$0(Lcom/konka/musicPlayer/musicPlayerActivity;)Lcom/konka/musicPlayer/MusicInfo;

    move-result-object v0

    invoke-virtual {v0}, Lcom/konka/musicPlayer/MusicInfo;->getData()Ljava/lang/String;

    move-result-object v0

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/konka/musicPlayer/musicPlayerActivity$1;->this$0:Lcom/konka/musicPlayer/musicPlayerActivity;

    invoke-virtual {v0}, Lcom/konka/musicPlayer/musicPlayerActivity;->cancelProgressDlg()V

    iget-object v0, p0, Lcom/konka/musicPlayer/musicPlayerActivity$1;->this$0:Lcom/konka/musicPlayer/musicPlayerActivity;

    # invokes: Lcom/konka/musicPlayer/musicPlayerActivity;->showProgressDialog(II)V
    invoke-static {v0, v4, v4}, Lcom/konka/musicPlayer/musicPlayerActivity;->access$1(Lcom/konka/musicPlayer/musicPlayerActivity;II)V

    iget-object v0, p0, Lcom/konka/musicPlayer/musicPlayerActivity$1;->this$0:Lcom/konka/musicPlayer/musicPlayerActivity;

    iget-object v1, p0, Lcom/konka/musicPlayer/musicPlayerActivity$1;->this$0:Lcom/konka/musicPlayer/musicPlayerActivity;

    # getter for: Lcom/konka/musicPlayer/musicPlayerActivity;->info:Lcom/konka/musicPlayer/MusicInfo;
    invoke-static {v1}, Lcom/konka/musicPlayer/musicPlayerActivity;->access$0(Lcom/konka/musicPlayer/musicPlayerActivity;)Lcom/konka/musicPlayer/MusicInfo;

    move-result-object v1

    # invokes: Lcom/konka/musicPlayer/musicPlayerActivity;->initBarInfo(Lcom/konka/musicPlayer/MusicInfo;)V
    invoke-static {v0, v1}, Lcom/konka/musicPlayer/musicPlayerActivity;->access$2(Lcom/konka/musicPlayer/musicPlayerActivity;Lcom/konka/musicPlayer/MusicInfo;)V

    iget-object v0, p0, Lcom/konka/musicPlayer/musicPlayerActivity$1;->this$0:Lcom/konka/musicPlayer/musicPlayerActivity;

    invoke-virtual {v0}, Lcom/konka/musicPlayer/musicPlayerActivity;->encodeMusicUri()Ljava/lang/String;

    iget-object v0, p0, Lcom/konka/musicPlayer/musicPlayerActivity$1;->this$0:Lcom/konka/musicPlayer/musicPlayerActivity;

    iget-object v1, p0, Lcom/konka/musicPlayer/musicPlayerActivity$1;->this$0:Lcom/konka/musicPlayer/musicPlayerActivity;

    # getter for: Lcom/konka/musicPlayer/musicPlayerActivity;->info:Lcom/konka/musicPlayer/MusicInfo;
    invoke-static {v1}, Lcom/konka/musicPlayer/musicPlayerActivity;->access$0(Lcom/konka/musicPlayer/musicPlayerActivity;)Lcom/konka/musicPlayer/MusicInfo;

    move-result-object v1

    # invokes: Lcom/konka/musicPlayer/musicPlayerActivity;->playMusicService(ILcom/konka/musicPlayer/MusicInfo;)V
    invoke-static {v0, v3, v1}, Lcom/konka/musicPlayer/musicPlayerActivity;->access$3(Lcom/konka/musicPlayer/musicPlayerActivity;ILcom/konka/musicPlayer/MusicInfo;)V

    iget-object v0, p0, Lcom/konka/musicPlayer/musicPlayerActivity$1;->this$0:Lcom/konka/musicPlayer/musicPlayerActivity;

    iput-boolean v3, v0, Lcom/konka/musicPlayer/musicPlayerActivity;->isMusicPlay:Z

    iget-object v0, p0, Lcom/konka/musicPlayer/musicPlayerActivity$1;->this$0:Lcom/konka/musicPlayer/musicPlayerActivity;

    iput-boolean v3, v0, Lcom/konka/musicPlayer/musicPlayerActivity;->isPlayerPrepare:Z

    goto :goto_0

    :pswitch_2
    iget-object v0, p0, Lcom/konka/musicPlayer/musicPlayerActivity$1;->this$0:Lcom/konka/musicPlayer/musicPlayerActivity;

    # getter for: Lcom/konka/musicPlayer/musicPlayerActivity;->info:Lcom/konka/musicPlayer/MusicInfo;
    invoke-static {v0}, Lcom/konka/musicPlayer/musicPlayerActivity;->access$0(Lcom/konka/musicPlayer/musicPlayerActivity;)Lcom/konka/musicPlayer/MusicInfo;

    move-result-object v0

    invoke-virtual {v0}, Lcom/konka/musicPlayer/MusicInfo;->getData()Ljava/lang/String;

    move-result-object v0

    if-eqz v0, :cond_0

    const-string v0, "noob"

    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "music thread: pos = "

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    iget-object v2, p0, Lcom/konka/musicPlayer/musicPlayerActivity$1;->this$0:Lcom/konka/musicPlayer/musicPlayerActivity;

    # getter for: Lcom/konka/musicPlayer/musicPlayerActivity;->resumePos:I
    invoke-static {v2}, Lcom/konka/musicPlayer/musicPlayerActivity;->access$4(Lcom/konka/musicPlayer/musicPlayerActivity;)I

    move-result v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    iget-object v0, p0, Lcom/konka/musicPlayer/musicPlayerActivity$1;->this$0:Lcom/konka/musicPlayer/musicPlayerActivity;

    iget-object v1, p0, Lcom/konka/musicPlayer/musicPlayerActivity$1;->this$0:Lcom/konka/musicPlayer/musicPlayerActivity;

    # getter for: Lcom/konka/musicPlayer/musicPlayerActivity;->info:Lcom/konka/musicPlayer/MusicInfo;
    invoke-static {v1}, Lcom/konka/musicPlayer/musicPlayerActivity;->access$0(Lcom/konka/musicPlayer/musicPlayerActivity;)Lcom/konka/musicPlayer/MusicInfo;

    move-result-object v1

    # invokes: Lcom/konka/musicPlayer/musicPlayerActivity;->initBarInfo(Lcom/konka/musicPlayer/MusicInfo;)V
    invoke-static {v0, v1}, Lcom/konka/musicPlayer/musicPlayerActivity;->access$2(Lcom/konka/musicPlayer/musicPlayerActivity;Lcom/konka/musicPlayer/MusicInfo;)V

    iget-object v0, p0, Lcom/konka/musicPlayer/musicPlayerActivity$1;->this$0:Lcom/konka/musicPlayer/musicPlayerActivity;

    const/4 v1, 0x3

    iget-object v2, p0, Lcom/konka/musicPlayer/musicPlayerActivity$1;->this$0:Lcom/konka/musicPlayer/musicPlayerActivity;

    # getter for: Lcom/konka/musicPlayer/musicPlayerActivity;->info:Lcom/konka/musicPlayer/MusicInfo;
    invoke-static {v2}, Lcom/konka/musicPlayer/musicPlayerActivity;->access$0(Lcom/konka/musicPlayer/musicPlayerActivity;)Lcom/konka/musicPlayer/MusicInfo;

    move-result-object v2

    # invokes: Lcom/konka/musicPlayer/musicPlayerActivity;->playMusicService(ILcom/konka/musicPlayer/MusicInfo;)V
    invoke-static {v0, v1, v2}, Lcom/konka/musicPlayer/musicPlayerActivity;->access$3(Lcom/konka/musicPlayer/musicPlayerActivity;ILcom/konka/musicPlayer/MusicInfo;)V

    iget-object v0, p0, Lcom/konka/musicPlayer/musicPlayerActivity$1;->this$0:Lcom/konka/musicPlayer/musicPlayerActivity;

    iput-boolean v3, v0, Lcom/konka/musicPlayer/musicPlayerActivity;->isMusicPlay:Z

    iget-object v0, p0, Lcom/konka/musicPlayer/musicPlayerActivity$1;->this$0:Lcom/konka/musicPlayer/musicPlayerActivity;

    iput-boolean v3, v0, Lcom/konka/musicPlayer/musicPlayerActivity;->isPlayerPrepare:Z

    goto :goto_0

    :pswitch_data_0
    .packed-switch 0x0
        :pswitch_0
        :pswitch_1
        :pswitch_2
    .end packed-switch
.end method
