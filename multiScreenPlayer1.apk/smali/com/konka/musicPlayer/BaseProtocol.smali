.class public Lcom/konka/musicPlayer/BaseProtocol;
.super Ljava/lang/Object;
.source "BaseProtocol.java"


# direct methods
.method public constructor <init>()V
    .locals 0

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method public static intToByteArray(I[BI)[B
    .locals 2
    .param p0    # I
    .param p1    # [B
    .param p2    # I

    const/4 v0, 0x3

    and-int/lit16 v1, p0, 0xff

    int-to-byte v1, v1

    aput-byte v1, p1, v0

    const/4 v0, 0x2

    const v1, 0xff00

    and-int/2addr v1, p0

    shr-int/lit8 v1, v1, 0x8

    int-to-byte v1, v1

    aput-byte v1, p1, v0

    const/4 v0, 0x1

    const/high16 v1, 0xff0000

    and-int/2addr v1, p0

    shr-int/lit8 v1, v1, 0x10

    int-to-byte v1, v1

    aput-byte v1, p1, v0

    const/4 v0, 0x0

    const/high16 v1, -0x1000000

    and-int/2addr v1, p0

    shr-int/lit8 v1, v1, 0x18

    int-to-byte v1, v1

    aput-byte v1, p1, v0

    return-object p1
.end method


# virtual methods
.method public byteToInt([B)I
    .locals 6
    .param p1    # [B

    const/4 v0, 0x0

    const/4 v1, 0x0

    const/4 v2, 0x0

    const/4 v3, 0x0

    const/4 v4, 0x0

    const/4 v5, 0x3

    aget-byte v5, p1, v5

    and-int/lit16 v1, v5, 0xff

    const/4 v5, 0x2

    aget-byte v5, p1, v5

    and-int/lit16 v2, v5, 0xff

    const/4 v5, 0x1

    aget-byte v5, p1, v5

    and-int/lit16 v3, v5, 0xff

    const/4 v5, 0x0

    aget-byte v5, p1, v5

    and-int/lit16 v4, v5, 0xff

    shl-int/lit8 v4, v4, 0x18

    shl-int/lit8 v3, v3, 0x10

    shl-int/lit8 v2, v2, 0x8

    or-int v5, v1, v2

    or-int/2addr v5, v3

    or-int v0, v5, v4

    return v0
.end method
