.class Lcom/konka/musicPlayer/MusicPlayService$2;
.super Landroid/os/Handler;
.source "MusicPlayService.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/konka/musicPlayer/MusicPlayService;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/konka/musicPlayer/MusicPlayService;


# direct methods
.method constructor <init>(Lcom/konka/musicPlayer/MusicPlayService;)V
    .locals 0

    iput-object p1, p0, Lcom/konka/musicPlayer/MusicPlayService$2;->this$0:Lcom/konka/musicPlayer/MusicPlayService;

    invoke-direct {p0}, Landroid/os/Handler;-><init>()V

    return-void
.end method


# virtual methods
.method public handleMessage(Landroid/os/Message;)V
    .locals 4
    .param p1    # Landroid/os/Message;

    const/4 v3, 0x0

    const-string v0, "musicplayService..."

    const-string v1, "musichandler()"

    invoke-static {v0, v1}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    iget v0, p1, Landroid/os/Message;->what:I

    packed-switch v0, :pswitch_data_0

    :goto_0
    return-void

    :pswitch_0
    const-string v0, "musicplayService..."

    const-string v1, "music path is null!!"

    invoke-static {v0, v1}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_0

    :pswitch_1
    const-string v0, "musicplayService..."

    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "music patht is 1___________"

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    iget-object v2, p0, Lcom/konka/musicPlayer/MusicPlayService$2;->this$0:Lcom/konka/musicPlayer/MusicPlayService;

    iget-object v2, v2, Lcom/konka/musicPlayer/MusicPlayService;->musicPath:Ljava/lang/String;

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    iget-object v0, p0, Lcom/konka/musicPlayer/MusicPlayService$2;->this$0:Lcom/konka/musicPlayer/MusicPlayService;

    # getter for: Lcom/konka/musicPlayer/MusicPlayService;->currentTime:I
    invoke-static {v0}, Lcom/konka/musicPlayer/MusicPlayService;->access$3(Lcom/konka/musicPlayer/MusicPlayService;)I

    move-result v0

    if-lez v0, :cond_0

    iget-object v0, p0, Lcom/konka/musicPlayer/MusicPlayService$2;->this$0:Lcom/konka/musicPlayer/MusicPlayService;

    # getter for: Lcom/konka/musicPlayer/MusicPlayService;->player:Landroid/media/MediaPlayer;
    invoke-static {v0}, Lcom/konka/musicPlayer/MusicPlayService;->access$1(Lcom/konka/musicPlayer/MusicPlayService;)Landroid/media/MediaPlayer;

    move-result-object v0

    iget-object v1, p0, Lcom/konka/musicPlayer/MusicPlayService$2;->this$0:Lcom/konka/musicPlayer/MusicPlayService;

    iget-object v1, v1, Lcom/konka/musicPlayer/MusicPlayService;->musicPath:Ljava/lang/String;

    iget-object v2, p0, Lcom/konka/musicPlayer/MusicPlayService$2;->this$0:Lcom/konka/musicPlayer/MusicPlayService;

    # getter for: Lcom/konka/musicPlayer/MusicPlayService;->currentTime:I
    invoke-static {v2}, Lcom/konka/musicPlayer/MusicPlayService;->access$3(Lcom/konka/musicPlayer/MusicPlayService;)I

    move-result v2

    invoke-static {v0, v1, v2}, Lcom/konka/musicPlayer/MusicTool;->playMusic(Landroid/media/MediaPlayer;Ljava/lang/String;I)Z

    goto :goto_0

    :cond_0
    iget-object v0, p0, Lcom/konka/musicPlayer/MusicPlayService$2;->this$0:Lcom/konka/musicPlayer/MusicPlayService;

    # getter for: Lcom/konka/musicPlayer/MusicPlayService;->currentTime:I
    invoke-static {v0}, Lcom/konka/musicPlayer/MusicPlayService;->access$3(Lcom/konka/musicPlayer/MusicPlayService;)I

    move-result v0

    if-nez v0, :cond_1

    iget-object v0, p0, Lcom/konka/musicPlayer/MusicPlayService$2;->this$0:Lcom/konka/musicPlayer/MusicPlayService;

    # getter for: Lcom/konka/musicPlayer/MusicPlayService;->player:Landroid/media/MediaPlayer;
    invoke-static {v0}, Lcom/konka/musicPlayer/MusicPlayService;->access$1(Lcom/konka/musicPlayer/MusicPlayService;)Landroid/media/MediaPlayer;

    move-result-object v0

    iget-object v1, p0, Lcom/konka/musicPlayer/MusicPlayService$2;->this$0:Lcom/konka/musicPlayer/MusicPlayService;

    iget-object v1, v1, Lcom/konka/musicPlayer/MusicPlayService;->musicPath:Ljava/lang/String;

    invoke-static {v0, v1, v3}, Lcom/konka/musicPlayer/MusicTool;->playMusic(Landroid/media/MediaPlayer;Ljava/lang/String;I)Z

    goto :goto_0

    :cond_1
    const-string v0, "musicplayService..."

    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "currentTime___________"

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    iget-object v2, p0, Lcom/konka/musicPlayer/MusicPlayService$2;->this$0:Lcom/konka/musicPlayer/MusicPlayService;

    # getter for: Lcom/konka/musicPlayer/MusicPlayService;->currentTime:I
    invoke-static {v2}, Lcom/konka/musicPlayer/MusicPlayService;->access$3(Lcom/konka/musicPlayer/MusicPlayService;)I

    move-result v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_0

    :pswitch_2
    const-string v0, "musicplayService..."

    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "music patht is 2___________"

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    iget-object v2, p0, Lcom/konka/musicPlayer/MusicPlayService$2;->this$0:Lcom/konka/musicPlayer/MusicPlayService;

    iget-object v2, v2, Lcom/konka/musicPlayer/MusicPlayService;->musicPath:Ljava/lang/String;

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    iget-object v0, p0, Lcom/konka/musicPlayer/MusicPlayService$2;->this$0:Lcom/konka/musicPlayer/MusicPlayService;

    # getter for: Lcom/konka/musicPlayer/MusicPlayService;->currentTime:I
    invoke-static {v0}, Lcom/konka/musicPlayer/MusicPlayService;->access$3(Lcom/konka/musicPlayer/MusicPlayService;)I

    move-result v0

    if-lez v0, :cond_2

    iget-object v0, p0, Lcom/konka/musicPlayer/MusicPlayService$2;->this$0:Lcom/konka/musicPlayer/MusicPlayService;

    # getter for: Lcom/konka/musicPlayer/MusicPlayService;->player:Landroid/media/MediaPlayer;
    invoke-static {v0}, Lcom/konka/musicPlayer/MusicPlayService;->access$1(Lcom/konka/musicPlayer/MusicPlayService;)Landroid/media/MediaPlayer;

    move-result-object v0

    iget-object v1, p0, Lcom/konka/musicPlayer/MusicPlayService$2;->this$0:Lcom/konka/musicPlayer/MusicPlayService;

    iget-object v1, v1, Lcom/konka/musicPlayer/MusicPlayService;->musicPath:Ljava/lang/String;

    iget-object v2, p0, Lcom/konka/musicPlayer/MusicPlayService$2;->this$0:Lcom/konka/musicPlayer/MusicPlayService;

    # getter for: Lcom/konka/musicPlayer/MusicPlayService;->currentTime:I
    invoke-static {v2}, Lcom/konka/musicPlayer/MusicPlayService;->access$3(Lcom/konka/musicPlayer/MusicPlayService;)I

    move-result v2

    invoke-static {v0, v1, v2}, Lcom/konka/musicPlayer/MusicTool;->playMusic(Landroid/media/MediaPlayer;Ljava/lang/String;I)Z

    goto/16 :goto_0

    :cond_2
    iget-object v0, p0, Lcom/konka/musicPlayer/MusicPlayService$2;->this$0:Lcom/konka/musicPlayer/MusicPlayService;

    # getter for: Lcom/konka/musicPlayer/MusicPlayService;->currentTime:I
    invoke-static {v0}, Lcom/konka/musicPlayer/MusicPlayService;->access$3(Lcom/konka/musicPlayer/MusicPlayService;)I

    move-result v0

    if-nez v0, :cond_3

    iget-object v0, p0, Lcom/konka/musicPlayer/MusicPlayService$2;->this$0:Lcom/konka/musicPlayer/MusicPlayService;

    # getter for: Lcom/konka/musicPlayer/MusicPlayService;->player:Landroid/media/MediaPlayer;
    invoke-static {v0}, Lcom/konka/musicPlayer/MusicPlayService;->access$1(Lcom/konka/musicPlayer/MusicPlayService;)Landroid/media/MediaPlayer;

    move-result-object v0

    iget-object v1, p0, Lcom/konka/musicPlayer/MusicPlayService$2;->this$0:Lcom/konka/musicPlayer/MusicPlayService;

    iget-object v1, v1, Lcom/konka/musicPlayer/MusicPlayService;->musicPath:Ljava/lang/String;

    invoke-static {v0, v1, v3}, Lcom/konka/musicPlayer/MusicTool;->playMusic(Landroid/media/MediaPlayer;Ljava/lang/String;I)Z

    goto/16 :goto_0

    :cond_3
    const-string v0, "musicplayService..."

    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "currentTime2___________"

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    iget-object v2, p0, Lcom/konka/musicPlayer/MusicPlayService$2;->this$0:Lcom/konka/musicPlayer/MusicPlayService;

    # getter for: Lcom/konka/musicPlayer/MusicPlayService;->currentTime:I
    invoke-static {v2}, Lcom/konka/musicPlayer/MusicPlayService;->access$3(Lcom/konka/musicPlayer/MusicPlayService;)I

    move-result v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    goto/16 :goto_0

    nop

    :pswitch_data_0
    .packed-switch 0x0
        :pswitch_0
        :pswitch_1
        :pswitch_2
    .end packed-switch
.end method
