.class Lcom/konka/musicPlayer/musicPlayerActivity$4;
.super Ljava/lang/Object;
.source "musicPlayerActivity.java"

# interfaces
.implements Landroid/widget/SeekBar$OnSeekBarChangeListener;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/konka/musicPlayer/musicPlayerActivity;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/konka/musicPlayer/musicPlayerActivity;


# direct methods
.method constructor <init>(Lcom/konka/musicPlayer/musicPlayerActivity;)V
    .locals 0

    iput-object p1, p0, Lcom/konka/musicPlayer/musicPlayerActivity$4;->this$0:Lcom/konka/musicPlayer/musicPlayerActivity;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onProgressChanged(Landroid/widget/SeekBar;IZ)V
    .locals 0
    .param p1    # Landroid/widget/SeekBar;
    .param p2    # I
    .param p3    # Z

    return-void
.end method

.method public onStartTrackingTouch(Landroid/widget/SeekBar;)V
    .locals 0
    .param p1    # Landroid/widget/SeekBar;

    return-void
.end method

.method public onStopTrackingTouch(Landroid/widget/SeekBar;)V
    .locals 3
    .param p1    # Landroid/widget/SeekBar;

    const-string v0, "MultiScreeenMusicActivity:"

    const-string v1, "onStopTrackingTouch"

    invoke-static {v0, v1}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    iget-object v0, p0, Lcom/konka/musicPlayer/musicPlayerActivity$4;->this$0:Lcom/konka/musicPlayer/musicPlayerActivity;

    # getter for: Lcom/konka/musicPlayer/musicPlayerActivity;->service:Landroid/content/Intent;
    invoke-static {v0}, Lcom/konka/musicPlayer/musicPlayerActivity;->access$17(Lcom/konka/musicPlayer/musicPlayerActivity;)Landroid/content/Intent;

    move-result-object v0

    const-string v1, "MSG"

    const/4 v2, 0x6

    invoke-virtual {v0, v1, v2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;I)Landroid/content/Intent;

    iget-object v0, p0, Lcom/konka/musicPlayer/musicPlayerActivity$4;->this$0:Lcom/konka/musicPlayer/musicPlayerActivity;

    # getter for: Lcom/konka/musicPlayer/musicPlayerActivity;->service:Landroid/content/Intent;
    invoke-static {v0}, Lcom/konka/musicPlayer/musicPlayerActivity;->access$17(Lcom/konka/musicPlayer/musicPlayerActivity;)Landroid/content/Intent;

    move-result-object v0

    const-string v1, "progress"

    invoke-virtual {p1}, Landroid/widget/SeekBar;->getProgress()I

    move-result v2

    invoke-virtual {v0, v1, v2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;I)Landroid/content/Intent;

    iget-object v0, p0, Lcom/konka/musicPlayer/musicPlayerActivity$4;->this$0:Lcom/konka/musicPlayer/musicPlayerActivity;

    iget-object v1, p0, Lcom/konka/musicPlayer/musicPlayerActivity$4;->this$0:Lcom/konka/musicPlayer/musicPlayerActivity;

    # getter for: Lcom/konka/musicPlayer/musicPlayerActivity;->service:Landroid/content/Intent;
    invoke-static {v1}, Lcom/konka/musicPlayer/musicPlayerActivity;->access$17(Lcom/konka/musicPlayer/musicPlayerActivity;)Landroid/content/Intent;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/konka/musicPlayer/musicPlayerActivity;->startService(Landroid/content/Intent;)Landroid/content/ComponentName;

    return-void
.end method
