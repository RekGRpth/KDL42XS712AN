.class Lcom/konka/musicPlayer/musicPlayerActivity$switchReceiver;
.super Landroid/content/BroadcastReceiver;
.source "musicPlayerActivity.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/konka/musicPlayer/musicPlayerActivity;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x2
    name = "switchReceiver"
.end annotation


# instance fields
.field final synthetic this$0:Lcom/konka/musicPlayer/musicPlayerActivity;


# direct methods
.method private constructor <init>(Lcom/konka/musicPlayer/musicPlayerActivity;)V
    .locals 0

    iput-object p1, p0, Lcom/konka/musicPlayer/musicPlayerActivity$switchReceiver;->this$0:Lcom/konka/musicPlayer/musicPlayerActivity;

    invoke-direct {p0}, Landroid/content/BroadcastReceiver;-><init>()V

    return-void
.end method

.method synthetic constructor <init>(Lcom/konka/musicPlayer/musicPlayerActivity;Lcom/konka/musicPlayer/musicPlayerActivity$switchReceiver;)V
    .locals 0

    invoke-direct {p0, p1}, Lcom/konka/musicPlayer/musicPlayerActivity$switchReceiver;-><init>(Lcom/konka/musicPlayer/musicPlayerActivity;)V

    return-void
.end method


# virtual methods
.method public onReceive(Landroid/content/Context;Landroid/content/Intent;)V
    .locals 8
    .param p1    # Landroid/content/Context;
    .param p2    # Landroid/content/Intent;

    const/4 v3, 0x0

    invoke-virtual {p2}, Landroid/content/Intent;->getExtras()Landroid/os/Bundle;

    move-result-object v6

    iget-object v0, p0, Lcom/konka/musicPlayer/musicPlayerActivity$switchReceiver;->this$0:Lcom/konka/musicPlayer/musicPlayerActivity;

    const-string v1, "mediaSwitch"

    const/4 v2, 0x1

    invoke-virtual {v6, v1, v2}, Landroid/os/Bundle;->getInt(Ljava/lang/String;I)I

    move-result v1

    iput v1, v0, Lcom/konka/musicPlayer/musicPlayerActivity;->mediaSwitch:I

    const-string v0, "media stop"

    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "-mediaSwitch-------------music--mediaSwitch: "

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    iget-object v2, p0, Lcom/konka/musicPlayer/musicPlayerActivity$switchReceiver;->this$0:Lcom/konka/musicPlayer/musicPlayerActivity;

    iget v2, v2, Lcom/konka/musicPlayer/musicPlayerActivity;->mediaSwitch:I

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    iget-object v0, p0, Lcom/konka/musicPlayer/musicPlayerActivity$switchReceiver;->this$0:Lcom/konka/musicPlayer/musicPlayerActivity;

    iget v0, v0, Lcom/konka/musicPlayer/musicPlayerActivity;->mediaSwitch:I

    if-nez v0, :cond_0

    iget-object v0, p0, Lcom/konka/musicPlayer/musicPlayerActivity$switchReceiver;->this$0:Lcom/konka/musicPlayer/musicPlayerActivity;

    # invokes: Lcom/konka/musicPlayer/musicPlayerActivity;->stopMusicService()V
    invoke-static {v0}, Lcom/konka/musicPlayer/musicPlayerActivity;->access$10(Lcom/konka/musicPlayer/musicPlayerActivity;)V

    iget-object v0, p0, Lcom/konka/musicPlayer/musicPlayerActivity$switchReceiver;->this$0:Lcom/konka/musicPlayer/musicPlayerActivity;

    invoke-static {v0, v3, v3, v3}, Lcom/konka/mediaSharePlayer/sendPlayerState;->MediaPlayerActionSendBroadcastIsCreat(Landroid/content/Context;III)V

    iget-object v0, p0, Lcom/konka/musicPlayer/musicPlayerActivity$switchReceiver;->this$0:Lcom/konka/musicPlayer/musicPlayerActivity;

    iget-boolean v0, v0, Lcom/konka/musicPlayer/musicPlayerActivity;->isPlayerPrepare:Z

    if-eqz v0, :cond_1

    iget-object v0, p0, Lcom/konka/musicPlayer/musicPlayerActivity$switchReceiver;->this$0:Lcom/konka/musicPlayer/musicPlayerActivity;

    iget-object v1, p0, Lcom/konka/musicPlayer/musicPlayerActivity$switchReceiver;->this$0:Lcom/konka/musicPlayer/musicPlayerActivity;

    sget-object v2, Lcom/konka/mediaSharePlayer/PlayerState;->PLAY_STATUS_SWITCH_PLAYER:Lcom/konka/mediaSharePlayer/PlayerState;

    invoke-virtual {v2}, Lcom/konka/mediaSharePlayer/PlayerState;->ordinal()I

    move-result v2

    iget-object v3, p0, Lcom/konka/musicPlayer/musicPlayerActivity$switchReceiver;->this$0:Lcom/konka/musicPlayer/musicPlayerActivity;

    # getter for: Lcom/konka/musicPlayer/musicPlayerActivity;->player:Landroid/media/MediaPlayer;
    invoke-static {v3}, Lcom/konka/musicPlayer/musicPlayerActivity;->access$7(Lcom/konka/musicPlayer/musicPlayerActivity;)Landroid/media/MediaPlayer;

    move-result-object v3

    invoke-virtual {v3}, Landroid/media/MediaPlayer;->getCurrentPosition()I

    move-result v3

    iget-object v4, p0, Lcom/konka/musicPlayer/musicPlayerActivity$switchReceiver;->this$0:Lcom/konka/musicPlayer/musicPlayerActivity;

    # getter for: Lcom/konka/musicPlayer/musicPlayerActivity;->player:Landroid/media/MediaPlayer;
    invoke-static {v4}, Lcom/konka/musicPlayer/musicPlayerActivity;->access$7(Lcom/konka/musicPlayer/musicPlayerActivity;)Landroid/media/MediaPlayer;

    move-result-object v4

    invoke-virtual {v4}, Landroid/media/MediaPlayer;->getDuration()I

    move-result v4

    new-instance v5, Ljava/lang/StringBuilder;

    iget-object v7, p0, Lcom/konka/musicPlayer/musicPlayerActivity$switchReceiver;->this$0:Lcom/konka/musicPlayer/musicPlayerActivity;

    # getter for: Lcom/konka/musicPlayer/musicPlayerActivity;->info:Lcom/konka/musicPlayer/MusicInfo;
    invoke-static {v7}, Lcom/konka/musicPlayer/musicPlayerActivity;->access$0(Lcom/konka/musicPlayer/musicPlayerActivity;)Lcom/konka/musicPlayer/MusicInfo;

    move-result-object v7

    invoke-virtual {v7}, Lcom/konka/musicPlayer/MusicInfo;->getData()Ljava/lang/String;

    move-result-object v7

    invoke-static {v7}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v7

    invoke-direct {v5, v7}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    const-string v7, "&ma"

    invoke-virtual {v5, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    invoke-virtual/range {v0 .. v5}, Lcom/konka/musicPlayer/musicPlayerActivity;->sendPlayerStateToPlayerService(Landroid/content/Context;IIILjava/lang/String;)V

    :goto_0
    iget-object v0, p0, Lcom/konka/musicPlayer/musicPlayerActivity$switchReceiver;->this$0:Lcom/konka/musicPlayer/musicPlayerActivity;

    invoke-virtual {v0}, Lcom/konka/musicPlayer/musicPlayerActivity;->musicPlayerStop()V

    :cond_0
    return-void

    :cond_1
    iget-object v0, p0, Lcom/konka/musicPlayer/musicPlayerActivity$switchReceiver;->this$0:Lcom/konka/musicPlayer/musicPlayerActivity;

    iget-object v1, p0, Lcom/konka/musicPlayer/musicPlayerActivity$switchReceiver;->this$0:Lcom/konka/musicPlayer/musicPlayerActivity;

    sget-object v2, Lcom/konka/mediaSharePlayer/PlayerState;->PLAY_STATUS_SWITCH_PLAYER:Lcom/konka/mediaSharePlayer/PlayerState;

    invoke-virtual {v2}, Lcom/konka/mediaSharePlayer/PlayerState;->ordinal()I

    move-result v2

    new-instance v4, Ljava/lang/StringBuilder;

    iget-object v5, p0, Lcom/konka/musicPlayer/musicPlayerActivity$switchReceiver;->this$0:Lcom/konka/musicPlayer/musicPlayerActivity;

    # getter for: Lcom/konka/musicPlayer/musicPlayerActivity;->info:Lcom/konka/musicPlayer/MusicInfo;
    invoke-static {v5}, Lcom/konka/musicPlayer/musicPlayerActivity;->access$0(Lcom/konka/musicPlayer/musicPlayerActivity;)Lcom/konka/musicPlayer/MusicInfo;

    move-result-object v5

    invoke-virtual {v5}, Lcom/konka/musicPlayer/MusicInfo;->getData()Ljava/lang/String;

    move-result-object v5

    invoke-static {v5}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v5

    invoke-direct {v4, v5}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    const-string v5, "&ma"

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    move v4, v3

    invoke-virtual/range {v0 .. v5}, Lcom/konka/musicPlayer/musicPlayerActivity;->sendPlayerStateToPlayerService(Landroid/content/Context;IIILjava/lang/String;)V

    goto :goto_0
.end method
