.class public Lcom/konka/musicPlayer/musicPlayerActivity;
.super Landroid/app/Activity;
.source "musicPlayerActivity.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/konka/musicPlayer/musicPlayerActivity$SeekToReceiver;,
        Lcom/konka/musicPlayer/musicPlayerActivity$closeReceiver;,
        Lcom/konka/musicPlayer/musicPlayerActivity$playerDataReceiver;,
        Lcom/konka/musicPlayer/musicPlayerActivity$switchReceiver;
    }
.end annotation


# static fields
.field private static final CYCLE:I = 0x2

.field private static final REPEAT:I = 0x1

.field private static final SEQUENCE:I = 0x0

.field private static final SHUFFLE:I = 0x3

.field private static final VOLUME_DOWN:I = 0xd4

.field private static final VOLUME_UP:I = 0xd5

.field private static final musicTag:Ljava/lang/String; = "MultiScreeenMusicActivity:"

.field private static final perPageCount:F = 12.0f


# instance fields
.field private artist:Landroid/widget/TextView;

.field private audioManager:Landroid/media/AudioManager;

.field private clickListener:Landroid/view/View$OnClickListener;

.field private comefrom:I

.field public completionListener:Landroid/media/MediaPlayer$OnCompletionListener;

.field private curIndex:I

.field private curPage:I

.field private curPlay:I

.field private curVolume:I

.field private cycleBtn:Landroid/widget/Button;

.field private cycleTxt:Landroid/widget/TextView;

.field dataReceiver:Lcom/konka/musicPlayer/musicPlayerActivity$playerDataReceiver;

.field fileUrl:[B

.field public fileUrlLen:I

.field private handler:Landroid/os/Handler;

.field private haveNextFlag:I

.field private info:Lcom/konka/musicPlayer/MusicInfo;

.field private infos:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Lcom/konka/musicPlayer/MusicInfo;",
            ">;"
        }
    .end annotation
.end field

.field private intentFilter:Landroid/content/IntentFilter;

.field isMusicPlay:Z

.field isPause:Z

.field isPlayerPrepare:Z

.field private isRegistReceiver:Z

.field isSend:Z

.field private isfocusInGrid:Z

.field private listBtn:Landroid/widget/Button;

.field private lrcBtn:Landroid/widget/Button;

.field public lyricView:Lcom/konka/musicPlayer/LyricView;

.field private maxVolume:I

.field mediaPlayerSwitch:Lcom/konka/musicPlayer/musicPlayerActivity$switchReceiver;

.field mediaSwitch:I

.field mediastop:I

.field private minVolume:I

.field private mobileIp:Ljava/lang/String;

.field musicClose:Lcom/konka/musicPlayer/musicPlayerActivity$closeReceiver;

.field private musicList:Lcom/konka/musicPlayer/ScrollLayout;

.field private musicListRl:Landroid/widget/RelativeLayout;

.field private musicListTxt:Landroid/widget/TextView;

.field private musicPath:Ljava/lang/String;

.field private musicPathTemp:Ljava/lang/String;

.field private musicSize:I

.field private musicplaystate:I

.field private muteFlag:Z

.field private nextBtn:Landroid/widget/Button;

.field private nextPage:Landroid/widget/ImageButton;

.field private nextTxt:Landroid/widget/TextView;

.field private numColumns:I

.field private pageCount:I

.field private pauseBtn:Landroid/widget/Button;

.field private perColumnWidth:I

.field private playBtn:Landroid/widget/Button;

.field private playPauseTxt:Landroid/widget/TextView;

.field private playTiem:Landroid/widget/TextView;

.field private player:Landroid/media/MediaPlayer;

.field public playerErrorListener:Landroid/media/MediaPlayer$OnErrorListener;

.field private preBtn:Landroid/widget/Button;

.field private prePage:Landroid/widget/ImageButton;

.field private preTxt:Landroid/widget/TextView;

.field public prepareListerer:Landroid/media/MediaPlayer$OnPreparedListener;

.field private proDlg:Landroid/app/ProgressDialog;

.field private progressSpeed:I

.field relayTimer:I

.field private repeatBtn:Landroid/widget/Button;

.field private repeatTxt:Landroid/widget/TextView;

.field resumeAsk:I

.field private resumePos:I

.field private seekBar:Landroid/widget/SeekBar;

.field private seekBarChangeListener:Landroid/widget/SeekBar$OnSeekBarChangeListener;

.field public seekListener:Landroid/media/MediaPlayer$OnSeekCompleteListener;

.field seekReceiver:Lcom/konka/musicPlayer/musicPlayerActivity$SeekToReceiver;

.field private seekbarOnKey:Landroid/view/View$OnKeyListener;

.field private sequenceBtn:Landroid/widget/Button;

.field private sequenceTxt:Landroid/widget/TextView;

.field private service:Landroid/content/Intent;

.field private shuffleBtn:Landroid/widget/Button;

.field private songname:Landroid/widget/TextView;

.field private time:Landroid/widget/TextView;

.field private type:I

.field private usb_path:Ljava/lang/String;

.field private voice:Landroid/widget/Button;

.field private voiceCount:Landroid/widget/TextView;

.field private voiceDown:Landroid/widget/Button;

.field private voiceDownTxt:Landroid/widget/TextView;

.field private voiceNo:Landroid/widget/Button;

.field private voiceNoTxt:Landroid/widget/TextView;

.field private voiceTxt:Landroid/widget/TextView;

.field private voiceUp:Landroid/widget/Button;

.field private voiceUpTxt:Landroid/widget/TextView;


# direct methods
.method public constructor <init>()V
    .locals 4

    const/4 v3, 0x1

    const/4 v2, 0x0

    const/4 v1, 0x0

    invoke-direct {p0}, Landroid/app/Activity;-><init>()V

    iput-boolean v3, p0, Lcom/konka/musicPlayer/musicPlayerActivity;->isfocusInGrid:Z

    iput-object v2, p0, Lcom/konka/musicPlayer/musicPlayerActivity;->intentFilter:Landroid/content/IntentFilter;

    iput v1, p0, Lcom/konka/musicPlayer/musicPlayerActivity;->curPlay:I

    iput-object v2, p0, Lcom/konka/musicPlayer/musicPlayerActivity;->player:Landroid/media/MediaPlayer;

    const/16 v0, 0x1c2

    iput v0, p0, Lcom/konka/musicPlayer/musicPlayerActivity;->perColumnWidth:I

    iput v1, p0, Lcom/konka/musicPlayer/musicPlayerActivity;->type:I

    iput-object v2, p0, Lcom/konka/musicPlayer/musicPlayerActivity;->usb_path:Ljava/lang/String;

    iput v1, p0, Lcom/konka/musicPlayer/musicPlayerActivity;->musicSize:I

    iput v1, p0, Lcom/konka/musicPlayer/musicPlayerActivity;->pageCount:I

    const/4 v0, 0x3

    iput v0, p0, Lcom/konka/musicPlayer/musicPlayerActivity;->numColumns:I

    iput v1, p0, Lcom/konka/musicPlayer/musicPlayerActivity;->curPage:I

    iput v1, p0, Lcom/konka/musicPlayer/musicPlayerActivity;->curIndex:I

    iput-object v2, p0, Lcom/konka/musicPlayer/musicPlayerActivity;->service:Landroid/content/Intent;

    iput-boolean v1, p0, Lcom/konka/musicPlayer/musicPlayerActivity;->isRegistReceiver:Z

    iput-object v2, p0, Lcom/konka/musicPlayer/musicPlayerActivity;->info:Lcom/konka/musicPlayer/MusicInfo;

    iput v1, p0, Lcom/konka/musicPlayer/musicPlayerActivity;->comefrom:I

    iput-object v2, p0, Lcom/konka/musicPlayer/musicPlayerActivity;->musicPath:Ljava/lang/String;

    iput-object v2, p0, Lcom/konka/musicPlayer/musicPlayerActivity;->musicPathTemp:Ljava/lang/String;

    iput-object v2, p0, Lcom/konka/musicPlayer/musicPlayerActivity;->mobileIp:Ljava/lang/String;

    iput-object v2, p0, Lcom/konka/musicPlayer/musicPlayerActivity;->infos:Ljava/util/List;

    iput-object v2, p0, Lcom/konka/musicPlayer/musicPlayerActivity;->lyricView:Lcom/konka/musicPlayer/LyricView;

    iput-boolean v1, p0, Lcom/konka/musicPlayer/musicPlayerActivity;->muteFlag:Z

    const/16 v0, 0x64

    iput v0, p0, Lcom/konka/musicPlayer/musicPlayerActivity;->maxVolume:I

    iput v1, p0, Lcom/konka/musicPlayer/musicPlayerActivity;->minVolume:I

    iput v1, p0, Lcom/konka/musicPlayer/musicPlayerActivity;->curVolume:I

    const/16 v0, 0x2710

    iput v0, p0, Lcom/konka/musicPlayer/musicPlayerActivity;->progressSpeed:I

    iput v1, p0, Lcom/konka/musicPlayer/musicPlayerActivity;->musicplaystate:I

    iput v1, p0, Lcom/konka/musicPlayer/musicPlayerActivity;->fileUrlLen:I

    const/16 v0, 0x190

    new-array v0, v0, [B

    iput-object v0, p0, Lcom/konka/musicPlayer/musicPlayerActivity;->fileUrl:[B

    iput v1, p0, Lcom/konka/musicPlayer/musicPlayerActivity;->haveNextFlag:I

    iput v1, p0, Lcom/konka/musicPlayer/musicPlayerActivity;->resumePos:I

    iput v3, p0, Lcom/konka/musicPlayer/musicPlayerActivity;->mediastop:I

    iput v3, p0, Lcom/konka/musicPlayer/musicPlayerActivity;->mediaSwitch:I

    iput v1, p0, Lcom/konka/musicPlayer/musicPlayerActivity;->resumeAsk:I

    iput v1, p0, Lcom/konka/musicPlayer/musicPlayerActivity;->relayTimer:I

    iput-boolean v1, p0, Lcom/konka/musicPlayer/musicPlayerActivity;->isSend:Z

    iput-boolean v1, p0, Lcom/konka/musicPlayer/musicPlayerActivity;->isMusicPlay:Z

    iput-boolean v1, p0, Lcom/konka/musicPlayer/musicPlayerActivity;->isPlayerPrepare:Z

    iput-boolean v1, p0, Lcom/konka/musicPlayer/musicPlayerActivity;->isPause:Z

    new-instance v0, Lcom/konka/musicPlayer/musicPlayerActivity$1;

    invoke-direct {v0, p0}, Lcom/konka/musicPlayer/musicPlayerActivity$1;-><init>(Lcom/konka/musicPlayer/musicPlayerActivity;)V

    iput-object v0, p0, Lcom/konka/musicPlayer/musicPlayerActivity;->handler:Landroid/os/Handler;

    new-instance v0, Lcom/konka/musicPlayer/musicPlayerActivity$2;

    invoke-direct {v0, p0}, Lcom/konka/musicPlayer/musicPlayerActivity$2;-><init>(Lcom/konka/musicPlayer/musicPlayerActivity;)V

    iput-object v0, p0, Lcom/konka/musicPlayer/musicPlayerActivity;->clickListener:Landroid/view/View$OnClickListener;

    new-instance v0, Lcom/konka/musicPlayer/musicPlayerActivity$3;

    invoke-direct {v0, p0}, Lcom/konka/musicPlayer/musicPlayerActivity$3;-><init>(Lcom/konka/musicPlayer/musicPlayerActivity;)V

    iput-object v0, p0, Lcom/konka/musicPlayer/musicPlayerActivity;->seekbarOnKey:Landroid/view/View$OnKeyListener;

    new-instance v0, Lcom/konka/musicPlayer/musicPlayerActivity$4;

    invoke-direct {v0, p0}, Lcom/konka/musicPlayer/musicPlayerActivity$4;-><init>(Lcom/konka/musicPlayer/musicPlayerActivity;)V

    iput-object v0, p0, Lcom/konka/musicPlayer/musicPlayerActivity;->seekBarChangeListener:Landroid/widget/SeekBar$OnSeekBarChangeListener;

    new-instance v0, Lcom/konka/musicPlayer/musicPlayerActivity$5;

    invoke-direct {v0, p0}, Lcom/konka/musicPlayer/musicPlayerActivity$5;-><init>(Lcom/konka/musicPlayer/musicPlayerActivity;)V

    iput-object v0, p0, Lcom/konka/musicPlayer/musicPlayerActivity;->prepareListerer:Landroid/media/MediaPlayer$OnPreparedListener;

    new-instance v0, Lcom/konka/musicPlayer/musicPlayerActivity$6;

    invoke-direct {v0, p0}, Lcom/konka/musicPlayer/musicPlayerActivity$6;-><init>(Lcom/konka/musicPlayer/musicPlayerActivity;)V

    iput-object v0, p0, Lcom/konka/musicPlayer/musicPlayerActivity;->seekListener:Landroid/media/MediaPlayer$OnSeekCompleteListener;

    new-instance v0, Lcom/konka/musicPlayer/musicPlayerActivity$7;

    invoke-direct {v0, p0}, Lcom/konka/musicPlayer/musicPlayerActivity$7;-><init>(Lcom/konka/musicPlayer/musicPlayerActivity;)V

    iput-object v0, p0, Lcom/konka/musicPlayer/musicPlayerActivity;->completionListener:Landroid/media/MediaPlayer$OnCompletionListener;

    new-instance v0, Lcom/konka/musicPlayer/musicPlayerActivity$8;

    invoke-direct {v0, p0}, Lcom/konka/musicPlayer/musicPlayerActivity$8;-><init>(Lcom/konka/musicPlayer/musicPlayerActivity;)V

    iput-object v0, p0, Lcom/konka/musicPlayer/musicPlayerActivity;->playerErrorListener:Landroid/media/MediaPlayer$OnErrorListener;

    return-void
.end method

.method static synthetic access$0(Lcom/konka/musicPlayer/musicPlayerActivity;)Lcom/konka/musicPlayer/MusicInfo;
    .locals 1

    iget-object v0, p0, Lcom/konka/musicPlayer/musicPlayerActivity;->info:Lcom/konka/musicPlayer/MusicInfo;

    return-object v0
.end method

.method static synthetic access$1(Lcom/konka/musicPlayer/musicPlayerActivity;II)V
    .locals 0

    invoke-direct {p0, p1, p2}, Lcom/konka/musicPlayer/musicPlayerActivity;->showProgressDialog(II)V

    return-void
.end method

.method static synthetic access$10(Lcom/konka/musicPlayer/musicPlayerActivity;)V
    .locals 0

    invoke-direct {p0}, Lcom/konka/musicPlayer/musicPlayerActivity;->stopMusicService()V

    return-void
.end method

.method static synthetic access$11(Lcom/konka/musicPlayer/musicPlayerActivity;Z)V
    .locals 0

    iput-boolean p1, p0, Lcom/konka/musicPlayer/musicPlayerActivity;->isfocusInGrid:Z

    return-void
.end method

.method static synthetic access$12(Lcom/konka/musicPlayer/musicPlayerActivity;)Z
    .locals 1

    iget-boolean v0, p0, Lcom/konka/musicPlayer/musicPlayerActivity;->isfocusInGrid:Z

    return v0
.end method

.method static synthetic access$13(Lcom/konka/musicPlayer/musicPlayerActivity;Z)V
    .locals 0

    invoke-direct {p0, p1}, Lcom/konka/musicPlayer/musicPlayerActivity;->setBtnFocus(Z)V

    return-void
.end method

.method static synthetic access$14(Lcom/konka/musicPlayer/musicPlayerActivity;)I
    .locals 1

    iget v0, p0, Lcom/konka/musicPlayer/musicPlayerActivity;->musicplaystate:I

    return v0
.end method

.method static synthetic access$15(Lcom/konka/musicPlayer/musicPlayerActivity;)I
    .locals 1

    iget v0, p0, Lcom/konka/musicPlayer/musicPlayerActivity;->progressSpeed:I

    return v0
.end method

.method static synthetic access$16(Lcom/konka/musicPlayer/musicPlayerActivity;I)V
    .locals 0

    invoke-direct {p0, p1}, Lcom/konka/musicPlayer/musicPlayerActivity;->playPos(I)V

    return-void
.end method

.method static synthetic access$17(Lcom/konka/musicPlayer/musicPlayerActivity;)Landroid/content/Intent;
    .locals 1

    iget-object v0, p0, Lcom/konka/musicPlayer/musicPlayerActivity;->service:Landroid/content/Intent;

    return-object v0
.end method

.method static synthetic access$18(Lcom/konka/musicPlayer/musicPlayerActivity;)Ljava/lang/String;
    .locals 1

    iget-object v0, p0, Lcom/konka/musicPlayer/musicPlayerActivity;->musicPath:Ljava/lang/String;

    return-object v0
.end method

.method static synthetic access$19(Lcom/konka/musicPlayer/musicPlayerActivity;)Ljava/lang/String;
    .locals 1

    iget-object v0, p0, Lcom/konka/musicPlayer/musicPlayerActivity;->musicPathTemp:Ljava/lang/String;

    return-object v0
.end method

.method static synthetic access$2(Lcom/konka/musicPlayer/musicPlayerActivity;Lcom/konka/musicPlayer/MusicInfo;)V
    .locals 0

    invoke-direct {p0, p1}, Lcom/konka/musicPlayer/musicPlayerActivity;->initBarInfo(Lcom/konka/musicPlayer/MusicInfo;)V

    return-void
.end method

.method static synthetic access$20(Lcom/konka/musicPlayer/musicPlayerActivity;Ljava/lang/String;)V
    .locals 0

    iput-object p1, p0, Lcom/konka/musicPlayer/musicPlayerActivity;->musicPath:Ljava/lang/String;

    return-void
.end method

.method static synthetic access$21(Lcom/konka/musicPlayer/musicPlayerActivity;)Landroid/os/Handler;
    .locals 1

    iget-object v0, p0, Lcom/konka/musicPlayer/musicPlayerActivity;->handler:Landroid/os/Handler;

    return-object v0
.end method

.method static synthetic access$22(Lcom/konka/musicPlayer/musicPlayerActivity;I)V
    .locals 0

    iput p1, p0, Lcom/konka/musicPlayer/musicPlayerActivity;->resumePos:I

    return-void
.end method

.method static synthetic access$23(Lcom/konka/musicPlayer/musicPlayerActivity;)I
    .locals 1

    iget v0, p0, Lcom/konka/musicPlayer/musicPlayerActivity;->haveNextFlag:I

    return v0
.end method

.method static synthetic access$24(Lcom/konka/musicPlayer/musicPlayerActivity;I)V
    .locals 0

    iput p1, p0, Lcom/konka/musicPlayer/musicPlayerActivity;->haveNextFlag:I

    return-void
.end method

.method static synthetic access$25(Lcom/konka/musicPlayer/musicPlayerActivity;Ljava/lang/String;)V
    .locals 0

    iput-object p1, p0, Lcom/konka/musicPlayer/musicPlayerActivity;->musicPathTemp:Ljava/lang/String;

    return-void
.end method

.method static synthetic access$26(Lcom/konka/musicPlayer/musicPlayerActivity;Ljava/lang/String;)V
    .locals 0

    iput-object p1, p0, Lcom/konka/musicPlayer/musicPlayerActivity;->mobileIp:Ljava/lang/String;

    return-void
.end method

.method static synthetic access$27(Lcom/konka/musicPlayer/musicPlayerActivity;Lcom/konka/musicPlayer/MusicInfo;)V
    .locals 0

    iput-object p1, p0, Lcom/konka/musicPlayer/musicPlayerActivity;->info:Lcom/konka/musicPlayer/MusicInfo;

    return-void
.end method

.method static synthetic access$28(Lcom/konka/musicPlayer/musicPlayerActivity;)V
    .locals 0

    invoke-direct {p0}, Lcom/konka/musicPlayer/musicPlayerActivity;->registerDataReceiver()V

    return-void
.end method

.method static synthetic access$3(Lcom/konka/musicPlayer/musicPlayerActivity;ILcom/konka/musicPlayer/MusicInfo;)V
    .locals 0

    invoke-direct {p0, p1, p2}, Lcom/konka/musicPlayer/musicPlayerActivity;->playMusicService(ILcom/konka/musicPlayer/MusicInfo;)V

    return-void
.end method

.method static synthetic access$4(Lcom/konka/musicPlayer/musicPlayerActivity;)I
    .locals 1

    iget v0, p0, Lcom/konka/musicPlayer/musicPlayerActivity;->resumePos:I

    return v0
.end method

.method static synthetic access$5(Lcom/konka/musicPlayer/musicPlayerActivity;)Landroid/widget/Button;
    .locals 1

    iget-object v0, p0, Lcom/konka/musicPlayer/musicPlayerActivity;->playBtn:Landroid/widget/Button;

    return-object v0
.end method

.method static synthetic access$6(Lcom/konka/musicPlayer/musicPlayerActivity;)Landroid/widget/Button;
    .locals 1

    iget-object v0, p0, Lcom/konka/musicPlayer/musicPlayerActivity;->pauseBtn:Landroid/widget/Button;

    return-object v0
.end method

.method static synthetic access$7(Lcom/konka/musicPlayer/musicPlayerActivity;)Landroid/media/MediaPlayer;
    .locals 1

    iget-object v0, p0, Lcom/konka/musicPlayer/musicPlayerActivity;->player:Landroid/media/MediaPlayer;

    return-object v0
.end method

.method static synthetic access$8(Lcom/konka/musicPlayer/musicPlayerActivity;I)V
    .locals 0

    iput p1, p0, Lcom/konka/musicPlayer/musicPlayerActivity;->musicplaystate:I

    return-void
.end method

.method static synthetic access$9(Lcom/konka/musicPlayer/musicPlayerActivity;)Landroid/widget/SeekBar;
    .locals 1

    iget-object v0, p0, Lcom/konka/musicPlayer/musicPlayerActivity;->seekBar:Landroid/widget/SeekBar;

    return-object v0
.end method

.method private findViews()V
    .locals 1

    const v0, 0x7f0b0002    # com.konka.mediaSharePlayer.R.id.music_scrollLayout

    invoke-virtual {p0, v0}, Lcom/konka/musicPlayer/musicPlayerActivity;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/konka/musicPlayer/ScrollLayout;

    iput-object v0, p0, Lcom/konka/musicPlayer/musicPlayerActivity;->musicList:Lcom/konka/musicPlayer/ScrollLayout;

    const v0, 0x7f0b0009    # com.konka.mediaSharePlayer.R.id.seekbar

    invoke-virtual {p0, v0}, Lcom/konka/musicPlayer/musicPlayerActivity;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/SeekBar;

    iput-object v0, p0, Lcom/konka/musicPlayer/musicPlayerActivity;->seekBar:Landroid/widget/SeekBar;

    const v0, 0x7f0b000a    # com.konka.mediaSharePlayer.R.id.tv_playbar_time_all

    invoke-virtual {p0, v0}, Lcom/konka/musicPlayer/musicPlayerActivity;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    iput-object v0, p0, Lcom/konka/musicPlayer/musicPlayerActivity;->time:Landroid/widget/TextView;

    const v0, 0x7f0b0008    # com.konka.mediaSharePlayer.R.id.tv_playbar_time

    invoke-virtual {p0, v0}, Lcom/konka/musicPlayer/musicPlayerActivity;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    iput-object v0, p0, Lcom/konka/musicPlayer/musicPlayerActivity;->playTiem:Landroid/widget/TextView;

    const v0, 0x7f0b0003    # com.konka.mediaSharePlayer.R.id.tv_lrcText

    invoke-virtual {p0, v0}, Lcom/konka/musicPlayer/musicPlayerActivity;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/konka/musicPlayer/LyricView;

    iput-object v0, p0, Lcom/konka/musicPlayer/musicPlayerActivity;->lyricView:Lcom/konka/musicPlayer/LyricView;

    const-string v0, "audio"

    invoke-virtual {p0, v0}, Lcom/konka/musicPlayer/musicPlayerActivity;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/media/AudioManager;

    iput-object v0, p0, Lcom/konka/musicPlayer/musicPlayerActivity;->audioManager:Landroid/media/AudioManager;

    invoke-direct {p0}, Lcom/konka/musicPlayer/musicPlayerActivity;->initBtns()V

    return-void
.end method

.method private initBarInfo(Lcom/konka/musicPlayer/MusicInfo;)V
    .locals 4
    .param p1    # Lcom/konka/musicPlayer/MusicInfo;

    if-eqz p1, :cond_0

    const-string v1, "MultiScreeenMusicActivity:"

    new-instance v2, Ljava/lang/StringBuilder;

    const-string v3, "initBarInfo info :"

    invoke-direct {v2, v3}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {p1}, Lcom/konka/musicPlayer/MusicInfo;->getData()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    invoke-virtual {p1}, Lcom/konka/musicPlayer/MusicInfo;->getDesplayName()Ljava/lang/String;

    move-result-object v1

    invoke-static {v1}, Ljava/net/URLDecoder;->decode(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    iget-object v1, p0, Lcom/konka/musicPlayer/musicPlayerActivity;->songname:Landroid/widget/TextView;

    invoke-virtual {v1, v0}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    iget-object v1, p0, Lcom/konka/musicPlayer/musicPlayerActivity;->songname:Landroid/widget/TextView;

    const/high16 v2, 0x41f00000    # 30.0f

    invoke-virtual {v1, v2}, Landroid/widget/TextView;->setTextSize(F)V

    :goto_0
    return-void

    :cond_0
    iget-object v1, p0, Lcom/konka/musicPlayer/musicPlayerActivity;->songname:Landroid/widget/TextView;

    const-string v2, ""

    invoke-virtual {v1, v2}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    goto :goto_0
.end method

.method private initBtns()V
    .locals 3

    const/4 v2, 0x1

    const v0, 0x7f0b0005    # com.konka.mediaSharePlayer.R.id.btn_music_play

    invoke-virtual {p0, v0}, Lcom/konka/musicPlayer/musicPlayerActivity;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/Button;

    iput-object v0, p0, Lcom/konka/musicPlayer/musicPlayerActivity;->playBtn:Landroid/widget/Button;

    const v0, 0x7f0b0006    # com.konka.mediaSharePlayer.R.id.btn_music_pause

    invoke-virtual {p0, v0}, Lcom/konka/musicPlayer/musicPlayerActivity;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/Button;

    iput-object v0, p0, Lcom/konka/musicPlayer/musicPlayerActivity;->pauseBtn:Landroid/widget/Button;

    const v0, 0x7f0b0004    # com.konka.mediaSharePlayer.R.id.btn_seekback

    invoke-virtual {p0, v0}, Lcom/konka/musicPlayer/musicPlayerActivity;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/Button;

    iput-object v0, p0, Lcom/konka/musicPlayer/musicPlayerActivity;->preBtn:Landroid/widget/Button;

    const v0, 0x7f0b0007    # com.konka.mediaSharePlayer.R.id.btn_seekforward

    invoke-virtual {p0, v0}, Lcom/konka/musicPlayer/musicPlayerActivity;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/Button;

    iput-object v0, p0, Lcom/konka/musicPlayer/musicPlayerActivity;->nextBtn:Landroid/widget/Button;

    iget-object v0, p0, Lcom/konka/musicPlayer/musicPlayerActivity;->pauseBtn:Landroid/widget/Button;

    invoke-virtual {v0, v2}, Landroid/widget/Button;->setFocusable(Z)V

    iget-object v0, p0, Lcom/konka/musicPlayer/musicPlayerActivity;->pauseBtn:Landroid/widget/Button;

    invoke-virtual {v0}, Landroid/widget/Button;->requestFocus()Z

    iget-object v0, p0, Lcom/konka/musicPlayer/musicPlayerActivity;->pauseBtn:Landroid/widget/Button;

    const v1, 0x7f020015    # com.konka.mediaSharePlayer.R.drawable.com_pause_sel

    invoke-virtual {v0, v1}, Landroid/widget/Button;->setBackgroundResource(I)V

    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/konka/musicPlayer/musicPlayerActivity;->isPause:Z

    iput v2, p0, Lcom/konka/musicPlayer/musicPlayerActivity;->musicplaystate:I

    const v0, 0x7f0b0001    # com.konka.mediaSharePlayer.R.id.playbar_songname

    invoke-virtual {p0, v0}, Lcom/konka/musicPlayer/musicPlayerActivity;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    iput-object v0, p0, Lcom/konka/musicPlayer/musicPlayerActivity;->songname:Landroid/widget/TextView;

    iget-object v0, p0, Lcom/konka/musicPlayer/musicPlayerActivity;->playBtn:Landroid/widget/Button;

    iget-object v1, p0, Lcom/konka/musicPlayer/musicPlayerActivity;->clickListener:Landroid/view/View$OnClickListener;

    invoke-virtual {v0, v1}, Landroid/widget/Button;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    iget-object v0, p0, Lcom/konka/musicPlayer/musicPlayerActivity;->pauseBtn:Landroid/widget/Button;

    iget-object v1, p0, Lcom/konka/musicPlayer/musicPlayerActivity;->clickListener:Landroid/view/View$OnClickListener;

    invoke-virtual {v0, v1}, Landroid/widget/Button;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    iget-object v0, p0, Lcom/konka/musicPlayer/musicPlayerActivity;->seekBar:Landroid/widget/SeekBar;

    iget-object v1, p0, Lcom/konka/musicPlayer/musicPlayerActivity;->clickListener:Landroid/view/View$OnClickListener;

    invoke-virtual {v0, v1}, Landroid/widget/SeekBar;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    invoke-direct {p0}, Lcom/konka/musicPlayer/musicPlayerActivity;->setBtnsOnFocusListeners()V

    return-void
.end method

.method private playMusicService(ILcom/konka/musicPlayer/MusicInfo;)V
    .locals 3
    .param p1    # I
    .param p2    # Lcom/konka/musicPlayer/MusicInfo;

    new-instance v0, Landroid/content/Intent;

    const-class v1, Lcom/konka/musicPlayer/MusicPlayService;

    invoke-direct {v0, p0, v1}, Landroid/content/Intent;-><init>(Landroid/content/Context;Ljava/lang/Class;)V

    iput-object v0, p0, Lcom/konka/musicPlayer/musicPlayerActivity;->service:Landroid/content/Intent;

    iget-object v0, p0, Lcom/konka/musicPlayer/musicPlayerActivity;->service:Landroid/content/Intent;

    const-string v1, "MSG"

    invoke-virtual {v0, v1, p1}, Landroid/content/Intent;->putExtra(Ljava/lang/String;I)Landroid/content/Intent;

    iget-object v0, p0, Lcom/konka/musicPlayer/musicPlayerActivity;->service:Landroid/content/Intent;

    const-string v1, "CurrentTime"

    iget v2, p0, Lcom/konka/musicPlayer/musicPlayerActivity;->resumePos:I

    invoke-virtual {v0, v1, v2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;I)Landroid/content/Intent;

    const-string v0, "noob"

    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "music service: "

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    iget v2, p0, Lcom/konka/musicPlayer/musicPlayerActivity;->resumePos:I

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    iget-object v0, p0, Lcom/konka/musicPlayer/musicPlayerActivity;->service:Landroid/content/Intent;

    const-string v1, "info"

    invoke-virtual {v0, v1, p2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/io/Serializable;)Landroid/content/Intent;

    const-string v0, "MultiScreeenMusicActivity:"

    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, " MSG:"

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, "-----info:"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {p2}, Lcom/konka/musicPlayer/MusicInfo;->getData()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    invoke-static {p0}, Lcom/konka/musicPlayer/MusicPlayService;->addActivity(Lcom/konka/musicPlayer/musicPlayerActivity;)V

    iget-object v0, p0, Lcom/konka/musicPlayer/musicPlayerActivity;->service:Landroid/content/Intent;

    invoke-virtual {p0, v0}, Lcom/konka/musicPlayer/musicPlayerActivity;->startService(Landroid/content/Intent;)Landroid/content/ComponentName;

    return-void
.end method

.method private playPos(I)V
    .locals 3
    .param p1    # I

    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/konka/musicPlayer/musicPlayerActivity;->isSend:Z

    const/4 v0, 0x0

    iput v0, p0, Lcom/konka/musicPlayer/musicPlayerActivity;->relayTimer:I

    iget-object v0, p0, Lcom/konka/musicPlayer/musicPlayerActivity;->service:Landroid/content/Intent;

    const-string v1, "MSG"

    const/4 v2, 0x6

    invoke-virtual {v0, v1, v2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;I)Landroid/content/Intent;

    iget-object v0, p0, Lcom/konka/musicPlayer/musicPlayerActivity;->service:Landroid/content/Intent;

    const-string v1, "progress"

    invoke-virtual {v0, v1, p1}, Landroid/content/Intent;->putExtra(Ljava/lang/String;I)Landroid/content/Intent;

    invoke-static {p0}, Lcom/konka/musicPlayer/MusicPlayService;->addActivity(Lcom/konka/musicPlayer/musicPlayerActivity;)V

    iget-object v0, p0, Lcom/konka/musicPlayer/musicPlayerActivity;->service:Landroid/content/Intent;

    invoke-virtual {p0, v0}, Lcom/konka/musicPlayer/musicPlayerActivity;->startService(Landroid/content/Intent;)Landroid/content/ComponentName;

    return-void
.end method

.method private registerDataReceiver()V
    .locals 3

    new-instance v1, Lcom/konka/musicPlayer/musicPlayerActivity$playerDataReceiver;

    const/4 v2, 0x0

    invoke-direct {v1, p0, v2}, Lcom/konka/musicPlayer/musicPlayerActivity$playerDataReceiver;-><init>(Lcom/konka/musicPlayer/musicPlayerActivity;Lcom/konka/musicPlayer/musicPlayerActivity$playerDataReceiver;)V

    iput-object v1, p0, Lcom/konka/musicPlayer/musicPlayerActivity;->dataReceiver:Lcom/konka/musicPlayer/musicPlayerActivity$playerDataReceiver;

    new-instance v0, Landroid/content/IntentFilter;

    invoke-direct {v0}, Landroid/content/IntentFilter;-><init>()V

    const-string v1, "com.konka.MultiScreenPlayer.musicPlayer"

    invoke-virtual {v0, v1}, Landroid/content/IntentFilter;->addAction(Ljava/lang/String;)V

    iget-object v1, p0, Lcom/konka/musicPlayer/musicPlayerActivity;->dataReceiver:Lcom/konka/musicPlayer/musicPlayerActivity$playerDataReceiver;

    invoke-virtual {p0, v1, v0}, Lcom/konka/musicPlayer/musicPlayerActivity;->registerReceiver(Landroid/content/BroadcastReceiver;Landroid/content/IntentFilter;)Landroid/content/Intent;

    return-void
.end method

.method private setBtnFocus(Z)V
    .locals 1
    .param p1    # Z

    iget-object v0, p0, Lcom/konka/musicPlayer/musicPlayerActivity;->playBtn:Landroid/widget/Button;

    invoke-virtual {v0, p1}, Landroid/widget/Button;->setFocusable(Z)V

    iget-object v0, p0, Lcom/konka/musicPlayer/musicPlayerActivity;->pauseBtn:Landroid/widget/Button;

    invoke-virtual {v0, p1}, Landroid/widget/Button;->setFocusable(Z)V

    iget-object v0, p0, Lcom/konka/musicPlayer/musicPlayerActivity;->seekBar:Landroid/widget/SeekBar;

    invoke-virtual {v0, p1}, Landroid/widget/SeekBar;->setFocusable(Z)V

    return-void
.end method

.method private setBtnsOnFocusListeners()V
    .locals 0

    return-void
.end method

.method private setDialogText(Landroid/view/View;)V
    .locals 5
    .param p1    # Landroid/view/View;

    instance-of v4, p1, Landroid/view/ViewGroup;

    if-eqz v4, :cond_2

    move-object v3, p1

    check-cast v3, Landroid/view/ViewGroup;

    const/high16 v4, -0x1000000

    invoke-virtual {p1, v4}, Landroid/view/View;->setBackgroundColor(I)V

    invoke-virtual {v3}, Landroid/view/ViewGroup;->getChildCount()I

    move-result v1

    const/4 v2, 0x0

    :goto_0
    if-lt v2, v1, :cond_1

    :cond_0
    :goto_1
    return-void

    :cond_1
    invoke-virtual {v3, v2}, Landroid/view/ViewGroup;->getChildAt(I)Landroid/view/View;

    move-result-object v0

    invoke-direct {p0, v0}, Lcom/konka/musicPlayer/musicPlayerActivity;->setDialogText(Landroid/view/View;)V

    add-int/lit8 v2, v2, 0x1

    goto :goto_0

    :cond_2
    instance-of v4, p1, Landroid/widget/TextView;

    if-eqz v4, :cond_0

    check-cast p1, Landroid/widget/TextView;

    const/high16 v4, 0x41700000    # 15.0f

    invoke-virtual {p1, v4}, Landroid/widget/TextView;->setTextSize(F)V

    goto :goto_1
.end method

.method private showProgressDialog(II)V
    .locals 3
    .param p1    # I
    .param p2    # I

    iget-object v1, p0, Lcom/konka/musicPlayer/musicPlayerActivity;->proDlg:Landroid/app/ProgressDialog;

    if-nez v1, :cond_0

    new-instance v1, Landroid/app/ProgressDialog;

    invoke-direct {v1, p0}, Landroid/app/ProgressDialog;-><init>(Landroid/content/Context;)V

    iput-object v1, p0, Lcom/konka/musicPlayer/musicPlayerActivity;->proDlg:Landroid/app/ProgressDialog;

    :cond_0
    iget-object v1, p0, Lcom/konka/musicPlayer/musicPlayerActivity;->proDlg:Landroid/app/ProgressDialog;

    invoke-virtual {p0}, Lcom/konka/musicPlayer/musicPlayerActivity;->getResources()Landroid/content/res/Resources;

    move-result-object v2

    invoke-virtual {v2, p2}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Landroid/app/ProgressDialog;->setMessage(Ljava/lang/CharSequence;)V

    iget-object v1, p0, Lcom/konka/musicPlayer/musicPlayerActivity;->proDlg:Landroid/app/ProgressDialog;

    invoke-virtual {v1}, Landroid/app/ProgressDialog;->show()V

    iget-object v1, p0, Lcom/konka/musicPlayer/musicPlayerActivity;->proDlg:Landroid/app/ProgressDialog;

    invoke-virtual {v1}, Landroid/app/ProgressDialog;->getWindow()Landroid/view/Window;

    move-result-object v1

    invoke-virtual {v1}, Landroid/view/Window;->getDecorView()Landroid/view/View;

    move-result-object v0

    invoke-direct {p0, v0}, Lcom/konka/musicPlayer/musicPlayerActivity;->setDialogText(Landroid/view/View;)V

    return-void
.end method

.method private stopMusicService()V
    .locals 1

    iget-object v0, p0, Lcom/konka/musicPlayer/musicPlayerActivity;->service:Landroid/content/Intent;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/konka/musicPlayer/musicPlayerActivity;->service:Landroid/content/Intent;

    invoke-virtual {p0, v0}, Lcom/konka/musicPlayer/musicPlayerActivity;->stopService(Landroid/content/Intent;)Z

    const/4 v0, 0x0

    iput-object v0, p0, Lcom/konka/musicPlayer/musicPlayerActivity;->service:Landroid/content/Intent;

    :cond_0
    return-void
.end method


# virtual methods
.method public broweToRoot()V
    .locals 6

    const/4 v3, 0x0

    const/4 v5, 0x1

    const-string v1, "MultiScreeenMusicActivity:"

    const-string v2, " broweToRoot()"

    invoke-static {v1, v2}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    iget-object v1, p0, Lcom/konka/musicPlayer/musicPlayerActivity;->pauseBtn:Landroid/widget/Button;

    invoke-virtual {v1, v5}, Landroid/widget/Button;->setFocusable(Z)V

    iget-object v1, p0, Lcom/konka/musicPlayer/musicPlayerActivity;->pauseBtn:Landroid/widget/Button;

    invoke-virtual {v1}, Landroid/widget/Button;->requestFocus()Z

    invoke-virtual {p0}, Lcom/konka/musicPlayer/musicPlayerActivity;->getIntent()Landroid/content/Intent;

    move-result-object v0

    const-string v1, "haveNextFlag"

    invoke-virtual {v0, v1, v3}, Landroid/content/Intent;->getIntExtra(Ljava/lang/String;I)I

    move-result v1

    iput v1, p0, Lcom/konka/musicPlayer/musicPlayerActivity;->haveNextFlag:I

    const-string v1, "musicPath"

    invoke-virtual {v0, v1}, Landroid/content/Intent;->getStringExtra(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    iput-object v1, p0, Lcom/konka/musicPlayer/musicPlayerActivity;->musicPath:Ljava/lang/String;

    iget-object v1, p0, Lcom/konka/musicPlayer/musicPlayerActivity;->musicPath:Ljava/lang/String;

    iput-object v1, p0, Lcom/konka/musicPlayer/musicPlayerActivity;->musicPathTemp:Ljava/lang/String;

    const-string v1, "mediastop"

    invoke-virtual {v0, v1, v5}, Landroid/content/Intent;->getIntExtra(Ljava/lang/String;I)I

    move-result v1

    iput v1, p0, Lcom/konka/musicPlayer/musicPlayerActivity;->mediastop:I

    const-string v1, "mobileIp"

    invoke-virtual {v0, v1}, Landroid/content/Intent;->getStringExtra(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    iput-object v1, p0, Lcom/konka/musicPlayer/musicPlayerActivity;->mobileIp:Ljava/lang/String;

    const-string v1, "resumePos"

    invoke-virtual {v0, v1, v3}, Landroid/content/Intent;->getIntExtra(Ljava/lang/String;I)I

    move-result v1

    iput v1, p0, Lcom/konka/musicPlayer/musicPlayerActivity;->resumePos:I

    new-instance v1, Lcom/konka/musicPlayer/MusicInfo;

    invoke-direct {v1}, Lcom/konka/musicPlayer/MusicInfo;-><init>()V

    iput-object v1, p0, Lcom/konka/musicPlayer/musicPlayerActivity;->info:Lcom/konka/musicPlayer/MusicInfo;

    iget-object v1, p0, Lcom/konka/musicPlayer/musicPlayerActivity;->info:Lcom/konka/musicPlayer/MusicInfo;

    iget-object v2, p0, Lcom/konka/musicPlayer/musicPlayerActivity;->musicPath:Ljava/lang/String;

    invoke-virtual {v1, v2}, Lcom/konka/musicPlayer/MusicInfo;->setData(Ljava/lang/String;)V

    iget-object v1, p0, Lcom/konka/musicPlayer/musicPlayerActivity;->info:Lcom/konka/musicPlayer/MusicInfo;

    iget-object v2, p0, Lcom/konka/musicPlayer/musicPlayerActivity;->musicPath:Ljava/lang/String;

    iget-object v3, p0, Lcom/konka/musicPlayer/musicPlayerActivity;->musicPath:Ljava/lang/String;

    const-string v4, "/"

    invoke-virtual {v3, v4}, Ljava/lang/String;->lastIndexOf(Ljava/lang/String;)I

    move-result v3

    add-int/lit8 v3, v3, 0x1

    iget-object v4, p0, Lcom/konka/musicPlayer/musicPlayerActivity;->musicPath:Ljava/lang/String;

    invoke-virtual {v4}, Ljava/lang/String;->length()I

    move-result v4

    invoke-virtual {v2, v3, v4}, Ljava/lang/String;->substring(II)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Lcom/konka/musicPlayer/MusicInfo;->setDesplayName(Ljava/lang/String;)V

    iget-object v1, p0, Lcom/konka/musicPlayer/musicPlayerActivity;->info:Lcom/konka/musicPlayer/MusicInfo;

    iget-object v2, p0, Lcom/konka/musicPlayer/musicPlayerActivity;->musicPath:Ljava/lang/String;

    iget-object v3, p0, Lcom/konka/musicPlayer/musicPlayerActivity;->musicPath:Ljava/lang/String;

    const-string v4, "/"

    invoke-virtual {v3, v4}, Ljava/lang/String;->lastIndexOf(Ljava/lang/String;)I

    move-result v3

    add-int/lit8 v3, v3, 0x1

    iget-object v4, p0, Lcom/konka/musicPlayer/musicPlayerActivity;->musicPath:Ljava/lang/String;

    invoke-virtual {v4}, Ljava/lang/String;->length()I

    move-result v4

    invoke-virtual {v2, v3, v4}, Ljava/lang/String;->substring(II)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Lcom/konka/musicPlayer/MusicInfo;->setTitle(Ljava/lang/String;)V

    iget-object v1, p0, Lcom/konka/musicPlayer/musicPlayerActivity;->handler:Landroid/os/Handler;

    invoke-virtual {v1, v5}, Landroid/os/Handler;->sendEmptyMessage(I)Z

    return-void
.end method

.method public cancelProgressDlg()V
    .locals 2

    const-string v0, "MultiScreeenMusicActivity:"

    const-string v1, "cancel progress dialog!!!"

    invoke-static {v0, v1}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    iget-object v0, p0, Lcom/konka/musicPlayer/musicPlayerActivity;->proDlg:Landroid/app/ProgressDialog;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/konka/musicPlayer/musicPlayerActivity;->proDlg:Landroid/app/ProgressDialog;

    invoke-virtual {v0}, Landroid/app/ProgressDialog;->dismiss()V

    const/4 v0, 0x0

    iput-object v0, p0, Lcom/konka/musicPlayer/musicPlayerActivity;->proDlg:Landroid/app/ProgressDialog;

    :cond_0
    return-void
.end method

.method public encodeMusicUri()Ljava/lang/String;
    .locals 8

    const/4 v2, 0x0

    const/4 v4, 0x0

    const/4 v3, 0x0

    const/4 v0, 0x0

    const-string v5, "info.getdata"

    new-instance v6, Ljava/lang/StringBuilder;

    const-string v7, "info.getdata: "

    invoke-direct {v6, v7}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    iget-object v7, p0, Lcom/konka/musicPlayer/musicPlayerActivity;->info:Lcom/konka/musicPlayer/MusicInfo;

    invoke-virtual {v7}, Lcom/konka/musicPlayer/MusicInfo;->getData()Ljava/lang/String;

    move-result-object v7

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v6}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v6

    invoke-static {v5, v6}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    const-string v5, "httpstr"

    new-instance v6, Ljava/lang/StringBuilder;

    const-string v7, "httpstr111111: "

    invoke-direct {v6, v7}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v6, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v6}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v6

    invoke-static {v5, v6}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    const-string v5, "picname"

    new-instance v6, Ljava/lang/StringBuilder;

    const-string v7, "picname111111: "

    invoke-direct {v6, v7}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v6, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v6}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v6

    invoke-static {v5, v6}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    const-string v5, "MultiScreeenMusicActivity:"

    new-instance v6, Ljava/lang/StringBuilder;

    const-string v7, "playMusicService MSG========="

    invoke-direct {v6, v7}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    iget v7, p0, Lcom/konka/musicPlayer/musicPlayerActivity;->resumePos:I

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v6}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v6

    invoke-static {v5, v6}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    iget-object v5, p0, Lcom/konka/musicPlayer/musicPlayerActivity;->info:Lcom/konka/musicPlayer/MusicInfo;

    invoke-virtual {v5}, Lcom/konka/musicPlayer/MusicInfo;->getData()Ljava/lang/String;

    move-result-object v4

    const-string v5, "8085/"

    invoke-virtual {v4, v5}, Ljava/lang/String;->indexOf(Ljava/lang/String;)I

    move-result v5

    add-int/lit8 v5, v5, 0x5

    invoke-virtual {v4}, Ljava/lang/String;->length()I

    move-result v6

    invoke-virtual {v4, v5, v6}, Ljava/lang/String;->substring(II)Ljava/lang/String;

    move-result-object v3

    const/4 v5, 0x0

    const-string v6, "8085/"

    invoke-virtual {v4, v6}, Ljava/lang/String;->indexOf(Ljava/lang/String;)I

    move-result v6

    add-int/lit8 v6, v6, 0x5

    invoke-virtual {v4, v5, v6}, Ljava/lang/String;->substring(II)Ljava/lang/String;

    move-result-object v0

    const-string v5, "httpstr"

    new-instance v6, Ljava/lang/StringBuilder;

    const-string v7, "httpstr: "

    invoke-direct {v6, v7}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v6, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v6}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v6

    invoke-static {v5, v6}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    const-string v5, "picname"

    new-instance v6, Ljava/lang/StringBuilder;

    const-string v7, "picname: "

    invoke-direct {v6, v7}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v6, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v6}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v6

    invoke-static {v5, v6}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    invoke-static {v3}, Lcom/konka/mediaSharePlayer/encodeUrl;->encodeUrl(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    new-instance v5, Ljava/lang/StringBuilder;

    invoke-static {v0}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v6

    invoke-direct {v5, v6}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v5, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    const-string v5, "newurl"

    new-instance v6, Ljava/lang/StringBuilder;

    const-string v7, "newurl: "

    invoke-direct {v6, v7}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v6, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v6}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v6

    invoke-static {v5, v6}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    return-object v2
.end method

.method public initSeekBar()V
    .locals 3

    const/4 v2, 0x0

    iget-object v0, p0, Lcom/konka/musicPlayer/musicPlayerActivity;->seekBar:Landroid/widget/SeekBar;

    invoke-virtual {v0, v2}, Landroid/widget/SeekBar;->setProgress(I)V

    iget-object v0, p0, Lcom/konka/musicPlayer/musicPlayerActivity;->playTiem:Landroid/widget/TextView;

    iget v1, p0, Lcom/konka/musicPlayer/musicPlayerActivity;->resumePos:I

    invoke-static {v1}, Lcom/konka/musicPlayer/MusicTool;->calculateDuration(I)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    iget-object v0, p0, Lcom/konka/musicPlayer/musicPlayerActivity;->time:Landroid/widget/TextView;

    invoke-static {v2}, Lcom/konka/musicPlayer/MusicTool;->calculateDuration(I)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    return-void
.end method

.method public musicInit()V
    .locals 2

    const-string v0, "MultiScreeenMusicActivity:"

    const-string v1, "musicInit()"

    invoke-static {v0, v1}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    invoke-direct {p0}, Lcom/konka/musicPlayer/musicPlayerActivity;->findViews()V

    iget-object v0, p0, Lcom/konka/musicPlayer/musicPlayerActivity;->seekBar:Landroid/widget/SeekBar;

    iget-object v1, p0, Lcom/konka/musicPlayer/musicPlayerActivity;->seekBarChangeListener:Landroid/widget/SeekBar$OnSeekBarChangeListener;

    invoke-virtual {v0, v1}, Landroid/widget/SeekBar;->setOnSeekBarChangeListener(Landroid/widget/SeekBar$OnSeekBarChangeListener;)V

    iget-object v0, p0, Lcom/konka/musicPlayer/musicPlayerActivity;->seekBar:Landroid/widget/SeekBar;

    iget-object v1, p0, Lcom/konka/musicPlayer/musicPlayerActivity;->seekbarOnKey:Landroid/view/View$OnKeyListener;

    invoke-virtual {v0, v1}, Landroid/widget/SeekBar;->setOnKeyListener(Landroid/view/View$OnKeyListener;)V

    invoke-virtual {p0}, Lcom/konka/musicPlayer/musicPlayerActivity;->broweToRoot()V

    return-void
.end method

.method public musicPlayerStop()V
    .locals 1

    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/konka/musicPlayer/musicPlayerActivity;->isPlayerPrepare:Z

    invoke-virtual {p0}, Lcom/konka/musicPlayer/musicPlayerActivity;->cancelProgressDlg()V

    invoke-direct {p0}, Lcom/konka/musicPlayer/musicPlayerActivity;->stopMusicService()V

    invoke-static {v0}, Ljava/lang/System;->exit(I)V

    return-void
.end method

.method protected onCreate(Landroid/os/Bundle;)V
    .locals 4
    .param p1    # Landroid/os/Bundle;

    const/16 v3, 0x400

    const/4 v2, 0x1

    const/4 v1, 0x0

    invoke-super {p0, p1}, Landroid/app/Activity;->onCreate(Landroid/os/Bundle;)V

    invoke-virtual {p0, v2}, Lcom/konka/musicPlayer/musicPlayerActivity;->requestWindowFeature(I)Z

    invoke-virtual {p0}, Lcom/konka/musicPlayer/musicPlayerActivity;->getWindow()Landroid/view/Window;

    move-result-object v0

    invoke-virtual {v0, v3, v3}, Landroid/view/Window;->setFlags(II)V

    const/high16 v0, 0x7f030000    # com.konka.mediaSharePlayer.R.layout.music_1280x720

    invoke-virtual {p0, v0}, Lcom/konka/musicPlayer/musicPlayerActivity;->setContentView(I)V

    invoke-static {p0, v1, v2, v1}, Lcom/konka/mediaSharePlayer/sendPlayerState;->MediaPlayerActionSendBroadcastIsCreat(Landroid/content/Context;III)V

    const-string v0, "noob"

    const-string v1, "onCreate-----------------@@@@!"

    invoke-static {v0, v1}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    new-instance v0, Ljava/lang/Thread;

    new-instance v1, Lcom/konka/musicPlayer/musicPlayerActivity$9;

    invoke-direct {v1, p0}, Lcom/konka/musicPlayer/musicPlayerActivity$9;-><init>(Lcom/konka/musicPlayer/musicPlayerActivity;)V

    invoke-direct {v0, v1}, Ljava/lang/Thread;-><init>(Ljava/lang/Runnable;)V

    invoke-virtual {v0}, Ljava/lang/Thread;->start()V

    invoke-virtual {p0}, Lcom/konka/musicPlayer/musicPlayerActivity;->musicInit()V

    return-void
.end method

.method protected onDestroy()V
    .locals 2

    iget-object v0, p0, Lcom/konka/musicPlayer/musicPlayerActivity;->dataReceiver:Lcom/konka/musicPlayer/musicPlayerActivity$playerDataReceiver;

    invoke-virtual {p0, v0}, Lcom/konka/musicPlayer/musicPlayerActivity;->unregisterReceiver(Landroid/content/BroadcastReceiver;)V

    const-string v0, "onDestroy"

    const-string v1, " music onDestroy()"

    invoke-static {v0, v1}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    iget-object v0, p0, Lcom/konka/musicPlayer/musicPlayerActivity;->seekReceiver:Lcom/konka/musicPlayer/musicPlayerActivity$SeekToReceiver;

    invoke-virtual {p0, v0}, Lcom/konka/musicPlayer/musicPlayerActivity;->unregisterReceiver(Landroid/content/BroadcastReceiver;)V

    iget-object v0, p0, Lcom/konka/musicPlayer/musicPlayerActivity;->musicClose:Lcom/konka/musicPlayer/musicPlayerActivity$closeReceiver;

    invoke-virtual {p0, v0}, Lcom/konka/musicPlayer/musicPlayerActivity;->unregisterReceiver(Landroid/content/BroadcastReceiver;)V

    iget-object v0, p0, Lcom/konka/musicPlayer/musicPlayerActivity;->mediaPlayerSwitch:Lcom/konka/musicPlayer/musicPlayerActivity$switchReceiver;

    invoke-virtual {p0, v0}, Lcom/konka/musicPlayer/musicPlayerActivity;->unregisterReceiver(Landroid/content/BroadcastReceiver;)V

    invoke-super {p0}, Landroid/app/Activity;->onDestroy()V

    return-void
.end method

.method public onKeyDown(ILandroid/view/KeyEvent;)Z
    .locals 9
    .param p1    # I
    .param p2    # Landroid/view/KeyEvent;

    const v5, 0x7f0900a5    # com.konka.mediaSharePlayer.R.string.forbidoption

    const v4, 0x7f02001e    # com.konka.mediaSharePlayer.R.drawable.com_play_sel

    const v2, 0x7f020015    # com.konka.mediaSharePlayer.R.drawable.com_pause_sel

    const/4 v3, 0x0

    const/4 v8, 0x1

    const/4 v7, 0x0

    iget-object v0, p0, Lcom/konka/musicPlayer/musicPlayerActivity;->musicList:Lcom/konka/musicPlayer/ScrollLayout;

    invoke-virtual {v0}, Lcom/konka/musicPlayer/ScrollLayout;->getCurScreen()I

    move-result v7

    iget-object v0, p0, Lcom/konka/musicPlayer/musicPlayerActivity;->seekBar:Landroid/widget/SeekBar;

    invoke-virtual {v0}, Landroid/widget/SeekBar;->getProgress()I

    move-result v6

    invoke-virtual {p2}, Landroid/view/KeyEvent;->getKeyCode()I

    move-result v0

    sparse-switch v0, :sswitch_data_0

    :goto_0
    invoke-super {p0, p1, p2}, Landroid/app/Activity;->onKeyDown(ILandroid/view/KeyEvent;)Z

    move-result v0

    return v0

    :sswitch_0
    sget-object v0, Lcom/konka/mediaSharePlayer/PlayerState;->PLAY_STATUS_EXIT:Lcom/konka/mediaSharePlayer/PlayerState;

    invoke-virtual {v0}, Lcom/konka/mediaSharePlayer/PlayerState;->ordinal()I

    move-result v2

    new-instance v0, Ljava/lang/StringBuilder;

    iget-object v1, p0, Lcom/konka/musicPlayer/musicPlayerActivity;->info:Lcom/konka/musicPlayer/MusicInfo;

    invoke-virtual {v1}, Lcom/konka/musicPlayer/MusicInfo;->getData()Ljava/lang/String;

    move-result-object v1

    invoke-static {v1}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    const-string v1, "&ma"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    move-object v0, p0

    move-object v1, p0

    move v4, v3

    invoke-virtual/range {v0 .. v5}, Lcom/konka/musicPlayer/musicPlayerActivity;->sendPlayerStateToPlayerService(Landroid/content/Context;IIILjava/lang/String;)V

    invoke-direct {p0}, Lcom/konka/musicPlayer/musicPlayerActivity;->stopMusicService()V

    iput v8, p0, Lcom/konka/musicPlayer/musicPlayerActivity;->mediastop:I

    invoke-virtual {p0}, Lcom/konka/musicPlayer/musicPlayerActivity;->finish()V

    goto :goto_0

    :sswitch_1
    const-string v0, "onkeydown"

    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "onkeydown1:"

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {p2}, Landroid/view/KeyEvent;->getKeyCode()I

    move-result v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, "isfocusInGrid :"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget-boolean v2, p0, Lcom/konka/musicPlayer/musicPlayerActivity;->isfocusInGrid:Z

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    invoke-virtual {p0}, Lcom/konka/musicPlayer/musicPlayerActivity;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    invoke-virtual {v0, v5}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v0

    invoke-static {p0, v0, v8}, Landroid/widget/Toast;->makeText(Landroid/content/Context;Ljava/lang/CharSequence;I)Landroid/widget/Toast;

    move-result-object v0

    invoke-virtual {v0}, Landroid/widget/Toast;->show()V

    goto :goto_0

    :sswitch_2
    const-string v0, "onkeydown"

    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "onkeydown5:"

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {p2}, Landroid/view/KeyEvent;->getKeyCode()I

    move-result v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, "isfocusInGrid :"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget-boolean v2, p0, Lcom/konka/musicPlayer/musicPlayerActivity;->isfocusInGrid:Z

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    invoke-virtual {p0}, Lcom/konka/musicPlayer/musicPlayerActivity;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    invoke-virtual {v0, v5}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v0

    invoke-static {p0, v0, v8}, Landroid/widget/Toast;->makeText(Landroid/content/Context;Ljava/lang/CharSequence;I)Landroid/widget/Toast;

    move-result-object v0

    invoke-virtual {v0}, Landroid/widget/Toast;->show()V

    goto/16 :goto_0

    :sswitch_3
    const-string v0, "onkeydown"

    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "onkeydown2:"

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {p2}, Landroid/view/KeyEvent;->getKeyCode()I

    move-result v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, "isfocusInGrid :"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget-boolean v2, p0, Lcom/konka/musicPlayer/musicPlayerActivity;->isfocusInGrid:Z

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    goto/16 :goto_0

    :sswitch_4
    iput-boolean v8, p0, Lcom/konka/musicPlayer/musicPlayerActivity;->isfocusInGrid:Z

    iget-boolean v0, p0, Lcom/konka/musicPlayer/musicPlayerActivity;->isfocusInGrid:Z

    invoke-direct {p0, v0}, Lcom/konka/musicPlayer/musicPlayerActivity;->setBtnFocus(Z)V

    iget-object v0, p0, Lcom/konka/musicPlayer/musicPlayerActivity;->seekBar:Landroid/widget/SeekBar;

    invoke-virtual {v0}, Landroid/widget/SeekBar;->requestFocus()Z

    iget v0, p0, Lcom/konka/musicPlayer/musicPlayerActivity;->musicplaystate:I

    if-ne v0, v8, :cond_0

    iget-object v0, p0, Lcom/konka/musicPlayer/musicPlayerActivity;->pauseBtn:Landroid/widget/Button;

    const v1, 0x7f020016    # com.konka.mediaSharePlayer.R.drawable.com_pause_uns

    invoke-virtual {v0, v1}, Landroid/widget/Button;->setBackgroundResource(I)V

    iput-boolean v3, p0, Lcom/konka/musicPlayer/musicPlayerActivity;->isPause:Z

    goto/16 :goto_0

    :cond_0
    iget-object v0, p0, Lcom/konka/musicPlayer/musicPlayerActivity;->playBtn:Landroid/widget/Button;

    const v1, 0x7f020021    # com.konka.mediaSharePlayer.R.drawable.com_play_uns

    invoke-virtual {v0, v1}, Landroid/widget/Button;->setBackgroundResource(I)V

    iput-boolean v8, p0, Lcom/konka/musicPlayer/musicPlayerActivity;->isPause:Z

    goto/16 :goto_0

    :sswitch_5
    iput-boolean v8, p0, Lcom/konka/musicPlayer/musicPlayerActivity;->isfocusInGrid:Z

    iget-boolean v0, p0, Lcom/konka/musicPlayer/musicPlayerActivity;->isfocusInGrid:Z

    invoke-direct {p0, v0}, Lcom/konka/musicPlayer/musicPlayerActivity;->setBtnFocus(Z)V

    iget v0, p0, Lcom/konka/musicPlayer/musicPlayerActivity;->musicplaystate:I

    if-ne v0, v8, :cond_1

    iget-object v0, p0, Lcom/konka/musicPlayer/musicPlayerActivity;->pauseBtn:Landroid/widget/Button;

    invoke-virtual {v0, v8}, Landroid/widget/Button;->setFocusable(Z)V

    iget-object v0, p0, Lcom/konka/musicPlayer/musicPlayerActivity;->pauseBtn:Landroid/widget/Button;

    invoke-virtual {v0}, Landroid/widget/Button;->requestFocus()Z

    iget-object v0, p0, Lcom/konka/musicPlayer/musicPlayerActivity;->pauseBtn:Landroid/widget/Button;

    invoke-virtual {v0, v2}, Landroid/widget/Button;->setBackgroundResource(I)V

    iput-boolean v3, p0, Lcom/konka/musicPlayer/musicPlayerActivity;->isPause:Z

    goto/16 :goto_0

    :cond_1
    iget-object v0, p0, Lcom/konka/musicPlayer/musicPlayerActivity;->playBtn:Landroid/widget/Button;

    invoke-virtual {v0, v8}, Landroid/widget/Button;->setFocusable(Z)V

    iget-object v0, p0, Lcom/konka/musicPlayer/musicPlayerActivity;->playBtn:Landroid/widget/Button;

    invoke-virtual {v0}, Landroid/widget/Button;->requestFocus()Z

    iget-object v0, p0, Lcom/konka/musicPlayer/musicPlayerActivity;->playBtn:Landroid/widget/Button;

    invoke-virtual {v0, v4}, Landroid/widget/Button;->setBackgroundResource(I)V

    iput-boolean v8, p0, Lcom/konka/musicPlayer/musicPlayerActivity;->isPause:Z

    goto/16 :goto_0

    :sswitch_6
    iget v0, p0, Lcom/konka/musicPlayer/musicPlayerActivity;->progressSpeed:I

    add-int/2addr v6, v0

    invoke-direct {p0, v6}, Lcom/konka/musicPlayer/musicPlayerActivity;->playPos(I)V

    sget-object v0, Lcom/konka/mediaSharePlayer/PlayerState;->PLAY_STATUS_SEEK:Lcom/konka/mediaSharePlayer/PlayerState;

    invoke-virtual {v0}, Lcom/konka/mediaSharePlayer/PlayerState;->ordinal()I

    move-result v2

    iget-object v0, p0, Lcom/konka/musicPlayer/musicPlayerActivity;->player:Landroid/media/MediaPlayer;

    invoke-virtual {v0}, Landroid/media/MediaPlayer;->getCurrentPosition()I

    move-result v3

    iget-object v0, p0, Lcom/konka/musicPlayer/musicPlayerActivity;->player:Landroid/media/MediaPlayer;

    invoke-virtual {v0}, Landroid/media/MediaPlayer;->getDuration()I

    move-result v4

    new-instance v0, Ljava/lang/StringBuilder;

    iget-object v1, p0, Lcom/konka/musicPlayer/musicPlayerActivity;->info:Lcom/konka/musicPlayer/MusicInfo;

    invoke-virtual {v1}, Lcom/konka/musicPlayer/MusicInfo;->getData()Ljava/lang/String;

    move-result-object v1

    invoke-static {v1}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    const-string v1, "&ma"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    move-object v0, p0

    move-object v1, p0

    invoke-virtual/range {v0 .. v5}, Lcom/konka/musicPlayer/musicPlayerActivity;->sendPlayerStateToPlayerService(Landroid/content/Context;IIILjava/lang/String;)V

    goto/16 :goto_0

    :sswitch_7
    iget v0, p0, Lcom/konka/musicPlayer/musicPlayerActivity;->progressSpeed:I

    sub-int/2addr v6, v0

    invoke-direct {p0, v6}, Lcom/konka/musicPlayer/musicPlayerActivity;->playPos(I)V

    sget-object v0, Lcom/konka/mediaSharePlayer/PlayerState;->PLAY_STATUS_SEEK:Lcom/konka/mediaSharePlayer/PlayerState;

    invoke-virtual {v0}, Lcom/konka/mediaSharePlayer/PlayerState;->ordinal()I

    move-result v2

    iget-object v0, p0, Lcom/konka/musicPlayer/musicPlayerActivity;->player:Landroid/media/MediaPlayer;

    invoke-virtual {v0}, Landroid/media/MediaPlayer;->getCurrentPosition()I

    move-result v3

    iget-object v0, p0, Lcom/konka/musicPlayer/musicPlayerActivity;->player:Landroid/media/MediaPlayer;

    invoke-virtual {v0}, Landroid/media/MediaPlayer;->getDuration()I

    move-result v4

    new-instance v0, Ljava/lang/StringBuilder;

    iget-object v1, p0, Lcom/konka/musicPlayer/musicPlayerActivity;->info:Lcom/konka/musicPlayer/MusicInfo;

    invoke-virtual {v1}, Lcom/konka/musicPlayer/MusicInfo;->getData()Ljava/lang/String;

    move-result-object v1

    invoke-static {v1}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    const-string v1, "&ma"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    move-object v0, p0

    move-object v1, p0

    invoke-virtual/range {v0 .. v5}, Lcom/konka/musicPlayer/musicPlayerActivity;->sendPlayerStateToPlayerService(Landroid/content/Context;IIILjava/lang/String;)V

    goto/16 :goto_0

    :sswitch_8
    iget v0, p0, Lcom/konka/musicPlayer/musicPlayerActivity;->musicplaystate:I

    if-ne v0, v8, :cond_2

    iget-object v0, p0, Lcom/konka/musicPlayer/musicPlayerActivity;->info:Lcom/konka/musicPlayer/MusicInfo;

    invoke-direct {p0, v3, v0}, Lcom/konka/musicPlayer/musicPlayerActivity;->playMusicService(ILcom/konka/musicPlayer/MusicInfo;)V

    iget-object v0, p0, Lcom/konka/musicPlayer/musicPlayerActivity;->pauseBtn:Landroid/widget/Button;

    iget-object v1, p0, Lcom/konka/musicPlayer/musicPlayerActivity;->playBtn:Landroid/widget/Button;

    invoke-virtual {p0, v0, v1}, Lcom/konka/musicPlayer/musicPlayerActivity;->setBtnShowHide(Landroid/widget/Button;Landroid/widget/Button;)V

    iget-object v0, p0, Lcom/konka/musicPlayer/musicPlayerActivity;->playBtn:Landroid/widget/Button;

    invoke-virtual {v0, v4}, Landroid/widget/Button;->setBackgroundResource(I)V

    iput-boolean v8, p0, Lcom/konka/musicPlayer/musicPlayerActivity;->isPause:Z

    iput v3, p0, Lcom/konka/musicPlayer/musicPlayerActivity;->musicplaystate:I

    goto/16 :goto_0

    :cond_2
    iget-object v0, p0, Lcom/konka/musicPlayer/musicPlayerActivity;->info:Lcom/konka/musicPlayer/MusicInfo;

    invoke-direct {p0, v8, v0}, Lcom/konka/musicPlayer/musicPlayerActivity;->playMusicService(ILcom/konka/musicPlayer/MusicInfo;)V

    iget-object v0, p0, Lcom/konka/musicPlayer/musicPlayerActivity;->playBtn:Landroid/widget/Button;

    iget-object v1, p0, Lcom/konka/musicPlayer/musicPlayerActivity;->pauseBtn:Landroid/widget/Button;

    invoke-virtual {p0, v0, v1}, Lcom/konka/musicPlayer/musicPlayerActivity;->setBtnShowHide(Landroid/widget/Button;Landroid/widget/Button;)V

    iget-object v0, p0, Lcom/konka/musicPlayer/musicPlayerActivity;->pauseBtn:Landroid/widget/Button;

    invoke-virtual {v0, v2}, Landroid/widget/Button;->setBackgroundResource(I)V

    iput-boolean v3, p0, Lcom/konka/musicPlayer/musicPlayerActivity;->isPause:Z

    iput v8, p0, Lcom/konka/musicPlayer/musicPlayerActivity;->musicplaystate:I

    goto/16 :goto_0

    :sswitch_data_0
    .sparse-switch
        0x4 -> :sswitch_0
        0x13 -> :sswitch_5
        0x14 -> :sswitch_4
        0x15 -> :sswitch_1
        0x16 -> :sswitch_2
        0x17 -> :sswitch_3
        0x42 -> :sswitch_8
        0x6f -> :sswitch_0
        0x8a -> :sswitch_8
        0xb9 -> :sswitch_6
        0xba -> :sswitch_7
    .end sparse-switch
.end method

.method protected onStart()V
    .locals 2

    const-string v0, "MultiScreeenMusicActivity:"

    const-string v1, "onStart()"

    invoke-static {v0, v1}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    invoke-super {p0}, Landroid/app/Activity;->onStart()V

    return-void
.end method

.method protected onStop()V
    .locals 6

    const/4 v4, 0x1

    const/4 v3, 0x0

    const-string v0, "MultiScreeenMusicActivity:"

    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "music onStart()----mediastop:"

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    iget v2, p0, Lcom/konka/musicPlayer/musicPlayerActivity;->mediastop:I

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, "mediaSwitch:"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget v2, p0, Lcom/konka/musicPlayer/musicPlayerActivity;->mediaSwitch:I

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    iget v0, p0, Lcom/konka/musicPlayer/musicPlayerActivity;->mediastop:I

    if-ne v0, v4, :cond_0

    iget v0, p0, Lcom/konka/musicPlayer/musicPlayerActivity;->mediaSwitch:I

    if-ne v0, v4, :cond_1

    invoke-static {p0, v3, v3, v3}, Lcom/konka/mediaSharePlayer/sendPlayerState;->MediaPlayerActionSendBroadcastIsCreat(Landroid/content/Context;III)V

    sget-object v0, Lcom/konka/mediaSharePlayer/PlayerState;->PLAY_STATUS_EXIT:Lcom/konka/mediaSharePlayer/PlayerState;

    invoke-virtual {v0}, Lcom/konka/mediaSharePlayer/PlayerState;->ordinal()I

    move-result v2

    new-instance v0, Ljava/lang/StringBuilder;

    iget-object v1, p0, Lcom/konka/musicPlayer/musicPlayerActivity;->info:Lcom/konka/musicPlayer/MusicInfo;

    invoke-virtual {v1}, Lcom/konka/musicPlayer/MusicInfo;->getData()Ljava/lang/String;

    move-result-object v1

    invoke-static {v1}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    const-string v1, "&ma"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    move-object v0, p0

    move-object v1, p0

    move v4, v3

    invoke-virtual/range {v0 .. v5}, Lcom/konka/musicPlayer/musicPlayerActivity;->sendPlayerStateToPlayerService(Landroid/content/Context;IIILjava/lang/String;)V

    :cond_0
    :goto_0
    invoke-direct {p0}, Lcom/konka/musicPlayer/musicPlayerActivity;->stopMusicService()V

    invoke-virtual {p0}, Lcom/konka/musicPlayer/musicPlayerActivity;->finish()V

    invoke-super {p0}, Landroid/app/Activity;->onStop()V

    return-void

    :cond_1
    const-string v0, "MultiScreeenMusicActivity:"

    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "music onStop()----mediaSwitch:"

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    iget v2, p0, Lcom/konka/musicPlayer/musicPlayerActivity;->mediaSwitch:I

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_0
.end method

.method public varargs refresh([Ljava/lang/Object;)V
    .locals 9
    .param p1    # [Ljava/lang/Object;

    const/4 v0, 0x0

    :try_start_0
    aget-object v0, p1, v0

    check-cast v0, Landroid/media/MediaPlayer;

    iput-object v0, p0, Lcom/konka/musicPlayer/musicPlayerActivity;->player:Landroid/media/MediaPlayer;

    const/4 v6, 0x0

    const/4 v7, 0x0

    iget-object v0, p0, Lcom/konka/musicPlayer/musicPlayerActivity;->player:Landroid/media/MediaPlayer;

    if-eqz v0, :cond_1

    iget-boolean v0, p0, Lcom/konka/musicPlayer/musicPlayerActivity;->isPlayerPrepare:Z

    if-eqz v0, :cond_0

    const-string v0, "MultiScreeenMusicActivity:"

    const-string v1, "richard player is not null!!!"

    invoke-static {v0, v1}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    iget-object v0, p0, Lcom/konka/musicPlayer/musicPlayerActivity;->player:Landroid/media/MediaPlayer;

    invoke-virtual {v0}, Landroid/media/MediaPlayer;->getCurrentPosition()I

    move-result v6

    const-string v0, "MultiScreeenMusicActivity:"

    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "richard current: "

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v1, v6}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    iget-object v0, p0, Lcom/konka/musicPlayer/musicPlayerActivity;->player:Landroid/media/MediaPlayer;

    invoke-virtual {v0}, Landroid/media/MediaPlayer;->getDuration()I

    move-result v7

    const-string v0, "MultiScreeenMusicActivity:"

    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "richard duration: "

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v1, v7}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    iget-object v0, p0, Lcom/konka/musicPlayer/musicPlayerActivity;->seekBar:Landroid/widget/SeekBar;

    invoke-virtual {v0, v7}, Landroid/widget/SeekBar;->setMax(I)V

    iget-object v0, p0, Lcom/konka/musicPlayer/musicPlayerActivity;->seekBar:Landroid/widget/SeekBar;

    invoke-virtual {v0, v6}, Landroid/widget/SeekBar;->setProgress(I)V

    iget-object v0, p0, Lcom/konka/musicPlayer/musicPlayerActivity;->seekBar:Landroid/widget/SeekBar;

    invoke-virtual {v0}, Landroid/widget/SeekBar;->postInvalidate()V

    iget-object v0, p0, Lcom/konka/musicPlayer/musicPlayerActivity;->playTiem:Landroid/widget/TextView;

    invoke-static {v6}, Lcom/konka/musicPlayer/MusicTool;->calculateDuration(I)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    iget-object v0, p0, Lcom/konka/musicPlayer/musicPlayerActivity;->time:Landroid/widget/TextView;

    invoke-static {v7}, Lcom/konka/musicPlayer/MusicTool;->calculateDuration(I)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    iget-boolean v0, p0, Lcom/konka/musicPlayer/musicPlayerActivity;->isPause:Z

    if-nez v0, :cond_0

    sget-object v0, Lcom/konka/mediaSharePlayer/PlayerState;->PLAY_STATUS_SYNC:Lcom/konka/mediaSharePlayer/PlayerState;

    invoke-virtual {v0}, Lcom/konka/mediaSharePlayer/PlayerState;->ordinal()I

    move-result v2

    iget-object v0, p0, Lcom/konka/musicPlayer/musicPlayerActivity;->player:Landroid/media/MediaPlayer;

    invoke-virtual {v0}, Landroid/media/MediaPlayer;->getCurrentPosition()I

    move-result v3

    iget-object v0, p0, Lcom/konka/musicPlayer/musicPlayerActivity;->player:Landroid/media/MediaPlayer;

    invoke-virtual {v0}, Landroid/media/MediaPlayer;->getDuration()I

    move-result v4

    new-instance v0, Ljava/lang/StringBuilder;

    iget-object v1, p0, Lcom/konka/musicPlayer/musicPlayerActivity;->info:Lcom/konka/musicPlayer/MusicInfo;

    invoke-virtual {v1}, Lcom/konka/musicPlayer/MusicInfo;->getData()Ljava/lang/String;

    move-result-object v1

    invoke-static {v1}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    const-string v1, "&ma"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    move-object v0, p0

    move-object v1, p0

    invoke-virtual/range {v0 .. v5}, Lcom/konka/musicPlayer/musicPlayerActivity;->sendPlayerStateToPlayerService(Landroid/content/Context;IIILjava/lang/String;)V

    :cond_0
    iget-object v0, p0, Lcom/konka/musicPlayer/musicPlayerActivity;->player:Landroid/media/MediaPlayer;

    iget-object v1, p0, Lcom/konka/musicPlayer/musicPlayerActivity;->seekListener:Landroid/media/MediaPlayer$OnSeekCompleteListener;

    invoke-virtual {v0, v1}, Landroid/media/MediaPlayer;->setOnSeekCompleteListener(Landroid/media/MediaPlayer$OnSeekCompleteListener;)V

    iget-object v0, p0, Lcom/konka/musicPlayer/musicPlayerActivity;->player:Landroid/media/MediaPlayer;

    iget-object v1, p0, Lcom/konka/musicPlayer/musicPlayerActivity;->completionListener:Landroid/media/MediaPlayer$OnCompletionListener;

    invoke-virtual {v0, v1}, Landroid/media/MediaPlayer;->setOnCompletionListener(Landroid/media/MediaPlayer$OnCompletionListener;)V

    iget-object v0, p0, Lcom/konka/musicPlayer/musicPlayerActivity;->player:Landroid/media/MediaPlayer;

    iget-object v1, p0, Lcom/konka/musicPlayer/musicPlayerActivity;->prepareListerer:Landroid/media/MediaPlayer$OnPreparedListener;

    invoke-virtual {v0, v1}, Landroid/media/MediaPlayer;->setOnPreparedListener(Landroid/media/MediaPlayer$OnPreparedListener;)V

    iget-object v0, p0, Lcom/konka/musicPlayer/musicPlayerActivity;->player:Landroid/media/MediaPlayer;

    iget-object v1, p0, Lcom/konka/musicPlayer/musicPlayerActivity;->playerErrorListener:Landroid/media/MediaPlayer$OnErrorListener;

    invoke-virtual {v0, v1}, Landroid/media/MediaPlayer;->setOnErrorListener(Landroid/media/MediaPlayer$OnErrorListener;)V

    :goto_0
    return-void

    :cond_1
    invoke-virtual {p0}, Lcom/konka/musicPlayer/musicPlayerActivity;->initSeekBar()V
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    goto :goto_0

    :catch_0
    move-exception v8

    invoke-virtual {p0}, Lcom/konka/musicPlayer/musicPlayerActivity;->initSeekBar()V

    invoke-virtual {v8}, Ljava/lang/Exception;->printStackTrace()V

    goto :goto_0
.end method

.method public registerCloseReceiver()V
    .locals 3

    new-instance v1, Lcom/konka/musicPlayer/musicPlayerActivity$closeReceiver;

    const/4 v2, 0x0

    invoke-direct {v1, p0, v2}, Lcom/konka/musicPlayer/musicPlayerActivity$closeReceiver;-><init>(Lcom/konka/musicPlayer/musicPlayerActivity;Lcom/konka/musicPlayer/musicPlayerActivity$closeReceiver;)V

    iput-object v1, p0, Lcom/konka/musicPlayer/musicPlayerActivity;->musicClose:Lcom/konka/musicPlayer/musicPlayerActivity$closeReceiver;

    new-instance v0, Landroid/content/IntentFilter;

    invoke-direct {v0}, Landroid/content/IntentFilter;-><init>()V

    const-string v1, "com.konka.playerService.MultiScreenPlayerService"

    invoke-virtual {v0, v1}, Landroid/content/IntentFilter;->addAction(Ljava/lang/String;)V

    iget-object v1, p0, Lcom/konka/musicPlayer/musicPlayerActivity;->musicClose:Lcom/konka/musicPlayer/musicPlayerActivity$closeReceiver;

    invoke-virtual {p0, v1, v0}, Lcom/konka/musicPlayer/musicPlayerActivity;->registerReceiver(Landroid/content/BroadcastReceiver;Landroid/content/IntentFilter;)Landroid/content/Intent;

    return-void
.end method

.method public registerSeekReceiver()V
    .locals 3

    new-instance v1, Lcom/konka/musicPlayer/musicPlayerActivity$SeekToReceiver;

    const/4 v2, 0x0

    invoke-direct {v1, p0, v2}, Lcom/konka/musicPlayer/musicPlayerActivity$SeekToReceiver;-><init>(Lcom/konka/musicPlayer/musicPlayerActivity;Lcom/konka/musicPlayer/musicPlayerActivity$SeekToReceiver;)V

    iput-object v1, p0, Lcom/konka/musicPlayer/musicPlayerActivity;->seekReceiver:Lcom/konka/musicPlayer/musicPlayerActivity$SeekToReceiver;

    new-instance v0, Landroid/content/IntentFilter;

    invoke-direct {v0}, Landroid/content/IntentFilter;-><init>()V

    const-string v1, "com.konka.MultiScreeenPlayer.playerseek"

    invoke-virtual {v0, v1}, Landroid/content/IntentFilter;->addAction(Ljava/lang/String;)V

    iget-object v1, p0, Lcom/konka/musicPlayer/musicPlayerActivity;->seekReceiver:Lcom/konka/musicPlayer/musicPlayerActivity$SeekToReceiver;

    invoke-virtual {p0, v1, v0}, Lcom/konka/musicPlayer/musicPlayerActivity;->registerReceiver(Landroid/content/BroadcastReceiver;Landroid/content/IntentFilter;)Landroid/content/Intent;

    return-void
.end method

.method public registerSwitchReceiver()V
    .locals 3

    new-instance v1, Lcom/konka/musicPlayer/musicPlayerActivity$switchReceiver;

    const/4 v2, 0x0

    invoke-direct {v1, p0, v2}, Lcom/konka/musicPlayer/musicPlayerActivity$switchReceiver;-><init>(Lcom/konka/musicPlayer/musicPlayerActivity;Lcom/konka/musicPlayer/musicPlayerActivity$switchReceiver;)V

    iput-object v1, p0, Lcom/konka/musicPlayer/musicPlayerActivity;->mediaPlayerSwitch:Lcom/konka/musicPlayer/musicPlayerActivity$switchReceiver;

    new-instance v0, Landroid/content/IntentFilter;

    invoke-direct {v0}, Landroid/content/IntentFilter;-><init>()V

    const-string v1, "com.konka.playerService.MultiScreenPlayerSwtich"

    invoke-virtual {v0, v1}, Landroid/content/IntentFilter;->addAction(Ljava/lang/String;)V

    iget-object v1, p0, Lcom/konka/musicPlayer/musicPlayerActivity;->mediaPlayerSwitch:Lcom/konka/musicPlayer/musicPlayerActivity$switchReceiver;

    invoke-virtual {p0, v1, v0}, Lcom/konka/musicPlayer/musicPlayerActivity;->registerReceiver(Landroid/content/BroadcastReceiver;Landroid/content/IntentFilter;)Landroid/content/Intent;

    return-void
.end method

.method public sendPlayerStateToPlayerService(Landroid/content/Context;IIILjava/lang/String;)V
    .locals 5
    .param p1    # Landroid/content/Context;
    .param p2    # I
    .param p3    # I
    .param p4    # I
    .param p5    # Ljava/lang/String;

    new-instance v1, Landroid/content/Intent;

    invoke-direct {v1}, Landroid/content/Intent;-><init>()V

    const-string v2, "com.konka.MultiScreeenPlayer.PlayerState"

    invoke-virtual {v1, v2}, Landroid/content/Intent;->setAction(Ljava/lang/String;)Landroid/content/Intent;

    new-instance v0, Landroid/os/Bundle;

    invoke-direct {v0}, Landroid/os/Bundle;-><init>()V

    const-string v2, "playerState"

    invoke-virtual {v0, v2, p2}, Landroid/os/Bundle;->putInt(Ljava/lang/String;I)V

    const-string v2, "currentTime"

    invoke-virtual {v0, v2, p3}, Landroid/os/Bundle;->putInt(Ljava/lang/String;I)V

    const-string v2, "DurationTime"

    invoke-virtual {v0, v2, p4}, Landroid/os/Bundle;->putInt(Ljava/lang/String;I)V

    const-string v2, "fileUrl"

    invoke-virtual {v0, v2, p5}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V

    const-string v2, "mobileIp"

    iget-object v3, p0, Lcom/konka/musicPlayer/musicPlayerActivity;->mobileIp:Ljava/lang/String;

    invoke-virtual {v0, v2, v3}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V

    invoke-virtual {v1, v0}, Landroid/content/Intent;->putExtras(Landroid/os/Bundle;)Landroid/content/Intent;

    invoke-virtual {p1, v1}, Landroid/content/Context;->sendBroadcast(Landroid/content/Intent;)V

    const-string v2, "sendBroadcast"

    new-instance v3, Ljava/lang/StringBuilder;

    const-string v4, "sendPlayerStateToPlayerService: "

    invoke-direct {v3, v4}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    iget-object v4, p0, Lcom/konka/musicPlayer/musicPlayerActivity;->mobileIp:Ljava/lang/String;

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v2, v3}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    const-string v2, "sendBroadcast"

    new-instance v3, Ljava/lang/StringBuilder;

    const-string v4, "sendPlayerStateToPlayerService:"

    invoke-direct {v3, v4}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v3, p2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v3

    const-string v4, ","

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3, p3}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v3

    const-string v4, ","

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3, p4}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v3

    const-string v4, ","

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3, p5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v2, v3}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    return-void
.end method

.method public setBtnShowHide(Landroid/widget/Button;Landroid/widget/Button;)V
    .locals 1
    .param p1    # Landroid/widget/Button;
    .param p2    # Landroid/widget/Button;

    const/16 v0, 0x8

    invoke-virtual {p1, v0}, Landroid/widget/Button;->setVisibility(I)V

    const/4 v0, 0x0

    invoke-virtual {p2, v0}, Landroid/widget/Button;->setVisibility(I)V

    const/4 v0, 0x1

    invoke-virtual {p2, v0}, Landroid/widget/Button;->setFocusable(Z)V

    invoke-virtual {p2}, Landroid/widget/Button;->requestFocus()Z

    return-void
.end method
