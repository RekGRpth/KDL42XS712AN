.class public interface abstract Lcom/konka/kkinterface/tv/SettingDesk;
.super Ljava/lang/Object;
.source "SettingDesk.java"

# interfaces
.implements Lcom/konka/kkinterface/tv/BaseDesk;


# virtual methods
.method public abstract ExecRestoreToDefault()Z
.end method

.method public abstract GetBlueScreenFlag()Z
.end method

.method public abstract GetCecStatus()Lcom/mstar/android/tvapi/common/vo/CecSetting;
.end method

.method public abstract GetChannelSWMode()Lcom/konka/kkinterface/tv/DataBaseDesk$EN_MS_CHANNEL_SWITCH_MODE;
.end method

.method public abstract GetColorRange()S
.end method

.method public abstract GetEnvironmentPowerOnLogoMode()Lcom/mstar/android/tvapi/common/vo/EnumPowerOnLogoMode;
.end method

.method public abstract GetEnvironmentPowerOnMusicMode()Lcom/mstar/android/tvapi/common/vo/EnumPowerOnMusicMode;
.end method

.method public abstract GetEnvironmentPowerOnMusicVolume()S
.end method

.method public abstract GetFilmMode()Lcom/konka/kkinterface/tv/DataBaseDesk$EN_MS_FILM;
.end method

.method public abstract GetOffDetMode()Lcom/konka/kkinterface/tv/DataBaseDesk$EN_MS_OFFLINE_DET_MODE;
.end method

.method public abstract GetOsdDuration()S
.end method

.method public abstract GetOsdLanguage()Lcom/mstar/android/tvapi/common/vo/TvOsType$EnumLanguage;
.end method

.method public abstract GetSystemAutoTimeType()I
.end method

.method public abstract SetBlueScreenFlag(Z)Z
.end method

.method public abstract SetCecStatus(Lcom/mstar/android/tvapi/common/vo/CecSetting;)Z
.end method

.method public abstract SetChannelSWMode(Lcom/konka/kkinterface/tv/DataBaseDesk$EN_MS_CHANNEL_SWITCH_MODE;)Z
.end method

.method public abstract SetColorRanger(S)Z
.end method

.method public abstract SetEnvironmentPowerOnLogoMode(Lcom/mstar/android/tvapi/common/vo/EnumPowerOnLogoMode;)Z
.end method

.method public abstract SetEnvironmentPowerOnMusicMode(Lcom/mstar/android/tvapi/common/vo/EnumPowerOnMusicMode;)Z
.end method

.method public abstract SetEnvironmentPowerOnMusicVolume(S)Z
.end method

.method public abstract SetFilmMode(Lcom/konka/kkinterface/tv/DataBaseDesk$EN_MS_FILM;)Z
.end method

.method public abstract SetOffDetMode(Lcom/konka/kkinterface/tv/DataBaseDesk$EN_MS_OFFLINE_DET_MODE;)Z
.end method

.method public abstract SetOsdDuration(S)Z
.end method

.method public abstract SetOsdLanguage(Lcom/mstar/android/tvapi/common/vo/TvOsType$EnumLanguage;)Z
.end method

.method public abstract SetSystemAutoTimeType(I)Z
.end method

.method public abstract getBurnInMode()Z
.end method

.method public abstract getColorWheelMode()Lcom/konka/kkinterface/tv/DataBaseDesk$ColorWheelMode;
.end method

.method public abstract getOsdTimeoutSecond()I
.end method

.method public abstract getScreenSaveModeStatus()Z
.end method

.method public abstract getSmartEnergySaving()Lcom/konka/kkinterface/tv/DataBaseDesk$SmartEnergySavingMode;
.end method

.method public abstract getStandbyNoOperation()I
.end method

.method public abstract getStandbyNoSignal()Z
.end method

.method public abstract setColorWheelMode(Lcom/konka/kkinterface/tv/DataBaseDesk$ColorWheelMode;)Z
.end method

.method public abstract setScreenSaveModeStatus(Z)Z
.end method

.method public abstract setSmartEnergySaving(Lcom/konka/kkinterface/tv/DataBaseDesk$SmartEnergySavingMode;)Z
.end method

.method public abstract setStandbyNoOperation(I)Z
.end method

.method public abstract setStandbyNoSignal(Z)Z
.end method
