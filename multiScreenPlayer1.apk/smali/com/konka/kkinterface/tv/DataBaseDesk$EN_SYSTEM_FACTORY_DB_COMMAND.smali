.class public final enum Lcom/konka/kkinterface/tv/DataBaseDesk$EN_SYSTEM_FACTORY_DB_COMMAND;
.super Ljava/lang/Enum;
.source "DataBaseDesk.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/konka/kkinterface/tv/DataBaseDesk;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x4019
    name = "EN_SYSTEM_FACTORY_DB_COMMAND"
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Enum",
        "<",
        "Lcom/konka/kkinterface/tv/DataBaseDesk$EN_SYSTEM_FACTORY_DB_COMMAND;",
        ">;"
    }
.end annotation


# static fields
.field private static final synthetic ENUM$VALUES:[Lcom/konka/kkinterface/tv/DataBaseDesk$EN_SYSTEM_FACTORY_DB_COMMAND;

.field public static final enum E_FACTORY_COLOR_TEMP_SET:Lcom/konka/kkinterface/tv/DataBaseDesk$EN_SYSTEM_FACTORY_DB_COMMAND;

.field public static final enum E_FACTORY_COMMAND_NUM:Lcom/konka/kkinterface/tv/DataBaseDesk$EN_SYSTEM_FACTORY_DB_COMMAND;

.field public static final enum E_FACTORY_RESTORE_DEFAULT:Lcom/konka/kkinterface/tv/DataBaseDesk$EN_SYSTEM_FACTORY_DB_COMMAND;

.field public static final enum E_FACTORY_VIDEO_ADC_SET:Lcom/konka/kkinterface/tv/DataBaseDesk$EN_SYSTEM_FACTORY_DB_COMMAND;

.field public static final enum E_USER_RESTORE_DEFAULT:Lcom/konka/kkinterface/tv/DataBaseDesk$EN_SYSTEM_FACTORY_DB_COMMAND;


# direct methods
.method static constructor <clinit>()V
    .locals 7

    const/4 v6, 0x4

    const/4 v5, 0x3

    const/4 v4, 0x2

    const/4 v3, 0x1

    const/4 v2, 0x0

    new-instance v0, Lcom/konka/kkinterface/tv/DataBaseDesk$EN_SYSTEM_FACTORY_DB_COMMAND;

    const-string v1, "E_FACTORY_COLOR_TEMP_SET"

    invoke-direct {v0, v1, v2}, Lcom/konka/kkinterface/tv/DataBaseDesk$EN_SYSTEM_FACTORY_DB_COMMAND;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/konka/kkinterface/tv/DataBaseDesk$EN_SYSTEM_FACTORY_DB_COMMAND;->E_FACTORY_COLOR_TEMP_SET:Lcom/konka/kkinterface/tv/DataBaseDesk$EN_SYSTEM_FACTORY_DB_COMMAND;

    new-instance v0, Lcom/konka/kkinterface/tv/DataBaseDesk$EN_SYSTEM_FACTORY_DB_COMMAND;

    const-string v1, "E_FACTORY_VIDEO_ADC_SET"

    invoke-direct {v0, v1, v3}, Lcom/konka/kkinterface/tv/DataBaseDesk$EN_SYSTEM_FACTORY_DB_COMMAND;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/konka/kkinterface/tv/DataBaseDesk$EN_SYSTEM_FACTORY_DB_COMMAND;->E_FACTORY_VIDEO_ADC_SET:Lcom/konka/kkinterface/tv/DataBaseDesk$EN_SYSTEM_FACTORY_DB_COMMAND;

    new-instance v0, Lcom/konka/kkinterface/tv/DataBaseDesk$EN_SYSTEM_FACTORY_DB_COMMAND;

    const-string v1, "E_FACTORY_RESTORE_DEFAULT"

    invoke-direct {v0, v1, v4}, Lcom/konka/kkinterface/tv/DataBaseDesk$EN_SYSTEM_FACTORY_DB_COMMAND;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/konka/kkinterface/tv/DataBaseDesk$EN_SYSTEM_FACTORY_DB_COMMAND;->E_FACTORY_RESTORE_DEFAULT:Lcom/konka/kkinterface/tv/DataBaseDesk$EN_SYSTEM_FACTORY_DB_COMMAND;

    new-instance v0, Lcom/konka/kkinterface/tv/DataBaseDesk$EN_SYSTEM_FACTORY_DB_COMMAND;

    const-string v1, "E_USER_RESTORE_DEFAULT"

    invoke-direct {v0, v1, v5}, Lcom/konka/kkinterface/tv/DataBaseDesk$EN_SYSTEM_FACTORY_DB_COMMAND;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/konka/kkinterface/tv/DataBaseDesk$EN_SYSTEM_FACTORY_DB_COMMAND;->E_USER_RESTORE_DEFAULT:Lcom/konka/kkinterface/tv/DataBaseDesk$EN_SYSTEM_FACTORY_DB_COMMAND;

    new-instance v0, Lcom/konka/kkinterface/tv/DataBaseDesk$EN_SYSTEM_FACTORY_DB_COMMAND;

    const-string v1, "E_FACTORY_COMMAND_NUM"

    invoke-direct {v0, v1, v6}, Lcom/konka/kkinterface/tv/DataBaseDesk$EN_SYSTEM_FACTORY_DB_COMMAND;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/konka/kkinterface/tv/DataBaseDesk$EN_SYSTEM_FACTORY_DB_COMMAND;->E_FACTORY_COMMAND_NUM:Lcom/konka/kkinterface/tv/DataBaseDesk$EN_SYSTEM_FACTORY_DB_COMMAND;

    const/4 v0, 0x5

    new-array v0, v0, [Lcom/konka/kkinterface/tv/DataBaseDesk$EN_SYSTEM_FACTORY_DB_COMMAND;

    sget-object v1, Lcom/konka/kkinterface/tv/DataBaseDesk$EN_SYSTEM_FACTORY_DB_COMMAND;->E_FACTORY_COLOR_TEMP_SET:Lcom/konka/kkinterface/tv/DataBaseDesk$EN_SYSTEM_FACTORY_DB_COMMAND;

    aput-object v1, v0, v2

    sget-object v1, Lcom/konka/kkinterface/tv/DataBaseDesk$EN_SYSTEM_FACTORY_DB_COMMAND;->E_FACTORY_VIDEO_ADC_SET:Lcom/konka/kkinterface/tv/DataBaseDesk$EN_SYSTEM_FACTORY_DB_COMMAND;

    aput-object v1, v0, v3

    sget-object v1, Lcom/konka/kkinterface/tv/DataBaseDesk$EN_SYSTEM_FACTORY_DB_COMMAND;->E_FACTORY_RESTORE_DEFAULT:Lcom/konka/kkinterface/tv/DataBaseDesk$EN_SYSTEM_FACTORY_DB_COMMAND;

    aput-object v1, v0, v4

    sget-object v1, Lcom/konka/kkinterface/tv/DataBaseDesk$EN_SYSTEM_FACTORY_DB_COMMAND;->E_USER_RESTORE_DEFAULT:Lcom/konka/kkinterface/tv/DataBaseDesk$EN_SYSTEM_FACTORY_DB_COMMAND;

    aput-object v1, v0, v5

    sget-object v1, Lcom/konka/kkinterface/tv/DataBaseDesk$EN_SYSTEM_FACTORY_DB_COMMAND;->E_FACTORY_COMMAND_NUM:Lcom/konka/kkinterface/tv/DataBaseDesk$EN_SYSTEM_FACTORY_DB_COMMAND;

    aput-object v1, v0, v6

    sput-object v0, Lcom/konka/kkinterface/tv/DataBaseDesk$EN_SYSTEM_FACTORY_DB_COMMAND;->ENUM$VALUES:[Lcom/konka/kkinterface/tv/DataBaseDesk$EN_SYSTEM_FACTORY_DB_COMMAND;

    return-void
.end method

.method private constructor <init>(Ljava/lang/String;I)V
    .locals 0

    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    return-void
.end method

.method public static valueOf(Ljava/lang/String;)Lcom/konka/kkinterface/tv/DataBaseDesk$EN_SYSTEM_FACTORY_DB_COMMAND;
    .locals 1

    const-class v0, Lcom/konka/kkinterface/tv/DataBaseDesk$EN_SYSTEM_FACTORY_DB_COMMAND;

    invoke-static {v0, p0}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;

    move-result-object v0

    check-cast v0, Lcom/konka/kkinterface/tv/DataBaseDesk$EN_SYSTEM_FACTORY_DB_COMMAND;

    return-object v0
.end method

.method public static values()[Lcom/konka/kkinterface/tv/DataBaseDesk$EN_SYSTEM_FACTORY_DB_COMMAND;
    .locals 4

    const/4 v3, 0x0

    sget-object v0, Lcom/konka/kkinterface/tv/DataBaseDesk$EN_SYSTEM_FACTORY_DB_COMMAND;->ENUM$VALUES:[Lcom/konka/kkinterface/tv/DataBaseDesk$EN_SYSTEM_FACTORY_DB_COMMAND;

    array-length v1, v0

    new-array v2, v1, [Lcom/konka/kkinterface/tv/DataBaseDesk$EN_SYSTEM_FACTORY_DB_COMMAND;

    invoke-static {v0, v3, v2, v3, v1}, Ljava/lang/System;->arraycopy(Ljava/lang/Object;ILjava/lang/Object;II)V

    return-object v2
.end method
