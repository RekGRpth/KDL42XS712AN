.class public Lcom/konka/kkimplements/tv/mstar/TvDeskImpl;
.super Landroid/app/Service;
.source "TvDeskImpl.java"

# interfaces
.implements Lcom/konka/kkinterface/tv/TvDesk;


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/konka/kkimplements/tv/mstar/TvDeskImpl$TvServiceBinder;
    }
.end annotation


# static fields
.field static final FIXVALUE:I = 0xa

.field static siTestValue:I


# instance fields
.field private com:Lcom/konka/kkinterface/tv/CommonDesk;

.field private context:Landroid/content/Context;

.field iTestValue:I

.field private tvServiceBinder:Lcom/konka/kkimplements/tv/mstar/TvDeskImpl$TvServiceBinder;


# direct methods
.method public constructor <init>(Landroid/content/Context;)V
    .locals 6
    .param p1    # Landroid/content/Context;

    const-wide/16 v2, 0x3e8

    invoke-direct {p0}, Landroid/app/Service;-><init>()V

    const/4 v1, 0x0

    iput-object v1, p0, Lcom/konka/kkimplements/tv/mstar/TvDeskImpl;->com:Lcom/konka/kkinterface/tv/CommonDesk;

    iput-object p1, p0, Lcom/konka/kkimplements/tv/mstar/TvDeskImpl;->context:Landroid/content/Context;

    invoke-static {p1}, Lcom/konka/kkimplements/tv/mstar/CommonDeskImpl;->getInstance(Landroid/content/Context;)Lcom/konka/kkinterface/tv/CommonDesk;

    move-result-object v1

    iput-object v1, p0, Lcom/konka/kkimplements/tv/mstar/TvDeskImpl;->com:Lcom/konka/kkinterface/tv/CommonDesk;

    iget-object v1, p0, Lcom/konka/kkimplements/tv/mstar/TvDeskImpl;->com:Lcom/konka/kkinterface/tv/CommonDesk;

    const-string v4, "TvService"

    const-string v5, "TvServiceImpl constructor!!"

    invoke-interface {v1, v4, v5}, Lcom/konka/kkinterface/tv/CommonDesk;->printfE(Ljava/lang/String;Ljava/lang/String;)V

    new-instance v1, Ljava/lang/Thread;

    new-instance v4, Lcom/konka/kkimplements/tv/mstar/TvDeskImpl$1;

    invoke-direct {v4, p0}, Lcom/konka/kkimplements/tv/mstar/TvDeskImpl$1;-><init>(Lcom/konka/kkimplements/tv/mstar/TvDeskImpl;)V

    invoke-direct {v1, v4}, Ljava/lang/Thread;-><init>(Ljava/lang/Runnable;)V

    invoke-virtual {v1}, Ljava/lang/Thread;->start()V

    iget-object v1, p0, Lcom/konka/kkimplements/tv/mstar/TvDeskImpl;->com:Lcom/konka/kkinterface/tv/CommonDesk;

    const-string v4, "start timer..."

    invoke-interface {v1, v4}, Lcom/konka/kkinterface/tv/CommonDesk;->printfE(Ljava/lang/String;)V

    new-instance v0, Ljava/util/Timer;

    invoke-direct {v0}, Ljava/util/Timer;-><init>()V

    new-instance v1, Lcom/konka/kkimplements/tv/mstar/TvDeskImpl$2;

    invoke-direct {v1, p0}, Lcom/konka/kkimplements/tv/mstar/TvDeskImpl$2;-><init>(Lcom/konka/kkimplements/tv/mstar/TvDeskImpl;)V

    move-wide v4, v2

    invoke-virtual/range {v0 .. v5}, Ljava/util/Timer;->schedule(Ljava/util/TimerTask;JJ)V

    return-void
.end method

.method static synthetic access$0(Lcom/konka/kkimplements/tv/mstar/TvDeskImpl;)Landroid/content/Context;
    .locals 1

    iget-object v0, p0, Lcom/konka/kkimplements/tv/mstar/TvDeskImpl;->context:Landroid/content/Context;

    return-object v0
.end method

.method static synthetic access$1(Lcom/konka/kkimplements/tv/mstar/TvDeskImpl;)Lcom/konka/kkinterface/tv/CommonDesk;
    .locals 1

    iget-object v0, p0, Lcom/konka/kkimplements/tv/mstar/TvDeskImpl;->com:Lcom/konka/kkinterface/tv/CommonDesk;

    return-object v0
.end method

.method private selffunc1()V
    .locals 3

    iget-object v0, p0, Lcom/konka/kkimplements/tv/mstar/TvDeskImpl;->com:Lcom/konka/kkinterface/tv/CommonDesk;

    const-string v1, "TvService"

    const-string v2, "selffunc1:hello!!"

    invoke-interface {v0, v1, v2}, Lcom/konka/kkinterface/tv/CommonDesk;->printfE(Ljava/lang/String;Ljava/lang/String;)V

    return-void
.end method


# virtual methods
.method public onBind(Landroid/content/Intent;)Landroid/os/IBinder;
    .locals 1
    .param p1    # Landroid/content/Intent;

    iget-object v0, p0, Lcom/konka/kkimplements/tv/mstar/TvDeskImpl;->tvServiceBinder:Lcom/konka/kkimplements/tv/mstar/TvDeskImpl$TvServiceBinder;

    return-object v0
.end method

.method public onCreate()V
    .locals 1

    new-instance v0, Lcom/konka/kkimplements/tv/mstar/TvDeskImpl$TvServiceBinder;

    invoke-direct {v0, p0}, Lcom/konka/kkimplements/tv/mstar/TvDeskImpl$TvServiceBinder;-><init>(Lcom/konka/kkimplements/tv/mstar/TvDeskImpl;)V

    iput-object v0, p0, Lcom/konka/kkimplements/tv/mstar/TvDeskImpl;->tvServiceBinder:Lcom/konka/kkimplements/tv/mstar/TvDeskImpl$TvServiceBinder;

    invoke-super {p0}, Landroid/app/Service;->onCreate()V

    return-void
.end method

.method public onDestroy()V
    .locals 2

    const-string v0, "service"

    const-string v1, "onDestroy"

    invoke-static {v0, v1}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    invoke-super {p0}, Landroid/app/Service;->onDestroy()V

    return-void
.end method

.method public openOrCreateDatabase(Ljava/lang/String;ILandroid/database/sqlite/SQLiteDatabase$CursorFactory;)Landroid/database/sqlite/SQLiteDatabase;
    .locals 1
    .param p1    # Ljava/lang/String;
    .param p2    # I
    .param p3    # Landroid/database/sqlite/SQLiteDatabase$CursorFactory;

    const-string v0, "data/data/mstar.tvsetting.factory/databases"

    invoke-super {p0, v0, p2, p3}, Landroid/app/Service;->openOrCreateDatabase(Ljava/lang/String;ILandroid/database/sqlite/SQLiteDatabase$CursorFactory;)Landroid/database/sqlite/SQLiteDatabase;

    move-result-object v0

    return-object v0
.end method

.method protected selffunc2()V
    .locals 3

    iget-object v0, p0, Lcom/konka/kkimplements/tv/mstar/TvDeskImpl;->com:Lcom/konka/kkinterface/tv/CommonDesk;

    const-string v1, "TvService"

    const-string v2, "selffunc2:hello!!"

    invoke-interface {v0, v1, v2}, Lcom/konka/kkinterface/tv/CommonDesk;->printfE(Ljava/lang/String;Ljava/lang/String;)V

    return-void
.end method

.method public showinfo()V
    .locals 3

    iget-object v0, p0, Lcom/konka/kkimplements/tv/mstar/TvDeskImpl;->com:Lcom/konka/kkinterface/tv/CommonDesk;

    const-string v1, "TvService"

    const-string v2, "This is mock class!!!"

    invoke-interface {v0, v1, v2}, Lcom/konka/kkinterface/tv/CommonDesk;->printfE(Ljava/lang/String;Ljava/lang/String;)V

    invoke-direct {p0}, Lcom/konka/kkimplements/tv/mstar/TvDeskImpl;->selffunc1()V

    invoke-virtual {p0}, Lcom/konka/kkimplements/tv/mstar/TvDeskImpl;->selffunc2()V

    return-void
.end method

.method public test(I)I
    .locals 3
    .param p1    # I

    iget-object v0, p0, Lcom/konka/kkimplements/tv/mstar/TvDeskImpl;->com:Lcom/konka/kkinterface/tv/CommonDesk;

    const-string v1, "TvService"

    const-string v2, "test!!"

    invoke-interface {v0, v1, v2}, Lcom/konka/kkinterface/tv/CommonDesk;->printfE(Ljava/lang/String;Ljava/lang/String;)V

    const/4 v0, 0x0

    return v0
.end method
