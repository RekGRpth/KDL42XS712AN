.class public Lcom/konka/picturePlayer/picturePlayerActivity;
.super Landroid/app/Activity;
.source "picturePlayerActivity.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/konka/picturePlayer/picturePlayerActivity$DOWNLOAD_STATE;,
        Lcom/konka/picturePlayer/picturePlayerActivity$DataReceiver;,
        Lcom/konka/picturePlayer/picturePlayerActivity$ImageAdapter;,
        Lcom/konka/picturePlayer/picturePlayerActivity$ImageViewOnTounchListener;,
        Lcom/konka/picturePlayer/picturePlayerActivity$MyApplication;,
        Lcom/konka/picturePlayer/picturePlayerActivity$closeReceiver;,
        Lcom/konka/picturePlayer/picturePlayerActivity$switchReceiver;
    }
.end annotation


# static fields
.field private static final DEFAULT_TIMEOUT:I = 0x3a98

.field static final DRAG:I = 0x1

.field public static final HIDE_PLAYER_CONTROL:I = 0x11

.field public static final HIDE_PLAYER_PICTURE_CONTROL:I = 0x12

.field public static final HIDE_TEXT_STATE:I = 0x14

.field static final NONE:I = 0x0

.field public static final OPTION_STATE_BACK:I = 0x2

.field public static final OPTION_STATE_BIG:I = 0x8

.field public static final OPTION_STATE_FULLSCREEN:I = 0x10

.field public static final OPTION_STATE_GALLERY:I = 0x1

.field public static final OPTION_STATE_NEXT:I = 0x4

.field public static final OPTION_STATE_PLAY:I = 0x3

.field public static final OPTION_STATE_SMALL:I = 0x9

.field public static final OPTION_STATE_STOP:I = 0x5

.field public static final OPTION_STATE_TURNLEFT:I = 0x6

.field public static final OPTION_STATE_TURNRIGHT:I = 0x7

.field public static final PROGRESS_DIALOG_LOAD:I = 0x15

.field public static final SHOW_PLAYER_PICTURE_CONTROL:I = 0x13

.field static final ZOOM:I = 0x2

.field private static final ZOOM_IN_SCALE:D = 1.25

.field private static final ZOOM_OUT_SCALE:D = 0.8

.field protected static descaleDrawable:[I = null

.field public static final downloadPicture:Ljava/lang/String; = "downloadPicture......"

.field protected static enlargeDrawable:[I = null

.field protected static isCanMove:Z = false

.field private static isProgressDlg:Z = false

.field protected static keyResponse:Z = false

.field private static final picTag:Ljava/lang/String; = "MultiScreenPictureActivity"

.field private static progressDialog:Landroid/app/ProgressDialog;


# instance fields
.field protected final PPT_PLAYER:I

.field private autoPlayflag:I

.field protected bitmapArray:[Lcom/konka/picturePlayer/RotateBitmap;

.field bmp:Landroid/graphics/Bitmap;

.field clockThread:Ljava/lang/Thread;

.field close:Lcom/konka/picturePlayer/picturePlayerActivity$closeReceiver;

.field private comefrom:I

.field private currentNum:I

.field dataReceiver:Lcom/konka/picturePlayer/picturePlayerActivity$DataReceiver;

.field public downLoad_state:Lcom/konka/picturePlayer/picturePlayerActivity$DOWNLOAD_STATE;

.field private downloadhandler:Landroid/os/Handler;

.field endDown:Z

.field file:Ljava/io/File;

.field filesize:J

.field protected handler:Landroid/os/Handler;

.field private haveNextFlag:I

.field protected hideHandler:Landroid/os/Handler;

.field private id:I

.field imageFile:[Ljava/lang/String;

.field imageFileList:[Ljava/io/File;

.field private imageview:Landroid/widget/ImageView;

.field protected imgPost:I

.field private isAutoPlaying:Z

.field private isControllerShow:Z

.field isCreat:Z

.field isImagePlay:Z

.field private isInitScrollimgview:Z

.field private isPlaying:Z

.field private isdown:I

.field isrun:Z

.field private layout1:Landroid/widget/LinearLayout;

.field protected mAlphaIn:Landroid/view/animation/Animation;

.field protected mAlphaOut:Landroid/view/animation/Animation;

.field protected mPicControlInAnim:Landroid/view/animation/Animation;

.field protected mPicControlOutAnim:Landroid/view/animation/Animation;

.field protected mPicPushLeftIn:Landroid/view/animation/Animation;

.field protected mPicPushRightIn:Landroid/view/animation/Animation;

.field private mReceiver:Landroid/content/BroadcastReceiver;

.field protected mRotation:Landroid/view/animation/Animation;

.field matrix:Landroid/graphics/Matrix;

.field mediaPlayerSwitch:Lcom/konka/picturePlayer/picturePlayerActivity$switchReceiver;

.field mediaSwitch:I

.field mediastop:I

.field mid:Landroid/graphics/PointF;

.field private mobileIp:Ljava/lang/String;

.field mode:I

.field private nextFlag:I

.field oldDist:F

.field protected parentPath:Ljava/lang/String;

.field private parentpath:Ljava/lang/String;

.field photoGallery:Landroid/widget/Gallery;

.field private picAutoPlay:Z

.field picFile:Ljava/io/File;

.field picFilePath:Ljava/lang/String;

.field private picImageview:Lcom/konka/picturePlayer/ImageViewTouch;

.field picPaths:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation
.end field

.field picPaths_broadcast:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation
.end field

.field protected picShowHolder:Lcom/konka/picturePlayer/PicShowHolder;

.field picturePath:Ljava/lang/String;

.field picturePath1:Ljava/lang/String;

.field protected playStateDrawable:[I

.field private proDlg:Landroid/app/ProgressDialog;

.field protected rotateBitmap:Lcom/konka/picturePlayer/RotateBitmap;

.field protected rotateTimes:I

.field private rotationClickCount:I

.field savedMatrix:Landroid/graphics/Matrix;

.field private scaleHeight:F

.field private scaleHeight1:F

.field private scaleImageView:Landroid/widget/ImageView;

.field private scaleWidth:F

.field private scaleWidth1:F

.field protected sourcePath:Ljava/lang/String;

.field start:Landroid/graphics/PointF;

.field private state:I

.field private stateDrawable:Landroid/widget/ImageView;

.field private textView:Landroid/widget/TextView;

.field protected zoomTimes:I

.field private zoom_level:I


# direct methods
.method static constructor <clinit>()V
    .locals 3

    const/4 v2, 0x5

    const/4 v1, 0x0

    sput-boolean v1, Lcom/konka/picturePlayer/picturePlayerActivity;->isCanMove:Z

    new-array v0, v2, [I

    fill-array-data v0, :array_0

    sput-object v0, Lcom/konka/picturePlayer/picturePlayerActivity;->enlargeDrawable:[I

    new-array v0, v2, [I

    fill-array-data v0, :array_1

    sput-object v0, Lcom/konka/picturePlayer/picturePlayerActivity;->descaleDrawable:[I

    sput-boolean v1, Lcom/konka/picturePlayer/picturePlayerActivity;->isProgressDlg:Z

    sput-boolean v1, Lcom/konka/picturePlayer/picturePlayerActivity;->keyResponse:Z

    return-void

    nop

    :array_0
    .array-data 4
        0x7f0200a2    # com.konka.mediaSharePlayer.R.drawable.pic_icon_enlarge_uns
        0x7f02009e    # com.konka.mediaSharePlayer.R.drawable.pic_icon_enlarge2_s
        0x7f02009f    # com.konka.mediaSharePlayer.R.drawable.pic_icon_enlarge4_s
        0x7f0200a0    # com.konka.mediaSharePlayer.R.drawable.pic_icon_enlarge8_s
        0x7f02009d    # com.konka.mediaSharePlayer.R.drawable.pic_icon_enlarge12_s
    .end array-data

    :array_1
    .array-data 4
        0x7f02009c    # com.konka.mediaSharePlayer.R.drawable.pic_icon_descale_uns
        0x7f020098    # com.konka.mediaSharePlayer.R.drawable.pic_icon_descale2_s
        0x7f020099    # com.konka.mediaSharePlayer.R.drawable.pic_icon_descale4_s
        0x7f02009a    # com.konka.mediaSharePlayer.R.drawable.pic_icon_descale8_s
        0x7f020097    # com.konka.mediaSharePlayer.R.drawable.pic_icon_descale12_s
    .end array-data
.end method

.method public constructor <init>()V
    .locals 6

    const/4 v5, 0x0

    const/high16 v4, 0x3f800000    # 1.0f

    const/4 v3, 0x1

    const/4 v2, 0x0

    invoke-direct {p0}, Landroid/app/Activity;-><init>()V

    iput v3, p0, Lcom/konka/picturePlayer/picturePlayerActivity;->state:I

    iput-boolean v3, p0, Lcom/konka/picturePlayer/picturePlayerActivity;->isControllerShow:Z

    iput v2, p0, Lcom/konka/picturePlayer/picturePlayerActivity;->id:I

    iput v2, p0, Lcom/konka/picturePlayer/picturePlayerActivity;->currentNum:I

    iput v2, p0, Lcom/konka/picturePlayer/picturePlayerActivity;->autoPlayflag:I

    iput-boolean v3, p0, Lcom/konka/picturePlayer/picturePlayerActivity;->isAutoPlaying:Z

    iput v2, p0, Lcom/konka/picturePlayer/picturePlayerActivity;->rotationClickCount:I

    iput v2, p0, Lcom/konka/picturePlayer/picturePlayerActivity;->isdown:I

    iput v2, p0, Lcom/konka/picturePlayer/picturePlayerActivity;->haveNextFlag:I

    iput v2, p0, Lcom/konka/picturePlayer/picturePlayerActivity;->nextFlag:I

    iput v2, p0, Lcom/konka/picturePlayer/picturePlayerActivity;->comefrom:I

    iput-object v5, p0, Lcom/konka/picturePlayer/picturePlayerActivity;->mobileIp:Ljava/lang/String;

    iput-boolean v3, p0, Lcom/konka/picturePlayer/picturePlayerActivity;->picAutoPlay:Z

    iput-object v5, p0, Lcom/konka/picturePlayer/picturePlayerActivity;->file:Ljava/io/File;

    iput-object v5, p0, Lcom/konka/picturePlayer/picturePlayerActivity;->picFile:Ljava/io/File;

    iput-boolean v2, p0, Lcom/konka/picturePlayer/picturePlayerActivity;->isImagePlay:Z

    const/4 v0, 0x4

    new-array v0, v0, [I

    fill-array-data v0, :array_0

    iput-object v0, p0, Lcom/konka/picturePlayer/picturePlayerActivity;->playStateDrawable:[I

    iput v4, p0, Lcom/konka/picturePlayer/picturePlayerActivity;->scaleWidth:F

    iput v4, p0, Lcom/konka/picturePlayer/picturePlayerActivity;->scaleHeight:F

    iput-boolean v2, p0, Lcom/konka/picturePlayer/picturePlayerActivity;->isInitScrollimgview:Z

    iput v2, p0, Lcom/konka/picturePlayer/picturePlayerActivity;->zoomTimes:I

    iput v2, p0, Lcom/konka/picturePlayer/picturePlayerActivity;->rotateTimes:I

    const/4 v0, -0x1

    iput v0, p0, Lcom/konka/picturePlayer/picturePlayerActivity;->imgPost:I

    const/4 v0, 0x3

    new-array v0, v0, [Lcom/konka/picturePlayer/RotateBitmap;

    iput-object v0, p0, Lcom/konka/picturePlayer/picturePlayerActivity;->bitmapArray:[Lcom/konka/picturePlayer/RotateBitmap;

    const v0, 0x50000001

    iput v0, p0, Lcom/konka/picturePlayer/picturePlayerActivity;->PPT_PLAYER:I

    iput v3, p0, Lcom/konka/picturePlayer/picturePlayerActivity;->mediaSwitch:I

    const-wide/16 v0, 0x0

    iput-wide v0, p0, Lcom/konka/picturePlayer/picturePlayerActivity;->filesize:J

    iput v3, p0, Lcom/konka/picturePlayer/picturePlayerActivity;->mediastop:I

    const-string v0, ""

    iput-object v0, p0, Lcom/konka/picturePlayer/picturePlayerActivity;->picturePath:Ljava/lang/String;

    const-string v0, ""

    iput-object v0, p0, Lcom/konka/picturePlayer/picturePlayerActivity;->picturePath1:Ljava/lang/String;

    const-string v0, ""

    iput-object v0, p0, Lcom/konka/picturePlayer/picturePlayerActivity;->picFilePath:Ljava/lang/String;

    iput-object v5, p0, Lcom/konka/picturePlayer/picturePlayerActivity;->imageFileList:[Ljava/io/File;

    iput-boolean v2, p0, Lcom/konka/picturePlayer/picturePlayerActivity;->isPlaying:Z

    iput-boolean v2, p0, Lcom/konka/picturePlayer/picturePlayerActivity;->isCreat:Z

    iput-boolean v2, p0, Lcom/konka/picturePlayer/picturePlayerActivity;->endDown:Z

    iput-boolean v2, p0, Lcom/konka/picturePlayer/picturePlayerActivity;->isrun:Z

    sget-object v0, Lcom/konka/picturePlayer/picturePlayerActivity$DOWNLOAD_STATE;->download_continue:Lcom/konka/picturePlayer/picturePlayerActivity$DOWNLOAD_STATE;

    iput-object v0, p0, Lcom/konka/picturePlayer/picturePlayerActivity;->downLoad_state:Lcom/konka/picturePlayer/picturePlayerActivity$DOWNLOAD_STATE;

    new-instance v0, Lcom/konka/picturePlayer/picturePlayerActivity$1;

    invoke-direct {v0, p0}, Lcom/konka/picturePlayer/picturePlayerActivity$1;-><init>(Lcom/konka/picturePlayer/picturePlayerActivity;)V

    iput-object v0, p0, Lcom/konka/picturePlayer/picturePlayerActivity;->handler:Landroid/os/Handler;

    new-instance v0, Lcom/konka/picturePlayer/picturePlayerActivity$2;

    invoke-direct {v0, p0}, Lcom/konka/picturePlayer/picturePlayerActivity$2;-><init>(Lcom/konka/picturePlayer/picturePlayerActivity;)V

    iput-object v0, p0, Lcom/konka/picturePlayer/picturePlayerActivity;->downloadhandler:Landroid/os/Handler;

    new-instance v0, Lcom/konka/picturePlayer/picturePlayerActivity$3;

    invoke-direct {v0, p0}, Lcom/konka/picturePlayer/picturePlayerActivity$3;-><init>(Lcom/konka/picturePlayer/picturePlayerActivity;)V

    iput-object v0, p0, Lcom/konka/picturePlayer/picturePlayerActivity;->hideHandler:Landroid/os/Handler;

    new-instance v0, Landroid/graphics/Matrix;

    invoke-direct {v0}, Landroid/graphics/Matrix;-><init>()V

    iput-object v0, p0, Lcom/konka/picturePlayer/picturePlayerActivity;->matrix:Landroid/graphics/Matrix;

    new-instance v0, Landroid/graphics/Matrix;

    invoke-direct {v0}, Landroid/graphics/Matrix;-><init>()V

    iput-object v0, p0, Lcom/konka/picturePlayer/picturePlayerActivity;->savedMatrix:Landroid/graphics/Matrix;

    new-instance v0, Landroid/graphics/PointF;

    invoke-direct {v0}, Landroid/graphics/PointF;-><init>()V

    iput-object v0, p0, Lcom/konka/picturePlayer/picturePlayerActivity;->start:Landroid/graphics/PointF;

    new-instance v0, Landroid/graphics/PointF;

    invoke-direct {v0}, Landroid/graphics/PointF;-><init>()V

    iput-object v0, p0, Lcom/konka/picturePlayer/picturePlayerActivity;->mid:Landroid/graphics/PointF;

    iput v4, p0, Lcom/konka/picturePlayer/picturePlayerActivity;->scaleWidth1:F

    iput v4, p0, Lcom/konka/picturePlayer/picturePlayerActivity;->scaleHeight1:F

    iput v2, p0, Lcom/konka/picturePlayer/picturePlayerActivity;->zoom_level:I

    iput v2, p0, Lcom/konka/picturePlayer/picturePlayerActivity;->mode:I

    return-void

    nop

    :array_0
    .array-data 4
        0x7f0200aa    # com.konka.mediaSharePlayer.R.drawable.playstate1
        0x7f0200ac    # com.konka.mediaSharePlayer.R.drawable.playstate3
        0x7f0200ab    # com.konka.mediaSharePlayer.R.drawable.playstate2
        0x7f0200ad    # com.konka.mediaSharePlayer.R.drawable.playstate4
    .end array-data
.end method

.method static synthetic access$0(Lcom/konka/picturePlayer/picturePlayerActivity;)Z
    .locals 1

    iget-boolean v0, p0, Lcom/konka/picturePlayer/picturePlayerActivity;->isAutoPlaying:Z

    return v0
.end method

.method static synthetic access$1(Lcom/konka/picturePlayer/picturePlayerActivity;)I
    .locals 1

    iget v0, p0, Lcom/konka/picturePlayer/picturePlayerActivity;->haveNextFlag:I

    return v0
.end method

.method static synthetic access$10(Lcom/konka/picturePlayer/picturePlayerActivity;Landroid/graphics/PointF;Landroid/view/MotionEvent;)V
    .locals 0

    invoke-direct {p0, p1, p2}, Lcom/konka/picturePlayer/picturePlayerActivity;->midPoint(Landroid/graphics/PointF;Landroid/view/MotionEvent;)V

    return-void
.end method

.method static synthetic access$11(Lcom/konka/picturePlayer/picturePlayerActivity;)Landroid/os/Handler;
    .locals 1

    iget-object v0, p0, Lcom/konka/picturePlayer/picturePlayerActivity;->downloadhandler:Landroid/os/Handler;

    return-object v0
.end method

.method static synthetic access$2(Lcom/konka/picturePlayer/picturePlayerActivity;)I
    .locals 1

    iget v0, p0, Lcom/konka/picturePlayer/picturePlayerActivity;->nextFlag:I

    return v0
.end method

.method static synthetic access$3(Lcom/konka/picturePlayer/picturePlayerActivity;I)V
    .locals 0

    iput p1, p0, Lcom/konka/picturePlayer/picturePlayerActivity;->nextFlag:I

    return-void
.end method

.method static synthetic access$4(Lcom/konka/picturePlayer/picturePlayerActivity;II)V
    .locals 0

    invoke-direct {p0, p1, p2}, Lcom/konka/picturePlayer/picturePlayerActivity;->showProgressDialog(II)V

    return-void
.end method

.method static synthetic access$5(Lcom/konka/picturePlayer/picturePlayerActivity;)Lcom/konka/picturePlayer/ImageViewTouch;
    .locals 1

    iget-object v0, p0, Lcom/konka/picturePlayer/picturePlayerActivity;->picImageview:Lcom/konka/picturePlayer/ImageViewTouch;

    return-object v0
.end method

.method static synthetic access$6(Lcom/konka/picturePlayer/picturePlayerActivity;)Landroid/widget/ImageView;
    .locals 1

    iget-object v0, p0, Lcom/konka/picturePlayer/picturePlayerActivity;->stateDrawable:Landroid/widget/ImageView;

    return-object v0
.end method

.method static synthetic access$7(Lcom/konka/picturePlayer/picturePlayerActivity;I)V
    .locals 0

    iput p1, p0, Lcom/konka/picturePlayer/picturePlayerActivity;->haveNextFlag:I

    return-void
.end method

.method static synthetic access$8(Lcom/konka/picturePlayer/picturePlayerActivity;Ljava/lang/String;)V
    .locals 0

    iput-object p1, p0, Lcom/konka/picturePlayer/picturePlayerActivity;->mobileIp:Ljava/lang/String;

    return-void
.end method

.method static synthetic access$9(Lcom/konka/picturePlayer/picturePlayerActivity;Landroid/view/MotionEvent;)F
    .locals 1

    invoke-direct {p0, p1}, Lcom/konka/picturePlayer/picturePlayerActivity;->spacing(Landroid/view/MotionEvent;)F

    move-result v0

    return v0
.end method

.method private static computeInitialSampleSize(Landroid/graphics/BitmapFactory$Options;II)I
    .locals 12
    .param p0    # Landroid/graphics/BitmapFactory$Options;
    .param p1    # I
    .param p2    # I

    const/4 v6, 0x1

    const/4 v11, -0x1

    iget v7, p0, Landroid/graphics/BitmapFactory$Options;->outWidth:I

    int-to-double v4, v7

    iget v7, p0, Landroid/graphics/BitmapFactory$Options;->outHeight:I

    int-to-double v0, v7

    if-ne p2, v11, :cond_1

    move v2, v6

    :goto_0
    if-ne p1, v11, :cond_2

    const/16 v3, 0x80

    :goto_1
    if-ge v3, v2, :cond_3

    :cond_0
    :goto_2
    return v2

    :cond_1
    mul-double v7, v4, v0

    int-to-double v9, p2

    div-double/2addr v7, v9

    invoke-static {v7, v8}, Ljava/lang/Math;->sqrt(D)D

    move-result-wide v7

    invoke-static {v7, v8}, Ljava/lang/Math;->ceil(D)D

    move-result-wide v7

    double-to-int v2, v7

    goto :goto_0

    :cond_2
    int-to-double v7, p1

    div-double v7, v4, v7

    invoke-static {v7, v8}, Ljava/lang/Math;->floor(D)D

    move-result-wide v7

    int-to-double v9, p1

    div-double v9, v0, v9

    invoke-static {v9, v10}, Ljava/lang/Math;->floor(D)D

    move-result-wide v9

    invoke-static {v7, v8, v9, v10}, Ljava/lang/Math;->min(DD)D

    move-result-wide v7

    double-to-int v3, v7

    goto :goto_1

    :cond_3
    if-ne p2, v11, :cond_4

    if-ne p1, v11, :cond_4

    move v2, v6

    goto :goto_2

    :cond_4
    if-eq p1, v11, :cond_0

    move v2, v3

    goto :goto_2
.end method

.method public static computeSampleSize(Landroid/graphics/BitmapFactory$Options;II)I
    .locals 3
    .param p0    # Landroid/graphics/BitmapFactory$Options;
    .param p1    # I
    .param p2    # I

    invoke-static {p0, p1, p2}, Lcom/konka/picturePlayer/picturePlayerActivity;->computeInitialSampleSize(Landroid/graphics/BitmapFactory$Options;II)I

    move-result v0

    const/16 v2, 0x8

    if-gt v0, v2, :cond_1

    const/4 v1, 0x1

    :goto_0
    if-lt v1, v0, :cond_0

    :goto_1
    return v1

    :cond_0
    shl-int/lit8 v1, v1, 0x1

    goto :goto_0

    :cond_1
    add-int/lit8 v2, v0, 0x7

    div-int/lit8 v2, v2, 0x8

    mul-int/lit8 v1, v2, 0x8

    goto :goto_1
.end method

.method private midPoint(Landroid/graphics/PointF;Landroid/view/MotionEvent;)V
    .locals 7
    .param p1    # Landroid/graphics/PointF;
    .param p2    # Landroid/view/MotionEvent;

    const/4 v6, 0x1

    const/4 v5, 0x0

    const/high16 v4, 0x40000000    # 2.0f

    invoke-virtual {p2, v5}, Landroid/view/MotionEvent;->getX(I)F

    move-result v2

    invoke-virtual {p2, v6}, Landroid/view/MotionEvent;->getX(I)F

    move-result v3

    add-float v0, v2, v3

    invoke-virtual {p2, v5}, Landroid/view/MotionEvent;->getY(I)F

    move-result v2

    invoke-virtual {p2, v6}, Landroid/view/MotionEvent;->getY(I)F

    move-result v3

    add-float v1, v2, v3

    div-float v2, v0, v4

    div-float v3, v1, v4

    invoke-virtual {p1, v2, v3}, Landroid/graphics/PointF;->set(FF)V

    return-void
.end method

.method private setDialogText(Landroid/view/View;)V
    .locals 5
    .param p1    # Landroid/view/View;

    instance-of v4, p1, Landroid/view/ViewGroup;

    if-eqz v4, :cond_2

    move-object v3, p1

    check-cast v3, Landroid/view/ViewGroup;

    const v4, 0x7f070006    # com.konka.mediaSharePlayer.R.color.black1

    invoke-virtual {p1, v4}, Landroid/view/View;->setBackgroundColor(I)V

    invoke-virtual {v3}, Landroid/view/ViewGroup;->getChildCount()I

    move-result v1

    const/4 v2, 0x0

    :goto_0
    if-lt v2, v1, :cond_1

    :cond_0
    :goto_1
    return-void

    :cond_1
    invoke-virtual {v3, v2}, Landroid/view/ViewGroup;->getChildAt(I)Landroid/view/View;

    move-result-object v0

    invoke-direct {p0, v0}, Lcom/konka/picturePlayer/picturePlayerActivity;->setDialogText(Landroid/view/View;)V

    add-int/lit8 v2, v2, 0x1

    goto :goto_0

    :cond_2
    instance-of v4, p1, Landroid/widget/TextView;

    if-eqz v4, :cond_0

    check-cast p1, Landroid/widget/TextView;

    const/high16 v4, 0x41700000    # 15.0f

    invoke-virtual {p1, v4}, Landroid/widget/TextView;->setTextSize(F)V

    goto :goto_1
.end method

.method private showController()V
    .locals 3

    const/4 v2, 0x0

    sput-boolean v2, Lcom/konka/picturePlayer/picturePlayerActivity;->isCanMove:Z

    iget-object v0, p0, Lcom/konka/picturePlayer/picturePlayerActivity;->picShowHolder:Lcom/konka/picturePlayer/PicShowHolder;

    iget-object v0, v0, Lcom/konka/picturePlayer/PicShowHolder;->pic_bts_layout:Landroid/widget/RelativeLayout;

    invoke-virtual {v0, v2}, Landroid/widget/RelativeLayout;->setVisibility(I)V

    iget-object v0, p0, Lcom/konka/picturePlayer/picturePlayerActivity;->textView:Landroid/widget/TextView;

    invoke-virtual {v0, v2}, Landroid/widget/TextView;->setVisibility(I)V

    iget-object v0, p0, Lcom/konka/picturePlayer/picturePlayerActivity;->stateDrawable:Landroid/widget/ImageView;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Landroid/widget/ImageView;->setImageBitmap(Landroid/graphics/Bitmap;)V

    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/konka/picturePlayer/picturePlayerActivity;->isControllerShow:Z

    iget-object v0, p0, Lcom/konka/picturePlayer/picturePlayerActivity;->picImageview:Lcom/konka/picturePlayer/ImageViewTouch;

    invoke-virtual {v0, v2}, Lcom/konka/picturePlayer/ImageViewTouch;->setFocusable(Z)V

    return-void
.end method

.method private showProgressDialog(II)V
    .locals 3
    .param p1    # I
    .param p2    # I

    iget-object v1, p0, Lcom/konka/picturePlayer/picturePlayerActivity;->proDlg:Landroid/app/ProgressDialog;

    if-nez v1, :cond_0

    new-instance v1, Landroid/app/ProgressDialog;

    invoke-direct {v1, p0}, Landroid/app/ProgressDialog;-><init>(Landroid/content/Context;)V

    iput-object v1, p0, Lcom/konka/picturePlayer/picturePlayerActivity;->proDlg:Landroid/app/ProgressDialog;

    :cond_0
    iget-object v1, p0, Lcom/konka/picturePlayer/picturePlayerActivity;->proDlg:Landroid/app/ProgressDialog;

    invoke-virtual {p0}, Lcom/konka/picturePlayer/picturePlayerActivity;->getResources()Landroid/content/res/Resources;

    move-result-object v2

    invoke-virtual {v2, p2}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Landroid/app/ProgressDialog;->setMessage(Ljava/lang/CharSequence;)V

    iget-object v1, p0, Lcom/konka/picturePlayer/picturePlayerActivity;->proDlg:Landroid/app/ProgressDialog;

    invoke-virtual {v1}, Landroid/app/ProgressDialog;->show()V

    iget-object v1, p0, Lcom/konka/picturePlayer/picturePlayerActivity;->proDlg:Landroid/app/ProgressDialog;

    invoke-virtual {v1}, Landroid/app/ProgressDialog;->getWindow()Landroid/view/Window;

    move-result-object v1

    invoke-virtual {v1}, Landroid/view/Window;->getDecorView()Landroid/view/View;

    move-result-object v0

    invoke-direct {p0, v0}, Lcom/konka/picturePlayer/picturePlayerActivity;->setDialogText(Landroid/view/View;)V

    return-void
.end method

.method private spacing(Landroid/view/MotionEvent;)F
    .locals 6
    .param p1    # Landroid/view/MotionEvent;

    const/4 v5, 0x1

    const/4 v4, 0x0

    invoke-virtual {p1, v4}, Landroid/view/MotionEvent;->getX(I)F

    move-result v2

    invoke-virtual {p1, v5}, Landroid/view/MotionEvent;->getX(I)F

    move-result v3

    sub-float v0, v2, v3

    invoke-virtual {p1, v4}, Landroid/view/MotionEvent;->getY(I)F

    move-result v2

    invoke-virtual {p1, v5}, Landroid/view/MotionEvent;->getY(I)F

    move-result v3

    sub-float v1, v2, v3

    mul-float v2, v0, v0

    mul-float v3, v1, v1

    add-float/2addr v2, v3

    invoke-static {v2}, Landroid/util/FloatMath;->sqrt(F)F

    move-result v2

    return v2
.end method


# virtual methods
.method public InitData()V
    .locals 9

    const/4 v8, 0x1

    const/4 v7, 0x0

    const v4, 0x7f0b0013    # com.konka.mediaSharePlayer.R.id.showview

    invoke-virtual {p0, v4}, Lcom/konka/picturePlayer/picturePlayerActivity;->findViewById(I)Landroid/view/View;

    move-result-object v4

    check-cast v4, Lcom/konka/picturePlayer/ImageViewTouch;

    iput-object v4, p0, Lcom/konka/picturePlayer/picturePlayerActivity;->picImageview:Lcom/konka/picturePlayer/ImageViewTouch;

    iget-object v4, p0, Lcom/konka/picturePlayer/picturePlayerActivity;->picImageview:Lcom/konka/picturePlayer/ImageViewTouch;

    iget-object v5, p0, Lcom/konka/picturePlayer/picturePlayerActivity;->mAlphaIn:Landroid/view/animation/Animation;

    invoke-virtual {v4, v5}, Lcom/konka/picturePlayer/ImageViewTouch;->setAnimation(Landroid/view/animation/Animation;)V

    const v4, 0x7f0b000e    # com.konka.mediaSharePlayer.R.id.textview1

    invoke-virtual {p0, v4}, Lcom/konka/picturePlayer/picturePlayerActivity;->findViewById(I)Landroid/view/View;

    move-result-object v4

    check-cast v4, Landroid/widget/TextView;

    iput-object v4, p0, Lcom/konka/picturePlayer/picturePlayerActivity;->textView:Landroid/widget/TextView;

    const v4, 0x7f0b000f    # com.konka.mediaSharePlayer.R.id.state_image

    invoke-virtual {p0, v4}, Lcom/konka/picturePlayer/picturePlayerActivity;->findViewById(I)Landroid/view/View;

    move-result-object v4

    check-cast v4, Landroid/widget/ImageView;

    iput-object v4, p0, Lcom/konka/picturePlayer/picturePlayerActivity;->stateDrawable:Landroid/widget/ImageView;

    const v4, 0x7f0b0010    # com.konka.mediaSharePlayer.R.id.scale_btn

    invoke-virtual {p0, v4}, Lcom/konka/picturePlayer/picturePlayerActivity;->findViewById(I)Landroid/view/View;

    move-result-object v4

    check-cast v4, Landroid/widget/ImageView;

    iput-object v4, p0, Lcom/konka/picturePlayer/picturePlayerActivity;->scaleImageView:Landroid/widget/ImageView;

    const v4, 0x7f0b0011    # com.konka.mediaSharePlayer.R.id.layout1

    invoke-virtual {p0, v4}, Lcom/konka/picturePlayer/picturePlayerActivity;->findViewById(I)Landroid/view/View;

    move-result-object v4

    check-cast v4, Landroid/widget/LinearLayout;

    iput-object v4, p0, Lcom/konka/picturePlayer/picturePlayerActivity;->layout1:Landroid/widget/LinearLayout;

    const-string v4, "/data/data/com.konka.mediaSharePlayer/cache"

    iput-object v4, p0, Lcom/konka/picturePlayer/picturePlayerActivity;->picFilePath:Ljava/lang/String;

    new-instance v4, Ljava/io/File;

    iget-object v5, p0, Lcom/konka/picturePlayer/picturePlayerActivity;->picFilePath:Ljava/lang/String;

    invoke-direct {v4, v5}, Ljava/io/File;-><init>(Ljava/lang/String;)V

    iput-object v4, p0, Lcom/konka/picturePlayer/picturePlayerActivity;->file:Ljava/io/File;

    iget-object v4, p0, Lcom/konka/picturePlayer/picturePlayerActivity;->file:Ljava/io/File;

    invoke-virtual {v4}, Ljava/io/File;->exists()Z

    move-result v4

    if-eqz v4, :cond_0

    const-string v4, "MultiScreenPictureActivity"

    const-string v5, "downloadPicture......file.exists() not "

    invoke-static {v4, v5}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    iget-object v4, p0, Lcom/konka/picturePlayer/picturePlayerActivity;->file:Ljava/io/File;

    invoke-virtual {v4}, Ljava/io/File;->mkdirs()Z

    :cond_0
    invoke-virtual {p0}, Lcom/konka/picturePlayer/picturePlayerActivity;->getIntent()Landroid/content/Intent;

    move-result-object v1

    invoke-virtual {v1}, Landroid/content/Intent;->getExtras()Landroid/os/Bundle;

    move-result-object v0

    new-instance v4, Ljava/util/ArrayList;

    invoke-direct {v4}, Ljava/util/ArrayList;-><init>()V

    iput-object v4, p0, Lcom/konka/picturePlayer/picturePlayerActivity;->picPaths:Ljava/util/List;

    const-string v4, "picPaths"

    invoke-virtual {v0, v4}, Landroid/os/Bundle;->getStringArrayList(Ljava/lang/String;)Ljava/util/ArrayList;

    move-result-object v4

    iput-object v4, p0, Lcom/konka/picturePlayer/picturePlayerActivity;->picPaths:Ljava/util/List;

    const-string v4, "filesize"

    const-wide/16 v5, 0x0

    invoke-virtual {v0, v4, v5, v6}, Landroid/os/Bundle;->getLong(Ljava/lang/String;J)J

    move-result-wide v4

    iput-wide v4, p0, Lcom/konka/picturePlayer/picturePlayerActivity;->filesize:J

    const-string v4, "haveNextFlag"

    invoke-virtual {v0, v4, v7}, Landroid/os/Bundle;->getInt(Ljava/lang/String;I)I

    move-result v4

    iput v4, p0, Lcom/konka/picturePlayer/picturePlayerActivity;->haveNextFlag:I

    const-string v4, "mediastop"

    invoke-virtual {v0, v4, v8}, Landroid/os/Bundle;->getInt(Ljava/lang/String;I)I

    move-result v4

    iput v4, p0, Lcom/konka/picturePlayer/picturePlayerActivity;->mediastop:I

    const-string v4, "comefrom"

    invoke-virtual {v0, v4, v7}, Landroid/os/Bundle;->getInt(Ljava/lang/String;I)I

    move-result v4

    iput v4, p0, Lcom/konka/picturePlayer/picturePlayerActivity;->comefrom:I

    iget-object v4, p0, Lcom/konka/picturePlayer/picturePlayerActivity;->mobileIp:Ljava/lang/String;

    invoke-virtual {v0, v4}, Landroid/os/Bundle;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v4

    iput-object v4, p0, Lcom/konka/picturePlayer/picturePlayerActivity;->mobileIp:Ljava/lang/String;

    const-string v4, "MultiScreenPictureActivity"

    new-instance v5, Ljava/lang/StringBuilder;

    const-string v6, "DataReceiver () haveNextFlag = "

    invoke-direct {v5, v6}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    iget v6, p0, Lcom/konka/picturePlayer/picturePlayerActivity;->haveNextFlag:I

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    invoke-static {v4, v5}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    iget-object v4, p0, Lcom/konka/picturePlayer/picturePlayerActivity;->picPaths:Ljava/util/List;

    invoke-interface {v4, v7}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Ljava/lang/String;

    const-string v4, "/"

    invoke-virtual {v3, v4}, Ljava/lang/String;->lastIndexOf(Ljava/lang/String;)I

    move-result v4

    add-int/lit8 v4, v4, 0x1

    invoke-virtual {v3}, Ljava/lang/String;->length()I

    move-result v5

    invoke-virtual {v3, v4, v5}, Ljava/lang/String;->substring(II)Ljava/lang/String;

    move-result-object v2

    const-string v4, "mediastop"

    invoke-virtual {v0, v4, v8}, Landroid/os/Bundle;->getInt(Ljava/lang/String;I)I

    move-result v4

    iput v4, p0, Lcom/konka/picturePlayer/picturePlayerActivity;->mediastop:I

    iget v4, p0, Lcom/konka/picturePlayer/picturePlayerActivity;->nextFlag:I

    iget v5, p0, Lcom/konka/picturePlayer/picturePlayerActivity;->haveNextFlag:I

    add-int/2addr v4, v5

    iput v4, p0, Lcom/konka/picturePlayer/picturePlayerActivity;->nextFlag:I

    invoke-virtual {p0, v2}, Lcom/konka/picturePlayer/picturePlayerActivity;->playImage(Ljava/lang/String;)V

    iget-object v4, p0, Lcom/konka/picturePlayer/picturePlayerActivity;->picImageview:Lcom/konka/picturePlayer/ImageViewTouch;

    new-instance v5, Lcom/konka/picturePlayer/picturePlayerActivity$6;

    invoke-direct {v5, p0}, Lcom/konka/picturePlayer/picturePlayerActivity$6;-><init>(Lcom/konka/picturePlayer/picturePlayerActivity;)V

    invoke-virtual {v4, v5}, Lcom/konka/picturePlayer/ImageViewTouch;->setOnKeyListener(Landroid/view/View$OnKeyListener;)V

    return-void
.end method

.method public bitmapPicturePlay(Ljava/lang/String;)V
    .locals 5
    .param p1    # Ljava/lang/String;

    const-string v2, "bitmapPicturePlay"

    new-instance v3, Ljava/lang/StringBuilder;

    const-string v4, "bitmapPicturePlay path: "

    invoke-direct {v3, v4}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v3, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v2, v3}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    new-instance v1, Landroid/graphics/BitmapFactory$Options;

    invoke-direct {v1}, Landroid/graphics/BitmapFactory$Options;-><init>()V

    const/4 v2, -0x1

    const v3, 0xe1000

    invoke-static {v1, v2, v3}, Lcom/konka/picturePlayer/picturePlayerActivity;->computeSampleSize(Landroid/graphics/BitmapFactory$Options;II)I

    move-result v2

    iput v2, v1, Landroid/graphics/BitmapFactory$Options;->inSampleSize:I

    invoke-static {p1}, Lcom/konka/picturePlayer/DecodeImageTool;->decodeBitmap(Ljava/lang/String;)Landroid/graphics/Bitmap;

    move-result-object v0

    iget v2, p0, Lcom/konka/picturePlayer/picturePlayerActivity;->rotateTimes:I

    if-nez v2, :cond_2

    iget v2, p0, Lcom/konka/picturePlayer/picturePlayerActivity;->zoomTimes:I

    if-nez v2, :cond_2

    iget-object v2, p0, Lcom/konka/picturePlayer/picturePlayerActivity;->picImageview:Lcom/konka/picturePlayer/ImageViewTouch;

    invoke-virtual {v2, v0}, Lcom/konka/picturePlayer/ImageViewTouch;->setImageBitmap(Landroid/graphics/Bitmap;)V

    :cond_0
    :goto_0
    invoke-virtual {p0}, Lcom/konka/picturePlayer/picturePlayerActivity;->hideController()V

    iget-boolean v2, p0, Lcom/konka/picturePlayer/picturePlayerActivity;->isAutoPlaying:Z

    if-eqz v2, :cond_1

    iget-object v2, p0, Lcom/konka/picturePlayer/picturePlayerActivity;->playStateDrawable:[I

    const/4 v3, 0x1

    aget v2, v2, v3

    invoke-virtual {p0, v2}, Lcom/konka/picturePlayer/picturePlayerActivity;->setStateDrawable(I)V

    :cond_1
    return-void

    :cond_2
    iget v2, p0, Lcom/konka/picturePlayer/picturePlayerActivity;->rotateTimes:I

    if-eqz v2, :cond_3

    iget-object v2, p0, Lcom/konka/picturePlayer/picturePlayerActivity;->picImageview:Lcom/konka/picturePlayer/ImageViewTouch;

    iget v3, p0, Lcom/konka/picturePlayer/picturePlayerActivity;->rotateTimes:I

    mul-int/lit8 v3, v3, -0x5a

    int-to-float v3, v3

    invoke-virtual {v2, v3}, Lcom/konka/picturePlayer/ImageViewTouch;->rotateImage(F)V

    iget-object v2, p0, Lcom/konka/picturePlayer/picturePlayerActivity;->picImageview:Lcom/konka/picturePlayer/ImageViewTouch;

    invoke-virtual {v2, v0}, Lcom/konka/picturePlayer/ImageViewTouch;->setImageBitmap(Landroid/graphics/Bitmap;)V

    const/4 v2, 0x0

    iput v2, p0, Lcom/konka/picturePlayer/picturePlayerActivity;->rotateTimes:I

    :cond_3
    iget v2, p0, Lcom/konka/picturePlayer/picturePlayerActivity;->zoomTimes:I

    if-eqz v2, :cond_0

    invoke-virtual {p0}, Lcom/konka/picturePlayer/picturePlayerActivity;->resetZoom()V

    iget-object v2, p0, Lcom/konka/picturePlayer/picturePlayerActivity;->picImageview:Lcom/konka/picturePlayer/ImageViewTouch;

    invoke-virtual {v2, v0}, Lcom/konka/picturePlayer/ImageViewTouch;->setImageBitmap(Landroid/graphics/Bitmap;)V

    goto :goto_0
.end method

.method public cancelProgressDlg()V
    .locals 1

    iget-object v0, p0, Lcom/konka/picturePlayer/picturePlayerActivity;->proDlg:Landroid/app/ProgressDialog;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/konka/picturePlayer/picturePlayerActivity;->proDlg:Landroid/app/ProgressDialog;

    invoke-virtual {v0}, Landroid/app/ProgressDialog;->dismiss()V

    const/4 v0, 0x0

    iput-object v0, p0, Lcom/konka/picturePlayer/picturePlayerActivity;->proDlg:Landroid/app/ProgressDialog;

    :cond_0
    return-void
.end method

.method public compareModifyTime([Ljava/io/File;)Ljava/io/File;
    .locals 7
    .param p1    # [Ljava/io/File;

    const/4 v0, 0x0

    :goto_0
    array-length v3, p1

    if-lt v0, v3, :cond_0

    const/4 v3, 0x0

    aget-object v3, p1, v3

    return-object v3

    :cond_0
    const/4 v1, 0x1

    :goto_1
    array-length v3, p1

    if-lt v1, v3, :cond_1

    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    :cond_1
    const-string v3, "compareModifyTime"

    new-instance v4, Ljava/lang/StringBuilder;

    const-string v5, "compareModifyTime file[j].lastModified: "

    invoke-direct {v4, v5}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    aget-object v5, p1, v1

    invoke-virtual {v5}, Ljava/io/File;->lastModified()J

    move-result-wide v5

    invoke-virtual {v4, v5, v6}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-static {v3, v4}, Landroid/util/Log;->v(Ljava/lang/String;Ljava/lang/String;)I

    add-int/lit8 v3, v1, -0x1

    aget-object v3, p1, v3

    invoke-virtual {v3}, Ljava/io/File;->lastModified()J

    move-result-wide v3

    aget-object v5, p1, v1

    invoke-virtual {v5}, Ljava/io/File;->lastModified()J

    move-result-wide v5

    cmp-long v3, v3, v5

    if-lez v3, :cond_2

    aget-object v2, p1, v1

    aget-object v3, p1, v1

    aput-object v3, p1, v1

    aput-object v2, p1, v1

    :cond_2
    add-int/lit8 v1, v1, 0x1

    goto :goto_1
.end method

.method public creatImageFile()V
    .locals 2

    new-instance v0, Ljava/lang/Thread;

    new-instance v1, Lcom/konka/picturePlayer/picturePlayerActivity$5;

    invoke-direct {v1, p0}, Lcom/konka/picturePlayer/picturePlayerActivity$5;-><init>(Lcom/konka/picturePlayer/picturePlayerActivity;)V

    invoke-direct {v0, v1}, Ljava/lang/Thread;-><init>(Ljava/lang/Runnable;)V

    invoke-virtual {v0}, Ljava/lang/Thread;->start()V

    return-void
.end method

.method public delAllFile(Ljava/lang/String;)V
    .locals 6
    .param p1    # Ljava/lang/String;

    new-instance v0, Ljava/io/File;

    invoke-direct {v0, p1}, Ljava/io/File;-><init>(Ljava/lang/String;)V

    invoke-virtual {v0}, Ljava/io/File;->exists()Z

    move-result v4

    if-nez v4, :cond_1

    :cond_0
    return-void

    :cond_1
    invoke-virtual {v0}, Ljava/io/File;->isDirectory()Z

    move-result v4

    if-eqz v4, :cond_0

    invoke-virtual {v0}, Ljava/io/File;->list()[Ljava/lang/String;

    move-result-object v3

    const/4 v2, 0x0

    const/4 v1, 0x0

    :goto_0
    array-length v4, v3

    if-ge v1, v4, :cond_0

    sget-object v4, Ljava/io/File;->separator:Ljava/lang/String;

    invoke-virtual {p1, v4}, Ljava/lang/String;->endsWith(Ljava/lang/String;)Z

    move-result v4

    if-eqz v4, :cond_4

    new-instance v2, Ljava/io/File;

    new-instance v4, Ljava/lang/StringBuilder;

    invoke-static {p1}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v5

    invoke-direct {v4, v5}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    aget-object v5, v3, v1

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-direct {v2, v4}, Ljava/io/File;-><init>(Ljava/lang/String;)V

    :goto_1
    invoke-virtual {v2}, Ljava/io/File;->isFile()Z

    move-result v4

    if-eqz v4, :cond_2

    invoke-virtual {v2}, Ljava/io/File;->delete()Z

    :cond_2
    invoke-virtual {v2}, Ljava/io/File;->isDirectory()Z

    move-result v4

    if-eqz v4, :cond_3

    new-instance v4, Ljava/lang/StringBuilder;

    invoke-static {p1}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v5

    invoke-direct {v4, v5}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    const-string v5, "/"

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    aget-object v5, v3, v1

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {p0, v4}, Lcom/konka/picturePlayer/picturePlayerActivity;->delAllFile(Ljava/lang/String;)V

    new-instance v4, Ljava/lang/StringBuilder;

    invoke-static {p1}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v5

    invoke-direct {v4, v5}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    const-string v5, "/"

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    aget-object v5, v3, v1

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {p0, v4}, Lcom/konka/picturePlayer/picturePlayerActivity;->delFolder(Ljava/lang/String;)V

    :cond_3
    add-int/lit8 v1, v1, 0x1

    goto :goto_0

    :cond_4
    new-instance v2, Ljava/io/File;

    new-instance v4, Ljava/lang/StringBuilder;

    invoke-static {p1}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v5

    invoke-direct {v4, v5}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    sget-object v5, Ljava/io/File;->separator:Ljava/lang/String;

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    aget-object v5, v3, v1

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-direct {v2, v4}, Ljava/io/File;-><init>(Ljava/lang/String;)V

    goto :goto_1
.end method

.method public delFolder(Ljava/lang/String;)V
    .locals 5
    .param p1    # Ljava/lang/String;

    :try_start_0
    invoke-virtual {p0, p1}, Lcom/konka/picturePlayer/picturePlayerActivity;->delAllFile(Ljava/lang/String;)V

    move-object v1, p1

    invoke-virtual {v1}, Ljava/lang/String;->toString()Ljava/lang/String;

    move-result-object v1

    new-instance v2, Ljava/io/File;

    invoke-direct {v2, v1}, Ljava/io/File;-><init>(Ljava/lang/String;)V

    invoke-virtual {v2}, Ljava/io/File;->delete()Z
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    :goto_0
    return-void

    :catch_0
    move-exception v0

    sget-object v3, Ljava/lang/System;->out:Ljava/io/PrintStream;

    const-string v4, "\u027e\ufffd\ufffd\ufffd\u013c\ufffd\ufffd\u0432\ufffd\ufffd\ufffd\ufffd\ufffd\ufffd\ufffd"

    invoke-virtual {v3, v4}, Ljava/io/PrintStream;->println(Ljava/lang/String;)V

    invoke-virtual {v0}, Ljava/lang/Exception;->printStackTrace()V

    goto :goto_0
.end method

.method public down(Ljava/lang/String;Ljava/lang/String;)V
    .locals 34
    .param p1    # Ljava/lang/String;
    .param p2    # Ljava/lang/String;

    const/16 v30, 0x1

    move/from16 v0, v30

    move-object/from16 v1, p0

    iput-boolean v0, v1, Lcom/konka/picturePlayer/picturePlayerActivity;->isAutoPlaying:Z

    const-string v30, "downloadPicture......"

    new-instance v31, Ljava/lang/StringBuilder;

    const-string v32, "path: "

    invoke-direct/range {v31 .. v32}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    move-object/from16 v0, v31

    move-object/from16 v1, p1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v31

    invoke-virtual/range {v31 .. v31}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v31

    invoke-static/range {v30 .. v31}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    const-string v30, "downloadPicture......"

    new-instance v31, Ljava/lang/StringBuilder;

    const-string v32, "url"

    invoke-direct/range {v31 .. v32}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    move-object/from16 v0, v31

    move-object/from16 v1, p2

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v31

    invoke-virtual/range {v31 .. v31}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v31

    invoke-static/range {v30 .. v31}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    const/4 v11, 0x0

    const/16 v17, 0x0

    const/16 v20, 0x0

    const/16 v27, 0x0

    const/4 v15, 0x0

    const-string v30, "/"

    move-object/from16 v0, p1

    move-object/from16 v1, v30

    invoke-virtual {v0, v1}, Ljava/lang/String;->lastIndexOf(Ljava/lang/String;)I

    move-result v30

    add-int/lit8 v30, v30, 0x1

    invoke-virtual/range {p1 .. p1}, Ljava/lang/String;->length()I

    move-result v31

    move-object/from16 v0, p1

    move/from16 v1, v30

    move/from16 v2, v31

    invoke-virtual {v0, v1, v2}, Ljava/lang/String;->substring(II)Ljava/lang/String;

    move-result-object v23

    const/16 v30, 0x0

    const-string v31, "/"

    move-object/from16 v0, p1

    move-object/from16 v1, v31

    invoke-virtual {v0, v1}, Ljava/lang/String;->lastIndexOf(Ljava/lang/String;)I

    move-result v31

    add-int/lit8 v31, v31, 0x1

    move-object/from16 v0, p1

    move/from16 v1, v30

    move/from16 v2, v31

    invoke-virtual {v0, v1, v2}, Ljava/lang/String;->substring(II)Ljava/lang/String;

    move-result-object v6

    new-instance v30, Ljava/lang/StringBuilder;

    invoke-static {v6}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v31

    invoke-direct/range {v30 .. v31}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    move-object/from16 v0, v30

    move-object/from16 v1, v23

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v30

    invoke-virtual/range {v30 .. v30}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v24

    new-instance v10, Ljava/io/File;

    new-instance v30, Ljava/lang/StringBuilder;

    invoke-static/range {v24 .. v24}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v31

    invoke-direct/range {v30 .. v31}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    const-string v31, ".uptemp"

    invoke-virtual/range {v30 .. v31}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v30

    invoke-virtual/range {v30 .. v30}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v30

    move-object/from16 v0, v30

    invoke-direct {v10, v0}, Ljava/io/File;-><init>(Ljava/lang/String;)V

    const-string v30, "downloadPicture......"

    new-instance v31, Ljava/lang/StringBuilder;

    const-string v32, "newpath: "

    invoke-direct/range {v31 .. v32}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    move-object/from16 v0, v31

    move-object/from16 v1, v24

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v31

    invoke-virtual/range {v31 .. v31}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v31

    invoke-static/range {v30 .. v31}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    :try_start_0
    const-string v30, "8085/"

    move-object/from16 v0, p2

    move-object/from16 v1, v30

    invoke-virtual {v0, v1}, Ljava/lang/String;->indexOf(Ljava/lang/String;)I

    move-result v30

    add-int/lit8 v30, v30, 0x5

    invoke-virtual/range {p2 .. p2}, Ljava/lang/String;->length()I

    move-result v31

    move-object/from16 v0, p2

    move/from16 v1, v30

    move/from16 v2, v31

    invoke-virtual {v0, v1, v2}, Ljava/lang/String;->substring(II)Ljava/lang/String;

    move-result-object v28

    const/16 v30, 0x0

    const-string v31, "8085/"

    move-object/from16 v0, p2

    move-object/from16 v1, v31

    invoke-virtual {v0, v1}, Ljava/lang/String;->indexOf(Ljava/lang/String;)I

    move-result v31

    add-int/lit8 v31, v31, 0x5

    move-object/from16 v0, p2

    move/from16 v1, v30

    move/from16 v2, v31

    invoke-virtual {v0, v1, v2}, Ljava/lang/String;->substring(II)Ljava/lang/String;

    move-result-object v18

    const-string v30, "httpstr"

    new-instance v31, Ljava/lang/StringBuilder;

    const-string v32, "httpstr: "

    invoke-direct/range {v31 .. v32}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    move-object/from16 v0, v31

    move-object/from16 v1, v18

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v31

    invoke-virtual/range {v31 .. v31}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v31

    invoke-static/range {v30 .. v31}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    const-string v30, "picname"

    new-instance v31, Ljava/lang/StringBuilder;

    const-string v32, "picname: "

    invoke-direct/range {v31 .. v32}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    move-object/from16 v0, v31

    move-object/from16 v1, v28

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v31

    invoke-virtual/range {v31 .. v31}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v31

    invoke-static/range {v30 .. v31}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    move-object/from16 v0, p0

    iget v0, v0, Lcom/konka/picturePlayer/picturePlayerActivity;->comefrom:I

    move/from16 v30, v0

    if-nez v30, :cond_3

    invoke-static/range {v28 .. v28}, Lcom/konka/mediaSharePlayer/encodeUrl;->encodeUrl(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v25

    const-string v30, "newpicname"

    new-instance v31, Ljava/lang/StringBuilder;

    const-string v32, "newpicname: "

    invoke-direct/range {v31 .. v32}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    move-object/from16 v0, v31

    move-object/from16 v1, v25

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v31

    invoke-virtual/range {v31 .. v31}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v31

    invoke-static/range {v30 .. v31}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    new-instance v30, Ljava/lang/StringBuilder;

    invoke-static/range {v18 .. v18}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v31

    invoke-direct/range {v30 .. v31}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    move-object/from16 v0, v30

    move-object/from16 v1, v25

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v30

    invoke-virtual/range {v30 .. v30}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v26

    const-string v30, "newurl"

    new-instance v31, Ljava/lang/StringBuilder;

    const-string v32, "newurl: "

    invoke-direct/range {v31 .. v32}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    move-object/from16 v0, v31

    move-object/from16 v1, v26

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v31

    invoke-virtual/range {v31 .. v31}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v31

    invoke-static/range {v30 .. v31}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    new-instance v3, Ljava/net/URL;

    move-object/from16 v0, v26

    invoke-direct {v3, v0}, Ljava/net/URL;-><init>(Ljava/lang/String;)V

    const-string v30, "downloadPicture......"

    new-instance v31, Ljava/lang/StringBuilder;

    const-string v32, "url: "

    invoke-direct/range {v31 .. v32}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    move-object/from16 v0, v31

    invoke-virtual {v0, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v31

    invoke-virtual/range {v31 .. v31}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v31

    invoke-static/range {v30 .. v31}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    invoke-virtual {v3}, Ljava/net/URL;->openConnection()Ljava/net/URLConnection;

    move-result-object v30

    move-object/from16 v0, v30

    check-cast v0, Ljava/net/HttpURLConnection;

    move-object/from16 v17, v0

    new-instance v12, Ljava/net/URL;

    move-object/from16 v0, v26

    invoke-direct {v12, v0}, Ljava/net/URL;-><init>(Ljava/lang/String;)V

    move-object v11, v12

    :goto_0
    invoke-virtual {v11}, Ljava/net/URL;->openConnection()Ljava/net/URLConnection;

    move-result-object v5

    check-cast v5, Ljava/net/HttpURLConnection;

    invoke-virtual/range {v17 .. v17}, Ljava/net/HttpURLConnection;->getContentLength()I

    move-result v30

    move/from16 v0, v30

    int-to-long v0, v0

    move-wide/from16 v21, v0

    const/16 v19, 0x0

    :goto_1
    const/16 v30, 0x11

    move/from16 v0, v19

    move/from16 v1, v30

    if-lt v0, v1, :cond_4

    const-wide/16 v30, 0x0

    cmp-long v30, v21, v30

    if-gez v30, :cond_5

    move-object/from16 v0, p0

    iget-wide v0, v0, Lcom/konka/picturePlayer/picturePlayerActivity;->filesize:J

    move-wide/from16 v30, v0

    const-wide/16 v32, 0x0

    cmp-long v30, v30, v32

    if-gtz v30, :cond_5

    invoke-virtual {v10}, Ljava/io/File;->delete()Z

    sget-object v30, Lcom/konka/picturePlayer/picturePlayerActivity$DOWNLOAD_STATE;->download_file_err:Lcom/konka/picturePlayer/picturePlayerActivity$DOWNLOAD_STATE;

    move-object/from16 v0, v30

    move-object/from16 v1, p0

    iput-object v0, v1, Lcom/konka/picturePlayer/picturePlayerActivity;->downLoad_state:Lcom/konka/picturePlayer/picturePlayerActivity$DOWNLOAD_STATE;
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    :goto_2
    if-eqz v20, :cond_2

    :try_start_1
    invoke-virtual/range {v20 .. v20}, Ljava/io/InputStream;->close()V

    if-eqz v27, :cond_0

    invoke-virtual/range {v27 .. v27}, Ljava/io/RandomAccessFile;->close()V

    :cond_0
    if-eqz v15, :cond_1

    invoke-virtual {v15}, Ljava/io/FileOutputStream;->close()V

    :cond_1
    if-eqz v17, :cond_2

    invoke-virtual/range {v17 .. v17}, Ljava/net/HttpURLConnection;->disconnect()V
    :try_end_1
    .catch Ljava/io/IOException; {:try_start_1 .. :try_end_1} :catch_4

    :cond_2
    :goto_3
    return-void

    :cond_3
    :try_start_2
    new-instance v30, Ljava/lang/StringBuilder;

    invoke-static/range {v18 .. v18}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v31

    invoke-direct/range {v30 .. v31}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    move-object/from16 v0, v30

    move-object/from16 v1, v28

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v30

    invoke-virtual/range {v30 .. v30}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v26

    const-string v30, "newurl"

    new-instance v31, Ljava/lang/StringBuilder;

    const-string v32, "newurl: "

    invoke-direct/range {v31 .. v32}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    move-object/from16 v0, v31

    move-object/from16 v1, v26

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v31

    invoke-virtual/range {v31 .. v31}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v31

    invoke-static/range {v30 .. v31}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    new-instance v3, Ljava/net/URL;

    move-object/from16 v0, v26

    invoke-direct {v3, v0}, Ljava/net/URL;-><init>(Ljava/lang/String;)V

    const-string v30, "downloadPicture......"

    new-instance v31, Ljava/lang/StringBuilder;

    const-string v32, "url: "

    invoke-direct/range {v31 .. v32}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    move-object/from16 v0, v31

    invoke-virtual {v0, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v31

    invoke-virtual/range {v31 .. v31}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v31

    invoke-static/range {v30 .. v31}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    invoke-virtual {v3}, Ljava/net/URL;->openConnection()Ljava/net/URLConnection;

    move-result-object v30

    move-object/from16 v0, v30

    check-cast v0, Ljava/net/HttpURLConnection;

    move-object/from16 v17, v0

    new-instance v12, Ljava/net/URL;

    move-object/from16 v0, v26

    invoke-direct {v12, v0}, Ljava/net/URL;-><init>(Ljava/lang/String;)V

    move-object v11, v12

    goto/16 :goto_0

    :cond_4
    const-string v30, "HeaderField "

    new-instance v31, Ljava/lang/StringBuilder;

    const-string v32, "HeaderField: "

    invoke-direct/range {v31 .. v32}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    move/from16 v0, v19

    invoke-virtual {v5, v0}, Ljava/net/HttpURLConnection;->getHeaderFieldKey(I)Ljava/lang/String;

    move-result-object v32

    invoke-virtual/range {v31 .. v32}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v31

    invoke-virtual/range {v31 .. v31}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v31

    invoke-static/range {v30 .. v31}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    add-int/lit8 v19, v19, 0x1

    goto/16 :goto_1

    :cond_5
    const-wide/32 v30, 0xf00000

    cmp-long v30, v21, v30

    if-lez v30, :cond_8

    invoke-virtual {v10}, Ljava/io/File;->delete()Z

    sget-object v30, Lcom/konka/picturePlayer/picturePlayerActivity$DOWNLOAD_STATE;->download_file_lage:Lcom/konka/picturePlayer/picturePlayerActivity$DOWNLOAD_STATE;

    move-object/from16 v0, v30

    move-object/from16 v1, p0

    iput-object v0, v1, Lcom/konka/picturePlayer/picturePlayerActivity;->downLoad_state:Lcom/konka/picturePlayer/picturePlayerActivity$DOWNLOAD_STATE;
    :try_end_2
    .catch Ljava/lang/Exception; {:try_start_2 .. :try_end_2} :catch_0
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    goto/16 :goto_2

    :catch_0
    move-exception v7

    :goto_4
    :try_start_3
    invoke-virtual {v7}, Ljava/lang/Exception;->printStackTrace()V
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_0

    if-eqz v20, :cond_2

    :try_start_4
    invoke-virtual/range {v20 .. v20}, Ljava/io/InputStream;->close()V

    if-eqz v27, :cond_6

    invoke-virtual/range {v27 .. v27}, Ljava/io/RandomAccessFile;->close()V

    :cond_6
    if-eqz v15, :cond_7

    invoke-virtual {v15}, Ljava/io/FileOutputStream;->close()V

    :cond_7
    if-eqz v17, :cond_2

    invoke-virtual/range {v17 .. v17}, Ljava/net/HttpURLConnection;->disconnect()V
    :try_end_4
    .catch Ljava/io/IOException; {:try_start_4 .. :try_end_4} :catch_1

    goto/16 :goto_3

    :catch_1
    move-exception v7

    invoke-virtual {v7}, Ljava/io/IOException;->printStackTrace()V

    goto/16 :goto_3

    :cond_8
    :try_start_5
    invoke-virtual {v10}, Ljava/io/File;->createNewFile()Z

    new-instance v16, Ljava/io/FileOutputStream;

    move-object/from16 v0, v16

    invoke-direct {v0, v10}, Ljava/io/FileOutputStream;-><init>(Ljava/io/File;)V
    :try_end_5
    .catch Ljava/lang/Exception; {:try_start_5 .. :try_end_5} :catch_0
    .catchall {:try_start_5 .. :try_end_5} :catchall_0

    :try_start_6
    invoke-virtual/range {v17 .. v17}, Ljava/net/HttpURLConnection;->getInputStream()Ljava/io/InputStream;

    move-result-object v20

    if-eqz v20, :cond_9

    const/16 v30, 0x1000

    move/from16 v0, v30

    new-array v4, v0, [B

    const/16 v29, 0x0

    :goto_5
    move-object/from16 v0, v20

    invoke-virtual {v0, v4}, Ljava/io/InputStream;->read([B)I

    move-result v29

    const/16 v30, -0x1

    move/from16 v0, v29

    move/from16 v1, v30

    if-ne v0, v1, :cond_b

    const-string v30, "read"

    new-instance v31, Ljava/lang/StringBuilder;

    const-string v32, "read:"

    invoke-direct/range {v31 .. v32}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    move-object/from16 v0, v31

    move/from16 v1, v29

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v31

    invoke-virtual/range {v31 .. v31}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v31

    invoke-static/range {v30 .. v31}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    invoke-virtual/range {v16 .. v16}, Ljava/io/FileOutputStream;->close()V

    invoke-virtual/range {v20 .. v20}, Ljava/io/InputStream;->close()V

    :cond_9
    invoke-virtual {v10}, Ljava/io/File;->length()J

    move-result-wide v8

    const-string v30, "long length "

    new-instance v31, Ljava/lang/StringBuilder;

    const-string v32, "endlength: "

    invoke-direct/range {v31 .. v32}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    move-object/from16 v0, v31

    invoke-virtual {v0, v8, v9}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v31

    invoke-virtual/range {v31 .. v31}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v31

    invoke-static/range {v30 .. v31}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    cmp-long v30, v8, v21

    if-nez v30, :cond_c

    invoke-virtual {v10}, Ljava/io/File;->getAbsolutePath()Ljava/lang/String;

    move-result-object v13

    const-string v30, "."

    move-object/from16 v0, v30

    invoke-virtual {v13, v0}, Ljava/lang/String;->indexOf(Ljava/lang/String;)I

    move-result v30

    if-ltz v30, :cond_a

    const/16 v30, 0x0

    const-string v31, "\\"

    move-object/from16 v0, v31

    invoke-virtual {v13, v0}, Ljava/lang/String;->lastIndexOf(Ljava/lang/String;)I

    move-result v31

    add-int/lit8 v31, v31, 0x1

    move/from16 v0, v30

    move/from16 v1, v31

    invoke-virtual {v13, v0, v1}, Ljava/lang/String;->substring(II)Ljava/lang/String;

    move-result-object v14

    const-string v30, "\\"

    move-object/from16 v0, v30

    invoke-virtual {v13, v0}, Ljava/lang/String;->lastIndexOf(Ljava/lang/String;)I

    move-result v30

    add-int/lit8 v30, v30, 0x1

    const-string v31, "."

    move-object/from16 v0, v31

    invoke-virtual {v13, v0}, Ljava/lang/String;->lastIndexOf(Ljava/lang/String;)I

    move-result v31

    move/from16 v0, v30

    move/from16 v1, v31

    invoke-virtual {v13, v0, v1}, Ljava/lang/String;->substring(II)Ljava/lang/String;

    move-result-object v13

    new-instance v30, Ljava/io/File;

    new-instance v31, Ljava/lang/StringBuilder;

    invoke-static {v14}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v32

    invoke-direct/range {v31 .. v32}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    move-object/from16 v0, v31

    invoke-virtual {v0, v13}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v31

    invoke-virtual/range {v31 .. v31}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v31

    invoke-direct/range {v30 .. v31}, Ljava/io/File;-><init>(Ljava/lang/String;)V

    move-object/from16 v0, v30

    invoke-virtual {v10, v0}, Ljava/io/File;->renameTo(Ljava/io/File;)Z

    :cond_a
    const/16 v30, 0x1

    move/from16 v0, v30

    move-object/from16 v1, p0

    iput-boolean v0, v1, Lcom/konka/picturePlayer/picturePlayerActivity;->endDown:Z

    sget-object v30, Lcom/konka/picturePlayer/picturePlayerActivity$DOWNLOAD_STATE;->download_finish:Lcom/konka/picturePlayer/picturePlayerActivity$DOWNLOAD_STATE;

    move-object/from16 v0, v30

    move-object/from16 v1, p0

    iput-object v0, v1, Lcom/konka/picturePlayer/picturePlayerActivity;->downLoad_state:Lcom/konka/picturePlayer/picturePlayerActivity$DOWNLOAD_STATE;

    move-object/from16 v15, v16

    goto/16 :goto_2

    :cond_b
    const/16 v30, 0x0

    move-object/from16 v0, v16

    move/from16 v1, v30

    move/from16 v2, v29

    invoke-virtual {v0, v4, v1, v2}, Ljava/io/FileOutputStream;->write([BII)V

    goto/16 :goto_5

    :catch_2
    move-exception v7

    move-object/from16 v15, v16

    goto/16 :goto_4

    :cond_c
    cmp-long v30, v8, v21

    if-lez v30, :cond_d

    invoke-virtual {v10}, Ljava/io/File;->delete()Z

    const/16 v30, 0x0

    move/from16 v0, v30

    move-object/from16 v1, p0

    iput-boolean v0, v1, Lcom/konka/picturePlayer/picturePlayerActivity;->endDown:Z

    sget-object v30, Lcom/konka/picturePlayer/picturePlayerActivity$DOWNLOAD_STATE;->download_file_err:Lcom/konka/picturePlayer/picturePlayerActivity$DOWNLOAD_STATE;

    move-object/from16 v0, v30

    move-object/from16 v1, p0

    iput-object v0, v1, Lcom/konka/picturePlayer/picturePlayerActivity;->downLoad_state:Lcom/konka/picturePlayer/picturePlayerActivity$DOWNLOAD_STATE;

    move-object/from16 v15, v16

    goto/16 :goto_2

    :cond_d
    cmp-long v30, v8, v21

    if-gez v30, :cond_11

    invoke-virtual {v10}, Ljava/io/File;->delete()Z

    const/16 v30, 0x0

    move/from16 v0, v30

    move-object/from16 v1, p0

    iput-boolean v0, v1, Lcom/konka/picturePlayer/picturePlayerActivity;->endDown:Z

    sget-object v30, Lcom/konka/picturePlayer/picturePlayerActivity$DOWNLOAD_STATE;->download_continue:Lcom/konka/picturePlayer/picturePlayerActivity$DOWNLOAD_STATE;

    move-object/from16 v0, v30

    move-object/from16 v1, p0

    iput-object v0, v1, Lcom/konka/picturePlayer/picturePlayerActivity;->downLoad_state:Lcom/konka/picturePlayer/picturePlayerActivity$DOWNLOAD_STATE;
    :try_end_6
    .catch Ljava/lang/Exception; {:try_start_6 .. :try_end_6} :catch_2
    .catchall {:try_start_6 .. :try_end_6} :catchall_1

    move-object/from16 v15, v16

    goto/16 :goto_2

    :catchall_0
    move-exception v30

    :goto_6
    if-eqz v20, :cond_10

    :try_start_7
    invoke-virtual/range {v20 .. v20}, Ljava/io/InputStream;->close()V

    if-eqz v27, :cond_e

    invoke-virtual/range {v27 .. v27}, Ljava/io/RandomAccessFile;->close()V

    :cond_e
    if-eqz v15, :cond_f

    invoke-virtual {v15}, Ljava/io/FileOutputStream;->close()V

    :cond_f
    if-eqz v17, :cond_10

    invoke-virtual/range {v17 .. v17}, Ljava/net/HttpURLConnection;->disconnect()V
    :try_end_7
    .catch Ljava/io/IOException; {:try_start_7 .. :try_end_7} :catch_3

    :cond_10
    :goto_7
    throw v30

    :catch_3
    move-exception v7

    invoke-virtual {v7}, Ljava/io/IOException;->printStackTrace()V

    goto :goto_7

    :catch_4
    move-exception v7

    invoke-virtual {v7}, Ljava/io/IOException;->printStackTrace()V

    goto/16 :goto_3

    :catchall_1
    move-exception v30

    move-object/from16 v15, v16

    goto :goto_6

    :cond_11
    move-object/from16 v15, v16

    goto/16 :goto_2
.end method

.method public enlarge()V
    .locals 12

    const/4 v1, 0x0

    iput-boolean v1, p0, Lcom/konka/picturePlayer/picturePlayerActivity;->isAutoPlaying:Z

    iget v0, p0, Lcom/konka/picturePlayer/picturePlayerActivity;->currentNum:I

    invoke-virtual {p0, v0}, Lcom/konka/picturePlayer/picturePlayerActivity;->getBitmap(I)Landroid/graphics/Bitmap;

    move-result-object v0

    invoke-virtual {v0}, Landroid/graphics/Bitmap;->getWidth()I

    move-result v3

    iget v0, p0, Lcom/konka/picturePlayer/picturePlayerActivity;->currentNum:I

    invoke-virtual {p0, v0}, Lcom/konka/picturePlayer/picturePlayerActivity;->getBitmap(I)Landroid/graphics/Bitmap;

    move-result-object v0

    invoke-virtual {v0}, Landroid/graphics/Bitmap;->getHeight()I

    move-result v4

    iget v0, p0, Lcom/konka/picturePlayer/picturePlayerActivity;->scaleWidth:F

    int-to-float v2, v3

    mul-float/2addr v0, v2

    iget-object v2, p0, Lcom/konka/picturePlayer/picturePlayerActivity;->layout1:Landroid/widget/LinearLayout;

    invoke-virtual {v2}, Landroid/widget/LinearLayout;->getWidth()I

    move-result v2

    int-to-float v2, v2

    cmpl-float v0, v0, v2

    if-gtz v0, :cond_0

    iget v0, p0, Lcom/konka/picturePlayer/picturePlayerActivity;->scaleHeight:F

    int-to-float v2, v4

    mul-float/2addr v0, v2

    iget-object v2, p0, Lcom/konka/picturePlayer/picturePlayerActivity;->layout1:Landroid/widget/LinearLayout;

    invoke-virtual {v2}, Landroid/widget/LinearLayout;->getHeight()I

    move-result v2

    int-to-float v2, v2

    cmpl-float v0, v0, v2

    if-lez v0, :cond_1

    :cond_0
    :goto_0
    return-void

    :cond_1
    const-wide v8, 0x3ff3333333333333L    # 1.2

    iget v0, p0, Lcom/konka/picturePlayer/picturePlayerActivity;->scaleWidth:F

    float-to-double v10, v0

    mul-double/2addr v10, v8

    double-to-float v0, v10

    iput v0, p0, Lcom/konka/picturePlayer/picturePlayerActivity;->scaleWidth:F

    iget v0, p0, Lcom/konka/picturePlayer/picturePlayerActivity;->scaleHeight:F

    float-to-double v10, v0

    mul-double/2addr v10, v8

    double-to-float v0, v10

    iput v0, p0, Lcom/konka/picturePlayer/picturePlayerActivity;->scaleHeight:F

    new-instance v5, Landroid/graphics/Matrix;

    invoke-direct {v5}, Landroid/graphics/Matrix;-><init>()V

    iget v0, p0, Lcom/konka/picturePlayer/picturePlayerActivity;->scaleWidth:F

    iget v2, p0, Lcom/konka/picturePlayer/picturePlayerActivity;->scaleHeight:F

    invoke-virtual {v5, v0, v2}, Landroid/graphics/Matrix;->postScale(FF)Z

    iget v0, p0, Lcom/konka/picturePlayer/picturePlayerActivity;->currentNum:I

    invoke-virtual {p0, v0}, Lcom/konka/picturePlayer/picturePlayerActivity;->getBitmap(I)Landroid/graphics/Bitmap;

    move-result-object v0

    const/4 v6, 0x1

    move v2, v1

    invoke-static/range {v0 .. v6}, Landroid/graphics/Bitmap;->createBitmap(Landroid/graphics/Bitmap;IIIILandroid/graphics/Matrix;Z)Landroid/graphics/Bitmap;

    move-result-object v7

    iget-object v0, p0, Lcom/konka/picturePlayer/picturePlayerActivity;->picImageview:Lcom/konka/picturePlayer/ImageViewTouch;

    invoke-virtual {v0, v7}, Lcom/konka/picturePlayer/ImageViewTouch;->setImageBitmap(Landroid/graphics/Bitmap;)V

    goto :goto_0
.end method

.method protected getBitmap(I)Landroid/graphics/Bitmap;
    .locals 7
    .param p1    # I

    const/4 v0, 0x0

    new-instance v3, Landroid/graphics/BitmapFactory$Options;

    invoke-direct {v3}, Landroid/graphics/BitmapFactory$Options;-><init>()V

    const/4 v5, 0x1

    iput-boolean v5, v3, Landroid/graphics/BitmapFactory$Options;->inJustDecodeBounds:Z

    iget-object v5, p0, Lcom/konka/picturePlayer/picturePlayerActivity;->picPaths:Ljava/util/List;

    invoke-interface {v5, p1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v4

    check-cast v4, Ljava/lang/String;

    invoke-static {v4, v3}, Landroid/graphics/BitmapFactory;->decodeFile(Ljava/lang/String;Landroid/graphics/BitmapFactory$Options;)Landroid/graphics/Bitmap;

    const/4 v5, -0x1

    const v6, 0xe1000

    invoke-static {v3, v5, v6}, Lcom/konka/picturePlayer/picturePlayerActivity;->computeSampleSize(Landroid/graphics/BitmapFactory$Options;II)I

    move-result v5

    iput v5, v3, Landroid/graphics/BitmapFactory$Options;->inSampleSize:I

    const/4 v5, 0x0

    iput-boolean v5, v3, Landroid/graphics/BitmapFactory$Options;->inJustDecodeBounds:Z

    sget-object v5, Landroid/graphics/Bitmap$Config;->ARGB_8888:Landroid/graphics/Bitmap$Config;

    iput-object v5, v3, Landroid/graphics/BitmapFactory$Options;->inPreferredConfig:Landroid/graphics/Bitmap$Config;

    const-string v5, "-----------------------"

    const-string v6, "---------------------------------6"

    invoke-static {v5, v6}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    :try_start_0
    invoke-static {v4}, Lcom/konka/picturePlayer/DecodeImageTool;->decodeBitmap(Ljava/lang/String;)Landroid/graphics/Bitmap;
    :try_end_0
    .catch Ljava/lang/OutOfMemoryError; {:try_start_0 .. :try_end_0} :catch_0

    move-result-object v0

    :goto_0
    return-object v0

    :catch_0
    move-exception v1

    sget-object v5, Ljava/lang/System;->err:Ljava/io/PrintStream;

    invoke-virtual {v5, v1}, Ljava/io/PrintStream;->println(Ljava/lang/Object;)V

    :try_start_1
    invoke-static {v4}, Lcom/konka/picturePlayer/DecodeImageTool;->decodeBitmap(Ljava/lang/String;)Landroid/graphics/Bitmap;
    :try_end_1
    .catch Ljava/lang/OutOfMemoryError; {:try_start_1 .. :try_end_1} :catch_1

    move-result-object v0

    goto :goto_0

    :catch_1
    move-exception v2

    sget-object v5, Ljava/lang/System;->err:Ljava/io/PrintStream;

    invoke-virtual {v5, v2}, Ljava/io/PrintStream;->println(Ljava/lang/Object;)V

    goto :goto_0
.end method

.method public hideControlDelay()V
    .locals 4

    iget-object v0, p0, Lcom/konka/picturePlayer/picturePlayerActivity;->hideHandler:Landroid/os/Handler;

    const/16 v1, 0x11

    const-wide/16 v2, 0x3a98

    invoke-virtual {v0, v1, v2, v3}, Landroid/os/Handler;->sendEmptyMessageDelayed(IJ)Z

    return-void
.end method

.method public hideController()V
    .locals 2

    const/16 v1, 0x8

    iget-object v0, p0, Lcom/konka/picturePlayer/picturePlayerActivity;->picShowHolder:Lcom/konka/picturePlayer/PicShowHolder;

    iget-object v0, v0, Lcom/konka/picturePlayer/PicShowHolder;->pic_bts_layout:Landroid/widget/RelativeLayout;

    invoke-virtual {v0, v1}, Landroid/widget/RelativeLayout;->setVisibility(I)V

    iget-object v0, p0, Lcom/konka/picturePlayer/picturePlayerActivity;->textView:Landroid/widget/TextView;

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setVisibility(I)V

    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/konka/picturePlayer/picturePlayerActivity;->isControllerShow:Z

    iget-boolean v0, p0, Lcom/konka/picturePlayer/picturePlayerActivity;->isAutoPlaying:Z

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/konka/picturePlayer/picturePlayerActivity;->playStateDrawable:[I

    const/4 v1, 0x1

    aget v0, v0, v1

    invoke-virtual {p0, v0}, Lcom/konka/picturePlayer/picturePlayerActivity;->setStateDrawable(I)V

    :cond_0
    return-void
.end method

.method public isAutoPlaying()Z
    .locals 1

    iget-boolean v0, p0, Lcom/konka/picturePlayer/picturePlayerActivity;->isAutoPlaying:Z

    return v0
.end method

.method public leftRotation()V
    .locals 8

    const/4 v1, 0x0

    iget v2, p0, Lcom/konka/picturePlayer/picturePlayerActivity;->rotationClickCount:I

    add-int/lit8 v2, v2, -0x1

    iput v2, p0, Lcom/konka/picturePlayer/picturePlayerActivity;->rotationClickCount:I

    iget v2, p0, Lcom/konka/picturePlayer/picturePlayerActivity;->rotationClickCount:I

    rem-int/lit8 v2, v2, 0x4

    iput v2, p0, Lcom/konka/picturePlayer/picturePlayerActivity;->rotationClickCount:I

    iput-boolean v1, p0, Lcom/konka/picturePlayer/picturePlayerActivity;->isAutoPlaying:Z

    iget v2, p0, Lcom/konka/picturePlayer/picturePlayerActivity;->currentNum:I

    invoke-virtual {p0, v2}, Lcom/konka/picturePlayer/picturePlayerActivity;->getBitmap(I)Landroid/graphics/Bitmap;

    move-result-object v0

    invoke-virtual {v0}, Landroid/graphics/Bitmap;->getWidth()I

    move-result v3

    invoke-virtual {v0}, Landroid/graphics/Bitmap;->getHeight()I

    move-result v4

    new-instance v5, Landroid/graphics/Matrix;

    invoke-direct {v5}, Landroid/graphics/Matrix;-><init>()V

    iget v2, p0, Lcom/konka/picturePlayer/picturePlayerActivity;->rotationClickCount:I

    mul-int/lit8 v2, v2, 0x5a

    int-to-float v2, v2

    invoke-virtual {v5, v2}, Landroid/graphics/Matrix;->postRotate(F)Z

    const/4 v6, 0x1

    move v2, v1

    invoke-static/range {v0 .. v6}, Landroid/graphics/Bitmap;->createBitmap(Landroid/graphics/Bitmap;IIIILandroid/graphics/Matrix;Z)Landroid/graphics/Bitmap;

    move-result-object v7

    iget-object v1, p0, Lcom/konka/picturePlayer/picturePlayerActivity;->picImageview:Lcom/konka/picturePlayer/ImageViewTouch;

    invoke-virtual {v1, v7}, Lcom/konka/picturePlayer/ImageViewTouch;->setImageBitmap(Landroid/graphics/Bitmap;)V

    return-void
.end method

.method public moveNext()V
    .locals 2

    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/konka/picturePlayer/picturePlayerActivity;->isAutoPlaying:Z

    iput v0, p0, Lcom/konka/picturePlayer/picturePlayerActivity;->rotationClickCount:I

    invoke-virtual {p0}, Lcom/konka/picturePlayer/picturePlayerActivity;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    const v1, 0x7f090085    # com.konka.mediaSharePlayer.R.string.last_photo

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p0, v0}, Lcom/konka/picturePlayer/picturePlayerActivity;->showToast(Ljava/lang/String;)V

    iget-object v0, p0, Lcom/konka/picturePlayer/picturePlayerActivity;->picImageview:Lcom/konka/picturePlayer/ImageViewTouch;

    iget v1, p0, Lcom/konka/picturePlayer/picturePlayerActivity;->currentNum:I

    invoke-virtual {p0, v1}, Lcom/konka/picturePlayer/picturePlayerActivity;->getBitmap(I)Landroid/graphics/Bitmap;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/konka/picturePlayer/ImageViewTouch;->setImageBitmap(Landroid/graphics/Bitmap;)V

    invoke-virtual {p0}, Lcom/konka/picturePlayer/picturePlayerActivity;->settextViewName()V

    return-void
.end method

.method protected moveNextOrPrevious(I)V
    .locals 3
    .param p1    # I

    const/4 v2, 0x0

    iput v2, p0, Lcom/konka/picturePlayer/picturePlayerActivity;->zoomTimes:I

    iget v0, p0, Lcom/konka/picturePlayer/picturePlayerActivity;->currentNum:I

    add-int/2addr v0, p1

    iput v0, p0, Lcom/konka/picturePlayer/picturePlayerActivity;->imgPost:I

    iget v0, p0, Lcom/konka/picturePlayer/picturePlayerActivity;->imgPost:I

    const/4 v1, -0x1

    if-gt v0, v1, :cond_1

    iput v2, p0, Lcom/konka/picturePlayer/picturePlayerActivity;->imgPost:I

    iget-object v0, p0, Lcom/konka/picturePlayer/picturePlayerActivity;->picShowHolder:Lcom/konka/picturePlayer/PicShowHolder;

    iget-object v0, v0, Lcom/konka/picturePlayer/PicShowHolder;->toast_first_image:Landroid/widget/Toast;

    invoke-virtual {v0}, Landroid/widget/Toast;->show()V

    iput-boolean v2, p0, Lcom/konka/picturePlayer/picturePlayerActivity;->isAutoPlaying:Z

    iget-object v0, p0, Lcom/konka/picturePlayer/picturePlayerActivity;->picShowHolder:Lcom/konka/picturePlayer/PicShowHolder;

    iget-object v0, v0, Lcom/konka/picturePlayer/PicShowHolder;->toast_first_image:Landroid/widget/Toast;

    invoke-virtual {v0}, Landroid/widget/Toast;->show()V

    :cond_0
    :goto_0
    return-void

    :cond_1
    iget v0, p0, Lcom/konka/picturePlayer/picturePlayerActivity;->imgPost:I

    iget-object v1, p0, Lcom/konka/picturePlayer/picturePlayerActivity;->picPaths:Ljava/util/List;

    invoke-interface {v1}, Ljava/util/List;->size()I

    move-result v1

    if-lt v0, v1, :cond_2

    iget-object v0, p0, Lcom/konka/picturePlayer/picturePlayerActivity;->picPaths:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->size()I

    move-result v0

    add-int/lit8 v0, v0, -0x1

    iput v0, p0, Lcom/konka/picturePlayer/picturePlayerActivity;->imgPost:I

    iget-object v0, p0, Lcom/konka/picturePlayer/picturePlayerActivity;->picShowHolder:Lcom/konka/picturePlayer/PicShowHolder;

    iget-object v0, v0, Lcom/konka/picturePlayer/PicShowHolder;->toast_last_image:Landroid/widget/Toast;

    invoke-virtual {v0}, Landroid/widget/Toast;->show()V

    iput-boolean v2, p0, Lcom/konka/picturePlayer/picturePlayerActivity;->isAutoPlaying:Z

    iget-object v0, p0, Lcom/konka/picturePlayer/picturePlayerActivity;->picShowHolder:Lcom/konka/picturePlayer/PicShowHolder;

    iget-object v0, v0, Lcom/konka/picturePlayer/PicShowHolder;->toast_last_image:Landroid/widget/Toast;

    invoke-virtual {v0}, Landroid/widget/Toast;->show()V

    iget-boolean v0, p0, Lcom/konka/picturePlayer/picturePlayerActivity;->isControllerShow:Z

    if-nez v0, :cond_0

    invoke-direct {p0}, Lcom/konka/picturePlayer/picturePlayerActivity;->showController()V

    goto :goto_0

    :cond_2
    iget v0, p0, Lcom/konka/picturePlayer/picturePlayerActivity;->imgPost:I

    iput v0, p0, Lcom/konka/picturePlayer/picturePlayerActivity;->currentNum:I

    goto :goto_0
.end method

.method public movePrevious()V
    .locals 2

    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/konka/picturePlayer/picturePlayerActivity;->isAutoPlaying:Z

    iput v0, p0, Lcom/konka/picturePlayer/picturePlayerActivity;->rotationClickCount:I

    invoke-virtual {p0}, Lcom/konka/picturePlayer/picturePlayerActivity;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    const v1, 0x7f090084    # com.konka.mediaSharePlayer.R.string.first_photo

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p0, v0}, Lcom/konka/picturePlayer/picturePlayerActivity;->showToast(Ljava/lang/String;)V

    iget-object v0, p0, Lcom/konka/picturePlayer/picturePlayerActivity;->picImageview:Lcom/konka/picturePlayer/ImageViewTouch;

    iget v1, p0, Lcom/konka/picturePlayer/picturePlayerActivity;->currentNum:I

    invoke-virtual {p0, v1}, Lcom/konka/picturePlayer/picturePlayerActivity;->getBitmap(I)Landroid/graphics/Bitmap;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/konka/picturePlayer/ImageViewTouch;->setImageBitmap(Landroid/graphics/Bitmap;)V

    invoke-virtual {p0}, Lcom/konka/picturePlayer/picturePlayerActivity;->settextViewName()V

    return-void
.end method

.method public onCreate(Landroid/os/Bundle;)V
    .locals 4
    .param p1    # Landroid/os/Bundle;

    const/16 v3, 0x400

    const/4 v1, 0x0

    const/4 v2, 0x1

    invoke-super {p0, p1}, Landroid/app/Activity;->onCreate(Landroid/os/Bundle;)V

    invoke-virtual {p0, v2}, Lcom/konka/picturePlayer/picturePlayerActivity;->requestWindowFeature(I)Z

    invoke-virtual {p0}, Lcom/konka/picturePlayer/picturePlayerActivity;->getWindow()Landroid/view/Window;

    move-result-object v0

    invoke-virtual {v0, v3, v3}, Landroid/view/Window;->setFlags(II)V

    const v0, 0x7f030003    # com.konka.mediaSharePlayer.R.layout.showpiclayout1

    invoke-virtual {p0, v0}, Lcom/konka/picturePlayer/picturePlayerActivity;->setContentView(I)V

    iget-boolean v0, p0, Lcom/konka/picturePlayer/picturePlayerActivity;->isCreat:Z

    if-nez v0, :cond_0

    invoke-static {p0, v2, v1, v1}, Lcom/konka/mediaSharePlayer/sendPlayerState;->MediaPlayerActionSendBroadcastIsCreat(Landroid/content/Context;III)V

    new-instance v0, Ljava/lang/Thread;

    new-instance v1, Lcom/konka/picturePlayer/picturePlayerActivity$4;

    invoke-direct {v1, p0}, Lcom/konka/picturePlayer/picturePlayerActivity$4;-><init>(Lcom/konka/picturePlayer/picturePlayerActivity;)V

    invoke-direct {v0, v1}, Ljava/lang/Thread;-><init>(Ljava/lang/Runnable;)V

    invoke-virtual {v0}, Ljava/lang/Thread;->start()V

    new-instance v0, Lcom/konka/picturePlayer/PicShowHolder;

    invoke-direct {v0, p0}, Lcom/konka/picturePlayer/PicShowHolder;-><init>(Lcom/konka/picturePlayer/picturePlayerActivity;)V

    iput-object v0, p0, Lcom/konka/picturePlayer/picturePlayerActivity;->picShowHolder:Lcom/konka/picturePlayer/PicShowHolder;

    iget-object v0, p0, Lcom/konka/picturePlayer/picturePlayerActivity;->picShowHolder:Lcom/konka/picturePlayer/PicShowHolder;

    invoke-virtual {v0}, Lcom/konka/picturePlayer/PicShowHolder;->findBtnView()V

    sget-object v0, Lcom/konka/picturePlayer/PicShowHolder;->bt_leftRotion:Landroid/widget/Button;

    invoke-virtual {v0, v2}, Landroid/widget/Button;->setFocusable(Z)V

    sget-object v0, Lcom/konka/picturePlayer/PicShowHolder;->bt_leftRotion:Landroid/widget/Button;

    invoke-virtual {v0}, Landroid/widget/Button;->requestFocus()Z

    invoke-virtual {p0}, Lcom/konka/picturePlayer/picturePlayerActivity;->InitData()V

    iget-object v0, p0, Lcom/konka/picturePlayer/picturePlayerActivity;->picImageview:Lcom/konka/picturePlayer/ImageViewTouch;

    invoke-virtual {v0, v2}, Lcom/konka/picturePlayer/ImageViewTouch;->setEnableTrackballScroll(Z)V

    iget-object v0, p0, Lcom/konka/picturePlayer/picturePlayerActivity;->picImageview:Lcom/konka/picturePlayer/ImageViewTouch;

    new-instance v1, Lcom/konka/picturePlayer/picturePlayerActivity$ImageViewOnTounchListener;

    invoke-direct {v1, p0}, Lcom/konka/picturePlayer/picturePlayerActivity$ImageViewOnTounchListener;-><init>(Lcom/konka/picturePlayer/picturePlayerActivity;)V

    invoke-virtual {v0, v1}, Lcom/konka/picturePlayer/ImageViewTouch;->setOnTouchListener(Landroid/view/View$OnTouchListener;)V

    :cond_0
    iput-boolean v2, p0, Lcom/konka/picturePlayer/picturePlayerActivity;->isCreat:Z

    return-void
.end method

.method protected onDestroy()V
    .locals 2

    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/konka/picturePlayer/picturePlayerActivity;->isAutoPlaying:Z

    const-string v0, "onDestroy"

    const-string v1, " picture onDestroy()"

    invoke-static {v0, v1}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    iget-object v0, p0, Lcom/konka/picturePlayer/picturePlayerActivity;->close:Lcom/konka/picturePlayer/picturePlayerActivity$closeReceiver;

    invoke-virtual {p0, v0}, Lcom/konka/picturePlayer/picturePlayerActivity;->unregisterReceiver(Landroid/content/BroadcastReceiver;)V

    iget-object v0, p0, Lcom/konka/picturePlayer/picturePlayerActivity;->dataReceiver:Lcom/konka/picturePlayer/picturePlayerActivity$DataReceiver;

    invoke-virtual {p0, v0}, Lcom/konka/picturePlayer/picturePlayerActivity;->unregisterReceiver(Landroid/content/BroadcastReceiver;)V

    iget-object v0, p0, Lcom/konka/picturePlayer/picturePlayerActivity;->mediaPlayerSwitch:Lcom/konka/picturePlayer/picturePlayerActivity$switchReceiver;

    invoke-virtual {p0, v0}, Lcom/konka/picturePlayer/picturePlayerActivity;->unregisterReceiver(Landroid/content/BroadcastReceiver;)V

    invoke-super {p0}, Landroid/app/Activity;->onDestroy()V

    return-void
.end method

.method public onKeyDown(ILandroid/view/KeyEvent;)Z
    .locals 6
    .param p1    # I
    .param p2    # Landroid/view/KeyEvent;

    const v2, 0x50000001

    const/4 v1, 0x1

    const/4 v3, 0x0

    invoke-virtual {p2}, Landroid/view/KeyEvent;->getKeyCode()I

    move-result v0

    sparse-switch v0, :sswitch_data_0

    :cond_0
    :goto_0
    invoke-super {p0, p1, p2}, Landroid/app/Activity;->onKeyDown(ILandroid/view/KeyEvent;)Z

    move-result v0

    return v0

    :sswitch_0
    const-string v0, "-----------------------"

    const-string v1, "--------------------KEYCODE_BACK"

    invoke-static {v0, v1}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    iput v3, p0, Lcom/konka/picturePlayer/picturePlayerActivity;->mediastop:I

    sget-object v0, Lcom/konka/mediaSharePlayer/PlayerState;->PLAY_STATUS_EXIT:Lcom/konka/mediaSharePlayer/PlayerState;

    invoke-virtual {v0}, Lcom/konka/mediaSharePlayer/PlayerState;->ordinal()I

    move-result v2

    new-instance v1, Ljava/lang/StringBuilder;

    iget-object v0, p0, Lcom/konka/picturePlayer/picturePlayerActivity;->picPaths:Ljava/util/List;

    invoke-interface {v0, v3}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/String;

    invoke-static {v0}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    invoke-direct {v1, v0}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    const-string v0, "&mp"

    invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    move-object v0, p0

    move-object v1, p0

    move v4, v3

    invoke-virtual/range {v0 .. v5}, Lcom/konka/picturePlayer/picturePlayerActivity;->sendPlayerStateToPlayerService(Landroid/content/Context;IIILjava/lang/String;)V

    iput-boolean v3, p0, Lcom/konka/picturePlayer/picturePlayerActivity;->isrun:Z

    invoke-static {p0, v3, v3, v3}, Lcom/konka/mediaSharePlayer/sendPlayerState;->MediaPlayerActionSendBroadcastIsCreat(Landroid/content/Context;III)V

    invoke-virtual {p0}, Lcom/konka/picturePlayer/picturePlayerActivity;->finish()V

    const-string v0, "-----------------------"

    const-string v1, "111--------------------KEYCODE_BACK"

    invoke-static {v0, v1}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_0

    :sswitch_1
    sget-boolean v0, Lcom/konka/picturePlayer/picturePlayerActivity;->isCanMove:Z

    if-nez v0, :cond_0

    iget-boolean v0, p0, Lcom/konka/picturePlayer/picturePlayerActivity;->isControllerShow:Z

    if-nez v0, :cond_0

    iget-boolean v0, p0, Lcom/konka/picturePlayer/picturePlayerActivity;->isAutoPlaying:Z

    if-nez v0, :cond_0

    iget-object v0, p0, Lcom/konka/picturePlayer/picturePlayerActivity;->playStateDrawable:[I

    aget v0, v0, v3

    invoke-virtual {p0, v0}, Lcom/konka/picturePlayer/picturePlayerActivity;->setStateDrawable(I)V

    goto :goto_0

    :sswitch_2
    sget-boolean v0, Lcom/konka/picturePlayer/picturePlayerActivity;->isCanMove:Z

    if-nez v0, :cond_0

    iget-boolean v0, p0, Lcom/konka/picturePlayer/picturePlayerActivity;->isControllerShow:Z

    if-nez v0, :cond_0

    iget-boolean v0, p0, Lcom/konka/picturePlayer/picturePlayerActivity;->isAutoPlaying:Z

    if-nez v0, :cond_0

    iget-object v0, p0, Lcom/konka/picturePlayer/picturePlayerActivity;->playStateDrawable:[I

    const/4 v1, 0x2

    aget v0, v0, v1

    invoke-virtual {p0, v0}, Lcom/konka/picturePlayer/picturePlayerActivity;->setStateDrawable(I)V

    goto :goto_0

    :sswitch_3
    const/4 v0, 0x7

    invoke-virtual {p0, v0}, Lcom/konka/picturePlayer/picturePlayerActivity;->setState(I)V

    invoke-virtual {p0}, Lcom/konka/picturePlayer/picturePlayerActivity;->registerListeners()V

    sget-object v0, Lcom/konka/picturePlayer/PicShowHolder;->bt_rightRotion:Landroid/widget/Button;

    invoke-virtual {v0, v1}, Landroid/widget/Button;->setFocusable(Z)V

    sget-object v0, Lcom/konka/picturePlayer/PicShowHolder;->bt_rightRotion:Landroid/widget/Button;

    invoke-virtual {v0}, Landroid/widget/Button;->requestFocus()Z

    goto :goto_0

    :sswitch_4
    const/4 v0, 0x6

    invoke-virtual {p0, v0}, Lcom/konka/picturePlayer/picturePlayerActivity;->setState(I)V

    invoke-virtual {p0}, Lcom/konka/picturePlayer/picturePlayerActivity;->registerListeners()V

    sget-object v0, Lcom/konka/picturePlayer/PicShowHolder;->bt_leftRotion:Landroid/widget/Button;

    invoke-virtual {v0, v1}, Landroid/widget/Button;->setFocusable(Z)V

    sget-object v0, Lcom/konka/picturePlayer/PicShowHolder;->bt_leftRotion:Landroid/widget/Button;

    invoke-virtual {v0}, Landroid/widget/Button;->requestFocus()Z

    goto/16 :goto_0

    :sswitch_5
    const/16 v0, 0x8

    invoke-virtual {p0, v0}, Lcom/konka/picturePlayer/picturePlayerActivity;->setState(I)V

    invoke-virtual {p0}, Lcom/konka/picturePlayer/picturePlayerActivity;->registerListeners()V

    sget-object v0, Lcom/konka/picturePlayer/PicShowHolder;->bt_big:Landroid/widget/Button;

    invoke-virtual {v0, v1}, Landroid/widget/Button;->setFocusable(Z)V

    sget-object v0, Lcom/konka/picturePlayer/PicShowHolder;->bt_big:Landroid/widget/Button;

    invoke-virtual {v0}, Landroid/widget/Button;->requestFocus()Z

    goto/16 :goto_0

    :sswitch_6
    const/16 v0, 0x9

    invoke-virtual {p0, v0}, Lcom/konka/picturePlayer/picturePlayerActivity;->setState(I)V

    invoke-virtual {p0}, Lcom/konka/picturePlayer/picturePlayerActivity;->registerListeners()V

    sget-object v0, Lcom/konka/picturePlayer/PicShowHolder;->bt_small:Landroid/widget/Button;

    invoke-virtual {v0, v1}, Landroid/widget/Button;->setFocusable(Z)V

    sget-object v0, Lcom/konka/picturePlayer/PicShowHolder;->bt_small:Landroid/widget/Button;

    invoke-virtual {v0}, Landroid/widget/Button;->requestFocus()Z

    goto/16 :goto_0

    :sswitch_7
    iget-boolean v0, p0, Lcom/konka/picturePlayer/picturePlayerActivity;->isControllerShow:Z

    if-nez v0, :cond_1

    sget-boolean v0, Lcom/konka/picturePlayer/picturePlayerActivity;->isCanMove:Z

    if-nez v0, :cond_1

    iget-boolean v0, p0, Lcom/konka/picturePlayer/picturePlayerActivity;->isAutoPlaying:Z

    if-eqz v0, :cond_2

    move v0, v3

    :goto_1
    iput-boolean v0, p0, Lcom/konka/picturePlayer/picturePlayerActivity;->isAutoPlaying:Z

    iget-boolean v0, p0, Lcom/konka/picturePlayer/picturePlayerActivity;->isAutoPlaying:Z

    if-eqz v0, :cond_3

    iget-object v0, p0, Lcom/konka/picturePlayer/picturePlayerActivity;->handler:Landroid/os/Handler;

    invoke-virtual {v0, v2}, Landroid/os/Handler;->removeMessages(I)V

    iget-object v0, p0, Lcom/konka/picturePlayer/picturePlayerActivity;->handler:Landroid/os/Handler;

    invoke-virtual {v0, v2}, Landroid/os/Handler;->sendEmptyMessage(I)Z

    iget-object v0, p0, Lcom/konka/picturePlayer/picturePlayerActivity;->playStateDrawable:[I

    aget v0, v0, v1

    invoke-virtual {p0, v0}, Lcom/konka/picturePlayer/picturePlayerActivity;->setStateDrawable(I)V

    :cond_1
    :goto_2
    iput-boolean v3, p0, Lcom/konka/picturePlayer/picturePlayerActivity;->picAutoPlay:Z

    goto/16 :goto_0

    :cond_2
    move v0, v1

    goto :goto_1

    :cond_3
    iget-object v0, p0, Lcom/konka/picturePlayer/picturePlayerActivity;->playStateDrawable:[I

    const/4 v1, 0x3

    aget v0, v0, v1

    invoke-virtual {p0, v0}, Lcom/konka/picturePlayer/picturePlayerActivity;->setStateDrawable(I)V

    goto :goto_2

    :sswitch_8
    iget-boolean v0, p0, Lcom/konka/picturePlayer/picturePlayerActivity;->isControllerShow:Z

    if-eqz v0, :cond_5

    invoke-virtual {p0}, Lcom/konka/picturePlayer/picturePlayerActivity;->hideController()V

    iget-boolean v0, p0, Lcom/konka/picturePlayer/picturePlayerActivity;->isAutoPlaying:Z

    if-eqz v0, :cond_4

    iget-object v0, p0, Lcom/konka/picturePlayer/picturePlayerActivity;->playStateDrawable:[I

    aget v0, v0, v1

    invoke-virtual {p0, v0}, Lcom/konka/picturePlayer/picturePlayerActivity;->setStateDrawable(I)V

    :cond_4
    :goto_3
    iget-boolean v0, p0, Lcom/konka/picturePlayer/picturePlayerActivity;->isAutoPlaying:Z

    if-eqz v0, :cond_0

    invoke-virtual {p0}, Lcom/konka/picturePlayer/picturePlayerActivity;->hideControlDelay()V

    goto/16 :goto_0

    :cond_5
    invoke-direct {p0}, Lcom/konka/picturePlayer/picturePlayerActivity;->showController()V

    iget-object v0, p0, Lcom/konka/picturePlayer/picturePlayerActivity;->stateDrawable:Landroid/widget/ImageView;

    const/4 v2, 0x0

    invoke-virtual {v0, v2}, Landroid/widget/ImageView;->setImageBitmap(Landroid/graphics/Bitmap;)V

    iget-boolean v0, p0, Lcom/konka/picturePlayer/picturePlayerActivity;->isAutoPlaying:Z

    if-eqz v0, :cond_4

    sget-object v0, Lcom/konka/picturePlayer/PicShowHolder;->bt_leftRotion:Landroid/widget/Button;

    invoke-virtual {v0, v1}, Landroid/widget/Button;->setFocusable(Z)V

    sget-object v0, Lcom/konka/picturePlayer/PicShowHolder;->bt_leftRotion:Landroid/widget/Button;

    invoke-virtual {v0}, Landroid/widget/Button;->requestFocus()Z

    goto :goto_3

    nop

    :sswitch_data_0
    .sparse-switch
        0x4 -> :sswitch_0
        0x15 -> :sswitch_1
        0x16 -> :sswitch_2
        0x42 -> :sswitch_7
        0x52 -> :sswitch_8
        0x6f -> :sswitch_0
        0xb7 -> :sswitch_5
        0xb8 -> :sswitch_6
        0xb9 -> :sswitch_3
        0xba -> :sswitch_4
    .end sparse-switch
.end method

.method protected onStart()V
    .locals 2

    const-string v0, "MultiScreenPictureActivity"

    const-string v1, "onStart()"

    invoke-static {v0, v1}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    invoke-super {p0}, Landroid/app/Activity;->onStart()V

    return-void
.end method

.method protected onStop()V
    .locals 6

    const/4 v1, 0x1

    const/4 v3, 0x0

    iput-boolean v3, p0, Lcom/konka/picturePlayer/picturePlayerActivity;->isAutoPlaying:Z

    iget v0, p0, Lcom/konka/picturePlayer/picturePlayerActivity;->mediastop:I

    if-ne v0, v1, :cond_0

    iget v0, p0, Lcom/konka/picturePlayer/picturePlayerActivity;->mediaSwitch:I

    if-ne v0, v1, :cond_0

    invoke-static {p0, v3, v3, v3}, Lcom/konka/mediaSharePlayer/sendPlayerState;->MediaPlayerActionSendBroadcastIsCreat(Landroid/content/Context;III)V

    sget-object v0, Lcom/konka/mediaSharePlayer/PlayerState;->PLAY_STATUS_EXIT:Lcom/konka/mediaSharePlayer/PlayerState;

    invoke-virtual {v0}, Lcom/konka/mediaSharePlayer/PlayerState;->ordinal()I

    move-result v2

    new-instance v1, Ljava/lang/StringBuilder;

    iget-object v0, p0, Lcom/konka/picturePlayer/picturePlayerActivity;->picPaths:Ljava/util/List;

    invoke-interface {v0, v3}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/String;

    invoke-static {v0}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    invoke-direct {v1, v0}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    const-string v0, "&mp"

    invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    move-object v0, p0

    move-object v1, p0

    move v4, v3

    invoke-virtual/range {v0 .. v5}, Lcom/konka/picturePlayer/picturePlayerActivity;->sendPlayerStateToPlayerService(Landroid/content/Context;IIILjava/lang/String;)V

    invoke-static {v3}, Ljava/lang/System;->exit(I)V

    :cond_0
    iput-boolean v3, p0, Lcom/konka/picturePlayer/picturePlayerActivity;->isrun:Z

    invoke-super {p0}, Landroid/app/Activity;->onStop()V

    return-void
.end method

.method public onTouch(Landroid/view/View;Landroid/view/MotionEvent;)Z
    .locals 8
    .param p1    # Landroid/view/View;
    .param p2    # Landroid/view/MotionEvent;

    const/4 v5, 0x2

    const/high16 v4, 0x41200000    # 10.0f

    const/4 v7, 0x1

    move-object v2, p1

    check-cast v2, Landroid/widget/ImageView;

    invoke-virtual {p2}, Landroid/view/MotionEvent;->getAction()I

    move-result v3

    and-int/lit16 v3, v3, 0xff

    packed-switch v3, :pswitch_data_0

    :cond_0
    :goto_0
    :pswitch_0
    iget-object v3, p0, Lcom/konka/picturePlayer/picturePlayerActivity;->matrix:Landroid/graphics/Matrix;

    invoke-virtual {v2, v3}, Landroid/widget/ImageView;->setImageMatrix(Landroid/graphics/Matrix;)V

    return v7

    :pswitch_1
    iget-object v3, p0, Lcom/konka/picturePlayer/picturePlayerActivity;->savedMatrix:Landroid/graphics/Matrix;

    iget-object v4, p0, Lcom/konka/picturePlayer/picturePlayerActivity;->matrix:Landroid/graphics/Matrix;

    invoke-virtual {v3, v4}, Landroid/graphics/Matrix;->set(Landroid/graphics/Matrix;)V

    iget-object v3, p0, Lcom/konka/picturePlayer/picturePlayerActivity;->start:Landroid/graphics/PointF;

    invoke-virtual {p2}, Landroid/view/MotionEvent;->getX()F

    move-result v4

    invoke-virtual {p2}, Landroid/view/MotionEvent;->getY()F

    move-result v5

    invoke-virtual {v3, v4, v5}, Landroid/graphics/PointF;->set(FF)V

    iput v7, p0, Lcom/konka/picturePlayer/picturePlayerActivity;->mode:I

    goto :goto_0

    :pswitch_2
    const/4 v3, 0x0

    iput v3, p0, Lcom/konka/picturePlayer/picturePlayerActivity;->mode:I

    goto :goto_0

    :pswitch_3
    invoke-direct {p0, p2}, Lcom/konka/picturePlayer/picturePlayerActivity;->spacing(Landroid/view/MotionEvent;)F

    move-result v3

    iput v3, p0, Lcom/konka/picturePlayer/picturePlayerActivity;->oldDist:F

    iget v3, p0, Lcom/konka/picturePlayer/picturePlayerActivity;->oldDist:F

    cmpl-float v3, v3, v4

    if-lez v3, :cond_0

    iget-object v3, p0, Lcom/konka/picturePlayer/picturePlayerActivity;->savedMatrix:Landroid/graphics/Matrix;

    iget-object v4, p0, Lcom/konka/picturePlayer/picturePlayerActivity;->matrix:Landroid/graphics/Matrix;

    invoke-virtual {v3, v4}, Landroid/graphics/Matrix;->set(Landroid/graphics/Matrix;)V

    iget-object v3, p0, Lcom/konka/picturePlayer/picturePlayerActivity;->mid:Landroid/graphics/PointF;

    invoke-direct {p0, v3, p2}, Lcom/konka/picturePlayer/picturePlayerActivity;->midPoint(Landroid/graphics/PointF;Landroid/view/MotionEvent;)V

    iput v5, p0, Lcom/konka/picturePlayer/picturePlayerActivity;->mode:I

    goto :goto_0

    :pswitch_4
    iget v3, p0, Lcom/konka/picturePlayer/picturePlayerActivity;->mode:I

    if-ne v3, v7, :cond_1

    iget-object v3, p0, Lcom/konka/picturePlayer/picturePlayerActivity;->matrix:Landroid/graphics/Matrix;

    iget-object v4, p0, Lcom/konka/picturePlayer/picturePlayerActivity;->savedMatrix:Landroid/graphics/Matrix;

    invoke-virtual {v3, v4}, Landroid/graphics/Matrix;->set(Landroid/graphics/Matrix;)V

    iget-object v3, p0, Lcom/konka/picturePlayer/picturePlayerActivity;->matrix:Landroid/graphics/Matrix;

    invoke-virtual {p2}, Landroid/view/MotionEvent;->getX()F

    move-result v4

    iget-object v5, p0, Lcom/konka/picturePlayer/picturePlayerActivity;->start:Landroid/graphics/PointF;

    iget v5, v5, Landroid/graphics/PointF;->x:F

    sub-float/2addr v4, v5

    invoke-virtual {p2}, Landroid/view/MotionEvent;->getX()F

    move-result v5

    iget-object v6, p0, Lcom/konka/picturePlayer/picturePlayerActivity;->start:Landroid/graphics/PointF;

    iget v6, v6, Landroid/graphics/PointF;->x:F

    sub-float/2addr v5, v6

    invoke-virtual {v3, v4, v5}, Landroid/graphics/Matrix;->postTranslate(FF)Z

    goto :goto_0

    :cond_1
    iget v3, p0, Lcom/konka/picturePlayer/picturePlayerActivity;->mode:I

    if-ne v3, v5, :cond_0

    invoke-direct {p0, p2}, Lcom/konka/picturePlayer/picturePlayerActivity;->spacing(Landroid/view/MotionEvent;)F

    move-result v0

    cmpl-float v3, v0, v4

    if-lez v3, :cond_0

    iget-object v3, p0, Lcom/konka/picturePlayer/picturePlayerActivity;->matrix:Landroid/graphics/Matrix;

    iget-object v4, p0, Lcom/konka/picturePlayer/picturePlayerActivity;->savedMatrix:Landroid/graphics/Matrix;

    invoke-virtual {v3, v4}, Landroid/graphics/Matrix;->set(Landroid/graphics/Matrix;)V

    iget v3, p0, Lcom/konka/picturePlayer/picturePlayerActivity;->oldDist:F

    div-float v1, v0, v3

    iget-object v3, p0, Lcom/konka/picturePlayer/picturePlayerActivity;->matrix:Landroid/graphics/Matrix;

    iget-object v4, p0, Lcom/konka/picturePlayer/picturePlayerActivity;->mid:Landroid/graphics/PointF;

    iget v4, v4, Landroid/graphics/PointF;->x:F

    iget-object v5, p0, Lcom/konka/picturePlayer/picturePlayerActivity;->mid:Landroid/graphics/PointF;

    iget v5, v5, Landroid/graphics/PointF;->y:F

    invoke-virtual {v3, v1, v1, v4, v5}, Landroid/graphics/Matrix;->postScale(FFFF)Z

    goto/16 :goto_0

    nop

    :pswitch_data_0
    .packed-switch 0x0
        :pswitch_1
        :pswitch_2
        :pswitch_4
        :pswitch_0
        :pswitch_0
        :pswitch_3
        :pswitch_2
    .end packed-switch
.end method

.method public playImage(Ljava/lang/String;)V
    .locals 9
    .param p1    # Ljava/lang/String;

    const v8, 0x7f0900a2    # com.konka.mediaSharePlayer.R.string.mediafile_buffering

    const v7, 0x7f090090    # com.konka.mediaSharePlayer.R.string.picture_too_lage

    const-string v3, "picFilePath"

    new-instance v4, Ljava/lang/StringBuilder;

    const-string v5, "file.length() : "

    invoke-direct {v4, v5}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v4, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-static {v3, v4}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    iget-object v3, p0, Lcom/konka/picturePlayer/picturePlayerActivity;->file:Ljava/io/File;

    invoke-virtual {v3}, Ljava/io/File;->listFiles()[Ljava/io/File;

    move-result-object v3

    iput-object v3, p0, Lcom/konka/picturePlayer/picturePlayerActivity;->imageFileList:[Ljava/io/File;

    const-string v3, "picFilePath"

    new-instance v4, Ljava/lang/StringBuilder;

    const-string v5, "file.length() : "

    invoke-direct {v4, v5}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    iget-object v5, p0, Lcom/konka/picturePlayer/picturePlayerActivity;->file:Ljava/io/File;

    invoke-virtual {v5}, Ljava/io/File;->length()J

    move-result-wide v5

    invoke-virtual {v4, v5, v6}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-static {v3, v4}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    const-string v3, "picFilePath"

    new-instance v4, Ljava/lang/StringBuilder;

    const-string v5, "file.getUsableSpace(): "

    invoke-direct {v4, v5}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    iget-object v5, p0, Lcom/konka/picturePlayer/picturePlayerActivity;->file:Ljava/io/File;

    invoke-virtual {v5}, Ljava/io/File;->getUsableSpace()J

    move-result-wide v5

    invoke-virtual {v4, v5, v6}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-static {v3, v4}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    const-string v3, "picFilePath"

    new-instance v4, Ljava/lang/StringBuilder;

    const-string v5, "file.getFreeSpace(): "

    invoke-direct {v4, v5}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    iget-object v5, p0, Lcom/konka/picturePlayer/picturePlayerActivity;->file:Ljava/io/File;

    invoke-virtual {v5}, Ljava/io/File;->getFreeSpace()J

    move-result-wide v5

    invoke-virtual {v4, v5, v6}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v4

    const-string v5, "filesize"

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    iget-wide v5, p0, Lcom/konka/picturePlayer/picturePlayerActivity;->filesize:J

    invoke-virtual {v4, v5, v6}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-static {v3, v4}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    iget-object v3, p0, Lcom/konka/picturePlayer/picturePlayerActivity;->file:Ljava/io/File;

    invoke-virtual {v3}, Ljava/io/File;->exists()Z

    move-result v3

    if-eqz v3, :cond_4

    const-string v3, "picFilePath"

    const-string v4, "/data/data/com.konka.playerService/cache is exist!!!"

    invoke-static {v3, v4}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    new-instance v3, Ljava/lang/StringBuilder;

    iget-object v4, p0, Lcom/konka/picturePlayer/picturePlayerActivity;->picFilePath:Ljava/lang/String;

    invoke-static {v4}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v4

    invoke-direct {v3, v4}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    const-string v4, "/"

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    iput-object v3, p0, Lcom/konka/picturePlayer/picturePlayerActivity;->picturePath:Ljava/lang/String;

    new-instance v3, Ljava/io/File;

    iget-object v4, p0, Lcom/konka/picturePlayer/picturePlayerActivity;->picturePath:Ljava/lang/String;

    invoke-direct {v3, v4}, Ljava/io/File;-><init>(Ljava/lang/String;)V

    iput-object v3, p0, Lcom/konka/picturePlayer/picturePlayerActivity;->picFile:Ljava/io/File;

    iget-object v3, p0, Lcom/konka/picturePlayer/picturePlayerActivity;->picFile:Ljava/io/File;

    invoke-virtual {v3}, Ljava/io/File;->exists()Z

    move-result v3

    if-nez v3, :cond_3

    const-string v3, "picFilePath"

    const-string v4, "/cache picFile.exists() is not exist!!!"

    invoke-static {v3, v4}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    const-wide/32 v1, 0x100000

    const-string v3, "picFilePath"

    new-instance v4, Ljava/lang/StringBuilder;

    const-string v5, "filesize > picFile.getUsableSpace():"

    invoke-direct {v4, v5}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    iget-wide v5, p0, Lcom/konka/picturePlayer/picturePlayerActivity;->filesize:J

    div-long/2addr v5, v1

    invoke-virtual {v4, v5, v6}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v4

    const-string v5, "---"

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    iget-object v5, p0, Lcom/konka/picturePlayer/picturePlayerActivity;->file:Ljava/io/File;

    invoke-virtual {v5}, Ljava/io/File;->getUsableSpace()J

    move-result-wide v5

    div-long/2addr v5, v1

    invoke-virtual {v4, v5, v6}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-static {v3, v4}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    :goto_0
    iget-wide v3, p0, Lcom/konka/picturePlayer/picturePlayerActivity;->filesize:J

    iget-object v5, p0, Lcom/konka/picturePlayer/picturePlayerActivity;->file:Ljava/io/File;

    invoke-virtual {v5}, Ljava/io/File;->getUsableSpace()J

    move-result-wide v5

    cmp-long v3, v3, v5

    if-gtz v3, :cond_1

    iget-wide v3, p0, Lcom/konka/picturePlayer/picturePlayerActivity;->filesize:J

    const-wide/32 v5, 0x300000

    cmp-long v3, v3, v5

    if-lez v3, :cond_0

    invoke-direct {p0, v8, v8}, Lcom/konka/picturePlayer/picturePlayerActivity;->showProgressDialog(II)V

    :cond_0
    invoke-virtual {p0}, Lcom/konka/picturePlayer/picturePlayerActivity;->creatImageFile()V

    :goto_1
    return-void

    :cond_1
    const-string v3, "picFilePath"

    new-instance v4, Ljava/lang/StringBuilder;

    const-string v5, "filesize > picFile.getUsableSpace()"

    invoke-direct {v4, v5}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    iget-wide v5, p0, Lcom/konka/picturePlayer/picturePlayerActivity;->filesize:J

    invoke-virtual {v4, v5, v6}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v4

    const-string v5, "=="

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    iget-object v5, p0, Lcom/konka/picturePlayer/picturePlayerActivity;->file:Ljava/io/File;

    invoke-virtual {v5}, Ljava/io/File;->getUsableSpace()J

    move-result-wide v5

    invoke-virtual {v4, v5, v6}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-static {v3, v4}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    iget-object v3, p0, Lcom/konka/picturePlayer/picturePlayerActivity;->imageFileList:[Ljava/io/File;

    array-length v3, v3

    if-lez v3, :cond_2

    iget-object v3, p0, Lcom/konka/picturePlayer/picturePlayerActivity;->imageFileList:[Ljava/io/File;

    invoke-virtual {p0, v3}, Lcom/konka/picturePlayer/picturePlayerActivity;->compareModifyTime([Ljava/io/File;)Ljava/io/File;

    move-result-object v3

    invoke-virtual {v3}, Ljava/io/File;->delete()Z

    goto :goto_0

    :cond_2
    invoke-direct {p0, v7, v7}, Lcom/konka/picturePlayer/picturePlayerActivity;->showProgressDialog(II)V

    goto :goto_0

    :cond_3
    const-string v3, "picFilePath"

    const-string v4, "/cache picFile.exists() is exist!!!"

    invoke-static {v3, v4}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    iget-object v3, p0, Lcom/konka/picturePlayer/picturePlayerActivity;->downloadhandler:Landroid/os/Handler;

    const/4 v4, 0x0

    invoke-virtual {v3, v4}, Landroid/os/Handler;->sendEmptyMessage(I)Z

    goto :goto_1

    :cond_4
    iget-object v3, p0, Lcom/konka/picturePlayer/picturePlayerActivity;->file:Ljava/io/File;

    invoke-virtual {v3}, Ljava/io/File;->mkdirs()Z

    const-string v3, "picFilePath"

    const-string v4, "/data/data/com.konka.playerService/cache is not exist!!!"

    invoke-static {v3, v4}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    new-instance v3, Ljava/lang/StringBuilder;

    iget-object v4, p0, Lcom/konka/picturePlayer/picturePlayerActivity;->picFilePath:Ljava/lang/String;

    invoke-static {v4}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v4

    invoke-direct {v3, v4}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    const-string v4, "/"

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    iput-object v3, p0, Lcom/konka/picturePlayer/picturePlayerActivity;->picturePath:Ljava/lang/String;

    new-instance v0, Ljava/io/File;

    iget-object v3, p0, Lcom/konka/picturePlayer/picturePlayerActivity;->picturePath:Ljava/lang/String;

    invoke-direct {v0, v3}, Ljava/io/File;-><init>(Ljava/lang/String;)V

    invoke-virtual {v0}, Ljava/io/File;->getParentFile()Ljava/io/File;

    move-result-object v3

    if-nez v3, :cond_5

    invoke-virtual {v0}, Ljava/io/File;->getParentFile()Ljava/io/File;

    move-result-object v3

    invoke-virtual {v3}, Ljava/io/File;->mkdirs()Z

    :cond_5
    invoke-virtual {p0}, Lcom/konka/picturePlayer/picturePlayerActivity;->creatImageFile()V

    goto/16 :goto_1
.end method

.method public registerCloseReceiver()V
    .locals 3

    new-instance v1, Lcom/konka/picturePlayer/picturePlayerActivity$closeReceiver;

    const/4 v2, 0x0

    invoke-direct {v1, p0, v2}, Lcom/konka/picturePlayer/picturePlayerActivity$closeReceiver;-><init>(Lcom/konka/picturePlayer/picturePlayerActivity;Lcom/konka/picturePlayer/picturePlayerActivity$closeReceiver;)V

    iput-object v1, p0, Lcom/konka/picturePlayer/picturePlayerActivity;->close:Lcom/konka/picturePlayer/picturePlayerActivity$closeReceiver;

    new-instance v0, Landroid/content/IntentFilter;

    invoke-direct {v0}, Landroid/content/IntentFilter;-><init>()V

    const-string v1, "com.konka.playerService.MultiScreenPlayerService"

    invoke-virtual {v0, v1}, Landroid/content/IntentFilter;->addAction(Ljava/lang/String;)V

    iget-object v1, p0, Lcom/konka/picturePlayer/picturePlayerActivity;->close:Lcom/konka/picturePlayer/picturePlayerActivity$closeReceiver;

    invoke-virtual {p0, v1, v0}, Lcom/konka/picturePlayer/picturePlayerActivity;->registerReceiver(Landroid/content/BroadcastReceiver;Landroid/content/IntentFilter;)Landroid/content/Intent;

    return-void
.end method

.method public registerDataReceiver()V
    .locals 3

    new-instance v1, Lcom/konka/picturePlayer/picturePlayerActivity$DataReceiver;

    const/4 v2, 0x0

    invoke-direct {v1, p0, v2}, Lcom/konka/picturePlayer/picturePlayerActivity$DataReceiver;-><init>(Lcom/konka/picturePlayer/picturePlayerActivity;Lcom/konka/picturePlayer/picturePlayerActivity$DataReceiver;)V

    iput-object v1, p0, Lcom/konka/picturePlayer/picturePlayerActivity;->dataReceiver:Lcom/konka/picturePlayer/picturePlayerActivity$DataReceiver;

    new-instance v0, Landroid/content/IntentFilter;

    invoke-direct {v0}, Landroid/content/IntentFilter;-><init>()V

    const-string v1, "com.konka.MultiScreenPlayer.picturePlayer"

    invoke-virtual {v0, v1}, Landroid/content/IntentFilter;->addAction(Ljava/lang/String;)V

    iget-object v1, p0, Lcom/konka/picturePlayer/picturePlayerActivity;->dataReceiver:Lcom/konka/picturePlayer/picturePlayerActivity$DataReceiver;

    invoke-virtual {p0, v1, v0}, Lcom/konka/picturePlayer/picturePlayerActivity;->registerReceiver(Landroid/content/BroadcastReceiver;Landroid/content/IntentFilter;)Landroid/content/Intent;

    return-void
.end method

.method public registerListeners()V
    .locals 4

    const/4 v1, 0x1

    const/high16 v3, 0x3f800000    # 1.0f

    const/4 v0, 0x0

    iget v2, p0, Lcom/konka/picturePlayer/picturePlayerActivity;->state:I

    packed-switch v2, :pswitch_data_0

    :goto_0
    :pswitch_0
    return-void

    :pswitch_1
    iput v0, p0, Lcom/konka/picturePlayer/picturePlayerActivity;->rotationClickCount:I

    iput v0, p0, Lcom/konka/picturePlayer/picturePlayerActivity;->zoomTimes:I

    iget-object v0, p0, Lcom/konka/picturePlayer/picturePlayerActivity;->picImageview:Lcom/konka/picturePlayer/ImageViewTouch;

    invoke-virtual {v0, v3}, Lcom/konka/picturePlayer/ImageViewTouch;->zoomTo(F)V

    invoke-virtual {p0}, Lcom/konka/picturePlayer/picturePlayerActivity;->setScaleDrawable()V

    goto :goto_0

    :pswitch_2
    iput v0, p0, Lcom/konka/picturePlayer/picturePlayerActivity;->rotationClickCount:I

    iput v0, p0, Lcom/konka/picturePlayer/picturePlayerActivity;->zoomTimes:I

    iget-object v0, p0, Lcom/konka/picturePlayer/picturePlayerActivity;->picImageview:Lcom/konka/picturePlayer/ImageViewTouch;

    invoke-virtual {v0, v3}, Lcom/konka/picturePlayer/ImageViewTouch;->zoomTo(F)V

    invoke-virtual {p0}, Lcom/konka/picturePlayer/picturePlayerActivity;->setScaleDrawable()V

    const/4 v0, -0x1

    invoke-virtual {p0, v0}, Lcom/konka/picturePlayer/picturePlayerActivity;->moveNextOrPrevious(I)V

    goto :goto_0

    :pswitch_3
    iput v0, p0, Lcom/konka/picturePlayer/picturePlayerActivity;->rotationClickCount:I

    iput v0, p0, Lcom/konka/picturePlayer/picturePlayerActivity;->rotationClickCount:I

    iput v0, p0, Lcom/konka/picturePlayer/picturePlayerActivity;->zoomTimes:I

    iget-boolean v2, p0, Lcom/konka/picturePlayer/picturePlayerActivity;->isAutoPlaying:Z

    if-eqz v2, :cond_1

    :goto_1
    iput-boolean v0, p0, Lcom/konka/picturePlayer/picturePlayerActivity;->isAutoPlaying:Z

    iget-boolean v0, p0, Lcom/konka/picturePlayer/picturePlayerActivity;->isAutoPlaying:Z

    if-eqz v0, :cond_0

    invoke-virtual {p0}, Lcom/konka/picturePlayer/picturePlayerActivity;->hideControlDelay()V

    :cond_0
    iget-object v0, p0, Lcom/konka/picturePlayer/picturePlayerActivity;->picImageview:Lcom/konka/picturePlayer/ImageViewTouch;

    invoke-virtual {v0, v3}, Lcom/konka/picturePlayer/ImageViewTouch;->zoomTo(F)V

    invoke-virtual {p0}, Lcom/konka/picturePlayer/picturePlayerActivity;->setScaleDrawable()V

    iget-object v0, p0, Lcom/konka/picturePlayer/picturePlayerActivity;->handler:Landroid/os/Handler;

    const v1, 0x50000001

    invoke-virtual {v0, v1}, Landroid/os/Handler;->sendEmptyMessage(I)Z

    goto :goto_0

    :cond_1
    move v0, v1

    goto :goto_1

    :pswitch_4
    iput v0, p0, Lcom/konka/picturePlayer/picturePlayerActivity;->rotationClickCount:I

    iput v0, p0, Lcom/konka/picturePlayer/picturePlayerActivity;->zoomTimes:I

    iget-object v0, p0, Lcom/konka/picturePlayer/picturePlayerActivity;->picImageview:Lcom/konka/picturePlayer/ImageViewTouch;

    invoke-virtual {v0, v3}, Lcom/konka/picturePlayer/ImageViewTouch;->zoomTo(F)V

    invoke-virtual {p0}, Lcom/konka/picturePlayer/picturePlayerActivity;->setScaleDrawable()V

    invoke-virtual {p0, v1}, Lcom/konka/picturePlayer/picturePlayerActivity;->moveNextOrPrevious(I)V

    goto :goto_0

    :pswitch_5
    iput v0, p0, Lcom/konka/picturePlayer/picturePlayerActivity;->rotationClickCount:I

    iput-boolean v0, p0, Lcom/konka/picturePlayer/picturePlayerActivity;->isAutoPlaying:Z

    goto :goto_0

    :pswitch_6
    iput-boolean v0, p0, Lcom/konka/picturePlayer/picturePlayerActivity;->isAutoPlaying:Z

    iget v0, p0, Lcom/konka/picturePlayer/picturePlayerActivity;->rotateTimes:I

    add-int/lit8 v0, v0, -0x1

    iput v0, p0, Lcom/konka/picturePlayer/picturePlayerActivity;->rotateTimes:I

    iget-object v0, p0, Lcom/konka/picturePlayer/picturePlayerActivity;->picImageview:Lcom/konka/picturePlayer/ImageViewTouch;

    const/high16 v1, -0x3d4c0000    # -90.0f

    invoke-virtual {v0, v1}, Lcom/konka/picturePlayer/ImageViewTouch;->rotateImage(F)V

    goto :goto_0

    :pswitch_7
    iput-boolean v0, p0, Lcom/konka/picturePlayer/picturePlayerActivity;->isAutoPlaying:Z

    iget v0, p0, Lcom/konka/picturePlayer/picturePlayerActivity;->rotateTimes:I

    add-int/lit8 v0, v0, 0x1

    iput v0, p0, Lcom/konka/picturePlayer/picturePlayerActivity;->rotateTimes:I

    iget-object v0, p0, Lcom/konka/picturePlayer/picturePlayerActivity;->picImageview:Lcom/konka/picturePlayer/ImageViewTouch;

    const/high16 v1, 0x42b40000    # 90.0f

    invoke-virtual {v0, v1}, Lcom/konka/picturePlayer/ImageViewTouch;->rotateImage(F)V

    goto :goto_0

    :pswitch_8
    iput-boolean v0, p0, Lcom/konka/picturePlayer/picturePlayerActivity;->isAutoPlaying:Z

    invoke-virtual {p0}, Lcom/konka/picturePlayer/picturePlayerActivity;->zoomIn()V

    goto :goto_0

    :pswitch_9
    iput-boolean v0, p0, Lcom/konka/picturePlayer/picturePlayerActivity;->isAutoPlaying:Z

    invoke-virtual {p0}, Lcom/konka/picturePlayer/picturePlayerActivity;->zoomOut()V

    goto/16 :goto_0

    :pswitch_a
    iput-boolean v0, p0, Lcom/konka/picturePlayer/picturePlayerActivity;->isAutoPlaying:Z

    sput-boolean v1, Lcom/konka/picturePlayer/picturePlayerActivity;->isCanMove:Z

    iget-object v0, p0, Lcom/konka/picturePlayer/picturePlayerActivity;->hideHandler:Landroid/os/Handler;

    const/16 v1, 0x12

    invoke-virtual {v0, v1}, Landroid/os/Handler;->sendEmptyMessage(I)Z

    goto/16 :goto_0

    nop

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_1
        :pswitch_2
        :pswitch_3
        :pswitch_4
        :pswitch_5
        :pswitch_6
        :pswitch_7
        :pswitch_8
        :pswitch_9
        :pswitch_0
        :pswitch_0
        :pswitch_0
        :pswitch_0
        :pswitch_0
        :pswitch_0
        :pswitch_a
    .end packed-switch
.end method

.method public registerSwitchReceiver()V
    .locals 3

    new-instance v1, Lcom/konka/picturePlayer/picturePlayerActivity$switchReceiver;

    const/4 v2, 0x0

    invoke-direct {v1, p0, v2}, Lcom/konka/picturePlayer/picturePlayerActivity$switchReceiver;-><init>(Lcom/konka/picturePlayer/picturePlayerActivity;Lcom/konka/picturePlayer/picturePlayerActivity$switchReceiver;)V

    iput-object v1, p0, Lcom/konka/picturePlayer/picturePlayerActivity;->mediaPlayerSwitch:Lcom/konka/picturePlayer/picturePlayerActivity$switchReceiver;

    new-instance v0, Landroid/content/IntentFilter;

    invoke-direct {v0}, Landroid/content/IntentFilter;-><init>()V

    const-string v1, "com.konka.playerService.MultiScreenPlayerSwtich"

    invoke-virtual {v0, v1}, Landroid/content/IntentFilter;->addAction(Ljava/lang/String;)V

    iget-object v1, p0, Lcom/konka/picturePlayer/picturePlayerActivity;->mediaPlayerSwitch:Lcom/konka/picturePlayer/picturePlayerActivity$switchReceiver;

    invoke-virtual {p0, v1, v0}, Lcom/konka/picturePlayer/picturePlayerActivity;->registerReceiver(Landroid/content/BroadcastReceiver;Landroid/content/IntentFilter;)Landroid/content/Intent;

    return-void
.end method

.method public resetScaleDrawable()V
    .locals 3

    const-string v0, "MultiScreenPictureActivity"

    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "zoomTimes = "

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    iget v2, p0, Lcom/konka/picturePlayer/picturePlayerActivity;->zoomTimes:I

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    const/4 v0, 0x0

    iput v0, p0, Lcom/konka/picturePlayer/picturePlayerActivity;->zoomTimes:I

    iget-object v0, p0, Lcom/konka/picturePlayer/picturePlayerActivity;->picImageview:Lcom/konka/picturePlayer/ImageViewTouch;

    const/high16 v1, 0x3f800000    # 1.0f

    invoke-virtual {v0, v1}, Lcom/konka/picturePlayer/ImageViewTouch;->zoomTo(F)V

    invoke-virtual {p0}, Lcom/konka/picturePlayer/picturePlayerActivity;->setScaleDrawable()V

    return-void
.end method

.method public resetZoom()V
    .locals 1

    iget v0, p0, Lcom/konka/picturePlayer/picturePlayerActivity;->zoomTimes:I

    rsub-int/lit8 v0, v0, 0x0

    iput v0, p0, Lcom/konka/picturePlayer/picturePlayerActivity;->zoomTimes:I

    invoke-virtual {p0}, Lcom/konka/picturePlayer/picturePlayerActivity;->resetScaleDrawable()V

    return-void
.end method

.method public rightRotation()V
    .locals 8

    const/4 v1, 0x0

    iget v2, p0, Lcom/konka/picturePlayer/picturePlayerActivity;->rotationClickCount:I

    add-int/lit8 v2, v2, 0x1

    iput v2, p0, Lcom/konka/picturePlayer/picturePlayerActivity;->rotationClickCount:I

    iget v2, p0, Lcom/konka/picturePlayer/picturePlayerActivity;->rotationClickCount:I

    rem-int/lit8 v2, v2, 0x4

    iput v2, p0, Lcom/konka/picturePlayer/picturePlayerActivity;->rotationClickCount:I

    iput-boolean v1, p0, Lcom/konka/picturePlayer/picturePlayerActivity;->isAutoPlaying:Z

    iget v2, p0, Lcom/konka/picturePlayer/picturePlayerActivity;->currentNum:I

    invoke-virtual {p0, v2}, Lcom/konka/picturePlayer/picturePlayerActivity;->getBitmap(I)Landroid/graphics/Bitmap;

    move-result-object v0

    invoke-virtual {v0}, Landroid/graphics/Bitmap;->getWidth()I

    move-result v3

    invoke-virtual {v0}, Landroid/graphics/Bitmap;->getHeight()I

    move-result v4

    new-instance v5, Landroid/graphics/Matrix;

    invoke-direct {v5}, Landroid/graphics/Matrix;-><init>()V

    iget v2, p0, Lcom/konka/picturePlayer/picturePlayerActivity;->rotationClickCount:I

    mul-int/lit8 v2, v2, 0x5a

    int-to-float v2, v2

    invoke-virtual {v5, v2}, Landroid/graphics/Matrix;->postRotate(F)Z

    const/4 v6, 0x1

    move v2, v1

    invoke-static/range {v0 .. v6}, Landroid/graphics/Bitmap;->createBitmap(Landroid/graphics/Bitmap;IIIILandroid/graphics/Matrix;Z)Landroid/graphics/Bitmap;

    move-result-object v7

    iget-object v1, p0, Lcom/konka/picturePlayer/picturePlayerActivity;->picImageview:Lcom/konka/picturePlayer/ImageViewTouch;

    invoke-virtual {v1, v7}, Lcom/konka/picturePlayer/ImageViewTouch;->setImageBitmap(Landroid/graphics/Bitmap;)V

    return-void
.end method

.method public sendPlayerStateToPlayerService(Landroid/content/Context;IIILjava/lang/String;)V
    .locals 5
    .param p1    # Landroid/content/Context;
    .param p2    # I
    .param p3    # I
    .param p4    # I
    .param p5    # Ljava/lang/String;

    new-instance v1, Landroid/content/Intent;

    invoke-direct {v1}, Landroid/content/Intent;-><init>()V

    const-string v2, "com.konka.MultiScreeenPlayer.PlayerState"

    invoke-virtual {v1, v2}, Landroid/content/Intent;->setAction(Ljava/lang/String;)Landroid/content/Intent;

    new-instance v0, Landroid/os/Bundle;

    invoke-direct {v0}, Landroid/os/Bundle;-><init>()V

    const-string v2, "playerState"

    invoke-virtual {v0, v2, p2}, Landroid/os/Bundle;->putInt(Ljava/lang/String;I)V

    const-string v2, "currentTime"

    invoke-virtual {v0, v2, p3}, Landroid/os/Bundle;->putInt(Ljava/lang/String;I)V

    const-string v2, "DurationTime"

    invoke-virtual {v0, v2, p4}, Landroid/os/Bundle;->putInt(Ljava/lang/String;I)V

    const-string v2, "fileUrl"

    invoke-virtual {v0, v2, p5}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V

    const-string v2, "mobileIp"

    iget-object v3, p0, Lcom/konka/picturePlayer/picturePlayerActivity;->mobileIp:Ljava/lang/String;

    invoke-virtual {v0, v2, v3}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V

    invoke-virtual {v1, v0}, Landroid/content/Intent;->putExtras(Landroid/os/Bundle;)Landroid/content/Intent;

    invoke-virtual {p1, v1}, Landroid/content/Context;->sendBroadcast(Landroid/content/Intent;)V

    const-string v2, "sendBroadcast"

    new-instance v3, Ljava/lang/StringBuilder;

    const-string v4, "sendPlayerStateToPlayerService:"

    invoke-direct {v3, v4}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v3, p2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v3

    const-string v4, ","

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3, p3}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v3

    const-string v4, ","

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3, p4}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v3

    const-string v4, ","

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3, p5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v2, v3}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    return-void
.end method

.method public setScaleDrawable()V
    .locals 3

    iget v0, p0, Lcom/konka/picturePlayer/picturePlayerActivity;->zoomTimes:I

    if-nez v0, :cond_0

    iget-object v0, p0, Lcom/konka/picturePlayer/picturePlayerActivity;->scaleImageView:Landroid/widget/ImageView;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Landroid/widget/ImageView;->setImageDrawable(Landroid/graphics/drawable/Drawable;)V

    :cond_0
    iget v0, p0, Lcom/konka/picturePlayer/picturePlayerActivity;->zoomTimes:I

    if-lez v0, :cond_1

    iget-object v0, p0, Lcom/konka/picturePlayer/picturePlayerActivity;->scaleImageView:Landroid/widget/ImageView;

    sget-object v1, Lcom/konka/picturePlayer/picturePlayerActivity;->enlargeDrawable:[I

    iget v2, p0, Lcom/konka/picturePlayer/picturePlayerActivity;->zoomTimes:I

    aget v1, v1, v2

    invoke-virtual {v0, v1}, Landroid/widget/ImageView;->setImageResource(I)V

    :cond_1
    iget v0, p0, Lcom/konka/picturePlayer/picturePlayerActivity;->zoomTimes:I

    if-gez v0, :cond_2

    iget-object v0, p0, Lcom/konka/picturePlayer/picturePlayerActivity;->scaleImageView:Landroid/widget/ImageView;

    sget-object v1, Lcom/konka/picturePlayer/picturePlayerActivity;->descaleDrawable:[I

    iget v2, p0, Lcom/konka/picturePlayer/picturePlayerActivity;->zoomTimes:I

    neg-int v2, v2

    aget v1, v1, v2

    invoke-virtual {v0, v1}, Landroid/widget/ImageView;->setImageResource(I)V

    :cond_2
    return-void
.end method

.method public setState(I)V
    .locals 0
    .param p1    # I

    iput p1, p0, Lcom/konka/picturePlayer/picturePlayerActivity;->state:I

    return-void
.end method

.method public setStateDrawable(I)V
    .locals 4
    .param p1    # I

    const/16 v3, 0x14

    iget-boolean v0, p0, Lcom/konka/picturePlayer/picturePlayerActivity;->isControllerShow:Z

    if-nez v0, :cond_0

    iget-boolean v0, p0, Lcom/konka/picturePlayer/picturePlayerActivity;->isAutoPlaying:Z

    if-eqz v0, :cond_1

    iget-object v0, p0, Lcom/konka/picturePlayer/picturePlayerActivity;->stateDrawable:Landroid/widget/ImageView;

    invoke-virtual {v0, p1}, Landroid/widget/ImageView;->setImageResource(I)V

    :cond_0
    :goto_0
    return-void

    :cond_1
    iget-object v0, p0, Lcom/konka/picturePlayer/picturePlayerActivity;->stateDrawable:Landroid/widget/ImageView;

    invoke-virtual {v0, p1}, Landroid/widget/ImageView;->setImageResource(I)V

    iget-object v0, p0, Lcom/konka/picturePlayer/picturePlayerActivity;->hideHandler:Landroid/os/Handler;

    invoke-virtual {v0, v3}, Landroid/os/Handler;->removeMessages(I)V

    iget-object v0, p0, Lcom/konka/picturePlayer/picturePlayerActivity;->hideHandler:Landroid/os/Handler;

    const-wide/16 v1, 0xfa0

    invoke-virtual {v0, v3, v1, v2}, Landroid/os/Handler;->sendEmptyMessageDelayed(IJ)Z

    goto :goto_0
.end method

.method public settextViewName()V
    .locals 4

    invoke-static {}, Ljava/lang/System;->gc()V

    iget-object v2, p0, Lcom/konka/picturePlayer/picturePlayerActivity;->picPaths:Ljava/util/List;

    const/4 v3, 0x0

    invoke-interface {v2, v3}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Ljava/lang/String;

    const-string v2, "/"

    invoke-virtual {v1, v2}, Ljava/lang/String;->lastIndexOf(Ljava/lang/String;)I

    move-result v2

    add-int/lit8 v2, v2, 0x1

    invoke-virtual {v1}, Ljava/lang/String;->length()I

    move-result v3

    invoke-virtual {v1, v2, v3}, Ljava/lang/String;->substring(II)Ljava/lang/String;

    move-result-object v2

    invoke-static {v2}, Ljava/net/URLDecoder;->decode(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    iget-object v2, p0, Lcom/konka/picturePlayer/picturePlayerActivity;->textView:Landroid/widget/TextView;

    invoke-virtual {v2, v0}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    return-void
.end method

.method protected showToast(Ljava/lang/String;)V
    .locals 1
    .param p1    # Ljava/lang/String;

    const/4 v0, 0x1

    invoke-static {p0, p1, v0}, Landroid/widget/Toast;->makeText(Landroid/content/Context;Ljava/lang/CharSequence;I)Landroid/widget/Toast;

    move-result-object v0

    invoke-virtual {v0}, Landroid/widget/Toast;->show()V

    return-void
.end method

.method public small()V
    .locals 12

    const/4 v1, 0x0

    iput-boolean v1, p0, Lcom/konka/picturePlayer/picturePlayerActivity;->isAutoPlaying:Z

    iget v0, p0, Lcom/konka/picturePlayer/picturePlayerActivity;->currentNum:I

    invoke-virtual {p0, v0}, Lcom/konka/picturePlayer/picturePlayerActivity;->getBitmap(I)Landroid/graphics/Bitmap;

    move-result-object v0

    invoke-virtual {v0}, Landroid/graphics/Bitmap;->getWidth()I

    move-result v3

    iget v0, p0, Lcom/konka/picturePlayer/picturePlayerActivity;->currentNum:I

    invoke-virtual {p0, v0}, Lcom/konka/picturePlayer/picturePlayerActivity;->getBitmap(I)Landroid/graphics/Bitmap;

    move-result-object v0

    invoke-virtual {v0}, Landroid/graphics/Bitmap;->getHeight()I

    move-result v4

    iget v0, p0, Lcom/konka/picturePlayer/picturePlayerActivity;->scaleWidth:F

    int-to-float v2, v3

    mul-float/2addr v0, v2

    iget-object v2, p0, Lcom/konka/picturePlayer/picturePlayerActivity;->layout1:Landroid/widget/LinearLayout;

    invoke-virtual {v2}, Landroid/widget/LinearLayout;->getWidth()I

    move-result v2

    div-int/lit8 v2, v2, 0xa

    int-to-float v2, v2

    cmpg-float v0, v0, v2

    if-ltz v0, :cond_0

    iget v0, p0, Lcom/konka/picturePlayer/picturePlayerActivity;->scaleHeight:F

    int-to-float v2, v4

    mul-float/2addr v0, v2

    iget-object v2, p0, Lcom/konka/picturePlayer/picturePlayerActivity;->layout1:Landroid/widget/LinearLayout;

    invoke-virtual {v2}, Landroid/widget/LinearLayout;->getHeight()I

    move-result v2

    div-int/lit8 v2, v2, 0xa

    int-to-float v2, v2

    cmpg-float v0, v0, v2

    if-gez v0, :cond_1

    :cond_0
    :goto_0
    return-void

    :cond_1
    const-wide v8, 0x3fe999999999999aL    # 0.8

    iget v0, p0, Lcom/konka/picturePlayer/picturePlayerActivity;->scaleWidth:F

    float-to-double v10, v0

    mul-double/2addr v10, v8

    double-to-float v0, v10

    iput v0, p0, Lcom/konka/picturePlayer/picturePlayerActivity;->scaleWidth:F

    iget v0, p0, Lcom/konka/picturePlayer/picturePlayerActivity;->scaleHeight:F

    float-to-double v10, v0

    mul-double/2addr v10, v8

    double-to-float v0, v10

    iput v0, p0, Lcom/konka/picturePlayer/picturePlayerActivity;->scaleHeight:F

    new-instance v5, Landroid/graphics/Matrix;

    invoke-direct {v5}, Landroid/graphics/Matrix;-><init>()V

    iget v0, p0, Lcom/konka/picturePlayer/picturePlayerActivity;->scaleWidth:F

    iget v2, p0, Lcom/konka/picturePlayer/picturePlayerActivity;->scaleHeight:F

    invoke-virtual {v5, v0, v2}, Landroid/graphics/Matrix;->postScale(FF)Z

    iget v0, p0, Lcom/konka/picturePlayer/picturePlayerActivity;->currentNum:I

    invoke-virtual {p0, v0}, Lcom/konka/picturePlayer/picturePlayerActivity;->getBitmap(I)Landroid/graphics/Bitmap;

    move-result-object v0

    const/4 v6, 0x1

    move v2, v1

    invoke-static/range {v0 .. v6}, Landroid/graphics/Bitmap;->createBitmap(Landroid/graphics/Bitmap;IIIILandroid/graphics/Matrix;Z)Landroid/graphics/Bitmap;

    move-result-object v7

    iget-object v0, p0, Lcom/konka/picturePlayer/picturePlayerActivity;->picImageview:Lcom/konka/picturePlayer/ImageViewTouch;

    invoke-virtual {v0, v7}, Lcom/konka/picturePlayer/ImageViewTouch;->setImageBitmap(Landroid/graphics/Bitmap;)V

    goto :goto_0
.end method

.method public zoomIn()V
    .locals 2

    iget v0, p0, Lcom/konka/picturePlayer/picturePlayerActivity;->zoomTimes:I

    const/4 v1, 0x4

    if-lt v0, v1, :cond_0

    :goto_0
    return-void

    :cond_0
    iget v0, p0, Lcom/konka/picturePlayer/picturePlayerActivity;->zoomTimes:I

    add-int/lit8 v0, v0, 0x1

    iput v0, p0, Lcom/konka/picturePlayer/picturePlayerActivity;->zoomTimes:I

    iget-object v0, p0, Lcom/konka/picturePlayer/picturePlayerActivity;->picImageview:Lcom/konka/picturePlayer/ImageViewTouch;

    invoke-virtual {v0}, Lcom/konka/picturePlayer/ImageViewTouch;->zoomIn()V

    invoke-virtual {p0}, Lcom/konka/picturePlayer/picturePlayerActivity;->setScaleDrawable()V

    goto :goto_0
.end method

.method public zoomOut()V
    .locals 2

    iget v0, p0, Lcom/konka/picturePlayer/picturePlayerActivity;->zoomTimes:I

    const/4 v1, -0x4

    if-gt v0, v1, :cond_0

    :goto_0
    return-void

    :cond_0
    iget v0, p0, Lcom/konka/picturePlayer/picturePlayerActivity;->zoomTimes:I

    add-int/lit8 v0, v0, -0x1

    iput v0, p0, Lcom/konka/picturePlayer/picturePlayerActivity;->zoomTimes:I

    iget-object v0, p0, Lcom/konka/picturePlayer/picturePlayerActivity;->picImageview:Lcom/konka/picturePlayer/ImageViewTouch;

    invoke-virtual {v0}, Lcom/konka/picturePlayer/ImageViewTouch;->zoomOut()V

    invoke-virtual {p0}, Lcom/konka/picturePlayer/picturePlayerActivity;->setScaleDrawable()V

    goto :goto_0
.end method
