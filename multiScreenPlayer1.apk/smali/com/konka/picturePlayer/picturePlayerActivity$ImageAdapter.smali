.class Lcom/konka/picturePlayer/picturePlayerActivity$ImageAdapter;
.super Landroid/widget/BaseAdapter;
.source "picturePlayerActivity.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/konka/picturePlayer/picturePlayerActivity;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = "ImageAdapter"
.end annotation


# instance fields
.field private context:Landroid/content/Context;

.field final synthetic this$0:Lcom/konka/picturePlayer/picturePlayerActivity;


# direct methods
.method public constructor <init>(Lcom/konka/picturePlayer/picturePlayerActivity;Landroid/content/Context;)V
    .locals 0
    .param p2    # Landroid/content/Context;

    iput-object p1, p0, Lcom/konka/picturePlayer/picturePlayerActivity$ImageAdapter;->this$0:Lcom/konka/picturePlayer/picturePlayerActivity;

    invoke-direct {p0}, Landroid/widget/BaseAdapter;-><init>()V

    iput-object p2, p0, Lcom/konka/picturePlayer/picturePlayerActivity$ImageAdapter;->context:Landroid/content/Context;

    return-void
.end method


# virtual methods
.method public getCount()I
    .locals 1

    iget-object v0, p0, Lcom/konka/picturePlayer/picturePlayerActivity$ImageAdapter;->this$0:Lcom/konka/picturePlayer/picturePlayerActivity;

    iget-object v0, v0, Lcom/konka/picturePlayer/picturePlayerActivity;->picPaths:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->size()I

    move-result v0

    return v0
.end method

.method public getItem(I)Ljava/lang/Object;
    .locals 1
    .param p1    # I

    invoke-static {p1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v0

    return-object v0
.end method

.method public getItemId(I)J
    .locals 2
    .param p1    # I

    int-to-long v0, p1

    return-wide v0
.end method

.method public getView(ILandroid/view/View;Landroid/view/ViewGroup;)Landroid/view/View;
    .locals 4
    .param p1    # I
    .param p2    # Landroid/view/View;
    .param p3    # Landroid/view/ViewGroup;

    const/4 v2, -0x1

    new-instance v0, Landroid/widget/ImageView;

    iget-object v1, p0, Lcom/konka/picturePlayer/picturePlayerActivity$ImageAdapter;->context:Landroid/content/Context;

    invoke-direct {v0, v1}, Landroid/widget/ImageView;-><init>(Landroid/content/Context;)V

    new-instance v1, Landroid/widget/Gallery$LayoutParams;

    invoke-direct {v1, v2, v2}, Landroid/widget/Gallery$LayoutParams;-><init>(II)V

    invoke-virtual {v0, v1}, Landroid/widget/ImageView;->setLayoutParams(Landroid/view/ViewGroup$LayoutParams;)V

    const/4 v1, 0x1

    invoke-virtual {v0, v1}, Landroid/widget/ImageView;->setAdjustViewBounds(Z)V

    new-instance v2, Lcom/konka/picturePlayer/ImageLoader;

    iget-object v3, p0, Lcom/konka/picturePlayer/picturePlayerActivity$ImageAdapter;->context:Landroid/content/Context;

    iget-object v1, p0, Lcom/konka/picturePlayer/picturePlayerActivity$ImageAdapter;->this$0:Lcom/konka/picturePlayer/picturePlayerActivity;

    iget-object v1, v1, Lcom/konka/picturePlayer/picturePlayerActivity;->picPaths:Ljava/util/List;

    invoke-interface {v1, p1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Ljava/lang/String;

    invoke-direct {v2, v3, v0, v1}, Lcom/konka/picturePlayer/ImageLoader;-><init>(Landroid/content/Context;Landroid/widget/ImageView;Ljava/lang/String;)V

    const/4 v1, 0x0

    new-array v1, v1, [Ljava/lang/Void;

    invoke-virtual {v2, v1}, Lcom/konka/picturePlayer/ImageLoader;->execute([Ljava/lang/Object;)Landroid/os/AsyncTask;

    return-object v0
.end method
