.class Lcom/konka/picturePlayer/picturePlayerActivity$2$1;
.super Ljava/lang/Object;
.source "picturePlayerActivity.java"

# interfaces
.implements Ljava/lang/Runnable;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/konka/picturePlayer/picturePlayerActivity$2;->handleMessage(Landroid/os/Message;)V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$1:Lcom/konka/picturePlayer/picturePlayerActivity$2;


# direct methods
.method constructor <init>(Lcom/konka/picturePlayer/picturePlayerActivity$2;)V
    .locals 0

    iput-object p1, p0, Lcom/konka/picturePlayer/picturePlayerActivity$2$1;->this$1:Lcom/konka/picturePlayer/picturePlayerActivity$2;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public run()V
    .locals 10

    const/4 v9, 0x1

    :cond_0
    :goto_0
    iget-object v0, p0, Lcom/konka/picturePlayer/picturePlayerActivity$2$1;->this$1:Lcom/konka/picturePlayer/picturePlayerActivity$2;

    # getter for: Lcom/konka/picturePlayer/picturePlayerActivity$2;->this$0:Lcom/konka/picturePlayer/picturePlayerActivity;
    invoke-static {v0}, Lcom/konka/picturePlayer/picturePlayerActivity$2;->access$0(Lcom/konka/picturePlayer/picturePlayerActivity$2;)Lcom/konka/picturePlayer/picturePlayerActivity;

    move-result-object v0

    iget-boolean v0, v0, Lcom/konka/picturePlayer/picturePlayerActivity;->isrun:Z

    if-nez v0, :cond_1

    return-void

    :cond_1
    :try_start_0
    iget-object v0, p0, Lcom/konka/picturePlayer/picturePlayerActivity$2$1;->this$1:Lcom/konka/picturePlayer/picturePlayerActivity$2;

    # getter for: Lcom/konka/picturePlayer/picturePlayerActivity$2;->this$0:Lcom/konka/picturePlayer/picturePlayerActivity;
    invoke-static {v0}, Lcom/konka/picturePlayer/picturePlayerActivity$2;->access$0(Lcom/konka/picturePlayer/picturePlayerActivity$2;)Lcom/konka/picturePlayer/picturePlayerActivity;

    move-result-object v0

    # getter for: Lcom/konka/picturePlayer/picturePlayerActivity;->haveNextFlag:I
    invoke-static {v0}, Lcom/konka/picturePlayer/picturePlayerActivity;->access$1(Lcom/konka/picturePlayer/picturePlayerActivity;)I

    move-result v0

    if-eqz v0, :cond_2

    const-wide/16 v0, 0xbb8

    invoke-static {v0, v1}, Ljava/lang/Thread;->sleep(J)V

    iget-object v0, p0, Lcom/konka/picturePlayer/picturePlayerActivity$2$1;->this$1:Lcom/konka/picturePlayer/picturePlayerActivity$2;

    # getter for: Lcom/konka/picturePlayer/picturePlayerActivity$2;->this$0:Lcom/konka/picturePlayer/picturePlayerActivity;
    invoke-static {v0}, Lcom/konka/picturePlayer/picturePlayerActivity$2;->access$0(Lcom/konka/picturePlayer/picturePlayerActivity$2;)Lcom/konka/picturePlayer/picturePlayerActivity;

    move-result-object v0

    # getter for: Lcom/konka/picturePlayer/picturePlayerActivity;->nextFlag:I
    invoke-static {v0}, Lcom/konka/picturePlayer/picturePlayerActivity;->access$2(Lcom/konka/picturePlayer/picturePlayerActivity;)I

    move-result v0

    if-lt v0, v9, :cond_0

    iget-object v0, p0, Lcom/konka/picturePlayer/picturePlayerActivity$2$1;->this$1:Lcom/konka/picturePlayer/picturePlayerActivity$2;

    # getter for: Lcom/konka/picturePlayer/picturePlayerActivity$2;->this$0:Lcom/konka/picturePlayer/picturePlayerActivity;
    invoke-static {v0}, Lcom/konka/picturePlayer/picturePlayerActivity$2;->access$0(Lcom/konka/picturePlayer/picturePlayerActivity$2;)Lcom/konka/picturePlayer/picturePlayerActivity;

    move-result-object v0

    iget-object v1, p0, Lcom/konka/picturePlayer/picturePlayerActivity$2$1;->this$1:Lcom/konka/picturePlayer/picturePlayerActivity$2;

    # getter for: Lcom/konka/picturePlayer/picturePlayerActivity$2;->this$0:Lcom/konka/picturePlayer/picturePlayerActivity;
    invoke-static {v1}, Lcom/konka/picturePlayer/picturePlayerActivity$2;->access$0(Lcom/konka/picturePlayer/picturePlayerActivity$2;)Lcom/konka/picturePlayer/picturePlayerActivity;

    move-result-object v1

    const/4 v2, 0x1

    const/4 v3, 0x0

    const/4 v4, 0x0

    new-instance v7, Ljava/lang/StringBuilder;

    iget-object v5, p0, Lcom/konka/picturePlayer/picturePlayerActivity$2$1;->this$1:Lcom/konka/picturePlayer/picturePlayerActivity$2;

    # getter for: Lcom/konka/picturePlayer/picturePlayerActivity$2;->this$0:Lcom/konka/picturePlayer/picturePlayerActivity;
    invoke-static {v5}, Lcom/konka/picturePlayer/picturePlayerActivity$2;->access$0(Lcom/konka/picturePlayer/picturePlayerActivity$2;)Lcom/konka/picturePlayer/picturePlayerActivity;

    move-result-object v5

    iget-object v5, v5, Lcom/konka/picturePlayer/picturePlayerActivity;->picPaths:Ljava/util/List;

    const/4 v8, 0x0

    invoke-interface {v5, v8}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v5

    check-cast v5, Ljava/lang/String;

    invoke-static {v5}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v5

    invoke-direct {v7, v5}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    const-string v5, "&mp"

    invoke-virtual {v7, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    invoke-virtual/range {v0 .. v5}, Lcom/konka/picturePlayer/picturePlayerActivity;->sendPlayerStateToPlayerService(Landroid/content/Context;IIILjava/lang/String;)V

    iget-object v0, p0, Lcom/konka/picturePlayer/picturePlayerActivity$2$1;->this$1:Lcom/konka/picturePlayer/picturePlayerActivity$2;

    # getter for: Lcom/konka/picturePlayer/picturePlayerActivity$2;->this$0:Lcom/konka/picturePlayer/picturePlayerActivity;
    invoke-static {v0}, Lcom/konka/picturePlayer/picturePlayerActivity$2;->access$0(Lcom/konka/picturePlayer/picturePlayerActivity$2;)Lcom/konka/picturePlayer/picturePlayerActivity;

    move-result-object v0

    const/4 v1, 0x0

    iput-boolean v1, v0, Lcom/konka/picturePlayer/picturePlayerActivity;->isrun:Z
    :try_end_0
    .catch Ljava/lang/InterruptedException; {:try_start_0 .. :try_end_0} :catch_0

    goto :goto_0

    :catch_0
    move-exception v6

    invoke-virtual {v6}, Ljava/lang/InterruptedException;->printStackTrace()V

    goto :goto_0

    :cond_2
    :try_start_1
    iget-object v0, p0, Lcom/konka/picturePlayer/picturePlayerActivity$2$1;->this$1:Lcom/konka/picturePlayer/picturePlayerActivity$2;

    # getter for: Lcom/konka/picturePlayer/picturePlayerActivity$2;->this$0:Lcom/konka/picturePlayer/picturePlayerActivity;
    invoke-static {v0}, Lcom/konka/picturePlayer/picturePlayerActivity$2;->access$0(Lcom/konka/picturePlayer/picturePlayerActivity$2;)Lcom/konka/picturePlayer/picturePlayerActivity;

    move-result-object v0

    const/4 v1, 0x0

    iput-boolean v1, v0, Lcom/konka/picturePlayer/picturePlayerActivity;->isrun:Z

    iget-object v0, p0, Lcom/konka/picturePlayer/picturePlayerActivity$2$1;->this$1:Lcom/konka/picturePlayer/picturePlayerActivity$2;

    # getter for: Lcom/konka/picturePlayer/picturePlayerActivity$2;->this$0:Lcom/konka/picturePlayer/picturePlayerActivity;
    invoke-static {v0}, Lcom/konka/picturePlayer/picturePlayerActivity$2;->access$0(Lcom/konka/picturePlayer/picturePlayerActivity$2;)Lcom/konka/picturePlayer/picturePlayerActivity;

    move-result-object v0

    const/4 v1, 0x0

    invoke-static {v0, v1}, Lcom/konka/picturePlayer/picturePlayerActivity;->access$3(Lcom/konka/picturePlayer/picturePlayerActivity;I)V
    :try_end_1
    .catch Ljava/lang/InterruptedException; {:try_start_1 .. :try_end_1} :catch_0

    goto :goto_0
.end method
