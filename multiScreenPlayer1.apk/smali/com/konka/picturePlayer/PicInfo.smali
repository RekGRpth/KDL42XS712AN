.class public Lcom/konka/picturePlayer/PicInfo;
.super Ljava/lang/Object;
.source "PicInfo.java"

# interfaces
.implements Landroid/os/Parcelable;


# instance fields
.field private id:I

.field private m_path:Ljava/lang/String;

.field private m_size:D

.field private m_title:Ljava/lang/String;


# direct methods
.method public constructor <init>()V
    .locals 0

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public describeContents()I
    .locals 1

    const/4 v0, 0x0

    return v0
.end method

.method public getId()I
    .locals 1

    iget v0, p0, Lcom/konka/picturePlayer/PicInfo;->id:I

    return v0
.end method

.method public getM_path()Ljava/lang/String;
    .locals 1

    iget-object v0, p0, Lcom/konka/picturePlayer/PicInfo;->m_path:Ljava/lang/String;

    return-object v0
.end method

.method public getM_size()D
    .locals 2

    iget-wide v0, p0, Lcom/konka/picturePlayer/PicInfo;->m_size:D

    return-wide v0
.end method

.method public getM_title()Ljava/lang/String;
    .locals 1

    iget-object v0, p0, Lcom/konka/picturePlayer/PicInfo;->m_title:Ljava/lang/String;

    return-object v0
.end method

.method public setId(I)V
    .locals 0
    .param p1    # I

    iput p1, p0, Lcom/konka/picturePlayer/PicInfo;->id:I

    return-void
.end method

.method public setM_path(Ljava/lang/String;)V
    .locals 0
    .param p1    # Ljava/lang/String;

    iput-object p1, p0, Lcom/konka/picturePlayer/PicInfo;->m_path:Ljava/lang/String;

    return-void
.end method

.method public setM_size(D)V
    .locals 0
    .param p1    # D

    iput-wide p1, p0, Lcom/konka/picturePlayer/PicInfo;->m_size:D

    return-void
.end method

.method public setM_title(Ljava/lang/String;)V
    .locals 0
    .param p1    # Ljava/lang/String;

    iput-object p1, p0, Lcom/konka/picturePlayer/PicInfo;->m_title:Ljava/lang/String;

    return-void
.end method

.method public writeToParcel(Landroid/os/Parcel;I)V
    .locals 2
    .param p1    # Landroid/os/Parcel;
    .param p2    # I

    iget-object v0, p0, Lcom/konka/picturePlayer/PicInfo;->m_path:Ljava/lang/String;

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeString(Ljava/lang/String;)V

    iget-object v0, p0, Lcom/konka/picturePlayer/PicInfo;->m_title:Ljava/lang/String;

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeString(Ljava/lang/String;)V

    iget-wide v0, p0, Lcom/konka/picturePlayer/PicInfo;->m_size:D

    invoke-virtual {p1, v0, v1}, Landroid/os/Parcel;->writeDouble(D)V

    return-void
.end method
