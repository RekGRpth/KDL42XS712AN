.class Lcom/konka/picturePlayer/picturePlayerActivity$2;
.super Landroid/os/Handler;
.source "picturePlayerActivity.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/konka/picturePlayer/picturePlayerActivity;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/konka/picturePlayer/picturePlayerActivity;


# direct methods
.method constructor <init>(Lcom/konka/picturePlayer/picturePlayerActivity;)V
    .locals 0

    iput-object p1, p0, Lcom/konka/picturePlayer/picturePlayerActivity$2;->this$0:Lcom/konka/picturePlayer/picturePlayerActivity;

    invoke-direct {p0}, Landroid/os/Handler;-><init>()V

    return-void
.end method

.method static synthetic access$0(Lcom/konka/picturePlayer/picturePlayerActivity$2;)Lcom/konka/picturePlayer/picturePlayerActivity;
    .locals 1

    iget-object v0, p0, Lcom/konka/picturePlayer/picturePlayerActivity$2;->this$0:Lcom/konka/picturePlayer/picturePlayerActivity;

    return-object v0
.end method


# virtual methods
.method public handleMessage(Landroid/os/Message;)V
    .locals 6
    .param p1    # Landroid/os/Message;

    const v3, 0x7f090090    # com.konka.mediaSharePlayer.R.string.picture_too_lage

    const v1, 0x7f09008f    # com.konka.mediaSharePlayer.R.string.down_loading_err

    const/4 v2, 0x0

    iget v0, p1, Landroid/os/Message;->what:I

    packed-switch v0, :pswitch_data_0

    :goto_0
    invoke-super {p0, p1}, Landroid/os/Handler;->handleMessage(Landroid/os/Message;)V

    return-void

    :pswitch_0
    iget-object v0, p0, Lcom/konka/picturePlayer/picturePlayerActivity$2;->this$0:Lcom/konka/picturePlayer/picturePlayerActivity;

    invoke-virtual {v0}, Lcom/konka/picturePlayer/picturePlayerActivity;->cancelProgressDlg()V

    iget-object v0, p0, Lcom/konka/picturePlayer/picturePlayerActivity$2;->this$0:Lcom/konka/picturePlayer/picturePlayerActivity;

    invoke-virtual {v0}, Lcom/konka/picturePlayer/picturePlayerActivity;->settextViewName()V

    iget-object v0, p0, Lcom/konka/picturePlayer/picturePlayerActivity$2;->this$0:Lcom/konka/picturePlayer/picturePlayerActivity;

    iget-object v1, p0, Lcom/konka/picturePlayer/picturePlayerActivity$2;->this$0:Lcom/konka/picturePlayer/picturePlayerActivity;

    iget-object v1, v1, Lcom/konka/picturePlayer/picturePlayerActivity;->picturePath:Ljava/lang/String;

    invoke-virtual {v0, v1}, Lcom/konka/picturePlayer/picturePlayerActivity;->bitmapPicturePlay(Ljava/lang/String;)V

    const-string v0, "MultiScreenPictureActivity"

    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "picturePath ==  "

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    iget-object v2, p0, Lcom/konka/picturePlayer/picturePlayerActivity$2;->this$0:Lcom/konka/picturePlayer/picturePlayerActivity;

    iget-object v2, v2, Lcom/konka/picturePlayer/picturePlayerActivity;->picturePath:Ljava/lang/String;

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    const-string v0, "MultiScreenPictureActivity"

    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "picFile ==  "

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    iget-object v2, p0, Lcom/konka/picturePlayer/picturePlayerActivity$2;->this$0:Lcom/konka/picturePlayer/picturePlayerActivity;

    iget-object v2, v2, Lcom/konka/picturePlayer/picturePlayerActivity;->picFile:Ljava/io/File;

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    iget-object v0, p0, Lcom/konka/picturePlayer/picturePlayerActivity$2;->this$0:Lcom/konka/picturePlayer/picturePlayerActivity;

    new-instance v1, Ljava/io/File;

    iget-object v2, p0, Lcom/konka/picturePlayer/picturePlayerActivity$2;->this$0:Lcom/konka/picturePlayer/picturePlayerActivity;

    iget-object v2, v2, Lcom/konka/picturePlayer/picturePlayerActivity;->picturePath:Ljava/lang/String;

    invoke-direct {v1, v2}, Ljava/io/File;-><init>(Ljava/lang/String;)V

    iput-object v1, v0, Lcom/konka/picturePlayer/picturePlayerActivity;->picFile:Ljava/io/File;

    iget-object v0, p0, Lcom/konka/picturePlayer/picturePlayerActivity$2;->this$0:Lcom/konka/picturePlayer/picturePlayerActivity;

    iget-object v0, v0, Lcom/konka/picturePlayer/picturePlayerActivity;->picFile:Ljava/io/File;

    invoke-virtual {v0}, Ljava/io/File;->exists()Z

    move-result v0

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/konka/picturePlayer/picturePlayerActivity$2;->this$0:Lcom/konka/picturePlayer/picturePlayerActivity;

    iget-object v0, v0, Lcom/konka/picturePlayer/picturePlayerActivity;->picFile:Ljava/io/File;

    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v1

    invoke-virtual {v0, v1, v2}, Ljava/io/File;->setLastModified(J)Z

    :cond_0
    iget-object v0, p0, Lcom/konka/picturePlayer/picturePlayerActivity$2;->this$0:Lcom/konka/picturePlayer/picturePlayerActivity;

    const/4 v1, 0x1

    iput-boolean v1, v0, Lcom/konka/picturePlayer/picturePlayerActivity;->isrun:Z

    iget-object v0, p0, Lcom/konka/picturePlayer/picturePlayerActivity$2;->this$0:Lcom/konka/picturePlayer/picturePlayerActivity;

    new-instance v1, Ljava/lang/Thread;

    new-instance v2, Lcom/konka/picturePlayer/picturePlayerActivity$2$1;

    invoke-direct {v2, p0}, Lcom/konka/picturePlayer/picturePlayerActivity$2$1;-><init>(Lcom/konka/picturePlayer/picturePlayerActivity$2;)V

    invoke-direct {v1, v2}, Ljava/lang/Thread;-><init>(Ljava/lang/Runnable;)V

    iput-object v1, v0, Lcom/konka/picturePlayer/picturePlayerActivity;->clockThread:Ljava/lang/Thread;

    iget-object v0, p0, Lcom/konka/picturePlayer/picturePlayerActivity$2;->this$0:Lcom/konka/picturePlayer/picturePlayerActivity;

    iget-object v0, v0, Lcom/konka/picturePlayer/picturePlayerActivity;->clockThread:Ljava/lang/Thread;

    invoke-virtual {v0}, Ljava/lang/Thread;->start()V

    goto/16 :goto_0

    :pswitch_1
    iget-object v0, p0, Lcom/konka/picturePlayer/picturePlayerActivity$2;->this$0:Lcom/konka/picturePlayer/picturePlayerActivity;

    # invokes: Lcom/konka/picturePlayer/picturePlayerActivity;->showProgressDialog(II)V
    invoke-static {v0, v1, v1}, Lcom/konka/picturePlayer/picturePlayerActivity;->access$4(Lcom/konka/picturePlayer/picturePlayerActivity;II)V

    iget-object v0, p0, Lcom/konka/picturePlayer/picturePlayerActivity$2;->this$0:Lcom/konka/picturePlayer/picturePlayerActivity;

    iget-object v1, p0, Lcom/konka/picturePlayer/picturePlayerActivity$2;->this$0:Lcom/konka/picturePlayer/picturePlayerActivity;

    new-instance v4, Ljava/lang/StringBuilder;

    iget-object v3, p0, Lcom/konka/picturePlayer/picturePlayerActivity$2;->this$0:Lcom/konka/picturePlayer/picturePlayerActivity;

    iget-object v3, v3, Lcom/konka/picturePlayer/picturePlayerActivity;->picPaths:Ljava/util/List;

    invoke-interface {v3, v2}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Ljava/lang/String;

    invoke-static {v3}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v3

    invoke-direct {v4, v3}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    const-string v3, "&mp"

    invoke-virtual {v4, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    move v3, v2

    move v4, v2

    invoke-virtual/range {v0 .. v5}, Lcom/konka/picturePlayer/picturePlayerActivity;->sendPlayerStateToPlayerService(Landroid/content/Context;IIILjava/lang/String;)V

    iget-object v0, p0, Lcom/konka/picturePlayer/picturePlayerActivity$2;->this$0:Lcom/konka/picturePlayer/picturePlayerActivity;

    iput-boolean v2, v0, Lcom/konka/picturePlayer/picturePlayerActivity;->isrun:Z

    goto/16 :goto_0

    :pswitch_2
    iget-object v0, p0, Lcom/konka/picturePlayer/picturePlayerActivity$2;->this$0:Lcom/konka/picturePlayer/picturePlayerActivity;

    # invokes: Lcom/konka/picturePlayer/picturePlayerActivity;->showProgressDialog(II)V
    invoke-static {v0, v3, v3}, Lcom/konka/picturePlayer/picturePlayerActivity;->access$4(Lcom/konka/picturePlayer/picturePlayerActivity;II)V

    iget-object v0, p0, Lcom/konka/picturePlayer/picturePlayerActivity$2;->this$0:Lcom/konka/picturePlayer/picturePlayerActivity;

    iget-object v1, p0, Lcom/konka/picturePlayer/picturePlayerActivity$2;->this$0:Lcom/konka/picturePlayer/picturePlayerActivity;

    new-instance v4, Ljava/lang/StringBuilder;

    iget-object v3, p0, Lcom/konka/picturePlayer/picturePlayerActivity$2;->this$0:Lcom/konka/picturePlayer/picturePlayerActivity;

    iget-object v3, v3, Lcom/konka/picturePlayer/picturePlayerActivity;->picPaths:Ljava/util/List;

    invoke-interface {v3, v2}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Ljava/lang/String;

    invoke-static {v3}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v3

    invoke-direct {v4, v3}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    const-string v3, "&mp"

    invoke-virtual {v4, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    move v3, v2

    move v4, v2

    invoke-virtual/range {v0 .. v5}, Lcom/konka/picturePlayer/picturePlayerActivity;->sendPlayerStateToPlayerService(Landroid/content/Context;IIILjava/lang/String;)V

    iget-object v0, p0, Lcom/konka/picturePlayer/picturePlayerActivity$2;->this$0:Lcom/konka/picturePlayer/picturePlayerActivity;

    iput-boolean v2, v0, Lcom/konka/picturePlayer/picturePlayerActivity;->isrun:Z

    goto/16 :goto_0

    nop

    :pswitch_data_0
    .packed-switch 0x0
        :pswitch_0
        :pswitch_1
        :pswitch_2
    .end packed-switch
.end method
