.class Lcom/konka/picturePlayer/picturePlayerActivity$DataReceiver;
.super Landroid/content/BroadcastReceiver;
.source "picturePlayerActivity.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/konka/picturePlayer/picturePlayerActivity;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x2
    name = "DataReceiver"
.end annotation


# instance fields
.field final synthetic this$0:Lcom/konka/picturePlayer/picturePlayerActivity;


# direct methods
.method private constructor <init>(Lcom/konka/picturePlayer/picturePlayerActivity;)V
    .locals 0

    iput-object p1, p0, Lcom/konka/picturePlayer/picturePlayerActivity$DataReceiver;->this$0:Lcom/konka/picturePlayer/picturePlayerActivity;

    invoke-direct {p0}, Landroid/content/BroadcastReceiver;-><init>()V

    return-void
.end method

.method synthetic constructor <init>(Lcom/konka/picturePlayer/picturePlayerActivity;Lcom/konka/picturePlayer/picturePlayerActivity$DataReceiver;)V
    .locals 0

    invoke-direct {p0, p1}, Lcom/konka/picturePlayer/picturePlayerActivity$DataReceiver;-><init>(Lcom/konka/picturePlayer/picturePlayerActivity;)V

    return-void
.end method


# virtual methods
.method public onReceive(Landroid/content/Context;Landroid/content/Intent;)V
    .locals 8
    .param p1    # Landroid/content/Context;
    .param p2    # Landroid/content/Intent;

    const/4 v6, 0x1

    const/4 v7, 0x0

    invoke-virtual {p2}, Landroid/content/Intent;->getExtras()Landroid/os/Bundle;

    move-result-object v0

    iget-object v3, p0, Lcom/konka/picturePlayer/picturePlayerActivity$DataReceiver;->this$0:Lcom/konka/picturePlayer/picturePlayerActivity;

    const-string v4, "mediastop"

    invoke-virtual {v0, v4, v6}, Landroid/os/Bundle;->getInt(Ljava/lang/String;I)I

    move-result v4

    iput v4, v3, Lcom/konka/picturePlayer/picturePlayerActivity;->mediastop:I

    const-string v3, "MultiScreenPictureActivity"

    new-instance v4, Ljava/lang/StringBuilder;

    const-string v5, "DataReceiver () mediastop = "

    invoke-direct {v4, v5}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    iget-object v5, p0, Lcom/konka/picturePlayer/picturePlayerActivity$DataReceiver;->this$0:Lcom/konka/picturePlayer/picturePlayerActivity;

    iget v5, v5, Lcom/konka/picturePlayer/picturePlayerActivity;->mediastop:I

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-static {v3, v4}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    iget-object v3, p0, Lcom/konka/picturePlayer/picturePlayerActivity$DataReceiver;->this$0:Lcom/konka/picturePlayer/picturePlayerActivity;

    const-string v4, "haveNextFlag"

    invoke-virtual {v0, v4, v7}, Landroid/os/Bundle;->getInt(Ljava/lang/String;I)I

    move-result v4

    invoke-static {v3, v4}, Lcom/konka/picturePlayer/picturePlayerActivity;->access$7(Lcom/konka/picturePlayer/picturePlayerActivity;I)V

    iget-object v3, p0, Lcom/konka/picturePlayer/picturePlayerActivity$DataReceiver;->this$0:Lcom/konka/picturePlayer/picturePlayerActivity;

    const-string v4, "mediastop"

    invoke-virtual {v0, v4, v6}, Landroid/os/Bundle;->getInt(Ljava/lang/String;I)I

    move-result v4

    iput v4, v3, Lcom/konka/picturePlayer/picturePlayerActivity;->mediastop:I

    iget-object v3, p0, Lcom/konka/picturePlayer/picturePlayerActivity$DataReceiver;->this$0:Lcom/konka/picturePlayer/picturePlayerActivity;

    const-string v4, "mobileIp"

    invoke-virtual {v0, v4}, Landroid/os/Bundle;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v4

    invoke-static {v3, v4}, Lcom/konka/picturePlayer/picturePlayerActivity;->access$8(Lcom/konka/picturePlayer/picturePlayerActivity;Ljava/lang/String;)V

    const-string v3, "MultiScreenPictureActivity"

    new-instance v4, Ljava/lang/StringBuilder;

    const-string v5, "DataReceiver () haveNextFlag = "

    invoke-direct {v4, v5}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    iget-object v5, p0, Lcom/konka/picturePlayer/picturePlayerActivity$DataReceiver;->this$0:Lcom/konka/picturePlayer/picturePlayerActivity;

    # getter for: Lcom/konka/picturePlayer/picturePlayerActivity;->haveNextFlag:I
    invoke-static {v5}, Lcom/konka/picturePlayer/picturePlayerActivity;->access$1(Lcom/konka/picturePlayer/picturePlayerActivity;)I

    move-result v5

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-static {v3, v4}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    iget-object v3, p0, Lcom/konka/picturePlayer/picturePlayerActivity$DataReceiver;->this$0:Lcom/konka/picturePlayer/picturePlayerActivity;

    const-string v4, "filesize"

    const-wide/16 v5, 0x0

    invoke-virtual {v0, v4, v5, v6}, Landroid/os/Bundle;->getLong(Ljava/lang/String;J)J

    move-result-wide v4

    iput-wide v4, v3, Lcom/konka/picturePlayer/picturePlayerActivity;->filesize:J

    const-string v3, "MultiScreenPictureActivity"

    new-instance v4, Ljava/lang/StringBuilder;

    const-string v5, "DataReceiver () filesize = "

    invoke-direct {v4, v5}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    iget-object v5, p0, Lcom/konka/picturePlayer/picturePlayerActivity$DataReceiver;->this$0:Lcom/konka/picturePlayer/picturePlayerActivity;

    iget-wide v5, v5, Lcom/konka/picturePlayer/picturePlayerActivity;->filesize:J

    invoke-virtual {v4, v5, v6}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-static {v3, v4}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    iget-object v3, p0, Lcom/konka/picturePlayer/picturePlayerActivity$DataReceiver;->this$0:Lcom/konka/picturePlayer/picturePlayerActivity;

    new-instance v4, Ljava/util/ArrayList;

    invoke-direct {v4}, Ljava/util/ArrayList;-><init>()V

    iput-object v4, v3, Lcom/konka/picturePlayer/picturePlayerActivity;->picPaths:Ljava/util/List;

    iget-object v3, p0, Lcom/konka/picturePlayer/picturePlayerActivity$DataReceiver;->this$0:Lcom/konka/picturePlayer/picturePlayerActivity;

    const-string v4, "picPaths"

    invoke-virtual {v0, v4}, Landroid/os/Bundle;->getStringArrayList(Ljava/lang/String;)Ljava/util/ArrayList;

    move-result-object v4

    iput-object v4, v3, Lcom/konka/picturePlayer/picturePlayerActivity;->picPaths:Ljava/util/List;

    iget-object v3, p0, Lcom/konka/picturePlayer/picturePlayerActivity$DataReceiver;->this$0:Lcom/konka/picturePlayer/picturePlayerActivity;

    iget-object v3, v3, Lcom/konka/picturePlayer/picturePlayerActivity;->picPaths:Ljava/util/List;

    invoke-interface {v3, v7}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Ljava/lang/String;

    const-string v3, "/"

    invoke-virtual {v2, v3}, Ljava/lang/String;->lastIndexOf(Ljava/lang/String;)I

    move-result v3

    add-int/lit8 v3, v3, 0x1

    invoke-virtual {v2}, Ljava/lang/String;->length()I

    move-result v4

    invoke-virtual {v2, v3, v4}, Ljava/lang/String;->substring(II)Ljava/lang/String;

    move-result-object v1

    iget-object v3, p0, Lcom/konka/picturePlayer/picturePlayerActivity$DataReceiver;->this$0:Lcom/konka/picturePlayer/picturePlayerActivity;

    # getter for: Lcom/konka/picturePlayer/picturePlayerActivity;->nextFlag:I
    invoke-static {v3}, Lcom/konka/picturePlayer/picturePlayerActivity;->access$2(Lcom/konka/picturePlayer/picturePlayerActivity;)I

    move-result v4

    iget-object v5, p0, Lcom/konka/picturePlayer/picturePlayerActivity$DataReceiver;->this$0:Lcom/konka/picturePlayer/picturePlayerActivity;

    # getter for: Lcom/konka/picturePlayer/picturePlayerActivity;->haveNextFlag:I
    invoke-static {v5}, Lcom/konka/picturePlayer/picturePlayerActivity;->access$1(Lcom/konka/picturePlayer/picturePlayerActivity;)I

    move-result v5

    add-int/2addr v4, v5

    invoke-static {v3, v4}, Lcom/konka/picturePlayer/picturePlayerActivity;->access$3(Lcom/konka/picturePlayer/picturePlayerActivity;I)V

    iget-object v3, p0, Lcom/konka/picturePlayer/picturePlayerActivity$DataReceiver;->this$0:Lcom/konka/picturePlayer/picturePlayerActivity;

    invoke-virtual {v3, v1}, Lcom/konka/picturePlayer/picturePlayerActivity;->playImage(Ljava/lang/String;)V

    return-void
.end method
