.class public Lcom/konka/picturePlayer/PicShowHolder$ItemOnClickEvent;
.super Ljava/lang/Object;
.source "PicShowHolder.java"

# interfaces
.implements Landroid/view/View$OnClickListener;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/konka/picturePlayer/PicShowHolder;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x1
    name = "ItemOnClickEvent"
.end annotation


# instance fields
.field final synthetic this$0:Lcom/konka/picturePlayer/PicShowHolder;


# direct methods
.method public constructor <init>(Lcom/konka/picturePlayer/PicShowHolder;)V
    .locals 0

    iput-object p1, p0, Lcom/konka/picturePlayer/PicShowHolder$ItemOnClickEvent;->this$0:Lcom/konka/picturePlayer/PicShowHolder;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onClick(Landroid/view/View;)V
    .locals 3
    .param p1    # Landroid/view/View;

    const/4 v2, 0x1

    invoke-virtual {p1}, Landroid/view/View;->getId()I

    move-result v0

    packed-switch v0, :pswitch_data_0

    :goto_0
    :pswitch_0
    return-void

    :pswitch_1
    iget-object v0, p0, Lcom/konka/picturePlayer/PicShowHolder$ItemOnClickEvent;->this$0:Lcom/konka/picturePlayer/PicShowHolder;

    # getter for: Lcom/konka/picturePlayer/PicShowHolder;->mPicShowActivity:Lcom/konka/picturePlayer/picturePlayerActivity;
    invoke-static {v0}, Lcom/konka/picturePlayer/PicShowHolder;->access$1(Lcom/konka/picturePlayer/PicShowHolder;)Lcom/konka/picturePlayer/picturePlayerActivity;

    move-result-object v0

    const/4 v1, 0x6

    invoke-virtual {v0, v1}, Lcom/konka/picturePlayer/picturePlayerActivity;->setState(I)V

    iget-object v0, p0, Lcom/konka/picturePlayer/PicShowHolder$ItemOnClickEvent;->this$0:Lcom/konka/picturePlayer/PicShowHolder;

    # getter for: Lcom/konka/picturePlayer/PicShowHolder;->mPicShowActivity:Lcom/konka/picturePlayer/picturePlayerActivity;
    invoke-static {v0}, Lcom/konka/picturePlayer/PicShowHolder;->access$1(Lcom/konka/picturePlayer/PicShowHolder;)Lcom/konka/picturePlayer/picturePlayerActivity;

    move-result-object v0

    invoke-virtual {v0}, Lcom/konka/picturePlayer/picturePlayerActivity;->registerListeners()V

    sget-object v0, Lcom/konka/picturePlayer/PicShowHolder;->bt_leftRotion:Landroid/widget/Button;

    invoke-virtual {v0, v2}, Landroid/widget/Button;->setFocusable(Z)V

    sget-object v0, Lcom/konka/picturePlayer/PicShowHolder;->bt_leftRotion:Landroid/widget/Button;

    invoke-virtual {v0}, Landroid/widget/Button;->requestFocus()Z

    iget-object v0, p0, Lcom/konka/picturePlayer/PicShowHolder$ItemOnClickEvent;->this$0:Lcom/konka/picturePlayer/PicShowHolder;

    const v1, 0x7f0b0014    # com.konka.mediaSharePlayer.R.id.buttomitem6

    # invokes: Lcom/konka/picturePlayer/PicShowHolder;->setTextViewVisible(I)V
    invoke-static {v0, v1}, Lcom/konka/picturePlayer/PicShowHolder;->access$2(Lcom/konka/picturePlayer/PicShowHolder;I)V

    goto :goto_0

    :pswitch_2
    iget-object v0, p0, Lcom/konka/picturePlayer/PicShowHolder$ItemOnClickEvent;->this$0:Lcom/konka/picturePlayer/PicShowHolder;

    # getter for: Lcom/konka/picturePlayer/PicShowHolder;->mPicShowActivity:Lcom/konka/picturePlayer/picturePlayerActivity;
    invoke-static {v0}, Lcom/konka/picturePlayer/PicShowHolder;->access$1(Lcom/konka/picturePlayer/PicShowHolder;)Lcom/konka/picturePlayer/picturePlayerActivity;

    move-result-object v0

    const/4 v1, 0x7

    invoke-virtual {v0, v1}, Lcom/konka/picturePlayer/picturePlayerActivity;->setState(I)V

    iget-object v0, p0, Lcom/konka/picturePlayer/PicShowHolder$ItemOnClickEvent;->this$0:Lcom/konka/picturePlayer/PicShowHolder;

    # getter for: Lcom/konka/picturePlayer/PicShowHolder;->mPicShowActivity:Lcom/konka/picturePlayer/picturePlayerActivity;
    invoke-static {v0}, Lcom/konka/picturePlayer/PicShowHolder;->access$1(Lcom/konka/picturePlayer/PicShowHolder;)Lcom/konka/picturePlayer/picturePlayerActivity;

    move-result-object v0

    invoke-virtual {v0}, Lcom/konka/picturePlayer/picturePlayerActivity;->registerListeners()V

    sget-object v0, Lcom/konka/picturePlayer/PicShowHolder;->bt_rightRotion:Landroid/widget/Button;

    invoke-virtual {v0, v2}, Landroid/widget/Button;->setFocusable(Z)V

    sget-object v0, Lcom/konka/picturePlayer/PicShowHolder;->bt_rightRotion:Landroid/widget/Button;

    invoke-virtual {v0}, Landroid/widget/Button;->requestFocus()Z

    iget-object v0, p0, Lcom/konka/picturePlayer/PicShowHolder$ItemOnClickEvent;->this$0:Lcom/konka/picturePlayer/PicShowHolder;

    const v1, 0x7f0b0016    # com.konka.mediaSharePlayer.R.id.buttomitem7

    # invokes: Lcom/konka/picturePlayer/PicShowHolder;->setTextViewVisible(I)V
    invoke-static {v0, v1}, Lcom/konka/picturePlayer/PicShowHolder;->access$2(Lcom/konka/picturePlayer/PicShowHolder;I)V

    goto :goto_0

    :pswitch_3
    iget-object v0, p0, Lcom/konka/picturePlayer/PicShowHolder$ItemOnClickEvent;->this$0:Lcom/konka/picturePlayer/PicShowHolder;

    # getter for: Lcom/konka/picturePlayer/PicShowHolder;->mPicShowActivity:Lcom/konka/picturePlayer/picturePlayerActivity;
    invoke-static {v0}, Lcom/konka/picturePlayer/PicShowHolder;->access$1(Lcom/konka/picturePlayer/PicShowHolder;)Lcom/konka/picturePlayer/picturePlayerActivity;

    move-result-object v0

    const/16 v1, 0x8

    invoke-virtual {v0, v1}, Lcom/konka/picturePlayer/picturePlayerActivity;->setState(I)V

    iget-object v0, p0, Lcom/konka/picturePlayer/PicShowHolder$ItemOnClickEvent;->this$0:Lcom/konka/picturePlayer/PicShowHolder;

    # getter for: Lcom/konka/picturePlayer/PicShowHolder;->mPicShowActivity:Lcom/konka/picturePlayer/picturePlayerActivity;
    invoke-static {v0}, Lcom/konka/picturePlayer/PicShowHolder;->access$1(Lcom/konka/picturePlayer/PicShowHolder;)Lcom/konka/picturePlayer/picturePlayerActivity;

    move-result-object v0

    invoke-virtual {v0}, Lcom/konka/picturePlayer/picturePlayerActivity;->registerListeners()V

    sget-object v0, Lcom/konka/picturePlayer/PicShowHolder;->bt_big:Landroid/widget/Button;

    invoke-virtual {v0, v2}, Landroid/widget/Button;->setFocusable(Z)V

    sget-object v0, Lcom/konka/picturePlayer/PicShowHolder;->bt_big:Landroid/widget/Button;

    invoke-virtual {v0}, Landroid/widget/Button;->requestFocus()Z

    iget-object v0, p0, Lcom/konka/picturePlayer/PicShowHolder$ItemOnClickEvent;->this$0:Lcom/konka/picturePlayer/PicShowHolder;

    const v1, 0x7f0b001a    # com.konka.mediaSharePlayer.R.id.buttomitem9

    # invokes: Lcom/konka/picturePlayer/PicShowHolder;->setTextViewVisible(I)V
    invoke-static {v0, v1}, Lcom/konka/picturePlayer/PicShowHolder;->access$2(Lcom/konka/picturePlayer/PicShowHolder;I)V

    goto :goto_0

    :pswitch_4
    iget-object v0, p0, Lcom/konka/picturePlayer/PicShowHolder$ItemOnClickEvent;->this$0:Lcom/konka/picturePlayer/PicShowHolder;

    # getter for: Lcom/konka/picturePlayer/PicShowHolder;->mPicShowActivity:Lcom/konka/picturePlayer/picturePlayerActivity;
    invoke-static {v0}, Lcom/konka/picturePlayer/PicShowHolder;->access$1(Lcom/konka/picturePlayer/PicShowHolder;)Lcom/konka/picturePlayer/picturePlayerActivity;

    move-result-object v0

    const/16 v1, 0x9

    invoke-virtual {v0, v1}, Lcom/konka/picturePlayer/picturePlayerActivity;->setState(I)V

    iget-object v0, p0, Lcom/konka/picturePlayer/PicShowHolder$ItemOnClickEvent;->this$0:Lcom/konka/picturePlayer/PicShowHolder;

    # getter for: Lcom/konka/picturePlayer/PicShowHolder;->mPicShowActivity:Lcom/konka/picturePlayer/picturePlayerActivity;
    invoke-static {v0}, Lcom/konka/picturePlayer/PicShowHolder;->access$1(Lcom/konka/picturePlayer/PicShowHolder;)Lcom/konka/picturePlayer/picturePlayerActivity;

    move-result-object v0

    invoke-virtual {v0}, Lcom/konka/picturePlayer/picturePlayerActivity;->registerListeners()V

    sget-object v0, Lcom/konka/picturePlayer/PicShowHolder;->bt_small:Landroid/widget/Button;

    invoke-virtual {v0, v2}, Landroid/widget/Button;->setFocusable(Z)V

    sget-object v0, Lcom/konka/picturePlayer/PicShowHolder;->bt_small:Landroid/widget/Button;

    invoke-virtual {v0}, Landroid/widget/Button;->requestFocus()Z

    iget-object v0, p0, Lcom/konka/picturePlayer/PicShowHolder$ItemOnClickEvent;->this$0:Lcom/konka/picturePlayer/PicShowHolder;

    const v1, 0x7f0b0018    # com.konka.mediaSharePlayer.R.id.buttomitem8

    # invokes: Lcom/konka/picturePlayer/PicShowHolder;->setTextViewVisible(I)V
    invoke-static {v0, v1}, Lcom/konka/picturePlayer/PicShowHolder;->access$2(Lcom/konka/picturePlayer/PicShowHolder;I)V

    goto/16 :goto_0

    :pswitch_data_0
    .packed-switch 0x7f0b0014
        :pswitch_1    # com.konka.mediaSharePlayer.R.id.buttomitem6
        :pswitch_0    # com.konka.mediaSharePlayer.R.id.textview_item6
        :pswitch_2    # com.konka.mediaSharePlayer.R.id.buttomitem7
        :pswitch_0    # com.konka.mediaSharePlayer.R.id.textview_item7
        :pswitch_4    # com.konka.mediaSharePlayer.R.id.buttomitem8
        :pswitch_0    # com.konka.mediaSharePlayer.R.id.textview_item8
        :pswitch_3    # com.konka.mediaSharePlayer.R.id.buttomitem9
    .end packed-switch
.end method
