.class Lcom/konka/picturePlayer/picturePlayerActivity$6;
.super Ljava/lang/Object;
.source "picturePlayerActivity.java"

# interfaces
.implements Landroid/view/View$OnKeyListener;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/konka/picturePlayer/picturePlayerActivity;->InitData()V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/konka/picturePlayer/picturePlayerActivity;


# direct methods
.method constructor <init>(Lcom/konka/picturePlayer/picturePlayerActivity;)V
    .locals 0

    iput-object p1, p0, Lcom/konka/picturePlayer/picturePlayerActivity$6;->this$0:Lcom/konka/picturePlayer/picturePlayerActivity;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onKey(Landroid/view/View;ILandroid/view/KeyEvent;)Z
    .locals 5
    .param p1    # Landroid/view/View;
    .param p2    # I
    .param p3    # Landroid/view/KeyEvent;

    const/high16 v4, 0x41f00000    # 30.0f

    const/high16 v3, -0x3e100000    # -30.0f

    const/4 v2, 0x0

    sget-boolean v0, Lcom/konka/picturePlayer/picturePlayerActivity;->isCanMove:Z

    if-eqz v0, :cond_3

    invoke-virtual {p3}, Landroid/view/KeyEvent;->getAction()I

    move-result v0

    if-nez v0, :cond_0

    invoke-virtual {p3}, Landroid/view/KeyEvent;->getKeyCode()I

    move-result v0

    const/16 v1, 0x15

    if-ne v0, v1, :cond_0

    iget-object v0, p0, Lcom/konka/picturePlayer/picturePlayerActivity$6;->this$0:Lcom/konka/picturePlayer/picturePlayerActivity;

    # getter for: Lcom/konka/picturePlayer/picturePlayerActivity;->picImageview:Lcom/konka/picturePlayer/ImageViewTouch;
    invoke-static {v0}, Lcom/konka/picturePlayer/picturePlayerActivity;->access$5(Lcom/konka/picturePlayer/picturePlayerActivity;)Lcom/konka/picturePlayer/ImageViewTouch;

    move-result-object v0

    iget-object v0, v0, Lcom/konka/picturePlayer/ImageViewTouch;->mDisplayMatrix:Landroid/graphics/Matrix;

    invoke-virtual {v0, v3, v2}, Landroid/graphics/Matrix;->postTranslate(FF)Z

    iget-object v0, p0, Lcom/konka/picturePlayer/picturePlayerActivity$6;->this$0:Lcom/konka/picturePlayer/picturePlayerActivity;

    # getter for: Lcom/konka/picturePlayer/picturePlayerActivity;->picImageview:Lcom/konka/picturePlayer/ImageViewTouch;
    invoke-static {v0}, Lcom/konka/picturePlayer/picturePlayerActivity;->access$5(Lcom/konka/picturePlayer/picturePlayerActivity;)Lcom/konka/picturePlayer/ImageViewTouch;

    move-result-object v0

    iget-object v1, p0, Lcom/konka/picturePlayer/picturePlayerActivity$6;->this$0:Lcom/konka/picturePlayer/picturePlayerActivity;

    # getter for: Lcom/konka/picturePlayer/picturePlayerActivity;->picImageview:Lcom/konka/picturePlayer/ImageViewTouch;
    invoke-static {v1}, Lcom/konka/picturePlayer/picturePlayerActivity;->access$5(Lcom/konka/picturePlayer/picturePlayerActivity;)Lcom/konka/picturePlayer/ImageViewTouch;

    move-result-object v1

    iget-object v1, v1, Lcom/konka/picturePlayer/ImageViewTouch;->mDisplayMatrix:Landroid/graphics/Matrix;

    invoke-virtual {v0, v1}, Lcom/konka/picturePlayer/ImageViewTouch;->setImageMatrix(Landroid/graphics/Matrix;)V

    :cond_0
    invoke-virtual {p3}, Landroid/view/KeyEvent;->getAction()I

    move-result v0

    if-nez v0, :cond_1

    invoke-virtual {p3}, Landroid/view/KeyEvent;->getKeyCode()I

    move-result v0

    const/16 v1, 0x16

    if-ne v0, v1, :cond_1

    iget-object v0, p0, Lcom/konka/picturePlayer/picturePlayerActivity$6;->this$0:Lcom/konka/picturePlayer/picturePlayerActivity;

    # getter for: Lcom/konka/picturePlayer/picturePlayerActivity;->picImageview:Lcom/konka/picturePlayer/ImageViewTouch;
    invoke-static {v0}, Lcom/konka/picturePlayer/picturePlayerActivity;->access$5(Lcom/konka/picturePlayer/picturePlayerActivity;)Lcom/konka/picturePlayer/ImageViewTouch;

    move-result-object v0

    iget-object v0, v0, Lcom/konka/picturePlayer/ImageViewTouch;->mDisplayMatrix:Landroid/graphics/Matrix;

    invoke-virtual {v0, v4, v2}, Landroid/graphics/Matrix;->postTranslate(FF)Z

    iget-object v0, p0, Lcom/konka/picturePlayer/picturePlayerActivity$6;->this$0:Lcom/konka/picturePlayer/picturePlayerActivity;

    # getter for: Lcom/konka/picturePlayer/picturePlayerActivity;->picImageview:Lcom/konka/picturePlayer/ImageViewTouch;
    invoke-static {v0}, Lcom/konka/picturePlayer/picturePlayerActivity;->access$5(Lcom/konka/picturePlayer/picturePlayerActivity;)Lcom/konka/picturePlayer/ImageViewTouch;

    move-result-object v0

    iget-object v1, p0, Lcom/konka/picturePlayer/picturePlayerActivity$6;->this$0:Lcom/konka/picturePlayer/picturePlayerActivity;

    # getter for: Lcom/konka/picturePlayer/picturePlayerActivity;->picImageview:Lcom/konka/picturePlayer/ImageViewTouch;
    invoke-static {v1}, Lcom/konka/picturePlayer/picturePlayerActivity;->access$5(Lcom/konka/picturePlayer/picturePlayerActivity;)Lcom/konka/picturePlayer/ImageViewTouch;

    move-result-object v1

    iget-object v1, v1, Lcom/konka/picturePlayer/ImageViewTouch;->mDisplayMatrix:Landroid/graphics/Matrix;

    invoke-virtual {v0, v1}, Lcom/konka/picturePlayer/ImageViewTouch;->setImageMatrix(Landroid/graphics/Matrix;)V

    :cond_1
    invoke-virtual {p3}, Landroid/view/KeyEvent;->getAction()I

    move-result v0

    if-nez v0, :cond_2

    invoke-virtual {p3}, Landroid/view/KeyEvent;->getKeyCode()I

    move-result v0

    const/16 v1, 0x13

    if-ne v0, v1, :cond_2

    iget-object v0, p0, Lcom/konka/picturePlayer/picturePlayerActivity$6;->this$0:Lcom/konka/picturePlayer/picturePlayerActivity;

    # getter for: Lcom/konka/picturePlayer/picturePlayerActivity;->picImageview:Lcom/konka/picturePlayer/ImageViewTouch;
    invoke-static {v0}, Lcom/konka/picturePlayer/picturePlayerActivity;->access$5(Lcom/konka/picturePlayer/picturePlayerActivity;)Lcom/konka/picturePlayer/ImageViewTouch;

    move-result-object v0

    iget-object v0, v0, Lcom/konka/picturePlayer/ImageViewTouch;->mDisplayMatrix:Landroid/graphics/Matrix;

    invoke-virtual {v0, v2, v3}, Landroid/graphics/Matrix;->postTranslate(FF)Z

    iget-object v0, p0, Lcom/konka/picturePlayer/picturePlayerActivity$6;->this$0:Lcom/konka/picturePlayer/picturePlayerActivity;

    # getter for: Lcom/konka/picturePlayer/picturePlayerActivity;->picImageview:Lcom/konka/picturePlayer/ImageViewTouch;
    invoke-static {v0}, Lcom/konka/picturePlayer/picturePlayerActivity;->access$5(Lcom/konka/picturePlayer/picturePlayerActivity;)Lcom/konka/picturePlayer/ImageViewTouch;

    move-result-object v0

    iget-object v1, p0, Lcom/konka/picturePlayer/picturePlayerActivity$6;->this$0:Lcom/konka/picturePlayer/picturePlayerActivity;

    # getter for: Lcom/konka/picturePlayer/picturePlayerActivity;->picImageview:Lcom/konka/picturePlayer/ImageViewTouch;
    invoke-static {v1}, Lcom/konka/picturePlayer/picturePlayerActivity;->access$5(Lcom/konka/picturePlayer/picturePlayerActivity;)Lcom/konka/picturePlayer/ImageViewTouch;

    move-result-object v1

    iget-object v1, v1, Lcom/konka/picturePlayer/ImageViewTouch;->mDisplayMatrix:Landroid/graphics/Matrix;

    invoke-virtual {v0, v1}, Lcom/konka/picturePlayer/ImageViewTouch;->setImageMatrix(Landroid/graphics/Matrix;)V

    :cond_2
    invoke-virtual {p3}, Landroid/view/KeyEvent;->getAction()I

    move-result v0

    if-nez v0, :cond_3

    invoke-virtual {p3}, Landroid/view/KeyEvent;->getKeyCode()I

    move-result v0

    const/16 v1, 0x14

    if-ne v0, v1, :cond_3

    iget-object v0, p0, Lcom/konka/picturePlayer/picturePlayerActivity$6;->this$0:Lcom/konka/picturePlayer/picturePlayerActivity;

    # getter for: Lcom/konka/picturePlayer/picturePlayerActivity;->picImageview:Lcom/konka/picturePlayer/ImageViewTouch;
    invoke-static {v0}, Lcom/konka/picturePlayer/picturePlayerActivity;->access$5(Lcom/konka/picturePlayer/picturePlayerActivity;)Lcom/konka/picturePlayer/ImageViewTouch;

    move-result-object v0

    iget-object v0, v0, Lcom/konka/picturePlayer/ImageViewTouch;->mDisplayMatrix:Landroid/graphics/Matrix;

    invoke-virtual {v0, v2, v4}, Landroid/graphics/Matrix;->postTranslate(FF)Z

    iget-object v0, p0, Lcom/konka/picturePlayer/picturePlayerActivity$6;->this$0:Lcom/konka/picturePlayer/picturePlayerActivity;

    # getter for: Lcom/konka/picturePlayer/picturePlayerActivity;->picImageview:Lcom/konka/picturePlayer/ImageViewTouch;
    invoke-static {v0}, Lcom/konka/picturePlayer/picturePlayerActivity;->access$5(Lcom/konka/picturePlayer/picturePlayerActivity;)Lcom/konka/picturePlayer/ImageViewTouch;

    move-result-object v0

    iget-object v1, p0, Lcom/konka/picturePlayer/picturePlayerActivity$6;->this$0:Lcom/konka/picturePlayer/picturePlayerActivity;

    # getter for: Lcom/konka/picturePlayer/picturePlayerActivity;->picImageview:Lcom/konka/picturePlayer/ImageViewTouch;
    invoke-static {v1}, Lcom/konka/picturePlayer/picturePlayerActivity;->access$5(Lcom/konka/picturePlayer/picturePlayerActivity;)Lcom/konka/picturePlayer/ImageViewTouch;

    move-result-object v1

    iget-object v1, v1, Lcom/konka/picturePlayer/ImageViewTouch;->mDisplayMatrix:Landroid/graphics/Matrix;

    invoke-virtual {v0, v1}, Lcom/konka/picturePlayer/ImageViewTouch;->setImageMatrix(Landroid/graphics/Matrix;)V

    :cond_3
    const/4 v0, 0x0

    return v0
.end method
