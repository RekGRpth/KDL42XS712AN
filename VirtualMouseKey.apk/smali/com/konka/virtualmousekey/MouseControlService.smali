.class public Lcom/konka/virtualmousekey/MouseControlService;
.super Landroid/app/Service;
.source "MouseControlService.java"


# static fields
.field private static final AUTO_MODE:Ljava/lang/String; = "auto"

.field private static final START_TYPE:Ljava/lang/String; = "start"

.field private static final STOP_TYPE:Ljava/lang/String; = "stop"


# instance fields
.field private final TAG:Ljava/lang/String;

.field private handler:Landroid/os/Handler;

.field private lastdowntime1:J

.field private lastdowntime2:J

.field private mMouseControl:Lcom/konka/virtualmousekey/MouseControl;

.field private stepLength:I


# direct methods
.method public constructor <init>()V
    .locals 3

    const-wide/16 v1, 0x0

    invoke-direct {p0}, Landroid/app/Service;-><init>()V

    const-class v0, Lcom/konka/virtualmousekey/MouseControlService;

    invoke-virtual {v0}, Ljava/lang/Class;->getSimpleName()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/konka/virtualmousekey/MouseControlService;->TAG:Ljava/lang/String;

    iput-wide v1, p0, Lcom/konka/virtualmousekey/MouseControlService;->lastdowntime1:J

    iput-wide v1, p0, Lcom/konka/virtualmousekey/MouseControlService;->lastdowntime2:J

    const/16 v0, 0xa

    iput v0, p0, Lcom/konka/virtualmousekey/MouseControlService;->stepLength:I

    return-void
.end method

.method private handleVirtualMouseKey(IIJ)Z
    .locals 11
    .param p1    # I
    .param p2    # I
    .param p3    # J

    const/4 v4, 0x0

    iget-object v5, p0, Lcom/konka/virtualmousekey/MouseControlService;->TAG:Ljava/lang/String;

    new-instance v6, Ljava/lang/StringBuilder;

    const-string v7, "Fetch key code "

    invoke-direct {v6, v7}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v6, p1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v6}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v6

    invoke-static {v5, v6}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    const/16 v5, 0x13

    if-lt p1, v5, :cond_0

    const/16 v5, 0x16

    if-le p1, v5, :cond_1

    :cond_0
    const/16 v5, 0x42

    if-ne p1, v5, :cond_4

    :cond_1
    if-nez p2, :cond_3

    move-wide v0, p3

    iget-wide v5, p0, Lcom/konka/virtualmousekey/MouseControlService;->lastdowntime2:J

    sub-long v5, v0, v5

    iget-wide v7, p0, Lcom/konka/virtualmousekey/MouseControlService;->lastdowntime2:J

    iget-wide v9, p0, Lcom/konka/virtualmousekey/MouseControlService;->lastdowntime1:J

    sub-long/2addr v7, v9

    add-long/2addr v5, v7

    const-wide/16 v7, 0x2

    div-long v2, v5, v7

    const-wide/16 v5, 0x5dc

    cmp-long v5, v2, v5

    if-lez v5, :cond_5

    const/16 v5, 0xa

    iput v5, p0, Lcom/konka/virtualmousekey/MouseControlService;->stepLength:I

    :cond_2
    :goto_0
    iget-object v5, p0, Lcom/konka/virtualmousekey/MouseControlService;->TAG:Ljava/lang/String;

    new-instance v6, Ljava/lang/StringBuilder;

    const-string v7, "stepLength = "

    invoke-direct {v6, v7}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    iget v7, p0, Lcom/konka/virtualmousekey/MouseControlService;->stepLength:I

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v6}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v6

    invoke-static {v5, v6}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    sparse-switch p1, :sswitch_data_0

    :cond_3
    :goto_1
    const/4 v4, 0x1

    :cond_4
    return v4

    :cond_5
    const-wide/16 v5, 0x0

    cmp-long v5, v2, v5

    if-lez v5, :cond_2

    const/high16 v5, 0x3f800000    # 1.0f

    long-to-float v6, v2

    const/high16 v7, 0x447a0000    # 1000.0f

    div-float/2addr v6, v7

    div-float/2addr v5, v6

    const/high16 v6, 0x41700000    # 15.0f

    mul-float/2addr v5, v6

    float-to-int v5, v5

    iput v5, p0, Lcom/konka/virtualmousekey/MouseControlService;->stepLength:I

    goto :goto_0

    :sswitch_0
    iget-object v5, p0, Lcom/konka/virtualmousekey/MouseControlService;->mMouseControl:Lcom/konka/virtualmousekey/MouseControl;

    iget v6, p0, Lcom/konka/virtualmousekey/MouseControlService;->stepLength:I

    rsub-int/lit8 v6, v6, 0x0

    invoke-virtual {v5, v4, v6}, Lcom/konka/virtualmousekey/MouseControl;->moveCursor(II)V

    iget-wide v4, p0, Lcom/konka/virtualmousekey/MouseControlService;->lastdowntime2:J

    iput-wide v4, p0, Lcom/konka/virtualmousekey/MouseControlService;->lastdowntime1:J

    iput-wide v0, p0, Lcom/konka/virtualmousekey/MouseControlService;->lastdowntime2:J

    goto :goto_1

    :sswitch_1
    iget-object v5, p0, Lcom/konka/virtualmousekey/MouseControlService;->mMouseControl:Lcom/konka/virtualmousekey/MouseControl;

    iget v6, p0, Lcom/konka/virtualmousekey/MouseControlService;->stepLength:I

    invoke-virtual {v5, v4, v6}, Lcom/konka/virtualmousekey/MouseControl;->moveCursor(II)V

    iget-wide v4, p0, Lcom/konka/virtualmousekey/MouseControlService;->lastdowntime2:J

    iput-wide v4, p0, Lcom/konka/virtualmousekey/MouseControlService;->lastdowntime1:J

    iput-wide v0, p0, Lcom/konka/virtualmousekey/MouseControlService;->lastdowntime2:J

    goto :goto_1

    :sswitch_2
    iget-object v5, p0, Lcom/konka/virtualmousekey/MouseControlService;->mMouseControl:Lcom/konka/virtualmousekey/MouseControl;

    iget v6, p0, Lcom/konka/virtualmousekey/MouseControlService;->stepLength:I

    rsub-int/lit8 v6, v6, 0x0

    invoke-virtual {v5, v6, v4}, Lcom/konka/virtualmousekey/MouseControl;->moveCursor(II)V

    iget-wide v4, p0, Lcom/konka/virtualmousekey/MouseControlService;->lastdowntime2:J

    iput-wide v4, p0, Lcom/konka/virtualmousekey/MouseControlService;->lastdowntime1:J

    iput-wide v0, p0, Lcom/konka/virtualmousekey/MouseControlService;->lastdowntime2:J

    goto :goto_1

    :sswitch_3
    iget-object v5, p0, Lcom/konka/virtualmousekey/MouseControlService;->mMouseControl:Lcom/konka/virtualmousekey/MouseControl;

    iget v6, p0, Lcom/konka/virtualmousekey/MouseControlService;->stepLength:I

    invoke-virtual {v5, v6, v4}, Lcom/konka/virtualmousekey/MouseControl;->moveCursor(II)V

    iget-wide v4, p0, Lcom/konka/virtualmousekey/MouseControlService;->lastdowntime2:J

    iput-wide v4, p0, Lcom/konka/virtualmousekey/MouseControlService;->lastdowntime1:J

    iput-wide v0, p0, Lcom/konka/virtualmousekey/MouseControlService;->lastdowntime2:J

    goto :goto_1

    :sswitch_4
    iget-object v4, p0, Lcom/konka/virtualmousekey/MouseControlService;->mMouseControl:Lcom/konka/virtualmousekey/MouseControl;

    invoke-virtual {v4}, Lcom/konka/virtualmousekey/MouseControl;->mouseLeftClick()V

    goto :goto_1

    nop

    :sswitch_data_0
    .sparse-switch
        0x13 -> :sswitch_0
        0x14 -> :sswitch_1
        0x15 -> :sswitch_2
        0x16 -> :sswitch_3
        0x42 -> :sswitch_4
    .end sparse-switch
.end method


# virtual methods
.method public onBind(Landroid/content/Intent;)Landroid/os/IBinder;
    .locals 1
    .param p1    # Landroid/content/Intent;

    const/4 v0, 0x0

    return-object v0
.end method

.method public onCreate()V
    .locals 2

    invoke-super {p0}, Landroid/app/Service;->onCreate()V

    new-instance v0, Lcom/konka/virtualmousekey/MouseControl;

    invoke-direct {v0}, Lcom/konka/virtualmousekey/MouseControl;-><init>()V

    iput-object v0, p0, Lcom/konka/virtualmousekey/MouseControlService;->mMouseControl:Lcom/konka/virtualmousekey/MouseControl;

    new-instance v0, Landroid/os/Handler;

    invoke-static {}, Landroid/os/Looper;->getMainLooper()Landroid/os/Looper;

    move-result-object v1

    invoke-direct {v0, v1}, Landroid/os/Handler;-><init>(Landroid/os/Looper;)V

    iput-object v0, p0, Lcom/konka/virtualmousekey/MouseControlService;->handler:Landroid/os/Handler;

    return-void
.end method

.method public onDestroy()V
    .locals 1

    iget-object v0, p0, Lcom/konka/virtualmousekey/MouseControlService;->mMouseControl:Lcom/konka/virtualmousekey/MouseControl;

    invoke-virtual {v0}, Lcom/konka/virtualmousekey/MouseControl;->closeInputDevice()V

    invoke-super {p0}, Landroid/app/Service;->onDestroy()V

    return-void
.end method

.method public onStartCommand(Landroid/content/Intent;II)I
    .locals 9
    .param p1    # Landroid/content/Intent;
    .param p2    # I
    .param p3    # I

    const/4 v7, -0x1

    if-eqz p1, :cond_0

    const-string v6, "TYPE"

    invoke-virtual {p1, v6}, Landroid/content/Intent;->getStringExtra(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v5

    const-string v6, "MODE"

    invoke-virtual {p1, v6}, Landroid/content/Intent;->getStringExtra(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v4

    const-string v6, "stop"

    invoke-virtual {v6, v5}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v6

    if-eqz v6, :cond_1

    iget-object v6, p0, Lcom/konka/virtualmousekey/MouseControlService;->TAG:Ljava/lang/String;

    const-string v7, "stop ......"

    invoke-static {v6, v7}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    iget-object v6, p0, Lcom/konka/virtualmousekey/MouseControlService;->mMouseControl:Lcom/konka/virtualmousekey/MouseControl;

    invoke-virtual {v6}, Lcom/konka/virtualmousekey/MouseControl;->closeInputDevice()V

    const-string v6, "auto"

    invoke-virtual {v6, v4}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v6

    if-nez v6, :cond_0

    iget-object v6, p0, Lcom/konka/virtualmousekey/MouseControlService;->handler:Landroid/os/Handler;

    new-instance v7, Lcom/konka/virtualmousekey/MouseControlService$1;

    invoke-direct {v7, p0}, Lcom/konka/virtualmousekey/MouseControlService$1;-><init>(Lcom/konka/virtualmousekey/MouseControlService;)V

    invoke-virtual {v6, v7}, Landroid/os/Handler;->post(Ljava/lang/Runnable;)Z

    :cond_0
    :goto_0
    invoke-super {p0, p1, p2, p3}, Landroid/app/Service;->onStartCommand(Landroid/content/Intent;II)I

    move-result v6

    return v6

    :cond_1
    const-string v6, "start"

    invoke-virtual {v6, v5}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v6

    if-eqz v6, :cond_2

    iget-object v6, p0, Lcom/konka/virtualmousekey/MouseControlService;->TAG:Ljava/lang/String;

    const-string v7, "start ......"

    invoke-static {v6, v7}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    const-string v6, "auto"

    invoke-virtual {v6, v4}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v6

    if-nez v6, :cond_0

    iget-object v6, p0, Lcom/konka/virtualmousekey/MouseControlService;->handler:Landroid/os/Handler;

    new-instance v7, Lcom/konka/virtualmousekey/MouseControlService$2;

    invoke-direct {v7, p0}, Lcom/konka/virtualmousekey/MouseControlService$2;-><init>(Lcom/konka/virtualmousekey/MouseControlService;)V

    invoke-virtual {v6, v7}, Landroid/os/Handler;->post(Ljava/lang/Runnable;)Z

    goto :goto_0

    :cond_2
    const-string v6, "KEYCODE"

    invoke-virtual {p1, v6, v7}, Landroid/content/Intent;->getIntExtra(Ljava/lang/String;I)I

    move-result v3

    const-string v6, "ACTION"

    invoke-virtual {p1, v6, v7}, Landroid/content/Intent;->getIntExtra(Ljava/lang/String;I)I

    move-result v0

    const-string v6, "DOWNTIME"

    const-wide/16 v7, -0x1

    invoke-virtual {p1, v6, v7, v8}, Landroid/content/Intent;->getLongExtra(Ljava/lang/String;J)J

    move-result-wide v1

    iget-object v6, p0, Lcom/konka/virtualmousekey/MouseControlService;->TAG:Ljava/lang/String;

    new-instance v7, Ljava/lang/StringBuilder;

    const-string v8, "keyCode = "

    invoke-direct {v7, v8}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v7, v3}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v7

    const-string v8, ",action = "

    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    invoke-virtual {v7, v0}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v7

    const-string v8, ",downTime = "

    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    invoke-virtual {v7, v1, v2}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v7

    invoke-virtual {v7}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v7

    invoke-static {v6, v7}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    invoke-direct {p0, v3, v0, v1, v2}, Lcom/konka/virtualmousekey/MouseControlService;->handleVirtualMouseKey(IIJ)Z

    goto :goto_0
.end method
