.class public Lorg/apache/xpath/XPathVisitor;
.super Ljava/lang/Object;
.source "XPathVisitor.java"


# direct methods
.method public constructor <init>()V
    .locals 0

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public visitBinaryOperation(Lorg/apache/xpath/ExpressionOwner;Lorg/apache/xpath/operations/Operation;)Z
    .locals 1
    .param p1    # Lorg/apache/xpath/ExpressionOwner;
    .param p2    # Lorg/apache/xpath/operations/Operation;

    const/4 v0, 0x1

    return v0
.end method

.method public visitFunction(Lorg/apache/xpath/ExpressionOwner;Lorg/apache/xpath/functions/Function;)Z
    .locals 1
    .param p1    # Lorg/apache/xpath/ExpressionOwner;
    .param p2    # Lorg/apache/xpath/functions/Function;

    const/4 v0, 0x1

    return v0
.end method

.method public visitLocationPath(Lorg/apache/xpath/ExpressionOwner;Lorg/apache/xpath/axes/LocPathIterator;)Z
    .locals 1
    .param p1    # Lorg/apache/xpath/ExpressionOwner;
    .param p2    # Lorg/apache/xpath/axes/LocPathIterator;

    const/4 v0, 0x1

    return v0
.end method

.method public visitMatchPattern(Lorg/apache/xpath/ExpressionOwner;Lorg/apache/xpath/patterns/StepPattern;)Z
    .locals 1
    .param p1    # Lorg/apache/xpath/ExpressionOwner;
    .param p2    # Lorg/apache/xpath/patterns/StepPattern;

    const/4 v0, 0x1

    return v0
.end method

.method public visitNumberLiteral(Lorg/apache/xpath/ExpressionOwner;Lorg/apache/xpath/objects/XNumber;)Z
    .locals 1
    .param p1    # Lorg/apache/xpath/ExpressionOwner;
    .param p2    # Lorg/apache/xpath/objects/XNumber;

    const/4 v0, 0x1

    return v0
.end method

.method public visitPredicate(Lorg/apache/xpath/ExpressionOwner;Lorg/apache/xpath/Expression;)Z
    .locals 1
    .param p1    # Lorg/apache/xpath/ExpressionOwner;
    .param p2    # Lorg/apache/xpath/Expression;

    const/4 v0, 0x1

    return v0
.end method

.method public visitStep(Lorg/apache/xpath/ExpressionOwner;Lorg/apache/xpath/patterns/NodeTest;)Z
    .locals 1
    .param p1    # Lorg/apache/xpath/ExpressionOwner;
    .param p2    # Lorg/apache/xpath/patterns/NodeTest;

    const/4 v0, 0x1

    return v0
.end method

.method public visitStringLiteral(Lorg/apache/xpath/ExpressionOwner;Lorg/apache/xpath/objects/XString;)Z
    .locals 1
    .param p1    # Lorg/apache/xpath/ExpressionOwner;
    .param p2    # Lorg/apache/xpath/objects/XString;

    const/4 v0, 0x1

    return v0
.end method

.method public visitUnaryOperation(Lorg/apache/xpath/ExpressionOwner;Lorg/apache/xpath/operations/UnaryOperation;)Z
    .locals 1
    .param p1    # Lorg/apache/xpath/ExpressionOwner;
    .param p2    # Lorg/apache/xpath/operations/UnaryOperation;

    const/4 v0, 0x1

    return v0
.end method

.method public visitUnionPath(Lorg/apache/xpath/ExpressionOwner;Lorg/apache/xpath/axes/UnionPathIterator;)Z
    .locals 1
    .param p1    # Lorg/apache/xpath/ExpressionOwner;
    .param p2    # Lorg/apache/xpath/axes/UnionPathIterator;

    const/4 v0, 0x1

    return v0
.end method

.method public visitUnionPattern(Lorg/apache/xpath/ExpressionOwner;Lorg/apache/xpath/patterns/UnionPattern;)Z
    .locals 1
    .param p1    # Lorg/apache/xpath/ExpressionOwner;
    .param p2    # Lorg/apache/xpath/patterns/UnionPattern;

    const/4 v0, 0x1

    return v0
.end method

.method public visitVariableRef(Lorg/apache/xpath/ExpressionOwner;Lorg/apache/xpath/operations/Variable;)Z
    .locals 1
    .param p1    # Lorg/apache/xpath/ExpressionOwner;
    .param p2    # Lorg/apache/xpath/operations/Variable;

    const/4 v0, 0x1

    return v0
.end method
