.class public Lcom/android/calculator2/Calculator;
.super Landroid/app/Activity;
.source "Calculator.java"

# interfaces
.implements Landroid/view/View$OnClickListener;
.implements Landroid/widget/PopupMenu$OnMenuItemClickListener;
.implements Lcom/android/calculator2/Logic$Listener;


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/android/calculator2/Calculator$PageAdapter;
    }
.end annotation


# static fields
.field static PrecursorPos:I


# instance fields
.field private mBackspaceButton:Landroid/view/View;

.field private mClearButton:Landroid/view/View;

.field private mDisplay:Lcom/android/calculator2/CalculatorDisplay;

.field private mHistory:Lcom/android/calculator2/History;

.field mListener:Lcom/android/calculator2/EventListener;

.field private mLogic:Lcom/android/calculator2/Logic;

.field private mOverflowMenuButton:Landroid/view/View;

.field private mPager:Landroid/support/v4/view/ViewPager;

.field private mPersist:Lcom/android/calculator2/Persist;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    const/4 v0, 0x0

    sput v0, Lcom/android/calculator2/Calculator;->PrecursorPos:I

    return-void
.end method

.method public constructor <init>()V
    .locals 1

    invoke-direct {p0}, Landroid/app/Activity;-><init>()V

    new-instance v0, Lcom/android/calculator2/EventListener;

    invoke-direct {v0}, Lcom/android/calculator2/EventListener;-><init>()V

    iput-object v0, p0, Lcom/android/calculator2/Calculator;->mListener:Lcom/android/calculator2/EventListener;

    return-void
.end method

.method static synthetic access$002(Lcom/android/calculator2/Calculator;Landroid/view/View;)Landroid/view/View;
    .locals 0
    .param p0    # Lcom/android/calculator2/Calculator;
    .param p1    # Landroid/view/View;

    iput-object p1, p0, Lcom/android/calculator2/Calculator;->mClearButton:Landroid/view/View;

    return-object p1
.end method

.method static synthetic access$102(Lcom/android/calculator2/Calculator;Landroid/view/View;)Landroid/view/View;
    .locals 0
    .param p0    # Lcom/android/calculator2/Calculator;
    .param p1    # Landroid/view/View;

    iput-object p1, p0, Lcom/android/calculator2/Calculator;->mBackspaceButton:Landroid/view/View;

    return-object p1
.end method

.method private constructPopupMenu()Landroid/widget/PopupMenu;
    .locals 3

    new-instance v1, Landroid/widget/PopupMenu;

    iget-object v2, p0, Lcom/android/calculator2/Calculator;->mOverflowMenuButton:Landroid/view/View;

    invoke-direct {v1, p0, v2}, Landroid/widget/PopupMenu;-><init>(Landroid/content/Context;Landroid/view/View;)V

    invoke-virtual {v1}, Landroid/widget/PopupMenu;->getMenu()Landroid/view/Menu;

    move-result-object v0

    const/high16 v2, 0x7f0b0000    # com.android.calculator2.R.menu.menu

    invoke-virtual {v1, v2}, Landroid/widget/PopupMenu;->inflate(I)V

    invoke-virtual {v1, p0}, Landroid/widget/PopupMenu;->setOnMenuItemClickListener(Landroid/widget/PopupMenu$OnMenuItemClickListener;)V

    invoke-virtual {p0, v0}, Lcom/android/calculator2/Calculator;->onPrepareOptionsMenu(Landroid/view/Menu;)Z

    return-object v1
.end method

.method private createFakeMenu()V
    .locals 2

    const v0, 0x7f0c0010    # com.android.calculator2.R.id.overflow_menu

    invoke-virtual {p0, v0}, Lcom/android/calculator2/Calculator;->findViewById(I)Landroid/view/View;

    move-result-object v0

    iput-object v0, p0, Lcom/android/calculator2/Calculator;->mOverflowMenuButton:Landroid/view/View;

    iget-object v0, p0, Lcom/android/calculator2/Calculator;->mOverflowMenuButton:Landroid/view/View;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/android/calculator2/Calculator;->mOverflowMenuButton:Landroid/view/View;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Landroid/view/View;->setVisibility(I)V

    iget-object v0, p0, Lcom/android/calculator2/Calculator;->mOverflowMenuButton:Landroid/view/View;

    invoke-virtual {v0, p0}, Landroid/view/View;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    :cond_0
    return-void
.end method

.method private getAdvancedVisibility()Z
    .locals 2

    const/4 v0, 0x1

    iget-object v1, p0, Lcom/android/calculator2/Calculator;->mPager:Landroid/support/v4/view/ViewPager;

    if-eqz v1, :cond_0

    iget-object v1, p0, Lcom/android/calculator2/Calculator;->mPager:Landroid/support/v4/view/ViewPager;

    invoke-virtual {v1}, Landroid/support/v4/view/ViewPager;->getCurrentItem()I

    move-result v1

    if-ne v1, v0, :cond_0

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method private getBasicVisibility()Z
    .locals 1

    iget-object v0, p0, Lcom/android/calculator2/Calculator;->mPager:Landroid/support/v4/view/ViewPager;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/android/calculator2/Calculator;->mPager:Landroid/support/v4/view/ViewPager;

    invoke-virtual {v0}, Landroid/support/v4/view/ViewPager;->getCurrentItem()I

    move-result v0

    if-nez v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method static log(Ljava/lang/String;)V
    .locals 0
    .param p0    # Ljava/lang/String;

    return-void
.end method

.method private updateDeleteMode()V
    .locals 3

    const/16 v2, 0x8

    const/4 v1, 0x0

    iget-object v0, p0, Lcom/android/calculator2/Calculator;->mLogic:Lcom/android/calculator2/Logic;

    invoke-virtual {v0}, Lcom/android/calculator2/Logic;->getDeleteMode()I

    move-result v0

    if-nez v0, :cond_0

    iget-object v0, p0, Lcom/android/calculator2/Calculator;->mClearButton:Landroid/view/View;

    invoke-virtual {v0, v2}, Landroid/view/View;->setVisibility(I)V

    iget-object v0, p0, Lcom/android/calculator2/Calculator;->mBackspaceButton:Landroid/view/View;

    invoke-virtual {v0, v1}, Landroid/view/View;->setVisibility(I)V

    :goto_0
    return-void

    :cond_0
    iget-object v0, p0, Lcom/android/calculator2/Calculator;->mClearButton:Landroid/view/View;

    invoke-virtual {v0, v1}, Landroid/view/View;->setVisibility(I)V

    iget-object v0, p0, Lcom/android/calculator2/Calculator;->mBackspaceButton:Landroid/view/View;

    invoke-virtual {v0, v2}, Landroid/view/View;->setVisibility(I)V

    goto :goto_0
.end method


# virtual methods
.method public onClick(Landroid/view/View;)V
    .locals 2
    .param p1    # Landroid/view/View;

    invoke-virtual {p1}, Landroid/view/View;->getId()I

    move-result v1

    packed-switch v1, :pswitch_data_0

    :cond_0
    :goto_0
    return-void

    :pswitch_0
    invoke-direct {p0}, Lcom/android/calculator2/Calculator;->constructPopupMenu()Landroid/widget/PopupMenu;

    move-result-object v0

    if-eqz v0, :cond_0

    invoke-virtual {v0}, Landroid/widget/PopupMenu;->show()V

    goto :goto_0

    :pswitch_data_0
    .packed-switch 0x7f0c0010
        :pswitch_0    # com.android.calculator2.R.id.overflow_menu
    .end packed-switch
.end method

.method public onCreate(Landroid/os/Bundle;)V
    .locals 7
    .param p1    # Landroid/os/Bundle;

    const/high16 v5, 0x20000

    const/4 v4, 0x0

    invoke-super {p0, p1}, Landroid/app/Activity;->onCreate(Landroid/os/Bundle;)V

    invoke-virtual {p0}, Lcom/android/calculator2/Calculator;->getWindow()Landroid/view/Window;

    move-result-object v3

    invoke-virtual {v3, v5, v5}, Landroid/view/Window;->setFlags(II)V

    const v3, 0x7f040002    # com.android.calculator2.R.layout.main

    invoke-virtual {p0, v3}, Lcom/android/calculator2/Calculator;->setContentView(I)V

    const v3, 0x7f0c0013    # com.android.calculator2.R.id.panelswitch

    invoke-virtual {p0, v3}, Lcom/android/calculator2/Calculator;->findViewById(I)Landroid/view/View;

    move-result-object v3

    check-cast v3, Landroid/support/v4/view/ViewPager;

    iput-object v3, p0, Lcom/android/calculator2/Calculator;->mPager:Landroid/support/v4/view/ViewPager;

    iget-object v3, p0, Lcom/android/calculator2/Calculator;->mPager:Landroid/support/v4/view/ViewPager;

    if-eqz v3, :cond_4

    iget-object v3, p0, Lcom/android/calculator2/Calculator;->mPager:Landroid/support/v4/view/ViewPager;

    new-instance v5, Lcom/android/calculator2/Calculator$PageAdapter;

    iget-object v6, p0, Lcom/android/calculator2/Calculator;->mPager:Landroid/support/v4/view/ViewPager;

    invoke-direct {v5, p0, v6}, Lcom/android/calculator2/Calculator$PageAdapter;-><init>(Lcom/android/calculator2/Calculator;Landroid/support/v4/view/ViewPager;)V

    invoke-virtual {v3, v5}, Landroid/support/v4/view/ViewPager;->setAdapter(Landroid/support/v4/view/PagerAdapter;)V

    :goto_0
    iget-object v3, p0, Lcom/android/calculator2/Calculator;->mClearButton:Landroid/view/View;

    if-nez v3, :cond_0

    const v3, 0x7f0c0011    # com.android.calculator2.R.id.clear

    invoke-virtual {p0, v3}, Lcom/android/calculator2/Calculator;->findViewById(I)Landroid/view/View;

    move-result-object v3

    iput-object v3, p0, Lcom/android/calculator2/Calculator;->mClearButton:Landroid/view/View;

    iget-object v3, p0, Lcom/android/calculator2/Calculator;->mClearButton:Landroid/view/View;

    iget-object v5, p0, Lcom/android/calculator2/Calculator;->mListener:Lcom/android/calculator2/EventListener;

    invoke-virtual {v3, v5}, Landroid/view/View;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    iget-object v3, p0, Lcom/android/calculator2/Calculator;->mClearButton:Landroid/view/View;

    iget-object v5, p0, Lcom/android/calculator2/Calculator;->mListener:Lcom/android/calculator2/EventListener;

    invoke-virtual {v3, v5}, Landroid/view/View;->setOnLongClickListener(Landroid/view/View$OnLongClickListener;)V

    :cond_0
    iget-object v3, p0, Lcom/android/calculator2/Calculator;->mBackspaceButton:Landroid/view/View;

    if-nez v3, :cond_1

    const v3, 0x7f0c0012    # com.android.calculator2.R.id.del

    invoke-virtual {p0, v3}, Lcom/android/calculator2/Calculator;->findViewById(I)Landroid/view/View;

    move-result-object v3

    iput-object v3, p0, Lcom/android/calculator2/Calculator;->mBackspaceButton:Landroid/view/View;

    iget-object v3, p0, Lcom/android/calculator2/Calculator;->mBackspaceButton:Landroid/view/View;

    iget-object v5, p0, Lcom/android/calculator2/Calculator;->mListener:Lcom/android/calculator2/EventListener;

    invoke-virtual {v3, v5}, Landroid/view/View;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    iget-object v3, p0, Lcom/android/calculator2/Calculator;->mBackspaceButton:Landroid/view/View;

    iget-object v5, p0, Lcom/android/calculator2/Calculator;->mListener:Lcom/android/calculator2/EventListener;

    invoke-virtual {v3, v5}, Landroid/view/View;->setOnLongClickListener(Landroid/view/View$OnLongClickListener;)V

    :cond_1
    new-instance v3, Lcom/android/calculator2/Persist;

    invoke-direct {v3, p0}, Lcom/android/calculator2/Persist;-><init>(Landroid/content/Context;)V

    iput-object v3, p0, Lcom/android/calculator2/Calculator;->mPersist:Lcom/android/calculator2/Persist;

    iget-object v3, p0, Lcom/android/calculator2/Calculator;->mPersist:Lcom/android/calculator2/Persist;

    invoke-virtual {v3}, Lcom/android/calculator2/Persist;->load()V

    iget-object v3, p0, Lcom/android/calculator2/Calculator;->mPersist:Lcom/android/calculator2/Persist;

    iget-object v3, v3, Lcom/android/calculator2/Persist;->history:Lcom/android/calculator2/History;

    iput-object v3, p0, Lcom/android/calculator2/Calculator;->mHistory:Lcom/android/calculator2/History;

    const v3, 0x7f0c000f    # com.android.calculator2.R.id.display

    invoke-virtual {p0, v3}, Lcom/android/calculator2/Calculator;->findViewById(I)Landroid/view/View;

    move-result-object v3

    check-cast v3, Lcom/android/calculator2/CalculatorDisplay;

    iput-object v3, p0, Lcom/android/calculator2/Calculator;->mDisplay:Lcom/android/calculator2/CalculatorDisplay;

    new-instance v3, Lcom/android/calculator2/Logic;

    iget-object v5, p0, Lcom/android/calculator2/Calculator;->mHistory:Lcom/android/calculator2/History;

    iget-object v6, p0, Lcom/android/calculator2/Calculator;->mDisplay:Lcom/android/calculator2/CalculatorDisplay;

    invoke-direct {v3, p0, v5, v6}, Lcom/android/calculator2/Logic;-><init>(Landroid/content/Context;Lcom/android/calculator2/History;Lcom/android/calculator2/CalculatorDisplay;)V

    iput-object v3, p0, Lcom/android/calculator2/Calculator;->mLogic:Lcom/android/calculator2/Logic;

    iget-object v3, p0, Lcom/android/calculator2/Calculator;->mLogic:Lcom/android/calculator2/Logic;

    invoke-virtual {v3, p0}, Lcom/android/calculator2/Logic;->setListener(Lcom/android/calculator2/Logic$Listener;)V

    iget-object v3, p0, Lcom/android/calculator2/Calculator;->mLogic:Lcom/android/calculator2/Logic;

    iget-object v5, p0, Lcom/android/calculator2/Calculator;->mPersist:Lcom/android/calculator2/Persist;

    invoke-virtual {v5}, Lcom/android/calculator2/Persist;->getDeleteMode()I

    move-result v5

    invoke-virtual {v3, v5}, Lcom/android/calculator2/Logic;->setDeleteMode(I)V

    iget-object v3, p0, Lcom/android/calculator2/Calculator;->mLogic:Lcom/android/calculator2/Logic;

    iget-object v5, p0, Lcom/android/calculator2/Calculator;->mDisplay:Lcom/android/calculator2/CalculatorDisplay;

    invoke-virtual {v5}, Lcom/android/calculator2/CalculatorDisplay;->getMaxDigits()I

    move-result v5

    invoke-virtual {v3, v5}, Lcom/android/calculator2/Logic;->setLineLength(I)V

    new-instance v1, Lcom/android/calculator2/HistoryAdapter;

    iget-object v3, p0, Lcom/android/calculator2/Calculator;->mHistory:Lcom/android/calculator2/History;

    iget-object v5, p0, Lcom/android/calculator2/Calculator;->mLogic:Lcom/android/calculator2/Logic;

    invoke-direct {v1, p0, v3, v5}, Lcom/android/calculator2/HistoryAdapter;-><init>(Landroid/content/Context;Lcom/android/calculator2/History;Lcom/android/calculator2/Logic;)V

    iget-object v3, p0, Lcom/android/calculator2/Calculator;->mHistory:Lcom/android/calculator2/History;

    invoke-virtual {v3, v1}, Lcom/android/calculator2/History;->setObserver(Landroid/widget/BaseAdapter;)V

    iget-object v3, p0, Lcom/android/calculator2/Calculator;->mPager:Landroid/support/v4/view/ViewPager;

    if-eqz v3, :cond_2

    iget-object v5, p0, Lcom/android/calculator2/Calculator;->mPager:Landroid/support/v4/view/ViewPager;

    if-nez p1, :cond_6

    move v3, v4

    :goto_1
    invoke-virtual {v5, v3}, Landroid/support/v4/view/ViewPager;->setCurrentItem(I)V

    :cond_2
    iget-object v3, p0, Lcom/android/calculator2/Calculator;->mListener:Lcom/android/calculator2/EventListener;

    iget-object v4, p0, Lcom/android/calculator2/Calculator;->mLogic:Lcom/android/calculator2/Logic;

    iget-object v5, p0, Lcom/android/calculator2/Calculator;->mPager:Landroid/support/v4/view/ViewPager;

    invoke-virtual {v3, v4, v5}, Lcom/android/calculator2/EventListener;->setHandler(Lcom/android/calculator2/Logic;Landroid/support/v4/view/ViewPager;)V

    iget-object v3, p0, Lcom/android/calculator2/Calculator;->mDisplay:Lcom/android/calculator2/CalculatorDisplay;

    iget-object v4, p0, Lcom/android/calculator2/Calculator;->mListener:Lcom/android/calculator2/EventListener;

    invoke-virtual {v3, v4}, Lcom/android/calculator2/CalculatorDisplay;->setOnKeyListener(Landroid/view/View$OnKeyListener;)V

    invoke-static {p0}, Landroid/view/ViewConfiguration;->get(Landroid/content/Context;)Landroid/view/ViewConfiguration;

    move-result-object v3

    invoke-virtual {v3}, Landroid/view/ViewConfiguration;->hasPermanentMenuKey()Z

    move-result v3

    if-nez v3, :cond_3

    invoke-direct {p0}, Lcom/android/calculator2/Calculator;->createFakeMenu()V

    :cond_3
    iget-object v3, p0, Lcom/android/calculator2/Calculator;->mLogic:Lcom/android/calculator2/Logic;

    invoke-virtual {v3}, Lcom/android/calculator2/Logic;->resumeWithHistory()V

    invoke-direct {p0}, Lcom/android/calculator2/Calculator;->updateDeleteMode()V

    return-void

    :cond_4
    invoke-virtual {p0}, Lcom/android/calculator2/Calculator;->getResources()Landroid/content/res/Resources;

    move-result-object v3

    const v5, 0x7f050002    # com.android.calculator2.R.array.buttons

    invoke-virtual {v3, v5}, Landroid/content/res/Resources;->obtainTypedArray(I)Landroid/content/res/TypedArray;

    move-result-object v0

    const/4 v2, 0x0

    :goto_2
    invoke-virtual {v0}, Landroid/content/res/TypedArray;->length()I

    move-result v3

    if-ge v2, v3, :cond_5

    const/4 v3, 0x0

    invoke-virtual {v0, v2, v4}, Landroid/content/res/TypedArray;->getResourceId(II)I

    move-result v5

    invoke-virtual {p0, v3, v5}, Lcom/android/calculator2/Calculator;->setOnClickListener(Landroid/view/View;I)V

    add-int/lit8 v2, v2, 0x1

    goto :goto_2

    :cond_5
    invoke-virtual {v0}, Landroid/content/res/TypedArray;->recycle()V

    goto/16 :goto_0

    :cond_6
    const-string v3, "state-current-view"

    invoke-virtual {p1, v3, v4}, Landroid/os/Bundle;->getInt(Ljava/lang/String;I)I

    move-result v3

    goto :goto_1
.end method

.method public onCreateOptionsMenu(Landroid/view/Menu;)Z
    .locals 2
    .param p1    # Landroid/view/Menu;

    invoke-super {p0, p1}, Landroid/app/Activity;->onCreateOptionsMenu(Landroid/view/Menu;)Z

    invoke-virtual {p0}, Lcom/android/calculator2/Calculator;->getMenuInflater()Landroid/view/MenuInflater;

    move-result-object v0

    const/high16 v1, 0x7f0b0000    # com.android.calculator2.R.menu.menu

    invoke-virtual {v0, v1, p1}, Landroid/view/MenuInflater;->inflate(ILandroid/view/Menu;)V

    const/4 v0, 0x1

    return v0
.end method

.method public onDeleteModeChange()V
    .locals 0

    invoke-direct {p0}, Lcom/android/calculator2/Calculator;->updateDeleteMode()V

    return-void
.end method

.method public onKeyDown(ILandroid/view/KeyEvent;)Z
    .locals 5
    .param p1    # I
    .param p2    # Landroid/view/KeyEvent;

    const/4 v4, 0x0

    const/4 v2, 0x1

    iget-object v3, p0, Lcom/android/calculator2/Calculator;->mDisplay:Lcom/android/calculator2/CalculatorDisplay;

    invoke-virtual {v3}, Lcom/android/calculator2/CalculatorDisplay;->getEditText()Landroid/widget/EditText;

    move-result-object v1

    invoke-virtual {v1}, Landroid/widget/EditText;->getSelectionStart()I

    move-result v0

    const/4 v3, 0x4

    if-ne p1, v3, :cond_0

    invoke-direct {p0}, Lcom/android/calculator2/Calculator;->getAdvancedVisibility()Z

    move-result v3

    if-eqz v3, :cond_0

    iget-object v3, p0, Lcom/android/calculator2/Calculator;->mPager:Landroid/support/v4/view/ViewPager;

    invoke-virtual {v3, v4}, Landroid/support/v4/view/ViewPager;->setCurrentItem(I)V

    :goto_0
    return v2

    :cond_0
    invoke-virtual {v1}, Landroid/widget/EditText;->isFocused()Z

    move-result v3

    if-eqz v3, :cond_2

    const/16 v3, 0x16

    if-ne p1, v3, :cond_2

    invoke-virtual {v1}, Landroid/widget/EditText;->length()I

    move-result v3

    if-ne v0, v3, :cond_2

    sget v3, Lcom/android/calculator2/Calculator;->PrecursorPos:I

    if-ne v3, v0, :cond_2

    iget-object v3, p0, Lcom/android/calculator2/Calculator;->mLogic:Lcom/android/calculator2/Logic;

    invoke-virtual {v3}, Lcom/android/calculator2/Logic;->getDeleteMode()I

    move-result v3

    if-nez v3, :cond_1

    iget-object v3, p0, Lcom/android/calculator2/Calculator;->mBackspaceButton:Landroid/view/View;

    invoke-virtual {v3}, Landroid/view/View;->requestFocus()Z

    :goto_1
    sput v0, Lcom/android/calculator2/Calculator;->PrecursorPos:I

    goto :goto_0

    :cond_1
    iget-object v3, p0, Lcom/android/calculator2/Calculator;->mClearButton:Landroid/view/View;

    invoke-virtual {v3}, Landroid/view/View;->requestFocus()Z

    goto :goto_1

    :cond_2
    const/16 v3, 0x13

    if-ne p1, v3, :cond_5

    iget-object v3, p0, Lcom/android/calculator2/Calculator;->mBackspaceButton:Landroid/view/View;

    invoke-virtual {v3}, Landroid/view/View;->isFocused()Z

    move-result v3

    if-eqz v3, :cond_4

    iget-object v3, p0, Lcom/android/calculator2/Calculator;->mLogic:Lcom/android/calculator2/Logic;

    invoke-virtual {v3}, Lcom/android/calculator2/Logic;->getDeleteMode()I

    move-result v3

    if-nez v3, :cond_4

    iget-object v3, p0, Lcom/android/calculator2/Calculator;->mLogic:Lcom/android/calculator2/Logic;

    invoke-virtual {v3, v2}, Lcom/android/calculator2/Logic;->setDeleteMode(I)V

    :cond_3
    :goto_2
    invoke-super {p0, p1, p2}, Landroid/app/Activity;->onKeyDown(ILandroid/view/KeyEvent;)Z

    move-result v2

    goto :goto_0

    :cond_4
    iget-object v3, p0, Lcom/android/calculator2/Calculator;->mClearButton:Landroid/view/View;

    invoke-virtual {v3}, Landroid/view/View;->isFocused()Z

    move-result v3

    if-eqz v3, :cond_3

    iget-object v3, p0, Lcom/android/calculator2/Calculator;->mLogic:Lcom/android/calculator2/Logic;

    invoke-virtual {v3}, Lcom/android/calculator2/Logic;->getDeleteMode()I

    move-result v3

    if-ne v3, v2, :cond_3

    iget-object v2, p0, Lcom/android/calculator2/Calculator;->mLogic:Lcom/android/calculator2/Logic;

    invoke-virtual {v2, v4}, Lcom/android/calculator2/Logic;->setDeleteMode(I)V

    goto :goto_2

    :cond_5
    invoke-super {p0, p1, p2}, Landroid/app/Activity;->onKeyDown(ILandroid/view/KeyEvent;)Z

    move-result v2

    goto :goto_0
.end method

.method public onMenuItemClick(Landroid/view/MenuItem;)Z
    .locals 1
    .param p1    # Landroid/view/MenuItem;

    invoke-virtual {p0, p1}, Lcom/android/calculator2/Calculator;->onOptionsItemSelected(Landroid/view/MenuItem;)Z

    move-result v0

    return v0
.end method

.method public onOptionsItemSelected(Landroid/view/MenuItem;)Z
    .locals 2
    .param p1    # Landroid/view/MenuItem;

    invoke-interface {p1}, Landroid/view/MenuItem;->getItemId()I

    move-result v0

    packed-switch v0, :pswitch_data_0

    :cond_0
    :goto_0
    invoke-super {p0, p1}, Landroid/app/Activity;->onOptionsItemSelected(Landroid/view/MenuItem;)Z

    move-result v0

    return v0

    :pswitch_0
    iget-object v0, p0, Lcom/android/calculator2/Calculator;->mHistory:Lcom/android/calculator2/History;

    invoke-virtual {v0}, Lcom/android/calculator2/History;->clear()V

    iget-object v0, p0, Lcom/android/calculator2/Calculator;->mLogic:Lcom/android/calculator2/Logic;

    invoke-virtual {v0}, Lcom/android/calculator2/Logic;->onClear()V

    goto :goto_0

    :pswitch_1
    invoke-direct {p0}, Lcom/android/calculator2/Calculator;->getBasicVisibility()Z

    move-result v0

    if-nez v0, :cond_0

    iget-object v0, p0, Lcom/android/calculator2/Calculator;->mPager:Landroid/support/v4/view/ViewPager;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Landroid/support/v4/view/ViewPager;->setCurrentItem(I)V

    goto :goto_0

    :pswitch_2
    invoke-direct {p0}, Lcom/android/calculator2/Calculator;->getAdvancedVisibility()Z

    move-result v0

    if-nez v0, :cond_0

    iget-object v0, p0, Lcom/android/calculator2/Calculator;->mPager:Landroid/support/v4/view/ViewPager;

    const/4 v1, 0x1

    invoke-virtual {v0, v1}, Landroid/support/v4/view/ViewPager;->setCurrentItem(I)V

    goto :goto_0

    nop

    :pswitch_data_0
    .packed-switch 0x7f0c0025
        :pswitch_0    # com.android.calculator2.R.id.clear_history
        :pswitch_2    # com.android.calculator2.R.id.advanced
        :pswitch_1    # com.android.calculator2.R.id.basic
    .end packed-switch
.end method

.method public onPause()V
    .locals 2

    invoke-super {p0}, Landroid/app/Activity;->onPause()V

    iget-object v0, p0, Lcom/android/calculator2/Calculator;->mLogic:Lcom/android/calculator2/Logic;

    invoke-virtual {v0}, Lcom/android/calculator2/Logic;->updateHistory()V

    iget-object v0, p0, Lcom/android/calculator2/Calculator;->mPersist:Lcom/android/calculator2/Persist;

    iget-object v1, p0, Lcom/android/calculator2/Calculator;->mLogic:Lcom/android/calculator2/Logic;

    invoke-virtual {v1}, Lcom/android/calculator2/Logic;->getDeleteMode()I

    move-result v1

    invoke-virtual {v0, v1}, Lcom/android/calculator2/Persist;->setDeleteMode(I)V

    iget-object v0, p0, Lcom/android/calculator2/Calculator;->mPersist:Lcom/android/calculator2/Persist;

    invoke-virtual {v0}, Lcom/android/calculator2/Persist;->save()V

    return-void
.end method

.method public onPrepareOptionsMenu(Landroid/view/Menu;)Z
    .locals 4
    .param p1    # Landroid/view/Menu;

    const/4 v2, 0x0

    const/4 v1, 0x1

    invoke-super {p0, p1}, Landroid/app/Activity;->onPrepareOptionsMenu(Landroid/view/Menu;)Z

    const v0, 0x7f0c0027    # com.android.calculator2.R.id.basic

    invoke-interface {p1, v0}, Landroid/view/Menu;->findItem(I)Landroid/view/MenuItem;

    move-result-object v3

    invoke-direct {p0}, Lcom/android/calculator2/Calculator;->getBasicVisibility()Z

    move-result v0

    if-nez v0, :cond_1

    move v0, v1

    :goto_0
    invoke-interface {v3, v0}, Landroid/view/MenuItem;->setVisible(Z)Landroid/view/MenuItem;

    const v0, 0x7f0c0026    # com.android.calculator2.R.id.advanced

    invoke-interface {p1, v0}, Landroid/view/Menu;->findItem(I)Landroid/view/MenuItem;

    move-result-object v0

    invoke-direct {p0}, Lcom/android/calculator2/Calculator;->getAdvancedVisibility()Z

    move-result v3

    if-nez v3, :cond_0

    move v2, v1

    :cond_0
    invoke-interface {v0, v2}, Landroid/view/MenuItem;->setVisible(Z)Landroid/view/MenuItem;

    return v1

    :cond_1
    move v0, v2

    goto :goto_0
.end method

.method protected onSaveInstanceState(Landroid/os/Bundle;)V
    .locals 2
    .param p1    # Landroid/os/Bundle;

    invoke-super {p0, p1}, Landroid/app/Activity;->onSaveInstanceState(Landroid/os/Bundle;)V

    iget-object v0, p0, Lcom/android/calculator2/Calculator;->mPager:Landroid/support/v4/view/ViewPager;

    if-eqz v0, :cond_0

    const-string v0, "state-current-view"

    iget-object v1, p0, Lcom/android/calculator2/Calculator;->mPager:Landroid/support/v4/view/ViewPager;

    invoke-virtual {v1}, Landroid/support/v4/view/ViewPager;->getCurrentItem()I

    move-result v1

    invoke-virtual {p1, v0, v1}, Landroid/os/Bundle;->putInt(Ljava/lang/String;I)V

    :cond_0
    return-void
.end method

.method setOnClickListener(Landroid/view/View;I)V
    .locals 2
    .param p1    # Landroid/view/View;
    .param p2    # I

    if-eqz p1, :cond_0

    invoke-virtual {p1, p2}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    :goto_0
    iget-object v1, p0, Lcom/android/calculator2/Calculator;->mListener:Lcom/android/calculator2/EventListener;

    invoke-virtual {v0, v1}, Landroid/view/View;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    return-void

    :cond_0
    invoke-virtual {p0, p2}, Lcom/android/calculator2/Calculator;->findViewById(I)Landroid/view/View;

    move-result-object v0

    goto :goto_0
.end method
