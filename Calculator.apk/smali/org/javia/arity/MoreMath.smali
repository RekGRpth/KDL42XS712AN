.class Lorg/javia/arity/MoreMath;
.super Ljava/lang/Object;
.source "MoreMath.java"


# static fields
.field static final FACT:[D

.field static final GAMMA:[D


# direct methods
.method static constructor <clinit>()V
    .locals 1

    const/16 v0, 0xe

    new-array v0, v0, [D

    fill-array-data v0, :array_0

    sput-object v0, Lorg/javia/arity/MoreMath;->GAMMA:[D

    const/16 v0, 0x16

    new-array v0, v0, [D

    fill-array-data v0, :array_1

    sput-object v0, Lorg/javia/arity/MoreMath;->FACT:[D

    return-void

    nop

    :array_0
    .array-data 8
        0x404c93ff87c1acceL    # 57.15623566586292
        -0x3fb2337608fa76d0L    # -59.59796035547549
        0x402c45aea23d22a1L    # 14.136097974741746
        -0x4020847be9da401cL    # -0.4919138160976202
        0x3f01d2af4786183aL    # 3.399464998481189E-5
        0x3f08644bb7c5e3bdL    # 4.652362892704858E-5
        -0x40e63633621a8b49L    # -9.837447530487956E-5
        0x3f24b8939ed4e66dL    # 1.580887032249125E-4
        -0x40d470b232d541caL    # -2.1026444172410488E-4
        0x3f2c801018e9e826L    # 2.1743961811521265E-4
        -0x40da7666366ad9c0L    # -1.643181065367639E-4
        0x3f1621360b773d55L    # 8.441822398385275E-5
        -0x410489734a2e1dfaL    # -2.6190838401581408E-5
        0x3ecef40a04fc9810L    # 3.6899182659531625E-6
    .end array-data

    :array_1
    .array-data 8
        0x3ff0000000000000L    # 1.0
        0x40e3b00000000000L    # 40320.0
        0x42b3077775800000L    # 2.0922789888E13
        0x44e06c52687a7b9aL    # 6.204484017332394E23
        0x474956ad0aae33a4L    # 2.631308369336935E35
        0x49e1dd5d037098feL    # 8.159152832478977E47
        0x4c9ee69a78d72cb6L    # 1.2413915592536073E61
        0x4f792693359a4003L    # 7.109985878048635E74
        0x526fe478ee34844aL    # 1.2688693218588417E89
        0x557b5705796695b6L    # 6.1234458376886085E103
        0x589c619094edabffL    # 7.156945704626381E118
        0x5bd0550c4b30743eL    # 1.8548264225739844E134
        0x5f13638dd7bd6347L    # 9.916779348709496E149
        0x62665b0eb1760a70L    # 1.0299016745145628E166
        0x65c7cac197cfe503L    # 1.974506857221074E182
        0x69365f6380a9d916L    # 6.689502913449127E198
        0x6cb1e5dfc140e1e5L    # 3.856204823625804E215
        0x70379185413b0855L    # 3.659042881952549E232
        0x73c8ce85fadb707eL    # 5.5502938327393044E249
        0x776455903aefd5a3L    # 1.3113358856834524E267
        0x7b095d5f3d928edeL    # 4.7147236359920616E284
        0x7eb7932fa79d3a43L    # 2.5260757449731984E302
    .end array-data
.end method

.method constructor <init>()V
    .locals 0

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method public static final acosh(D)D
    .locals 6

    const-wide/high16 v4, 0x3ff0000000000000L    # 1.0

    add-double v0, p0, p0

    mul-double v2, p0, p0

    sub-double/2addr v2, v4

    invoke-static {v2, v3}, Ljava/lang/Math;->sqrt(D)D

    move-result-wide v2

    add-double/2addr v2, p0

    div-double v2, v4, v2

    sub-double/2addr v0, v2

    invoke-static {v0, v1}, Ljava/lang/Math;->log(D)D

    move-result-wide v0

    return-wide v0
.end method

.method public static final asinh(D)D
    .locals 6

    const-wide/high16 v4, 0x3ff0000000000000L    # 1.0

    const-wide/16 v0, 0x0

    cmpg-double v0, p0, v0

    if-gez v0, :cond_0

    neg-double v0, p0

    invoke-static {v0, v1}, Lorg/javia/arity/MoreMath;->asinh(D)D

    move-result-wide v0

    neg-double v0, v0

    :goto_0
    return-wide v0

    :cond_0
    add-double v0, p0, p0

    mul-double v2, p0, p0

    add-double/2addr v2, v4

    invoke-static {v2, v3}, Ljava/lang/Math;->sqrt(D)D

    move-result-wide v2

    add-double/2addr v2, p0

    div-double v2, v4, v2

    add-double/2addr v0, v2

    invoke-static {v0, v1}, Ljava/lang/Math;->log(D)D

    move-result-wide v0

    goto :goto_0
.end method

.method public static final atanh(D)D
    .locals 8

    const-wide/high16 v6, 0x3ff0000000000000L    # 1.0

    const-wide/16 v0, 0x0

    cmpg-double v0, p0, v0

    if-gez v0, :cond_0

    neg-double v0, p0

    invoke-static {v0, v1}, Lorg/javia/arity/MoreMath;->atanh(D)D

    move-result-wide v0

    neg-double v0, v0

    :goto_0
    return-wide v0

    :cond_0
    const-wide/high16 v0, 0x3fe0000000000000L    # 0.5

    add-double v2, p0, p0

    sub-double v4, v6, p0

    div-double/2addr v2, v4

    add-double/2addr v2, v6

    invoke-static {v2, v3}, Ljava/lang/Math;->log(D)D

    move-result-wide v2

    mul-double/2addr v0, v2

    goto :goto_0
.end method

.method public static final combinations(DD)D
    .locals 10

    const-wide v6, 0x4065400000000000L    # 170.0

    const-wide/high16 v4, 0x3ff0000000000000L    # 1.0

    const-wide/16 v0, 0x0

    cmpg-double v2, p0, v0

    if-ltz v2, :cond_0

    cmpg-double v2, p2, v0

    if-gez v2, :cond_2

    :cond_0
    const-wide/high16 v0, 0x7ff8000000000000L    # NaN

    :cond_1
    :goto_0
    return-wide v0

    :cond_2
    cmpg-double v2, p0, p2

    if-ltz v2, :cond_1

    invoke-static {p0, p1}, Ljava/lang/Math;->floor(D)D

    move-result-wide v0

    cmpl-double v0, v0, p0

    if-nez v0, :cond_4

    invoke-static {p2, p3}, Ljava/lang/Math;->floor(D)D

    move-result-wide v0

    cmpl-double v0, v0, p2

    if-nez v0, :cond_4

    sub-double v0, p0, p2

    invoke-static {p2, p3, v0, v1}, Ljava/lang/Math;->min(DD)D

    move-result-wide v0

    cmpg-double v2, p0, v6

    if-gtz v2, :cond_3

    const-wide/high16 v2, 0x4028000000000000L    # 12.0

    cmpg-double v2, v2, v0

    if-gez v2, :cond_3

    cmpg-double v2, v0, v6

    if-gtz v2, :cond_3

    invoke-static {p0, p1}, Lorg/javia/arity/MoreMath;->factorial(D)D

    move-result-wide v2

    invoke-static {v0, v1}, Lorg/javia/arity/MoreMath;->factorial(D)D

    move-result-wide v4

    div-double/2addr v2, v4

    sub-double v0, p0, v0

    invoke-static {v0, v1}, Lorg/javia/arity/MoreMath;->factorial(D)D

    move-result-wide v0

    div-double v0, v2, v0

    goto :goto_0

    :cond_3
    sub-double v8, p0, v0

    move-wide v2, v0

    move-wide v0, v4

    :goto_1
    const-wide/high16 v6, 0x3fe0000000000000L    # 0.5

    cmpl-double v6, v2, v6

    if-lez v6, :cond_1

    const-wide/high16 v6, 0x7ff0000000000000L    # Double.POSITIVE_INFINITY

    cmpg-double v6, v0, v6

    if-gez v6, :cond_1

    add-double v6, v8, v2

    div-double/2addr v6, v2

    mul-double/2addr v6, v0

    sub-double v0, v2, v4

    move-wide v2, v0

    move-wide v0, v6

    goto :goto_1

    :cond_4
    invoke-static {p0, p1}, Lorg/javia/arity/MoreMath;->lgamma(D)D

    move-result-wide v0

    invoke-static {p2, p3}, Lorg/javia/arity/MoreMath;->lgamma(D)D

    move-result-wide v2

    sub-double/2addr v0, v2

    sub-double v2, p0, p2

    invoke-static {v2, v3}, Lorg/javia/arity/MoreMath;->lgamma(D)D

    move-result-wide v2

    sub-double/2addr v0, v2

    invoke-static {v0, v1}, Ljava/lang/Math;->exp(D)D

    move-result-wide v0

    goto :goto_0
.end method

.method public static final cos(D)D
    .locals 2

    const-wide v0, 0x3ff921fb54442d18L    # 1.5707963267948966

    sub-double v0, p0, v0

    invoke-static {v0, v1}, Lorg/javia/arity/MoreMath;->isPiMultiple(D)Z

    move-result v0

    if-eqz v0, :cond_0

    const-wide/16 v0, 0x0

    :goto_0
    return-wide v0

    :cond_0
    invoke-static {p0, p1}, Ljava/lang/Math;->cos(D)D

    move-result-wide v0

    goto :goto_0
.end method

.method public static final factorial(D)D
    .locals 5

    const-wide/high16 v3, 0x3ff0000000000000L    # 1.0

    const-wide/16 v0, 0x0

    cmpg-double v0, p0, v0

    if-gez v0, :cond_0

    const-wide/high16 v0, 0x7ff8000000000000L    # NaN

    :goto_0
    return-wide v0

    :cond_0
    const-wide v0, 0x4065400000000000L    # 170.0

    cmpg-double v0, p0, v0

    if-gtz v0, :cond_1

    invoke-static {p0, p1}, Ljava/lang/Math;->floor(D)D

    move-result-wide v0

    cmpl-double v0, v0, p0

    if-nez v0, :cond_1

    double-to-int v2, p0

    and-int/lit8 v0, v2, 0x7

    packed-switch v0, :pswitch_data_0

    :cond_1
    invoke-static {p0, p1}, Lorg/javia/arity/MoreMath;->lgamma(D)D

    move-result-wide v0

    invoke-static {v0, v1}, Ljava/lang/Math;->exp(D)D

    move-result-wide v0

    goto :goto_0

    :pswitch_0
    sub-double v0, p0, v3

    mul-double/2addr p0, v0

    :goto_1
    sub-double/2addr v0, v3

    mul-double/2addr p0, v0

    :goto_2
    sub-double/2addr v0, v3

    mul-double/2addr p0, v0

    :goto_3
    sub-double/2addr v0, v3

    mul-double/2addr p0, v0

    :goto_4
    sub-double/2addr v0, v3

    mul-double/2addr p0, v0

    :goto_5
    sub-double/2addr v0, v3

    mul-double/2addr p0, v0

    :pswitch_1
    sget-object v0, Lorg/javia/arity/MoreMath;->FACT:[D

    shr-int/lit8 v1, v2, 0x3

    aget-wide v0, v0, v1

    mul-double/2addr v0, p0

    goto :goto_0

    :pswitch_2
    sget-object v0, Lorg/javia/arity/MoreMath;->FACT:[D

    shr-int/lit8 v1, v2, 0x3

    aget-wide v0, v0, v1

    goto :goto_0

    :pswitch_3
    move-wide v0, p0

    goto :goto_1

    :pswitch_4
    move-wide v0, p0

    goto :goto_2

    :pswitch_5
    move-wide v0, p0

    goto :goto_3

    :pswitch_6
    move-wide v0, p0

    goto :goto_4

    :pswitch_7
    move-wide v0, p0

    goto :goto_5

    nop

    :pswitch_data_0
    .packed-switch 0x0
        :pswitch_2
        :pswitch_1
        :pswitch_7
        :pswitch_6
        :pswitch_5
        :pswitch_4
        :pswitch_3
        :pswitch_0
    .end packed-switch
.end method

.method public static final gcd(DD)D
    .locals 8

    invoke-static {p0, p1}, Ljava/lang/Double;->isNaN(D)Z

    move-result v0

    if-nez v0, :cond_0

    invoke-static {p2, p3}, Ljava/lang/Double;->isNaN(D)Z

    move-result v0

    if-nez v0, :cond_0

    invoke-static {p0, p1}, Ljava/lang/Double;->isInfinite(D)Z

    move-result v0

    if-nez v0, :cond_0

    invoke-static {p2, p3}, Ljava/lang/Double;->isInfinite(D)Z

    move-result v0

    if-eqz v0, :cond_2

    :cond_0
    const-wide/high16 v0, 0x7ff8000000000000L    # NaN

    :cond_1
    return-wide v0

    :cond_2
    invoke-static {p0, p1}, Ljava/lang/Math;->abs(D)D

    move-result-wide v2

    invoke-static {p2, p3}, Ljava/lang/Math;->abs(D)D

    move-result-wide v0

    move-wide v6, v0

    move-wide v0, v2

    move-wide v2, v6

    :goto_0
    const-wide v4, 0x430c6bf526340000L    # 1.0E15

    mul-double/2addr v4, v2

    cmpg-double v4, v0, v4

    if-gez v4, :cond_1

    rem-double/2addr v0, v2

    move-wide v6, v0

    move-wide v0, v2

    move-wide v2, v6

    goto :goto_0
.end method

.method private static final isPiMultiple(D)Z
    .locals 4

    const-wide v0, 0x400921fb54442d18L    # Math.PI

    div-double v0, p0, v0

    invoke-static {v0, v1}, Ljava/lang/Math;->floor(D)D

    move-result-wide v2

    cmpl-double v0, v0, v2

    if-nez v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public static final lgamma(D)D
    .locals 9

    const-wide v0, 0x4014f80000000000L    # 5.2421875

    add-double v3, p0, v0

    const-wide v1, 0x3fefffffffffffe6L    # 0.9999999999999971

    const/4 v0, 0x0

    :goto_0
    sget-object v5, Lorg/javia/arity/MoreMath;->GAMMA:[D

    array-length v5, v5

    if-ge v0, v5, :cond_0

    sget-object v5, Lorg/javia/arity/MoreMath;->GAMMA:[D

    aget-wide v5, v5, v0

    const-wide/high16 v7, 0x3ff0000000000000L    # 1.0

    add-double/2addr p0, v7

    div-double/2addr v5, p0

    add-double/2addr v1, v5

    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    :cond_0
    const-wide v5, 0x3fed67f1c864beb5L    # 0.9189385332046728

    invoke-static {v1, v2}, Ljava/lang/Math;->log(D)D

    move-result-wide v0

    add-double/2addr v0, v5

    const-wide v5, 0x4012f80000000000L    # 4.7421875

    sub-double v5, v3, v5

    invoke-static {v3, v4}, Ljava/lang/Math;->log(D)D

    move-result-wide v7

    mul-double/2addr v5, v7

    add-double/2addr v0, v5

    sub-double/2addr v0, v3

    return-wide v0
.end method

.method public static final permutations(DD)D
    .locals 8

    const-wide v5, 0x4065400000000000L    # 170.0

    const-wide/high16 v2, 0x3ff0000000000000L    # 1.0

    const-wide/16 v0, 0x0

    cmpg-double v4, p0, v0

    if-ltz v4, :cond_0

    cmpg-double v4, p2, v0

    if-gez v4, :cond_2

    :cond_0
    const-wide/high16 v0, 0x7ff8000000000000L    # NaN

    :cond_1
    :goto_0
    return-wide v0

    :cond_2
    cmpg-double v4, p0, p2

    if-ltz v4, :cond_1

    invoke-static {p0, p1}, Ljava/lang/Math;->floor(D)D

    move-result-wide v0

    cmpl-double v0, v0, p0

    if-nez v0, :cond_4

    invoke-static {p2, p3}, Ljava/lang/Math;->floor(D)D

    move-result-wide v0

    cmpl-double v0, v0, p2

    if-nez v0, :cond_4

    cmpg-double v0, p0, v5

    if-gtz v0, :cond_3

    const-wide/high16 v0, 0x4024000000000000L    # 10.0

    cmpg-double v0, v0, p2

    if-gez v0, :cond_3

    cmpg-double v0, p2, v5

    if-gtz v0, :cond_3

    invoke-static {p0, p1}, Lorg/javia/arity/MoreMath;->factorial(D)D

    move-result-wide v0

    sub-double v2, p0, p2

    invoke-static {v2, v3}, Lorg/javia/arity/MoreMath;->factorial(D)D

    move-result-wide v2

    div-double/2addr v0, v2

    goto :goto_0

    :cond_3
    sub-double v0, p0, p2

    const-wide/high16 v4, 0x3fe0000000000000L    # 0.5

    add-double/2addr v4, v0

    move-wide v0, v2

    :goto_1
    cmpl-double v6, p0, v4

    if-lez v6, :cond_1

    const-wide/high16 v6, 0x7ff0000000000000L    # Double.POSITIVE_INFINITY

    cmpg-double v6, v0, v6

    if-gez v6, :cond_1

    mul-double/2addr v0, p0

    sub-double/2addr p0, v2

    goto :goto_1

    :cond_4
    invoke-static {p0, p1}, Lorg/javia/arity/MoreMath;->lgamma(D)D

    move-result-wide v0

    sub-double v2, p0, p2

    invoke-static {v2, v3}, Lorg/javia/arity/MoreMath;->lgamma(D)D

    move-result-wide v2

    sub-double/2addr v0, v2

    invoke-static {v0, v1}, Ljava/lang/Math;->exp(D)D

    move-result-wide v0

    goto :goto_0
.end method

.method public static final sin(D)D
    .locals 2

    invoke-static {p0, p1}, Lorg/javia/arity/MoreMath;->isPiMultiple(D)Z

    move-result v0

    if-eqz v0, :cond_0

    const-wide/16 v0, 0x0

    :goto_0
    return-wide v0

    :cond_0
    invoke-static {p0, p1}, Ljava/lang/Math;->sin(D)D

    move-result-wide v0

    goto :goto_0
.end method

.method public static final tan(D)D
    .locals 2

    invoke-static {p0, p1}, Lorg/javia/arity/MoreMath;->isPiMultiple(D)Z

    move-result v0

    if-eqz v0, :cond_0

    const-wide/16 v0, 0x0

    :goto_0
    return-wide v0

    :cond_0
    invoke-static {p0, p1}, Ljava/lang/Math;->tan(D)D

    move-result-wide v0

    goto :goto_0
.end method
