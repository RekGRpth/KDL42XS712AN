.class Lorg/javia/arity/OptCodeGen;
.super Lorg/javia/arity/SimpleCodeGen;
.source "OptCodeGen.java"


# instance fields
.field context:Lorg/javia/arity/EvalContext;

.field intrinsicArity:I

.field private isPercent:Z

.field sp:I

.field stack:[Lorg/javia/arity/Complex;

.field traceCode:[B

.field traceConstsIm:[D

.field traceConstsRe:[D

.field traceFuncs:[Lorg/javia/arity/Function;

.field tracer:Lorg/javia/arity/CompiledFunction;


# direct methods
.method constructor <init>(Lorg/javia/arity/SyntaxException;)V
    .locals 6

    const/4 v1, 0x1

    invoke-direct {p0, p1}, Lorg/javia/arity/SimpleCodeGen;-><init>(Lorg/javia/arity/SyntaxException;)V

    new-instance v0, Lorg/javia/arity/EvalContext;

    invoke-direct {v0}, Lorg/javia/arity/EvalContext;-><init>()V

    iput-object v0, p0, Lorg/javia/arity/OptCodeGen;->context:Lorg/javia/arity/EvalContext;

    iget-object v0, p0, Lorg/javia/arity/OptCodeGen;->context:Lorg/javia/arity/EvalContext;

    iget-object v0, v0, Lorg/javia/arity/EvalContext;->stackComplex:[Lorg/javia/arity/Complex;

    iput-object v0, p0, Lorg/javia/arity/OptCodeGen;->stack:[Lorg/javia/arity/Complex;

    new-array v0, v1, [D

    iput-object v0, p0, Lorg/javia/arity/OptCodeGen;->traceConstsRe:[D

    new-array v0, v1, [D

    iput-object v0, p0, Lorg/javia/arity/OptCodeGen;->traceConstsIm:[D

    new-array v0, v1, [Lorg/javia/arity/Function;

    iput-object v0, p0, Lorg/javia/arity/OptCodeGen;->traceFuncs:[Lorg/javia/arity/Function;

    new-array v0, v1, [B

    iput-object v0, p0, Lorg/javia/arity/OptCodeGen;->traceCode:[B

    new-instance v0, Lorg/javia/arity/CompiledFunction;

    const/4 v1, 0x0

    iget-object v2, p0, Lorg/javia/arity/OptCodeGen;->traceCode:[B

    iget-object v3, p0, Lorg/javia/arity/OptCodeGen;->traceConstsRe:[D

    iget-object v4, p0, Lorg/javia/arity/OptCodeGen;->traceConstsIm:[D

    iget-object v5, p0, Lorg/javia/arity/OptCodeGen;->traceFuncs:[Lorg/javia/arity/Function;

    invoke-direct/range {v0 .. v5}, Lorg/javia/arity/CompiledFunction;-><init>(I[B[D[D[Lorg/javia/arity/Function;)V

    iput-object v0, p0, Lorg/javia/arity/OptCodeGen;->tracer:Lorg/javia/arity/CompiledFunction;

    return-void
.end method


# virtual methods
.method getFun(I)Lorg/javia/arity/CompiledFunction;
    .locals 6

    new-instance v0, Lorg/javia/arity/CompiledFunction;

    iget-object v1, p0, Lorg/javia/arity/OptCodeGen;->code:Lorg/javia/arity/ByteStack;

    invoke-virtual {v1}, Lorg/javia/arity/ByteStack;->toArray()[B

    move-result-object v2

    iget-object v1, p0, Lorg/javia/arity/OptCodeGen;->consts:Lorg/javia/arity/DoubleStack;

    invoke-virtual {v1}, Lorg/javia/arity/DoubleStack;->getRe()[D

    move-result-object v3

    iget-object v1, p0, Lorg/javia/arity/OptCodeGen;->consts:Lorg/javia/arity/DoubleStack;

    invoke-virtual {v1}, Lorg/javia/arity/DoubleStack;->getIm()[D

    move-result-object v4

    iget-object v1, p0, Lorg/javia/arity/OptCodeGen;->funcs:Lorg/javia/arity/FunctionStack;

    invoke-virtual {v1}, Lorg/javia/arity/FunctionStack;->toArray()[Lorg/javia/arity/Function;

    move-result-object v5

    move v1, p1

    invoke-direct/range {v0 .. v5}, Lorg/javia/arity/CompiledFunction;-><init>(I[B[D[D[Lorg/javia/arity/Function;)V

    return-object v0
.end method

.method push(Lorg/javia/arity/Token;)V
    .locals 12
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lorg/javia/arity/SyntaxException;
        }
    .end annotation

    const-wide/high16 v10, 0x7ff8000000000000L    # NaN

    const-wide/16 v8, 0x0

    const/4 v2, 0x2

    const/4 v1, 0x1

    const/4 v7, 0x0

    iget-boolean v3, p0, Lorg/javia/arity/OptCodeGen;->isPercent:Z

    iput-boolean v7, p0, Lorg/javia/arity/OptCodeGen;->isPercent:Z

    iget v0, p1, Lorg/javia/arity/Token;->id:I

    packed-switch v0, :pswitch_data_0

    iget-byte v0, p1, Lorg/javia/arity/Token;->vmop:B

    if-gtz v0, :cond_6

    new-instance v1, Ljava/lang/Error;

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "wrong vmop: "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-direct {v1, v0}, Ljava/lang/Error;-><init>(Ljava/lang/String;)V

    throw v1

    :pswitch_0
    iget-object v0, p0, Lorg/javia/arity/OptCodeGen;->traceConstsRe:[D

    iget-wide v4, p1, Lorg/javia/arity/Token;->value:D

    aput-wide v4, v0, v7

    iget-object v0, p0, Lorg/javia/arity/OptCodeGen;->traceConstsIm:[D

    aput-wide v8, v0, v7

    move v0, v1

    :cond_0
    :goto_0
    iget v4, p0, Lorg/javia/arity/OptCodeGen;->sp:I

    iget-object v4, p0, Lorg/javia/arity/OptCodeGen;->traceCode:[B

    aput-byte v0, v4, v7

    const/16 v4, 0x8

    if-eq v0, v4, :cond_8

    iget-object v4, p0, Lorg/javia/arity/OptCodeGen;->tracer:Lorg/javia/arity/CompiledFunction;

    iget-object v5, p0, Lorg/javia/arity/OptCodeGen;->context:Lorg/javia/arity/EvalContext;

    iget v6, p0, Lorg/javia/arity/OptCodeGen;->sp:I

    if-eqz v3, :cond_7

    const/4 v3, -0x1

    :goto_1
    invoke-virtual {v4, v5, v6, v3}, Lorg/javia/arity/CompiledFunction;->execWithoutCheckComplex(Lorg/javia/arity/EvalContext;II)I

    move-result v3

    iput v3, p0, Lorg/javia/arity/OptCodeGen;->sp:I

    :goto_2
    iget-object v3, p0, Lorg/javia/arity/OptCodeGen;->stack:[Lorg/javia/arity/Complex;

    iget v4, p0, Lorg/javia/arity/OptCodeGen;->sp:I

    aget-object v3, v3, v4

    invoke-virtual {v3}, Lorg/javia/arity/Complex;->isNaN()Z

    move-result v3

    if-eqz v3, :cond_1

    if-ne v0, v1, :cond_e

    :cond_1
    if-ne v0, v2, :cond_9

    iget-object v0, p0, Lorg/javia/arity/OptCodeGen;->traceFuncs:[Lorg/javia/arity/Function;

    aget-object v0, v0, v7

    invoke-virtual {v0}, Lorg/javia/arity/Function;->arity()I

    move-result v0

    :goto_3
    if-lez v0, :cond_c

    iget-object v3, p0, Lorg/javia/arity/OptCodeGen;->code:Lorg/javia/arity/ByteStack;

    invoke-virtual {v3}, Lorg/javia/arity/ByteStack;->pop()B

    move-result v3

    if-ne v3, v1, :cond_a

    iget-object v3, p0, Lorg/javia/arity/OptCodeGen;->consts:Lorg/javia/arity/DoubleStack;

    invoke-virtual {v3}, Lorg/javia/arity/DoubleStack;->pop()V

    :goto_4
    add-int/lit8 v0, v0, -0x1

    goto :goto_3

    :pswitch_1
    invoke-virtual {p0, p1}, Lorg/javia/arity/OptCodeGen;->getSymbol(Lorg/javia/arity/Token;)Lorg/javia/arity/Symbol;

    move-result-object v0

    invoke-virtual {p1}, Lorg/javia/arity/Token;->isDerivative()Z

    move-result v4

    if-eqz v4, :cond_2

    iget-object v4, p0, Lorg/javia/arity/OptCodeGen;->traceFuncs:[Lorg/javia/arity/Function;

    iget-object v0, v0, Lorg/javia/arity/Symbol;->fun:Lorg/javia/arity/Function;

    invoke-virtual {v0}, Lorg/javia/arity/Function;->getDerivative()Lorg/javia/arity/Function;

    move-result-object v0

    aput-object v0, v4, v7

    move v0, v2

    goto :goto_0

    :cond_2
    iget-byte v4, v0, Lorg/javia/arity/Symbol;->op:B

    if-lez v4, :cond_4

    iget-byte v0, v0, Lorg/javia/arity/Symbol;->op:B

    const/16 v4, 0x26

    if-lt v0, v4, :cond_0

    const/16 v4, 0x2a

    if-gt v0, v4, :cond_0

    add-int/lit8 v1, v0, -0x26

    add-int/lit8 v2, v1, 0x1

    iget v3, p0, Lorg/javia/arity/OptCodeGen;->intrinsicArity:I

    if-le v2, v3, :cond_3

    add-int/lit8 v1, v1, 0x1

    iput v1, p0, Lorg/javia/arity/OptCodeGen;->intrinsicArity:I

    :cond_3
    iget-object v1, p0, Lorg/javia/arity/OptCodeGen;->stack:[Lorg/javia/arity/Complex;

    iget v2, p0, Lorg/javia/arity/OptCodeGen;->sp:I

    add-int/lit8 v2, v2, 0x1

    iput v2, p0, Lorg/javia/arity/OptCodeGen;->sp:I

    aget-object v1, v1, v2

    iput-wide v10, v1, Lorg/javia/arity/Complex;->re:D

    iget-object v1, p0, Lorg/javia/arity/OptCodeGen;->stack:[Lorg/javia/arity/Complex;

    iget v2, p0, Lorg/javia/arity/OptCodeGen;->sp:I

    aget-object v1, v1, v2

    iput-wide v8, v1, Lorg/javia/arity/Complex;->im:D

    iget-object v1, p0, Lorg/javia/arity/OptCodeGen;->code:Lorg/javia/arity/ByteStack;

    invoke-virtual {v1, v0}, Lorg/javia/arity/ByteStack;->push(B)V

    :goto_5
    return-void

    :cond_4
    iget-object v4, v0, Lorg/javia/arity/Symbol;->fun:Lorg/javia/arity/Function;

    if-eqz v4, :cond_5

    iget-object v4, p0, Lorg/javia/arity/OptCodeGen;->traceFuncs:[Lorg/javia/arity/Function;

    iget-object v0, v0, Lorg/javia/arity/Symbol;->fun:Lorg/javia/arity/Function;

    aput-object v0, v4, v7

    move v0, v2

    goto/16 :goto_0

    :cond_5
    iget-object v4, p0, Lorg/javia/arity/OptCodeGen;->traceConstsRe:[D

    iget-wide v5, v0, Lorg/javia/arity/Symbol;->valueRe:D

    aput-wide v5, v4, v7

    iget-object v4, p0, Lorg/javia/arity/OptCodeGen;->traceConstsIm:[D

    iget-wide v5, v0, Lorg/javia/arity/Symbol;->valueIm:D

    aput-wide v5, v4, v7

    move v0, v1

    goto/16 :goto_0

    :cond_6
    const/16 v4, 0xc

    if-ne v0, v4, :cond_0

    iput-boolean v1, p0, Lorg/javia/arity/OptCodeGen;->isPercent:Z

    goto/16 :goto_0

    :cond_7
    const/4 v3, -0x2

    goto/16 :goto_1

    :cond_8
    iget-object v3, p0, Lorg/javia/arity/OptCodeGen;->stack:[Lorg/javia/arity/Complex;

    iget v4, p0, Lorg/javia/arity/OptCodeGen;->sp:I

    add-int/lit8 v4, v4, 0x1

    iput v4, p0, Lorg/javia/arity/OptCodeGen;->sp:I

    aget-object v3, v3, v4

    iput-wide v10, v3, Lorg/javia/arity/Complex;->re:D

    iget-object v3, p0, Lorg/javia/arity/OptCodeGen;->stack:[Lorg/javia/arity/Complex;

    iget v4, p0, Lorg/javia/arity/OptCodeGen;->sp:I

    aget-object v3, v3, v4

    iput-wide v8, v3, Lorg/javia/arity/Complex;->im:D

    goto/16 :goto_2

    :cond_9
    sget-object v3, Lorg/javia/arity/VM;->arity:[B

    aget-byte v0, v3, v0

    goto/16 :goto_3

    :cond_a
    if-ne v3, v2, :cond_b

    iget-object v3, p0, Lorg/javia/arity/OptCodeGen;->funcs:Lorg/javia/arity/FunctionStack;

    invoke-virtual {v3}, Lorg/javia/arity/FunctionStack;->pop()Lorg/javia/arity/Function;

    move-result-object v3

    invoke-virtual {v3}, Lorg/javia/arity/Function;->arity()I

    move-result v3

    add-int/lit8 v3, v3, -0x1

    add-int/2addr v0, v3

    goto/16 :goto_4

    :cond_b
    sget-object v4, Lorg/javia/arity/VM;->arity:[B

    aget-byte v3, v4, v3

    add-int/2addr v0, v3

    goto/16 :goto_4

    :cond_c
    iget-object v0, p0, Lorg/javia/arity/OptCodeGen;->consts:Lorg/javia/arity/DoubleStack;

    iget-object v2, p0, Lorg/javia/arity/OptCodeGen;->stack:[Lorg/javia/arity/Complex;

    iget v3, p0, Lorg/javia/arity/OptCodeGen;->sp:I

    aget-object v2, v2, v3

    iget-wide v2, v2, Lorg/javia/arity/Complex;->re:D

    iget-object v4, p0, Lorg/javia/arity/OptCodeGen;->stack:[Lorg/javia/arity/Complex;

    iget v5, p0, Lorg/javia/arity/OptCodeGen;->sp:I

    aget-object v4, v4, v5

    iget-wide v4, v4, Lorg/javia/arity/Complex;->im:D

    invoke-virtual {v0, v2, v3, v4, v5}, Lorg/javia/arity/DoubleStack;->push(DD)V

    move v0, v1

    :cond_d
    :goto_6
    iget-object v1, p0, Lorg/javia/arity/OptCodeGen;->code:Lorg/javia/arity/ByteStack;

    invoke-virtual {v1, v0}, Lorg/javia/arity/ByteStack;->push(B)V

    goto :goto_5

    :cond_e
    if-ne v0, v2, :cond_d

    iget-object v1, p0, Lorg/javia/arity/OptCodeGen;->funcs:Lorg/javia/arity/FunctionStack;

    iget-object v2, p0, Lorg/javia/arity/OptCodeGen;->traceFuncs:[Lorg/javia/arity/Function;

    aget-object v2, v2, v7

    invoke-virtual {v1, v2}, Lorg/javia/arity/FunctionStack;->push(Lorg/javia/arity/Function;)V

    goto :goto_6

    :pswitch_data_0
    .packed-switch 0x9
        :pswitch_0
        :pswitch_1
        :pswitch_1
    .end packed-switch
.end method

.method start()V
    .locals 2

    const/4 v1, 0x0

    invoke-super {p0}, Lorg/javia/arity/SimpleCodeGen;->start()V

    const/4 v0, -0x1

    iput v0, p0, Lorg/javia/arity/OptCodeGen;->sp:I

    iput v1, p0, Lorg/javia/arity/OptCodeGen;->intrinsicArity:I

    iput-boolean v1, p0, Lorg/javia/arity/OptCodeGen;->isPercent:Z

    return-void
.end method
