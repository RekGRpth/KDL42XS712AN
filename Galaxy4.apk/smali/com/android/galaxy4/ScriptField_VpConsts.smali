.class public Lcom/android/galaxy4/ScriptField_VpConsts;
.super Landroid/renderscript/Script$FieldBase;
.source "ScriptField_VpConsts.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/android/galaxy4/ScriptField_VpConsts$Item;
    }
.end annotation


# static fields
.field private static mElementCache:Ljava/lang/ref/WeakReference;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/lang/ref/WeakReference",
            "<",
            "Landroid/renderscript/Element;",
            ">;"
        }
    .end annotation
.end field


# instance fields
.field private mIOBuffer:Landroid/renderscript/FieldPacker;

.field private mItemArray:[Lcom/android/galaxy4/ScriptField_VpConsts$Item;


# direct methods
.method static constructor <clinit>()V
    .locals 2

    new-instance v0, Ljava/lang/ref/WeakReference;

    const/4 v1, 0x0

    invoke-direct {v0, v1}, Ljava/lang/ref/WeakReference;-><init>(Ljava/lang/Object;)V

    sput-object v0, Lcom/android/galaxy4/ScriptField_VpConsts;->mElementCache:Ljava/lang/ref/WeakReference;

    return-void
.end method

.method public constructor <init>(Landroid/renderscript/RenderScript;I)V
    .locals 1
    .param p1    # Landroid/renderscript/RenderScript;
    .param p2    # I

    const/4 v0, 0x0

    invoke-direct {p0}, Landroid/renderscript/Script$FieldBase;-><init>()V

    iput-object v0, p0, Lcom/android/galaxy4/ScriptField_VpConsts;->mItemArray:[Lcom/android/galaxy4/ScriptField_VpConsts$Item;

    iput-object v0, p0, Lcom/android/galaxy4/ScriptField_VpConsts;->mIOBuffer:Landroid/renderscript/FieldPacker;

    invoke-static {p1}, Lcom/android/galaxy4/ScriptField_VpConsts;->createElement(Landroid/renderscript/RenderScript;)Landroid/renderscript/Element;

    move-result-object v0

    iput-object v0, p0, Lcom/android/galaxy4/ScriptField_VpConsts;->mElement:Landroid/renderscript/Element;

    invoke-virtual {p0, p1, p2}, Lcom/android/galaxy4/ScriptField_VpConsts;->init(Landroid/renderscript/RenderScript;I)V

    return-void
.end method

.method private copyToArray(Lcom/android/galaxy4/ScriptField_VpConsts$Item;I)V
    .locals 2
    .param p1    # Lcom/android/galaxy4/ScriptField_VpConsts$Item;
    .param p2    # I

    iget-object v0, p0, Lcom/android/galaxy4/ScriptField_VpConsts;->mIOBuffer:Landroid/renderscript/FieldPacker;

    if-nez v0, :cond_0

    new-instance v0, Landroid/renderscript/FieldPacker;

    invoke-virtual {p0}, Lcom/android/galaxy4/ScriptField_VpConsts;->getType()Landroid/renderscript/Type;

    move-result-object v1

    invoke-virtual {v1}, Landroid/renderscript/Type;->getX()I

    move-result v1

    mul-int/lit8 v1, v1, 0x44

    invoke-direct {v0, v1}, Landroid/renderscript/FieldPacker;-><init>(I)V

    iput-object v0, p0, Lcom/android/galaxy4/ScriptField_VpConsts;->mIOBuffer:Landroid/renderscript/FieldPacker;

    :cond_0
    iget-object v0, p0, Lcom/android/galaxy4/ScriptField_VpConsts;->mIOBuffer:Landroid/renderscript/FieldPacker;

    mul-int/lit8 v1, p2, 0x44

    invoke-virtual {v0, v1}, Landroid/renderscript/FieldPacker;->reset(I)V

    iget-object v0, p0, Lcom/android/galaxy4/ScriptField_VpConsts;->mIOBuffer:Landroid/renderscript/FieldPacker;

    invoke-direct {p0, p1, v0}, Lcom/android/galaxy4/ScriptField_VpConsts;->copyToArrayLocal(Lcom/android/galaxy4/ScriptField_VpConsts$Item;Landroid/renderscript/FieldPacker;)V

    return-void
.end method

.method private copyToArrayLocal(Lcom/android/galaxy4/ScriptField_VpConsts$Item;Landroid/renderscript/FieldPacker;)V
    .locals 1
    .param p1    # Lcom/android/galaxy4/ScriptField_VpConsts$Item;
    .param p2    # Landroid/renderscript/FieldPacker;

    iget-object v0, p1, Lcom/android/galaxy4/ScriptField_VpConsts$Item;->MVP:Landroid/renderscript/Matrix4f;

    invoke-virtual {p2, v0}, Landroid/renderscript/FieldPacker;->addMatrix(Landroid/renderscript/Matrix4f;)V

    iget v0, p1, Lcom/android/galaxy4/ScriptField_VpConsts$Item;->scaleSize:F

    invoke-virtual {p2, v0}, Landroid/renderscript/FieldPacker;->addF32(F)V

    return-void
.end method

.method public static createElement(Landroid/renderscript/RenderScript;)Landroid/renderscript/Element;
    .locals 3
    .param p0    # Landroid/renderscript/RenderScript;

    new-instance v0, Landroid/renderscript/Element$Builder;

    invoke-direct {v0, p0}, Landroid/renderscript/Element$Builder;-><init>(Landroid/renderscript/RenderScript;)V

    invoke-static {p0}, Landroid/renderscript/Element;->MATRIX_4X4(Landroid/renderscript/RenderScript;)Landroid/renderscript/Element;

    move-result-object v1

    const-string v2, "MVP"

    invoke-virtual {v0, v1, v2}, Landroid/renderscript/Element$Builder;->add(Landroid/renderscript/Element;Ljava/lang/String;)Landroid/renderscript/Element$Builder;

    invoke-static {p0}, Landroid/renderscript/Element;->F32(Landroid/renderscript/RenderScript;)Landroid/renderscript/Element;

    move-result-object v1

    const-string v2, "scaleSize"

    invoke-virtual {v0, v1, v2}, Landroid/renderscript/Element$Builder;->add(Landroid/renderscript/Element;Ljava/lang/String;)Landroid/renderscript/Element$Builder;

    invoke-virtual {v0}, Landroid/renderscript/Element$Builder;->create()Landroid/renderscript/Element;

    move-result-object v1

    return-object v1
.end method


# virtual methods
.method public declared-synchronized set(Lcom/android/galaxy4/ScriptField_VpConsts$Item;IZ)V
    .locals 2
    .param p1    # Lcom/android/galaxy4/ScriptField_VpConsts$Item;
    .param p2    # I
    .param p3    # Z

    monitor-enter p0

    :try_start_0
    iget-object v1, p0, Lcom/android/galaxy4/ScriptField_VpConsts;->mItemArray:[Lcom/android/galaxy4/ScriptField_VpConsts$Item;

    if-nez v1, :cond_0

    invoke-virtual {p0}, Lcom/android/galaxy4/ScriptField_VpConsts;->getType()Landroid/renderscript/Type;

    move-result-object v1

    invoke-virtual {v1}, Landroid/renderscript/Type;->getX()I

    move-result v1

    new-array v1, v1, [Lcom/android/galaxy4/ScriptField_VpConsts$Item;

    iput-object v1, p0, Lcom/android/galaxy4/ScriptField_VpConsts;->mItemArray:[Lcom/android/galaxy4/ScriptField_VpConsts$Item;

    :cond_0
    iget-object v1, p0, Lcom/android/galaxy4/ScriptField_VpConsts;->mItemArray:[Lcom/android/galaxy4/ScriptField_VpConsts$Item;

    aput-object p1, v1, p2

    if-eqz p3, :cond_1

    invoke-direct {p0, p1, p2}, Lcom/android/galaxy4/ScriptField_VpConsts;->copyToArray(Lcom/android/galaxy4/ScriptField_VpConsts$Item;I)V

    new-instance v0, Landroid/renderscript/FieldPacker;

    const/16 v1, 0x44

    invoke-direct {v0, v1}, Landroid/renderscript/FieldPacker;-><init>(I)V

    invoke-direct {p0, p1, v0}, Lcom/android/galaxy4/ScriptField_VpConsts;->copyToArrayLocal(Lcom/android/galaxy4/ScriptField_VpConsts$Item;Landroid/renderscript/FieldPacker;)V

    iget-object v1, p0, Lcom/android/galaxy4/ScriptField_VpConsts;->mAllocation:Landroid/renderscript/Allocation;

    invoke-virtual {v1, p2, v0}, Landroid/renderscript/Allocation;->setFromFieldPacker(ILandroid/renderscript/FieldPacker;)V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    :cond_1
    monitor-exit p0

    return-void

    :catchall_0
    move-exception v1

    monitor-exit p0

    throw v1
.end method
