.class public Lcom/android/galaxy4/ScriptC_galaxy;
.super Landroid/renderscript/ScriptC;
.source "ScriptC_galaxy.java"


# instance fields
.field private __ALLOCATION:Landroid/renderscript/Element;

.field private __F32:Landroid/renderscript/Element;

.field private __MESH:Landroid/renderscript/Element;

.field private __PROGRAM_FRAGMENT:Landroid/renderscript/Element;

.field private __PROGRAM_VERTEX:Landroid/renderscript/Element;

.field private mExportVar_bgStars:Lcom/android/galaxy4/ScriptField_Particle;

.field private mExportVar_bgStarsMesh:Landroid/renderscript/Mesh;

.field private mExportVar_densityDPI:F

.field private mExportVar_fragBg:Landroid/renderscript/ProgramFragment;

.field private mExportVar_fragBgStars:Landroid/renderscript/ProgramFragment;

.field private mExportVar_fragSpaceClouds:Landroid/renderscript/ProgramFragment;

.field private mExportVar_fragStaticStars:Landroid/renderscript/ProgramFragment;

.field private mExportVar_spaceClouds:Lcom/android/galaxy4/ScriptField_Particle;

.field private mExportVar_spaceCloudsMesh:Landroid/renderscript/Mesh;

.field private mExportVar_staticStars:Lcom/android/galaxy4/ScriptField_Particle;

.field private mExportVar_staticStarsMesh:Landroid/renderscript/Mesh;

.field private mExportVar_textureBg:Landroid/renderscript/Allocation;

.field private mExportVar_textureSpaceCloud:Landroid/renderscript/Allocation;

.field private mExportVar_textureStaticStar:Landroid/renderscript/Allocation;

.field private mExportVar_textureStaticStar2:Landroid/renderscript/Allocation;

.field private mExportVar_vertBg:Landroid/renderscript/ProgramVertex;

.field private mExportVar_vertBgStars:Landroid/renderscript/ProgramVertex;

.field private mExportVar_vertSpaceClouds:Landroid/renderscript/ProgramVertex;

.field private mExportVar_vertStaticStars:Landroid/renderscript/ProgramVertex;


# direct methods
.method public constructor <init>(Landroid/renderscript/RenderScript;Landroid/content/res/Resources;I)V
    .locals 1
    .param p1    # Landroid/renderscript/RenderScript;
    .param p2    # Landroid/content/res/Resources;
    .param p3    # I

    invoke-direct {p0, p1, p2, p3}, Landroid/renderscript/ScriptC;-><init>(Landroid/renderscript/RenderScript;Landroid/content/res/Resources;I)V

    invoke-static {p1}, Landroid/renderscript/Element;->MESH(Landroid/renderscript/RenderScript;)Landroid/renderscript/Element;

    move-result-object v0

    iput-object v0, p0, Lcom/android/galaxy4/ScriptC_galaxy;->__MESH:Landroid/renderscript/Element;

    invoke-static {p1}, Landroid/renderscript/Element;->PROGRAM_VERTEX(Landroid/renderscript/RenderScript;)Landroid/renderscript/Element;

    move-result-object v0

    iput-object v0, p0, Lcom/android/galaxy4/ScriptC_galaxy;->__PROGRAM_VERTEX:Landroid/renderscript/Element;

    invoke-static {p1}, Landroid/renderscript/Element;->PROGRAM_FRAGMENT(Landroid/renderscript/RenderScript;)Landroid/renderscript/Element;

    move-result-object v0

    iput-object v0, p0, Lcom/android/galaxy4/ScriptC_galaxy;->__PROGRAM_FRAGMENT:Landroid/renderscript/Element;

    invoke-static {p1}, Landroid/renderscript/Element;->ALLOCATION(Landroid/renderscript/RenderScript;)Landroid/renderscript/Element;

    move-result-object v0

    iput-object v0, p0, Lcom/android/galaxy4/ScriptC_galaxy;->__ALLOCATION:Landroid/renderscript/Element;

    invoke-static {p1}, Landroid/renderscript/Element;->F32(Landroid/renderscript/RenderScript;)Landroid/renderscript/Element;

    move-result-object v0

    iput-object v0, p0, Lcom/android/galaxy4/ScriptC_galaxy;->__F32:Landroid/renderscript/Element;

    return-void
.end method


# virtual methods
.method public bind_bgStars(Lcom/android/galaxy4/ScriptField_Particle;)V
    .locals 2
    .param p1    # Lcom/android/galaxy4/ScriptField_Particle;

    const/4 v1, 0x2

    iput-object p1, p0, Lcom/android/galaxy4/ScriptC_galaxy;->mExportVar_bgStars:Lcom/android/galaxy4/ScriptField_Particle;

    if-nez p1, :cond_0

    const/4 v0, 0x0

    invoke-virtual {p0, v0, v1}, Lcom/android/galaxy4/ScriptC_galaxy;->bindAllocation(Landroid/renderscript/Allocation;I)V

    :goto_0
    return-void

    :cond_0
    invoke-virtual {p1}, Lcom/android/galaxy4/ScriptField_Particle;->getAllocation()Landroid/renderscript/Allocation;

    move-result-object v0

    invoke-virtual {p0, v0, v1}, Lcom/android/galaxy4/ScriptC_galaxy;->bindAllocation(Landroid/renderscript/Allocation;I)V

    goto :goto_0
.end method

.method public bind_spaceClouds(Lcom/android/galaxy4/ScriptField_Particle;)V
    .locals 2
    .param p1    # Lcom/android/galaxy4/ScriptField_Particle;

    const/4 v1, 0x1

    iput-object p1, p0, Lcom/android/galaxy4/ScriptC_galaxy;->mExportVar_spaceClouds:Lcom/android/galaxy4/ScriptField_Particle;

    if-nez p1, :cond_0

    const/4 v0, 0x0

    invoke-virtual {p0, v0, v1}, Lcom/android/galaxy4/ScriptC_galaxy;->bindAllocation(Landroid/renderscript/Allocation;I)V

    :goto_0
    return-void

    :cond_0
    invoke-virtual {p1}, Lcom/android/galaxy4/ScriptField_Particle;->getAllocation()Landroid/renderscript/Allocation;

    move-result-object v0

    invoke-virtual {p0, v0, v1}, Lcom/android/galaxy4/ScriptC_galaxy;->bindAllocation(Landroid/renderscript/Allocation;I)V

    goto :goto_0
.end method

.method public bind_staticStars(Lcom/android/galaxy4/ScriptField_Particle;)V
    .locals 2
    .param p1    # Lcom/android/galaxy4/ScriptField_Particle;

    const/4 v1, 0x3

    iput-object p1, p0, Lcom/android/galaxy4/ScriptC_galaxy;->mExportVar_staticStars:Lcom/android/galaxy4/ScriptField_Particle;

    if-nez p1, :cond_0

    const/4 v0, 0x0

    invoke-virtual {p0, v0, v1}, Lcom/android/galaxy4/ScriptC_galaxy;->bindAllocation(Landroid/renderscript/Allocation;I)V

    :goto_0
    return-void

    :cond_0
    invoke-virtual {p1}, Lcom/android/galaxy4/ScriptField_Particle;->getAllocation()Landroid/renderscript/Allocation;

    move-result-object v0

    invoke-virtual {p0, v0, v1}, Lcom/android/galaxy4/ScriptC_galaxy;->bindAllocation(Landroid/renderscript/Allocation;I)V

    goto :goto_0
.end method

.method public invoke_positionParticles()V
    .locals 1

    const/4 v0, 0x0

    invoke-virtual {p0, v0}, Lcom/android/galaxy4/ScriptC_galaxy;->invoke(I)V

    return-void
.end method

.method public declared-synchronized set_bgStarsMesh(Landroid/renderscript/Mesh;)V
    .locals 1
    .param p1    # Landroid/renderscript/Mesh;

    monitor-enter p0

    const/4 v0, 0x5

    :try_start_0
    invoke-virtual {p0, v0, p1}, Lcom/android/galaxy4/ScriptC_galaxy;->setVar(ILandroid/renderscript/BaseObj;)V

    iput-object p1, p0, Lcom/android/galaxy4/ScriptC_galaxy;->mExportVar_bgStarsMesh:Landroid/renderscript/Mesh;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    monitor-exit p0

    return-void

    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method public declared-synchronized set_densityDPI(F)V
    .locals 1
    .param p1    # F

    monitor-enter p0

    const/16 v0, 0x13

    :try_start_0
    invoke-virtual {p0, v0, p1}, Lcom/android/galaxy4/ScriptC_galaxy;->setVar(IF)V

    iput p1, p0, Lcom/android/galaxy4/ScriptC_galaxy;->mExportVar_densityDPI:F
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    monitor-exit p0

    return-void

    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method public declared-synchronized set_fragBg(Landroid/renderscript/ProgramFragment;)V
    .locals 1
    .param p1    # Landroid/renderscript/ProgramFragment;

    monitor-enter p0

    const/16 v0, 0xe

    :try_start_0
    invoke-virtual {p0, v0, p1}, Lcom/android/galaxy4/ScriptC_galaxy;->setVar(ILandroid/renderscript/BaseObj;)V

    iput-object p1, p0, Lcom/android/galaxy4/ScriptC_galaxy;->mExportVar_fragBg:Landroid/renderscript/ProgramFragment;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    monitor-exit p0

    return-void

    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method public declared-synchronized set_fragBgStars(Landroid/renderscript/ProgramFragment;)V
    .locals 1
    .param p1    # Landroid/renderscript/ProgramFragment;

    monitor-enter p0

    const/16 v0, 0xb

    :try_start_0
    invoke-virtual {p0, v0, p1}, Lcom/android/galaxy4/ScriptC_galaxy;->setVar(ILandroid/renderscript/BaseObj;)V

    iput-object p1, p0, Lcom/android/galaxy4/ScriptC_galaxy;->mExportVar_fragBgStars:Landroid/renderscript/ProgramFragment;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    monitor-exit p0

    return-void

    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method public declared-synchronized set_fragSpaceClouds(Landroid/renderscript/ProgramFragment;)V
    .locals 1
    .param p1    # Landroid/renderscript/ProgramFragment;

    monitor-enter p0

    const/16 v0, 0xa

    :try_start_0
    invoke-virtual {p0, v0, p1}, Lcom/android/galaxy4/ScriptC_galaxy;->setVar(ILandroid/renderscript/BaseObj;)V

    iput-object p1, p0, Lcom/android/galaxy4/ScriptC_galaxy;->mExportVar_fragSpaceClouds:Landroid/renderscript/ProgramFragment;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    monitor-exit p0

    return-void

    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method public declared-synchronized set_fragStaticStars(Landroid/renderscript/ProgramFragment;)V
    .locals 1
    .param p1    # Landroid/renderscript/ProgramFragment;

    monitor-enter p0

    const/16 v0, 0xc

    :try_start_0
    invoke-virtual {p0, v0, p1}, Lcom/android/galaxy4/ScriptC_galaxy;->setVar(ILandroid/renderscript/BaseObj;)V

    iput-object p1, p0, Lcom/android/galaxy4/ScriptC_galaxy;->mExportVar_fragStaticStars:Landroid/renderscript/ProgramFragment;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    monitor-exit p0

    return-void

    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method public declared-synchronized set_spaceCloudsMesh(Landroid/renderscript/Mesh;)V
    .locals 1
    .param p1    # Landroid/renderscript/Mesh;

    monitor-enter p0

    const/4 v0, 0x4

    :try_start_0
    invoke-virtual {p0, v0, p1}, Lcom/android/galaxy4/ScriptC_galaxy;->setVar(ILandroid/renderscript/BaseObj;)V

    iput-object p1, p0, Lcom/android/galaxy4/ScriptC_galaxy;->mExportVar_spaceCloudsMesh:Landroid/renderscript/Mesh;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    monitor-exit p0

    return-void

    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method public declared-synchronized set_staticStarsMesh(Landroid/renderscript/Mesh;)V
    .locals 1
    .param p1    # Landroid/renderscript/Mesh;

    monitor-enter p0

    const/4 v0, 0x6

    :try_start_0
    invoke-virtual {p0, v0, p1}, Lcom/android/galaxy4/ScriptC_galaxy;->setVar(ILandroid/renderscript/BaseObj;)V

    iput-object p1, p0, Lcom/android/galaxy4/ScriptC_galaxy;->mExportVar_staticStarsMesh:Landroid/renderscript/Mesh;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    monitor-exit p0

    return-void

    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method public declared-synchronized set_textureBg(Landroid/renderscript/Allocation;)V
    .locals 1
    .param p1    # Landroid/renderscript/Allocation;

    monitor-enter p0

    const/16 v0, 0x12

    :try_start_0
    invoke-virtual {p0, v0, p1}, Lcom/android/galaxy4/ScriptC_galaxy;->setVar(ILandroid/renderscript/BaseObj;)V

    iput-object p1, p0, Lcom/android/galaxy4/ScriptC_galaxy;->mExportVar_textureBg:Landroid/renderscript/Allocation;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    monitor-exit p0

    return-void

    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method public declared-synchronized set_textureSpaceCloud(Landroid/renderscript/Allocation;)V
    .locals 1
    .param p1    # Landroid/renderscript/Allocation;

    monitor-enter p0

    const/16 v0, 0xf

    :try_start_0
    invoke-virtual {p0, v0, p1}, Lcom/android/galaxy4/ScriptC_galaxy;->setVar(ILandroid/renderscript/BaseObj;)V

    iput-object p1, p0, Lcom/android/galaxy4/ScriptC_galaxy;->mExportVar_textureSpaceCloud:Landroid/renderscript/Allocation;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    monitor-exit p0

    return-void

    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method public declared-synchronized set_textureStaticStar(Landroid/renderscript/Allocation;)V
    .locals 1
    .param p1    # Landroid/renderscript/Allocation;

    monitor-enter p0

    const/16 v0, 0x10

    :try_start_0
    invoke-virtual {p0, v0, p1}, Lcom/android/galaxy4/ScriptC_galaxy;->setVar(ILandroid/renderscript/BaseObj;)V

    iput-object p1, p0, Lcom/android/galaxy4/ScriptC_galaxy;->mExportVar_textureStaticStar:Landroid/renderscript/Allocation;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    monitor-exit p0

    return-void

    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method public declared-synchronized set_textureStaticStar2(Landroid/renderscript/Allocation;)V
    .locals 1
    .param p1    # Landroid/renderscript/Allocation;

    monitor-enter p0

    const/16 v0, 0x11

    :try_start_0
    invoke-virtual {p0, v0, p1}, Lcom/android/galaxy4/ScriptC_galaxy;->setVar(ILandroid/renderscript/BaseObj;)V

    iput-object p1, p0, Lcom/android/galaxy4/ScriptC_galaxy;->mExportVar_textureStaticStar2:Landroid/renderscript/Allocation;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    monitor-exit p0

    return-void

    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method public declared-synchronized set_vertBg(Landroid/renderscript/ProgramVertex;)V
    .locals 1
    .param p1    # Landroid/renderscript/ProgramVertex;

    monitor-enter p0

    const/16 v0, 0xd

    :try_start_0
    invoke-virtual {p0, v0, p1}, Lcom/android/galaxy4/ScriptC_galaxy;->setVar(ILandroid/renderscript/BaseObj;)V

    iput-object p1, p0, Lcom/android/galaxy4/ScriptC_galaxy;->mExportVar_vertBg:Landroid/renderscript/ProgramVertex;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    monitor-exit p0

    return-void

    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method public declared-synchronized set_vertBgStars(Landroid/renderscript/ProgramVertex;)V
    .locals 1
    .param p1    # Landroid/renderscript/ProgramVertex;

    monitor-enter p0

    const/16 v0, 0x8

    :try_start_0
    invoke-virtual {p0, v0, p1}, Lcom/android/galaxy4/ScriptC_galaxy;->setVar(ILandroid/renderscript/BaseObj;)V

    iput-object p1, p0, Lcom/android/galaxy4/ScriptC_galaxy;->mExportVar_vertBgStars:Landroid/renderscript/ProgramVertex;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    monitor-exit p0

    return-void

    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method public declared-synchronized set_vertSpaceClouds(Landroid/renderscript/ProgramVertex;)V
    .locals 1
    .param p1    # Landroid/renderscript/ProgramVertex;

    monitor-enter p0

    const/4 v0, 0x7

    :try_start_0
    invoke-virtual {p0, v0, p1}, Lcom/android/galaxy4/ScriptC_galaxy;->setVar(ILandroid/renderscript/BaseObj;)V

    iput-object p1, p0, Lcom/android/galaxy4/ScriptC_galaxy;->mExportVar_vertSpaceClouds:Landroid/renderscript/ProgramVertex;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    monitor-exit p0

    return-void

    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method public declared-synchronized set_vertStaticStars(Landroid/renderscript/ProgramVertex;)V
    .locals 1
    .param p1    # Landroid/renderscript/ProgramVertex;

    monitor-enter p0

    const/16 v0, 0x9

    :try_start_0
    invoke-virtual {p0, v0, p1}, Lcom/android/galaxy4/ScriptC_galaxy;->setVar(ILandroid/renderscript/BaseObj;)V

    iput-object p1, p0, Lcom/android/galaxy4/ScriptC_galaxy;->mExportVar_vertStaticStars:Landroid/renderscript/ProgramVertex;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    monitor-exit p0

    return-void

    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method
