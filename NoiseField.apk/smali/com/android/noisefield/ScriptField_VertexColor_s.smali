.class public Lcom/android/noisefield/ScriptField_VertexColor_s;
.super Landroid/renderscript/Script$FieldBase;
.source "ScriptField_VertexColor_s.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/android/noisefield/ScriptField_VertexColor_s$Item;
    }
.end annotation


# static fields
.field private static mElementCache:Ljava/lang/ref/WeakReference;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/lang/ref/WeakReference",
            "<",
            "Landroid/renderscript/Element;",
            ">;"
        }
    .end annotation
.end field


# instance fields
.field private mIOBuffer:Landroid/renderscript/FieldPacker;

.field private mItemArray:[Lcom/android/noisefield/ScriptField_VertexColor_s$Item;


# direct methods
.method static constructor <clinit>()V
    .locals 2

    new-instance v0, Ljava/lang/ref/WeakReference;

    const/4 v1, 0x0

    invoke-direct {v0, v1}, Ljava/lang/ref/WeakReference;-><init>(Ljava/lang/Object;)V

    sput-object v0, Lcom/android/noisefield/ScriptField_VertexColor_s;->mElementCache:Ljava/lang/ref/WeakReference;

    return-void
.end method

.method public constructor <init>(Landroid/renderscript/RenderScript;I)V
    .locals 1
    .param p1    # Landroid/renderscript/RenderScript;
    .param p2    # I

    const/4 v0, 0x0

    invoke-direct {p0}, Landroid/renderscript/Script$FieldBase;-><init>()V

    iput-object v0, p0, Lcom/android/noisefield/ScriptField_VertexColor_s;->mItemArray:[Lcom/android/noisefield/ScriptField_VertexColor_s$Item;

    iput-object v0, p0, Lcom/android/noisefield/ScriptField_VertexColor_s;->mIOBuffer:Landroid/renderscript/FieldPacker;

    invoke-static {p1}, Lcom/android/noisefield/ScriptField_VertexColor_s;->createElement(Landroid/renderscript/RenderScript;)Landroid/renderscript/Element;

    move-result-object v0

    iput-object v0, p0, Lcom/android/noisefield/ScriptField_VertexColor_s;->mElement:Landroid/renderscript/Element;

    invoke-virtual {p0, p1, p2}, Lcom/android/noisefield/ScriptField_VertexColor_s;->init(Landroid/renderscript/RenderScript;I)V

    return-void
.end method

.method private copyToArray(Lcom/android/noisefield/ScriptField_VertexColor_s$Item;I)V
    .locals 2
    .param p1    # Lcom/android/noisefield/ScriptField_VertexColor_s$Item;
    .param p2    # I

    iget-object v0, p0, Lcom/android/noisefield/ScriptField_VertexColor_s;->mIOBuffer:Landroid/renderscript/FieldPacker;

    if-nez v0, :cond_0

    new-instance v0, Landroid/renderscript/FieldPacker;

    invoke-virtual {p0}, Lcom/android/noisefield/ScriptField_VertexColor_s;->getType()Landroid/renderscript/Type;

    move-result-object v1

    invoke-virtual {v1}, Landroid/renderscript/Type;->getX()I

    move-result v1

    mul-int/lit8 v1, v1, 0x30

    invoke-direct {v0, v1}, Landroid/renderscript/FieldPacker;-><init>(I)V

    iput-object v0, p0, Lcom/android/noisefield/ScriptField_VertexColor_s;->mIOBuffer:Landroid/renderscript/FieldPacker;

    :cond_0
    iget-object v0, p0, Lcom/android/noisefield/ScriptField_VertexColor_s;->mIOBuffer:Landroid/renderscript/FieldPacker;

    mul-int/lit8 v1, p2, 0x30

    invoke-virtual {v0, v1}, Landroid/renderscript/FieldPacker;->reset(I)V

    iget-object v0, p0, Lcom/android/noisefield/ScriptField_VertexColor_s;->mIOBuffer:Landroid/renderscript/FieldPacker;

    invoke-direct {p0, p1, v0}, Lcom/android/noisefield/ScriptField_VertexColor_s;->copyToArrayLocal(Lcom/android/noisefield/ScriptField_VertexColor_s$Item;Landroid/renderscript/FieldPacker;)V

    return-void
.end method

.method private copyToArrayLocal(Lcom/android/noisefield/ScriptField_VertexColor_s$Item;Landroid/renderscript/FieldPacker;)V
    .locals 1
    .param p1    # Lcom/android/noisefield/ScriptField_VertexColor_s$Item;
    .param p2    # Landroid/renderscript/FieldPacker;

    iget-object v0, p1, Lcom/android/noisefield/ScriptField_VertexColor_s$Item;->position:Landroid/renderscript/Float3;

    invoke-virtual {p2, v0}, Landroid/renderscript/FieldPacker;->addF32(Landroid/renderscript/Float3;)V

    const/4 v0, 0x4

    invoke-virtual {p2, v0}, Landroid/renderscript/FieldPacker;->skip(I)V

    iget-object v0, p1, Lcom/android/noisefield/ScriptField_VertexColor_s$Item;->color:Landroid/renderscript/Float4;

    invoke-virtual {p2, v0}, Landroid/renderscript/FieldPacker;->addF32(Landroid/renderscript/Float4;)V

    iget v0, p1, Lcom/android/noisefield/ScriptField_VertexColor_s$Item;->offsetX:F

    invoke-virtual {p2, v0}, Landroid/renderscript/FieldPacker;->addF32(F)V

    const/16 v0, 0xc

    invoke-virtual {p2, v0}, Landroid/renderscript/FieldPacker;->skip(I)V

    return-void
.end method

.method public static createElement(Landroid/renderscript/RenderScript;)Landroid/renderscript/Element;
    .locals 3
    .param p0    # Landroid/renderscript/RenderScript;

    new-instance v0, Landroid/renderscript/Element$Builder;

    invoke-direct {v0, p0}, Landroid/renderscript/Element$Builder;-><init>(Landroid/renderscript/RenderScript;)V

    invoke-static {p0}, Landroid/renderscript/Element;->F32_3(Landroid/renderscript/RenderScript;)Landroid/renderscript/Element;

    move-result-object v1

    const-string v2, "position"

    invoke-virtual {v0, v1, v2}, Landroid/renderscript/Element$Builder;->add(Landroid/renderscript/Element;Ljava/lang/String;)Landroid/renderscript/Element$Builder;

    invoke-static {p0}, Landroid/renderscript/Element;->F32_4(Landroid/renderscript/RenderScript;)Landroid/renderscript/Element;

    move-result-object v1

    const-string v2, "color"

    invoke-virtual {v0, v1, v2}, Landroid/renderscript/Element$Builder;->add(Landroid/renderscript/Element;Ljava/lang/String;)Landroid/renderscript/Element$Builder;

    invoke-static {p0}, Landroid/renderscript/Element;->F32(Landroid/renderscript/RenderScript;)Landroid/renderscript/Element;

    move-result-object v1

    const-string v2, "offsetX"

    invoke-virtual {v0, v1, v2}, Landroid/renderscript/Element$Builder;->add(Landroid/renderscript/Element;Ljava/lang/String;)Landroid/renderscript/Element$Builder;

    invoke-static {p0}, Landroid/renderscript/Element;->U32(Landroid/renderscript/RenderScript;)Landroid/renderscript/Element;

    move-result-object v1

    const-string v2, "#rs_padding_1"

    invoke-virtual {v0, v1, v2}, Landroid/renderscript/Element$Builder;->add(Landroid/renderscript/Element;Ljava/lang/String;)Landroid/renderscript/Element$Builder;

    invoke-static {p0}, Landroid/renderscript/Element;->U32(Landroid/renderscript/RenderScript;)Landroid/renderscript/Element;

    move-result-object v1

    const-string v2, "#rs_padding_2"

    invoke-virtual {v0, v1, v2}, Landroid/renderscript/Element$Builder;->add(Landroid/renderscript/Element;Ljava/lang/String;)Landroid/renderscript/Element$Builder;

    invoke-static {p0}, Landroid/renderscript/Element;->U32(Landroid/renderscript/RenderScript;)Landroid/renderscript/Element;

    move-result-object v1

    const-string v2, "#rs_padding_3"

    invoke-virtual {v0, v1, v2}, Landroid/renderscript/Element$Builder;->add(Landroid/renderscript/Element;Ljava/lang/String;)Landroid/renderscript/Element$Builder;

    invoke-virtual {v0}, Landroid/renderscript/Element$Builder;->create()Landroid/renderscript/Element;

    move-result-object v1

    return-object v1
.end method


# virtual methods
.method public declared-synchronized copyAll()V
    .locals 4

    monitor-enter p0

    const/4 v0, 0x0

    :goto_0
    :try_start_0
    iget-object v1, p0, Lcom/android/noisefield/ScriptField_VertexColor_s;->mItemArray:[Lcom/android/noisefield/ScriptField_VertexColor_s$Item;

    array-length v1, v1

    if-ge v0, v1, :cond_0

    iget-object v1, p0, Lcom/android/noisefield/ScriptField_VertexColor_s;->mItemArray:[Lcom/android/noisefield/ScriptField_VertexColor_s$Item;

    aget-object v1, v1, v0

    invoke-direct {p0, v1, v0}, Lcom/android/noisefield/ScriptField_VertexColor_s;->copyToArray(Lcom/android/noisefield/ScriptField_VertexColor_s$Item;I)V

    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    :cond_0
    iget-object v1, p0, Lcom/android/noisefield/ScriptField_VertexColor_s;->mAllocation:Landroid/renderscript/Allocation;

    const/4 v2, 0x0

    iget-object v3, p0, Lcom/android/noisefield/ScriptField_VertexColor_s;->mIOBuffer:Landroid/renderscript/FieldPacker;

    invoke-virtual {v1, v2, v3}, Landroid/renderscript/Allocation;->setFromFieldPacker(ILandroid/renderscript/FieldPacker;)V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    monitor-exit p0

    return-void

    :catchall_0
    move-exception v1

    monitor-exit p0

    throw v1
.end method

.method public declared-synchronized set_color(ILandroid/renderscript/Float4;Z)V
    .locals 3
    .param p1    # I
    .param p2    # Landroid/renderscript/Float4;
    .param p3    # Z

    monitor-enter p0

    :try_start_0
    iget-object v1, p0, Lcom/android/noisefield/ScriptField_VertexColor_s;->mIOBuffer:Landroid/renderscript/FieldPacker;

    if-nez v1, :cond_0

    new-instance v1, Landroid/renderscript/FieldPacker;

    invoke-virtual {p0}, Lcom/android/noisefield/ScriptField_VertexColor_s;->getType()Landroid/renderscript/Type;

    move-result-object v2

    invoke-virtual {v2}, Landroid/renderscript/Type;->getX()I

    move-result v2

    mul-int/lit8 v2, v2, 0x30

    invoke-direct {v1, v2}, Landroid/renderscript/FieldPacker;-><init>(I)V

    iput-object v1, p0, Lcom/android/noisefield/ScriptField_VertexColor_s;->mIOBuffer:Landroid/renderscript/FieldPacker;

    :cond_0
    iget-object v1, p0, Lcom/android/noisefield/ScriptField_VertexColor_s;->mItemArray:[Lcom/android/noisefield/ScriptField_VertexColor_s$Item;

    if-nez v1, :cond_1

    invoke-virtual {p0}, Lcom/android/noisefield/ScriptField_VertexColor_s;->getType()Landroid/renderscript/Type;

    move-result-object v1

    invoke-virtual {v1}, Landroid/renderscript/Type;->getX()I

    move-result v1

    new-array v1, v1, [Lcom/android/noisefield/ScriptField_VertexColor_s$Item;

    iput-object v1, p0, Lcom/android/noisefield/ScriptField_VertexColor_s;->mItemArray:[Lcom/android/noisefield/ScriptField_VertexColor_s$Item;

    :cond_1
    iget-object v1, p0, Lcom/android/noisefield/ScriptField_VertexColor_s;->mItemArray:[Lcom/android/noisefield/ScriptField_VertexColor_s$Item;

    aget-object v1, v1, p1

    if-nez v1, :cond_2

    iget-object v1, p0, Lcom/android/noisefield/ScriptField_VertexColor_s;->mItemArray:[Lcom/android/noisefield/ScriptField_VertexColor_s$Item;

    new-instance v2, Lcom/android/noisefield/ScriptField_VertexColor_s$Item;

    invoke-direct {v2}, Lcom/android/noisefield/ScriptField_VertexColor_s$Item;-><init>()V

    aput-object v2, v1, p1

    :cond_2
    iget-object v1, p0, Lcom/android/noisefield/ScriptField_VertexColor_s;->mItemArray:[Lcom/android/noisefield/ScriptField_VertexColor_s$Item;

    aget-object v1, v1, p1

    iput-object p2, v1, Lcom/android/noisefield/ScriptField_VertexColor_s$Item;->color:Landroid/renderscript/Float4;

    if-eqz p3, :cond_3

    iget-object v1, p0, Lcom/android/noisefield/ScriptField_VertexColor_s;->mIOBuffer:Landroid/renderscript/FieldPacker;

    mul-int/lit8 v2, p1, 0x30

    add-int/lit8 v2, v2, 0x10

    invoke-virtual {v1, v2}, Landroid/renderscript/FieldPacker;->reset(I)V

    iget-object v1, p0, Lcom/android/noisefield/ScriptField_VertexColor_s;->mIOBuffer:Landroid/renderscript/FieldPacker;

    invoke-virtual {v1, p2}, Landroid/renderscript/FieldPacker;->addF32(Landroid/renderscript/Float4;)V

    new-instance v0, Landroid/renderscript/FieldPacker;

    const/16 v1, 0x10

    invoke-direct {v0, v1}, Landroid/renderscript/FieldPacker;-><init>(I)V

    invoke-virtual {v0, p2}, Landroid/renderscript/FieldPacker;->addF32(Landroid/renderscript/Float4;)V

    iget-object v1, p0, Lcom/android/noisefield/ScriptField_VertexColor_s;->mAllocation:Landroid/renderscript/Allocation;

    const/4 v2, 0x1

    invoke-virtual {v1, p1, v2, v0}, Landroid/renderscript/Allocation;->setFromFieldPacker(IILandroid/renderscript/FieldPacker;)V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    :cond_3
    monitor-exit p0

    return-void

    :catchall_0
    move-exception v1

    monitor-exit p0

    throw v1
.end method

.method public declared-synchronized set_position(ILandroid/renderscript/Float3;Z)V
    .locals 3
    .param p1    # I
    .param p2    # Landroid/renderscript/Float3;
    .param p3    # Z

    monitor-enter p0

    :try_start_0
    iget-object v1, p0, Lcom/android/noisefield/ScriptField_VertexColor_s;->mIOBuffer:Landroid/renderscript/FieldPacker;

    if-nez v1, :cond_0

    new-instance v1, Landroid/renderscript/FieldPacker;

    invoke-virtual {p0}, Lcom/android/noisefield/ScriptField_VertexColor_s;->getType()Landroid/renderscript/Type;

    move-result-object v2

    invoke-virtual {v2}, Landroid/renderscript/Type;->getX()I

    move-result v2

    mul-int/lit8 v2, v2, 0x30

    invoke-direct {v1, v2}, Landroid/renderscript/FieldPacker;-><init>(I)V

    iput-object v1, p0, Lcom/android/noisefield/ScriptField_VertexColor_s;->mIOBuffer:Landroid/renderscript/FieldPacker;

    :cond_0
    iget-object v1, p0, Lcom/android/noisefield/ScriptField_VertexColor_s;->mItemArray:[Lcom/android/noisefield/ScriptField_VertexColor_s$Item;

    if-nez v1, :cond_1

    invoke-virtual {p0}, Lcom/android/noisefield/ScriptField_VertexColor_s;->getType()Landroid/renderscript/Type;

    move-result-object v1

    invoke-virtual {v1}, Landroid/renderscript/Type;->getX()I

    move-result v1

    new-array v1, v1, [Lcom/android/noisefield/ScriptField_VertexColor_s$Item;

    iput-object v1, p0, Lcom/android/noisefield/ScriptField_VertexColor_s;->mItemArray:[Lcom/android/noisefield/ScriptField_VertexColor_s$Item;

    :cond_1
    iget-object v1, p0, Lcom/android/noisefield/ScriptField_VertexColor_s;->mItemArray:[Lcom/android/noisefield/ScriptField_VertexColor_s$Item;

    aget-object v1, v1, p1

    if-nez v1, :cond_2

    iget-object v1, p0, Lcom/android/noisefield/ScriptField_VertexColor_s;->mItemArray:[Lcom/android/noisefield/ScriptField_VertexColor_s$Item;

    new-instance v2, Lcom/android/noisefield/ScriptField_VertexColor_s$Item;

    invoke-direct {v2}, Lcom/android/noisefield/ScriptField_VertexColor_s$Item;-><init>()V

    aput-object v2, v1, p1

    :cond_2
    iget-object v1, p0, Lcom/android/noisefield/ScriptField_VertexColor_s;->mItemArray:[Lcom/android/noisefield/ScriptField_VertexColor_s$Item;

    aget-object v1, v1, p1

    iput-object p2, v1, Lcom/android/noisefield/ScriptField_VertexColor_s$Item;->position:Landroid/renderscript/Float3;

    if-eqz p3, :cond_3

    iget-object v1, p0, Lcom/android/noisefield/ScriptField_VertexColor_s;->mIOBuffer:Landroid/renderscript/FieldPacker;

    mul-int/lit8 v2, p1, 0x30

    invoke-virtual {v1, v2}, Landroid/renderscript/FieldPacker;->reset(I)V

    iget-object v1, p0, Lcom/android/noisefield/ScriptField_VertexColor_s;->mIOBuffer:Landroid/renderscript/FieldPacker;

    invoke-virtual {v1, p2}, Landroid/renderscript/FieldPacker;->addF32(Landroid/renderscript/Float3;)V

    new-instance v0, Landroid/renderscript/FieldPacker;

    const/16 v1, 0xc

    invoke-direct {v0, v1}, Landroid/renderscript/FieldPacker;-><init>(I)V

    invoke-virtual {v0, p2}, Landroid/renderscript/FieldPacker;->addF32(Landroid/renderscript/Float3;)V

    iget-object v1, p0, Lcom/android/noisefield/ScriptField_VertexColor_s;->mAllocation:Landroid/renderscript/Allocation;

    const/4 v2, 0x0

    invoke-virtual {v1, p1, v2, v0}, Landroid/renderscript/Allocation;->setFromFieldPacker(IILandroid/renderscript/FieldPacker;)V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    :cond_3
    monitor-exit p0

    return-void

    :catchall_0
    move-exception v1

    monitor-exit p0

    throw v1
.end method
