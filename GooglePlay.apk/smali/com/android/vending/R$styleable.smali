.class public final Lcom/android/vending/R$styleable;
.super Ljava/lang/Object;
.source "R.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/android/vending/R;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x19
    name = "styleable"
.end annotation


# static fields
.field public static final AccountSelectorView:[I

.field public static final AudienceView:[I

.field public static final CircleButton:[I

.field public static final CircleListItemView:[I

.field public static final ColumnGridView_Layout:[I

.field public static final ContactListItemView:[I

.field public static final ContentFrame:[I

.field public static final DecoratedTextView:[I

.field public static final DetailsPlusOne:[I

.field public static final ExactLayout_Layout:[I

.field public static final FifeImageView:[I

.field public static final MarginBox:[I

.field public static final MaxSizeLayoutHeight:[I

.field public static final MaxWidthView:[I

.field public static final MultiWaveView:[I

.field public static final ParticipantsGalleryFragment:[I

.field public static final PlayActionButton:[I

.field public static final PlayCardClusterViewHeader:[I

.field public static final PlayCardThumbnail:[I

.field public static final PlayCardView:[I

.field public static final PlaySeparatorLayout:[I

.field public static final PlayTextView:[I

.field public static final ProportionalWidthFrame:[I

.field public static final ScaledLayout:[I

.field public static final TabButton:[I

.field public static final Theme:[I

.field public static final Thermometer:[I


# direct methods
.method static constructor <clinit>()V
    .locals 7

    const/4 v6, 0x3

    const/4 v5, 0x4

    const/4 v4, 0x2

    const/4 v3, 0x1

    const/4 v2, 0x0

    new-array v0, v3, [I

    const v1, 0x7f01000c    # com.android.vending.R.attr.multi_account_drawable

    aput v1, v0, v2

    sput-object v0, Lcom/android/vending/R$styleable;->AccountSelectorView:[I

    new-array v0, v3, [I

    const v1, 0x1010153    # android.R.attr.maxLines

    aput v1, v0, v2

    sput-object v0, Lcom/android/vending/R$styleable;->AudienceView:[I

    new-array v0, v4, [I

    fill-array-data v0, :array_0

    sput-object v0, Lcom/android/vending/R$styleable;->CircleButton:[I

    const/16 v0, 0xf

    new-array v0, v0, [I

    fill-array-data v0, :array_1

    sput-object v0, Lcom/android/vending/R$styleable;->CircleListItemView:[I

    new-array v0, v6, [I

    fill-array-data v0, :array_2

    sput-object v0, Lcom/android/vending/R$styleable;->ColumnGridView_Layout:[I

    const/16 v0, 0x12

    new-array v0, v0, [I

    fill-array-data v0, :array_3

    sput-object v0, Lcom/android/vending/R$styleable;->ContactListItemView:[I

    new-array v0, v4, [I

    fill-array-data v0, :array_4

    sput-object v0, Lcom/android/vending/R$styleable;->ContentFrame:[I

    new-array v0, v3, [I

    const v1, 0x7f010008    # com.android.vending.R.attr.decoration_position

    aput v1, v0, v2

    sput-object v0, Lcom/android/vending/R$styleable;->DecoratedTextView:[I

    new-array v0, v3, [I

    const v1, 0x7f010021    # com.android.vending.R.attr.restrict_avatars_to_button_width

    aput v1, v0, v2

    sput-object v0, Lcom/android/vending/R$styleable;->DetailsPlusOne:[I

    new-array v0, v5, [I

    fill-array-data v0, :array_5

    sput-object v0, Lcom/android/vending/R$styleable;->ExactLayout_Layout:[I

    const/4 v0, 0x6

    new-array v0, v0, [I

    fill-array-data v0, :array_6

    sput-object v0, Lcom/android/vending/R$styleable;->FifeImageView:[I

    new-array v0, v3, [I

    const v1, 0x7f01000b    # com.android.vending.R.attr.max_content_width

    aput v1, v0, v2

    sput-object v0, Lcom/android/vending/R$styleable;->MarginBox:[I

    new-array v0, v3, [I

    const v1, 0x7f01000a    # com.android.vending.R.attr.max_size_layout_height

    aput v1, v0, v2

    sput-object v0, Lcom/android/vending/R$styleable;->MaxSizeLayoutHeight:[I

    new-array v0, v3, [I

    const v1, 0x7f010009    # com.android.vending.R.attr.max_width

    aput v1, v0, v2

    sput-object v0, Lcom/android/vending/R$styleable;->MaxWidthView:[I

    const/16 v0, 0xf

    new-array v0, v0, [I

    fill-array-data v0, :array_7

    sput-object v0, Lcom/android/vending/R$styleable;->MultiWaveView:[I

    new-array v0, v4, [I

    fill-array-data v0, :array_8

    sput-object v0, Lcom/android/vending/R$styleable;->ParticipantsGalleryFragment:[I

    new-array v0, v5, [I

    fill-array-data v0, :array_9

    sput-object v0, Lcom/android/vending/R$styleable;->PlayActionButton:[I

    new-array v0, v3, [I

    const v1, 0x7f010020    # com.android.vending.R.attr.min_header_height

    aput v1, v0, v2

    sput-object v0, Lcom/android/vending/R$styleable;->PlayCardClusterViewHeader:[I

    new-array v0, v3, [I

    const v1, 0x7f01001b    # com.android.vending.R.attr.app_thumbnail_padding

    aput v1, v0, v2

    sput-object v0, Lcom/android/vending/R$styleable;->PlayCardThumbnail:[I

    new-array v0, v5, [I

    fill-array-data v0, :array_a

    sput-object v0, Lcom/android/vending/R$styleable;->PlayCardView:[I

    new-array v0, v5, [I

    fill-array-data v0, :array_b

    sput-object v0, Lcom/android/vending/R$styleable;->PlaySeparatorLayout:[I

    const/4 v0, 0x5

    new-array v0, v0, [I

    fill-array-data v0, :array_c

    sput-object v0, Lcom/android/vending/R$styleable;->PlayTextView:[I

    new-array v0, v3, [I

    const v1, 0x7f01000d    # com.android.vending.R.attr.proportion

    aput v1, v0, v2

    sput-object v0, Lcom/android/vending/R$styleable;->ProportionalWidthFrame:[I

    const/16 v0, 0x9

    new-array v0, v0, [I

    fill-array-data v0, :array_d

    sput-object v0, Lcom/android/vending/R$styleable;->ScaledLayout:[I

    new-array v0, v4, [I

    fill-array-data v0, :array_e

    sput-object v0, Lcom/android/vending/R$styleable;->TabButton:[I

    const/16 v0, 0x8

    new-array v0, v0, [I

    fill-array-data v0, :array_f

    sput-object v0, Lcom/android/vending/R$styleable;->Theme:[I

    new-array v0, v6, [I

    fill-array-data v0, :array_10

    sput-object v0, Lcom/android/vending/R$styleable;->Thermometer:[I

    return-void

    nop

    :array_0
    .array-data 4
        0x7f01006d    # com.android.vending.R.attr.circle_button_icon_spacing
        0x7f01006e    # com.android.vending.R.attr.circle_button_label_spacing
    .end array-data

    :array_1
    .array-data 4
        0x7f01004c    # com.android.vending.R.attr.circle_list_item_height
        0x7f01004d    # com.android.vending.R.attr.circle_list_item_padding_top
        0x7f01004e    # com.android.vending.R.attr.circle_list_item_padding_bottom
        0x7f01004f    # com.android.vending.R.attr.circle_list_item_padding_left
        0x7f010050    # com.android.vending.R.attr.circle_list_item_padding_right
        0x7f010051    # com.android.vending.R.attr.circle_list_item_gap_between_icon_and_text
        0x7f010052    # com.android.vending.R.attr.circle_list_item_name_text_size
        0x7f010053    # com.android.vending.R.attr.circle_list_item_name_text_bold
        0x7f010054    # com.android.vending.R.attr.circle_list_item_avatar_size
        0x7f010055    # com.android.vending.R.attr.circle_list_item_avatar_spacing
        0x7f010056    # com.android.vending.R.attr.circle_list_item_circle_icon_size_small
        0x7f010057    # com.android.vending.R.attr.circle_list_item_circle_icon_size_large
        0x7f010058    # com.android.vending.R.attr.circle_list_item_gap_between_name_and_count
        0x7f010059    # com.android.vending.R.attr.circle_list_item_gap_between_count_and_checkbox
        0x7f01005a    # com.android.vending.R.attr.circle_list_item_member_count_text_color
    .end array-data

    :array_2
    .array-data 4
        0x7f01006a    # com.android.vending.R.attr.layout_orientation
        0x7f01006b    # com.android.vending.R.attr.layout_minorSpan
        0x7f01006c    # com.android.vending.R.attr.layout_majorSpan
    .end array-data

    :array_3
    .array-data 4
        0x7f01003a    # com.android.vending.R.attr.contact_list_item_height
        0x7f01003b    # com.android.vending.R.attr.contact_list_item_padding_top
        0x7f01003c    # com.android.vending.R.attr.contact_list_item_padding_bottom
        0x7f01003d    # com.android.vending.R.attr.contact_list_item_padding_left
        0x7f01003e    # com.android.vending.R.attr.contact_list_item_padding_right
        0x7f01003f    # com.android.vending.R.attr.contact_list_item_gap_between_image_and_text
        0x7f010040    # com.android.vending.R.attr.contact_list_item_name_text_size
        0x7f010041    # com.android.vending.R.attr.contact_list_item_circle_icon_drawable
        0x7f010042    # com.android.vending.R.attr.contact_list_item_circle_icon_size
        0x7f010043    # com.android.vending.R.attr.contact_list_item_circles_text_size
        0x7f010044    # com.android.vending.R.attr.contact_list_item_circles_text_color
        0x7f010045    # com.android.vending.R.attr.contact_list_item_gap_between_name_and_circles
        0x7f010046    # com.android.vending.R.attr.contact_list_item_gap_between_icon_and_circles
        0x7f010047    # com.android.vending.R.attr.contact_list_item_gap_between_text_and_button
        0x7f010048    # com.android.vending.R.attr.contact_list_item_action_button_background
        0x7f010049    # com.android.vending.R.attr.contact_list_item_action_button_width
        0x7f01004a    # com.android.vending.R.attr.contact_list_item_vertical_divider_width
        0x7f01004b    # com.android.vending.R.attr.contact_list_item_vertical_divider_padding
    .end array-data

    :array_4
    .array-data 4
        0x7f010006    # com.android.vending.R.attr.content
        0x7f010007    # com.android.vending.R.attr.contentId
    .end array-data

    :array_5
    .array-data 4
        0x7f01006f    # com.android.vending.R.attr.layout_x
        0x7f010070    # com.android.vending.R.attr.layout_y
        0x7f010071    # com.android.vending.R.attr.layout_center_horizontal_bound
        0x7f010072    # com.android.vending.R.attr.layout_center_vertical_bound
    .end array-data

    :array_6
    .array-data 4
        0x7f010000    # com.android.vending.R.attr.load_once
        0x7f010001    # com.android.vending.R.attr.fade_in_after_load
        0x7f010002    # com.android.vending.R.attr.enforce_ratio
        0x7f010003    # com.android.vending.R.attr.fixed_bounds
        0x7f010004    # com.android.vending.R.attr.circle_cut
        0x7f010005    # com.android.vending.R.attr.max_height
    .end array-data

    :array_7
    .array-data 4
        0x7f01005b    # com.android.vending.R.attr.targetDrawables
        0x7f01005c    # com.android.vending.R.attr.targetDescriptions
        0x7f01005d    # com.android.vending.R.attr.directionDescriptions
        0x7f01005e    # com.android.vending.R.attr.handleDrawable
        0x7f01005f    # com.android.vending.R.attr.leftChevronDrawable
        0x7f010060    # com.android.vending.R.attr.rightChevronDrawable
        0x7f010061    # com.android.vending.R.attr.topChevronDrawable
        0x7f010062    # com.android.vending.R.attr.bottomChevronDrawable
        0x7f010063    # com.android.vending.R.attr.waveDrawable
        0x7f010064    # com.android.vending.R.attr.hitRadius
        0x7f010065    # com.android.vending.R.attr.vibrationDuration
        0x7f010066    # com.android.vending.R.attr.snapMargin
        0x7f010067    # com.android.vending.R.attr.feedbackCount
        0x7f010068    # com.android.vending.R.attr.verticalOffset
        0x7f010069    # com.android.vending.R.attr.horizontalOffset
    .end array-data

    :array_8
    .array-data 4
        0x7f01002c    # com.android.vending.R.attr.backgroundColor
        0x7f01002d    # com.android.vending.R.attr.emptyMessage
    .end array-data

    :array_9
    .array-data 4
        0x7f01001c    # com.android.vending.R.attr.draw_as_label
        0x7f01001d    # com.android.vending.R.attr.use_all_caps_in_label_mode
        0x7f01001e    # com.android.vending.R.attr.ignore_corpus_color
        0x7f01001f    # com.android.vending.R.attr.action_xpadding
    .end array-data

    :array_a
    .array-data 4
        0x7f010017    # com.android.vending.R.attr.show_inline_creator_badge
        0x7f010018    # com.android.vending.R.attr.supports_subtitle_and_rating
        0x7f010019    # com.android.vending.R.attr.text_only_reason_margin_left
        0x7f01001a    # com.android.vending.R.attr.owned_status_rendering_type
    .end array-data

    :array_b
    .array-data 4
        0x7f010013    # com.android.vending.R.attr.separator_padding_top
        0x7f010014    # com.android.vending.R.attr.separator_padding_bottom
        0x7f010015    # com.android.vending.R.attr.separator_padding_left
        0x7f010016    # com.android.vending.R.attr.separator_padding_right
    .end array-data

    :array_c
    .array-data 4
        0x7f01000e    # com.android.vending.R.attr.compactPercentage
        0x7f01000f    # com.android.vending.R.attr.lastLineOverdrawColor
        0x7f010010    # com.android.vending.R.attr.lastLineOverdrawHint
        0x7f010011    # com.android.vending.R.attr.supportsHtmlLinks
        0x7f010012    # com.android.vending.R.attr.expandable
    .end array-data

    :array_d
    .array-data 4
        0x7f01002e    # com.android.vending.R.attr.scale
        0x7f01002f    # com.android.vending.R.attr.scaleHeight
        0x7f010030    # com.android.vending.R.attr.scaleWidth
        0x7f010031    # com.android.vending.R.attr.scaleMargin
        0x7f010032    # com.android.vending.R.attr.scaleMarginTop
        0x7f010033    # com.android.vending.R.attr.scaleMarginBottom
        0x7f010034    # com.android.vending.R.attr.scaleMarginLeft
        0x7f010035    # com.android.vending.R.attr.scaleMarginRight
        0x7f010036    # com.android.vending.R.attr.scaleMarginMode
    .end array-data

    :array_e
    .array-data 4
        0x1010002    # android.R.attr.icon
        0x101014f    # android.R.attr.text
    .end array-data

    :array_f
    .array-data 4
        0x7f010024    # com.android.vending.R.attr.buttonBarStyle
        0x7f010025    # com.android.vending.R.attr.buttonBarButtonStyle
        0x7f010026    # com.android.vending.R.attr.listSelector
        0x7f010027    # com.android.vending.R.attr.editAudienceBackground
        0x7f010028    # com.android.vending.R.attr.editLocationBackground
        0x7f010029    # com.android.vending.R.attr.buttonSelectableBackground
        0x7f01002a    # com.android.vending.R.attr.actionDropDownStyle
        0x7f01002b    # com.android.vending.R.attr.navigationItemHeight
    .end array-data

    :array_10
    .array-data 4
        0x7f010037    # com.android.vending.R.attr.background
        0x7f010038    # com.android.vending.R.attr.foreground
        0x7f010039    # com.android.vending.R.attr.orientation
    .end array-data
.end method

.method public constructor <init>()V
    .locals 0

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method
