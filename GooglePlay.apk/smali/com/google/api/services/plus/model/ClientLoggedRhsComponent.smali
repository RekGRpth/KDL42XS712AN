.class public final Lcom/google/api/services/plus/model/ClientLoggedRhsComponent;
.super Lcom/google/android/apps/plus/json/GenericJson;
.source "ClientLoggedRhsComponent.java"


# instance fields
.field public barType:Ljava/lang/Integer;

.field public componentType:Lcom/google/api/services/plus/model/ClientLoggedRhsComponentType;

.field public index:Ljava/lang/Integer;

.field public item:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Lcom/google/api/services/plus/model/ClientLoggedRhsComponentItem;",
            ">;"
        }
    .end annotation
.end field

.field public neighborInfo:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Lcom/google/api/services/plus/model/ClientLoggedRhsComponentType;",
            ">;"
        }
    .end annotation
.end field

.field public suggestionSummaryInfo:Lcom/google/api/services/plus/model/ClientLoggedSuggestionSummaryInfo;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    const-class v0, Lcom/google/api/services/plus/model/ClientLoggedRhsComponentType;

    invoke-static {v0}, Lcom/google/android/apps/plus/json/Data;->nullOf(Ljava/lang/Class;)Ljava/lang/Object;

    const-class v0, Lcom/google/api/services/plus/model/ClientLoggedRhsComponentItem;

    invoke-static {v0}, Lcom/google/android/apps/plus/json/Data;->nullOf(Ljava/lang/Class;)Ljava/lang/Object;

    return-void
.end method

.method public constructor <init>()V
    .locals 0

    invoke-direct {p0}, Lcom/google/android/apps/plus/json/GenericJson;-><init>()V

    return-void
.end method
