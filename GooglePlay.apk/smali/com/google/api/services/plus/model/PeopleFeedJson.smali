.class public final Lcom/google/api/services/plus/model/PeopleFeedJson;
.super Lcom/google/android/apps/plus/json/EsJson;
.source "PeopleFeedJson.java"


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/google/android/apps/plus/json/EsJson",
        "<",
        "Lcom/google/api/services/plus/model/PeopleFeed;",
        ">;"
    }
.end annotation


# static fields
.field static final INSTANCE:Lcom/google/api/services/plus/model/PeopleFeedJson;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    new-instance v0, Lcom/google/api/services/plus/model/PeopleFeedJson;

    invoke-direct {v0}, Lcom/google/api/services/plus/model/PeopleFeedJson;-><init>()V

    sput-object v0, Lcom/google/api/services/plus/model/PeopleFeedJson;->INSTANCE:Lcom/google/api/services/plus/model/PeopleFeedJson;

    return-void
.end method

.method private constructor <init>()V
    .locals 4

    const-class v0, Lcom/google/api/services/plus/model/PeopleFeed;

    const/16 v1, 0x8

    new-array v1, v1, [Ljava/lang/Object;

    const/4 v2, 0x0

    const-string v3, "nextPageToken"

    aput-object v3, v1, v2

    const/4 v2, 0x1

    const-string v3, "kind"

    aput-object v3, v1, v2

    const/4 v2, 0x2

    const-string v3, "title"

    aput-object v3, v1, v2

    const/4 v2, 0x3

    const-class v3, Lcom/google/api/services/plus/model/PersonJson;

    aput-object v3, v1, v2

    const/4 v2, 0x4

    const-string v3, "items"

    aput-object v3, v1, v2

    const/4 v2, 0x5

    const-string v3, "etag"

    aput-object v3, v1, v2

    const/4 v2, 0x6

    const-string v3, "totalItems"

    aput-object v3, v1, v2

    const/4 v2, 0x7

    const-string v3, "selfLink"

    aput-object v3, v1, v2

    invoke-direct {p0, v0, v1}, Lcom/google/android/apps/plus/json/EsJson;-><init>(Ljava/lang/Class;[Ljava/lang/Object;)V

    return-void
.end method

.method public static getInstance()Lcom/google/api/services/plus/model/PeopleFeedJson;
    .locals 1

    sget-object v0, Lcom/google/api/services/plus/model/PeopleFeedJson;->INSTANCE:Lcom/google/api/services/plus/model/PeopleFeedJson;

    return-object v0
.end method


# virtual methods
.method public getValues(Lcom/google/api/services/plus/model/PeopleFeed;)[Ljava/lang/Object;
    .locals 3
    .param p1    # Lcom/google/api/services/plus/model/PeopleFeed;

    const/4 v0, 0x7

    new-array v0, v0, [Ljava/lang/Object;

    const/4 v1, 0x0

    iget-object v2, p1, Lcom/google/api/services/plus/model/PeopleFeed;->nextPageToken:Ljava/lang/String;

    aput-object v2, v0, v1

    const/4 v1, 0x1

    iget-object v2, p1, Lcom/google/api/services/plus/model/PeopleFeed;->kind:Ljava/lang/String;

    aput-object v2, v0, v1

    const/4 v1, 0x2

    iget-object v2, p1, Lcom/google/api/services/plus/model/PeopleFeed;->title:Ljava/lang/String;

    aput-object v2, v0, v1

    const/4 v1, 0x3

    iget-object v2, p1, Lcom/google/api/services/plus/model/PeopleFeed;->items:Ljava/util/List;

    aput-object v2, v0, v1

    const/4 v1, 0x4

    iget-object v2, p1, Lcom/google/api/services/plus/model/PeopleFeed;->etag:Ljava/lang/String;

    aput-object v2, v0, v1

    const/4 v1, 0x5

    iget-object v2, p1, Lcom/google/api/services/plus/model/PeopleFeed;->totalItems:Ljava/lang/Integer;

    aput-object v2, v0, v1

    const/4 v1, 0x6

    iget-object v2, p1, Lcom/google/api/services/plus/model/PeopleFeed;->selfLink:Ljava/lang/String;

    aput-object v2, v0, v1

    return-object v0
.end method

.method public bridge synthetic getValues(Ljava/lang/Object;)[Ljava/lang/Object;
    .locals 1
    .param p1    # Ljava/lang/Object;

    check-cast p1, Lcom/google/api/services/plus/model/PeopleFeed;

    invoke-virtual {p0, p1}, Lcom/google/api/services/plus/model/PeopleFeedJson;->getValues(Lcom/google/api/services/plus/model/PeopleFeed;)[Ljava/lang/Object;

    move-result-object v0

    return-object v0
.end method

.method public newInstance()Lcom/google/api/services/plus/model/PeopleFeed;
    .locals 1

    new-instance v0, Lcom/google/api/services/plus/model/PeopleFeed;

    invoke-direct {v0}, Lcom/google/api/services/plus/model/PeopleFeed;-><init>()V

    return-object v0
.end method

.method public bridge synthetic newInstance()Ljava/lang/Object;
    .locals 1

    invoke-virtual {p0}, Lcom/google/api/services/plus/model/PeopleFeedJson;->newInstance()Lcom/google/api/services/plus/model/PeopleFeed;

    move-result-object v0

    return-object v0
.end method
