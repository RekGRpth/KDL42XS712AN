.class public final Lcom/google/api/services/plus/model/ClientLoggedCircleMemberJson;
.super Lcom/google/android/apps/plus/json/EsJson;
.source "ClientLoggedCircleMemberJson.java"


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/google/android/apps/plus/json/EsJson",
        "<",
        "Lcom/google/api/services/plus/model/ClientLoggedCircleMember;",
        ">;"
    }
.end annotation


# static fields
.field static final INSTANCE:Lcom/google/api/services/plus/model/ClientLoggedCircleMemberJson;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    new-instance v0, Lcom/google/api/services/plus/model/ClientLoggedCircleMemberJson;

    invoke-direct {v0}, Lcom/google/api/services/plus/model/ClientLoggedCircleMemberJson;-><init>()V

    sput-object v0, Lcom/google/api/services/plus/model/ClientLoggedCircleMemberJson;->INSTANCE:Lcom/google/api/services/plus/model/ClientLoggedCircleMemberJson;

    return-void
.end method

.method private constructor <init>()V
    .locals 4

    const-class v0, Lcom/google/api/services/plus/model/ClientLoggedCircleMember;

    const/4 v1, 0x5

    new-array v1, v1, [Ljava/lang/Object;

    const/4 v2, 0x0

    const-string v3, "plusPageType"

    aput-object v3, v1, v2

    const/4 v2, 0x1

    const-string v3, "contactInAnyCircle"

    aput-object v3, v1, v2

    const/4 v2, 0x2

    const-string v3, "emailAddress"

    aput-object v3, v1, v2

    const/4 v2, 0x3

    const-string v3, "focusContactId"

    aput-object v3, v1, v2

    const/4 v2, 0x4

    const-string v3, "obfuscatedGaiaId"

    aput-object v3, v1, v2

    invoke-direct {p0, v0, v1}, Lcom/google/android/apps/plus/json/EsJson;-><init>(Ljava/lang/Class;[Ljava/lang/Object;)V

    return-void
.end method

.method public static getInstance()Lcom/google/api/services/plus/model/ClientLoggedCircleMemberJson;
    .locals 1

    sget-object v0, Lcom/google/api/services/plus/model/ClientLoggedCircleMemberJson;->INSTANCE:Lcom/google/api/services/plus/model/ClientLoggedCircleMemberJson;

    return-object v0
.end method


# virtual methods
.method public getValues(Lcom/google/api/services/plus/model/ClientLoggedCircleMember;)[Ljava/lang/Object;
    .locals 3
    .param p1    # Lcom/google/api/services/plus/model/ClientLoggedCircleMember;

    const/4 v0, 0x5

    new-array v0, v0, [Ljava/lang/Object;

    const/4 v1, 0x0

    iget-object v2, p1, Lcom/google/api/services/plus/model/ClientLoggedCircleMember;->plusPageType:Ljava/lang/Integer;

    aput-object v2, v0, v1

    const/4 v1, 0x1

    iget-object v2, p1, Lcom/google/api/services/plus/model/ClientLoggedCircleMember;->contactInAnyCircle:Ljava/lang/Boolean;

    aput-object v2, v0, v1

    const/4 v1, 0x2

    iget-object v2, p1, Lcom/google/api/services/plus/model/ClientLoggedCircleMember;->emailAddress:Ljava/lang/String;

    aput-object v2, v0, v1

    const/4 v1, 0x3

    iget-object v2, p1, Lcom/google/api/services/plus/model/ClientLoggedCircleMember;->focusContactId:Ljava/lang/String;

    aput-object v2, v0, v1

    const/4 v1, 0x4

    iget-object v2, p1, Lcom/google/api/services/plus/model/ClientLoggedCircleMember;->obfuscatedGaiaId:Ljava/lang/String;

    aput-object v2, v0, v1

    return-object v0
.end method

.method public bridge synthetic getValues(Ljava/lang/Object;)[Ljava/lang/Object;
    .locals 1
    .param p1    # Ljava/lang/Object;

    check-cast p1, Lcom/google/api/services/plus/model/ClientLoggedCircleMember;

    invoke-virtual {p0, p1}, Lcom/google/api/services/plus/model/ClientLoggedCircleMemberJson;->getValues(Lcom/google/api/services/plus/model/ClientLoggedCircleMember;)[Ljava/lang/Object;

    move-result-object v0

    return-object v0
.end method

.method public newInstance()Lcom/google/api/services/plus/model/ClientLoggedCircleMember;
    .locals 1

    new-instance v0, Lcom/google/api/services/plus/model/ClientLoggedCircleMember;

    invoke-direct {v0}, Lcom/google/api/services/plus/model/ClientLoggedCircleMember;-><init>()V

    return-object v0
.end method

.method public bridge synthetic newInstance()Ljava/lang/Object;
    .locals 1

    invoke-virtual {p0}, Lcom/google/api/services/plus/model/ClientLoggedCircleMemberJson;->newInstance()Lcom/google/api/services/plus/model/ClientLoggedCircleMember;

    move-result-object v0

    return-object v0
.end method
