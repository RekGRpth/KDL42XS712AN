.class public final Lcom/google/api/services/plus/model/ProfileEditJson;
.super Lcom/google/android/apps/plus/json/EsJson;
.source "ProfileEditJson.java"


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/google/android/apps/plus/json/EsJson",
        "<",
        "Lcom/google/api/services/plus/model/ProfileEdit;",
        ">;"
    }
.end annotation


# static fields
.field static final INSTANCE:Lcom/google/api/services/plus/model/ProfileEditJson;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    new-instance v0, Lcom/google/api/services/plus/model/ProfileEditJson;

    invoke-direct {v0}, Lcom/google/api/services/plus/model/ProfileEditJson;-><init>()V

    sput-object v0, Lcom/google/api/services/plus/model/ProfileEditJson;->INSTANCE:Lcom/google/api/services/plus/model/ProfileEditJson;

    return-void
.end method

.method private constructor <init>()V
    .locals 4

    const-class v0, Lcom/google/api/services/plus/model/ProfileEdit;

    const/16 v1, 0xb

    new-array v1, v1, [Ljava/lang/Object;

    const/4 v2, 0x0

    const-string v3, "fullBleedPhotoId"

    aput-object v3, v1, v2

    const/4 v2, 0x1

    const-string v3, "givenName"

    aput-object v3, v1, v2

    const/4 v2, 0x2

    const-string v3, "coverPhotoOwnerType"

    aput-object v3, v1, v2

    const/4 v2, 0x3

    const-string v3, "familyName"

    aput-object v3, v1, v2

    const/4 v2, 0x4

    const-string v3, "coverPhotoOffset"

    aput-object v3, v1, v2

    const/4 v2, 0x5

    const-string v3, "scrapbookLayout"

    aput-object v3, v1, v2

    const/4 v2, 0x6

    const-class v3, Lcom/google/api/services/plus/model/ProfileVisibilityEditJson;

    aput-object v3, v1, v2

    const/4 v2, 0x7

    const-string v3, "visibility"

    aput-object v3, v1, v2

    const/16 v2, 0x8

    const-string v3, "education"

    aput-object v3, v1, v2

    const/16 v2, 0x9

    const-string v3, "employment"

    aput-object v3, v1, v2

    const/16 v2, 0xa

    const-string v3, "currentLocation"

    aput-object v3, v1, v2

    invoke-direct {p0, v0, v1}, Lcom/google/android/apps/plus/json/EsJson;-><init>(Ljava/lang/Class;[Ljava/lang/Object;)V

    return-void
.end method

.method public static getInstance()Lcom/google/api/services/plus/model/ProfileEditJson;
    .locals 1

    sget-object v0, Lcom/google/api/services/plus/model/ProfileEditJson;->INSTANCE:Lcom/google/api/services/plus/model/ProfileEditJson;

    return-object v0
.end method


# virtual methods
.method public getValues(Lcom/google/api/services/plus/model/ProfileEdit;)[Ljava/lang/Object;
    .locals 3
    .param p1    # Lcom/google/api/services/plus/model/ProfileEdit;

    const/16 v0, 0xa

    new-array v0, v0, [Ljava/lang/Object;

    const/4 v1, 0x0

    iget-object v2, p1, Lcom/google/api/services/plus/model/ProfileEdit;->fullBleedPhotoId:Ljava/util/List;

    aput-object v2, v0, v1

    const/4 v1, 0x1

    iget-object v2, p1, Lcom/google/api/services/plus/model/ProfileEdit;->givenName:Ljava/lang/String;

    aput-object v2, v0, v1

    const/4 v1, 0x2

    iget-object v2, p1, Lcom/google/api/services/plus/model/ProfileEdit;->coverPhotoOwnerType:Ljava/lang/String;

    aput-object v2, v0, v1

    const/4 v1, 0x3

    iget-object v2, p1, Lcom/google/api/services/plus/model/ProfileEdit;->familyName:Ljava/lang/String;

    aput-object v2, v0, v1

    const/4 v1, 0x4

    iget-object v2, p1, Lcom/google/api/services/plus/model/ProfileEdit;->coverPhotoOffset:Ljava/lang/String;

    aput-object v2, v0, v1

    const/4 v1, 0x5

    iget-object v2, p1, Lcom/google/api/services/plus/model/ProfileEdit;->scrapbookLayout:Ljava/lang/String;

    aput-object v2, v0, v1

    const/4 v1, 0x6

    iget-object v2, p1, Lcom/google/api/services/plus/model/ProfileEdit;->visibility:Ljava/util/List;

    aput-object v2, v0, v1

    const/4 v1, 0x7

    iget-object v2, p1, Lcom/google/api/services/plus/model/ProfileEdit;->education:Ljava/util/List;

    aput-object v2, v0, v1

    const/16 v1, 0x8

    iget-object v2, p1, Lcom/google/api/services/plus/model/ProfileEdit;->employment:Ljava/util/List;

    aput-object v2, v0, v1

    const/16 v1, 0x9

    iget-object v2, p1, Lcom/google/api/services/plus/model/ProfileEdit;->currentLocation:Ljava/lang/String;

    aput-object v2, v0, v1

    return-object v0
.end method

.method public bridge synthetic getValues(Ljava/lang/Object;)[Ljava/lang/Object;
    .locals 1
    .param p1    # Ljava/lang/Object;

    check-cast p1, Lcom/google/api/services/plus/model/ProfileEdit;

    invoke-virtual {p0, p1}, Lcom/google/api/services/plus/model/ProfileEditJson;->getValues(Lcom/google/api/services/plus/model/ProfileEdit;)[Ljava/lang/Object;

    move-result-object v0

    return-object v0
.end method

.method public newInstance()Lcom/google/api/services/plus/model/ProfileEdit;
    .locals 1

    new-instance v0, Lcom/google/api/services/plus/model/ProfileEdit;

    invoke-direct {v0}, Lcom/google/api/services/plus/model/ProfileEdit;-><init>()V

    return-object v0
.end method

.method public bridge synthetic newInstance()Ljava/lang/Object;
    .locals 1

    invoke-virtual {p0}, Lcom/google/api/services/plus/model/ProfileEditJson;->newInstance()Lcom/google/api/services/plus/model/ProfileEdit;

    move-result-object v0

    return-object v0
.end method
