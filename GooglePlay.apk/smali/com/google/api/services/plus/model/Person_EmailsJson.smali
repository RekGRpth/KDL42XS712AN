.class public final Lcom/google/api/services/plus/model/Person_EmailsJson;
.super Lcom/google/android/apps/plus/json/EsJson;
.source "Person_EmailsJson.java"


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/google/android/apps/plus/json/EsJson",
        "<",
        "Lcom/google/api/services/plus/model/Person$Emails;",
        ">;"
    }
.end annotation


# static fields
.field static final INSTANCE:Lcom/google/api/services/plus/model/Person_EmailsJson;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    new-instance v0, Lcom/google/api/services/plus/model/Person_EmailsJson;

    invoke-direct {v0}, Lcom/google/api/services/plus/model/Person_EmailsJson;-><init>()V

    sput-object v0, Lcom/google/api/services/plus/model/Person_EmailsJson;->INSTANCE:Lcom/google/api/services/plus/model/Person_EmailsJson;

    return-void
.end method

.method private constructor <init>()V
    .locals 4

    const-class v0, Lcom/google/api/services/plus/model/Person$Emails;

    const/4 v1, 0x3

    new-array v1, v1, [Ljava/lang/Object;

    const/4 v2, 0x0

    const-string v3, "type"

    aput-object v3, v1, v2

    const/4 v2, 0x1

    const-string v3, "primary"

    aput-object v3, v1, v2

    const/4 v2, 0x2

    const-string v3, "value"

    aput-object v3, v1, v2

    invoke-direct {p0, v0, v1}, Lcom/google/android/apps/plus/json/EsJson;-><init>(Ljava/lang/Class;[Ljava/lang/Object;)V

    return-void
.end method

.method public static getInstance()Lcom/google/api/services/plus/model/Person_EmailsJson;
    .locals 1

    sget-object v0, Lcom/google/api/services/plus/model/Person_EmailsJson;->INSTANCE:Lcom/google/api/services/plus/model/Person_EmailsJson;

    return-object v0
.end method


# virtual methods
.method public getValues(Lcom/google/api/services/plus/model/Person$Emails;)[Ljava/lang/Object;
    .locals 3
    .param p1    # Lcom/google/api/services/plus/model/Person$Emails;

    const/4 v0, 0x3

    new-array v0, v0, [Ljava/lang/Object;

    const/4 v1, 0x0

    iget-object v2, p1, Lcom/google/api/services/plus/model/Person$Emails;->type:Ljava/lang/String;

    aput-object v2, v0, v1

    const/4 v1, 0x1

    iget-object v2, p1, Lcom/google/api/services/plus/model/Person$Emails;->primary:Ljava/lang/Boolean;

    aput-object v2, v0, v1

    const/4 v1, 0x2

    iget-object v2, p1, Lcom/google/api/services/plus/model/Person$Emails;->value:Ljava/lang/String;

    aput-object v2, v0, v1

    return-object v0
.end method

.method public bridge synthetic getValues(Ljava/lang/Object;)[Ljava/lang/Object;
    .locals 1
    .param p1    # Ljava/lang/Object;

    check-cast p1, Lcom/google/api/services/plus/model/Person$Emails;

    invoke-virtual {p0, p1}, Lcom/google/api/services/plus/model/Person_EmailsJson;->getValues(Lcom/google/api/services/plus/model/Person$Emails;)[Ljava/lang/Object;

    move-result-object v0

    return-object v0
.end method

.method public newInstance()Lcom/google/api/services/plus/model/Person$Emails;
    .locals 1

    new-instance v0, Lcom/google/api/services/plus/model/Person$Emails;

    invoke-direct {v0}, Lcom/google/api/services/plus/model/Person$Emails;-><init>()V

    return-object v0
.end method

.method public bridge synthetic newInstance()Ljava/lang/Object;
    .locals 1

    invoke-virtual {p0}, Lcom/google/api/services/plus/model/Person_EmailsJson;->newInstance()Lcom/google/api/services/plus/model/Person$Emails;

    move-result-object v0

    return-object v0
.end method
