.class public final Lcom/google/api/services/plus/model/ClientUserInfoJson;
.super Lcom/google/android/apps/plus/json/EsJson;
.source "ClientUserInfoJson.java"


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/google/android/apps/plus/json/EsJson",
        "<",
        "Lcom/google/api/services/plus/model/ClientUserInfo;",
        ">;"
    }
.end annotation


# static fields
.field static final INSTANCE:Lcom/google/api/services/plus/model/ClientUserInfoJson;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    new-instance v0, Lcom/google/api/services/plus/model/ClientUserInfoJson;

    invoke-direct {v0}, Lcom/google/api/services/plus/model/ClientUserInfoJson;-><init>()V

    sput-object v0, Lcom/google/api/services/plus/model/ClientUserInfoJson;->INSTANCE:Lcom/google/api/services/plus/model/ClientUserInfoJson;

    return-void
.end method

.method private constructor <init>()V
    .locals 4

    const-class v0, Lcom/google/api/services/plus/model/ClientUserInfo;

    const/4 v1, 0x4

    new-array v1, v1, [Ljava/lang/Object;

    const/4 v2, 0x0

    const-string v3, "obfuscatedGaiaId"

    aput-object v3, v1, v2

    const/4 v2, 0x1

    const-string v3, "plusPageType"

    aput-object v3, v1, v2

    const/4 v2, 0x2

    const-string v3, "entityTypeId"

    aput-object v3, v1, v2

    const/4 v2, 0x3

    const-string v3, "userContext"

    aput-object v3, v1, v2

    invoke-direct {p0, v0, v1}, Lcom/google/android/apps/plus/json/EsJson;-><init>(Ljava/lang/Class;[Ljava/lang/Object;)V

    return-void
.end method

.method public static getInstance()Lcom/google/api/services/plus/model/ClientUserInfoJson;
    .locals 1

    sget-object v0, Lcom/google/api/services/plus/model/ClientUserInfoJson;->INSTANCE:Lcom/google/api/services/plus/model/ClientUserInfoJson;

    return-object v0
.end method


# virtual methods
.method public getValues(Lcom/google/api/services/plus/model/ClientUserInfo;)[Ljava/lang/Object;
    .locals 3
    .param p1    # Lcom/google/api/services/plus/model/ClientUserInfo;

    const/4 v0, 0x4

    new-array v0, v0, [Ljava/lang/Object;

    const/4 v1, 0x0

    iget-object v2, p1, Lcom/google/api/services/plus/model/ClientUserInfo;->obfuscatedGaiaId:Ljava/lang/String;

    aput-object v2, v0, v1

    const/4 v1, 0x1

    iget-object v2, p1, Lcom/google/api/services/plus/model/ClientUserInfo;->plusPageType:Ljava/lang/Integer;

    aput-object v2, v0, v1

    const/4 v1, 0x2

    iget-object v2, p1, Lcom/google/api/services/plus/model/ClientUserInfo;->entityTypeId:Ljava/lang/Integer;

    aput-object v2, v0, v1

    const/4 v1, 0x3

    iget-object v2, p1, Lcom/google/api/services/plus/model/ClientUserInfo;->userContext:Ljava/lang/String;

    aput-object v2, v0, v1

    return-object v0
.end method

.method public bridge synthetic getValues(Ljava/lang/Object;)[Ljava/lang/Object;
    .locals 1
    .param p1    # Ljava/lang/Object;

    check-cast p1, Lcom/google/api/services/plus/model/ClientUserInfo;

    invoke-virtual {p0, p1}, Lcom/google/api/services/plus/model/ClientUserInfoJson;->getValues(Lcom/google/api/services/plus/model/ClientUserInfo;)[Ljava/lang/Object;

    move-result-object v0

    return-object v0
.end method

.method public newInstance()Lcom/google/api/services/plus/model/ClientUserInfo;
    .locals 1

    new-instance v0, Lcom/google/api/services/plus/model/ClientUserInfo;

    invoke-direct {v0}, Lcom/google/api/services/plus/model/ClientUserInfo;-><init>()V

    return-object v0
.end method

.method public bridge synthetic newInstance()Ljava/lang/Object;
    .locals 1

    invoke-virtual {p0}, Lcom/google/api/services/plus/model/ClientUserInfoJson;->newInstance()Lcom/google/api/services/plus/model/ClientUserInfo;

    move-result-object v0

    return-object v0
.end method
