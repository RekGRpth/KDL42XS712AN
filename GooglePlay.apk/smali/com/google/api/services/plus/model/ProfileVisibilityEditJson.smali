.class public final Lcom/google/api/services/plus/model/ProfileVisibilityEditJson;
.super Lcom/google/android/apps/plus/json/EsJson;
.source "ProfileVisibilityEditJson.java"


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/google/android/apps/plus/json/EsJson",
        "<",
        "Lcom/google/api/services/plus/model/ProfileVisibilityEdit;",
        ">;"
    }
.end annotation


# static fields
.field static final INSTANCE:Lcom/google/api/services/plus/model/ProfileVisibilityEditJson;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    new-instance v0, Lcom/google/api/services/plus/model/ProfileVisibilityEditJson;

    invoke-direct {v0}, Lcom/google/api/services/plus/model/ProfileVisibilityEditJson;-><init>()V

    sput-object v0, Lcom/google/api/services/plus/model/ProfileVisibilityEditJson;->INSTANCE:Lcom/google/api/services/plus/model/ProfileVisibilityEditJson;

    return-void
.end method

.method private constructor <init>()V
    .locals 4

    const-class v0, Lcom/google/api/services/plus/model/ProfileVisibilityEdit;

    const/4 v1, 0x2

    new-array v1, v1, [Ljava/lang/Object;

    const/4 v2, 0x0

    const-string v3, "madeVisible"

    aput-object v3, v1, v2

    const/4 v2, 0x1

    const-string v3, "setting"

    aput-object v3, v1, v2

    invoke-direct {p0, v0, v1}, Lcom/google/android/apps/plus/json/EsJson;-><init>(Ljava/lang/Class;[Ljava/lang/Object;)V

    return-void
.end method

.method public static getInstance()Lcom/google/api/services/plus/model/ProfileVisibilityEditJson;
    .locals 1

    sget-object v0, Lcom/google/api/services/plus/model/ProfileVisibilityEditJson;->INSTANCE:Lcom/google/api/services/plus/model/ProfileVisibilityEditJson;

    return-object v0
.end method


# virtual methods
.method public getValues(Lcom/google/api/services/plus/model/ProfileVisibilityEdit;)[Ljava/lang/Object;
    .locals 3
    .param p1    # Lcom/google/api/services/plus/model/ProfileVisibilityEdit;

    const/4 v0, 0x2

    new-array v0, v0, [Ljava/lang/Object;

    const/4 v1, 0x0

    iget-object v2, p1, Lcom/google/api/services/plus/model/ProfileVisibilityEdit;->madeVisible:Ljava/lang/Boolean;

    aput-object v2, v0, v1

    const/4 v1, 0x1

    iget-object v2, p1, Lcom/google/api/services/plus/model/ProfileVisibilityEdit;->setting:Ljava/lang/String;

    aput-object v2, v0, v1

    return-object v0
.end method

.method public bridge synthetic getValues(Ljava/lang/Object;)[Ljava/lang/Object;
    .locals 1
    .param p1    # Ljava/lang/Object;

    check-cast p1, Lcom/google/api/services/plus/model/ProfileVisibilityEdit;

    invoke-virtual {p0, p1}, Lcom/google/api/services/plus/model/ProfileVisibilityEditJson;->getValues(Lcom/google/api/services/plus/model/ProfileVisibilityEdit;)[Ljava/lang/Object;

    move-result-object v0

    return-object v0
.end method

.method public newInstance()Lcom/google/api/services/plus/model/ProfileVisibilityEdit;
    .locals 1

    new-instance v0, Lcom/google/api/services/plus/model/ProfileVisibilityEdit;

    invoke-direct {v0}, Lcom/google/api/services/plus/model/ProfileVisibilityEdit;-><init>()V

    return-object v0
.end method

.method public bridge synthetic newInstance()Ljava/lang/Object;
    .locals 1

    invoke-virtual {p0}, Lcom/google/api/services/plus/model/ProfileVisibilityEditJson;->newInstance()Lcom/google/api/services/plus/model/ProfileVisibilityEdit;

    move-result-object v0

    return-object v0
.end method
