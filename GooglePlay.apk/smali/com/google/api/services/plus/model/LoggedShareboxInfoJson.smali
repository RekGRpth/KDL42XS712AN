.class public final Lcom/google/api/services/plus/model/LoggedShareboxInfoJson;
.super Lcom/google/android/apps/plus/json/EsJson;
.source "LoggedShareboxInfoJson.java"


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/google/android/apps/plus/json/EsJson",
        "<",
        "Lcom/google/api/services/plus/model/LoggedShareboxInfo;",
        ">;"
    }
.end annotation


# static fields
.field static final INSTANCE:Lcom/google/api/services/plus/model/LoggedShareboxInfoJson;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    new-instance v0, Lcom/google/api/services/plus/model/LoggedShareboxInfoJson;

    invoke-direct {v0}, Lcom/google/api/services/plus/model/LoggedShareboxInfoJson;-><init>()V

    sput-object v0, Lcom/google/api/services/plus/model/LoggedShareboxInfoJson;->INSTANCE:Lcom/google/api/services/plus/model/LoggedShareboxInfoJson;

    return-void
.end method

.method private constructor <init>()V
    .locals 4

    const-class v0, Lcom/google/api/services/plus/model/LoggedShareboxInfo;

    const/4 v1, 0x4

    new-array v1, v1, [Ljava/lang/Object;

    const/4 v2, 0x0

    const-string v3, "autocheckedAlsoSendEmail"

    aput-object v3, v1, v2

    const/4 v2, 0x1

    const-string v3, "emailRecipients"

    aput-object v3, v1, v2

    const/4 v2, 0x2

    const-string v3, "alsoSendEmail"

    aput-object v3, v1, v2

    const/4 v2, 0x3

    const-string v3, "postSize"

    aput-object v3, v1, v2

    invoke-direct {p0, v0, v1}, Lcom/google/android/apps/plus/json/EsJson;-><init>(Ljava/lang/Class;[Ljava/lang/Object;)V

    return-void
.end method

.method public static getInstance()Lcom/google/api/services/plus/model/LoggedShareboxInfoJson;
    .locals 1

    sget-object v0, Lcom/google/api/services/plus/model/LoggedShareboxInfoJson;->INSTANCE:Lcom/google/api/services/plus/model/LoggedShareboxInfoJson;

    return-object v0
.end method


# virtual methods
.method public getValues(Lcom/google/api/services/plus/model/LoggedShareboxInfo;)[Ljava/lang/Object;
    .locals 3
    .param p1    # Lcom/google/api/services/plus/model/LoggedShareboxInfo;

    const/4 v0, 0x4

    new-array v0, v0, [Ljava/lang/Object;

    const/4 v1, 0x0

    iget-object v2, p1, Lcom/google/api/services/plus/model/LoggedShareboxInfo;->autocheckedAlsoSendEmail:Ljava/lang/Boolean;

    aput-object v2, v0, v1

    const/4 v1, 0x1

    iget-object v2, p1, Lcom/google/api/services/plus/model/LoggedShareboxInfo;->emailRecipients:Ljava/lang/Integer;

    aput-object v2, v0, v1

    const/4 v1, 0x2

    iget-object v2, p1, Lcom/google/api/services/plus/model/LoggedShareboxInfo;->alsoSendEmail:Ljava/lang/Boolean;

    aput-object v2, v0, v1

    const/4 v1, 0x3

    iget-object v2, p1, Lcom/google/api/services/plus/model/LoggedShareboxInfo;->postSize:Ljava/lang/Integer;

    aput-object v2, v0, v1

    return-object v0
.end method

.method public bridge synthetic getValues(Ljava/lang/Object;)[Ljava/lang/Object;
    .locals 1
    .param p1    # Ljava/lang/Object;

    check-cast p1, Lcom/google/api/services/plus/model/LoggedShareboxInfo;

    invoke-virtual {p0, p1}, Lcom/google/api/services/plus/model/LoggedShareboxInfoJson;->getValues(Lcom/google/api/services/plus/model/LoggedShareboxInfo;)[Ljava/lang/Object;

    move-result-object v0

    return-object v0
.end method

.method public newInstance()Lcom/google/api/services/plus/model/LoggedShareboxInfo;
    .locals 1

    new-instance v0, Lcom/google/api/services/plus/model/LoggedShareboxInfo;

    invoke-direct {v0}, Lcom/google/api/services/plus/model/LoggedShareboxInfo;-><init>()V

    return-object v0
.end method

.method public bridge synthetic newInstance()Ljava/lang/Object;
    .locals 1

    invoke-virtual {p0}, Lcom/google/api/services/plus/model/LoggedShareboxInfoJson;->newInstance()Lcom/google/api/services/plus/model/LoggedShareboxInfo;

    move-result-object v0

    return-object v0
.end method
