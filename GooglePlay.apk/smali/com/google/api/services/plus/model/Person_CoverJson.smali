.class public final Lcom/google/api/services/plus/model/Person_CoverJson;
.super Lcom/google/android/apps/plus/json/EsJson;
.source "Person_CoverJson.java"


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/google/android/apps/plus/json/EsJson",
        "<",
        "Lcom/google/api/services/plus/model/Person$Cover;",
        ">;"
    }
.end annotation


# static fields
.field static final INSTANCE:Lcom/google/api/services/plus/model/Person_CoverJson;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    new-instance v0, Lcom/google/api/services/plus/model/Person_CoverJson;

    invoke-direct {v0}, Lcom/google/api/services/plus/model/Person_CoverJson;-><init>()V

    sput-object v0, Lcom/google/api/services/plus/model/Person_CoverJson;->INSTANCE:Lcom/google/api/services/plus/model/Person_CoverJson;

    return-void
.end method

.method private constructor <init>()V
    .locals 4

    const-class v0, Lcom/google/api/services/plus/model/Person$Cover;

    const/4 v1, 0x5

    new-array v1, v1, [Ljava/lang/Object;

    const/4 v2, 0x0

    const-string v3, "layout"

    aput-object v3, v1, v2

    const/4 v2, 0x1

    const-class v3, Lcom/google/api/services/plus/model/Person_Cover_CoverInfoJson;

    aput-object v3, v1, v2

    const/4 v2, 0x2

    const-string v3, "coverInfo"

    aput-object v3, v1, v2

    const/4 v2, 0x3

    const-class v3, Lcom/google/api/services/plus/model/Person_Cover_CoverPhotoJson;

    aput-object v3, v1, v2

    const/4 v2, 0x4

    const-string v3, "coverPhoto"

    aput-object v3, v1, v2

    invoke-direct {p0, v0, v1}, Lcom/google/android/apps/plus/json/EsJson;-><init>(Ljava/lang/Class;[Ljava/lang/Object;)V

    return-void
.end method

.method public static getInstance()Lcom/google/api/services/plus/model/Person_CoverJson;
    .locals 1

    sget-object v0, Lcom/google/api/services/plus/model/Person_CoverJson;->INSTANCE:Lcom/google/api/services/plus/model/Person_CoverJson;

    return-object v0
.end method


# virtual methods
.method public getValues(Lcom/google/api/services/plus/model/Person$Cover;)[Ljava/lang/Object;
    .locals 3
    .param p1    # Lcom/google/api/services/plus/model/Person$Cover;

    const/4 v0, 0x3

    new-array v0, v0, [Ljava/lang/Object;

    const/4 v1, 0x0

    iget-object v2, p1, Lcom/google/api/services/plus/model/Person$Cover;->layout:Ljava/lang/String;

    aput-object v2, v0, v1

    const/4 v1, 0x1

    iget-object v2, p1, Lcom/google/api/services/plus/model/Person$Cover;->coverInfo:Lcom/google/api/services/plus/model/Person$Cover$CoverInfo;

    aput-object v2, v0, v1

    const/4 v1, 0x2

    iget-object v2, p1, Lcom/google/api/services/plus/model/Person$Cover;->coverPhoto:Lcom/google/api/services/plus/model/Person$Cover$CoverPhoto;

    aput-object v2, v0, v1

    return-object v0
.end method

.method public bridge synthetic getValues(Ljava/lang/Object;)[Ljava/lang/Object;
    .locals 1
    .param p1    # Ljava/lang/Object;

    check-cast p1, Lcom/google/api/services/plus/model/Person$Cover;

    invoke-virtual {p0, p1}, Lcom/google/api/services/plus/model/Person_CoverJson;->getValues(Lcom/google/api/services/plus/model/Person$Cover;)[Ljava/lang/Object;

    move-result-object v0

    return-object v0
.end method

.method public newInstance()Lcom/google/api/services/plus/model/Person$Cover;
    .locals 1

    new-instance v0, Lcom/google/api/services/plus/model/Person$Cover;

    invoke-direct {v0}, Lcom/google/api/services/plus/model/Person$Cover;-><init>()V

    return-object v0
.end method

.method public bridge synthetic newInstance()Ljava/lang/Object;
    .locals 1

    invoke-virtual {p0}, Lcom/google/api/services/plus/model/Person_CoverJson;->newInstance()Lcom/google/api/services/plus/model/Person$Cover;

    move-result-object v0

    return-object v0
.end method
