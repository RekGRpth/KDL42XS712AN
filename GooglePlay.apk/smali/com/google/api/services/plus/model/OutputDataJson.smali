.class public final Lcom/google/api/services/plus/model/OutputDataJson;
.super Lcom/google/android/apps/plus/json/EsJson;
.source "OutputDataJson.java"


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/google/android/apps/plus/json/EsJson",
        "<",
        "Lcom/google/api/services/plus/model/OutputData;",
        ">;"
    }
.end annotation


# static fields
.field static final INSTANCE:Lcom/google/api/services/plus/model/OutputDataJson;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    new-instance v0, Lcom/google/api/services/plus/model/OutputDataJson;

    invoke-direct {v0}, Lcom/google/api/services/plus/model/OutputDataJson;-><init>()V

    sput-object v0, Lcom/google/api/services/plus/model/OutputDataJson;->INSTANCE:Lcom/google/api/services/plus/model/OutputDataJson;

    return-void
.end method

.method private constructor <init>()V
    .locals 4

    const-class v0, Lcom/google/api/services/plus/model/OutputData;

    const/16 v1, 0x18

    new-array v1, v1, [Ljava/lang/Object;

    const/4 v2, 0x0

    const-class v3, Lcom/google/api/services/plus/model/LoggedProfileJson;

    aput-object v3, v1, v2

    const/4 v2, 0x1

    const-string v3, "profile"

    aput-object v3, v1, v2

    const/4 v2, 0x2

    const-string v3, "tab"

    aput-object v3, v1, v2

    const/4 v2, 0x3

    const-class v3, Lcom/google/api/services/plus/model/LoggedCircleJson;

    aput-object v3, v1, v2

    const/4 v2, 0x4

    const-string v3, "filterCircle"

    aput-object v3, v1, v2

    const/4 v2, 0x5

    const-class v3, Lcom/google/api/services/plus/model/LoggedSuggestionInfoJson;

    aput-object v3, v1, v2

    const/4 v2, 0x6

    const-string v3, "suggestionInfo"

    aput-object v3, v1, v2

    const/4 v2, 0x7

    sget-object v3, Lcom/google/api/services/plus/model/OutputDataJson;->JSON_STRING:Ljava/lang/Object;

    aput-object v3, v1, v2

    const/16 v2, 0x8

    const-string v3, "photoAlbumId"

    aput-object v3, v1, v2

    const/16 v2, 0x9

    const-class v3, Lcom/google/api/services/plus/model/LoggedPhotoJson;

    aput-object v3, v1, v2

    const/16 v2, 0xa

    const-string v3, "photo"

    aput-object v3, v1, v2

    const/16 v2, 0xb

    const-string v3, "filterType"

    aput-object v3, v1, v2

    const/16 v2, 0xc

    const-class v3, Lcom/google/api/services/plus/model/LoggedUpdateJson;

    aput-object v3, v1, v2

    const/16 v2, 0xd

    const-string v3, "update"

    aput-object v3, v1, v2

    const/16 v2, 0xe

    const-string v3, "streamSort"

    aput-object v3, v1, v2

    const/16 v2, 0xf

    const-class v3, Lcom/google/api/services/plus/model/UserInfoJson;

    aput-object v3, v1, v2

    const/16 v2, 0x10

    const-string v3, "userInfo"

    aput-object v3, v1, v2

    const/16 v2, 0x11

    const-string v3, "interest"

    aput-object v3, v1, v2

    const/16 v2, 0x12

    const-string v3, "getStartedStepIndex"

    aput-object v3, v1, v2

    const/16 v2, 0x13

    const-class v3, Lcom/google/api/services/plus/model/LoggedCircleJson;

    aput-object v3, v1, v2

    const/16 v2, 0x14

    const-string v3, "circle"

    aput-object v3, v1, v2

    const/16 v2, 0x15

    const-string v3, "containerPropertyId"

    aput-object v3, v1, v2

    const/16 v2, 0x16

    const-class v3, Lcom/google/api/services/plus/model/LoggedCircleMemberJson;

    aput-object v3, v1, v2

    const/16 v2, 0x17

    const-string v3, "circleMember"

    aput-object v3, v1, v2

    invoke-direct {p0, v0, v1}, Lcom/google/android/apps/plus/json/EsJson;-><init>(Ljava/lang/Class;[Ljava/lang/Object;)V

    return-void
.end method

.method public static getInstance()Lcom/google/api/services/plus/model/OutputDataJson;
    .locals 1

    sget-object v0, Lcom/google/api/services/plus/model/OutputDataJson;->INSTANCE:Lcom/google/api/services/plus/model/OutputDataJson;

    return-object v0
.end method


# virtual methods
.method public getValues(Lcom/google/api/services/plus/model/OutputData;)[Ljava/lang/Object;
    .locals 3
    .param p1    # Lcom/google/api/services/plus/model/OutputData;

    const/16 v0, 0xf

    new-array v0, v0, [Ljava/lang/Object;

    const/4 v1, 0x0

    iget-object v2, p1, Lcom/google/api/services/plus/model/OutputData;->profile:Lcom/google/api/services/plus/model/LoggedProfile;

    aput-object v2, v0, v1

    const/4 v1, 0x1

    iget-object v2, p1, Lcom/google/api/services/plus/model/OutputData;->tab:Ljava/lang/Integer;

    aput-object v2, v0, v1

    const/4 v1, 0x2

    iget-object v2, p1, Lcom/google/api/services/plus/model/OutputData;->filterCircle:Lcom/google/api/services/plus/model/LoggedCircle;

    aput-object v2, v0, v1

    const/4 v1, 0x3

    iget-object v2, p1, Lcom/google/api/services/plus/model/OutputData;->suggestionInfo:Ljava/util/List;

    aput-object v2, v0, v1

    const/4 v1, 0x4

    iget-object v2, p1, Lcom/google/api/services/plus/model/OutputData;->photoAlbumId:Ljava/util/List;

    aput-object v2, v0, v1

    const/4 v1, 0x5

    iget-object v2, p1, Lcom/google/api/services/plus/model/OutputData;->photo:Ljava/util/List;

    aput-object v2, v0, v1

    const/4 v1, 0x6

    iget-object v2, p1, Lcom/google/api/services/plus/model/OutputData;->filterType:Ljava/lang/Integer;

    aput-object v2, v0, v1

    const/4 v1, 0x7

    iget-object v2, p1, Lcom/google/api/services/plus/model/OutputData;->update:Ljava/util/List;

    aput-object v2, v0, v1

    const/16 v1, 0x8

    iget-object v2, p1, Lcom/google/api/services/plus/model/OutputData;->streamSort:Ljava/lang/String;

    aput-object v2, v0, v1

    const/16 v1, 0x9

    iget-object v2, p1, Lcom/google/api/services/plus/model/OutputData;->userInfo:Ljava/util/List;

    aput-object v2, v0, v1

    const/16 v1, 0xa

    iget-object v2, p1, Lcom/google/api/services/plus/model/OutputData;->interest:Ljava/lang/String;

    aput-object v2, v0, v1

    const/16 v1, 0xb

    iget-object v2, p1, Lcom/google/api/services/plus/model/OutputData;->getStartedStepIndex:Ljava/lang/Integer;

    aput-object v2, v0, v1

    const/16 v1, 0xc

    iget-object v2, p1, Lcom/google/api/services/plus/model/OutputData;->circle:Ljava/util/List;

    aput-object v2, v0, v1

    const/16 v1, 0xd

    iget-object v2, p1, Lcom/google/api/services/plus/model/OutputData;->containerPropertyId:Ljava/lang/String;

    aput-object v2, v0, v1

    const/16 v1, 0xe

    iget-object v2, p1, Lcom/google/api/services/plus/model/OutputData;->circleMember:Ljava/util/List;

    aput-object v2, v0, v1

    return-object v0
.end method

.method public bridge synthetic getValues(Ljava/lang/Object;)[Ljava/lang/Object;
    .locals 1
    .param p1    # Ljava/lang/Object;

    check-cast p1, Lcom/google/api/services/plus/model/OutputData;

    invoke-virtual {p0, p1}, Lcom/google/api/services/plus/model/OutputDataJson;->getValues(Lcom/google/api/services/plus/model/OutputData;)[Ljava/lang/Object;

    move-result-object v0

    return-object v0
.end method

.method public newInstance()Lcom/google/api/services/plus/model/OutputData;
    .locals 1

    new-instance v0, Lcom/google/api/services/plus/model/OutputData;

    invoke-direct {v0}, Lcom/google/api/services/plus/model/OutputData;-><init>()V

    return-object v0
.end method

.method public bridge synthetic newInstance()Ljava/lang/Object;
    .locals 1

    invoke-virtual {p0}, Lcom/google/api/services/plus/model/OutputDataJson;->newInstance()Lcom/google/api/services/plus/model/OutputData;

    move-result-object v0

    return-object v0
.end method
