.class public final Lcom/google/api/services/plus/model/LoggedRhsComponent;
.super Lcom/google/android/apps/plus/json/GenericJson;
.source "LoggedRhsComponent.java"


# instance fields
.field public barType:Ljava/lang/Integer;

.field public componentType:Lcom/google/api/services/plus/model/LoggedRhsComponentType;

.field public index:Ljava/lang/Integer;

.field public item:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Lcom/google/api/services/plus/model/LoggedRhsComponentItem;",
            ">;"
        }
    .end annotation
.end field

.field public neighborInfo:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Lcom/google/api/services/plus/model/LoggedRhsComponentType;",
            ">;"
        }
    .end annotation
.end field

.field public suggestionSummaryInfo:Lcom/google/api/services/plus/model/LoggedSuggestionSummaryInfo;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    const-class v0, Lcom/google/api/services/plus/model/LoggedRhsComponentType;

    invoke-static {v0}, Lcom/google/android/apps/plus/json/Data;->nullOf(Ljava/lang/Class;)Ljava/lang/Object;

    const-class v0, Lcom/google/api/services/plus/model/LoggedRhsComponentItem;

    invoke-static {v0}, Lcom/google/android/apps/plus/json/Data;->nullOf(Ljava/lang/Class;)Ljava/lang/Object;

    return-void
.end method

.method public constructor <init>()V
    .locals 0

    invoke-direct {p0}, Lcom/google/android/apps/plus/json/GenericJson;-><init>()V

    return-void
.end method
