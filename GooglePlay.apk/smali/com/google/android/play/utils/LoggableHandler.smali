.class public Lcom/google/android/play/utils/LoggableHandler;
.super Landroid/os/Handler;
.source "LoggableHandler.java"


# direct methods
.method public constructor <init>(Ljava/lang/String;)V
    .locals 1
    .param p1    # Ljava/lang/String;

    const/16 v0, 0xa

    invoke-direct {p0, p1, v0}, Lcom/google/android/play/utils/LoggableHandler;-><init>(Ljava/lang/String;I)V

    return-void
.end method

.method private constructor <init>(Ljava/lang/String;I)V
    .locals 1
    .param p1    # Ljava/lang/String;
    .param p2    # I

    invoke-static {p1, p2}, Lcom/google/android/play/utils/LoggableHandler;->startHandlerThread(Ljava/lang/String;I)Landroid/os/HandlerThread;

    move-result-object v0

    invoke-virtual {v0}, Landroid/os/HandlerThread;->getLooper()Landroid/os/Looper;

    move-result-object v0

    invoke-direct {p0, v0}, Landroid/os/Handler;-><init>(Landroid/os/Looper;)V

    return-void
.end method

.method private static startHandlerThread(Ljava/lang/String;I)Landroid/os/HandlerThread;
    .locals 3
    .param p0    # Ljava/lang/String;
    .param p1    # I

    new-instance v1, Ljava/util/concurrent/Semaphore;

    const/4 v2, 0x0

    invoke-direct {v1, v2}, Ljava/util/concurrent/Semaphore;-><init>(I)V

    new-instance v0, Lcom/google/android/play/utils/LoggableHandler$1;

    invoke-direct {v0, p0, p1, v1}, Lcom/google/android/play/utils/LoggableHandler$1;-><init>(Ljava/lang/String;ILjava/util/concurrent/Semaphore;)V

    invoke-virtual {v0}, Landroid/os/HandlerThread;->start()V

    invoke-virtual {v1}, Ljava/util/concurrent/Semaphore;->acquireUninterruptibly()V

    return-object v0
.end method


# virtual methods
.method public dispatchMessage(Landroid/os/Message;)V
    .locals 0
    .param p1    # Landroid/os/Message;

    invoke-super {p0, p1}, Landroid/os/Handler;->dispatchMessage(Landroid/os/Message;)V

    return-void
.end method

.method public sendMessageAtTime(Landroid/os/Message;J)Z
    .locals 1
    .param p1    # Landroid/os/Message;
    .param p2    # J

    invoke-super {p0, p1, p2, p3}, Landroid/os/Handler;->sendMessageAtTime(Landroid/os/Message;J)Z

    move-result v0

    return v0
.end method
