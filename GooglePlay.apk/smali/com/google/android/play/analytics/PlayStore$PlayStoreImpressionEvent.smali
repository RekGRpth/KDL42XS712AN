.class public final Lcom/google/android/play/analytics/PlayStore$PlayStoreImpressionEvent;
.super Lcom/google/protobuf/micro/MessageMicro;
.source "PlayStore.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/google/android/play/analytics/PlayStore;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x19
    name = "PlayStoreImpressionEvent"
.end annotation


# instance fields
.field private cachedSize:I

.field private hasId:Z

.field private hasTree:Z

.field private id_:J

.field private referrerPath_:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Lcom/google/android/play/analytics/PlayStore$PlayStoreUiElement;",
            ">;"
        }
    .end annotation
.end field

.field private tree_:Lcom/google/android/play/analytics/PlayStore$PlayStoreUiElement;


# direct methods
.method public constructor <init>()V
    .locals 2

    invoke-direct {p0}, Lcom/google/protobuf/micro/MessageMicro;-><init>()V

    const/4 v0, 0x0

    iput-object v0, p0, Lcom/google/android/play/analytics/PlayStore$PlayStoreImpressionEvent;->tree_:Lcom/google/android/play/analytics/PlayStore$PlayStoreUiElement;

    invoke-static {}, Ljava/util/Collections;->emptyList()Ljava/util/List;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/play/analytics/PlayStore$PlayStoreImpressionEvent;->referrerPath_:Ljava/util/List;

    const-wide/16 v0, 0x0

    iput-wide v0, p0, Lcom/google/android/play/analytics/PlayStore$PlayStoreImpressionEvent;->id_:J

    const/4 v0, -0x1

    iput v0, p0, Lcom/google/android/play/analytics/PlayStore$PlayStoreImpressionEvent;->cachedSize:I

    return-void
.end method


# virtual methods
.method public addReferrerPath(Lcom/google/android/play/analytics/PlayStore$PlayStoreUiElement;)Lcom/google/android/play/analytics/PlayStore$PlayStoreImpressionEvent;
    .locals 1
    .param p1    # Lcom/google/android/play/analytics/PlayStore$PlayStoreUiElement;

    if-nez p1, :cond_0

    new-instance v0, Ljava/lang/NullPointerException;

    invoke-direct {v0}, Ljava/lang/NullPointerException;-><init>()V

    throw v0

    :cond_0
    iget-object v0, p0, Lcom/google/android/play/analytics/PlayStore$PlayStoreImpressionEvent;->referrerPath_:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->isEmpty()Z

    move-result v0

    if-eqz v0, :cond_1

    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, Lcom/google/android/play/analytics/PlayStore$PlayStoreImpressionEvent;->referrerPath_:Ljava/util/List;

    :cond_1
    iget-object v0, p0, Lcom/google/android/play/analytics/PlayStore$PlayStoreImpressionEvent;->referrerPath_:Ljava/util/List;

    invoke-interface {v0, p1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    return-object p0
.end method

.method public final clear()Lcom/google/android/play/analytics/PlayStore$PlayStoreImpressionEvent;
    .locals 1

    invoke-virtual {p0}, Lcom/google/android/play/analytics/PlayStore$PlayStoreImpressionEvent;->clearTree()Lcom/google/android/play/analytics/PlayStore$PlayStoreImpressionEvent;

    invoke-virtual {p0}, Lcom/google/android/play/analytics/PlayStore$PlayStoreImpressionEvent;->clearReferrerPath()Lcom/google/android/play/analytics/PlayStore$PlayStoreImpressionEvent;

    invoke-virtual {p0}, Lcom/google/android/play/analytics/PlayStore$PlayStoreImpressionEvent;->clearId()Lcom/google/android/play/analytics/PlayStore$PlayStoreImpressionEvent;

    const/4 v0, -0x1

    iput v0, p0, Lcom/google/android/play/analytics/PlayStore$PlayStoreImpressionEvent;->cachedSize:I

    return-object p0
.end method

.method public clearId()Lcom/google/android/play/analytics/PlayStore$PlayStoreImpressionEvent;
    .locals 2

    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/google/android/play/analytics/PlayStore$PlayStoreImpressionEvent;->hasId:Z

    const-wide/16 v0, 0x0

    iput-wide v0, p0, Lcom/google/android/play/analytics/PlayStore$PlayStoreImpressionEvent;->id_:J

    return-object p0
.end method

.method public clearReferrerPath()Lcom/google/android/play/analytics/PlayStore$PlayStoreImpressionEvent;
    .locals 1

    invoke-static {}, Ljava/util/Collections;->emptyList()Ljava/util/List;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/play/analytics/PlayStore$PlayStoreImpressionEvent;->referrerPath_:Ljava/util/List;

    return-object p0
.end method

.method public clearTree()Lcom/google/android/play/analytics/PlayStore$PlayStoreImpressionEvent;
    .locals 1

    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/google/android/play/analytics/PlayStore$PlayStoreImpressionEvent;->hasTree:Z

    const/4 v0, 0x0

    iput-object v0, p0, Lcom/google/android/play/analytics/PlayStore$PlayStoreImpressionEvent;->tree_:Lcom/google/android/play/analytics/PlayStore$PlayStoreUiElement;

    return-object p0
.end method

.method public getCachedSize()I
    .locals 1

    iget v0, p0, Lcom/google/android/play/analytics/PlayStore$PlayStoreImpressionEvent;->cachedSize:I

    if-gez v0, :cond_0

    invoke-virtual {p0}, Lcom/google/android/play/analytics/PlayStore$PlayStoreImpressionEvent;->getSerializedSize()I

    :cond_0
    iget v0, p0, Lcom/google/android/play/analytics/PlayStore$PlayStoreImpressionEvent;->cachedSize:I

    return v0
.end method

.method public getId()J
    .locals 2

    iget-wide v0, p0, Lcom/google/android/play/analytics/PlayStore$PlayStoreImpressionEvent;->id_:J

    return-wide v0
.end method

.method public getReferrerPathList()Ljava/util/List;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/List",
            "<",
            "Lcom/google/android/play/analytics/PlayStore$PlayStoreUiElement;",
            ">;"
        }
    .end annotation

    iget-object v0, p0, Lcom/google/android/play/analytics/PlayStore$PlayStoreImpressionEvent;->referrerPath_:Ljava/util/List;

    return-object v0
.end method

.method public getSerializedSize()I
    .locals 6

    const/4 v2, 0x0

    invoke-virtual {p0}, Lcom/google/android/play/analytics/PlayStore$PlayStoreImpressionEvent;->hasTree()Z

    move-result v3

    if-eqz v3, :cond_0

    const/4 v3, 0x1

    invoke-virtual {p0}, Lcom/google/android/play/analytics/PlayStore$PlayStoreImpressionEvent;->getTree()Lcom/google/android/play/analytics/PlayStore$PlayStoreUiElement;

    move-result-object v4

    invoke-static {v3, v4}, Lcom/google/protobuf/micro/CodedOutputStreamMicro;->computeMessageSize(ILcom/google/protobuf/micro/MessageMicro;)I

    move-result v3

    add-int/2addr v2, v3

    :cond_0
    invoke-virtual {p0}, Lcom/google/android/play/analytics/PlayStore$PlayStoreImpressionEvent;->getReferrerPathList()Ljava/util/List;

    move-result-object v3

    invoke-interface {v3}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :goto_0
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v3

    if-eqz v3, :cond_1

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/play/analytics/PlayStore$PlayStoreUiElement;

    const/4 v3, 0x2

    invoke-static {v3, v0}, Lcom/google/protobuf/micro/CodedOutputStreamMicro;->computeMessageSize(ILcom/google/protobuf/micro/MessageMicro;)I

    move-result v3

    add-int/2addr v2, v3

    goto :goto_0

    :cond_1
    invoke-virtual {p0}, Lcom/google/android/play/analytics/PlayStore$PlayStoreImpressionEvent;->hasId()Z

    move-result v3

    if-eqz v3, :cond_2

    const/4 v3, 0x3

    invoke-virtual {p0}, Lcom/google/android/play/analytics/PlayStore$PlayStoreImpressionEvent;->getId()J

    move-result-wide v4

    invoke-static {v3, v4, v5}, Lcom/google/protobuf/micro/CodedOutputStreamMicro;->computeInt64Size(IJ)I

    move-result v3

    add-int/2addr v2, v3

    :cond_2
    iput v2, p0, Lcom/google/android/play/analytics/PlayStore$PlayStoreImpressionEvent;->cachedSize:I

    return v2
.end method

.method public getTree()Lcom/google/android/play/analytics/PlayStore$PlayStoreUiElement;
    .locals 1

    iget-object v0, p0, Lcom/google/android/play/analytics/PlayStore$PlayStoreImpressionEvent;->tree_:Lcom/google/android/play/analytics/PlayStore$PlayStoreUiElement;

    return-object v0
.end method

.method public hasId()Z
    .locals 1

    iget-boolean v0, p0, Lcom/google/android/play/analytics/PlayStore$PlayStoreImpressionEvent;->hasId:Z

    return v0
.end method

.method public hasTree()Z
    .locals 1

    iget-boolean v0, p0, Lcom/google/android/play/analytics/PlayStore$PlayStoreImpressionEvent;->hasTree:Z

    return v0
.end method

.method public mergeFrom(Lcom/google/protobuf/micro/CodedInputStreamMicro;)Lcom/google/android/play/analytics/PlayStore$PlayStoreImpressionEvent;
    .locals 4
    .param p1    # Lcom/google/protobuf/micro/CodedInputStreamMicro;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    :cond_0
    :goto_0
    invoke-virtual {p1}, Lcom/google/protobuf/micro/CodedInputStreamMicro;->readTag()I

    move-result v0

    sparse-switch v0, :sswitch_data_0

    invoke-virtual {p0, p1, v0}, Lcom/google/android/play/analytics/PlayStore$PlayStoreImpressionEvent;->parseUnknownField(Lcom/google/protobuf/micro/CodedInputStreamMicro;I)Z

    move-result v2

    if-nez v2, :cond_0

    :sswitch_0
    return-object p0

    :sswitch_1
    new-instance v1, Lcom/google/android/play/analytics/PlayStore$PlayStoreUiElement;

    invoke-direct {v1}, Lcom/google/android/play/analytics/PlayStore$PlayStoreUiElement;-><init>()V

    invoke-virtual {p1, v1}, Lcom/google/protobuf/micro/CodedInputStreamMicro;->readMessage(Lcom/google/protobuf/micro/MessageMicro;)V

    invoke-virtual {p0, v1}, Lcom/google/android/play/analytics/PlayStore$PlayStoreImpressionEvent;->setTree(Lcom/google/android/play/analytics/PlayStore$PlayStoreUiElement;)Lcom/google/android/play/analytics/PlayStore$PlayStoreImpressionEvent;

    goto :goto_0

    :sswitch_2
    new-instance v1, Lcom/google/android/play/analytics/PlayStore$PlayStoreUiElement;

    invoke-direct {v1}, Lcom/google/android/play/analytics/PlayStore$PlayStoreUiElement;-><init>()V

    invoke-virtual {p1, v1}, Lcom/google/protobuf/micro/CodedInputStreamMicro;->readMessage(Lcom/google/protobuf/micro/MessageMicro;)V

    invoke-virtual {p0, v1}, Lcom/google/android/play/analytics/PlayStore$PlayStoreImpressionEvent;->addReferrerPath(Lcom/google/android/play/analytics/PlayStore$PlayStoreUiElement;)Lcom/google/android/play/analytics/PlayStore$PlayStoreImpressionEvent;

    goto :goto_0

    :sswitch_3
    invoke-virtual {p1}, Lcom/google/protobuf/micro/CodedInputStreamMicro;->readInt64()J

    move-result-wide v2

    invoke-virtual {p0, v2, v3}, Lcom/google/android/play/analytics/PlayStore$PlayStoreImpressionEvent;->setId(J)Lcom/google/android/play/analytics/PlayStore$PlayStoreImpressionEvent;

    goto :goto_0

    :sswitch_data_0
    .sparse-switch
        0x0 -> :sswitch_0
        0xa -> :sswitch_1
        0x12 -> :sswitch_2
        0x18 -> :sswitch_3
    .end sparse-switch
.end method

.method public bridge synthetic mergeFrom(Lcom/google/protobuf/micro/CodedInputStreamMicro;)Lcom/google/protobuf/micro/MessageMicro;
    .locals 1
    .param p1    # Lcom/google/protobuf/micro/CodedInputStreamMicro;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    invoke-virtual {p0, p1}, Lcom/google/android/play/analytics/PlayStore$PlayStoreImpressionEvent;->mergeFrom(Lcom/google/protobuf/micro/CodedInputStreamMicro;)Lcom/google/android/play/analytics/PlayStore$PlayStoreImpressionEvent;

    move-result-object v0

    return-object v0
.end method

.method public setId(J)Lcom/google/android/play/analytics/PlayStore$PlayStoreImpressionEvent;
    .locals 1
    .param p1    # J

    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/google/android/play/analytics/PlayStore$PlayStoreImpressionEvent;->hasId:Z

    iput-wide p1, p0, Lcom/google/android/play/analytics/PlayStore$PlayStoreImpressionEvent;->id_:J

    return-object p0
.end method

.method public setTree(Lcom/google/android/play/analytics/PlayStore$PlayStoreUiElement;)Lcom/google/android/play/analytics/PlayStore$PlayStoreImpressionEvent;
    .locals 1
    .param p1    # Lcom/google/android/play/analytics/PlayStore$PlayStoreUiElement;

    if-nez p1, :cond_0

    new-instance v0, Ljava/lang/NullPointerException;

    invoke-direct {v0}, Ljava/lang/NullPointerException;-><init>()V

    throw v0

    :cond_0
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/google/android/play/analytics/PlayStore$PlayStoreImpressionEvent;->hasTree:Z

    iput-object p1, p0, Lcom/google/android/play/analytics/PlayStore$PlayStoreImpressionEvent;->tree_:Lcom/google/android/play/analytics/PlayStore$PlayStoreUiElement;

    return-object p0
.end method

.method public writeTo(Lcom/google/protobuf/micro/CodedOutputStreamMicro;)V
    .locals 5
    .param p1    # Lcom/google/protobuf/micro/CodedOutputStreamMicro;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    invoke-virtual {p0}, Lcom/google/android/play/analytics/PlayStore$PlayStoreImpressionEvent;->hasTree()Z

    move-result v2

    if-eqz v2, :cond_0

    const/4 v2, 0x1

    invoke-virtual {p0}, Lcom/google/android/play/analytics/PlayStore$PlayStoreImpressionEvent;->getTree()Lcom/google/android/play/analytics/PlayStore$PlayStoreUiElement;

    move-result-object v3

    invoke-virtual {p1, v2, v3}, Lcom/google/protobuf/micro/CodedOutputStreamMicro;->writeMessage(ILcom/google/protobuf/micro/MessageMicro;)V

    :cond_0
    invoke-virtual {p0}, Lcom/google/android/play/analytics/PlayStore$PlayStoreImpressionEvent;->getReferrerPathList()Ljava/util/List;

    move-result-object v2

    invoke-interface {v2}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :goto_0
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v2

    if-eqz v2, :cond_1

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/play/analytics/PlayStore$PlayStoreUiElement;

    const/4 v2, 0x2

    invoke-virtual {p1, v2, v0}, Lcom/google/protobuf/micro/CodedOutputStreamMicro;->writeMessage(ILcom/google/protobuf/micro/MessageMicro;)V

    goto :goto_0

    :cond_1
    invoke-virtual {p0}, Lcom/google/android/play/analytics/PlayStore$PlayStoreImpressionEvent;->hasId()Z

    move-result v2

    if-eqz v2, :cond_2

    const/4 v2, 0x3

    invoke-virtual {p0}, Lcom/google/android/play/analytics/PlayStore$PlayStoreImpressionEvent;->getId()J

    move-result-wide v3

    invoke-virtual {p1, v2, v3, v4}, Lcom/google/protobuf/micro/CodedOutputStreamMicro;->writeInt64(IJ)V

    :cond_2
    return-void
.end method
