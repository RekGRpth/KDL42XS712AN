.class public final Lcom/google/android/play/analytics/PlayStore$PlayStoreSearchEvent;
.super Lcom/google/protobuf/micro/MessageMicro;
.source "PlayStore.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/google/android/play/analytics/PlayStore;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x19
    name = "PlayStoreSearchEvent"
.end annotation


# instance fields
.field private cachedSize:I

.field private hasQuery:Z

.field private hasQueryUrl:Z

.field private hasReferrerUrl:Z

.field private queryUrl_:Ljava/lang/String;

.field private query_:Ljava/lang/String;

.field private referrerUrl_:Ljava/lang/String;


# direct methods
.method public constructor <init>()V
    .locals 1

    invoke-direct {p0}, Lcom/google/protobuf/micro/MessageMicro;-><init>()V

    const-string v0, ""

    iput-object v0, p0, Lcom/google/android/play/analytics/PlayStore$PlayStoreSearchEvent;->query_:Ljava/lang/String;

    const-string v0, ""

    iput-object v0, p0, Lcom/google/android/play/analytics/PlayStore$PlayStoreSearchEvent;->queryUrl_:Ljava/lang/String;

    const-string v0, ""

    iput-object v0, p0, Lcom/google/android/play/analytics/PlayStore$PlayStoreSearchEvent;->referrerUrl_:Ljava/lang/String;

    const/4 v0, -0x1

    iput v0, p0, Lcom/google/android/play/analytics/PlayStore$PlayStoreSearchEvent;->cachedSize:I

    return-void
.end method


# virtual methods
.method public final clear()Lcom/google/android/play/analytics/PlayStore$PlayStoreSearchEvent;
    .locals 1

    invoke-virtual {p0}, Lcom/google/android/play/analytics/PlayStore$PlayStoreSearchEvent;->clearQuery()Lcom/google/android/play/analytics/PlayStore$PlayStoreSearchEvent;

    invoke-virtual {p0}, Lcom/google/android/play/analytics/PlayStore$PlayStoreSearchEvent;->clearQueryUrl()Lcom/google/android/play/analytics/PlayStore$PlayStoreSearchEvent;

    invoke-virtual {p0}, Lcom/google/android/play/analytics/PlayStore$PlayStoreSearchEvent;->clearReferrerUrl()Lcom/google/android/play/analytics/PlayStore$PlayStoreSearchEvent;

    const/4 v0, -0x1

    iput v0, p0, Lcom/google/android/play/analytics/PlayStore$PlayStoreSearchEvent;->cachedSize:I

    return-object p0
.end method

.method public clearQuery()Lcom/google/android/play/analytics/PlayStore$PlayStoreSearchEvent;
    .locals 1

    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/google/android/play/analytics/PlayStore$PlayStoreSearchEvent;->hasQuery:Z

    const-string v0, ""

    iput-object v0, p0, Lcom/google/android/play/analytics/PlayStore$PlayStoreSearchEvent;->query_:Ljava/lang/String;

    return-object p0
.end method

.method public clearQueryUrl()Lcom/google/android/play/analytics/PlayStore$PlayStoreSearchEvent;
    .locals 1

    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/google/android/play/analytics/PlayStore$PlayStoreSearchEvent;->hasQueryUrl:Z

    const-string v0, ""

    iput-object v0, p0, Lcom/google/android/play/analytics/PlayStore$PlayStoreSearchEvent;->queryUrl_:Ljava/lang/String;

    return-object p0
.end method

.method public clearReferrerUrl()Lcom/google/android/play/analytics/PlayStore$PlayStoreSearchEvent;
    .locals 1

    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/google/android/play/analytics/PlayStore$PlayStoreSearchEvent;->hasReferrerUrl:Z

    const-string v0, ""

    iput-object v0, p0, Lcom/google/android/play/analytics/PlayStore$PlayStoreSearchEvent;->referrerUrl_:Ljava/lang/String;

    return-object p0
.end method

.method public getCachedSize()I
    .locals 1

    iget v0, p0, Lcom/google/android/play/analytics/PlayStore$PlayStoreSearchEvent;->cachedSize:I

    if-gez v0, :cond_0

    invoke-virtual {p0}, Lcom/google/android/play/analytics/PlayStore$PlayStoreSearchEvent;->getSerializedSize()I

    :cond_0
    iget v0, p0, Lcom/google/android/play/analytics/PlayStore$PlayStoreSearchEvent;->cachedSize:I

    return v0
.end method

.method public getQuery()Ljava/lang/String;
    .locals 1

    iget-object v0, p0, Lcom/google/android/play/analytics/PlayStore$PlayStoreSearchEvent;->query_:Ljava/lang/String;

    return-object v0
.end method

.method public getQueryUrl()Ljava/lang/String;
    .locals 1

    iget-object v0, p0, Lcom/google/android/play/analytics/PlayStore$PlayStoreSearchEvent;->queryUrl_:Ljava/lang/String;

    return-object v0
.end method

.method public getReferrerUrl()Ljava/lang/String;
    .locals 1

    iget-object v0, p0, Lcom/google/android/play/analytics/PlayStore$PlayStoreSearchEvent;->referrerUrl_:Ljava/lang/String;

    return-object v0
.end method

.method public getSerializedSize()I
    .locals 3

    const/4 v0, 0x0

    invoke-virtual {p0}, Lcom/google/android/play/analytics/PlayStore$PlayStoreSearchEvent;->hasQuery()Z

    move-result v1

    if-eqz v1, :cond_0

    const/4 v1, 0x1

    invoke-virtual {p0}, Lcom/google/android/play/analytics/PlayStore$PlayStoreSearchEvent;->getQuery()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2}, Lcom/google/protobuf/micro/CodedOutputStreamMicro;->computeStringSize(ILjava/lang/String;)I

    move-result v1

    add-int/2addr v0, v1

    :cond_0
    invoke-virtual {p0}, Lcom/google/android/play/analytics/PlayStore$PlayStoreSearchEvent;->hasQueryUrl()Z

    move-result v1

    if-eqz v1, :cond_1

    const/4 v1, 0x2

    invoke-virtual {p0}, Lcom/google/android/play/analytics/PlayStore$PlayStoreSearchEvent;->getQueryUrl()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2}, Lcom/google/protobuf/micro/CodedOutputStreamMicro;->computeStringSize(ILjava/lang/String;)I

    move-result v1

    add-int/2addr v0, v1

    :cond_1
    invoke-virtual {p0}, Lcom/google/android/play/analytics/PlayStore$PlayStoreSearchEvent;->hasReferrerUrl()Z

    move-result v1

    if-eqz v1, :cond_2

    const/4 v1, 0x3

    invoke-virtual {p0}, Lcom/google/android/play/analytics/PlayStore$PlayStoreSearchEvent;->getReferrerUrl()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2}, Lcom/google/protobuf/micro/CodedOutputStreamMicro;->computeStringSize(ILjava/lang/String;)I

    move-result v1

    add-int/2addr v0, v1

    :cond_2
    iput v0, p0, Lcom/google/android/play/analytics/PlayStore$PlayStoreSearchEvent;->cachedSize:I

    return v0
.end method

.method public hasQuery()Z
    .locals 1

    iget-boolean v0, p0, Lcom/google/android/play/analytics/PlayStore$PlayStoreSearchEvent;->hasQuery:Z

    return v0
.end method

.method public hasQueryUrl()Z
    .locals 1

    iget-boolean v0, p0, Lcom/google/android/play/analytics/PlayStore$PlayStoreSearchEvent;->hasQueryUrl:Z

    return v0
.end method

.method public hasReferrerUrl()Z
    .locals 1

    iget-boolean v0, p0, Lcom/google/android/play/analytics/PlayStore$PlayStoreSearchEvent;->hasReferrerUrl:Z

    return v0
.end method

.method public mergeFrom(Lcom/google/protobuf/micro/CodedInputStreamMicro;)Lcom/google/android/play/analytics/PlayStore$PlayStoreSearchEvent;
    .locals 2
    .param p1    # Lcom/google/protobuf/micro/CodedInputStreamMicro;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    :cond_0
    :goto_0
    invoke-virtual {p1}, Lcom/google/protobuf/micro/CodedInputStreamMicro;->readTag()I

    move-result v0

    sparse-switch v0, :sswitch_data_0

    invoke-virtual {p0, p1, v0}, Lcom/google/android/play/analytics/PlayStore$PlayStoreSearchEvent;->parseUnknownField(Lcom/google/protobuf/micro/CodedInputStreamMicro;I)Z

    move-result v1

    if-nez v1, :cond_0

    :sswitch_0
    return-object p0

    :sswitch_1
    invoke-virtual {p1}, Lcom/google/protobuf/micro/CodedInputStreamMicro;->readString()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {p0, v1}, Lcom/google/android/play/analytics/PlayStore$PlayStoreSearchEvent;->setQuery(Ljava/lang/String;)Lcom/google/android/play/analytics/PlayStore$PlayStoreSearchEvent;

    goto :goto_0

    :sswitch_2
    invoke-virtual {p1}, Lcom/google/protobuf/micro/CodedInputStreamMicro;->readString()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {p0, v1}, Lcom/google/android/play/analytics/PlayStore$PlayStoreSearchEvent;->setQueryUrl(Ljava/lang/String;)Lcom/google/android/play/analytics/PlayStore$PlayStoreSearchEvent;

    goto :goto_0

    :sswitch_3
    invoke-virtual {p1}, Lcom/google/protobuf/micro/CodedInputStreamMicro;->readString()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {p0, v1}, Lcom/google/android/play/analytics/PlayStore$PlayStoreSearchEvent;->setReferrerUrl(Ljava/lang/String;)Lcom/google/android/play/analytics/PlayStore$PlayStoreSearchEvent;

    goto :goto_0

    :sswitch_data_0
    .sparse-switch
        0x0 -> :sswitch_0
        0xa -> :sswitch_1
        0x12 -> :sswitch_2
        0x1a -> :sswitch_3
    .end sparse-switch
.end method

.method public bridge synthetic mergeFrom(Lcom/google/protobuf/micro/CodedInputStreamMicro;)Lcom/google/protobuf/micro/MessageMicro;
    .locals 1
    .param p1    # Lcom/google/protobuf/micro/CodedInputStreamMicro;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    invoke-virtual {p0, p1}, Lcom/google/android/play/analytics/PlayStore$PlayStoreSearchEvent;->mergeFrom(Lcom/google/protobuf/micro/CodedInputStreamMicro;)Lcom/google/android/play/analytics/PlayStore$PlayStoreSearchEvent;

    move-result-object v0

    return-object v0
.end method

.method public setQuery(Ljava/lang/String;)Lcom/google/android/play/analytics/PlayStore$PlayStoreSearchEvent;
    .locals 1
    .param p1    # Ljava/lang/String;

    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/google/android/play/analytics/PlayStore$PlayStoreSearchEvent;->hasQuery:Z

    iput-object p1, p0, Lcom/google/android/play/analytics/PlayStore$PlayStoreSearchEvent;->query_:Ljava/lang/String;

    return-object p0
.end method

.method public setQueryUrl(Ljava/lang/String;)Lcom/google/android/play/analytics/PlayStore$PlayStoreSearchEvent;
    .locals 1
    .param p1    # Ljava/lang/String;

    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/google/android/play/analytics/PlayStore$PlayStoreSearchEvent;->hasQueryUrl:Z

    iput-object p1, p0, Lcom/google/android/play/analytics/PlayStore$PlayStoreSearchEvent;->queryUrl_:Ljava/lang/String;

    return-object p0
.end method

.method public setReferrerUrl(Ljava/lang/String;)Lcom/google/android/play/analytics/PlayStore$PlayStoreSearchEvent;
    .locals 1
    .param p1    # Ljava/lang/String;

    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/google/android/play/analytics/PlayStore$PlayStoreSearchEvent;->hasReferrerUrl:Z

    iput-object p1, p0, Lcom/google/android/play/analytics/PlayStore$PlayStoreSearchEvent;->referrerUrl_:Ljava/lang/String;

    return-object p0
.end method

.method public writeTo(Lcom/google/protobuf/micro/CodedOutputStreamMicro;)V
    .locals 2
    .param p1    # Lcom/google/protobuf/micro/CodedOutputStreamMicro;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    invoke-virtual {p0}, Lcom/google/android/play/analytics/PlayStore$PlayStoreSearchEvent;->hasQuery()Z

    move-result v0

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    invoke-virtual {p0}, Lcom/google/android/play/analytics/PlayStore$PlayStoreSearchEvent;->getQuery()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {p1, v0, v1}, Lcom/google/protobuf/micro/CodedOutputStreamMicro;->writeString(ILjava/lang/String;)V

    :cond_0
    invoke-virtual {p0}, Lcom/google/android/play/analytics/PlayStore$PlayStoreSearchEvent;->hasQueryUrl()Z

    move-result v0

    if-eqz v0, :cond_1

    const/4 v0, 0x2

    invoke-virtual {p0}, Lcom/google/android/play/analytics/PlayStore$PlayStoreSearchEvent;->getQueryUrl()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {p1, v0, v1}, Lcom/google/protobuf/micro/CodedOutputStreamMicro;->writeString(ILjava/lang/String;)V

    :cond_1
    invoke-virtual {p0}, Lcom/google/android/play/analytics/PlayStore$PlayStoreSearchEvent;->hasReferrerUrl()Z

    move-result v0

    if-eqz v0, :cond_2

    const/4 v0, 0x3

    invoke-virtual {p0}, Lcom/google/android/play/analytics/PlayStore$PlayStoreSearchEvent;->getReferrerUrl()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {p1, v0, v1}, Lcom/google/protobuf/micro/CodedOutputStreamMicro;->writeString(ILjava/lang/String;)V

    :cond_2
    return-void
.end method
