.class public Lcom/google/android/play/analytics/RollingFileStream;
.super Ljava/lang/Object;
.source "RollingFileStream.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/google/android/play/analytics/RollingFileStream$RollingFileStreamCallbacks;
    }
.end annotation


# instance fields
.field private mCallbacks:Lcom/google/android/play/analytics/RollingFileStream$RollingFileStreamCallbacks;

.field private mCurrentOutputFile:Ljava/io/File;

.field private final mDirectory:Ljava/io/File;

.field private final mFileNamePrefix:Ljava/lang/String;

.field private final mFileNameSuffix:Ljava/lang/String;

.field private mFileOutputStream:Ljava/io/FileOutputStream;

.field private final mMaxStorageSize:J

.field private final mReadFiles:Ljava/util/ArrayList;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/ArrayList",
            "<",
            "Ljava/io/File;",
            ">;"
        }
    .end annotation
.end field

.field private final mRecommendedFileSize:J

.field private final mWrittenFiles:Ljava/util/ArrayList;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/ArrayList",
            "<",
            "Ljava/io/File;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method public constructor <init>(Ljava/io/File;Ljava/lang/String;Ljava/lang/String;JJLcom/google/android/play/analytics/RollingFileStream$RollingFileStreamCallbacks;)V
    .locals 3
    .param p1    # Ljava/io/File;
    .param p2    # Ljava/lang/String;
    .param p3    # Ljava/lang/String;
    .param p4    # J
    .param p6    # J
    .param p8    # Lcom/google/android/play/analytics/RollingFileStream$RollingFileStreamCallbacks;

    const-wide/16 v1, 0x0

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    cmp-long v0, p4, v1

    if-lez v0, :cond_0

    cmp-long v0, p6, v1

    if-gtz v0, :cond_1

    :cond_0
    new-instance v0, Ljava/lang/IllegalArgumentException;

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, " recommendedFileSize: "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p4, p5}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, " maxStorageSize:"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p6, p7}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v0

    :cond_1
    if-nez p8, :cond_2

    new-instance v0, Ljava/lang/IllegalArgumentException;

    const-string v1, "A callback must be specified."

    invoke-direct {v0, v1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v0

    :cond_2
    iput-object p1, p0, Lcom/google/android/play/analytics/RollingFileStream;->mDirectory:Ljava/io/File;

    iput-object p2, p0, Lcom/google/android/play/analytics/RollingFileStream;->mFileNamePrefix:Ljava/lang/String;

    iput-object p3, p0, Lcom/google/android/play/analytics/RollingFileStream;->mFileNameSuffix:Ljava/lang/String;

    iput-wide p4, p0, Lcom/google/android/play/analytics/RollingFileStream;->mRecommendedFileSize:J

    iput-wide p6, p0, Lcom/google/android/play/analytics/RollingFileStream;->mMaxStorageSize:J

    iput-object p8, p0, Lcom/google/android/play/analytics/RollingFileStream;->mCallbacks:Lcom/google/android/play/analytics/RollingFileStream$RollingFileStreamCallbacks;

    invoke-direct {p0}, Lcom/google/android/play/analytics/RollingFileStream;->createNewOutputFile()V

    iget-object v0, p0, Lcom/google/android/play/analytics/RollingFileStream;->mCurrentOutputFile:Ljava/io/File;

    if-nez v0, :cond_3

    const-string v0, "RollingFileStream"

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "Could not create a temp file with prefix: \""

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget-object v2, p0, Lcom/google/android/play/analytics/RollingFileStream;->mFileNamePrefix:Ljava/lang/String;

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, "\" and suffix: \""

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget-object v2, p0, Lcom/google/android/play/analytics/RollingFileStream;->mFileNameSuffix:Ljava/lang/String;

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, "\" in dir: \""

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget-object v2, p0, Lcom/google/android/play/analytics/RollingFileStream;->mDirectory:Ljava/io/File;

    invoke-virtual {v2}, Ljava/io/File;->getAbsolutePath()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, "\"."

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    :cond_3
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, Lcom/google/android/play/analytics/RollingFileStream;->mWrittenFiles:Ljava/util/ArrayList;

    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, Lcom/google/android/play/analytics/RollingFileStream;->mReadFiles:Ljava/util/ArrayList;

    invoke-direct {p0}, Lcom/google/android/play/analytics/RollingFileStream;->loadWrittenFiles()V

    invoke-direct {p0}, Lcom/google/android/play/analytics/RollingFileStream;->ensureMaxStorageSizeLimit()V

    return-void
.end method

.method private createNewOutputFile()V
    .locals 5

    const/4 v4, 0x0

    iget-object v1, p0, Lcom/google/android/play/analytics/RollingFileStream;->mDirectory:Ljava/io/File;

    invoke-virtual {v1}, Ljava/io/File;->exists()Z

    move-result v1

    if-nez v1, :cond_0

    iget-object v1, p0, Lcom/google/android/play/analytics/RollingFileStream;->mDirectory:Ljava/io/File;

    invoke-virtual {v1}, Ljava/io/File;->mkdirs()Z

    :cond_0
    iput-object v4, p0, Lcom/google/android/play/analytics/RollingFileStream;->mCurrentOutputFile:Ljava/io/File;

    :try_start_0
    iget-object v1, p0, Lcom/google/android/play/analytics/RollingFileStream;->mFileNamePrefix:Ljava/lang/String;

    iget-object v2, p0, Lcom/google/android/play/analytics/RollingFileStream;->mFileNameSuffix:Ljava/lang/String;

    iget-object v3, p0, Lcom/google/android/play/analytics/RollingFileStream;->mDirectory:Ljava/io/File;

    invoke-static {v1, v2, v3}, Ljava/io/File;->createTempFile(Ljava/lang/String;Ljava/lang/String;Ljava/io/File;)Ljava/io/File;

    move-result-object v1

    iput-object v1, p0, Lcom/google/android/play/analytics/RollingFileStream;->mCurrentOutputFile:Ljava/io/File;

    new-instance v1, Ljava/io/FileOutputStream;

    iget-object v2, p0, Lcom/google/android/play/analytics/RollingFileStream;->mCurrentOutputFile:Ljava/io/File;

    invoke-direct {v1, v2}, Ljava/io/FileOutputStream;-><init>(Ljava/io/File;)V

    iput-object v1, p0, Lcom/google/android/play/analytics/RollingFileStream;->mFileOutputStream:Ljava/io/FileOutputStream;

    iget-object v1, p0, Lcom/google/android/play/analytics/RollingFileStream;->mCallbacks:Lcom/google/android/play/analytics/RollingFileStream$RollingFileStreamCallbacks;

    iget-object v2, p0, Lcom/google/android/play/analytics/RollingFileStream;->mFileOutputStream:Ljava/io/FileOutputStream;

    invoke-interface {v1, v2}, Lcom/google/android/play/analytics/RollingFileStream$RollingFileStreamCallbacks;->onNewOutputFile(Ljava/io/OutputStream;)V
    :try_end_0
    .catch Ljava/io/FileNotFoundException; {:try_start_0 .. :try_end_0} :catch_0
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_1

    :goto_0
    return-void

    :catch_0
    move-exception v0

    iget-object v1, p0, Lcom/google/android/play/analytics/RollingFileStream;->mCurrentOutputFile:Ljava/io/File;

    if-eqz v1, :cond_1

    iget-object v1, p0, Lcom/google/android/play/analytics/RollingFileStream;->mCurrentOutputFile:Ljava/io/File;

    invoke-virtual {v1}, Ljava/io/File;->delete()Z

    :cond_1
    iput-object v4, p0, Lcom/google/android/play/analytics/RollingFileStream;->mCurrentOutputFile:Ljava/io/File;

    goto :goto_0

    :catch_1
    move-exception v1

    goto :goto_0
.end method

.method private ensureMaxStorageSizeLimit()V
    .locals 9

    const/4 v7, 0x0

    const-wide/16 v3, 0x0

    iget-object v5, p0, Lcom/google/android/play/analytics/RollingFileStream;->mReadFiles:Ljava/util/ArrayList;

    invoke-virtual {v5}, Ljava/util/ArrayList;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :goto_0
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v5

    if-eqz v5, :cond_0

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/io/File;

    invoke-virtual {v0}, Ljava/io/File;->length()J

    move-result-wide v5

    add-long/2addr v3, v5

    goto :goto_0

    :cond_0
    iget-object v5, p0, Lcom/google/android/play/analytics/RollingFileStream;->mWrittenFiles:Ljava/util/ArrayList;

    invoke-virtual {v5}, Ljava/util/ArrayList;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :goto_1
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v5

    if-eqz v5, :cond_1

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/io/File;

    invoke-virtual {v0}, Ljava/io/File;->length()J

    move-result-wide v5

    add-long/2addr v3, v5

    goto :goto_1

    :cond_1
    iget-object v5, p0, Lcom/google/android/play/analytics/RollingFileStream;->mCurrentOutputFile:Ljava/io/File;

    if-eqz v5, :cond_2

    iget-object v5, p0, Lcom/google/android/play/analytics/RollingFileStream;->mCurrentOutputFile:Ljava/io/File;

    invoke-virtual {v5}, Ljava/io/File;->length()J

    move-result-wide v5

    add-long/2addr v3, v5

    :cond_2
    const/4 v2, 0x0

    :cond_3
    :goto_2
    iget-wide v5, p0, Lcom/google/android/play/analytics/RollingFileStream;->mMaxStorageSize:J

    cmp-long v5, v3, v5

    if-lez v5, :cond_6

    add-int/lit8 v2, v2, 0x1

    iget-object v5, p0, Lcom/google/android/play/analytics/RollingFileStream;->mReadFiles:Ljava/util/ArrayList;

    invoke-virtual {v5}, Ljava/util/ArrayList;->size()I

    move-result v5

    if-lez v5, :cond_4

    iget-object v5, p0, Lcom/google/android/play/analytics/RollingFileStream;->mReadFiles:Ljava/util/ArrayList;

    invoke-virtual {v5, v7}, Ljava/util/ArrayList;->remove(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/io/File;

    invoke-virtual {v0}, Ljava/io/File;->length()J

    move-result-wide v5

    sub-long/2addr v3, v5

    invoke-virtual {v0}, Ljava/io/File;->delete()Z

    goto :goto_2

    :cond_4
    iget-object v5, p0, Lcom/google/android/play/analytics/RollingFileStream;->mWrittenFiles:Ljava/util/ArrayList;

    invoke-virtual {v5}, Ljava/util/ArrayList;->size()I

    move-result v5

    if-lez v5, :cond_5

    iget-object v5, p0, Lcom/google/android/play/analytics/RollingFileStream;->mWrittenFiles:Ljava/util/ArrayList;

    invoke-virtual {v5, v7}, Ljava/util/ArrayList;->remove(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/io/File;

    invoke-virtual {v0}, Ljava/io/File;->length()J

    move-result-wide v5

    sub-long/2addr v3, v5

    invoke-virtual {v0}, Ljava/io/File;->delete()Z

    goto :goto_2

    :cond_5
    iget-object v5, p0, Lcom/google/android/play/analytics/RollingFileStream;->mCurrentOutputFile:Ljava/io/File;

    if-eqz v5, :cond_3

    iget-object v5, p0, Lcom/google/android/play/analytics/RollingFileStream;->mCurrentOutputFile:Ljava/io/File;

    invoke-virtual {v5}, Ljava/io/File;->length()J

    move-result-wide v5

    sub-long/2addr v3, v5

    iget-object v5, p0, Lcom/google/android/play/analytics/RollingFileStream;->mCurrentOutputFile:Ljava/io/File;

    invoke-virtual {v5}, Ljava/io/File;->delete()Z

    const/4 v5, 0x0

    iput-object v5, p0, Lcom/google/android/play/analytics/RollingFileStream;->mCurrentOutputFile:Ljava/io/File;

    goto :goto_2

    :cond_6
    if-lez v2, :cond_7

    const-string v5, "RollingFileStream"

    new-instance v6, Ljava/lang/StringBuilder;

    invoke-direct {v6}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v6, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v6

    const-string v7, " files were purged due to exceeding total storage size of: "

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    iget-wide v7, p0, Lcom/google/android/play/analytics/RollingFileStream;->mMaxStorageSize:J

    invoke-virtual {v6, v7, v8}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v6}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v6

    invoke-static {v5, v6}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    :cond_7
    return-void
.end method

.method private loadWrittenFiles()V
    .locals 9

    iget-object v5, p0, Lcom/google/android/play/analytics/RollingFileStream;->mDirectory:Ljava/io/File;

    invoke-virtual {v5}, Ljava/io/File;->exists()Z

    move-result v5

    if-nez v5, :cond_0

    iget-object v5, p0, Lcom/google/android/play/analytics/RollingFileStream;->mDirectory:Ljava/io/File;

    invoke-virtual {v5}, Ljava/io/File;->mkdirs()Z

    :cond_0
    iget-object v5, p0, Lcom/google/android/play/analytics/RollingFileStream;->mDirectory:Ljava/io/File;

    invoke-virtual {v5}, Ljava/io/File;->isDirectory()Z

    move-result v5

    if-nez v5, :cond_1

    new-instance v5, Ljava/lang/IllegalStateException;

    new-instance v6, Ljava/lang/StringBuilder;

    invoke-direct {v6}, Ljava/lang/StringBuilder;-><init>()V

    const-string v7, "Expected a directory for path: "

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    iget-object v7, p0, Lcom/google/android/play/analytics/RollingFileStream;->mDirectory:Ljava/io/File;

    invoke-virtual {v7}, Ljava/io/File;->getAbsolutePath()Ljava/lang/String;

    move-result-object v7

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v6}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v6

    invoke-direct {v5, v6}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    throw v5

    :cond_1
    iget-object v5, p0, Lcom/google/android/play/analytics/RollingFileStream;->mWrittenFiles:Ljava/util/ArrayList;

    invoke-virtual {v5}, Ljava/util/ArrayList;->clear()V

    iget-object v5, p0, Lcom/google/android/play/analytics/RollingFileStream;->mDirectory:Ljava/io/File;

    invoke-virtual {v5}, Ljava/io/File;->listFiles()[Ljava/io/File;

    move-result-object v1

    move-object v0, v1

    array-length v4, v0

    const/4 v3, 0x0

    :goto_0
    if-ge v3, v4, :cond_4

    aget-object v2, v0, v3

    invoke-virtual {v2}, Ljava/io/File;->isFile()Z

    move-result v5

    if-eqz v5, :cond_2

    iget-object v5, p0, Lcom/google/android/play/analytics/RollingFileStream;->mCurrentOutputFile:Ljava/io/File;

    invoke-virtual {v2, v5}, Ljava/io/File;->equals(Ljava/lang/Object;)Z

    move-result v5

    if-nez v5, :cond_2

    invoke-virtual {v2}, Ljava/io/File;->length()J

    move-result-wide v5

    const-wide/16 v7, 0x0

    cmp-long v5, v5, v7

    if-nez v5, :cond_3

    invoke-virtual {v2}, Ljava/io/File;->delete()Z

    :cond_2
    :goto_1
    add-int/lit8 v3, v3, 0x1

    goto :goto_0

    :cond_3
    iget-object v5, p0, Lcom/google/android/play/analytics/RollingFileStream;->mWrittenFiles:Ljava/util/ArrayList;

    invoke-virtual {v5, v2}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    goto :goto_1

    :cond_4
    iget-object v5, p0, Lcom/google/android/play/analytics/RollingFileStream;->mWrittenFiles:Ljava/util/ArrayList;

    sget-object v6, Lcom/google/android/play/utils/FileModifiedDateComparator;->INSTANCE:Lcom/google/android/play/utils/FileModifiedDateComparator;

    invoke-static {v5, v6}, Ljava/util/Collections;->sort(Ljava/util/List;Ljava/util/Comparator;)V

    return-void
.end method

.method private shouldStartNewOutputFile()Z
    .locals 4

    iget-object v0, p0, Lcom/google/android/play/analytics/RollingFileStream;->mCurrentOutputFile:Ljava/io/File;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/google/android/play/analytics/RollingFileStream;->mCurrentOutputFile:Ljava/io/File;

    invoke-virtual {v0}, Ljava/io/File;->length()J

    move-result-wide v0

    iget-wide v2, p0, Lcom/google/android/play/analytics/RollingFileStream;->mRecommendedFileSize:J

    cmp-long v0, v0, v2

    if-ltz v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method


# virtual methods
.method public deleteAllReadFiles()V
    .locals 3

    iget-object v2, p0, Lcom/google/android/play/analytics/RollingFileStream;->mReadFiles:Ljava/util/ArrayList;

    invoke-virtual {v2}, Ljava/util/ArrayList;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :goto_0
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v2

    if-eqz v2, :cond_0

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/io/File;

    invoke-virtual {v0}, Ljava/io/File;->delete()Z

    goto :goto_0

    :cond_0
    iget-object v2, p0, Lcom/google/android/play/analytics/RollingFileStream;->mReadFiles:Ljava/util/ArrayList;

    invoke-virtual {v2}, Ljava/util/ArrayList;->clear()V

    return-void
.end method

.method public hasUnreadFiles()Z
    .locals 1

    iget-object v0, p0, Lcom/google/android/play/analytics/RollingFileStream;->mWrittenFiles:Ljava/util/ArrayList;

    invoke-virtual {v0}, Ljava/util/ArrayList;->isEmpty()Z

    move-result v0

    if-nez v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public markAllFilesAsUnread()V
    .locals 2

    iget-object v0, p0, Lcom/google/android/play/analytics/RollingFileStream;->mWrittenFiles:Ljava/util/ArrayList;

    iget-object v1, p0, Lcom/google/android/play/analytics/RollingFileStream;->mReadFiles:Ljava/util/ArrayList;

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->addAll(Ljava/util/Collection;)Z

    iget-object v0, p0, Lcom/google/android/play/analytics/RollingFileStream;->mWrittenFiles:Ljava/util/ArrayList;

    sget-object v1, Lcom/google/android/play/utils/FileModifiedDateComparator;->INSTANCE:Lcom/google/android/play/utils/FileModifiedDateComparator;

    invoke-static {v0, v1}, Ljava/util/Collections;->sort(Ljava/util/List;Ljava/util/Comparator;)V

    iget-object v0, p0, Lcom/google/android/play/analytics/RollingFileStream;->mReadFiles:Ljava/util/ArrayList;

    invoke-virtual {v0}, Ljava/util/ArrayList;->clear()V

    return-void
.end method

.method public read(Ljava/lang/Object;)V
    .locals 5
    .param p1    # Ljava/lang/Object;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    iget-object v3, p0, Lcom/google/android/play/analytics/RollingFileStream;->mWrittenFiles:Ljava/util/ArrayList;

    invoke-virtual {v3}, Ljava/util/ArrayList;->isEmpty()Z

    move-result v3

    if-eqz v3, :cond_0

    const-string v3, "RollingFileStream"

    const-string v4, "This method should never be called when there are no written files."

    invoke-static {v3, v4}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    :goto_0
    return-void

    :cond_0
    iget-object v3, p0, Lcom/google/android/play/analytics/RollingFileStream;->mWrittenFiles:Ljava/util/ArrayList;

    const/4 v4, 0x0

    invoke-virtual {v3, v4}, Ljava/util/ArrayList;->remove(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/io/File;

    const/4 v1, 0x0

    :try_start_0
    new-instance v2, Ljava/io/FileInputStream;

    invoke-direct {v2, v0}, Ljava/io/FileInputStream;-><init>(Ljava/io/File;)V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    :try_start_1
    iget-object v3, p0, Lcom/google/android/play/analytics/RollingFileStream;->mCallbacks:Lcom/google/android/play/analytics/RollingFileStream$RollingFileStreamCallbacks;

    invoke-interface {v3, v2, p1}, Lcom/google/android/play/analytics/RollingFileStream$RollingFileStreamCallbacks;->onRead(Ljava/io/InputStream;Ljava/lang/Object;)V
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_1

    if-eqz v2, :cond_1

    invoke-virtual {v2}, Ljava/io/FileInputStream;->close()V

    :cond_1
    iget-object v3, p0, Lcom/google/android/play/analytics/RollingFileStream;->mReadFiles:Ljava/util/ArrayList;

    invoke-virtual {v3, v0}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    goto :goto_0

    :catchall_0
    move-exception v3

    :goto_1
    if-eqz v1, :cond_2

    invoke-virtual {v1}, Ljava/io/FileInputStream;->close()V

    :cond_2
    throw v3

    :catchall_1
    move-exception v3

    move-object v1, v2

    goto :goto_1
.end method

.method public write(Ljava/lang/Object;)V
    .locals 2
    .param p1    # Ljava/lang/Object;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    iget-object v0, p0, Lcom/google/android/play/analytics/RollingFileStream;->mCurrentOutputFile:Ljava/io/File;

    if-nez v0, :cond_1

    invoke-direct {p0}, Lcom/google/android/play/analytics/RollingFileStream;->createNewOutputFile()V

    iget-object v0, p0, Lcom/google/android/play/analytics/RollingFileStream;->mCurrentOutputFile:Ljava/io/File;

    if-nez v0, :cond_1

    :cond_0
    :goto_0
    return-void

    :cond_1
    iget-object v0, p0, Lcom/google/android/play/analytics/RollingFileStream;->mCallbacks:Lcom/google/android/play/analytics/RollingFileStream$RollingFileStreamCallbacks;

    iget-object v1, p0, Lcom/google/android/play/analytics/RollingFileStream;->mFileOutputStream:Ljava/io/FileOutputStream;

    invoke-interface {v0, v1, p1}, Lcom/google/android/play/analytics/RollingFileStream$RollingFileStreamCallbacks;->onWrite(Ljava/io/OutputStream;Ljava/lang/Object;)V

    iget-object v0, p0, Lcom/google/android/play/analytics/RollingFileStream;->mFileOutputStream:Ljava/io/FileOutputStream;

    invoke-virtual {v0}, Ljava/io/FileOutputStream;->flush()V

    invoke-direct {p0}, Lcom/google/android/play/analytics/RollingFileStream;->shouldStartNewOutputFile()Z

    move-result v0

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/google/android/play/analytics/RollingFileStream;->mFileOutputStream:Ljava/io/FileOutputStream;

    invoke-virtual {v0}, Ljava/io/FileOutputStream;->close()V

    iget-object v0, p0, Lcom/google/android/play/analytics/RollingFileStream;->mWrittenFiles:Ljava/util/ArrayList;

    iget-object v1, p0, Lcom/google/android/play/analytics/RollingFileStream;->mCurrentOutputFile:Ljava/io/File;

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    invoke-direct {p0}, Lcom/google/android/play/analytics/RollingFileStream;->createNewOutputFile()V

    invoke-direct {p0}, Lcom/google/android/play/analytics/RollingFileStream;->ensureMaxStorageSizeLimit()V

    goto :goto_0
.end method
