.class public final Lcom/google/android/play/analytics/PlayStore$PlayStoreUiElement;
.super Lcom/google/protobuf/micro/MessageMicro;
.source "PlayStore.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/google/android/play/analytics/PlayStore;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x19
    name = "PlayStoreUiElement"
.end annotation


# instance fields
.field private cachedSize:I

.field private child_:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Lcom/google/android/play/analytics/PlayStore$PlayStoreUiElement;",
            ">;"
        }
    .end annotation
.end field

.field private clientLogsCookie_:Lcom/google/android/play/analytics/PlayStore$PlayStoreUiElementInfo;

.field private hasClientLogsCookie:Z

.field private hasServerLogsCookie:Z

.field private hasType:Z

.field private serverLogsCookie_:Lcom/google/protobuf/micro/ByteStringMicro;

.field private type_:I


# direct methods
.method public constructor <init>()V
    .locals 1

    invoke-direct {p0}, Lcom/google/protobuf/micro/MessageMicro;-><init>()V

    const/4 v0, 0x0

    iput v0, p0, Lcom/google/android/play/analytics/PlayStore$PlayStoreUiElement;->type_:I

    sget-object v0, Lcom/google/protobuf/micro/ByteStringMicro;->EMPTY:Lcom/google/protobuf/micro/ByteStringMicro;

    iput-object v0, p0, Lcom/google/android/play/analytics/PlayStore$PlayStoreUiElement;->serverLogsCookie_:Lcom/google/protobuf/micro/ByteStringMicro;

    const/4 v0, 0x0

    iput-object v0, p0, Lcom/google/android/play/analytics/PlayStore$PlayStoreUiElement;->clientLogsCookie_:Lcom/google/android/play/analytics/PlayStore$PlayStoreUiElementInfo;

    invoke-static {}, Ljava/util/Collections;->emptyList()Ljava/util/List;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/play/analytics/PlayStore$PlayStoreUiElement;->child_:Ljava/util/List;

    const/4 v0, -0x1

    iput v0, p0, Lcom/google/android/play/analytics/PlayStore$PlayStoreUiElement;->cachedSize:I

    return-void
.end method


# virtual methods
.method public addChild(Lcom/google/android/play/analytics/PlayStore$PlayStoreUiElement;)Lcom/google/android/play/analytics/PlayStore$PlayStoreUiElement;
    .locals 1
    .param p1    # Lcom/google/android/play/analytics/PlayStore$PlayStoreUiElement;

    if-nez p1, :cond_0

    new-instance v0, Ljava/lang/NullPointerException;

    invoke-direct {v0}, Ljava/lang/NullPointerException;-><init>()V

    throw v0

    :cond_0
    iget-object v0, p0, Lcom/google/android/play/analytics/PlayStore$PlayStoreUiElement;->child_:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->isEmpty()Z

    move-result v0

    if-eqz v0, :cond_1

    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, Lcom/google/android/play/analytics/PlayStore$PlayStoreUiElement;->child_:Ljava/util/List;

    :cond_1
    iget-object v0, p0, Lcom/google/android/play/analytics/PlayStore$PlayStoreUiElement;->child_:Ljava/util/List;

    invoke-interface {v0, p1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    return-object p0
.end method

.method public final clear()Lcom/google/android/play/analytics/PlayStore$PlayStoreUiElement;
    .locals 1

    invoke-virtual {p0}, Lcom/google/android/play/analytics/PlayStore$PlayStoreUiElement;->clearType()Lcom/google/android/play/analytics/PlayStore$PlayStoreUiElement;

    invoke-virtual {p0}, Lcom/google/android/play/analytics/PlayStore$PlayStoreUiElement;->clearServerLogsCookie()Lcom/google/android/play/analytics/PlayStore$PlayStoreUiElement;

    invoke-virtual {p0}, Lcom/google/android/play/analytics/PlayStore$PlayStoreUiElement;->clearClientLogsCookie()Lcom/google/android/play/analytics/PlayStore$PlayStoreUiElement;

    invoke-virtual {p0}, Lcom/google/android/play/analytics/PlayStore$PlayStoreUiElement;->clearChild()Lcom/google/android/play/analytics/PlayStore$PlayStoreUiElement;

    const/4 v0, -0x1

    iput v0, p0, Lcom/google/android/play/analytics/PlayStore$PlayStoreUiElement;->cachedSize:I

    return-object p0
.end method

.method public clearChild()Lcom/google/android/play/analytics/PlayStore$PlayStoreUiElement;
    .locals 1

    invoke-static {}, Ljava/util/Collections;->emptyList()Ljava/util/List;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/play/analytics/PlayStore$PlayStoreUiElement;->child_:Ljava/util/List;

    return-object p0
.end method

.method public clearClientLogsCookie()Lcom/google/android/play/analytics/PlayStore$PlayStoreUiElement;
    .locals 1

    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/google/android/play/analytics/PlayStore$PlayStoreUiElement;->hasClientLogsCookie:Z

    const/4 v0, 0x0

    iput-object v0, p0, Lcom/google/android/play/analytics/PlayStore$PlayStoreUiElement;->clientLogsCookie_:Lcom/google/android/play/analytics/PlayStore$PlayStoreUiElementInfo;

    return-object p0
.end method

.method public clearServerLogsCookie()Lcom/google/android/play/analytics/PlayStore$PlayStoreUiElement;
    .locals 1

    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/google/android/play/analytics/PlayStore$PlayStoreUiElement;->hasServerLogsCookie:Z

    sget-object v0, Lcom/google/protobuf/micro/ByteStringMicro;->EMPTY:Lcom/google/protobuf/micro/ByteStringMicro;

    iput-object v0, p0, Lcom/google/android/play/analytics/PlayStore$PlayStoreUiElement;->serverLogsCookie_:Lcom/google/protobuf/micro/ByteStringMicro;

    return-object p0
.end method

.method public clearType()Lcom/google/android/play/analytics/PlayStore$PlayStoreUiElement;
    .locals 1

    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/google/android/play/analytics/PlayStore$PlayStoreUiElement;->hasType:Z

    iput v0, p0, Lcom/google/android/play/analytics/PlayStore$PlayStoreUiElement;->type_:I

    return-object p0
.end method

.method public getCachedSize()I
    .locals 1

    iget v0, p0, Lcom/google/android/play/analytics/PlayStore$PlayStoreUiElement;->cachedSize:I

    if-gez v0, :cond_0

    invoke-virtual {p0}, Lcom/google/android/play/analytics/PlayStore$PlayStoreUiElement;->getSerializedSize()I

    :cond_0
    iget v0, p0, Lcom/google/android/play/analytics/PlayStore$PlayStoreUiElement;->cachedSize:I

    return v0
.end method

.method public getChildCount()I
    .locals 1

    iget-object v0, p0, Lcom/google/android/play/analytics/PlayStore$PlayStoreUiElement;->child_:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->size()I

    move-result v0

    return v0
.end method

.method public getChildList()Ljava/util/List;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/List",
            "<",
            "Lcom/google/android/play/analytics/PlayStore$PlayStoreUiElement;",
            ">;"
        }
    .end annotation

    iget-object v0, p0, Lcom/google/android/play/analytics/PlayStore$PlayStoreUiElement;->child_:Ljava/util/List;

    return-object v0
.end method

.method public getClientLogsCookie()Lcom/google/android/play/analytics/PlayStore$PlayStoreUiElementInfo;
    .locals 1

    iget-object v0, p0, Lcom/google/android/play/analytics/PlayStore$PlayStoreUiElement;->clientLogsCookie_:Lcom/google/android/play/analytics/PlayStore$PlayStoreUiElementInfo;

    return-object v0
.end method

.method public getSerializedSize()I
    .locals 5

    const/4 v2, 0x0

    invoke-virtual {p0}, Lcom/google/android/play/analytics/PlayStore$PlayStoreUiElement;->hasType()Z

    move-result v3

    if-eqz v3, :cond_0

    const/4 v3, 0x1

    invoke-virtual {p0}, Lcom/google/android/play/analytics/PlayStore$PlayStoreUiElement;->getType()I

    move-result v4

    invoke-static {v3, v4}, Lcom/google/protobuf/micro/CodedOutputStreamMicro;->computeInt32Size(II)I

    move-result v3

    add-int/2addr v2, v3

    :cond_0
    invoke-virtual {p0}, Lcom/google/android/play/analytics/PlayStore$PlayStoreUiElement;->hasServerLogsCookie()Z

    move-result v3

    if-eqz v3, :cond_1

    const/4 v3, 0x2

    invoke-virtual {p0}, Lcom/google/android/play/analytics/PlayStore$PlayStoreUiElement;->getServerLogsCookie()Lcom/google/protobuf/micro/ByteStringMicro;

    move-result-object v4

    invoke-static {v3, v4}, Lcom/google/protobuf/micro/CodedOutputStreamMicro;->computeBytesSize(ILcom/google/protobuf/micro/ByteStringMicro;)I

    move-result v3

    add-int/2addr v2, v3

    :cond_1
    invoke-virtual {p0}, Lcom/google/android/play/analytics/PlayStore$PlayStoreUiElement;->hasClientLogsCookie()Z

    move-result v3

    if-eqz v3, :cond_2

    const/4 v3, 0x3

    invoke-virtual {p0}, Lcom/google/android/play/analytics/PlayStore$PlayStoreUiElement;->getClientLogsCookie()Lcom/google/android/play/analytics/PlayStore$PlayStoreUiElementInfo;

    move-result-object v4

    invoke-static {v3, v4}, Lcom/google/protobuf/micro/CodedOutputStreamMicro;->computeMessageSize(ILcom/google/protobuf/micro/MessageMicro;)I

    move-result v3

    add-int/2addr v2, v3

    :cond_2
    invoke-virtual {p0}, Lcom/google/android/play/analytics/PlayStore$PlayStoreUiElement;->getChildList()Ljava/util/List;

    move-result-object v3

    invoke-interface {v3}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :goto_0
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v3

    if-eqz v3, :cond_3

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/play/analytics/PlayStore$PlayStoreUiElement;

    const/4 v3, 0x4

    invoke-static {v3, v0}, Lcom/google/protobuf/micro/CodedOutputStreamMicro;->computeMessageSize(ILcom/google/protobuf/micro/MessageMicro;)I

    move-result v3

    add-int/2addr v2, v3

    goto :goto_0

    :cond_3
    iput v2, p0, Lcom/google/android/play/analytics/PlayStore$PlayStoreUiElement;->cachedSize:I

    return v2
.end method

.method public getServerLogsCookie()Lcom/google/protobuf/micro/ByteStringMicro;
    .locals 1

    iget-object v0, p0, Lcom/google/android/play/analytics/PlayStore$PlayStoreUiElement;->serverLogsCookie_:Lcom/google/protobuf/micro/ByteStringMicro;

    return-object v0
.end method

.method public getType()I
    .locals 1

    iget v0, p0, Lcom/google/android/play/analytics/PlayStore$PlayStoreUiElement;->type_:I

    return v0
.end method

.method public hasClientLogsCookie()Z
    .locals 1

    iget-boolean v0, p0, Lcom/google/android/play/analytics/PlayStore$PlayStoreUiElement;->hasClientLogsCookie:Z

    return v0
.end method

.method public hasServerLogsCookie()Z
    .locals 1

    iget-boolean v0, p0, Lcom/google/android/play/analytics/PlayStore$PlayStoreUiElement;->hasServerLogsCookie:Z

    return v0
.end method

.method public hasType()Z
    .locals 1

    iget-boolean v0, p0, Lcom/google/android/play/analytics/PlayStore$PlayStoreUiElement;->hasType:Z

    return v0
.end method

.method public mergeFrom(Lcom/google/protobuf/micro/CodedInputStreamMicro;)Lcom/google/android/play/analytics/PlayStore$PlayStoreUiElement;
    .locals 3
    .param p1    # Lcom/google/protobuf/micro/CodedInputStreamMicro;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    :cond_0
    :goto_0
    invoke-virtual {p1}, Lcom/google/protobuf/micro/CodedInputStreamMicro;->readTag()I

    move-result v0

    sparse-switch v0, :sswitch_data_0

    invoke-virtual {p0, p1, v0}, Lcom/google/android/play/analytics/PlayStore$PlayStoreUiElement;->parseUnknownField(Lcom/google/protobuf/micro/CodedInputStreamMicro;I)Z

    move-result v2

    if-nez v2, :cond_0

    :sswitch_0
    return-object p0

    :sswitch_1
    invoke-virtual {p1}, Lcom/google/protobuf/micro/CodedInputStreamMicro;->readInt32()I

    move-result v2

    invoke-virtual {p0, v2}, Lcom/google/android/play/analytics/PlayStore$PlayStoreUiElement;->setType(I)Lcom/google/android/play/analytics/PlayStore$PlayStoreUiElement;

    goto :goto_0

    :sswitch_2
    invoke-virtual {p1}, Lcom/google/protobuf/micro/CodedInputStreamMicro;->readBytes()Lcom/google/protobuf/micro/ByteStringMicro;

    move-result-object v2

    invoke-virtual {p0, v2}, Lcom/google/android/play/analytics/PlayStore$PlayStoreUiElement;->setServerLogsCookie(Lcom/google/protobuf/micro/ByteStringMicro;)Lcom/google/android/play/analytics/PlayStore$PlayStoreUiElement;

    goto :goto_0

    :sswitch_3
    new-instance v1, Lcom/google/android/play/analytics/PlayStore$PlayStoreUiElementInfo;

    invoke-direct {v1}, Lcom/google/android/play/analytics/PlayStore$PlayStoreUiElementInfo;-><init>()V

    invoke-virtual {p1, v1}, Lcom/google/protobuf/micro/CodedInputStreamMicro;->readMessage(Lcom/google/protobuf/micro/MessageMicro;)V

    invoke-virtual {p0, v1}, Lcom/google/android/play/analytics/PlayStore$PlayStoreUiElement;->setClientLogsCookie(Lcom/google/android/play/analytics/PlayStore$PlayStoreUiElementInfo;)Lcom/google/android/play/analytics/PlayStore$PlayStoreUiElement;

    goto :goto_0

    :sswitch_4
    new-instance v1, Lcom/google/android/play/analytics/PlayStore$PlayStoreUiElement;

    invoke-direct {v1}, Lcom/google/android/play/analytics/PlayStore$PlayStoreUiElement;-><init>()V

    invoke-virtual {p1, v1}, Lcom/google/protobuf/micro/CodedInputStreamMicro;->readMessage(Lcom/google/protobuf/micro/MessageMicro;)V

    invoke-virtual {p0, v1}, Lcom/google/android/play/analytics/PlayStore$PlayStoreUiElement;->addChild(Lcom/google/android/play/analytics/PlayStore$PlayStoreUiElement;)Lcom/google/android/play/analytics/PlayStore$PlayStoreUiElement;

    goto :goto_0

    :sswitch_data_0
    .sparse-switch
        0x0 -> :sswitch_0
        0x8 -> :sswitch_1
        0x12 -> :sswitch_2
        0x1a -> :sswitch_3
        0x22 -> :sswitch_4
    .end sparse-switch
.end method

.method public bridge synthetic mergeFrom(Lcom/google/protobuf/micro/CodedInputStreamMicro;)Lcom/google/protobuf/micro/MessageMicro;
    .locals 1
    .param p1    # Lcom/google/protobuf/micro/CodedInputStreamMicro;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    invoke-virtual {p0, p1}, Lcom/google/android/play/analytics/PlayStore$PlayStoreUiElement;->mergeFrom(Lcom/google/protobuf/micro/CodedInputStreamMicro;)Lcom/google/android/play/analytics/PlayStore$PlayStoreUiElement;

    move-result-object v0

    return-object v0
.end method

.method public setClientLogsCookie(Lcom/google/android/play/analytics/PlayStore$PlayStoreUiElementInfo;)Lcom/google/android/play/analytics/PlayStore$PlayStoreUiElement;
    .locals 1
    .param p1    # Lcom/google/android/play/analytics/PlayStore$PlayStoreUiElementInfo;

    if-nez p1, :cond_0

    new-instance v0, Ljava/lang/NullPointerException;

    invoke-direct {v0}, Ljava/lang/NullPointerException;-><init>()V

    throw v0

    :cond_0
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/google/android/play/analytics/PlayStore$PlayStoreUiElement;->hasClientLogsCookie:Z

    iput-object p1, p0, Lcom/google/android/play/analytics/PlayStore$PlayStoreUiElement;->clientLogsCookie_:Lcom/google/android/play/analytics/PlayStore$PlayStoreUiElementInfo;

    return-object p0
.end method

.method public setServerLogsCookie(Lcom/google/protobuf/micro/ByteStringMicro;)Lcom/google/android/play/analytics/PlayStore$PlayStoreUiElement;
    .locals 1
    .param p1    # Lcom/google/protobuf/micro/ByteStringMicro;

    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/google/android/play/analytics/PlayStore$PlayStoreUiElement;->hasServerLogsCookie:Z

    iput-object p1, p0, Lcom/google/android/play/analytics/PlayStore$PlayStoreUiElement;->serverLogsCookie_:Lcom/google/protobuf/micro/ByteStringMicro;

    return-object p0
.end method

.method public setType(I)Lcom/google/android/play/analytics/PlayStore$PlayStoreUiElement;
    .locals 1
    .param p1    # I

    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/google/android/play/analytics/PlayStore$PlayStoreUiElement;->hasType:Z

    iput p1, p0, Lcom/google/android/play/analytics/PlayStore$PlayStoreUiElement;->type_:I

    return-object p0
.end method

.method public writeTo(Lcom/google/protobuf/micro/CodedOutputStreamMicro;)V
    .locals 4
    .param p1    # Lcom/google/protobuf/micro/CodedOutputStreamMicro;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    invoke-virtual {p0}, Lcom/google/android/play/analytics/PlayStore$PlayStoreUiElement;->hasType()Z

    move-result v2

    if-eqz v2, :cond_0

    const/4 v2, 0x1

    invoke-virtual {p0}, Lcom/google/android/play/analytics/PlayStore$PlayStoreUiElement;->getType()I

    move-result v3

    invoke-virtual {p1, v2, v3}, Lcom/google/protobuf/micro/CodedOutputStreamMicro;->writeInt32(II)V

    :cond_0
    invoke-virtual {p0}, Lcom/google/android/play/analytics/PlayStore$PlayStoreUiElement;->hasServerLogsCookie()Z

    move-result v2

    if-eqz v2, :cond_1

    const/4 v2, 0x2

    invoke-virtual {p0}, Lcom/google/android/play/analytics/PlayStore$PlayStoreUiElement;->getServerLogsCookie()Lcom/google/protobuf/micro/ByteStringMicro;

    move-result-object v3

    invoke-virtual {p1, v2, v3}, Lcom/google/protobuf/micro/CodedOutputStreamMicro;->writeBytes(ILcom/google/protobuf/micro/ByteStringMicro;)V

    :cond_1
    invoke-virtual {p0}, Lcom/google/android/play/analytics/PlayStore$PlayStoreUiElement;->hasClientLogsCookie()Z

    move-result v2

    if-eqz v2, :cond_2

    const/4 v2, 0x3

    invoke-virtual {p0}, Lcom/google/android/play/analytics/PlayStore$PlayStoreUiElement;->getClientLogsCookie()Lcom/google/android/play/analytics/PlayStore$PlayStoreUiElementInfo;

    move-result-object v3

    invoke-virtual {p1, v2, v3}, Lcom/google/protobuf/micro/CodedOutputStreamMicro;->writeMessage(ILcom/google/protobuf/micro/MessageMicro;)V

    :cond_2
    invoke-virtual {p0}, Lcom/google/android/play/analytics/PlayStore$PlayStoreUiElement;->getChildList()Ljava/util/List;

    move-result-object v2

    invoke-interface {v2}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :goto_0
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v2

    if-eqz v2, :cond_3

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/play/analytics/PlayStore$PlayStoreUiElement;

    const/4 v2, 0x4

    invoke-virtual {p1, v2, v0}, Lcom/google/protobuf/micro/CodedOutputStreamMicro;->writeMessage(ILcom/google/protobuf/micro/MessageMicro;)V

    goto :goto_0

    :cond_3
    return-void
.end method
