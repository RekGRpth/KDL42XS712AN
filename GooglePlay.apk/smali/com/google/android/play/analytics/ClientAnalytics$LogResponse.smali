.class public final Lcom/google/android/play/analytics/ClientAnalytics$LogResponse;
.super Lcom/google/protobuf/micro/MessageMicro;
.source "ClientAnalytics.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/google/android/play/analytics/ClientAnalytics;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x19
    name = "LogResponse"
.end annotation


# instance fields
.field private cachedSize:I

.field private experiments_:Lcom/google/android/play/analytics/ClientAnalytics$ExperimentIdList;

.field private hasExperiments:Z

.field private hasNextRequestWaitMillis:Z

.field private nextRequestWaitMillis_:J


# direct methods
.method public constructor <init>()V
    .locals 2

    invoke-direct {p0}, Lcom/google/protobuf/micro/MessageMicro;-><init>()V

    const-wide/16 v0, -0x1

    iput-wide v0, p0, Lcom/google/android/play/analytics/ClientAnalytics$LogResponse;->nextRequestWaitMillis_:J

    const/4 v0, 0x0

    iput-object v0, p0, Lcom/google/android/play/analytics/ClientAnalytics$LogResponse;->experiments_:Lcom/google/android/play/analytics/ClientAnalytics$ExperimentIdList;

    const/4 v0, -0x1

    iput v0, p0, Lcom/google/android/play/analytics/ClientAnalytics$LogResponse;->cachedSize:I

    return-void
.end method

.method public static parseFrom([B)Lcom/google/android/play/analytics/ClientAnalytics$LogResponse;
    .locals 1
    .param p0    # [B
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/google/protobuf/micro/InvalidProtocolBufferMicroException;
        }
    .end annotation

    new-instance v0, Lcom/google/android/play/analytics/ClientAnalytics$LogResponse;

    invoke-direct {v0}, Lcom/google/android/play/analytics/ClientAnalytics$LogResponse;-><init>()V

    invoke-virtual {v0, p0}, Lcom/google/android/play/analytics/ClientAnalytics$LogResponse;->mergeFrom([B)Lcom/google/protobuf/micro/MessageMicro;

    move-result-object v0

    check-cast v0, Lcom/google/android/play/analytics/ClientAnalytics$LogResponse;

    check-cast v0, Lcom/google/android/play/analytics/ClientAnalytics$LogResponse;

    return-object v0
.end method


# virtual methods
.method public getCachedSize()I
    .locals 1

    iget v0, p0, Lcom/google/android/play/analytics/ClientAnalytics$LogResponse;->cachedSize:I

    if-gez v0, :cond_0

    invoke-virtual {p0}, Lcom/google/android/play/analytics/ClientAnalytics$LogResponse;->getSerializedSize()I

    :cond_0
    iget v0, p0, Lcom/google/android/play/analytics/ClientAnalytics$LogResponse;->cachedSize:I

    return v0
.end method

.method public getExperiments()Lcom/google/android/play/analytics/ClientAnalytics$ExperimentIdList;
    .locals 1

    iget-object v0, p0, Lcom/google/android/play/analytics/ClientAnalytics$LogResponse;->experiments_:Lcom/google/android/play/analytics/ClientAnalytics$ExperimentIdList;

    return-object v0
.end method

.method public getNextRequestWaitMillis()J
    .locals 2

    iget-wide v0, p0, Lcom/google/android/play/analytics/ClientAnalytics$LogResponse;->nextRequestWaitMillis_:J

    return-wide v0
.end method

.method public getSerializedSize()I
    .locals 4

    const/4 v0, 0x0

    invoke-virtual {p0}, Lcom/google/android/play/analytics/ClientAnalytics$LogResponse;->hasNextRequestWaitMillis()Z

    move-result v1

    if-eqz v1, :cond_0

    const/4 v1, 0x1

    invoke-virtual {p0}, Lcom/google/android/play/analytics/ClientAnalytics$LogResponse;->getNextRequestWaitMillis()J

    move-result-wide v2

    invoke-static {v1, v2, v3}, Lcom/google/protobuf/micro/CodedOutputStreamMicro;->computeInt64Size(IJ)I

    move-result v1

    add-int/2addr v0, v1

    :cond_0
    invoke-virtual {p0}, Lcom/google/android/play/analytics/ClientAnalytics$LogResponse;->hasExperiments()Z

    move-result v1

    if-eqz v1, :cond_1

    const/4 v1, 0x2

    invoke-virtual {p0}, Lcom/google/android/play/analytics/ClientAnalytics$LogResponse;->getExperiments()Lcom/google/android/play/analytics/ClientAnalytics$ExperimentIdList;

    move-result-object v2

    invoke-static {v1, v2}, Lcom/google/protobuf/micro/CodedOutputStreamMicro;->computeMessageSize(ILcom/google/protobuf/micro/MessageMicro;)I

    move-result v1

    add-int/2addr v0, v1

    :cond_1
    iput v0, p0, Lcom/google/android/play/analytics/ClientAnalytics$LogResponse;->cachedSize:I

    return v0
.end method

.method public hasExperiments()Z
    .locals 1

    iget-boolean v0, p0, Lcom/google/android/play/analytics/ClientAnalytics$LogResponse;->hasExperiments:Z

    return v0
.end method

.method public hasNextRequestWaitMillis()Z
    .locals 1

    iget-boolean v0, p0, Lcom/google/android/play/analytics/ClientAnalytics$LogResponse;->hasNextRequestWaitMillis:Z

    return v0
.end method

.method public mergeFrom(Lcom/google/protobuf/micro/CodedInputStreamMicro;)Lcom/google/android/play/analytics/ClientAnalytics$LogResponse;
    .locals 4
    .param p1    # Lcom/google/protobuf/micro/CodedInputStreamMicro;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    :cond_0
    :goto_0
    invoke-virtual {p1}, Lcom/google/protobuf/micro/CodedInputStreamMicro;->readTag()I

    move-result v0

    sparse-switch v0, :sswitch_data_0

    invoke-virtual {p0, p1, v0}, Lcom/google/android/play/analytics/ClientAnalytics$LogResponse;->parseUnknownField(Lcom/google/protobuf/micro/CodedInputStreamMicro;I)Z

    move-result v2

    if-nez v2, :cond_0

    :sswitch_0
    return-object p0

    :sswitch_1
    invoke-virtual {p1}, Lcom/google/protobuf/micro/CodedInputStreamMicro;->readInt64()J

    move-result-wide v2

    invoke-virtual {p0, v2, v3}, Lcom/google/android/play/analytics/ClientAnalytics$LogResponse;->setNextRequestWaitMillis(J)Lcom/google/android/play/analytics/ClientAnalytics$LogResponse;

    goto :goto_0

    :sswitch_2
    new-instance v1, Lcom/google/android/play/analytics/ClientAnalytics$ExperimentIdList;

    invoke-direct {v1}, Lcom/google/android/play/analytics/ClientAnalytics$ExperimentIdList;-><init>()V

    invoke-virtual {p1, v1}, Lcom/google/protobuf/micro/CodedInputStreamMicro;->readMessage(Lcom/google/protobuf/micro/MessageMicro;)V

    invoke-virtual {p0, v1}, Lcom/google/android/play/analytics/ClientAnalytics$LogResponse;->setExperiments(Lcom/google/android/play/analytics/ClientAnalytics$ExperimentIdList;)Lcom/google/android/play/analytics/ClientAnalytics$LogResponse;

    goto :goto_0

    :sswitch_data_0
    .sparse-switch
        0x0 -> :sswitch_0
        0x8 -> :sswitch_1
        0x12 -> :sswitch_2
    .end sparse-switch
.end method

.method public bridge synthetic mergeFrom(Lcom/google/protobuf/micro/CodedInputStreamMicro;)Lcom/google/protobuf/micro/MessageMicro;
    .locals 1
    .param p1    # Lcom/google/protobuf/micro/CodedInputStreamMicro;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    invoke-virtual {p0, p1}, Lcom/google/android/play/analytics/ClientAnalytics$LogResponse;->mergeFrom(Lcom/google/protobuf/micro/CodedInputStreamMicro;)Lcom/google/android/play/analytics/ClientAnalytics$LogResponse;

    move-result-object v0

    return-object v0
.end method

.method public setExperiments(Lcom/google/android/play/analytics/ClientAnalytics$ExperimentIdList;)Lcom/google/android/play/analytics/ClientAnalytics$LogResponse;
    .locals 1
    .param p1    # Lcom/google/android/play/analytics/ClientAnalytics$ExperimentIdList;

    if-nez p1, :cond_0

    new-instance v0, Ljava/lang/NullPointerException;

    invoke-direct {v0}, Ljava/lang/NullPointerException;-><init>()V

    throw v0

    :cond_0
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/google/android/play/analytics/ClientAnalytics$LogResponse;->hasExperiments:Z

    iput-object p1, p0, Lcom/google/android/play/analytics/ClientAnalytics$LogResponse;->experiments_:Lcom/google/android/play/analytics/ClientAnalytics$ExperimentIdList;

    return-object p0
.end method

.method public setNextRequestWaitMillis(J)Lcom/google/android/play/analytics/ClientAnalytics$LogResponse;
    .locals 1
    .param p1    # J

    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/google/android/play/analytics/ClientAnalytics$LogResponse;->hasNextRequestWaitMillis:Z

    iput-wide p1, p0, Lcom/google/android/play/analytics/ClientAnalytics$LogResponse;->nextRequestWaitMillis_:J

    return-object p0
.end method

.method public writeTo(Lcom/google/protobuf/micro/CodedOutputStreamMicro;)V
    .locals 3
    .param p1    # Lcom/google/protobuf/micro/CodedOutputStreamMicro;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    invoke-virtual {p0}, Lcom/google/android/play/analytics/ClientAnalytics$LogResponse;->hasNextRequestWaitMillis()Z

    move-result v0

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    invoke-virtual {p0}, Lcom/google/android/play/analytics/ClientAnalytics$LogResponse;->getNextRequestWaitMillis()J

    move-result-wide v1

    invoke-virtual {p1, v0, v1, v2}, Lcom/google/protobuf/micro/CodedOutputStreamMicro;->writeInt64(IJ)V

    :cond_0
    invoke-virtual {p0}, Lcom/google/android/play/analytics/ClientAnalytics$LogResponse;->hasExperiments()Z

    move-result v0

    if-eqz v0, :cond_1

    const/4 v0, 0x2

    invoke-virtual {p0}, Lcom/google/android/play/analytics/ClientAnalytics$LogResponse;->getExperiments()Lcom/google/android/play/analytics/ClientAnalytics$ExperimentIdList;

    move-result-object v1

    invoke-virtual {p1, v0, v1}, Lcom/google/protobuf/micro/CodedOutputStreamMicro;->writeMessage(ILcom/google/protobuf/micro/MessageMicro;)V

    :cond_1
    return-void
.end method
