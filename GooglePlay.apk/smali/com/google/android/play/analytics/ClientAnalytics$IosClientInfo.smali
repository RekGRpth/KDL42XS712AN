.class public final Lcom/google/android/play/analytics/ClientAnalytics$IosClientInfo;
.super Lcom/google/protobuf/micro/MessageMicro;
.source "ClientAnalytics.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/google/android/play/analytics/ClientAnalytics;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x19
    name = "IosClientInfo"
.end annotation


# instance fields
.field private applicationBuild_:Ljava/lang/String;

.field private cachedSize:I

.field private clientId_:Ljava/lang/String;

.field private hasApplicationBuild:Z

.field private hasClientId:Z

.field private hasLoggingId:Z

.field private hasOsFullVersion:Z

.field private hasOsMajorVersion:Z

.field private loggingId_:Ljava/lang/String;

.field private osFullVersion_:Ljava/lang/String;

.field private osMajorVersion_:Ljava/lang/String;


# direct methods
.method public constructor <init>()V
    .locals 1

    invoke-direct {p0}, Lcom/google/protobuf/micro/MessageMicro;-><init>()V

    const-string v0, ""

    iput-object v0, p0, Lcom/google/android/play/analytics/ClientAnalytics$IosClientInfo;->clientId_:Ljava/lang/String;

    const-string v0, ""

    iput-object v0, p0, Lcom/google/android/play/analytics/ClientAnalytics$IosClientInfo;->loggingId_:Ljava/lang/String;

    const-string v0, ""

    iput-object v0, p0, Lcom/google/android/play/analytics/ClientAnalytics$IosClientInfo;->osMajorVersion_:Ljava/lang/String;

    const-string v0, ""

    iput-object v0, p0, Lcom/google/android/play/analytics/ClientAnalytics$IosClientInfo;->osFullVersion_:Ljava/lang/String;

    const-string v0, ""

    iput-object v0, p0, Lcom/google/android/play/analytics/ClientAnalytics$IosClientInfo;->applicationBuild_:Ljava/lang/String;

    const/4 v0, -0x1

    iput v0, p0, Lcom/google/android/play/analytics/ClientAnalytics$IosClientInfo;->cachedSize:I

    return-void
.end method


# virtual methods
.method public getApplicationBuild()Ljava/lang/String;
    .locals 1

    iget-object v0, p0, Lcom/google/android/play/analytics/ClientAnalytics$IosClientInfo;->applicationBuild_:Ljava/lang/String;

    return-object v0
.end method

.method public getCachedSize()I
    .locals 1

    iget v0, p0, Lcom/google/android/play/analytics/ClientAnalytics$IosClientInfo;->cachedSize:I

    if-gez v0, :cond_0

    invoke-virtual {p0}, Lcom/google/android/play/analytics/ClientAnalytics$IosClientInfo;->getSerializedSize()I

    :cond_0
    iget v0, p0, Lcom/google/android/play/analytics/ClientAnalytics$IosClientInfo;->cachedSize:I

    return v0
.end method

.method public getClientId()Ljava/lang/String;
    .locals 1

    iget-object v0, p0, Lcom/google/android/play/analytics/ClientAnalytics$IosClientInfo;->clientId_:Ljava/lang/String;

    return-object v0
.end method

.method public getLoggingId()Ljava/lang/String;
    .locals 1

    iget-object v0, p0, Lcom/google/android/play/analytics/ClientAnalytics$IosClientInfo;->loggingId_:Ljava/lang/String;

    return-object v0
.end method

.method public getOsFullVersion()Ljava/lang/String;
    .locals 1

    iget-object v0, p0, Lcom/google/android/play/analytics/ClientAnalytics$IosClientInfo;->osFullVersion_:Ljava/lang/String;

    return-object v0
.end method

.method public getOsMajorVersion()Ljava/lang/String;
    .locals 1

    iget-object v0, p0, Lcom/google/android/play/analytics/ClientAnalytics$IosClientInfo;->osMajorVersion_:Ljava/lang/String;

    return-object v0
.end method

.method public getSerializedSize()I
    .locals 3

    const/4 v0, 0x0

    invoke-virtual {p0}, Lcom/google/android/play/analytics/ClientAnalytics$IosClientInfo;->hasClientId()Z

    move-result v1

    if-eqz v1, :cond_0

    const/4 v1, 0x1

    invoke-virtual {p0}, Lcom/google/android/play/analytics/ClientAnalytics$IosClientInfo;->getClientId()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2}, Lcom/google/protobuf/micro/CodedOutputStreamMicro;->computeStringSize(ILjava/lang/String;)I

    move-result v1

    add-int/2addr v0, v1

    :cond_0
    invoke-virtual {p0}, Lcom/google/android/play/analytics/ClientAnalytics$IosClientInfo;->hasLoggingId()Z

    move-result v1

    if-eqz v1, :cond_1

    const/4 v1, 0x2

    invoke-virtual {p0}, Lcom/google/android/play/analytics/ClientAnalytics$IosClientInfo;->getLoggingId()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2}, Lcom/google/protobuf/micro/CodedOutputStreamMicro;->computeStringSize(ILjava/lang/String;)I

    move-result v1

    add-int/2addr v0, v1

    :cond_1
    invoke-virtual {p0}, Lcom/google/android/play/analytics/ClientAnalytics$IosClientInfo;->hasOsMajorVersion()Z

    move-result v1

    if-eqz v1, :cond_2

    const/4 v1, 0x3

    invoke-virtual {p0}, Lcom/google/android/play/analytics/ClientAnalytics$IosClientInfo;->getOsMajorVersion()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2}, Lcom/google/protobuf/micro/CodedOutputStreamMicro;->computeStringSize(ILjava/lang/String;)I

    move-result v1

    add-int/2addr v0, v1

    :cond_2
    invoke-virtual {p0}, Lcom/google/android/play/analytics/ClientAnalytics$IosClientInfo;->hasOsFullVersion()Z

    move-result v1

    if-eqz v1, :cond_3

    const/4 v1, 0x4

    invoke-virtual {p0}, Lcom/google/android/play/analytics/ClientAnalytics$IosClientInfo;->getOsFullVersion()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2}, Lcom/google/protobuf/micro/CodedOutputStreamMicro;->computeStringSize(ILjava/lang/String;)I

    move-result v1

    add-int/2addr v0, v1

    :cond_3
    invoke-virtual {p0}, Lcom/google/android/play/analytics/ClientAnalytics$IosClientInfo;->hasApplicationBuild()Z

    move-result v1

    if-eqz v1, :cond_4

    const/4 v1, 0x5

    invoke-virtual {p0}, Lcom/google/android/play/analytics/ClientAnalytics$IosClientInfo;->getApplicationBuild()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2}, Lcom/google/protobuf/micro/CodedOutputStreamMicro;->computeStringSize(ILjava/lang/String;)I

    move-result v1

    add-int/2addr v0, v1

    :cond_4
    iput v0, p0, Lcom/google/android/play/analytics/ClientAnalytics$IosClientInfo;->cachedSize:I

    return v0
.end method

.method public hasApplicationBuild()Z
    .locals 1

    iget-boolean v0, p0, Lcom/google/android/play/analytics/ClientAnalytics$IosClientInfo;->hasApplicationBuild:Z

    return v0
.end method

.method public hasClientId()Z
    .locals 1

    iget-boolean v0, p0, Lcom/google/android/play/analytics/ClientAnalytics$IosClientInfo;->hasClientId:Z

    return v0
.end method

.method public hasLoggingId()Z
    .locals 1

    iget-boolean v0, p0, Lcom/google/android/play/analytics/ClientAnalytics$IosClientInfo;->hasLoggingId:Z

    return v0
.end method

.method public hasOsFullVersion()Z
    .locals 1

    iget-boolean v0, p0, Lcom/google/android/play/analytics/ClientAnalytics$IosClientInfo;->hasOsFullVersion:Z

    return v0
.end method

.method public hasOsMajorVersion()Z
    .locals 1

    iget-boolean v0, p0, Lcom/google/android/play/analytics/ClientAnalytics$IosClientInfo;->hasOsMajorVersion:Z

    return v0
.end method

.method public mergeFrom(Lcom/google/protobuf/micro/CodedInputStreamMicro;)Lcom/google/android/play/analytics/ClientAnalytics$IosClientInfo;
    .locals 2
    .param p1    # Lcom/google/protobuf/micro/CodedInputStreamMicro;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    :cond_0
    :goto_0
    invoke-virtual {p1}, Lcom/google/protobuf/micro/CodedInputStreamMicro;->readTag()I

    move-result v0

    sparse-switch v0, :sswitch_data_0

    invoke-virtual {p0, p1, v0}, Lcom/google/android/play/analytics/ClientAnalytics$IosClientInfo;->parseUnknownField(Lcom/google/protobuf/micro/CodedInputStreamMicro;I)Z

    move-result v1

    if-nez v1, :cond_0

    :sswitch_0
    return-object p0

    :sswitch_1
    invoke-virtual {p1}, Lcom/google/protobuf/micro/CodedInputStreamMicro;->readString()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {p0, v1}, Lcom/google/android/play/analytics/ClientAnalytics$IosClientInfo;->setClientId(Ljava/lang/String;)Lcom/google/android/play/analytics/ClientAnalytics$IosClientInfo;

    goto :goto_0

    :sswitch_2
    invoke-virtual {p1}, Lcom/google/protobuf/micro/CodedInputStreamMicro;->readString()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {p0, v1}, Lcom/google/android/play/analytics/ClientAnalytics$IosClientInfo;->setLoggingId(Ljava/lang/String;)Lcom/google/android/play/analytics/ClientAnalytics$IosClientInfo;

    goto :goto_0

    :sswitch_3
    invoke-virtual {p1}, Lcom/google/protobuf/micro/CodedInputStreamMicro;->readString()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {p0, v1}, Lcom/google/android/play/analytics/ClientAnalytics$IosClientInfo;->setOsMajorVersion(Ljava/lang/String;)Lcom/google/android/play/analytics/ClientAnalytics$IosClientInfo;

    goto :goto_0

    :sswitch_4
    invoke-virtual {p1}, Lcom/google/protobuf/micro/CodedInputStreamMicro;->readString()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {p0, v1}, Lcom/google/android/play/analytics/ClientAnalytics$IosClientInfo;->setOsFullVersion(Ljava/lang/String;)Lcom/google/android/play/analytics/ClientAnalytics$IosClientInfo;

    goto :goto_0

    :sswitch_5
    invoke-virtual {p1}, Lcom/google/protobuf/micro/CodedInputStreamMicro;->readString()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {p0, v1}, Lcom/google/android/play/analytics/ClientAnalytics$IosClientInfo;->setApplicationBuild(Ljava/lang/String;)Lcom/google/android/play/analytics/ClientAnalytics$IosClientInfo;

    goto :goto_0

    :sswitch_data_0
    .sparse-switch
        0x0 -> :sswitch_0
        0xa -> :sswitch_1
        0x12 -> :sswitch_2
        0x1a -> :sswitch_3
        0x22 -> :sswitch_4
        0x2a -> :sswitch_5
    .end sparse-switch
.end method

.method public bridge synthetic mergeFrom(Lcom/google/protobuf/micro/CodedInputStreamMicro;)Lcom/google/protobuf/micro/MessageMicro;
    .locals 1
    .param p1    # Lcom/google/protobuf/micro/CodedInputStreamMicro;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    invoke-virtual {p0, p1}, Lcom/google/android/play/analytics/ClientAnalytics$IosClientInfo;->mergeFrom(Lcom/google/protobuf/micro/CodedInputStreamMicro;)Lcom/google/android/play/analytics/ClientAnalytics$IosClientInfo;

    move-result-object v0

    return-object v0
.end method

.method public setApplicationBuild(Ljava/lang/String;)Lcom/google/android/play/analytics/ClientAnalytics$IosClientInfo;
    .locals 1
    .param p1    # Ljava/lang/String;

    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/google/android/play/analytics/ClientAnalytics$IosClientInfo;->hasApplicationBuild:Z

    iput-object p1, p0, Lcom/google/android/play/analytics/ClientAnalytics$IosClientInfo;->applicationBuild_:Ljava/lang/String;

    return-object p0
.end method

.method public setClientId(Ljava/lang/String;)Lcom/google/android/play/analytics/ClientAnalytics$IosClientInfo;
    .locals 1
    .param p1    # Ljava/lang/String;

    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/google/android/play/analytics/ClientAnalytics$IosClientInfo;->hasClientId:Z

    iput-object p1, p0, Lcom/google/android/play/analytics/ClientAnalytics$IosClientInfo;->clientId_:Ljava/lang/String;

    return-object p0
.end method

.method public setLoggingId(Ljava/lang/String;)Lcom/google/android/play/analytics/ClientAnalytics$IosClientInfo;
    .locals 1
    .param p1    # Ljava/lang/String;

    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/google/android/play/analytics/ClientAnalytics$IosClientInfo;->hasLoggingId:Z

    iput-object p1, p0, Lcom/google/android/play/analytics/ClientAnalytics$IosClientInfo;->loggingId_:Ljava/lang/String;

    return-object p0
.end method

.method public setOsFullVersion(Ljava/lang/String;)Lcom/google/android/play/analytics/ClientAnalytics$IosClientInfo;
    .locals 1
    .param p1    # Ljava/lang/String;

    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/google/android/play/analytics/ClientAnalytics$IosClientInfo;->hasOsFullVersion:Z

    iput-object p1, p0, Lcom/google/android/play/analytics/ClientAnalytics$IosClientInfo;->osFullVersion_:Ljava/lang/String;

    return-object p0
.end method

.method public setOsMajorVersion(Ljava/lang/String;)Lcom/google/android/play/analytics/ClientAnalytics$IosClientInfo;
    .locals 1
    .param p1    # Ljava/lang/String;

    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/google/android/play/analytics/ClientAnalytics$IosClientInfo;->hasOsMajorVersion:Z

    iput-object p1, p0, Lcom/google/android/play/analytics/ClientAnalytics$IosClientInfo;->osMajorVersion_:Ljava/lang/String;

    return-object p0
.end method

.method public writeTo(Lcom/google/protobuf/micro/CodedOutputStreamMicro;)V
    .locals 2
    .param p1    # Lcom/google/protobuf/micro/CodedOutputStreamMicro;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    invoke-virtual {p0}, Lcom/google/android/play/analytics/ClientAnalytics$IosClientInfo;->hasClientId()Z

    move-result v0

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    invoke-virtual {p0}, Lcom/google/android/play/analytics/ClientAnalytics$IosClientInfo;->getClientId()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {p1, v0, v1}, Lcom/google/protobuf/micro/CodedOutputStreamMicro;->writeString(ILjava/lang/String;)V

    :cond_0
    invoke-virtual {p0}, Lcom/google/android/play/analytics/ClientAnalytics$IosClientInfo;->hasLoggingId()Z

    move-result v0

    if-eqz v0, :cond_1

    const/4 v0, 0x2

    invoke-virtual {p0}, Lcom/google/android/play/analytics/ClientAnalytics$IosClientInfo;->getLoggingId()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {p1, v0, v1}, Lcom/google/protobuf/micro/CodedOutputStreamMicro;->writeString(ILjava/lang/String;)V

    :cond_1
    invoke-virtual {p0}, Lcom/google/android/play/analytics/ClientAnalytics$IosClientInfo;->hasOsMajorVersion()Z

    move-result v0

    if-eqz v0, :cond_2

    const/4 v0, 0x3

    invoke-virtual {p0}, Lcom/google/android/play/analytics/ClientAnalytics$IosClientInfo;->getOsMajorVersion()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {p1, v0, v1}, Lcom/google/protobuf/micro/CodedOutputStreamMicro;->writeString(ILjava/lang/String;)V

    :cond_2
    invoke-virtual {p0}, Lcom/google/android/play/analytics/ClientAnalytics$IosClientInfo;->hasOsFullVersion()Z

    move-result v0

    if-eqz v0, :cond_3

    const/4 v0, 0x4

    invoke-virtual {p0}, Lcom/google/android/play/analytics/ClientAnalytics$IosClientInfo;->getOsFullVersion()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {p1, v0, v1}, Lcom/google/protobuf/micro/CodedOutputStreamMicro;->writeString(ILjava/lang/String;)V

    :cond_3
    invoke-virtual {p0}, Lcom/google/android/play/analytics/ClientAnalytics$IosClientInfo;->hasApplicationBuild()Z

    move-result v0

    if-eqz v0, :cond_4

    const/4 v0, 0x5

    invoke-virtual {p0}, Lcom/google/android/play/analytics/ClientAnalytics$IosClientInfo;->getApplicationBuild()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {p1, v0, v1}, Lcom/google/protobuf/micro/CodedOutputStreamMicro;->writeString(ILjava/lang/String;)V

    :cond_4
    return-void
.end method
