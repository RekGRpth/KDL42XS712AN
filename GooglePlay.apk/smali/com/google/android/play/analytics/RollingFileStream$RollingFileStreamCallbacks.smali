.class public interface abstract Lcom/google/android/play/analytics/RollingFileStream$RollingFileStreamCallbacks;
.super Ljava/lang/Object;
.source "RollingFileStream.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/google/android/play/analytics/RollingFileStream;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x609
    name = "RollingFileStreamCallbacks"
.end annotation


# virtual methods
.method public abstract onNewOutputFile(Ljava/io/OutputStream;)V
.end method

.method public abstract onRead(Ljava/io/InputStream;Ljava/lang/Object;)V
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation
.end method

.method public abstract onWrite(Ljava/io/OutputStream;Ljava/lang/Object;)V
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation
.end method
