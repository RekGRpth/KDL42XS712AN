.class public final Lcom/google/android/play/analytics/PlayStore$PlayStoreBackgroundActionEvent;
.super Lcom/google/protobuf/micro/MessageMicro;
.source "PlayStore.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/google/android/play/analytics/PlayStore;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x19
    name = "PlayStoreBackgroundActionEvent"
.end annotation


# instance fields
.field private appData_:Lcom/google/android/play/analytics/PlayStore$AppData;

.field private cachedSize:I

.field private clientLatencyMs_:J

.field private document_:Ljava/lang/String;

.field private errorCode_:I

.field private exceptionType_:Ljava/lang/String;

.field private fromSetting_:I

.field private hasAppData:Z

.field private hasClientLatencyMs:Z

.field private hasDocument:Z

.field private hasErrorCode:Z

.field private hasExceptionType:Z

.field private hasFromSetting:Z

.field private hasOfferType:Z

.field private hasReason:Z

.field private hasServerLatencyMs:Z

.field private hasServerLogsCookie:Z

.field private hasSessionInfo:Z

.field private hasToSetting:Z

.field private hasType:Z

.field private offerType_:I

.field private reason_:Ljava/lang/String;

.field private serverLatencyMs_:J

.field private serverLogsCookie_:Lcom/google/protobuf/micro/ByteStringMicro;

.field private sessionInfo_:Lcom/google/android/play/analytics/PlayStore$PlayStoreSessionData;

.field private toSetting_:I

.field private type_:I


# direct methods
.method public constructor <init>()V
    .locals 5

    const-wide/16 v3, 0x0

    const/4 v2, 0x0

    const/4 v1, 0x0

    invoke-direct {p0}, Lcom/google/protobuf/micro/MessageMicro;-><init>()V

    iput v1, p0, Lcom/google/android/play/analytics/PlayStore$PlayStoreBackgroundActionEvent;->type_:I

    const-string v0, ""

    iput-object v0, p0, Lcom/google/android/play/analytics/PlayStore$PlayStoreBackgroundActionEvent;->document_:Ljava/lang/String;

    const-string v0, ""

    iput-object v0, p0, Lcom/google/android/play/analytics/PlayStore$PlayStoreBackgroundActionEvent;->reason_:Ljava/lang/String;

    iput v1, p0, Lcom/google/android/play/analytics/PlayStore$PlayStoreBackgroundActionEvent;->errorCode_:I

    const-string v0, ""

    iput-object v0, p0, Lcom/google/android/play/analytics/PlayStore$PlayStoreBackgroundActionEvent;->exceptionType_:Ljava/lang/String;

    sget-object v0, Lcom/google/protobuf/micro/ByteStringMicro;->EMPTY:Lcom/google/protobuf/micro/ByteStringMicro;

    iput-object v0, p0, Lcom/google/android/play/analytics/PlayStore$PlayStoreBackgroundActionEvent;->serverLogsCookie_:Lcom/google/protobuf/micro/ByteStringMicro;

    iput v1, p0, Lcom/google/android/play/analytics/PlayStore$PlayStoreBackgroundActionEvent;->offerType_:I

    iput v1, p0, Lcom/google/android/play/analytics/PlayStore$PlayStoreBackgroundActionEvent;->fromSetting_:I

    iput v1, p0, Lcom/google/android/play/analytics/PlayStore$PlayStoreBackgroundActionEvent;->toSetting_:I

    iput-object v2, p0, Lcom/google/android/play/analytics/PlayStore$PlayStoreBackgroundActionEvent;->sessionInfo_:Lcom/google/android/play/analytics/PlayStore$PlayStoreSessionData;

    iput-object v2, p0, Lcom/google/android/play/analytics/PlayStore$PlayStoreBackgroundActionEvent;->appData_:Lcom/google/android/play/analytics/PlayStore$AppData;

    iput-wide v3, p0, Lcom/google/android/play/analytics/PlayStore$PlayStoreBackgroundActionEvent;->serverLatencyMs_:J

    iput-wide v3, p0, Lcom/google/android/play/analytics/PlayStore$PlayStoreBackgroundActionEvent;->clientLatencyMs_:J

    const/4 v0, -0x1

    iput v0, p0, Lcom/google/android/play/analytics/PlayStore$PlayStoreBackgroundActionEvent;->cachedSize:I

    return-void
.end method


# virtual methods
.method public final clear()Lcom/google/android/play/analytics/PlayStore$PlayStoreBackgroundActionEvent;
    .locals 1

    invoke-virtual {p0}, Lcom/google/android/play/analytics/PlayStore$PlayStoreBackgroundActionEvent;->clearType()Lcom/google/android/play/analytics/PlayStore$PlayStoreBackgroundActionEvent;

    invoke-virtual {p0}, Lcom/google/android/play/analytics/PlayStore$PlayStoreBackgroundActionEvent;->clearDocument()Lcom/google/android/play/analytics/PlayStore$PlayStoreBackgroundActionEvent;

    invoke-virtual {p0}, Lcom/google/android/play/analytics/PlayStore$PlayStoreBackgroundActionEvent;->clearReason()Lcom/google/android/play/analytics/PlayStore$PlayStoreBackgroundActionEvent;

    invoke-virtual {p0}, Lcom/google/android/play/analytics/PlayStore$PlayStoreBackgroundActionEvent;->clearErrorCode()Lcom/google/android/play/analytics/PlayStore$PlayStoreBackgroundActionEvent;

    invoke-virtual {p0}, Lcom/google/android/play/analytics/PlayStore$PlayStoreBackgroundActionEvent;->clearExceptionType()Lcom/google/android/play/analytics/PlayStore$PlayStoreBackgroundActionEvent;

    invoke-virtual {p0}, Lcom/google/android/play/analytics/PlayStore$PlayStoreBackgroundActionEvent;->clearServerLogsCookie()Lcom/google/android/play/analytics/PlayStore$PlayStoreBackgroundActionEvent;

    invoke-virtual {p0}, Lcom/google/android/play/analytics/PlayStore$PlayStoreBackgroundActionEvent;->clearOfferType()Lcom/google/android/play/analytics/PlayStore$PlayStoreBackgroundActionEvent;

    invoke-virtual {p0}, Lcom/google/android/play/analytics/PlayStore$PlayStoreBackgroundActionEvent;->clearFromSetting()Lcom/google/android/play/analytics/PlayStore$PlayStoreBackgroundActionEvent;

    invoke-virtual {p0}, Lcom/google/android/play/analytics/PlayStore$PlayStoreBackgroundActionEvent;->clearToSetting()Lcom/google/android/play/analytics/PlayStore$PlayStoreBackgroundActionEvent;

    invoke-virtual {p0}, Lcom/google/android/play/analytics/PlayStore$PlayStoreBackgroundActionEvent;->clearSessionInfo()Lcom/google/android/play/analytics/PlayStore$PlayStoreBackgroundActionEvent;

    invoke-virtual {p0}, Lcom/google/android/play/analytics/PlayStore$PlayStoreBackgroundActionEvent;->clearAppData()Lcom/google/android/play/analytics/PlayStore$PlayStoreBackgroundActionEvent;

    invoke-virtual {p0}, Lcom/google/android/play/analytics/PlayStore$PlayStoreBackgroundActionEvent;->clearServerLatencyMs()Lcom/google/android/play/analytics/PlayStore$PlayStoreBackgroundActionEvent;

    invoke-virtual {p0}, Lcom/google/android/play/analytics/PlayStore$PlayStoreBackgroundActionEvent;->clearClientLatencyMs()Lcom/google/android/play/analytics/PlayStore$PlayStoreBackgroundActionEvent;

    const/4 v0, -0x1

    iput v0, p0, Lcom/google/android/play/analytics/PlayStore$PlayStoreBackgroundActionEvent;->cachedSize:I

    return-object p0
.end method

.method public clearAppData()Lcom/google/android/play/analytics/PlayStore$PlayStoreBackgroundActionEvent;
    .locals 1

    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/google/android/play/analytics/PlayStore$PlayStoreBackgroundActionEvent;->hasAppData:Z

    const/4 v0, 0x0

    iput-object v0, p0, Lcom/google/android/play/analytics/PlayStore$PlayStoreBackgroundActionEvent;->appData_:Lcom/google/android/play/analytics/PlayStore$AppData;

    return-object p0
.end method

.method public clearClientLatencyMs()Lcom/google/android/play/analytics/PlayStore$PlayStoreBackgroundActionEvent;
    .locals 2

    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/google/android/play/analytics/PlayStore$PlayStoreBackgroundActionEvent;->hasClientLatencyMs:Z

    const-wide/16 v0, 0x0

    iput-wide v0, p0, Lcom/google/android/play/analytics/PlayStore$PlayStoreBackgroundActionEvent;->clientLatencyMs_:J

    return-object p0
.end method

.method public clearDocument()Lcom/google/android/play/analytics/PlayStore$PlayStoreBackgroundActionEvent;
    .locals 1

    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/google/android/play/analytics/PlayStore$PlayStoreBackgroundActionEvent;->hasDocument:Z

    const-string v0, ""

    iput-object v0, p0, Lcom/google/android/play/analytics/PlayStore$PlayStoreBackgroundActionEvent;->document_:Ljava/lang/String;

    return-object p0
.end method

.method public clearErrorCode()Lcom/google/android/play/analytics/PlayStore$PlayStoreBackgroundActionEvent;
    .locals 1

    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/google/android/play/analytics/PlayStore$PlayStoreBackgroundActionEvent;->hasErrorCode:Z

    iput v0, p0, Lcom/google/android/play/analytics/PlayStore$PlayStoreBackgroundActionEvent;->errorCode_:I

    return-object p0
.end method

.method public clearExceptionType()Lcom/google/android/play/analytics/PlayStore$PlayStoreBackgroundActionEvent;
    .locals 1

    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/google/android/play/analytics/PlayStore$PlayStoreBackgroundActionEvent;->hasExceptionType:Z

    const-string v0, ""

    iput-object v0, p0, Lcom/google/android/play/analytics/PlayStore$PlayStoreBackgroundActionEvent;->exceptionType_:Ljava/lang/String;

    return-object p0
.end method

.method public clearFromSetting()Lcom/google/android/play/analytics/PlayStore$PlayStoreBackgroundActionEvent;
    .locals 1

    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/google/android/play/analytics/PlayStore$PlayStoreBackgroundActionEvent;->hasFromSetting:Z

    iput v0, p0, Lcom/google/android/play/analytics/PlayStore$PlayStoreBackgroundActionEvent;->fromSetting_:I

    return-object p0
.end method

.method public clearOfferType()Lcom/google/android/play/analytics/PlayStore$PlayStoreBackgroundActionEvent;
    .locals 1

    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/google/android/play/analytics/PlayStore$PlayStoreBackgroundActionEvent;->hasOfferType:Z

    iput v0, p0, Lcom/google/android/play/analytics/PlayStore$PlayStoreBackgroundActionEvent;->offerType_:I

    return-object p0
.end method

.method public clearReason()Lcom/google/android/play/analytics/PlayStore$PlayStoreBackgroundActionEvent;
    .locals 1

    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/google/android/play/analytics/PlayStore$PlayStoreBackgroundActionEvent;->hasReason:Z

    const-string v0, ""

    iput-object v0, p0, Lcom/google/android/play/analytics/PlayStore$PlayStoreBackgroundActionEvent;->reason_:Ljava/lang/String;

    return-object p0
.end method

.method public clearServerLatencyMs()Lcom/google/android/play/analytics/PlayStore$PlayStoreBackgroundActionEvent;
    .locals 2

    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/google/android/play/analytics/PlayStore$PlayStoreBackgroundActionEvent;->hasServerLatencyMs:Z

    const-wide/16 v0, 0x0

    iput-wide v0, p0, Lcom/google/android/play/analytics/PlayStore$PlayStoreBackgroundActionEvent;->serverLatencyMs_:J

    return-object p0
.end method

.method public clearServerLogsCookie()Lcom/google/android/play/analytics/PlayStore$PlayStoreBackgroundActionEvent;
    .locals 1

    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/google/android/play/analytics/PlayStore$PlayStoreBackgroundActionEvent;->hasServerLogsCookie:Z

    sget-object v0, Lcom/google/protobuf/micro/ByteStringMicro;->EMPTY:Lcom/google/protobuf/micro/ByteStringMicro;

    iput-object v0, p0, Lcom/google/android/play/analytics/PlayStore$PlayStoreBackgroundActionEvent;->serverLogsCookie_:Lcom/google/protobuf/micro/ByteStringMicro;

    return-object p0
.end method

.method public clearSessionInfo()Lcom/google/android/play/analytics/PlayStore$PlayStoreBackgroundActionEvent;
    .locals 1

    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/google/android/play/analytics/PlayStore$PlayStoreBackgroundActionEvent;->hasSessionInfo:Z

    const/4 v0, 0x0

    iput-object v0, p0, Lcom/google/android/play/analytics/PlayStore$PlayStoreBackgroundActionEvent;->sessionInfo_:Lcom/google/android/play/analytics/PlayStore$PlayStoreSessionData;

    return-object p0
.end method

.method public clearToSetting()Lcom/google/android/play/analytics/PlayStore$PlayStoreBackgroundActionEvent;
    .locals 1

    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/google/android/play/analytics/PlayStore$PlayStoreBackgroundActionEvent;->hasToSetting:Z

    iput v0, p0, Lcom/google/android/play/analytics/PlayStore$PlayStoreBackgroundActionEvent;->toSetting_:I

    return-object p0
.end method

.method public clearType()Lcom/google/android/play/analytics/PlayStore$PlayStoreBackgroundActionEvent;
    .locals 1

    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/google/android/play/analytics/PlayStore$PlayStoreBackgroundActionEvent;->hasType:Z

    iput v0, p0, Lcom/google/android/play/analytics/PlayStore$PlayStoreBackgroundActionEvent;->type_:I

    return-object p0
.end method

.method public getAppData()Lcom/google/android/play/analytics/PlayStore$AppData;
    .locals 1

    iget-object v0, p0, Lcom/google/android/play/analytics/PlayStore$PlayStoreBackgroundActionEvent;->appData_:Lcom/google/android/play/analytics/PlayStore$AppData;

    return-object v0
.end method

.method public getCachedSize()I
    .locals 1

    iget v0, p0, Lcom/google/android/play/analytics/PlayStore$PlayStoreBackgroundActionEvent;->cachedSize:I

    if-gez v0, :cond_0

    invoke-virtual {p0}, Lcom/google/android/play/analytics/PlayStore$PlayStoreBackgroundActionEvent;->getSerializedSize()I

    :cond_0
    iget v0, p0, Lcom/google/android/play/analytics/PlayStore$PlayStoreBackgroundActionEvent;->cachedSize:I

    return v0
.end method

.method public getClientLatencyMs()J
    .locals 2

    iget-wide v0, p0, Lcom/google/android/play/analytics/PlayStore$PlayStoreBackgroundActionEvent;->clientLatencyMs_:J

    return-wide v0
.end method

.method public getDocument()Ljava/lang/String;
    .locals 1

    iget-object v0, p0, Lcom/google/android/play/analytics/PlayStore$PlayStoreBackgroundActionEvent;->document_:Ljava/lang/String;

    return-object v0
.end method

.method public getErrorCode()I
    .locals 1

    iget v0, p0, Lcom/google/android/play/analytics/PlayStore$PlayStoreBackgroundActionEvent;->errorCode_:I

    return v0
.end method

.method public getExceptionType()Ljava/lang/String;
    .locals 1

    iget-object v0, p0, Lcom/google/android/play/analytics/PlayStore$PlayStoreBackgroundActionEvent;->exceptionType_:Ljava/lang/String;

    return-object v0
.end method

.method public getFromSetting()I
    .locals 1

    iget v0, p0, Lcom/google/android/play/analytics/PlayStore$PlayStoreBackgroundActionEvent;->fromSetting_:I

    return v0
.end method

.method public getOfferType()I
    .locals 1

    iget v0, p0, Lcom/google/android/play/analytics/PlayStore$PlayStoreBackgroundActionEvent;->offerType_:I

    return v0
.end method

.method public getReason()Ljava/lang/String;
    .locals 1

    iget-object v0, p0, Lcom/google/android/play/analytics/PlayStore$PlayStoreBackgroundActionEvent;->reason_:Ljava/lang/String;

    return-object v0
.end method

.method public getSerializedSize()I
    .locals 4

    const/4 v0, 0x0

    invoke-virtual {p0}, Lcom/google/android/play/analytics/PlayStore$PlayStoreBackgroundActionEvent;->hasType()Z

    move-result v1

    if-eqz v1, :cond_0

    const/4 v1, 0x1

    invoke-virtual {p0}, Lcom/google/android/play/analytics/PlayStore$PlayStoreBackgroundActionEvent;->getType()I

    move-result v2

    invoke-static {v1, v2}, Lcom/google/protobuf/micro/CodedOutputStreamMicro;->computeInt32Size(II)I

    move-result v1

    add-int/2addr v0, v1

    :cond_0
    invoke-virtual {p0}, Lcom/google/android/play/analytics/PlayStore$PlayStoreBackgroundActionEvent;->hasDocument()Z

    move-result v1

    if-eqz v1, :cond_1

    const/4 v1, 0x2

    invoke-virtual {p0}, Lcom/google/android/play/analytics/PlayStore$PlayStoreBackgroundActionEvent;->getDocument()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2}, Lcom/google/protobuf/micro/CodedOutputStreamMicro;->computeStringSize(ILjava/lang/String;)I

    move-result v1

    add-int/2addr v0, v1

    :cond_1
    invoke-virtual {p0}, Lcom/google/android/play/analytics/PlayStore$PlayStoreBackgroundActionEvent;->hasReason()Z

    move-result v1

    if-eqz v1, :cond_2

    const/4 v1, 0x3

    invoke-virtual {p0}, Lcom/google/android/play/analytics/PlayStore$PlayStoreBackgroundActionEvent;->getReason()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2}, Lcom/google/protobuf/micro/CodedOutputStreamMicro;->computeStringSize(ILjava/lang/String;)I

    move-result v1

    add-int/2addr v0, v1

    :cond_2
    invoke-virtual {p0}, Lcom/google/android/play/analytics/PlayStore$PlayStoreBackgroundActionEvent;->hasErrorCode()Z

    move-result v1

    if-eqz v1, :cond_3

    const/4 v1, 0x4

    invoke-virtual {p0}, Lcom/google/android/play/analytics/PlayStore$PlayStoreBackgroundActionEvent;->getErrorCode()I

    move-result v2

    invoke-static {v1, v2}, Lcom/google/protobuf/micro/CodedOutputStreamMicro;->computeInt32Size(II)I

    move-result v1

    add-int/2addr v0, v1

    :cond_3
    invoke-virtual {p0}, Lcom/google/android/play/analytics/PlayStore$PlayStoreBackgroundActionEvent;->hasExceptionType()Z

    move-result v1

    if-eqz v1, :cond_4

    const/4 v1, 0x5

    invoke-virtual {p0}, Lcom/google/android/play/analytics/PlayStore$PlayStoreBackgroundActionEvent;->getExceptionType()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2}, Lcom/google/protobuf/micro/CodedOutputStreamMicro;->computeStringSize(ILjava/lang/String;)I

    move-result v1

    add-int/2addr v0, v1

    :cond_4
    invoke-virtual {p0}, Lcom/google/android/play/analytics/PlayStore$PlayStoreBackgroundActionEvent;->hasServerLogsCookie()Z

    move-result v1

    if-eqz v1, :cond_5

    const/4 v1, 0x6

    invoke-virtual {p0}, Lcom/google/android/play/analytics/PlayStore$PlayStoreBackgroundActionEvent;->getServerLogsCookie()Lcom/google/protobuf/micro/ByteStringMicro;

    move-result-object v2

    invoke-static {v1, v2}, Lcom/google/protobuf/micro/CodedOutputStreamMicro;->computeBytesSize(ILcom/google/protobuf/micro/ByteStringMicro;)I

    move-result v1

    add-int/2addr v0, v1

    :cond_5
    invoke-virtual {p0}, Lcom/google/android/play/analytics/PlayStore$PlayStoreBackgroundActionEvent;->hasOfferType()Z

    move-result v1

    if-eqz v1, :cond_6

    const/4 v1, 0x7

    invoke-virtual {p0}, Lcom/google/android/play/analytics/PlayStore$PlayStoreBackgroundActionEvent;->getOfferType()I

    move-result v2

    invoke-static {v1, v2}, Lcom/google/protobuf/micro/CodedOutputStreamMicro;->computeInt32Size(II)I

    move-result v1

    add-int/2addr v0, v1

    :cond_6
    invoke-virtual {p0}, Lcom/google/android/play/analytics/PlayStore$PlayStoreBackgroundActionEvent;->hasFromSetting()Z

    move-result v1

    if-eqz v1, :cond_7

    const/16 v1, 0x8

    invoke-virtual {p0}, Lcom/google/android/play/analytics/PlayStore$PlayStoreBackgroundActionEvent;->getFromSetting()I

    move-result v2

    invoke-static {v1, v2}, Lcom/google/protobuf/micro/CodedOutputStreamMicro;->computeInt32Size(II)I

    move-result v1

    add-int/2addr v0, v1

    :cond_7
    invoke-virtual {p0}, Lcom/google/android/play/analytics/PlayStore$PlayStoreBackgroundActionEvent;->hasToSetting()Z

    move-result v1

    if-eqz v1, :cond_8

    const/16 v1, 0x9

    invoke-virtual {p0}, Lcom/google/android/play/analytics/PlayStore$PlayStoreBackgroundActionEvent;->getToSetting()I

    move-result v2

    invoke-static {v1, v2}, Lcom/google/protobuf/micro/CodedOutputStreamMicro;->computeInt32Size(II)I

    move-result v1

    add-int/2addr v0, v1

    :cond_8
    invoke-virtual {p0}, Lcom/google/android/play/analytics/PlayStore$PlayStoreBackgroundActionEvent;->hasSessionInfo()Z

    move-result v1

    if-eqz v1, :cond_9

    const/16 v1, 0xa

    invoke-virtual {p0}, Lcom/google/android/play/analytics/PlayStore$PlayStoreBackgroundActionEvent;->getSessionInfo()Lcom/google/android/play/analytics/PlayStore$PlayStoreSessionData;

    move-result-object v2

    invoke-static {v1, v2}, Lcom/google/protobuf/micro/CodedOutputStreamMicro;->computeMessageSize(ILcom/google/protobuf/micro/MessageMicro;)I

    move-result v1

    add-int/2addr v0, v1

    :cond_9
    invoke-virtual {p0}, Lcom/google/android/play/analytics/PlayStore$PlayStoreBackgroundActionEvent;->hasAppData()Z

    move-result v1

    if-eqz v1, :cond_a

    const/16 v1, 0xb

    invoke-virtual {p0}, Lcom/google/android/play/analytics/PlayStore$PlayStoreBackgroundActionEvent;->getAppData()Lcom/google/android/play/analytics/PlayStore$AppData;

    move-result-object v2

    invoke-static {v1, v2}, Lcom/google/protobuf/micro/CodedOutputStreamMicro;->computeMessageSize(ILcom/google/protobuf/micro/MessageMicro;)I

    move-result v1

    add-int/2addr v0, v1

    :cond_a
    invoke-virtual {p0}, Lcom/google/android/play/analytics/PlayStore$PlayStoreBackgroundActionEvent;->hasServerLatencyMs()Z

    move-result v1

    if-eqz v1, :cond_b

    const/16 v1, 0xc

    invoke-virtual {p0}, Lcom/google/android/play/analytics/PlayStore$PlayStoreBackgroundActionEvent;->getServerLatencyMs()J

    move-result-wide v2

    invoke-static {v1, v2, v3}, Lcom/google/protobuf/micro/CodedOutputStreamMicro;->computeInt64Size(IJ)I

    move-result v1

    add-int/2addr v0, v1

    :cond_b
    invoke-virtual {p0}, Lcom/google/android/play/analytics/PlayStore$PlayStoreBackgroundActionEvent;->hasClientLatencyMs()Z

    move-result v1

    if-eqz v1, :cond_c

    const/16 v1, 0xd

    invoke-virtual {p0}, Lcom/google/android/play/analytics/PlayStore$PlayStoreBackgroundActionEvent;->getClientLatencyMs()J

    move-result-wide v2

    invoke-static {v1, v2, v3}, Lcom/google/protobuf/micro/CodedOutputStreamMicro;->computeInt64Size(IJ)I

    move-result v1

    add-int/2addr v0, v1

    :cond_c
    iput v0, p0, Lcom/google/android/play/analytics/PlayStore$PlayStoreBackgroundActionEvent;->cachedSize:I

    return v0
.end method

.method public getServerLatencyMs()J
    .locals 2

    iget-wide v0, p0, Lcom/google/android/play/analytics/PlayStore$PlayStoreBackgroundActionEvent;->serverLatencyMs_:J

    return-wide v0
.end method

.method public getServerLogsCookie()Lcom/google/protobuf/micro/ByteStringMicro;
    .locals 1

    iget-object v0, p0, Lcom/google/android/play/analytics/PlayStore$PlayStoreBackgroundActionEvent;->serverLogsCookie_:Lcom/google/protobuf/micro/ByteStringMicro;

    return-object v0
.end method

.method public getSessionInfo()Lcom/google/android/play/analytics/PlayStore$PlayStoreSessionData;
    .locals 1

    iget-object v0, p0, Lcom/google/android/play/analytics/PlayStore$PlayStoreBackgroundActionEvent;->sessionInfo_:Lcom/google/android/play/analytics/PlayStore$PlayStoreSessionData;

    return-object v0
.end method

.method public getToSetting()I
    .locals 1

    iget v0, p0, Lcom/google/android/play/analytics/PlayStore$PlayStoreBackgroundActionEvent;->toSetting_:I

    return v0
.end method

.method public getType()I
    .locals 1

    iget v0, p0, Lcom/google/android/play/analytics/PlayStore$PlayStoreBackgroundActionEvent;->type_:I

    return v0
.end method

.method public hasAppData()Z
    .locals 1

    iget-boolean v0, p0, Lcom/google/android/play/analytics/PlayStore$PlayStoreBackgroundActionEvent;->hasAppData:Z

    return v0
.end method

.method public hasClientLatencyMs()Z
    .locals 1

    iget-boolean v0, p0, Lcom/google/android/play/analytics/PlayStore$PlayStoreBackgroundActionEvent;->hasClientLatencyMs:Z

    return v0
.end method

.method public hasDocument()Z
    .locals 1

    iget-boolean v0, p0, Lcom/google/android/play/analytics/PlayStore$PlayStoreBackgroundActionEvent;->hasDocument:Z

    return v0
.end method

.method public hasErrorCode()Z
    .locals 1

    iget-boolean v0, p0, Lcom/google/android/play/analytics/PlayStore$PlayStoreBackgroundActionEvent;->hasErrorCode:Z

    return v0
.end method

.method public hasExceptionType()Z
    .locals 1

    iget-boolean v0, p0, Lcom/google/android/play/analytics/PlayStore$PlayStoreBackgroundActionEvent;->hasExceptionType:Z

    return v0
.end method

.method public hasFromSetting()Z
    .locals 1

    iget-boolean v0, p0, Lcom/google/android/play/analytics/PlayStore$PlayStoreBackgroundActionEvent;->hasFromSetting:Z

    return v0
.end method

.method public hasOfferType()Z
    .locals 1

    iget-boolean v0, p0, Lcom/google/android/play/analytics/PlayStore$PlayStoreBackgroundActionEvent;->hasOfferType:Z

    return v0
.end method

.method public hasReason()Z
    .locals 1

    iget-boolean v0, p0, Lcom/google/android/play/analytics/PlayStore$PlayStoreBackgroundActionEvent;->hasReason:Z

    return v0
.end method

.method public hasServerLatencyMs()Z
    .locals 1

    iget-boolean v0, p0, Lcom/google/android/play/analytics/PlayStore$PlayStoreBackgroundActionEvent;->hasServerLatencyMs:Z

    return v0
.end method

.method public hasServerLogsCookie()Z
    .locals 1

    iget-boolean v0, p0, Lcom/google/android/play/analytics/PlayStore$PlayStoreBackgroundActionEvent;->hasServerLogsCookie:Z

    return v0
.end method

.method public hasSessionInfo()Z
    .locals 1

    iget-boolean v0, p0, Lcom/google/android/play/analytics/PlayStore$PlayStoreBackgroundActionEvent;->hasSessionInfo:Z

    return v0
.end method

.method public hasToSetting()Z
    .locals 1

    iget-boolean v0, p0, Lcom/google/android/play/analytics/PlayStore$PlayStoreBackgroundActionEvent;->hasToSetting:Z

    return v0
.end method

.method public hasType()Z
    .locals 1

    iget-boolean v0, p0, Lcom/google/android/play/analytics/PlayStore$PlayStoreBackgroundActionEvent;->hasType:Z

    return v0
.end method

.method public mergeFrom(Lcom/google/protobuf/micro/CodedInputStreamMicro;)Lcom/google/android/play/analytics/PlayStore$PlayStoreBackgroundActionEvent;
    .locals 4
    .param p1    # Lcom/google/protobuf/micro/CodedInputStreamMicro;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    :cond_0
    :goto_0
    invoke-virtual {p1}, Lcom/google/protobuf/micro/CodedInputStreamMicro;->readTag()I

    move-result v0

    sparse-switch v0, :sswitch_data_0

    invoke-virtual {p0, p1, v0}, Lcom/google/android/play/analytics/PlayStore$PlayStoreBackgroundActionEvent;->parseUnknownField(Lcom/google/protobuf/micro/CodedInputStreamMicro;I)Z

    move-result v2

    if-nez v2, :cond_0

    :sswitch_0
    return-object p0

    :sswitch_1
    invoke-virtual {p1}, Lcom/google/protobuf/micro/CodedInputStreamMicro;->readInt32()I

    move-result v2

    invoke-virtual {p0, v2}, Lcom/google/android/play/analytics/PlayStore$PlayStoreBackgroundActionEvent;->setType(I)Lcom/google/android/play/analytics/PlayStore$PlayStoreBackgroundActionEvent;

    goto :goto_0

    :sswitch_2
    invoke-virtual {p1}, Lcom/google/protobuf/micro/CodedInputStreamMicro;->readString()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {p0, v2}, Lcom/google/android/play/analytics/PlayStore$PlayStoreBackgroundActionEvent;->setDocument(Ljava/lang/String;)Lcom/google/android/play/analytics/PlayStore$PlayStoreBackgroundActionEvent;

    goto :goto_0

    :sswitch_3
    invoke-virtual {p1}, Lcom/google/protobuf/micro/CodedInputStreamMicro;->readString()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {p0, v2}, Lcom/google/android/play/analytics/PlayStore$PlayStoreBackgroundActionEvent;->setReason(Ljava/lang/String;)Lcom/google/android/play/analytics/PlayStore$PlayStoreBackgroundActionEvent;

    goto :goto_0

    :sswitch_4
    invoke-virtual {p1}, Lcom/google/protobuf/micro/CodedInputStreamMicro;->readInt32()I

    move-result v2

    invoke-virtual {p0, v2}, Lcom/google/android/play/analytics/PlayStore$PlayStoreBackgroundActionEvent;->setErrorCode(I)Lcom/google/android/play/analytics/PlayStore$PlayStoreBackgroundActionEvent;

    goto :goto_0

    :sswitch_5
    invoke-virtual {p1}, Lcom/google/protobuf/micro/CodedInputStreamMicro;->readString()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {p0, v2}, Lcom/google/android/play/analytics/PlayStore$PlayStoreBackgroundActionEvent;->setExceptionType(Ljava/lang/String;)Lcom/google/android/play/analytics/PlayStore$PlayStoreBackgroundActionEvent;

    goto :goto_0

    :sswitch_6
    invoke-virtual {p1}, Lcom/google/protobuf/micro/CodedInputStreamMicro;->readBytes()Lcom/google/protobuf/micro/ByteStringMicro;

    move-result-object v2

    invoke-virtual {p0, v2}, Lcom/google/android/play/analytics/PlayStore$PlayStoreBackgroundActionEvent;->setServerLogsCookie(Lcom/google/protobuf/micro/ByteStringMicro;)Lcom/google/android/play/analytics/PlayStore$PlayStoreBackgroundActionEvent;

    goto :goto_0

    :sswitch_7
    invoke-virtual {p1}, Lcom/google/protobuf/micro/CodedInputStreamMicro;->readInt32()I

    move-result v2

    invoke-virtual {p0, v2}, Lcom/google/android/play/analytics/PlayStore$PlayStoreBackgroundActionEvent;->setOfferType(I)Lcom/google/android/play/analytics/PlayStore$PlayStoreBackgroundActionEvent;

    goto :goto_0

    :sswitch_8
    invoke-virtual {p1}, Lcom/google/protobuf/micro/CodedInputStreamMicro;->readInt32()I

    move-result v2

    invoke-virtual {p0, v2}, Lcom/google/android/play/analytics/PlayStore$PlayStoreBackgroundActionEvent;->setFromSetting(I)Lcom/google/android/play/analytics/PlayStore$PlayStoreBackgroundActionEvent;

    goto :goto_0

    :sswitch_9
    invoke-virtual {p1}, Lcom/google/protobuf/micro/CodedInputStreamMicro;->readInt32()I

    move-result v2

    invoke-virtual {p0, v2}, Lcom/google/android/play/analytics/PlayStore$PlayStoreBackgroundActionEvent;->setToSetting(I)Lcom/google/android/play/analytics/PlayStore$PlayStoreBackgroundActionEvent;

    goto :goto_0

    :sswitch_a
    new-instance v1, Lcom/google/android/play/analytics/PlayStore$PlayStoreSessionData;

    invoke-direct {v1}, Lcom/google/android/play/analytics/PlayStore$PlayStoreSessionData;-><init>()V

    invoke-virtual {p1, v1}, Lcom/google/protobuf/micro/CodedInputStreamMicro;->readMessage(Lcom/google/protobuf/micro/MessageMicro;)V

    invoke-virtual {p0, v1}, Lcom/google/android/play/analytics/PlayStore$PlayStoreBackgroundActionEvent;->setSessionInfo(Lcom/google/android/play/analytics/PlayStore$PlayStoreSessionData;)Lcom/google/android/play/analytics/PlayStore$PlayStoreBackgroundActionEvent;

    goto :goto_0

    :sswitch_b
    new-instance v1, Lcom/google/android/play/analytics/PlayStore$AppData;

    invoke-direct {v1}, Lcom/google/android/play/analytics/PlayStore$AppData;-><init>()V

    invoke-virtual {p1, v1}, Lcom/google/protobuf/micro/CodedInputStreamMicro;->readMessage(Lcom/google/protobuf/micro/MessageMicro;)V

    invoke-virtual {p0, v1}, Lcom/google/android/play/analytics/PlayStore$PlayStoreBackgroundActionEvent;->setAppData(Lcom/google/android/play/analytics/PlayStore$AppData;)Lcom/google/android/play/analytics/PlayStore$PlayStoreBackgroundActionEvent;

    goto :goto_0

    :sswitch_c
    invoke-virtual {p1}, Lcom/google/protobuf/micro/CodedInputStreamMicro;->readInt64()J

    move-result-wide v2

    invoke-virtual {p0, v2, v3}, Lcom/google/android/play/analytics/PlayStore$PlayStoreBackgroundActionEvent;->setServerLatencyMs(J)Lcom/google/android/play/analytics/PlayStore$PlayStoreBackgroundActionEvent;

    goto :goto_0

    :sswitch_d
    invoke-virtual {p1}, Lcom/google/protobuf/micro/CodedInputStreamMicro;->readInt64()J

    move-result-wide v2

    invoke-virtual {p0, v2, v3}, Lcom/google/android/play/analytics/PlayStore$PlayStoreBackgroundActionEvent;->setClientLatencyMs(J)Lcom/google/android/play/analytics/PlayStore$PlayStoreBackgroundActionEvent;

    goto :goto_0

    :sswitch_data_0
    .sparse-switch
        0x0 -> :sswitch_0
        0x8 -> :sswitch_1
        0x12 -> :sswitch_2
        0x1a -> :sswitch_3
        0x20 -> :sswitch_4
        0x2a -> :sswitch_5
        0x32 -> :sswitch_6
        0x38 -> :sswitch_7
        0x40 -> :sswitch_8
        0x48 -> :sswitch_9
        0x52 -> :sswitch_a
        0x5a -> :sswitch_b
        0x60 -> :sswitch_c
        0x68 -> :sswitch_d
    .end sparse-switch
.end method

.method public bridge synthetic mergeFrom(Lcom/google/protobuf/micro/CodedInputStreamMicro;)Lcom/google/protobuf/micro/MessageMicro;
    .locals 1
    .param p1    # Lcom/google/protobuf/micro/CodedInputStreamMicro;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    invoke-virtual {p0, p1}, Lcom/google/android/play/analytics/PlayStore$PlayStoreBackgroundActionEvent;->mergeFrom(Lcom/google/protobuf/micro/CodedInputStreamMicro;)Lcom/google/android/play/analytics/PlayStore$PlayStoreBackgroundActionEvent;

    move-result-object v0

    return-object v0
.end method

.method public setAppData(Lcom/google/android/play/analytics/PlayStore$AppData;)Lcom/google/android/play/analytics/PlayStore$PlayStoreBackgroundActionEvent;
    .locals 1
    .param p1    # Lcom/google/android/play/analytics/PlayStore$AppData;

    if-nez p1, :cond_0

    new-instance v0, Ljava/lang/NullPointerException;

    invoke-direct {v0}, Ljava/lang/NullPointerException;-><init>()V

    throw v0

    :cond_0
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/google/android/play/analytics/PlayStore$PlayStoreBackgroundActionEvent;->hasAppData:Z

    iput-object p1, p0, Lcom/google/android/play/analytics/PlayStore$PlayStoreBackgroundActionEvent;->appData_:Lcom/google/android/play/analytics/PlayStore$AppData;

    return-object p0
.end method

.method public setClientLatencyMs(J)Lcom/google/android/play/analytics/PlayStore$PlayStoreBackgroundActionEvent;
    .locals 1
    .param p1    # J

    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/google/android/play/analytics/PlayStore$PlayStoreBackgroundActionEvent;->hasClientLatencyMs:Z

    iput-wide p1, p0, Lcom/google/android/play/analytics/PlayStore$PlayStoreBackgroundActionEvent;->clientLatencyMs_:J

    return-object p0
.end method

.method public setDocument(Ljava/lang/String;)Lcom/google/android/play/analytics/PlayStore$PlayStoreBackgroundActionEvent;
    .locals 1
    .param p1    # Ljava/lang/String;

    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/google/android/play/analytics/PlayStore$PlayStoreBackgroundActionEvent;->hasDocument:Z

    iput-object p1, p0, Lcom/google/android/play/analytics/PlayStore$PlayStoreBackgroundActionEvent;->document_:Ljava/lang/String;

    return-object p0
.end method

.method public setErrorCode(I)Lcom/google/android/play/analytics/PlayStore$PlayStoreBackgroundActionEvent;
    .locals 1
    .param p1    # I

    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/google/android/play/analytics/PlayStore$PlayStoreBackgroundActionEvent;->hasErrorCode:Z

    iput p1, p0, Lcom/google/android/play/analytics/PlayStore$PlayStoreBackgroundActionEvent;->errorCode_:I

    return-object p0
.end method

.method public setExceptionType(Ljava/lang/String;)Lcom/google/android/play/analytics/PlayStore$PlayStoreBackgroundActionEvent;
    .locals 1
    .param p1    # Ljava/lang/String;

    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/google/android/play/analytics/PlayStore$PlayStoreBackgroundActionEvent;->hasExceptionType:Z

    iput-object p1, p0, Lcom/google/android/play/analytics/PlayStore$PlayStoreBackgroundActionEvent;->exceptionType_:Ljava/lang/String;

    return-object p0
.end method

.method public setFromSetting(I)Lcom/google/android/play/analytics/PlayStore$PlayStoreBackgroundActionEvent;
    .locals 1
    .param p1    # I

    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/google/android/play/analytics/PlayStore$PlayStoreBackgroundActionEvent;->hasFromSetting:Z

    iput p1, p0, Lcom/google/android/play/analytics/PlayStore$PlayStoreBackgroundActionEvent;->fromSetting_:I

    return-object p0
.end method

.method public setOfferType(I)Lcom/google/android/play/analytics/PlayStore$PlayStoreBackgroundActionEvent;
    .locals 1
    .param p1    # I

    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/google/android/play/analytics/PlayStore$PlayStoreBackgroundActionEvent;->hasOfferType:Z

    iput p1, p0, Lcom/google/android/play/analytics/PlayStore$PlayStoreBackgroundActionEvent;->offerType_:I

    return-object p0
.end method

.method public setReason(Ljava/lang/String;)Lcom/google/android/play/analytics/PlayStore$PlayStoreBackgroundActionEvent;
    .locals 1
    .param p1    # Ljava/lang/String;

    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/google/android/play/analytics/PlayStore$PlayStoreBackgroundActionEvent;->hasReason:Z

    iput-object p1, p0, Lcom/google/android/play/analytics/PlayStore$PlayStoreBackgroundActionEvent;->reason_:Ljava/lang/String;

    return-object p0
.end method

.method public setServerLatencyMs(J)Lcom/google/android/play/analytics/PlayStore$PlayStoreBackgroundActionEvent;
    .locals 1
    .param p1    # J

    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/google/android/play/analytics/PlayStore$PlayStoreBackgroundActionEvent;->hasServerLatencyMs:Z

    iput-wide p1, p0, Lcom/google/android/play/analytics/PlayStore$PlayStoreBackgroundActionEvent;->serverLatencyMs_:J

    return-object p0
.end method

.method public setServerLogsCookie(Lcom/google/protobuf/micro/ByteStringMicro;)Lcom/google/android/play/analytics/PlayStore$PlayStoreBackgroundActionEvent;
    .locals 1
    .param p1    # Lcom/google/protobuf/micro/ByteStringMicro;

    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/google/android/play/analytics/PlayStore$PlayStoreBackgroundActionEvent;->hasServerLogsCookie:Z

    iput-object p1, p0, Lcom/google/android/play/analytics/PlayStore$PlayStoreBackgroundActionEvent;->serverLogsCookie_:Lcom/google/protobuf/micro/ByteStringMicro;

    return-object p0
.end method

.method public setSessionInfo(Lcom/google/android/play/analytics/PlayStore$PlayStoreSessionData;)Lcom/google/android/play/analytics/PlayStore$PlayStoreBackgroundActionEvent;
    .locals 1
    .param p1    # Lcom/google/android/play/analytics/PlayStore$PlayStoreSessionData;

    if-nez p1, :cond_0

    new-instance v0, Ljava/lang/NullPointerException;

    invoke-direct {v0}, Ljava/lang/NullPointerException;-><init>()V

    throw v0

    :cond_0
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/google/android/play/analytics/PlayStore$PlayStoreBackgroundActionEvent;->hasSessionInfo:Z

    iput-object p1, p0, Lcom/google/android/play/analytics/PlayStore$PlayStoreBackgroundActionEvent;->sessionInfo_:Lcom/google/android/play/analytics/PlayStore$PlayStoreSessionData;

    return-object p0
.end method

.method public setToSetting(I)Lcom/google/android/play/analytics/PlayStore$PlayStoreBackgroundActionEvent;
    .locals 1
    .param p1    # I

    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/google/android/play/analytics/PlayStore$PlayStoreBackgroundActionEvent;->hasToSetting:Z

    iput p1, p0, Lcom/google/android/play/analytics/PlayStore$PlayStoreBackgroundActionEvent;->toSetting_:I

    return-object p0
.end method

.method public setType(I)Lcom/google/android/play/analytics/PlayStore$PlayStoreBackgroundActionEvent;
    .locals 1
    .param p1    # I

    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/google/android/play/analytics/PlayStore$PlayStoreBackgroundActionEvent;->hasType:Z

    iput p1, p0, Lcom/google/android/play/analytics/PlayStore$PlayStoreBackgroundActionEvent;->type_:I

    return-object p0
.end method

.method public writeTo(Lcom/google/protobuf/micro/CodedOutputStreamMicro;)V
    .locals 3
    .param p1    # Lcom/google/protobuf/micro/CodedOutputStreamMicro;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    invoke-virtual {p0}, Lcom/google/android/play/analytics/PlayStore$PlayStoreBackgroundActionEvent;->hasType()Z

    move-result v0

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    invoke-virtual {p0}, Lcom/google/android/play/analytics/PlayStore$PlayStoreBackgroundActionEvent;->getType()I

    move-result v1

    invoke-virtual {p1, v0, v1}, Lcom/google/protobuf/micro/CodedOutputStreamMicro;->writeInt32(II)V

    :cond_0
    invoke-virtual {p0}, Lcom/google/android/play/analytics/PlayStore$PlayStoreBackgroundActionEvent;->hasDocument()Z

    move-result v0

    if-eqz v0, :cond_1

    const/4 v0, 0x2

    invoke-virtual {p0}, Lcom/google/android/play/analytics/PlayStore$PlayStoreBackgroundActionEvent;->getDocument()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {p1, v0, v1}, Lcom/google/protobuf/micro/CodedOutputStreamMicro;->writeString(ILjava/lang/String;)V

    :cond_1
    invoke-virtual {p0}, Lcom/google/android/play/analytics/PlayStore$PlayStoreBackgroundActionEvent;->hasReason()Z

    move-result v0

    if-eqz v0, :cond_2

    const/4 v0, 0x3

    invoke-virtual {p0}, Lcom/google/android/play/analytics/PlayStore$PlayStoreBackgroundActionEvent;->getReason()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {p1, v0, v1}, Lcom/google/protobuf/micro/CodedOutputStreamMicro;->writeString(ILjava/lang/String;)V

    :cond_2
    invoke-virtual {p0}, Lcom/google/android/play/analytics/PlayStore$PlayStoreBackgroundActionEvent;->hasErrorCode()Z

    move-result v0

    if-eqz v0, :cond_3

    const/4 v0, 0x4

    invoke-virtual {p0}, Lcom/google/android/play/analytics/PlayStore$PlayStoreBackgroundActionEvent;->getErrorCode()I

    move-result v1

    invoke-virtual {p1, v0, v1}, Lcom/google/protobuf/micro/CodedOutputStreamMicro;->writeInt32(II)V

    :cond_3
    invoke-virtual {p0}, Lcom/google/android/play/analytics/PlayStore$PlayStoreBackgroundActionEvent;->hasExceptionType()Z

    move-result v0

    if-eqz v0, :cond_4

    const/4 v0, 0x5

    invoke-virtual {p0}, Lcom/google/android/play/analytics/PlayStore$PlayStoreBackgroundActionEvent;->getExceptionType()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {p1, v0, v1}, Lcom/google/protobuf/micro/CodedOutputStreamMicro;->writeString(ILjava/lang/String;)V

    :cond_4
    invoke-virtual {p0}, Lcom/google/android/play/analytics/PlayStore$PlayStoreBackgroundActionEvent;->hasServerLogsCookie()Z

    move-result v0

    if-eqz v0, :cond_5

    const/4 v0, 0x6

    invoke-virtual {p0}, Lcom/google/android/play/analytics/PlayStore$PlayStoreBackgroundActionEvent;->getServerLogsCookie()Lcom/google/protobuf/micro/ByteStringMicro;

    move-result-object v1

    invoke-virtual {p1, v0, v1}, Lcom/google/protobuf/micro/CodedOutputStreamMicro;->writeBytes(ILcom/google/protobuf/micro/ByteStringMicro;)V

    :cond_5
    invoke-virtual {p0}, Lcom/google/android/play/analytics/PlayStore$PlayStoreBackgroundActionEvent;->hasOfferType()Z

    move-result v0

    if-eqz v0, :cond_6

    const/4 v0, 0x7

    invoke-virtual {p0}, Lcom/google/android/play/analytics/PlayStore$PlayStoreBackgroundActionEvent;->getOfferType()I

    move-result v1

    invoke-virtual {p1, v0, v1}, Lcom/google/protobuf/micro/CodedOutputStreamMicro;->writeInt32(II)V

    :cond_6
    invoke-virtual {p0}, Lcom/google/android/play/analytics/PlayStore$PlayStoreBackgroundActionEvent;->hasFromSetting()Z

    move-result v0

    if-eqz v0, :cond_7

    const/16 v0, 0x8

    invoke-virtual {p0}, Lcom/google/android/play/analytics/PlayStore$PlayStoreBackgroundActionEvent;->getFromSetting()I

    move-result v1

    invoke-virtual {p1, v0, v1}, Lcom/google/protobuf/micro/CodedOutputStreamMicro;->writeInt32(II)V

    :cond_7
    invoke-virtual {p0}, Lcom/google/android/play/analytics/PlayStore$PlayStoreBackgroundActionEvent;->hasToSetting()Z

    move-result v0

    if-eqz v0, :cond_8

    const/16 v0, 0x9

    invoke-virtual {p0}, Lcom/google/android/play/analytics/PlayStore$PlayStoreBackgroundActionEvent;->getToSetting()I

    move-result v1

    invoke-virtual {p1, v0, v1}, Lcom/google/protobuf/micro/CodedOutputStreamMicro;->writeInt32(II)V

    :cond_8
    invoke-virtual {p0}, Lcom/google/android/play/analytics/PlayStore$PlayStoreBackgroundActionEvent;->hasSessionInfo()Z

    move-result v0

    if-eqz v0, :cond_9

    const/16 v0, 0xa

    invoke-virtual {p0}, Lcom/google/android/play/analytics/PlayStore$PlayStoreBackgroundActionEvent;->getSessionInfo()Lcom/google/android/play/analytics/PlayStore$PlayStoreSessionData;

    move-result-object v1

    invoke-virtual {p1, v0, v1}, Lcom/google/protobuf/micro/CodedOutputStreamMicro;->writeMessage(ILcom/google/protobuf/micro/MessageMicro;)V

    :cond_9
    invoke-virtual {p0}, Lcom/google/android/play/analytics/PlayStore$PlayStoreBackgroundActionEvent;->hasAppData()Z

    move-result v0

    if-eqz v0, :cond_a

    const/16 v0, 0xb

    invoke-virtual {p0}, Lcom/google/android/play/analytics/PlayStore$PlayStoreBackgroundActionEvent;->getAppData()Lcom/google/android/play/analytics/PlayStore$AppData;

    move-result-object v1

    invoke-virtual {p1, v0, v1}, Lcom/google/protobuf/micro/CodedOutputStreamMicro;->writeMessage(ILcom/google/protobuf/micro/MessageMicro;)V

    :cond_a
    invoke-virtual {p0}, Lcom/google/android/play/analytics/PlayStore$PlayStoreBackgroundActionEvent;->hasServerLatencyMs()Z

    move-result v0

    if-eqz v0, :cond_b

    const/16 v0, 0xc

    invoke-virtual {p0}, Lcom/google/android/play/analytics/PlayStore$PlayStoreBackgroundActionEvent;->getServerLatencyMs()J

    move-result-wide v1

    invoke-virtual {p1, v0, v1, v2}, Lcom/google/protobuf/micro/CodedOutputStreamMicro;->writeInt64(IJ)V

    :cond_b
    invoke-virtual {p0}, Lcom/google/android/play/analytics/PlayStore$PlayStoreBackgroundActionEvent;->hasClientLatencyMs()Z

    move-result v0

    if-eqz v0, :cond_c

    const/16 v0, 0xd

    invoke-virtual {p0}, Lcom/google/android/play/analytics/PlayStore$PlayStoreBackgroundActionEvent;->getClientLatencyMs()J

    move-result-wide v1

    invoke-virtual {p1, v0, v1, v2}, Lcom/google/protobuf/micro/CodedOutputStreamMicro;->writeInt64(IJ)V

    :cond_c
    return-void
.end method
