.class public final Lcom/google/android/finsky/protos/DocDetails;
.super Ljava/lang/Object;
.source "DocDetails.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/google/android/finsky/protos/DocDetails$TvEpisodeDetails;,
        Lcom/google/android/finsky/protos/DocDetails$TvSeasonDetails;,
        Lcom/google/android/finsky/protos/DocDetails$TvShowDetails;,
        Lcom/google/android/finsky/protos/DocDetails$NewsDetails;,
        Lcom/google/android/finsky/protos/DocDetails$MagazineDetails;,
        Lcom/google/android/finsky/protos/DocDetails$SubscriptionDetails;,
        Lcom/google/android/finsky/protos/DocDetails$VideoRentalTerm;,
        Lcom/google/android/finsky/protos/DocDetails$Trailer;,
        Lcom/google/android/finsky/protos/DocDetails$VideoCredit;,
        Lcom/google/android/finsky/protos/DocDetails$VideoDetails;,
        Lcom/google/android/finsky/protos/DocDetails$ArtistExternalLinks;,
        Lcom/google/android/finsky/protos/DocDetails$ArtistDetails;,
        Lcom/google/android/finsky/protos/DocDetails$SongDetails;,
        Lcom/google/android/finsky/protos/DocDetails$AlbumDetails;,
        Lcom/google/android/finsky/protos/DocDetails$MusicDetails;,
        Lcom/google/android/finsky/protos/DocDetails$AppPermission;,
        Lcom/google/android/finsky/protos/DocDetails$AppDetails;,
        Lcom/google/android/finsky/protos/DocDetails$FileMetadata;,
        Lcom/google/android/finsky/protos/DocDetails$DocumentDetails;
    }
.end annotation


# direct methods
.method private constructor <init>()V
    .locals 0

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method
