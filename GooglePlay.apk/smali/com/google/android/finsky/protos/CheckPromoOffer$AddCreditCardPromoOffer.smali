.class public final Lcom/google/android/finsky/protos/CheckPromoOffer$AddCreditCardPromoOffer;
.super Lcom/google/protobuf/micro/MessageMicro;
.source "CheckPromoOffer.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/google/android/finsky/protos/CheckPromoOffer;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x19
    name = "AddCreditCardPromoOffer"
.end annotation


# instance fields
.field private cachedSize:I

.field private descriptionHtml_:Ljava/lang/String;

.field private hasDescriptionHtml:Z

.field private hasHeaderText:Z

.field private hasImage:Z

.field private hasIntroductoryTextHtml:Z

.field private hasNoActionDescription:Z

.field private hasOfferTitle:Z

.field private hasTermsAndConditionsHtml:Z

.field private headerText_:Ljava/lang/String;

.field private image_:Lcom/google/android/finsky/protos/Doc$Image;

.field private introductoryTextHtml_:Ljava/lang/String;

.field private noActionDescription_:Ljava/lang/String;

.field private offerTitle_:Ljava/lang/String;

.field private termsAndConditionsHtml_:Ljava/lang/String;


# direct methods
.method public constructor <init>()V
    .locals 1

    invoke-direct {p0}, Lcom/google/protobuf/micro/MessageMicro;-><init>()V

    const-string v0, ""

    iput-object v0, p0, Lcom/google/android/finsky/protos/CheckPromoOffer$AddCreditCardPromoOffer;->headerText_:Ljava/lang/String;

    const-string v0, ""

    iput-object v0, p0, Lcom/google/android/finsky/protos/CheckPromoOffer$AddCreditCardPromoOffer;->introductoryTextHtml_:Ljava/lang/String;

    const-string v0, ""

    iput-object v0, p0, Lcom/google/android/finsky/protos/CheckPromoOffer$AddCreditCardPromoOffer;->offerTitle_:Ljava/lang/String;

    const-string v0, ""

    iput-object v0, p0, Lcom/google/android/finsky/protos/CheckPromoOffer$AddCreditCardPromoOffer;->descriptionHtml_:Ljava/lang/String;

    const-string v0, ""

    iput-object v0, p0, Lcom/google/android/finsky/protos/CheckPromoOffer$AddCreditCardPromoOffer;->termsAndConditionsHtml_:Ljava/lang/String;

    const/4 v0, 0x0

    iput-object v0, p0, Lcom/google/android/finsky/protos/CheckPromoOffer$AddCreditCardPromoOffer;->image_:Lcom/google/android/finsky/protos/Doc$Image;

    const-string v0, ""

    iput-object v0, p0, Lcom/google/android/finsky/protos/CheckPromoOffer$AddCreditCardPromoOffer;->noActionDescription_:Ljava/lang/String;

    const/4 v0, -0x1

    iput v0, p0, Lcom/google/android/finsky/protos/CheckPromoOffer$AddCreditCardPromoOffer;->cachedSize:I

    return-void
.end method


# virtual methods
.method public getCachedSize()I
    .locals 1

    iget v0, p0, Lcom/google/android/finsky/protos/CheckPromoOffer$AddCreditCardPromoOffer;->cachedSize:I

    if-gez v0, :cond_0

    invoke-virtual {p0}, Lcom/google/android/finsky/protos/CheckPromoOffer$AddCreditCardPromoOffer;->getSerializedSize()I

    :cond_0
    iget v0, p0, Lcom/google/android/finsky/protos/CheckPromoOffer$AddCreditCardPromoOffer;->cachedSize:I

    return v0
.end method

.method public getDescriptionHtml()Ljava/lang/String;
    .locals 1

    iget-object v0, p0, Lcom/google/android/finsky/protos/CheckPromoOffer$AddCreditCardPromoOffer;->descriptionHtml_:Ljava/lang/String;

    return-object v0
.end method

.method public getHeaderText()Ljava/lang/String;
    .locals 1

    iget-object v0, p0, Lcom/google/android/finsky/protos/CheckPromoOffer$AddCreditCardPromoOffer;->headerText_:Ljava/lang/String;

    return-object v0
.end method

.method public getImage()Lcom/google/android/finsky/protos/Doc$Image;
    .locals 1

    iget-object v0, p0, Lcom/google/android/finsky/protos/CheckPromoOffer$AddCreditCardPromoOffer;->image_:Lcom/google/android/finsky/protos/Doc$Image;

    return-object v0
.end method

.method public getIntroductoryTextHtml()Ljava/lang/String;
    .locals 1

    iget-object v0, p0, Lcom/google/android/finsky/protos/CheckPromoOffer$AddCreditCardPromoOffer;->introductoryTextHtml_:Ljava/lang/String;

    return-object v0
.end method

.method public getNoActionDescription()Ljava/lang/String;
    .locals 1

    iget-object v0, p0, Lcom/google/android/finsky/protos/CheckPromoOffer$AddCreditCardPromoOffer;->noActionDescription_:Ljava/lang/String;

    return-object v0
.end method

.method public getOfferTitle()Ljava/lang/String;
    .locals 1

    iget-object v0, p0, Lcom/google/android/finsky/protos/CheckPromoOffer$AddCreditCardPromoOffer;->offerTitle_:Ljava/lang/String;

    return-object v0
.end method

.method public getSerializedSize()I
    .locals 3

    const/4 v0, 0x0

    invoke-virtual {p0}, Lcom/google/android/finsky/protos/CheckPromoOffer$AddCreditCardPromoOffer;->hasHeaderText()Z

    move-result v1

    if-eqz v1, :cond_0

    const/4 v1, 0x1

    invoke-virtual {p0}, Lcom/google/android/finsky/protos/CheckPromoOffer$AddCreditCardPromoOffer;->getHeaderText()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2}, Lcom/google/protobuf/micro/CodedOutputStreamMicro;->computeStringSize(ILjava/lang/String;)I

    move-result v1

    add-int/2addr v0, v1

    :cond_0
    invoke-virtual {p0}, Lcom/google/android/finsky/protos/CheckPromoOffer$AddCreditCardPromoOffer;->hasDescriptionHtml()Z

    move-result v1

    if-eqz v1, :cond_1

    const/4 v1, 0x2

    invoke-virtual {p0}, Lcom/google/android/finsky/protos/CheckPromoOffer$AddCreditCardPromoOffer;->getDescriptionHtml()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2}, Lcom/google/protobuf/micro/CodedOutputStreamMicro;->computeStringSize(ILjava/lang/String;)I

    move-result v1

    add-int/2addr v0, v1

    :cond_1
    invoke-virtual {p0}, Lcom/google/android/finsky/protos/CheckPromoOffer$AddCreditCardPromoOffer;->hasImage()Z

    move-result v1

    if-eqz v1, :cond_2

    const/4 v1, 0x3

    invoke-virtual {p0}, Lcom/google/android/finsky/protos/CheckPromoOffer$AddCreditCardPromoOffer;->getImage()Lcom/google/android/finsky/protos/Doc$Image;

    move-result-object v2

    invoke-static {v1, v2}, Lcom/google/protobuf/micro/CodedOutputStreamMicro;->computeMessageSize(ILcom/google/protobuf/micro/MessageMicro;)I

    move-result v1

    add-int/2addr v0, v1

    :cond_2
    invoke-virtual {p0}, Lcom/google/android/finsky/protos/CheckPromoOffer$AddCreditCardPromoOffer;->hasIntroductoryTextHtml()Z

    move-result v1

    if-eqz v1, :cond_3

    const/4 v1, 0x4

    invoke-virtual {p0}, Lcom/google/android/finsky/protos/CheckPromoOffer$AddCreditCardPromoOffer;->getIntroductoryTextHtml()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2}, Lcom/google/protobuf/micro/CodedOutputStreamMicro;->computeStringSize(ILjava/lang/String;)I

    move-result v1

    add-int/2addr v0, v1

    :cond_3
    invoke-virtual {p0}, Lcom/google/android/finsky/protos/CheckPromoOffer$AddCreditCardPromoOffer;->hasOfferTitle()Z

    move-result v1

    if-eqz v1, :cond_4

    const/4 v1, 0x5

    invoke-virtual {p0}, Lcom/google/android/finsky/protos/CheckPromoOffer$AddCreditCardPromoOffer;->getOfferTitle()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2}, Lcom/google/protobuf/micro/CodedOutputStreamMicro;->computeStringSize(ILjava/lang/String;)I

    move-result v1

    add-int/2addr v0, v1

    :cond_4
    invoke-virtual {p0}, Lcom/google/android/finsky/protos/CheckPromoOffer$AddCreditCardPromoOffer;->hasNoActionDescription()Z

    move-result v1

    if-eqz v1, :cond_5

    const/4 v1, 0x6

    invoke-virtual {p0}, Lcom/google/android/finsky/protos/CheckPromoOffer$AddCreditCardPromoOffer;->getNoActionDescription()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2}, Lcom/google/protobuf/micro/CodedOutputStreamMicro;->computeStringSize(ILjava/lang/String;)I

    move-result v1

    add-int/2addr v0, v1

    :cond_5
    invoke-virtual {p0}, Lcom/google/android/finsky/protos/CheckPromoOffer$AddCreditCardPromoOffer;->hasTermsAndConditionsHtml()Z

    move-result v1

    if-eqz v1, :cond_6

    const/4 v1, 0x7

    invoke-virtual {p0}, Lcom/google/android/finsky/protos/CheckPromoOffer$AddCreditCardPromoOffer;->getTermsAndConditionsHtml()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2}, Lcom/google/protobuf/micro/CodedOutputStreamMicro;->computeStringSize(ILjava/lang/String;)I

    move-result v1

    add-int/2addr v0, v1

    :cond_6
    iput v0, p0, Lcom/google/android/finsky/protos/CheckPromoOffer$AddCreditCardPromoOffer;->cachedSize:I

    return v0
.end method

.method public getTermsAndConditionsHtml()Ljava/lang/String;
    .locals 1

    iget-object v0, p0, Lcom/google/android/finsky/protos/CheckPromoOffer$AddCreditCardPromoOffer;->termsAndConditionsHtml_:Ljava/lang/String;

    return-object v0
.end method

.method public hasDescriptionHtml()Z
    .locals 1

    iget-boolean v0, p0, Lcom/google/android/finsky/protos/CheckPromoOffer$AddCreditCardPromoOffer;->hasDescriptionHtml:Z

    return v0
.end method

.method public hasHeaderText()Z
    .locals 1

    iget-boolean v0, p0, Lcom/google/android/finsky/protos/CheckPromoOffer$AddCreditCardPromoOffer;->hasHeaderText:Z

    return v0
.end method

.method public hasImage()Z
    .locals 1

    iget-boolean v0, p0, Lcom/google/android/finsky/protos/CheckPromoOffer$AddCreditCardPromoOffer;->hasImage:Z

    return v0
.end method

.method public hasIntroductoryTextHtml()Z
    .locals 1

    iget-boolean v0, p0, Lcom/google/android/finsky/protos/CheckPromoOffer$AddCreditCardPromoOffer;->hasIntroductoryTextHtml:Z

    return v0
.end method

.method public hasNoActionDescription()Z
    .locals 1

    iget-boolean v0, p0, Lcom/google/android/finsky/protos/CheckPromoOffer$AddCreditCardPromoOffer;->hasNoActionDescription:Z

    return v0
.end method

.method public hasOfferTitle()Z
    .locals 1

    iget-boolean v0, p0, Lcom/google/android/finsky/protos/CheckPromoOffer$AddCreditCardPromoOffer;->hasOfferTitle:Z

    return v0
.end method

.method public hasTermsAndConditionsHtml()Z
    .locals 1

    iget-boolean v0, p0, Lcom/google/android/finsky/protos/CheckPromoOffer$AddCreditCardPromoOffer;->hasTermsAndConditionsHtml:Z

    return v0
.end method

.method public mergeFrom(Lcom/google/protobuf/micro/CodedInputStreamMicro;)Lcom/google/android/finsky/protos/CheckPromoOffer$AddCreditCardPromoOffer;
    .locals 3
    .param p1    # Lcom/google/protobuf/micro/CodedInputStreamMicro;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    :cond_0
    :goto_0
    invoke-virtual {p1}, Lcom/google/protobuf/micro/CodedInputStreamMicro;->readTag()I

    move-result v0

    sparse-switch v0, :sswitch_data_0

    invoke-virtual {p0, p1, v0}, Lcom/google/android/finsky/protos/CheckPromoOffer$AddCreditCardPromoOffer;->parseUnknownField(Lcom/google/protobuf/micro/CodedInputStreamMicro;I)Z

    move-result v2

    if-nez v2, :cond_0

    :sswitch_0
    return-object p0

    :sswitch_1
    invoke-virtual {p1}, Lcom/google/protobuf/micro/CodedInputStreamMicro;->readString()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {p0, v2}, Lcom/google/android/finsky/protos/CheckPromoOffer$AddCreditCardPromoOffer;->setHeaderText(Ljava/lang/String;)Lcom/google/android/finsky/protos/CheckPromoOffer$AddCreditCardPromoOffer;

    goto :goto_0

    :sswitch_2
    invoke-virtual {p1}, Lcom/google/protobuf/micro/CodedInputStreamMicro;->readString()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {p0, v2}, Lcom/google/android/finsky/protos/CheckPromoOffer$AddCreditCardPromoOffer;->setDescriptionHtml(Ljava/lang/String;)Lcom/google/android/finsky/protos/CheckPromoOffer$AddCreditCardPromoOffer;

    goto :goto_0

    :sswitch_3
    new-instance v1, Lcom/google/android/finsky/protos/Doc$Image;

    invoke-direct {v1}, Lcom/google/android/finsky/protos/Doc$Image;-><init>()V

    invoke-virtual {p1, v1}, Lcom/google/protobuf/micro/CodedInputStreamMicro;->readMessage(Lcom/google/protobuf/micro/MessageMicro;)V

    invoke-virtual {p0, v1}, Lcom/google/android/finsky/protos/CheckPromoOffer$AddCreditCardPromoOffer;->setImage(Lcom/google/android/finsky/protos/Doc$Image;)Lcom/google/android/finsky/protos/CheckPromoOffer$AddCreditCardPromoOffer;

    goto :goto_0

    :sswitch_4
    invoke-virtual {p1}, Lcom/google/protobuf/micro/CodedInputStreamMicro;->readString()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {p0, v2}, Lcom/google/android/finsky/protos/CheckPromoOffer$AddCreditCardPromoOffer;->setIntroductoryTextHtml(Ljava/lang/String;)Lcom/google/android/finsky/protos/CheckPromoOffer$AddCreditCardPromoOffer;

    goto :goto_0

    :sswitch_5
    invoke-virtual {p1}, Lcom/google/protobuf/micro/CodedInputStreamMicro;->readString()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {p0, v2}, Lcom/google/android/finsky/protos/CheckPromoOffer$AddCreditCardPromoOffer;->setOfferTitle(Ljava/lang/String;)Lcom/google/android/finsky/protos/CheckPromoOffer$AddCreditCardPromoOffer;

    goto :goto_0

    :sswitch_6
    invoke-virtual {p1}, Lcom/google/protobuf/micro/CodedInputStreamMicro;->readString()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {p0, v2}, Lcom/google/android/finsky/protos/CheckPromoOffer$AddCreditCardPromoOffer;->setNoActionDescription(Ljava/lang/String;)Lcom/google/android/finsky/protos/CheckPromoOffer$AddCreditCardPromoOffer;

    goto :goto_0

    :sswitch_7
    invoke-virtual {p1}, Lcom/google/protobuf/micro/CodedInputStreamMicro;->readString()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {p0, v2}, Lcom/google/android/finsky/protos/CheckPromoOffer$AddCreditCardPromoOffer;->setTermsAndConditionsHtml(Ljava/lang/String;)Lcom/google/android/finsky/protos/CheckPromoOffer$AddCreditCardPromoOffer;

    goto :goto_0

    :sswitch_data_0
    .sparse-switch
        0x0 -> :sswitch_0
        0xa -> :sswitch_1
        0x12 -> :sswitch_2
        0x1a -> :sswitch_3
        0x22 -> :sswitch_4
        0x2a -> :sswitch_5
        0x32 -> :sswitch_6
        0x3a -> :sswitch_7
    .end sparse-switch
.end method

.method public bridge synthetic mergeFrom(Lcom/google/protobuf/micro/CodedInputStreamMicro;)Lcom/google/protobuf/micro/MessageMicro;
    .locals 1
    .param p1    # Lcom/google/protobuf/micro/CodedInputStreamMicro;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    invoke-virtual {p0, p1}, Lcom/google/android/finsky/protos/CheckPromoOffer$AddCreditCardPromoOffer;->mergeFrom(Lcom/google/protobuf/micro/CodedInputStreamMicro;)Lcom/google/android/finsky/protos/CheckPromoOffer$AddCreditCardPromoOffer;

    move-result-object v0

    return-object v0
.end method

.method public setDescriptionHtml(Ljava/lang/String;)Lcom/google/android/finsky/protos/CheckPromoOffer$AddCreditCardPromoOffer;
    .locals 1
    .param p1    # Ljava/lang/String;

    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/google/android/finsky/protos/CheckPromoOffer$AddCreditCardPromoOffer;->hasDescriptionHtml:Z

    iput-object p1, p0, Lcom/google/android/finsky/protos/CheckPromoOffer$AddCreditCardPromoOffer;->descriptionHtml_:Ljava/lang/String;

    return-object p0
.end method

.method public setHeaderText(Ljava/lang/String;)Lcom/google/android/finsky/protos/CheckPromoOffer$AddCreditCardPromoOffer;
    .locals 1
    .param p1    # Ljava/lang/String;

    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/google/android/finsky/protos/CheckPromoOffer$AddCreditCardPromoOffer;->hasHeaderText:Z

    iput-object p1, p0, Lcom/google/android/finsky/protos/CheckPromoOffer$AddCreditCardPromoOffer;->headerText_:Ljava/lang/String;

    return-object p0
.end method

.method public setImage(Lcom/google/android/finsky/protos/Doc$Image;)Lcom/google/android/finsky/protos/CheckPromoOffer$AddCreditCardPromoOffer;
    .locals 1
    .param p1    # Lcom/google/android/finsky/protos/Doc$Image;

    if-nez p1, :cond_0

    new-instance v0, Ljava/lang/NullPointerException;

    invoke-direct {v0}, Ljava/lang/NullPointerException;-><init>()V

    throw v0

    :cond_0
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/google/android/finsky/protos/CheckPromoOffer$AddCreditCardPromoOffer;->hasImage:Z

    iput-object p1, p0, Lcom/google/android/finsky/protos/CheckPromoOffer$AddCreditCardPromoOffer;->image_:Lcom/google/android/finsky/protos/Doc$Image;

    return-object p0
.end method

.method public setIntroductoryTextHtml(Ljava/lang/String;)Lcom/google/android/finsky/protos/CheckPromoOffer$AddCreditCardPromoOffer;
    .locals 1
    .param p1    # Ljava/lang/String;

    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/google/android/finsky/protos/CheckPromoOffer$AddCreditCardPromoOffer;->hasIntroductoryTextHtml:Z

    iput-object p1, p0, Lcom/google/android/finsky/protos/CheckPromoOffer$AddCreditCardPromoOffer;->introductoryTextHtml_:Ljava/lang/String;

    return-object p0
.end method

.method public setNoActionDescription(Ljava/lang/String;)Lcom/google/android/finsky/protos/CheckPromoOffer$AddCreditCardPromoOffer;
    .locals 1
    .param p1    # Ljava/lang/String;

    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/google/android/finsky/protos/CheckPromoOffer$AddCreditCardPromoOffer;->hasNoActionDescription:Z

    iput-object p1, p0, Lcom/google/android/finsky/protos/CheckPromoOffer$AddCreditCardPromoOffer;->noActionDescription_:Ljava/lang/String;

    return-object p0
.end method

.method public setOfferTitle(Ljava/lang/String;)Lcom/google/android/finsky/protos/CheckPromoOffer$AddCreditCardPromoOffer;
    .locals 1
    .param p1    # Ljava/lang/String;

    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/google/android/finsky/protos/CheckPromoOffer$AddCreditCardPromoOffer;->hasOfferTitle:Z

    iput-object p1, p0, Lcom/google/android/finsky/protos/CheckPromoOffer$AddCreditCardPromoOffer;->offerTitle_:Ljava/lang/String;

    return-object p0
.end method

.method public setTermsAndConditionsHtml(Ljava/lang/String;)Lcom/google/android/finsky/protos/CheckPromoOffer$AddCreditCardPromoOffer;
    .locals 1
    .param p1    # Ljava/lang/String;

    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/google/android/finsky/protos/CheckPromoOffer$AddCreditCardPromoOffer;->hasTermsAndConditionsHtml:Z

    iput-object p1, p0, Lcom/google/android/finsky/protos/CheckPromoOffer$AddCreditCardPromoOffer;->termsAndConditionsHtml_:Ljava/lang/String;

    return-object p0
.end method

.method public writeTo(Lcom/google/protobuf/micro/CodedOutputStreamMicro;)V
    .locals 2
    .param p1    # Lcom/google/protobuf/micro/CodedOutputStreamMicro;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    invoke-virtual {p0}, Lcom/google/android/finsky/protos/CheckPromoOffer$AddCreditCardPromoOffer;->hasHeaderText()Z

    move-result v0

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    invoke-virtual {p0}, Lcom/google/android/finsky/protos/CheckPromoOffer$AddCreditCardPromoOffer;->getHeaderText()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {p1, v0, v1}, Lcom/google/protobuf/micro/CodedOutputStreamMicro;->writeString(ILjava/lang/String;)V

    :cond_0
    invoke-virtual {p0}, Lcom/google/android/finsky/protos/CheckPromoOffer$AddCreditCardPromoOffer;->hasDescriptionHtml()Z

    move-result v0

    if-eqz v0, :cond_1

    const/4 v0, 0x2

    invoke-virtual {p0}, Lcom/google/android/finsky/protos/CheckPromoOffer$AddCreditCardPromoOffer;->getDescriptionHtml()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {p1, v0, v1}, Lcom/google/protobuf/micro/CodedOutputStreamMicro;->writeString(ILjava/lang/String;)V

    :cond_1
    invoke-virtual {p0}, Lcom/google/android/finsky/protos/CheckPromoOffer$AddCreditCardPromoOffer;->hasImage()Z

    move-result v0

    if-eqz v0, :cond_2

    const/4 v0, 0x3

    invoke-virtual {p0}, Lcom/google/android/finsky/protos/CheckPromoOffer$AddCreditCardPromoOffer;->getImage()Lcom/google/android/finsky/protos/Doc$Image;

    move-result-object v1

    invoke-virtual {p1, v0, v1}, Lcom/google/protobuf/micro/CodedOutputStreamMicro;->writeMessage(ILcom/google/protobuf/micro/MessageMicro;)V

    :cond_2
    invoke-virtual {p0}, Lcom/google/android/finsky/protos/CheckPromoOffer$AddCreditCardPromoOffer;->hasIntroductoryTextHtml()Z

    move-result v0

    if-eqz v0, :cond_3

    const/4 v0, 0x4

    invoke-virtual {p0}, Lcom/google/android/finsky/protos/CheckPromoOffer$AddCreditCardPromoOffer;->getIntroductoryTextHtml()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {p1, v0, v1}, Lcom/google/protobuf/micro/CodedOutputStreamMicro;->writeString(ILjava/lang/String;)V

    :cond_3
    invoke-virtual {p0}, Lcom/google/android/finsky/protos/CheckPromoOffer$AddCreditCardPromoOffer;->hasOfferTitle()Z

    move-result v0

    if-eqz v0, :cond_4

    const/4 v0, 0x5

    invoke-virtual {p0}, Lcom/google/android/finsky/protos/CheckPromoOffer$AddCreditCardPromoOffer;->getOfferTitle()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {p1, v0, v1}, Lcom/google/protobuf/micro/CodedOutputStreamMicro;->writeString(ILjava/lang/String;)V

    :cond_4
    invoke-virtual {p0}, Lcom/google/android/finsky/protos/CheckPromoOffer$AddCreditCardPromoOffer;->hasNoActionDescription()Z

    move-result v0

    if-eqz v0, :cond_5

    const/4 v0, 0x6

    invoke-virtual {p0}, Lcom/google/android/finsky/protos/CheckPromoOffer$AddCreditCardPromoOffer;->getNoActionDescription()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {p1, v0, v1}, Lcom/google/protobuf/micro/CodedOutputStreamMicro;->writeString(ILjava/lang/String;)V

    :cond_5
    invoke-virtual {p0}, Lcom/google/android/finsky/protos/CheckPromoOffer$AddCreditCardPromoOffer;->hasTermsAndConditionsHtml()Z

    move-result v0

    if-eqz v0, :cond_6

    const/4 v0, 0x7

    invoke-virtual {p0}, Lcom/google/android/finsky/protos/CheckPromoOffer$AddCreditCardPromoOffer;->getTermsAndConditionsHtml()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {p1, v0, v1}, Lcom/google/protobuf/micro/CodedOutputStreamMicro;->writeString(ILjava/lang/String;)V

    :cond_6
    return-void
.end method
