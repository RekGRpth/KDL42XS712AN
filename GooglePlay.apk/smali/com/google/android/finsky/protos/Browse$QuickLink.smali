.class public final Lcom/google/android/finsky/protos/Browse$QuickLink;
.super Lcom/google/protobuf/micro/MessageMicro;
.source "Browse.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/google/android/finsky/protos/Browse;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x19
    name = "QuickLink"
.end annotation


# instance fields
.field private backendId_:I

.field private cachedSize:I

.field private displayRequired_:Z

.field private hasBackendId:Z

.field private hasDisplayRequired:Z

.field private hasImage:Z

.field private hasLink:Z

.field private hasName:Z

.field private hasPrismStyle:Z

.field private hasServerLogsCookie:Z

.field private image_:Lcom/google/android/finsky/protos/Doc$Image;

.field private link_:Lcom/google/android/finsky/protos/ResolveLink$ResolvedLink;

.field private name_:Ljava/lang/String;

.field private prismStyle_:Z

.field private serverLogsCookie_:Lcom/google/protobuf/micro/ByteStringMicro;


# direct methods
.method public constructor <init>()V
    .locals 3

    const/4 v2, 0x0

    const/4 v1, 0x0

    invoke-direct {p0}, Lcom/google/protobuf/micro/MessageMicro;-><init>()V

    const-string v0, ""

    iput-object v0, p0, Lcom/google/android/finsky/protos/Browse$QuickLink;->name_:Ljava/lang/String;

    iput-object v2, p0, Lcom/google/android/finsky/protos/Browse$QuickLink;->image_:Lcom/google/android/finsky/protos/Doc$Image;

    iput-object v2, p0, Lcom/google/android/finsky/protos/Browse$QuickLink;->link_:Lcom/google/android/finsky/protos/ResolveLink$ResolvedLink;

    iput-boolean v1, p0, Lcom/google/android/finsky/protos/Browse$QuickLink;->displayRequired_:Z

    iput v1, p0, Lcom/google/android/finsky/protos/Browse$QuickLink;->backendId_:I

    iput-boolean v1, p0, Lcom/google/android/finsky/protos/Browse$QuickLink;->prismStyle_:Z

    sget-object v0, Lcom/google/protobuf/micro/ByteStringMicro;->EMPTY:Lcom/google/protobuf/micro/ByteStringMicro;

    iput-object v0, p0, Lcom/google/android/finsky/protos/Browse$QuickLink;->serverLogsCookie_:Lcom/google/protobuf/micro/ByteStringMicro;

    const/4 v0, -0x1

    iput v0, p0, Lcom/google/android/finsky/protos/Browse$QuickLink;->cachedSize:I

    return-void
.end method


# virtual methods
.method public getBackendId()I
    .locals 1

    iget v0, p0, Lcom/google/android/finsky/protos/Browse$QuickLink;->backendId_:I

    return v0
.end method

.method public getCachedSize()I
    .locals 1

    iget v0, p0, Lcom/google/android/finsky/protos/Browse$QuickLink;->cachedSize:I

    if-gez v0, :cond_0

    invoke-virtual {p0}, Lcom/google/android/finsky/protos/Browse$QuickLink;->getSerializedSize()I

    :cond_0
    iget v0, p0, Lcom/google/android/finsky/protos/Browse$QuickLink;->cachedSize:I

    return v0
.end method

.method public getDisplayRequired()Z
    .locals 1

    iget-boolean v0, p0, Lcom/google/android/finsky/protos/Browse$QuickLink;->displayRequired_:Z

    return v0
.end method

.method public getImage()Lcom/google/android/finsky/protos/Doc$Image;
    .locals 1

    iget-object v0, p0, Lcom/google/android/finsky/protos/Browse$QuickLink;->image_:Lcom/google/android/finsky/protos/Doc$Image;

    return-object v0
.end method

.method public getLink()Lcom/google/android/finsky/protos/ResolveLink$ResolvedLink;
    .locals 1

    iget-object v0, p0, Lcom/google/android/finsky/protos/Browse$QuickLink;->link_:Lcom/google/android/finsky/protos/ResolveLink$ResolvedLink;

    return-object v0
.end method

.method public getName()Ljava/lang/String;
    .locals 1

    iget-object v0, p0, Lcom/google/android/finsky/protos/Browse$QuickLink;->name_:Ljava/lang/String;

    return-object v0
.end method

.method public getPrismStyle()Z
    .locals 1

    iget-boolean v0, p0, Lcom/google/android/finsky/protos/Browse$QuickLink;->prismStyle_:Z

    return v0
.end method

.method public getSerializedSize()I
    .locals 3

    const/4 v0, 0x0

    invoke-virtual {p0}, Lcom/google/android/finsky/protos/Browse$QuickLink;->hasName()Z

    move-result v1

    if-eqz v1, :cond_0

    const/4 v1, 0x1

    invoke-virtual {p0}, Lcom/google/android/finsky/protos/Browse$QuickLink;->getName()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2}, Lcom/google/protobuf/micro/CodedOutputStreamMicro;->computeStringSize(ILjava/lang/String;)I

    move-result v1

    add-int/2addr v0, v1

    :cond_0
    invoke-virtual {p0}, Lcom/google/android/finsky/protos/Browse$QuickLink;->hasImage()Z

    move-result v1

    if-eqz v1, :cond_1

    const/4 v1, 0x2

    invoke-virtual {p0}, Lcom/google/android/finsky/protos/Browse$QuickLink;->getImage()Lcom/google/android/finsky/protos/Doc$Image;

    move-result-object v2

    invoke-static {v1, v2}, Lcom/google/protobuf/micro/CodedOutputStreamMicro;->computeMessageSize(ILcom/google/protobuf/micro/MessageMicro;)I

    move-result v1

    add-int/2addr v0, v1

    :cond_1
    invoke-virtual {p0}, Lcom/google/android/finsky/protos/Browse$QuickLink;->hasLink()Z

    move-result v1

    if-eqz v1, :cond_2

    const/4 v1, 0x3

    invoke-virtual {p0}, Lcom/google/android/finsky/protos/Browse$QuickLink;->getLink()Lcom/google/android/finsky/protos/ResolveLink$ResolvedLink;

    move-result-object v2

    invoke-static {v1, v2}, Lcom/google/protobuf/micro/CodedOutputStreamMicro;->computeMessageSize(ILcom/google/protobuf/micro/MessageMicro;)I

    move-result v1

    add-int/2addr v0, v1

    :cond_2
    invoke-virtual {p0}, Lcom/google/android/finsky/protos/Browse$QuickLink;->hasDisplayRequired()Z

    move-result v1

    if-eqz v1, :cond_3

    const/4 v1, 0x4

    invoke-virtual {p0}, Lcom/google/android/finsky/protos/Browse$QuickLink;->getDisplayRequired()Z

    move-result v2

    invoke-static {v1, v2}, Lcom/google/protobuf/micro/CodedOutputStreamMicro;->computeBoolSize(IZ)I

    move-result v1

    add-int/2addr v0, v1

    :cond_3
    invoke-virtual {p0}, Lcom/google/android/finsky/protos/Browse$QuickLink;->hasServerLogsCookie()Z

    move-result v1

    if-eqz v1, :cond_4

    const/4 v1, 0x5

    invoke-virtual {p0}, Lcom/google/android/finsky/protos/Browse$QuickLink;->getServerLogsCookie()Lcom/google/protobuf/micro/ByteStringMicro;

    move-result-object v2

    invoke-static {v1, v2}, Lcom/google/protobuf/micro/CodedOutputStreamMicro;->computeBytesSize(ILcom/google/protobuf/micro/ByteStringMicro;)I

    move-result v1

    add-int/2addr v0, v1

    :cond_4
    invoke-virtual {p0}, Lcom/google/android/finsky/protos/Browse$QuickLink;->hasBackendId()Z

    move-result v1

    if-eqz v1, :cond_5

    const/4 v1, 0x6

    invoke-virtual {p0}, Lcom/google/android/finsky/protos/Browse$QuickLink;->getBackendId()I

    move-result v2

    invoke-static {v1, v2}, Lcom/google/protobuf/micro/CodedOutputStreamMicro;->computeInt32Size(II)I

    move-result v1

    add-int/2addr v0, v1

    :cond_5
    invoke-virtual {p0}, Lcom/google/android/finsky/protos/Browse$QuickLink;->hasPrismStyle()Z

    move-result v1

    if-eqz v1, :cond_6

    const/4 v1, 0x7

    invoke-virtual {p0}, Lcom/google/android/finsky/protos/Browse$QuickLink;->getPrismStyle()Z

    move-result v2

    invoke-static {v1, v2}, Lcom/google/protobuf/micro/CodedOutputStreamMicro;->computeBoolSize(IZ)I

    move-result v1

    add-int/2addr v0, v1

    :cond_6
    iput v0, p0, Lcom/google/android/finsky/protos/Browse$QuickLink;->cachedSize:I

    return v0
.end method

.method public getServerLogsCookie()Lcom/google/protobuf/micro/ByteStringMicro;
    .locals 1

    iget-object v0, p0, Lcom/google/android/finsky/protos/Browse$QuickLink;->serverLogsCookie_:Lcom/google/protobuf/micro/ByteStringMicro;

    return-object v0
.end method

.method public hasBackendId()Z
    .locals 1

    iget-boolean v0, p0, Lcom/google/android/finsky/protos/Browse$QuickLink;->hasBackendId:Z

    return v0
.end method

.method public hasDisplayRequired()Z
    .locals 1

    iget-boolean v0, p0, Lcom/google/android/finsky/protos/Browse$QuickLink;->hasDisplayRequired:Z

    return v0
.end method

.method public hasImage()Z
    .locals 1

    iget-boolean v0, p0, Lcom/google/android/finsky/protos/Browse$QuickLink;->hasImage:Z

    return v0
.end method

.method public hasLink()Z
    .locals 1

    iget-boolean v0, p0, Lcom/google/android/finsky/protos/Browse$QuickLink;->hasLink:Z

    return v0
.end method

.method public hasName()Z
    .locals 1

    iget-boolean v0, p0, Lcom/google/android/finsky/protos/Browse$QuickLink;->hasName:Z

    return v0
.end method

.method public hasPrismStyle()Z
    .locals 1

    iget-boolean v0, p0, Lcom/google/android/finsky/protos/Browse$QuickLink;->hasPrismStyle:Z

    return v0
.end method

.method public hasServerLogsCookie()Z
    .locals 1

    iget-boolean v0, p0, Lcom/google/android/finsky/protos/Browse$QuickLink;->hasServerLogsCookie:Z

    return v0
.end method

.method public mergeFrom(Lcom/google/protobuf/micro/CodedInputStreamMicro;)Lcom/google/android/finsky/protos/Browse$QuickLink;
    .locals 3
    .param p1    # Lcom/google/protobuf/micro/CodedInputStreamMicro;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    :cond_0
    :goto_0
    invoke-virtual {p1}, Lcom/google/protobuf/micro/CodedInputStreamMicro;->readTag()I

    move-result v0

    sparse-switch v0, :sswitch_data_0

    invoke-virtual {p0, p1, v0}, Lcom/google/android/finsky/protos/Browse$QuickLink;->parseUnknownField(Lcom/google/protobuf/micro/CodedInputStreamMicro;I)Z

    move-result v2

    if-nez v2, :cond_0

    :sswitch_0
    return-object p0

    :sswitch_1
    invoke-virtual {p1}, Lcom/google/protobuf/micro/CodedInputStreamMicro;->readString()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {p0, v2}, Lcom/google/android/finsky/protos/Browse$QuickLink;->setName(Ljava/lang/String;)Lcom/google/android/finsky/protos/Browse$QuickLink;

    goto :goto_0

    :sswitch_2
    new-instance v1, Lcom/google/android/finsky/protos/Doc$Image;

    invoke-direct {v1}, Lcom/google/android/finsky/protos/Doc$Image;-><init>()V

    invoke-virtual {p1, v1}, Lcom/google/protobuf/micro/CodedInputStreamMicro;->readMessage(Lcom/google/protobuf/micro/MessageMicro;)V

    invoke-virtual {p0, v1}, Lcom/google/android/finsky/protos/Browse$QuickLink;->setImage(Lcom/google/android/finsky/protos/Doc$Image;)Lcom/google/android/finsky/protos/Browse$QuickLink;

    goto :goto_0

    :sswitch_3
    new-instance v1, Lcom/google/android/finsky/protos/ResolveLink$ResolvedLink;

    invoke-direct {v1}, Lcom/google/android/finsky/protos/ResolveLink$ResolvedLink;-><init>()V

    invoke-virtual {p1, v1}, Lcom/google/protobuf/micro/CodedInputStreamMicro;->readMessage(Lcom/google/protobuf/micro/MessageMicro;)V

    invoke-virtual {p0, v1}, Lcom/google/android/finsky/protos/Browse$QuickLink;->setLink(Lcom/google/android/finsky/protos/ResolveLink$ResolvedLink;)Lcom/google/android/finsky/protos/Browse$QuickLink;

    goto :goto_0

    :sswitch_4
    invoke-virtual {p1}, Lcom/google/protobuf/micro/CodedInputStreamMicro;->readBool()Z

    move-result v2

    invoke-virtual {p0, v2}, Lcom/google/android/finsky/protos/Browse$QuickLink;->setDisplayRequired(Z)Lcom/google/android/finsky/protos/Browse$QuickLink;

    goto :goto_0

    :sswitch_5
    invoke-virtual {p1}, Lcom/google/protobuf/micro/CodedInputStreamMicro;->readBytes()Lcom/google/protobuf/micro/ByteStringMicro;

    move-result-object v2

    invoke-virtual {p0, v2}, Lcom/google/android/finsky/protos/Browse$QuickLink;->setServerLogsCookie(Lcom/google/protobuf/micro/ByteStringMicro;)Lcom/google/android/finsky/protos/Browse$QuickLink;

    goto :goto_0

    :sswitch_6
    invoke-virtual {p1}, Lcom/google/protobuf/micro/CodedInputStreamMicro;->readInt32()I

    move-result v2

    invoke-virtual {p0, v2}, Lcom/google/android/finsky/protos/Browse$QuickLink;->setBackendId(I)Lcom/google/android/finsky/protos/Browse$QuickLink;

    goto :goto_0

    :sswitch_7
    invoke-virtual {p1}, Lcom/google/protobuf/micro/CodedInputStreamMicro;->readBool()Z

    move-result v2

    invoke-virtual {p0, v2}, Lcom/google/android/finsky/protos/Browse$QuickLink;->setPrismStyle(Z)Lcom/google/android/finsky/protos/Browse$QuickLink;

    goto :goto_0

    :sswitch_data_0
    .sparse-switch
        0x0 -> :sswitch_0
        0xa -> :sswitch_1
        0x12 -> :sswitch_2
        0x1a -> :sswitch_3
        0x20 -> :sswitch_4
        0x2a -> :sswitch_5
        0x30 -> :sswitch_6
        0x38 -> :sswitch_7
    .end sparse-switch
.end method

.method public bridge synthetic mergeFrom(Lcom/google/protobuf/micro/CodedInputStreamMicro;)Lcom/google/protobuf/micro/MessageMicro;
    .locals 1
    .param p1    # Lcom/google/protobuf/micro/CodedInputStreamMicro;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    invoke-virtual {p0, p1}, Lcom/google/android/finsky/protos/Browse$QuickLink;->mergeFrom(Lcom/google/protobuf/micro/CodedInputStreamMicro;)Lcom/google/android/finsky/protos/Browse$QuickLink;

    move-result-object v0

    return-object v0
.end method

.method public setBackendId(I)Lcom/google/android/finsky/protos/Browse$QuickLink;
    .locals 1
    .param p1    # I

    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/google/android/finsky/protos/Browse$QuickLink;->hasBackendId:Z

    iput p1, p0, Lcom/google/android/finsky/protos/Browse$QuickLink;->backendId_:I

    return-object p0
.end method

.method public setDisplayRequired(Z)Lcom/google/android/finsky/protos/Browse$QuickLink;
    .locals 1
    .param p1    # Z

    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/google/android/finsky/protos/Browse$QuickLink;->hasDisplayRequired:Z

    iput-boolean p1, p0, Lcom/google/android/finsky/protos/Browse$QuickLink;->displayRequired_:Z

    return-object p0
.end method

.method public setImage(Lcom/google/android/finsky/protos/Doc$Image;)Lcom/google/android/finsky/protos/Browse$QuickLink;
    .locals 1
    .param p1    # Lcom/google/android/finsky/protos/Doc$Image;

    if-nez p1, :cond_0

    new-instance v0, Ljava/lang/NullPointerException;

    invoke-direct {v0}, Ljava/lang/NullPointerException;-><init>()V

    throw v0

    :cond_0
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/google/android/finsky/protos/Browse$QuickLink;->hasImage:Z

    iput-object p1, p0, Lcom/google/android/finsky/protos/Browse$QuickLink;->image_:Lcom/google/android/finsky/protos/Doc$Image;

    return-object p0
.end method

.method public setLink(Lcom/google/android/finsky/protos/ResolveLink$ResolvedLink;)Lcom/google/android/finsky/protos/Browse$QuickLink;
    .locals 1
    .param p1    # Lcom/google/android/finsky/protos/ResolveLink$ResolvedLink;

    if-nez p1, :cond_0

    new-instance v0, Ljava/lang/NullPointerException;

    invoke-direct {v0}, Ljava/lang/NullPointerException;-><init>()V

    throw v0

    :cond_0
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/google/android/finsky/protos/Browse$QuickLink;->hasLink:Z

    iput-object p1, p0, Lcom/google/android/finsky/protos/Browse$QuickLink;->link_:Lcom/google/android/finsky/protos/ResolveLink$ResolvedLink;

    return-object p0
.end method

.method public setName(Ljava/lang/String;)Lcom/google/android/finsky/protos/Browse$QuickLink;
    .locals 1
    .param p1    # Ljava/lang/String;

    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/google/android/finsky/protos/Browse$QuickLink;->hasName:Z

    iput-object p1, p0, Lcom/google/android/finsky/protos/Browse$QuickLink;->name_:Ljava/lang/String;

    return-object p0
.end method

.method public setPrismStyle(Z)Lcom/google/android/finsky/protos/Browse$QuickLink;
    .locals 1
    .param p1    # Z

    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/google/android/finsky/protos/Browse$QuickLink;->hasPrismStyle:Z

    iput-boolean p1, p0, Lcom/google/android/finsky/protos/Browse$QuickLink;->prismStyle_:Z

    return-object p0
.end method

.method public setServerLogsCookie(Lcom/google/protobuf/micro/ByteStringMicro;)Lcom/google/android/finsky/protos/Browse$QuickLink;
    .locals 1
    .param p1    # Lcom/google/protobuf/micro/ByteStringMicro;

    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/google/android/finsky/protos/Browse$QuickLink;->hasServerLogsCookie:Z

    iput-object p1, p0, Lcom/google/android/finsky/protos/Browse$QuickLink;->serverLogsCookie_:Lcom/google/protobuf/micro/ByteStringMicro;

    return-object p0
.end method

.method public writeTo(Lcom/google/protobuf/micro/CodedOutputStreamMicro;)V
    .locals 2
    .param p1    # Lcom/google/protobuf/micro/CodedOutputStreamMicro;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    invoke-virtual {p0}, Lcom/google/android/finsky/protos/Browse$QuickLink;->hasName()Z

    move-result v0

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    invoke-virtual {p0}, Lcom/google/android/finsky/protos/Browse$QuickLink;->getName()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {p1, v0, v1}, Lcom/google/protobuf/micro/CodedOutputStreamMicro;->writeString(ILjava/lang/String;)V

    :cond_0
    invoke-virtual {p0}, Lcom/google/android/finsky/protos/Browse$QuickLink;->hasImage()Z

    move-result v0

    if-eqz v0, :cond_1

    const/4 v0, 0x2

    invoke-virtual {p0}, Lcom/google/android/finsky/protos/Browse$QuickLink;->getImage()Lcom/google/android/finsky/protos/Doc$Image;

    move-result-object v1

    invoke-virtual {p1, v0, v1}, Lcom/google/protobuf/micro/CodedOutputStreamMicro;->writeMessage(ILcom/google/protobuf/micro/MessageMicro;)V

    :cond_1
    invoke-virtual {p0}, Lcom/google/android/finsky/protos/Browse$QuickLink;->hasLink()Z

    move-result v0

    if-eqz v0, :cond_2

    const/4 v0, 0x3

    invoke-virtual {p0}, Lcom/google/android/finsky/protos/Browse$QuickLink;->getLink()Lcom/google/android/finsky/protos/ResolveLink$ResolvedLink;

    move-result-object v1

    invoke-virtual {p1, v0, v1}, Lcom/google/protobuf/micro/CodedOutputStreamMicro;->writeMessage(ILcom/google/protobuf/micro/MessageMicro;)V

    :cond_2
    invoke-virtual {p0}, Lcom/google/android/finsky/protos/Browse$QuickLink;->hasDisplayRequired()Z

    move-result v0

    if-eqz v0, :cond_3

    const/4 v0, 0x4

    invoke-virtual {p0}, Lcom/google/android/finsky/protos/Browse$QuickLink;->getDisplayRequired()Z

    move-result v1

    invoke-virtual {p1, v0, v1}, Lcom/google/protobuf/micro/CodedOutputStreamMicro;->writeBool(IZ)V

    :cond_3
    invoke-virtual {p0}, Lcom/google/android/finsky/protos/Browse$QuickLink;->hasServerLogsCookie()Z

    move-result v0

    if-eqz v0, :cond_4

    const/4 v0, 0x5

    invoke-virtual {p0}, Lcom/google/android/finsky/protos/Browse$QuickLink;->getServerLogsCookie()Lcom/google/protobuf/micro/ByteStringMicro;

    move-result-object v1

    invoke-virtual {p1, v0, v1}, Lcom/google/protobuf/micro/CodedOutputStreamMicro;->writeBytes(ILcom/google/protobuf/micro/ByteStringMicro;)V

    :cond_4
    invoke-virtual {p0}, Lcom/google/android/finsky/protos/Browse$QuickLink;->hasBackendId()Z

    move-result v0

    if-eqz v0, :cond_5

    const/4 v0, 0x6

    invoke-virtual {p0}, Lcom/google/android/finsky/protos/Browse$QuickLink;->getBackendId()I

    move-result v1

    invoke-virtual {p1, v0, v1}, Lcom/google/protobuf/micro/CodedOutputStreamMicro;->writeInt32(II)V

    :cond_5
    invoke-virtual {p0}, Lcom/google/android/finsky/protos/Browse$QuickLink;->hasPrismStyle()Z

    move-result v0

    if-eqz v0, :cond_6

    const/4 v0, 0x7

    invoke-virtual {p0}, Lcom/google/android/finsky/protos/Browse$QuickLink;->getPrismStyle()Z

    move-result v1

    invoke-virtual {p1, v0, v1}, Lcom/google/protobuf/micro/CodedOutputStreamMicro;->writeBool(IZ)V

    :cond_6
    return-void
.end method
