.class public final Lcom/google/android/finsky/protos/Rev$Review;
.super Lcom/google/protobuf/micro/MessageMicro;
.source "Rev.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/google/android/finsky/protos/Rev;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x19
    name = "Review"
.end annotation


# instance fields
.field private authorName_:Ljava/lang/String;

.field private cachedSize:I

.field private commentId_:Ljava/lang/String;

.field private comment_:Ljava/lang/String;

.field private deviceName_:Ljava/lang/String;

.field private documentVersion_:Ljava/lang/String;

.field private hasAuthorName:Z

.field private hasComment:Z

.field private hasCommentId:Z

.field private hasDeviceName:Z

.field private hasDocumentVersion:Z

.field private hasPlusProfile:Z

.field private hasReplyText:Z

.field private hasReplyTimestampMsec:Z

.field private hasSource:Z

.field private hasStarRating:Z

.field private hasTimestampMsec:Z

.field private hasTitle:Z

.field private hasUrl:Z

.field private plusProfile_:Lcom/google/android/finsky/protos/PlusData$PlusProfile;

.field private replyText_:Ljava/lang/String;

.field private replyTimestampMsec_:J

.field private source_:Ljava/lang/String;

.field private starRating_:I

.field private timestampMsec_:J

.field private title_:Ljava/lang/String;

.field private url_:Ljava/lang/String;


# direct methods
.method public constructor <init>()V
    .locals 3

    const-wide/16 v1, 0x0

    invoke-direct {p0}, Lcom/google/protobuf/micro/MessageMicro;-><init>()V

    const-string v0, ""

    iput-object v0, p0, Lcom/google/android/finsky/protos/Rev$Review;->authorName_:Ljava/lang/String;

    const-string v0, ""

    iput-object v0, p0, Lcom/google/android/finsky/protos/Rev$Review;->url_:Ljava/lang/String;

    const-string v0, ""

    iput-object v0, p0, Lcom/google/android/finsky/protos/Rev$Review;->source_:Ljava/lang/String;

    const-string v0, ""

    iput-object v0, p0, Lcom/google/android/finsky/protos/Rev$Review;->documentVersion_:Ljava/lang/String;

    iput-wide v1, p0, Lcom/google/android/finsky/protos/Rev$Review;->timestampMsec_:J

    const/4 v0, 0x0

    iput v0, p0, Lcom/google/android/finsky/protos/Rev$Review;->starRating_:I

    const-string v0, ""

    iput-object v0, p0, Lcom/google/android/finsky/protos/Rev$Review;->title_:Ljava/lang/String;

    const-string v0, ""

    iput-object v0, p0, Lcom/google/android/finsky/protos/Rev$Review;->comment_:Ljava/lang/String;

    const-string v0, ""

    iput-object v0, p0, Lcom/google/android/finsky/protos/Rev$Review;->commentId_:Ljava/lang/String;

    const-string v0, ""

    iput-object v0, p0, Lcom/google/android/finsky/protos/Rev$Review;->deviceName_:Ljava/lang/String;

    const-string v0, ""

    iput-object v0, p0, Lcom/google/android/finsky/protos/Rev$Review;->replyText_:Ljava/lang/String;

    iput-wide v1, p0, Lcom/google/android/finsky/protos/Rev$Review;->replyTimestampMsec_:J

    const/4 v0, 0x0

    iput-object v0, p0, Lcom/google/android/finsky/protos/Rev$Review;->plusProfile_:Lcom/google/android/finsky/protos/PlusData$PlusProfile;

    const/4 v0, -0x1

    iput v0, p0, Lcom/google/android/finsky/protos/Rev$Review;->cachedSize:I

    return-void
.end method


# virtual methods
.method public getAuthorName()Ljava/lang/String;
    .locals 1

    iget-object v0, p0, Lcom/google/android/finsky/protos/Rev$Review;->authorName_:Ljava/lang/String;

    return-object v0
.end method

.method public getCachedSize()I
    .locals 1

    iget v0, p0, Lcom/google/android/finsky/protos/Rev$Review;->cachedSize:I

    if-gez v0, :cond_0

    invoke-virtual {p0}, Lcom/google/android/finsky/protos/Rev$Review;->getSerializedSize()I

    :cond_0
    iget v0, p0, Lcom/google/android/finsky/protos/Rev$Review;->cachedSize:I

    return v0
.end method

.method public getComment()Ljava/lang/String;
    .locals 1

    iget-object v0, p0, Lcom/google/android/finsky/protos/Rev$Review;->comment_:Ljava/lang/String;

    return-object v0
.end method

.method public getCommentId()Ljava/lang/String;
    .locals 1

    iget-object v0, p0, Lcom/google/android/finsky/protos/Rev$Review;->commentId_:Ljava/lang/String;

    return-object v0
.end method

.method public getDeviceName()Ljava/lang/String;
    .locals 1

    iget-object v0, p0, Lcom/google/android/finsky/protos/Rev$Review;->deviceName_:Ljava/lang/String;

    return-object v0
.end method

.method public getDocumentVersion()Ljava/lang/String;
    .locals 1

    iget-object v0, p0, Lcom/google/android/finsky/protos/Rev$Review;->documentVersion_:Ljava/lang/String;

    return-object v0
.end method

.method public getPlusProfile()Lcom/google/android/finsky/protos/PlusData$PlusProfile;
    .locals 1

    iget-object v0, p0, Lcom/google/android/finsky/protos/Rev$Review;->plusProfile_:Lcom/google/android/finsky/protos/PlusData$PlusProfile;

    return-object v0
.end method

.method public getReplyText()Ljava/lang/String;
    .locals 1

    iget-object v0, p0, Lcom/google/android/finsky/protos/Rev$Review;->replyText_:Ljava/lang/String;

    return-object v0
.end method

.method public getReplyTimestampMsec()J
    .locals 2

    iget-wide v0, p0, Lcom/google/android/finsky/protos/Rev$Review;->replyTimestampMsec_:J

    return-wide v0
.end method

.method public getSerializedSize()I
    .locals 4

    const/4 v0, 0x0

    invoke-virtual {p0}, Lcom/google/android/finsky/protos/Rev$Review;->hasAuthorName()Z

    move-result v1

    if-eqz v1, :cond_0

    const/4 v1, 0x1

    invoke-virtual {p0}, Lcom/google/android/finsky/protos/Rev$Review;->getAuthorName()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2}, Lcom/google/protobuf/micro/CodedOutputStreamMicro;->computeStringSize(ILjava/lang/String;)I

    move-result v1

    add-int/2addr v0, v1

    :cond_0
    invoke-virtual {p0}, Lcom/google/android/finsky/protos/Rev$Review;->hasUrl()Z

    move-result v1

    if-eqz v1, :cond_1

    const/4 v1, 0x2

    invoke-virtual {p0}, Lcom/google/android/finsky/protos/Rev$Review;->getUrl()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2}, Lcom/google/protobuf/micro/CodedOutputStreamMicro;->computeStringSize(ILjava/lang/String;)I

    move-result v1

    add-int/2addr v0, v1

    :cond_1
    invoke-virtual {p0}, Lcom/google/android/finsky/protos/Rev$Review;->hasSource()Z

    move-result v1

    if-eqz v1, :cond_2

    const/4 v1, 0x3

    invoke-virtual {p0}, Lcom/google/android/finsky/protos/Rev$Review;->getSource()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2}, Lcom/google/protobuf/micro/CodedOutputStreamMicro;->computeStringSize(ILjava/lang/String;)I

    move-result v1

    add-int/2addr v0, v1

    :cond_2
    invoke-virtual {p0}, Lcom/google/android/finsky/protos/Rev$Review;->hasDocumentVersion()Z

    move-result v1

    if-eqz v1, :cond_3

    const/4 v1, 0x4

    invoke-virtual {p0}, Lcom/google/android/finsky/protos/Rev$Review;->getDocumentVersion()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2}, Lcom/google/protobuf/micro/CodedOutputStreamMicro;->computeStringSize(ILjava/lang/String;)I

    move-result v1

    add-int/2addr v0, v1

    :cond_3
    invoke-virtual {p0}, Lcom/google/android/finsky/protos/Rev$Review;->hasTimestampMsec()Z

    move-result v1

    if-eqz v1, :cond_4

    const/4 v1, 0x5

    invoke-virtual {p0}, Lcom/google/android/finsky/protos/Rev$Review;->getTimestampMsec()J

    move-result-wide v2

    invoke-static {v1, v2, v3}, Lcom/google/protobuf/micro/CodedOutputStreamMicro;->computeInt64Size(IJ)I

    move-result v1

    add-int/2addr v0, v1

    :cond_4
    invoke-virtual {p0}, Lcom/google/android/finsky/protos/Rev$Review;->hasStarRating()Z

    move-result v1

    if-eqz v1, :cond_5

    const/4 v1, 0x6

    invoke-virtual {p0}, Lcom/google/android/finsky/protos/Rev$Review;->getStarRating()I

    move-result v2

    invoke-static {v1, v2}, Lcom/google/protobuf/micro/CodedOutputStreamMicro;->computeInt32Size(II)I

    move-result v1

    add-int/2addr v0, v1

    :cond_5
    invoke-virtual {p0}, Lcom/google/android/finsky/protos/Rev$Review;->hasTitle()Z

    move-result v1

    if-eqz v1, :cond_6

    const/4 v1, 0x7

    invoke-virtual {p0}, Lcom/google/android/finsky/protos/Rev$Review;->getTitle()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2}, Lcom/google/protobuf/micro/CodedOutputStreamMicro;->computeStringSize(ILjava/lang/String;)I

    move-result v1

    add-int/2addr v0, v1

    :cond_6
    invoke-virtual {p0}, Lcom/google/android/finsky/protos/Rev$Review;->hasComment()Z

    move-result v1

    if-eqz v1, :cond_7

    const/16 v1, 0x8

    invoke-virtual {p0}, Lcom/google/android/finsky/protos/Rev$Review;->getComment()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2}, Lcom/google/protobuf/micro/CodedOutputStreamMicro;->computeStringSize(ILjava/lang/String;)I

    move-result v1

    add-int/2addr v0, v1

    :cond_7
    invoke-virtual {p0}, Lcom/google/android/finsky/protos/Rev$Review;->hasCommentId()Z

    move-result v1

    if-eqz v1, :cond_8

    const/16 v1, 0x9

    invoke-virtual {p0}, Lcom/google/android/finsky/protos/Rev$Review;->getCommentId()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2}, Lcom/google/protobuf/micro/CodedOutputStreamMicro;->computeStringSize(ILjava/lang/String;)I

    move-result v1

    add-int/2addr v0, v1

    :cond_8
    invoke-virtual {p0}, Lcom/google/android/finsky/protos/Rev$Review;->hasDeviceName()Z

    move-result v1

    if-eqz v1, :cond_9

    const/16 v1, 0x13

    invoke-virtual {p0}, Lcom/google/android/finsky/protos/Rev$Review;->getDeviceName()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2}, Lcom/google/protobuf/micro/CodedOutputStreamMicro;->computeStringSize(ILjava/lang/String;)I

    move-result v1

    add-int/2addr v0, v1

    :cond_9
    invoke-virtual {p0}, Lcom/google/android/finsky/protos/Rev$Review;->hasReplyText()Z

    move-result v1

    if-eqz v1, :cond_a

    const/16 v1, 0x1d

    invoke-virtual {p0}, Lcom/google/android/finsky/protos/Rev$Review;->getReplyText()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2}, Lcom/google/protobuf/micro/CodedOutputStreamMicro;->computeStringSize(ILjava/lang/String;)I

    move-result v1

    add-int/2addr v0, v1

    :cond_a
    invoke-virtual {p0}, Lcom/google/android/finsky/protos/Rev$Review;->hasReplyTimestampMsec()Z

    move-result v1

    if-eqz v1, :cond_b

    const/16 v1, 0x1e

    invoke-virtual {p0}, Lcom/google/android/finsky/protos/Rev$Review;->getReplyTimestampMsec()J

    move-result-wide v2

    invoke-static {v1, v2, v3}, Lcom/google/protobuf/micro/CodedOutputStreamMicro;->computeInt64Size(IJ)I

    move-result v1

    add-int/2addr v0, v1

    :cond_b
    invoke-virtual {p0}, Lcom/google/android/finsky/protos/Rev$Review;->hasPlusProfile()Z

    move-result v1

    if-eqz v1, :cond_c

    const/16 v1, 0x1f

    invoke-virtual {p0}, Lcom/google/android/finsky/protos/Rev$Review;->getPlusProfile()Lcom/google/android/finsky/protos/PlusData$PlusProfile;

    move-result-object v2

    invoke-static {v1, v2}, Lcom/google/protobuf/micro/CodedOutputStreamMicro;->computeMessageSize(ILcom/google/protobuf/micro/MessageMicro;)I

    move-result v1

    add-int/2addr v0, v1

    :cond_c
    iput v0, p0, Lcom/google/android/finsky/protos/Rev$Review;->cachedSize:I

    return v0
.end method

.method public getSource()Ljava/lang/String;
    .locals 1

    iget-object v0, p0, Lcom/google/android/finsky/protos/Rev$Review;->source_:Ljava/lang/String;

    return-object v0
.end method

.method public getStarRating()I
    .locals 1

    iget v0, p0, Lcom/google/android/finsky/protos/Rev$Review;->starRating_:I

    return v0
.end method

.method public getTimestampMsec()J
    .locals 2

    iget-wide v0, p0, Lcom/google/android/finsky/protos/Rev$Review;->timestampMsec_:J

    return-wide v0
.end method

.method public getTitle()Ljava/lang/String;
    .locals 1

    iget-object v0, p0, Lcom/google/android/finsky/protos/Rev$Review;->title_:Ljava/lang/String;

    return-object v0
.end method

.method public getUrl()Ljava/lang/String;
    .locals 1

    iget-object v0, p0, Lcom/google/android/finsky/protos/Rev$Review;->url_:Ljava/lang/String;

    return-object v0
.end method

.method public hasAuthorName()Z
    .locals 1

    iget-boolean v0, p0, Lcom/google/android/finsky/protos/Rev$Review;->hasAuthorName:Z

    return v0
.end method

.method public hasComment()Z
    .locals 1

    iget-boolean v0, p0, Lcom/google/android/finsky/protos/Rev$Review;->hasComment:Z

    return v0
.end method

.method public hasCommentId()Z
    .locals 1

    iget-boolean v0, p0, Lcom/google/android/finsky/protos/Rev$Review;->hasCommentId:Z

    return v0
.end method

.method public hasDeviceName()Z
    .locals 1

    iget-boolean v0, p0, Lcom/google/android/finsky/protos/Rev$Review;->hasDeviceName:Z

    return v0
.end method

.method public hasDocumentVersion()Z
    .locals 1

    iget-boolean v0, p0, Lcom/google/android/finsky/protos/Rev$Review;->hasDocumentVersion:Z

    return v0
.end method

.method public hasPlusProfile()Z
    .locals 1

    iget-boolean v0, p0, Lcom/google/android/finsky/protos/Rev$Review;->hasPlusProfile:Z

    return v0
.end method

.method public hasReplyText()Z
    .locals 1

    iget-boolean v0, p0, Lcom/google/android/finsky/protos/Rev$Review;->hasReplyText:Z

    return v0
.end method

.method public hasReplyTimestampMsec()Z
    .locals 1

    iget-boolean v0, p0, Lcom/google/android/finsky/protos/Rev$Review;->hasReplyTimestampMsec:Z

    return v0
.end method

.method public hasSource()Z
    .locals 1

    iget-boolean v0, p0, Lcom/google/android/finsky/protos/Rev$Review;->hasSource:Z

    return v0
.end method

.method public hasStarRating()Z
    .locals 1

    iget-boolean v0, p0, Lcom/google/android/finsky/protos/Rev$Review;->hasStarRating:Z

    return v0
.end method

.method public hasTimestampMsec()Z
    .locals 1

    iget-boolean v0, p0, Lcom/google/android/finsky/protos/Rev$Review;->hasTimestampMsec:Z

    return v0
.end method

.method public hasTitle()Z
    .locals 1

    iget-boolean v0, p0, Lcom/google/android/finsky/protos/Rev$Review;->hasTitle:Z

    return v0
.end method

.method public hasUrl()Z
    .locals 1

    iget-boolean v0, p0, Lcom/google/android/finsky/protos/Rev$Review;->hasUrl:Z

    return v0
.end method

.method public mergeFrom(Lcom/google/protobuf/micro/CodedInputStreamMicro;)Lcom/google/android/finsky/protos/Rev$Review;
    .locals 4
    .param p1    # Lcom/google/protobuf/micro/CodedInputStreamMicro;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    :cond_0
    :goto_0
    invoke-virtual {p1}, Lcom/google/protobuf/micro/CodedInputStreamMicro;->readTag()I

    move-result v0

    sparse-switch v0, :sswitch_data_0

    invoke-virtual {p0, p1, v0}, Lcom/google/android/finsky/protos/Rev$Review;->parseUnknownField(Lcom/google/protobuf/micro/CodedInputStreamMicro;I)Z

    move-result v2

    if-nez v2, :cond_0

    :sswitch_0
    return-object p0

    :sswitch_1
    invoke-virtual {p1}, Lcom/google/protobuf/micro/CodedInputStreamMicro;->readString()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {p0, v2}, Lcom/google/android/finsky/protos/Rev$Review;->setAuthorName(Ljava/lang/String;)Lcom/google/android/finsky/protos/Rev$Review;

    goto :goto_0

    :sswitch_2
    invoke-virtual {p1}, Lcom/google/protobuf/micro/CodedInputStreamMicro;->readString()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {p0, v2}, Lcom/google/android/finsky/protos/Rev$Review;->setUrl(Ljava/lang/String;)Lcom/google/android/finsky/protos/Rev$Review;

    goto :goto_0

    :sswitch_3
    invoke-virtual {p1}, Lcom/google/protobuf/micro/CodedInputStreamMicro;->readString()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {p0, v2}, Lcom/google/android/finsky/protos/Rev$Review;->setSource(Ljava/lang/String;)Lcom/google/android/finsky/protos/Rev$Review;

    goto :goto_0

    :sswitch_4
    invoke-virtual {p1}, Lcom/google/protobuf/micro/CodedInputStreamMicro;->readString()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {p0, v2}, Lcom/google/android/finsky/protos/Rev$Review;->setDocumentVersion(Ljava/lang/String;)Lcom/google/android/finsky/protos/Rev$Review;

    goto :goto_0

    :sswitch_5
    invoke-virtual {p1}, Lcom/google/protobuf/micro/CodedInputStreamMicro;->readInt64()J

    move-result-wide v2

    invoke-virtual {p0, v2, v3}, Lcom/google/android/finsky/protos/Rev$Review;->setTimestampMsec(J)Lcom/google/android/finsky/protos/Rev$Review;

    goto :goto_0

    :sswitch_6
    invoke-virtual {p1}, Lcom/google/protobuf/micro/CodedInputStreamMicro;->readInt32()I

    move-result v2

    invoke-virtual {p0, v2}, Lcom/google/android/finsky/protos/Rev$Review;->setStarRating(I)Lcom/google/android/finsky/protos/Rev$Review;

    goto :goto_0

    :sswitch_7
    invoke-virtual {p1}, Lcom/google/protobuf/micro/CodedInputStreamMicro;->readString()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {p0, v2}, Lcom/google/android/finsky/protos/Rev$Review;->setTitle(Ljava/lang/String;)Lcom/google/android/finsky/protos/Rev$Review;

    goto :goto_0

    :sswitch_8
    invoke-virtual {p1}, Lcom/google/protobuf/micro/CodedInputStreamMicro;->readString()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {p0, v2}, Lcom/google/android/finsky/protos/Rev$Review;->setComment(Ljava/lang/String;)Lcom/google/android/finsky/protos/Rev$Review;

    goto :goto_0

    :sswitch_9
    invoke-virtual {p1}, Lcom/google/protobuf/micro/CodedInputStreamMicro;->readString()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {p0, v2}, Lcom/google/android/finsky/protos/Rev$Review;->setCommentId(Ljava/lang/String;)Lcom/google/android/finsky/protos/Rev$Review;

    goto :goto_0

    :sswitch_a
    invoke-virtual {p1}, Lcom/google/protobuf/micro/CodedInputStreamMicro;->readString()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {p0, v2}, Lcom/google/android/finsky/protos/Rev$Review;->setDeviceName(Ljava/lang/String;)Lcom/google/android/finsky/protos/Rev$Review;

    goto :goto_0

    :sswitch_b
    invoke-virtual {p1}, Lcom/google/protobuf/micro/CodedInputStreamMicro;->readString()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {p0, v2}, Lcom/google/android/finsky/protos/Rev$Review;->setReplyText(Ljava/lang/String;)Lcom/google/android/finsky/protos/Rev$Review;

    goto :goto_0

    :sswitch_c
    invoke-virtual {p1}, Lcom/google/protobuf/micro/CodedInputStreamMicro;->readInt64()J

    move-result-wide v2

    invoke-virtual {p0, v2, v3}, Lcom/google/android/finsky/protos/Rev$Review;->setReplyTimestampMsec(J)Lcom/google/android/finsky/protos/Rev$Review;

    goto :goto_0

    :sswitch_d
    new-instance v1, Lcom/google/android/finsky/protos/PlusData$PlusProfile;

    invoke-direct {v1}, Lcom/google/android/finsky/protos/PlusData$PlusProfile;-><init>()V

    invoke-virtual {p1, v1}, Lcom/google/protobuf/micro/CodedInputStreamMicro;->readMessage(Lcom/google/protobuf/micro/MessageMicro;)V

    invoke-virtual {p0, v1}, Lcom/google/android/finsky/protos/Rev$Review;->setPlusProfile(Lcom/google/android/finsky/protos/PlusData$PlusProfile;)Lcom/google/android/finsky/protos/Rev$Review;

    goto :goto_0

    :sswitch_data_0
    .sparse-switch
        0x0 -> :sswitch_0
        0xa -> :sswitch_1
        0x12 -> :sswitch_2
        0x1a -> :sswitch_3
        0x22 -> :sswitch_4
        0x28 -> :sswitch_5
        0x30 -> :sswitch_6
        0x3a -> :sswitch_7
        0x42 -> :sswitch_8
        0x4a -> :sswitch_9
        0x9a -> :sswitch_a
        0xea -> :sswitch_b
        0xf0 -> :sswitch_c
        0xfa -> :sswitch_d
    .end sparse-switch
.end method

.method public bridge synthetic mergeFrom(Lcom/google/protobuf/micro/CodedInputStreamMicro;)Lcom/google/protobuf/micro/MessageMicro;
    .locals 1
    .param p1    # Lcom/google/protobuf/micro/CodedInputStreamMicro;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    invoke-virtual {p0, p1}, Lcom/google/android/finsky/protos/Rev$Review;->mergeFrom(Lcom/google/protobuf/micro/CodedInputStreamMicro;)Lcom/google/android/finsky/protos/Rev$Review;

    move-result-object v0

    return-object v0
.end method

.method public setAuthorName(Ljava/lang/String;)Lcom/google/android/finsky/protos/Rev$Review;
    .locals 1
    .param p1    # Ljava/lang/String;

    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/google/android/finsky/protos/Rev$Review;->hasAuthorName:Z

    iput-object p1, p0, Lcom/google/android/finsky/protos/Rev$Review;->authorName_:Ljava/lang/String;

    return-object p0
.end method

.method public setComment(Ljava/lang/String;)Lcom/google/android/finsky/protos/Rev$Review;
    .locals 1
    .param p1    # Ljava/lang/String;

    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/google/android/finsky/protos/Rev$Review;->hasComment:Z

    iput-object p1, p0, Lcom/google/android/finsky/protos/Rev$Review;->comment_:Ljava/lang/String;

    return-object p0
.end method

.method public setCommentId(Ljava/lang/String;)Lcom/google/android/finsky/protos/Rev$Review;
    .locals 1
    .param p1    # Ljava/lang/String;

    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/google/android/finsky/protos/Rev$Review;->hasCommentId:Z

    iput-object p1, p0, Lcom/google/android/finsky/protos/Rev$Review;->commentId_:Ljava/lang/String;

    return-object p0
.end method

.method public setDeviceName(Ljava/lang/String;)Lcom/google/android/finsky/protos/Rev$Review;
    .locals 1
    .param p1    # Ljava/lang/String;

    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/google/android/finsky/protos/Rev$Review;->hasDeviceName:Z

    iput-object p1, p0, Lcom/google/android/finsky/protos/Rev$Review;->deviceName_:Ljava/lang/String;

    return-object p0
.end method

.method public setDocumentVersion(Ljava/lang/String;)Lcom/google/android/finsky/protos/Rev$Review;
    .locals 1
    .param p1    # Ljava/lang/String;

    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/google/android/finsky/protos/Rev$Review;->hasDocumentVersion:Z

    iput-object p1, p0, Lcom/google/android/finsky/protos/Rev$Review;->documentVersion_:Ljava/lang/String;

    return-object p0
.end method

.method public setPlusProfile(Lcom/google/android/finsky/protos/PlusData$PlusProfile;)Lcom/google/android/finsky/protos/Rev$Review;
    .locals 1
    .param p1    # Lcom/google/android/finsky/protos/PlusData$PlusProfile;

    if-nez p1, :cond_0

    new-instance v0, Ljava/lang/NullPointerException;

    invoke-direct {v0}, Ljava/lang/NullPointerException;-><init>()V

    throw v0

    :cond_0
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/google/android/finsky/protos/Rev$Review;->hasPlusProfile:Z

    iput-object p1, p0, Lcom/google/android/finsky/protos/Rev$Review;->plusProfile_:Lcom/google/android/finsky/protos/PlusData$PlusProfile;

    return-object p0
.end method

.method public setReplyText(Ljava/lang/String;)Lcom/google/android/finsky/protos/Rev$Review;
    .locals 1
    .param p1    # Ljava/lang/String;

    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/google/android/finsky/protos/Rev$Review;->hasReplyText:Z

    iput-object p1, p0, Lcom/google/android/finsky/protos/Rev$Review;->replyText_:Ljava/lang/String;

    return-object p0
.end method

.method public setReplyTimestampMsec(J)Lcom/google/android/finsky/protos/Rev$Review;
    .locals 1
    .param p1    # J

    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/google/android/finsky/protos/Rev$Review;->hasReplyTimestampMsec:Z

    iput-wide p1, p0, Lcom/google/android/finsky/protos/Rev$Review;->replyTimestampMsec_:J

    return-object p0
.end method

.method public setSource(Ljava/lang/String;)Lcom/google/android/finsky/protos/Rev$Review;
    .locals 1
    .param p1    # Ljava/lang/String;

    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/google/android/finsky/protos/Rev$Review;->hasSource:Z

    iput-object p1, p0, Lcom/google/android/finsky/protos/Rev$Review;->source_:Ljava/lang/String;

    return-object p0
.end method

.method public setStarRating(I)Lcom/google/android/finsky/protos/Rev$Review;
    .locals 1
    .param p1    # I

    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/google/android/finsky/protos/Rev$Review;->hasStarRating:Z

    iput p1, p0, Lcom/google/android/finsky/protos/Rev$Review;->starRating_:I

    return-object p0
.end method

.method public setTimestampMsec(J)Lcom/google/android/finsky/protos/Rev$Review;
    .locals 1
    .param p1    # J

    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/google/android/finsky/protos/Rev$Review;->hasTimestampMsec:Z

    iput-wide p1, p0, Lcom/google/android/finsky/protos/Rev$Review;->timestampMsec_:J

    return-object p0
.end method

.method public setTitle(Ljava/lang/String;)Lcom/google/android/finsky/protos/Rev$Review;
    .locals 1
    .param p1    # Ljava/lang/String;

    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/google/android/finsky/protos/Rev$Review;->hasTitle:Z

    iput-object p1, p0, Lcom/google/android/finsky/protos/Rev$Review;->title_:Ljava/lang/String;

    return-object p0
.end method

.method public setUrl(Ljava/lang/String;)Lcom/google/android/finsky/protos/Rev$Review;
    .locals 1
    .param p1    # Ljava/lang/String;

    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/google/android/finsky/protos/Rev$Review;->hasUrl:Z

    iput-object p1, p0, Lcom/google/android/finsky/protos/Rev$Review;->url_:Ljava/lang/String;

    return-object p0
.end method

.method public writeTo(Lcom/google/protobuf/micro/CodedOutputStreamMicro;)V
    .locals 3
    .param p1    # Lcom/google/protobuf/micro/CodedOutputStreamMicro;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    invoke-virtual {p0}, Lcom/google/android/finsky/protos/Rev$Review;->hasAuthorName()Z

    move-result v0

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    invoke-virtual {p0}, Lcom/google/android/finsky/protos/Rev$Review;->getAuthorName()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {p1, v0, v1}, Lcom/google/protobuf/micro/CodedOutputStreamMicro;->writeString(ILjava/lang/String;)V

    :cond_0
    invoke-virtual {p0}, Lcom/google/android/finsky/protos/Rev$Review;->hasUrl()Z

    move-result v0

    if-eqz v0, :cond_1

    const/4 v0, 0x2

    invoke-virtual {p0}, Lcom/google/android/finsky/protos/Rev$Review;->getUrl()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {p1, v0, v1}, Lcom/google/protobuf/micro/CodedOutputStreamMicro;->writeString(ILjava/lang/String;)V

    :cond_1
    invoke-virtual {p0}, Lcom/google/android/finsky/protos/Rev$Review;->hasSource()Z

    move-result v0

    if-eqz v0, :cond_2

    const/4 v0, 0x3

    invoke-virtual {p0}, Lcom/google/android/finsky/protos/Rev$Review;->getSource()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {p1, v0, v1}, Lcom/google/protobuf/micro/CodedOutputStreamMicro;->writeString(ILjava/lang/String;)V

    :cond_2
    invoke-virtual {p0}, Lcom/google/android/finsky/protos/Rev$Review;->hasDocumentVersion()Z

    move-result v0

    if-eqz v0, :cond_3

    const/4 v0, 0x4

    invoke-virtual {p0}, Lcom/google/android/finsky/protos/Rev$Review;->getDocumentVersion()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {p1, v0, v1}, Lcom/google/protobuf/micro/CodedOutputStreamMicro;->writeString(ILjava/lang/String;)V

    :cond_3
    invoke-virtual {p0}, Lcom/google/android/finsky/protos/Rev$Review;->hasTimestampMsec()Z

    move-result v0

    if-eqz v0, :cond_4

    const/4 v0, 0x5

    invoke-virtual {p0}, Lcom/google/android/finsky/protos/Rev$Review;->getTimestampMsec()J

    move-result-wide v1

    invoke-virtual {p1, v0, v1, v2}, Lcom/google/protobuf/micro/CodedOutputStreamMicro;->writeInt64(IJ)V

    :cond_4
    invoke-virtual {p0}, Lcom/google/android/finsky/protos/Rev$Review;->hasStarRating()Z

    move-result v0

    if-eqz v0, :cond_5

    const/4 v0, 0x6

    invoke-virtual {p0}, Lcom/google/android/finsky/protos/Rev$Review;->getStarRating()I

    move-result v1

    invoke-virtual {p1, v0, v1}, Lcom/google/protobuf/micro/CodedOutputStreamMicro;->writeInt32(II)V

    :cond_5
    invoke-virtual {p0}, Lcom/google/android/finsky/protos/Rev$Review;->hasTitle()Z

    move-result v0

    if-eqz v0, :cond_6

    const/4 v0, 0x7

    invoke-virtual {p0}, Lcom/google/android/finsky/protos/Rev$Review;->getTitle()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {p1, v0, v1}, Lcom/google/protobuf/micro/CodedOutputStreamMicro;->writeString(ILjava/lang/String;)V

    :cond_6
    invoke-virtual {p0}, Lcom/google/android/finsky/protos/Rev$Review;->hasComment()Z

    move-result v0

    if-eqz v0, :cond_7

    const/16 v0, 0x8

    invoke-virtual {p0}, Lcom/google/android/finsky/protos/Rev$Review;->getComment()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {p1, v0, v1}, Lcom/google/protobuf/micro/CodedOutputStreamMicro;->writeString(ILjava/lang/String;)V

    :cond_7
    invoke-virtual {p0}, Lcom/google/android/finsky/protos/Rev$Review;->hasCommentId()Z

    move-result v0

    if-eqz v0, :cond_8

    const/16 v0, 0x9

    invoke-virtual {p0}, Lcom/google/android/finsky/protos/Rev$Review;->getCommentId()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {p1, v0, v1}, Lcom/google/protobuf/micro/CodedOutputStreamMicro;->writeString(ILjava/lang/String;)V

    :cond_8
    invoke-virtual {p0}, Lcom/google/android/finsky/protos/Rev$Review;->hasDeviceName()Z

    move-result v0

    if-eqz v0, :cond_9

    const/16 v0, 0x13

    invoke-virtual {p0}, Lcom/google/android/finsky/protos/Rev$Review;->getDeviceName()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {p1, v0, v1}, Lcom/google/protobuf/micro/CodedOutputStreamMicro;->writeString(ILjava/lang/String;)V

    :cond_9
    invoke-virtual {p0}, Lcom/google/android/finsky/protos/Rev$Review;->hasReplyText()Z

    move-result v0

    if-eqz v0, :cond_a

    const/16 v0, 0x1d

    invoke-virtual {p0}, Lcom/google/android/finsky/protos/Rev$Review;->getReplyText()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {p1, v0, v1}, Lcom/google/protobuf/micro/CodedOutputStreamMicro;->writeString(ILjava/lang/String;)V

    :cond_a
    invoke-virtual {p0}, Lcom/google/android/finsky/protos/Rev$Review;->hasReplyTimestampMsec()Z

    move-result v0

    if-eqz v0, :cond_b

    const/16 v0, 0x1e

    invoke-virtual {p0}, Lcom/google/android/finsky/protos/Rev$Review;->getReplyTimestampMsec()J

    move-result-wide v1

    invoke-virtual {p1, v0, v1, v2}, Lcom/google/protobuf/micro/CodedOutputStreamMicro;->writeInt64(IJ)V

    :cond_b
    invoke-virtual {p0}, Lcom/google/android/finsky/protos/Rev$Review;->hasPlusProfile()Z

    move-result v0

    if-eqz v0, :cond_c

    const/16 v0, 0x1f

    invoke-virtual {p0}, Lcom/google/android/finsky/protos/Rev$Review;->getPlusProfile()Lcom/google/android/finsky/protos/PlusData$PlusProfile;

    move-result-object v1

    invoke-virtual {p1, v0, v1}, Lcom/google/protobuf/micro/CodedOutputStreamMicro;->writeMessage(ILcom/google/protobuf/micro/MessageMicro;)V

    :cond_c
    return-void
.end method
