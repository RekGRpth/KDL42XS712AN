.class public final Lcom/google/android/finsky/protos/DocAnnotations$Link;
.super Lcom/google/protobuf/micro/MessageMicro;
.source "DocAnnotations.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/google/android/finsky/protos/DocAnnotations;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x19
    name = "Link"
.end annotation


# instance fields
.field private cachedSize:I

.field private hasResolvedLink:Z

.field private hasUri:Z

.field private resolvedLink_:Lcom/google/android/finsky/protos/ResolveLink$ResolvedLink;

.field private uri_:Ljava/lang/String;


# direct methods
.method public constructor <init>()V
    .locals 1

    invoke-direct {p0}, Lcom/google/protobuf/micro/MessageMicro;-><init>()V

    const-string v0, ""

    iput-object v0, p0, Lcom/google/android/finsky/protos/DocAnnotations$Link;->uri_:Ljava/lang/String;

    const/4 v0, 0x0

    iput-object v0, p0, Lcom/google/android/finsky/protos/DocAnnotations$Link;->resolvedLink_:Lcom/google/android/finsky/protos/ResolveLink$ResolvedLink;

    const/4 v0, -0x1

    iput v0, p0, Lcom/google/android/finsky/protos/DocAnnotations$Link;->cachedSize:I

    return-void
.end method


# virtual methods
.method public getCachedSize()I
    .locals 1

    iget v0, p0, Lcom/google/android/finsky/protos/DocAnnotations$Link;->cachedSize:I

    if-gez v0, :cond_0

    invoke-virtual {p0}, Lcom/google/android/finsky/protos/DocAnnotations$Link;->getSerializedSize()I

    :cond_0
    iget v0, p0, Lcom/google/android/finsky/protos/DocAnnotations$Link;->cachedSize:I

    return v0
.end method

.method public getResolvedLink()Lcom/google/android/finsky/protos/ResolveLink$ResolvedLink;
    .locals 1

    iget-object v0, p0, Lcom/google/android/finsky/protos/DocAnnotations$Link;->resolvedLink_:Lcom/google/android/finsky/protos/ResolveLink$ResolvedLink;

    return-object v0
.end method

.method public getSerializedSize()I
    .locals 3

    const/4 v0, 0x0

    invoke-virtual {p0}, Lcom/google/android/finsky/protos/DocAnnotations$Link;->hasUri()Z

    move-result v1

    if-eqz v1, :cond_0

    const/4 v1, 0x1

    invoke-virtual {p0}, Lcom/google/android/finsky/protos/DocAnnotations$Link;->getUri()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2}, Lcom/google/protobuf/micro/CodedOutputStreamMicro;->computeStringSize(ILjava/lang/String;)I

    move-result v1

    add-int/2addr v0, v1

    :cond_0
    invoke-virtual {p0}, Lcom/google/android/finsky/protos/DocAnnotations$Link;->hasResolvedLink()Z

    move-result v1

    if-eqz v1, :cond_1

    const/4 v1, 0x2

    invoke-virtual {p0}, Lcom/google/android/finsky/protos/DocAnnotations$Link;->getResolvedLink()Lcom/google/android/finsky/protos/ResolveLink$ResolvedLink;

    move-result-object v2

    invoke-static {v1, v2}, Lcom/google/protobuf/micro/CodedOutputStreamMicro;->computeMessageSize(ILcom/google/protobuf/micro/MessageMicro;)I

    move-result v1

    add-int/2addr v0, v1

    :cond_1
    iput v0, p0, Lcom/google/android/finsky/protos/DocAnnotations$Link;->cachedSize:I

    return v0
.end method

.method public getUri()Ljava/lang/String;
    .locals 1

    iget-object v0, p0, Lcom/google/android/finsky/protos/DocAnnotations$Link;->uri_:Ljava/lang/String;

    return-object v0
.end method

.method public hasResolvedLink()Z
    .locals 1

    iget-boolean v0, p0, Lcom/google/android/finsky/protos/DocAnnotations$Link;->hasResolvedLink:Z

    return v0
.end method

.method public hasUri()Z
    .locals 1

    iget-boolean v0, p0, Lcom/google/android/finsky/protos/DocAnnotations$Link;->hasUri:Z

    return v0
.end method

.method public mergeFrom(Lcom/google/protobuf/micro/CodedInputStreamMicro;)Lcom/google/android/finsky/protos/DocAnnotations$Link;
    .locals 3
    .param p1    # Lcom/google/protobuf/micro/CodedInputStreamMicro;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    :cond_0
    :goto_0
    invoke-virtual {p1}, Lcom/google/protobuf/micro/CodedInputStreamMicro;->readTag()I

    move-result v0

    sparse-switch v0, :sswitch_data_0

    invoke-virtual {p0, p1, v0}, Lcom/google/android/finsky/protos/DocAnnotations$Link;->parseUnknownField(Lcom/google/protobuf/micro/CodedInputStreamMicro;I)Z

    move-result v2

    if-nez v2, :cond_0

    :sswitch_0
    return-object p0

    :sswitch_1
    invoke-virtual {p1}, Lcom/google/protobuf/micro/CodedInputStreamMicro;->readString()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {p0, v2}, Lcom/google/android/finsky/protos/DocAnnotations$Link;->setUri(Ljava/lang/String;)Lcom/google/android/finsky/protos/DocAnnotations$Link;

    goto :goto_0

    :sswitch_2
    new-instance v1, Lcom/google/android/finsky/protos/ResolveLink$ResolvedLink;

    invoke-direct {v1}, Lcom/google/android/finsky/protos/ResolveLink$ResolvedLink;-><init>()V

    invoke-virtual {p1, v1}, Lcom/google/protobuf/micro/CodedInputStreamMicro;->readMessage(Lcom/google/protobuf/micro/MessageMicro;)V

    invoke-virtual {p0, v1}, Lcom/google/android/finsky/protos/DocAnnotations$Link;->setResolvedLink(Lcom/google/android/finsky/protos/ResolveLink$ResolvedLink;)Lcom/google/android/finsky/protos/DocAnnotations$Link;

    goto :goto_0

    :sswitch_data_0
    .sparse-switch
        0x0 -> :sswitch_0
        0xa -> :sswitch_1
        0x12 -> :sswitch_2
    .end sparse-switch
.end method

.method public bridge synthetic mergeFrom(Lcom/google/protobuf/micro/CodedInputStreamMicro;)Lcom/google/protobuf/micro/MessageMicro;
    .locals 1
    .param p1    # Lcom/google/protobuf/micro/CodedInputStreamMicro;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    invoke-virtual {p0, p1}, Lcom/google/android/finsky/protos/DocAnnotations$Link;->mergeFrom(Lcom/google/protobuf/micro/CodedInputStreamMicro;)Lcom/google/android/finsky/protos/DocAnnotations$Link;

    move-result-object v0

    return-object v0
.end method

.method public setResolvedLink(Lcom/google/android/finsky/protos/ResolveLink$ResolvedLink;)Lcom/google/android/finsky/protos/DocAnnotations$Link;
    .locals 1
    .param p1    # Lcom/google/android/finsky/protos/ResolveLink$ResolvedLink;

    if-nez p1, :cond_0

    new-instance v0, Ljava/lang/NullPointerException;

    invoke-direct {v0}, Ljava/lang/NullPointerException;-><init>()V

    throw v0

    :cond_0
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/google/android/finsky/protos/DocAnnotations$Link;->hasResolvedLink:Z

    iput-object p1, p0, Lcom/google/android/finsky/protos/DocAnnotations$Link;->resolvedLink_:Lcom/google/android/finsky/protos/ResolveLink$ResolvedLink;

    return-object p0
.end method

.method public setUri(Ljava/lang/String;)Lcom/google/android/finsky/protos/DocAnnotations$Link;
    .locals 1
    .param p1    # Ljava/lang/String;

    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/google/android/finsky/protos/DocAnnotations$Link;->hasUri:Z

    iput-object p1, p0, Lcom/google/android/finsky/protos/DocAnnotations$Link;->uri_:Ljava/lang/String;

    return-object p0
.end method

.method public writeTo(Lcom/google/protobuf/micro/CodedOutputStreamMicro;)V
    .locals 2
    .param p1    # Lcom/google/protobuf/micro/CodedOutputStreamMicro;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    invoke-virtual {p0}, Lcom/google/android/finsky/protos/DocAnnotations$Link;->hasUri()Z

    move-result v0

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    invoke-virtual {p0}, Lcom/google/android/finsky/protos/DocAnnotations$Link;->getUri()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {p1, v0, v1}, Lcom/google/protobuf/micro/CodedOutputStreamMicro;->writeString(ILjava/lang/String;)V

    :cond_0
    invoke-virtual {p0}, Lcom/google/android/finsky/protos/DocAnnotations$Link;->hasResolvedLink()Z

    move-result v0

    if-eqz v0, :cond_1

    const/4 v0, 0x2

    invoke-virtual {p0}, Lcom/google/android/finsky/protos/DocAnnotations$Link;->getResolvedLink()Lcom/google/android/finsky/protos/ResolveLink$ResolvedLink;

    move-result-object v1

    invoke-virtual {p1, v0, v1}, Lcom/google/protobuf/micro/CodedOutputStreamMicro;->writeMessage(ILcom/google/protobuf/micro/MessageMicro;)V

    :cond_1
    return-void
.end method
