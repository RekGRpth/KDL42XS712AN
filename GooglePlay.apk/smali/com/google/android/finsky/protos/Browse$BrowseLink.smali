.class public final Lcom/google/android/finsky/protos/Browse$BrowseLink;
.super Lcom/google/protobuf/micro/MessageMicro;
.source "Browse.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/google/android/finsky/protos/Browse;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x19
    name = "BrowseLink"
.end annotation


# instance fields
.field private cachedSize:I

.field private dataUrl_:Ljava/lang/String;

.field private hasDataUrl:Z

.field private hasName:Z

.field private hasServerLogsCookie:Z

.field private name_:Ljava/lang/String;

.field private serverLogsCookie_:Lcom/google/protobuf/micro/ByteStringMicro;


# direct methods
.method public constructor <init>()V
    .locals 1

    invoke-direct {p0}, Lcom/google/protobuf/micro/MessageMicro;-><init>()V

    const-string v0, ""

    iput-object v0, p0, Lcom/google/android/finsky/protos/Browse$BrowseLink;->name_:Ljava/lang/String;

    const-string v0, ""

    iput-object v0, p0, Lcom/google/android/finsky/protos/Browse$BrowseLink;->dataUrl_:Ljava/lang/String;

    sget-object v0, Lcom/google/protobuf/micro/ByteStringMicro;->EMPTY:Lcom/google/protobuf/micro/ByteStringMicro;

    iput-object v0, p0, Lcom/google/android/finsky/protos/Browse$BrowseLink;->serverLogsCookie_:Lcom/google/protobuf/micro/ByteStringMicro;

    const/4 v0, -0x1

    iput v0, p0, Lcom/google/android/finsky/protos/Browse$BrowseLink;->cachedSize:I

    return-void
.end method


# virtual methods
.method public getCachedSize()I
    .locals 1

    iget v0, p0, Lcom/google/android/finsky/protos/Browse$BrowseLink;->cachedSize:I

    if-gez v0, :cond_0

    invoke-virtual {p0}, Lcom/google/android/finsky/protos/Browse$BrowseLink;->getSerializedSize()I

    :cond_0
    iget v0, p0, Lcom/google/android/finsky/protos/Browse$BrowseLink;->cachedSize:I

    return v0
.end method

.method public getDataUrl()Ljava/lang/String;
    .locals 1

    iget-object v0, p0, Lcom/google/android/finsky/protos/Browse$BrowseLink;->dataUrl_:Ljava/lang/String;

    return-object v0
.end method

.method public getName()Ljava/lang/String;
    .locals 1

    iget-object v0, p0, Lcom/google/android/finsky/protos/Browse$BrowseLink;->name_:Ljava/lang/String;

    return-object v0
.end method

.method public getSerializedSize()I
    .locals 3

    const/4 v0, 0x0

    invoke-virtual {p0}, Lcom/google/android/finsky/protos/Browse$BrowseLink;->hasName()Z

    move-result v1

    if-eqz v1, :cond_0

    const/4 v1, 0x1

    invoke-virtual {p0}, Lcom/google/android/finsky/protos/Browse$BrowseLink;->getName()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2}, Lcom/google/protobuf/micro/CodedOutputStreamMicro;->computeStringSize(ILjava/lang/String;)I

    move-result v1

    add-int/2addr v0, v1

    :cond_0
    invoke-virtual {p0}, Lcom/google/android/finsky/protos/Browse$BrowseLink;->hasDataUrl()Z

    move-result v1

    if-eqz v1, :cond_1

    const/4 v1, 0x3

    invoke-virtual {p0}, Lcom/google/android/finsky/protos/Browse$BrowseLink;->getDataUrl()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2}, Lcom/google/protobuf/micro/CodedOutputStreamMicro;->computeStringSize(ILjava/lang/String;)I

    move-result v1

    add-int/2addr v0, v1

    :cond_1
    invoke-virtual {p0}, Lcom/google/android/finsky/protos/Browse$BrowseLink;->hasServerLogsCookie()Z

    move-result v1

    if-eqz v1, :cond_2

    const/4 v1, 0x4

    invoke-virtual {p0}, Lcom/google/android/finsky/protos/Browse$BrowseLink;->getServerLogsCookie()Lcom/google/protobuf/micro/ByteStringMicro;

    move-result-object v2

    invoke-static {v1, v2}, Lcom/google/protobuf/micro/CodedOutputStreamMicro;->computeBytesSize(ILcom/google/protobuf/micro/ByteStringMicro;)I

    move-result v1

    add-int/2addr v0, v1

    :cond_2
    iput v0, p0, Lcom/google/android/finsky/protos/Browse$BrowseLink;->cachedSize:I

    return v0
.end method

.method public getServerLogsCookie()Lcom/google/protobuf/micro/ByteStringMicro;
    .locals 1

    iget-object v0, p0, Lcom/google/android/finsky/protos/Browse$BrowseLink;->serverLogsCookie_:Lcom/google/protobuf/micro/ByteStringMicro;

    return-object v0
.end method

.method public hasDataUrl()Z
    .locals 1

    iget-boolean v0, p0, Lcom/google/android/finsky/protos/Browse$BrowseLink;->hasDataUrl:Z

    return v0
.end method

.method public hasName()Z
    .locals 1

    iget-boolean v0, p0, Lcom/google/android/finsky/protos/Browse$BrowseLink;->hasName:Z

    return v0
.end method

.method public hasServerLogsCookie()Z
    .locals 1

    iget-boolean v0, p0, Lcom/google/android/finsky/protos/Browse$BrowseLink;->hasServerLogsCookie:Z

    return v0
.end method

.method public mergeFrom(Lcom/google/protobuf/micro/CodedInputStreamMicro;)Lcom/google/android/finsky/protos/Browse$BrowseLink;
    .locals 2
    .param p1    # Lcom/google/protobuf/micro/CodedInputStreamMicro;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    :cond_0
    :goto_0
    invoke-virtual {p1}, Lcom/google/protobuf/micro/CodedInputStreamMicro;->readTag()I

    move-result v0

    sparse-switch v0, :sswitch_data_0

    invoke-virtual {p0, p1, v0}, Lcom/google/android/finsky/protos/Browse$BrowseLink;->parseUnknownField(Lcom/google/protobuf/micro/CodedInputStreamMicro;I)Z

    move-result v1

    if-nez v1, :cond_0

    :sswitch_0
    return-object p0

    :sswitch_1
    invoke-virtual {p1}, Lcom/google/protobuf/micro/CodedInputStreamMicro;->readString()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {p0, v1}, Lcom/google/android/finsky/protos/Browse$BrowseLink;->setName(Ljava/lang/String;)Lcom/google/android/finsky/protos/Browse$BrowseLink;

    goto :goto_0

    :sswitch_2
    invoke-virtual {p1}, Lcom/google/protobuf/micro/CodedInputStreamMicro;->readString()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {p0, v1}, Lcom/google/android/finsky/protos/Browse$BrowseLink;->setDataUrl(Ljava/lang/String;)Lcom/google/android/finsky/protos/Browse$BrowseLink;

    goto :goto_0

    :sswitch_3
    invoke-virtual {p1}, Lcom/google/protobuf/micro/CodedInputStreamMicro;->readBytes()Lcom/google/protobuf/micro/ByteStringMicro;

    move-result-object v1

    invoke-virtual {p0, v1}, Lcom/google/android/finsky/protos/Browse$BrowseLink;->setServerLogsCookie(Lcom/google/protobuf/micro/ByteStringMicro;)Lcom/google/android/finsky/protos/Browse$BrowseLink;

    goto :goto_0

    :sswitch_data_0
    .sparse-switch
        0x0 -> :sswitch_0
        0xa -> :sswitch_1
        0x1a -> :sswitch_2
        0x22 -> :sswitch_3
    .end sparse-switch
.end method

.method public bridge synthetic mergeFrom(Lcom/google/protobuf/micro/CodedInputStreamMicro;)Lcom/google/protobuf/micro/MessageMicro;
    .locals 1
    .param p1    # Lcom/google/protobuf/micro/CodedInputStreamMicro;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    invoke-virtual {p0, p1}, Lcom/google/android/finsky/protos/Browse$BrowseLink;->mergeFrom(Lcom/google/protobuf/micro/CodedInputStreamMicro;)Lcom/google/android/finsky/protos/Browse$BrowseLink;

    move-result-object v0

    return-object v0
.end method

.method public setDataUrl(Ljava/lang/String;)Lcom/google/android/finsky/protos/Browse$BrowseLink;
    .locals 1
    .param p1    # Ljava/lang/String;

    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/google/android/finsky/protos/Browse$BrowseLink;->hasDataUrl:Z

    iput-object p1, p0, Lcom/google/android/finsky/protos/Browse$BrowseLink;->dataUrl_:Ljava/lang/String;

    return-object p0
.end method

.method public setName(Ljava/lang/String;)Lcom/google/android/finsky/protos/Browse$BrowseLink;
    .locals 1
    .param p1    # Ljava/lang/String;

    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/google/android/finsky/protos/Browse$BrowseLink;->hasName:Z

    iput-object p1, p0, Lcom/google/android/finsky/protos/Browse$BrowseLink;->name_:Ljava/lang/String;

    return-object p0
.end method

.method public setServerLogsCookie(Lcom/google/protobuf/micro/ByteStringMicro;)Lcom/google/android/finsky/protos/Browse$BrowseLink;
    .locals 1
    .param p1    # Lcom/google/protobuf/micro/ByteStringMicro;

    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/google/android/finsky/protos/Browse$BrowseLink;->hasServerLogsCookie:Z

    iput-object p1, p0, Lcom/google/android/finsky/protos/Browse$BrowseLink;->serverLogsCookie_:Lcom/google/protobuf/micro/ByteStringMicro;

    return-object p0
.end method

.method public writeTo(Lcom/google/protobuf/micro/CodedOutputStreamMicro;)V
    .locals 2
    .param p1    # Lcom/google/protobuf/micro/CodedOutputStreamMicro;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    invoke-virtual {p0}, Lcom/google/android/finsky/protos/Browse$BrowseLink;->hasName()Z

    move-result v0

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    invoke-virtual {p0}, Lcom/google/android/finsky/protos/Browse$BrowseLink;->getName()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {p1, v0, v1}, Lcom/google/protobuf/micro/CodedOutputStreamMicro;->writeString(ILjava/lang/String;)V

    :cond_0
    invoke-virtual {p0}, Lcom/google/android/finsky/protos/Browse$BrowseLink;->hasDataUrl()Z

    move-result v0

    if-eqz v0, :cond_1

    const/4 v0, 0x3

    invoke-virtual {p0}, Lcom/google/android/finsky/protos/Browse$BrowseLink;->getDataUrl()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {p1, v0, v1}, Lcom/google/protobuf/micro/CodedOutputStreamMicro;->writeString(ILjava/lang/String;)V

    :cond_1
    invoke-virtual {p0}, Lcom/google/android/finsky/protos/Browse$BrowseLink;->hasServerLogsCookie()Z

    move-result v0

    if-eqz v0, :cond_2

    const/4 v0, 0x4

    invoke-virtual {p0}, Lcom/google/android/finsky/protos/Browse$BrowseLink;->getServerLogsCookie()Lcom/google/protobuf/micro/ByteStringMicro;

    move-result-object v1

    invoke-virtual {p1, v0, v1}, Lcom/google/protobuf/micro/CodedOutputStreamMicro;->writeBytes(ILcom/google/protobuf/micro/ByteStringMicro;)V

    :cond_2
    return-void
.end method
