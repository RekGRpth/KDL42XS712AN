.class public final Lcom/google/android/finsky/protos/Details$DetailsResponse;
.super Lcom/google/protobuf/micro/MessageMicro;
.source "Details.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/google/android/finsky/protos/Details;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x19
    name = "DetailsResponse"
.end annotation


# instance fields
.field private analyticsCookie_:Ljava/lang/String;

.field private cachedSize:I

.field private docV1_:Lcom/google/android/finsky/protos/DocumentV1$DocV1;

.field private docV2_:Lcom/google/android/finsky/protos/DocumentV2$DocV2;

.field private footerHtml_:Ljava/lang/String;

.field private hasAnalyticsCookie:Z

.field private hasDocV1:Z

.field private hasDocV2:Z

.field private hasFooterHtml:Z

.field private hasServerLogsCookie:Z

.field private hasUserReview:Z

.field private serverLogsCookie_:Lcom/google/protobuf/micro/ByteStringMicro;

.field private userReview_:Lcom/google/android/finsky/protos/Rev$Review;


# direct methods
.method public constructor <init>()V
    .locals 2

    const/4 v1, 0x0

    invoke-direct {p0}, Lcom/google/protobuf/micro/MessageMicro;-><init>()V

    iput-object v1, p0, Lcom/google/android/finsky/protos/Details$DetailsResponse;->docV1_:Lcom/google/android/finsky/protos/DocumentV1$DocV1;

    iput-object v1, p0, Lcom/google/android/finsky/protos/Details$DetailsResponse;->docV2_:Lcom/google/android/finsky/protos/DocumentV2$DocV2;

    const-string v0, ""

    iput-object v0, p0, Lcom/google/android/finsky/protos/Details$DetailsResponse;->analyticsCookie_:Ljava/lang/String;

    iput-object v1, p0, Lcom/google/android/finsky/protos/Details$DetailsResponse;->userReview_:Lcom/google/android/finsky/protos/Rev$Review;

    const-string v0, ""

    iput-object v0, p0, Lcom/google/android/finsky/protos/Details$DetailsResponse;->footerHtml_:Ljava/lang/String;

    sget-object v0, Lcom/google/protobuf/micro/ByteStringMicro;->EMPTY:Lcom/google/protobuf/micro/ByteStringMicro;

    iput-object v0, p0, Lcom/google/android/finsky/protos/Details$DetailsResponse;->serverLogsCookie_:Lcom/google/protobuf/micro/ByteStringMicro;

    const/4 v0, -0x1

    iput v0, p0, Lcom/google/android/finsky/protos/Details$DetailsResponse;->cachedSize:I

    return-void
.end method


# virtual methods
.method public getAnalyticsCookie()Ljava/lang/String;
    .locals 1

    iget-object v0, p0, Lcom/google/android/finsky/protos/Details$DetailsResponse;->analyticsCookie_:Ljava/lang/String;

    return-object v0
.end method

.method public getCachedSize()I
    .locals 1

    iget v0, p0, Lcom/google/android/finsky/protos/Details$DetailsResponse;->cachedSize:I

    if-gez v0, :cond_0

    invoke-virtual {p0}, Lcom/google/android/finsky/protos/Details$DetailsResponse;->getSerializedSize()I

    :cond_0
    iget v0, p0, Lcom/google/android/finsky/protos/Details$DetailsResponse;->cachedSize:I

    return v0
.end method

.method public getDocV1()Lcom/google/android/finsky/protos/DocumentV1$DocV1;
    .locals 1

    iget-object v0, p0, Lcom/google/android/finsky/protos/Details$DetailsResponse;->docV1_:Lcom/google/android/finsky/protos/DocumentV1$DocV1;

    return-object v0
.end method

.method public getDocV2()Lcom/google/android/finsky/protos/DocumentV2$DocV2;
    .locals 1

    iget-object v0, p0, Lcom/google/android/finsky/protos/Details$DetailsResponse;->docV2_:Lcom/google/android/finsky/protos/DocumentV2$DocV2;

    return-object v0
.end method

.method public getFooterHtml()Ljava/lang/String;
    .locals 1

    iget-object v0, p0, Lcom/google/android/finsky/protos/Details$DetailsResponse;->footerHtml_:Ljava/lang/String;

    return-object v0
.end method

.method public getSerializedSize()I
    .locals 3

    const/4 v0, 0x0

    invoke-virtual {p0}, Lcom/google/android/finsky/protos/Details$DetailsResponse;->hasDocV1()Z

    move-result v1

    if-eqz v1, :cond_0

    const/4 v1, 0x1

    invoke-virtual {p0}, Lcom/google/android/finsky/protos/Details$DetailsResponse;->getDocV1()Lcom/google/android/finsky/protos/DocumentV1$DocV1;

    move-result-object v2

    invoke-static {v1, v2}, Lcom/google/protobuf/micro/CodedOutputStreamMicro;->computeMessageSize(ILcom/google/protobuf/micro/MessageMicro;)I

    move-result v1

    add-int/2addr v0, v1

    :cond_0
    invoke-virtual {p0}, Lcom/google/android/finsky/protos/Details$DetailsResponse;->hasAnalyticsCookie()Z

    move-result v1

    if-eqz v1, :cond_1

    const/4 v1, 0x2

    invoke-virtual {p0}, Lcom/google/android/finsky/protos/Details$DetailsResponse;->getAnalyticsCookie()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2}, Lcom/google/protobuf/micro/CodedOutputStreamMicro;->computeStringSize(ILjava/lang/String;)I

    move-result v1

    add-int/2addr v0, v1

    :cond_1
    invoke-virtual {p0}, Lcom/google/android/finsky/protos/Details$DetailsResponse;->hasUserReview()Z

    move-result v1

    if-eqz v1, :cond_2

    const/4 v1, 0x3

    invoke-virtual {p0}, Lcom/google/android/finsky/protos/Details$DetailsResponse;->getUserReview()Lcom/google/android/finsky/protos/Rev$Review;

    move-result-object v2

    invoke-static {v1, v2}, Lcom/google/protobuf/micro/CodedOutputStreamMicro;->computeMessageSize(ILcom/google/protobuf/micro/MessageMicro;)I

    move-result v1

    add-int/2addr v0, v1

    :cond_2
    invoke-virtual {p0}, Lcom/google/android/finsky/protos/Details$DetailsResponse;->hasDocV2()Z

    move-result v1

    if-eqz v1, :cond_3

    const/4 v1, 0x4

    invoke-virtual {p0}, Lcom/google/android/finsky/protos/Details$DetailsResponse;->getDocV2()Lcom/google/android/finsky/protos/DocumentV2$DocV2;

    move-result-object v2

    invoke-static {v1, v2}, Lcom/google/protobuf/micro/CodedOutputStreamMicro;->computeMessageSize(ILcom/google/protobuf/micro/MessageMicro;)I

    move-result v1

    add-int/2addr v0, v1

    :cond_3
    invoke-virtual {p0}, Lcom/google/android/finsky/protos/Details$DetailsResponse;->hasFooterHtml()Z

    move-result v1

    if-eqz v1, :cond_4

    const/4 v1, 0x5

    invoke-virtual {p0}, Lcom/google/android/finsky/protos/Details$DetailsResponse;->getFooterHtml()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2}, Lcom/google/protobuf/micro/CodedOutputStreamMicro;->computeStringSize(ILjava/lang/String;)I

    move-result v1

    add-int/2addr v0, v1

    :cond_4
    invoke-virtual {p0}, Lcom/google/android/finsky/protos/Details$DetailsResponse;->hasServerLogsCookie()Z

    move-result v1

    if-eqz v1, :cond_5

    const/4 v1, 0x6

    invoke-virtual {p0}, Lcom/google/android/finsky/protos/Details$DetailsResponse;->getServerLogsCookie()Lcom/google/protobuf/micro/ByteStringMicro;

    move-result-object v2

    invoke-static {v1, v2}, Lcom/google/protobuf/micro/CodedOutputStreamMicro;->computeBytesSize(ILcom/google/protobuf/micro/ByteStringMicro;)I

    move-result v1

    add-int/2addr v0, v1

    :cond_5
    iput v0, p0, Lcom/google/android/finsky/protos/Details$DetailsResponse;->cachedSize:I

    return v0
.end method

.method public getServerLogsCookie()Lcom/google/protobuf/micro/ByteStringMicro;
    .locals 1

    iget-object v0, p0, Lcom/google/android/finsky/protos/Details$DetailsResponse;->serverLogsCookie_:Lcom/google/protobuf/micro/ByteStringMicro;

    return-object v0
.end method

.method public getUserReview()Lcom/google/android/finsky/protos/Rev$Review;
    .locals 1

    iget-object v0, p0, Lcom/google/android/finsky/protos/Details$DetailsResponse;->userReview_:Lcom/google/android/finsky/protos/Rev$Review;

    return-object v0
.end method

.method public hasAnalyticsCookie()Z
    .locals 1

    iget-boolean v0, p0, Lcom/google/android/finsky/protos/Details$DetailsResponse;->hasAnalyticsCookie:Z

    return v0
.end method

.method public hasDocV1()Z
    .locals 1

    iget-boolean v0, p0, Lcom/google/android/finsky/protos/Details$DetailsResponse;->hasDocV1:Z

    return v0
.end method

.method public hasDocV2()Z
    .locals 1

    iget-boolean v0, p0, Lcom/google/android/finsky/protos/Details$DetailsResponse;->hasDocV2:Z

    return v0
.end method

.method public hasFooterHtml()Z
    .locals 1

    iget-boolean v0, p0, Lcom/google/android/finsky/protos/Details$DetailsResponse;->hasFooterHtml:Z

    return v0
.end method

.method public hasServerLogsCookie()Z
    .locals 1

    iget-boolean v0, p0, Lcom/google/android/finsky/protos/Details$DetailsResponse;->hasServerLogsCookie:Z

    return v0
.end method

.method public hasUserReview()Z
    .locals 1

    iget-boolean v0, p0, Lcom/google/android/finsky/protos/Details$DetailsResponse;->hasUserReview:Z

    return v0
.end method

.method public mergeFrom(Lcom/google/protobuf/micro/CodedInputStreamMicro;)Lcom/google/android/finsky/protos/Details$DetailsResponse;
    .locals 3
    .param p1    # Lcom/google/protobuf/micro/CodedInputStreamMicro;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    :cond_0
    :goto_0
    invoke-virtual {p1}, Lcom/google/protobuf/micro/CodedInputStreamMicro;->readTag()I

    move-result v0

    sparse-switch v0, :sswitch_data_0

    invoke-virtual {p0, p1, v0}, Lcom/google/android/finsky/protos/Details$DetailsResponse;->parseUnknownField(Lcom/google/protobuf/micro/CodedInputStreamMicro;I)Z

    move-result v2

    if-nez v2, :cond_0

    :sswitch_0
    return-object p0

    :sswitch_1
    new-instance v1, Lcom/google/android/finsky/protos/DocumentV1$DocV1;

    invoke-direct {v1}, Lcom/google/android/finsky/protos/DocumentV1$DocV1;-><init>()V

    invoke-virtual {p1, v1}, Lcom/google/protobuf/micro/CodedInputStreamMicro;->readMessage(Lcom/google/protobuf/micro/MessageMicro;)V

    invoke-virtual {p0, v1}, Lcom/google/android/finsky/protos/Details$DetailsResponse;->setDocV1(Lcom/google/android/finsky/protos/DocumentV1$DocV1;)Lcom/google/android/finsky/protos/Details$DetailsResponse;

    goto :goto_0

    :sswitch_2
    invoke-virtual {p1}, Lcom/google/protobuf/micro/CodedInputStreamMicro;->readString()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {p0, v2}, Lcom/google/android/finsky/protos/Details$DetailsResponse;->setAnalyticsCookie(Ljava/lang/String;)Lcom/google/android/finsky/protos/Details$DetailsResponse;

    goto :goto_0

    :sswitch_3
    new-instance v1, Lcom/google/android/finsky/protos/Rev$Review;

    invoke-direct {v1}, Lcom/google/android/finsky/protos/Rev$Review;-><init>()V

    invoke-virtual {p1, v1}, Lcom/google/protobuf/micro/CodedInputStreamMicro;->readMessage(Lcom/google/protobuf/micro/MessageMicro;)V

    invoke-virtual {p0, v1}, Lcom/google/android/finsky/protos/Details$DetailsResponse;->setUserReview(Lcom/google/android/finsky/protos/Rev$Review;)Lcom/google/android/finsky/protos/Details$DetailsResponse;

    goto :goto_0

    :sswitch_4
    new-instance v1, Lcom/google/android/finsky/protos/DocumentV2$DocV2;

    invoke-direct {v1}, Lcom/google/android/finsky/protos/DocumentV2$DocV2;-><init>()V

    invoke-virtual {p1, v1}, Lcom/google/protobuf/micro/CodedInputStreamMicro;->readMessage(Lcom/google/protobuf/micro/MessageMicro;)V

    invoke-virtual {p0, v1}, Lcom/google/android/finsky/protos/Details$DetailsResponse;->setDocV2(Lcom/google/android/finsky/protos/DocumentV2$DocV2;)Lcom/google/android/finsky/protos/Details$DetailsResponse;

    goto :goto_0

    :sswitch_5
    invoke-virtual {p1}, Lcom/google/protobuf/micro/CodedInputStreamMicro;->readString()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {p0, v2}, Lcom/google/android/finsky/protos/Details$DetailsResponse;->setFooterHtml(Ljava/lang/String;)Lcom/google/android/finsky/protos/Details$DetailsResponse;

    goto :goto_0

    :sswitch_6
    invoke-virtual {p1}, Lcom/google/protobuf/micro/CodedInputStreamMicro;->readBytes()Lcom/google/protobuf/micro/ByteStringMicro;

    move-result-object v2

    invoke-virtual {p0, v2}, Lcom/google/android/finsky/protos/Details$DetailsResponse;->setServerLogsCookie(Lcom/google/protobuf/micro/ByteStringMicro;)Lcom/google/android/finsky/protos/Details$DetailsResponse;

    goto :goto_0

    :sswitch_data_0
    .sparse-switch
        0x0 -> :sswitch_0
        0xa -> :sswitch_1
        0x12 -> :sswitch_2
        0x1a -> :sswitch_3
        0x22 -> :sswitch_4
        0x2a -> :sswitch_5
        0x32 -> :sswitch_6
    .end sparse-switch
.end method

.method public bridge synthetic mergeFrom(Lcom/google/protobuf/micro/CodedInputStreamMicro;)Lcom/google/protobuf/micro/MessageMicro;
    .locals 1
    .param p1    # Lcom/google/protobuf/micro/CodedInputStreamMicro;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    invoke-virtual {p0, p1}, Lcom/google/android/finsky/protos/Details$DetailsResponse;->mergeFrom(Lcom/google/protobuf/micro/CodedInputStreamMicro;)Lcom/google/android/finsky/protos/Details$DetailsResponse;

    move-result-object v0

    return-object v0
.end method

.method public setAnalyticsCookie(Ljava/lang/String;)Lcom/google/android/finsky/protos/Details$DetailsResponse;
    .locals 1
    .param p1    # Ljava/lang/String;

    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/google/android/finsky/protos/Details$DetailsResponse;->hasAnalyticsCookie:Z

    iput-object p1, p0, Lcom/google/android/finsky/protos/Details$DetailsResponse;->analyticsCookie_:Ljava/lang/String;

    return-object p0
.end method

.method public setDocV1(Lcom/google/android/finsky/protos/DocumentV1$DocV1;)Lcom/google/android/finsky/protos/Details$DetailsResponse;
    .locals 1
    .param p1    # Lcom/google/android/finsky/protos/DocumentV1$DocV1;

    if-nez p1, :cond_0

    new-instance v0, Ljava/lang/NullPointerException;

    invoke-direct {v0}, Ljava/lang/NullPointerException;-><init>()V

    throw v0

    :cond_0
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/google/android/finsky/protos/Details$DetailsResponse;->hasDocV1:Z

    iput-object p1, p0, Lcom/google/android/finsky/protos/Details$DetailsResponse;->docV1_:Lcom/google/android/finsky/protos/DocumentV1$DocV1;

    return-object p0
.end method

.method public setDocV2(Lcom/google/android/finsky/protos/DocumentV2$DocV2;)Lcom/google/android/finsky/protos/Details$DetailsResponse;
    .locals 1
    .param p1    # Lcom/google/android/finsky/protos/DocumentV2$DocV2;

    if-nez p1, :cond_0

    new-instance v0, Ljava/lang/NullPointerException;

    invoke-direct {v0}, Ljava/lang/NullPointerException;-><init>()V

    throw v0

    :cond_0
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/google/android/finsky/protos/Details$DetailsResponse;->hasDocV2:Z

    iput-object p1, p0, Lcom/google/android/finsky/protos/Details$DetailsResponse;->docV2_:Lcom/google/android/finsky/protos/DocumentV2$DocV2;

    return-object p0
.end method

.method public setFooterHtml(Ljava/lang/String;)Lcom/google/android/finsky/protos/Details$DetailsResponse;
    .locals 1
    .param p1    # Ljava/lang/String;

    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/google/android/finsky/protos/Details$DetailsResponse;->hasFooterHtml:Z

    iput-object p1, p0, Lcom/google/android/finsky/protos/Details$DetailsResponse;->footerHtml_:Ljava/lang/String;

    return-object p0
.end method

.method public setServerLogsCookie(Lcom/google/protobuf/micro/ByteStringMicro;)Lcom/google/android/finsky/protos/Details$DetailsResponse;
    .locals 1
    .param p1    # Lcom/google/protobuf/micro/ByteStringMicro;

    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/google/android/finsky/protos/Details$DetailsResponse;->hasServerLogsCookie:Z

    iput-object p1, p0, Lcom/google/android/finsky/protos/Details$DetailsResponse;->serverLogsCookie_:Lcom/google/protobuf/micro/ByteStringMicro;

    return-object p0
.end method

.method public setUserReview(Lcom/google/android/finsky/protos/Rev$Review;)Lcom/google/android/finsky/protos/Details$DetailsResponse;
    .locals 1
    .param p1    # Lcom/google/android/finsky/protos/Rev$Review;

    if-nez p1, :cond_0

    new-instance v0, Ljava/lang/NullPointerException;

    invoke-direct {v0}, Ljava/lang/NullPointerException;-><init>()V

    throw v0

    :cond_0
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/google/android/finsky/protos/Details$DetailsResponse;->hasUserReview:Z

    iput-object p1, p0, Lcom/google/android/finsky/protos/Details$DetailsResponse;->userReview_:Lcom/google/android/finsky/protos/Rev$Review;

    return-object p0
.end method

.method public writeTo(Lcom/google/protobuf/micro/CodedOutputStreamMicro;)V
    .locals 2
    .param p1    # Lcom/google/protobuf/micro/CodedOutputStreamMicro;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    invoke-virtual {p0}, Lcom/google/android/finsky/protos/Details$DetailsResponse;->hasDocV1()Z

    move-result v0

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    invoke-virtual {p0}, Lcom/google/android/finsky/protos/Details$DetailsResponse;->getDocV1()Lcom/google/android/finsky/protos/DocumentV1$DocV1;

    move-result-object v1

    invoke-virtual {p1, v0, v1}, Lcom/google/protobuf/micro/CodedOutputStreamMicro;->writeMessage(ILcom/google/protobuf/micro/MessageMicro;)V

    :cond_0
    invoke-virtual {p0}, Lcom/google/android/finsky/protos/Details$DetailsResponse;->hasAnalyticsCookie()Z

    move-result v0

    if-eqz v0, :cond_1

    const/4 v0, 0x2

    invoke-virtual {p0}, Lcom/google/android/finsky/protos/Details$DetailsResponse;->getAnalyticsCookie()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {p1, v0, v1}, Lcom/google/protobuf/micro/CodedOutputStreamMicro;->writeString(ILjava/lang/String;)V

    :cond_1
    invoke-virtual {p0}, Lcom/google/android/finsky/protos/Details$DetailsResponse;->hasUserReview()Z

    move-result v0

    if-eqz v0, :cond_2

    const/4 v0, 0x3

    invoke-virtual {p0}, Lcom/google/android/finsky/protos/Details$DetailsResponse;->getUserReview()Lcom/google/android/finsky/protos/Rev$Review;

    move-result-object v1

    invoke-virtual {p1, v0, v1}, Lcom/google/protobuf/micro/CodedOutputStreamMicro;->writeMessage(ILcom/google/protobuf/micro/MessageMicro;)V

    :cond_2
    invoke-virtual {p0}, Lcom/google/android/finsky/protos/Details$DetailsResponse;->hasDocV2()Z

    move-result v0

    if-eqz v0, :cond_3

    const/4 v0, 0x4

    invoke-virtual {p0}, Lcom/google/android/finsky/protos/Details$DetailsResponse;->getDocV2()Lcom/google/android/finsky/protos/DocumentV2$DocV2;

    move-result-object v1

    invoke-virtual {p1, v0, v1}, Lcom/google/protobuf/micro/CodedOutputStreamMicro;->writeMessage(ILcom/google/protobuf/micro/MessageMicro;)V

    :cond_3
    invoke-virtual {p0}, Lcom/google/android/finsky/protos/Details$DetailsResponse;->hasFooterHtml()Z

    move-result v0

    if-eqz v0, :cond_4

    const/4 v0, 0x5

    invoke-virtual {p0}, Lcom/google/android/finsky/protos/Details$DetailsResponse;->getFooterHtml()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {p1, v0, v1}, Lcom/google/protobuf/micro/CodedOutputStreamMicro;->writeString(ILjava/lang/String;)V

    :cond_4
    invoke-virtual {p0}, Lcom/google/android/finsky/protos/Details$DetailsResponse;->hasServerLogsCookie()Z

    move-result v0

    if-eqz v0, :cond_5

    const/4 v0, 0x6

    invoke-virtual {p0}, Lcom/google/android/finsky/protos/Details$DetailsResponse;->getServerLogsCookie()Lcom/google/protobuf/micro/ByteStringMicro;

    move-result-object v1

    invoke-virtual {p1, v0, v1}, Lcom/google/protobuf/micro/CodedOutputStreamMicro;->writeBytes(ILcom/google/protobuf/micro/ByteStringMicro;)V

    :cond_5
    return-void
.end method
