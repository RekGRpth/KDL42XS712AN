.class public final Lcom/google/android/finsky/protos/CommonDevice$Instrument;
.super Lcom/google/protobuf/micro/MessageMicro;
.source "CommonDevice.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/google/android/finsky/protos/CommonDevice;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x19
    name = "Instrument"
.end annotation


# instance fields
.field private billingAddressSpec_:Lcom/google/android/finsky/protos/CommonDevice$BillingAddressSpec;

.field private billingAddress_:Lcom/google/android/finsky/protos/BillingAddress$Address;

.field private cachedSize:I

.field private carrierBillingStatus_:Lcom/google/android/finsky/protos/CommonDevice$CarrierBillingInstrumentStatus;

.field private carrierBilling_:Lcom/google/android/finsky/protos/CommonDevice$CarrierBillingInstrument;

.field private creditCard_:Lcom/google/android/finsky/protos/CommonDevice$CreditCardInstrument;

.field private disabledInfo_:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Lcom/google/android/finsky/protos/CommonDevice$DisabledInfo;",
            ">;"
        }
    .end annotation
.end field

.field private displayTitle_:Ljava/lang/String;

.field private externalInstrumentId_:Ljava/lang/String;

.field private hasBillingAddress:Z

.field private hasBillingAddressSpec:Z

.field private hasCarrierBilling:Z

.field private hasCarrierBillingStatus:Z

.field private hasCreditCard:Z

.field private hasDisplayTitle:Z

.field private hasExternalInstrumentId:Z

.field private hasInstrumentFamily:Z

.field private hasStoredValue:Z

.field private hasTopupInfoDeprecated:Z

.field private hasVersion:Z

.field private instrumentFamily_:I

.field private storedValue_:Lcom/google/android/finsky/protos/CommonDevice$StoredValueInstrument;

.field private topupInfoDeprecated_:Lcom/google/android/finsky/protos/CommonDevice$TopupInfo;

.field private version_:I


# direct methods
.method public constructor <init>()V
    .locals 3

    const/4 v2, 0x0

    const/4 v1, 0x0

    invoke-direct {p0}, Lcom/google/protobuf/micro/MessageMicro;-><init>()V

    const-string v0, ""

    iput-object v0, p0, Lcom/google/android/finsky/protos/CommonDevice$Instrument;->externalInstrumentId_:Ljava/lang/String;

    const-string v0, ""

    iput-object v0, p0, Lcom/google/android/finsky/protos/CommonDevice$Instrument;->displayTitle_:Ljava/lang/String;

    iput v2, p0, Lcom/google/android/finsky/protos/CommonDevice$Instrument;->instrumentFamily_:I

    iput-object v1, p0, Lcom/google/android/finsky/protos/CommonDevice$Instrument;->billingAddress_:Lcom/google/android/finsky/protos/BillingAddress$Address;

    iput-object v1, p0, Lcom/google/android/finsky/protos/CommonDevice$Instrument;->billingAddressSpec_:Lcom/google/android/finsky/protos/CommonDevice$BillingAddressSpec;

    iput-object v1, p0, Lcom/google/android/finsky/protos/CommonDevice$Instrument;->creditCard_:Lcom/google/android/finsky/protos/CommonDevice$CreditCardInstrument;

    iput-object v1, p0, Lcom/google/android/finsky/protos/CommonDevice$Instrument;->carrierBilling_:Lcom/google/android/finsky/protos/CommonDevice$CarrierBillingInstrument;

    iput-object v1, p0, Lcom/google/android/finsky/protos/CommonDevice$Instrument;->carrierBillingStatus_:Lcom/google/android/finsky/protos/CommonDevice$CarrierBillingInstrumentStatus;

    iput-object v1, p0, Lcom/google/android/finsky/protos/CommonDevice$Instrument;->storedValue_:Lcom/google/android/finsky/protos/CommonDevice$StoredValueInstrument;

    iput-object v1, p0, Lcom/google/android/finsky/protos/CommonDevice$Instrument;->topupInfoDeprecated_:Lcom/google/android/finsky/protos/CommonDevice$TopupInfo;

    iput v2, p0, Lcom/google/android/finsky/protos/CommonDevice$Instrument;->version_:I

    invoke-static {}, Ljava/util/Collections;->emptyList()Ljava/util/List;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/finsky/protos/CommonDevice$Instrument;->disabledInfo_:Ljava/util/List;

    const/4 v0, -0x1

    iput v0, p0, Lcom/google/android/finsky/protos/CommonDevice$Instrument;->cachedSize:I

    return-void
.end method


# virtual methods
.method public addDisabledInfo(Lcom/google/android/finsky/protos/CommonDevice$DisabledInfo;)Lcom/google/android/finsky/protos/CommonDevice$Instrument;
    .locals 1
    .param p1    # Lcom/google/android/finsky/protos/CommonDevice$DisabledInfo;

    if-nez p1, :cond_0

    new-instance v0, Ljava/lang/NullPointerException;

    invoke-direct {v0}, Ljava/lang/NullPointerException;-><init>()V

    throw v0

    :cond_0
    iget-object v0, p0, Lcom/google/android/finsky/protos/CommonDevice$Instrument;->disabledInfo_:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->isEmpty()Z

    move-result v0

    if-eqz v0, :cond_1

    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, Lcom/google/android/finsky/protos/CommonDevice$Instrument;->disabledInfo_:Ljava/util/List;

    :cond_1
    iget-object v0, p0, Lcom/google/android/finsky/protos/CommonDevice$Instrument;->disabledInfo_:Ljava/util/List;

    invoke-interface {v0, p1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    return-object p0
.end method

.method public getBillingAddress()Lcom/google/android/finsky/protos/BillingAddress$Address;
    .locals 1

    iget-object v0, p0, Lcom/google/android/finsky/protos/CommonDevice$Instrument;->billingAddress_:Lcom/google/android/finsky/protos/BillingAddress$Address;

    return-object v0
.end method

.method public getBillingAddressSpec()Lcom/google/android/finsky/protos/CommonDevice$BillingAddressSpec;
    .locals 1

    iget-object v0, p0, Lcom/google/android/finsky/protos/CommonDevice$Instrument;->billingAddressSpec_:Lcom/google/android/finsky/protos/CommonDevice$BillingAddressSpec;

    return-object v0
.end method

.method public getCachedSize()I
    .locals 1

    iget v0, p0, Lcom/google/android/finsky/protos/CommonDevice$Instrument;->cachedSize:I

    if-gez v0, :cond_0

    invoke-virtual {p0}, Lcom/google/android/finsky/protos/CommonDevice$Instrument;->getSerializedSize()I

    :cond_0
    iget v0, p0, Lcom/google/android/finsky/protos/CommonDevice$Instrument;->cachedSize:I

    return v0
.end method

.method public getCarrierBilling()Lcom/google/android/finsky/protos/CommonDevice$CarrierBillingInstrument;
    .locals 1

    iget-object v0, p0, Lcom/google/android/finsky/protos/CommonDevice$Instrument;->carrierBilling_:Lcom/google/android/finsky/protos/CommonDevice$CarrierBillingInstrument;

    return-object v0
.end method

.method public getCarrierBillingStatus()Lcom/google/android/finsky/protos/CommonDevice$CarrierBillingInstrumentStatus;
    .locals 1

    iget-object v0, p0, Lcom/google/android/finsky/protos/CommonDevice$Instrument;->carrierBillingStatus_:Lcom/google/android/finsky/protos/CommonDevice$CarrierBillingInstrumentStatus;

    return-object v0
.end method

.method public getCreditCard()Lcom/google/android/finsky/protos/CommonDevice$CreditCardInstrument;
    .locals 1

    iget-object v0, p0, Lcom/google/android/finsky/protos/CommonDevice$Instrument;->creditCard_:Lcom/google/android/finsky/protos/CommonDevice$CreditCardInstrument;

    return-object v0
.end method

.method public getDisabledInfo(I)Lcom/google/android/finsky/protos/CommonDevice$DisabledInfo;
    .locals 1
    .param p1    # I

    iget-object v0, p0, Lcom/google/android/finsky/protos/CommonDevice$Instrument;->disabledInfo_:Ljava/util/List;

    invoke-interface {v0, p1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/finsky/protos/CommonDevice$DisabledInfo;

    return-object v0
.end method

.method public getDisabledInfoCount()I
    .locals 1

    iget-object v0, p0, Lcom/google/android/finsky/protos/CommonDevice$Instrument;->disabledInfo_:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->size()I

    move-result v0

    return v0
.end method

.method public getDisabledInfoList()Ljava/util/List;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/List",
            "<",
            "Lcom/google/android/finsky/protos/CommonDevice$DisabledInfo;",
            ">;"
        }
    .end annotation

    iget-object v0, p0, Lcom/google/android/finsky/protos/CommonDevice$Instrument;->disabledInfo_:Ljava/util/List;

    return-object v0
.end method

.method public getDisplayTitle()Ljava/lang/String;
    .locals 1

    iget-object v0, p0, Lcom/google/android/finsky/protos/CommonDevice$Instrument;->displayTitle_:Ljava/lang/String;

    return-object v0
.end method

.method public getExternalInstrumentId()Ljava/lang/String;
    .locals 1

    iget-object v0, p0, Lcom/google/android/finsky/protos/CommonDevice$Instrument;->externalInstrumentId_:Ljava/lang/String;

    return-object v0
.end method

.method public getInstrumentFamily()I
    .locals 1

    iget v0, p0, Lcom/google/android/finsky/protos/CommonDevice$Instrument;->instrumentFamily_:I

    return v0
.end method

.method public getSerializedSize()I
    .locals 5

    const/4 v2, 0x0

    invoke-virtual {p0}, Lcom/google/android/finsky/protos/CommonDevice$Instrument;->hasExternalInstrumentId()Z

    move-result v3

    if-eqz v3, :cond_0

    const/4 v3, 0x1

    invoke-virtual {p0}, Lcom/google/android/finsky/protos/CommonDevice$Instrument;->getExternalInstrumentId()Ljava/lang/String;

    move-result-object v4

    invoke-static {v3, v4}, Lcom/google/protobuf/micro/CodedOutputStreamMicro;->computeStringSize(ILjava/lang/String;)I

    move-result v3

    add-int/2addr v2, v3

    :cond_0
    invoke-virtual {p0}, Lcom/google/android/finsky/protos/CommonDevice$Instrument;->hasBillingAddress()Z

    move-result v3

    if-eqz v3, :cond_1

    const/4 v3, 0x2

    invoke-virtual {p0}, Lcom/google/android/finsky/protos/CommonDevice$Instrument;->getBillingAddress()Lcom/google/android/finsky/protos/BillingAddress$Address;

    move-result-object v4

    invoke-static {v3, v4}, Lcom/google/protobuf/micro/CodedOutputStreamMicro;->computeMessageSize(ILcom/google/protobuf/micro/MessageMicro;)I

    move-result v3

    add-int/2addr v2, v3

    :cond_1
    invoke-virtual {p0}, Lcom/google/android/finsky/protos/CommonDevice$Instrument;->hasCreditCard()Z

    move-result v3

    if-eqz v3, :cond_2

    const/4 v3, 0x3

    invoke-virtual {p0}, Lcom/google/android/finsky/protos/CommonDevice$Instrument;->getCreditCard()Lcom/google/android/finsky/protos/CommonDevice$CreditCardInstrument;

    move-result-object v4

    invoke-static {v3, v4}, Lcom/google/protobuf/micro/CodedOutputStreamMicro;->computeMessageSize(ILcom/google/protobuf/micro/MessageMicro;)I

    move-result v3

    add-int/2addr v2, v3

    :cond_2
    invoke-virtual {p0}, Lcom/google/android/finsky/protos/CommonDevice$Instrument;->hasCarrierBilling()Z

    move-result v3

    if-eqz v3, :cond_3

    const/4 v3, 0x4

    invoke-virtual {p0}, Lcom/google/android/finsky/protos/CommonDevice$Instrument;->getCarrierBilling()Lcom/google/android/finsky/protos/CommonDevice$CarrierBillingInstrument;

    move-result-object v4

    invoke-static {v3, v4}, Lcom/google/protobuf/micro/CodedOutputStreamMicro;->computeMessageSize(ILcom/google/protobuf/micro/MessageMicro;)I

    move-result v3

    add-int/2addr v2, v3

    :cond_3
    invoke-virtual {p0}, Lcom/google/android/finsky/protos/CommonDevice$Instrument;->hasBillingAddressSpec()Z

    move-result v3

    if-eqz v3, :cond_4

    const/4 v3, 0x5

    invoke-virtual {p0}, Lcom/google/android/finsky/protos/CommonDevice$Instrument;->getBillingAddressSpec()Lcom/google/android/finsky/protos/CommonDevice$BillingAddressSpec;

    move-result-object v4

    invoke-static {v3, v4}, Lcom/google/protobuf/micro/CodedOutputStreamMicro;->computeMessageSize(ILcom/google/protobuf/micro/MessageMicro;)I

    move-result v3

    add-int/2addr v2, v3

    :cond_4
    invoke-virtual {p0}, Lcom/google/android/finsky/protos/CommonDevice$Instrument;->hasInstrumentFamily()Z

    move-result v3

    if-eqz v3, :cond_5

    const/4 v3, 0x6

    invoke-virtual {p0}, Lcom/google/android/finsky/protos/CommonDevice$Instrument;->getInstrumentFamily()I

    move-result v4

    invoke-static {v3, v4}, Lcom/google/protobuf/micro/CodedOutputStreamMicro;->computeInt32Size(II)I

    move-result v3

    add-int/2addr v2, v3

    :cond_5
    invoke-virtual {p0}, Lcom/google/android/finsky/protos/CommonDevice$Instrument;->hasCarrierBillingStatus()Z

    move-result v3

    if-eqz v3, :cond_6

    const/4 v3, 0x7

    invoke-virtual {p0}, Lcom/google/android/finsky/protos/CommonDevice$Instrument;->getCarrierBillingStatus()Lcom/google/android/finsky/protos/CommonDevice$CarrierBillingInstrumentStatus;

    move-result-object v4

    invoke-static {v3, v4}, Lcom/google/protobuf/micro/CodedOutputStreamMicro;->computeMessageSize(ILcom/google/protobuf/micro/MessageMicro;)I

    move-result v3

    add-int/2addr v2, v3

    :cond_6
    invoke-virtual {p0}, Lcom/google/android/finsky/protos/CommonDevice$Instrument;->hasDisplayTitle()Z

    move-result v3

    if-eqz v3, :cond_7

    const/16 v3, 0x8

    invoke-virtual {p0}, Lcom/google/android/finsky/protos/CommonDevice$Instrument;->getDisplayTitle()Ljava/lang/String;

    move-result-object v4

    invoke-static {v3, v4}, Lcom/google/protobuf/micro/CodedOutputStreamMicro;->computeStringSize(ILjava/lang/String;)I

    move-result v3

    add-int/2addr v2, v3

    :cond_7
    invoke-virtual {p0}, Lcom/google/android/finsky/protos/CommonDevice$Instrument;->hasTopupInfoDeprecated()Z

    move-result v3

    if-eqz v3, :cond_8

    const/16 v3, 0x9

    invoke-virtual {p0}, Lcom/google/android/finsky/protos/CommonDevice$Instrument;->getTopupInfoDeprecated()Lcom/google/android/finsky/protos/CommonDevice$TopupInfo;

    move-result-object v4

    invoke-static {v3, v4}, Lcom/google/protobuf/micro/CodedOutputStreamMicro;->computeMessageSize(ILcom/google/protobuf/micro/MessageMicro;)I

    move-result v3

    add-int/2addr v2, v3

    :cond_8
    invoke-virtual {p0}, Lcom/google/android/finsky/protos/CommonDevice$Instrument;->hasVersion()Z

    move-result v3

    if-eqz v3, :cond_9

    const/16 v3, 0xa

    invoke-virtual {p0}, Lcom/google/android/finsky/protos/CommonDevice$Instrument;->getVersion()I

    move-result v4

    invoke-static {v3, v4}, Lcom/google/protobuf/micro/CodedOutputStreamMicro;->computeInt32Size(II)I

    move-result v3

    add-int/2addr v2, v3

    :cond_9
    invoke-virtual {p0}, Lcom/google/android/finsky/protos/CommonDevice$Instrument;->hasStoredValue()Z

    move-result v3

    if-eqz v3, :cond_a

    const/16 v3, 0xb

    invoke-virtual {p0}, Lcom/google/android/finsky/protos/CommonDevice$Instrument;->getStoredValue()Lcom/google/android/finsky/protos/CommonDevice$StoredValueInstrument;

    move-result-object v4

    invoke-static {v3, v4}, Lcom/google/protobuf/micro/CodedOutputStreamMicro;->computeMessageSize(ILcom/google/protobuf/micro/MessageMicro;)I

    move-result v3

    add-int/2addr v2, v3

    :cond_a
    invoke-virtual {p0}, Lcom/google/android/finsky/protos/CommonDevice$Instrument;->getDisabledInfoList()Ljava/util/List;

    move-result-object v3

    invoke-interface {v3}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :goto_0
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v3

    if-eqz v3, :cond_b

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/finsky/protos/CommonDevice$DisabledInfo;

    const/16 v3, 0xc

    invoke-static {v3, v0}, Lcom/google/protobuf/micro/CodedOutputStreamMicro;->computeMessageSize(ILcom/google/protobuf/micro/MessageMicro;)I

    move-result v3

    add-int/2addr v2, v3

    goto :goto_0

    :cond_b
    iput v2, p0, Lcom/google/android/finsky/protos/CommonDevice$Instrument;->cachedSize:I

    return v2
.end method

.method public getStoredValue()Lcom/google/android/finsky/protos/CommonDevice$StoredValueInstrument;
    .locals 1

    iget-object v0, p0, Lcom/google/android/finsky/protos/CommonDevice$Instrument;->storedValue_:Lcom/google/android/finsky/protos/CommonDevice$StoredValueInstrument;

    return-object v0
.end method

.method public getTopupInfoDeprecated()Lcom/google/android/finsky/protos/CommonDevice$TopupInfo;
    .locals 1

    iget-object v0, p0, Lcom/google/android/finsky/protos/CommonDevice$Instrument;->topupInfoDeprecated_:Lcom/google/android/finsky/protos/CommonDevice$TopupInfo;

    return-object v0
.end method

.method public getVersion()I
    .locals 1

    iget v0, p0, Lcom/google/android/finsky/protos/CommonDevice$Instrument;->version_:I

    return v0
.end method

.method public hasBillingAddress()Z
    .locals 1

    iget-boolean v0, p0, Lcom/google/android/finsky/protos/CommonDevice$Instrument;->hasBillingAddress:Z

    return v0
.end method

.method public hasBillingAddressSpec()Z
    .locals 1

    iget-boolean v0, p0, Lcom/google/android/finsky/protos/CommonDevice$Instrument;->hasBillingAddressSpec:Z

    return v0
.end method

.method public hasCarrierBilling()Z
    .locals 1

    iget-boolean v0, p0, Lcom/google/android/finsky/protos/CommonDevice$Instrument;->hasCarrierBilling:Z

    return v0
.end method

.method public hasCarrierBillingStatus()Z
    .locals 1

    iget-boolean v0, p0, Lcom/google/android/finsky/protos/CommonDevice$Instrument;->hasCarrierBillingStatus:Z

    return v0
.end method

.method public hasCreditCard()Z
    .locals 1

    iget-boolean v0, p0, Lcom/google/android/finsky/protos/CommonDevice$Instrument;->hasCreditCard:Z

    return v0
.end method

.method public hasDisplayTitle()Z
    .locals 1

    iget-boolean v0, p0, Lcom/google/android/finsky/protos/CommonDevice$Instrument;->hasDisplayTitle:Z

    return v0
.end method

.method public hasExternalInstrumentId()Z
    .locals 1

    iget-boolean v0, p0, Lcom/google/android/finsky/protos/CommonDevice$Instrument;->hasExternalInstrumentId:Z

    return v0
.end method

.method public hasInstrumentFamily()Z
    .locals 1

    iget-boolean v0, p0, Lcom/google/android/finsky/protos/CommonDevice$Instrument;->hasInstrumentFamily:Z

    return v0
.end method

.method public hasStoredValue()Z
    .locals 1

    iget-boolean v0, p0, Lcom/google/android/finsky/protos/CommonDevice$Instrument;->hasStoredValue:Z

    return v0
.end method

.method public hasTopupInfoDeprecated()Z
    .locals 1

    iget-boolean v0, p0, Lcom/google/android/finsky/protos/CommonDevice$Instrument;->hasTopupInfoDeprecated:Z

    return v0
.end method

.method public hasVersion()Z
    .locals 1

    iget-boolean v0, p0, Lcom/google/android/finsky/protos/CommonDevice$Instrument;->hasVersion:Z

    return v0
.end method

.method public mergeFrom(Lcom/google/protobuf/micro/CodedInputStreamMicro;)Lcom/google/android/finsky/protos/CommonDevice$Instrument;
    .locals 3
    .param p1    # Lcom/google/protobuf/micro/CodedInputStreamMicro;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    :cond_0
    :goto_0
    invoke-virtual {p1}, Lcom/google/protobuf/micro/CodedInputStreamMicro;->readTag()I

    move-result v0

    sparse-switch v0, :sswitch_data_0

    invoke-virtual {p0, p1, v0}, Lcom/google/android/finsky/protos/CommonDevice$Instrument;->parseUnknownField(Lcom/google/protobuf/micro/CodedInputStreamMicro;I)Z

    move-result v2

    if-nez v2, :cond_0

    :sswitch_0
    return-object p0

    :sswitch_1
    invoke-virtual {p1}, Lcom/google/protobuf/micro/CodedInputStreamMicro;->readString()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {p0, v2}, Lcom/google/android/finsky/protos/CommonDevice$Instrument;->setExternalInstrumentId(Ljava/lang/String;)Lcom/google/android/finsky/protos/CommonDevice$Instrument;

    goto :goto_0

    :sswitch_2
    new-instance v1, Lcom/google/android/finsky/protos/BillingAddress$Address;

    invoke-direct {v1}, Lcom/google/android/finsky/protos/BillingAddress$Address;-><init>()V

    invoke-virtual {p1, v1}, Lcom/google/protobuf/micro/CodedInputStreamMicro;->readMessage(Lcom/google/protobuf/micro/MessageMicro;)V

    invoke-virtual {p0, v1}, Lcom/google/android/finsky/protos/CommonDevice$Instrument;->setBillingAddress(Lcom/google/android/finsky/protos/BillingAddress$Address;)Lcom/google/android/finsky/protos/CommonDevice$Instrument;

    goto :goto_0

    :sswitch_3
    new-instance v1, Lcom/google/android/finsky/protos/CommonDevice$CreditCardInstrument;

    invoke-direct {v1}, Lcom/google/android/finsky/protos/CommonDevice$CreditCardInstrument;-><init>()V

    invoke-virtual {p1, v1}, Lcom/google/protobuf/micro/CodedInputStreamMicro;->readMessage(Lcom/google/protobuf/micro/MessageMicro;)V

    invoke-virtual {p0, v1}, Lcom/google/android/finsky/protos/CommonDevice$Instrument;->setCreditCard(Lcom/google/android/finsky/protos/CommonDevice$CreditCardInstrument;)Lcom/google/android/finsky/protos/CommonDevice$Instrument;

    goto :goto_0

    :sswitch_4
    new-instance v1, Lcom/google/android/finsky/protos/CommonDevice$CarrierBillingInstrument;

    invoke-direct {v1}, Lcom/google/android/finsky/protos/CommonDevice$CarrierBillingInstrument;-><init>()V

    invoke-virtual {p1, v1}, Lcom/google/protobuf/micro/CodedInputStreamMicro;->readMessage(Lcom/google/protobuf/micro/MessageMicro;)V

    invoke-virtual {p0, v1}, Lcom/google/android/finsky/protos/CommonDevice$Instrument;->setCarrierBilling(Lcom/google/android/finsky/protos/CommonDevice$CarrierBillingInstrument;)Lcom/google/android/finsky/protos/CommonDevice$Instrument;

    goto :goto_0

    :sswitch_5
    new-instance v1, Lcom/google/android/finsky/protos/CommonDevice$BillingAddressSpec;

    invoke-direct {v1}, Lcom/google/android/finsky/protos/CommonDevice$BillingAddressSpec;-><init>()V

    invoke-virtual {p1, v1}, Lcom/google/protobuf/micro/CodedInputStreamMicro;->readMessage(Lcom/google/protobuf/micro/MessageMicro;)V

    invoke-virtual {p0, v1}, Lcom/google/android/finsky/protos/CommonDevice$Instrument;->setBillingAddressSpec(Lcom/google/android/finsky/protos/CommonDevice$BillingAddressSpec;)Lcom/google/android/finsky/protos/CommonDevice$Instrument;

    goto :goto_0

    :sswitch_6
    invoke-virtual {p1}, Lcom/google/protobuf/micro/CodedInputStreamMicro;->readInt32()I

    move-result v2

    invoke-virtual {p0, v2}, Lcom/google/android/finsky/protos/CommonDevice$Instrument;->setInstrumentFamily(I)Lcom/google/android/finsky/protos/CommonDevice$Instrument;

    goto :goto_0

    :sswitch_7
    new-instance v1, Lcom/google/android/finsky/protos/CommonDevice$CarrierBillingInstrumentStatus;

    invoke-direct {v1}, Lcom/google/android/finsky/protos/CommonDevice$CarrierBillingInstrumentStatus;-><init>()V

    invoke-virtual {p1, v1}, Lcom/google/protobuf/micro/CodedInputStreamMicro;->readMessage(Lcom/google/protobuf/micro/MessageMicro;)V

    invoke-virtual {p0, v1}, Lcom/google/android/finsky/protos/CommonDevice$Instrument;->setCarrierBillingStatus(Lcom/google/android/finsky/protos/CommonDevice$CarrierBillingInstrumentStatus;)Lcom/google/android/finsky/protos/CommonDevice$Instrument;

    goto :goto_0

    :sswitch_8
    invoke-virtual {p1}, Lcom/google/protobuf/micro/CodedInputStreamMicro;->readString()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {p0, v2}, Lcom/google/android/finsky/protos/CommonDevice$Instrument;->setDisplayTitle(Ljava/lang/String;)Lcom/google/android/finsky/protos/CommonDevice$Instrument;

    goto :goto_0

    :sswitch_9
    new-instance v1, Lcom/google/android/finsky/protos/CommonDevice$TopupInfo;

    invoke-direct {v1}, Lcom/google/android/finsky/protos/CommonDevice$TopupInfo;-><init>()V

    invoke-virtual {p1, v1}, Lcom/google/protobuf/micro/CodedInputStreamMicro;->readMessage(Lcom/google/protobuf/micro/MessageMicro;)V

    invoke-virtual {p0, v1}, Lcom/google/android/finsky/protos/CommonDevice$Instrument;->setTopupInfoDeprecated(Lcom/google/android/finsky/protos/CommonDevice$TopupInfo;)Lcom/google/android/finsky/protos/CommonDevice$Instrument;

    goto :goto_0

    :sswitch_a
    invoke-virtual {p1}, Lcom/google/protobuf/micro/CodedInputStreamMicro;->readInt32()I

    move-result v2

    invoke-virtual {p0, v2}, Lcom/google/android/finsky/protos/CommonDevice$Instrument;->setVersion(I)Lcom/google/android/finsky/protos/CommonDevice$Instrument;

    goto :goto_0

    :sswitch_b
    new-instance v1, Lcom/google/android/finsky/protos/CommonDevice$StoredValueInstrument;

    invoke-direct {v1}, Lcom/google/android/finsky/protos/CommonDevice$StoredValueInstrument;-><init>()V

    invoke-virtual {p1, v1}, Lcom/google/protobuf/micro/CodedInputStreamMicro;->readMessage(Lcom/google/protobuf/micro/MessageMicro;)V

    invoke-virtual {p0, v1}, Lcom/google/android/finsky/protos/CommonDevice$Instrument;->setStoredValue(Lcom/google/android/finsky/protos/CommonDevice$StoredValueInstrument;)Lcom/google/android/finsky/protos/CommonDevice$Instrument;

    goto/16 :goto_0

    :sswitch_c
    new-instance v1, Lcom/google/android/finsky/protos/CommonDevice$DisabledInfo;

    invoke-direct {v1}, Lcom/google/android/finsky/protos/CommonDevice$DisabledInfo;-><init>()V

    invoke-virtual {p1, v1}, Lcom/google/protobuf/micro/CodedInputStreamMicro;->readMessage(Lcom/google/protobuf/micro/MessageMicro;)V

    invoke-virtual {p0, v1}, Lcom/google/android/finsky/protos/CommonDevice$Instrument;->addDisabledInfo(Lcom/google/android/finsky/protos/CommonDevice$DisabledInfo;)Lcom/google/android/finsky/protos/CommonDevice$Instrument;

    goto/16 :goto_0

    :sswitch_data_0
    .sparse-switch
        0x0 -> :sswitch_0
        0xa -> :sswitch_1
        0x12 -> :sswitch_2
        0x1a -> :sswitch_3
        0x22 -> :sswitch_4
        0x2a -> :sswitch_5
        0x30 -> :sswitch_6
        0x3a -> :sswitch_7
        0x42 -> :sswitch_8
        0x4a -> :sswitch_9
        0x50 -> :sswitch_a
        0x5a -> :sswitch_b
        0x62 -> :sswitch_c
    .end sparse-switch
.end method

.method public bridge synthetic mergeFrom(Lcom/google/protobuf/micro/CodedInputStreamMicro;)Lcom/google/protobuf/micro/MessageMicro;
    .locals 1
    .param p1    # Lcom/google/protobuf/micro/CodedInputStreamMicro;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    invoke-virtual {p0, p1}, Lcom/google/android/finsky/protos/CommonDevice$Instrument;->mergeFrom(Lcom/google/protobuf/micro/CodedInputStreamMicro;)Lcom/google/android/finsky/protos/CommonDevice$Instrument;

    move-result-object v0

    return-object v0
.end method

.method public setBillingAddress(Lcom/google/android/finsky/protos/BillingAddress$Address;)Lcom/google/android/finsky/protos/CommonDevice$Instrument;
    .locals 1
    .param p1    # Lcom/google/android/finsky/protos/BillingAddress$Address;

    if-nez p1, :cond_0

    new-instance v0, Ljava/lang/NullPointerException;

    invoke-direct {v0}, Ljava/lang/NullPointerException;-><init>()V

    throw v0

    :cond_0
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/google/android/finsky/protos/CommonDevice$Instrument;->hasBillingAddress:Z

    iput-object p1, p0, Lcom/google/android/finsky/protos/CommonDevice$Instrument;->billingAddress_:Lcom/google/android/finsky/protos/BillingAddress$Address;

    return-object p0
.end method

.method public setBillingAddressSpec(Lcom/google/android/finsky/protos/CommonDevice$BillingAddressSpec;)Lcom/google/android/finsky/protos/CommonDevice$Instrument;
    .locals 1
    .param p1    # Lcom/google/android/finsky/protos/CommonDevice$BillingAddressSpec;

    if-nez p1, :cond_0

    new-instance v0, Ljava/lang/NullPointerException;

    invoke-direct {v0}, Ljava/lang/NullPointerException;-><init>()V

    throw v0

    :cond_0
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/google/android/finsky/protos/CommonDevice$Instrument;->hasBillingAddressSpec:Z

    iput-object p1, p0, Lcom/google/android/finsky/protos/CommonDevice$Instrument;->billingAddressSpec_:Lcom/google/android/finsky/protos/CommonDevice$BillingAddressSpec;

    return-object p0
.end method

.method public setCarrierBilling(Lcom/google/android/finsky/protos/CommonDevice$CarrierBillingInstrument;)Lcom/google/android/finsky/protos/CommonDevice$Instrument;
    .locals 1
    .param p1    # Lcom/google/android/finsky/protos/CommonDevice$CarrierBillingInstrument;

    if-nez p1, :cond_0

    new-instance v0, Ljava/lang/NullPointerException;

    invoke-direct {v0}, Ljava/lang/NullPointerException;-><init>()V

    throw v0

    :cond_0
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/google/android/finsky/protos/CommonDevice$Instrument;->hasCarrierBilling:Z

    iput-object p1, p0, Lcom/google/android/finsky/protos/CommonDevice$Instrument;->carrierBilling_:Lcom/google/android/finsky/protos/CommonDevice$CarrierBillingInstrument;

    return-object p0
.end method

.method public setCarrierBillingStatus(Lcom/google/android/finsky/protos/CommonDevice$CarrierBillingInstrumentStatus;)Lcom/google/android/finsky/protos/CommonDevice$Instrument;
    .locals 1
    .param p1    # Lcom/google/android/finsky/protos/CommonDevice$CarrierBillingInstrumentStatus;

    if-nez p1, :cond_0

    new-instance v0, Ljava/lang/NullPointerException;

    invoke-direct {v0}, Ljava/lang/NullPointerException;-><init>()V

    throw v0

    :cond_0
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/google/android/finsky/protos/CommonDevice$Instrument;->hasCarrierBillingStatus:Z

    iput-object p1, p0, Lcom/google/android/finsky/protos/CommonDevice$Instrument;->carrierBillingStatus_:Lcom/google/android/finsky/protos/CommonDevice$CarrierBillingInstrumentStatus;

    return-object p0
.end method

.method public setCreditCard(Lcom/google/android/finsky/protos/CommonDevice$CreditCardInstrument;)Lcom/google/android/finsky/protos/CommonDevice$Instrument;
    .locals 1
    .param p1    # Lcom/google/android/finsky/protos/CommonDevice$CreditCardInstrument;

    if-nez p1, :cond_0

    new-instance v0, Ljava/lang/NullPointerException;

    invoke-direct {v0}, Ljava/lang/NullPointerException;-><init>()V

    throw v0

    :cond_0
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/google/android/finsky/protos/CommonDevice$Instrument;->hasCreditCard:Z

    iput-object p1, p0, Lcom/google/android/finsky/protos/CommonDevice$Instrument;->creditCard_:Lcom/google/android/finsky/protos/CommonDevice$CreditCardInstrument;

    return-object p0
.end method

.method public setDisplayTitle(Ljava/lang/String;)Lcom/google/android/finsky/protos/CommonDevice$Instrument;
    .locals 1
    .param p1    # Ljava/lang/String;

    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/google/android/finsky/protos/CommonDevice$Instrument;->hasDisplayTitle:Z

    iput-object p1, p0, Lcom/google/android/finsky/protos/CommonDevice$Instrument;->displayTitle_:Ljava/lang/String;

    return-object p0
.end method

.method public setExternalInstrumentId(Ljava/lang/String;)Lcom/google/android/finsky/protos/CommonDevice$Instrument;
    .locals 1
    .param p1    # Ljava/lang/String;

    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/google/android/finsky/protos/CommonDevice$Instrument;->hasExternalInstrumentId:Z

    iput-object p1, p0, Lcom/google/android/finsky/protos/CommonDevice$Instrument;->externalInstrumentId_:Ljava/lang/String;

    return-object p0
.end method

.method public setInstrumentFamily(I)Lcom/google/android/finsky/protos/CommonDevice$Instrument;
    .locals 1
    .param p1    # I

    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/google/android/finsky/protos/CommonDevice$Instrument;->hasInstrumentFamily:Z

    iput p1, p0, Lcom/google/android/finsky/protos/CommonDevice$Instrument;->instrumentFamily_:I

    return-object p0
.end method

.method public setStoredValue(Lcom/google/android/finsky/protos/CommonDevice$StoredValueInstrument;)Lcom/google/android/finsky/protos/CommonDevice$Instrument;
    .locals 1
    .param p1    # Lcom/google/android/finsky/protos/CommonDevice$StoredValueInstrument;

    if-nez p1, :cond_0

    new-instance v0, Ljava/lang/NullPointerException;

    invoke-direct {v0}, Ljava/lang/NullPointerException;-><init>()V

    throw v0

    :cond_0
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/google/android/finsky/protos/CommonDevice$Instrument;->hasStoredValue:Z

    iput-object p1, p0, Lcom/google/android/finsky/protos/CommonDevice$Instrument;->storedValue_:Lcom/google/android/finsky/protos/CommonDevice$StoredValueInstrument;

    return-object p0
.end method

.method public setTopupInfoDeprecated(Lcom/google/android/finsky/protos/CommonDevice$TopupInfo;)Lcom/google/android/finsky/protos/CommonDevice$Instrument;
    .locals 1
    .param p1    # Lcom/google/android/finsky/protos/CommonDevice$TopupInfo;

    if-nez p1, :cond_0

    new-instance v0, Ljava/lang/NullPointerException;

    invoke-direct {v0}, Ljava/lang/NullPointerException;-><init>()V

    throw v0

    :cond_0
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/google/android/finsky/protos/CommonDevice$Instrument;->hasTopupInfoDeprecated:Z

    iput-object p1, p0, Lcom/google/android/finsky/protos/CommonDevice$Instrument;->topupInfoDeprecated_:Lcom/google/android/finsky/protos/CommonDevice$TopupInfo;

    return-object p0
.end method

.method public setVersion(I)Lcom/google/android/finsky/protos/CommonDevice$Instrument;
    .locals 1
    .param p1    # I

    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/google/android/finsky/protos/CommonDevice$Instrument;->hasVersion:Z

    iput p1, p0, Lcom/google/android/finsky/protos/CommonDevice$Instrument;->version_:I

    return-object p0
.end method

.method public writeTo(Lcom/google/protobuf/micro/CodedOutputStreamMicro;)V
    .locals 4
    .param p1    # Lcom/google/protobuf/micro/CodedOutputStreamMicro;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    invoke-virtual {p0}, Lcom/google/android/finsky/protos/CommonDevice$Instrument;->hasExternalInstrumentId()Z

    move-result v2

    if-eqz v2, :cond_0

    const/4 v2, 0x1

    invoke-virtual {p0}, Lcom/google/android/finsky/protos/CommonDevice$Instrument;->getExternalInstrumentId()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {p1, v2, v3}, Lcom/google/protobuf/micro/CodedOutputStreamMicro;->writeString(ILjava/lang/String;)V

    :cond_0
    invoke-virtual {p0}, Lcom/google/android/finsky/protos/CommonDevice$Instrument;->hasBillingAddress()Z

    move-result v2

    if-eqz v2, :cond_1

    const/4 v2, 0x2

    invoke-virtual {p0}, Lcom/google/android/finsky/protos/CommonDevice$Instrument;->getBillingAddress()Lcom/google/android/finsky/protos/BillingAddress$Address;

    move-result-object v3

    invoke-virtual {p1, v2, v3}, Lcom/google/protobuf/micro/CodedOutputStreamMicro;->writeMessage(ILcom/google/protobuf/micro/MessageMicro;)V

    :cond_1
    invoke-virtual {p0}, Lcom/google/android/finsky/protos/CommonDevice$Instrument;->hasCreditCard()Z

    move-result v2

    if-eqz v2, :cond_2

    const/4 v2, 0x3

    invoke-virtual {p0}, Lcom/google/android/finsky/protos/CommonDevice$Instrument;->getCreditCard()Lcom/google/android/finsky/protos/CommonDevice$CreditCardInstrument;

    move-result-object v3

    invoke-virtual {p1, v2, v3}, Lcom/google/protobuf/micro/CodedOutputStreamMicro;->writeMessage(ILcom/google/protobuf/micro/MessageMicro;)V

    :cond_2
    invoke-virtual {p0}, Lcom/google/android/finsky/protos/CommonDevice$Instrument;->hasCarrierBilling()Z

    move-result v2

    if-eqz v2, :cond_3

    const/4 v2, 0x4

    invoke-virtual {p0}, Lcom/google/android/finsky/protos/CommonDevice$Instrument;->getCarrierBilling()Lcom/google/android/finsky/protos/CommonDevice$CarrierBillingInstrument;

    move-result-object v3

    invoke-virtual {p1, v2, v3}, Lcom/google/protobuf/micro/CodedOutputStreamMicro;->writeMessage(ILcom/google/protobuf/micro/MessageMicro;)V

    :cond_3
    invoke-virtual {p0}, Lcom/google/android/finsky/protos/CommonDevice$Instrument;->hasBillingAddressSpec()Z

    move-result v2

    if-eqz v2, :cond_4

    const/4 v2, 0x5

    invoke-virtual {p0}, Lcom/google/android/finsky/protos/CommonDevice$Instrument;->getBillingAddressSpec()Lcom/google/android/finsky/protos/CommonDevice$BillingAddressSpec;

    move-result-object v3

    invoke-virtual {p1, v2, v3}, Lcom/google/protobuf/micro/CodedOutputStreamMicro;->writeMessage(ILcom/google/protobuf/micro/MessageMicro;)V

    :cond_4
    invoke-virtual {p0}, Lcom/google/android/finsky/protos/CommonDevice$Instrument;->hasInstrumentFamily()Z

    move-result v2

    if-eqz v2, :cond_5

    const/4 v2, 0x6

    invoke-virtual {p0}, Lcom/google/android/finsky/protos/CommonDevice$Instrument;->getInstrumentFamily()I

    move-result v3

    invoke-virtual {p1, v2, v3}, Lcom/google/protobuf/micro/CodedOutputStreamMicro;->writeInt32(II)V

    :cond_5
    invoke-virtual {p0}, Lcom/google/android/finsky/protos/CommonDevice$Instrument;->hasCarrierBillingStatus()Z

    move-result v2

    if-eqz v2, :cond_6

    const/4 v2, 0x7

    invoke-virtual {p0}, Lcom/google/android/finsky/protos/CommonDevice$Instrument;->getCarrierBillingStatus()Lcom/google/android/finsky/protos/CommonDevice$CarrierBillingInstrumentStatus;

    move-result-object v3

    invoke-virtual {p1, v2, v3}, Lcom/google/protobuf/micro/CodedOutputStreamMicro;->writeMessage(ILcom/google/protobuf/micro/MessageMicro;)V

    :cond_6
    invoke-virtual {p0}, Lcom/google/android/finsky/protos/CommonDevice$Instrument;->hasDisplayTitle()Z

    move-result v2

    if-eqz v2, :cond_7

    const/16 v2, 0x8

    invoke-virtual {p0}, Lcom/google/android/finsky/protos/CommonDevice$Instrument;->getDisplayTitle()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {p1, v2, v3}, Lcom/google/protobuf/micro/CodedOutputStreamMicro;->writeString(ILjava/lang/String;)V

    :cond_7
    invoke-virtual {p0}, Lcom/google/android/finsky/protos/CommonDevice$Instrument;->hasTopupInfoDeprecated()Z

    move-result v2

    if-eqz v2, :cond_8

    const/16 v2, 0x9

    invoke-virtual {p0}, Lcom/google/android/finsky/protos/CommonDevice$Instrument;->getTopupInfoDeprecated()Lcom/google/android/finsky/protos/CommonDevice$TopupInfo;

    move-result-object v3

    invoke-virtual {p1, v2, v3}, Lcom/google/protobuf/micro/CodedOutputStreamMicro;->writeMessage(ILcom/google/protobuf/micro/MessageMicro;)V

    :cond_8
    invoke-virtual {p0}, Lcom/google/android/finsky/protos/CommonDevice$Instrument;->hasVersion()Z

    move-result v2

    if-eqz v2, :cond_9

    const/16 v2, 0xa

    invoke-virtual {p0}, Lcom/google/android/finsky/protos/CommonDevice$Instrument;->getVersion()I

    move-result v3

    invoke-virtual {p1, v2, v3}, Lcom/google/protobuf/micro/CodedOutputStreamMicro;->writeInt32(II)V

    :cond_9
    invoke-virtual {p0}, Lcom/google/android/finsky/protos/CommonDevice$Instrument;->hasStoredValue()Z

    move-result v2

    if-eqz v2, :cond_a

    const/16 v2, 0xb

    invoke-virtual {p0}, Lcom/google/android/finsky/protos/CommonDevice$Instrument;->getStoredValue()Lcom/google/android/finsky/protos/CommonDevice$StoredValueInstrument;

    move-result-object v3

    invoke-virtual {p1, v2, v3}, Lcom/google/protobuf/micro/CodedOutputStreamMicro;->writeMessage(ILcom/google/protobuf/micro/MessageMicro;)V

    :cond_a
    invoke-virtual {p0}, Lcom/google/android/finsky/protos/CommonDevice$Instrument;->getDisabledInfoList()Ljava/util/List;

    move-result-object v2

    invoke-interface {v2}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :goto_0
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v2

    if-eqz v2, :cond_b

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/finsky/protos/CommonDevice$DisabledInfo;

    const/16 v2, 0xc

    invoke-virtual {p1, v2, v0}, Lcom/google/protobuf/micro/CodedOutputStreamMicro;->writeMessage(ILcom/google/protobuf/micro/MessageMicro;)V

    goto :goto_0

    :cond_b
    return-void
.end method
