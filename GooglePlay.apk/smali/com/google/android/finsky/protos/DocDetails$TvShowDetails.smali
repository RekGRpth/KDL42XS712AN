.class public final Lcom/google/android/finsky/protos/DocDetails$TvShowDetails;
.super Lcom/google/protobuf/micro/MessageMicro;
.source "DocDetails.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/google/android/finsky/protos/DocDetails;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x19
    name = "TvShowDetails"
.end annotation


# instance fields
.field private broadcaster_:Ljava/lang/String;

.field private cachedSize:I

.field private endYear_:I

.field private hasBroadcaster:Z

.field private hasEndYear:Z

.field private hasSeasonCount:Z

.field private hasStartYear:Z

.field private seasonCount_:I

.field private startYear_:I


# direct methods
.method public constructor <init>()V
    .locals 1

    const/4 v0, 0x0

    invoke-direct {p0}, Lcom/google/protobuf/micro/MessageMicro;-><init>()V

    iput v0, p0, Lcom/google/android/finsky/protos/DocDetails$TvShowDetails;->seasonCount_:I

    iput v0, p0, Lcom/google/android/finsky/protos/DocDetails$TvShowDetails;->startYear_:I

    iput v0, p0, Lcom/google/android/finsky/protos/DocDetails$TvShowDetails;->endYear_:I

    const-string v0, ""

    iput-object v0, p0, Lcom/google/android/finsky/protos/DocDetails$TvShowDetails;->broadcaster_:Ljava/lang/String;

    const/4 v0, -0x1

    iput v0, p0, Lcom/google/android/finsky/protos/DocDetails$TvShowDetails;->cachedSize:I

    return-void
.end method


# virtual methods
.method public getBroadcaster()Ljava/lang/String;
    .locals 1

    iget-object v0, p0, Lcom/google/android/finsky/protos/DocDetails$TvShowDetails;->broadcaster_:Ljava/lang/String;

    return-object v0
.end method

.method public getCachedSize()I
    .locals 1

    iget v0, p0, Lcom/google/android/finsky/protos/DocDetails$TvShowDetails;->cachedSize:I

    if-gez v0, :cond_0

    invoke-virtual {p0}, Lcom/google/android/finsky/protos/DocDetails$TvShowDetails;->getSerializedSize()I

    :cond_0
    iget v0, p0, Lcom/google/android/finsky/protos/DocDetails$TvShowDetails;->cachedSize:I

    return v0
.end method

.method public getEndYear()I
    .locals 1

    iget v0, p0, Lcom/google/android/finsky/protos/DocDetails$TvShowDetails;->endYear_:I

    return v0
.end method

.method public getSeasonCount()I
    .locals 1

    iget v0, p0, Lcom/google/android/finsky/protos/DocDetails$TvShowDetails;->seasonCount_:I

    return v0
.end method

.method public getSerializedSize()I
    .locals 3

    const/4 v0, 0x0

    invoke-virtual {p0}, Lcom/google/android/finsky/protos/DocDetails$TvShowDetails;->hasSeasonCount()Z

    move-result v1

    if-eqz v1, :cond_0

    const/4 v1, 0x1

    invoke-virtual {p0}, Lcom/google/android/finsky/protos/DocDetails$TvShowDetails;->getSeasonCount()I

    move-result v2

    invoke-static {v1, v2}, Lcom/google/protobuf/micro/CodedOutputStreamMicro;->computeInt32Size(II)I

    move-result v1

    add-int/2addr v0, v1

    :cond_0
    invoke-virtual {p0}, Lcom/google/android/finsky/protos/DocDetails$TvShowDetails;->hasStartYear()Z

    move-result v1

    if-eqz v1, :cond_1

    const/4 v1, 0x2

    invoke-virtual {p0}, Lcom/google/android/finsky/protos/DocDetails$TvShowDetails;->getStartYear()I

    move-result v2

    invoke-static {v1, v2}, Lcom/google/protobuf/micro/CodedOutputStreamMicro;->computeInt32Size(II)I

    move-result v1

    add-int/2addr v0, v1

    :cond_1
    invoke-virtual {p0}, Lcom/google/android/finsky/protos/DocDetails$TvShowDetails;->hasEndYear()Z

    move-result v1

    if-eqz v1, :cond_2

    const/4 v1, 0x3

    invoke-virtual {p0}, Lcom/google/android/finsky/protos/DocDetails$TvShowDetails;->getEndYear()I

    move-result v2

    invoke-static {v1, v2}, Lcom/google/protobuf/micro/CodedOutputStreamMicro;->computeInt32Size(II)I

    move-result v1

    add-int/2addr v0, v1

    :cond_2
    invoke-virtual {p0}, Lcom/google/android/finsky/protos/DocDetails$TvShowDetails;->hasBroadcaster()Z

    move-result v1

    if-eqz v1, :cond_3

    const/4 v1, 0x4

    invoke-virtual {p0}, Lcom/google/android/finsky/protos/DocDetails$TvShowDetails;->getBroadcaster()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2}, Lcom/google/protobuf/micro/CodedOutputStreamMicro;->computeStringSize(ILjava/lang/String;)I

    move-result v1

    add-int/2addr v0, v1

    :cond_3
    iput v0, p0, Lcom/google/android/finsky/protos/DocDetails$TvShowDetails;->cachedSize:I

    return v0
.end method

.method public getStartYear()I
    .locals 1

    iget v0, p0, Lcom/google/android/finsky/protos/DocDetails$TvShowDetails;->startYear_:I

    return v0
.end method

.method public hasBroadcaster()Z
    .locals 1

    iget-boolean v0, p0, Lcom/google/android/finsky/protos/DocDetails$TvShowDetails;->hasBroadcaster:Z

    return v0
.end method

.method public hasEndYear()Z
    .locals 1

    iget-boolean v0, p0, Lcom/google/android/finsky/protos/DocDetails$TvShowDetails;->hasEndYear:Z

    return v0
.end method

.method public hasSeasonCount()Z
    .locals 1

    iget-boolean v0, p0, Lcom/google/android/finsky/protos/DocDetails$TvShowDetails;->hasSeasonCount:Z

    return v0
.end method

.method public hasStartYear()Z
    .locals 1

    iget-boolean v0, p0, Lcom/google/android/finsky/protos/DocDetails$TvShowDetails;->hasStartYear:Z

    return v0
.end method

.method public mergeFrom(Lcom/google/protobuf/micro/CodedInputStreamMicro;)Lcom/google/android/finsky/protos/DocDetails$TvShowDetails;
    .locals 2
    .param p1    # Lcom/google/protobuf/micro/CodedInputStreamMicro;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    :cond_0
    :goto_0
    invoke-virtual {p1}, Lcom/google/protobuf/micro/CodedInputStreamMicro;->readTag()I

    move-result v0

    sparse-switch v0, :sswitch_data_0

    invoke-virtual {p0, p1, v0}, Lcom/google/android/finsky/protos/DocDetails$TvShowDetails;->parseUnknownField(Lcom/google/protobuf/micro/CodedInputStreamMicro;I)Z

    move-result v1

    if-nez v1, :cond_0

    :sswitch_0
    return-object p0

    :sswitch_1
    invoke-virtual {p1}, Lcom/google/protobuf/micro/CodedInputStreamMicro;->readInt32()I

    move-result v1

    invoke-virtual {p0, v1}, Lcom/google/android/finsky/protos/DocDetails$TvShowDetails;->setSeasonCount(I)Lcom/google/android/finsky/protos/DocDetails$TvShowDetails;

    goto :goto_0

    :sswitch_2
    invoke-virtual {p1}, Lcom/google/protobuf/micro/CodedInputStreamMicro;->readInt32()I

    move-result v1

    invoke-virtual {p0, v1}, Lcom/google/android/finsky/protos/DocDetails$TvShowDetails;->setStartYear(I)Lcom/google/android/finsky/protos/DocDetails$TvShowDetails;

    goto :goto_0

    :sswitch_3
    invoke-virtual {p1}, Lcom/google/protobuf/micro/CodedInputStreamMicro;->readInt32()I

    move-result v1

    invoke-virtual {p0, v1}, Lcom/google/android/finsky/protos/DocDetails$TvShowDetails;->setEndYear(I)Lcom/google/android/finsky/protos/DocDetails$TvShowDetails;

    goto :goto_0

    :sswitch_4
    invoke-virtual {p1}, Lcom/google/protobuf/micro/CodedInputStreamMicro;->readString()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {p0, v1}, Lcom/google/android/finsky/protos/DocDetails$TvShowDetails;->setBroadcaster(Ljava/lang/String;)Lcom/google/android/finsky/protos/DocDetails$TvShowDetails;

    goto :goto_0

    :sswitch_data_0
    .sparse-switch
        0x0 -> :sswitch_0
        0x8 -> :sswitch_1
        0x10 -> :sswitch_2
        0x18 -> :sswitch_3
        0x22 -> :sswitch_4
    .end sparse-switch
.end method

.method public bridge synthetic mergeFrom(Lcom/google/protobuf/micro/CodedInputStreamMicro;)Lcom/google/protobuf/micro/MessageMicro;
    .locals 1
    .param p1    # Lcom/google/protobuf/micro/CodedInputStreamMicro;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    invoke-virtual {p0, p1}, Lcom/google/android/finsky/protos/DocDetails$TvShowDetails;->mergeFrom(Lcom/google/protobuf/micro/CodedInputStreamMicro;)Lcom/google/android/finsky/protos/DocDetails$TvShowDetails;

    move-result-object v0

    return-object v0
.end method

.method public setBroadcaster(Ljava/lang/String;)Lcom/google/android/finsky/protos/DocDetails$TvShowDetails;
    .locals 1
    .param p1    # Ljava/lang/String;

    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/google/android/finsky/protos/DocDetails$TvShowDetails;->hasBroadcaster:Z

    iput-object p1, p0, Lcom/google/android/finsky/protos/DocDetails$TvShowDetails;->broadcaster_:Ljava/lang/String;

    return-object p0
.end method

.method public setEndYear(I)Lcom/google/android/finsky/protos/DocDetails$TvShowDetails;
    .locals 1
    .param p1    # I

    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/google/android/finsky/protos/DocDetails$TvShowDetails;->hasEndYear:Z

    iput p1, p0, Lcom/google/android/finsky/protos/DocDetails$TvShowDetails;->endYear_:I

    return-object p0
.end method

.method public setSeasonCount(I)Lcom/google/android/finsky/protos/DocDetails$TvShowDetails;
    .locals 1
    .param p1    # I

    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/google/android/finsky/protos/DocDetails$TvShowDetails;->hasSeasonCount:Z

    iput p1, p0, Lcom/google/android/finsky/protos/DocDetails$TvShowDetails;->seasonCount_:I

    return-object p0
.end method

.method public setStartYear(I)Lcom/google/android/finsky/protos/DocDetails$TvShowDetails;
    .locals 1
    .param p1    # I

    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/google/android/finsky/protos/DocDetails$TvShowDetails;->hasStartYear:Z

    iput p1, p0, Lcom/google/android/finsky/protos/DocDetails$TvShowDetails;->startYear_:I

    return-object p0
.end method

.method public writeTo(Lcom/google/protobuf/micro/CodedOutputStreamMicro;)V
    .locals 2
    .param p1    # Lcom/google/protobuf/micro/CodedOutputStreamMicro;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    invoke-virtual {p0}, Lcom/google/android/finsky/protos/DocDetails$TvShowDetails;->hasSeasonCount()Z

    move-result v0

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    invoke-virtual {p0}, Lcom/google/android/finsky/protos/DocDetails$TvShowDetails;->getSeasonCount()I

    move-result v1

    invoke-virtual {p1, v0, v1}, Lcom/google/protobuf/micro/CodedOutputStreamMicro;->writeInt32(II)V

    :cond_0
    invoke-virtual {p0}, Lcom/google/android/finsky/protos/DocDetails$TvShowDetails;->hasStartYear()Z

    move-result v0

    if-eqz v0, :cond_1

    const/4 v0, 0x2

    invoke-virtual {p0}, Lcom/google/android/finsky/protos/DocDetails$TvShowDetails;->getStartYear()I

    move-result v1

    invoke-virtual {p1, v0, v1}, Lcom/google/protobuf/micro/CodedOutputStreamMicro;->writeInt32(II)V

    :cond_1
    invoke-virtual {p0}, Lcom/google/android/finsky/protos/DocDetails$TvShowDetails;->hasEndYear()Z

    move-result v0

    if-eqz v0, :cond_2

    const/4 v0, 0x3

    invoke-virtual {p0}, Lcom/google/android/finsky/protos/DocDetails$TvShowDetails;->getEndYear()I

    move-result v1

    invoke-virtual {p1, v0, v1}, Lcom/google/protobuf/micro/CodedOutputStreamMicro;->writeInt32(II)V

    :cond_2
    invoke-virtual {p0}, Lcom/google/android/finsky/protos/DocDetails$TvShowDetails;->hasBroadcaster()Z

    move-result v0

    if-eqz v0, :cond_3

    const/4 v0, 0x4

    invoke-virtual {p0}, Lcom/google/android/finsky/protos/DocDetails$TvShowDetails;->getBroadcaster()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {p1, v0, v1}, Lcom/google/protobuf/micro/CodedOutputStreamMicro;->writeString(ILjava/lang/String;)V

    :cond_3
    return-void
.end method
