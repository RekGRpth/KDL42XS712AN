.class public final Lcom/google/android/finsky/protos/Response$PreFetch;
.super Lcom/google/protobuf/micro/MessageMicro;
.source "Response.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/google/android/finsky/protos/Response;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x19
    name = "PreFetch"
.end annotation


# instance fields
.field private cachedSize:I

.field private etag_:Ljava/lang/String;

.field private hasEtag:Z

.field private hasResponse:Z

.field private hasSoftTtl:Z

.field private hasTtl:Z

.field private hasUrl:Z

.field private response_:Lcom/google/protobuf/micro/ByteStringMicro;

.field private softTtl_:J

.field private ttl_:J

.field private url_:Ljava/lang/String;


# direct methods
.method public constructor <init>()V
    .locals 3

    const-wide/16 v1, 0x0

    invoke-direct {p0}, Lcom/google/protobuf/micro/MessageMicro;-><init>()V

    const-string v0, ""

    iput-object v0, p0, Lcom/google/android/finsky/protos/Response$PreFetch;->url_:Ljava/lang/String;

    sget-object v0, Lcom/google/protobuf/micro/ByteStringMicro;->EMPTY:Lcom/google/protobuf/micro/ByteStringMicro;

    iput-object v0, p0, Lcom/google/android/finsky/protos/Response$PreFetch;->response_:Lcom/google/protobuf/micro/ByteStringMicro;

    const-string v0, ""

    iput-object v0, p0, Lcom/google/android/finsky/protos/Response$PreFetch;->etag_:Ljava/lang/String;

    iput-wide v1, p0, Lcom/google/android/finsky/protos/Response$PreFetch;->ttl_:J

    iput-wide v1, p0, Lcom/google/android/finsky/protos/Response$PreFetch;->softTtl_:J

    const/4 v0, -0x1

    iput v0, p0, Lcom/google/android/finsky/protos/Response$PreFetch;->cachedSize:I

    return-void
.end method


# virtual methods
.method public getCachedSize()I
    .locals 1

    iget v0, p0, Lcom/google/android/finsky/protos/Response$PreFetch;->cachedSize:I

    if-gez v0, :cond_0

    invoke-virtual {p0}, Lcom/google/android/finsky/protos/Response$PreFetch;->getSerializedSize()I

    :cond_0
    iget v0, p0, Lcom/google/android/finsky/protos/Response$PreFetch;->cachedSize:I

    return v0
.end method

.method public getEtag()Ljava/lang/String;
    .locals 1

    iget-object v0, p0, Lcom/google/android/finsky/protos/Response$PreFetch;->etag_:Ljava/lang/String;

    return-object v0
.end method

.method public getResponse()Lcom/google/protobuf/micro/ByteStringMicro;
    .locals 1

    iget-object v0, p0, Lcom/google/android/finsky/protos/Response$PreFetch;->response_:Lcom/google/protobuf/micro/ByteStringMicro;

    return-object v0
.end method

.method public getSerializedSize()I
    .locals 4

    const/4 v0, 0x0

    invoke-virtual {p0}, Lcom/google/android/finsky/protos/Response$PreFetch;->hasUrl()Z

    move-result v1

    if-eqz v1, :cond_0

    const/4 v1, 0x1

    invoke-virtual {p0}, Lcom/google/android/finsky/protos/Response$PreFetch;->getUrl()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2}, Lcom/google/protobuf/micro/CodedOutputStreamMicro;->computeStringSize(ILjava/lang/String;)I

    move-result v1

    add-int/2addr v0, v1

    :cond_0
    invoke-virtual {p0}, Lcom/google/android/finsky/protos/Response$PreFetch;->hasResponse()Z

    move-result v1

    if-eqz v1, :cond_1

    const/4 v1, 0x2

    invoke-virtual {p0}, Lcom/google/android/finsky/protos/Response$PreFetch;->getResponse()Lcom/google/protobuf/micro/ByteStringMicro;

    move-result-object v2

    invoke-static {v1, v2}, Lcom/google/protobuf/micro/CodedOutputStreamMicro;->computeBytesSize(ILcom/google/protobuf/micro/ByteStringMicro;)I

    move-result v1

    add-int/2addr v0, v1

    :cond_1
    invoke-virtual {p0}, Lcom/google/android/finsky/protos/Response$PreFetch;->hasEtag()Z

    move-result v1

    if-eqz v1, :cond_2

    const/4 v1, 0x3

    invoke-virtual {p0}, Lcom/google/android/finsky/protos/Response$PreFetch;->getEtag()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2}, Lcom/google/protobuf/micro/CodedOutputStreamMicro;->computeStringSize(ILjava/lang/String;)I

    move-result v1

    add-int/2addr v0, v1

    :cond_2
    invoke-virtual {p0}, Lcom/google/android/finsky/protos/Response$PreFetch;->hasTtl()Z

    move-result v1

    if-eqz v1, :cond_3

    const/4 v1, 0x4

    invoke-virtual {p0}, Lcom/google/android/finsky/protos/Response$PreFetch;->getTtl()J

    move-result-wide v2

    invoke-static {v1, v2, v3}, Lcom/google/protobuf/micro/CodedOutputStreamMicro;->computeInt64Size(IJ)I

    move-result v1

    add-int/2addr v0, v1

    :cond_3
    invoke-virtual {p0}, Lcom/google/android/finsky/protos/Response$PreFetch;->hasSoftTtl()Z

    move-result v1

    if-eqz v1, :cond_4

    const/4 v1, 0x5

    invoke-virtual {p0}, Lcom/google/android/finsky/protos/Response$PreFetch;->getSoftTtl()J

    move-result-wide v2

    invoke-static {v1, v2, v3}, Lcom/google/protobuf/micro/CodedOutputStreamMicro;->computeInt64Size(IJ)I

    move-result v1

    add-int/2addr v0, v1

    :cond_4
    iput v0, p0, Lcom/google/android/finsky/protos/Response$PreFetch;->cachedSize:I

    return v0
.end method

.method public getSoftTtl()J
    .locals 2

    iget-wide v0, p0, Lcom/google/android/finsky/protos/Response$PreFetch;->softTtl_:J

    return-wide v0
.end method

.method public getTtl()J
    .locals 2

    iget-wide v0, p0, Lcom/google/android/finsky/protos/Response$PreFetch;->ttl_:J

    return-wide v0
.end method

.method public getUrl()Ljava/lang/String;
    .locals 1

    iget-object v0, p0, Lcom/google/android/finsky/protos/Response$PreFetch;->url_:Ljava/lang/String;

    return-object v0
.end method

.method public hasEtag()Z
    .locals 1

    iget-boolean v0, p0, Lcom/google/android/finsky/protos/Response$PreFetch;->hasEtag:Z

    return v0
.end method

.method public hasResponse()Z
    .locals 1

    iget-boolean v0, p0, Lcom/google/android/finsky/protos/Response$PreFetch;->hasResponse:Z

    return v0
.end method

.method public hasSoftTtl()Z
    .locals 1

    iget-boolean v0, p0, Lcom/google/android/finsky/protos/Response$PreFetch;->hasSoftTtl:Z

    return v0
.end method

.method public hasTtl()Z
    .locals 1

    iget-boolean v0, p0, Lcom/google/android/finsky/protos/Response$PreFetch;->hasTtl:Z

    return v0
.end method

.method public hasUrl()Z
    .locals 1

    iget-boolean v0, p0, Lcom/google/android/finsky/protos/Response$PreFetch;->hasUrl:Z

    return v0
.end method

.method public mergeFrom(Lcom/google/protobuf/micro/CodedInputStreamMicro;)Lcom/google/android/finsky/protos/Response$PreFetch;
    .locals 3
    .param p1    # Lcom/google/protobuf/micro/CodedInputStreamMicro;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    :cond_0
    :goto_0
    invoke-virtual {p1}, Lcom/google/protobuf/micro/CodedInputStreamMicro;->readTag()I

    move-result v0

    sparse-switch v0, :sswitch_data_0

    invoke-virtual {p0, p1, v0}, Lcom/google/android/finsky/protos/Response$PreFetch;->parseUnknownField(Lcom/google/protobuf/micro/CodedInputStreamMicro;I)Z

    move-result v1

    if-nez v1, :cond_0

    :sswitch_0
    return-object p0

    :sswitch_1
    invoke-virtual {p1}, Lcom/google/protobuf/micro/CodedInputStreamMicro;->readString()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {p0, v1}, Lcom/google/android/finsky/protos/Response$PreFetch;->setUrl(Ljava/lang/String;)Lcom/google/android/finsky/protos/Response$PreFetch;

    goto :goto_0

    :sswitch_2
    invoke-virtual {p1}, Lcom/google/protobuf/micro/CodedInputStreamMicro;->readBytes()Lcom/google/protobuf/micro/ByteStringMicro;

    move-result-object v1

    invoke-virtual {p0, v1}, Lcom/google/android/finsky/protos/Response$PreFetch;->setResponse(Lcom/google/protobuf/micro/ByteStringMicro;)Lcom/google/android/finsky/protos/Response$PreFetch;

    goto :goto_0

    :sswitch_3
    invoke-virtual {p1}, Lcom/google/protobuf/micro/CodedInputStreamMicro;->readString()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {p0, v1}, Lcom/google/android/finsky/protos/Response$PreFetch;->setEtag(Ljava/lang/String;)Lcom/google/android/finsky/protos/Response$PreFetch;

    goto :goto_0

    :sswitch_4
    invoke-virtual {p1}, Lcom/google/protobuf/micro/CodedInputStreamMicro;->readInt64()J

    move-result-wide v1

    invoke-virtual {p0, v1, v2}, Lcom/google/android/finsky/protos/Response$PreFetch;->setTtl(J)Lcom/google/android/finsky/protos/Response$PreFetch;

    goto :goto_0

    :sswitch_5
    invoke-virtual {p1}, Lcom/google/protobuf/micro/CodedInputStreamMicro;->readInt64()J

    move-result-wide v1

    invoke-virtual {p0, v1, v2}, Lcom/google/android/finsky/protos/Response$PreFetch;->setSoftTtl(J)Lcom/google/android/finsky/protos/Response$PreFetch;

    goto :goto_0

    :sswitch_data_0
    .sparse-switch
        0x0 -> :sswitch_0
        0xa -> :sswitch_1
        0x12 -> :sswitch_2
        0x1a -> :sswitch_3
        0x20 -> :sswitch_4
        0x28 -> :sswitch_5
    .end sparse-switch
.end method

.method public bridge synthetic mergeFrom(Lcom/google/protobuf/micro/CodedInputStreamMicro;)Lcom/google/protobuf/micro/MessageMicro;
    .locals 1
    .param p1    # Lcom/google/protobuf/micro/CodedInputStreamMicro;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    invoke-virtual {p0, p1}, Lcom/google/android/finsky/protos/Response$PreFetch;->mergeFrom(Lcom/google/protobuf/micro/CodedInputStreamMicro;)Lcom/google/android/finsky/protos/Response$PreFetch;

    move-result-object v0

    return-object v0
.end method

.method public setEtag(Ljava/lang/String;)Lcom/google/android/finsky/protos/Response$PreFetch;
    .locals 1
    .param p1    # Ljava/lang/String;

    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/google/android/finsky/protos/Response$PreFetch;->hasEtag:Z

    iput-object p1, p0, Lcom/google/android/finsky/protos/Response$PreFetch;->etag_:Ljava/lang/String;

    return-object p0
.end method

.method public setResponse(Lcom/google/protobuf/micro/ByteStringMicro;)Lcom/google/android/finsky/protos/Response$PreFetch;
    .locals 1
    .param p1    # Lcom/google/protobuf/micro/ByteStringMicro;

    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/google/android/finsky/protos/Response$PreFetch;->hasResponse:Z

    iput-object p1, p0, Lcom/google/android/finsky/protos/Response$PreFetch;->response_:Lcom/google/protobuf/micro/ByteStringMicro;

    return-object p0
.end method

.method public setSoftTtl(J)Lcom/google/android/finsky/protos/Response$PreFetch;
    .locals 1
    .param p1    # J

    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/google/android/finsky/protos/Response$PreFetch;->hasSoftTtl:Z

    iput-wide p1, p0, Lcom/google/android/finsky/protos/Response$PreFetch;->softTtl_:J

    return-object p0
.end method

.method public setTtl(J)Lcom/google/android/finsky/protos/Response$PreFetch;
    .locals 1
    .param p1    # J

    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/google/android/finsky/protos/Response$PreFetch;->hasTtl:Z

    iput-wide p1, p0, Lcom/google/android/finsky/protos/Response$PreFetch;->ttl_:J

    return-object p0
.end method

.method public setUrl(Ljava/lang/String;)Lcom/google/android/finsky/protos/Response$PreFetch;
    .locals 1
    .param p1    # Ljava/lang/String;

    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/google/android/finsky/protos/Response$PreFetch;->hasUrl:Z

    iput-object p1, p0, Lcom/google/android/finsky/protos/Response$PreFetch;->url_:Ljava/lang/String;

    return-object p0
.end method

.method public writeTo(Lcom/google/protobuf/micro/CodedOutputStreamMicro;)V
    .locals 3
    .param p1    # Lcom/google/protobuf/micro/CodedOutputStreamMicro;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    invoke-virtual {p0}, Lcom/google/android/finsky/protos/Response$PreFetch;->hasUrl()Z

    move-result v0

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    invoke-virtual {p0}, Lcom/google/android/finsky/protos/Response$PreFetch;->getUrl()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {p1, v0, v1}, Lcom/google/protobuf/micro/CodedOutputStreamMicro;->writeString(ILjava/lang/String;)V

    :cond_0
    invoke-virtual {p0}, Lcom/google/android/finsky/protos/Response$PreFetch;->hasResponse()Z

    move-result v0

    if-eqz v0, :cond_1

    const/4 v0, 0x2

    invoke-virtual {p0}, Lcom/google/android/finsky/protos/Response$PreFetch;->getResponse()Lcom/google/protobuf/micro/ByteStringMicro;

    move-result-object v1

    invoke-virtual {p1, v0, v1}, Lcom/google/protobuf/micro/CodedOutputStreamMicro;->writeBytes(ILcom/google/protobuf/micro/ByteStringMicro;)V

    :cond_1
    invoke-virtual {p0}, Lcom/google/android/finsky/protos/Response$PreFetch;->hasEtag()Z

    move-result v0

    if-eqz v0, :cond_2

    const/4 v0, 0x3

    invoke-virtual {p0}, Lcom/google/android/finsky/protos/Response$PreFetch;->getEtag()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {p1, v0, v1}, Lcom/google/protobuf/micro/CodedOutputStreamMicro;->writeString(ILjava/lang/String;)V

    :cond_2
    invoke-virtual {p0}, Lcom/google/android/finsky/protos/Response$PreFetch;->hasTtl()Z

    move-result v0

    if-eqz v0, :cond_3

    const/4 v0, 0x4

    invoke-virtual {p0}, Lcom/google/android/finsky/protos/Response$PreFetch;->getTtl()J

    move-result-wide v1

    invoke-virtual {p1, v0, v1, v2}, Lcom/google/protobuf/micro/CodedOutputStreamMicro;->writeInt64(IJ)V

    :cond_3
    invoke-virtual {p0}, Lcom/google/android/finsky/protos/Response$PreFetch;->hasSoftTtl()Z

    move-result v0

    if-eqz v0, :cond_4

    const/4 v0, 0x5

    invoke-virtual {p0}, Lcom/google/android/finsky/protos/Response$PreFetch;->getSoftTtl()J

    move-result-wide v1

    invoke-virtual {p1, v0, v1, v2}, Lcom/google/protobuf/micro/CodedOutputStreamMicro;->writeInt64(IJ)V

    :cond_4
    return-void
.end method
