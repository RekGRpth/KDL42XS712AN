.class public final Lcom/google/android/finsky/protos/ChallengeProtos$AddressChallenge;
.super Lcom/google/protobuf/micro/MessageMicro;
.source "ChallengeProtos.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/google/android/finsky/protos/ChallengeProtos;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x19
    name = "AddressChallenge"
.end annotation


# instance fields
.field private address_:Lcom/google/android/finsky/protos/BillingAddress$Address;

.field private cachedSize:I

.field private checkbox_:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Lcom/google/android/finsky/protos/ChallengeProtos$FormCheckbox;",
            ">;"
        }
    .end annotation
.end field

.field private descriptionHtml_:Ljava/lang/String;

.field private errorHtml_:Ljava/lang/String;

.field private errorInputField_:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Lcom/google/android/finsky/protos/ChallengeProtos$InputValidationError;",
            ">;"
        }
    .end annotation
.end field

.field private hasAddress:Z

.field private hasDescriptionHtml:Z

.field private hasErrorHtml:Z

.field private hasResponseAddressParam:Z

.field private hasResponseCheckboxesParam:Z

.field private hasTitle:Z

.field private requiredField_:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Ljava/lang/Integer;",
            ">;"
        }
    .end annotation
.end field

.field private responseAddressParam_:Ljava/lang/String;

.field private responseCheckboxesParam_:Ljava/lang/String;

.field private supportedCountry_:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Lcom/google/android/finsky/protos/ChallengeProtos$Country;",
            ">;"
        }
    .end annotation
.end field

.field private title_:Ljava/lang/String;


# direct methods
.method public constructor <init>()V
    .locals 1

    invoke-direct {p0}, Lcom/google/protobuf/micro/MessageMicro;-><init>()V

    const-string v0, ""

    iput-object v0, p0, Lcom/google/android/finsky/protos/ChallengeProtos$AddressChallenge;->responseAddressParam_:Ljava/lang/String;

    const-string v0, ""

    iput-object v0, p0, Lcom/google/android/finsky/protos/ChallengeProtos$AddressChallenge;->responseCheckboxesParam_:Ljava/lang/String;

    const-string v0, ""

    iput-object v0, p0, Lcom/google/android/finsky/protos/ChallengeProtos$AddressChallenge;->title_:Ljava/lang/String;

    const-string v0, ""

    iput-object v0, p0, Lcom/google/android/finsky/protos/ChallengeProtos$AddressChallenge;->descriptionHtml_:Ljava/lang/String;

    invoke-static {}, Ljava/util/Collections;->emptyList()Ljava/util/List;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/finsky/protos/ChallengeProtos$AddressChallenge;->checkbox_:Ljava/util/List;

    const/4 v0, 0x0

    iput-object v0, p0, Lcom/google/android/finsky/protos/ChallengeProtos$AddressChallenge;->address_:Lcom/google/android/finsky/protos/BillingAddress$Address;

    invoke-static {}, Ljava/util/Collections;->emptyList()Ljava/util/List;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/finsky/protos/ChallengeProtos$AddressChallenge;->errorInputField_:Ljava/util/List;

    const-string v0, ""

    iput-object v0, p0, Lcom/google/android/finsky/protos/ChallengeProtos$AddressChallenge;->errorHtml_:Ljava/lang/String;

    invoke-static {}, Ljava/util/Collections;->emptyList()Ljava/util/List;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/finsky/protos/ChallengeProtos$AddressChallenge;->requiredField_:Ljava/util/List;

    invoke-static {}, Ljava/util/Collections;->emptyList()Ljava/util/List;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/finsky/protos/ChallengeProtos$AddressChallenge;->supportedCountry_:Ljava/util/List;

    const/4 v0, -0x1

    iput v0, p0, Lcom/google/android/finsky/protos/ChallengeProtos$AddressChallenge;->cachedSize:I

    return-void
.end method


# virtual methods
.method public addCheckbox(Lcom/google/android/finsky/protos/ChallengeProtos$FormCheckbox;)Lcom/google/android/finsky/protos/ChallengeProtos$AddressChallenge;
    .locals 1
    .param p1    # Lcom/google/android/finsky/protos/ChallengeProtos$FormCheckbox;

    if-nez p1, :cond_0

    new-instance v0, Ljava/lang/NullPointerException;

    invoke-direct {v0}, Ljava/lang/NullPointerException;-><init>()V

    throw v0

    :cond_0
    iget-object v0, p0, Lcom/google/android/finsky/protos/ChallengeProtos$AddressChallenge;->checkbox_:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->isEmpty()Z

    move-result v0

    if-eqz v0, :cond_1

    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, Lcom/google/android/finsky/protos/ChallengeProtos$AddressChallenge;->checkbox_:Ljava/util/List;

    :cond_1
    iget-object v0, p0, Lcom/google/android/finsky/protos/ChallengeProtos$AddressChallenge;->checkbox_:Ljava/util/List;

    invoke-interface {v0, p1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    return-object p0
.end method

.method public addErrorInputField(Lcom/google/android/finsky/protos/ChallengeProtos$InputValidationError;)Lcom/google/android/finsky/protos/ChallengeProtos$AddressChallenge;
    .locals 1
    .param p1    # Lcom/google/android/finsky/protos/ChallengeProtos$InputValidationError;

    if-nez p1, :cond_0

    new-instance v0, Ljava/lang/NullPointerException;

    invoke-direct {v0}, Ljava/lang/NullPointerException;-><init>()V

    throw v0

    :cond_0
    iget-object v0, p0, Lcom/google/android/finsky/protos/ChallengeProtos$AddressChallenge;->errorInputField_:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->isEmpty()Z

    move-result v0

    if-eqz v0, :cond_1

    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, Lcom/google/android/finsky/protos/ChallengeProtos$AddressChallenge;->errorInputField_:Ljava/util/List;

    :cond_1
    iget-object v0, p0, Lcom/google/android/finsky/protos/ChallengeProtos$AddressChallenge;->errorInputField_:Ljava/util/List;

    invoke-interface {v0, p1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    return-object p0
.end method

.method public addRequiredField(I)Lcom/google/android/finsky/protos/ChallengeProtos$AddressChallenge;
    .locals 2
    .param p1    # I

    iget-object v0, p0, Lcom/google/android/finsky/protos/ChallengeProtos$AddressChallenge;->requiredField_:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->isEmpty()Z

    move-result v0

    if-eqz v0, :cond_0

    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, Lcom/google/android/finsky/protos/ChallengeProtos$AddressChallenge;->requiredField_:Ljava/util/List;

    :cond_0
    iget-object v0, p0, Lcom/google/android/finsky/protos/ChallengeProtos$AddressChallenge;->requiredField_:Ljava/util/List;

    invoke-static {p1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    invoke-interface {v0, v1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    return-object p0
.end method

.method public addSupportedCountry(Lcom/google/android/finsky/protos/ChallengeProtos$Country;)Lcom/google/android/finsky/protos/ChallengeProtos$AddressChallenge;
    .locals 1
    .param p1    # Lcom/google/android/finsky/protos/ChallengeProtos$Country;

    if-nez p1, :cond_0

    new-instance v0, Ljava/lang/NullPointerException;

    invoke-direct {v0}, Ljava/lang/NullPointerException;-><init>()V

    throw v0

    :cond_0
    iget-object v0, p0, Lcom/google/android/finsky/protos/ChallengeProtos$AddressChallenge;->supportedCountry_:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->isEmpty()Z

    move-result v0

    if-eqz v0, :cond_1

    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, Lcom/google/android/finsky/protos/ChallengeProtos$AddressChallenge;->supportedCountry_:Ljava/util/List;

    :cond_1
    iget-object v0, p0, Lcom/google/android/finsky/protos/ChallengeProtos$AddressChallenge;->supportedCountry_:Ljava/util/List;

    invoke-interface {v0, p1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    return-object p0
.end method

.method public getAddress()Lcom/google/android/finsky/protos/BillingAddress$Address;
    .locals 1

    iget-object v0, p0, Lcom/google/android/finsky/protos/ChallengeProtos$AddressChallenge;->address_:Lcom/google/android/finsky/protos/BillingAddress$Address;

    return-object v0
.end method

.method public getCachedSize()I
    .locals 1

    iget v0, p0, Lcom/google/android/finsky/protos/ChallengeProtos$AddressChallenge;->cachedSize:I

    if-gez v0, :cond_0

    invoke-virtual {p0}, Lcom/google/android/finsky/protos/ChallengeProtos$AddressChallenge;->getSerializedSize()I

    :cond_0
    iget v0, p0, Lcom/google/android/finsky/protos/ChallengeProtos$AddressChallenge;->cachedSize:I

    return v0
.end method

.method public getCheckbox(I)Lcom/google/android/finsky/protos/ChallengeProtos$FormCheckbox;
    .locals 1
    .param p1    # I

    iget-object v0, p0, Lcom/google/android/finsky/protos/ChallengeProtos$AddressChallenge;->checkbox_:Ljava/util/List;

    invoke-interface {v0, p1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/finsky/protos/ChallengeProtos$FormCheckbox;

    return-object v0
.end method

.method public getCheckboxCount()I
    .locals 1

    iget-object v0, p0, Lcom/google/android/finsky/protos/ChallengeProtos$AddressChallenge;->checkbox_:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->size()I

    move-result v0

    return v0
.end method

.method public getCheckboxList()Ljava/util/List;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/List",
            "<",
            "Lcom/google/android/finsky/protos/ChallengeProtos$FormCheckbox;",
            ">;"
        }
    .end annotation

    iget-object v0, p0, Lcom/google/android/finsky/protos/ChallengeProtos$AddressChallenge;->checkbox_:Ljava/util/List;

    return-object v0
.end method

.method public getDescriptionHtml()Ljava/lang/String;
    .locals 1

    iget-object v0, p0, Lcom/google/android/finsky/protos/ChallengeProtos$AddressChallenge;->descriptionHtml_:Ljava/lang/String;

    return-object v0
.end method

.method public getErrorHtml()Ljava/lang/String;
    .locals 1

    iget-object v0, p0, Lcom/google/android/finsky/protos/ChallengeProtos$AddressChallenge;->errorHtml_:Ljava/lang/String;

    return-object v0
.end method

.method public getErrorInputFieldList()Ljava/util/List;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/List",
            "<",
            "Lcom/google/android/finsky/protos/ChallengeProtos$InputValidationError;",
            ">;"
        }
    .end annotation

    iget-object v0, p0, Lcom/google/android/finsky/protos/ChallengeProtos$AddressChallenge;->errorInputField_:Ljava/util/List;

    return-object v0
.end method

.method public getRequiredFieldList()Ljava/util/List;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/List",
            "<",
            "Ljava/lang/Integer;",
            ">;"
        }
    .end annotation

    iget-object v0, p0, Lcom/google/android/finsky/protos/ChallengeProtos$AddressChallenge;->requiredField_:Ljava/util/List;

    return-object v0
.end method

.method public getResponseAddressParam()Ljava/lang/String;
    .locals 1

    iget-object v0, p0, Lcom/google/android/finsky/protos/ChallengeProtos$AddressChallenge;->responseAddressParam_:Ljava/lang/String;

    return-object v0
.end method

.method public getResponseCheckboxesParam()Ljava/lang/String;
    .locals 1

    iget-object v0, p0, Lcom/google/android/finsky/protos/ChallengeProtos$AddressChallenge;->responseCheckboxesParam_:Ljava/lang/String;

    return-object v0
.end method

.method public getSerializedSize()I
    .locals 6

    const/4 v3, 0x0

    invoke-virtual {p0}, Lcom/google/android/finsky/protos/ChallengeProtos$AddressChallenge;->hasResponseAddressParam()Z

    move-result v4

    if-eqz v4, :cond_0

    const/4 v4, 0x1

    invoke-virtual {p0}, Lcom/google/android/finsky/protos/ChallengeProtos$AddressChallenge;->getResponseAddressParam()Ljava/lang/String;

    move-result-object v5

    invoke-static {v4, v5}, Lcom/google/protobuf/micro/CodedOutputStreamMicro;->computeStringSize(ILjava/lang/String;)I

    move-result v4

    add-int/2addr v3, v4

    :cond_0
    invoke-virtual {p0}, Lcom/google/android/finsky/protos/ChallengeProtos$AddressChallenge;->hasResponseCheckboxesParam()Z

    move-result v4

    if-eqz v4, :cond_1

    const/4 v4, 0x2

    invoke-virtual {p0}, Lcom/google/android/finsky/protos/ChallengeProtos$AddressChallenge;->getResponseCheckboxesParam()Ljava/lang/String;

    move-result-object v5

    invoke-static {v4, v5}, Lcom/google/protobuf/micro/CodedOutputStreamMicro;->computeStringSize(ILjava/lang/String;)I

    move-result v4

    add-int/2addr v3, v4

    :cond_1
    invoke-virtual {p0}, Lcom/google/android/finsky/protos/ChallengeProtos$AddressChallenge;->hasTitle()Z

    move-result v4

    if-eqz v4, :cond_2

    const/4 v4, 0x3

    invoke-virtual {p0}, Lcom/google/android/finsky/protos/ChallengeProtos$AddressChallenge;->getTitle()Ljava/lang/String;

    move-result-object v5

    invoke-static {v4, v5}, Lcom/google/protobuf/micro/CodedOutputStreamMicro;->computeStringSize(ILjava/lang/String;)I

    move-result v4

    add-int/2addr v3, v4

    :cond_2
    invoke-virtual {p0}, Lcom/google/android/finsky/protos/ChallengeProtos$AddressChallenge;->hasDescriptionHtml()Z

    move-result v4

    if-eqz v4, :cond_3

    const/4 v4, 0x4

    invoke-virtual {p0}, Lcom/google/android/finsky/protos/ChallengeProtos$AddressChallenge;->getDescriptionHtml()Ljava/lang/String;

    move-result-object v5

    invoke-static {v4, v5}, Lcom/google/protobuf/micro/CodedOutputStreamMicro;->computeStringSize(ILjava/lang/String;)I

    move-result v4

    add-int/2addr v3, v4

    :cond_3
    invoke-virtual {p0}, Lcom/google/android/finsky/protos/ChallengeProtos$AddressChallenge;->getCheckboxList()Ljava/util/List;

    move-result-object v4

    invoke-interface {v4}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v2

    :goto_0
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    move-result v4

    if-eqz v4, :cond_4

    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/google/android/finsky/protos/ChallengeProtos$FormCheckbox;

    const/4 v4, 0x5

    invoke-static {v4, v1}, Lcom/google/protobuf/micro/CodedOutputStreamMicro;->computeMessageSize(ILcom/google/protobuf/micro/MessageMicro;)I

    move-result v4

    add-int/2addr v3, v4

    goto :goto_0

    :cond_4
    invoke-virtual {p0}, Lcom/google/android/finsky/protos/ChallengeProtos$AddressChallenge;->hasAddress()Z

    move-result v4

    if-eqz v4, :cond_5

    const/4 v4, 0x6

    invoke-virtual {p0}, Lcom/google/android/finsky/protos/ChallengeProtos$AddressChallenge;->getAddress()Lcom/google/android/finsky/protos/BillingAddress$Address;

    move-result-object v5

    invoke-static {v4, v5}, Lcom/google/protobuf/micro/CodedOutputStreamMicro;->computeMessageSize(ILcom/google/protobuf/micro/MessageMicro;)I

    move-result v4

    add-int/2addr v3, v4

    :cond_5
    invoke-virtual {p0}, Lcom/google/android/finsky/protos/ChallengeProtos$AddressChallenge;->getErrorInputFieldList()Ljava/util/List;

    move-result-object v4

    invoke-interface {v4}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v2

    :goto_1
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    move-result v4

    if-eqz v4, :cond_6

    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/google/android/finsky/protos/ChallengeProtos$InputValidationError;

    const/4 v4, 0x7

    invoke-static {v4, v1}, Lcom/google/protobuf/micro/CodedOutputStreamMicro;->computeMessageSize(ILcom/google/protobuf/micro/MessageMicro;)I

    move-result v4

    add-int/2addr v3, v4

    goto :goto_1

    :cond_6
    invoke-virtual {p0}, Lcom/google/android/finsky/protos/ChallengeProtos$AddressChallenge;->hasErrorHtml()Z

    move-result v4

    if-eqz v4, :cond_7

    const/16 v4, 0x8

    invoke-virtual {p0}, Lcom/google/android/finsky/protos/ChallengeProtos$AddressChallenge;->getErrorHtml()Ljava/lang/String;

    move-result-object v5

    invoke-static {v4, v5}, Lcom/google/protobuf/micro/CodedOutputStreamMicro;->computeStringSize(ILjava/lang/String;)I

    move-result v4

    add-int/2addr v3, v4

    :cond_7
    const/4 v0, 0x0

    invoke-virtual {p0}, Lcom/google/android/finsky/protos/ChallengeProtos$AddressChallenge;->getRequiredFieldList()Ljava/util/List;

    move-result-object v4

    invoke-interface {v4}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v2

    :goto_2
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    move-result v4

    if-eqz v4, :cond_8

    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Ljava/lang/Integer;

    invoke-virtual {v1}, Ljava/lang/Integer;->intValue()I

    move-result v4

    invoke-static {v4}, Lcom/google/protobuf/micro/CodedOutputStreamMicro;->computeInt32SizeNoTag(I)I

    move-result v4

    add-int/2addr v0, v4

    goto :goto_2

    :cond_8
    add-int/2addr v3, v0

    invoke-virtual {p0}, Lcom/google/android/finsky/protos/ChallengeProtos$AddressChallenge;->getRequiredFieldList()Ljava/util/List;

    move-result-object v4

    invoke-interface {v4}, Ljava/util/List;->size()I

    move-result v4

    mul-int/lit8 v4, v4, 0x1

    add-int/2addr v3, v4

    invoke-virtual {p0}, Lcom/google/android/finsky/protos/ChallengeProtos$AddressChallenge;->getSupportedCountryList()Ljava/util/List;

    move-result-object v4

    invoke-interface {v4}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v2

    :goto_3
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    move-result v4

    if-eqz v4, :cond_9

    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/google/android/finsky/protos/ChallengeProtos$Country;

    const/16 v4, 0xa

    invoke-static {v4, v1}, Lcom/google/protobuf/micro/CodedOutputStreamMicro;->computeMessageSize(ILcom/google/protobuf/micro/MessageMicro;)I

    move-result v4

    add-int/2addr v3, v4

    goto :goto_3

    :cond_9
    iput v3, p0, Lcom/google/android/finsky/protos/ChallengeProtos$AddressChallenge;->cachedSize:I

    return v3
.end method

.method public getSupportedCountryCount()I
    .locals 1

    iget-object v0, p0, Lcom/google/android/finsky/protos/ChallengeProtos$AddressChallenge;->supportedCountry_:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->size()I

    move-result v0

    return v0
.end method

.method public getSupportedCountryList()Ljava/util/List;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/List",
            "<",
            "Lcom/google/android/finsky/protos/ChallengeProtos$Country;",
            ">;"
        }
    .end annotation

    iget-object v0, p0, Lcom/google/android/finsky/protos/ChallengeProtos$AddressChallenge;->supportedCountry_:Ljava/util/List;

    return-object v0
.end method

.method public getTitle()Ljava/lang/String;
    .locals 1

    iget-object v0, p0, Lcom/google/android/finsky/protos/ChallengeProtos$AddressChallenge;->title_:Ljava/lang/String;

    return-object v0
.end method

.method public hasAddress()Z
    .locals 1

    iget-boolean v0, p0, Lcom/google/android/finsky/protos/ChallengeProtos$AddressChallenge;->hasAddress:Z

    return v0
.end method

.method public hasDescriptionHtml()Z
    .locals 1

    iget-boolean v0, p0, Lcom/google/android/finsky/protos/ChallengeProtos$AddressChallenge;->hasDescriptionHtml:Z

    return v0
.end method

.method public hasErrorHtml()Z
    .locals 1

    iget-boolean v0, p0, Lcom/google/android/finsky/protos/ChallengeProtos$AddressChallenge;->hasErrorHtml:Z

    return v0
.end method

.method public hasResponseAddressParam()Z
    .locals 1

    iget-boolean v0, p0, Lcom/google/android/finsky/protos/ChallengeProtos$AddressChallenge;->hasResponseAddressParam:Z

    return v0
.end method

.method public hasResponseCheckboxesParam()Z
    .locals 1

    iget-boolean v0, p0, Lcom/google/android/finsky/protos/ChallengeProtos$AddressChallenge;->hasResponseCheckboxesParam:Z

    return v0
.end method

.method public hasTitle()Z
    .locals 1

    iget-boolean v0, p0, Lcom/google/android/finsky/protos/ChallengeProtos$AddressChallenge;->hasTitle:Z

    return v0
.end method

.method public mergeFrom(Lcom/google/protobuf/micro/CodedInputStreamMicro;)Lcom/google/android/finsky/protos/ChallengeProtos$AddressChallenge;
    .locals 3
    .param p1    # Lcom/google/protobuf/micro/CodedInputStreamMicro;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    :cond_0
    :goto_0
    invoke-virtual {p1}, Lcom/google/protobuf/micro/CodedInputStreamMicro;->readTag()I

    move-result v0

    sparse-switch v0, :sswitch_data_0

    invoke-virtual {p0, p1, v0}, Lcom/google/android/finsky/protos/ChallengeProtos$AddressChallenge;->parseUnknownField(Lcom/google/protobuf/micro/CodedInputStreamMicro;I)Z

    move-result v2

    if-nez v2, :cond_0

    :sswitch_0
    return-object p0

    :sswitch_1
    invoke-virtual {p1}, Lcom/google/protobuf/micro/CodedInputStreamMicro;->readString()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {p0, v2}, Lcom/google/android/finsky/protos/ChallengeProtos$AddressChallenge;->setResponseAddressParam(Ljava/lang/String;)Lcom/google/android/finsky/protos/ChallengeProtos$AddressChallenge;

    goto :goto_0

    :sswitch_2
    invoke-virtual {p1}, Lcom/google/protobuf/micro/CodedInputStreamMicro;->readString()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {p0, v2}, Lcom/google/android/finsky/protos/ChallengeProtos$AddressChallenge;->setResponseCheckboxesParam(Ljava/lang/String;)Lcom/google/android/finsky/protos/ChallengeProtos$AddressChallenge;

    goto :goto_0

    :sswitch_3
    invoke-virtual {p1}, Lcom/google/protobuf/micro/CodedInputStreamMicro;->readString()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {p0, v2}, Lcom/google/android/finsky/protos/ChallengeProtos$AddressChallenge;->setTitle(Ljava/lang/String;)Lcom/google/android/finsky/protos/ChallengeProtos$AddressChallenge;

    goto :goto_0

    :sswitch_4
    invoke-virtual {p1}, Lcom/google/protobuf/micro/CodedInputStreamMicro;->readString()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {p0, v2}, Lcom/google/android/finsky/protos/ChallengeProtos$AddressChallenge;->setDescriptionHtml(Ljava/lang/String;)Lcom/google/android/finsky/protos/ChallengeProtos$AddressChallenge;

    goto :goto_0

    :sswitch_5
    new-instance v1, Lcom/google/android/finsky/protos/ChallengeProtos$FormCheckbox;

    invoke-direct {v1}, Lcom/google/android/finsky/protos/ChallengeProtos$FormCheckbox;-><init>()V

    invoke-virtual {p1, v1}, Lcom/google/protobuf/micro/CodedInputStreamMicro;->readMessage(Lcom/google/protobuf/micro/MessageMicro;)V

    invoke-virtual {p0, v1}, Lcom/google/android/finsky/protos/ChallengeProtos$AddressChallenge;->addCheckbox(Lcom/google/android/finsky/protos/ChallengeProtos$FormCheckbox;)Lcom/google/android/finsky/protos/ChallengeProtos$AddressChallenge;

    goto :goto_0

    :sswitch_6
    new-instance v1, Lcom/google/android/finsky/protos/BillingAddress$Address;

    invoke-direct {v1}, Lcom/google/android/finsky/protos/BillingAddress$Address;-><init>()V

    invoke-virtual {p1, v1}, Lcom/google/protobuf/micro/CodedInputStreamMicro;->readMessage(Lcom/google/protobuf/micro/MessageMicro;)V

    invoke-virtual {p0, v1}, Lcom/google/android/finsky/protos/ChallengeProtos$AddressChallenge;->setAddress(Lcom/google/android/finsky/protos/BillingAddress$Address;)Lcom/google/android/finsky/protos/ChallengeProtos$AddressChallenge;

    goto :goto_0

    :sswitch_7
    new-instance v1, Lcom/google/android/finsky/protos/ChallengeProtos$InputValidationError;

    invoke-direct {v1}, Lcom/google/android/finsky/protos/ChallengeProtos$InputValidationError;-><init>()V

    invoke-virtual {p1, v1}, Lcom/google/protobuf/micro/CodedInputStreamMicro;->readMessage(Lcom/google/protobuf/micro/MessageMicro;)V

    invoke-virtual {p0, v1}, Lcom/google/android/finsky/protos/ChallengeProtos$AddressChallenge;->addErrorInputField(Lcom/google/android/finsky/protos/ChallengeProtos$InputValidationError;)Lcom/google/android/finsky/protos/ChallengeProtos$AddressChallenge;

    goto :goto_0

    :sswitch_8
    invoke-virtual {p1}, Lcom/google/protobuf/micro/CodedInputStreamMicro;->readString()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {p0, v2}, Lcom/google/android/finsky/protos/ChallengeProtos$AddressChallenge;->setErrorHtml(Ljava/lang/String;)Lcom/google/android/finsky/protos/ChallengeProtos$AddressChallenge;

    goto :goto_0

    :sswitch_9
    invoke-virtual {p1}, Lcom/google/protobuf/micro/CodedInputStreamMicro;->readInt32()I

    move-result v2

    invoke-virtual {p0, v2}, Lcom/google/android/finsky/protos/ChallengeProtos$AddressChallenge;->addRequiredField(I)Lcom/google/android/finsky/protos/ChallengeProtos$AddressChallenge;

    goto :goto_0

    :sswitch_a
    new-instance v1, Lcom/google/android/finsky/protos/ChallengeProtos$Country;

    invoke-direct {v1}, Lcom/google/android/finsky/protos/ChallengeProtos$Country;-><init>()V

    invoke-virtual {p1, v1}, Lcom/google/protobuf/micro/CodedInputStreamMicro;->readMessage(Lcom/google/protobuf/micro/MessageMicro;)V

    invoke-virtual {p0, v1}, Lcom/google/android/finsky/protos/ChallengeProtos$AddressChallenge;->addSupportedCountry(Lcom/google/android/finsky/protos/ChallengeProtos$Country;)Lcom/google/android/finsky/protos/ChallengeProtos$AddressChallenge;

    goto :goto_0

    :sswitch_data_0
    .sparse-switch
        0x0 -> :sswitch_0
        0xa -> :sswitch_1
        0x12 -> :sswitch_2
        0x1a -> :sswitch_3
        0x22 -> :sswitch_4
        0x2a -> :sswitch_5
        0x32 -> :sswitch_6
        0x3a -> :sswitch_7
        0x42 -> :sswitch_8
        0x48 -> :sswitch_9
        0x52 -> :sswitch_a
    .end sparse-switch
.end method

.method public bridge synthetic mergeFrom(Lcom/google/protobuf/micro/CodedInputStreamMicro;)Lcom/google/protobuf/micro/MessageMicro;
    .locals 1
    .param p1    # Lcom/google/protobuf/micro/CodedInputStreamMicro;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    invoke-virtual {p0, p1}, Lcom/google/android/finsky/protos/ChallengeProtos$AddressChallenge;->mergeFrom(Lcom/google/protobuf/micro/CodedInputStreamMicro;)Lcom/google/android/finsky/protos/ChallengeProtos$AddressChallenge;

    move-result-object v0

    return-object v0
.end method

.method public setAddress(Lcom/google/android/finsky/protos/BillingAddress$Address;)Lcom/google/android/finsky/protos/ChallengeProtos$AddressChallenge;
    .locals 1
    .param p1    # Lcom/google/android/finsky/protos/BillingAddress$Address;

    if-nez p1, :cond_0

    new-instance v0, Ljava/lang/NullPointerException;

    invoke-direct {v0}, Ljava/lang/NullPointerException;-><init>()V

    throw v0

    :cond_0
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/google/android/finsky/protos/ChallengeProtos$AddressChallenge;->hasAddress:Z

    iput-object p1, p0, Lcom/google/android/finsky/protos/ChallengeProtos$AddressChallenge;->address_:Lcom/google/android/finsky/protos/BillingAddress$Address;

    return-object p0
.end method

.method public setDescriptionHtml(Ljava/lang/String;)Lcom/google/android/finsky/protos/ChallengeProtos$AddressChallenge;
    .locals 1
    .param p1    # Ljava/lang/String;

    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/google/android/finsky/protos/ChallengeProtos$AddressChallenge;->hasDescriptionHtml:Z

    iput-object p1, p0, Lcom/google/android/finsky/protos/ChallengeProtos$AddressChallenge;->descriptionHtml_:Ljava/lang/String;

    return-object p0
.end method

.method public setErrorHtml(Ljava/lang/String;)Lcom/google/android/finsky/protos/ChallengeProtos$AddressChallenge;
    .locals 1
    .param p1    # Ljava/lang/String;

    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/google/android/finsky/protos/ChallengeProtos$AddressChallenge;->hasErrorHtml:Z

    iput-object p1, p0, Lcom/google/android/finsky/protos/ChallengeProtos$AddressChallenge;->errorHtml_:Ljava/lang/String;

    return-object p0
.end method

.method public setResponseAddressParam(Ljava/lang/String;)Lcom/google/android/finsky/protos/ChallengeProtos$AddressChallenge;
    .locals 1
    .param p1    # Ljava/lang/String;

    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/google/android/finsky/protos/ChallengeProtos$AddressChallenge;->hasResponseAddressParam:Z

    iput-object p1, p0, Lcom/google/android/finsky/protos/ChallengeProtos$AddressChallenge;->responseAddressParam_:Ljava/lang/String;

    return-object p0
.end method

.method public setResponseCheckboxesParam(Ljava/lang/String;)Lcom/google/android/finsky/protos/ChallengeProtos$AddressChallenge;
    .locals 1
    .param p1    # Ljava/lang/String;

    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/google/android/finsky/protos/ChallengeProtos$AddressChallenge;->hasResponseCheckboxesParam:Z

    iput-object p1, p0, Lcom/google/android/finsky/protos/ChallengeProtos$AddressChallenge;->responseCheckboxesParam_:Ljava/lang/String;

    return-object p0
.end method

.method public setTitle(Ljava/lang/String;)Lcom/google/android/finsky/protos/ChallengeProtos$AddressChallenge;
    .locals 1
    .param p1    # Ljava/lang/String;

    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/google/android/finsky/protos/ChallengeProtos$AddressChallenge;->hasTitle:Z

    iput-object p1, p0, Lcom/google/android/finsky/protos/ChallengeProtos$AddressChallenge;->title_:Ljava/lang/String;

    return-object p0
.end method

.method public writeTo(Lcom/google/protobuf/micro/CodedOutputStreamMicro;)V
    .locals 4
    .param p1    # Lcom/google/protobuf/micro/CodedOutputStreamMicro;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    invoke-virtual {p0}, Lcom/google/android/finsky/protos/ChallengeProtos$AddressChallenge;->hasResponseAddressParam()Z

    move-result v2

    if-eqz v2, :cond_0

    const/4 v2, 0x1

    invoke-virtual {p0}, Lcom/google/android/finsky/protos/ChallengeProtos$AddressChallenge;->getResponseAddressParam()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {p1, v2, v3}, Lcom/google/protobuf/micro/CodedOutputStreamMicro;->writeString(ILjava/lang/String;)V

    :cond_0
    invoke-virtual {p0}, Lcom/google/android/finsky/protos/ChallengeProtos$AddressChallenge;->hasResponseCheckboxesParam()Z

    move-result v2

    if-eqz v2, :cond_1

    const/4 v2, 0x2

    invoke-virtual {p0}, Lcom/google/android/finsky/protos/ChallengeProtos$AddressChallenge;->getResponseCheckboxesParam()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {p1, v2, v3}, Lcom/google/protobuf/micro/CodedOutputStreamMicro;->writeString(ILjava/lang/String;)V

    :cond_1
    invoke-virtual {p0}, Lcom/google/android/finsky/protos/ChallengeProtos$AddressChallenge;->hasTitle()Z

    move-result v2

    if-eqz v2, :cond_2

    const/4 v2, 0x3

    invoke-virtual {p0}, Lcom/google/android/finsky/protos/ChallengeProtos$AddressChallenge;->getTitle()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {p1, v2, v3}, Lcom/google/protobuf/micro/CodedOutputStreamMicro;->writeString(ILjava/lang/String;)V

    :cond_2
    invoke-virtual {p0}, Lcom/google/android/finsky/protos/ChallengeProtos$AddressChallenge;->hasDescriptionHtml()Z

    move-result v2

    if-eqz v2, :cond_3

    const/4 v2, 0x4

    invoke-virtual {p0}, Lcom/google/android/finsky/protos/ChallengeProtos$AddressChallenge;->getDescriptionHtml()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {p1, v2, v3}, Lcom/google/protobuf/micro/CodedOutputStreamMicro;->writeString(ILjava/lang/String;)V

    :cond_3
    invoke-virtual {p0}, Lcom/google/android/finsky/protos/ChallengeProtos$AddressChallenge;->getCheckboxList()Ljava/util/List;

    move-result-object v2

    invoke-interface {v2}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :goto_0
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v2

    if-eqz v2, :cond_4

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/finsky/protos/ChallengeProtos$FormCheckbox;

    const/4 v2, 0x5

    invoke-virtual {p1, v2, v0}, Lcom/google/protobuf/micro/CodedOutputStreamMicro;->writeMessage(ILcom/google/protobuf/micro/MessageMicro;)V

    goto :goto_0

    :cond_4
    invoke-virtual {p0}, Lcom/google/android/finsky/protos/ChallengeProtos$AddressChallenge;->hasAddress()Z

    move-result v2

    if-eqz v2, :cond_5

    const/4 v2, 0x6

    invoke-virtual {p0}, Lcom/google/android/finsky/protos/ChallengeProtos$AddressChallenge;->getAddress()Lcom/google/android/finsky/protos/BillingAddress$Address;

    move-result-object v3

    invoke-virtual {p1, v2, v3}, Lcom/google/protobuf/micro/CodedOutputStreamMicro;->writeMessage(ILcom/google/protobuf/micro/MessageMicro;)V

    :cond_5
    invoke-virtual {p0}, Lcom/google/android/finsky/protos/ChallengeProtos$AddressChallenge;->getErrorInputFieldList()Ljava/util/List;

    move-result-object v2

    invoke-interface {v2}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :goto_1
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v2

    if-eqz v2, :cond_6

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/finsky/protos/ChallengeProtos$InputValidationError;

    const/4 v2, 0x7

    invoke-virtual {p1, v2, v0}, Lcom/google/protobuf/micro/CodedOutputStreamMicro;->writeMessage(ILcom/google/protobuf/micro/MessageMicro;)V

    goto :goto_1

    :cond_6
    invoke-virtual {p0}, Lcom/google/android/finsky/protos/ChallengeProtos$AddressChallenge;->hasErrorHtml()Z

    move-result v2

    if-eqz v2, :cond_7

    const/16 v2, 0x8

    invoke-virtual {p0}, Lcom/google/android/finsky/protos/ChallengeProtos$AddressChallenge;->getErrorHtml()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {p1, v2, v3}, Lcom/google/protobuf/micro/CodedOutputStreamMicro;->writeString(ILjava/lang/String;)V

    :cond_7
    invoke-virtual {p0}, Lcom/google/android/finsky/protos/ChallengeProtos$AddressChallenge;->getRequiredFieldList()Ljava/util/List;

    move-result-object v2

    invoke-interface {v2}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :goto_2
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v2

    if-eqz v2, :cond_8

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/Integer;

    const/16 v2, 0x9

    invoke-virtual {v0}, Ljava/lang/Integer;->intValue()I

    move-result v3

    invoke-virtual {p1, v2, v3}, Lcom/google/protobuf/micro/CodedOutputStreamMicro;->writeInt32(II)V

    goto :goto_2

    :cond_8
    invoke-virtual {p0}, Lcom/google/android/finsky/protos/ChallengeProtos$AddressChallenge;->getSupportedCountryList()Ljava/util/List;

    move-result-object v2

    invoke-interface {v2}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :goto_3
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v2

    if-eqz v2, :cond_9

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/finsky/protos/ChallengeProtos$Country;

    const/16 v2, 0xa

    invoke-virtual {p1, v2, v0}, Lcom/google/protobuf/micro/CodedOutputStreamMicro;->writeMessage(ILcom/google/protobuf/micro/MessageMicro;)V

    goto :goto_3

    :cond_9
    return-void
.end method
