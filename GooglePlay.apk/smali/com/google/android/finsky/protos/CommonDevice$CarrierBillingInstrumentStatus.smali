.class public final Lcom/google/android/finsky/protos/CommonDevice$CarrierBillingInstrumentStatus;
.super Lcom/google/protobuf/micro/MessageMicro;
.source "CommonDevice.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/google/android/finsky/protos/CommonDevice;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x19
    name = "CarrierBillingInstrumentStatus"
.end annotation


# instance fields
.field private apiVersion_:I

.field private associationRequired_:Z

.field private cachedSize:I

.field private carrierPasswordPrompt_:Lcom/google/android/finsky/protos/CommonDevice$PasswordPrompt;

.field private carrierSupportPhoneNumber_:Ljava/lang/String;

.field private carrierTos_:Lcom/google/android/finsky/protos/CommonDevice$CarrierTos;

.field private deviceAssociation_:Lcom/google/android/finsky/protos/CommonDevice$DeviceAssociation;

.field private hasApiVersion:Z

.field private hasAssociationRequired:Z

.field private hasCarrierPasswordPrompt:Z

.field private hasCarrierSupportPhoneNumber:Z

.field private hasCarrierTos:Z

.field private hasDeviceAssociation:Z

.field private hasName:Z

.field private hasPasswordRequired:Z

.field private name_:Ljava/lang/String;

.field private passwordRequired_:Z


# direct methods
.method public constructor <init>()V
    .locals 2

    const/4 v1, 0x0

    const/4 v0, 0x0

    invoke-direct {p0}, Lcom/google/protobuf/micro/MessageMicro;-><init>()V

    iput-object v1, p0, Lcom/google/android/finsky/protos/CommonDevice$CarrierBillingInstrumentStatus;->carrierTos_:Lcom/google/android/finsky/protos/CommonDevice$CarrierTos;

    iput-boolean v0, p0, Lcom/google/android/finsky/protos/CommonDevice$CarrierBillingInstrumentStatus;->associationRequired_:Z

    iput-boolean v0, p0, Lcom/google/android/finsky/protos/CommonDevice$CarrierBillingInstrumentStatus;->passwordRequired_:Z

    iput-object v1, p0, Lcom/google/android/finsky/protos/CommonDevice$CarrierBillingInstrumentStatus;->carrierPasswordPrompt_:Lcom/google/android/finsky/protos/CommonDevice$PasswordPrompt;

    iput v0, p0, Lcom/google/android/finsky/protos/CommonDevice$CarrierBillingInstrumentStatus;->apiVersion_:I

    const-string v0, ""

    iput-object v0, p0, Lcom/google/android/finsky/protos/CommonDevice$CarrierBillingInstrumentStatus;->name_:Ljava/lang/String;

    iput-object v1, p0, Lcom/google/android/finsky/protos/CommonDevice$CarrierBillingInstrumentStatus;->deviceAssociation_:Lcom/google/android/finsky/protos/CommonDevice$DeviceAssociation;

    const-string v0, ""

    iput-object v0, p0, Lcom/google/android/finsky/protos/CommonDevice$CarrierBillingInstrumentStatus;->carrierSupportPhoneNumber_:Ljava/lang/String;

    const/4 v0, -0x1

    iput v0, p0, Lcom/google/android/finsky/protos/CommonDevice$CarrierBillingInstrumentStatus;->cachedSize:I

    return-void
.end method


# virtual methods
.method public getApiVersion()I
    .locals 1

    iget v0, p0, Lcom/google/android/finsky/protos/CommonDevice$CarrierBillingInstrumentStatus;->apiVersion_:I

    return v0
.end method

.method public getAssociationRequired()Z
    .locals 1

    iget-boolean v0, p0, Lcom/google/android/finsky/protos/CommonDevice$CarrierBillingInstrumentStatus;->associationRequired_:Z

    return v0
.end method

.method public getCachedSize()I
    .locals 1

    iget v0, p0, Lcom/google/android/finsky/protos/CommonDevice$CarrierBillingInstrumentStatus;->cachedSize:I

    if-gez v0, :cond_0

    invoke-virtual {p0}, Lcom/google/android/finsky/protos/CommonDevice$CarrierBillingInstrumentStatus;->getSerializedSize()I

    :cond_0
    iget v0, p0, Lcom/google/android/finsky/protos/CommonDevice$CarrierBillingInstrumentStatus;->cachedSize:I

    return v0
.end method

.method public getCarrierPasswordPrompt()Lcom/google/android/finsky/protos/CommonDevice$PasswordPrompt;
    .locals 1

    iget-object v0, p0, Lcom/google/android/finsky/protos/CommonDevice$CarrierBillingInstrumentStatus;->carrierPasswordPrompt_:Lcom/google/android/finsky/protos/CommonDevice$PasswordPrompt;

    return-object v0
.end method

.method public getCarrierSupportPhoneNumber()Ljava/lang/String;
    .locals 1

    iget-object v0, p0, Lcom/google/android/finsky/protos/CommonDevice$CarrierBillingInstrumentStatus;->carrierSupportPhoneNumber_:Ljava/lang/String;

    return-object v0
.end method

.method public getCarrierTos()Lcom/google/android/finsky/protos/CommonDevice$CarrierTos;
    .locals 1

    iget-object v0, p0, Lcom/google/android/finsky/protos/CommonDevice$CarrierBillingInstrumentStatus;->carrierTos_:Lcom/google/android/finsky/protos/CommonDevice$CarrierTos;

    return-object v0
.end method

.method public getDeviceAssociation()Lcom/google/android/finsky/protos/CommonDevice$DeviceAssociation;
    .locals 1

    iget-object v0, p0, Lcom/google/android/finsky/protos/CommonDevice$CarrierBillingInstrumentStatus;->deviceAssociation_:Lcom/google/android/finsky/protos/CommonDevice$DeviceAssociation;

    return-object v0
.end method

.method public getName()Ljava/lang/String;
    .locals 1

    iget-object v0, p0, Lcom/google/android/finsky/protos/CommonDevice$CarrierBillingInstrumentStatus;->name_:Ljava/lang/String;

    return-object v0
.end method

.method public getPasswordRequired()Z
    .locals 1

    iget-boolean v0, p0, Lcom/google/android/finsky/protos/CommonDevice$CarrierBillingInstrumentStatus;->passwordRequired_:Z

    return v0
.end method

.method public getSerializedSize()I
    .locals 3

    const/4 v0, 0x0

    invoke-virtual {p0}, Lcom/google/android/finsky/protos/CommonDevice$CarrierBillingInstrumentStatus;->hasCarrierTos()Z

    move-result v1

    if-eqz v1, :cond_0

    const/4 v1, 0x1

    invoke-virtual {p0}, Lcom/google/android/finsky/protos/CommonDevice$CarrierBillingInstrumentStatus;->getCarrierTos()Lcom/google/android/finsky/protos/CommonDevice$CarrierTos;

    move-result-object v2

    invoke-static {v1, v2}, Lcom/google/protobuf/micro/CodedOutputStreamMicro;->computeMessageSize(ILcom/google/protobuf/micro/MessageMicro;)I

    move-result v1

    add-int/2addr v0, v1

    :cond_0
    invoke-virtual {p0}, Lcom/google/android/finsky/protos/CommonDevice$CarrierBillingInstrumentStatus;->hasAssociationRequired()Z

    move-result v1

    if-eqz v1, :cond_1

    const/4 v1, 0x2

    invoke-virtual {p0}, Lcom/google/android/finsky/protos/CommonDevice$CarrierBillingInstrumentStatus;->getAssociationRequired()Z

    move-result v2

    invoke-static {v1, v2}, Lcom/google/protobuf/micro/CodedOutputStreamMicro;->computeBoolSize(IZ)I

    move-result v1

    add-int/2addr v0, v1

    :cond_1
    invoke-virtual {p0}, Lcom/google/android/finsky/protos/CommonDevice$CarrierBillingInstrumentStatus;->hasPasswordRequired()Z

    move-result v1

    if-eqz v1, :cond_2

    const/4 v1, 0x3

    invoke-virtual {p0}, Lcom/google/android/finsky/protos/CommonDevice$CarrierBillingInstrumentStatus;->getPasswordRequired()Z

    move-result v2

    invoke-static {v1, v2}, Lcom/google/protobuf/micro/CodedOutputStreamMicro;->computeBoolSize(IZ)I

    move-result v1

    add-int/2addr v0, v1

    :cond_2
    invoke-virtual {p0}, Lcom/google/android/finsky/protos/CommonDevice$CarrierBillingInstrumentStatus;->hasCarrierPasswordPrompt()Z

    move-result v1

    if-eqz v1, :cond_3

    const/4 v1, 0x4

    invoke-virtual {p0}, Lcom/google/android/finsky/protos/CommonDevice$CarrierBillingInstrumentStatus;->getCarrierPasswordPrompt()Lcom/google/android/finsky/protos/CommonDevice$PasswordPrompt;

    move-result-object v2

    invoke-static {v1, v2}, Lcom/google/protobuf/micro/CodedOutputStreamMicro;->computeMessageSize(ILcom/google/protobuf/micro/MessageMicro;)I

    move-result v1

    add-int/2addr v0, v1

    :cond_3
    invoke-virtual {p0}, Lcom/google/android/finsky/protos/CommonDevice$CarrierBillingInstrumentStatus;->hasApiVersion()Z

    move-result v1

    if-eqz v1, :cond_4

    const/4 v1, 0x5

    invoke-virtual {p0}, Lcom/google/android/finsky/protos/CommonDevice$CarrierBillingInstrumentStatus;->getApiVersion()I

    move-result v2

    invoke-static {v1, v2}, Lcom/google/protobuf/micro/CodedOutputStreamMicro;->computeInt32Size(II)I

    move-result v1

    add-int/2addr v0, v1

    :cond_4
    invoke-virtual {p0}, Lcom/google/android/finsky/protos/CommonDevice$CarrierBillingInstrumentStatus;->hasName()Z

    move-result v1

    if-eqz v1, :cond_5

    const/4 v1, 0x6

    invoke-virtual {p0}, Lcom/google/android/finsky/protos/CommonDevice$CarrierBillingInstrumentStatus;->getName()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2}, Lcom/google/protobuf/micro/CodedOutputStreamMicro;->computeStringSize(ILjava/lang/String;)I

    move-result v1

    add-int/2addr v0, v1

    :cond_5
    invoke-virtual {p0}, Lcom/google/android/finsky/protos/CommonDevice$CarrierBillingInstrumentStatus;->hasDeviceAssociation()Z

    move-result v1

    if-eqz v1, :cond_6

    const/4 v1, 0x7

    invoke-virtual {p0}, Lcom/google/android/finsky/protos/CommonDevice$CarrierBillingInstrumentStatus;->getDeviceAssociation()Lcom/google/android/finsky/protos/CommonDevice$DeviceAssociation;

    move-result-object v2

    invoke-static {v1, v2}, Lcom/google/protobuf/micro/CodedOutputStreamMicro;->computeMessageSize(ILcom/google/protobuf/micro/MessageMicro;)I

    move-result v1

    add-int/2addr v0, v1

    :cond_6
    invoke-virtual {p0}, Lcom/google/android/finsky/protos/CommonDevice$CarrierBillingInstrumentStatus;->hasCarrierSupportPhoneNumber()Z

    move-result v1

    if-eqz v1, :cond_7

    const/16 v1, 0x8

    invoke-virtual {p0}, Lcom/google/android/finsky/protos/CommonDevice$CarrierBillingInstrumentStatus;->getCarrierSupportPhoneNumber()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2}, Lcom/google/protobuf/micro/CodedOutputStreamMicro;->computeStringSize(ILjava/lang/String;)I

    move-result v1

    add-int/2addr v0, v1

    :cond_7
    iput v0, p0, Lcom/google/android/finsky/protos/CommonDevice$CarrierBillingInstrumentStatus;->cachedSize:I

    return v0
.end method

.method public hasApiVersion()Z
    .locals 1

    iget-boolean v0, p0, Lcom/google/android/finsky/protos/CommonDevice$CarrierBillingInstrumentStatus;->hasApiVersion:Z

    return v0
.end method

.method public hasAssociationRequired()Z
    .locals 1

    iget-boolean v0, p0, Lcom/google/android/finsky/protos/CommonDevice$CarrierBillingInstrumentStatus;->hasAssociationRequired:Z

    return v0
.end method

.method public hasCarrierPasswordPrompt()Z
    .locals 1

    iget-boolean v0, p0, Lcom/google/android/finsky/protos/CommonDevice$CarrierBillingInstrumentStatus;->hasCarrierPasswordPrompt:Z

    return v0
.end method

.method public hasCarrierSupportPhoneNumber()Z
    .locals 1

    iget-boolean v0, p0, Lcom/google/android/finsky/protos/CommonDevice$CarrierBillingInstrumentStatus;->hasCarrierSupportPhoneNumber:Z

    return v0
.end method

.method public hasCarrierTos()Z
    .locals 1

    iget-boolean v0, p0, Lcom/google/android/finsky/protos/CommonDevice$CarrierBillingInstrumentStatus;->hasCarrierTos:Z

    return v0
.end method

.method public hasDeviceAssociation()Z
    .locals 1

    iget-boolean v0, p0, Lcom/google/android/finsky/protos/CommonDevice$CarrierBillingInstrumentStatus;->hasDeviceAssociation:Z

    return v0
.end method

.method public hasName()Z
    .locals 1

    iget-boolean v0, p0, Lcom/google/android/finsky/protos/CommonDevice$CarrierBillingInstrumentStatus;->hasName:Z

    return v0
.end method

.method public hasPasswordRequired()Z
    .locals 1

    iget-boolean v0, p0, Lcom/google/android/finsky/protos/CommonDevice$CarrierBillingInstrumentStatus;->hasPasswordRequired:Z

    return v0
.end method

.method public mergeFrom(Lcom/google/protobuf/micro/CodedInputStreamMicro;)Lcom/google/android/finsky/protos/CommonDevice$CarrierBillingInstrumentStatus;
    .locals 3
    .param p1    # Lcom/google/protobuf/micro/CodedInputStreamMicro;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    :cond_0
    :goto_0
    invoke-virtual {p1}, Lcom/google/protobuf/micro/CodedInputStreamMicro;->readTag()I

    move-result v0

    sparse-switch v0, :sswitch_data_0

    invoke-virtual {p0, p1, v0}, Lcom/google/android/finsky/protos/CommonDevice$CarrierBillingInstrumentStatus;->parseUnknownField(Lcom/google/protobuf/micro/CodedInputStreamMicro;I)Z

    move-result v2

    if-nez v2, :cond_0

    :sswitch_0
    return-object p0

    :sswitch_1
    new-instance v1, Lcom/google/android/finsky/protos/CommonDevice$CarrierTos;

    invoke-direct {v1}, Lcom/google/android/finsky/protos/CommonDevice$CarrierTos;-><init>()V

    invoke-virtual {p1, v1}, Lcom/google/protobuf/micro/CodedInputStreamMicro;->readMessage(Lcom/google/protobuf/micro/MessageMicro;)V

    invoke-virtual {p0, v1}, Lcom/google/android/finsky/protos/CommonDevice$CarrierBillingInstrumentStatus;->setCarrierTos(Lcom/google/android/finsky/protos/CommonDevice$CarrierTos;)Lcom/google/android/finsky/protos/CommonDevice$CarrierBillingInstrumentStatus;

    goto :goto_0

    :sswitch_2
    invoke-virtual {p1}, Lcom/google/protobuf/micro/CodedInputStreamMicro;->readBool()Z

    move-result v2

    invoke-virtual {p0, v2}, Lcom/google/android/finsky/protos/CommonDevice$CarrierBillingInstrumentStatus;->setAssociationRequired(Z)Lcom/google/android/finsky/protos/CommonDevice$CarrierBillingInstrumentStatus;

    goto :goto_0

    :sswitch_3
    invoke-virtual {p1}, Lcom/google/protobuf/micro/CodedInputStreamMicro;->readBool()Z

    move-result v2

    invoke-virtual {p0, v2}, Lcom/google/android/finsky/protos/CommonDevice$CarrierBillingInstrumentStatus;->setPasswordRequired(Z)Lcom/google/android/finsky/protos/CommonDevice$CarrierBillingInstrumentStatus;

    goto :goto_0

    :sswitch_4
    new-instance v1, Lcom/google/android/finsky/protos/CommonDevice$PasswordPrompt;

    invoke-direct {v1}, Lcom/google/android/finsky/protos/CommonDevice$PasswordPrompt;-><init>()V

    invoke-virtual {p1, v1}, Lcom/google/protobuf/micro/CodedInputStreamMicro;->readMessage(Lcom/google/protobuf/micro/MessageMicro;)V

    invoke-virtual {p0, v1}, Lcom/google/android/finsky/protos/CommonDevice$CarrierBillingInstrumentStatus;->setCarrierPasswordPrompt(Lcom/google/android/finsky/protos/CommonDevice$PasswordPrompt;)Lcom/google/android/finsky/protos/CommonDevice$CarrierBillingInstrumentStatus;

    goto :goto_0

    :sswitch_5
    invoke-virtual {p1}, Lcom/google/protobuf/micro/CodedInputStreamMicro;->readInt32()I

    move-result v2

    invoke-virtual {p0, v2}, Lcom/google/android/finsky/protos/CommonDevice$CarrierBillingInstrumentStatus;->setApiVersion(I)Lcom/google/android/finsky/protos/CommonDevice$CarrierBillingInstrumentStatus;

    goto :goto_0

    :sswitch_6
    invoke-virtual {p1}, Lcom/google/protobuf/micro/CodedInputStreamMicro;->readString()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {p0, v2}, Lcom/google/android/finsky/protos/CommonDevice$CarrierBillingInstrumentStatus;->setName(Ljava/lang/String;)Lcom/google/android/finsky/protos/CommonDevice$CarrierBillingInstrumentStatus;

    goto :goto_0

    :sswitch_7
    new-instance v1, Lcom/google/android/finsky/protos/CommonDevice$DeviceAssociation;

    invoke-direct {v1}, Lcom/google/android/finsky/protos/CommonDevice$DeviceAssociation;-><init>()V

    invoke-virtual {p1, v1}, Lcom/google/protobuf/micro/CodedInputStreamMicro;->readMessage(Lcom/google/protobuf/micro/MessageMicro;)V

    invoke-virtual {p0, v1}, Lcom/google/android/finsky/protos/CommonDevice$CarrierBillingInstrumentStatus;->setDeviceAssociation(Lcom/google/android/finsky/protos/CommonDevice$DeviceAssociation;)Lcom/google/android/finsky/protos/CommonDevice$CarrierBillingInstrumentStatus;

    goto :goto_0

    :sswitch_8
    invoke-virtual {p1}, Lcom/google/protobuf/micro/CodedInputStreamMicro;->readString()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {p0, v2}, Lcom/google/android/finsky/protos/CommonDevice$CarrierBillingInstrumentStatus;->setCarrierSupportPhoneNumber(Ljava/lang/String;)Lcom/google/android/finsky/protos/CommonDevice$CarrierBillingInstrumentStatus;

    goto :goto_0

    :sswitch_data_0
    .sparse-switch
        0x0 -> :sswitch_0
        0xa -> :sswitch_1
        0x10 -> :sswitch_2
        0x18 -> :sswitch_3
        0x22 -> :sswitch_4
        0x28 -> :sswitch_5
        0x32 -> :sswitch_6
        0x3a -> :sswitch_7
        0x42 -> :sswitch_8
    .end sparse-switch
.end method

.method public bridge synthetic mergeFrom(Lcom/google/protobuf/micro/CodedInputStreamMicro;)Lcom/google/protobuf/micro/MessageMicro;
    .locals 1
    .param p1    # Lcom/google/protobuf/micro/CodedInputStreamMicro;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    invoke-virtual {p0, p1}, Lcom/google/android/finsky/protos/CommonDevice$CarrierBillingInstrumentStatus;->mergeFrom(Lcom/google/protobuf/micro/CodedInputStreamMicro;)Lcom/google/android/finsky/protos/CommonDevice$CarrierBillingInstrumentStatus;

    move-result-object v0

    return-object v0
.end method

.method public setApiVersion(I)Lcom/google/android/finsky/protos/CommonDevice$CarrierBillingInstrumentStatus;
    .locals 1
    .param p1    # I

    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/google/android/finsky/protos/CommonDevice$CarrierBillingInstrumentStatus;->hasApiVersion:Z

    iput p1, p0, Lcom/google/android/finsky/protos/CommonDevice$CarrierBillingInstrumentStatus;->apiVersion_:I

    return-object p0
.end method

.method public setAssociationRequired(Z)Lcom/google/android/finsky/protos/CommonDevice$CarrierBillingInstrumentStatus;
    .locals 1
    .param p1    # Z

    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/google/android/finsky/protos/CommonDevice$CarrierBillingInstrumentStatus;->hasAssociationRequired:Z

    iput-boolean p1, p0, Lcom/google/android/finsky/protos/CommonDevice$CarrierBillingInstrumentStatus;->associationRequired_:Z

    return-object p0
.end method

.method public setCarrierPasswordPrompt(Lcom/google/android/finsky/protos/CommonDevice$PasswordPrompt;)Lcom/google/android/finsky/protos/CommonDevice$CarrierBillingInstrumentStatus;
    .locals 1
    .param p1    # Lcom/google/android/finsky/protos/CommonDevice$PasswordPrompt;

    if-nez p1, :cond_0

    new-instance v0, Ljava/lang/NullPointerException;

    invoke-direct {v0}, Ljava/lang/NullPointerException;-><init>()V

    throw v0

    :cond_0
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/google/android/finsky/protos/CommonDevice$CarrierBillingInstrumentStatus;->hasCarrierPasswordPrompt:Z

    iput-object p1, p0, Lcom/google/android/finsky/protos/CommonDevice$CarrierBillingInstrumentStatus;->carrierPasswordPrompt_:Lcom/google/android/finsky/protos/CommonDevice$PasswordPrompt;

    return-object p0
.end method

.method public setCarrierSupportPhoneNumber(Ljava/lang/String;)Lcom/google/android/finsky/protos/CommonDevice$CarrierBillingInstrumentStatus;
    .locals 1
    .param p1    # Ljava/lang/String;

    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/google/android/finsky/protos/CommonDevice$CarrierBillingInstrumentStatus;->hasCarrierSupportPhoneNumber:Z

    iput-object p1, p0, Lcom/google/android/finsky/protos/CommonDevice$CarrierBillingInstrumentStatus;->carrierSupportPhoneNumber_:Ljava/lang/String;

    return-object p0
.end method

.method public setCarrierTos(Lcom/google/android/finsky/protos/CommonDevice$CarrierTos;)Lcom/google/android/finsky/protos/CommonDevice$CarrierBillingInstrumentStatus;
    .locals 1
    .param p1    # Lcom/google/android/finsky/protos/CommonDevice$CarrierTos;

    if-nez p1, :cond_0

    new-instance v0, Ljava/lang/NullPointerException;

    invoke-direct {v0}, Ljava/lang/NullPointerException;-><init>()V

    throw v0

    :cond_0
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/google/android/finsky/protos/CommonDevice$CarrierBillingInstrumentStatus;->hasCarrierTos:Z

    iput-object p1, p0, Lcom/google/android/finsky/protos/CommonDevice$CarrierBillingInstrumentStatus;->carrierTos_:Lcom/google/android/finsky/protos/CommonDevice$CarrierTos;

    return-object p0
.end method

.method public setDeviceAssociation(Lcom/google/android/finsky/protos/CommonDevice$DeviceAssociation;)Lcom/google/android/finsky/protos/CommonDevice$CarrierBillingInstrumentStatus;
    .locals 1
    .param p1    # Lcom/google/android/finsky/protos/CommonDevice$DeviceAssociation;

    if-nez p1, :cond_0

    new-instance v0, Ljava/lang/NullPointerException;

    invoke-direct {v0}, Ljava/lang/NullPointerException;-><init>()V

    throw v0

    :cond_0
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/google/android/finsky/protos/CommonDevice$CarrierBillingInstrumentStatus;->hasDeviceAssociation:Z

    iput-object p1, p0, Lcom/google/android/finsky/protos/CommonDevice$CarrierBillingInstrumentStatus;->deviceAssociation_:Lcom/google/android/finsky/protos/CommonDevice$DeviceAssociation;

    return-object p0
.end method

.method public setName(Ljava/lang/String;)Lcom/google/android/finsky/protos/CommonDevice$CarrierBillingInstrumentStatus;
    .locals 1
    .param p1    # Ljava/lang/String;

    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/google/android/finsky/protos/CommonDevice$CarrierBillingInstrumentStatus;->hasName:Z

    iput-object p1, p0, Lcom/google/android/finsky/protos/CommonDevice$CarrierBillingInstrumentStatus;->name_:Ljava/lang/String;

    return-object p0
.end method

.method public setPasswordRequired(Z)Lcom/google/android/finsky/protos/CommonDevice$CarrierBillingInstrumentStatus;
    .locals 1
    .param p1    # Z

    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/google/android/finsky/protos/CommonDevice$CarrierBillingInstrumentStatus;->hasPasswordRequired:Z

    iput-boolean p1, p0, Lcom/google/android/finsky/protos/CommonDevice$CarrierBillingInstrumentStatus;->passwordRequired_:Z

    return-object p0
.end method

.method public writeTo(Lcom/google/protobuf/micro/CodedOutputStreamMicro;)V
    .locals 2
    .param p1    # Lcom/google/protobuf/micro/CodedOutputStreamMicro;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    invoke-virtual {p0}, Lcom/google/android/finsky/protos/CommonDevice$CarrierBillingInstrumentStatus;->hasCarrierTos()Z

    move-result v0

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    invoke-virtual {p0}, Lcom/google/android/finsky/protos/CommonDevice$CarrierBillingInstrumentStatus;->getCarrierTos()Lcom/google/android/finsky/protos/CommonDevice$CarrierTos;

    move-result-object v1

    invoke-virtual {p1, v0, v1}, Lcom/google/protobuf/micro/CodedOutputStreamMicro;->writeMessage(ILcom/google/protobuf/micro/MessageMicro;)V

    :cond_0
    invoke-virtual {p0}, Lcom/google/android/finsky/protos/CommonDevice$CarrierBillingInstrumentStatus;->hasAssociationRequired()Z

    move-result v0

    if-eqz v0, :cond_1

    const/4 v0, 0x2

    invoke-virtual {p0}, Lcom/google/android/finsky/protos/CommonDevice$CarrierBillingInstrumentStatus;->getAssociationRequired()Z

    move-result v1

    invoke-virtual {p1, v0, v1}, Lcom/google/protobuf/micro/CodedOutputStreamMicro;->writeBool(IZ)V

    :cond_1
    invoke-virtual {p0}, Lcom/google/android/finsky/protos/CommonDevice$CarrierBillingInstrumentStatus;->hasPasswordRequired()Z

    move-result v0

    if-eqz v0, :cond_2

    const/4 v0, 0x3

    invoke-virtual {p0}, Lcom/google/android/finsky/protos/CommonDevice$CarrierBillingInstrumentStatus;->getPasswordRequired()Z

    move-result v1

    invoke-virtual {p1, v0, v1}, Lcom/google/protobuf/micro/CodedOutputStreamMicro;->writeBool(IZ)V

    :cond_2
    invoke-virtual {p0}, Lcom/google/android/finsky/protos/CommonDevice$CarrierBillingInstrumentStatus;->hasCarrierPasswordPrompt()Z

    move-result v0

    if-eqz v0, :cond_3

    const/4 v0, 0x4

    invoke-virtual {p0}, Lcom/google/android/finsky/protos/CommonDevice$CarrierBillingInstrumentStatus;->getCarrierPasswordPrompt()Lcom/google/android/finsky/protos/CommonDevice$PasswordPrompt;

    move-result-object v1

    invoke-virtual {p1, v0, v1}, Lcom/google/protobuf/micro/CodedOutputStreamMicro;->writeMessage(ILcom/google/protobuf/micro/MessageMicro;)V

    :cond_3
    invoke-virtual {p0}, Lcom/google/android/finsky/protos/CommonDevice$CarrierBillingInstrumentStatus;->hasApiVersion()Z

    move-result v0

    if-eqz v0, :cond_4

    const/4 v0, 0x5

    invoke-virtual {p0}, Lcom/google/android/finsky/protos/CommonDevice$CarrierBillingInstrumentStatus;->getApiVersion()I

    move-result v1

    invoke-virtual {p1, v0, v1}, Lcom/google/protobuf/micro/CodedOutputStreamMicro;->writeInt32(II)V

    :cond_4
    invoke-virtual {p0}, Lcom/google/android/finsky/protos/CommonDevice$CarrierBillingInstrumentStatus;->hasName()Z

    move-result v0

    if-eqz v0, :cond_5

    const/4 v0, 0x6

    invoke-virtual {p0}, Lcom/google/android/finsky/protos/CommonDevice$CarrierBillingInstrumentStatus;->getName()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {p1, v0, v1}, Lcom/google/protobuf/micro/CodedOutputStreamMicro;->writeString(ILjava/lang/String;)V

    :cond_5
    invoke-virtual {p0}, Lcom/google/android/finsky/protos/CommonDevice$CarrierBillingInstrumentStatus;->hasDeviceAssociation()Z

    move-result v0

    if-eqz v0, :cond_6

    const/4 v0, 0x7

    invoke-virtual {p0}, Lcom/google/android/finsky/protos/CommonDevice$CarrierBillingInstrumentStatus;->getDeviceAssociation()Lcom/google/android/finsky/protos/CommonDevice$DeviceAssociation;

    move-result-object v1

    invoke-virtual {p1, v0, v1}, Lcom/google/protobuf/micro/CodedOutputStreamMicro;->writeMessage(ILcom/google/protobuf/micro/MessageMicro;)V

    :cond_6
    invoke-virtual {p0}, Lcom/google/android/finsky/protos/CommonDevice$CarrierBillingInstrumentStatus;->hasCarrierSupportPhoneNumber()Z

    move-result v0

    if-eqz v0, :cond_7

    const/16 v0, 0x8

    invoke-virtual {p0}, Lcom/google/android/finsky/protos/CommonDevice$CarrierBillingInstrumentStatus;->getCarrierSupportPhoneNumber()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {p1, v0, v1}, Lcom/google/protobuf/micro/CodedOutputStreamMicro;->writeString(ILjava/lang/String;)V

    :cond_7
    return-void
.end method
