.class public final Lcom/google/android/finsky/protos/Purchase$ClientCart;
.super Lcom/google/protobuf/micro/MessageMicro;
.source "Purchase.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/google/android/finsky/protos/Purchase;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x19
    name = "ClientCart"
.end annotation


# instance fields
.field private addInstrumentPromptHtml_:Ljava/lang/String;

.field private buttonText_:Ljava/lang/String;

.field private cachedSize:I

.field private completePurchaseChallenge_:Lcom/google/android/finsky/protos/ChallengeProtos$Challenge;

.field private detailHtml_:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation
.end field

.field private extendedDetailHtml_:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation
.end field

.field private footerHtml_:Ljava/lang/String;

.field private formattedPrice_:Ljava/lang/String;

.field private hasAddInstrumentPromptHtml:Z

.field private hasButtonText:Z

.field private hasCompletePurchaseChallenge:Z

.field private hasFooterHtml:Z

.field private hasFormattedPrice:Z

.field private hasInstrument:Z

.field private hasPriceByline:Z

.field private hasPurchaseContextToken:Z

.field private hasTitle:Z

.field private instrument_:Lcom/google/android/finsky/protos/CommonDevice$Instrument;

.field private priceByline_:Ljava/lang/String;

.field private purchaseContextToken_:Ljava/lang/String;

.field private title_:Ljava/lang/String;


# direct methods
.method public constructor <init>()V
    .locals 2

    const/4 v1, 0x0

    invoke-direct {p0}, Lcom/google/protobuf/micro/MessageMicro;-><init>()V

    const-string v0, ""

    iput-object v0, p0, Lcom/google/android/finsky/protos/Purchase$ClientCart;->title_:Ljava/lang/String;

    const-string v0, ""

    iput-object v0, p0, Lcom/google/android/finsky/protos/Purchase$ClientCart;->formattedPrice_:Ljava/lang/String;

    const-string v0, ""

    iput-object v0, p0, Lcom/google/android/finsky/protos/Purchase$ClientCart;->priceByline_:Ljava/lang/String;

    const-string v0, ""

    iput-object v0, p0, Lcom/google/android/finsky/protos/Purchase$ClientCart;->purchaseContextToken_:Ljava/lang/String;

    iput-object v1, p0, Lcom/google/android/finsky/protos/Purchase$ClientCart;->instrument_:Lcom/google/android/finsky/protos/CommonDevice$Instrument;

    invoke-static {}, Ljava/util/Collections;->emptyList()Ljava/util/List;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/finsky/protos/Purchase$ClientCart;->detailHtml_:Ljava/util/List;

    invoke-static {}, Ljava/util/Collections;->emptyList()Ljava/util/List;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/finsky/protos/Purchase$ClientCart;->extendedDetailHtml_:Ljava/util/List;

    const-string v0, ""

    iput-object v0, p0, Lcom/google/android/finsky/protos/Purchase$ClientCart;->footerHtml_:Ljava/lang/String;

    const-string v0, ""

    iput-object v0, p0, Lcom/google/android/finsky/protos/Purchase$ClientCart;->addInstrumentPromptHtml_:Ljava/lang/String;

    const-string v0, ""

    iput-object v0, p0, Lcom/google/android/finsky/protos/Purchase$ClientCart;->buttonText_:Ljava/lang/String;

    iput-object v1, p0, Lcom/google/android/finsky/protos/Purchase$ClientCart;->completePurchaseChallenge_:Lcom/google/android/finsky/protos/ChallengeProtos$Challenge;

    const/4 v0, -0x1

    iput v0, p0, Lcom/google/android/finsky/protos/Purchase$ClientCart;->cachedSize:I

    return-void
.end method


# virtual methods
.method public addDetailHtml(Ljava/lang/String;)Lcom/google/android/finsky/protos/Purchase$ClientCart;
    .locals 1
    .param p1    # Ljava/lang/String;

    if-nez p1, :cond_0

    new-instance v0, Ljava/lang/NullPointerException;

    invoke-direct {v0}, Ljava/lang/NullPointerException;-><init>()V

    throw v0

    :cond_0
    iget-object v0, p0, Lcom/google/android/finsky/protos/Purchase$ClientCart;->detailHtml_:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->isEmpty()Z

    move-result v0

    if-eqz v0, :cond_1

    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, Lcom/google/android/finsky/protos/Purchase$ClientCart;->detailHtml_:Ljava/util/List;

    :cond_1
    iget-object v0, p0, Lcom/google/android/finsky/protos/Purchase$ClientCart;->detailHtml_:Ljava/util/List;

    invoke-interface {v0, p1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    return-object p0
.end method

.method public addExtendedDetailHtml(Ljava/lang/String;)Lcom/google/android/finsky/protos/Purchase$ClientCart;
    .locals 1
    .param p1    # Ljava/lang/String;

    if-nez p1, :cond_0

    new-instance v0, Ljava/lang/NullPointerException;

    invoke-direct {v0}, Ljava/lang/NullPointerException;-><init>()V

    throw v0

    :cond_0
    iget-object v0, p0, Lcom/google/android/finsky/protos/Purchase$ClientCart;->extendedDetailHtml_:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->isEmpty()Z

    move-result v0

    if-eqz v0, :cond_1

    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, Lcom/google/android/finsky/protos/Purchase$ClientCart;->extendedDetailHtml_:Ljava/util/List;

    :cond_1
    iget-object v0, p0, Lcom/google/android/finsky/protos/Purchase$ClientCart;->extendedDetailHtml_:Ljava/util/List;

    invoke-interface {v0, p1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    return-object p0
.end method

.method public getAddInstrumentPromptHtml()Ljava/lang/String;
    .locals 1

    iget-object v0, p0, Lcom/google/android/finsky/protos/Purchase$ClientCart;->addInstrumentPromptHtml_:Ljava/lang/String;

    return-object v0
.end method

.method public getButtonText()Ljava/lang/String;
    .locals 1

    iget-object v0, p0, Lcom/google/android/finsky/protos/Purchase$ClientCart;->buttonText_:Ljava/lang/String;

    return-object v0
.end method

.method public getCachedSize()I
    .locals 1

    iget v0, p0, Lcom/google/android/finsky/protos/Purchase$ClientCart;->cachedSize:I

    if-gez v0, :cond_0

    invoke-virtual {p0}, Lcom/google/android/finsky/protos/Purchase$ClientCart;->getSerializedSize()I

    :cond_0
    iget v0, p0, Lcom/google/android/finsky/protos/Purchase$ClientCart;->cachedSize:I

    return v0
.end method

.method public getCompletePurchaseChallenge()Lcom/google/android/finsky/protos/ChallengeProtos$Challenge;
    .locals 1

    iget-object v0, p0, Lcom/google/android/finsky/protos/Purchase$ClientCart;->completePurchaseChallenge_:Lcom/google/android/finsky/protos/ChallengeProtos$Challenge;

    return-object v0
.end method

.method public getDetailHtmlList()Ljava/util/List;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/List",
            "<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation

    iget-object v0, p0, Lcom/google/android/finsky/protos/Purchase$ClientCart;->detailHtml_:Ljava/util/List;

    return-object v0
.end method

.method public getExtendedDetailHtmlList()Ljava/util/List;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/List",
            "<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation

    iget-object v0, p0, Lcom/google/android/finsky/protos/Purchase$ClientCart;->extendedDetailHtml_:Ljava/util/List;

    return-object v0
.end method

.method public getFooterHtml()Ljava/lang/String;
    .locals 1

    iget-object v0, p0, Lcom/google/android/finsky/protos/Purchase$ClientCart;->footerHtml_:Ljava/lang/String;

    return-object v0
.end method

.method public getFormattedPrice()Ljava/lang/String;
    .locals 1

    iget-object v0, p0, Lcom/google/android/finsky/protos/Purchase$ClientCart;->formattedPrice_:Ljava/lang/String;

    return-object v0
.end method

.method public getInstrument()Lcom/google/android/finsky/protos/CommonDevice$Instrument;
    .locals 1

    iget-object v0, p0, Lcom/google/android/finsky/protos/Purchase$ClientCart;->instrument_:Lcom/google/android/finsky/protos/CommonDevice$Instrument;

    return-object v0
.end method

.method public getPriceByline()Ljava/lang/String;
    .locals 1

    iget-object v0, p0, Lcom/google/android/finsky/protos/Purchase$ClientCart;->priceByline_:Ljava/lang/String;

    return-object v0
.end method

.method public getPurchaseContextToken()Ljava/lang/String;
    .locals 1

    iget-object v0, p0, Lcom/google/android/finsky/protos/Purchase$ClientCart;->purchaseContextToken_:Ljava/lang/String;

    return-object v0
.end method

.method public getSerializedSize()I
    .locals 6

    const/4 v3, 0x0

    invoke-virtual {p0}, Lcom/google/android/finsky/protos/Purchase$ClientCart;->hasTitle()Z

    move-result v4

    if-eqz v4, :cond_0

    const/4 v4, 0x1

    invoke-virtual {p0}, Lcom/google/android/finsky/protos/Purchase$ClientCart;->getTitle()Ljava/lang/String;

    move-result-object v5

    invoke-static {v4, v5}, Lcom/google/protobuf/micro/CodedOutputStreamMicro;->computeStringSize(ILjava/lang/String;)I

    move-result v4

    add-int/2addr v3, v4

    :cond_0
    invoke-virtual {p0}, Lcom/google/android/finsky/protos/Purchase$ClientCart;->hasFormattedPrice()Z

    move-result v4

    if-eqz v4, :cond_1

    const/4 v4, 0x2

    invoke-virtual {p0}, Lcom/google/android/finsky/protos/Purchase$ClientCart;->getFormattedPrice()Ljava/lang/String;

    move-result-object v5

    invoke-static {v4, v5}, Lcom/google/protobuf/micro/CodedOutputStreamMicro;->computeStringSize(ILjava/lang/String;)I

    move-result v4

    add-int/2addr v3, v4

    :cond_1
    invoke-virtual {p0}, Lcom/google/android/finsky/protos/Purchase$ClientCart;->hasPurchaseContextToken()Z

    move-result v4

    if-eqz v4, :cond_2

    const/4 v4, 0x3

    invoke-virtual {p0}, Lcom/google/android/finsky/protos/Purchase$ClientCart;->getPurchaseContextToken()Ljava/lang/String;

    move-result-object v5

    invoke-static {v4, v5}, Lcom/google/protobuf/micro/CodedOutputStreamMicro;->computeStringSize(ILjava/lang/String;)I

    move-result v4

    add-int/2addr v3, v4

    :cond_2
    invoke-virtual {p0}, Lcom/google/android/finsky/protos/Purchase$ClientCart;->hasInstrument()Z

    move-result v4

    if-eqz v4, :cond_3

    const/4 v4, 0x4

    invoke-virtual {p0}, Lcom/google/android/finsky/protos/Purchase$ClientCart;->getInstrument()Lcom/google/android/finsky/protos/CommonDevice$Instrument;

    move-result-object v5

    invoke-static {v4, v5}, Lcom/google/protobuf/micro/CodedOutputStreamMicro;->computeMessageSize(ILcom/google/protobuf/micro/MessageMicro;)I

    move-result v4

    add-int/2addr v3, v4

    :cond_3
    const/4 v0, 0x0

    invoke-virtual {p0}, Lcom/google/android/finsky/protos/Purchase$ClientCart;->getExtendedDetailHtmlList()Ljava/util/List;

    move-result-object v4

    invoke-interface {v4}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v2

    :goto_0
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    move-result v4

    if-eqz v4, :cond_4

    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Ljava/lang/String;

    invoke-static {v1}, Lcom/google/protobuf/micro/CodedOutputStreamMicro;->computeStringSizeNoTag(Ljava/lang/String;)I

    move-result v4

    add-int/2addr v0, v4

    goto :goto_0

    :cond_4
    add-int/2addr v3, v0

    invoke-virtual {p0}, Lcom/google/android/finsky/protos/Purchase$ClientCart;->getExtendedDetailHtmlList()Ljava/util/List;

    move-result-object v4

    invoke-interface {v4}, Ljava/util/List;->size()I

    move-result v4

    mul-int/lit8 v4, v4, 0x1

    add-int/2addr v3, v4

    invoke-virtual {p0}, Lcom/google/android/finsky/protos/Purchase$ClientCart;->hasFooterHtml()Z

    move-result v4

    if-eqz v4, :cond_5

    const/4 v4, 0x6

    invoke-virtual {p0}, Lcom/google/android/finsky/protos/Purchase$ClientCart;->getFooterHtml()Ljava/lang/String;

    move-result-object v5

    invoke-static {v4, v5}, Lcom/google/protobuf/micro/CodedOutputStreamMicro;->computeStringSize(ILjava/lang/String;)I

    move-result v4

    add-int/2addr v3, v4

    :cond_5
    invoke-virtual {p0}, Lcom/google/android/finsky/protos/Purchase$ClientCart;->hasAddInstrumentPromptHtml()Z

    move-result v4

    if-eqz v4, :cond_6

    const/4 v4, 0x7

    invoke-virtual {p0}, Lcom/google/android/finsky/protos/Purchase$ClientCart;->getAddInstrumentPromptHtml()Ljava/lang/String;

    move-result-object v5

    invoke-static {v4, v5}, Lcom/google/protobuf/micro/CodedOutputStreamMicro;->computeStringSize(ILjava/lang/String;)I

    move-result v4

    add-int/2addr v3, v4

    :cond_6
    invoke-virtual {p0}, Lcom/google/android/finsky/protos/Purchase$ClientCart;->hasButtonText()Z

    move-result v4

    if-eqz v4, :cond_7

    const/16 v4, 0x8

    invoke-virtual {p0}, Lcom/google/android/finsky/protos/Purchase$ClientCart;->getButtonText()Ljava/lang/String;

    move-result-object v5

    invoke-static {v4, v5}, Lcom/google/protobuf/micro/CodedOutputStreamMicro;->computeStringSize(ILjava/lang/String;)I

    move-result v4

    add-int/2addr v3, v4

    :cond_7
    invoke-virtual {p0}, Lcom/google/android/finsky/protos/Purchase$ClientCart;->hasCompletePurchaseChallenge()Z

    move-result v4

    if-eqz v4, :cond_8

    const/16 v4, 0x9

    invoke-virtual {p0}, Lcom/google/android/finsky/protos/Purchase$ClientCart;->getCompletePurchaseChallenge()Lcom/google/android/finsky/protos/ChallengeProtos$Challenge;

    move-result-object v5

    invoke-static {v4, v5}, Lcom/google/protobuf/micro/CodedOutputStreamMicro;->computeMessageSize(ILcom/google/protobuf/micro/MessageMicro;)I

    move-result v4

    add-int/2addr v3, v4

    :cond_8
    invoke-virtual {p0}, Lcom/google/android/finsky/protos/Purchase$ClientCart;->hasPriceByline()Z

    move-result v4

    if-eqz v4, :cond_9

    const/16 v4, 0xa

    invoke-virtual {p0}, Lcom/google/android/finsky/protos/Purchase$ClientCart;->getPriceByline()Ljava/lang/String;

    move-result-object v5

    invoke-static {v4, v5}, Lcom/google/protobuf/micro/CodedOutputStreamMicro;->computeStringSize(ILjava/lang/String;)I

    move-result v4

    add-int/2addr v3, v4

    :cond_9
    const/4 v0, 0x0

    invoke-virtual {p0}, Lcom/google/android/finsky/protos/Purchase$ClientCart;->getDetailHtmlList()Ljava/util/List;

    move-result-object v4

    invoke-interface {v4}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v2

    :goto_1
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    move-result v4

    if-eqz v4, :cond_a

    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Ljava/lang/String;

    invoke-static {v1}, Lcom/google/protobuf/micro/CodedOutputStreamMicro;->computeStringSizeNoTag(Ljava/lang/String;)I

    move-result v4

    add-int/2addr v0, v4

    goto :goto_1

    :cond_a
    add-int/2addr v3, v0

    invoke-virtual {p0}, Lcom/google/android/finsky/protos/Purchase$ClientCart;->getDetailHtmlList()Ljava/util/List;

    move-result-object v4

    invoke-interface {v4}, Ljava/util/List;->size()I

    move-result v4

    mul-int/lit8 v4, v4, 0x1

    add-int/2addr v3, v4

    iput v3, p0, Lcom/google/android/finsky/protos/Purchase$ClientCart;->cachedSize:I

    return v3
.end method

.method public getTitle()Ljava/lang/String;
    .locals 1

    iget-object v0, p0, Lcom/google/android/finsky/protos/Purchase$ClientCart;->title_:Ljava/lang/String;

    return-object v0
.end method

.method public hasAddInstrumentPromptHtml()Z
    .locals 1

    iget-boolean v0, p0, Lcom/google/android/finsky/protos/Purchase$ClientCart;->hasAddInstrumentPromptHtml:Z

    return v0
.end method

.method public hasButtonText()Z
    .locals 1

    iget-boolean v0, p0, Lcom/google/android/finsky/protos/Purchase$ClientCart;->hasButtonText:Z

    return v0
.end method

.method public hasCompletePurchaseChallenge()Z
    .locals 1

    iget-boolean v0, p0, Lcom/google/android/finsky/protos/Purchase$ClientCart;->hasCompletePurchaseChallenge:Z

    return v0
.end method

.method public hasFooterHtml()Z
    .locals 1

    iget-boolean v0, p0, Lcom/google/android/finsky/protos/Purchase$ClientCart;->hasFooterHtml:Z

    return v0
.end method

.method public hasFormattedPrice()Z
    .locals 1

    iget-boolean v0, p0, Lcom/google/android/finsky/protos/Purchase$ClientCart;->hasFormattedPrice:Z

    return v0
.end method

.method public hasInstrument()Z
    .locals 1

    iget-boolean v0, p0, Lcom/google/android/finsky/protos/Purchase$ClientCart;->hasInstrument:Z

    return v0
.end method

.method public hasPriceByline()Z
    .locals 1

    iget-boolean v0, p0, Lcom/google/android/finsky/protos/Purchase$ClientCart;->hasPriceByline:Z

    return v0
.end method

.method public hasPurchaseContextToken()Z
    .locals 1

    iget-boolean v0, p0, Lcom/google/android/finsky/protos/Purchase$ClientCart;->hasPurchaseContextToken:Z

    return v0
.end method

.method public hasTitle()Z
    .locals 1

    iget-boolean v0, p0, Lcom/google/android/finsky/protos/Purchase$ClientCart;->hasTitle:Z

    return v0
.end method

.method public mergeFrom(Lcom/google/protobuf/micro/CodedInputStreamMicro;)Lcom/google/android/finsky/protos/Purchase$ClientCart;
    .locals 3
    .param p1    # Lcom/google/protobuf/micro/CodedInputStreamMicro;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    :cond_0
    :goto_0
    invoke-virtual {p1}, Lcom/google/protobuf/micro/CodedInputStreamMicro;->readTag()I

    move-result v0

    sparse-switch v0, :sswitch_data_0

    invoke-virtual {p0, p1, v0}, Lcom/google/android/finsky/protos/Purchase$ClientCart;->parseUnknownField(Lcom/google/protobuf/micro/CodedInputStreamMicro;I)Z

    move-result v2

    if-nez v2, :cond_0

    :sswitch_0
    return-object p0

    :sswitch_1
    invoke-virtual {p1}, Lcom/google/protobuf/micro/CodedInputStreamMicro;->readString()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {p0, v2}, Lcom/google/android/finsky/protos/Purchase$ClientCart;->setTitle(Ljava/lang/String;)Lcom/google/android/finsky/protos/Purchase$ClientCart;

    goto :goto_0

    :sswitch_2
    invoke-virtual {p1}, Lcom/google/protobuf/micro/CodedInputStreamMicro;->readString()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {p0, v2}, Lcom/google/android/finsky/protos/Purchase$ClientCart;->setFormattedPrice(Ljava/lang/String;)Lcom/google/android/finsky/protos/Purchase$ClientCart;

    goto :goto_0

    :sswitch_3
    invoke-virtual {p1}, Lcom/google/protobuf/micro/CodedInputStreamMicro;->readString()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {p0, v2}, Lcom/google/android/finsky/protos/Purchase$ClientCart;->setPurchaseContextToken(Ljava/lang/String;)Lcom/google/android/finsky/protos/Purchase$ClientCart;

    goto :goto_0

    :sswitch_4
    new-instance v1, Lcom/google/android/finsky/protos/CommonDevice$Instrument;

    invoke-direct {v1}, Lcom/google/android/finsky/protos/CommonDevice$Instrument;-><init>()V

    invoke-virtual {p1, v1}, Lcom/google/protobuf/micro/CodedInputStreamMicro;->readMessage(Lcom/google/protobuf/micro/MessageMicro;)V

    invoke-virtual {p0, v1}, Lcom/google/android/finsky/protos/Purchase$ClientCart;->setInstrument(Lcom/google/android/finsky/protos/CommonDevice$Instrument;)Lcom/google/android/finsky/protos/Purchase$ClientCart;

    goto :goto_0

    :sswitch_5
    invoke-virtual {p1}, Lcom/google/protobuf/micro/CodedInputStreamMicro;->readString()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {p0, v2}, Lcom/google/android/finsky/protos/Purchase$ClientCart;->addExtendedDetailHtml(Ljava/lang/String;)Lcom/google/android/finsky/protos/Purchase$ClientCart;

    goto :goto_0

    :sswitch_6
    invoke-virtual {p1}, Lcom/google/protobuf/micro/CodedInputStreamMicro;->readString()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {p0, v2}, Lcom/google/android/finsky/protos/Purchase$ClientCart;->setFooterHtml(Ljava/lang/String;)Lcom/google/android/finsky/protos/Purchase$ClientCart;

    goto :goto_0

    :sswitch_7
    invoke-virtual {p1}, Lcom/google/protobuf/micro/CodedInputStreamMicro;->readString()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {p0, v2}, Lcom/google/android/finsky/protos/Purchase$ClientCart;->setAddInstrumentPromptHtml(Ljava/lang/String;)Lcom/google/android/finsky/protos/Purchase$ClientCart;

    goto :goto_0

    :sswitch_8
    invoke-virtual {p1}, Lcom/google/protobuf/micro/CodedInputStreamMicro;->readString()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {p0, v2}, Lcom/google/android/finsky/protos/Purchase$ClientCart;->setButtonText(Ljava/lang/String;)Lcom/google/android/finsky/protos/Purchase$ClientCart;

    goto :goto_0

    :sswitch_9
    new-instance v1, Lcom/google/android/finsky/protos/ChallengeProtos$Challenge;

    invoke-direct {v1}, Lcom/google/android/finsky/protos/ChallengeProtos$Challenge;-><init>()V

    invoke-virtual {p1, v1}, Lcom/google/protobuf/micro/CodedInputStreamMicro;->readMessage(Lcom/google/protobuf/micro/MessageMicro;)V

    invoke-virtual {p0, v1}, Lcom/google/android/finsky/protos/Purchase$ClientCart;->setCompletePurchaseChallenge(Lcom/google/android/finsky/protos/ChallengeProtos$Challenge;)Lcom/google/android/finsky/protos/Purchase$ClientCart;

    goto :goto_0

    :sswitch_a
    invoke-virtual {p1}, Lcom/google/protobuf/micro/CodedInputStreamMicro;->readString()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {p0, v2}, Lcom/google/android/finsky/protos/Purchase$ClientCart;->setPriceByline(Ljava/lang/String;)Lcom/google/android/finsky/protos/Purchase$ClientCart;

    goto :goto_0

    :sswitch_b
    invoke-virtual {p1}, Lcom/google/protobuf/micro/CodedInputStreamMicro;->readString()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {p0, v2}, Lcom/google/android/finsky/protos/Purchase$ClientCart;->addDetailHtml(Ljava/lang/String;)Lcom/google/android/finsky/protos/Purchase$ClientCart;

    goto :goto_0

    :sswitch_data_0
    .sparse-switch
        0x0 -> :sswitch_0
        0xa -> :sswitch_1
        0x12 -> :sswitch_2
        0x1a -> :sswitch_3
        0x22 -> :sswitch_4
        0x2a -> :sswitch_5
        0x32 -> :sswitch_6
        0x3a -> :sswitch_7
        0x42 -> :sswitch_8
        0x4a -> :sswitch_9
        0x52 -> :sswitch_a
        0x5a -> :sswitch_b
    .end sparse-switch
.end method

.method public bridge synthetic mergeFrom(Lcom/google/protobuf/micro/CodedInputStreamMicro;)Lcom/google/protobuf/micro/MessageMicro;
    .locals 1
    .param p1    # Lcom/google/protobuf/micro/CodedInputStreamMicro;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    invoke-virtual {p0, p1}, Lcom/google/android/finsky/protos/Purchase$ClientCart;->mergeFrom(Lcom/google/protobuf/micro/CodedInputStreamMicro;)Lcom/google/android/finsky/protos/Purchase$ClientCart;

    move-result-object v0

    return-object v0
.end method

.method public setAddInstrumentPromptHtml(Ljava/lang/String;)Lcom/google/android/finsky/protos/Purchase$ClientCart;
    .locals 1
    .param p1    # Ljava/lang/String;

    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/google/android/finsky/protos/Purchase$ClientCart;->hasAddInstrumentPromptHtml:Z

    iput-object p1, p0, Lcom/google/android/finsky/protos/Purchase$ClientCart;->addInstrumentPromptHtml_:Ljava/lang/String;

    return-object p0
.end method

.method public setButtonText(Ljava/lang/String;)Lcom/google/android/finsky/protos/Purchase$ClientCart;
    .locals 1
    .param p1    # Ljava/lang/String;

    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/google/android/finsky/protos/Purchase$ClientCart;->hasButtonText:Z

    iput-object p1, p0, Lcom/google/android/finsky/protos/Purchase$ClientCart;->buttonText_:Ljava/lang/String;

    return-object p0
.end method

.method public setCompletePurchaseChallenge(Lcom/google/android/finsky/protos/ChallengeProtos$Challenge;)Lcom/google/android/finsky/protos/Purchase$ClientCart;
    .locals 1
    .param p1    # Lcom/google/android/finsky/protos/ChallengeProtos$Challenge;

    if-nez p1, :cond_0

    new-instance v0, Ljava/lang/NullPointerException;

    invoke-direct {v0}, Ljava/lang/NullPointerException;-><init>()V

    throw v0

    :cond_0
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/google/android/finsky/protos/Purchase$ClientCart;->hasCompletePurchaseChallenge:Z

    iput-object p1, p0, Lcom/google/android/finsky/protos/Purchase$ClientCart;->completePurchaseChallenge_:Lcom/google/android/finsky/protos/ChallengeProtos$Challenge;

    return-object p0
.end method

.method public setFooterHtml(Ljava/lang/String;)Lcom/google/android/finsky/protos/Purchase$ClientCart;
    .locals 1
    .param p1    # Ljava/lang/String;

    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/google/android/finsky/protos/Purchase$ClientCart;->hasFooterHtml:Z

    iput-object p1, p0, Lcom/google/android/finsky/protos/Purchase$ClientCart;->footerHtml_:Ljava/lang/String;

    return-object p0
.end method

.method public setFormattedPrice(Ljava/lang/String;)Lcom/google/android/finsky/protos/Purchase$ClientCart;
    .locals 1
    .param p1    # Ljava/lang/String;

    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/google/android/finsky/protos/Purchase$ClientCart;->hasFormattedPrice:Z

    iput-object p1, p0, Lcom/google/android/finsky/protos/Purchase$ClientCart;->formattedPrice_:Ljava/lang/String;

    return-object p0
.end method

.method public setInstrument(Lcom/google/android/finsky/protos/CommonDevice$Instrument;)Lcom/google/android/finsky/protos/Purchase$ClientCart;
    .locals 1
    .param p1    # Lcom/google/android/finsky/protos/CommonDevice$Instrument;

    if-nez p1, :cond_0

    new-instance v0, Ljava/lang/NullPointerException;

    invoke-direct {v0}, Ljava/lang/NullPointerException;-><init>()V

    throw v0

    :cond_0
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/google/android/finsky/protos/Purchase$ClientCart;->hasInstrument:Z

    iput-object p1, p0, Lcom/google/android/finsky/protos/Purchase$ClientCart;->instrument_:Lcom/google/android/finsky/protos/CommonDevice$Instrument;

    return-object p0
.end method

.method public setPriceByline(Ljava/lang/String;)Lcom/google/android/finsky/protos/Purchase$ClientCart;
    .locals 1
    .param p1    # Ljava/lang/String;

    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/google/android/finsky/protos/Purchase$ClientCart;->hasPriceByline:Z

    iput-object p1, p0, Lcom/google/android/finsky/protos/Purchase$ClientCart;->priceByline_:Ljava/lang/String;

    return-object p0
.end method

.method public setPurchaseContextToken(Ljava/lang/String;)Lcom/google/android/finsky/protos/Purchase$ClientCart;
    .locals 1
    .param p1    # Ljava/lang/String;

    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/google/android/finsky/protos/Purchase$ClientCart;->hasPurchaseContextToken:Z

    iput-object p1, p0, Lcom/google/android/finsky/protos/Purchase$ClientCart;->purchaseContextToken_:Ljava/lang/String;

    return-object p0
.end method

.method public setTitle(Ljava/lang/String;)Lcom/google/android/finsky/protos/Purchase$ClientCart;
    .locals 1
    .param p1    # Ljava/lang/String;

    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/google/android/finsky/protos/Purchase$ClientCart;->hasTitle:Z

    iput-object p1, p0, Lcom/google/android/finsky/protos/Purchase$ClientCart;->title_:Ljava/lang/String;

    return-object p0
.end method

.method public writeTo(Lcom/google/protobuf/micro/CodedOutputStreamMicro;)V
    .locals 4
    .param p1    # Lcom/google/protobuf/micro/CodedOutputStreamMicro;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    invoke-virtual {p0}, Lcom/google/android/finsky/protos/Purchase$ClientCart;->hasTitle()Z

    move-result v2

    if-eqz v2, :cond_0

    const/4 v2, 0x1

    invoke-virtual {p0}, Lcom/google/android/finsky/protos/Purchase$ClientCart;->getTitle()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {p1, v2, v3}, Lcom/google/protobuf/micro/CodedOutputStreamMicro;->writeString(ILjava/lang/String;)V

    :cond_0
    invoke-virtual {p0}, Lcom/google/android/finsky/protos/Purchase$ClientCart;->hasFormattedPrice()Z

    move-result v2

    if-eqz v2, :cond_1

    const/4 v2, 0x2

    invoke-virtual {p0}, Lcom/google/android/finsky/protos/Purchase$ClientCart;->getFormattedPrice()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {p1, v2, v3}, Lcom/google/protobuf/micro/CodedOutputStreamMicro;->writeString(ILjava/lang/String;)V

    :cond_1
    invoke-virtual {p0}, Lcom/google/android/finsky/protos/Purchase$ClientCart;->hasPurchaseContextToken()Z

    move-result v2

    if-eqz v2, :cond_2

    const/4 v2, 0x3

    invoke-virtual {p0}, Lcom/google/android/finsky/protos/Purchase$ClientCart;->getPurchaseContextToken()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {p1, v2, v3}, Lcom/google/protobuf/micro/CodedOutputStreamMicro;->writeString(ILjava/lang/String;)V

    :cond_2
    invoke-virtual {p0}, Lcom/google/android/finsky/protos/Purchase$ClientCart;->hasInstrument()Z

    move-result v2

    if-eqz v2, :cond_3

    const/4 v2, 0x4

    invoke-virtual {p0}, Lcom/google/android/finsky/protos/Purchase$ClientCart;->getInstrument()Lcom/google/android/finsky/protos/CommonDevice$Instrument;

    move-result-object v3

    invoke-virtual {p1, v2, v3}, Lcom/google/protobuf/micro/CodedOutputStreamMicro;->writeMessage(ILcom/google/protobuf/micro/MessageMicro;)V

    :cond_3
    invoke-virtual {p0}, Lcom/google/android/finsky/protos/Purchase$ClientCart;->getExtendedDetailHtmlList()Ljava/util/List;

    move-result-object v2

    invoke-interface {v2}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :goto_0
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v2

    if-eqz v2, :cond_4

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/String;

    const/4 v2, 0x5

    invoke-virtual {p1, v2, v0}, Lcom/google/protobuf/micro/CodedOutputStreamMicro;->writeString(ILjava/lang/String;)V

    goto :goto_0

    :cond_4
    invoke-virtual {p0}, Lcom/google/android/finsky/protos/Purchase$ClientCart;->hasFooterHtml()Z

    move-result v2

    if-eqz v2, :cond_5

    const/4 v2, 0x6

    invoke-virtual {p0}, Lcom/google/android/finsky/protos/Purchase$ClientCart;->getFooterHtml()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {p1, v2, v3}, Lcom/google/protobuf/micro/CodedOutputStreamMicro;->writeString(ILjava/lang/String;)V

    :cond_5
    invoke-virtual {p0}, Lcom/google/android/finsky/protos/Purchase$ClientCart;->hasAddInstrumentPromptHtml()Z

    move-result v2

    if-eqz v2, :cond_6

    const/4 v2, 0x7

    invoke-virtual {p0}, Lcom/google/android/finsky/protos/Purchase$ClientCart;->getAddInstrumentPromptHtml()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {p1, v2, v3}, Lcom/google/protobuf/micro/CodedOutputStreamMicro;->writeString(ILjava/lang/String;)V

    :cond_6
    invoke-virtual {p0}, Lcom/google/android/finsky/protos/Purchase$ClientCart;->hasButtonText()Z

    move-result v2

    if-eqz v2, :cond_7

    const/16 v2, 0x8

    invoke-virtual {p0}, Lcom/google/android/finsky/protos/Purchase$ClientCart;->getButtonText()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {p1, v2, v3}, Lcom/google/protobuf/micro/CodedOutputStreamMicro;->writeString(ILjava/lang/String;)V

    :cond_7
    invoke-virtual {p0}, Lcom/google/android/finsky/protos/Purchase$ClientCart;->hasCompletePurchaseChallenge()Z

    move-result v2

    if-eqz v2, :cond_8

    const/16 v2, 0x9

    invoke-virtual {p0}, Lcom/google/android/finsky/protos/Purchase$ClientCart;->getCompletePurchaseChallenge()Lcom/google/android/finsky/protos/ChallengeProtos$Challenge;

    move-result-object v3

    invoke-virtual {p1, v2, v3}, Lcom/google/protobuf/micro/CodedOutputStreamMicro;->writeMessage(ILcom/google/protobuf/micro/MessageMicro;)V

    :cond_8
    invoke-virtual {p0}, Lcom/google/android/finsky/protos/Purchase$ClientCart;->hasPriceByline()Z

    move-result v2

    if-eqz v2, :cond_9

    const/16 v2, 0xa

    invoke-virtual {p0}, Lcom/google/android/finsky/protos/Purchase$ClientCart;->getPriceByline()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {p1, v2, v3}, Lcom/google/protobuf/micro/CodedOutputStreamMicro;->writeString(ILjava/lang/String;)V

    :cond_9
    invoke-virtual {p0}, Lcom/google/android/finsky/protos/Purchase$ClientCart;->getDetailHtmlList()Ljava/util/List;

    move-result-object v2

    invoke-interface {v2}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :goto_1
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v2

    if-eqz v2, :cond_a

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/String;

    const/16 v2, 0xb

    invoke-virtual {p1, v2, v0}, Lcom/google/protobuf/micro/CodedOutputStreamMicro;->writeString(ILjava/lang/String;)V

    goto :goto_1

    :cond_a
    return-void
.end method
