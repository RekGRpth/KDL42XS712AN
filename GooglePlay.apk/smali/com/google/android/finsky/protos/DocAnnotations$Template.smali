.class public final Lcom/google/android/finsky/protos/DocAnnotations$Template;
.super Lcom/google/protobuf/micro/MessageMicro;
.source "DocAnnotations.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/google/android/finsky/protos/DocAnnotations;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x19
    name = "Template"
.end annotation


# instance fields
.field private cachedSize:I

.field private containerWithBanner_:Lcom/google/android/finsky/protos/DocAnnotations$ContainerWithBanner;

.field private dealOfTheDay_:Lcom/google/android/finsky/protos/DocAnnotations$DealOfTheDay;

.field private editorialSeriesContainer_:Lcom/google/android/finsky/protos/DocAnnotations$EditorialSeriesContainer;

.field private hasContainerWithBanner:Z

.field private hasDealOfTheDay:Z

.field private hasEditorialSeriesContainer:Z

.field private hasNextBanner:Z

.field private hasRecommendationsContainer:Z

.field private hasSeriesAntenna:Z

.field private hasTileDetailsReflectedGraphic2X2:Z

.field private hasTileFourBlock4X2:Z

.field private hasTileGraphic2X1:Z

.field private hasTileGraphic4X2:Z

.field private hasTileGraphicColoredTitle2X1:Z

.field private hasTileGraphicColoredTitle4X2:Z

.field private hasTileGraphicUpperLeftTitle2X1:Z

.field private nextBanner_:Lcom/google/android/finsky/protos/DocAnnotations$NextBanner;

.field private recommendationsContainer_:Lcom/google/android/finsky/protos/DocAnnotations$RecommendationsContainer;

.field private seriesAntenna_:Lcom/google/android/finsky/protos/DocAnnotations$SeriesAntenna;

.field private tileDetailsReflectedGraphic2X2_:Lcom/google/android/finsky/protos/DocAnnotations$TileTemplate;

.field private tileFourBlock4X2_:Lcom/google/android/finsky/protos/DocAnnotations$TileTemplate;

.field private tileGraphic2X1_:Lcom/google/android/finsky/protos/DocAnnotations$TileTemplate;

.field private tileGraphic4X2_:Lcom/google/android/finsky/protos/DocAnnotations$TileTemplate;

.field private tileGraphicColoredTitle2X1_:Lcom/google/android/finsky/protos/DocAnnotations$TileTemplate;

.field private tileGraphicColoredTitle4X2_:Lcom/google/android/finsky/protos/DocAnnotations$TileTemplate;

.field private tileGraphicUpperLeftTitle2X1_:Lcom/google/android/finsky/protos/DocAnnotations$TileTemplate;


# direct methods
.method public constructor <init>()V
    .locals 1

    const/4 v0, 0x0

    invoke-direct {p0}, Lcom/google/protobuf/micro/MessageMicro;-><init>()V

    iput-object v0, p0, Lcom/google/android/finsky/protos/DocAnnotations$Template;->seriesAntenna_:Lcom/google/android/finsky/protos/DocAnnotations$SeriesAntenna;

    iput-object v0, p0, Lcom/google/android/finsky/protos/DocAnnotations$Template;->tileGraphic2X1_:Lcom/google/android/finsky/protos/DocAnnotations$TileTemplate;

    iput-object v0, p0, Lcom/google/android/finsky/protos/DocAnnotations$Template;->tileGraphic4X2_:Lcom/google/android/finsky/protos/DocAnnotations$TileTemplate;

    iput-object v0, p0, Lcom/google/android/finsky/protos/DocAnnotations$Template;->tileGraphicColoredTitle2X1_:Lcom/google/android/finsky/protos/DocAnnotations$TileTemplate;

    iput-object v0, p0, Lcom/google/android/finsky/protos/DocAnnotations$Template;->tileGraphicUpperLeftTitle2X1_:Lcom/google/android/finsky/protos/DocAnnotations$TileTemplate;

    iput-object v0, p0, Lcom/google/android/finsky/protos/DocAnnotations$Template;->tileDetailsReflectedGraphic2X2_:Lcom/google/android/finsky/protos/DocAnnotations$TileTemplate;

    iput-object v0, p0, Lcom/google/android/finsky/protos/DocAnnotations$Template;->tileFourBlock4X2_:Lcom/google/android/finsky/protos/DocAnnotations$TileTemplate;

    iput-object v0, p0, Lcom/google/android/finsky/protos/DocAnnotations$Template;->tileGraphicColoredTitle4X2_:Lcom/google/android/finsky/protos/DocAnnotations$TileTemplate;

    iput-object v0, p0, Lcom/google/android/finsky/protos/DocAnnotations$Template;->containerWithBanner_:Lcom/google/android/finsky/protos/DocAnnotations$ContainerWithBanner;

    iput-object v0, p0, Lcom/google/android/finsky/protos/DocAnnotations$Template;->dealOfTheDay_:Lcom/google/android/finsky/protos/DocAnnotations$DealOfTheDay;

    iput-object v0, p0, Lcom/google/android/finsky/protos/DocAnnotations$Template;->editorialSeriesContainer_:Lcom/google/android/finsky/protos/DocAnnotations$EditorialSeriesContainer;

    iput-object v0, p0, Lcom/google/android/finsky/protos/DocAnnotations$Template;->recommendationsContainer_:Lcom/google/android/finsky/protos/DocAnnotations$RecommendationsContainer;

    iput-object v0, p0, Lcom/google/android/finsky/protos/DocAnnotations$Template;->nextBanner_:Lcom/google/android/finsky/protos/DocAnnotations$NextBanner;

    const/4 v0, -0x1

    iput v0, p0, Lcom/google/android/finsky/protos/DocAnnotations$Template;->cachedSize:I

    return-void
.end method


# virtual methods
.method public getCachedSize()I
    .locals 1

    iget v0, p0, Lcom/google/android/finsky/protos/DocAnnotations$Template;->cachedSize:I

    if-gez v0, :cond_0

    invoke-virtual {p0}, Lcom/google/android/finsky/protos/DocAnnotations$Template;->getSerializedSize()I

    :cond_0
    iget v0, p0, Lcom/google/android/finsky/protos/DocAnnotations$Template;->cachedSize:I

    return v0
.end method

.method public getContainerWithBanner()Lcom/google/android/finsky/protos/DocAnnotations$ContainerWithBanner;
    .locals 1

    iget-object v0, p0, Lcom/google/android/finsky/protos/DocAnnotations$Template;->containerWithBanner_:Lcom/google/android/finsky/protos/DocAnnotations$ContainerWithBanner;

    return-object v0
.end method

.method public getDealOfTheDay()Lcom/google/android/finsky/protos/DocAnnotations$DealOfTheDay;
    .locals 1

    iget-object v0, p0, Lcom/google/android/finsky/protos/DocAnnotations$Template;->dealOfTheDay_:Lcom/google/android/finsky/protos/DocAnnotations$DealOfTheDay;

    return-object v0
.end method

.method public getEditorialSeriesContainer()Lcom/google/android/finsky/protos/DocAnnotations$EditorialSeriesContainer;
    .locals 1

    iget-object v0, p0, Lcom/google/android/finsky/protos/DocAnnotations$Template;->editorialSeriesContainer_:Lcom/google/android/finsky/protos/DocAnnotations$EditorialSeriesContainer;

    return-object v0
.end method

.method public getNextBanner()Lcom/google/android/finsky/protos/DocAnnotations$NextBanner;
    .locals 1

    iget-object v0, p0, Lcom/google/android/finsky/protos/DocAnnotations$Template;->nextBanner_:Lcom/google/android/finsky/protos/DocAnnotations$NextBanner;

    return-object v0
.end method

.method public getRecommendationsContainer()Lcom/google/android/finsky/protos/DocAnnotations$RecommendationsContainer;
    .locals 1

    iget-object v0, p0, Lcom/google/android/finsky/protos/DocAnnotations$Template;->recommendationsContainer_:Lcom/google/android/finsky/protos/DocAnnotations$RecommendationsContainer;

    return-object v0
.end method

.method public getSerializedSize()I
    .locals 3

    const/4 v0, 0x0

    invoke-virtual {p0}, Lcom/google/android/finsky/protos/DocAnnotations$Template;->hasSeriesAntenna()Z

    move-result v1

    if-eqz v1, :cond_0

    const/4 v1, 0x1

    invoke-virtual {p0}, Lcom/google/android/finsky/protos/DocAnnotations$Template;->getSeriesAntenna()Lcom/google/android/finsky/protos/DocAnnotations$SeriesAntenna;

    move-result-object v2

    invoke-static {v1, v2}, Lcom/google/protobuf/micro/CodedOutputStreamMicro;->computeMessageSize(ILcom/google/protobuf/micro/MessageMicro;)I

    move-result v1

    add-int/2addr v0, v1

    :cond_0
    invoke-virtual {p0}, Lcom/google/android/finsky/protos/DocAnnotations$Template;->hasTileGraphic2X1()Z

    move-result v1

    if-eqz v1, :cond_1

    const/4 v1, 0x2

    invoke-virtual {p0}, Lcom/google/android/finsky/protos/DocAnnotations$Template;->getTileGraphic2X1()Lcom/google/android/finsky/protos/DocAnnotations$TileTemplate;

    move-result-object v2

    invoke-static {v1, v2}, Lcom/google/protobuf/micro/CodedOutputStreamMicro;->computeMessageSize(ILcom/google/protobuf/micro/MessageMicro;)I

    move-result v1

    add-int/2addr v0, v1

    :cond_1
    invoke-virtual {p0}, Lcom/google/android/finsky/protos/DocAnnotations$Template;->hasTileGraphic4X2()Z

    move-result v1

    if-eqz v1, :cond_2

    const/4 v1, 0x3

    invoke-virtual {p0}, Lcom/google/android/finsky/protos/DocAnnotations$Template;->getTileGraphic4X2()Lcom/google/android/finsky/protos/DocAnnotations$TileTemplate;

    move-result-object v2

    invoke-static {v1, v2}, Lcom/google/protobuf/micro/CodedOutputStreamMicro;->computeMessageSize(ILcom/google/protobuf/micro/MessageMicro;)I

    move-result v1

    add-int/2addr v0, v1

    :cond_2
    invoke-virtual {p0}, Lcom/google/android/finsky/protos/DocAnnotations$Template;->hasTileGraphicColoredTitle2X1()Z

    move-result v1

    if-eqz v1, :cond_3

    const/4 v1, 0x4

    invoke-virtual {p0}, Lcom/google/android/finsky/protos/DocAnnotations$Template;->getTileGraphicColoredTitle2X1()Lcom/google/android/finsky/protos/DocAnnotations$TileTemplate;

    move-result-object v2

    invoke-static {v1, v2}, Lcom/google/protobuf/micro/CodedOutputStreamMicro;->computeMessageSize(ILcom/google/protobuf/micro/MessageMicro;)I

    move-result v1

    add-int/2addr v0, v1

    :cond_3
    invoke-virtual {p0}, Lcom/google/android/finsky/protos/DocAnnotations$Template;->hasTileGraphicUpperLeftTitle2X1()Z

    move-result v1

    if-eqz v1, :cond_4

    const/4 v1, 0x5

    invoke-virtual {p0}, Lcom/google/android/finsky/protos/DocAnnotations$Template;->getTileGraphicUpperLeftTitle2X1()Lcom/google/android/finsky/protos/DocAnnotations$TileTemplate;

    move-result-object v2

    invoke-static {v1, v2}, Lcom/google/protobuf/micro/CodedOutputStreamMicro;->computeMessageSize(ILcom/google/protobuf/micro/MessageMicro;)I

    move-result v1

    add-int/2addr v0, v1

    :cond_4
    invoke-virtual {p0}, Lcom/google/android/finsky/protos/DocAnnotations$Template;->hasTileDetailsReflectedGraphic2X2()Z

    move-result v1

    if-eqz v1, :cond_5

    const/4 v1, 0x6

    invoke-virtual {p0}, Lcom/google/android/finsky/protos/DocAnnotations$Template;->getTileDetailsReflectedGraphic2X2()Lcom/google/android/finsky/protos/DocAnnotations$TileTemplate;

    move-result-object v2

    invoke-static {v1, v2}, Lcom/google/protobuf/micro/CodedOutputStreamMicro;->computeMessageSize(ILcom/google/protobuf/micro/MessageMicro;)I

    move-result v1

    add-int/2addr v0, v1

    :cond_5
    invoke-virtual {p0}, Lcom/google/android/finsky/protos/DocAnnotations$Template;->hasTileFourBlock4X2()Z

    move-result v1

    if-eqz v1, :cond_6

    const/4 v1, 0x7

    invoke-virtual {p0}, Lcom/google/android/finsky/protos/DocAnnotations$Template;->getTileFourBlock4X2()Lcom/google/android/finsky/protos/DocAnnotations$TileTemplate;

    move-result-object v2

    invoke-static {v1, v2}, Lcom/google/protobuf/micro/CodedOutputStreamMicro;->computeMessageSize(ILcom/google/protobuf/micro/MessageMicro;)I

    move-result v1

    add-int/2addr v0, v1

    :cond_6
    invoke-virtual {p0}, Lcom/google/android/finsky/protos/DocAnnotations$Template;->hasContainerWithBanner()Z

    move-result v1

    if-eqz v1, :cond_7

    const/16 v1, 0x8

    invoke-virtual {p0}, Lcom/google/android/finsky/protos/DocAnnotations$Template;->getContainerWithBanner()Lcom/google/android/finsky/protos/DocAnnotations$ContainerWithBanner;

    move-result-object v2

    invoke-static {v1, v2}, Lcom/google/protobuf/micro/CodedOutputStreamMicro;->computeMessageSize(ILcom/google/protobuf/micro/MessageMicro;)I

    move-result v1

    add-int/2addr v0, v1

    :cond_7
    invoke-virtual {p0}, Lcom/google/android/finsky/protos/DocAnnotations$Template;->hasDealOfTheDay()Z

    move-result v1

    if-eqz v1, :cond_8

    const/16 v1, 0x9

    invoke-virtual {p0}, Lcom/google/android/finsky/protos/DocAnnotations$Template;->getDealOfTheDay()Lcom/google/android/finsky/protos/DocAnnotations$DealOfTheDay;

    move-result-object v2

    invoke-static {v1, v2}, Lcom/google/protobuf/micro/CodedOutputStreamMicro;->computeMessageSize(ILcom/google/protobuf/micro/MessageMicro;)I

    move-result v1

    add-int/2addr v0, v1

    :cond_8
    invoke-virtual {p0}, Lcom/google/android/finsky/protos/DocAnnotations$Template;->hasTileGraphicColoredTitle4X2()Z

    move-result v1

    if-eqz v1, :cond_9

    const/16 v1, 0xa

    invoke-virtual {p0}, Lcom/google/android/finsky/protos/DocAnnotations$Template;->getTileGraphicColoredTitle4X2()Lcom/google/android/finsky/protos/DocAnnotations$TileTemplate;

    move-result-object v2

    invoke-static {v1, v2}, Lcom/google/protobuf/micro/CodedOutputStreamMicro;->computeMessageSize(ILcom/google/protobuf/micro/MessageMicro;)I

    move-result v1

    add-int/2addr v0, v1

    :cond_9
    invoke-virtual {p0}, Lcom/google/android/finsky/protos/DocAnnotations$Template;->hasEditorialSeriesContainer()Z

    move-result v1

    if-eqz v1, :cond_a

    const/16 v1, 0xb

    invoke-virtual {p0}, Lcom/google/android/finsky/protos/DocAnnotations$Template;->getEditorialSeriesContainer()Lcom/google/android/finsky/protos/DocAnnotations$EditorialSeriesContainer;

    move-result-object v2

    invoke-static {v1, v2}, Lcom/google/protobuf/micro/CodedOutputStreamMicro;->computeMessageSize(ILcom/google/protobuf/micro/MessageMicro;)I

    move-result v1

    add-int/2addr v0, v1

    :cond_a
    invoke-virtual {p0}, Lcom/google/android/finsky/protos/DocAnnotations$Template;->hasRecommendationsContainer()Z

    move-result v1

    if-eqz v1, :cond_b

    const/16 v1, 0xc

    invoke-virtual {p0}, Lcom/google/android/finsky/protos/DocAnnotations$Template;->getRecommendationsContainer()Lcom/google/android/finsky/protos/DocAnnotations$RecommendationsContainer;

    move-result-object v2

    invoke-static {v1, v2}, Lcom/google/protobuf/micro/CodedOutputStreamMicro;->computeMessageSize(ILcom/google/protobuf/micro/MessageMicro;)I

    move-result v1

    add-int/2addr v0, v1

    :cond_b
    invoke-virtual {p0}, Lcom/google/android/finsky/protos/DocAnnotations$Template;->hasNextBanner()Z

    move-result v1

    if-eqz v1, :cond_c

    const/16 v1, 0xd

    invoke-virtual {p0}, Lcom/google/android/finsky/protos/DocAnnotations$Template;->getNextBanner()Lcom/google/android/finsky/protos/DocAnnotations$NextBanner;

    move-result-object v2

    invoke-static {v1, v2}, Lcom/google/protobuf/micro/CodedOutputStreamMicro;->computeMessageSize(ILcom/google/protobuf/micro/MessageMicro;)I

    move-result v1

    add-int/2addr v0, v1

    :cond_c
    iput v0, p0, Lcom/google/android/finsky/protos/DocAnnotations$Template;->cachedSize:I

    return v0
.end method

.method public getSeriesAntenna()Lcom/google/android/finsky/protos/DocAnnotations$SeriesAntenna;
    .locals 1

    iget-object v0, p0, Lcom/google/android/finsky/protos/DocAnnotations$Template;->seriesAntenna_:Lcom/google/android/finsky/protos/DocAnnotations$SeriesAntenna;

    return-object v0
.end method

.method public getTileDetailsReflectedGraphic2X2()Lcom/google/android/finsky/protos/DocAnnotations$TileTemplate;
    .locals 1

    iget-object v0, p0, Lcom/google/android/finsky/protos/DocAnnotations$Template;->tileDetailsReflectedGraphic2X2_:Lcom/google/android/finsky/protos/DocAnnotations$TileTemplate;

    return-object v0
.end method

.method public getTileFourBlock4X2()Lcom/google/android/finsky/protos/DocAnnotations$TileTemplate;
    .locals 1

    iget-object v0, p0, Lcom/google/android/finsky/protos/DocAnnotations$Template;->tileFourBlock4X2_:Lcom/google/android/finsky/protos/DocAnnotations$TileTemplate;

    return-object v0
.end method

.method public getTileGraphic2X1()Lcom/google/android/finsky/protos/DocAnnotations$TileTemplate;
    .locals 1

    iget-object v0, p0, Lcom/google/android/finsky/protos/DocAnnotations$Template;->tileGraphic2X1_:Lcom/google/android/finsky/protos/DocAnnotations$TileTemplate;

    return-object v0
.end method

.method public getTileGraphic4X2()Lcom/google/android/finsky/protos/DocAnnotations$TileTemplate;
    .locals 1

    iget-object v0, p0, Lcom/google/android/finsky/protos/DocAnnotations$Template;->tileGraphic4X2_:Lcom/google/android/finsky/protos/DocAnnotations$TileTemplate;

    return-object v0
.end method

.method public getTileGraphicColoredTitle2X1()Lcom/google/android/finsky/protos/DocAnnotations$TileTemplate;
    .locals 1

    iget-object v0, p0, Lcom/google/android/finsky/protos/DocAnnotations$Template;->tileGraphicColoredTitle2X1_:Lcom/google/android/finsky/protos/DocAnnotations$TileTemplate;

    return-object v0
.end method

.method public getTileGraphicColoredTitle4X2()Lcom/google/android/finsky/protos/DocAnnotations$TileTemplate;
    .locals 1

    iget-object v0, p0, Lcom/google/android/finsky/protos/DocAnnotations$Template;->tileGraphicColoredTitle4X2_:Lcom/google/android/finsky/protos/DocAnnotations$TileTemplate;

    return-object v0
.end method

.method public getTileGraphicUpperLeftTitle2X1()Lcom/google/android/finsky/protos/DocAnnotations$TileTemplate;
    .locals 1

    iget-object v0, p0, Lcom/google/android/finsky/protos/DocAnnotations$Template;->tileGraphicUpperLeftTitle2X1_:Lcom/google/android/finsky/protos/DocAnnotations$TileTemplate;

    return-object v0
.end method

.method public hasContainerWithBanner()Z
    .locals 1

    iget-boolean v0, p0, Lcom/google/android/finsky/protos/DocAnnotations$Template;->hasContainerWithBanner:Z

    return v0
.end method

.method public hasDealOfTheDay()Z
    .locals 1

    iget-boolean v0, p0, Lcom/google/android/finsky/protos/DocAnnotations$Template;->hasDealOfTheDay:Z

    return v0
.end method

.method public hasEditorialSeriesContainer()Z
    .locals 1

    iget-boolean v0, p0, Lcom/google/android/finsky/protos/DocAnnotations$Template;->hasEditorialSeriesContainer:Z

    return v0
.end method

.method public hasNextBanner()Z
    .locals 1

    iget-boolean v0, p0, Lcom/google/android/finsky/protos/DocAnnotations$Template;->hasNextBanner:Z

    return v0
.end method

.method public hasRecommendationsContainer()Z
    .locals 1

    iget-boolean v0, p0, Lcom/google/android/finsky/protos/DocAnnotations$Template;->hasRecommendationsContainer:Z

    return v0
.end method

.method public hasSeriesAntenna()Z
    .locals 1

    iget-boolean v0, p0, Lcom/google/android/finsky/protos/DocAnnotations$Template;->hasSeriesAntenna:Z

    return v0
.end method

.method public hasTileDetailsReflectedGraphic2X2()Z
    .locals 1

    iget-boolean v0, p0, Lcom/google/android/finsky/protos/DocAnnotations$Template;->hasTileDetailsReflectedGraphic2X2:Z

    return v0
.end method

.method public hasTileFourBlock4X2()Z
    .locals 1

    iget-boolean v0, p0, Lcom/google/android/finsky/protos/DocAnnotations$Template;->hasTileFourBlock4X2:Z

    return v0
.end method

.method public hasTileGraphic2X1()Z
    .locals 1

    iget-boolean v0, p0, Lcom/google/android/finsky/protos/DocAnnotations$Template;->hasTileGraphic2X1:Z

    return v0
.end method

.method public hasTileGraphic4X2()Z
    .locals 1

    iget-boolean v0, p0, Lcom/google/android/finsky/protos/DocAnnotations$Template;->hasTileGraphic4X2:Z

    return v0
.end method

.method public hasTileGraphicColoredTitle2X1()Z
    .locals 1

    iget-boolean v0, p0, Lcom/google/android/finsky/protos/DocAnnotations$Template;->hasTileGraphicColoredTitle2X1:Z

    return v0
.end method

.method public hasTileGraphicColoredTitle4X2()Z
    .locals 1

    iget-boolean v0, p0, Lcom/google/android/finsky/protos/DocAnnotations$Template;->hasTileGraphicColoredTitle4X2:Z

    return v0
.end method

.method public hasTileGraphicUpperLeftTitle2X1()Z
    .locals 1

    iget-boolean v0, p0, Lcom/google/android/finsky/protos/DocAnnotations$Template;->hasTileGraphicUpperLeftTitle2X1:Z

    return v0
.end method

.method public mergeFrom(Lcom/google/protobuf/micro/CodedInputStreamMicro;)Lcom/google/android/finsky/protos/DocAnnotations$Template;
    .locals 3
    .param p1    # Lcom/google/protobuf/micro/CodedInputStreamMicro;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    :cond_0
    :goto_0
    invoke-virtual {p1}, Lcom/google/protobuf/micro/CodedInputStreamMicro;->readTag()I

    move-result v0

    sparse-switch v0, :sswitch_data_0

    invoke-virtual {p0, p1, v0}, Lcom/google/android/finsky/protos/DocAnnotations$Template;->parseUnknownField(Lcom/google/protobuf/micro/CodedInputStreamMicro;I)Z

    move-result v2

    if-nez v2, :cond_0

    :sswitch_0
    return-object p0

    :sswitch_1
    new-instance v1, Lcom/google/android/finsky/protos/DocAnnotations$SeriesAntenna;

    invoke-direct {v1}, Lcom/google/android/finsky/protos/DocAnnotations$SeriesAntenna;-><init>()V

    invoke-virtual {p1, v1}, Lcom/google/protobuf/micro/CodedInputStreamMicro;->readMessage(Lcom/google/protobuf/micro/MessageMicro;)V

    invoke-virtual {p0, v1}, Lcom/google/android/finsky/protos/DocAnnotations$Template;->setSeriesAntenna(Lcom/google/android/finsky/protos/DocAnnotations$SeriesAntenna;)Lcom/google/android/finsky/protos/DocAnnotations$Template;

    goto :goto_0

    :sswitch_2
    new-instance v1, Lcom/google/android/finsky/protos/DocAnnotations$TileTemplate;

    invoke-direct {v1}, Lcom/google/android/finsky/protos/DocAnnotations$TileTemplate;-><init>()V

    invoke-virtual {p1, v1}, Lcom/google/protobuf/micro/CodedInputStreamMicro;->readMessage(Lcom/google/protobuf/micro/MessageMicro;)V

    invoke-virtual {p0, v1}, Lcom/google/android/finsky/protos/DocAnnotations$Template;->setTileGraphic2X1(Lcom/google/android/finsky/protos/DocAnnotations$TileTemplate;)Lcom/google/android/finsky/protos/DocAnnotations$Template;

    goto :goto_0

    :sswitch_3
    new-instance v1, Lcom/google/android/finsky/protos/DocAnnotations$TileTemplate;

    invoke-direct {v1}, Lcom/google/android/finsky/protos/DocAnnotations$TileTemplate;-><init>()V

    invoke-virtual {p1, v1}, Lcom/google/protobuf/micro/CodedInputStreamMicro;->readMessage(Lcom/google/protobuf/micro/MessageMicro;)V

    invoke-virtual {p0, v1}, Lcom/google/android/finsky/protos/DocAnnotations$Template;->setTileGraphic4X2(Lcom/google/android/finsky/protos/DocAnnotations$TileTemplate;)Lcom/google/android/finsky/protos/DocAnnotations$Template;

    goto :goto_0

    :sswitch_4
    new-instance v1, Lcom/google/android/finsky/protos/DocAnnotations$TileTemplate;

    invoke-direct {v1}, Lcom/google/android/finsky/protos/DocAnnotations$TileTemplate;-><init>()V

    invoke-virtual {p1, v1}, Lcom/google/protobuf/micro/CodedInputStreamMicro;->readMessage(Lcom/google/protobuf/micro/MessageMicro;)V

    invoke-virtual {p0, v1}, Lcom/google/android/finsky/protos/DocAnnotations$Template;->setTileGraphicColoredTitle2X1(Lcom/google/android/finsky/protos/DocAnnotations$TileTemplate;)Lcom/google/android/finsky/protos/DocAnnotations$Template;

    goto :goto_0

    :sswitch_5
    new-instance v1, Lcom/google/android/finsky/protos/DocAnnotations$TileTemplate;

    invoke-direct {v1}, Lcom/google/android/finsky/protos/DocAnnotations$TileTemplate;-><init>()V

    invoke-virtual {p1, v1}, Lcom/google/protobuf/micro/CodedInputStreamMicro;->readMessage(Lcom/google/protobuf/micro/MessageMicro;)V

    invoke-virtual {p0, v1}, Lcom/google/android/finsky/protos/DocAnnotations$Template;->setTileGraphicUpperLeftTitle2X1(Lcom/google/android/finsky/protos/DocAnnotations$TileTemplate;)Lcom/google/android/finsky/protos/DocAnnotations$Template;

    goto :goto_0

    :sswitch_6
    new-instance v1, Lcom/google/android/finsky/protos/DocAnnotations$TileTemplate;

    invoke-direct {v1}, Lcom/google/android/finsky/protos/DocAnnotations$TileTemplate;-><init>()V

    invoke-virtual {p1, v1}, Lcom/google/protobuf/micro/CodedInputStreamMicro;->readMessage(Lcom/google/protobuf/micro/MessageMicro;)V

    invoke-virtual {p0, v1}, Lcom/google/android/finsky/protos/DocAnnotations$Template;->setTileDetailsReflectedGraphic2X2(Lcom/google/android/finsky/protos/DocAnnotations$TileTemplate;)Lcom/google/android/finsky/protos/DocAnnotations$Template;

    goto :goto_0

    :sswitch_7
    new-instance v1, Lcom/google/android/finsky/protos/DocAnnotations$TileTemplate;

    invoke-direct {v1}, Lcom/google/android/finsky/protos/DocAnnotations$TileTemplate;-><init>()V

    invoke-virtual {p1, v1}, Lcom/google/protobuf/micro/CodedInputStreamMicro;->readMessage(Lcom/google/protobuf/micro/MessageMicro;)V

    invoke-virtual {p0, v1}, Lcom/google/android/finsky/protos/DocAnnotations$Template;->setTileFourBlock4X2(Lcom/google/android/finsky/protos/DocAnnotations$TileTemplate;)Lcom/google/android/finsky/protos/DocAnnotations$Template;

    goto :goto_0

    :sswitch_8
    new-instance v1, Lcom/google/android/finsky/protos/DocAnnotations$ContainerWithBanner;

    invoke-direct {v1}, Lcom/google/android/finsky/protos/DocAnnotations$ContainerWithBanner;-><init>()V

    invoke-virtual {p1, v1}, Lcom/google/protobuf/micro/CodedInputStreamMicro;->readMessage(Lcom/google/protobuf/micro/MessageMicro;)V

    invoke-virtual {p0, v1}, Lcom/google/android/finsky/protos/DocAnnotations$Template;->setContainerWithBanner(Lcom/google/android/finsky/protos/DocAnnotations$ContainerWithBanner;)Lcom/google/android/finsky/protos/DocAnnotations$Template;

    goto :goto_0

    :sswitch_9
    new-instance v1, Lcom/google/android/finsky/protos/DocAnnotations$DealOfTheDay;

    invoke-direct {v1}, Lcom/google/android/finsky/protos/DocAnnotations$DealOfTheDay;-><init>()V

    invoke-virtual {p1, v1}, Lcom/google/protobuf/micro/CodedInputStreamMicro;->readMessage(Lcom/google/protobuf/micro/MessageMicro;)V

    invoke-virtual {p0, v1}, Lcom/google/android/finsky/protos/DocAnnotations$Template;->setDealOfTheDay(Lcom/google/android/finsky/protos/DocAnnotations$DealOfTheDay;)Lcom/google/android/finsky/protos/DocAnnotations$Template;

    goto :goto_0

    :sswitch_a
    new-instance v1, Lcom/google/android/finsky/protos/DocAnnotations$TileTemplate;

    invoke-direct {v1}, Lcom/google/android/finsky/protos/DocAnnotations$TileTemplate;-><init>()V

    invoke-virtual {p1, v1}, Lcom/google/protobuf/micro/CodedInputStreamMicro;->readMessage(Lcom/google/protobuf/micro/MessageMicro;)V

    invoke-virtual {p0, v1}, Lcom/google/android/finsky/protos/DocAnnotations$Template;->setTileGraphicColoredTitle4X2(Lcom/google/android/finsky/protos/DocAnnotations$TileTemplate;)Lcom/google/android/finsky/protos/DocAnnotations$Template;

    goto/16 :goto_0

    :sswitch_b
    new-instance v1, Lcom/google/android/finsky/protos/DocAnnotations$EditorialSeriesContainer;

    invoke-direct {v1}, Lcom/google/android/finsky/protos/DocAnnotations$EditorialSeriesContainer;-><init>()V

    invoke-virtual {p1, v1}, Lcom/google/protobuf/micro/CodedInputStreamMicro;->readMessage(Lcom/google/protobuf/micro/MessageMicro;)V

    invoke-virtual {p0, v1}, Lcom/google/android/finsky/protos/DocAnnotations$Template;->setEditorialSeriesContainer(Lcom/google/android/finsky/protos/DocAnnotations$EditorialSeriesContainer;)Lcom/google/android/finsky/protos/DocAnnotations$Template;

    goto/16 :goto_0

    :sswitch_c
    new-instance v1, Lcom/google/android/finsky/protos/DocAnnotations$RecommendationsContainer;

    invoke-direct {v1}, Lcom/google/android/finsky/protos/DocAnnotations$RecommendationsContainer;-><init>()V

    invoke-virtual {p1, v1}, Lcom/google/protobuf/micro/CodedInputStreamMicro;->readMessage(Lcom/google/protobuf/micro/MessageMicro;)V

    invoke-virtual {p0, v1}, Lcom/google/android/finsky/protos/DocAnnotations$Template;->setRecommendationsContainer(Lcom/google/android/finsky/protos/DocAnnotations$RecommendationsContainer;)Lcom/google/android/finsky/protos/DocAnnotations$Template;

    goto/16 :goto_0

    :sswitch_d
    new-instance v1, Lcom/google/android/finsky/protos/DocAnnotations$NextBanner;

    invoke-direct {v1}, Lcom/google/android/finsky/protos/DocAnnotations$NextBanner;-><init>()V

    invoke-virtual {p1, v1}, Lcom/google/protobuf/micro/CodedInputStreamMicro;->readMessage(Lcom/google/protobuf/micro/MessageMicro;)V

    invoke-virtual {p0, v1}, Lcom/google/android/finsky/protos/DocAnnotations$Template;->setNextBanner(Lcom/google/android/finsky/protos/DocAnnotations$NextBanner;)Lcom/google/android/finsky/protos/DocAnnotations$Template;

    goto/16 :goto_0

    :sswitch_data_0
    .sparse-switch
        0x0 -> :sswitch_0
        0xa -> :sswitch_1
        0x12 -> :sswitch_2
        0x1a -> :sswitch_3
        0x22 -> :sswitch_4
        0x2a -> :sswitch_5
        0x32 -> :sswitch_6
        0x3a -> :sswitch_7
        0x42 -> :sswitch_8
        0x4a -> :sswitch_9
        0x52 -> :sswitch_a
        0x5a -> :sswitch_b
        0x62 -> :sswitch_c
        0x6a -> :sswitch_d
    .end sparse-switch
.end method

.method public bridge synthetic mergeFrom(Lcom/google/protobuf/micro/CodedInputStreamMicro;)Lcom/google/protobuf/micro/MessageMicro;
    .locals 1
    .param p1    # Lcom/google/protobuf/micro/CodedInputStreamMicro;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    invoke-virtual {p0, p1}, Lcom/google/android/finsky/protos/DocAnnotations$Template;->mergeFrom(Lcom/google/protobuf/micro/CodedInputStreamMicro;)Lcom/google/android/finsky/protos/DocAnnotations$Template;

    move-result-object v0

    return-object v0
.end method

.method public setContainerWithBanner(Lcom/google/android/finsky/protos/DocAnnotations$ContainerWithBanner;)Lcom/google/android/finsky/protos/DocAnnotations$Template;
    .locals 1
    .param p1    # Lcom/google/android/finsky/protos/DocAnnotations$ContainerWithBanner;

    if-nez p1, :cond_0

    new-instance v0, Ljava/lang/NullPointerException;

    invoke-direct {v0}, Ljava/lang/NullPointerException;-><init>()V

    throw v0

    :cond_0
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/google/android/finsky/protos/DocAnnotations$Template;->hasContainerWithBanner:Z

    iput-object p1, p0, Lcom/google/android/finsky/protos/DocAnnotations$Template;->containerWithBanner_:Lcom/google/android/finsky/protos/DocAnnotations$ContainerWithBanner;

    return-object p0
.end method

.method public setDealOfTheDay(Lcom/google/android/finsky/protos/DocAnnotations$DealOfTheDay;)Lcom/google/android/finsky/protos/DocAnnotations$Template;
    .locals 1
    .param p1    # Lcom/google/android/finsky/protos/DocAnnotations$DealOfTheDay;

    if-nez p1, :cond_0

    new-instance v0, Ljava/lang/NullPointerException;

    invoke-direct {v0}, Ljava/lang/NullPointerException;-><init>()V

    throw v0

    :cond_0
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/google/android/finsky/protos/DocAnnotations$Template;->hasDealOfTheDay:Z

    iput-object p1, p0, Lcom/google/android/finsky/protos/DocAnnotations$Template;->dealOfTheDay_:Lcom/google/android/finsky/protos/DocAnnotations$DealOfTheDay;

    return-object p0
.end method

.method public setEditorialSeriesContainer(Lcom/google/android/finsky/protos/DocAnnotations$EditorialSeriesContainer;)Lcom/google/android/finsky/protos/DocAnnotations$Template;
    .locals 1
    .param p1    # Lcom/google/android/finsky/protos/DocAnnotations$EditorialSeriesContainer;

    if-nez p1, :cond_0

    new-instance v0, Ljava/lang/NullPointerException;

    invoke-direct {v0}, Ljava/lang/NullPointerException;-><init>()V

    throw v0

    :cond_0
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/google/android/finsky/protos/DocAnnotations$Template;->hasEditorialSeriesContainer:Z

    iput-object p1, p0, Lcom/google/android/finsky/protos/DocAnnotations$Template;->editorialSeriesContainer_:Lcom/google/android/finsky/protos/DocAnnotations$EditorialSeriesContainer;

    return-object p0
.end method

.method public setNextBanner(Lcom/google/android/finsky/protos/DocAnnotations$NextBanner;)Lcom/google/android/finsky/protos/DocAnnotations$Template;
    .locals 1
    .param p1    # Lcom/google/android/finsky/protos/DocAnnotations$NextBanner;

    if-nez p1, :cond_0

    new-instance v0, Ljava/lang/NullPointerException;

    invoke-direct {v0}, Ljava/lang/NullPointerException;-><init>()V

    throw v0

    :cond_0
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/google/android/finsky/protos/DocAnnotations$Template;->hasNextBanner:Z

    iput-object p1, p0, Lcom/google/android/finsky/protos/DocAnnotations$Template;->nextBanner_:Lcom/google/android/finsky/protos/DocAnnotations$NextBanner;

    return-object p0
.end method

.method public setRecommendationsContainer(Lcom/google/android/finsky/protos/DocAnnotations$RecommendationsContainer;)Lcom/google/android/finsky/protos/DocAnnotations$Template;
    .locals 1
    .param p1    # Lcom/google/android/finsky/protos/DocAnnotations$RecommendationsContainer;

    if-nez p1, :cond_0

    new-instance v0, Ljava/lang/NullPointerException;

    invoke-direct {v0}, Ljava/lang/NullPointerException;-><init>()V

    throw v0

    :cond_0
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/google/android/finsky/protos/DocAnnotations$Template;->hasRecommendationsContainer:Z

    iput-object p1, p0, Lcom/google/android/finsky/protos/DocAnnotations$Template;->recommendationsContainer_:Lcom/google/android/finsky/protos/DocAnnotations$RecommendationsContainer;

    return-object p0
.end method

.method public setSeriesAntenna(Lcom/google/android/finsky/protos/DocAnnotations$SeriesAntenna;)Lcom/google/android/finsky/protos/DocAnnotations$Template;
    .locals 1
    .param p1    # Lcom/google/android/finsky/protos/DocAnnotations$SeriesAntenna;

    if-nez p1, :cond_0

    new-instance v0, Ljava/lang/NullPointerException;

    invoke-direct {v0}, Ljava/lang/NullPointerException;-><init>()V

    throw v0

    :cond_0
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/google/android/finsky/protos/DocAnnotations$Template;->hasSeriesAntenna:Z

    iput-object p1, p0, Lcom/google/android/finsky/protos/DocAnnotations$Template;->seriesAntenna_:Lcom/google/android/finsky/protos/DocAnnotations$SeriesAntenna;

    return-object p0
.end method

.method public setTileDetailsReflectedGraphic2X2(Lcom/google/android/finsky/protos/DocAnnotations$TileTemplate;)Lcom/google/android/finsky/protos/DocAnnotations$Template;
    .locals 1
    .param p1    # Lcom/google/android/finsky/protos/DocAnnotations$TileTemplate;

    if-nez p1, :cond_0

    new-instance v0, Ljava/lang/NullPointerException;

    invoke-direct {v0}, Ljava/lang/NullPointerException;-><init>()V

    throw v0

    :cond_0
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/google/android/finsky/protos/DocAnnotations$Template;->hasTileDetailsReflectedGraphic2X2:Z

    iput-object p1, p0, Lcom/google/android/finsky/protos/DocAnnotations$Template;->tileDetailsReflectedGraphic2X2_:Lcom/google/android/finsky/protos/DocAnnotations$TileTemplate;

    return-object p0
.end method

.method public setTileFourBlock4X2(Lcom/google/android/finsky/protos/DocAnnotations$TileTemplate;)Lcom/google/android/finsky/protos/DocAnnotations$Template;
    .locals 1
    .param p1    # Lcom/google/android/finsky/protos/DocAnnotations$TileTemplate;

    if-nez p1, :cond_0

    new-instance v0, Ljava/lang/NullPointerException;

    invoke-direct {v0}, Ljava/lang/NullPointerException;-><init>()V

    throw v0

    :cond_0
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/google/android/finsky/protos/DocAnnotations$Template;->hasTileFourBlock4X2:Z

    iput-object p1, p0, Lcom/google/android/finsky/protos/DocAnnotations$Template;->tileFourBlock4X2_:Lcom/google/android/finsky/protos/DocAnnotations$TileTemplate;

    return-object p0
.end method

.method public setTileGraphic2X1(Lcom/google/android/finsky/protos/DocAnnotations$TileTemplate;)Lcom/google/android/finsky/protos/DocAnnotations$Template;
    .locals 1
    .param p1    # Lcom/google/android/finsky/protos/DocAnnotations$TileTemplate;

    if-nez p1, :cond_0

    new-instance v0, Ljava/lang/NullPointerException;

    invoke-direct {v0}, Ljava/lang/NullPointerException;-><init>()V

    throw v0

    :cond_0
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/google/android/finsky/protos/DocAnnotations$Template;->hasTileGraphic2X1:Z

    iput-object p1, p0, Lcom/google/android/finsky/protos/DocAnnotations$Template;->tileGraphic2X1_:Lcom/google/android/finsky/protos/DocAnnotations$TileTemplate;

    return-object p0
.end method

.method public setTileGraphic4X2(Lcom/google/android/finsky/protos/DocAnnotations$TileTemplate;)Lcom/google/android/finsky/protos/DocAnnotations$Template;
    .locals 1
    .param p1    # Lcom/google/android/finsky/protos/DocAnnotations$TileTemplate;

    if-nez p1, :cond_0

    new-instance v0, Ljava/lang/NullPointerException;

    invoke-direct {v0}, Ljava/lang/NullPointerException;-><init>()V

    throw v0

    :cond_0
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/google/android/finsky/protos/DocAnnotations$Template;->hasTileGraphic4X2:Z

    iput-object p1, p0, Lcom/google/android/finsky/protos/DocAnnotations$Template;->tileGraphic4X2_:Lcom/google/android/finsky/protos/DocAnnotations$TileTemplate;

    return-object p0
.end method

.method public setTileGraphicColoredTitle2X1(Lcom/google/android/finsky/protos/DocAnnotations$TileTemplate;)Lcom/google/android/finsky/protos/DocAnnotations$Template;
    .locals 1
    .param p1    # Lcom/google/android/finsky/protos/DocAnnotations$TileTemplate;

    if-nez p1, :cond_0

    new-instance v0, Ljava/lang/NullPointerException;

    invoke-direct {v0}, Ljava/lang/NullPointerException;-><init>()V

    throw v0

    :cond_0
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/google/android/finsky/protos/DocAnnotations$Template;->hasTileGraphicColoredTitle2X1:Z

    iput-object p1, p0, Lcom/google/android/finsky/protos/DocAnnotations$Template;->tileGraphicColoredTitle2X1_:Lcom/google/android/finsky/protos/DocAnnotations$TileTemplate;

    return-object p0
.end method

.method public setTileGraphicColoredTitle4X2(Lcom/google/android/finsky/protos/DocAnnotations$TileTemplate;)Lcom/google/android/finsky/protos/DocAnnotations$Template;
    .locals 1
    .param p1    # Lcom/google/android/finsky/protos/DocAnnotations$TileTemplate;

    if-nez p1, :cond_0

    new-instance v0, Ljava/lang/NullPointerException;

    invoke-direct {v0}, Ljava/lang/NullPointerException;-><init>()V

    throw v0

    :cond_0
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/google/android/finsky/protos/DocAnnotations$Template;->hasTileGraphicColoredTitle4X2:Z

    iput-object p1, p0, Lcom/google/android/finsky/protos/DocAnnotations$Template;->tileGraphicColoredTitle4X2_:Lcom/google/android/finsky/protos/DocAnnotations$TileTemplate;

    return-object p0
.end method

.method public setTileGraphicUpperLeftTitle2X1(Lcom/google/android/finsky/protos/DocAnnotations$TileTemplate;)Lcom/google/android/finsky/protos/DocAnnotations$Template;
    .locals 1
    .param p1    # Lcom/google/android/finsky/protos/DocAnnotations$TileTemplate;

    if-nez p1, :cond_0

    new-instance v0, Ljava/lang/NullPointerException;

    invoke-direct {v0}, Ljava/lang/NullPointerException;-><init>()V

    throw v0

    :cond_0
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/google/android/finsky/protos/DocAnnotations$Template;->hasTileGraphicUpperLeftTitle2X1:Z

    iput-object p1, p0, Lcom/google/android/finsky/protos/DocAnnotations$Template;->tileGraphicUpperLeftTitle2X1_:Lcom/google/android/finsky/protos/DocAnnotations$TileTemplate;

    return-object p0
.end method

.method public writeTo(Lcom/google/protobuf/micro/CodedOutputStreamMicro;)V
    .locals 2
    .param p1    # Lcom/google/protobuf/micro/CodedOutputStreamMicro;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    invoke-virtual {p0}, Lcom/google/android/finsky/protos/DocAnnotations$Template;->hasSeriesAntenna()Z

    move-result v0

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    invoke-virtual {p0}, Lcom/google/android/finsky/protos/DocAnnotations$Template;->getSeriesAntenna()Lcom/google/android/finsky/protos/DocAnnotations$SeriesAntenna;

    move-result-object v1

    invoke-virtual {p1, v0, v1}, Lcom/google/protobuf/micro/CodedOutputStreamMicro;->writeMessage(ILcom/google/protobuf/micro/MessageMicro;)V

    :cond_0
    invoke-virtual {p0}, Lcom/google/android/finsky/protos/DocAnnotations$Template;->hasTileGraphic2X1()Z

    move-result v0

    if-eqz v0, :cond_1

    const/4 v0, 0x2

    invoke-virtual {p0}, Lcom/google/android/finsky/protos/DocAnnotations$Template;->getTileGraphic2X1()Lcom/google/android/finsky/protos/DocAnnotations$TileTemplate;

    move-result-object v1

    invoke-virtual {p1, v0, v1}, Lcom/google/protobuf/micro/CodedOutputStreamMicro;->writeMessage(ILcom/google/protobuf/micro/MessageMicro;)V

    :cond_1
    invoke-virtual {p0}, Lcom/google/android/finsky/protos/DocAnnotations$Template;->hasTileGraphic4X2()Z

    move-result v0

    if-eqz v0, :cond_2

    const/4 v0, 0x3

    invoke-virtual {p0}, Lcom/google/android/finsky/protos/DocAnnotations$Template;->getTileGraphic4X2()Lcom/google/android/finsky/protos/DocAnnotations$TileTemplate;

    move-result-object v1

    invoke-virtual {p1, v0, v1}, Lcom/google/protobuf/micro/CodedOutputStreamMicro;->writeMessage(ILcom/google/protobuf/micro/MessageMicro;)V

    :cond_2
    invoke-virtual {p0}, Lcom/google/android/finsky/protos/DocAnnotations$Template;->hasTileGraphicColoredTitle2X1()Z

    move-result v0

    if-eqz v0, :cond_3

    const/4 v0, 0x4

    invoke-virtual {p0}, Lcom/google/android/finsky/protos/DocAnnotations$Template;->getTileGraphicColoredTitle2X1()Lcom/google/android/finsky/protos/DocAnnotations$TileTemplate;

    move-result-object v1

    invoke-virtual {p1, v0, v1}, Lcom/google/protobuf/micro/CodedOutputStreamMicro;->writeMessage(ILcom/google/protobuf/micro/MessageMicro;)V

    :cond_3
    invoke-virtual {p0}, Lcom/google/android/finsky/protos/DocAnnotations$Template;->hasTileGraphicUpperLeftTitle2X1()Z

    move-result v0

    if-eqz v0, :cond_4

    const/4 v0, 0x5

    invoke-virtual {p0}, Lcom/google/android/finsky/protos/DocAnnotations$Template;->getTileGraphicUpperLeftTitle2X1()Lcom/google/android/finsky/protos/DocAnnotations$TileTemplate;

    move-result-object v1

    invoke-virtual {p1, v0, v1}, Lcom/google/protobuf/micro/CodedOutputStreamMicro;->writeMessage(ILcom/google/protobuf/micro/MessageMicro;)V

    :cond_4
    invoke-virtual {p0}, Lcom/google/android/finsky/protos/DocAnnotations$Template;->hasTileDetailsReflectedGraphic2X2()Z

    move-result v0

    if-eqz v0, :cond_5

    const/4 v0, 0x6

    invoke-virtual {p0}, Lcom/google/android/finsky/protos/DocAnnotations$Template;->getTileDetailsReflectedGraphic2X2()Lcom/google/android/finsky/protos/DocAnnotations$TileTemplate;

    move-result-object v1

    invoke-virtual {p1, v0, v1}, Lcom/google/protobuf/micro/CodedOutputStreamMicro;->writeMessage(ILcom/google/protobuf/micro/MessageMicro;)V

    :cond_5
    invoke-virtual {p0}, Lcom/google/android/finsky/protos/DocAnnotations$Template;->hasTileFourBlock4X2()Z

    move-result v0

    if-eqz v0, :cond_6

    const/4 v0, 0x7

    invoke-virtual {p0}, Lcom/google/android/finsky/protos/DocAnnotations$Template;->getTileFourBlock4X2()Lcom/google/android/finsky/protos/DocAnnotations$TileTemplate;

    move-result-object v1

    invoke-virtual {p1, v0, v1}, Lcom/google/protobuf/micro/CodedOutputStreamMicro;->writeMessage(ILcom/google/protobuf/micro/MessageMicro;)V

    :cond_6
    invoke-virtual {p0}, Lcom/google/android/finsky/protos/DocAnnotations$Template;->hasContainerWithBanner()Z

    move-result v0

    if-eqz v0, :cond_7

    const/16 v0, 0x8

    invoke-virtual {p0}, Lcom/google/android/finsky/protos/DocAnnotations$Template;->getContainerWithBanner()Lcom/google/android/finsky/protos/DocAnnotations$ContainerWithBanner;

    move-result-object v1

    invoke-virtual {p1, v0, v1}, Lcom/google/protobuf/micro/CodedOutputStreamMicro;->writeMessage(ILcom/google/protobuf/micro/MessageMicro;)V

    :cond_7
    invoke-virtual {p0}, Lcom/google/android/finsky/protos/DocAnnotations$Template;->hasDealOfTheDay()Z

    move-result v0

    if-eqz v0, :cond_8

    const/16 v0, 0x9

    invoke-virtual {p0}, Lcom/google/android/finsky/protos/DocAnnotations$Template;->getDealOfTheDay()Lcom/google/android/finsky/protos/DocAnnotations$DealOfTheDay;

    move-result-object v1

    invoke-virtual {p1, v0, v1}, Lcom/google/protobuf/micro/CodedOutputStreamMicro;->writeMessage(ILcom/google/protobuf/micro/MessageMicro;)V

    :cond_8
    invoke-virtual {p0}, Lcom/google/android/finsky/protos/DocAnnotations$Template;->hasTileGraphicColoredTitle4X2()Z

    move-result v0

    if-eqz v0, :cond_9

    const/16 v0, 0xa

    invoke-virtual {p0}, Lcom/google/android/finsky/protos/DocAnnotations$Template;->getTileGraphicColoredTitle4X2()Lcom/google/android/finsky/protos/DocAnnotations$TileTemplate;

    move-result-object v1

    invoke-virtual {p1, v0, v1}, Lcom/google/protobuf/micro/CodedOutputStreamMicro;->writeMessage(ILcom/google/protobuf/micro/MessageMicro;)V

    :cond_9
    invoke-virtual {p0}, Lcom/google/android/finsky/protos/DocAnnotations$Template;->hasEditorialSeriesContainer()Z

    move-result v0

    if-eqz v0, :cond_a

    const/16 v0, 0xb

    invoke-virtual {p0}, Lcom/google/android/finsky/protos/DocAnnotations$Template;->getEditorialSeriesContainer()Lcom/google/android/finsky/protos/DocAnnotations$EditorialSeriesContainer;

    move-result-object v1

    invoke-virtual {p1, v0, v1}, Lcom/google/protobuf/micro/CodedOutputStreamMicro;->writeMessage(ILcom/google/protobuf/micro/MessageMicro;)V

    :cond_a
    invoke-virtual {p0}, Lcom/google/android/finsky/protos/DocAnnotations$Template;->hasRecommendationsContainer()Z

    move-result v0

    if-eqz v0, :cond_b

    const/16 v0, 0xc

    invoke-virtual {p0}, Lcom/google/android/finsky/protos/DocAnnotations$Template;->getRecommendationsContainer()Lcom/google/android/finsky/protos/DocAnnotations$RecommendationsContainer;

    move-result-object v1

    invoke-virtual {p1, v0, v1}, Lcom/google/protobuf/micro/CodedOutputStreamMicro;->writeMessage(ILcom/google/protobuf/micro/MessageMicro;)V

    :cond_b
    invoke-virtual {p0}, Lcom/google/android/finsky/protos/DocAnnotations$Template;->hasNextBanner()Z

    move-result v0

    if-eqz v0, :cond_c

    const/16 v0, 0xd

    invoke-virtual {p0}, Lcom/google/android/finsky/protos/DocAnnotations$Template;->getNextBanner()Lcom/google/android/finsky/protos/DocAnnotations$NextBanner;

    move-result-object v1

    invoke-virtual {p1, v0, v1}, Lcom/google/protobuf/micro/CodedOutputStreamMicro;->writeMessage(ILcom/google/protobuf/micro/MessageMicro;)V

    :cond_c
    return-void
.end method
