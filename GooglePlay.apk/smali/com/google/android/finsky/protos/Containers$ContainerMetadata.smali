.class public final Lcom/google/android/finsky/protos/Containers$ContainerMetadata;
.super Lcom/google/protobuf/micro/MessageMicro;
.source "Containers.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/google/android/finsky/protos/Containers;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x19
    name = "ContainerMetadata"
.end annotation


# instance fields
.field private analyticsCookie_:Ljava/lang/String;

.field private browseUrl_:Ljava/lang/String;

.field private cachedSize:I

.field private containerView_:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Lcom/google/android/finsky/protos/Containers$ContainerView;",
            ">;"
        }
    .end annotation
.end field

.field private estimatedResults_:J

.field private hasAnalyticsCookie:Z

.field private hasBrowseUrl:Z

.field private hasEstimatedResults:Z

.field private hasNextPageUrl:Z

.field private hasOrdered:Z

.field private hasRelevance:Z

.field private nextPageUrl_:Ljava/lang/String;

.field private ordered_:Z

.field private relevance_:D


# direct methods
.method public constructor <init>()V
    .locals 2

    invoke-direct {p0}, Lcom/google/protobuf/micro/MessageMicro;-><init>()V

    const-string v0, ""

    iput-object v0, p0, Lcom/google/android/finsky/protos/Containers$ContainerMetadata;->browseUrl_:Ljava/lang/String;

    const-string v0, ""

    iput-object v0, p0, Lcom/google/android/finsky/protos/Containers$ContainerMetadata;->nextPageUrl_:Ljava/lang/String;

    const-wide/16 v0, 0x0

    iput-wide v0, p0, Lcom/google/android/finsky/protos/Containers$ContainerMetadata;->relevance_:D

    const-wide/16 v0, 0x0

    iput-wide v0, p0, Lcom/google/android/finsky/protos/Containers$ContainerMetadata;->estimatedResults_:J

    const-string v0, ""

    iput-object v0, p0, Lcom/google/android/finsky/protos/Containers$ContainerMetadata;->analyticsCookie_:Ljava/lang/String;

    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/google/android/finsky/protos/Containers$ContainerMetadata;->ordered_:Z

    invoke-static {}, Ljava/util/Collections;->emptyList()Ljava/util/List;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/finsky/protos/Containers$ContainerMetadata;->containerView_:Ljava/util/List;

    const/4 v0, -0x1

    iput v0, p0, Lcom/google/android/finsky/protos/Containers$ContainerMetadata;->cachedSize:I

    return-void
.end method


# virtual methods
.method public addContainerView(Lcom/google/android/finsky/protos/Containers$ContainerView;)Lcom/google/android/finsky/protos/Containers$ContainerMetadata;
    .locals 1
    .param p1    # Lcom/google/android/finsky/protos/Containers$ContainerView;

    if-nez p1, :cond_0

    new-instance v0, Ljava/lang/NullPointerException;

    invoke-direct {v0}, Ljava/lang/NullPointerException;-><init>()V

    throw v0

    :cond_0
    iget-object v0, p0, Lcom/google/android/finsky/protos/Containers$ContainerMetadata;->containerView_:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->isEmpty()Z

    move-result v0

    if-eqz v0, :cond_1

    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, Lcom/google/android/finsky/protos/Containers$ContainerMetadata;->containerView_:Ljava/util/List;

    :cond_1
    iget-object v0, p0, Lcom/google/android/finsky/protos/Containers$ContainerMetadata;->containerView_:Ljava/util/List;

    invoke-interface {v0, p1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    return-object p0
.end method

.method public getAnalyticsCookie()Ljava/lang/String;
    .locals 1

    iget-object v0, p0, Lcom/google/android/finsky/protos/Containers$ContainerMetadata;->analyticsCookie_:Ljava/lang/String;

    return-object v0
.end method

.method public getBrowseUrl()Ljava/lang/String;
    .locals 1

    iget-object v0, p0, Lcom/google/android/finsky/protos/Containers$ContainerMetadata;->browseUrl_:Ljava/lang/String;

    return-object v0
.end method

.method public getCachedSize()I
    .locals 1

    iget v0, p0, Lcom/google/android/finsky/protos/Containers$ContainerMetadata;->cachedSize:I

    if-gez v0, :cond_0

    invoke-virtual {p0}, Lcom/google/android/finsky/protos/Containers$ContainerMetadata;->getSerializedSize()I

    :cond_0
    iget v0, p0, Lcom/google/android/finsky/protos/Containers$ContainerMetadata;->cachedSize:I

    return v0
.end method

.method public getContainerViewCount()I
    .locals 1

    iget-object v0, p0, Lcom/google/android/finsky/protos/Containers$ContainerMetadata;->containerView_:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->size()I

    move-result v0

    return v0
.end method

.method public getContainerViewList()Ljava/util/List;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/List",
            "<",
            "Lcom/google/android/finsky/protos/Containers$ContainerView;",
            ">;"
        }
    .end annotation

    iget-object v0, p0, Lcom/google/android/finsky/protos/Containers$ContainerMetadata;->containerView_:Ljava/util/List;

    return-object v0
.end method

.method public getEstimatedResults()J
    .locals 2

    iget-wide v0, p0, Lcom/google/android/finsky/protos/Containers$ContainerMetadata;->estimatedResults_:J

    return-wide v0
.end method

.method public getNextPageUrl()Ljava/lang/String;
    .locals 1

    iget-object v0, p0, Lcom/google/android/finsky/protos/Containers$ContainerMetadata;->nextPageUrl_:Ljava/lang/String;

    return-object v0
.end method

.method public getOrdered()Z
    .locals 1

    iget-boolean v0, p0, Lcom/google/android/finsky/protos/Containers$ContainerMetadata;->ordered_:Z

    return v0
.end method

.method public getRelevance()D
    .locals 2

    iget-wide v0, p0, Lcom/google/android/finsky/protos/Containers$ContainerMetadata;->relevance_:D

    return-wide v0
.end method

.method public getSerializedSize()I
    .locals 6

    const/4 v2, 0x0

    invoke-virtual {p0}, Lcom/google/android/finsky/protos/Containers$ContainerMetadata;->hasBrowseUrl()Z

    move-result v3

    if-eqz v3, :cond_0

    const/4 v3, 0x1

    invoke-virtual {p0}, Lcom/google/android/finsky/protos/Containers$ContainerMetadata;->getBrowseUrl()Ljava/lang/String;

    move-result-object v4

    invoke-static {v3, v4}, Lcom/google/protobuf/micro/CodedOutputStreamMicro;->computeStringSize(ILjava/lang/String;)I

    move-result v3

    add-int/2addr v2, v3

    :cond_0
    invoke-virtual {p0}, Lcom/google/android/finsky/protos/Containers$ContainerMetadata;->hasNextPageUrl()Z

    move-result v3

    if-eqz v3, :cond_1

    const/4 v3, 0x2

    invoke-virtual {p0}, Lcom/google/android/finsky/protos/Containers$ContainerMetadata;->getNextPageUrl()Ljava/lang/String;

    move-result-object v4

    invoke-static {v3, v4}, Lcom/google/protobuf/micro/CodedOutputStreamMicro;->computeStringSize(ILjava/lang/String;)I

    move-result v3

    add-int/2addr v2, v3

    :cond_1
    invoke-virtual {p0}, Lcom/google/android/finsky/protos/Containers$ContainerMetadata;->hasRelevance()Z

    move-result v3

    if-eqz v3, :cond_2

    const/4 v3, 0x3

    invoke-virtual {p0}, Lcom/google/android/finsky/protos/Containers$ContainerMetadata;->getRelevance()D

    move-result-wide v4

    invoke-static {v3, v4, v5}, Lcom/google/protobuf/micro/CodedOutputStreamMicro;->computeDoubleSize(ID)I

    move-result v3

    add-int/2addr v2, v3

    :cond_2
    invoke-virtual {p0}, Lcom/google/android/finsky/protos/Containers$ContainerMetadata;->hasEstimatedResults()Z

    move-result v3

    if-eqz v3, :cond_3

    const/4 v3, 0x4

    invoke-virtual {p0}, Lcom/google/android/finsky/protos/Containers$ContainerMetadata;->getEstimatedResults()J

    move-result-wide v4

    invoke-static {v3, v4, v5}, Lcom/google/protobuf/micro/CodedOutputStreamMicro;->computeInt64Size(IJ)I

    move-result v3

    add-int/2addr v2, v3

    :cond_3
    invoke-virtual {p0}, Lcom/google/android/finsky/protos/Containers$ContainerMetadata;->hasAnalyticsCookie()Z

    move-result v3

    if-eqz v3, :cond_4

    const/4 v3, 0x5

    invoke-virtual {p0}, Lcom/google/android/finsky/protos/Containers$ContainerMetadata;->getAnalyticsCookie()Ljava/lang/String;

    move-result-object v4

    invoke-static {v3, v4}, Lcom/google/protobuf/micro/CodedOutputStreamMicro;->computeStringSize(ILjava/lang/String;)I

    move-result v3

    add-int/2addr v2, v3

    :cond_4
    invoke-virtual {p0}, Lcom/google/android/finsky/protos/Containers$ContainerMetadata;->hasOrdered()Z

    move-result v3

    if-eqz v3, :cond_5

    const/4 v3, 0x6

    invoke-virtual {p0}, Lcom/google/android/finsky/protos/Containers$ContainerMetadata;->getOrdered()Z

    move-result v4

    invoke-static {v3, v4}, Lcom/google/protobuf/micro/CodedOutputStreamMicro;->computeBoolSize(IZ)I

    move-result v3

    add-int/2addr v2, v3

    :cond_5
    invoke-virtual {p0}, Lcom/google/android/finsky/protos/Containers$ContainerMetadata;->getContainerViewList()Ljava/util/List;

    move-result-object v3

    invoke-interface {v3}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :goto_0
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v3

    if-eqz v3, :cond_6

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/finsky/protos/Containers$ContainerView;

    const/4 v3, 0x7

    invoke-static {v3, v0}, Lcom/google/protobuf/micro/CodedOutputStreamMicro;->computeMessageSize(ILcom/google/protobuf/micro/MessageMicro;)I

    move-result v3

    add-int/2addr v2, v3

    goto :goto_0

    :cond_6
    iput v2, p0, Lcom/google/android/finsky/protos/Containers$ContainerMetadata;->cachedSize:I

    return v2
.end method

.method public hasAnalyticsCookie()Z
    .locals 1

    iget-boolean v0, p0, Lcom/google/android/finsky/protos/Containers$ContainerMetadata;->hasAnalyticsCookie:Z

    return v0
.end method

.method public hasBrowseUrl()Z
    .locals 1

    iget-boolean v0, p0, Lcom/google/android/finsky/protos/Containers$ContainerMetadata;->hasBrowseUrl:Z

    return v0
.end method

.method public hasEstimatedResults()Z
    .locals 1

    iget-boolean v0, p0, Lcom/google/android/finsky/protos/Containers$ContainerMetadata;->hasEstimatedResults:Z

    return v0
.end method

.method public hasNextPageUrl()Z
    .locals 1

    iget-boolean v0, p0, Lcom/google/android/finsky/protos/Containers$ContainerMetadata;->hasNextPageUrl:Z

    return v0
.end method

.method public hasOrdered()Z
    .locals 1

    iget-boolean v0, p0, Lcom/google/android/finsky/protos/Containers$ContainerMetadata;->hasOrdered:Z

    return v0
.end method

.method public hasRelevance()Z
    .locals 1

    iget-boolean v0, p0, Lcom/google/android/finsky/protos/Containers$ContainerMetadata;->hasRelevance:Z

    return v0
.end method

.method public mergeFrom(Lcom/google/protobuf/micro/CodedInputStreamMicro;)Lcom/google/android/finsky/protos/Containers$ContainerMetadata;
    .locals 4
    .param p1    # Lcom/google/protobuf/micro/CodedInputStreamMicro;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    :cond_0
    :goto_0
    invoke-virtual {p1}, Lcom/google/protobuf/micro/CodedInputStreamMicro;->readTag()I

    move-result v0

    sparse-switch v0, :sswitch_data_0

    invoke-virtual {p0, p1, v0}, Lcom/google/android/finsky/protos/Containers$ContainerMetadata;->parseUnknownField(Lcom/google/protobuf/micro/CodedInputStreamMicro;I)Z

    move-result v2

    if-nez v2, :cond_0

    :sswitch_0
    return-object p0

    :sswitch_1
    invoke-virtual {p1}, Lcom/google/protobuf/micro/CodedInputStreamMicro;->readString()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {p0, v2}, Lcom/google/android/finsky/protos/Containers$ContainerMetadata;->setBrowseUrl(Ljava/lang/String;)Lcom/google/android/finsky/protos/Containers$ContainerMetadata;

    goto :goto_0

    :sswitch_2
    invoke-virtual {p1}, Lcom/google/protobuf/micro/CodedInputStreamMicro;->readString()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {p0, v2}, Lcom/google/android/finsky/protos/Containers$ContainerMetadata;->setNextPageUrl(Ljava/lang/String;)Lcom/google/android/finsky/protos/Containers$ContainerMetadata;

    goto :goto_0

    :sswitch_3
    invoke-virtual {p1}, Lcom/google/protobuf/micro/CodedInputStreamMicro;->readDouble()D

    move-result-wide v2

    invoke-virtual {p0, v2, v3}, Lcom/google/android/finsky/protos/Containers$ContainerMetadata;->setRelevance(D)Lcom/google/android/finsky/protos/Containers$ContainerMetadata;

    goto :goto_0

    :sswitch_4
    invoke-virtual {p1}, Lcom/google/protobuf/micro/CodedInputStreamMicro;->readInt64()J

    move-result-wide v2

    invoke-virtual {p0, v2, v3}, Lcom/google/android/finsky/protos/Containers$ContainerMetadata;->setEstimatedResults(J)Lcom/google/android/finsky/protos/Containers$ContainerMetadata;

    goto :goto_0

    :sswitch_5
    invoke-virtual {p1}, Lcom/google/protobuf/micro/CodedInputStreamMicro;->readString()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {p0, v2}, Lcom/google/android/finsky/protos/Containers$ContainerMetadata;->setAnalyticsCookie(Ljava/lang/String;)Lcom/google/android/finsky/protos/Containers$ContainerMetadata;

    goto :goto_0

    :sswitch_6
    invoke-virtual {p1}, Lcom/google/protobuf/micro/CodedInputStreamMicro;->readBool()Z

    move-result v2

    invoke-virtual {p0, v2}, Lcom/google/android/finsky/protos/Containers$ContainerMetadata;->setOrdered(Z)Lcom/google/android/finsky/protos/Containers$ContainerMetadata;

    goto :goto_0

    :sswitch_7
    new-instance v1, Lcom/google/android/finsky/protos/Containers$ContainerView;

    invoke-direct {v1}, Lcom/google/android/finsky/protos/Containers$ContainerView;-><init>()V

    invoke-virtual {p1, v1}, Lcom/google/protobuf/micro/CodedInputStreamMicro;->readMessage(Lcom/google/protobuf/micro/MessageMicro;)V

    invoke-virtual {p0, v1}, Lcom/google/android/finsky/protos/Containers$ContainerMetadata;->addContainerView(Lcom/google/android/finsky/protos/Containers$ContainerView;)Lcom/google/android/finsky/protos/Containers$ContainerMetadata;

    goto :goto_0

    :sswitch_data_0
    .sparse-switch
        0x0 -> :sswitch_0
        0xa -> :sswitch_1
        0x12 -> :sswitch_2
        0x19 -> :sswitch_3
        0x20 -> :sswitch_4
        0x2a -> :sswitch_5
        0x30 -> :sswitch_6
        0x3a -> :sswitch_7
    .end sparse-switch
.end method

.method public bridge synthetic mergeFrom(Lcom/google/protobuf/micro/CodedInputStreamMicro;)Lcom/google/protobuf/micro/MessageMicro;
    .locals 1
    .param p1    # Lcom/google/protobuf/micro/CodedInputStreamMicro;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    invoke-virtual {p0, p1}, Lcom/google/android/finsky/protos/Containers$ContainerMetadata;->mergeFrom(Lcom/google/protobuf/micro/CodedInputStreamMicro;)Lcom/google/android/finsky/protos/Containers$ContainerMetadata;

    move-result-object v0

    return-object v0
.end method

.method public setAnalyticsCookie(Ljava/lang/String;)Lcom/google/android/finsky/protos/Containers$ContainerMetadata;
    .locals 1
    .param p1    # Ljava/lang/String;

    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/google/android/finsky/protos/Containers$ContainerMetadata;->hasAnalyticsCookie:Z

    iput-object p1, p0, Lcom/google/android/finsky/protos/Containers$ContainerMetadata;->analyticsCookie_:Ljava/lang/String;

    return-object p0
.end method

.method public setBrowseUrl(Ljava/lang/String;)Lcom/google/android/finsky/protos/Containers$ContainerMetadata;
    .locals 1
    .param p1    # Ljava/lang/String;

    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/google/android/finsky/protos/Containers$ContainerMetadata;->hasBrowseUrl:Z

    iput-object p1, p0, Lcom/google/android/finsky/protos/Containers$ContainerMetadata;->browseUrl_:Ljava/lang/String;

    return-object p0
.end method

.method public setEstimatedResults(J)Lcom/google/android/finsky/protos/Containers$ContainerMetadata;
    .locals 1
    .param p1    # J

    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/google/android/finsky/protos/Containers$ContainerMetadata;->hasEstimatedResults:Z

    iput-wide p1, p0, Lcom/google/android/finsky/protos/Containers$ContainerMetadata;->estimatedResults_:J

    return-object p0
.end method

.method public setNextPageUrl(Ljava/lang/String;)Lcom/google/android/finsky/protos/Containers$ContainerMetadata;
    .locals 1
    .param p1    # Ljava/lang/String;

    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/google/android/finsky/protos/Containers$ContainerMetadata;->hasNextPageUrl:Z

    iput-object p1, p0, Lcom/google/android/finsky/protos/Containers$ContainerMetadata;->nextPageUrl_:Ljava/lang/String;

    return-object p0
.end method

.method public setOrdered(Z)Lcom/google/android/finsky/protos/Containers$ContainerMetadata;
    .locals 1
    .param p1    # Z

    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/google/android/finsky/protos/Containers$ContainerMetadata;->hasOrdered:Z

    iput-boolean p1, p0, Lcom/google/android/finsky/protos/Containers$ContainerMetadata;->ordered_:Z

    return-object p0
.end method

.method public setRelevance(D)Lcom/google/android/finsky/protos/Containers$ContainerMetadata;
    .locals 1
    .param p1    # D

    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/google/android/finsky/protos/Containers$ContainerMetadata;->hasRelevance:Z

    iput-wide p1, p0, Lcom/google/android/finsky/protos/Containers$ContainerMetadata;->relevance_:D

    return-object p0
.end method

.method public writeTo(Lcom/google/protobuf/micro/CodedOutputStreamMicro;)V
    .locals 5
    .param p1    # Lcom/google/protobuf/micro/CodedOutputStreamMicro;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    invoke-virtual {p0}, Lcom/google/android/finsky/protos/Containers$ContainerMetadata;->hasBrowseUrl()Z

    move-result v2

    if-eqz v2, :cond_0

    const/4 v2, 0x1

    invoke-virtual {p0}, Lcom/google/android/finsky/protos/Containers$ContainerMetadata;->getBrowseUrl()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {p1, v2, v3}, Lcom/google/protobuf/micro/CodedOutputStreamMicro;->writeString(ILjava/lang/String;)V

    :cond_0
    invoke-virtual {p0}, Lcom/google/android/finsky/protos/Containers$ContainerMetadata;->hasNextPageUrl()Z

    move-result v2

    if-eqz v2, :cond_1

    const/4 v2, 0x2

    invoke-virtual {p0}, Lcom/google/android/finsky/protos/Containers$ContainerMetadata;->getNextPageUrl()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {p1, v2, v3}, Lcom/google/protobuf/micro/CodedOutputStreamMicro;->writeString(ILjava/lang/String;)V

    :cond_1
    invoke-virtual {p0}, Lcom/google/android/finsky/protos/Containers$ContainerMetadata;->hasRelevance()Z

    move-result v2

    if-eqz v2, :cond_2

    const/4 v2, 0x3

    invoke-virtual {p0}, Lcom/google/android/finsky/protos/Containers$ContainerMetadata;->getRelevance()D

    move-result-wide v3

    invoke-virtual {p1, v2, v3, v4}, Lcom/google/protobuf/micro/CodedOutputStreamMicro;->writeDouble(ID)V

    :cond_2
    invoke-virtual {p0}, Lcom/google/android/finsky/protos/Containers$ContainerMetadata;->hasEstimatedResults()Z

    move-result v2

    if-eqz v2, :cond_3

    const/4 v2, 0x4

    invoke-virtual {p0}, Lcom/google/android/finsky/protos/Containers$ContainerMetadata;->getEstimatedResults()J

    move-result-wide v3

    invoke-virtual {p1, v2, v3, v4}, Lcom/google/protobuf/micro/CodedOutputStreamMicro;->writeInt64(IJ)V

    :cond_3
    invoke-virtual {p0}, Lcom/google/android/finsky/protos/Containers$ContainerMetadata;->hasAnalyticsCookie()Z

    move-result v2

    if-eqz v2, :cond_4

    const/4 v2, 0x5

    invoke-virtual {p0}, Lcom/google/android/finsky/protos/Containers$ContainerMetadata;->getAnalyticsCookie()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {p1, v2, v3}, Lcom/google/protobuf/micro/CodedOutputStreamMicro;->writeString(ILjava/lang/String;)V

    :cond_4
    invoke-virtual {p0}, Lcom/google/android/finsky/protos/Containers$ContainerMetadata;->hasOrdered()Z

    move-result v2

    if-eqz v2, :cond_5

    const/4 v2, 0x6

    invoke-virtual {p0}, Lcom/google/android/finsky/protos/Containers$ContainerMetadata;->getOrdered()Z

    move-result v3

    invoke-virtual {p1, v2, v3}, Lcom/google/protobuf/micro/CodedOutputStreamMicro;->writeBool(IZ)V

    :cond_5
    invoke-virtual {p0}, Lcom/google/android/finsky/protos/Containers$ContainerMetadata;->getContainerViewList()Ljava/util/List;

    move-result-object v2

    invoke-interface {v2}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :goto_0
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v2

    if-eqz v2, :cond_6

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/finsky/protos/Containers$ContainerView;

    const/4 v2, 0x7

    invoke-virtual {p1, v2, v0}, Lcom/google/protobuf/micro/CodedOutputStreamMicro;->writeMessage(ILcom/google/protobuf/micro/MessageMicro;)V

    goto :goto_0

    :cond_6
    return-void
.end method
