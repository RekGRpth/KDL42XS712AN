.class public final Lcom/google/android/finsky/protos/CommonDevice$CarrierBillingCredentials;
.super Lcom/google/protobuf/micro/MessageMicro;
.source "CommonDevice.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/google/android/finsky/protos/CommonDevice;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x19
    name = "CarrierBillingCredentials"
.end annotation


# instance fields
.field private cachedSize:I

.field private expiration_:J

.field private hasExpiration:Z

.field private hasValue:Z

.field private value_:Ljava/lang/String;


# direct methods
.method public constructor <init>()V
    .locals 2

    invoke-direct {p0}, Lcom/google/protobuf/micro/MessageMicro;-><init>()V

    const-string v0, ""

    iput-object v0, p0, Lcom/google/android/finsky/protos/CommonDevice$CarrierBillingCredentials;->value_:Ljava/lang/String;

    const-wide/16 v0, 0x0

    iput-wide v0, p0, Lcom/google/android/finsky/protos/CommonDevice$CarrierBillingCredentials;->expiration_:J

    const/4 v0, -0x1

    iput v0, p0, Lcom/google/android/finsky/protos/CommonDevice$CarrierBillingCredentials;->cachedSize:I

    return-void
.end method


# virtual methods
.method public getCachedSize()I
    .locals 1

    iget v0, p0, Lcom/google/android/finsky/protos/CommonDevice$CarrierBillingCredentials;->cachedSize:I

    if-gez v0, :cond_0

    invoke-virtual {p0}, Lcom/google/android/finsky/protos/CommonDevice$CarrierBillingCredentials;->getSerializedSize()I

    :cond_0
    iget v0, p0, Lcom/google/android/finsky/protos/CommonDevice$CarrierBillingCredentials;->cachedSize:I

    return v0
.end method

.method public getExpiration()J
    .locals 2

    iget-wide v0, p0, Lcom/google/android/finsky/protos/CommonDevice$CarrierBillingCredentials;->expiration_:J

    return-wide v0
.end method

.method public getSerializedSize()I
    .locals 4

    const/4 v0, 0x0

    invoke-virtual {p0}, Lcom/google/android/finsky/protos/CommonDevice$CarrierBillingCredentials;->hasValue()Z

    move-result v1

    if-eqz v1, :cond_0

    const/4 v1, 0x1

    invoke-virtual {p0}, Lcom/google/android/finsky/protos/CommonDevice$CarrierBillingCredentials;->getValue()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2}, Lcom/google/protobuf/micro/CodedOutputStreamMicro;->computeStringSize(ILjava/lang/String;)I

    move-result v1

    add-int/2addr v0, v1

    :cond_0
    invoke-virtual {p0}, Lcom/google/android/finsky/protos/CommonDevice$CarrierBillingCredentials;->hasExpiration()Z

    move-result v1

    if-eqz v1, :cond_1

    const/4 v1, 0x2

    invoke-virtual {p0}, Lcom/google/android/finsky/protos/CommonDevice$CarrierBillingCredentials;->getExpiration()J

    move-result-wide v2

    invoke-static {v1, v2, v3}, Lcom/google/protobuf/micro/CodedOutputStreamMicro;->computeInt64Size(IJ)I

    move-result v1

    add-int/2addr v0, v1

    :cond_1
    iput v0, p0, Lcom/google/android/finsky/protos/CommonDevice$CarrierBillingCredentials;->cachedSize:I

    return v0
.end method

.method public getValue()Ljava/lang/String;
    .locals 1

    iget-object v0, p0, Lcom/google/android/finsky/protos/CommonDevice$CarrierBillingCredentials;->value_:Ljava/lang/String;

    return-object v0
.end method

.method public hasExpiration()Z
    .locals 1

    iget-boolean v0, p0, Lcom/google/android/finsky/protos/CommonDevice$CarrierBillingCredentials;->hasExpiration:Z

    return v0
.end method

.method public hasValue()Z
    .locals 1

    iget-boolean v0, p0, Lcom/google/android/finsky/protos/CommonDevice$CarrierBillingCredentials;->hasValue:Z

    return v0
.end method

.method public mergeFrom(Lcom/google/protobuf/micro/CodedInputStreamMicro;)Lcom/google/android/finsky/protos/CommonDevice$CarrierBillingCredentials;
    .locals 3
    .param p1    # Lcom/google/protobuf/micro/CodedInputStreamMicro;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    :cond_0
    :goto_0
    invoke-virtual {p1}, Lcom/google/protobuf/micro/CodedInputStreamMicro;->readTag()I

    move-result v0

    sparse-switch v0, :sswitch_data_0

    invoke-virtual {p0, p1, v0}, Lcom/google/android/finsky/protos/CommonDevice$CarrierBillingCredentials;->parseUnknownField(Lcom/google/protobuf/micro/CodedInputStreamMicro;I)Z

    move-result v1

    if-nez v1, :cond_0

    :sswitch_0
    return-object p0

    :sswitch_1
    invoke-virtual {p1}, Lcom/google/protobuf/micro/CodedInputStreamMicro;->readString()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {p0, v1}, Lcom/google/android/finsky/protos/CommonDevice$CarrierBillingCredentials;->setValue(Ljava/lang/String;)Lcom/google/android/finsky/protos/CommonDevice$CarrierBillingCredentials;

    goto :goto_0

    :sswitch_2
    invoke-virtual {p1}, Lcom/google/protobuf/micro/CodedInputStreamMicro;->readInt64()J

    move-result-wide v1

    invoke-virtual {p0, v1, v2}, Lcom/google/android/finsky/protos/CommonDevice$CarrierBillingCredentials;->setExpiration(J)Lcom/google/android/finsky/protos/CommonDevice$CarrierBillingCredentials;

    goto :goto_0

    :sswitch_data_0
    .sparse-switch
        0x0 -> :sswitch_0
        0xa -> :sswitch_1
        0x10 -> :sswitch_2
    .end sparse-switch
.end method

.method public bridge synthetic mergeFrom(Lcom/google/protobuf/micro/CodedInputStreamMicro;)Lcom/google/protobuf/micro/MessageMicro;
    .locals 1
    .param p1    # Lcom/google/protobuf/micro/CodedInputStreamMicro;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    invoke-virtual {p0, p1}, Lcom/google/android/finsky/protos/CommonDevice$CarrierBillingCredentials;->mergeFrom(Lcom/google/protobuf/micro/CodedInputStreamMicro;)Lcom/google/android/finsky/protos/CommonDevice$CarrierBillingCredentials;

    move-result-object v0

    return-object v0
.end method

.method public setExpiration(J)Lcom/google/android/finsky/protos/CommonDevice$CarrierBillingCredentials;
    .locals 1
    .param p1    # J

    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/google/android/finsky/protos/CommonDevice$CarrierBillingCredentials;->hasExpiration:Z

    iput-wide p1, p0, Lcom/google/android/finsky/protos/CommonDevice$CarrierBillingCredentials;->expiration_:J

    return-object p0
.end method

.method public setValue(Ljava/lang/String;)Lcom/google/android/finsky/protos/CommonDevice$CarrierBillingCredentials;
    .locals 1
    .param p1    # Ljava/lang/String;

    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/google/android/finsky/protos/CommonDevice$CarrierBillingCredentials;->hasValue:Z

    iput-object p1, p0, Lcom/google/android/finsky/protos/CommonDevice$CarrierBillingCredentials;->value_:Ljava/lang/String;

    return-object p0
.end method

.method public writeTo(Lcom/google/protobuf/micro/CodedOutputStreamMicro;)V
    .locals 3
    .param p1    # Lcom/google/protobuf/micro/CodedOutputStreamMicro;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    invoke-virtual {p0}, Lcom/google/android/finsky/protos/CommonDevice$CarrierBillingCredentials;->hasValue()Z

    move-result v0

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    invoke-virtual {p0}, Lcom/google/android/finsky/protos/CommonDevice$CarrierBillingCredentials;->getValue()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {p1, v0, v1}, Lcom/google/protobuf/micro/CodedOutputStreamMicro;->writeString(ILjava/lang/String;)V

    :cond_0
    invoke-virtual {p0}, Lcom/google/android/finsky/protos/CommonDevice$CarrierBillingCredentials;->hasExpiration()Z

    move-result v0

    if-eqz v0, :cond_1

    const/4 v0, 0x2

    invoke-virtual {p0}, Lcom/google/android/finsky/protos/CommonDevice$CarrierBillingCredentials;->getExpiration()J

    move-result-wide v1

    invoke-virtual {p1, v0, v1, v2}, Lcom/google/protobuf/micro/CodedOutputStreamMicro;->writeInt64(IJ)V

    :cond_1
    return-void
.end method
