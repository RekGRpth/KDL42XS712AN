.class public final Lcom/google/android/finsky/protos/Purchase$PreparePurchaseResponse;
.super Lcom/google/protobuf/micro/MessageMicro;
.source "Purchase.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/google/android/finsky/protos/Purchase;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x19
    name = "PreparePurchaseResponse"
.end annotation


# instance fields
.field private cachedSize:I

.field private cart_:Lcom/google/android/finsky/protos/Purchase$ClientCart;

.field private challenge_:Lcom/google/android/finsky/protos/ChallengeProtos$Challenge;

.field private hasCart:Z

.field private hasChallenge:Z

.field private hasPurchaseStatus:Z

.field private hasServerLogsCookie:Z

.field private libraryUpdate_:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Lcom/google/android/finsky/protos/Library$LibraryUpdate;",
            ">;"
        }
    .end annotation
.end field

.field private purchaseStatus_:Lcom/google/android/finsky/protos/Purchase$PurchaseStatus;

.field private serverLogsCookie_:Lcom/google/protobuf/micro/ByteStringMicro;


# direct methods
.method public constructor <init>()V
    .locals 1

    const/4 v0, 0x0

    invoke-direct {p0}, Lcom/google/protobuf/micro/MessageMicro;-><init>()V

    iput-object v0, p0, Lcom/google/android/finsky/protos/Purchase$PreparePurchaseResponse;->purchaseStatus_:Lcom/google/android/finsky/protos/Purchase$PurchaseStatus;

    iput-object v0, p0, Lcom/google/android/finsky/protos/Purchase$PreparePurchaseResponse;->challenge_:Lcom/google/android/finsky/protos/ChallengeProtos$Challenge;

    iput-object v0, p0, Lcom/google/android/finsky/protos/Purchase$PreparePurchaseResponse;->cart_:Lcom/google/android/finsky/protos/Purchase$ClientCart;

    invoke-static {}, Ljava/util/Collections;->emptyList()Ljava/util/List;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/finsky/protos/Purchase$PreparePurchaseResponse;->libraryUpdate_:Ljava/util/List;

    sget-object v0, Lcom/google/protobuf/micro/ByteStringMicro;->EMPTY:Lcom/google/protobuf/micro/ByteStringMicro;

    iput-object v0, p0, Lcom/google/android/finsky/protos/Purchase$PreparePurchaseResponse;->serverLogsCookie_:Lcom/google/protobuf/micro/ByteStringMicro;

    const/4 v0, -0x1

    iput v0, p0, Lcom/google/android/finsky/protos/Purchase$PreparePurchaseResponse;->cachedSize:I

    return-void
.end method


# virtual methods
.method public addLibraryUpdate(Lcom/google/android/finsky/protos/Library$LibraryUpdate;)Lcom/google/android/finsky/protos/Purchase$PreparePurchaseResponse;
    .locals 1
    .param p1    # Lcom/google/android/finsky/protos/Library$LibraryUpdate;

    if-nez p1, :cond_0

    new-instance v0, Ljava/lang/NullPointerException;

    invoke-direct {v0}, Ljava/lang/NullPointerException;-><init>()V

    throw v0

    :cond_0
    iget-object v0, p0, Lcom/google/android/finsky/protos/Purchase$PreparePurchaseResponse;->libraryUpdate_:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->isEmpty()Z

    move-result v0

    if-eqz v0, :cond_1

    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, Lcom/google/android/finsky/protos/Purchase$PreparePurchaseResponse;->libraryUpdate_:Ljava/util/List;

    :cond_1
    iget-object v0, p0, Lcom/google/android/finsky/protos/Purchase$PreparePurchaseResponse;->libraryUpdate_:Ljava/util/List;

    invoke-interface {v0, p1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    return-object p0
.end method

.method public getCachedSize()I
    .locals 1

    iget v0, p0, Lcom/google/android/finsky/protos/Purchase$PreparePurchaseResponse;->cachedSize:I

    if-gez v0, :cond_0

    invoke-virtual {p0}, Lcom/google/android/finsky/protos/Purchase$PreparePurchaseResponse;->getSerializedSize()I

    :cond_0
    iget v0, p0, Lcom/google/android/finsky/protos/Purchase$PreparePurchaseResponse;->cachedSize:I

    return v0
.end method

.method public getCart()Lcom/google/android/finsky/protos/Purchase$ClientCart;
    .locals 1

    iget-object v0, p0, Lcom/google/android/finsky/protos/Purchase$PreparePurchaseResponse;->cart_:Lcom/google/android/finsky/protos/Purchase$ClientCart;

    return-object v0
.end method

.method public getChallenge()Lcom/google/android/finsky/protos/ChallengeProtos$Challenge;
    .locals 1

    iget-object v0, p0, Lcom/google/android/finsky/protos/Purchase$PreparePurchaseResponse;->challenge_:Lcom/google/android/finsky/protos/ChallengeProtos$Challenge;

    return-object v0
.end method

.method public getLibraryUpdateList()Ljava/util/List;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/List",
            "<",
            "Lcom/google/android/finsky/protos/Library$LibraryUpdate;",
            ">;"
        }
    .end annotation

    iget-object v0, p0, Lcom/google/android/finsky/protos/Purchase$PreparePurchaseResponse;->libraryUpdate_:Ljava/util/List;

    return-object v0
.end method

.method public getPurchaseStatus()Lcom/google/android/finsky/protos/Purchase$PurchaseStatus;
    .locals 1

    iget-object v0, p0, Lcom/google/android/finsky/protos/Purchase$PreparePurchaseResponse;->purchaseStatus_:Lcom/google/android/finsky/protos/Purchase$PurchaseStatus;

    return-object v0
.end method

.method public getSerializedSize()I
    .locals 5

    const/4 v2, 0x0

    invoke-virtual {p0}, Lcom/google/android/finsky/protos/Purchase$PreparePurchaseResponse;->hasPurchaseStatus()Z

    move-result v3

    if-eqz v3, :cond_0

    const/4 v3, 0x1

    invoke-virtual {p0}, Lcom/google/android/finsky/protos/Purchase$PreparePurchaseResponse;->getPurchaseStatus()Lcom/google/android/finsky/protos/Purchase$PurchaseStatus;

    move-result-object v4

    invoke-static {v3, v4}, Lcom/google/protobuf/micro/CodedOutputStreamMicro;->computeMessageSize(ILcom/google/protobuf/micro/MessageMicro;)I

    move-result v3

    add-int/2addr v2, v3

    :cond_0
    invoke-virtual {p0}, Lcom/google/android/finsky/protos/Purchase$PreparePurchaseResponse;->hasChallenge()Z

    move-result v3

    if-eqz v3, :cond_1

    const/4 v3, 0x2

    invoke-virtual {p0}, Lcom/google/android/finsky/protos/Purchase$PreparePurchaseResponse;->getChallenge()Lcom/google/android/finsky/protos/ChallengeProtos$Challenge;

    move-result-object v4

    invoke-static {v3, v4}, Lcom/google/protobuf/micro/CodedOutputStreamMicro;->computeMessageSize(ILcom/google/protobuf/micro/MessageMicro;)I

    move-result v3

    add-int/2addr v2, v3

    :cond_1
    invoke-virtual {p0}, Lcom/google/android/finsky/protos/Purchase$PreparePurchaseResponse;->hasCart()Z

    move-result v3

    if-eqz v3, :cond_2

    const/4 v3, 0x3

    invoke-virtual {p0}, Lcom/google/android/finsky/protos/Purchase$PreparePurchaseResponse;->getCart()Lcom/google/android/finsky/protos/Purchase$ClientCart;

    move-result-object v4

    invoke-static {v3, v4}, Lcom/google/protobuf/micro/CodedOutputStreamMicro;->computeMessageSize(ILcom/google/protobuf/micro/MessageMicro;)I

    move-result v3

    add-int/2addr v2, v3

    :cond_2
    invoke-virtual {p0}, Lcom/google/android/finsky/protos/Purchase$PreparePurchaseResponse;->getLibraryUpdateList()Ljava/util/List;

    move-result-object v3

    invoke-interface {v3}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :goto_0
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v3

    if-eqz v3, :cond_3

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/finsky/protos/Library$LibraryUpdate;

    const/4 v3, 0x4

    invoke-static {v3, v0}, Lcom/google/protobuf/micro/CodedOutputStreamMicro;->computeMessageSize(ILcom/google/protobuf/micro/MessageMicro;)I

    move-result v3

    add-int/2addr v2, v3

    goto :goto_0

    :cond_3
    invoke-virtual {p0}, Lcom/google/android/finsky/protos/Purchase$PreparePurchaseResponse;->hasServerLogsCookie()Z

    move-result v3

    if-eqz v3, :cond_4

    const/4 v3, 0x5

    invoke-virtual {p0}, Lcom/google/android/finsky/protos/Purchase$PreparePurchaseResponse;->getServerLogsCookie()Lcom/google/protobuf/micro/ByteStringMicro;

    move-result-object v4

    invoke-static {v3, v4}, Lcom/google/protobuf/micro/CodedOutputStreamMicro;->computeBytesSize(ILcom/google/protobuf/micro/ByteStringMicro;)I

    move-result v3

    add-int/2addr v2, v3

    :cond_4
    iput v2, p0, Lcom/google/android/finsky/protos/Purchase$PreparePurchaseResponse;->cachedSize:I

    return v2
.end method

.method public getServerLogsCookie()Lcom/google/protobuf/micro/ByteStringMicro;
    .locals 1

    iget-object v0, p0, Lcom/google/android/finsky/protos/Purchase$PreparePurchaseResponse;->serverLogsCookie_:Lcom/google/protobuf/micro/ByteStringMicro;

    return-object v0
.end method

.method public hasCart()Z
    .locals 1

    iget-boolean v0, p0, Lcom/google/android/finsky/protos/Purchase$PreparePurchaseResponse;->hasCart:Z

    return v0
.end method

.method public hasChallenge()Z
    .locals 1

    iget-boolean v0, p0, Lcom/google/android/finsky/protos/Purchase$PreparePurchaseResponse;->hasChallenge:Z

    return v0
.end method

.method public hasPurchaseStatus()Z
    .locals 1

    iget-boolean v0, p0, Lcom/google/android/finsky/protos/Purchase$PreparePurchaseResponse;->hasPurchaseStatus:Z

    return v0
.end method

.method public hasServerLogsCookie()Z
    .locals 1

    iget-boolean v0, p0, Lcom/google/android/finsky/protos/Purchase$PreparePurchaseResponse;->hasServerLogsCookie:Z

    return v0
.end method

.method public mergeFrom(Lcom/google/protobuf/micro/CodedInputStreamMicro;)Lcom/google/android/finsky/protos/Purchase$PreparePurchaseResponse;
    .locals 3
    .param p1    # Lcom/google/protobuf/micro/CodedInputStreamMicro;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    :cond_0
    :goto_0
    invoke-virtual {p1}, Lcom/google/protobuf/micro/CodedInputStreamMicro;->readTag()I

    move-result v0

    sparse-switch v0, :sswitch_data_0

    invoke-virtual {p0, p1, v0}, Lcom/google/android/finsky/protos/Purchase$PreparePurchaseResponse;->parseUnknownField(Lcom/google/protobuf/micro/CodedInputStreamMicro;I)Z

    move-result v2

    if-nez v2, :cond_0

    :sswitch_0
    return-object p0

    :sswitch_1
    new-instance v1, Lcom/google/android/finsky/protos/Purchase$PurchaseStatus;

    invoke-direct {v1}, Lcom/google/android/finsky/protos/Purchase$PurchaseStatus;-><init>()V

    invoke-virtual {p1, v1}, Lcom/google/protobuf/micro/CodedInputStreamMicro;->readMessage(Lcom/google/protobuf/micro/MessageMicro;)V

    invoke-virtual {p0, v1}, Lcom/google/android/finsky/protos/Purchase$PreparePurchaseResponse;->setPurchaseStatus(Lcom/google/android/finsky/protos/Purchase$PurchaseStatus;)Lcom/google/android/finsky/protos/Purchase$PreparePurchaseResponse;

    goto :goto_0

    :sswitch_2
    new-instance v1, Lcom/google/android/finsky/protos/ChallengeProtos$Challenge;

    invoke-direct {v1}, Lcom/google/android/finsky/protos/ChallengeProtos$Challenge;-><init>()V

    invoke-virtual {p1, v1}, Lcom/google/protobuf/micro/CodedInputStreamMicro;->readMessage(Lcom/google/protobuf/micro/MessageMicro;)V

    invoke-virtual {p0, v1}, Lcom/google/android/finsky/protos/Purchase$PreparePurchaseResponse;->setChallenge(Lcom/google/android/finsky/protos/ChallengeProtos$Challenge;)Lcom/google/android/finsky/protos/Purchase$PreparePurchaseResponse;

    goto :goto_0

    :sswitch_3
    new-instance v1, Lcom/google/android/finsky/protos/Purchase$ClientCart;

    invoke-direct {v1}, Lcom/google/android/finsky/protos/Purchase$ClientCart;-><init>()V

    invoke-virtual {p1, v1}, Lcom/google/protobuf/micro/CodedInputStreamMicro;->readMessage(Lcom/google/protobuf/micro/MessageMicro;)V

    invoke-virtual {p0, v1}, Lcom/google/android/finsky/protos/Purchase$PreparePurchaseResponse;->setCart(Lcom/google/android/finsky/protos/Purchase$ClientCart;)Lcom/google/android/finsky/protos/Purchase$PreparePurchaseResponse;

    goto :goto_0

    :sswitch_4
    new-instance v1, Lcom/google/android/finsky/protos/Library$LibraryUpdate;

    invoke-direct {v1}, Lcom/google/android/finsky/protos/Library$LibraryUpdate;-><init>()V

    invoke-virtual {p1, v1}, Lcom/google/protobuf/micro/CodedInputStreamMicro;->readMessage(Lcom/google/protobuf/micro/MessageMicro;)V

    invoke-virtual {p0, v1}, Lcom/google/android/finsky/protos/Purchase$PreparePurchaseResponse;->addLibraryUpdate(Lcom/google/android/finsky/protos/Library$LibraryUpdate;)Lcom/google/android/finsky/protos/Purchase$PreparePurchaseResponse;

    goto :goto_0

    :sswitch_5
    invoke-virtual {p1}, Lcom/google/protobuf/micro/CodedInputStreamMicro;->readBytes()Lcom/google/protobuf/micro/ByteStringMicro;

    move-result-object v2

    invoke-virtual {p0, v2}, Lcom/google/android/finsky/protos/Purchase$PreparePurchaseResponse;->setServerLogsCookie(Lcom/google/protobuf/micro/ByteStringMicro;)Lcom/google/android/finsky/protos/Purchase$PreparePurchaseResponse;

    goto :goto_0

    :sswitch_data_0
    .sparse-switch
        0x0 -> :sswitch_0
        0xa -> :sswitch_1
        0x12 -> :sswitch_2
        0x1a -> :sswitch_3
        0x22 -> :sswitch_4
        0x2a -> :sswitch_5
    .end sparse-switch
.end method

.method public bridge synthetic mergeFrom(Lcom/google/protobuf/micro/CodedInputStreamMicro;)Lcom/google/protobuf/micro/MessageMicro;
    .locals 1
    .param p1    # Lcom/google/protobuf/micro/CodedInputStreamMicro;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    invoke-virtual {p0, p1}, Lcom/google/android/finsky/protos/Purchase$PreparePurchaseResponse;->mergeFrom(Lcom/google/protobuf/micro/CodedInputStreamMicro;)Lcom/google/android/finsky/protos/Purchase$PreparePurchaseResponse;

    move-result-object v0

    return-object v0
.end method

.method public setCart(Lcom/google/android/finsky/protos/Purchase$ClientCart;)Lcom/google/android/finsky/protos/Purchase$PreparePurchaseResponse;
    .locals 1
    .param p1    # Lcom/google/android/finsky/protos/Purchase$ClientCart;

    if-nez p1, :cond_0

    new-instance v0, Ljava/lang/NullPointerException;

    invoke-direct {v0}, Ljava/lang/NullPointerException;-><init>()V

    throw v0

    :cond_0
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/google/android/finsky/protos/Purchase$PreparePurchaseResponse;->hasCart:Z

    iput-object p1, p0, Lcom/google/android/finsky/protos/Purchase$PreparePurchaseResponse;->cart_:Lcom/google/android/finsky/protos/Purchase$ClientCart;

    return-object p0
.end method

.method public setChallenge(Lcom/google/android/finsky/protos/ChallengeProtos$Challenge;)Lcom/google/android/finsky/protos/Purchase$PreparePurchaseResponse;
    .locals 1
    .param p1    # Lcom/google/android/finsky/protos/ChallengeProtos$Challenge;

    if-nez p1, :cond_0

    new-instance v0, Ljava/lang/NullPointerException;

    invoke-direct {v0}, Ljava/lang/NullPointerException;-><init>()V

    throw v0

    :cond_0
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/google/android/finsky/protos/Purchase$PreparePurchaseResponse;->hasChallenge:Z

    iput-object p1, p0, Lcom/google/android/finsky/protos/Purchase$PreparePurchaseResponse;->challenge_:Lcom/google/android/finsky/protos/ChallengeProtos$Challenge;

    return-object p0
.end method

.method public setPurchaseStatus(Lcom/google/android/finsky/protos/Purchase$PurchaseStatus;)Lcom/google/android/finsky/protos/Purchase$PreparePurchaseResponse;
    .locals 1
    .param p1    # Lcom/google/android/finsky/protos/Purchase$PurchaseStatus;

    if-nez p1, :cond_0

    new-instance v0, Ljava/lang/NullPointerException;

    invoke-direct {v0}, Ljava/lang/NullPointerException;-><init>()V

    throw v0

    :cond_0
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/google/android/finsky/protos/Purchase$PreparePurchaseResponse;->hasPurchaseStatus:Z

    iput-object p1, p0, Lcom/google/android/finsky/protos/Purchase$PreparePurchaseResponse;->purchaseStatus_:Lcom/google/android/finsky/protos/Purchase$PurchaseStatus;

    return-object p0
.end method

.method public setServerLogsCookie(Lcom/google/protobuf/micro/ByteStringMicro;)Lcom/google/android/finsky/protos/Purchase$PreparePurchaseResponse;
    .locals 1
    .param p1    # Lcom/google/protobuf/micro/ByteStringMicro;

    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/google/android/finsky/protos/Purchase$PreparePurchaseResponse;->hasServerLogsCookie:Z

    iput-object p1, p0, Lcom/google/android/finsky/protos/Purchase$PreparePurchaseResponse;->serverLogsCookie_:Lcom/google/protobuf/micro/ByteStringMicro;

    return-object p0
.end method

.method public writeTo(Lcom/google/protobuf/micro/CodedOutputStreamMicro;)V
    .locals 4
    .param p1    # Lcom/google/protobuf/micro/CodedOutputStreamMicro;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    invoke-virtual {p0}, Lcom/google/android/finsky/protos/Purchase$PreparePurchaseResponse;->hasPurchaseStatus()Z

    move-result v2

    if-eqz v2, :cond_0

    const/4 v2, 0x1

    invoke-virtual {p0}, Lcom/google/android/finsky/protos/Purchase$PreparePurchaseResponse;->getPurchaseStatus()Lcom/google/android/finsky/protos/Purchase$PurchaseStatus;

    move-result-object v3

    invoke-virtual {p1, v2, v3}, Lcom/google/protobuf/micro/CodedOutputStreamMicro;->writeMessage(ILcom/google/protobuf/micro/MessageMicro;)V

    :cond_0
    invoke-virtual {p0}, Lcom/google/android/finsky/protos/Purchase$PreparePurchaseResponse;->hasChallenge()Z

    move-result v2

    if-eqz v2, :cond_1

    const/4 v2, 0x2

    invoke-virtual {p0}, Lcom/google/android/finsky/protos/Purchase$PreparePurchaseResponse;->getChallenge()Lcom/google/android/finsky/protos/ChallengeProtos$Challenge;

    move-result-object v3

    invoke-virtual {p1, v2, v3}, Lcom/google/protobuf/micro/CodedOutputStreamMicro;->writeMessage(ILcom/google/protobuf/micro/MessageMicro;)V

    :cond_1
    invoke-virtual {p0}, Lcom/google/android/finsky/protos/Purchase$PreparePurchaseResponse;->hasCart()Z

    move-result v2

    if-eqz v2, :cond_2

    const/4 v2, 0x3

    invoke-virtual {p0}, Lcom/google/android/finsky/protos/Purchase$PreparePurchaseResponse;->getCart()Lcom/google/android/finsky/protos/Purchase$ClientCart;

    move-result-object v3

    invoke-virtual {p1, v2, v3}, Lcom/google/protobuf/micro/CodedOutputStreamMicro;->writeMessage(ILcom/google/protobuf/micro/MessageMicro;)V

    :cond_2
    invoke-virtual {p0}, Lcom/google/android/finsky/protos/Purchase$PreparePurchaseResponse;->getLibraryUpdateList()Ljava/util/List;

    move-result-object v2

    invoke-interface {v2}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :goto_0
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v2

    if-eqz v2, :cond_3

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/finsky/protos/Library$LibraryUpdate;

    const/4 v2, 0x4

    invoke-virtual {p1, v2, v0}, Lcom/google/protobuf/micro/CodedOutputStreamMicro;->writeMessage(ILcom/google/protobuf/micro/MessageMicro;)V

    goto :goto_0

    :cond_3
    invoke-virtual {p0}, Lcom/google/android/finsky/protos/Purchase$PreparePurchaseResponse;->hasServerLogsCookie()Z

    move-result v2

    if-eqz v2, :cond_4

    const/4 v2, 0x5

    invoke-virtual {p0}, Lcom/google/android/finsky/protos/Purchase$PreparePurchaseResponse;->getServerLogsCookie()Lcom/google/protobuf/micro/ByteStringMicro;

    move-result-object v3

    invoke-virtual {p1, v2, v3}, Lcom/google/protobuf/micro/CodedOutputStreamMicro;->writeBytes(ILcom/google/protobuf/micro/ByteStringMicro;)V

    :cond_4
    return-void
.end method
