.class Lcom/google/android/finsky/utils/ThumbnailUtils$CustomBoundsTransitionDrawable;
.super Landroid/graphics/drawable/TransitionDrawable;
.source "ThumbnailUtils.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/google/android/finsky/utils/ThumbnailUtils;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0xa
    name = "CustomBoundsTransitionDrawable"
.end annotation


# instance fields
.field private final mIntrinsicHeight:I

.field private final mIntrinsicWidth:I


# direct methods
.method public constructor <init>([Landroid/graphics/drawable/Drawable;II)V
    .locals 0
    .param p1    # [Landroid/graphics/drawable/Drawable;
    .param p2    # I
    .param p3    # I

    invoke-direct {p0, p1}, Landroid/graphics/drawable/TransitionDrawable;-><init>([Landroid/graphics/drawable/Drawable;)V

    iput p2, p0, Lcom/google/android/finsky/utils/ThumbnailUtils$CustomBoundsTransitionDrawable;->mIntrinsicWidth:I

    iput p3, p0, Lcom/google/android/finsky/utils/ThumbnailUtils$CustomBoundsTransitionDrawable;->mIntrinsicHeight:I

    return-void
.end method


# virtual methods
.method public getIntrinsicHeight()I
    .locals 1

    iget v0, p0, Lcom/google/android/finsky/utils/ThumbnailUtils$CustomBoundsTransitionDrawable;->mIntrinsicHeight:I

    return v0
.end method

.method public getIntrinsicWidth()I
    .locals 1

    iget v0, p0, Lcom/google/android/finsky/utils/ThumbnailUtils$CustomBoundsTransitionDrawable;->mIntrinsicWidth:I

    return v0
.end method
