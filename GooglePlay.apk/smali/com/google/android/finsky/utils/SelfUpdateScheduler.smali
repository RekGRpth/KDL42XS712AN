.class public Lcom/google/android/finsky/utils/SelfUpdateScheduler;
.super Ljava/lang/Object;
.source "SelfUpdateScheduler.java"

# interfaces
.implements Lcom/google/android/finsky/download/DownloadQueueListener;


# instance fields
.field private final mDownloadQueue:Lcom/google/android/finsky/download/DownloadQueue;

.field private mLogAppData:Lcom/google/android/play/analytics/PlayStore$AppData;

.field private mMarketVersion:I

.field private mOnAppExitDeferrer:Lcom/google/android/finsky/utils/ApplicationDismissedDeferrer;

.field private mUpdateDownload:Lcom/google/android/finsky/download/Download;

.field private mUpdateInProgress:Z


# direct methods
.method public constructor <init>(Lcom/google/android/finsky/download/DownloadQueue;I)V
    .locals 2
    .param p1    # Lcom/google/android/finsky/download/DownloadQueue;
    .param p2    # I

    const/4 v1, 0x0

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/google/android/finsky/utils/SelfUpdateScheduler;->mUpdateInProgress:Z

    iput-object v1, p0, Lcom/google/android/finsky/utils/SelfUpdateScheduler;->mLogAppData:Lcom/google/android/play/analytics/PlayStore$AppData;

    iput-object v1, p0, Lcom/google/android/finsky/utils/SelfUpdateScheduler;->mUpdateDownload:Lcom/google/android/finsky/download/Download;

    iput-object p1, p0, Lcom/google/android/finsky/utils/SelfUpdateScheduler;->mDownloadQueue:Lcom/google/android/finsky/download/DownloadQueue;

    iput p2, p0, Lcom/google/android/finsky/utils/SelfUpdateScheduler;->mMarketVersion:I

    return-void
.end method

.method static synthetic access$000(Lcom/google/android/finsky/utils/SelfUpdateScheduler;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V
    .locals 0
    .param p0    # Lcom/google/android/finsky/utils/SelfUpdateScheduler;
    .param p1    # Ljava/lang/String;
    .param p2    # Ljava/lang/String;
    .param p3    # Ljava/lang/String;

    invoke-direct {p0, p1, p2, p3}, Lcom/google/android/finsky/utils/SelfUpdateScheduler;->startSelfUpdateDownload(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    return-void
.end method

.method static synthetic access$100(Lcom/google/android/finsky/utils/SelfUpdateScheduler;)Lcom/google/android/play/analytics/PlayStore$AppData;
    .locals 1
    .param p0    # Lcom/google/android/finsky/utils/SelfUpdateScheduler;

    iget-object v0, p0, Lcom/google/android/finsky/utils/SelfUpdateScheduler;->mLogAppData:Lcom/google/android/play/analytics/PlayStore$AppData;

    return-object v0
.end method

.method private startSelfUpdateDownload(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V
    .locals 15
    .param p1    # Ljava/lang/String;
    .param p2    # Ljava/lang/String;
    .param p3    # Ljava/lang/String;

    new-instance v0, Lcom/google/android/finsky/download/DownloadImpl;

    const-string v2, ""

    const/4 v3, 0x0

    const/4 v4, 0x0

    const/4 v7, 0x0

    const-wide/16 v8, -0x1

    const-wide/16 v10, -0x1

    const/4 v12, 0x0

    const/4 v13, 0x0

    const/4 v14, 0x1

    move-object/from16 v1, p1

    move-object/from16 v5, p2

    move-object/from16 v6, p3

    invoke-direct/range {v0 .. v14}, Lcom/google/android/finsky/download/DownloadImpl;-><init>(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;ILjava/lang/String;Ljava/lang/String;Landroid/net/Uri;JJLcom/google/android/finsky/download/obb/Obb;ZZ)V

    iput-object v0, p0, Lcom/google/android/finsky/utils/SelfUpdateScheduler;->mUpdateDownload:Lcom/google/android/finsky/download/Download;

    iget-object v1, p0, Lcom/google/android/finsky/utils/SelfUpdateScheduler;->mDownloadQueue:Lcom/google/android/finsky/download/DownloadQueue;

    invoke-interface {v1, p0}, Lcom/google/android/finsky/download/DownloadQueue;->addListener(Lcom/google/android/finsky/download/DownloadQueueListener;)V

    iget-object v1, p0, Lcom/google/android/finsky/utils/SelfUpdateScheduler;->mDownloadQueue:Lcom/google/android/finsky/download/DownloadQueue;

    invoke-interface {v1, v0}, Lcom/google/android/finsky/download/DownloadQueue;->add(Lcom/google/android/finsky/download/InternalDownload;)V

    invoke-static {}, Lcom/google/android/finsky/FinskyApp;->get()Lcom/google/android/finsky/FinskyApp;

    move-result-object v1

    invoke-virtual {v1}, Lcom/google/android/finsky/FinskyApp;->getAnalytics()Lcom/google/android/finsky/analytics/Analytics;

    move-result-object v1

    const-string v2, "install.downloadQueued"

    const-string v3, "self-update-download"

    const/4 v4, 0x0

    invoke-interface {v1, v2, v3, v4}, Lcom/google/android/finsky/analytics/Analytics;->logTagAndPackage(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    invoke-static {}, Lcom/google/android/finsky/FinskyApp;->get()Lcom/google/android/finsky/FinskyApp;

    move-result-object v1

    invoke-virtual {v1}, Lcom/google/android/finsky/FinskyApp;->getEventLogger()Lcom/google/android/finsky/analytics/FinskyEventLog;

    move-result-object v1

    const/16 v2, 0x64

    invoke-static {}, Lcom/google/android/finsky/FinskyApp;->get()Lcom/google/android/finsky/FinskyApp;

    move-result-object v3

    invoke-virtual {v3}, Lcom/google/android/finsky/FinskyApp;->getPackageName()Ljava/lang/String;

    move-result-object v3

    const/4 v4, 0x0

    const/4 v5, 0x0

    const/4 v6, 0x0

    iget-object v7, p0, Lcom/google/android/finsky/utils/SelfUpdateScheduler;->mLogAppData:Lcom/google/android/play/analytics/PlayStore$AppData;

    invoke-virtual/range {v1 .. v7}, Lcom/google/android/finsky/analytics/FinskyEventLog;->logBackgroundEvent(ILjava/lang/String;Ljava/lang/String;ILjava/lang/String;Lcom/google/android/play/analytics/PlayStore$AppData;)V

    return-void
.end method


# virtual methods
.method public checkForSelfUpdate(Lcom/google/android/finsky/protos/Toc$TocResponse;)V
    .locals 14
    .param p1    # Lcom/google/android/finsky/protos/Toc$TocResponse;

    const/4 v4, 0x2

    const/4 v3, 0x0

    const/4 v6, 0x0

    const/4 v2, 0x1

    iget-boolean v0, p0, Lcom/google/android/finsky/utils/SelfUpdateScheduler;->mUpdateInProgress:Z

    if-eqz v0, :cond_0

    const-string v0, "Skipping DFE self-update check as there is an update already queued."

    new-array v2, v6, [Ljava/lang/Object;

    invoke-static {v0, v2}, Lcom/google/android/finsky/utils/FinskyLog;->d(Ljava/lang/String;[Ljava/lang/Object;)V

    :goto_0
    return-void

    :cond_0
    const/4 v13, -0x1

    invoke-virtual {p1}, Lcom/google/android/finsky/protos/Toc$TocResponse;->hasSelfUpdateConfig()Z

    move-result v0

    if-eqz v0, :cond_1

    invoke-virtual {p1}, Lcom/google/android/finsky/protos/Toc$TocResponse;->getSelfUpdateConfig()Lcom/google/android/finsky/protos/Toc$SelfUpdateConfig;

    move-result-object v11

    invoke-virtual {v11}, Lcom/google/android/finsky/protos/Toc$SelfUpdateConfig;->hasLatestClientVersionCode()Z

    move-result v0

    if-eqz v0, :cond_1

    invoke-virtual {v11}, Lcom/google/android/finsky/protos/Toc$SelfUpdateConfig;->getLatestClientVersionCode()I

    move-result v13

    :cond_1
    iget v0, p0, Lcom/google/android/finsky/utils/SelfUpdateScheduler;->mMarketVersion:I

    if-lt v0, v13, :cond_2

    const-string v0, "Skipping DFE self-update. Local Version [%d] >= Server Version [%d]"

    new-array v3, v4, [Ljava/lang/Object;

    iget v4, p0, Lcom/google/android/finsky/utils/SelfUpdateScheduler;->mMarketVersion:I

    invoke-static {v4}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v4

    aput-object v4, v3, v6

    invoke-static {v13}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v4

    aput-object v4, v3, v2

    invoke-static {v0, v3}, Lcom/google/android/finsky/utils/FinskyLog;->d(Ljava/lang/String;[Ljava/lang/Object;)V

    goto :goto_0

    :cond_2
    const-string v0, "Starting DFE self-update from local version [%d] to server version [%d]"

    new-array v4, v4, [Ljava/lang/Object;

    iget v5, p0, Lcom/google/android/finsky/utils/SelfUpdateScheduler;->mMarketVersion:I

    invoke-static {v5}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v5

    aput-object v5, v4, v6

    invoke-static {v13}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v5

    aput-object v5, v4, v2

    invoke-static {v0, v4}, Lcom/google/android/finsky/utils/FinskyLog;->d(Ljava/lang/String;[Ljava/lang/Object;)V

    iput-boolean v2, p0, Lcom/google/android/finsky/utils/SelfUpdateScheduler;->mUpdateInProgress:Z

    new-instance v0, Lcom/google/android/play/analytics/PlayStore$AppData;

    invoke-direct {v0}, Lcom/google/android/play/analytics/PlayStore$AppData;-><init>()V

    iget v4, p0, Lcom/google/android/finsky/utils/SelfUpdateScheduler;->mMarketVersion:I

    invoke-virtual {v0, v4}, Lcom/google/android/play/analytics/PlayStore$AppData;->setOldVersion(I)Lcom/google/android/play/analytics/PlayStore$AppData;

    move-result-object v0

    invoke-virtual {v0, v13}, Lcom/google/android/play/analytics/PlayStore$AppData;->setVersion(I)Lcom/google/android/play/analytics/PlayStore$AppData;

    move-result-object v0

    invoke-virtual {v0, v2}, Lcom/google/android/play/analytics/PlayStore$AppData;->setSystemApp(Z)Lcom/google/android/play/analytics/PlayStore$AppData;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/finsky/utils/SelfUpdateScheduler;->mLogAppData:Lcom/google/android/play/analytics/PlayStore$AppData;

    invoke-static {}, Lcom/google/android/finsky/FinskyApp;->get()Lcom/google/android/finsky/FinskyApp;

    move-result-object v12

    invoke-virtual {v12}, Landroid/content/Context;->getPackageName()Ljava/lang/String;

    move-result-object v1

    invoke-static {v12}, Lcom/google/android/finsky/api/AccountHandler;->getFirstAccount(Landroid/content/Context;)Landroid/accounts/Account;

    move-result-object v10

    invoke-static {v12}, Lcom/google/android/finsky/utils/GetTocHelper;->getSignatureHashHash(Landroid/content/Context;)Ljava/lang/String;

    move-result-object v7

    invoke-static {}, Lcom/google/android/finsky/FinskyApp;->get()Lcom/google/android/finsky/FinskyApp;

    move-result-object v0

    iget-object v4, v10, Landroid/accounts/Account;->name:Ljava/lang/String;

    invoke-virtual {v0, v4}, Lcom/google/android/finsky/FinskyApp;->getDfeApi(Ljava/lang/String;)Lcom/google/android/finsky/api/DfeApi;

    move-result-object v0

    invoke-static {v13}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v4

    new-instance v8, Lcom/google/android/finsky/utils/SelfUpdateScheduler$1;

    invoke-direct {v8, p0}, Lcom/google/android/finsky/utils/SelfUpdateScheduler$1;-><init>(Lcom/google/android/finsky/utils/SelfUpdateScheduler;)V

    new-instance v9, Lcom/google/android/finsky/utils/SelfUpdateScheduler$2;

    invoke-direct {v9, p0}, Lcom/google/android/finsky/utils/SelfUpdateScheduler$2;-><init>(Lcom/google/android/finsky/utils/SelfUpdateScheduler;)V

    move-object v5, v3

    move-object v6, v3

    invoke-interface/range {v0 .. v9}, Lcom/google/android/finsky/api/DfeApi;->delivery(Ljava/lang/String;I[BLjava/lang/Integer;Ljava/lang/Integer;Ljava/lang/String;Ljava/lang/String;Lcom/android/volley/Response$Listener;Lcom/android/volley/Response$ErrorListener;)Lcom/android/volley/Request;

    goto/16 :goto_0
.end method

.method public onCancel(Lcom/google/android/finsky/download/Download;)V
    .locals 0
    .param p1    # Lcom/google/android/finsky/download/Download;

    return-void
.end method

.method public onComplete(Lcom/google/android/finsky/download/Download;)V
    .locals 7
    .param p1    # Lcom/google/android/finsky/download/Download;

    const/4 v3, 0x0

    const/4 v4, 0x0

    iget-object v0, p0, Lcom/google/android/finsky/utils/SelfUpdateScheduler;->mUpdateDownload:Lcom/google/android/finsky/download/Download;

    if-eq p1, v0, :cond_0

    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v1, "Self-update ignoring completed download "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    new-array v1, v4, [Ljava/lang/Object;

    invoke-static {v0, v1}, Lcom/google/android/finsky/utils/FinskyLog;->d(Ljava/lang/String;[Ljava/lang/Object;)V

    :goto_0
    return-void

    :cond_0
    invoke-static {}, Lcom/google/android/finsky/FinskyApp;->get()Lcom/google/android/finsky/FinskyApp;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/android/finsky/FinskyApp;->getAnalytics()Lcom/google/android/finsky/analytics/Analytics;

    move-result-object v0

    const-string v1, "install.downloadComplete"

    const-string v2, "self-update-download"

    invoke-interface {v0, v1, v2, v3}, Lcom/google/android/finsky/analytics/Analytics;->logTagAndPackage(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    invoke-static {}, Lcom/google/android/finsky/FinskyApp;->get()Lcom/google/android/finsky/FinskyApp;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/android/finsky/FinskyApp;->getEventLogger()Lcom/google/android/finsky/analytics/FinskyEventLog;

    move-result-object v0

    const/16 v1, 0x66

    invoke-static {}, Lcom/google/android/finsky/FinskyApp;->get()Lcom/google/android/finsky/FinskyApp;

    move-result-object v2

    invoke-virtual {v2}, Lcom/google/android/finsky/FinskyApp;->getPackageName()Ljava/lang/String;

    move-result-object v2

    iget-object v6, p0, Lcom/google/android/finsky/utils/SelfUpdateScheduler;->mLogAppData:Lcom/google/android/play/analytics/PlayStore$AppData;

    move-object v5, v3

    invoke-virtual/range {v0 .. v6}, Lcom/google/android/finsky/analytics/FinskyEventLog;->logBackgroundEvent(ILjava/lang/String;Ljava/lang/String;ILjava/lang/String;Lcom/google/android/play/analytics/PlayStore$AppData;)V

    iput-object v3, p0, Lcom/google/android/finsky/utils/SelfUpdateScheduler;->mUpdateDownload:Lcom/google/android/finsky/download/Download;

    iget-object v0, p0, Lcom/google/android/finsky/utils/SelfUpdateScheduler;->mOnAppExitDeferrer:Lcom/google/android/finsky/utils/ApplicationDismissedDeferrer;

    if-eqz v0, :cond_1

    const-string v0, "Self-update package Uri was already assigned!"

    new-array v1, v4, [Ljava/lang/Object;

    invoke-static {v0, v1}, Lcom/google/android/finsky/utils/FinskyLog;->d(Ljava/lang/String;[Ljava/lang/Object;)V

    goto :goto_0

    :cond_1
    const-string v0, "Self-update ready to be installed, waiting for market to close."

    new-array v1, v4, [Ljava/lang/Object;

    invoke-static {v0, v1}, Lcom/google/android/finsky/utils/FinskyLog;->d(Ljava/lang/String;[Ljava/lang/Object;)V

    new-instance v0, Lcom/google/android/finsky/utils/ApplicationDismissedDeferrer;

    invoke-static {}, Lcom/google/android/finsky/FinskyApp;->get()Lcom/google/android/finsky/FinskyApp;

    move-result-object v1

    invoke-direct {v0, v1}, Lcom/google/android/finsky/utils/ApplicationDismissedDeferrer;-><init>(Landroid/content/Context;)V

    iput-object v0, p0, Lcom/google/android/finsky/utils/SelfUpdateScheduler;->mOnAppExitDeferrer:Lcom/google/android/finsky/utils/ApplicationDismissedDeferrer;

    iget-object v0, p0, Lcom/google/android/finsky/utils/SelfUpdateScheduler;->mOnAppExitDeferrer:Lcom/google/android/finsky/utils/ApplicationDismissedDeferrer;

    new-instance v1, Lcom/google/android/finsky/utils/SelfUpdateScheduler$3;

    invoke-direct {v1, p0, p1}, Lcom/google/android/finsky/utils/SelfUpdateScheduler$3;-><init>(Lcom/google/android/finsky/utils/SelfUpdateScheduler;Lcom/google/android/finsky/download/Download;)V

    const/16 v2, 0x2710

    invoke-virtual {v0, v1, v2}, Lcom/google/android/finsky/utils/ApplicationDismissedDeferrer;->runOnApplicationClose(Ljava/lang/Runnable;I)V

    goto :goto_0
.end method

.method public onError(Lcom/google/android/finsky/download/Download;I)V
    .locals 7
    .param p1    # Lcom/google/android/finsky/download/Download;
    .param p2    # I

    const/4 v3, 0x0

    iget-object v0, p0, Lcom/google/android/finsky/utils/SelfUpdateScheduler;->mUpdateDownload:Lcom/google/android/finsky/download/Download;

    if-ne p1, v0, :cond_0

    invoke-static {}, Lcom/google/android/finsky/FinskyApp;->get()Lcom/google/android/finsky/FinskyApp;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/android/finsky/FinskyApp;->getAnalytics()Lcom/google/android/finsky/analytics/Analytics;

    move-result-object v0

    const-string v1, "install.downloadError"

    const-string v2, "self-update-download"

    invoke-static {p2}, Ljava/lang/String;->valueOf(I)Ljava/lang/String;

    move-result-object v4

    invoke-interface {v0, v1, v2, v4}, Lcom/google/android/finsky/analytics/Analytics;->logTagAndPackage(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    invoke-static {}, Lcom/google/android/finsky/FinskyApp;->get()Lcom/google/android/finsky/FinskyApp;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/android/finsky/FinskyApp;->getEventLogger()Lcom/google/android/finsky/analytics/FinskyEventLog;

    move-result-object v0

    const/16 v1, 0x68

    invoke-static {}, Lcom/google/android/finsky/FinskyApp;->get()Lcom/google/android/finsky/FinskyApp;

    move-result-object v2

    invoke-virtual {v2}, Lcom/google/android/finsky/FinskyApp;->getPackageName()Ljava/lang/String;

    move-result-object v2

    iget-object v6, p0, Lcom/google/android/finsky/utils/SelfUpdateScheduler;->mLogAppData:Lcom/google/android/play/analytics/PlayStore$AppData;

    move v4, p2

    move-object v5, v3

    invoke-virtual/range {v0 .. v6}, Lcom/google/android/finsky/analytics/FinskyEventLog;->logBackgroundEvent(ILjava/lang/String;Ljava/lang/String;ILjava/lang/String;Lcom/google/android/play/analytics/PlayStore$AppData;)V

    const-string v0, "Self-update failed because of HTTP error code: %d"

    const/4 v1, 0x1

    new-array v1, v1, [Ljava/lang/Object;

    const/4 v2, 0x0

    invoke-static {p2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v3

    aput-object v3, v1, v2

    invoke-static {v0, v1}, Lcom/google/android/finsky/utils/FinskyLog;->e(Ljava/lang/String;[Ljava/lang/Object;)V

    :cond_0
    return-void
.end method

.method public onNotificationClicked(Lcom/google/android/finsky/download/Download;)V
    .locals 0
    .param p1    # Lcom/google/android/finsky/download/Download;

    return-void
.end method

.method public onProgress(Lcom/google/android/finsky/download/Download;Lcom/google/android/finsky/download/DownloadProgress;)V
    .locals 0
    .param p1    # Lcom/google/android/finsky/download/Download;
    .param p2    # Lcom/google/android/finsky/download/DownloadProgress;

    return-void
.end method

.method public onStart(Lcom/google/android/finsky/download/Download;)V
    .locals 7
    .param p1    # Lcom/google/android/finsky/download/Download;

    const/4 v3, 0x0

    iget-object v0, p0, Lcom/google/android/finsky/utils/SelfUpdateScheduler;->mUpdateDownload:Lcom/google/android/finsky/download/Download;

    if-ne p1, v0, :cond_0

    invoke-static {}, Lcom/google/android/finsky/FinskyApp;->get()Lcom/google/android/finsky/FinskyApp;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/android/finsky/FinskyApp;->getAnalytics()Lcom/google/android/finsky/analytics/Analytics;

    move-result-object v0

    const-string v1, "install.downloadStarted"

    const-string v2, "self-update-download"

    invoke-interface {v0, v1, v2, v3}, Lcom/google/android/finsky/analytics/Analytics;->logTagAndPackage(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    invoke-static {}, Lcom/google/android/finsky/FinskyApp;->get()Lcom/google/android/finsky/FinskyApp;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/android/finsky/FinskyApp;->getEventLogger()Lcom/google/android/finsky/analytics/FinskyEventLog;

    move-result-object v0

    const/16 v1, 0x65

    invoke-static {}, Lcom/google/android/finsky/FinskyApp;->get()Lcom/google/android/finsky/FinskyApp;

    move-result-object v2

    invoke-virtual {v2}, Lcom/google/android/finsky/FinskyApp;->getPackageName()Ljava/lang/String;

    move-result-object v2

    const/4 v4, 0x0

    iget-object v6, p0, Lcom/google/android/finsky/utils/SelfUpdateScheduler;->mLogAppData:Lcom/google/android/play/analytics/PlayStore$AppData;

    move-object v5, v3

    invoke-virtual/range {v0 .. v6}, Lcom/google/android/finsky/analytics/FinskyEventLog;->logBackgroundEvent(ILjava/lang/String;Ljava/lang/String;ILjava/lang/String;Lcom/google/android/play/analytics/PlayStore$AppData;)V

    :cond_0
    return-void
.end method
