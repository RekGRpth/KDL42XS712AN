.class public Lcom/google/android/finsky/utils/PurchaseButtonHelper;
.super Ljava/lang/Object;
.source "PurchaseButtonHelper.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/google/android/finsky/utils/PurchaseButtonHelper$PriceStylingInfo;
    }
.end annotation


# direct methods
.method public constructor <init>()V
    .locals 0

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method public static getPurchaseOrOpenClickListener(Landroid/accounts/Account;Lcom/google/android/finsky/receivers/Installer;Lcom/google/android/finsky/library/Libraries;Lcom/google/android/finsky/appstate/AppStates;Lcom/google/android/finsky/api/model/DfeToc;Lcom/google/android/finsky/api/model/Document;Landroid/accounts/Account;Lcom/google/android/finsky/navigationmanager/NavigationManager;Ljava/lang/String;Lcom/google/android/finsky/layout/play/PlayStoreUiElementNode;)Landroid/view/View$OnClickListener;
    .locals 20
    .param p0    # Landroid/accounts/Account;
    .param p1    # Lcom/google/android/finsky/receivers/Installer;
    .param p2    # Lcom/google/android/finsky/library/Libraries;
    .param p3    # Lcom/google/android/finsky/appstate/AppStates;
    .param p4    # Lcom/google/android/finsky/api/model/DfeToc;
    .param p5    # Lcom/google/android/finsky/api/model/Document;
    .param p6    # Landroid/accounts/Account;
    .param p7    # Lcom/google/android/finsky/navigationmanager/NavigationManager;
    .param p8    # Ljava/lang/String;
    .param p9    # Lcom/google/android/finsky/layout/play/PlayStoreUiElementNode;

    move-object/from16 v0, p5

    move-object/from16 v1, p2

    move-object/from16 v2, p0

    invoke-static {v0, v1, v2}, Lcom/google/android/finsky/utils/LibraryUtils;->getOwnerWithCurrentAccount(Lcom/google/android/finsky/api/model/Document;Lcom/google/android/finsky/library/Libraries;Landroid/accounts/Account;)Landroid/accounts/Account;

    move-result-object v4

    move-object/from16 v0, p2

    move-object/from16 v1, p0

    invoke-virtual {v0, v1}, Lcom/google/android/finsky/library/Libraries;->getAccountLibrary(Landroid/accounts/Account;)Lcom/google/android/finsky/library/AccountLibrary;

    move-result-object v3

    move-object/from16 v0, p5

    move-object/from16 v1, p4

    invoke-static {v0, v1, v3}, Lcom/google/android/finsky/utils/DocUtils;->getListingOffer(Lcom/google/android/finsky/api/model/Document;Lcom/google/android/finsky/api/model/DfeToc;Lcom/google/android/finsky/library/Library;)Lcom/google/android/finsky/protos/Common$Offer;

    move-result-object v17

    if-eqz v17, :cond_3

    invoke-virtual/range {v17 .. v17}, Lcom/google/android/finsky/protos/Common$Offer;->getOfferType()I

    move-result v6

    :goto_0
    if-eqz v4, :cond_4

    const/16 v18, 0x1

    :goto_1
    const/4 v15, 0x1

    const/16 v16, 0x0

    const/4 v11, 0x0

    invoke-virtual/range {p5 .. p5}, Lcom/google/android/finsky/api/model/Document;->getDocumentType()I

    move-result v14

    const/4 v3, 0x1

    if-ne v14, v3, :cond_0

    invoke-virtual/range {p5 .. p5}, Lcom/google/android/finsky/api/model/Document;->getAppDetails()Lcom/google/android/finsky/protos/DocDetails$AppDetails;

    move-result-object v12

    invoke-virtual {v12}, Lcom/google/android/finsky/protos/DocDetails$AppDetails;->getPackageName()Ljava/lang/String;

    move-result-object v19

    new-instance v11, Lcom/google/android/finsky/activities/AppActionAnalyzer;

    move-object/from16 v0, v19

    move-object/from16 v1, p3

    move-object/from16 v2, p2

    invoke-direct {v11, v0, v1, v2}, Lcom/google/android/finsky/activities/AppActionAnalyzer;-><init>(Ljava/lang/String;Lcom/google/android/finsky/appstate/AppStates;Lcom/google/android/finsky/library/Libraries;)V

    iget-boolean v15, v11, Lcom/google/android/finsky/activities/AppActionAnalyzer;->isInstalled:Z

    move-object/from16 v0, p1

    move-object/from16 v1, v19

    invoke-interface {v0, v1}, Lcom/google/android/finsky/receivers/Installer;->getState(Ljava/lang/String;)Lcom/google/android/finsky/receivers/Installer$InstallerState;

    move-result-object v3

    sget-object v5, Lcom/google/android/finsky/receivers/Installer$InstallerState;->NOT_TRACKED:Lcom/google/android/finsky/receivers/Installer$InstallerState;

    if-eq v3, v5, :cond_5

    const/16 v16, 0x1

    :cond_0
    :goto_2
    const/4 v13, 0x0

    if-nez v18, :cond_7

    invoke-virtual/range {p5 .. p5}, Lcom/google/android/finsky/api/model/Document;->getAvailableOffers()Ljava/util/List;

    move-result-object v3

    invoke-interface {v3}, Ljava/util/List;->size()I

    move-result v3

    const/4 v5, 0x1

    if-le v3, v5, :cond_6

    const/4 v3, 0x6

    if-eq v14, v3, :cond_1

    const/16 v3, 0x13

    if-eq v14, v3, :cond_1

    const/16 v3, 0x14

    if-ne v14, v3, :cond_6

    :cond_1
    const/4 v6, 0x0

    const/4 v7, 0x0

    const/4 v8, 0x0

    const/16 v9, 0xc8

    move-object/from16 v3, p7

    move-object/from16 v4, p6

    move-object/from16 v5, p5

    move-object/from16 v10, p9

    invoke-virtual/range {v3 .. v10}, Lcom/google/android/finsky/navigationmanager/NavigationManager;->getBuyImmediateClickListener(Landroid/accounts/Account;Lcom/google/android/finsky/api/model/Document;ILjava/lang/String;Ljava/lang/String;ILcom/google/android/finsky/layout/play/PlayStoreUiElementNode;)Landroid/view/View$OnClickListener;

    move-result-object v13

    :cond_2
    :goto_3
    move-object v3, v13

    :goto_4
    return-object v3

    :cond_3
    const/4 v6, 0x1

    goto :goto_0

    :cond_4
    const/16 v18, 0x0

    goto :goto_1

    :cond_5
    const/16 v16, 0x0

    goto :goto_2

    :cond_6
    const/4 v7, 0x0

    const/4 v8, 0x0

    const/16 v9, 0xc8

    move-object/from16 v3, p7

    move-object/from16 v4, p6

    move-object/from16 v5, p5

    move-object/from16 v10, p9

    invoke-virtual/range {v3 .. v10}, Lcom/google/android/finsky/navigationmanager/NavigationManager;->getBuyImmediateClickListener(Landroid/accounts/Account;Lcom/google/android/finsky/api/model/Document;ILjava/lang/String;Ljava/lang/String;ILcom/google/android/finsky/layout/play/PlayStoreUiElementNode;)Landroid/view/View$OnClickListener;

    move-result-object v13

    goto :goto_3

    :cond_7
    if-nez v15, :cond_8

    const/4 v7, 0x0

    const/4 v8, 0x0

    const/16 v9, 0xdd

    move-object/from16 v3, p7

    move-object/from16 v5, p5

    move-object/from16 v10, p9

    invoke-virtual/range {v3 .. v10}, Lcom/google/android/finsky/navigationmanager/NavigationManager;->getBuyImmediateClickListener(Landroid/accounts/Account;Lcom/google/android/finsky/api/model/Document;ILjava/lang/String;Ljava/lang/String;ILcom/google/android/finsky/layout/play/PlayStoreUiElementNode;)Landroid/view/View$OnClickListener;

    move-result-object v13

    goto :goto_3

    :cond_8
    if-eqz v11, :cond_a

    iget-boolean v3, v11, Lcom/google/android/finsky/activities/AppActionAnalyzer;->isDisabled:Z

    if-eqz v3, :cond_9

    const/4 v3, 0x0

    goto :goto_4

    :cond_9
    move-object/from16 v0, p5

    invoke-virtual {v11, v0}, Lcom/google/android/finsky/activities/AppActionAnalyzer;->hasUpdateAvailable(Lcom/google/android/finsky/api/model/Document;)Z

    move-result v3

    if-eqz v3, :cond_b

    const/4 v7, 0x0

    const/4 v8, 0x0

    const/16 v9, 0xd9

    move-object/from16 v3, p7

    move-object/from16 v5, p5

    move-object/from16 v10, p9

    invoke-virtual/range {v3 .. v10}, Lcom/google/android/finsky/navigationmanager/NavigationManager;->getBuyImmediateClickListener(Landroid/accounts/Account;Lcom/google/android/finsky/api/model/Document;ILjava/lang/String;Ljava/lang/String;ILcom/google/android/finsky/layout/play/PlayStoreUiElementNode;)Landroid/view/View$OnClickListener;

    move-result-object v13

    :cond_a
    if-nez v13, :cond_2

    move-object/from16 v0, p7

    move-object/from16 v1, p5

    move-object/from16 v2, p9

    invoke-virtual {v0, v1, v4, v2}, Lcom/google/android/finsky/navigationmanager/NavigationManager;->getOpenClickListener(Lcom/google/android/finsky/api/model/Document;Landroid/accounts/Account;Lcom/google/android/finsky/layout/play/PlayStoreUiElementNode;)Landroid/view/View$OnClickListener;

    move-result-object v13

    goto :goto_3

    :cond_b
    if-eqz v16, :cond_a

    const/4 v3, 0x0

    goto :goto_4
.end method

.method private static styleFromOffer(Lcom/google/android/finsky/api/model/Document;Lcom/google/android/finsky/protos/Common$Offer;ZLcom/google/android/finsky/utils/PurchaseButtonHelper$PriceStylingInfo;)V
    .locals 11
    .param p0    # Lcom/google/android/finsky/api/model/Document;
    .param p1    # Lcom/google/android/finsky/protos/Common$Offer;
    .param p2    # Z
    .param p3    # Lcom/google/android/finsky/utils/PurchaseButtonHelper$PriceStylingInfo;

    const/4 v5, 0x1

    const/4 v10, -0x1

    iput-object p1, p3, Lcom/google/android/finsky/utils/PurchaseButtonHelper$PriceStylingInfo;->labelOffer:Lcom/google/android/finsky/protos/Common$Offer;

    invoke-virtual {p1}, Lcom/google/android/finsky/protos/Common$Offer;->getOfferType()I

    move-result v3

    const/4 v4, 0x3

    if-eq v3, v4, :cond_0

    const/4 v4, 0x4

    if-ne v3, v4, :cond_1

    :cond_0
    move v1, v5

    :goto_0
    if-eqz p2, :cond_3

    if-eqz v1, :cond_2

    const v2, 0x7f070145    # com.android.vending.R.string.rent_resolution

    :goto_1
    # invokes: Lcom/google/android/finsky/utils/PurchaseButtonHelper$PriceStylingInfo;->setLabels(II)V
    invoke-static {p3, v10, v2}, Lcom/google/android/finsky/utils/PurchaseButtonHelper$PriceStylingInfo;->access$100(Lcom/google/android/finsky/utils/PurchaseButtonHelper$PriceStylingInfo;II)V

    :goto_2
    iput-boolean v5, p3, Lcom/google/android/finsky/utils/PurchaseButtonHelper$PriceStylingInfo;->isActionEnabled:Z

    return-void

    :cond_1
    const/4 v1, 0x0

    goto :goto_0

    :cond_2
    const v2, 0x7f070148    # com.android.vending.R.string.purchase_resolution

    goto :goto_1

    :cond_3
    if-eqz p1, :cond_4

    invoke-virtual {p1}, Lcom/google/android/finsky/protos/Common$Offer;->getMicros()J

    move-result-wide v6

    const-wide/16 v8, 0x0

    cmp-long v4, v6, v8

    if-nez v4, :cond_6

    :cond_4
    invoke-virtual {p0}, Lcom/google/android/finsky/api/model/Document;->getDocumentType()I

    move-result v4

    if-ne v4, v5, :cond_5

    const v0, 0x7f070128    # com.android.vending.R.string.install

    :goto_3
    # invokes: Lcom/google/android/finsky/utils/PurchaseButtonHelper$PriceStylingInfo;->setLabels(II)V
    invoke-static {p3, v10, v0}, Lcom/google/android/finsky/utils/PurchaseButtonHelper$PriceStylingInfo;->access$100(Lcom/google/android/finsky/utils/PurchaseButtonHelper$PriceStylingInfo;II)V

    goto :goto_2

    :cond_5
    const v0, 0x7f0702b9    # com.android.vending.R.string.add_to_library

    goto :goto_3

    :cond_6
    invoke-static {p1}, Lcom/google/android/finsky/api/model/Document;->isPreorderOffer(Lcom/google/android/finsky/protos/Common$Offer;)Z

    move-result v4

    if-eqz v4, :cond_7

    const v4, 0x7f07014e    # com.android.vending.R.string.preorder_action

    # invokes: Lcom/google/android/finsky/utils/PurchaseButtonHelper$PriceStylingInfo;->setLabels(II)V
    invoke-static {p3, v10, v4}, Lcom/google/android/finsky/utils/PurchaseButtonHelper$PriceStylingInfo;->access$100(Lcom/google/android/finsky/utils/PurchaseButtonHelper$PriceStylingInfo;II)V

    goto :goto_2

    :cond_7
    if-eqz v1, :cond_8

    const v4, 0x7f070142    # com.android.vending.R.string.rent

    :goto_4
    # invokes: Lcom/google/android/finsky/utils/PurchaseButtonHelper$PriceStylingInfo;->setLabels(II)V
    invoke-static {p3, v10, v4}, Lcom/google/android/finsky/utils/PurchaseButtonHelper$PriceStylingInfo;->access$100(Lcom/google/android/finsky/utils/PurchaseButtonHelper$PriceStylingInfo;II)V

    goto :goto_2

    :cond_8
    const v4, 0x7f070141    # com.android.vending.R.string.buy

    goto :goto_4
.end method

.method public static stylePurchaseButton(Landroid/accounts/Account;Lcom/google/android/finsky/receivers/Installer;Lcom/google/android/finsky/library/Libraries;Lcom/google/android/finsky/appstate/AppStates;Lcom/google/android/finsky/api/model/DfeToc;Lcom/google/android/finsky/api/model/Document;Lcom/google/android/finsky/utils/PurchaseButtonHelper$PriceStylingInfo;)V
    .locals 14
    .param p0    # Landroid/accounts/Account;
    .param p1    # Lcom/google/android/finsky/receivers/Installer;
    .param p2    # Lcom/google/android/finsky/library/Libraries;
    .param p3    # Lcom/google/android/finsky/appstate/AppStates;
    .param p4    # Lcom/google/android/finsky/api/model/DfeToc;
    .param p5    # Lcom/google/android/finsky/api/model/Document;
    .param p6    # Lcom/google/android/finsky/utils/PurchaseButtonHelper$PriceStylingInfo;

    const/4 v10, 0x0

    move-object/from16 v0, p6

    iput-object v10, v0, Lcom/google/android/finsky/utils/PurchaseButtonHelper$PriceStylingInfo;->labelOffer:Lcom/google/android/finsky/protos/Common$Offer;

    const/4 v10, -0x1

    move-object/from16 v0, p6

    iput v10, v0, Lcom/google/android/finsky/utils/PurchaseButtonHelper$PriceStylingInfo;->labelListingResourceId:I

    const/4 v10, -0x1

    move-object/from16 v0, p6

    iput v10, v0, Lcom/google/android/finsky/utils/PurchaseButtonHelper$PriceStylingInfo;->labelActionResourceId:I

    const/4 v10, 0x1

    move-object/from16 v0, p6

    iput-boolean v10, v0, Lcom/google/android/finsky/utils/PurchaseButtonHelper$PriceStylingInfo;->isActionEnabled:Z

    const/4 v10, 0x0

    move-object/from16 v0, p6

    iput-boolean v10, v0, Lcom/google/android/finsky/utils/PurchaseButtonHelper$PriceStylingInfo;->isOwned:Z

    move-object/from16 v0, p2

    invoke-virtual {v0, p0}, Lcom/google/android/finsky/library/Libraries;->getAccountLibrary(Landroid/accounts/Account;)Lcom/google/android/finsky/library/AccountLibrary;

    move-result-object v5

    move-object/from16 v0, p5

    move-object/from16 v1, p4

    invoke-static {v0, v1, v5}, Lcom/google/android/finsky/utils/DocUtils;->getListingOffer(Lcom/google/android/finsky/api/model/Document;Lcom/google/android/finsky/api/model/DfeToc;Lcom/google/android/finsky/library/Library;)Lcom/google/android/finsky/protos/Common$Offer;

    move-result-object v6

    if-nez v6, :cond_0

    const/4 v10, 0x0

    move-object/from16 v0, p6

    iput-boolean v10, v0, Lcom/google/android/finsky/utils/PurchaseButtonHelper$PriceStylingInfo;->isActionEnabled:Z

    :goto_0
    return-void

    :cond_0
    move-object/from16 v0, p5

    move-object/from16 v1, p4

    invoke-static {v0, v1, v5}, Lcom/google/android/finsky/utils/DocUtils;->hasMultipleOffers(Lcom/google/android/finsky/api/model/Document;Lcom/google/android/finsky/api/model/DfeToc;Lcom/google/android/finsky/library/Library;)Z

    move-result v9

    move-object/from16 v0, p5

    move-object/from16 v1, p2

    invoke-static {v0, v1, p0}, Lcom/google/android/finsky/utils/LibraryUtils;->getOwnerWithCurrentAccount(Lcom/google/android/finsky/api/model/Document;Lcom/google/android/finsky/library/Libraries;Landroid/accounts/Account;)Landroid/accounts/Account;

    move-result-object v8

    if-eqz v8, :cond_1

    const/4 v7, 0x1

    :goto_1
    if-eqz v7, :cond_2

    invoke-static {v6}, Lcom/google/android/finsky/api/model/Document;->isPreorderOffer(Lcom/google/android/finsky/protos/Common$Offer;)Z

    move-result v4

    :goto_2
    move-object/from16 v0, p6

    iput-boolean v7, v0, Lcom/google/android/finsky/utils/PurchaseButtonHelper$PriceStylingInfo;->isOwned:Z

    invoke-virtual/range {p5 .. p5}, Lcom/google/android/finsky/api/model/Document;->getBackend()I

    move-result v10

    packed-switch v10, :pswitch_data_0

    :pswitch_0
    const-string v10, "Unsupported backend: %d"

    const/4 v11, 0x1

    new-array v11, v11, [Ljava/lang/Object;

    const/4 v12, 0x0

    invoke-virtual/range {p5 .. p5}, Lcom/google/android/finsky/api/model/Document;->getBackend()I

    move-result v13

    invoke-static {v13}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v13

    aput-object v13, v11, v12

    invoke-static {v10, v11}, Lcom/google/android/finsky/utils/FinskyLog;->wtf(Ljava/lang/String;[Ljava/lang/Object;)V

    move-object/from16 v0, p5

    move-object/from16 v1, p6

    invoke-static {v0, v6, v9, v1}, Lcom/google/android/finsky/utils/PurchaseButtonHelper;->styleFromOffer(Lcom/google/android/finsky/api/model/Document;Lcom/google/android/finsky/protos/Common$Offer;ZLcom/google/android/finsky/utils/PurchaseButtonHelper$PriceStylingInfo;)V

    goto :goto_0

    :cond_1
    const/4 v7, 0x0

    goto :goto_1

    :cond_2
    const/4 v4, 0x0

    goto :goto_2

    :pswitch_1
    invoke-virtual/range {p5 .. p5}, Lcom/google/android/finsky/api/model/Document;->getDocumentType()I

    move-result v10

    const/4 v11, 0x1

    if-ne v10, v11, :cond_6

    invoke-virtual/range {p5 .. p5}, Lcom/google/android/finsky/api/model/Document;->getAppDetails()Lcom/google/android/finsky/protos/DocDetails$AppDetails;

    move-result-object v3

    new-instance v2, Lcom/google/android/finsky/activities/AppActionAnalyzer;

    invoke-virtual {v3}, Lcom/google/android/finsky/protos/DocDetails$AppDetails;->getPackageName()Ljava/lang/String;

    move-result-object v10

    move-object/from16 v0, p3

    move-object/from16 v1, p2

    invoke-direct {v2, v10, v0, v1}, Lcom/google/android/finsky/activities/AppActionAnalyzer;-><init>(Ljava/lang/String;Lcom/google/android/finsky/appstate/AppStates;Lcom/google/android/finsky/library/Libraries;)V

    iget-boolean v10, v2, Lcom/google/android/finsky/activities/AppActionAnalyzer;->isInstalled:Z

    if-eqz v10, :cond_5

    iget-boolean v10, v2, Lcom/google/android/finsky/activities/AppActionAnalyzer;->isDisabled:Z

    if-eqz v10, :cond_3

    const/4 v10, 0x0

    move-object/from16 v0, p6

    iput-boolean v10, v0, Lcom/google/android/finsky/utils/PurchaseButtonHelper$PriceStylingInfo;->isActionEnabled:Z

    const v10, 0x7f070111    # com.android.vending.R.string.disabled_list_state

    move-object/from16 v0, p6

    # invokes: Lcom/google/android/finsky/utils/PurchaseButtonHelper$PriceStylingInfo;->setLabels(I)V
    invoke-static {v0, v10}, Lcom/google/android/finsky/utils/PurchaseButtonHelper$PriceStylingInfo;->access$000(Lcom/google/android/finsky/utils/PurchaseButtonHelper$PriceStylingInfo;I)V

    :goto_3
    const/4 v10, 0x1

    move-object/from16 v0, p6

    iput-boolean v10, v0, Lcom/google/android/finsky/utils/PurchaseButtonHelper$PriceStylingInfo;->isOwned:Z

    goto :goto_0

    :cond_3
    move-object/from16 v0, p5

    invoke-virtual {v2, v0}, Lcom/google/android/finsky/activities/AppActionAnalyzer;->hasUpdateAvailable(Lcom/google/android/finsky/api/model/Document;)Z

    move-result v10

    if-eqz v10, :cond_4

    const v10, 0x7f070115    # com.android.vending.R.string.updates_list_state

    move-object/from16 v0, p6

    # invokes: Lcom/google/android/finsky/utils/PurchaseButtonHelper$PriceStylingInfo;->setLabels(I)V
    invoke-static {v0, v10}, Lcom/google/android/finsky/utils/PurchaseButtonHelper$PriceStylingInfo;->access$000(Lcom/google/android/finsky/utils/PurchaseButtonHelper$PriceStylingInfo;I)V

    const v10, 0x7f070115    # com.android.vending.R.string.updates_list_state

    move-object/from16 v0, p6

    iput v10, v0, Lcom/google/android/finsky/utils/PurchaseButtonHelper$PriceStylingInfo;->labelListingResourceId:I

    const v10, 0x7f070115    # com.android.vending.R.string.updates_list_state

    move-object/from16 v0, p6

    iput v10, v0, Lcom/google/android/finsky/utils/PurchaseButtonHelper$PriceStylingInfo;->labelActionResourceId:I

    goto :goto_3

    :cond_4
    const v10, 0x7f070110    # com.android.vending.R.string.installed_list_state

    const v11, 0x7f070154    # com.android.vending.R.string.open

    move-object/from16 v0, p6

    # invokes: Lcom/google/android/finsky/utils/PurchaseButtonHelper$PriceStylingInfo;->setLabels(II)V
    invoke-static {v0, v10, v11}, Lcom/google/android/finsky/utils/PurchaseButtonHelper$PriceStylingInfo;->access$100(Lcom/google/android/finsky/utils/PurchaseButtonHelper$PriceStylingInfo;II)V

    goto :goto_3

    :cond_5
    invoke-virtual {v3}, Lcom/google/android/finsky/protos/DocDetails$AppDetails;->getPackageName()Ljava/lang/String;

    move-result-object v10

    invoke-interface {p1, v10}, Lcom/google/android/finsky/receivers/Installer;->getState(Ljava/lang/String;)Lcom/google/android/finsky/receivers/Installer$InstallerState;

    move-result-object v10

    sget-object v11, Lcom/google/android/finsky/receivers/Installer$InstallerState;->NOT_TRACKED:Lcom/google/android/finsky/receivers/Installer$InstallerState;

    if-eq v10, v11, :cond_6

    const v10, 0x7f0701dd    # com.android.vending.R.string.installing

    move-object/from16 v0, p6

    # invokes: Lcom/google/android/finsky/utils/PurchaseButtonHelper$PriceStylingInfo;->setLabels(I)V
    invoke-static {v0, v10}, Lcom/google/android/finsky/utils/PurchaseButtonHelper$PriceStylingInfo;->access$000(Lcom/google/android/finsky/utils/PurchaseButtonHelper$PriceStylingInfo;I)V

    const/4 v10, 0x0

    move-object/from16 v0, p6

    iput-boolean v10, v0, Lcom/google/android/finsky/utils/PurchaseButtonHelper$PriceStylingInfo;->isActionEnabled:Z

    goto/16 :goto_0

    :cond_6
    if-eqz v7, :cond_8

    if-eqz v6, :cond_8

    invoke-virtual {v6}, Lcom/google/android/finsky/protos/Common$Offer;->getMicros()J

    move-result-wide v10

    const-wide/16 v12, 0x0

    cmp-long v10, v10, v12

    if-lez v10, :cond_8

    if-eqz v4, :cond_7

    const v10, 0x7f070113    # com.android.vending.R.string.preordered_list_state

    move-object/from16 v0, p6

    # invokes: Lcom/google/android/finsky/utils/PurchaseButtonHelper$PriceStylingInfo;->setLabels(I)V
    invoke-static {v0, v10}, Lcom/google/android/finsky/utils/PurchaseButtonHelper$PriceStylingInfo;->access$000(Lcom/google/android/finsky/utils/PurchaseButtonHelper$PriceStylingInfo;I)V

    const/4 v10, 0x0

    move-object/from16 v0, p6

    iput-boolean v10, v0, Lcom/google/android/finsky/utils/PurchaseButtonHelper$PriceStylingInfo;->isActionEnabled:Z

    goto/16 :goto_0

    :cond_7
    const v10, 0x7f070112    # com.android.vending.R.string.purchased_list_state

    const v11, 0x7f070128    # com.android.vending.R.string.install

    move-object/from16 v0, p6

    # invokes: Lcom/google/android/finsky/utils/PurchaseButtonHelper$PriceStylingInfo;->setLabels(II)V
    invoke-static {v0, v10, v11}, Lcom/google/android/finsky/utils/PurchaseButtonHelper$PriceStylingInfo;->access$100(Lcom/google/android/finsky/utils/PurchaseButtonHelper$PriceStylingInfo;II)V

    const/4 v10, 0x1

    move-object/from16 v0, p6

    iput-boolean v10, v0, Lcom/google/android/finsky/utils/PurchaseButtonHelper$PriceStylingInfo;->isActionEnabled:Z

    goto/16 :goto_0

    :cond_8
    move-object/from16 v0, p5

    move-object/from16 v1, p6

    invoke-static {v0, v6, v9, v1}, Lcom/google/android/finsky/utils/PurchaseButtonHelper;->styleFromOffer(Lcom/google/android/finsky/api/model/Document;Lcom/google/android/finsky/protos/Common$Offer;ZLcom/google/android/finsky/utils/PurchaseButtonHelper$PriceStylingInfo;)V

    goto/16 :goto_0

    :pswitch_2
    invoke-virtual/range {p5 .. p5}, Lcom/google/android/finsky/api/model/Document;->hasSubscriptions()Z

    move-result v10

    if-eqz v10, :cond_9

    invoke-virtual/range {p5 .. p5}, Lcom/google/android/finsky/api/model/Document;->getSubscriptionsList()Ljava/util/List;

    move-result-object v10

    move-object/from16 v0, p2

    invoke-static {v10, v0, p0}, Lcom/google/android/finsky/utils/LibraryUtils;->getOwnerWithCurrentAccount(Ljava/util/List;Lcom/google/android/finsky/library/Libraries;Landroid/accounts/Account;)Landroid/accounts/Account;

    move-result-object v10

    if-eqz v10, :cond_9

    const v10, 0x7f070116    # com.android.vending.R.string.subscribed_list_state

    const v11, 0x7f070127    # com.android.vending.R.string.read

    move-object/from16 v0, p6

    # invokes: Lcom/google/android/finsky/utils/PurchaseButtonHelper$PriceStylingInfo;->setLabels(II)V
    invoke-static {v0, v10, v11}, Lcom/google/android/finsky/utils/PurchaseButtonHelper$PriceStylingInfo;->access$100(Lcom/google/android/finsky/utils/PurchaseButtonHelper$PriceStylingInfo;II)V

    const/4 v10, 0x1

    move-object/from16 v0, p6

    iput-boolean v10, v0, Lcom/google/android/finsky/utils/PurchaseButtonHelper$PriceStylingInfo;->isOwned:Z

    goto/16 :goto_0

    :cond_9
    move-object/from16 v0, p5

    move-object/from16 v1, p6

    invoke-static {v0, v6, v9, v1}, Lcom/google/android/finsky/utils/PurchaseButtonHelper;->styleFromOffer(Lcom/google/android/finsky/api/model/Document;Lcom/google/android/finsky/protos/Common$Offer;ZLcom/google/android/finsky/utils/PurchaseButtonHelper$PriceStylingInfo;)V

    goto/16 :goto_0

    :pswitch_3
    if-eqz v7, :cond_b

    if-eqz v4, :cond_a

    const v10, 0x7f070113    # com.android.vending.R.string.preordered_list_state

    move-object/from16 v0, p6

    # invokes: Lcom/google/android/finsky/utils/PurchaseButtonHelper$PriceStylingInfo;->setLabels(I)V
    invoke-static {v0, v10}, Lcom/google/android/finsky/utils/PurchaseButtonHelper$PriceStylingInfo;->access$000(Lcom/google/android/finsky/utils/PurchaseButtonHelper$PriceStylingInfo;I)V

    const/4 v10, 0x0

    move-object/from16 v0, p6

    iput-boolean v10, v0, Lcom/google/android/finsky/utils/PurchaseButtonHelper$PriceStylingInfo;->isActionEnabled:Z

    goto/16 :goto_0

    :cond_a
    const v10, 0x7f070112    # com.android.vending.R.string.purchased_list_state

    const v11, 0x7f070127    # com.android.vending.R.string.read

    move-object/from16 v0, p6

    # invokes: Lcom/google/android/finsky/utils/PurchaseButtonHelper$PriceStylingInfo;->setLabels(II)V
    invoke-static {v0, v10, v11}, Lcom/google/android/finsky/utils/PurchaseButtonHelper$PriceStylingInfo;->access$100(Lcom/google/android/finsky/utils/PurchaseButtonHelper$PriceStylingInfo;II)V

    const/4 v10, 0x1

    move-object/from16 v0, p6

    iput-boolean v10, v0, Lcom/google/android/finsky/utils/PurchaseButtonHelper$PriceStylingInfo;->isActionEnabled:Z

    goto/16 :goto_0

    :cond_b
    move-object/from16 v0, p5

    move-object/from16 v1, p6

    invoke-static {v0, v6, v9, v1}, Lcom/google/android/finsky/utils/PurchaseButtonHelper;->styleFromOffer(Lcom/google/android/finsky/api/model/Document;Lcom/google/android/finsky/protos/Common$Offer;ZLcom/google/android/finsky/utils/PurchaseButtonHelper$PriceStylingInfo;)V

    goto/16 :goto_0

    :pswitch_4
    if-eqz v7, :cond_d

    if-eqz v4, :cond_c

    const v10, 0x7f070113    # com.android.vending.R.string.preordered_list_state

    move-object/from16 v0, p6

    # invokes: Lcom/google/android/finsky/utils/PurchaseButtonHelper$PriceStylingInfo;->setLabels(I)V
    invoke-static {v0, v10}, Lcom/google/android/finsky/utils/PurchaseButtonHelper$PriceStylingInfo;->access$000(Lcom/google/android/finsky/utils/PurchaseButtonHelper$PriceStylingInfo;I)V

    const/4 v10, 0x0

    move-object/from16 v0, p6

    iput-boolean v10, v0, Lcom/google/android/finsky/utils/PurchaseButtonHelper$PriceStylingInfo;->isActionEnabled:Z

    goto/16 :goto_0

    :cond_c
    const v10, 0x7f070112    # com.android.vending.R.string.purchased_list_state

    const v11, 0x7f070156    # com.android.vending.R.string.listen

    move-object/from16 v0, p6

    # invokes: Lcom/google/android/finsky/utils/PurchaseButtonHelper$PriceStylingInfo;->setLabels(II)V
    invoke-static {v0, v10, v11}, Lcom/google/android/finsky/utils/PurchaseButtonHelper$PriceStylingInfo;->access$100(Lcom/google/android/finsky/utils/PurchaseButtonHelper$PriceStylingInfo;II)V

    const/4 v10, 0x1

    move-object/from16 v0, p6

    iput-boolean v10, v0, Lcom/google/android/finsky/utils/PurchaseButtonHelper$PriceStylingInfo;->isActionEnabled:Z

    goto/16 :goto_0

    :cond_d
    move-object/from16 v0, p5

    move-object/from16 v1, p6

    invoke-static {v0, v6, v9, v1}, Lcom/google/android/finsky/utils/PurchaseButtonHelper;->styleFromOffer(Lcom/google/android/finsky/api/model/Document;Lcom/google/android/finsky/protos/Common$Offer;ZLcom/google/android/finsky/utils/PurchaseButtonHelper$PriceStylingInfo;)V

    goto/16 :goto_0

    :pswitch_5
    if-eqz v7, :cond_11

    const/4 v10, 0x7

    move-object/from16 v0, p5

    invoke-static {v0, v5, v10}, Lcom/google/android/finsky/utils/LibraryUtils;->isOfferOwned(Lcom/google/android/finsky/api/model/Document;Lcom/google/android/finsky/library/Library;I)Z

    move-result v10

    if-nez v10, :cond_e

    const/4 v10, 0x1

    move-object/from16 v0, p5

    invoke-static {v0, v5, v10}, Lcom/google/android/finsky/utils/LibraryUtils;->isOfferOwned(Lcom/google/android/finsky/api/model/Document;Lcom/google/android/finsky/library/Library;I)Z

    move-result v10

    if-eqz v10, :cond_10

    :cond_e
    if-eqz v4, :cond_f

    const v10, 0x7f070113    # com.android.vending.R.string.preordered_list_state

    move-object/from16 v0, p6

    # invokes: Lcom/google/android/finsky/utils/PurchaseButtonHelper$PriceStylingInfo;->setLabels(I)V
    invoke-static {v0, v10}, Lcom/google/android/finsky/utils/PurchaseButtonHelper$PriceStylingInfo;->access$000(Lcom/google/android/finsky/utils/PurchaseButtonHelper$PriceStylingInfo;I)V

    const/4 v10, 0x0

    move-object/from16 v0, p6

    iput-boolean v10, v0, Lcom/google/android/finsky/utils/PurchaseButtonHelper$PriceStylingInfo;->isActionEnabled:Z

    goto/16 :goto_0

    :cond_f
    const v10, 0x7f070112    # com.android.vending.R.string.purchased_list_state

    const v11, 0x7f070126    # com.android.vending.R.string.play

    move-object/from16 v0, p6

    # invokes: Lcom/google/android/finsky/utils/PurchaseButtonHelper$PriceStylingInfo;->setLabels(II)V
    invoke-static {v0, v10, v11}, Lcom/google/android/finsky/utils/PurchaseButtonHelper$PriceStylingInfo;->access$100(Lcom/google/android/finsky/utils/PurchaseButtonHelper$PriceStylingInfo;II)V

    const/4 v10, 0x1

    move-object/from16 v0, p6

    iput-boolean v10, v0, Lcom/google/android/finsky/utils/PurchaseButtonHelper$PriceStylingInfo;->isActionEnabled:Z

    goto/16 :goto_0

    :cond_10
    const v10, 0x7f070114    # com.android.vending.R.string.rented_list_state

    const v11, 0x7f070126    # com.android.vending.R.string.play

    move-object/from16 v0, p6

    # invokes: Lcom/google/android/finsky/utils/PurchaseButtonHelper$PriceStylingInfo;->setLabels(II)V
    invoke-static {v0, v10, v11}, Lcom/google/android/finsky/utils/PurchaseButtonHelper$PriceStylingInfo;->access$100(Lcom/google/android/finsky/utils/PurchaseButtonHelper$PriceStylingInfo;II)V

    const/4 v10, 0x1

    move-object/from16 v0, p6

    iput-boolean v10, v0, Lcom/google/android/finsky/utils/PurchaseButtonHelper$PriceStylingInfo;->isActionEnabled:Z

    goto/16 :goto_0

    :cond_11
    move-object/from16 v0, p5

    move-object/from16 v1, p6

    invoke-static {v0, v6, v9, v1}, Lcom/google/android/finsky/utils/PurchaseButtonHelper;->styleFromOffer(Lcom/google/android/finsky/api/model/Document;Lcom/google/android/finsky/protos/Common$Offer;ZLcom/google/android/finsky/utils/PurchaseButtonHelper$PriceStylingInfo;)V

    goto/16 :goto_0

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_3
        :pswitch_4
        :pswitch_1
        :pswitch_5
        :pswitch_0
        :pswitch_2
    .end packed-switch
.end method
