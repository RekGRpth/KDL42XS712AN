.class public Lcom/google/android/finsky/layout/DocImageView;
.super Lcom/google/android/finsky/layout/FifeImageView;
.source "DocImageView.java"


# instance fields
.field private mDoc:Lcom/google/android/finsky/api/model/Document;

.field private mImageTypes:[I


# direct methods
.method public constructor <init>(Landroid/content/Context;)V
    .locals 1
    .param p1    # Landroid/content/Context;

    const/4 v0, 0x0

    invoke-direct {p0, p1, v0}, Lcom/google/android/finsky/layout/DocImageView;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;)V

    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;)V
    .locals 0
    .param p1    # Landroid/content/Context;
    .param p2    # Landroid/util/AttributeSet;

    invoke-direct {p0, p1, p2}, Lcom/google/android/finsky/layout/FifeImageView;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;)V

    return-void
.end method


# virtual methods
.method public bind(Lcom/google/android/finsky/api/model/Document;Lcom/google/android/finsky/utils/BitmapLoader;)V
    .locals 1
    .param p1    # Lcom/google/android/finsky/api/model/Document;
    .param p2    # Lcom/google/android/finsky/utils/BitmapLoader;

    const/4 v0, 0x2

    new-array v0, v0, [I

    fill-array-data v0, :array_0

    invoke-virtual {p0, p1, p2, v0}, Lcom/google/android/finsky/layout/DocImageView;->bind(Lcom/google/android/finsky/api/model/Document;Lcom/google/android/finsky/utils/BitmapLoader;[I)V

    return-void

    :array_0
    .array-data 4
        0x4
        0x0
    .end array-data
.end method

.method public varargs bind(Lcom/google/android/finsky/api/model/Document;Lcom/google/android/finsky/utils/BitmapLoader;[I)V
    .locals 1
    .param p1    # Lcom/google/android/finsky/api/model/Document;
    .param p2    # Lcom/google/android/finsky/utils/BitmapLoader;
    .param p3    # [I

    const/4 v0, 0x0

    iput-object p1, p0, Lcom/google/android/finsky/layout/DocImageView;->mDoc:Lcom/google/android/finsky/api/model/Document;

    iput-object p2, p0, Lcom/google/android/finsky/layout/DocImageView;->mBitmapLoader:Lcom/google/android/finsky/utils/BitmapLoader;

    iput-object p3, p0, Lcom/google/android/finsky/layout/DocImageView;->mImageTypes:[I

    invoke-virtual {p0, v0}, Lcom/google/android/finsky/layout/DocImageView;->setLoaded(Z)V

    iput v0, p0, Lcom/google/android/finsky/layout/DocImageView;->mRequestCount:I

    invoke-virtual {p0}, Lcom/google/android/finsky/layout/DocImageView;->loadImageIfNecessary()V

    return-void
.end method

.method protected getImageToLoad()Lcom/google/android/finsky/protos/Doc$Image;
    .locals 6

    const/4 v5, 0x0

    iget-object v3, p0, Lcom/google/android/finsky/layout/DocImageView;->mDoc:Lcom/google/android/finsky/api/model/Document;

    if-nez v3, :cond_0

    const/4 v1, 0x0

    :goto_0
    return-object v1

    :cond_0
    invoke-virtual {p0}, Lcom/google/android/finsky/layout/DocImageView;->getWidth()I

    move-result v2

    invoke-virtual {p0}, Lcom/google/android/finsky/layout/DocImageView;->getHeight()I

    move-result v0

    if-lez v0, :cond_1

    iget-object v3, p0, Lcom/google/android/finsky/layout/DocImageView;->mDoc:Lcom/google/android/finsky/api/model/Document;

    iget-object v4, p0, Lcom/google/android/finsky/layout/DocImageView;->mImageTypes:[I

    invoke-static {v3, v5, v0, v4}, Lcom/google/android/finsky/utils/ThumbnailUtils;->getImageFromDocument(Lcom/google/android/finsky/api/model/Document;II[I)Lcom/google/android/finsky/protos/Doc$Image;

    move-result-object v1

    :goto_1
    goto :goto_0

    :cond_1
    iget-object v3, p0, Lcom/google/android/finsky/layout/DocImageView;->mDoc:Lcom/google/android/finsky/api/model/Document;

    iget-object v4, p0, Lcom/google/android/finsky/layout/DocImageView;->mImageTypes:[I

    invoke-static {v3, v2, v5, v4}, Lcom/google/android/finsky/utils/ThumbnailUtils;->getImageFromDocument(Lcom/google/android/finsky/api/model/Document;II[I)Lcom/google/android/finsky/protos/Doc$Image;

    move-result-object v1

    goto :goto_1
.end method
