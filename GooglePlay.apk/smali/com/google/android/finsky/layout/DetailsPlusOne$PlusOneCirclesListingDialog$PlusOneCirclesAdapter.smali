.class Lcom/google/android/finsky/layout/DetailsPlusOne$PlusOneCirclesListingDialog$PlusOneCirclesAdapter;
.super Landroid/widget/BaseAdapter;
.source "DetailsPlusOne.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/google/android/finsky/layout/DetailsPlusOne$PlusOneCirclesListingDialog;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0xa
    name = "PlusOneCirclesAdapter"
.end annotation


# instance fields
.field private final mBitmapLoader:Lcom/google/android/finsky/utils/BitmapLoader;

.field private final mContext:Landroid/content/Context;

.field private final mLayoutInflater:Landroid/view/LayoutInflater;

.field private final mPlusOneData:Lcom/google/android/finsky/protos/DocAnnotations$PlusOneData;


# direct methods
.method public constructor <init>(Landroid/content/Context;Lcom/google/android/finsky/utils/BitmapLoader;Lcom/google/android/finsky/protos/DocAnnotations$PlusOneData;)V
    .locals 1
    .param p1    # Landroid/content/Context;
    .param p2    # Lcom/google/android/finsky/utils/BitmapLoader;
    .param p3    # Lcom/google/android/finsky/protos/DocAnnotations$PlusOneData;

    invoke-direct {p0}, Landroid/widget/BaseAdapter;-><init>()V

    iput-object p1, p0, Lcom/google/android/finsky/layout/DetailsPlusOne$PlusOneCirclesListingDialog$PlusOneCirclesAdapter;->mContext:Landroid/content/Context;

    invoke-static {p1}, Landroid/view/LayoutInflater;->from(Landroid/content/Context;)Landroid/view/LayoutInflater;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/finsky/layout/DetailsPlusOne$PlusOneCirclesListingDialog$PlusOneCirclesAdapter;->mLayoutInflater:Landroid/view/LayoutInflater;

    iput-object p2, p0, Lcom/google/android/finsky/layout/DetailsPlusOne$PlusOneCirclesListingDialog$PlusOneCirclesAdapter;->mBitmapLoader:Lcom/google/android/finsky/utils/BitmapLoader;

    iput-object p3, p0, Lcom/google/android/finsky/layout/DetailsPlusOne$PlusOneCirclesListingDialog$PlusOneCirclesAdapter;->mPlusOneData:Lcom/google/android/finsky/protos/DocAnnotations$PlusOneData;

    return-void
.end method

.method private isShowingPartialList()Z
    .locals 4

    iget-object v0, p0, Lcom/google/android/finsky/layout/DetailsPlusOne$PlusOneCirclesListingDialog$PlusOneCirclesAdapter;->mPlusOneData:Lcom/google/android/finsky/protos/DocAnnotations$PlusOneData;

    invoke-virtual {v0}, Lcom/google/android/finsky/protos/DocAnnotations$PlusOneData;->getCirclesTotal()J

    move-result-wide v0

    iget-object v2, p0, Lcom/google/android/finsky/layout/DetailsPlusOne$PlusOneCirclesListingDialog$PlusOneCirclesAdapter;->mPlusOneData:Lcom/google/android/finsky/protos/DocAnnotations$PlusOneData;

    invoke-virtual {v2}, Lcom/google/android/finsky/protos/DocAnnotations$PlusOneData;->getCirclesProfilesCount()I

    move-result v2

    int-to-long v2, v2

    cmp-long v0, v0, v2

    if-lez v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method


# virtual methods
.method public getCount()I
    .locals 2

    iget-object v0, p0, Lcom/google/android/finsky/layout/DetailsPlusOne$PlusOneCirclesListingDialog$PlusOneCirclesAdapter;->mPlusOneData:Lcom/google/android/finsky/protos/DocAnnotations$PlusOneData;

    invoke-virtual {v0}, Lcom/google/android/finsky/protos/DocAnnotations$PlusOneData;->getCirclesProfilesCount()I

    move-result v1

    invoke-direct {p0}, Lcom/google/android/finsky/layout/DetailsPlusOne$PlusOneCirclesListingDialog$PlusOneCirclesAdapter;->isShowingPartialList()Z

    move-result v0

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    add-int/2addr v0, v1

    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public getItem(I)Ljava/lang/Object;
    .locals 1
    .param p1    # I

    iget-object v0, p0, Lcom/google/android/finsky/layout/DetailsPlusOne$PlusOneCirclesListingDialog$PlusOneCirclesAdapter;->mPlusOneData:Lcom/google/android/finsky/protos/DocAnnotations$PlusOneData;

    invoke-virtual {v0}, Lcom/google/android/finsky/protos/DocAnnotations$PlusOneData;->getCirclesProfilesCount()I

    move-result v0

    if-ge p1, v0, :cond_0

    iget-object v0, p0, Lcom/google/android/finsky/layout/DetailsPlusOne$PlusOneCirclesListingDialog$PlusOneCirclesAdapter;->mPlusOneData:Lcom/google/android/finsky/protos/DocAnnotations$PlusOneData;

    invoke-virtual {v0, p1}, Lcom/google/android/finsky/protos/DocAnnotations$PlusOneData;->getCirclesProfiles(I)Lcom/google/android/finsky/protos/PlusData$PlusProfile;

    move-result-object v0

    :goto_0
    return-object v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public getItemId(I)J
    .locals 2
    .param p1    # I

    int-to-long v0, p1

    return-wide v0
.end method

.method public getItemViewType(I)I
    .locals 1
    .param p1    # I

    iget-object v0, p0, Lcom/google/android/finsky/layout/DetailsPlusOne$PlusOneCirclesListingDialog$PlusOneCirclesAdapter;->mPlusOneData:Lcom/google/android/finsky/protos/DocAnnotations$PlusOneData;

    invoke-virtual {v0}, Lcom/google/android/finsky/protos/DocAnnotations$PlusOneData;->getCirclesProfilesCount()I

    move-result v0

    if-ge p1, v0, :cond_0

    const/4 v0, 0x0

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x1

    goto :goto_0
.end method

.method public getView(ILandroid/view/View;Landroid/view/ViewGroup;)Landroid/view/View;
    .locals 9
    .param p1    # I
    .param p2    # Landroid/view/View;
    .param p3    # Landroid/view/ViewGroup;

    const/4 v8, 0x0

    if-nez p2, :cond_0

    iget-object v3, p0, Lcom/google/android/finsky/layout/DetailsPlusOne$PlusOneCirclesListingDialog$PlusOneCirclesAdapter;->mLayoutInflater:Landroid/view/LayoutInflater;

    const v4, 0x7f0400f1    # com.android.vending.R.layout.plus_one_profile_full_row

    invoke-virtual {v3, v4, p3, v8}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;Z)Landroid/view/View;

    move-result-object p2

    :cond_0
    invoke-virtual {p2}, Landroid/view/View;->getTag()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/finsky/layout/DetailsPlusOne$PlusOneCirclesListingDialog$CircleRowHolder;

    if-nez v0, :cond_1

    new-instance v0, Lcom/google/android/finsky/layout/DetailsPlusOne$PlusOneCirclesListingDialog$CircleRowHolder;

    invoke-direct {v0, p2}, Lcom/google/android/finsky/layout/DetailsPlusOne$PlusOneCirclesListingDialog$CircleRowHolder;-><init>(Landroid/view/View;)V

    :cond_1
    invoke-virtual {p0, p1}, Lcom/google/android/finsky/layout/DetailsPlusOne$PlusOneCirclesListingDialog$PlusOneCirclesAdapter;->getItemViewType(I)I

    move-result v3

    packed-switch v3, :pswitch_data_0

    iget-object v3, p0, Lcom/google/android/finsky/layout/DetailsPlusOne$PlusOneCirclesListingDialog$PlusOneCirclesAdapter;->mPlusOneData:Lcom/google/android/finsky/protos/DocAnnotations$PlusOneData;

    invoke-virtual {v3}, Lcom/google/android/finsky/protos/DocAnnotations$PlusOneData;->getCirclesTotal()J

    move-result-wide v3

    long-to-int v3, v3

    iget-object v4, p0, Lcom/google/android/finsky/layout/DetailsPlusOne$PlusOneCirclesListingDialog$PlusOneCirclesAdapter;->mPlusOneData:Lcom/google/android/finsky/protos/DocAnnotations$PlusOneData;

    invoke-virtual {v4}, Lcom/google/android/finsky/protos/DocAnnotations$PlusOneData;->getCirclesProfilesCount()I

    move-result v4

    sub-int v1, v3, v4

    iget-object v3, v0, Lcom/google/android/finsky/layout/DetailsPlusOne$PlusOneCirclesListingDialog$CircleRowHolder;->avatarView:Lcom/google/android/finsky/layout/FifeImageView;

    const/4 v4, 0x4

    invoke-virtual {v3, v4}, Lcom/google/android/finsky/layout/FifeImageView;->setVisibility(I)V

    iget-object v3, v0, Lcom/google/android/finsky/layout/DetailsPlusOne$PlusOneCirclesListingDialog$CircleRowHolder;->nameView:Landroid/widget/TextView;

    iget-object v4, p0, Lcom/google/android/finsky/layout/DetailsPlusOne$PlusOneCirclesListingDialog$PlusOneCirclesAdapter;->mContext:Landroid/content/Context;

    invoke-virtual {v4}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v4

    const v5, 0x7f0e0005    # com.android.vending.R.plurals.plus_one_circles_listing_dialog_more

    const/4 v6, 0x1

    new-array v6, v6, [Ljava/lang/Object;

    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v7

    aput-object v7, v6, v8

    invoke-virtual {v4, v5, v1, v6}, Landroid/content/res/Resources;->getQuantityString(II[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v3, v4}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    :goto_0
    return-object p2

    :pswitch_0
    iget-object v3, p0, Lcom/google/android/finsky/layout/DetailsPlusOne$PlusOneCirclesListingDialog$PlusOneCirclesAdapter;->mPlusOneData:Lcom/google/android/finsky/protos/DocAnnotations$PlusOneData;

    invoke-virtual {v3, p1}, Lcom/google/android/finsky/protos/DocAnnotations$PlusOneData;->getCirclesProfiles(I)Lcom/google/android/finsky/protos/PlusData$PlusProfile;

    move-result-object v2

    iget-object v3, v0, Lcom/google/android/finsky/layout/DetailsPlusOne$PlusOneCirclesListingDialog$CircleRowHolder;->avatarView:Lcom/google/android/finsky/layout/FifeImageView;

    invoke-virtual {v3, v8}, Lcom/google/android/finsky/layout/FifeImageView;->setVisibility(I)V

    iget-object v3, v0, Lcom/google/android/finsky/layout/DetailsPlusOne$PlusOneCirclesListingDialog$CircleRowHolder;->avatarView:Lcom/google/android/finsky/layout/FifeImageView;

    invoke-virtual {v2}, Lcom/google/android/finsky/protos/PlusData$PlusProfile;->getProfileImage()Lcom/google/android/finsky/protos/Doc$Image;

    move-result-object v4

    iget-object v5, p0, Lcom/google/android/finsky/layout/DetailsPlusOne$PlusOneCirclesListingDialog$PlusOneCirclesAdapter;->mBitmapLoader:Lcom/google/android/finsky/utils/BitmapLoader;

    invoke-virtual {v3, v4, v5}, Lcom/google/android/finsky/layout/FifeImageView;->setImage(Lcom/google/android/finsky/protos/Doc$Image;Lcom/google/android/finsky/utils/BitmapLoader;)V

    iget-object v3, v0, Lcom/google/android/finsky/layout/DetailsPlusOne$PlusOneCirclesListingDialog$CircleRowHolder;->nameView:Landroid/widget/TextView;

    invoke-virtual {v2}, Lcom/google/android/finsky/protos/PlusData$PlusProfile;->getDisplayName()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v3, v4}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    goto :goto_0

    nop

    :pswitch_data_0
    .packed-switch 0x0
        :pswitch_0
    .end packed-switch
.end method

.method public getViewTypeCount()I
    .locals 1

    const/4 v0, 0x2

    return v0
.end method
