.class public Lcom/google/android/finsky/layout/play/PlayEditorialNonAppCardView;
.super Lcom/google/android/finsky/layout/play/PlayEditorialCardView;
.source "PlayEditorialNonAppCardView.java"


# static fields
.field private static final IMAGE_TYPES:[I


# direct methods
.method static constructor <clinit>()V
    .locals 1

    const/4 v0, 0x3

    new-array v0, v0, [I

    fill-array-data v0, :array_0

    sput-object v0, Lcom/google/android/finsky/layout/play/PlayEditorialNonAppCardView;->IMAGE_TYPES:[I

    return-void

    nop

    :array_0
    .array-data 4
        0x4
        0x0
        0x2
    .end array-data
.end method

.method public constructor <init>(Landroid/content/Context;)V
    .locals 1
    .param p1    # Landroid/content/Context;

    const/4 v0, 0x0

    invoke-direct {p0, p1, v0}, Lcom/google/android/finsky/layout/play/PlayEditorialNonAppCardView;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;)V

    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;)V
    .locals 0
    .param p1    # Landroid/content/Context;
    .param p2    # Landroid/util/AttributeSet;

    invoke-direct {p0, p1, p2}, Lcom/google/android/finsky/layout/play/PlayEditorialCardView;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;)V

    return-void
.end method


# virtual methods
.method protected getPlayStoreUiElementType()I
    .locals 1

    const/16 v0, 0x201

    return v0
.end method

.method protected getThumbnailImageTypes()[I
    .locals 1

    sget-object v0, Lcom/google/android/finsky/layout/play/PlayEditorialNonAppCardView;->IMAGE_TYPES:[I

    return-object v0
.end method
