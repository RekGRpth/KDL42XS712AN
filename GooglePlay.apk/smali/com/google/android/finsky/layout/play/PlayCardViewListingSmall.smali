.class public Lcom/google/android/finsky/layout/play/PlayCardViewListingSmall;
.super Lcom/google/android/finsky/layout/play/PlayCardView;
.source "PlayCardViewListingSmall.java"


# instance fields
.field protected mDeviceClassWarning:Lcom/google/android/finsky/layout/play/PlayTextView;

.field protected mRatingBadgeContainer:Landroid/view/View;

.field private mShowDeviceClassWarning:Z

.field private final mTextContentHeight:I


# direct methods
.method public constructor <init>(Landroid/content/Context;)V
    .locals 1
    .param p1    # Landroid/content/Context;

    const/4 v0, 0x0

    invoke-direct {p0, p1, v0}, Lcom/google/android/finsky/layout/play/PlayCardViewListingSmall;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;)V

    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;)V
    .locals 2
    .param p1    # Landroid/content/Context;
    .param p2    # Landroid/util/AttributeSet;

    invoke-direct {p0, p1, p2}, Lcom/google/android/finsky/layout/play/PlayCardView;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;)V

    invoke-virtual {p1}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    const v1, 0x7f0b0060    # com.android.vending.R.dimen.play_listing_card_content_height

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v0

    iput v0, p0, Lcom/google/android/finsky/layout/play/PlayCardViewListingSmall;->mTextContentHeight:I

    return-void
.end method


# virtual methods
.method public bindLoading()V
    .locals 2

    invoke-super {p0}, Lcom/google/android/finsky/layout/play/PlayCardView;->bindLoading()V

    iget-object v0, p0, Lcom/google/android/finsky/layout/play/PlayCardViewListingSmall;->mDeviceClassWarning:Lcom/google/android/finsky/layout/play/PlayTextView;

    const/16 v1, 0x8

    invoke-virtual {v0, v1}, Lcom/google/android/finsky/layout/play/PlayTextView;->setVisibility(I)V

    return-void
.end method

.method public configureDeviceClassWarningDisplay(Lcom/google/android/finsky/api/model/Document;)V
    .locals 9
    .param p1    # Lcom/google/android/finsky/api/model/Document;

    const/16 v8, 0xc

    const/16 v7, 0x8

    const/4 v6, 0x4

    const/4 v5, 0x0

    invoke-virtual {p1}, Lcom/google/android/finsky/api/model/Document;->hasOptimalDeviceClassWarning()Z

    move-result v1

    iget-boolean v3, p0, Lcom/google/android/finsky/layout/play/PlayCardViewListingSmall;->mShowDeviceClassWarning:Z

    if-ne v3, v1, :cond_0

    :goto_0
    return-void

    :cond_0
    iput-boolean v1, p0, Lcom/google/android/finsky/layout/play/PlayCardViewListingSmall;->mShowDeviceClassWarning:Z

    iget-object v3, p0, Lcom/google/android/finsky/layout/play/PlayCardViewListingSmall;->mPrice:Lcom/google/android/finsky/layout/play/PlayActionButton;

    invoke-virtual {v3}, Lcom/google/android/finsky/layout/play/PlayActionButton;->getLayoutParams()Landroid/view/ViewGroup$LayoutParams;

    move-result-object v0

    check-cast v0, Landroid/widget/RelativeLayout$LayoutParams;

    iget-object v3, p0, Lcom/google/android/finsky/layout/play/PlayCardViewListingSmall;->mSubtitle:Lcom/google/android/finsky/layout/DecoratedTextView;

    invoke-virtual {v3}, Lcom/google/android/finsky/layout/DecoratedTextView;->getLayoutParams()Landroid/view/ViewGroup$LayoutParams;

    move-result-object v2

    check-cast v2, Landroid/widget/RelativeLayout$LayoutParams;

    iget-boolean v3, p0, Lcom/google/android/finsky/layout/play/PlayCardViewListingSmall;->mShowDeviceClassWarning:Z

    if-eqz v3, :cond_1

    iget-object v3, p0, Lcom/google/android/finsky/layout/play/PlayCardViewListingSmall;->mTitle:Landroid/widget/TextView;

    const/4 v4, 0x1

    invoke-virtual {v3, v4}, Landroid/widget/TextView;->setMaxLines(I)V

    iget-object v3, p0, Lcom/google/android/finsky/layout/play/PlayCardViewListingSmall;->mDeviceClassWarning:Lcom/google/android/finsky/layout/play/PlayTextView;

    invoke-virtual {v3, v5}, Lcom/google/android/finsky/layout/play/PlayTextView;->setVisibility(I)V

    iget-object v3, p0, Lcom/google/android/finsky/layout/play/PlayCardViewListingSmall;->mDeviceClassWarning:Lcom/google/android/finsky/layout/play/PlayTextView;

    invoke-virtual {p1}, Lcom/google/android/finsky/api/model/Document;->getOptimalDeviceClassWarning()Lcom/google/android/finsky/protos/DocAnnotations$Warning;

    move-result-object v4

    invoke-virtual {v4}, Lcom/google/android/finsky/protos/DocAnnotations$Warning;->getLocalizedMessage()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v3, v4}, Lcom/google/android/finsky/layout/play/PlayTextView;->setText(Ljava/lang/CharSequence;)V

    iget-object v3, p0, Lcom/google/android/finsky/layout/play/PlayCardViewListingSmall;->mRatingBadgeContainer:Landroid/view/View;

    invoke-virtual {v3, v7}, Landroid/view/View;->setVisibility(I)V

    invoke-virtual {v0, v8, v5}, Landroid/widget/RelativeLayout$LayoutParams;->addRule(II)V

    iget-object v3, p0, Lcom/google/android/finsky/layout/play/PlayCardViewListingSmall;->mSubtitle:Lcom/google/android/finsky/layout/DecoratedTextView;

    invoke-virtual {v3}, Lcom/google/android/finsky/layout/DecoratedTextView;->getId()I

    move-result v3

    invoke-virtual {v0, v6, v3}, Landroid/widget/RelativeLayout$LayoutParams;->addRule(II)V

    iget-object v3, p0, Lcom/google/android/finsky/layout/play/PlayCardViewListingSmall;->mPrice:Lcom/google/android/finsky/layout/play/PlayActionButton;

    invoke-virtual {v3}, Lcom/google/android/finsky/layout/play/PlayActionButton;->getId()I

    move-result v3

    invoke-virtual {v2, v5, v3}, Landroid/widget/RelativeLayout$LayoutParams;->addRule(II)V

    :goto_1
    invoke-virtual {p0}, Lcom/google/android/finsky/layout/play/PlayCardViewListingSmall;->requestLayout()V

    goto :goto_0

    :cond_1
    iget-object v3, p0, Lcom/google/android/finsky/layout/play/PlayCardViewListingSmall;->mTitle:Landroid/widget/TextView;

    const/4 v4, 0x2

    invoke-virtual {v3, v4}, Landroid/widget/TextView;->setMaxLines(I)V

    iget-object v3, p0, Lcom/google/android/finsky/layout/play/PlayCardViewListingSmall;->mDeviceClassWarning:Lcom/google/android/finsky/layout/play/PlayTextView;

    invoke-virtual {v3, v7}, Lcom/google/android/finsky/layout/play/PlayTextView;->setVisibility(I)V

    iget-object v3, p0, Lcom/google/android/finsky/layout/play/PlayCardViewListingSmall;->mRatingBadgeContainer:Landroid/view/View;

    invoke-virtual {v3, v5}, Landroid/view/View;->setVisibility(I)V

    const/4 v3, -0x1

    invoke-virtual {v0, v8, v3}, Landroid/widget/RelativeLayout$LayoutParams;->addRule(II)V

    invoke-virtual {v0, v6, v5}, Landroid/widget/RelativeLayout$LayoutParams;->addRule(II)V

    invoke-virtual {v2, v5, v5}, Landroid/widget/RelativeLayout$LayoutParams;->addRule(II)V

    goto :goto_1
.end method

.method protected getPlayStoreUiElementType()I
    .locals 1

    const/16 v0, 0x1fa

    return v0
.end method

.method protected onFinishInflate()V
    .locals 1

    invoke-super {p0}, Lcom/google/android/finsky/layout/play/PlayCardView;->onFinishInflate()V

    const v0, 0x7f0801c4    # com.android.vending.R.id.rating_badge_container

    invoke-virtual {p0, v0}, Lcom/google/android/finsky/layout/play/PlayCardViewListingSmall;->findViewById(I)Landroid/view/View;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/finsky/layout/play/PlayCardViewListingSmall;->mRatingBadgeContainer:Landroid/view/View;

    const v0, 0x7f0801c8    # com.android.vending.R.id.li_device_class_warning

    invoke-virtual {p0, v0}, Lcom/google/android/finsky/layout/play/PlayCardViewListingSmall;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/google/android/finsky/layout/play/PlayTextView;

    iput-object v0, p0, Lcom/google/android/finsky/layout/play/PlayCardViewListingSmall;->mDeviceClassWarning:Lcom/google/android/finsky/layout/play/PlayTextView;

    return-void
.end method

.method protected onLayout(ZIIII)V
    .locals 5
    .param p1    # Z
    .param p2    # I
    .param p3    # I
    .param p4    # I
    .param p5    # I

    invoke-super/range {p0 .. p5}, Lcom/google/android/finsky/layout/play/PlayCardView;->onLayout(ZIIII)V

    iget-boolean v0, p0, Lcom/google/android/finsky/layout/play/PlayCardViewListingSmall;->mShowDeviceClassWarning:Z

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/google/android/finsky/layout/play/PlayCardViewListingSmall;->mSubtitle:Lcom/google/android/finsky/layout/DecoratedTextView;

    iget-object v1, p0, Lcom/google/android/finsky/layout/play/PlayCardViewListingSmall;->mSubtitle:Lcom/google/android/finsky/layout/DecoratedTextView;

    invoke-virtual {v1}, Lcom/google/android/finsky/layout/DecoratedTextView;->getLeft()I

    move-result v1

    iget-object v2, p0, Lcom/google/android/finsky/layout/play/PlayCardViewListingSmall;->mSubtitle:Lcom/google/android/finsky/layout/DecoratedTextView;

    invoke-virtual {v2}, Lcom/google/android/finsky/layout/DecoratedTextView;->getTop()I

    move-result v2

    iget-object v3, p0, Lcom/google/android/finsky/layout/play/PlayCardViewListingSmall;->mSubtitle:Lcom/google/android/finsky/layout/DecoratedTextView;

    invoke-virtual {v3}, Lcom/google/android/finsky/layout/DecoratedTextView;->getLeft()I

    move-result v3

    iget-object v4, p0, Lcom/google/android/finsky/layout/play/PlayCardViewListingSmall;->mSubtitle:Lcom/google/android/finsky/layout/DecoratedTextView;

    invoke-virtual {v4}, Lcom/google/android/finsky/layout/DecoratedTextView;->getMeasuredWidth()I

    move-result v4

    add-int/2addr v3, v4

    iget-object v4, p0, Lcom/google/android/finsky/layout/play/PlayCardViewListingSmall;->mSubtitle:Lcom/google/android/finsky/layout/DecoratedTextView;

    invoke-virtual {v4}, Lcom/google/android/finsky/layout/DecoratedTextView;->getBottom()I

    move-result v4

    invoke-virtual {v0, v1, v2, v3, v4}, Lcom/google/android/finsky/layout/DecoratedTextView;->layout(IIII)V

    :cond_0
    return-void
.end method

.method protected onMeasure(II)V
    .locals 11
    .param p1    # I
    .param p2    # I

    const/high16 v10, 0x40000000    # 2.0f

    const/4 v9, 0x0

    invoke-virtual {p0, p1}, Lcom/google/android/finsky/layout/play/PlayCardViewListingSmall;->measureThumbnailSpanningWidth(I)V

    iget-object v6, p0, Lcom/google/android/finsky/layout/play/PlayCardViewListingSmall;->mThumbnail:Lcom/google/android/finsky/layout/play/PlayCardThumbnail;

    invoke-virtual {v6}, Lcom/google/android/finsky/layout/play/PlayCardThumbnail;->getLayoutParams()Landroid/view/ViewGroup$LayoutParams;

    move-result-object v5

    check-cast v5, Landroid/view/ViewGroup$MarginLayoutParams;

    iget v6, v5, Landroid/view/ViewGroup$MarginLayoutParams;->height:I

    iget v7, p0, Lcom/google/android/finsky/layout/play/PlayCardViewListingSmall;->mTextContentHeight:I

    add-int/2addr v6, v7

    invoke-virtual {p0}, Lcom/google/android/finsky/layout/play/PlayCardViewListingSmall;->getPaddingTop()I

    move-result v7

    add-int/2addr v6, v7

    invoke-virtual {p0}, Lcom/google/android/finsky/layout/play/PlayCardViewListingSmall;->getPaddingBottom()I

    move-result v7

    add-int v0, v6, v7

    invoke-static {v0, v10}, Landroid/view/View$MeasureSpec;->makeMeasureSpec(II)I

    move-result v6

    invoke-super {p0, p1, v6}, Lcom/google/android/finsky/layout/play/PlayCardView;->onMeasure(II)V

    iget-boolean v6, p0, Lcom/google/android/finsky/layout/play/PlayCardViewListingSmall;->mShowDeviceClassWarning:Z

    if-eqz v6, :cond_0

    iget-object v6, p0, Lcom/google/android/finsky/layout/play/PlayCardViewListingSmall;->mPrice:Lcom/google/android/finsky/layout/play/PlayActionButton;

    invoke-virtual {v6, v9, v9}, Lcom/google/android/finsky/layout/play/PlayActionButton;->measure(II)V

    iget-object v6, p0, Lcom/google/android/finsky/layout/play/PlayCardViewListingSmall;->mSubtitle:Lcom/google/android/finsky/layout/DecoratedTextView;

    invoke-virtual {v6, v9, v9}, Lcom/google/android/finsky/layout/DecoratedTextView;->measure(II)V

    iget-object v6, p0, Lcom/google/android/finsky/layout/play/PlayCardViewListingSmall;->mSubtitle:Lcom/google/android/finsky/layout/DecoratedTextView;

    invoke-virtual {v6}, Lcom/google/android/finsky/layout/DecoratedTextView;->getLayoutParams()Landroid/view/ViewGroup$LayoutParams;

    move-result-object v3

    check-cast v3, Landroid/view/ViewGroup$MarginLayoutParams;

    iget-object v6, p0, Lcom/google/android/finsky/layout/play/PlayCardViewListingSmall;->mOverflow:Lcom/google/android/finsky/layout/play/PlayCardOverflowView;

    invoke-virtual {v6}, Lcom/google/android/finsky/layout/play/PlayCardOverflowView;->getLayoutParams()Landroid/view/ViewGroup$LayoutParams;

    move-result-object v1

    check-cast v1, Landroid/view/ViewGroup$MarginLayoutParams;

    iget-object v6, p0, Lcom/google/android/finsky/layout/play/PlayCardViewListingSmall;->mPrice:Lcom/google/android/finsky/layout/play/PlayActionButton;

    invoke-virtual {v6}, Lcom/google/android/finsky/layout/play/PlayActionButton;->getLayoutParams()Landroid/view/ViewGroup$LayoutParams;

    move-result-object v2

    check-cast v2, Landroid/view/ViewGroup$MarginLayoutParams;

    iget-object v6, p0, Lcom/google/android/finsky/layout/play/PlayCardViewListingSmall;->mSubtitle:Lcom/google/android/finsky/layout/DecoratedTextView;

    invoke-virtual {v6}, Lcom/google/android/finsky/layout/DecoratedTextView;->getMeasuredWidth()I

    move-result v6

    invoke-static {p1}, Landroid/view/View$MeasureSpec;->getSize(I)I

    move-result v7

    invoke-virtual {p0}, Lcom/google/android/finsky/layout/play/PlayCardViewListingSmall;->getPaddingLeft()I

    move-result v8

    sub-int/2addr v7, v8

    invoke-virtual {p0}, Lcom/google/android/finsky/layout/play/PlayCardViewListingSmall;->getPaddingRight()I

    move-result v8

    sub-int/2addr v7, v8

    iget v8, v3, Landroid/view/ViewGroup$MarginLayoutParams;->leftMargin:I

    sub-int/2addr v7, v8

    iget v8, v1, Landroid/view/ViewGroup$MarginLayoutParams;->rightMargin:I

    sub-int/2addr v7, v8

    iget v8, v2, Landroid/view/ViewGroup$MarginLayoutParams;->leftMargin:I

    sub-int/2addr v7, v8

    iget v8, v2, Landroid/view/ViewGroup$MarginLayoutParams;->rightMargin:I

    sub-int/2addr v7, v8

    iget-object v8, p0, Lcom/google/android/finsky/layout/play/PlayCardViewListingSmall;->mPrice:Lcom/google/android/finsky/layout/play/PlayActionButton;

    invoke-virtual {v8}, Lcom/google/android/finsky/layout/play/PlayActionButton;->getMeasuredWidth()I

    move-result v8

    sub-int/2addr v7, v8

    invoke-static {v6, v7}, Ljava/lang/Math;->min(II)I

    move-result v4

    iget-object v6, p0, Lcom/google/android/finsky/layout/play/PlayCardViewListingSmall;->mSubtitle:Lcom/google/android/finsky/layout/DecoratedTextView;

    invoke-static {v4, v10}, Landroid/view/View$MeasureSpec;->makeMeasureSpec(II)I

    move-result v7

    invoke-virtual {v6, v7, v9}, Lcom/google/android/finsky/layout/DecoratedTextView;->measure(II)V

    :cond_0
    return-void
.end method
