.class public Lcom/google/android/finsky/layout/play/PlayCardHeap;
.super Ljava/lang/Object;
.source "PlayCardHeap.java"


# instance fields
.field private final mHeap:Landroid/util/SparseArray;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Landroid/util/SparseArray",
            "<",
            "Ljava/util/List",
            "<",
            "Lcom/google/android/finsky/layout/play/PlayCardView;",
            ">;>;"
        }
    .end annotation
.end field


# direct methods
.method public constructor <init>()V
    .locals 1

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    new-instance v0, Landroid/util/SparseArray;

    invoke-direct {v0}, Landroid/util/SparseArray;-><init>()V

    iput-object v0, p0, Lcom/google/android/finsky/layout/play/PlayCardHeap;->mHeap:Landroid/util/SparseArray;

    return-void
.end method


# virtual methods
.method public getCard(Lcom/google/android/finsky/layout/play/PlayCardClusterMetadata$CardMetadata;Landroid/view/LayoutInflater;Z)Lcom/google/android/finsky/layout/play/PlayCardView;
    .locals 4
    .param p1    # Lcom/google/android/finsky/layout/play/PlayCardClusterMetadata$CardMetadata;
    .param p2    # Landroid/view/LayoutInflater;
    .param p3    # Z

    const/4 v3, 0x0

    invoke-static {}, Lcom/google/android/finsky/utils/Utils;->ensureOnMainThread()V

    invoke-virtual {p1}, Lcom/google/android/finsky/layout/play/PlayCardClusterMetadata$CardMetadata;->hasNoReasonLayoutId()Z

    move-result v2

    if-eqz v2, :cond_1

    if-nez p3, :cond_1

    invoke-virtual {p1}, Lcom/google/android/finsky/layout/play/PlayCardClusterMetadata$CardMetadata;->getNoReasonLayoutId()I

    move-result v1

    :goto_0
    iget-object v2, p0, Lcom/google/android/finsky/layout/play/PlayCardHeap;->mHeap:Landroid/util/SparseArray;

    invoke-virtual {v2, v1}, Landroid/util/SparseArray;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/util/List;

    if-nez v0, :cond_0

    invoke-static {}, Lcom/google/android/finsky/utils/Lists;->newArrayList()Ljava/util/ArrayList;

    move-result-object v0

    iget-object v2, p0, Lcom/google/android/finsky/layout/play/PlayCardHeap;->mHeap:Landroid/util/SparseArray;

    invoke-virtual {v2, v1, v0}, Landroid/util/SparseArray;->put(ILjava/lang/Object;)V

    :cond_0
    invoke-interface {v0}, Ljava/util/List;->isEmpty()Z

    move-result v2

    if-eqz v2, :cond_2

    const/4 v2, 0x0

    invoke-virtual {p2, v1, v2, v3}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;Z)Landroid/view/View;

    move-result-object v2

    check-cast v2, Lcom/google/android/finsky/layout/play/PlayCardView;

    :goto_1
    return-object v2

    :cond_1
    invoke-virtual {p1}, Lcom/google/android/finsky/layout/play/PlayCardClusterMetadata$CardMetadata;->getLayoutId()I

    move-result v1

    goto :goto_0

    :cond_2
    invoke-interface {v0, v3}, Ljava/util/List;->remove(I)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lcom/google/android/finsky/layout/play/PlayCardView;

    goto :goto_1
.end method

.method public recycle(Lcom/google/android/finsky/layout/play/PlayCardClusterView;)V
    .locals 7
    .param p1    # Lcom/google/android/finsky/layout/play/PlayCardClusterView;

    invoke-static {}, Lcom/google/android/finsky/utils/Utils;->ensureOnMainThread()V

    if-eqz p1, :cond_0

    invoke-virtual {p1}, Lcom/google/android/finsky/layout/play/PlayCardClusterView;->hasCards()Z

    move-result v6

    if-nez v6, :cond_1

    :cond_0
    :goto_0
    return-void

    :cond_1
    invoke-virtual {p1}, Lcom/google/android/finsky/layout/play/PlayCardClusterView;->getMetadata()Lcom/google/android/finsky/layout/play/PlayCardClusterMetadata;

    move-result-object v2

    invoke-virtual {v2}, Lcom/google/android/finsky/layout/play/PlayCardClusterMetadata;->getTileCount()I

    move-result v4

    const/4 v5, 0x0

    :goto_1
    if-ge v5, v4, :cond_5

    invoke-virtual {p1, v5}, Lcom/google/android/finsky/layout/play/PlayCardClusterView;->getCardChild(I)Lcom/google/android/finsky/layout/play/PlayCardView;

    move-result-object v0

    invoke-virtual {v2, v5}, Lcom/google/android/finsky/layout/play/PlayCardClusterMetadata;->getTileMetadata(I)Lcom/google/android/finsky/layout/play/PlayCardClusterMetadata$ClusterTileMetadata;

    move-result-object v6

    invoke-virtual {v6}, Lcom/google/android/finsky/layout/play/PlayCardClusterMetadata$ClusterTileMetadata;->getCardMetadata()Lcom/google/android/finsky/layout/play/PlayCardClusterMetadata$CardMetadata;

    move-result-object v1

    if-eqz v0, :cond_3

    invoke-virtual {v0}, Lcom/google/android/finsky/layout/play/PlayCardView;->resetUiElementNode()V

    invoke-virtual {v0}, Lcom/google/android/finsky/layout/play/PlayCardView;->supportsDisplayingReason()Z

    move-result v6

    if-nez v6, :cond_2

    invoke-virtual {v1}, Lcom/google/android/finsky/layout/play/PlayCardClusterMetadata$CardMetadata;->hasNoReasonLayoutId()Z

    move-result v6

    if-nez v6, :cond_4

    :cond_2
    invoke-virtual {v1}, Lcom/google/android/finsky/layout/play/PlayCardClusterMetadata$CardMetadata;->getLayoutId()I

    move-result v3

    :goto_2
    iget-object v6, p0, Lcom/google/android/finsky/layout/play/PlayCardHeap;->mHeap:Landroid/util/SparseArray;

    invoke-virtual {v6, v3}, Landroid/util/SparseArray;->get(I)Ljava/lang/Object;

    move-result-object v6

    check-cast v6, Ljava/util/List;

    invoke-interface {v6, v0}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    :cond_3
    add-int/lit8 v5, v5, 0x1

    goto :goto_1

    :cond_4
    invoke-virtual {v1}, Lcom/google/android/finsky/layout/play/PlayCardClusterMetadata$CardMetadata;->getNoReasonLayoutId()I

    move-result v3

    goto :goto_2

    :cond_5
    invoke-virtual {p1}, Lcom/google/android/finsky/layout/play/PlayCardClusterView;->removeAllCards()V

    goto :goto_0
.end method
