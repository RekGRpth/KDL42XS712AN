.class public Lcom/google/android/finsky/layout/play/PlayCardOverflowView;
.super Landroid/widget/ImageView;
.source "PlayCardOverflowView.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/google/android/finsky/layout/play/PlayCardOverflowView$CardDismissalAction;
    }
.end annotation


# instance fields
.field private final mPriceStylingInfo:Lcom/google/android/finsky/utils/PurchaseButtonHelper$PriceStylingInfo;


# direct methods
.method public constructor <init>(Landroid/content/Context;)V
    .locals 1
    .param p1    # Landroid/content/Context;

    const/4 v0, 0x0

    invoke-direct {p0, p1, v0}, Lcom/google/android/finsky/layout/play/PlayCardOverflowView;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;)V

    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;)V
    .locals 1
    .param p1    # Landroid/content/Context;
    .param p2    # Landroid/util/AttributeSet;

    invoke-direct {p0, p1, p2}, Landroid/widget/ImageView;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;)V

    new-instance v0, Lcom/google/android/finsky/utils/PurchaseButtonHelper$PriceStylingInfo;

    invoke-direct {v0}, Lcom/google/android/finsky/utils/PurchaseButtonHelper$PriceStylingInfo;-><init>()V

    iput-object v0, p0, Lcom/google/android/finsky/layout/play/PlayCardOverflowView;->mPriceStylingInfo:Lcom/google/android/finsky/utils/PurchaseButtonHelper$PriceStylingInfo;

    return-void
.end method

.method static synthetic access$200(Lcom/google/android/finsky/layout/play/PlayCardOverflowView;Lcom/google/android/finsky/layout/play/PlayPopupMenu;Landroid/content/Context;Lcom/google/android/finsky/api/model/Document;Landroid/accounts/Account;Lcom/google/android/finsky/navigationmanager/NavigationManager;Ljava/lang/String;Lcom/google/android/finsky/layout/play/PlayStoreUiElementNode;)V
    .locals 0
    .param p0    # Lcom/google/android/finsky/layout/play/PlayCardOverflowView;
    .param p1    # Lcom/google/android/finsky/layout/play/PlayPopupMenu;
    .param p2    # Landroid/content/Context;
    .param p3    # Lcom/google/android/finsky/api/model/Document;
    .param p4    # Landroid/accounts/Account;
    .param p5    # Lcom/google/android/finsky/navigationmanager/NavigationManager;
    .param p6    # Ljava/lang/String;
    .param p7    # Lcom/google/android/finsky/layout/play/PlayStoreUiElementNode;

    invoke-direct/range {p0 .. p7}, Lcom/google/android/finsky/layout/play/PlayCardOverflowView;->configureMenuPrice(Lcom/google/android/finsky/layout/play/PlayPopupMenu;Landroid/content/Context;Lcom/google/android/finsky/api/model/Document;Landroid/accounts/Account;Lcom/google/android/finsky/navigationmanager/NavigationManager;Ljava/lang/String;Lcom/google/android/finsky/layout/play/PlayStoreUiElementNode;)V

    return-void
.end method

.method private configureMenuPrice(Lcom/google/android/finsky/layout/play/PlayPopupMenu;Landroid/content/Context;Lcom/google/android/finsky/api/model/Document;Landroid/accounts/Account;Lcom/google/android/finsky/navigationmanager/NavigationManager;Ljava/lang/String;Lcom/google/android/finsky/layout/play/PlayStoreUiElementNode;)V
    .locals 15
    .param p1    # Lcom/google/android/finsky/layout/play/PlayPopupMenu;
    .param p2    # Landroid/content/Context;
    .param p3    # Lcom/google/android/finsky/api/model/Document;
    .param p4    # Landroid/accounts/Account;
    .param p5    # Lcom/google/android/finsky/navigationmanager/NavigationManager;
    .param p6    # Ljava/lang/String;
    .param p7    # Lcom/google/android/finsky/layout/play/PlayStoreUiElementNode;

    invoke-static {}, Lcom/google/android/finsky/FinskyApp;->get()Lcom/google/android/finsky/FinskyApp;

    move-result-object v11

    invoke-virtual {v11}, Lcom/google/android/finsky/FinskyApp;->getCurrentAccount()Landroid/accounts/Account;

    move-result-object v1

    invoke-virtual {v11}, Lcom/google/android/finsky/FinskyApp;->getInstaller()Lcom/google/android/finsky/receivers/Installer;

    move-result-object v2

    invoke-virtual {v11}, Lcom/google/android/finsky/FinskyApp;->getLibraries()Lcom/google/android/finsky/library/Libraries;

    move-result-object v3

    invoke-virtual {v11}, Lcom/google/android/finsky/FinskyApp;->getAppStates()Lcom/google/android/finsky/appstate/AppStates;

    move-result-object v4

    invoke-virtual {v11}, Lcom/google/android/finsky/FinskyApp;->getToc()Lcom/google/android/finsky/api/model/DfeToc;

    move-result-object v5

    iget-object v7, p0, Lcom/google/android/finsky/layout/play/PlayCardOverflowView;->mPriceStylingInfo:Lcom/google/android/finsky/utils/PurchaseButtonHelper$PriceStylingInfo;

    move-object/from16 v6, p3

    invoke-static/range {v1 .. v7}, Lcom/google/android/finsky/utils/PurchaseButtonHelper;->stylePurchaseButton(Landroid/accounts/Account;Lcom/google/android/finsky/receivers/Installer;Lcom/google/android/finsky/library/Libraries;Lcom/google/android/finsky/appstate/AppStates;Lcom/google/android/finsky/api/model/DfeToc;Lcom/google/android/finsky/api/model/Document;Lcom/google/android/finsky/utils/PurchaseButtonHelper$PriceStylingInfo;)V

    iget-object v1, p0, Lcom/google/android/finsky/layout/play/PlayCardOverflowView;->mPriceStylingInfo:Lcom/google/android/finsky/utils/PurchaseButtonHelper$PriceStylingInfo;

    iget-object v1, v1, Lcom/google/android/finsky/utils/PurchaseButtonHelper$PriceStylingInfo;->labelOffer:Lcom/google/android/finsky/protos/Common$Offer;

    if-nez v1, :cond_0

    iget-object v1, p0, Lcom/google/android/finsky/layout/play/PlayCardOverflowView;->mPriceStylingInfo:Lcom/google/android/finsky/utils/PurchaseButtonHelper$PriceStylingInfo;

    iget v1, v1, Lcom/google/android/finsky/utils/PurchaseButtonHelper$PriceStylingInfo;->labelActionResourceId:I

    const/4 v2, -0x1

    if-eq v1, v2, :cond_1

    :cond_0
    const/4 v13, 0x0

    iget-object v1, p0, Lcom/google/android/finsky/layout/play/PlayCardOverflowView;->mPriceStylingInfo:Lcom/google/android/finsky/utils/PurchaseButtonHelper$PriceStylingInfo;

    iget-object v1, v1, Lcom/google/android/finsky/utils/PurchaseButtonHelper$PriceStylingInfo;->labelOffer:Lcom/google/android/finsky/protos/Common$Offer;

    if-eqz v1, :cond_3

    iget-object v1, p0, Lcom/google/android/finsky/layout/play/PlayCardOverflowView;->mPriceStylingInfo:Lcom/google/android/finsky/utils/PurchaseButtonHelper$PriceStylingInfo;

    iget-object v1, v1, Lcom/google/android/finsky/utils/PurchaseButtonHelper$PriceStylingInfo;->labelOffer:Lcom/google/android/finsky/protos/Common$Offer;

    invoke-virtual {v1}, Lcom/google/android/finsky/protos/Common$Offer;->getFormattedAmount()Ljava/lang/String;

    move-result-object v12

    iget-object v1, p0, Lcom/google/android/finsky/layout/play/PlayCardOverflowView;->mPriceStylingInfo:Lcom/google/android/finsky/utils/PurchaseButtonHelper$PriceStylingInfo;

    iget v1, v1, Lcom/google/android/finsky/utils/PurchaseButtonHelper$PriceStylingInfo;->labelActionResourceId:I

    const/4 v2, -0x1

    if-ne v1, v2, :cond_2

    move-object v13, v12

    :goto_0
    invoke-virtual {v11}, Lcom/google/android/finsky/FinskyApp;->getCurrentAccount()Landroid/accounts/Account;

    move-result-object v1

    invoke-virtual {v11}, Lcom/google/android/finsky/FinskyApp;->getInstaller()Lcom/google/android/finsky/receivers/Installer;

    move-result-object v2

    invoke-virtual {v11}, Lcom/google/android/finsky/FinskyApp;->getLibraries()Lcom/google/android/finsky/library/Libraries;

    move-result-object v3

    invoke-virtual {v11}, Lcom/google/android/finsky/FinskyApp;->getAppStates()Lcom/google/android/finsky/appstate/AppStates;

    move-result-object v4

    invoke-virtual {v11}, Lcom/google/android/finsky/FinskyApp;->getToc()Lcom/google/android/finsky/api/model/DfeToc;

    move-result-object v5

    move-object/from16 v6, p3

    move-object/from16 v7, p4

    move-object/from16 v8, p5

    move-object/from16 v9, p6

    move-object/from16 v10, p7

    invoke-static/range {v1 .. v10}, Lcom/google/android/finsky/utils/PurchaseButtonHelper;->getPurchaseOrOpenClickListener(Landroid/accounts/Account;Lcom/google/android/finsky/receivers/Installer;Lcom/google/android/finsky/library/Libraries;Lcom/google/android/finsky/appstate/AppStates;Lcom/google/android/finsky/api/model/DfeToc;Lcom/google/android/finsky/api/model/Document;Landroid/accounts/Account;Lcom/google/android/finsky/navigationmanager/NavigationManager;Ljava/lang/String;Lcom/google/android/finsky/layout/play/PlayStoreUiElementNode;)Landroid/view/View$OnClickListener;

    move-result-object v14

    if-eqz v14, :cond_1

    iget-object v1, p0, Lcom/google/android/finsky/layout/play/PlayCardOverflowView;->mPriceStylingInfo:Lcom/google/android/finsky/utils/PurchaseButtonHelper$PriceStylingInfo;

    iget-boolean v1, v1, Lcom/google/android/finsky/utils/PurchaseButtonHelper$PriceStylingInfo;->isActionEnabled:Z

    new-instance v2, Lcom/google/android/finsky/layout/play/PlayCardOverflowView$2;

    invoke-direct {v2, p0, v14}, Lcom/google/android/finsky/layout/play/PlayCardOverflowView$2;-><init>(Lcom/google/android/finsky/layout/play/PlayCardOverflowView;Landroid/view/View$OnClickListener;)V

    move-object/from16 v0, p1

    invoke-interface {v0, v13, v1, v2}, Lcom/google/android/finsky/layout/play/PlayPopupMenu;->addMenuItem(Ljava/lang/CharSequence;ZLcom/google/android/finsky/layout/play/PlayPopupMenu$OnActionSelectedListener;)V

    :cond_1
    return-void

    :cond_2
    iget-object v1, p0, Lcom/google/android/finsky/layout/play/PlayCardOverflowView;->mPriceStylingInfo:Lcom/google/android/finsky/utils/PurchaseButtonHelper$PriceStylingInfo;

    iget v1, v1, Lcom/google/android/finsky/utils/PurchaseButtonHelper$PriceStylingInfo;->labelActionResourceId:I

    const/4 v2, 0x1

    new-array v2, v2, [Ljava/lang/Object;

    const/4 v3, 0x0

    aput-object v12, v2, v3

    move-object/from16 v0, p2

    invoke-virtual {v0, v1, v2}, Landroid/content/Context;->getString(I[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v13

    goto :goto_0

    :cond_3
    iget-object v1, p0, Lcom/google/android/finsky/layout/play/PlayCardOverflowView;->mPriceStylingInfo:Lcom/google/android/finsky/utils/PurchaseButtonHelper$PriceStylingInfo;

    iget v1, v1, Lcom/google/android/finsky/utils/PurchaseButtonHelper$PriceStylingInfo;->labelActionResourceId:I

    move-object/from16 v0, p2

    invoke-virtual {v0, v1}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v13

    goto :goto_0
.end method


# virtual methods
.method public configure(Landroid/content/Context;Lcom/google/android/finsky/api/model/Document;Lcom/google/android/finsky/api/DfeApi;Landroid/accounts/Account;Lcom/google/android/finsky/navigationmanager/NavigationManager;Ljava/lang/String;Lcom/google/android/finsky/utils/WishlistHelper$WishlistStatusListener;Lcom/google/android/finsky/layout/play/PlayCardView$OnDismissListener;Lcom/google/android/finsky/layout/play/PlayStoreUiElementNode;)V
    .locals 11
    .param p1    # Landroid/content/Context;
    .param p2    # Lcom/google/android/finsky/api/model/Document;
    .param p3    # Lcom/google/android/finsky/api/DfeApi;
    .param p4    # Landroid/accounts/Account;
    .param p5    # Lcom/google/android/finsky/navigationmanager/NavigationManager;
    .param p6    # Ljava/lang/String;
    .param p7    # Lcom/google/android/finsky/utils/WishlistHelper$WishlistStatusListener;
    .param p8    # Lcom/google/android/finsky/layout/play/PlayCardView$OnDismissListener;
    .param p9    # Lcom/google/android/finsky/layout/play/PlayStoreUiElementNode;

    new-instance v0, Lcom/google/android/finsky/layout/play/PlayCardOverflowView$1;

    move-object v1, p0

    move-object/from16 v2, p9

    move-object v3, p2

    move-object v4, p3

    move-object v5, p4

    move-object/from16 v6, p7

    move-object/from16 v7, p6

    move-object/from16 v8, p8

    move-object/from16 v9, p5

    move-object v10, p1

    invoke-direct/range {v0 .. v10}, Lcom/google/android/finsky/layout/play/PlayCardOverflowView$1;-><init>(Lcom/google/android/finsky/layout/play/PlayCardOverflowView;Lcom/google/android/finsky/layout/play/PlayStoreUiElementNode;Lcom/google/android/finsky/api/model/Document;Lcom/google/android/finsky/api/DfeApi;Landroid/accounts/Account;Lcom/google/android/finsky/utils/WishlistHelper$WishlistStatusListener;Ljava/lang/String;Lcom/google/android/finsky/layout/play/PlayCardView$OnDismissListener;Lcom/google/android/finsky/navigationmanager/NavigationManager;Landroid/content/Context;)V

    invoke-virtual {p0, v0}, Lcom/google/android/finsky/layout/play/PlayCardOverflowView;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    return-void
.end method
