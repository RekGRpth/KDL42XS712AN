.class public Lcom/google/android/finsky/layout/play/PlayCardMerchClusterMetadata;
.super Lcom/google/android/finsky/layout/play/PlayCardClusterMetadata;
.source "PlayCardMerchClusterMetadata.java"


# instance fields
.field private mMerchImageHSpan:I


# direct methods
.method public constructor <init>(II)V
    .locals 0
    .param p1    # I
    .param p2    # I

    invoke-direct {p0, p1, p2}, Lcom/google/android/finsky/layout/play/PlayCardClusterMetadata;-><init>(II)V

    return-void
.end method


# virtual methods
.method public bridge synthetic addTile(Lcom/google/android/finsky/layout/play/PlayCardClusterMetadata$CardMetadata;II)Lcom/google/android/finsky/layout/play/PlayCardClusterMetadata;
    .locals 1
    .param p1    # Lcom/google/android/finsky/layout/play/PlayCardClusterMetadata$CardMetadata;
    .param p2    # I
    .param p3    # I

    invoke-virtual {p0, p1, p2, p3}, Lcom/google/android/finsky/layout/play/PlayCardMerchClusterMetadata;->addTile(Lcom/google/android/finsky/layout/play/PlayCardClusterMetadata$CardMetadata;II)Lcom/google/android/finsky/layout/play/PlayCardMerchClusterMetadata;

    move-result-object v0

    return-object v0
.end method

.method public addTile(Lcom/google/android/finsky/layout/play/PlayCardClusterMetadata$CardMetadata;II)Lcom/google/android/finsky/layout/play/PlayCardMerchClusterMetadata;
    .locals 0
    .param p1    # Lcom/google/android/finsky/layout/play/PlayCardClusterMetadata$CardMetadata;
    .param p2    # I
    .param p3    # I

    invoke-super {p0, p1, p2, p3}, Lcom/google/android/finsky/layout/play/PlayCardClusterMetadata;->addTile(Lcom/google/android/finsky/layout/play/PlayCardClusterMetadata$CardMetadata;II)Lcom/google/android/finsky/layout/play/PlayCardClusterMetadata;

    return-object p0
.end method

.method public getLeadingGap()I
    .locals 1

    iget v0, p0, Lcom/google/android/finsky/layout/play/PlayCardMerchClusterMetadata;->mMerchImageHSpan:I

    return v0
.end method

.method public withMerchImageHSpan(I)Lcom/google/android/finsky/layout/play/PlayCardMerchClusterMetadata;
    .locals 0
    .param p1    # I

    iput p1, p0, Lcom/google/android/finsky/layout/play/PlayCardMerchClusterMetadata;->mMerchImageHSpan:I

    return-object p0
.end method
