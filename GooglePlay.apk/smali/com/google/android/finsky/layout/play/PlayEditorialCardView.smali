.class public abstract Lcom/google/android/finsky/layout/play/PlayEditorialCardView;
.super Lcom/google/android/finsky/layout/play/PlayCardView;
.source "PlayEditorialCardView.java"


# direct methods
.method public constructor <init>(Landroid/content/Context;)V
    .locals 1
    .param p1    # Landroid/content/Context;

    const/4 v0, 0x0

    invoke-direct {p0, p1, v0}, Lcom/google/android/finsky/layout/play/PlayEditorialCardView;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;)V

    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;)V
    .locals 0
    .param p1    # Landroid/content/Context;
    .param p2    # Landroid/util/AttributeSet;

    invoke-direct {p0, p1, p2}, Lcom/google/android/finsky/layout/play/PlayCardView;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;)V

    return-void
.end method


# virtual methods
.method protected shouldPreferRatingOverSubtitle(Lcom/google/android/finsky/api/model/Document;)Z
    .locals 1
    .param p1    # Lcom/google/android/finsky/api/model/Document;

    const/4 v0, 0x0

    return v0
.end method
