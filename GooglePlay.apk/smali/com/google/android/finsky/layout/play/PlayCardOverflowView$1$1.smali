.class Lcom/google/android/finsky/layout/play/PlayCardOverflowView$1$1;
.super Ljava/lang/Object;
.source "PlayCardOverflowView.java"

# interfaces
.implements Lcom/google/android/finsky/layout/play/PlayPopupMenu$OnActionSelectedListener;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/google/android/finsky/layout/play/PlayCardOverflowView$1;->onClick(Landroid/view/View;)V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$1:Lcom/google/android/finsky/layout/play/PlayCardOverflowView$1;

.field final synthetic val$eventLog:Lcom/google/android/finsky/analytics/FinskyEventLog;

.field final synthetic val$finalWishlistClickEventType:I


# direct methods
.method constructor <init>(Lcom/google/android/finsky/layout/play/PlayCardOverflowView$1;Lcom/google/android/finsky/analytics/FinskyEventLog;I)V
    .locals 0

    iput-object p1, p0, Lcom/google/android/finsky/layout/play/PlayCardOverflowView$1$1;->this$1:Lcom/google/android/finsky/layout/play/PlayCardOverflowView$1;

    iput-object p2, p0, Lcom/google/android/finsky/layout/play/PlayCardOverflowView$1$1;->val$eventLog:Lcom/google/android/finsky/analytics/FinskyEventLog;

    iput p3, p0, Lcom/google/android/finsky/layout/play/PlayCardOverflowView$1$1;->val$finalWishlistClickEventType:I

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onActionSelected()V
    .locals 4

    iget-object v0, p0, Lcom/google/android/finsky/layout/play/PlayCardOverflowView$1$1;->val$eventLog:Lcom/google/android/finsky/analytics/FinskyEventLog;

    iget v1, p0, Lcom/google/android/finsky/layout/play/PlayCardOverflowView$1$1;->val$finalWishlistClickEventType:I

    const/4 v2, 0x0

    iget-object v3, p0, Lcom/google/android/finsky/layout/play/PlayCardOverflowView$1$1;->this$1:Lcom/google/android/finsky/layout/play/PlayCardOverflowView$1;

    iget-object v3, v3, Lcom/google/android/finsky/layout/play/PlayCardOverflowView$1;->val$clickedNode:Lcom/google/android/finsky/layout/play/PlayStoreUiElementNode;

    invoke-virtual {v0, v1, v2, v3}, Lcom/google/android/finsky/analytics/FinskyEventLog;->logClickEvent(ILcom/google/protobuf/micro/ByteStringMicro;Lcom/google/android/finsky/layout/play/PlayStoreUiElementNode;)V

    iget-object v0, p0, Lcom/google/android/finsky/layout/play/PlayCardOverflowView$1$1;->this$1:Lcom/google/android/finsky/layout/play/PlayCardOverflowView$1;

    iget-object v0, v0, Lcom/google/android/finsky/layout/play/PlayCardOverflowView$1;->val$doc:Lcom/google/android/finsky/api/model/Document;

    iget-object v1, p0, Lcom/google/android/finsky/layout/play/PlayCardOverflowView$1$1;->this$1:Lcom/google/android/finsky/layout/play/PlayCardOverflowView$1;

    iget-object v1, v1, Lcom/google/android/finsky/layout/play/PlayCardOverflowView$1;->val$dfeApi:Lcom/google/android/finsky/api/DfeApi;

    iget-object v2, p0, Lcom/google/android/finsky/layout/play/PlayCardOverflowView$1$1;->this$1:Lcom/google/android/finsky/layout/play/PlayCardOverflowView$1;

    iget-object v2, v2, Lcom/google/android/finsky/layout/play/PlayCardOverflowView$1;->val$onWishlistStatusListener:Lcom/google/android/finsky/utils/WishlistHelper$WishlistStatusListener;

    iget-object v3, p0, Lcom/google/android/finsky/layout/play/PlayCardOverflowView$1$1;->this$1:Lcom/google/android/finsky/layout/play/PlayCardOverflowView$1;

    iget-object v3, v3, Lcom/google/android/finsky/layout/play/PlayCardOverflowView$1;->val$referrerUrl:Ljava/lang/String;

    invoke-static {v0, v1, v2, v3}, Lcom/google/android/finsky/utils/WishlistHelper;->processWishlistClick(Lcom/google/android/finsky/api/model/Document;Lcom/google/android/finsky/api/DfeApi;Lcom/google/android/finsky/utils/WishlistHelper$WishlistStatusListener;Ljava/lang/String;)V

    return-void
.end method
