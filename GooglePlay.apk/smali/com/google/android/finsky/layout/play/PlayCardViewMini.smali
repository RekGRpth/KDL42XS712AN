.class public Lcom/google/android/finsky/layout/play/PlayCardViewMini;
.super Lcom/google/android/finsky/layout/play/PlayCardView;
.source "PlayCardViewMini.java"


# instance fields
.field private final mPriceThreshold:I

.field private final mTextContentHeight:I

.field private mVerticalOverlap:I


# direct methods
.method public constructor <init>(Landroid/content/Context;)V
    .locals 1
    .param p1    # Landroid/content/Context;

    const/4 v0, 0x0

    invoke-direct {p0, p1, v0}, Lcom/google/android/finsky/layout/play/PlayCardViewMini;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;)V

    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;)V
    .locals 2
    .param p1    # Landroid/content/Context;
    .param p2    # Landroid/util/AttributeSet;

    invoke-direct {p0, p1, p2}, Lcom/google/android/finsky/layout/play/PlayCardView;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;)V

    invoke-virtual {p1}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    const v1, 0x7f0b0061    # com.android.vending.R.dimen.play_mini_card_content_height

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v1

    iput v1, p0, Lcom/google/android/finsky/layout/play/PlayCardViewMini;->mTextContentHeight:I

    const v1, 0x7f0b006e    # com.android.vending.R.dimen.play_mini_card_price_threshold

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v1

    iput v1, p0, Lcom/google/android/finsky/layout/play/PlayCardViewMini;->mPriceThreshold:I

    return-void
.end method


# virtual methods
.method protected getPlayStoreUiElementType()I
    .locals 1

    const/16 v0, 0x1fd

    return v0
.end method

.method protected onLayout(ZIIII)V
    .locals 33
    .param p1    # Z
    .param p2    # I
    .param p3    # I
    .param p4    # I
    .param p5    # I

    invoke-virtual/range {p0 .. p0}, Lcom/google/android/finsky/layout/play/PlayCardViewMini;->getPaddingLeft()I

    move-result v14

    invoke-virtual/range {p0 .. p0}, Lcom/google/android/finsky/layout/play/PlayCardViewMini;->getPaddingRight()I

    move-result v15

    invoke-virtual/range {p0 .. p0}, Lcom/google/android/finsky/layout/play/PlayCardViewMini;->getPaddingTop()I

    move-result v16

    invoke-virtual/range {p0 .. p0}, Lcom/google/android/finsky/layout/play/PlayCardViewMini;->getPaddingBottom()I

    move-result v13

    invoke-virtual/range {p0 .. p0}, Lcom/google/android/finsky/layout/play/PlayCardViewMini;->getWidth()I

    move-result v29

    invoke-virtual/range {p0 .. p0}, Lcom/google/android/finsky/layout/play/PlayCardViewMini;->getHeight()I

    move-result v5

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/google/android/finsky/layout/play/PlayCardViewMini;->mThumbnail:Lcom/google/android/finsky/layout/play/PlayCardThumbnail;

    move-object/from16 v30, v0

    invoke-virtual/range {v30 .. v30}, Lcom/google/android/finsky/layout/play/PlayCardThumbnail;->getMeasuredHeight()I

    move-result v25

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/google/android/finsky/layout/play/PlayCardViewMini;->mThumbnail:Lcom/google/android/finsky/layout/play/PlayCardThumbnail;

    move-object/from16 v30, v0

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/google/android/finsky/layout/play/PlayCardViewMini;->mThumbnail:Lcom/google/android/finsky/layout/play/PlayCardThumbnail;

    move-object/from16 v31, v0

    invoke-virtual/range {v31 .. v31}, Lcom/google/android/finsky/layout/play/PlayCardThumbnail;->getMeasuredWidth()I

    move-result v31

    add-int v31, v31, v14

    add-int v32, v16, v25

    move-object/from16 v0, v30

    move/from16 v1, v16

    move/from16 v2, v31

    move/from16 v3, v32

    invoke-virtual {v0, v14, v1, v2, v3}, Lcom/google/android/finsky/layout/play/PlayCardThumbnail;->layout(IIII)V

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/google/android/finsky/layout/play/PlayCardViewMini;->mTitle:Landroid/widget/TextView;

    move-object/from16 v30, v0

    invoke-virtual/range {v30 .. v30}, Landroid/widget/TextView;->getLayoutParams()Landroid/view/ViewGroup$LayoutParams;

    move-result-object v27

    check-cast v27, Landroid/view/ViewGroup$MarginLayoutParams;

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/google/android/finsky/layout/play/PlayCardViewMini;->mSubtitle:Lcom/google/android/finsky/layout/DecoratedTextView;

    move-object/from16 v30, v0

    invoke-virtual/range {v30 .. v30}, Lcom/google/android/finsky/layout/DecoratedTextView;->getLayoutParams()Landroid/view/ViewGroup$LayoutParams;

    move-result-object v23

    check-cast v23, Landroid/view/ViewGroup$MarginLayoutParams;

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/google/android/finsky/layout/play/PlayCardViewMini;->mRatingBar:Landroid/widget/RatingBar;

    move-object/from16 v30, v0

    invoke-virtual/range {v30 .. v30}, Landroid/widget/RatingBar;->getLayoutParams()Landroid/view/ViewGroup$LayoutParams;

    move-result-object v20

    check-cast v20, Landroid/view/ViewGroup$MarginLayoutParams;

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/google/android/finsky/layout/play/PlayCardViewMini;->mPrice:Lcom/google/android/finsky/layout/play/PlayActionButton;

    move-object/from16 v30, v0

    invoke-virtual/range {v30 .. v30}, Lcom/google/android/finsky/layout/play/PlayActionButton;->getLayoutParams()Landroid/view/ViewGroup$LayoutParams;

    move-result-object v18

    check-cast v18, Landroid/view/ViewGroup$MarginLayoutParams;

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/google/android/finsky/layout/play/PlayCardViewMini;->mOverflow:Lcom/google/android/finsky/layout/play/PlayCardOverflowView;

    move-object/from16 v30, v0

    invoke-virtual/range {v30 .. v30}, Lcom/google/android/finsky/layout/play/PlayCardOverflowView;->getLayoutParams()Landroid/view/ViewGroup$LayoutParams;

    move-result-object v10

    check-cast v10, Landroid/view/ViewGroup$MarginLayoutParams;

    add-int v30, v16, v25

    move-object/from16 v0, v27

    iget v0, v0, Landroid/view/ViewGroup$MarginLayoutParams;->topMargin:I

    move/from16 v31, v0

    add-int v30, v30, v31

    move-object/from16 v0, p0

    iget v0, v0, Lcom/google/android/finsky/layout/play/PlayCardViewMini;->mVerticalOverlap:I

    move/from16 v31, v0

    sub-int v28, v30, v31

    move-object/from16 v0, v27

    iget v0, v0, Landroid/view/ViewGroup$MarginLayoutParams;->leftMargin:I

    move/from16 v30, v0

    add-int v26, v14, v30

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/google/android/finsky/layout/play/PlayCardViewMini;->mTitle:Landroid/widget/TextView;

    move-object/from16 v30, v0

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/google/android/finsky/layout/play/PlayCardViewMini;->mTitle:Landroid/widget/TextView;

    move-object/from16 v31, v0

    invoke-virtual/range {v31 .. v31}, Landroid/widget/TextView;->getMeasuredWidth()I

    move-result v31

    add-int v31, v31, v26

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/google/android/finsky/layout/play/PlayCardViewMini;->mTitle:Landroid/widget/TextView;

    move-object/from16 v32, v0

    invoke-virtual/range {v32 .. v32}, Landroid/widget/TextView;->getMeasuredHeight()I

    move-result v32

    add-int v32, v32, v28

    move-object/from16 v0, v30

    move/from16 v1, v26

    move/from16 v2, v28

    move/from16 v3, v31

    move/from16 v4, v32

    invoke-virtual {v0, v1, v2, v3, v4}, Landroid/widget/TextView;->layout(IIII)V

    add-int v30, v16, v25

    iget v0, v10, Landroid/view/ViewGroup$MarginLayoutParams;->topMargin:I

    move/from16 v31, v0

    add-int v12, v30, v31

    sub-int v11, v29, v15

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/google/android/finsky/layout/play/PlayCardViewMini;->mOverflow:Lcom/google/android/finsky/layout/play/PlayCardOverflowView;

    move-object/from16 v30, v0

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/google/android/finsky/layout/play/PlayCardViewMini;->mOverflow:Lcom/google/android/finsky/layout/play/PlayCardOverflowView;

    move-object/from16 v31, v0

    invoke-virtual/range {v31 .. v31}, Lcom/google/android/finsky/layout/play/PlayCardOverflowView;->getMeasuredWidth()I

    move-result v31

    sub-int v31, v11, v31

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/google/android/finsky/layout/play/PlayCardViewMini;->mOverflow:Lcom/google/android/finsky/layout/play/PlayCardOverflowView;

    move-object/from16 v32, v0

    invoke-virtual/range {v32 .. v32}, Lcom/google/android/finsky/layout/play/PlayCardOverflowView;->getMeasuredHeight()I

    move-result v32

    add-int v32, v32, v12

    move-object/from16 v0, v30

    move/from16 v1, v31

    move/from16 v2, v32

    invoke-virtual {v0, v1, v12, v11, v2}, Lcom/google/android/finsky/layout/play/PlayCardOverflowView;->layout(IIII)V

    sub-int v30, v5, v13

    move-object/from16 v0, v18

    iget v0, v0, Landroid/view/ViewGroup$MarginLayoutParams;->bottomMargin:I

    move/from16 v31, v0

    sub-int v30, v30, v31

    move-object/from16 v0, p0

    iget v0, v0, Lcom/google/android/finsky/layout/play/PlayCardViewMini;->mVerticalOverlap:I

    move/from16 v31, v0

    add-int v17, v30, v31

    move-object/from16 v0, v18

    iget v0, v0, Landroid/view/ViewGroup$MarginLayoutParams;->rightMargin:I

    move/from16 v30, v0

    sub-int v19, v11, v30

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/google/android/finsky/layout/play/PlayCardViewMini;->mPrice:Lcom/google/android/finsky/layout/play/PlayActionButton;

    move-object/from16 v30, v0

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/google/android/finsky/layout/play/PlayCardViewMini;->mPrice:Lcom/google/android/finsky/layout/play/PlayActionButton;

    move-object/from16 v31, v0

    invoke-virtual/range {v31 .. v31}, Lcom/google/android/finsky/layout/play/PlayActionButton;->getMeasuredWidth()I

    move-result v31

    sub-int v31, v19, v31

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/google/android/finsky/layout/play/PlayCardViewMini;->mPrice:Lcom/google/android/finsky/layout/play/PlayActionButton;

    move-object/from16 v32, v0

    invoke-virtual/range {v32 .. v32}, Lcom/google/android/finsky/layout/play/PlayActionButton;->getMeasuredHeight()I

    move-result v32

    sub-int v32, v17, v32

    move-object/from16 v0, v30

    move/from16 v1, v31

    move/from16 v2, v32

    move/from16 v3, v19

    move/from16 v4, v17

    invoke-virtual {v0, v1, v2, v3, v4}, Lcom/google/android/finsky/layout/play/PlayActionButton;->layout(IIII)V

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/google/android/finsky/layout/play/PlayCardViewMini;->mSubtitle:Lcom/google/android/finsky/layout/DecoratedTextView;

    move-object/from16 v30, v0

    invoke-virtual/range {v30 .. v30}, Lcom/google/android/finsky/layout/DecoratedTextView;->getVisibility()I

    move-result v30

    const/16 v31, 0x8

    move/from16 v0, v30

    move/from16 v1, v31

    if-eq v0, v1, :cond_0

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/google/android/finsky/layout/play/PlayCardViewMini;->mTitle:Landroid/widget/TextView;

    move-object/from16 v30, v0

    invoke-virtual/range {v30 .. v30}, Landroid/widget/TextView;->getLineCount()I

    move-result v30

    const/16 v31, 0x1

    move/from16 v0, v30

    move/from16 v1, v31

    if-ne v0, v1, :cond_2

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/google/android/finsky/layout/play/PlayCardViewMini;->mTitle:Landroid/widget/TextView;

    move-object/from16 v30, v0

    invoke-virtual/range {v30 .. v30}, Landroid/widget/TextView;->getMeasuredHeight()I

    move-result v30

    add-int v30, v30, v28

    move-object/from16 v0, v27

    iget v0, v0, Landroid/view/ViewGroup$MarginLayoutParams;->bottomMargin:I

    move/from16 v31, v0

    add-int v30, v30, v31

    move-object/from16 v0, v23

    iget v0, v0, Landroid/view/ViewGroup$MarginLayoutParams;->topMargin:I

    move/from16 v31, v0

    add-int v24, v30, v31

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/google/android/finsky/layout/play/PlayCardViewMini;->mSubtitle:Lcom/google/android/finsky/layout/DecoratedTextView;

    move-object/from16 v30, v0

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/google/android/finsky/layout/play/PlayCardViewMini;->mSubtitle:Lcom/google/android/finsky/layout/DecoratedTextView;

    move-object/from16 v31, v0

    invoke-virtual/range {v31 .. v31}, Lcom/google/android/finsky/layout/DecoratedTextView;->getMeasuredWidth()I

    move-result v31

    add-int v31, v31, v26

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/google/android/finsky/layout/play/PlayCardViewMini;->mSubtitle:Lcom/google/android/finsky/layout/DecoratedTextView;

    move-object/from16 v32, v0

    invoke-virtual/range {v32 .. v32}, Lcom/google/android/finsky/layout/DecoratedTextView;->getMeasuredHeight()I

    move-result v32

    add-int v32, v32, v24

    move-object/from16 v0, v30

    move/from16 v1, v26

    move/from16 v2, v24

    move/from16 v3, v31

    move/from16 v4, v32

    invoke-virtual {v0, v1, v2, v3, v4}, Lcom/google/android/finsky/layout/DecoratedTextView;->layout(IIII)V

    :cond_0
    :goto_0
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/google/android/finsky/layout/play/PlayCardViewMini;->mRatingBar:Landroid/widget/RatingBar;

    move-object/from16 v30, v0

    invoke-virtual/range {v30 .. v30}, Landroid/widget/RatingBar;->getVisibility()I

    move-result v30

    const/16 v31, 0x8

    move/from16 v0, v30

    move/from16 v1, v31

    if-eq v0, v1, :cond_1

    sub-int v30, v5, v13

    move-object/from16 v0, v20

    iget v0, v0, Landroid/view/ViewGroup$MarginLayoutParams;->bottomMargin:I

    move/from16 v31, v0

    sub-int v30, v30, v31

    move-object/from16 v0, p0

    iget v0, v0, Lcom/google/android/finsky/layout/play/PlayCardViewMini;->mVerticalOverlap:I

    move/from16 v31, v0

    add-int v21, v30, v31

    move-object/from16 v0, v20

    iget v0, v0, Landroid/view/ViewGroup$MarginLayoutParams;->leftMargin:I

    move/from16 v30, v0

    add-int v22, v14, v30

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/google/android/finsky/layout/play/PlayCardViewMini;->mRatingBar:Landroid/widget/RatingBar;

    move-object/from16 v30, v0

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/google/android/finsky/layout/play/PlayCardViewMini;->mRatingBar:Landroid/widget/RatingBar;

    move-object/from16 v31, v0

    invoke-virtual/range {v31 .. v31}, Landroid/widget/RatingBar;->getMeasuredHeight()I

    move-result v31

    sub-int v31, v21, v31

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/google/android/finsky/layout/play/PlayCardViewMini;->mRatingBar:Landroid/widget/RatingBar;

    move-object/from16 v32, v0

    invoke-virtual/range {v32 .. v32}, Landroid/widget/RatingBar;->getMeasuredWidth()I

    move-result v32

    add-int v32, v32, v22

    move-object/from16 v0, v30

    move/from16 v1, v22

    move/from16 v2, v31

    move/from16 v3, v32

    move/from16 v4, v21

    invoke-virtual {v0, v1, v2, v3, v4}, Landroid/widget/RatingBar;->layout(IIII)V

    :cond_1
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/google/android/finsky/layout/play/PlayCardViewMini;->mLoadingIndicator:Landroid/view/View;

    move-object/from16 v30, v0

    invoke-virtual/range {v30 .. v30}, Landroid/view/View;->getMeasuredWidth()I

    move-result v9

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/google/android/finsky/layout/play/PlayCardViewMini;->mLoadingIndicator:Landroid/view/View;

    move-object/from16 v30, v0

    invoke-virtual/range {v30 .. v30}, Landroid/view/View;->getMeasuredHeight()I

    move-result v6

    sub-int v30, v29, v14

    sub-int v30, v30, v15

    sub-int v30, v30, v9

    div-int/lit8 v30, v30, 0x2

    add-int v7, v14, v30

    sub-int v30, v5, v16

    sub-int v30, v30, v13

    sub-int v30, v30, v6

    div-int/lit8 v30, v30, 0x2

    add-int v8, v16, v30

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/google/android/finsky/layout/play/PlayCardViewMini;->mLoadingIndicator:Landroid/view/View;

    move-object/from16 v30, v0

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/google/android/finsky/layout/play/PlayCardViewMini;->mLoadingIndicator:Landroid/view/View;

    move-object/from16 v31, v0

    invoke-virtual/range {v31 .. v31}, Landroid/view/View;->getMeasuredWidth()I

    move-result v31

    add-int v31, v31, v7

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/google/android/finsky/layout/play/PlayCardViewMini;->mLoadingIndicator:Landroid/view/View;

    move-object/from16 v32, v0

    invoke-virtual/range {v32 .. v32}, Landroid/view/View;->getMeasuredHeight()I

    move-result v32

    add-int v32, v32, v8

    move-object/from16 v0, v30

    move/from16 v1, v31

    move/from16 v2, v32

    invoke-virtual {v0, v7, v8, v1, v2}, Landroid/view/View;->layout(IIII)V

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/google/android/finsky/layout/play/PlayCardViewMini;->mAccessibilityOverlay:Landroid/view/View;

    move-object/from16 v30, v0

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/google/android/finsky/layout/play/PlayCardViewMini;->mAccessibilityOverlay:Landroid/view/View;

    move-object/from16 v31, v0

    invoke-virtual/range {v31 .. v31}, Landroid/view/View;->getMeasuredWidth()I

    move-result v31

    add-int v31, v31, v14

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/google/android/finsky/layout/play/PlayCardViewMini;->mAccessibilityOverlay:Landroid/view/View;

    move-object/from16 v32, v0

    invoke-virtual/range {v32 .. v32}, Landroid/view/View;->getMeasuredHeight()I

    move-result v32

    add-int v32, v32, v16

    move-object/from16 v0, v30

    move/from16 v1, v16

    move/from16 v2, v31

    move/from16 v3, v32

    invoke-virtual {v0, v14, v1, v2, v3}, Landroid/view/View;->layout(IIII)V

    return-void

    :cond_2
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/google/android/finsky/layout/play/PlayCardViewMini;->mPrice:Lcom/google/android/finsky/layout/play/PlayActionButton;

    move-object/from16 v30, v0

    invoke-virtual/range {v30 .. v30}, Lcom/google/android/finsky/layout/play/PlayActionButton;->getMeasuredHeight()I

    move-result v30

    sub-int v30, v17, v30

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/google/android/finsky/layout/play/PlayCardViewMini;->mPrice:Lcom/google/android/finsky/layout/play/PlayActionButton;

    move-object/from16 v31, v0

    invoke-virtual/range {v31 .. v31}, Lcom/google/android/finsky/layout/play/PlayActionButton;->getBaseline()I

    move-result v31

    add-int v30, v30, v31

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/google/android/finsky/layout/play/PlayCardViewMini;->mSubtitle:Lcom/google/android/finsky/layout/DecoratedTextView;

    move-object/from16 v31, v0

    invoke-virtual/range {v31 .. v31}, Lcom/google/android/finsky/layout/DecoratedTextView;->getBaseline()I

    move-result v31

    sub-int v24, v30, v31

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/google/android/finsky/layout/play/PlayCardViewMini;->mSubtitle:Lcom/google/android/finsky/layout/DecoratedTextView;

    move-object/from16 v30, v0

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/google/android/finsky/layout/play/PlayCardViewMini;->mSubtitle:Lcom/google/android/finsky/layout/DecoratedTextView;

    move-object/from16 v31, v0

    invoke-virtual/range {v31 .. v31}, Lcom/google/android/finsky/layout/DecoratedTextView;->getMeasuredWidth()I

    move-result v31

    add-int v31, v31, v26

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/google/android/finsky/layout/play/PlayCardViewMini;->mSubtitle:Lcom/google/android/finsky/layout/DecoratedTextView;

    move-object/from16 v32, v0

    invoke-virtual/range {v32 .. v32}, Lcom/google/android/finsky/layout/DecoratedTextView;->getMeasuredHeight()I

    move-result v32

    add-int v32, v32, v24

    move-object/from16 v0, v30

    move/from16 v1, v26

    move/from16 v2, v24

    move/from16 v3, v31

    move/from16 v4, v32

    invoke-virtual {v0, v1, v2, v3, v4}, Lcom/google/android/finsky/layout/DecoratedTextView;->layout(IIII)V

    goto/16 :goto_0
.end method

.method protected onMeasure(II)V
    .locals 31
    .param p1    # I
    .param p2    # I

    invoke-virtual/range {p0 .. p1}, Lcom/google/android/finsky/layout/play/PlayCardViewMini;->measureThumbnailSpanningWidth(I)V

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/google/android/finsky/layout/play/PlayCardViewMini;->mThumbnail:Lcom/google/android/finsky/layout/play/PlayCardThumbnail;

    move-object/from16 v28, v0

    invoke-virtual/range {v28 .. v28}, Lcom/google/android/finsky/layout/play/PlayCardThumbnail;->getLayoutParams()Landroid/view/ViewGroup$LayoutParams;

    move-result-object v22

    check-cast v22, Landroid/view/ViewGroup$MarginLayoutParams;

    invoke-static/range {p2 .. p2}, Landroid/view/View$MeasureSpec;->getMode(I)I

    move-result v8

    invoke-static/range {p2 .. p2}, Landroid/view/View$MeasureSpec;->getSize(I)I

    move-result v9

    move-object/from16 v0, v22

    iget v0, v0, Landroid/view/ViewGroup$MarginLayoutParams;->height:I

    move/from16 v28, v0

    move-object/from16 v0, p0

    iget v0, v0, Lcom/google/android/finsky/layout/play/PlayCardViewMini;->mTextContentHeight:I

    move/from16 v29, v0

    add-int v28, v28, v29

    invoke-virtual/range {p0 .. p0}, Lcom/google/android/finsky/layout/play/PlayCardViewMini;->getPaddingTop()I

    move-result v29

    add-int v28, v28, v29

    invoke-virtual/range {p0 .. p0}, Lcom/google/android/finsky/layout/play/PlayCardViewMini;->getPaddingBottom()I

    move-result v29

    add-int v10, v28, v29

    const/high16 v28, 0x40000000    # 2.0f

    move/from16 v0, v28

    if-ne v8, v0, :cond_2

    if-lez v9, :cond_2

    move v7, v9

    :goto_0
    invoke-static/range {p1 .. p1}, Landroid/view/View$MeasureSpec;->getSize(I)I

    move-result v26

    const/16 v28, 0x0

    sub-int v29, v10, v7

    div-int/lit8 v29, v29, 0x3

    invoke-static/range {v28 .. v29}, Ljava/lang/Math;->max(II)I

    move-result v28

    move/from16 v0, v28

    move-object/from16 v1, p0

    iput v0, v1, Lcom/google/android/finsky/layout/play/PlayCardViewMini;->mVerticalOverlap:I

    invoke-virtual/range {p0 .. p0}, Lcom/google/android/finsky/layout/play/PlayCardViewMini;->getPaddingLeft()I

    move-result v13

    invoke-virtual/range {p0 .. p0}, Lcom/google/android/finsky/layout/play/PlayCardViewMini;->getPaddingRight()I

    move-result v14

    invoke-virtual/range {p0 .. p0}, Lcom/google/android/finsky/layout/play/PlayCardViewMini;->getPaddingTop()I

    move-result v15

    invoke-virtual/range {p0 .. p0}, Lcom/google/android/finsky/layout/play/PlayCardViewMini;->getPaddingBottom()I

    move-result v12

    sub-int v28, v26, v13

    sub-int v5, v28, v14

    const/high16 v28, 0x40000000    # 2.0f

    move/from16 v0, v28

    invoke-static {v5, v0}, Landroid/view/View$MeasureSpec;->makeMeasureSpec(II)I

    move-result v6

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/google/android/finsky/layout/play/PlayCardViewMini;->mThumbnail:Lcom/google/android/finsky/layout/play/PlayCardThumbnail;

    move-object/from16 v28, v0

    move-object/from16 v0, v22

    iget v0, v0, Landroid/view/ViewGroup$MarginLayoutParams;->height:I

    move/from16 v29, v0

    const/high16 v30, 0x40000000    # 2.0f

    invoke-static/range {v29 .. v30}, Landroid/view/View$MeasureSpec;->makeMeasureSpec(II)I

    move-result v29

    move-object/from16 v0, v28

    move/from16 v1, v29

    invoke-virtual {v0, v6, v1}, Lcom/google/android/finsky/layout/play/PlayCardThumbnail;->measure(II)V

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/google/android/finsky/layout/play/PlayCardViewMini;->mTitle:Landroid/widget/TextView;

    move-object/from16 v28, v0

    invoke-virtual/range {v28 .. v28}, Landroid/widget/TextView;->getLayoutParams()Landroid/view/ViewGroup$LayoutParams;

    move-result-object v23

    check-cast v23, Landroid/view/ViewGroup$MarginLayoutParams;

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/google/android/finsky/layout/play/PlayCardViewMini;->mSubtitle:Lcom/google/android/finsky/layout/DecoratedTextView;

    move-object/from16 v28, v0

    invoke-virtual/range {v28 .. v28}, Lcom/google/android/finsky/layout/DecoratedTextView;->getLayoutParams()Landroid/view/ViewGroup$LayoutParams;

    move-result-object v20

    check-cast v20, Landroid/view/ViewGroup$MarginLayoutParams;

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/google/android/finsky/layout/play/PlayCardViewMini;->mRatingBar:Landroid/widget/RatingBar;

    move-object/from16 v28, v0

    invoke-virtual/range {v28 .. v28}, Landroid/widget/RatingBar;->getLayoutParams()Landroid/view/ViewGroup$LayoutParams;

    move-result-object v18

    check-cast v18, Landroid/view/ViewGroup$MarginLayoutParams;

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/google/android/finsky/layout/play/PlayCardViewMini;->mPrice:Lcom/google/android/finsky/layout/play/PlayActionButton;

    move-object/from16 v28, v0

    invoke-virtual/range {v28 .. v28}, Lcom/google/android/finsky/layout/play/PlayActionButton;->getLayoutParams()Landroid/view/ViewGroup$LayoutParams;

    move-result-object v16

    check-cast v16, Landroid/view/ViewGroup$MarginLayoutParams;

    const/4 v11, 0x0

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/google/android/finsky/layout/play/PlayCardViewMini;->mDoc:Lcom/google/android/finsky/api/model/Document;

    move-object/from16 v28, v0

    if-eqz v28, :cond_0

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/google/android/finsky/layout/play/PlayCardViewMini;->mDoc:Lcom/google/android/finsky/api/model/Document;

    move-object/from16 v28, v0

    invoke-virtual/range {v28 .. v28}, Lcom/google/android/finsky/api/model/Document;->getDocumentType()I

    move-result v28

    const/16 v29, 0x1

    move/from16 v0, v28

    move/from16 v1, v29

    if-ne v0, v1, :cond_3

    const/4 v11, 0x1

    :cond_0
    :goto_1
    if-eqz v11, :cond_6

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/google/android/finsky/layout/play/PlayCardViewMini;->mPrice:Lcom/google/android/finsky/layout/play/PlayActionButton;

    move-object/from16 v28, v0

    invoke-virtual/range {v28 .. v28}, Lcom/google/android/finsky/layout/play/PlayActionButton;->getVisibility()I

    move-result v28

    const/16 v29, 0x8

    move/from16 v0, v28

    move/from16 v1, v29

    if-eq v0, v1, :cond_6

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/google/android/finsky/layout/play/PlayCardViewMini;->mPrice:Lcom/google/android/finsky/layout/play/PlayActionButton;

    move-object/from16 v28, v0

    const/16 v29, 0x0

    const/16 v30, 0x0

    invoke-virtual/range {v28 .. v30}, Lcom/google/android/finsky/layout/play/PlayActionButton;->measure(II)V

    :goto_2
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/google/android/finsky/layout/play/PlayCardViewMini;->mPrice:Lcom/google/android/finsky/layout/play/PlayActionButton;

    move-object/from16 v28, v0

    invoke-virtual/range {v28 .. v28}, Lcom/google/android/finsky/layout/play/PlayActionButton;->getMeasuredWidth()I

    move-result v28

    move-object/from16 v0, v16

    iget v0, v0, Landroid/view/ViewGroup$MarginLayoutParams;->leftMargin:I

    move/from16 v29, v0

    add-int v28, v28, v29

    move-object/from16 v0, v16

    iget v0, v0, Landroid/view/ViewGroup$MarginLayoutParams;->rightMargin:I

    move/from16 v29, v0

    add-int v17, v28, v29

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/google/android/finsky/layout/play/PlayCardViewMini;->mOverflow:Lcom/google/android/finsky/layout/play/PlayCardOverflowView;

    move-object/from16 v28, v0

    const/16 v29, 0x0

    const/16 v30, 0x0

    invoke-virtual/range {v28 .. v30}, Lcom/google/android/finsky/layout/play/PlayCardOverflowView;->measure(II)V

    move-object/from16 v0, v23

    iget v0, v0, Landroid/view/ViewGroup$MarginLayoutParams;->leftMargin:I

    move/from16 v28, v0

    sub-int v28, v5, v28

    move-object/from16 v0, v23

    iget v0, v0, Landroid/view/ViewGroup$MarginLayoutParams;->rightMargin:I

    move/from16 v29, v0

    sub-int v24, v28, v29

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/google/android/finsky/layout/play/PlayCardViewMini;->mTitle:Landroid/widget/TextView;

    move-object/from16 v28, v0

    const/high16 v29, 0x40000000    # 2.0f

    move/from16 v0, v24

    move/from16 v1, v29

    invoke-static {v0, v1}, Landroid/view/View$MeasureSpec;->makeMeasureSpec(II)I

    move-result v29

    const/16 v30, 0x0

    invoke-virtual/range {v28 .. v30}, Landroid/widget/TextView;->measure(II)V

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/google/android/finsky/layout/play/PlayCardViewMini;->mLoadingIndicator:Landroid/view/View;

    move-object/from16 v28, v0

    invoke-virtual/range {v28 .. v28}, Landroid/view/View;->getVisibility()I

    move-result v28

    if-eqz v28, :cond_1

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/google/android/finsky/layout/play/PlayCardViewMini;->mSubtitle:Lcom/google/android/finsky/layout/DecoratedTextView;

    move-object/from16 v28, v0

    invoke-virtual/range {v28 .. v28}, Lcom/google/android/finsky/layout/DecoratedTextView;->getVisibility()I

    move-result v28

    if-eqz v28, :cond_8

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/google/android/finsky/layout/play/PlayCardViewMini;->mRatingBar:Landroid/widget/RatingBar;

    move-object/from16 v28, v0

    const/16 v29, 0x0

    const/16 v30, 0x0

    invoke-virtual/range {v28 .. v30}, Landroid/widget/RatingBar;->measure(II)V

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/google/android/finsky/layout/play/PlayCardViewMini;->mRatingBar:Landroid/widget/RatingBar;

    move-object/from16 v28, v0

    invoke-virtual/range {v28 .. v28}, Landroid/widget/RatingBar;->getMeasuredWidth()I

    move-result v19

    move-object/from16 v0, v18

    iget v0, v0, Landroid/view/ViewGroup$MarginLayoutParams;->leftMargin:I

    move/from16 v28, v0

    add-int v28, v28, v19

    move-object/from16 v0, v18

    iget v0, v0, Landroid/view/ViewGroup$MarginLayoutParams;->rightMargin:I

    move/from16 v29, v0

    add-int v28, v28, v29

    add-int v25, v28, v17

    move/from16 v0, v25

    if-le v0, v5, :cond_7

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/google/android/finsky/layout/play/PlayCardViewMini;->mRatingBar:Landroid/widget/RatingBar;

    move-object/from16 v28, v0

    const/16 v29, 0x4

    invoke-virtual/range {v28 .. v29}, Landroid/widget/RatingBar;->setVisibility(I)V

    :cond_1
    :goto_3
    sub-int v28, v7, v15

    sub-int v3, v28, v12

    const/high16 v28, 0x40000000    # 2.0f

    move/from16 v0, v28

    invoke-static {v3, v0}, Landroid/view/View$MeasureSpec;->makeMeasureSpec(II)I

    move-result v4

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/google/android/finsky/layout/play/PlayCardViewMini;->mLoadingIndicator:Landroid/view/View;

    move-object/from16 v28, v0

    const/16 v29, 0x0

    const/16 v30, 0x0

    invoke-virtual/range {v28 .. v30}, Landroid/view/View;->measure(II)V

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/google/android/finsky/layout/play/PlayCardViewMini;->mAccessibilityOverlay:Landroid/view/View;

    move-object/from16 v28, v0

    move-object/from16 v0, v28

    invoke-virtual {v0, v6, v4}, Landroid/view/View;->measure(II)V

    move-object/from16 v0, p0

    move/from16 v1, v26

    invoke-virtual {v0, v1, v7}, Lcom/google/android/finsky/layout/play/PlayCardViewMini;->setMeasuredDimension(II)V

    return-void

    :cond_2
    move v7, v10

    goto/16 :goto_0

    :cond_3
    move-object/from16 v0, p0

    iget-boolean v0, v0, Lcom/google/android/finsky/layout/play/PlayCardViewMini;->mIsDocOwned:Z

    move/from16 v28, v0

    if-nez v28, :cond_4

    move-object/from16 v0, p0

    iget v0, v0, Lcom/google/android/finsky/layout/play/PlayCardViewMini;->mPriceThreshold:I

    move/from16 v28, v0

    move/from16 v0, v26

    move/from16 v1, v28

    if-lt v0, v1, :cond_5

    :cond_4
    const/4 v11, 0x1

    :goto_4
    goto/16 :goto_1

    :cond_5
    const/4 v11, 0x0

    goto :goto_4

    :cond_6
    const/16 v28, 0x0

    const/high16 v29, 0x40000000    # 2.0f

    invoke-static/range {v28 .. v29}, Landroid/view/View$MeasureSpec;->makeMeasureSpec(II)I

    move-result v27

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/google/android/finsky/layout/play/PlayCardViewMini;->mPrice:Lcom/google/android/finsky/layout/play/PlayActionButton;

    move-object/from16 v28, v0

    const/16 v29, 0x0

    move-object/from16 v0, v28

    move/from16 v1, v27

    move/from16 v2, v29

    invoke-virtual {v0, v1, v2}, Lcom/google/android/finsky/layout/play/PlayActionButton;->measure(II)V

    goto/16 :goto_2

    :cond_7
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/google/android/finsky/layout/play/PlayCardViewMini;->mRatingBar:Landroid/widget/RatingBar;

    move-object/from16 v28, v0

    const/16 v29, 0x0

    invoke-virtual/range {v28 .. v29}, Landroid/widget/RatingBar;->setVisibility(I)V

    goto :goto_3

    :cond_8
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/google/android/finsky/layout/play/PlayCardViewMini;->mSubtitle:Lcom/google/android/finsky/layout/DecoratedTextView;

    move-object/from16 v28, v0

    const/16 v29, 0x0

    const/16 v30, 0x0

    invoke-virtual/range {v28 .. v30}, Lcom/google/android/finsky/layout/DecoratedTextView;->measure(II)V

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/google/android/finsky/layout/play/PlayCardViewMini;->mTitle:Landroid/widget/TextView;

    move-object/from16 v28, v0

    invoke-virtual/range {v28 .. v28}, Landroid/widget/TextView;->getLineCount()I

    move-result v28

    const/16 v29, 0x1

    move/from16 v0, v28

    move/from16 v1, v29

    if-eq v0, v1, :cond_9

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/google/android/finsky/layout/play/PlayCardViewMini;->mPrice:Lcom/google/android/finsky/layout/play/PlayActionButton;

    move-object/from16 v28, v0

    invoke-virtual/range {v28 .. v28}, Lcom/google/android/finsky/layout/play/PlayActionButton;->getVisibility()I

    move-result v28

    if-eqz v28, :cond_a

    :cond_9
    move-object/from16 v0, v20

    iget v0, v0, Landroid/view/ViewGroup$MarginLayoutParams;->leftMargin:I

    move/from16 v28, v0

    sub-int v28, v5, v28

    move-object/from16 v0, v20

    iget v0, v0, Landroid/view/ViewGroup$MarginLayoutParams;->rightMargin:I

    move/from16 v29, v0

    sub-int v21, v28, v29

    :goto_5
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/google/android/finsky/layout/play/PlayCardViewMini;->mSubtitle:Lcom/google/android/finsky/layout/DecoratedTextView;

    move-object/from16 v28, v0

    const/high16 v29, 0x40000000    # 2.0f

    move/from16 v0, v21

    move/from16 v1, v29

    invoke-static {v0, v1}, Landroid/view/View$MeasureSpec;->makeMeasureSpec(II)I

    move-result v29

    const/16 v30, 0x0

    invoke-virtual/range {v28 .. v30}, Lcom/google/android/finsky/layout/DecoratedTextView;->measure(II)V

    goto/16 :goto_3

    :cond_a
    move-object/from16 v0, v20

    iget v0, v0, Landroid/view/ViewGroup$MarginLayoutParams;->leftMargin:I

    move/from16 v28, v0

    sub-int v28, v5, v28

    sub-int v21, v28, v17

    goto :goto_5
.end method
