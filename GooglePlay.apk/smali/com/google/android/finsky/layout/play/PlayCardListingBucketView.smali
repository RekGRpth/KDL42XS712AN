.class public Lcom/google/android/finsky/layout/play/PlayCardListingBucketView;
.super Landroid/widget/LinearLayout;
.source "PlayCardListingBucketView.java"

# interfaces
.implements Lcom/google/android/finsky/layout/play/PlayStoreUiElementNode;


# instance fields
.field private mColumnCount:I

.field protected mContent:Landroid/widget/LinearLayout;

.field protected mHeader:Lcom/google/android/finsky/layout/play/PlayCardClusterViewHeader;

.field private mParentNode:Lcom/google/android/finsky/layout/play/PlayStoreUiElementNode;

.field private mRowCount:I

.field private mUiElementProto:Lcom/google/android/play/analytics/PlayStore$PlayStoreUiElement;


# direct methods
.method public constructor <init>(Landroid/content/Context;)V
    .locals 1
    .param p1    # Landroid/content/Context;

    const/4 v0, 0x0

    invoke-direct {p0, p1, v0}, Lcom/google/android/finsky/layout/play/PlayCardListingBucketView;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;)V

    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;)V
    .locals 2
    .param p1    # Landroid/content/Context;
    .param p2    # Landroid/util/AttributeSet;

    invoke-direct {p0, p1, p2}, Landroid/widget/LinearLayout;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;)V

    invoke-static {}, Lcom/google/android/play/analytics/ProtoCache;->getInstance()Lcom/google/android/play/analytics/ProtoCache;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/android/play/analytics/ProtoCache;->obtainPlayStoreUiElement()Lcom/google/android/play/analytics/PlayStore$PlayStoreUiElement;

    move-result-object v0

    const/16 v1, 0x190

    invoke-virtual {v0, v1}, Lcom/google/android/play/analytics/PlayStore$PlayStoreUiElement;->setType(I)Lcom/google/android/play/analytics/PlayStore$PlayStoreUiElement;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/finsky/layout/play/PlayCardListingBucketView;->mUiElementProto:Lcom/google/android/play/analytics/PlayStore$PlayStoreUiElement;

    const/4 v0, 0x0

    iput-object v0, p0, Lcom/google/android/finsky/layout/play/PlayCardListingBucketView;->mParentNode:Lcom/google/android/finsky/layout/play/PlayStoreUiElementNode;

    return-void
.end method

.method private bindBucketEntries(Lcom/google/android/finsky/api/model/Bucket;Lcom/google/android/finsky/utils/BitmapLoader;Lcom/google/android/finsky/navigationmanager/NavigationManager;Ljava/lang/String;Z)V
    .locals 14
    .param p1    # Lcom/google/android/finsky/api/model/Bucket;
    .param p2    # Lcom/google/android/finsky/utils/BitmapLoader;
    .param p3    # Lcom/google/android/finsky/navigationmanager/NavigationManager;
    .param p4    # Ljava/lang/String;
    .param p5    # Z

    const/4 v12, 0x0

    :goto_0
    iget v2, p0, Lcom/google/android/finsky/layout/play/PlayCardListingBucketView;->mRowCount:I

    if-ge v12, v2, :cond_4

    iget-object v2, p0, Lcom/google/android/finsky/layout/play/PlayCardListingBucketView;->mContent:Landroid/widget/LinearLayout;

    invoke-virtual {v2, v12}, Landroid/widget/LinearLayout;->getChildAt(I)Landroid/view/View;

    move-result-object v13

    check-cast v13, Landroid/view/ViewGroup;

    invoke-direct {p0, p1}, Lcom/google/android/finsky/layout/play/PlayCardListingBucketView;->getDisplayedRowsForBucket(Lcom/google/android/finsky/api/model/Bucket;)I

    move-result v2

    if-lt v12, v2, :cond_1

    const/16 v2, 0x8

    invoke-virtual {v13, v2}, Landroid/view/ViewGroup;->setVisibility(I)V

    :cond_0
    add-int/lit8 v12, v12, 0x1

    goto :goto_0

    :cond_1
    const/4 v2, 0x0

    invoke-virtual {v13, v2}, Landroid/view/ViewGroup;->setVisibility(I)V

    invoke-direct {p0, p1, v12}, Lcom/google/android/finsky/layout/play/PlayCardListingBucketView;->getDisplayedColumnsForBucket(Lcom/google/android/finsky/api/model/Bucket;I)I

    move-result v9

    const/4 v8, 0x0

    :goto_1
    iget v2, p0, Lcom/google/android/finsky/layout/play/PlayCardListingBucketView;->mColumnCount:I

    if-ge v8, v2, :cond_0

    invoke-virtual {v13, v8}, Landroid/view/ViewGroup;->getChildAt(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/google/android/finsky/layout/play/PlayCardView;

    if-ge v8, v9, :cond_3

    iget v2, p0, Lcom/google/android/finsky/layout/play/PlayCardListingBucketView;->mColumnCount:I

    mul-int/2addr v2, v12

    add-int v10, v2, v8

    invoke-virtual {v0}, Lcom/google/android/finsky/layout/play/PlayCardView;->resetUiElementNode()V

    invoke-virtual {p1}, Lcom/google/android/finsky/api/model/Bucket;->getChildren()Ljava/util/List;

    move-result-object v2

    invoke-interface {v2, v10}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/google/android/finsky/api/model/Document;

    if-eqz p5, :cond_2

    instance-of v2, v0, Lcom/google/android/finsky/layout/play/PlayCardViewListingSmall;

    if-eqz v2, :cond_2

    move-object v11, v0

    check-cast v11, Lcom/google/android/finsky/layout/play/PlayCardViewListingSmall;

    invoke-virtual {v11, v1}, Lcom/google/android/finsky/layout/play/PlayCardViewListingSmall;->configureDeviceClassWarningDisplay(Lcom/google/android/finsky/api/model/Document;)V

    :cond_2
    const/4 v5, 0x0

    const/4 v6, 0x0

    move-object/from16 v2, p2

    move-object/from16 v3, p3

    move-object/from16 v4, p4

    move-object v7, p0

    invoke-virtual/range {v0 .. v7}, Lcom/google/android/finsky/layout/play/PlayCardView;->bindInList(Lcom/google/android/finsky/api/model/Document;Lcom/google/android/finsky/utils/BitmapLoader;Lcom/google/android/finsky/navigationmanager/NavigationManager;Ljava/lang/String;ZLcom/google/android/finsky/utils/WishlistHelper$WishlistStatusListener;Lcom/google/android/finsky/layout/play/PlayStoreUiElementNode;)V

    const/4 v2, 0x0

    invoke-virtual {v0, v2}, Lcom/google/android/finsky/layout/play/PlayCardView;->setVisibility(I)V

    :goto_2
    add-int/lit8 v8, v8, 0x1

    goto :goto_1

    :cond_3
    const/4 v2, 0x4

    invoke-virtual {v0, v2}, Lcom/google/android/finsky/layout/play/PlayCardView;->setVisibility(I)V

    goto :goto_2

    :cond_4
    return-void
.end method

.method private bindBucketHeader(Lcom/google/android/finsky/api/model/Bucket;Landroid/view/View$OnClickListener;)V
    .locals 8
    .param p1    # Lcom/google/android/finsky/api/model/Bucket;
    .param p2    # Landroid/view/View$OnClickListener;

    const/4 v4, 0x0

    invoke-virtual {p1}, Lcom/google/android/finsky/api/model/Bucket;->getEstimatedResults()I

    move-result v6

    if-lez v6, :cond_0

    invoke-virtual {p0}, Lcom/google/android/finsky/layout/play/PlayCardListingBucketView;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    const/high16 v1, 0x7f0e0000    # com.android.vending.R.plurals.search_results_estimated_in_bucket

    invoke-virtual {v0, v1, v6}, Landroid/content/res/Resources;->getQuantityText(II)Ljava/lang/CharSequence;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object v7

    const/4 v0, 0x1

    new-array v0, v0, [Ljava/lang/Object;

    const/4 v1, 0x0

    invoke-static {v6}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    aput-object v2, v0, v1

    invoke-static {v7, v0}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v4

    :cond_0
    iget-object v0, p0, Lcom/google/android/finsky/layout/play/PlayCardListingBucketView;->mHeader:Lcom/google/android/finsky/layout/play/PlayCardClusterViewHeader;

    invoke-virtual {p1}, Lcom/google/android/finsky/api/model/Bucket;->getBackend()I

    move-result v1

    invoke-virtual {p1}, Lcom/google/android/finsky/api/model/Bucket;->getTitle()Ljava/lang/String;

    move-result-object v2

    const/4 v3, 0x0

    move-object v5, p2

    invoke-virtual/range {v0 .. v5}, Lcom/google/android/finsky/layout/play/PlayCardClusterViewHeader;->setContent(ILjava/lang/String;Ljava/lang/String;Ljava/lang/String;Landroid/view/View$OnClickListener;)V

    return-void
.end method

.method private getDisplayedColumnsForBucket(Lcom/google/android/finsky/api/model/Bucket;I)I
    .locals 2
    .param p1    # Lcom/google/android/finsky/api/model/Bucket;
    .param p2    # I

    invoke-direct {p0, p1}, Lcom/google/android/finsky/layout/play/PlayCardListingBucketView;->getDisplayedRowsForBucket(Lcom/google/android/finsky/api/model/Bucket;)I

    move-result v0

    if-lt p2, v0, :cond_0

    const/4 v0, 0x0

    :goto_0
    return v0

    :cond_0
    invoke-virtual {p1}, Lcom/google/android/finsky/api/model/Bucket;->getChildCount()I

    move-result v0

    iget v1, p0, Lcom/google/android/finsky/layout/play/PlayCardListingBucketView;->mColumnCount:I

    mul-int/2addr v1, p2

    sub-int/2addr v0, v1

    iget v1, p0, Lcom/google/android/finsky/layout/play/PlayCardListingBucketView;->mColumnCount:I

    invoke-static {v0, v1}, Ljava/lang/Math;->min(II)I

    move-result v0

    goto :goto_0
.end method

.method private getDisplayedRowsForBucket(Lcom/google/android/finsky/api/model/Bucket;)I
    .locals 4
    .param p1    # Lcom/google/android/finsky/api/model/Bucket;

    invoke-virtual {p1}, Lcom/google/android/finsky/api/model/Bucket;->getChildCount()I

    move-result v0

    int-to-double v0, v0

    iget v2, p0, Lcom/google/android/finsky/layout/play/PlayCardListingBucketView;->mColumnCount:I

    int-to-double v2, v2

    div-double/2addr v0, v2

    invoke-static {v0, v1}, Ljava/lang/Math;->ceil(D)D

    move-result-wide v0

    double-to-int v0, v0

    iget v1, p0, Lcom/google/android/finsky/layout/play/PlayCardListingBucketView;->mRowCount:I

    invoke-static {v0, v1}, Ljava/lang/Math;->min(II)I

    move-result v0

    return v0
.end method


# virtual methods
.method public bind(Lcom/google/android/finsky/api/model/Bucket;Lcom/google/android/finsky/utils/BitmapLoader;Lcom/google/android/finsky/navigationmanager/NavigationManager;Ljava/lang/String;ZLandroid/view/View$OnClickListener;Lcom/google/android/finsky/layout/play/PlayStoreUiElementNode;)V
    .locals 2
    .param p1    # Lcom/google/android/finsky/api/model/Bucket;
    .param p2    # Lcom/google/android/finsky/utils/BitmapLoader;
    .param p3    # Lcom/google/android/finsky/navigationmanager/NavigationManager;
    .param p4    # Ljava/lang/String;
    .param p5    # Z
    .param p6    # Landroid/view/View$OnClickListener;
    .param p7    # Lcom/google/android/finsky/layout/play/PlayStoreUiElementNode;

    iget-object v0, p0, Lcom/google/android/finsky/layout/play/PlayCardListingBucketView;->mUiElementProto:Lcom/google/android/play/analytics/PlayStore$PlayStoreUiElement;

    invoke-virtual {p1}, Lcom/google/android/finsky/api/model/Bucket;->getServerLogsCookie()Lcom/google/protobuf/micro/ByteStringMicro;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/google/android/finsky/analytics/FinskyEventLog;->setServerLogCookie(Lcom/google/android/play/analytics/PlayStore$PlayStoreUiElement;Lcom/google/protobuf/micro/ByteStringMicro;)Lcom/google/android/play/analytics/PlayStore$PlayStoreUiElement;

    iput-object p7, p0, Lcom/google/android/finsky/layout/play/PlayCardListingBucketView;->mParentNode:Lcom/google/android/finsky/layout/play/PlayStoreUiElementNode;

    invoke-direct/range {p0 .. p5}, Lcom/google/android/finsky/layout/play/PlayCardListingBucketView;->bindBucketEntries(Lcom/google/android/finsky/api/model/Bucket;Lcom/google/android/finsky/utils/BitmapLoader;Lcom/google/android/finsky/navigationmanager/NavigationManager;Ljava/lang/String;Z)V

    invoke-direct {p0, p1, p6}, Lcom/google/android/finsky/layout/play/PlayCardListingBucketView;->bindBucketHeader(Lcom/google/android/finsky/api/model/Bucket;Landroid/view/View$OnClickListener;)V

    return-void
.end method

.method public childImpression(Lcom/google/android/finsky/layout/play/PlayStoreUiElementNode;)V
    .locals 0
    .param p1    # Lcom/google/android/finsky/layout/play/PlayStoreUiElementNode;

    invoke-static {p0, p1}, Lcom/google/android/finsky/analytics/FinskyEventLog;->childImpression(Lcom/google/android/finsky/layout/play/PlayStoreUiElementNode;Lcom/google/android/finsky/layout/play/PlayStoreUiElementNode;)V

    return-void
.end method

.method public getParentNode()Lcom/google/android/finsky/layout/play/PlayStoreUiElementNode;
    .locals 1

    iget-object v0, p0, Lcom/google/android/finsky/layout/play/PlayCardListingBucketView;->mParentNode:Lcom/google/android/finsky/layout/play/PlayStoreUiElementNode;

    return-object v0
.end method

.method public getPlayStoreUiElement()Lcom/google/android/play/analytics/PlayStore$PlayStoreUiElement;
    .locals 1

    iget-object v0, p0, Lcom/google/android/finsky/layout/play/PlayCardListingBucketView;->mUiElementProto:Lcom/google/android/play/analytics/PlayStore$PlayStoreUiElement;

    return-object v0
.end method

.method public inflateGrid(II)V
    .locals 8
    .param p1    # I
    .param p2    # I

    const/4 v7, 0x0

    iput p1, p0, Lcom/google/android/finsky/layout/play/PlayCardListingBucketView;->mRowCount:I

    iput p2, p0, Lcom/google/android/finsky/layout/play/PlayCardListingBucketView;->mColumnCount:I

    invoke-virtual {p0}, Lcom/google/android/finsky/layout/play/PlayCardListingBucketView;->getContext()Landroid/content/Context;

    move-result-object v5

    invoke-static {v5}, Landroid/view/LayoutInflater;->from(Landroid/content/Context;)Landroid/view/LayoutInflater;

    move-result-object v1

    const/4 v4, 0x0

    :goto_0
    iget v5, p0, Lcom/google/android/finsky/layout/play/PlayCardListingBucketView;->mRowCount:I

    if-ge v4, v5, :cond_1

    const v5, 0x7f040026    # com.android.vending.R.layout.bucket_row

    const/4 v6, 0x0

    invoke-virtual {v1, v5, v6, v7}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;Z)Landroid/view/View;

    move-result-object v3

    check-cast v3, Landroid/view/ViewGroup;

    const/4 v0, 0x0

    :goto_1
    iget v5, p0, Lcom/google/android/finsky/layout/play/PlayCardListingBucketView;->mColumnCount:I

    if-ge v0, v5, :cond_0

    const v5, 0x7f0400d7    # com.android.vending.R.layout.play_card_listing

    invoke-virtual {v1, v5, v3, v7}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;Z)Landroid/view/View;

    move-result-object v2

    const/16 v5, 0x8

    invoke-virtual {v2, v5}, Landroid/view/View;->setVisibility(I)V

    invoke-virtual {v3, v2}, Landroid/view/ViewGroup;->addView(Landroid/view/View;)V

    add-int/lit8 v0, v0, 0x1

    goto :goto_1

    :cond_0
    iget-object v5, p0, Lcom/google/android/finsky/layout/play/PlayCardListingBucketView;->mContent:Landroid/widget/LinearLayout;

    invoke-virtual {v5, v3}, Landroid/widget/LinearLayout;->addView(Landroid/view/View;)V

    add-int/lit8 v4, v4, 0x1

    goto :goto_0

    :cond_1
    return-void
.end method

.method protected onFinishInflate()V
    .locals 1

    invoke-super {p0}, Landroid/widget/LinearLayout;->onFinishInflate()V

    const v0, 0x7f0801ba    # com.android.vending.R.id.bucket_content

    invoke-virtual {p0, v0}, Lcom/google/android/finsky/layout/play/PlayCardListingBucketView;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/LinearLayout;

    iput-object v0, p0, Lcom/google/android/finsky/layout/play/PlayCardListingBucketView;->mContent:Landroid/widget/LinearLayout;

    const v0, 0x7f0800de    # com.android.vending.R.id.cluster_header

    invoke-virtual {p0, v0}, Lcom/google/android/finsky/layout/play/PlayCardListingBucketView;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/google/android/finsky/layout/play/PlayCardClusterViewHeader;

    iput-object v0, p0, Lcom/google/android/finsky/layout/play/PlayCardListingBucketView;->mHeader:Lcom/google/android/finsky/layout/play/PlayCardClusterViewHeader;

    return-void
.end method
