.class public Lcom/google/android/finsky/layout/play/PlayCardSubtitlePrice;
.super Landroid/widget/RelativeLayout;
.source "PlayCardSubtitlePrice.java"


# instance fields
.field protected mPrice:Lcom/google/android/finsky/layout/play/PlayActionButton;

.field protected mSubtitle:Lcom/google/android/finsky/layout/DecoratedTextView;


# direct methods
.method public constructor <init>(Landroid/content/Context;)V
    .locals 1
    .param p1    # Landroid/content/Context;

    const/4 v0, 0x0

    invoke-direct {p0, p1, v0}, Lcom/google/android/finsky/layout/play/PlayCardSubtitlePrice;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;)V

    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;)V
    .locals 0
    .param p1    # Landroid/content/Context;
    .param p2    # Landroid/util/AttributeSet;

    invoke-direct {p0, p1, p2}, Landroid/widget/RelativeLayout;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;)V

    return-void
.end method


# virtual methods
.method protected onFinishInflate()V
    .locals 1

    invoke-super {p0}, Landroid/widget/RelativeLayout;->onFinishInflate()V

    const v0, 0x7f080143    # com.android.vending.R.id.li_subtitle

    invoke-virtual {p0, v0}, Lcom/google/android/finsky/layout/play/PlayCardSubtitlePrice;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/google/android/finsky/layout/DecoratedTextView;

    iput-object v0, p0, Lcom/google/android/finsky/layout/play/PlayCardSubtitlePrice;->mSubtitle:Lcom/google/android/finsky/layout/DecoratedTextView;

    const v0, 0x7f08010d    # com.android.vending.R.id.li_price

    invoke-virtual {p0, v0}, Lcom/google/android/finsky/layout/play/PlayCardSubtitlePrice;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/google/android/finsky/layout/play/PlayActionButton;

    iput-object v0, p0, Lcom/google/android/finsky/layout/play/PlayCardSubtitlePrice;->mPrice:Lcom/google/android/finsky/layout/play/PlayActionButton;

    return-void
.end method

.method protected onLayout(ZIIII)V
    .locals 8
    .param p1    # Z
    .param p2    # I
    .param p3    # I
    .param p4    # I
    .param p5    # I

    const/4 v7, 0x0

    invoke-virtual {p0}, Lcom/google/android/finsky/layout/play/PlayCardSubtitlePrice;->getWidth()I

    move-result v3

    iget-object v4, p0, Lcom/google/android/finsky/layout/play/PlayCardSubtitlePrice;->mSubtitle:Lcom/google/android/finsky/layout/DecoratedTextView;

    iget-object v5, p0, Lcom/google/android/finsky/layout/play/PlayCardSubtitlePrice;->mSubtitle:Lcom/google/android/finsky/layout/DecoratedTextView;

    invoke-virtual {v5}, Lcom/google/android/finsky/layout/DecoratedTextView;->getMeasuredWidth()I

    move-result v5

    iget-object v6, p0, Lcom/google/android/finsky/layout/play/PlayCardSubtitlePrice;->mSubtitle:Lcom/google/android/finsky/layout/DecoratedTextView;

    invoke-virtual {v6}, Lcom/google/android/finsky/layout/DecoratedTextView;->getMeasuredHeight()I

    move-result v6

    invoke-virtual {v4, v7, v7, v5, v6}, Lcom/google/android/finsky/layout/DecoratedTextView;->layout(IIII)V

    iget-object v4, p0, Lcom/google/android/finsky/layout/play/PlayCardSubtitlePrice;->mPrice:Lcom/google/android/finsky/layout/play/PlayActionButton;

    invoke-virtual {v4}, Lcom/google/android/finsky/layout/play/PlayActionButton;->getLayoutParams()Landroid/view/ViewGroup$LayoutParams;

    move-result-object v0

    check-cast v0, Landroid/view/ViewGroup$MarginLayoutParams;

    iget-object v4, p0, Lcom/google/android/finsky/layout/play/PlayCardSubtitlePrice;->mSubtitle:Lcom/google/android/finsky/layout/DecoratedTextView;

    invoke-virtual {v4}, Lcom/google/android/finsky/layout/DecoratedTextView;->getBaseline()I

    move-result v4

    iget-object v5, p0, Lcom/google/android/finsky/layout/play/PlayCardSubtitlePrice;->mPrice:Lcom/google/android/finsky/layout/play/PlayActionButton;

    invoke-virtual {v5}, Lcom/google/android/finsky/layout/play/PlayActionButton;->getBaseline()I

    move-result v5

    sub-int v2, v4, v5

    iget v4, v0, Landroid/view/ViewGroup$MarginLayoutParams;->rightMargin:I

    sub-int v1, v3, v4

    iget-object v4, p0, Lcom/google/android/finsky/layout/play/PlayCardSubtitlePrice;->mPrice:Lcom/google/android/finsky/layout/play/PlayActionButton;

    iget-object v5, p0, Lcom/google/android/finsky/layout/play/PlayCardSubtitlePrice;->mPrice:Lcom/google/android/finsky/layout/play/PlayActionButton;

    invoke-virtual {v5}, Lcom/google/android/finsky/layout/play/PlayActionButton;->getMeasuredWidth()I

    move-result v5

    sub-int v5, v1, v5

    iget-object v6, p0, Lcom/google/android/finsky/layout/play/PlayCardSubtitlePrice;->mPrice:Lcom/google/android/finsky/layout/play/PlayActionButton;

    invoke-virtual {v6}, Lcom/google/android/finsky/layout/play/PlayActionButton;->getMeasuredHeight()I

    move-result v6

    add-int/2addr v6, v2

    invoke-virtual {v4, v5, v2, v1, v6}, Lcom/google/android/finsky/layout/play/PlayActionButton;->layout(IIII)V

    return-void
.end method

.method protected onMeasure(II)V
    .locals 7
    .param p1    # I
    .param p2    # I

    const/4 v6, 0x0

    invoke-static {p1}, Landroid/view/View$MeasureSpec;->getSize(I)I

    move-result v2

    iget-object v3, p0, Lcom/google/android/finsky/layout/play/PlayCardSubtitlePrice;->mPrice:Lcom/google/android/finsky/layout/play/PlayActionButton;

    invoke-virtual {v3, v6, v6}, Lcom/google/android/finsky/layout/play/PlayActionButton;->measure(II)V

    iget-object v3, p0, Lcom/google/android/finsky/layout/play/PlayCardSubtitlePrice;->mSubtitle:Lcom/google/android/finsky/layout/DecoratedTextView;

    invoke-virtual {v3, v6, v6}, Lcom/google/android/finsky/layout/DecoratedTextView;->measure(II)V

    iget-object v3, p0, Lcom/google/android/finsky/layout/play/PlayCardSubtitlePrice;->mPrice:Lcom/google/android/finsky/layout/play/PlayActionButton;

    invoke-virtual {v3}, Lcom/google/android/finsky/layout/play/PlayActionButton;->getLayoutParams()Landroid/view/ViewGroup$LayoutParams;

    move-result-object v0

    check-cast v0, Landroid/view/ViewGroup$MarginLayoutParams;

    iget-object v3, p0, Lcom/google/android/finsky/layout/play/PlayCardSubtitlePrice;->mSubtitle:Lcom/google/android/finsky/layout/DecoratedTextView;

    invoke-virtual {v3}, Lcom/google/android/finsky/layout/DecoratedTextView;->getMeasuredWidth()I

    move-result v3

    iget-object v4, p0, Lcom/google/android/finsky/layout/play/PlayCardSubtitlePrice;->mPrice:Lcom/google/android/finsky/layout/play/PlayActionButton;

    invoke-virtual {v4}, Lcom/google/android/finsky/layout/play/PlayActionButton;->getMeasuredWidth()I

    move-result v4

    sub-int v4, v2, v4

    iget v5, v0, Landroid/view/ViewGroup$MarginLayoutParams;->leftMargin:I

    sub-int/2addr v4, v5

    iget v5, v0, Landroid/view/ViewGroup$MarginLayoutParams;->rightMargin:I

    sub-int/2addr v4, v5

    invoke-static {v3, v4}, Ljava/lang/Math;->min(II)I

    move-result v1

    iget-object v3, p0, Lcom/google/android/finsky/layout/play/PlayCardSubtitlePrice;->mSubtitle:Lcom/google/android/finsky/layout/DecoratedTextView;

    const/high16 v4, 0x40000000    # 2.0f

    invoke-static {v1, v4}, Landroid/view/View$MeasureSpec;->makeMeasureSpec(II)I

    move-result v4

    invoke-virtual {v3, v4, v6}, Lcom/google/android/finsky/layout/DecoratedTextView;->measure(II)V

    iget-object v3, p0, Lcom/google/android/finsky/layout/play/PlayCardSubtitlePrice;->mSubtitle:Lcom/google/android/finsky/layout/DecoratedTextView;

    invoke-virtual {v3}, Lcom/google/android/finsky/layout/DecoratedTextView;->getMeasuredHeight()I

    move-result v3

    invoke-virtual {p0, v2, v3}, Lcom/google/android/finsky/layout/play/PlayCardSubtitlePrice;->setMeasuredDimension(II)V

    return-void
.end method
