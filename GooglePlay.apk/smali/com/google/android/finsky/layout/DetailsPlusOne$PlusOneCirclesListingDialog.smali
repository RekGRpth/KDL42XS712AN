.class public Lcom/google/android/finsky/layout/DetailsPlusOne$PlusOneCirclesListingDialog;
.super Landroid/support/v4/app/DialogFragment;
.source "DetailsPlusOne.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/google/android/finsky/layout/DetailsPlusOne;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x9
    name = "PlusOneCirclesListingDialog"
.end annotation

.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/google/android/finsky/layout/DetailsPlusOne$PlusOneCirclesListingDialog$PlusOneCirclesAdapter;,
        Lcom/google/android/finsky/layout/DetailsPlusOne$PlusOneCirclesListingDialog$CircleRowHolder;
    }
.end annotation


# direct methods
.method public constructor <init>()V
    .locals 0

    invoke-direct {p0}, Landroid/support/v4/app/DialogFragment;-><init>()V

    return-void
.end method

.method static showInfo(Landroid/support/v4/app/FragmentManager;Lcom/google/android/finsky/protos/DocAnnotations$PlusOneData;)V
    .locals 4
    .param p0    # Landroid/support/v4/app/FragmentManager;
    .param p1    # Lcom/google/android/finsky/protos/DocAnnotations$PlusOneData;

    const-string v2, "plus_one_circles_dialog"

    invoke-virtual {p0, v2}, Landroid/support/v4/app/FragmentManager;->findFragmentByTag(Ljava/lang/String;)Landroid/support/v4/app/Fragment;

    move-result-object v2

    if-eqz v2, :cond_0

    :goto_0
    return-void

    :cond_0
    new-instance v0, Landroid/os/Bundle;

    invoke-direct {v0}, Landroid/os/Bundle;-><init>()V

    const-string v2, "plus_one_info"

    new-instance v3, Lcom/google/android/finsky/layout/DetailsPlusOne$ParcelablePlusOneData;

    invoke-direct {v3, p1}, Lcom/google/android/finsky/layout/DetailsPlusOne$ParcelablePlusOneData;-><init>(Lcom/google/android/finsky/protos/DocAnnotations$PlusOneData;)V

    invoke-virtual {v0, v2, v3}, Landroid/os/Bundle;->putParcelable(Ljava/lang/String;Landroid/os/Parcelable;)V

    new-instance v1, Lcom/google/android/finsky/layout/DetailsPlusOne$PlusOneCirclesListingDialog;

    invoke-direct {v1}, Lcom/google/android/finsky/layout/DetailsPlusOne$PlusOneCirclesListingDialog;-><init>()V

    invoke-virtual {v1, v0}, Lcom/google/android/finsky/layout/DetailsPlusOne$PlusOneCirclesListingDialog;->setArguments(Landroid/os/Bundle;)V

    const-string v2, "plus_one_circles_dialog"

    invoke-virtual {v1, p0, v2}, Lcom/google/android/finsky/layout/DetailsPlusOne$PlusOneCirclesListingDialog;->show(Landroid/support/v4/app/FragmentManager;Ljava/lang/String;)V

    goto :goto_0
.end method


# virtual methods
.method public onCreateDialog(Landroid/os/Bundle;)Landroid/app/Dialog;
    .locals 6
    .param p1    # Landroid/os/Bundle;

    new-instance v1, Landroid/view/ContextThemeWrapper;

    invoke-virtual {p0}, Lcom/google/android/finsky/layout/DetailsPlusOne$PlusOneCirclesListingDialog;->getActivity()Landroid/support/v4/app/FragmentActivity;

    move-result-object v4

    const v5, 0x7f0d0054    # com.android.vending.R.style.FinskyDialogTheme

    invoke-direct {v1, v4, v5}, Landroid/view/ContextThemeWrapper;-><init>(Landroid/content/Context;I)V

    invoke-virtual {p0}, Lcom/google/android/finsky/layout/DetailsPlusOne$PlusOneCirclesListingDialog;->getArguments()Landroid/os/Bundle;

    move-result-object v4

    const-string v5, "plus_one_info"

    invoke-virtual {v4, v5}, Landroid/os/Bundle;->getParcelable(Ljava/lang/String;)Landroid/os/Parcelable;

    move-result-object v2

    check-cast v2, Lcom/google/android/finsky/layout/DetailsPlusOne$ParcelablePlusOneData;

    new-instance v0, Lcom/google/android/finsky/layout/DetailsPlusOne$PlusOneCirclesListingDialog$PlusOneCirclesAdapter;

    invoke-static {}, Lcom/google/android/finsky/FinskyApp;->get()Lcom/google/android/finsky/FinskyApp;

    move-result-object v4

    invoke-virtual {v4}, Lcom/google/android/finsky/FinskyApp;->getBitmapLoader()Lcom/google/android/finsky/utils/BitmapLoader;

    move-result-object v4

    # getter for: Lcom/google/android/finsky/layout/DetailsPlusOne$ParcelablePlusOneData;->mPlusOneData:Lcom/google/android/finsky/protos/DocAnnotations$PlusOneData;
    invoke-static {v2}, Lcom/google/android/finsky/layout/DetailsPlusOne$ParcelablePlusOneData;->access$000(Lcom/google/android/finsky/layout/DetailsPlusOne$ParcelablePlusOneData;)Lcom/google/android/finsky/protos/DocAnnotations$PlusOneData;

    move-result-object v5

    invoke-direct {v0, v1, v4, v5}, Lcom/google/android/finsky/layout/DetailsPlusOne$PlusOneCirclesListingDialog$PlusOneCirclesAdapter;-><init>(Landroid/content/Context;Lcom/google/android/finsky/utils/BitmapLoader;Lcom/google/android/finsky/protos/DocAnnotations$PlusOneData;)V

    new-instance v4, Landroid/app/AlertDialog$Builder;

    invoke-direct {v4, v1}, Landroid/app/AlertDialog$Builder;-><init>(Landroid/content/Context;)V

    const v5, 0x7f070279    # com.android.vending.R.string.plus_one_circles_listing_dialog_title

    invoke-virtual {v1, v5}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v4, v5}, Landroid/app/AlertDialog$Builder;->setTitle(Ljava/lang/CharSequence;)Landroid/app/AlertDialog$Builder;

    move-result-object v4

    const/4 v5, 0x0

    invoke-virtual {v4, v0, v5}, Landroid/app/AlertDialog$Builder;->setAdapter(Landroid/widget/ListAdapter;Landroid/content/DialogInterface$OnClickListener;)Landroid/app/AlertDialog$Builder;

    move-result-object v4

    invoke-virtual {v4}, Landroid/app/AlertDialog$Builder;->create()Landroid/app/AlertDialog;

    move-result-object v3

    const/4 v4, 0x1

    invoke-virtual {v3, v4}, Landroid/app/Dialog;->setCanceledOnTouchOutside(Z)V

    return-object v3
.end method
