.class public Lcom/google/android/finsky/layout/UpdateApprovalLayout;
.super Lcom/google/android/finsky/layout/MaxSizeLinearLayout;
.source "UpdateApprovalLayout.java"

# interfaces
.implements Landroid/view/View$OnClickListener;


# instance fields
.field private mAppNameView:Landroid/widget/TextView;

.field private mContinueButton:Lcom/google/android/finsky/layout/play/PlayActionButton;

.field private mLargeSizeWarningView:Landroid/widget/TextView;

.field private mPermissionsView:Lcom/google/android/finsky/layout/AppSecurityPermissions;

.field private mProgressTextView:Landroid/widget/TextView;

.field private mSkipButton:Lcom/google/android/finsky/layout/play/PlayActionButton;

.field private mTitleView:Landroid/widget/TextView;

.field private mUpdateApprovalActivity:Lcom/google/android/finsky/activities/UpdateApprovalActivity;


# direct methods
.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;)V
    .locals 0
    .param p1    # Landroid/content/Context;
    .param p2    # Landroid/util/AttributeSet;

    invoke-direct {p0, p1, p2}, Lcom/google/android/finsky/layout/MaxSizeLinearLayout;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;)V

    return-void
.end method


# virtual methods
.method public onClick(Landroid/view/View;)V
    .locals 1
    .param p1    # Landroid/view/View;

    iget-object v0, p0, Lcom/google/android/finsky/layout/UpdateApprovalLayout;->mUpdateApprovalActivity:Lcom/google/android/finsky/activities/UpdateApprovalActivity;

    if-nez v0, :cond_1

    :cond_0
    :goto_0
    return-void

    :cond_1
    iget-object v0, p0, Lcom/google/android/finsky/layout/UpdateApprovalLayout;->mContinueButton:Lcom/google/android/finsky/layout/play/PlayActionButton;

    if-ne p1, v0, :cond_2

    iget-object v0, p0, Lcom/google/android/finsky/layout/UpdateApprovalLayout;->mUpdateApprovalActivity:Lcom/google/android/finsky/activities/UpdateApprovalActivity;

    invoke-virtual {v0}, Lcom/google/android/finsky/activities/UpdateApprovalActivity;->onApprove()V

    goto :goto_0

    :cond_2
    iget-object v0, p0, Lcom/google/android/finsky/layout/UpdateApprovalLayout;->mSkipButton:Lcom/google/android/finsky/layout/play/PlayActionButton;

    if-ne p1, v0, :cond_0

    iget-object v0, p0, Lcom/google/android/finsky/layout/UpdateApprovalLayout;->mUpdateApprovalActivity:Lcom/google/android/finsky/activities/UpdateApprovalActivity;

    invoke-virtual {v0}, Lcom/google/android/finsky/activities/UpdateApprovalActivity;->onSkip()V

    goto :goto_0
.end method

.method protected onFinishInflate()V
    .locals 4

    const/4 v3, 0x3

    invoke-super {p0}, Lcom/google/android/finsky/layout/MaxSizeLinearLayout;->onFinishInflate()V

    const v0, 0x7f080076    # com.android.vending.R.id.positive_button

    invoke-virtual {p0, v0}, Lcom/google/android/finsky/layout/UpdateApprovalLayout;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/google/android/finsky/layout/play/PlayActionButton;

    iput-object v0, p0, Lcom/google/android/finsky/layout/UpdateApprovalLayout;->mContinueButton:Lcom/google/android/finsky/layout/play/PlayActionButton;

    iget-object v0, p0, Lcom/google/android/finsky/layout/UpdateApprovalLayout;->mContinueButton:Lcom/google/android/finsky/layout/play/PlayActionButton;

    const v1, 0x7f0701df    # com.android.vending.R.string.ok

    invoke-virtual {v0, v3, v1, p0}, Lcom/google/android/finsky/layout/play/PlayActionButton;->configure(IILandroid/view/View$OnClickListener;)V

    const v0, 0x7f080077    # com.android.vending.R.id.negative_button

    invoke-virtual {p0, v0}, Lcom/google/android/finsky/layout/UpdateApprovalLayout;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/google/android/finsky/layout/play/PlayActionButton;

    iput-object v0, p0, Lcom/google/android/finsky/layout/UpdateApprovalLayout;->mSkipButton:Lcom/google/android/finsky/layout/play/PlayActionButton;

    iget-object v0, p0, Lcom/google/android/finsky/layout/UpdateApprovalLayout;->mSkipButton:Lcom/google/android/finsky/layout/play/PlayActionButton;

    invoke-virtual {p0}, Lcom/google/android/finsky/layout/UpdateApprovalLayout;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    const v2, 0x7f0701d7    # com.android.vending.R.string.update_approval_no

    invoke-virtual {v1, v2}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v3, v1, p0}, Lcom/google/android/finsky/layout/play/PlayActionButton;->configure(ILjava/lang/String;Landroid/view/View$OnClickListener;)V

    const v0, 0x7f08006b    # com.android.vending.R.id.title

    invoke-virtual {p0, v0}, Lcom/google/android/finsky/layout/UpdateApprovalLayout;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    iput-object v0, p0, Lcom/google/android/finsky/layout/UpdateApprovalLayout;->mTitleView:Landroid/widget/TextView;

    const v0, 0x7f0801b0    # com.android.vending.R.id.app_name

    invoke-virtual {p0, v0}, Lcom/google/android/finsky/layout/UpdateApprovalLayout;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    iput-object v0, p0, Lcom/google/android/finsky/layout/UpdateApprovalLayout;->mAppNameView:Landroid/widget/TextView;

    const v0, 0x7f08024b    # com.android.vending.R.id.progress_text

    invoke-virtual {p0, v0}, Lcom/google/android/finsky/layout/UpdateApprovalLayout;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    iput-object v0, p0, Lcom/google/android/finsky/layout/UpdateApprovalLayout;->mProgressTextView:Landroid/widget/TextView;

    const v0, 0x7f08024c    # com.android.vending.R.id.large_update_warning

    invoke-virtual {p0, v0}, Lcom/google/android/finsky/layout/UpdateApprovalLayout;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    iput-object v0, p0, Lcom/google/android/finsky/layout/UpdateApprovalLayout;->mLargeSizeWarningView:Landroid/widget/TextView;

    const v0, 0x7f08015c    # com.android.vending.R.id.app_permissions

    invoke-virtual {p0, v0}, Lcom/google/android/finsky/layout/UpdateApprovalLayout;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/google/android/finsky/layout/AppSecurityPermissions;

    iput-object v0, p0, Lcom/google/android/finsky/layout/UpdateApprovalLayout;->mPermissionsView:Lcom/google/android/finsky/layout/AppSecurityPermissions;

    return-void
.end method

.method public saveInstanceState(Landroid/os/Bundle;)V
    .locals 1
    .param p1    # Landroid/os/Bundle;

    iget-object v0, p0, Lcom/google/android/finsky/layout/UpdateApprovalLayout;->mPermissionsView:Lcom/google/android/finsky/layout/AppSecurityPermissions;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/google/android/finsky/layout/UpdateApprovalLayout;->mPermissionsView:Lcom/google/android/finsky/layout/AppSecurityPermissions;

    invoke-virtual {v0, p1}, Lcom/google/android/finsky/layout/AppSecurityPermissions;->saveInstanceState(Landroid/os/Bundle;)V

    :cond_0
    return-void
.end method

.method public set(Ljava/lang/String;Ljava/lang/String;IIZLjava/util/List;Lcom/google/android/finsky/activities/UpdateApprovalActivity;)V
    .locals 8
    .param p1    # Ljava/lang/String;
    .param p2    # Ljava/lang/String;
    .param p3    # I
    .param p4    # I
    .param p5    # Z
    .param p7    # Lcom/google/android/finsky/activities/UpdateApprovalActivity;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/String;",
            "Ljava/lang/String;",
            "IIZ",
            "Ljava/util/List",
            "<",
            "Ljava/lang/String;",
            ">;",
            "Lcom/google/android/finsky/activities/UpdateApprovalActivity;",
            ")V"
        }
    .end annotation

    iput-object p7, p0, Lcom/google/android/finsky/layout/UpdateApprovalLayout;->mUpdateApprovalActivity:Lcom/google/android/finsky/activities/UpdateApprovalActivity;

    iget-object v0, p0, Lcom/google/android/finsky/layout/UpdateApprovalLayout;->mAppNameView:Landroid/widget/TextView;

    invoke-virtual {v0, p2}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    invoke-virtual {p0}, Lcom/google/android/finsky/layout/UpdateApprovalLayout;->getResources()Landroid/content/res/Resources;

    move-result-object v7

    iget-object v0, p0, Lcom/google/android/finsky/layout/UpdateApprovalLayout;->mProgressTextView:Landroid/widget/TextView;

    const v1, 0x7f0701d4    # com.android.vending.R.string.update_approval_progress_text

    const/4 v2, 0x2

    new-array v2, v2, [Ljava/lang/Object;

    const/4 v3, 0x0

    invoke-static {p3}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v4

    aput-object v4, v2, v3

    const/4 v3, 0x1

    invoke-static {p4}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v4

    aput-object v4, v2, v3

    invoke-virtual {v7, v1, v2}, Landroid/content/res/Resources;->getString(I[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    if-eqz p5, :cond_0

    iget-object v0, p0, Lcom/google/android/finsky/layout/UpdateApprovalLayout;->mTitleView:Landroid/widget/TextView;

    const v1, 0x7f0701d2    # com.android.vending.R.string.update_approval_large_size_title

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setText(I)V

    iget-object v0, p0, Lcom/google/android/finsky/layout/UpdateApprovalLayout;->mLargeSizeWarningView:Landroid/widget/TextView;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setVisibility(I)V

    iget-object v0, p0, Lcom/google/android/finsky/layout/UpdateApprovalLayout;->mPermissionsView:Lcom/google/android/finsky/layout/AppSecurityPermissions;

    const/16 v1, 0x8

    invoke-virtual {v0, v1}, Lcom/google/android/finsky/layout/AppSecurityPermissions;->setVisibility(I)V

    iget-object v0, p0, Lcom/google/android/finsky/layout/UpdateApprovalLayout;->mContinueButton:Lcom/google/android/finsky/layout/play/PlayActionButton;

    const v1, 0x7f0701d6    # com.android.vending.R.string.update_approval_large_size_yes

    invoke-virtual {v7, v1}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/String;->toUpperCase()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/google/android/finsky/layout/play/PlayActionButton;->setText(Ljava/lang/CharSequence;)V

    :goto_0
    return-void

    :cond_0
    iget-object v0, p0, Lcom/google/android/finsky/layout/UpdateApprovalLayout;->mTitleView:Landroid/widget/TextView;

    const v1, 0x7f0701c7    # com.android.vending.R.string.permissions_title

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setText(I)V

    iget-object v0, p0, Lcom/google/android/finsky/layout/UpdateApprovalLayout;->mLargeSizeWarningView:Landroid/widget/TextView;

    const/16 v1, 0x8

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setVisibility(I)V

    iget-object v0, p0, Lcom/google/android/finsky/layout/UpdateApprovalLayout;->mPermissionsView:Lcom/google/android/finsky/layout/AppSecurityPermissions;

    iget-object v1, p0, Lcom/google/android/finsky/layout/UpdateApprovalLayout;->mUpdateApprovalActivity:Lcom/google/android/finsky/activities/UpdateApprovalActivity;

    invoke-virtual {v1}, Lcom/google/android/finsky/activities/UpdateApprovalActivity;->getSupportFragmentManager()Landroid/support/v4/app/FragmentManager;

    move-result-object v1

    const/4 v4, 0x1

    iget-object v2, p0, Lcom/google/android/finsky/layout/UpdateApprovalLayout;->mUpdateApprovalActivity:Lcom/google/android/finsky/activities/UpdateApprovalActivity;

    invoke-virtual {v2, p6, p1}, Lcom/google/android/finsky/activities/UpdateApprovalActivity;->getPermissionInfo(Ljava/util/List;Ljava/lang/String;)Ljava/util/List;

    move-result-object v5

    const/4 v6, 0x0

    move-object v2, p1

    move-object v3, p2

    invoke-virtual/range {v0 .. v6}, Lcom/google/android/finsky/layout/AppSecurityPermissions;->bindInfo(Landroid/support/v4/app/FragmentManager;Ljava/lang/String;Ljava/lang/String;ZLjava/util/List;Landroid/os/Bundle;)V

    iget-object v0, p0, Lcom/google/android/finsky/layout/UpdateApprovalLayout;->mPermissionsView:Lcom/google/android/finsky/layout/AppSecurityPermissions;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Lcom/google/android/finsky/layout/AppSecurityPermissions;->setVisibility(I)V

    iget-object v0, p0, Lcom/google/android/finsky/layout/UpdateApprovalLayout;->mContinueButton:Lcom/google/android/finsky/layout/play/PlayActionButton;

    const v1, 0x7f0701d5    # com.android.vending.R.string.update_approval_permissions_yes

    invoke-virtual {v7, v1}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/String;->toUpperCase()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/google/android/finsky/layout/play/PlayActionButton;->setText(Ljava/lang/CharSequence;)V

    goto :goto_0
.end method
