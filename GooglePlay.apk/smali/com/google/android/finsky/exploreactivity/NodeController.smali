.class public Lcom/google/android/finsky/exploreactivity/NodeController;
.super Ljava/lang/Object;
.source "NodeController.java"

# interfaces
.implements Lcom/google/android/finsky/exploreactivity/MusicPreviewManager$MusicPreviewListener;


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/google/android/finsky/exploreactivity/NodeController$SongListener;
    }
.end annotation


# static fields
.field private static final ERROR_LISTENER:Lcom/android/volley/Response$ErrorListener;

.field public static final FIXED_CHILD_ANGLES:[F

.field private static final FIXED_GRANDCHILD_ANGLES:[F


# instance fields
.field private mActivity:Lcom/google/android/finsky/exploreactivity/ExploreActivity;

.field private final mCenter:Lcom/jme3/math/Vector2f;

.field private mCenterNode:Lcom/google/android/finsky/exploreactivity/DocumentNode;

.field private final mCenterNodeCenter:Lcom/jme3/math/Vector2f;

.field private mCurrentNodes:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Lcom/google/android/finsky/exploreactivity/DocumentNode;",
            ">;"
        }
    .end annotation
.end field

.field private final mDfeApi:Lcom/google/android/finsky/api/DfeApi;

.field private mDrawingUtils:Lcom/google/android/finsky/exploreactivity/DrawingUtils;

.field private final mFadeOutNodes:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Lcom/google/android/finsky/exploreactivity/DocumentNode;",
            ">;"
        }
    .end annotation
.end field

.field private mFadingIn:Z

.field private mFrameNum:I

.field private final mMusicPreviewManager:Lcom/google/android/finsky/exploreactivity/MusicPreviewManager;

.field private final mNodeVector:Lcom/jme3/math/Vector2f;

.field private mPlayState:I

.field private mPlaybackShouldStartTime:J

.field private mPlayer:Lcom/google/android/finsky/previews/MediaPlayerWrapper;

.field private mPreviousNodes:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Lcom/google/android/finsky/exploreactivity/DocumentNode;",
            ">;"
        }
    .end annotation
.end field

.field private final mRequestQueue:Lcom/android/volley/RequestQueue;

.field private final mRequestsToCancel:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Ljava/lang/Object;",
            ">;"
        }
    .end annotation
.end field

.field private final mReturnNodes:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Lcom/google/android/finsky/exploreactivity/DocumentNode;",
            ">;"
        }
    .end annotation
.end field

.field private mRotation:F

.field private mSeedNode:Lcom/google/android/finsky/exploreactivity/DocumentNode;

.field private final mSize:Lcom/jme3/math/Vector2f;

.field private mSongListener:Lcom/google/android/finsky/exploreactivity/NodeController$SongListener;

.field private mSongPlayingNow:Ljava/lang/String;

.field mStarHalf:Landroid/graphics/Bitmap;

.field mStarOff:Landroid/graphics/Bitmap;

.field mStarOn:Landroid/graphics/Bitmap;

.field final mUiComponents:Landroid/view/ViewGroup;

.field private mVolume:F

.field private final mWishlistEnabled:Z


# direct methods
.method static constructor <clinit>()V
    .locals 1

    const/4 v0, 0x3

    new-array v0, v0, [F

    fill-array-data v0, :array_0

    sput-object v0, Lcom/google/android/finsky/exploreactivity/NodeController;->FIXED_CHILD_ANGLES:[F

    const/4 v0, 0x2

    new-array v0, v0, [F

    fill-array-data v0, :array_1

    sput-object v0, Lcom/google/android/finsky/exploreactivity/NodeController;->FIXED_GRANDCHILD_ANGLES:[F

    new-instance v0, Lcom/google/android/finsky/exploreactivity/NodeController$1;

    invoke-direct {v0}, Lcom/google/android/finsky/exploreactivity/NodeController$1;-><init>()V

    sput-object v0, Lcom/google/android/finsky/exploreactivity/NodeController;->ERROR_LISTENER:Lcom/android/volley/Response$ErrorListener;

    return-void

    :array_0
    .array-data 4
        -0x40c2c3e7
        -0x404d473d
        -0x3ffe485a
    .end array-data

    :array_1
    .array-data 4
        -0x404adcf6
        -0x40230d86
    .end array-data
.end method

.method public constructor <init>(Lcom/google/android/finsky/exploreactivity/ExploreActivity;Lcom/google/android/finsky/api/DfeApi;Lcom/google/android/finsky/exploreactivity/MusicPreviewManager;Lcom/android/volley/RequestQueue;)V
    .locals 8
    .param p1    # Lcom/google/android/finsky/exploreactivity/ExploreActivity;
    .param p2    # Lcom/google/android/finsky/api/DfeApi;
    .param p3    # Lcom/google/android/finsky/exploreactivity/MusicPreviewManager;
    .param p4    # Lcom/android/volley/RequestQueue;

    const/4 v7, 0x0

    const/4 v6, 0x1

    const/4 v5, 0x0

    const/4 v4, 0x0

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    invoke-static {}, Lcom/google/android/finsky/utils/Lists;->newArrayList()Ljava/util/ArrayList;

    move-result-object v2

    iput-object v2, p0, Lcom/google/android/finsky/exploreactivity/NodeController;->mPreviousNodes:Ljava/util/List;

    invoke-static {}, Lcom/google/android/finsky/utils/Lists;->newArrayList()Ljava/util/ArrayList;

    move-result-object v2

    iput-object v2, p0, Lcom/google/android/finsky/exploreactivity/NodeController;->mCurrentNodes:Ljava/util/List;

    invoke-static {}, Lcom/google/android/finsky/utils/Lists;->newArrayList()Ljava/util/ArrayList;

    move-result-object v2

    iput-object v2, p0, Lcom/google/android/finsky/exploreactivity/NodeController;->mFadeOutNodes:Ljava/util/List;

    invoke-static {}, Lcom/google/android/finsky/utils/Lists;->newArrayList()Ljava/util/ArrayList;

    move-result-object v2

    iput-object v2, p0, Lcom/google/android/finsky/exploreactivity/NodeController;->mReturnNodes:Ljava/util/List;

    invoke-static {}, Lcom/google/android/finsky/utils/Lists;->newArrayList()Ljava/util/ArrayList;

    move-result-object v2

    iput-object v2, p0, Lcom/google/android/finsky/exploreactivity/NodeController;->mRequestsToCancel:Ljava/util/List;

    new-instance v2, Lcom/google/android/finsky/previews/MediaPlayerWrapper;

    new-instance v3, Lcom/google/android/finsky/exploreactivity/NodeController$2;

    invoke-direct {v3, p0}, Lcom/google/android/finsky/exploreactivity/NodeController$2;-><init>(Lcom/google/android/finsky/exploreactivity/NodeController;)V

    invoke-direct {v2, v3}, Lcom/google/android/finsky/previews/MediaPlayerWrapper;-><init>(Lcom/google/android/finsky/previews/StatusListener;)V

    iput-object v2, p0, Lcom/google/android/finsky/exploreactivity/NodeController;->mPlayer:Lcom/google/android/finsky/previews/MediaPlayerWrapper;

    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v2

    iput-wide v2, p0, Lcom/google/android/finsky/exploreactivity/NodeController;->mPlaybackShouldStartTime:J

    new-instance v2, Lcom/jme3/math/Vector2f;

    invoke-direct {v2, v5, v5}, Lcom/jme3/math/Vector2f;-><init>(FF)V

    iput-object v2, p0, Lcom/google/android/finsky/exploreactivity/NodeController;->mCenter:Lcom/jme3/math/Vector2f;

    iput v5, p0, Lcom/google/android/finsky/exploreactivity/NodeController;->mRotation:F

    iput v5, p0, Lcom/google/android/finsky/exploreactivity/NodeController;->mVolume:F

    iput-boolean v6, p0, Lcom/google/android/finsky/exploreactivity/NodeController;->mFadingIn:Z

    iput v6, p0, Lcom/google/android/finsky/exploreactivity/NodeController;->mPlayState:I

    iput-object v7, p0, Lcom/google/android/finsky/exploreactivity/NodeController;->mSongPlayingNow:Ljava/lang/String;

    new-instance v2, Lcom/jme3/math/Vector2f;

    invoke-direct {v2, v5, v5}, Lcom/jme3/math/Vector2f;-><init>(FF)V

    iput-object v2, p0, Lcom/google/android/finsky/exploreactivity/NodeController;->mCenterNodeCenter:Lcom/jme3/math/Vector2f;

    iput v4, p0, Lcom/google/android/finsky/exploreactivity/NodeController;->mFrameNum:I

    new-instance v2, Lcom/jme3/math/Vector2f;

    invoke-direct {v2, v5, v5}, Lcom/jme3/math/Vector2f;-><init>(FF)V

    iput-object v2, p0, Lcom/google/android/finsky/exploreactivity/NodeController;->mNodeVector:Lcom/jme3/math/Vector2f;

    new-instance v2, Lcom/jme3/math/Vector2f;

    invoke-direct {v2}, Lcom/jme3/math/Vector2f;-><init>()V

    iput-object v2, p0, Lcom/google/android/finsky/exploreactivity/NodeController;->mSize:Lcom/jme3/math/Vector2f;

    iput-object p2, p0, Lcom/google/android/finsky/exploreactivity/NodeController;->mDfeApi:Lcom/google/android/finsky/api/DfeApi;

    iput-object p3, p0, Lcom/google/android/finsky/exploreactivity/NodeController;->mMusicPreviewManager:Lcom/google/android/finsky/exploreactivity/MusicPreviewManager;

    iput-object p1, p0, Lcom/google/android/finsky/exploreactivity/NodeController;->mActivity:Lcom/google/android/finsky/exploreactivity/ExploreActivity;

    iput-object p4, p0, Lcom/google/android/finsky/exploreactivity/NodeController;->mRequestQueue:Lcom/android/volley/RequestQueue;

    sget-object v2, Lcom/google/android/finsky/config/G;->wishlistEnabled:Lcom/google/android/finsky/config/GservicesValue;

    invoke-virtual {v2}, Lcom/google/android/finsky/config/GservicesValue;->get()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Ljava/lang/Boolean;

    invoke-virtual {v2}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v2

    iput-boolean v2, p0, Lcom/google/android/finsky/exploreactivity/NodeController;->mWishlistEnabled:Z

    iget-object v2, p0, Lcom/google/android/finsky/exploreactivity/NodeController;->mActivity:Lcom/google/android/finsky/exploreactivity/ExploreActivity;

    invoke-virtual {v2}, Lcom/google/android/finsky/exploreactivity/ExploreActivity;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    const v2, 0x7f02009d    # com.android.vending.R.drawable.ic_rate_star_market_grid_on

    invoke-static {v1, v2}, Landroid/graphics/BitmapFactory;->decodeResource(Landroid/content/res/Resources;I)Landroid/graphics/Bitmap;

    move-result-object v2

    iput-object v2, p0, Lcom/google/android/finsky/exploreactivity/NodeController;->mStarOn:Landroid/graphics/Bitmap;

    const v2, 0x7f02009c    # com.android.vending.R.drawable.ic_rate_star_market_grid_off

    invoke-static {v1, v2}, Landroid/graphics/BitmapFactory;->decodeResource(Landroid/content/res/Resources;I)Landroid/graphics/Bitmap;

    move-result-object v2

    iput-object v2, p0, Lcom/google/android/finsky/exploreactivity/NodeController;->mStarOff:Landroid/graphics/Bitmap;

    const v2, 0x7f02009b    # com.android.vending.R.drawable.ic_rate_star_market_grid_half

    invoke-static {v1, v2}, Landroid/graphics/BitmapFactory;->decodeResource(Landroid/content/res/Resources;I)Landroid/graphics/Bitmap;

    move-result-object v2

    iput-object v2, p0, Lcom/google/android/finsky/exploreactivity/NodeController;->mStarHalf:Landroid/graphics/Bitmap;

    iget-object v2, p0, Lcom/google/android/finsky/exploreactivity/NodeController;->mActivity:Lcom/google/android/finsky/exploreactivity/ExploreActivity;

    invoke-virtual {v2}, Lcom/google/android/finsky/exploreactivity/ExploreActivity;->getApplicationContext()Landroid/content/Context;

    move-result-object v2

    const-string v3, "layout_inflater"

    invoke-virtual {v2, v3}, Landroid/content/Context;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/view/LayoutInflater;

    const v2, 0x7f04006f    # com.android.vending.R.layout.explorer_node_ui

    invoke-virtual {v0, v2, v7}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;)Landroid/view/View;

    move-result-object v2

    check-cast v2, Landroid/view/ViewGroup;

    iput-object v2, p0, Lcom/google/android/finsky/exploreactivity/NodeController;->mUiComponents:Landroid/view/ViewGroup;

    iget-object v2, p0, Lcom/google/android/finsky/exploreactivity/NodeController;->mUiComponents:Landroid/view/ViewGroup;

    new-instance v3, Landroid/widget/RelativeLayout$LayoutParams;

    invoke-direct {v3, v4, v4}, Landroid/widget/RelativeLayout$LayoutParams;-><init>(II)V

    invoke-virtual {v2, v3}, Landroid/view/ViewGroup;->setLayoutParams(Landroid/view/ViewGroup$LayoutParams;)V

    iget-object v2, p0, Lcom/google/android/finsky/exploreactivity/NodeController;->mUiComponents:Landroid/view/ViewGroup;

    invoke-virtual {v2, v4, v4}, Landroid/view/ViewGroup;->measure(II)V

    iget-object v2, p0, Lcom/google/android/finsky/exploreactivity/NodeController;->mUiComponents:Landroid/view/ViewGroup;

    invoke-virtual {v2, v4, v4, v4, v4}, Landroid/view/ViewGroup;->layout(IIII)V

    return-void
.end method

.method static synthetic access$000(Lcom/google/android/finsky/exploreactivity/NodeController;I)V
    .locals 0
    .param p0    # Lcom/google/android/finsky/exploreactivity/NodeController;
    .param p1    # I

    invoke-direct {p0, p1}, Lcom/google/android/finsky/exploreactivity/NodeController;->updatePlayState(I)V

    return-void
.end method

.method static synthetic access$100(Lcom/google/android/finsky/exploreactivity/NodeController;)F
    .locals 1
    .param p0    # Lcom/google/android/finsky/exploreactivity/NodeController;

    iget v0, p0, Lcom/google/android/finsky/exploreactivity/NodeController;->mVolume:F

    return v0
.end method

.method static synthetic access$1000(Lcom/google/android/finsky/exploreactivity/NodeController;Lcom/google/android/finsky/protos/DocList$ListResponse;)Ljava/util/List;
    .locals 1
    .param p0    # Lcom/google/android/finsky/exploreactivity/NodeController;
    .param p1    # Lcom/google/android/finsky/protos/DocList$ListResponse;

    invoke-direct {p0, p1}, Lcom/google/android/finsky/exploreactivity/NodeController;->convertDocList(Lcom/google/android/finsky/protos/DocList$ListResponse;)Ljava/util/List;

    move-result-object v0

    return-object v0
.end method

.method static synthetic access$1100(Lcom/google/android/finsky/exploreactivity/NodeController;)Lcom/google/android/finsky/exploreactivity/DocumentNode;
    .locals 1
    .param p0    # Lcom/google/android/finsky/exploreactivity/NodeController;

    iget-object v0, p0, Lcom/google/android/finsky/exploreactivity/NodeController;->mCenterNode:Lcom/google/android/finsky/exploreactivity/DocumentNode;

    return-object v0
.end method

.method static synthetic access$1200(Lcom/google/android/finsky/exploreactivity/NodeController;)Lcom/google/android/finsky/api/DfeApi;
    .locals 1
    .param p0    # Lcom/google/android/finsky/exploreactivity/NodeController;

    iget-object v0, p0, Lcom/google/android/finsky/exploreactivity/NodeController;->mDfeApi:Lcom/google/android/finsky/api/DfeApi;

    return-object v0
.end method

.method static synthetic access$200(Lcom/google/android/finsky/exploreactivity/NodeController;)Lcom/google/android/finsky/previews/MediaPlayerWrapper;
    .locals 1
    .param p0    # Lcom/google/android/finsky/exploreactivity/NodeController;

    iget-object v0, p0, Lcom/google/android/finsky/exploreactivity/NodeController;->mPlayer:Lcom/google/android/finsky/previews/MediaPlayerWrapper;

    return-object v0
.end method

.method static synthetic access$302(Lcom/google/android/finsky/exploreactivity/NodeController;Z)Z
    .locals 0
    .param p0    # Lcom/google/android/finsky/exploreactivity/NodeController;
    .param p1    # Z

    iput-boolean p1, p0, Lcom/google/android/finsky/exploreactivity/NodeController;->mFadingIn:Z

    return p1
.end method

.method static synthetic access$400(Lcom/google/android/finsky/exploreactivity/NodeController;)Ljava/util/List;
    .locals 1
    .param p0    # Lcom/google/android/finsky/exploreactivity/NodeController;

    iget-object v0, p0, Lcom/google/android/finsky/exploreactivity/NodeController;->mRequestsToCancel:Ljava/util/List;

    return-object v0
.end method

.method static synthetic access$500(Lcom/google/android/finsky/exploreactivity/NodeController;)Lcom/android/volley/RequestQueue;
    .locals 1
    .param p0    # Lcom/google/android/finsky/exploreactivity/NodeController;

    iget-object v0, p0, Lcom/google/android/finsky/exploreactivity/NodeController;->mRequestQueue:Lcom/android/volley/RequestQueue;

    return-object v0
.end method

.method static synthetic access$600(Lcom/google/android/finsky/exploreactivity/NodeController;)Lcom/google/android/finsky/exploreactivity/MusicPreviewManager;
    .locals 1
    .param p0    # Lcom/google/android/finsky/exploreactivity/NodeController;

    iget-object v0, p0, Lcom/google/android/finsky/exploreactivity/NodeController;->mMusicPreviewManager:Lcom/google/android/finsky/exploreactivity/MusicPreviewManager;

    return-object v0
.end method

.method static synthetic access$700(Lcom/google/android/finsky/exploreactivity/NodeController;)Lcom/google/android/finsky/exploreactivity/DocumentNode;
    .locals 1
    .param p0    # Lcom/google/android/finsky/exploreactivity/NodeController;

    iget-object v0, p0, Lcom/google/android/finsky/exploreactivity/NodeController;->mSeedNode:Lcom/google/android/finsky/exploreactivity/DocumentNode;

    return-object v0
.end method

.method static synthetic access$800(Lcom/google/android/finsky/exploreactivity/NodeController;Lcom/google/android/finsky/exploreactivity/DocumentNode;)V
    .locals 0
    .param p0    # Lcom/google/android/finsky/exploreactivity/NodeController;
    .param p1    # Lcom/google/android/finsky/exploreactivity/DocumentNode;

    invoke-direct {p0, p1}, Lcom/google/android/finsky/exploreactivity/NodeController;->disposeObjects(Lcom/google/android/finsky/exploreactivity/DocumentNode;)V

    return-void
.end method

.method static synthetic access$900(Lcom/google/android/finsky/exploreactivity/NodeController;)Ljava/util/List;
    .locals 1
    .param p0    # Lcom/google/android/finsky/exploreactivity/NodeController;

    iget-object v0, p0, Lcom/google/android/finsky/exploreactivity/NodeController;->mPreviousNodes:Ljava/util/List;

    return-object v0
.end method

.method private animateNode(Lcom/google/android/finsky/exploreactivity/DocumentNode;[FF)V
    .locals 12
    .param p1    # Lcom/google/android/finsky/exploreactivity/DocumentNode;
    .param p2    # [F
    .param p3    # F

    invoke-virtual {p1}, Lcom/google/android/finsky/exploreactivity/DocumentNode;->getChildNodes()Ljava/util/List;

    move-result-object v4

    invoke-interface {v4}, Ljava/util/List;->size()I

    move-result v5

    const/4 v1, 0x0

    :goto_0
    if-ge v1, v5, :cond_5

    invoke-virtual {p1, v1}, Lcom/google/android/finsky/exploreactivity/DocumentNode;->getChildNode(I)Lcom/google/android/finsky/exploreactivity/DocumentNode;

    move-result-object v2

    array-length v8, p2

    if-ge v1, v8, :cond_1

    aget v8, p2, v1

    iget v9, p0, Lcom/google/android/finsky/exploreactivity/NodeController;->mRotation:F

    add-float v7, v8, v9

    :goto_1
    invoke-direct {p0, v7}, Lcom/google/android/finsky/exploreactivity/NodeController;->normalizeAngle(F)F

    move-result v7

    invoke-virtual {v2}, Lcom/google/android/finsky/exploreactivity/DocumentNode;->getAngle()F

    move-result v3

    sub-float v0, v7, v3

    const v8, 0x40490fdb    # (float)Math.PI

    cmpl-float v8, v0, v8

    if-lez v8, :cond_3

    const v8, 0x40490fdb    # (float)Math.PI

    sub-float v0, v8, v0

    :cond_0
    :goto_2
    const/4 v8, 0x0

    cmpl-float v8, v0, v8

    if-nez v8, :cond_4

    const/4 v6, 0x0

    :goto_3
    mul-float v8, v0, v6

    float-to-double v8, v8

    const-wide v10, 0x3ff3333340000000L    # 1.2000000476837158

    invoke-static {v8, v9, v10, v11}, Ljava/lang/Math;->pow(DD)D

    move-result-wide v8

    double-to-float v8, v8

    const/high16 v9, 0x40800000    # 4.0f

    mul-float v0, v8, v9

    const v8, 0x41490fdb

    invoke-static {v0, v8}, Ljava/lang/Math;->min(FF)F

    move-result v0

    mul-float v8, p3, v6

    mul-float/2addr v0, v8

    add-float v8, v3, v0

    invoke-direct {p0, v8}, Lcom/google/android/finsky/exploreactivity/NodeController;->normalizeAngle(F)F

    move-result v8

    invoke-virtual {v2, v8}, Lcom/google/android/finsky/exploreactivity/DocumentNode;->setAngle(F)V

    add-int/lit8 v1, v1, 0x1

    goto :goto_0

    :cond_1
    sget-object v8, Lcom/google/android/finsky/exploreactivity/NodeController;->FIXED_CHILD_ANGLES:[F

    array-length v8, v8

    if-ge v1, v8, :cond_2

    const v7, 0x4034f4ab

    goto :goto_1

    :cond_2
    const v8, 0x3fc90fdb

    iget v9, p0, Lcom/google/android/finsky/exploreactivity/NodeController;->mRotation:F

    add-float v7, v8, v9

    goto :goto_1

    :cond_3
    const v8, -0x3fb6f025

    cmpg-float v8, v0, v8

    if-gez v8, :cond_0

    const v8, -0x3fb6f025

    sub-float v0, v8, v0

    goto :goto_2

    :cond_4
    invoke-static {v0}, Ljava/lang/Math;->abs(F)F

    move-result v8

    div-float v6, v0, v8

    goto :goto_3

    :cond_5
    return-void
.end method

.method private animateNodes(F)V
    .locals 8
    .param p1    # F

    const/4 v7, 0x0

    iget-object v4, p0, Lcom/google/android/finsky/exploreactivity/NodeController;->mCenterNode:Lcom/google/android/finsky/exploreactivity/DocumentNode;

    sget-object v5, Lcom/google/android/finsky/exploreactivity/NodeController;->FIXED_CHILD_ANGLES:[F

    invoke-direct {p0, v4, v5, p1}, Lcom/google/android/finsky/exploreactivity/NodeController;->animateNode(Lcom/google/android/finsky/exploreactivity/DocumentNode;[FF)V

    iget-object v4, p0, Lcom/google/android/finsky/exploreactivity/NodeController;->mCenterNode:Lcom/google/android/finsky/exploreactivity/DocumentNode;

    invoke-virtual {v4}, Lcom/google/android/finsky/exploreactivity/DocumentNode;->getChildNodes()Ljava/util/List;

    move-result-object v1

    invoke-interface {v1}, Ljava/util/List;->size()I

    move-result v2

    const/4 v0, 0x0

    :goto_0
    if-ge v0, v2, :cond_1

    sget-object v4, Lcom/google/android/finsky/exploreactivity/NodeController;->FIXED_CHILD_ANGLES:[F

    array-length v4, v4

    if-ge v0, v4, :cond_0

    iget-object v4, p0, Lcom/google/android/finsky/exploreactivity/NodeController;->mCenterNode:Lcom/google/android/finsky/exploreactivity/DocumentNode;

    invoke-virtual {v4, v0}, Lcom/google/android/finsky/exploreactivity/DocumentNode;->getChildNode(I)Lcom/google/android/finsky/exploreactivity/DocumentNode;

    move-result-object v4

    sget-object v5, Lcom/google/android/finsky/exploreactivity/NodeController;->FIXED_GRANDCHILD_ANGLES:[F

    invoke-direct {p0, v4, v5, p1}, Lcom/google/android/finsky/exploreactivity/NodeController;->animateNode(Lcom/google/android/finsky/exploreactivity/DocumentNode;[FF)V

    :cond_0
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    :cond_1
    iget-object v4, p0, Lcom/google/android/finsky/exploreactivity/NodeController;->mPlayer:Lcom/google/android/finsky/previews/MediaPlayerWrapper;

    invoke-virtual {v4}, Lcom/google/android/finsky/previews/MediaPlayerWrapper;->isPlaying()Z

    move-result v4

    if-eqz v4, :cond_2

    iget v3, p0, Lcom/google/android/finsky/exploreactivity/NodeController;->mVolume:F

    iget-boolean v4, p0, Lcom/google/android/finsky/exploreactivity/NodeController;->mFadingIn:Z

    if-eqz v4, :cond_4

    const/high16 v4, 0x3f800000    # 1.0f

    iget v5, p0, Lcom/google/android/finsky/exploreactivity/NodeController;->mVolume:F

    const/high16 v6, 0x3f000000    # 0.5f

    div-float v6, p1, v6

    add-float/2addr v5, v6

    invoke-static {v4, v5}, Ljava/lang/Math;->min(FF)F

    move-result v4

    iput v4, p0, Lcom/google/android/finsky/exploreactivity/NodeController;->mVolume:F

    :goto_1
    iget v4, p0, Lcom/google/android/finsky/exploreactivity/NodeController;->mVolume:F

    cmpl-float v4, v3, v4

    if-eqz v4, :cond_2

    iget-object v4, p0, Lcom/google/android/finsky/exploreactivity/NodeController;->mPlayer:Lcom/google/android/finsky/previews/MediaPlayerWrapper;

    iget v5, p0, Lcom/google/android/finsky/exploreactivity/NodeController;->mVolume:F

    iget v6, p0, Lcom/google/android/finsky/exploreactivity/NodeController;->mVolume:F

    invoke-virtual {v4, v5, v6}, Lcom/google/android/finsky/previews/MediaPlayerWrapper;->setVolume(FF)V

    iget v4, p0, Lcom/google/android/finsky/exploreactivity/NodeController;->mVolume:F

    cmpl-float v4, v4, v7

    if-nez v4, :cond_2

    iget-boolean v4, p0, Lcom/google/android/finsky/exploreactivity/NodeController;->mFadingIn:Z

    if-nez v4, :cond_2

    iget-object v4, p0, Lcom/google/android/finsky/exploreactivity/NodeController;->mPlayer:Lcom/google/android/finsky/previews/MediaPlayerWrapper;

    invoke-virtual {v4}, Lcom/google/android/finsky/previews/MediaPlayerWrapper;->pause()V

    :cond_2
    iget v4, p0, Lcom/google/android/finsky/exploreactivity/NodeController;->mPlayState:I

    const/4 v5, 0x2

    if-ne v4, v5, :cond_3

    iget-object v4, p0, Lcom/google/android/finsky/exploreactivity/NodeController;->mCenterNode:Lcom/google/android/finsky/exploreactivity/DocumentNode;

    invoke-virtual {v4}, Lcom/google/android/finsky/exploreactivity/DocumentNode;->getDoc()Lcom/google/android/finsky/exploreactivity/DocWrapper;

    move-result-object v4

    invoke-virtual {v4}, Lcom/google/android/finsky/exploreactivity/DocWrapper;->getSong()Lcom/google/android/finsky/api/model/Document;

    move-result-object v4

    if-eqz v4, :cond_3

    iget-object v4, p0, Lcom/google/android/finsky/exploreactivity/NodeController;->mCenterNode:Lcom/google/android/finsky/exploreactivity/DocumentNode;

    invoke-virtual {v4}, Lcom/google/android/finsky/exploreactivity/DocumentNode;->getDoc()Lcom/google/android/finsky/exploreactivity/DocWrapper;

    move-result-object v4

    invoke-virtual {v4}, Lcom/google/android/finsky/exploreactivity/DocWrapper;->getSong()Lcom/google/android/finsky/api/model/Document;

    move-result-object v4

    invoke-virtual {v4}, Lcom/google/android/finsky/api/model/Document;->getDocId()Ljava/lang/String;

    move-result-object v4

    iget-object v5, p0, Lcom/google/android/finsky/exploreactivity/NodeController;->mSongPlayingNow:Ljava/lang/String;

    invoke-virtual {v4, v5}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v4

    if-nez v4, :cond_3

    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v4

    iget-wide v6, p0, Lcom/google/android/finsky/exploreactivity/NodeController;->mPlaybackShouldStartTime:J

    sub-long/2addr v4, v6

    const-wide/16 v6, 0x1f4

    cmp-long v4, v4, v6

    if-lez v4, :cond_3

    const/4 v4, 0x3

    invoke-virtual {p0, v4}, Lcom/google/android/finsky/exploreactivity/NodeController;->resetPlayback(I)V

    :cond_3
    return-void

    :cond_4
    iget v4, p0, Lcom/google/android/finsky/exploreactivity/NodeController;->mVolume:F

    const v5, 0x3dcccccd    # 0.1f

    div-float v5, p1, v5

    sub-float/2addr v4, v5

    invoke-static {v7, v4}, Ljava/lang/Math;->max(FF)F

    move-result v4

    iput v4, p0, Lcom/google/android/finsky/exploreactivity/NodeController;->mVolume:F

    goto :goto_1
.end method

.method private calculateCenter(Lcom/google/android/finsky/exploreactivity/DocumentNode;F)V
    .locals 2
    .param p1    # Lcom/google/android/finsky/exploreactivity/DocumentNode;
    .param p2    # F

    invoke-virtual {p0, p1}, Lcom/google/android/finsky/exploreactivity/NodeController;->getNodeCenter(Lcom/google/android/finsky/exploreactivity/DocumentNode;)Lcom/jme3/math/Vector2f;

    move-result-object v0

    iget-object v1, p0, Lcom/google/android/finsky/exploreactivity/NodeController;->mCenter:Lcom/jme3/math/Vector2f;

    invoke-virtual {v0, v1}, Lcom/jme3/math/Vector2f;->distance(Lcom/jme3/math/Vector2f;)F

    move-result v1

    div-float/2addr v1, p2

    invoke-virtual {p1, v1}, Lcom/google/android/finsky/exploreactivity/DocumentNode;->setDistanceToCenter(F)V

    return-void
.end method

.method private clearNodePositions(Lcom/google/android/finsky/exploreactivity/DocumentNode;)V
    .locals 4
    .param p1    # Lcom/google/android/finsky/exploreactivity/DocumentNode;

    const/high16 v3, 0x7fc00000    # NaNf

    invoke-virtual {p1}, Lcom/google/android/finsky/exploreactivity/DocumentNode;->getCenter()Lcom/jme3/math/Vector2f;

    move-result-object v2

    invoke-virtual {v2, v3, v3}, Lcom/jme3/math/Vector2f;->set(FF)Lcom/jme3/math/Vector2f;

    invoke-virtual {p1}, Lcom/google/android/finsky/exploreactivity/DocumentNode;->getChildNodes()Ljava/util/List;

    move-result-object v0

    const/4 v1, 0x0

    :goto_0
    invoke-interface {v0}, Ljava/util/List;->size()I

    move-result v2

    if-ge v1, v2, :cond_0

    invoke-interface {v0, v1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lcom/google/android/finsky/exploreactivity/DocumentNode;

    invoke-direct {p0, v2}, Lcom/google/android/finsky/exploreactivity/NodeController;->clearNodePositions(Lcom/google/android/finsky/exploreactivity/DocumentNode;)V

    add-int/lit8 v1, v1, 0x1

    goto :goto_0

    :cond_0
    return-void
.end method

.method private convertDocList(Lcom/google/android/finsky/protos/DocList$ListResponse;)Ljava/util/List;
    .locals 6
    .param p1    # Lcom/google/android/finsky/protos/DocList$ListResponse;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/google/android/finsky/protos/DocList$ListResponse;",
            ")",
            "Ljava/util/List",
            "<",
            "Lcom/google/android/finsky/api/model/Document;",
            ">;"
        }
    .end annotation

    const/4 v4, 0x0

    new-instance v2, Ljava/util/ArrayList;

    invoke-direct {v2}, Ljava/util/ArrayList;-><init>()V

    invoke-virtual {p1}, Lcom/google/android/finsky/protos/DocList$ListResponse;->getDocCount()I

    move-result v3

    if-lez v3, :cond_0

    invoke-virtual {p1, v4}, Lcom/google/android/finsky/protos/DocList$ListResponse;->getDoc(I)Lcom/google/android/finsky/protos/DocumentV2$DocV2;

    move-result-object v3

    invoke-virtual {v3}, Lcom/google/android/finsky/protos/DocumentV2$DocV2;->getChildCount()I

    move-result v3

    if-lez v3, :cond_0

    invoke-virtual {p1, v4}, Lcom/google/android/finsky/protos/DocList$ListResponse;->getDoc(I)Lcom/google/android/finsky/protos/DocumentV2$DocV2;

    move-result-object v3

    invoke-virtual {v3}, Lcom/google/android/finsky/protos/DocumentV2$DocV2;->getChildList()Ljava/util/List;

    move-result-object v1

    const/4 v0, 0x0

    :goto_0
    invoke-interface {v1}, Ljava/util/List;->size()I

    move-result v3

    if-ge v0, v3, :cond_0

    new-instance v4, Lcom/google/android/finsky/api/model/Document;

    invoke-interface {v1, v0}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Lcom/google/android/finsky/protos/DocumentV2$DocV2;

    const/4 v5, 0x0

    invoke-direct {v4, v3, v5}, Lcom/google/android/finsky/api/model/Document;-><init>(Lcom/google/android/finsky/protos/DocumentV2$DocV2;Ljava/lang/String;)V

    invoke-virtual {v2, v4}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    :cond_0
    invoke-virtual {v2}, Ljava/util/ArrayList;->trimToSize()V

    return-object v2
.end method

.method private disposeObjects(Lcom/google/android/finsky/exploreactivity/DocumentNode;)V
    .locals 3
    .param p1    # Lcom/google/android/finsky/exploreactivity/DocumentNode;

    if-nez p1, :cond_0

    :goto_0
    return-void

    :cond_0
    invoke-virtual {p1}, Lcom/google/android/finsky/exploreactivity/DocumentNode;->disposeObjects()V

    invoke-virtual {p1}, Lcom/google/android/finsky/exploreactivity/DocumentNode;->getChildNodes()Ljava/util/List;

    move-result-object v0

    const/4 v1, 0x0

    :goto_1
    invoke-interface {v0}, Ljava/util/List;->size()I

    move-result v2

    if-ge v1, v2, :cond_1

    invoke-interface {v0, v1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lcom/google/android/finsky/exploreactivity/DocumentNode;

    invoke-direct {p0, v2}, Lcom/google/android/finsky/exploreactivity/NodeController;->disposeObjects(Lcom/google/android/finsky/exploreactivity/DocumentNode;)V

    add-int/lit8 v1, v1, 0x1

    goto :goto_1

    :cond_1
    invoke-virtual {p1}, Lcom/google/android/finsky/exploreactivity/DocumentNode;->removeFromParent()Z

    goto :goto_0
.end method

.method private dumpNodeLeaf(Lcom/google/android/finsky/exploreactivity/DocumentNode;I)V
    .locals 6

    const/4 v2, 0x0

    new-instance v1, Ljava/lang/StringBuilder;

    const/16 v0, 0x100

    invoke-direct {v1, v0}, Ljava/lang/StringBuilder;-><init>(I)V

    move v0, v2

    :goto_0
    if-ge v0, p2, :cond_0

    const-string v3, "  "

    invoke-virtual {v1, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    :cond_0
    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3, v1}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/CharSequence;)V

    invoke-virtual {p1}, Lcom/google/android/finsky/exploreactivity/DocumentNode;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    new-array v1, v2, [Ljava/lang/Object;

    invoke-static {v0, v1}, Lcom/google/android/finsky/utils/FinskyLog;->d(Ljava/lang/String;[Ljava/lang/Object;)V

    iget-object v0, p1, Lcom/google/android/finsky/exploreactivity/DocumentNode;->mDocWrapper:Lcom/google/android/finsky/exploreactivity/DocWrapper;

    invoke-virtual {v0}, Lcom/google/android/finsky/exploreactivity/DocWrapper;->getRelations()Ljava/util/List;

    move-result-object v4

    if-eqz v4, :cond_1

    move v1, v2

    :goto_1
    invoke-interface {v4}, Ljava/util/List;->size()I

    move-result v0

    if-ge v1, v0, :cond_1

    new-instance v5, Ljava/lang/StringBuilder;

    invoke-direct {v5, v3}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/CharSequence;)V

    const-string v0, "  - "

    invoke-virtual {v5, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-interface {v4, v1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/finsky/exploreactivity/DocWrapper;

    invoke-virtual {v0}, Lcom/google/android/finsky/exploreactivity/DocWrapper;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v5, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    new-array v5, v2, [Ljava/lang/Object;

    invoke-static {v0, v5}, Lcom/google/android/finsky/utils/FinskyLog;->d(Ljava/lang/String;[Ljava/lang/Object;)V

    add-int/lit8 v0, v1, 0x1

    move v1, v0

    goto :goto_1

    :cond_1
    invoke-virtual {p1}, Lcom/google/android/finsky/exploreactivity/DocumentNode;->getChildNodes()Ljava/util/List;

    move-result-object v1

    :goto_2
    invoke-interface {v1}, Ljava/util/List;->size()I

    move-result v0

    if-ge v2, v0, :cond_2

    invoke-interface {v1, v2}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/finsky/exploreactivity/DocumentNode;

    add-int/lit8 v3, p2, 0x1

    invoke-direct {p0, v0, v3}, Lcom/google/android/finsky/exploreactivity/NodeController;->dumpNodeLeaf(Lcom/google/android/finsky/exploreactivity/DocumentNode;I)V

    add-int/lit8 v2, v2, 0x1

    goto :goto_2

    :cond_2
    return-void
.end method

.method private normalizeAngle(F)F
    .locals 2
    .param p1    # F

    const v1, 0x40c90fdb

    :goto_0
    cmpl-float v0, p1, v1

    if-lez v0, :cond_0

    sub-float/2addr p1, v1

    goto :goto_0

    :cond_0
    :goto_1
    const/4 v0, 0x0

    cmpg-float v0, p1, v0

    if-gez v0, :cond_1

    add-float/2addr p1, v1

    goto :goto_1

    :cond_1
    return p1
.end method

.method private prepareOnscreenNodes(Lcom/google/android/finsky/exploreactivity/DocumentNode;F)V
    .locals 8
    .param p1    # Lcom/google/android/finsky/exploreactivity/DocumentNode;
    .param p2    # F

    const/16 v7, 0xf

    const/4 v4, 0x0

    const/4 v2, 0x0

    iget-object v6, p0, Lcom/google/android/finsky/exploreactivity/NodeController;->mCenterNode:Lcom/google/android/finsky/exploreactivity/DocumentNode;

    if-ne p1, v6, :cond_3

    sget-object v6, Lcom/android/volley/Request$Priority;->HIGH:Lcom/android/volley/Request$Priority;

    invoke-virtual {p0, p1, v7, v6}, Lcom/google/android/finsky/exploreactivity/NodeController;->loadData(Lcom/google/android/finsky/exploreactivity/DocumentNode;ILcom/android/volley/Request$Priority;)V

    iget-object v6, p0, Lcom/google/android/finsky/exploreactivity/NodeController;->mCenterNode:Lcom/google/android/finsky/exploreactivity/DocumentNode;

    const/16 v7, 0x8

    invoke-virtual {v6, v7}, Lcom/google/android/finsky/exploreactivity/DocumentNode;->createChildren(I)V

    const/4 v4, 0x1

    :cond_0
    :goto_0
    if-eqz v4, :cond_1

    iget-object v6, p0, Lcom/google/android/finsky/exploreactivity/NodeController;->mCurrentNodes:Ljava/util/List;

    invoke-interface {v6, p1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    invoke-direct {p0, p1, p2}, Lcom/google/android/finsky/exploreactivity/NodeController;->calculateCenter(Lcom/google/android/finsky/exploreactivity/DocumentNode;F)V

    :cond_1
    if-nez v4, :cond_8

    if-nez v2, :cond_8

    iget-object v6, p1, Lcom/google/android/finsky/exploreactivity/DocumentNode;->mDocWrapper:Lcom/google/android/finsky/exploreactivity/DocWrapper;

    invoke-virtual {v6}, Lcom/google/android/finsky/exploreactivity/DocWrapper;->getInProgressState()I

    move-result v6

    if-eqz v6, :cond_2

    iget-object v6, p0, Lcom/google/android/finsky/exploreactivity/NodeController;->mRequestsToCancel:Ljava/util/List;

    iget-object v7, p1, Lcom/google/android/finsky/exploreactivity/DocumentNode;->mDocWrapper:Lcom/google/android/finsky/exploreactivity/DocWrapper;

    invoke-interface {v6, v7}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    :cond_2
    iget-object v6, p1, Lcom/google/android/finsky/exploreactivity/DocumentNode;->mDocWrapper:Lcom/google/android/finsky/exploreactivity/DocWrapper;

    invoke-virtual {v6}, Lcom/google/android/finsky/exploreactivity/DocWrapper;->getRelations()Ljava/util/List;

    move-result-object v5

    if-eqz v5, :cond_7

    iget-object v6, p0, Lcom/google/android/finsky/exploreactivity/NodeController;->mRequestsToCancel:Ljava/util/List;

    invoke-interface {v6, v5}, Ljava/util/List;->addAll(Ljava/util/Collection;)Z

    invoke-virtual {p1}, Lcom/google/android/finsky/exploreactivity/DocumentNode;->getChildNodes()Ljava/util/List;

    move-result-object v0

    const/4 v1, 0x0

    :goto_1
    invoke-interface {v0}, Ljava/util/List;->size()I

    move-result v6

    if-ge v1, v6, :cond_7

    iget-object v7, p0, Lcom/google/android/finsky/exploreactivity/NodeController;->mRequestsToCancel:Ljava/util/List;

    invoke-interface {v0, v1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v6

    check-cast v6, Lcom/google/android/finsky/exploreactivity/DocumentNode;

    iget-object v6, v6, Lcom/google/android/finsky/exploreactivity/DocumentNode;->mDocWrapper:Lcom/google/android/finsky/exploreactivity/DocWrapper;

    invoke-interface {v7, v6}, Ljava/util/List;->remove(Ljava/lang/Object;)Z

    add-int/lit8 v1, v1, 0x1

    goto :goto_1

    :cond_3
    iget-object v6, p0, Lcom/google/android/finsky/exploreactivity/NodeController;->mCenterNode:Lcom/google/android/finsky/exploreactivity/DocumentNode;

    invoke-virtual {v6}, Lcom/google/android/finsky/exploreactivity/DocumentNode;->hasParentNode()Z

    move-result v6

    if-eqz v6, :cond_4

    iget-object v6, p0, Lcom/google/android/finsky/exploreactivity/NodeController;->mCenterNode:Lcom/google/android/finsky/exploreactivity/DocumentNode;

    invoke-virtual {v6}, Lcom/google/android/finsky/exploreactivity/DocumentNode;->getParentNode()Lcom/google/android/finsky/exploreactivity/DocumentNode;

    move-result-object v6

    if-ne v6, p1, :cond_4

    sget-object v6, Lcom/android/volley/Request$Priority;->LOW:Lcom/android/volley/Request$Priority;

    invoke-virtual {p0, p1, v7, v6}, Lcom/google/android/finsky/exploreactivity/NodeController;->loadData(Lcom/google/android/finsky/exploreactivity/DocumentNode;ILcom/android/volley/Request$Priority;)V

    const/4 v2, 0x1

    goto :goto_0

    :cond_4
    invoke-virtual {p1}, Lcom/google/android/finsky/exploreactivity/DocumentNode;->hasParentNode()Z

    move-result v6

    if-eqz v6, :cond_6

    invoke-virtual {p1}, Lcom/google/android/finsky/exploreactivity/DocumentNode;->getParentNode()Lcom/google/android/finsky/exploreactivity/DocumentNode;

    move-result-object v6

    iget-object v7, p0, Lcom/google/android/finsky/exploreactivity/NodeController;->mCenterNode:Lcom/google/android/finsky/exploreactivity/DocumentNode;

    if-ne v6, v7, :cond_6

    const/16 v6, 0xb

    sget-object v7, Lcom/android/volley/Request$Priority;->NORMAL:Lcom/android/volley/Request$Priority;

    invoke-virtual {p0, p1, v6, v7}, Lcom/google/android/finsky/exploreactivity/NodeController;->loadData(Lcom/google/android/finsky/exploreactivity/DocumentNode;ILcom/android/volley/Request$Priority;)V

    iget-object v6, p0, Lcom/google/android/finsky/exploreactivity/NodeController;->mCenterNode:Lcom/google/android/finsky/exploreactivity/DocumentNode;

    invoke-virtual {v6, p1}, Lcom/google/android/finsky/exploreactivity/DocumentNode;->getChildNodePos(Lcom/google/android/finsky/exploreactivity/DocumentNode;)I

    move-result v6

    sget-object v7, Lcom/google/android/finsky/exploreactivity/NodeController;->FIXED_CHILD_ANGLES:[F

    array-length v7, v7

    if-ge v6, v7, :cond_5

    const/4 v6, 0x4

    sget-object v7, Lcom/android/volley/Request$Priority;->NORMAL:Lcom/android/volley/Request$Priority;

    invoke-virtual {p0, p1, v6, v7}, Lcom/google/android/finsky/exploreactivity/NodeController;->loadData(Lcom/google/android/finsky/exploreactivity/DocumentNode;ILcom/android/volley/Request$Priority;)V

    sget-object v6, Lcom/google/android/finsky/exploreactivity/NodeController;->FIXED_CHILD_ANGLES:[F

    array-length v6, v6

    invoke-virtual {p1, v6}, Lcom/google/android/finsky/exploreactivity/DocumentNode;->createChildren(I)V

    const/4 v4, 0x1

    goto/16 :goto_0

    :cond_5
    const/4 v2, 0x1

    goto/16 :goto_0

    :cond_6
    invoke-virtual {p1}, Lcom/google/android/finsky/exploreactivity/DocumentNode;->hasParentNode()Z

    move-result v6

    if-eqz v6, :cond_0

    invoke-virtual {p1}, Lcom/google/android/finsky/exploreactivity/DocumentNode;->getParentNode()Lcom/google/android/finsky/exploreactivity/DocumentNode;

    move-result-object v6

    invoke-virtual {v6}, Lcom/google/android/finsky/exploreactivity/DocumentNode;->hasParentNode()Z

    move-result v6

    if-eqz v6, :cond_0

    invoke-virtual {p1}, Lcom/google/android/finsky/exploreactivity/DocumentNode;->getParentNode()Lcom/google/android/finsky/exploreactivity/DocumentNode;

    move-result-object v6

    invoke-virtual {v6}, Lcom/google/android/finsky/exploreactivity/DocumentNode;->getParentNode()Lcom/google/android/finsky/exploreactivity/DocumentNode;

    move-result-object v6

    iget-object v7, p0, Lcom/google/android/finsky/exploreactivity/NodeController;->mCenterNode:Lcom/google/android/finsky/exploreactivity/DocumentNode;

    if-ne v6, v7, :cond_0

    invoke-virtual {p1}, Lcom/google/android/finsky/exploreactivity/DocumentNode;->getParentNode()Lcom/google/android/finsky/exploreactivity/DocumentNode;

    move-result-object v3

    iget-object v6, p0, Lcom/google/android/finsky/exploreactivity/NodeController;->mCenterNode:Lcom/google/android/finsky/exploreactivity/DocumentNode;

    invoke-virtual {v6, v3}, Lcom/google/android/finsky/exploreactivity/DocumentNode;->getChildNodePos(Lcom/google/android/finsky/exploreactivity/DocumentNode;)I

    move-result v6

    sget-object v7, Lcom/google/android/finsky/exploreactivity/NodeController;->FIXED_CHILD_ANGLES:[F

    array-length v7, v7

    if-ge v6, v7, :cond_0

    invoke-virtual {v3, p1}, Lcom/google/android/finsky/exploreactivity/DocumentNode;->getChildNodePos(Lcom/google/android/finsky/exploreactivity/DocumentNode;)I

    move-result v6

    sget-object v7, Lcom/google/android/finsky/exploreactivity/NodeController;->FIXED_GRANDCHILD_ANGLES:[F

    array-length v7, v7

    if-ge v6, v7, :cond_0

    const/4 v6, 0x3

    sget-object v7, Lcom/android/volley/Request$Priority;->LOW:Lcom/android/volley/Request$Priority;

    invoke-virtual {p0, p1, v6, v7}, Lcom/google/android/finsky/exploreactivity/NodeController;->loadData(Lcom/google/android/finsky/exploreactivity/DocumentNode;ILcom/android/volley/Request$Priority;)V

    const/4 v4, 0x1

    goto/16 :goto_0

    :cond_7
    invoke-virtual {p1}, Lcom/google/android/finsky/exploreactivity/DocumentNode;->disposeObjects()V

    :cond_8
    invoke-virtual {p1}, Lcom/google/android/finsky/exploreactivity/DocumentNode;->getChildNodes()Ljava/util/List;

    move-result-object v0

    const/4 v1, 0x0

    :goto_2
    invoke-interface {v0}, Ljava/util/List;->size()I

    move-result v6

    if-ge v1, v6, :cond_9

    invoke-interface {v0, v1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v6

    check-cast v6, Lcom/google/android/finsky/exploreactivity/DocumentNode;

    invoke-direct {p0, v6, p2}, Lcom/google/android/finsky/exploreactivity/NodeController;->prepareOnscreenNodes(Lcom/google/android/finsky/exploreactivity/DocumentNode;F)V

    add-int/lit8 v1, v1, 0x1

    goto :goto_2

    :cond_9
    return-void
.end method

.method private processFadeOutNodes()Ljava/util/List;
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/List",
            "<",
            "Lcom/google/android/finsky/exploreactivity/DocumentNode;",
            ">;"
        }
    .end annotation

    const/4 v0, 0x0

    :goto_0
    iget-object v1, p0, Lcom/google/android/finsky/exploreactivity/NodeController;->mFadeOutNodes:Ljava/util/List;

    invoke-interface {v1}, Ljava/util/List;->size()I

    move-result v1

    if-ge v0, v1, :cond_1

    iget-object v1, p0, Lcom/google/android/finsky/exploreactivity/NodeController;->mFadeOutNodes:Ljava/util/List;

    invoke-interface {v1, v0}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/google/android/finsky/exploreactivity/DocumentNode;

    invoke-virtual {v1}, Lcom/google/android/finsky/exploreactivity/DocumentNode;->isVisible()Z

    move-result v1

    if-nez v1, :cond_0

    iget-object v1, p0, Lcom/google/android/finsky/exploreactivity/NodeController;->mFadeOutNodes:Ljava/util/List;

    invoke-interface {v1, v0}, Ljava/util/List;->remove(I)Ljava/lang/Object;

    add-int/lit8 v0, v0, -0x1

    :cond_0
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    :cond_1
    iget-object v1, p0, Lcom/google/android/finsky/exploreactivity/NodeController;->mFadeOutNodes:Ljava/util/List;

    return-object v1
.end method

.method private queueRequest(Ljava/lang/String;Lcom/google/android/finsky/exploreactivity/DocWrapper;Lcom/android/volley/Request$Priority;Ljava/lang/Class;Lcom/android/volley/Response$Listener;)V
    .locals 8
    .param p1    # Ljava/lang/String;
    .param p2    # Lcom/google/android/finsky/exploreactivity/DocWrapper;
    .param p3    # Lcom/android/volley/Request$Priority;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/String;",
            "Lcom/google/android/finsky/exploreactivity/DocWrapper;",
            "Lcom/android/volley/Request$Priority;",
            "Ljava/lang/Class",
            "<*>;",
            "Lcom/android/volley/Response$Listener",
            "<*>;)V"
        }
    .end annotation

    new-instance v0, Lcom/google/android/finsky/exploreactivity/NodeController$9;

    iget-object v1, p0, Lcom/google/android/finsky/exploreactivity/NodeController;->mDfeApi:Lcom/google/android/finsky/api/DfeApi;

    invoke-interface {v1}, Lcom/google/android/finsky/api/DfeApi;->getApiContext()Lcom/google/android/finsky/api/DfeApiContext;

    move-result-object v3

    sget-object v6, Lcom/google/android/finsky/exploreactivity/NodeController;->ERROR_LISTENER:Lcom/android/volley/Response$ErrorListener;

    move-object v1, p0

    move-object v2, p1

    move-object v4, p4

    move-object v5, p5

    move-object v7, p3

    invoke-direct/range {v0 .. v7}, Lcom/google/android/finsky/exploreactivity/NodeController$9;-><init>(Lcom/google/android/finsky/exploreactivity/NodeController;Ljava/lang/String;Lcom/google/android/finsky/api/DfeApiContext;Ljava/lang/Class;Lcom/android/volley/Response$Listener;Lcom/android/volley/Response$ErrorListener;Lcom/android/volley/Request$Priority;)V

    invoke-virtual {v0, p2}, Lcom/android/volley/Request;->setTag(Ljava/lang/Object;)V

    iget-object v1, p0, Lcom/google/android/finsky/exploreactivity/NodeController;->mRequestQueue:Lcom/android/volley/RequestQueue;

    invoke-virtual {v1, v0}, Lcom/android/volley/RequestQueue;->add(Lcom/android/volley/Request;)Lcom/android/volley/Request;

    return-void
.end method

.method private raise(Lcom/android/volley/Request$Priority;)Lcom/android/volley/Request$Priority;
    .locals 3
    .param p1    # Lcom/android/volley/Request$Priority;

    invoke-static {}, Lcom/android/volley/Request$Priority;->values()[Lcom/android/volley/Request$Priority;

    move-result-object v0

    array-length v1, v0

    add-int/lit8 v1, v1, -0x1

    invoke-virtual {p1}, Lcom/android/volley/Request$Priority;->ordinal()I

    move-result v2

    add-int/lit8 v2, v2, 0x1

    invoke-static {v1, v2}, Ljava/lang/Math;->min(II)I

    move-result v1

    aget-object v1, v0, v1

    return-object v1
.end method

.method private updatePlayState(I)V
    .locals 3
    .param p1    # I

    iput p1, p0, Lcom/google/android/finsky/exploreactivity/NodeController;->mPlayState:I

    iget-object v1, p0, Lcom/google/android/finsky/exploreactivity/NodeController;->mSongListener:Lcom/google/android/finsky/exploreactivity/NodeController$SongListener;

    if-eqz v1, :cond_0

    iget-object v1, p0, Lcom/google/android/finsky/exploreactivity/NodeController;->mCenterNode:Lcom/google/android/finsky/exploreactivity/DocumentNode;

    if-eqz v1, :cond_0

    iget-object v1, p0, Lcom/google/android/finsky/exploreactivity/NodeController;->mCenterNode:Lcom/google/android/finsky/exploreactivity/DocumentNode;

    invoke-virtual {v1}, Lcom/google/android/finsky/exploreactivity/DocumentNode;->getDoc()Lcom/google/android/finsky/exploreactivity/DocWrapper;

    move-result-object v1

    invoke-virtual {v1}, Lcom/google/android/finsky/exploreactivity/DocWrapper;->getSong()Lcom/google/android/finsky/api/model/Document;

    move-result-object v1

    if-eqz v1, :cond_1

    iget-object v1, p0, Lcom/google/android/finsky/exploreactivity/NodeController;->mCenterNode:Lcom/google/android/finsky/exploreactivity/DocumentNode;

    invoke-virtual {v1}, Lcom/google/android/finsky/exploreactivity/DocumentNode;->getDoc()Lcom/google/android/finsky/exploreactivity/DocWrapper;

    move-result-object v1

    invoke-virtual {v1}, Lcom/google/android/finsky/exploreactivity/DocWrapper;->getSong()Lcom/google/android/finsky/api/model/Document;

    move-result-object v1

    invoke-virtual {v1}, Lcom/google/android/finsky/api/model/Document;->getTitle()Ljava/lang/String;

    move-result-object v0

    :goto_0
    iget-object v1, p0, Lcom/google/android/finsky/exploreactivity/NodeController;->mSongListener:Lcom/google/android/finsky/exploreactivity/NodeController$SongListener;

    iget v2, p0, Lcom/google/android/finsky/exploreactivity/NodeController;->mPlayState:I

    invoke-interface {v1, v2, v0}, Lcom/google/android/finsky/exploreactivity/NodeController$SongListener;->onPlayStateChanged(ILjava/lang/String;)V

    :cond_0
    return-void

    :cond_1
    const/4 v0, 0x0

    goto :goto_0
.end method


# virtual methods
.method public chooseCenterNode(Lcom/google/android/finsky/exploreactivity/DocumentNode;)V
    .locals 6
    .param p1    # Lcom/google/android/finsky/exploreactivity/DocumentNode;

    move-object v1, p1

    iget-object v3, p0, Lcom/google/android/finsky/exploreactivity/NodeController;->mCenter:Lcom/jme3/math/Vector2f;

    iget v3, v3, Lcom/jme3/math/Vector2f;->y:F

    invoke-virtual {p0, p1}, Lcom/google/android/finsky/exploreactivity/NodeController;->getNodeCenter(Lcom/google/android/finsky/exploreactivity/DocumentNode;)Lcom/jme3/math/Vector2f;

    move-result-object v4

    iget v4, v4, Lcom/jme3/math/Vector2f;->y:F

    cmpl-float v3, v3, v4

    if-lez v3, :cond_0

    const/4 v0, 0x0

    :goto_0
    invoke-virtual {p1}, Lcom/google/android/finsky/exploreactivity/DocumentNode;->getChildNodes()Ljava/util/List;

    move-result-object v3

    invoke-interface {v3}, Ljava/util/List;->size()I

    move-result v3

    if-ge v0, v3, :cond_0

    invoke-virtual {p1}, Lcom/google/android/finsky/exploreactivity/DocumentNode;->getChildNodes()Ljava/util/List;

    move-result-object v3

    invoke-interface {v3, v0}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lcom/google/android/finsky/exploreactivity/DocumentNode;

    invoke-virtual {v2}, Lcom/google/android/finsky/exploreactivity/DocumentNode;->isVisible()Z

    move-result v3

    if-eqz v3, :cond_3

    invoke-virtual {v2}, Lcom/google/android/finsky/exploreactivity/DocumentNode;->getDistanceToCenter()F

    move-result v3

    invoke-virtual {v1}, Lcom/google/android/finsky/exploreactivity/DocumentNode;->getDistanceToCenter()F

    move-result v4

    cmpg-float v3, v3, v4

    if-gez v3, :cond_3

    invoke-virtual {p1}, Lcom/google/android/finsky/exploreactivity/DocumentNode;->getChildNodes()Ljava/util/List;

    move-result-object v3

    invoke-interface {v3, v0}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/google/android/finsky/exploreactivity/DocumentNode;

    :cond_0
    invoke-virtual {p1}, Lcom/google/android/finsky/exploreactivity/DocumentNode;->hasParentNode()Z

    move-result v3

    if-eqz v3, :cond_1

    iget-object v3, p0, Lcom/google/android/finsky/exploreactivity/NodeController;->mCenter:Lcom/jme3/math/Vector2f;

    iget v3, v3, Lcom/jme3/math/Vector2f;->y:F

    invoke-virtual {p1}, Lcom/google/android/finsky/exploreactivity/DocumentNode;->getParentNode()Lcom/google/android/finsky/exploreactivity/DocumentNode;

    move-result-object v4

    invoke-virtual {p0, v4}, Lcom/google/android/finsky/exploreactivity/NodeController;->getNodeCenter(Lcom/google/android/finsky/exploreactivity/DocumentNode;)Lcom/jme3/math/Vector2f;

    move-result-object v4

    iget v4, v4, Lcom/jme3/math/Vector2f;->y:F

    sub-float/2addr v3, v4

    invoke-static {v3}, Ljava/lang/Math;->abs(F)F

    move-result v3

    iget-object v4, p0, Lcom/google/android/finsky/exploreactivity/NodeController;->mCenter:Lcom/jme3/math/Vector2f;

    iget v4, v4, Lcom/jme3/math/Vector2f;->y:F

    invoke-virtual {p0, v1}, Lcom/google/android/finsky/exploreactivity/NodeController;->getNodeCenter(Lcom/google/android/finsky/exploreactivity/DocumentNode;)Lcom/jme3/math/Vector2f;

    move-result-object v5

    iget v5, v5, Lcom/jme3/math/Vector2f;->y:F

    sub-float/2addr v4, v5

    invoke-static {v4}, Ljava/lang/Math;->abs(F)F

    move-result v4

    cmpg-float v3, v3, v4

    if-gez v3, :cond_1

    invoke-virtual {p1}, Lcom/google/android/finsky/exploreactivity/DocumentNode;->getParentNode()Lcom/google/android/finsky/exploreactivity/DocumentNode;

    move-result-object v1

    :cond_1
    iget-object v3, p0, Lcom/google/android/finsky/exploreactivity/NodeController;->mCenterNode:Lcom/google/android/finsky/exploreactivity/DocumentNode;

    if-eq v3, v1, :cond_2

    invoke-virtual {p0, v1}, Lcom/google/android/finsky/exploreactivity/NodeController;->setCenterNode(Lcom/google/android/finsky/exploreactivity/DocumentNode;)V

    :cond_2
    return-void

    :cond_3
    add-int/lit8 v0, v0, 0x1

    goto :goto_0
.end method

.method public createDocumentNode(Lcom/google/android/finsky/exploreactivity/DocumentNode;Lcom/google/android/finsky/exploreactivity/DocWrapper;Lcom/google/android/finsky/exploreactivity/DrawingUtils;)Lcom/google/android/finsky/exploreactivity/DocumentNode;
    .locals 6
    .param p1    # Lcom/google/android/finsky/exploreactivity/DocumentNode;
    .param p2    # Lcom/google/android/finsky/exploreactivity/DocWrapper;
    .param p3    # Lcom/google/android/finsky/exploreactivity/DrawingUtils;

    invoke-virtual {p2}, Lcom/google/android/finsky/exploreactivity/DocWrapper;->getDoc()Lcom/google/android/finsky/api/model/Document;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/android/finsky/api/model/Document;->getBackend()I

    move-result v0

    packed-switch v0, :pswitch_data_0

    new-instance v0, Ljava/lang/IllegalArgumentException;

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "Unknown backend type "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {p2}, Lcom/google/android/finsky/exploreactivity/DocWrapper;->getDoc()Lcom/google/android/finsky/api/model/Document;

    move-result-object v2

    invoke-virtual {v2}, Lcom/google/android/finsky/api/model/Document;->getBackend()I

    move-result v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v0

    :pswitch_0
    new-instance v0, Lcom/google/android/finsky/exploreactivity/AppNode;

    iget-object v5, p0, Lcom/google/android/finsky/exploreactivity/NodeController;->mUiComponents:Landroid/view/ViewGroup;

    move-object v1, p1

    move-object v2, p2

    move-object v3, p0

    move-object v4, p3

    invoke-direct/range {v0 .. v5}, Lcom/google/android/finsky/exploreactivity/AppNode;-><init>(Lcom/google/android/finsky/exploreactivity/DocumentNode;Lcom/google/android/finsky/exploreactivity/DocWrapper;Lcom/google/android/finsky/exploreactivity/NodeController;Lcom/google/android/finsky/exploreactivity/DrawingUtils;Landroid/view/ViewGroup;)V

    :goto_0
    return-object v0

    :pswitch_1
    invoke-virtual {p2}, Lcom/google/android/finsky/exploreactivity/DocWrapper;->getDoc()Lcom/google/android/finsky/api/model/Document;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/android/finsky/api/model/Document;->getDocumentType()I

    move-result v0

    packed-switch v0, :pswitch_data_1

    new-instance v0, Lcom/google/android/finsky/exploreactivity/MusicArtistNode;

    iget-object v5, p0, Lcom/google/android/finsky/exploreactivity/NodeController;->mUiComponents:Landroid/view/ViewGroup;

    move-object v1, p1

    move-object v2, p2

    move-object v3, p0

    move-object v4, p3

    invoke-direct/range {v0 .. v5}, Lcom/google/android/finsky/exploreactivity/MusicArtistNode;-><init>(Lcom/google/android/finsky/exploreactivity/DocumentNode;Lcom/google/android/finsky/exploreactivity/DocWrapper;Lcom/google/android/finsky/exploreactivity/NodeController;Lcom/google/android/finsky/exploreactivity/DrawingUtils;Landroid/view/ViewGroup;)V

    goto :goto_0

    :pswitch_2
    new-instance v0, Lcom/google/android/finsky/exploreactivity/MusicArtistNode;

    iget-object v5, p0, Lcom/google/android/finsky/exploreactivity/NodeController;->mUiComponents:Landroid/view/ViewGroup;

    move-object v1, p1

    move-object v2, p2

    move-object v3, p0

    move-object v4, p3

    invoke-direct/range {v0 .. v5}, Lcom/google/android/finsky/exploreactivity/MusicArtistNode;-><init>(Lcom/google/android/finsky/exploreactivity/DocumentNode;Lcom/google/android/finsky/exploreactivity/DocWrapper;Lcom/google/android/finsky/exploreactivity/NodeController;Lcom/google/android/finsky/exploreactivity/DrawingUtils;Landroid/view/ViewGroup;)V

    goto :goto_0

    :pswitch_3
    new-instance v0, Lcom/google/android/finsky/exploreactivity/MusicAlbumNode;

    iget-object v5, p0, Lcom/google/android/finsky/exploreactivity/NodeController;->mUiComponents:Landroid/view/ViewGroup;

    move-object v1, p1

    move-object v2, p2

    move-object v3, p0

    move-object v4, p3

    invoke-direct/range {v0 .. v5}, Lcom/google/android/finsky/exploreactivity/MusicAlbumNode;-><init>(Lcom/google/android/finsky/exploreactivity/DocumentNode;Lcom/google/android/finsky/exploreactivity/DocWrapper;Lcom/google/android/finsky/exploreactivity/NodeController;Lcom/google/android/finsky/exploreactivity/DrawingUtils;Landroid/view/ViewGroup;)V

    goto :goto_0

    :pswitch_4
    new-instance v0, Lcom/google/android/finsky/exploreactivity/BookNode;

    iget-object v5, p0, Lcom/google/android/finsky/exploreactivity/NodeController;->mUiComponents:Landroid/view/ViewGroup;

    move-object v1, p1

    move-object v2, p2

    move-object v3, p0

    move-object v4, p3

    invoke-direct/range {v0 .. v5}, Lcom/google/android/finsky/exploreactivity/BookNode;-><init>(Lcom/google/android/finsky/exploreactivity/DocumentNode;Lcom/google/android/finsky/exploreactivity/DocWrapper;Lcom/google/android/finsky/exploreactivity/NodeController;Lcom/google/android/finsky/exploreactivity/DrawingUtils;Landroid/view/ViewGroup;)V

    goto :goto_0

    :pswitch_5
    new-instance v0, Lcom/google/android/finsky/exploreactivity/VideoNode;

    iget-object v5, p0, Lcom/google/android/finsky/exploreactivity/NodeController;->mUiComponents:Landroid/view/ViewGroup;

    move-object v1, p1

    move-object v2, p2

    move-object v3, p0

    move-object v4, p3

    invoke-direct/range {v0 .. v5}, Lcom/google/android/finsky/exploreactivity/VideoNode;-><init>(Lcom/google/android/finsky/exploreactivity/DocumentNode;Lcom/google/android/finsky/exploreactivity/DocWrapper;Lcom/google/android/finsky/exploreactivity/NodeController;Lcom/google/android/finsky/exploreactivity/DrawingUtils;Landroid/view/ViewGroup;)V

    goto :goto_0

    nop

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_4
        :pswitch_1
        :pswitch_0
        :pswitch_5
    .end packed-switch

    :pswitch_data_1
    .packed-switch 0x2
        :pswitch_3
        :pswitch_2
    .end packed-switch
.end method

.method public declared-synchronized createRoot(Lcom/google/android/finsky/exploreactivity/DocWrapper;Lcom/google/android/finsky/exploreactivity/DrawingUtils;)V
    .locals 3
    .param p1    # Lcom/google/android/finsky/exploreactivity/DocWrapper;
    .param p2    # Lcom/google/android/finsky/exploreactivity/DrawingUtils;

    monitor-enter p0

    :try_start_0
    new-instance v0, Lcom/google/android/finsky/exploreactivity/NodeController$3;

    invoke-direct {v0, p0}, Lcom/google/android/finsky/exploreactivity/NodeController$3;-><init>(Lcom/google/android/finsky/exploreactivity/NodeController;)V

    invoke-virtual {p0, v0}, Lcom/google/android/finsky/exploreactivity/NodeController;->runOnGlThread(Ljava/lang/Runnable;)V

    const/4 v0, 0x0

    invoke-virtual {p0, v0, p1, p2}, Lcom/google/android/finsky/exploreactivity/NodeController;->createDocumentNode(Lcom/google/android/finsky/exploreactivity/DocumentNode;Lcom/google/android/finsky/exploreactivity/DocWrapper;Lcom/google/android/finsky/exploreactivity/DrawingUtils;)Lcom/google/android/finsky/exploreactivity/DocumentNode;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/finsky/exploreactivity/NodeController;->mCenterNode:Lcom/google/android/finsky/exploreactivity/DocumentNode;

    iget-object v0, p0, Lcom/google/android/finsky/exploreactivity/NodeController;->mCenterNode:Lcom/google/android/finsky/exploreactivity/DocumentNode;

    iput-object v0, p0, Lcom/google/android/finsky/exploreactivity/NodeController;->mSeedNode:Lcom/google/android/finsky/exploreactivity/DocumentNode;

    iput-object p2, p0, Lcom/google/android/finsky/exploreactivity/NodeController;->mDrawingUtils:Lcom/google/android/finsky/exploreactivity/DrawingUtils;

    invoke-virtual {p0}, Lcom/google/android/finsky/exploreactivity/NodeController;->shouldShowPlaybackControls()Z

    move-result v0

    if-eqz v0, :cond_0

    const/4 v0, 0x3

    iput v0, p0, Lcom/google/android/finsky/exploreactivity/NodeController;->mPlayState:I

    :cond_0
    iget-object v0, p0, Lcom/google/android/finsky/exploreactivity/NodeController;->mCenterNode:Lcom/google/android/finsky/exploreactivity/DocumentNode;

    iget-object v0, v0, Lcom/google/android/finsky/exploreactivity/DocumentNode;->mDocWrapper:Lcom/google/android/finsky/exploreactivity/DocWrapper;

    const/16 v1, 0xf

    sget-object v2, Lcom/android/volley/Request$Priority;->IMMEDIATE:Lcom/android/volley/Request$Priority;

    invoke-virtual {p0, v0, v1, v2}, Lcom/google/android/finsky/exploreactivity/NodeController;->loadData(Lcom/google/android/finsky/exploreactivity/DocWrapper;ILcom/android/volley/Request$Priority;)V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    monitor-exit p0

    return-void

    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method public disposeObjects()V
    .locals 1

    new-instance v0, Lcom/google/android/finsky/exploreactivity/NodeController$5;

    invoke-direct {v0, p0}, Lcom/google/android/finsky/exploreactivity/NodeController$5;-><init>(Lcom/google/android/finsky/exploreactivity/NodeController;)V

    invoke-virtual {p0, v0}, Lcom/google/android/finsky/exploreactivity/NodeController;->runOnUiThread(Ljava/lang/Runnable;)V

    return-void
.end method

.method public dumpControllerState()V
    .locals 5

    const/4 v4, 0x1

    const/4 v3, 0x0

    const-string v0, "NodeController state:"

    new-array v1, v3, [Ljava/lang/Object;

    invoke-static {v0, v1}, Lcom/google/android/finsky/utils/FinskyLog;->d(Ljava/lang/String;[Ljava/lang/Object;)V

    const-string v0, "Center node: %s"

    new-array v1, v4, [Ljava/lang/Object;

    iget-object v2, p0, Lcom/google/android/finsky/exploreactivity/NodeController;->mCenterNode:Lcom/google/android/finsky/exploreactivity/DocumentNode;

    invoke-virtual {v2}, Lcom/google/android/finsky/exploreactivity/DocumentNode;->toString()Ljava/lang/String;

    move-result-object v2

    aput-object v2, v1, v3

    invoke-static {v0, v1}, Lcom/google/android/finsky/utils/FinskyLog;->d(Ljava/lang/String;[Ljava/lang/Object;)V

    const-string v0, "Center %s"

    new-array v1, v4, [Ljava/lang/Object;

    iget-object v2, p0, Lcom/google/android/finsky/exploreactivity/NodeController;->mCenter:Lcom/jme3/math/Vector2f;

    invoke-virtual {v2}, Lcom/jme3/math/Vector2f;->toString()Ljava/lang/String;

    move-result-object v2

    aput-object v2, v1, v3

    invoke-static {v0, v1}, Lcom/google/android/finsky/utils/FinskyLog;->d(Ljava/lang/String;[Ljava/lang/Object;)V

    const-string v0, "Node tree:"

    new-array v1, v3, [Ljava/lang/Object;

    invoke-static {v0, v1}, Lcom/google/android/finsky/utils/FinskyLog;->d(Ljava/lang/String;[Ljava/lang/Object;)V

    iget-object v0, p0, Lcom/google/android/finsky/exploreactivity/NodeController;->mSeedNode:Lcom/google/android/finsky/exploreactivity/DocumentNode;

    invoke-direct {p0, v0, v3}, Lcom/google/android/finsky/exploreactivity/NodeController;->dumpNodeLeaf(Lcom/google/android/finsky/exploreactivity/DocumentNode;I)V

    return-void
.end method

.method public fileReady(Lcom/google/android/finsky/exploreactivity/DocWrapper;Ljava/io/File;)V
    .locals 7

    const/4 v6, 0x1

    const/4 v5, 0x0

    invoke-virtual {p1}, Lcom/google/android/finsky/exploreactivity/DocWrapper;->getSong()Lcom/google/android/finsky/api/model/Document;

    move-result-object v0

    iget-object v1, p0, Lcom/google/android/finsky/exploreactivity/NodeController;->mCenterNode:Lcom/google/android/finsky/exploreactivity/DocumentNode;

    iget-object v1, v1, Lcom/google/android/finsky/exploreactivity/DocumentNode;->mDocWrapper:Lcom/google/android/finsky/exploreactivity/DocWrapper;

    if-ne v1, p1, :cond_2

    invoke-virtual {v0}, Lcom/google/android/finsky/api/model/Document;->getDocId()Ljava/lang/String;

    move-result-object v1

    iget-object v2, p0, Lcom/google/android/finsky/exploreactivity/NodeController;->mSongPlayingNow:Ljava/lang/String;

    invoke-virtual {v1, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-nez v1, :cond_2

    :try_start_0
    iget v1, p0, Lcom/google/android/finsky/exploreactivity/NodeController;->mPlayState:I

    const/4 v2, 0x3

    if-eq v1, v2, :cond_0

    iget v1, p0, Lcom/google/android/finsky/exploreactivity/NodeController;->mPlayState:I

    const/4 v2, 0x2

    if-ne v1, v2, :cond_1

    :cond_0
    const-string v1, "Starting playback of song %s"

    const/4 v2, 0x1

    new-array v2, v2, [Ljava/lang/Object;

    const/4 v3, 0x0

    invoke-virtual {v0}, Lcom/google/android/finsky/api/model/Document;->getTitle()Ljava/lang/String;

    move-result-object v4

    aput-object v4, v2, v3

    invoke-static {v1, v2}, Lcom/google/android/finsky/utils/FinskyLog;->d(Ljava/lang/String;[Ljava/lang/Object;)V

    iget-object v1, p0, Lcom/google/android/finsky/exploreactivity/NodeController;->mPlayer:Lcom/google/android/finsky/previews/MediaPlayerWrapper;

    invoke-virtual {v1}, Lcom/google/android/finsky/previews/MediaPlayerWrapper;->reset()V

    new-instance v1, Ljava/io/FileInputStream;

    invoke-direct {v1, p2}, Ljava/io/FileInputStream;-><init>(Ljava/io/File;)V

    iget-object v2, p0, Lcom/google/android/finsky/exploreactivity/NodeController;->mPlayer:Lcom/google/android/finsky/previews/MediaPlayerWrapper;

    invoke-virtual {v1}, Ljava/io/FileInputStream;->getFD()Ljava/io/FileDescriptor;

    move-result-object v3

    invoke-virtual {v2, v3}, Lcom/google/android/finsky/previews/MediaPlayerWrapper;->setDataSource(Ljava/io/FileDescriptor;)V

    invoke-virtual {v1}, Ljava/io/FileInputStream;->close()V

    iget-object v1, p0, Lcom/google/android/finsky/exploreactivity/NodeController;->mPlayer:Lcom/google/android/finsky/previews/MediaPlayerWrapper;

    invoke-virtual {v1}, Lcom/google/android/finsky/previews/MediaPlayerWrapper;->prepareAsync()V

    invoke-virtual {v0}, Lcom/google/android/finsky/api/model/Document;->getDocId()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/finsky/exploreactivity/NodeController;->mSongPlayingNow:Ljava/lang/String;

    :cond_1
    iget v0, p0, Lcom/google/android/finsky/exploreactivity/NodeController;->mPlayState:I

    invoke-direct {p0, v0}, Lcom/google/android/finsky/exploreactivity/NodeController;->updatePlayState(I)V
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    :cond_2
    :goto_0
    return-void

    :catch_0
    move-exception v0

    const-string v1, "Exception trying to get sample: %s"

    new-array v2, v6, [Ljava/lang/Object;

    invoke-virtual {v0}, Ljava/lang/Exception;->toString()Ljava/lang/String;

    move-result-object v0

    aput-object v0, v2, v5

    invoke-static {v1, v2}, Lcom/google/android/finsky/utils/FinskyLog;->e(Ljava/lang/String;[Ljava/lang/Object;)V

    goto :goto_0
.end method

.method getCenterNode()Lcom/google/android/finsky/exploreactivity/DocumentNode;
    .locals 1

    iget-object v0, p0, Lcom/google/android/finsky/exploreactivity/NodeController;->mCenterNode:Lcom/google/android/finsky/exploreactivity/DocumentNode;

    return-object v0
.end method

.method public getNodeCenter(Lcom/google/android/finsky/exploreactivity/DocumentNode;)Lcom/jme3/math/Vector2f;
    .locals 5
    .param p1    # Lcom/google/android/finsky/exploreactivity/DocumentNode;

    invoke-virtual {p1}, Lcom/google/android/finsky/exploreactivity/DocumentNode;->getCenter()Lcom/jme3/math/Vector2f;

    move-result-object v0

    iget v3, v0, Lcom/jme3/math/Vector2f;->x:F

    invoke-static {v3}, Ljava/lang/Float;->isNaN(F)Z

    move-result v3

    if-nez v3, :cond_1

    iget v3, v0, Lcom/jme3/math/Vector2f;->y:F

    invoke-static {v3}, Ljava/lang/Float;->isNaN(F)Z

    move-result v3

    if-nez v3, :cond_1

    :cond_0
    :goto_0
    return-object v0

    :cond_1
    iget-object v3, p0, Lcom/google/android/finsky/exploreactivity/NodeController;->mSeedNode:Lcom/google/android/finsky/exploreactivity/DocumentNode;

    invoke-virtual {v3}, Lcom/google/android/finsky/exploreactivity/DocumentNode;->getCenter()Lcom/jme3/math/Vector2f;

    move-result-object v3

    iget v3, v3, Lcom/jme3/math/Vector2f;->x:F

    invoke-static {v3}, Ljava/lang/Float;->isNaN(F)Z

    move-result v3

    if-nez v3, :cond_2

    iget-object v3, p0, Lcom/google/android/finsky/exploreactivity/NodeController;->mSeedNode:Lcom/google/android/finsky/exploreactivity/DocumentNode;

    invoke-virtual {v3}, Lcom/google/android/finsky/exploreactivity/DocumentNode;->getCenter()Lcom/jme3/math/Vector2f;

    move-result-object v3

    iget v3, v3, Lcom/jme3/math/Vector2f;->y:F

    invoke-static {v3}, Ljava/lang/Float;->isNaN(F)Z

    move-result v3

    if-eqz v3, :cond_3

    :cond_2
    iget-object v3, p0, Lcom/google/android/finsky/exploreactivity/NodeController;->mCenterNode:Lcom/google/android/finsky/exploreactivity/DocumentNode;

    invoke-virtual {v3}, Lcom/google/android/finsky/exploreactivity/DocumentNode;->getCenter()Lcom/jme3/math/Vector2f;

    move-result-object v3

    iget-object v4, p0, Lcom/google/android/finsky/exploreactivity/NodeController;->mCenterNodeCenter:Lcom/jme3/math/Vector2f;

    invoke-virtual {v3, v4}, Lcom/jme3/math/Vector2f;->set(Lcom/jme3/math/Vector2f;)Lcom/jme3/math/Vector2f;

    iget-object v1, p0, Lcom/google/android/finsky/exploreactivity/NodeController;->mCenterNode:Lcom/google/android/finsky/exploreactivity/DocumentNode;

    :goto_1
    invoke-virtual {v1}, Lcom/google/android/finsky/exploreactivity/DocumentNode;->getParentNode()Lcom/google/android/finsky/exploreactivity/DocumentNode;

    move-result-object v2

    if-eqz v2, :cond_3

    invoke-virtual {v2}, Lcom/google/android/finsky/exploreactivity/DocumentNode;->getCenter()Lcom/jme3/math/Vector2f;

    move-result-object v3

    invoke-virtual {v1}, Lcom/google/android/finsky/exploreactivity/DocumentNode;->getCenter()Lcom/jme3/math/Vector2f;

    move-result-object v4

    invoke-virtual {v3, v4}, Lcom/jme3/math/Vector2f;->set(Lcom/jme3/math/Vector2f;)Lcom/jme3/math/Vector2f;

    move-result-object v3

    invoke-virtual {v1}, Lcom/google/android/finsky/exploreactivity/DocumentNode;->getRelativePosition()Lcom/jme3/math/Vector2f;

    move-result-object v4

    invoke-virtual {v3, v4}, Lcom/jme3/math/Vector2f;->subtractLocal(Lcom/jme3/math/Vector2f;)Lcom/jme3/math/Vector2f;

    move-object v1, v2

    goto :goto_1

    :cond_3
    invoke-virtual {p1}, Lcom/google/android/finsky/exploreactivity/DocumentNode;->getParentNode()Lcom/google/android/finsky/exploreactivity/DocumentNode;

    move-result-object v3

    if-eqz v3, :cond_0

    iget-object v3, p0, Lcom/google/android/finsky/exploreactivity/NodeController;->mNodeVector:Lcom/jme3/math/Vector2f;

    invoke-virtual {p1}, Lcom/google/android/finsky/exploreactivity/DocumentNode;->getRelativePosition()Lcom/jme3/math/Vector2f;

    move-result-object v4

    invoke-virtual {v3, v4}, Lcom/jme3/math/Vector2f;->set(Lcom/jme3/math/Vector2f;)Lcom/jme3/math/Vector2f;

    invoke-virtual {p1}, Lcom/google/android/finsky/exploreactivity/DocumentNode;->getParentNode()Lcom/google/android/finsky/exploreactivity/DocumentNode;

    move-result-object v3

    invoke-virtual {p0, v3}, Lcom/google/android/finsky/exploreactivity/NodeController;->getNodeCenter(Lcom/google/android/finsky/exploreactivity/DocumentNode;)Lcom/jme3/math/Vector2f;

    move-result-object v3

    invoke-virtual {v0, v3}, Lcom/jme3/math/Vector2f;->set(Lcom/jme3/math/Vector2f;)Lcom/jme3/math/Vector2f;

    move-result-object v3

    iget-object v4, p0, Lcom/google/android/finsky/exploreactivity/NodeController;->mNodeVector:Lcom/jme3/math/Vector2f;

    invoke-virtual {v3, v4}, Lcom/jme3/math/Vector2f;->addLocal(Lcom/jme3/math/Vector2f;)Lcom/jme3/math/Vector2f;

    goto :goto_0
.end method

.method public getRotation()F
    .locals 1

    iget v0, p0, Lcom/google/android/finsky/exploreactivity/NodeController;->mRotation:F

    return v0
.end method

.method public isSwipable()Z
    .locals 2

    iget-object v0, p0, Lcom/google/android/finsky/exploreactivity/NodeController;->mCenterNode:Lcom/google/android/finsky/exploreactivity/DocumentNode;

    invoke-virtual {v0}, Lcom/google/android/finsky/exploreactivity/DocumentNode;->getChildNodes()Ljava/util/List;

    move-result-object v0

    invoke-interface {v0}, Ljava/util/List;->size()I

    move-result v0

    sget-object v1, Lcom/google/android/finsky/exploreactivity/NodeController;->FIXED_CHILD_ANGLES:[F

    array-length v1, v1

    if-le v0, v1, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public isWishlistEnabled()Z
    .locals 1

    iget-boolean v0, p0, Lcom/google/android/finsky/exploreactivity/NodeController;->mWishlistEnabled:Z

    return v0
.end method

.method public loadData(Lcom/google/android/finsky/exploreactivity/DocWrapper;ILcom/android/volley/Request$Priority;)V
    .locals 7

    invoke-virtual {p1}, Lcom/google/android/finsky/exploreactivity/DocWrapper;->getLoadedState()I

    move-result v1

    xor-int/lit8 v0, v1, -0x1

    and-int/2addr v0, p2

    invoke-virtual {p1}, Lcom/google/android/finsky/exploreactivity/DocWrapper;->getInProgressState()I

    move-result v2

    xor-int/lit8 v2, v2, -0x1

    and-int/2addr v0, v2

    and-int/lit8 v1, v1, 0x2

    if-nez v1, :cond_6

    and-int/lit8 v0, v0, -0xd

    move v6, v0

    :goto_0
    invoke-virtual {p1, v6}, Lcom/google/android/finsky/exploreactivity/DocWrapper;->markInProgress(I)V

    and-int/lit8 v0, v6, 0x1

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/google/android/finsky/exploreactivity/NodeController;->mDrawingUtils:Lcom/google/android/finsky/exploreactivity/DrawingUtils;

    sget-object v1, Lcom/google/android/finsky/exploreactivity/NodeController;->ERROR_LISTENER:Lcom/android/volley/Response$ErrorListener;

    invoke-virtual {v0, p1, p3, v1}, Lcom/google/android/finsky/exploreactivity/DrawingUtils;->getThumbnailRequest(Lcom/google/android/finsky/exploreactivity/DocWrapper;Lcom/android/volley/Request$Priority;Lcom/android/volley/Response$ErrorListener;)Lcom/android/volley/Request;

    move-result-object v0

    invoke-virtual {v0, p1}, Lcom/android/volley/Request;->setTag(Ljava/lang/Object;)V

    iget-object v1, p0, Lcom/google/android/finsky/exploreactivity/NodeController;->mRequestQueue:Lcom/android/volley/RequestQueue;

    invoke-virtual {v1, v0}, Lcom/android/volley/RequestQueue;->add(Lcom/android/volley/Request;)Lcom/android/volley/Request;

    :cond_0
    and-int/lit8 v0, v6, 0x2

    if-eqz v0, :cond_1

    invoke-virtual {p1}, Lcom/google/android/finsky/exploreactivity/DocWrapper;->getDetailsUrl()Ljava/lang/String;

    move-result-object v1

    const-class v4, Lcom/google/android/finsky/protos/Details$DetailsResponse;

    new-instance v5, Lcom/google/android/finsky/exploreactivity/NodeController$6;

    invoke-direct {v5, p0, p1, p2, p3}, Lcom/google/android/finsky/exploreactivity/NodeController$6;-><init>(Lcom/google/android/finsky/exploreactivity/NodeController;Lcom/google/android/finsky/exploreactivity/DocWrapper;ILcom/android/volley/Request$Priority;)V

    move-object v0, p0

    move-object v2, p1

    move-object v3, p3

    invoke-direct/range {v0 .. v5}, Lcom/google/android/finsky/exploreactivity/NodeController;->queueRequest(Ljava/lang/String;Lcom/google/android/finsky/exploreactivity/DocWrapper;Lcom/android/volley/Request$Priority;Ljava/lang/Class;Lcom/android/volley/Response$Listener;)V

    :cond_1
    and-int/lit8 v0, v6, 0x4

    if-eqz v0, :cond_4

    invoke-virtual {p1}, Lcom/google/android/finsky/exploreactivity/DocWrapper;->getDoc()Lcom/google/android/finsky/api/model/Document;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/android/finsky/api/model/Document;->getBackend()I

    move-result v0

    const/4 v1, 0x2

    if-eq v0, v1, :cond_3

    :cond_2
    :goto_1
    return-void

    :cond_3
    invoke-virtual {p1}, Lcom/google/android/finsky/exploreactivity/DocWrapper;->getDoc()Lcom/google/android/finsky/api/model/Document;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/android/finsky/api/model/Document;->getCoreContentListUrl()Ljava/lang/String;

    move-result-object v1

    invoke-static {v1}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v0

    if-nez v0, :cond_5

    :goto_2
    invoke-static {v1}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v0

    if-nez v0, :cond_4

    const-class v4, Lcom/google/android/finsky/protos/DocList$ListResponse;

    new-instance v5, Lcom/google/android/finsky/exploreactivity/NodeController$7;

    invoke-direct {v5, p0, p1}, Lcom/google/android/finsky/exploreactivity/NodeController$7;-><init>(Lcom/google/android/finsky/exploreactivity/NodeController;Lcom/google/android/finsky/exploreactivity/DocWrapper;)V

    move-object v0, p0

    move-object v2, p1

    move-object v3, p3

    invoke-direct/range {v0 .. v5}, Lcom/google/android/finsky/exploreactivity/NodeController;->queueRequest(Ljava/lang/String;Lcom/google/android/finsky/exploreactivity/DocWrapper;Lcom/android/volley/Request$Priority;Ljava/lang/Class;Lcom/android/volley/Response$Listener;)V

    :cond_4
    and-int/lit8 v0, v6, 0x8

    if-eqz v0, :cond_2

    invoke-virtual {p1}, Lcom/google/android/finsky/exploreactivity/DocWrapper;->getRelatedItemUrl()Ljava/lang/String;

    move-result-object v1

    invoke-direct {p0, p3}, Lcom/google/android/finsky/exploreactivity/NodeController;->raise(Lcom/android/volley/Request$Priority;)Lcom/android/volley/Request$Priority;

    move-result-object v3

    const-class v4, Lcom/google/android/finsky/protos/DocList$ListResponse;

    new-instance v5, Lcom/google/android/finsky/exploreactivity/NodeController$8;

    invoke-direct {v5, p0, p1}, Lcom/google/android/finsky/exploreactivity/NodeController$8;-><init>(Lcom/google/android/finsky/exploreactivity/NodeController;Lcom/google/android/finsky/exploreactivity/DocWrapper;)V

    move-object v0, p0

    move-object v2, p1

    invoke-direct/range {v0 .. v5}, Lcom/google/android/finsky/exploreactivity/NodeController;->queueRequest(Ljava/lang/String;Lcom/google/android/finsky/exploreactivity/DocWrapper;Lcom/android/volley/Request$Priority;Ljava/lang/Class;Lcom/android/volley/Response$Listener;)V

    goto :goto_1

    :cond_5
    invoke-virtual {p1}, Lcom/google/android/finsky/exploreactivity/DocWrapper;->getDoc()Lcom/google/android/finsky/api/model/Document;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/android/finsky/api/model/Document;->getRelatedDocTypeListUrl()Ljava/lang/String;

    move-result-object v1

    goto :goto_2

    :cond_6
    move v6, v0

    goto :goto_0
.end method

.method public loadData(Lcom/google/android/finsky/exploreactivity/DocumentNode;ILcom/android/volley/Request$Priority;)V
    .locals 1
    .param p1    # Lcom/google/android/finsky/exploreactivity/DocumentNode;
    .param p2    # I
    .param p3    # Lcom/android/volley/Request$Priority;

    invoke-virtual {p1}, Lcom/google/android/finsky/exploreactivity/DocumentNode;->hasThumbnail()Z

    move-result v0

    if-eqz v0, :cond_1

    and-int/lit8 p2, p2, -0x2

    iget-object v0, p1, Lcom/google/android/finsky/exploreactivity/DocumentNode;->mDocWrapper:Lcom/google/android/finsky/exploreactivity/DocWrapper;

    invoke-virtual {v0}, Lcom/google/android/finsky/exploreactivity/DocWrapper;->getLoadedState()I

    move-result v0

    and-int/lit8 v0, v0, 0x1

    if-lez v0, :cond_0

    invoke-virtual {p1}, Lcom/google/android/finsky/exploreactivity/DocumentNode;->recycleThumbnail()V

    :cond_0
    :goto_0
    iget-object v0, p1, Lcom/google/android/finsky/exploreactivity/DocumentNode;->mDocWrapper:Lcom/google/android/finsky/exploreactivity/DocWrapper;

    invoke-virtual {p0, v0, p2, p3}, Lcom/google/android/finsky/exploreactivity/NodeController;->loadData(Lcom/google/android/finsky/exploreactivity/DocWrapper;ILcom/android/volley/Request$Priority;)V

    return-void

    :cond_1
    iget-object v0, p1, Lcom/google/android/finsky/exploreactivity/DocumentNode;->mDocWrapper:Lcom/google/android/finsky/exploreactivity/DocWrapper;

    invoke-virtual {v0}, Lcom/google/android/finsky/exploreactivity/DocWrapper;->getLoadedState()I

    move-result v0

    and-int/lit8 v0, v0, 0x1

    if-lez v0, :cond_0

    invoke-virtual {p1}, Lcom/google/android/finsky/exploreactivity/DocumentNode;->hasThumbnail()Z

    move-result v0

    if-nez v0, :cond_0

    and-int/lit8 p2, p2, -0x2

    invoke-virtual {p1}, Lcom/google/android/finsky/exploreactivity/DocumentNode;->setNodeThumbnail()Z

    goto :goto_0
.end method

.method public processOnscreenNodes([Lcom/jme3/math/Vector2f;F)Ljava/util/List;
    .locals 9
    .param p1    # [Lcom/jme3/math/Vector2f;
    .param p2    # F
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "([",
            "Lcom/jme3/math/Vector2f;",
            "F)",
            "Ljava/util/List",
            "<",
            "Lcom/google/android/finsky/exploreactivity/DocumentNode;",
            ">;"
        }
    .end annotation

    iget-object v5, p0, Lcom/google/android/finsky/exploreactivity/NodeController;->mSeedNode:Lcom/google/android/finsky/exploreactivity/DocumentNode;

    invoke-direct {p0, v5}, Lcom/google/android/finsky/exploreactivity/NodeController;->clearNodePositions(Lcom/google/android/finsky/exploreactivity/DocumentNode;)V

    iget-object v4, p0, Lcom/google/android/finsky/exploreactivity/NodeController;->mCurrentNodes:Ljava/util/List;

    iget-object v5, p0, Lcom/google/android/finsky/exploreactivity/NodeController;->mPreviousNodes:Ljava/util/List;

    iput-object v5, p0, Lcom/google/android/finsky/exploreactivity/NodeController;->mCurrentNodes:Ljava/util/List;

    iput-object v4, p0, Lcom/google/android/finsky/exploreactivity/NodeController;->mPreviousNodes:Ljava/util/List;

    iget-object v5, p0, Lcom/google/android/finsky/exploreactivity/NodeController;->mCurrentNodes:Ljava/util/List;

    invoke-interface {v5}, Ljava/util/List;->clear()V

    iget-object v5, p0, Lcom/google/android/finsky/exploreactivity/NodeController;->mCenter:Lcom/jme3/math/Vector2f;

    invoke-static {p1, v5}, Lcom/google/android/finsky/exploreactivity/DrawingUtils;->getPolyCenter([Lcom/jme3/math/Vector2f;Lcom/jme3/math/Vector2f;)V

    iget-object v5, p0, Lcom/google/android/finsky/exploreactivity/NodeController;->mSize:Lcom/jme3/math/Vector2f;

    invoke-static {p1, v5}, Lcom/google/android/finsky/exploreactivity/DrawingUtils;->getPolySize([Lcom/jme3/math/Vector2f;Lcom/jme3/math/Vector2f;)V

    iget-object v5, p0, Lcom/google/android/finsky/exploreactivity/NodeController;->mCenter:Lcom/jme3/math/Vector2f;

    iget v6, v5, Lcom/jme3/math/Vector2f;->y:F

    iget-object v7, p0, Lcom/google/android/finsky/exploreactivity/NodeController;->mSize:Lcom/jme3/math/Vector2f;

    iget v7, v7, Lcom/jme3/math/Vector2f;->y:F

    const v8, 0x3ecccccd    # 0.4f

    mul-float/2addr v7, v8

    sub-float/2addr v6, v7

    iput v6, v5, Lcom/jme3/math/Vector2f;->y:F

    iget-object v5, p0, Lcom/google/android/finsky/exploreactivity/NodeController;->mCenter:Lcom/jme3/math/Vector2f;

    iget v6, v5, Lcom/jme3/math/Vector2f;->x:F

    iget-object v7, p0, Lcom/google/android/finsky/exploreactivity/NodeController;->mSize:Lcom/jme3/math/Vector2f;

    iget v7, v7, Lcom/jme3/math/Vector2f;->x:F

    const v8, 0x3e2e147b    # 0.17f

    mul-float/2addr v7, v8

    sub-float/2addr v6, v7

    iput v6, v5, Lcom/jme3/math/Vector2f;->x:F

    iget-object v5, p0, Lcom/google/android/finsky/exploreactivity/NodeController;->mSize:Lcom/jme3/math/Vector2f;

    iget v5, v5, Lcom/jme3/math/Vector2f;->x:F

    iget-object v6, p0, Lcom/google/android/finsky/exploreactivity/NodeController;->mSize:Lcom/jme3/math/Vector2f;

    iget v6, v6, Lcom/jme3/math/Vector2f;->x:F

    mul-float/2addr v5, v6

    iget-object v6, p0, Lcom/google/android/finsky/exploreactivity/NodeController;->mSize:Lcom/jme3/math/Vector2f;

    iget v6, v6, Lcom/jme3/math/Vector2f;->y:F

    iget-object v7, p0, Lcom/google/android/finsky/exploreactivity/NodeController;->mSize:Lcom/jme3/math/Vector2f;

    iget v7, v7, Lcom/jme3/math/Vector2f;->y:F

    mul-float/2addr v6, v7

    add-float/2addr v5, v6

    float-to-double v5, v5

    invoke-static {v5, v6}, Ljava/lang/Math;->sqrt(D)D

    move-result-wide v5

    double-to-float v5, v5

    const/high16 v6, 0x40000000    # 2.0f

    div-float v1, v5, v6

    iget-object v6, p0, Lcom/google/android/finsky/exploreactivity/NodeController;->mRequestsToCancel:Ljava/util/List;

    monitor-enter v6

    :try_start_0
    iget-object v5, p0, Lcom/google/android/finsky/exploreactivity/NodeController;->mSeedNode:Lcom/google/android/finsky/exploreactivity/DocumentNode;

    invoke-direct {p0, v5, v1}, Lcom/google/android/finsky/exploreactivity/NodeController;->prepareOnscreenNodes(Lcom/google/android/finsky/exploreactivity/DocumentNode;F)V

    monitor-exit v6
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    iget-object v5, p0, Lcom/google/android/finsky/exploreactivity/NodeController;->mRequestsToCancel:Ljava/util/List;

    invoke-interface {v5}, Ljava/util/List;->size()I

    move-result v5

    if-lez v5, :cond_0

    new-instance v5, Lcom/google/android/finsky/exploreactivity/NodeController$4;

    invoke-direct {v5, p0}, Lcom/google/android/finsky/exploreactivity/NodeController$4;-><init>(Lcom/google/android/finsky/exploreactivity/NodeController;)V

    invoke-virtual {p0, v5}, Lcom/google/android/finsky/exploreactivity/NodeController;->runOnUiThread(Ljava/lang/Runnable;)V

    :cond_0
    invoke-direct {p0, p2}, Lcom/google/android/finsky/exploreactivity/NodeController;->animateNodes(F)V

    const/4 v2, 0x0

    :goto_0
    iget-object v5, p0, Lcom/google/android/finsky/exploreactivity/NodeController;->mCurrentNodes:Ljava/util/List;

    invoke-interface {v5}, Ljava/util/List;->size()I

    move-result v5

    if-ge v2, v5, :cond_3

    iget-object v5, p0, Lcom/google/android/finsky/exploreactivity/NodeController;->mCurrentNodes:Ljava/util/List;

    invoke-interface {v5, v2}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Lcom/google/android/finsky/exploreactivity/DocumentNode;

    iget-object v5, p0, Lcom/google/android/finsky/exploreactivity/NodeController;->mPreviousNodes:Ljava/util/List;

    invoke-interface {v5, v3}, Ljava/util/List;->contains(Ljava/lang/Object;)Z

    move-result v5

    if-nez v5, :cond_2

    iget-object v5, p0, Lcom/google/android/finsky/exploreactivity/NodeController;->mFadeOutNodes:Ljava/util/List;

    invoke-interface {v5, v3}, Ljava/util/List;->contains(Ljava/lang/Object;)Z

    move-result v5

    if-eqz v5, :cond_1

    iget-object v5, p0, Lcom/google/android/finsky/exploreactivity/NodeController;->mFadeOutNodes:Ljava/util/List;

    invoke-interface {v5, v3}, Ljava/util/List;->remove(Ljava/lang/Object;)Z

    :cond_1
    iget-object v5, p0, Lcom/google/android/finsky/exploreactivity/NodeController;->mCurrentNodes:Ljava/util/List;

    invoke-interface {v5, v2}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v5

    check-cast v5, Lcom/google/android/finsky/exploreactivity/DocumentNode;

    const/4 v6, 0x1

    invoke-virtual {v5, v6}, Lcom/google/android/finsky/exploreactivity/DocumentNode;->fade(Z)V

    :cond_2
    add-int/lit8 v2, v2, 0x1

    goto :goto_0

    :catchall_0
    move-exception v5

    :try_start_1
    monitor-exit v6
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    throw v5

    :cond_3
    const/4 v2, 0x0

    :goto_1
    iget-object v5, p0, Lcom/google/android/finsky/exploreactivity/NodeController;->mPreviousNodes:Ljava/util/List;

    invoke-interface {v5}, Ljava/util/List;->size()I

    move-result v5

    if-ge v2, v5, :cond_5

    iget-object v5, p0, Lcom/google/android/finsky/exploreactivity/NodeController;->mCurrentNodes:Ljava/util/List;

    iget-object v6, p0, Lcom/google/android/finsky/exploreactivity/NodeController;->mPreviousNodes:Ljava/util/List;

    invoke-interface {v6, v2}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v6

    invoke-interface {v5, v6}, Ljava/util/List;->contains(Ljava/lang/Object;)Z

    move-result v5

    if-nez v5, :cond_4

    iget-object v5, p0, Lcom/google/android/finsky/exploreactivity/NodeController;->mFadeOutNodes:Ljava/util/List;

    iget-object v6, p0, Lcom/google/android/finsky/exploreactivity/NodeController;->mPreviousNodes:Ljava/util/List;

    invoke-interface {v6, v2}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v6

    invoke-interface {v5, v6}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    iget-object v5, p0, Lcom/google/android/finsky/exploreactivity/NodeController;->mPreviousNodes:Ljava/util/List;

    invoke-interface {v5, v2}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v5

    check-cast v5, Lcom/google/android/finsky/exploreactivity/DocumentNode;

    const/4 v6, 0x0

    invoke-virtual {v5, v6}, Lcom/google/android/finsky/exploreactivity/DocumentNode;->fade(Z)V

    :cond_4
    add-int/lit8 v2, v2, 0x1

    goto :goto_1

    :cond_5
    iget-object v5, p0, Lcom/google/android/finsky/exploreactivity/NodeController;->mReturnNodes:Ljava/util/List;

    invoke-interface {v5}, Ljava/util/List;->clear()V

    invoke-direct {p0}, Lcom/google/android/finsky/exploreactivity/NodeController;->processFadeOutNodes()Ljava/util/List;

    move-result-object v0

    const/4 v2, 0x0

    :goto_2
    invoke-interface {v0}, Ljava/util/List;->size()I

    move-result v5

    if-ge v2, v5, :cond_6

    iget-object v5, p0, Lcom/google/android/finsky/exploreactivity/NodeController;->mReturnNodes:Ljava/util/List;

    invoke-interface {v0, v2}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v6

    invoke-interface {v5, v6}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    add-int/lit8 v2, v2, 0x1

    goto :goto_2

    :cond_6
    const/4 v2, 0x0

    :goto_3
    iget-object v5, p0, Lcom/google/android/finsky/exploreactivity/NodeController;->mCurrentNodes:Ljava/util/List;

    invoke-interface {v5}, Ljava/util/List;->size()I

    move-result v5

    if-ge v2, v5, :cond_7

    iget-object v5, p0, Lcom/google/android/finsky/exploreactivity/NodeController;->mReturnNodes:Ljava/util/List;

    iget-object v6, p0, Lcom/google/android/finsky/exploreactivity/NodeController;->mCurrentNodes:Ljava/util/List;

    invoke-interface {v6, v2}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v6

    invoke-interface {v5, v6}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    add-int/lit8 v2, v2, 0x1

    goto :goto_3

    :cond_7
    const/4 v2, 0x0

    :goto_4
    iget-object v5, p0, Lcom/google/android/finsky/exploreactivity/NodeController;->mReturnNodes:Ljava/util/List;

    invoke-interface {v5}, Ljava/util/List;->size()I

    move-result v5

    if-ge v2, v5, :cond_8

    iget-object v5, p0, Lcom/google/android/finsky/exploreactivity/NodeController;->mReturnNodes:Ljava/util/List;

    invoke-interface {v5, v2}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v5

    check-cast v5, Lcom/google/android/finsky/exploreactivity/DocumentNode;

    invoke-virtual {p0, v5}, Lcom/google/android/finsky/exploreactivity/NodeController;->getNodeCenter(Lcom/google/android/finsky/exploreactivity/DocumentNode;)Lcom/jme3/math/Vector2f;

    add-int/lit8 v2, v2, 0x1

    goto :goto_4

    :cond_8
    sget-boolean v5, Lcom/google/android/finsky/utils/FinskyLog;->DEBUG:Z

    if-eqz v5, :cond_9

    iget v5, p0, Lcom/google/android/finsky/exploreactivity/NodeController;->mFrameNum:I

    add-int/lit8 v5, v5, 0x1

    iput v5, p0, Lcom/google/android/finsky/exploreactivity/NodeController;->mFrameNum:I

    rem-int/lit16 v5, v5, 0x1f4

    if-nez v5, :cond_9

    invoke-virtual {p0}, Lcom/google/android/finsky/exploreactivity/NodeController;->dumpControllerState()V

    :cond_9
    iget-object v5, p0, Lcom/google/android/finsky/exploreactivity/NodeController;->mReturnNodes:Ljava/util/List;

    return-object v5
.end method

.method public resetPlayback(I)V
    .locals 1
    .param p1    # I

    const/4 v0, 0x0

    iput-object v0, p0, Lcom/google/android/finsky/exploreactivity/NodeController;->mSongPlayingNow:Ljava/lang/String;

    iget-object v0, p0, Lcom/google/android/finsky/exploreactivity/NodeController;->mPlayer:Lcom/google/android/finsky/previews/MediaPlayerWrapper;

    invoke-virtual {v0}, Lcom/google/android/finsky/previews/MediaPlayerWrapper;->pause()V

    invoke-direct {p0, p1}, Lcom/google/android/finsky/exploreactivity/NodeController;->updatePlayState(I)V

    return-void
.end method

.method public rotate(F)V
    .locals 8
    .param p1    # F

    const/4 v7, 0x0

    const v6, 0x3f060a92

    iget v4, p0, Lcom/google/android/finsky/exploreactivity/NodeController;->mRotation:F

    add-float/2addr v4, p1

    invoke-direct {p0, v4}, Lcom/google/android/finsky/exploreactivity/NodeController;->normalizeAngle(F)F

    move-result v4

    iput v4, p0, Lcom/google/android/finsky/exploreactivity/NodeController;->mRotation:F

    iget-object v4, p0, Lcom/google/android/finsky/exploreactivity/NodeController;->mCenterNode:Lcom/google/android/finsky/exploreactivity/DocumentNode;

    invoke-virtual {v4}, Lcom/google/android/finsky/exploreactivity/DocumentNode;->getChildNodes()Ljava/util/List;

    move-result-object v2

    invoke-interface {v2}, Ljava/util/List;->size()I

    move-result v3

    if-nez v3, :cond_1

    :cond_0
    :goto_0
    return-void

    :cond_1
    const/4 v0, 0x0

    :goto_1
    sget-object v4, Lcom/google/android/finsky/exploreactivity/NodeController;->FIXED_CHILD_ANGLES:[F

    array-length v4, v4

    if-ge v0, v4, :cond_2

    iget-object v4, p0, Lcom/google/android/finsky/exploreactivity/NodeController;->mCenterNode:Lcom/google/android/finsky/exploreactivity/DocumentNode;

    invoke-virtual {v4, v0}, Lcom/google/android/finsky/exploreactivity/DocumentNode;->getChildNode(I)Lcom/google/android/finsky/exploreactivity/DocumentNode;

    move-result-object v1

    invoke-virtual {v1}, Lcom/google/android/finsky/exploreactivity/DocumentNode;->getAngle()F

    move-result v4

    add-float/2addr v4, p1

    invoke-direct {p0, v4}, Lcom/google/android/finsky/exploreactivity/NodeController;->normalizeAngle(F)F

    move-result v4

    invoke-virtual {v1, v4}, Lcom/google/android/finsky/exploreactivity/DocumentNode;->setAngle(F)V

    add-int/lit8 v0, v0, 0x1

    goto :goto_1

    :cond_2
    iget v4, p0, Lcom/google/android/finsky/exploreactivity/NodeController;->mRotation:F

    cmpl-float v4, v4, v6

    if-lez v4, :cond_0

    const v4, 0x40c90fdb

    iget v5, p0, Lcom/google/android/finsky/exploreactivity/NodeController;->mRotation:F

    sub-float/2addr v4, v5

    cmpl-float v4, v4, v6

    if-lez v4, :cond_0

    iget v4, p0, Lcom/google/android/finsky/exploreactivity/NodeController;->mRotation:F

    const v5, 0x40490fdb    # (float)Math.PI

    cmpg-float v4, v4, v5

    if-gez v4, :cond_3

    iget v4, p0, Lcom/google/android/finsky/exploreactivity/NodeController;->mRotation:F

    sub-float/2addr v4, v6

    invoke-direct {p0, v4}, Lcom/google/android/finsky/exploreactivity/NodeController;->normalizeAngle(F)F

    move-result v4

    iput v4, p0, Lcom/google/android/finsky/exploreactivity/NodeController;->mRotation:F

    iget-object v4, p0, Lcom/google/android/finsky/exploreactivity/NodeController;->mCenterNode:Lcom/google/android/finsky/exploreactivity/DocumentNode;

    const/4 v5, 0x1

    invoke-virtual {v4, v5}, Lcom/google/android/finsky/exploreactivity/DocumentNode;->rotateChildren(I)V

    const-string v4, "Applied child rotation of +1"

    new-array v5, v7, [Ljava/lang/Object;

    invoke-static {v4, v5}, Lcom/google/android/finsky/utils/FinskyLog;->d(Ljava/lang/String;[Ljava/lang/Object;)V

    goto :goto_0

    :cond_3
    iget v4, p0, Lcom/google/android/finsky/exploreactivity/NodeController;->mRotation:F

    add-float/2addr v4, v6

    invoke-direct {p0, v4}, Lcom/google/android/finsky/exploreactivity/NodeController;->normalizeAngle(F)F

    move-result v4

    iput v4, p0, Lcom/google/android/finsky/exploreactivity/NodeController;->mRotation:F

    iget-object v4, p0, Lcom/google/android/finsky/exploreactivity/NodeController;->mCenterNode:Lcom/google/android/finsky/exploreactivity/DocumentNode;

    const/4 v5, -0x1

    invoke-virtual {v4, v5}, Lcom/google/android/finsky/exploreactivity/DocumentNode;->rotateChildren(I)V

    const-string v4, "Applied child rotation of -1"

    new-array v5, v7, [Ljava/lang/Object;

    invoke-static {v4, v5}, Lcom/google/android/finsky/utils/FinskyLog;->d(Ljava/lang/String;[Ljava/lang/Object;)V

    goto :goto_0
.end method

.method public runOnGlThread(Ljava/lang/Runnable;)V
    .locals 1
    .param p1    # Ljava/lang/Runnable;

    iget-object v0, p0, Lcom/google/android/finsky/exploreactivity/NodeController;->mActivity:Lcom/google/android/finsky/exploreactivity/ExploreActivity;

    invoke-virtual {v0, p1}, Lcom/google/android/finsky/exploreactivity/ExploreActivity;->runOnGlThread(Ljava/lang/Runnable;)V

    return-void
.end method

.method public runOnUiThread(Ljava/lang/Runnable;)V
    .locals 1
    .param p1    # Ljava/lang/Runnable;

    iget-object v0, p0, Lcom/google/android/finsky/exploreactivity/NodeController;->mActivity:Lcom/google/android/finsky/exploreactivity/ExploreActivity;

    invoke-virtual {v0, p1}, Lcom/google/android/finsky/exploreactivity/ExploreActivity;->runOnUiThread(Ljava/lang/Runnable;)V

    return-void
.end method

.method public setCenterNode(Lcom/google/android/finsky/exploreactivity/DocumentNode;)V
    .locals 10
    .param p1    # Lcom/google/android/finsky/exploreactivity/DocumentNode;

    const/4 v9, 0x2

    const/4 v8, 0x0

    const/4 v7, 0x1

    const-string v2, "New center: %s"

    new-array v3, v7, [Ljava/lang/Object;

    invoke-virtual {p1}, Lcom/google/android/finsky/exploreactivity/DocumentNode;->toString()Ljava/lang/String;

    move-result-object v4

    aput-object v4, v3, v8

    invoke-static {v2, v3}, Lcom/google/android/finsky/utils/FinskyLog;->d(Ljava/lang/String;[Ljava/lang/Object;)V

    iget-object v2, p0, Lcom/google/android/finsky/exploreactivity/NodeController;->mCenterNodeCenter:Lcom/jme3/math/Vector2f;

    invoke-virtual {p0, p1}, Lcom/google/android/finsky/exploreactivity/NodeController;->getNodeCenter(Lcom/google/android/finsky/exploreactivity/DocumentNode;)Lcom/jme3/math/Vector2f;

    move-result-object v3

    invoke-virtual {v2, v3}, Lcom/jme3/math/Vector2f;->set(Lcom/jme3/math/Vector2f;)Lcom/jme3/math/Vector2f;

    iput-object p1, p0, Lcom/google/android/finsky/exploreactivity/NodeController;->mCenterNode:Lcom/google/android/finsky/exploreactivity/DocumentNode;

    const/4 v2, 0x0

    iput v2, p0, Lcom/google/android/finsky/exploreactivity/NodeController;->mRotation:F

    invoke-static {}, Lcom/google/android/finsky/FinskyApp;->get()Lcom/google/android/finsky/FinskyApp;

    move-result-object v2

    invoke-virtual {v2}, Lcom/google/android/finsky/FinskyApp;->getAnalytics()Lcom/google/android/finsky/analytics/Analytics;

    move-result-object v2

    const/4 v3, 0x0

    invoke-virtual {p1}, Lcom/google/android/finsky/exploreactivity/DocumentNode;->getDoc()Lcom/google/android/finsky/exploreactivity/DocWrapper;

    move-result-object v4

    invoke-virtual {v4}, Lcom/google/android/finsky/exploreactivity/DocWrapper;->getDoc()Lcom/google/android/finsky/api/model/Document;

    move-result-object v4

    invoke-virtual {v4}, Lcom/google/android/finsky/api/model/Document;->getCookie()Ljava/lang/String;

    move-result-object v4

    new-instance v5, Ljava/lang/StringBuilder;

    invoke-direct {v5}, Ljava/lang/StringBuilder;-><init>()V

    const-string v6, "exploreNavigateTo?doc="

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {p1}, Lcom/google/android/finsky/exploreactivity/DocumentNode;->getDoc()Lcom/google/android/finsky/exploreactivity/DocWrapper;

    move-result-object v6

    invoke-virtual {v6}, Lcom/google/android/finsky/exploreactivity/DocWrapper;->getDocId()Ljava/lang/String;

    move-result-object v6

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    invoke-interface {v2, v3, v4, v5}, Lcom/google/android/finsky/analytics/Analytics;->logPageView(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    invoke-static {}, Lcom/google/android/finsky/FinskyApp;->get()Lcom/google/android/finsky/FinskyApp;

    move-result-object v2

    invoke-virtual {v2}, Lcom/google/android/finsky/FinskyApp;->getEventLogger()Lcom/google/android/finsky/analytics/FinskyEventLog;

    move-result-object v2

    const-string v3, "exploreNavigateTo"

    new-array v4, v9, [Ljava/lang/Object;

    const-string v5, "cidi"

    aput-object v5, v4, v8

    invoke-virtual {p1}, Lcom/google/android/finsky/exploreactivity/DocumentNode;->getDoc()Lcom/google/android/finsky/exploreactivity/DocWrapper;

    move-result-object v5

    invoke-virtual {v5}, Lcom/google/android/finsky/exploreactivity/DocWrapper;->getDocId()Ljava/lang/String;

    move-result-object v5

    aput-object v5, v4, v7

    invoke-virtual {v2, v3, v4}, Lcom/google/android/finsky/analytics/FinskyEventLog;->logTag(Ljava/lang/String;[Ljava/lang/Object;)V

    iget v2, p0, Lcom/google/android/finsky/exploreactivity/NodeController;->mPlayState:I

    const/4 v3, 0x3

    if-eq v2, v3, :cond_0

    iget v2, p0, Lcom/google/android/finsky/exploreactivity/NodeController;->mPlayState:I

    if-ne v2, v9, :cond_3

    :cond_0
    invoke-virtual {p0}, Lcom/google/android/finsky/exploreactivity/NodeController;->startPlayback()V

    :goto_0
    const/4 v1, 0x1

    move-object v0, p1

    :cond_1
    invoke-virtual {v0}, Lcom/google/android/finsky/exploreactivity/DocumentNode;->hasParentNode()Z

    move-result v2

    if-eqz v2, :cond_2

    invoke-virtual {v0}, Lcom/google/android/finsky/exploreactivity/DocumentNode;->getParentNode()Lcom/google/android/finsky/exploreactivity/DocumentNode;

    move-result-object v0

    add-int/lit8 v1, v1, 0x1

    const/16 v2, 0xa

    if-le v1, v2, :cond_1

    iput-object v0, p0, Lcom/google/android/finsky/exploreactivity/NodeController;->mSeedNode:Lcom/google/android/finsky/exploreactivity/DocumentNode;

    invoke-virtual {v0}, Lcom/google/android/finsky/exploreactivity/DocumentNode;->clearParentNode()V

    :cond_2
    return-void

    :cond_3
    invoke-virtual {p0, v7}, Lcom/google/android/finsky/exploreactivity/NodeController;->resetPlayback(I)V

    goto :goto_0
.end method

.method public setSongListener(Lcom/google/android/finsky/exploreactivity/NodeController$SongListener;)V
    .locals 0
    .param p1    # Lcom/google/android/finsky/exploreactivity/NodeController$SongListener;

    iput-object p1, p0, Lcom/google/android/finsky/exploreactivity/NodeController;->mSongListener:Lcom/google/android/finsky/exploreactivity/NodeController$SongListener;

    return-void
.end method

.method public shouldShowPlaybackControls()Z
    .locals 2

    iget-object v0, p0, Lcom/google/android/finsky/exploreactivity/NodeController;->mCenterNode:Lcom/google/android/finsky/exploreactivity/DocumentNode;

    invoke-virtual {v0}, Lcom/google/android/finsky/exploreactivity/DocumentNode;->getDoc()Lcom/google/android/finsky/exploreactivity/DocWrapper;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/android/finsky/exploreactivity/DocWrapper;->getDoc()Lcom/google/android/finsky/api/model/Document;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/android/finsky/api/model/Document;->getBackend()I

    move-result v0

    const/4 v1, 0x2

    if-ne v0, v1, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public startPlayback()V
    .locals 6

    const/4 v5, 0x1

    iget-object v1, p0, Lcom/google/android/finsky/exploreactivity/NodeController;->mCenterNode:Lcom/google/android/finsky/exploreactivity/DocumentNode;

    iget-object v1, v1, Lcom/google/android/finsky/exploreactivity/DocumentNode;->mDocWrapper:Lcom/google/android/finsky/exploreactivity/DocWrapper;

    invoke-virtual {v1}, Lcom/google/android/finsky/exploreactivity/DocWrapper;->getSong()Lcom/google/android/finsky/api/model/Document;

    move-result-object v0

    if-eqz v0, :cond_1

    iget v1, p0, Lcom/google/android/finsky/exploreactivity/NodeController;->mPlayState:I

    if-ne v1, v5, :cond_0

    const/4 v1, 0x2

    iput v1, p0, Lcom/google/android/finsky/exploreactivity/NodeController;->mPlayState:I

    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v1

    iput-wide v1, p0, Lcom/google/android/finsky/exploreactivity/NodeController;->mPlaybackShouldStartTime:J

    :cond_0
    const-string v1, "Fetching preview of %s"

    new-array v2, v5, [Ljava/lang/Object;

    const/4 v3, 0x0

    invoke-virtual {v0}, Lcom/google/android/finsky/api/model/Document;->getTitle()Ljava/lang/String;

    move-result-object v4

    aput-object v4, v2, v3

    invoke-static {v1, v2}, Lcom/google/android/finsky/utils/FinskyLog;->d(Ljava/lang/String;[Ljava/lang/Object;)V

    iget-object v1, p0, Lcom/google/android/finsky/exploreactivity/NodeController;->mMusicPreviewManager:Lcom/google/android/finsky/exploreactivity/MusicPreviewManager;

    iget-object v2, p0, Lcom/google/android/finsky/exploreactivity/NodeController;->mCenterNode:Lcom/google/android/finsky/exploreactivity/DocumentNode;

    iget-object v2, v2, Lcom/google/android/finsky/exploreactivity/DocumentNode;->mDocWrapper:Lcom/google/android/finsky/exploreactivity/DocWrapper;

    invoke-virtual {v1, v2, p0, v5}, Lcom/google/android/finsky/exploreactivity/MusicPreviewManager;->fetchPreview(Lcom/google/android/finsky/exploreactivity/DocWrapper;Lcom/google/android/finsky/exploreactivity/MusicPreviewManager$MusicPreviewListener;Z)V

    :cond_1
    return-void
.end method

.method public togglePlayback()V
    .locals 4

    const/4 v3, 0x1

    iget v0, p0, Lcom/google/android/finsky/exploreactivity/NodeController;->mPlayState:I

    const/4 v1, 0x3

    if-ne v0, v1, :cond_0

    invoke-direct {p0, v3}, Lcom/google/android/finsky/exploreactivity/NodeController;->updatePlayState(I)V

    :goto_0
    return-void

    :cond_0
    iget-object v0, p0, Lcom/google/android/finsky/exploreactivity/NodeController;->mPlayer:Lcom/google/android/finsky/previews/MediaPlayerWrapper;

    invoke-virtual {v0}, Lcom/google/android/finsky/previews/MediaPlayerWrapper;->isPlaying()Z

    move-result v0

    if-eqz v0, :cond_1

    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/google/android/finsky/exploreactivity/NodeController;->mFadingIn:Z

    goto :goto_0

    :cond_1
    iget-object v0, p0, Lcom/google/android/finsky/exploreactivity/NodeController;->mSongPlayingNow:Ljava/lang/String;

    if-eqz v0, :cond_2

    iget-object v0, p0, Lcom/google/android/finsky/exploreactivity/NodeController;->mPlayer:Lcom/google/android/finsky/previews/MediaPlayerWrapper;

    iget v1, p0, Lcom/google/android/finsky/exploreactivity/NodeController;->mVolume:F

    iget v2, p0, Lcom/google/android/finsky/exploreactivity/NodeController;->mVolume:F

    invoke-virtual {v0, v1, v2}, Lcom/google/android/finsky/previews/MediaPlayerWrapper;->setVolume(FF)V

    iget-object v0, p0, Lcom/google/android/finsky/exploreactivity/NodeController;->mPlayer:Lcom/google/android/finsky/previews/MediaPlayerWrapper;

    invoke-virtual {v0}, Lcom/google/android/finsky/previews/MediaPlayerWrapper;->start()V

    iput-boolean v3, p0, Lcom/google/android/finsky/exploreactivity/NodeController;->mFadingIn:Z

    goto :goto_0

    :cond_2
    invoke-virtual {p0}, Lcom/google/android/finsky/exploreactivity/NodeController;->startPlayback()V

    goto :goto_0
.end method

.method public toggleWishlist(Lcom/google/android/finsky/exploreactivity/DocumentNode;)V
    .locals 2
    .param p1    # Lcom/google/android/finsky/exploreactivity/DocumentNode;

    new-instance v0, Lcom/google/android/finsky/exploreactivity/NodeController$10;

    invoke-direct {v0, p0, p1}, Lcom/google/android/finsky/exploreactivity/NodeController$10;-><init>(Lcom/google/android/finsky/exploreactivity/NodeController;Lcom/google/android/finsky/exploreactivity/DocumentNode;)V

    new-instance v1, Lcom/google/android/finsky/exploreactivity/NodeController$11;

    invoke-direct {v1, p0, p1, v0}, Lcom/google/android/finsky/exploreactivity/NodeController$11;-><init>(Lcom/google/android/finsky/exploreactivity/NodeController;Lcom/google/android/finsky/exploreactivity/DocumentNode;Lcom/google/android/finsky/utils/WishlistHelper$WishlistStatusListener;)V

    invoke-virtual {p0, v1}, Lcom/google/android/finsky/exploreactivity/NodeController;->runOnUiThread(Ljava/lang/Runnable;)V

    return-void
.end method
