.class Lcom/google/android/finsky/activities/SimpleAlertDialog$1;
.super Ljava/lang/Object;
.source "SimpleAlertDialog.java"

# interfaces
.implements Landroid/content/DialogInterface$OnClickListener;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/google/android/finsky/activities/SimpleAlertDialog;->onCreateDialog(Landroid/os/Bundle;)Landroid/app/Dialog;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/google/android/finsky/activities/SimpleAlertDialog;

.field final synthetic val$clickEventTypePositive:I

.field final synthetic val$extraArguments:Landroid/os/Bundle;

.field final synthetic val$finalDialogNode:Lcom/google/android/finsky/layout/play/PlayStoreUiElementNode;

.field final synthetic val$requestCode:I


# direct methods
.method constructor <init>(Lcom/google/android/finsky/activities/SimpleAlertDialog;ILcom/google/android/finsky/layout/play/PlayStoreUiElementNode;ILandroid/os/Bundle;)V
    .locals 0

    iput-object p1, p0, Lcom/google/android/finsky/activities/SimpleAlertDialog$1;->this$0:Lcom/google/android/finsky/activities/SimpleAlertDialog;

    iput p2, p0, Lcom/google/android/finsky/activities/SimpleAlertDialog$1;->val$clickEventTypePositive:I

    iput-object p3, p0, Lcom/google/android/finsky/activities/SimpleAlertDialog$1;->val$finalDialogNode:Lcom/google/android/finsky/layout/play/PlayStoreUiElementNode;

    iput p4, p0, Lcom/google/android/finsky/activities/SimpleAlertDialog$1;->val$requestCode:I

    iput-object p5, p0, Lcom/google/android/finsky/activities/SimpleAlertDialog$1;->val$extraArguments:Landroid/os/Bundle;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onClick(Landroid/content/DialogInterface;I)V
    .locals 5
    .param p1    # Landroid/content/DialogInterface;
    .param p2    # I

    iget-object v1, p0, Lcom/google/android/finsky/activities/SimpleAlertDialog$1;->this$0:Lcom/google/android/finsky/activities/SimpleAlertDialog;

    invoke-virtual {v1}, Lcom/google/android/finsky/activities/SimpleAlertDialog;->dismiss()V

    iget-object v1, p0, Lcom/google/android/finsky/activities/SimpleAlertDialog$1;->this$0:Lcom/google/android/finsky/activities/SimpleAlertDialog;

    # getter for: Lcom/google/android/finsky/activities/SimpleAlertDialog;->mHasBeenDismissed:Z
    invoke-static {v1}, Lcom/google/android/finsky/activities/SimpleAlertDialog;->access$000(Lcom/google/android/finsky/activities/SimpleAlertDialog;)Z

    move-result v1

    if-eqz v1, :cond_1

    :cond_0
    :goto_0
    return-void

    :cond_1
    iget-object v1, p0, Lcom/google/android/finsky/activities/SimpleAlertDialog$1;->this$0:Lcom/google/android/finsky/activities/SimpleAlertDialog;

    const/4 v2, 0x1

    # setter for: Lcom/google/android/finsky/activities/SimpleAlertDialog;->mHasBeenDismissed:Z
    invoke-static {v1, v2}, Lcom/google/android/finsky/activities/SimpleAlertDialog;->access$002(Lcom/google/android/finsky/activities/SimpleAlertDialog;Z)Z

    iget v1, p0, Lcom/google/android/finsky/activities/SimpleAlertDialog$1;->val$clickEventTypePositive:I

    const/4 v2, -0x1

    if-eq v1, v2, :cond_2

    invoke-static {}, Lcom/google/android/finsky/FinskyApp;->get()Lcom/google/android/finsky/FinskyApp;

    move-result-object v1

    invoke-virtual {v1}, Lcom/google/android/finsky/FinskyApp;->getEventLogger()Lcom/google/android/finsky/analytics/FinskyEventLog;

    move-result-object v1

    iget v2, p0, Lcom/google/android/finsky/activities/SimpleAlertDialog$1;->val$clickEventTypePositive:I

    const/4 v3, 0x0

    iget-object v4, p0, Lcom/google/android/finsky/activities/SimpleAlertDialog$1;->val$finalDialogNode:Lcom/google/android/finsky/layout/play/PlayStoreUiElementNode;

    invoke-virtual {v1, v2, v3, v4}, Lcom/google/android/finsky/analytics/FinskyEventLog;->logClickEvent(ILcom/google/protobuf/micro/ByteStringMicro;Lcom/google/android/finsky/layout/play/PlayStoreUiElementNode;)V

    :cond_2
    iget-object v1, p0, Lcom/google/android/finsky/activities/SimpleAlertDialog$1;->this$0:Lcom/google/android/finsky/activities/SimpleAlertDialog;

    # invokes: Lcom/google/android/finsky/activities/SimpleAlertDialog;->getListener()Lcom/google/android/finsky/activities/SimpleAlertDialog$Listener;
    invoke-static {v1}, Lcom/google/android/finsky/activities/SimpleAlertDialog;->access$100(Lcom/google/android/finsky/activities/SimpleAlertDialog;)Lcom/google/android/finsky/activities/SimpleAlertDialog$Listener;

    move-result-object v0

    if-eqz v0, :cond_0

    iget v1, p0, Lcom/google/android/finsky/activities/SimpleAlertDialog$1;->val$requestCode:I

    iget-object v2, p0, Lcom/google/android/finsky/activities/SimpleAlertDialog$1;->val$extraArguments:Landroid/os/Bundle;

    invoke-interface {v0, v1, v2}, Lcom/google/android/finsky/activities/SimpleAlertDialog$Listener;->onPositiveClick(ILandroid/os/Bundle;)V

    goto :goto_0
.end method
