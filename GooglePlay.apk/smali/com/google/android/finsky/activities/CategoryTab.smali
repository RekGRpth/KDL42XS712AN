.class public Lcom/google/android/finsky/activities/CategoryTab;
.super Ljava/lang/Object;
.source "CategoryTab.java"

# interfaces
.implements Lcom/google/android/finsky/activities/ViewPagerTab;


# instance fields
.field private final mBitmapLoader:Lcom/google/android/finsky/utils/BitmapLoader;

.field private mCategoryAdapter:Lcom/google/android/finsky/adapters/CategoryAdapter;

.field private mCategoryView:Landroid/view/ViewGroup;

.field private final mContext:Landroid/content/Context;

.field private final mCurrentBrowseUrl:Ljava/lang/String;

.field private final mData:Lcom/google/android/finsky/api/model/DfeBrowse;

.field private mIsCurrentlySelected:Z

.field private final mLayoutInflater:Landroid/view/LayoutInflater;

.field private final mNavigationManager:Lcom/google/android/finsky/navigationmanager/NavigationManager;

.field private mParentNode:Lcom/google/android/finsky/layout/play/SelectableUiElementNode;

.field private final mQuickLinks:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Lcom/google/android/finsky/adapters/QuickLinkHelper$QuickLinkInfo;",
            ">;"
        }
    .end annotation
.end field

.field private final mTabTitle:Ljava/lang/String;

.field private final mToc:Lcom/google/android/finsky/api/model/DfeToc;


# direct methods
.method public constructor <init>(Landroid/content/Context;Lcom/google/android/finsky/navigationmanager/NavigationManager;Lcom/google/android/finsky/utils/BitmapLoader;Landroid/view/LayoutInflater;Lcom/google/android/finsky/activities/TabbedAdapter$TabType;Lcom/google/android/finsky/api/model/DfeBrowse;Ljava/lang/String;Lcom/google/android/finsky/api/model/DfeToc;Ljava/lang/String;)V
    .locals 2
    .param p1    # Landroid/content/Context;
    .param p2    # Lcom/google/android/finsky/navigationmanager/NavigationManager;
    .param p3    # Lcom/google/android/finsky/utils/BitmapLoader;
    .param p4    # Landroid/view/LayoutInflater;
    .param p5    # Lcom/google/android/finsky/activities/TabbedAdapter$TabType;
    .param p6    # Lcom/google/android/finsky/api/model/DfeBrowse;
    .param p7    # Ljava/lang/String;
    .param p8    # Lcom/google/android/finsky/api/model/DfeToc;
    .param p9    # Ljava/lang/String;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    if-eqz p6, :cond_0

    invoke-virtual {p6}, Lcom/google/android/finsky/api/model/DfeBrowse;->isReady()Z

    move-result v0

    if-nez v0, :cond_1

    :cond_0
    new-instance v0, Ljava/lang/IllegalStateException;

    const-string v1, "Tried to create category tab with invalid data!"

    invoke-direct {v0, v1}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    throw v0

    :cond_1
    iput-object p1, p0, Lcom/google/android/finsky/activities/CategoryTab;->mContext:Landroid/content/Context;

    iput-object p2, p0, Lcom/google/android/finsky/activities/CategoryTab;->mNavigationManager:Lcom/google/android/finsky/navigationmanager/NavigationManager;

    iput-object p3, p0, Lcom/google/android/finsky/activities/CategoryTab;->mBitmapLoader:Lcom/google/android/finsky/utils/BitmapLoader;

    iput-object p4, p0, Lcom/google/android/finsky/activities/CategoryTab;->mLayoutInflater:Landroid/view/LayoutInflater;

    iput-object p6, p0, Lcom/google/android/finsky/activities/CategoryTab;->mData:Lcom/google/android/finsky/api/model/DfeBrowse;

    iput-object p7, p0, Lcom/google/android/finsky/activities/CategoryTab;->mCurrentBrowseUrl:Ljava/lang/String;

    iput-object p8, p0, Lcom/google/android/finsky/activities/CategoryTab;->mToc:Lcom/google/android/finsky/api/model/DfeToc;

    iput-object p9, p0, Lcom/google/android/finsky/activities/CategoryTab;->mTabTitle:Ljava/lang/String;

    iget-object v0, p5, Lcom/google/android/finsky/activities/TabbedAdapter$TabType;->mQuickLinks:Ljava/util/List;

    iput-object v0, p0, Lcom/google/android/finsky/activities/CategoryTab;->mQuickLinks:Ljava/util/List;

    iget-object v0, p5, Lcom/google/android/finsky/activities/TabbedAdapter$TabType;->mElementNode:Lcom/google/android/finsky/layout/play/SelectableUiElementNode;

    iput-object v0, p0, Lcom/google/android/finsky/activities/CategoryTab;->mParentNode:Lcom/google/android/finsky/layout/play/SelectableUiElementNode;

    return-void
.end method

.method private logClickForCurrentPage()V
    .locals 4

    invoke-static {}, Lcom/google/android/finsky/FinskyApp;->get()Lcom/google/android/finsky/FinskyApp;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/android/finsky/FinskyApp;->getEventLogger()Lcom/google/android/finsky/analytics/FinskyEventLog;

    move-result-object v0

    iget-object v1, p0, Lcom/google/android/finsky/activities/CategoryTab;->mCurrentBrowseUrl:Ljava/lang/String;

    const/4 v2, 0x0

    const-string v3, "cat"

    invoke-virtual {v0, v1, v2, v3}, Lcom/google/android/finsky/analytics/FinskyEventLog;->logView(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    return-void
.end method


# virtual methods
.method public getView(I)Landroid/view/View;
    .locals 12
    .param p1    # I

    iget-object v0, p0, Lcom/google/android/finsky/activities/CategoryTab;->mCategoryView:Landroid/view/ViewGroup;

    if-nez v0, :cond_0

    iget-object v0, p0, Lcom/google/android/finsky/activities/CategoryTab;->mLayoutInflater:Landroid/view/LayoutInflater;

    const v1, 0x7f04002f    # com.android.vending.R.layout.category_tab

    const/4 v2, 0x0

    invoke-virtual {v0, v1, v2}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/view/ViewGroup;

    iput-object v0, p0, Lcom/google/android/finsky/activities/CategoryTab;->mCategoryView:Landroid/view/ViewGroup;

    iget-object v0, p0, Lcom/google/android/finsky/activities/CategoryTab;->mCategoryView:Landroid/view/ViewGroup;

    const v1, 0x7f080074    # com.android.vending.R.id.bucket_list_view

    invoke-virtual {v0, v1}, Landroid/view/ViewGroup;->findViewById(I)Landroid/view/View;

    move-result-object v11

    check-cast v11, Landroid/widget/ListView;

    const/4 v0, 0x1

    invoke-virtual {v11, v0}, Landroid/widget/ListView;->setItemsCanFocus(Z)V

    const/4 v0, 0x0

    invoke-virtual {v11, v0}, Landroid/widget/ListView;->setVisibility(I)V

    new-instance v0, Lcom/google/android/finsky/adapters/CategoryAdapter;

    iget-object v1, p0, Lcom/google/android/finsky/activities/CategoryTab;->mContext:Landroid/content/Context;

    iget-object v2, p0, Lcom/google/android/finsky/activities/CategoryTab;->mData:Lcom/google/android/finsky/api/model/DfeBrowse;

    iget-object v3, p0, Lcom/google/android/finsky/activities/CategoryTab;->mNavigationManager:Lcom/google/android/finsky/navigationmanager/NavigationManager;

    iget-object v5, p0, Lcom/google/android/finsky/activities/CategoryTab;->mCurrentBrowseUrl:Ljava/lang/String;

    iget-object v6, p0, Lcom/google/android/finsky/activities/CategoryTab;->mToc:Lcom/google/android/finsky/api/model/DfeToc;

    iget-object v7, p0, Lcom/google/android/finsky/activities/CategoryTab;->mBitmapLoader:Lcom/google/android/finsky/utils/BitmapLoader;

    iget-object v8, p0, Lcom/google/android/finsky/activities/CategoryTab;->mQuickLinks:Ljava/util/List;

    iget-object v9, p0, Lcom/google/android/finsky/activities/CategoryTab;->mTabTitle:Ljava/lang/String;

    iget-object v10, p0, Lcom/google/android/finsky/activities/CategoryTab;->mParentNode:Lcom/google/android/finsky/layout/play/SelectableUiElementNode;

    move v4, p1

    invoke-direct/range {v0 .. v10}, Lcom/google/android/finsky/adapters/CategoryAdapter;-><init>(Landroid/content/Context;Lcom/google/android/finsky/api/model/DfeBrowse;Lcom/google/android/finsky/navigationmanager/NavigationManager;ILjava/lang/String;Lcom/google/android/finsky/api/model/DfeToc;Lcom/google/android/finsky/utils/BitmapLoader;Ljava/util/List;Ljava/lang/String;Lcom/google/android/finsky/layout/play/PlayStoreUiElementNode;)V

    iput-object v0, p0, Lcom/google/android/finsky/activities/CategoryTab;->mCategoryAdapter:Lcom/google/android/finsky/adapters/CategoryAdapter;

    iget-object v0, p0, Lcom/google/android/finsky/activities/CategoryTab;->mCategoryAdapter:Lcom/google/android/finsky/adapters/CategoryAdapter;

    invoke-virtual {v11, v0}, Landroid/widget/ListView;->setAdapter(Landroid/widget/ListAdapter;)V

    :cond_0
    iget-object v0, p0, Lcom/google/android/finsky/activities/CategoryTab;->mCategoryView:Landroid/view/ViewGroup;

    return-object v0
.end method

.method public onDestroy()V
    .locals 1

    const/4 v0, 0x0

    iput-object v0, p0, Lcom/google/android/finsky/activities/CategoryTab;->mCategoryAdapter:Lcom/google/android/finsky/adapters/CategoryAdapter;

    iput-object v0, p0, Lcom/google/android/finsky/activities/CategoryTab;->mCategoryView:Landroid/view/ViewGroup;

    return-void
.end method

.method public onRestoreInstanceState(Landroid/os/Bundle;)V
    .locals 0
    .param p1    # Landroid/os/Bundle;

    return-void
.end method

.method public onSaveInstanceState()Landroid/os/Bundle;
    .locals 1

    const/4 v0, 0x0

    return-object v0
.end method

.method public setTabSelected(Z)V
    .locals 2
    .param p1    # Z

    iget-boolean v0, p0, Lcom/google/android/finsky/activities/CategoryTab;->mIsCurrentlySelected:Z

    if-eq p1, v0, :cond_1

    if-eqz p1, :cond_2

    iget-object v0, p0, Lcom/google/android/finsky/activities/CategoryTab;->mParentNode:Lcom/google/android/finsky/layout/play/SelectableUiElementNode;

    invoke-static {v0}, Lcom/google/android/finsky/analytics/FinskyEventLog;->startNewImpression(Lcom/google/android/finsky/layout/play/PlayStoreUiElementNode;)V

    iget-object v0, p0, Lcom/google/android/finsky/activities/CategoryTab;->mParentNode:Lcom/google/android/finsky/layout/play/SelectableUiElementNode;

    const/4 v1, 0x1

    invoke-virtual {v0, v1}, Lcom/google/android/finsky/layout/play/SelectableUiElementNode;->setNodeSelected(Z)V

    iget-object v0, p0, Lcom/google/android/finsky/activities/CategoryTab;->mParentNode:Lcom/google/android/finsky/layout/play/SelectableUiElementNode;

    invoke-virtual {v0}, Lcom/google/android/finsky/layout/play/SelectableUiElementNode;->getPlayStoreUiElement()Lcom/google/android/play/analytics/PlayStore$PlayStoreUiElement;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/android/play/analytics/PlayStore$PlayStoreUiElement;->getChildCount()I

    move-result v0

    if-nez v0, :cond_0

    iget-object v0, p0, Lcom/google/android/finsky/activities/CategoryTab;->mCategoryView:Landroid/view/ViewGroup;

    invoke-static {v0}, Lcom/google/android/finsky/analytics/FinskyEventLog;->requestImpressions(Landroid/view/ViewGroup;)V

    :cond_0
    invoke-direct {p0}, Lcom/google/android/finsky/activities/CategoryTab;->logClickForCurrentPage()V

    :goto_0
    iput-boolean p1, p0, Lcom/google/android/finsky/activities/CategoryTab;->mIsCurrentlySelected:Z

    :cond_1
    return-void

    :cond_2
    iget-object v0, p0, Lcom/google/android/finsky/activities/CategoryTab;->mParentNode:Lcom/google/android/finsky/layout/play/SelectableUiElementNode;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Lcom/google/android/finsky/layout/play/SelectableUiElementNode;->setNodeSelected(Z)V

    goto :goto_0
.end method
