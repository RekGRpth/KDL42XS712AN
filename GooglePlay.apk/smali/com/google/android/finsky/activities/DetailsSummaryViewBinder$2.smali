.class Lcom/google/android/finsky/activities/DetailsSummaryViewBinder$2;
.super Ljava/lang/Object;
.source "DetailsSummaryViewBinder.java"

# interfaces
.implements Landroid/view/View$OnClickListener;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/google/android/finsky/activities/DetailsSummaryViewBinder;->configureCancelButton(Lcom/google/android/finsky/layout/play/PlayActionButton;Lcom/google/android/finsky/api/model/Document;Landroid/accounts/Account;)V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/google/android/finsky/activities/DetailsSummaryViewBinder;

.field final synthetic val$doc:Lcom/google/android/finsky/api/model/Document;

.field final synthetic val$owner:Landroid/accounts/Account;


# direct methods
.method constructor <init>(Lcom/google/android/finsky/activities/DetailsSummaryViewBinder;Lcom/google/android/finsky/api/model/Document;Landroid/accounts/Account;)V
    .locals 0

    iput-object p1, p0, Lcom/google/android/finsky/activities/DetailsSummaryViewBinder$2;->this$0:Lcom/google/android/finsky/activities/DetailsSummaryViewBinder;

    iput-object p2, p0, Lcom/google/android/finsky/activities/DetailsSummaryViewBinder$2;->val$doc:Lcom/google/android/finsky/api/model/Document;

    iput-object p3, p0, Lcom/google/android/finsky/activities/DetailsSummaryViewBinder$2;->val$owner:Landroid/accounts/Account;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onClick(Landroid/view/View;)V
    .locals 9
    .param p1    # Landroid/view/View;

    const/4 v7, 0x0

    iget-object v4, p0, Lcom/google/android/finsky/activities/DetailsSummaryViewBinder$2;->this$0:Lcom/google/android/finsky/activities/DetailsSummaryViewBinder;

    iget-object v4, v4, Lcom/google/android/finsky/activities/DetailsSummaryViewBinder;->mContainerFragment:Lcom/google/android/finsky/fragments/PageFragment;

    invoke-virtual {v4}, Lcom/google/android/finsky/fragments/PageFragment;->getFragmentManager()Landroid/support/v4/app/FragmentManager;

    move-result-object v2

    const-string v4, "DetailsSummaryViewBinder.confirm_cancel_dialog"

    invoke-virtual {v2, v4}, Landroid/support/v4/app/FragmentManager;->findFragmentByTag(Ljava/lang/String;)Landroid/support/v4/app/Fragment;

    move-result-object v4

    if-eqz v4, :cond_0

    :goto_0
    return-void

    :cond_0
    iget-object v4, p0, Lcom/google/android/finsky/activities/DetailsSummaryViewBinder$2;->this$0:Lcom/google/android/finsky/activities/DetailsSummaryViewBinder;

    # getter for: Lcom/google/android/finsky/activities/DetailsSummaryViewBinder;->mAnalytics:Lcom/google/android/finsky/analytics/Analytics;
    invoke-static {v4}, Lcom/google/android/finsky/activities/DetailsSummaryViewBinder;->access$000(Lcom/google/android/finsky/activities/DetailsSummaryViewBinder;)Lcom/google/android/finsky/analytics/Analytics;

    move-result-object v4

    new-instance v5, Ljava/lang/StringBuilder;

    invoke-direct {v5}, Ljava/lang/StringBuilder;-><init>()V

    const-string v6, "preorder.cancelStarted?doc="

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    iget-object v6, p0, Lcom/google/android/finsky/activities/DetailsSummaryViewBinder$2;->val$doc:Lcom/google/android/finsky/api/model/Document;

    invoke-virtual {v6}, Lcom/google/android/finsky/api/model/Document;->getDocId()Ljava/lang/String;

    move-result-object v6

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    invoke-interface {v4, v7, v7, v5}, Lcom/google/android/finsky/analytics/Analytics;->logPageView(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    iget-object v4, p0, Lcom/google/android/finsky/activities/DetailsSummaryViewBinder$2;->this$0:Lcom/google/android/finsky/activities/DetailsSummaryViewBinder;

    iget-object v4, v4, Lcom/google/android/finsky/activities/DetailsSummaryViewBinder;->mEventLogger:Lcom/google/android/finsky/analytics/FinskyEventLog;

    const/16 v5, 0xeb

    iget-object v6, p0, Lcom/google/android/finsky/activities/DetailsSummaryViewBinder$2;->this$0:Lcom/google/android/finsky/activities/DetailsSummaryViewBinder;

    iget-object v6, v6, Lcom/google/android/finsky/activities/DetailsSummaryViewBinder;->mParentNode:Lcom/google/android/finsky/layout/play/PlayStoreUiElementNode;

    invoke-virtual {v4, v5, v7, v6}, Lcom/google/android/finsky/analytics/FinskyEventLog;->logClickEvent(ILcom/google/protobuf/micro/ByteStringMicro;Lcom/google/android/finsky/layout/play/PlayStoreUiElementNode;)V

    iget-object v4, p0, Lcom/google/android/finsky/activities/DetailsSummaryViewBinder$2;->this$0:Lcom/google/android/finsky/activities/DetailsSummaryViewBinder;

    iget-object v4, v4, Lcom/google/android/finsky/activities/DetailsSummaryViewBinder;->mContext:Landroid/content/Context;

    invoke-virtual {v4}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v4

    const v5, 0x7f070176    # com.android.vending.R.string.confirm_preorder_cancel

    const/4 v6, 0x1

    new-array v6, v6, [Ljava/lang/Object;

    const/4 v7, 0x0

    iget-object v8, p0, Lcom/google/android/finsky/activities/DetailsSummaryViewBinder$2;->val$doc:Lcom/google/android/finsky/api/model/Document;

    invoke-virtual {v8}, Lcom/google/android/finsky/api/model/Document;->getTitle()Ljava/lang/String;

    move-result-object v8

    aput-object v8, v6, v7

    invoke-virtual {v4, v5, v6}, Landroid/content/res/Resources;->getString(I[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v3

    const v4, 0x7f0700fd    # com.android.vending.R.string.yes

    const v5, 0x7f0700fe    # com.android.vending.R.string.no

    invoke-static {v3, v4, v5}, Lcom/google/android/finsky/activities/SimpleAlertDialog;->newInstance(Ljava/lang/String;II)Lcom/google/android/finsky/activities/SimpleAlertDialog;

    move-result-object v0

    new-instance v1, Landroid/os/Bundle;

    invoke-direct {v1}, Landroid/os/Bundle;-><init>()V

    const-string v4, "DetailsSummaryViewBinder.doc"

    iget-object v5, p0, Lcom/google/android/finsky/activities/DetailsSummaryViewBinder$2;->val$doc:Lcom/google/android/finsky/api/model/Document;

    invoke-virtual {v1, v4, v5}, Landroid/os/Bundle;->putParcelable(Ljava/lang/String;Landroid/os/Parcelable;)V

    const-string v4, "DetailsSummaryViewBinder.ownerAccountName"

    iget-object v5, p0, Lcom/google/android/finsky/activities/DetailsSummaryViewBinder$2;->val$owner:Landroid/accounts/Account;

    iget-object v5, v5, Landroid/accounts/Account;->name:Ljava/lang/String;

    invoke-virtual {v1, v4, v5}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V

    iget-object v4, p0, Lcom/google/android/finsky/activities/DetailsSummaryViewBinder$2;->this$0:Lcom/google/android/finsky/activities/DetailsSummaryViewBinder;

    iget-object v4, v4, Lcom/google/android/finsky/activities/DetailsSummaryViewBinder;->mContainerFragment:Lcom/google/android/finsky/fragments/PageFragment;

    const/4 v5, 0x7

    invoke-virtual {v0, v4, v5, v1}, Lcom/google/android/finsky/activities/SimpleAlertDialog;->setCallback(Landroid/support/v4/app/Fragment;ILandroid/os/Bundle;)Lcom/google/android/finsky/activities/SimpleAlertDialog;

    const/16 v4, 0x131

    iget-object v5, p0, Lcom/google/android/finsky/activities/DetailsSummaryViewBinder$2;->val$doc:Lcom/google/android/finsky/api/model/Document;

    invoke-virtual {v5}, Lcom/google/android/finsky/api/model/Document;->getServerLogsCookie()Lcom/google/protobuf/micro/ByteStringMicro;

    move-result-object v5

    const/16 v6, 0xf5

    const/16 v7, 0xf6

    invoke-virtual {v0, v4, v5, v6, v7}, Lcom/google/android/finsky/activities/SimpleAlertDialog;->setEventLog(ILcom/google/protobuf/micro/ByteStringMicro;II)Lcom/google/android/finsky/activities/SimpleAlertDialog;

    const-string v4, "DetailsSummaryViewBinder.confirm_cancel_dialog"

    invoke-virtual {v0, v2, v4}, Lcom/google/android/finsky/activities/SimpleAlertDialog;->show(Landroid/support/v4/app/FragmentManager;Ljava/lang/String;)V

    goto/16 :goto_0
.end method
