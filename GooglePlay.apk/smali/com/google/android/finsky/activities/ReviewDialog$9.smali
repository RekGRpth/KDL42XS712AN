.class Lcom/google/android/finsky/activities/ReviewDialog$9;
.super Ljava/lang/Object;
.source "ReviewDialog.java"

# interfaces
.implements Lcom/google/android/gms/common/GooglePlayServicesClient$ConnectionCallbacks;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/google/android/finsky/activities/ReviewDialog;->launchGooglePlusSignup()V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/google/android/finsky/activities/ReviewDialog;


# direct methods
.method constructor <init>(Lcom/google/android/finsky/activities/ReviewDialog;)V
    .locals 0

    iput-object p1, p0, Lcom/google/android/finsky/activities/ReviewDialog$9;->this$0:Lcom/google/android/finsky/activities/ReviewDialog;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onConnected()V
    .locals 2

    const-string v0, "Got unexpected PlusClient connection."

    const/4 v1, 0x0

    new-array v1, v1, [Ljava/lang/Object;

    invoke-static {v0, v1}, Lcom/google/android/finsky/utils/FinskyLog;->w(Ljava/lang/String;[Ljava/lang/Object;)V

    iget-object v0, p0, Lcom/google/android/finsky/activities/ReviewDialog$9;->this$0:Lcom/google/android/finsky/activities/ReviewDialog;

    # getter for: Lcom/google/android/finsky/activities/ReviewDialog;->mClient:Lcom/google/android/gms/plus/PlusClient;
    invoke-static {v0}, Lcom/google/android/finsky/activities/ReviewDialog;->access$1800(Lcom/google/android/finsky/activities/ReviewDialog;)Lcom/google/android/gms/plus/PlusClient;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/android/gms/plus/PlusClient;->disconnect()V

    iget-object v0, p0, Lcom/google/android/finsky/activities/ReviewDialog$9;->this$0:Lcom/google/android/finsky/activities/ReviewDialog;

    # invokes: Lcom/google/android/finsky/activities/ReviewDialog;->showErrorToast()V
    invoke-static {v0}, Lcom/google/android/finsky/activities/ReviewDialog;->access$1600(Lcom/google/android/finsky/activities/ReviewDialog;)V

    iget-object v0, p0, Lcom/google/android/finsky/activities/ReviewDialog$9;->this$0:Lcom/google/android/finsky/activities/ReviewDialog;

    invoke-virtual {v0}, Lcom/google/android/finsky/activities/ReviewDialog;->dismiss()V

    return-void
.end method

.method public onDisconnected()V
    .locals 0

    return-void
.end method
