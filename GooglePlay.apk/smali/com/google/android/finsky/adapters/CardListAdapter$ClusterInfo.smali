.class Lcom/google/android/finsky/adapters/CardListAdapter$ClusterInfo;
.super Ljava/lang/Object;
.source "CardListAdapter.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/google/android/finsky/adapters/CardListAdapter;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0xa
    name = "ClusterInfo"
.end annotation


# instance fields
.field private final clusterMetadata:Lcom/google/android/finsky/layout/play/PlayCardClusterMetadata;

.field private final clusterType:I

.field private final respectChildHeight:Z


# direct methods
.method public constructor <init>(ILcom/google/android/finsky/layout/play/PlayCardClusterMetadata;Z)V
    .locals 0
    .param p1    # I
    .param p2    # Lcom/google/android/finsky/layout/play/PlayCardClusterMetadata;
    .param p3    # Z

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    iput p1, p0, Lcom/google/android/finsky/adapters/CardListAdapter$ClusterInfo;->clusterType:I

    iput-object p2, p0, Lcom/google/android/finsky/adapters/CardListAdapter$ClusterInfo;->clusterMetadata:Lcom/google/android/finsky/layout/play/PlayCardClusterMetadata;

    iput-boolean p3, p0, Lcom/google/android/finsky/adapters/CardListAdapter$ClusterInfo;->respectChildHeight:Z

    return-void
.end method


# virtual methods
.method public getClusterMetadata()Lcom/google/android/finsky/layout/play/PlayCardClusterMetadata;
    .locals 1

    iget-object v0, p0, Lcom/google/android/finsky/adapters/CardListAdapter$ClusterInfo;->clusterMetadata:Lcom/google/android/finsky/layout/play/PlayCardClusterMetadata;

    return-object v0
.end method

.method public getClusterType()I
    .locals 1

    iget v0, p0, Lcom/google/android/finsky/adapters/CardListAdapter$ClusterInfo;->clusterType:I

    return v0
.end method

.method public shouldRespectChildHeight()Z
    .locals 1

    iget-boolean v0, p0, Lcom/google/android/finsky/adapters/CardListAdapter$ClusterInfo;->respectChildHeight:Z

    return v0
.end method
