.class Lcom/google/android/finsky/adapters/CardListAdapter$2;
.super Ljava/lang/Object;
.source "CardListAdapter.java"

# interfaces
.implements Landroid/widget/AdapterView$OnItemSelectedListener;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/google/android/finsky/adapters/CardListAdapter;->getContainerFilterView(Landroid/view/View;Landroid/view/ViewGroup;)Landroid/view/View;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/google/android/finsky/adapters/CardListAdapter;

.field final synthetic val$containerViews:Ljava/util/List;

.field final synthetic val$spinner:Landroid/widget/Spinner;


# direct methods
.method constructor <init>(Lcom/google/android/finsky/adapters/CardListAdapter;Landroid/widget/Spinner;Ljava/util/List;)V
    .locals 0

    iput-object p1, p0, Lcom/google/android/finsky/adapters/CardListAdapter$2;->this$0:Lcom/google/android/finsky/adapters/CardListAdapter;

    iput-object p2, p0, Lcom/google/android/finsky/adapters/CardListAdapter$2;->val$spinner:Landroid/widget/Spinner;

    iput-object p3, p0, Lcom/google/android/finsky/adapters/CardListAdapter$2;->val$containerViews:Ljava/util/List;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onItemSelected(Landroid/widget/AdapterView;Landroid/view/View;IJ)V
    .locals 7
    .param p2    # Landroid/view/View;
    .param p3    # I
    .param p4    # J
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Landroid/widget/AdapterView",
            "<*>;",
            "Landroid/view/View;",
            "IJ)V"
        }
    .end annotation

    iget-object v3, p0, Lcom/google/android/finsky/adapters/CardListAdapter$2;->val$spinner:Landroid/widget/Spinner;

    invoke-virtual {v3}, Landroid/widget/Spinner;->getAdapter()Landroid/widget/SpinnerAdapter;

    move-result-object v3

    invoke-interface {v3, p3}, Landroid/widget/SpinnerAdapter;->getItem(I)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lcom/google/android/finsky/protos/Containers$ContainerView;

    iget-object v3, p0, Lcom/google/android/finsky/adapters/CardListAdapter$2;->val$spinner:Landroid/widget/Spinner;

    invoke-virtual {v3}, Landroid/widget/Spinner;->getVisibility()I

    move-result v3

    if-nez v3, :cond_2

    invoke-virtual {v2}, Lcom/google/android/finsky/protos/Containers$ContainerView;->getSelected()Z

    move-result v3

    if-nez v3, :cond_2

    invoke-static {}, Lcom/google/android/finsky/FinskyApp;->get()Lcom/google/android/finsky/FinskyApp;

    move-result-object v3

    invoke-virtual {v3}, Lcom/google/android/finsky/FinskyApp;->getEventLogger()Lcom/google/android/finsky/analytics/FinskyEventLog;

    move-result-object v3

    const/16 v4, 0xf9

    invoke-virtual {v2}, Lcom/google/android/finsky/protos/Containers$ContainerView;->getServerLogsCookie()Lcom/google/protobuf/micro/ByteStringMicro;

    move-result-object v5

    iget-object v6, p0, Lcom/google/android/finsky/adapters/CardListAdapter$2;->this$0:Lcom/google/android/finsky/adapters/CardListAdapter;

    iget-object v6, v6, Lcom/google/android/finsky/adapters/CardListAdapter;->mParentNode:Lcom/google/android/finsky/layout/play/PlayStoreUiElementNode;

    invoke-virtual {v3, v4, v5, v6}, Lcom/google/android/finsky/analytics/FinskyEventLog;->logClickEvent(ILcom/google/protobuf/micro/ByteStringMicro;Lcom/google/android/finsky/layout/play/PlayStoreUiElementNode;)V

    iget-object v3, p0, Lcom/google/android/finsky/adapters/CardListAdapter$2;->this$0:Lcom/google/android/finsky/adapters/CardListAdapter;

    iget-object v3, v3, Lcom/google/android/finsky/adapters/CardListAdapter;->mParentNode:Lcom/google/android/finsky/layout/play/PlayStoreUiElementNode;

    invoke-static {v3}, Lcom/google/android/finsky/analytics/FinskyEventLog;->startNewImpression(Lcom/google/android/finsky/layout/play/PlayStoreUiElementNode;)V

    iget-object v3, p0, Lcom/google/android/finsky/adapters/CardListAdapter$2;->val$containerViews:Ljava/util/List;

    invoke-interface {v3}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :goto_0
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v3

    if-eqz v3, :cond_1

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/finsky/protos/Containers$ContainerView;

    if-ne v0, v2, :cond_0

    const/4 v3, 0x1

    :goto_1
    invoke-virtual {v0, v3}, Lcom/google/android/finsky/protos/Containers$ContainerView;->setSelected(Z)Lcom/google/android/finsky/protos/Containers$ContainerView;

    goto :goto_0

    :cond_0
    const/4 v3, 0x0

    goto :goto_1

    :cond_1
    iget-object v3, p0, Lcom/google/android/finsky/adapters/CardListAdapter$2;->this$0:Lcom/google/android/finsky/adapters/CardListAdapter;

    iget-object v3, v3, Lcom/google/android/finsky/adapters/CardListAdapter;->mItems:Ljava/util/ArrayList;

    invoke-virtual {v3}, Ljava/util/ArrayList;->clear()V

    iget-object v3, p0, Lcom/google/android/finsky/adapters/CardListAdapter$2;->this$0:Lcom/google/android/finsky/adapters/CardListAdapter;

    iget-object v3, v3, Lcom/google/android/finsky/adapters/CardListAdapter;->mBucketedList:Lcom/google/android/finsky/api/model/BucketedList;

    invoke-virtual {v2}, Lcom/google/android/finsky/protos/Containers$ContainerView;->getListUrl()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v3, v4}, Lcom/google/android/finsky/api/model/BucketedList;->clearDataAndReplaceInitialUrl(Ljava/lang/String;)V

    iget-object v3, p0, Lcom/google/android/finsky/adapters/CardListAdapter$2;->this$0:Lcom/google/android/finsky/adapters/CardListAdapter;

    iget-object v3, v3, Lcom/google/android/finsky/adapters/CardListAdapter;->mBucketedList:Lcom/google/android/finsky/api/model/BucketedList;

    invoke-virtual {v3}, Lcom/google/android/finsky/api/model/BucketedList;->startLoadItems()V

    :cond_2
    return-void
.end method

.method public onNothingSelected(Landroid/widget/AdapterView;)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Landroid/widget/AdapterView",
            "<*>;)V"
        }
    .end annotation

    return-void
.end method
