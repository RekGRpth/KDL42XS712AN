.class final Lcom/google/android/finsky/billing/carrierbilling/Dcb3Instrument$1;
.super Lcom/google/android/finsky/billing/carrierbilling/CarrierBillingFop;
.source "Dcb3Instrument.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/google/android/finsky/billing/carrierbilling/Dcb3Instrument;->registerWithFactory(Lcom/google/android/finsky/billing/InstrumentFactory;)V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x8
    name = null
.end annotation


# direct methods
.method constructor <init>()V
    .locals 0

    invoke-direct {p0}, Lcom/google/android/finsky/billing/carrierbilling/CarrierBillingFop;-><init>()V

    return-void
.end method


# virtual methods
.method public create(Lcom/google/android/finsky/billing/BillingFlowContext;Lcom/google/android/finsky/billing/BillingFlowListener;Landroid/os/Bundle;)Lcom/google/android/finsky/billing/BillingFlow;
    .locals 1
    .param p1    # Lcom/google/android/finsky/billing/BillingFlowContext;
    .param p2    # Lcom/google/android/finsky/billing/BillingFlowListener;
    .param p3    # Landroid/os/Bundle;

    new-instance v0, Ljava/lang/UnsupportedOperationException;

    invoke-direct {v0}, Ljava/lang/UnsupportedOperationException;-><init>()V

    throw v0
.end method

.method public getPrepareOrBillingProfileParams(Z)Ljava/util/Map;
    .locals 1
    .param p1    # Z
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(Z)",
            "Ljava/util/Map",
            "<",
            "Ljava/lang/String;",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation

    # invokes: Lcom/google/android/finsky/billing/carrierbilling/Dcb3Instrument;->getDcb3UserIdentifierParams(Z)Ljava/util/Map;
    invoke-static {p1}, Lcom/google/android/finsky/billing/carrierbilling/Dcb3Instrument;->access$000(Z)Ljava/util/Map;

    move-result-object v0

    return-object v0
.end method
