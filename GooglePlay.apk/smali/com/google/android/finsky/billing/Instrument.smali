.class public abstract Lcom/google/android/finsky/billing/Instrument;
.super Ljava/lang/Object;
.source "Instrument.java"


# instance fields
.field private mCheckoutOption:Lcom/google/android/finsky/protos/Buy$BuyResponse$CheckoutInfo$CheckoutOption;


# virtual methods
.method public getDisplayName()Ljava/lang/String;
    .locals 1

    iget-object v0, p0, Lcom/google/android/finsky/billing/Instrument;->mCheckoutOption:Lcom/google/android/finsky/protos/Buy$BuyResponse$CheckoutInfo$CheckoutOption;

    invoke-virtual {v0}, Lcom/google/android/finsky/protos/Buy$BuyResponse$CheckoutInfo$CheckoutOption;->getFormOfPayment()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public toString()Ljava/lang/String;
    .locals 1

    invoke-virtual {p0}, Lcom/google/android/finsky/billing/Instrument;->getDisplayName()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method
