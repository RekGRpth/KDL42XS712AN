.class public Lcom/google/android/finsky/billing/giftcard/RedeemGiftCardFragment;
.super Landroid/support/v4/app/Fragment;
.source "RedeemGiftCardFragment.java"

# interfaces
.implements Lcom/google/android/finsky/activities/SimpleAlertDialog$Listener;


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/google/android/finsky/billing/giftcard/RedeemGiftCardFragment$Listener;
    }
.end annotation


# static fields
.field private static final IGNORED_CHARS_PATTERN:Ljava/util/regex/Pattern;


# instance fields
.field private mAddressChallenge:Lcom/google/android/finsky/protos/ChallengeProtos$AddressChallenge;

.field private mBalance:Ljava/lang/String;

.field private mBalanceView:Landroid/widget/TextView;

.field private mButton:Lcom/google/android/finsky/layout/play/PlayActionButton;

.field private mCodeEntry:Landroid/widget/EditText;

.field private mContentView:Landroid/view/View;

.field private mFootersContainer:Landroid/view/ViewGroup;

.field private mFootersHtml:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation
.end field

.field private mListener:Lcom/google/android/finsky/billing/giftcard/RedeemGiftCardFragment$Listener;

.field private mLoadingIndicator:Landroid/view/View;

.field private mProgressBar:Landroid/view/View;

.field private mShowErrorFinish:Z

.field private mShowErrorMessage:Ljava/lang/String;

.field private mSwitchToContinueForm:Z

.field private mSwitchToLoading:Z

.field private mSwitchToProgress:Z

.field private mSwitchToRedeemForm:Z

.field private mUserMessage:Ljava/lang/String;

.field private mUserMessageView:Landroid/widget/TextView;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    const-string v0, "[\\s-]"

    invoke-static {v0}, Ljava/util/regex/Pattern;->compile(Ljava/lang/String;)Ljava/util/regex/Pattern;

    move-result-object v0

    sput-object v0, Lcom/google/android/finsky/billing/giftcard/RedeemGiftCardFragment;->IGNORED_CHARS_PATTERN:Ljava/util/regex/Pattern;

    return-void
.end method

.method public constructor <init>()V
    .locals 0

    invoke-direct {p0}, Landroid/support/v4/app/Fragment;-><init>()V

    return-void
.end method

.method static synthetic access$000(Lcom/google/android/finsky/billing/giftcard/RedeemGiftCardFragment;)V
    .locals 0
    .param p0    # Lcom/google/android/finsky/billing/giftcard/RedeemGiftCardFragment;

    invoke-direct {p0}, Lcom/google/android/finsky/billing/giftcard/RedeemGiftCardFragment;->redeem()V

    return-void
.end method

.method static synthetic access$100(Lcom/google/android/finsky/billing/giftcard/RedeemGiftCardFragment;)V
    .locals 0
    .param p0    # Lcom/google/android/finsky/billing/giftcard/RedeemGiftCardFragment;

    invoke-direct {p0}, Lcom/google/android/finsky/billing/giftcard/RedeemGiftCardFragment;->notifyListenerCancel()V

    return-void
.end method

.method private clearCodeError()V
    .locals 2

    iget-object v0, p0, Lcom/google/android/finsky/billing/giftcard/RedeemGiftCardFragment;->mCodeEntry:Landroid/widget/EditText;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Landroid/widget/EditText;->setError(Ljava/lang/CharSequence;)V

    return-void
.end method

.method private internalShowChallenge(Landroid/os/Bundle;)V
    .locals 6
    .param p1    # Landroid/os/Bundle;

    const/4 v5, 0x1

    new-instance v3, Lcom/google/android/finsky/protos/ChallengeProtos$Challenge;

    invoke-direct {v3}, Lcom/google/android/finsky/protos/ChallengeProtos$Challenge;-><init>()V

    iget-object v4, p0, Lcom/google/android/finsky/billing/giftcard/RedeemGiftCardFragment;->mAddressChallenge:Lcom/google/android/finsky/protos/ChallengeProtos$AddressChallenge;

    invoke-virtual {v3, v4}, Lcom/google/android/finsky/protos/ChallengeProtos$Challenge;->setAddressChallenge(Lcom/google/android/finsky/protos/ChallengeProtos$AddressChallenge;)Lcom/google/android/finsky/protos/ChallengeProtos$Challenge;

    move-result-object v0

    invoke-virtual {p0}, Lcom/google/android/finsky/billing/giftcard/RedeemGiftCardFragment;->getArguments()Landroid/os/Bundle;

    move-result-object v3

    const-string v4, "address_challenge_params"

    invoke-virtual {v3, v4}, Landroid/os/Bundle;->containsKey(Ljava/lang/String;)Z

    move-result v3

    if-eqz v3, :cond_1

    new-instance v2, Landroid/os/Bundle;

    invoke-virtual {p0}, Lcom/google/android/finsky/billing/giftcard/RedeemGiftCardFragment;->getArguments()Landroid/os/Bundle;

    move-result-object v3

    const-string v4, "address_challenge_params"

    invoke-virtual {v3, v4}, Landroid/os/Bundle;->getBundle(Ljava/lang/String;)Landroid/os/Bundle;

    move-result-object v3

    invoke-direct {v2, v3}, Landroid/os/Bundle;-><init>(Landroid/os/Bundle;)V

    :goto_0
    const-string v3, "AddressChallengeFlow.resultFormat"

    invoke-virtual {v2, v3, v5}, Landroid/os/Bundle;->putInt(Ljava/lang/String;I)V

    const-string v3, "AddressChallengeFlow.finishOnSwitchCountry"

    invoke-virtual {v2, v3, v5}, Landroid/os/Bundle;->putBoolean(Ljava/lang/String;Z)V

    if-eqz p1, :cond_0

    const-string v3, "AddressChallengeFlow.previousState"

    invoke-virtual {v2, v3, p1}, Landroid/os/Bundle;->putBundle(Ljava/lang/String;Landroid/os/Bundle;)V

    :cond_0
    const/4 v3, 0x0

    invoke-static {v3, v0, v2}, Lcom/google/android/finsky/activities/AddressChallengeDialog;->getIntent(ILcom/google/android/finsky/protos/ChallengeProtos$Challenge;Landroid/os/Bundle;)Landroid/content/Intent;

    move-result-object v1

    const/4 v3, 0x2

    invoke-virtual {p0, v1, v3}, Lcom/google/android/finsky/billing/giftcard/RedeemGiftCardFragment;->startActivityForResult(Landroid/content/Intent;I)V

    return-void

    :cond_1
    new-instance v2, Landroid/os/Bundle;

    invoke-direct {v2}, Landroid/os/Bundle;-><init>()V

    goto :goto_0
.end method

.method public static newInstance(Ljava/lang/String;)Lcom/google/android/finsky/billing/giftcard/RedeemGiftCardFragment;
    .locals 5
    .param p0    # Ljava/lang/String;

    new-instance v2, Lcom/google/android/finsky/billing/giftcard/RedeemGiftCardFragment;

    invoke-direct {v2}, Lcom/google/android/finsky/billing/giftcard/RedeemGiftCardFragment;-><init>()V

    new-instance v0, Landroid/os/Bundle;

    invoke-direct {v0}, Landroid/os/Bundle;-><init>()V

    const-string v3, "AddressChallengeFlow.finishOnSwitchCountry"

    const/4 v4, 0x1

    invoke-virtual {v0, v3, v4}, Landroid/os/Bundle;->putBoolean(Ljava/lang/String;Z)V

    new-instance v1, Landroid/os/Bundle;

    invoke-direct {v1}, Landroid/os/Bundle;-><init>()V

    const-string v3, "authAccount"

    invoke-virtual {v1, v3, p0}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V

    const-string v3, "address_challenge_params"

    invoke-virtual {v1, v3, v0}, Landroid/os/Bundle;->putBundle(Ljava/lang/String;Landroid/os/Bundle;)V

    invoke-virtual {v2, v1}, Lcom/google/android/finsky/billing/giftcard/RedeemGiftCardFragment;->setArguments(Landroid/os/Bundle;)V

    return-object v2
.end method

.method private normalizeCode(Ljava/lang/String;)Ljava/lang/String;
    .locals 3
    .param p1    # Ljava/lang/String;

    sget-object v0, Lcom/google/android/finsky/billing/giftcard/RedeemGiftCardFragment;->IGNORED_CHARS_PATTERN:Ljava/util/regex/Pattern;

    invoke-virtual {v0, p1}, Ljava/util/regex/Pattern;->matcher(Ljava/lang/CharSequence;)Ljava/util/regex/Matcher;

    move-result-object v0

    const-string v1, ""

    invoke-virtual {v0, v1}, Ljava/util/regex/Matcher;->replaceAll(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/String;->toUpperCase()Ljava/lang/String;

    move-result-object v0

    const/16 v1, 0x49

    const/16 v2, 0x31

    invoke-virtual {v0, v1, v2}, Ljava/lang/String;->replace(CC)Ljava/lang/String;

    move-result-object v0

    const/16 v1, 0x4f

    const/16 v2, 0x30

    invoke-virtual {v0, v1, v2}, Ljava/lang/String;->replace(CC)Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method private notifyListenerCancel()V
    .locals 1

    iget-object v0, p0, Lcom/google/android/finsky/billing/giftcard/RedeemGiftCardFragment;->mListener:Lcom/google/android/finsky/billing/giftcard/RedeemGiftCardFragment$Listener;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/google/android/finsky/billing/giftcard/RedeemGiftCardFragment;->mListener:Lcom/google/android/finsky/billing/giftcard/RedeemGiftCardFragment$Listener;

    invoke-interface {v0}, Lcom/google/android/finsky/billing/giftcard/RedeemGiftCardFragment$Listener;->onCancel()V

    :cond_0
    return-void
.end method

.method private notifyListenerCountrySwitch(Ljava/lang/String;Landroid/os/Bundle;)V
    .locals 1
    .param p1    # Ljava/lang/String;
    .param p2    # Landroid/os/Bundle;

    iget-object v0, p0, Lcom/google/android/finsky/billing/giftcard/RedeemGiftCardFragment;->mListener:Lcom/google/android/finsky/billing/giftcard/RedeemGiftCardFragment$Listener;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/google/android/finsky/billing/giftcard/RedeemGiftCardFragment;->mListener:Lcom/google/android/finsky/billing/giftcard/RedeemGiftCardFragment$Listener;

    invoke-interface {v0, p1, p2}, Lcom/google/android/finsky/billing/giftcard/RedeemGiftCardFragment$Listener;->onCountrySwitch(Ljava/lang/String;Landroid/os/Bundle;)V

    :cond_0
    return-void
.end method

.method private notifyListenerRedeem(Lcom/google/android/finsky/protos/BillingAddress$Address;Ljava/util/ArrayList;)V
    .locals 2
    .param p1    # Lcom/google/android/finsky/protos/BillingAddress$Address;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/google/android/finsky/protos/BillingAddress$Address;",
            "Ljava/util/ArrayList",
            "<",
            "Ljava/lang/String;",
            ">;)V"
        }
    .end annotation

    iget-object v1, p0, Lcom/google/android/finsky/billing/giftcard/RedeemGiftCardFragment;->mListener:Lcom/google/android/finsky/billing/giftcard/RedeemGiftCardFragment$Listener;

    if-eqz v1, :cond_0

    iget-object v1, p0, Lcom/google/android/finsky/billing/giftcard/RedeemGiftCardFragment;->mCodeEntry:Landroid/widget/EditText;

    invoke-virtual {v1}, Landroid/widget/EditText;->getText()Landroid/text/Editable;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-direct {p0, v1}, Lcom/google/android/finsky/billing/giftcard/RedeemGiftCardFragment;->normalizeCode(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    iget-object v1, p0, Lcom/google/android/finsky/billing/giftcard/RedeemGiftCardFragment;->mListener:Lcom/google/android/finsky/billing/giftcard/RedeemGiftCardFragment$Listener;

    invoke-interface {v1, v0, p1, p2}, Lcom/google/android/finsky/billing/giftcard/RedeemGiftCardFragment$Listener;->onRedeem(Ljava/lang/String;Lcom/google/android/finsky/protos/BillingAddress$Address;Ljava/util/ArrayList;)V

    :cond_0
    return-void
.end method

.method private redeem()V
    .locals 3

    const/4 v2, 0x0

    iget-object v1, p0, Lcom/google/android/finsky/billing/giftcard/RedeemGiftCardFragment;->mCodeEntry:Landroid/widget/EditText;

    invoke-virtual {v1}, Landroid/widget/EditText;->getText()Landroid/text/Editable;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-direct {p0, v1}, Lcom/google/android/finsky/billing/giftcard/RedeemGiftCardFragment;->normalizeCode(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Lcom/google/android/finsky/billing/giftcard/GiftCardVerifier;->verifyGiftCardCode(Ljava/lang/String;)Z

    move-result v1

    if-nez v1, :cond_0

    invoke-static {v0}, Lcom/google/android/finsky/billing/giftcard/PromoCodeVerifier;->verifyPromoCode(Ljava/lang/String;)Z

    move-result v1

    if-nez v1, :cond_0

    const v1, 0x7f070084    # com.android.vending.R.string.invalid_gift_card_code

    invoke-virtual {p0, v1}, Lcom/google/android/finsky/billing/giftcard/RedeemGiftCardFragment;->getString(I)Ljava/lang/String;

    move-result-object v1

    invoke-direct {p0, v1}, Lcom/google/android/finsky/billing/giftcard/RedeemGiftCardFragment;->showCodeError(Ljava/lang/String;)V

    :goto_0
    return-void

    :cond_0
    iget-object v1, p0, Lcom/google/android/finsky/billing/giftcard/RedeemGiftCardFragment;->mAddressChallenge:Lcom/google/android/finsky/protos/ChallengeProtos$AddressChallenge;

    if-eqz v1, :cond_1

    invoke-direct {p0}, Lcom/google/android/finsky/billing/giftcard/RedeemGiftCardFragment;->clearCodeError()V

    invoke-direct {p0, v2}, Lcom/google/android/finsky/billing/giftcard/RedeemGiftCardFragment;->internalShowChallenge(Landroid/os/Bundle;)V

    goto :goto_0

    :cond_1
    iget-object v1, p0, Lcom/google/android/finsky/billing/giftcard/RedeemGiftCardFragment;->mListener:Lcom/google/android/finsky/billing/giftcard/RedeemGiftCardFragment$Listener;

    if-eqz v1, :cond_2

    invoke-direct {p0}, Lcom/google/android/finsky/billing/giftcard/RedeemGiftCardFragment;->clearCodeError()V

    invoke-direct {p0, v2, v2}, Lcom/google/android/finsky/billing/giftcard/RedeemGiftCardFragment;->notifyListenerRedeem(Lcom/google/android/finsky/protos/BillingAddress$Address;Ljava/util/ArrayList;)V

    goto :goto_0

    :cond_2
    const-string v1, "No listener registered."

    const/4 v2, 0x0

    new-array v2, v2, [Ljava/lang/Object;

    invoke-static {v1, v2}, Lcom/google/android/finsky/utils/FinskyLog;->wtf(Ljava/lang/String;[Ljava/lang/Object;)V

    goto :goto_0
.end method

.method private showCodeError(Ljava/lang/String;)V
    .locals 1
    .param p1    # Ljava/lang/String;

    iget-object v0, p0, Lcom/google/android/finsky/billing/giftcard/RedeemGiftCardFragment;->mCodeEntry:Landroid/widget/EditText;

    invoke-virtual {v0, p1}, Landroid/widget/EditText;->setError(Ljava/lang/CharSequence;)V

    return-void
.end method

.method private turnOffProgress()V
    .locals 3

    const/4 v2, 0x1

    iget-object v0, p0, Lcom/google/android/finsky/billing/giftcard/RedeemGiftCardFragment;->mProgressBar:Landroid/view/View;

    const/16 v1, 0x8

    invoke-virtual {v0, v1}, Landroid/view/View;->setVisibility(I)V

    iget-object v0, p0, Lcom/google/android/finsky/billing/giftcard/RedeemGiftCardFragment;->mCodeEntry:Landroid/widget/EditText;

    invoke-virtual {v0, v2}, Landroid/widget/EditText;->setEnabled(Z)V

    iget-object v0, p0, Lcom/google/android/finsky/billing/giftcard/RedeemGiftCardFragment;->mCodeEntry:Landroid/widget/EditText;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Landroid/widget/EditText;->setVisibility(I)V

    iget-object v0, p0, Lcom/google/android/finsky/billing/giftcard/RedeemGiftCardFragment;->mButton:Lcom/google/android/finsky/layout/play/PlayActionButton;

    invoke-virtual {v0, v2}, Lcom/google/android/finsky/layout/play/PlayActionButton;->setEnabled(Z)V

    return-void
.end method


# virtual methods
.method public clearGiftCardCode()V
    .locals 2

    iget-object v0, p0, Lcom/google/android/finsky/billing/giftcard/RedeemGiftCardFragment;->mCodeEntry:Landroid/widget/EditText;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Landroid/widget/EditText;->setText(Ljava/lang/CharSequence;)V

    return-void
.end method

.method public onActivityResult(IILandroid/content/Intent;)V
    .locals 6
    .param p1    # I
    .param p2    # I
    .param p3    # Landroid/content/Intent;

    const/4 v5, 0x2

    if-ne p1, v5, :cond_2

    const/4 v5, -0x1

    if-ne p2, v5, :cond_0

    const/4 v5, 0x0

    iput-object v5, p0, Lcom/google/android/finsky/billing/giftcard/RedeemGiftCardFragment;->mAddressChallenge:Lcom/google/android/finsky/protos/ChallengeProtos$AddressChallenge;

    const-string v5, "challenge_response"

    invoke-virtual {p3, v5}, Landroid/content/Intent;->getBundleExtra(Ljava/lang/String;)Landroid/os/Bundle;

    move-result-object v4

    const-string v5, "AddressChallengeFlow.switchCountry"

    invoke-virtual {v4, v5}, Landroid/os/Bundle;->containsKey(Ljava/lang/String;)Z

    move-result v5

    if-eqz v5, :cond_1

    const-string v5, "AddressChallengeFlow.switchCountry"

    invoke-virtual {v4, v5}, Landroid/os/Bundle;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v2

    const-string v5, "AddressChallengeFlow.currentState"

    invoke-virtual {v4, v5}, Landroid/os/Bundle;->getBundle(Ljava/lang/String;)Landroid/os/Bundle;

    move-result-object v3

    invoke-direct {p0, v2, v3}, Lcom/google/android/finsky/billing/giftcard/RedeemGiftCardFragment;->notifyListenerCountrySwitch(Ljava/lang/String;Landroid/os/Bundle;)V

    :cond_0
    :goto_0
    return-void

    :cond_1
    const-string v5, "AddressChallengeFlow.address"

    invoke-static {v4, v5}, Lcom/google/android/finsky/utils/ParcelableProto;->getProtoFromBundle(Landroid/os/Bundle;Ljava/lang/String;)Lcom/google/protobuf/micro/MessageMicro;

    move-result-object v0

    check-cast v0, Lcom/google/android/finsky/protos/BillingAddress$Address;

    const-string v5, "AddressChallengeFlow.checkedCheckboxes"

    invoke-virtual {v4, v5}, Landroid/os/Bundle;->getStringArrayList(Ljava/lang/String;)Ljava/util/ArrayList;

    move-result-object v1

    invoke-direct {p0, v0, v1}, Lcom/google/android/finsky/billing/giftcard/RedeemGiftCardFragment;->notifyListenerRedeem(Lcom/google/android/finsky/protos/BillingAddress$Address;Ljava/util/ArrayList;)V

    goto :goto_0

    :cond_2
    invoke-super {p0, p1, p2, p3}, Landroid/support/v4/app/Fragment;->onActivityResult(IILandroid/content/Intent;)V

    goto :goto_0
.end method

.method public onCreateView(Landroid/view/LayoutInflater;Landroid/view/ViewGroup;Landroid/os/Bundle;)Landroid/view/View;
    .locals 6
    .param p1    # Landroid/view/LayoutInflater;
    .param p2    # Landroid/view/ViewGroup;
    .param p3    # Landroid/os/Bundle;

    const v4, 0x7f04010b    # com.android.vending.R.layout.redeem_code_fragment

    const/4 v5, 0x0

    invoke-virtual {p1, v4, p2, v5}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;Z)Landroid/view/View;

    move-result-object v3

    const v4, 0x7f080076    # com.android.vending.R.id.positive_button

    invoke-virtual {v3, v4}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v4

    check-cast v4, Lcom/google/android/finsky/layout/play/PlayActionButton;

    iput-object v4, p0, Lcom/google/android/finsky/billing/giftcard/RedeemGiftCardFragment;->mButton:Lcom/google/android/finsky/layout/play/PlayActionButton;

    const v4, 0x7f08020d    # com.android.vending.R.id.pin_entry

    invoke-virtual {v3, v4}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v4

    check-cast v4, Landroid/widget/EditText;

    iput-object v4, p0, Lcom/google/android/finsky/billing/giftcard/RedeemGiftCardFragment;->mCodeEntry:Landroid/widget/EditText;

    const v4, 0x7f08020c    # com.android.vending.R.id.balance

    invoke-virtual {v3, v4}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v4

    check-cast v4, Landroid/widget/TextView;

    iput-object v4, p0, Lcom/google/android/finsky/billing/giftcard/RedeemGiftCardFragment;->mBalanceView:Landroid/widget/TextView;

    const v4, 0x7f08020e    # com.android.vending.R.id.user_message

    invoke-virtual {v3, v4}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v4

    check-cast v4, Landroid/widget/TextView;

    iput-object v4, p0, Lcom/google/android/finsky/billing/giftcard/RedeemGiftCardFragment;->mUserMessageView:Landroid/widget/TextView;

    const v4, 0x7f08020a    # com.android.vending.R.id.redeem_screen

    invoke-virtual {v3, v4}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v4

    iput-object v4, p0, Lcom/google/android/finsky/billing/giftcard/RedeemGiftCardFragment;->mContentView:Landroid/view/View;

    const v4, 0x7f08006a    # com.android.vending.R.id.loading_indicator

    invoke-virtual {v3, v4}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v4

    iput-object v4, p0, Lcom/google/android/finsky/billing/giftcard/RedeemGiftCardFragment;->mLoadingIndicator:Landroid/view/View;

    const v4, 0x7f08020f    # com.android.vending.R.id.footers

    invoke-virtual {v3, v4}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v4

    check-cast v4, Landroid/view/ViewGroup;

    iput-object v4, p0, Lcom/google/android/finsky/billing/giftcard/RedeemGiftCardFragment;->mFootersContainer:Landroid/view/ViewGroup;

    const v4, 0x7f080072    # com.android.vending.R.id.progress_bar

    invoke-virtual {v3, v4}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v4

    iput-object v4, p0, Lcom/google/android/finsky/billing/giftcard/RedeemGiftCardFragment;->mProgressBar:Landroid/view/View;

    const v4, 0x7f080158    # com.android.vending.R.id.account

    invoke-virtual {v3, v4}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    invoke-virtual {p0}, Lcom/google/android/finsky/billing/giftcard/RedeemGiftCardFragment;->getArguments()Landroid/os/Bundle;

    move-result-object v4

    const-string v5, "authAccount"

    invoke-virtual {v4, v5}, Landroid/os/Bundle;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v0, v4}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    const/4 v1, 0x0

    const/4 v2, 0x0

    if-eqz p3, :cond_0

    const-string v4, "address_challenge"

    invoke-static {p3, v4}, Lcom/google/android/finsky/utils/ParcelableProto;->getProtoFromBundle(Landroid/os/Bundle;Ljava/lang/String;)Lcom/google/protobuf/micro/MessageMicro;

    move-result-object v4

    check-cast v4, Lcom/google/android/finsky/protos/ChallengeProtos$AddressChallenge;

    iput-object v4, p0, Lcom/google/android/finsky/billing/giftcard/RedeemGiftCardFragment;->mAddressChallenge:Lcom/google/android/finsky/protos/ChallengeProtos$AddressChallenge;

    const-string v4, "balance"

    invoke-virtual {p3, v4}, Landroid/os/Bundle;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    const-string v4, "user_message"

    invoke-virtual {p3, v4}, Landroid/os/Bundle;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v2

    const-string v4, "footers_html"

    invoke-virtual {p3, v4}, Landroid/os/Bundle;->containsKey(Ljava/lang/String;)Z

    move-result v4

    if-eqz v4, :cond_0

    const-string v4, "footers_html"

    invoke-virtual {p3, v4}, Landroid/os/Bundle;->getStringArrayList(Ljava/lang/String;)Ljava/util/ArrayList;

    move-result-object v4

    invoke-virtual {p0, v4}, Lcom/google/android/finsky/billing/giftcard/RedeemGiftCardFragment;->setFooters(Ljava/util/List;)V

    :cond_0
    invoke-virtual {p0, v1}, Lcom/google/android/finsky/billing/giftcard/RedeemGiftCardFragment;->setBalance(Ljava/lang/String;)V

    invoke-virtual {p0, v2}, Lcom/google/android/finsky/billing/giftcard/RedeemGiftCardFragment;->setUserMessage(Ljava/lang/String;)V

    iget-boolean v4, p0, Lcom/google/android/finsky/billing/giftcard/RedeemGiftCardFragment;->mSwitchToProgress:Z

    if-eqz v4, :cond_1

    invoke-virtual {p0}, Lcom/google/android/finsky/billing/giftcard/RedeemGiftCardFragment;->switchToProgress()V

    :cond_1
    iget-boolean v4, p0, Lcom/google/android/finsky/billing/giftcard/RedeemGiftCardFragment;->mSwitchToLoading:Z

    if-eqz v4, :cond_2

    invoke-virtual {p0}, Lcom/google/android/finsky/billing/giftcard/RedeemGiftCardFragment;->switchToLoading()V

    :cond_2
    iget-boolean v4, p0, Lcom/google/android/finsky/billing/giftcard/RedeemGiftCardFragment;->mSwitchToRedeemForm:Z

    if-eqz v4, :cond_3

    invoke-virtual {p0}, Lcom/google/android/finsky/billing/giftcard/RedeemGiftCardFragment;->switchToRedeemForm()V

    :cond_3
    iget-boolean v4, p0, Lcom/google/android/finsky/billing/giftcard/RedeemGiftCardFragment;->mSwitchToContinueForm:Z

    if-eqz v4, :cond_4

    invoke-virtual {p0}, Lcom/google/android/finsky/billing/giftcard/RedeemGiftCardFragment;->switchToContinueForm()V

    :cond_4
    return-object v3
.end method

.method public onNegativeClick(ILandroid/os/Bundle;)V
    .locals 0
    .param p1    # I
    .param p2    # Landroid/os/Bundle;

    invoke-virtual {p0, p1, p2}, Lcom/google/android/finsky/billing/giftcard/RedeemGiftCardFragment;->onPositiveClick(ILandroid/os/Bundle;)V

    return-void
.end method

.method public onPositiveClick(ILandroid/os/Bundle;)V
    .locals 1
    .param p1    # I
    .param p2    # Landroid/os/Bundle;

    const/16 v0, 0x64

    if-ne p1, v0, :cond_0

    const-string v0, "finish"

    invoke-virtual {p2, v0}, Landroid/os/Bundle;->getBoolean(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-direct {p0}, Lcom/google/android/finsky/billing/giftcard/RedeemGiftCardFragment;->notifyListenerCancel()V

    :cond_0
    return-void
.end method

.method public onResume()V
    .locals 2

    invoke-super {p0}, Landroid/support/v4/app/Fragment;->onResume()V

    iget-object v0, p0, Lcom/google/android/finsky/billing/giftcard/RedeemGiftCardFragment;->mShowErrorMessage:Ljava/lang/String;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/google/android/finsky/billing/giftcard/RedeemGiftCardFragment;->mShowErrorMessage:Ljava/lang/String;

    iget-boolean v1, p0, Lcom/google/android/finsky/billing/giftcard/RedeemGiftCardFragment;->mShowErrorFinish:Z

    invoke-virtual {p0, v0, v1}, Lcom/google/android/finsky/billing/giftcard/RedeemGiftCardFragment;->showError(Ljava/lang/String;Z)V

    :cond_0
    return-void
.end method

.method public onSaveInstanceState(Landroid/os/Bundle;)V
    .locals 3
    .param p1    # Landroid/os/Bundle;

    invoke-super {p0, p1}, Landroid/support/v4/app/Fragment;->onSaveInstanceState(Landroid/os/Bundle;)V

    iget-object v0, p0, Lcom/google/android/finsky/billing/giftcard/RedeemGiftCardFragment;->mAddressChallenge:Lcom/google/android/finsky/protos/ChallengeProtos$AddressChallenge;

    if-eqz v0, :cond_0

    const-string v0, "address_challenge"

    iget-object v1, p0, Lcom/google/android/finsky/billing/giftcard/RedeemGiftCardFragment;->mAddressChallenge:Lcom/google/android/finsky/protos/ChallengeProtos$AddressChallenge;

    invoke-static {v1}, Lcom/google/android/finsky/utils/ParcelableProto;->forProto(Lcom/google/protobuf/micro/MessageMicro;)Lcom/google/android/finsky/utils/ParcelableProto;

    move-result-object v1

    invoke-virtual {p1, v0, v1}, Landroid/os/Bundle;->putParcelable(Ljava/lang/String;Landroid/os/Parcelable;)V

    :cond_0
    iget-object v0, p0, Lcom/google/android/finsky/billing/giftcard/RedeemGiftCardFragment;->mFootersHtml:Ljava/util/List;

    if-eqz v0, :cond_1

    const-string v0, "footers_html"

    new-instance v1, Ljava/util/ArrayList;

    iget-object v2, p0, Lcom/google/android/finsky/billing/giftcard/RedeemGiftCardFragment;->mFootersHtml:Ljava/util/List;

    invoke-direct {v1, v2}, Ljava/util/ArrayList;-><init>(Ljava/util/Collection;)V

    invoke-virtual {p1, v0, v1}, Landroid/os/Bundle;->putStringArrayList(Ljava/lang/String;Ljava/util/ArrayList;)V

    :cond_1
    const-string v0, "balance"

    iget-object v1, p0, Lcom/google/android/finsky/billing/giftcard/RedeemGiftCardFragment;->mBalance:Ljava/lang/String;

    invoke-virtual {p1, v0, v1}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V

    const-string v0, "user_message"

    iget-object v1, p0, Lcom/google/android/finsky/billing/giftcard/RedeemGiftCardFragment;->mUserMessage:Ljava/lang/String;

    invoke-virtual {p1, v0, v1}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V

    return-void
.end method

.method public setBalance(Ljava/lang/String;)V
    .locals 2
    .param p1    # Ljava/lang/String;

    iput-object p1, p0, Lcom/google/android/finsky/billing/giftcard/RedeemGiftCardFragment;->mBalance:Ljava/lang/String;

    invoke-static {p1}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v0

    if-nez v0, :cond_0

    iget-object v0, p0, Lcom/google/android/finsky/billing/giftcard/RedeemGiftCardFragment;->mBalanceView:Landroid/widget/TextView;

    invoke-virtual {v0, p1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    :goto_0
    return-void

    :cond_0
    iget-object v0, p0, Lcom/google/android/finsky/billing/giftcard/RedeemGiftCardFragment;->mBalanceView:Landroid/widget/TextView;

    const v1, 0x7f070082    # com.android.vending.R.string.no_balance

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setText(I)V

    goto :goto_0
.end method

.method public setChallenge(Lcom/google/android/finsky/protos/ChallengeProtos$AddressChallenge;)V
    .locals 0
    .param p1    # Lcom/google/android/finsky/protos/ChallengeProtos$AddressChallenge;

    iput-object p1, p0, Lcom/google/android/finsky/billing/giftcard/RedeemGiftCardFragment;->mAddressChallenge:Lcom/google/android/finsky/protos/ChallengeProtos$AddressChallenge;

    return-void
.end method

.method public setFooters(Ljava/util/List;)V
    .locals 7
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List",
            "<",
            "Ljava/lang/String;",
            ">;)V"
        }
    .end annotation

    iget-object v4, p0, Lcom/google/android/finsky/billing/giftcard/RedeemGiftCardFragment;->mContentView:Landroid/view/View;

    if-eqz v4, :cond_0

    iget-object v4, p0, Lcom/google/android/finsky/billing/giftcard/RedeemGiftCardFragment;->mFootersContainer:Landroid/view/ViewGroup;

    invoke-virtual {v4}, Landroid/view/ViewGroup;->removeAllViews()V

    iput-object p1, p0, Lcom/google/android/finsky/billing/giftcard/RedeemGiftCardFragment;->mFootersHtml:Ljava/util/List;

    iget-object v4, p0, Lcom/google/android/finsky/billing/giftcard/RedeemGiftCardFragment;->mContentView:Landroid/view/View;

    invoke-virtual {v4}, Landroid/view/View;->getContext()Landroid/content/Context;

    move-result-object v4

    invoke-static {v4}, Landroid/view/LayoutInflater;->from(Landroid/content/Context;)Landroid/view/LayoutInflater;

    move-result-object v3

    invoke-interface {p1}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v2

    :goto_0
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    move-result v4

    if-eqz v4, :cond_0

    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Ljava/lang/String;

    const v4, 0x7f04010a    # com.android.vending.R.layout.redeem_code_footer

    iget-object v5, p0, Lcom/google/android/finsky/billing/giftcard/RedeemGiftCardFragment;->mFootersContainer:Landroid/view/ViewGroup;

    const/4 v6, 0x0

    invoke-virtual {v3, v4, v5, v6}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;Z)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    invoke-static {v1}, Landroid/text/Html;->fromHtml(Ljava/lang/String;)Landroid/text/Spanned;

    move-result-object v4

    invoke-virtual {v0, v4}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    invoke-static {}, Landroid/text/method/LinkMovementMethod;->getInstance()Landroid/text/method/MovementMethod;

    move-result-object v4

    invoke-virtual {v0, v4}, Landroid/widget/TextView;->setMovementMethod(Landroid/text/method/MovementMethod;)V

    invoke-virtual {v0}, Landroid/widget/TextView;->getTextColors()Landroid/content/res/ColorStateList;

    move-result-object v4

    invoke-virtual {v0, v4}, Landroid/widget/TextView;->setLinkTextColor(Landroid/content/res/ColorStateList;)V

    iget-object v4, p0, Lcom/google/android/finsky/billing/giftcard/RedeemGiftCardFragment;->mFootersContainer:Landroid/view/ViewGroup;

    invoke-virtual {v4, v0}, Landroid/view/ViewGroup;->addView(Landroid/view/View;)V

    goto :goto_0

    :cond_0
    return-void
.end method

.method public setListener(Lcom/google/android/finsky/billing/giftcard/RedeemGiftCardFragment$Listener;)V
    .locals 0
    .param p1    # Lcom/google/android/finsky/billing/giftcard/RedeemGiftCardFragment$Listener;

    iput-object p1, p0, Lcom/google/android/finsky/billing/giftcard/RedeemGiftCardFragment;->mListener:Lcom/google/android/finsky/billing/giftcard/RedeemGiftCardFragment$Listener;

    return-void
.end method

.method public setUserMessage(Ljava/lang/String;)V
    .locals 2
    .param p1    # Ljava/lang/String;

    iput-object p1, p0, Lcom/google/android/finsky/billing/giftcard/RedeemGiftCardFragment;->mUserMessage:Ljava/lang/String;

    invoke-static {p1}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v0

    if-nez v0, :cond_0

    iget-object v0, p0, Lcom/google/android/finsky/billing/giftcard/RedeemGiftCardFragment;->mUserMessageView:Landroid/widget/TextView;

    iget-object v1, p0, Lcom/google/android/finsky/billing/giftcard/RedeemGiftCardFragment;->mUserMessage:Ljava/lang/String;

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    iget-object v0, p0, Lcom/google/android/finsky/billing/giftcard/RedeemGiftCardFragment;->mUserMessageView:Landroid/widget/TextView;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setVisibility(I)V

    :goto_0
    return-void

    :cond_0
    iget-object v0, p0, Lcom/google/android/finsky/billing/giftcard/RedeemGiftCardFragment;->mUserMessageView:Landroid/widget/TextView;

    const/16 v1, 0x8

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setVisibility(I)V

    goto :goto_0
.end method

.method public showChallenge(Lcom/google/android/finsky/protos/ChallengeProtos$AddressChallenge;Landroid/os/Bundle;)V
    .locals 0
    .param p1    # Lcom/google/android/finsky/protos/ChallengeProtos$AddressChallenge;
    .param p2    # Landroid/os/Bundle;

    invoke-virtual {p0, p1}, Lcom/google/android/finsky/billing/giftcard/RedeemGiftCardFragment;->setChallenge(Lcom/google/android/finsky/protos/ChallengeProtos$AddressChallenge;)V

    invoke-direct {p0, p2}, Lcom/google/android/finsky/billing/giftcard/RedeemGiftCardFragment;->internalShowChallenge(Landroid/os/Bundle;)V

    return-void
.end method

.method public showError(Ljava/lang/String;Z)V
    .locals 4
    .param p1    # Ljava/lang/String;
    .param p2    # Z

    invoke-virtual {p0}, Lcom/google/android/finsky/billing/giftcard/RedeemGiftCardFragment;->isResumed()Z

    move-result v2

    if-nez v2, :cond_0

    iput-object p1, p0, Lcom/google/android/finsky/billing/giftcard/RedeemGiftCardFragment;->mShowErrorMessage:Ljava/lang/String;

    iput-boolean p2, p0, Lcom/google/android/finsky/billing/giftcard/RedeemGiftCardFragment;->mShowErrorFinish:Z

    :goto_0
    return-void

    :cond_0
    const/4 v2, 0x0

    iput-object v2, p0, Lcom/google/android/finsky/billing/giftcard/RedeemGiftCardFragment;->mShowErrorMessage:Ljava/lang/String;

    const v2, 0x7f0701df    # com.android.vending.R.string.ok

    const/4 v3, -0x1

    invoke-static {p1, v2, v3}, Lcom/google/android/finsky/activities/SimpleAlertDialog;->newInstance(Ljava/lang/String;II)Lcom/google/android/finsky/activities/SimpleAlertDialog;

    move-result-object v0

    new-instance v1, Landroid/os/Bundle;

    const/4 v2, 0x1

    invoke-direct {v1, v2}, Landroid/os/Bundle;-><init>(I)V

    const-string v2, "finish"

    invoke-virtual {v1, v2, p2}, Landroid/os/Bundle;->putBoolean(Ljava/lang/String;Z)V

    const/16 v2, 0x64

    invoke-virtual {v0, p0, v2, v1}, Lcom/google/android/finsky/activities/SimpleAlertDialog;->setCallback(Landroid/support/v4/app/Fragment;ILandroid/os/Bundle;)Lcom/google/android/finsky/activities/SimpleAlertDialog;

    invoke-virtual {p0}, Lcom/google/android/finsky/billing/giftcard/RedeemGiftCardFragment;->getFragmentManager()Landroid/support/v4/app/FragmentManager;

    move-result-object v2

    const-string v3, "error_dialog"

    invoke-virtual {v0, v2, v3}, Lcom/google/android/finsky/activities/SimpleAlertDialog;->show(Landroid/support/v4/app/FragmentManager;Ljava/lang/String;)V

    goto :goto_0
.end method

.method public switchToContinueForm()V
    .locals 4

    const/16 v2, 0x8

    const/4 v1, 0x0

    iget-object v0, p0, Lcom/google/android/finsky/billing/giftcard/RedeemGiftCardFragment;->mContentView:Landroid/view/View;

    if-nez v0, :cond_0

    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/google/android/finsky/billing/giftcard/RedeemGiftCardFragment;->mSwitchToContinueForm:Z

    :goto_0
    return-void

    :cond_0
    iput-boolean v1, p0, Lcom/google/android/finsky/billing/giftcard/RedeemGiftCardFragment;->mSwitchToContinueForm:Z

    invoke-direct {p0}, Lcom/google/android/finsky/billing/giftcard/RedeemGiftCardFragment;->turnOffProgress()V

    iget-object v0, p0, Lcom/google/android/finsky/billing/giftcard/RedeemGiftCardFragment;->mContentView:Landroid/view/View;

    invoke-virtual {v0, v1}, Landroid/view/View;->setVisibility(I)V

    iget-object v0, p0, Lcom/google/android/finsky/billing/giftcard/RedeemGiftCardFragment;->mLoadingIndicator:Landroid/view/View;

    invoke-virtual {v0, v2}, Landroid/view/View;->setVisibility(I)V

    iget-object v0, p0, Lcom/google/android/finsky/billing/giftcard/RedeemGiftCardFragment;->mCodeEntry:Landroid/widget/EditText;

    invoke-virtual {v0, v2}, Landroid/widget/EditText;->setVisibility(I)V

    iget-object v0, p0, Lcom/google/android/finsky/billing/giftcard/RedeemGiftCardFragment;->mButton:Lcom/google/android/finsky/layout/play/PlayActionButton;

    const/4 v1, 0x3

    const v2, 0x7f070155    # com.android.vending.R.string.continue_text

    new-instance v3, Lcom/google/android/finsky/billing/giftcard/RedeemGiftCardFragment$2;

    invoke-direct {v3, p0}, Lcom/google/android/finsky/billing/giftcard/RedeemGiftCardFragment$2;-><init>(Lcom/google/android/finsky/billing/giftcard/RedeemGiftCardFragment;)V

    invoke-virtual {v0, v1, v2, v3}, Lcom/google/android/finsky/layout/play/PlayActionButton;->configure(IILandroid/view/View$OnClickListener;)V

    goto :goto_0
.end method

.method public switchToLoading()V
    .locals 3

    const/4 v2, 0x0

    iget-object v0, p0, Lcom/google/android/finsky/billing/giftcard/RedeemGiftCardFragment;->mContentView:Landroid/view/View;

    if-nez v0, :cond_0

    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/google/android/finsky/billing/giftcard/RedeemGiftCardFragment;->mSwitchToLoading:Z

    :goto_0
    return-void

    :cond_0
    iput-boolean v2, p0, Lcom/google/android/finsky/billing/giftcard/RedeemGiftCardFragment;->mSwitchToLoading:Z

    invoke-direct {p0}, Lcom/google/android/finsky/billing/giftcard/RedeemGiftCardFragment;->turnOffProgress()V

    iget-object v0, p0, Lcom/google/android/finsky/billing/giftcard/RedeemGiftCardFragment;->mContentView:Landroid/view/View;

    const/16 v1, 0x8

    invoke-virtual {v0, v1}, Landroid/view/View;->setVisibility(I)V

    iget-object v0, p0, Lcom/google/android/finsky/billing/giftcard/RedeemGiftCardFragment;->mLoadingIndicator:Landroid/view/View;

    invoke-virtual {v0, v2}, Landroid/view/View;->setVisibility(I)V

    goto :goto_0
.end method

.method public switchToProgress()V
    .locals 2

    const/4 v1, 0x0

    iget-object v0, p0, Lcom/google/android/finsky/billing/giftcard/RedeemGiftCardFragment;->mContentView:Landroid/view/View;

    if-nez v0, :cond_0

    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/google/android/finsky/billing/giftcard/RedeemGiftCardFragment;->mSwitchToProgress:Z

    :goto_0
    return-void

    :cond_0
    iput-boolean v1, p0, Lcom/google/android/finsky/billing/giftcard/RedeemGiftCardFragment;->mSwitchToProgress:Z

    iget-object v0, p0, Lcom/google/android/finsky/billing/giftcard/RedeemGiftCardFragment;->mProgressBar:Landroid/view/View;

    invoke-virtual {v0, v1}, Landroid/view/View;->setVisibility(I)V

    iget-object v0, p0, Lcom/google/android/finsky/billing/giftcard/RedeemGiftCardFragment;->mCodeEntry:Landroid/widget/EditText;

    invoke-virtual {v0, v1}, Landroid/widget/EditText;->setEnabled(Z)V

    iget-object v0, p0, Lcom/google/android/finsky/billing/giftcard/RedeemGiftCardFragment;->mCodeEntry:Landroid/widget/EditText;

    invoke-virtual {v0, v1}, Landroid/widget/EditText;->setVisibility(I)V

    iget-object v0, p0, Lcom/google/android/finsky/billing/giftcard/RedeemGiftCardFragment;->mButton:Lcom/google/android/finsky/layout/play/PlayActionButton;

    invoke-virtual {v0, v1}, Lcom/google/android/finsky/layout/play/PlayActionButton;->setEnabled(Z)V

    goto :goto_0
.end method

.method public switchToRedeemForm()V
    .locals 4

    const/4 v2, 0x0

    iget-object v0, p0, Lcom/google/android/finsky/billing/giftcard/RedeemGiftCardFragment;->mContentView:Landroid/view/View;

    if-nez v0, :cond_0

    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/google/android/finsky/billing/giftcard/RedeemGiftCardFragment;->mSwitchToRedeemForm:Z

    :goto_0
    return-void

    :cond_0
    iput-boolean v2, p0, Lcom/google/android/finsky/billing/giftcard/RedeemGiftCardFragment;->mSwitchToRedeemForm:Z

    invoke-direct {p0}, Lcom/google/android/finsky/billing/giftcard/RedeemGiftCardFragment;->turnOffProgress()V

    iget-object v0, p0, Lcom/google/android/finsky/billing/giftcard/RedeemGiftCardFragment;->mContentView:Landroid/view/View;

    invoke-virtual {v0, v2}, Landroid/view/View;->setVisibility(I)V

    iget-object v0, p0, Lcom/google/android/finsky/billing/giftcard/RedeemGiftCardFragment;->mLoadingIndicator:Landroid/view/View;

    const/16 v1, 0x8

    invoke-virtual {v0, v1}, Landroid/view/View;->setVisibility(I)V

    iget-object v0, p0, Lcom/google/android/finsky/billing/giftcard/RedeemGiftCardFragment;->mCodeEntry:Landroid/widget/EditText;

    invoke-virtual {v0, v2}, Landroid/widget/EditText;->setVisibility(I)V

    iget-object v0, p0, Lcom/google/android/finsky/billing/giftcard/RedeemGiftCardFragment;->mCodeEntry:Landroid/widget/EditText;

    invoke-virtual {v0}, Landroid/widget/EditText;->requestFocus()Z

    iget-object v0, p0, Lcom/google/android/finsky/billing/giftcard/RedeemGiftCardFragment;->mButton:Lcom/google/android/finsky/layout/play/PlayActionButton;

    const/4 v1, 0x3

    const v2, 0x7f070080    # com.android.vending.R.string.redeem_gift_card_button

    new-instance v3, Lcom/google/android/finsky/billing/giftcard/RedeemGiftCardFragment$1;

    invoke-direct {v3, p0}, Lcom/google/android/finsky/billing/giftcard/RedeemGiftCardFragment$1;-><init>(Lcom/google/android/finsky/billing/giftcard/RedeemGiftCardFragment;)V

    invoke-virtual {v0, v1, v2, v3}, Lcom/google/android/finsky/layout/play/PlayActionButton;->configure(IILandroid/view/View$OnClickListener;)V

    goto :goto_0
.end method
