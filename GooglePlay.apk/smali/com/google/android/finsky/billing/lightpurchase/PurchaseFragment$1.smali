.class Lcom/google/android/finsky/billing/lightpurchase/PurchaseFragment$1;
.super Ljava/lang/Object;
.source "PurchaseFragment.java"

# interfaces
.implements Ljava/lang/Runnable;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/google/android/finsky/billing/lightpurchase/PurchaseFragment;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/google/android/finsky/billing/lightpurchase/PurchaseFragment;


# direct methods
.method constructor <init>(Lcom/google/android/finsky/billing/lightpurchase/PurchaseFragment;)V
    .locals 0

    iput-object p1, p0, Lcom/google/android/finsky/billing/lightpurchase/PurchaseFragment$1;->this$0:Lcom/google/android/finsky/billing/lightpurchase/PurchaseFragment;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public run()V
    .locals 2

    iget-object v0, p0, Lcom/google/android/finsky/billing/lightpurchase/PurchaseFragment$1;->this$0:Lcom/google/android/finsky/billing/lightpurchase/PurchaseFragment;

    # getter for: Lcom/google/android/finsky/billing/lightpurchase/PurchaseFragment;->mCurrentFragment:Lcom/google/android/finsky/billing/lightpurchase/steps/PurchaseStepFragment;
    invoke-static {v0}, Lcom/google/android/finsky/billing/lightpurchase/PurchaseFragment;->access$000(Lcom/google/android/finsky/billing/lightpurchase/PurchaseFragment;)Lcom/google/android/finsky/billing/lightpurchase/steps/PurchaseStepFragment;

    move-result-object v0

    if-nez v0, :cond_0

    const-string v0, "Current fragment null."

    const/4 v1, 0x0

    new-array v1, v1, [Ljava/lang/Object;

    invoke-static {v0, v1}, Lcom/google/android/finsky/utils/FinskyLog;->w(Ljava/lang/String;[Ljava/lang/Object;)V

    :goto_0
    return-void

    :cond_0
    iget-object v0, p0, Lcom/google/android/finsky/billing/lightpurchase/PurchaseFragment$1;->this$0:Lcom/google/android/finsky/billing/lightpurchase/PurchaseFragment;

    # invokes: Lcom/google/android/finsky/billing/lightpurchase/PurchaseFragment;->fadeOutProgressBar()V
    invoke-static {v0}, Lcom/google/android/finsky/billing/lightpurchase/PurchaseFragment;->access$100(Lcom/google/android/finsky/billing/lightpurchase/PurchaseFragment;)V

    iget-object v0, p0, Lcom/google/android/finsky/billing/lightpurchase/PurchaseFragment$1;->this$0:Lcom/google/android/finsky/billing/lightpurchase/PurchaseFragment;

    # invokes: Lcom/google/android/finsky/billing/lightpurchase/PurchaseFragment;->fadeInFragment()V
    invoke-static {v0}, Lcom/google/android/finsky/billing/lightpurchase/PurchaseFragment;->access$200(Lcom/google/android/finsky/billing/lightpurchase/PurchaseFragment;)V

    iget-object v0, p0, Lcom/google/android/finsky/billing/lightpurchase/PurchaseFragment$1;->this$0:Lcom/google/android/finsky/billing/lightpurchase/PurchaseFragment;

    iget-object v1, p0, Lcom/google/android/finsky/billing/lightpurchase/PurchaseFragment$1;->this$0:Lcom/google/android/finsky/billing/lightpurchase/PurchaseFragment;

    # getter for: Lcom/google/android/finsky/billing/lightpurchase/PurchaseFragment;->mCurrentFragment:Lcom/google/android/finsky/billing/lightpurchase/steps/PurchaseStepFragment;
    invoke-static {v1}, Lcom/google/android/finsky/billing/lightpurchase/PurchaseFragment;->access$000(Lcom/google/android/finsky/billing/lightpurchase/PurchaseFragment;)Lcom/google/android/finsky/billing/lightpurchase/steps/PurchaseStepFragment;

    move-result-object v1

    # invokes: Lcom/google/android/finsky/billing/lightpurchase/PurchaseFragment;->syncContinueButton(Lcom/google/android/finsky/billing/lightpurchase/steps/PurchaseStepFragment;)V
    invoke-static {v0, v1}, Lcom/google/android/finsky/billing/lightpurchase/PurchaseFragment;->access$300(Lcom/google/android/finsky/billing/lightpurchase/PurchaseFragment;Lcom/google/android/finsky/billing/lightpurchase/steps/PurchaseStepFragment;)V

    goto :goto_0
.end method
