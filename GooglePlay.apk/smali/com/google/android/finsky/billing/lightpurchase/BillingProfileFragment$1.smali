.class Lcom/google/android/finsky/billing/lightpurchase/BillingProfileFragment$1;
.super Ljava/lang/Object;
.source "BillingProfileFragment.java"

# interfaces
.implements Ljava/lang/Runnable;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/google/android/finsky/billing/lightpurchase/BillingProfileFragment;->onResume()V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/google/android/finsky/billing/lightpurchase/BillingProfileFragment;


# direct methods
.method constructor <init>(Lcom/google/android/finsky/billing/lightpurchase/BillingProfileFragment;)V
    .locals 0

    iput-object p1, p0, Lcom/google/android/finsky/billing/lightpurchase/BillingProfileFragment$1;->this$0:Lcom/google/android/finsky/billing/lightpurchase/BillingProfileFragment;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public run()V
    .locals 2

    iget-object v0, p0, Lcom/google/android/finsky/billing/lightpurchase/BillingProfileFragment$1;->this$0:Lcom/google/android/finsky/billing/lightpurchase/BillingProfileFragment;

    invoke-virtual {v0}, Lcom/google/android/finsky/billing/lightpurchase/BillingProfileFragment;->isResumed()Z

    move-result v0

    if-nez v0, :cond_0

    :goto_0
    return-void

    :cond_0
    iget-object v0, p0, Lcom/google/android/finsky/billing/lightpurchase/BillingProfileFragment$1;->this$0:Lcom/google/android/finsky/billing/lightpurchase/BillingProfileFragment;

    new-instance v1, Lcom/google/android/finsky/billing/InstrumentFactory;

    invoke-direct {v1}, Lcom/google/android/finsky/billing/InstrumentFactory;-><init>()V

    # setter for: Lcom/google/android/finsky/billing/lightpurchase/BillingProfileFragment;->mInstrumentFactory:Lcom/google/android/finsky/billing/InstrumentFactory;
    invoke-static {v0, v1}, Lcom/google/android/finsky/billing/lightpurchase/BillingProfileFragment;->access$202(Lcom/google/android/finsky/billing/lightpurchase/BillingProfileFragment;Lcom/google/android/finsky/billing/InstrumentFactory;)Lcom/google/android/finsky/billing/InstrumentFactory;

    iget-object v0, p0, Lcom/google/android/finsky/billing/lightpurchase/BillingProfileFragment$1;->this$0:Lcom/google/android/finsky/billing/lightpurchase/BillingProfileFragment;

    # getter for: Lcom/google/android/finsky/billing/lightpurchase/BillingProfileFragment;->mInstrumentFactory:Lcom/google/android/finsky/billing/InstrumentFactory;
    invoke-static {v0}, Lcom/google/android/finsky/billing/lightpurchase/BillingProfileFragment;->access$200(Lcom/google/android/finsky/billing/lightpurchase/BillingProfileFragment;)Lcom/google/android/finsky/billing/InstrumentFactory;

    move-result-object v0

    const/4 v1, 0x1

    invoke-static {v0, v1}, Lcom/google/android/finsky/billing/carrierbilling/CarrierBillingUtils;->registerDcbInstrument(Lcom/google/android/finsky/billing/InstrumentFactory;Z)V

    iget-object v0, p0, Lcom/google/android/finsky/billing/lightpurchase/BillingProfileFragment$1;->this$0:Lcom/google/android/finsky/billing/lightpurchase/BillingProfileFragment;

    # invokes: Lcom/google/android/finsky/billing/lightpurchase/BillingProfileFragment;->renderProfile()V
    invoke-static {v0}, Lcom/google/android/finsky/billing/lightpurchase/BillingProfileFragment;->access$300(Lcom/google/android/finsky/billing/lightpurchase/BillingProfileFragment;)V

    iget-object v0, p0, Lcom/google/android/finsky/billing/lightpurchase/BillingProfileFragment$1;->this$0:Lcom/google/android/finsky/billing/lightpurchase/BillingProfileFragment;

    # getter for: Lcom/google/android/finsky/billing/lightpurchase/BillingProfileFragment;->mBillingProfileSidecar:Lcom/google/android/finsky/billing/lightpurchase/billingprofile/BillingProfileSidecar;
    invoke-static {v0}, Lcom/google/android/finsky/billing/lightpurchase/BillingProfileFragment;->access$500(Lcom/google/android/finsky/billing/lightpurchase/BillingProfileFragment;)Lcom/google/android/finsky/billing/lightpurchase/billingprofile/BillingProfileSidecar;

    move-result-object v0

    iget-object v1, p0, Lcom/google/android/finsky/billing/lightpurchase/BillingProfileFragment$1;->this$0:Lcom/google/android/finsky/billing/lightpurchase/BillingProfileFragment;

    # getter for: Lcom/google/android/finsky/billing/lightpurchase/BillingProfileFragment;->mBillingProfileListener:Lcom/google/android/finsky/billing/lightpurchase/BillingProfileFragment$BillingProfileListener;
    invoke-static {v1}, Lcom/google/android/finsky/billing/lightpurchase/BillingProfileFragment;->access$400(Lcom/google/android/finsky/billing/lightpurchase/BillingProfileFragment;)Lcom/google/android/finsky/billing/lightpurchase/BillingProfileFragment$BillingProfileListener;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/google/android/finsky/billing/lightpurchase/billingprofile/BillingProfileSidecar;->setListener(Lcom/google/android/finsky/fragments/SidecarFragment$Listener;)V

    goto :goto_0
.end method
