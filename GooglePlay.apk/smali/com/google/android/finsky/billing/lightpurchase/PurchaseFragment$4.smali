.class Lcom/google/android/finsky/billing/lightpurchase/PurchaseFragment$4;
.super Ljava/lang/Object;
.source "PurchaseFragment.java"

# interfaces
.implements Landroid/view/View$OnClickListener;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/google/android/finsky/billing/lightpurchase/PurchaseFragment;->onCreateView(Landroid/view/LayoutInflater;Landroid/view/ViewGroup;Landroid/os/Bundle;)Landroid/view/View;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/google/android/finsky/billing/lightpurchase/PurchaseFragment;


# direct methods
.method constructor <init>(Lcom/google/android/finsky/billing/lightpurchase/PurchaseFragment;)V
    .locals 0

    iput-object p1, p0, Lcom/google/android/finsky/billing/lightpurchase/PurchaseFragment$4;->this$0:Lcom/google/android/finsky/billing/lightpurchase/PurchaseFragment;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onClick(Landroid/view/View;)V
    .locals 2
    .param p1    # Landroid/view/View;

    iget-object v0, p0, Lcom/google/android/finsky/billing/lightpurchase/PurchaseFragment$4;->this$0:Lcom/google/android/finsky/billing/lightpurchase/PurchaseFragment;

    # getter for: Lcom/google/android/finsky/billing/lightpurchase/PurchaseFragment;->mCurrentFragment:Lcom/google/android/finsky/billing/lightpurchase/steps/PurchaseStepFragment;
    invoke-static {v0}, Lcom/google/android/finsky/billing/lightpurchase/PurchaseFragment;->access$000(Lcom/google/android/finsky/billing/lightpurchase/PurchaseFragment;)Lcom/google/android/finsky/billing/lightpurchase/steps/PurchaseStepFragment;

    move-result-object v0

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/google/android/finsky/billing/lightpurchase/PurchaseFragment$4;->this$0:Lcom/google/android/finsky/billing/lightpurchase/PurchaseFragment;

    # getter for: Lcom/google/android/finsky/billing/lightpurchase/PurchaseFragment;->mCurrentFragment:Lcom/google/android/finsky/billing/lightpurchase/steps/PurchaseStepFragment;
    invoke-static {v0}, Lcom/google/android/finsky/billing/lightpurchase/PurchaseFragment;->access$000(Lcom/google/android/finsky/billing/lightpurchase/PurchaseFragment;)Lcom/google/android/finsky/billing/lightpurchase/steps/PurchaseStepFragment;

    move-result-object v0

    iget-object v1, p0, Lcom/google/android/finsky/billing/lightpurchase/PurchaseFragment$4;->this$0:Lcom/google/android/finsky/billing/lightpurchase/PurchaseFragment;

    # getter for: Lcom/google/android/finsky/billing/lightpurchase/PurchaseFragment;->mParams:Lcom/google/android/finsky/billing/lightpurchase/PurchaseParams;
    invoke-static {v1}, Lcom/google/android/finsky/billing/lightpurchase/PurchaseFragment;->access$700(Lcom/google/android/finsky/billing/lightpurchase/PurchaseFragment;)Lcom/google/android/finsky/billing/lightpurchase/PurchaseParams;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/google/android/finsky/billing/lightpurchase/steps/PurchaseStepFragment;->onContinueButtonClicked(Lcom/google/android/finsky/billing/lightpurchase/PurchaseParams;)V

    :cond_0
    return-void
.end method
