.class public Lcom/google/android/finsky/billing/lightpurchase/steps/SuccessStep;
.super Lcom/google/android/finsky/billing/lightpurchase/steps/PurchaseStepFragment;
.source "SuccessStep.java"


# instance fields
.field private final mFinishRunnable:Ljava/lang/Runnable;

.field private final mHandler:Landroid/os/Handler;

.field private final mUiElement:Lcom/google/android/play/analytics/PlayStore$PlayStoreUiElement;


# direct methods
.method public constructor <init>()V
    .locals 2

    invoke-direct {p0}, Lcom/google/android/finsky/billing/lightpurchase/steps/PurchaseStepFragment;-><init>()V

    new-instance v0, Lcom/google/android/finsky/billing/lightpurchase/steps/SuccessStep$1;

    invoke-direct {v0, p0}, Lcom/google/android/finsky/billing/lightpurchase/steps/SuccessStep$1;-><init>(Lcom/google/android/finsky/billing/lightpurchase/steps/SuccessStep;)V

    iput-object v0, p0, Lcom/google/android/finsky/billing/lightpurchase/steps/SuccessStep;->mFinishRunnable:Ljava/lang/Runnable;

    invoke-static {}, Lcom/google/android/play/analytics/ProtoCache;->getInstance()Lcom/google/android/play/analytics/ProtoCache;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/android/play/analytics/ProtoCache;->obtainPlayStoreUiElement()Lcom/google/android/play/analytics/PlayStore$PlayStoreUiElement;

    move-result-object v0

    const/16 v1, 0x307

    invoke-virtual {v0, v1}, Lcom/google/android/play/analytics/PlayStore$PlayStoreUiElement;->setType(I)Lcom/google/android/play/analytics/PlayStore$PlayStoreUiElement;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/finsky/billing/lightpurchase/steps/SuccessStep;->mUiElement:Lcom/google/android/play/analytics/PlayStore$PlayStoreUiElement;

    new-instance v0, Landroid/os/Handler;

    invoke-direct {v0}, Landroid/os/Handler;-><init>()V

    iput-object v0, p0, Lcom/google/android/finsky/billing/lightpurchase/steps/SuccessStep;->mHandler:Landroid/os/Handler;

    return-void
.end method


# virtual methods
.method public getContinueButtonLabel(Landroid/content/res/Resources;)Ljava/lang/String;
    .locals 1
    .param p1    # Landroid/content/res/Resources;

    const/4 v0, 0x0

    return-object v0
.end method

.method public getPlayStoreUiElement()Lcom/google/android/play/analytics/PlayStore$PlayStoreUiElement;
    .locals 1

    iget-object v0, p0, Lcom/google/android/finsky/billing/lightpurchase/steps/SuccessStep;->mUiElement:Lcom/google/android/play/analytics/PlayStore$PlayStoreUiElement;

    return-object v0
.end method

.method public onContinueButtonClicked(Lcom/google/android/finsky/billing/lightpurchase/PurchaseParams;)V
    .locals 0
    .param p1    # Lcom/google/android/finsky/billing/lightpurchase/PurchaseParams;

    return-void
.end method

.method public onCreateView(Landroid/view/LayoutInflater;Landroid/view/ViewGroup;Landroid/os/Bundle;)Landroid/view/View;
    .locals 2
    .param p1    # Landroid/view/LayoutInflater;
    .param p2    # Landroid/view/ViewGroup;
    .param p3    # Landroid/os/Bundle;

    const v0, 0x7f040094    # com.android.vending.R.layout.light_purchase_success_step

    const/4 v1, 0x0

    invoke-virtual {p1, v0, p2, v1}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;Z)Landroid/view/View;

    move-result-object v0

    return-object v0
.end method

.method public onStart()V
    .locals 5

    invoke-super {p0}, Lcom/google/android/finsky/billing/lightpurchase/steps/PurchaseStepFragment;->onStart()V

    iget-object v1, p0, Lcom/google/android/finsky/billing/lightpurchase/steps/SuccessStep;->mHandler:Landroid/os/Handler;

    iget-object v2, p0, Lcom/google/android/finsky/billing/lightpurchase/steps/SuccessStep;->mFinishRunnable:Ljava/lang/Runnable;

    sget-object v0, Lcom/google/android/finsky/config/G;->lightPurchaseAutoDismissMs:Lcom/google/android/finsky/config/GservicesValue;

    invoke-virtual {v0}, Lcom/google/android/finsky/config/GservicesValue;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/Long;

    invoke-virtual {v0}, Ljava/lang/Long;->longValue()J

    move-result-wide v3

    invoke-virtual {v1, v2, v3, v4}, Landroid/os/Handler;->postDelayed(Ljava/lang/Runnable;J)Z

    return-void
.end method

.method public onStop()V
    .locals 2

    iget-object v0, p0, Lcom/google/android/finsky/billing/lightpurchase/steps/SuccessStep;->mHandler:Landroid/os/Handler;

    iget-object v1, p0, Lcom/google/android/finsky/billing/lightpurchase/steps/SuccessStep;->mFinishRunnable:Ljava/lang/Runnable;

    invoke-virtual {v0, v1}, Landroid/os/Handler;->removeCallbacks(Ljava/lang/Runnable;)V

    invoke-super {p0}, Lcom/google/android/finsky/billing/lightpurchase/steps/PurchaseStepFragment;->onStop()V

    return-void
.end method
