.class public Lcom/google/android/finsky/billing/creditcard/SetupWizardAddCreditCardActivity;
.super Lcom/google/android/finsky/activities/InstrumentActivity;
.source "SetupWizardAddCreditCardActivity.java"


# instance fields
.field private mInitialSetup:Z


# direct methods
.method public constructor <init>()V
    .locals 1

    invoke-direct {p0}, Lcom/google/android/finsky/activities/InstrumentActivity;-><init>()V

    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/google/android/finsky/billing/creditcard/SetupWizardAddCreditCardActivity;->mInitialSetup:Z

    return-void
.end method


# virtual methods
.method public onBackPressed()V
    .locals 2

    const-string v0, "Cannot interrupt the add credit card flow in the setup wizard."

    const/4 v1, 0x0

    new-array v1, v1, [Ljava/lang/Object;

    invoke-static {v0, v1}, Lcom/google/android/finsky/utils/FinskyLog;->d(Ljava/lang/String;[Ljava/lang/Object;)V

    return-void
.end method

.method protected onCreate(Landroid/os/Bundle;)V
    .locals 6
    .param p1    # Landroid/os/Bundle;

    const/4 v5, 0x0

    invoke-virtual {p0}, Lcom/google/android/finsky/billing/creditcard/SetupWizardAddCreditCardActivity;->getIntent()Landroid/content/Intent;

    move-result-object v1

    new-instance v2, Lcom/google/android/finsky/protos/CommonDevice$Instrument;

    invoke-direct {v2}, Lcom/google/android/finsky/protos/CommonDevice$Instrument;-><init>()V

    invoke-virtual {v2, v5}, Lcom/google/android/finsky/protos/CommonDevice$Instrument;->setInstrumentFamily(I)Lcom/google/android/finsky/protos/CommonDevice$Instrument;

    move-result-object v0

    const-string v2, "billing_flow_instrument"

    invoke-static {v0}, Lcom/google/android/finsky/utils/ParcelableProto;->forProto(Lcom/google/protobuf/micro/MessageMicro;)Lcom/google/android/finsky/utils/ParcelableProto;

    move-result-object v3

    invoke-virtual {v1, v2, v3}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Landroid/os/Parcelable;)Landroid/content/Intent;

    invoke-super {p0, p1}, Lcom/google/android/finsky/activities/InstrumentActivity;->onCreate(Landroid/os/Bundle;)V

    const-string v2, "on_initial_setup"

    const/4 v3, 0x1

    invoke-virtual {v1, v2, v3}, Landroid/content/Intent;->getBooleanExtra(Ljava/lang/String;Z)Z

    move-result v2

    if-eqz v2, :cond_0

    iget-object v2, p0, Lcom/google/android/finsky/billing/creditcard/SetupWizardAddCreditCardActivity;->mMainView:Landroid/view/View;

    const/high16 v3, 0x770000

    invoke-static {v2, v3}, Lcom/google/android/finsky/utils/Compat;->viewSetSystemUiVisibility(Landroid/view/View;I)V

    :cond_0
    iget-object v2, p0, Lcom/google/android/finsky/billing/creditcard/SetupWizardAddCreditCardActivity;->mUiMode:Lcom/google/android/finsky/billing/BillingUtils$CreateInstrumentUiMode;

    sget-object v3, Lcom/google/android/finsky/billing/BillingUtils$CreateInstrumentUiMode;->PROMO_OFFER:Lcom/google/android/finsky/billing/BillingUtils$CreateInstrumentUiMode;

    if-ne v2, v3, :cond_1

    iget-object v2, p0, Lcom/google/android/finsky/billing/creditcard/SetupWizardAddCreditCardActivity;->mTitleView:Landroid/widget/TextView;

    const v3, 0x7f07003e    # com.android.vending.R.string.add_credit_card_title

    invoke-virtual {v2, v3}, Landroid/widget/TextView;->setText(I)V

    :cond_1
    invoke-virtual {p0}, Lcom/google/android/finsky/billing/creditcard/SetupWizardAddCreditCardActivity;->removeActionBar()V

    iget-object v2, p0, Lcom/google/android/finsky/billing/creditcard/SetupWizardAddCreditCardActivity;->mBillingFlowParameters:Landroid/os/Bundle;

    const-string v3, "cardholder_name"

    const-string v4, "cardholder_name"

    invoke-virtual {v1, v4}, Landroid/content/Intent;->getStringExtra(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v2, v3, v4}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V

    const-string v2, "on_initial_setup"

    invoke-virtual {v1, v2, v5}, Landroid/content/Intent;->getBooleanExtra(Ljava/lang/String;Z)Z

    move-result v2

    iput-boolean v2, p0, Lcom/google/android/finsky/billing/creditcard/SetupWizardAddCreditCardActivity;->mInitialSetup:Z

    return-void
.end method

.method protected setTitle(Ljava/lang/String;)V
    .locals 3
    .param p1    # Ljava/lang/String;

    const-string v0, "Swallowing title: %s"

    const/4 v1, 0x1

    new-array v1, v1, [Ljava/lang/Object;

    const/4 v2, 0x0

    aput-object p1, v1, v2

    invoke-static {v0, v1}, Lcom/google/android/finsky/utils/FinskyLog;->d(Ljava/lang/String;[Ljava/lang/Object;)V

    return-void
.end method

.method protected showRedeemedOfferDialog()Z
    .locals 1

    iget-boolean v0, p0, Lcom/google/android/finsky/billing/creditcard/SetupWizardAddCreditCardActivity;->mInitialSetup:Z

    if-nez v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method
