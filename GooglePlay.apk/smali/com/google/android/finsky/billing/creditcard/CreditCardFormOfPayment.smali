.class public Lcom/google/android/finsky/billing/creditcard/CreditCardFormOfPayment;
.super Lcom/google/android/finsky/billing/InstrumentFactory$FormOfPayment;
.source "CreditCardFormOfPayment.java"


# direct methods
.method public constructor <init>()V
    .locals 0

    invoke-direct {p0}, Lcom/google/android/finsky/billing/InstrumentFactory$FormOfPayment;-><init>()V

    return-void
.end method

.method public static registerWithFactory(Lcom/google/android/finsky/billing/InstrumentFactory;)V
    .locals 2
    .param p0    # Lcom/google/android/finsky/billing/InstrumentFactory;

    const/4 v0, 0x0

    new-instance v1, Lcom/google/android/finsky/billing/creditcard/CreditCardFormOfPayment;

    invoke-direct {v1}, Lcom/google/android/finsky/billing/creditcard/CreditCardFormOfPayment;-><init>()V

    invoke-virtual {p0, v0, v1}, Lcom/google/android/finsky/billing/InstrumentFactory;->registerFormOfPayment(ILcom/google/android/finsky/billing/InstrumentFactory$FormOfPayment;)V

    return-void
.end method


# virtual methods
.method public create(Lcom/google/android/finsky/billing/BillingFlowContext;Lcom/google/android/finsky/billing/BillingFlowListener;Landroid/os/Bundle;)Lcom/google/android/finsky/billing/BillingFlow;
    .locals 1
    .param p1    # Lcom/google/android/finsky/billing/BillingFlowContext;
    .param p2    # Lcom/google/android/finsky/billing/BillingFlowListener;
    .param p3    # Landroid/os/Bundle;

    const/4 v0, 0x0

    return-object v0
.end method

.method public createFragment(Landroid/os/Bundle;)Lcom/google/android/finsky/billing/BillingFlowFragment;
    .locals 5
    .param p1    # Landroid/os/Bundle;

    if-eqz p1, :cond_0

    const-string v3, "authAccount"

    invoke-virtual {p1, v3}, Landroid/os/Bundle;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    const-string v3, "cardholder_name"

    invoke-virtual {p1, v3}, Landroid/os/Bundle;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    const-string v3, "ui_mode"

    sget-object v4, Lcom/google/android/finsky/billing/BillingUtils$CreateInstrumentUiMode;->INTERNAL:Lcom/google/android/finsky/billing/BillingUtils$CreateInstrumentUiMode;

    invoke-virtual {v4}, Lcom/google/android/finsky/billing/BillingUtils$CreateInstrumentUiMode;->name()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {p1, v3, v4}, Landroid/os/Bundle;->getString(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v2

    invoke-static {v2}, Lcom/google/android/finsky/billing/BillingUtils$CreateInstrumentUiMode;->valueOf(Ljava/lang/String;)Lcom/google/android/finsky/billing/BillingUtils$CreateInstrumentUiMode;

    move-result-object v3

    invoke-static {v0, v1, v3}, Lcom/google/android/finsky/billing/creditcard/AddCreditCardFlowFragment;->newInstance(Ljava/lang/String;Ljava/lang/String;Lcom/google/android/finsky/billing/BillingUtils$CreateInstrumentUiMode;)Lcom/google/android/finsky/billing/creditcard/AddCreditCardFlowFragment;

    move-result-object v3

    :goto_0
    return-object v3

    :cond_0
    const/4 v3, 0x0

    goto :goto_0
.end method
