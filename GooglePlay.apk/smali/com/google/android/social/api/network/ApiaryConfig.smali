.class public Lcom/google/android/social/api/network/ApiaryConfig;
.super Ljava/lang/Object;
.source "ApiaryConfig.java"


# direct methods
.method public constructor <init>()V
    .locals 0

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method public static getAddPeople(Ljava/lang/String;Ljava/lang/String;)Lcom/google/android/social/api/network/ApiaryJsonHttpRequest$JsonConfiguration;
    .locals 6
    .param p0    # Ljava/lang/String;
    .param p1    # Ljava/lang/String;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/String;",
            "Ljava/lang/String;",
            ")",
            "Lcom/google/android/social/api/network/ApiaryJsonHttpRequest$JsonConfiguration",
            "<",
            "Lcom/google/android/apps/plus/json/GenericJson;",
            "Lcom/google/api/services/plus/model/Circle;",
            ">;"
        }
    .end annotation

    new-instance v0, Lcom/google/android/social/api/network/ApiaryJsonHttpRequest$JsonConfiguration;

    const-string v1, "https://www.googleapis.com"

    const-string v2, "%s/circles/%s/people?userId=%s"

    const/4 v3, 0x3

    new-array v3, v3, [Ljava/lang/Object;

    const/4 v4, 0x0

    const-string v5, "/plus/v1whitelisted"

    aput-object v5, v3, v4

    const/4 v4, 0x1

    aput-object p0, v3, v4

    const/4 v4, 0x2

    aput-object p1, v3, v4

    invoke-static {v2, v3}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v2

    const-string v3, "PUT"

    invoke-static {}, Lcom/google/api/services/plus/model/CircleJson;->getInstance()Lcom/google/api/services/plus/model/CircleJson;

    move-result-object v4

    const/4 v5, 0x0

    invoke-direct/range {v0 .. v5}, Lcom/google/android/social/api/network/ApiaryJsonHttpRequest$JsonConfiguration;-><init>(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Lcom/google/android/apps/plus/json/EsJson;Lcom/google/android/apps/plus/json/EsJson;)V

    return-object v0
.end method

.method public static getBatchConfiguration()Lcom/google/android/social/api/network/ApiaryBatchHttpRequest$BatchConfiguration;
    .locals 4

    new-instance v0, Lcom/google/android/social/api/network/ApiaryBatchHttpRequest$BatchConfiguration;

    const-string v1, "https://www.googleapis.com"

    const-string v2, "/batch"

    const-string v3, "POST"

    invoke-direct {v0, v1, v2, v3}, Lcom/google/android/social/api/network/ApiaryBatchHttpRequest$BatchConfiguration;-><init>(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    return-object v0
.end method

.method public static getIgnoreSuggestion(Ljava/lang/String;)Lcom/google/android/social/api/network/ApiaryJsonHttpRequest$JsonConfiguration;
    .locals 6
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/String;",
            ")",
            "Lcom/google/android/social/api/network/ApiaryJsonHttpRequest$JsonConfiguration",
            "<",
            "Lcom/google/android/apps/plus/json/GenericJson;",
            "Lcom/google/android/apps/plus/json/GenericJson;",
            ">;"
        }
    .end annotation

    new-instance v0, Lcom/google/android/social/api/network/ApiaryJsonHttpRequest$JsonConfiguration;

    const-string v1, "https://www.googleapis.com"

    const-string v2, "%s/people/%s/ignore"

    const/4 v3, 0x2

    new-array v3, v3, [Ljava/lang/Object;

    const/4 v4, 0x0

    const-string v5, "/plus/v1whitelisted"

    aput-object v5, v3, v4

    const/4 v4, 0x1

    aput-object p0, v3, v4

    invoke-static {v2, v3}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v2

    const-string v3, "POST"

    const/4 v4, 0x0

    const-class v5, Lcom/google/android/apps/plus/json/GenericJson;

    invoke-static {v5}, Lcom/google/android/apps/plus/json/EsJson;->getSimpleJson(Ljava/lang/Class;)Lcom/google/android/apps/plus/json/EsJson;

    move-result-object v5

    invoke-direct/range {v0 .. v5}, Lcom/google/android/social/api/network/ApiaryJsonHttpRequest$JsonConfiguration;-><init>(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Lcom/google/android/apps/plus/json/EsJson;Lcom/google/android/apps/plus/json/EsJson;)V

    return-object v0
.end method

.method public static getLoadSocialNetwork()Lcom/google/android/social/api/network/ApiaryJsonHttpRequest$JsonConfiguration;
    .locals 6
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Lcom/google/android/social/api/network/ApiaryJsonHttpRequest$JsonConfiguration",
            "<",
            "Lcom/google/android/apps/plus/json/GenericJson;",
            "Lcom/google/api/services/plus/model/AudiencesFeed;",
            ">;"
        }
    .end annotation

    new-instance v0, Lcom/google/android/social/api/network/ApiaryJsonHttpRequest$JsonConfiguration;

    const-string v1, "https://www.googleapis.com"

    const-string v2, "/plus/v1whitelisted/people/me/audiences?maxResults=100"

    const-string v3, "GET"

    invoke-static {}, Lcom/google/api/services/plus/model/AudiencesFeedJson;->getInstance()Lcom/google/api/services/plus/model/AudiencesFeedJson;

    move-result-object v4

    const/4 v5, 0x0

    invoke-direct/range {v0 .. v5}, Lcom/google/android/social/api/network/ApiaryJsonHttpRequest$JsonConfiguration;-><init>(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Lcom/google/android/apps/plus/json/EsJson;Lcom/google/android/apps/plus/json/EsJson;)V

    return-object v0
.end method

.method public static getPeopleList()Lcom/google/android/social/api/network/ApiaryJsonHttpRequest$JsonConfiguration;
    .locals 6
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Lcom/google/android/social/api/network/ApiaryJsonHttpRequest$JsonConfiguration",
            "<",
            "Lcom/google/android/apps/plus/json/GenericJson;",
            "Lcom/google/api/services/plus/model/PeopleFeed;",
            ">;"
        }
    .end annotation

    new-instance v0, Lcom/google/android/social/api/network/ApiaryJsonHttpRequest$JsonConfiguration;

    const-string v1, "https://www.googleapis.com"

    const-string v2, "/plus/v1whitelisted/people/me/people/suggestions"

    const-string v3, "GET"

    invoke-static {}, Lcom/google/api/services/plus/model/PeopleFeedJson;->getInstance()Lcom/google/api/services/plus/model/PeopleFeedJson;

    move-result-object v4

    const/4 v5, 0x0

    invoke-direct/range {v0 .. v5}, Lcom/google/android/social/api/network/ApiaryJsonHttpRequest$JsonConfiguration;-><init>(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Lcom/google/android/apps/plus/json/EsJson;Lcom/google/android/apps/plus/json/EsJson;)V

    return-object v0
.end method

.method public static getPerson(Ljava/lang/String;)Lcom/google/android/social/api/network/ApiaryJsonHttpRequest$JsonConfiguration;
    .locals 6
    .param p0    # Ljava/lang/String;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/String;",
            ")",
            "Lcom/google/android/social/api/network/ApiaryJsonHttpRequest$JsonConfiguration",
            "<",
            "Lcom/google/android/apps/plus/json/GenericJson;",
            "Lcom/google/api/services/plus/model/Person;",
            ">;"
        }
    .end annotation

    new-instance v0, Lcom/google/android/social/api/network/ApiaryJsonHttpRequest$JsonConfiguration;

    const-string v1, "https://www.googleapis.com"

    const-string v2, "%s/people/%s"

    const/4 v3, 0x2

    new-array v3, v3, [Ljava/lang/Object;

    const/4 v4, 0x0

    const-string v5, "/plus/v1whitelisted"

    aput-object v5, v3, v4

    const/4 v4, 0x1

    aput-object p0, v3, v4

    invoke-static {v2, v3}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v2

    const-string v3, "GET"

    invoke-static {}, Lcom/google/api/services/plus/model/PersonJson;->getInstance()Lcom/google/api/services/plus/model/PersonJson;

    move-result-object v4

    const/4 v5, 0x0

    invoke-direct/range {v0 .. v5}, Lcom/google/android/social/api/network/ApiaryJsonHttpRequest$JsonConfiguration;-><init>(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Lcom/google/android/apps/plus/json/EsJson;Lcom/google/android/apps/plus/json/EsJson;)V

    return-object v0
.end method

.method public static getRemovePeople(Ljava/lang/String;Ljava/lang/String;)Lcom/google/android/social/api/network/ApiaryJsonHttpRequest$JsonConfiguration;
    .locals 6
    .param p0    # Ljava/lang/String;
    .param p1    # Ljava/lang/String;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/String;",
            "Ljava/lang/String;",
            ")",
            "Lcom/google/android/social/api/network/ApiaryJsonHttpRequest$JsonConfiguration",
            "<",
            "Lcom/google/android/apps/plus/json/GenericJson;",
            "Lcom/google/api/services/plus/model/Circle;",
            ">;"
        }
    .end annotation

    new-instance v0, Lcom/google/android/social/api/network/ApiaryJsonHttpRequest$JsonConfiguration;

    const-string v1, "https://www.googleapis.com"

    const-string v2, "%s/circles/%s/people?userId=%s"

    const/4 v3, 0x3

    new-array v3, v3, [Ljava/lang/Object;

    const/4 v4, 0x0

    const-string v5, "/plus/v1whitelisted"

    aput-object v5, v3, v4

    const/4 v4, 0x1

    aput-object p0, v3, v4

    const/4 v4, 0x2

    aput-object p1, v3, v4

    invoke-static {v2, v3}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v2

    const-string v3, "DELETE"

    invoke-static {}, Lcom/google/api/services/plus/model/CircleJson;->getInstance()Lcom/google/api/services/plus/model/CircleJson;

    move-result-object v4

    const/4 v5, 0x0

    invoke-direct/range {v0 .. v5}, Lcom/google/android/social/api/network/ApiaryJsonHttpRequest$JsonConfiguration;-><init>(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Lcom/google/android/apps/plus/json/EsJson;Lcom/google/android/apps/plus/json/EsJson;)V

    return-object v0
.end method

.method public static getRpcPostInsertLog()Lcom/google/android/social/api/network/ApiaryJsonHttpRequest$JsonConfiguration;
    .locals 6
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Lcom/google/android/social/api/network/ApiaryJsonHttpRequest$JsonConfiguration",
            "<",
            "Lcom/google/api/services/plus/model/ClientOzExtension;",
            "Lcom/google/android/apps/plus/json/GenericJson;",
            ">;"
        }
    .end annotation

    new-instance v0, Lcom/google/android/social/api/network/ApiaryJsonHttpRequest$JsonConfiguration;

    const-string v1, "https://www.googleapis.com"

    const-string v2, "%s/rpc/insertLog"

    const/4 v3, 0x1

    new-array v3, v3, [Ljava/lang/Object;

    const/4 v4, 0x0

    const-string v5, "/plus/v1whitelisted"

    aput-object v5, v3, v4

    invoke-static {v2, v3}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v2

    const-string v3, "POST"

    const/4 v4, 0x0

    invoke-static {}, Lcom/google/api/services/plus/model/ClientOzExtensionJson;->getInstance()Lcom/google/api/services/plus/model/ClientOzExtensionJson;

    move-result-object v5

    invoke-direct/range {v0 .. v5}, Lcom/google/android/social/api/network/ApiaryJsonHttpRequest$JsonConfiguration;-><init>(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Lcom/google/android/apps/plus/json/EsJson;Lcom/google/android/apps/plus/json/EsJson;)V

    return-object v0
.end method

.method public static getSearchQuery(Ljava/lang/String;)Lcom/google/android/social/api/network/ApiaryJsonHttpRequest$JsonConfiguration;
    .locals 6
    .param p0    # Ljava/lang/String;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/String;",
            ")",
            "Lcom/google/android/social/api/network/ApiaryJsonHttpRequest$JsonConfiguration",
            "<",
            "Lcom/google/android/apps/plus/json/GenericJson;",
            "Lcom/google/api/services/plus/model/PeopleFeed;",
            ">;"
        }
    .end annotation

    new-instance v0, Lcom/google/android/social/api/network/ApiaryJsonHttpRequest$JsonConfiguration;

    const-string v1, "https://www.googleapis.com"

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "/plus/v1whitelisted/people?query="

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-static {p0}, Landroid/net/Uri;->encode(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    const-string v3, "GET"

    invoke-static {}, Lcom/google/api/services/plus/model/PeopleFeedJson;->getInstance()Lcom/google/api/services/plus/model/PeopleFeedJson;

    move-result-object v4

    const/4 v5, 0x0

    invoke-direct/range {v0 .. v5}, Lcom/google/android/social/api/network/ApiaryJsonHttpRequest$JsonConfiguration;-><init>(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Lcom/google/android/apps/plus/json/EsJson;Lcom/google/android/apps/plus/json/EsJson;)V

    return-object v0
.end method

.method public static getUnignoreSuggestion(Ljava/lang/String;)Lcom/google/android/social/api/network/ApiaryJsonHttpRequest$JsonConfiguration;
    .locals 6
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/String;",
            ")",
            "Lcom/google/android/social/api/network/ApiaryJsonHttpRequest$JsonConfiguration",
            "<",
            "Lcom/google/android/apps/plus/json/GenericJson;",
            "Lcom/google/android/apps/plus/json/GenericJson;",
            ">;"
        }
    .end annotation

    new-instance v0, Lcom/google/android/social/api/network/ApiaryJsonHttpRequest$JsonConfiguration;

    const-string v1, "https://www.googleapis.com"

    const-string v2, "%s/people/%s/unignore"

    const/4 v3, 0x2

    new-array v3, v3, [Ljava/lang/Object;

    const/4 v4, 0x0

    const-string v5, "/plus/v1whitelisted"

    aput-object v5, v3, v4

    const/4 v4, 0x1

    aput-object p0, v3, v4

    invoke-static {v2, v3}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v2

    const-string v3, "POST"

    const/4 v4, 0x0

    const-class v5, Lcom/google/android/apps/plus/json/GenericJson;

    invoke-static {v5}, Lcom/google/android/apps/plus/json/EsJson;->getSimpleJson(Ljava/lang/Class;)Lcom/google/android/apps/plus/json/EsJson;

    move-result-object v5

    invoke-direct/range {v0 .. v5}, Lcom/google/android/social/api/network/ApiaryJsonHttpRequest$JsonConfiguration;-><init>(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Lcom/google/android/apps/plus/json/EsJson;Lcom/google/android/apps/plus/json/EsJson;)V

    return-object v0
.end method
