.class Lcom/google/android/social/api/network/ApiaryBatchHttpRequest$VolleyBatchRequest;
.super Lcom/android/volley/Request;
.source "ApiaryBatchHttpRequest.java"

# interfaces
.implements Lcom/google/android/social/api/network/ApiaryNetworkStack$HttpRequestMethod;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/google/android/social/api/network/ApiaryBatchHttpRequest;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = "VolleyBatchRequest"
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/android/volley/Request",
        "<",
        "Ljava/util/ArrayList",
        "<TResultType;>;>;",
        "Lcom/google/android/social/api/network/ApiaryNetworkStack$HttpRequestMethod;"
    }
.end annotation


# instance fields
.field private final listener:Lcom/android/volley/Response$Listener;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/android/volley/Response$Listener",
            "<",
            "Ljava/util/ArrayList",
            "<TResultType;>;>;"
        }
    .end annotation
.end field

.field final synthetic this$0:Lcom/google/android/social/api/network/ApiaryBatchHttpRequest;


# direct methods
.method public constructor <init>(Lcom/google/android/social/api/network/ApiaryBatchHttpRequest;Lcom/google/android/social/api/network/RequestFuture;)V
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/google/android/social/api/network/RequestFuture",
            "<",
            "Ljava/util/ArrayList",
            "<TResultType;>;>;)V"
        }
    .end annotation

    iput-object p1, p0, Lcom/google/android/social/api/network/ApiaryBatchHttpRequest$VolleyBatchRequest;->this$0:Lcom/google/android/social/api/network/ApiaryBatchHttpRequest;

    invoke-virtual {p1}, Lcom/google/android/social/api/network/ApiaryBatchHttpRequest;->getConfiguration()Lcom/google/android/social/api/network/ApiaryHttpRequest$Configuration;

    move-result-object v0

    iget-object v0, v0, Lcom/google/android/social/api/network/ApiaryHttpRequest$Configuration;->url:Ljava/lang/String;

    invoke-direct {p0, v0, p2}, Lcom/android/volley/Request;-><init>(Ljava/lang/String;Lcom/android/volley/Response$ErrorListener;)V

    iput-object p2, p0, Lcom/google/android/social/api/network/ApiaryBatchHttpRequest$VolleyBatchRequest;->listener:Lcom/android/volley/Response$Listener;

    return-void
.end method

.method private writeRequest(Ljava/io/OutputStreamWriter;Ljava/util/Map;Lcom/google/android/social/api/network/ApiaryHttpRequest;I)V
    .locals 8
    .param p1    # Ljava/io/OutputStreamWriter;
    .param p4    # I
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/io/OutputStreamWriter;",
            "Ljava/util/Map",
            "<",
            "Ljava/lang/String;",
            "Ljava/lang/String;",
            ">;",
            "Lcom/google/android/social/api/network/ApiaryHttpRequest",
            "<+TResultType;>;I)V"
        }
    .end annotation

    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;,
            Lcom/android/volley/AuthFailureError;
        }
    .end annotation

    const/4 v4, 0x0

    invoke-virtual {p3, v4}, Lcom/google/android/social/api/network/ApiaryHttpRequest;->getRequest(Lcom/google/android/social/api/network/RequestFuture;)Lcom/android/volley/Request;

    move-result-object v3

    const-string v4, "Content-Type: application/http\n"

    invoke-virtual {p1, v4}, Ljava/io/OutputStreamWriter;->write(Ljava/lang/String;)V

    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    const-string v5, "Content-ID: <item:"

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4, p4}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v4

    const-string v5, ">\n"

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {p1, v4}, Ljava/io/OutputStreamWriter;->write(Ljava/lang/String;)V

    const-string v4, "\n"

    invoke-virtual {p1, v4}, Ljava/io/OutputStreamWriter;->write(Ljava/lang/String;)V

    const-string v4, "%s %s\n"

    const/4 v5, 0x2

    new-array v5, v5, [Ljava/lang/Object;

    const/4 v6, 0x0

    invoke-virtual {p3}, Lcom/google/android/social/api/network/ApiaryHttpRequest;->getConfiguration()Lcom/google/android/social/api/network/ApiaryHttpRequest$Configuration;

    move-result-object v7

    iget-object v7, v7, Lcom/google/android/social/api/network/ApiaryHttpRequest$Configuration;->method:Ljava/lang/String;

    aput-object v7, v5, v6

    const/4 v6, 0x1

    invoke-virtual {p3}, Lcom/google/android/social/api/network/ApiaryHttpRequest;->getConfiguration()Lcom/google/android/social/api/network/ApiaryHttpRequest$Configuration;

    move-result-object v7

    iget-object v7, v7, Lcom/google/android/social/api/network/ApiaryHttpRequest$Configuration;->path:Ljava/lang/String;

    aput-object v7, v5, v6

    invoke-static {v4, v5}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v4

    invoke-virtual {p1, v4}, Ljava/io/OutputStreamWriter;->write(Ljava/lang/String;)V

    invoke-virtual {v3}, Lcom/android/volley/Request;->getHeaders()Ljava/util/Map;

    move-result-object v4

    if-eqz v4, :cond_1

    invoke-virtual {v3}, Lcom/android/volley/Request;->getHeaders()Ljava/util/Map;

    move-result-object v4

    invoke-interface {v4}, Ljava/util/Map;->entrySet()Ljava/util/Set;

    move-result-object v4

    invoke-interface {v4}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    move-result-object v2

    :cond_0
    :goto_0
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    move-result v4

    if-eqz v4, :cond_1

    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Ljava/util/Map$Entry;

    invoke-interface {v1}, Ljava/util/Map$Entry;->getValue()Ljava/lang/Object;

    move-result-object v4

    check-cast v4, Ljava/lang/String;

    invoke-interface {v1}, Ljava/util/Map$Entry;->getKey()Ljava/lang/Object;

    move-result-object v5

    invoke-interface {p2, v5}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v5

    invoke-virtual {v4, v5}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v4

    if-nez v4, :cond_0

    new-instance v5, Ljava/lang/StringBuilder;

    invoke-direct {v5}, Ljava/lang/StringBuilder;-><init>()V

    invoke-interface {v1}, Ljava/util/Map$Entry;->getKey()Ljava/lang/Object;

    move-result-object v4

    check-cast v4, Ljava/lang/String;

    invoke-virtual {v5, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    const-string v5, ": "

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-interface {v1}, Ljava/util/Map$Entry;->getValue()Ljava/lang/Object;

    move-result-object v4

    check-cast v4, Ljava/lang/String;

    invoke-virtual {v5, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    const-string v5, "\n"

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {p1, v4}, Ljava/io/OutputStreamWriter;->write(Ljava/lang/String;)V

    goto :goto_0

    :cond_1
    invoke-virtual {v3}, Lcom/android/volley/Request;->getPostBody()[B

    move-result-object v0

    if-eqz v0, :cond_2

    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    const-string v5, "\n"

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    new-instance v5, Ljava/lang/String;

    invoke-direct {v5, v0}, Ljava/lang/String;-><init>([B)V

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    const-string v5, "\n"

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {p1, v4}, Ljava/io/OutputStreamWriter;->write(Ljava/lang/String;)V

    :cond_2
    const-string v4, "\n"

    invoke-virtual {p1, v4}, Ljava/io/OutputStreamWriter;->write(Ljava/lang/String;)V

    return-void
.end method


# virtual methods
.method protected bridge synthetic deliverResponse(Ljava/lang/Object;)V
    .locals 0
    .param p1    # Ljava/lang/Object;

    check-cast p1, Ljava/util/ArrayList;

    invoke-virtual {p0, p1}, Lcom/google/android/social/api/network/ApiaryBatchHttpRequest$VolleyBatchRequest;->deliverResponse(Ljava/util/ArrayList;)V

    return-void
.end method

.method protected deliverResponse(Ljava/util/ArrayList;)V
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/ArrayList",
            "<TResultType;>;)V"
        }
    .end annotation

    iget-object v0, p0, Lcom/google/android/social/api/network/ApiaryBatchHttpRequest$VolleyBatchRequest;->listener:Lcom/android/volley/Response$Listener;

    invoke-interface {v0, p1}, Lcom/android/volley/Response$Listener;->onResponse(Ljava/lang/Object;)V

    return-void
.end method

.method public getMethodNotVolley()Ljava/lang/String;
    .locals 1

    iget-object v0, p0, Lcom/google/android/social/api/network/ApiaryBatchHttpRequest$VolleyBatchRequest;->this$0:Lcom/google/android/social/api/network/ApiaryBatchHttpRequest;

    invoke-virtual {v0}, Lcom/google/android/social/api/network/ApiaryBatchHttpRequest;->getConfiguration()Lcom/google/android/social/api/network/ApiaryHttpRequest$Configuration;

    move-result-object v0

    iget-object v0, v0, Lcom/google/android/social/api/network/ApiaryHttpRequest$Configuration;->method:Ljava/lang/String;

    return-object v0
.end method

.method public getPostBody()[B
    .locals 6

    new-instance v0, Ljava/io/ByteArrayOutputStream;

    invoke-direct {v0}, Ljava/io/ByteArrayOutputStream;-><init>()V

    new-instance v4, Ljava/io/OutputStreamWriter;

    invoke-direct {v4, v0}, Ljava/io/OutputStreamWriter;-><init>(Ljava/io/OutputStream;)V

    :try_start_0
    invoke-virtual {p0}, Lcom/google/android/social/api/network/ApiaryBatchHttpRequest$VolleyBatchRequest;->getHeaders()Ljava/util/Map;

    move-result-object v3

    const/4 v2, 0x0

    :goto_0
    iget-object v5, p0, Lcom/google/android/social/api/network/ApiaryBatchHttpRequest$VolleyBatchRequest;->this$0:Lcom/google/android/social/api/network/ApiaryBatchHttpRequest;

    # getter for: Lcom/google/android/social/api/network/ApiaryBatchHttpRequest;->requests:Ljava/util/List;
    invoke-static {v5}, Lcom/google/android/social/api/network/ApiaryBatchHttpRequest;->access$000(Lcom/google/android/social/api/network/ApiaryBatchHttpRequest;)Ljava/util/List;

    move-result-object v5

    invoke-interface {v5}, Ljava/util/List;->size()I

    move-result v5

    if-ge v2, v5, :cond_0

    const-string v5, "--MultiPartRequest\n"

    invoke-virtual {v4, v5}, Ljava/io/OutputStreamWriter;->write(Ljava/lang/String;)V

    iget-object v5, p0, Lcom/google/android/social/api/network/ApiaryBatchHttpRequest$VolleyBatchRequest;->this$0:Lcom/google/android/social/api/network/ApiaryBatchHttpRequest;

    # getter for: Lcom/google/android/social/api/network/ApiaryBatchHttpRequest;->requests:Ljava/util/List;
    invoke-static {v5}, Lcom/google/android/social/api/network/ApiaryBatchHttpRequest;->access$000(Lcom/google/android/social/api/network/ApiaryBatchHttpRequest;)Ljava/util/List;

    move-result-object v5

    invoke-interface {v5, v2}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v5

    check-cast v5, Lcom/google/android/social/api/network/ApiaryHttpRequest;

    invoke-direct {p0, v4, v3, v5, v2}, Lcom/google/android/social/api/network/ApiaryBatchHttpRequest$VolleyBatchRequest;->writeRequest(Ljava/io/OutputStreamWriter;Ljava/util/Map;Lcom/google/android/social/api/network/ApiaryHttpRequest;I)V

    add-int/lit8 v2, v2, 0x1

    goto :goto_0

    :cond_0
    const-string v5, "--MultiPartRequest--\n"

    invoke-virtual {v4, v5}, Ljava/io/OutputStreamWriter;->write(Ljava/lang/String;)V

    invoke-virtual {v4}, Ljava/io/OutputStreamWriter;->flush()V
    :try_end_0
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_0
    .catch Lcom/android/volley/AuthFailureError; {:try_start_0 .. :try_end_0} :catch_1

    invoke-virtual {v0}, Ljava/io/ByteArrayOutputStream;->toByteArray()[B

    move-result-object v5

    return-object v5

    :catch_0
    move-exception v1

    new-instance v5, Ljava/lang/RuntimeException;

    invoke-direct {v5, v1}, Ljava/lang/RuntimeException;-><init>(Ljava/lang/Throwable;)V

    throw v5

    :catch_1
    move-exception v1

    new-instance v5, Ljava/lang/RuntimeException;

    invoke-direct {v5, v1}, Ljava/lang/RuntimeException;-><init>(Ljava/lang/Throwable;)V

    throw v5
.end method

.method public getPostBodyContentType()Ljava/lang/String;
    .locals 1

    const-string v0, "multipart/mixed; boundary=MultiPartRequest"

    return-object v0
.end method

.method protected parseNetworkResponse(Lcom/android/volley/NetworkResponse;)Lcom/android/volley/Response;
    .locals 2
    .param p1    # Lcom/android/volley/NetworkResponse;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/android/volley/NetworkResponse;",
            ")",
            "Lcom/android/volley/Response",
            "<",
            "Ljava/util/ArrayList",
            "<TResultType;>;>;"
        }
    .end annotation

    iget-object v0, p0, Lcom/google/android/social/api/network/ApiaryBatchHttpRequest$VolleyBatchRequest;->this$0:Lcom/google/android/social/api/network/ApiaryBatchHttpRequest;

    invoke-virtual {p0}, Lcom/google/android/social/api/network/ApiaryBatchHttpRequest$VolleyBatchRequest;->getCacheEntry()Lcom/android/volley/Cache$Entry;

    move-result-object v1

    invoke-virtual {v0, p1, v1}, Lcom/google/android/social/api/network/ApiaryBatchHttpRequest;->processResponseData(Lcom/android/volley/NetworkResponse;Lcom/android/volley/Cache$Entry;)Lcom/android/volley/Response;

    move-result-object v0

    return-object v0
.end method
