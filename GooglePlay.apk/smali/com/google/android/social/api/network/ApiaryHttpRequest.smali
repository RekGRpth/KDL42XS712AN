.class public abstract Lcom/google/android/social/api/network/ApiaryHttpRequest;
.super Ljava/lang/Object;
.source "ApiaryHttpRequest.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/google/android/social/api/network/ApiaryHttpRequest$GoogleAuthUtilHelper;,
        Lcom/google/android/social/api/network/ApiaryHttpRequest$Configuration;
    }
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "<Data:",
        "Ljava/lang/Object;",
        ">",
        "Ljava/lang/Object;"
    }
.end annotation


# static fields
.field public static final OAUTH2_SCOPES:[Ljava/lang/String;

.field static final OAUTH2_SCOPE_STRING:Ljava/lang/String;

.field private static final log:Lcom/google/android/social/api/internal/GLog;


# instance fields
.field private final accountName:Ljava/lang/String;

.field private authToken:Ljava/lang/String;

.field private final configuration:Lcom/google/android/social/api/network/ApiaryHttpRequest$Configuration;

.field private final context:Landroid/content/Context;

.field private final future:Lcom/google/android/social/api/network/RequestFuture;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/google/android/social/api/network/RequestFuture",
            "<TData;>;"
        }
    .end annotation
.end field

.field private final googleAuthUtilHelper:Lcom/google/android/social/api/network/ApiaryHttpRequest$GoogleAuthUtilHelper;


# direct methods
.method static constructor <clinit>()V
    .locals 3

    new-instance v0, Lcom/google/android/social/api/internal/GLog;

    invoke-direct {v0}, Lcom/google/android/social/api/internal/GLog;-><init>()V

    sput-object v0, Lcom/google/android/social/api/network/ApiaryHttpRequest;->log:Lcom/google/android/social/api/internal/GLog;

    const/4 v0, 0x4

    new-array v0, v0, [Ljava/lang/String;

    const/4 v1, 0x0

    const-string v2, "https://www.googleapis.com/auth/plus.me"

    aput-object v2, v0, v1

    const/4 v1, 0x1

    const-string v2, "https://www.googleapis.com/auth/plus.circles.read"

    aput-object v2, v0, v1

    const/4 v1, 0x2

    const-string v2, "https://www.googleapis.com/auth/plus.circles.write"

    aput-object v2, v0, v1

    const/4 v1, 0x3

    const-string v2, "https://www.googleapis.com/auth/plus.profiles.read"

    aput-object v2, v0, v1

    sput-object v0, Lcom/google/android/social/api/network/ApiaryHttpRequest;->OAUTH2_SCOPES:[Ljava/lang/String;

    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v1, "oauth2:"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, " "

    sget-object v2, Lcom/google/android/social/api/network/ApiaryHttpRequest;->OAUTH2_SCOPES:[Ljava/lang/String;

    invoke-static {v1, v2}, Landroid/text/TextUtils;->join(Ljava/lang/CharSequence;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    sput-object v0, Lcom/google/android/social/api/network/ApiaryHttpRequest;->OAUTH2_SCOPE_STRING:Ljava/lang/String;

    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Ljava/lang/String;Lcom/google/android/social/api/network/ApiaryHttpRequest$Configuration;Lcom/google/android/social/api/network/ApiaryHttpRequest$GoogleAuthUtilHelper;)V
    .locals 1
    .param p1    # Landroid/content/Context;
    .param p2    # Ljava/lang/String;
    .param p3    # Lcom/google/android/social/api/network/ApiaryHttpRequest$Configuration;
    .param p4    # Lcom/google/android/social/api/network/ApiaryHttpRequest$GoogleAuthUtilHelper;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    invoke-static {}, Lcom/google/android/social/api/network/RequestFuture;->newFuture()Lcom/google/android/social/api/network/RequestFuture;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/social/api/network/ApiaryHttpRequest;->future:Lcom/google/android/social/api/network/RequestFuture;

    invoke-virtual {p1}, Landroid/content/Context;->getApplicationContext()Landroid/content/Context;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/social/api/network/ApiaryHttpRequest;->context:Landroid/content/Context;

    iput-object p2, p0, Lcom/google/android/social/api/network/ApiaryHttpRequest;->accountName:Ljava/lang/String;

    iput-object p3, p0, Lcom/google/android/social/api/network/ApiaryHttpRequest;->configuration:Lcom/google/android/social/api/network/ApiaryHttpRequest$Configuration;

    iput-object p4, p0, Lcom/google/android/social/api/network/ApiaryHttpRequest;->googleAuthUtilHelper:Lcom/google/android/social/api/network/ApiaryHttpRequest$GoogleAuthUtilHelper;

    return-void
.end method


# virtual methods
.method public getAuthToken()V
    .locals 5
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/android/volley/AuthFailureError;
        }
    .end annotation

    :try_start_0
    iget-object v1, p0, Lcom/google/android/social/api/network/ApiaryHttpRequest;->googleAuthUtilHelper:Lcom/google/android/social/api/network/ApiaryHttpRequest$GoogleAuthUtilHelper;

    iget-object v2, p0, Lcom/google/android/social/api/network/ApiaryHttpRequest;->context:Landroid/content/Context;

    iget-object v3, p0, Lcom/google/android/social/api/network/ApiaryHttpRequest;->accountName:Ljava/lang/String;

    sget-object v4, Lcom/google/android/social/api/network/ApiaryHttpRequest;->OAUTH2_SCOPE_STRING:Ljava/lang/String;

    invoke-virtual {v1, v2, v3, v4}, Lcom/google/android/social/api/network/ApiaryHttpRequest$GoogleAuthUtilHelper;->getToken(Landroid/content/Context;Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    iput-object v1, p0, Lcom/google/android/social/api/network/ApiaryHttpRequest;->authToken:Ljava/lang/String;
    :try_end_0
    .catch Lcom/google/android/gms/auth/UserRecoverableAuthException; {:try_start_0 .. :try_end_0} :catch_0
    .catch Lcom/google/android/gms/auth/GoogleAuthException; {:try_start_0 .. :try_end_0} :catch_1
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_2

    return-void

    :catch_0
    move-exception v0

    new-instance v1, Lcom/android/volley/AuthFailureError;

    invoke-virtual {v0}, Lcom/google/android/gms/auth/UserRecoverableAuthException;->getIntent()Landroid/content/Intent;

    move-result-object v2

    invoke-direct {v1, v2}, Lcom/android/volley/AuthFailureError;-><init>(Landroid/content/Intent;)V

    throw v1

    :catch_1
    move-exception v0

    new-instance v1, Lcom/android/volley/AuthFailureError;

    const-string v2, "Unrecoverable authentication error."

    invoke-direct {v1, v2, v0}, Lcom/android/volley/AuthFailureError;-><init>(Ljava/lang/String;Ljava/lang/Exception;)V

    throw v1

    :catch_2
    move-exception v0

    new-instance v1, Lcom/google/android/social/api/network/RetryableAuthFailureError;

    invoke-direct {v1, v0}, Lcom/google/android/social/api/network/RetryableAuthFailureError;-><init>(Ljava/io/IOException;)V

    throw v1
.end method

.method protected getConfiguration()Lcom/google/android/social/api/network/ApiaryHttpRequest$Configuration;
    .locals 1

    iget-object v0, p0, Lcom/google/android/social/api/network/ApiaryHttpRequest;->configuration:Lcom/google/android/social/api/network/ApiaryHttpRequest$Configuration;

    return-object v0
.end method

.method protected getHeaders(Ljava/util/Map;)Ljava/util/Map;
    .locals 4
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/Map",
            "<",
            "Ljava/lang/String;",
            "Ljava/lang/String;",
            ">;)",
            "Ljava/util/Map",
            "<",
            "Ljava/lang/String;",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation

    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/android/volley/AuthFailureError;
        }
    .end annotation

    new-instance v0, Ljava/util/HashMap;

    invoke-direct {v0, p1}, Ljava/util/HashMap;-><init>(Ljava/util/Map;)V

    iget-object v1, p0, Lcom/google/android/social/api/network/ApiaryHttpRequest;->accountName:Ljava/lang/String;

    if-eqz v1, :cond_0

    invoke-virtual {p0}, Lcom/google/android/social/api/network/ApiaryHttpRequest;->getAuthToken()V

    const-string v1, "Authorization"

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "OAuth "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    iget-object v3, p0, Lcom/google/android/social/api/network/ApiaryHttpRequest;->authToken:Ljava/lang/String;

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-interface {v0, v1, v2}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    :cond_0
    const-string v1, "Accept-Encoding"

    const-string v2, "gzip"

    invoke-interface {v0, v1, v2}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    return-object v0
.end method

.method public abstract getRequest(Lcom/google/android/social/api/network/RequestFuture;)Lcom/android/volley/Request;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/google/android/social/api/network/RequestFuture",
            "<TData;>;)",
            "Lcom/android/volley/Request",
            "<TData;>;"
        }
    .end annotation
.end method

.method public getResults()Ljava/lang/Object;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()TData;"
        }
    .end annotation

    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/lang/InterruptedException;,
            Ljava/util/concurrent/ExecutionException;
        }
    .end annotation

    iget-object v0, p0, Lcom/google/android/social/api/network/ApiaryHttpRequest;->future:Lcom/google/android/social/api/network/RequestFuture;

    invoke-virtual {v0}, Lcom/google/android/social/api/network/RequestFuture;->get()Ljava/lang/Object;

    move-result-object v0

    return-object v0
.end method

.method public makeRequest(Lcom/android/volley/RequestQueue;)Ljava/lang/Object;
    .locals 1
    .param p1    # Lcom/android/volley/RequestQueue;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/android/volley/RequestQueue;",
            ")TData;"
        }
    .end annotation

    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/lang/InterruptedException;,
            Ljava/util/concurrent/ExecutionException;
        }
    .end annotation

    iget-object v0, p0, Lcom/google/android/social/api/network/ApiaryHttpRequest;->future:Lcom/google/android/social/api/network/RequestFuture;

    invoke-virtual {p0, v0}, Lcom/google/android/social/api/network/ApiaryHttpRequest;->getRequest(Lcom/google/android/social/api/network/RequestFuture;)Lcom/android/volley/Request;

    move-result-object v0

    invoke-virtual {p1, v0}, Lcom/android/volley/RequestQueue;->add(Lcom/android/volley/Request;)Lcom/android/volley/Request;

    invoke-virtual {p0}, Lcom/google/android/social/api/network/ApiaryHttpRequest;->getResults()Ljava/lang/Object;

    move-result-object v0

    return-object v0
.end method

.method protected processResponseData(Lcom/android/volley/NetworkResponse;Lcom/android/volley/Cache$Entry;)Lcom/android/volley/Response;
    .locals 8
    .param p1    # Lcom/android/volley/NetworkResponse;
    .param p2    # Lcom/android/volley/Cache$Entry;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/android/volley/NetworkResponse;",
            "Lcom/android/volley/Cache$Entry;",
            ")",
            "Lcom/android/volley/Response",
            "<TData;>;"
        }
    .end annotation

    sget-object v3, Lcom/google/android/social/api/network/ApiaryHttpRequest;->log:Lcom/google/android/social/api/internal/GLog;

    const-string v4, "%s Response: %s (%d bytes)"

    const/4 v5, 0x3

    new-array v5, v5, [Ljava/lang/Object;

    const/4 v6, 0x0

    invoke-virtual {p0}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v7

    invoke-virtual {v7}, Ljava/lang/Class;->getSimpleName()Ljava/lang/String;

    move-result-object v7

    aput-object v7, v5, v6

    const/4 v6, 0x1

    iget v7, p1, Lcom/android/volley/NetworkResponse;->statusCode:I

    invoke-static {v7}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v7

    aput-object v7, v5, v6

    const/4 v6, 0x2

    iget-object v7, p1, Lcom/android/volley/NetworkResponse;->data:[B

    array-length v7, v7

    invoke-static {v7}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v7

    aput-object v7, v5, v6

    invoke-virtual {v3, v4, v5}, Lcom/google/android/social/api/internal/GLog;->debug(Ljava/lang/String;[Ljava/lang/Object;)V

    new-instance v1, Ljava/io/ByteArrayInputStream;

    iget-object v3, p1, Lcom/android/volley/NetworkResponse;->data:[B

    invoke-direct {v1, v3}, Ljava/io/ByteArrayInputStream;-><init>([B)V

    :try_start_0
    new-instance v2, Ljava/util/zip/GZIPInputStream;

    invoke-direct {v2, v1}, Ljava/util/zip/GZIPInputStream;-><init>(Ljava/io/InputStream;)V
    :try_end_0
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_0

    move-object v1, v2

    :goto_0
    iget v3, p1, Lcom/android/volley/NetworkResponse;->statusCode:I

    const/16 v4, 0x190

    if-ge v3, v4, :cond_0

    :try_start_1
    invoke-virtual {p0, v1}, Lcom/google/android/social/api/network/ApiaryHttpRequest;->processResponseData(Ljava/io/InputStream;)Ljava/lang/Object;

    move-result-object v3

    invoke-static {v3, p2}, Lcom/android/volley/Response;->success(Ljava/lang/Object;Lcom/android/volley/Cache$Entry;)Lcom/android/volley/Response;
    :try_end_1
    .catch Ljava/io/IOException; {:try_start_1 .. :try_end_1} :catch_1
    .catch Landroid/accounts/NetworkErrorException; {:try_start_1 .. :try_end_1} :catch_2

    move-result-object v3

    :goto_1
    return-object v3

    :catch_0
    move-exception v0

    sget-object v3, Lcom/google/android/social/api/network/ApiaryHttpRequest;->log:Lcom/google/android/social/api/internal/GLog;

    new-instance v4, Lcom/android/volley/VolleyError;

    const-string v5, "GZip decoding failed."

    invoke-direct {v4, v5, v0}, Lcom/android/volley/VolleyError;-><init>(Ljava/lang/String;Ljava/lang/Throwable;)V

    invoke-virtual {v3, v4}, Lcom/google/android/social/api/internal/GLog;->debug(Ljava/lang/Throwable;)V

    goto :goto_0

    :catch_1
    move-exception v0

    new-instance v3, Lcom/android/volley/VolleyError;

    const-string v4, "Reading response failed."

    invoke-direct {v3, v4, v0}, Lcom/android/volley/VolleyError;-><init>(Ljava/lang/String;Ljava/lang/Throwable;)V

    invoke-static {v3}, Lcom/android/volley/Response;->error(Lcom/android/volley/VolleyError;)Lcom/android/volley/Response;

    move-result-object v3

    goto :goto_1

    :catch_2
    move-exception v0

    new-instance v3, Lcom/android/volley/VolleyError;

    const-string v4, "Reading response failed."

    invoke-direct {v3, v4, v0}, Lcom/android/volley/VolleyError;-><init>(Ljava/lang/String;Ljava/lang/Throwable;)V

    invoke-static {v3}, Lcom/android/volley/Response;->error(Lcom/android/volley/VolleyError;)Lcom/android/volley/Response;

    move-result-object v3

    goto :goto_1

    :cond_0
    new-instance v3, Lcom/android/volley/VolleyError;

    invoke-direct {v3, p1}, Lcom/android/volley/VolleyError;-><init>(Lcom/android/volley/NetworkResponse;)V

    invoke-static {v3}, Lcom/android/volley/Response;->error(Lcom/android/volley/VolleyError;)Lcom/android/volley/Response;

    move-result-object v3

    goto :goto_1
.end method

.method protected abstract processResponseData(Ljava/io/InputStream;)Ljava/lang/Object;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/io/InputStream;",
            ")TData;"
        }
    .end annotation

    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;,
            Landroid/accounts/NetworkErrorException;
        }
    .end annotation
.end method
