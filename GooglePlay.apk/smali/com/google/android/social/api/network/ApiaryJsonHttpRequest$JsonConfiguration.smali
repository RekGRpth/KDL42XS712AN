.class public Lcom/google/android/social/api/network/ApiaryJsonHttpRequest$JsonConfiguration;
.super Lcom/google/android/social/api/network/ApiaryHttpRequest$Configuration;
.source "ApiaryJsonHttpRequest.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/google/android/social/api/network/ApiaryJsonHttpRequest;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x9
    name = "JsonConfiguration"
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "<RequestType:",
        "Ljava/lang/Object;",
        "ResponseType:",
        "Ljava/lang/Object;",
        ">",
        "Lcom/google/android/social/api/network/ApiaryHttpRequest$Configuration;"
    }
.end annotation


# instance fields
.field public final jsonReader:Lcom/google/android/apps/plus/json/EsJson;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/google/android/apps/plus/json/EsJson",
            "<TResponseType;>;"
        }
    .end annotation
.end field

.field public final jsonWriter:Lcom/google/android/apps/plus/json/EsJson;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/google/android/apps/plus/json/EsJson",
            "<TRequestType;>;"
        }
    .end annotation
.end field


# direct methods
.method public constructor <init>(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Lcom/google/android/apps/plus/json/EsJson;Lcom/google/android/apps/plus/json/EsJson;)V
    .locals 0
    .param p1    # Ljava/lang/String;
    .param p2    # Ljava/lang/String;
    .param p3    # Ljava/lang/String;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/String;",
            "Ljava/lang/String;",
            "Ljava/lang/String;",
            "Lcom/google/android/apps/plus/json/EsJson",
            "<TResponseType;>;",
            "Lcom/google/android/apps/plus/json/EsJson",
            "<TRequestType;>;)V"
        }
    .end annotation

    invoke-direct {p0, p1, p2, p3}, Lcom/google/android/social/api/network/ApiaryHttpRequest$Configuration;-><init>(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    iput-object p4, p0, Lcom/google/android/social/api/network/ApiaryJsonHttpRequest$JsonConfiguration;->jsonReader:Lcom/google/android/apps/plus/json/EsJson;

    iput-object p5, p0, Lcom/google/android/social/api/network/ApiaryJsonHttpRequest$JsonConfiguration;->jsonWriter:Lcom/google/android/apps/plus/json/EsJson;

    return-void
.end method
