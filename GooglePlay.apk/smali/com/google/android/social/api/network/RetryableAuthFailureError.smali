.class public Lcom/google/android/social/api/network/RetryableAuthFailureError;
.super Lcom/android/volley/AuthFailureError;
.source "RetryableAuthFailureError.java"


# direct methods
.method public constructor <init>(Ljava/io/IOException;)V
    .locals 1
    .param p1    # Ljava/io/IOException;

    const-string v0, "retry"

    invoke-direct {p0, v0, p1}, Lcom/android/volley/AuthFailureError;-><init>(Ljava/lang/String;Ljava/lang/Exception;)V

    return-void
.end method


# virtual methods
.method public getCause()Ljava/io/IOException;
    .locals 1

    invoke-super {p0}, Lcom/android/volley/AuthFailureError;->getCause()Ljava/lang/Throwable;

    move-result-object v0

    check-cast v0, Ljava/io/IOException;

    return-object v0
.end method

.method public bridge synthetic getCause()Ljava/lang/Throwable;
    .locals 1

    invoke-virtual {p0}, Lcom/google/android/social/api/network/RetryableAuthFailureError;->getCause()Ljava/io/IOException;

    move-result-object v0

    return-object v0
.end method
