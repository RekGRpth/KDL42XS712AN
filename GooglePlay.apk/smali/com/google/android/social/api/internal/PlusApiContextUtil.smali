.class public final Lcom/google/android/social/api/internal/PlusApiContextUtil;
.super Ljava/lang/Object;
.source "PlusApiContextUtil.java"


# direct methods
.method private constructor <init>()V
    .locals 0

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method public static getPlusApiContext(Landroid/content/Context;)Lcom/google/android/social/api/PlusApiContext;
    .locals 2
    .param p0    # Landroid/content/Context;

    invoke-virtual {p0}, Landroid/content/Context;->getApplicationContext()Landroid/content/Context;

    move-result-object v0

    instance-of v1, v0, Lcom/google/android/social/api/PlusApiContextProvider;

    if-eqz v1, :cond_0

    check-cast v0, Lcom/google/android/social/api/PlusApiContextProvider;

    invoke-interface {v0}, Lcom/google/android/social/api/PlusApiContextProvider;->getPlusApiContext()Lcom/google/android/social/api/PlusApiContext;

    move-result-object v1

    :goto_0
    return-object v1

    :cond_0
    invoke-static {}, Lcom/google/android/social/api/DefaultPlusApiContext;->getInstance()Lcom/google/android/social/api/DefaultPlusApiContext;

    move-result-object v1

    goto :goto_0
.end method
