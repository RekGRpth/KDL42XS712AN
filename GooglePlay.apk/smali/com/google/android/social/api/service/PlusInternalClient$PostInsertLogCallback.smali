.class final Lcom/google/android/social/api/service/PlusInternalClient$PostInsertLogCallback;
.super Lcom/google/android/gms/common/internal/GmsClient$CallbackProxy;
.source "PlusInternalClient.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/google/android/social/api/service/PlusInternalClient;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x10
    name = "PostInsertLogCallback"
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/google/android/gms/common/internal/GmsClient",
        "<",
        "Lcom/google/android/social/api/service/IPlusInternalService;",
        ">.CallbackProxy<",
        "Lcom/google/android/social/api/operations/PostInsertLogOperation$Callback;",
        ">;"
    }
.end annotation


# instance fields
.field public final status:Lcom/google/android/gms/common/ConnectionResult;

.field final synthetic this$0:Lcom/google/android/social/api/service/PlusInternalClient;


# direct methods
.method public constructor <init>(Lcom/google/android/social/api/service/PlusInternalClient;Lcom/google/android/social/api/operations/PostInsertLogOperation$Callback;Lcom/google/android/gms/common/ConnectionResult;)V
    .locals 0
    .param p2    # Lcom/google/android/social/api/operations/PostInsertLogOperation$Callback;
    .param p3    # Lcom/google/android/gms/common/ConnectionResult;

    iput-object p1, p0, Lcom/google/android/social/api/service/PlusInternalClient$PostInsertLogCallback;->this$0:Lcom/google/android/social/api/service/PlusInternalClient;

    invoke-direct {p0, p1, p2}, Lcom/google/android/gms/common/internal/GmsClient$CallbackProxy;-><init>(Lcom/google/android/gms/common/internal/GmsClient;Ljava/lang/Object;)V

    iput-object p3, p0, Lcom/google/android/social/api/service/PlusInternalClient$PostInsertLogCallback;->status:Lcom/google/android/gms/common/ConnectionResult;

    return-void
.end method


# virtual methods
.method protected deliverCallback(Lcom/google/android/social/api/operations/PostInsertLogOperation$Callback;)V
    .locals 1
    .param p1    # Lcom/google/android/social/api/operations/PostInsertLogOperation$Callback;

    if-eqz p1, :cond_0

    iget-object v0, p0, Lcom/google/android/social/api/service/PlusInternalClient$PostInsertLogCallback;->status:Lcom/google/android/gms/common/ConnectionResult;

    invoke-interface {p1, v0}, Lcom/google/android/social/api/operations/PostInsertLogOperation$Callback;->onPostInsertLog(Lcom/google/android/gms/common/ConnectionResult;)V

    :cond_0
    return-void
.end method

.method protected bridge synthetic deliverCallback(Ljava/lang/Object;)V
    .locals 0
    .param p1    # Ljava/lang/Object;

    check-cast p1, Lcom/google/android/social/api/operations/PostInsertLogOperation$Callback;

    invoke-virtual {p0, p1}, Lcom/google/android/social/api/service/PlusInternalClient$PostInsertLogCallback;->deliverCallback(Lcom/google/android/social/api/operations/PostInsertLogOperation$Callback;)V

    return-void
.end method
