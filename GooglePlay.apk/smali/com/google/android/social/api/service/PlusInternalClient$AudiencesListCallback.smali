.class final Lcom/google/android/social/api/service/PlusInternalClient$AudiencesListCallback;
.super Lcom/google/android/gms/common/internal/GmsClient$CallbackProxy;
.source "PlusInternalClient.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/google/android/social/api/service/PlusInternalClient;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x10
    name = "AudiencesListCallback"
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/google/android/gms/common/internal/GmsClient",
        "<",
        "Lcom/google/android/social/api/service/IPlusInternalService;",
        ">.CallbackProxy<",
        "Lcom/google/android/social/api/people/operations/AudiencesListOperation$Callback;",
        ">;"
    }
.end annotation


# instance fields
.field public final audience:Ljava/util/ArrayList;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/ArrayList",
            "<",
            "Lcom/google/android/social/api/people/model/AudienceMember;",
            ">;"
        }
    .end annotation
.end field

.field public final status:Lcom/google/android/gms/common/ConnectionResult;

.field final synthetic this$0:Lcom/google/android/social/api/service/PlusInternalClient;


# direct methods
.method public constructor <init>(Lcom/google/android/social/api/service/PlusInternalClient;Lcom/google/android/social/api/people/operations/AudiencesListOperation$Callback;Lcom/google/android/gms/common/ConnectionResult;Ljava/util/ArrayList;)V
    .locals 0
    .param p2    # Lcom/google/android/social/api/people/operations/AudiencesListOperation$Callback;
    .param p3    # Lcom/google/android/gms/common/ConnectionResult;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/google/android/social/api/people/operations/AudiencesListOperation$Callback;",
            "Lcom/google/android/gms/common/ConnectionResult;",
            "Ljava/util/ArrayList",
            "<",
            "Lcom/google/android/social/api/people/model/AudienceMember;",
            ">;)V"
        }
    .end annotation

    iput-object p1, p0, Lcom/google/android/social/api/service/PlusInternalClient$AudiencesListCallback;->this$0:Lcom/google/android/social/api/service/PlusInternalClient;

    invoke-direct {p0, p1, p2}, Lcom/google/android/gms/common/internal/GmsClient$CallbackProxy;-><init>(Lcom/google/android/gms/common/internal/GmsClient;Ljava/lang/Object;)V

    iput-object p3, p0, Lcom/google/android/social/api/service/PlusInternalClient$AudiencesListCallback;->status:Lcom/google/android/gms/common/ConnectionResult;

    iput-object p4, p0, Lcom/google/android/social/api/service/PlusInternalClient$AudiencesListCallback;->audience:Ljava/util/ArrayList;

    return-void
.end method


# virtual methods
.method protected deliverCallback(Lcom/google/android/social/api/people/operations/AudiencesListOperation$Callback;)V
    .locals 2
    .param p1    # Lcom/google/android/social/api/people/operations/AudiencesListOperation$Callback;

    if-eqz p1, :cond_0

    iget-object v0, p0, Lcom/google/android/social/api/service/PlusInternalClient$AudiencesListCallback;->status:Lcom/google/android/gms/common/ConnectionResult;

    iget-object v1, p0, Lcom/google/android/social/api/service/PlusInternalClient$AudiencesListCallback;->audience:Ljava/util/ArrayList;

    invoke-interface {p1, v0, v1}, Lcom/google/android/social/api/people/operations/AudiencesListOperation$Callback;->onAudiencesList(Lcom/google/android/gms/common/ConnectionResult;Ljava/util/ArrayList;)V

    :cond_0
    return-void
.end method

.method protected bridge synthetic deliverCallback(Ljava/lang/Object;)V
    .locals 0
    .param p1    # Ljava/lang/Object;

    check-cast p1, Lcom/google/android/social/api/people/operations/AudiencesListOperation$Callback;

    invoke-virtual {p0, p1}, Lcom/google/android/social/api/service/PlusInternalClient$AudiencesListCallback;->deliverCallback(Lcom/google/android/social/api/people/operations/AudiencesListOperation$Callback;)V

    return-void
.end method
