.class final Lcom/google/android/social/api/service/PlusInternalClient$DownloadImageBinderCallbacks;
.super Lcom/google/android/social/api/service/IPlusInternalCallbacks$Stub;
.source "PlusInternalClient.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/google/android/social/api/service/PlusInternalClient;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x10
    name = "DownloadImageBinderCallbacks"
.end annotation


# instance fields
.field private final listener:Lcom/google/android/social/api/operations/DownloadImageOperation$Callback;

.field final synthetic this$0:Lcom/google/android/social/api/service/PlusInternalClient;


# direct methods
.method public constructor <init>(Lcom/google/android/social/api/service/PlusInternalClient;Lcom/google/android/social/api/operations/DownloadImageOperation$Callback;)V
    .locals 0
    .param p2    # Lcom/google/android/social/api/operations/DownloadImageOperation$Callback;

    iput-object p1, p0, Lcom/google/android/social/api/service/PlusInternalClient$DownloadImageBinderCallbacks;->this$0:Lcom/google/android/social/api/service/PlusInternalClient;

    invoke-direct {p0}, Lcom/google/android/social/api/service/IPlusInternalCallbacks$Stub;-><init>()V

    iput-object p2, p0, Lcom/google/android/social/api/service/PlusInternalClient$DownloadImageBinderCallbacks;->listener:Lcom/google/android/social/api/operations/DownloadImageOperation$Callback;

    return-void
.end method


# virtual methods
.method public onBundleLoaded(ILandroid/os/Bundle;Landroid/os/Bundle;)V
    .locals 9
    .param p1    # I
    .param p2    # Landroid/os/Bundle;
    .param p3    # Landroid/os/Bundle;

    iget-object v8, p0, Lcom/google/android/social/api/service/PlusInternalClient$DownloadImageBinderCallbacks;->this$0:Lcom/google/android/social/api/service/PlusInternalClient;

    new-instance v0, Lcom/google/android/social/api/service/PlusInternalClient$DownloadImageCallback;

    iget-object v1, p0, Lcom/google/android/social/api/service/PlusInternalClient$DownloadImageBinderCallbacks;->this$0:Lcom/google/android/social/api/service/PlusInternalClient;

    iget-object v2, p0, Lcom/google/android/social/api/service/PlusInternalClient$DownloadImageBinderCallbacks;->listener:Lcom/google/android/social/api/operations/DownloadImageOperation$Callback;

    invoke-static {p1, p2}, Lcom/google/android/social/api/service/Results;->getConnectionResult(ILandroid/os/Bundle;)Lcom/google/android/gms/common/ConnectionResult;

    move-result-object v3

    const-string v4, "bitmap"

    invoke-virtual {p3, v4}, Landroid/os/Bundle;->getParcelable(Ljava/lang/String;)Landroid/os/Parcelable;

    move-result-object v4

    check-cast v4, Landroid/graphics/Bitmap;

    const-string v5, "url"

    invoke-virtual {p3, v5}, Landroid/os/Bundle;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v5

    const-string v6, "width"

    invoke-virtual {p3, v6}, Landroid/os/Bundle;->getInt(Ljava/lang/String;)I

    move-result v6

    const-string v7, "height"

    invoke-virtual {p3, v7}, Landroid/os/Bundle;->getInt(Ljava/lang/String;)I

    move-result v7

    invoke-direct/range {v0 .. v7}, Lcom/google/android/social/api/service/PlusInternalClient$DownloadImageCallback;-><init>(Lcom/google/android/social/api/service/PlusInternalClient;Lcom/google/android/social/api/operations/DownloadImageOperation$Callback;Lcom/google/android/gms/common/ConnectionResult;Landroid/graphics/Bitmap;Ljava/lang/String;II)V

    invoke-virtual {v8, v0}, Lcom/google/android/social/api/service/PlusInternalClient;->doCallback(Lcom/google/android/gms/common/internal/GmsClient$CallbackProxy;)V

    return-void
.end method
