.class abstract Lcom/google/android/social/api/logging/AbstractAnalyticsNamespace;
.super Ljava/lang/Object;
.source "AbstractAnalyticsNamespace.java"


# direct methods
.method constructor <init>()V
    .locals 0

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method static final buildNamespacedType(Ljava/lang/String;I)Lcom/google/api/services/plus/model/FavaDiagnosticsNamespacedType;
    .locals 2
    .param p0    # Ljava/lang/String;
    .param p1    # I

    new-instance v0, Lcom/google/api/services/plus/model/FavaDiagnosticsNamespacedType;

    invoke-direct {v0}, Lcom/google/api/services/plus/model/FavaDiagnosticsNamespacedType;-><init>()V

    iput-object p0, v0, Lcom/google/api/services/plus/model/FavaDiagnosticsNamespacedType;->namespace:Ljava/lang/String;

    invoke-static {p1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    iput-object v1, v0, Lcom/google/api/services/plus/model/FavaDiagnosticsNamespacedType;->typeNum:Ljava/lang/Integer;

    return-object v0
.end method
