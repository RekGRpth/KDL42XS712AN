.class Lcom/google/android/social/api/people/activities/PeopleSuggestHandler;
.super Landroid/os/Handler;
.source "PeopleSuggestHandler.java"


# direct methods
.method public constructor <init>()V
    .locals 1

    invoke-static {}, Landroid/os/Looper;->getMainLooper()Landroid/os/Looper;

    move-result-object v0

    invoke-direct {p0, v0}, Landroid/os/Handler;-><init>(Landroid/os/Looper;)V

    return-void
.end method


# virtual methods
.method public destroy(Lcom/google/android/social/api/people/activities/PeopleSuggestActivity;)V
    .locals 0
    .param p1    # Lcom/google/android/social/api/people/activities/PeopleSuggestActivity;

    invoke-virtual {p0, p1}, Lcom/google/android/social/api/people/activities/PeopleSuggestHandler;->removeCallbacksAndMessages(Ljava/lang/Object;)V

    return-void
.end method

.method public handleMessage(Landroid/os/Message;)V
    .locals 2
    .param p1    # Landroid/os/Message;

    iget-object v0, p1, Landroid/os/Message;->obj:Ljava/lang/Object;

    check-cast v0, Lcom/google/android/social/api/people/activities/PeopleSuggestActivity;

    if-nez v0, :cond_0

    :goto_0
    return-void

    :cond_0
    iget v1, p1, Landroid/os/Message;->what:I

    packed-switch v1, :pswitch_data_0

    :pswitch_0
    goto :goto_0

    :pswitch_1
    invoke-virtual {v0}, Lcom/google/android/social/api/people/activities/PeopleSuggestActivity;->onExecuteTextChanged()V

    const/4 v1, 0x0

    invoke-virtual {p0, v1}, Lcom/google/android/social/api/people/activities/PeopleSuggestHandler;->removeMessages(I)V

    goto :goto_0

    :pswitch_2
    invoke-virtual {v0}, Lcom/google/android/social/api/people/activities/PeopleSuggestActivity;->onHideProgressDialog()V

    goto :goto_0

    :pswitch_3
    invoke-virtual {v0}, Lcom/google/android/social/api/people/activities/PeopleSuggestActivity;->onShowErrorDialog()V

    goto :goto_0

    nop

    :pswitch_data_0
    .packed-switch 0x0
        :pswitch_1
        :pswitch_0
        :pswitch_2
        :pswitch_3
    .end packed-switch
.end method

.method public sendExecuteTextChanged(Lcom/google/android/social/api/people/activities/PeopleSuggestActivity;)V
    .locals 1
    .param p1    # Lcom/google/android/social/api/people/activities/PeopleSuggestActivity;

    const/4 v0, 0x0

    invoke-virtual {p0, v0, p1}, Lcom/google/android/social/api/people/activities/PeopleSuggestHandler;->obtainMessage(ILjava/lang/Object;)Landroid/os/Message;

    move-result-object v0

    invoke-virtual {p0, v0}, Lcom/google/android/social/api/people/activities/PeopleSuggestHandler;->sendMessage(Landroid/os/Message;)Z

    return-void
.end method

.method public sendHideProgressDialog(Lcom/google/android/social/api/people/activities/PeopleSuggestActivity;)V
    .locals 1
    .param p1    # Lcom/google/android/social/api/people/activities/PeopleSuggestActivity;

    const/4 v0, 0x2

    invoke-virtual {p0, v0, p1}, Lcom/google/android/social/api/people/activities/PeopleSuggestHandler;->obtainMessage(ILjava/lang/Object;)Landroid/os/Message;

    move-result-object v0

    invoke-virtual {p0, v0}, Lcom/google/android/social/api/people/activities/PeopleSuggestHandler;->sendMessage(Landroid/os/Message;)Z

    return-void
.end method

.method public sendShowErrorDialog(Lcom/google/android/social/api/people/activities/PeopleSuggestActivity;)V
    .locals 1
    .param p1    # Lcom/google/android/social/api/people/activities/PeopleSuggestActivity;

    const/4 v0, 0x3

    invoke-virtual {p0, v0, p1}, Lcom/google/android/social/api/people/activities/PeopleSuggestHandler;->obtainMessage(ILjava/lang/Object;)Landroid/os/Message;

    move-result-object v0

    invoke-virtual {p0, v0}, Lcom/google/android/social/api/people/activities/PeopleSuggestHandler;->sendMessage(Landroid/os/Message;)Z

    return-void
.end method
