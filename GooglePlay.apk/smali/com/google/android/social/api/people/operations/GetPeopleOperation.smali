.class public final Lcom/google/android/social/api/people/operations/GetPeopleOperation;
.super Ljava/lang/Object;
.source "GetPeopleOperation.java"

# interfaces
.implements Lcom/google/android/social/api/operations/PlusApiOperation;


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/google/android/social/api/people/operations/GetPeopleOperation$GetPersonRequest;,
        Lcom/google/android/social/api/people/operations/GetPeopleOperation$Callback;
    }
.end annotation


# instance fields
.field private final accountName:Ljava/lang/String;

.field private final callback:Lcom/google/android/social/api/people/operations/GetPeopleOperation$Callback;

.field private final people:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Lcom/google/android/social/api/people/model/AudienceMember;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method public constructor <init>(Lcom/google/android/social/api/people/operations/GetPeopleOperation$Callback;Ljava/lang/String;Landroid/os/Bundle;)V
    .locals 1
    .param p1    # Lcom/google/android/social/api/people/operations/GetPeopleOperation$Callback;
    .param p2    # Ljava/lang/String;
    .param p3    # Landroid/os/Bundle;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    iput-object p1, p0, Lcom/google/android/social/api/people/operations/GetPeopleOperation;->callback:Lcom/google/android/social/api/people/operations/GetPeopleOperation$Callback;

    iput-object p2, p0, Lcom/google/android/social/api/people/operations/GetPeopleOperation;->accountName:Ljava/lang/String;

    const-string v0, "people"

    invoke-virtual {p3, v0}, Landroid/os/Bundle;->getParcelableArrayList(Ljava/lang/String;)Ljava/util/ArrayList;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/social/api/people/operations/GetPeopleOperation;->people:Ljava/util/List;

    return-void
.end method


# virtual methods
.method public execute(Landroid/content/Context;Lcom/android/volley/RequestQueue;)V
    .locals 6
    .param p1    # Landroid/content/Context;
    .param p2    # Lcom/android/volley/RequestQueue;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/lang/InterruptedException;,
            Ljava/util/concurrent/ExecutionException;
        }
    .end annotation

    new-instance v1, Lcom/google/android/social/api/network/ApiaryBatchHttpRequest;

    iget-object v3, p0, Lcom/google/android/social/api/people/operations/GetPeopleOperation;->accountName:Ljava/lang/String;

    invoke-direct {v1, p1, v3}, Lcom/google/android/social/api/network/ApiaryBatchHttpRequest;-><init>(Landroid/content/Context;Ljava/lang/String;)V

    const/4 v0, 0x0

    :goto_0
    iget-object v3, p0, Lcom/google/android/social/api/people/operations/GetPeopleOperation;->people:Ljava/util/List;

    invoke-interface {v3}, Ljava/util/List;->size()I

    move-result v3

    if-ge v0, v3, :cond_0

    new-instance v4, Lcom/google/android/social/api/people/operations/GetPeopleOperation$GetPersonRequest;

    iget-object v5, p0, Lcom/google/android/social/api/people/operations/GetPeopleOperation;->accountName:Ljava/lang/String;

    iget-object v3, p0, Lcom/google/android/social/api/people/operations/GetPeopleOperation;->people:Ljava/util/List;

    invoke-interface {v3, v0}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Lcom/google/android/social/api/people/model/AudienceMember;

    invoke-virtual {v3}, Lcom/google/android/social/api/people/model/AudienceMember;->getId()Ljava/lang/String;

    move-result-object v3

    invoke-direct {v4, p1, v5, v3}, Lcom/google/android/social/api/people/operations/GetPeopleOperation$GetPersonRequest;-><init>(Landroid/content/Context;Ljava/lang/String;Ljava/lang/String;)V

    invoke-virtual {v1, v4}, Lcom/google/android/social/api/network/ApiaryBatchHttpRequest;->add(Lcom/google/android/social/api/network/ApiaryHttpRequest;)V

    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    :cond_0
    invoke-virtual {v1, p2}, Lcom/google/android/social/api/network/ApiaryBatchHttpRequest;->makeRequest(Lcom/android/volley/RequestQueue;)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Ljava/util/ArrayList;

    iget-object v3, p0, Lcom/google/android/social/api/people/operations/GetPeopleOperation;->callback:Lcom/google/android/social/api/people/operations/GetPeopleOperation$Callback;

    sget-object v4, Lcom/google/android/gms/common/ConnectionResult;->RESULT_SUCCESS:Lcom/google/android/gms/common/ConnectionResult;

    invoke-interface {v3, v4, v2}, Lcom/google/android/social/api/people/operations/GetPeopleOperation$Callback;->onPeopleLoaded(Lcom/google/android/gms/common/ConnectionResult;Ljava/util/ArrayList;)V

    return-void
.end method

.method public onFailure(Lcom/google/android/gms/common/ConnectionResult;)V
    .locals 2
    .param p1    # Lcom/google/android/gms/common/ConnectionResult;

    iget-object v0, p0, Lcom/google/android/social/api/people/operations/GetPeopleOperation;->callback:Lcom/google/android/social/api/people/operations/GetPeopleOperation$Callback;

    const/4 v1, 0x0

    invoke-interface {v0, p1, v1}, Lcom/google/android/social/api/people/operations/GetPeopleOperation$Callback;->onPeopleLoaded(Lcom/google/android/gms/common/ConnectionResult;Ljava/util/ArrayList;)V

    return-void
.end method
