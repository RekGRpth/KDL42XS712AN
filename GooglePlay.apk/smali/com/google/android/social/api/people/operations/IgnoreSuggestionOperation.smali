.class public Lcom/google/android/social/api/people/operations/IgnoreSuggestionOperation;
.super Ljava/lang/Object;
.source "IgnoreSuggestionOperation.java"

# interfaces
.implements Lcom/google/android/social/api/operations/PlusApiOperation;


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/google/android/social/api/people/operations/IgnoreSuggestionOperation$IgnoreSuggestionHttpRequest;,
        Lcom/google/android/social/api/people/operations/IgnoreSuggestionOperation$Callback;
    }
.end annotation


# instance fields
.field private final accountName:Ljava/lang/String;

.field private final callback:Lcom/google/android/social/api/people/operations/IgnoreSuggestionOperation$Callback;

.field private final ignore:Z

.field private final userId:Ljava/lang/String;


# direct methods
.method public constructor <init>(Lcom/google/android/social/api/people/operations/IgnoreSuggestionOperation$Callback;Ljava/lang/String;Ljava/lang/String;Z)V
    .locals 0
    .param p1    # Lcom/google/android/social/api/people/operations/IgnoreSuggestionOperation$Callback;
    .param p2    # Ljava/lang/String;
    .param p3    # Ljava/lang/String;
    .param p4    # Z

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    iput-object p2, p0, Lcom/google/android/social/api/people/operations/IgnoreSuggestionOperation;->accountName:Ljava/lang/String;

    iput-object p1, p0, Lcom/google/android/social/api/people/operations/IgnoreSuggestionOperation;->callback:Lcom/google/android/social/api/people/operations/IgnoreSuggestionOperation$Callback;

    iput-boolean p4, p0, Lcom/google/android/social/api/people/operations/IgnoreSuggestionOperation;->ignore:Z

    iput-object p3, p0, Lcom/google/android/social/api/people/operations/IgnoreSuggestionOperation;->userId:Ljava/lang/String;

    return-void
.end method


# virtual methods
.method public execute(Landroid/content/Context;Lcom/android/volley/RequestQueue;)V
    .locals 4
    .param p1    # Landroid/content/Context;
    .param p2    # Lcom/android/volley/RequestQueue;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/lang/InterruptedException;,
            Ljava/util/concurrent/ExecutionException;
        }
    .end annotation

    iget-boolean v1, p0, Lcom/google/android/social/api/people/operations/IgnoreSuggestionOperation;->ignore:Z

    if-eqz v1, :cond_0

    iget-object v1, p0, Lcom/google/android/social/api/people/operations/IgnoreSuggestionOperation;->userId:Ljava/lang/String;

    invoke-static {v1}, Lcom/google/android/social/api/network/ApiaryConfig;->getIgnoreSuggestion(Ljava/lang/String;)Lcom/google/android/social/api/network/ApiaryJsonHttpRequest$JsonConfiguration;

    move-result-object v0

    :goto_0
    new-instance v1, Lcom/google/android/social/api/people/operations/IgnoreSuggestionOperation$IgnoreSuggestionHttpRequest;

    iget-object v2, p0, Lcom/google/android/social/api/people/operations/IgnoreSuggestionOperation;->accountName:Ljava/lang/String;

    invoke-direct {v1, p0, p1, v2, v0}, Lcom/google/android/social/api/people/operations/IgnoreSuggestionOperation$IgnoreSuggestionHttpRequest;-><init>(Lcom/google/android/social/api/people/operations/IgnoreSuggestionOperation;Landroid/content/Context;Ljava/lang/String;Lcom/google/android/social/api/network/ApiaryJsonHttpRequest$JsonConfiguration;)V

    invoke-virtual {v1, p2}, Lcom/google/android/social/api/people/operations/IgnoreSuggestionOperation$IgnoreSuggestionHttpRequest;->makeRequest(Lcom/android/volley/RequestQueue;)Ljava/lang/Object;

    iget-object v1, p0, Lcom/google/android/social/api/people/operations/IgnoreSuggestionOperation;->callback:Lcom/google/android/social/api/people/operations/IgnoreSuggestionOperation$Callback;

    sget-object v2, Lcom/google/android/gms/common/ConnectionResult;->RESULT_SUCCESS:Lcom/google/android/gms/common/ConnectionResult;

    iget-boolean v3, p0, Lcom/google/android/social/api/people/operations/IgnoreSuggestionOperation;->ignore:Z

    invoke-interface {v1, v2, v3}, Lcom/google/android/social/api/people/operations/IgnoreSuggestionOperation$Callback;->onIgnoreSuggestion(Lcom/google/android/gms/common/ConnectionResult;Z)V

    return-void

    :cond_0
    iget-object v1, p0, Lcom/google/android/social/api/people/operations/IgnoreSuggestionOperation;->userId:Ljava/lang/String;

    invoke-static {v1}, Lcom/google/android/social/api/network/ApiaryConfig;->getUnignoreSuggestion(Ljava/lang/String;)Lcom/google/android/social/api/network/ApiaryJsonHttpRequest$JsonConfiguration;

    move-result-object v0

    goto :goto_0
.end method

.method public onFailure(Lcom/google/android/gms/common/ConnectionResult;)V
    .locals 2
    .param p1    # Lcom/google/android/gms/common/ConnectionResult;

    iget-object v0, p0, Lcom/google/android/social/api/people/operations/IgnoreSuggestionOperation;->callback:Lcom/google/android/social/api/people/operations/IgnoreSuggestionOperation$Callback;

    iget-boolean v1, p0, Lcom/google/android/social/api/people/operations/IgnoreSuggestionOperation;->ignore:Z

    invoke-interface {v0, p1, v1}, Lcom/google/android/social/api/people/operations/IgnoreSuggestionOperation$Callback;->onIgnoreSuggestion(Lcom/google/android/gms/common/ConnectionResult;Z)V

    return-void
.end method
