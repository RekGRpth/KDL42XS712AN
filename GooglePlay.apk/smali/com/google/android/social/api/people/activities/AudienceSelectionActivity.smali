.class public Lcom/google/android/social/api/people/activities/AudienceSelectionActivity;
.super Lcom/google/android/social/api/people/activities/BaseAudienceSelectionActivity;
.source "AudienceSelectionActivity.java"

# interfaces
.implements Lcom/google/android/gms/common/GooglePlayServicesClient$ConnectionCallbacks;
.implements Lcom/google/android/gms/common/GooglePlayServicesClient$OnConnectionFailedListener;
.implements Lcom/google/android/social/api/people/operations/AudiencesListOperation$Callback;


# static fields
.field private static final log:Lcom/google/android/social/api/internal/GLog;


# instance fields
.field private apiClient:Lcom/google/android/social/api/service/PlusInternalClient;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    new-instance v0, Lcom/google/android/social/api/internal/GLog;

    invoke-direct {v0}, Lcom/google/android/social/api/internal/GLog;-><init>()V

    sput-object v0, Lcom/google/android/social/api/people/activities/AudienceSelectionActivity;->log:Lcom/google/android/social/api/internal/GLog;

    return-void
.end method

.method public constructor <init>()V
    .locals 0

    invoke-direct {p0}, Lcom/google/android/social/api/people/activities/BaseAudienceSelectionActivity;-><init>()V

    return-void
.end method


# virtual methods
.method public onAudiencesList(Lcom/google/android/gms/common/ConnectionResult;Ljava/util/ArrayList;)V
    .locals 10
    .param p1    # Lcom/google/android/gms/common/ConnectionResult;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/google/android/gms/common/ConnectionResult;",
            "Ljava/util/ArrayList",
            "<",
            "Lcom/google/android/social/api/people/model/AudienceMember;",
            ">;)V"
        }
    .end annotation

    const/4 v9, 0x0

    const/4 v6, 0x1

    const/4 v8, 0x0

    invoke-virtual {p1}, Lcom/google/android/gms/common/ConnectionResult;->isSuccess()Z

    move-result v4

    if-eqz v4, :cond_3

    sget-object v4, Lcom/google/android/social/api/people/activities/AudienceSelectionActivity;->log:Lcom/google/android/social/api/internal/GLog;

    const-string v5, "Got %d Audiences"

    new-array v6, v6, [Ljava/lang/Object;

    invoke-virtual {p2}, Ljava/util/ArrayList;->size()I

    move-result v7

    invoke-static {v7}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v7

    aput-object v7, v6, v8

    invoke-virtual {v4, v5, v6}, Lcom/google/android/social/api/internal/GLog;->debug(Ljava/lang/String;[Ljava/lang/Object;)V

    new-instance v1, Ljava/util/ArrayList;

    invoke-direct {v1}, Ljava/util/ArrayList;-><init>()V

    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    const/4 v2, 0x0

    :goto_0
    invoke-virtual {p2}, Ljava/util/ArrayList;->size()I

    move-result v4

    if-ge v2, v4, :cond_2

    invoke-virtual {p2, v2}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Lcom/google/android/social/api/people/model/AudienceMember;

    invoke-virtual {v3}, Lcom/google/android/social/api/people/model/AudienceMember;->isSystemGroup()Z

    move-result v4

    if-eqz v4, :cond_1

    invoke-interface {v1, v3}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    :cond_0
    :goto_1
    add-int/lit8 v2, v2, 0x1

    goto :goto_0

    :cond_1
    invoke-virtual {v3}, Lcom/google/android/social/api/people/model/AudienceMember;->isCircle()Z

    move-result v4

    if-eqz v4, :cond_0

    invoke-interface {v0, v3}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    goto :goto_1

    :cond_2
    invoke-virtual {p0, v1, v0, v9, v9}, Lcom/google/android/social/api/people/activities/AudienceSelectionActivity;->swapContents(Ljava/util/List;Ljava/util/List;Ljava/util/List;Ljava/util/List;)V

    :goto_2
    return-void

    :cond_3
    sget-object v4, Lcom/google/android/social/api/people/activities/AudienceSelectionActivity;->log:Lcom/google/android/social/api/internal/GLog;

    const-string v5, "Getting Audience Failed %d."

    new-array v6, v6, [Ljava/lang/Object;

    invoke-virtual {p1}, Lcom/google/android/gms/common/ConnectionResult;->getErrorCode()I

    move-result v7

    invoke-static {v7}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v7

    aput-object v7, v6, v8

    invoke-virtual {v4, v5, v6}, Lcom/google/android/social/api/internal/GLog;->debug(Ljava/lang/String;[Ljava/lang/Object;)V

    goto :goto_2
.end method

.method public onConnected()V
    .locals 7

    const/4 v4, 0x1

    const/4 v5, 0x0

    invoke-virtual {p0}, Lcom/google/android/social/api/people/activities/AudienceSelectionActivity;->getAudienceIntent()Lcom/google/android/social/api/people/AudienceSelectionIntent;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/android/social/api/people/AudienceSelectionIntent;->getLoadGroups()Z

    move-result v6

    if-eqz v6, :cond_2

    invoke-virtual {p0}, Lcom/google/android/social/api/people/activities/AudienceSelectionActivity;->getGroupsLoaded()Z

    move-result v6

    if-nez v6, :cond_2

    move v2, v4

    :goto_0
    invoke-virtual {v0}, Lcom/google/android/social/api/people/AudienceSelectionIntent;->getLoadCircles()Z

    move-result v6

    if-eqz v6, :cond_3

    invoke-virtual {p0}, Lcom/google/android/social/api/people/activities/AudienceSelectionActivity;->getCirclesLoaded()Z

    move-result v6

    if-nez v6, :cond_3

    move v1, v4

    :goto_1
    invoke-virtual {v0}, Lcom/google/android/social/api/people/AudienceSelectionIntent;->getLoadPeople()Z

    move-result v6

    if-eqz v6, :cond_4

    invoke-virtual {p0}, Lcom/google/android/social/api/people/activities/AudienceSelectionActivity;->getPeopleLoaded()Z

    move-result v6

    if-nez v6, :cond_4

    move v3, v4

    :goto_2
    if-nez v2, :cond_0

    if-nez v1, :cond_0

    if-eqz v3, :cond_1

    :cond_0
    iget-object v4, p0, Lcom/google/android/social/api/people/activities/AudienceSelectionActivity;->apiClient:Lcom/google/android/social/api/service/PlusInternalClient;

    invoke-virtual {v4, p0, v2, v1}, Lcom/google/android/social/api/service/PlusInternalClient;->loadSocialNetwork(Lcom/google/android/social/api/people/operations/AudiencesListOperation$Callback;ZZ)V

    :cond_1
    return-void

    :cond_2
    move v2, v5

    goto :goto_0

    :cond_3
    move v1, v5

    goto :goto_1

    :cond_4
    move v3, v5

    goto :goto_2
.end method

.method public onConnectionFailed(Lcom/google/android/gms/common/ConnectionResult;)V
    .locals 0
    .param p1    # Lcom/google/android/gms/common/ConnectionResult;

    return-void
.end method

.method protected onCreateComplete()V
    .locals 3

    invoke-virtual {p0}, Lcom/google/android/social/api/people/activities/AudienceSelectionActivity;->getAudienceIntent()Lcom/google/android/social/api/people/AudienceSelectionIntent;

    move-result-object v1

    invoke-virtual {v1}, Lcom/google/android/social/api/people/AudienceSelectionIntent;->getAccountName()Ljava/lang/String;

    move-result-object v0

    const-string v2, "Account name must not be null."

    invoke-static {v0, v2}, Lcom/google/android/gms/common/internal/Preconditions;->checkNotNull(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    new-instance v2, Lcom/google/android/social/api/service/PlusInternalClient;

    invoke-direct {v2, p0, p0, p0, v0}, Lcom/google/android/social/api/service/PlusInternalClient;-><init>(Landroid/content/Context;Lcom/google/android/gms/common/GooglePlayServicesClient$ConnectionCallbacks;Lcom/google/android/gms/common/GooglePlayServicesClient$OnConnectionFailedListener;Ljava/lang/String;)V

    iput-object v2, p0, Lcom/google/android/social/api/people/activities/AudienceSelectionActivity;->apiClient:Lcom/google/android/social/api/service/PlusInternalClient;

    return-void
.end method

.method public onDisconnected()V
    .locals 0

    return-void
.end method

.method protected onStart()V
    .locals 1

    invoke-super {p0}, Lcom/google/android/social/api/people/activities/BaseAudienceSelectionActivity;->onStart()V

    iget-object v0, p0, Lcom/google/android/social/api/people/activities/AudienceSelectionActivity;->apiClient:Lcom/google/android/social/api/service/PlusInternalClient;

    invoke-virtual {v0}, Lcom/google/android/social/api/service/PlusInternalClient;->connect()V

    return-void
.end method

.method protected onStop()V
    .locals 1

    iget-object v0, p0, Lcom/google/android/social/api/people/activities/AudienceSelectionActivity;->apiClient:Lcom/google/android/social/api/service/PlusInternalClient;

    invoke-virtual {v0}, Lcom/google/android/social/api/service/PlusInternalClient;->disconnect()V

    invoke-super {p0}, Lcom/google/android/social/api/people/activities/BaseAudienceSelectionActivity;->onStop()V

    return-void
.end method
