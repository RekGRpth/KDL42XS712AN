.class public Lcom/google/android/social/api/people/adapters/PeopleSuggestAdapter;
.super Landroid/widget/BaseAdapter;
.source "PeopleSuggestAdapter.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/google/android/social/api/people/adapters/PeopleSuggestAdapter$PopupStrategy;,
        Lcom/google/android/social/api/people/adapters/PeopleSuggestAdapter$PersonShownListener;
    }
.end annotation


# static fields
.field private static final DEFAULT_POPUP_STRATEGY:Lcom/google/android/social/api/people/adapters/PeopleSuggestAdapter$PopupStrategy;


# instance fields
.field private apiClient:Lcom/google/android/social/api/service/PlusInternalClient;

.field private final idsToPendingOperation:Ljava/util/HashMap;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/HashMap",
            "<",
            "Ljava/lang/String;",
            "Ljava/lang/Integer;",
            ">;"
        }
    .end annotation
.end field

.field private final inflater:Landroid/view/LayoutInflater;

.field private personRowListener:Lcom/google/android/social/api/people/views/PeopleSuggestRowView$PersonRowListener;

.field private personShownListener:Lcom/google/android/social/api/people/adapters/PeopleSuggestAdapter$PersonShownListener;

.field private popupStrategy:Lcom/google/android/social/api/people/adapters/PeopleSuggestAdapter$PopupStrategy;

.field private results:Lcom/google/android/social/api/people/model/PersonList;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    new-instance v0, Lcom/google/android/social/api/people/adapters/PeopleSuggestAdapter$1;

    invoke-direct {v0}, Lcom/google/android/social/api/people/adapters/PeopleSuggestAdapter$1;-><init>()V

    sput-object v0, Lcom/google/android/social/api/people/adapters/PeopleSuggestAdapter;->DEFAULT_POPUP_STRATEGY:Lcom/google/android/social/api/people/adapters/PeopleSuggestAdapter$PopupStrategy;

    return-void
.end method

.method public constructor <init>(Lcom/google/android/social/api/service/PlusInternalClient;Landroid/view/LayoutInflater;)V
    .locals 1
    .param p1    # Lcom/google/android/social/api/service/PlusInternalClient;
    .param p2    # Landroid/view/LayoutInflater;

    invoke-direct {p0}, Landroid/widget/BaseAdapter;-><init>()V

    new-instance v0, Ljava/util/HashMap;

    invoke-direct {v0}, Ljava/util/HashMap;-><init>()V

    iput-object v0, p0, Lcom/google/android/social/api/people/adapters/PeopleSuggestAdapter;->idsToPendingOperation:Ljava/util/HashMap;

    sget-object v0, Lcom/google/android/social/api/people/adapters/PeopleSuggestAdapter;->DEFAULT_POPUP_STRATEGY:Lcom/google/android/social/api/people/adapters/PeopleSuggestAdapter$PopupStrategy;

    iput-object v0, p0, Lcom/google/android/social/api/people/adapters/PeopleSuggestAdapter;->popupStrategy:Lcom/google/android/social/api/people/adapters/PeopleSuggestAdapter$PopupStrategy;

    iput-object p2, p0, Lcom/google/android/social/api/people/adapters/PeopleSuggestAdapter;->inflater:Landroid/view/LayoutInflater;

    iput-object p1, p0, Lcom/google/android/social/api/people/adapters/PeopleSuggestAdapter;->apiClient:Lcom/google/android/social/api/service/PlusInternalClient;

    return-void
.end method


# virtual methods
.method public addPendingOperation(Ljava/lang/String;I)V
    .locals 2
    .param p1    # Ljava/lang/String;
    .param p2    # I

    iget-object v0, p0, Lcom/google/android/social/api/people/adapters/PeopleSuggestAdapter;->idsToPendingOperation:Ljava/util/HashMap;

    invoke-static {p2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    invoke-virtual {v0, p1, v1}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    invoke-virtual {p0}, Lcom/google/android/social/api/people/adapters/PeopleSuggestAdapter;->notifyDataSetChanged()V

    return-void
.end method

.method public clearPendingOperation()V
    .locals 1

    iget-object v0, p0, Lcom/google/android/social/api/people/adapters/PeopleSuggestAdapter;->idsToPendingOperation:Ljava/util/HashMap;

    invoke-virtual {v0}, Ljava/util/HashMap;->clear()V

    invoke-virtual {p0}, Lcom/google/android/social/api/people/adapters/PeopleSuggestAdapter;->notifyDataSetChanged()V

    return-void
.end method

.method public getCount()I
    .locals 1

    iget-object v0, p0, Lcom/google/android/social/api/people/adapters/PeopleSuggestAdapter;->results:Lcom/google/android/social/api/people/model/PersonList;

    if-nez v0, :cond_0

    const/4 v0, 0x0

    :goto_0
    return v0

    :cond_0
    iget-object v0, p0, Lcom/google/android/social/api/people/adapters/PeopleSuggestAdapter;->results:Lcom/google/android/social/api/people/model/PersonList;

    invoke-virtual {v0}, Lcom/google/android/social/api/people/model/PersonList;->getPersonCount()I

    move-result v0

    goto :goto_0
.end method

.method public getItem(I)Lcom/google/android/social/api/people/model/Person;
    .locals 1
    .param p1    # I

    iget-object v0, p0, Lcom/google/android/social/api/people/adapters/PeopleSuggestAdapter;->results:Lcom/google/android/social/api/people/model/PersonList;

    invoke-virtual {v0, p1}, Lcom/google/android/social/api/people/model/PersonList;->getPerson(I)Lcom/google/android/social/api/people/model/Person;

    move-result-object v0

    return-object v0
.end method

.method public bridge synthetic getItem(I)Ljava/lang/Object;
    .locals 1
    .param p1    # I

    invoke-virtual {p0, p1}, Lcom/google/android/social/api/people/adapters/PeopleSuggestAdapter;->getItem(I)Lcom/google/android/social/api/people/model/Person;

    move-result-object v0

    return-object v0
.end method

.method public getItemId(I)J
    .locals 2
    .param p1    # I

    invoke-virtual {p0, p1}, Lcom/google/android/social/api/people/adapters/PeopleSuggestAdapter;->getItem(I)Lcom/google/android/social/api/people/model/Person;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/android/social/api/people/model/Person;->getStableId()J

    move-result-wide v0

    return-wide v0
.end method

.method public getView(ILandroid/view/View;Landroid/view/ViewGroup;)Landroid/view/View;
    .locals 7
    .param p1    # I
    .param p2    # Landroid/view/View;
    .param p3    # Landroid/view/ViewGroup;

    move-object v1, p2

    check-cast v1, Lcom/google/android/social/api/people/views/PeopleSuggestRowView;

    invoke-virtual {p0, p1}, Lcom/google/android/social/api/people/adapters/PeopleSuggestAdapter;->getItem(I)Lcom/google/android/social/api/people/model/Person;

    move-result-object v0

    iget-object v3, p0, Lcom/google/android/social/api/people/adapters/PeopleSuggestAdapter;->personShownListener:Lcom/google/android/social/api/people/adapters/PeopleSuggestAdapter$PersonShownListener;

    invoke-interface {v3, v0}, Lcom/google/android/social/api/people/adapters/PeopleSuggestAdapter$PersonShownListener;->onPersonShown(Lcom/google/android/social/api/people/model/Person;)V

    if-eqz v1, :cond_0

    invoke-virtual {v1}, Lcom/google/android/social/api/people/views/PeopleSuggestRowView;->isReusable()Z

    move-result v3

    if-nez v3, :cond_1

    :cond_0
    iget-object v3, p0, Lcom/google/android/social/api/people/adapters/PeopleSuggestAdapter;->inflater:Landroid/view/LayoutInflater;

    const v4, 0x7f0400f4    # com.android.vending.R.layout.plus_people_suggest_row

    const/4 v5, 0x0

    invoke-virtual {v3, v4, v5}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;)Landroid/view/View;

    move-result-object v1

    check-cast v1, Lcom/google/android/social/api/people/views/PeopleSuggestRowView;

    iget-object v3, p0, Lcom/google/android/social/api/people/adapters/PeopleSuggestAdapter;->personRowListener:Lcom/google/android/social/api/people/views/PeopleSuggestRowView$PersonRowListener;

    invoke-virtual {v1, v3}, Lcom/google/android/social/api/people/views/PeopleSuggestRowView;->setPersonRowListener(Lcom/google/android/social/api/people/views/PeopleSuggestRowView$PersonRowListener;)V

    :cond_1
    iget-object v3, p0, Lcom/google/android/social/api/people/adapters/PeopleSuggestAdapter;->idsToPendingOperation:Ljava/util/HashMap;

    invoke-virtual {v0}, Lcom/google/android/social/api/people/model/Person;->getId()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v3, v4}, Ljava/util/HashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Ljava/lang/Integer;

    if-nez v2, :cond_2

    const/4 v3, 0x0

    invoke-static {v3}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    :cond_2
    iget-object v3, p0, Lcom/google/android/social/api/people/adapters/PeopleSuggestAdapter;->apiClient:Lcom/google/android/social/api/service/PlusInternalClient;

    invoke-virtual {v2}, Ljava/lang/Integer;->intValue()I

    move-result v4

    iget-object v5, p0, Lcom/google/android/social/api/people/adapters/PeopleSuggestAdapter;->popupStrategy:Lcom/google/android/social/api/people/adapters/PeopleSuggestAdapter$PopupStrategy;

    invoke-virtual {v2}, Ljava/lang/Integer;->intValue()I

    move-result v6

    invoke-interface {v5, v0, v6}, Lcom/google/android/social/api/people/adapters/PeopleSuggestAdapter$PopupStrategy;->shouldPopupShow(Lcom/google/android/social/api/people/model/Person;I)Z

    move-result v5

    invoke-virtual {v1, v3, v0, v4, v5}, Lcom/google/android/social/api/people/views/PeopleSuggestRowView;->setupView(Lcom/google/android/social/api/service/PlusInternalClient;Lcom/google/android/social/api/people/model/Person;IZ)V

    return-object v1
.end method

.method public getViewTypeCount()I
    .locals 1

    const/4 v0, 0x1

    return v0
.end method

.method public hasStableIds()Z
    .locals 1

    const/4 v0, 0x1

    return v0
.end method

.method public isPendingOperationEmpty()Z
    .locals 1

    iget-object v0, p0, Lcom/google/android/social/api/people/adapters/PeopleSuggestAdapter;->idsToPendingOperation:Ljava/util/HashMap;

    invoke-virtual {v0}, Ljava/util/HashMap;->size()I

    move-result v0

    if-nez v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public remove(I)V
    .locals 2
    .param p1    # I

    iget-object v0, p0, Lcom/google/android/social/api/people/adapters/PeopleSuggestAdapter;->results:Lcom/google/android/social/api/people/model/PersonList;

    invoke-virtual {p0, p1}, Lcom/google/android/social/api/people/adapters/PeopleSuggestAdapter;->getItem(I)Lcom/google/android/social/api/people/model/Person;

    move-result-object v1

    invoke-virtual {v1}, Lcom/google/android/social/api/people/model/Person;->getId()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/google/android/social/api/people/model/PersonList;->removePerson(Ljava/lang/String;)Lcom/google/android/social/api/people/model/Person;

    invoke-virtual {p0}, Lcom/google/android/social/api/people/adapters/PeopleSuggestAdapter;->notifyDataSetChanged()V

    return-void
.end method

.method public removePendingOperation(Ljava/lang/String;)V
    .locals 1
    .param p1    # Ljava/lang/String;

    iget-object v0, p0, Lcom/google/android/social/api/people/adapters/PeopleSuggestAdapter;->idsToPendingOperation:Ljava/util/HashMap;

    invoke-virtual {v0, p1}, Ljava/util/HashMap;->remove(Ljava/lang/Object;)Ljava/lang/Object;

    invoke-virtual {p0}, Lcom/google/android/social/api/people/adapters/PeopleSuggestAdapter;->notifyDataSetChanged()V

    return-void
.end method

.method public setPersonList(Lcom/google/android/social/api/people/model/PersonList;)V
    .locals 0
    .param p1    # Lcom/google/android/social/api/people/model/PersonList;

    iput-object p1, p0, Lcom/google/android/social/api/people/adapters/PeopleSuggestAdapter;->results:Lcom/google/android/social/api/people/model/PersonList;

    invoke-virtual {p0}, Lcom/google/android/social/api/people/adapters/PeopleSuggestAdapter;->notifyDataSetChanged()V

    return-void
.end method

.method public setPersonRowListener(Lcom/google/android/social/api/people/views/PeopleSuggestRowView$PersonRowListener;)V
    .locals 0
    .param p1    # Lcom/google/android/social/api/people/views/PeopleSuggestRowView$PersonRowListener;

    iput-object p1, p0, Lcom/google/android/social/api/people/adapters/PeopleSuggestAdapter;->personRowListener:Lcom/google/android/social/api/people/views/PeopleSuggestRowView$PersonRowListener;

    return-void
.end method

.method public setPersonShownListener(Lcom/google/android/social/api/people/adapters/PeopleSuggestAdapter$PersonShownListener;)V
    .locals 0
    .param p1    # Lcom/google/android/social/api/people/adapters/PeopleSuggestAdapter$PersonShownListener;

    iput-object p1, p0, Lcom/google/android/social/api/people/adapters/PeopleSuggestAdapter;->personShownListener:Lcom/google/android/social/api/people/adapters/PeopleSuggestAdapter$PersonShownListener;

    return-void
.end method

.method public setPopupStrategy(Lcom/google/android/social/api/people/adapters/PeopleSuggestAdapter$PopupStrategy;)V
    .locals 0
    .param p1    # Lcom/google/android/social/api/people/adapters/PeopleSuggestAdapter$PopupStrategy;

    if-nez p1, :cond_0

    sget-object p1, Lcom/google/android/social/api/people/adapters/PeopleSuggestAdapter;->DEFAULT_POPUP_STRATEGY:Lcom/google/android/social/api/people/adapters/PeopleSuggestAdapter$PopupStrategy;

    :cond_0
    iput-object p1, p0, Lcom/google/android/social/api/people/adapters/PeopleSuggestAdapter;->popupStrategy:Lcom/google/android/social/api/people/adapters/PeopleSuggestAdapter$PopupStrategy;

    return-void
.end method

.method public updatePerson(Lcom/google/android/social/api/people/model/Person;)V
    .locals 3
    .param p1    # Lcom/google/android/social/api/people/model/Person;

    const/4 v0, 0x0

    :goto_0
    iget-object v1, p0, Lcom/google/android/social/api/people/adapters/PeopleSuggestAdapter;->results:Lcom/google/android/social/api/people/model/PersonList;

    invoke-virtual {v1}, Lcom/google/android/social/api/people/model/PersonList;->getPersonCount()I

    move-result v1

    if-ge v0, v1, :cond_1

    iget-object v1, p0, Lcom/google/android/social/api/people/adapters/PeopleSuggestAdapter;->results:Lcom/google/android/social/api/people/model/PersonList;

    invoke-virtual {v1, v0}, Lcom/google/android/social/api/people/model/PersonList;->getPerson(I)Lcom/google/android/social/api/people/model/Person;

    move-result-object v1

    invoke-virtual {v1}, Lcom/google/android/social/api/people/model/Person;->getId()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {p1}, Lcom/google/android/social/api/people/model/Person;->getId()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_0

    iget-object v1, p0, Lcom/google/android/social/api/people/adapters/PeopleSuggestAdapter;->results:Lcom/google/android/social/api/people/model/PersonList;

    invoke-virtual {v1, v0, p1}, Lcom/google/android/social/api/people/model/PersonList;->setPerson(ILcom/google/android/social/api/people/model/Person;)Z

    :cond_0
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    :cond_1
    invoke-virtual {p0}, Lcom/google/android/social/api/people/adapters/PeopleSuggestAdapter;->notifyDataSetChanged()V

    return-void
.end method
