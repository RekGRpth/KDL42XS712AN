.class public interface abstract Lcom/google/android/social/api/people/operations/GetPeopleOperation$Callback;
.super Ljava/lang/Object;
.source "GetPeopleOperation.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/google/android/social/api/people/operations/GetPeopleOperation;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x609
    name = "Callback"
.end annotation


# virtual methods
.method public abstract onPeopleLoaded(Lcom/google/android/gms/common/ConnectionResult;Ljava/util/ArrayList;)V
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/google/android/gms/common/ConnectionResult;",
            "Ljava/util/ArrayList",
            "<",
            "Lcom/google/android/social/api/people/model/Person;",
            ">;)V"
        }
    .end annotation
.end method
