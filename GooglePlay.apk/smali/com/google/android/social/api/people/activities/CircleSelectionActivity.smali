.class public Lcom/google/android/social/api/people/activities/CircleSelectionActivity;
.super Lcom/google/android/social/api/people/activities/AudienceSelectionActivity;
.source "CircleSelectionActivity.java"

# interfaces
.implements Landroid/widget/CompoundButton$OnCheckedChangeListener;


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/google/android/social/api/people/activities/CircleSelectionActivity$HiddenCircleAwareEditAudienceAdapter;
    }
.end annotation


# static fields
.field private static final CIRCLE_COMPARER:Ljava/util/Comparator;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Comparator",
            "<",
            "Lcom/google/android/social/api/people/model/AudienceMember;",
            ">;"
        }
    .end annotation
.end field

.field private static final COLLATOR:Ljava/text/Collator;


# instance fields
.field private mAudienceBackup:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Lcom/google/android/social/api/people/model/AudienceMember;",
            ">;"
        }
    .end annotation
.end field

.field private mEveryoneCheckbox:Landroid/widget/CheckBox;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    invoke-static {}, Ljava/text/Collator;->getInstance()Ljava/text/Collator;

    move-result-object v0

    sput-object v0, Lcom/google/android/social/api/people/activities/CircleSelectionActivity;->COLLATOR:Ljava/text/Collator;

    new-instance v0, Lcom/google/android/social/api/people/activities/CircleSelectionActivity$1;

    invoke-direct {v0}, Lcom/google/android/social/api/people/activities/CircleSelectionActivity$1;-><init>()V

    sput-object v0, Lcom/google/android/social/api/people/activities/CircleSelectionActivity;->CIRCLE_COMPARER:Ljava/util/Comparator;

    return-void
.end method

.method public constructor <init>()V
    .locals 0

    invoke-direct {p0}, Lcom/google/android/social/api/people/activities/AudienceSelectionActivity;-><init>()V

    return-void
.end method

.method static synthetic access$000()Ljava/text/Collator;
    .locals 1

    sget-object v0, Lcom/google/android/social/api/people/activities/CircleSelectionActivity;->COLLATOR:Ljava/text/Collator;

    return-object v0
.end method

.method static synthetic access$100()Ljava/util/Comparator;
    .locals 1

    sget-object v0, Lcom/google/android/social/api/people/activities/CircleSelectionActivity;->CIRCLE_COMPARER:Ljava/util/Comparator;

    return-object v0
.end method


# virtual methods
.method public audienceMemberRemoved(Lcom/google/android/social/api/people/adapters/EditAudienceAdapter;Lcom/google/android/social/api/people/model/AudienceMember;)V
    .locals 2
    .param p1    # Lcom/google/android/social/api/people/adapters/EditAudienceAdapter;
    .param p2    # Lcom/google/android/social/api/people/model/AudienceMember;

    invoke-virtual {p0}, Lcom/google/android/social/api/people/activities/CircleSelectionActivity;->getAdapter()Lcom/google/android/social/api/people/adapters/EditAudienceAdapter;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/android/social/api/people/adapters/EditAudienceAdapter;->getAudienceSelections()Ljava/util/ArrayList;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/social/api/people/activities/CircleSelectionActivity;->mAudienceBackup:Ljava/util/List;

    iget-object v0, p0, Lcom/google/android/social/api/people/activities/CircleSelectionActivity;->mEveryoneCheckbox:Landroid/widget/CheckBox;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Landroid/widget/CheckBox;->setChecked(Z)V

    invoke-super {p0, p1, p2}, Lcom/google/android/social/api/people/activities/AudienceSelectionActivity;->audienceMemberRemoved(Lcom/google/android/social/api/people/adapters/EditAudienceAdapter;Lcom/google/android/social/api/people/model/AudienceMember;)V

    return-void
.end method

.method public audienceMemberRemoved(Lcom/google/android/social/api/people/views/AudienceView;Lcom/google/android/social/api/people/model/AudienceMember;)V
    .locals 2
    .param p1    # Lcom/google/android/social/api/people/views/AudienceView;
    .param p2    # Lcom/google/android/social/api/people/model/AudienceMember;

    invoke-virtual {p0}, Lcom/google/android/social/api/people/activities/CircleSelectionActivity;->getAdapter()Lcom/google/android/social/api/people/adapters/EditAudienceAdapter;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/android/social/api/people/adapters/EditAudienceAdapter;->getAudienceSelections()Ljava/util/ArrayList;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/social/api/people/activities/CircleSelectionActivity;->mAudienceBackup:Ljava/util/List;

    iget-object v0, p0, Lcom/google/android/social/api/people/activities/CircleSelectionActivity;->mEveryoneCheckbox:Landroid/widget/CheckBox;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Landroid/widget/CheckBox;->setChecked(Z)V

    invoke-super {p0, p1, p2}, Lcom/google/android/social/api/people/activities/AudienceSelectionActivity;->audienceMemberRemoved(Lcom/google/android/social/api/people/views/AudienceView;Lcom/google/android/social/api/people/model/AudienceMember;)V

    return-void
.end method

.method protected buildResult()Lcom/google/android/social/api/people/AudienceSelectionIntent;
    .locals 2

    invoke-super {p0}, Lcom/google/android/social/api/people/activities/AudienceSelectionActivity;->buildResult()Lcom/google/android/social/api/people/AudienceSelectionIntent;

    move-result-object v0

    iget-object v1, p0, Lcom/google/android/social/api/people/activities/CircleSelectionActivity;->mEveryoneCheckbox:Landroid/widget/CheckBox;

    invoke-virtual {v1}, Landroid/widget/CheckBox;->isChecked()Z

    move-result v1

    invoke-virtual {v0, v1}, Lcom/google/android/social/api/people/AudienceSelectionIntent;->setEveryoneChecked(Z)Lcom/google/android/social/api/people/AudienceSelectionIntent;

    move-result-object v0

    return-object v0
.end method

.method protected createAdapter()Lcom/google/android/social/api/people/adapters/EditAudienceAdapter;
    .locals 1

    new-instance v0, Lcom/google/android/social/api/people/activities/CircleSelectionActivity$HiddenCircleAwareEditAudienceAdapter;

    invoke-direct {v0, p0, p0}, Lcom/google/android/social/api/people/activities/CircleSelectionActivity$HiddenCircleAwareEditAudienceAdapter;-><init>(Lcom/google/android/social/api/people/activities/CircleSelectionActivity;Landroid/content/Context;)V

    return-object v0
.end method

.method public onCheckedChanged(Landroid/widget/CompoundButton;Z)V
    .locals 2
    .param p1    # Landroid/widget/CompoundButton;
    .param p2    # Z

    invoke-virtual {p0}, Lcom/google/android/social/api/people/activities/CircleSelectionActivity;->getListView()Landroid/widget/ListView;

    move-result-object v1

    if-nez p2, :cond_1

    const/4 v0, 0x1

    :goto_0
    invoke-virtual {v1, v0}, Landroid/widget/ListView;->setClickable(Z)V

    if-eqz p2, :cond_2

    invoke-virtual {p0}, Lcom/google/android/social/api/people/activities/CircleSelectionActivity;->getAdapter()Lcom/google/android/social/api/people/adapters/EditAudienceAdapter;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/android/social/api/people/adapters/EditAudienceAdapter;->getAudienceSelections()Ljava/util/ArrayList;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/social/api/people/activities/CircleSelectionActivity;->mAudienceBackup:Ljava/util/List;

    invoke-virtual {p0}, Lcom/google/android/social/api/people/activities/CircleSelectionActivity;->getAudienceView()Lcom/google/android/social/api/people/views/AudienceView;

    move-result-object v0

    invoke-virtual {p0}, Lcom/google/android/social/api/people/activities/CircleSelectionActivity;->getAdapter()Lcom/google/android/social/api/people/adapters/EditAudienceAdapter;

    move-result-object v1

    invoke-virtual {v1}, Lcom/google/android/social/api/people/adapters/EditAudienceAdapter;->getCircles()Ljava/util/List;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/google/android/social/api/people/views/AudienceView;->set(Ljava/util/List;)V

    invoke-virtual {p0}, Lcom/google/android/social/api/people/activities/CircleSelectionActivity;->getAdapter()Lcom/google/android/social/api/people/adapters/EditAudienceAdapter;

    move-result-object v0

    invoke-virtual {p0}, Lcom/google/android/social/api/people/activities/CircleSelectionActivity;->getAdapter()Lcom/google/android/social/api/people/adapters/EditAudienceAdapter;

    move-result-object v1

    invoke-virtual {v1}, Lcom/google/android/social/api/people/adapters/EditAudienceAdapter;->getCircles()Ljava/util/List;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/google/android/social/api/people/adapters/EditAudienceAdapter;->setAudienceSelections(Ljava/util/Collection;)V

    :cond_0
    :goto_1
    invoke-virtual {p0}, Lcom/google/android/social/api/people/activities/CircleSelectionActivity;->selectionChanged()V

    return-void

    :cond_1
    const/4 v0, 0x0

    goto :goto_0

    :cond_2
    iget-object v0, p0, Lcom/google/android/social/api/people/activities/CircleSelectionActivity;->mAudienceBackup:Ljava/util/List;

    if-eqz v0, :cond_0

    invoke-virtual {p0}, Lcom/google/android/social/api/people/activities/CircleSelectionActivity;->getAudienceView()Lcom/google/android/social/api/people/views/AudienceView;

    move-result-object v0

    iget-object v1, p0, Lcom/google/android/social/api/people/activities/CircleSelectionActivity;->mAudienceBackup:Ljava/util/List;

    invoke-virtual {v0, v1}, Lcom/google/android/social/api/people/views/AudienceView;->set(Ljava/util/List;)V

    invoke-virtual {p0}, Lcom/google/android/social/api/people/activities/CircleSelectionActivity;->getAdapter()Lcom/google/android/social/api/people/adapters/EditAudienceAdapter;

    move-result-object v0

    iget-object v1, p0, Lcom/google/android/social/api/people/activities/CircleSelectionActivity;->mAudienceBackup:Ljava/util/List;

    invoke-virtual {v0, v1}, Lcom/google/android/social/api/people/adapters/EditAudienceAdapter;->setAudienceSelections(Ljava/util/Collection;)V

    goto :goto_1
.end method

.method protected onCreateComplete()V
    .locals 2

    invoke-virtual {p0}, Lcom/google/android/social/api/people/activities/CircleSelectionActivity;->getAudienceIntent()Lcom/google/android/social/api/people/AudienceSelectionIntent;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/android/social/api/people/AudienceSelectionIntent;->getEveryoneCheckboxText()Ljava/lang/String;

    move-result-object v0

    if-eqz v0, :cond_0

    const v0, 0x7f0801d2    # com.android.vending.R.id.audience_select_all_text

    invoke-virtual {p0, v0}, Lcom/google/android/social/api/people/activities/CircleSelectionActivity;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    invoke-virtual {p0}, Lcom/google/android/social/api/people/activities/CircleSelectionActivity;->getAudienceIntent()Lcom/google/android/social/api/people/AudienceSelectionIntent;

    move-result-object v1

    invoke-virtual {v1}, Lcom/google/android/social/api/people/AudienceSelectionIntent;->getEveryoneCheckboxText()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    :cond_0
    const v0, 0x7f0801d1    # com.android.vending.R.id.audience_select_all

    invoke-virtual {p0, v0}, Lcom/google/android/social/api/people/activities/CircleSelectionActivity;->findViewById(I)Landroid/view/View;

    move-result-object v0

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Landroid/view/View;->setVisibility(I)V

    const v0, 0x7f0801d3    # com.android.vending.R.id.audience_select_all_checkbox

    invoke-virtual {p0, v0}, Lcom/google/android/social/api/people/activities/CircleSelectionActivity;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/CheckBox;

    iput-object v0, p0, Lcom/google/android/social/api/people/activities/CircleSelectionActivity;->mEveryoneCheckbox:Landroid/widget/CheckBox;

    iget-object v0, p0, Lcom/google/android/social/api/people/activities/CircleSelectionActivity;->mEveryoneCheckbox:Landroid/widget/CheckBox;

    invoke-virtual {p0}, Lcom/google/android/social/api/people/activities/CircleSelectionActivity;->getAudienceIntent()Lcom/google/android/social/api/people/AudienceSelectionIntent;

    move-result-object v1

    invoke-virtual {v1}, Lcom/google/android/social/api/people/AudienceSelectionIntent;->isEveryoneChecked()Z

    move-result v1

    invoke-virtual {v0, v1}, Landroid/widget/CheckBox;->setChecked(Z)V

    iget-object v0, p0, Lcom/google/android/social/api/people/activities/CircleSelectionActivity;->mEveryoneCheckbox:Landroid/widget/CheckBox;

    invoke-virtual {v0, p0}, Landroid/widget/CheckBox;->setOnCheckedChangeListener(Landroid/widget/CompoundButton$OnCheckedChangeListener;)V

    iget-object v0, p0, Lcom/google/android/social/api/people/activities/CircleSelectionActivity;->mEveryoneCheckbox:Landroid/widget/CheckBox;

    invoke-virtual {v0}, Landroid/widget/CheckBox;->isChecked()Z

    move-result v0

    if-eqz v0, :cond_1

    invoke-virtual {p0}, Lcom/google/android/social/api/people/activities/CircleSelectionActivity;->getAdapter()Lcom/google/android/social/api/people/adapters/EditAudienceAdapter;

    move-result-object v0

    invoke-virtual {p0}, Lcom/google/android/social/api/people/activities/CircleSelectionActivity;->getAdapter()Lcom/google/android/social/api/people/adapters/EditAudienceAdapter;

    move-result-object v1

    invoke-virtual {v1}, Lcom/google/android/social/api/people/adapters/EditAudienceAdapter;->getCircles()Ljava/util/List;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/google/android/social/api/people/adapters/EditAudienceAdapter;->setAudienceSelections(Ljava/util/Collection;)V

    :cond_1
    invoke-super {p0}, Lcom/google/android/social/api/people/activities/AudienceSelectionActivity;->onCreateComplete()V

    return-void
.end method

.method protected selectionChanged()V
    .locals 2

    invoke-super {p0}, Lcom/google/android/social/api/people/activities/AudienceSelectionActivity;->selectionChanged()V

    iget-object v0, p0, Lcom/google/android/social/api/people/activities/CircleSelectionActivity;->mEveryoneCheckbox:Landroid/widget/CheckBox;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/google/android/social/api/people/activities/CircleSelectionActivity;->mEveryoneCheckbox:Landroid/widget/CheckBox;

    invoke-virtual {v0}, Landroid/widget/CheckBox;->isChecked()Z

    move-result v0

    if-eqz v0, :cond_0

    const v0, 0x7f0801d6    # com.android.vending.R.id.ok

    invoke-virtual {p0, v0}, Lcom/google/android/social/api/people/activities/CircleSelectionActivity;->findViewById(I)Landroid/view/View;

    move-result-object v0

    const/4 v1, 0x1

    invoke-virtual {v0, v1}, Landroid/view/View;->setEnabled(Z)V

    :cond_0
    return-void
.end method
