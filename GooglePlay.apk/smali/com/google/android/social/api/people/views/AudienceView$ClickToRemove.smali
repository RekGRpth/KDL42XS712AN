.class public Lcom/google/android/social/api/people/views/AudienceView$ClickToRemove;
.super Lcom/google/android/social/api/people/views/AudienceView;
.source "AudienceView.java"

# interfaces
.implements Landroid/view/View$OnClickListener;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/google/android/social/api/people/views/AudienceView;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x9
    name = "ClickToRemove"
.end annotation


# instance fields
.field private mAudienceChangedListener:Lcom/google/android/social/api/people/views/AudienceView$AudienceChangedListener;


# direct methods
.method public constructor <init>(Landroid/content/Context;)V
    .locals 0
    .param p1    # Landroid/content/Context;

    invoke-direct {p0, p1}, Lcom/google/android/social/api/people/views/AudienceView;-><init>(Landroid/content/Context;)V

    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;)V
    .locals 0
    .param p1    # Landroid/content/Context;
    .param p2    # Landroid/util/AttributeSet;

    invoke-direct {p0, p1, p2}, Lcom/google/android/social/api/people/views/AudienceView;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;)V

    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V
    .locals 0
    .param p1    # Landroid/content/Context;
    .param p2    # Landroid/util/AttributeSet;
    .param p3    # I

    invoke-direct {p0, p1, p2, p3}, Lcom/google/android/social/api/people/views/AudienceView;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V

    return-void
.end method


# virtual methods
.method protected getChipBorderRight(Lcom/google/android/social/api/people/model/AudienceMember;)I
    .locals 1
    .param p1    # Lcom/google/android/social/api/people/model/AudienceMember;

    const v0, 0x7f0200cb    # com.android.vending.R.drawable.plus_chip_close

    return v0
.end method

.method protected getChipView(Lcom/google/android/social/api/people/model/AudienceMember;Landroid/view/LayoutInflater;)Landroid/view/View;
    .locals 1
    .param p1    # Lcom/google/android/social/api/people/model/AudienceMember;
    .param p2    # Landroid/view/LayoutInflater;

    invoke-super {p0, p1, p2}, Lcom/google/android/social/api/people/views/AudienceView;->getChipView(Lcom/google/android/social/api/people/model/AudienceMember;Landroid/view/LayoutInflater;)Landroid/view/View;

    move-result-object v0

    invoke-virtual {v0, p1}, Landroid/view/View;->setTag(Ljava/lang/Object;)V

    invoke-virtual {v0, p0}, Landroid/view/View;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    return-object v0
.end method

.method public onClick(Landroid/view/View;)V
    .locals 2
    .param p1    # Landroid/view/View;

    invoke-virtual {p1}, Landroid/view/View;->getTag()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/social/api/people/model/AudienceMember;

    invoke-virtual {p0, v0}, Lcom/google/android/social/api/people/views/AudienceView$ClickToRemove;->remove(Lcom/google/android/social/api/people/model/AudienceMember;)V

    iget-object v1, p0, Lcom/google/android/social/api/people/views/AudienceView$ClickToRemove;->mAudienceChangedListener:Lcom/google/android/social/api/people/views/AudienceView$AudienceChangedListener;

    if-eqz v1, :cond_0

    iget-object v1, p0, Lcom/google/android/social/api/people/views/AudienceView$ClickToRemove;->mAudienceChangedListener:Lcom/google/android/social/api/people/views/AudienceView$AudienceChangedListener;

    invoke-interface {v1, p0, v0}, Lcom/google/android/social/api/people/views/AudienceView$AudienceChangedListener;->audienceMemberRemoved(Lcom/google/android/social/api/people/views/AudienceView;Lcom/google/android/social/api/people/model/AudienceMember;)V

    :cond_0
    return-void
.end method

.method public setAudienceChangedListener(Lcom/google/android/social/api/people/views/AudienceView$AudienceChangedListener;)V
    .locals 0
    .param p1    # Lcom/google/android/social/api/people/views/AudienceView$AudienceChangedListener;

    iput-object p1, p0, Lcom/google/android/social/api/people/views/AudienceView$ClickToRemove;->mAudienceChangedListener:Lcom/google/android/social/api/people/views/AudienceView$AudienceChangedListener;

    return-void
.end method

.method public setOnClickListener(Landroid/view/View$OnClickListener;)V
    .locals 0
    .param p1    # Landroid/view/View$OnClickListener;

    return-void
.end method
