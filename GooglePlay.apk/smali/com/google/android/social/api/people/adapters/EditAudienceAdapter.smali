.class public Lcom/google/android/social/api/people/adapters/EditAudienceAdapter;
.super Landroid/widget/BaseAdapter;
.source "EditAudienceAdapter.java"

# interfaces
.implements Landroid/widget/SectionIndexer;


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/google/android/social/api/people/adapters/EditAudienceAdapter$Section;,
        Lcom/google/android/social/api/people/adapters/EditAudienceAdapter$CheckedListener;,
        Lcom/google/android/social/api/people/adapters/EditAudienceAdapter$AudienceChangedListener;
    }
.end annotation


# static fields
.field private static final AUDIENCE_COMPARER:Ljava/util/Comparator;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Comparator",
            "<",
            "Lcom/google/android/social/api/people/model/AudienceMember;",
            ">;"
        }
    .end annotation
.end field


# instance fields
.field private final mCircles:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Lcom/google/android/social/api/people/model/AudienceMember;",
            ">;"
        }
    .end annotation
.end field

.field private mContext:Landroid/content/Context;

.field private final mInflater:Landroid/view/LayoutInflater;

.field private mListener:Lcom/google/android/social/api/people/adapters/EditAudienceAdapter$AudienceChangedListener;

.field private final mPeople:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Lcom/google/android/social/api/people/model/AudienceMember;",
            ">;"
        }
    .end annotation
.end field

.field private final mSearchResults:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Lcom/google/android/social/api/people/model/AudienceMember;",
            ">;"
        }
    .end annotation
.end field

.field private mSections:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Lcom/google/android/social/api/people/adapters/EditAudienceAdapter$Section;",
            ">;"
        }
    .end annotation
.end field

.field private final mSelectedAudience:Ljava/util/HashSet;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/HashSet",
            "<",
            "Lcom/google/android/social/api/people/model/AudienceMember;",
            ">;"
        }
    .end annotation
.end field

.field private final mSystemGroups:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Lcom/google/android/social/api/people/model/AudienceMember;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method static constructor <clinit>()V
    .locals 1

    new-instance v0, Lcom/google/android/social/api/people/adapters/EditAudienceAdapter$1;

    invoke-direct {v0}, Lcom/google/android/social/api/people/adapters/EditAudienceAdapter$1;-><init>()V

    sput-object v0, Lcom/google/android/social/api/people/adapters/EditAudienceAdapter;->AUDIENCE_COMPARER:Ljava/util/Comparator;

    return-void
.end method

.method public constructor <init>(Landroid/content/Context;)V
    .locals 1
    .param p1    # Landroid/content/Context;

    invoke-direct {p0}, Landroid/widget/BaseAdapter;-><init>()V

    new-instance v0, Ljava/util/HashSet;

    invoke-direct {v0}, Ljava/util/HashSet;-><init>()V

    iput-object v0, p0, Lcom/google/android/social/api/people/adapters/EditAudienceAdapter;->mSelectedAudience:Ljava/util/HashSet;

    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, Lcom/google/android/social/api/people/adapters/EditAudienceAdapter;->mSearchResults:Ljava/util/List;

    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, Lcom/google/android/social/api/people/adapters/EditAudienceAdapter;->mCircles:Ljava/util/List;

    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, Lcom/google/android/social/api/people/adapters/EditAudienceAdapter;->mPeople:Ljava/util/List;

    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, Lcom/google/android/social/api/people/adapters/EditAudienceAdapter;->mSystemGroups:Ljava/util/List;

    iput-object p1, p0, Lcom/google/android/social/api/people/adapters/EditAudienceAdapter;->mContext:Landroid/content/Context;

    const-string v0, "layout_inflater"

    invoke-virtual {p1, v0}, Landroid/content/Context;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/view/LayoutInflater;

    iput-object v0, p0, Lcom/google/android/social/api/people/adapters/EditAudienceAdapter;->mInflater:Landroid/view/LayoutInflater;

    invoke-virtual {p0}, Lcom/google/android/social/api/people/adapters/EditAudienceAdapter;->rebuildSections()V

    return-void
.end method

.method static synthetic access$200(Lcom/google/android/social/api/people/adapters/EditAudienceAdapter;)Lcom/google/android/social/api/people/adapters/EditAudienceAdapter$AudienceChangedListener;
    .locals 1
    .param p0    # Lcom/google/android/social/api/people/adapters/EditAudienceAdapter;

    iget-object v0, p0, Lcom/google/android/social/api/people/adapters/EditAudienceAdapter;->mListener:Lcom/google/android/social/api/people/adapters/EditAudienceAdapter$AudienceChangedListener;

    return-object v0
.end method

.method static synthetic access$300(Lcom/google/android/social/api/people/adapters/EditAudienceAdapter;)Ljava/util/HashSet;
    .locals 1
    .param p0    # Lcom/google/android/social/api/people/adapters/EditAudienceAdapter;

    iget-object v0, p0, Lcom/google/android/social/api/people/adapters/EditAudienceAdapter;->mSelectedAudience:Ljava/util/HashSet;

    return-object v0
.end method

.method protected static rebuildSections(Landroid/content/Context;Ljava/util/List;Ljava/util/List;Ljava/util/List;Ljava/util/List;)Ljava/util/List;
    .locals 17
    .param p0    # Landroid/content/Context;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Landroid/content/Context;",
            "Ljava/util/List",
            "<",
            "Lcom/google/android/social/api/people/model/AudienceMember;",
            ">;",
            "Ljava/util/List",
            "<",
            "Lcom/google/android/social/api/people/model/AudienceMember;",
            ">;",
            "Ljava/util/List",
            "<",
            "Lcom/google/android/social/api/people/model/AudienceMember;",
            ">;",
            "Ljava/util/List",
            "<",
            "Lcom/google/android/social/api/people/model/AudienceMember;",
            ">;)",
            "Ljava/util/List",
            "<",
            "Lcom/google/android/social/api/people/adapters/EditAudienceAdapter$Section;",
            ">;"
        }
    .end annotation

    new-instance v6, Ljava/util/ArrayList;

    invoke-direct {v6}, Ljava/util/ArrayList;-><init>()V

    new-instance v10, Ljava/util/ArrayList;

    invoke-direct {v10}, Ljava/util/ArrayList;-><init>()V

    if-eqz p3, :cond_0

    move-object/from16 v0, p3

    invoke-virtual {v10, v0}, Ljava/util/ArrayList;->addAll(Ljava/util/Collection;)Z

    :cond_0
    invoke-virtual {v10}, Ljava/util/ArrayList;->isEmpty()Z

    move-result v12

    if-nez v12, :cond_1

    sget-object v12, Lcom/google/android/social/api/people/adapters/EditAudienceAdapter;->AUDIENCE_COMPARER:Ljava/util/Comparator;

    invoke-static {v10, v12}, Ljava/util/Collections;->sort(Ljava/util/List;Ljava/util/Comparator;)V

    new-instance v12, Lcom/google/android/social/api/people/adapters/EditAudienceAdapter$Section;

    invoke-virtual/range {p0 .. p0}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v13

    const v14, 0x7f0700dc    # com.android.vending.R.string.plus_audience_selection_header_search

    invoke-virtual {v13, v14}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v13

    const-string v14, "\u2620"

    const/4 v15, 0x0

    invoke-direct {v12, v10, v13, v14, v15}, Lcom/google/android/social/api/people/adapters/EditAudienceAdapter$Section;-><init>(Ljava/util/List;Ljava/lang/String;Ljava/lang/String;Lcom/google/android/social/api/people/adapters/EditAudienceAdapter$1;)V

    invoke-virtual {v6, v12}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    :cond_1
    new-instance v1, Ljava/util/ArrayList;

    invoke-direct {v1}, Ljava/util/ArrayList;-><init>()V

    if-eqz p1, :cond_2

    move-object/from16 v0, p1

    invoke-virtual {v1, v0}, Ljava/util/ArrayList;->addAll(Ljava/util/Collection;)Z

    :cond_2
    if-eqz p2, :cond_3

    move-object/from16 v0, p2

    invoke-virtual {v1, v0}, Ljava/util/ArrayList;->addAll(Ljava/util/Collection;)Z

    :cond_3
    invoke-virtual {v1}, Ljava/util/ArrayList;->isEmpty()Z

    move-result v12

    if-nez v12, :cond_4

    new-instance v12, Lcom/google/android/social/api/people/adapters/EditAudienceAdapter$Section;

    invoke-virtual/range {p0 .. p0}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v13

    const v14, 0x7f0700dd    # com.android.vending.R.string.plus_audience_selection_header_circles

    invoke-virtual {v13, v14}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v13

    const-string v14, "\u25ef"

    const/4 v15, 0x0

    invoke-direct {v12, v1, v13, v14, v15}, Lcom/google/android/social/api/people/adapters/EditAudienceAdapter$Section;-><init>(Ljava/util/List;Ljava/lang/String;Ljava/lang/String;Lcom/google/android/social/api/people/adapters/EditAudienceAdapter$1;)V

    invoke-virtual {v6, v12}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    :cond_4
    new-instance v8, Ljava/util/TreeMap;

    invoke-direct {v8}, Ljava/util/TreeMap;-><init>()V

    if-eqz p4, :cond_7

    invoke-interface/range {p4 .. p4}, Ljava/util/List;->size()I

    move-result v7

    const/4 v2, 0x0

    :goto_0
    if-ge v2, v7, :cond_6

    move-object/from16 v0, p4

    invoke-interface {v0, v2}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v12

    check-cast v12, Lcom/google/android/social/api/people/model/AudienceMember;

    invoke-virtual {v12}, Lcom/google/android/social/api/people/model/AudienceMember;->getName()Ljava/lang/String;

    move-result-object v12

    const/4 v13, 0x0

    const/4 v14, 0x1

    invoke-virtual {v12, v13, v14}, Ljava/lang/String;->substring(II)Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v8, v4}, Ljava/util/TreeMap;->containsKey(Ljava/lang/Object;)Z

    move-result v12

    if-nez v12, :cond_5

    new-instance v12, Ljava/util/ArrayList;

    invoke-direct {v12}, Ljava/util/ArrayList;-><init>()V

    invoke-virtual {v8, v4, v12}, Ljava/util/TreeMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    :cond_5
    invoke-virtual {v8, v4}, Ljava/util/TreeMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v12

    check-cast v12, Ljava/util/ArrayList;

    move-object/from16 v0, p4

    invoke-interface {v0, v2}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v13

    invoke-virtual {v12, v13}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    add-int/lit8 v2, v2, 0x1

    goto :goto_0

    :cond_6
    invoke-virtual {v8}, Ljava/util/TreeMap;->entrySet()Ljava/util/Set;

    move-result-object v12

    invoke-interface {v12}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    move-result-object v3

    :goto_1
    invoke-interface {v3}, Ljava/util/Iterator;->hasNext()Z

    move-result v12

    if-eqz v12, :cond_7

    invoke-interface {v3}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v5

    check-cast v5, Ljava/util/Map$Entry;

    new-instance v14, Lcom/google/android/social/api/people/adapters/EditAudienceAdapter$Section;

    invoke-interface {v5}, Ljava/util/Map$Entry;->getValue()Ljava/lang/Object;

    move-result-object v12

    check-cast v12, Ljava/util/List;

    invoke-interface {v5}, Ljava/util/Map$Entry;->getKey()Ljava/lang/Object;

    move-result-object v13

    check-cast v13, Ljava/lang/String;

    const/4 v15, 0x0

    invoke-direct {v14, v12, v13, v15}, Lcom/google/android/social/api/people/adapters/EditAudienceAdapter$Section;-><init>(Ljava/util/List;Ljava/lang/String;Lcom/google/android/social/api/people/adapters/EditAudienceAdapter$1;)V

    invoke-virtual {v6, v14}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    goto :goto_1

    :cond_7
    const-string v12, "Audience"

    const/4 v13, 0x3

    invoke-static {v12, v13}, Landroid/util/Log;->isLoggable(Ljava/lang/String;I)Z

    move-result v12

    if-eqz v12, :cond_9

    const/4 v11, 0x0

    const/4 v9, 0x0

    const/4 v2, 0x0

    :goto_2
    invoke-virtual {v6}, Ljava/util/ArrayList;->size()I

    move-result v12

    if-ge v2, v12, :cond_8

    invoke-virtual {v6, v2}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v12

    check-cast v12, Lcom/google/android/social/api/people/adapters/EditAudienceAdapter$Section;

    invoke-virtual {v12}, Lcom/google/android/social/api/people/adapters/EditAudienceAdapter$Section;->getLength()I

    move-result v12

    add-int/2addr v11, v12

    invoke-virtual {v6, v2}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v12

    check-cast v12, Lcom/google/android/social/api/people/adapters/EditAudienceAdapter$Section;

    iget-object v12, v12, Lcom/google/android/social/api/people/adapters/EditAudienceAdapter$Section;->mAudience:Ljava/util/List;

    invoke-interface {v12}, Ljava/util/List;->size()I

    move-result v12

    add-int/2addr v9, v12

    add-int/lit8 v2, v2, 0x1

    goto :goto_2

    :cond_8
    const-string v12, "Audience"

    const-string v13, "Organized %d audience members (%d total items) into %d sections"

    const/4 v14, 0x3

    new-array v14, v14, [Ljava/lang/Object;

    const/4 v15, 0x0

    invoke-static {v9}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v16

    aput-object v16, v14, v15

    const/4 v15, 0x1

    invoke-static {v11}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v16

    aput-object v16, v14, v15

    const/4 v15, 0x2

    invoke-virtual {v6}, Ljava/util/ArrayList;->size()I

    move-result v16

    invoke-static/range {v16 .. v16}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v16

    aput-object v16, v14, v15

    invoke-static {v13, v14}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v13

    invoke-static {v12, v13}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    :cond_9
    invoke-virtual {v6}, Ljava/util/ArrayList;->size()I

    move-result v12

    const/4 v13, 0x1

    if-ne v12, v13, :cond_a

    const/4 v12, 0x0

    invoke-virtual {v6, v12}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v12

    check-cast v12, Lcom/google/android/social/api/people/adapters/EditAudienceAdapter$Section;

    invoke-virtual {v12}, Lcom/google/android/social/api/people/adapters/EditAudienceAdapter$Section;->hideHeader()V

    :cond_a
    return-object v6
.end method


# virtual methods
.method protected castOrInflate(Landroid/view/View;I)Landroid/view/View;
    .locals 3
    .param p1    # Landroid/view/View;
    .param p2    # I
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "<T:",
            "Landroid/view/View;",
            ">(",
            "Landroid/view/View;",
            "I)TT;"
        }
    .end annotation

    if-eqz p1, :cond_0

    :goto_0
    return-object p1

    :cond_0
    if-lez p2, :cond_1

    iget-object v1, p0, Lcom/google/android/social/api/people/adapters/EditAudienceAdapter;->mInflater:Landroid/view/LayoutInflater;

    const/4 v2, 0x0

    invoke-virtual {v1, p2, v2}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;)Landroid/view/View;

    move-result-object v0

    move-object p1, v0

    goto :goto_0

    :cond_1
    new-instance v1, Ljava/lang/IllegalStateException;

    const-string v2, "No usable view found and no layout provided"

    invoke-direct {v1, v2}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    throw v1
.end method

.method public getAudienceSelections()Ljava/util/ArrayList;
    .locals 8
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/ArrayList",
            "<",
            "Lcom/google/android/social/api/people/model/AudienceMember;",
            ">;"
        }
    .end annotation

    new-instance v3, Ljava/util/ArrayList;

    invoke-direct {v3}, Ljava/util/ArrayList;-><init>()V

    iget-object v7, p0, Lcom/google/android/social/api/people/adapters/EditAudienceAdapter;->mSections:Ljava/util/List;

    invoke-interface {v7}, Ljava/util/List;->size()I

    move-result v5

    const/4 v0, 0x0

    :goto_0
    if-ge v0, v5, :cond_2

    iget-object v7, p0, Lcom/google/android/social/api/people/adapters/EditAudienceAdapter;->mSections:Ljava/util/List;

    invoke-interface {v7, v0}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v4

    check-cast v4, Lcom/google/android/social/api/people/adapters/EditAudienceAdapter$Section;

    invoke-virtual {v4}, Lcom/google/android/social/api/people/adapters/EditAudienceAdapter$Section;->getLength()I

    move-result v6

    const/4 v2, 0x0

    :goto_1
    if-ge v2, v6, :cond_1

    invoke-virtual {v4, v2}, Lcom/google/android/social/api/people/adapters/EditAudienceAdapter$Section;->getItem(I)Ljava/lang/Object;

    move-result-object v1

    instance-of v7, v1, Lcom/google/android/social/api/people/model/AudienceMember;

    if-eqz v7, :cond_0

    iget-object v7, p0, Lcom/google/android/social/api/people/adapters/EditAudienceAdapter;->mSelectedAudience:Ljava/util/HashSet;

    invoke-virtual {v7, v1}, Ljava/util/HashSet;->contains(Ljava/lang/Object;)Z

    move-result v7

    if-eqz v7, :cond_0

    check-cast v1, Lcom/google/android/social/api/people/model/AudienceMember;

    invoke-virtual {v3, v1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    :cond_0
    add-int/lit8 v2, v2, 0x1

    goto :goto_1

    :cond_1
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    :cond_2
    return-object v3
.end method

.method protected getCircleView(Lcom/google/android/social/api/people/model/AudienceMember;Landroid/view/View;Landroid/view/ViewGroup;)Landroid/view/View;
    .locals 6
    .param p1    # Lcom/google/android/social/api/people/model/AudienceMember;
    .param p2    # Landroid/view/View;
    .param p3    # Landroid/view/ViewGroup;

    const v1, 0x7f0400ec    # com.android.vending.R.layout.plus_audience_selection_circle

    invoke-virtual {p0, p2, v1}, Lcom/google/android/social/api/people/adapters/EditAudienceAdapter;->castOrInflate(Landroid/view/View;I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/google/android/social/api/people/views/AudienceSelectionCircleView;

    sget-object v1, Lcom/google/android/social/api/people/model/AudienceMember;->PUBLIC:Lcom/google/android/social/api/people/model/AudienceMember;

    invoke-virtual {v1, p1}, Lcom/google/android/social/api/people/model/AudienceMember;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_2

    const v1, 0x7f0700d5    # com.android.vending.R.string.plus_chips_label_public

    invoke-virtual {v0, v1}, Lcom/google/android/social/api/people/views/AudienceSelectionCircleView;->setName(I)V

    const v1, 0x7f0200db    # com.android.vending.R.drawable.plus_list_public

    invoke-virtual {v0, v1}, Lcom/google/android/social/api/people/views/AudienceSelectionCircleView;->setIcon(I)V

    :cond_0
    :goto_0
    invoke-virtual {p1}, Lcom/google/android/social/api/people/model/AudienceMember;->getName()Ljava/lang/String;

    move-result-object v1

    if-eqz v1, :cond_1

    invoke-virtual {p1}, Lcom/google/android/social/api/people/model/AudienceMember;->getName()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/google/android/social/api/people/views/AudienceSelectionCircleView;->setName(Ljava/lang/String;)V

    :cond_1
    invoke-virtual {p1}, Lcom/google/android/social/api/people/model/AudienceMember;->getMemberCount()I

    move-result v1

    if-ltz v1, :cond_5

    iget-object v1, p0, Lcom/google/android/social/api/people/adapters/EditAudienceAdapter;->mContext:Landroid/content/Context;

    invoke-virtual {v1}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    const v2, 0x7f0700ed    # com.android.vending.R.string.plus_circle_size_pattern

    const/4 v3, 0x1

    new-array v3, v3, [Ljava/lang/Object;

    const/4 v4, 0x0

    invoke-virtual {p1}, Lcom/google/android/social/api/people/model/AudienceMember;->getMemberCount()I

    move-result v5

    invoke-static {v5}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v5

    aput-object v5, v3, v4

    invoke-virtual {v1, v2, v3}, Landroid/content/res/Resources;->getString(I[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/google/android/social/api/people/views/AudienceSelectionCircleView;->setCountText(Ljava/lang/String;)V

    :goto_1
    new-instance v1, Lcom/google/android/social/api/people/adapters/EditAudienceAdapter$CheckedListener;

    invoke-direct {v1, p0, p1}, Lcom/google/android/social/api/people/adapters/EditAudienceAdapter$CheckedListener;-><init>(Lcom/google/android/social/api/people/adapters/EditAudienceAdapter;Lcom/google/android/social/api/people/model/AudienceMember;)V

    invoke-virtual {v0, v1}, Lcom/google/android/social/api/people/views/AudienceSelectionCircleView;->setOnCheckedChangeListener(Landroid/widget/CompoundButton$OnCheckedChangeListener;)V

    invoke-virtual {p0, p1}, Lcom/google/android/social/api/people/adapters/EditAudienceAdapter;->isSelected(Lcom/google/android/social/api/people/model/AudienceMember;)Z

    move-result v1

    invoke-virtual {v0, v1}, Lcom/google/android/social/api/people/views/AudienceSelectionCircleView;->setChecked(Z)V

    return-object v0

    :cond_2
    sget-object v1, Lcom/google/android/social/api/people/model/AudienceMember;->YOUR_CIRCLES:Lcom/google/android/social/api/people/model/AudienceMember;

    invoke-virtual {v1, p1}, Lcom/google/android/social/api/people/model/AudienceMember;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_3

    const v1, 0x7f0700d6    # com.android.vending.R.string.plus_chips_label_your_circles

    invoke-virtual {v0, v1}, Lcom/google/android/social/api/people/views/AudienceSelectionCircleView;->setName(I)V

    goto :goto_0

    :cond_3
    sget-object v1, Lcom/google/android/social/api/people/model/AudienceMember;->EXTENDED_CIRCLES:Lcom/google/android/social/api/people/model/AudienceMember;

    invoke-virtual {v1, p1}, Lcom/google/android/social/api/people/model/AudienceMember;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_4

    const v1, 0x7f0700d7    # com.android.vending.R.string.plus_chips_label_extended_circles

    invoke-virtual {v0, v1}, Lcom/google/android/social/api/people/views/AudienceSelectionCircleView;->setName(I)V

    const v1, 0x7f0200da    # com.android.vending.R.drawable.plus_list_extended

    invoke-virtual {v0, v1}, Lcom/google/android/social/api/people/views/AudienceSelectionCircleView;->setIcon(I)V

    goto :goto_0

    :cond_4
    sget-object v1, Lcom/google/android/social/api/people/model/AudienceMember;->DOMAIN:Lcom/google/android/social/api/people/model/AudienceMember;

    invoke-virtual {v1, p1}, Lcom/google/android/social/api/people/model/AudienceMember;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_0

    const v1, 0x7f0200d9    # com.android.vending.R.drawable.plus_list_domain

    invoke-virtual {v0, v1}, Lcom/google/android/social/api/people/views/AudienceSelectionCircleView;->setIcon(I)V

    goto :goto_0

    :cond_5
    const-string v1, ""

    invoke-virtual {v0, v1}, Lcom/google/android/social/api/people/views/AudienceSelectionCircleView;->setCountText(Ljava/lang/String;)V

    goto :goto_1
.end method

.method public getCircles()Ljava/util/List;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/List",
            "<",
            "Lcom/google/android/social/api/people/model/AudienceMember;",
            ">;"
        }
    .end annotation

    iget-object v0, p0, Lcom/google/android/social/api/people/adapters/EditAudienceAdapter;->mCircles:Ljava/util/List;

    return-object v0
.end method

.method public getCount()I
    .locals 4

    const/4 v1, 0x0

    iget-object v3, p0, Lcom/google/android/social/api/people/adapters/EditAudienceAdapter;->mSections:Ljava/util/List;

    invoke-interface {v3}, Ljava/util/List;->size()I

    move-result v2

    const/4 v0, 0x0

    :goto_0
    if-ge v0, v2, :cond_0

    iget-object v3, p0, Lcom/google/android/social/api/people/adapters/EditAudienceAdapter;->mSections:Ljava/util/List;

    invoke-interface {v3, v0}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Lcom/google/android/social/api/people/adapters/EditAudienceAdapter$Section;

    invoke-virtual {v3}, Lcom/google/android/social/api/people/adapters/EditAudienceAdapter$Section;->getLength()I

    move-result v3

    add-int/2addr v1, v3

    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    :cond_0
    return v1
.end method

.method protected getGroupView(Lcom/google/android/social/api/people/model/AudienceMember;Landroid/view/View;Landroid/view/ViewGroup;)Landroid/view/View;
    .locals 1
    .param p1    # Lcom/google/android/social/api/people/model/AudienceMember;
    .param p2    # Landroid/view/View;
    .param p3    # Landroid/view/ViewGroup;

    invoke-virtual {p0, p1, p2, p3}, Lcom/google/android/social/api/people/adapters/EditAudienceAdapter;->getCircleView(Lcom/google/android/social/api/people/model/AudienceMember;Landroid/view/View;Landroid/view/ViewGroup;)Landroid/view/View;

    move-result-object v0

    return-object v0
.end method

.method protected getHeaderView(Ljava/lang/String;Landroid/view/View;Landroid/view/ViewGroup;)Landroid/view/View;
    .locals 2
    .param p1    # Ljava/lang/String;
    .param p2    # Landroid/view/View;
    .param p3    # Landroid/view/ViewGroup;

    const v1, 0x7f0400ed    # com.android.vending.R.layout.plus_audience_selection_header

    invoke-virtual {p0, p2, v1}, Lcom/google/android/social/api/people/adapters/EditAudienceAdapter;->castOrInflate(Landroid/view/View;I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/google/android/social/api/people/views/AudienceSelectionHeaderView;

    invoke-virtual {v0, p1}, Lcom/google/android/social/api/people/views/AudienceSelectionHeaderView;->setText(Ljava/lang/String;)V

    return-object v0
.end method

.method public getItem(I)Ljava/lang/Object;
    .locals 4
    .param p1    # I

    iget-object v3, p0, Lcom/google/android/social/api/people/adapters/EditAudienceAdapter;->mSections:Ljava/util/List;

    invoke-interface {v3}, Ljava/util/List;->size()I

    move-result v1

    const/4 v0, 0x0

    :goto_0
    if-ge v0, v1, :cond_1

    iget-object v3, p0, Lcom/google/android/social/api/people/adapters/EditAudienceAdapter;->mSections:Ljava/util/List;

    invoke-interface {v3, v0}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Lcom/google/android/social/api/people/adapters/EditAudienceAdapter$Section;

    invoke-virtual {v3}, Lcom/google/android/social/api/people/adapters/EditAudienceAdapter$Section;->getLength()I

    move-result v2

    if-ge p1, v2, :cond_0

    iget-object v3, p0, Lcom/google/android/social/api/people/adapters/EditAudienceAdapter;->mSections:Ljava/util/List;

    invoke-interface {v3, v0}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Lcom/google/android/social/api/people/adapters/EditAudienceAdapter$Section;

    invoke-virtual {v3, p1}, Lcom/google/android/social/api/people/adapters/EditAudienceAdapter$Section;->getItem(I)Ljava/lang/Object;

    move-result-object v3

    :goto_1
    return-object v3

    :cond_0
    sub-int/2addr p1, v2

    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    :cond_1
    const/4 v3, 0x0

    goto :goto_1
.end method

.method public getItemId(I)J
    .locals 2
    .param p1    # I

    int-to-long v0, p1

    return-wide v0
.end method

.method public getItemViewType(I)I
    .locals 4
    .param p1    # I

    invoke-virtual {p0, p1}, Lcom/google/android/social/api/people/adapters/EditAudienceAdapter;->getItem(I)Ljava/lang/Object;

    move-result-object v0

    instance-of v2, v0, Ljava/lang/String;

    if-eqz v2, :cond_0

    const/4 v2, 0x0

    :goto_0
    return v2

    :cond_0
    instance-of v2, v0, Lcom/google/android/social/api/people/model/AudienceMember;

    if-eqz v2, :cond_1

    move-object v1, v0

    check-cast v1, Lcom/google/android/social/api/people/model/AudienceMember;

    invoke-virtual {v1}, Lcom/google/android/social/api/people/model/AudienceMember;->getType()I

    move-result v2

    packed-switch v2, :pswitch_data_0

    :cond_1
    new-instance v2, Ljava/lang/IllegalStateException;

    const-string v3, "Unknown item in adapter"

    invoke-direct {v2, v3}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    throw v2

    :pswitch_0
    const/4 v2, 0x1

    goto :goto_0

    :pswitch_1
    const/4 v2, 0x2

    goto :goto_0

    :pswitch_2
    const/4 v2, 0x3

    goto :goto_0

    :pswitch_data_0
    .packed-switch -0x4
        :pswitch_2
        :pswitch_2
        :pswitch_1
        :pswitch_0
    .end packed-switch
.end method

.method public getPeople()Ljava/util/List;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/List",
            "<",
            "Lcom/google/android/social/api/people/model/AudienceMember;",
            ">;"
        }
    .end annotation

    iget-object v0, p0, Lcom/google/android/social/api/people/adapters/EditAudienceAdapter;->mPeople:Ljava/util/List;

    return-object v0
.end method

.method protected getPersonView(Lcom/google/android/social/api/people/model/AudienceMember;Landroid/view/View;Landroid/view/ViewGroup;)Landroid/view/View;
    .locals 2
    .param p1    # Lcom/google/android/social/api/people/model/AudienceMember;
    .param p2    # Landroid/view/View;
    .param p3    # Landroid/view/ViewGroup;

    const v1, 0x7f0400ee    # com.android.vending.R.layout.plus_audience_selection_person

    invoke-virtual {p0, p2, v1}, Lcom/google/android/social/api/people/adapters/EditAudienceAdapter;->castOrInflate(Landroid/view/View;I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/google/android/social/api/people/views/AudienceSelectionPersonView;

    invoke-virtual {p1}, Lcom/google/android/social/api/people/model/AudienceMember;->getName()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/google/android/social/api/people/views/AudienceSelectionPersonView;->setName(Ljava/lang/String;)V

    new-instance v1, Lcom/google/android/social/api/people/adapters/EditAudienceAdapter$CheckedListener;

    invoke-direct {v1, p0, p1}, Lcom/google/android/social/api/people/adapters/EditAudienceAdapter$CheckedListener;-><init>(Lcom/google/android/social/api/people/adapters/EditAudienceAdapter;Lcom/google/android/social/api/people/model/AudienceMember;)V

    invoke-virtual {v0, v1}, Lcom/google/android/social/api/people/views/AudienceSelectionPersonView;->setOnCheckedChangeListener(Landroid/widget/CompoundButton$OnCheckedChangeListener;)V

    iget-object v1, p0, Lcom/google/android/social/api/people/adapters/EditAudienceAdapter;->mSelectedAudience:Ljava/util/HashSet;

    invoke-virtual {v1, p1}, Ljava/util/HashSet;->contains(Ljava/lang/Object;)Z

    move-result v1

    invoke-virtual {v0, v1}, Lcom/google/android/social/api/people/views/AudienceSelectionPersonView;->setChecked(Z)V

    return-object v0
.end method

.method public getPositionForSection(I)I
    .locals 3
    .param p1    # I

    const/4 v1, 0x0

    const/4 v0, 0x0

    :goto_0
    if-ge v0, p1, :cond_1

    iget-object v2, p0, Lcom/google/android/social/api/people/adapters/EditAudienceAdapter;->mSections:Ljava/util/List;

    invoke-interface {v2, v0}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lcom/google/android/social/api/people/adapters/EditAudienceAdapter$Section;

    iget-object v2, v2, Lcom/google/android/social/api/people/adapters/EditAudienceAdapter$Section;->mAudience:Ljava/util/List;

    invoke-interface {v2}, Ljava/util/List;->size()I

    move-result v2

    if-lez v2, :cond_0

    iget-object v2, p0, Lcom/google/android/social/api/people/adapters/EditAudienceAdapter;->mSections:Ljava/util/List;

    invoke-interface {v2, v0}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lcom/google/android/social/api/people/adapters/EditAudienceAdapter$Section;

    invoke-virtual {v2}, Lcom/google/android/social/api/people/adapters/EditAudienceAdapter$Section;->getLength()I

    move-result v2

    add-int/2addr v1, v2

    :cond_0
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    :cond_1
    return v1
.end method

.method protected getSearchResults()Ljava/util/List;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/List",
            "<",
            "Lcom/google/android/social/api/people/model/AudienceMember;",
            ">;"
        }
    .end annotation

    iget-object v0, p0, Lcom/google/android/social/api/people/adapters/EditAudienceAdapter;->mSearchResults:Ljava/util/List;

    return-object v0
.end method

.method public getSectionForPosition(I)I
    .locals 3
    .param p1    # I

    iget-object v2, p0, Lcom/google/android/social/api/people/adapters/EditAudienceAdapter;->mSections:Ljava/util/List;

    invoke-interface {v2}, Ljava/util/List;->size()I

    move-result v1

    const/4 v0, 0x0

    :goto_0
    if-ge v0, v1, :cond_1

    iget-object v2, p0, Lcom/google/android/social/api/people/adapters/EditAudienceAdapter;->mSections:Ljava/util/List;

    invoke-interface {v2, v0}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lcom/google/android/social/api/people/adapters/EditAudienceAdapter$Section;

    invoke-virtual {v2}, Lcom/google/android/social/api/people/adapters/EditAudienceAdapter$Section;->getLength()I

    move-result v2

    sub-int/2addr p1, v2

    if-gez p1, :cond_0

    :goto_1
    return v0

    :cond_0
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    :cond_1
    const/4 v0, -0x1

    goto :goto_1
.end method

.method public getSections()[Ljava/lang/Object;
    .locals 1

    iget-object v0, p0, Lcom/google/android/social/api/people/adapters/EditAudienceAdapter;->mSections:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->toArray()[Ljava/lang/Object;

    move-result-object v0

    return-object v0
.end method

.method protected getSystemGroups()Ljava/util/List;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/List",
            "<",
            "Lcom/google/android/social/api/people/model/AudienceMember;",
            ">;"
        }
    .end annotation

    iget-object v0, p0, Lcom/google/android/social/api/people/adapters/EditAudienceAdapter;->mSystemGroups:Ljava/util/List;

    return-object v0
.end method

.method public getView(ILandroid/view/View;Landroid/view/ViewGroup;)Landroid/view/View;
    .locals 4
    .param p1    # I
    .param p2    # Landroid/view/View;
    .param p3    # Landroid/view/ViewGroup;

    invoke-virtual {p0, p1}, Lcom/google/android/social/api/people/adapters/EditAudienceAdapter;->getItem(I)Ljava/lang/Object;

    move-result-object v0

    invoke-virtual {p0, p1}, Lcom/google/android/social/api/people/adapters/EditAudienceAdapter;->getItemViewType(I)I

    move-result v1

    packed-switch v1, :pswitch_data_0

    new-instance v1, Ljava/lang/RuntimeException;

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "Unexpected item type: "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-direct {v1, v2}, Ljava/lang/RuntimeException;-><init>(Ljava/lang/String;)V

    throw v1

    :pswitch_0
    check-cast v0, Ljava/lang/String;

    invoke-virtual {p0, v0, p2, p3}, Lcom/google/android/social/api/people/adapters/EditAudienceAdapter;->getHeaderView(Ljava/lang/String;Landroid/view/View;Landroid/view/ViewGroup;)Landroid/view/View;

    move-result-object v1

    :goto_0
    return-object v1

    :pswitch_1
    check-cast v0, Lcom/google/android/social/api/people/model/AudienceMember;

    invoke-virtual {p0, v0, p2, p3}, Lcom/google/android/social/api/people/adapters/EditAudienceAdapter;->getGroupView(Lcom/google/android/social/api/people/model/AudienceMember;Landroid/view/View;Landroid/view/ViewGroup;)Landroid/view/View;

    move-result-object v1

    goto :goto_0

    :pswitch_2
    check-cast v0, Lcom/google/android/social/api/people/model/AudienceMember;

    invoke-virtual {p0, v0, p2, p3}, Lcom/google/android/social/api/people/adapters/EditAudienceAdapter;->getCircleView(Lcom/google/android/social/api/people/model/AudienceMember;Landroid/view/View;Landroid/view/ViewGroup;)Landroid/view/View;

    move-result-object v1

    goto :goto_0

    :pswitch_3
    check-cast v0, Lcom/google/android/social/api/people/model/AudienceMember;

    invoke-virtual {p0, v0, p2, p3}, Lcom/google/android/social/api/people/adapters/EditAudienceAdapter;->getPersonView(Lcom/google/android/social/api/people/model/AudienceMember;Landroid/view/View;Landroid/view/ViewGroup;)Landroid/view/View;

    move-result-object v1

    goto :goto_0

    :pswitch_data_0
    .packed-switch 0x0
        :pswitch_0
        :pswitch_1
        :pswitch_2
        :pswitch_3
    .end packed-switch
.end method

.method public getViewTypeCount()I
    .locals 1

    const/4 v0, 0x4

    return v0
.end method

.method public hasStableIds()Z
    .locals 1

    const/4 v0, 0x0

    return v0
.end method

.method public isSelected(Lcom/google/android/social/api/people/model/AudienceMember;)Z
    .locals 1
    .param p1    # Lcom/google/android/social/api/people/model/AudienceMember;

    iget-object v0, p0, Lcom/google/android/social/api/people/adapters/EditAudienceAdapter;->mSelectedAudience:Ljava/util/HashSet;

    invoke-virtual {v0, p1}, Ljava/util/HashSet;->contains(Ljava/lang/Object;)Z

    move-result v0

    return v0
.end method

.method rebuildSections()V
    .locals 5

    iget-object v0, p0, Lcom/google/android/social/api/people/adapters/EditAudienceAdapter;->mContext:Landroid/content/Context;

    invoke-virtual {p0}, Lcom/google/android/social/api/people/adapters/EditAudienceAdapter;->getSystemGroups()Ljava/util/List;

    move-result-object v1

    invoke-virtual {p0}, Lcom/google/android/social/api/people/adapters/EditAudienceAdapter;->getCircles()Ljava/util/List;

    move-result-object v2

    invoke-virtual {p0}, Lcom/google/android/social/api/people/adapters/EditAudienceAdapter;->getSearchResults()Ljava/util/List;

    move-result-object v3

    invoke-virtual {p0}, Lcom/google/android/social/api/people/adapters/EditAudienceAdapter;->getPeople()Ljava/util/List;

    move-result-object v4

    invoke-static {v0, v1, v2, v3, v4}, Lcom/google/android/social/api/people/adapters/EditAudienceAdapter;->rebuildSections(Landroid/content/Context;Ljava/util/List;Ljava/util/List;Ljava/util/List;Ljava/util/List;)Ljava/util/List;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/social/api/people/adapters/EditAudienceAdapter;->mSections:Ljava/util/List;

    invoke-virtual {p0}, Lcom/google/android/social/api/people/adapters/EditAudienceAdapter;->notifyDataSetChanged()V

    return-void
.end method

.method public setAudienceChangedListener(Lcom/google/android/social/api/people/adapters/EditAudienceAdapter$AudienceChangedListener;)V
    .locals 0
    .param p1    # Lcom/google/android/social/api/people/adapters/EditAudienceAdapter$AudienceChangedListener;

    iput-object p1, p0, Lcom/google/android/social/api/people/adapters/EditAudienceAdapter;->mListener:Lcom/google/android/social/api/people/adapters/EditAudienceAdapter$AudienceChangedListener;

    return-void
.end method

.method public setAudienceSelections(Ljava/util/Collection;)V
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/Collection",
            "<",
            "Lcom/google/android/social/api/people/model/AudienceMember;",
            ">;)V"
        }
    .end annotation

    iget-object v0, p0, Lcom/google/android/social/api/people/adapters/EditAudienceAdapter;->mSelectedAudience:Ljava/util/HashSet;

    invoke-virtual {v0}, Ljava/util/HashSet;->clear()V

    iget-object v0, p0, Lcom/google/android/social/api/people/adapters/EditAudienceAdapter;->mSelectedAudience:Ljava/util/HashSet;

    invoke-virtual {v0, p1}, Ljava/util/HashSet;->addAll(Ljava/util/Collection;)Z

    invoke-virtual {p0}, Lcom/google/android/social/api/people/adapters/EditAudienceAdapter;->notifyDataSetChanged()V

    return-void
.end method

.method protected swapCircles(Ljava/util/List;)V
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List",
            "<",
            "Lcom/google/android/social/api/people/model/AudienceMember;",
            ">;)V"
        }
    .end annotation

    if-eqz p1, :cond_0

    iget-object v0, p0, Lcom/google/android/social/api/people/adapters/EditAudienceAdapter;->mCircles:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->clear()V

    iget-object v0, p0, Lcom/google/android/social/api/people/adapters/EditAudienceAdapter;->mCircles:Ljava/util/List;

    invoke-interface {v0, p1}, Ljava/util/List;->addAll(Ljava/util/Collection;)Z

    :cond_0
    return-void
.end method

.method public swapContents(Ljava/util/List;Ljava/util/List;Ljava/util/List;Ljava/util/List;)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List",
            "<",
            "Lcom/google/android/social/api/people/model/AudienceMember;",
            ">;",
            "Ljava/util/List",
            "<",
            "Lcom/google/android/social/api/people/model/AudienceMember;",
            ">;",
            "Ljava/util/List",
            "<",
            "Lcom/google/android/social/api/people/model/AudienceMember;",
            ">;",
            "Ljava/util/List",
            "<",
            "Lcom/google/android/social/api/people/model/AudienceMember;",
            ">;)V"
        }
    .end annotation

    invoke-virtual {p0, p1}, Lcom/google/android/social/api/people/adapters/EditAudienceAdapter;->swapGroups(Ljava/util/List;)V

    invoke-virtual {p0, p2}, Lcom/google/android/social/api/people/adapters/EditAudienceAdapter;->swapCircles(Ljava/util/List;)V

    invoke-virtual {p0, p3}, Lcom/google/android/social/api/people/adapters/EditAudienceAdapter;->swapPeople(Ljava/util/List;)V

    invoke-virtual {p0, p4}, Lcom/google/android/social/api/people/adapters/EditAudienceAdapter;->swapSearchResults(Ljava/util/List;)V

    invoke-virtual {p0}, Lcom/google/android/social/api/people/adapters/EditAudienceAdapter;->rebuildSections()V

    return-void
.end method

.method protected swapGroups(Ljava/util/List;)V
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List",
            "<",
            "Lcom/google/android/social/api/people/model/AudienceMember;",
            ">;)V"
        }
    .end annotation

    if-eqz p1, :cond_0

    iget-object v0, p0, Lcom/google/android/social/api/people/adapters/EditAudienceAdapter;->mSystemGroups:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->clear()V

    iget-object v0, p0, Lcom/google/android/social/api/people/adapters/EditAudienceAdapter;->mSystemGroups:Ljava/util/List;

    invoke-interface {v0, p1}, Ljava/util/List;->addAll(Ljava/util/Collection;)Z

    :cond_0
    return-void
.end method

.method protected swapPeople(Ljava/util/List;)V
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List",
            "<",
            "Lcom/google/android/social/api/people/model/AudienceMember;",
            ">;)V"
        }
    .end annotation

    if-eqz p1, :cond_0

    iget-object v0, p0, Lcom/google/android/social/api/people/adapters/EditAudienceAdapter;->mPeople:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->clear()V

    iget-object v0, p0, Lcom/google/android/social/api/people/adapters/EditAudienceAdapter;->mPeople:Ljava/util/List;

    invoke-interface {v0, p1}, Ljava/util/List;->addAll(Ljava/util/Collection;)Z

    :cond_0
    return-void
.end method

.method protected swapSearchResults(Ljava/util/List;)V
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List",
            "<",
            "Lcom/google/android/social/api/people/model/AudienceMember;",
            ">;)V"
        }
    .end annotation

    if-eqz p1, :cond_0

    iget-object v0, p0, Lcom/google/android/social/api/people/adapters/EditAudienceAdapter;->mSearchResults:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->clear()V

    iget-object v0, p0, Lcom/google/android/social/api/people/adapters/EditAudienceAdapter;->mSearchResults:Ljava/util/List;

    invoke-interface {v0, p1}, Ljava/util/List;->addAll(Ljava/util/Collection;)Z

    :cond_0
    return-void
.end method
