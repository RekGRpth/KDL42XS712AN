.class Lcom/google/android/social/api/people/activities/PeopleSuggestActivity$LogItemAcceptedClientRequest;
.super Ljava/lang/Object;
.source "PeopleSuggestActivity.java"

# interfaces
.implements Lcom/google/android/social/api/people/activities/PeopleSuggestActivity$PlusInternalClientRequest;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/google/android/social/api/people/activities/PeopleSuggestActivity;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x2
    name = "LogItemAcceptedClientRequest"
.end annotation


# instance fields
.field private final personId:Ljava/lang/String;

.field final synthetic this$0:Lcom/google/android/social/api/people/activities/PeopleSuggestActivity;


# direct methods
.method public constructor <init>(Lcom/google/android/social/api/people/activities/PeopleSuggestActivity;Ljava/lang/String;)V
    .locals 0
    .param p2    # Ljava/lang/String;

    iput-object p1, p0, Lcom/google/android/social/api/people/activities/PeopleSuggestActivity$LogItemAcceptedClientRequest;->this$0:Lcom/google/android/social/api/people/activities/PeopleSuggestActivity;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    iput-object p2, p0, Lcom/google/android/social/api/people/activities/PeopleSuggestActivity$LogItemAcceptedClientRequest;->personId:Ljava/lang/String;

    return-void
.end method


# virtual methods
.method public execute(Lcom/google/android/social/api/people/activities/PeopleSuggestActivity;Lcom/google/android/social/api/service/PlusInternalClient;)V
    .locals 2
    .param p1    # Lcom/google/android/social/api/people/activities/PeopleSuggestActivity;
    .param p2    # Lcom/google/android/social/api/service/PlusInternalClient;

    sget-object v0, Lcom/google/android/social/api/logging/Rhs$Action;->ITEM_ACCEPTED:Lcom/google/api/services/plus/model/FavaDiagnosticsNamespacedType;

    iget-object v1, p0, Lcom/google/android/social/api/people/activities/PeopleSuggestActivity$LogItemAcceptedClientRequest;->personId:Ljava/lang/String;

    invoke-virtual {p2, p1, v0, v1}, Lcom/google/android/social/api/service/PlusInternalClient;->postInsertLogPeopleSuggestionAction(Lcom/google/android/social/api/operations/PostInsertLogOperation$Callback;Lcom/google/api/services/plus/model/FavaDiagnosticsNamespacedType;Ljava/lang/String;)V

    return-void
.end method
