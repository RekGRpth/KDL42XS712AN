.class Lcom/google/android/social/api/people/operations/PeopleSearchOperation$PeopleSearchRequest;
.super Lcom/google/android/social/api/network/ApiaryJsonHttpRequest;
.source "PeopleSearchOperation.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/google/android/social/api/people/operations/PeopleSearchOperation;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x2
    name = "PeopleSearchRequest"
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/google/android/social/api/network/ApiaryJsonHttpRequest",
        "<",
        "Lcom/google/api/services/plus/model/PeopleFeed;",
        "Lcom/google/android/apps/plus/json/GenericJson;",
        "Lcom/google/api/services/plus/model/PeopleFeed;",
        ">;"
    }
.end annotation


# instance fields
.field final synthetic this$0:Lcom/google/android/social/api/people/operations/PeopleSearchOperation;


# direct methods
.method public constructor <init>(Lcom/google/android/social/api/people/operations/PeopleSearchOperation;Landroid/content/Context;Ljava/lang/String;Ljava/lang/String;)V
    .locals 1
    .param p2    # Landroid/content/Context;
    .param p3    # Ljava/lang/String;
    .param p4    # Ljava/lang/String;

    iput-object p1, p0, Lcom/google/android/social/api/people/operations/PeopleSearchOperation$PeopleSearchRequest;->this$0:Lcom/google/android/social/api/people/operations/PeopleSearchOperation;

    invoke-static {p4}, Lcom/google/android/social/api/network/ApiaryConfig;->getSearchQuery(Ljava/lang/String;)Lcom/google/android/social/api/network/ApiaryJsonHttpRequest$JsonConfiguration;

    move-result-object v0

    invoke-direct {p0, p2, p3, v0}, Lcom/google/android/social/api/network/ApiaryJsonHttpRequest;-><init>(Landroid/content/Context;Ljava/lang/String;Lcom/google/android/social/api/network/ApiaryJsonHttpRequest$JsonConfiguration;)V

    return-void
.end method


# virtual methods
.method protected processResponseData(Lcom/google/api/services/plus/model/PeopleFeed;)Lcom/google/api/services/plus/model/PeopleFeed;
    .locals 0
    .param p1    # Lcom/google/api/services/plus/model/PeopleFeed;

    return-object p1
.end method

.method protected bridge synthetic processResponseData(Lcom/google/android/apps/plus/json/GenericJson;)Ljava/lang/Object;
    .locals 1
    .param p1    # Lcom/google/android/apps/plus/json/GenericJson;

    check-cast p1, Lcom/google/api/services/plus/model/PeopleFeed;

    invoke-virtual {p0, p1}, Lcom/google/android/social/api/people/operations/PeopleSearchOperation$PeopleSearchRequest;->processResponseData(Lcom/google/api/services/plus/model/PeopleFeed;)Lcom/google/api/services/plus/model/PeopleFeed;

    move-result-object v0

    return-object v0
.end method
