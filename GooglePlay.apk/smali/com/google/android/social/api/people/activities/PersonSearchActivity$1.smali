.class Lcom/google/android/social/api/people/activities/PersonSearchActivity$1;
.super Ljava/lang/Object;
.source "PersonSearchActivity.java"

# interfaces
.implements Landroid/text/TextWatcher;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/google/android/social/api/people/activities/PersonSearchActivity;->onCreate(Landroid/os/Bundle;)V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/google/android/social/api/people/activities/PersonSearchActivity;


# direct methods
.method constructor <init>(Lcom/google/android/social/api/people/activities/PersonSearchActivity;)V
    .locals 0

    iput-object p1, p0, Lcom/google/android/social/api/people/activities/PersonSearchActivity$1;->this$0:Lcom/google/android/social/api/people/activities/PersonSearchActivity;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public afterTextChanged(Landroid/text/Editable;)V
    .locals 0
    .param p1    # Landroid/text/Editable;

    return-void
.end method

.method public beforeTextChanged(Ljava/lang/CharSequence;III)V
    .locals 0
    .param p1    # Ljava/lang/CharSequence;
    .param p2    # I
    .param p3    # I
    .param p4    # I

    return-void
.end method

.method public onTextChanged(Ljava/lang/CharSequence;III)V
    .locals 2
    .param p1    # Ljava/lang/CharSequence;
    .param p2    # I
    .param p3    # I
    .param p4    # I

    invoke-virtual {p1}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/String;->trim()Ljava/lang/String;

    move-result-object v0

    iget-object v1, p0, Lcom/google/android/social/api/people/activities/PersonSearchActivity$1;->this$0:Lcom/google/android/social/api/people/activities/PersonSearchActivity;

    # getter for: Lcom/google/android/social/api/people/activities/PersonSearchActivity;->currentSearchQuery:Ljava/lang/String;
    invoke-static {v1}, Lcom/google/android/social/api/people/activities/PersonSearchActivity;->access$000(Lcom/google/android/social/api/people/activities/PersonSearchActivity;)Ljava/lang/String;

    move-result-object v1

    if-nez v1, :cond_0

    iget-object v1, p0, Lcom/google/android/social/api/people/activities/PersonSearchActivity$1;->this$0:Lcom/google/android/social/api/people/activities/PersonSearchActivity;

    # invokes: Lcom/google/android/social/api/people/activities/PersonSearchActivity;->search(Ljava/lang/String;)V
    invoke-static {v1, v0}, Lcom/google/android/social/api/people/activities/PersonSearchActivity;->access$100(Lcom/google/android/social/api/people/activities/PersonSearchActivity;Ljava/lang/String;)V

    :goto_0
    return-void

    :cond_0
    iget-object v1, p0, Lcom/google/android/social/api/people/activities/PersonSearchActivity$1;->this$0:Lcom/google/android/social/api/people/activities/PersonSearchActivity;

    # setter for: Lcom/google/android/social/api/people/activities/PersonSearchActivity;->pendingSearchQuery:Ljava/lang/String;
    invoke-static {v1, v0}, Lcom/google/android/social/api/people/activities/PersonSearchActivity;->access$202(Lcom/google/android/social/api/people/activities/PersonSearchActivity;Ljava/lang/String;)Ljava/lang/String;

    goto :goto_0
.end method
