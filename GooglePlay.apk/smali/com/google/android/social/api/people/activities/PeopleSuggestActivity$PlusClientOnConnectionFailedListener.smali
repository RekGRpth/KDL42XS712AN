.class Lcom/google/android/social/api/people/activities/PeopleSuggestActivity$PlusClientOnConnectionFailedListener;
.super Ljava/lang/Object;
.source "PeopleSuggestActivity.java"

# interfaces
.implements Lcom/google/android/gms/common/GooglePlayServicesClient$OnConnectionFailedListener;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/google/android/social/api/people/activities/PeopleSuggestActivity;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x2
    name = "PlusClientOnConnectionFailedListener"
.end annotation


# instance fields
.field final synthetic this$0:Lcom/google/android/social/api/people/activities/PeopleSuggestActivity;


# direct methods
.method private constructor <init>(Lcom/google/android/social/api/people/activities/PeopleSuggestActivity;)V
    .locals 0

    iput-object p1, p0, Lcom/google/android/social/api/people/activities/PeopleSuggestActivity$PlusClientOnConnectionFailedListener;->this$0:Lcom/google/android/social/api/people/activities/PeopleSuggestActivity;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method synthetic constructor <init>(Lcom/google/android/social/api/people/activities/PeopleSuggestActivity;Lcom/google/android/social/api/people/activities/PeopleSuggestActivity$1;)V
    .locals 0
    .param p1    # Lcom/google/android/social/api/people/activities/PeopleSuggestActivity;
    .param p2    # Lcom/google/android/social/api/people/activities/PeopleSuggestActivity$1;

    invoke-direct {p0, p1}, Lcom/google/android/social/api/people/activities/PeopleSuggestActivity$PlusClientOnConnectionFailedListener;-><init>(Lcom/google/android/social/api/people/activities/PeopleSuggestActivity;)V

    return-void
.end method


# virtual methods
.method public onConnectionFailed(Lcom/google/android/gms/common/ConnectionResult;)V
    .locals 3
    .param p1    # Lcom/google/android/gms/common/ConnectionResult;

    invoke-virtual {p1}, Lcom/google/android/gms/common/ConnectionResult;->hasResolution()Z

    move-result v1

    if-eqz v1, :cond_0

    :try_start_0
    iget-object v1, p0, Lcom/google/android/social/api/people/activities/PeopleSuggestActivity$PlusClientOnConnectionFailedListener;->this$0:Lcom/google/android/social/api/people/activities/PeopleSuggestActivity;

    const/16 v2, 0x7d2

    invoke-virtual {p1, v1, v2}, Lcom/google/android/gms/common/ConnectionResult;->startResolutionForResult(Landroid/app/Activity;I)V
    :try_end_0
    .catch Landroid/content/IntentSender$SendIntentException; {:try_start_0 .. :try_end_0} :catch_0

    :goto_0
    return-void

    :catch_0
    move-exception v0

    iget-object v1, p0, Lcom/google/android/social/api/people/activities/PeopleSuggestActivity$PlusClientOnConnectionFailedListener;->this$0:Lcom/google/android/social/api/people/activities/PeopleSuggestActivity;

    # getter for: Lcom/google/android/social/api/people/activities/PeopleSuggestActivity;->plusClient:Lcom/google/android/gms/plus/PlusClient;
    invoke-static {v1}, Lcom/google/android/social/api/people/activities/PeopleSuggestActivity;->access$300(Lcom/google/android/social/api/people/activities/PeopleSuggestActivity;)Lcom/google/android/gms/plus/PlusClient;

    move-result-object v1

    invoke-virtual {v1}, Lcom/google/android/gms/plus/PlusClient;->connect()V

    goto :goto_0

    :cond_0
    iget-object v1, p0, Lcom/google/android/social/api/people/activities/PeopleSuggestActivity$PlusClientOnConnectionFailedListener;->this$0:Lcom/google/android/social/api/people/activities/PeopleSuggestActivity;

    # getter for: Lcom/google/android/social/api/people/activities/PeopleSuggestActivity;->handler:Lcom/google/android/social/api/people/activities/PeopleSuggestHandler;
    invoke-static {v1}, Lcom/google/android/social/api/people/activities/PeopleSuggestActivity;->access$400(Lcom/google/android/social/api/people/activities/PeopleSuggestActivity;)Lcom/google/android/social/api/people/activities/PeopleSuggestHandler;

    move-result-object v1

    iget-object v2, p0, Lcom/google/android/social/api/people/activities/PeopleSuggestActivity$PlusClientOnConnectionFailedListener;->this$0:Lcom/google/android/social/api/people/activities/PeopleSuggestActivity;

    invoke-virtual {v1, v2}, Lcom/google/android/social/api/people/activities/PeopleSuggestHandler;->sendShowErrorDialog(Lcom/google/android/social/api/people/activities/PeopleSuggestActivity;)V

    goto :goto_0
.end method
