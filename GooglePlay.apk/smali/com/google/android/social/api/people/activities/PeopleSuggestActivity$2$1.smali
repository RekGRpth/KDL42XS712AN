.class Lcom/google/android/social/api/people/activities/PeopleSuggestActivity$2$1;
.super Ljava/lang/Object;
.source "PeopleSuggestActivity.java"

# interfaces
.implements Ljava/lang/Runnable;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/google/android/social/api/people/activities/PeopleSuggestActivity$2;->onAnimationEnd(Landroid/view/animation/Animation;)V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$1:Lcom/google/android/social/api/people/activities/PeopleSuggestActivity$2;


# direct methods
.method constructor <init>(Lcom/google/android/social/api/people/activities/PeopleSuggestActivity$2;)V
    .locals 0

    iput-object p1, p0, Lcom/google/android/social/api/people/activities/PeopleSuggestActivity$2$1;->this$1:Lcom/google/android/social/api/people/activities/PeopleSuggestActivity$2;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public run()V
    .locals 3

    const/4 v0, 0x0

    :goto_0
    iget-object v1, p0, Lcom/google/android/social/api/people/activities/PeopleSuggestActivity$2$1;->this$1:Lcom/google/android/social/api/people/activities/PeopleSuggestActivity$2;

    iget-object v1, v1, Lcom/google/android/social/api/people/activities/PeopleSuggestActivity$2;->this$0:Lcom/google/android/social/api/people/activities/PeopleSuggestActivity;

    # getter for: Lcom/google/android/social/api/people/activities/PeopleSuggestActivity;->adapter:Lcom/google/android/social/api/people/adapters/PeopleSuggestAdapter;
    invoke-static {v1}, Lcom/google/android/social/api/people/activities/PeopleSuggestActivity;->access$500(Lcom/google/android/social/api/people/activities/PeopleSuggestActivity;)Lcom/google/android/social/api/people/adapters/PeopleSuggestAdapter;

    move-result-object v1

    invoke-virtual {v1}, Lcom/google/android/social/api/people/adapters/PeopleSuggestAdapter;->getCount()I

    move-result v1

    if-ge v0, v1, :cond_0

    iget-object v1, p0, Lcom/google/android/social/api/people/activities/PeopleSuggestActivity$2$1;->this$1:Lcom/google/android/social/api/people/activities/PeopleSuggestActivity$2;

    iget-object v1, v1, Lcom/google/android/social/api/people/activities/PeopleSuggestActivity$2;->this$0:Lcom/google/android/social/api/people/activities/PeopleSuggestActivity;

    # getter for: Lcom/google/android/social/api/people/activities/PeopleSuggestActivity;->adapter:Lcom/google/android/social/api/people/adapters/PeopleSuggestAdapter;
    invoke-static {v1}, Lcom/google/android/social/api/people/activities/PeopleSuggestActivity;->access$500(Lcom/google/android/social/api/people/activities/PeopleSuggestActivity;)Lcom/google/android/social/api/people/adapters/PeopleSuggestAdapter;

    move-result-object v1

    invoke-virtual {v1, v0}, Lcom/google/android/social/api/people/adapters/PeopleSuggestAdapter;->getItem(I)Lcom/google/android/social/api/people/model/Person;

    move-result-object v1

    iget-object v2, p0, Lcom/google/android/social/api/people/activities/PeopleSuggestActivity$2$1;->this$1:Lcom/google/android/social/api/people/activities/PeopleSuggestActivity$2;

    iget-object v2, v2, Lcom/google/android/social/api/people/activities/PeopleSuggestActivity$2;->val$person:Lcom/google/android/social/api/people/model/Person;

    if-ne v1, v2, :cond_1

    iget-object v1, p0, Lcom/google/android/social/api/people/activities/PeopleSuggestActivity$2$1;->this$1:Lcom/google/android/social/api/people/activities/PeopleSuggestActivity$2;

    iget-object v1, v1, Lcom/google/android/social/api/people/activities/PeopleSuggestActivity$2;->this$0:Lcom/google/android/social/api/people/activities/PeopleSuggestActivity;

    # getter for: Lcom/google/android/social/api/people/activities/PeopleSuggestActivity;->adapter:Lcom/google/android/social/api/people/adapters/PeopleSuggestAdapter;
    invoke-static {v1}, Lcom/google/android/social/api/people/activities/PeopleSuggestActivity;->access$500(Lcom/google/android/social/api/people/activities/PeopleSuggestActivity;)Lcom/google/android/social/api/people/adapters/PeopleSuggestAdapter;

    move-result-object v1

    invoke-virtual {v1, v0}, Lcom/google/android/social/api/people/adapters/PeopleSuggestAdapter;->remove(I)V

    :cond_0
    return-void

    :cond_1
    add-int/lit8 v0, v0, 0x1

    goto :goto_0
.end method
