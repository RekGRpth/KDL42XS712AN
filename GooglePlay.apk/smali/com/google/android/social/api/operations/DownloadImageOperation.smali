.class public Lcom/google/android/social/api/operations/DownloadImageOperation;
.super Ljava/lang/Object;
.source "DownloadImageOperation.java"

# interfaces
.implements Lcom/google/android/social/api/operations/PlusApiOperation;


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/google/android/social/api/operations/DownloadImageOperation$Callback;
    }
.end annotation


# instance fields
.field private final callback:Lcom/google/android/social/api/operations/DownloadImageOperation$Callback;

.field private final height:I

.field private final url:Ljava/lang/String;

.field private final width:I


# direct methods
.method public constructor <init>(Lcom/google/android/social/api/operations/DownloadImageOperation$Callback;Ljava/lang/String;II)V
    .locals 0
    .param p1    # Lcom/google/android/social/api/operations/DownloadImageOperation$Callback;
    .param p2    # Ljava/lang/String;
    .param p3    # I
    .param p4    # I

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    iput-object p1, p0, Lcom/google/android/social/api/operations/DownloadImageOperation;->callback:Lcom/google/android/social/api/operations/DownloadImageOperation$Callback;

    iput-object p2, p0, Lcom/google/android/social/api/operations/DownloadImageOperation;->url:Ljava/lang/String;

    iput p3, p0, Lcom/google/android/social/api/operations/DownloadImageOperation;->width:I

    iput p4, p0, Lcom/google/android/social/api/operations/DownloadImageOperation;->height:I

    return-void
.end method


# virtual methods
.method public execute(Landroid/content/Context;Lcom/android/volley/RequestQueue;)V
    .locals 9
    .param p1    # Landroid/content/Context;
    .param p2    # Lcom/android/volley/RequestQueue;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/lang/InterruptedException;,
            Ljava/util/concurrent/ExecutionException;
        }
    .end annotation

    iget v3, p0, Lcom/google/android/social/api/operations/DownloadImageOperation;->width:I

    iget v4, p0, Lcom/google/android/social/api/operations/DownloadImageOperation;->height:I

    iget-object v5, p0, Lcom/google/android/social/api/operations/DownloadImageOperation;->url:Ljava/lang/String;

    const/4 v6, 0x1

    const/4 v7, 0x0

    invoke-static {v3, v4, v5, v6, v7}, Lcom/google/android/social/api/internal/FIFEUtil;->setImageUrlSize(IILjava/lang/String;ZZ)Ljava/lang/String;

    move-result-object v1

    invoke-static {}, Lcom/android/volley/toolbox/RequestFuture;->newFuture()Lcom/android/volley/toolbox/RequestFuture;

    move-result-object v2

    new-instance v0, Lcom/android/volley/toolbox/ImageRequest;

    iget v3, p0, Lcom/google/android/social/api/operations/DownloadImageOperation;->width:I

    iget v4, p0, Lcom/google/android/social/api/operations/DownloadImageOperation;->height:I

    sget-object v5, Landroid/graphics/Bitmap$Config;->ARGB_4444:Landroid/graphics/Bitmap$Config;

    move-object v6, v2

    invoke-direct/range {v0 .. v6}, Lcom/android/volley/toolbox/ImageRequest;-><init>(Ljava/lang/String;Lcom/android/volley/Response$Listener;IILandroid/graphics/Bitmap$Config;Lcom/android/volley/Response$ErrorListener;)V

    invoke-virtual {p2, v0}, Lcom/android/volley/RequestQueue;->add(Lcom/android/volley/Request;)Lcom/android/volley/Request;

    invoke-virtual {v2}, Lcom/android/volley/toolbox/RequestFuture;->get()Ljava/lang/Object;

    move-result-object v8

    check-cast v8, Landroid/graphics/Bitmap;

    iget-object v3, p0, Lcom/google/android/social/api/operations/DownloadImageOperation;->callback:Lcom/google/android/social/api/operations/DownloadImageOperation$Callback;

    sget-object v4, Lcom/google/android/gms/common/ConnectionResult;->RESULT_SUCCESS:Lcom/google/android/gms/common/ConnectionResult;

    iget-object v5, p0, Lcom/google/android/social/api/operations/DownloadImageOperation;->url:Ljava/lang/String;

    iget v6, p0, Lcom/google/android/social/api/operations/DownloadImageOperation;->width:I

    iget v7, p0, Lcom/google/android/social/api/operations/DownloadImageOperation;->height:I

    invoke-interface/range {v3 .. v8}, Lcom/google/android/social/api/operations/DownloadImageOperation$Callback;->onDownloadImage(Lcom/google/android/gms/common/ConnectionResult;Ljava/lang/String;IILandroid/graphics/Bitmap;)V

    return-void
.end method

.method public onFailure(Lcom/google/android/gms/common/ConnectionResult;)V
    .locals 6
    .param p1    # Lcom/google/android/gms/common/ConnectionResult;

    iget-object v0, p0, Lcom/google/android/social/api/operations/DownloadImageOperation;->callback:Lcom/google/android/social/api/operations/DownloadImageOperation$Callback;

    iget-object v2, p0, Lcom/google/android/social/api/operations/DownloadImageOperation;->url:Ljava/lang/String;

    iget v3, p0, Lcom/google/android/social/api/operations/DownloadImageOperation;->width:I

    iget v4, p0, Lcom/google/android/social/api/operations/DownloadImageOperation;->height:I

    const/4 v5, 0x0

    move-object v1, p1

    invoke-interface/range {v0 .. v5}, Lcom/google/android/social/api/operations/DownloadImageOperation$Callback;->onDownloadImage(Lcom/google/android/gms/common/ConnectionResult;Ljava/lang/String;IILandroid/graphics/Bitmap;)V

    return-void
.end method
