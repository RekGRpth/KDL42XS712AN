.class public final Lcom/google/android/social/api/operations/OperationUtils;
.super Ljava/lang/Object;
.source "OperationUtils.java"


# direct methods
.method public constructor <init>()V
    .locals 0

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method private static audienceMemberFromPersonResource(Lcom/google/api/services/plus/model/Person;)Lcom/google/android/social/api/people/model/AudienceMember;
    .locals 3
    .param p0    # Lcom/google/api/services/plus/model/Person;

    iget-object v1, p0, Lcom/google/api/services/plus/model/Person;->id:Ljava/lang/String;

    iget-object v2, p0, Lcom/google/api/services/plus/model/Person;->displayName:Ljava/lang/String;

    iget-object v0, p0, Lcom/google/api/services/plus/model/Person;->image:Lcom/google/api/services/plus/model/Person$Image;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/google/api/services/plus/model/Person;->image:Lcom/google/api/services/plus/model/Person$Image;

    iget-object v0, v0, Lcom/google/api/services/plus/model/Person$Image;->url:Ljava/lang/String;

    :goto_0
    invoke-static {v1, v2, v0}, Lcom/google/android/social/api/people/model/AudienceMember;->forPerson(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)Lcom/google/android/social/api/people/model/AudienceMember;

    move-result-object v0

    return-object v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public static personFromPersonResource(Lcom/google/api/services/plus/model/Person;)Lcom/google/android/social/api/people/model/Person;
    .locals 6
    .param p0    # Lcom/google/api/services/plus/model/Person;

    iget-object v4, p0, Lcom/google/api/services/plus/model/Person;->statusForViewer:Lcom/google/api/services/plus/model/Person$StatusForViewer;

    if-eqz v4, :cond_0

    iget-object v4, p0, Lcom/google/api/services/plus/model/Person;->statusForViewer:Lcom/google/api/services/plus/model/Person$StatusForViewer;

    iget-object v4, v4, Lcom/google/api/services/plus/model/Person$StatusForViewer;->circles:Ljava/util/List;

    if-eqz v4, :cond_0

    iget-object v4, p0, Lcom/google/api/services/plus/model/Person;->statusForViewer:Lcom/google/api/services/plus/model/Person$StatusForViewer;

    iget-object v4, v4, Lcom/google/api/services/plus/model/Person$StatusForViewer;->circles:Ljava/util/List;

    invoke-interface {v4}, Ljava/util/List;->size()I

    move-result v1

    :goto_0
    new-instance v2, Ljava/util/ArrayList;

    invoke-direct {v2, v1}, Ljava/util/ArrayList;-><init>(I)V

    const/4 v3, 0x0

    :goto_1
    if-ge v3, v1, :cond_1

    iget-object v4, p0, Lcom/google/api/services/plus/model/Person;->statusForViewer:Lcom/google/api/services/plus/model/Person$StatusForViewer;

    iget-object v4, v4, Lcom/google/api/services/plus/model/Person$StatusForViewer;->circles:Ljava/util/List;

    invoke-interface {v4, v3}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/api/services/plus/model/Person$StatusForViewer$Circles;

    iget-object v4, v0, Lcom/google/api/services/plus/model/Person$StatusForViewer$Circles;->id:Ljava/lang/String;

    iget-object v5, v0, Lcom/google/api/services/plus/model/Person$StatusForViewer$Circles;->displayName:Ljava/lang/String;

    invoke-static {v4, v5}, Lcom/google/android/social/api/people/model/AudienceMember;->forCircle(Ljava/lang/String;Ljava/lang/String;)Lcom/google/android/social/api/people/model/AudienceMember;

    move-result-object v4

    invoke-virtual {v2, v4}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    add-int/lit8 v3, v3, 0x1

    goto :goto_1

    :cond_0
    const/4 v1, 0x0

    goto :goto_0

    :cond_1
    new-instance v4, Lcom/google/android/social/api/people/model/Person;

    invoke-static {p0}, Lcom/google/android/social/api/operations/OperationUtils;->audienceMemberFromPersonResource(Lcom/google/api/services/plus/model/Person;)Lcom/google/android/social/api/people/model/AudienceMember;

    move-result-object v5

    invoke-direct {v4, v5, v2}, Lcom/google/android/social/api/people/model/Person;-><init>(Lcom/google/android/social/api/people/model/AudienceMember;Ljava/util/ArrayList;)V

    return-object v4
.end method

.method public static personListFromPeopleFeed(Lcom/google/api/services/plus/model/PeopleFeed;)Lcom/google/android/social/api/people/model/PersonList;
    .locals 4
    .param p0    # Lcom/google/api/services/plus/model/PeopleFeed;

    iget-object v3, p0, Lcom/google/api/services/plus/model/PeopleFeed;->items:Ljava/util/List;

    if-eqz v3, :cond_0

    iget-object v3, p0, Lcom/google/api/services/plus/model/PeopleFeed;->items:Ljava/util/List;

    invoke-interface {v3}, Ljava/util/List;->size()I

    move-result v2

    :goto_0
    new-instance v1, Ljava/util/ArrayList;

    invoke-direct {v1, v2}, Ljava/util/ArrayList;-><init>(I)V

    const/4 v0, 0x0

    :goto_1
    if-ge v0, v2, :cond_1

    iget-object v3, p0, Lcom/google/api/services/plus/model/PeopleFeed;->items:Ljava/util/List;

    invoke-interface {v3, v0}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Lcom/google/api/services/plus/model/Person;

    invoke-static {v3}, Lcom/google/android/social/api/operations/OperationUtils;->personFromPersonResource(Lcom/google/api/services/plus/model/Person;)Lcom/google/android/social/api/people/model/Person;

    move-result-object v3

    invoke-virtual {v1, v3}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    add-int/lit8 v0, v0, 0x1

    goto :goto_1

    :cond_0
    const/4 v2, 0x0

    goto :goto_0

    :cond_1
    new-instance v3, Lcom/google/android/social/api/people/model/PersonList;

    invoke-direct {v3, v1}, Lcom/google/android/social/api/people/model/PersonList;-><init>(Ljava/util/ArrayList;)V

    return-object v3
.end method
