.class public Lcom/google/android/social/api/DefaultPlusApiContext;
.super Ljava/lang/Object;
.source "DefaultPlusApiContext.java"

# interfaces
.implements Lcom/google/android/social/api/PlusApiContext;


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/google/android/social/api/DefaultPlusApiContext$ImageCache;
    }
.end annotation


# static fields
.field private static instance:Lcom/google/android/social/api/DefaultPlusApiContext;


# instance fields
.field private final imageCache:Lcom/google/android/social/api/DefaultPlusApiContext$ImageCache;


# direct methods
.method private constructor <init>(Lcom/google/android/social/api/DefaultPlusApiContext$ImageCache;)V
    .locals 0
    .param p1    # Lcom/google/android/social/api/DefaultPlusApiContext$ImageCache;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    iput-object p1, p0, Lcom/google/android/social/api/DefaultPlusApiContext;->imageCache:Lcom/google/android/social/api/DefaultPlusApiContext$ImageCache;

    return-void
.end method

.method public static getInstance()Lcom/google/android/social/api/DefaultPlusApiContext;
    .locals 4

    const-class v1, Lcom/google/android/social/api/DefaultPlusApiContext;

    monitor-enter v1

    :try_start_0
    sget-object v0, Lcom/google/android/social/api/DefaultPlusApiContext;->instance:Lcom/google/android/social/api/DefaultPlusApiContext;

    if-nez v0, :cond_0

    new-instance v0, Lcom/google/android/social/api/DefaultPlusApiContext;

    new-instance v2, Lcom/google/android/social/api/DefaultPlusApiContext$ImageCache;

    const/high16 v3, 0xf00000

    invoke-direct {v2, v3}, Lcom/google/android/social/api/DefaultPlusApiContext$ImageCache;-><init>(I)V

    invoke-direct {v0, v2}, Lcom/google/android/social/api/DefaultPlusApiContext;-><init>(Lcom/google/android/social/api/DefaultPlusApiContext$ImageCache;)V

    sput-object v0, Lcom/google/android/social/api/DefaultPlusApiContext;->instance:Lcom/google/android/social/api/DefaultPlusApiContext;

    :cond_0
    sget-object v0, Lcom/google/android/social/api/DefaultPlusApiContext;->instance:Lcom/google/android/social/api/DefaultPlusApiContext;

    monitor-exit v1

    return-object v0

    :catchall_0
    move-exception v0

    monitor-exit v1
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    throw v0
.end method


# virtual methods
.method public clearImageCache()V
    .locals 1

    iget-object v0, p0, Lcom/google/android/social/api/DefaultPlusApiContext;->imageCache:Lcom/google/android/social/api/DefaultPlusApiContext$ImageCache;

    invoke-virtual {v0}, Lcom/google/android/social/api/DefaultPlusApiContext$ImageCache;->evictAll()V

    return-void
.end method

.method public getImage(Ljava/lang/String;)Landroid/graphics/Bitmap;
    .locals 1
    .param p1    # Ljava/lang/String;

    iget-object v0, p0, Lcom/google/android/social/api/DefaultPlusApiContext;->imageCache:Lcom/google/android/social/api/DefaultPlusApiContext$ImageCache;

    invoke-virtual {v0, p1}, Lcom/google/android/social/api/DefaultPlusApiContext$ImageCache;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/graphics/Bitmap;

    return-object v0
.end method

.method public putImage(Ljava/lang/String;Landroid/graphics/Bitmap;)V
    .locals 1
    .param p1    # Ljava/lang/String;
    .param p2    # Landroid/graphics/Bitmap;

    iget-object v0, p0, Lcom/google/android/social/api/DefaultPlusApiContext;->imageCache:Lcom/google/android/social/api/DefaultPlusApiContext$ImageCache;

    invoke-virtual {v0, p1, p2}, Lcom/google/android/social/api/DefaultPlusApiContext$ImageCache;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    return-void
.end method
