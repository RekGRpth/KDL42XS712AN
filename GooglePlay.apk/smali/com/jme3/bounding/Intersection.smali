.class public Lcom/jme3/bounding/Intersection;
.super Ljava/lang/Object;
.source "Intersection.java"


# direct methods
.method public constructor <init>()V
    .locals 0

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method private static final findMinMax(FFFLcom/jme3/math/Vector3f;)V
    .locals 1
    .param p0    # F
    .param p1    # F
    .param p2    # F
    .param p3    # Lcom/jme3/math/Vector3f;

    const/4 v0, 0x0

    invoke-virtual {p3, p0, p0, v0}, Lcom/jme3/math/Vector3f;->set(FFF)Lcom/jme3/math/Vector3f;

    iget v0, p3, Lcom/jme3/math/Vector3f;->x:F

    cmpg-float v0, p1, v0

    if-gez v0, :cond_0

    invoke-virtual {p3, p1}, Lcom/jme3/math/Vector3f;->setX(F)Lcom/jme3/math/Vector3f;

    :cond_0
    iget v0, p3, Lcom/jme3/math/Vector3f;->y:F

    cmpl-float v0, p1, v0

    if-lez v0, :cond_1

    invoke-virtual {p3, p1}, Lcom/jme3/math/Vector3f;->setY(F)Lcom/jme3/math/Vector3f;

    :cond_1
    iget v0, p3, Lcom/jme3/math/Vector3f;->x:F

    cmpg-float v0, p2, v0

    if-gez v0, :cond_2

    invoke-virtual {p3, p2}, Lcom/jme3/math/Vector3f;->setX(F)Lcom/jme3/math/Vector3f;

    :cond_2
    iget v0, p3, Lcom/jme3/math/Vector3f;->y:F

    cmpl-float v0, p2, v0

    if-lez v0, :cond_3

    invoke-virtual {p3, p2}, Lcom/jme3/math/Vector3f;->setY(F)Lcom/jme3/math/Vector3f;

    :cond_3
    return-void
.end method

.method public static intersect(Lcom/jme3/bounding/BoundingBox;Lcom/jme3/math/Vector3f;Lcom/jme3/math/Vector3f;Lcom/jme3/math/Vector3f;)Z
    .locals 26
    .param p0    # Lcom/jme3/bounding/BoundingBox;
    .param p1    # Lcom/jme3/math/Vector3f;
    .param p2    # Lcom/jme3/math/Vector3f;
    .param p3    # Lcom/jme3/math/Vector3f;

    invoke-static {}, Lcom/jme3/util/TempVars;->get()Lcom/jme3/util/TempVars;

    move-result-object v22

    move-object/from16 v0, v22

    iget-object v0, v0, Lcom/jme3/util/TempVars;->vect1:Lcom/jme3/math/Vector3f;

    move-object/from16 v19, v0

    move-object/from16 v0, v22

    iget-object v0, v0, Lcom/jme3/util/TempVars;->vect2:Lcom/jme3/math/Vector3f;

    move-object/from16 v20, v0

    move-object/from16 v0, v22

    iget-object v0, v0, Lcom/jme3/util/TempVars;->vect3:Lcom/jme3/math/Vector3f;

    move-object/from16 v21, v0

    move-object/from16 v0, v22

    iget-object v4, v0, Lcom/jme3/util/TempVars;->vect4:Lcom/jme3/math/Vector3f;

    move-object/from16 v0, v22

    iget-object v5, v0, Lcom/jme3/util/TempVars;->vect5:Lcom/jme3/math/Vector3f;

    move-object/from16 v0, v22

    iget-object v6, v0, Lcom/jme3/util/TempVars;->vect6:Lcom/jme3/math/Vector3f;

    invoke-virtual/range {p0 .. p0}, Lcom/jme3/bounding/BoundingBox;->getCenter()Lcom/jme3/math/Vector3f;

    move-result-object v3

    const/16 v23, 0x0

    move-object/from16 v0, p0

    move-object/from16 v1, v23

    invoke-virtual {v0, v1}, Lcom/jme3/bounding/BoundingBox;->getExtent(Lcom/jme3/math/Vector3f;)Lcom/jme3/math/Vector3f;

    move-result-object v7

    move-object/from16 v0, p1

    move-object/from16 v1, v19

    invoke-virtual {v0, v3, v1}, Lcom/jme3/math/Vector3f;->subtract(Lcom/jme3/math/Vector3f;Lcom/jme3/math/Vector3f;)Lcom/jme3/math/Vector3f;

    move-object/from16 v0, p2

    move-object/from16 v1, v20

    invoke-virtual {v0, v3, v1}, Lcom/jme3/math/Vector3f;->subtract(Lcom/jme3/math/Vector3f;Lcom/jme3/math/Vector3f;)Lcom/jme3/math/Vector3f;

    move-object/from16 v0, p3

    move-object/from16 v1, v21

    invoke-virtual {v0, v3, v1}, Lcom/jme3/math/Vector3f;->subtract(Lcom/jme3/math/Vector3f;Lcom/jme3/math/Vector3f;)Lcom/jme3/math/Vector3f;

    move-object/from16 v0, v20

    move-object/from16 v1, v19

    invoke-virtual {v0, v1, v4}, Lcom/jme3/math/Vector3f;->subtract(Lcom/jme3/math/Vector3f;Lcom/jme3/math/Vector3f;)Lcom/jme3/math/Vector3f;

    move-object/from16 v0, v21

    move-object/from16 v1, v20

    invoke-virtual {v0, v1, v5}, Lcom/jme3/math/Vector3f;->subtract(Lcom/jme3/math/Vector3f;Lcom/jme3/math/Vector3f;)Lcom/jme3/math/Vector3f;

    move-object/from16 v0, v19

    move-object/from16 v1, v21

    invoke-virtual {v0, v1, v6}, Lcom/jme3/math/Vector3f;->subtract(Lcom/jme3/math/Vector3f;Lcom/jme3/math/Vector3f;)Lcom/jme3/math/Vector3f;

    iget v0, v4, Lcom/jme3/math/Vector3f;->x:F

    move/from16 v23, v0

    invoke-static/range {v23 .. v23}, Lcom/jme3/math/FastMath;->abs(F)F

    move-result v8

    iget v0, v4, Lcom/jme3/math/Vector3f;->y:F

    move/from16 v23, v0

    invoke-static/range {v23 .. v23}, Lcom/jme3/math/FastMath;->abs(F)F

    move-result v9

    iget v0, v4, Lcom/jme3/math/Vector3f;->z:F

    move/from16 v23, v0

    invoke-static/range {v23 .. v23}, Lcom/jme3/math/FastMath;->abs(F)F

    move-result v10

    iget v0, v4, Lcom/jme3/math/Vector3f;->z:F

    move/from16 v23, v0

    move-object/from16 v0, v19

    iget v0, v0, Lcom/jme3/math/Vector3f;->y:F

    move/from16 v24, v0

    mul-float v23, v23, v24

    iget v0, v4, Lcom/jme3/math/Vector3f;->y:F

    move/from16 v24, v0

    move-object/from16 v0, v19

    iget v0, v0, Lcom/jme3/math/Vector3f;->z:F

    move/from16 v25, v0

    mul-float v24, v24, v25

    sub-float v15, v23, v24

    iget v0, v4, Lcom/jme3/math/Vector3f;->z:F

    move/from16 v23, v0

    move-object/from16 v0, v21

    iget v0, v0, Lcom/jme3/math/Vector3f;->y:F

    move/from16 v24, v0

    mul-float v23, v23, v24

    iget v0, v4, Lcom/jme3/math/Vector3f;->y:F

    move/from16 v24, v0

    move-object/from16 v0, v21

    iget v0, v0, Lcom/jme3/math/Vector3f;->z:F

    move/from16 v25, v0

    mul-float v24, v24, v25

    sub-float v17, v23, v24

    move/from16 v0, v17

    invoke-static {v15, v0}, Ljava/lang/Math;->min(FF)F

    move-result v12

    move/from16 v0, v17

    invoke-static {v15, v0}, Ljava/lang/Math;->max(FF)F

    move-result v11

    iget v0, v7, Lcom/jme3/math/Vector3f;->y:F

    move/from16 v23, v0

    mul-float v23, v23, v10

    iget v0, v7, Lcom/jme3/math/Vector3f;->z:F

    move/from16 v24, v0

    mul-float v24, v24, v9

    add-float v18, v23, v24

    cmpl-float v23, v12, v18

    if-gtz v23, :cond_0

    move/from16 v0, v18

    neg-float v0, v0

    move/from16 v23, v0

    cmpg-float v23, v11, v23

    if-gez v23, :cond_1

    :cond_0
    invoke-virtual/range {v22 .. v22}, Lcom/jme3/util/TempVars;->release()V

    const/16 v23, 0x0

    :goto_0
    return v23

    :cond_1
    iget v0, v4, Lcom/jme3/math/Vector3f;->z:F

    move/from16 v23, v0

    move/from16 v0, v23

    neg-float v0, v0

    move/from16 v23, v0

    move-object/from16 v0, v19

    iget v0, v0, Lcom/jme3/math/Vector3f;->x:F

    move/from16 v24, v0

    mul-float v23, v23, v24

    iget v0, v4, Lcom/jme3/math/Vector3f;->x:F

    move/from16 v24, v0

    move-object/from16 v0, v19

    iget v0, v0, Lcom/jme3/math/Vector3f;->z:F

    move/from16 v25, v0

    mul-float v24, v24, v25

    add-float v15, v23, v24

    iget v0, v4, Lcom/jme3/math/Vector3f;->z:F

    move/from16 v23, v0

    move/from16 v0, v23

    neg-float v0, v0

    move/from16 v23, v0

    move-object/from16 v0, v21

    iget v0, v0, Lcom/jme3/math/Vector3f;->x:F

    move/from16 v24, v0

    mul-float v23, v23, v24

    iget v0, v4, Lcom/jme3/math/Vector3f;->x:F

    move/from16 v24, v0

    move-object/from16 v0, v21

    iget v0, v0, Lcom/jme3/math/Vector3f;->z:F

    move/from16 v25, v0

    mul-float v24, v24, v25

    add-float v17, v23, v24

    move/from16 v0, v17

    invoke-static {v15, v0}, Ljava/lang/Math;->min(FF)F

    move-result v12

    move/from16 v0, v17

    invoke-static {v15, v0}, Ljava/lang/Math;->max(FF)F

    move-result v11

    iget v0, v7, Lcom/jme3/math/Vector3f;->x:F

    move/from16 v23, v0

    mul-float v23, v23, v10

    iget v0, v7, Lcom/jme3/math/Vector3f;->z:F

    move/from16 v24, v0

    mul-float v24, v24, v8

    add-float v18, v23, v24

    cmpl-float v23, v12, v18

    if-gtz v23, :cond_2

    move/from16 v0, v18

    neg-float v0, v0

    move/from16 v23, v0

    cmpg-float v23, v11, v23

    if-gez v23, :cond_3

    :cond_2
    invoke-virtual/range {v22 .. v22}, Lcom/jme3/util/TempVars;->release()V

    const/16 v23, 0x0

    goto :goto_0

    :cond_3
    iget v0, v4, Lcom/jme3/math/Vector3f;->y:F

    move/from16 v23, v0

    move-object/from16 v0, v20

    iget v0, v0, Lcom/jme3/math/Vector3f;->x:F

    move/from16 v24, v0

    mul-float v23, v23, v24

    iget v0, v4, Lcom/jme3/math/Vector3f;->x:F

    move/from16 v24, v0

    move-object/from16 v0, v20

    iget v0, v0, Lcom/jme3/math/Vector3f;->y:F

    move/from16 v25, v0

    mul-float v24, v24, v25

    sub-float v16, v23, v24

    iget v0, v4, Lcom/jme3/math/Vector3f;->y:F

    move/from16 v23, v0

    move-object/from16 v0, v21

    iget v0, v0, Lcom/jme3/math/Vector3f;->x:F

    move/from16 v24, v0

    mul-float v23, v23, v24

    iget v0, v4, Lcom/jme3/math/Vector3f;->x:F

    move/from16 v24, v0

    move-object/from16 v0, v21

    iget v0, v0, Lcom/jme3/math/Vector3f;->y:F

    move/from16 v25, v0

    mul-float v24, v24, v25

    sub-float v17, v23, v24

    invoke-static/range {v16 .. v17}, Ljava/lang/Math;->min(FF)F

    move-result v12

    invoke-static/range {v16 .. v17}, Ljava/lang/Math;->max(FF)F

    move-result v11

    iget v0, v7, Lcom/jme3/math/Vector3f;->x:F

    move/from16 v23, v0

    mul-float v23, v23, v9

    iget v0, v7, Lcom/jme3/math/Vector3f;->y:F

    move/from16 v24, v0

    mul-float v24, v24, v8

    add-float v18, v23, v24

    cmpl-float v23, v12, v18

    if-gtz v23, :cond_4

    move/from16 v0, v18

    neg-float v0, v0

    move/from16 v23, v0

    cmpg-float v23, v11, v23

    if-gez v23, :cond_5

    :cond_4
    invoke-virtual/range {v22 .. v22}, Lcom/jme3/util/TempVars;->release()V

    const/16 v23, 0x0

    goto/16 :goto_0

    :cond_5
    iget v0, v5, Lcom/jme3/math/Vector3f;->x:F

    move/from16 v23, v0

    invoke-static/range {v23 .. v23}, Lcom/jme3/math/FastMath;->abs(F)F

    move-result v8

    iget v0, v5, Lcom/jme3/math/Vector3f;->y:F

    move/from16 v23, v0

    invoke-static/range {v23 .. v23}, Lcom/jme3/math/FastMath;->abs(F)F

    move-result v9

    iget v0, v5, Lcom/jme3/math/Vector3f;->z:F

    move/from16 v23, v0

    invoke-static/range {v23 .. v23}, Lcom/jme3/math/FastMath;->abs(F)F

    move-result v10

    iget v0, v5, Lcom/jme3/math/Vector3f;->z:F

    move/from16 v23, v0

    move-object/from16 v0, v19

    iget v0, v0, Lcom/jme3/math/Vector3f;->y:F

    move/from16 v24, v0

    mul-float v23, v23, v24

    iget v0, v5, Lcom/jme3/math/Vector3f;->y:F

    move/from16 v24, v0

    move-object/from16 v0, v19

    iget v0, v0, Lcom/jme3/math/Vector3f;->z:F

    move/from16 v25, v0

    mul-float v24, v24, v25

    sub-float v15, v23, v24

    iget v0, v5, Lcom/jme3/math/Vector3f;->z:F

    move/from16 v23, v0

    move-object/from16 v0, v21

    iget v0, v0, Lcom/jme3/math/Vector3f;->y:F

    move/from16 v24, v0

    mul-float v23, v23, v24

    iget v0, v5, Lcom/jme3/math/Vector3f;->y:F

    move/from16 v24, v0

    move-object/from16 v0, v21

    iget v0, v0, Lcom/jme3/math/Vector3f;->z:F

    move/from16 v25, v0

    mul-float v24, v24, v25

    sub-float v17, v23, v24

    move/from16 v0, v17

    invoke-static {v15, v0}, Ljava/lang/Math;->min(FF)F

    move-result v12

    move/from16 v0, v17

    invoke-static {v15, v0}, Ljava/lang/Math;->max(FF)F

    move-result v11

    iget v0, v7, Lcom/jme3/math/Vector3f;->y:F

    move/from16 v23, v0

    mul-float v23, v23, v10

    iget v0, v7, Lcom/jme3/math/Vector3f;->z:F

    move/from16 v24, v0

    mul-float v24, v24, v9

    add-float v18, v23, v24

    cmpl-float v23, v12, v18

    if-gtz v23, :cond_6

    move/from16 v0, v18

    neg-float v0, v0

    move/from16 v23, v0

    cmpg-float v23, v11, v23

    if-gez v23, :cond_7

    :cond_6
    invoke-virtual/range {v22 .. v22}, Lcom/jme3/util/TempVars;->release()V

    const/16 v23, 0x0

    goto/16 :goto_0

    :cond_7
    iget v0, v5, Lcom/jme3/math/Vector3f;->z:F

    move/from16 v23, v0

    move/from16 v0, v23

    neg-float v0, v0

    move/from16 v23, v0

    move-object/from16 v0, v19

    iget v0, v0, Lcom/jme3/math/Vector3f;->x:F

    move/from16 v24, v0

    mul-float v23, v23, v24

    iget v0, v5, Lcom/jme3/math/Vector3f;->x:F

    move/from16 v24, v0

    move-object/from16 v0, v19

    iget v0, v0, Lcom/jme3/math/Vector3f;->z:F

    move/from16 v25, v0

    mul-float v24, v24, v25

    add-float v15, v23, v24

    iget v0, v5, Lcom/jme3/math/Vector3f;->z:F

    move/from16 v23, v0

    move/from16 v0, v23

    neg-float v0, v0

    move/from16 v23, v0

    move-object/from16 v0, v21

    iget v0, v0, Lcom/jme3/math/Vector3f;->x:F

    move/from16 v24, v0

    mul-float v23, v23, v24

    iget v0, v5, Lcom/jme3/math/Vector3f;->x:F

    move/from16 v24, v0

    move-object/from16 v0, v21

    iget v0, v0, Lcom/jme3/math/Vector3f;->z:F

    move/from16 v25, v0

    mul-float v24, v24, v25

    add-float v17, v23, v24

    move/from16 v0, v17

    invoke-static {v15, v0}, Ljava/lang/Math;->min(FF)F

    move-result v12

    move/from16 v0, v17

    invoke-static {v15, v0}, Ljava/lang/Math;->max(FF)F

    move-result v11

    iget v0, v7, Lcom/jme3/math/Vector3f;->x:F

    move/from16 v23, v0

    mul-float v23, v23, v10

    iget v0, v7, Lcom/jme3/math/Vector3f;->z:F

    move/from16 v24, v0

    mul-float v24, v24, v8

    add-float v18, v23, v24

    cmpl-float v23, v12, v18

    if-gtz v23, :cond_8

    move/from16 v0, v18

    neg-float v0, v0

    move/from16 v23, v0

    cmpg-float v23, v11, v23

    if-gez v23, :cond_9

    :cond_8
    invoke-virtual/range {v22 .. v22}, Lcom/jme3/util/TempVars;->release()V

    const/16 v23, 0x0

    goto/16 :goto_0

    :cond_9
    iget v0, v5, Lcom/jme3/math/Vector3f;->y:F

    move/from16 v23, v0

    move-object/from16 v0, v19

    iget v0, v0, Lcom/jme3/math/Vector3f;->x:F

    move/from16 v24, v0

    mul-float v23, v23, v24

    iget v0, v5, Lcom/jme3/math/Vector3f;->x:F

    move/from16 v24, v0

    move-object/from16 v0, v19

    iget v0, v0, Lcom/jme3/math/Vector3f;->y:F

    move/from16 v25, v0

    mul-float v24, v24, v25

    sub-float v15, v23, v24

    iget v0, v5, Lcom/jme3/math/Vector3f;->y:F

    move/from16 v23, v0

    move-object/from16 v0, v20

    iget v0, v0, Lcom/jme3/math/Vector3f;->x:F

    move/from16 v24, v0

    mul-float v23, v23, v24

    iget v0, v5, Lcom/jme3/math/Vector3f;->x:F

    move/from16 v24, v0

    move-object/from16 v0, v20

    iget v0, v0, Lcom/jme3/math/Vector3f;->y:F

    move/from16 v25, v0

    mul-float v24, v24, v25

    sub-float v16, v23, v24

    invoke-static/range {v15 .. v16}, Ljava/lang/Math;->min(FF)F

    move-result v12

    invoke-static/range {v15 .. v16}, Ljava/lang/Math;->max(FF)F

    move-result v11

    iget v0, v7, Lcom/jme3/math/Vector3f;->x:F

    move/from16 v23, v0

    mul-float v23, v23, v9

    iget v0, v7, Lcom/jme3/math/Vector3f;->y:F

    move/from16 v24, v0

    mul-float v24, v24, v8

    add-float v18, v23, v24

    cmpl-float v23, v12, v18

    if-gtz v23, :cond_a

    move/from16 v0, v18

    neg-float v0, v0

    move/from16 v23, v0

    cmpg-float v23, v11, v23

    if-gez v23, :cond_b

    :cond_a
    invoke-virtual/range {v22 .. v22}, Lcom/jme3/util/TempVars;->release()V

    const/16 v23, 0x0

    goto/16 :goto_0

    :cond_b
    iget v0, v6, Lcom/jme3/math/Vector3f;->x:F

    move/from16 v23, v0

    invoke-static/range {v23 .. v23}, Lcom/jme3/math/FastMath;->abs(F)F

    move-result v8

    iget v0, v6, Lcom/jme3/math/Vector3f;->y:F

    move/from16 v23, v0

    invoke-static/range {v23 .. v23}, Lcom/jme3/math/FastMath;->abs(F)F

    move-result v9

    iget v0, v6, Lcom/jme3/math/Vector3f;->z:F

    move/from16 v23, v0

    invoke-static/range {v23 .. v23}, Lcom/jme3/math/FastMath;->abs(F)F

    move-result v10

    iget v0, v6, Lcom/jme3/math/Vector3f;->z:F

    move/from16 v23, v0

    move-object/from16 v0, v19

    iget v0, v0, Lcom/jme3/math/Vector3f;->y:F

    move/from16 v24, v0

    mul-float v23, v23, v24

    iget v0, v6, Lcom/jme3/math/Vector3f;->y:F

    move/from16 v24, v0

    move-object/from16 v0, v19

    iget v0, v0, Lcom/jme3/math/Vector3f;->z:F

    move/from16 v25, v0

    mul-float v24, v24, v25

    sub-float v15, v23, v24

    iget v0, v6, Lcom/jme3/math/Vector3f;->z:F

    move/from16 v23, v0

    move-object/from16 v0, v20

    iget v0, v0, Lcom/jme3/math/Vector3f;->y:F

    move/from16 v24, v0

    mul-float v23, v23, v24

    iget v0, v6, Lcom/jme3/math/Vector3f;->y:F

    move/from16 v24, v0

    move-object/from16 v0, v20

    iget v0, v0, Lcom/jme3/math/Vector3f;->z:F

    move/from16 v25, v0

    mul-float v24, v24, v25

    sub-float v16, v23, v24

    invoke-static/range {v15 .. v16}, Ljava/lang/Math;->min(FF)F

    move-result v12

    invoke-static/range {v15 .. v16}, Ljava/lang/Math;->max(FF)F

    move-result v11

    iget v0, v7, Lcom/jme3/math/Vector3f;->y:F

    move/from16 v23, v0

    mul-float v23, v23, v10

    iget v0, v7, Lcom/jme3/math/Vector3f;->z:F

    move/from16 v24, v0

    mul-float v24, v24, v9

    add-float v18, v23, v24

    cmpl-float v23, v12, v18

    if-gtz v23, :cond_c

    move/from16 v0, v18

    neg-float v0, v0

    move/from16 v23, v0

    cmpg-float v23, v11, v23

    if-gez v23, :cond_d

    :cond_c
    invoke-virtual/range {v22 .. v22}, Lcom/jme3/util/TempVars;->release()V

    const/16 v23, 0x0

    goto/16 :goto_0

    :cond_d
    iget v0, v6, Lcom/jme3/math/Vector3f;->z:F

    move/from16 v23, v0

    move/from16 v0, v23

    neg-float v0, v0

    move/from16 v23, v0

    move-object/from16 v0, v19

    iget v0, v0, Lcom/jme3/math/Vector3f;->x:F

    move/from16 v24, v0

    mul-float v23, v23, v24

    iget v0, v6, Lcom/jme3/math/Vector3f;->x:F

    move/from16 v24, v0

    move-object/from16 v0, v19

    iget v0, v0, Lcom/jme3/math/Vector3f;->z:F

    move/from16 v25, v0

    mul-float v24, v24, v25

    add-float v15, v23, v24

    iget v0, v6, Lcom/jme3/math/Vector3f;->z:F

    move/from16 v23, v0

    move/from16 v0, v23

    neg-float v0, v0

    move/from16 v23, v0

    move-object/from16 v0, v20

    iget v0, v0, Lcom/jme3/math/Vector3f;->x:F

    move/from16 v24, v0

    mul-float v23, v23, v24

    iget v0, v6, Lcom/jme3/math/Vector3f;->x:F

    move/from16 v24, v0

    move-object/from16 v0, v20

    iget v0, v0, Lcom/jme3/math/Vector3f;->z:F

    move/from16 v25, v0

    mul-float v24, v24, v25

    add-float v16, v23, v24

    invoke-static/range {v15 .. v16}, Ljava/lang/Math;->min(FF)F

    move-result v12

    invoke-static/range {v15 .. v16}, Ljava/lang/Math;->max(FF)F

    move-result v11

    iget v0, v7, Lcom/jme3/math/Vector3f;->x:F

    move/from16 v23, v0

    mul-float v23, v23, v10

    iget v0, v7, Lcom/jme3/math/Vector3f;->y:F

    move/from16 v24, v0

    mul-float v24, v24, v8

    add-float v18, v23, v24

    cmpl-float v23, v12, v18

    if-gtz v23, :cond_e

    move/from16 v0, v18

    neg-float v0, v0

    move/from16 v23, v0

    cmpg-float v23, v11, v23

    if-gez v23, :cond_f

    :cond_e
    invoke-virtual/range {v22 .. v22}, Lcom/jme3/util/TempVars;->release()V

    const/16 v23, 0x0

    goto/16 :goto_0

    :cond_f
    iget v0, v6, Lcom/jme3/math/Vector3f;->y:F

    move/from16 v23, v0

    move-object/from16 v0, v20

    iget v0, v0, Lcom/jme3/math/Vector3f;->x:F

    move/from16 v24, v0

    mul-float v23, v23, v24

    iget v0, v6, Lcom/jme3/math/Vector3f;->x:F

    move/from16 v24, v0

    move-object/from16 v0, v20

    iget v0, v0, Lcom/jme3/math/Vector3f;->y:F

    move/from16 v25, v0

    mul-float v24, v24, v25

    sub-float v16, v23, v24

    iget v0, v6, Lcom/jme3/math/Vector3f;->y:F

    move/from16 v23, v0

    move-object/from16 v0, v21

    iget v0, v0, Lcom/jme3/math/Vector3f;->x:F

    move/from16 v24, v0

    mul-float v23, v23, v24

    iget v0, v6, Lcom/jme3/math/Vector3f;->x:F

    move/from16 v24, v0

    move-object/from16 v0, v21

    iget v0, v0, Lcom/jme3/math/Vector3f;->y:F

    move/from16 v25, v0

    mul-float v24, v24, v25

    sub-float v17, v23, v24

    invoke-static/range {v16 .. v17}, Ljava/lang/Math;->min(FF)F

    move-result v12

    invoke-static/range {v16 .. v17}, Ljava/lang/Math;->max(FF)F

    move-result v11

    iget v0, v7, Lcom/jme3/math/Vector3f;->x:F

    move/from16 v23, v0

    mul-float v23, v23, v9

    iget v0, v7, Lcom/jme3/math/Vector3f;->y:F

    move/from16 v24, v0

    mul-float v24, v24, v8

    add-float v18, v23, v24

    cmpl-float v23, v12, v18

    if-gtz v23, :cond_10

    move/from16 v0, v18

    neg-float v0, v0

    move/from16 v23, v0

    cmpg-float v23, v11, v23

    if-gez v23, :cond_11

    :cond_10
    invoke-virtual/range {v22 .. v22}, Lcom/jme3/util/TempVars;->release()V

    const/16 v23, 0x0

    goto/16 :goto_0

    :cond_11
    move-object/from16 v0, v22

    iget-object v13, v0, Lcom/jme3/util/TempVars;->vect7:Lcom/jme3/math/Vector3f;

    move-object/from16 v0, v19

    iget v0, v0, Lcom/jme3/math/Vector3f;->x:F

    move/from16 v23, v0

    move-object/from16 v0, v20

    iget v0, v0, Lcom/jme3/math/Vector3f;->x:F

    move/from16 v24, v0

    move-object/from16 v0, v21

    iget v0, v0, Lcom/jme3/math/Vector3f;->x:F

    move/from16 v25, v0

    move/from16 v0, v23

    move/from16 v1, v24

    move/from16 v2, v25

    invoke-static {v0, v1, v2, v13}, Lcom/jme3/bounding/Intersection;->findMinMax(FFFLcom/jme3/math/Vector3f;)V

    iget v0, v13, Lcom/jme3/math/Vector3f;->x:F

    move/from16 v23, v0

    iget v0, v7, Lcom/jme3/math/Vector3f;->x:F

    move/from16 v24, v0

    cmpl-float v23, v23, v24

    if-gtz v23, :cond_12

    iget v0, v13, Lcom/jme3/math/Vector3f;->y:F

    move/from16 v23, v0

    iget v0, v7, Lcom/jme3/math/Vector3f;->x:F

    move/from16 v24, v0

    move/from16 v0, v24

    neg-float v0, v0

    move/from16 v24, v0

    cmpg-float v23, v23, v24

    if-gez v23, :cond_13

    :cond_12
    invoke-virtual/range {v22 .. v22}, Lcom/jme3/util/TempVars;->release()V

    const/16 v23, 0x0

    goto/16 :goto_0

    :cond_13
    move-object/from16 v0, v19

    iget v0, v0, Lcom/jme3/math/Vector3f;->y:F

    move/from16 v23, v0

    move-object/from16 v0, v20

    iget v0, v0, Lcom/jme3/math/Vector3f;->y:F

    move/from16 v24, v0

    move-object/from16 v0, v21

    iget v0, v0, Lcom/jme3/math/Vector3f;->y:F

    move/from16 v25, v0

    move/from16 v0, v23

    move/from16 v1, v24

    move/from16 v2, v25

    invoke-static {v0, v1, v2, v13}, Lcom/jme3/bounding/Intersection;->findMinMax(FFFLcom/jme3/math/Vector3f;)V

    iget v0, v13, Lcom/jme3/math/Vector3f;->x:F

    move/from16 v23, v0

    iget v0, v7, Lcom/jme3/math/Vector3f;->y:F

    move/from16 v24, v0

    cmpl-float v23, v23, v24

    if-gtz v23, :cond_14

    iget v0, v13, Lcom/jme3/math/Vector3f;->y:F

    move/from16 v23, v0

    iget v0, v7, Lcom/jme3/math/Vector3f;->y:F

    move/from16 v24, v0

    move/from16 v0, v24

    neg-float v0, v0

    move/from16 v24, v0

    cmpg-float v23, v23, v24

    if-gez v23, :cond_15

    :cond_14
    invoke-virtual/range {v22 .. v22}, Lcom/jme3/util/TempVars;->release()V

    const/16 v23, 0x0

    goto/16 :goto_0

    :cond_15
    move-object/from16 v0, v19

    iget v0, v0, Lcom/jme3/math/Vector3f;->z:F

    move/from16 v23, v0

    move-object/from16 v0, v20

    iget v0, v0, Lcom/jme3/math/Vector3f;->z:F

    move/from16 v24, v0

    move-object/from16 v0, v21

    iget v0, v0, Lcom/jme3/math/Vector3f;->z:F

    move/from16 v25, v0

    move/from16 v0, v23

    move/from16 v1, v24

    move/from16 v2, v25

    invoke-static {v0, v1, v2, v13}, Lcom/jme3/bounding/Intersection;->findMinMax(FFFLcom/jme3/math/Vector3f;)V

    iget v0, v13, Lcom/jme3/math/Vector3f;->x:F

    move/from16 v23, v0

    iget v0, v7, Lcom/jme3/math/Vector3f;->z:F

    move/from16 v24, v0

    cmpl-float v23, v23, v24

    if-gtz v23, :cond_16

    iget v0, v13, Lcom/jme3/math/Vector3f;->y:F

    move/from16 v23, v0

    iget v0, v7, Lcom/jme3/math/Vector3f;->z:F

    move/from16 v24, v0

    move/from16 v0, v24

    neg-float v0, v0

    move/from16 v24, v0

    cmpg-float v23, v23, v24

    if-gez v23, :cond_17

    :cond_16
    invoke-virtual/range {v22 .. v22}, Lcom/jme3/util/TempVars;->release()V

    const/16 v23, 0x0

    goto/16 :goto_0

    :cond_17
    move-object/from16 v0, v22

    iget-object v14, v0, Lcom/jme3/util/TempVars;->plane:Lcom/jme3/math/Plane;

    move-object/from16 v0, p1

    move-object/from16 v1, p2

    move-object/from16 v2, p3

    invoke-virtual {v14, v0, v1, v2}, Lcom/jme3/math/Plane;->setPlanePoints(Lcom/jme3/math/Vector3f;Lcom/jme3/math/Vector3f;Lcom/jme3/math/Vector3f;)V

    move-object/from16 v0, p0

    invoke-virtual {v0, v14}, Lcom/jme3/bounding/BoundingBox;->whichSide(Lcom/jme3/math/Plane;)Lcom/jme3/math/Plane$Side;

    move-result-object v23

    sget-object v24, Lcom/jme3/math/Plane$Side;->Negative:Lcom/jme3/math/Plane$Side;

    move-object/from16 v0, v23

    move-object/from16 v1, v24

    if-ne v0, v1, :cond_18

    invoke-virtual/range {v22 .. v22}, Lcom/jme3/util/TempVars;->release()V

    const/16 v23, 0x0

    goto/16 :goto_0

    :cond_18
    invoke-virtual/range {v22 .. v22}, Lcom/jme3/util/TempVars;->release()V

    const/16 v23, 0x1

    goto/16 :goto_0
.end method
